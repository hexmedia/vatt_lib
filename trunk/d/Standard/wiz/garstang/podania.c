//Vera. Pomieszczenie zarz�dzania podaniami graczy.
inherit "/std/room";

#include <stdproperties.h>
#define GAR "/d/Standard/wiz/garstang/"
#include "/d/Standard/Redania/Rinde/Centrum/Ratusz/admin/admin.h"

create_room()
{
    set_short("Zarz�dzanie podaniami");
    set_long("W tej ciasnej szafie zewsz�d otaczaj� ci� papierzyska, stare, "+
		"i nowe, bia�e, zdobione, po��k�e, grube pliki i sterty pojedynczych "+
		"papierk�w. S�owem: kupa brudnej papierkowej roboty.\n");
  
    add_exit(GAR+"korytarz2.c",({"w","wg��b korytarza",
				"z szafy widniej�cej na wschodzie"}));
    add_prop(ROOM_I_INSIDE,1);
}


void
usun_imie(string imie,object kto)
{
    (MAIN)->usun_podanie(kto,imie);
    return;
}

int
usun(string str)
{
    //if (this_player()->query_name() != BURMISTRZ)
        if (member_array(this_player()->query_name(), CZARODZIEJE) == -1)
            return 0;
        
    if(lower_case(str)!="podanie")
        {
            notify_fail("Co chcesz usun^a^c? Podanie?\n");
                return 0;
        }
    write("Podaj imi^e osoby, kt^orej podanie chcesz usun^a^c: ");
    input_to("usun_imie",0,this_player());
    return 1;
}

void
przeczytaj_imie(string imie,object czytajacy,string jakie)
{
     imie=lower_case(imie);
         jakie=lower_case(jakie);

         if(!imie)
         {
             czytajacy->catch_msg("Musisz poda^c jakie^s imi^e!\n");
                 return;
         }

     switch(jakie)
         {
             case "podanie":  (MAIN)->przeczytaj_podanie(czytajacy,imie);
                                  break;
                 case "archiwum": (MAIN)->wczytaj_z_archiwum(czytajacy,imie);
                                  break;
                 default : czytajacy->catch_msg("Co chcesz przeczyta^c? Podanie czy archiwum?\n");
                           return;
                                   break;
         }

        return;
}

int
przeczytaj(string str)
{
    //if (this_player()->query_name() != BURMISTRZ)
        if (member_array(this_player()->query_name(), CZARODZIEJE) == -1)
            if (member_array(this_player()->query_name(), RADNI) == -1)
                return 0;
        
    str=lower_case(str);

    if(str!="podanie" && str!="archiwum")
        {
           notify_fail("Co chcesz przeczyta^c? Podanie czy archiwum?\n");
           return 0;
        }

        write("Podaj imi^e osoby, kt^orej podanie chcesz przeczyta^c: ");
        input_to("przeczytaj_imie",0,this_player(),str);
        return 1;
}

void
zatwierdz_imie(string imie,object kto)
{
        (MAIN)->zatwierdz_podanie(kto,imie,1);
        return;
}

int
zatwierdz(string str)
{
    //if (this_player()->query_name() != BURMISTRZ)
        if (member_array(this_player()->query_name(), CZARODZIEJE) == -1)
            return 0;
        
    write("Podaj imi^e osoby, kt^orej podanie chcesz zatwierdzi^c: ");
    input_to("zatwierdz_imie",0,this_player());
    return 1;
}

int
przejrzyj(string str)
{
    //if (this_player()->query_name() != BURMISTRZ)
        if (member_array(this_player()->query_name(), CZARODZIEJE) == -1)
            if (member_array(this_player()->query_name(), RADNI) == -1)
                return 0;
        
    (MAIN)->przeglada_podania(this_player(),str);
        return 1;
}


void
umiesc_imie(string czyje,object kto,string gdzie)
{
    (MAIN)->przesun_podanie(kto,czyje,gdzie);
        return;
}

int
umiesc(string str)
{
    //if (this_player()->query_name() != BURMISTRZ)
        if (member_array(this_player()->query_name(), CZARODZIEJE) == -1)
            return 0;
        
    object kto=this_player();

        str=lower_case(str);

        if(!str)
        {
            notify_fail("Co i gdzie chcesz umie^sci^c?\n");
                return 0;
        }

    write("Podaj imi^e osoby, kt^orej podanie chcesz przenie^s^c: ");
    input_to("umiesc_imie",0,kto,str);
        return 1;
}

int
pomoc()
{
    if (/*this_player()->query_name() == BURMISTRZ ||*/ member_array(this_player()->query_name(), CZARODZIEJE) != -1)
    {
        write("Zarz^adzaniu podaniami s^lu^z^a nast^epuj^ace komendy:\n\n\t" +
        "przejrzyj - przegl^adanie poda^n w kategorii,\n\tprzeczytaj - " +
        "czytanie wybranego podania,\n\tzatwierd^x - zatwierdzenie wybranego"+
        " podania,\n\tumie^s^c - umieszczenie podania w wybranej kategorii," +
        "\n\tusu^n - usuni^ecie podania i umieszczenie go w archiwum.\n");
    
        return 1;
    }
    else
        if (member_array(this_player()->query_name(), RADNI) != -1)
        {
            write("Zarz^adzaniu podaniami s^lu^z^a te dwie komendy:\n\n\t" +
            "przejrzyj - przegl^adanie poda^n w kategorii,\n\tprzeczytaj - " +
            "czytanie wybranego podania.\n");
    
            return 1;
        }
        else
		{
			write("Sorry gregory, ale nie jeste� MG!\n");
            return 1;
		}
}

void
init()
{
    ::init();

    add_action(usun,"usu^n");
    add_action(przeczytaj,"przeczytaj");
    add_action(zatwierdz,"zatwierd^x");
    add_action(przejrzyj,"przejrzyj");
    add_action(umiesc,"umie^s^c");
    add_action(pomoc, "?podania");
    add_action(pomoc, "?podanie");
	add_action(pomoc,"?");
}