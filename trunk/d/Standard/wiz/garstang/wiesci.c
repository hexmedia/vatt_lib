//Lil, operacja wstepne Thanedd, dnia 8.11.2005
inherit "/std/room";

#include <stdproperties.h>
#define GAR "/d/Standard/wiz/garstang/"

create_room()
{
    set_short("Du�a sala");
    set_long("G��wnym elementem tej sali jest ogromna tablica zajmuj�ca " +
	"podwy�szenie na �rodku pomieszczenia. Od tablicy emanuuje jaka� " + 
	"pot�na energia, sprawiaj�ca, �e ka�da istota znajduj�ca si� " + 
	"w dowolnym miejscu mo�e w swym umy�le zobaczy� obraz tablicy " +
	"wraz z jej tre�ci�.\n");

    add_object(GAR+"wiesciboard");
    add_prop(ROOM_I_LIGHT,2);
    add_prop(ROOM_I_INSIDE, 1);
    add_exit(GAR+"korytarz1","p�noc");
}
