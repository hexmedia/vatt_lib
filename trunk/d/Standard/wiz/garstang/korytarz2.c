//Lil, operacja wstepne Thanedd, dnia 8.11.2005
inherit "/std/room";

#include <std.h>
#include <macros.h>
#include <stdproperties.h>
#define GAR "/d/Standard/wiz/garstang/"

int moze_przejsc();

void
create_room()
{
    set_short("Dalsza cz�� kamiennego Korytarza w Garstangu");
    set_long("Nagi, ciemny i ch�odny kamie� otacza ci� zewsz�d. Korytarz ci�gnie "+
        "si� jeszcze dalej na po�udnie. Wej�cia do komnat zarz�dzania "+
        "patrolami gwardii ca�ego �wiata znajduj� si� prawie z ka�dej strony." +
        "W zachodni� �cian� wkomponowane s� drewniane, masywne drzwi - "+
        "w tej chwili zamkni�te.\n");

    add_exit(GAR+"korytarz1.c",  ({"s","wg��b korytarza"}));
    add_exit(GAR+"podania.c",    ({"e","do szafy na zachodzie","z korytarza"}));
    add_exit(GAR+"admin_straza", ({"n","na p�noc do pomieszczenia zarz�dzania patrolami"}));
    add_exit(GAR+"korytarz_admin1.c", "w", &moze_przejsc());
    add_exit("/secure/npce",    ({"nw", "do pomieszczenia zarz�dzania npc'ami"}));
    add_exit("/d/Standard/wiz/players", "ne");
    //pok�j do zarz�dzania walk
    add_exit("/std/combat/combat_object", ({"d","do lochu walk"}));
}

nomask int
moze_przejsc()
{
    if(SECURITY->query_wiz_rank(TP->query_real_name()) >= WIZ_MAGE)
        return 0;

    write("Jaka� magiczna si�a nie pozawala Ci tam wej��.\n");
    return 1;
}
