/**
 * \file /d/Standard/wiz/garstang/pochwaly.c
 *
 * @author Krun
 * @date Stycze� 2008
 */

#pragma strict_types

#include <files.h>
#include <stdproperties.h>

inherit ROOM_OBJECT;

#include "dir.h"

object tab;
int pomoc();

void
create_room()
{
    set_short("Mortalis Laudationis");
    set_long("Na �rodku pomieszczenia stoi du�a tablica z wieloma zapiskami. " +
             "Co chwila wypisuj� si� na niej nowe pochwa�y od graczy.\n"+
             "Wpisz 'pomoc', by dowiedzie� si� czego� wi�cej.\n");
    PRAISE_OBJECT->move(this_object());
    add_exit(GAR+"bledy", ({"n","do Vox Populi"}));
    add_prop(ROOM_I_INSIDE, 1);
    setuid();
    seteuid(getuid());
}

void
init()
{
    ::init();
    add_action(pomoc, "?tablica");
}

int
pomoc()
{
    string str;

    str="\n---------------------POMOC-----------------------------------\n"+
        "Standardowy wpis na tablicy wygl�da tak:\n"+
        "1: /d/Standard/wiz/wizroom            pochwa�a Rantaur       1  10 VI 2006"+
        "\n\nPierwsza liczba to numer notki, nast�pnie jako tytu� jest podawany "+
        "obiekt, w kt�rym zosta�o wys�ane zg�oszenie, nast�pna kolumna "+
        "m�wi o charakterze zg�oszenia. DU�YMI literami s� pisane "+
        "zg�oszenia globalne, za� ma�ymi - w danym obiekcie.\n"+
        "Dalej mamy imi� zg�aszaj�cego, kolejny numerek to liczba "+
        "wierszy wyst�puj�cych w zg�oszeniu, a na samym ko�cu widnieje data "+
        "zg�oszenia.\n"+
        "-------------------------------------------------------------\n";
    write(str);
    return 1;
}
