//Vera, operacja wstepne Thanedd, dnia 8.11.2005
inherit "/std/room";

#pragma strict_types

#include <stdproperties.h>
#include <macros.h>

void
create_room()
{
    set_short("G��wny hall pa�acu Aretuzy");
    set_long("@@dlugi@@");

    add_object("/d/Standard/wiz/tablica");
    add_prop(ROOM_I_INSIDE,1);
//    add_prop(CONT_I_MAX_RZECZY, 3);
    add_sit("na ziemi","na kamiennej posadzce","z ziemi",0);
    add_sit("w lo^zy","w lo^zy idler^ow","z lo^zy idler^ow",0);
    add_exit("/d/Standard/wiz/biblioteka",({"e",
                       "do Akademii, w kierunku biblioteki"}));
    add_exit("/d/Standard/wiz/specjalny",({"nw","do Galerii Chwa�y"}));
    add_exit("/d/Standard/wiz/pierdoly",
              ({"sw","do pokoju lu�nych przemy�le�"}));
    add_exit("/d/Standard/wiz/sala_akademii",
                 ({"se","do Akademii, w kierunku jej g��wnej sali"}));
    add_exit("/d/Standard/wiz/placyk",({"w","na zewn�trz"}));
    add_exit("/d/Standard/wiz/korytarz1",({"s","na po�udnie w kierunku korytarza"}));
    //add_npc("/d/Standard/Redania/Rinde/npc/sekretarz", 1);
//    add_exit("/d/Standard/wiz/bledy", ({"ne", "do Pokoju P�aczu"}));
//    PRZESUNI�TE DO GARSTANGU :P

    //To jest po to, zeby sie wszystkie lokacje zaladowaly od razu :) Bo pozniej jest lag, jak jest
    //ta funkcja uzywana po raz pierwszy ;] poza tym, stra� si� nie �aduje bez tego i kilka innych rzeczy
    //WARNING JAK KOMU� TO NIE PASUJE TO NIECH WYMY�LI CO� LEPSZEGO ZAMIAST KOMENTOWA� I PSU� MUDA
      znajdz_sciezke("/d/Standard/wiz/wizroom","/d/Standard/wiz/specjalny");
}

string dlugi()
{
    return "Obszerny g��wny hall zdobi� liczne rze�by, freski i obrazy. "+
        "Na �rodku, tu� nad ziemi� unosi si� poka�na tablica - "+
        "zapewne za sprawk� magii. Na wschodzie jest kilka przej�� "+
        "do r�nych cz�ci Akademii, a od strony przeciwnej odbiegaj� "+
        "inne wyj�cia, prowadz�ce do kolejnych komnat. Drzwi wyj�ciowe "+
        "u�wiadczysz na zachodzie. " +
        "Na zewn^atrz jest <dzien>dzien</dzien><noc>noc</noc>.\n";
}

string query_pomoc()
{
    return "Wizroom, ot co.\n";
}