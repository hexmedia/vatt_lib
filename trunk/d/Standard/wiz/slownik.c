/**
 * \file /d/Standard/wiz/slownik.c
 *
 * Pomieszczenie obs�ugi s�ownika.
 *
 * @author  Krun (krun@vattghern.pl)
 * @date    27.4.2008
 * @version 1.0
 */

#pragma strict_types
#pragma no_clone
#pragma no_inherit
#pragma binary_save

#include <stdproperties.h>

inherit "/std/room.c";

void
create_room()
{
    set_short("W ogromnej maszynie - S�owniku Vatt'gherna.\n");

    set_long("Znajdujesz si� w maszynie wielkiego s�ownika Vatt'gherna.\n");

    add_prop(ROOM_I_LIGHT, 1);
    add_prop(ROOM_I_NO_CLEANUP, 1);
    add_prop(ROOM_I_INSIDE, 1);
    add_exit("biblioteka", ({"w", "na zach�d do biblioteki"}));
    add_exit("sala_akademii", ({"sw","na po�udniowy zach�d do Akademii"}));

}

string
query_pomoc()
{
    return "Chwilowo nie ma obs�ugi s�ownika driverowego, a s�ownik mudlibowy zosta� wycofany.\n";
}

