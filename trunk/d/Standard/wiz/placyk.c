//Lil, operacja wstepne Thanedd, dnia 8.11.2005
inherit "/std/room";

#include <macros.h>
#include <pogoda.h>
#include <colors.h>
#include <stdproperties.h>

#define GAR "/d/Standard/wiz/garstang/"
#define ARE "/d/Standard/wiz/"
string blabla();
string pogodynka();

create_room()
{
    set_short("Niewielki placyk w^sr^od ogrodu piwonii");
    set_long("Starannie wybrukowany placyk, gdzieniegdzie z "+
        "pozostawionymi miejscami na rosn^ace tu piwonie otaczaj^a "+
        "zewsz^ad mury pa^lacu Aretuzy. @@blabla@@\n");

    add_sit("na bruku","na bruku","z bruku",0);
    add_exit(GAR+"schody_out",({"p^o^lnoc","na schody, w kierunku Garstangu"}));
    add_exit(ARE+"specjalny",({"ne","na p^o^lnocny wsch^od, do Galerii Chwa^ly"}));
    add_exit(ARE+"wizroom",({"e","na wsch^od do g^l^ownego hallu pa^lacu Aretuzy"}));
    add_object("/d/Standard/items/meble/lawka_zielona_wygodna");
    dodaj_rzecz_niewyswietlana("zielona wygodna ^lawka", 1);

    add_item(({"po^swiat^e","niebieskaw^a po^swiat^e","pogod^e"}),
             "@@pogodynka@@");
}

string
sprawdz_pogode_dla(int x, int y)
{
    string str = "";
    object plan_pogody = POGODA_OBJECT->get_plan(x, y);

    str += " Zachmurzenie:   ";
    switch(plan_pogody->get_zachmurzenie())
    {
        case POG_ZACH_ZEROWE:       str += "zerowe";      break;
        case POG_ZACH_LEKKIE:       str += "lekkie";      break;
        case POG_ZACH_SREDNIE:      str += "^srednie";     break;
        case POG_ZACH_DUZE:         str += "du^ze";        break;
        case POG_ZACH_CALKOWITE:    str += "ca^lkowite";   break;
    }

    str += ",\tTemperatura: " + plan_pogody->get_temperatura() + ",\n";
    str += " Opady:          ";

    switch(plan_pogody->get_opady())
    {
        case POG_OP_ZEROWE:     str += "brak";                break;
        case POG_OP_D_L:        str += "lekki deszcz";        break;
        case POG_OP_D_S:        str += "^sredni deszcz";       break;
        case POG_OP_D_C:        str += "ci^e^zki deszcz";       break;
        case POG_OP_D_O:        str += "oberwanie chmury";    break;
        case POG_OP_S_L:        str += "lekki ^snieg";         break;
        case POG_OP_S_S:        str += "^sredni ^snieg";        break;
        case POG_OP_S_C:        str += "ci^e^zki ^snieg";        break;
        case POG_OP_G_L:        str += "lekki grad";          break;
        case POG_OP_G_S:        str += "^sredni grad";         break;
    }

    str += ",\t\tWiatry: ";
    switch(plan_pogody->get_wiatry())
    {
        case POG_WI_ZEROWE:     str += "brak";            break;
        case POG_WI_LEKKIE:     str += "lekkie";          break;
        case POG_WI_SREDNIE:    str += "^srednie";         break;
        case POG_WI_DUZE:       str += "du^ze";            break;
        case POG_WI_BDUZE:      str += "bardzo du^ze";     break;
    }

    int *wschod = localtime(plan_pogody->get_wschod());
    int *zachod = localtime(plan_pogody->get_zachod());

    str += ",\n Wsch^od s^lo^nca: " + wschod[2] + ":" + wschod[1] ;


    str += ",   Zach^od s^lo^nca:  " + zachod[2] + ":" + zachod[1] + "\n";

    return str;
}

string
pogodynka()
{
    //super bajer, nie? :P
    //napisałem dla treningu, Vera.

    string *tmp1=({}), *tmp2=({});
    string ccp;
    int i = 0;
    ccp = explode(read_file("/d/Standard/Redania/dir.h"), "//=== POGODA ===")[1];
    tmp1 = explode(ccp, "//")[1..];
    foreach(string s : tmp1)
    {
	tmp2 = explode(tmp1[i],"\t");
	write(set_color(COLOR_FG_GREEN) +explode(explode(s,"\n")[0], "#define")[0]+"\n" + clear_color());
	write(sprawdz_pogode_dla(atoi(tmp2[(sizeof(tmp2)-1)/2]), atoi(tmp2[sizeof(tmp2)-1]))+"\n");
	i++;
    }

    return "";
}

string blabla()
{
    string str;
    if(jest_rzecz_w_sublokacji(0,"zielona wygodna ^lawka"))
    str="W ^srodkowej cz^e^sci na bruku dostrzegasz niebieskaw^a po^swiat^e "+
        "emanuj^ac^a migotliwymi promieniami uk^ladaj^acymi si^e w kr^ag. "+
        "Przed ni^a stoi sobie zielona wygodna ^lawka.";
    else
    str="W ^srodkowej cz^e^sci na bruku dostrzegasz niebieskaw^a po^swiat^e "+
        "emanuj^ac^a migotliwymi promieniami uk^ladaj^acymi si^e w kr^ag.";
    return str;
}
