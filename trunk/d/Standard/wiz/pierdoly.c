//Lil, operacja wstepne Thanedd, dnia 8.11.2005
inherit "/std/room";

#include <stdproperties.h>

create_room()
{
    set_short("Pok�j z tablic� pe�n� lu�nych przemy�le�");
    set_long("Wszelkie wolne my�li, jawne zapiski, czy notatki tutaj "+
             "mog� by� zapisane na tablicy. S�u�y ona za swego rodzaju "+
	     "supe�ki na sznurku. By pami�ta� o czym�.\n");

    add_object("/d/Standard/wiz/pierdoly_tablica");

    add_prop(ROOM_I_LIGHT,2);
    add_prop(ROOM_I_INSIDE, 1);

    add_exit("/d/Standard/wiz/wizroom","ne");
}
