//Lil, operacja wstepne Thanedd, dnia 8.11.2005
#pragma save_binary
inherit "/std/room";

#include <stdproperties.h>
#include <mail.h>       // Dla #define READER_ID

#define MAILCHECKER "/secure/mail_checker"
#define MAILREADER "/secure/mail_reader"

create_room()
{
    set_short("Poczta");
    set_long("Malutki pokoiczek s�u�y w Akademii za urz�d pocztowy.\n");


    add_prop(ROOM_I_INSIDE, 1);

    add_exit("/d/Standard/wiz/sala_akademii",
              ({"n","na p�noc, do g��wnej sali Akademii"}));
    add_exit("/d/Standard/wiz/korytarz1", "sw");
}

init()
{
  object obj;
  ::init();

  if (present(READER_ID, this_player()))
    return 1;
  if ((obj = clone_object(MAILREADER)) != 0)
    {
        obj->move(this_player());
    }
}

leave_inv(object who, object where)
{
  object obj;

  if (who &&
      where &&
      where != this_object() &&
      (obj = present(READER_ID, who)) != 0)
    obj->remove_object();
  ::leave_inv(who, where);
}

query_mail(silent)
{
  int mail;
  string tekst;

  seteuid(getuid());
  mail = MAILCHECKER->query_mail();
  if (!mail)
    return 0;
  if (silent)
    return 1;
  tekst = "";
  if (mail == 2)
    tekst += "\n-------------------------------------------------------------";

  tekst += "\nCzeka na ciebie";

  if (mail == 2)
    tekst += " NOWA";

  if (mail == 3)
    tekst += " nieprzeczytana";

  tekst += " poczta w najblizszym urzedzie pocztowym.\n";

  if (mail == 2)
    tekst += "-------------------------------------------------------------\n";

  tekst += "\n\n";

  write(tekst);
  return 1;
}
