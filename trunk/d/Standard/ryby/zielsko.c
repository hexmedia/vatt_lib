/* 
 * autor Cairell
 * data   2009
 */
inherit "/std/ryby/ryba";

#include <stdproperties.h>

void
create_ryba()
{
    ustaw_nazwe("zielsko");

    dodaj_przym("gnij^acy", "gnij^ace");

    set_id_long("Niewielki wieche^c gnij^acych ro^slin trzyma si^e w "+
        "ca^lo^sci nie tylko dzi^eki temu, ^ze wodorosty s^a spl^atane, "+
        "lecz tak^ze dzi^eki oblepiaj^acej je sporej ilo^sci b^lota i "+
        "mu^lu od kt^orych bije charatkerystyczny zapach mieszaj^acy "+
        "si^e z i tak ju^z nieprzyjemn^a woni^a zgnilizny.\n");

    set_unid_long("Niewielki wieche^c gnij^acych ro^slin trzyma si^e w "+
        "ca^lo^sci nie tylko dzi^eki temu, ^ze wodorosty s^a spl^atane, "+
        "lecz tak^ze dzi^eki oblepiaj^acej je sporej ilo^sci b^lota i "+
        "mu^lu od kt^orych bije charatkerystyczny zapach mieszaj^acy "+
        "si^e z i tak ju^z nieprzyjemn^a woni^a zgnilizny.\n");

    set_min_weight(100);

    set_avg_weight(300);

    set_max_weight(500);

    set_mod_pory(({1.0, 1.0, 1.0, 1.0, 1.0}));

    set_przynety(({"ciasto", "robak", "rybka"}), ({"ciasto", "robak", 
        "rybka"}));

    set_walecznosc(({0.1, 0.1}));

    set_rodzaj_brania(({"trykanie"}));

    set_max_cena(0);

    set_rozpoznawalnosc(0);
    
    jak_pachnie="Gdy tylko podnosisz ub^locon^a k^epk^e ro^slin do nosa "+
        "uderza ci^e silny zapach mu^lu i butwiej^acych ro^slin.\n";
    jak_wacha_jak_sie_zachowuje=" w^acha ub^locon^a k^epk^e ro^slin.\n";

    set_alarm(itof(1800+random(1800)), 0.0, "rozpad");
}

string query_auto_load()
{
    return 0;
}

void rozpad()
{
     saybb("Wyschni^ety mu^l rozkrusza si^e, a nie utrzymywane ju^z "+
         "niczym ^lodygi ro^slin rozsypuj^a.\n");
     remove_object();
}
