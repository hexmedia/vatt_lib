/* 
 * autor Cairell
 * data   2009
 */
inherit "/std/ryby/ryba";

#include <stdproperties.h>

void
create_ryba()
{
    ustaw_nazwe("ryba");

    dodaj_przym("w^asaty", "w^asaci");
    dodaj_przym("p^laskog^lowy", "p^laskog^lowi");

    set_id_long("G^ladkiego, wrzecionowatego cia^la tej ryby nie pokrywaj^a "+
        "widoczne ^luski, a jedynie wydziela^jacy charakterystyczny zapach "+
        "^sluz. Niemal czarn^a na grzbiecie, a ja^sniejsz^a od spodu sk^or^e "+
        "od p^laskiego ^lba z szerokim pyskiem, szczyc^acego si^e par^a "+
        "okaza^lych w^as^ow, po niezwykle d^lugi ogon, znacz^a liczne plamy, "+
        "przebarwienia, a miejscami drobne blizny. Bez trudu mo^zna "+
        "odgadn^a^c, ^ze jest to kr^ol rzek i jezior, sum. D^lugowieczna "+
        "ryba zapewne wiele widzia^la szeroko rozstawionymi oczami, a wielki "+
        "pysk po^zar^l niezliczone ^zaby, ryby, czy raki. Jakoby najstarsze "+
        "potrafi^a nawet polowa^c na ptactwo wodne.\n");

    set_unid_long("G^ladkiego, wrzecionowatego cia^la tej ryby nie pokrywaj^a "+
        "widoczne ^luski, a jedynie wydziela^jacy charakterystyczny zapach "+
        "^sluz. Niemal czarn^a na grzbiecie, a ja^sniejsz^a od spodu sk^or^e "+
        "od p^laskiego ^lba z szerokim pyskiem, szczyc^acego si^e par^a "+
        "okaza^lych w^as^ow, po niezwykle d^lugi ogon, znacz^a liczne plamy, "+
        "przebarwienia, a miejscami drobne blizny.\n");

    set_min_weight(500);

    set_avg_weight(5000);

    set_max_weight(30000);

    set_mod_pory(({0.5, 0.1, 0.1, 0.5, 1.0}));

    set_przynety(({"robak", "rybka"}), ({"rybka"}));

    set_walecznosc(({0.25, 0.35}));

    set_rodzaj_brania(({"zgiecie"}));

    set_max_cena(500);

    set_rozpoznawalnosc(15);
}
