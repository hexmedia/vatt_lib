inherit "/std/ryby/ryba";

void
create_ryba()
{
    ustaw_nazwe("ryba");

    dodaj_przym("p�aski", "p�ascy");
    dodaj_przym("�uskowaty", "�uskowaci");

    set_unid_long("Ta brunatna ryba wydziela nieco nieprzyjemn� wo� mu�u, a"
	   +" jej brunatna barwa zdaje si� jeszcze bardziej potwierdza�"
	   +" skojarzenia z dnem jakiego� jeziorka. Wypuk�y brzuch i"
	   +" grzbiet zaopatrzone s� w silne, niemal czarne p�etwy.\n");

    set_id_long("Ten okaz leszcza wydziela nieco nieprzyjemn� wo� mu�u."
	   +" Dodatkowo brunatna barwa przypomina ci, i� ryba ta najcz�ciej"
	   +" bytuje w jeziorach i wolno p�yn�cych rzekach. Wypuk�y brzuch i"
	   +" grzbiet zaopatrzone s� w silne, niemal czarne p�etwy,"
	   +" u�atwiaj�ce jej wyszukiwanie pokarmu przy dnie. Niekt�re"
	   +" okazy potrafi� osi�ga� ponad �okie� d�ugo�ci.\n");

    set_min_weight(100);
    set_avg_weight(900);
    set_max_weight(3000);

    set_mod_pory(({1.0, 1.0, 1.0, 0.5, 0.1}));
    set_przynety(({"chleb", "robak"}), ({"chleb", "robak"}));
    set_walecznosc(({0.16, 0.1}));
    set_rodzaj_brania(({"trykanie", "zgiecie"}));
    set_max_cena(39);
    set_rozpoznawalnosc(22);
}
