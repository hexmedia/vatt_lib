/* 
 * autor Cairell
 * data   2009
 */

inherit "/std/ryby/ryba";

#include <stdproperties.h>

void
create_ryba()
{
    ustaw_nazwe("ryba");

    dodaj_przym("pod^lu^zny", "pod^lu^zni");
    dodaj_przym("c^etkowany", "c^etkowani");

    /* Ustawiamy longa zidentyfikowanej ryby */
    set_id_long("Pod^lu^zne cia^lo tej ryby pokrywaj^a drobne plamy w kilku "+
        "brunatnych i ciemnozielonych odcieniach. P^laski ^leb z szerokim "+
        "pyskiem wyr^o^znia si^e jednym, niezbyt d^lugim w^asem po ^srodku "+
        "brody, a^askie w przekroju cia^lo zako^nczone jest p^o^lokr^ag^l^a "+
        "p^letw^a do kt^orej biegn^a dwie wyj^atkowo d^lugie, grzbietowa i "+
        "brzuszna. W^as, kszta^lt oraz p^letwy piersiowe wyra^znie "+
        "wysuni^e przed boczne s^a bardzo charakterystyczne i zdradzaj^a, "+
        "i^z z pewno^sci^a jest to mi^etus, zwany ryb^a ch^lodu i nocy. "+
        "Ryba o upodobaniach ca^lkowicie odmiennych od wi^ekszo^sci, "+
        "najlepiej si^e czuj^aca w norze na dnie lodowatej wody, skryta przed "+
        "wszelkim ^swiatlem.\n");


    /* Ustawiamy longa niezidentyfikowanej ryby */
    set_unid_long("Pod^lu^zne cia^lo tej ryby pokrywaj^a drobne plamy w kilku "+
        "brunatnych i ciemnozielonych odcieniach. P^laski ^leb z szerokim "+
        "pyskiem wyr^o^znia si^e jednym, niezbyt d^lugim w^asem po ^srodku "+
        "brody, a^askie w przekroju cia^lo zako^nczone jest p^o^lokr^ag^l^a "+
        "p^letw^a do kt^orej biegn^a dwie wyj^atkowo d^lugie, grzbietowa i "+
        "brzuszna.\n");

    set_min_weight(200);

    set_avg_weight(500);

    set_max_weight(2000);

    set_mod_pory(({0.1, 0.0, 0.0, 0.1, 1.0}));

    set_przynety(({"robak", "rybka"}), ({"robak", "rybka"}));

    set_walecznosc(({0.05, 0.15}));

    set_rodzaj_brania(({"zgiecie"}));

    set_max_cena(40);

    set_rozpoznawalnosc(10);
}
