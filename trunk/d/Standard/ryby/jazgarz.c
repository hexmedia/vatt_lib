inherit "/std/ryby/ryba";

void
create_ryba()
{
    ustaw_nazwe("ryba");

    dodaj_przym("nakrapiany", "nakrapiani");
    dodaj_przym("zielonkawy", "zielonkawi");

    set_unid_long("Cia�o o zielonkawej barwie, lekko wygi�te"
            +" do g�ry, usiane jest licznymi plamkami o kolorze"
            +" ciemnobr�zowym oraz posiada charakterystyczn� lini� boczn�,"
            +" kt�ra u tej ryby jest bardzo wyra�nie zaznaczona."
            +" P�etwa grzbietowa rozpi�ta jest na ostrych"
            +" promieniach, dzi�ki czemu ryba wygl�da bardziej"
            +" drapie�nie.\n");


    set_id_long("Cia�o o zielonkawej barwie, lekko wygi�te do g�ry,"
            +" usiane jest licznymi plamkami o kolorze ciemnobr�zowym"
            +" oraz posiada charakterystyczn� lini� boczn�, kt�ra u tego"
            +" jazgarza jest bardzo wyra�nie zaznaczona. P�etwa"
            +" grzbietowa rozpi�ta jest na ostrych promieniach,"
            +" dzi�ki czemu ryba wygl�da bardziej drapie�nie. Jazgarz"
            +" nale�y do rodziny okoniowatych i charakteryzuje si�"
            +" ogromn� �ar�oczno�ci�, lecz pomimo tego osi�ga tylko"
            +" oko�o �okcia d�ugo�ci.\n");

    set_min_weight(10);
    set_avg_weight(200);
    set_max_weight(500);

    set_mod_pory(({0.5, 1.0, 1.0, 1.0, 0.5}));
    set_przynety(({"chleb", "robak"}), ({"chleb", "robak"}));
    set_walecznosc(({0.01, 0.03}));
    set_rodzaj_brania(({"trykanie", "trykanie"}));
    set_max_cena(15);
    set_rozpoznawalnosc(5);
}
