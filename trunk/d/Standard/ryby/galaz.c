/* 
 * autor Cairell
 * data   2009
 */
inherit "/std/ryby/ryba";

#include <stdproperties.h>

void
create_ryba()
{
    ustaw_nazwe("ga^l^a^x");

    random_przym(({"nadgni^la", "gnij^aca", "pokrzywiona", "powyginana", 
        "s^ekata", "poczernia^la"}), ({"niedu^za", "niewielka", "ma^la", 
        "ub^locona"}), 2);

    set_id_long("Niedu^zy patyk, oblepiony jeszcze resztkami wodorost^ow i "+
        "b^lota roztacza silny zapach mu^lu. Poczernia^le, pozbawione kory "+
        "drewno jest bardzo mocno nasi^ete wod^a. Po li^sciach nie zosta^l "+
        "ju^z nawet ^slad, jedynie kilka niewielkich s^ek^ow znaczy jego "+
        "powierzchni^e.\n");

    set_unid_long("Niedu^zy patyk, oblepiony jeszcze resztkami wodorost^ow i "+
        "b^lota roztacza silny zapach mu^lu. Poczernia^le, pozbawione kory "+
        "drewno jest bardzo mocno nasi^ete wod^a. Po li^sciach nie zosta^l "+
        "ju^z nawet ^slad, jedynie kilka niewielkich s^ek^ow znaczy jego "+
        "powierzchni^e.\n");

    set_min_weight(300);

    set_avg_weight(1500);

    set_max_weight(3000);

    set_mod_pory(({1.0, 1.0, 1.0, 1.0, 1.0}));

    set_przynety(({"ciasto", "robak", "rybka"}), ({"ciasto", "robak", 
        "rybka"}));

    set_walecznosc(({0.1, 0.1}));

    set_rodzaj_brania(({"trykanie"}));

    set_max_cena(0);

    set_rozpoznawalnosc(0);
}
