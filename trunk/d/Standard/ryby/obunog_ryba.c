
/**
 * Obunog - gdy jest ryba
 * Mozna go zlowic na wedke, ale potem zmienia sie w creature i gryzie;)
 *
 * Mo�na go wy�owi� w jeziorku przy zakonie. 
 *
 * autor Cairell
 * opisy Tabakista
 * data   2009
 */

inherit "/std/ryby/ryba";

#include <stdproperties.h>
#include <std.h>
#include <macros.h>
#include <stdproperties.h>
#include <pl.h>
#include "../dir.h"

int transformacja();

void
create_ryba()
{
    ustaw_nazwe("obun^og"); 

    set_id_long("To zbli^zone kszta^ltem do raka stworzenie znacznie przerasta "+
        "swego w^atpliwego kuzyna. W zasadzie obun^og ten, przerasta nawet "+
        "sporego bobra. Para d^lugich na prawie dwie p^edzi szczypiec wydaje "+
        "si^e bardziej smuk^la ni^z u raka, lecz niema^le rozmiary czyni^a "+
        "dra^znienie stworzenia niezbyt bezpiecznym zaj^eciem. Jego korpus "+
        "oraz segmentowany, zako^nczony szerokim wachlarzem odw^lok pokrywa "+
        "porowaty, poro^sni^ety glonami pancerz brunatnej, przechodz^acej "+
        "w ziele^n barwy. wyrastaj^ace z ^lba czu^lki poruszaj^a si^e "+
        "bezustannie, podobnie jak znajduj^ace si^e ni^zej, pod pozbawionymi "+
        "wyrazu, jednolicie czarnymi ^slepiami, szcz^eki. Ostre ^zuwaczki "+
        "l^sni^a, trudno powiedzie^c od wilgoci, czy jakiego^s rodzaju "+
        "^sliny. Po bokach pancernego korpusu wyrastaj^a paj^akowate nogi, "+
        "po pi^e^c z ka^zdej strony. Je tak^ze pokrywaj^a wodorosty i "+
        "mu^l.\n");

    set_unid_long("To zbli^zone kszta^ltem do raka stworzenie znacznie przerasta "+
        "swego w^atpliwego kuzyna. W zasadzie obun^og ten, przerasta nawet "+
        "sporego bobra. Para d^lugich na prawie dwie p^edzi szczypiec wydaje "+
        "si^e bardziej smuk^la ni^z u raka, lecz niema^le rozmiary czyni^a "+
        "dra^znienie stworzenia niezbyt bezpiecznym zaj^eciem. Jego korpus "+
        "oraz segmentowany, zako^nczony szerokim wachlarzem odw^lok pokrywa "+
        "porowaty, poro^sni^ety glonami pancerz brunatnej, przechodz^acej "+
        "w ziele^n barwy. wyrastaj^ace z ^lba czu^lki poruszaj^a si^e "+
        "bezustannie, podobnie jak znajduj^ace si^e ni^zej, pod pozbawionymi "+
        "wyrazu, jednolicie czarnymi ^slepiami, szcz^eki. Ostre ^zuwaczki "+
        "l^sni^a, trudno powiedzie^c od wilgoci, czy jakiego^s rodzaju "+
        "^sliny. Po bokach pancernego korpusu wyrastaj^a paj^akowate nogi, "+
        "po pi^e^c z ka^zdej strony. Je tak^ze pokrywaj^a wodorosty i "+
        "mu^l.\n");

    set_min_weight(1000);

    set_avg_weight(3000);

    set_max_weight(10000);

    set_mod_pory(({0.1, 0.01, 0.01, 0.1, 0.05}));

    set_przynety(({"rybka"}), ({"rybka"}));

    set_walecznosc(({0.25, 0.35}));

    set_rodzaj_brania(({"uderzenie"}));

    set_max_cena(0);

    set_rozpoznawalnosc(0);
}

void enter_env(object dest, object old)
{
    ::enter_env(dest,old);
    transformacja();
}

int transformacja()
{
    object x;
    x = TO;
    while (ENV(x) != 0)
    {
        x = ENV(x);
    }
    
    tell_roombb(x, capitalize(this_object()->short(PL_MIA))+
        " zrywa si^e z w^edki, upadaj^ac ci pod nogami. B^lyskawicznie "+
        "staje na paj^akowatych nogach i sycz^ac wznosi d^lugie "+
        "szczypce.\n", 0, ({x}));
    clone_object(REDANIA+"Zakon_okolice/Jeziorko/livingi/obunog_mob")->move(x);
    remove_object();
    return 1;
}
