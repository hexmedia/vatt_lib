inherit "/std/ryby/ryba";

#include <stdproperties.h>

void
create_ryba()
{
    ustaw_nazwe("ryba");    // No comments ;P

    dodaj_przym("p�askog�owy", "p�askog�owi");
    dodaj_przym("c�tkowany", "c�tkowani");

    /* Ustawiamy longa zidentyfikowanej ryby */
    set_id_long("Cia�o tej ryby jest bardzo wyd�u�one, o pe�nej linii bocznej,"
	  +" pokryte drobnymi, d�ugimi i twardymi �uskami. Szczupak jako drapie�nik"
	  +" posiada ma�e bardzo ostre i zakrzywione do �rodka paszczy z�by, kt�re"
	  +" s�u�� do odrywania kawa�k�w mi�sa. Potrafi dorasta� do naprawd� poka�nych"
	  +" rozmiar�w i osi�ga� nawet trzy �okcie d�ugo�ci. Uznawany jest tak�e za"
	  +" jedna ze smaczniejszych ryb jest cenionym okazem na wszelakich rybnych"
	  +" targach.\n");

    /* Ustawiamy longa niezidentyfikowanej ryby */
    set_unid_long("Cia�o tej ryby jest bardzo wyd�u�one, o pe�nej linii"
	   +" bocznej,  pokryte drobnymi, d�ugimi i twardymi �uskami."
	   +" Ma�e bardzo ostre i zakrzywione do �rodka paszczy z�by,"
	   +" mog� �wiadczy� o drapie�no�ci tej ryby.\n");

    /* Ustawiamy minimalna wage ryby */
    set_min_weight(300);

    /* Ustawiamy srednia wage ryby, wszystkie lzejsze od niej
     * beda zaliczac sie do ryb 'mlodych', powyzej do 'starych
     */
    set_avg_weight(3100);

    /* Ustawiamy maksymalna wage ryby */
    set_max_weight(8000);

    /* Ustawiamy modyfikatory pory dnia, inaczej jaki ulamek ryb obecnych
     * w lowisku bierze o danej porze dnia. Odpowiednio:
     * rano, poludnie, popoludnie, wieczor, noc
     */ 
    set_mod_pory(({0.5, 1.0, 0.5, 0.5, 0.1}));

    /* Ustawiamy przynety na jakie bierze ryba. Odpowiednio dla
     * ryby mlodszej i starszej :P
     * Dostepne wartosci: robak, rybka, ciasto
     */
    set_przynety(({"robak", "rybka"}), ({"rybka"}));

    /* Okreslamy walecznosc ryby, odpowiedno dla mlodszej i starszej.
     * Walecznosc powinna mniej wiecej zawierac sie w zakresie 0.01 - 0.35
     * Im wieksza walecznosc, tym dluzej trwa wyciaganie ryby.
     */
    set_walecznosc(({0.2, 0.3}));

    /* Okreslamy sposob w jaki bierze ryba.
     * Mozliwe wartosci: trykanie, zgiecie, uderzenie
     */
    set_rodzaj_brania(({"trykanie", "zgiecie"}));

    /* Ustalamy maksymalna cene dla ryby,
     * to jest cene za najwiekszy okaz danego gatunku
     */
    set_max_cena(55);

    /* Okreslany ile punktow skilla trzeba miec, aby
     * rozpoznac dana rybe 
     */
    set_rozpoznawalnosc(15);
}
