/*Jest to wiadomo��, kt�r� dostaje ka�dy apprentice (przez automat)
 */

#pragma save_binary

inherit "/std/scroll";

#include <stdproperties.h>
#include <pl.h>
#include <macros.h>

void
create_scroll()
{
    seteuid(getuid(this_object()));
    add_prop(OBJ_I_NO_DROP, "Pot�na magia zabrania ci tego.\n");
    ustaw_nazwe("zw�j");
    dodaj_przym("ma�y", "mali");
    dodaj_przym("srebrzysty", "srebrzy�ci");
    set_long("Jest to niewielki zapisany zw�j. Na pewno warto go przeczyta�.\n");
    set_autoload();
    set_file("/d/Standard/obj/WIZ_SCROLL_FILE");
    add_prop(WIZARD_ONLY, 1);
}

void
init()
{
    ::init();
    add_action("czytaj", "czytaj");
}

int
czytaj(string str)
{
    write("Komendy na Vatt'ghernie wyst�puja w formie dokonanej. Proponuj� u�y� "
        + "'przeczytaj'.\n");
    return 1;
}
