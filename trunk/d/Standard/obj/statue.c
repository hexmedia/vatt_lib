/**
 * \file /d/Standard/obj/statue.c
 *
 * Pomieszczenie w kt�rym umieszczane s� statuy graczy kt�rzy zerwali linka.
 * TODO:
 * Po wykryciu b��du z statuami tworz�cych si� postaci, trzeba b�dzie
 * troche to edytowa� - usun�� destructowanie byt�w astralnych.
 */

#pragma no_clone
#pragma no_inherit
#pragma save_binary
#pragma strict_types

inherit "/std/room";

#include <std.h>
#include <macros.h>
#include <stdproperties.h>

#define BSN(message) (break_string(message, 75) + "\n")
#define STATUE_ROOM  "_statue_room"
#define EDITOR_ID    "_editor_"

/*
 * These properties are used in move_living and therefore we save them
 * when a player goes linkdeath.
 */
static string *props_to_save = ({ LIVE_O_LAST_ROOM, LIVE_S_LAST_MOVE });

mapping places;

/*
 * Function name: create_room
 * Description  : Called to create the room.
 */
public nomask void
create_room()
{
    set_short("Ogromna komnata");
    set_long("Miejsce to wype�niaj� rze�by przedstawicieli "+
        "wszystkich ras, kt�re kiedykowliek chodzi�y po �wiecie. "+
        "Jest ich tu tyle, i� niemal musisz si� mi�dzy nimi "+
        "przeciska� a ich kolory i kszta�t niemal do z�udzenia "+
        "przypominaj� �ywe osobniki. Jakby kto� je zamrozi�.\n");

    add_exit("/d/Standard/wiz/garstang/korytarz_admin1", "p�noc", "@@may_leave@@");

    change_prop(ROOM_I_LIGHT, 100);
    places = ([]);
}

public nomask string
exits_description()
{
    return "P�nocn� �cian� zdobi� niewielkie, metalowe drzwi nie bardzo " +
        "pasuj�ce do otoczenia.\n";
}

/*
 * Function name: may_leave
 * Description  : To prevent linkdead players from leaving this room, we
 *                check that in the exit.
 * Returns      : int 1/0 - false if the player may leave.
 */
nomask public int
may_leave()
{
    if (!interactive(this_player()) ||
        SECURITY->query_wiz_level(TP->query_real_name()) < WIZ_MAGE)
    {
        write("Podchodzisz do drzwi - nagle twoje cia�o " +
            "przestaje by� Ci pos�uszne. Przez chwile nie "+
            "mo�esz si� rusza�, ale wszystko wraca do normy.\n");
        return 1;
    }

    return 0;
}

/*
 * Function name: remove_player
 * Description  : Can be called to remove a player from the room if it has
 *                been in here too long.
 * Arguments    : object ob - the player to remove.
 */
static nomask void
remove_player(object ob)
{
    if (ob && present(ob))
    {
        places = m_delete(places, ob);
        ob->remove_object();
    }
}

/*
 * Function name: die
 * Description  : This function is called from SECURITY when a player
 *                linkdies. We more him to the statue room and do some
 *                additional stuff.
 * Arguments    : object ob - the object that linkdies.
 */
public nomask void
die(object ob)
{
    int index, size, id_alarm;
    string env;
    object old_env = ENV(ob);

    /* If Armageddon is active when the player looses his/her link, we
     * make him quit instantly.
     */
    if (ARMAGEDDON->shutdown_active())
    {
        set_this_player(ob);
        ob->quit();

        return;
    }

    /* Just to be sure */
    if (!objectp(environment(ob)))
    {
        ob->quit();
        return;
    }

    //Je�li to byt astralny to od razu go wylogowywujemy...
    //bo sie kaszani logowanie je�li byty s� linkdeathni�te:P
    if(ob->query_race_name() == "byt astralny")
    {
        ob->quit();
        return;
    }

#ifdef 0
    if (member_array(ob->query_race_name(), m_indices(DEATH_MESSAGE)) < 0)
    {
        tell_room(environment(ob), QCTNAME(ob) + " goes link dead.\n",
            ({ ob }) );
    }
    else
    {
        tell_room(environment(ob),
            QCTNAME(ob) + DEATH_MESSAGE[ob->query_race_name()], ({ ob }) );
    }
#endif

    tell_roombb(environment(ob), QCIMIE(ob, PL_MIA) +
        " traci kontakt z rzeczywisto�ci�.\n", ({ ob }));

    env = file_name(environment(ob));

    /* save the props altered in move_living() */
    index = -1;
    size = sizeof(props_to_save);
    while(++index < size)
    {
        ob->add_prop(STATUE_ROOM + props_to_save[index],
        ob->query_prop_setting(props_to_save[index]));
    }

    tell_room(this_object(), "Z chmury dymu wy�ania si� statua " +
        QIMIE(ob, PL_DOP) + ".\n ");

    //Jesli si� nie uda przeniesc w spos�b zwyk�y przenosimy si�owo
    if(ob->move_living("M", this_object(), 1))
    {
        ob->move(this_object(), 1);
    }

#ifdef STATUE_WHEN_LINKDEAD
#ifdef OWN_STATUE
    //Jesli si� w �aden spos�b nie uda�o gracza wyrzuci� to musimy co� zrobi�,
    //coby statua nie zostawa�a
    if(old_env == ENV(ob))
    {
        ob->save_me();
        ob->quit();
        return;
    }
#endif OWN_STATUE
#endif STATUE_WHEN_LINKDEAD

    ob->save_me();
    id_alarm = set_alarm(1800.0, 0.0, "remove_player", ob); /* 30 minut */

    places += ([ ob : ({ env, id_alarm }) ]);

    if (objectp(interactive(ob)))
        ob->linkdie();
}

/*
 * Function name: revive
 * Description  : This function is called from the login object if a player
 *                revives from linkdeath. The player object is moved into
 *                his original room and some additional stuff is done.
 * Arguments    : object ob - the object that revives from linkdeath
 */
public nomask void
revive(object ob)
{
    int index, size, aid;
    object roomob;
    mixed room, *alarms, *args;

    find_player("krun")->catch_msg("Wyszukiwarka b��du ze statuami: Wywo�uje revive z statue rooma na " +
        ob->query_imie(find_player("krun"), PL_NAR) + ".\n");

    /* Check it just in case. We do _not_ want a runtime error here */
    if (pointerp(places[ob]))
    {
        room = places[ob][0];
        aid = places[ob][1];
    }
    else
    {
        room = 0;
        aid = 0;
    }

    if (objectp(room))
        roomob = room;
    else if(stringp(room))
    {
        roomob = find_object(room);
        if (!objectp(roomob))
        {
            catch(room->baba());
            roomob = find_object(room);
        }
    }
    if (!objectp(roomob))
    {
        ob->catch_msg("Nie uda�o si� zlokalizowa� miejsca, w kt�rym "+
            "zerwa�e� po��czenie. Spr�buj zalogowa� si� jeszcze raz.\n");
        roomob->quit();
    }

    tell_roombb(roomob, QCIMIE(ob, PL_MIA) +
        " odzyskuje kontakt z rzeczywisto�ci�.\n", ({ ob }));

    ob->move_living("M", roomob, 1);

    tell_roombb(this_object(), "Statua " + QIMIE(ob, PL_DOP) +
        " zamienia si� w chmur� dymu i znika.\n", ({ob}));

    /* restore the props altered in move_living() */
    index = -1;
    size = sizeof(props_to_save);
    while(++index < size)
    {
        ob->add_prop(props_to_save[index],
        ob->query_prop_setting(STATUE_ROOM + props_to_save[index]));
        ob->remove_prop(STATUE_ROOM + props_to_save[index]);
    }

    remove_alarm(aid);
    places = m_delete(places, ob);
}

/*
 * Function name: shutdown_activated
 * Description  : When shutdown is activated, this function is called to
 *                deal with the people who are linkdead. They are
 *                destructed.
 */
public nomask void
shutdown_activated()
{
    object *players;
    int     index;
    int     size;

    /* It may only be called from ARMAGEDDON. */
    if (!CALL_BY(ARMAGEDDON))
        return;

    players = m_indices(places);
    index = -1;
    size = sizeof(players);

    /* We force all players that are linkdead to quit. */
    while(++index < size)
    {
        if (objectp(players[index]) && present(players[index]))
        {
            set_this_player(players[index]);
            players[index]->quit();
        }
    }
}

/**
 * Blokujemy, �eby nie ka�dy m�g� sobie spokojnie wej��
 * do tego pomieszczenia.
 */
public nomask int
prevent_enter(object ob)
{
    if(SECURITY->query_wiz_rank(ob->query_real_name()) >= WIZ_MAGE ||
        ob->query_linkdead() || calling_function(-3) == "actual_linkdeath" ||
        member_array(TO, all_environment(ob)) != -1)
    {
        return 0;
    }

    write("Jaka� magiczna si�a odsy�a Ci� spowrotem, sk�d chcia�e� tu wej��.\n");
    return 1;
}
