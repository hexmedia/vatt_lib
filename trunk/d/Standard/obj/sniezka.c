/**
 * Oto �nie�ka!! Ma�a rzecz a cieszy. W zim� na zewn�trz gdy CZY_JEST_SNIEG mo�na j� ulepi� i do boju!
 * Vera. Po �wi�tach 2010.
 *
 */

inherit "/std/object.c";

#include <object_types.h>
// #include <materialy.h>
#include <macros.h>
#include <stdproperties.h>
#include <pl.h>
#define ZASNIEZENIE "_zasniezenie"

void
throw_effect(object cel) //to si� wywo�uje z soula rzucania po rzucie
{
    if(interactive(cel))
    {
        //dodajemy zasnie�enie
        cel->add_prop(ZASNIEZENIE, cel->query_prop(ZASNIEZENIE) + 1);
        
        object *efekty_zasniezenia = filter(all_inventory(cel), &->jestem_efektem_zasniezenia());
        
        if(cel->query_prop(ZASNIEZENIE) > 2 && !sizeof(efekty_zasniezenia))
        {
            object x = clone_object("/std/efekty/zasniezenie.c");
            x->start_event_sniegowy(cel);
            x->move(cel, 1);
        }

    }
    
    set_alarm(1.0, 0.0, "remove_object");
}

void
topniejemy()
{
    if(interactive(ENV(TO)) && !random(4)) //co jaki� czas wysy�amy w�a�cicielowi �nie�ki komunikat
        ENV(TO)->catch_msg("^Snie^zka b^ed^aca w twoim ekwipunku topi si^e.\n");
        
    remove_object();
}

create_object()
{
    ustaw_nazwe("�nie�ka");
    set_long("^Snie^zka mie^sci si^e w d^loni stanowi^ac idealny pocisk na niewielkie odleg^lo^sci. "+
             "^Snieg jest czysty i bia^ly, tak ^ze pocisk trzeba by kilkakrotnie obr^oci^c w d^loniach by "+
             "dostrzec jakie^s ^zdziebe^lko trawy, czy drobin^e igliwia. Pigu^la zgnieciona zosta^la na "+
             "tyle silnie, ^ze nie powinna rozlecie^c si^e w locie nim osi^agnie sw^oj cel.\n");
    //materia� woda? hmm
    //ustaw_material(MATERIALY_DR_BUK, 100);
    add_prop(OBJ_I_WEIGHT, 199+random(200));
    add_prop(OBJ_I_VOLUME, 340+random(300));
    remove_prop(OBJ_I_VALUE);
    
    
//     add_prop(WEAPON_I_LEGAL_WIELD, 1);
    //sniezka sama znika po czasie
    set_alarm(15.0+itof(random(30)), 0.0, topniejemy);
    
}

int query_sniezka()
{
    return 1;
}
/*
void
init()
{
    ::init();
    add_action(strzyzenie,  "ostrzy�");
    add_action(strzyzenie,  "zg�l");
    add_action(strzyzenie,  "og�l");
    add_action(skladanie,   "z��");
    add_action(rozkladanie, "roz��");
}

int
strzyzenie(string str)
{
    notify_fail(capitalize(query_verb())+" co ?\n");

    if(!str)
        return 0;

    if(!this_object()->query_wielded())
    {
        notify_fail("Musisz wpierw chwyci� brzytw�, aby to zrobi�.\n");
        return 0;
    }

    if(!this_object()->query_prop(BRZYTWA_ROZLOZONA))
    {
        NF("Jak chcesz si� ogoli� z�o�on� brzytw�?\n");
        return 0;
    }

    if(TP->query_gender()==1)
    {
       write("Hmm..ciekawe co mog�aby� sobie ostrzyc?\n");
       return 1;
    }
    
    if(zniszczona())
    {
        NF("Ta brzytwa jest ju� kompletnie zniszczona i nie nadaje si� do niczego.\n");
        return 0;
    }

    sscanf("%s",str);

    if(str ~= "brod�")
    {
        if(TP->query_dlugosc_brody() < 1.0)
        {
            write("Przecie� nie masz brody.\n");
            return 1;
        }
        niszczeje(); //u�ywamy obiektu, niszczy si� wi�c.
        write("Wprawnymi ruchami rozpoczynasz strzy�enie sobie brody "+
                this_object()->query_short(PL_NAR)+".\n");
        saybb(QCIMIE(this_player(), PL_MIA)+" wprawnymi ruchami rozpoczyna strzyc "+
                "sobie brod� "+this_object()->query_short(PL_NAR)+".\n");
        clone_object("/std/brzytwa_paralize/broda")->move(this_player());
        return 1;
    }

    if(str~="w�sy")
    {
        if(TP->query_dlugosc_wasow()*10.0 < 1.0)
        {
            write("Przecie� nie masz w�s�w.\n");
            return 1;
        }
        niszczeje(); //u�ywamy obiektu, niszczy si� wi�c.
        write("Wprawnymi ruchami rozpoczynasz strzy�enie sobie w�s�w "+
            this_object()->query_short(PL_NAR)+".\n");
        saybb(QCIMIE(this_player(), PL_MIA)+" wprawnymi ruchami rozpoczyna strzyc "+
                "sobie w�sy "+this_object()->query_short(PL_NAR)+".\n");
        clone_object("/std/brzytwa_paralize/wasy")->move(this_player());
        return 1;
    }
    if(str~="si�" || str~="brod� i w�sy" || str~="w�sy i brod�" || str=="zarost")
    {
        if(TP->query_dlugosc_wasow()*10.0 < 0.7 && TP->query_dlugosc_brody() < 0.7)
        {
            write("Przecie� nie masz zarostu.\n");
            return 1;
        }
        niszczeje(); //u�ywamy obiektu, niszczy si� wi�c.
        write("Wprawnymi ruchami rozpoczynasz si� strzyc "+
                this_object()->query_short(PL_NAR)+".\n");
        saybb(QCIMIE(this_player(), PL_MIA)+" wprawnymi ruchami rozpoczyna si� strzyc "+
                this_object()->query_short(PL_NAR)+".\n");
        clone_object("/std/brzytwa_paralize/zarost")->move(this_player());
        return 1;
    }
}

int
skladanie(string str)
{
    object brzytfa;

    notify_fail("Z�� co ?\n");

    if (!str) return 0;

    if (!parse_command(str, this_player(), "%o:" + PL_BIE, brzytfa))
        return 0;

    if (!brzytfa) return 0;

    if (brzytfa != this_object())
        return 0;

    if(!brzytfa->query_prop(BRZYTWA_ROZLOZONA))
    {
        write(capitalize(brzytfa->short(PL_MIA))+" ju� jest z�o�ona.\n");
        return 1;
    }
    niszczeje(); //u�ywamy obiektu, niszczy si� wi�c.
    write("Sk�adasz "+brzytfa->short(PL_BIE)+".\n");
    saybb(QCIMIE(this_player(),PL_MIA)+" sk�ada "+brzytfa->short(PL_BIE)+".\n");
    brzytfa->remove_prop(BRZYTWA_ROZLOZONA);
    brzytfa->remove_adj("roz�o�ony");
    brzytfa->dodaj_przym("rozk�adany","rozk�adani");
    brzytfa->odmien_short();
    return 1;
}

int
rozkladanie(string str)
{
    object brzytfa;

    notify_fail("Roz�� co ?\n");

    if (!str) return 0;

    if (!parse_command(str, this_player(), "%o:" + PL_BIE, brzytfa))
        return 0;

    if (!brzytfa) return 0;

    if (brzytfa != this_object())
        return 0;

    if(!brzytfa->query_wielded())
    {
        write("Wpierw musisz j� chwyci�.\n");
        return 1;
    }

    if(brzytfa->query_prop(BRZYTWA_ROZLOZONA))
    {
        write(capitalize(brzytfa->short(PL_MIA))+" ju� jest roz�o�ona.\n");
        return 1;
    }
    niszczeje(); //u�ywamy obiektu, niszczy si� wi�c.
    write("Rozk�adasz "+brzytfa->short(PL_BIE)+".\n");
    saybb(QCIMIE(this_player(),PL_MIA)+" rozk�ada "+brzytfa->short(PL_BIE)+".\n");
    brzytfa->add_prop(BRZYTWA_ROZLOZONA, 1);
    brzytfa->remove_adj("rozk�adany");
    brzytfa->dodaj_przym("roz�o�ony","roz�o�eni");
    brzytfa->odmien_short();
    return 1;
}*/

string
query_pomoc()
{
    return "Oto ^snie^zka. Niezb^edny atrybut zimowych wojen. Nie wahaj si^e - rzu^c ni^a w kogo^s!\n";
}
