/*
 * Copyright (c) 1991 Chalmers Computer Society
 *
 * This code may not be copied or used without the written permission
 * from Chalmers Computer Society.
 */

#pragma save_binary

inherit "/std/scroll";
#include <stdproperties.h>
#include <pl.h>
#include "/config/sys/local.h"

create_scroll()
{
    seteuid(getuid(this_object()));
    add_prop(OBJ_I_NO_DROP, "Nie wyrzucaj tej kartki, gdy� zawiera cenne " +
	"informacje!\n");
    ustaw_nazwe("kartka");
    dodaj_przym("bia�y", "biali");
    set_long("Jest to bia�a kartka, zapisana g�stym ma�ym druczkiem. Na "+
        "samej g�rze widnieje du�y, czerwony napis 'KONIECZNIE " +
        "PRZECZYTAJ'.\n");
    set_autoload();
    set_file(APPRENTICE_SCROLL_FILE);
}

void
init()
{
    ::init();
    add_action("czytaj", "czytaj");
}

int
czytaj(string str)
{
    write("Komendy na Vatt'ghernie wyst�puj� w formie dokonanej. Proponuj� u�y� "
        + "'przeczytaj'.\n");
    return 1;
}
