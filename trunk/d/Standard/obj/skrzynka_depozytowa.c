/**
 * \file /lib/depozyt.c
 *
 * Wszelkiego rodzaju depozyty zostawiane w bankach i nie tylko.
 *  Poprawki po Krunie: zmiana systemu liczenia obj�to�ci i inne - Vera
 *
 * @author Krun
 * @date 10 Stycze� 2008
 */

inherit "/std/receptacle.c";

#include <macros.h>
#include <stdproperties.h>

int     rodzaj_skrzynki,
        rozmiar_skrzynki,
        alarm_prv;
string  wlasciciel_skrzynki,
        unikalny_kod,
        depozyt;

void create_receptalce()
{
    set_open(0);
    add_prop(CONT_I_RIGID, 1); //skrzynia jest przecie� sztywna!
}

public void ustaw_rodzaj_skrzynki(int r)    { rodzaj_skrzynki = r; }
public int query_rodzaj_skrzynki()          { return rodzaj_skrzynki; }
public void ustaw_wlasciciela(string w)     { wlasciciel_skrzynki = w; }
public string query_wlasciciel()            { return wlasciciel_skrzynki; }
public void ustaw_rozmiar(int r)            { rozmiar_skrzynki = r; }
public int query_rozmiar()                  { return rozmiar_skrzynki; }
public void ustaw_depozyt(mixed d)          { depozyt = (objectp(d) ? MASTER_OB(d) : (stringp(d) ? d : 0)); }
public string query_depozyt()               { return depozyt; }
public void ustaw_unikalny_kod_skrzyni(string k) { unikalny_kod = k; }
public string query_unikalny_kod_strzyni()  { return unikalny_kod; }

public int
prevent_enter(object ob)
{
    int suma_obj = 0;

    foreach(object x : all_inventory(TO))
        suma_obj += x->query_prop(OBJ_I_VOLUME);

    //Maks ju� jest w �rodku nic wi�cej nie wejdzie.
    //if(sizeof(all_inventory(this_object())) >= rozmiar_skrzynki)
    if(suma_obj > rozmiar_skrzynki)
    {
        remove_alarm(alarm_prv);
        alarm_prv = set_alarm(0.1, 0.0, &write(capitalize(short(TP, PL_MIA)) + " nie zmie�ci ju� nic wi�cej.\n"));
        return 1;
    }

    return 0;
}

public string query_skrzynka_depozytowa_auto_load()
{
    return "#SK$DEP#" + wlasciciel_skrzynki + "$" + rozmiar_skrzynki + "$" + rodzaj_skrzynki +
        "$" + depozyt + "$" + unikalny_kod + "#SK$DEP#";
}

public string init_skrzynka_depozytowa_arg(string arg)
{
    string pre, post;

    if(sscanf(arg, "%s#SK$DEP#%s$%d$%d$%d$%s#SK$DEP#%s", pre, wlasciciel_skrzynki, rozmiar_skrzynki,
        rodzaj_skrzynki, depozyt, unikalny_kod, post) != 7)
    {
        return arg;
    }

    return pre + post;
}

public string query_auto_load()
{
    return ::query_auto_load() + query_skrzynka_depozytowa_auto_load() + query_opis_auto_load();
}

public string init_arg(string arg)
{
    return init_opis_arg(init_skrzynka_depozytowa_arg(::init_arg(arg)));
}

string
query_pomoc()
{
    if(ENV(TP)->query_tu_jest_depozyt())
        return "Jest to skrzynka depozytowa, pomoc do tego uzyskasz "+
        "w pomocy do pomieszczenia, w kt�rym si� znajdujesz - wpisz '?'.\n";
    else
        return "Hmm... Wygl�da na zwyk�� skrzynk�.\n";
}
