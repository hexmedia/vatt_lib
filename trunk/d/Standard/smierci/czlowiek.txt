Z otch^lani niezwyk^lego, g^l^ebokiego snu budz^a ci^e powoli twoje zmys^ly, 
przekazuj^ac ci doznania kt^orych nie jeste^s w stanie zrozumie^c. Jakby 
wydobywaj^ac si^e z czarnej przepa^sci, powoli dochodzisz do siebie. 
Otwierasz oczy... Nad tob^a rozci^aga si^e jasne, lazurowe niebo. Uspokajasz 
si^e na chwil^e.

11

Nagle dociera do ciebie, co wyrwa^lo ci^e z letargu. Co wype^lnia ci nozdrza, 
dra^zni^ac zmys^ly. Wo^n zakrzep^lej krwi. Wo^n starych ran. Wo^n ^smierci.

7

Z okrzykiem przera^zenia zrywasz si^e, ogl^adaj^ac si^e dooko^la siebie.

5

Otacza ci^e pole bitwy, jakiego w swoim ^zyciu nie widzia^l[e/a]^s. Stratowana 
ziemia pod twoimi nogami jest czarna od krwi. Wok^o^l ciebie w b^locie le^z^a 
martwi, w^sr^od po^lamanych pancerzy, strzaskanych ostrzy, i porzuconych lanc. 
Podnosisz wzrok, i widzisz rozciag^aj^ace si^e po horyzont pole masakry. 
Chmury much gromadz^a si^e wok^o^l ran i oczu le^z^acych...

15

Zbiera ci sie na md^lo^sci.

4

Zapach ^smierci nasila si^e. Machasz r^ek^a, przeganiaj^ac g^lo^sne, 
natarczywe muchy.

5

'Gdzie ja jestem...?' - szepczesz, boj^ac si^e naruszy^c ca^lun ^smierci 
otaczaj^acy ci^e.

5

Odpowiada ci natarczywe brz^eczenie much.

4

Rozgladaj^ac si^e, powoli ruszasz przed siebie, omijaj^ac zabitych, 
odwracaj^ac wzrok od okropno^sci otaczaj^acych ci^e z ka^zdej strony...

7

'Pom^o^z...' - cichy szept rozlega si^e, przysi[^ag^l/^eg^la]by^s, znik^ad. 
Odwracasz si^e, zmuszaj^ac wzrok do odnalezienia...

6

... kobiety, opartej o ^scierwo zwierz^ecia, kt^ore mog^loby by^c koniem, 
gdyby nie pokryta zielonymi ^luskami sk^ora... Prze^lykasz ^slin^e, 
podchodz^ac bli^zej i przygladaj^ac si^e rannej. Spod burzy jasnych, 
platynowych wr^ecz w^los^ow splamionych i sko^ltunionych zakrzep^l^a krwi^a 
wpatruj^a si^e w ciebie szmaragdowe oczy, pe^lne b^olu i na nowo rozbudzonej 
nadziei...

16

'Pom^o^z... mi.' - znowu ten szept, pe^len rozpaczy i b^lagania. Podchodzisz 
bli^zej, i w tej samej chwili zauwa^zasz czarn^a plam^e zakrzep^lej krwi na 
boku kobiety... spod rozdartej szaty wystaje z^lamany drzewiec w^l^oczni. Jej 
drugi koniec, z grotem, opiera si^e o ziemi^e, wystaj^ac z plec^ow tu^z 
poni^zej... dw^och d^lugopi^orych skrzyde^l, wyrastaj^acych z plec^ow kobiety. 
Jedno z nich jest z^lo^zone, drugie, rozrzucone w bok, ukazuje wy^lamane pi^ora 
i zakrwawiony puch.

17

Przygryzasz warg^e, niepewn[y/a] co powiedzie^c.

4

Jakby czytaj^ac w twoich my^slach, kobieta... nie, anio^l, dotyka koniuszkami 
palc^ow u^lomka w^l^oczni tkwi^acego w jej boku, i cicho j^eczy. W jej oczach 
widzisz rezygnacj^e...

8

Przykl^ekasz przy niej, pew[ien/na], ^ze cokolwiek zrobisz tylko zwi^ekszy 
jej cierpienie.

4

Anio^l unosi g^low^e, patrz^ac si^e na ciebie jeszcze raz... Po jej policzkach 
sp^lywaj^a dwie kryszta^lowe ^lzy.

4

'Nie ma ju^z... nieba...' - szepce do ciebie, 'Nie ma... ju^z...' - dr^zenie 
przebiega przez z^lo^zone skrzyd^lo. Szmaragdowe oczy rozszerzaj^a si^e, by 
po chwili zgasn^a^c na zawsze.","9","Nagle nachodzi ci^e niewyt^lumaczalna 
ochota, aby wy^c z ^zalu. Gniew zalewa ci^e niczym fala lawy niszcz^acej 
wszystko na swojej drodze... Czujesz jak co^s, niczym r^eka ciemno^sci, 
podnosi ci^e, i ciska w pustk^e.

12