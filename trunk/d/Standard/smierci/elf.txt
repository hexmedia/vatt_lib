Stoisz w^sr^od wysokich drzew, na pokrytym zielonym mchem runie le^snym. Szara 
mg^la spowijaj^aca ciemne pnie najbli^zszych ^swierk^ow niesie ze sob^a won 
kory i bzu...

7

Rozgl^adasz si^e ze zdziwieniem.

4

'Gdzie ja jestem?' - tw^oj szept unosi si^e w^sr^od pasemek mg^ly, nikn^ac w 
szarej ^scianie.

6

Stawiasz pierwszy niepewny krok, potem drugi, wyciagaj^ac r^ek^e w kierunku 
najbli^zszego drzewa. Ciemna, chropowata kora pod twoj^a d^loni^a sprawia 
wra^zenie dziwnie ch^lodnej... obcej. Zamykasz oczy, skupiaj^ac si^e... i 
wspomnienia wracaj^a jak lodowate fale zimowego przyp^lywu.

10

'Umar^l[e/a]m...' - tw^oj szept niesie ze sob^a rezygnacj^e, zalewa ci^e 
nieprzeparte uczucie smutku. 'Umar^l[e/a]m.'

6

Najdelikatniejsze ugryzienie strachu burzy ci krew. 'Ale gdzie ja...' Daleko, 
w^sr^od mg^ly co^s si^e poruszy^lo. Delikatne z^lote promienie przebijaj^a 
szar^a scian^e, ta^ncz^ac w^sr^od czarnych drzew.

9

T^esknota silniejsza od ^smiertelnego zrozumienia ogarnia ci^e jak b^lysk gromu, 
wype^lnia ci serce cicha piesnia. Zrywasz si^e do biegu, a w uszach szumi ci 
melodia r^ownie stara jak pierwsze elfy.

8

Konar, skr^econy jak ranny w^a^z, ^lapie ci^e za nog^e. Z cichym okrzykiem 
padasz, czuj^ac jak r^ece wie^zn^a ci w wilgotnym mchu. Daleko, w^sr^od 
szarej mg^ly oddala si^e z^lota po^swiata...

8

'Nie...' - tw^oj j^ek przeradza sie w okrzyk sprzeciwu. Wyrywasz si^e, 
zataczaj^ac. Biegniesz w^sr^od drzew.

5

Las g^estnieje. Niskie konary ^smagaj^a ci^e po ramionach, ostre ig^ly 
targaj^a w^losy, stopy wi^a^ze zapadaj^acy si^e mech. Las z obcego staje 
si^e wrogi... przysi[^ag^l/^eg^la]by^s, ^ze stara si^e ciebie zatrzyma^c.

7

Zaciskasz z^eby, ignoruj^ac b^ol rozci^etego policzka i biegniesz dalej, 
uskakuj^ac przed konarami, przemykaj^ac nad zdradliwymi jamami, 
wpatrzon[y/a] w z^lote ^swiat^lo.

7

Wpadasz na polan^e, przegrodzon^a powalonym drzewem. Jego konary toczy czarna 
zgnilizna. Na drugim ko^ncu polany, otoczona z^lot^a po^swiat^a, smuk^la 
sylwetka wkracza mi^edzy drzewa. Tam gdzie st^apa, szara mg^la rozwiewa 
si^e, niknie.

10

'Pani!' T^esknota w twoim sercu jest wr^ecz namacalna. Biegniesz, 
przeskakuj^ac drzewo. 'Le^sna Pani!'","5","Sylwetka zatrzymuje si^e, i 
odwraca. Oczy koloru migda^l^ow wpatruj^a si^e w ciebie spod delikatnych 
brwi i fali jasnych w^los^ow. L^sni z^lote swiat^lo.

7

Obdart[y/a], zakrwawion[y/a], padasz na kolana przed Ni^a. Szczup^la, wysoka, 
nieziemska kobieta jest esencj^a elfiej ^swiadomo^sci - czujesz jej aur^e 
wszystkimi swoimi zmys^lami, pie^s^n jej istoty wype^lnia ci dusz^e.

8

'Pani... Pani z^lotego blasku... Zabierz mnie do Le^snej Krainy! Otw^orz 
bram^e... Nie chc^e... Nie mog^e tu zosta^c!' ^lkasz, wpatrzon[y/a] w 
Jej twarz.

6

Oczy wpatrzone w ciebie s^a pe^lne zrozumienia. Pe^lne wsp^o^lczucia. Smuk^la 
d^lo^n dotyka twojego policzka, przesuwa si^e wzd^lu^z rozdartej sk^ory 
delikatniej ni^z ptasi puch. Czujesz ciep^lo... B^ol znika.

9

'Dlaczego tu jeste^s?' ^spiew niczym szelest p^latkow r^o^z delikatnie 
rozlega si^e w^sr^od twoich my^sli. 'Tw^oj czas jeszcze nie nadszed^l, 
dziecko... Wracaj. Wracaj, i ciesz si^e ^zyciem.'

8

Pani u^smiecha si^e do ciebie, a jej d^lo^n dotyka twojego czo^la. Otacza 
ci^e z^lote ^swiat^lo, zamglony las powoli rozwiewa si^e. Ostatnie co widzisz 
to Jej twarz, wpatrzona gdzie^s w czarny mrok. Jej usta, zacisni^ete w gniewie.

11