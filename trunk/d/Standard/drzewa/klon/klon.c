inherit "/std/drzewo";

#include <materialy.h>
#include <mudtime.h>
#include <object_types.h>
#include <pl.h>
#include <stdproperties.h>

void create_tree()
{
	ustaw_nazwe(({"klon", "klonu", "klonowi", "klon", "klonem", "klonie"}),
		    ({"klony", "klon闚", "klonom", "klony", "klonami", "klonach"}),
		    PL_MESKI_NZYW);

	random_przym("kszta速ny:kszta速ni||powykr璚any:powykr璚ani||roz這篡sty:roz這篡軼i||"
				+ "omsza造:omszali", 1);

	set_long("@@dlugi@@");

	set_type(O_DREWNO);
	ustaw_material(MATERIALY_DR_KLON, 100);

	set_gestosc(0.65);
	add_prop(OBJ_I_WEIGHT, random(1450000)+50000);

	set_sciezka_galezi("/d/Standard/drzewa/klon/galaz.c");
	set_sciezka_klody("/d/Standard/drzewa/klon/kloda.c");
	set_cena(3.85);
		    
	setuid();
	seteuid(getuid());
}

string dlugi()
{
	string opis = "";

	if(query_mlode())
	{
		opis += "Od niezbyt grubego pnia odrastaj� proste ga喚zie,"
				+" kt鏎e z uporem pn� si� w g鏎�.";
	}
	else
	{
		opis += "Gruby pie� stanowi solidn� podstaw� dla korony drzewa,"
				+" kt鏎a wydaje si� by� na� nasadzona niczym kula.";
	}

	if(find_room()->pora_roku() == MT_WIOSNA)
	{
		opis += " Drzewo wygl康a nieco ponuro, cho� pozytywnego akcentu dodaj�"
				+" mu niewielkie, jasnozielone p帷zki oraz miniaturowe listki, kt鏎e juz"
				+" zd捫y造 si� z nich rozwin望.";
	}

	if(find_room()->pora_roku() == MT_LATO)
	{
		opis += " Co jaki� czas, delikatnie si� ko造sz帷, drzewo szumi wspania造mi, du篡mi li嗆mi.";
	}

	if(find_room()->pora_roku() == MT_JESIEN)
	{
		opis += " Du瞠 li軼ie klonu przybra造 niezliczon� ilo嗆 jesiennych barw"
				+" poczynaj帷 od 鄴販i, poprzez czerwie�, a na br您ie ko鎍z帷.";
	}

        if(find_room()->pora_roku() == MT_ZIMA)
        {
                opis += " Drzewo jest starannie opruszone 郾iegiem, dzi瘯i czemu wygl康a"
				+" nieco mniej posepnie, mo積aby nawet rzec, i� 闚 郾ieg czyni je bardziej"
				+" malowniczym.";
        }

	opis += "\n";

	return opis;

}
