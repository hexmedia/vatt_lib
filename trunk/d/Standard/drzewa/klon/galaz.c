inherit "/std/drzewo/galaz";

#include <materialy.h>
#include <mudtime.h>
#include <object_types.h>

void create_galaz()
{
	ustaw_nazwe("galaz");
	random_przym("prosty:pro�ci wykr�cony:wykr�ceni �ukowaty:�ukowaci"
		+" szponiasty:szponia�ci", 1);

	set_long("@@dlugi@@");

	ustaw_material(MATERIALY_DR_KLON, 100);
	set_type(O_DREWNO);
}

string dlugi()
{
	string opis = "";

	opis += "Do�� d�uga ga��� wygl�da na ca�kiem gi�tk�.";

	if(find_room()->pora_roku() == MT_WIOSNA)
	{
		opis += " Zauwa�asz na niej drobne p�czki i listki, kt�re powoli"
				+" budz� si� do �ycia.";
	}

	if(find_room()->pora_roku() == MT_LATO)
        {
		opis += " Jest g�sto pokryta ciemnozielonymi li��mi, kt�rych kszta�t"
				+" przypomina nieco ludzk� d�o�.";
	}
	
	if(find_room()->pora_roku() == MT_JESIEN)
	{
		opis += " Dostrzegasz na niej wiele r�nobarwnych li�ci, kt�re"
				+" z pewno�ci� oderwa�yby sie od niej nawet przy"
				+" delikatnym dotyku.";
			
	}
	
	if(find_room()->pora_roku() == MT_ZIMA)
	{
		opis += " Jest zupe�nie ogo�ocona z li�ci.";
	}
	
	opis += "\n";
	
	return opis;
}
