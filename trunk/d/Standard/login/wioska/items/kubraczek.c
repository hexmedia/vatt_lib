
/* Szary prosty kubraczek 
   Wykonany przez Avarda, dnia 08.05.06 */

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <pl.h>
#include <macros.h>
#include <materialy.h>
#include <object_types.h>
void
create_armour()
{
	ustaw_nazwe("kubraczek");
	dodaj_przym("szary","szarzy");
	dodaj_przym("prosty","pro^sci");
	set_long("Kubraczek zosta^l wykonany z szarego, do^s^c grubego materia^lu. "+
        "Jest ^srednio przyjemny w dotyku, ale jako ubranie zapewne spe^lnia "+
        "si^e idealnie, gwarantuj^ac ochron^e przed ch^lodem. Nie ma wielu "+
        "zdobie�, o ile zielone guziczki mo�na nazwac zdobieniami. Poza "+
        "nimi nie ma w kubraczku element^ow nadaj^acych mu atrakcyjno^sci i "+
        "bogatego wygl^adu. Mimo wszystko stanowi prosty i wygodny ubi^or dla "+
        "niewielkiej osoby.\n");

	set_slots(A_TORSO, A_FOREARMS, A_ARMS);
	add_prop(OBJ_I_VOLUME, 1250);
	add_prop(OBJ_I_WEIGHT, 500);
	ustaw_material(MATERIALY_BAWELNA, 100);
	set_type(O_UBRANIA);
	add_prop(ARMOUR_I_U_ROZCIAGLIWOSC, 30);
        add_prop(ARMOUR_I_D_ROZCIAGLIWOSC, 70);
        add_prop(OBJ_I_VALUE, 15); 
        add_prop(ARMOUR_S_DLA_RASY, "nizio�ek");
}
