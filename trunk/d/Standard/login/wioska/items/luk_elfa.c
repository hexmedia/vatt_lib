// luk dla elfa z astralwioski, g.
// na szybkersa.

inherit "/std/luk";
#include <stdproperties.h>
#include <macros.h>

void
create_luk() 
{
    dodaj_przym("d^lugi", "d^ludzy");
    dodaj_przym("cisowy", "cisowi");

    set_long("Ju^z na pierwszy rzut oka wida^c, ^ze wyszed^l on spod r^eki "+
              "rzemie^slnika niew^atpliwie znaj^acego si^e na rzeczy. "+
              "Opleciony rzemieniem majdan zapewnia wygod^e uchwytu, a "+
              "gruba, konopna ci^eciwa nie porani palc^ow strzelca. Samo "+
              "^luczysko za^s, d^lugie na cztery ^lokcie i umiej^etnie "+
              "wyprofilowane, pozwala przy odrobinie wprawy posy^la^c "+
              "strza^l^e nawet na setki jard^ow.\n");

    add_prop(OBJ_I_WEIGHT, 1500);
    add_prop(OBJ_I_VOLUME, 1500); // ?
    add_prop(OBJ_I_VALUE, 200);
    ustaw_jakosc(5);
}
/*
void
init()
{
    ::init();
    missile_init();
}*/





