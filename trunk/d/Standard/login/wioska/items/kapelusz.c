
/* Zolty slomkowy kapelusz
Wykonano dnia 11.05.2006 przez Avarda. 
Opis by Gorej */

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <pl.h>
#include <macros.h>
#include <materialy.h>
#include <object_types.h>
void
create_armour()
{
	ustaw_nazwe("kapelusz");
	dodaj_przym("z^o^lty","z^o^lci");
	dodaj_przym("s^lomkowy","s^lomkowi");
	set_long("Patrzysz na s^lomkowy kapelusz stanowi^acy ca^lkiem przyzwoita "+
		"ochrone g^lowy. Liczne ^xd^xb^la zbo�a s^a ze soba mocno splecione, "+
		"dzi^eki czemu ca^la konstrukcja jest stabilna i pewna. Brzeg ronda "+
		"kapelusza ozdobiony jest czym^s w rodzaju kolorowej wst^a�ki, co "+
		"nadaje nakryciu g^lowy swoist^a elegancje polaczona z funkcjonaln^a "+
		"prostot^a. Jeste^s przekonany �e zar^owno w deszczowe jak i s^loneczne "+
		"dni b^edzie Ci dobrze s^lu�y^l.\n");

	set_slots(A_HEAD);
	add_prop(OBJ_I_VOLUME, 90);
	add_prop(OBJ_I_WEIGHT, 100);
	ustaw_material(MATERIALY_WIKLINA, 100);
	set_type(O_UBRANIA);
	add_prop(ARMOUR_I_U_ROZCIAGLIWOSC, 20);
    add_prop(ARMOUR_I_D_ROZCIAGLIWOSC, 40);
    add_prop(ARMOUR_S_DLA_RASY, "nizio�ek");
    add_prop(OBJ_I_VALUE, 7);
}
