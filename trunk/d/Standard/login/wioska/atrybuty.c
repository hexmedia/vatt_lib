/**
 * system z obliczaniem dostepnych do wyboru atrybutow:
 * -kolor oczu, kolor wlosow, dlugosc wlosow
 * -broda, wasy
 * -zaleznie od zadeklarowanej wczesniej wagi i wzrostu wybieramy ceche ciala
 * -cecha charakterystyczna (np. garb, karnacja skory) Opcjonalnie do wyboru
 * -pochodzenie
 * -itd.
 *              vera.
 *
 *
 * W mappingach na ogol trzecia wartosc jest niepotrzebna, bo mozna
 * ja odmienic z pierwszej wartosci, ale o tym zorientowalem sie dopiero pozniej
 * wiec w sumie...to trudno sie mowi. W niczym to nie przeszkadza i tak.
 *
 * TODO:
 *  - D^lugo軼i w^los^ow trzeba przerobi^c.
 */

//inherit "/d/Standard/login/wioska/std.c";
#include <composite.h>
#include <ss_types.h>

int wzrost, waga;
mixed kolor_oczu,kolor_wlosow;
string lista_kolorow_oczu;
int tmp_wiek;
string *tmp_kolor_oczu, tmp_kolor_wlosow, *tmp_przym_wlosy;
float tmp_dlugosc_wlosow;
float tmp_dlugosc_brody, tmp_dlugosc_wasow;
string tmp_budowa_ciala, tmp_pochodzenie;
string *tmp_cecha_ciala, *tmp_cecha_szczegolna/*, *dostepne_cechy_ciala*/;
string modyfikator_od_ciala;
int faza=0, faza_pomocy=0;

//dostepne kolory oczu dla kobiety:
mapping oczy_kobiety = ([
                "b^l^ekitne":({"b^l^ekitnooki","b^l^ekitnoocy","b^l^ekitnych"}),
                "br^azowe": ({"br^azowooki","br^azowoocy","br^azowych"}),
                "ciemne":  ({"ciemnooki","ciemnoocy","ciemnych"}),
                "czarne":  ({"czarnooki","czarnoocy","czarnych"}),
                "czerwone":({"czerwonooki","czerwonoocy","czerwonych"}),
                "fio^lkowe":({"fio^lkowooki","fio^lkowoocy","fio^lkowych"}),
                "jasne":   ({"jasnooki","jasnoocy","jasnych"}),
                "migda^lowe":({"migda^lowooki","migda^lowoocy","migda^lowych"}),
                "modre":   ({"modrooki","modroocy","modrych"}),
                "niebieskie":({"niebieskooki","niebieskoocy","niebieskich"}),
                "piwne":   ({"piwnooki","piwnoocy","piwnych"}),
                "p^lomienne":({"p^lomiennooki","p^lomiennoocy","p^lomiennych"}),
                "promienne":({"promiennooki","promiennoocy","promiennych"}),
                "rybie":    ({"rybiooki","rybioocy","rybich"}),
                "sko^sne":   ({"sko^snooki","sko^snoocy","sko^snych"}),
                "szare":    ({"szarooki","szaroocy","szarych"}),
                "^z^o^lte":    ({"^z^o^ltooki","^z^o^ltoocy","^z^o^ltych"}),
                "z^lote":    ({"z^lotooki","z^lotoocy","z^lotych"}),
                "stalowe":    ({"stalowooki","stalowoocy","stalowych"}),
                "matowe":    ({"matowooki","matowoocy","matowych"}),
                "szmaragdowe":({"szmaragdowooki","szmaragdowoocy","szmaragdowych"}),
                "zielone":  ({"zielonooki","zielonoocy","zielonych"})
             ]);
//dostepne kolory oczu dla mezczyzny:
mapping oczy_mezczyzny = ([
                "b^l^ekitne":({"b^l^ekitnooki","b^l^ekitnoocy","b^l^ekitnych"}),
                "br^azowe": ({"br^azowooki","br^azowoocy","br^azowych"}),
                "ciemne":  ({"ciemnooki","ciemnoocy","ciemnych"}),
                "czarne":  ({"czarnooki","czarnoocy","czarnych"}),
                "czerwone":({"czerwonooki","czerwonoocy","czerwonych"}),
                "modre":   ({"modrooki","modroocy","modrych"}),
                "niebieskie":({"niebieskooki","niebieskoocy","niebieskich"}),
                "piwne":   ({"piwnooki","piwnoocy","piwnych"}),
                "p^lomienne":({"p^lomiennooki","p^lomiennoocy","p^lomiennych"}),
                "rybie":    ({"rybiooki","rybioocy","rybich"}),
                "sko^sne":   ({"sko^snooki","sko^snoocy","sko^snych"}),
                "szare":    ({"szarooki","szaroocy","szarych"}),
                "^z^o^lte":    ({"^z^o^ltooki","^z^o^ltoocy","^z^o^ltych"}),
                "z^lote":    ({"z^lotooki","z^lotoocy","z^lotych"}),
                "stalowe":    ({"stalowooki","stalowoocy","stalowych"}),
                "matowe":    ({"matowooki","matowoocy","matowych"}),
                "zielone":  ({"zielonooki","zielonoocy","zielonych"})
             ]);
//dostepne kolory oczu dla elfki:
mapping oczy_elfki = ([
                "b^l^ekitne":({"b^l^ekitnooki","b^l^ekitnoocy","b^l^ekitnych"}),
                "br^azowe": ({"br^azowooki","br^azowoocy","br^azowych"}),
                "ciemne":  ({"ciemnooki","ciemnoocy","ciemnych"}),
                "czarne":  ({"czarnooki","czarnoocy","czarnych"}),
                "fio^lkowe":({"fio^lkowooki","fio^lkowoocy","fio^lkowych"}),
                "jasne":   ({"jasnooki","jasnoocy","jasnych"}),
                "migda^lowe":({"migda^lowooki","migda^lowoocy","migda^lowych"}),
                "modre":   ({"modrooki","modroocy","modrych"}),
                "niebieskie":({"niebieskooki","niebieskoocy","niebieskich"}),
                "p^lomienne":({"p^lomiennooki","p^lomiennoocy","p^lomiennych"}),
                "promienne":({"promiennooki","promiennoocy","promiennych"}),
                "srebrne":  ({"srebrnooki","srebrnoocy","srebrnych"}),
                "szare":    ({"szarooki","szaroocy","szarych"}),
                "z^lote":    ({"z^lotooki","z^lotoocy","z^lotych"}),
                "stalowe":    ({"stalowooki","stalowoocy","stalowych"}),
                "szmaragdowe":({"szmaragdowooki","szmaragdowoocy","szmaragdowych"}),
                "orzechowe":    ({"orzechowooki","orzechowoocy","orzechowych"}),
                "zielone":  ({"zielonooki","zielonoocy","zielonych"})
             ]);
//dostepne kolory oczu dla elfa:
mapping oczy_elfa = ([
                "b^l^ekitne":({"b^l^ekitnooki","b^l^ekitnoocy","b^l^ekitnych"}),
                "br^azowe": ({"br^azowooki","br^azowoocy","br^azowych"}),
                "ciemne":  ({"ciemnooki","ciemnoocy","ciemnych"}),
                "czarne":  ({"czarnooki","czarnoocy","czarnych"}),
                "fio^lkowe":({"fio^lkowooki","fio^lkowoocy","fio^lkowych"}),
                "jasne":   ({"jasnooki","jasnoocy","jasnych"}),
                "modre":   ({"modrooki","modroocy","modrych"}),
                "niebieskie":({"niebieskooki","niebieskoocy","niebieskich"}),
                "p^lomienne":({"p^lomiennooki","p^lomiennoocy","p^lomiennych"}),
                "promienne":({"promiennooki","promiennoocy","promiennych"}),
                "srebrne":  ({"srebrnooki","srebrnoocy","srebrnych"}),
                "szare":    ({"szarooki","szaroocy","szarych"}),
                "z^lote":    ({"z^lotooki","z^lotoocy","z^lotych"}),
                "stalowe":    ({"stalowooki","stalowoocy","stalowych"}),
                "szmaragdowe":({"szmaragdowooki","szmaragdowoocy","szmaragdowych"}),
                "orzechowe":    ({"orzechowooki","orzechowoocy","orzechowych"}),
                "zielone":  ({"zielonooki","zielonoocy","zielonych"})
             ]);
//dostepne kolory oczu dla nizio^lki:
mapping oczy_niziolki = ([
                "b^l^ekitne":({"b^l^ekitnooki","b^l^ekitnoocy","b^l^ekitnych"}),
                "br^azowe": ({"br^azowooki","br^azowoocy","br^azowych"}),
                "ciemne":  ({"ciemnooki","ciemnoocy","ciemnych"}),
                "czarne":  ({"czarnooki","czarnoocy","czarnych"}),
                "fio^lkowe":({"fio^lkowooki","fio^lkowoocy","fio^lkowych"}),
                "jasne":   ({"jasnooki","jasnoocy","jasnych"}),
                "migda^lowe":({"migda^lowooki","migda^lowoocy","migda^lowych"}),
                "modre":   ({"modrooki","modroocy","modrych"}),
                "niebieskie":({"niebieskooki","niebieskoocy","niebieskich"}),
                "piwne":   ({"piwnooki","piwnoocy","piwnych"}),
                "promienne":({"promiennooki","promiennoocy","promiennych"}),
                "szare":    ({"szarooki","szaroocy","szarych"}),
                "z^lote":    ({"z^lotooki","z^lotoocy","z^lotych"}),
                "stalowe":    ({"stalowooki","stalowoocy","stalowych"}),
                "matowe":    ({"matowooki","matowoocy","matowych"}),
                "szmaragdowe":({"szmaragdowooki","szmaragdowoocy","szmaragdowych"}),
                "orzechowe":    ({"orzechowooki","orzechowoocy","orzechowych"}),
                "zielone":  ({"zielonooki","zielonoocy","zielonych"})
             ]);
//dostepne kolory oczu dla nizio^lka:
mapping oczy_niziolka = ([
                "b^l^ekitne":({"b^l^ekitnooki","b^l^ekitnoocy","b^l^ekitnych"}),
                "br^azowe": ({"br^azowooki","br^azowoocy","br^azowych"}),
                "ciemne":  ({"ciemnooki","ciemnoocy","ciemnych"}),
                "czarne":  ({"czarnooki","czarnoocy","czarnych"}),
                "jasne":   ({"jasnooki","jasnoocy","jasnych"}),
                "modre":   ({"modrooki","modroocy","modrych"}),
                "niebieskie":({"niebieskooki","niebieskoocy","niebieskich"}),
                "piwne":   ({"piwnooki","piwnoocy","piwnych"}),
                "promienne":({"promiennooki","promiennoocy","promiennych"}),
                "szare":    ({"szarooki","szaroocy","szarych"}),
                "z^lote":    ({"z^lotooki","z^lotoocy","z^lotych"}),
                "stalowe":    ({"stalowooki","stalowoocy","stalowych"}),
                "matowe":    ({"matowooki","matowoocy","matowych"}),
                "szmaragdowe":({"szmaragdowooki","szmaragdowoocy","szmaragdowych"}),
                "orzechowe":    ({"orzechowooki","orzechowoocy","orzechowych"}),
                "zielone":  ({"zielonooki","zielonoocy","zielonych"})
             ]);
//dostepne kolory oczu dla krasnoludow (obie plcie maja takie same):
mapping oczy_krasnoludow = ([
                "b^l^ekitne":({"b^l^ekitnooki","b^l^ekitnoocy","b^l^ekitnych"}),
                "br^azowe": ({"br^azowooki","br^azowoocy","br^azowych"}),
                "ciemne":  ({"ciemnooki","ciemnoocy","ciemnych"}),
                "czarne":  ({"czarnooki","czarnoocy","czarnych"}),
                "czerwone":({"czerwonooki","czerwonoocy","czerwonych"}),
                "jasne":   ({"jasnooki","jasnoocy","jasnych"}),
                "niebieskie":({"niebieskooki","niebieskoocy","niebieskich"}),
                "piwne":   ({"piwnooki","piwnoocy","piwnych"}),
                "rybie":    ({"rybiooki","rybioocy","rybich"}),
                "sko^sne":   ({"sko^snooki","sko^snoocy","sko^snych"}),
                "szare":    ({"szarooki","szaroocy","szarych"}),
                "^z^o^lte":    ({"^z^o^ltooki","^z^o^ltoocy","^z^o^ltych"}),
                "stalowe":    ({"stalowooki","stalowoocy","stalowych"}),
                "matowe":    ({"matowooki","matowoocy","matowych"}),
                "zielone":  ({"zielonooki","zielonoocy","zielonych"})
             ]);
//dostepne kolory oczu dla p^o^lelfki:
mapping oczy_polelfki = ([
                "b^l^ekitne":({"b^l^ekitnooki","b^l^ekitnoocy","b^l^ekitnych"}),
                "br^azowe": ({"br^azowooki","br^azowoocy","br^azowych"}),
                "ciemne":  ({"ciemnooki","ciemnoocy","ciemnych"}),
                "czarne":  ({"czarnooki","czarnoocy","czarnych"}),
                "fio^lkowe":({"fio^lkowooki","fio^lkowoocy","fio^lkowych"}),
                "jasne":   ({"jasnooki","jasnoocy","jasnych"}),
                "migda^lowe":({"migda^lowooki","migda^lowoocy","migda^lowych"}),
                "modre":   ({"modrooki","modroocy","modrych"}),
                "niebieskie":({"niebieskooki","niebieskoocy","niebieskich"}),
                "piwne":   ({"piwnooki","piwnoocy","piwnych"}),
                "p^lomienne":({"p^lomiennooki","p^lomiennoocy","p^lomiennych"}),
                "promienne":({"promiennooki","promiennoocy","promiennych"}),
                "rybie":    ({"rybiooki","rybioocy","rybich"}),
                "sko^sne":   ({"sko^snooki","sko^snoocy","sko^snych"}),
                "szare":    ({"szarooki","szaroocy","szarych"}),
                "^z^o^lte":    ({"^z^o^ltooki","^z^o^ltoocy","^z^o^ltych"}),
                "z^lote":    ({"z^lotooki","z^lotoocy","z^lotych"}),
                "stalowe":    ({"stalowooki","stalowoocy","stalowych"}),
                "matowe":    ({"matowooki","matowoocy","matowych"}),
                "orzechowe":    ({"orzechowooki","orzechowoocy","orzechowych"}),
                "zielone":  ({"zielonooki","zielonoocy","zielonych"})
             ]);
//dostepne kolory oczu dla p^o^lelfa:
mapping oczy_polelfa = ([
                "b^l^ekitne":({"b^l^ekitnooki","b^l^ekitnoocy","b^l^ekitnych"}),
                "br^azowe": ({"br^azowooki","br^azowoocy","br^azowych"}),
                "ciemne":  ({"ciemnooki","ciemnoocy","ciemnych"}),
                "czarne":  ({"czarnooki","czarnoocy","czarnych"}),
                "jasne":   ({"jasnooki","jasnoocy","jasnych"}),
                "modre":   ({"modrooki","modroocy","modrych"}),
                "niebieskie":({"niebieskooki","niebieskoocy","niebieskich"}),
                "piwne":   ({"piwnooki","piwnoocy","piwnych"}),
                "p^lomienne":({"p^lomiennooki","p^lomiennoocy","p^lomiennych"}),
                "rybie":    ({"rybiooki","rybioocy","rybich"}),
                "sko^sne":   ({"sko^snooki","sko^snoocy","sko^snych"}),
                "szare":    ({"szarooki","szaroocy","szarych"}),
                "^z^o^lte":    ({"^z^o^ltooki","^z^o^ltoocy","^z^o^ltych"}),
                "z^lote":    ({"z^lotooki","z^lotoocy","z^lotych"}),
                "stalowe":    ({"stalowooki","stalowoocy","stalowych"}),
                "matowe":    ({"matowooki","matowoocy","matowych"}),
                "zielone":  ({"zielonooki","zielonoocy","zielonych"})
             ]);
//dostepne kolory oczu dla gnomow (obie plcie maja te same):
mapping oczy_gnomow = ([
              "b^l^ekitne":({"b^l^ekitnooki","b^l^ekitnoocy","b^l^ekitnych"}),
              "br^azowe": ({"br^azowooki","br^azowoocy","br^azowych"}),
              "ciemne":  ({"ciemnooki","ciemnoocy","ciemnych"}),
              "czarne":  ({"czarnooki","czarnoocy","czarnych"}),
              "czerwone":({"czerwonooki","czerwonoocy","czerwonych"}),
              "jasne":   ({"jasnooki","jasnoocy","jasnych"}),
              "niebieskie":({"niebieskooki","niebieskoocy","niebieskich"}),
              "rybie":    ({"rybiooki","rybioocy","rybich"}),
              "sko^sne":   ({"sko^snooki","sko^snoocy","sko^snych"}),
              "szare":    ({"szarooki","szaroocy","szarych"}),
              "^z^o^lte":    ({"^z^o^ltooki","^z^o^ltoocy","^z^o^ltych"}),
              "stalowe":    ({"stalowooki","stalowoocy","stalowych"}),
              "matowe":    ({"matowooki","matowoocy","matowych"}),
              "zielone":  ({"zielonooki","zielonoocy","zielonych"})
             ]);

//dostepne naturalne kolory wlosow dla kobiety:
mapping wlosy_kobiety = ([
              "bia^le":   ({"bia^low^losy","bia^low^losi"}),
              "br您owe": ({"br您owow^losy","br您owow^losi"}),
              "czarne":  ({"czarnow^losy","czarnow^losi"}),
              "ciemne":  ({"ciemnow^losy","ciemnow^losi"}),
              "czerwone":({"czerwonow^losy","czerwonow^losi"}),
              "jasne":   ({"jasnow^losy","jasnow^losi"}),
              "kasztanowe":({"kasztanowow^losy","kasztanowow^losi"}),
              "kruczoczarne":({"kruczow^losy","kruczow^losi"}),
              "ogniste":  ({"ognistow^losy","ognistow^losi"}),
              "p^lowe":   ({"p^lowow^losy","p^lowow^losi"}),
              "rude":    ({"rudow^losy","rudow^losi"}),
              "szare":   ({"szarow^losy","szarow^losi"}),
              "z^lote":   ({"z^lotow^losy","z^lotow^losi"}),
              "z^lociste":   ({"z^locistow^losy","z^locistow^losi"}),
              "miedziane":   ({"miedzianow^losy","miedzianow^losi"}),
              "siwe":    ({"siwow^losy","siwow^losi"}),
              "popielate":({"popielatow^losy","popielatow^losi"})
              ]);
//dostepne naturalne kolory wlosow dla mezczyzny:
mapping wlosy_mezczyzny = ([
              "bia^le":   ({"bia^low^losy","bia^low^losi"}),
              "br您owe": ({"br您owow^losy","br您owow^losi"}),
              "czarne":  ({"czarnow^losy","czarnow^losi"}),
              "ciemne":  ({"ciemnow^losy","ciemnow^losi"}),
              "czerwone":({"czerwonow^losy","czerwonow^losi"}),
              "jasne":   ({"jasnow^losy","jasnow^losi"}),
              "kruczoczarne":({"kruczow^losy","kruczow^losi"}),
              "ogniste":  ({"ognistow^losy","ognistow^losi"}),
              "p^lowe":   ({"p^lowow^losy","p^lowow^losi"}),
              "rude":    ({"rudow^losy","rudow^losi"}),
              "z^lote":   ({"z^lotow^losy","z^lotow^losi"}),
              "z^lociste":   ({"z^locistow^losy","z^locistow^losi"}),
              "miedziane":   ({"miedzianow^losy","miedzianow^losi"}),
              "siwe":    ({"siwow^losy","siwow^losi"}),
              "szare":   ({"szarow^losy","szarow^losi"}),
              "popielate":({"popielatow^losy","popielatow^losi"})
              ]);
//dostepne naturalne kolory wlosow dla elfow (obie plcie tak samo):
mapping wlosy_elfow = ([
              "bia^le":   ({"bia^low^losy","bia^low^losi"}),
              "br您owe": ({"br您owow^losy","br您owow^losi"}),
              "czarne":  ({"czarnow^losy","czarnow^losi"}),
              "ciemne":  ({"ciemnow^losy","ciemnow^losi"}),
              "czerwone":({"czerwonow^losy","czerwonow^losi"}),
              "jasne":   ({"jasnow^losy","jasnow^losi"}),
              "kasztanowe":({"kasztanowow^losy","kasztanowow^losi"}),
              "kruczoczarne":({"kruczow^losy","kruczow^losi"}),
              "ogniste":  ({"ognistow^losy","ognistow^losi"}),
              "rude":    ({"rudow^losy","rudow^losi"}),
              "z^lote":   ({"z^lotow^losy","z^lotow^losi"}),
              "z^lociste":   ({"z^locistow^losy","z^locistow^losi"}),
              "miedziane":   ({"miedzianow^losy","miedzianow^losi"}),
              "miodowe":     ({"miodowow^losy","miodowow^losi"}),
              "srebrne": ({"srebrnow^losy","srebrnow^losi"}),
              "zielone": ({"zielonow^losy","zielonow^losi"})
              ]);
//dostepne naturalne kolory wlosow dla nizio^lk^ow (bez podzia^lu na p^le^c):
mapping wlosy_niziolkow = ([
              "bia^le":   ({"bia^low^losy","bia^low^losi"}),
              "br您owe": ({"br您owow^losy","br您owow^losi"}),
              "czarne":  ({"czarnow^losy","czarnow^losi"}),
              "ciemne":  ({"ciemnow^losy","ciemnow^losi"}),
              "jasne":   ({"jasnow^losy","jasnow^losi"}),
              "kasztanowe":({"kasztanowow^losy","kasztanowow^losi"}),
              "p^lowe":   ({"p^lowow^losy","p^lowow^losi"}),
              "ogniste":  ({"ognistow^losy","ognistow^losi"}),
              "rude":    ({"rudow^losy","rudow^losi"}),
              "z^lote":   ({"z^lotow^losy","z^lotow^losi"}),
              "z^lociste":   ({"z^locistow^losy","z^locistow^losi"}),
              "miedziane":   ({"miedzianow^losy","miedzianow^losi"}),
              "siwe":    ({"siwow^losy","siwow^losi"}),
              "popielate":({"popielatow^losy","popielatow^losi"}),
              ]);
//dostepne naturalne kolory wlosow dla krasnoludow (bez podzia^lu na p^le^c):
mapping wlosy_krasnoludow = ([
              "bia^le":   ({"bia^low^losy","bia^low^losi"}),
              "czarne":  ({"czarnow^losy","czarnow^losi"}),
              "ciemne":  ({"ciemnow^losy","ciemnow^losi"}),
              "czerwone":({"czerwonow^losy","czerwonow^losi"}),
              "jasne":   ({"jasnow^losy","jasnow^losi"}),
              "pomara^nczowe":({"pomara^nczowow^losy","pomara^nczowow^losi"}),
              "ogniste":  ({"ognistow^losy","ognistow^losi"}),
              "rude":    ({"rudow^losy","rudow^losi"}),
              "miedziane":   ({"miedzianow^losy","miedzianow^losi"}),
              "siwe":    ({"siwow^losy","siwow^losi"})
              ]);
//dostepne naturalne kolory wlosow dla p^o^lelfow (obie plcie tak samo):
mapping wlosy_polelfow = ([
              "bia^le":   ({"bia^low^losy","bia^low^losi"}),
              "br您owe": ({"br您owow^losy","br您owow^losi"}),
              "czarne":  ({"czarnow^losy","czarnow^losi"}),
              "ciemne":  ({"ciemnow^losy","ciemnow^losi"}),
              "czerwone":({"czerwonow^losy","czerwonow^losi"}),
              "jasne":   ({"jasnow^losy","jasnow^losi"}),
              "kruczoczarne":({"kruczow^losy","kruczow^losi"}),
              "miodowe":     ({"miodowow^losy","miodowow^losi"}),
              "p^lowe":   ({"p^lowow^losy","p^lowow^losi"}),
              "ogniste":  ({"ognistow^losy","ognistow^losi"}),
              "rude":    ({"rudow^losy","rudow^losi"}),
              "z^lote":   ({"z^lotow^losy","z^lotow^losi"}),
              "z^lociste":   ({"z^locistow^losy","z^locistow^losi"}),
              "miedziane":   ({"miedzianow^losy","miedzianow^losi"}),
              "srebrne": ({"srebrnow^losy","srebrnow^losi"}),
              "szare":   ({"szarow^losy","szarow^losi"}),
              ]);
//dostepne naturalne kolory wlosow dla gnom^ow (bez podzia^lu na p^le^c):
mapping wlosy_gnomow = ([
              "bia^le":   ({"bia^low^losy","bia^low^losi"}),
              "czarne":  ({"czarnow^losy","czarnow^losi"}),
              "ciemne":  ({"ciemnow^losy","ciemnow^losi"}),
              "czerwone":({"czerwonow^losy","czerwonow^losi"}),
              "jasne":   ({"jasnow^losy","jasnow^losi"}),
              "ogniste":  ({"ognistow^losy","ognistow^losi"}),
              "rude":    ({"rudow^losy","rudow^losi"}),
              "miedziane":   ({"miedzianow^losy","miedzianow^losi"}),
              "siwe":    ({"siwow^losy","siwow^losi"}),
              ]);

//dostepne dlugosci wlosow
mapping dlugosc_wlosow = ([
                "brak":               0.0,
                "bardzo kr^otkie":       3.0,
                "kr^otkie":            6.0,
                "^sredniej d^lugo^sci":  10.0,
                "si^egaj^ace ramion":   30.0,
                "d^lugie":             40.0,
                "bardzo d^lugie":      50.0,
                "si^egaj^ace pasa":     65.0,
                "si^egaj^ace po^lowy ud":85.0,
                "si^egaj^ace kolan":    110.0,
                "si^egaj^ace ziemi":    160.0
              ]);

/*
 * Cyfry na koncu tych mappingow oznaczaja na jakie cechy dany przymiotnik ma wplyw.
 * Np, gdy jest s to znaczy, ze postac bedzie troche silna po wybraniu tego.
 *
 * s-sila,
 * z-zrecznosc,
 * w-wytrzymalosc,
 * b-brak
 */

//dostepne szczegolne cechy dotyczace budowy ciala (w zastepstwie opisu wagi) dla kobiety:
mapping cialo_kobiety = ([
    "szerokie barki":   ({"barczysty","barczy^sci","barczyst^a","s","osiem"}),
    "bary^lkowato^s^c": ({"bary^lkowaty","bary^lkowaci","bary^lkowat^a","b","osiem"}),
    "beczkowato^s^c":  ({"beczkowaty","beczkowaci","beczkowat^a","b","siedem"}),
    "poka^xny brzuch": ({"brzuchaty","brzuchaci","brzuchat^a","b","dziewiec"}),
    "szczup^la budowa cia^la": ({"szczup^ly","szczupli","szczup^l^a","z","trzy"}),
    "chuda budowa cia^la": ({"chudy","chudzi","chud^a","z","cztery"}),
    "ci^e^zka postura":  ({"ci^e^zki","ci^e^zcy","ci^e^zk^a","w","osiem"}),
    "ko^scisto^s^c":  ({"ko^scisty","ko^sci^sci","ko^scist^a","b","siedem"}),
    "lekka budowa cia^la": ({"lekki","lekcy","lekk^a","z","cztery"}),
    "gruba budowa cia^la": ({"gruby","grubi","grub^a","w","szesc"}),
    "oty^lo^s^c": ({"oty^ly","otyli","oty^l^a","w","siedem"}),
    "niepozorna budowa cia^la":({"niepozorny","niepozorni","niepozorn^a","b","piec"}),
    "pulchna budowa cia^la": ({"pulchny","pulchni","pulchn^a","w","piec"}),
    "smuk^la budowa cia^la":  ({"smuk^ly","smukli","smuk^l^a","z","piec"}),
    "w^at^la budowa cia^la": ({"w^at^ly","w^atli","w^at^l^a","b","szesc"}),
    "wielka postura":   ({"wielki","wielcy","wielk^a","s","dziewiec"}),
    "t^luste cia^lo":     ({"t^lusty","t^lu^sci","t^lust^a","w","osiem"}),
    "kr^epa budowa cia^la": ({"kr^epy","kr^epi","kr^ep^a","s","osiem"}),
    "puszysto^s^c": ({"puszysty","puszy^sci","puszyst^a","w","szesc"}),
    "drobna budowa cia^la": ({"drobny","drobni","drobn^a","b","piec"}),
    "filigranowa budowa cia^la": ({"filigranowy","filigranowi","filigranow^a","b","szesc"}),
    "^zylasta budowa cia^la": ({"^zylasty","^zyla^sci","^zylast^a","b","siedem"}),
    //KURDE, musi byc cos 'przecietnego' :P
    "proporcjonalna budowa cia^la": ({"proporcjonalny","proporcjonalni",
                                     "proporcjonaln^a","b","zero"}),
    "przeci^etna waga":  ({"przeci^etnej wagi","przeci^etnych wag","przeci^etnej wagi","b",
                          "zero"})
              ]);
//dostepne szczegolne cechy dostyczace budowy ciala (w zastepstwie opisu wagi) dla mezczyzny:
mapping cialo_mezczyzny = ([
    "masywna postura":  ({"masywny","masywni","masywnym","w","szesc"}),
    "poka^xne musku^ly":  ({"muskularny","muskularni","muskularnym","s","siedem"}),
    "szerokie barki":   ({"barczysty","barczy^sci","barczystym","s","szesc"}),
    "bary^lkowato^s^c": ({"bary^lkowaty","bary^lkowaci","bary^lkowatym","b","siedem"}),
    "beczkowato^s^c":  ({"beczkowaty","beczkowaci","beczkowatym","b","siedem"}),
    "poka^xny brzuch": ({"brzuchaty","brzuchaci","brzuchatym","b","piec"}),
    "szczup^la budowa cia^la": ({"szczup^ly","szczupli","szczup^lym","z","trzy"}),
    "chuda budowa cia^la": ({"chudy","chudzi","chudym","z","cztery"}),
    "ci^e^zka postura":  ({"ci^e^zki","ci^e^zcy","ci^e^zkim","w","osiem"}),
    "ko^scisto^s^c":  ({"ko^scisty","ko^sci^sci","ko^scistym","b","osiem"}),
    "lekka budowa cia^la": ({"lekki","lekcy","lekkim","z","piec"}),
    "gruba budowa cia^la": ({"gruby","grubi","grubym","w","szesc"}),
    "oty^lo^s^c": ({"oty^ly","otyli","oty^lym","w","szesc"}),
    "niepozorna budowa cia^la": ({"niepozorny","niepozorni","niepozornym","b","szesc"}),
    "pulchna budowa cia^la": ({"pulchny","pulchni","pulchnym","w","piec"}),
    "smuk^la budowa cia^la":  ({"smuk^ly","smukli","smuk^lym","z","piec"}),
    "w^at^la budowa cia^la": ({"w^at^ly","w^atli","w^at^lym","b","szesc"}),
    "wielka postura":   ({"wielki","wielcy","wielkim","s","osiem"}),
    "zwalista postura": ({"zwalisty","zwali^sci","zwalistym","w","dziewiec"}),
    "t^luste cia^lo":     ({"t^lusty","t^lu^sci","t^lustym","w","osiem"}),
    "poka^xne mi^e^snie":  ({"umi^e^sniony","umi^e^snieni","umi^e^snionym","s","szesc"}),
    "kr^epa budowa cia^la": ({"kr^epy","kr^epi","kr^epym","s","siedem"}),
    "puszysto^s^c": ({"puszysty","puszy^sci","puszystym","w","szesc"}),
    "drobna budowa cia^la": ({"drobny","drobni","drobnym","b","szesc"}),
    "^zylasta budowa cia^la": ({"^zylasty","^zyla^sci","^zylastym","b","cztery"}),
    "proporcjonalna budowa cia^la": ({"proporcjonalny","proporcjonalni",
                                     "proporcjonalnym","b","zero"}),
    "przeci^etna waga":  ({"przeci^etnej wagi","przeci^etnych wag","przeci^etnej wagi","b",
                          "zero"})
              ]);

//dostepne szczegolne cechy dotyczace budowy ciala
// (w zastepstwie opisu wagi) dla polelfki:
mapping cialo_polelfki = ([
    "beczkowato^s^c":  ({"beczkowaty","beczkowaci","beczkowat^a","b","osiem"}),
    "poka^xny brzuch": ({"brzuchaty","brzuchaci","brzuchat^a","b","dziewiec"}),
    "szczup^la budowa cia^la": ({"szczup^ly","szczupli","szczup^l^a","z","piec"}),
    "chuda budowa cia^la": ({"chudy","chudzi","chud^a","z","szesc"}),
    "ko^scisto^s^c":  ({"ko^scisty","ko^sci^sci","ko^scist^a","b","siedem"}),
    "lekka budowa cia^la": ({"lekki","lekcy","lekk^a","z","piec"}),
    "oty^lo^s^c": ({"oty^ly","otyli","oty^l^a","w","osiem"}),
    "niepozorna budowa cia^la":({"niepozorny","niepozorni","niepozorn^a","b","szesc"}),
    "pulchna budowa cia^la": ({"pulchny","pulchni","pulchn^a","w","siedem"}),
    "smuk^la budowa cia^la":  ({"smuk^ly","smukli","smuk^l^a","z","szesc"}),
    "w^at^la budowa cia^la": ({"w^at^ly","w^atli","w^at^l^a","b","szesc"}),
    "puszysto^s^c": ({"puszysty","puszy^sci","puszyst^a","w","osiem"}),
    "drobna budowa cia^la": ({"drobny","drobni","drobn^a","b","piec"}),
    "filigranowa budowa cia^la": ({"filigranowy","filigranowi","filigranow^a","b","siedem"}),
    "^zylasta budowa cia^la": ({"^zylasty","^zyla^sci","^zylast^a","b","siedem"}),
    //KURDE, musi byc cos 'przecietnego' :P
    "proporcjonalna budowa cia^la": ({"proporcjonalny","proporcjonalni",
                                     "proporcjonaln^a","b","zero"}),
    "przeci^etna waga":  ({"przeci^etnej wagi","przeci^etnej wagi","przeci^etnej wagi","b",
                          "zero"})
              ]);
//dostepne szczegolne cechy dostyczace budowy ciala
//(w zastepstwie opisu wagi) dla polelfa:
mapping cialo_polelfa= ([
    "poka^xne musku^ly":  ({"muskularny","muskularni","muskularnym","s","siedem"}),
    "szerokie barki":   ({"barczysty","barczy^sci","barczystym","s","siedem"}),
    "beczkowato^s^c":  ({"beczkowaty","beczkowaci","beczkowatym","b","osiem"}),
    "poka^xny brzuch": ({"brzuchaty","brzuchaci","brzuchatym","b","osiem"}),
    "szczup^la budowa cia^la": ({"szczup^ly","szczupli","szczup^lym","z","trzy"}),
    "chuda budowa cia^la": ({"chudy","chudzi","chudym","z","cztery"}),
    "ko^scisto^s^c":  ({"ko^scisty","ko^sci^sci","ko^scistym","b","piec"}),
    "lekka budowa cia^la": ({"lekki","lekcy","lekkim","z","piec"}),
    "oty^lo^s^c": ({"oty^ly","otyli","oty^lym","w","siedem"}),
    "niepozorna budowa cia^la": ({"niepozorny","niepozorni","niepozornym","b","szesc"}),
    "pulchna budowa cia^la": ({"pulchny","pulchni","pulchnym","w","siedem"}),
    "smuk^la budowa cia^la":  ({"smuk^ly","smukli","smuk^lym","z","piec"}),
    "w^at^la budowa cia^la": ({"w^at^ly","w^atli","w^at^lym","b","piec"}),
    "poka^xne mi^e^snie":  ({"umi^e^sniony","umi^e^snieni","umi^e^snionym","s","siedem"}),
    "kr^epa budowa cia^la": ({"kr^epy","kr^epi","kr^epym","s","osiem"}),
    "puszysto^s^c": ({"puszysty","puszy^sci","puszystym","w","osiem"}),
    "drobna budowa cia^la": ({"drobny","drobni","drobnym","b","szesc"}),
    "^zylasta budowa cia^la": ({"^zylasty","^zyla^sci","^zylastym","b","szesc"}),
    "proporcjonalna budowa cia^la": ({"proporcjonalny","proporcjonalni",
                                     "proporcjonalnym","b","zero"}),
    "przeci^etna waga":  ({"przeci^etnej wagi","przeci^etnej wagi","przeci^etnej wagi","b",
                          "zero"})
              ]);

//dostepne szczegolne cechy dotyczace budowy ciala
// (w zastepstwie opisu wagi) dla elfow:
mapping cialo_elfki = ([
    "szczup^la budowa cia^la": ({"szczup^ly","szczupli","szczup^l^a","z","dwa"}),
    "chuda budowa cia^la": ({"chudy","chudzi","chud^a","z","trzy"}),
    "ko^scisto^s^c":  ({"ko^scisty","ko^sci^sci","ko^scist^a","b","cztery"}),
    "lekka budowa cia^la": ({"lekki","lekcy","lekk^a","z","trzy"}),
    "niepozorna budowa cia^la":({"niepozorny","niepozorni","niepozorn^a","b","szesc"}),
    "pulchna budowa cia^la": ({"pulchny","pulchni","pulchn^a","w","osiem"}),
    "smuk^la budowa cia^la":  ({"smuk^ly","smukli","smuk^l^a","z","trzy"}),
    "w^at^la budowa cia^la": ({"w^at^ly","w^atli","w^at^l^a","b","cztery"}),
    "drobna budowa cia^la": ({"drobny","drobni","drobn^a","b","piec"}),
    "filigranowa budowa cia^la": ({"filigranowy","filigranowi","filigranow^a","b","piec"}),
    //KURDE, musi byc cos 'przecietnego' :P
    "proporcjonalna budowa cia^la": ({"proporcjonalny","proporcjonalni",
                                     "proporcjonaln^a","b","brak"}),
    "przeci^etna waga":  ({"przeci^etnej wagi","przeci^etnej wagi","przeci^etnej wagi","b",
                          "brak"})
              ]);
mapping cialo_elfa = ([
    "szczup^la budowa cia^la": ({"szczup^ly","szczupli","szczup^lym","z","dwa"}),
    "chuda budowa cia^la": ({"chudy","chudzi","chudym","z","trzy"}),
    "ko^scisto^s^c":  ({"ko^scisty","ko^sci^sci","ko^scistym","b","cztery"}),
    "lekka budowa cia^la": ({"lekki","lekcy","lekkim","z","trzy"}),
    "niepozorna budowa cia^la":({"niepozorny","niepozorni","niepozornym","b","szesc"}),
    "pulchna budowa cia^la": ({"pulchny","pulchni","pulchnym","w","osiem"}),
    "smuk^la budowa cia^la":  ({"smuk^ly","smukli","smuk^lym","z","trzy"}),
    "w^at^la budowa cia^la": ({"w^at^ly","w^atli","w^at^lym","b","cztery"}),
    "drobna budowa cia^la": ({"drobny","drobni","drobnym","b","piec"}),
    "filigranowa budowa cia^la": ({"filigranowy","filigranowi","filigranowym","b","piec"}),
    //KURDE, musi byc cos 'przecietnego' :P
    "proporcjonalna budowa cia^la": ({"proporcjonalny","proporcjonalni",
                                     "proporcjonalnym","b","brak"}),
    "przeci^etna waga":  ({"przeci^etnej wagi","przeci^etnej wagi","przeci^etnej wagi","b",
                          "brak"})
              ]);

//dostepne szczegolne cechy dotyczace budowy ciala
//(w zastepstwie opisu wagi) dla niziolkow:
mapping cialo_niziolki = ([
    "bary^lkowato^s^c": ({"bary^lkowaty","bary^lkowaci","bary^lkowat^a","b","szesc"}),
    "beczkowato^s^c":  ({"beczkowaty","beczkowaci","beczkowat^a","b","szesc"}),
    "poka^xny brzuch": ({"brzuchaty","brzuchaci","brzuchat^a","b","piec"}),
    "szczup^la budowa cia^la": ({"szczup^ly","szczupli","szczup^l^a","z","szesc"}),
    "chuda budowa cia^la": ({"chudy","chudzi","chud^a","z","siedem"}),
    "lekka budowa cia^la": ({"lekki","lekcy","lekk^a","z","szesc"}),
    "gruba budowa cia^la": ({"gruby","grubi","grub^a","w","siedem"}),
    "oty^lo^s^c": ({"oty^ly","otyli","oty^l^a","w","osiem"}),
    "niepozorna budowa cia^la":({"niepozorny","niepozorni","niepozorn^a","b","piec"}),
    "pulchna budowa cia^la": ({"pulchny","pulchni","pulchn^a","w","piec"}),
    "w^at^la budowa cia^la": ({"w^at^ly","w^atli","w^at^l^a","b","szesc"}),
    "t^luste cia^lo":     ({"t^lusty","t^lu^sci","t^lust^a","w","osiem"}),
    "puszysto^s^c": ({"puszysty","puszy^sci","puszyst^a","w","szesc"}),
    "drobna budowa cia^la": ({"drobny","drobni","drobn^a","b","szesc"}),
    "filigranowa budowa cia^la": ({"filigranowy","filigranowi","filigranow^a","b","osiem"}),
    //KURDE, musi byc cos 'przecietnego' :P
    "proporcjonalna budowa cia^la": ({"proporcjonalny","proporcjonalni",
                                     "proporcjonaln^a","b","zero"}),
    "przeci^etna waga":  ({"przeci^etnej wagi","przeci^etnej wagi","przeci^etnej wagi","b",
                          "zero"})
              ]);
mapping cialo_niziolka = ([
    "bary^lkowato^s^c": ({"bary^lkowaty","bary^lkowaci","bary^lkowatym","b","szesc"}),
    "beczkowato^s^c":  ({"beczkowaty","beczkowaci","beczkowatym","b","szesc"}),
    "poka^xny brzuch": ({"brzuchaty","brzuchaci","brzuchatym","b","piec"}),
    "szczup^la budowa cia^la": ({"szczup^ly","szczupli","szczup^lym","z","szesc"}),
    "chuda budowa cia^la": ({"chudy","chudzi","chudym","z","siedem"}),
    "lekka budowa cia^la": ({"lekki","lekcy","lekkim","z","szesc"}),
    "gruba budowa cia^la": ({"gruby","grubi","grubym","w","siedem"}),
    "oty^lo^s^c": ({"oty^ly","otyli","oty^lym","w","osiem"}),
    "niepozorna budowa cia^la":({"niepozorny","niepozorni","niepozornym","b","piec"}),
    "pulchna budowa cia^la": ({"pulchny","pulchni","pulchnym","w","piec"}),
    "w^at^la budowa cia^la": ({"w^at^ly","w^atli","w^at^lym","b","szesc"}),
    "t^luste cia^lo":     ({"t^lusty","t^lu^sci","t^lustym","w","osiem"}),
    "puszysto^s^c": ({"puszysty","puszy^sci","puszystym","w","szesc"}),
    "drobna budowa cia^la": ({"drobny","drobni","drobnym","b","szesc"}),
    "filigranowa budowa cia^la": ({"filigranowy","filigranowi","filigranowym","b","osiem"}),
    //KURDE, musi byc cos 'przecietnego' :P
    "proporcjonalna budowa cia^la": ({"proporcjonalny","proporcjonalni",
                                     "proporcjonalnym","b","zero"}),
    "przeci^etna waga":  ({"przeci^etnej wagi","przeci^etnej wagi","przeci^etnej wagi","b",
                          "zero"})
              ]);



//dostepne szczegolne cechy dotyczace budowy ciala
// (w zastepstwie opisu wagi) dla krasnoludki:
mapping cialo_krasnoludki = ([
    "szerokie barki":   ({"barczysty","barczy^sci","barczyst^a","s","osiem"}),
    "bary^lkowato^s^c": ({"bary^lkowaty","bary^lkowaci","bary^lkowat^a","b","osiem"}),
    "beczkowato^s^c":  ({"beczkowaty","beczkowaci","beczkowat^a","b","siedem"}),
    "poka^xny brzuch": ({"brzuchaty","brzuchaci","brzuchat^a","b","siedem"}),
    "szczup^la budowa cia^la": ({"szczup^ly","szczupli","szczup^l^a","z","szesc"}),
    "chuda budowa cia^la": ({"chudy","chudzi","chud^a","z","siedem"}),
    "lekka budowa cia^la": ({"lekki","lekcy","lekk^a","z","szesc"}),
    "gruba budowa cia^la": ({"gruby","grubi","grub^a","w","siedem"}),
    "oty^lo^s^c": ({"oty^ly","otyli","oty^l^a","w","siedem"}),
    "niepozorna budowa cia^la":({"niepozorny","niepozorni","niepozorn^a","b","osiem"}),
    "pulchna budowa cia^la": ({"pulchny","pulchni","pulchn^a","w","piec"}),
    "t^luste cia^lo":     ({"t^lusty","t^lu^sci","t^lust^a","w","siedem"}),
    "kr^epa budowa cia^la": ({"kr^epy","kr^epi","kr^ep^a","s","piec"}),
    "puszysto^s^c": ({"puszysty","puszy^sci","puszyst^a","w","szesc"}),
    //KURDE, musi byc cos 'przecietnego' :P
    "przeci^etna waga":  ({"przeci^etnej wagi","przeci^etnych wag","przeci^etnej wagi","b",
                          "zero"})
              ]);
//dostepne szczegolne cechy dostyczace budowy ciala
//(w zastepstwie opisu wagi) dla krasnoluda:
mapping cialo_krasnoluda = ([
    "masywna postura":  ({"masywny","masywni","masywnym","w","piec"}),
    "poka^xne musku^ly":  ({"muskularny","muskularni","muskularnym","s","szesc"}),
    "szerokie barki":   ({"barczysty","barczy^sci","barczystym","s","cztery"}),
    "bary^lkowato^s^c": ({"bary^lkowaty","bary^lkowaci","bary^lkowatym","b","szesc"}),
    "beczkowato^s^c":  ({"beczkowaty","beczkowaci","beczkowatym","b","szesc"}),
    "poka^xny brzuch": ({"brzuchaty","brzuchaci","brzuchatym","b","piec"}),
    "chuda budowa cia^la": ({"chudy","chudzi","chudym","z","siedem"}),
    "ci^e^zka postura":  ({"ci^e^zki","ci^e^zcy","ci^e^zkim","w","szesc"}),
    "lekka budowa cia^la": ({"lekki","lekcy","lekkim","z","siedem"}),
    "gruba budowa cia^la": ({"gruby","grubi","grubym","w","szesc"}),
    "oty^lo^s^c": ({"oty^ly","otyli","oty^lym","w","siedem"}),
    "niepozorna budowa cia^la": ({"niepozorny","niepozorni","niepozornym","b","siedem"}),
    "pulchna budowa cia^la": ({"pulchny","pulchni","pulchnym","w","piec"}),
    "zwalista postura": ({"zwalisty","zwali^sci","zwalistym","w","siedem"}),
    "t^luste cia^lo":     ({"t^lusty","t^lu^sci","t^lustym","w","siedem"}),
    "poka^xne mi^e^snie":  ({"umi^e^sniony","umi^e^snieni","umi^e^snionym","s","piec"}),
    "kr^epa budowa cia^la": ({"kr^epy","kr^epi","kr^epym","s","cztery"}),
    "puszysto^s^c": ({"puszysty","puszy^sci","puszystym","w","szesc"}),
    "przeci^etna waga":  ({"przeci^etnej wagi","przeci^etnej wagi","przeci^etnej wagi","b",
                          "zero"})
              ]);

//dostepne szczegolne cechy dotyczace budowy ciala
// (w zastepstwie opisu wagi) dla gnomki:
mapping cialo_gnomki = ([
    "poka^xny brzuch": ({"brzuchaty","brzuchaci","brzuchat^a","b","siedem"}),
    "szczup^la budowa cia^la": ({"szczup^ly","szczupli","szczup^l^a","z","piec"}),
    "chuda budowa cia^la": ({"chudy","chudzi","chud^a","z","szesc"}),
    "lekka budowa cia^la": ({"lekki","lekcy","lekk^a","z","piec"}),
    "oty^lo^s^c": ({"oty^ly","otyli","oty^l^a","w","osiem"}),
    "w^at^la budowa cia^la": ({"w^at^ly","w^atli","w^at^lym","b","szesc"}),
    "niepozorna budowa cia^la":({"niepozorny","niepozorni","niepozorn^a","b","szesc"}),
    "pulchna budowa cia^la": ({"pulchny","pulchni","pulchn^a","w","siedem"}),
    "kr^epa budowa cia^la": ({"kr^epy","kr^epi","kr^ep^a","s","siedem"}),
    "puszysto^s^c": ({"puszysty","puszy^sci","puszyst^a","w","siedem"}),
    "drobna budowa cia^la": ({"drobny","drobni","drobn^a","b","piec"}),
    "filigranowa budowa cia^la": ({"filigranowy","filigranowi","filigranow^a","b","szesc"}),
    "^zylasta budowa cia^la": ({"^zylasty","^zyla^sci","^zylast^a","b","szesc"}),
    //KURDE, musi byc cos 'przecietnego' :P
    "proporcjonalna budowa cia^la": ({"proporcjonalny","proporcjonalni",
                                     "proporcjonaln^a","b","zero"}),
    "przeci^etna waga":  ({"przeci^etnej wagi","przeci^etnej wagi","przeci^etnej wagi","b",
                          "zero"})
              ]);
//dostepne szczegolne cechy dostyczace budowy ciala
//(w zastepstwie opisu wagi) dla gnoma:
mapping cialo_gnoma = ([
    "masywna postura":  ({"masywny","masywni","masywnym","w","osiem"}),
    "poka^xne musku^ly":  ({"muskularny","muskularni","muskularnym","s","siedem"}),
    "szerokie barki":   ({"barczysty","barczy^sci","barczystym","s","siedem"}),
    "bary^lkowato^s^c": ({"bary^lkowaty","bary^lkowaci","bary^lkowatym","b","szesc"}),
    "beczkowato^s^c":  ({"beczkowaty","beczkowaci","beczkowatym","b","siedem"}),
    "poka^xny brzuch": ({"brzuchaty","brzuchaci","brzuchatym","b","siedem"}),
    "chuda budowa cia^la": ({"chudy","chudzi","chudym","z","szesc"}),
    "lekka budowa cia^la": ({"lekki","lekcy","lekkim","z","szesc"}),
    "gruba budowa cia^la": ({"gruby","grubi","grubym","w","siedem"}),
    "oty^lo^s^c": ({"oty^ly","otyli","oty^lym","w","siedem"}),
    "niepozorna budowa cia^la": ({"niepozorny","niepozorni","niepozornym","b","szesc"}),
    "pulchna budowa cia^la": ({"pulchny","pulchni","pulchnym","w","szesc"}),
    "w^at^la budowa cia^la": ({"w^at^ly","w^atli","w^at^lym","b","szesc"}),
    "zwalista postura": ({"zwalisty","zwali^sci","zwalistym","w","osiem"}),
    "t^luste cia^lo":     ({"t^lusty","t^lu^sci","t^lustym","w","osiem"}),
    "poka^xne mi^e^snie":  ({"umi^e^sniony","umi^e^snieni","umi^e^snionym","s","siedem"}),
    "kr^epa budowa cia^la": ({"kr^epy","kr^epi","kr^epym","s","siedem"}),
    "puszysto^s^c": ({"puszysty","puszy^sci","puszystym","w","siedem"}),
    "^zylasta budowa cia^la": ({"^zylasty","^zyla^sci","^zylastym","b","szesc"}),
    "proporcjonalna budowa cia^la": ({"proporcjonalny","proporcjonalni",
                                     "proporcjonalnym","b","zero"}),
    "przeci^etna waga":  ({"przeci^etnej wagi","przeci^etnej wagi","przeci^etnej wagi","b",
                          "zero"})
              ]);





//cechy 'szczegolne' dla kobiet (kazdej rasy):
mapping cechy_szczegolne_kobiet= ([
      "garb":        ({"garbaty","garbaci","garbat^a","noelf"}),
      //KULAWY^a
      "poka^xne piersi": ({"piersiasty","piersia^sci","piersiast^a","noelf"}),
      "lekki garb": ({"przygarbiony","przygarbieni","przygarbion^a","noelf"}),
      "ko^slawe nogi": ({"ko^slawy","ko^slawi","ko^slaw^a","noelf"}),
      "kr^otkie nogi": ({"kr^otkonogi","kr^otkonodzy","kr^otkonog^a","noelf"}),
      "krzywe nogi":  ({"krzywonogi","krzywonodzy","krzywonog^a","noelf"}),
      "grube nogi":  ({"grubonogi","grubonodzy","grubonog^a","noelf"}),
      "d^lugie nogi": ({"d^lugonogi","d^lugonodzy","d^lugonog^a"}),
      "d^lugie r^ece": ({"d^lugor^eki","d^lugor^ecy","d^lugor^ek^a","noelf"}),
      "blada sk^ora":  ({"bladosk^ory","bladosk^orzy","bladosk^or^a"}),
      "ciemna sk^ora": ({"ciemnosk^ory","ciemnosk^orzy","ciemnosk^or^a","noelf"}),
      "czarna sk^ora": ({"czarnosk^ory","czarnosk^orzy","czarnosk^or^a","noelf"}),
      "czerwona sk^ora": ({"czerwonosk^ory","czerwonosk^orzy","czerwonosk^or^a","noelf"}),
      //TE CHYBA NIE SA 'szczegolne' :)
      //"jasna sk^ora":  ({"jasnosk^ory","jasnosk^orzy","jasnosk^or^a"}),
      //"smag^la sk^ora":  ({"smag^ly","smagli","smag^l^a"}),
      //"^aniada sk^ora":  ({"^sniady","^sniadzi","^sniad^a"}),
      "^z^o^lta sk^ora":   ({"^z^o^ltosk^ory","^z^o^ltosk^orzy","^z^o^ltosk^or^a","noelf"}),
      //TWARZ:
      "brak uz^ebienia": ({"bezz^ebny","bezz^ebni","bezzebn^a","noelf"}),
      "czerwony nos":   ({"czerwononosy","czerwononosi","czerwononos^a","noelf"}),
      "d^lugi nos":     ({"d^lugonosy","d^lugonosi","d^lugonos^a","noelf"}),
      "d^lugie uszy":   ({"d^lugouchy","d^lugousi","d^lugouch^a"}),
      "kostropato^s^c":  ({"kostropaty","kostropaci","kostropat^a","noelf"}),
      "krzywy nos":    ({"krzywonosy","krzywonosi","krzywonos^a","noelf"}),
      "ogorza^la twarz": ({"ogorza^ly","ogorzali","ogorza^l^a","noelf"}),
      "ostry nos":     ({"ostronosy","ostronosi","ostronos^a"}),
      "ostre uszy":    ({"ostrouchy","ostrousi","ostrouch^a"}),
      "liczne piegi":  ({"piegowaty","piegowaci","piegowat^a"}),
      "pyzata twarz":  ({"pyzaty","pyzaci","pyzat^a","noelf"}),
      "rumiana twarz": ({"rumiany","rumiani","rumian^a","noelf"}),
      "wielki nos":    ({"wielkonosy","wielkonosi","wielkonos^a","noelf"}),
      "wielkie uszy":  ({"wielkouchy","wielkousi","wielkouch^a"})
              ]);

//cechy 'szczegolne' dla mezczyzn (kazdej rasy):
mapping cechy_szczegolne_mezczyzn= ([
      "garb":        ({"garbaty","garbaci","garbatym","noelf"}),
      "lekki garb": ({"przygarbiony","przygarbieni","przygarbionym","noelf"}),
      "ko^slawe nogi": ({"ko^slawy","ko^slawi","ko^slawym","noelf"}),
      "kr^otkie nogi": ({"kr^otkonogi","kr^otkonodzy","kr^otkonogim","noelf"}),
      "krzywe nogi":  ({"krzywonogi","krzywonodzy","krzywonogim","noelf"}),
      "grube nogi":  ({"grubonogi","grubonodzy","grubonogim","noelf"}),
      "d^lugie nogi": ({"d^lugonogi","d^lugonodzy","d^lugonogim"}),
      "d^lugie r^ece": ({"d^lugor^eki","d^lugor^ecy","d^lugor^ekim"}),
      "blada sk^ora":  ({"bladosk^ory","bladosk^orzy","bladosk^orym"}),
      "ciemna sk^ora": ({"ciemnosk^ory","ciemnosk^orzy","ciemnosk^orym","noelf"}),
      "czarna sk^ora": ({"czarnosk^ory","czarnosk^orzy","czarnosk^orym","noelf"}),
      "czerwona sk^ora": ({"czerwonosk^ory","czerwonosk^orzy","czerwonosk^orym","noelf"}),
      //TE CHYBA NIE SA 'szczegolne' :)
      //"jasna sk^ora":  ({"jasnosk^ory","jasnosk^orzy","jasnosk^orym"}),
      //"smag^la sk^ora":  ({"smag^ly","smagli","smag^lym"}),
      //"^sniada sk^ora":  ({"^sniady","^sniadzi","^sniadym"}),
      "^z^o^lta sk^ora":   ({"^z^o^ltosk^ory","^z^o^ltosk^orzy","^z^o^ltosk^orym","noelf"}),
      //TWARZ:
      "brak uz^ebienia": ({"bezz^ebny","bezz^ebni","bezz^ebnym","noelf"}),
      "czerwony nos":   ({"czerwononosy","czerwononosi","czerwononosym","noelf"}),
      "d^lugi nos":     ({"d^lugonosy","d^lugonosi","d^lugonosym"}),
      "d^lugie uszy":   ({"d^lugouchy","d^lugousi","d^lugouchym"}),
      "kostropato^s^c":  ({"kostropaty","kostropaci","kostropatym","noelf"}),
      "krzywy nos":    ({"krzywonosy","krzywonosi","krzywonosym","noelf"}),
      "ogorza^la twarz": ({"ogorza^ly","ogorzali","ogorza^lym","noelf"}),
      "ostry nos":     ({"ostronosy","ostronosi","ostronosym"}),
      "ostre uszy":    ({"ostrouchy","ostrousi","ostrouchym"}),
      "liczne piegi":  ({"piegowaty","piegowaci","piegowatym"}),
      "pyzata twarz":  ({"pyzaty","pyzaci","pyzatym","noelf"}),
      "rumiana twarz": ({"rumiany","rumiani","rumianym","noelf"}),
      "wielki nos":    ({"wielkonosy","wielkonosi","wielkonosym","noelf"}),
      "wielkie uszy":  ({"wielkouchy","wielkousi","wielkouchym"})
              ]);

//tablica pochodzenia jest wspolna dla kazdej rasy.
//UWAGA, jak kto^a tu b^edzie chcia^l co^s dopisa^c, to prosz^e te^z
//zerkn^a^c na funkcje powiedz_pochodzenie() w std!!!
/*UWAGA2!!! Zakomentowa^lem miejsca inne ni^z wsie i miasta dost^epne
 ka^zdym. Ju^z nie b^edzie mo^zna wybra^c np. G^or Sinych, albo Mahakamu -
 takie pochodzenie ^swiadczy o jakim^s ju^z statucie, nie mo^ze by^c tak
 ^ze pierwszy lepszy elf, czy krasnolud mo^ze pochodzi^c z takich miejsc
 (analogicznie - Tir Tochair dla gnom^ow). Je^sli chce, niech z^lo^zy
 podanie. Oczywi^scie o ile nie ma tego miejsca, gdzie mo^zna zdoby^c
 te pochodzenie!! :)
 Vera. PROSZ^E NIE EDYTOWA^C BEZ UZGODNIENIA TEGO ZE MNA
 */
string *pochodzenie=({
               "z Rinde",               "z Dol Blathanna",
               "z Lan Exeter",          "z Pont Vanis",
               "z Blaviken",            "z Creyden",
               "z Ho^lopola",            "z Tridam",
               "z Aedd Gynvael",        "z Rakvelerin",
               "z Gors Velen",          "z Hirundum",
               "z Yspaden",             "z Mirt",
               "z Ard Carraigh",        "z Daevon",
               "z Hengfors",            "z Crinfrid",
               "z Murivel",             "z Tretogoru",
               "z Oxenfurtu",           "z Novigradu",
               "z Grabowej Buchty",     "z Cidaris",
               "z Vole",                "z Wyzimy",
               "z Dorian",              "z Anchor",
               "z Piany",               "z Bia^lego Mostu",
               "z Burdorff",            "z Ellander",
               "z Zavady",              "z Mariboru",
               "z Carreras",            "z Hagge",
               "z Ban Glean",           "z Gulety",
               "z Aldersbergu",         "z Vengerbergu",
               "z Ban Ard",             "z Mahakamu",
               "z Rivii",               "z Lyrii",
               "z Brugge",              "z Dillingen",
               "z Nastrog",             "z Bremervoord",
               "z Ard Skellige",        "z Cintry",
               "z Tigg",                "z Kagen",
               "z Klucza",              "z Riedbrune",
               "z Beauclair",           "z Belhaven",
               "z Neunreuth",           "z Metinny",
               "z Maecht",              "z Amarillo",
               "z Claremont",           "z Thurn",
               "z Nilfgaardu",          "z G^or Sinych",
               "z Gelibolu",            "z Tir Tochair"
              });
/*oto lista umiejetnosci dostepnych dla wszystkich do wyboru.
 *jest umyslnie tak okrojona, by byly to tylko te umiejetnosci, ktore
 *potencjalnie moga dac od razu graczowi zawod. Tak wiec nie ma tu
 *zadnej wymyslnej listy ;)
 */
mapping lista_skilli = ([
               "miecze":                  SS_WEP_SWORD,
               "bronie drzewcowe":        SS_WEP_POLEARM,
               "topory":                  SS_WEP_AXE,
               "sztylety":                SS_WEP_KNIFE,
               "maczugi":                 SS_WEP_CLUB,
               "m^loty":                  SS_WEP_WARHAMMER,
               "bronie strzeleckie":      SS_WEP_MISSILE,
               "walka bez broni":         SS_UNARM_COMBAT,
               "parowanie":               SS_PARRY,
               "rzucanie":                SS_THROWING,
               "ocena przeciwnika":       SS_APPR_MON,
               "ocena obiektu":           SS_APPR_OBJ,
               "szacowanie":              SS_APPR_VAL,
//               "kieszonkostwo":           51,
               "znajomo^s^c j^ezyk^ow":   SS_LANGUAGE,
               "opieka nad zwierz^etami": SS_ANI_HANDL,
               "jazda konna":             SS_RIDING,
               "tropienie":               SS_TRACKING,
               "wyczucie kierunku":	  SS_LOC_SENSE,
               "p造wanie":	  	  SS_SWIM,
               "wspinaczka":		  SS_CLIMB,
               "targowanie si^e":         SS_TRADING,
               "muzykalno^s^c":             SS_MUSIC,
               "drwalstwo": SS_WOODCUTTING,
               "rybo堯stwo": SS_FISHING,
               "zielarstwo": SS_HERBALISM,
               "skradanie si�": SS_SNEAK,
               "ukrywanie": SS_HIDE
                        
               
               ]);

//kolory zarostu sa do wyboru tylko wtedy, gdy ktos zechce
//byc lysy permanentnie. W przeciwnym wypadku, kolor zarostu
//jest podobny do koloru wlosow.
string *kolory_zarostu = ({
              "bia^le",
              "czarne",
              "ciemne",
              "czerwone",
              "jasne",
              "kruczoczarne",
              "rude",
              "siwe",
              "popielate"
              });


mapping dlugosc_wasow = ([
               "brak":0.1,              "kr^otkie":0.3,
              "^sredniej d^lugo^sci":0.5,  "d^lugie":1.0
               ]);
mapping dlugosc_brody = ([
               "brak":0.1,       "kr^otk^a":3.1,
               "si^egaj^ac^a szyi":8.1, "si^egaj^ac^a piersi":19.1,
               "d^lug^a": 19.1,
               "si^egaj^ac^a pasa":40.1
               ]);

//ograniczenia w wyborze wieku.
mapping DOLNA_GRANICA_WIEKU = ([
            "cz^lowiek" :    14,
            "elf"        :    20,
            "p^o^lelf"  :    14,
            "krasnolud" :    19,
            "nizio^lek" :   14,
                        "gnom"      :   18
                        ]);
mapping GORNA_GRANICA_WIEKU = ([
            "cz^lowiek" :    70,
            "elf"        :    300,
            "p^o^lelf"  :    70,
            "krasnolud" :    90,
            "nizio^lek" :   80,
                        "gnom"      :   85
                        ]);

/* A tu jest taka sprytna mala funkcja, ktora w wiosce zastepuje funkcje 'koncowka'.
 * Dlatego, ze z koncowka dla bytow astralnych byly same klopoty :/
 */
string koncoweczka(string m, string k)
{
    if(this_player()->query_gender()==1)
      return k;
    else
      return m;
}

/*
void
//filtrowane_atrybuty(string str)
filtrowane_atrybuty()
{
    string *arr;
    object kto=this_player(); //???

    arr = m_indices(this_player()->query_gender()==1^acialo_kobiety:cialo_mezczyzny);
    arr = filter(arr, &call_other("/d/Standard/login/wioska/atrybuty", ,kto) @ &operator(+)("p",) @ &implode(,"_") @ &explode(," ") @ plain_string);

    dostepne_cechy_ciala=arr;
   // write(COMPOSITE_WORDS(arr));


  //str=filter(m_indices(this_player()->query_gender()==1^acialo_kobiety:cialo_mezczyzny),
    //         &call_other("/d/Standard/login/wioska/atrybuty.c", ,kto) @ &operator(+)("p",) @ &implode(,"_") @ &explode(," ") @ plain_string);
}
*/





/* Funkcje obliczajace czy dany przymiotnik jest mozliwy
 * do wyboru (dzieki zadeklarowanej wczesniej wadze i wzroscie)
 *
 *POZIOMY TO:     WAGA            WZROST
 *    0         bardzo chud        niespotykanie nisk
 *    1         chud               bardzo nisk
 *    2         szczupl            nisk
 *    3         przecietnej w.     przecietnego wzrostu
 *    4         lekko otyl         wysok
 *    5         otyl               bardzo wysok
 *    6         bardzo otyl        niespotykanie wysok
 */

int
pszerokie_barki(object kto, int waga, int wzrost)
{
   if(waga>=3)
    return 1;
   else
    return 0;

}
int
pbarylkowatosc(object kto, int waga, int wzrost)
{
   if(waga>=4)
    return 1;
   else
    return 0;

}
pbeczkowatosc(object kto, int waga, int wzrost)
{
   if(waga>=4)
    return 1;
   else
    return 0;

}
int
ppokazny_brzuch(object kto, int waga, int wzrost)
{
   if(waga>=2)
    return 1;
   else
    return 0;

}
int
pszczupla_budowa_ciala(object kto, int waga, int wzrost)
{
   if(waga==1 || waga==2)
    return 1;
   else
    return 0;

}
int
pchuda_budowa_ciala(object kto, int waga, int wzrost)
{
   if(waga<=2)
    return 1;
   else
    return 0;

}
int
pciezka_postura(object kto, int waga, int wzrost)
{
   if(waga>=4)
    return 1;
   else
    return 0;

}
int
pkoscistosc(object kto, int waga, int wzrost)
{
   if(waga<=2)
    return 1;
   else
    return 0;

}
int
plekka_budowa_ciala(object kto, int waga, int wzrost)
{
   if(waga<=2)
    return 1;
   else
    return 0;

}
int
pgruba_budowa_ciala(object kto, int waga, int wzrost)
{
   if(waga>=4)
    return 1;
   else
    return 0;

}
int
potylosc(object kto, int waga, int wzrost)
{
   if(waga>=5)
    return 1;
   else
    return 0;

}
int
pniepozorna_budowa_ciala(object kto, int waga, int wzrost)
{
   if(waga<=3)
    return 1;
   else
    return 0;

}
int
ppulchna_budowa_ciala(object kto, int waga, int wzrost)
{
   if(waga>=4)
    return 1;
   else
    return 0;

}
int
psmukla_budowa_ciala(object kto, int waga, int wzrost)
{
   if(waga<=3)
    return 1;
   else
    return 0;

}
int
pwatla_budowa_ciala(object kto, int waga, int wzrost)
{
   if(waga<=2)
    return 1;
   else
    return 0;

}
int
pwielka_postura(object kto, int waga, int wzrost)
{
   if(waga>=3 && wzrost>=4)
    return 1;
   else
    return 0;

}
int
pzwalista_postura(object kto, int waga, int wzrost)
{
   if(waga>=4 && wzrost>=3)
    return 1;
   else
    return 0;

}
int
ptluste_cialo(object kto, int waga, int wzrost)
{
   if(waga>=4)
    return 1;
   else
    return 0;

}

int
pkrepa_budowa_ciala(object kto, int waga, int wzrost)
{
   if(waga>=3)
        return 1;
   else
        return 0;
}

int
pprzecietna_waga(object kto, int waga, int wzrost)
{
   if(waga==3)
        return 1;
   else
        return 0;
}

int
pmasywna_postura(object kto, int waga, int wzrost)
{
   if(waga>=4 && wzrost>=2)
        return 1;
   else
        return 0;
}

int
ppokazne_muskuly(object kto, int waga, int wzrost)
{
   if(waga>=2)
        return 1;
   else
        return 0;
}

int
ppokazne_miesnie(object kto, int waga, int wzrost)
{
   if(waga>=3)
        return 1;
   else
        return 0;
}

int
ppuszystosc(object kto,int waga, int wzrost)
{
   if(waga>=3)
        return 1;
   else
        return 0;
}

int
pdrobna_budowa_ciala(object kto,int waga, int wzrost)
{
   if(waga<=2)
        return 1;
   else
        return 0;
}

int
pfiligranowa_budowa_ciala(object kto,int waga, int wzrost)
{
   if(waga<=2)
        return 1;
   else
        return 0;
}
int
pzylasta_budowa_ciala(object kto,int waga, int wzrost)
{
   if(waga<=2)
        return 1;
   else
        return 0;
}
int
pproporcjonalna_budowa_ciala(object kto,int waga, int wzrost)
{
   if(waga==3)
        return 1;
   else
        return 0;
}
