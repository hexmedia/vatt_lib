/*
 *  Stale wykorzystywane przez procedure logowania sie postaci.
 *
 *  /d/Standard/login/wioska/login.h
 */

#include <pl.h>

/*
 * PATH
 *
 * The path in which the login rooms reside.
 */
#define PATH "/d/Standard/login/"

/*
 * LOGIN_FILE_NEW_PLAYER_INFO
 *
 * This is the names of the files that are written to new users logging in.
 *
#define LOGIN_FILE_NEW_PLAYER_INFO "/d/Standard/doc/login/NEW_PLAYER_INFO"*/

/*
 * RACEMAP
 *
 * This mapping holds the names of the allowed player races in the game and
 * which player file to use for new characters of a given race.
 */
#define RACEMAP ([ \
    "cz^lowiek"     : "/d/Standard/race/czlowiek_std",  \
    "elf"           : "/d/Standard/race/elf_std",       \
    "p^o^lelf"      : "/d/Standard/race/polelf_std",    \
    "krasnolud"     : "/d/Standard/race/krasnolud_std", \
    "nizio^lek"     : "/d/Standard/race/niziolek_std",  \
    "gnom"          : "/d/Standard/race/gnom_std",      \
                ])

/*
 * RACEATTR
 *
 * Mapping z atrybutami standardowych ras. Pierwszy element tablicy
 * odpowiada plci meskiej, drugi - zenskiej. Kolejne pola to:
 *	sredni wzrost		(cm)
 *	srednia waga		(kg)
 *      srednia gestosc liniowa	(g/cm) = srednia waga / sredni wzrost
 *
 * Przyjmujemy, ze ciezar wlasciwy wszystkich ras jest rowny ciezarowi
 * wlasciwemu wody.
 */
#define RACEATTR ([ \
	"cz^lowiek"	: ({({175,  75, 429}), ({165,  60, 364})}),	\
	"elf"		: ({({190,  60, 316}), ({185,  50, 270})}),	\
	"p^o^lelf"	: ({({175,  75, 429}), ({165,  60, 364})}),	\
	"krasnolud"	: ({({140,  80, 500}), ({140,  66, 429})}),	\
	"nizio^lek"	: ({({120,  55, 458}), ({115,  50, 435})}),	\
	"gnom"		: ({({120,  50, 417}), ({110,  45, 409})}),	\
                ])

/*
 * RACESTATMOD
 *
 * This mapping holds the standard modifiers of each stat, i.e. a dwarf
 * should have it easier to raise con than other races, but get a harder
 * time raising its int.
 *
 *	race		:  str, dex, con, int, wis, dis, race, occ, lay
 */
#define RACESTATMOD ([ \
	"cz^lowiek"	: ({10,  10,  10,  10,  10,  10, 10, 10, 10}),	\
	"elf"		: ({ 7,  13,   8,  13,  12,   8, 10, 10, 10}),	\
	"p^o^lelf"	: ({10,  10,  10,  10,  10,  10, 10, 10, 10}),	\
	"krasnolud"	: ({12,   6,  13,   8,   9,  12, 10, 10, 10}),	\
	"nizio^lek"	: ({ 7,  17,   8,  11,  10,   8, 10, 10, 10}),	\
	"gnom"		: ({ 8,  14,   7,  14,   9,   9, 10, 10, 10}),	\
                    ])

/*
 * Wartosci z Genesis, dla porownania.
 *
		"elf"    : ({ 7, 12, 8, 15, 14, 9, 10, 10, 10 }), \
                "dwarf"  : ({ 15, 5, 15, 8, 9, 13, 10, 10, 10 }), \
                "hobbit" : ({ 6, 22, 8, 10, 11, 9, 10, 10, 10 }), \
                "gnome"  : ({ 9, 14, 7, 19, 10, 8, 10, 10, 10 }), \
                "goblin" : ({ 18, 10, 16, 6, 10, 6, 10, 10, 10 }), \
 */

/*
 * Odmiana ras. (zmienic, by korzystac z <pl.h>.)
 */
#define ODMIANA_RASY ([ \
        "cz^lowiek"  : 	({ ({ "cz^lowiek", "cz^lowieka", "cz^lowiekowi",\
			"cz^lowieka", "cz^lowiekiem", "cz^lowieku" }),\
			({ "cz^lowiek", "cz^lowieka", "cz^lowiekowi",\
			"cz^lowieka", "cz^lowiekiem", "cz^lowieku" }) }),\
	"elf"       :	({ ({ "elf", "elfa", "elfowi", "elfa", "elfem",\
			"elfie" }), ({ "elfka", "elfki", "elfce", "elfk^e",\
			"elfk^a", "elfce" }) }),\
	"p^o^lelf"  : 	({ ({ "p^o^lelf", "p^o^lelfa", "p^o^lelfowi",\
			"p^o^lelfa", "p^o^lelfem", "p^o^lelfie" }),\
			({ "p^o^lelfka", "p^o^lelfki", "p^o^lelfce",\
			"p^o^lelfke", "p^o^lelfk^a", "p^o^lelfce" }) }),\
	"krasnolud" :	({ ({ "krasnolud", "krasnoluda", "krasnoludowi",\
			"krasnoluda", "krasnoludem", "krasnoludzie" }),\
			({ "krasnoludka", "krasnoludki", "krasnoludce",\
			"krasnoludk^e", "krasnoludk^a", "krasnoludce" }) }),\
	"nizio^lek"    : ({ ({ "nizio^lek", "nizio^lka", "nizio^lkowi",	\
			"nizio^lka", "nizio^lkiem", "nizio^lku" }),	\
			({ "nizio^lka", "nizio^lki", "nizio^lce",	\
			"nizio^lk^e", "nizio^lk^a", "nizio^lce" }) }),	\
	"gnom"         : ({ ({ "gnom", "gnoma", "gnomowi",	\
			"gnoma", "gnomem", "gnomie" }),	\
			({ "gnomka", "gnomki", "gnomce",	\
			"gnomk^e", "gnomk^a", "gnomce" }) })	\
			])

#define ODMIANA_PRASY  ([\
	"cz^lowiek"  :	({ ({ "ludzie", "ludzi", "ludziom", "ludzi",\
			"lud^xmi", "ludziach" }), ({ "ludzie", "ludzi",\
			"ludziom", "ludzi", "lud^xmi", "ludziach" }) }),\
	"elf"       :	({ ({ "elfy", "elf^ow", "elfom", "elfy", "elfami",\
			"elfach" }), ({ "elfki", "elfek", "elfkom", "elfki",\
			"elfkami", "elfkach" }) }),\
	"p^o^lelf"  :	({ ({ "p^o^lelfy", "p^o^lelf^ow", "p^o^lelfom", "p^o^lelfy", "p^o^lelfami",\
			"p^o^lelfach" }), ({ "p^o^lelfki", "p^o^lelfek", "p^o^lelfkom", "p^o^lelfki",\
			"p^o^lelfkami", "p^o^lelfkach" }) }),\
	"krasnolud" :	({ ({ "krasnoludy", "krasnolud^ow", "krasnoludom",\
			"krasnoludy", "krasnoludami", "krasnoludach" }),\
			({ "krasnoludki", "krasnoludek", "krasnoludkom",\
			"krasnoludki", "krasnoludkami", "krasnoludkach" }) }),\
	"nizio^lek" :	({ ({ "nizio^lki", "nizio^lk^ow", "nizio^lkom",	\
			"nizio^lki", "nizio^lkami", "nizio^lkach" }),	\
			({ "nizio^lki", "nizio^lek", "nizio^lkom",	\
			"nizio^lki", "nizio^lkami", "nizio^lkach" }) }),\
	"gnom"	    :	({ ({ "gnomy", "gnom^ow", "gnomom",	\
			"gnomy", "gnomami", "gnomach" }),	\
			({ "gnomki", "gnomek", "gnomkom",	\
			"gnomki", "gnomkami", "gnomkach" }) })	\
			])
#define ODMIANA_RASY_OSOBNO ([\
			"cz^lowiek" :	1,\
			"elf"	    :	0,\
			"p^o^lelf"  :	0,\
			"krasnolud" :	0,\
			"nizio^lek" :   0,\
			"gnom"	    :   0,\
			])
#define ODMIANA_RASY_RODZAJ ([\
			"cz^lowiek" 	: ({ PL_MESKI_OS, PL_ZENSKI }),	\
			"elf"		: ({ PL_MESKI_NOS_ZYW, PL_ZENSKI }),\
			"p^o^lelf"	: ({ PL_MESKI_NOS_ZYW, PL_ZENSKI }),\
			"krasnolud"	: ({ PL_MESKI_NOS_ZYW, PL_ZENSKI }),\
			"nizio^lek"	: ({ PL_MESKI_NOS_ZYW, PL_ZENSKI }),\
			"gnom"		: ({ PL_MESKI_NOS_ZYW, PL_ZENSKI }),\
			"bezkszta^ltny"	: ({ PL_MESKI_NOS_ZYW, PL_ZENSKI }),\
			])

/*
 * when m_indicex work on constants: m_indices(RACEMAP)
 */
#define RACES ({"cz^lowiek", "elf", "p^o^lelf", "krasnolud", "nizio�ek", "gnom"})

#define RACES_SHORT ([ \
	"cz^lowiek"	: "czl",	\
	"elf"		: "elf",	\
	"p^o^lelf"	: "pol",	\
	"krasnolud"	: "kra",	\
	"nizio^lek"	: "niz",	\
	"gnom"		: "gno",	\
                    ])

/*
 * RACESTART
 *
 * This mapping holds the files of the starting locations for each race.
 */
#define RACESTART ([ \
	"cz^lowiek"	: ({ "/d/Standard/Redania/Rinde/Polnoc/Karczma/lokacje/start", }),	\
	"elf"		: ({ "/d/Standard/Redania/Rinde/Polnoc/Karczma/lokacje/start", }),	\
	"p^o^lelf"	: ({ "/d/Standard/Redania/Rinde/Polnoc/Karczma/lokacje/start", }),	\
	"krasnolud"	: ({ "/d/Standard/Redania/Rinde/Polnoc/Karczma/lokacje/start", }),	\
	"nizio^lek"	: ({ "/d/Standard/Redania/Rinde/Polnoc/Karczma/lokacje/start", }),	\
	"gnom"		: ({ "/d/Standard/Redania/Rinde/Polnoc/Karczma/lokacje/start", }),	\
                  ])

/*
 * RACEPOST
 *
 * This mapping holds the files of the post offices locations for each race.
 */
#define RACEPOST ([ \
	"cz^lowiek"	: "/d/Standard/start/mailroom",	\
	"elf"		: "/d/Standard/start/mailroom",	\
	"p^o^lelf"	: "/d/Standard/start/mailroom",	\
	"krasnolud"	: "/d/Standard/start/mailroom",	\
	"nizio^lek"	: "/d/Standard/start/mailroom",	\
	"gnom"		: "/d/Standard/start/mailroom",	\
                 ])

/*
 * RACESTAT
 *
 * This mapping holds the stats that each race has on start
 */
#define RACESTAT ([ \
	"cz^lowiek"	: ({10,  10,  10,  10,  10,  10, 10, 10, 10}),	\
	"elf"		: ({ 7,  13,   8,  13,  12,   8, 10, 10, 10}),	\
	"p^o^lelf"	: ({10,  10,  10,  10,  10,  10, 10, 10, 10}),	\
	"krasnolud"	: ({12,   6,  13,   8,   9,  12, 10, 10, 10}),	\
	"nizio^lek"	: ({ 7,  17,   8,  11,  10,   8, 10, 10, 10}),	\
	"gnom"		: ({ 8,  14,   7,  14,   9,   9, 10, 10, 10}),	\
                    ])
#if 0
#define RACESTAT ([      /* str, dex, con, int, wis, dis */	\
	"cz^lowiek"	: ({  9,   9,   9,   9,   9,   9}),	\
	"elf"		: ({  6,  11,   7,  12,  11,   7}),	\
	"krasnolud"	: ({ 11,   5,  12,   7,   8,  11}),	\
	"nizio^lek"	: ({  6,  15,   7,  10,   9,   7}),	\
	"gnom"		: ({  7,  13,   6,  13,   8,   7}),	\
	"ogr"		: ({ 15,   5,  13,   3,   4,  12}),	\
                 ])
#endif

/*
 * RACESKILL
 *
 * Poczatkowe umiejetnosci roznych ras.
 */

#define RACESKILL ([ \
	"cz^lowiek"	: ([		\
		SS_THROWING	: 609, \
		SS_WEP_SWORD	: 604,	\
		SS_PARRY	: 604,	\
		SS_APPR_VAL	: 603,	\
		SS_SWIM		: 603,	\
		SS_TRADING	: 605,	\
		/* 64 + 51 + 13 + 13 + 62 = 203 cc ???????????*/\
                          ]),		\
	"elf"		: ([		\
		SS_WEP_KNIFE	: 603,	\
		SS_THROWING	: 609, \
		SS_DEFENCE	: 605,	\
		SS_HERBALISM	: 603,	\
		SS_SNEAK	: 603,	\
		SS_LOC_SENSE	: 603,	\
		SS_TRACKING	: 603,	\
		/* 29 + 100 + 18 + 18 + 13 + 13 = 201 cc */\
                          ]),		\
	"p^o^lelf"	: ([		\
		SS_WEP_SWORD	: 604,	\
		SS_THROWING	: 609, \
		SS_PARRY	: 604,	\
		SS_APPR_VAL	: 603,	\
		SS_SWIM		: 603,	\
		SS_TRADING	: 605,	\
		/* 64 + 51 + 13 + 13 + 62 = 203 cc */\
                          ]),		\
	"krasnolud"	: ([		\
		SS_WEP_AXE	: 605,	\
		SS_THROWING	: 609, \
		SS_PARRY	: 604,	\
		SS_APPR_MON	: 604,	\
		SS_APPR_VAL	: 604,	\
		/* 87 + 51 + 32 + 32 = 202 cc */\
                          ]),		\
	"nizio^lek"	: ([		\
		SS_THROWING	: 609, \
		SS_DEFENCE	: 604,	\
		SS_HERBALISM	: 605,	\
		SS_HIDE		: 603,	\
		SS_ANI_HANDL	: 602,	\
		/* 29 + 51 + 87 + 18 + 13 = 198 cc */\
                          ]),		\
	"gnom"		: ([		\
		SS_WEP_KNIFE	: 603,	\
		SS_THROWING	: 609, \
		SS_DEFENCE	: 603,	\
		SS_APPR_OBJ	: 603,	\
		SS_CLIMB	: 603,	\
		SS_TRADING	: 605,	\
		/* Nie zbalansowane jeszcze. Zrobi sie pozniej. */\
                          ]),		\
                  ])
/*
 * RACEMISCCMD
 *
 * This mapping holds the files of the souls that should be used as
 * misc command soul for each race
 */
#define RACEMISCCMD ([ \
	"cz^lowiek"	: "/d/Standard/cmd/misc_cmd_czlowiek",	\
	"elf"		: "/d/Standard/cmd/misc_cmd_elf",	\
	"p^o^lelf"	: "/d/Standard/cmd/misc_cmd_polelf",	\
	"krasnolud"	: "/d/Standard/cmd/misc_cmd_krasnolud",	\
	"nizio^lek"	: "/d/Standard/cmd/misc_cmd_niziolek",	\
	"gnom"		: "/d/Standard/cmd/misc_cmd_gnom",	\
                    ])

/*
 * RACESOULCMD
 *
 * This mapping holds the files of the souls that should be used as
 * standard command soul for each race
 */
#define RACESOULCMD ([ \
	"cz^lowiek"	: "/d/Standard/cmd/soul_cmd_czlowiek",	\
	"elf"		: "/d/Standard/cmd/soul_cmd_elf",	\
	"p^o^lelf"	: "/d/Standard/cmd/soul_cmd_polelf",	\
	"krasnolud"	: "/d/Standard/cmd/soul_cmd_krasnolud",	\
	"nizio^lek"	: "/d/Standard/cmd/soul_cmd_niziolek",	\
	"gnom"		: "/d/Standard/cmd/soul_cmd_gnom",	\
                    ])




/*
 * RACESOUND
 *
 * What sound do mainindex-race hear when subindex race speaks
 */
#define RACESOUND ([ \
	"cz^lowiek"	: ([			\
		"cz^lowiek"	: "m^owi",	\
		"elf"		: "m^owi",	\
		"p^o^lelf"	: "m^owi",	\
		"krasnolud"	: "szepce",	\
		"nizio^lek"	: "m^owi",	\
		"gnom"		: "syczy",	\
                          ]),			\
	"elf"		: ([			\
		"cz^lowiek"	: "^spiewa",	\
		"elf"		: "m^owi",	\
		"p^o^lelf"	: "m^owi",	\
		"krasnolud"	: "nuci",	\
		"nizio^lek"	: "^spiewa",	\
		"gnom"		: "nuci",	\
                          ]),			\
	"p^o^lelf"		: ([			\
		"cz^lowiek"	: "m^owi",	\
		"elf"		: "m^owi",	\
		"p^o^lelf"	: "m^owi",	\
		"krasnolud"	: "nuci",	\
		"nizio^lek"	: "^spiewa",	\
		"gnom"		: "nuci",	\
                          ]),			\
	"krasnolud"	: ([			\
		"cz^lowiek"	: "dudni",	\
		"elf"		: "mruczy",	\
		"p^o^lelf"	: "mruczy",	\
		"krasnolud"	: "m^owi",	\
		"nizio^lek"	: "dudni",	\
		"gnom"		: "huczy",	\
                          ]),			\
	"nizio^lek"	: ([			\
		"cz^lowiek"	: "nuci",	\
		"elf"		: "nuci",	\
		"p^o^lelf"	: "nuci",	\
		"krasnolud"	: "^spiewa",	\
		"nizio^lek"	: "m^owi",	\
		"gnom"		: "m^owi",	\
                          ]),			\
	"gnom"		: ([			\
		"cz^lowiek"	: "m^owi",	\
		"elf"		: "skrzypi",	\
		"p^o^lelf"	: "skrzypi",	\
		"krasnolud"	: "syczy",	\
		"nizio^lek"	: "skrzypi",	\
		"gnom"		: "m^owi",	\
                          ]),			\
                  ])

/*
 * HEIGHTDESC
 */
#define HEIGHTDESC(x)	({ "niespotykanie nisk" + x, "bardzo nisk" + x,	\
			   "nisk" + x, "przeci^etnego wzrostu",		\
			   "wysok" + x, "bardzo wysok" + x,		\
			   "niespotykanie wysok" + x })

/*
 * WIDTHDESC
 */
#define WIDTHDESC(x)	({ "bardzo chud" + x, "chud" + x,		\
			   "szczup^l" + x, "przeci^etnej wagi",		\
			   "lekko oty^l" + x, "oty^l" + x,		\
			   "bardzo oty^l" + x })

/*
 * HEIGHTPROC
 * WIDTHPROC
 *
 * Kazdy gracz powinien miec wzrost i wage (procentowo, w stosunku do
 * standardowego wzrostu i standardowej dla wzrostu gracza wagi) nie
 * mniejsza od pierwszego elementu tablicy i mniejsza od ostatniego.
 * Kolejne przedzialy odpowiadaja kolejnym opisom z HEIGHTDESC i
 * WIDTHDESC. Oznacza to zmiennosc wzrostu +/- 17% i wagi +/- 31%.
 */
#define HEIGHTPROC	({83, 88, 93, 98, 103, 108, 113, 118})
#define WEIGHTPROC	({69, 78, 87, 96, 105, 114, 123, 132})

#define LOGIN_NO_NEW	"/d/Standard/login/no_new_players"
#define CONVERT_OLD	"/d/Standard/login/convert_old_players"

/*
 * This is the current state of this ghost (uses set_ghost() / query_ghost())
 */
#define GP_INTRO        1
#define GP_BODY		2
#define GP_EMAIL	4
#define GP_FEATURES	8
#define GP_SKILLS       16
#define GP_NEW          (GP_BODY | GP_EMAIL | GP_FEATURES | GP_SKILLS)
#define GP_CONVERT      32
#define GP_DEAD         64

/* dodane ze wzgl�du na rozmiary
 *
 * Mapping ze wsp�czynnikami standardowych ras. Pierwszy element tablicy
 * odpowiada p�ci m�skiej, drugi - �e�skiej, trzeci to odniesienie.
 * Kolejne pola dw�ch pierwszych element�w to kolejne rozmiary jak w pliku
 * /std/player/rozmiary
 *
 * Przyk�adowy punkt odniesienia to:
 *  - rasa: cz�owiek
 *  - wzrost: 179 cm
 *  - waga: 65 kg
 *  - rozmiary: ({59.0,37.0,104.0,30.0,28.0,28.0,25.0,17.0,20.0,11.0,\
 *                60.0,99.0,80.0,92.0,50.0,51.0,40.0,36.0,24.0,28.0,11.0})
 *
 *  F.I.X.M.E: trzeba zr�nicowa� troch� rasy oraz uwzgl�dni� r�nice mi�dzy p�ciami
 *    FIXED by lil.
 *
 * --- jeremian */

#define RACEROZMOD ([ \
	"cz^lowiek"	: ({({59.0,37.0,104.0,30.0,28.0,28.0,25.0,17.0,20.0,11.0,60.0,\
			      99.0,80.0,92.0,50.0,51.0,40.0,36.0,24.0,28.0,11.0}),\
		            ({57.0,35.0,95.0,30.0,25.0,26.0,23.0,16.0,17.0,10.0,62.0,\
			      95.0,66.0,95.0,45.0,48.0,39.0,34.0,24.0,25.0,10.0}),\
		            ({179.0,65000.0})}),\
	"elf"		: ({({59.0,35.0,97.0,35.0,24.0,27.0,22.0,16.0,21.0,10.0,73.0,\
			      87.0,71.0,88.0,49.0,45.0,44.0,34.0,24.0,26.0,10.0}),\
		            ({56.0,31.0,90.0,34.0,22.0,28.0,20.0,14.0,19.0,9.0,64.0,\
			      89.0,62.0,90.0,50.0,44.0,42.0,32.0,22.0,24.0,9.0}),\
		            ({188.0,60000.0})}),\
	"p^o^lelf"	: ({({59.0,37.0,104.0,30.0,28.0,28.0,25.0,17.0,20.0,11.0,60.0,\
			      99.0,80.0,92.0,50.0,51.0,40.0,36.0,24.0,28.0,11.0}),\
		            ({57.0,35.0,95.0,30.0,25.0,26.0,23.0,16.0,17.0,10.0,62.0,\
			      95.0,66.0,95.0,45.0,48.0,39.0,34.0,24.0,25.0,10.0}),\
		            ({180.0,64000.0})}),\
	"krasnolud"	: ({({60.0,42.0,120.0,27.0,33.0,25.0,30.0,20.0,17.0,14.0,40.0,\
			      109.0,100.0,107.0,40.0,59.0,31.0,45.0,31.0,23.0,11.0}),\
		            ({59.0,39.0,105.0,26.0,28.0,24.0,27.0,17.0,17.0,12.0,39.0,\
			      111.0,90.0,101.0,39.0,61.0,30.0,39.0,27.0,20.0,10.0}),\
		            ({140.0,80000.0})}),\
	"nizio^lek"	: ({({57.0,35.0,86.0,25.0,25.0,24.0,23.0,15.0,15.0,9.0,34.0,\
			      89.0,87.0,87.0,37.0,48.0,27.0,31.0,20.0,21.0,9.0}),\
		            ({56.0,34.0,82.0,24.0,24.0,23.0,22.0,13.0,14.0,8.0,33.0,\
			      89.0,85.0,89.0,36.0,50.0,26.0,30.0,20.0,20.0,8.0}),\
		            ({120.0,55000.0})}),\
	"gnom"		: ({({57.0,35.0,86.0,27.0,25.0,26.0,23.0,15.0,17.0,9.0,34.0,\
			      87.0,84.0,86.0,37.0,47.0,27.0,30.0,20.0,22.0,10.0}),\
		            ({56.0,34.0,82.0,27.0,25.0,26.0,23.0,14.0,16.0,9.0,33.0,\
			      87.0,82.0,88.0,36.0,50.0,26.0,29.0,20.0,21.0,9.0}),\
		            ({120.0,50000.0})}),\
                    ])


