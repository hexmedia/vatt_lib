/* Standard do wioski etapu tworzenia postaci.
 *
 *
 * Do poprawnego funkcjonowania CALEGO ETAPU potrzebny jest takze plik
 * /d/Standard/login/ghost_player.c oraz email.c. Reszta z tamtego katalogu jest juz
 * chyba bezuzyteczna, ale zawsze mozna ja zostawic jako 'tryb awaryjny' ;)
 *
 * Kod jest w 99% pisany na nowo.
 * Dla ka�dego gracza tworzy si� jego w�asna kopia ca�ej wioski. A to dlatego,
 * �e w wiosce mo�na zrobi� niez�y burdel np. z obiektami, a nie chcemy, �eby
 * nast�pny gracz zastawa� taki burdel, kt�ry stworz� poprzedni gracze ju�
 * na dzie� dobry... :)
 *
 *
 * Vera. Kwiecie� 2006
 * PS.Ca�a wioska to zdecydowanie najbrzydszy kod, jaki do tej pory napisa�em.:P
 *
 *
 * Poprawki: Vera, 2006-12-08 13:08:23
 *           Zaimplementowano dodawanie przymiotnik�w od zarostu (kolor brody),
 *           poprawiono kilka b��d�w (m.in. dawanie niepasuj�cych ubra�)
 * 
 * 
 * 
 * FIXME: TODO: WARNING: modyfikatory od ciala je�li nie ma �adnego to dodajemy na sztywno 35 na ka�d� cech�. Brzydko zrobione, zrobi� lepiej. [v]
 */

#include <exp.h>
#include <login.h>
#include <ss_types.h>

inherit "/std/room.c";
inherit "/d/Standard/login/wioska/atrybuty.c";

#pragma strict_types
#include <colors.h>
#include <std.h>
#include <files.h>
#include <stdproperties.h>
#include <macros.h>
#include <filter_funs.h>
#include <options.h>
#include <language.h>
#include <state_desc.h>
#include "atrybuty.h"

#define WIOSKA "/d/Standard/login/wioska/"

#define DBG(x) find_player("vera")->catch_msg("B��D W WIOSCE TWORZENIA POSTACI:" + x + "\n");

int     plec,
        atr_puli,
        pula=44,                //tyle dodajemy do cech +modyfikatory od rasy
        modminus=24,            //modyfikator minusowy od wyboru budowy cia�a
        lysy_na_stale=0,
        alrm;

string  slowo_lat,
        race,
        rasa,                   //race zwraca np. czlowiek, a rasa np. mezczyzna.
        strum="",               //string pierwszej wybranej umiejetnosci
        tmp_skill1,
        tmp_skill2,             //wybierane przez gracza skille
        priorytet_od_ciala,
       *dostepne_cechy_ciala;   //zaleznie od rasy inny mapping budowy cia�a.

mixed   wloski,                 //zaleznie od rasy inny mapping koloru wlosow.
        oczka,                  //zaleznie od rasy inny mapping koloru oczu.
        wzrost_i_waga,
        cialko;                 //j.w.
object  byt;                    //czyli TP ale trzeba zrobic tak, bo ciagle bierze this_playera()
                                //mnie, czyli osobe, ktora laduje plik...


int startujemy();
int wiek;
int ustawiamy();
void ustawiamy_gabaryty();
//To sa funkcje w ktorych dany npc cos mowi. Te funkcje sa w lokacjach.
void reakcje_na_odpowiedzi() {}
void faza0start() {}
void fazaniebyt() {}
void faza0tak() {}
void faza0nie() {}
void faza0inne() {}
void faza1tak() {}
void faza1nie() {}
void faza1inne() {}
void faza2tak() {}
void faza2nie() {}
void faza2inne() {}
void faza3tak() {}
void faza3nie() {}
void faza3inne() {}
void faza4zle() {}
void faza4dobrze() {}
void faza5zle() {}
void faza5dobrze() {}
void faza6nierozumiem() {}
void faza6zamlody() {}
void faza6zastary() {}
void faza6dobrze() {}
void faza7zle() {}
void faza7dobrze8() {}
void faza7dobrze9() {}
void faza8tak() {}
void faza8nie1() {}
void faza8nie2() {}
void faza8inne() {}
void faza9zle() {}
void faza9dobrze() {}
void faza10zle() {}
void faza10dobrze() {}
void faza11zle() {}
void faza11dobrze1() {}
void faza11dobrze2() {}
void faza12zle() {}
void faza12dobrze() {}
void faza13zle() {}
void faza13dobrze() {}
void faza14zle() {}
void faza14dobrze() {}
void faza15tak() {}
void faza15nie() {}
void faza15inne() {}
void faza16zle() {}
void faza16dobrze() {}
void faza17tak() {}
void faza17nie() {}
void faza17inne() {}
void faza18zle() {}
void faza18dobrze() {}
void faza19zle() {}
void faza19dobrze() {}
void faza20zle() {}
void faza20dobrze() {}
void faza21zle() {}
void faza21zle2() {}
void faza21dobrze() {}
void faza22tak() {}
void faza22nie() {}
void faza22inne() {}
void jaka_rasa() {}

//funkcja przdzielaj�ca prezenty
void przydziel_prezenty(object komu) {}

//do on_logout
int signal_logout = 0;

void
create_wioska_room()
{
}

public nomask void
create_room()
{
    set_short("W wiosce");
    set_long("@@dlugi_opis@@"+"\n");
    add_prop(ROOM_I_LIGHT,2);
    //dzieki temu lokacje nie tworza swoich plikow .o
    add_prop(ROOM_I_NIE_ZAPISUJ,1);
    create_wioska_room();

    set_event_time(240.0);
    add_event("Wszystko wydaje si� by� jak ze snu.\n");
    add_event("Nasycone magi� powietrze zdaje si� wibrowa�.\n");
    add_event("Obraz postaci jawi�cej si� przed tob� wygina si� nienaturalnie "+
                "w lewo i prawo, by po chwili powr�ci� do normy.\n");
    add_event("Co� bardzo jasnego szybko przeci�o powietrze nad tob� "+
              "i r�wnie szybko znikn�o.\n");
    add_event("W oddali s�ycha� jakie� �miechy.\n");
    add_event("Przebiegaj�ca para dzieci �mieje si� weso�o, po czym znika.\n");
    add_event("Kolorowe t�czowe smugi przelatuj� co chwil� przez nasycone "+
              "srebrzystym py�em powietrze.\n");
    add_event("Przebiega ci� niesamowity dreszcz.\n");
    add_event("Czujesz, jak jaka� magiczna si�a pr�buje ci� odepchn��.\n");
    add_event("Jaka� posta� przemkn�a ci przed oczami.\n");
    add_event("Oniryczne kszta�ty migaj� ci przed oczyma.\n");
}

/* funkcyjka pomocnicza, u�ywana przy zrywaniu linka i ponownym
 * ustawianiu przymiotnik�w - /std/player/zmienne_shorty
 */
public int
query_lok_wioska_login()
{
    return 1;
}

/* ta funkcja odpowiada za tworzenie si� osobnej kopii wioski
 * dla ka�dego z graczy.
 */
public int
add_exit(string place, mixed cmd)
{
    string *parts;

    if (!IS_CLONE)
        return ::add_exit(place, cmd);

    if (stringp(place) &&
        (place[0] != '/') &&
        (place[0] != '@'))
    {
        parts = explode(file_name(this_object()), "/");
            parts[sizeof(parts) - 1] = place;
        place = implode(parts, "/");
    }

    LOAD_ERR(place);

    foreach (object x : object_clones(find_object(place)))
        if (member_array(file_name(this_object()), x->query_exit_rooms()) != -1)
            return ::add_exit(file_name(x), cmd);

    return ::add_exit(file_name(clone_object(place)), cmd);
}

public int komendy(string str);

public void
init()
{
    ::init();
    add_action(komendy, "", 1);
}

void
enter_inv(object ob, object from)
{ 
    ::enter_inv(ob,from);
    if (interactive(ob) && (ob->query_race() == "byt astralny" || ob->query_player_file() == "/d/Standard/login/ghost_player"))
       {
         set_alarm(3.0,0.0,"startujemy");
         byt=ob;
         plec=byt->query_gender();
       }
         //tablica z wzrostem i waga
          wzrost_i_waga=({HEIGHTDESC((plec == 1 ? "a" : "i")),
                          WIDTHDESC((plec == 1 ? "a" : "y")) });
}
void
leave_inv(object ob, object to)
{
    ::leave_inv(ob,to);
    if (interactive(ob) && ob->query_race() == "byt astralny" && faza>0)
    {
        write("\nOpuszczasz to miejsce. Pami�taj, �e je�li jednak si� namy�lisz "+
                "i zechcesz powr�ci� do tej istoty by wybra� jej ras� - b�dziesz "+
                "musia�"+koncoweczka("","a")+" zacz�� wszystko od pocz�tku.\n");
        faza=0;
        faza_pomocy=0;
    }
}

/*Funkcja pomocnicza, dla npc�w, kt�re
 *m�wi� po wybraniu pochodzenia np. "Ach, Rinde, co za wspania�e miejsce!"
 *;)
 */
string
powiedz_pochodzenie()
{
    string pocho="";

    if (tmp_pochodzenie[0..0] == " ")
        tmp_pochodzenie = tmp_pochodzenie[1..];
    if(tmp_pochodzenie~="z Bia�ego Mostu")
       pocho="z Bia�y Most";
    else if(tmp_pochodzenie~="z Grabowej Buchty")
       pocho="z Grabowa Buchta";
    else if(tmp_pochodzenie~="z G�r Sinych")
       pocho="z G�ry Sine";
    else if(tmp_pochodzenie~="z Klucza")
       pocho="z Klucz";
    else if(tmp_pochodzenie~="z Ho�opola")
       pocho="z Ho�opole";
    else if(tmp_pochodzenie[-1..-1] == "u")
       pocho=tmp_pochodzenie[..-2];
    else if(tmp_pochodzenie[-1..-1] == "y" || tmp_pochodzenie[-1..-1] == "i")
    {
        pocho=tmp_pochodzenie[..-2];
        pocho+="a";
    }
    else
       pocho=tmp_pochodzenie;

    return pocho;
}

void filtrowane_atrybuty()
{
    string *arr;
    object kto=TP; //???

    switch(race)
    {
        case "cz�owiek":
            cialko=(plec==1?cialo_kobiety:cialo_mezczyzny);break;
        case "elf":      cialko=plec==1?cialo_elfki:cialo_elfa;       break;
        case "nizio�ek": cialko=plec==1?cialo_niziolki:cialo_niziolka;break;
        case "krasnolud":cialko=plec==1?cialo_krasnoludki:cialo_krasnoluda;
                                                                        break;
        case "p�elf":   cialko=plec==1?cialo_polelfki:cialo_polelfa; break;
        case "gnom":     cialko=plec==1?cialo_gnomki:cialo_gnoma;     break;
        default:         cialko=plec==1?cialo_kobiety:cialo_mezczyzny;break;
    }

    arr = m_indices(cialko);
    arr = filter(arr, &call_other("/d/Standard/login/wioska/atrybuty",,
        kto,waga,wzrost) @ &operator(+)("p",) @ &implode(,"_") @ &explode(," ") @ plain_string);
    dostepne_cechy_ciala=arr;
}

int
startujemy()
{
    faza0start();
    faza=0;
    return 1;
}


//ten pierdolnik pokazuje si� czasem jak gracz ma z czym� problemy
public void
wyswietl_pomocniczy_komunikat()
{
    if(!random(3))
        write("Je�li masz problem, spr�buj skorzysta� z pomocy dost�pnej pod '?'.\n");
}
    

public void
pomoc(string str)
{
    string str;
    str = "\t*----------------POMOC----------------*\n";

    switch(faza_pomocy)
    {
        case 0:
            str += "Witaj w etapie tworzenia postaci!\n"+
                "Je�li chcesz dowiedzie� si� czego� wi�cej o "+
                "komendach na mudzie og�lnie, wpisz '?komendy' "+
                "oraz zapoznaj si� ze sk�adni� ('?skladnia'). "+
                "Pami�taj, �e prawie zawsze znajdziesz pomoc pod: "+
                "'?komenda' lub czasami '?przedmiot'.\n"+
                "Rozejrzyj si� tu nieco za pomoc� komend takich "+
                "jak 'sp�jrz' lub 'obejrzyj'. Przemieszcza� si� "+
                "z lokacji na inn� lokacj� mo�esz za pomoc� "+
                "kierunk�w (np. 'polnoc' lub 'poludniowy-zachod'). "+
                "Mo�esz tak�e skorzysta� ze skr�t�w. "+
                "S� to JEDYNE komendy, kt�rych skr�ty s� zaczerpni�te "+
                "z j�zyka angielskiego. Ot, tak si� og�lnie przyj�o na "+
                "polskich mudach. Je�li nie znasz tych skr�t�w wpisz: "+
                "'?kierunki'.\n"+
                "W tym miejscu twoim zadaniem jest kreacja postaci, "+
                "kt�r� b�dziesz przemierza�"+koncoweczka("","a")+
                " �wiat Vatt'gherna. "+
                "Podczas tego zadania b�dzie ci towarzyszy� pomoc, "+
                "kt�r� uzyskasz wpisuj�c oczywi�cie '?'.\n"+
                "Wpierw musisz wybra� ras�. By to uczyni�, 'odpowiedz' "+
                "na pytania istoty rasy, kt�ra ci� interesuje. "+
                "Zazwyczaj wystarczy odpowiedzie� tak lub nie.\n"+
                "UWAGA: Je�li nie znasz reali�w sagi o wied�minie Andrzeja "+
                "Sapkowskiego - mocno sugeruj�, by� "+
                "wybra�"+koncoweczka("","a")+" cz�owieka. "+
                "Pozwoli to unikn�� wiele przykrych sytuacji.";
                break;
        case 1:
            str += "Gratuluje! Uda�o ci si� porozumie� z istot�. "+
                "Na pewno zatem poradzisz sobie i teraz. Po prostu odpowiadaj "+
                "na jej pytania."; break;
        case 2:
            str += "Nierzadko znajomo�� historii rasy "+
                "oraz nastawienia do innych ras, kt�r� "+
                "w�a�nie zamierzasz wybra� pozwala si� przekona� "+
                "czy jest to dobry wyb�r. We� tak�e pod uwag� fakt, "+
                "�e np. odgrywa� posta� elfa jest znacznie trudniej ni� "+
                "cz�owieka.\n"+
                "Je�li tworzysz na Vatt'ghernie posta� po raz pierwszy - "+
                "sugeruj�, by� zechcia�"+koncoweczka("","a")+
                " wys�ucha� tego, co ma do powiedzenia "+
                "istota rasy, kt�ra ci� interesuje."; break;
        case 3:
            str += "No w�a�nie? Czy jeste� pewn"+
                koncoweczka("y","a")+
                " tego wyboru?\nZ drugiej strony..."+
                "W ka�dej chwili mo�esz przerwa� konwersacj� "+
                "z istot�. By to uczyni� - najpro�ciej jest po "+
                "prostu wyj�� z tej lokacji."; break;
        case 4:
            str += "Je�li masz problem z wyborem - "+
                "zapytaj go o wzrost."; break;
        case 5:
            str += "Je�li masz problem z wyborem - "+
                "zapytaj go o wag�."; break;
        case 6:
            str += "Musisz zdefiniowa� sw�j wiek. "+
                "Zapytaj go o wiek, je�li masz z tym problem."; break;
        case 7:
            str += "Zapytaj istot� o kolory oczu i wybierz."; break;
        case 8:
            str += "Je�li w tym momencie zadecydujesz o nieposiadaniu "+
                "w�os�w, nie urosn� ci one ju� nigdy! Pami�taj o tym. "+
                "Natomiast, je�li zechcia�"+koncoweczka("by�","aby�")+
                " wst�pi� do �wiata jako posta� "+
                "�ysa, lecz nie permanentnie - odpowiedz pozytywnie, a "+
                "w nast�pnym pytaniu o d�ugo�� w�os�w wybierzesz sobie "+
                "tego aktualn� warto��."; break;
        case 9:
            str += "Zapytaj istot� o kolory w�os�w i wybierz."; break;
        case 10:
            str += "Zdecydowa�"+koncoweczka("e�","a�")+
                " si� na nieposiadanie w�os�w w og�le. "+
                "W takim przypadku musisz poda� kolor zarostu, jaki �yczysz "+
                "sobie posiada�. Zapytaj istot� o kolory zarostu, je�li nie wiesz "+
                "jakie mo�esz wybra�.";break;
        case 11: str += "Zapytaj istot� o d�ugo�ci w�os�w i wybierz."; break;
        case 12:
            str += "Zapytaj istot� o d�ugo�ci w�s�w/brody i wybierz. "+
                "Je�li nie chcesz posiada� w�s�w/brody na ten czas wybierz "+
                "'brak'."; break;
        case 13:
            str += "Zapytaj istot� o d�ugo�ci w�s�w/brody i wybierz. "+
                "Je�li nie chcesz posiada� w�s�w/brody na ten czas wybierz "+
                "'brak'."; break;
        case 14:
            str += "Wybierz w�r�d listy cech� charakterystyczn� "+
                "dla twojego cia�a."; break;
        case 15: str += "Odpowiedz tak lub nie."; break;
        case 16: str += "Zapytaj go o cechy szczeg�lne i wybierz."; break;
        case 17:
            str += "Chcesz posiada� pochodzenie? "+
                "Pami�taj, �e raz ustawione, ju� nigdy nie ulegnie zmianie. "+
                "W czasie gry b�dziesz m"+koncoweczka("�g�","og�a")+
                " je sobie ustawi�, je�li teraz tego nie wybierzesz. "+
                "Lecz o wyb�r by� mo�e czasem b�dzie trzeba si� postara� bardziej "+
                "ni� teraz. "+
                "Dzi�ki pochodzeniu zazwyczaj jeste� bardziej szanowan"+
                koncoweczka("y","a")+" ni� inni w twojej rodzimej "+
                "miejscowo�ci."; break;
        case 18:
            str += "Zapytaj go o pochodzenie, je�li chcesz uzyska� "+
                "list�."; break;
        case 19:
            str += "Mo�esz wybra� tylko jedn� cech�, kt�ra "+
                "b�dzie silniejsza ni� inne. Do dyspozycji masz: "+
                "si��, zr�czno��, wytrzyma�o��, inteligencj�, m�dro�� i odwag�.";
                break;
        case 20:
            str += "Wybierz pierwsz� umiej�tno��, kt�r� "+
                "chcesz, by twoja posta� posiad�a."; break;
        case 21:
            str += "Wybierz drug� umiej�tno��, kt�r� "+
                "chcesz, by twoja posta� posiad�a."; break;
        case 62:
            str += "A wi�c zdecydowa�"+koncoweczka("e�","a�")+" si� by� permanentnie "+
                "�ys"+koncoweczka("y","a")+". Musisz w takim razie poda� kolor swego "+
                "zarostu (np. brody). By dowiedzie� si� czego� wi�cej o mo�liwych kolorach "+
                "do wyboru, zapytaj istot� o kolor brody."; break;
        default:
            str += "Odpowiedz na zadawane pytania, a wszystko b�dzie dobrze!"; break;
    }

    str+="\n\t*-------------------------------------*\n";
    write(str);
}

/*Ta funkcja sluzy do wywolywania
 *alarmow(kolejnych pytan npca oraz zmieniania fazy)
 */
void fazka(int x, string faz)
{
    faza=x;
    string lokacja;
    call_other(lokacja,faz);
    call_other(environment(TP),faz);
}

/*
 *Funkcje pomocnicze: wildmatch_tak oraz wildmatch_nie
 */
int
wildmatch_tak(string str)
{
    if(wildmatch("tak *", str) || wildmatch("tak, *", str) ||
       wildmatch("Tak *", str) || wildmatch("Tak, *", str) ||
       wildmatch("* tak *",str) || wildmatch("* tak",str) ||
       wildmatch("* tak.",str) || str=="tak" || str=="Tak"||
       str=="Tak." || str=="tak.")
    {
        return 1;
    }
    else
        return 0;
}
int
wildmatch_nie(string str)
{
    if(wildmatch("nie *", str) || wildmatch("nie, *", str) ||
       wildmatch("Nie *", str) || wildmatch("Nie, *", str) ||
       wildmatch("* nie *",str) || wildmatch("* nie",str) ||
       wildmatch("* nie.",str) || str=="nie" || str=="Nie"||
       str=="Nie." || str=="nie.")
    {
        return 1;
    }
    else
        return 0;
}
int
wildmatch_elfow_waga(string str)
{
    str=LC(str);
    if(byt->query_gender() && wildmatch("*oty�a*",str))
        return 1;
    else if(wildmatch("*oty�y*",str))
        return 1;

    return 0;
}

public int
komendy(string str)
{
    string verb=query_verb();
    object ob;

    if(verb[0] == '\'' && strlen(verb) == 1)
        verb = "'"+str;

    if(TP->query_race() != "byt astralny")
        return 0;

    if(TP->query_wiz_level())
        return 0;

    if(verb=="odpowiedz")
    {
        if(!str || str=="0")
        {
            notify_fail("Odpowiedz co?\n");
            return 0;
        }

        write(set_color(COLOR_BOLD_ON,COLOR_FG_GREEN)+"Odpowiadasz: "+
        clear_color()+set_color(COLOR_FG_GREEN)+str+clear_color()+"\n");
        notify_fail("",20);
    }

    if (verb == "odpowiedz" || verb[0] == '\'' || verb ~= "potwierd�" || verb == "zaprzecz" || verb == "powiedz")
    {
        if (verb[0] == '\'')
        {
            if (strlen(verb) == 1)
                return 0;
            else
                str = verb[1..] + ((strlen(str) && str != verb[1..]) ? (" " + str) : "");
        }

        if (!stringp(str))
            str = "";
        if(str[0..0] == " ")
            str=str[1..];

	//a oto zabezpieczenie przed powtarzaniem si� npcy ;]
	if(faza != -1 && get_alarm(alrm) != 0)
	    return 0;

        switch(faza)
        {
        case 0:
            faza_pomocy=0;

            if(interactive(ob) && ob->query_race() != "byt astralny")
                fazaniebyt();
            else if(wildmatch_tak(str) || verb~="potwierd�")
            {
                faza_pomocy=1;
                faza=-1;
                alrm = set_alarm(0.1,0.0,&reakcje_na_odpowiedzi());
                alrm = set_alarm(1.5,0.0,&fazka(1,"faza0tak"));
            }
            else if(wildmatch_nie(str) || verb=="zaprzecz")
                alrm = set_alarm(0.1,0.0,&faza0nie());
            else
                alrm = set_alarm(0.1,0.0,&faza0inne());

            return 0;
        break;

        case 1:
            faza_pomocy=1;

            if(wildmatch_tak(str) || verb~="potwierd�")
            {
                faza=-1; faza_pomocy=2;
                alrm = set_alarm(0.1,0.0,&reakcje_na_odpowiedzi());
                alrm = set_alarm(1.5,0.0,&fazka(2,"faza1tak"));
            }
            else if(wildmatch_nie(str) || verb=="zaprzecz")
                alrm = set_alarm(0.1,0.0,&faza1nie());
            else
                alrm = set_alarm(0.1,0.0,&faza1inne());

            return 0;
        break;

        case 2: //Czy opowiadac historie
            faza_pomocy=2;

            if(wildmatch_tak(str) || verb~="potwierd�")
            {
                faza=-1; faza_pomocy=3;
                alrm = set_alarm(0.1,0.0,&reakcje_na_odpowiedzi());
                alrm = set_alarm(1.5,0.0,&fazka(3,"faza2tak"));
            }
            else if(wildmatch_nie(str) || verb=="zaprzecz")
            {
                faza=-1;
                alrm = set_alarm(0.1,0.0,&reakcje_na_odpowiedzi());
                alrm = set_alarm(1.5,0.0,&fazka(3,"faza2nie"));
            }
            else
                alrm = set_alarm(0.1,0.0,&faza2inne());

            return 0;
        break;

        case 3: //Ostateczne pytanie o rase. Nastepnie pytanie o wzrost
            faza_pomocy=3;
            if(wildmatch_tak(str) || verb~="potwierd�")
            {
                faza=-1; faza_pomocy=4;
                alrm = set_alarm(0.1,0.0,&reakcje_na_odpowiedzi());
                alrm = set_alarm(1.5,0.0,&fazka(4,"faza3tak"));
            }
            else if(wildmatch_nie(str) || verb=="zaprzecz")
                alrm = set_alarm(0.1,0.0,&faza3nie());
            else
                alrm = set_alarm(0.1,0.0,&faza3inne());

            return 0;
        break;

        case 4: //odpowiedz na wzrost, pytanie o wage
            faza_pomocy=4;

            if(str[-1..-1] == ".") str=LC(str[..-2]); else str=LC(str);
                //if(str[..0] == " ") str=str[1..];

            if ((wzrost = member_array(str, wzrost_i_waga[0])) == -1)
            {
                alrm = set_alarm(0.1,0.0,&faza4zle());
                return 0;
            }

            faza_pomocy=5;
            faza=-1;
            alrm = set_alarm(0.1,0.0,&reakcje_na_odpowiedzi());
            alrm = set_alarm(1.5,0.0,&fazka(5,"faza4dobrze"));

            return 0;
        break;

        case 5: //waga
            faza_pomocy=5;
            if(str[-1..-1] == ".")
                str=LC(str[..-2]);
            else
                str=LC(str);

            if ((waga = member_array(str, wzrost_i_waga[1])) == -1)
            {
                alrm = set_alarm(0.1,0.0,&faza5zle());
                return 0;
            }

            jaka_rasa();
            //ponoc elfy sa takie piekne i nie moga byc grube :P
            if(race=="elf" && wildmatch_elfow_waga(str))
            {
                alrm = set_alarm(0.1,0.0,&faza5zle());
                return 0;
            }

            faza=-1; faza_pomocy=6;
            alrm = set_alarm(0.1,0.0,&reakcje_na_odpowiedzi());
            alrm = set_alarm(1.5,0.0,&fazka(6,"faza5dobrze"));

            return 0;
        break;

        case 6: //wiek
            faza_pomocy=6;
            if(str[-1..-1] == ".") str=str[..-2];
            wiek=atoi(str);

            if(!wiek)
                wiek=LANG_NUMS(LC(str)); //gdyby kto� poda� s�ownie

            if(!wiek)
            {
                alrm = set_alarm(0.1,0.0,&faza6nierozumiem());
                return 0;
            }

            jaka_rasa();

            if(wiek<DOLNA_GRANICA_WIEKU[race])
            {
                alrm = set_alarm(0.1,0.0,&faza6zamlody());
                return 0;
            }
            if(wiek>GORNA_GRANICA_WIEKU[race])
            {
                alrm = set_alarm(0.1,0.0,&faza6zastary());
                return 0;
            }

            switch(wiek) //Czy ma pisac slowo 'lat' czy 'lata'
            { //Bardzo s�aby spos�b, jak wida�. Ale informatykiem
	      //nie jestem, nie mog�em znale�� niczego innego. V.
                case 2..4:        case 22..24:         case 32..34:
                case 42..44:      case 52..54:         case 62..64:
                case 72..74:      case 82..84:         case 92..94:
                case 102..104:    case 122..124:       case 132..134:
                case 142..144:    case 152..154:       case 162..164:
                case 172..174:    case 182..184:       case 192..194:
                case 202..204:    case 222..224:       case 232..234:
                case 242..244:    case 252..254:       case 262..264:
                case 272..274:    case 282..284:       case 292..294:
                case 302..304:    case 322..324:       case 332..334:
                case 342..344:    case 352..354:       case 362..364:
                case 372..374:    case 382..384:       case 392..394:
                case 402..404:
                    slowo_lat="lata";
                    break;
                default:
                    slowo_lat="lat";
                    break;
            }

            tmp_wiek=wiek;
            faza=-1; faza_pomocy=7;
            alrm = set_alarm(0.1,0.0,&reakcje_na_odpowiedzi());
            alrm = set_alarm(1.5,0.0,&fazka(7,"faza6dobrze"));
            return 0;
        break;

        case 7: //kolor oczu
            faza_pomocy=7;
            switch(race)
            {
                case "cz�owiek": oczka = (plec == 1 ? oczy_kobiety : oczy_mezczyzny);   break;
                case "elf":      oczka = (plec == 1 ? oczy_elfki : oczy_elfa);          break;
                case "nizio�ek": oczka = (plec == 1 ? oczy_niziolki : oczy_niziolka);   break;
                case "krasnolud":oczka = oczy_krasnoludow;                              break;
                case "p�elf":   oczka = (plec == 1 ? oczy_polelfki : oczy_polelfa);    break;
                case "gnom":     oczka = oczy_gnomow;                                   break;
                default:         oczka = (plec == 1 ? oczy_kobiety : oczy_mezczyzny);   break;
            }

            if(str[-1..-1] == ".")
                str=LC(str[..-2]);
            else
                str=LC(str);

            if((kolor_oczu=member_array(str,m_indices(oczka)))==-1)
            {
                alrm = set_alarm(0.1,0.0,&faza7zle());
                return 0;
            }

            filtrowane_atrybuty();//to dot. cech budowy ciala, ale juz mozemy filtrowac
            tmp_kolor_oczu=oczka[str][0..2];

            if(race=="elf" || race~="p�elf")//te rasy nie moga byc lyse na stale
            {
                faza_pomocy=9;
                faza=-1;
                alrm = set_alarm(0.1,0.0,&reakcje_na_odpowiedzi());
                alrm = set_alarm(1.5,0.0,&fazka(9,"faza7dobrze9"));
            }
            else
            {
                faza_pomocy=8;
                faza=-1;
                alrm = set_alarm(0.1,0.0,&reakcje_na_odpowiedzi());
                alrm = set_alarm(1.5,0.0,&fazka(8,"faza7dobrze8"));
            }
        break;

        case 8: //pyt. czy chce sie posiadac wlosy
            faza_pomocy=8;
            if(wildmatch_tak(str) || verb~="potwierd�")
            {
                faza_pomocy=9;
                faza=-1;
                alrm = set_alarm(0.1,0.0,&reakcje_na_odpowiedzi());
                alrm = set_alarm(1.5,0.0,&fazka(9,"faza8tak"));
                return 0;
            }
            if(wildmatch_nie(str) || verb=="zaprzecz")
            {
                if(plec==1) //kobietom nie zadaje sie pytan o zarost :P
                { //�ysa na sta�e kobieta
                    tmp_kolor_wlosow="matowe";
                    tmp_przym_wlosy=({"matowow�osy","matowow�osi"});

                    faza_pomocy=14;
                    tmp_dlugosc_wlosow=0.0;
                    faza=-1;
                    alrm = set_alarm(0.1,0.0,&reakcje_na_odpowiedzi());
                    alrm = set_alarm(1.5,0.0,&fazka(14,"faza8nie1"));
                    return 0;
                }
                else //a mezczyzni i tak musza podac kolor zarostu do brody i wasow.
                {
                    faza_pomocy=10;
                    tmp_dlugosc_wlosow=0.0;
                    faza=-1;
                    alrm = set_alarm(0.1,0.0,&reakcje_na_odpowiedzi());
                    alrm = set_alarm(1.5,0.0,&fazka(10,"faza8nie2"));
                    return 0;
                }
            }
            else
            {
                alrm = set_alarm(0.1,0.0,&faza8inne());
                return 0;
            }
        break;

        case 9: //kolor wlosow, pytanie o dlugosc
            faza_pomocy=11;
            switch(race)
            {
                case "cz�owiek":
                    if (plec==1)
                        wloski=wlosy_kobiety;
                    else
                        wloski=wlosy_mezczyzny;
                    break;
                case "elf":         wloski=wlosy_elfow;             break;
                case "nizio�ek":    wloski=wlosy_niziolkow;         break;
                case "krasnolud":   wloski=wlosy_krasnoludow;       break;
                case "p�elf":      wloski=wlosy_polelfow;          break;
                case "gnom":        wloski=wlosy_gnomow;            break;
                default:
                    if (plec==1)
                        wloski=wlosy_kobiety;
                    else
                        wloski=wlosy_mezczyzny;
            }

            if(str[-1..-1] == ".") str=LC(str[..-2]); else str=LC(str);

            if((kolor_wlosow=member_array(str,m_indices(wloski)))==-1)
            {
                faza_pomocy=9;
                alrm = set_alarm(0.1,0.0,&faza9zle());
                return 0;
            }

            tmp_kolor_wlosow=str;
            tmp_przym_wlosy=wloski[str];
            faza=-1;
            alrm = set_alarm(0.1,0.0,&reakcje_na_odpowiedzi());
            alrm = set_alarm(1.5,0.0,&fazka(11,"faza9dobrze"));
            return 0;
        break;

        case 10: //ta faza jest tylko jesli wybralo sie permanentnie �ysego.
                    //podajemy tu kolor swojego zarostu, ktory
                    //zapisywany jest i tak do naturalny_kolor_wlosow
            faza_pomocy=62;
            if(str[-1..-1] == ".") str=LC(str[..-2]); else str=LC(str);

            if((member_array(str,kolory_zarostu))==-1)
            {
                faza_pomocy=10;
                alrm = set_alarm(0.1,0.0,&faza10zle());
                return 0;
            }

            lysy_na_stale=1;

            switch(race)
            {
                case "cz�owiek":
                    if (plec==1)
                        wloski=wlosy_kobiety;
                    else
                        wloski=wlosy_mezczyzny;
                    break;
                case "elf":        wloski=wlosy_elfow;                          break;
                case "nizio�ek":   wloski=wlosy_niziolkow;                      break;
                case "krasnolud":  wloski=wlosy_krasnoludow;                    break;
                case "p�elf":     wloski=wlosy_polelfow;                       break;
                case "gnom":       wloski=wlosy_gnomow;                         break;
                default:
                    if (plec==1)
                        wloski=wlosy_kobiety;
                    else
                        wloski=wlosy_mezczyzny;
            }

            tmp_kolor_wlosow=str;
            tmp_przym_wlosy=wloski[str]; //do zarostu to potrzebne...
            //tmp_przym_wlosy=({"",""});
            faza=-1;
            alrm = set_alarm(0.1,0.0,&reakcje_na_odpowiedzi());
            alrm = set_alarm(1.5,0.0,&fazka(12,"faza10dobrze"));
            return 0;
        break;

        case 11: //dlugosc wlosow, pytanie o wasy(jesli mezczyzna;P)
            faza_pomocy=-1;
            if(str[-1..-1] == ".") str=LC(str[..-2]); else str=LC(str);

            if((member_array(str, m_indices(dlugosc_wlosow)))==-1)
            {
                alrm = set_alarm(0.1,0.0,&faza11zle());
                return 0;
            }
            tmp_dlugosc_wlosow=dlugosc_wlosow[str];
            if(byt->query_gender()==1)//omijamy pyt. o zarost, jesli 1
            {
                faza_pomocy=14;
                faza=-1;
                alrm = set_alarm(0.1,0.0,&reakcje_na_odpowiedzi());
                alrm = set_alarm(1.5,0.0,&fazka(14,"faza11dobrze1"));
                return 0;
            }      //elfy nie maja zarostu, dzieci takze
            else if(race=="elf" || tmp_wiek<=19)
            {
                faza_pomocy=14;
                faza=-1;
                alrm = set_alarm(0.1,0.0,&reakcje_na_odpowiedzi());
                alrm = set_alarm(1.5,0.0,&fazka(14,"faza11dobrze1"));
                return 0;
            }
            else
            {
                faza_pomocy=12;
                faza=-1;
                alrm = set_alarm(0.1,0.0,&reakcje_na_odpowiedzi());
                alrm = set_alarm(1.5,0.0,&fazka(12,"faza11dobrze2"));
                return 0;
            }
        break;

        case 12: //dlugosc wasow, pyt o brode
            faza_pomocy=13;
            if(str[-1..-1] == ".") str=LC(str[..-2]); else str=LC(str);

            if((member_array(str, m_indices(dlugosc_wasow)))==-1)
            {
                faza_pomocy=12;
                alrm = set_alarm(0.1,0.0,&faza12zle());
                return 0;
            }
            tmp_dlugosc_wasow=dlugosc_wasow[str];
            faza=-1;
            alrm = set_alarm(0.1,0.0,&reakcje_na_odpowiedzi());
            alrm = set_alarm(1.5,0.0,&fazka(13,"faza12dobrze"));
            return 0;
        break;

        case 13: //dlugosc brody, pyt o budowe ciala
            faza_pomocy=14;
            if(str[-1..-1] == ".") str=LC(str[..-2]); else str=LC(str);

            if((member_array(str, m_indices(dlugosc_brody)))==-1)
            {
                faza_pomocy=13;
                alrm = set_alarm(0.1,0.0,&faza13zle());
                return 0;
            }
            tmp_dlugosc_brody=dlugosc_brody[str];
            faza=-1;
            alrm = set_alarm(0.1,0.0,&reakcje_na_odpowiedzi());
            alrm = set_alarm(1.5,0.0,&fazka(14,"faza13dobrze"));
            return 0;
        break;

        case 14: //budowa ciala (to to, co ma byc zamiast wagi w opisie)
            faza_pomocy=15;
            filtrowane_atrybuty();

            if(str[-1..-1] == ".")
                str=LC(str[..-2]);
            else
                str=LC(str);

            if((member_array(str, dostepne_cechy_ciala)) == -1)
            {
                faza_pomocy=14;
                alrm = set_alarm(0.1,0.0,&faza14zle());
                return 0;
            }
            tmp_cecha_ciala=cialko[str][0..2];
            priorytet_od_ciala=cialko[str][4];
            modyfikator_od_ciala=cialko[str][3];

            faza=-1;
            alrm = set_alarm(0.1,0.0,&reakcje_na_odpowiedzi());
            alrm = set_alarm(1.5,0.0,&fazka(15,"faza14dobrze"));
            return 0;
        break;

        case 15: //odpowiedz czy chce sie miec ceche szczegolna <opcjonalnie>
            faza_pomocy=-1;
            if(wildmatch_tak(str) || verb~="potwierd�")
            {
                faza_pomocy=16;
                faza=-1;
                alrm = set_alarm(0.1,0.0,&reakcje_na_odpowiedzi());
                alrm = set_alarm(1.5,0.0,&fazka(16,"faza15tak"));
                return 0;

            }
            else if(wildmatch_nie(str) || verb=="zaprzecz")
            {
                faza_pomocy=17;
                faza=-1;
                alrm = set_alarm(0.1,0.0,&reakcje_na_odpowiedzi());
                alrm = set_alarm(1.5,0.0,&fazka(17,"faza15nie"));
                return 0;
            }
            else
            {
                alrm = set_alarm(0.1,0.0,&faza15inne());
                return 0;
            }
        break;

        case 16: //wybieranie cechy szczegolnej (jesli sie zgodzilo wczesniej)
          //tutaj jest tylko podzial na plec, nie ma od rasy.
            faza_pomocy=17;
            mixed mapc= ( plec== 1 ?cechy_szczegolne_kobiet : cechy_szczegolne_mezczyzn);
            mixed szczegolne_k_lub_m;
            if(race=="elf") //elfy nie moga wybrac np. 'garba' i takie tam...
            szczegolne_k_lub_m=m_indices(filter(mapc, &operator(==)(, 3) @ &sizeof()));
            else
            szczegolne_k_lub_m = m_indices(mapc);

            if(str[-1..-1] == ".")
                str=LC(str[..-2]);
            else
                str=LC(str);

            if((member_array(str, szczegolne_k_lub_m)) == -1)
            {
                faza_pomocy=16;
                alrm = set_alarm(0.1,0.0,&faza16zle());
                return 0;
            }

            tmp_cecha_szczegolna=mapc[str][0..2];
    //write("!!!DEBUGGER!!! wybrana tablica to: "+COMPOSITE_WORDS(tmp_cecha_szczegolna)+".\n");
            faza=-1;
            alrm = set_alarm(0.1,0.0,&reakcje_na_odpowiedzi());
            alrm = set_alarm(1.5,0.0,&fazka(17,"faza16dobrze"));

            return 0;

        break;

        case 17:  //odpowiedz czy chce sie posiadac pochodzenie.
            faza_pomocy=-1;
            if(wildmatch_tak(str) || verb~="potwierd�")
            {
                faza_pomocy=18;
                faza=-1;
                alrm = set_alarm(0.1,0.0,&reakcje_na_odpowiedzi());
                alrm = set_alarm(1.5,0.0,&fazka(18,"faza17tak"));
                return 0;

            }
            else if(wildmatch_nie(str) || verb=="zaprzecz")
            {
                faza_pomocy=19;
                faza=-1;
                alrm = set_alarm(0.1,0.0,&reakcje_na_odpowiedzi());
                alrm = set_alarm(1.5,0.0,&fazka(19,"faza17nie"));
                return 0;
            }
            else
            {
                faza_pomocy=17;
                alrm = set_alarm(0.1,0.0,&faza17inne());
                return 0;
            }
        break;

        case 18: //wybor pochodzenia jesli sie zgodzilo wczesniej.
            faza_pomocy=19;
            if(str[-1..-1] == ".") str=LC(str[..-2]); else str=LC(str);
            //a mo�e kto� z ma�ej podaje...np. "z rinde" zamiast "z Rinde"
            if(member_array(lower_case(str), map(pochodzenie, lower_case)) >= 0)
                str=pochodzenie[member_array(lower_case(str), map(pochodzenie,lower_case))];

            if((member_array(str, pochodzenie)) == -1)
            {
                    faza_pomocy=18;
                alrm = set_alarm(0.1,0.0,&faza18zle());
                return 0;
            }

            tmp_pochodzenie=str;
            faza=-1;
            alrm = set_alarm(0.1,0.0,&reakcje_na_odpowiedzi());
            alrm = set_alarm(1.5,0.0,&fazka(19,"faza18dobrze"));
            return 0;
        break;

        case 19: //cechy
            faza_pomocy=20;
            string *cechy=SD_STAT_NAMES_BIE;

            if(str[-1..-1] == ".") str=LC(str[..-2]); else str=LC(str);

            if((member_array(str, cechy)) == -1)
            {
                faza_pomocy=19;
                alrm = set_alarm(0.1,0.0,&faza19zle());
                return 0;
            }

            /*find_player("vera")->catch_msg("pula = " + pula + "\n" +
                "modminus = " + modminus + "\nmodyfikator_od_ciala  = " +
                modyfikator_od_ciala + "\n");*/

            if(modyfikator_od_ciala=="s")
            {
                //write("!!!DEBUGGER!!! od ciala dodajemy do: sila\n");
                pula=pula-modminus;
            }
            else if(modyfikator_od_ciala=="z")
            {
                //write("!!!DEBUGGER!!! od ciala dodajemy do: zrecz\n");
                pula=pula-modminus;
                pula+=5;     //A to dlatego, ze budowa ciala najmniej
                modminus-=5;      //wplywa na zrecznosc.
            }
            else if(modyfikator_od_ciala=="w")
            {
                //write("!!!DEBUGGER!!! od ciala dodajemy do: wt\n");
                pula=pula-modminus;
            }

            if((member_array(str,cechy))==0)
            {
                atr_puli=0;
                //write("!!!DEBUGGER!!! wybrales atrybut puli: "+atr_puli+".\n");
                faza=-1;
                alrm = set_alarm(0.1,0.0,&reakcje_na_odpowiedzi());
                alrm = set_alarm(1.5,0.0,&fazka(20,"faza19dobrze"));
            }
            else if((member_array(str,cechy))==1)
            {
                atr_puli=1;
                //write("!!!DEBUGGER!!! wybrales atrybut puli: "+atr_puli+".\n");
                faza=-1;
                alrm = set_alarm(0.1,0.0,&reakcje_na_odpowiedzi());
                alrm = set_alarm(1.5,0.0,&fazka(20,"faza19dobrze"));
            }
            else if((member_array(str,cechy))==2)
            {
                atr_puli=2;
                //write("!!!DEBUGGER!!! wybrales atrybut puli: "+atr_puli+".\n");
                faza=-1;
                alrm = set_alarm(0.1,0.0,&reakcje_na_odpowiedzi());
                alrm = set_alarm(1.5,0.0,&fazka(20,"faza19dobrze"));
            }
            else if((member_array(str,cechy))==3)
            {
                atr_puli=3;
                //write("!!!DEBUGGER!!! wybrales atrybut puli: "+atr_puli+".\n");
                faza=-1;
                alrm = set_alarm(0.1,0.0,&reakcje_na_odpowiedzi());
                alrm = set_alarm(1.5,0.0,&fazka(20,"faza19dobrze"));
            }
            else if((member_array(str,cechy))==4)
            {
                atr_puli=4;
                //write("!!!DEBUGGER!!! wybrales atrybut puli: "+atr_puli+".\n");
                faza=-1;
                alrm = set_alarm(0.1,0.0,&reakcje_na_odpowiedzi());
                alrm = set_alarm(1.5,0.0,&fazka(20,"faza19dobrze"));
            }
#if 0
            else if((member_array(str,cechy))==5)
            {
                atr_puli=5;
                //write("!!!DEBUGGER!!! wybrales atrybut puli: "+atr_puli+".\n");
                faza=-1;
                alrm = set_alarm(0.1,0.0,&reakcje_na_odpowiedzi());
                alrm = set_alarm(1.5,0.0,&fazka(20,"faza19dobrze"));
            }
#endif
            else
            {
                write("\n\nUWAGA: Ten tekst nie powinien ci si� nigdy "+
                    "pokaza�. Zg�o� to natychmiast administracji!\n\n");
                notify_wizards(TO,"B��d przy tworzeniu postaci! Ta posta� nie ma cech!\n");
            }

            return 0;
        break;

        case 20: //umiejetnosci, wybor pierwszej.
            faza_pomocy=21;
            if(str[-1..-1] == ".") str=LC(str[..-2]); else str=LC(str);

            if((member_array(str, m_indices(lista_skilli))) == -1)
            {
                faza_pomocy=20;
                alrm = set_alarm(0.1,0.0,&faza20zle());
                return 0;
            }

            strum=str;
            tmp_skill1=lista_skilli[str];
            //write("!!!DEBUGGER!!! Wybrano: "+tmp_skill1+".\n");
            faza=-1;
            alrm = set_alarm(0.1,0.0,&reakcje_na_odpowiedzi());
            alrm = set_alarm(1.5,0.0,&fazka(21,"faza20dobrze"));
            return 0;
        break;

        case 21: //umiejetnosci, wybor drugiej.
            faza_pomocy=-1;
            if(str[-1..-1] == ".") str=LC(str[..-2]); else str=LC(str);

            if((member_array(str, m_indices(lista_skilli))) == -1)
            {
                faza_pomocy=21;
                alrm = set_alarm(0.1,0.0,&faza21zle());
                return 0;
            }
            if(str==strum)
            {
                faza_pomocy=21;
                alrm = set_alarm(0.1,0.0,&faza21zle2());
                return 0;
            }
            tmp_skill2=lista_skilli[str];
            //write("!!!DEBUGGER!!! Wybrano: "+tmp_skill2+".\n");
            faza=-1;
            alrm = set_alarm(0.1,0.0,&reakcje_na_odpowiedzi());
            alrm = set_alarm(1.5,0.0,&fazka(22,"faza21dobrze"));
            return 0;
        break;

        case 22: //gracz dostaje krzyz na droge i odpalamy funkcje ustawiamy()
            faza_pomocy=-1;
            if(wildmatch_tak(str) || verb~="potwierd�")
            {
                faza_pomocy=23;
                faza=-1;
                TP->set_option(OPT_RECEIVING, 1);//przyjmowanie
                alrm = set_alarm(0.1,0.0,&reakcje_na_odpowiedzi());
                //alrm = set_alarm(7.5,0.0,&->ustawiamy() @ this_object); //WTF? Kto to da�?
                ustawiamy();
                przydziel_prezenty(byt);
                alrm = set_alarm(3.0,0.0,&fazka(-1,"faza22tak"));

                TP->set_option(OPT_ECHO, 1);
                            //bo kurde nie dziala to set_option() w ustawiamy() :/
                return 0;
            }
            else if(wildmatch_nie(str) || verb=="zaprzecz")
            {
                faza_pomocy=23;
                faza=-1;
                alrm = set_alarm(0.1,0.0,&faza22nie());
                return 0;
            }
            else
            {
                faza_pomocy=-1;
                alrm = set_alarm(0.1,0.0,&faza22inne());
                return 0;
            }
        break;
        }
    
        return 0;
    }

    /* Jako byt astralny, czyli forma eteryczna, pozbawiona ciala - Kilka komend
     * musi byc zablokowanych. :)
     */
    if(verb=="ob" || verb=="obejrzyj" ||verb~="sp�jrz"||verb=="sp"||verb~="oce�")
    {
        if(str=="siebie" || str=="na siebie")
        {
            write("Jeste� bytem astralnym. Jak wi�c m"+
                koncoweczka("�g�by�","og�aby�")+
                " na siebie spojrze�?\n");
            return 1;
        }
        return 0;
    }

    switch(verb)
    {
        case "usi�d�":
        case "czas":
        case "za��":
        case "napij si�":
        case "beknij":
        case "chrz�knij":
        case "czknij":
        case "j�zyk":
        case "kaszlnij":
        case "kichnij":
        case "klepnij":
        case "klaszcz":
        case "kopnij":
        case "machnij":
        case "mrugnij":
        case "nadepnij":
        case "obejmij":
        case "obgry�":
        case "obli� si�":
        case "oczko":
        case "odetchnij":
        case "opluj":
        case "otrz��nij si�":
        case "otrzyj":
        case "otw�rz oczy":
        case "poca�uj":
        case "pierdnij":
        case "poci�gnij":
        case "podaj":
        case "pod�ub":
        case "podrap":
        case "podrepcz":
        case "podskocz":
        case "pog�ad�":
        case "pog�aszcz":
        case "pogr�":
        case "pokiwaj":
        case "poklep":
        case "pok�o�":
        case "pokr��":
        case "po�askocz":
        case "pomachaj":
        case "popukaj":
        case "potrz��nij":
        case "potrzyj":
        case "przebieraj":
        case "przeci�gnij":
        case "prze�knij":
        case "przest�p":
        case "przewr��":
        case "przymru�":
        case "przytul":
        case "pstryknij":
        case "pu��":
        case "roz��":
        case "ski�":
        case "skrzyw si�":
        case "splu�":
        case "spoliczkuj":
        case "szturchnij":
        case "tupnij":
        case "u�ciskaj":
        case "u�miechnij si�":
        case "wyba�usz":
        case "wykrzyw":
        case "wyp�acz":
        case "wytrzeszcz":
        case "wzdrygnij":
        case "zachrumkaj":
        case "zaci�nij":
        case "zaczerwie�":
        case "zadr�yj":
        case "zagry�":
        case "zaklaszcz":
        case "zamknij oczy":
        case "zarumie�":
        case "zata�cz":
        case "zatkaj":
        case "zatrzyj":
        case "zazgrzytaj":
        case "zbe�taj si�":
        case "zblednij":
        case "zdziw si�":
        case "zmarszcz":
        case "zmru�":
        case "zwie�":
        case "nagraj":
        case "zwymiotuj": write("W tym czasie i miejscu nie jest to mo�liwe.\n" +
                    "Je�li chcesz uzyska� pomoc, wpisz '?'.\n"); return 1;
        case "wie�ci":
        case "stan":
        case "system":
        case "ostatnio":
        case "kondycja":
        case "k":
        case "kto": write("Tutaj to nie podzia�a...\n"); return 1;
	//case "opcje":write("Tutaj to nie podzia�a... Zmieni� opcje mo�na dopiero "+
	//    "po uko�czeniu etapu kreowania postaci.\n"); return 1;
        case "?": this_object()->pomoc(str); return 1;

        case "zako�cz":
            write("Posta� nie zosta�a uko�czona, nast�pnym razem zaczniesz " +
                "zn�w od pocz�tku!\n");

            if(!SECURITY->remove_playerfile(lower_case(TP->query_name()),"zako�czenie w wiosce"))
                write("b��d: " + query_notify_fail());

            TP->remove_object();
            return 1;

        default:
#define SPECIAL_NF "_ilosc_wywolan_specjalnego_nf"
            //�adny komunikacik �eby gracze wiedzieli co zrobi�.
        if(TP->query_prop(SPECIAL_NF) % 10 == 0) // co 10 pokazujemy komunikat specjalny inaczej pokazujemy
            //zwyk�e "s�ucham?"
        {
            NF2("Nie mo�esz tego zrobi�. Je�li odpowiedzi� na pytanie jest 'tak' lub 'nie' " +
                "mo�esz 'potwierdzi�' lub 'zaprzeczy�'. W innym przypadku musisz 'odpowiedzie�' " +
                "lub 'powiedzie�' swoj� odpowied�. Mo�esz to zrobi� tak�e zamiast potwierdzenia " +
                "czy zaprzeczenia. Aby dowiedzie� si� wi�cej wpisz '?pomoc' za� aby zobaczy� " +
                "co teraz mo�esz zrobi� u�yj 'pomoc'.\n", 1);
        }
        TP->add_prop(SPECIAL_NF, (TP->query_prop(SPECIAL_NF) + 1));
        return 0;
    }

}

//Dodajemy standardowe umy od rasy.
private void
dodaj_skille()
{
    mapping skille;
    string *tab;
    int x;

    skille = RACESKILL[byt->query_race_name()];
    tab = m_indices(skille);

    x = sizeof(tab);
    while (--x >= 0)
        byt->increase_ss(tab[x],skille[tab[x]]);
        //byt->set_skill(tab[x], skille[tab[x]]);
}

/*Jak wszystko skonczone, to pora zaczac ustawiac w graczu to, co
 *sobie wybral.
 */

void
ustawiamy_gabaryty()
{
    int *tab, *wproc, *hproc, real_wzrost, real_waga;

    int plecx=byt->query_gender(); //a taki tam pierdolniczek... ;P

    jaka_rasa(); //ustawia zmienne 'rasa' i 'race'.
    byt->ustaw_odmiane_rasy(rasa);
    byt->set_race_name(race);

    tab = RACEATTR[race][plecx];
//    tab = RACEATTR[race];
    hproc = HEIGHTPROC;
    wproc = WEIGHTPROC;
    //czasem sie zdazy, ze odrobine zaniza wzrost!
    //wybrane:wysoka, a jest:przecietnego wzrostu..
    //za choler� nie ogarniam czemu...
    real_wzrost = tab[0] * (hproc[wzrost] + random(hproc[wzrost + 1] -
        hproc[wzrost])) / 100;
    real_waga = real_wzrost * tab[2] * (wproc[waga] +
        random(wproc[waga + 1] - wproc[waga])) / 100;

    byt->add_prop(CONT_I_WEIGHT,real_waga);
    byt->add_prop(CONT_I_VOLUME,real_waga);
    byt->add_prop(CONT_I_HEIGHT,real_wzrost);

    /*Wywo�ujemy w npcu funkcje, w kt�rej przypisywane s� prezenty.
     *podajemy jako argument gracza, na ktorym bedziemy mierzyc
        rozmiary */
}

void
komunikat_na_koniec(object b)
{
    b->catch_msg("\n\tBudzisz si� z tego dziwnego snu...\n\n");

    b->catch_msg("Witaj! Zdaje si�, �e logujesz si� do �wiata po raz pierwszy - "+
        "w takim razie czym pr�dzej zapoznaj si� z zasadami "+
        "(wpisz: \"?zasady\"). "+
        "Przydatn� pomoc zdob�dziesz tak�e "+
        "pod: \"?wprowadzenie\".\nAhoj przygodo!\n\n");
    b->ghost_ready();
}

void
ustawiamy()
{
    ustawiamy_gabaryty();

    int x, stat, plec;
    mapping skille;
    int bleh=byt->query_stat(atr_puli); //o, a to drugi pierdolniczek ;)
    int p_oczy=3, p_wlosy=3, p_budowa=0, p_szczegolna=9,
        p_zarost=0; //priorytety przymiotnikow

    byt->set_kolor_oczu(tmp_kolor_oczu);
    byt->set_wiek(tmp_wiek);
    byt->set_origin(tmp_pochodzenie);;
    byt->set_naturalny_kolor_wlosow(tmp_kolor_wlosow,tmp_przym_wlosy[0],
                                    tmp_przym_wlosy[1]);
    byt->set_dlugosc_wlosow(tmp_dlugosc_wlosow);

    if(lysy_na_stale)
        byt->set_lysy_na_stale(lysy_na_stale);

    if(byt->query_gender() == 0)
    {
        byt->set_dlugosc_brody(tmp_dlugosc_brody);
        byt->set_dlugosc_wasow(tmp_dlugosc_wasow);
    }

    byt->set_budowa_ciala(tmp_cecha_ciala);
    byt->set_cecha_szczegolna(tmp_cecha_szczegolna);
    byt->set_ghost(0);
    byt->set_option(OPT_ECHO, 1);  //nie ustawia sie to...wrzucilam wyzej.
    byt->set_whimpy(40);
    byt->set_m_in("przybywa"); //kurde, ze tez trzeba takie rzeczy pisac?!
    byt->set_m_out("pod��a");  //ehhh...

    byt->set_learn_pref(0);

    for (x = 0; x < SS_NO_STATS; x++)
    {
        stat = random(SS_NO_STATS);
        byt->set_acc_exp(stat, (byt->query_acc_exp(stat) + 150));
        
        if(member_array(modyfikator_od_ciala, ({"s","w","z"})) == -1) //je�li �adnego moda od cia�a nie ma
            byt->modify_stat(x,36,"Tworzenie postaci");
        else
            byt->modify_stat(x,30,"Tworzenie postaci");
    }

    //tutaj dodajemy do cechy jesli wybrana budowa ciala tak mowi...
    if(modyfikator_od_ciala == "s")
        byt->modify_stat(0, (bleh+modminus), "Tworzenie postaci");
    if(modyfikator_od_ciala == "z")
        byt->modify_stat(1, (bleh+modminus), "Tworzenie postaci");
    if(modyfikator_od_ciala == "w")
        byt->modify_stat(2, (bleh+modminus), "Tworzenie postaci");
    
    
    byt->modify_stat(atr_puli,(bleh+pula), "Tworzenie postaci");

//    byt->acc_exp_to_stats();
//    byt->stats_to_acc_exp();
    dodaj_skille(); //te podstawowe, od rasy.

    byt->increase_ss(tmp_skill1, 10600); //te wybrane
    byt->increase_ss(tmp_skill2, 10600);
    byt->increase_ss(tmp_skill1, 7000, EXP_TEORETYCZNY);
    byt->increase_ss(tmp_skill2, 7000, EXP_TEORETYCZNY);

    //---------DODAJEMY PRZYMIOTNIKI-----------
    //Ustawiamy wpierw priorytety oczu
    switch(tmp_kolor_oczu[0]) //od 3 do 7
    {
        case "b��kitnooki":
        case "niebieskooki":
        case "szarooki":
        case "matowooki":       p_oczy = 3;     break;
        case "br�zowooki":
        case "jasnooki":
        case "migda�owooki":    p_oczy = 4;     break;
        case "ciemnooki":
        case "modrooki":
        case "piwnooki":
        case "��tooki":
        case "stalowooki":
        case "zielonooki":
        case "promiennooki":    p_oczy = 5;     break;
        case "czarnooki":
        case "fio�kowooki":
        case "p�omiennooki":
        case "rybiooki":
        case "sko�nooki":
        case "z�otooki":
        case "srebrnooki":      p_oczy = 6;     break;
        case "czerwonooki":     p_oczy = 7;     break;
        default:                p_oczy = 4;     break;
    }
    //Priorytety koloru wlosow
    switch(tmp_kolor_wlosow) //od 3 do 7
    {
        case "ciemne":
        case "jasne":           p_wlosy = 3;        break;
        case "czarne":
        case "kruczoczarne":
        case "siwe":
        case "kasztanowe":      p_wlosy = 4;        break;
        case "miedziane":
        case "popielate":       p_wlosy = 5;        break;
        case "bia�e":
        case "ogniste":
        case "rude":
        case "z�ote":
        case "z�ociste":
        case "srebrne":
        case "pomara�czowe":    p_wlosy = 6;        break;
        case "czerwone":
        case "zielone":         p_wlosy = 7;        break;
        case "matowe":          p_wlosy = 0;        break; //"matowe"=brak
        default:                p_wlosy = 4;        break;
    }

    //Priorytety budowy ciala (sa w atrybutach) ;)
    switch(priorytet_od_ciala)
    {
        case "zero":        p_budowa = 0;           break;
        case "jeden":       p_budowa = 1;           break;
        case "dwa":         p_budowa = 2;           break;
        case "trzy":        p_budowa = 3;           break;
        case "cztery":      p_budowa = 4;           break;
        case "piec":        p_budowa = 5;           break;
        case "szesc":       p_budowa = 6;           break;
        case "siedem":      p_budowa = 7;           break;
        case "osiem":       p_budowa = 8;           break;
        case "dziewiec":    p_budowa = 9;           break;
        default:            p_budowa = 0;           break;
    }

    string *kolor_zarostu=({});

    //Priorytety od zarostu... Od razu ustawiamy kolor_zarostu
    if(byt->query_gender() == 0 && race != "elf" && tmp_wiek > 19)
    {
        string kolor;
        switch(tmp_kolor_wlosow)
        {
            case "bia�e":
                kolor = "bia��";
                p_zarost = 4;
                kolor_zarostu = ({kolor, "bia�obrody", "bia�obrodzi"});
                break;
            case "z�ote":
            case "jasne":
                kolor = "jasn�";
                p_zarost = 3;
                kolor_zarostu = ({kolor, "jasnobrody", "jasnobrodzi"});
                break;
            case "siwe":
            case "srebrne":
                kolor = "siw�";
                p_zarost = 3;
                kolor_zarostu = ({kolor, "siwobrody", "siwobrodzi"});
                break;
            case "popielate":
                kolor = "popielat�";
                p_zarost = 3;
                kolor_zarostu = ({kolor, "popielatobrody", "popielatobrodzi"});
                break;
            case "kasztanowe":
            case "zielone":
            case "ciemne":
                kolor = "ciemn�";
                p_zarost = 5;
                kolor_zarostu = ({kolor, "ciemnobrody", "ciemnobrodzi"});
                break;
            case "rude":
                kolor = "rud�";
                p_zarost = 6;
                kolor_zarostu = ({kolor, "rudobrody", "rudobrodzi"});
                break;
            case "pomara�czowe":
                kolor = "pomara�czow�";
                p_zarost = 7;
                kolor_zarostu = ({kolor, "pomara�czowobrody", "pomara�czowoobrodzi"});
                break;
            case "ogniste":
            case "czerwone":
                kolor = "czerwon�";
                p_zarost = 7;
                kolor_zarostu = ({kolor, "czerwonobrody", "czerwonobrodzi"});
                break;
            case "kruczoczarne":
            case "czarne":
                kolor = "czarn�";
                p_zarost = 5;
                kolor_zarostu = ({kolor, "czarnobrody", "czarnobrodzi"});
                break;
            default:
                kolor = tmp_kolor_wlosow;
                p_zarost = 4;
                kolor = kolor[..-2] + "�";
                string bla1 = explode(tmp_przym_wlosy[0], "w^losy")[0] + "brody";
                string bla2 = explode(tmp_przym_wlosy[1], "w^losi")[0] + "brodzi";
                string *blabla_arr = ({bla1, bla2});
                kolor_zarostu = ({kolor, blabla_arr[0], blabla_arr[1]});
                break;
        }

        byt->set_kolor_zarostu(kolor_zarostu);

    }

    //Priorytety cech szczegolnych
    if(sizeof(tmp_cecha_szczegolna))
    {
        switch(tmp_cecha_szczegolna[0])
        {
            case "bezz�bny":    p_szczegolna = 4;   break;
            default:            p_szczegolna = 9;   break;
        }
    }

     /*Dodajemy przymiotniki do listy:
      Na dziendobry beda to: cecha szczegolna oraz  budowa ciala,
      wlosy lub jesli nie ma to "�ysy", kolor oczu,
      broda, je�li posiada*/

    if(tmp_dlugosc_wlosow>=3.0)
    {
       //byt->dodaj_przym_do_listy(p_wlosy,wloski[tmp_przym_wlosy][0],
         //            wloski[tmp_przym_wlosy][1]);
       byt->dodaj_przym_do_listy(p_wlosy, tmp_przym_wlosy[0], tmp_przym_wlosy[1]);
       byt->set_priorytet_naturalnego(p_wlosy);
    }
    else
       byt->dodaj_przym_do_listy(8, "�ysy", "�ysi");

    if(sizeof(tmp_cecha_szczegolna))
    {
        byt->dodaj_przym_do_listy(p_szczegolna, tmp_cecha_szczegolna[0], tmp_cecha_szczegolna[1]);
    }
    byt->dodaj_przym_do_listy(p_budowa, tmp_cecha_ciala[0], tmp_cecha_ciala[1]);
    byt->dodaj_przym_do_listy(p_oczy, tmp_kolor_oczu[0], tmp_kolor_oczu[1]);

    if(tmp_dlugosc_brody > 12.0)
    {
        byt->dodaj_przym_do_listy(p_zarost, kolor_zarostu[1],kolor_zarostu[2]);
        byt->set_priorytet_zarostu(p_zarost);
    }

    byt->init_zarost();
    byt->set_hp(0,byt->query_max_hp());

    set_alarm(5.0, 0.0, &komunikat_na_koniec(byt));

    return;
}




void set_signal_logout() {  signal_logout = 1; }
int query_signal_logout() { return signal_logout; }
// Kurde - kto� wie o co tu chodzi - kto� wie co autor mia�
// na my�li? Czy�by usuwanie wioski?
//Si - Odpowiada Vera.
void
on_player_logout(object player)
{
    foreach(object x : (mixed *)query_exit_rooms() - ({ 0 }))
    {
        if(!x->query_signal_logout())
            x->on_player_logout(player);
        x->set_signal_logout();
    }

    set_alarm(1.5, 0.0, &(this_object())->remove_object());
    
//     SECURITY->remove_playerfile(player, "niedokonczona_postac");
//     player->set_ghost(1);
    
}
