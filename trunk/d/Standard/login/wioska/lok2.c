	/*
 *  Kuchnia wioski z etapu tworzenia postaci.
 *        By Lil & Aneta (long)
 *                          Wed Mar 22 2006
 */

inherit "/d/Standard/login/wioska/std";

#include <std.h>
#include <stdproperties.h>
#include <macros.h>
#include <ss_types.h>
#include <composite.h>
#include <filter_funs.h>
#include <materialy.h>
#include <language.h>
#include <state_desc.h>
#include "login.h"

#define UBRANIA "/d/Standard/items/ubrania/"
#define PREZENTY "/d/Standard/login/wioska/items/"

object gift1, gift2, gift3, gift4; //prezenty :P
object kubek;
object talerz;
object npc;
int faza_reakcji=0; //jesli jest 1, to npc nie reaguje na wybory np. mowiac
             //"Wspaniale."
             //dotyczy to przypadkow kiedy gracz np. stwierdzi, ze nie chce byc
             //danej rasy. Wtedy to by brzmialo bez sensu.
object byt=this_player();

public void
create_wioska_room()
{
    set_short("Kuchnia nizio�ka");

    add_exit("lok1",({"wyj�cie","do wyj�cia","z kuchni"}));
    add_subloc("na p�ce",0,"z p�ki");
    add_item(({"p�ki","p�k�"}),
            "Be�owe p�ki znajduj� si� po przeciwleg�ej "+
            "stronie drzwi. S� zakurzone i brudne."+
            "@@opis_sublokacji| Dostrzegasz na nich |na p�ce|.|||||@@\n");

    dodaj_rzecz_niewyswietlana("niewielki kocio�ek");
    dodaj_rzecz_niewyswietlana("niewielki kocio�ek");

    npc=clone_object(PATH+"wioska/npc/niziolek.c");
    npc->init_arg(0);
    npc->move(this_object());

    clone_object(PATH+"wioska/items/rondel.c")->move(this_object(),
                "na p�ce");
    clone_object(PATH+"wioska/items/rondel.c")->move(this_object(),
                "na p�ce");
    clone_object(PATH+"wioska/items/rondel.c")->move(this_object(),
                "na p�ce");
    clone_object(PATH+"wioska/items/rondel.c")->move(this_object(),
                "na p�ce");
    clone_object(PATH+"wioska/items/kociolek.c")->move(this_object());
    clone_object(PATH+"wioska/items/kociolek.c")->move(this_object());
    clone_object(PATH+"wioska/items/patelnia.c")->move(this_object(),
                "na p�ce");
    clone_object(PATH+"wioska/items/patelnia.c")->move(this_object(),
                "na p�ce");
    clone_object(PATH+"wioska/items/maly_garnek.c")->move(this_object(),
                "na p�ce");
    clone_object(PATH+"wioska/items/maly_garnek.c")->move(this_object(),
                "na p�ce");
    clone_object(PATH+"wioska/items/maly_garnek.c")->move(this_object(),
                "na p�ce");
    clone_object(PATH+"wioska/items/maly_garnek.c")->move(this_object(),
                "na p�ce");
    clone_object(PATH+"wioska/items/maly_garnek.c")->move(this_object(),
                "na p�ce");
    clone_object(PATH+"wioska/items/maly_garnek.c")->move(this_object(),
                "na p�ce");
    clone_object(PATH+"wioska/items/maly_garnek.c")->move(this_object(),
                "na p�ce");
    clone_object(PATH+"wioska/items/stary_garnek.c")->move(this_object(),
                "na p�ce");
    clone_object(PATH+"wioska/items/stary_garnek.c")->move(this_object(),
                "na p�ce");
    clone_object(PATH+"wioska/items/stary_garnek.c")->move(this_object(),
                "na p�ce");
    clone_object(PATH+"wioska/items/stary_garnek.c")->move(this_object(),
                "na p�ce");

    add_sit("na ziemi","na ziemi","z ziemi",0);

    kubek = clone_object("/lib/pub_naczynie_picie.c");
    kubek->ustaw_nazwe("kubek");
    kubek->dodaj_przym("gliniany","gliniani");
    kubek->ustaw_material(MATERIALY_GLINA,100);
    kubek->move(this_object());
    talerz = clone_object("/lib/pub_naczynie_jedzenie.c");
    talerz->ustaw_nazwe("talerz");
    talerz->dodaj_przym("st�uczony","st�uczeni");
    talerz->dodaj_przym("gliniany","gliniani");
    talerz->ustaw_materialy(MATERIALY_GLINA,100);
    talerz->move(this_object());
    kubek = clone_object("/lib/pub_naczynie_picie.c");
    kubek->ustaw_nazwe("kubek");
    kubek->dodaj_przym("gliniany","gliniani");
    kubek->ustaw_material(MATERIALY_GLINA,100);
    kubek->move(this_object());
    talerz = clone_object("/lib/pub_naczynie_jedzenie.c");
    talerz->ustaw_nazwe("talerz");
    talerz->dodaj_przym("st�uczony","st�uczeni");
    talerz->dodaj_przym("gliniany","gliniani");
    talerz->ustaw_materialy(MATERIALY_GLINA,100);
    talerz->move(this_object());
    kubek = clone_object("/lib/pub_naczynie_picie.c");
    kubek->ustaw_nazwe("kubek");
    kubek->dodaj_przym("gliniany","gliniani");
    kubek->ustaw_material(MATERIALY_GLINA,100);
    kubek->move(this_object());
    talerz = clone_object("/lib/pub_naczynie_jedzenie.c");
    talerz->ustaw_nazwe("talerz");
    talerz->dodaj_przym("st�uczony","st�uczeni");
    talerz->dodaj_przym("gliniany","gliniani");
    talerz->ustaw_materialy(MATERIALY_GLINA,100);
    talerz->move(this_object());
    kubek = clone_object("/lib/pub_naczynie_picie.c");
    kubek->ustaw_nazwe("kubek");
    kubek->dodaj_przym("gliniany","gliniani");
    kubek->ustaw_material(MATERIALY_GLINA,100);
    kubek->move(this_object());
    talerz = clone_object("/lib/pub_naczynie_jedzenie.c");
    talerz->ustaw_nazwe("talerz");
    talerz->dodaj_przym("st�uczony","st�uczeni");
    talerz->dodaj_przym("gliniany","gliniani");
    talerz->ustaw_materialy(MATERIALY_GLINA,100);
    talerz->move(this_object());
    kubek = clone_object("/lib/pub_naczynie_picie.c");
    kubek->ustaw_nazwe("kubek");
    kubek->dodaj_przym("gliniany","gliniani");
    kubek->ustaw_material(MATERIALY_GLINA,100);
    kubek->move(this_object());
    talerz = clone_object("/lib/pub_naczynie_jedzenie.c");
    talerz->ustaw_nazwe("talerz");
    talerz->dodaj_przym("st�uczony","st�uczeni");
    talerz->dodaj_przym("gliniany","gliniani");
    talerz->ustaw_materialy(MATERIALY_GLINA,100);
    talerz->move(this_object());
    kubek = clone_object("/lib/pub_naczynie_picie.c");
    kubek->ustaw_nazwe("kubek");
    kubek->dodaj_przym("gliniany","gliniani");
    kubek->ustaw_material(MATERIALY_GLINA,100);
    kubek->move(this_object());
    talerz = clone_object("/lib/pub_naczynie_jedzenie.c");
    talerz->ustaw_nazwe("talerz");
    talerz->dodaj_przym("st�uczony","st�uczeni");
    talerz->dodaj_przym("gliniany","gliniani");
    talerz->ustaw_materialy(MATERIALY_GLINA,100);
    talerz->move(this_object());
    kubek = clone_object("/lib/pub_naczynie_picie.c");
    kubek->ustaw_nazwe("kubek");
    kubek->dodaj_przym("gliniany","gliniani");
    kubek->ustaw_material(MATERIALY_GLINA,100);
    kubek->move(this_object());
    talerz = clone_object("/lib/pub_naczynie_jedzenie.c");
    talerz->ustaw_nazwe("talerz");
    talerz->dodaj_przym("st�uczony","st�uczeni");
    talerz->dodaj_przym("gliniany","gliniani");
    talerz->ustaw_materialy(MATERIALY_GLINA,100);
    talerz->move(this_object());
    kubek = clone_object("/lib/pub_naczynie_picie.c");
    kubek->ustaw_nazwe("kubek");
    kubek->dodaj_przym("gliniany","gliniani");
    kubek->ustaw_material(MATERIALY_GLINA,100);
    kubek->move(this_object());
    talerz = clone_object("/lib/pub_naczynie_jedzenie.c");
    talerz->ustaw_nazwe("talerz");
    talerz->dodaj_przym("st�uczony","st�uczeni");
    talerz->dodaj_przym("gliniany","gliniani");
    talerz->ustaw_materialy(MATERIALY_GLINA,100);
    talerz->move(this_object());
    kubek = clone_object("/lib/pub_naczynie_picie.c");
    kubek->ustaw_nazwe("kubek");
    kubek->dodaj_przym("gliniany","gliniani");
    kubek->ustaw_material(MATERIALY_GLINA,100);
    kubek->move(this_object());
    talerz = clone_object("/lib/pub_naczynie_jedzenie.c");
    talerz->ustaw_nazwe("talerz");
    talerz->dodaj_przym("st�uczony","st�uczeni");
    talerz->dodaj_przym("gliniany","gliniani");
    talerz->ustaw_materialy(MATERIALY_GLINA,100);
    talerz->move(this_object());
    kubek = clone_object("/lib/pub_naczynie_picie.c");
    kubek->ustaw_nazwe("kubek");
    kubek->dodaj_przym("gliniany","gliniani");
    kubek->ustaw_material(MATERIALY_GLINA,100);
    kubek->move(this_object());
    talerz = clone_object("/lib/pub_naczynie_jedzenie.c");
    talerz->ustaw_nazwe("talerz");
    talerz->dodaj_przym("st�uczony","st�uczeni");
    talerz->dodaj_przym("gliniany","gliniani");
    talerz->ustaw_materialy(MATERIALY_GLINA,100);
    talerz->move(this_object());
    kubek = clone_object("/lib/pub_naczynie_picie.c");
    kubek->ustaw_nazwe("kubek");
    kubek->dodaj_przym("gliniany","gliniani");
    kubek->ustaw_material(MATERIALY_GLINA,100);
    kubek->move(this_object());
    talerz = clone_object("/lib/pub_naczynie_jedzenie.c");
    talerz->ustaw_nazwe("talerz");
    talerz->dodaj_przym("st�uczony","st�uczeni");
    talerz->dodaj_przym("gliniany","gliniani");
    talerz->ustaw_materialy(MATERIALY_GLINA,100);
    talerz->move(this_object());
    kubek = clone_object("/lib/pub_naczynie_picie.c");
    kubek->ustaw_nazwe("kubek");
    kubek->dodaj_przym("gliniany","gliniani");
    kubek->ustaw_material(MATERIALY_GLINA,100);
    kubek->move(this_object());
    talerz = clone_object("/lib/pub_naczynie_jedzenie.c");
    talerz->ustaw_nazwe("talerz");
    talerz->dodaj_przym("st�uczony","st�uczeni");
    talerz->dodaj_przym("gliniany","gliniani");
    talerz->ustaw_materialy(MATERIALY_GLINA,100);
    talerz->move(this_object());
    kubek = clone_object("/lib/pub_naczynie_picie.c");
    kubek->ustaw_nazwe("kubek");
    kubek->dodaj_przym("gliniany","gliniani");
    kubek->ustaw_material(MATERIALY_GLINA,100);
    kubek->move(this_object());
    talerz = clone_object("/lib/pub_naczynie_jedzenie.c");
    talerz->ustaw_nazwe("talerz");
    talerz->dodaj_przym("st�uczony","st�uczeni");
    talerz->dodaj_przym("gliniany","gliniani");
    talerz->ustaw_materialy(MATERIALY_GLINA,100);
    talerz->move(this_object());
    kubek = clone_object("/lib/pub_naczynie_picie.c");
    kubek->ustaw_nazwe("kubek");
    kubek->dodaj_przym("gliniany","gliniani");
    kubek->ustaw_material(MATERIALY_GLINA,100);
    kubek->move(this_object());
    talerz = clone_object("/lib/pub_naczynie_jedzenie.c");
    talerz->ustaw_nazwe("talerz");
    talerz->dodaj_przym("st�uczony","st�uczeni");
    talerz->dodaj_przym("gliniany","gliniani");
    talerz->ustaw_materialy(MATERIALY_GLINA,100);
    talerz->move(this_object());
    kubek = clone_object("/lib/pub_naczynie_picie.c");
    kubek->ustaw_nazwe("kubek");
    kubek->dodaj_przym("gliniany","gliniani");
    kubek->ustaw_material(MATERIALY_GLINA,100);
    kubek->move(this_object());
    talerz = clone_object("/lib/pub_naczynie_jedzenie.c");
    talerz->ustaw_nazwe("talerz");
    talerz->dodaj_przym("st�uczony","st�uczeni");
    talerz->dodaj_przym("gliniany","gliniani");
    talerz->ustaw_materialy(MATERIALY_GLINA,100);
    talerz->move(this_object());
    kubek = clone_object("/lib/pub_naczynie_picie.c");
    kubek->ustaw_nazwe("kubek");
    kubek->dodaj_przym("gliniany","gliniani");
    kubek->ustaw_material(MATERIALY_GLINA,100);
    kubek->move(this_object());
    talerz = clone_object("/lib/pub_naczynie_jedzenie.c");
    talerz->ustaw_nazwe("talerz");
    talerz->dodaj_przym("st�uczony","st�uczeni");
    talerz->dodaj_przym("gliniany","gliniani");
    talerz->ustaw_materialy(MATERIALY_GLINA,100);
    talerz->move(this_object());
    kubek = clone_object("/lib/pub_naczynie_picie.c");
    kubek->ustaw_nazwe("kubek");
    kubek->dodaj_przym("gliniany","gliniani");
    kubek->ustaw_material(MATERIALY_GLINA,100);
    kubek->move(this_object());
    talerz = clone_object("/lib/pub_naczynie_jedzenie.c");
    talerz->ustaw_nazwe("talerz");
    talerz->dodaj_przym("st�uczony","st�uczeni");
    talerz->dodaj_przym("gliniany","gliniani");
    talerz->ustaw_materialy(MATERIALY_GLINA,100);
    talerz->move(this_object());
    kubek = clone_object("/lib/pub_naczynie_picie.c");
    kubek->ustaw_nazwe("kubek");
    kubek->dodaj_przym("gliniany","gliniani");
    kubek->ustaw_material(MATERIALY_GLINA,100);
    kubek->move(this_object());
    talerz = clone_object("/lib/pub_naczynie_jedzenie.c");
    talerz->ustaw_nazwe("talerz");
    talerz->dodaj_przym("st�uczony","st�uczeni");
    talerz->dodaj_przym("gliniany","gliniani");
    talerz->ustaw_materialy(MATERIALY_GLINA,100);
    talerz->move(this_object());
    kubek = clone_object("/lib/pub_naczynie_picie.c");
    kubek->ustaw_nazwe("kubek");
    kubek->dodaj_przym("gliniany","gliniani");
    kubek->ustaw_material(MATERIALY_GLINA,100);
    kubek->move(this_object());
    talerz = clone_object("/lib/pub_naczynie_jedzenie.c");
    talerz->ustaw_nazwe("talerz");
    talerz->dodaj_przym("st�uczony","st�uczeni");
    talerz->dodaj_przym("gliniany","gliniani");
    talerz->ustaw_materialy(MATERIALY_GLINA,100);
    talerz->move(this_object());
    kubek = clone_object("/lib/pub_naczynie_picie.c");
    kubek->ustaw_nazwe("kubek");
    kubek->dodaj_przym("gliniany","gliniani");
    kubek->ustaw_material(MATERIALY_GLINA,100);
    kubek->move(this_object());
    talerz = clone_object("/lib/pub_naczynie_jedzenie.c");
    talerz->ustaw_nazwe("talerz");
    talerz->dodaj_przym("st�uczony","st�uczeni");
    talerz->dodaj_przym("gliniany","gliniani");
    talerz->ustaw_materialy(MATERIALY_GLINA,100);
    talerz->move(this_object());
    kubek = clone_object("/lib/pub_naczynie_picie.c");
    kubek->ustaw_nazwe("kubek");
    kubek->dodaj_przym("gliniany","gliniani");
    kubek->ustaw_material(MATERIALY_GLINA,100);
    kubek->move(this_object());
    talerz = clone_object("/lib/pub_naczynie_jedzenie.c");
    talerz->ustaw_nazwe("talerz");
    talerz->dodaj_przym("st�uczony","st�uczeni");
    talerz->dodaj_przym("gliniany","gliniani");
    talerz->ustaw_materialy(MATERIALY_GLINA,100);
    talerz->move(this_object());
    kubek = clone_object("/lib/pub_naczynie_picie.c");
    kubek->ustaw_nazwe("kubek");
    kubek->dodaj_przym("gliniany","gliniani");
    kubek->ustaw_material(MATERIALY_GLINA,100);
    kubek->move(this_object());
    talerz = clone_object("/lib/pub_naczynie_jedzenie.c");
    talerz->ustaw_nazwe("talerz");
    talerz->dodaj_przym("st�uczony","st�uczeni");
    talerz->dodaj_przym("gliniany","gliniani");
    talerz->ustaw_materialy(MATERIALY_GLINA,100);
    talerz->move(this_object());
    kubek = clone_object("/lib/pub_naczynie_picie.c");
    kubek->ustaw_nazwe("kubek");
    kubek->dodaj_przym("gliniany","gliniani");
    kubek->ustaw_material(MATERIALY_GLINA,100);
    kubek->move(this_object());
    talerz = clone_object("/lib/pub_naczynie_jedzenie.c");
    talerz->ustaw_nazwe("talerz");
    talerz->dodaj_przym("st�uczony","st�uczeni");
    talerz->dodaj_przym("gliniany","gliniani");
    talerz->ustaw_materialy(MATERIALY_GLINA,100);
    talerz->move(this_object());
    kubek = clone_object("/lib/pub_naczynie_picie.c");
    kubek->ustaw_nazwe("kubek");
    kubek->dodaj_przym("gliniany","gliniani");
    kubek->ustaw_material(MATERIALY_GLINA,100);
    kubek->move(this_object());
    talerz = clone_object("/lib/pub_naczynie_jedzenie.c");
    talerz->ustaw_nazwe("talerz");
    talerz->dodaj_przym("st�uczony","st�uczeni");
    talerz->dodaj_przym("gliniany","gliniani");
    talerz->ustaw_materialy(MATERIALY_GLINA,100);
    talerz->move(this_object());
    kubek = clone_object("/lib/pub_naczynie_picie.c");
    kubek->ustaw_nazwe("kubek");
    kubek->dodaj_przym("gliniany","gliniani");
    kubek->ustaw_material(MATERIALY_GLINA,100);
    kubek->move(this_object());
    talerz = clone_object("/lib/pub_naczynie_jedzenie.c");
    talerz->ustaw_nazwe("talerz");
    talerz->dodaj_przym("st�uczony","st�uczeni");
    talerz->dodaj_przym("gliniany","gliniani");
    talerz->ustaw_materialy(MATERIALY_GLINA,100);
    talerz->move(this_object());
    kubek = clone_object("/lib/pub_naczynie_picie.c");
    kubek->ustaw_nazwe("kubek");
    kubek->dodaj_przym("gliniany","gliniani");
    kubek->ustaw_material(MATERIALY_GLINA,100);
    kubek->move(this_object());
    talerz = clone_object("/lib/pub_naczynie_jedzenie.c");
    talerz->ustaw_nazwe("talerz");
    talerz->dodaj_przym("st�uczony","st�uczeni");
    talerz->dodaj_przym("gliniany","gliniani");
    talerz->ustaw_materialy(MATERIALY_GLINA,100);
    talerz->move(this_object());
    kubek = clone_object("/lib/pub_naczynie_picie.c");
    kubek->ustaw_nazwe("kubek");
    kubek->dodaj_przym("gliniany","gliniani");
    kubek->ustaw_material(MATERIALY_GLINA,100);
    kubek->move(this_object());
    talerz = clone_object("/lib/pub_naczynie_jedzenie.c");
    talerz->ustaw_nazwe("talerz");
    talerz->dodaj_przym("st�uczony","st�uczeni");
    talerz->dodaj_przym("gliniany","gliniani");
    talerz->ustaw_materialy(MATERIALY_GLINA,100);
    talerz->move(this_object());
    kubek = clone_object("/lib/pub_naczynie_picie.c");
    kubek->ustaw_nazwe("kubek");
    kubek->dodaj_przym("gliniany","gliniani");
    kubek->ustaw_material(MATERIALY_GLINA,100);
    kubek->move(this_object());
    talerz = clone_object("/lib/pub_naczynie_jedzenie.c");
    talerz->ustaw_nazwe("talerz");
    talerz->dodaj_przym("st�uczony","st�uczeni");
    talerz->dodaj_przym("gliniany","gliniani");
    talerz->ustaw_materialy(MATERIALY_GLINA,100);
    talerz->move(this_object());
    kubek = clone_object("/lib/pub_naczynie_picie.c");
    kubek->ustaw_nazwe("kubek");
    kubek->dodaj_przym("gliniany","gliniani");
    kubek->ustaw_material(MATERIALY_GLINA,100);
    kubek->move(this_object());
    talerz = clone_object("/lib/pub_naczynie_jedzenie.c");
    talerz->ustaw_nazwe("talerz");
    talerz->dodaj_przym("st�uczony","st�uczeni");
    talerz->dodaj_przym("gliniany","gliniani");
    talerz->ustaw_materialy(MATERIALY_GLINA,100);
    talerz->move(this_object());
    kubek = clone_object("/lib/pub_naczynie_picie.c");
    kubek->ustaw_nazwe("kubek");
    kubek->dodaj_przym("gliniany","gliniani");
    kubek->ustaw_material(MATERIALY_GLINA,100);
    kubek->move(this_object());
    talerz = clone_object("/lib/pub_naczynie_jedzenie.c");
    talerz->ustaw_nazwe("talerz");
    talerz->dodaj_przym("st�uczony","st�uczeni");
    talerz->dodaj_przym("gliniany","gliniani");
    talerz->ustaw_materialy(MATERIALY_GLINA,100);
    talerz->move(this_object());
    kubek = clone_object("/lib/pub_naczynie_picie.c");
    kubek->ustaw_nazwe("kubek");
    kubek->dodaj_przym("gliniany","gliniani");
    kubek->ustaw_material(MATERIALY_GLINA,100);
    kubek->move(this_object());
    talerz = clone_object("/lib/pub_naczynie_jedzenie.c");
    talerz->ustaw_nazwe("talerz");
    talerz->dodaj_przym("st�uczony","st�uczeni");
    talerz->dodaj_przym("gliniany","gliniani");
    talerz->ustaw_materialy(MATERIALY_GLINA,100);
    talerz->move(this_object());
    kubek = clone_object("/lib/pub_naczynie_picie.c");
    kubek->ustaw_nazwe("kubek");
    kubek->dodaj_przym("gliniany","gliniani");
    kubek->ustaw_material(MATERIALY_GLINA,100);
    kubek->move(this_object());
    talerz = clone_object("/lib/pub_naczynie_jedzenie.c");
    talerz->ustaw_nazwe("talerz");
    talerz->dodaj_przym("st�uczony","st�uczeni");
    talerz->dodaj_przym("gliniany","gliniani");
    talerz->ustaw_materialy(MATERIALY_GLINA,100);
    talerz->move(this_object());
    kubek = clone_object("/lib/pub_naczynie_picie.c");
    kubek->ustaw_nazwe("kubek");
    kubek->dodaj_przym("gliniany","gliniani");
    kubek->ustaw_material(MATERIALY_GLINA,100);
    kubek->move(this_object());
    talerz = clone_object("/lib/pub_naczynie_jedzenie.c");
    talerz->ustaw_nazwe("talerz");
    talerz->dodaj_przym("st�uczony","st�uczeni");
    talerz->dodaj_przym("gliniany","gliniani");
    talerz->ustaw_materialy(MATERIALY_GLINA,100);
    talerz->move(this_object());
    kubek = clone_object("/lib/pub_naczynie_picie.c");
    kubek->ustaw_nazwe("kubek");
    kubek->dodaj_przym("gliniany","gliniani");
    kubek->ustaw_material(MATERIALY_GLINA,100);
    kubek->move(this_object());
    talerz = clone_object("/lib/pub_naczynie_jedzenie.c");
    talerz->ustaw_nazwe("talerz");
    talerz->dodaj_przym("st�uczony","st�uczeni");
    talerz->dodaj_przym("gliniany","gliniani");
    talerz->ustaw_materialy(MATERIALY_GLINA,100);
    talerz->move(this_object());
    kubek = clone_object("/lib/pub_naczynie_picie.c");
    kubek->ustaw_nazwe("kubek");
    kubek->dodaj_przym("gliniany","gliniani");
    kubek->ustaw_material(MATERIALY_GLINA,100);
    kubek->move(this_object());
    talerz = clone_object("/lib/pub_naczynie_jedzenie.c");
    talerz->ustaw_nazwe("talerz");
    talerz->dodaj_przym("st�uczony","st�uczeni");
    talerz->dodaj_przym("gliniany","gliniani");
    talerz->ustaw_materialy(MATERIALY_GLINA,100);
    talerz->move(this_object());
    kubek = clone_object("/lib/pub_naczynie_picie.c");
    kubek->ustaw_nazwe("kubek");
    kubek->dodaj_przym("gliniany","gliniani");
    kubek->ustaw_material(MATERIALY_GLINA,100);
    kubek->move(this_object());
    talerz = clone_object("/lib/pub_naczynie_jedzenie.c");
    talerz->ustaw_nazwe("talerz");
    talerz->dodaj_przym("st�uczony","st�uczeni");
    talerz->dodaj_przym("gliniany","gliniani");
    talerz->ustaw_materialy(MATERIALY_GLINA,100);
    talerz->move(this_object());
    kubek = clone_object("/lib/pub_naczynie_picie.c");
    kubek->ustaw_nazwe("kubek");
    kubek->dodaj_przym("gliniany","gliniani");
    kubek->ustaw_material(MATERIALY_GLINA,100);
    kubek->move(this_object());
    talerz = clone_object("/lib/pub_naczynie_jedzenie.c");
    talerz->ustaw_nazwe("talerz");
    talerz->dodaj_przym("st�uczony","st�uczeni");
    talerz->dodaj_przym("gliniany","gliniani");
    talerz->ustaw_materialy(MATERIALY_GLINA,100);
    talerz->move(this_object());
    talerz = clone_object("/lib/pub_naczynie_jedzenie.c");
    talerz->ustaw_nazwe("talerz");
    talerz->dodaj_przym("st�uczony","st�uczeni");
    talerz->dodaj_przym("gliniany","gliniani");
    talerz->ustaw_materialy(MATERIALY_GLINA,100);
    talerz->move(this_object());
    talerz = clone_object("/lib/pub_naczynie_jedzenie.c");
    talerz->ustaw_nazwe("talerz");
    talerz->dodaj_przym("st�uczony","st�uczeni");
    talerz->dodaj_przym("gliniany","gliniani");
    talerz->ustaw_materialy(MATERIALY_GLINA,100);
    talerz->move(this_object());
    talerz = clone_object("/lib/pub_naczynie_jedzenie.c");
    talerz->ustaw_nazwe("talerz");
    talerz->dodaj_przym("st�uczony","st�uczeni");
    talerz->dodaj_przym("gliniany","gliniani");
    talerz->ustaw_materialy(MATERIALY_GLINA,100);
    talerz->move(this_object());
    talerz = clone_object("/lib/pub_naczynie_jedzenie.c");
    talerz->ustaw_nazwe("talerz");
    talerz->dodaj_przym("st�uczony","st�uczeni");
    talerz->dodaj_przym("gliniany","gliniani");
    talerz->ustaw_materialy(MATERIALY_GLINA,100);
    talerz->move(this_object());
    talerz = clone_object("/lib/pub_naczynie_jedzenie.c");
    talerz->ustaw_nazwe("talerz");
    talerz->dodaj_przym("st�uczony","st�uczeni");
    talerz->dodaj_przym("gliniany","gliniani");
    talerz->ustaw_materialy(MATERIALY_GLINA,100);
    talerz->move(this_object());
    talerz = clone_object("/lib/pub_naczynie_jedzenie.c");
    talerz->ustaw_nazwe("talerz");
    talerz->dodaj_przym("st�uczony","st�uczeni");
    talerz->dodaj_przym("gliniany","gliniani");
    talerz->ustaw_materialy(MATERIALY_GLINA,100);
    talerz->move(this_object());
    talerz = clone_object("/lib/pub_naczynie_jedzenie.c");
    talerz->ustaw_nazwe("talerz");
    talerz->dodaj_przym("st�uczony","st�uczeni");
    talerz->dodaj_przym("gliniany","gliniani");
    talerz->ustaw_materialy(MATERIALY_GLINA,100);
    talerz->move(this_object());
    talerz = clone_object("/lib/pub_naczynie_jedzenie.c");
    talerz->ustaw_nazwe("talerz");
    talerz->dodaj_przym("st�uczony","st�uczeni");
    talerz->dodaj_przym("gliniany","gliniani");
    talerz->ustaw_materialy(MATERIALY_GLINA,100);
    talerz->move(this_object());

    dodaj_rzecz_niewyswietlana("st�uczony gliniany talerz",0);
    dodaj_rzecz_niewyswietlana("gliniany kubek",0);

    add_prop(ROOM_I_INSIDE,1);
}

exits_description()
{
	return "Wyj�cie st�d prowadzi na plac.\n";
}

string
dlugi_opis()
{
    string str;

    str = "P�aszcz tajemniczej mg�y otacza t� niewielk� kuchni�. "+
          "Panuje tu nie�ad i ba�agan. Oty�y nizio�ek wiruje w "+
          "przedziwnym ta�cu, biega i skacze ";

    if(jest_rzecz_w_sublokacji(0,"niewielki kocio�ek"))
       str+="pomi�dzy garnkami oraz kocio�kami ";

    str+="dogl�daj�c potraw";

    if(jest_rzecz_w_sublokacji(0,"st�uczony gliniany talerz"))
       str+=". Brudne naczynia pi�trza si� pod �cianami";

    if(jest_rzecz_w_sublokacji("na p�ce","ma�y skromny rondelek"))
      {
        str+=" a rondelki";

        if(jest_rzecz_w_sublokacji("na p�ce","wiekowa obszerna patelnia"))
        str+=" i patelnie ";

        str+=" rozrzucono niedbale po kuchennych p�kach. ";

      }
    else
     {
      if(jest_rzecz_w_sublokacji("na p�ce","wiekowa obszerna patelnia"))
       str+=" a patelnie rozrzucono niedbale po kuchennych p�kach. ";
      else
       str+=". ";
     }

    str+="Zapach zi� zmieszany z aromatem gotowanych potraw "+
          "unosi si� w powietrzu, niczym natr�tny go�� wdziera si� "+
          "w nozdrza i przyprawia o zawr�t g�owy.";

    return str;
}

void jaka_rasa()
{
    if(plec==1)
      {
        rasa="nizio�ka";
      }
    else
      {
        rasa="nizio�ek";
      }
  race="nizio�ek";
}

/* Wykonano przez Avarda na zlecenie Lil, dnia 02.05.06 */

/* Generalnie staramy si� tworzy� jak najmniej akcji npca
 * w jednej fazie. �eby na zafloodowa�o nagle ekranu...
 */

void faza0start()
{
    faza_reakcji=1;
    npc->command("usmiechnij sie");
    npc->command("powiedz do "+ OB_NAME(byt) +
                " A witam, witam.");
    npc->command("popatrz uwaznie na byt astralny");
    npc->command("powiedz do "+ OB_NAME(byt) +
               " Hoho, ju� wiem o co chodzi! Pragniesz dosta� sw� "+
               "cielesn� form�?");
}

void fazaniebyt()
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Widz�, �e ju� podj"+koncoweczka("��e�","�a�")+
	     " decyzj�, nic tu po mnie.");
}
void faza0tak()
{
    faza_reakcji=1;
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Chcesz zosta� nizio�k"+koncoweczka("iem","�")+"? By�by to "+
             "oczywi�cie najlepszy wyb�r.");
}
void faza0nie()
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Dobrze, wr�� kiedy si� zdecydujesz.");
}
void faza0inne()
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Chyba si� nie zrozumieli�my. Tak czy nie?");
    wyswietl_pomocniczy_komunikat();
}
void faza1tak()

{
    faza_reakcji=1;
    npc->command("zatrzyj rece");
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Wspania�y wyb�r! Zanim przejdziemy do konkret�w mo�e "+
	     "zechcia�"+koncoweczka("by�","aby�")+
	     " wys�ucha� mojej opowie�ci? "+
	     "Moi kuzyni zawsze powtarzaj�, �e ciekawie opowiadam.");
    npc->command("usmiechnij sie zachecajaco");
}
void faza1nie()
{
    faza_reakcji=1;
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Szkoda, by�by z ciebie dobry nizio�ek.");
}
void faza1inne()
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Nie zrozumia�em, tak czy nie?");
    wyswietl_pomocniczy_komunikat();
}

/* Dobrze by by�o, gdyby ka�dy enpec opowiada� histori� z w�asnego
 * punktu widzenia, nie?
 * Historia wi�c ma by� subiektywna. Im bardziej, tym lepiej ;)
 *
 *
 * HISTORIA ROZBITA NA FAZY. PIERWSZA FAZA JEST NA SAMYM DOLE: faza2tak()
 * A OSTATNIA NA SAMEJ GORZE (faza2tak4()), wiec od konca czytamy ;)
 */

void faza2tak5()
{
    npc->command("powiedz do "+ OB_NAME(byt) +
         " Czyli pami�taj, na ludzi trzeba uwa�a� i nie wchodzi� im w drog�, "+
         "bo jeszcze znowu dojdzie do jakiego� pogromu... "+
         "No i zawsze najlepiej trzyma� si� razem. "+
         "Ka�dy wie, �e gdy nizio�k�w kupa tam i "+
         "cz�owiek... nie jest taki mocny. To jak, jeste� z nami?");
	npc->command("usmiechnij sie z nadzieja");
}
void faza2tak4()
{
    npc->command("powiedz do "+ OB_NAME(byt) +
         " Tak, teraz ludzie tutaj dominuj�. Nie by�oby to nawet takie z�e "+
         "gdyby nie fakt, �e oni za wszystko obwiniaj� nas, nieludzi. "+
         "I tak oto w rz�dzonym przez nich �wiecie bardzo wielu naszych " +
         "niewinnych braci i si�str ko�czy na szubienicach... ");
    npc->command("westchnij cicho");
    set_alarm(9.5,0.0,&faza2tak5());
}
void faza2tak3()
{
    npc->command("powiedz do "+ OB_NAME(byt) +
         " Byli�my tu na tej ziemi pierwsi. "+
         "Dopiero p�niej przyp�yn�y elfy, a kilkaset lat po nich "+
         "ludzie. Z elfami nie by�o zbyt du�ych k�opotow, mo�e "+
         "i by�y jakie� k��tnie zaraz po ich przybyciu, jednak sytuacja "+
         "szybko si� uspokoi�a. Z lud�mi, hmm, nie by�o ju� tak "+
         "dobrze, od razu zacz�li podb�j. Wydzierali elfom krainy kawa�ek "+
          "po kawa�eczku, a elfy cofa�y si� i cofa�y...");
    set_alarm(12.5,0.0,&faza2tak4());
}
void faza2tak2()
{
    npc->command("powiedz do "+ OB_NAME(byt) +
         " Co do samej historii... Hmm...");
    npc->command("emote pociera policzek w zamy�leniu.");
    set_alarm(5.5,0.0,&faza2tak3());
}
void faza2tak1()
{
    npc->command("powiedz do "+ OB_NAME(byt) +
         " Dlatego najcz�ciej spotkasz nas w polu, na wsi "+
         "lub w kuchni! Cho� nierzadko te� odkrywamy w sobie "+
         "�y�k� do kupiectwa.");
    set_alarm(7.5,0.0,&faza2tak2());
}
void faza2tak() //Opowiada historie + ostateczne pytanie o rase
{
    faza_reakcji=1;
    npc->command("usmiechnij sie");
    npc->command("powiedz do "+ OB_NAME(byt) +
         " Zaczn� od tego, �e my, nizio�ki ponad wszystko "+
         "cenimy sobie dobr� kuchni� i wiejskie wygody...");
    set_alarm(8.0,0.0,&faza2tak1());
}
//Koniec opowiadania historii...


void faza2nie()
{
    faza_reakcji=1;
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Nie wiesz co tracisz, ale to tw�j wyb�r, nie "+
			"zamierzam ci� zmusza�. "+
			"A wi�c, jeste� pew"+koncoweczka("ien","na")+
             " swego wyboru?");
}
void faza2inne()
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Tak czy nie?");
    wyswietl_pomocniczy_komunikat();
}
void faza3tak() //Ostateczne pytanie o rase. Nastepnie pytanie o wzrost
{
    npc->command("powiedz do "+ OB_NAME(byt) +
                 " Wiedzia�em, �e podejmiesz w�a�ciw� decyzj�! Przejd�my "+
		"teraz do spraw mniej oczywistych.");
    npc->command("mrugnij porozumiewawczo");
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Jakiego chcesz by� wzrostu?");
}
void faza3nie()
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Nie? Szkoda, my�la�em, �e b�de mia� "+
		"now"+koncoweczka("ego","�")+
		" kuzyn"+koncoweczka("a","k�")+
		". Naprawd� szkoda.");
}
void faza3inne()
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Nie rozumiem.");
    wyswietl_pomocniczy_komunikat();
}
void faza4zle() //odpowiedz na wzrost, pytanie o wage
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Nie wiem o co chodzi, je�li masz problem wystarczy "+
		"mnie zapyta� o wzrost.");
}
void faza4dobrze()
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Dobry wyb�r, a teraz powiedz mi jakiej chcesz by� wagi.");
}
void faza5zle() //waga
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " M"+koncoweczka("�g�by�","og�aby�")+
		" powt�rzy�, bo chyba nie zrozumia�em?");
    wyswietl_pomocniczy_komunikat();
}
void faza5dobrze()
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Dobrze, ile wiosen masz juz za sob�?");
}
void faza6nierozumiem() //wiek
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Nie rozumiem, ile?");
    wyswietl_pomocniczy_komunikat();
}
void faza6zamlody()
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Az tak"+koncoweczka("i","a")+
             " m�od"+koncoweczka("y","a")+
             " to nie mo�esz by�! Musisz wybra� conajmniej "+
		"czterna�cie.");
}
void faza6zastary()
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " To zdecydowanie za du�o, w takim wieku nie "+
		"m"+koncoweczka("�g�by�","og�aby�")+
		" pozwoli� sobie na wiele rzeczy. Wybierz mniej ni� "+
		"osiemdziesi�t, a wi�cej ni� czterna�cie.");
}
void faza6dobrze()
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " B�dziesz mia�"+koncoweczka("","a")+
             " "+LANG_SNUM(wiek,0,1)+" "+slowo_lat+
             ". Jeden z moich kuzyn�w jest w dok�adnie takim samym "+
             "wieku. Ale co z oczami, jakiego chcesz mie� koloru?");
}
void faza7zle() //kolor oczu
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Hmm, nie znam takiego koloru. "+
             "Znam tylko: "+
    implode(m_indices(plec==1? oczy_niziolki:oczy_niziolka), ", ")+".");
}

/* Po prostu idziemy dalej, czyli
 * pytamy o to, czy �yczy sobie posiada� w�osy W OG�LE,
 * czyli, czy gracz chce by� �ysy NA STA�E.
 */
void faza7dobrze8()
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Dobrze. "+
             "A co z w�osami? Chcesz je w og�le posiada� "+
		"kuzy"+koncoweczka("nie","nko")+
		" ?");
}
/* A tutaj omijamy pytanie o to, czy gracz chce w og�le
 * posiada� ow�osienie, poniewa� jest elfem lub p�elfem, a
 * oni nie mog� by� 'na sta�e' �ysi.
 * Oczywi�cie nikt im nie zabroni si� zgoli�, ale w�osy
 * b�d� ros�y im i tak... :)
 * Wi�c pytamy od razu o kolor w�os�w.
 */
void faza7dobrze9()
{
    npc->command("spanikuj"); //w przypadku elfa i polelfa tu
                              //ma byc pytanie od razu o kolor w�os�w.
}

/* Je�li gracz chce posiada� w�osy w og�le, to jedziemy z pytaniem
 * o kolor w�os�w.
 */
void faza8tak()
{
   npc->command("powiedz do "+ OB_NAME(byt) +
             " Jakiego koloru maj� by� twoje w�osy?");
}

/* Kobietom nie zadajemy pyta� o zarost, wi�c omijamy kilka faz
 * i pytamy odrazu o budow� cia�a t� �ys� kobietk� <szok> ....
 */
void faza8nie1()
{

   npc->command("powiedz do "+ OB_NAME(byt) +
                " Nie chcesz mie� w�os�w? Nie widzia�em jeszcze "+
	        "�ysej hobbitki... no c� to tw�j wyb�r.");
   faza11dobrze1();
}

/* A m�czy�ni i tak musz� poda� kolor... zarostu. �eby by�o wiadomo
 * jaki kolor brody i w�s�w im da� (gdyby posiadali w�osy, to wyliczaloby
 * si� to z koloru w�os�w, ale �e wybrali, �e s� permanentnie �ysi...)
 */
void faza8nie2()
{
   npc->command("powiedz do "+ OB_NAME(byt) +
             " A wi�c b�dziesz �ysy, musisz jednak wybra� kolor "+
	   "swojego zarostu, jaki to b�dzie kolor?");
}
/* Tak czy nie? Inna odpowied�...
 */
void faza8inne()
{
   npc->command("powiedz do "+ OB_NAME(byt) +
             " Odpowiedz pozytywnie, je�li chcesz posiada� w�osy.");
    wyswietl_pomocniczy_komunikat();
}
/* kolor wlosow, pytanie o dlugosc
 */
void faza9zle()
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Nie, taki kolor si� nie nadaje, wybierz kt�ry� z tych: "+
           //w przypadku innych ras jest s� to inne indeksy mapping�w
    implode(m_indices(wlosy_niziolkow), ", ")+".");
}
void faza9dobrze()
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Wspaniale. "+
             "A teraz wybierz d�ugo�� w�os�w!");
}
/* Ta faza jest tylko wtedy, je�li si� wybra�o permanentnie �ysego.
 * Bo i tak trzeba poda� kolor zarostu. Wi�c ta faza jest tylko dla m�czyzn
 */
void faza10zle()
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Nie znam takiego koloru zarostu.");
    wyswietl_pomocniczy_komunikat();
}
void faza10dobrze()
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " A jakiej d�ugo�ci w�sy �yczysz sobie posiada�?");
}
/* W tej fazie gracz wybiera d�ugo�� w�os�w i dostaje pytanie
 * O w�sy, je�li jest m�czyzn�. Je�li jest elfem lub kobiet�,
 * to omijamy te pytania i skaczemy odrazu do pyt. o budowe cia�a
 */
void faza11zle()
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Nie znam takiej d�ugo�ci w�os�w.");
    wyswietl_pomocniczy_komunikat();
}


/* jesli jest: albo elfem, albo kobiet�, albo poni�ej 19 roku �ycia:
 */
void faza11dobrze1()
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Teraz zajmiemy si� twoim cia�em, jak� cech� budowy "+
		"cia�a chcesz posiadac?");
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Masz do dyspozycji: "+   //m["indeks"][2]
             COMPOSITE_WORDS2(dostepne_cechy_ciala," lub ")+".");
                             //lista filtrowana przez wybrana wage i wzros
}
/* w przeciwnym wypadku:
 */
void faza11dobrze2()
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " A co z w�sami? Jak d�ugie maj� by�? Je�li "+
		"chcesz mo�emy je ogoli�, ale z czasem i tak odrosn�.");
}
/* d�ugo�� w�s�w:
 */
void faza12zle()
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Niestety nie znam takiej d�ugo�ci w�s�w. Je�li masz "+
		"z czym� jaki� problem wystarczy mnie spyta�, "+
		"drog"+koncoweczka("i","a")+
		" kuzy"+koncoweczka("nie","nko")+
	        " .");
}
void faza12dobrze()
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " A co z brod�? Jakiej d�ugo�ci ma by�?");
}
/* d�ugo�� brody
 * i pytanie o ceche budowy cia�a
 */
void faza13zle()
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Nie znam takiej d�ugo�ci.");
    wyswietl_pomocniczy_komunikat();
}
void faza13dobrze()

{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Teraz musisz wybra� cech� charakterystyczn� "+
             "dla budowy twojego przysz�ego cia�a.");
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Masz do dyspozycji: "+   //m["indeks"][2]
             COMPOSITE_WORDS2(dostepne_cechy_ciala," lub ")+".");
                             //lista filtrowana przez wybrana wage i wzrost
}


void faza14zle() //budowa ciala. Pytanie o ceche szczegolna
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Nie znam takiej cechy. Masz do dyspozycji: "+
               COMPOSITE_WORDS2(dostepne_cechy_ciala," lub ")+".");
}
void faza14dobrze()

{
    npc->command("powiedz do "+ OB_NAME(byt) +
         " Teraz najciekawsze! Chcesz mie� jak�� ceche szczeg�ln�? "+
         "Mo�esz wybra� w�r�d nast�puj�cych cech: "+
         COMPOSITE_WORDS2(m_indexes(plec==1?cechy_szczegolne_kobiet:
         cechy_szczegolne_mezczyzn)," lub ")+". Odpowiedz tak lub nie.");
}
void faza15tak() //odpowiedz czy chce sie miec ceche szczegolna <opcjonalnie>
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " �wietnie! Zatem jaka to b�dzie cecha?");
}
void faza15nie()
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Szkoda, mia�em nadzieje, �e b�dziesz wyj�tkow"+
             koncoweczka("y","a")+"...");
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Teraz musimy wybra� twoje pochodzenie! Czy chcesz "+
             "posiada� jakie�? "+
             "Pami�taj kuzyn"+koncoweczka("ie","ko")+
             " p�niej tak�e b�dziesz "+
             "m"+koncoweczka("�g�","og�a")+" si� na nie "+
             "zdecydowa�, lecz do wyboru b�dziesz mia�"+
             koncoweczka("","a")+" jedynie miejsce, "+
             "w kt�rym si� b�dziesz aktualnie znajdowa�"+
             koncoweczka("","a")+
             ". Pami�taj, �e raz wybrane miejsce ju� nigdy si� "+
             "nie zmieni.");
}
void faza15inne()
{
   npc->command("powiedz do "+ OB_NAME(byt) +
             " Hmmm... Tak czy nie?");
    wyswietl_pomocniczy_komunikat();
}
void faza16zle() //wybieranie cechy szczegolnej (jesli sie zgodzilo wczesniej)
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Nie znam takiej cechy szczeg�lnej.");
    wyswietl_pomocniczy_komunikat();
}
void faza16dobrze()
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Ha! Wida� w tobie moj� krew, "+
             "kuzyn"+koncoweczka("ie","ko")+
             " .");
      npc->command("powiedz do "+ OB_NAME(byt) +
             " Teraz musimy wybra� twoje pochodzenie! Czy chcesz "+
             "posiada� jakie�? "+
             "Pami�taj kuzyn"+koncoweczka("ie","ko")+
             " p�niej tak�e b�dziesz "+
             "m"+koncoweczka("�g�","og�a")+" si� na nie "+
             "zdecydowa�, lecz do wyboru b�dziesz mia�"+
             koncoweczka("","a")+" jedynie miejsce, "+
             "w kt�rym si� b�dziesz aktualnie znajdowa�"+
             koncoweczka("","a")+".");
}
void faza17tak() //odpowiedz czy chce sie posiadac pochodzenie.
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " �wietnie! A wi�c sk�d chcesz pochodzi�?");
}
void faza17nie()
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Dobrze, a na jak� cech� zwracasz wyj�tkow� uwag� i "+
             "masz zamiar po�wi�ca� na jej rozw�j najwi�cej czasu? Ta cecha "+
             "b�dzie dominowa� w twoich cechach pocz�tkowych. Do wyboru " +
             "masz " + COMPOSITE_WORDS2(SD_STAT_NAMES_BIE, " oraz ") + ".");
}
void faza17inne()
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Nie rozumiem. Tak czy nie?");
    wyswietl_pomocniczy_komunikat();
}
void faza18zle() //wybor pochodzenia jesli sie zgodzilo wczesniej.
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Nie s�ysza�em o takim miejscu, musisz wybra� inne.");
    wyswietl_pomocniczy_komunikat();
}
void faza18dobrze()
{
    string pocho = powiedz_pochodzenie();

    npc->command("powiedz do "+ OB_NAME(byt) +
             " Ach tak! "+pocho[2..]+
             ", mam tam kilku kuzyn�w! No ale wr��my do pracy... "+
             "Teraz musisz wybra� cech� og�ln�, kt�ra "+
             "b�dzie dominowa� z pocz�tku w twych cechach. Do wyboru "+
             "masz " + COMPOSITE_WORDS2(SD_STAT_NAMES_BIE, " oraz ") + ".");
}
void faza19zle() //cechy
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Nie ma takiej cechy. Musisz wybra� kt�r�� z tych, "+
             "kt�re wymieni�em.");
    wyswietl_pomocniczy_komunikat();
}
void faza19dobrze() //cechy
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Przejd�my do twoich umiej�tno�ci, wybierz "+
             "dwie. Jaka b�dzie pierwsza?");
}
void faza20zle() //umiejetnosci - wybor pierwszej
{

    npc->command("powiedz do "+ OB_NAME(byt) +
             " Przykro mi, nie znam takiej umiej�tno�ci.");
    wyswietl_pomocniczy_komunikat();
}
void faza20dobrze()
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Rozumiem. A jaka b�dzie druga umiej�tno��?");
}

void faza21zle() //umiejetnosci - wybor drugiej
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Niestety, nie znam takiej, wybierz inn�.");
    wyswietl_pomocniczy_komunikat();
}
/* nie mo�na wybra� dw�ch umiej�tno�ci takich samych pod rz�d.
 */
void faza21zle2()
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Nie mo�esz wybra� dw�ch takich samych umiej�tno�ci.");

}
void faza21dobrze()
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " No dobrze, chyba ju� wszystko za�atwili�my. "+
             "Dobrze, �e to ju� koniec, wreszcie mog� p�j�� "+
             "co� przek�si�... "+
             "A co z tob�? Jeste� got"+koncoweczka("�w","owa")+"?");
}
void faza22tak() //podsumowanie + prezenty?:) No wlasnie...Jakie?
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Ach! By�bym zapomnia�! Nie mo�esz wej�� do �wiata "+
             "w tym stanie! Jako "+
             "m"+koncoweczka("�j","oja")+
             " kuzyn"+koncoweczka("","ka")+
             " dostaniesz kilka prezent�w. Oto one:");
    npc->command("daj monety, kapelusz, spodnie, ciastko i kubraczek "+OB_NAME(byt));
}
void faza22nie()
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Rozumiem. Wr�� do mnie jak si� namy�lisz, a zaczniemy "+
             "wszystko od nowa.");
}
void faza22inne()
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Hmmm... Tak czy nie?");
    wyswietl_pomocniczy_komunikat();
}

/* A tutaj mamy list� reakcji na odpowiedzi :P
 * Pokazuje sie to za ka�dym razem (natychmiastowo!) po
 * pozytywnej lub negatywnej odpowiedzi.
 * Odpowiedzi b��dne (czyli wywo�uj�ce fazaXinne() nie s�
 * brane pod uwag�).
 */
void reakcje_na_odpowiedzi()
{
    switch(random(17))//im wiecej tym lepiej. 12-15 jest optymalne.
    {
      case 0:
             switch(faza_reakcji)
             {
               case 1: break;
               default: npc->command("powiedz do "+ OB_NAME(byt) +
                        " S�uszny wyb�r."); break;
             }
      case 1:
             switch(faza_reakcji)
             {
               case 1: break;
               default: npc->command("powiedz Dobrze, dobrze."); break;
             }
      case 2:
             npc->command("mlasnij cicho"); break;
      case 3:
             npc->command("namysl sie"); break;
      case 4:
             npc->command("pokiwaj powoli"); break;
      case 5:
             switch(faza_reakcji)
             {
               case 1: break;
               default: npc->command("usmiechnij sie wesolo"); break;
             }
      case 6:
             switch(faza_reakcji)
             {
               case 1: break;
               default: npc->command("usmiechnij sie do siebie"); break;
             }
      case 7:
             switch(faza_reakcji)
             {
               case 1: break;
               default: npc->command("powiedz No, tego si� spodziewa�em.");
                                                            break;
             }
      case 8:
             npc->command("pokiwaj nieznacznie"); break;
      case 9:
             npc->command("pokiwaj ze zrozumieniem"); break;
      case 10:
             npc->command("hmm"); break;
      case 11:
             switch(faza_reakcji)
             {
               case 1: break;
               default: npc->command("usmiechnij sie ."); break;
             }
      case 12:
             switch(faza_reakcji)
             {
               case 1: break;
               default: npc->command("powiedz No, no."); break;
             }
      case 13:
             npc->command("powiedz"); break;
      case 14:
             switch(faza_reakcji)
             {
               case 1: break;
               default: npc->command("powiedz �wietnie."); break;
             }
      case 15:
             npc->command("rozejrzyj sie ."); break;
      case 16:
             switch(faza_reakcji)
             {
               case 1: break;
               default: npc->command("powiedz Te� bym tak zrobi�.");break;
             }

      default:
             npc->command("emote u�miecha si� domy�lnie."); break;
     }
}


void
przydziel_prezenty(object komu)
{
    gift1=clone_object(PREZENTY+"kapelusz.c");
    gift2=clone_object(PREZENTY+"spodnie.c");
    gift3=clone_object(PREZENTY+"ciastko.c");
    gift4=clone_object(PREZENTY+"kubraczek.c");
    gift2->wylicz_rozmiary(0,komu);
    gift1->wylicz_rozmiary(0,komu);
    gift4->wylicz_rozmiary(0,komu);

//    gift1->init_arg(0);
    gift1->move(npc);
//    gift2->init_arg(0);
    gift2->move(npc);
//    gift3->init_arg(0);
    gift3->move(npc);
//    gift4->init_arg(0);
    gift4->move(npc);

}
