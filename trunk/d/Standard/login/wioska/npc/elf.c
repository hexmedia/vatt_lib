/**
 * \file /d/Standard/login/wioska/npc/gnom.c
 *
 * @author Lil
 * @date   Nieznana
 *
 * Elf dzi�ki, kt�remu mo�na zosta� jednym/� ze starszego ludu
 */

inherit "/std/humanoid.c";
inherit "/d/Standard/login/wioska/atrybuty.c";

#include <macros.h>
#include <stdproperties.h>
#include <money.h>
#include <pl.h>
#include <formulas.h>
#include <std.h>
#include <ss_types.h>
#include <wa_types.h>
#include <composite.h>
#include <filter_funs.h>
#include <login.h>
#include <state_desc.h>

mixed wzrost_i_waga;
int plec;
string race, rasa;

void
create_humanoid()
{
    ustaw_odmiane_rasy("elf");
    set_gender(G_MALE);
    //dupny -_-
    set_long("Istota z determinacj� dzier�y sw�j �uk napi�ty mierz�c "+
             "raz po raz do kolorowej tarczy. Poprawia przy tym swe d�ugie, "+
             "srebrzystoszare w�osy, kt�re z�o�liwie opadaj� mu na oczy. "+
             "Wygl�da na istot� ca�kowicie poch�oni�t� tym, co robi, cho� "+
             "czasem podnosi wzrok i rozgl�da si� uwa�nie po otoczeniu, "+
             "jakby na kogo� czeka�.\n");

    dodaj_przym("wynios�y", "wynio�li");
    dodaj_przym("wiekowy", "wiekowi");

    set_act_time(40);
    add_act("emote celuje do tarczy.");
    add_act("emote wypuszcza strza�� w kierunku tarczy. Strza�a trafia w sam �rodek.");
    add_act("emote poleruje sw�j �uk.");
    add_act("popatrz krytycznie na luk");
    add_act("emote przys�uchuje si� �piewu ptak�w.");
//    add_act();
//    add_act();

    set_default_answer(VBFC_ME("default_answer"));

    set_stats ( ({ 65, 31, 70, 21, 30, 58 }) );

    set_skill(SS_DEFENCE, 10 + random(4));
    set_skill(SS_WEP_MISSILE, 99);
    set_skill(SS_PARRY, 10 + random(10));
    set_skill(SS_UNARM_COMBAT, 15 + random(5));

    add_armour("/d/Standard/items/ubrania/szaty/dluga_jasnobrazowa.c");
    //add_weapon("/d/Standard/login/wioska/items/luk_elfa.c");


    add_prop(LIVE_I_NEVERKNOWN, 1);
    add_prop(CONT_I_WEIGHT, 59000);
    add_prop(CONT_I_HEIGHT, 187);

    add_ask(({"wzrost","poziomy wzrostu","dost�pne poziomy wzrostu"}),
            VBFC_ME("pyt_o_wzrost"));
    add_ask(({"wag�","poziomy wagi","dost�pne poziomy wagi","oty�o��",
             "poziomy oty�o�ci","dost�pne poziomy oty�o�ci"}),
            VBFC_ME("pyt_o_wage"));
    add_ask("wiek",VBFC_ME("pyt_o_wiek"));
    add_ask(({"kolory oczu","kolor oczu","oczy"}), VBFC_ME("pyt_o_oczy"));
    add_ask(({"kolor w�os�w","kolory w�os�w"}), VBFC_ME("pyt_o_kol_wlosow"));
    add_ask(({"d�ugo�� w�os�w","d�ugo�ci w�os�w"}), VBFC_ME("pyt_o_dl_wlosow"));
    add_ask(({"pochodzenie","pochodzenia","dost�pne miejsca pochodzenia","dost�pne pochodzenie"}),
             VBFC_ME("pyt_o_pochodzenie"));
    add_ask(({"cechy szczeg�lne","cechy dodatkowe","cechy wyj�tkowe"}),
             VBFC_ME("pyt_o_cechy_szczegolne"));
    add_ask(({"cechy budowy cia�a","cechy dotycz�ce budowy cia�a",
              "budow� cia�a"}),
             VBFC_ME("pyt_o_cechy_budowy"));
    add_ask("cechy",VBFC_ME("pyt_o_cechy"));
    add_ask(({"d�ugo�� w�os�w","dost�pne d�ugo�ci w�os�w","poziomy d�ugo�ci w�os�w"}),
              VBFC_ME("pyt_o_dlugosc_wlosow"));
    add_ask(({"umiej�tno�ci","list� umiej�tno�ci"}),
              VBFC_ME("pyt_o_umy"));
    add_ask(({"histori�","histori� rasy","opowie��"}),
              VBFC_ME("pyt_o_historie"));
    add_ask("w�osy", VBFC_ME("pyt_o_wlosy"));
    add_ask(({"kolor","kolory"}), VBFC_ME("pyt_o_kolor"));

    MONEY_MAKE_K(10)->move(this_object());
}

void jaka_rasa()
{
    if(plec==1)
      {
        rasa="elfka";
      }
    else
      {
        rasa="elf";
      }
  race="elf";
}

string
pyt_o_kolor()
{
    set_alarm(1.0, 0.0, "command_present", this_player(),
        "powiedz :do " + OB_NAME(TP) + " �piewnie: Masz na my�li kolor oczu, w�os�w? "+
        "A mo�e kolor zarostu?");

    return "";
}
string
pyt_o_wzrost()
{
    wzrost_i_waga=({HEIGHTDESC(koncoweczka("i","a")),
                      WIDTHDESC(koncoweczka("y","a")) });

    set_alarm(1.0, 0.0, "command_present", this_player(),
        "powiedz :do " + OB_NAME(TP) + " �piewnie: Mo�esz by� " +
        implode(wzrost_i_waga[0][0..-2], ", ") + " lub " +
        wzrost_i_waga[0][sizeof(wzrost_i_waga[0]) - 1] + ".");

    return "";
}
string
pyt_o_wage()
{
    wzrost_i_waga=({HEIGHTDESC(koncoweczka("i","a")),
                      WIDTHDESC(koncoweczka("y","a")) });
    set_alarm(1.0, 0.0, "command_present", this_player(),
        "powiedz :do " + OB_NAME(TP) + " �piewnie: Mo�esz by� " + //tiaaa...
        implode(wzrost_i_waga[1][0..2], ", ") + " lub " +
        wzrost_i_waga[1][sizeof(wzrost_i_waga[1]) -4] + ".");

    return "";
}
string
pyt_o_wlosy()
{
    set_alarm(1.0, 0.0, "command_present", this_player(),
        "powiedz :do " + OB_NAME(TP) + " �piewnie: Masz na my�li "+
        "d�ugo�� w�os�w, czy kolor w�os�w?");
    return "";
}
string
pyt_o_wiek()
{
     jaka_rasa();
     set_alarm(1.0, 0.0, "command_present", this_player(),
        "powiedz :do " + OB_NAME(TP) + " �piewnie: Nie mniej "+
        "ni� "+LANG_SNUM(DOLNA_GRANICA_WIEKU[race],0,1) +
        " lat, lecz nie "+
        "wi�cej ni�li "+LANG_SNUM(GORNA_GRANICA_WIEKU[race],0,1)+" lat.");
     return "";
}
string
pyt_o_oczy()
{
     plec=this_player()->query_gender();
     set_alarm(1.0, 0.0, "command_present", this_player(),
        "powiedz :do " + OB_NAME(TP) + " �piewnie: Masz do wyboru: "+
        COMPOSITE_WORDS2(m_indices(plec==1?oczy_elfki:oczy_elfa)," lub ")+".");
     return "";
}
string
pyt_o_kol_wlosow()
{
    plec=this_player()->query_gender();
    set_alarm(1.0, 0.0, "command_present", this_player(),
        "powiedz :do " + OB_NAME(TP) + " �piewnie: Masz do wyboru: "+
         COMPOSITE_WORDS2(m_indices(wlosy_elfow), " lub ")+
         ".");
         return "";
}
string
pyt_o_dl_wlosow()
{
     set_alarm(1.0, 0.0, "command_present", this_player(),
        "powiedz :do " + OB_NAME(TP) + " �piewnie: Masz do wyboru: "+
         COMPOSITE_WORDS2(m_indices(dlugosc_wlosow), " lub ")+
         ".");
         return "";
}
string
pyt_o_pochodzenie()
{
    set_alarm(1.0, 0.0, "command_present", this_player(),
        "powiedz :do " + OB_NAME(TP) + " �piewnie: Mo�esz wybra� w�r�d: ");
    set_alarm(2.0, 0.0, "command_present", this_player(),
        "powiedz :do " + OB_NAME(TP) + " �piewnie: "+COMPOSITE_WORDS2(pochodzenie," lub ")+".");
    return "";
}

string
pyt_o_cechy_szczegolne()
{
    mixed mapka=this_player()->query_gender()==1?cechy_szczegolne_kobiet:
         cechy_szczegolne_mezczyzn;
    mixed postmapka;
    postmapka=filter(mapka, &operator(==)(, 3) @ &sizeof());
    set_alarm(1.0, 0.0, "command_present", this_player(),
        "powiedz :do " + OB_NAME(TP) + " �piewnie: Mo�esz wybra� w�r�d: ");
    set_alarm(1.5, 0.0, "command_present", this_player(),
        "powiedz :do " + OB_NAME(TP) + " �piewnie: "+
    COMPOSITE_WORDS2(m_indices(postmapka)," lub ")+".");

    return "";
}

string
pyt_o_cechy_budowy()
{
    set_alarm(1.0, 0.0, "command_present", this_player(),
        "powiedz :do " + OB_NAME(TP) + " �piewnie: Ju� m�wi�em! Nie b�d� si� powtarza�.");
    return "";
}

string
pyt_o_cechy()
{
    set_alarm(1.0, 0.0, "command_present", this_player(),
        "powiedz :do " + OB_NAME(TP) + " �piewnie: Jest "+
         COMPOSITE_WORDS(SD_STAT_NAMES_MIA) + ".");
    return "";
}

string
pyt_o_dlugosc_wlosow()
{
    set_alarm(1.0, 0.0, "command_present", this_player(),
        "powiedz :do " + OB_NAME(TP) + " �piewnie: Mo�esz wybra� jedn� z nast�puj�cych d�ugo�ci w�os�w: ");
    set_alarm(1.5, 0.0, "command_present", this_player(),
        "powiedz :do " + OB_NAME(TP) + " �piewnie: "+
    COMPOSITE_WORDS2(m_indices(dlugosc_wlosow)," lub ")+".");
    return "";
}

string
pyt_o_umy()
{
    set_alarm(1.0, 0.0, "command_present", this_player(),
        "powiedz :do " + OB_NAME(TP) + " �piewnie: Do wyboru masz nast�puj�ce umiej�tno�ci: ");
    set_alarm(1.5, 0.0, "command_present", this_player(),
        "powiedz :do " + OB_NAME(TP) + " �piewnie: "+
    COMPOSITE_WORDS(m_indices(lista_skilli))+".");
    return "";
}
string
pyt_o_historie()
{
   set_alarm(1.0, 0.0, "command_present",
      this_player(),call_other(environment(this_player()),"faza2tak"));
   return "";
}

string
race_sound()
{
    return "szepce";
}

string
default_answer()
{
     set_alarm(1.5, 0.0, "command_present", this_player(),
        "powiedz :do " + OB_NAME(TP) + " �piewnie:  Nie wiem o czym m�wisz.");
     return "";
}

void
return_introduce(string imie)
{
    object osoba = present(imie, environment());

    if (osoba)
    {
        command("powiedz do " + OB_NAME(osoba) + " Witaj.");
    }
}
void
add_introduced(string imie, string biernik)
{
    set_alarm(1.0, 0.0, "return_introduce", imie);
}
