inherit "/std/object";

#include "/sys/stdproperties.h"
#include "/sys/macros.h"
#include "/sys/pl.h"
#include "/sys/materialy.h"
#include "/sys/object_types.h"

void
create_object()
{
    ustaw_nazwe("�awka");
    dodaj_przym("drewniany", "drewniani");
    dodaj_przym("d�ugi", "d�udzy");
    ustaw_material(MATERIALY_DR_DAB);
    set_type(O_MEBLE);
    set_long("Prosta, wr�cz prymitywna �awka wykonana z " +
             "d�bowego drewna. Nogi �awki to dwa fragmenty grubszych " +
             "konar�w drzewa, za� siedzisko to przeci�ty na p� pie� " +
             "przybity grubymi gwo�dziami do n�g.\n");

    add_prop(OBJ_I_WEIGHT, 22000);
    add_prop(OBJ_I_VOLUME, 40300);
    add_prop(OBJ_I_VALUE, 5);
    make_me_sitable("na","na d�bowej �awce","z d�bowej �awki",4);
}
