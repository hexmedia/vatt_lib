inherit "/std/object";

#include "/sys/stdproperties.h"
#include "/sys/macros.h"
#include "/sys/pl.h"
#include "/sys/materialy.h"
#include "/sys/object_types.h"

void
create_object()
{
    ustaw_nazwe("krzes�o");
    dodaj_przym("drewniany", "drewniani");
    dodaj_przym("jasny", "jasni");
    ustaw_material(MATERIALY_DR_BUK);
    set_type(O_MEBLE);
    set_long("Proste, ale jednak �adne krzes�o, wykonane z jasnego " +
             "drewna i z prost�kotnym oparciem z wyrze�bionym na nim " +
             "sercem.\n");
	     
    add_prop(OBJ_I_WEIGHT, 6800);
    add_prop(OBJ_I_VOLUME, 8900);
    add_prop(OBJ_I_VALUE, 3);

    make_me_sitable("na","na krze�le","z krzes�a",1);
}
