
/* Lawa z klecznikiem, made by Avard 02.06.06 
   Opis by Tinardan */

inherit "/std/object.c";

#include <pl.h>
#include <stdproperties.h>
#include <macros.h>
#include <filter_funs.h>
#include <materialy.h>
#include <object_types.h>

create_object()
{
    ustaw_nazwe("^lawa");
    dodaj_przym("solidny","solidni");
    dodaj_przym("d^ebowy","d^ebowi");


    set_long("Drewniana ^lawa, wykonana z najprawdziwszego d^ebu, solidna, "+
        "mocna i twarda niczym kamie^n. Po bokach pokrywaj^a j^a rze^xbienia "+
        "wyobra^zaj^ace na przemian ogie^n i b^lyskawice. Kl^ecznik jest "+
        "w^aski i niewygodny, w sam raz dla pokutuj^acych dusz, kt^ore "+
        "przysz^ly si^e ukorzy^c przed b^ostwem za pope^lnione brzegi. "+
        "^Lawa pachnie staro^sci^a i brudem.\n");
    make_me_sitable("na","na ^lawie","z ^lawy",5);
    add_prop(OBJ_I_WEIGHT, 20000);
    add_prop(OBJ_I_VOLUME, 10000);
    add_prop(OBJ_I_VALUE, 400);
    add_prop(OBJ_M_NO_SELL, 1);
    ustaw_material(MATERIALY_DR_DAB);
    set_type(O_INNE);
}

public int
jestem_lawa_swiatyni()
{
        return 1;
}
