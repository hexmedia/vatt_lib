inherit "/std/object.c";

#include <pl.h>
#include <stdproperties.h>
#include <macros.h>
#include <filter_funs.h>
#include <materialy.h>
#include <object_types.h>

create_object()
{
    ustaw_nazwe("krzes�o");
                 
    dodaj_przym("drewniany","drewniani");
   set_type(O_MEBLE);
   ustaw_material(MATERIALY_DR_DAB);
   make_me_sitable("na","na drewnianym krze�le","z krzes�a",1);
    set_long("Zbite gwo�dziami deski tworz� te najzwyklejsze w �wiecie krzes�o.\n");
    add_prop(OBJ_I_WEIGHT, 6300);
    add_prop(OBJ_I_VOLUME, 5990);
    add_prop(OBJ_I_VALUE, 2);
}



