inherit "/std/object";

#include "/sys/stdproperties.h"
#include "/sys/macros.h"
#include "/sys/pl.h"
#include "/sys/materialy.h"
#include "/sys/object_types.h"

void
create_object()
{
    ustaw_nazwe("krzes�o");
    dodaj_przym("zdobiony", "zdobieni");
    dodaj_przym("ciemny", "ciemni");
    ustaw_material(MATERIALY_DR_DAB);
    set_type(O_MEBLE);
    set_long("Bogato zdobione i starannie wykonane krzes�o przypomina " +
             "bardziej kr�lewski tron ni� zwyk�y mebel. Siedzisko i oparcie " +
             "obite s� mi�kkim, czerwonym materia�em, a wysokie por�cze " + 
             "zako�czone starannie wyrze�bionymi �bami lw�w.\n");
	     
    add_prop(OBJ_I_WEIGHT, 10000);
    add_prop(OBJ_I_VOLUME, 19000);
    add_prop(OBJ_I_VALUE, 65);
    make_me_sitable("na","na zdobionym krze�le","z krzes�a",1);
}
