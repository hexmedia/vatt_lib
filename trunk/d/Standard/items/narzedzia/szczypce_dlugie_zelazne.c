/*
 * Autor: Rantaur
 * Data: 23.04.2006
 *  Szczypce do kuzni w wiosce tworzenia postaci
 *  */
inherit "/std/object.c";

#include <pl.h>
#include <stdproperties.h>
#include <macros.h>
#include <object_types.h>
#include <materialy.h>

void create_object()
{
    ustaw_nazwe("szczypce");
    ustaw_nazwe_glowna("para");
    dodaj_przym("d�ugi", "d�udzy");
    dodaj_przym("�elazny", "�ela�ni");
    set_long("Szczypce s� ca�kiem nie�le przystosowane do pracy w ku�ni -"
	    +" d�ugie, lekko sp�aszczone na ko�cach ramiona u�atwiaj� chwytanie r�nych"
	    +" przedmiot�w, za� oprawione w grub� sk�r� r�czki skutecznie chroni� przed"
	    +" ciep�em. Kleszcze wygl�daj� na do�� ci�kie, wi�c wprawne pos�ugiwanie" 
	    +" si� nimi musi wymaga� krzepy, a tak�e pewnego wyczucia.\n");
	add_prop(OBJ_I_VALUE, 447);
	add_prop(OBJ_I_WEIGHT, 895);
	add_prop(OBJ_I_VOLUME, 380);
	set_type(O_NARZEDZIA);
	ustaw_material(MATERIALY_ZELAZO);
}
