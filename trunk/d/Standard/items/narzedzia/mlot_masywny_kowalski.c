/*
 * Autor: Rantaur
 * Data: 23.04.2006
 *  Mlot dla kowala w wiesce tworzenia postaci
 *  */
inherit "/std/weapon";
inherit "/lib/tool";

#include <stdproperties.h>
#include <wa_types.h>
#include <pl.h>
#include <materialy.h>
#include <object_types.h>
#include <tools.h>

void create_weapon()
{
    ustaw_nazwe("mlot");
    dodaj_przym("masywny", "masywne");
    dodaj_przym("kowalski", "kowalskie");
    set_long("Solidny, d�ugi na oko�o dwa �okcie trzon zwie�czony zosta�"
	    +" ci�kim, �elaznym bijakiem, na kt�rym dostrzegasz liczne rysy,"
	    +" �wiadcz�ce o tym, i� m�otem wykuto ju� niejedn� bro�. R�koje��" 
	    +" gdzieniegdzie poznaczona jest w�skimi p�kni�ciami, jednak" 
	    +" narz�dzie z powodzeniem powinno spe�nia� swoj� rol� jeszcze" 
	    +" przez d�ugi czas. Na �rodku bijaka wygrawerowano niewielki" 
	    +" symbol, lecz zosta� on ju� do�� mocno zatarty.\n");
    add_item("symbol", "Z niema�ym trudem odgadujesz, i� symbol przedstawia"
            +" miniaturowy miecz. Masz wra�enie, �e umieszczono pod nim jaki�"
            +" napis, lecz jest on na tyle zatarty, �e nie jeste� w stanie"
            +" go odczyta�.\n");
	set_hit(7);
	set_pen(17);	
	set_wt(W_WARHAMMER);
	set_dt(W_BLUDGEON);
	set_hands(W_RIGHT);
	add_prop(OBJ_I_VALUE, 850);
	add_prop(OBJ_I_WEIGHT, 6500);
	add_prop(OBJ_I_VOLUME, 3500);
	set_type(O_BRON_MLOTY);
	ustaw_material(MATERIALY_STAL);

    add_tool_domain(TOOL_DOMAIN_BLACKSMITH);
    set_tool_diffculty(60);
}
