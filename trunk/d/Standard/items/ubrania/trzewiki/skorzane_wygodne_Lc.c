/* Hadart, 17.12.2006
* Opis Tinardan
*/

inherit "/std/armour";
#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <pl.h>
#include <macros.h>
#include <materialy.h>
#include <object_types.h>
void
create_armour()
{  
    ustaw_nazwe_glowna("para");
    ustaw_nazwe("trzewiki");
    dodaj_nazwy("buty");

    dodaj_przym("sk^orzany","sk^orzane");
    dodaj_przym("wygodny","wygodni");

    set_long("Jedno spojrzenie na nie wystarczy, by stwierdzi^c, ^ze takie "
        + "buty musz^a kosztowa^c maj^atek. Wykonane z najwy^zsz^a "
	+ "staranno^sci^a, stanowi^a istne arcydzie^lo sztuki szewskiej. "
	+ "Zielono-szarawe, o unikalnym deseniu, kryte specjal^a pow^lok^a "
	+ "dla ochrony ciesz^a oko swoim wygl^adem. Podeszwa, p^laska "
	+ "i cienka, wykonana jest z najlepszej jako^sci wo^lowej sk^ory. "
	+ "Pantofelki zapinaj^a si^e na pojedyncz^a, niewielk^a klamr^e, "
	+ "misternie splecion^a ze z^lotych i srebrnych "
	+ "drucik^ow.  \n");
                
    set_slots(A_FEET);
    add_prop(OBJ_I_VOLUME, 1000);
    add_prop(OBJ_I_VALUE, 8500);
    add_prop(OBJ_I_WEIGHT, 500);
    add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
    add_prop(ARMOUR_I_DLA_PLCI, 1);
    add_prop(ARMOUR_I_D_ROZCIAGLIWOSC, 25);
    add_prop(ARMOUR_I_U_ROZCIAGLIWOSC, 35);
    set_size("L");

    ustaw_material(MATERIALY_SK_CIELE, 70);
    ustaw_material(MATERIALY_ZLOTO, 15);
    ustaw_material(MATERIALY_SREBRO, 15);
    set_type(O_UBRANIA);
}
string
query_auto_load()
{
   return ::query_auto_load();
}
