
/* Autor: Avard
   Opis : Kotra
   Data : 29.07.06 
   Uzyte: RINDE/npc/landsknecht.c */

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <macros.h>
#include <materialy.h>
#include <object_types.h>
#include <pl.h>

void
create_armour()
{
    ustaw_nazwe("beret");

    dodaj_przym("p^laski","p^lascy");
    dodaj_przym("elastyczny","elastyczni");

    set_long("B^l^ekitny beret wykonany z dzianiny jest niesamowicie "+
        "elastyczny, dzi^eki tej w^la^sciwo^sci jego posiadacz mo^ze w "+
        "dowolny spos^ob modelowa^c wygl^ad swojego nakrycia g^lowy tak, "+
        "aby jak najlepiej pasowa^l do sytuacji w kt^orej si^e znajduje. "+
        "Przyczepione do beretu czane pi^oro dodaje mu troche "+
        "ekstrawagancji.\n");
    add_item(({"pi^orko","pi^oro"}), "Do^s^c d^lugie, ponad siedmio calowe, czarne "+
        "pi^oro osadzone w b^l^ekitnym berecie.\n");

    set_slots(A_HEAD);
    add_prop(OBJ_I_WEIGHT, 100);
    add_prop(OBJ_I_VOLUME, 200);
    add_prop(OBJ_I_VALUE, 19);
    set_type(O_UBRANIA);
    ustaw_material(MATERIALY_BAWELNA, 100); // Do zmiany
    add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    set_size("XL");
    set_likely_cond(25);
}

string
query_auto_load()
{
   return ::query_auto_load();
}