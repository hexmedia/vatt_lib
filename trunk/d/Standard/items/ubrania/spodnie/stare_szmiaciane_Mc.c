
/* Stare szmaciane spodnie
Wykonano dnia 23.07.2005 przez Avarda. Opis zaczerpniety z banku opisow,
napisany przez Artonula w dziale "ubrania i inne przedmioty" 
Poprawki by Avard 20.05.06 */

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <macros.h>
#include <materialy.h>
#include <object_types.h>
#include <pl.h>

void
create_armour()
{
    ustaw_nazwe("spodnie");
    ustaw_nazwe_glowna("para");
    dodaj_przym("stary","starzy");
    dodaj_przym("szmaciany","szmaciani");

    set_long("Spodnie te zosta^ly przed wielu laty wykonane dla jakiego^s "+
        "biednego rolnika. S^a strasznie niewygodne, a szmaty z kt^orych je "+
        "zszyto mog^a w ka^zdej chwili si^e rozpa^s^c. Kiedy^s by^ly "+
        "barwione na br^azowo ale teraz kolor prawie ca^lkowicie wyblak^l.\n");

    set_slots(A_LEGS, A_HIPS);
    add_prop(OBJ_I_WEIGHT, 700);
    add_prop(OBJ_I_VOLUME, 1000);
    add_prop(OBJ_I_VALUE, 10);
    set_type(O_UBRANIA);
    ustaw_material(MATERIALY_LEN, 100);
    add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    set_size("M");
    set_likely_cond(29);
}

string
query_auto_load()
{
   return ::query_auto_load();
}