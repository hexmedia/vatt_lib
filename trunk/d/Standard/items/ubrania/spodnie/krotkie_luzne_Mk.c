
/* Kr^otkie lu^xne spodnie
Wykonano dnia 19.05.06 przez Avarda. 
Opis by Tinardan */

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <pl.h>
#include <macros.h>
#include <materialy.h>
#include <object_types.h>
void
create_armour()
{
    ustaw_nazwe("spodnie");
    ustaw_nazwe_glowna("para");

    dodaj_przym("kr^otki","kr^otcy");
    dodaj_przym("lu^xny","lu^xni");

    set_long("Wygl^ada na to, ^ze nogawki tych spodni by^ly kiedy^s "+
        "d^lu^zsze i dopiero niedawno zosta^ly skr^ocone na tyle, ^ze "+
        "doros^lemu cz^lowiekowi si^egaj^a nieco poni^zej kolan. Ich "+
        "jasnoniebieski materia^l jest mi^ekki i przewiewny, ale w wielu "+
        "miejscach przetarty i ^laty, kt^ore s^a na niego naszyte, s^a "+
        "do^s^c dobrze widoczne. Do^ly nogawek s^a wystrz^epione i poprute, "+
        "kilka nitek zwisa z nich sm^etnie. Kieszenie, w kt^ore spodnie s^a "+
        "zaopatrzone s^a dosy^c obszerne.\n");

    set_slots(A_LEGS, A_HIPS);
    add_prop(OBJ_I_WEIGHT, 600);
    add_prop(OBJ_I_VOLUME, 1500);
    add_prop(OBJ_I_VALUE, 42);
    add_prop(ARMOUR_S_DLA_RASY, "krasnolud");
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    add_prop(ARMOUR_I_D_ROZCIAGLIWOSC, 30);
    add_prop(ARMOUR_I_U_ROZCIAGLIWOSC, 40);
    set_size("M");
    ustaw_material(MATERIALY_LEN, 100);
    set_type(O_UBRANIA);
}
string
query_auto_load()
{
   return ::query_auto_load();
}
