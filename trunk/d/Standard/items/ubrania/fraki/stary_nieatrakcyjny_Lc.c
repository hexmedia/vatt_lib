
/* Stary nieatrakcyjny frak
Wykonano dnia 25.07.2005 przez Avarda. Opis zaczerpniety z banku opisow,
napisany przez Zwierzaczka w dziale "Rinde - lombard" 
Poprawki by Avard 20.05.06 */

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <macros.h>
#include <materialy.h>
#include <object_types.h>
#include <pl.h>
void
create_armour()
{
    ustaw_nazwe("frak");
    dodaj_przym("stary","starzy");
    dodaj_przym("nieatrakcyjny","nieatrakcyjni");

    set_long("Nieco ju^z stary frak si^egaj^acy do po^lowy ud "+
        "w^la^sciciel"+this_player()->koncowka("a","ki")+". "+
        "Wyblak^ly kolor, pokryty domieszk^a kurzu nasuwa na my^sl, ^ze "+
        "kiedy^s dawno temu, ubranie to by^lo czarne. Jednak wiadomo, czas,"+
        "jak r�wnie� cz�ste u�ywanie robi� swoje. W wielu miejscach "+
        "dostrzec mo^zna dziury oraz nitk^e, kt^ora nie do ko^nca odpruta "+
        "zwisa w nie^ladzie na przyk^lad z rekaw^ow. Z^locenia na guzikach "+
        "s^a ju^z pozdrapywane, udowadniaj^ac tym samym marn^a podr^obke "+
        "z^lota, a raczej jedynie z^loty barwnik, jakiego u^zyto przy ich "+
        "wyrobie.\n");
		
    set_slots(A_TORSO, A_FOREARMS, A_ARMS, A_HIPS, A_THIGHS);
    add_prop(OBJ_I_WEIGHT, 1000);
    add_prop(OBJ_I_VOLUME, 2000);
    add_prop(OBJ_I_VALUE, 25);
    add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    set_type(O_UBRANIA);
    ustaw_material(MATERIALY_BAWELNA, 95);
    ustaw_material(MATERIALY_ZELAZO, 5);
    set_size("L");
}

string
query_auto_load()
{
   return ::query_auto_load();
}