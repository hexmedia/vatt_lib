
/* Ciep^ly we^lniany p^laszcz
   Wykonany przez Avarda dnia 19.05.06
   Opis by Tinardan */

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <pl.h>
#include <macros.h>
#include <materialy.h>
#include <object_types.h>

void
create_armour()
{ 

    ustaw_nazwe("plaszcz");
    dodaj_przym("ciep^ly","ciepli");
    dodaj_przym("we^lniany", "welniani");
    set_long("P^laszcz nawet z daleka wygl^ada na bardzo ciep^ly. Zosta^l "+
        "uszyty z grubej mi^ekkiej we^lny w kolorze ciemnej zieleni. "+
        "Tkanina, z kt^orej zosta^l zrobiony jest mocna, bez zb^ednych "+
        "ozd^ob, a kolor p^laszcza nie rzuca si^e w oczy. U g^ory "+
        "pob^lyskuje przypi^eta ^zelazna spinka o spiralnym kszta^lcie. "+
        "D^lugi, spiczasty kaptur zosta^l doszyty nieco p^o^xniej, dla "+
        "lepszej ochrony przed zimnem.\n"); 

    /* Trzeba dodac mozliwosc zalozenia kaptura */

    ustaw_material(MATERIALY_WELNA, 90);
    ustaw_material(MATERIALY_ZELAZO, 10);
    set_type(O_UBRANIA);
    set_slots(A_CLOAK);
    add_prop(OBJ_I_WEIGHT, 700);
    add_prop(OBJ_I_VOLUME, 1000);
    add_prop(OBJ_I_VALUE, 230);
    add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    add_prop(ARMOUR_I_D_ROZCIAGLIWOSC, 50);
    add_prop(ARMOUR_I_U_ROZCIAGLIWOSC, 50);
    set_size("XS");

}

string
query_auto_load()
{
   return ::query_auto_load();
} 

