inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <macros.h>
#include <materialy.h>

void
create_armour()
{  
   ustaw_nazwe(({"gorset","gorsetu","gorsetowi","gorset","gorsetem","gorsecie"}),
                ({"gorsety","gorset闚","gorsetom","gorsety","gorsetami","gorsetach"}),
                PL_MESKI_NOS_NZYW);
    dodaj_przym("zwyk造","zwykli");
    dodaj_przym("br您owy", "br您owi");
    set_long("Obcis造 br您owy gorset wi您any z przodu, idealnie dopasowuje si� "+
             "do cia豉 nosz帷ej go kobiety. Nie zak豉da si� go pod sp鏚 ubrania, lecz na "+ 
             "wierzch. Tak, by wygl康a� efektownie. Przewi您any z przodu czarnym sznurkiem, "+
             "daje krzy穎wy wz鏎 zawi您any na g鏎ze w ma陰 lu幡o zwisaj帷� kokardk�. "+
             "Gorset nie posiada 瘸dnych zb璠nych zdobie�. Wygl康a elegancko i w do嗆 widoczny "+
             "spos鏏 uwydatnia kobiece kszta速y, lecz nie nadaje si� na wytworne okazje. "+
             "Zrobiony jest z do嗆 twardego i sztywnego materia逝, ale mimo to jest wygodny "+
             "i nie kr瘼uje ruch闚. Ten gorset wydaje si� by� bardzo ma造.\n");

    set_slots(A_TORSO);
    add_prop(OBJ_I_WEIGHT, 500);
    add_prop(OBJ_I_VOLUME, 1200);
    add_prop(OBJ_I_VALUE, 200);

    /* gorset jest sztywny i ma這 rozci庵liwy */
    add_prop(ARMOUR_F_PRZELICZNIK, 20.0);
    /* gorset jest przeznaczony dla kobiet */
    add_prop(ARMOUR_I_DLA_PLCI, 1);
    /* ustawiamy materia� */
    ustaw_material(MATERIALY_SK_SWINIA);
}
string
query_auto_load()
{
   return ::query_auto_load();
} 
