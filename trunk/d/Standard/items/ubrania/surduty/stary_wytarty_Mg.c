
/* Stary wytarty surdut
Wykonano dnia 23.07.2005 przez Avarda. Opis zaczerpniety z banku opisow,
napisany przez Ghalleriana w dziale "ubrania i inne przedmioty" 
dnia 10.04.2005*/

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <macros.h>
#include <materialy.h>
#include <object_types.h>
#include <pl.h>

void
create_armour()
{
	ustaw_nazwe("surdut");

	dodaj_przym("stary","starzy");
	dodaj_przym("wytarty","wytarci");

	set_long("Kiedy^s elegencki i wytworny obecnie za^s przedstawiaj^acy "+
		"si^e w op^lakanym stanie surdut nie jest ju^z w stanie spe^lnia^c "+
		"innej roli jak wierzchniego okrycia na codzie^n. Kolor "+
		"materia^lu wyblak^l i gdzieniegdzie pojawily sie jasne plamy. Mimo "+
		"wszystko szwy jeszcze mocno trzymaja.\n");
		
    set_slots(A_TORSO, A_FOREARMS, A_ARMS);
    add_prop(OBJ_I_WEIGHT, 1400);
    add_prop(OBJ_I_VOLUME, 1000);
    add_prop(OBJ_I_VALUE, 35);
    set_type(O_UBRANIA);
    ustaw_material(MATERIALY_BAWELNA, 100);
    add_prop(ARMOUR_S_DLA_RASY, "gnom");
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    set_size("M");
}

string
query_auto_load()
{
   return ::query_auto_load();
}