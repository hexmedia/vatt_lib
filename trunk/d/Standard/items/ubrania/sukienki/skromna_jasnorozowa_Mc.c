
/* skromna jasnorozowa sukienka
Wykonano dnia 07.06.06 przez Avarda. 
Opis by Faeve */


inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <pl.h>
#include <macros.h>
#include <materialy.h>
#include <object_types.h>
void
create_armour()
{
    ustaw_nazwe("sukienka");
    dodaj_przym("skromny","skromni");
    dodaj_przym("jasnor^o^zowy","jasnor^o^zowi");

    set_long("G^ora sukienki, przylegaj^aca do cia^la, zako^nczona jest "+
        "niedu^za st^ojka, na ^srodku kt^orej przyszyta jest ciemnoczerwona "+
        "tiulowa r^o^zyczka. D^lugie, obcis^le rekawy sukni, delikatnie "+
        "rozszerzaj^ace si^e w okolicach nadgarstka, wyko^nczone s^a "+
        "misternie wykonan^a koronk^a. Lekko rozkloszowana, si^egaj^aca "+
        "ziemi sp^odnica opada swobodnie w d^o^l. Jako ozdobny dodatek w "+
        "pasie przewieszony jest cieniutki srebrny ^la^ncuszek, kt^oremu "+
        "jako zapi^ecie s^lu^zy, r^ownie^z srebrny, kwiatuszek zdobiony "+
        "male^nkimi rubinami. \n");



    set_slots(A_TORSO, A_FOREARMS, A_ARMS, A_LEGS, A_HIPS, A_STOMACH);
    add_prop(OBJ_I_WEIGHT, 1000);
    add_prop(OBJ_I_VOLUME, 1500);
    add_prop(OBJ_I_VALUE, 180);
    add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
    add_prop(ARMOUR_I_DLA_PLCI, 1);
    add_prop(ARMOUR_I_D_ROZCIAGLIWOSC, 30);
    add_prop(ARMOUR_I_U_ROZCIAGLIWOSC, 40);
    set_size("M");
    ustaw_material(MATERIALY_TIUL, 85);
    ustaw_material(MATERIALY_RUBIN, 5);
    ustaw_material(MATERIALY_SREBRO, 10);

    set_type(O_UBRANIA);
}
string
query_auto_load()
{
   return ::query_auto_load();
}
