
/* Aksamitna czerwona suknia
Wykonano dnia 23.07.2005 przez Avarda. Opis zaczerpniety z banku opisow,
napisany przez Tinardan w dziale "ubrania i inne przedmioty" 
dnia 18.07.2005
Poprawki by Avard dnia 20.05.06*/

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <pl.h>
#include <macros.h>
#include <materialy.h>
#include <object_types.h>

void
create_armour()
{
    ustaw_nazwe("suknia");
    dodaj_przym("aksamitny","aksamitni");
    dodaj_przym("czerwony","czerwoni");

    set_long("Suknia ta, kt^ora mo^ze stanowi� marzenie ka^zdej "+
        "mieszczanki, zosta^la uszyta z kilku warstw aksamitu i jest "+
        "niemi^losiernie ci^e^zka. Jej gorset i r^ekawy s^a pokryte "+
        "eleganckim z^lotym haftem przedstawiaj^acym ptaki. Przy "+
        "ka^zdym, nawet najmniejszym poruszeniu, suknia szele^sci "+
        "lekko i po^lyskuje wplecionymi w ni^a z^lotymi nitkami. "+
        "G^leboki, elegancki dekolt i po^lyskuj^acy "+
        "z^locony pasek przyci^agaj^a wzrok.\n");

    set_slots(A_TORSO, A_FOREARMS, A_ARMS, A_LEGS, A_HIPS, A_STOMACH);
    add_prop(OBJ_I_WEIGHT, 4000);
    add_prop(OBJ_I_VOLUME, 2000);
    add_prop(OBJ_I_VALUE, 320);
    add_prop(ARMOUR_I_U_ROZCIAGLIWOSC, 40);
    ustaw_material(MATERIALY_AKSAMIT, 95);
    ustaw_material(MATERIALY_BAWELNA, 5);
    set_type(O_UBRANIA);
    add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
    add_prop(ARMOUR_I_DLA_PLCI, 1);
    add_prop(ARMOUR_I_U_ROZCIAGLIWOSC, 40);
    set_size("M");
}
string
query_auto_load()
{
   return ::query_auto_load();
}
