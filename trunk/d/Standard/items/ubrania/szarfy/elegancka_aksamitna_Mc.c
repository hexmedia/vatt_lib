
/*
 * Elegancka aksamitna szarfa.
 *
 * Autor: Caandil
 */

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <macros.h>
#include <materialy.h>
#include <object_types.h>

void
create_armour()
{

    ustaw_nazwe("szarfa");

    dodaj_przym("elegancki","eleganccy");
    dodaj_przym("aksamitny","aksamitni");

    set_long("Ulubiona cz�� stroju miejskich elegant�w. Czarny, mocny "+
             "aksamit �wietnie si� nadaje do przewi�zania w pasie i noszenia "+
             "na nim niezbyt ci�kich rzeczy. G��boki kolor i dobry gatunek "+
	     "materia�u kontrastuje elegancko z niemal ka�dym, bardziej "+
	     "wyszukanym, strojem w jakimkolwiek kolorze. Ostatni krzyk mody.\n");
	
    ustaw_material(MATERIALY_TK_LEN);
    set_type(O_UBRANIA);
    set_size("M");
    set_slots(A_HIPS);
    	
    add_prop(OBJ_I_WEIGHT, 100);
    add_prop(OBJ_I_VOLUME, 1800);
    add_prop(OBJ_I_VALUE, 100);
    add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
    add_prop(ARMOUR_I_DLA_PLCI, 1);

}
