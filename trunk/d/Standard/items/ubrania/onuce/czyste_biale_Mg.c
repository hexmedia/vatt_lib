
/* Czyste biale onuce
Wykonano dnia 23.07.2005 przez Avarda. 
Opis napisany przez Tinardan 
Poprawki by Avard dnia 20.05.06 */

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <macros.h>
#include <materialy.h>
#include <pl.h>
#include <object_types.h>

void
create_armour()
{
    ustaw_nazwe("onuce");
    ustaw_nazwe_glowna("para");
    dodaj_przym("czysty","czy^sci");
    dodaj_przym("bia^ly","biali");

    set_long("Paski materia^lu, kt^orymi owija si^e stopy dla wygody i "+
        "ciep^la wygl^adaj^a na nowe i czyste. S^a zrobione z lnianego "+
        "materia^lu i sprawiaj^a wra^zenie miekkich i ciep^lych. "+
        "Wydzielaj^a przyjemny zapach ^swie^zego materia^lu.\n");
		
    set_slots(A_FEET);
    ustaw_material(MATERIALY_LEN, 100);
    add_prop(OBJ_I_VALUE, 16);
    add_prop(OBJ_I_WEIGHT, 100);
    add_prop(OBJ_I_VOLUME, 90);
    add_prop(ARMOUR_I_D_ROZCIAGLIWOSC, 30);
    add_prop(ARMOUR_S_DLA_RASY, "gnom");
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    set_size("M");
}
string
query_auto_load()
{
   return ::query_auto_load();
}