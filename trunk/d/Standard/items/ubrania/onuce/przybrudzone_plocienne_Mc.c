
/* Przybrudzone plocienne onuce
Wykonano dnia 24.07.2005 przez Avarda. 
Opis napisany przez Tinardan 
Poprawki by Avard, dnia 20.05.06 */

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <macros.h>
#include <materialy.h>
#include <object_types.h>
#include <pl.h>
void
create_armour()
{
   ustaw_nazwe("onuce");
    ustaw_nazwe_glowna("para");

    dodaj_przym("przybrudzony","przybrudzeni");
    dodaj_przym("p^l^ocienny","p^l^ocienni");

    set_long("Te onuce s^a lekko przybrudzone, a ich materia^l jest do^s^c "+
        "twardy. Wygl^adaj^a jednak na ciep^le i bardzo przydatne, a ich "+
        "zapach, cho^c nie zbyt przyjemny, to nie jest intensywny ani "+
        "ani natarczywy. Zdaje si^e, ^ze zosta^ly zrobione z czyjej^s "+
        "starej koszuli.\n");
		
    set_slots(A_FEET);
    add_prop(OBJ_I_WEIGHT, 500);
    add_prop(OBJ_I_VOLUME, 700);
    add_prop(OBJ_I_VALUE, 6);
    set_type(O_UBRANIA);
    ustaw_material(MATERIALY_LEN, 100);
    add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    set_size("M");
}
string
query_auto_load()
{
   return ::query_auto_load();
}