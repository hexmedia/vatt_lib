
/* Przybrudzone chlopskie lapcie
Wykonano dnia 25.07.2005 przez Avarda. Opis napisany przez Tinardan 
Poprawki by Avard, dnia 20.05.06 */

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <macros.h>
#include <materialy.h>
#include <object_types.h>
#include <pl.h>

void
create_armour()
{
     ustaw_nazwe("^lapcie");
     ustaw_nazwe_glowna("para");
     dodaj_przym("przybrudzony","przybrudzeni");
     dodaj_przym("ch^lopski","ch^lopscy");

     set_long("Sprawiaj^a wra^zenie, ^ze zosta^ly wykonane z wielu "+
          "niepasuj^acych do siebie sk^orzanych ^scink^ow, a zszyte "+
          "zosta^ly nie dratw^a, a no^zem do patroszenia ryb. Brud, "+
          "zalegaj^acy na nich zapewne juz od kilku lat, w^zar^l si^e "+
          "w materia^l, os^labi^l szwy i utworzy^l na powierzchni "+
          "tward^a skorup^e. Wi^azane w kostkach lnianym sznurem, "+
          "wygl^adaj^a jakby za chwile mia^ly si^e rozpa^s^c. Kto^s "+
          "zapobiegawczy uszczelni^l kiedy^s ich spody warstw^a dziegcu "+
          "i doszy^l kolejn^a porcj^e sk^ory, ale przez to do szerokiej "+
          "gamy nieprzyjemnych zapach^ow wydzielanych przez to obuwie "+
          "doszed^l jeszcze jeden.\n");

    set_slots(A_FEET);
    add_prop(OBJ_I_WEIGHT, 600);
    add_prop(OBJ_I_VOLUME, 900);
    add_prop(OBJ_I_VALUE, 1);
    set_type(O_UBRANIA);
    ustaw_material(MATERIALY_SK_CIELE, 90);
    ustaw_material(MATERIALY_LEN, 10);
    add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    set_size("S");

}
string
query_auto_load()
{
   return ::query_auto_load();
}