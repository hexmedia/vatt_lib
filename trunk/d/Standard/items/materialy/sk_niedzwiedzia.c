inherit "/std/object";
inherit "/lib/material";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <macros.h>
#include <materialy.h>

void
create_object()
{
    ustaw_nazwe("sk^ora");
    dodaj_przym("niedzwiedzi","niedzwiedzi");

    set_long("Gruba i niezwykle wytrzyma^la sk^ora pochodzi od "+
        "nied^xwiedzia, trudno j^a pomyli^c z czym^s innym, ze wzgl^edu na "+
        "jej rozmiar.\n");

    add_prop(OBJ_I_WEIGHT, 10000);
    add_prop(OBJ_I_VOLUME, 5000);
    add_prop(OBJ_I_VALUE, 1500);

    ustaw_material(MATERIALY_SK_NIEDZWIEDZ);
    set_material_capacity(MATERIALY_SK_NIEDZWIEDZ, 10000);
}

public void
notify_change_material_amount(string material, int oldValue, int newValue)
{
    float factor = itof(newValue)/itof(query_material_capacity(material));
    if (newValue == 0) {
        TO->remove_object();
        return;
    }
    add_prop(OBJ_I_WEIGHT, ftoi(factor * 10000.0));
    add_prop(OBJ_I_VOLUME, ftoi(factor * 5000.0));
    add_prop(OBJ_I_VALUE, ftoi(factor * 1000.0));
}

public string
query_auto_load()
{
    return ::query_auto_load() + query_material_auto_load();
}

public string
init_arg(string arg)
{
    return init_material_arg(::init_arg(arg));
}
