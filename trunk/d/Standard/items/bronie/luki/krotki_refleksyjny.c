
/* Autor: Avard
   Opis : vortak
   Data : 21.01.07 */

inherit "/std/luk.c";

#include "/sys/formulas.h"
#include <stdproperties.h>
#include <wa_types.h>
#include <pl.h>
#include <materialy.h>
#include <object_types.h>

void
create_luk()
{
    ustaw_nazwe("^luk");
    dodaj_przym("jesionowy","jesionowi");
    dodaj_przym("refleksyjny","refleksyjni");

    set_long("^Luk ten jest zbudowany z kilku warstw po^l�czonych ze sob� "+
        "tak �ci�le, ^ze ci^e^zko wyr^o^zni^c granic^e mi^edzy nimi. "+
        "Dzi^eki zastosowaniu �ci^egien zwierz^ecych bro^n jest "+
        "niespotykanie spr^e^zysta, za� warstwa rogowa, kt^or� pokryto "+
        "brzusiec ^l^eczyska zapewnia niezwyk^l� si^l^e ra^zenia. Rdze^n "+
        "stanowi jednolity fragment jesionu. Do naci�gni^ecia takiej "+
        "konstrukcji wymagana jest niema^la si^la. Ca^ly drzewiec zosta^l "+
        "specjalnie zmatowiony dzi^eki czemu nie odbija nawet "+
        "najmniejszych refleks^ow �wietlnych. Podczas walki d^lo^n mo^zna "+
        "wygodnie oprze^c na wyprofilowanym korkowym majdanie, za� na "+
        "ko^ncach obydwu ramion umieszczono miedziane, finezyjnie "+
        "rze�bione gryfy. Jedwabna ci^eciwa, zako^nczona wzmocnionymi "+
        "owijk� p^etlami zaczepiona jest na specjalnie w tym celu "+
        "wykonanych zag^l^ebiebiach. Mniej wi^ecej w po^lowie jej "+
        "d^lugo�ci podobna owijka tworzy siode^lko - miejsce, w kt^orym "+
        "zak^lada si^e strza^l^e.\n");
    /* W opisie jest o cieciwie, jezeli cieciwa bedzie oddzielnym itemem to 
       nalezy to zmienic! */

    ustaw_jakosc(7);//Nie mam pojecia na jakim poziomie to ustalac.
    ustaw_potrzebna_sile(45);
    add_prop(OBJ_I_WEIGHT, 1000);
    add_prop(OBJ_I_VOLUME, 1000);
    add_prop(OBJ_I_VALUE, 4500);
}