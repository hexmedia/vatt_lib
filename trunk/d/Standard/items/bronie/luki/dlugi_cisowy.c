#include <stdproperties.h>
#include <macros.h>
#include <object_types.h>
#include <materialy.h>

inherit "/std/bron_strzelecka/luk";

void
create_luk()
{                    
    dodaj_przym("d^lugi", "d^ludzy");
    dodaj_przym("cisowy", "cisowi");
    ustaw_nazwe("^luk");
                    
    set_long("Ju^z na pierwszy rzut oka wida^c, ^ze wyszed^l on spod r^eki "+
              "rzemie^slnika niew^atpliwie znaj^acego si^e na rzeczy. "+
              "Opleciony rzemieniem majdan zapewnia wygod^e uchwytu, a "+
              "gruba, konopna ci^eciwa nie porani palc^ow strzelca. Samo "+
              "^luczysko za^s, d^lugie na cztery ^lokcie i umiej^etnie "+
              "wyprofilowane, pozwala przy odrobinie wprawy posy^la^c "+
              "strza^l^e nawet na setki jard^ow.\n");

    add_prop(OBJ_I_WEIGHT, 1500);
    add_prop(OBJ_I_VOLUME, 1200); //?
    add_prop(OBJ_I_VALUE, 3600);
    ustaw_jakosc(5);//Nie mam pojecia na jakim poziomie to ustalac.
    ustaw_potrzebna_sile(45);
}
