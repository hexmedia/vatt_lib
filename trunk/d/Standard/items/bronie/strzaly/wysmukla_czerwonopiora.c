/* Autor: Avard
   Opis : Zwierzaczek
   Data : 21.01.07 */

inherit "/std/pocisk.c";

#include "/sys/formulas.h"
#include <stdproperties.h>
#include <wa_types.h>
#include <pl.h>
#include <materialy.h>
#include <object_types.h>

void
create_pocisk()
{
    ustaw_nazwe("strza�a");
    dodaj_przym("smuk�y","smukli");
    dodaj_przym("czerwonopi�ry","czerwonopi�rzy");

    set_long("Strza�a jest wykonana z d�bowego drewna, a ostrugana zosta�a "+
    "w�spos�b pe�ny precyzji, zapewniaj�cy prawid�owy tor lotu. Umocowane "+
    "do� lotki s� koloru czerwonego, zapewne specjalnie zabarwione przez�"+
    "rzemie�lnika. W po��czeniu z dobrym �ukiem, strza�a ta bez w�tpienia� "+
    "b�dzie zab�jcz� broni� nie tylko dla zwierz�t, ale te� i dla ludzi.\n");

    set_jakosc(6);
    set_typ(W_IMPALE);
    ustaw_material(MATERIALY_DR_DAB, 90);
    ustaw_material(MATERIALY_STAL, 10);
    set_type(O_BRON_STRZALY);
    add_prop(OBJ_I_VALUE, 15);
}
