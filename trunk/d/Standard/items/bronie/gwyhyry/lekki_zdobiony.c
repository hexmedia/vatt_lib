/* lekki zdobiony gwyhyr by Sed 28 lipca 2005r. */

inherit "/std/weapon";

#include "/sys/formulas.h"
#include <stdproperties.h>
#include <wa_types.h>
#include <pl.h>
#include <materialy.h>
#include <object_types.h>

void
create_weapon()
{
    ustaw_nazwe("gwyhyr");
    dodaj_nazwy("miecz");

    dodaj_przym("lekki", "lekcy");
    dodaj_przym("zdobiony", "zdobieni");

    set_long("Ten niezwyk^ly miecz jest lekki, a zarazem doskonale "+
        "wywa^zony. D^lugi, ostry brzeszczot i r^ekoje^s^c wykonana "+
        "ze sk^ory plaszczki sprawiaj^aca, ^ze miecz ten nie ^slizga "+
        "si^e w d^loni, doskonale z soba wsp^o^lgraj^a. Niewielki "+
        "jelec stanowi w tym przypadku jedynie ozdob^e. Pi^eknie zdobiona "+
        "g^lownia, runy zapisane na klindze dodaj^a tej broni "+
        "niepowtarzalnego urokliwego, a zarazem gro^xnego wygl^adu. Bez "+
        "w^atpienia jest to jeden z os^lawionych gnomich gwyhyr^ow.\n");

    add_prop(OBJ_I_WEIGHT, 1500);
    add_prop(OBJ_I_VOLUME, 1500);
    add_prop(OBJ_I_VALUE, 140000);
    add_prop(OBJ_I_IS_MAGIC_WEAPON, 1);

    set_hit(34);
    set_pen(45);

    ustaw_material(MATERIALY_STAL, 90);
    ustaw_material(MATERIALY_SK_SWINIA, 10); //To ta plaszczka niby ;)
    set_type(O_BRON_MIECZE);
    set_wt(W_SWORD);
    set_dt(W_SLASH | W_IMPALE);

    set_hands(W_BOTH);
}