inherit "/std/object";

#include <macros.h>
#include <pl.h>
#include <stdproperties.h>

string przyneta = "ciasto";
int alarm_id;

void decay();

void
create_object()
{
	ustaw_nazwe(({"kulka ciasta", "kulki ciasta", "kulce ciasta",
				"kulk� ciasta", "kulk� ciasta", "kulce ciasta"}),
				({"kulki ciasta", "kulek ciasta", "kulkom ciasta",
				 "kulki ciasta", "kulkami ciasta", "kulkach ciasta"}),
				PL_ZENSKI);

	dodaj_nazwy("kulka");

	set_long("Szarawa, lekko wilgotna kulka ulepiona jest z bli�ej"
			+" nieokre�lonego rodzaju ciasta. Bez w�tpienia"
			+" nadaje si� ona do jedzenia, niemniej jednak"
			+" du�o lepiej by�oby spo�ytkowa� j� jako przyn�t�"
			+" na smaczniejsz� i po�ywniejsz� od niej samej ryb�.\n");

	add_prop(OBJ_I_WEIGHT, 3);
	add_prop(OBJ_I_VOLUME, 5);
	add_prop(OBJ_I_VALUE, 1);
	add_prop(OBJ_M_NO_SELL, "Ale� ta rzecz nie ma �adnej warto�ci!\n");

}

string
query_przyneta_typ()
{
    return przyneta;
}

void enter_env(object from, object old)
{
	if(interactive(ENV(TO)) || (function_exists("create_container", ENV(TO)) == "/std/room"))
		alarm_id = set_alarm(600.0, 0.0, "decay");
	else
		remove_alarm(alarm_id);
}

void decay()
{
	dodaj_przym("czerstwy", "czerstwi");
	odmien_short();
	odmien_plural_short();
	przyneta = 0;
}
