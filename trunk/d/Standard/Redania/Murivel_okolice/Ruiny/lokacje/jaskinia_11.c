/**
 * Groty niedaleko ruin na p^olnoc od traktu Rinde - Murivel
 *
 * autor Cairell
 * opisy Tabakista
 * data   2008
 */

#include "dir.h"
inherit RUINY_STD_JASKIN;
#include <stdproperties.h>
#include <macros.h>

string dlugi_opis();

void
create_jaskinie()
{
    set_short("Gdzie^s w jaskini.");
    set_long(&dlugi_opis());

    add_exit(RUINY_LOKACJE + "jaskinia_8.c", "ne");
    add_exit(RUINY_LOKACJE + "jaskinia_10.c", "n");
    add_exit(RUINY_LOKACJE + "jaskinia_12.c", "nw");
    add_exit(RUINY_LOKACJE + "jaskinia_9.c", "e");
}

string dlugi_opis() 
{
    string str;

    str = "Kilka wielkich, grubych niemal jak ludzki tors stalaktyt^ow "+
        "si^egn^e^lo w tym miejscu wyrastaj^acych przez wieki im na spotkanie "+
        "podstaw tworz^ac okaza^le kolumny, zdaj^ace si^e podtrzymywa^c "+
        "niewidoczny w ciemno^sci strop. Nadal od czasu do czasu sp^lywaj^a "+
        "po nich drobne krople s^lonej wody, zbieraj^ac si^e w licznych "+
        "ka^lu^zach u ich st^op s^lup^ow. Pe^lne za^lom^ow ^sciany tak^ze "+
        "zaros^ly wyj^atkowo du^za ilo^sci^a naciek^ow tworz^acych teraz "+
        "chaotyczn^a pl^atanin^e guz^ow i wg^l^ebie^n, niezmiennie o ob^lych "+
        "kraw^edziach.\n";

    return str;
}

public string
exits_description() 
{
    return "Na wschodzie, p^o^lnocy, p^o^lnocnym-zachodzie i "+
        "p^o^lnocnym-zachodzie rozci^aga si^e dalsza cz^e^c groty.\n";
}
