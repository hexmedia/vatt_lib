/**
 * W^awoz na p^olnoc od traktu Rinde - Murivel
 *
 * autor Cairell
 * opisy Tabakista
 * data   2009
 */

#include "dir.h"
inherit RUINY_STD_WAWOZU;
#include <stdproperties.h>
#include <macros.h>
#include <pogoda.h>

string dlugi_opis();
string opis_drzewa();

void
create_wawoz()
{
    set_short("W g^l^ebi w^awozu.");
    set_long(&dlugi_opis());

    add_prop(ROOM_S_DARK_LONG, "Wysokie ^sciany w^awozu"+
        "czyni^a mroki nocy jeszcze g^lebszymi i "+
        "trudniejszymi do przenikni^ecia wzrokiem. Ponad "+
        "twoj^a g^lowa wznosi si^e wieli, ciemny kszta^lt "+
        "spinaj^acy oba zbocza i przes^laniaj^acy niebo.\n");

    add_exit(RUINY_LOKACJE + "wawoz_10.c", "ne");
    add_exit(RUINY_LOKACJE + "wawoz_8.c", "sw");

    add_item(({"drzewo", "^swierk"}), &opis_drzewa());
}

string dlugi_opis() 
{
    string str;

    str = "Strome, niemal pionowe ^sciany w^awozu odst^epuj^a tutaj "+
        "nieco od siebie pozwalaj^ac dostrzec wi^ekszy fragment nieba. "+
        "Okalaj^a go wierzcho^lki drzew pochylaj^acych si^e nad skarp^a "+
        "tak wysok^a i^z ledwie odr^o^zni^c mo^zna ^swierki czy sosny. Jedno "+
        "z nich ju^z zako^nczy^lo sw^oj ^zywot, gdy kraw^ed^x w^awozu "+
        "osun^e^la si^e, lecz zamiast spa^sc na dno, zablokowa^lo si^e w "+
        "poprzek ^scian w po^lowie ich wysoko^sci. Jego ^zywiczny zapach "+
        "jednak ledwie tutaj dociera, gin^ac w wilgotnym, "+
        "ch^lodnym powietrzu. ";

    if (CZY_JEST_SNIEG(TO))
    {
        str += "Wsz^edzie doko^la spod ^snie^znych zasp "+
            "wystaj^a wi^eksze i mniejsze g^lazy, przewa^znie o ob^lych "+
            "kszta^ltach zwyk^lych dla rzecznych kamieni.";
    }
    else
    {
        str += "Dno za^scielaj^a wi^eksze i mniejsze g^lazy, "+
            "przewa^znie o ob^lych kszta^ltach zwyk^lych dla "+
            "rzecznych kamieni.";
    }

    str +="\n";
    return str;
}

string opis_drzewa()
{
    string st;

    st = "Wielki, stary ^swierk, kt^orego korzenie nie zdo^la^ly w ko^ncu "+
       "utrzyma^c go nad kraw^edzi^a w^awozu i run^a^l on w d^o^l, lecz "+
       "jego niespotykana wysoko^s^c nie pozwoli^la mu spa^c na dno. "+
       "Zatrzyma^l si^e w po^lowie g^l^eboko^sci parowu, gdy wci^a^z "+
       "obejmuj^ace grud^e ziemi korzenie wspar^ly sie o niewielki wyst^ep "+
       "skalny, a czub zablokowa^l o przeciwleg^l^a ska^le. ";

    if (pora_roku() == MT_ZIMA)
    {
        st+="O^szronione ga^l^ezie nadal pokryte s^a ciemnozielonym igliwiem, "+
            "a z konar^ow zwisaj^a gdzieniegdzie drobne sople lodu. ";
    }
    else
    {
        st+="Potargane ga^l^ezie nadal pokryte s^a ciemnozielonym igliwiem, "+
            "a na nadwyr^e^zonych upadkiem konarach skrz^a si^e liczne "+
            "krople ^zywicy. ";
    }

    st+="\n";
    return st;
}

public string
exits_description() 
{
    return "W^aw^oz biegnie prosto na p^o^lnocny-wsch^od "+
        "i po^ludniowy-zach^od.\n";
}
