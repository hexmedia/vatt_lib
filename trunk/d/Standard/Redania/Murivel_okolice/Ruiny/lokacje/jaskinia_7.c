/**
 * Groty niedaleko ruin na p^olnoc od traktu Rinde - Murivel
 *
 * autor Cairell
 * opisy Tabakista
 * data   2008
 */

#include "dir.h"
inherit RUINY_STD_JASKIN;
#include <stdproperties.h>
#include <macros.h>

string dlugi_opis();

void
create_jaskinie()
{
    set_short("^Slepa odnoga jaskini.");
    set_long(&dlugi_opis());

    add_exit(RUINY_LOKACJE + "jaskinia_5.c", "se");

    add_item(({"ko^sci", "kostki"}), "Tu i ^owdzie w za^lomach pod "+
        "^scianami, lub w p^lytkich, w wi^ekszo^sci wype^lnionych "+
        "ka^lu^zami nieckach, dostrzec mo^zna drobne, zwierz^ece ko^sci. "+
        "Cz^e^s^c jest po^lamana, lub ogryziona, kilka wklinowanych z "+
        "szczeliny mi^edzy kamieniami. Trudno dostrzec cho^c jedn^a "+
        "d^lu^zsz^a ni^z p^o^l d^loni.\n");
    
    add_npc(RUINY_LIVINGI + "barbegazi.c");
    add_npc(RUINY_LIVINGI + "barbegazi.c");
    add_npc(RUINY_LIVINGI + "barbegazi.c");
}

string dlugi_opis() 
{
    string str;

    str = "Korytarz groty rozszerza si^e tutaj przechodz^ac ^lagodnie w "+
        "ca^lkiem spor^a pieczar^e, lecz ko^nczy si^e ^slepo nie pozwalaj^ac "+
        "kontynuowa^c w^edr^owki. Strop znika w mroku utrudniaj^ac ocen^e "+
        "rozmiar^ow pieczary, lecz musz^a one by^c nie najmniejsze, gdy^z "+
        "w ciemno^sci nie wida^c nawet czubk^ow do^s^c powszechnych dot^ad "+
        "stalaktyt^ow. W za^lomach ska^l i zbieraj^acych wod^e zag^lebieniach "+
        "dostrzec mo^zna gdzieniegdzie wi^eksze lub mniejsze ko^sci. Dziwny, "+
        "pi^zmowy zapach, kt^ory wyczu^c mo^zna w innych cz^e^sciach jaskini, "+
        "tutaj jest wyra^xnie silniejszy, cho^c nie natarczywy.\n";

    return str;
}

public string
exits_description() 
{
    return "Korytarz ci^agnie si^e dalej na po^ludniowy-wsch^od.\n";
}
