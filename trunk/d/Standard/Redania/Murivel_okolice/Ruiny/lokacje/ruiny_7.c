/**
 * Ruiny na p^olnoc od traktu Rinde - Murivel
 *
 * autor Cairell
 * opisy Tabakista
 * data   2008
 */

#include "dir.h"
inherit RUINY_STD_RUIN;

#include <stdproperties.h>
#include <macros.h>

string dlugi_opis();

void
create_ruiny()
{ 
    set_short("Wykute w skale pomieszczenie.");
    set_long(&dlugi_opis());

    add_prop(ROOM_I_INSIDE,1);
    remove_prop(ROOM_I_LIGHT);
    add_prop(ROOM_S_DARK_LONG, "W ciemno^sci, na jednej "+
        "ze ^scian majacz^y jaka^s ja^sniejsza plama.\n");

    add_exit(RUINY_LOKACJE + "ruiny_8.c", "n");
    add_exit(RUINY_LOKACJE + "jaskinia_12.c", "d");

    add_sit(({"na posadzce"}), "na posadzce","z posadzki",10);
    add_sit(({"pod ^scian^a"}), "pod ^scian^a","z pod ^sciany",5);

    add_item(({"otw^or"}), "Ca^lkiem spore przej^scie wykute "+
        "w wapiennej skale zosta�o dok^ladnie zamurowane. "+
        "Jednak na samym ^srodku ponownie wybity zosta^l "+
        "szeroki na ponad ^lokie� otw^or, mimo ^ze "+
        "spogl^adaj^ac na jego kraw^edzie wida^c, ^ze do "+
        "zagrodzenia tej drogi u^zyto podw^ojnej warstwy "+
        "cegie�.\n");
    add_item(({"jaskini^e", "szczelin^e"}), "W miejscu gdzie "+
        "dopiero wykuwane by^lo pomieszczenie wyra^xnie "+
        "wida^c t^apni^ecie kt^ore ods^loni^lo w^ask^a "+
        "szczelin^e wiod^ac^a w mroczn^a otch�a^n poni^zej "+
        "zamkowych podziemi. Z wn^etrza dochodzi "+
        "cichutki, miarowy odg^los jakby kapi�cej wody.\n");
    
    set_event_time(400.0);
    add_event("Przenikliwy ch^l^od bije od strony wiod^acej "+
        "w g^l^ab ziemi szczeliny.\n");
    add_event("Wszechobecna cisza, a^z dzwoni w uszach.\n");
    add_event("^Zaden z cieni nawet nie drgnie zachowuj^ac "+
        "nienaturalny spok^oj.\n");
    add_event("Wymar^le miejsce wype^lnia cisza przerywana "+
        "jedynie odleg^lymi gwizdami wiatru.\n");
}

string dlugi_opis() 
{
    string str;

    str = "Wykute w wapiennej skale pomieszczenie nigdy nie "+
        "zosta^lo zako^nczone. Jeden jej skraj ma g^ladkie "+
        "i wyko^nczone ^sciany otaczaj^ace wiod�cy dalej "+
        "otw^or, kt^ore im dalej od owego wej^scia tym "+
        "bardziej pokrywaj^a nier^owno^sci i ^slady po "+
        "d^lutach. Jeszcze dalej przechodz� one w kanciaste "+
        "za^lomy powsta^le, gdy ta cz^e^s^c groty by^la "+
        "dopiero dr^a^zona. W^la^snie w takiej "+
        "szczelinie ciemnieje otw^or wiod^acy w g^l^ab "+
        "ziemi, w czelu^sci jeszcze ni^zsze ni^z zamkowe "+
        "krypty. Zimno z niej bij^ace na poz^or, co "+
        "najwy^zej natarczywe mrozi krew w ^zy^lach, gdy "+
        "spogl^ada si^e w nieprzeniknion� ciemno^s^c. "+
        "Potrzaskane kraw^edzie jaskini nie wygl^adaj^a "+
        "na wykute, raczej jak miejsce gdzie ska^la "+
        "samoistnie si^e obsun^e^la.\n";

    return str;
}

public string
exits_description() 
{
    return "Mo^zna st^ad pod^a^zy^c przez wybity w "+
        "^scianie otw^or, lub w d^o^l, w g^l^ab "+
        "w^askiej jaskini.\n";
}
