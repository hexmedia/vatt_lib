/**
 * Groty niedaleko ruin na p^olnoc od traktu Rinde - Murivel
 *
 * autor Cairell
 * opisy Tabakista
 * data   2008
 */

#include "dir.h"
inherit RUINY_STD_JASKIN;
inherit "/lib/drink_water";
#include <stdproperties.h>
#include <macros.h>

string dlugi_opis();

void
create_jaskinie()
{
    set_short("^Slepy koniec groty.");
    set_long(&dlugi_opis());

    add_exit(RUINY_LOKACJE + "grota_5.c", "sw");

    set_drink_places(({"ze strumyka","ze strumienia", 
        "z ka^lu^zy", "z potoku"}));

    add_item(({"strumyk", "strumie^n", "potok"}), "Cienka "+
        "str^o^zka wody wyp^lywa spod wielkiego g^lazu, "+
        "wij^ac si^e by odnale^s^c drog^e pomi^edzy "+
        "nier^owno^sciami pod�o�a. Zasilana jest niemal "+
        "ca^ly czas przez krople spadaj^ace z sufitu "+
        "oraz sp^lywaj^ace po wilgotnych ^scianach. Co "+
        "kawa^lek ciek natrafia na wi^eksze zag^l^ebienia "+
        "gdzie woda zatrzymuje si^e na chwil^e tworz^ac "+
        "p^lytkie ka^lu^ze, lecz zaraz potem p^lynie "+
        "dalej znikaj^ac za za^lomem jaskini.\n");
    add_item(({"g^laz"}), "Ciemniejsza barwa ogromnego, "+
        "tarasuj^acego dalsz^a drog^e g^lazu wyra^xnie "+
        "odcina si^e od jasnych, wapiennych ^scian. Jego "+
        "g^ladka powierzchnia pokryta jest cienk^a warstw^a "+
        "soli, lecz nietrudno dostrzec pod ni^a "+
        "drobniutkie kryszta^lki b^lyszcz^ace na "+
        "powierzchni kamienia. Nie spos^ob oceni� jego "+
        "w^la^sciwych rozmiar^ow, gdy^z w wi�kszo�ci nadal "+
        "zatopiony jest w skale, a jedynie pod nim woda "+
        "wyp^luka�a ciasn^a szczelin^e z kt^orej nadal "+
        "bierze pocz^atek w^aski strumyk.\n");
}

public void
init()
{
    ::init();
    init_drink_water();
}

void
drink_effect(string skad)
{
    write("Kl^ekasz na skraju potoku i nabierasz wody w d^lonie, gasz^ac "+
        "ni^a pragnienie.\n");
    saybb(QCIMIE(this_player(), PL_MIA) + " kl^eka na skraju potoku i nabiera "+
        " wody w d^lonie, gasz^ac ni^a pragnienie.\n");
}

string dlugi_opis() 
{
    string str;

    str = "^Latw^a do pokonania do tej pory grot^e "+
        "tarasuje w tym miejscu ogromny g^laz. Ciemniejsza "+
        "barwa wyra^xnie odcina go od otaczaj^acych ci^e "+
        "wapiennych ^scian, pe^lnych solnych zaciek^ow o "+
        "fantazyjnych nierzadko kszta^ltach. Z do^s^c "+
        "wysokiego, sk^apanego w mroku sklepienia "+
        "zwisaj^a tak^ze nieliczne stalaktyty, z kt^orych "+
        "co i rusz spadaj^a pojedyncze krople wody wydaj^ac "+
        "przy tym niemal jedyny m^ac^acy cisz^e d^xwi^ek. "+
        "Drugim i ostatnim jest szmer w^askiego strumyka "+
        "wij^acego si^e u twoich st^op pomi^edzy licznymi "+
        "nier^owno^sciami skalnego pod^lo^za. Potok kluczy "+
        "jeszcze jaki^s czas oddalaj^ac si^e, po czym znika "+
        "gdzie^s za zakr^etem jaskini. Dzi^eki niemu "+
        "powietrze jest przesycone wilgoci^a, a ch^l^od "+
        "panuj^acy doko�a jest jeszcze bardziej przenikliwy.\n";

    return str;
}

public string
exits_description() 
{
    return "Korytarz prowadzi na po�udniowy-zach^od.\n";
}
