/**
 * Groty niedaleko ruin na p^olnoc od traktu Rinde - Murivel
 *
 * autor Cairell
 * opisy Tabakista
 * data   2008
 */

#include "dir.h"
inherit RUINY_STD_JASKIN;
inherit "/lib/drink_water";
#include <stdproperties.h>
#include <macros.h>

string dlugi_opis();

void
create_jaskinie()
{
    set_short("Na zakr^ecie korytarza.");
    set_long(&dlugi_opis());

    add_exit(RUINY_LOKACJE + "grota_4.c", "w");
    add_exit(RUINY_LOKACJE + "grota_6.c", "ne");

    set_drink_places(({"ze strumyka","ze strumienia", 
        "z ka^lu^zy"}));

    add_item(({"strumyk", "strumie^n"}), "Cienka str^o^zka "+
        "wody wije si^e odnajduj^ac drog^e pomi^edzy "+
        "nier^owno^sciami pod^lo^za. Zasilana jest niemal "+
        "ca�y czas przez krople spadaj^ace z sufitu oraz "+
        "sp^lywaj^ace po wilgotnych ^scianach. Co kawa^lek "+
        "ciek natrafia na wi^eksze zag^l^ebienia gdzie woda "+
        "zatrzymuje si^e na chwil^e tworz^ac p^lytkie "+
        "ka^lu^ze, lecz zaraz potem p^lynie dalej pod^a^zaj�c "+
        "niespiesznie w kierunku wylotu jaskini.\n");
}

public void
init()
{
    ::init();
    init_drink_water();
}

void
drink_effect(string skad)
{
    write("Kl^ekasz na skraju potoku i nabierasz wody w d^lonie, gasz^ac "+
        "ni^a pragnienie.\n");
    saybb(QCIMIE(this_player(), PL_MIA) + " kl^eka na skraju potoku i nabiera "+
        " wody w d^lonie, gasz^ac ni^a pragnienie.\n");
}

string dlugi_opis() 
{
    string str;

    str = "Pe^lny za^lom^ow i nier^owno^sci tunel jaskini skr^eca w tym "+
        "miejscu do^s^c mocno. Jednocze^snie niknie ostatni poblask ^swiat^la "+
        "zewn^etrznego wyd^lu^zaj^ac cienie i sprawiaj^ac, ^ze sklepienie "+
        "staje si^e ca^lkowicie niewidoczne, a o tym jak jest wysoko "+
        "^swiadcz^a tylko sporadycznie widoczne czubki stalaktyt^ow. Miarowo "+
        "spadaj^ace z nich krople wody sp^lywaj� do ma^lego, bystrego "+
        "strumyka p^lyn^acego ^srodkiem groty. Cicho szemrz^ac odnajduje on "+
        "drog^e po�r�d licznych nier^owno^sci, z kt^orych na niejednej z "+
        "^latwo^sci^a mo^zna skr^eci^c nog^e, by w ko^ncu znikn^a^c z oczu "+
        "kieruj^ac si^e do wylotu jaskini. Powietrze wype^lnia nieprzyjemna "+
        "wilgo^c, kt^ora wesp^o^l ze zwyk^lym w jaskiniach ch^lodem sprawia, "+
        "^ze zimno wciska si^e w ka^zdy zakamarek przenikaj^ac do szpiku "+
        "ko^sci.\n";

    return str;
}

public string
exits_description() 
{
    return "W^aski korytarz prowadzi na p^o^lnocny-wsch^od oraz zach^od.\n";
}
