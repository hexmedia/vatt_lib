/**
 * Groty niedaleko ruin na p^olnoc od traktu Rinde - Murivel
 *
 * autor Cairell
 * opisy Tabakista
 * data   2008
 */

#include "dir.h"
inherit RUINY_STD_JASKIN;

#include <stdproperties.h>
#include <macros.h>

string dlugi_opis();

int jest_wyjscie = 0;

void
create_jaskinie()
{ 
    set_short("Niska grota.");
    set_long(&dlugi_opis());

    add_exit(RUINY_LOKACJE + "jaskinia_1.c", "nw");

    add_item(({"uchwyt"}), "Prze^zarta rdz^a niemal nie do "+
        "poznania, wisz^aca na ^scianie sze^s^c st^op nad "+
        "ziemi^a obr^ecz oraz p^lytkie wg^l^ebienie w "+
        "skale tu^z pod ni^a mog^a by^c tylko uchwytem "+
        "do pochodni. A raczej by^ly, gdy^z wszechobecna "+
        "w jaskini s^ol przyspieszy^la jeszcze "+
        "niszczenie metalu. Wygl^ada tak jakby mia^la nie "+
        "utrzyma^c s^lomianego wiechcia, lecz mimo to nadal "+
        "tkwi przy wylocie korytarza stanowi^ac pami^atk^e "+
        "po kim^s, kto uzna^l za potrzebne j^a tutaj "+
        "umie^sci^c. Pod ni^a, na wapiennej skale wydrapany "+
        "zosta^l kawa^lkiem w^egla napis, o ca^le lata "+
        "m^lodszy od uchwytu, skoro nadal widoczny mimo "+
        "panuj^acej doko^la wilgoci i osadzaj^acej si^e soli.\n");

    add_item(({"napis"}), "Topornie wykaligrafowany w^eglem "+
        "napis pokry^l si^e ju� drobinami soli, lecz runy "+
        "nadal s^a wyra^xne. Sk^ladaj^a si^e w zdanie "+
        "zwi^e^xle i zdecydowanie, cho^c przy tym bardzo "+
        "obra^xliwie wypraszaj^ace st^ad ka^zdego, kto "+
        "czyta te s^lowa. Anonimowy tw^orca okre^sla na "+
        "koniec ow^a czytaj^ac^a osob^e s^lowem zwykle "+
        "zarezerwowanym dla pewnej cz^e^sci cia^la, "+
        "cho^c pope^lniaj^ac przy tym karygodny "+
        "b^l^ad w pisowni.\n");
}

void
dodaj_wyjscie()
{
    jest_wyjscie = 1;
    add_exit(RUINY_LOKACJE + "wawoz_7.c", ({"wyj^scie","w kierunku wyj^scia",
						  "z jaskini"}),0,9);

    if(jest_dzien())
    {
	tell_room(TO,"Do jaskini wpadaj^a nagle o^slepiaj^ace promienie ^swiat^la, "+
		  "czemu towarzyszy pot^e^zny d^xwi^ek odsuwanego g^lazu.\n");
	//add_prop(ROOM_I_LIGHT,1);
    }
    else
    {
	tell_room(TO,"Przy d^xwi^eku odsuwanego g^lazu pojawia si^e wyj^scie z jaskini.\n");
    }
}
void
usun_wyjscie()
{
    jest_wyjscie = 0;
    remove_exit("wyj^scie");

    tell_room(TO,"Przy d^xwi^eku zasuwanego g^lazu jaskinie zalewaj^a "+
	  "ciemno^sci.\n");
    //remove_prop(ROOM_I_LIGHT);

}

string dlugi_opis() 
{
    object *inwentarz;
    inwentarz = all_inventory();

    string str;

    str = "W niewielkiej pieczarze panuje niemi^ly ch^l^od "+
        "pot^egowany jeszcze przez wszechobecn^a wilgo^c. W "+
        "powietrzu unosi si^e dodatkowo nieprzyjemny, "+
        "pi^zmowy zapach silniejszy im dalej od wej^scia. "+
        "Zdaje si^e nap^lywa� niskim, mrocznym korytarzem "+
        "wiod^acym w g^l^ab jaskini. Po lewo od niego "+
        "dostrzec mo^zna co^s niecodziennego, poni^zej "+
        "pustego, zardzewia^lego uchwytu do pochodni "+
        "kto^s wydrapa^l w^eglem spory, nadal czytelny "+
        "napis, nie^xle widoczny na jasnej, wapiennej "+
        "skale. Poza tym nie spos^ob dojrze^c doko^la "+
        "jakichkolwiek ^slad�w pozostawionych "+
        "mieszka^nc^ow, czy te^z go^sci tej groty. Nie "+
        "s^lycha^c tak^ze nic poza miarowym odg^losem "+
        "wody kapi^acej spod niemal ka^z^acego si^e "+
        "zgarbi^c sklepienia.\n";

    return str;
}

public string
exits_description() 
{
    if(jest_wyjscie)
	return "Znajduje si^e tutaj wyj^scie na "+
        "powierzchni^e, oraz niski, biegn^acy "+
        "na p^olnocny zach^od korytarz.\n";
    else
	return "Znajduje si^e tutaj niski, biegn^acy na p^olnocny-zach^od "+
        "korytarz.\n";
}
