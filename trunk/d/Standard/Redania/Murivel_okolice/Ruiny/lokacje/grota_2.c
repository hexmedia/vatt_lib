/**
 * Groty niedaleko ruin na p^olnoc od traktu Rinde - Murivel
 *
 * autor Cairell
 * opisy Tabakista
 * data   2008
 */

#include "dir.h"
inherit RUINY_STD_JASKIN;
#include <stdproperties.h>
#include <macros.h>

string dlugi_opis();

void
create_jaskinie()
{
    set_short("Sk^ladowisko.");
    set_long(&dlugi_opis());

    add_exit(RUINY_LOKACJE + "grota_1.c", "n");

    add_item(({"drewno", "sterty", "szmaty", "pryzmy", "pryzmy szmat"}), "Z "+
        "pomi^edzy sterty szmat wystaje kilka zbutwiałych, "+
        "wygl^adaj^acych jak sto^lowe nogi dr^ag^aw, doko^la "+
        "kt^orych jednak owini^ete s^a resztki szarej "+
        "materii, kt^orej wi^ekszo^s^c zmieni^la si^e ju^z "+
        "jednak w nierozpoznawalną papk^e. Pomi^edzy "+
        "le^z^acymi kawa^lek dalej obr^eczami beczek "+
        "nie pozosta^l nawet ^slad ich dawnej zawarto^sci, "+
        "a jedynie kilka co odporniejszych na czas deszczułek "+
        "stawia op^or wilgoci. Za to pomi^edzy przegni^lymi "+
        "deskami uk^ladaj^acymi si^e w zarys skrzyni le^z^a "+
        "dziesi^atki pod^lu^znych, ca^lkowicie zardzewia^lych "+
        "przedmiot^ow, kt^orych zarysy nie pozostawiaj^a "+
        "jednak w^atpliwo^sci, ^ze stanowi^ly kiedy^s ostrza "+
        "w^l^oczni, lub kopii.\n");
}

string dlugi_opis() 
{
    object *inwentarz;
    inwentarz = all_inventory();

    string str;

    str = "Grota ^lagodnie rozszerza si^e w tym miejscu "+
        "tworz^ac du^z^a, pod^lu^zn^a sal^e. Ledwie "+
        "wyczuwalny ruch powietrza daje zna^c, ^ze "+
        "gin^acy w mroku sufit, z kt^orego wisz^a p^eki "+
        "stalaktyt^ow musi znajdowa^c si^e na niema^lej "+
        "wysoko^sci. Natomiast po wszelkich niżej "+
        "znajduj^acych si^e zaciekach pozostały jedynie "+
        "poorane d^lutami plamy, gruz za^s za^sciela "+
        "pod^lo^ze w miejscach gdzie by^lo ono nier^owne. "+
        "W kilku miejscach pod ^scianami le^z^a sterty "+
        "zbutwia^lego drewna i pryzmy szmat roztaczaj^ac "+
        "dooko^la nieprzyjemn^a wo^n rozpadu. Z "+
        "pomi^edzy nich stercz^a resztki pojedynczych "+
        "przedmiotów, zardzewia^le obr^ecze beczek, "+
        "nogi jakich^s zydli czy sto^l^ow, przerdzewia^le "+
        "do cna p^o^l kocio^lka. Za tymi resztkami, w "+
        "g^l^ebi groty, ^sciany zw^e^zaj^a si^e "+
        "przybieraj^ac o ton ciemniejsz^a barw^e. "+
        "Przestrze^n mi^edzy nimi dok^ladnie zamurowano, "+
        "odcinaj^ac dalsz^a cz^e^s^c groty, a s^adz^ac "+
        "po wy^z^lobieniach, tak^ze drog^e p^lyn^acej "+
        "tu dawniej wody. W tej chwili jedynie jedna, "+
        "czy dwie ka^lu^ze zebra^ly si^e na ^srodku "+
        "sali, tam gdzie uderzaj^a spadaj^ace "+
        "jednostajnie z g^ory krople.\n";

    return str;
}

public string
exits_description() 
{
    return "Korytarz jaskini wiedzie na p^o^lnoc.\n";
}
