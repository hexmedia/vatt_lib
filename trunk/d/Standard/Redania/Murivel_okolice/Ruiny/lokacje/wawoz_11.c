/**
 * W^awoz na p^olnoc od traktu Rinde - Murivel
 *
 * autor Cairell
 * opisy Tabakista
 * data   2009
 */

#include "dir.h"
inherit RUINY_STD_WAWOZU;
#include <stdproperties.h>
#include <macros.h>
#include <pogoda.h>
inherit "/lib/drink_water";

string dlugi_opis();
string opis_wodospadu();
string opis_potoku();

void
create_wawoz()
{
    set_short("W g^l^ebi w^awozu.");
    set_long(&dlugi_opis());

    add_prop(ROOM_S_DARK_LONG, "Wysokie ^sciany w^awozu"+
        "czyni^a mroki nocy jeszcze g^lebszymi i "+
        "trudniejszymi do przenikni^ecia wzrokiem. Ponad "+
        "twoj^a g^lowa wznosi si^e wieli, ciemny kszta^lt "+
        "spinaj^acy oba zbocza i przes^laniaj^acy niebo.\n");

    add_exit(RUINY_LOKACJE + "wawoz_10.c", "s");

    set_drink_places(({"ze strumienia", "z potoku"}));

    add_item(({"wodospad", "kaskad^e"}), &opis_wodospadu());
    add_item(({"strumie^n", "strumyk", "potok"}), &opis_potoku());

    add_event("Niewielki strumie^n szemrze cicho.\n");
    add_event("Ponad wodospadem unosi si^e ledwie widoczna mgie^lka.\n");
}

public void
init()
{
    ::init();
    init_drink_water();
}

public void
attempt_drink(string skad)
{
    if(TEMPERATURA(TO) < 0)
        TP->catch_msg("Strumie^n jest skuty lodem.\n");
    else if(CZY_JEST_SNIEG(TO))
        TP->catch_msg("Strumie^n skryty jest pod warstw^a ^sniegu, nie "+
            "spos^ob zaczerpn^a^c wody.\n");
    else
        ::attempt_drink(skad);
}

void
drink_effect(string skad)
{
    write("Kl^ekasz na skraju potoku i nabierasz wody w d^lonie, gasz^ac "+
        "ni^a pragnienie.\n");
    saybb(QCIMIE(this_player(), PL_MIA) + " kl^eka na skraju potoku i nabiera "+
        " wody w d^lonie, gasz^ac ni^a pragnienie.\n");
}

string dlugi_opis() 
{
    string str;

    str = "W miar^e dogodna, wiod^aca dnem w^awozu droga, urywa si^e "+
        "nagle, w chwili gdy docierasz do stromego uskoku. Nadp^lywaj^acy "+
        "z po^ludnia strumie^n ^l^aczy si^e z drugim, nieco wi^ekszym, "+
        "bij^acym wprost ze ska^ly, za^s ich wsp^olne wody przelewaj^a "+
        "si^e przez jego kraw^ed^x by w "+
        "formie malowniczej kaskady spa^s^c na je^z^ace si^e w dole ska^ly. ";

    if (jest_dzien() == 1)
    {
        str += "Wysokie, niemal pionowe zbocza jaru, ";
    }
    
    else
    {
        str += "Wysokie, zatopione w nocnych ciemno^sciach zbocza jaru, ";
    }
    
    str += "milcz^aco broni^a przej^scia w ka^zd^a inn^a stron^e. W "+
        "powietrzu unosi si^e sporo wilgoci oraz ledwie wyczuwalny zapach "+
        "butwiej^acych resztek drzew, kt^orych wierzcho^lki miarowo "+
        "ko^lysz^a si^e na tle ";
        
    if (jest_dzien() == 1)
    {
        str += "nieba. ";
    }
    
    else
    {
        str += " ciemnego, nocnego nieba. ";
    }

    str +="\n";
    return str;
}

string opis_wodospadu()
{
    string st;

    st = "Bystry potok docieraj^ac na skraj uskoku z szumem przelewa swe "+
        "wody przez jego kraw^ed^x. Kaskada rozbija si^e o ska^ly "+
        "kilkana^scie metr^ow poni^zej wype^lniajac powietrze wilgoci^a. "+
        "Kolejne miejsze wodospady ^l^acz^a si^e rw^acym "+
        "strumieniem klucz^acym pomi^edzy g^lazami";

    if(CZY_JEST_SNIEG(TO))
    {
        st = ", nikn^acy miejscami pod zaspami lub lodem";
    } 
    st+=". Dalej, dok^ad ska^ly nie przes^laniaj^a widoku, w^awoz staje "+
        "si^e coraz bardziej w^aski i kr^ety.";
    st+="\n";
    return st;
}

string opis_potoku()
{
    string st;

    st = "Nadp^lywaj^acy z po^ludnia potok ^smia^lo zmierza w kierunku "+
        "odcina^j^acego drog^e na p^o^lnoc uskoku. Kilka s^a^zni od "+
        "kraw^edzi wpada do drugiego, nios^acego niemal dwukrotnie wi^ecej "+
        "wody, a kt^ory wybija wprost ze ska^ly przez co nawet stoj^ac obok "+
        "czu^c bij^acy od niego ch^l^od";
        
    if (pora_roku() == MT_ZIMA)
    {
        st += ". Mimo zimy, jedynie miejscami l^od skrywa brzegi obu "+
            "potok^ow. ";
    }
    else
    {
        st += " podobnie jak w wi^ekszo^sci g^orskich strumieni niezmienny "+
            "niezale^znie od pory roku. ";
    }

    st+="Dno za^scielone jest okr^ag^lymi, rzecznymi kamieniami pomi^edzy "+
        "kt^orymi odnajduje drog^e krystalicznie czysta woda.\n";

    return st;
}

public string
exits_description() 
{
    return "W^aw^oz biegnie st^ad prosto na po^ludnie.\n";
}
