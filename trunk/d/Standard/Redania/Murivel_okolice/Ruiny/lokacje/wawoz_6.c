/**
 * W^awoz na p^olnoc od traktu Rinde - Murivel
 *
 * autor Cairell
 * opisy Ania & Tabakista
 * data   2009
 */

#include "dir.h"
inherit RUINY_STD_WAWOZU;
inherit "/lib/drink_water";
#include <pogoda.h>
#include <stdproperties.h>
#include <macros.h>

string dlugi_opis();
string opis_ruin();

void
create_wawoz()
{ 

    set_short("G^l^eboki w^aw^oz.");
    set_long(&dlugi_opis());

    add_exit(RUINY_LOKACJE + "wawoz_5.c", "sw");
    add_exit(RUINY_LOKACJE + "wawoz_7.c", "e");

    set_drink_places(({"z oczka","z jeziorka"}));
    
    add_item(({"ruiny"}), &opis_ruin());
    
    add_item(({"jeziorko", "oczko", "oczko wodne"}), "Nieco wody zebra^lo "+
        "si^e w sporej niecce pod niewysokim nawisem, z kt^orego nie sp^lywa "+
        "ju^z jednak woda. Po strumieniu niew^atpliwie wyp^lywaj^acym "+
        "dawniej z oczka wodnego pozosta^ly  jedynie wyg^ladzone przez wod^e "+
        "kamienie. Woda jest nieco zasta^la, jednak czysta, wi^ec "+
        "najwyra^xniej nie ro^snie, ani nie mieszka w niej nic, co mog^loby "+
        "j^a zanieczy^sci^c.\n"); 

    add_item(({"g^lazy", "kamienie", "bloki"}), "Kilka "+
        "bezkszta^ltnych, szarych bry^l kamienia nadal nosi "+
        "^slady obr^obki i zaprawy. W jednym z nich "+
        "pozosta^l nawet w^aski otw^or strzelniczy. Le^z^a "+
        "one pod p^o^lnocn^a skarp�, tam gdzie uderzy^ly o "+
        "dno po upadku z widocznych w g^orze mur^ow. "+
        "Kamienie otaczane s^a ju^z przez paprocie oraz "+
        "porastane przez mchy nie pozwalaj^ace stwierdzi^c "+
        "czy oderwa^ly si^e pod wp^lywem czasu, burzy, "+
        "czy skruszone r^ek^a ludzk^a.\n");
}

string dlugi_opis() 
{
    string str;

    str = "W^aw^oz w tym miejscu nieco si^e poszerza, "+
        "kamienie pod^lo^za staj^a si^e coraz bardziej "+
        "nier^owne, a ich powierzchni^e cz^e^sciej "+
        "pokrywa cienka warstwa ";
    
    if (pora_roku() == MT_ZIMA)
    {
        str += "szronu. ";
    }
    else
    {
        str += "mchu. ";
    }
    
    str += "Nier^owne, cz^esto sp^ekane g^lazy zmuszaj^a "+
        "do uwa^znego stawiania krok�w, by poprzez "+
        "jedno nieuwa^zne po^slizgni^ecie nie "+
        "zako^nczy^c swej dalszej w^edr^owki. Otulaj^ace "+
        "drog^e strome zbocza g^orskie, rzucaj^a ponury "+
        "cie^n na kamienne bloki, zapewniaj^ac jednak "+
        "piechurom ochron^e od wiatru. Wy^zej, ponad "+
        "kraw^edziami w^awozu ";
    
    if(CZY_JEST_SNIEG(TO))
    {
        str += "zalegaj^a grube warstwy ^sniegu. ";
    }
    else
    {
        str += "przechylaj^a si^e mizerne kosodrzewiny i "+
            "zniszczone wieloletnimi burzami i wiatrami "+
            "sosny. ";
    }
    
    str += "Ponad nimi, na zachodzie rysuj^a si^e "+
        "murszej^ace ruiny. Skaliste pod^lo^ze ";
    
    if(CZY_JEST_SNIEG(TO))
    {
        str += "przysypane jest r^owno ^sniegiem, "+
            "zbieraj^acym si^e w spore zaspy.";
    }
    else
    {
        str += "utrudnia bytowanie jakiejkolwiek "+
            "bujniejszej ro^slinno�ci, wi^ec okolic^e "+
            "porastaj^a jedynie g^este k^epy paproci.";
    }

    str +="\n";
    return str;
}

public void
init()
{
    ::init();
    init_drink_water();
}

public void
attempt_drink(string skad)
{
    if(TEMPERATURA(TO) < 0)
        TP->catch_msg("Jeziorko jest skute lodem.\n");
    else
        ::attempt_drink(skad);
}

void
drink_effect(string skad)
{
    write("Kl^ekasz na skraju jeziorka i nabierasz wody w d^lonie, gasz^ac "+
        "ni^a pragnienie.\n");
    saybb(QCIMIE(this_player(), PL_MIA) + " kl^eka na skraju jeziorka i "+
        "nabiera wody w d^lonie, gasz^ac ni^a pragnienie.\n");
}

string opis_ruin()
{
    string strr;

    strr = "Kawa^lek na zach^od, ponad kraw^edzi^a w�wozu ";
    
    if (jest_dzien() == 1)
    {
        strr += "chyl^a si^e niepewnie resztki mur^ow obronnych, "+
            "wygl^adaj^acych jakby waha^ly si^e czy nadszed^l ich czas by "+
            "run^a^c w d^o^l.";
    }
    else
    {
        strr += "czarny, poszarpany kontur ledwie daje si^e odr^o^zni^c od "+
            "r^ownie nieregularnych wierz^cho^lk^ow drzew.";
    }

    strr += "\n";
    return strr;
}

public string
exits_description() 
{
    return "W^aw^oz biegnie dalej na po^ludniowy-zach^od "+
        "i wsch^od.\n";
}
