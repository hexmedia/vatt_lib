/**
 * Groty niedaleko ruin na p^olnoc od traktu Rinde - Murivel
 *
 * autor Cairell
 * opisy Tabakista
 * data   2008
 */

#include "dir.h"
inherit RUINY_STD_JASKIN;
#include <stdproperties.h>
#include <macros.h>

string dlugi_opis();

void
create_jaskinie()
{
    set_short("Przy rozpadlinie.");
    set_long(&dlugi_opis());

    add_exit(RUINY_LOKACJE + "jaskinia_3.c", "se");
    add_exit(RUINY_LOKACJE + "jaskinia_5.c", "w");
    add_exit(RUINY_LOKACJE + "jaskinia_6.c", "sw");

    add_object(RUINY_OBIEKTY + "rozpadlina4.c", 1);

    dodaj_rzecz_niewyswietlana("rozpadlina",1);

}

string dlugi_opis() 
{
    string str;

    str = "Korytarz jaskini rozszerza si^e w tym miejscu ^lagodnie "+
        "przechodz^ac w pod^lu^zn^a grot^e, kt^orej pod^lo^ze w p^o^lnocnej "+
        "cz^e^sci urywa si^e nagle nikn^ac w czelu^sciach niezbyt szerokiej "+
        "rozpadliny. Otaczaj^ace ci^e ^sciany pe^lne s^a ma^lych nisz "+
        "i nieregularnych zag^l^ebie^n w^sr^od kt^orych ^swiat^lo tworzy "+
        "bezustannie zmieniaj^ac^a si^e pl^atanin^e cieni. Wilgotne powietrze "+
        "niesie szum wartko przelewaj^acej si^e gdzie^s niedaleko wody.\n";

    return str;
}

public string
exits_description() 
{
    return "Korytarz ci^agnie si^e dalej na zach^od, po^ludniowy "+
        "wsch^od i po^ludniowy-zach^od.\n";
}
