/**
 * Ruiny na p^olnoc od traktu Rinde - Murivel
 *
 * autor Cairell
 * opisy Tabakista
 * data   2008
 */

#include "dir.h"
inherit RUINY_STD_RUIN;

#include <stdproperties.h>
#include <macros.h>

string dlugi_opis();

void
create_ruiny()
{
    set_short("Pusty, zamkowy dziedziniec.");
    set_long(&dlugi_opis());

    add_prop(ROOM_I_INSIDE,0);
    add_prop(ROOM_S_DARK_LONG, "Mroczne, nieregularne zarysy "+
        "jakich^s budynk^ow majacz^a niewyra^xnie na tle "+
        "o ton ja^sniejszego od nich nieba.\n");

    add_exit(RUINY_LOKACJE + "ruiny_1.c", "ne");
    add_exit(RUINY_LOKACJE + "ruiny_2.c", "n");
    add_exit(RUINY_LOKACJE + "ruiny_3.c", "e");
    add_exit(RUINY_LOKACJE + "ruiny_5.c", "se");
    add_exit(RUINY_LOKACJE + "ruiny_6.c", "s");

    add_sit(({"na ziemi"}), "na ziemi","z ziemi",10);
    add_sit(({"pod ^scian^a"}), "pod ^scian^a","z pod ^sciany",5);
    add_sit(({"przy studni"}), "przy studni","z pod studni",2);

    
    add_object(RUINY_OBIEKTY + "krata.c", 1);

    dodaj_rzecz_niewyswietlana("przerdzewia^la krata",1);
    

    add_item(({"studni^e"}), "Okr^ag^le, rzeczne kamienie u^zyte "+
        "do obmurowania brzeg^ow studni pomy^slnie przesz^ly "+
        "pr^ob^e czasu. ^Z^o^lte, szare i rdzawe, przetykaj^a "+
        "si^e chaotycznie stanowi^ac jedyny barwny obraz w tym "+
        "zrujnowanym miejscu. Te z nich, kt^ore znajduj^a na "+
        "samej kraw^edzi l^sni^a od wilgoci, jednak ^zuraw do "+
        "czerpania wody le^zy obok zbutwia�y tak, ^ze drewno "+
        "rozsypuje si^e w r^ekach. Po jakimkolwiek wiadrze lub "+
        "cebrze nie pozosta�y nawet obr�cze. \n");
    add_item(({"baszt^e"}) ,"Kamienna wie^za wyrasta z dziedzi^nca "+
        "celuj^ac w niebo wysokimi blankami. Okr^ag^la podstawa "+
        "podtrzymuje masywne, kamienne ^sciany kt^ore zdradzaj^a "+
        "sw^oj wiek jedynie krusz^ac^a si^e zapraw^a. Po "+
        "broni^acych wej^acia drzwiach pozosta^l tylko dolny "+
        "zawias. Miejsce gdzie by� niegdy^s drugi znaczy jedynie "+
        "dziura mi�dzy wapiennymi blokami. Tu^z obok kilka "+
        "brukowc^ow dziedzi^nca pokrywa jakby ciemnobrunatny "+
        "osad, za^s szpary mi�dzy nimi znaczy kilka "+
        "zaschni^etych strumyk^ow tej samej barwy.\n");
    add_item(({"mury", "mur", "ruiny"}), "Grube mury z du^zych "+
        "wapiennych blok^ow nada dobrze si^e trzymaj^a mimo lat "+
        "porzucenia. A przynajmniej te okalaj^ace p^o^lnocn^a "+
        "cz^e^s^c, kawa^lek na po^ludnie st^ad na sporym odcinku "+
        "zawali^l si^e ods^laniaj^ac widok na las le^z^acy za "+
        "otaczaj^acym zamek w^awozem. Jeszcze gorzej pr^ob^e "+
        "czasu znios^ly drewniane pomosty na murach. Nawet tam "+
        "gdzie kamie^n pozosta^l nietkni^ety pozosta^ly tylko "+
        "kikuty belek stercz�ce z spomi�dzy blok�w.\n"); 
    add_item(({"bram^e"}), "Przej^scie pod masywnym ^lukiem bramy "+
        "zagradza ^zelazna krata, kt^orej pr^ety mimo i^z "+
        "prze^zarte przez rdz�, nadal pozosta�y do^s^c grube "+
        "by powstrzyma^c od latwego pokonania ich. Obok kraty, "+
        "na wysoko^sci kilku s^a^zni, zwisa lu^xno ^la^ncuch "+
        "niegdy^s zapewne po^l^aczony z rdzewiej�cym obok bramy "+
        "ko^lowrotem, teraz za^x nie pozostawiaj^acy tego "+
        "najprostrzego sposobu uniesienia kraty. Niemniej "+
        "przez jej oka wida^c, ^ze most zarwa^l si^e, wi^ec "+
        "trud otwarcia przej^scia by^lby i tak bezowocny.\n");

    set_event_time(400.0);
    add_event("^Zaden z cieni nawet nie drgnie zachowuj^ac "+
        "nienaturalny spok^oj.\n");
    add_event("Wymar^le miejsce wype^lnia jedynie cisza "+
        "przerywana gwizdami wiatru.\n");
    add_event("Kilka czarnych ptak^ow ko^luje ponad "+
        "blankami baszty.\n");
    add_event("Wiatr gwi^zd^ze cicho po^srod gruz^ow i "+
        "kruszej^acych powoli mur^ow.\n");
    add_event("Martwej ciszy nie m^aci najmniejsze nawet "+
        "poruszenie w^sr^od kamieni czy w ocala^lych "+
        "budynkach.\n");
    add_event("Wszechobecna cisza, a^z dzwoni w uszach.\n");
}

string dlugi_opis() 
{

    string str;

    str = "Martwe, kamienne ^sciany otaczaj^a dziedziniec z "+
        "trzech stron. Ociosane wapienie lekko wietrzej�cych "+
        "mur�w obronnych wraz z zamkni^et^a krat^a bramy "+
        "wznosz^a si^e na wschodzie, du^ze, dobrze znosz^ace "+
        "czas bloki z kt�rych zbudowano baszt^e dostrzec "+
        "mo^zna na p^o^lnocy, za^s cz^e^sciowo wal^ace "+
        "si^e �ciany wewn^etrznego zamku przes^laniaj^a "+
        "widnokr^ag na zachodzie. Przer^o^znych rozmiar^ow "+
        "sterty gruzu ";
    
    if (jest_dzien() == 1)
    {
        str += "rzucaj^a nier^owne cienie pozwalaj^ac "+
            "jedynie zgadywa^c rozk^lad i wielko^s^c stoj^acych "+
            "tu niegdy^s, pomniejszych budynk^ow. ";
    }
    else
    {
        str += "rzucaj^a d^lugie, mroczne cienie, tworz^ac tym "+
            "samym dziesi^atki potencjalnych kryj^owek w�r�d "+
            "kamieni. ";
    }
    
    str += "^Srodek dziedzi^nca zajmuje spora studnia murowana "+
        "z ";

    if (jest_dzien() == 1)
    {   
        str += "kolorowych, rzecznych kamieni, otoczona "+
            "zbutwia^lymi resztkami ^zurawia. ";
    }
    else
    {
        str +="ja^sniejszego kamienia otoczona "+
            "nierozpoznawalnymi w tym ^swietle resztkami. ";
    }
    
    if (jest_dzien() == 1)
    {
        str +="Jest jedyn� barwniejsz^a rzecz^a w pobli^zu, "+
            "gdy^z dopiero za obiegaj�cym ruiny w^awozem "+
            "wida^c ciemnozielone czubki sosen. ";
    }
    else
    {
    }
    
    str+="Ani pomi^edzy brukiem dziedzi^nca, ani w^sr^od "+
        "kamieni rumowisk nie spos^ob dostrzec nawet "+
        "nawet ^sladu traw, ro^slin czy jakiegokolwiek innego "+
        "�ycia.\n";
    
    return str;
}

public string
exits_description() 
{
    return "Wej^scia do ocala^lych budynk^ow wiod^a na wsch^od, "+
        "p^o^lnocny-wsch^od i p^o^lnoc, za^s na po^ludniowym-wschodzie "+
        "i po^ludniu rozci^agaj^a si^e ruiny i gruzowiska.\n";
}
