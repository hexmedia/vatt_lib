/**
 * Ruiny na p^olnoc od traktu Rinde - Murivel
 *
 * autor Cairell
 * opisy Tabakista
 * data   2008
 */

#include "dir.h"
inherit RUINY_STD_RUIN;

#include <stdproperties.h>
#include <macros.h>
#include <mudtime.h>

string dlugi_opis();
string opis_kamieni();

void
create_ruiny()
{
    set_short("W^sr^od szcz^atk^ow wal^acego si^e muru.");
    set_long(&dlugi_opis());

    add_prop(ROOM_I_INSIDE,0);
    add_prop(ROOM_S_DARK_LONG, "Mroczne, nieregularne zarysy "+
        "jakich^s budynk^ow majacz^a niewyra^xnie na tle "+
        "o ton ja^sniejszego od nich nieba.\n");

    add_exit(RUINY_LOKACJE + "ruiny_3.c", "n");
    add_exit(RUINY_LOKACJE + "ruiny_4.c", "nw");
    add_exit(RUINY_LOKACJE + "ruiny_6.c", "w");

    add_sit(({"na ziemi"}), "na ziemi","z ziemi",5);
    add_sit(({"na wapiennym bloku"}), "na wapiennym bloku",
        "z wapiennego bloku",2);

    add_item(({"kamienie", "mur", "ruiny"}), &opis_kamieni());
    add_item(({"szczelin^e"}), "Spogl^adaj^ac do wn^etrza "+
        "przez chwil^e widzisz jedynie mrok, lecz "+
        "gdy wzrok zaczyna przyzwyczaja� si^e do ciemno^sci "+
        "dostrzegasz zarys czego^s, co wygl^ada jak ludzki "+
        "szkielet. Blade ko^sci ramion zapieraj^a si^e o "+
        "g^lazy jakby staraj^ac si^e wcisn^a^c jak "+
        "najg^l^ebiej za^x puste oczodo^ly spogl^adaj^a "+
        "wprost na ciebie. Nagle spostrzegasz, ^ze szkielet "+
        "pozbawiony jest n^og, a kikuty ko^sci ud poznaczone "+
        "s^a g^l^ebokimi rysami.\n");
    add_item(({"w^aw^oz"}), "Gardziel g^l^ebokiego parowu "+
        "otwiera si^e zaraz za resztkami muru. Strome, "+
        "niemal pionowe ^sciany rzucaj^a g^l^eboki cie^n "+
        "na jego dno, gdzie dostrzec mo^zna jedynie kilka "+
        "ja^sniejszych plam - kawa^l^ow gruzu, kt^ore przez "+
        "lata przegra^ly w starciu z mrozem i niepogod�.\n");

    set_event_time(400.0);
    add_event("^Zaden z cieni nawet nie drgnie zachowuj^ac "+
        "nienaturalny spok^oj.\n");
    add_event("Wymar^le miejsce wype^lnia jedynie cisza "+
        "przerywana gwizdami wiatru.\n");
    add_event("Kilka czarnych ptak^ow ko^luje ponad "+
        "blankami baszty.\n");
    add_event("Wiatr gwi^zd^ze cicho po^srod gruz^ow i "+
        "kruszej^acych powoli mur^ow.\n");
    add_event("Martwej ciszy nie m^aci najmniejsze nawet "+
        "poruszenie w^sr^od kamieni czy w ocala^lych "+
        "budynkach.\n");
    add_event("Wszechobecna cisza, a^z dzwoni w uszach.\n");
}

string dlugi_opis() 
{
    string str;

    str = "Wi^eksza cz^e^s^c placu zaj^eta jest przez sterty "+
        "gruzu i kamieni, kt^ore pozosta�y po tym jak "+
        "przewr^oci^l si^e mur obronny. ";

    if (jest_dzien() == 1)
    {
        str += "Promienie s�o^nca ta^ncz^a mi^edzy nimi "+
            "bezskutecznie staraj^ac si^e nada^c odrobin^e "+
            "ciep^la ch^lodnym, martwym kamieniom, po^sr^od "+
            "kt^orych nie przemyka nawet najmniejsze zwierz^e";
    }
    else
    {
        str += "W zimnym ksi^e^zycowym ^swietle cienie "+
            "wyd^lu^zaj^a si^e i g^estniej^a tworz^ac pl^atanin^e "+
            "mrocznych kszta^lt^ow, w^sr^od kt^orej kryj^owek "+
            "wystarczy�oby dla ca^lej hordy wszelkiej ma^sci "+
            "potwor^ow";
     }
     
     str+=". Bli^zej p^o^lnocnego skraju rumowiska wyrasta "+
         "sp^ekana ^sciana wewn^etrznego zamku, ponad kt^or^a "+
         "wznosi si^e ledwie widoczny st^ad kontur baszty. "+
         "Mur przy nich pozosta^l w lepszym stanie ni^z "+
         "tutaj, gdzie jego wi^ekszo^s^c le^zy na bruku, "+
         "lub spoczywa na dnie biegn^acego tu^z za jego "+
         "okr^egiem g^l^ebokiego w^awozu. Jedynie kilka "+
         "niskich, cudem ocala�ych fragment�w nadal stoi "+
         "mniej lub bardziej prosto.\n";
    
    return str;
}

string opis_kamieni()
{
    string strr;

    strr = "Ca^le fragmenty muru nadal po^l^aczone zapraw^a "+
        "le^z^a woko^lo w stertach przemieszane z mniejszymi "+
        "kamieniami";

    if (pora_roku() == MT_ZIMA)
    {
        strr+=", zaspami �niegu";
    }
    
    strr+= " i blokami ociosanego wapienia. ";
    
    if (jest_dzien() == 1)
    {
        strr+="Mimo s^lonecznego ^swiat^la g^l^ebokie cienie "+
            "nadal skrywaj^a spor^a ich cz^e^s^c";
    }
    else
    {
        strr+="G��bokie cienie tworz^a si^e wsz^edzie gdzie "+
            "nie si^ega blada, ksi^e^zycowa po^swiata";
    }
    
    strr+=". Kilka wystrz^epionych fragment�w muru nadal "+
        "stoi w miar^e prosto, lecz wi^ekszo^s^c le^zy w "+
        "rumowiskach, lub spad^la na dno otwieraj^acego "+
        "si^e za nim w^awozu. W jednym miejscu du^zy "+
        "g^laz opar^l si^e o spor^a pryzm^e mniejszych "+
        "tworz^ac ciasn^a, ale g^l^ebok^a szczelin^e.\n";

    return strr;
}

public string
exits_description() 
{
    return "W murze na p^o�nocy widnieje niskie wej^scie, "+
        "natomiast id^ac w^sr^od gruz�w dotrze^c mo�na na "+
        "zach^od, lub p^o^lnocny-zach^od.\n";
}
