/**
 * Groty niedaleko ruin na p^olnoc od traktu Rinde - Murivel
 *
 * autor Cairell
 * opisy Tabakista
 * data   2008
 */

#include "dir.h"
inherit RUINY_STD_JASKIN;
#include <stdproperties.h>
#include <macros.h>

string dlugi_opis();

void
create_jaskinie()
{
    set_short("Przy starym rumowisku.");
    set_long(&dlugi_opis());
    
    add_sit(({"na g^lazie"}), "na jednym z g^laz^ow","z g^lazu",1);

    add_exit(RUINY_LOKACJE + "jaskinia_10.c", "e");
    add_exit(RUINY_LOKACJE + "jaskinia_11.c", "se");
    add_exit(RUINY_LOKACJE + "ruiny_7.c", "u");
    
    add_item(({"osuwisko", "szczelin^e", "rumowisko"}), "Ska^ly osun^e^ly "+
        "si^e zostawiaj^ac szczelin^e niewiele szersz^a ni^z ludzki tors i "+
        "zw^e^zaj^ac^a si^e jeszcze im g^l^ebiej si^egn^a^c, lecz za to "+
        "zapewniaj^ac^a w miare r^owne oparcie pozwalaj^ace si^egn^a^c do "+
        "znajduj^acego si^e ponad rumowiskiem otworu.\n");
}

string dlugi_opis() 
{
    string str;

    str = "Dooko^la ciebie le^z^a liczne skruszone ska^ly, spore g^lazy i "+
        "mniejsze kamienie. Osun^e^ly si^e one z zachodniej ^sciany, kto^ra "+
        "sta^la si^e teraz pochy^l^a, zw^e^zaj^ac^a si^e wg^l^ab szczelin^a. "+
        "Jedynie u samej g^ory dostrzec mo^zna niewyra^zn^a czer^n otworu, "+
        "od strony kt^orego daje si^e wyczu^c leciutki podmuch powietrza. "+
        "Ostre kraw^edzie g^laz^ow woko^lo zd^a^zy^la wyg^ladzi^c ju^z cienka "+
        "warstwa soli skapuj^acej z odleg^lego stropu jaskini. Mimo ruchu "+
        "powietrza nadal wyczuwalny jest tutaj dziwny, pi^zmowy zapach.\n";

    return str;
}

public string
exits_description() 
{
    return "Na wschodzie i po^ludniowym-wschodzie rozci^aga si^e "+
        "dalsza cz^e^c groty, zas po rumowisku mo^zna wspi^a^c si^e "+
        "na g^or^e.\n";
}
