/**
 * Groty niedaleko ruin na p^olnoc od traktu Rinde - Murivel
 *
 * autor Cairell
 * opisy Tabakista
 * data   2008
 */

#include "dir.h"
inherit RUINY_STD_JASKIN;
#include <stdproperties.h>
#include <macros.h>

string dlugi_opis();

void
create_jaskinie()
{
    set_short("^Srodek pieczary.");
    set_long(&dlugi_opis());

    add_exit(RUINY_LOKACJE + "jaskinia_12.c", "w");
    add_exit(RUINY_LOKACJE + "jaskinia_8.c", "e");
    add_exit(RUINY_LOKACJE + "jaskinia_11.c", "s");
    add_exit(RUINY_LOKACJE + "jaskinia_9.c", "se");
}

string dlugi_opis() 
{
    string str;

    str = "^Sciany tej du^zej groty nikn^a poza kr^egiem ^swiat^la. Jedynie "+
        "p^o^lnocna majaczy niewyra^znie, staj^ac si^e siedliskiem setek "+
        "rozedrganych cieni ^l^acz^acych si^e mroczn^a zas^lon^a nie "+
        "pozwalaj^ac^a dojrze^c stropu. Nawet wy^laniaj^ace si^e z niego "+
        "spiczaste kszta^lty stalaktyt^ow s^a nieliczne i odleg^le, nie "+
        "pozwalaj^ace zgadn^a^c jak wysoka jest w^la^sciwie ta jaskinia. Pod "+
        "nogami co kawa^lek widzi si^e wi^eksze i mniejsze ka^lu^ze, do "+
        "kt^orych sk^ad^s miarowo kapie woda.\n";

    return str;
}

public string
exits_description() 
{
    return "Ma wschodzie, zachodzie, po^ludniu i po^ludniowym-wschodzie "+
        "rozci^aga si^e dalsza cz^e^c groty.\n";
}
