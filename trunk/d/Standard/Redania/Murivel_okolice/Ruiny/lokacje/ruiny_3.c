/**
 * Ruiny na p^olnoc od traktu Rinde - Murivel
 *
 * autor Cairell
 * opisy Tabakista
 * data   2008
 */

#include "dir.h"
inherit RUINY_STD_RUIN;

#include <stdproperties.h>
#include <macros.h>
#include <mudtime.h>

string dlugi_opis();
string opis_tarczy();

void
create_ruiny()
{ 
    set_short("Pusta sala.");
    set_long(&dlugi_opis());

    add_prop(ROOM_I_INSIDE,1);
    remove_prop(ROOM_I_LIGHT);
    add_prop(ROOM_S_DARK_LONG, "W ciemno^sci majacz^a "+
        "zarysy jakich^s kszta^lt^ow, wylawianych "+
        "z mroku przez po^swiate padaj^ac^a przez "+
        "dziurawy dach.\n");

    add_exit(RUINY_LOKACJE + "ruiny_1.c", "n");
    add_exit(RUINY_LOKACJE + "ruiny_5.c", "s");
    add_exit(RUINY_LOKACJE + "ruiny_4.c", "w");
    add_exit(RUINY_LOKACJE + "ruiny_6.c", "sw");

    add_sit(({"na pod^lodze"}), "na pod^lodze","z pod^logi",10);
    add_sit(({"pod ^scian^a"}), "pod ^scian^a","z pod ^sciany",5);

    add_item(({"okna"}), "Umieszczone wysoko nad ziemi^a "+
        "w^askie okna si^egaj^a niemal wal^acego si^e "+
        "dachu. Ka^zde z nich ma z pewno^sci^a co najmniej "+
        "dziesi^e^c st^op wysoko^sci, lecz niewiele ponad "+
        "dwie szeroko^sci. Pozbawione zas^lon czy "+
        "jakichkolwiek ram s� jedynie otworami w "+
        "grubym, wapiennym murze.\n");
    add_item(({"herb", "tarcz^e", "kszta�t", "ciemny kszta�t"}),
        &opis_tarczy());
    
    set_event_time(400.0);
    add_event("Na parapecie jednego z okien przysiad^l "+
        "kruk obserwuj�c ci� przez moment w ciszy.\n");
    add_event("Cichy trzepot pi^or burzy na moment "+
        "cisz^e tego miejsca.\n");
    add_event("Wymar^le miejsce wype^lnia jedynie cisza "+
        "przerywana gwizdami wiatru.\n");
    add_event("Wiatr gwi^zd^ze cicho po pustych "+
        "pomieszczeniach.\n");
    add_event("Martwej ciszy nie m^aci najmniejsze nawet "+
        "poruszenie w^sr^od kamieni czy w ocala^lych "+
        "budynkach.\n");
    add_event("Wszechobecna cisza, a^z dzwoni w uszach.\n");
    add_event("W panuj^acej dooko^la ciszy s^lycha^c "+
        "miarowe cykanie kornik^ow pracowicie "+
        "niszcz^acych dach.\n");
}

string dlugi_opis() 
{
    string str;

    str = "Pod^lu^zna sala jest ca^lkowicie pozbawiona mebli, "+
        "czy codziennych przedmiot^ow. Jedynie naprzeciw "+
        "wej^scia, niemal pod sufitem ";
    
    if (jest_dzien() == 1)
    {
        str += "wisi wielka herbowa tarcza z d�bowego drewna. ";
    }
    else
    {
        str += "przycupn^a^l wielki, ciemny kszta^lt zdaj�cy "+
            "si^e na wp^o^l rozpo^sciera^c skrzyd^la. ";
    }

    str += "Komnata s�dz�c z po^lo^zenia oraz "+
        "znacznych rozmiar^ow mog^la by^c sal^a g^l^owna "+
        "na zamku. Odleg^la powa^la kryje wi^ekszo�� "+
        "sali, lecz w po^ludniowej cz^e^sci zaczyna si^e "+
        "stopniowo wali^c, a przez powsta�� dziur^e ";
    
    if (pora_roku() == MT_ZIMA)    
    {
        str += "wiatr nawiewa pojedyncze p�atki �niegu. ";
    }

    if (pora_roku() == MT_WIOSNA)
    {
        str += "wpadaj� podmuchy ciep�ego wiosennego wiatru. ";
    }

    if (pora_roku() == MT_LATO)
    {
        str += "dociera ciep^ly wiatr rozpraszaj^acy "+
            "cz^e^sciowo panuj^acy tutaj ch^l^od. ";
    }

    if (pora_roku() == MT_JESIEN)
    {
        str += "wpada ch^lodny, jesienny wiatr. "; 
    }

    str += "Tak^ze ^sciana w tej cz^e^sci, mimo ^ze ociosane, "+
           "wapienne bloki solidnie ^l^aczy zaprawa, nosi kilka "+
           "szerokich p^ekni^e^c, biegn^acych od ciasnej wn^eki "+
           "jednego z okien wychodz^acych w tamt� stron^e. Drobne "+
           "od^lamki skalne ju^z teraz le^z^a w sporej ilo^sci na "+
           "nier^ownej, kamiennej pod^lodze.\n";

    return str;
}

string opis_tarczy()
{
    string strr;

    if (jest_dzien() == 1)
    {
        strr = "Wielka d^ebowa tarcza "+
        "heraldyczna wisi majestatycznie pod powa^l^a "+
        "naprzeciw wej^scia. Ostatnia pozosta�o^s^c po "+
        "mieszka^ncach zamku nie u�atwia odgadni^ecia ich "+
        "to^zsamo^sci. Farba z�uszczy^la si^e przez lata, drewno "+
        "pociemnia^lo ca^lkowicie. Prawdopodobnie czterodzielny "+
        "herb w jednym polu ma jakie^s trudne do rozpoznania "+
        "wspinaj^ace si^e na tylne nogi zwierze. W s^asiednim "+
        "czarna, czy raczej obecnie szara tynktura, jakie^s "+
        "trzy ja^sniejsze symbole. Lilie? Romby?\n";
    }
    else
    {
        strr = "Daleko poza kr^egiem ^swiat^la, wysoko pod "+
            "powa^l^a, przykucn^al wielki mroczny kszta^lt, "+
            "zamar^ly w bezruchu i jakby u^spiony. Pozostaje "+
            "jedynie ^zywi^c nadziej^e i^z jest to jakie^s "+
            "dawne zdobieniei nie otworzy nagle oczu "+
            "wypatruj^ac posi^lku.\n";
    }
    return strr;
}

public string
exits_description() 
{
    return "Niewielkie wn^eki przej^s^c prowadz^a na p^o^lnoc, "+
        "po^ludnie i po^ludniowy-zach^od, za^s g^l^owne, "+
        "tak^ze pozbawione wr^ot wyj^scie wiedzie na zach^od.\n";
}
