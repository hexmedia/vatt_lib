/**
 * Groty niedaleko ruin na p^olnoc od traktu Rinde - Murivel
 *
 * autor Cairell
 * opisy Tabakista
 * data   2008
 */

#include "dir.h"
inherit RUINY_STD_JASKIN;
#include <stdproperties.h>
#include <macros.h>

string dlugi_opis();

void
create_jaskinie()
{
    set_short("W du^zej pieczarze.");
    set_long(&dlugi_opis());

    add_exit(RUINY_LOKACJE + "jaskinia_6.c", "ne");
    add_exit(RUINY_LOKACJE + "jaskinia_10.c", "w");
    add_exit(RUINY_LOKACJE + "jaskinia_11.c", "sw");
    add_exit(RUINY_LOKACJE + "jaskinia_9.c", "s");

}

string dlugi_opis() 
{
    string str;

    str = "Dwie olbrzymie masy wapiennej ska^ly p^o^lnocnej i wschodniej "+
        "^sciany opieraj^a si^e niemal o siebie pozostawiaj^ac w^ask^a, ale "+
        "za to bardzo wysok^a szczelin^e przechodz^ac^a w nikn^acy w mroku "+
        "korytarz. Reszta pieczary tak^ze jest do^s^c imponuj^acych jak na te "+
        "strony rozmiar^ow. Z odleg^lego stropu opadaj^a wielkie stalaktyty, "+
        "z kt^orych jednak tylko nieliczne si^egaj^a pofa^ldowanego, pe^lnego "+
        "zdradliwych i ^sliskich szczelin dna. Cienie drgaj^a niepokoj^aco "+
        "w^sr^od licznych zag^l^ebie^n, naciek^ow i szczelin w ^scianach, "+
        "w^sr^od kt^orych kryj^owk^e bez trudu znalaz^lby nie jeden, a tuzin "+
        "potwor^ow.\n";

    return str;
}


public string
exits_description() 
{
    return "Korytarz ci^agnie si^e dalej na p^o^lnocny-wsch^od, za^s na "+
        "zachodzie, po^ludniu i po^ludniowym-zachodzie rozci^aga si^e "+
        "dalsza cz^e^c groty.\n";
}
