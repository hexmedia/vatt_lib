/**
 * Groty niedaleko ruin na p^olnoc od traktu Rinde - Murivel
 *
 * autor Cairell
 * opisy Tabakista
 * data   2008
 */

#include "dir.h"
inherit RUINY_STD_JASKIN;
#include <stdproperties.h>
#include <macros.h>

string dlugi_opis();

void
create_jaskinie()
{
    set_short("Jaskinie.");
    set_long(&dlugi_opis());

    add_exit(RUINY_LOKACJE + "jaskinia_4.c", "e");
    add_exit(RUINY_LOKACJE + "jaskinia_6.c", "s");
    add_exit(RUINY_LOKACJE + "jaskinia_7.c", "nw");

    add_item(({"otw^or"}), "Niskie przej^scie wiedzie na p^o^lnocny zach^od. "+
        "Korytarz ten do^s^c wyra^xnie opada w d^o^l, lecz dalece nie na "+
        "tyle, by utrudnia^c zej^scie nim. Nawet zacieki, nie przegradzaj^a "+
        "go zbyt cz^esto. Niepokoj^acy staje si^e natomiast pi^zmowy zapach, "+
        "zdecydowanie ^latwiej wyczuwalny z bocznej jaskini.\n");
}

string dlugi_opis() 
{
    string str;

    str = "Jeste^s w rogu sporej groty, gdzie wapienne pod^lo^ze do^s^c "+
        "stromo pnie si^e na spotkanie z opadaj^acym stropem. Stalaktyt^ow "+
        "jest tutaj jakby wi^ecej ni^z w innych cz^e^sciach jaskini, a z "+
        "pewno^sci^a s^a one okazalsze. Kilka z nich si^egn^e^lo ju^z nawet "+
        "dna tworz^ac kolumny cz^e^sciowo przes^laniaj^ace otw^or "+
        "niewielkiego, odchodz^acego w bok korytarza. Wprawdzie w tym miejscu "+
        "jest bardziej sucho, lecz odnoga jaskini wydaje si^e ponownie "+
        "opada^c w d^o^l zapowiadaj^ac kolejne lodowato zimne ka^lu^ze. \n";

    return str;
}

public string
exits_description() 
{
    return "Korytarz ci^agnie si^e dalej na po^ludnie, wsch^od i "+
        "p^o^lnocny-zach^od.\n";
}
