/**
 * Groty niedaleko ruin na p^olnoc od traktu Rinde - Murivel
 *
 * autor Cairell
 * opisy Tabakista
 * data   2008
 */

#include "dir.h"
inherit RUINY_STD_JASKIN;
#include <stdproperties.h>
#include <macros.h>

string dlugi_opis();

void
create_jaskinie()
{
    set_short("Ostry zakr^et korytarza.");
    set_long(&dlugi_opis());

    add_exit(RUINY_LOKACJE + "jaskinia_4.c", "nw");
    add_exit(RUINY_LOKACJE + "jaskinia_2.c", "sw");
}

string dlugi_opis() 
{
    string str;

    str = "Niski korytarz skr^eca w tym miejscu gwa^ltownie jakby "+
        "wyp^lukuj^aca wapienne ska^ly woda zmieni^la nagle zamiary. "+
        "W kilku niewielkich zag^l^ebieniach w dnie zebra^lo si^e nieco "+
        "wymieszanej z drobnymi kamykami i ^zwirem wody, lecz poza nimi "+
        "pod^lo^ze niezbyt szybko, ale wyra^znie opada w kierunku "+
        "po^ludniowo zachodnim. Strop jest do^s^c niski, lecz na szcz^e^scie "+
        "w tym miejscu nie wyros^ly ^zadne stalaktyty, nic wi^ec nie "+
        "utrudnia w^edr^owki przez ciemny korytarz.\n";

    return str;
}

public string
exits_description() 
{
    return "Korytarz ci^agnie si^e dalej na po^ludniowy-zach^od i "+
        "p^o^lnocny-zach^od.\n";
}
