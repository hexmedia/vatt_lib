/**
 * Groty niedaleko ruin na p^olnoc od traktu Rinde - Murivel
 *
 * autor Cairell
 * opisy Tabakista
 * data   2008
 */

#include "dir.h"
inherit RUINY_STD_JASKIN;
#include <stdproperties.h>
#include <macros.h>

string dlugi_opis();

void
create_jaskinie()
{
    set_short("W w^askiej szczelinie.");
    set_long(&dlugi_opis());

    add_exit(RUINY_LOKACJE + "jaskinia_8.c", "sw");
    add_exit(RUINY_LOKACJE + "jaskinia_4.c", "ne");
    add_exit(RUINY_LOKACJE + "jaskinia_5.c", "n");
}

string dlugi_opis() 
{
    string str;

    str = "Strop w tej cz^e^sci jaskini znajduje si^e tak wysoko, ^ze w tym "+
        "^swietle dostrzec mo^zna jedynie spiczaste ko^nce stalaktyt^ow "+
        "wisz^ace nad twoj^a g^low^a. Korytarz zw^e^za si^e przy tym i to "+
        "znacznie, na szczeg^olnie po^ludniowym zachodzie gdzie ka^zdy bez "+
        "trudu by^lby w stanie si^egn^a^c od jednej wapiennej, okrytej "+
        "naciekami ^sciany, do drugiej. Pod^lo^ze jest do^s^c suche, gdy^z "+
        "opada w miar^e regularnie pozwalaj^ac wodzie ^scieka^c dalej. Nawet "+
        "piasek i ^zwir s^a tu trudne do zauwa^zenia.\n";

    return str;
}

public string
exits_description() 
{
    return "Korytarz ci^agnie si^e dalej na p^o^lnoc, p^o^lnocny "+
        "wsch^od i po^ludniowy-zach^od.\n";
}
