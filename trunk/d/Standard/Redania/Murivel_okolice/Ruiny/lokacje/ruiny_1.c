/**
 * Ruiny na p^olnoc od traktu Rinde - Murivel
 *
 * autor Cairell
 * opisy Tabakista
 * data   2008
 */

#include "dir.h"
inherit RUINY_STD_RUIN;

#include <stdproperties.h>
#include <macros.h>

string dlugi_opis();

void
create_ruiny()
{ 
    set_short("Zrujnowana kaplica.");
    set_long(&dlugi_opis());

    add_prop(ROOM_I_INSIDE,1);
    remove_prop(ROOM_I_LIGHT);
    add_prop(ROOM_S_DARK_LONG, "W ciemno^sci majacz^a "+
        "zarysy jakich^s kszta^lt^ow, wy^lawianych "+
        "z mroku przez wpadaj^ac^a przez wej^scie, "+
        "blad^a po^swiat^e.\n");

    add_exit(RUINY_LOKACJE + "ruiny_8.c", "d");
    add_exit(RUINY_LOKACJE + "ruiny_3.c", "s");
    add_exit(RUINY_LOKACJE + "ruiny_4.c", "sw");

    add_sit(({"na posadzce"}), "na posadzce","z posadzki",10);
    add_sit(({"pod ^scian^a"}), "pod ^scian^a","z pod ^sciany",5);

    add_object(RUINY_OBIEKTY + "plyta.c", 1);
    add_object(RUINY_OBIEKTY + "oltarz.c", 1);

    dodaj_rzecz_niewyswietlana("kamienna p^lyta",1);
    dodaj_rzecz_niewyswietlana("wapienny o^ltarz",1);

    add_item(({"otw^or"}), "Spogl^adaj^ac w mroczny otw^or dostrzec "+
        "mo^zna jedynie kilka pierwszych stopni nikn^acych w mroku "+
        "w^askich schodk^ow. Bije stamt^ad przenikliwy ch^l^od "+
        "si^egaj^acy, zdawa^loby si^e, do szpiku ko^sci. Kraw^edzie "+
        "otworu s^a idealnie g^ladkie i r^owne, kamienna p^lyta "+
        "musia^la dawniej bardzo dok^ladnie do nich przylega^c. "+
        "Jedynie w jednym miejscu cztery r^ownoleg^le, ledwie "+
        "dostrzegalne rysy znacz^a brzeg otworu.\n");

    set_event_time(400.0);
    add_event("Przenikliwy ch^l^od bije z otworu w posadzce.\n");
    add_event("Wszechobecna cisza, a^z dzwoni w uszach.\n");
    add_event("Z zewn^atrz dochodzi przez chwil^e odleg^le "+
        "krakanie.\n");
    add_event("^Zaden z cieni nawet nie drgnie zachowuj^ac "+
        "nienaturalny spok^oj.\n");
    add_event("Wymar^le miejsce wype^lnia jedynie cisza "+
        "przerywana gwizdami wiatru.\n");
    add_event("Wiatr gwi�d�e cicho po pustych pomieszczeniach.\n");
}

string dlugi_opis() 
{
    object *inwentarz;
    inwentarz = all_inventory();

    string str;

    str = "Nisk^a, ciemn^a sal^e wype^lnia jedynie cisza i ch^l^od "+
        "bij^acy z ogo^loconych ^scian, jakby skupiaj^acy si^e "+
        "woko^lo ziej^acego w ^srodku pod^logi otworu.";

    if (present ("p^lyta", inwentarz) == 0)
    {
        str += " Tu^z obok niego le^zy sp^ekana kamienna p^lyta";
        
        if (present ("o^ltarz", inwentarz) == 0)
        {
            str += " poza kt^or^a jedynym znajduj^acym si^e tutaj "+
                "przedmiotem jest stoj^acy pod p^o^lnocn^a ^scian^a "+
                "o^ltarz. Wapienny blok s^lu^z^acy za jego podstaw^e "+
                "oraz ^s";
        }
        else
        {
            str += ". ^S";
        }
    }
    else
    {
        if (present ("o^ltarz", inwentarz) == 0)
        {
            str += " Jedynym znajduj^acym si^e tutaj przedmiotem jest "+
                "stoj^acy pod p^o^lnocn^a ^scian^a o^ltarz. Wapienny "+
                "blok s^lu^z^acy za jego podstaw^e oraz ^s";
        }
        else
        {
            str += "^S";
        }
    }

    str += "ciany z g^ladko ociosanych kamieni nie pokrywaj^a ^zadne "+
        "zdobienia, czy malunki. Je^sli za^s nawet wisia^ly tu jakie^s "+
        "ozdoby, to ^slad po nich zagin^a^l.\n";

    return str;
}

public string
exits_description() 
{
    return "Niewysokie dziury po drzwiach prowadz^a na po^ludnie i "+
        "po^ludniowy-zach^od, za^s otw^or w pod^lodze wiedzie na d^o^l.\n";
}
