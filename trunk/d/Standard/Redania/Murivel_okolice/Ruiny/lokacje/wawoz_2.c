/**
 * W^awoz na p^olnoc od traktu Rinde - Murivel
 *
 * autor Cairell
 * opisy Tabakista
 * data   2008
 */

#include "dir.h"
inherit RUINY_STD_WAWOZU;
#include <pogoda.h>
#include <stdproperties.h>
#include <macros.h>

string dlugi_opis();
string opis_mostu();

void
create_wawoz()
{ 
    set_short("G^l^eboki w^aw^oz");
    set_long(&dlugi_opis());

    add_exit(RUINY_LOKACJE + "wawoz_1.c", "nw");
    add_exit(RUINY_LOKACJE + "wawoz_3.c", "se");

    add_item(({"most", "belki", "stert^e"}), &opis_mostu());
    add_item(({"ruiny"}), "Wysoko nad twoj^a g^low^a "+
        "powoli obracaj^acy si^e w gruzy mur przechyla "+
        "si^e nad kraw^edzi^a parowu. Stare kamienie "+
        "wygl^adaj^a jakby oczekiwa^ly jedynie silniejszej "+
        "burzy by run^a^c w d^o^l zasypuj^ac skaliste dno "+
        "tysi^acami od^lamk^ow. Fragment muru blisko bramy "+
        "obro^sni^ety jest jakim^s anemicznym pn^aczem "+
        "zdaj^acym si^e trzyma^c kamienie w "+
        "ca^lo^sci, lecz tocz^acym beznadziejn^a walk^e "+
        "z czasem i niepogod^a.\n");
}

string dlugi_opis() 
{
    string str;

    str = "Wznosz^ace si� po obu stronach ^sciany nieznacznie, "+
        "lecz stale oddalaj^a si^e od siebie im dalej od "+
        "widocznego w oddali zerwanego mostu. Jednocze^snie "+
        "staj^a si^e coraz bardziej strome i niedost^epne, a ";
    
    if (pora_roku() == MT_ZIMA)
    {
        str += "suche ^lodygi paproci i bluszczu ";
    }
    else
    {
        str += "porastaj^ace je zielone paprocie i bluszcze ";
    }

    
    str += "powoli ust^epuj^a miejsca nagim, wapiennym "+
        "ska^lom si^egaj^acym a^z do widocznych w g^orze "+
        "ruin. P^laskie dno za^scielaj^a przer^o^znych "+
        "kszta^lt^ow i rozmiar^ow kamienie, kt^orych cz^e^s^c "+
        "wyg^ladzonymi kraw^edziami wskazuje dawny bieg "+
        "strumienia. Obecnie wprawdzie nie wida^c nigdzie "+
        "nurtu, lecz wi^ekszo^s^c g^laz^ow i tak pokrywa ";

    if (pora_roku() == MT_ZIMA)
    {
        if(CZY_JEST_SNIEG(TO))
        {
            str += "l^od, a z ^snie^znych zasp stercz^a "+
                "czubki paproci.\n";
        }
        else
        {
            str += "szron, a z k^at^ow stercz^a suche patyki po pokrzywach.\n";
        }
    }
    else
    {
        str += "wilgo^c, a z co bardziej zacienionych "+
            "miejsc wychylaj^a si^e k^epy paproci.\n";
    }
    return str;
}

string opis_mostu()
{
    string strr;

    strr = "Widoczna w oddali konstrukcja, niegdy^s "+
        "^l^acz^aca zbocza parowu, obecnie spoczywa "+
        "na jego dnie skryta niemal zupe�nie ";
    
    if (pora_roku() == MT_ZIMA)
    {
        strr += "pod grub^a warstw^a ^sniegu, kt^orego "+
            "wiatr nie by^l w stanie przenie^s^c przez "+
            "t^e barier^e.";
    }
    else
    {
        strr += "w g^estej ro^slinno^sci wybuja^lej "+
            "dzi^eki wsz^edobylskiej wilgoci.";
    }

    if (jest_dzien() == 1)
    {
        strr += " Dopiero teraz, z pewnej odleg^lo^sci, "+
            "^latwiej dostrzec mo^zna usypany od wschodu "+
            "podjazd zabezpieczony z boku niskim "+
            "kamiennym murkiem, ca^lkiem nie^xle "+
            "znosz^acym pr^ob^e czasu.\n";
    }
    else
    {
        strr += "\n";
    }
    return strr;
}

public string
exits_description() 
{
    return "W^aw^oz biegnie prosto na po^ludniowy-wsch^od i "+
        "p^o^lnocny-zach^od.\n";
}
