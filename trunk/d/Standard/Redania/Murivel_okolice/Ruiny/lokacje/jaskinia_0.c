/**
 * Groty niedaleko ruin na p^olnoc od traktu Rinde - Murivel
 *
 * autor Cairell, Vera
 * opisy Tabakista
 * data   2008
 */

#include "dir.h"
inherit RUINY_STD_JASKIN;
#include <stdproperties.h>
#include <macros.h>

void
create_jaskinie()
{
    set_short("Dno rozpadliny.");
    set_long("Dnem rozpadliny p^lynie rw^acy potok, kt^orego nury "+
        "ci^ety jest i spi^etrzany przez ostre ska^ly.\n");
}

void
enter_inv(object ob, object from)
{
    ::enter_inv(ob, from);
    
    if(ob->query_lit())
        ob->extinguish_me();
    
    foreach(object lampa : filter(AI(ob), &->query_lit()))
        lampa->extinguish_me();
    
    //wywalamy zamoczenie, bo b�dziemy dawa� nowe przy wyj�ciu z wody
    foreach(object kropelki : filter(AI(ob), &->query_jestem_efektem_przemoczenia()))
        kropelki->wysusz();
}

//pomocnicza funkcja do filtrowania ekwipunku gracza.
int
filtruj_niewidoczne(object ob)
{
    return !(ob->query_no_show() || ob->query_no_show_composite() ||
        /*ob->query_prop(OBJ_I_DONT_INV) || bleble! g�wno. Obiekty w subloku tez maj� by� liczone!*/
        ob->query_prop(OBJ_M_NO_DROP) ||
        ob->query_prop(WIZARD_ONLY) );
}

void
leave_inv(object ob, object to)
{
    ::leave_inv(ob,to);
    
    //gubienie rzeczy
    if(!ob->is_living())
        return;
    
    object *ekw = filter(AI(ob), &filtruj_niewidoczne());
    foreach(object t : ekw)
        if(!random(3)) //33% szans na zgubienie tej rzeczy
            t->move(TO, 1);
    
}
