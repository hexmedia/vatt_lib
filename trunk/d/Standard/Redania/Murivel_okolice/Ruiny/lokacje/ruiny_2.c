/**
 * Ruiny na p^olnoc od traktu Rinde - Murivel
 *
 * autor Cairell
 * opisy Tabakista
 * data   2008
 */

#include "dir.h"
inherit RUINY_STD_RUIN;

#include <stdproperties.h>
#include <macros.h>
#include <mudtime.h>

string dlugi_opis();

void
create_ruiny()
{ 
    set_short("Wn^etrze pustej baszty.");
    set_long(&dlugi_opis());

    add_prop(ROOM_I_INSIDE,0);
    add_prop(ROOM_S_DARK_LONG, "Otacza ci^e niemal "+
        "namacalny mrok, bezglo^snie rozdzierany "+
        "przez wpadaj^ac^a przez wylot wie^zy, "+
        "nik^l^a po^swiat^e.\n");

    add_exit(RUINY_LOKACJE + "ruiny_4.c", "s");

    add_sit(({"na ziemi"}), "na ziemi","z ziemi",5);
    add_sit(({"na belce"}), "na spr^ochnia^lej belce",
        "z spr^ochnia^lej belki",2);

    add_item(({"stert^e", "stert^e belek", "stos", 
        "stos belek", "belki"}), "Sterta spr^ochnia^lych "+
        "belek i potrzaskanych dach^owek jest "+
        "wszystkim, co pozosta^lo po dachu oraz "+
        "drewnianych pod^logach kolejnych pi^eter "+
        "baszty. Zajmuj^a one wi^eksz^a cz^e^^c dna "+
        "wie^zy, ka^za^c patrze^c pod nogi, nie za^s "+
        "przygl^ada^c si^e skrawkowi nieba widocznemu "+
        "ponad blankami. Pomi^edzy nimi, mimo ^latwo "+
        "wyczuwalnej wilgoci, nie ro^nie nawet k^epka "+
        "mchu. Liczne ptacie odchody s^a wszystkim "+
        "co pokrywa szare, wapienne ^sciany.\n");

    set_event_time(400.0);
    add_event("Samotny kruk zerwa^l si^e z jakiej^s niszy "+
        "w ^scianie i powoli zmierza ku niebu ko^luj^ac "+
        "wewn^atrz pustej baszty.\n");
    add_event("Chrypliwe krakanie odbija si^e "+
        "nieprzyjemnym echem pomi^edzy go^lymi ^scianami.\n");
    add_event("Cichy trzepot pi^or burzy na moment "+
        "cisz^e tego miejsca.\n");
    add_event("Wymar^le miejsce wype^lnia jedynie cisza "+
        "przerywana gwizdami wiatru.\n");
    add_event("Wiatr gwi^zd^ze cicho po pustych "+
        "pomieszczeniach.\n");
    add_event("Martwej ciszy nie m^aci najmniejsze nawet "+
        "poruszenie w^sr^od kamieni czy w ocala^lych "+
        "budynkach.\n");
    add_event("Wszechobecna cisza, a^z dzwoni w uszach.\n");
}

string dlugi_opis() 
{
    string str;

    str = "Wn^etrze szerokiej baszty o okr^ag^lej "+
        "podstawie w wi^ekszo^sci zaj^ete jest "+
        "przez wielki stos przegni^lych belek, "+
        "dach^owek pokrywaj^acych niegdyś jej dach, "+
        "schod^ow i drabin ^l^acz^acych kolejne "+
        "pi^etra oraz wszystkiego, co nie zosta^lo "+
        "wyszabrowane nim run^e^ly stropy. "+
        "Spogl^adaj^ac w g^or^e, ponad pr^ochniej^ac^a "+
        "stert^a, poprzez przypominającą komin pust^a "+
        "wie^z^e widzi si^e okalany blankami, odleg^ly "+
        "skrawek ";

    if (pora_roku() == MT_ZIMA)
    {
        str += "zimowego ";
    }
    if (pora_roku() == MT_WIOSNA)
    {
        str += "wiosennego ";
    }
    if (pora_roku() == MT_LATO)
    {
        str += "letniego ";
    }
    else
    {
        str += "jesiennego ";
    }
     
    str += "nieba. Im wy^zej tym mniej jest pokrywających "+
        "zimne, wapienne ^sciany porost^ow utrzymuj^acych "+
        "w powietrzu wilgo^c oraz sprawiaj^acych, ^ze "+
        "zapach butwiejącego drewna jest jeszcze "+
        "intensywniejszy.\n";
    
    return str;
}

public string
exits_description() 
{
    return "Ciasne wyj^scie wiedzie na po^ludnie.\n";
}
