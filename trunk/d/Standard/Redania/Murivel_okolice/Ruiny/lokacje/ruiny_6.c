/**
 * Ruiny na p^olnoc od traktu Rinde - Murivel
 *
 * autor Cairell
 * opisy Tabakista
 * data   2008
 */

#include "dir.h"
inherit RUINY_STD_RUIN;

#include <stdproperties.h>
#include <macros.h>
#include <mudtime.h>

string dlugi_opis();
string opis_kamieni();
string opis_baszty();

void
create_ruiny()
{ 
    set_short("Po^sr^od gruz^ow.");
    set_long(&dlugi_opis());

    add_prop(ROOM_I_INSIDE,0);
    add_prop(ROOM_S_DARK_LONG, "Mroczne, nieregularne zarysy "+
        "jakich^s budynk^ow majacz^a niewyra^xnie na tle "+
        "o ton ja^sniejszego od nich nieba.\n");

    add_exit(RUINY_LOKACJE + "ruiny_4.c", "n");
    add_exit(RUINY_LOKACJE + "ruiny_3.c", "ne");
    add_exit(RUINY_LOKACJE + "ruiny_5.c", "e");

    add_sit(({"na ziemi"}), "na ziemi","z ziemi",5);
    add_sit(({"na wapiennym bloku"}), "na wapiennym bloku",
        "z wapiennego bloku",2);

    add_item(({"kamienie", "mur", "ruiny"}), &opis_kamieni());
    add_item(({"baszt^e", "wie^z^e"}), &opis_baszty());
    add_item(({"w^aw^oz"}), "Gardziel g^l^ebokiego parowu "+
        "otwiera si^e zaraz za resztkami muru. Strome, "+
        "niemal pionowe ^sciany rzucaj^a g^l^eboki cie^n "+
        "na jego dno, gdzie dostrzec mo^zna jedynie kilka "+
        "ja^sniejszych plam kawa^lk^ow gruzu, kt^ore "+
        "przez lata nie wytrzyma^ly w starciu z "+
        "mrozem i niepogod�.\n");

    set_event_time(400.0);
    add_event("^Zaden z cieni nawet nie drgnie zachowuj^ac "+
        "nienaturalny spok^oj.\n");
    add_event("Wymar^le miejsce wype^lnia jedynie cisza "+
        "przerywana gwizdami wiatru.\n");
    add_event("Kilka czarnych ptak^ow ko^luje ponad "+
        "blankami baszty.\n");
    add_event("Wiatr gwi^zd^ze cicho po^srod gruz^ow i "+
        "kruszej^acych powoli mur^ow.\n");
    add_event("Martwej ciszy nie m^aci najmniejsze nawet "+
        "poruszenie w^sr^od kamieni czy w ocala^lych "+
        "budynkach.\n");
    add_event("Wszechobecna cisza, a^z dzwoni w uszach.\n");
}

string dlugi_opis() 
{
    string str;

    str = "Fragmenty wal^acych si^e ^scian i naro^zniki "+
        "z trudem odr^o^znialne od zwyk^lych stert "+
        "gruzu wskazuj^a pobie^znie rozk^lad dawnych "+
        "pomieszcze^n. Mi^edzy rumowiskami przeb^lyskuje "+
        "czasami fragment posadzki, czy czerni� si^e "+
        "kamienie po resztkach kominka, lecz w ruinach "+
        "nie pozosta�o nic z przytulno�ci mieszka^n. ";

    if (jest_dzien() == 1)
    {
        str += "S^loneczne ^swiat�o kt^ore gdzie "+
            "indziej grza^loby przyjemnie, tutaj wydaje "+
            "si^e blade i pozbawione mocy, niezdolne "+
            "do wskrzeszenia ^zycia mi^edzy szarymi "+
            "kamieniami. ";
    }
    else
    {
        str += "Ksi^e^zycowe ^swiat�o zalewa je nadaj^ac "+
            "kamieniom niepokoj�cy po^lysk i mno^z^ac "+
            "dooko^la g^l^ebokie cienie karmi^ace "+
            "wszelkie l^eki jakie mo^ze mie^c zagubiony "+
            "tutaj w^edrowiec";
    }
     
    str += "Niedaleko na p^o^lnocy wznosi si^e masywna "+
        "baszta trwaj^aca  na stra^zy wal�cego si^e "+
        "zamku. Po przeciwnej za^s stronie mury obronne "+
        "stopniowo ko^ncz^a swoj^a s^lu^zb^e znikaj^ac "+
        "w gardzieli w^awozu nad kt^orym dot^ad trwa�y.\n";
    
    return str;
}

string opis_kamieni()
{
    string strr;

    strr = "Ca^le fragmenty muru nadal po^l^aczone zapraw^a "+
        "le^z^a woko^lo w stertach przemieszane z mniejszymi "+
        "kamieniami";

    if (pora_roku() == MT_ZIMA)
    {
        strr+=", zaspami �niegu";
    }
    
    strr+= " i blokami ociosanego wapienia. ";
    
    if (jest_dzien() == 1)
    {
        strr+="Mimo s^lonecznego ^swiat^la g^l^ebokie cienie "+
            "nadal skrywaj^a spor^a ich cz^e^s^c";
    }
    else
    {
        strr+="G��bokie cienie tworz^a si^e wsz^edzie gdzie "+
            "nie si^ega blada, ksi^e^zycowa po^swiata";
    }
    
    strr+=". Kilka wystrz^epionych fragment^ow muru nadal "+
        "stoi w miar^e prosto lecz wi^ekszo^s^c le^zy w "+
        "rumowiskach, lub spad^la na dno otwieraj^acego "+
        "si^eza nim w^awozu. Dalej na p^o^lnoc mur wydaje "+
        "si^e by^c w lepszym stanie. Nietrudno tak^ze "+
        "dostrzec tam kontur bramy.\n";

    return strr;
}

string opis_baszty()
{
    string st;

    st = "Masywny s^lup szarego kamienia wznosi si^e ku "+
        "niebu daj^ac jednocze^snie schronienie kr^a^z^acym ";

    if (jest_dzien() == 1)
    {
        st+="nad nim czarnym ptakom, kt�rych krakanie "+
            "burzy panuj^ac^a w^sr^od ruin cisz^e. ";
    }
    else
    {
        st+="woko�o niego nietoperzom, kt�rych cienie "+
            "przemykaj^a na tle nocnego nieba. ";
    }
    
    st+="^Sciany wie^zy nadal zachowa�y si� w niez^lym "+
        "stanie i nawet blanki nie s� zbyt mocno "+
        "wyszczerbione. U podstawy budowli wida^c, zdaje "+
        "si^e, wej^scie do niej, lecz otaczaj^ace ci^e "+
        "rumowiska oraz odleg�o^s^c utrudniaj� dok^ladniejsze "+
        "przyjrzenie si�.\n";

    return st;
}

public string
exits_description() 
{
    return "^Scie^zki mi^edzy gruzami pozwalaj^a uda^c "+
        "si^e na zach^od lub p^o^lnoc. Niewielkie "+
        "przej^scie prowadzi do wn^etrza budynku na "+
        "p^o^lnocnym-wschodzie.\n";
}
