/**
 * Groty niedaleko ruin na p^olnoc od traktu Rinde - Murivel
 *
 * autor Cairell
 * opisy Tabakista
 * data   2008
 */

#include "dir.h"
inherit RUINY_STD_JASKIN;
#include <stdproperties.h>
#include <macros.h>

string dlugi_opis();

void
create_jaskinie()
{
    set_short("Cz^e^s^c zamieszkanej jaskini.");
    set_long(&dlugi_opis());

    add_exit(RUINY_LOKACJE + "jaskinia_14.c", "w");
    add_exit(RUINY_LOKACJE + "jaskinia_16.c", "s");

    add_sit(({"na srodku przej^scia"}), "na ^srodku przejscia",
        "z przej^scia",3);
    add_sit(({"pod ^scian^a"}), "pod ^scian^a","z pod ^sciany",5);
    
    add_object(RUINY_OBIEKTY + "kaganek.c", 1);

    dodaj_rzecz_niewyswietlana("kaganek",1);
    dodaj_rzecz_niewyswietlana("zapalony kaganek",1);
    dodaj_rzecz_niewyswietlana("wypalony kaganek",1);
}

string dlugi_opis() 
{
    string str;

    str = "Niewielki zak^atek w rogu jaskini wype^lnia wyra^xnie wyczuwalny "+
        "zapach krwi. Jej krople zastyg^ly miejscami na wiechciach rzuconej "+
        "na ziemi^e s^lomy, a wi^eksze plamy barwi^a na karminowo "+
        "przykrywaj^ace je gar^sci trocin. Najwi^ecej jest jej pod ochlapan^a "+
        "ni^a ^scian^a, przy kt^orej le^z^a tak^ze strz^epki futra oraz "+
        "drobne, ogo^locone ko^sci. Nietrudno dostrzec w^sr^od nich nietypowe "+
        "dla popularnych w tych stronach zwierz^at czy ptak^ow. Niedaleko "+
        "jatki wykuta zosta^la w skale niewielka nisza, w kt^orej mie^sci "+
        "si^e do^s^c nietypowy kaganek.\n";

    return str;
}

public string
exits_description() 
{
    return "Korytarz ci^agnie si^e dalej na zach^od i po^ludnie.\n";
}
