/**
 * W^awoz na p^olnoc od traktu Rinde - Murivel
 *
 * autor Cairell
 *        - grupowe odsuwanie g�azu - Vera
 * opisy Tabakista
 * data   2009
 */

#include "dir.h"
inherit RUINY_STD_WAWOZU;
#include <stdproperties.h>
#include <ss_types.h>
#include <sit.h>
#include <macros.h>
#include <pogoda.h>

string dlugi_opis();
string opis_nawisu();
int jest_wyjscie = 0;
int czy_odsuniety_glaz = 0;

void
create_wawoz()
{ 

    set_short("Pod skalnym nawisem.");
    set_long(&dlugi_opis());

    add_prop(ROOM_S_DARK_LONG, "Wysokie ^sciany w^awozu"+
        "czyni^a mroki nocy jeszcze g^lebszymi i "+
        "trudniejszymi do przenikni^ecia wzrokiem. Ponad "+
        "twoj^a g^lowa wznosi si^e wieli, ciemny kszta^lt "+
        "spinaj^acy oba zbocza i przes^laniaj^acy niebo.\n");

    add_exit(RUINY_LOKACJE + "wawoz_8.c", "se");
    add_exit(RUINY_LOKACJE + "wawoz_6.c", "w");

    add_sit(({"na ziemi"}), "na ziemi","z ziemi",0);

    add_item(({"nawis", "nawis skalny", "skalny nawis"}), 
        &opis_nawisu());

    add_object(RUINY_OBIEKTY + "glaz.c", 1);

    dodaj_rzecz_niewyswietlana("wielki szary g^laz",1);
}

string dlugi_opis() 
{
    string str;

    str = "Wij^acy si^e w^aw^oz przebiega tutaj pod ogromnym skalnym nawisem ";
    if (jest_dzien() == 1)
    {
        str +="zacienaj^acym ";
    }
    else
    {
        str+="zatapiaj^acym w ca^lkowitym mroku ";
    }
    str+= "spor^a cz^e^s^c nier^ownego, uci^a^zliwego do marszu dna, nadal "+
        "pokrytego ob^lymi, rzecznymi kamieniami, cho^c po potoku nie "+
        "pozosta^l nawet ^slad dawnego koryta. Jeden z g^laz^ow, "+
        "szczeg^olnie wielki, wklinowany jest u samych st^op nawisu. Po obu "+
        "stronach wznosz^a si^e strome, wapienne ^sciany, gdzieniegdzie "+
        "poro^sni^ete ";
        
    if(pora_roku() == MT_ZIMA)
    {
        str+="rachitycznymi, nagimi o tej porze roku";
    }
    else
    {
        str+="pojedy^nczymi, rachitycznymi ";
    }
    str+="krzewinkami, a za to zwie^nczone ponad odleg^lymi "+
        "kraw^edziami koron^a sosnowych i ^swierkowych ga^l^ezi ko^lysz^acz^a "+
        "si^e majestatycznie ";
        
    if (jest_dzien() == 1)
    {
        str +="przes^laniaj^ac wi^ekszo^s^c i tak ledwie tu docieraj^acych "+
            "promieni s^lonecznych. ";
        
        if(CZY_JEST_SNIEG(TO))
        {
            str+="Nagie ska^ly wydaj^a si^e tym bardziej szare i ponure, "+
                "^ze kontrastuj^a z nawianym na dno w^awozu i "+
                "zalegaj^acym we wszelkich zag^l^ebieniach ^sniegiem.";
        }
        else
        {
            str+="Kontrastuj^ace z t^a zieleni^a szare ska^ly wydaj^a "+
                "si^e tym bardziej nagie i ponure.";
        }
    }
    else
    {
        str+="na tle granatowego, nocnego nieba. ";
    }

    str +="\n";
    return str;
}

string opis_nawisu()
{
    string st;

    st = "Ogromna ska^la pochyla si^e nad tob^a przes^laniaj^ac spor^a "+
        "cz^e^s^c nieba. ";
        
    if (jest_dzien() == 1)
    {
        st += "O ile monumentalne rozmiary sprawiaj^a i^z wydaje si^e "+
            "niemal wieczna, na jej czole bielej^a w s^lo^ncu nagie, "+
            "ostre kraw^edzie, od kt^orych niedawno oderwa^c si^e musia^l "+
            "spory kawa^l ska^ly.";
    }
    else
    {
        st += "Monumentalne rozmiary starej ska^ly sprawia^a wra^zene, ^ze "+
            "jest niemal wiecznym stra^znikiem tego przej^scia "+
            "sk^laniaj^acym si^e by lepiej dostrzec w^edrowc^ow "+
            "zapuszczaj^acych si^e tutaj pod os^lon^a nocy";
    } 

    st+="\n";
    return st;
}

void
dodaj_wyjscie()
{
    jest_wyjscie = 1;
    add_exit(RUINY_LOKACJE + "wawoz_7.c", ({"jaskinia","do jaskini",
						  "z zewn^atrz"}),0,9);
}
void
usun_wyjscie()
{
    jest_wyjscie = 0;
    remove_exit("jaskinia");

    //remove_prop(ROOM_I_LIGHT);

}

public string
exits_description() 
{
    if (czy_odsuniety_glaz == 1)
    {
        return "W^aw^az biegnie prosto na zach^od"+
            " i po^ludniowy wsch^od, za^s w ^scianie w^awozu "+
            "dostrzec mo^zna jaskini^e.\n";       
    }
    else
    {
        return "W^aw^oz biegnie prosto na zach^od "+
            "i po^ludniowy-wsch^od.\n";
    }
}
