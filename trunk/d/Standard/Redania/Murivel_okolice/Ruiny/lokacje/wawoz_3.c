/**
 * W^awoz na p^olnoc od traktu Rinde - Murivel
 *
 * autor Cairell, vera
 * opisy Tabakista
 * data   2008
 */

#include "dir.h"
inherit RUINY_STD_WAWOZU;
#include <pogoda.h>
#include <stdproperties.h>
#include <macros.h>
#include <mudtime.h>

string dlugi_opis();
string opis_ruin();
string opis_krzewu();
void wejdz(string str);
int zerwij(string str);

int czarny_bez_kwiaty = 0;
int czarny_bez_lisc = 0;
int czarny_bez_owoce = 0;

void
create_wawoz()
{
    set_short("Dno parowu.");
    set_long(&dlugi_opis());

    add_exit(RUINY_LOKACJE + "wawoz_2.c", "nw");
    add_exit(RUINY_LOKACJE + "wawoz_4.c", "e");
    add_exit(RUINY_LOKACJE + "grota_1.c", "otwor");
    add_exit(RUINY_LOKACJE + "grota_1.c", "grota");

    add_sit(({"na g^lazie"}), "na g^lazie","z g^lazu",3);
    
    set_alarm_every_hour("dodaj_ziola");

    add_item(({"ruiny"}), &opis_ruin());
    add_item(({"krzew", "krzak", "krzew bzu", "krzak bzu"}), &opis_krzewu());
    add_item(({"g^lazy", "g^laz", "resztki", "bloki",
        "resztki mur^ow"}), "Kilka bezkszta^ltnych, szarych "+
        "bry^l kamienia nadal nosi ^slady obr^obki i "+
        "zaprawy. W jednym z nich pozosta^l nawet w^aski "+
        "otw^or strzelniczy. Le^z^a one pod p^o^lnocn^a "+
        "skarp^a, tam gdzie uderzy^ly o dno po upadku z "+
        "widocznych w g^orze mur^ow. Kamienie otaczaj^a "+
        "ju^z paprocie oraz porastaj^a mchy nie pozwalaj^ac "+
        "stwierdzi^c czy oderwa^ly si^e pod wp^lywem "+
        "czasu, burzy, czy skruszone r^ek^ cz^lowieka.\n");
    add_item(({"otw^or", "grot^e"}), "Mroczna czelu^s^c "+
        "groty otwiera si^e zaraz za krzewem, tak blisko "+
        "^ze kilka p^ed^ow, ju^z suchych, nieopatrznie "+
        "zapu^sci�o si^e w tamtym kierunku. Zimny powiew "+
        "z wn^etrza powoli ko^lysze nimi w jednostajnym "+
        "rytmie. Szare, kanciaste kamienie pod^lo^za, "+
        "identyczne jak wsz^edzie dooko^la, nie "+
        "zdradzaj^a szansy napotkania jakiegokolwiek "+
        "mieszka^nca, lub mieszka^nc�w pieczary.\n");
}

int
dodaj_ziola()
{
    if(pora_roku() == MT_ZIMA)
        return 1;
    else
    {
	    switch(random(7))
	    {
		    case 0: czarny_bez_kwiaty =  random(5); break;
		    case 1..2: czarny_bez_lisc = random(10); break;
		    case 3: czarny_bez_owoce =   random(5); break;
		    default: break;
	    }
	    return 1;
    }
}

init()
{
    ::init();
    add_action(wejdz, "wejd�");
	add_action(zerwij,"zerwij");
}

int
zerwij(string str)
{
	notify_fail("Co chcesz zerwa^c?\n");
	
	if(str ~= "owoc" || str~="owoc bzu" || str~="owoc czarnego bzu")
	{
		if(czarny_bez_owoce)
		{
			clone_object(CZARNYBEZ + "czarny_bez-owoce.c")->move(TP);
			czarny_bez_owoce--;
			write("Zrywasz troch^e owoc^ow z krzewu.\n");
			saybb(QCIMIE(TP,PL_MIA)+" zrywa jakie^s owoce z krzewu.\n");
			
			return 1;
		}
		else
		{
			write("Na krzaku nie ma ^zadnych owoc^ow.\n");
			return 1;
		}
	}
	if(str ~= "li^s^c" || str~="li^s^c bzu" || str~="li^s^c czarnego bzu")
	{
		if(czarny_bez_lisc)
		{
			clone_object(CZARNYBEZ + "czarny_bez-lisc.c")->move(TP);
			czarny_bez_lisc--;
			write("Zrywasz li^s^c z krzewu.\n");
			saybb(QCIMIE(TP,PL_MIA)+" zrywa li^s^c z krzewu.\n");
			
			return 1;
		}
		else
		{
			write("Na krzaku nie ma ^zadnych dorodnych li^sci.\n");
			return 1;
		}
	}
	if(str ~= "kwiat" || str~="kwiat bzu" || str~="kwiat czarnego bzu")
	{
		if(czarny_bez_kwiaty)
		{
			clone_object(CZARNYBEZ + "czarny_bez-kwiaty.c")->move(TP);
			czarny_bez_kwiaty--;
			write("Zrywasz troch^e kwiat^ow z krzewu.\n");
			saybb(QCIMIE(TP,PL_MIA)+" zrywa troch^e kwiat^ow z krzewu.\n");
			
			return 1;
		}
		else
		{
			write("Na krzaku nie ma ^zadnych kwiat^ow.\n");
			return 1;
		}
	}
	return 0;
}

string dlugi_opis() 
{
    string str;

    str = "Prosty dalej w stron^e mostu w^aw^oz zaczyna "+
        "przechodzi^c tutaj w szeroki ^luk, obiegaj^ac "+
        "po^lo^zone nad jego skrajem ruiny. Kilka "+
        "wi^ekszych blok^ow, kt^ore odpad^ly od "+
        "murszej^acych mur^ow zalega teraz dno parowu. ";
    
    if(CZY_JEST_SNIEG(TO))
    {
        str += "Widoczne s^a jako niewielkie pag^rki, "+
            "czy wi^eksze zaspy pod pokrywaj^ac^a je "+
            "pierzyn^a ^sniegu. ";
    }
    else
    {
        str += "Mimo szczeg^olnie bujnych w tym miejscu "+
            "paproci niemal nie spos^ob ich nie dostrzec. ";
    }

    
    str += "Same zbocza stanowi^a niemal go^l^a, wapienn^a "+
        "ska^l^e, jedynie przy ziemi przypr^oszon^a "+
        "mchami, pe^ln^a p^lytkich nisz i za^lom^ow. "+
        "W jednym z nich wyr^os^l bujny krzew bzu "+
        "niecodziennie dobrze sobie radz^acy w "+
        "skalistym gruncie. Pod^lo^ze bowiem w wi^ekszej "+
        "cz^e^sci stanowi^a go^le kamienie, ";

    if (CZY_JEST_SNIEG(TO))
    {
        str += "widoczne tam gdzie wiatr wymi^ot^l "+
            "^snieg.\n";
    }
    else
    {
        str += "z kt^orych wiele jest ob^lych, jakby "+
            "wyj^etych ze strumienia.\n";
    }
    return str;
}

void wejdz(string str)
{
    if(!str)
    write("Gdzie chcesz wej^s^c?\n");
    if(str ~= "do otworu" || str ~="w otw^or" || str ~="do groty")
    {
        this_player()->command("grota");
    }
}
    
string opis_ruin()
{
    string strr;

    strr = "Widoczna w oddali konstrukcja, niegdy^s "+
        "^l^acz^aca zbocza parowu, obecnie spoczywa "+
        "na jego dnie skryta niemal zupe�nie ";
    
    if (pora_roku() == MT_ZIMA)
    {
        strr += "pod grub^a warstw^a ^sniegu, kt^orego "+
            "wiatr nie by^l w stanie przenie^s^c przez "+
            "t^e barier^e.";
    }
    else
    {
        strr += "w g^estej ro^slinno^sci wybuja^lej "+
            "dzi^eki wsz^edobylskiej wilgoci.";
    }

    if (jest_dzien() == 1)
    {
        strr += " Dopiero teraz, z pewnej odleg^lo^sci, "+
            "^latwiej dostrzec mo^zna usypany od wschodu "+
            "podjazd zabezpieczony z boku niskim "+
            "kamiennym murkiem, ca^lkiem nie^xle "+
            "znosz^acym pr^ob^e czasu.\n";
    }
    else
    {
        strr += "\n";
    }
    return strr;
}


string opis_krzewu()
{
    string stt;
    
    stt = "Bujny krzew dzikiego bzu wyrasta z "+
        "zag^l^ebienia w po^ludniowym zboczu "+
        "w^awozu. Szeroko rozpostarte ga^l^ezie ";
    
    if (pora_roku() == MT_WIOSNA)
    {
        stt += "ca^le obsypane ^swie^zymi p^akami ";
    }
    else if (pora_roku() == MT_LATO)
    {
        stt += "ca^le pokryte ciemnozielonymi li^s^cmi ";
    }
    else if (pora_roku() == MT_JESIEN)
    {
        stt += "pokryte ^z^o^lkn^acymi li^s^cmi ";
    }
    else
    {
        stt += "przypr^oszone jedynie ^sniegiem ";
    }
    
    stt += "nie s^a w stanie dok^ladnie zamaskowa^c "+
        "niewielkiego, czarnego otworu w wapiennej "+
        "skale.\n";
    return stt;
}

public string
exits_description() 
{
    return "W^aw^oz skr^eca ^lagodnie biegn^ac na "+
        "wsch^od i p^o^lnocny-zach^od.\n";
}
