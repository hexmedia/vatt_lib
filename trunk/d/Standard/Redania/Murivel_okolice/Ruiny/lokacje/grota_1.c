/**
 * Groty niedaleko ruin na p^olnoc od traktu Rinde - Murivel
 *
 * autor Cairell
 * opisy Tabakista
 * data   2008
 */

#include "dir.h"
inherit RUINY_STD_JASKIN;
#include <stdproperties.h>
#include <macros.h>

void
create_jaskinie()
{
    set_short("W grocie.");
    set_long(&dlugi_opis());

    add_exit(RUINY_LOKACJE + "wawoz_3.c", "wyj^scie");
    add_exit(RUINY_LOKACJE + "grota_2.c", "s");

    add_item(({"otw^or"}), "S^laby blask wpada przez w^ask^a "+
        "szczelin^e o^swietlaj^ac skalne pod^lo^ze nie dalej "+
        "ni^z na s^a^ze^n. ^Swiat^lo w wi^ekszo^sci "+
        "zas^laniane jest przez rosn^ace zaraz za wej^sciem "+
        "krzewy dzikiego bzu kt^orych kilka suchych "+
        "ga^l^azek si^ega nawet do wn^etrza groty.\n");
}

string dlugi_opis() 
{
    object *inwentarz;
    inwentarz = all_inventory();

    string str;

    str = "^Sciany wyp^lukanej w wapiennej skale jaskini "+
        "pe^lne s^a ^lagodnych, nieregularnych nisz, zag^l^ebie^n "+
        "i naciek^ow. Mo^zna si^e tutaj swobodnie wyprostowa^c, "+
        "gdy^z wysoko^s^c nier^ownego stropu niemal ca^ly czas "+
        "waha si^e w granicach zasi^egu ramion doros^lego "+
        "m^e^zczyzny. Jedynie co kilka krok^ow zwisaj^a z "+
        "sufitu d^lugie stalaktyty, niekt^ore si^egaj^ace a^z "+
        "do wyrastających z pod^lo^za stalagmit^ow, ^l^acz^a "+
        "si^e z nimi by utworzy^c swoiste kolumny. Pomi^edzy "+
        "nimi wyra^xnie wida^c drobne od^lamki drewna, czy "+
        "inne butwiejące resztki. Kawa^lek dalej "+
        "zagradzaj^acy drog^e naciek zosta^l brutalnie skuty, "+
        "a w gro^z^ac^a potkni^eciem szczelin^e kilka krok^ow "+
        "za nim wepchni^eto okr^ag^ly klocek drewna.\n";

    return str;
}

public string
exits_description() 
{
    return "Wyj^scie z groty prowadzi przez w^ask^a szczelin^e. "+
        "Korytarz ci^agnie si^e dalej na po^ludnie.\n";
}
