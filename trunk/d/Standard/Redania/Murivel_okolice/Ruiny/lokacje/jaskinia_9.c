/**
 * Groty niedaleko ruin na p^olnoc od traktu Rinde - Murivel
 *
 * autor Cairell
 * opisy Tabakista
 * data   2008
 */

#include "dir.h"
inherit RUINY_STD_JASKIN;
#include <stdproperties.h>
#include <macros.h>
inherit "/lib/drink_water";

string dlugi_opis();

void
create_jaskinie()
{
    set_short("Cz^e^s^c du^zej groty.");
    set_long(&dlugi_opis());

    add_exit(RUINY_LOKACJE + "jaskinia_8.c", "n");
    add_exit(RUINY_LOKACJE + "jaskinia_11.c", "w");
    add_exit(RUINY_LOKACJE + "jaskinia_10.c", "nw");

    set_drink_places(({"z sadzawki", "z niecki"}));
    
    add_npc(RUINY_LIVINGI + "barbegazi.c");
    add_npc(RUINY_LIVINGI + "barbegazi.c");
    add_npc(RUINY_LIVINGI + "barbegazi.c");
    if(random(1))
    {
        add_npc(RUINY_LIVINGI + "barbegazi.c");
    }
}

string dlugi_opis() 
{
    string str;

    str = "Wapienna ska^la po kt^orej kroczysz jest w tym miejscu wyj^atkowo "+
        "nieprzychylna w^edrowcom. Jest tu nier^owna powierzchnia, liczne "+
        "uskoki i skryte za stalaktytami niecki pe^lne wody, sprawiaj^acej, "+
        "^ze kamie^n staje si^e jeszcze bardziej ^sliski i zdradliwy. W "+
        "jednej, szczeg^olnie du^zej sadzawce zebralo si^e dobrych kilka "+
        "wiader wody. Strop ponad ni^a, jak i wsz^edzie dooko^la tak^ze "+
        "niesie niespodzianki. Nacieki i wystaj^ace na wysoko^sci twarzy "+
        "ska^ly tylko czekaj^a by kto^s rozbi^l sobie o nie g^low^e.\n";

    return str;
}

public string
exits_description() 
{
    return "Na zachodzie, p^o^lnocy i p^o^lnocnym-zachodzie rozci^aga si^e "+
        "dalsza cz^e^c groty.\n";
}
