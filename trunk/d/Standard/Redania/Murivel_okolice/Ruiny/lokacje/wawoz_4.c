/**
 * W^awoz na p^olnoc od traktu Rinde - Murivel
 *
 * autor Cairell
 * opisy Tabakista
 * data   2008
 */

#include "dir.h"
inherit RUINY_STD_WAWOZU;
#include <pogoda.h>
#include <stdproperties.h>
#include <macros.h>

string dlugi_opis();
string opis_ruin();

void
create_wawoz()
{
    set_short("Na dnie w^awozu.");
    set_long(&dlugi_opis());

    add_exit(RUINY_LOKACJE + "wawoz_3.c", "w");
    add_exit(RUINY_LOKACJE + "wawoz_5.c", "se");

    add_item(({"ruiny"}), &opis_ruin());

    add_item(({"g^lazy", "resztki mur^ow"}), "Kilka "+
        "bezkszta^ltnych, szarych bry^l kamienia nadal nosi "+
        "^slady obr^obki i zaprawy. W jednym z nich "+
        "pozosta^l nawet w^aski otw^or strzelniczy. Le^z^a "+
        "one pod p^o^lnocn^a skarp^a, tam gdzie uderzy^ly o "+
        "dno po upadku z widocznych w g^orze mur^ow. "+
        "Kamienie otaczane s^a ju^z przez paprocie oraz "+
        "porastane przez mchy nie pozwalaj^ace stwierdzi^c "+
        "czy oderwa^ly si^e pod wp^lywem czasu, burzy, "+
        "czy skruszone r^ek^a ludzk^a.\n");
}

string dlugi_opis() 
{
    string str;

    str = "Na tym odcinku drog^e zaczynaj^a coraz bardziej "+
        "utrudnia^c spore g^lazy, po^sr^od kt^orych "+
        "dzi^eki pozosta�o^sciom zaprawy i regularnym "+
        "kszta�tom nietrudno odr^o^zni^c fragmenty mur^ow "+
        "kt^orych cz^e^s^c nadal pochyla si^e wysoko "+
        "ponad stromymi ^scianami w^awozu, okalaj^ac "+
        "widoczne tam ruiny. Pomi^edzy g^lazami ";
    
    if(CZY_JEST_SNIEG(TO))
    {
        str += "zalegaj^a ^snie^zne zaspy dodatkowo "+
            "utrudniaj^ace marsz. ";
    }
    else
    {
        str += "g^esto pleni^a si^e k^epy krzew^ow, "+
            "g^l^ownie paproci. ";
    }

    
    str += "Ponad zas^lanym drobnymi kamieniami "+
        "dnem wyrastaj^a ";

    if (jest_dzien() == 1)
    {
        str += "strome zbocza niemal zamykaj^ace si^e "+
            "nad tob^a, tak ^ze niemal nie wida^c nieba, "+
            "a s^lo^nce nie ma szans zagl^ada^c tutaj "+
            "cz^e^sciej.\n";
    }
    else
    {
        str += "mroczne, strome zbocza niemal zamykaj^ace "+
            "si^e nad tob^a, tak ^ze niemal nie wida^c "+
            "nocnego nieba, a okolica zatopiona jest w "+
            "trudnym do przenikni�cia p^o^lmroku.\n";
    }
    return str;
}

string opis_ruin()
{
    string strr;

    strr = "Wysoko w g^orze, ponad kraw^edzi^a w^awozu ";
    
    if (jest_dzien() == 1)
    {
        strr += "chyl^a si^e niepewnie resztki mur^ow "+
            "obronnych otaczaj^ace dalsze, "+
            "nierozpoznawalne st^ad ruiny. ";
    }
    else
    {
        strr += "czarny, poszarpany kontur ruin daje "+
            "si^e bez trudu odr^o^zni^c od o kilka "+
            "ton^ow ja^sniejszego nieba. ";
    }


    if (jest_dzien() == 1)
    {
        if(CZY_JEST_SNIEG(TO))
        {
            strr += "^Snie^zne czapy nieco "+
                "wyg^ladzaj^a z^ebate kraw^edzie";
        }
        else
        {
            strr += "O ile mo�na dostrzec, w^sr^od "+
                "ruin nie ma ^zadnej ro^slinno^sci, "+
                "m^lodych drzewek, czy cho^cby "+
                "chwast^ow";
        }
        strr += ", a jedynie kilka czarnych ptak^ow "+
            "bez przerwy kr^a^zy wysoko ponad nimi.";
    }
    strr += "\n";
    return strr;
}

public string
exits_description() 
{
    return "W^aw^oz biegnie dalej na zach^od "+
        "i po^ludniowy-wsch^od.\n";
}
