/**
 * Groty niedaleko ruin na p^olnoc od traktu Rinde - Murivel
 *
 * autor Cairell
 * opisy Tabakista
 * data   2008
 */

#include "dir.h"
inherit RUINY_STD_JASKIN;
#include <stdproperties.h>
#include <macros.h>

string dlugi_opis();

void
create_jaskinie()
{
    set_short("Za rozpadlin^a");
    set_long(&dlugi_opis());

    add_exit(RUINY_LOKACJE + "jaskinia_14.c", "ne");

    add_object(RUINY_OBIEKTY + "rozpadlina13.c", 1);

    dodaj_rzecz_niewyswietlana("rozpadlina",1);
    
    add_item(({"wi^ory", "wi^ory drewna", "^xd^xb^la", "^xd^xb^la s^lomy"}), 
        "Gdzieniegdzie, t w jakim^s zag^l^ebieniu, to zn^ow p[od ^scian^a "+
        "zauwa^zy^c mo^zna drobne wi^ory, oraz pojedyncze ^x^d^xb^la lekko "+
        "zbutwia^lej ju^z s^lomy. Poniewieraj^a si^e jak przed wiejskim "+
        "obej^sciem naniesione butami gospodarza i kopytami jego przych^owku. "+
        "Tyle, ^ze nie jeste^s na wsi, tylko w ^srodku jaskini, na zupe^lnym "+
        "odludziu.\n");
}

string dlugi_opis() 
{
    string str;

    str = "Korytarz na po^ludniu urywa si^e nagle gdy spora rozpadlina "+
        "przecina skalne pod^lo^ze po kt^orym st^apasz. Po przeciwleg^lej "+
        "stronie rozpo^sciera si^e ca^lkiem spora pieczara. Tu gdzie stoisz "+
        "jest wprost przeciwnie, korytarz kaza^lby si^e do^s^c mocno "+
        "przygarbi^c ka^zdemu ^sredniego wzrostu m^e^zczy^xnie. Rzuca si^e "+
        "za to w oczy nietypowy w jaskiniach widok, gdzieniegdzie le^z^a "+
        "pojedy^ncze wi^ory drewna oraz ^xd^xb^la s^lomy.\n";

    return str;
}

public string
exits_description() 
{
    return "Korytarz ci^agnie si^e dalej na p^o^lnocny-wsch^od.\n";
}
