/**
 * Groty niedaleko ruin na p^olnoc od traktu Rinde - Murivel
 *
 * autor Cairell
 * opisy Tabakista
 * data   2008
 */

#include "dir.h"
inherit RUINY_STD_JASKIN;
#include <stdproperties.h>
#include <macros.h>

string dlugi_opis();

void
create_jaskinie()
{
    set_short("W ciasnej jaskini.");
    set_long(&dlugi_opis());

    add_exit(RUINY_LOKACJE + "jaskinia_2.c", "n");
    add_exit(RUINY_LOKACJE + "grota_3.c", "se");

}

string dlugi_opis() 
{
    string str;

    str = "Lekki ^luk korytarza jaskini nie ko^nczy si^e przynajmniej "+
        "tak daleko, jak wzrok daje rad^e przedrze^c si^e przez wszechobecny, "+
        "g^l^eboki mrok. Tonie w nim tak^ze strop i wiele zak^atk^ow w "+
        "pokrytych niewielkimi zaciekami, wapiennych ^scianach. Sp^lywaj^a "+
        "po nich drobne, s^lone krople, pokonuj^ace labirynt p^ekni^e^c i "+
        "wyst^ep^ow by wreszcie dotrze^c do kt^orej^s z ka^lu^z widocznych "+
        "to tu, to tam na dnie groty. Sporadyczne podmuchy kr^a^z^acego "+
        "tunelami powietrza przynosz^a nik^l^a wo^n pi^zma.\n";

    return str;
}

public string
exits_description() 
{
    return "Korytarz ci^agnie si^e dalej na po^ludniowy-wsch^od i p^o^lnoc.\n";
}
