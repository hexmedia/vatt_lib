/**
 * W^awoz na p^olnoc od traktu Rinde - Murivel
 *
 * autor Cairell
 * opisy Ania & Tabakista
 * data   2009
 */

#include "dir.h"
inherit RUINY_STD_WAWOZU;
#include <pogoda.h>
#include <stdproperties.h>
#include <macros.h>

string dlugi_opis();
string opis_ruin();

void
create_wawoz()
{ 

    set_short("W w^awozie.");
    set_long(&dlugi_opis());

    add_exit(RUINY_LOKACJE + "wawoz_4.c", "nw");
    add_exit(RUINY_LOKACJE + "wawoz_6.c", "ne");

    add_item(({"ruiny"}), &opis_ruin());
    add_item(({"g^lazy", "kamienie", "bloki"}), "Kilka "+
        "bezkszta^ltnych, szarych bry^l kamienia nadal nosi "+
        "^slady obr^obki i zaprawy. W jednym z nich "+
        "pozosta^l nawet w^aski otw^or strzelniczy. Le^z^a "+
        "one pod p^o^lnocn^a skarp^a, tam gdzie uderzy^ly o "+
        "dno po upadku z widocznych w g^orze mur^ow. "+
        "Kamienie otaczane s^a ju^z przez paprocie oraz "+
        "porastane przez mchy nie pozwalaj^ace stwierdzi^c "+
        "czy oderwa^ly si^e pod wp^lywem czasu, burzy, "+
        "czy skruszone r^ek^a ludzk^a.\n");
}

string dlugi_opis() 
{
    string str;

    str = "Wysoko ponad tob^a, nad skrajem pn^acych si^e niemal pionowo w "+
        "g^or^e zboczy w^awozu wznosz^a si^e mocno nadszarpni^ete z^ebem "+
        "czasu ruiny. �ciany w^awozu - nagie, wapienne ska^ly broni^a do nich "+
        "dost^epu nwet przed najwytrwalczymi i najsprawniejszymi ^smia^lkami, "+
        "lecz by obejrze^c stare mury, nie trzeba podejmowa^c karko^lomnej "+
        "wspinaczki. Na dnie jaru, po^sr^od ";
    
    if (pora_roku() == MT_ZIMA)
    {
        if (CZY_JEST_SNIEG(TO))
        {
            str += "liczny, nawianych z g^ory ^snie^znych zasp ";
        }
        else
        {
            str += "pokrytych szronem, ob^lych, rzecznych kamieni ";
        }
    }
    else
    {
        str += "porastaj^acych co bardziej zacienione miejsca k^ep paproci ";
    }
    
    str += "nietrudno dostrzec po^l^aczone zapraw^a bry^ly, kt^ore przez lata "+
        "sta^ly si^e sp^ekane i nieregularne tak, ^ze nie spos^ob dostrzec "+
        "^slady blanek, czy otwor^ow strzelniczych. Pomi^edzi nimi wije "+
        "si^we koryto starego potoku, wyschni^etego ju^z jednak zupe^lnie.";

    str +="\n";
    return str;
}

string opis_ruin()
{
    string strr;

    strr = "Wysoko w g^orze, ponad kraw^edzi^a w^awozu ";
    
    if (jest_dzien() == 1)
    {
        strr += "chyl^a si^e niepewnie resztki mur^ow "+
            "obronnych otaczaj^ace dalsze, "+
            "nierozpoznawalne st^ad ruiny. ";
    }
    else
    {
        strr += "czarny, poszarpany kontur ruin daje "+
            "si^e bez trudu odr^o^zni^c od o kilka "+
            "ton^ow ja^sniejszego nieba. ";
    }


    if (jest_dzien() == 1)
    {
        if(CZY_JEST_SNIEG(TO))
        {
            strr += "^Snie^zne czapy nieco "+
                "wyg^ladzaj^a z^ebate kraw^edzie";
        }
        else
        {
            strr += "O ile mo�na dostrzec, w^sr^od "+
                "ruin nie ma ^zadnej ro^slinno^sci, "+
                "m^lodych drzewek, czy cho^cby "+
                "chwast^ow";
        }
        strr += ", a jedynie kilka czarnych ptak^ow "+
            "bez przerwy kr^a^zy wysoko ponad nimi.";
    }
    strr += "\n";
    return strr;
}

public string
exits_description() 
{
    return "W^aw^oz biegnie dalej na p^o^lnocny-zach^od "+
        "i p^o^lnocny-wsch^od.\n";
}
