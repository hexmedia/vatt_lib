/**
 * W^awoz na p^olnoc od traktu Rinde - Murivel
 *
 * autor Cairell
 * opisy Tabakista
 * data   2009
 */
 
#include "dir.h"
inherit RUINY_STD_WAWOZU;
#include <stdproperties.h>
#include <macros.h>
#include <ss_types.h>
#include <sit.h>
#include <pogoda.h>
inherit "/lib/drink_water";

string dlugi_opis();
string opis_potoku();
string opis_groty();
string opis_wodospadu();
int wespnij(string str);

void
create_wawoz()
{
    set_short("Przy ma^lej kaskadzie.");
    set_long(&dlugi_opis());

    add_exit(RUINY_LOKACJE + "wawoz_11.c", "n");
    add_exit(RUINY_LOKACJE + "wawoz_9.c", "sw");

    set_drink_places(({"ze strumienia", "z potoku"}));

    add_item(({"potok", "strumie^n"}), &opis_potoku());
    add_item(({"grot^e", "jaskini^e", "otw^or"}), &opis_groty());
    add_item(({"wodospad", "kaskad^e"}), &opis_wodospadu());

    add_event("Niewielki strumie^n szemrze cicho.\n");
    add_event("Ponad wodospadem unosi si^e ledwie widoczna mgie^lka.\n");
}

public void
init()
{
    ::init();
    init_drink_water();
    add_action(wespnij,"wespnij");
    add_action(wespnij,"wejd^x");
}

public void
attempt_drink(string skad)
{
    if(TEMPERATURA(TO) < 0)
        TP->catch_msg("Strumie^n jest skuty lodem.\n");
    else if(CZY_JEST_SNIEG(TO))
        TP->catch_msg("Strumie^n skryty jest pod warstw^a ^sniegu, nie "+
            "spos^ob zaczerpn^a^c wody.\n");
    else
        ::attempt_drink(skad);
}

void
drink_effect(string skad)
{
    write("Kl^ekasz na skraju potoku i nabierasz wody w d^lonie, gasz^ac "+
        "ni^a pragnienie.\n");
    saybb(QCIMIE(this_player(), PL_MIA) + " kl^eka na skraju potoku i nabiera "+
        " wody w d^lonie, gasz^ac ni^a pragnienie.\n");
}

string dlugi_opis() 
{
    string str;

    str = "Miejsce to wype^lnia miarowy szum rozbijaj^acej si^e o ska^ly "+
        "wody. Niewysoki, bo spadaj^acy ledwie z kilkunastu st^op wodospad "+
        "sprawia, ze powietrze wype^lnione jest wilgoci^a. Bior^acy z "+
        "niego pocz^atek strumie^n w^edruje, klucz^ac chaotycznie na "+
        "p^o^lnoc, tam gdzie ";

    if(CZY_JEST_SNIEG(TO))
    {
        str += "o^snie^zone ";
    }
    else
    {
        str += "kamieniste ";
    }
    
    str += "dno w^awozu opada stopniowo. Wznosz^ace si^e po obu stronach "+
        "strome, wapienne ^zbocza sporadycznie jedynie porastane s^a przez "+
        "rachityczne krzewinki. Wprawdzie nie pozwalaj^a dostrzec nic ponad "+
        "nimi, lecz charakterystyczny, ^zywiczny zapach wskazuje, ^ze "+
        "ca^lkiem blisko musi znajdowa^c si^e iglasty las. ";
        
    if (jest_dzien() == 1)
    {
        str += "Ledwie tu si^egaj^ace s^loneczne promienie jedynie "+
            "cz^e^sciowo rozpraszaj^a p^o^lmrok tego za^scielonego "+
            "^sliskimi kamieniami, w^askiego parowu.";
    }
    else
    {
        str += "Mroki nocy spowijaj^a dno tego w^askiego parowu, "+
            "utrudniaj^ac w^edrowk^e po^sr^od ^sliskich, wilgotnych kamieni.";
    }

    str +="\n";
    return str;
}

string opis_potoku()
{
    string st;

    st = "Z wyp^lukanego przez niewielki wodospad zag^l^ebienia wyp^lywa "+
        "wartki potok weso^lo szemrz^acy w^sr^od ob^lych, rzecznych kamieni. "+
        "Krystaliczna, lodowato zimna woda w ^zadnym miejscu nie wydaje "+
        "si^e g^l^ebsza ni^z na dwa, czy trzy palce. Klucz^ac pomi^edzy "+
        "wi^ekszymi g^lazami, strumie^n niknie z oczu gdzie^s na p^o^lnocy. ";
        
    st+="\n";
    return st;
}

string opis_groty()
{
    string st;

    st = "W skale ponad tob^a, na wysoko^sci kilkunastu st^op, zieje "+
        "sporych rozmiar^ow grota z kt^orej tryska strumie^n. Ska^la "+
        "obok nie wydaje si^e a^z tak stroma jak wi^ekszo^s^c zboczy "+
        "i ^scian dooko^la.";
        
    st+="\n";
    return st;
}

string opis_wodospadu()
{
    string st;

    if (jest_dzien() == 1)
    {
        st = "Delikatna t^ecza tworzy si^e w^sr^od wisz^acych w powietrzu "+
            "drobin wody rozsianych ";
    }
    else
    {
        st = "W ch^lodnym, nocnym powietrzu wisz^a drobiny wody rozsiane ";
    }
    
    st += "przez niewysoki wodospad bior^acy pocz^atek z mrocznej groty "+
        "otwieraj^acej si^e w stoku w^awozu. W miejscu gdzie kaskada uderza "+
        "o ska^ly, wyp^luka^la si^e ju^z p^lytka niecka, z kt^orej "+
        "wy^plywa niewieki, wartki potok.";

    st+="\n";
    return st;
}

int wespnij(string str)
{
    object *kto;

    if(query_verb() == "wespnij")
        notify_fail("Gdzie chcesz si^e wspi^a^c?\n");
    else
        notify_fail("Gdzie chcesz wej^s^c?\n");


    if(!strlen(str))
        return 0;

    if(!parse_command(str, environment(TP), "'si^e' 'do' 'groty'")
        && !parse_command(str, environment(TP), "'do' 'groty'"))
        return 0;

    if(!HAS_FREE_HANDS(TP))
    {
       write("Musisz mie^c obie d^lonie wolne, aby m^oc to zrobi^c.\n");
       return 1;
    }

    if (!str) 
    return 0;

    if(TP->query_prop(SIT_LEZACY) || TP->query_prop(SIT_SIEDZACY))
    {
        write("B^edzie ^latwiej je^sli najpierw wstaniesz.\n");
        return 1;
    }

    if(TP->query_old_fatigue() <= 35)
    {
        write("Nie masz ju� na to si^ly.\n");
        return 1;
    }

    write("Szukasz dogodnego uchwytu oraz oparcia dla st^op, "+
        "po czym i rozpoczynasz wspinaczk^e.\n");
    saybb(QCIMIE(TP, PL_MIA) + " szuka dogodnego uchwytu oraz oparcia "+
        "dla st^op po czym rozpoczyna wspinaczk^e po ska^lach.\n");

    if(environment(TO)->pora_roku() == MT_ZIMA &&
        TP->query_skill(SS_CLIMB) <= 30)
    {
        clone_object(RUINY_OBIEKTY+"paraliz_wspinania_grota_nie")
            ->move(TP);
        return 1;
    }
            
    if(environment(TO)->pora_roku() != MT_ZIMA &&
       TP->query_skill(SS_CLIMB) <= 20)
    {
        clone_object(RUINY_OBIEKTY+"paraliz_wspinania_grota_nie")
            ->move(TP);
        return 1;
    }
        
    if(environment(TO)->pora_roku() == MT_ZIMA &&
        TP->query_skill(SS_CLIMB) >= 31 &&
        TP->query_skill(SS_CLIMB) <= 40)
    {
        clone_object(RUINY_OBIEKTY+"paraliz_wspinania_grota_prawie")
            ->move(TP);
        return 1;
    }
            
    if(environment(TO)->pora_roku() != MT_ZIMA &&
        TP->query_skill(SS_CLIMB) >= 21 &&
        TP->query_skill(SS_CLIMB) <= 35)
    {
        clone_object(RUINY_OBIEKTY+"paraliz_wspinania_grota_prawie")
            ->move(TP);
        return 1;
    }

    clone_object(RUINY_OBIEKTY+"paraliz_wspinania_grota")->move(TP);
    return 1;
}

public string
exits_description() 
{
    return "W^aw^oz biegnie prosto na p^o^lnoc "+
        "i po^ludniowy-zach^od.\n";
}
