/**
 * W^awoz na p^olnoc od traktu Rinde - Murivel
 *
 * autor Cairell
 * opisy Tabakista
 * data   2008
 */

#include "dir.h"
inherit RUINY_STD_WAWOZU;
#include <pogoda.h>
#include <stdproperties.h>
#include <macros.h>

string dlugi_opis();
string opis_mostu();

void
create_wawoz()
{
    set_short("Poni�ej zniszczonego mostu.");
    set_long(&dlugi_opis());

    add_exit(RUINY_LOKACJE + "wawoz_2.c", "se");

    add_object(RUINY_OBIEKTY + "most.c", 1);

    dodaj_rzecz_niewyswietlana("most",1);

    add_item(({"belki", "stert^e", "sterte belek"}), &opis_mostu());
}

public int unq_no_move(string str)
{
    notify_fail("Ruszasz przed siebie, lecz wysokie ^sciany w^awozu "+
        "okazuj^a si^e zbyt strome.\n");
        
        string vrb = query_verb();

    if(vrb ~= "g^ora" || vrb ~= "d^o^l")
        notify_fail("Nie mo^zesz tam si^e uda^c!\n");
    if(vrb ~= "n" || vrb ~= "p^o^lnoc" || vrb ~= "north")
        notify_fail("Resztki zawalonego mostu ca^lkowicie tarasuj^a drog^e "+
            "na p^o^lnoc.\n");

    return 0;
}

string dlugi_opis() 
{
    string str;

    str = "Strome zbocza w^awozu zbli^zaj^ace si^e w tym "+
        "miejscu do siebie spina^l dawniej drewniany most, "+
        "obecnie przemieniony przez czas i niepogod^e w "+
        "malownicz^a stert^e belek zalegaj^acych dno i "+
        "ca^lkowicie odcinaj^acych drog^e na p^o^lnoc. ";
    
    if (jest_dzien() == 1)
    {
        str += "Rumowisko, wysokie na ponad pi^e^c st^op "+
            "sk^lada niem^a obietnic^e po^lamanych n^og "+
            "ka^zdemu kto zechcia^lby si^e zmierzy^c z "+
            "stercz^acymi we wszystkie strony kikutami "+
            "belek, spr^ochnia^lymi, ^sliskimi od mch^ow "+
            "dr^agami oraz licznymi, w wi^ekszo�ci skrytymi ";

        if(CZY_JEST_SNIEG(TO))
        {
            str += "pod ^sniegiem ";
        }
        else
        {
            str += "w^sr^od pokrzyw ";
        }

        str += "dziurami i za�omami. ";
    }
    else
    {
        str += "Sk^apane w mroku rumowisko je^zy si^e od "+
            "kikut^ow belek, nawet noc^a l^sni^acych od "+
            "wilgoci, za^s ka^zdy przesmyk mi^edzy nimi ";

        if (CZY_JEST_SNIEG(TO))
        {
            str += "za^sciela kryj^aca zdradliwe "+
                "dziury warstwa ^sniegu. ";
        }
        else
        {
            str += "przerastaj^a kryj�ce zdradliwe "+
                "dziury k^epy pokrzyw i paproci. ";
        }
    }

    
    str += "Droga na po^ludniowy zach^od, mimo i^z "+
        "bez tak spektakularnych przeszk^od, tak^ze "+
        "nie nale^zy do naj^latwiejszych, gdy^z "+
        "nier^owne, wapienne kamienie zalegaj^ace dno ";

    if (pora_roku() == MT_ZIMA)
    {
        str += "w wielu miejscach pokrywa l^od. ";
    }
    else
    {
        str += "s^a r^ownie wilgotne i ^sliskie. ";
    }

    if (jest_dzien() == 1)
    {
        str += "Dok^ad si^ega wzrok ";
    }
    else
    {
        str += "Czarniej^ace na tle nocnego nieba, ";
    }
    
    str += "wysokie zbocza staj^a si^e bardziej "+
        "strome, cho^c oddalaj^a si^e od siebie "+
        "czyni^ac drog^e szersz^a i "+
        "przestronniejsz^a. Jedynie przy wi^ekszych "+
        "g^lazach i w za^lomach skalnych ";
    
    if (pora_roku() == MT_ZIMA)
    {
        if(CZY_JEST_SNIEG(TO))
        {
            str += "zebra^ly si^e g^l^ebokie zaspy.\n";
        }
        else
        {
            str += "stercz^a suche ^lodygi paproci.\n";
        }
    }
    else
    {
        str += "wyrastaj^a bujne k^epy paproci.\n";
    }

    return str;
}

string opis_mostu()
{
    string strr;

    strr = "Sterta grubych, przegni^lych bali sm^etnie si^ega "+
        "w g^or^e licznymi, murszej^acymi u^lomkami pomi^edzy "+
        "kt^orymi ";
    
    if(CZY_JEST_SNIEG(TO))
    {
        strr += "roz^sciela si� gruba warstwa ^sniegu";
    }
    else
    {
        strr += "z szpar i dziur wyrastaj^a dziesi^atki pokrzyw "+
            "i paproci";
    }

    strr += ". Kilka kr^otszych belek nadal osadzonych jest "+
        "w zboczu w^awozu nie podtrzymuj^ac ju^z jednak "+
        "niczego poza porastaj^acymi je g^esto mchami. "+
        "Wzd^lu^z ich linii niewyra^xna ^scie^zka biegnie "+
        "w g^or^e nikn^ac przy niskim kamiennym murku "+
        "flankuj^acym podjazd do mostu.\n";
    
    return strr;
}

public string
exits_description() 
{
    return "W^aw^oz biegnie na po^ludniowy-wsch^od.\n";
}
