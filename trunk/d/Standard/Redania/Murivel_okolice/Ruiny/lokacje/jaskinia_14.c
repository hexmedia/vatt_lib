/**
 * Groty niedaleko ruin na p^olnoc od traktu Rinde - Murivel
 *
 * autor Cairell
 * opisy Tabakista
 * data   2008
 */

#include "dir.h"
inherit RUINY_STD_JASKIN;
#include <stdproperties.h>
#include <macros.h>

string dlugi_opis();

void
create_jaskinie()
{
    set_short("W niskiej grocie.");
    set_long(&dlugi_opis());

    add_exit(RUINY_LOKACJE + "jaskinia_15.c", "e");
    add_exit(RUINY_LOKACJE + "jaskinia_16.c", "se");
    add_exit(RUINY_LOKACJE + "jaskinia_13.c", "sw");
    
    add_object(RUINY_OBIEKTY + "kaganek.c", 1);

    dodaj_rzecz_niewyswietlana("kaganek",1);
    dodaj_rzecz_niewyswietlana("zapalony kaganek",1);
    dodaj_rzecz_niewyswietlana("wypalony kaganek",1);
    
    add_npc(RUINY_LIVINGI + "pukaczM.c");
}


string dlugi_opis() 
{
    string str;

    str = "Grota, im dalej od wiod^acego na po^ludniowy zach^od korytarza, "+
        "tym bardziej rozszerza si^e i staje przestronniejsza. Wapienne "+
        "^sciany gdzieniegdzie nosz^a ^slady kilofa i d^luta, lecz tyle tylko "+
        "by nacieki i nawisy nie przecina^ly wn^etrza pieczary. Ponadto w "+
        "w niewielkiej niszy po lewej stronie dostrzec mo^zna co^s na "+
        "kszta^lt kaganka - jej dno zota^lo zalane woskiem z licznych na "+
        "wp^o^l wypalonych ogark^ow. Pod^lo^ze grubo wysypano trocinami i "+
        "star^a ju^z do^s^c s^lom^a.\n";

    return str;
}

public string
exits_description() 
{
    return "Korytarz ci^agnie si^e dalej na wsch^od, po^ludniowy-wsch^od "+
        "i po^ludniowy-zach^od.\n";
}
