/**
 * Groty niedaleko ruin na p^olnoc od traktu Rinde - Murivel
 *
 * autor Cairell
 * opisy Tabakista
 * data   2008
 */

#include "dir.h"
inherit RUINY_STD_JASKIN;
#include <stdproperties.h>
#include <macros.h>

string dlugi_opis();

void
create_jaskinie()
{
    set_short("Niska jaskinia.");
    set_long(&dlugi_opis());

    add_exit(RUINY_LOKACJE + "jaskinia_3.c", "ne");
    add_exit(RUINY_LOKACJE + "jaskinia_1.c", "s");
}

string dlugi_opis() 
{
    string str;

    str = "Porowaty, pokryty zaciekami strop zbli^za si^e w tym miejscu do "+
        "pokrytego plamami ka^lu^z pod^lo^za ka^z^ac si^e nieznacznie "+
        "zgarbi^c co wy^zszym m^e^zczyznom. Za to nier^owne ^sciany oddalaj^a "+
        "si^e od siebie, wprawdzie nie tworz^ac jeszce pieczary, lecz z "+
        "pewno^sci^a zapewniaj^ac wi^ecej miejsca. Tu i ^owdzie pokrywa j^a "+
        "je b^lyszcz^ace od s^lonej wody zacieki, lecz jakby mniej liczne "+
        "ni^z kawa^lek dalej. Wilgotne powietrze niesie ledwie wyczuwalny, "+
        "kojarz^acy si^e chyba tylko z pi^zmem zapach. \n";

    return str;
}


public string
exits_description() 
{
    return "Korytarz ci^agnie si^e dalej na po^ludnie i p^o^lnocny-wsch^od.\n";
}
