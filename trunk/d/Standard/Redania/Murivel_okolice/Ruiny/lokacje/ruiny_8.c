/**
 * Ruiny na p^olnoc od traktu Rinde - Murivel
 *
 * autor Cairell
 * opisy Tabakista
 * data   2008
 */

#include "dir.h"
inherit RUINY_STD_RUIN;

#include <stdproperties.h>
#include <macros.h>

string dlugi_opis();

void
create_ruiny()
{
    set_short("Niewielka krypta.");
    set_long(&dlugi_opis());

    add_prop(ROOM_I_INSIDE,1);
    remove_prop(ROOM_I_LIGHT);
    add_prop(ROOM_S_DARK_LONG, "W ciemno^sci majacz^a "+
        "zarysy jakich^s kszta^lt^ow, wylawianych "+
        "z mroku przez padaj^ac^a z g^ory po^swiate.\n");

    add_exit(RUINY_LOKACJE + "ruiny_1.c", "u");
    add_exit(RUINY_LOKACJE + "ruiny_7.c", "s");

    add_sit(({"na posadzce"}), "na posadzce","z posadzki",10);
    add_sit(({"pod ^scian^a"}), "pod ^scian^a","z pod ^sciany",5);

    add_object(RUINY_OBIEKTY + "sarkofag.c", 1);

    dodaj_rzecz_niewyswietlana("du^zy sarkofag",1);

    add_item(({"nisze"}), "Dwa rz^edy pod^lu^znych nisz biegn^a "+
        "wzd^lu^z ^scian krypty. W wi^ekszo�ci pozostaj^a one "+
        "puste, lecz do kilku wsuni^eto proste, kamienne "+
        "sarkofagi, lub drewniane trumny, po kt^orych jednak "+
        "pozosta^lo niewiele wi^ecej ni^z resztki pr^ochna "+
        "i szkielet^ow szkielety oraz pozosta^lo^sci drobnych "+
        "metalowych przedmiot^ow.\n");
    add_item(({"otw^or", "dzi^or^e"}), "Ca^lkiem spore przej^scie "+
        "wykute w wapiennej skale zosta�o dok^ladnie zamurowane, "+
        "jednak na samym ^srodku ponownie wybity zosta^l "+
        "szeroki na ponad ^lokie^c otw^or, mimo ^ze spogl^adaj�c "+
        "na jego kraw^edzie wida^c, �e do zagrodzenia tej "+
        "drogi u^zyto podw^ojnej warstwy cegie^l.\n");
    add_item(({"stopnie", "schody"}), "Cz^e^s^c w^askich, jakby "+
        "wci^sni^etych w r^og sali schod^ow wykuto wprost w "+
        "skale, zn^ow inne ich fragmenty powsta^ly z mniejszych "+
        "kamieni dok^ladnie do siebie dopasowanych i solidnie "+
        "po^l^aczonych zapraw^a. Jest to jedyny widoczny "+
        "^slad jakoby pomieszczenie to by^lo uprzednio "+
        "naturaln^a jaskini^a. Ze szczytu schod^ow sp^lywaj^a "+
        "promienie m^etnego, bladego ^swiat^la tworz^ac "+
        "g^l^ebokie cienie na ka^zdej wi^ekszej nier^owno^sci "+
        "^sciany, czy stopniu schod^ow. Na jednym z ostatnich "+
        "dostrzec mo^zna kilka brunatnych kropli z kt^orych "+
        "cz^e^s^c zastyga sp^lywaj^ac po pochy^lo^sci kamienia.\n");

    set_event_time(400.0);
    add_event("Przenikliwy ch^l^od bije od strony od strony "+
        "wybitego w ^scianie otworu.\n");
    add_event("Wszechobecna cisza, a^z dzwoni w uszach.\n");
    add_event("^Zaden z cieni nawet nie drgnie zachowuj^ac "+
        "nienaturalny spok^oj.\n");
    add_event("Wymar^le miejsce wype^lnia cisza przerywana "+
        "jedynie odleg^lymi gwizdami wiatru.\n");
}

string dlugi_opis() 
{
    object *inwentarz;
    inwentarz = all_inventory();
    string str;

    str = "Zimne, nagie ^sciany roj^a si^e od migotliwych cieni "+
        "zdaj^acych si^e wype^lza^c z niskich, pod^lu^znych "+
        "nisz kt^orych dwa rz^edy obiegaj^a krypt^e. Nie ma "+
        "ich tylko poni^zej w^askich, pn�cych si^e stromo "+
        "w g^or^e, w kierunku bladego ^swiat^la. Drugie i "+
        "ostatnie widoczne wyj^scie wiedzie przez wybit^a w "+
        "^scianie dziur^e za kt^or panuje tylko ciemno^s^c. "+
        "Bije stamt�d nienaturalny wr�cz ch^l^od.";

    if (present ("sarkofag", inwentarz) == 0)
    {
        str += " ^Srodek krypty niepodzielnie nale^zy do "+
            "wielkiego, kamiennego sarkofagu kt^orego wierzchnia "+
            "p^lyta ozdobiona jest p^laskorze^xb^a "+
            "przedstawiaj^ac^a rycerza w pe^lnej zbroi."; 
    }

    str += "\n";

    return str;
}

public string
exits_description() 
{
    return "Mo^zna st^ad pod^a^zy^c na po^ludnie przez wybity w "+
        "^scianie otw^or, lub po kamiennych stopniach do g^ory.\n";
}
