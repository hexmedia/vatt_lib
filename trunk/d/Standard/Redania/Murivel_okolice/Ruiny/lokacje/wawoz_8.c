/**
 * W^awoz na p^olnoc od traktu Rinde - Murivel
 *
 * autor Cairell
 * opisy Ania & Tabakista
 * data   2009
 */

#include "dir.h"
inherit RUINY_STD_WAWOZU;
#include <pogoda.h>
#include <stdproperties.h>
#include <macros.h>

string dlugi_opis();

void
create_wawoz()
{ 
    set_short("W w^awozie.");
    set_long(&dlugi_opis());

    add_exit(RUINY_LOKACJE + "wawoz_7.c", "nw");
    add_exit(RUINY_LOKACJE + "wawoz_9.c", "ne");

    add_sit(({"na ziemi"}), "na ziemi","z ziemi",0);

    add_item(({"g^lazy", "kamienie", "bloki"}), "Kilka "+
        "bezkszta^ltnych, szarych bry^l kamienia nadal nosi "+
        "^slady obr^obki i zaprawy. W jednym z nich "+
        "pozosta^l nawet w^aski otw^or strzelniczy. Le^z^a "+
        "one pod p^o^lnocn^a skarp^a, tam gdzie uderzy^ly o "+
        "dno po upadku z widocznych w g^orze mur^ow. "+
        "Kamienie otaczane s^a ju^z przez paprocie oraz "+
        "porastane przez mchy nie pozwalaj^ace stwierdzi^c "+
        "czy oderwa^ly si^e pod wp^lywem czasu, burzy, "+
        "czy skruszone r^ek^a ludzk^a.\n");
}

string dlugi_opis() 
{
    string str;

    str = "^Sliskie kamienie flankuj^acych w^aw^oz zboczy pn^a si^e "+
        "niemal pionowo ku g^orze, uniemo^zliwiaj^ac jakiekolwiek pr^oby "+
        "wspinaczki. Znacznie ni^zej, przez ^srodek kotliny, koryto "+
        "wyschni^etego potoku, dawniej odcinaj^acego "+
        "piechurom doj^scie pod po^ludniowe zbocze jaru. "+
        "Dooko^la panuje majestatyczna cisza, zak^l^ocana "+
        "jedynie przez miarowy szum drzew gdzie^s wysoko "+
        "oraz sporadyczne, odleg^le krakanie. ";
    
    if (jest_dzien() == 1)
    {
        str += "Spoza stromych ^scian z trudem dochodzi "+
            "tu jakiekolwiek ^swiat^lo, pogr^a^zaj^ac "+
            "okolic^e w ch^lodnym p^o^lcieniu i nadaj^ac "+
            "odbieraj^ac jej wi^ekszo^s^c barw.";
    }
    else
    {
        str += "Mroczne, strome ^sciany nachylaj^a si^e nad "+
            "dnem niemal przes^laniaj^ac granatowe, nocne "+
            "niebo i sprawiaj^ac, ^ze okolica tonie w "+
            "ciemno^sciach. ";
    }
    
    if (jest_dzien() == 1)
    {
        if(CZY_JEST_SNIEG(TO))
        {
            str += "Pod nimi zebra^ly si^e liczne, ^snie^zne "+
                "zaspy wymiecione jednak ze ^srodka drogi i "+
                "nie utrudnia^j^ace przej^scia. ";
        }
        else
        {
            str += " Jedyn^a ro^slinno^sci^a w tym miejscu s^a "+
                "po^z^o^lk^le, wyschni^ete k^epy paproci "+
                "porastaj^ace jedynie ni^zsze partie "+
                "zboczy i za^lomy mi^edzy ska^lami, wy^zej "+
                "bowiem g^lazy pozostaj^a ca^lkowicie nagie i "+
                "puste. Dopiero jeszcze wy^zej, ponad skrajem "+
                "w^awozu dostrzec mo^zna odleg^le czubki drzew.";
        }
    }

    str +="\n";
    return str;
}

public string
exits_description() 
{
    return "W^aw^oz biegnie dalej na p^o^lnocny-zach^od "+
        "i p^o^lnocny-wsch^od.\n";
}
