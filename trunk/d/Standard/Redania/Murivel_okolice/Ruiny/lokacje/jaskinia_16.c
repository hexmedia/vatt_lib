/**
 * Groty niedaleko ruin na p^olnoc od traktu Rinde - Murivel
 *
 * autor Cairell
 * opisy Tabakista
 * data   2008
 */

#include "dir.h"
inherit RUINY_STD_JASKIN;
#include <stdproperties.h>
#include <macros.h>

string dlugi_opis();

void
create_jaskinie()
{
    set_short("Mieszkalna grota.");
    set_long(&dlugi_opis());

    add_exit(RUINY_LOKACJE + "jaskinia_15.c", "n");
    add_exit(RUINY_LOKACJE + "jaskinia_14.c", "nw");
    
    add_npc(RUINY_LIVINGI + "pukaczF.c");
    
    add_object(RUINY_OBIEKTY + "palenisko.c", 1);
    add_object(RUINY_OBIEKTY + "kaganek.c", 1);

    dodaj_rzecz_niewyswietlana("palenisko",1);
    dodaj_rzecz_niewyswietlana("zapalone palenisko",1);
    dodaj_rzecz_niewyswietlana("wypalone palenisko",1);

    dodaj_rzecz_niewyswietlana("kaganek",1);
    dodaj_rzecz_niewyswietlana("zapalony kaganek",1);
    dodaj_rzecz_niewyswietlana("wypalony kaganek",1);
    
    add_sit(({"na pos^laniu"}), "na s^lomianym pos^laniu","z pos^lania",2);
    
    add_item(({"s^lom^e", "pos^lanie"}), "Rozrzucona wi^azkami to tu to "+
        "tam s^loma nie wydaje si^e zbyt ^swie^za. R^o^zni si^e zdecydowanie "+
        "kolorem od sporej, niemal z^lotej sterty z kt^orej wymoszczono "+
        "w rogu pos^lanie. Rzucono na nie kilka sk^or kozic lub muflon^ow.\n");
}

string dlugi_opis() 
{
    string str;

    str = "Spora pieczara jest w tym miejscu wyj^atkowo wysoko wysklepiona. "+
        "Biegn^acy idealnie w g^or^e dym wskazuje, ^ze gdzie^s tam, wysoko "+
        "w ciemno^sci musi znajdowa^c si^e jaki^s szyb. Pod^lo^ze wsz^edzie "+
        "grubo pokrywaj^a wi^ory i s^loma, ods^laniaj^ac nag^a ska^l^e "+
        "tylko woko^lo p^lytkiego zag^l^ebienia mieszcz^acego spore "+
        "palenisko. Niedaleko od niego, pod pochy^la ska^l^a s^loma zebrana "+
        "zosta^la w spory stos i wymoszczona na pos^lanie. Ponad ni^a, w "+
        "wykutej w kamieniu niszy mie^sci si^e prymitywny kaganek pe^len "+
        "mniej lub bardziej wypalonych ogark^ow ^swiec.\n";

    return str;
}

public string
exits_description() 
{
    return "Korytarz ci^agnie si^e dalej na p^o^lnoc i p^o^lnocny-zach^od.\n";
}
