/**
 * Groty niedaleko ruin na p^olnoc od traktu Rinde - Murivel
 *
 * autor Cairell
 * opisy Tabakista
 * data   2008
 */

#include "dir.h"
inherit RUINY_STD_JASKIN;
inherit "/lib/drink_water";
#include <stdproperties.h>
#include <macros.h>

string dlugi_opis();

void
create_jaskinie()
{
    set_short("Przy wyj^sciu z groty");
    set_long(&dlugi_opis());

    add_exit(RUINY_LOKACJE + "wawoz_10.c", "wyj^scie");
    add_exit(RUINY_LOKACJE + "grota_5.c", "e");

    set_drink_places(({"ze strumyka","ze strumienia", 
        "z ka^lu^zy"}));

    add_item(({"strumyk", "strumie^n"}), "Cienka stru^zka "+
        "wody wije si^e odnajduj^ac drog^e pomi^edzy "+
        "nier^owno^sciami pod^lo^za. Zasilana jest niemal "+
        "ca^ly czas przez krople spadaj^ace z sufitu oraz "+
        "sp^lywaj^ace po wilgotnych ^scianach. Co kawa^lek "+
        "ciek natrafia na wi^eksze zag^l^ebienia gdzie "+
        "woda zatrzymuje si^e na chwil^e tworz^ac p^lytkie "+
        "ka^lu^ze, lecz zaraz potem p^lynie dalej "+
        "pod^a^zaj^ac niespiesznie w kierunku wylotu "+
        "jaskini.\n");
}

public void
init()
{
    ::init();
    init_drink_water();
}

void
drink_effect(string skad)
{
    write("Kl^ekasz na skraju potoku i nabierasz wody w d^lonie, gasz^ac "+
        "ni^a pragnienie.\n");
    saybb(QCIMIE(this_player(), PL_MIA) + " kl^eka na skraju potoku i nabiera "+
        " wody w d^lonie, gasz^ac ni^a pragnienie.\n");
}

string dlugi_opis() 
{
    string str;

    if (jest_dzien() == 1)
    {
        str = "Jasne s^loneczne ^swiat^lo ";
    }
    else
    {
        str = "Blada ksi^e^zycowa ^luna "; 
    }

    str += "wpada przez szeroki wylot jaskini, za kt^orego "+
        "kraw^edzi^a znika p^lytki strumyk wyp^lywaj^acy "+
        "gdzie^s z g^l^ebi jaskini. Miarowy chlupot "+
        "zdradza, ^ze woda po opuszczeniu groty spada "+
        "z do^s^c znacznej wysoko^sci. Powietrze tak^ze "+
        "przesycone jest wilgoci^a. Po ska^lach powoli "+
        "w^edruj^a niewielkie krople daj^ace pocz^atek "+
        "zdobi^acym ^sciany zaciekom soli, pomi^edzy "+
        "kt^orymi, na go^lych, wapiennych kamieniach rosn^a "+
        "k^epki bladych porost^ow. Dalej w g^l^ebi "+
        "jaskini jest ich coraz mniej, za to zaczynaj^a "+
        "si� pojedyncze stalaktyty, z kt^orych tak^ze "+
        "kapie woda zasilaj^ac potok.\n";

    return str;
}

public string
exits_description() 
{
    return "Znajduje si^e tutaj wyj^scie na powierzchni^e, oraz w^aski "+
        "korytarz prowadz^acy na wsch^od.\n";
}
