/*
 * Standard do grot i jaskin przy ruinach za lasem wiewiorek.
 * Autor: Cairell
 *
 */

inherit "/std/room";
#include "/d/Standard/Redania/dir.h"
#include <stdproperties.h>
#include <macros.h>

void create_ruiny()
{
}

nomask void
create_room()
{
    add_prop(ROOM_I_WSP_Y, WSP_Y_RUINY_I_WAWOZ); //Wsp�rz�dne, do systemu pogodowego.
    add_prop(ROOM_I_WSP_X, WSP_X_RUINY_I_WAWOZ);
             
    create_ruiny();
}

public int unq_no_move(string str)
{
    notify_fail("Ruszasz przed siebie, lecz nie udaje ci si^e odnale^s^c "+
        "drogi w t^e stron^e.\n");
        
    string vrb = query_verb();

    if(vrb ~= "g�ra" || vrb ~= "d�")
        notify_fail("Nie mo�esz tam si� uda�!\n");

    return 0;
}

string dlugi_opis() 
{
}
