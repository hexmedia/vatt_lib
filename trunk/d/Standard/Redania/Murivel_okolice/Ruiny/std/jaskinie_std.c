/*
 * Standard do grot i jaskin przy ruinach za lasem wiewiorek.
 * Autor: Cairell
 *
 */

inherit "/std/room";
#include "/d/Standard/Redania/dir.h"
#include <stdproperties.h>
#include <macros.h>

string dlugi_opis();
string event();

void create_jaskinie()
{
}

nomask void
create_room()
{
    add_prop(ROOM_I_INSIDE,1);
    remove_prop(ROOM_I_LIGHT);
    add_prop(ROOM_S_DARK_LONG, "Panuje tutaj absolutna "+
        "ciemno^s^c nieprzenikniona tak bardzo, ^ze "+
        "nie wida^c nawet zbli^zonej do twarzy r^eki.\n");
    add_prop(ROOM_I_TYPE, ROOM_CAVE);

    add_sit(({"na srodku przej^scia"}), "na ^srodku przejscia",
        "z przej^scia",3);
    add_sit(({"pod ^scian^a"}), "pod ^scian^a","z pod ^sciany",5);
    
    add_prop(ROOM_I_WSP_Y, WSP_Y_RUINY_I_WAWOZ); //Wsp�rz�dne, do systemu pogodowego.
    add_prop(ROOM_I_WSP_X, WSP_X_RUINY_I_WAWOZ);
    
    set_event_time(400.0);
    add_event("Z niskiego korytarza dobiega przez moment, "+
        "powtarzane dalej po dziesi^eciokro^c przez "+
        "echo, jednostajne, metaliczne stukanie.\n");
    add_event("Gdzie^s blisko miarowo kapie woda.\n");
    add_event("Cienie przeskakuj^a niepokoj^aco pomi^edzy "+
        "pl^atanin^a naciek^ow i zag^l^ebie^n w ^scianach.\n");
    add_event("Spod stropu dochodzi ciche drapanie malutkich "+
        "pazurk^ow.\n");
    add_event(&event());
    
    create_jaskinie();
}

public int unq_no_move(string str)
{
    notify_fail("Ruszasz przed siebie, lecz zatrzymuje ci^e lita, "+
        "wapienna ska^la.\n");
        
    string vrb = query_verb();

    if(vrb ~= "g�ra" || vrb ~= "d�")
        notify_fail("Nie widzisz korytarzy wiod^acych w t^e stron^e.\n");
        
    return 0;
}

string dlugi_opis() 
{
}

string event()
{
    string strr;
    
    strr = "Kropla wody spada ci na ";

    switch(random(2)) 
    {
        case 0: strr += "rami^e"; break;
        case 1: strr += "kark"; break; 
        case 2: strr += "g^low^e"; break;
    }

    strr += ".\n";

    return strr;
}
