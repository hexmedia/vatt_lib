/*
 * Standard do grot i jaskin przy ruinach za lasem wiewiorek.
 * Autor: Cairell
 *
 */

inherit "/std/room";
#include "/d/Standard/Redania/dir.h"
#include <pogoda.h>
#include <mudtime.h>
#include <stdproperties.h>
#include <macros.h>

string dlugi_opis();
string opis_paproci();
string opis_zboczy();

void create_wawoz()
{
}

nomask void
create_room()
{
    add_prop(ROOM_I_INSIDE,0);

    add_prop(ROOM_S_DARK_LONG, "Wysokie ^sciany w^awozu"+
        "czyni^a mroki nocy jeszcze g^lebszymi i "+
        "trudniejszymi do przenikni^ecia wzrokiem.\n");

    add_sit(({"na ziemi"}), "na ziemi","z ziemi",0);
    
    add_item(({"paprocie", "zaspy"}), &opis_paproci());
    add_item(({"zbocza", "^sciany"}), &opis_zboczy());
    
    add_prop(ROOM_I_WSP_Y, WSP_Y_RUINY_I_WAWOZ); //Wsp�rz�dne, do systemu pogodowego.
    add_prop(ROOM_I_WSP_X, WSP_X_RUINY_I_WAWOZ);
    
    set_event_time(400.0);
    add_event("Widoczne wysoko w g^orze wierzcho^lki ^swierk^ow "+
        "i sosen wydaj^a si^e porusza^c niespiesznie.\n");
    add_event("Doko^la unosi si^e niezbyt natarczywy, ale wyra^xnie "+
        "wyczuwalny zapach butwiej^acego drewna.\n");
    add_event("Gdzie^s niedaleko odzywa si^e sroka.\n");
    add_event("Jaki^s drobny gryzo^n mign^a^l ci przez moment "+
        "po^sr^od kamieni.\n");
    add_event("Gdzie^s z g^ory dochodzi trzask ^lamanych ga^l^ezi.\n");
    
    create_wawoz();
}

public int unq_no_move(string str)
{
    notify_fail("Ruszasz przed siebie, lecz wysokie ^sciany w^awozu "+
        "okazuj^a si^e zbyt strome.\n");
        
        string vrb = query_verb();

    if(vrb ~= "g^ora" || vrb ~= "d^o^l")
        notify_fail("Nie mo^zesz tam si^e uda^c!\n");

    return 0;
}

string dlugi_opis() 
{
}

string opis_paproci()
{
    string st;

    if(CZY_JEST_SNIEG(TO))
    {
        st = "G^l^ebokie zaspy zalegaj^a ";
    } 
    else
    {
        st = "G^este zaro^sla pleni^a si^e ";
    }

    st += "w co wi^ekszych za^lomach wapiennych ^scian, "+
        "szczeg^olnie pod po^ludniow^a, wiecznie "+
        "ocienion^a ^scian^a. ";

    if (pora_roku() == MT_ZIMA)
    {
        st += "^Srodek natomiast pokrywa jedynie cienka "+
            "warstwa ^swie^zego, ^snie^znego puchu kt^orego "+
            "nie zd^a^zy^ly wymie^s^c jeszcze hulaj^ace "+
            "w^awozem zimowe wiatry.\n";
    }
    else
    {
        st += "W wi^ekszo^sci s^a to niewysokie paprocie "+
            "tworz^ace zwarty kobierzec, lecz miejscami "+
            "z pomi^edzy nich wystrzeliwuj^a k^epy bujnych "+
            "pokrzyw z daleka widoczne dzi^eki drobnym, "+
            "bia^lym kwiatom skupionymi pod li^s^cmi.\n";
    }
    return st;
}

string opis_zboczy()
{
    string st;
    
    st = "Niemal pionowe, a miejscami wr^ecz pochylaj^ace si^e nad dnem "+
        "parowu zbocza przes^laniaj^a wi^eksz^a cz^e^s^c nieba. Na niech^etne "+
        "wszelkiemu ^zyciu go^le, wapienne ska^ly jedynie z rzadka zosta^ly "+
        "opanowane przez nieliczne mchy czy k^epy rachitycznej trawy. Wydaje "+
        "si^e niemo^zliwo^sci^a by jakikolwiek cz^lowiek, czy zwierze byli w "+
        "stanie pokona^c kilkana^scie metr^ow zdradliwych za^lom^ow i dosta^c "+
        "si^e w pobli^ze ko^lysz^acych si^e nad skrajem w^awozu drzew.\n";
    
    return st;
}
