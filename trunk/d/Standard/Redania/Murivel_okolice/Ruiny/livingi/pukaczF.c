/**
 * Potw^or - pukacz, osobnik zenski
 *
 * autor Cairell
 * opisy Tabakista
 * data   2009
 */
inherit "/std/creature";
inherit "/std/combat/unarmed";
inherit "/std/act/attack";
inherit "/std/act/action";
inherit "/std/act/ask_gen";
inherit "/std/act/domove";
#pragma unique
#include <macros.h>
#include <stdproperties.h>
#include <money.h>
#include <pl.h>
#include <formulas.h>
#include <std.h>
#include <ss_types.h>
#include <wa_types.h>
#include <composite.h>
#include <filter_funs.h>

void
create_creature()
{
    ustaw_odmiane_rasy("pukacz");
    set_gender(G_MALE);

    set_long("Rudow^losy potw^or poszczyci^c si^e mo^ze barkami tak "+
        "szerokimi, ^ze mimo s^lusznego wzrostu, wydaje si^e niemal "+
        "kwadratowy. Graj^ace pod sk^or^a mi^esnie cz^e^sciowo jedynie "+
        "przys^lania obfita broda, pod kt^or^a czasami daj^a si^e dostrzec "+
        "jedna z ci^e^zkich, nieco obwis^lych piersi, zdradzaj^acych p^le^c "+
        "stworzenia. Samej twarzy niemal nie spos^ob dojrze^c gdy^z to czego "+
        "nie pokrywa bujny zarost skrywa si^e pod opadaj^ac^a na czo^lo, "+
        "wystrz^epion^a czupryn^a. Ogromne d^lonie i stopy o zrogowacia^lej "+
        "sk^orze zako^nczone s^a t^epymi, ale grubymi paznokciami "+
        "powyrastanymi w krzywe szpony. \n");

    dodaj_przym("du^zy", "duzi");
    dodaj_przym("przygarbiony", "przygarbieni");

    set_act_time(30);
    add_act("zapal kaganek");
    add_act("zapal palenisko");
    add_act("emote drapie si^e po rzyci.");
    add_act("emote smarka w palce po czym wyciera je o kosmate udo.");
    add_act("emote ziewa szeroko pokazuj^ac przy tym krzywe, "+
        "po^z^o^lk^le z^ebiska.");

    set_default_answer(VBFC_ME("default_answer"));

//FIXME: do zbalansowania tak umy jak i cechy
    set_stats ( ({ 65, 30, 70, 25, 60 }) );

    set_skill(SS_DEFENCE, 20 + random(5));
    set_skill(SS_UNARM_COMBAT, 20 + random(5));
    set_skill(SS_CLIMB, 40 + random(5));
    set_skill(SS_AWARENESS, 20 + random(5));
    set_skill(SS_BLIND_COMBAT,  70 + random(30));

    add_object("/std/krzemien.c");
    add_object("/std/krzemien.c");
    MONEY_MAKE_G(random(10))->move(this_object());
    MONEY_MAKE_D(random(5))->move(this_object());

    add_prop(LIVE_I_NEVERKNOWN, 1);
    add_prop(LIVE_I_INTRODUCING, 1);
    add_prop(CONT_I_WEIGHT, 110000);
    add_prop(CONT_I_HEIGHT, 180);
    set_random_move(10);
}

string
default_answer()
{
     set_alarm(1.0, 0.0, "command_present", this_player(),
        "':dudni^acym g^losem: Znajd^x se kogo innego do pogadania.");
     return "";
}

void
return_introduce(string imie)
{
    object osoba = present(imie, environment());

    if (osoba)
    {
        command("wzrusz ramionami");
    }
}

void
emote_hook(string emote, object wykonujacy, string przyslowek)
{
    switch (emote)
    {
        case "kopnij":
        case "spoliczkuj":
        case "opluj":
	    case "nadepnij":
	    if (random(2))
		set_alarm(1.5, 0.0, "command_present", wykonujacy,
		    "zabij "+lower_case(wykonujacy->query_name(PL_BIE)));
	    else
		set_alarm(1.5, 0.0, "command_present", wykonujacy,
		    "':dudni^acym g^losem: Jeszcze raz, a b^edziesz po omacku "+
            "szuka^c swoich z^eb^ow!");
        break;

        case "za�miej":
        case "poca�uj":
        case "przytul":
        case "pog�aszcz":
        if(wykonujacy->query_gender() == 0)
        {
	        if (random(2))
		    set_alarm(1.5, 0.0, "command_present", wykonujacy,
		        "emote odsuwa si^e o krok.");
	        else
		    set_alarm(1.5, 0.0, "command_present", wykonujacy,
		        "':dudni^acym g^losem: Poszo^l won odemnie.");
                break;
        }
        else
        {

            if (random(2))
		    set_alarm(1.5, 0.0, "command_present", wykonujacy,
		        "':dudni^acym g^losem: Daj^ze spok^oj.");
	        else
		    set_alarm(1.5, 0.0, "command_present", wykonujacy,
		        "':dudni^acym g^losem: We^x se lepiej ch^lopa poszukaj.");
                break;
        }
    }
}

void
attacked_by(object wrog)
{
    command("emote ryczy w^sciekle zaciskaj^ac wielkie jak bochny pi^e^sci.");
    ::attacked_by(wrog);
}
