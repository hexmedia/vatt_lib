/**
 * Potw^or - barbegazi
 *
 * autor Cairell
 * opisy Tabakista
 * data   2009
 */

inherit "/std/creature";
inherit "/std/combat/unarmed";
inherit "/std/act/attack";
inherit "/std/act/action";
inherit "/std/act/ask_gen";
inherit "/std/act/domove";
#include <std.h>
#include <ss_types.h>
#include <macros.h>
#include <stdproperties.h>
#include <pl.h>
#include <money.h>
#include <object_types.h>

int czy_atakowac();

void
create_creature()
{
    set_living_name("stworek");
    ustaw_odmiane_rasy("barbegazi");
    dodaj_nazwy("potw^or", "stw^or", "barbegazi");
    set_gender(G_MALE);

    random_przym(({"rudawy","rudawi","br^azowy", "br^azowi", "czarny", 
        "czarni", "szarawy", "szarawi"}), ({"kud^laty", "kud^laci", 
        "futrzasty", "futrza^sci", "wylnia^ly", "wylniali"}), ({"brudny", 
        "brudni","zakurzony","zakurzeni", "du^zy", "duzi", "ma^ly", 
        "mali"}), 2);

    set_long("Du^ze, jarz^ace si^e m^etnym blaskiem ^slepia wydaj^a si^e "+
        "utkwione w tobie, o ile nie jest to z^ludzenie wywo^lane przez "+
        "szaro br^azowe futro, opadaj^ace na nie. Takie spl^atane "+
        "kud^ly pokrywaj^a tak^ze resz^e przypominaj^acego spory kamie^n, "+
        "kr^epego cia^la. Spod niego wystaj^a ko^slawe, gruzo^lowate ^lapy "+
        "szuraj^ace co chwila o kamienie t^epo zako^nczonymi, lecz grubymi "+
        "i mocnymi pazurami. Stworek jest ca^ly pokryty kurzem i py^lem "+
        "skalnym, kt^orego drobiny unosz^a si^e w powietrze przy ka^zdym "+
        "gwa^ltowniejszym poruszeniu. Dociera tak^ze do ciebie jego "+
        "niezbyt natarczywy, pizmowy zapach.\n");

    set_stats (({35+random(15),40+random(20),30+random(10),10+random(10),
                40+random(10)}));
    add_prop(CONT_I_WEIGHT, 10000+random(5000));
    add_prop(CONT_I_HEIGHT, 30+random(10));
    add_prop(LIVE_I_NEVERKNOWN, 1);
    add_prop(LIVE_I_SEE_DARK, 1);
    set_random_move(10);
    set_aggressive(&czy_atakowac());
    set_attack_chance(41+random(10));
    set_npc_react(1);

//FIXME do zbalansowania
    set_skill(SS_UNARM_COMBAT,  40 + random(14));
    set_skill(SS_DEFENCE,       24);
    set_skill(SS_AWARENESS,     64 + random(10));
    set_skill(SS_BLIND_COMBAT,  70 + random(30));
    set_skill(SS_HIDE,          30 + random(40));
    set_skill(SS_SNEAK,         30 + random(30));
    
    add_prop(LIVE_I_SEE_DARK, 1);

    set_attack_unarmed(0, 20, 22, W_SLASH,  100, "pazury");
    set_attack_unarmed(0, 20, 22, W_IMPALE,  100, "z^eby");

    set_hitloc_unarmed(0, ({ 1, 1, 1 }), 10, "^leb");
    set_hitloc_unarmed(1, ({ 1, 1, 4 }), 80, "tu^l^ow");
    set_hitloc_unarmed(2, ({ 1, 1, 1 }), 10, "^lapa");

    add_leftover("/std/leftover", "z^ab", random(5) + 2, 0, 1, 0, 0, O_KOSCI);
    add_leftover("/std/leftover", "czaszka", 1, 0, 1, 1, 0, O_KOSCI);
    add_leftover("/std/leftover", "ko^s^c", random(3) + 2, 0, 1, 1, 0, O_KOSCI);

    if(random(2))
        add_leftover("/std/leftover", "serce", 1, 0, 0, 1, 0, O_JEDZENIE);
}

string default_answer()
{
    command("emote chrumka sobie pod nosem.");
    return "";
}

//rozbudowac ! ! !
int
czy_atakowac()
{
    if(TP->query_rasa() != "barbegazi")
        return 1;
    else
        return 0;
}
