/**
 * Potw^or - pukacz, osobnik meskis
 *
 * autor Cairell
 * opisy Tabakista
 * data   2009
 */
inherit "/std/creature";
inherit "/std/combat/unarmed";
inherit "/std/act/attack";
inherit "/std/act/action";
inherit "/std/act/ask_gen";
inherit "/std/act/domove";
#pragma unique
#include <macros.h>
#include <stdproperties.h>
#include <money.h>
#include <pl.h>
#include <formulas.h>
#include <std.h>
#include <ss_types.h>
#include <wa_types.h>
#include <composite.h>
#include <filter_funs.h>

void
create_creature()
{
    ustaw_odmiane_rasy("pukacz");
    set_gender(G_MALE);

    set_long("Ca^le cia^lo, tak szerokie bary jak i muskularne ramiona oraz "+
        "grube nogi stwora pokryte s^a jedynie rudymi w^losami, na tors "+
        "jednak dodatkowo sp^lywa d^lugie, posklejane brodzisko. Na jego "+
        "twarzy ponad podobnymi brodzie w^asami, a pod krzaczastymi brwiami "+
        "znalaz^lo si^e jedynie tyle miejsca by zmie^sci^l si^e "+
        "kartoflowaty nos oraz lekko zamglone ^slepia. Z wype^lnionych "+
        "krzywymi, ^z^o^ltymi z^ebami ust tak mocno zieje gorza^l^a, ^ze "+
        "niejednemu by w g^lowie zaszumia^lo od samego zapachu. ^Zylaste "+
        "^lapska stworzenia zako^nczone s^a t^epymi, zrogowacia^lymi "+
        "paznokciami, lecz musku^ly wyra^xnie rysuj^a si^e pod brudn^a "+
        "sk^or^a.\n");

    dodaj_przym("wielki", "wielcy");
    dodaj_przym("rudobrody", "rudobrodzi");

    set_act_time(30);
    add_act("zapal kaganek");
    add_act("zapal palenisko");
    add_act("emote drapie si^e po rzyci.");
    add_act("emote smarka w palce po czym wyciera je o kosmate udo.");
    add_act("emote beka pot^e^znie, a powietrze momentalnie wype^lnia "+
        "zapach gorza^lki.");
    add_act("emote ziewa szeroko pokazuj^ac przy tym krzywe, "+
        "po^z^o^lk^le z^ebiska.");

    set_default_answer(VBFC_ME("default_answer"));

//FIXME: do zbalansowania tak umy jak i cechy
    set_stats ( ({ 65, 30, 70, 25, 60 }) );

    set_skill(SS_DEFENCE, 20 + random(5));
    set_skill(SS_UNARM_COMBAT, 20 + random(5));
    set_skill(SS_CLIMB, 40 + random(5));
    set_skill(SS_AWARENESS, 20 + random(5));
    set_skill(SS_BLIND_COMBAT,  70 + random(30));

    add_object("/std/krzemien.c");
    add_object("/std/krzemien.c");
    MONEY_MAKE_G(random(10))->move(this_object());
    MONEY_MAKE_D(random(5))->move(this_object());
    add_object("/d/Standard/items/inne/buklaki/niewielki.c");

    add_prop(LIVE_I_NEVERKNOWN, 1);
    add_prop(LIVE_I_INTRODUCING, 1);
    add_prop(CONT_I_WEIGHT, 130000);
    add_prop(CONT_I_HEIGHT, 190);
    set_random_move(10);
}

string
default_answer()
{
     set_alarm(1.0, 0.0, "command_present", this_player(),
        "':dudni^acym g^losem: Znajd^x se kogo innego do pogadania.");
     return "";
}

void
return_introduce(string imie)
{
    object osoba = present(imie, environment());

    if (osoba)
    {
        command("wzrusz ramionami");
    }
}

void
emote_hook(string emote, object wykonujacy, string przyslowek)
{
    switch (emote)
    {
        case "kopnij":
        case "spoliczkuj":
        case "opluj":
	    case "nadepnij":
	    if (random(2))
		set_alarm(1.5, 0.0, "command_present", wykonujacy,
		    "zabij "+lower_case(wykonujacy->query_name(PL_BIE)));
	    else
		set_alarm(1.5, 0.0, "command_present", wykonujacy,
		    "':dudni^acym g^losem: Jeszcze raz, a b^edziesz po omacku "+
            "szuka^c swoich z^eb^ow!");
        break;

        case "za�miej":
        case "poca�uj":
        case "przytul":
        case "pog�aszcz":
        if(wykonujacy->query_gender() == 0)
        {
	        if (random(2))
		    set_alarm(1.5, 0.0, "command_present", wykonujacy,
		        "popatrz dziwnie na "+lower_case(wykonujacy->query_name(PL_BIE)));
	        else
		    set_alarm(1.5, 0.0, "command_present", wykonujacy,
		        "':dudni^acym g^losem: Poszo^l won odemnie.");
                break;
        }
        else
        {
	        if (random(2))
		    set_alarm(1.5, 0.0, "command_present", wykonujacy,
		        "emote u^smiecha si^e oble^snie.");
	        else
		    set_alarm(1.5, 0.0, "command_present", wykonujacy,
		        "':dudni^acym g^losem: Daj spok^oj, bo moja baba zobaczy.");
                break;
        }
    }
}

void
attacked_by(object wrog)
{
    command("emote ryczy w^sciekle zaciskaj^ac wielkie jak bochny pi^e^sci.");
    ::attacked_by(wrog);
}
