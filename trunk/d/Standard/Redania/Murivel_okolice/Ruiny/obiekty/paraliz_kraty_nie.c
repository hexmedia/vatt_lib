/**
 * W^aw^oz na p^olnoc od traktu Rinde - Murivel
 *
 * autor Cairell
 * opisy Tabakista
 * data   2008
 */
#pragma strict_types
#include <macros.h>
#include <ss_types.h>
#include "dir.h"

inherit "/std/paralyze";

static int alarm_id;

void
create_paralyze()
{
    set_name("wspinasie");
    set_stop_verb("przesta^n");
    set_stop_message("Przestajesz stara^c sie unie^s^c krat^e.\n");
    set_fail_message("Jeste� teraz zaj^et"+TP->koncowka("y","a") +
        "unoszeniem kraty, musisz "+
        "przesta^c by m^oc zrobi^c, to co chcesz.\n");

    set_finish_object(this_object());
    set_finish_fun("zatrzymanie");
    set_remove_time(5);
    setuid();
    seteuid(getuid());
}

int
zatrzymanie(object player)
{
    write("Zardzewia^le ^zelazo nie chce ust^api^c szoruj^ac o "+
        "mur. Napr^e^zasz jeszcze przez chwil^e wszystkie "+
        "mi^e^snie lecz w ko^ncu musisz si� podda�.\n");
    saybb("Zardzewia^le ^zelazo nie chce ust^api^c szoruj^ac "+
        "o mur. "+QCIMIE(player,PL_MIA)+" napr^e^za jeszcze "+
        "przez chwil^e wszystkie mi^e^snie lecz w ko^ncu "+
        "musi si^e podda^c.\n");
    player->add_old_fatigue(-40);

    remove_object();

    return 1;
}
