/**
 * W^aw^oz na p^olnoc od traktu Rinde - Murivel
 *
 * autor Cairell
 * opisy Tabakista
 * data   2008
 */
#pragma strict_types
#include <macros.h>
#include <exp.h>
#include <ss_types.h>
#include "dir.h"

inherit "/std/paralyze";

static int alarm_id;

void
create_paralyze()
{
    set_name("wspinasie");
    set_stop_verb("przesta^n");
    set_stop_message("Przestajesz si^e wspina^c na most.\n");
    set_fail_message("Jeste� teraz zaj^et"+TP->koncowka("y","a") +
        "wspinaniem si^e na most. Musisz "+
        "przesta^c by m^oc zrobi^c, to co chcesz.\n");

    set_finish_object(this_object());
    set_finish_fun("zatrzymanie");
    set_remove_time(4);
    setuid();
    seteuid(getuid());
}

int
zatrzymanie(object player)
{
    write("Ostro^znie przytrzymuj^ac si^e jednego z ocala^lych filar^ow "+
        "stawiasz kolejne kroki po^sr^od stercz^acych korzeni i "+
        "chybotliwych kamieni. Nagle jeden z nich osuwa si^e a ^sliski "+
        "bal wymyka ci si^e z r^ak, przez co zsuwasz si^e na dno "+
        "w^awozu wzbijaj^ac w powietrze tuman ziemi i suchych li^sci.\n");
    saybb(QCIMIE(player, PL_MIA) +" traci oparcie i zje^zdza na dno "+
        "w^awozu wzbijaj^ac w powietrze tuman ziemi i suchych li^sci.\n");
    player->add_old_fatigue(-40);
    player->increase_ss(SS_CLIMB,EXP_WSPINAM_PRAWIE_UDANY_CLIMB);
    player->increase_ss(SS_STR,EXP_WSPINAM_PRAWIE_UDANY_CLIMB_STR);
    player->increase_ss(SS_DEX,EXP_WSPINAM_PRAWIE_UDANY_CLIMB_DEX);
    player->increase_ss(SS_CON,EXP_WSPINAM_PRAWIE_UDANY_CLIMB_CON);
    remove_object();

    return 1;
}
