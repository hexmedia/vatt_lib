/**
 * W^aw^oz na p^olnoc od traktu Rinde - Murivel
 *
 * autor Cairell
 * opisy Tabakista
 * data   2009
 */
#pragma strict_types
#include <exp.h>
#include <macros.h>
#include <ss_types.h>
#include "dir.h"

inherit "/std/paralyze";

void
create_paralyze()
{
    set_name("wspinasie");
    set_stop_verb("przesta^n");
    set_stop_message("Przestajesz si^e wspina^c.\n");
    set_fail_message("Jeste� teraz zaj^et"+TP->koncowka("y","a") +
        "wspinaniem si^e. Musisz "+
        "przesta^c by m^oc zrobi^c, to co chcesz.\n");
    set_finish_object(this_object());
    set_finish_fun("koniec_wspinaczki");

    set_remove_time(6);
    setuid();
    seteuid(getuid());
}

void
koniec_wspinaczki(object player)
{
    player->catch_msg("Wyszukuj^ac palcami kolejne zag^l^ebienia w skale "+
        "stopniowo pokonujesz ^scian^e. Si^egaj^ac w ko^ncu "+
        "kraw^edzi otworu podci^agasz si^e i zag^lebiasz w mrok jaskini.\n");
    saybb(QCIMIE(player,PL_MIA)+" wyszukuj^ac palcami kolejne zag^l^ebienia "+
        "w skale stopniowo pokonuje ^scian^e. Si^egaj^ac w ko^ncu "+
        "kraw^edzi otworu podci^agas si^e i zag^lebia w mrok jaskini.\n");
    player->add_old_fatigue(-45);
    player->increase_ss(SS_CLIMB,EXP_WSPINAM_UDANY_CLIMB);
    player->increase_ss(SS_STR,EXP_WSPINAM_UDANY_CLIMB_STR);
    player->increase_ss(SS_DEX,EXP_WSPINAM_UDANY_CLIMB_DEX);
    player->increase_ss(SS_CON,EXP_WSPINAM_UDANY_CLIMB_CON);

    player->move_living("M", RUINY_LOKACJE+"grota_4.c");
    saybb(QCIMIE(player,PL_MIA)+" wychyla si^e zza kraw^edzi groty, po czym "+
        "podci^aga i wchodzi do ^srodka.\n");
    remove_object();
    return; 

}
