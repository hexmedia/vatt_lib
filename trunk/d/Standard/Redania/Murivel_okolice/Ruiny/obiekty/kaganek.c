/**
 * Ruiny na p^olnoc od traktu Rinde - Murivel
 *
 * autor Cairell
 * opisy Tabakista
 * data   2008
 */

#pragma save_binary
#pragma strict_types
inherit "/lib/light.c";
inherit "/std/lampa.c";
#include <object_types.h>
#include <stdproperties.h>
#include <wa_types.h>
#include <macros.h>

string dlugi_opis();
//private int Light_Strength, Time_Left=MAXINT;

void
create_lamp()
{
    ustaw_nazwe("kaganek");

    add_prop(OBJ_I_WEIGHT, 50000);
    add_prop(OBJ_I_VOLUME, 35000);
    add_prop(OBJ_M_NO_SELL, 1);
    add_prop(OBJ_I_NO_GET, "Ten kaganek jest cz^e^sci^a ska^ly.\n");
    
    set_type(O_INNE);

    set_long(&dlugi_opis());

    set_time(MAXINT);
    set_strength(2);
    set_value(13);
    set_time_left(MAXINT);

    config_light();
}

string dlugi_opis()
{
    string str;

    str ="Topornie wykuta na wysoko^sci oczu wprost w wapiennej skale "+
        "nisza pe^lna jest niedopalonych ogark^ow i knot^ow ^swiec, cho^c "+
        "dwie czy trzy nadal ";
        
   	if(this_object()->query_prop(OBJ_I_LIGHT))
    {
        str+="rozprasza nieco mrok bladymi p^lomieniami, daj^acymi "+
            "jednak wi^ecej dymu ni^z ^swiat^la. ";
    }
    else
    {
        str+="s^a na tyle d^lugie, ^ze cho^c przez jaki^s czas da^lyby rad^e "+
            "roz^swietli^c nieco mrok. ";
    }
    str+="P^lytkie zag^l^ebienie w dnie niszy dawno cale zape^lni^lo si^e "+
        "stopionym woskiem, kt^orego krople sp^lywaj^ac w d^o^l po skale "+
        "utworzy^ly ca^lkiem ju^z poka^xne zacieki.\n";
    return str;  
}
