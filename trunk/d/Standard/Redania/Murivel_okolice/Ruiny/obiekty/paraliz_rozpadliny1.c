/**
 * Jaskinie na p^olnoc od traktu Rinde - Murivel
 *
 * autor Cairell
 * opisy Tabakista
 * data   2008
 */
#pragma strict_types
#include <exp.h>
#include <macros.h>
#include <ss_types.h>
#include "dir.h"

inherit "/std/paralyze";

void
create_paralyze()
{
    set_name("paraliz");
    set_stop_message("");
    set_fail_message("Jeste^s niesion"+TP->koncowka("y","a") +
        " gwa^ltownym pr^adem. Musisz skupi^c ca^l^a uwag^e by cho^c co "+
        "chwila zaczerpn^a^c powietrza.\n");
    set_finish_object(this_object());
    set_finish_fun("polowa_wycieczki");

    set_remove_time(15+random(10));
    setuid();
    seteuid(getuid());
}

void polowa_wycieczki(object player)
{
    player->catch_msg(" T^epy b^ol osza^lamia ci^e na moment zgaszony lodowato "+
        "zimn^a wod^a, kt^ora natychmiast porywa ci^e by cisn^a^c o kolejn^a "+
        "i kolejn^a ska^l^e. Karuzela wydaje si^e trwa^c wieczno^s^c, a "+
        "pr^ad ledwie pozwala z^lapa^c oddech pomi^edzy kolejnymi atakami.\n");
    saybb(QCIMIE(player,PL_MIA)+" przep^lywa obok walcz^ac z pr^adem i co "+
        "chwila uderzaj^ac o kamienie.\n");
    clone_object(RUINY_OBIEKTY+"paraliz_rozpadliny2")->move(TP);
    remove_object();
    return;
}
