/**
 * Ruiny na p^olnoc od traktu Rinde - Murivel
 *
 * autor Cairell
 * opisy Tabakista
 * data   2008
 */
#pragma strict_types
#include "dir.h"
inherit "/std/object.c";

#include <stdproperties.h>
#include <macros.h>
#include <pl.h>
#include <cmdparse.h>
#include <ss_types.h>
#include <materialy.h>
#include <object_types.h>
#include <sit.h>

int unies(string str);

void
create_object()
{
    ustaw_nazwe(("krata"), PL_ZENSKI);
    dodaj_przym("przerdzewia^la", "przerdzewiali");

    add_prop(OBJ_I_WEIGHT, 100000);
    add_prop(OBJ_I_VOLUME, 35000);
    add_prop(OBJ_I_NO_GET, "Krata jest troche za du^za i za za "+
        "dobrze osadzona w bramie by j^a po prostu wzi^a^s^c.\n");

    set_long("W grube, ^zelazne pr^ety solidnie w^zar^la si^e "+
        "ju^z rdza, lecz nadal pozostaj^ac solidnie osadzone w "+
        "bramie. Za nimi p^lyty dziedzi^nca urywaj^a si^e "+
        "nagle na kraw^edzi fosy, czy te^z w^awozu, na dnie "+
        "kt^orego dostrzec mo^zna to co zosta^lo z mostu.\n");
}


public void init()
{
   ::init();
   add_action(unies, "unie^s");
}

int unies(string str)
{
    object *kto, *most;
	
    if(TP->query_prop(SIT_LEZACY) || TP->query_prop(SIT_SIEDZACY))
    {
        write("Najpierw wsta^n.\n");
        return 1;
    }

    if(str != "krat^e" && str != "krate")
	{
       write("Co chcesz unie^s^c?\n");
       return 1;
    }
	else
	{
        write("Stajesz w rozkroku tu^z przy kracie, ostro^znie "+
            "dobierając miejsce chwytu tam gdzie pr^ety nadal "+
            "s^a do^s^c grube, po czym zapieraj^ac si^e z "+
            "ca^lych si^l zaczynasz ci^agn^a^c w g^or^e.\n");
        saybb(QCIMIE(TP, PL_MIA) + " staje w rozkroku tu^z "+
            "przy kracie, ostro^znie dobieraj^ac miejsce "+
            "chwytu, po czym zapieraj^ac si^e z ca^lych "+
            "si^l zaczyna ci^agn^a^c w g^or^e.\n");
        if (TP->query_stat(SS_STR) <= 60)
        {
            clone_object(RUINY_OBIEKTY+"paraliz_kraty_nie")->move(TP);
            return 1;
        }
        else if (TP->query_stat(SS_STR) >= 60 && 
            TP->query_stat(SS_STR) <= 75)
        {
            clone_object(RUINY_OBIEKTY+"paraliz_kraty_prawie")->move(TP);
            return 1;
        }
        else 
        {
            clone_object(RUINY_OBIEKTY+"paraliz_kraty_done")->move(TP);
            return 1;
        }
		return 1;
	}
}

public int
is_krata_z_ruin()
{
    return 1;
}
