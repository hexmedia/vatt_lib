/**
 * Jaskinie na p^olnoc od traktu Rinde - Murivel
 *
 * autor Cairell
 * opisy Tabakista
 * data   2008
 */
#include "dir.h"
inherit "/std/object.c";
#include <pl.h>
#include <stdproperties.h>
#include <macros.h>
#include <filter_funs.h>
#include <object_types.h>
#include <ss_types.h>
#include <sit.h>
#include <cmdparse.h>
#include <composite.h>

string dlugi_opis();
int przeskocz(string str);
int wskocz(string str);
int wrzuc(string str);
void plum();

create_object()
{
    ustaw_nazwe("rozpadlina");

    set_long(&dlugi_opis());

    add_prop(OBJ_I_WEIGHT, 0);
    add_prop(OBJ_I_VOLUME, 0);
    add_prop(OBJ_I_VALUE, 0);
    add_prop(OBJ_M_NO_SELL, 1);
    add_prop(OBJ_I_NO_GET, "We� co?\n"); //po co pisa� co innego i sugerowa� graczom �e rozpadlina jest obiektem;p [vera]

    set_type(O_INNE);
    seteuid(getuid());
}

string dlugi_opis()
{
    string str;
    str="Skalne, wapienne pod^lo^ze urywa si^e gwa^ltownie przechodz^ac "+
        "w rozpadlin^e, kt^orej dna nie spos^ob dostrzec w panuj^acej "+
        "tam ciemno^sci. Dochodzi do ciebie jedynie szum p^lyn^acej "+
        "gdzie^s tam wody. Po przeciwnej stronie rozpadliny otwiera "+
        "si^e korytarz bli^zniaczo podobny do tego w kt^orym stoisz. \n";
    return str;
}

init()
{
    ::init();
    add_action(przeskocz, "przeskocz");
    add_action(wskocz, "wskocz");
    add_action(wrzuc, "wrzu^c");
}

int przeskocz(string str)
{
    mixed *ob;
    notify_fail("Gdzie chcesz skoczy^c?\n");

    if(!strlen(str))
        return 0;

    if(!parse_command(str, environment(TP), "%i:" +
        PL_BIE,ob))
        return 0;

    ob = NORMAL_ACCESS(ob, 0, 0);
    if (!ob) return 0;
    if (ob[0] != TO) return 0; 

    if(TP->query_prop(SIT_LEZACY) || TP->query_prop(SIT_SIEDZACY))
    {
        write("Najpierw wsta^n!\n");
        return 1;
    }

    if(TP->query_fatigue() <= 2504)
    {
        write("Czujesz, ^ze mo^ze zabrakn^a^c ci na to si^ly, lepiej "+
            "wpierw z^lap oddech.\n");
        return 1;
    }
    
    //zaczyna przeskakiwac

    write("Cofasz si^e o krok bor^ac rozbieg, po czym wybijasz si^e "+
        "pewnie i skaczesz ponad rozpadlin^a. ");
    saybb(QCIMIE(TP, PL_MIA) + " cofa si^e o krok bor^ac rozbieg, po "+
        "czym wybijasz si^e pewnie i skacze ponad rozpadlin^a. ");
    //zostawic spacje na koncu zamiast '\n', by sie ladnie skleilo 
    //z dalsza czescia tekstu

    //nieudane
    if(TP->query_skill(SS_DEX) + TP->query_skill(SS_ACROBAT) + 
        random(10) <= 40 + random(10))
    {
        write("Zderzasz si^e jednak z przeciwleg^la kraw^edzi^a, "+
            "przez sekund^e starasz jeszcze chwyci^c czegokolwiek, "+
            "lecz w ko^ncu spadasz w d^o^l. Przez zdaj^acy si^e "+
            "ci^agn^a^c w niesko^nczono^s^c moment zag^l^ebiasz si^e "+
            "w ciemno^s^c na dnie by w ko^ncu uderzy^c o "+
            "powierzchni^e wody.\n");
        saybb(QCIMIE(TP, PL_MIA) + " zderza si^e jednak z przeciwleg^la "+
            "kraw^edzi^a, przez sekund^e stara jeszcze chwyci^c "+
            "czegokolwiek, lecz w ko^ncu spada w d^o^l, w ciemno^s^c.\n");
        set_alarm(2.0, 0.0, "plum");
        tell_room(RUINY_LOKACJE + "jaskinia_4", "Jaka^s posta^c po "+
            "przeciwleg^lej stronie jaskini wyskakuje w powietrze by "+
            "zaraz znikn^a^c w ciemnej od^ch^lani.\n");
        clone_object(RUINY_OBIEKTY+"paraliz_rozpadliny1")->move(TP);
        TP->move(RUINY_LOKACJE+"jaskinia_0.c",1);
        TP->add_fatigue(-2050);

        return 1;
    }
    //udane
    else
    {
        write("Przykl^ekasz na jedno kolano l^aduj^ac bezpiecznie "+
            "po drugiej stronie.\n");
        saybb(QCIMIE(TP, PL_MIA) + " przykl^eka na jedno kolano "+
            "l^aduj^ac bezpiecznie po drugiej stronie.\n");
        TP->move(RUINY_LOKACJE+"jaskinia_4.c",1);
        ENV(TP)->saybb(QCIMIE(TP, PL_MIA) + " przykl^eka na jedno "+
            "kolano l^aduj^ac tutaj bezpiecznie po skoku z drugiej strony"+ 
            "rozpadliny.\n");
        TP->add_fatigue(-2050);
        return 1;
    }
    return 1;
}

int wskocz(string str)
{
    mixed *ob;
    notify_fail("Gdzie chcesz skoczy^c?\n");

    if(!strlen(str))
        return 0;

    if(!parse_command(str, environment(TP), " 'do' %i:" +PL_DOP,ob))
        return 0;

    ob = NORMAL_ACCESS(ob, 0, 0);
    if (!ob) return 0;
    if (ob[0] != TO) return 0; 

    if(TP->query_prop(SIT_LEZACY) || TP->query_prop(SIT_SIEDZACY))
    {
        write("Najpierw wsta^n!\n");
        return 1;
    }

    write("Robisz krok w ty^l nabieraj^ac rozp^edu po czym przeskakujesz "+
        "ponad kraw^edzi^a rozpadliny. Przez "+
        "zdaj^acy si^e ci^agn^a^c w niesko^nczono^s^c moment "+
        "zag^l^ebiasz si^e w ciemno^s^c na dnie by w ko^ncu uderzy^c o "+
        "powierzchni^e wody.\n");
    saybb(QCIMIE(TP, PL_MIA) + " robi krok w ty^l nabieraj^ac rozp^edu "+
        "po czym przeskakuje ponad kraw^edzi^a rozpadliny znikaj^ac "+
        "w ciemno^sci.\n");
    tell_room(RUINY_LOKACJE + "jaskinia_13", "Jaka^s posta^c po "+
        "przeciwleg^lej stronie jaskini wyskakuje w powietrze by "+
        "zaraz znikn^a^c w ciemnej od^ch^lani.\n");
    set_alarm(2.0, 0.0, "plum");
    clone_object(RUINY_OBIEKTY+"paraliz_rozpadliny1")->move(TP);
    TP->move(RUINY_LOKACJE+"jaskinia_0.c",1);

    return 1;
}

//pomocnicza funkcja do filtrowania ekwipunku gracza.
int
filtruj_niewidoczne(object ob)
{
    return !(ob->query_no_show() || ob->query_no_show_composite() ||
        /*ob->query_prop(OBJ_I_DONT_INV) || bleble! g�wno. Obiekty w subloku tez maj� by� liczone!*/
        ob->query_prop(OBJ_M_NO_DROP) ||
        ob->query_prop(WIZARD_ONLY) );
}

int wrzuc(string str)
{
    mixed *ob1; 
    mixed ob2;
    notify_fail("Co i gdzie chcesz wrzuci^c?\n");
    
    if(!strlen(str))
        return 0;

    if(!parse_command(str, AI(ENV(TP))+AI(TP), 
        "%i:"+PL_BIE+" 'do' %o:" +PL_DOP, ob1, ob2))
        return 0;
    
    if (!objectp (ob2))
        return 0;
    
    ob1 = NORMAL_ACCESS(ob1, 0, 0);
    
    if(!sizeof(ob1)) return 0;
    if (ob2 != TO) return 0;
    
    
    
    //trzeba jeszcze sprawdza� czy to legalne obiekty!
    ob1 = filter(ob1, &filtruj_niewidoczne());
    
    
    TP->catch_msg("Wrzucasz "+COMPOSITE_DEAD(ob1, PL_BIE)+" do rozpadliny.\n");
    saybb(QCIMIE(TP, PL_MIA) + " wrzuca "+COMPOSITE_DEAD(ob1, PL_BIE)+
        " do rozpadliny.\n");
    foreach(object x : ob1)
        x->remove_object();
    
    set_alarm(2.0, 0.0, "plum");
    return 1;
}

void plum()
{
     tell_room(RUINY_LOKACJE + "jaskinia_13", "Z do^lu dochodzi plusk "+
         "rozbryzgiwanej wody.\n");
     tell_room(RUINY_LOKACJE + "jaskinia_4", "Z do^lu dochodzi plusk "+
         "rozbryzgiwanej wody.\n");
     return;
}
