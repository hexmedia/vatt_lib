/**
 * W^aw^oz na p^olnoc od traktu Rinde - Murivel
 *
 * autor Cairell
 * opisy Tabakista
 * data   2008
 */
#include "dir.h"
inherit "/std/object.c";
#include <pl.h>
#include <stdproperties.h>
#include <macros.h>
#include <filter_funs.h>
#include <materialy.h>
#include <object_types.h>
#include <ss_types.h>
#include <sit.h>

int wespnij(string str);
string dlugi_opis();

create_object()
{
    ustaw_nazwe("most");
    //dodaj_nazwe("sterta");

    set_long(&dlugi_opis());

    add_prop(OBJ_I_WEIGHT, 5000000);
    add_prop(OBJ_I_VOLUME, 5000000);
    add_prop(OBJ_I_VALUE, 1);
    add_prop(OBJ_M_NO_SELL, 1);
    add_prop(OBJ_I_NO_GET, "Most jest troche za du^zy "+
        "by go po prostu wzi^a^c.\n");

    set_type(O_INNE);
    seteuid(getuid());
}

string dlugi_opis()
{
    string str;

    str = "Sterta grubych, przegni^lych bali sm^etnie si^ega w g^or^e "+
        "licznymi, murszej^acymi u^lomkami. Kilka kr^otszych belek nadal "+
        "osadzonych jest w zboczu w^awozu nie podtrzymuj^ac "+
        "ju^z jednak niczego poza porastaj^acymi je g^esto mchami. "+
        "Natomiast porosty na prymitywnej ^scie^zce, tu^z przy filarach "+
        "zosta^ly wydarte butami wspinaj^acych si^e. Strome podej^scie "+
        "ko^nczy si^e przy ostatnich pozosta^lo^sciach mostu, kt^ore "+
        "pozosta^ly na swoim miejscu - flankuj^acych podjazd, kamiennych "+
        "murkach.\n";

    return str;
}
public void
init()
{
    ::init();
    add_action(wespnij,"wespnij");
    add_action(wespnij,"wejd^x");
}

int wespnij(string str)
{
    object *kto, *most;

    if(query_verb() == "wespnij")
        notify_fail("Wespnij si� gdzie?\n");
    else
        notify_fail("Wejd^x gdzie?\n");


    if(!strlen(str))
       return 0;

    if(!parse_command(str, environment(TP), "'si^e' 'na' %i:" +
        PL_BIE,most) && !parse_command(str, environment(TP), 
        "'na' %i:" +PL_BIE,most))
        return 0;

    if(!HAS_FREE_HANDS(TP))
    {
       write("Musisz mie^c obie d^lonie wolne, aby m^oc to zrobi^c.\n");
       return 1;
    }

    if (!most) return 0;
    if (!str) return 0;
    if (most[0][1] != TO)
        return 0;


        if(TP->query_prop(SIT_LEZACY) || TP->query_prop(SIT_SIEDZACY))
        {
            write("Najpierw wsta^n!\n");
            return 1;
        }

        if(TP->query_old_fatigue() <= 35)
        {
           write("Nie masz ju� na to si^ly.\n");
           return 1;
        }

        write("Szukasz dogodnego podej^scia i rozpoczynasz wspinaczk^e.\n");
        saybb(QCIMIE(TP, PL_MIA) + " zaczyna wspina^c si^e "+
            "na most.\n");
        if(environment(TO)->pora_roku() == MT_ZIMA &&
           TP->query_skill(SS_CLIMB) <= 3)
            {
            clone_object(RUINY_OBIEKTY+"paraliz_wspinania_most_nie")
            ->move(TP);
                return 1;

            }
        if(environment(TO)->pora_roku() != MT_ZIMA &&
           TP->query_skill(SS_CLIMB) <= 2)
        {
            clone_object(RUINY_OBIEKTY+"paraliz_wspinania_most_nie")
            ->move(TP);
            return 1;
        }
            if(environment(TO)->pora_roku() == MT_ZIMA &&
           TP->query_skill(SS_CLIMB) >= 4 &&
           TP->query_skill(SS_CLIMB) <= 6)
            {
                clone_object(RUINY_OBIEKTY+ 
                    "paraliz_wspinania_most_prawie")->move(TP);
                return 1;
            }
        if(environment(TO)->pora_roku() != MT_ZIMA &&
           TP->query_skill(SS_CLIMB) >= 3 &&
           TP->query_skill(SS_CLIMB) <= 5)
        {
            clone_object(RUINY_OBIEKTY+"paraliz_wspinania_most_prawie")
            ->move(TP);
                return 1;

        }
        

    clone_object(RUINY_OBIEKTY+"paraliz_wspinania_most")->move(TP);
    return 1;
}

public int
is_most_z_ruin()
{
    return 1;
}
