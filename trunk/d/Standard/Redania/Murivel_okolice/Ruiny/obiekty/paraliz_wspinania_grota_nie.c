/**
 * W^aw^oz na p^olnoc od traktu Rinde - Murivel
 *
 * autor Cairell
 * opisy Tabakista
 * data   2009
 */
#pragma strict_types
#include <exp.h>
#include <macros.h>
#include <ss_types.h>
#include "dir.h"

inherit "/std/paralyze";

static int alarm_id;

void
create_paralyze()
{
    set_name("wspinasie");
    set_stop_verb("przesta^n");
    set_stop_message("Przestajesz si^e wspina^c.\n");
    set_fail_message("Jeste� teraz zaj^et"+TP->koncowka("y","a") +
        "wspinaniem si^e. Musisz "+
        "przesta^c by m^oc zrobi^c, to co chcesz.\n");

    set_finish_object(this_object());
    set_finish_fun("zatrzymanie");
    set_remove_time(5);
    setuid();
    seteuid(getuid());
}

int
zatrzymanie(object player)
{
    write("Starasz si^e wyszuka^c palcami zag^l^ebienia w skale by pokona^c "+
        "^scian^e, lecz wszelkie wysi^lki spe^lzaj^a na niczym i po chwili "+
        "zsuwasz si^e bezsilnie na dno w^awozu.\n");
    saybb(QCIMIE(player, PL_MIA) + " stara si^e wyszuka^c palcami "+
        "zag^l^ebienia w skale by pokona^c ^scian^e, lecz wszelkie wysi^lki "+
        "spe^lzaj^a na niczym i po chwili zsuwa si^e bezsilnie na dno "+
        "w^awozu.\n");
    player->add_old_fatigue(-40);
    player->increase_ss(SS_CLIMB,EXP_WSPINAM_NIEUDANY_CLIMB);
    player->increase_ss(SS_STR,EXP_WSPINAM_NIEUDANY_CLIMB_STR);
    player->increase_ss(SS_DEX,EXP_WSPINAM_NIEUDANY_CLIMB_DEX);
    player->increase_ss(SS_CON,EXP_WSPINAM_NIEUDANY_CLIMB_CON);
    remove_object();

    return 1;
}
