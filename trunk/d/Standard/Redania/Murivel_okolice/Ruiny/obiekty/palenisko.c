/**
 * Ruiny na p^olnoc od traktu Rinde - Murivel
 *
 * autor Cairell
 * opisy Tabakista
 * data   2008
 */

#pragma save_binary
#pragma strict_types
inherit "/lib/light.c";
inherit "/std/lampa.c";
#include <object_types.h>
#include <stdproperties.h>
#include <wa_types.h>
#include <macros.h>

string dlugi_opis();
//private int Light_Strength, Time_Left=MAXINT;

void
create_lamp()
{
    ustaw_nazwe("palenisko");

    add_prop(OBJ_I_WEIGHT, 50000);
    add_prop(OBJ_I_VOLUME, 35000);
    add_prop(OBJ_M_NO_SELL, 1);
    add_prop(OBJ_I_NO_GET, "To palenisko jest tutaj wymurowane, nie da si^e "+
        "go podnie^s^c.\n");
    
    set_type(O_INNE);

    set_long(&dlugi_opis());

    set_time(MAXINT);
    set_strength(4);
    set_value(13);
    set_time_left(MAXINT);

    config_light();
}

string dlugi_opis()
{
    string str;

    str ="Palenisko powsta^lo poprzez niezbyt wprawne, lecz solidne "+
        "po^aczenie zapraw^a kilkunastu r^o^znej wielko^sci kamieni. "+
        "Wewn^atrz tego wapiennego kr^egu ";
        
   	if(this_object()->query_prop(OBJ_I_LIGHT))
    {
        str+="znajduje si^e spore ognisko, kt^orego p^lomienie "+
            "roz^swietlaj^a mrok pe^lzaj^ac po grubych, smolnych polanach. ";
    }
    else
    {
        str+="le^z^a jeszcze ciep^le, mocno ju^z poczernia^le, smolne polana. ";
    }
    str+="\n";
    return str;  
}
