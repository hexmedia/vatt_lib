/**
 * Jaskinie na p^olnoc od traktu Rinde - Murivel
 *
 * autor Cairell
 * opisy Tabakista
 * data   2008
 */
#pragma strict_types
#include <exp.h>
#include <macros.h>
#include <ss_types.h>
#include <sit.h>
#include "dir.h"

inherit "/std/paralyze";

void
create_paralyze()
{
    set_name("paraliz");
    set_stop_message("");
    set_fail_message("Jeste^s niesion"+TP->koncowka("y","a") +
        " gwa^ltownym pr^adem. Musisz skupi^c ca^l^a uwag^e by cho^c co "+
        "chwila zaczerpn^a^c powietrza.\n");
    set_finish_object(this_object());
    set_finish_fun("koniec_wycieczki");

    set_remove_time(15+random(10));
    setuid();
    seteuid(getuid());
}

void koniec_wycieczki(object player)
{
    int a;

    TP->add_fatigue(1500+random(1000));
    //FIXME wlaczyc, jak bedzie heal_hp dzialac
    /*for(i = 0; i < 5 ; i++) 
    { 
        a = rand(10); 
        switch (a) 
        { 
            case 0: heal_hp(HA_L_REKA, 100, 10); break;
            case 1: heal_hp(HA_L_REKA, 100, 10); break;
            case 2: heal_hp(HA_R_REKA, 100, 10); break;            
            case 3: heal_hp(HA_R_REKA, 100, 10); break;
            case 4: heal_hp(HA_L_NOGA, 100, 10); break;
            case 5: heal_hp(HA_R_NOGA, 100, 10); break;
            case 6: heal_hp(HA_GLOWA, 100, 10); break;
            case 7: heal_hp(HA_PLECY, 100, 10); break;
            case 8: heal_hp(HA_PLECY, 100, 10); break;
            case 9: heal_hp(HA_PLECY, 100, 10); break;
        } 
    }*/
    saybb(QCIMIE(player,PL_MIA)+" przep^lywa obok walcz^ac z pr^adem i co "+
        "chwila uderzaj^ac o kamienie.\n");
    TP->move(RUINY_LOKACJE+"wawoz_10.c",1);
    saybb(QCIMIE(player,PL_MIA)+" Zostaje nagle wyrzucony przez tryskaj^acy "+
        "ze ska^ly wodospad. Bezw^ladne, ociekaj^ace krwi^a cia^lo "+
        "zatrzymuje si^e na kraw^edzi potoku.\n");
    
    
    if(random(2))
    {
        player->catch_msg(" Zakr^et podziemnego strumienia ciska tob^a o "+
            "skalny za^lom. Gdy uderzasz o niego g^low^a szum potoku nagle "+
            "ustaje. Jeszcze przez moment widzisz wype^lniaj^ac^a wszystko "+
            "czerwie^n po czym ^swiat^lo ga^snie.\n");
        clone_object("/std/paralize/ogluszenie")->move(TP);
    }
    else
    {
        player->catch_msg(" Zakr^et podziemnego strumienia ciska tob^a o "+
            "skalny za^lom. Ledwie ochraniasz g^low^e, gdy kt^ora^s z "+
            "kolejnych kaskad wyrzuca ci^e w powietrze. Po uderzeniu o "+
            "pod^lo^ze powoli u^swiadamiasz sobie, ^ze wszystko si^e "+
            "uspokoi^lo, a zamiast og^luszaj^acego szumu s^lycha^c tylko "+
            "szemranie niewielkiego wodospadu.\n");
        clone_object("/std/paralize/lezenie.c")->move(TP);
    }
    object przemoczenie = clone_object("/std/efekty/przemoczenie.c");
    przemoczenie->przemocz(TP);
    remove_object();
    return;
}
