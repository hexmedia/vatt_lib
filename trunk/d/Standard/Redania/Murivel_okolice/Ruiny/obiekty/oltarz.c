/**
 * Ruiny na p^olnoc od traktu Rinde - Murivel
 *
 * autor Cairell
 * opisy Tabakista
 * data   2008
 */
#pragma strict_types
inherit "/std/object.c";

#include <stdproperties.h>
#include <macros.h>
#include <pl.h>
#include <materialy.h>
#include <object_types.h>

void
create_object()
{
    ustaw_nazwe("o^ltarz");
    dodaj_przym("wapienny", "wapienni");

    add_prop(OBJ_I_WEIGHT, 50000);
    add_prop(OBJ_I_VOLUME, 35000);

    set_long("Du^zy wapienny blok s^lu^zy za podstaw^e dla blatu "+
        "wykonanego w ca^lo^sci z jednej marmurowej p^lyty. Ich boki "+
        "nosz^a liczne p^ekni^ecia, rysy i ^slady narz^edzi. Ten "+
        "kto ich u^zywa� oczy^sci� dok^ladnie o^ltarz z najmniejszych "+
        "nawet element^ow zdobie^n pozostawiaj^ac go^le, jasne "+
        "kamienie. Jedynie powierzchnia blatu pozosta�a g^ladka, "+
        "ciesz^ac oko delikatnymi, nieregularnymi wzorami minera^l^ow "+
        "zakl�tych w lekko po�yskuj�cym bloku kamienia. Zniszczenia "+
        "jednak pozwalaj^a co najwy^zej domy^sla^c jakiemu b^ostwu "+
        "sk^ladano tutaj ofiary.\n");
}

public int
is_oltarz_z_ruin()
{
    return 1;
}
