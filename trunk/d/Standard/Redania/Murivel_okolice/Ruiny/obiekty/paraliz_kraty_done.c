/**
 * W^aw^oz na p^olnoc od traktu Rinde - Murivel
 *
 * autor Cairell
 * opisy Tabakista
 * data   2008
 */
#pragma strict_types
#include <macros.h>
#include <ss_types.h>
#include "dir.h"

inherit "/std/paralyze";

static int alarm_id;

void
create_paralyze()
{
    set_name("wspinasie");
    set_stop_verb("przesta^n");
    set_stop_message("Przestajesz stara^c sie unie^s^c krat^e.\n");
    set_fail_message("Jeste� teraz zaj^et"+TP->koncowka("y","a") +
        "unoszeniem kraty, musisz "+
        "przesta^c by m^oc zrobi^c, to co chcesz.\n");

    set_finish_object(this_object());
    set_finish_fun("zatrzymanie");
    set_remove_time(5);
    setuid();
    seteuid(getuid());
}

int
zatrzymanie(object player)
{
    write("Zardzewia^le ^zelazo chrz^eszcz�c o mur powoli "+
        "ust^epuje, ukazuj^ac z pocz^atku w^ask^a, lecz "+
        "ju^z po chwili ca^lkiem spor^a szczelin^e pomi^edzy "+
        "pr^etami a brukiem dziedzi^nca. Zapieraj^ac si^e "+
        "pewnie trzymasz krat^e w tej pozycji jeszcze przez "+
        "chwil^e po czym pozwalasz jej opa^s^c z "+
        "og^luszaj^acym ^loskotem i tumanem rdzawego py^lu.\n");
    saybb("Zardzewia^le ^zelazo chrz^eszcz�c o mur powoli "+
        "ust^epuje, ukazuj^ac z pocz^atku w^ask^a, lecz "+
        "ju^z po chwili ca^lkiem spor^a szczelin^e pomi^edzy "+
        "pr^etami a brukiem dziedzi^nca. Zapieraj^ac si^e "+
        "pewnie "+QCIMIE(player,PL_MIA)+" trzyma krat^e w tej "+
        "pozycji jeszcze przez chwil^e po czym pozwala "+
        "jej opa^s^c z og^luszaj^acym ^loskotem i "+
        "tumanem rdzawego py^lu.\n");
    player->add_old_fatigue(-45);

    remove_object();

    return 1;
}
