/**
 * Ruiny na p^olnoc od traktu Rinde - Murivel
 *
 * autor Cairell
 * opisy Tabakista
 * data   2008
 */
#pragma strict_types
inherit "/std/object.c";

#include <stdproperties.h>
#include <macros.h>
#include <pl.h>
#include <cmdparse.h>
#include <ss_types.h>
#include <materialy.h>
#include <object_types.h>

void
create_object()
{
    ustaw_nazwe(("p^lyta"), PL_ZENSKI);
    dodaj_przym("kamienny", "kamienni");

    add_prop(OBJ_I_WEIGHT, 50000);
    add_prop(OBJ_I_VOLUME, 35000);

    set_long("Masywny blok piaskowca le^zy roztrzaskany na trzy "+
        "cz^e^sci, tu^z obok ziej^acego w posadzce otworu. Jedynie "+
        "jego powierzchnia jest, w odr^o^znieniu od pod^logi, "+
        "bardziej chropowata, tak jakby p�yta zosta^la odwr^ocona, "+
        "lub odrzucona, a nie tylko uniesiona podczas otwierania.\n");
}

public int
is_plyta_z_ruin()
{
    return 1;
}