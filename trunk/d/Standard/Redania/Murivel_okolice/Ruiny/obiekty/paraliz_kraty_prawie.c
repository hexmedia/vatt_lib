/**
 * W^aw^oz na p^olnoc od traktu Rinde - Murivel
 *
 * autor Cairell
 * opisy Tabakista
 * data   2008
 */
#pragma strict_types
#include <macros.h>
#include <ss_types.h>
#include "dir.h"

inherit "/std/paralyze";

static int alarm_id;

void
create_paralyze()
{
    set_name("wspinasie");
    set_stop_verb("przesta^n");
    set_stop_message("Przestajesz stara^c sie unie^s^c krat^e.\n");
    set_fail_message("Jeste� teraz zaj^et"+TP->koncowka("y","a") +
        "unoszeniem kraty, musisz "+
        "przesta^c by m^oc zrobi^c, to co chcesz.\n");

    set_finish_object(this_object());
    set_finish_fun("zatrzymanie");
    set_remove_time(5);
    setuid();
    seteuid(getuid());
}

int
zatrzymanie(object player)
{
    write("Zardzewia�e ^zelazo nie chce ust^api^c szoruj�c o mur. "+
        "Dopiero zbieraj^ac wszystkie si^ly dajesz rad^e unie^s^c "+
        "j^a na kilka cali ponad bruk dziedzi^nca. Zapieraj^ac "+
        "si^e rozpaczliwie trzymasz krat^e w tej pozycji jeszcze "+
        "przez kr^otk^a chwil^e po czym pozwalasz jej opa^s^c z "+
        "og^luszaj^acym ^loskotem i tumanem rdzawego py^lu.\n");
    saybb("Zardzewia�e ^zelazo nie chce ust^api^c szoruj�c o mur. "+
        "Dopiero zbieraj^ac wszystkie si^ly "+
        QCIMIE(player, PL_MIA) + " daje rad^e unie^s^c "+
        "j^a na kilka cali ponad bruk dziedzi^nca. Zapieraj^ac "+
        "si^e rozpaczliwie trzyma krat^e w tej pozycji jeszcze "+
        "przez kr^otk^a chwil^e po czym pozwala jej opa^s^c z "+
        "og^luszaj^acym ^loskotem i tumanem rdzawego py^lu\n");
    player->add_old_fatigue(-40);

    remove_object();

    return 1;
}
