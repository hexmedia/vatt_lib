/**
 * Ruiny na p^olnoc od traktu Rinde - Murivel
 *
 * autor Cairell
 * opisy Tabakista
 * data   2008
 */
#pragma strict_types
inherit "/std/container";

#include <stdproperties.h>
#include <macros.h>
#include <pl.h>
#include <cmdparse.h>
#include <ss_types.h>
#include <materialy.h>
#include <object_types.h>


void
create_container()
{
    ustaw_nazwe("sarkofag");
    dodaj_przym("du^zy", "duzi");

    make_me_sitable("na","na p^lycie sarkofagu",
        "z sarkofagu",3);

    add_prop(OBJ_I_WEIGHT, 300000);
    add_prop(OBJ_I_VOLUME, 400000);
    add_prop(CONT_I_DONT_SHOW_CONTENTS, 1);

    add_subloc("na");
    add_subloc(({"sarkofag"}));
    add_subloc_prop("na", SUBLOC_I_TYP_W, 1);
    add_subloc_prop("na", CONT_I_MAX_WEIGHT, 500);	

    set_long("Miejsce poch^owku zajmuj^ace ^srodek krypty jest "+
        "zdecydowanie wi�ksze ni^z te w niszach. Wapienne bloki "+
        "s^a g^ladkie, nie poznaczone jakimikolwiek zdobieniamia"+
        ", a opiero wierzchnia p^lyta przykuwa uwag^e. "+
        "Wyrze^xbiono na niej naturalnej wielko^sci ^spi�cego "+
        "rycerza w pe^lnej zbroi, z tarcz� z^lo^zon� na piersi. "+
        "Herb na niej przedstawia trzy gwiazdy na g^ladkim polu. "+
        "Ponad n^a, pod uniesion^a przy^lbic� he^lmu znajduje si^e "+
        "wykute oblicze zmar^lego. Wi�ksza cz^e^s^c twarzy "+
        "przes^loni^eta jest bujn� brod^a opadaj^ac^a a� na "+
        "kamienny ryngraf. Poza ni^a zamkni^ete oczy, niskie "+
        "czo^lo i krzywy, wyra^xnie niegdy^s z^lamany nos "+
        "obiega sie^c g^l^ebokich zmarszczek.\n");
}

public int
is_sarkofag_z_ruin()
{
    return 1;
}
