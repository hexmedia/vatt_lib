/**
 * W^aw^oz na p^olnoc od traktu Rinde - Murivel
 *
 * autor Cairell
 * opisy Tabakista
 * data   2008
 */
#include "dir.h"
inherit "/std/object.c";
#include <pl.h>
#include <stdproperties.h>
#include <macros.h>
#include <filter_funs.h>
#include <object_types.h>
#include <ss_types.h>
#include <sit.h>
#include <mudtime.h>
#include <cmdparse.h>

int czy_odsuniety_glaz = 0;
string dlugi_opis();
int odsun(string str);
int dosun(string str);
int query_czy_odsuniety_glaz();

create_object()
{
    ustaw_nazwe("g^laz");
    dodaj_przym("wielki", "wielcy");
    dodaj_przym("szary", "szarzy");

    set_long(&dlugi_opis());

    add_prop(OBJ_I_WEIGHT, 5000000);
    add_prop(OBJ_I_VOLUME, 5000000);
    add_prop(OBJ_I_VALUE, 1);
    add_prop(OBJ_M_NO_SELL, 1);
    add_prop(OBJ_I_NO_GET, "Ogromny g^laz jest za du^zy "+
        "by go po prostu wzi^a^c.\n");
    add_prop(make_me_sitable("na", "na g^lazie", "z g^lazu", 2));

    set_type(O_INNE);
    seteuid(getuid());
}

string dlugi_opis()
{
    string st;

    st ="Jedna z bezkszta^ltnych bry^l wapienia ";
    
    if(czy_odsuniety_glaz == 0)
    {
         st +="^sci^sle wklinowana jest w "+
             "niewielkie zag^lebienie pod stromym stokiem w^awozu. ";
    }
    else
    {
        st +="le^zy bardzo blisko zag^l^ebienia w stromym stoku w^awozu, "+
            "w kt^orym widnieje niewielki, czarny otw^or jaskini "+
            "ziej^acy nieprzyjemnym, przenikaj^acym ch^lodem. ";
    }
    if (ENV(TO)->pora_roku() == MT_ZIMA)
    {
        st += "G^laz pokrywa siwy szron, skrz^acy si^e nieznacznie ";
    }
    else
    {
        st += "Glaz pokrywa wilgo^c, blyszcz^aca nieznacznie ";
    }
    
    if(ENV(TO)->jest_dzien() == 1)
    {
        st +="w s^labych, ledwie tu si^egaj^acych promieniach s^lo^nca. "+
            "Na jego boku oraz na ska^lach na lewo od niego widniej^a liczne "+
            "^slady odprysk^ow i zarysowa^n, jakie powstaj^a gdy kamie^n "+
            "szoruje o kamie^n.";
    }
    else
    {
        st +="w s^labym blasku, mimo ^ze o tej porze niemal niemal nie "+
            "dociera tutaj ^swiat^lo. ";
    }
    
    st+="\n";
    return st;
}

init()
{
    ::init();
    add_action(odsun, "odsu^n"); 
    add_action(dosun, "dosu^n");
    add_action(dosun, "zasu^n");

}

int odsun(string str)
{
    object *ob1;
    notify_fail("Co chcesz odsun^a^c?\n");
    
    if(!str)
        return 0;
    
    if (!parse_command(str, AI(environment(this_player())),
        "%i:" + PL_BIE, ob1))
        return 0;
    
    ob1 = NORMAL_ACCESS(ob1, 0, 0 );
    
    if (ob1[0] != TO)
        return 0;
    
   
        if(!HAS_FREE_HANDS(TP))
        {
            write("Musisz mie^c obie d^lonie wolne, aby m^oc to zrobi^c.\n");
            return 1;
        }
        
        if(TP->query_prop(SIT_LEZACY) || TP->query_prop(SIT_SIEDZACY))
        {
            write("Najpierw wsta^n!\n");
            return 1;
        }

        if(TP->query_old_fatigue() <= 40)
        {
           write("Nie masz ju^z na to si^ly.\n");
           return 1;
        }
        
        if(!query_czy_odsuniety_glaz())
        {

	    clone_object(RUINY_OBIEKTY+"paraliz_odsuwania")->move(TP);
            TP->add_old_fatigue(-10);
	    TP->add_prop(ODSUWAM_GLAZ,1);
	    //taka czynnosc ujawnia gracza:
	if(TP->query_prop(OBJ_I_HIDE))
		TP->command("ujawnij sie");
	    //dodajmy jeszcze opisik w shorcie gracza:
	    TP->add_prop(LIVE_S_EXTRA_SHORT," odsuwaj^ac"+TP->koncowka("y","a")+" g^laz");
        write("Obejmujesz g^laz, zapierasz "+
            "si^e pewnie o ziemi^e po czym napierasz na niego z "+
            "ca^lych si^l.\n");
	    saybb(QCIMIE(TP, PL_MIA) + " obejmuje g^laz, zapiera "+
                    "si^e pewnie o ziemi^e po czym napiera na niego z "+
                    "ca^lych si^l.\n");
            return 1;
            
        }
        else
        {
            write("G^laz jest ju^z odsuni^ety od wej^scia "+
                 "do jaskini tak daleko jak to mo^zliwe.\n");
            return 1;
        }
    
    return 1;
}

int dosun(string str)
{
    object *ob1;
    
    
    notify_fail("Co chcesz dosun^a^c?\n");
    
    if(!str)
        return 0;
    
    if (!parse_command(str, AI(environment(this_player())),
        " %i:" + PL_CEL, ob1))
        return 0;
    
    ob1 = NORMAL_ACCESS(ob1, 0, 0 );
    
    if (ob1[0] != TO)
        return 0;
        
    if(!HAS_FREE_HANDS(TP))
        {
            write("Musisz mie^c obie d^lonie wolne, aby m^oc to zrobi^c.\n");
            return 1;
        }
        
        if(TP->query_prop(SIT_LEZACY) || TP->query_prop(SIT_SIEDZACY))
        {
            write("Najpierw wsta^n!\n");
            return 1;
        }

        if(TP->query_old_fatigue() <= 15)
        {
           write("Nie masz ju^z na to si^ly.\n");
           return 1;
        }
        
        if(czy_odsuniety_glaz == 1)
        {
            write("Opierasz stop^e o g^laz oraz zapieraj^ac si^e plecami o "+
                "jaki^s skalisty wyst^ep, jednym silnym pchni^eciem "+
                "sprawiasz, ^ze wielki kamie^n stacza si^e ca^lkowicie "+
                "przes^laniaj^ac wej^scie do jaskini.\n");
            saybb(QCIMIE(TP, PL_MIA) + " opierasz stop^e o g^laz oraz "+
                "zapieraj^ac si^e plecami o jaki^s skalisty wyst^ep, "+
                "jednym silnym pchni^eciem sprawiasz, ^ze wielki kamie^n "+
                "stacza si^e ca^lkowicie przes^laniaj^ac wej^scie do "+
                "jaskini.\n");
            TP->add_old_fatigue(-10); 
            czy_odsuniety_glaz = 0;
            find_object(RUINY_LOKACJE +"wawoz_7.c")->usun_wyjscie();

	    //usuwamy wyjscie z wewnetrznej lokacji wyjscie:
	    LOAD_ERR(RUINY_LOKACJE + "grota_3.c");
	    find_object(RUINY_LOKACJE + "grota_3.c")->usun_wyjscie();
            return 1;
        }
        else
        {
            write("G^laz jest ju^z ^sci^sle dosuni^ety do wej^scia "+
                         "do jaskini.\n");
            return 1;
        }
        
        
    return 1;
}

public int
is_glaz_z_ruin()
{
    return 1;
}

int
query_czy_odsuniety_glaz()
{
    return czy_odsuniety_glaz;
}
void
set_czy_odsuniety_glaz(int i)
{
   czy_odsuniety_glaz = i;
}
