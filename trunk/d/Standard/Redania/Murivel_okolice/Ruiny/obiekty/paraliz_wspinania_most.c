/**
 * W^aw^oz na p^olnoc od traktu Rinde - Murivel
 *
 * autor Cairell
 * opisy Tabakista
 * data   2008
 */
#pragma strict_types
#include <exp.h>
#include <macros.h>
#include <ss_types.h>
#include "dir.h"

inherit "/std/paralyze";

void
create_paralyze()
{
    set_name("wspinasie");
    set_stop_verb("przesta^n");
    set_stop_message("Przestajesz si^e wspina^c na most.\n");
    set_fail_message("Jeste� teraz zaj^et"+TP->koncowka("y","a") +
        "wspinaniem si^e na most. Musisz "+
        "przesta^c by m^oc zrobi^c, to co chcesz.\n");
    set_finish_object(this_object());
    set_finish_fun("koniec_wspinaczki");

    set_remove_time(6);
    setuid();
    seteuid(getuid());
}

void
koniec_wspinaczki(object player)
{
    player->catch_msg("Powoli, przytrzymuj^ac sie omsza^lych "+
        "filar^ow wygrzebujesz si^e na g^or^e i "+
        "przeskakujesz nisk^a, murowana balustrad^e.\n");
    saybb(QCIMIE(player,PL_MIA)+" niespiesznie odnajduje drog^e "+
        "na g^or^e, przytrzymuj^ac si^e ocala^lych filar^ow. "+
        "Po chwili znika za kraw^edzi^a murowanej balustrady.\n");
    player->add_old_fatigue(-45);
    player->increase_ss(SS_CLIMB,EXP_WSPINAM_UDANY_CLIMB);
    player->increase_ss(SS_STR,EXP_WSPINAM_UDANY_CLIMB_STR);
    player->increase_ss(SS_DEX,EXP_WSPINAM_UDANY_CLIMB_DEX);
    player->increase_ss(SS_CON,EXP_WSPINAM_UDANY_CLIMB_CON);
    //powinno prowadzic na droge: las elfow - wawoz
    //player->move_living("M", GDZIES+"na_trakcie");
    saybb(QCIMIE(player,PL_MIA)+" przybywa z do^lu.\n");
    remove_object();
    return; 

}
