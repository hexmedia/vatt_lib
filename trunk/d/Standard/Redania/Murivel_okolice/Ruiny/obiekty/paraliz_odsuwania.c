/**
 * paraliz do grupowego odsuwania skaly na lokacji wawoz_7
 *
 * autor Vera
 * data   2009
 */
#pragma strict_types
#include <exp.h>
#include <macros.h>
#include <ss_types.h>
#include <filter_funs.h>
#include <stdproperties.h>
#include "dir.h"


inherit "/std/paralyze";

void
create_paralyze()
{
    set_name("odsuwaglaz");
    set_stop_verb("przesta^n");
    set_stop_message("Zaprzestajesz swych wysi^lk^ow w odsuwaniu g^lazu.\n");
    set_fail_message("Jeste^s teraz zaj^et"+TP->koncowka("y","a") +
        " odsuwaniem g^lazu. Musisz "+
        "przesta^c by m^oc zrobi^c, to co chcesz.\n");
    set_finish_object(this_object());
    set_finish_fun("koniec_odsuwania");
    set_remove_time(7+random(5));
    setuid();
    seteuid(getuid());
}

void
koniec_odsuwania(object player)
{
    //funkcja ta przechodzi tylko u tej osoby, ktorej pierwszy skonczy sie paraliz.
    //reszcie go automagicznie usuwamy
    object *odsuwajacy = filter(FILTER_LIVE(all_inventory(ENV(player))),&->query_prop(ODSUWAM_GLAZ));
    int suma_sil = 0;

    //jesli juz ktos wczesniej odsunal z ekipy odsuwajacej glaz, to nic nie robimy.
    //to sie nie powinno raczej nigdy zdazyc, bo jak ktos pierwszy zacznie odsuwac
    //to w jego paralizu wszystko sie dokona
    if(ENV(player)->query_czy_odsuniety_glaz())
	return;

    foreach(object x : odsuwajacy)
    {
	suma_sil += x->query_stat(SS_STR);

	//i odrazu usu�my reszcie odsuwaj�cych (pr�cz tego pierwszego playera) parali�
	if(x != player)
	{
	    foreach(object paraliz : filter(all_inventory(x),&->jestem_paralizem_odsuwania()))
		paraliz->remove_object();
	}
	//usu�my te� prop
	x->remove_prop(ODSUWAM_GLAZ);
	//i prop extra shorta:
	x->remove_prop(LIVE_S_EXTRA_SHORT);
	//dodajmy zm�czenie graczom odsuwaj�cym
	x->add_old_fatigue(-35-random(15));
    }

    if(suma_sil < 205 +random(20)) //dobry random nie jest z�y ;)
    {
        //wysy�amy wiadomo�� do wszystkich z tablicy 'odsuwajacy'
        odsuwajacy->catch_msg("Mimo i^z przez moment daje si^e s^lysze^c "+
                    "obiecuj^ace chrobotanie, musi"+ (sizeof(odsuwajacy)>1?"cie":"sz") +
		    " si^e jednak w ko^ncu "+
                    "podda^c gdy^z wysi^lki okazuj^a si^e bezowocne.\n");
        //a tu wiadomosc do calej reszty gapiow na lokacji, nie bioracych udzialu w akcji
        //tu juz trzeba osobno, bo jedni moga znac, a drudzy nie znac tych graczy
        foreach(object x : FILTER_LIVE(all_inventory(ENV(player))) - odsuwajacy)
        {
        x->catch_msg(
            FO_COMPOSITE_LIVE(odsuwajacy,x,PL_MIA)+" daj"+ (sizeof(odsuwajacy)>1?"^a":"e")+
            " za wygran^a i zrezygnowan"+(sizeof(odsuwajacy)>1?"i":player->koncowka("y","a","e"))+
            " zostawia"+(sizeof(odsuwajacy)>1?"j^a":"")+" g^laz w spokoju.\n");
        }
        return;
    }


    //tak sie dodaje opisy do przemieszczania sie:
    ENV(player)->add_exit(RUINY_LOKACJE + "grota_3.c", ({"jaskinia",
	    "do wewn^atrz jaskini, wychyla si^e zza kraw^edzi groty, po czym podci^aga i "+
	    "wchodzi do ^srodka","przybywa z zewn^atrz"})); 
    //dodajemy tez do wewnetrznej lokacji wyjscie:
    LOAD_ERR(RUINY_LOKACJE + "grota_3.c");
    find_object(RUINY_LOKACJE + "grota_3.c")->dodaj_wyjscie();
    find_object(RUINY_LOKACJE + "wawoz_7.c")->dodaj_wyjscie();

    ENV(player)->set_czy_odsuniety_glaz(1);
    LOAD_ERR(RUINY_OBIEKTY +"glaz.c"); //najpierw zaladujmy do pamieci obiekt lokacji
						//zanim cos bedziemy chcieli na niej wykonywac:
    find_object(RUINY_OBIEKTY +"glaz.c")->odsun(1);


    tell_room(ENV(player),"Po chwili ukazuje si^e niewielka "+
                    "szczelina w skale, okazuj^aca si^e by^c "+
                    "wej^sciem do jaskini.\n");

    foreach(object x : odsuwajacy)
    {
        x->increase_ss(SS_STR,EXP_ODSUWAM_UDANY_STR);
        x->increase_ss(SS_CON,EXP_ODSUWAM_UDANY_CON);
    }

    return; 
}
 
int
jestem_paralizem_odsuwania()
{
    return 1;
}
