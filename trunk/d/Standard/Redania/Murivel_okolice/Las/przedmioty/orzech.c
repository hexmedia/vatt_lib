/* Autor: Avard
   Opis : Edrain 
   Data : 4 sierpnia 2010 */
   
#include <macros.h>
#include <stdproperties.h>

inherit "/std/food";

void
create_food()
{
	ustaw_nazwe("orzech");
	dodaj_przym("ma�y","mali");
	set_amount(5);
	add_prop(OBJ_I_VOLUME,5);
	add_prop(OBJ_I_WEIGHT,5);
	set_decay_time(90000);
	set_long("Ma�y orzeszek, w drobnej kolczastej skorupce. Suche pokrycie "+
	"�atwo mo�e p�kn�� w d�oniach ukazuj�c smaczne i syc�ce nasiono.\n");
}