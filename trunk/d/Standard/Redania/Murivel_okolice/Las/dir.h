
#include "/d/Standard/Redania/dir.h"

#include <pogoda.h>

#define FATIGUE                     6
#define LAS_ELFOW ("/d/Standard/Redania/Murivel_okolice/Las/")
#define TRAKT ("/d/Standard/Redania/Murivel_okolice/Trakt/")
#define LAS_ELFOW_STD (LAS_ELFOW + "std/")

#define LAS_ELFOW_GESTY (LAS_ELFOW_STD + "las_gesty.c")
#define LAS_ELFOW_MROCZNY (LAS_ELFOW_STD + "las_mroczny.c")
#define LAS_ELFOW_TRAKT (LAS_ELFOW_STD + "las_trakt.c")
#define LAS_ELFOW_RZEKA (LAS_ELFOW_STD + "las_rzeka.c")
#define LAS_ELFOW_BAGIENKO (LAS_ELFOW_STD + "las_bagienko.c")

#define LAS_ELFOW_LOKACJE (LAS_ELFOW + "lokacje/")
#define LAS_ELFOW_NPC (LAS_ELFOW + "npc/")
#define LAS_ELFOW_PRZEDMIOTY (LAS_ELFOW + "przedmioty/")

#define TRAKT_LOKACJE (TRAKT + "lokacje/")
