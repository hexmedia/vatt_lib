#include <stdproperties.h>
#include <mudtime.h>
#include "dir.h"
inherit LAS_ELFOW_GESTY;

string dlugi_opis();

void
create_las()
{
    set_short("W^sr^od drzew g^esto zaro^sni^etego lasu");
    set_long(&dlugi_opis());
    add_exit(LAS_ELFOW_LOKACJE+ "las_21.c","w");
    add_exit(LAS_ELFOW_LOKACJE+ "las_23.c","se");
    add_exit(LAS_ELFOW_LOKACJE+ "las_45.c","ne");
    
    add_item(({"papro^c"}),"Niewysoka, lecz posiadaj^aca du^ze li^scie papro^"+
    "c ugina si^e do ziemi pod ci^e^zarem zal^a^zk^ow znajduj^acych si^e pod "+
    "listkami. Jej ^zywy, zielony kolor o^zywia miejsce. Mi^edzy jej listkami "+
    "wiele ma^ych stworzonek znalaz^lo schronienie.\n");
    if(!CZY_JEST_SNIEG(this_object()))
    {
        add_item("mech","Po�acie soczysto zielonych �ody�ek wznosz? si� "+
        "nieznacznie nad ziemi�. Mi�dzy drobnymi listkami gromadz? "+
        "du�e ilo?ci wody, przez co ca�o?� jest wyj?tkowo wilgotna. "+
        "Ziemia zas�ana tymi ro?linkami wygl?da jakby by�a pokryta "+
        "naturalnym dywanem.\n");
    }
}
string dlugi_opis()
{
    string str;
    if(jest_dzien() == 1)
    {
    if(CZY_JEST_SNIEG(this_object()))
    {
        str="Znajdujesz si^e w jasnym, ale bardzo g^estym lesie, gdzie poskr"+
        "^ecane pnie wi^az^ow z licznymi bruzdami ciekawi^a sw^a niezwyk^lo^s"+
        "ci^a, a  rozga^l^ezione d^eby i strzeliste sosny stanowi^ace swoje p"+
        "rzeciwie^nstwo uzupe^lniaj^a flor^e lasu. Ca^lo^sci dope^lniaj^a lic"+
        "zne krzewy - leszczyna, ja^lowiec, czy czeremcha obecnie bez li^sci "+
        "ale z resztkami owoc^ow, kt^ore stanowi^a po^zywienie ptak^ow  w zim"+
        "^e. Ca^lo^sci dope^lniaj^a liczne krzewy strasz^ace bezlistnymi rami"+
        "onami. Runo pokryte jest obecnie grub^a warstw^a bia^lego ^sniegu, k"+
        "t^ory skrzy si^e w ^swietle t^eczowymi iskierkami dope^lniaj^ac wra^"+
        "zenia ba^sniowo^sci.";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str="Znajdujesz si^e w jasnym, ale mimo to bardzo g^estym lesie, gdz"+
        "ie poskr^ecane pnie wi^az^ow z licznymi bruzdami ciekawi^a sw^a niez"+
        "wyk^lo^sci^a. Rozga^l^ezione d^eby i strzeliste sosny stanowi^ace sw"+
        "oje przeciwie^nstwo uzupe^lniaj^a flor^e lasu. Ca^lo^sci dope^lniaj^"+
        "a liczne krzewy - leszczyna, ja^lowiec, czy czeremcha. Runo, g^esto "+
        "usiane k^epkami fio^lk^ow i zawilc^ow, a przede wszystkim mchem, pap"+
        "rociami, wrzosami i dzwonkami le^snymi, ^ludzi swym pi^eknem i spraw"+
        "ia wra^zenie, ^ze miejsce to jest magiczne. Ca^ly las topi si^e w ja"+
        "snym ^swietle i wydaje si^kby t^etni^l w^lasnym, wewn^etrznym ^zycie"+
        "m. Liczne ptaki ^spiewaj^ace w ga^l^eziach, oraz ma^le zwierz^atka, "+
        "zamieszkuj^ace najni^zsze partie, nape^lniaj^a las ^zyciem i harmoni"+
        "^a.";
    }
    else if(pora_roku() == MT_LATO)
    {
        str="Znajdujesz si^e w jasnym, ale mimo to bardzo g^estym lesie, gdz"+
        "ie poskr^ecane pnie wi^az^ow z licznymi bruzdami ciekawi^a sw^a niez"+
        "wyk^lo^sci^a. Rozga^l^ezione d^eby i strzeliste sosny stanowi^ace sw"+
        "oje przeciwie^nstwo uzupe^lniaj^a flor^e lasu. Ca^lo^sci dope^lniaj^"+
        "a liczne krzewy - leszczyna, ja^lowiec, czy czeremcha. Runo, g^esto "+
        "usiane k^epkami fio^lk^ow i zawilc^ow, a przede wszystkim mchem, pap"+
        "rociami, wrzosami i dzwonkami le^snymi, ^ludzi swym pi^eknem i spraw"+
        "ia wra^zenie, ^ze miejsce to jest magiczne. Ca^ly las topi si^e w ja"+
        "snym ^swietle i wydaje si^kby t^etni^l w^lasnym, wewn^etrznym ^zycie"+
        "m. Liczne ptaki ^spiewaj^ace w ga^l^eziach, oraz ma^le zwierz^atka, "+
        "zamieszkuj^ace najni^zsze partie, nape^lniaj^a las ^zyciem i harmoni"+
        "^a.";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str="Znajdujesz si^e w jasnym, ale bardzo g^estym lesie, gdzie poskr"+
        "^ecane pnie wi^az^ow z licznymi bruzdami ciekawi^a sw^a niezwyk^lo^s"+
        "ci^a, a  rozga^l^ezione d^eby i strzeliste sosny stanowi^ace swoje p"+
        "rzeciwie^nstwo uzupe^lniaj^a flor^e lasu. Ca^lo^sci dope^lniaj^a lic"+
        "zne krzewy - leszczyna, ja^lowiec, czy czeremcha oraz runo g^esto us"+
        "iane li^s^cmi i szyszkami, a przede wszystkim mchem, paprociami, wrz"+
        "osami i dzwonkami le^snymi dope^lnia wra^zenia ba^sniowo^sci.  Ca^ly"+
        "las topi si^e w jasnym ^swietle s^lo^nca i wydaje jakby t^etni^l wew"+
        "n^etrznym ^zyciem. Liczne ptaki ^spiewaj^ace w ga^l^eziach, oraz ma^"+
        "le zwierz^atka runa nape^lniaj^a las ^zyciem i harmoni^a.";
    }
    }
    if(jest_dzien() == 0)
    {
    if(CZY_JEST_SNIEG(this_object()))
    {
        str="Znajdujesz si^e w jasnym, ale bardzo g^estym lesie, gdzie poskr"+
        "^ecane pnie wi^az^ow z licznymi bruzdami ciekawi^a sw^a niezwyk^lo^s"+
        "ci^a, a  rozga^l^ezione d^eby i strzeliste sosny stanowi^ace swoje p"+
        "rzeciwie^nstwo uzupe^lniaj^a flor^e lasu. Ca^lo^sci dope^lniaj^a lic"+
        "zne krzewy strasz^ace bezlistnymi ramionami. Runo pokryte jest obecn"+
        "ie grub^a warstw^a bia^lego ^sniegu, kt^ory w srebrzystym ^swietle k"+
        "si^e^zyca skrzy si^e t^eczowymi iskierkami dope^lniaj^ac wra^zenia b"+
        "a^sniowo^sci";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str="Znajdujesz si^e w jasnym, ale mimo to bardzo g^estym lesie, gdz"+
        "ie poskr^ecane pnie wi^az^ow z licznymi bruzdami ciekawi^a sw^a niez"+
        "wyk^lo^sci^a. Rozga^l^ezione d^eby i strzeliste sosny stanowi^ace sw"+
        "oje przeciwie^nstwo uzupe^lniaj^a flor^e lasu. Ca^lo^sci dope^lniaj^"+
        "a krzewy, kt^orych li^scie pob^lyskuj^a w ^swietle ksi^e^zyca. Runo,"+
        "usiane mchem, drobnymi ro^slinami i paprociami, ^ludzi swym pi^eknem"+
        "i sprawia wra^zenie, ^ze miejsce to jest magiczne. Ca^ly las topi si"+
        "^e w srebrzystym ^swietle ksi^e^zyca i wydaje si^e jakby t^etni^l w^"+
        "lasnym, wewn^etrznym ^zyciem.";
    }
    else if(pora_roku() == MT_LATO)
    {
        str="Znajdujesz si^e w jasnym, ale mimo to bardzo g^estym lesie, gdz"+
        "ie poskr^ecane pnie wi^az^ow z licznymi bruzdami ciekawi^a sw^a niez"+
        "wyk^lo^sci^a. Rozga^l^ezione d^eby i strzeliste sosny stanowi^ace sw"+
        "oje przeciwie^nstwo uzupe^lniaj^a flor^e lasu. Ca^lo^sci dope^lniaj^"+
        "a krzewy, kt^orych li^scie pob^lyskuj^a w ^swietle ksi^e^zyca. Runo,"+
        "usiane mchem, drobnymi ro^slinami i paprociami, ^ludzi swym pi^eknem"+
        "i sprawia wra^zenie, ^ze miejsce to jest magiczne. Ca^ly las topi si"+
        "^e w srebrzystym ^swietle ksi^e^zyca i wydaje si^e jakby t^etni^l w^"+
        "lasnym, wewn^etrznym ^zyciem.";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str="Znajdujesz si^e w jasnym, ale bardzo g^estym lesie, gdzie poskr"+
        "^ecane pnie wi^az^ow z licznymi bruzdami ciekawi^a sw^a niezwyk^lo^s"+
        "ci^a, a  rozga^l^ezione d^eby i strzeliste sosny stanowi^ace swoje p"+
        "rzeciwie^nstwo uzupe^lniaj^a flor^e lasu. Ca^lo^sci dope^lniaj^a lic"+
        "zne krzewy, kt^orych ga^l^ezie strasz^a ju^z prawie bezlistnymi rami"+
        "onami. Ca^ly las topi si^e w srebrzystym ^swietle ksi^e^zyca i wydaj"+
        "e jakby t^etni^l wewn^etrznym ^zyciem.";
    }
    }
    str += "\n";
    return str;
}
