/**
 * Las Komanda
 *
 * autor Cairell
 * opisy Edrain & Aladriel
 * data  styczen 2009
 */

#include <stdproperties.h>
#include <mudtime.h>
#include "dir.h"
inherit LAS_ELFOW_RZEKA;

string dlugi_opis();

void
create_las()
{
    set_short("Zielony zagajnik.");
    set_long(&dlugi_opis());

    add_exit(LAS_ELFOW_LOKACJE+ "las_15.c","e");
    add_exit(TRAKT_LOKACJE+ "trakt_32.c","w");
}

string dlugi_opis()
{
    string str = ""; 
 
    if(pora_roku() == MT_ZIMA)
    {
        str += "Miejsce to by^lo niegdy^s ogrodem o czym ^swiadcz^a "+
            "krzewy r^o^z rosn^ace tu w du^zej liczbie. G^l^ebokie "+
            "zaspy ^snie^zne zas^laniaj^a delikatne kwiaty "+
            "broni^ac je przed zimnem i mrozem. Po dok^ladniejszym "+
            "przyjrzeniu si^e krzaczkom zauwa^zasz, ^ze kilka p^ed^ow "+
            "zosta^lo z do^lu okorowanych, najpewniej przez "+
            "przyci^sni^ete przez g^l^od zaj^ace.";
    }
    if(pora_roku() == MT_JESIEN)
    {
        str += "Miejsce to by^lo niegdy^s ogrodem o czym ^swiadcz^a "+
            "krzewy r^o^z rosn^ace tu w du^zych ilo^sciach. Wysoka "+
            "trawa, cz^e^sciowo zas^lania pi^ekne kwiaty, lecz "+
            "d^lugi brak piel^egnacji sprawi^l, ^ze ca^lkowicie "+
            "zdzicza^ly rosn^ac jak chc^a. Dooko^la unosi si^e "+
            "cudowny zapach jesiennych r^o^z, a lekki zefir "+
            "sprawia, ^ze z krzaczk^ow odrywaj^a si^e delikatne "+
            "p^latki wiruj^ac w powietrzu w ostatnim ta^ncu, "+
            "nim za^sciel� ziemi�...";
    }
    else //wiosna i lato
    {
        str += "Miejsce to by^lo niegdy^s ogrodem o czym "+
            "^swiadcz^a krzewy r^o^z rosn^ace tu w du^zych "+
            "ilo^sciach. Wysoka trawa, cz^e^sciowo zas^lania "+
            "pi^ekne kwiaty, lecz d^lugi brak piel^egnacji "+
            "sprawi^l, ^ze ca^lkowicie zdzicza^ly rosn^ac "+
            "jak chc^a. Dooko^la unosi si^e cudowny zapach r^o^z, "+
            "a lekki zefir sprawia, ^ze z krzaczk^ow odrywaj^a "+
            "si^e delikatne p^latki wiruj^ac w powietrzu w "+
            "ostatnim ta^ncu, nim za^sciel^a ziemi^e.";
    }

str+="\n";
return str;
}


public string
exits_description() 
{
    return "Drog^e mi^edzy drzewami mo^zna odnale^s^c ruszaj^ac "+
        "na wsch^od, za^s na zachodzie majaczy mi^edzy drzewami "+
        "trakt.\n";
}