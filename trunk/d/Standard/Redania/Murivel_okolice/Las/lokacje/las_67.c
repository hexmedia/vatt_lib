/* Las komanda
   Autor: Avard 
   Opis : Edrain */

#include <stdproperties.h>
#include <macros.h>
#include <mudtime.h>
#include "dir.h"
inherit LAS_ELFOW_GESTY;

string dlugi_opis();

void
create_las()
{
    set_short("Serce puszczy");
    set_long(&dlugi_opis());
    add_exit(LAS_ELFOW_LOKACJE+ "las_66.c","w");
    add_exit(LAS_ELFOW_LOKACJE+ "las_68.c","e");
}

string dlugi_opis()
{
    string str = "";
    
    if(jest_dzien()) //Dzie�
    {
        str += " Brak krzew�w powoduje, �e puszcza ta wygl�da niesamowicie "+
        "ba�niowo. Strzeliste, g�adkie i pozbawione ga��zi pnie drzew rosn� "+
        "g�sto i r�wno, niemal�e idealnie, tak jakby kto� specjalnie sadzi� "+
        "je w rz�dkach. Du�e drzewa zwie�czone s� wysoko osadzonymi "+
        "koronami, zatrzymuj�cymi wi�kszo�� �wiat�a. Tylko gdzieniegdzie "+
        "dostrzec mo�na nik�e wi�zki promieni, w kt�rych igraj� kurz i "+
        "py�ki, nadaj�c miejscu mrocznego wymiaru.";
        
    }
    else //Noc
    {
        str += "Brak krzew�w powoduje, �e puszcza ta wygl�da niesamowicie "+
        "ba�niowo. Strzeliste, g�adkie i pozbawione ga��zi pnie drzew rosn� "+
        "g�sto i r�wno, niemal�e idealnie, tak jakby kto� specjalnie sadzi� "+
        "je w rz�dkach. Du�e drzewa zwie�czone s� wysoko osadzonymi "+
        "koronami, zatrzymuj�cymi wi�kszo�� �wiat�a ksi�yca. Tylko "+
        "gdzieniegdzie dostrzec mo�na nik�e wi�zki srebrno fioletowych "+
        "promieni nadaj�cych miejscu mrocznego wymiaru.";
    }
    str += "\n";
    return str;
}
