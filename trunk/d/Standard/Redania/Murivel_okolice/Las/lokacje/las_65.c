/* Las komanda
   Autor: Avard 
   Opis : Edrain */

#include <stdproperties.h>
#include <macros.h>
#include <mudtime.h>
#include "dir.h"
inherit LAS_ELFOW_GESTY;

string dlugi_opis();

string opis_kwiatow();
string opis_fiolkow();
string opis_zawilcow();
string opis_dzwonkow();
string opis_konwalii();
string opis_wrzosow();
string opis_leszczyny();

int wez(string str);

void
create_las()
{
    set_short("Na skraju puszczy");
    set_long(&dlugi_opis());
    add_exit(LAS_ELFOW_LOKACJE+ "las_62.c","se");
    
    if(!CZY_JEST_SNIEG(this_object()))
    {
        add_item("mech","Po�acie soczysto zielonych �ody�ek wznosz? si� "+
        "nieznacznie nad ziemi�. Mi�dzy drobnymi listkami gromadz? "+
        "du�e ilo?ci wody, przez co ca�o?� jest wyj?tkowo wilgotna. "+
        "Ziemia zas�ana tymi ro?linkami wygl?da jakby by�a pokryta "+
        "naturalnym dywanem.\n");
    }
    add_item(({"zawilca","zawilce"}), &opis_zawilcow());
    add_item(({"dzwonek","dzwonki"}), &opis_dzwonkow());
    add_item(({"wrzos","wrzosy"}), &opis_wrzosow());
    add_item(({"fio^lka","fio^lki"}), &opis_fiolkow());
    add_item(({"konwalie"}), &opis_konwalii());
    add_item(({"kwiat","kwiaty"}), &opis_kwiatow());
    add_item(({"leszczyn�","krzak","krzew","krzak leszczyny",
        "krzew leszczyny"}), &opis_leszczyny());
}

public void
init()
{
    ::init();
    add_action(wez,"we�");
    add_action(wez,"zbieraj");
    add_action(wez,"zbierz");
}

string dlugi_opis()
{
    string str = "";
    if(jest_dzien())
    {
        if(CZY_JEST_SNIEG(this_object()))
        {
            str+="Promienie s�o�ca docieraj�ce w stron� ziemi, odbijaj� si� "+
            "od srebrzystych pni drzew zalewaj�c okolic� ostrym blaskiem. W "+
            "miejscu tym smuk�e buki mieszaj� si� z kr�pymi sylwetkami "+
            "krzew�w le�nych, pot�nymi d�bami i wysokimi wi�zami. Pod�o�e "+
            "pokryte jest zimny puchem �niegu, odbijaj�cym ostre �wiat�o "+
            "mro�nego dnia zimy.";
        }
        else
        {
            str+="Promienie s�o�ca docieraj�ce w stron� ziemi, odbijaj� si� "+
            "od srebrzystych pni drzew zalewaj�c okolic� ciep�ym blaskiem. W "+
            "miejscu tym smuk�e buki mieszaj� si� z kr�pymi sylwetkami "+
            "krzew�w le�nych, pot�nymi d�bami i wysokimi wi�zami. Pod�o�e "+
            "usiane r�nokolorowym kwieciem cieszy oczy swoj� �wie�o�ci�, a "+
            "odurzaj�cy zapach lasu powoduje zawroty g�owy. Bzyczenie owad�w "+
            "zapylaj�cych kwiaty przeplata si� z weso�ym �piewem ptak�w. I "+
            "tylko mroczny las majacz�cy w oddali napawa l�kiem.";
        }
    }
    else
    {
        if(CZY_JEST_SNIEG(this_object()))
        {
            str+="Blask ksi�yca docieraj�cego w stron� ziemi, odbija si� "+
            "od srebrzystych pni drzew zalewaj�c okolic� delikatnym, sino "+
            "fioletowym blaskiem. W miejscu tym smuk�e buki mieszaj� si� z "+
            "kr�pymi sylwetkami krzew�w le�nych, pot�nymi d�bami i wysokimi "+
            "wi�zami. Pod�o�e pokryte jest zimny puchem �niegu.";
        }
        else
        {
            str+="Blask ksi�yca docieraj�cego w stron� ziemi, odbija si� "+
            "od srebrzystych pni drzew zalewaj�c okolic� delikatnym, sino "+
            "fioletowym �wiat�em. W miejscu tym smuk�e buki mieszaj� si� z "+
            "kr�pymi sylwetkami krzew�w le�nych, pot�nymi d�bami i wysokimi "+
            "wi�zami. Pod�o�e usiane r�nokolorowym kwieciem cieszy oczy "+
            "swoj� �wie�o�ci�,a odurzaj�cy zapach lasu powoduje zawroty "+
            "g�owy. Bzyczenie owad�w zapylaj�cych kwiaty przeplata si� z "+
            "weso�ym �piewem ptak�w. I tylko mroczny las majacz�cy w oddali "+
            "napawa l�kiem.";
        }
    }
    str += "\n";
    return str;
}

string opis_kwiatow()
{
    string str;
    if (pora_roku() == MT_JESIEN)
    {
    str += "Bujne, zielono- czerwone li^scie tworz^a co^s na "+
    "kszta^lt sp^ojnej kuli, na kt^orej wierzchu znajduj^a "+
    "si^e liczne pozosta^lo^sci po niegdy^s pi^eknych "+
    "kwiatach.\n"; 
    return str;
    }
    else if(pora_roku() == MT_ZIMA)
    {
    return 0;
    }
    else
    {
    str += "Bujne, w^sciekle zielone li^scie tworz^a co^s na "+
    "kszta^lt sp^ojnej kuli, na kt^orej wierzchu znajduj^a "+
    "si^e liczne kwiaty o mlecznej barwie. Ka^zdy kwiat "+
    "ma potr^ojn^a warstw^e p^latk^ow, co czyni je "+
    "okazalszymi.\n"; 
    return str;
    }
}

string opis_zawilcow()
{
    string str;
    if (pora_roku() == MT_WIOSNA)
    {
    str += "Zawilec gajowy jest pojedynczym kwiatem o d^lugiej "+
    "delikatnej ^lody^zce wyrastaj^acej z k^l^acza. "+
    "Poszarpane listki, zazwyczaj tylko trzy "+
    "wyst^epuj^ace w ok^o^lkach i wygl^adaj^a jak szpony "+
    "jakiej^s zjawy. Aktualnie ka^zda ^lody^zka zwie^nczona "+
    "jest prze^slicznym bia^lym kwiatkiem, sk^ladaj^acym "+
    "si^e z pi^eciu lub siedmiu p^latk^ow. Soki tej "+
    "ro^sliny s^a silnie truj^ace i powoduj^a zapalenie "+
    "nerek i uk^ladu pokarmowego, wywo^luj^ac silne "+
    "wymioty i biegunk^e.\n"; 
    return str;
    }
    else
    {
    return 0;
    }
}

string opis_wrzosow()
{
    string str;
    if (pora_roku() == MT_JESIEN)
    {
    str = "Ta zimozielona dwupienna krzewinka o listkach "+
    "rozes^lanych po ziemi, p^lo^zy si^e g^esto "+
    "wsz^edzie dooko^la, tworz^ac wyciszaj^acy dla "+
    "krok^ow ciemnozielony dywan. Jej drobne, "+
    "igie^lkowate li^scie u^lo^zone s^a naprzeciw siebie "+
    "i sprawiaj^a mylne wra^zenie bardzo ubogich, "+
    "jednak s^a one tylko dodatkiem do pi^eknych "+
    "r^o^zowo- fioletowych kwiat^ow w pe^lni "+
    "rozkwitaj^acych wczesn^a jesieni^a, r^ownie "+
    "drobnych, a jednak zachwycaj^acych swym pi^eknem, "+
    "kt^ore maluje ^swiat w czasie gdy prawie wszelkie "+
    "inne ozdoby przyrody ju^z przekwit^ly.\n";
    return str;
    }
    else
    {
    return 0;
    }
}

string opis_dzwonkow()
{
    string str;
    if (pora_roku() == MT_LATO)
    {
    str = "Wiotkie ^lody^zki wznosz^a si^e kilkadziesi^at "+
    "centymetr^ow nad ziemi^e, ich ko^nce zwieszaj^a "+
    "si^e w stron^e pod^lo^za obci^a^zone okaza^lymi "+
    "kwiatami. Fioletowe, zros^le p^latki tworz^a kielich "+
    "z jasno^z^o^ltym ^srodkiem. Odurzaj^acy zapach "+
    "dzwonk^ow le^snych wabi owady.\n";
    return str;
    }
    else
    {
    return 0;
    }
}

string opis_fiolkow()
{
    string str;
    if (pora_roku() == MT_WIOSNA)
    {
    str = "P^lo^z^aca si^e po ziemi bylina jest teraz g^esto "+
    "upstrzona pi^eknie pachn^acymi fioletowymi "+
    "kwiatami. Delikatne kwiaty wyniesione s^a nad "+
    "ziemie na wysokich szypu^lkach, ka^zdy z nich "+
    "sk^lada si^e z pi^eciu p^latk^ow dw^och "+
    "skierowanych w g^or^e i trzech w d^o^l. "+
    "Zielone listki, kt^ore dodatkowo eksponuj^a "+
    "pi^ekno koloru fio^lk^ow, maj^a kszta^lt sercowaty "+
    "i s^a lekko karbowane od g^ory.\n";
    return str;
    }
    else if (pora_roku() == MT_LATO)
    {
    str = "Na p^lo^z^acej si^e po ziemi bylinie nie wida^c "+
    "ju^z ^slad^ow niedawno pi^eknie kwitn^acych, "+
    "drobnych p^latk^ow fio^lk^ow. Z wonnej ro^sliny "+
    "pozosta^ly ^lody^zki i ciemnozielone, sercowate "+
    "li^scie, kt^ore t^lumi^a wszelkie odg^losy "+
    "zwierz^at i podr^o^znych.\n";
    return str;
    }
    else return 0;
}
string
opis_konwalii()
{
    string str;
    if(pora_roku() == MT_WIOSNA || pora_roku() == MT_LATO)
    {
        str = " Ma�e kwiaty g�sto rosn� na ziemi. Na wiotkich przygi�tych "+
        "do ziemi �ody�kach wyrastaj� drobne �nie�nobia�y kwiatki. Ca�o�� "+
        "okryta jest pojedynczym soczy�cie zielonym li�ciem o r�wnoleg�ej "+
        "nerwacji.\n";
        return str;
    }
    else return 0;
}

string
opis_leszczyny()
{
    string str;
    if(CZY_JEST_SNIEG(this_object))
    {
        str = "est to niewysoka forma drzewiasta krzewu o �redniej wielko�ci, "+
        "kt�rego pie� w barwie szarobr�zowej do�� g�sto przetykany jest "+
        "bia�awymi przetchlinkami, kt�re s� jedynym elementem naruszaj�cym "+
        "niesamowit� g�adko�� kory. Obecnie na krzewie dostrzegasz tylko "+
        "pojedyncze, obumar�e li�cie, kt�re nie spad�y na jesieni, a teraz "+
        "trzymaj� si� tylko przez dzia�anie mrozu.\n";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str = "Jest to niewysoka forma drzewiasta krzewu o �redniej "+
        "wielko�ci, kt�rego pie� w barwie szarobr�zowej do�� g�sto "+
        "przetykany jest bia�awymi przetchlinkami, kt�re s� jedynym "+
        "elementem naruszaj�cym niesamowit� g�adko�� kory. Krzew ten bogaty "+
        "jest w do�� du�e, okr�g�awe li�cie, u nasady sercowate, kr�tko "+
        "zaostrzone, o barwie soczystej zieleni, aktualnie g�sto przetykany "+
        "br�zowo ��tymi kotami, kt�re zwisaj� z ga��zi wystawione na "+
        "dzia�anie wiatru rozsiewaj�cego py�ek po okolicy.\n";
    }
    else if(pora_roku() == MT_LATO)
    {
        str = "Jest to niewysoka forma drzewiasta krzewu o �redniej "+
        "wielko�ci, kt�rego pie� w barwie szarobr�zowej do�� g�sto "+
        "przetykany jest bia�awymi przetchlinkami, kt�re s� jedynym "+
        "elementem naruszaj�cym niesamowit� g�adko�� kory. Krzew ten bogaty "+
        "jest w do�� du�e, okr�g�awe li�cie, u nasady sercowate, kr�tko "+
        "zaostrzone, o barwie soczystej zieleni.\n";
    }
    else
    {  
        str = "Jest to niewysoka forma drzewiasta krzewu o �redniej "+
        "wielko�ci, kt�rego pie� w barwie szarobr�zowej do�� g�sto "+
        "przetykany jest bia�awymi przetchlinkami, kt�re s� jedynym "+
        "elementem naruszaj�cym niesamowit� g�adko�� kory. Krzew ten bogaty "+
        "jest w do�� du�e, okr�g�awe li�cie, u nasady sercowate, kr�tko "+
        "zaostrzone, o barwie ��tozielonej. Mi�dzy nimi dostrzec mo�na "+
        "pojedyncze orzeszki.\n";
    }
    return str;
}

int wez(string str)
{
    if(query_verb() ~= "we�")
        notify_fail("We� co?\n");
    if(query_verb() ~= "zbieraj")
        notify_fail("Zbieraj co?\n");
    if(query_verb() ~= "zbierz")
        notify_fail("Zbierz co?\n");
        
    if (!str) return 0;
    if (!strlen(str)) return 0;
    
    if(pora_roku() != MT_JESIEN)
        return 0;
    
    if(query_verb() ~= "we�")
    {
        if(!parse_command(str,environment(TP),"'orzech' / 'orzechy'"))
            return 0;
    }
    else 
    {
        if(!parse_command(str,environment(TP),"'orzechy'"))
            return 0;
    }
    write("Zaczynasz zbiera� orzechy.\n");
    saybb(QCIMIE(TP,PL_MIA)+" zaczyna zbiera� orzechy.\n");
    clone_object(LAS_ELFOW_PRZEDMIOTY+"paraliz_orzechy_leszczyny.c")->move(TP);
    return 1;
}