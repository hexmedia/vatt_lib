/**
 * Las Komanda
 *
 * autor Cairell
 * opisy Edrain & Aladriel
 * data  styczen 2009
 */

#include <stdproperties.h>
#include <mudtime.h>
#include "dir.h"
inherit LAS_ELFOW_RZEKA;

string dlugi_opis();
string opis_krzewu();

void
create_las()
{
    set_short("Lipowy lasek.");
    set_long(&dlugi_opis());
    add_exit(LAS_ELFOW_LOKACJE+ "las_5.c","w");
    add_exit(LAS_ELFOW_LOKACJE+ "bagno_4.c","n");
    add_exit(LAS_ELFOW_LOKACJE+ "las_7.c","e");
    add_exit(LAS_ELFOW_LOKACJE+ "las_8.c","s");
    add_exit(LAS_ELFOW_LOKACJE+ "las_9.c","se");

    add_item(({"krzewy", "krzew", "r^o^z^e"}), opis_krzewu());
}

string dlugi_opis()
{
    string str = ""; 
    
    str+="Panuje tu niezm^acona krzykami ludzi cisza. Jedyne, "+
        "co mo^zesz us^lysze^c, to odg^losy natury. W oddali sw^a "+
        "pie^s^n szumi rzeka, kt^orej wartki nurt brnie na prz^od "+
        "nie zwa^zaj^ac na up^lyw czasu, a korony drzew ";

    if (jest_dzien() == 1)
    {
        if(pora_roku() == MT_WIOSNA)
        {
            str += "rozbrzmiewaj^a trelami ptak^ow wy^spiewuj�cych "+
                "pie^sni na cze^s^c dziewiczej natury. Stare "+
                "leszczyny rosn^ace w tym miejscu ju� od wielu lat "+
                "sprawiaj^a, ^ze mo^zna tu zapomnie^c niemal o "+
                "ka^zdych troskach codziennego ^zycia. G^esta, "+
                "soczy^scie zielona i ^swie^za trawa porasta t^e "+
                "okolic^e. Pod konarami rozrastaj^a si^e krzewy "+
                "dzikiej r^o^zy, nasycaj^acej powietrze s^lodk^a "+
                "woni^a."; 
        }
        if(pora_roku() == MT_LATO)
        {
            str += "rozbrzmiewaj^a trelami ptak^ow wy^spiewuj�cych "+
                "pie^sni na cze^s^c dziewiczej natury. Stare "+
                "leszczyny rosn^ace w tym miejscu ju� od wielu lat "+
                "sprawiaj^a, ^ze mo^zna tu zapomnie^c niemal o "+
                "ka^zdych troskach codziennego ^zycia. G^esta, "+
                "soczy^scie zielona i ^swie^za trawa porasta t^e "+
                "okolic^e. Pod konarami rozrastaj^a si^e krzewy "+
                "dzikiej r^o^zy, nasycaj^acej powietrze s^lodk^a "+
                "woni^a."; 
        }
        if(pora_roku() == MT_JESIEN)
        {
            str += "rozbrzmiewaj^a trelami ptak^ow wy^spiewuj�cych "+
                "pie^sni na cze^s^c dziewiczej natury. Stare "+
                "leszczyny rosn^ace w tym miejscu ju^z od wielu "+
                "lat sprawiaj^a, �e mo^zna tu zapomnie^c niemal "+
                "o ka^zdych troskach codziennego ^zycia. G^esta, "+
                "zielona trawa, kt^ora powoli zmienia barw^e na "+
                "br^azow^a bujnie porasta okolice. Pod konarami "+
                "rozrastaj^a si^e krzewy jesiennej dzikiej r^o^zy, "+
                "nasycaj^acej powietrze s^lodk^a woni^a.";
        }
        if(pora_roku() == MT_ZIMA)
        {
            str += "rozbrzmiewaj^a trelami ptak^ow przewrzaskuj^acych "+
                "si^e w bitwie o ostatnie owoce jesieni, jedyny "+
                "pokarm w te d^lugie zimowe noce i zbyt kr^otkie "+
                "dni, aby znale^s^c cokolwiek do jedzenia. "+
                "Leszczyny jeszcze niedawno wydaj^ace pyszne "+
                "orzechy, teraz spowija g^esty puch ^sniegu, "+
                "chroni^acy nowe p^aczki przed przemarzni^eciem.";

        }
    }
    else //noc
    {
        str += "przes^laniaj^a granatowe, nocne niebo i tworz^a "+
            "doko�a setki wi^ekszych i mniejszych cieni.";
    }

str+="\n";
return str;
}

string opis_krzewu()
{
    string strr;

    strr = "";

    if(pora_roku() == MT_WIOSNA)
    {
        strr += "Dziko rosn^acy krzew dodaje uroku temu miejscu. "+
            "Bielutkie kwiaty wychylaj� spomi^edzy ciemnozielonych "+
            "li^sci swe dumne g^l^owki. Niekt^ore zdaj^a si^e dopiero "+
            "rozwija^c, inne s^a ju^z w pe^lni otwarte. Ga^l^azki "+
            "krzaku opr^ocz li^sci pokryte s^a tak^ze ostrymi "+
            "du^zymi kolcami."; 
    }
    if(pora_roku() == MT_LATO)
    {
        strr += "Dziko rosn^acy krzew dodaje uroku temu miejscu. "+
            "Bielutkie kwiaty wychylaj� spomi^edzy ciemnozielonych "+
            "li^sci swe dumne g^l^owki. Niekt^ore zdaj^a si^e dopiero "+
            "rozwija^c, inne s^a ju^z w pe^lni otwarte. Ga^l^azki "+
            "krzaku opr^ocz li^sci pokryte s^a tak^ze ostrymi "+
            "du^zymi kolcami."; 
    }
    if(pora_roku() == MT_JESIEN)
    {
        strr += "Dziko rosn^acy krzew dodaje uroku temu miejscu. "+
            "Bielutkie p^o^xnojesienne kwiaty wychylaj^a "+
            "spomi^edzy oliwkowo - br^azowych li^sci swe "+
            "dumne g^l^owki. Ga^l^azki krzaku opr^ocz li^sci "+
            "pokryte s^a tak�e ostrymi du^zymi kolcami.";
    }
    if(pora_roku() == MT_ZIMA)
    {
        strr += "Dziko rosn^acy krzew dzikiej r^o^zy okryty "+
            "jest teraz grub^a warst^a bielutkiego puchu, zdaj^acej "+
            "boni^c dost^epu do du^zych p^aczk^ow. W kilku miejscach "+
            "kora p^ed^ow zosta^la przy samej ziemi ogryziona, "+
            "najpewniej przez g^loduj^ace o tej porze roku zaj^ace.";
    }
    strr += "\n";
    return strr;
}

public string
exits_description() 
{
    return "Na p^o^lnocy wida^c niewielkie bagienko, "+
        "za^s na zachodzie, wschodzie, po^ludniu i po^ludniowym "+
        "wschodzie rozci^aga si^e dalsza cz^e^s^c lasu.\n";
}