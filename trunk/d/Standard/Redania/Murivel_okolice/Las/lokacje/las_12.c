/**
 * Las Komanda
 *
 * autor Cairell
 * opisy Pavel
 * data  styczen 2009
 */

#include <stdproperties.h>
#include <mudtime.h>
#include "dir.h"
inherit LAS_ELFOW_RZEKA;

string dlugi_opis();

void
create_las()
{
    set_short("Nad rzek^a.");
    set_long(&dlugi_opis());

    add_exit(LAS_ELFOW_LOKACJE+ "las_11.c","w");
    add_exit(LAS_ELFOW_LOKACJE+ "las_19.c","se");
    add_exit(TRAKT_LOKACJE+ "trakt_29.c","n");

    add_item(({"sitowie"}), "Sk^lada si^e z kilku gatunk^ow ro^slin, "+
        "rosn^acych przy samym brzegu. Nieco utrudnia dojscie do "+
        "wody czepiaj^ac sie but^ow i oplatujac nogi./n");
}

string dlugi_opis()
{
    string str = ""; 
 
    str+="Znajdujesz si^e aktualnie nad brzegiem rzeki Pontar. "+
        "W tym miejscu jej wody p^lyn^a spokojnie, a woda tworzy "+
        "sporej wielkosci rozlewisko. W niezmaconym lustrze wody ";

    if (jest_dzien() == 1)
    {
        str+="odbijaja sie migotliwe promienie slonca";
    }
    else
    {
        str+="odbija sie poswiata ksiezyca";
    }

    str+=". Woda przy brzegu jest p^lytka i przejrzysta, jednak "+
        "dojscie do niej utrudnia sitowie g^esto porastaj^ace brzeg. "+
        "Kawa^lek l^adu pomi^edzy rzek^a a lasem, na kt^orym w^la^snie "+
        "si^e znajdujesz jest ";

    if(pora_roku() == MT_ZIMA)
    {
        str += "pokryty r^owna warstw^a ^sniegu";
    }
    if(pora_roku() == MT_JESIEN)
    {
        str += "poro^sni^ety k^epami bujnej trawy";
    }
    if(pora_roku() == MT_LATO)
    {
        str += "poro^sni^ety k^epami bujnej, soczy^scie zielonej trawy";
    }
    else //wiosna
    {
        str += "poro^sni^ety ^swie^za, zielona trawa, miejscami "+
            "poprzetykana nielicznymi o tej porze roku kwiatami";
    }

    str+=". Odleg^ly las po drugiej stronie rzeki majaczy niewyra^xnie "+
        "r^own^a lini^a oddzielaj^ac to^n rzeki od nieba.";

str+="\n";
return str;
}


public string
exits_description() 
{
    return "Drog^e mi^edzy drzewami mo^zna odnale^s^c ruszaj^ac "+
        "na zach^od, lub po^ludniowy wsch^od, za^s na p^o^lnocy "+
        "majaczy mi^edzy drzewami trakt.\n";
}