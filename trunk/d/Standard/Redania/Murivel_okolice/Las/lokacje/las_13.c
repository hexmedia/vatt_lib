/**
 * Las Komanda
 *
 * autor Cairell
 * opisy Edrain & Aladriel
 * data  styczen 2009
 */

#include <stdproperties.h>
#include <mudtime.h>
#include "dir.h"
inherit LAS_ELFOW_RZEKA;

string dlugi_opis();
string opis_paproci();
string opis_krzewow();

void
create_las()
{
    set_short("Zielony zagajnik.");
    set_long(&dlugi_opis());

    add_exit(LAS_ELFOW_LOKACJE+ "las_15.c","ne");
    add_exit(TRAKT_LOKACJE+ "las_32.c","nw");

    add_item(({"krzew","krewy", "kwiaty"}), &opis_krzewow());
    add_item(({"papro^c","paprocie"}), &opis_paproci());
}

string dlugi_opis()
{
    string str = ""; 

    if (jest_dzien() == 1)
    {
        if(pora_roku() == MT_ZIMA)
        {
            if (CZY_JEST_SNIEG(this_object()))
            {
                str += "Gdzie nie si^egniesz wzrokiem dostrzegasz "+
                    "rozro^sni^ete krzewy malin. Nie s^a zanadto "+
                    "wysokie jednak pokrywaj^a niemal ca^l^a "+
                    "przestrze^n tutaj dost^epn^a. Ca^lkiem ju^z "+
                    "nagie i nieposiadaj^ace li^sci, g^esto "+
                    "przykryte s^a czapami bia^lego ^sniegu, kt^ory "+
                    "jest obecnie ich jedyn^a ozdob^a.";
            }
            else
            {
                str += "Gdzie nie si^egniesz wzrokiem dostrzegasz "+
                    "rozro^sni^ete krzewy malin. Nie s^a zanadto "+
                    "wysokie jednak pokrywaj^a niemal ca^l^a "+
                    "przestrze^n tutaj dost^epn^a. Ca^lkiem ju^z "+
                    "nagie i nieposiadaj^ace li^sci, stercz^a smutno "+
                    "po^sr^od b^lota ktore pozosta^lo po ostatnim "+
                    "^sniegu.";
            }
        }
        if(pora_roku() == MT_WIOSNA)
        {
            str += "Gdzie nie si^egniesz wzrokiem dostrzegasz "+
                "rozro^sni^ete krzewy malin. Nie s^a zanadto wysokie "+
                "jednak pokrywaj^a niemal ca^l^a przestrze^n tutaj "+
                "dost^epn^a. Oblepione s^a dorodnymi ciemnor^o^zowymi "+
                "kwiatami malin. S^lodki zapach unosz^acy si^e w "+
                "powietrzu delikatnie pobudza twoje zmys^ly. Na "+
                "ziemi zauwa^zasz ^swie^ze ^slady zwierzyny, kt^ora "+
                "odkry^la miejsce, gdzie s^lodycze le^z^a na ziemi.";
        }
        if(pora_roku() == MT_LATO)
        {
            str += "Gdzie nie si^egniesz wzrokiem dostrzegasz "+
                "rozro^sni^ete krzewy malin. Nie s^a zanadto wysokie "+
                "jednak pokrywaj^a niemal ca^l^a przestrze^n tutaj "+
                "dost^epn^a. Oblepione s^a dorodnymi ciemnor^o^zowymi "+
                "malinami. Niekt^ore s^a tak dojrza^le, ^ze przybra^ly "+
                "ju^z niemal ciemny rubinowy kolor i a^z prosz^a si^e "+
                "o zerwanie. Owocowy zapach unosz^acy si^e w powietrzu "+
                "delikatnie pobudza twoje zmys^ly. Na ziemi zauwa^zasz "+
                "^swie^ze ^slady zwierzyny, kt^ora odkry^la miejsce, "+
                "gdzie s^lodycze le^z^a na ziemi.";
        }
        else //jesien
        {
            str += "Gdzie nie si^egniesz wzrokiem dostrzegasz "+
                "rozro^sni^ete krzewy malin. Nie s^a zanadto wysokie "+
                "jednak pokrywaj^a niemal ca^l^a przestrze^n tutaj "+
                "dost^epn^a. Ca^lkiem ju^z nagie i nieposiadaj^ace "+
                "li^sci, strasz^a sw^a mizerno^sci^a i jakby "+
                "zabiedzeniem, powoli czekaj^ac na upragnion^a "+
                "zim^e - por^e odpoczynku.";
        }
    }
    else //noc
    {
        str += "Wysokie, mroczne zarysy drzew przes^laniaj^a nocne "+
            "niebo. U ich st^op kul^a si^e mniejsze kszta^lty kep "+
            "paproci, czy te^z zwyklych krzew^ow. Powietrze "+
            "przesycone jest wilgoci^a oraz zapachem "+
            "butwiej^acych ro^slin.";
    }

str+="\n";
return str;
}

string opis_paproci()
{
    string strr;
    if (pora_roku() == MT_ZIMA)
    {
        return 0;
    }
    if (pora_roku() == MT_JESIEN)
    {
        strr = "Niewysoka, lecz posiadaj^aca du^ze li^scie papro^c "+
            "ugina si^e do ziemi pod ci^e^zarem zal^a^zk^ow "+
            "znajduj^acych si^e pod listkami. Jej przebarwiaj^ace "+
            "si^e na br^azowo li^scie, powoli usychaj^ace, "+
            "przypominaj^a o nadchodz^acej zimie. Mi^edzy jej "+
            "listkami wiele ma^lych stworzonek znalaz^lo schronienie.\n";
        return strr;
    }
    else
    {
        strr = "Niewysoka, lecz posiadaj^aca du^ze li^scie papro^c "+
            "ugina si^e do ziemi pod ci^e^zarem zal^a^zk^ow "+
            "znajduj^acych si^e pod listkami. Jej soczysty, zielony "+
            "kolor o^zywia miejsce. Mi^edzy jej listkami wiele "+
            "ma^lych stworzonek znalaz^lo schronienie.\n";
        return strr;
    }
}

string opis_krzewow()
{
    string st;
    if (pora_roku() == MT_ZIMA)
    {
        return 0;
    }
    if (pora_roku() == MT_JESIEN)
    {
        st = "Bujne, zielono - czerwone li^scie tworz^a co^s na "+
            "kszta^lt sp^ojnej kuli, na kt^orej wierzchu znajduj^a "+
            "si^e liczne pozosta^lo^sci po niegdy^s pi^eknych "+
            "kwiatach.\n";
        return st;
    }
    else
    {
        st = "Bujne, w^sciekle zielone li^scie tworz^a co^s na kszta^lt "+
            "sp^ojnej kuli, na kt^orej wierzchu znajduj^a si^e liczne "+
            "kwiaty o mlecznej barwie. Ka^zdy kwiat ma potr^ojn^a "+
            "warstw^e p^latk^ow, co czyni je okazalszymi.\n";
        return st;
    }
}

public string
exits_description() 
{
    return "Drog^e mi^edzy drzewami mo^zna odnale^s^c ruszaj^ac "+
        "na p^o^lnocny wsch^od, za^s na p^olnocnycm zachodzie majaczy "+
        "mi^edzy drzewami trakt.\n";
}