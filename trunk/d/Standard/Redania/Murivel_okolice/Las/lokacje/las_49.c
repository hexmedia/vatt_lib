/*Las Komanda
  opisy Edrain
  kodowa� Cairell*/

#include <stdproperties.h>
#include <mudtime.h>
#include "dir.h"
inherit LAS_ELFOW_GESTY;

string dlugi_opis();
string opis_badylkow();
string opis_innych_badylkow();

void
create_las()
{
    set_short("W g^estym lesie.");
    set_long(&dlugi_opis());
    add_exit(LAS_ELFOW_LOKACJE+ "las_48.c","s");
    add_exit(LAS_ELFOW_LOKACJE+ "las_47.c","sw");
    add_exit(LAS_ELFOW_LOKACJE+ "las_77.c","n");
    
    add_item(({"wrzosy", "ro^sliny"}),&opis_badylkow());
    add_item(({"mchy", "paprocie"}),&opis_innych_badylkow());
}
string dlugi_opis()
{
    string str;
    if(jest_dzien() == 1) // DZIEN
    {
        if(CZY_JEST_SNIEG(this_object()))
        {
            str="Ziemia ta wydaje si^e od zawsze poro^sni^eta g^estymi, "+
                "pi^eknymi drzewami. D^eby, kt^ore tajemniczo g^oruj^a nad "+
                "innymi gatunkami przywodz^a na my^sl ba^sniowe lasy, o "+
                "kt^orych zazwyczaj czyta si^e tylko w ksi^a^zkach. Wi^azy "+
                "stanowi^a niesamowite dope^lnienie tego wra^zenia, ze swoimi "+
                "sp^ekanymi, ^luszcz�cymi si^e pniami i powyginanymi konarami. "+
                "Na ziemi pokrytej obecnie zaspami ^snie^znymi igra ^swiat^lo, "+
                "kt^ore ta^ncz^ac na p^latkach bia^lego puchu nadaje miejscu "+
                "lekko^sci. Harmonia panuj^aca tu ca^lkowicie pozwala "+
                "zapomnie^c o zgie^lku cywilizacji.";
        }
        else if(pora_roku() == MT_WIOSNA)
        {
            str="Ziemia ta wydaje si^e od zawsze poro^sni^eta g^estymi, "+
                "pi^eknymi drzewami. D^eby kt�re tajemniczo g^oruj^a nad "+
                "innymi gatunkami przywodz^a na my^sl ba^sniowe lasy, o "+
                "kt^orych zazwyczaj czyta si^e tylko w ksi^a^zkach. Wi^azy, "+
                "ze swoimi sp�kanymi, ^luszcz^acymi si^e pniami i powyginanymi "+
                "konarami, stanowi^a niesamowite dope^lnienie tego wra^zenia. "+
                "Na ziemi zauwa^zy^c mo�na paprocie, mchy, wrzosy oraz inne "+
                "kwiaty le^sne, kt^ore nadaj^a stawianym w tym miejscu krokom "+
                "lekko^sci. Harmonia panuj^aca tu ca^lkowicie pozwala "+
                "zapomnie^c o zgie^lku cywilizacji.";
        }
        else if(pora_roku() == MT_LATO)
        {
            str="Ziemia ta wydaje si^e od zawsze poro^sni^eta g^estymi, "+
                "pi^eknymi drzewami. D^eby kt�re tajemniczo g^oruj^a nad "+
                "innymi gatunkami przywodz^a na my^sl ba^sniowe lasy, o "+
                "kt^orych zazwyczaj czyta si^e tylko w ksi^a^zkach. Wi^azy, "+
                "ze swoimi sp�kanymi, ^luszcz^acymi si^e pniami i powyginanymi "+
                "konarami, stanowi^a niesamowite dope^lnienie tego wra^zenia. "+
                "Na ziemi zauwa^zy^c mo�na paprocie, mchy, wrzosy oraz inne "+
                "kwiaty le^sne, kt^ore nadaj^a stawianym w tym miejscu krokom "+
                "lekko^sci. Harmonia panuj^aca tu ca^lkowicie pozwala "+
                "zapomnie^c o zgie^lku cywilizacji.";
        }
        else //jesien i zima gdy nie ma sniegu
        {
            str="Ziemia ta wydaje si^e od zawsze poro^sni^eta g^estymi, "+
                "pi^eknymi drzewami. D^eby kt�re tajemniczo g^oruj^a nad "+
                "innymi gatunkami przywodz^a na my^sl ba^sniowe lasy, o "+
                "kt^orych zazwyczaj czyta si^e tylko w ksi^a^zkach. Wi^azy, "+
                "ze swoimi sp�kanymi, ^luszcz^acymi si^e pniami i powyginanymi "+
                "konarami, stanowi^a niesamowite dope^lnienie tego wra^zenia. "+
                "Na ziemi zauwa^zy^c mo�na uschni^ete paprocie, po^z^o^lk^le "+
                "mchy, wrzosy oraz inne pozosta^lo^sci le^snych kwiat^ow, "+
                "kt^ore nadaj^a stawianym w tym miejscu krokom lekko^sci, "+
                "delikatnie jedynie szeleszcz^ac. Harmonia panuj^aca tu "+
                "ca^lkowicie pozwala zapomnie^c o zgie^lku cywilizacji.";
        }
    }
    else // NOC
    {
        if(CZY_JEST_SNIEG(this_object()))
        {
            str="Skryta w mroku i nocnej ciszy okolica wydaje si^e od zawsze "+
                "poro^sni^eta g^estymi, "+
                "pi^eknymi drzewami. D^eby, kt^ore tajemniczo g^oruj^a nad "+
                "innymi gatunkami przywodz^a na my^sl ba^sniowe lasy, o "+
                "kt^orych zazwyczaj czyta si^e tylko w ksi^a^zkach. Wi^azy "+
                "stanowi^a niesamowite dope^lnienie tego wra^zenia, ze swoimi "+
                "sp^ekanymi, ^luszcz�cymi si^e pniami i powyginanymi konarami. "+
                "Na ziemi pokrytej obecnie zaspami ^snie^znymi igra ^swiat^lo, "+
                "kt^ore ta^ncz^ac na p^latkach bia^lego puchu nadaje miejscu "+
                "lekko^sci. Harmonia panuj^aca tu ca^lkowicie pozwala "+
                "zapomnie^c o zgie^lku cywilizacji.";
        }
        else if(pora_roku() == MT_WIOSNA)
        {
            str="Skryta w mroku i nocnej ciszy okolica wydaje si^e od zawsze "+
                "poro^sni^eta g^estymi, "+
                "pi^eknymi drzewami. D^eby kt�re tajemniczo g^oruj^a nad "+
                "innymi gatunkami przywodz^a na my^sl ba^sniowe lasy, o "+
                "kt^orych zazwyczaj czyta si^e tylko w ksi^a^zkach. Wi^azy, "+
                "ze swoimi sp�kanymi, ^luszcz^acymi si^e pniami i powyginanymi "+
                "konarami, stanowi^a niesamowite dope^lnienie tego wra^zenia. "+
                "Na ziemi zauwa^zy^c mo�na paprocie, mchy, wrzosy oraz inne "+
                "kwiaty le^sne, kt^ore nadaj^a stawianym w tym miejscu krokom "+
                "lekko^sci. Harmonia panuj^aca tu ca^lkowicie pozwala "+
                "zapomnie^c o zgie^lku cywilizacji.";
        }
        else if(pora_roku() == MT_LATO)
        {
            str="Skryta w mroku i nocnej ciszy okolica wydaje si^e od zawsze "+
                "poro^sni^eta g^estymi, "+
                "pi^eknymi drzewami. D^eby kt�re tajemniczo g^oruj^a nad "+
                "innymi gatunkami przywodz^a na my^sl ba^sniowe lasy, o "+
                "kt^orych zazwyczaj czyta si^e tylko w ksi^a^zkach. Wi^azy, "+
                "ze swoimi sp�kanymi, ^luszcz^acymi si^e pniami i powyginanymi "+
                "konarami, stanowi^a niesamowite dope^lnienie tego wra^zenia. "+
                "Na ziemi zauwa^zy^c mo�na paprocie, mchy, wrzosy oraz inne "+
                "kwiaty le^sne, kt^ore nadaj^a stawianym w tym miejscu krokom "+
                "lekko^sci. Harmonia panuj^aca tu ca^lkowicie pozwala "+
                "zapomnie^c o zgie^lku cywilizacji.";
        }
        else //jesien i zima gdy nie ma sniegu
        {
            str="Skryta w mroku i nocnej ciszy okolica wydaje si^e od zawsze "+
                "poro^sni^eta g^estymi, pi^eknymi drzewami. D^eby kt�re "+
                "tajemniczo g^oruj^a nad innymi gatunkami przywodz^a na my^sl "+
                "ba^sniowe lasy, o kt^orych zazwyczaj czyta si^e tylko w "+
                "ksi^a^zkach. Wi^azy, ze swoimi sp�kanymi, ^luszcz^acymi si^e "+
                "pniami i powyginanymi konarami, stanowi^a niesamowite "+
                "dope^lnienie tego wra^zenia. Na ziemi zauwa^zy^c mo�na "+
                "uschni^ete paprocie, po^z^o^lk^le mchy, wrzosy oraz inne "+
                "pozosta^lo^sci le^snych kwiat^ow, kt^ore nadaj^a stawianym w "+
                "tym miejscu krokom lekko^sci, delikatnie jedynie "+
                "szeleszcz^ac. Harmonia panuj^aca tu ca^lkowicie pozwala "+
                "zapomnie^c o zgie^lku cywilizacji.";
        }
    }
    str += "\n";
    return str;
}

string opis_badylkow()
{   
    string str;
    if(pora_roku() == MT_ZIMA)
    {
        str = "Wsz^edzie dooko^la wystaj^a z przeczekuj^acego zim^e runa "+
            "g^este, lecz ca^lkiem uschni^ete ^lody^zki wrzosu. Latem p^lo^zy "+
            "si^e on g^esto tworz^ac wyciszaj^acy dla krok^ow ciemnozielony "+
            "dywan, teraz jednak, szeleszcz^ac cichutko, gdy patyczki "+
            "p^ekaj^a pod stopami. Po pi^eknych, r^o^zowo-fioletowych kwiatach "+
            "w pe^lni rozwijaj^acych si^e wczesn^a jesieni^a teraz nie "+
            "pozosta^l najmiejszy nawet ^slad.\n"; 
    }
    else
    {
        str = "Ta zimozielona dwupienna krzewinka o listkach rozes^lanych po "+
            "ziemi zwana jest wrzosem. P^lo^zy si^e g^esto wsz^edzie dooko^la, "+
            "tworz^ac wyciszaj^acy dla krok^ow ciemnozielony dywan. Jej drobne, "+
            "igie^lkowate li^scie u^lo^zone s^a naprzeciw siebie i sprawiaj^a "+
            "mylne wra^zenie bardzo ubogich, jednak s^a one tylko dodatkiem do "+
            "pi^eknych, r^o^zowo-fioletowych kwiat^ow w pe^lni rozwijaj^acych "+
            "si^e wczesn^a jesieni^a, r^ownie drobnych, a jednak "+
            "zachwycaj�cych swym pi^eknem, kt^ore maluje ^swiat w czasie gdy "+
            "prawie wszelkie inne ozdoby przyrody ju� przekwit^ly.\n";
    }
    
    return str;
}

string opis_innych_badylkow()
{
    string str;
    
    if(pora_roku() == MT_ZIMA)
    {
       str = "W zaciszu, pod drzewami w ^sr^od obumar^lego runa dostrzec "+
           "mo^zna po^z^o^lk^le mchy i ^slady po paprociach, oczekuj^ace na "+
           "nadej^scie wiosny.\n";
    }
    else
    {
        str = "Gdzieniegdzie, g^l^ownie pod drzewami stercz^a k^epy paproci, "+
            "lub p^lo^z^a si^e plamy mie^kkiego, ciemnozielonego mchu, lecz "+
            "w wi^ekszo^s^c ro^slin le^snego runa zag^luszona zosta^la "+
            "przez wybuja^ly w tym miejscu, ciemnozielony dywan wrzos^ow.\n";
    }
    
    return str;
}
