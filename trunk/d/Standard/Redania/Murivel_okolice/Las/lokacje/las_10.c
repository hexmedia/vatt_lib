/**
 * Las Komanda
 *
 * autor Cairell
 * opisy Edrain & Aladriel
 * data  styczen 2009
 */

#include <stdproperties.h>
#include <mudtime.h>
#include "dir.h"
inherit LAS_ELFOW_RZEKA;

string dlugi_opis();

void
create_las()
{
    set_short("Po^sr^od wierzb.");
    set_long(&dlugi_opis());

    add_exit(TRAKT_LOKACJE+ "trakt_29.c","e");
    add_exit(LAS_ELFOW_LOKACJE+ "las_32.c","n");
    add_exit(LAS_ELFOW_LOKACJE+ "las_7.c","nw");
    add_exit(LAS_ELFOW_LOKACJE+ "las_9.c","w");
}

string dlugi_opis()
{
    string str = ""; 
    
    str+="Olbrzymie wierzby p^lacz^ace zdecydowanie s^a paniami "+
        "tego miejsca. Nie ma ich tutaj du^zo, jednak ich ga^l^ezie s^a "+
        "na tyle roz^lo^zyste, ^ze niemal stykaj^a si^e ze sob^a. Jest to "+
        "na pewno niezapomniany widok, gdy^z witki zwisaj^ace do ziemi ";

    if (jest_dzien() == 1)
    {
        if(pora_roku() == MT_WIOSNA)
        {
            str += "tworz^a ^z^o^lto-zielon^a kurtyn^e poruszaj^ac^a si^e "+
                "przy ka^zdym, najl^zejszym nawet mu^sni^eciem wiatru. "+
                "Delikatno�� a zarazem moc drzemie w p^lacz^acych "+
                "wierzbach, idealnych towarzyszach nieszcz^e^sliwych "+
                "kochank^ow, przychodz^acych tu, aby w ciszy przemy^sle^c "+
                "smutki i ^zale, kt^ore ich gryz^a. Nie ma tu zbyt "+
                "wiele ro^slin. Nawet trawa uleg^la magii drzew i "+
                "ledwo przykrywa ziemi^e, dziel^ac z nimi smutek.";
        }
        if(pora_roku() == MT_LATO)
        {
            str += "tworz^a ^z^o^lto-zielon^a kurtyn^e poruszaj^ac^a si^e "+
                "przy ka^zdym, najl^zejszym nawet mu^sni^eciem wiatru. "+
                "Delikatno�� a zarazem moc drzemie w p^lacz^acych "+
                "wierzbach, idealnych towarzyszach nieszcz^e^sliwych "+
                "kochank^ow, przychodz^acych tu, aby w ciszy przemy^sle^c "+
                "smutki i ^zale, kt^ore ich gryz^a. Nie ma tu zbyt "+
                "wiele ro^slin. Nawet trawa uleg^la magii drzew i "+
                "ledwo przykrywa ziemi^e, dziel^ac z nimi smutek.";
        }
        if(pora_roku() == MT_JESIEN)
        {
            str += "tworz� ^z^o^lto-br^azow^a kurtyn^e poruszaj^ac^a "+
                "si^e przy ka^zdym, najl^zejszym nawet mu^sni^eciem "+
                "wiatru. Delikatno^s^c a zarazem moc drzemie w "+
                "p^lacz^acych wierzbach, idealnych towarzyszach "+
                "nieszcz^e^sliwych kochank^ow, przychodz^acych tu, "+
                "aby w ciszy przemy^sle^c smutki i ^zale, kt^ore "+
                "ich gryz^a. Nie ma tu zbyt wiele ro^slin. Nawet "+
                "trawa uleg^la magii drzew i ledwo przykrywa ziemi^e, "+
                "dziel^ac z nimi smutek.";
        }
        if(pora_roku() == MT_ZIMA)
        {
            str += "bezlistn� kurtyn^e poruszaj^ac^a si^e przy ka^zdym"+
                ", najl^zejszym nawet mu^sni^eciem wiatru. Delikatno^s^c "+
                "a zarazem moc drzemie w p^lacz^acych wierzbach, "+
                "idealnych towarzyszach nieszcz^e^sliwych kochank^ow, "+
                "przychodz^acych tu, aby w ciszy przemy^sle^c smutki "+
                "i ^zale, kt^ore ich gryz^a. Nie ma tu zbyt wiele "+
                "ro^slin. A te kt^ore dostrzegasz, ledwie wystaj^a "+
                "spod g^l^ebokich zasp ^snie^znych le^z^acych, a^z "+
                "po horyzont.";
        }
    }
    else //noc
    {
        str += "po^lyskuj^a delikatnie w blasku ksi^e^zyca tworz^ac "+
            "jednocze^snie g^l^ebokie cienie u podstaw drzew.";
    }

str+="\n";
return str;
}

public string
exits_description() 
{
    return "Drog^e mi^edzy drzewami mo^zna odnale^s^c ruszaj^ac "+
        "na zach^od, p^o^lnocny zach^od i p^o^lnoc. Na wschodzie "+
        "za^s, mi^edzy drzewami majaczy trakt.\n";
}