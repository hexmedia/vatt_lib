/*Las Komanda
  opisy Eskela
  kod Cairell*/

#include <stdproperties.h>
#include <mudtime.h>
#include <macros.h>
#include "dir.h"
inherit LAS_ELFOW_BAGIENKO;

string dlugi_opis();

void
create_bagienko()
{
    set_short("Podmok^la cz^e^s^c lasu.");
    set_long(&dlugi_opis());
    add_exit(TRAKT_LOKACJE+ "trakt_25.c","ne");
    add_exit(TRAKT_LOKACJE+ "trakt_23.c","w");
    add_exit(LAS_ELFOW_LOKACJE+ "bagienko_e.c","e");
    add_exit(LAS_ELFOW_LOKACJE+ "bagienko_se.c","se");
    add_exit(LAS_ELFOW_LOKACJE+ "bagienko_sw.c","s");

    add_item(({"wode", "bagno"}), "Nieruchom^a tafl^e wody przykrywa "+
        "zielony dywan rz^esy i ^lopianu. Po chwili dostrzegasz skrzydlate "+
        "^zycie tocz^ace si^e tu^z nad wod^a. Zielone wa^zki, chmary "+
        "drobniutkich muszek, brz^eczących komar^ow i gz^ow unosz^a si^e w "+
        "bez^ladnym ta^ncu, wiruj^ac bez ko^nca.\n");
    add_item(({"drzewa"}), "Spr^ochnia^le, nagryzione z^ebem czasu konary "+
        "d^eb^ow i lip pochylaj^a si^e w zadumie nad nieprzejrzyst^a wod^a. "+
        "Niekt^ore kikuty ga^l^ezi zwisaj^a bezradnie, inne jakby z pro^sb^a "+
        "o pomoc skierowane s^a ku g^orze. Drewno prze^zarte przez korniki "+
        "zdaje si^e trzeszcze^c niemym j^ekiem rozpaczy, a dziuple "+
        "wyd^lubane przez bezlitosne dzi^ecio^ly przypominaj^a czarne, "+
        "przepe^lnione smutkiem oczy. Powalone konary le^z^a do po^lowy "+
        "w wodzie, powoli zapadając si^e w bezdenne bagno.\n");
    add_item(({"trzciny", "tatarak", "rokicine", "k^epy rokiciny"}), "Wp^o^l "+
        "zgni^le, nasi^akniete wod^a trzciny stanowi^a azyl dla stworze^n. "+
        "Zielonkawo-brunatny kszta^lt, kt^ory wydawa^l si^e by^c ga^l^ezi^a, "+
        "drgn^a^l i zasycza^l jadowicie. Na li^sciu lilii wodnej wielkimi, "+
        "wy^lupiastymi oczami spoziera stara, paskudna ropucha. Nieopodal "+
        "za^s pstrokata cyranka w ca^lkowitym bezruchu, przekrzywiaj^ac "+
        "lekko ^lepek, patrzy czarnymi oczami w bli^zej nieokre^slony "+
        "punkt na wodzie.\n");
}

string dlugi_opis()
{
    string str;

    str = "G^l^ebokia kotlina, otoczona ledwo widocznym z dolu traktem, "+
        "jest przesi^akni^eta wilgoci^a. To miejsce przypomina zapomniany "+
        "cmentarz, wyczuwasz dziwn^a obecno^s^c... tylko czego? Kogo? "+
        "Pozornie martwa woda skrywa w swej g^l^ebi mroczne tajemnice, "+
        "kt^ore nigdy nie zostana poznane. Drzewa zdaj^a si^e wyci^aga^c "+
        "swoje ga^l^ezie, by pochwyci^c podr^o^znika, zakleszczy^c w "+
        "mocarnym u^scisku i nigdy nie pu^sci^c. Z^lowieszcze cienie "+
        "dr^z^a wraz z cichym szeptem - ^zalem drzew. Bobownik, "+
        "szorstki wiszar, mech i k^epina tworz^a pancerz, po kt^orym st^apa "+
        "si^e zadziwiająco mi^ekko, ale to mo^ze by^c tylko nast^epny "+
        "podst^ep...";
    
    str += "\n";
    return str;
}
