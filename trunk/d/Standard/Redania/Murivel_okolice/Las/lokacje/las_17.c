/**
 * Las Komanda
 *
 * autor Cairell
 * opisy Edrain & Aladriel
 * data  styczen 2009
 */

#include <stdproperties.h>
#include <mudtime.h>
#include "dir.h"
inherit LAS_ELFOW_RZEKA;

string dlugi_opis();

void
create_las()
{
    set_short("Piaszczysta pla^za nad brzegiem rzeki.");
    set_long(&dlugi_opis());
    add_exit(LAS_ELFOW_LOKACJE+ "las_15.c","sw");
    add_exit(LAS_ELFOW_LOKACJE+ "las_16.c.c","w");
    add_exit(TRAKT_LOKACJE+ "trakt_34.c","nw");
    add_exit(TRAKT_LOKACJE+ "trakt_36.c","e");
    add_exit(TRAKT_LOKACJE+ "trakt_35.c","n");

    add_item(({"ul"}), "");
}

string dlugi_opis()
{
    string str = ""; 
    
    str+="Teren ten pokrywa z^locisty piasek, co troch^e wymywany "+
        "z dna rzeki, razem z miodowymi ma^lymi kamyczkami i drobnymi "+
        "muszelkami. Miejsce z jednej strony ograniczone ^zywo "+
        "p^lyn^ac^a rzek^a, a z drugiej skarp^a, wydaje si^e wr^ecz "+
        "stworzone do odpoczynku, a cichy szmer p^lyn^acej wody koi "+
        "zmys^ly. Co jaki^s czas widzisz ryb^e wyskakuj^ac^a z toni "+
        "wysoko w g^or^e, kt^ora po chwili na powr^ot zanurza si^e "+
        "w odm^etach.";

str+="\n";
return str;
}

public string
exits_description() 
{
    return "Drog^e przez las mo^zna odnale^s^c id^ac na zach^od, "+
        "lub po^udniowy zach^od, za^s na p^o^lnocnym zachodzie, "+
        "po^lnocy oraz wschodzie majaczy mi^edzy drzewami trakt.\n";
}