/*Las Komanda
  opisy Edrain
  kodowa� Cairell*/

#include <stdproperties.h>
#include <mudtime.h>
#include "dir.h"
inherit LAS_ELFOW_GESTY;

string dlugi_opis();

void
create_las()
{
    set_short("Gdzie^s w g^estym lesie.");
    set_long(&dlugi_opis());
    add_exit(LAS_ELFOW_LOKACJE+ "las_51.c","w");
    add_exit(LAS_ELFOW_LOKACJE+ "las_33.c","sw");
    add_exit(LAS_ELFOW_LOKACJE+ "las_30.c","nw");
    add_exit(LAS_ELFOW_LOKACJE+ "las_54.c","e");
    add_exit(LAS_ELFOW_LOKACJE+ "las_36.c","se");
    add_exit(LAS_ELFOW_LOKACJE+ "las_60.c","ne");
}
string dlugi_opis()
{
    string str;
    if(jest_dzien() == 1) // DZIEN
    {
            str="Teren g^esto poro^sni^ety drzewami ci^agnie si^e a^z po "+
                "horyzont, na kt^orym dostrzegasz ja^sniejsz^a ^lun^e "+
                "^swiat^la, tak jakby w tamtym miejscu g^esto rosn^ace "+
                "drzewa troch^e przerzedzi^ly swe szeregi i pozwoli^ly "+
                "^swiat^lu igra^c w dolnych partiach lasu. Szum rosn^acych "+
                "dooko^la wiekowych d^eb^ow i wi^az^ow przywodzi na my^sl "+
                "cichy szept istot strzeg^acych lasu, prosz^acych by^s nie "+
                "naruszy^l dziewiczo^sci tego miejsca. Zaro^sla okalaj^ace "+
                "ci^e z ka^zdej strony powoduj^a, ^ze trudno gdziekolwiek "+
                "si^e ruszy^c, aby nie po^lama^c ^zadnych le^snych ro^slin.";
    }
    else // NOC
    {
            str="Czarne ga^l^ezie przes^laniaj^a nocne niebo, jedynie na "+
                "wschodzie tworz^ac cienie bli^zej ziemi, tak jakby w tamtym "+
                "miejscu g^esto rosn^ace drzewa troch^e przerzedzi^ly swe "+
                "szeregi i pozwoli^ly blaskowi ksi^e^zyca igra^c w dolnych "+
                "partiach lasu. Szum rosn^acych "+
                "dooko^la wiekowych d^eb^ow i wi^az^ow przywodzi na my^sl "+
                "cichy szept istot strzeg^acych lasu, prosz^acych by^s nie "+
                "naruszy^l dziewiczo^sci tego miejsca. Zaro^sla okalaj^ace "+
                "ci^e z ka^zdej strony powoduj^a, ^ze trudno gdziekolwiek "+
                "si^e ruszy^c, aby nie po^lama^c ^zadnych le^snych ro^slin.";
    }
    str += "\n";
    return str;
}
