/* Las komanda
   Autor: Avard 
   Opis : Edrain */

#include <stdproperties.h>
#include <macros.h>
#include <mudtime.h>
#include "dir.h"
inherit LAS_ELFOW_GESTY;

string dlugi_opis();

string opis_wrzosow();
string opis_fiolkow();
string opis_zawilcow();

int wez(string str);

void
create_las()
{
    set_short("W sercu puszczy");
    set_long(&dlugi_opis());
    add_exit(LAS_ELFOW_LOKACJE+ "las_67.c","w");
    add_exit(LAS_ELFOW_LOKACJE+ "las_64.c","se");
    add_exit(LAS_ELFOW_LOKACJE+ "las_58.c","s");
    add_item(({"zawilca","zawilce"}), &opis_zawilcow());
    add_item(({"wrzos","wrzosy"}), &opis_wrzosow());
    add_item(({"fio^lka","fio^lki"}), &opis_fiolkow());
}

public void
init()
{
    ::init();
    add_action(wez,"we�");
    add_action(wez,"zbieraj");
    add_action(wez,"zbierz");
}

string dlugi_opis()
{
    string str = "";
    
    if(jest_dzien()) //Dzie�
    {
        if(CZY_JEST_SNIEG(this_object()))
        {
            str+="Dooko�a rozci�gaj� si� po�acie puszczy bukowej. Strzeliste "+
            "buki tworz� w g�rze baldachim,przez kt�ry prze�wieca "+
            "przyt�umione �wiat�o, zalewaj�c okolic� ostrym s�onecznym "+
            "blaskiem. Na ziemi dywan tworzy bia�y puch �niegu mieni�cy si� "+
            "w promieniach kolorami t�czy. Gdzieniegdzie przez korony buk�w "+
            "przedostaje si� �wiat�o, tworz�c niezwyk�e smugi w mroku lasu, "+
            "nadaj�c tym samym niesamowito�ci temu miejscu";
        }
        else if(pora_roku() == MT_WIOSNA || pora_roku() == MT_LATO)
        {
            str+="Dooko�a rozci�gaj� si� po�acie puszczy bukowej. Strzeliste "+
            "buki tworz� w g�rze baldachim,przez kt�ry prze�wieca "+
            "przyt�umione �wiat�o, zalewaj�c okolic� szmaragdowym blaskiem. "+
            "Na ziemi dywan tworz� krzaczki zawilc�w i fio�k�w, porozrzucane "+
            "mi�dzy orzeszkami buk�w pokrywaj�cymi ziemi�. Gdzieniegdzie "+
            "przez korony buk�w przedostaje si� �wiat�o, tworz�c niezwyk�e "+
            "smugi w czerni lasu, nadaj�c tym samym niesamowito�ci temu "+
            "miejscu.";
        }
        else
        {
            str+="Dooko�a rozci�gaj� si� po�acie puszczy bukowej. Strzeliste "+
            "buki tworz� w g�rze baldachim,przez kt�ry prze�wieca "+
            "przyt�umione �wiat�o, zalewaj�c okolic� pomara�czowym blaskiem. "+
            "Na ziemi dywan tworz� krzaczki wrzos�w porozrzucane mi�dzy "+
            "orzeszkami buk�w pokrywaj�cymi ziemi�. Gdzieniegdzie przez "+
            "korony buk�w upstrzone czerwonymi i ��tymi li��mi przedostaje "+
            "si� �wiat�o, tworz�c niezwyk�e smugi w czerni lasu, nadaj�c "+
            "tym samym niesamowito�ci temu miejscu.";
        }
    }
    else //Noc
    {
        if(CZY_JEST_SNIEG(this_object()))
        {
            str+="Dooko�a rozci�gaj� si� po�acie puszczy bukowej. Strzeliste "+
            "buki tworz� w g�rze baldachim,przez kt�ry prze�wieca "+
            "przyt�umione �wiat�o, zalewaj�c okolic� fioletowo sinym "+
            "blaskiem. Na ziemi dywan tworzy bia�y puch �niegu mieni�cy si� "+
            "diamentowo w promieniach ksi�yca . Gdzieniegdzie przez korony "+
            "buk�w przedostaje si� ksi�ycowa po�wiata, tworz�c niezwyk�e "+
            "smugi w mroku lasu, nadaj�c tym samym niesamowito�ci temu miejscu";
        }
        else if(pora_roku() == MT_WIOSNA || pora_roku() == MT_LATO)
        {
            str+="Dooko�a rozci�gaj� si� po�acie puszczy bukowej. Strzeliste "+
            "buki tworz� w g�rze baldachim,przez kt�ry prze�wieca "+
            "przyt�umione �wiat�o, zalewaj�c okolic� fioletowo sinym "+
            "blaskiem. Na ziemi dywan tworz� krzaczki zawilc�w i fio�k�w, "+
            "porozrzucane mi�dzy orzeszkami buk�w pokrywaj�cymi ziemi�. "+
            "Gdzieniegdzie przez korony buk�w przedostaje si� blask "+
            "ksi�yca, tworz�c niezwyk�e smugi w czerni lasu, nadaj�c tym "+
            "samym niesamowito�ci temu miejscu.";
        }
        else
        {
            str+="Dooko�a rozci�gaj� si� po�acie puszczy bukowej. Strzeliste "+
            "buki tworz� w g�rze baldachim,przez kt�ry prze�wieca "+
            "przyt�umione �wiat�o, zalewaj�c okolic� fioletowo sinym "+
            "blaskiem. Na ziemi dywan tworz� krzaczki wrzos�w porozrzucane "+
            "mi�dzy orzeszkami buk�w pokrywaj�cymi ziemi�. Gdzieniegdzie "+
            "przez korony buk�w upstrzone czerwonymi i ��tymi li��mi "+
            "przedostaje si� blask ksi�yca, tworz�c niezwyk�e smugi w "+
            "czerni lasu, nadaj�c tym samym niesamowito�ci temu miejscu.";
        }
    }
    str += "\n";
    return str;
}

string opis_zawilcow()
{
    string str;
    if (pora_roku() == MT_WIOSNA)
    {
    str += "Zawilec gajowy jest pojedynczym kwiatem o d^lugiej "+
    "delikatnej ^lody^zce wyrastaj^acej z k^l^acza. "+
    "Poszarpane listki, zazwyczaj tylko trzy "+
    "wyst^epuj^ace w ok^o^lkach i wygl^adaj^a jak szpony "+
    "jakiej^s zjawy. Aktualnie ka^zda ^lody^zka zwie^nczona "+
    "jest prze^slicznym bia^lym kwiatkiem, sk^ladaj^acym "+
    "si^e z pi^eciu lub siedmiu p^latk^ow. Soki tej "+
    "ro^sliny s^a silnie truj^ace i powoduj^a zapalenie "+
    "nerek i uk^ladu pokarmowego, wywo^luj^ac silne "+
    "wymioty i biegunk^e.\n"; 
    return str;
    }
    else
    {
    return 0;
    }
}

string opis_wrzosow()
{
    string str;
    if (pora_roku() == MT_JESIEN)
    {
    str = "Ta zimozielona dwupienna krzewinka o listkach "+
    "rozes^lanych po ziemi, p^lo^zy si^e g^esto "+
    "wsz^edzie dooko^la, tworz^ac wyciszaj^acy dla "+
    "krok^ow ciemnozielony dywan. Jej drobne, "+
    "igie^lkowate li^scie u^lo^zone s^a naprzeciw siebie "+
    "i sprawiaj^a mylne wra^zenie bardzo ubogich, "+
    "jednak s^a one tylko dodatkiem do pi^eknych "+
    "r^o^zowo- fioletowych kwiat^ow w pe^lni "+
    "rozkwitaj^acych wczesn^a jesieni^a, r^ownie "+
    "drobnych, a jednak zachwycaj^acych swym pi^eknem, "+
    "kt^ore maluje ^swiat w czasie gdy prawie wszelkie "+
    "inne ozdoby przyrody ju^z przekwit^ly.\n";
    return str;
    }
    else
    {
    return 0;
    }
}
string opis_fiolkow()
{
    string str;
    if (pora_roku() == MT_WIOSNA)
    {
    str = "P^lo^z^aca si^e po ziemi bylina jest teraz g^esto "+
    "upstrzona pi^eknie pachn^acymi fioletowymi "+
    "kwiatami. Delikatne kwiaty wyniesione s^a nad "+
    "ziemie na wysokich szypu^lkach, ka^zdy z nich "+
    "sk^lada si^e z pi^eciu p^latk^ow dw^och "+
    "skierowanych w g^or^e i trzech w d^o^l. "+
    "Zielone listki, kt^ore dodatkowo eksponuj^a "+
    "pi^ekno koloru fio^lk^ow, maj^a kszta^lt sercowaty "+
    "i s^a lekko karbowane od g^ory.\n";
    return str;
    }
    else if (pora_roku() == MT_LATO)
    {
    str = "Na p^lo^z^acej si^e po ziemi bylinie nie wida^c "+
    "ju^z ^slad^ow niedawno pi^eknie kwitn^acych, "+
    "drobnych p^latk^ow fio^lk^ow. Z wonnej ro^sliny "+
    "pozosta^ly ^lody^zki i ciemnozielone, sercowate "+
    "li^scie, kt^ore t^lumi^a wszelkie odg^losy "+
    "zwierz^at i podr^o^znych.\n";
    return str;
    }
    else return 0;
}

int wez(string str)
{
    if(query_verb() ~= "we�")
        notify_fail("We� co?\n");
    if(query_verb() ~= "zbieraj")
        notify_fail("Zbieraj co?\n");
    if(query_verb() ~= "zbierz")
        notify_fail("Zbierz co?\n");
        
    if (!str) return 0;
    if (!strlen(str)) return 0;
    
    if(pora_roku() == MT_ZIMA)
        return 0;
    
    if(query_verb() ~= "we�")
    {
        if(!parse_command(str,environment(TP),"'orzech' / 'orzechy'"))
            return 0;
    }
    else 
    {
        if(!parse_command(str,environment(TP),"'orzechy'"))
            return 0;
    }
    write("Zaczynasz zbiera� orzechy.\n");
    saybb(QCIMIE(TP,PL_MIA)+" zaczyna zbiera� orzechy.\n");
    clone_object(LAS_ELFOW_PRZEDMIOTY+"paraliz_orzechy.c")->move(TP);
    return 1;
}