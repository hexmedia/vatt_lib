/**
 * Las Komanda
 *
 * autor Cairell
 * opisy Edrain & Aladriel
 * data  styczen 2009
 */

#include <stdproperties.h>
#include <mudtime.h>
#include "dir.h"
inherit LAS_ELFOW_RZEKA;

string dlugi_opis();
string opis_paproci();
string opis_krzewow();

void
create_las()
{
    set_short("Zielony zagajnik.");
    set_long(&dlugi_opis());

    add_exit(LAS_ELFOW_LOKACJE+ "las_4.c","nw");
    add_exit(LAS_ELFOW_LOKACJE+ "las_6.c","e");

    add_item(({"krzew","krewy", "kwiaty"}), &opis_krzewow());
    add_item(({"papro^c","paprocie"}), &opis_paproci());
}

string dlugi_opis()
{
    string str = ""; 

    if (jest_dzien() == 1)
    {
        if(pora_roku() == MT_ZIMA)
        {
            if (CZY_JEST_SNIEG(this_object()))
            {
                str += "Znajdujesz si^e w jednej z najbardziej "+
                    "zaro^sni^etych cz^e^sci tego lasu. Panuje "+
                    "tu do^s^c du^za wilgotno^s^c, kt^ora jest "+
                    "wyczuwalna w powietrzu. Niezbyt gruba, wymieciona "+
                    "wiatrem warstwa ^sniegu r^owno bieli okolic^e, "+
                    "sprawiaj^ac, ^ze wida^c tutaj niewiele ro^slin "+
                    "charakterystycznych dla takiego typu las^ow. "+
                    "Wszystkie teraz ^spia, czekaj^ac nastania wiosny.";
            }
            else
            {
                str += "Znajdujesz si^e w jednej z najbardziej "+
                    "zaro^sni^etych cz^e^sci tego lasu. Panuje "+
                    "tu do^s^c du^za wilgotno^s^c, kt^ora jest "+
                    "wyczuwalna w powietrzu. Zamiast ^sniegu widzisz "+
                    "na ziemi blotnista ma^x, gdy^z od rzeki wieje "+
                    "zbyt cieply wiatr by bia^ly puch utrzyma^l "+
                    "si^e d^lugo. Gruby mech porasta runo. "+
                    "Dostrzegasz tu wiele ro^slin "+
                    "charakterystycznych dla takiego typu las^ow. "+
                    "Wszystkie teraz ^spia, czekaj^ac nastania wiosny.";
            }
        }
        else //inne pory roku
        {
            str += "Znajdujesz si^e w jednej z najbardziej zielonych "+
                "cz^e^sci tego lasu. Panuje tu do^s^c du^za "+
                "wilgotno^s^c, kt^ora jest wyczuwalna w powietrzu. "+
                "Gruby mech przerasta runo. Dostrzegasz tu wiele "+
                "ro^slin charakterystycznych dla takiego typu las^ow. "+
                "Opr^ocz paproci i krzewu du^zych kwiat^ow zauwa^zasz "+
                "tak^ze wiele krzaczk^ow jag^od wyrastaj^acych "+
                "pomi^edzy drzewami.";
        }
    }
    else //noc
    {
        str += "Wysokie, mroczne zarysy drzew przes^laniaj^a nocne "+
            "niebo. U ich st^op kul^a si^e mniejsze kszta^lty kep "+
            "paproci, czy te^z zwyklych krzew^ow. Powietrze "+
            "przesycone jest wilgoci^a oraz zapachem "+
            "butwiej^acych ro^slin.";
    }

str+="\n";
return str;
}

string opis_paproci()
{
    string strr;
    if (pora_roku() == MT_ZIMA)
    {
        return 0;
    }
    if (pora_roku() == MT_JESIEN)
    {
        strr = "Niewysoka, lecz posiadaj^aca du^ze li^scie papro^c "+
            "ugina si^e do ziemi pod ci^e^zarem zal^a^zk^ow "+
            "znajduj^acych si^e pod listkami. Jej przebarwiaj^ace "+
            "si^e na br^azowo li^scie, powoli usychaj^ace, "+
            "przypominaj^a o nadchodz^acej zimie. Mi^edzy jej "+
            "listkami wiele ma^lych stworzonek znalaz^lo schronienie.\n";
        return strr;
    }
    else
    {
        strr = "Niewysoka, lecz posiadaj^aca du^ze li^scie papro^c "+
            "ugina si^e do ziemi pod ci^e^zarem zal^a^zk^ow "+
            "znajduj^acych si^e pod listkami. Jej soczysty, zielony "+
            "kolor o^zywia miejsce. Mi^edzy jej listkami wiele "+
            "ma^lych stworzonek znalaz^lo schronienie.\n";
        return strr;
    }
}

string opis_krzewow()
{
    string st;
    if (pora_roku() == MT_ZIMA)
    {
        return 0;
    }
    if (pora_roku() == MT_JESIEN)
    {
        st = "Bujne, zielono - czerwone li^scie tworz^a co^s na "+
            "kszta^lt sp^ojnej kuli, na kt^orej wierzchu znajduj^a "+
            "si^e liczne pozosta^lo^sci po niegdy^s pi^eknych "+
            "kwiatach.\n";
        return st;
    }
    else
    {
        st = "Bujne, w^sciekle zielone li^scie tworz^a co^s na kszta^lt "+
            "sp^ojnej kuli, na kt^orej wierzchu znajduj^a si^e liczne "+
            "kwiaty o mlecznej barwie. Ka^zdy kwiat ma potr^ojn^a "+
            "warstw^e p^latk^ow, co czyni je okazalszymi.\n";
        return st;
    }
}

public string
exits_description() 
{
    return "Drog^e mi^edzy drzewami mo^zna odnale^s^c ruszaj^ac "+
        "na p^o^lnocny zach^od, lub wsch^od.\n";
}