/**
 * Las Komanda
 *
 * autor Cairell
 * opisy Edrain & Aladriel
 * data  styczen 2009
 */
 
#include <stdproperties.h>
#include <mudtime.h>
#include "dir.h"
#include "pogoda.h"
#include <macros.h>

inherit LAS_ELFOW_RZEKA;

string dlugi_opis();

string opis_bluszczu();
string opis_kwiatow();
string opis_plyt();
string opis_blokow();
string opis_murow();


void create_las()
{
    set_short("W ruinach.");
    set_long(&dlugi_opis());

    add_exit(LAS_ELFOW_LOKACJE+ "las_84.c","nw");
    add_exit(LAS_ELFOW_LOKACJE+ "las_85.c","n");
    add_exit(LAS_ELFOW_LOKACJE+ "las_74.c","se");

    add_item(({"bluszcz","bluszcze"}), &opis_bluszczu());
    add_item(({"kwiat","kwiaty"}), &opis_kwiatow());
    add_item(({"p^lyt^e","p^lyty"}), &opis_plyt());
    add_item(({"blok","bloki"}), &opis_blokow());
    add_item(({"mur","mury"}), &opis_murow());
}

string dlugi_opis()
{
    string str = "";
    if(pora_roku() == MT_ZIMA)
    {
        if (jest_dzien() == 1)
        {
            if (ZACHMURZENIE(TO) <= 2)
            {
            str += "Smugi ^swiat^la opadaj^ac kaskadami na ziemi^e "+
            "wydobywaj^a z mroku lasu stare ruiny elfich"+
            "budowli. ";
            }
            else
            {
            str += "";
            } 
        str += "^Snie^znobia^le mury zniszczone przez czas "+
        "wznosz^a si^e w g^or^e i urywaj^a ca^lkiem nagle. "+
        "Ukruszone marmurowe bloki ^sciel^a si^e pod stopami "+
        "pokryte bia^lym ^sniegiem. Spadaj^acy z mur^ow ^snieg "+
        "utworzy^l wielk^a zasp^e, w pobli^zu kt^orej stoi"+
        "bia^la marmurowa ^laweczka."; 
        }
        else //noc
        {
            if (ZACHMURZENIE(TO) <= 2)
            {
            str += "Smugi ksi^e^zycowej po^swiaty opadaj^a kaskadami na "+
            "ziemi^e wydobywaj^ac z mroku lasu stare ruiny "+
            "elfich budowli. ";
            }
            else
            {
            str += "";
            } 
        str += "^Snie^znobia^le mury zniszczone "+
        "przez czas wznosz^a si^e w g^or^e i urywaj^a ca^lkiem "+
        "nagle. Ukruszone marmurowe bloki ^sciel^a si^e pod "+
        "stopami pokryte bia^lym ^sniegiem."+
        "Spadaj^acy z mur^ow ^snieg utworzy^l wielk^a zasp^e, w "+
        "pobli^zu kt^orej stoi bia^la marmurowa ^laweczka."; 
        }
    }
    else //Lato_wiosna_jesien
    {
        if (jest_dzien() == 1)
        {
            if (ZACHMURZENIE(TO) <= 2)
            {
            str += "Smugi ^swiat^la opadaj^ac kaskadami na ziemi^e "+
            "wydobywaj^a z mroku lasu stare ruiny elfich "+
            "budowli. ";
            }
            else
            {
            str += "";
            } 
        str += "^Snie^znobia^le mury zniszczone przez czas "+
        "wznosz^a si^e w g^or^e i urywaj^a ca^lkiem nagle. "+
        "Ukruszone marmurowe bloki ^sciel^a si^e pod stopami "+
        "poro^sni^ete p^lo^z^acymi si^e kwiatami i bluszczem. "+
        "Pow^oj oplata p^lyty wspinaj^ac si^e na bia^l^a "+
        "^laweczk^e, kt^ora wydaje si^e nietkni^eta przez czas."; 
        }
        else //noc
        {
            if (ZACHMURZENIE(TO) <= 2)
            {
            str += "Smugi ksi^e^zycowej po^swiaty opadaj^a kaskadami na "+
            "ziemi^e wydobywaj^ac z mroku lasu stare ruiny "+
            "elfich budowli. ";
            }
            else
            {
            str += "";
            } 
        str += "^snie^znobia^le mury zniszczone "+
        "przez czas wznosz^a si^e w g^or^e i urywaj^a ca^lkiem "+
        "nagle. Ukruszone marmurowe bloki ^sciel^a si^e pod "+
        "stopami poro^sni^ete p^lo^z^acymi si^e kwiatami i "+
        "bluszczem. Pow^oj oplata p^lyty wspinaj^ac si^e na "+
        "bia^l^a ^laweczk^e, kt^ora wydaje si^e nietkni^eta przez "+
        "czas."; 
        }
    }

str+="\n";
return str;
}

string opis_kwiatow()
{
    string str;
    if (pora_roku() == MT_JESIEN)
    {
    str += "Bujne, zielono- czerwone li^scie tworz^a co^s na "+
    "kszta^lt sp^ojnej kuli, na kt^orej wierzchu znajduj^a "+
    "si^e liczne pozosta^lo^sci po niegdy^s pi^eknych "+
    "kwiatach.\n"; 
    return str;
    }
    else if(pora_roku() == MT_ZIMA)
    {
    return 0;
    }
    else
    {
    str += "Bujne, w^sciekle zielone li^scie tworz^a co^s na "+
    "kszta^lt sp^ojnej kuli, na kt^orej wierzchu znajduj^a "+
    "si^e liczne kwiaty o mlecznej barwie. Ka^zdy kwiat "+
    "ma potr^ojn^a warstw^e p^latk^ow, co czyni je "+
    "okazalszymi.\n"; 
    return str;
    }
}

string opis_bluszczu()
{
    string str;
    if (pora_roku() == MT_ZIMA)
    {
    return 0;
    }
    else
    {
    str = "Delikatne p^edy p^lo^z^acej si^e ro^sliny "+
    "oplataj^a bia^le bloki marmurowych ruin. "+
    "Tworz^a na ich powierzchni niemal paj^ecz^a "+
    "sie^c us^lan^a zielonkawymi, do^s^c du^zymi "+
    "li^s^cmi. W ko^ncu wspinaj^a si^e w g^or^e "+
    "po resztkach ^scian i ^luk^ow, opadaj^ac "+
    "szeleszcz^ac^a zas^lon^a ku ziemi.\n";
    return str;
    }
}

string opis_plyt()
{
    string str;
    if (jest_dzien() == 1)
    {
        if (ZACHMURZENIE(TO) <= 2)
        {
        str =  "Bia^le, marmurowe p^lyty odbijaj^a promienie "+
        "sloneczne, kt^ore zalewa ca^l^a okolic^e. ";
        }
        else
        {
            str = "";
        }
    str += "Roztrzaskany kamie^n w wielu miejscach jest "+
    "porysowany i wy^z^lobiony przez czas i natur^e. "+
    "W niekt^orych miejscach p^lyty zachodz^a "+
    "na siebie tworz^ac wiele miejsc o kt^ore "+
    "mo^zna si^e potkn^a^c.\n";
    return str;
    }
    else
    {
        if (ZACHMURZENIE(TO) <= 2)
        {
        str = "Bia^le, marmurowe p^lyty odbijaj^a smugi blasku "+
        "ksiezyca, kt^ore zalewaja ca^l^a okolic^e.";
        }
        else
        {
        str = "";
        }
    str += "Roztrzaskany kamie^n w wielu miejscach "+
    "wy^z^lobiony przez czas i natur^e. W niekt^orych "+
    "miejscach p^lyty zachodz^a na siebie tworz^ac "+
    "wiele miejsc o kt^ore mo^zna si^e potkn^a^c.\n";
    return str;
    }
}

string opis_blokow()
{
    string str;
    str = "P^ekni^ete w wielu miejscach bloki marmuru musz^a "+
    "tu le^ze^c od bardzo dawna. Niegdysiejsze "+
    "cz^e^sci okaza^lych mur^ow le^z^a teraz bez^ladnie "+
    "porozrzucane po okolicy.\n";
    return str;
}

string opis_murow()
{
    string str;
    str = "Bia^le, pot^e^zne bloki marmuru ukszta^ltowane "+
    "niegdy^s w okaza^l^a budowl^e obecnie s^a tylko "+
    "sm^etnymi resztkami jej dawnej ^swietno^sci. "+
    "W wielu miejscach pop^ekane i wyszczerbione "+
    "s^a ju^z jedynie zapomnianymi ruinami.\n";
    return str;
}
