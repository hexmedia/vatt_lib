/* Las komanda
   Autor: Avard 
   Opis : Edrain */

#include <stdproperties.h>
#include <macros.h>
#include <mudtime.h>
#include "dir.h"
inherit LAS_ELFOW_GESTY;

string dlugi_opis();

string opis_dzwonkow();
string opis_wrzosow();
string opis_fiolkow();
string opis_zawilcow();
string opis_konwalii();

int wez(string str);

void
create_las()
{
    set_short("Po^srod buk�w.");
    set_long(&dlugi_opis());
    add_exit(LAS_ELFOW_LOKACJE+ "las_41.c","w");
    add_exit(LAS_ELFOW_LOKACJE+ "las_42.c","s");
    
    if(!CZY_JEST_SNIEG(this_object()))
    {
        add_item("mech","Po�acie soczysto zielonych �ody�ek wznosz� si� "+
        "nieznacznie nad ziemi�. Mi�dzy drobnymi listkami gromadz� "+
        "du�e ilo�ci wody, przez co ca�o�� jest wyj�tkowo wilgotna. "+
        "Ziemia zas�ana tymi ro�linkami wygl�da jakby by�a pokryta "+
        "naturalnym dywanem.\n");
    }
    add_item(({"dzwonek","dzwonki"}), &opis_dzwonkow());
    add_item(({"zawilca","zawilce"}), &opis_zawilcow());
    add_item(({"wrzos","wrzosy"}), &opis_wrzosow());
    add_item(({"fio^lka","fio^lki"}), &opis_fiolkow());
    add_item(({"konwalie"}), &opis_konwalii());
}

public void
init()
{
    ::init();
    add_action(wez,"we�");
    add_action(wez,"zbieraj");
    add_action(wez,"zbierz");
}

string dlugi_opis()
{
    string str = "";
    
    if(jest_dzien()) //Dzie�
    {
        if(CZY_JEST_SNIEG(this_object()))
        {
            str+="W tym miejscu z�owroga cisza gra na strunach wiatru. "+
            "Wysokie, strzeliste buki pn� si� pod niebosk�on stwarzaj�c "+
            "wra�enie ma�o�ci wszelkich stworze� pozostaj�cych na dole. "+
            "Przez ci�ki baldachim koron nie przenikaj� �adne promienie "+
            "�wiat�a zgrabnie zatrzymywane mozaik� ga��zi i bia�ego puchu "+
            "g�sto pokrywaj�cego konary. Pod stopami ci�gnie si� bia�y "+
            "dywan �niegu dodaj� uroku temu miejscu.";
        }
        else if(pora_roku() == MT_WIOSNA || pora_roku() == MT_LATO)
        {
            str+="W tym miejscu z�owroga cisza gra na strunach wiatru. "+
            "Wysokie, strzeliste buki pn� si� pod niebosk�on stwarzaj�c "+
            "wra�enie ma�o�ci wszelkich stworze� pozostaj�cych na dole. "+
            "Przez ci�ki baldachim koron nie przenikaj� �adne promienie "+
            "�wiat�a zgrabnie zatrzymywane mozaik� ga��zi. Przyt�umione "+
            "�wiat�o oplata p�mrokiem krajobraz puszczy. Pod stopami "+
            "ci�gn� si� krzewinki konwalii, fio�k�w i zawilc�w, "+
            "przemieszane z drobnymi orzeszkami buk�w. Mchy, oplataj� drzewa "+
            "i pn� si� na srebrzystoszar� kor�, dodaj�c uroku temu miejscu.";
        }
        else 
        {
            str+="W tym miejscu z�owroga cisza gra na strunach wiatru. "+
            "Wysokie, strzeliste buki pn� si� pod niebosk�on stwarzaj�c "+
            "wra�enie ma�o�ci wszelkich stworze� pozostaj�cych na dole. "+
            "Przez ci�ki baldachim koron nie przenikaj� �adne promienie "+
            "�wiat�a zgrabnie zatrzymywane mozaik� ga��zi i r�nokolorowych "+
            "li�ci. Przyt�umione �wiat�o oplata p�mrokiem krajobraz "+
            "puszczy. Pod stopami ci�gn� si� krzewinki wrzos�w i dzwonk�w "+
            "le�nych, przemieszane z drobnymi orzeszkami buk�w. Suche mchy, "+
            "oplataj� drzewa i pn� si� na srebrzystoszar� kor�, dodaj�c "+
            "uroku temu miejscu.";
        }
    }
    else //Noc
    {
        if(CZY_JEST_SNIEG(this_object()))
        {
            str+="W tym miejscu z�owroga cisza gra na strunach wiatru. "+
            "Wysokie, strzeliste buki pn� si� pod niebosk�on stwarzaj�c "+
            "wra�enie ma�o�ci wszelkich stworze� pozostaj�cych na dole. "+
            "Przez ci�ki baldachim koron nie przenikaj� �adne promienie "+
            "ksi�ycowego �wiat�a zgrabnie zatrzymywane mozaik� ga��zi i "+
            "bia�ego puchu g�sto pokrywaj�cego konary. Sino blady krajobraz "+
            "ci�gnie si� w dal, znikaj�c w mroku. Pod stopami ci�gnie si� "+
            "bia�y dywan �niegu dodaj� uroku temu miejscu.";
        }
        else if(pora_roku() == MT_WIOSNA || pora_roku() == MT_LATO)
        {
            str+="W tym miejscu z�owroga cisza gra na strunach wiatru. "+
            "Wysokie, strzeliste buki pn� si� pod niebosk�on stwarzaj�c "+
            "wra�enie ma�o�ci wszelkich stworze� pozostaj�cych na dole. "+
            "Przez ci�ki baldachim koron nie przenikaj� �adne promienie "+
            "ksi�ycowego �wiat�a zgrabnie zatrzymywane mozaik� ga��zi. "+
            "Przyt�umione cienie oplataj� mrokiem krajobraz puszczy. Pod "+
            "stopami ci�gn� si� krzewinki konwalii, fio�k�w i zawilc�w, "+
            "przemieszane z drobnymi orzeszkami buk�w. Mchy, oplataj� "+
            "drzewa i pn� si� na srebrzystoszar� kor�, dodaj�c uroku temu "+
            "miejscu.";
        }
        else
        {
            str+="W tym miejscu z�owroga cisza gra na strunach wiatru. "+
            "Wysokie, strzeliste buki pn� si� pod niebosk�on stwarzaj�c "+
            "wra�enie ma�o�ci wszelkich stworze� pozostaj�cych na dole. "+
            "Przez ci�ki baldachim koron nie przenikaj� �adne promienie "+
            "ksi�ycowego �wiat�a zgrabnie zatrzymywane mozaik� ga��zi i "+
            "r�nokolorowych li�ci. Przyt�umione cienie oplataj� "+
            "p�mrokiem krajobraz puszczy. Pod stopami ci�gn� si� krzewinki "+
            "wrzos�w i dzwonk�w le�nych, przemieszane z drobnymi orzeszkami "+
            "buk�w. Suche mchy, oplataj� drzewa i pn� si� na srebrzystoszar� "+
            "kor�, dodaj�c uroku temu miejscu.";
        }
    }
    str += "\n";
    return str;
}

string opis_zawilcow()
{
    string str;
    if (pora_roku() == MT_WIOSNA)
    {
    str += "Zawilec gajowy jest pojedynczym kwiatem o d^lugiej "+
    "delikatnej ^lody^zce wyrastaj^acej z k^l^acza. "+
    "Poszarpane listki, zazwyczaj tylko trzy "+
    "wyst^epuj^ace w ok^o^lkach i wygl^adaj^a jak szpony "+
    "jakiej^s zjawy. Aktualnie ka^zda ^lody^zka zwie^nczona "+
    "jest prze^slicznym bia^lym kwiatkiem, sk^ladaj^acym "+
    "si^e z pi^eciu lub siedmiu p^latk^ow. Soki tej "+
    "ro^sliny s^a silnie truj^ace i powoduj^a zapalenie "+
    "nerek i uk^ladu pokarmowego, wywo^luj^ac silne "+
    "wymioty i biegunk^e.\n"; 
    return str;
    }
    else
    {
    return 0;
    }
}

string opis_wrzosow()
{
    string str;
    if (pora_roku() == MT_JESIEN)
    {
    str = "Ta zimozielona dwupienna krzewinka o listkach "+
    "rozes^lanych po ziemi, p^lo^zy si^e g^esto "+
    "wsz^edzie dooko^la, tworz^ac wyciszaj^acy dla "+
    "krok^ow ciemnozielony dywan. Jej drobne, "+
    "igie^lkowate li^scie u^lo^zone s^a naprzeciw siebie "+
    "i sprawiaj^a mylne wra^zenie bardzo ubogich, "+
    "jednak s^a one tylko dodatkiem do pi^eknych "+
    "r^o^zowo- fioletowych kwiat^ow w pe^lni "+
    "rozkwitaj^acych wczesn^a jesieni^a, r^ownie "+
    "drobnych, a jednak zachwycaj^acych swym pi^eknem, "+
    "kt^ore maluje ^swiat w czasie gdy prawie wszelkie "+
    "inne ozdoby przyrody ju^z przekwit^ly.\n";
    return str;
    }
    else
    {
    return 0;
    }
}

string opis_dzwonkow()
{
    string str;
    if (pora_roku() == MT_LATO)
    {
    str = "Wiotkie ^lody^zki wznosz^a si^e kilkadziesi^at "+
    "centymetr^ow nad ziemi^e, ich ko^nce zwieszaj^a "+
    "si^e w stron^e pod^lo^za obci^a^zone okaza^lymi "+
    "kwiatami. Fioletowe, zros^le p^latki tworz^a kielich "+
    "z jasno^z^o^ltym ^srodkiem. Odurzaj^acy zapach "+
    "dzwonk^ow le^snych wabi owady.\n";
    return str;
    }
    else
    {
    return 0;
    }
}

string opis_fiolkow()
{
    string str;
    if (pora_roku() == MT_WIOSNA)
    {
    str = "P^lo^z^aca si^e po ziemi bylina jest teraz g^esto "+
    "upstrzona pi^eknie pachn^acymi fioletowymi "+
    "kwiatami. Delikatne kwiaty wyniesione s^a nad "+
    "ziemie na wysokich szypu^lkach, ka^zdy z nich "+
    "sk^lada si^e z pi^eciu p^latk^ow dw^och "+
    "skierowanych w g^or^e i trzech w d^o^l. "+
    "Zielone listki, kt^ore dodatkowo eksponuj^a "+
    "pi^ekno koloru fio^lk^ow, maj^a kszta^lt sercowaty "+
    "i s^a lekko karbowane od g^ory.\n";
    return str;
    }
    else if (pora_roku() == MT_LATO)
    {
    str = "Na p^lo^z^acej si^e po ziemi bylinie nie wida^c "+
    "ju^z ^slad^ow niedawno pi^eknie kwitn^acych, "+
    "drobnych p^latk^ow fio^lk^ow. Z wonnej ro^sliny "+
    "pozosta^ly ^lody^zki i ciemnozielone, sercowate "+
    "li^scie, kt^ore t^lumi^a wszelkie odg^losy "+
    "zwierz^at i podr^o^znych.\n";
    return str;
    }
    else return 0;
}
string
opis_konwalii()
{
    string str;
    if(pora_roku() == MT_WIOSNA || pora_roku() == MT_LATO)
    {
        str = " Ma�e kwiaty g�sto rosn� na ziemi. Na wiotkich przygi�tych "+
        "do ziemi �ody�kach wyrastaj� drobne �nie�nobia�y kwiatki. Ca�o�� "+
        "okryta jest pojedynczym soczy�cie zielonym li�ciem o r�wnoleg�ej "+
        "nerwacji.\n";
        return str;
    }
    else return 0;
 }

int wez(string str)
{
    if(query_verb() ~= "we�")
        notify_fail("We� co?\n");
    if(query_verb() ~= "zbieraj")
        notify_fail("Zbieraj co?\n");
    if(query_verb() ~= "zbierz")
        notify_fail("Zbierz co?\n");
        
    if (!str) return 0;
    if (!strlen(str)) return 0;
    
    if(pora_roku() == MT_ZIMA)
        return 0;
    
    if(query_verb() ~= "we�")
    {
        if(!parse_command(str,environment(TP),"'orzech' / 'orzechy'"))
            return 0;
    }
    else 
    {
        if(!parse_command(str,environment(TP),"'orzechy'"))
            return 0;
    }
    write("Zaczynasz zbiera� orzechy.\n");
    saybb(QCIMIE(TP,PL_MIA)+" zaczyna zbiera� orzechy.\n");
    clone_object(LAS_ELFOW_PRZEDMIOTY+"paraliz_orzechy.c")->move(TP);
    return 1;
}
