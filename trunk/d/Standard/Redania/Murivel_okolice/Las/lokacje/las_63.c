/* Las komanda
   Autor: Avard 
   Opis : Edrain */

#include <stdproperties.h>
#include <macros.h>
#include <mudtime.h>
#include "dir.h"
inherit LAS_ELFOW_GESTY;

string dlugi_opis();

void
create_las()
{
    set_short("W puszczy");
    set_long(&dlugi_opis());
    add_exit(LAS_ELFOW_LOKACJE+ "las_66.c","nw");
    add_exit(LAS_ELFOW_LOKACJE+ "las_62.c","w");
    add_exit(LAS_ELFOW_LOKACJE+ "las_55.c","s");
    
    if(!CZY_JEST_SNIEG(this_object()))
    {
        add_item("mech","Po�acie soczysto zielonych �ody�ek wznosz? si� "+
        "nieznacznie nad ziemi�. Mi�dzy drobnymi listkami gromadz? "+
        "du�e ilo?ci wody, przez co ca�o?� jest wyj?tkowo wilgotna. "+
        "Ziemia zas�ana tymi ro?linkami wygl?da jakby by�a pokryta "+
        "naturalnym dywanem.\n");
    }
}

string dlugi_opis()
{
    string str = "";
    
    if(jest_dzien()) //Dzie�
    {
        if(CZY_JEST_SNIEG(this_object()))
        {
            str+="Md�e �wiat�o dochodz�ce z g�stych koron buk�w, pokrytych "+
            "�niegiem, o�wietla strzeliste, wiekowe pnie nie poro�ni�te "+
            "ga��ziami, a jedynie ozdobione kor� srebrzystoszarego koloru. "+
            "Wydaje si�, �e �ycie tego miejsca skupione jest w koronach. "+
            "�piew ptak�w na przemian przeplatany j�kami wiatru dochodzi do "+
            "ziemi przyt�umiony, jakby z innej rzeczywisto�ci. Po ziemi "+
            "snuj� si� srebrne nitki mg�y, oplataj�c wszystko, co stanie na "+
            "ich drodze. Pnie statecznych drzew ci�gn� si� po horyzont, "+
            "nadaj�c temu miejscu jeszcze wi�kszej g��bi.";
        }
        else
        {
            str+="Md�e �wiat�o dochodz�ce z g�stych koron buk�w o�wietla "+
            "strzeliste, wiekowe pnie nie poro�ni�te ga��ziami, a jedynie "+
            "ozdobione kor� srebrzystoszarego koloru. Wydaje si�, �e �ycie "+
            "tego miejsca skupione jest w koronach. �piew ptak�w na przemian "+
            "przeplatany j�kami wiatru dochodzi do ziemi przyt�umiony, jakby "+
            "z innej rzeczywisto�ci. Po ziemi snuj� si� srebrne nitki mg�y, "+
            "oplataj�c wszystko, co stanie na ich drodze. Pnie statecznych "+
            "drzew ci�gn� si� po horyzont, nadaj�c temu miejscu jeszcze "+
            "wi�kszej g��bi.";
        }
    }
    else //Noc
    {
        if(CZY_JEST_SNIEG(this_object()))
        {
            str+="Md�e �wiat�o dochodz�ce z g�stych koron, pokrytych "+
            "�niegiem, o�wietla na sino strzeliste, wiekowe pnie nie "+
            "poro�ni�te ga��ziami, a jedynie ozdobione kor� "+
            "srebrzystoszarego koloru. Wydaje si�, �e �ycie tego miejsca "+
            "skupione jest w koronach. �piew ptak�w na przemian przeplatany "+
            "j�kami wiatru dochodzi do ziemi przyt�umiony, jakby z innej "+
            "rzeczywisto�ci. Po ziemi snuj� si� srebrne nitki mg�y, "+
            "oplataj�c wszystko, co stanie na ich drodze. Pnie statecznych "+
            "drzew ci�gn� si� po horyzont, nadaj�c temu miejscu jeszcze "+
            "wi�kszej g��bi.";
        }
        else
        {
            str+="Md�e �wiat�o dochodz�ce z g�stych koron buk�w o�wietla na "+
            "sino strzeliste, wiekowe pnie nie poro�ni�te ga��ziami, a "+
            "jedynie ozdobione kor� srebrzystoszarego koloru. Wydaje si�, �e "+
            "�ycie tego miejsca skupione jest w koronach. Pohukiwanie s�w na "+
            "przemian przeplatane j�kami wiatru dochodzi do ziemi "+
            "przyt�umione, jakby z innej rzeczywisto�ci. Po ziemi snuj� si� "+
            "srebrne nitki mg�y, oplataj�c wszystko, co stanie na ich "+
            "drodze. Pnie statecznych drzew ci�gn� si� po horyzont, nadaj�c "+
            "temu miejscu jeszcze wi�kszej g��bi.";
        }
    }
    str += "\n";
    return str;
}
