/**
 * Las Komanda
 *
 * autor Cairell
 * opisy Edrain & Aladriel
 * data  styczen 2009
 */

#include <stdproperties.h>
#include <mudtime.h>
#include "dir.h"
inherit LAS_ELFOW_RZEKA;

string dlugi_opis();
string opis_paproci();

void
create_las()
{
    set_short("W lesie nad brzegiem rzeki.");
    set_long(&dlugi_opis());

    add_exit(LAS_ELFOW_LOKACJE+ "las_6.c","n");
    add_exit(LAS_ELFOW_LOKACJE+ "las_7.c","ne");
    add_exit(LAS_ELFOW_LOKACJE+ "las_9.c","e");
    add_item(({"krzew","r^o^z^e", "krew r^o^zy"}), &opis_paproci());
}

string dlugi_opis()
{
    string str = ""; 
    
    str+="Panuje tu niezm^acona krzykami ludzi cisza. Jedyne, "+
        "co mo^zesz us�ysze^c, to odg^losy natury. W oddali "+
        "sw^a pie^s^n szumi rzeka, kt^orej wartki nurt brnie "+
        "na prz^od nie zwa^zaj^ac na up^lyw czasu, a korony "+
        "drzew ";

    if (jest_dzien() == 1)
    {
        if(pora_roku() == MT_WIOSNA)
        {
            str += "rozbrzmiewaj^a trelami ptak^ow wy�piewuj^acych "+
                "pie^sni na cze^s^c dziewiczej natury. Stare leszczyny "+
                "rosn^ace w tym miejscu ju^z od wielu lat "+
                "sprawiaj^a, ^ze mo^zna tu zapomnie^c niemal "+
                "o ka^zdych troskach codziennego ^zycia. "+
                "G^esta, soczy^scie zielona i ^swie^za trawa "+
                "porasta t^a okolice. Pod konarami rozrastaj^a "+
                "si^e krzewy dzikiej r^o^zy, nasycaj^acej "+
                "powietrze s^lodk^a woni^a.";
        }
        if(pora_roku() == MT_LATO)
        {
            str += "rozbrzmiewaj^a trelami ptak^ow wy�piewuj^acych "+
                "pie^sni na cze^s^c dziewiczej natury. Stare leszczyny "+
                "rosn^ace w tym miejscu ju^z od wielu lat "+
                "sprawiaj^a, ^ze mo^zna tu zapomnie^c niemal "+
                "o ka^zdych troskach codziennego ^zycia. "+
                "G^esta, soczy^scie zielona i ^swie^za trawa "+
                "porasta t^a okolice. Pod konarami rozrastaj^a "+
                "si^e krzewy dzikiej r^o^zy, nasycaj^acej "+
                "powietrze s^lodk^a woni^a.";
        }
        if(pora_roku() == MT_JESIEN)
        {
            str += "rozbrzmiewaj^a relami ptak^ow wy^spiewuj^acych "+
                "pie^sni na cze^s^c dziewiczej natury. Stare leszczyny "+
                "rosn^ace w tym miejscu ju^z od wielu lat "+
                "sprawiaj^a, ^ze mo^zna tu zapomnie^c niemal "+
                "o ka^zdych troskach codziennego ^zycia. G^esta, "+
                "zielona trawa, kt^ora powoli zmienia barw^e na "+
                "br^azow^a bujnie porasta okolice. Pod konarami "+
                "rozrastaj^a si^e krzewy jesiennej dzikiej r^o^zy, "+
                "nasycaj^acej powietrze s^lodk^a woni^a.";
        }
        if(pora_roku() == MT_ZIMA)
        {
            str += "rozbrzmiewaj^a trelami ptak^ow przewrzaskuj^acych "+
                "si^e w bitwie o ostatnie owoce jesieni, jedyny pokarm "+
                "w te d^lugie, zimowe noce i zbyt kr^otkie dni, "+
                "aby znale^x^c cokolwiek do jedzenia. Leszczyny "+
                "jeszcze niedawno wydaj^ace pyszne orzechy, "+
                "teraz spowija g^esty puch ^sniegu, chroni^acy "+
                "nowe p^aczki przed przemarzni^eciem. ";
        }
    }
    else //noc
    {
        str += "przes^laniaj^a granatowe, nocne niebo i tworz^a "+
            "doko�a setki wi^ekszych i mniejszych cieni.";
    }

str+="\n";
return str;
}

string opis_paproci()
{
    string strr;
    if (pora_roku() == MT_ZIMA)
    {
        strr += "Dziko rosn^acy krzew dzikiej r^o^zy okryty "+
            "jest teraz grub^a warst^a bielutkiego puchu, zdaj^acej "+
            "boni^c dost^epu do du^zych p^aczk^ow. W kilku miejscach "+
            "kora p^ed^ow zosta^la przy samej ziemi ogryziona, "+
            "najpewniej przez g^loduj^ace o tej porze roku zaj^ace.";
        return strr;
    }
    if (pora_roku() == MT_JESIEN)
    {
        strr = "Dziko rosn^acy krzew dodaje uroku temu miejscu. "+
            "Bielutkie p^o^xnojesienne kwiaty wychylaj� spomi^edzy "+
            "oliwkowo - br^azowych li^sci swe dumne g^l^owki. "+
            "Ga^l^azki krzaku opr^ocz li^sci pokryte s^a tak^ze "+
            "ostrymi du^zymi kolcami.\n";
        return strr;
    }
    else
    {
        strr = "Dziko rosn^acy krzew dodaje uroku temu miejscu. "+
            "Bielutkie kwiaty wychylaj^a spomi^edzy ciemnozielonych "+
            "li^sci swe dumne g^l^owki. Niekt^ore zdaj^a si^e dopiero "+
            "rozwija^c, inne s^a ju^z w pe^lni otwarte. Ga^l^azki "+
            "krzaku opr^ocz li^sci pokryte s^a tak^ze ostrymi du^zymi "+
            "kolcami.\n";
        return strr;
    }
}

public string
exits_description() 
{
    return "Drog^e mi^edzy drzewami mo^zna odnale^s^c udaj^ac si^e "+
        "na p^o^lnoc, p^o^lnocny wsch^od i wsch^od.\n";
}