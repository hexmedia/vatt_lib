#include <stdproperties.h>
#include <mudtime.h>
#include "dir.h"
inherit LAS_ELFOW_GESTY;

string dlugi_opis();

void
create_las()
{
    set_short("Las");
    set_long(&dlugi_opis());
    add_exit(LAS_ELFOW_LOKACJE+ "las_gesty.c","w");

}
string dlugi_opis()
{
    string str;
    if(jest_dzien() == 1)
    {
    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="Zimowate.";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str+="Wiosnowate.";
    }
    else if(pora_roku() == MT_LATO)
    {
        str+="Latowate.";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str+="Jesieniowate.";
    }
    }
    if(jest_dzien() == 0)
    {
    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="Zimowate.";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str+="Wiosnowate.";
    }
    else if(pora_roku() == MT_LATO)
    {
        str+="Latowate.";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str+="Jesieniowate.";
    }
    }
    str += "\n";
    return str;
}
