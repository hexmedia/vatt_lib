/* Las komanda
   Autor: Avard 
   Opis : Edrain */

#include <stdproperties.h>
#include <macros.h>
#include <mudtime.h>
#include "dir.h"
inherit LAS_ELFOW_GESTY;

string dlugi_opis();

string opis_fiolkow();
string opis_zawilcow();
string opis_dzwonkow();

void
create_las()
{
    set_short("Po^srod buk�w.");
    set_long(&dlugi_opis());
    add_exit(LAS_ELFOW_LOKACJE+ "las_50.c","nw");
    add_exit(LAS_ELFOW_LOKACJE+ "las_30.c","sw");
    add_exit(LAS_ELFOW_LOKACJE+ "las_52.c","s");
    add_exit(LAS_ELFOW_LOKACJE+ "las_60.c","se");
    add_exit(LAS_ELFOW_LOKACJE+ "las_66.c","ne");
    
    if(!CZY_JEST_SNIEG(this_object()))
    {
        add_item("mech","Po�acie soczysto zielonych �ody�ek wznosz? si� "+
        "nieznacznie nad ziemi�. Mi�dzy drobnymi listkami gromadz? "+
        "du�e ilo?ci wody, przez co ca�o?� jest wyj?tkowo wilgotna. "+
        "Ziemia zas�ana tymi ro?linkami wygl?da jakby by�a pokryta "+
        "naturalnym dywanem.\n");
    }
    add_item(({"zawilca","zawilce"}), &opis_zawilcow());
    add_item(({"dzwonek","dzwonki"}), &opis_dzwonkow());
    add_item(({"fio^lka","fio^lki"}), &opis_fiolkow());
}

string dlugi_opis()
{
    string str = "";
    
    if(jest_dzien()) //Dzie�
    {
        if(CZY_JEST_SNIEG(this_object()))
        {
            str+="Dooko�a rozci�ga si� pas buk�w. Ich wysokie, g�adkie pnie "+
            "strzelaj� w g�r�, aby na ko�cu przej�� w g�ste korony "+
            "ca�kowicie pozbawione li�ci. Tworz� jakby �wi�tyni� z "+
            "kolumnami i pi�knym sklepieniem utworzonym z ga��zi zas�anymi "+
            "�niegiem. �wiat�o zalewa ziemi� i razi w oczy odbijaj�c si� "+
            "ostro od bia�ego �niegu. Wra�enie nienaturalno�ci pot�guj� "+
            "ciemne sylwetki drzew. Ziemia pokryta dywanem z puchu "+
            "ca�kowicie t�umi kroki, a jedyne odg�osy dostaj�ce si� tu z "+
            "g�rnych pi�ter lasu s� przyt�umione i odleg�e jakby "+
            "pochodzi�y z innego �wiata.";
        }
        else if(pora_roku() == MT_WIOSNA)
        {
            str+="Dooko�a rozci�ga si� pas buk�w. Ich wysokie, g�adkie pnie "+
            "strzelaj� w g�r�, aby na ko�cu przej�� w g�ste korony. Tworz� "+
            "jakby �wi�tyni� z kolumnami i pi�knym, pod�wietlonym przez "+
            "zielone refleksy sklepieniem. �wiat�o nie dostaje si� na "+
            "ziemi�, a wok� panuje mrok. Wra�enie nienaturalno�ci pot�guj� "+
            "ciemne sylwetki drzew. Ziemia pokryta mchem, przetykanym "+
            "gdzieniegdzie fio�kami i zawilcami ca�kowicie t�umi kroki, a "+
            "jedyne odg�osy dostaj�ce si� tu z g�rnych pi�ter lasu s� "+
            "przyt�umione i odleg�e jakby pochodzi�y z innego �wiata.";
        }
        else if(pora_roku() == MT_LATO)
        {
            str+="Dooko�a rozci�ga si� pas buk�w. Ich wysokie, g�adkie "+
            "pnie strzelaj� w g�r�, aby na ko�cu przej�� w g�ste korony. "+
            "Tworz� jakby �wi�tyni� z kolumnami i pi�knym, pod�wietlonym "+
            "przez zielone refleksy sklepieniem. �wiat�o nie dostaje si� "+
            "na ziemi�, a wok� panuje mrok. Wra�enie nienaturalno�ci "+
            "pot�guj� ciemne sylwetki drzew. Ziemia pokryta mchem, "+
            "przetykanym gdzieniegdzie fio�kami i dzwonkami le�nymi "+
            "ca�kowicie t�umi kroki, a jedyne odg�osy dostaj�ce si� tu z "+
            "g�rnych pi�ter lasu s� przyt�umione i odleg�e jakby "+
            "pochodzi�y z innego �wiata.";
        }
        else
        {
            str+="Dooko�a rozci�ga si� pas buk�w. Ich wysokie, g�adkie "+
            "pnie strzelaj� w g�r�, aby na ko�cu przej�� w g�ste korony. "+
            "Tworz� jakby �wi�tyni� z kolumnami i pi�knym, pod�wietlonym "+
            "przez zielone refleksy sklepieniem. �wiat�o nie dostaje si� na "+
            "ziemi�, a wok� panuje mrok. Wra�enie nienaturalno�ci pot�guj� "+
            "ciemne sylwetki drzew. Ziemia pokryta wyschni�tym mchem, "+
            "zas�anym r�nokolorowymi, opad�ymi li��mi, ca�kowicie t�umi "+
            "kroki, a jedyne odg�osy dostaj�ce si� tu z g�rnych pi�ter lasu "+
            "s� przyt�umione i odleg�e jakby pochodzi�y z innego �wiata.";
        }
    }
    else //Noc
    {
        if(CZY_JEST_SNIEG(this_object()))
        {
            str+="Dooko�a rozci�ga si� pas buk�w. Ich wysokie, g�adkie pnie "+
            "strzelaj� w g�r�, aby na ko�cu przej�� w g�ste korony "+
            "ca�kowicie pozbawione li�ci. Tworz� jakby �wi�tyni� z kolumnami "+
            "i pi�knym sklepieniem utworzonym z ga��zi zas�anymi �niegiem. "+
            "�wiat�o ksi�yca zalewa ziemi� sinym blaskiem odbijaj�cym si� "+
            "od bia�ego �niegu. Wra�enie nienaturalno�ci pot�guj� ciemne "+
            "sylwetki drzew. Ziemia pokryta dywanem z puchu ca�kowicie t�umi "+
            "kroki, a jedyne odg�osy dostaj�ce si� tu z g�rnych pi�ter lasu "+
            "s� przyt�umione i odleg�e jakby pochodzi�y z innego �wiata.";
        }
        else if(pora_roku() == MT_WIOSNA)
        {
            str+="Dooko�a rozci�ga si� pas buk�w. Ich wysokie, g�adkie pnie "+
            "strzelaj� w g�r�, aby na ko�cu przej�� w g�ste korony. Tworz� "+
            "jakby �wi�tyni� z kolumnami i pi�knym, pod�wietlonym przez "+
            "srebrzyste refleksy sklepieniem. �wiat�o ksi�yca nie dostaje "+
            "si� na ziemi�, a wok� panuje mrok. Wra�enie nienaturalno�ci "+
            "pot�guj� ciemne sylwetki drzew. Ziemia pokryta mchem, "+
            "przetykanym gdzieniegdzie fio�kami i zawilcami ca�kowicie t�umi "+
            "kroki, a jedyne odg�osy dostaj�ce si� tu z g�rnych pi�ter lasu "+
            "s� przyt�umione i odleg�e jakby pochodzi�y z innego �wiata.";
        }
        else if(pora_roku() == MT_LATO)
        {
            str+="Dooko�a rozci�ga si� pas buk�w. Ich wysokie, g�adkie pnie "+
            "strzelaj� w g�r�, aby na ko�cu przej�� w g�ste korony. Tworz� "+
            "jakby �wi�tyni� z kolumnami i pi�knym, pod�wietlonym przez "+
            "srebrzyste refleksy sklepieniem. �wiat�o ksi�yca nie dostaje "+
            "si� na ziemi�, a wok� panuje mrok. Wra�enie nienaturalno�ci "+
            "pot�guj� ciemne sylwetki drzew. Ziemia pokryta mchem, "+
            "przetykanym gdzieniegdzie fio�kami i dzwonkami le�nymi "+
            "ca�kowicie t�umi kroki, a jedyne odg�osy dostaj�ce si� tu z "+
            "g�rnych pi�ter lasu s� przyt�umione i odleg�e jakby pochodzi�y "+
            "z innego �wiata.";
        }
        else
        {
            str+="Dooko�a rozci�ga si� pas buk�w. Ich wysokie, g�adkie pnie "+
            "strzelaj� w g�r�, aby na ko�cu przej�� w g�ste korony. Tworz� "+
            "jakby �wi�tyni� z kolumnami i pi�knym, pod�wietlonym przez "+
            "srebrzyste refleksy sklepieniem. �wiat�o ksi�yca nie dostaje "+
            "si� na ziemi�, a wok� panuje mrok. Wra�enie nienaturalno�ci "+
            "pot�guj� ciemne sylwetki drzew. Ziemia pokryta wyschni�tym "+
            "mchem, zas�anym r�nokolorowymi, opad�ymi li��mi, ca�kowicie "+
            "t�umi kroki, a jedyne odg�osy dostaj�ce si� tu z g�rnych "+
            "pi�ter lasu s� przyt�umione i odleg�e jakby pochodzi�y z "+
            "innego �wiata.";
        }
    }
    str += "\n";
    return str;
}

string opis_zawilcow()
{
    string str;
    if (pora_roku() == MT_WIOSNA)
    {
    str += "Zawilec gajowy jest pojedynczym kwiatem o d^lugiej "+
    "delikatnej ^lody^zce wyrastaj^acej z k^l^acza. "+
    "Poszarpane listki, zazwyczaj tylko trzy "+
    "wyst^epuj^ace w ok^o^lkach i wygl^adaj^a jak szpony "+
    "jakiej^s zjawy. Aktualnie ka^zda ^lody^zka zwie^nczona "+
    "jest prze^slicznym bia^lym kwiatkiem, sk^ladaj^acym "+
    "si^e z pi^eciu lub siedmiu p^latk^ow. Soki tej "+
    "ro^sliny s^a silnie truj^ace i powoduj^a zapalenie "+
    "nerek i uk^ladu pokarmowego, wywo^luj^ac silne "+
    "wymioty i biegunk^e.\n"; 
    return str;
    }
    else
    {
    return 0;
    }
}

string opis_dzwonkow()
{
    string str;
    if (pora_roku() == MT_LATO)
    {
    str = "Wiotkie ^lody^zki wznosz^a si^e kilkadziesi^at "+
    "centymetr^ow nad ziemi^e, ich ko^nce zwieszaj^a "+
    "si^e w stron^e pod^lo^za obci^a^zone okaza^lymi "+
    "kwiatami. Fioletowe, zros^le p^latki tworz^a kielich "+
    "z jasno^z^o^ltym ^srodkiem. Odurzaj^acy zapach "+
    "dzwonk^ow le^snych wabi owady.\n";
    return str;
    }
    else
    {
    return 0;
    }
}

string opis_fiolkow()
{
    string str;
    if (pora_roku() == MT_WIOSNA)
    {
    str = "P^lo^z^aca si^e po ziemi bylina jest teraz g^esto "+
    "upstrzona pi^eknie pachn^acymi fioletowymi "+
    "kwiatami. Delikatne kwiaty wyniesione s^a nad "+
    "ziemie na wysokich szypu^lkach, ka^zdy z nich "+
    "sk^lada si^e z pi^eciu p^latk^ow dw^och "+
    "skierowanych w g^or^e i trzech w d^o^l. "+
    "Zielone listki, kt^ore dodatkowo eksponuj^a "+
    "pi^ekno koloru fio^lk^ow, maj^a kszta^lt sercowaty "+
    "i s^a lekko karbowane od g^ory.\n";
    return str;
    }
    else if (pora_roku() == MT_LATO)
    {
    str = "Na p^lo^z^acej si^e po ziemi bylinie nie wida^c "+
    "ju^z ^slad^ow niedawno pi^eknie kwitn^acych, "+
    "drobnych p^latk^ow fio^lk^ow. Z wonnej ro^sliny "+
    "pozosta^ly ^lody^zki i ciemnozielone, sercowate "+
    "li^scie, kt^ore t^lumi^a wszelkie odg^losy "+
    "zwierz^at i podr^o^znych.\n";
    return str;
    }
    else return 0;
}