/* Las komanda
   Autor: Avard 
   Opis : Edrain */

#include <stdproperties.h>
#include <mudtime.h>
#include "dir.h"
inherit LAS_ELFOW_GESTY;

string dlugi_opis();

void
create_las()
{
    set_short("Po^srod buk�w.");
    set_long(&dlugi_opis());
    add_exit(LAS_ELFOW_LOKACJE+ "las_56.c","s");
    add_exit(LAS_ELFOW_LOKACJE+ "las_57.c","se");
    add_exit(LAS_ELFOW_LOKACJE+ "las_68.c","n");
    
    if(!CZY_JEST_SNIEG(this_object()))
    {
        add_item("mech","Po�acie soczysto zielonych �ody�ek wznosz� si� "+
        "nieznacznie nad ziemi�. Mi�dzy drobnymi listkami gromadz� "+
        "du�e ilo�ci wody, przez co ca�o�� jest wyj�tkowo wilgotna. "+
        "Ziemia zas�ana tymi ro�linkami wygl�da jakby by�a pokryta "+
        "naturalnym dywanem.\n");
    }
}
string dlugi_opis()
{
    string str = "";
    
    if(jest_dzien()) //Dzie�
    {
        if(CZY_JEST_SNIEG(this_object()))
        {
            str+="Dooko�a rozci�ga si� pas buk�w. Ich wysokie, g�adkie pnie "+
            "strzelaj� w g�r�, aby na ko�cu przej�� w g�ste korony "+
            "ca�kowicie pozbawione li�ci. Tworz� jakby �wi�tyni� z "+
            "kolumnami i pi�knym sklepieniem utworzonym z ga��zi zas�anymi "+
            "�niegiem. �wiat�o zalewa ziemi� i razi w oczy odbijaj�c si� "+
            "ostro od bia�ego �niegu. Wra�enie nienaturalno�ci pot�guj� "+
            "ciemne sylwetki drzew. Ziemia pokryta dywanem z puchu "+
            "ca�kowicie t�umi kroki, a jedyne odg�osy dostaj�ce si� tu z "+
            "g�rnych pi�ter lasu s� przyt�umione i odleg�e jakby "+
            "pochodzi�y z innego �wiata.";
        }
        else if(pora_roku() == MT_WIOSNA)
        {
            str+="Dooko�a rozci�ga si� pas buk�w. Ich wysokie, g�adkie "+
            "pnie strzelaj� w g�r�, aby na ko�cu przej�� w g�ste korony. "+
            "Tworz� jakby �wi�tyni� z kolumnami i pi�knym, pod�wietlonym "+
            "przez zielone refleksy sklepieniem. �wiat�o nie dostaje si� "+
            "na ziemi�, a wok� panuje mrok. Wra�enie nienaturalno�ci "+
            "pot�guj� ciemne sylwetki drzew. Ziemia pokryta mchem, "+
            "przetykanym gdzieniegdzie fio�kami i zawilcami ca�kowicie "+
            "t�umi kroki, a jedyne odg�osy dostaj�ce si� tu z g�rnych "+
            "pi�ter lasu s� przyt�umione i odleg�e jakby pochodzi�y z "+
            "innego �wiata.";
        }
        else if(pora_roku() == MT_LATO)
        {
            str+="Dooko�a rozci�ga si� pas buk�w. Ich wysokie, g�adkie "+
            "pnie strzelaj� w g�r�, aby na ko�cu przej�� w g�ste korony. "+
            "Tworz� jakby �wi�tyni� z kolumnami i pi�knym, pod�wietlonym "+
            "przez zielone refleksy sklepieniem. �wiat�o nie dostaje si� "+
            "na ziemi�, a wok� panuje mrok. Wra�enie nienaturalno�ci "+
            "pot�guj� ciemne sylwetki drzew. Ziemia pokryta mchem, "+
            "przetykanym gdzieniegdzie fio�kami i dzwonkami le�nymi "+
            "ca�kowicie t�umi kroki, a jedyne odg�osy dostaj�ce si� tu z "+
            "g�rnych pi�ter lasu s� przyt�umione i odleg�e jakby "+
            "pochodzi�y z innego �wiata.";
        }
        else //jesien i zima gdy nie ma sniegu
        {
            str+="Dooko�a rozci�ga si� pas buk�w. Ich wysokie, g�adkie "+
            "pnie strzelaj� w g�r�, aby na ko�cu przej�� w g�ste korony. "+
            "Tworz� jakby �wi�tyni� z kolumnami i pi�knym, pod�wietlonym "+
            "przez zielone refleksy sklepieniem. �wiat�o nie dostaje si� "+
            "na ziemi�, a wok� panuje mrok. Wra�enie nienaturalno�ci "+
            "pot�guj� ciemne sylwetki drzew. Ziemia pokryta wyschni�tym "+
            "mchem, zas�anym r�nokolorowymi, opad�ymi li��mi, ca�kowicie "+
            "t�umi kroki, a jedyne odg�osy dostaj�ce si� tu z g�rnych "+
            "pi�ter lasu s� przyt�umione i odleg�e jakby pochodzi�y z "+
            "innego �wiata.";
        }
    }
    else //Noc
    {
        if(CZY_JEST_SNIEG(this_object()))
        {
            str+="Dooko�a rozci�ga si� pas buk�w. Ich wysokie, g�adkie "+
            "pnie strzelaj� w g�r�, aby na ko�cu przej�� w g�ste korony "+
            "ca�kowicie pozbawione li�ci. Tworz� jakby �wi�tyni� z "+
            "kolumnami i pi�knym sklepieniem utworzonym z ga��zi zas�anymi "+
            "�niegiem. �wiat�o ksi�yca zalewa ziemi� sinym blaskiem "+
            "odbijaj�cym si� od bia�ego �niegu. Wra�enie nienaturalno�ci "+
            "pot�guj� ciemne sylwetki drzew. Ziemia pokryta dywanem z "+
            "puchu ca�kowicie t�umi kroki, a jedyne odg�osy dostaj�ce si� "+
            "tu z g�rnych pi�ter lasu s� przyt�umione i odleg�e jakby "+
            "pochodzi�y z innego �wiata.";
        }
        else if(pora_roku() == MT_WIOSNA)
        {
            str+="Dooko�a rozci�ga si� pas buk�w. Ich wysokie, g�adkie pnie "+
            "strzelaj� w g�r�, aby na ko�cu przej�� w g�ste korony. Tworz� "+
            "jakby �wi�tyni� z kolumnami i pi�knym, pod�wietlonym przez "+
            "srebrzyste refleksy sklepieniem. �wiat�o ksi�yca nie dostaje "+
            "si� na ziemi�, a wok� panuje mrok. Wra�enie nienaturalno�ci "+
            "pot�guj� ciemne sylwetki drzew. Ziemia pokryta mchem, "+
            "przetykanym gdzieniegdzie fio�kami i zawilcami ca�kowicie "+
            "t�umi kroki, a jedyne odg�osy dostaj�ce si� tu z g�rnych "+
            "pi�ter lasu s� przyt�umione i odleg�e jakby pochodzi�y z "+
            "innego �wiata.";
        }
        else if(pora_roku() == MT_LATO)
        {
            str+="Dooko�a rozci�ga si� pas buk�w. Ich wysokie, g�adkie pnie "+
            "strzelaj� w g�r�, aby na ko�cu przej�� w g�ste korony. Tworz� "+
            "jakby �wi�tyni� z kolumnami i pi�knym, pod�wietlonym przez "+
            "srebrzyste refleksy sklepieniem. �wiat�o ksi�yca nie dostaje "+
            "si� na ziemi�, a wok� panuje mrok. Wra�enie nienaturalno�ci "+
            "pot�guj� ciemne sylwetki drzew. Ziemia pokryta mchem, "+
            "przetykanym gdzieniegdzie fio�kami i dzwonkami le�nymi "+
            "ca�kowicie t�umi kroki, a jedyne odg�osy dostaj�ce si� tu z "+
            "g�rnych pi�ter lasu s� przyt�umione i odleg�e jakby "+
            "pochodzi�y z innego �wiata.";
        }
        else //jesien i zima gdy nie ma sniegu
        {
            str+="Dooko�a rozci�ga si� pas buk�w. Ich wysokie, g�adkie "+
            "pnie strzelaj� w g�r�, aby na ko�cu przej�� w g�ste korony. "+
            "Tworz� jakby �wi�tyni� z kolumnami i pi�knym, pod�wietlonym "+
            "przez srebrzyste refleksy sklepieniem. �wiat�o ksi�yca nie "+
            "dostaje si� na ziemi�, a wok� panuje mrok. Wra�enie "+
            "nienaturalno�ci pot�guj� ciemne sylwetki drzew. Ziemia pokryta "+
            "wyschni�tym mchem, zas�anym r�nokolorowymi, opad�ymi li��mi, "+
            "ca�kowicie t�umi kroki, a jedyne odg�osy dostaj�ce si� tu z "+
            "g�rnych pi�ter lasu s� przyt�umione i odleg�e jakby "+
            "pochodzi�y z innego �wiata.";
        }
    }
    str += "\n";
    return str;
}
