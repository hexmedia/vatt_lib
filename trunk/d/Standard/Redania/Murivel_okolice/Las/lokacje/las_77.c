/* Las komanda
   Autor: Avard 
   Opis : Edrain */

#include <stdproperties.h>
#include <macros.h>
#include <mudtime.h>
#include "dir.h"
inherit LAS_ELFOW_MROCZNY;

string dlugi_opis();

string opis_kwiatow();
string opis_leszczyny();

int wez(string str);

void
create_las()
{
    set_short("Skraj puszczy");
    set_long(&dlugi_opis());
    add_exit(LAS_ELFOW_LOKACJE+ "las_66.c","w");
    add_exit(LAS_ELFOW_LOKACJE+ "las_68.c","e");
    
    add_item(({"kwiat","kwiaty"}), &opis_kwiatow());
    add_item(({"leszczyn�","krzak","krzew","krzak leszczyny",
        "krzew leszczyny"}), &opis_leszczyny());
}

string dlugi_opis()
{
    string str = "";
    
    if(jest_dzien())
    {
        if(CZY_JEST_SNIEG(this_object()))
        {
            str += "Promienie s�o�ca docieraj�ce w stron� ziemi, odbijaj� "+
            "si� od srebrzystych pni drzew zalewaj�c okolic� ostrym "+
            "blaskiem. W miejscu tym smuk�e buki mieszaj� si� z kr�pymi "+
            "sylwetkami krzew�w le�nych, pot�nymi d�bami i wysokimi "+
            "wi�zami. Pod�o�e pokryte jest zimny puchem �niegu, odbijaj�cym "+
            "ostre �wiat�o mro�nego dnia zimy.";
        }
        else
        {
            str += "Promienie s�o�ca docieraj�ce w stron� ziemi, odbijaj� "+
            "si� od srebrzystych pni drzew zalewaj�c okolic� ciep�ym "+
            "blaskiem. W miejscu tym smuk�e buki mieszaj� si� z kr�pymi "+
            "sylwetkami krzew�w le�nych, pot�nymi d�bami i wysokimi "+
            "wi�zami. Pod�o�e usiane r�nokolorowym kwieciem cieszy oczy "+
            "swoj� �wie�o�ci�,a odurzaj�cy zapach lasu powoduje zawroty "+
            "g�owy. Bzyczenie owad�w zapylaj�cych kwiaty przeplata si� z "+
            "weso�ym �piewem ptak�w. I tylko mroczny las majacz�cy w "+
            "oddali napawa l�kiem.";        
        }
    }
    else
    {
        if(CZY_JEST_SNIEG(this_object()))
        {
            str += "Blask ksi�yca docieraj�cego w stron� ziemi, odbija si� "+
            "od srebrzystych pni drzew zalewaj�c okolic� delikatnym, sino "+
            "fioletowym blaskiem. W miejscu tym smuk�e buki mieszaj� si� z "+
            "kr�pymi sylwetkami krzew�w le�nych, pot�nymi d�bami i "+
            "wysokimi wi�zami. Pod�o�e pokryte jest zimny puchem �niegu.";
        }
        else
        {
            str += "Blask ksi�yca docieraj�cego w stron� ziemi, odbija si� "+
            "od srebrzystych pni drzew zalewaj�c okolic� delikatnym, sino "+
            "fioletowym �wiat�em. W miejscu tym smuk�e buki mieszaj� si� z "+
            "kr�pymi sylwetkami krzew�w le�nych, pot�nymi d�bami i "+
            "wysokimi wi�zami. Pod�o�e usiane r�nokolorowym kwieciem cieszy "+
            "oczy swoj� �wie�o�ci�,a odurzaj�cy zapach lasu powoduje zawroty "+
            "g�owy. Bzyczenie owad�w zapylaj�cych kwiaty przeplata si� z "+
            "weso�ym �piewem ptak�w. I tylko mroczny las majacz�cy w oddali "+
            "napawa l�kiem.";        
        }
    }
    str += "\n";
    return str;
}

string opis_kwiatow()
{
    string str;
    if (pora_roku() == MT_JESIEN)
    {
    str += "Bujne, zielono- czerwone li^scie tworz^a co^s na "+
    "kszta^lt sp^ojnej kuli, na kt^orej wierzchu znajduj^a "+
    "si^e liczne pozosta^lo^sci po niegdy^s pi^eknych "+
    "kwiatach.\n"; 
    return str;
    }
    else if(pora_roku() == MT_ZIMA)
    {
    return 0;
    }
    else
    {
    str += "Bujne, w^sciekle zielone li^scie tworz^a co^s na "+
    "kszta^lt sp^ojnej kuli, na kt^orej wierzchu znajduj^a "+
    "si^e liczne kwiaty o mlecznej barwie. Ka^zdy kwiat "+
    "ma potr^ojn^a warstw^e p^latk^ow, co czyni je "+
    "okazalszymi.\n"; 
    return str;
    }
}

string
opis_leszczyny()
{
    if(CZY_JEST_SNIEG(this_object))
    {
        str = "est to niewysoka forma drzewiasta krzewu o ?redniej wielko?ci, "+
        "kt�rego pie� w barwie szarobr?zowej do?� g�sto przetykany jest "+
        "bia�awymi przetchlinkami, kt�re s? jedynym elementem naruszaj?cym "+
        "niesamowit? g�adko?� kory. Obecnie na krzewie dostrzegasz tylko "+
        "pojedyncze, obumar�e li?cie, kt�re nie spad�y na jesieni, a teraz "+
        "trzymaj? si� tylko przez dzia�anie mrozu.\n";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str = "Jest to niewysoka forma drzewiasta krzewu o ?redniej "+
        "wielko?ci, kt�rego pie� w barwie szarobr?zowej do?� g�sto "+
        "przetykany jest bia�awymi przetchlinkami, kt�re s? jedynym "+
        "elementem naruszaj?cym niesamowit? g�adko?� kory. Krzew ten bogaty "+
        "jest w do?� du�e, okr?g�awe li?cie, u nasady sercowate, kr�tko "+
        "zaostrzone, o barwie soczystej zieleni, aktualnie g�sto przetykany "+
        "br?zowo ��tymi kotami, kt�re zwisaj? z ga��zi wystawione na "+
        "dzia�anie wiatru rozsiewaj?cego py�ek po okolicy.\n";
    }
    else if(pora_roku() == MT_LATO)
    {
        str = "Jest to niewysoka forma drzewiasta krzewu o ?redniej "+
        "wielko?ci, kt�rego pie� w barwie szarobr?zowej do?� g�sto "+
        "przetykany jest bia�awymi przetchlinkami, kt�re s? jedynym "+
        "elementem naruszaj?cym niesamowit? g�adko?� kory. Krzew ten bogaty "+
        "jest w do?� du�e, okr?g�awe li?cie, u nasady sercowate, kr�tko "+
        "zaostrzone, o barwie soczystej zieleni.\n";
    }
    else
    {  
        str = "Jest to niewysoka forma drzewiasta krzewu o ?redniej "+
        "wielko?ci, kt�rego pie� w barwie szarobr?zowej do?� g�sto "+
        "przetykany jest bia�awymi przetchlinkami, kt�re s? jedynym "+
        "elementem naruszaj?cym niesamowit? g�adko?� kory. Krzew ten bogaty "+
        "jest w do?� du�e, okr?g�awe li?cie, u nasady sercowate, kr�tko "+
        "zaostrzone, o barwie ��tozielonej. Mi�dzy nimi dostrzec mo�na "+
        "pojedyncze orzeszki.\n";
    }
    return str;
}

int wez(string str)
{
    if(query_verb() ~= "we�")
        notify_fail("We� co?\n");
    if(query_verb() ~= "zbieraj")
        notify_fail("Zbieraj co?\n");
    if(query_verb() ~= "zbierz")
        notify_fail("Zbierz co?\n");
        
    if (!str) return 0;
    if (!strlen(str)) return 0;
    
    if(pora_roku() != MT_JESIEN)
        return 0;
    
    if(query_verb() ~= "we�")
    {
        if(!parse_command(str,environment(TP),"'orzech' / 'orzechy'"))
            return 0;
    }
    else 
    {
        if(!parse_command(str,environment(TP),"'orzechy'"))
            return 0;
    }
    write("Zaczynasz zbiera� orzechy.\n");
    saybb(QCIMIE(TP,PL_MIA)+" zaczyna zbiera� orzechy.\n");
    clone_object(LAS_ELFOW_PRZEDMIOTY+"paraliz_orzechy_leszczyny.c")->move(TP);
    return 1;
}