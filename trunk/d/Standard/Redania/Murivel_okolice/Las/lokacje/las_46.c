#include <stdproperties.h>
#include <mudtime.h>
#include "dir.h"
inherit LAS_ELFOW_GESTY;

string dlugi_opis();

void
create_las()
{
    set_short("W^sr^od drzew g^esto zaro^sni^etego lasu");
    set_long(&dlugi_opis());
    add_exit(LAS_ELFOW_LOKACJE+ "las_24.c","sw");
    add_exit(LAS_ELFOW_LOKACJE+ "las_26.c","se");
    add_exit(LAS_ELFOW_LOKACJE+ "las_47.c","e");
    
    if(!CZY_JEST_SNIEG(this_object()))
    {
        add_item("mech","Po�acie soczysto zielonych �ody�ek wznosz? si� "+
        "nieznacznie nad ziemi�. Mi�dzy drobnymi listkami gromadz? "+
        "du�e ilo?ci wody, przez co ca�o?� jest wyj?tkowo wilgotna. "+
        "Ziemia zas�ana tymi ro?linkami wygl?da jakby by�a pokryta "+
        "naturalnym dywanem.\n");
    }
}
string dlugi_opis()
{
    string str;
    if(jest_dzien() == 1) // DZIEN
    {
    if(CZY_JEST_SNIEG(this_object()))
    {
        str="Dooko^la ci^agn^a si^e wi^azy, d^eby, gdzieniegdzie dostrzegasz "+
        "tak^ze graby i sosny, tworz^ace nieprzejednany g^aszcz p^lawi^acy si"+
        "^e w promieniach s^lo^nca. Cis, ja^lowiec czy leszczyna dodatkowo po"+
        "t^eguj^a uczucie odci^ecia od reszty ^swiata, a poprzewracane pnie, "+
        "runo g^esto usiane ga^l^eziami i pn^acy si^e wsz^edzie bluszcz dodat"+
        "kowo utwierdzaj^a w tym przekonaniu. Pod stopami rozpo^sciera si^e b"+
        "ielutki puch ^sniegu, kt^ory wyciszaj^a kroki pozwalaj^ac zadowala^c"+
        " uszy jedynie d^xwi^ekami lasu.";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str="Dooko^la ci^agn^a si^e wi^azy i d^eby, gdzieniegdzie dostrzec mo"+
        "^zna tak^ze graby i sosny, tworz^ace niemal nieprzebyty g^aszcz, p^l"+
        "awi^acy si^e w promieniach s^lo^nca. Cis, ja^lowiec czy leszczyna do"+
        "datkowo pot^eguj^a uczucie odci^ecia od reszty ^swiata, a poprzewrac"+
        "ane pnie, runo le^sne g^esto usiane ga^l^eziami i pn^acy si^e wsz^ed"+
        "zie bluszcz utwierdzaj^a jedynie w tym przekonaniu. Pod stopami dost"+
        "rzegasz mech, zawilce, wrzosy i bor^owki, kt^ore wyciszaj^a kroki, p"+
        "ozwalaj^ac zadowala^c uszy jedynie d^xwi^ekami lasu.";
    }
    else if(pora_roku() == MT_LATO)
    {
        str="Dooko^la ci^agn^a si^e wi^azy i d^eby, gdzieniegdzie dostrzec mo"+
        "^zna tak^ze graby i sosny, tworz^ace niemal nieprzebyty g^aszcz, p^l"+
        "awi^acy si^e w promieniach s^lo^nca. Cis, ja^lowiec czy leszczyna do"+
        "datkowo pot^eguj^a uczucie odci^ecia od reszty ^swiata, a poprzewrac"+
        "ane pnie, runo le^sne g^esto usiane ga^l^eziami i pn^acy si^e wsz^ed"+
        "zie bluszcz utwierdzaj^a jedynie w tym przekonaniu. Pod stopami dost"+
        "rzegasz mech, zawilce, wrzosy i bor^owki, kt^ore wyciszaj^a kroki, p"+
        "ozwalaj^ac zadowala^c uszy jedynie d^xwi^ekami lasu.";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str="Dooko^la ci^agn^a si^e wi^azy i d^eby, gdzieniegdzie dostrzegasz"+
        " tak^ze graby i sosny, tworz^ace nieprzejednany kolorowy g^aszcz p^l"+
        "awi^acy si^e w promieniach s^lo^nca. Cis, ja^lowiec czy leszczyna do"+
        "datkowo pot^eguj^a uczucie odci^ecia od reszty ^swiata, a poprzewrac"+
        "ane pnie, runo g^esto usiane ga^l^eziami i pn^ace si^e wsz^edzie res"+
        "ztki niegdy^s zielonego, a teraz czerwonego bluszczu dodatkowo utwie"+
        "rdzaj^a w tym przekonaniu. Pod stopami dostrzegasz mech, kwitn^ace l"+
        "ila i fioletowo wrzosy oraz granatowe bor^owki, kt^ore wyciszaj^a kr"+
        "oki pozwalaj^ac zadowala^c uszy jedynie d^xwi^ekami lasu.";
    }
    }
    if(jest_dzien() == 0) // NOC
    {
    if(CZY_JEST_SNIEG(this_object()))
    {
        str="Dooko^la ci^agn^a si^e wi^azy i d^eby, gdzieniegdzie dostrzegasz"+
        " tak^ze graby i sosny, tworz^ac nieprzejednany g^aszcz, kt^ory spraw"+
        "ia mroczne wra^zenie o^swietlany jedynie blaskiem ksi^e^zyca. Cis, j"+
        "a^lowiec czy leszczyna dodatkowo pot^eguj^a uczucie odci^ecia od res"+
        "zty ^swiata,  poprzewracane pnie, runo g^esto usiane ga^l^eziami i p"+
        "n^acy si^e wsz^edzie bluszcz dodatkowo utwierdzaj^a w tym przekonani"+
        "u. Pod stopami rozpo^sciera si^e bielutki puch ^sniegu, kt^ory wycis"+
        "za kroki pozwalaj^ac zadowala^c uszy jedynie d^xwi^ekami lasu.";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str="Dooko^la ci^agn^a si^e wi^azy i d^eby, gdzieniegdzie dostrzegasz"+
        " tak^ze graby i sosny, tworz^ace nieprzejednany g^aszcz, kt^ory spra"+
        "wia mroczne wra^zenie, o^swietlany jedynie blaskiem ksi^e^zyca. Cis,"+
        " ja^lowiec czy leszczyna dodatkowo pot^eguj^a uczucie odci^ecia od r"+
        "eszty ^swiata, a poprzewracane pnie, runo g^esto usiane ga^l^eziami "+
        "i pn^acy si^e wsz^edzie bluszcz dodatkowo utwierdzaj^a w tym przekon"+
        "aniu. Pod stopami wyczuwasz k^l^ebi^ace si^e ro^sliny, kt^ore wycisz"+
        "aj^a kroki pozwalaj^ac zadowala^c uszy jedynie d^xwi^ekami lasu.";
    }
    else if(pora_roku() == MT_LATO)
    {
        str="Dooko^la ci^agn^a si^e wi^azy i d^eby, gdzieniegdzie dostrzegasz"+
        " tak^ze graby i sosny, tworz^ace nieprzejednany g^aszcz, kt^ory spra"+
        "wia mroczne wra^zenie, o^swietlany jedynie blaskiem ksi^e^zyca. Cis,"+
        " ja^lowiec czy leszczyna dodatkowo pot^eguj^a uczucie odci^ecia od r"+
        "eszty ^swiata, a poprzewracane pnie, runo g^esto usiane ga^l^eziami "+
        "i pn^acy si^e wsz^edzie bluszcz dodatkowo utwierdzaj^a w tym przekon"+
        "aniu. Pod stopami wyczuwasz k^l^ebi^ace si^e ro^sliny, kt^ore wycisz"+
        "aj^a kroki pozwalaj^ac zadowala^c uszy jedynie d^xwi^ekami lasu.";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str="Dooko^la ci^agn^a si^e wi^azy i d^eby, gdzieniegdzie dostrzegasz"+
        " tak^ze graby i sosny, tworz^ac nieprzejednany kolorowy g^aszcz, kt^"+
        "ory sprawia mroczne wra^zenie o^swietlany jedynie blaskiem ksi^e^zyc"+
        "a. Cis, ja^lowiec czy leszczyna dodatkowo pot^eguj^a uczucie odci^ec"+
        "ia od reszty ^swiata,  poprzewracane pnie, runo g^esto usiane ga^l^e"+
        "ziami i pn^ace si^e wsz^edzie resztki niegdy^s zielonego, a teraz cz"+
        "erwonego bluszczu dodatkowo utwierdzaj^a w tym przekonaniu. Pod stop"+
        "ami wyczuwasz k^l^ebi^ace si^e ro^sliny, kt^ore wyciszaj^a kroki poz"+
        "walaj^ac zadowala^c uszy jedynie d^xwi^ekami lasu.";
    }
    }
    str += "\n";
    return str;
}
