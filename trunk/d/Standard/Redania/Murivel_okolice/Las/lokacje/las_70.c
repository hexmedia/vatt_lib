/* Las komanda
   Autor: Avard 
   Opis : Edrain */

#include <stdproperties.h>
#include <macros.h>
#include <mudtime.h>
#include "dir.h"
inherit LAS_ELFOW_MROCZNY;

string dlugi_opis();

void
create_las()
{
    set_short("Serce puszczy");
    set_long(&dlugi_opis());
    add_exit(LAS_ELFOW_LOKACJE+ "las_69.c","w");
    add_exit(LAS_ELFOW_LOKACJE+ "las_66.c","se");
}

string dlugi_opis()
{
    string str = "";
    
    if(jest_dzien())
    {
        str += "Buki porastaj�ce to miejsce tworz� szpaler sk�adaj�cy si� z "+
        "samych pni. Strzeliste drzewa o wysoko osadzonych koronach i "+
        "prostych pniach uniemo�liwiaj� dotarcie w ni�sze rejony lasu. "+
        "Po�yskuj�ca, srebrzystoszara kora pokrywaj�ca pnie nadaje "+
        "krajobrazowi tajemniczego wymiaru. W koronach s�ycha� �piew ptak�w, "+
        "jednak do ziemi dochodz� tylko przyt�umione d�wi�ki. S�o�ce czasem "+
        "wygrywa walk� z nieprzejednanymi koronami buk�w i �wiat�o opada "+
        "kaskadami do ziemi nadaj�c uroku temu miejscu.\n";
    }
    else
    {
        str += "Buki porastaj�ce to miejsce tworz� szpaler sk�adaj�cy si� z "+
        "samych pni. Strzeliste drzewa o wysoko osadzonych koronach i "+
        "prostych pniach uniemo�liwiaj� dotarcie w ni�sze rejony lasu. "+
        "Po�yskuj�ca, srebrzystoszara kora pokrywaj�ca pnie nadaje "+
        "krajobrazowi tajemniczego wymiaru. W koronach s�ycha� �piew "+
        "ptak�w, jednak do ziemi dochodz� tylko przyt�umione d�wi�ki. Blask "+
        "ksi�yca czasem wygrywa walk� z nieprzejednanymi koronami buk�w i "+
        "�wiat�o opada fioletowo szarymi kaskadami do ziemi nadaj�c uroku "+
        "temu miejscu.\n";
    }
    str += "\n";
    return str;
}
