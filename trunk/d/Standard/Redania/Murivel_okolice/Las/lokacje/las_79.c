/**
 * Las Komanda
 *
 * autor Cairell
 * opisy Edrain & Aladriel
 * data  styczen 2009
 */
 
#include <stdproperties.h>
#include <mudtime.h>
#include "dir.h"
#include "pogoda.h"
#include <macros.h>

inherit LAS_ELFOW_RZEKA;

string dlugi_opis();

string opis_roz();
string opis_rzezbien();
string opis_plyt();
string opis_blokow();
string opis_murow();


void create_las()
{
    set_short("W ^srodku ruin.");
    set_long(&dlugi_opis());

    add_exit(LAS_ELFOW_LOKACJE+ "trakt.c","n");//FIXME
    add_exit(LAS_ELFOW_LOKACJE+ "las_86.c","ne");
    add_exit(LAS_ELFOW_LOKACJE+ "las_75.c","se");
    add_exit(LAS_ELFOW_LOKACJE+ "las_74.c","s");
    add_exit(LAS_ELFOW_LOKACJE+ "las_73.c","sw");
    add_exit(LAS_ELFOW_LOKACJE+ "las_85.c","nw");

    add_item(({"r^o^z^e","r^o^ze"}), &opis_roz());
    add_item(({"rze^xbienie","rze^xbienia"}), &opis_rzezbien());
    add_item(({"p^lyt","p^lyty"}), &opis_plyt());
    add_item(({"blok","bloki"}), &opis_blokow());
    add_item(({"mur","mury"}), &opis_murow());

}

string dlugi_opis()
{
    string str = "";
    if(pora_roku() == MT_ZIMA)
    {
        if (jest_dzien() == 1)
        {
            if (ZACHMURZENIE(TO) <= 2)
            {
            str += "Wst^egi jasnych promieni s^lo^nca opadaj^a do ziemi "+
            "^sciel^ac si^e po bia^lych, marmurowych blokach i "+
            "ostro skrz^ac si^e na czystym ^sniegu. "; 
            }
            else
            {
            str += "";
            } 
            str += "Kamienie wygl^adaj^a jakby by^ly niedbale "+
            "rozrzucone, zniszczone czasem i zapomnieniem. Po "+
            "^snie^znobia^lych murach pn^a si^e usch^le ga^l^azki "+
            "r^o^zanych krzew^ow, kontrastuj^ac z jasnymi p^lytami. "+
            "Oplataj^a rze^xbienia w ^scianach i przechodz^a na "+
            "drug^a stron^e przez strzeliste okno ruin pa^lacu."; 
        }
        else //noc
        {
            if (ZACHMURZENIE(TO) <= 2)
            {
            str += "Wst^egi sinych smug ksi^e^zycowego ^swiat^la opadaj^a "+
            "do ziemi ^sciel^ac si^e po bia^lych, marmurowych "+
            "blokach i ostro skrz^ac si^e na czystym ^sniegu. "; 
            }
            else
            {
            str += "";
            } 
            str += "Kamienie wygl^adaj^a jakby by^ly niedbale "+
            "rozrzucone, zniszczone czasem i zapomnieniem. Po "+
            "^snie^znobia^lych murach pn^a si^e usch^le ga^l^azki "+
            "r^o^zanych krzew^ow, kontrastuj^ac z jasnymi p^lytami. "+
            "Oplataj^a rze^xbienia w ^scianach i przechodz^a na "+
            "drug^a stron^e przez strzeliste okno ruin pa^lacu."; 
        }
    }
    else //Lato_wiosna_jesien
    {
        if (jest_dzien() == 1)
        {
            if (ZACHMURZENIE(TO) <= 2)
            {
            str += "Wst^egi jasnych promieni s^lo^nca opadaj^a do ziemi "+
            "^sciel^ac si^e po bia^lych, marmurowych blokach. "; 
            }
            else
            {
            str += "";
            } 
        str += "Kamienie wygl^adaj^a jakby by^ly niedbale "+
        "rozrzucone, zniszczone czasem i zapomnieniem. Po "+
        "^snie^znobia^lych murach pn^a si^e czerwone r^o^ze, "+
        "kontrastuj^ac z jasnymi p^lytami. Oplataj^a "+
        "rze^xbienia w ^scianach i przechodz^a na drug^a "+
        "stron^e przez strzeliste okno ruin pa^lacu."; 
        }
        else //noc
        {
            if (ZACHMURZENIE(TO) <= 2)
            {
            str += "Wst^egi sinych smug ksi^e^zycowego ^swiat^la opadaj^a "+
            "do ziemi ^sciel^ac si^e po bia^lych, marmurowych. "+
            "blokach."; 
            }
            else
            {
            str += "";
            } 
        str += "Kamienie wygl^adaj^a jakby by^ly niedbale "+
        "rozrzucone, zniszczone czasem i zapomnieniem. Po "+
        "^snie^znobia^lych murach pn^a si^e czerwone r^o^ze, "+
        "kontrastuj^ac z jasnymi p^lytami. Oplataj^a "+
        "rze^xbienia w ^scianach i przechodz^a na drug^a "+
        "stron^e przez strzeliste okno ruin pa^lacu."; 
        }
    }

str+="\n";
return str;
}

string opis_roz()
{
    string str;
    if (pora_roku() == MT_ZIMA)
    {
    str += "Krzewy pn^acych r^o^z porastaj^a ca^l^a okolic^e. Mocne "+
    "^lody^zki oplataj^a mury zaczepiaj^ac si^e kolcami w "+
    "nielicznych zag^l^ebieniach. Pn^acza pozbawione "+
    "kwiat^ow wprowadzaj^a w to miejsce nieco smutku i "+
    "melancholii.\n"; 
    return str;
    }
    else
    {
    str += "Krzewy pn^acych r^o^z porastaj^a ca^l^a okolic^e. Mocne "+
    "^lody^zki oplataj^a mury zaczepiaj^ac si^e kolcami w "+
    "nielicznych zag^l^ebieniach. Ciemnoczerwone p^latki "+
    "kwiat^ow odcinaj^a si^e od ^snie^znych p^lyt, "+
    "wygl^adaj^ac jak krew plami^aca nieskalan^a czysto^s^c "+
    "marmuru.\n";
    return str;
    }
}

string opis_rzezbien()
{
    string str;
    str += "Ro^slinne ornamenty ci^agn^a si^e wydr^a^zone w bia^lym "+
    "marmurze. W niekt^orych miejscach s^a zatarte przez "+
    "czas. Urywaj^ac si^e ca^lkiem nagle by po chwili "+
    "ponownie si^e pojawi^c zdobi^ac p^lyty.\n"; 
    return str;
}




string opis_plyt()
{
    string str;
    if (jest_dzien() == 1)
    {
        if (ZACHMURZENIE(TO) <= 2)
        {
        str =  "Bia^le, marmurowe p^lyty odbijaj^a promienei "+
        "sloneczne, kt^ore zalewa ca^l^a okolic^e. ";
        }
        else
        {
            str = "";
        }
    str += "Roztrzaskany kamie^n w wielu miejscach jest "+
    "porysowany i wy^z^lobiony przez czas i natur^e. "+
    "W niekt^orych miejscach p^lyty zachodz^a "+
    "na siebie tworz^ac wiele miejsc o kt^ore "+
    "mo^zna si^e potkn^a^c.\n";
    return str;
    }
    else
    {
        if (ZACHMURZENIE(TO) <= 2)
        {
        str = "Bia^le, marmurowe p^lyty odbijaj^a smugi blasku "+
        "ksiezyca, kt^ore zalewaja ca^l^a okolic^e.";
        }
        else
        {
        str = "";
        }
    str += "Roztrzaskany kamie^n w wielu miejscach "+
    "wy^z^lobiony przez czas i natur^e. W niekt^orych "+
    "miejscach p^lyty zachodz^a na siebie tworz^ac "+
    "wiele miejsc o kt^ore mo^zna si^e potkn^a^c.\n";
    return str;
    }
}

string opis_blokow()
{
    string str;
    str = "P^ekni^ete w wielu miejscach bloki marmuru musz^a "+
    "tu le^ze^c od bardzo dawna. Niegdysiejsze "+
    "cz^e^sci okaza^lych mur^ow le^z^a teraz bez^ladnie "+
    "porozrzucane po okolicy.\n";
    return str;
}

string opis_murow()
{
    string str;
    str = "Bia^le, pot^e^zne bloki marmuru ukszta^ltowane "+
    "niegdy^s w okaza^l^a budowl^e obecnie s^a tylko "+
    "sm^etnymi resztkami jej dawnej ^swietno^sci. "+
    "W wielu miejscach pop^ekane i wyszczerbione "+
    "s^a ju^z jedynie zapomnianymi ruinami.\n";
    return str;
}
