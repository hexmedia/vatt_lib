/*Las Komanda
  opisy Eskela
  kod Cairell*/

#include <stdproperties.h>
#include <mudtime.h>
#include <macros.h>
#include "dir.h"
inherit LAS_ELFOW_BAGIENKO;

string dlugi_opis();
void wyjscie();
void bujam_sie();

void
create_bagienko()
{
    set_short("W malutkiej kotlince.");
    set_long(&dlugi_opis());
}

init()
{
   ::init();
   add_action(wyjscie, "wyj^scie");
   add_action(wyjscie, "bagno");
}

void wyjscie()
{
    write("Odchylaj^ac ga^l^a^x wy^lazisz z nory.\n");
    saybb(QCIMIE(TP, PL_MIA)+" odchylaj^ac ga^l^a^x wy^lazi z nory.\n");
    TP->move_living("M", LAS_ELFOW_LOKACJE+ "bagienko_ne.c");
    set_alarm(2.0,0.0,"bujam_sie");

}

void bujam_sie()
{
    tell_roombb("Puszczona ga^l^a^z ko^lysze si^e chwil^e, by po chwili "+
        "zamrze� w bezruchu.\n");
}

string dlugi_opis()
{
    string str;

    str = "Ma^l^a, stosunkowo such^a nisz^e z jednej strony "+
        "ogranicza powalony konar, a z pozosta^lych dw^och zbocze kotliny. "+
        "Stworzona przez Natur^e kryj^owka jest stosunkowo sucha i zapewnia "+
        "ochron^e przed wiatrem. Nie zauwa^zasz ^slad�w jakich^s zwierz^at, "+
        "ale niczego nie mo^zna by� pewnym...\n Wyj^scie z niszy prowadzi "+
        "na bagno.\n";
    
    str += "\n";
    return str;
}
