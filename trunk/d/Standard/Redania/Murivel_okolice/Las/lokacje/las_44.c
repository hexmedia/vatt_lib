#include <stdproperties.h>
#include <mudtime.h>
#include "dir.h"
inherit LAS_ELFOW_GESTY;

string dlugi_opis();

void
create_las()
{
    set_short("Gdzie^s pomi^edzy drzewami g^estego lasu");
    set_long(&dlugi_opis());
    add_exit(LAS_ELFOW_LOKACJE+ "las_45.c","n");
    add_exit(LAS_ELFOW_LOKACJE+ "las_22.c","sw");
    
    if(!CZY_JEST_SNIEG(this_object()))
    {
        add_item("mech","Po�acie soczysto zielonych �ody�ek wznosz? si� "+
        "nieznacznie nad ziemi�. Mi�dzy drobnymi listkami gromadz? "+
        "du�e ilo?ci wody, przez co ca�o?� jest wyj?tkowo wilgotna. "+
        "Ziemia zas�ana tymi ro?linkami wygl?da jakby by�a pokryta "+
        "naturalnym dywanem.\n");
    }
}
string dlugi_opis()
{
    string str;
    if(jest_dzien() == 1)
    {
    if(CZY_JEST_SNIEG(this_object()))
    {
        str="Gdzie nie spojrzysz - rozci^aga si^e przed tob^a pl^atanina ga^l"+
        "^ezi, g^estwina lasu zamieszkana w czasie lata przez wiele ciekawych"+
        " zwierz^at aktualnie wydaje si^e ca^lkowicie u^spiona. D^eby przepla"+
        "taj^ace si^e z grabami, wi^azami, sosnami, czy jod^lami stanowi^a na"+
        "jliczniejsz^a grup^e gatunkow^a w tym miejscu, a  drzewa ca^lkowicie"+
        " sk^apane w jasno^sci pot^egowanej odblaskiem ^swiat^la od bielutkie"+
        "go ^sniegu nadaj^a temu miejscu wewn^etrznej ^swietlisto^sci. Leszcz"+
        "yna czy ja^lowiec to g^l^owne elementy podszytu le^snego, natomiast "+
        "pod nogami rozpo^sciera si^e bia^ly dywan, kt^ory t^lumi odg^losy kr"+
        "ok^ow nie zak^l^ocaj^ac ciszy tego miejsca.";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str="Gdzie nie spojrzysz - rozci^agaj^a si^e przed tob^a spl^atane ga"+
        "^l^ezie, trawy i li^scie, a g^estwina zamieszkana przez wiele ciekaw"+
        "ych zwierz^at t^etni w^lasnym ^zyciem. D^eby przeplataj^ace si^e tu "+
        "z grabami, wi^azami, sosnami, czy jod^lami stanowi^a najliczniejsz^a"+
        " grup^e gatunkow^a w tym miejscu, a wszystkie drzewa razem, ca^lkowi"+
        "cie sk^apane w jasno^sci nadaj^a temu miejscu wewn^etrznej ^swietlis"+
        "to^sci. Leszczyna czy ja^lowiec to g^l^owne elementy podszytu le^sne"+
        "go, natomiast na samej ziemi dostrzec mo^zna mech, paprocie i le^sne"+
        " kwiaty, kt^ore t^lumi^a odg^losy krok^ow, nie zak^l^ocaj^ac koncert"+
        "u jaki daj^a ptaki zamieszkuj^ace korony drzew.";
    }
    else if(pora_roku() == MT_LATO)
    {
        str="Gdzie nie spojrzysz - rozci^agaj^a si^e przed tob^a spl^at"+
             "ane ga^l^ezie, trawy i li^scie, a g^estwina zamieszkana pr"+
             "zez wiele ciekawych zwierz^at t^etni w^lasnym ^zyciem. D^e"+
             "by przeplataj^ace si^e tu z grabami, wi^azami, sosnami, cz"+
             "y jod^lami stanowi^a najliczniejsz^a grup^e gatunkow^a w tym"+
             " miejscu, a wszystkie drzewa razem, ca^lkowicie sk^apane w"+
             " jasno^sci nadaj^a temu miejscu wewn^etrznej ^swietlisto"+
             "^sci. Leszczyna czy ja^lowiec to g^l^owne elementy podszyt"+
             "u le^snego, natomiast na samej ziemi dostrzec mo^zna mech, p"+
             "aprocie i le^sne kwiaty, kt^ore t^lumi^a odg^losy krok^ow,"+
             " nie zak^l^ocaj^ac koncertu jaki daj^a ptaki zamieszkuj^ac"+
             "e korony drzew.";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str="Gdzie nie spojrzysz - rozci^aga si^e przed tob^a pl^atanina "+
             "ga^l^ezi, traw i li^sci, g^estwina lasu zamieszkana przez "+
             "wiele ciekawych zwierz^at t^etni w^lasnym ^zyciem. D^eby p"+
             "rzeplataj^ace si^e z grabami, wi^azami, sosnami, czy jod^l"+
             "ami stanowi^a najliczniejsz^a grup^e gatunkow^a w tym miejsc"+
             "u, a wszystkie drzewa ca^lkowicie sk^apane w jasno^sci "+
             "nadaj^a temu miejscu wewn^etrznej ^swietlisto^sci dodatkowo "+
             "pot^egowanej pi^eknymi kolorami li^sci przebarwiaj^acymi s"+
             "i^e pod wp^lywem czasu. Leszczyna czy ja^lowiec to g^l^own"+
             "e elementy podszytu le^snego, natomiast na ziemi dostrzec "+
             " mo^zna mech, paprocie czy le^sne kwiaty, kt^ore t^lumi^a od"+
             "g^losy krok^ow nie zak^l^ocaj^ac koncertu jaki daj^a ptaki"+
             " zamieszkuj^ace korony drzew.";
    }
    }
    if(jest_dzien() == 0)
    {
    if(CZY_JEST_SNIEG(this_object()))
    {
        str="Gdzie nie spojrzysz - rozci^aga si^e przed tob^a pl^atanin"+
             "a ga^l^ezi, g^estwina lasu zamieszkana w czasie lata przez"+
             " wiele ciekawych zwierz^at aktualnie wydaje si^e ca^lkowic"+
             "ie u^spiona. D^eby przeplataj^ace si^e z grabami, wi^azami"+
             ", sosnami, czy jod^lami stanowi^a najliczniejsz^a grup^e gat"+
             "unkow^a w tym miejscu, a drzewa ca^lkowicie sk^apane w ^s"+
             "wietle ksi^e^zyca pot^egowanej odblaskiem ^swiat^la od biel"+
             "utkiego ^sniegu nadaj^a temu miejscu wewn^etrznej ^swietli"+
             "sto^sci. R^o^zne krzewy rozsiane mi^edzy drzewami wdzi^ecz"+
             "nie dope^lniaj^a krajobrazu, natomiast pod nogami rozpo^sci"+
             "era si^e bia^ly dywan, kt^ory t^lumi odg^losy krok^ow nie "+
             "zak^l^ocaj^ac ciszy tego miejsca.";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str="Gdzie nie spojrzysz - rozci^agaj^a si^e przed tob^a spl^at"+
             "ane ga^l^ezie, trawy i li^scie, a g^estwina zamieszkana pr"+
             "zez wiele ciekawych zwierz^at t^etni w^lasnym ^zyciem. D^e"+
             "by przeplataj^ace si^e tu z grabami, wi^azami, sosnami, cz"+
             "y jod^lami stanowi^a najliczniejsz^a grup^e gatunkow^a w tym"+
             " miejscu, a wszystkie drzewa razem, ca^lkowicie sk^apane w"+
             " ^swietle ksi^e^zyca nadaj^a temu miejscu wewn^etrznej ^sw"+
             "ietlisto^sci. R^o^zne krzewy rozsiane mi^edzy drzewami wdz"+
             "i^ecznie dope^lniaj^a krajobrazu, a porastaj^ace ziemi^e m"+
             "chy i paprocie t^lumi^a odg^losy krok^ow zapewniaj^ac miejsc"+
             "u swoist^a magi^e.";
    }
    else if(pora_roku() == MT_LATO)
    {
        str="Gdzie nie spojrzysz - rozci^agaj^a si^e przed tob^a spl^at"+
             "ane ga^l^ezie, trawy i li^scie, a g^estwina zamieszkana pr"+
             "zez wiele ciekawych zwierz^at t^etni w^lasnym ^zyciem. D^e"+
             "by przeplataj^ace si^e tu z grabami, wi^azami, sosnami, cz"+
             "y jod^lami stanowi^a najliczniejsz^a grup^e gatunkow^a w tym"+
             " miejscu, a wszystkie drzewa razem, ca^lkowicie sk^apane w"+
             " ^swietle ksi^e^zyca nadaj^a temu miejscu wewn^etrznej ^sw"+
             "ietlisto^sci. R^o^zne krzewy rozsiane mi^edzy drzewami wdz"+
             "i^ecznie dope^lniaj^a krajobrazu, a porastaj^ace ziemi^e m"+
             "chy i paprocie t^lumi^a odg^losy krok^ow zapewniaj^ac miejsc"+
             "u swoist^a magi^e.";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str="Gdzie nie spojrzysz - rozci^aga si^e przed tob^a pl^atanin"+
             "a ga^l^ezi, traw i li^sci, g^estwina lasu zamieszkana prze"+
             "z wiele ciekawych zwierz^at t^etni w^lasnym ^zyciem. D^eby"+
             " przeplataj^ace si^e z grabami, wi^azami, sosnami, czy jod"+
             "^lami stanowi^a najliczniejsz^a grup^e gatunkow^a w tym miej"+
             "scu, a wszystkie drzewa ca^lkowicie sk^apane w ^swietle k"+
             "si^e^zyca nadaj^a temu miejscu wewn^etrznej ^swietlisto^sc"+
             "i. R^o^zne krzewy rozsiane mi^edzy drzewami wdzi^ecznie do"+
             "pe^lniaj^a krajobrazu, a porastaj^ace ziemi^e mchy i papro"+
             "cie t^lumi^a odg^losy krok^ow zapewniaj^ac miejscu swoist^a "+
             "magi^e.";
    }
    }
    str += "\n";
    return str;
}

