/* Las komanda
   Autor: Avard 
   Opis : Edrain */

#include <stdproperties.h>
#include <macros.h>
#include <mudtime.h>
#include "dir.h"
inherit LAS_ELFOW_MROCZNY;

string dlugi_opis();

string opis_kwiatow();
string opis_leszczyny();

void
create_las()
{
    set_short("Skraj ciemnego lasu");
    set_long(&dlugi_opis());
    add_exit(LAS_ELFOW_LOKACJE+ "las_70.c","e");
    
    add_item(({"kwiaty","kwiat"}), &opis_kwiatow());
    add_item(({"leszczyn�","krzak","krzew",
    "krzak leszczyny","krzew leszczyny"}), &opis_leszczyny());
}

string dlugi_opis()
{
    string str = "";
    
    if(jest_dzien())
    {
        if(CZY_JEST_SNIEG(this_object()))
        {
            str += "Promienie s�o�ca docieraj�ce w stron� ziemi, odbijaj� "+
            "si� od srebrzystych pni drzew zalewaj�c okolic� ostrym blaskiem. "+
            "W miejscu tym smuk�e buki mieszaj� si� z kr�pymi sylwetkami "+
            "krzew�w le�nych, pot�nymi d�bami i wysokimi wi�zami. Pod�o�e "+
            "pokryte jest zimny puchem �niegu, odbijaj�cym ostre �wiat�o "+
            "mro�nego dnia zimy.";
        }
        else
        {
            str += "Promienie s�o�ca docieraj�ce w stron� ziemi, odbijaj� "+
            "si� od srebrzystych pni drzew zalewaj�c okolic� ciep�ym "+
            "blaskiem. W miejscu tym smuk�e buki mieszaj� si� z kr�pymi "+
            "sylwetkami krzew�w le�nych, pot�nymi d�bami i wysokimi "+
            "wi�zami. Pod�o�e usiane r�nokolorowym kwieciem cieszy oczy "+
            "swoj� �wie�o�ci�,a odurzaj�cy zapach lasu powoduje zawroty "+
            "g�owy. Bzyczenie owad�w zapylaj�cych kwiaty przeplata si� z "+
            "weso�ym �piewem ptak�w. I tylko mroczny las majacz�cy w oddali "+
            "napawa l�kiem.";
        }
    }
    else
    {
        if(CZY_JEST_SNIEG(this_object()))
        {
            str += "Blask ksi�yca docieraj�cego w stron� ziemi, odbija si� "+
            "od srebrzystych pni drzew zalewaj�c okolic� delikatnym, sino "+
            "fioletowym blaskiem. W miejscu tym smuk�e buki mieszaj� si� z "+
            "kr�pymi sylwetkami krzew�w le�nych, pot�nymi d�bami i "+
            "wysokimi wi�zami. Pod�o�e pokryte jest zimny puchem �niegu.";
        }
        else
        {
            str += "Blask ksi�yca docieraj�cego w stron� ziemi, odbija si� "+
            "od srebrzystych pni drzew zalewaj�c okolic� delikatnym, sino "+
            "fioletowym �wiat�em. W miejscu tym smuk�e buki mieszaj� si� z "+
            "kr�pymi sylwetkami krzew�w le�nych, pot�nymi d�bami i wysokimi "+
            "wi�zami. Pod�o�e usiane r�nokolorowym kwieciem cieszy oczy "+
            "swoj� �wie�o�ci�,a odurzaj�cy zapach lasu powoduje zawroty "+
            "g�owy. Bzyczenie owad�w zapylaj�cych kwiaty przeplata si� z "+
            "weso�ym �piewem ptak�w. I tylko mroczny las majacz�cy w oddali "+
            "napawa l�kiem.";
        }
    }
    str += "\n";
    return str;
}

string opis_wrzosow()
{
    string str;
    if (pora_roku() == MT_JESIEN)
    {
        str = "Ta zimozielona dwupienna krzewinka o listkach "+
        "rozes^lanych po ziemi, p^lo^zy si^e g^esto "+
        "wsz^edzie dooko^la, tworz^ac wyciszaj^acy dla "+
        "krok^ow ciemnozielony dywan. Jej drobne, "+
        "igie^lkowate li^scie u^lo^zone s^a naprzeciw siebie "+
        "i sprawiaj^a mylne wra^zenie bardzo ubogich, "+
        "jednak s^a one tylko dodatkiem do pi^eknych "+
        "r^o^zowo- fioletowych kwiat^ow w pe^lni "+
        "rozkwitaj^acych wczesn^a jesieni^a, r^ownie "+
        "drobnych, a jednak zachwycaj^acych swym pi^eknem, "+
        "kt^ore maluje ^swiat w czasie gdy prawie wszelkie "+
        "inne ozdoby przyrody ju^z przekwit^ly.\n";
        return str;
    }
    else
    {
        return 0;
    }
}

string opis_kwiatow()
{
    string str;
    if (pora_roku() == MT_WIOSNA || pora_roku() == MT_LATO)
    {
        str = "Bujne, w�ciekle zielone li�cie tworz� co� na kszta�t sp�jnej "+
        "kuli, na kt�rej wierzchu znajduj� si� liczne kwiaty o mlecznej "+
        "barwie. Ka�dy kwiat ma potr�jn� warstw� p�atk�w, co czyni je "+
        "okazalszymi.\n";
        return str;
    }
    else if (pora_roku() == MT_JESIEN)
    {
        str = "Bujne, zielono- czerwone li�cie tworz� co� na kszta�t sp�jnej "+
        "kuli, na kt�rej wierzchu znajduj� si� liczne pozosta�o�ci po "+
        "niegdy� pi�knych kwiatach.\n";
        return str;
    }
    else
    {
        return 0;
    }
}
string opis_leszczyny()
{
    string str;
    if (pora_roku() == MT_WIOSNA )
    {
        str = "Jest to niewysoka forma drzewiasta krzewu o �redniej "+
        "wielko�ci, kt�rego pie� w barwie szarobr�zowej do�� g�sto "+
        "przetykany jest bia�awymi przetchlinkami, kt�re s� jedynym "+
        "elementem naruszaj�cym niesamowit� g�adko�� kory. Krzew ten bogaty "+
        "jest w do�� du�e, okr�g�awe li�cie, u nasady sercowate, kr�tko "+
        "zaostrzone, o barwie soczystej zieleni., aktualnie g�sto "+
        "przetykany br�zowo ��tymi kotami, kt�re zwisaj� z ga��zi "+
        "wystawione na dzia�anie wiatru rozsiewaj�cego py�ek po okolicy.\n";
        return str;
    }
    if (pora_roku() == MT_LATO )
    {
        str = "Jest to niewysoka forma drzewiasta krzewu o �redniej "+
        "wielko�ci, kt�rego pie� w barwie szarobr�zowej do�� g�sto "+
        "przetykany jest bia�awymi przetchlinkami, kt�re s� jedynym "+
        "elementem naruszaj�cym niesamowit� g�adko�� kory. Krzew ten bogaty "+
        "jest w do�� du�e, okr�g�awe li�cie, u nasady sercowate, kr�tko "+
        "zaostrzone, o barwie soczystej zieleni.\n";
        return str;
    }
    if (pora_roku() == MT_JESIEN )
    {
        str = "Jest to niewysoka forma drzewiasta krzewu o �redniej "+
        "wielko�ci, kt�rego pie� w barwie szarobr�zowej do�� g�sto "+
        "przetykany jest bia�awymi przetchlinkami, kt�re s� jedynym "+
        "elementem naruszaj�cym niesamowit� g�adko�� kory. Krzew ten bogaty "+
        "jest w do�� du�e, okr�g�awe li�cie, u nasady sercowate, kr�tko "+
        "zaostrzone, o barwie ��tozielonej.\n";
        return str;
    }
    if (pora_roku() == MT_ZIMA )
    {
        str = "Jest to niewysoka forma drzewiasta krzewu o �redniej "+
        "wielko�ci, kt�rego pie� w barwie szarobr�zowej do�� g�sto "+
        "przetykany jest bia�awymi przetchlinkami, kt�re s� jedynym "+
        "elementem naruszaj�cym niesamowit� g�adko�� kory. Obecnie na "+
        "krzewie dostrzegasz tylko pojedyncze, obumar�e li�cie, kt�re nie "+
        "spad�y na jesieni, a teraz trzymaj� si� tylko przez dzia�anie "+
        "mrozu.\n";
        return str;
    }
}
