/**
 * Las Komanda
 *
 * autor Cairell
 * opisy Edrain & Aladriel
 * data  styczen 2009
 */

#include <stdproperties.h>
#include <mudtime.h>
#include "dir.h"
inherit LAS_ELFOW_RZEKA;

string dlugi_opis();
string opis_paproci();

void
create_las()
{
    set_short("Lipowy lasek.");
    set_long(&dlugi_opis());
    add_exit(TRAKT_LOKACJE+ "las_27.c","ne");
    add_exit(LAS_ELFOW_LOKACJE+ "bagno_3.c","n");
    add_exit(LAS_ELFOW_LOKACJE+ "las_23.c","e");
    add_exit(LAS_ELFOW_LOKACJE+ "las_6.c","w");
    add_exit(LAS_ELFOW_LOKACJE+ "las_8.c","sw");
    add_exit(LAS_ELFOW_LOKACJE+ "las_9.c","s");
    add_exit(LAS_ELFOW_LOKACJE+ "las_10.c","se");
    add_item(({"papro^c", "paproc", "paprocie"}), &opis_paproci());
}

string dlugi_opis()
{
    string str = ""; 
    
    str+="Panuje tu niezm^acona krzykami ludzi cisza, a niedaleko "+
        "szumi^a wody wartkiego Pontaru. ";

    if (jest_dzien() == 1)
    {
        if(pora_roku() == MT_WIOSNA)
        {
            str += "W koronie drzew zielony czy^zyk dzielnie "+
                "wy^spiewuje trele na cze^s^c swej ukochanej "+
                "partnerki. Stare leszczyny rosn^a tu do^s^c "+
                "rzadko, ze wzgl^edu na blisko^s^c wody. Wszelkie "+
                "krzewy zosta^ly wyparte przez trawy i tataraki "+
                "porastaj^ace brzeg rzeki. Ro^snie tu r^ownie^z "+
                "kilka paproci. ^Spiew ptak^ow i szum wody dzia^la "+
                "koj^aco i uspokajaj^aco. ";
        }
        if(pora_roku() == MT_LATO)
        {
            str += "W koronie drzew zielony czy^zyk dzielnie "+
                "wy^spiewuje trele na cze^s^c swej ukochanej "+
                "partnerki. Stare leszczyny rosn^a tu do^s^c "+
                "rzadko, ze wzgl^edu na blisko^s^c wody. Wszelkie "+
                "krzewy zosta^ly wyparte przez trawy i tataraki "+
                "porastaj^ace brzeg rzeki. Ro^snie tu r^ownie^z "+
                "kilka paproci. ^Spiew ptak^ow i szum wody dzia^la "+
                "koj^aco i uspokajaj^aco. ";
        }
        if(pora_roku() == MT_JESIEN)
        {
            str += "W koronie drzew zielony czy^zyk dzielnie "+
                "wy^spiewuje trele na cze^s^c swej ukochanej "+
                "partnerki. Stare leszczyny rosn^a tu do^s^c "+
                "rzadko, ze wzgl^edu na blisko^s^c wody. Wszelkie "+
                "krzewy zosta^ly wyparte przez trawy i tataraki "+
                "porastaj^ace brzeg rzeki. Ro^snie tu r^ownie^z "+
                "kilka paproci. Ro^sliny zdaj^a si^e powoli "+
                "przygotowywa^c do zimnowego snu, o czym "+
                "^swiadcz^a przebarwione li^scie i br^azowa ju^z "+
                "trawa, g^esto przetykana dojrza^lymi orzechami "+
                "laskowymi. ^Spiew ptak^ow i szum wody dzia^la "+
                "koj^aco i uspokajaj^aco. ";
        }
        if(pora_roku() == MT_ZIMA)
        {
            str += "W koronie drzew ptaki wykrzykuj^a do siebie "+
                "trele w czasie bitew o ostatnie jesienne owoce, "+
                "tak upragnione w mrozne, zimowe dnie. Stare "+
                "leszczyny, pokryte teraz grub^a warst^a bia^lego "+
                "puchu rosn^a tu do^s^c rzadko, ze wzgl^edu na "+
                "blisko^s^c wody. Wszelkie krzewy zosta^ly "+
                "wyparte przez trawy i tataraki porastaj^ace "+
                "brzeg rzeki, kt^ory teraz ^scina twardy l^od. ";

        }
    }
    else //noc
    {
        str += "Mroki nocy pog^l^ebione przez cienie drzew"+
            "nie pozwalaj^a poza tym odr^o^zni^c zbyt wielu "+
            "szczeg^o^l^ow. ";
    }

str+="\n";
return str;
}

string opis_paproci()
{
    string strr;
    if (pora_roku() == MT_ZIMA)
    {
        strr = "Spod ^sniegu wystaj^a tylko pojedyncze, cienkie "+
            "patyczki zesz^lorocznych paproci.\n";
        return strr;
    }
    if (pora_roku() == MT_JESIEN)
    {
        strr = "Niewysoka, lecz posiadaj^aca du^ze li�cie papro^c "+
            "ugina si^e do ziemi pod ci^e^zarem zal^a^zk^ow "+
            "znajduj^acych si^e pod listkami. Jej przebarwiaj^ace "+
            "si^e na br^azowo li^scie, powoli usychaj^ace, "+
            "przypominaj^a o nadchodz^acej zimie. Mi^edzy jej "+
            "listkami wiele ma^lych stworzonek znalaz^lo "+
            "schronienie.";
        return strr;
    }
    else
    {
        strr = "Niewysoka, lecz posiadaj^aca du^ze li^scie papro^c "+
            "ugina si^e do ziemi pod ci^e^zarem zal^a^zk^ow "+
            "znajduj^acych si^e pod listkami. Jej soczysty, "+
            "zielony kolor o^zywia miejsce. Mi^edzy jej "+
            "listkami wiele ma^lych stworzonek znalaz^lo "+
            "schronienie.\n";
        return strr;
    }
}

public string
exits_description() 
{
    return "Na p^o^lnocy wida^c niewielkie bagienko, "+
        "za^s na zachodzie, wschodzie, po^ludniu, po^ludniowym "+
        "wschodzie i po^ludniowym zachodzi rozci^aga si^e "+
        "dalsza cz^e^s^c lasu. Na po^lnocnym wschodzie mi^edzy "+
        "drzewami majaczy trakt.\n";
}