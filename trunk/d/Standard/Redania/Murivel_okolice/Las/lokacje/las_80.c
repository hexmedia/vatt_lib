/**
 * Las Komanda
 *
 * autor Cairell
 * opisy Edrain & Aladriel
 * data  styczen 2009
 */
 
#include <stdproperties.h>
#include <mudtime.h>
#include "dir.h"
#include "pogoda.h"
#include <macros.h>

inherit LAS_ELFOW_RZEKA;

string dlugi_opis();

string opis_dzwonkow();
string opis_wrzosow();
string opis_fiolkow();
string opis_zawilcow();
string opis_plyt();


void create_las()
{
    set_short("Ruiny starej fontanny.");
    set_long(&dlugi_opis());

    add_exit(LAS_ELFOW_LOKACJE+ "las_75.c","sw");

    add_item(({"zawilca","zawilce"}), &opis_zawilcow());
    add_item(({"dzwonek","dzwonki"}), &opis_dzwonkow());
    add_item(({"wrzos","wrzosy"}), &opis_wrzosow());
    add_item(({"fio^lka","fio^lki"}), &opis_fiolkow());
    add_item(({"p^lyt^e","p^lyty"}), &opis_plyt());

}

string dlugi_opis()
{
    string str = "";
    if(pora_roku() == MT_ZIMA)
    {
        if (jest_dzien() == 1)
        {
            if (CZY_JEST_SNIEG(this_object()))
            {
                  if (ZACHMURZENIE(TO) <= 2)
                {
                str += "S^loneczne ^swiat^lo zalewaj^ace okolic^e rozprasza "+
                "si^e na bia^lym kamieniu i skrzy si^e na ^sniegu. ";
                }
                else
                {
                str += "";
                } 
                str += "P^lyty s^a tu uformowane w fontann^e, kt^ora niestety "+
                "ju^z dawno ma za sob^a czasy ^swietno^sci. W wielu "+
                "miejscach ukruszona i zniszczona nie przypomina "+
                "cudownej budowli, kt^or^a niew^atpliwie niegdy^s "+
                "by^la. Mimo to cieszy oczy swym nieokie^lznanym "+
                "pi^eknem, a wci^a^z tryskaj^aca weso^lo woda napawa "+
                "rado^sci^a. Ca^la okolica pokryta jest malowniczymi "+
                "zaspami bia^lego puchu."; 
            }
                else
            { 
             
                if (ZACHMURZENIE(TO) <= 2)
                {
                str += "S^loneczne ^swiat^lo zalewaj^ace okolic^e rozprasza "+
                "si^e na bia^lym kamieniu. ";
                }
                else
                {
                str += "";
                } 
                str += "P^lyty s^a tu uformowane w fontann^e, kt^ora niestety "+
                "ju^z dawno ma za sob^a czasy ^swietno^sci. W wielu "+
                "miejscach ukruszona i zniszczona nie przypomina "+
                "cudownej budowli, kt^or^a niew^atpliwie niegdy^s "+
                "by^la. Mimo to cieszy oczy swym nieokie^lznanym "+
                "pi^eknem, a wci^a^z tryskaj^aca weso^lo woda napawa "+
                "rado^sci^a. "; 
            }
        }
        else //noc
        {
            if (CZY_JEST_SNIEG(this_object()))
            {
                  if (ZACHMURZENIE(TO) <= 2)
                {
                str += "Srebrzyste smugi ksi^e^zycowego blasku zalewaj^a "+
                "okolic^e rozpraszaj^ac si^e na bia^lym kamieniu i "+
                "skrzy si^e na ^sniegu. "; 
                }
                else
                {
                str += "";
                } 
                str += "P^lyty s^a tu uformowane w fontann^e, kt^ora niestety "+
                "ju^z dawno ma za sob^a czasy ^swietno^sci. W wielu "+
                "miejscach ukruszona i zniszczona nie przypomina "+
                "cudownej budowli, kt^or^a niew^atpliwie niegdy^s "+
                "by^la. Mimo to cieszy oczy swym nieokie^lznanym "+
                "pi^eknem, a wci^a^z tryskaj^aca weso^lo woda napawa "+
                "rado^sci^a. Ca^la okolica pokryta jest malowniczymi "+
                "zaspami bia^lego puchu."; 
            }
                else
            { 
             
                if (ZACHMURZENIE(TO) <= 2)
                {
                str += "Srebrzyste smugi ksi^e^zycowego blasku zalewaj^a "+
                "okolic^e rozpraszaj^ac si^e na bia^lym kamieniu. "; 
                }
                else
                {
                str += "";
                } 
                str += "P^lyty s^a tu uformowane w fontann^e, kt^ora niestety "+
                "ju^z dawno ma za sob^a czasy ^swietno^sci. W wielu "+
                "miejscach ukruszona i zniszczona nie przypomina "+
                "cudownej budowli, kt^or^a niew^atpliwie niegdy^s "+
                "by^la. Mimo to cieszy oczy swym nieokie^lznanym "+
                "pi^eknem, a wci^a^z tryskaj^aca weso^lo woda napawa "+
                "rado^sci^a. ";
            }
        }
    }
    else if(pora_roku() == MT_JESIEN)
    {
        if (jest_dzien() == 1)
        { 
            if (ZACHMURZENIE(TO) <= 2)
            {
            str += "S^loneczne ^swiat^lo zalewaj^ace okolic^e rozprasza "+
            "si^e na bia^lym kamieniu.";
            }
            else
            {
            str += "";
            } 
            str += "P^lyty s^a tu uformowane w "+
            "fontann^e, kt^ora niestety ju^z dawno ma za sob^a "+
            "czasy ^swietno^sci. W wielu miejscach ukruszona i "+
            "zniszczona nie przypomina cudownej budowli, kt^or^a "+
            "niew^atpliwie niegdy^s by^la. Mimo to cieszy oczy "+
            "swym nieokie^lznanym pi^eknem, a wci^a^z tryskaj^aca "+
            "weso^lo woda napawa rado^sci^a. Dooko^la fontanny "+
            "rosn^a wrzosy i dzwonki le^sne zraszane kroplami "+
            "rozbijaj^acymi si^e na bia^lych p^lytach."; 
        }
            else //noc
        { 
            if (ZACHMURZENIE(TO) <= 2)
            {
            str += "Srebrzyste smugi ksi^e^zycowego blasku zalewaj^a "+
            "okolic^e rozpraszaj^ac si^e na bia^lym kamieniu.";
            }
            else
            {
            str += "";
            } 
        str += "P^lyty s^a tu uformowane w fontann^e, kt^ora niestety "+
            "ju^z dawno ma za sob^a czasy ^swietno^sci. W wielu "+
            "miejscach ukruszona i zniszczona nie przypomina "+
            "cudownej budowli, kt^or^a niew^atpliwie niegdy^s "+
            "by^la. Mimo to cieszy oczy swym nieokie^lznanym "+
            "pi^eknem, a wci^a^z tryskaj^aca weso^lo woda napawa "+
            "rado^sci^a. Dooko^la fontanny rosn^a wrzosy i dzwonki "+
            "le^sne zraszane kroplami rozbijaj^acymi si^e na "+
            "bia^lych p^lytach."; 
        }
    }
    else //Lato_wiosna
    {
        if (jest_dzien() == 1)
        { 
            if (ZACHMURZENIE(TO) <= 2)
            {
            str += "S^loneczne ^swiat^lo zalewaj^ace okolic^e rozprasza "+
            "si^e na bia^lym kamieniu.";
            }
            else
            {
            str += "";
            } 
             str += "P^lyty s^a tu uformowane w "+
            "fontann^e, kt^ora niestety ju^z dawno ma za sob^a "+
            "czasy ^swietno^sci. W wielu miejscach ukruszona i "+
            "zniszczona nie przypomina cudownej budowli, kt^or^a "+
            "niew^atpliwie niegdy^s by^la. Mimo to cieszy oczy "+
            "swym nieokie^lznanym pi^eknem, a wci^a^z tryskaj^aca "+
            "weso^lo woda napawa rado^sci^a. Dooko^la fontanny "+
            "rosn^a fio^lki i zawilce zraszane kroplami "+
            "rozbijaj^acymi si^e na bia^lych p^lytach."; 
        }
            else //noc
        { 
            if (ZACHMURZENIE(TO) <= 2)
            {
            str += "Srebrzyste smugi ksi^e^zycowego blasku zalewaj^a "+
            "okolic^e rozpraszaj^ac si^e na bia^lym kamieniu. ";
            }
            else
            {
            str += "";
            } 
            str += "P^lyty s^a tu uformowane w fontann^e, kt^ora niestety "+
            "ju^z dawno ma za sob^a czasy ^swietno^sci. W wielu "+
            "miejscach ukruszona i zniszczona nie przypomina "+
            "cudownej budowli, kt^or^a niew^atpliwie niegdy^s "+
            "by^la. Mimo to cieszy oczy swym nieokie^lznanym "+
            "pi^eknem, a wci^a^z tryskaj^aca weso^lo woda napawa "+
            "rado^sci^a. Dooko^la fontanny rosn^a fio^lki i zawilce "+
            "zraszane kroplami rozbijaj^acymi si^e na bia^lych "+
            "p^lytach."; 
        }
    }

str+="\n";
return str;
}

string opis_zawilcow()
{
    string str;
    if (pora_roku() == MT_WIOSNA)
    {
    str += "Zawilec gajowy jest pojedynczym kwiatem o d^lugiej "+
    "delikatnej ^lody^zce wyrastaj^acej z k^l^acza. "+
    "Poszarpane listki, zazwyczaj tylko trzy "+
    "wyst^epuj^ace w ok^o^lkach i wygl^adaj^a jak szpony "+
    "jakiej^s zjawy. Aktualnie ka^zda ^lody^zka zwie^nczona "+
    "jest prze^slicznym bia^lym kwiatkiem, sk^ladaj^acym "+
    "si^e z pi^eciu lub siedmiu p^latk^ow. Soki tej "+
    "ro^sliny s^a silnie truj^ace i powoduj^a zapalenie "+
    "nerek i uk^ladu pokarmowego, wywo^luj^ac silne "+
    "wymioty i biegunk^e.\n"; 
    return str;
    }
    else
    {
    return 0;
    }
}

string opis_wrzosow()
{
    string str;
    if (pora_roku() == MT_JESIEN)
    {
    str = "Ta zimozielona dwupienna krzewinka o listkach "+
    "rozes^lanych po ziemi, p^lo^zy si^e g^esto "+
    "wsz^edzie dooko^la, tworz^ac wyciszaj^acy dla "+
    "krok^ow ciemnozielony dywan. Jej drobne, "+
    "igie^lkowate li^scie u^lo^zone s^a naprzeciw siebie "+
    "i sprawiaj^a mylne wra^zenie bardzo ubogich, "+
    "jednak s^a one tylko dodatkiem do pi^eknych "+
    "r^o^zowo- fioletowych kwiat^ow w pe^lni "+
    "rozkwitaj^acych wczesn^a jesieni^a, r^ownie "+
    "drobnych, a jednak zachwycaj^acych swym pi^eknem, "+
    "kt^ore maluje ^swiat w czasie gdy prawie wszelkie "+
    "inne ozdoby przyrody ju^z przekwit^ly.\n";
    return str;
    }
    else
    {
    return 0;
    }
}

string opis_dzwonkow()
{
    string str;
    if (pora_roku() == MT_LATO)
    {
    str = "Wiotkie ^lody^zki wznosz^a si^e kilkadziesi^at "+
    "centymetr^ow nad ziemi^e, ich ko^nce zwieszaj^a "+
    "si^e w stron^e pod^lo^za obci^a^zone okaza^lymi "+
    "kwiatami. Fioletowe, zros^le p^latki tworz^a kielich "+
    "z jasno^z^o^ltym ^srodkiem. Odurzaj^acy zapach "+
    "dzwonk^ow le^snych wabi owady.\n";
    return str;
    }
    else
    {
    return 0;
    }
}

string opis_fiolkow()
{
    string str;
    if (pora_roku() == MT_WIOSNA)
    {
    str = "P^lo^z^aca si^e po ziemi bylina jest teraz g^esto "+
    "upstrzona pi^eknie pachn^acymi fioletowymi "+
    "kwiatami. Delikatne kwiaty wyniesione s^a nad "+
    "ziemie na wysokich szypu^lkach, ka^zdy z nich "+
    "sk^lada si^e z pi^eciu p^latk^ow dw^och "+
    "skierowanych w g^or^e i trzech w d^o^l. "+
    "Zielone listki, kt^ore dodatkowo eksponuj^a "+
    "pi^ekno koloru fio^lk^ow, maj^a kszta^lt sercowaty "+
    "i s^a lekko karbowane od g^ory.\n";
    return str;
    }
    else if (pora_roku() == MT_LATO)
    {
    str = "Na p^lo^z^acej si^e po ziemi bylinie nie wida^c "+
    "ju^z ^slad^ow niedawno pi^eknie kwitn^acych, "+
    "drobnych p^latk^ow fio^lk^ow. Z wonnej ro^sliny "+
    "pozosta^ly ^lody^zki i ciemnozielone, sercowate "+
    "li^scie, kt^ore t^lumi^a wszelkie odg^losy "+
    "zwierz^at i podr^o^znych.\n";
    return str;
    }
    else
    {
    return 0;
    }
}
string opis_plyt()
{
    string str;
    if (jest_dzien() == 1)
    {
        if (ZACHMURZENIE(TO) <= 2)
        {
        str =  "Bia^le, marmurowe p^lyty odbijaj^a promienei "+
        "sloneczne, kt^ore zalewa ca^l^a okolic^e. ";
        }
        else
        {
            str = "";
        }
    str += "Roztrzaskany kamie^n w wielu miejscach jest "+
    "porysowany i wy^z^lobiony przez czas i natur^e. "+
    "W niekt^orych miejscach p^lyty zachodz^a "+
    "na siebie tworz^ac wiele miejsc o kt^ore "+
    "mo^zna si^e potkn^a^c.\n";
    return str;
    }
    else
    {
        if (ZACHMURZENIE(TO) <= 2)
        {
        str = "Bia^le, marmurowe p^lyty odbijaj^a smugi blasku "+
        "ksiezyca, kt^ore zalewaja ca^l^a okolic^e.";
        }
        else
        {
        str = "";
        }
    str += "Roztrzaskany kamie^n w wielu miejscach "+
    "wy^z^lobiony przez czas i natur^e. W niekt^orych "+
    "miejscach p^lyty zachodz^a na siebie tworz^ac "+
    "wiele miejsc o kt^ore mo^zna si^e potkn^a^c.\n";
    return str;
    }
}



