/* Las komanda
   Autor: Avard 
   Opis : Edrain */

#include <stdproperties.h>
#include <macros.h>
#include <mudtime.h>
#include "dir.h"
inherit LAS_ELFOW_GESTY;

string dlugi_opis();

string opis_kwiatow();
string opis_paproci();

void
create_las()
{
    set_short("Puszcza bukowa");
    set_long(&dlugi_opis());
    add_exit(LAS_ELFOW_LOKACJE+ "las_71.c","w");
    add_exit(LAS_ELFOW_LOKACJE+ "las_74.c","nw");
    add_exit(LAS_ELFOW_LOKACJE+ "las_75.c","n");
    
    if(!CZY_JEST_SNIEG(this_object()))
    {
        add_item("mech","Po�acie soczysto zielonych �ody�ek wznosz? si� "+
        "nieznacznie nad ziemi�. Mi�dzy drobnymi listkami gromadz? "+
        "du�e ilo?ci wody, przez co ca�o?� jest wyj?tkowo wilgotna. "+
        "Ziemia zas�ana tymi ro?linkami wygl?da jakby by�a pokryta "+
        "naturalnym dywanem.\n");
    }
    add_item(({"kwiat","kwiaty"}), &opis_kwiatow());
    add_item(({"papro�"}), &opis_paproci());
}

string dlugi_opis()
{
    string str = "";
    
    if(jest_dzien())
    {
        if(CZY_JEST_SNIEG(this_object()))
        {
            str+="Teren ten porastaj� strzeliste drzewa. Go�e, ca�kowicie "+
            "pozbawione ga��zi smuk�e pnie wygl�daj� z�owrogo i "+
            "nienaturalnie. Promienie �wiat�a nie dochodz� do ziemi "+
            "zatrzymywane przez korony drzew pokryte bia�ymi czapami. "+
            "Gdzieniegdzie tylko jasne smugi dostaj� si� na ni�sze pi�tra "+
            "lasu, przez co otoczenie zdaje si� jeszcze bardziej "+
            "niesamowite. Ziemia pokryta jest �niegiem ca�kowicie t�umi�cym "+
            "kroki.";
        }
        else if(pora_roku() == MT_WIOSNA || pora_roku() == MT_LATO)
        {
            str+="Teren ten porastaj� strzeliste drzewa. Go�e, ca�kowicie "+
            "pozbawione ga��zi smuk�e pnie wygl�daj� z�owrogo i "+
            "nienaturalnie. Promienie �wiat�a nie dochodz� do ziemi "+
            "zatrzymywane przez korony drzew. Gdzieniegdzie tylko jasne "+
            "smugi dostaj� si� na ni�sze pi�tra lasu, przez co otoczenie "+
            "zdaje si� jeszcze bardziej niesamowite. Ziemia g�sto poro�ni�ta "+
            "jest mokrym mchem, miniaturowymi paprociami i k�pkami kwiat�w.";
        }
        else
        {
            str+="Teren ten porastaj� strzeliste drzewa. Go�e, ca�kowicie "+
            "pozbawione ga��zi smuk�e pnie wygl�daj� z�owrogo i "+
            "nienaturalnie. Promienie �wiat�a nie dochodz� do ziemi "+
            "zatrzymywane przez korony drzew upstrzone r�nokolorowymi "+
            "li��mi. Gdzieniegdzie tylko jasne smugi dostaj� si� na ni�sze "+
            "pi�tra lasu, przez co otoczenie zdaje si� jeszcze bardziej "+
            "niesamowite. Ziemia g�sto poro�ni�ta jest suchym mchem, "+
            "miniaturowymi paprociami i k�pkami wrzos�w.";
        }
    }
    else
    {
        if(CZY_JEST_SNIEG(this_object()))
        {
            str+="Teren ten porastaj� strzeliste drzewa. Go�e, ca�kowicie "+
            "pozbawione ga��zi smuk�e pnie wygl�daj� z�owrogo i "+
            "nienaturalnie. Promienie sinego �wiat�a ksi�yca nie dochodz� "+
            "do ziemi zatrzymywane przez korony drzew pokryte bia�ymi "+
            "czapami. Gdzieniegdzie tylko fioletowe smugi dostaj� si� na "+
            "ni�sze pi�tra lasu, przez co otoczenie zdaje si� jeszcze "+
            "bardziej niesamowite. Ziemia pokryta jest �niegiem ca�kowicie "+
            "t�umi�cym kroki.";
        }
        else if(pora_roku() == MT_WIOSNA || pora_roku() == MT_LATO)
        {
            str+="Teren ten porastaj� strzeliste drzewa. Go�e, ca�kowicie "+
            "pozbawione ga��zi smuk�e pnie wygl�daj� z�owrogo i "+
            "nienaturalnie. Promienie sinego �wiat�a ksi�yca nie dochodz� "+
            "do ziemi zatrzymywane przez korony drzew. Gdzieniegdzie tylko "+
            "fioletowe smugi dostaj� si� na ni�sze pi�tra lasu, przez co "+
            "otoczenie zdaje si� jeszcze bardziej niesamowite. Ziemia g�sto "+
            "poro�ni�ta jest mokrym mchem, miniaturowymi paprociami i "+
            "k�pkami kwiat�w.";
        }
        else
        {
            str+="Teren ten porastaj� strzeliste drzewa. Go�e, ca�kowicie "+
            "pozbawione ga��zi smuk�e pnie wygl�daj� z�owrogo i "+
            "nienaturalnie. Promienie sinego �wiat�a ksi�yca nie dochodz� "+
            "do ziemi zatrzymywane przez korony drzew upstrzone "+
            "r�nokolorowymi li��mi. Gdzieniegdzie tylko fioletowe smugi "+
            "dostaj� si� na ni�sze pi�tra lasu, przez co otoczenie zdaje "+
            "si� jeszcze bardziej niesamowite. Ziemia g�sto poro�ni�ta jest "+
            "suchym mchem, miniaturowymi paprociami i k�pkami wrzos�w.";
        }
    }
    
    str += "\n";
    return str;
}
string opis_kwiatow()
{
    string str;
    if (pora_roku() == MT_JESIEN)
    {
    str += "Bujne, zielono- czerwone li^scie tworz^a co^s na "+
    "kszta^lt sp^ojnej kuli, na kt^orej wierzchu znajduj^a "+
    "si^e liczne pozosta^lo^sci po niegdy^s pi^eknych "+
    "kwiatach.\n"; 
    return str;
    }
    else if(pora_roku() == MT_ZIMA)
    {
    return 0;
    }
    else
    {
    str += "Bujne, w^sciekle zielone li^scie tworz^a co^s na "+
    "kszta^lt sp^ojnej kuli, na kt^orej wierzchu znajduj^a "+
    "si^e liczne kwiaty o mlecznej barwie. Ka^zdy kwiat "+
    "ma potr^ojn^a warstw^e p^latk^ow, co czyni je "+
    "okazalszymi.\n"; 
    return str;
    }
}

string opis_paproci()
{
    string str;
    if (pora_roku() == MT_WIOSNA || pora_roku() == MT_LATO)
    {
        str = "Niewysoka, lecz posiadaj?ca du�e li?cie papro� ugina si� do "+
        "ziemi pod ci�arem zal?�k�w znajduj?cych si� pod listkami. Jej "+
        "soczysty, zielony kolor o�ywia miejsce. Mi�dzy jej listkami wiele "+
        "ma�ych stworzonek znalaz�o schronienie.\n";
        return str;
    }
    else if (pora_roku() == MT_JESIEN)
    {
        str = "Niewysoka, lecz posiadaj?ca du�e li?cie papro� ugina si� do "+
        "ziemi pod ci�arem zal?�k�w znajduj?cych si� pod listkami. Jej "+
        "przebarwiaj?ce si� na br?zowo li?cie, powoli usychaj?ce, "+
        "przypominaj? o nadchodz?cej zimie. Mi�dzy jej listkami wiele "+
        "ma�ych stworzonek znalaz�o schronienie.\n";
        return str;
    }
    else return 0;
}
