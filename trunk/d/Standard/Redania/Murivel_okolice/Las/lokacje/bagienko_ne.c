/*Las Komanda
  opisy Eskela
  kod Cairell*/

#include <stdproperties.h>
#include <mudtime.h>
#include <macros.h>
#include "dir.h"
inherit LAS_ELFOW_BAGIENKO;

string dlugi_opis();
void do_niszy(string str);
void odsun_galaz(string str);
void bujam_sie();

void
create_bagienko()
{
    set_short("Podmok^la cz^e^s^c lasu.");
    set_long(&dlugi_opis());
    add_exit(TRAKT_LOKACJE+ "trakt_27.c","se");
    add_exit(LAS_ELFOW_LOKACJE+ "bagienko_nw.c","w");
    add_exit(LAS_ELFOW_LOKACJE+ "bagienko_sw.c","sw");
    add_exit(LAS_ELFOW_LOKACJE+ "bagienko_se.c","s");

    add_item(({"wode", "bagno"}), "Nieruchom^a tafl^e wody przykrywa "+
        "zielony dywan rz^esy i ^lopianu. Po chwili dostrzegasz "+
        "skrzydlate ^zycie tocz^ace si^e tu^z nad wod^a. Zielone wa^zki, "+
        "chmary drobniutkich muszek, brz^ecz�cych komar^ow i gz^ow unosz^a "+
        "si^e w bez^ladnym ta^ncu, wiruj^ac bez ko^nca.\n");

    add_item(({"trzciny", "tatarak", "rokicine", "k^epy rokiciny"}), "Wp^o^l "+
        "zgni^le, nasi^akniete wod^a trzciny stanowi^a azyl dla stworze^n. "+
        "Zielonkawo-brunatny kszta^lt, kt^ory wydawa^l si^e by^c ga^l^ezi^a, "+
        "drgn^a^l i zasycza^l jadowicie. Na li^sciu lilii wodnej wielkimi, "+
        "wy^lupiastymi oczami spoziera stara, paskudna ropucha. Nieopodal "+
        "za^s pstrokata cyranka w ca^lkowitym bezruchu, przekrzywiaj^ac "+
        "lekko ^lepek, patrzy czarnymi oczami w bli^zej nieokre^slony "+
        "punkt na wodzie.\n");
    add_item(({"gnaty", "gnaty drzew", "drzewa", "k^epy rokiciny"}), ""+
        "Spr^ochnia^le, nagryzione z^ebem czasu konary d^eb^ow i lip "+
        "pochylaj^a si^e w zadumie nad nieprzejrzyst^a wod^a. Niekt^ore "+
        "kikuty ga^l^ezi zwisaj^a bezradnie, inne jakby z pro^sb^a o pomoc "+
        "skierowane s^a ku g^orze. Drewno prze^zarte przez korniki zdaje "+
        "si^e trzeszcze^c niemym j^ekiem rozpaczy, a dziuple wyd^lubane przez "+
        "bezlitosne dzi^ecio^ly przypominaj^a czarne, przepe^lnione smutkiem "+
        "oczy. Powalone konary le^z^a do po^lowy w wodzie, powoli "+
        "zapadaj^ac si^e w bezdenne bagno.\n");
    add_item(({"ga^l^ezie", "konary"}), "Niedaleko dostrzegasz niewielk^a "+
        "nisz^e, skryt^a za nisko zwieszon^a ga^l^ezi^a lipy. Powsta^la w "+
        "naturalny spos^ob mi^edzy powalonym konarem, a kawa^lkiem w miar^e "+
        "suchego gruntu.\n");
}

init()
{
   ::init();
   add_action(odsun_galaz, "odsu^n");
   add_action(do_niszy, "nisza");
   add_action(do_niszy, "wejd^x");
}

void do_niszy(string str)
{
    if(str == "do niszy")
    {
        write("Schylaj^ac si^e nisko, w^lazisz do ciemnej niszy.\n");
        saybb(QCIMIE(TP, PL_MIA)+" schylaj^ac si^e nisko, w^lazi do ciemnej "+
            "niszy.\n");
        TP->move_living("M", LAS_ELFOW_LOKACJE+ "nisza.c");
        set_alarm(2.0,0.0,"bujam_sie");
    }
    else
    {
        write("Co odsun^a^c?\n");
    }
}

void bujam_sie()
{
    tell_roombb("Puszczona ga^l^a^z ko^lysze si^e chwil^e, by po chwili "+
        "zamrze� w bezruchu.\n");    
}

void odsun_galaz(string str)
{
    if(str == "ga^l^a^z")
    {
        write("Zdecydowanym ruchem odchylasz ga^l^a^x zas^laniaj^ac^a "+
            "niewielk^a nisz^e.\n");
        saybb(QCIMIE(TP, PL_MIA)+" zdecydowanym ruchem odchyla ga^l^a^x "+
            "zas^laniaj^ac^a niewielk^a nisz^e.\n");
        set_alarm(2.0,0.0,"bujam_sie");
    }
    else
    {
        write("Co chcesz odsun^a^c?\n");
    }
}

string dlugi_opis()
{
    string str;

    str = "G^l^ebokia kotlina, otoczona ledwo widocznym z dolu "+
        "traktem, jest przesi^akni^eta wilgoci^a. Na torfiastym ko^zuchu "+
        "rosn^a g�ste trzciny, tatarak i k^epy niskiej rokiciny, a olbrzymie "+
        "gnaty d^eb^ow i lip pochylaj^a si^e nad zielonym rozlewiskiem. "+
        "Pozornie martwa woda krywa w swej g^l^ebi mroczne tajemnice, kt^ore "+
        "na zawsze pozostan^a nieznane. D^xwi^ecz^ac^a w uszach cisz^e "+
        "przerywa czasem rechot niewidocznych ^zab i nie^smia�e trele ptak^ow "+
        "siedz^acych wysoko na ga^l^eziach drzew. Niezale^znie od pory dnia, "+
        "p^o^lmrok i drapie^zne j^ezory mg^ly nigdy nie odchodz^a z tego "+
        "miejsca.";
    
    str += "\n";
    return str;
}
