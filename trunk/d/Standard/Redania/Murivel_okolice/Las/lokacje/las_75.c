/**
 * Las Komanda
 *
 * autor Cairell
 * opisy Edrain & Aladriel
 * data  styczen 2009
 */
 
#include <stdproperties.h>
#include <mudtime.h>
#include "dir.h"
#include "pogoda.h"
#include <macros.h>

inherit LAS_ELFOW_RZEKA;

string dlugi_opis();

string opis_okien();
string opis_blokow();
string opis_kwiatow();


void create_las()
{
    set_short("Ruiny marmurowego pa^lacu.");
    set_long(&dlugi_opis());

    add_exit(LAS_ELFOW_LOKACJE+ "las_74.c","w");
    add_exit(LAS_ELFOW_LOKACJE+ "las_79.c","nw");
    add_exit(LAS_ELFOW_LOKACJE+ "las_80.c","ne");
    add_exit(LAS_ELFOW_LOKACJE+ "las_76.c","e");
    add_exit(LAS_ELFOW_LOKACJE+ "las_72.c","s");
    add_exit(LAS_ELFOW_LOKACJE+ "las_71.c","sw");

    add_item(({"kwiat","kwiaty"}), &opis_kwiatow());
    add_item(({"okno","okna"}), &opis_okien());
    add_item(({"blok","bloki"}), &opis_blokow());

}

string dlugi_opis()
{
    string str = "";
    if(pora_roku() == MT_ZIMA)
    {
    str += "Ukruszone przez czas wie^zyczki z bia^lego marmuru "+
    "wci^a^z zachwycaj^a niemym pi^eknem. Strzeliste okno "+
    "w murach zniszczone przez up^lywaj^ace lata "+
    "przypomina o przemijaj^acej ^swietno^sci tego "+
    "miejsca. Pod nogami ci^agn^a si^e rozrzucone, "+
    "pojedyncze kamienne bloki, na kt^orych igraj^a "+
    "cienie, zupe^lnie jakby bawi^ly si^e w chowanego. "; 
    }
    else if(pora_roku() == MT_JESIEN)
    {
    str += "Ukruszone przez czas wie^zyczki z bia^lego marmuru "+
    "wci^a^z zachwycaj^a niemym pi^eknem. Strzeliste okno "+
    "w murach zniszczone przez up^lywaj^ace lata "+
    "przypomina o przemijaj^acej ^swietno^sci tego "+
    "miejsca. Pod nogami ci^agn^a si^e rozrzucone, "+
    "pojedyncze kamienne bloki, na kt^orych igraj^a "+
    "cienie, zupe^lnie jakby bawi^ly si^e w chowanego. "+
    "Pod^lo^ze g^esto zas^lane jest opadaj^acymi z drzew "+
    "r^o^znokolorowymi li^s^cmi. M^lode brz^ozki o nagich "+
    "bezlistnych ga^l^eziach szumi^a w takt wiatru "+
    "przynosz^ac echo dawnych dni. ";  
    }
    else //Lato_wiosna
    {
    str += "Ukruszone przez czas wie^zyczki z bia^lego marmuru "+
    "wci^a^z zachwycaj^a niemym pi^eknem. Strzeliste okno "+
    "w murach zniszczone przez up^lywaj^ace lata "+
    "przypomina o przemijaj^acej ^swietno^sci tego "+
    "miejsca. Pod nogami ci^agn^a si^e rozrzucone, "+
    "pojedyncze kamienne bloki, na kt^orych igraj^a "+
    "cienie, zupe^lnie jakby bawi^ly si^e w chowanego. "+
    "Pod^lo^ze ju^z dawno zaros^lo traw^a i dzikimi "+
    "kwiatami. M^lode brz^ozki i krzewy sk^apane w "+
    "kwiatach szumi^a w takt wiatru przynosz^ac echo "+
    "dawnych dni. "; 
    }

str+="\n";
return str;
}

string opis_okien()
{
    string str;
    str += "Strzeliste okno zajmuj^ace znaczn^a cz^e^s^c ^sciany "+
    "ozdobione jest ro^slinnymi ornamentami "+
    "wyrze^xbionymi w bia^lym kamieniu. Zako^nczone "+
    "ostrym ^lukiem wydaje si^e by^c wy^zsze ni^z w "+
    "rzeczywisto^sci. Cienkie kolumienki wewn^atrz "+
    "dziel^a je na kilka cz^e^sci.\n"; 
    return str;
}

string opis_kwiatow()
{
    string str;
    if (pora_roku() == MT_JESIEN)
    {
    str += "Bujne, zielono- czerwone li^scie tworz^a co^s na "+
    "kszta^lt sp^ojnej kuli, na kt^orej wierzchu znajduj^a "+
    "si^e liczne pozosta^lo^sci po niegdy^s pi^eknych "+
    "kwiatach.\n"; 
    return str;
    }
    else if(pora_roku() == MT_ZIMA)
    {
    return 0;
    }
    else
    {
    str += "Bujne, w^sciekle zielone li^scie tworz^a co^s na "+
    "kszta^lt sp^ojnej kuli, na kt^orej wierzchu znajduj^a "+
    "si^e liczne kwiaty o mlecznej barwie. Ka^zdy kwiat "+
    "ma potr^ojn^a warstw^e p^latk^ow, co czyni je "+
    "okazalszymi.\n"; 
    return str;
    }
}

string opis_blokow()
{
    string str;
    str = "P^ekni^ete w wielu miejscach bloki marmuru musz^a "+
    "tu le^ze^c od bardzo dawna. Niegdysiejsze "+
    "cz^e^sci okaza^lych mur^ow le^z^a teraz bez^ladnie "+
    "porozrzucane po okolicy.\n";
    return str;
}


