/*Las Komanda
  opisy Edrain
  kodowa� Cairell*/

#include <stdproperties.h>
#include <mudtime.h>
#include "dir.h"
inherit LAS_ELFOW_GESTY;

string dlugi_opis();

void
create_las()
{
    set_short("Gdzie^s w lesie.");
    set_long(&dlugi_opis());
    add_exit(LAS_ELFOW_LOKACJE+ "las_30.c","n");
    add_exit(LAS_ELFOW_LOKACJE+ "las_53.c","e");
}
string dlugi_opis()
{
    string str;
    if(jest_dzien() == 1) // DZIEN
    {
        if(CZY_JEST_SNIEG(this_object()))
        {
            str="Oddychaj^ac g^l^eboko czujesz ^swie^zo^s^c i "+
                "charakterystyczny zapach lasu. Rosn^ace wsz^edzie d^eby "+
                "przynosz^a na my^sl stare podania i legendy, a wra^zenie to "+
                "dope^lniaj^a poprzewracane pniaki, a czasem ca^le drzewa. "+
                "Strzeliste sosny ci^agn^ace si^e w g^or^e, dotykaj^ac prawie "+
                "nieba przypominaj^a o niesko^nczono^sci. Nieskalany ^snieg "+
                "pokrywaj^acy ziemi^e sprawia, ^ze ma si^e ochot^e po^lo^zy^c "+
                "i zasn^a^c w^sr^od tej niesamowito^sci sk^apanej w jasno^sci. "+
                "Cisze zak^l^ocaj^a tu j^eki wiatru i g^losy zab^l^akanych "+
                "w^sr^od mrozu ptak^ow.";
        }
        else if(pora_roku() == MT_WIOSNA)
        {
            str="Oddychaj^ac g^l^eboko czujesz ^swie^zo^s^c i "+
                "charakterystyczny zapach lasu. Rosn^ace wsz^edzie d^eby "+
                "przynosz^a na my^sl stare podania i legendy, a wra^zenie to "+
                "dope^lniaj^a poprzewracane pniaki, a czasem ca^le drzewa. "+
                "Strzeliste sosny ci^agn�ce si^e w g^or^e, dotykaj^ac prawie "+
                "nieba przypominaj^a o niesko^nczono^sci. Paprocie i mech "+
                "rosn^acy na ziemi sprawia, ^ze ma sie ochot^e po^lo^zy^c i "+
                "zasn^a^c w^sr^od tej niesamowito^sci sk^apanej w jasno^sci. "+
                "Cisze zak^l^ocaj^a tu j^eki wiatru i ^spiew ptak^ow, dzi^eki "+
                "kt^oremu czu^c ^ze las ^zyje.";
        }
        else if(pora_roku() == MT_LATO)
        {
            str="Oddychaj^ac g^l^eboko czujesz ^swie^zo^s^c i "+
                "charakterystyczny zapach lasu. Rosn^ace wsz^edzie d^eby "+
                "przynosz^a na my^sl stare podania i legendy, a wra^zenie to "+
                "dope^lniaj^a poprzewracane pniaki, a czasem ca^le drzewa. "+
                "Strzeliste sosny ci^agn�ce si^e w g^or^e, dotykaj^ac prawie "+
                "nieba przypominaj^a o niesko^nczono^sci. Paprocie i mech "+
                "rosn^acy na ziemi sprawia, ^ze ma sie ochot^e po^lo^zy^c i "+
                "zasn^a^c w^sr^od tej niesamowito^sci sk^apanej w jasno^sci. "+
                "Cisze zak^l^ocaj^a tu j^eki wiatru i ^spiew ptak^ow, dzi^eki "+
                "kt^oremu czu^c ^ze las ^zyje.";
        }
        else //jesien i zima gdy nie ma sniegu
        {
            str="Oddychaj^ac g^l^eboko czujesz ^swie^zo^s^c i "+
                "charakterystyczny zapach lasu. Rosn^ace wsz^edzie d^eby "+
                "przynosz^a na my^sl stare podania i legendy, a wra^zenie to "+
                "dope^lniaj^a poprzewracane pniaki, a czasem ca^le drzewa. "+
                "Strzeliste sosny ci^agn^ace si^e w g^or^e, dotykaj^ac prawie "+
                "nieba przypominaj^a o niesko^nczono^sci. Mi^ekki dywan "+
                "po^z^o^lk^lych mch^ow i suchych traw  "+
                "pokrywaj^acy ziemi^e sprawia, ^ze ma si^e ochot^e po^lo^zy^c "+
                "i zasn^a^c w^sr^od tej niesamowito^sci sk^apanej w jasno^sci. "+
                "Cisze zak^l^ocaj^a tu j^eki wiatru i g^losy zab^l^akanych "+
                "w^sr^od mrozu ptak^ow.";
        }
    }
    else // NOC
    {
        if(CZY_JEST_SNIEG(this_object()))
        {
            str="Oddychaj^ac g^l^eboko czujesz ^swie^zo^s^c i "+
                "charakterystyczny zapach lasu. Rosn^ace wsz^edzie d^eby "+
                "przynosz^a na my^sl stare podania i legendy, a wra^zenie to "+
                "dope^lniaj^a poprzewracane pniaki, a czasem ca^le drzewa. "+
                "Strzeliste sosny ci^agn^ace si^e w g^or^e, dotykaj^ac prawie "+
                "nieba przypominaj^a o niesko^nczono^sci. Cisze zak^l^ocaj^a "+
                "tu j^eki wiatru i g^losy nocnych, zab^l^akanych w^sr^od "+
                "mrozu ptak^ow.";
        }
        else if(pora_roku() == MT_WIOSNA)
        {
            str="Oddychaj^ac g^l^eboko czujesz ^swie^zo^s^c i "+
                "charakterystyczny zapach lasu. Rosn^ace wsz^edzie d^eby "+
                "przynosz^a na my^sl stare podania i legendy, a wra^zenie to "+
                "dope^lniaj^a poprzewracane pniaki, a czasem ca^le drzewa. "+
                "Strzeliste sosny ci^agn�ce si^e w g^or^e, dotykaj^ac prawie "+
                "nieba przypominaj^a o niesko^nczono^sci. Cisze zak^l^ocaj^a "+
                "tu j^eki wiatru i ^spiew nocnych ptak^ow, dzi^eki kt^oremu "+
                "czu^c ^ze las ^zyje.";
        }
        else if(pora_roku() == MT_LATO)
        {
            str="Oddychaj^ac g^l^eboko czujesz ^swie^zo^s^c i "+
                "charakterystyczny zapach lasu. Rosn^ace wsz^edzie d^eby "+
                "przynosz^a na my^sl stare podania i legendy, a wra^zenie to "+
                "dope^lniaj^a poprzewracane pniaki, a czasem ca^le drzewa. "+
                "Strzeliste sosny ci^agn�ce si^e w g^or^e, dotykaj^ac prawie "+
                "nieba przypominaj^a o niesko^nczono^sci. Cisze zak^l^ocaj^a "+
                "tu j^eki wiatru i ^spiew nocnych ptak^ow, dzi^eki kt^oremu "+
                "czu^c ^ze las ^zyje.";
        }
        else //jesien i zima gdy nie ma sniegu
        {
            str="Oddychaj^ac g^l^eboko czujesz ^swie^zo^s^c i "+
                "charakterystyczny zapach lasu. Rosn^ace wsz^edzie d^eby "+
                "przynosz^a na my^sl stare podania i legendy, a wra^zenie to "+
                "dope^lniaj^a poprzewracane pniaki, a czasem ca^le drzewa. "+
                "Strzeliste sosny ci^agn^ace si^e w g^or^e, dotykaj^ac prawie "+
                "nieba przypominaj^a o niesko^nczono^sci. Cisze zak^l^ocaj^a "+
                "tu j^eki wiatru i g^losy nocnych, zab^l^akanych w^sr^od "+
                "mrozu ptak^ow.";
        }
    }
    str += "\n";
    return str;
}
