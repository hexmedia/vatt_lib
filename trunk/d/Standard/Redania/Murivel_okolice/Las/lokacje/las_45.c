#include <stdproperties.h>
#include <mudtime.h>
#include "dir.h"
inherit LAS_ELFOW_GESTY;

string dlugi_opis();

void
create_las()
{
    set_short("W^sr^od drzew g^estego lasu");
    set_long(&dlugi_opis());
    add_exit(LAS_ELFOW_LOKACJE+ "las_43.c","sw");
    add_exit(LAS_ELFOW_LOKACJE+ "las_44.c","s");
    
    if(!CZY_JEST_SNIEG(this_object()))
    {
        add_item("mech","Po�acie soczysto zielonych �ody�ek wznosz? si� "+
        "nieznacznie nad ziemi�. Mi�dzy drobnymi listkami gromadz? "+
        "du�e ilo?ci wody, przez co ca�o?� jest wyj?tkowo wilgotna. "+
        "Ziemia zas�ana tymi ro?linkami wygl?da jakby by�a pokryta "+
        "naturalnym dywanem.\n");
    }
}
string dlugi_opis()
{
    string str;
        str="Tajemnicza mowa lasu sprawia, ^ze przez chwil^e masz wra^zenie, "+
        "jakby czas stan^a^l w miejscu, a leciwe d^eby i wi^azy dodatkowo pot"+
        "^eguj^a uczucie bezradno^sci i przywodz^a na my^sl czasy, kt^ore ju^"+
        "z dawno min^e^ly. Sosny, jesiony i klony dope^lniaj^a wizerunku niez"+
        "wyk^lej ca^lo^sci, lekko przerzedzaj^ac g^estwin^e tworzon^a przez z"+
        "walone pniaki i krzewy, pn^acza oplataj^ace pnie, krzaki rosn^ace w "+
        "podszyciu pozwalaj^a dozna^c uczucia spokoju i odosobnienia.";
    str += "\n";
    return str;
}

