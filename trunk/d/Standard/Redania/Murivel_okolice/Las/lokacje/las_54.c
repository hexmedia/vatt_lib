/*Las Komanda
  Cairell*/

#include <stdproperties.h>
#include <mudtime.h>
#include "dir.h"
inherit LAS_ELFOW_GESTY;

string dlugi_opis();

void
create_las()
{
    set_short("Po^srod drzew.");
    set_long(&dlugi_opis());
    add_exit(LAS_ELFOW_LOKACJE+ "las_53.c","w");
    add_exit(LAS_ELFOW_LOKACJE+ "las_52.c","nw");
    add_exit(LAS_ELFOW_LOKACJE+ "las_39.c","e");
}
string dlugi_opis()
{
    string str = "Okolica sk^apana jest w prze^swituj^acych mi^edzy "+
        "konarami drzew, ";
    if(jest_dzien() == 1) // DZIEN
    {
        str += "nienatr^etnych promieniach s^lo^nca. ";
    }
    else // NOC
    {
        str += "bladych promieniach ksi^e^zyca. ";
    }
    
    str += "Tworz^a one d^lugie smugi cieni, najg^l^ebsze w w^askich "+
        "korytach sezonowych strumyk^ow niby blizny znacz^acych stok lekkiej "+
        "opadaj^acej na wsch^od pochy^lo^sci. W kilku miejscach rozpinaj^a "+
        "si^e nad nimi kostropate mostki podmytych korzeni, lub przegradzaj^a "+
        "wyp^lukane kamienie, im ni^zej tym mniej wij^ac si^e, a cz^e^sciej "+
        "^l^acz^ac si^e, by w ko^ncu znikn^a^c z oczu gdzie^s za drzewami.";
    
    if(CZY_JEST_SNIEG(this_object()))
    {
        str += "W dole wida^c tak^ze kilka wi^ekszych zasp nagonionego "+
            "wiatrem ^sniegu, kt^ory otula teraz le^sne runo.";
    }
    else if(pora_roku() == MT_ZIMA || pora_roku() == MT_JESIEN)
    {
         str += "Suche runo le^sne wyczekuje wiosny, o^smielaj^c si^e jedynie "+
             "pomacha^c na wietrze suchymi ^lodygami paproci i pokrzyw.";
    }
    else
    {
         str += "Zielone k^epy paproci wystrzeliwuj^a pomi^edzy nimi, "+
             "wsz^edzie tam gdzie mog^a odnale^s^c cie^n i zacisze.";
    }
    
    str += "\n";
    return str;
}
