/* Las komanda
   Autor: Avard 
   Opis : Edrain */

#include <stdproperties.h>
#include <macros.h>
#include <mudtime.h>
#include "dir.h"
inherit LAS_ELFOW_GESTY;

string dlugi_opis();

string opis_fiolkow();
string opis_zawilcow();
string opis_dzwonkow();
string opis_wrzosow();
string opis_paproci();

int wez(string str);

void
create_las()
{
    set_short("Bukowa puszcza");
    set_long(&dlugi_opis());
    add_exit(LAS_ELFOW_LOKACJE+ "las_73.c","nw");
    add_exit(LAS_ELFOW_LOKACJE+ "las_74.c","n");
    add_exit(LAS_ELFOW_LOKACJE+ "las_75.c","ne");
    add_exit(LAS_ELFOW_LOKACJE+ "las_72.c","e");
    add_exit(LAS_ELFOW_LOKACJE+ "las_66.c","sw");
    
    if(!CZY_JEST_SNIEG(this_object()))
    {
        add_item("mech","Po�acie soczysto zielonych �ody�ek wznosz? si� "+
        "nieznacznie nad ziemi�. Mi�dzy drobnymi listkami gromadz? "+
        "du�e ilo?ci wody, przez co ca�o?� jest wyj?tkowo wilgotna. "+
        "Ziemia zas�ana tymi ro?linkami wygl?da jakby by�a pokryta "+
        "naturalnym dywanem.\n");
    }
    add_item(({"zawilca","zawilce"}), &opis_zawilcow());
    add_item(({"dzwonek","dzwonki"}), &opis_dzwonkow());
    add_item(({"wrzos","wrzosy"}), &opis_wrzosow());
    add_item(({"fio^lka","fio^lki"}), &opis_fiolkow());
    add_item(({"papro�"}), &opis_paproci());
}

public void
init()
{
    ::init();
    add_action(wez,"we�");
    add_action(wez,"zbieraj");
    add_action(wez,"zbierz");
}

string dlugi_opis()
{
    string str = "";
    
    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="Spok�j panuj�cy tutaj napawa zar�wno l�kiem jak i ciekawo�ci�. "+
        "Wysokie proste drzewa wygl�daj� jak stra�nicy spokoju natury. "+
        "Pozbawione ga��zi pnie niepokoj� ka�dego prostot� i g�adko�ci�. "+
        "Ca�y b�r wydaje si� wymar�y. Dochodz�ce z wysoka d�wi�ki �wiadcz� o "+
        "tym, ze to w�a�nie w g�rnych partiach puszczy skupia si� �ycie "+
        "lasu. Ziemia g�sto pokryta jest czapami bia�ego �niegu.";
    }
    else if(pora_roku() == MT_WIOSNA || pora_roku() == MT_LATO)
    {
        str+="Spok�j panuj�cy tutaj napawa zar�wno l�kiem jak i ciekawo�ci�. "+
        "Wysokie proste drzewa wygl�daj� jak stra�nicy spokoju natury. "+
        "Pozbawione ga��zi pnie niepokoj� ka�dego prostot� i g�adko�ci�. "+
        "Ca�y b�r wydaje si� wymar�y. Dochodz�ce z wysoka d�wi�ki �wiadcz� "+
        "o tym, ze to w�a�nie w g�rnych partiach puszczy skupia si� �ycie "+
        "lasu. Ziemia g�sto poro�ni�ta jest mokrym mchem i porozrzucanymi "+
        "niemal wsz�dzie bukowymi orzeszkami oraz miniaturowymi paprociami "+
        "i k�pkami kwiat�w.";
    }
    else
    {
        str+="pok�j panuj�cy tutaj napawa zar�wno l�kiem jak i ciekawo�ci�. "+
        "Wysokie proste drzewa wygl�daj� jak stra�nicy spokoju natury. "+
        "Pozbawione ga��zi pnie niepokoj� ka�dego prostot� i g�adko�ci�. "+
        "Ca�y b�r wydaje si� wymar�y. Dochodz�ce z wysoka d�wi�ki �wiadcz� "+
        "o tym, ze to w�a�nie w g�rnych partiach puszczy skupia si� �ycie "+
        "lasu. Ziemia g�sto poro�ni�ta jest suchym mchem i porozrzucanymi "+
        "niemal wsz�dzie bukowymi orzeszkami oraz miniaturowymi paprociami "+
        "i k�pkami wrzos�w.";
    }
    
    str += "\n";
    return str;
}

string opis_zawilcow()
{
    string str;
    if (pora_roku() == MT_WIOSNA)
    {
    str += "Zawilec gajowy jest pojedynczym kwiatem o d^lugiej "+
    "delikatnej ^lody^zce wyrastaj^acej z k^l^acza. "+
    "Poszarpane listki, zazwyczaj tylko trzy "+
    "wyst^epuj^ace w ok^o^lkach i wygl^adaj^a jak szpony "+
    "jakiej^s zjawy. Aktualnie ka^zda ^lody^zka zwie^nczona "+
    "jest prze^slicznym bia^lym kwiatkiem, sk^ladaj^acym "+
    "si^e z pi^eciu lub siedmiu p^latk^ow. Soki tej "+
    "ro^sliny s^a silnie truj^ace i powoduj^a zapalenie "+
    "nerek i uk^ladu pokarmowego, wywo^luj^ac silne "+
    "wymioty i biegunk^e.\n"; 
    return str;
    }
    else
    {
    return 0;
    }
}

string opis_dzwonkow()
{
    string str;
    if (pora_roku() == MT_LATO)
    {
    str = "Wiotkie ^lody^zki wznosz^a si^e kilkadziesi^at "+
    "centymetr^ow nad ziemi^e, ich ko^nce zwieszaj^a "+
    "si^e w stron^e pod^lo^za obci^a^zone okaza^lymi "+
    "kwiatami. Fioletowe, zros^le p^latki tworz^a kielich "+
    "z jasno^z^o^ltym ^srodkiem. Odurzaj^acy zapach "+
    "dzwonk^ow le^snych wabi owady.\n";
    return str;
    }
    else
    {
    return 0;
    }
}

string opis_fiolkow()
{
    string str;
    if (pora_roku() == MT_WIOSNA)
    {
    str = "P^lo^z^aca si^e po ziemi bylina jest teraz g^esto "+
    "upstrzona pi^eknie pachn^acymi fioletowymi "+
    "kwiatami. Delikatne kwiaty wyniesione s^a nad "+
    "ziemie na wysokich szypu^lkach, ka^zdy z nich "+
    "sk^lada si^e z pi^eciu p^latk^ow dw^och "+
    "skierowanych w g^or^e i trzech w d^o^l. "+
    "Zielone listki, kt^ore dodatkowo eksponuj^a "+
    "pi^ekno koloru fio^lk^ow, maj^a kszta^lt sercowaty "+
    "i s^a lekko karbowane od g^ory.\n";
    return str;
    }
    else if (pora_roku() == MT_LATO)
    {
    str = "Na p^lo^z^acej si^e po ziemi bylinie nie wida^c "+
    "ju^z ^slad^ow niedawno pi^eknie kwitn^acych, "+
    "drobnych p^latk^ow fio^lk^ow. Z wonnej ro^sliny "+
    "pozosta^ly ^lody^zki i ciemnozielone, sercowate "+
    "li^scie, kt^ore t^lumi^a wszelkie odg^losy "+
    "zwierz^at i podr^o^znych.\n";
    return str;
    }
    else return 0;
}

string opis_wrzosow()
{
    string str;
    if (pora_roku() == MT_JESIEN)
    {
    str = "Ta zimozielona dwupienna krzewinka o listkach "+
    "rozes^lanych po ziemi, p^lo^zy si^e g^esto "+
    "wsz^edzie dooko^la, tworz^ac wyciszaj^acy dla "+
    "krok^ow ciemnozielony dywan. Jej drobne, "+
    "igie^lkowate li^scie u^lo^zone s^a naprzeciw siebie "+
    "i sprawiaj^a mylne wra^zenie bardzo ubogich, "+
    "jednak s^a one tylko dodatkiem do pi^eknych "+
    "r^o^zowo- fioletowych kwiat^ow w pe^lni "+
    "rozkwitaj^acych wczesn^a jesieni^a, r^ownie "+
    "drobnych, a jednak zachwycaj^acych swym pi^eknem, "+
    "kt^ore maluje ^swiat w czasie gdy prawie wszelkie "+
    "inne ozdoby przyrody ju^z przekwit^ly.\n";
    return str;
    }
    else
    {
    return 0;
    }
}

string opis_paproci()
{
    string str;
    if (pora_roku() == MT_WIOSNA || pora_roku() == MT_LATO)
    {
        str = "Niewysoka, lecz posiadaj?ca du�e li?cie papro� ugina si� do "+
        "ziemi pod ci�arem zal?�k�w znajduj?cych si� pod listkami. Jej "+
        "soczysty, zielony kolor o�ywia miejsce. Mi�dzy jej listkami wiele "+
        "ma�ych stworzonek znalaz�o schronienie.\n";
        return str;
    }
    else if (pora_roku() == MT_JESIEN)
    {
        str = "Niewysoka, lecz posiadaj?ca du�e li?cie papro� ugina si� do "+
        "ziemi pod ci�arem zal?�k�w znajduj?cych si� pod listkami. Jej "+
        "przebarwiaj?ce si� na br?zowo li?cie, powoli usychaj?ce, "+
        "przypominaj? o nadchodz?cej zimie. Mi�dzy jej listkami wiele "+
        "ma�ych stworzonek znalaz�o schronienie.\n";
        return str;
    }
    else return 0;
}

int wez(string str)
{
    if(query_verb() ~= "we�")
        notify_fail("We� co?\n");
    if(query_verb() ~= "zbieraj")
        notify_fail("Zbieraj co?\n");
    if(query_verb() ~= "zbierz")
        notify_fail("Zbierz co?\n");
        
    if (!str) return 0;
    if (!strlen(str)) return 0;
    
    if(pora_roku() == MT_ZIMA)
        return 0;
    
    if(query_verb() ~= "we�")
    {
        if(!parse_command(str,environment(TP),"'orzech' / 'orzechy'"))
            return 0;
    }
    else 
    {
        if(!parse_command(str,environment(TP),"'orzechy'"))
            return 0;
    }
    write("Zaczynasz zbiera� orzechy.\n");
    saybb(QCIMIE(TP,PL_MIA)+" zaczyna zbiera� orzechy.\n");
    clone_object(LAS_ELFOW_PRZEDMIOTY+"paraliz_orzechy.c")->move(TP);
    return 1;
}