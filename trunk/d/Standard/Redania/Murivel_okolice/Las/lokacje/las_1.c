/**
 * Las Komanda
 *
 * autor Cairell
 * opisy Edrain & Aladriel
 * data  styczen 2009
 */

#include <stdproperties.h>
#include <mudtime.h>
#include "dir.h"
inherit LAS_ELFOW_RZEKA;

string dlugi_opis();

void
create_las()
{
    set_short("Las");
    set_long(&dlugi_opis());
    add_exit(TRAKT_LOKACJE+ "trakt_19.c","nw");
    add_exit(TRAKT_LOKACJE+ "trakt_20.c","n");
    add_exit(TRAKT_LOKACJE+ "trakt_21.c","ne");
    add_exit(LAS_ELFOW_LOKACJE+ "las_2.c","e");
    add_exit(LAS_ELFOW_LOKACJE+ "las_3.c","se");
}

string dlugi_opis()
{
    string str = ""; 
    
    str+="Na pierwszy rzut oka to miejsce wygl^ada "+
        "jak zwyk^ly kawa^lek lasu, jednak po chwili "+
        "mo^zna dostrzec, ^ze nie rosn^a tu zwyczajne "+
        "drzewa, ale drzewka owocowe. Najwidoczniej "+
        "kiedy^s musia^l znajdowa� si^e tu sad. "+
        "Pokrzywionych, nieprzycinanych konarach "+
        "mo^zna stwierdzi^c, ^ze zosta^l opuszczony "+
        "przez swego opiekuna, a lata ^swietno ci ma "+
        "ju^z zdecydowanie za sob^a. Drzewka, "+
        "nieprzycinane i niepiel^egnowane, ros^ly tu "+
        "przez wiele lat samopas, ";

    if (jest_dzien() == 1)
    {
        if(pora_roku() == MT_WIOSNA)
        {
            str += "jednak nie przeszkadza im to w "+
                "bujnym kwitnieniu, a p^o^xniej wydawaniu "+
                "smacznych owoc^ow. W powietrzu unosi "+
                "si^e pi^ekny zapach bia^lego kwiecia "+
                "czere^sni, kt^ore oblepia g^esto "+
                "drzewka, tworz^ac pi^ekny ba^sniowy "+
                "krajobraz. Dooko^la kwiat^ow unosz^a "+
                "si^e pracowite pszczo^ly zbieraj^ace "+
                "nektar z kielich^ow, a kolorowe motyle "+
                "fruwaj^ace dooko^la sk^laniaj^a, "+
                "aby zatrzymac si^e tu na d^lu^zsz^a "+
                "chwil^e.";
        }
        if(pora_roku() == MT_LATO)
        {
            str += "jednak nie przeszkadza im to w "+
                "wydawaniu pi^eknych i zdrowych "+
                "owoc^ow, kt^ore oblepiaj^a ga^l^ezie. "+
                "W powietrzu unosi si^e kusz^acy "+
                "zapach dojrza^lych czere^sni.";
        }
        if(pora_roku() == MT_JESIEN)
        {
            str += "a g^este li^scie powoli zmieniaj^a "+
                "barw^e z zielonej na z^lot^a. "+
                "Gdzieniegdzie w ga^l^eziach "+
                "dostrzegasz resztki czerwcowych owoc^ow "+
                "czere^sni, teraz ju^z pomarszonych i "+
                "nog^acych s^lu^zy^c za po^zywienie "+
                "tylko ptakom.";
        }
        if(pora_roku() == MT_ZIMA)
        {
            if(CZY_JEST_SNIEG(this_object()))
            {
                str += "przez co wygl^adaj^a tajemniczo "+
                    "i upiornie zarazem, wyci^agaj^ac "+
                    "d^lugie, g^esto upstrzone bia^lym "+
                    "puchem, konary do nieba.";
            }
            else
            {
                str += "przez co wygl^adaj^a tajemniczo "+
                    "i upiornie zarazem, wyci^agaj^ac "+
                    "d^lugie konary do nieba.";
            }
        }
    }
    else //noc
    {
        if(pora_roku() == MT_WIOSNA)
        {
            str += "jednak nie przeszkadza im to w "+
                "bujnym kwitnieniu, p^o^xniej "+
                "wydawaniu smacznych owoc^ow. W "+
                "powietrzu unosi si^e pi^ekny "+
                "zapach kwiat^ow czere^sni.";
        }
        if(pora_roku() == MT_LATO)
        {
            str += "lecz mimo to dalej wydaj^a owoce, "+
                "kt^orych kusz^acy zapach unosi si^e "+
                "w powietrzu.";
        }
        if(pora_roku() == MT_JESIEN)
        {
            str += "a wraz z nadej^sciem jesieni "+
                "g^este li^scie powoli zaczynaj^a "+
                "wiotcze^c. Gdzieniegdzie pomi^edzy "+
                "ga^l^eziami dostrzegasz resztki "+
                "owoc^ow czere^sni lekko pob^lyskuj^ace "+
                "w ^swietle ksi^e^zyca.";
        }
        if(pora_roku() == MT_ZIMA)
        {
            if(CZY_JEST_SNIEG(this_object()))
            {
                str += "przez co wygl^adaj^a tajemniczo "+
                    "i upiornie zarazem, wyci^agaj^ac "+
                    "d^lugie, g^esto upstrzone bia^lym "+
                    "puchem, konary do nieba.";
            }
            else
            {
                str += "przez co wygl^adaj^a tajemniczo "+
                    "i upiornie zarazem, wyci^agaj^ac "+
                    "d^lugie konary do nieba.";
            }
        }
    }

str+="\n";
return str;
}

public string
exits_description() 
{
    return "Na p^o^lnocnym zachodzie, p^o^lnocy i p^o^lnocnym "+
        "wschodzie znajduje si^e takt, za^s na wschodzie i "+
        "po^ludniowym wschodzie rozci^aga si^e dalsza cz^e^s^c lasu.\n";
}