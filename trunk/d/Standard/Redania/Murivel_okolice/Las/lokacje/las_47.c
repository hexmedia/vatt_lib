
#include <stdproperties.h>
#include <mudtime.h>
#include "dir.h"
inherit LAS_ELFOW_GESTY;

string dlugi_opis();

void
create_las()
{
    set_short("G^esty las");
    set_long(&dlugi_opis());
    add_exit(LAS_ELFOW_LOKACJE+ "las_46.c","w");
    add_exit(LAS_ELFOW_LOKACJE+ "las_27.c","se");
    add_exit(LAS_ELFOW_LOKACJE+ "las_49.c","ne");
    
    if(!CZY_JEST_SNIEG(this_object()))
    {
        add_item("mech","Po�acie soczysto zielonych �ody�ek wznosz? si� "+
        "nieznacznie nad ziemi�. Mi�dzy drobnymi listkami gromadz? "+
        "du�e ilo?ci wody, przez co ca�o?� jest wyj?tkowo wilgotna. "+
        "Ziemia zas�ana tymi ro?linkami wygl?da jakby by�a pokryta "+
        "naturalnym dywanem.\n");
    }
}
string dlugi_opis()
{
    string str;
    if(jest_dzien() == 1) // DZIEN
    {
    if(CZY_JEST_SNIEG(this_object()))
    {
        str="Pierwsze co dostrzegasz po rozejrzeniu si^e dooko^la to powywrac"+
        "ane pnie poro^sni^ete mchem i bluszczem oraz g^este krzewy cisu i le"+
        "szczyny zagradzaj^ace dost^ep do najbardziej dziewiczych cz^e^sci bo"+
        "ru. D^eby i wi^azy tworz^a charakterystyczny krajobraz lasu mieszane"+
        "go przesi^akni^ety zapachem li^sci i igie^l, kt^ory wsp^o^lgra z cha"+
        "rakterystycznym zapachem zimy. Ca^ly las zdaje si^e by^c otoczony ja"+
        "sn^a aur^a ^swiat^la, kt^ora sprawia ^ze ^snieg, kt^ory ha^ldami zal"+
        "ega na tym terenie skrzy si^e milionami ma^lych iskierek ^swiat^la -"+
        "w dzie^n t^eczowymi, w nocy - blado-niebiesko-fioletowymi. Przebywaj"+
        "^ac tu masz wra^zenie, ^ze wszed^le^s do innego ^swiata - ^swiata sp"+
        "okoju i harmonii.";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str="Pierwszym co dostrzegasz po rozejrzeniu si^e dooko^la s^a powywr"+
        "acane pnie drzew, kt^ore zd^a^zy^ly ju^z obrosn^a^c mchem i bluszcze"+
        "m, oraz g^este krzewy cisu i leszczyny, zagradzaj^ace dost^ep do naj"+
        "bardziej dziewiczych cz^e^sci boru. D^eby i wi^azy tworz^a charakter"+
        "ystyczny krajobraz lasu mieszanego, przesi^akni^ety zapachem li^sci "+
        "i igie^l. Wszystko zdaje si^e by^c otoczone jasn^a aur^a ^swiat^la, "+
        "kt^ora sprawia ^ze rosn^ace w runie paprocie, mchy i kwiaty le^sne w"+
        "ystawiaj^a swe li^scie na promienie jasno^sci, spragnione ^swiat^la."+
        " Przebywaj^ac tu masz wra^zenie, ^ze wszed^le^s do innego ^swiata - "+
        "^swiata spokoju i harmonii.";
    }
    else if(pora_roku() == MT_LATO)
    {
        str="Pierwszym co dostrzegasz po rozejrzeniu si^e dooko^la s^a powywr"+
        "acane pnie drzew, kt^ore zd^a^zy^ly ju^z obrosn^a^c mchem i bluszcze"+
        "m, oraz g^este krzewy cisu i leszczyny, zagradzaj^ace dost^ep do naj"+
        "bardziej dziewiczych cz^e^sci boru. D^eby i wi^azy tworz^a charakter"+
        "ystyczny krajobraz lasu mieszanego, przesi^akni^ety zapachem li^sci "+
        "i igie^l. Wszystko zdaje si^e by^c otoczone jasn^a aur^a ^swiat^la, "+
        "kt^ora sprawia ^ze rosn^ace w runie paprocie, mchy i kwiaty le^sne w"+
        "ystawiaj^a swe li^scie na promienie jasno^sci, spragnione ^swiat^la."+
        " Przebywaj^ac tu masz wra^zenie, ^ze wszed^le^s do innego ^swiata - "+
        "^swiata spokoju i harmonii.";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str="Pierwsze co dostrzegasz po rozejrzeniu si^e dooko^la to powywrac"+
        "ane pnie poro^sni^ete mchem i bluszczem oraz g^este krzewy cisu i le"+
        "szczyny zagradzaj^ace dost^ep do najbardziej dziewiczych cz^e^sci bo"+
        "ru. Wielobarwne obecnie d^eby i wi^azy tworz^a charakterystyczny kra"+
        "jobraz lasu mieszanego przesi^akni^ety zapachem li^sci i igie^l. Ca^"+
        "ly las zdaje si^e by^c otoczony jasn^a aur^a ^swiat^la, kt^ora spraw"+
        "ia, ^ze rosn^ace w runie paprocie, mchy i kwiaty le^sne wystawiaj^a "+
        "li^scie na promienie jasno^sci, spragnione ^swiat^la. Przebywaj^ac t"+
        "u masz wra^zenie, ^ze wszed^le^s do innego ^swiata- ^swiata spokoju "+
        "i harmonii.";
    }
    }
    if(jest_dzien() == 0) // NOC
    {
    if(CZY_JEST_SNIEG(this_object()))
    {
        str="Pierwszym co dostrzegasz po rozejrzeniu si^e dooko^la s^a powywr"+
        "acane pnie jakich^s drzew oraz trudne do zidentyfikowania w ciemno^s"+
        "ciach, g^este krzewy, zagradzaj^ace dost^ep do najbardziej dziewiczy"+
        "ch cz^e^sci boru. W jasnym ^swietle ksi^e^zyca wszystko nabiera taje"+
        "mniczo^sci, a pozbawione li^sci ga^l^ezie drzew, zdaj^a si^e przypom"+
        "ina^c szponiaste d^lonie jakich^s legendarnych stworze^n, broni^acyc"+
        "h tajemnic lasu. Wszystko to przesi^akni^ete jest niesamowit^a miesz"+
        "anin^a zapachu igie^l i li^sci, wzbpgaconego charakterystycznym zapa"+
        "chem zimy. Ca^ly las zdaje si^e by^c otoczony jasn^a aur^a ^swiat^la"+
        ", kt^ora sprawia ^ze ^snieg, kt^ory ha^ldami zalega na tym terenie s"+
        "krzy si^e milionami ma^lych iskierek ^swiat^la - w dzie^n t^eczowymi"+
        ", w nocy - blado-niebiesko-fioletowymi. Przebywaj^ac tu masz wra^zen"+
        "ie, ^ze wszed^le^s do innego ^swiata - ^swiata spokoju i harmonii.";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str="Pierwszym co dostrzegasz po rozejrzeniu si^e dooko^la s^a powywr"+
        "acane pnie jakich^s drzew oraz trudne do zidentyfikowania w ciemno^s"+
        "ciach, g^este krzewy, zagradzaj^ace dost^ep do najbardziej dziewiczy"+
        "ch cz^e^sci lasu. Wszystko zdaje si^e by^c otoczone jasn^a aur^a ksi"+
        "^e^zycowego ^swiat^la i przesi^akni^ete zapachem igie^l i li^sci. Pr"+
        "zebywaj^ac tu masz wra^zenie, ^ze wszed^le^s do innego ^swiata - ^sw"+
        "iata spokoju i harmonii.";
    }
    else if(pora_roku() == MT_LATO)
    {
        str="Pierwszym co dostrzegasz po rozejrzeniu si^e dooko^la s^a powywr"+
        "acane pnie jakich^s drzew oraz trudne do zidentyfikowania w ciemno^s"+
        "ciach, g^este krzewy, zagradzaj^ace dost^ep do najbardziej dziewiczy"+
        "ch cz^e^sci lasu. Wszystko zdaje si^e by^c otoczone jasn^a aur^a ksi"+
        "^e^zycowego ^swiat^la i przesi^akni^ete zapachem igie^l i li^sci. Pr"+
        "zebywaj^ac tu masz wra^zenie, ^ze wszed^le^s do innego ^swiata - ^sw"+
        "iata spokoju i harmonii.";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str="Pierwszym co dostrzegasz po rozejrzeniu si^e dooko^la s^a powywr"+
        "acane pnie jakich^s drzew oraz trudne do zidentyfikowania w ciemno^s"+
        "ciach, g^este krzewy, zagradzaj^ace dost^ep do najbardziej dziewiczy"+
        "ch cz^e^sci lasu. W jasnym ^swietle ksi^e^zyca wszystko nabiera taje"+
        "mniczo^sci, a ga^l^ezie drzew, cz^e^sciowo ju^z pozbawione li^sci, z"+
        "daj^a si^e przypomina^c szponiaste d^lonie jakich^s legendarnych stw"+
        "orze^n, broni^acych tajemnic lasu. Wszystko to przesi^akni^ete jest "+
        "niesamowit^a mieszanin^a zapachu igie^l i li^sci. Przebywaj^ac tu ma"+
        "sz wra^zenie, ^ze wszed^le^s do innego ^swiata - ^swiata spokoju i h"+
        "armonii.";
    }
    }
    str += "\n";
    return str;
}
