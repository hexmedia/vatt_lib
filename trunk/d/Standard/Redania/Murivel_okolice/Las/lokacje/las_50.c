/*Las Komanda
  opisy Edrain
  kodowa� Cairell*/

#include <stdproperties.h>
#include <mudtime.h>
#include "dir.h"
inherit LAS_ELFOW_GESTY;

string dlugi_opis();

void
create_las()
{
    set_short("W^sr^od drzew.");
    set_long(&dlugi_opis());
    add_exit(LAS_ELFOW_LOKACJE+ "las_27.c","w");
    add_exit(LAS_ELFOW_LOKACJE+ "las_48.c","nw");
    add_exit(LAS_ELFOW_LOKACJE+ "las_61.c","se");
}
string dlugi_opis()
{
    string str;
    if(jest_dzien() == 1) // DZIEN
    {
        if(CZY_JEST_SNIEG(this_object()))
        {
            str="Spokojny szum wiatru i ^spiew ptak^ow nape^lnia harmoni^a i "+
                "wycisza, a ty czujesz jak zmys^ly odpoczywaj^a od "+
                "codziennych trosk rozpraszane jedynie przez pi^ekne ciche "+
                "d^xwi^eki lasu i zapach ^zywicy. Roz^lo^zyste d^eby, smuk^le "+
                "sosny czy wiekowe wi^azy pozwalaj^a przypuszcza^c ^ze "+
                "cofn^a�e^s si^e w czasie, przes^lniaj^ace runo ha^ldy "+
                "^sniegu wyciszaj^ kroki pozwalaj^ac kontemplowa^c swoist^a "+
                "cisz^e boru. G^este krzewy i powywracane pnie broni^a "+
                "dost^epu do miejsc nieskalanych ludzk^a stop^a, dzi^eki "+
                "czemu las wydaje si^e by� jeszcze bardziej tajemniczy.";
        }
        else if(pora_roku() == MT_WIOSNA)
        {
            str="Spokojny szum wiatru i ^spiew ptak^ow nape^lnia harmoni^a "+
                "i wycisza, a ty czujesz jak zmys^ly odpoczywaj^a od "+
                "codziennych trosk, jedynie niekiedy rozpraszane przez "+
                "pi^ekne, ciche d^xwi^eki lasu i zapach ^zywicy. Roz^lo^zyste "+
                "d^eby, smuk^le sosny czy wiekowe wi^azy pozwalaj^a "+
                "przypuszcza^c, ^ze cofn^a^le^s si^e w czasie. Zape^lniaj^ace "+
                "runo paprocie i mchy wyciszaj^a kroki pozwalaj^ac "+
                "kontemplowa^c swoist^a cisz^e boru. G^este krzewy i "+
                "powywracane pnie broni^a dost^epu do miejsc nieskalanych "+
                "ludzk� stop^a, dzi^eki czemu las wydaje si^e by^c jeszcze "+
                "bardziej tajemniczy.";
        }
        else if(pora_roku() == MT_LATO)
        {
            str="Spokojny szum wiatru i ^spiew ptak^ow nape^lnia harmoni^a "+
                "i wycisza, a ty czujesz jak zmys^ly odpoczywaj^a od "+
                "codziennych trosk, jedynie niekiedy rozpraszane przez "+
                "pi^ekne, ciche d^xwi^eki lasu i zapach ^zywicy. Roz^lo^zyste "+
                "d^eby, smuk^le sosny czy wiekowe wi^azy pozwalaj^a "+
                "przypuszcza^c, ^ze cofn^a^le^s si^e w czasie. Zape^lniaj^ace "+
                "runo paprocie i mchy wyciszaj^a kroki pozwalaj^ac "+
                "kontemplowa^c swoist^a cisz^e boru. G^este krzewy i "+
                "powywracane pnie broni^a dost^epu do miejsc nieskalanych "+
                "ludzk� stop^a, dzi^eki czemu las wydaje si^e by^c jeszcze "+
                "bardziej tajemniczy.";
        }
        else //jesien i zima gdy nie ma sniegu
        {
            str="Spokojny szum wiatru i ^spiew ptak^ow nape^lnia harmoni^a i "+
                "wycisza, a ty czujesz jak zmys^ly odpoczywaj^a od "+
                "codziennych trosk rozpraszane jedynie przez pi^ekne ciche "+
                "d^xwi^eki lasu i zapach ^zywicy. Przesypiaj^ace zim^e "+
                "roz^lo^zyste d^eby, smuk^le sosny czy wiekowe wi^azy "+
                "pozwalaj^a przypuszcza^c ^ze "+
                "cofn^a�e^s si^e w czasie, suche mchy i po��k�e trawy "+
                "wyciszaj^ kroki pozwalaj^ac kontemplowa^c swoist^a "+
                "cisz^e boru. G^este krzewy i powywracane pnie broni^a "+
                "dost^epu do miejsc nieskalanych ludzk^a stop^a, dzi^eki "+
                "czemu las wydaje si^e by� jeszcze bardziej tajemniczy.";
        }
    }
    else // NOC
    {
        if(CZY_JEST_SNIEG(this_object()))
        {
            str="Skryta w mroku i nocnej ciszy okolica wydaje si^e od zawsze "+
                "poro^sni^eta g^estymi, "+
                "pi^eknymi drzewami. D^eby, kt^ore tajemniczo g^oruj^a nad "+
                "innymi gatunkami przywodz^a na my^sl ba^sniowe lasy, o "+
                "kt^orych zazwyczaj czyta si^e tylko w ksi^a^zkach. Wi^azy "+
                "stanowi^a niesamowite dope^lnienie tego wra^zenia, ze swoimi "+
                "sp^ekanymi, ^luszcz�cymi si^e pniami i powyginanymi konarami. "+
                "Na ziemi pokrytej obecnie zaspami ^snie^znymi igra ^swiat^lo, "+
                "kt^ore ta^ncz^ac na p^latkach bia^lego puchu nadaje miejscu "+
                "lekko^sci. Harmonia panuj^aca tu ca^lkowicie pozwala "+
                "zapomnie^c o zgie^lku cywilizacji.";
        }
        else if(pora_roku() == MT_WIOSNA)
        {
            str="Skryta w mroku i nocnej ciszy okolica wydaje si^e od zawsze "+
                "poro^sni^eta g^estymi, "+
                "pi^eknymi drzewami. D^eby kt�re tajemniczo g^oruj^a nad "+
                "innymi gatunkami przywodz^a na my^sl ba^sniowe lasy, o "+
                "kt^orych zazwyczaj czyta si^e tylko w ksi^a^zkach. Wi^azy, "+
                "ze swoimi sp�kanymi, ^luszcz^acymi si^e pniami i powyginanymi "+
                "konarami, stanowi^a niesamowite dope^lnienie tego wra^zenia. "+
                "Na ziemi zauwa^zy^c mo�na paprocie, mchy, wrzosy oraz inne "+
                "kwiaty le^sne, kt^ore nadaj^a stawianym w tym miejscu krokom "+
                "lekko^sci. Harmonia panuj^aca tu ca^lkowicie pozwala "+
                "zapomnie^c o zgie^lku cywilizacji.";
        }
        else if(pora_roku() == MT_LATO)
        {
            str="Skryta w mroku i nocnej ciszy okolica wydaje si^e od zawsze "+
                "poro^sni^eta g^estymi, "+
                "pi^eknymi drzewami. D^eby kt�re tajemniczo g^oruj^a nad "+
                "innymi gatunkami przywodz^a na my^sl ba^sniowe lasy, o "+
                "kt^orych zazwyczaj czyta si^e tylko w ksi^a^zkach. Wi^azy, "+
                "ze swoimi sp�kanymi, ^luszcz^acymi si^e pniami i powyginanymi "+
                "konarami, stanowi^a niesamowite dope^lnienie tego wra^zenia. "+
                "Na ziemi zauwa^zy^c mo�na paprocie, mchy, wrzosy oraz inne "+
                "kwiaty le^sne, kt^ore nadaj^a stawianym w tym miejscu krokom "+
                "lekko^sci. Harmonia panuj^aca tu ca^lkowicie pozwala "+
                "zapomnie^c o zgie^lku cywilizacji.";
        }
        else //jesien i zima gdy nie ma sniegu
        {
            str="Skryta w mroku i nocnej ciszy okolica wydaje si^e od zawsze "+
                "poro^sni^eta g^estymi, pi^eknymi drzewami. D^eby kt�re "+
                "tajemniczo g^oruj^a nad innymi gatunkami przywodz^a na my^sl "+
                "ba^sniowe lasy, o kt^orych zazwyczaj czyta si^e tylko w "+
                "ksi^a^zkach. Wi^azy, ze swoimi sp�kanymi, ^luszcz^acymi si^e "+
                "pniami i powyginanymi konarami, stanowi^a niesamowite "+
                "dope^lnienie tego wra^zenia. Na ziemi zauwa^zy^c mo�na "+
                "uschni^ete paprocie, po^z^o^lk^le mchy, wrzosy oraz inne "+
                "pozosta^lo^sci le^snych kwiat^ow, kt^ore nadaj^a stawianym w "+
                "tym miejscu krokom lekko^sci, delikatnie jedynie "+
                "szeleszcz^ac. Harmonia panuj^aca tu ca^lkowicie pozwala "+
                "zapomnie^c o zgie^lku cywilizacji.";
        }
    }
    str += "\n";
    return str;
}
