/* Las komanda
   Autor: Avard 
   Opis : Edrain */

#include <stdproperties.h>
#include <macros.h>
#include <mudtime.h>
#include "dir.h"
inherit LAS_ELFOW_MROCZNY;

string dlugi_opis();

void
create_las()
{
    set_short("Puszcza bukowa.");
    set_long(&dlugi_opis());
    add_exit(LAS_ELFOW_LOKACJE+ "las_75.c","w");
    add_exit(LAS_ELFOW_LOKACJE+ "las_81.c","ne");
}

string dlugi_opis()
{
    string str = "";
    
    if(jest_dzien())
    {
        str += "Smuk�e sylwetki buk�w wygl�daj� niczym duchy lasu. Wra�enie "+
        "to pot�guje mrok panuj�cy dooko�a, ciemne powietrze czasem "+
        "przecinane smugami jasnego �wiat�a, kt�re wygrywa walk� z "+
        "nieprzyst�pnymi koronami buk�w. Puszcz� spowija mg�a i cie�, za "+
        "kt�rym nie wiadomo co mo�e czyha�. Cisz� zak��ca jedynie wiatr "+
        "nieustannie �piewaj�cy sw� pie�� gorzkiej samotno�ci i przemijania. "+
        "Zdaje si� jakby czas tutaj zatrzyma� sw�j bieg, nawet ptaki nie "+
        "zak��caj� ha�asami dumy tego miejsca.";
    }
    else
    {
        str += "Smuk�e sylwetki buk�w wygl�daj� niczym duchy lasu. Wra�enie "+
        "to pot�guje mrok panuj�cy dooko�a, ciemne powietrze czasem "+
        "przecinane smugami sino fioletowego blasku ksi�yca, kt�ry wygrywa "+
        "walk� z nieprzyst�pnymi koronami buk�w. Puszcz� spowija mg�a i "+
        "cie�, za kt�rym nie wiadomo co mo�e czyha�. Cisz� zak��ca jedynie "+
        "wiatr nieustannie �piewaj�cy sw� pie�� gorzkiej samotno�ci i "+
        "przemijania. Zdaje si� jakby czas tutaj zatrzyma� sw�j bieg, nawet "+
        "ptaki nie zak��caj� ha�asami dumy tego miejsca.";        
    }
    str += "\n";
    return str;
}
