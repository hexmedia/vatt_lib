/* Las komanda
   Autor: Avard 
   Opis : Edrain */

#include <stdproperties.h>
#include <macros.h>
#include <mudtime.h>
#include "dir.h"
inherit LAS_ELFOW_GESTY;

string dlugi_opis();

string opis_fiolkow();
string opis_zawilcow();
string opis_dzwonkow();
string opis_wrzosow();

int wez(string str);

void
create_las()
{
    set_short("W puszczy");
    set_long(&dlugi_opis());
    add_exit(LAS_ELFOW_LOKACJE+ "las_65.c","nw");
    add_exit(LAS_ELFOW_LOKACJE+ "las_66.c","n");
    add_exit(LAS_ELFOW_LOKACJE+ "las_63.c","e");
    
    if(!CZY_JEST_SNIEG(this_object()))
    {
        add_item("mech","Po�acie soczysto zielonych �ody�ek wznosz? si� "+
        "nieznacznie nad ziemi�. Mi�dzy drobnymi listkami gromadz? "+
        "du�e ilo?ci wody, przez co ca�o?� jest wyj?tkowo wilgotna. "+
        "Ziemia zas�ana tymi ro?linkami wygl?da jakby by�a pokryta "+
        "naturalnym dywanem.\n");
    }
    add_item(({"zawilca","zawilce"}), &opis_zawilcow());
    add_item(({"dzwonek","dzwonki"}), &opis_dzwonkow());
    add_item(({"wrzos","wrzosy"}), &opis_wrzosow());
    add_item(({"fio^lka","fio^lki"}), &opis_fiolkow());
}

public void
init()
{
    ::init();
    add_action(wez,"we�");
    add_action(wez,"zbieraj");
    add_action(wez,"zbierz");
}

string dlugi_opis()
{
    string str = "";
    
    if(jest_dzien()) //Dzie�
    {
        if(CZY_JEST_SNIEG(this_object()))
        {
            str+="Wysokie drzewa o prostych strzelistych pniach poro�ni�tych "+
            "srebrzystoszar� g�adk� kor� ci�gn� si� w dal. Panuj�cy tu "+
            "p�mrok nadaje puszczy, pozbawionej podszycia, tajemniczy "+
            "wymiar. �wiat�o przepuszczane przez korony pokryte �niegiem, "+
            "opada kaskadami na ziemi� zachwycaj�c wydobywanym dla oczu "+
            "pi�knem boru. Gdzie� w oddali majaczy g�stwina lasu mieszanego, "+
            "sk�pana w ostrym mro�nym �wietle s�o�ca. Ziemia jest tu g�sto "+
            "pokryta bia�ym puchem, t�umi�c ca�kiem kroki w�drowca.";
        }
        else if(pora_roku() == MT_WIOSNA || pora_roku() == MT_LATO)
        {
            str+="Wysokie drzewa o prostych strzelistych pniach poro�ni�tych "+
            "srebrzystoszar� g�adk� kor� ci�gn� si� w dal. Panuj�cy tu "+
            "p�mrok nadaje puszczy, pozbawionej podszycia, tajemniczy "+
            "wymiar. Szmaragdowe �wiat�o przepuszczane przez korony opada "+
            "kaskadami na ziemi� zachwycaj�c wydobywanym dla oczu pi�knem "+
            "boru. Gdzie� w oddali majaczy g�stwina lasu mieszanego, jasno "+
            "o�wietlonego s�o�cem. Ziemia jest tu g�sto pokryta fio�kami i "+
            "zawilcami, a tak�e orzeszkami buku, kt�re s� na tyle smaczne, "+
            "i� mog� zaspokoi� pierwszy g��d, a nawet s�u�y� jako g��wna "+
            "strawa w�drowca.";
        }
        else if(pora_roku() == MT_JESIEN)
        {
            str+="Wysokie drzewa o prostych strzelistych pniach poro�ni�tych "+
            "srebrzystoszar� g�adk� kor� ci�gn� si� w dal. Panuj�cy tu "+
            "p�mrok nadaje puszczy, pozbawionej podszycia, tajemniczy "+
            "wymiar. R�nokolorowe �wiat�o przepuszczane przez korony "+
            "upstrzone ��tymi i czerwonymi li��mi, opada kaskadami na "+
            "ziemi� zachwycaj�c wydobywanym dla oczu pi�knem boru. Gdzie� w "+
            "oddali majaczy g�stwina lasu mieszanego, jasno o�wietlonego "+
            "s�o�cem. Ziemia jest tu g�sto pokryta wrzosami i dzwonkami "+
            "le�nymi, a tak�e orzeszkami buku, kt�re s� na tyle smaczne, i� "+
            "mog� zaspokoi� pierwszy g��d, a nawet s�u�y� jako g��wna strawa "+
            "w�drowca.";
        }
    }
    else //Noc
    {
        if(CZY_JEST_SNIEG(this_object()))
        {
            str+="Wysokie drzewa o prostych strzelistych pniach poro�ni�tych "+
            "srebrzystoszar� g�adk� kor� ci�gn� si� w dal. Panuj�cy tu mrok "+
            "nadaje puszczy, pozbawionej podszycia, tajemniczy wymiar. Sino "+
            "fioletowe �wiat�o ksi�yca przepuszczane przez korony pokryte "+
            "czapami �niegu opada kaskadami na ziemi� zachwycaj�c "+
            "wydobywanym dla oczu pi�knem boru. Gdzie� w oddali majaczy "+
            "g�stwina lasu mieszanego, ton�cego w mroku nocy. Ziemia jest tu "+
            "g�sto pokryta bia�ym puchem, t�umi�c ca�kiem kroki w�drowca.";
        }
        else if(pora_roku() == MT_WIOSNA || pora_roku() == MT_LATO)
        {
            str+="Wysokie drzewa o prostych strzelistych pniach "+
            "poro�ni�tych srebrzystoszar� g�adk� kor� ci�gn� si� w dal. "+
            "Panuj�cy tu mrok nadaje puszczy, pozbawionej podszycia, "+
            "tajemniczy wymiar. Sino fioletowe �wiat�o ksi�yca "+
            "przepuszczane przez korony opada kaskadami na ziemi� "+
            "zachwycaj�c wydobywanym dla oczu pi�knem boru. Gdzie� w oddali "+
            "majaczy g�stwina lasu mieszanego, ton�cego w mroku nocy. Ziemia "+
            "jest tu g�sto pokryta fio�kami i zawilcami, a tak�e orzeszkami "+
            "buku, kt�re s� na tyle smaczne, i� mog� zaspokoi� pierwszy "+
            "g��d, a nawet s�u�y� jako g��wna strawa w�drowca.";
        }
        else if(pora_roku() == MT_JESIEN)
        {
            str+="Wysokie drzewa o prostych strzelistych pniach poro�ni�tych "+
            "srebrzystoszar� g�adk� kor� ci�gn� si� w dal. Panuj�cy tu mrok "+
            "nadaje puszczy, pozbawionej podszycia, tajemniczy wymiar. Sino "+
            "fioletowe �wiat�o ksi�yca przepuszczane przez korony opada "+
            "kaskadami na ziemi� zachwycaj�c wydobywanym dla oczu pi�knem "+
            "boru. Gdzie� w oddali majaczy g�stwina lasu mieszanego, "+
            "ton�cego w mroku nocy. Z Ziemia jest tu g�sto pokryta wrzosami "+
            "i dzwonkami le�nymi, a tak�e orzeszkami buku, kt�re s� na tyle "+
            "smaczne, i� mog� zaspokoi� pierwszy g��d, a nawet s�u�y� jako "+
            "g��wna strawa w�drowca.";
        }
    }
    str += "\n";
    return str;
}

string opis_zawilcow()
{
    string str;
    if (pora_roku() == MT_WIOSNA)
    {
    str += "Zawilec gajowy jest pojedynczym kwiatem o d^lugiej "+
    "delikatnej ^lody^zce wyrastaj^acej z k^l^acza. "+
    "Poszarpane listki, zazwyczaj tylko trzy "+
    "wyst^epuj^ace w ok^o^lkach i wygl^adaj^a jak szpony "+
    "jakiej^s zjawy. Aktualnie ka^zda ^lody^zka zwie^nczona "+
    "jest prze^slicznym bia^lym kwiatkiem, sk^ladaj^acym "+
    "si^e z pi^eciu lub siedmiu p^latk^ow. Soki tej "+
    "ro^sliny s^a silnie truj^ace i powoduj^a zapalenie "+
    "nerek i uk^ladu pokarmowego, wywo^luj^ac silne "+
    "wymioty i biegunk^e.\n"; 
    return str;
    }
    else
    {
    return 0;
    }
}

string opis_dzwonkow()
{
    string str;
    if (pora_roku() == MT_LATO)
    {
    str = "Wiotkie ^lody^zki wznosz^a si^e kilkadziesi^at "+
    "centymetr^ow nad ziemi^e, ich ko^nce zwieszaj^a "+
    "si^e w stron^e pod^lo^za obci^a^zone okaza^lymi "+
    "kwiatami. Fioletowe, zros^le p^latki tworz^a kielich "+
    "z jasno^z^o^ltym ^srodkiem. Odurzaj^acy zapach "+
    "dzwonk^ow le^snych wabi owady.\n";
    return str;
    }
    else
    {
    return 0;
    }
}

string opis_fiolkow()
{
    string str;
    if (pora_roku() == MT_WIOSNA)
    {
    str = "P^lo^z^aca si^e po ziemi bylina jest teraz g^esto "+
    "upstrzona pi^eknie pachn^acymi fioletowymi "+
    "kwiatami. Delikatne kwiaty wyniesione s^a nad "+
    "ziemie na wysokich szypu^lkach, ka^zdy z nich "+
    "sk^lada si^e z pi^eciu p^latk^ow dw^och "+
    "skierowanych w g^or^e i trzech w d^o^l. "+
    "Zielone listki, kt^ore dodatkowo eksponuj^a "+
    "pi^ekno koloru fio^lk^ow, maj^a kszta^lt sercowaty "+
    "i s^a lekko karbowane od g^ory.\n";
    return str;
    }
    else if (pora_roku() == MT_LATO)
    {
    str = "Na p^lo^z^acej si^e po ziemi bylinie nie wida^c "+
    "ju^z ^slad^ow niedawno pi^eknie kwitn^acych, "+
    "drobnych p^latk^ow fio^lk^ow. Z wonnej ro^sliny "+
    "pozosta^ly ^lody^zki i ciemnozielone, sercowate "+
    "li^scie, kt^ore t^lumi^a wszelkie odg^losy "+
    "zwierz^at i podr^o^znych.\n";
    return str;
    }
    else return 0;
}

string opis_wrzosow()
{
    string str;
    if (pora_roku() == MT_JESIEN)
    {
    str = "Ta zimozielona dwupienna krzewinka o listkach "+
    "rozes^lanych po ziemi, p^lo^zy si^e g^esto "+
    "wsz^edzie dooko^la, tworz^ac wyciszaj^acy dla "+
    "krok^ow ciemnozielony dywan. Jej drobne, "+
    "igie^lkowate li^scie u^lo^zone s^a naprzeciw siebie "+
    "i sprawiaj^a mylne wra^zenie bardzo ubogich, "+
    "jednak s^a one tylko dodatkiem do pi^eknych "+
    "r^o^zowo- fioletowych kwiat^ow w pe^lni "+
    "rozkwitaj^acych wczesn^a jesieni^a, r^ownie "+
    "drobnych, a jednak zachwycaj^acych swym pi^eknem, "+
    "kt^ore maluje ^swiat w czasie gdy prawie wszelkie "+
    "inne ozdoby przyrody ju^z przekwit^ly.\n";
    return str;
    }
    else
    {
    return 0;
    }
}

int wez(string str)
{
    if(query_verb() ~= "we�")
        notify_fail("We� co?\n");
    if(query_verb() ~= "zbieraj")
        notify_fail("Zbieraj co?\n");
    if(query_verb() ~= "zbierz")
        notify_fail("Zbierz co?\n");
        
    if (!str) return 0;
    if (!strlen(str)) return 0;
    
    if(pora_roku() == MT_ZIMA)
        return 0;
    
    if(query_verb() ~= "we�")
    {
        if(!parse_command(str,environment(TP),"'orzech' / 'orzechy'"))
            return 0;
    }
    else 
    {
        if(!parse_command(str,environment(TP),"'orzechy'"))
            return 0;
    }
    write("Zaczynasz zbiera� orzechy.\n");
    saybb(QCIMIE(TP,PL_MIA)+" zaczyna zbiera� orzechy.\n");
    clone_object(LAS_ELFOW_PRZEDMIOTY+"paraliz_orzechy.c")->move(TP);
    return 1;
}