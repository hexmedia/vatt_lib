/* Las komanda
   Autor: Avard 
   Opis : Edrain */

#include <stdproperties.h>
#include <macros.h>
#include <mudtime.h>
#include "dir.h"
inherit LAS_ELFOW_GESTY;

string dlugi_opis();

string opis_fiolkow();
string opis_zawilcow();

int wez(string str);

void
create_las()
{
    set_short("Na skraju puszczy");
    set_long(&dlugi_opis());
    add_exit(LAS_ELFOW_LOKACJE+ "las_68.c","nw");
    
    if(!CZY_JEST_SNIEG(this_object()))
    {
        add_item("mech","Po�acie soczysto zielonych �ody�ek wznosz? si� "+
        "nieznacznie nad ziemi�. Mi�dzy drobnymi listkami gromadz? "+
        "du�e ilo?ci wody, przez co ca�o?� jest wyj?tkowo wilgotna. "+
        "Ziemia zas�ana tymi ro?linkami wygl?da jakby by�a pokryta "+
        "naturalnym dywanem.\n");
    }
    add_item(({"zawilca","zawilce"}), &opis_zawilcow());
    add_item(({"fio^lka","fio^lki"}), &opis_fiolkow());
}

public void
init()
{
    ::init();
    add_action(wez,"we�");
    add_action(wez,"zbieraj");
    add_action(wez,"zbierz");
}

string dlugi_opis()
{
    string str = "";
    
    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="Strzeliste buki porastaj�ce ten teren tworz� g�st� puszcz�. Las "+
        "z rzadka przerzedzony przez poprzewracane pojedyncze drzewa sprawia "+
        "ba�niowe wra�enie. Ga��zie buk�w wyrastaj� dopiero w g�rnych "+
        "cz�ciach drzew przy samej koronie, przez co wchodz�c do puszczy "+
        "zauwa�a si� tylko omsza�e pnie i mrok panuj�cy dooko�a. Ziemi� "+
        "g�sto pokrywa bia�y puch �niegu. W puszczy panuje ca�kowita cisza "+
        "zak��cana tylko wyciem wiatru i pohukiwaniem s�w.";
    }
    else if(pora_roku() == MT_WIOSNA || pora_roku() == MT_LATO)
    {
        str+="Strzeliste buki porastaj�ce ten teren tworz� g�st� puszcz�. "+
        "Las z rzadka przerzedzony przez poprzewracane pojedyncze drzewa "+
        "sprawia ba�niowe wra�enie. Ga��zie buk�w wyrastaj� dopiero w "+
        "g�rnych cz�ciach drzew przy samej koronie, przez co wchodz�c do "+
        "puszczy zauwa�a si� tylko omsza�e pnie i mrok panuj�cy dooko�a. "+
        "Ziemi� g�sto pokrywaj� li�cie, drobne bukowe orzeszki, k�pki "+
        "zawilc�w i fio�k�w, a tak�e wilgotny mech. W puszczy panuje "+
        "ca�kowita cisza zak��cana tylko wyciem wiatru i pohukiwaniem s�w.";
    }
    else if(pora_roku() == MT_JESIEN)
    {
        str+="Strzeliste buki porastaj�ce ten teren tworz� g�st� puszcz�. "+
        "Las z rzadka przerzedzony przez poprzewracane pojedyncze drzewa "+
        "sprawia ba�niowe wra�enie. Ga��zie buk�w wyrastaj� dopiero w "+
        "g�rnych cz�ciach drzew przy samej koronie, przez co wchodz�c do "+
        "puszczy zauwa�a si� tylko omsza�e pnie i mrok panuj�cy dooko�a. "+
        "Ziemi� g�sto pokrywaj� li�cie, drobne bukowe orzeszki, k�pki "+
        "wrzos�w, a tak�e suchy mech. W puszczy panuje ca�kowita cisza "+
        "zak��cana tylko wyciem wiatru i pohukiwaniem s�w.";
    }
    
    
    str += "\n";
    return str;
}

string opis_zawilcow()
{
    string str;
    if (pora_roku() == MT_WIOSNA)
    {
    str += "Zawilec gajowy jest pojedynczym kwiatem o d^lugiej "+
    "delikatnej ^lody^zce wyrastaj^acej z k^l^acza. "+
    "Poszarpane listki, zazwyczaj tylko trzy "+
    "wyst^epuj^ace w ok^o^lkach i wygl^adaj^a jak szpony "+
    "jakiej^s zjawy. Aktualnie ka^zda ^lody^zka zwie^nczona "+
    "jest prze^slicznym bia^lym kwiatkiem, sk^ladaj^acym "+
    "si^e z pi^eciu lub siedmiu p^latk^ow. Soki tej "+
    "ro^sliny s^a silnie truj^ace i powoduj^a zapalenie "+
    "nerek i uk^ladu pokarmowego, wywo^luj^ac silne "+
    "wymioty i biegunk^e.\n"; 
    return str;
    }
    else
    {
    return 0;
    }
}

string opis_fiolkow()
{
    string str;
    if (pora_roku() == MT_WIOSNA)
    {
    str = "P^lo^z^aca si^e po ziemi bylina jest teraz g^esto "+
    "upstrzona pi^eknie pachn^acymi fioletowymi "+
    "kwiatami. Delikatne kwiaty wyniesione s^a nad "+
    "ziemie na wysokich szypu^lkach, ka^zdy z nich "+
    "sk^lada si^e z pi^eciu p^latk^ow dw^och "+
    "skierowanych w g^or^e i trzech w d^o^l. "+
    "Zielone listki, kt^ore dodatkowo eksponuj^a "+
    "pi^ekno koloru fio^lk^ow, maj^a kszta^lt sercowaty "+
    "i s^a lekko karbowane od g^ory.\n";
    return str;
    }
    else if (pora_roku() == MT_LATO)
    {
    str = "Na p^lo^z^acej si^e po ziemi bylinie nie wida^c "+
    "ju^z ^slad^ow niedawno pi^eknie kwitn^acych, "+
    "drobnych p^latk^ow fio^lk^ow. Z wonnej ro^sliny "+
    "pozosta^ly ^lody^zki i ciemnozielone, sercowate "+
    "li^scie, kt^ore t^lumi^a wszelkie odg^losy "+
    "zwierz^at i podr^o^znych.\n";
    return str;
    }
    else return 0;
}

int wez(string str)
{
    if(query_verb() ~= "we�")
        notify_fail("We� co?\n");
    if(query_verb() ~= "zbieraj")
        notify_fail("Zbieraj co?\n");
    if(query_verb() ~= "zbierz")
        notify_fail("Zbierz co?\n");
        
    if (!str) return 0;
    if (!strlen(str)) return 0;
    
    if(pora_roku() == MT_ZIMA)
        return 0;
    
    if(query_verb() ~= "we�")
    {
        if(!parse_command(str,environment(TP),"'orzech' / 'orzechy'"))
            return 0;
    }
    else 
    {
        if(!parse_command(str,environment(TP),"'orzechy'"))
            return 0;
    }
    write("Zaczynasz zbiera� orzechy.\n");
    saybb(QCIMIE(TP,PL_MIA)+" zaczyna zbiera� orzechy.\n");
    clone_object(LAS_ELFOW_PRZEDMIOTY+"paraliz_orzechy.c")->move(TP);
    return 1;
}