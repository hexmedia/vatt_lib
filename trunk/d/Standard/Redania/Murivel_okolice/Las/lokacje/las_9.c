/**
 * Las Komanda
 *
 * autor Cairell
 * opisy Edrain & Aladriel
 * data  styczen 2009
 */

#include <stdproperties.h>
#include <mudtime.h>
#include "dir.h"
inherit LAS_ELFOW_RZEKA;

string dlugi_opis();
string opis_poziomek();

void
create_las()
{
    set_short("Lipowy lasek.");
    set_long(&dlugi_opis());
    add_exit(LAS_ELFOW_LOKACJE+ "las_8.c","w");
    add_exit(LAS_ELFOW_LOKACJE+ "las_7.c.c","n");
    add_exit(LAS_ELFOW_LOKACJE+ "las_10.c","e");
    add_exit(LAS_ELFOW_LOKACJE+ "las_32.c","ne");
    add_exit(LAS_ELFOW_LOKACJE+ "las_6.c","nw");
    add_exit(LAS_ELFOW_LOKACJE+ "las_11.c","se");

    add_item(({"ul"}), "Od zbudowanego przez wierne swej kr^olowej "+
        "pszczo^ly ula bez ustanku dochodzi wyra^xne brz^eczenie. "+
        "Nie jest to d^xwi�k przyjemny dla ucha. Z jego dna na "+
        "traw^e powoli pokapuje bursztynowo - z^loty mi^od. Wida^c "+
        "niedawno ul przeszed^l najazd jakiego� ^lasego na "+
        "s^lodko^sci drapie^znika i a robotnice nie upora^ly si^e "+
        "jeszcze ze wszystkimi szkodami.");
    add_item(({"krzaczek", "poziomki"}), &opis_poziomek());
}

string dlugi_opis()
{
    string str = ""; 
    
    str+="Drzewa stoj^a tu w do^s^c du^zych odst^epach, "+
        "ze wzgl^edu na swoje wielkie korony, "+
        "majestatycznie wznosz^ace si^e nad lasem. ";

    if (jest_dzien() == 1)
    {
        if(pora_roku() == MT_WIOSNA)
        {
            str += "Opr^ocz lip ro^snie tutaj tylko ^swie^za, g^esta "+
                "trawa i niewielkie krzewy pachn^acych poziomek. "+
                "W powietrzu unosi si^e s^lodki zapach, pochodz^acy "+
                "z drobnych kwiat^ow lip. Ich nektar jest doskona^lym "+
                "materia^lem na mi^od, wi^ec widok ula uczepionego "+
                "jednej z ga^l^ezi nie powinien zaskakiwa^c. S^lyszysz "+
                "brz^eczenie setek pszczelich skrzyde^lek, pilnie "+
                "pracuj^acych dla swej kr^olowej.";
        }
        if(pora_roku() == MT_LATO)
        {
            str += "Opr^ocz lip ro^snie tutaj tylko ^swie^za, g^esta "+
                "trawa i niewielkie krzewy pachn^acych poziomek. "+
                "W powietrzu unosi si^e s^lodki zapach, pochodz^acy "+
                "z drobnych kwiat^ow lip. Ich nektar jest doskona^lym "+
                "materia^lem na mi^od, wi^ec widok ula uczepionego "+
                "jednej z ga^l^ezi nie powinien zaskakiwa^c. S^lyszysz "+
                "brz^eczenie setek pszczelich skrzyde^lek, pilnie "+
                "pracuj^acych dla swej kr^olowej.";
        }
        if(pora_roku() == MT_JESIEN)
        {
            str += "Opr^ocz lip ro^snie tutaj "+
                "tylko g^esta trawa powoli usychaj^aca na zim^e i "+
                "niewielkie krzewy ju^z dawno przekwit^lych poziomek, "+
                "kt^orych owoce cieszy^ly w czasie lata. W powietrzu "+
                "unosi si^e zapach jesiennych li^sci lip, "+
                "przebawiaj^acych si^e z zieleni, na z^loto i br^az.";
        }
        if(pora_roku() == MT_ZIMA)
        {
            str += "Opr^ocz lip ro^snie tutaj tylko g^esta trawa "+
                "teraz pokryta du^zymi zaspami ^sniegu, kt^ory "+
                "mi^lo trzeszczy pod butami. W powietrzu unosi "+
                "si^e zapach strzaskanych przez mr^oz drzew, "+
                "kt^ore spokojnie przesypiaj^a nieprzychyln^a "+
                "por^e roku.";

        }
    }
    else //noc
    {
        str += "Poza tym mroki nocy nie pozwalaj^a "+
            "odr^o^zni^c zbyt wielu szczeg^o^l^ow. ";
    }

str+="\n";
return str;
}

string opis_poziomek()
{
    string strr;
    if (pora_roku() == MT_ZIMA)
    {
        strr = "^Snieg i zimowa pora nie sprzyja szukaniu "+
            "poziomek.\n";
        return strr;
    }
    else
    {
        strr = "Z ma^lej k^epki listk^ow "+
            "wyrastaj^a d^lugie wygi^ete w ^luk ^lody^zki. Na ich "+
            "ko^ncach zawieszone s^a drobne owoce poziomki le^snej. "+
            "Niekt^ore s^a jeszcze czerwono � zielone, gdy^z "+
            "nie dosta^ly odpowiedniej dawki cennych promieni "+
            "s^lo^nca.\n";
        return strr;
    }
}

public string
exits_description() 
{
    return "Drog^e przez las mo^zna odnale^s^c id^ac na zach^od, "+
        "p^o^lnoc, po^lnocny wsch^od, wsch^od lub po^ludniowy "+
        "wsch^od.\n";
}
