/**
 * Las Komanda
 *
 * autor Cairell
 * opisy Edrain & Aladriel
 * data  styczen 2009
 */

#include <stdproperties.h>
#include <mudtime.h>
#include "dir.h"
inherit LAS_ELFOW_RZEKA;

string dlugi_opis();
string opis_krzewu();
string opis_fiolkow();

void
create_las()
{
    set_short("Wierzba nad brzegiem rzeki.");
    set_long(&dlugi_opis());

    add_exit(TRAKT_LOKACJE+ "trakt_34.c","n");
    add_exit(TRAKT_LOKACJE+ "trakt_33.c","w");
    add_exit(LAS_ELFOW_LOKACJE+ "las_17.c","e");
    add_exit(TRAKT_LOKACJE+ "trakt_35.c","ne");

    add_item(({"kwiat", "kwiaty", "fiolki"}), 
        &opis_fiolkow());
    add_item(({"krzewy kwiatow", "krzew", 
        "krzewy"}), &opis_krzewu());
}

string dlugi_opis()
{
    string str = ""; 

    if(pora_roku() == MT_ZIMA)
    {
        str += "Bujna w czasie lata trawa teraz jest g^esto "+
            "pokryta bia^lym puchem, kt^ory chroni p^aczki ro^slin "+
            "przed przemarzni^eciem. Od strony p^lyn^acej "+
            "nieopodal rzeki dociera tu troch^e cieplejszy wiatr "+
            "przebijaj^acy mro^xn^a aur^e zimowej pory.";
    }

    if(pora_roku() == MT_JESIEN)
    {
        str += "Bujna trawa porastaj^aca ten teren jest g^esto "+
            "przetykana jesiennymi kwaitami, kt^ore nieustannie "+
            "przypominaj^a, �e powoli ^swiat przyrody "+
            "przygotowuje si^e do zimowego snu. O up^lywaj^acym "+
            "czasie dodatkowo przypomina cichy szmer rzeki "+
            "p^lyn^acej nieopodal. ^Spiew ptak^ow miesza si^e z "+
            "delikatnym trzepotem skrzyde^l p^o^xnoletnich motyli.";
    }

    else //inne pory roku
    {
        str += "Bujna trawa porastaj^aca ten teren jest g^esto "+
            "przetykana kolorowymi kwiatami, kt^ore nadaj^a temu "+
            "miejscu ba^sniowego uroku. Wydaje si^e jakby "+
            "t^ecza upad^la na ziemi^e przyozdabiaj^ac j^a "+
            "swym pi^eknem, a o up^lywaj�cym czasie przypomina "+
            "tylko cichy szmer rzeki p^lyn^acej nieopodal. "+
            "^Spiew ptak^ow miesza si^e z delikatnym trzepotem "+
            "skrzyde^l motyli i brz^eczeniem pszcz^o^l.";
    }

str+="\n";
return str;
}

string opis_krzewu()
{
    string strr;

    if (pora_roku() == MT_JESIEN)
    {
        strr = "Bujne, zielono - czerwone li^scie tworz^a co^s na "+
            "kszta^lt sp^ojnej kuli, na kt^orej wierzchu znajduj^a "+
            "si^e liczne pozosta^lo^sci po niegdy^s pi^eknych "+
            "kwiatach.\n";
        return strr;
    }
    if (pora_roku() == MT_ZIMA)
    {
        strr = "";
        return strr;
    }
    else
    {
        strr = "Bujne, w^sciekle zielone li^scie tworz^a co^s na "+
            "kszta^lt sp^ojnej kuli, na kt^orej wierzchu znajduj^a "+
            "si^e liczne kwiaty o mlecznej barwie. Ka^zdy kwiat "+
            "ma potr^ojn^a warstw^e p^latk^ow, co czyni je "+
            "okazalszymi.\n";
        return strr;
    }
}

string opis_fiolkow()
{
    string st;

    if (pora_roku() == MT_WIOSNA)
    {
        st = "P^lo^z^aca si^e po ziemi bylina jest teraz g^esto "+
            "upstrzona pi^eknie pachn^acymi fioletowymi kwiatami. "+
            "Delikatne kwiaty wyniesione s^a nad ziemie na wysokich "+
            "szypu^lkach, ka^zdy z nich sk^lada si^e z pi^eciu p^latk^ow "+
            "dw^och skierowanych w g^or^e i trzech w d^o^l. Zielone "+
            "listki, kt^ore dodatkowo eksponuj^a piekno koloru "+
            "fio^lk^ow, maj^a kszta^lt sercowaty i sa lekko karbowane "+
            "od g^ory.\n";
        return st;
    }
    if (pora_roku() == MT_LATO)
    {
        st = "Na p^lo^z^acej sie po ziemi bylinie nie wida^c ju^z "+
            "^slad^ow niedawno pi^eknie kwitn^acych, drobnych "+
            "p^latk^ow fio^lk^ow. Z wonnej ro^sliny pozosta^ly "+
            "^lody^zki i ciemnozielone, sercowate li^scie, kt^ore "+
            "t^lumi^a wszelkie odg^losy zwierz^at i podr^o^znych.\n";
        return st;
    }

    else
    {
        st = "";
        return st;
    }
}

public string
exits_description() 
{
    return "Drog^e mi^edzy drzewami mo^zna odnale^s^c ruszaj^ac "+
        "na wsch^od, za^s na zachodzie, po^lnocy oraz p^olnocnycm "+
        "wschodzie majaczy mi^edzy drzewami trakt.\n";
}