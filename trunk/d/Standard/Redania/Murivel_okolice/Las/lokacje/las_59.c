/* Las komanda
   Autor: Avard 
   Opis : Edrain */

#include <stdproperties.h>
#include <mudtime.h>
#include "dir.h"
inherit LAS_ELFOW_GESTY;

string dlugi_opis();

void
create_las()
{
    set_short("Po^srod buk�w.");
    set_long(&dlugi_opis());
    add_exit(LAS_ELFOW_LOKACJE+ "las_41.c","w");
    add_exit(LAS_ELFOW_LOKACJE+ "las_42.c","s");
    
    if(!CZY_JEST_SNIEG(this_object()))
    {
        add_item("mech","Po�acie soczysto zielonych �ody�ek wznosz� si� "+
        "nieznacznie nad ziemi�. Mi�dzy drobnymi listkami gromadz� "+
        "du�e ilo�ci wody, przez co ca�o�� jest wyj�tkowo wilgotna. "+
        "Ziemia zas�ana tymi ro�linkami wygl�da jakby by�a pokryta "+
        "naturalnym dywanem.\n");
    }
}
string dlugi_opis()
{
    string str = "";
    
    if(jest_dzien()) //Dzie�
    {
        if(CZY_JEST_SNIEG(this_object()))
        {
            str+="W tym miejscu z�owroga cisza gra na strunach wiatru. "+
            "Wysokie, strzeliste buki pn� si� pod niebosk�on stwarzaj�c "+
            "wra�enie ma�o�ci wszelkich stworze� pozostaj�cych na dole. "+
            "Przez ci�ki baldachim koron nie przenikaj� �adne promienie "+
            "�wiat�a zgrabnie zatrzymywane mozaik� ga��zi i bia�ego puchu "+
            "g�sto pokrywaj�cego konary. Pod stopami ci�gnie si� bia�y "+
            "dywan �niegu dodaj� uroku temu miejscu.";
        }
        else 
        {
            str+="W tym miejscu z�owroga cisza gra na strunach wiatru. "+
            "Wysokie, strzeliste buki pn� si� pod niebosk�on stwarzaj�c "+
            "wra�enie ma�o�ci wszelkich stworze� pozostaj�cych na dole. "+
            "Przez ci�ki baldachim koron nie przenikaj� �adne promienie "+
            "�wiat�a zgrabnie zatrzymywane mozaik� ga��zi. Przyt�umione "+
            "�wiat�o oplata p�mrokiem krajobraz puszczy. Pod stopami "+
            "ci�gn� si� mchy, oplataj�c drzewa i pn�c si� na "+
            "srebrzystoszar� kor�, dodaj� uroku temu miejscu.";
        }
    }
    else //Noc
    {
        if(CZY_JEST_SNIEG(this_object()))
        {
            str+="W tym miejscu z�owroga cisza gra na strunach wiatru. "+
            "Wysokie, strzeliste buki pn� si� pod niebosk�on stwarzaj�c "+
            "wra�enie ma�o�ci wszelkich stworze� pozostaj�cych na dole. "+
            "Przez ci�ki baldachim koron nie przenikaj� �adne promienie "+
            "ksi�ycowego �wiat�a zgrabnie zatrzymywane mozaik� ga��zi i "+
            "bia�ego puchu g�sto pokrywaj�cego konary. Sino blady krajobraz "+
            "ci�gnie si� w dal, znikaj�c w mroku. Pod stopami ci�gnie si� "+
            "bia�y dywan �niegu dodaj� uroku temu miejscu.";
        }
        else 
        {
            str+="W tym miejscu z�owroga cisza gra na strunach wiatru. "+
            "Wysokie, strzeliste buki pn� si� pod niebosk�on stwarzaj�c "+
            "wra�enie ma�o�ci wszelkich stworze� pozostaj�cych na dole. "+
            "Przez ci�ki baldachim koron nie przenikaj� �adne promienie "+
            "ksi�ycowego �wiat�a zgrabnie zatrzymywane mozaik� ga��zi. "+
            "Przyt�umione cienie oplataj� mrokiem krajobraz puszczy. Pod "+
            "stopami ci�gn� si� mchy, oplataj�c drzewa i pn�c si� na "+
            "srebrzystoszar� kor�, dodaj� uroku temu miejscu.";
        }
    }
    str += "\n";
    return str;
}
