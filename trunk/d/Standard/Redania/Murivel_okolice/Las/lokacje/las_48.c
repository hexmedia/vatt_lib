
#include <stdproperties.h>
#include <mudtime.h>
#include "dir.h"
inherit LAS_ELFOW_GESTY;

string dlugi_opis();

void
create_las()
{
    set_short("Gdzie^s po^sr^od d^eb^ow g^estego lasu");
    set_long(&dlugi_opis());
    add_exit(LAS_ELFOW_LOKACJE+ "ruiny_1.c","^scie^zka");
    add_exit(LAS_ELFOW_LOKACJE+ "las_49.c","n");
    add_exit(LAS_ELFOW_LOKACJE+ "las_50.c","se");
    add_exit(LAS_ELFOW_LOKACJE+ "las_27.c","s");
    
    add_item(({"^s^cie^zke"}),"Kr^otka, ale kr^eta ^scie^zka prowadzi gdzie^"+
    "s mi^edzy pnie d^eb^ow. Momentami na ko^ncu ^scie^zki pob^lyskuj^a bia^"+
    "le ruiny jakiej^s budowli.\n");
    add_item(({"ruiny","budowle"}),"Niedaleko za ^scie^zka poblyskuj^a ruiny"+
    " budynku jakiej^s elfiej budowli. Niemal^ze ca^le mury okalaj^ace po^sia"+
    "d^lo^s^c zostal^y zniszczone, natomiast resztki ^s^cian budynku poro^sni"+
    "^ete s^a g^estym, zielonym bluszczem.\n");
}
string dlugi_opis()
{
    string str;
    if(jest_dzien() == 1) // DZIEN
    {
    if(CZY_JEST_SNIEG(this_object()))
    {
        str="Miejsce to porastaj^a g^l^ownie d^eby - drzewa bardzo du^ze i ok"+
        "aza^le, o prostych pniach i silnie rozga^l^ezionych, nieregularnych "+
        "koronach. Mi^edzy nimi mo^zna dostrzec tak^ze wi^azy. Zewsz^ad docho"+
        "dz^a ci^e odg^losy zwierz^at, od tych najmniejszych, ^zyj^acych w ru"+
        "nie teraz ca^lkowicie pokrytym ^sniegiem, a^z po ryk nied^xwiedzi. L"+
        "as z rzadka wype^lniaj^a przewr^ocone pnie, czy nawet ca^le drzewa, "+
        "niew^atpliwie za s^labe by oprze^c si^e niszczycielskiej sile wiatru.";
    }
    else //wiosna, lato, jesien i zima kiedy nie ma sniegu
    {
        str="Miejsce to porastaj^a g^l^ownie d^eby - drzewa ogromne i niezwyk"+
        "le okaza^le, o prostych pniach i silnie rozga^l^ezionych, nieregular"+
        "nych koronach. Mi^edzy nimi mo^zna dostrzec tak^ze wi^azy. Zewsz^ad "+
        "dochodz^a ci^e odg^losy zwierz^at, od pisk^ow tych najmniejszych, ^z"+
        "yj^acych w mchu i paproci, a^z po ryk nied^xwiedzi. Las z rzadka wyp"+
        "e^lniaj^a przewr^ocone pnie, czy nawet ca^le, wyrwane z korzeniami d"+
        "rzewa, niew^atpliwie za s^labe by oprze^c si^e niszczycielskiej sile"+
        " natury. W koronach g^lo^sno ^spiewaj^a ptaki daj^ac upust swojej ra"+
        "do^sci.";
    }
    }
    if(jest_dzien() == 0) // NOC
    {
    if(CZY_JEST_SNIEG(this_object()))
    {
        str="Miejsce to porastaj^a g^l^ownie d^eby - drzewa bardzo du^ze i ok"+
        "aza^le, o prostych pniach i silnie rozga^l^ezionych, nieregularnych "+
        "koronach. Mi^edzy nimi mo^zna dostrzec tak^ze wi^azy. Zewsz^ad docho"+
        "dz^a ci^e odg^losy zwierz^at, od tych najmniejszych, ^zyj^acych w ru"+
        "nie teraz ca^lkowicie pokrytym ^sniegiem, a^z po ryk nied^xwiedzi. L"+
        "as z rzadka wype^lniaj^a przewr^ocone pnie, czy nawet ca^le drzewa, "+
        "niew^atpliwie za s^labe by oprze^c si^e niszczycielskiej sile wiatru.";
    }
    else //wiosna, lato, jesien i zima kiedy nie ma sniegu
    {
        str="Miejsce to porastaj^a g^l^ownie d^eby - drzewa ogromne i niezwyk"+
        "le okaza^le, o prostych pniach i silnie rozga^l^ezionych, nieregular"+
        "nych koronach. Mi^edzy nimi mo^zna dostrzec tak^ze wi^azy. Zewsz^ad "+
        "dochodz^a ci^e odg^losy zwierz^at, od pisk^ow tych najmniejszych, ^z"+
        "yj^acych w mchu i paproci, a^z po ryk nied^xwiedzi. Las z rzadka wyp"+
        "e^lniaj^a przewr^ocone pnie, czy nawet ca^le, wyrwane z korzeniami d"+
        "rzewa, niew^atpliwie za s^labe by oprze^c si^e niszczycielskiej sile"+
        " natury. W koronach g^lo^sno ^spiewaj^a ptaki daj^ac upust swojej ra"+
        "do^sci.";
    }
    }
    str += " Rozgl^adaj^ac si^e uwa^znie postrzegasz niewielk^a ^scie^zke pr"+
    "owadz^ac^a mi^edzy drzewa.\n";
    return str;
}
