/**
 * Las Komanda
 *
 * autor Cairell
 * opisy Edrain & Aladriel
 * data  styczen 2009
 */
 
#include <stdproperties.h>
#include <mudtime.h>
#include "dir.h"
#include "pogoda.h"
#include <macros.h>

inherit LAS_ELFOW_RZEKA;

string dlugi_opis();

string opis_bluszczu();
string opis_dzwonkow();
string opis_wrzosow();
string opis_mchu();
string opis_fiolkow();
string opis_paproci();
string opis_plyt();
string opis_blokow();
string opis_murow();


void create_las()
{
    set_short("Ruiny marmurowego pa^lacu.");
    set_long(&dlugi_opis());

    add_exit(LAS_ELFOW_LOKACJE+ "las_79.c","ne");
    add_exit(LAS_ELFOW_LOKACJE+ "las_71.c","se");

    add_item(({"bluszcz","bluszcze"}), &opis_bluszczu());
    add_item(({"dzwonek","dzwonki"}), &opis_dzwonkow());
    add_item(({"wrzos","wrzosy"}), &opis_wrzosow());
    add_item(({"mech","mchy"}), &opis_mchu());
    add_item(({"fio^lek","fio^lki"}), &opis_fiolkow());
    add_item(({"papro^c","paprocie"}), &opis_paproci());
    add_item(({"p^lyt^e","p^lyty"}), &opis_plyt());
    add_item(({"blok","bloki"}), &opis_blokow());
    add_item(({"mur","mury"}), &opis_murow());

}

string dlugi_opis()
{
    string str = "";
    if(pora_roku() == MT_ZIMA)
    {
        if (jest_dzien() == 1)
        {
            if (CZY_JEST_SNIEG(this_object()))
            {
                  if (ZACHMURZENIE(TO) <= 2)
                {
                str += "Kaskada promieni s^lonecznych opadaj^acych ostro "+
                "w g^l^ab lasu odbija si^e od marmurowych "+
                "kamieniach prawie o^slepiaj^ac. ";
                }
                else
                {
                str += "";
                } 
            str += "Niegdy^s wspania^le i pi^ekne mury s^a juz tylko "+
            "wspomnieniem wcze^sniejszego blasku tego miejsca. "+
            "Bia^le p^lyty pokrywaj^a czapy ^sniegu, niemal "+
            "ca^lkowicie zas^laniaj^ac ich wspania^lo^s^c. "+
            "Mi^edzy zburzonymi budowlami ju^z dawno "+
            "wyros^lo nowe pokolenie drzew. M^lode brz^ozki, "+
            "ca^lkowicie pozbawione o tej porze roku li^sci "+
            "szumi^a jednostajnie, a w ich pie^sni pobrzmiewa "+
            "echo dawnych g^los^ow i ^smiechu elfich dzieci. "+
            "Pod stopami, po^sr^od wydawa^loby si^e "+
            "rozrzuconych niedbale blok^ow le^zy bia^ly dywan "+
            "^sniegu, kt^ory t^lumi wszelkie kroki."; 
            }
                else
            { 
             
                if (ZACHMURZENIE(TO) <= 2)
                {
                str += "Kaskada promieni s^lonecznych opadaj^acych ostro "+
                "w g^l^ab lasu odbija si^e od marmurowych "+
                "kamieniach prawie o^slepiaj^ac. ";
                }
                else
                {
                str += "";
                } 
            str += "Niegdy^s wspania^le i pi^ekne mury s^a juz tylko "+
            "wspomnieniem wcze^sniejszego blasku tego miejsca. "+
            "Mi^edzy zburzonymi budowlami ju^z dawno "+
            "wyros^lo nowe pokolenie drzew. M^lode brz^ozki, "+
            "ca^lkowicie pozbawione o tej porze roku li^sci "+
            "szumi^a jednostajnie, a w ich pie^sni pobrzmiewa "+
            "echo dawnych g^los^ow i ^smiechu elfich dzieci."; 
            }
        }
        else //noc
        {
            if (CZY_JEST_SNIEG(this_object()))
            { 
            if (ZACHMURZENIE(TO) <= 2)
            {
            str += "Smugi bladego blasku ksi^e^zyca opadaj^ace "+
            "delikatnie w g^l^ab lasu rozpraszaj^a si^e na "+
            "bia^lym puchu i zalewaj^ac okolic^e sinym "+
            "^swiat^lem. ";
            }
            else
            {
                str += "";
            } 
        str += "Niegdy^s wspaniale i piekne mury"+
        "dzisiaj s^a tylko wspomnieniem "+
        "wcze^sniejszego blasku tego miejsca. "+
        "Bia^le p^lyty pokrywaj^a czapy ^sniegu, niemal "+
        "ca^lkowicie zas^laniaj^ac ich wspania^lo^s^c. "+
        "Mi^edzy zburzonymi budowlami juz dawno "+
        "wyros^lo nowe pokolenie drzew. Mlode brzozki, "+
        "ca^lkowicie pozbawione o tej porze roku li^sci "+
        "szumi^a jednostajnie, a w ich pie^sni pobrzmiewa "+
        "echo danwych g^los^ow i ^smiechu elfich dzieci. "+
        "Pod stopami, po^sr^od wydawa^loby si^e "+
        "rozrzuconych niedbale blok^ow le^zy bia^ly dywan "+
        "^sniegu, kt^ory t^lumi wszelkie kroki."; 
            }
            else
            { 
         if (ZACHMURZENIE(TO) <= 2)
            {
            str += "Smugi bladego blasku ksi^e^zyca opadaj^ace "+
            "delikatnie w g^l^ab lasu rozpraszaj^a si^e na "+
            "bia^lym puchu i zalewaj^ac okolic^e sinym "+
            "^swiat^lem. ";
            }
            else
            {
                str += "";
            } 
        str += "Niegdy^s wspaniale i piekne "+
        "mury dzisiaj s^a tylko wspomnieniem "+
        "wcze^sniejszego blasku tego miejsca. "+
        "Mi^edzy zburzonymi budowlami juz dawno "+
        "wyros^lo nowe pokolenie drzew. Mlode brzozki, "+
        "ca^lkowicie pozbawione o tej porze roku li^sci "+
        "szumi^a jednostajnie, a w ich pie^sni pobrzmiewa "+
        "echo danwych g^los^ow i ^smiechu elfich dzieci."; 
            }
        }
    }
    else if(pora_roku() == MT_JESIEN)
    {
        if (jest_dzien() == 1)
        { 
            if (ZACHMURZENIE(TO) <= 2)
            {
            str += "Kaskada promieni s^lonecznych opadaj^acych "+
            "delikatnie w g^l^ab lasu rozprasza si^e na "+
            "marmurowych kamieniach. ";
            }
            else
            {
            str += "";
            } 
        str += "Niegdy^s wspania^le i piekne mury dzisiaj s^a juz "+
        "wspomnieniem wcze^sniejszego blasku tego miejsca. "+
        "wcze^sniejszego blasku tego miejsca. "+
        "Bia^le p^lyty porasta bluszcz i mech, a mi^edzy "+
        "zburzonymi budowlami ju^z dawno wyros^lo nowe "+
        "pokolenie drzew. M^lode brz^ozki i sucha, wysoka "+
        "trawa szumi^a jednostajnie, a w ich pie^sni "+
        "pobrzmiewa echo dawnych g^los^ow i ^smiechu "+
        "elfich dzieci. Pod stopami, po^sr^od wydawa^loby "+
        "si^e rozrzuconych niedbale blok^ow, rosn^a wrzosy "+
        "i dzwonki le^sne przetykanie niskimi paprociami."; 
        }
            else //noc
        { 
            if (ZACHMURZENIE(TO) <= 2)
            {
            str += "Smugi bladego blasku ksi^e^zyca opadaj^ace "+
            "delikatnie w g^l^ab lasu rozpraszaj^a si^e na "+
            "marmurowych kamieniach zalewaj^ac okolic^e sinym "+
            "^swiat^lem. ";
            }
            else
            {
            str += "";
            } 
        str += "Niegdy^s wspania^le i pi^ekne mury "+
        "dzisiaj s^a tylko wspomnieniem wcze^sniejszego "+
        "blasku tego miejsca. Bia^le p^lyty porasta "+
        "bluszcz i mech, a mi^edzy zburzonymi budowlami "+
        "juz dawno wyros^lo nowe pokolenie drzew. "+
        "M^lode brz^ozki i sucha, wysoka trawa szumi^a "+
        "jednostajnie, a w ich pie^sni pobrzmiewa echo "+
        "dawnych glos^ow i ^smiechu elfich dzieci. "+
        "Pod stopami, po^sr^od wydawa^loby si^e "+
        "rozrzuconych niedbale blok^ow, rosn^a wrzosy "+
        "i dzwonki le^sne przetykanie niskimi paprociami."; 
        }
    }
    else //Lato_wiosna
    {
        if (jest_dzien() == 1)
        { 
            if (ZACHMURZENIE(TO) <= 2)
            {
            str += "Kaskada promieni s^lonecznych opadaj^acych "+
            "delikatnie w g^l^ab lasu rozprasza si^e na "+
            "marmurowych kamieniach. ";
            }
            else
            {
            str += "";
            } 
         str += "Niegdy^s wspania^le i piekne mury dzisiaj s^a juz "+
        "wspomnieniem wcze^sniejszego blasku tego miejsca. "+
        "Biale p^lyty porasta bluszcz i mech, a mi^edzy "+
        "zburzonymi budowlami ju^z dawno wyros^lo nowe "+
        "pokolenie drzew. M^lode brz^ozki i g^esta wysoka "+
        "trawa szumi^a jednostajnie, a w ich pie^sni "+
        "pobrzmiewa echo dawnych g^los^ow i ^smiechu "+
        "elfich dzieci. Pod stopami, po^sr^od wydawa^loby "+
        "si^e rozrzuconych niedbale blok^ow, rosn^a "+
        "fio^lki i zawilce przetykanie niskimi paprociami."; 
        }
            else //noc
        { 
            if (ZACHMURZENIE(TO) <= 2)
            {
            str += "Smugi bladego blasku ksi^e^zyca opadaj^ace "+
            "delikatnie w g^l^ab lasu rozpraszaj^a si^e na "+
            "marmurowych kamieniach zalewaj^ac okolic^e sinym "+
            "^swiat^lem. ";
            }
            else
            {
            str += "";
            } 
        str += "Niegdy^s wspania^le i pi^ekne mury "+
        "dzisiaj s^a tylko wspomnieniem wcze^sniejszego "+
        "blasku tego miejsca. Bia^le p^lyty porasta "+
        "bluszcz i mech, a mi^edzy zburzonymi budowlami "+
        "ju^z dawno wyros^lo nowe pokolenie drzew. "+
        "M^lode brz^ozki i g^esta wysoka trawa szumi^a "+
        "jednostajnie, a w ich pie^sni pobrzmiewa echo "+
        "dawnych g^los^ow i ^smiechu elfich dzieci. "+
        "Pod stopami, po^sr^od wydawa^loby si^e "+
        "rozrzuconych niedbale blok^ow, rosn^a fio^lki "+
        "i zawilce przetykanie niskimi paprociami."; 
        }
    }

str+="\n";
return str;
}

string opis_paproci()
{
    string str;
    if (pora_roku() == MT_JESIEN)
    {
    str = "Niewysoka, lecz posiadaj^aca du^ze li^scie "+
    "papro^c ugina si^e do ziemi pod ci^e^zarem "+
    "zal^a^zk^ow znajduj^acych si^e pod listkami. "+
    "Jej przebarwiaj^ace si^e na br^azowo li^scie, "+
    "powoli usychaj^ace, przypominaj^a o nadchodz^acej "+
    "zimie. Mi^edzy jej listkami wiele ma造ch "+
    "stworzonek znalaz這 schronienie.\n";
    return str;
    }
    else if(pora_roku() == MT_ZIMA)
    {
    return 0;
    }
    else
    {
    str = "Niewysoka, lecz posiadaj^aca du^ze li^scie "+
    "papro^c ugina si^e do ziemi pod ci^e^zarem "+
    "zal^a^zk^ow znajduj^acych si^e pod listkami. "+
    "Jej soczysty, zielony kolor o^zywia miejsce. "+
    "Mi^edzy jej listkami wiele ma造ch stworzonek "+
    "znalaz這 schronienie.\n";
    return str;
    }
}

string opis_mchu()
{
    string str;
    if (pora_roku() == MT_ZIMA)
    {
    return 0;
    }
    else
    {
    str =     "Po豉cie soczysto zielonych 這dy瞠k wznosz� si� "+
    "nieznacznie nad ziemi�. Mi璠zy drobnymi listkami "+
    "gromadz� du瞠 ilo�ci wody, przez co ca這�� jest "+
    "wyj靖kowo wilgotna. Ziemia zas豉na tymi ro�linkami "+
    "wygl鉅a jakby by豉 pokryta naturalnym dywanem\n";
    return str;
    }
}

string opis_wrzosow()
{
    string str;
    if (pora_roku() == MT_JESIEN)
    {
    str =     "Ta zimozielona dwupienna krzewinka o listkach "+
    "rozes豉nych po ziemi, p這^zy si^e g^esto "+
    "wsz^edzie dooko豉, tworz^ac wyciszaj^acy dla "+
    "krok^ow ciemnozielony dywan. Jej drobne, "+
    "igie趾owate li^scie u這^zone s^a naprzeciw siebie "+
    "i sprawiaj^a mylne wra^zenie bardzo ubogich, "+
    "jednak s^a one tylko dodatkiem do pi^eknych "+
    "r^o^zowo- fioletowych kwiat^ow w pe軟i "+
    "rozkwitaj^acych wczesn^a jesieni^a, r^ownie "+
    "drobnych, a jednak zachwycaj^acych swym pi^eknem, "+
    "kt^ore maluje ^swiat w czasie gdy prawie wszelkie "+
    "inne ozdoby przyrody ju^z przekwit造.\n";
    return str;
    }
    else
    {
    return 0;
    }
}

string opis_bluszczu()
{
    string str;
    if (pora_roku() == MT_ZIMA)
    {
    return 0;
    }
    else
    {
    str = "Delikatne p^edy p這^z^acej si^e ro^sliny "+
    "oplataj^a bia貫 bloki marmurowych ruin. "+
    "Tworz^a na ich powierzchni niemal paj^ecz^a "+
    "sie^c us豉n^a zielonkawymi, do^s^c du^zymi "+
    "li^s^cmi. W ko^ncu wspinaj^a si^e w g^or^e "+
    "po resztkach ^scian i 逝k^ow, opadaj^ac "+
    "szeleszcz^ac^a zas這n^a ku ziemi.\n";
    return str;
    }
}

string opis_dzwonkow()
{
    string str;
    if (pora_roku() == MT_LATO)
    {
    str = "Wiotkie 這dy^zki wznosz^a si^e kilkadziesi^at "+
    "centymetr^ow nad ziemi^e, ich ko^nce zwieszaj^a "+
    "si^e w stron^e pod這^za obci^a^zone okaza造mi "+
    "kwiatami. Fioletowe, zros貫 p豉tki tworz^a kielich "+
    "z jasno^z^o速ym ^srodkiem. Odurzaj^acy zapach "+
    "dzwonk^ow le^snych wabi owady.\n";
    return str;
    }
    else
    {
    return 0;
    }
}

string opis_fiolkow()
{
    string str;
    if (pora_roku() == MT_WIOSNA)
    {
    str = "P這^z^aca si^e po ziemi bylina jest teraz g^esto "+
    "upstrzona pi^eknie pachn^acymi fioletowymi "+
    "kwiatami. Delikatne kwiaty wyniesione s^a nad "+
    "ziemie na wysokich szypu趾ach, ka^zdy z nich "+
    "sk豉da si^e z pi^eciu p豉tk^ow dw^och "+
    "skierowanych w g^or^e i trzech w d^o�. "+
    "Zielone listki, kt^ore dodatkowo eksponuj^a "+
    "pi^ekno koloru fio趾^ow, maj^a kszta速 sercowaty "+
    "i s^a lekko karbowane od g^ory.\n";
    return str;
    }
    else if (pora_roku() == MT_LATO)
    {
    str = "Na p這^z^acej si^e po ziemi bylinie nie wida^c "+
    "ju^z ^slad^ow niedawno pi^eknie kwitn^acych, "+
    "drobnych p豉tk^ow fio趾^ow. Z wonnej ro^sliny "+
    "pozosta造 這dy^zki i ciemnozielone, sercowate "+
    "li^scie, kt^ore t逝mi^a wszelkie odg這sy "+
    "zwierz^at i podr^o^znych.\n";
    return str;
    }
    else
    {
    return 0;
    }
}


string opis_plyt()
{
    string str;
    if (jest_dzien() == 1)
    {
        if (ZACHMURZENIE(TO) <= 2)
        {
        str =  "Bia貫, marmurowe p造ty odbijaj^a promienie "+
        "sloneczne, kt^ore zalewa ca訟a okolic^e. ";
        }
        else
        {
            str = "";
        }
    str += "Roztrzaskany kamie^n w wielu miejscach jest "+
    "porysowany i wy^z這biony przez czas i natur^e. "+
    "W niekt^orych miejscach p造ty zachodz^a "+
    "na siebie tworz^ac wiele miejsc o kt^ore "+
    "mo^zna si^e potkn^a^c.\n";
    return str;
    }
    else
    {
        if (ZACHMURZENIE(TO) <= 2)
        {
        str = "Bia貫, marmurowe p造ty odbijaj^a smugi blasku "+
        "ksiezyca, kt^ore zalewaja ca訟a okolic^e.";
        }
        else
        {
        str = "";
        }
    str += "Roztrzaskany kamie^n w wielu miejscach "+
    "wy^z這biony przez czas i natur^e. W niekt^orych "+
    "miejscach p造ty zachodz^a na siebie tworz^ac "+
    "wiele miejsc o kt^ore mo^zna si^e potkn^a^c.\n";
    return str;
    }
}

string opis_blokow()
{
    string str;
    str = "P^ekni^ete w wielu miejscach bloki marmuru musz^a "+
    "tu le^ze^c od bardzo dawna. Niegdysiejsze "+
    "cz^e^sci okaza造ch mur^ow le^z^a teraz bez豉dnie "+
    "porozrzucane po okolicy.\n";
    return str;
}

string opis_murow()
{
    string str;
    str = "Bia貫, pot^e^zne bloki marmuru ukszta速owane "+
    "niegdy^s w okaza訟a budowl^e obecnie s^a tylko "+
    "sm^etnymi resztkami jej dawnej ^swietno^sci. "+
    "W wielu miejscach pop^ekane i wyszczerbione "+
    "s^a ju^z jedynie zapomnianymi ruinami.\n";
    return str;
}
