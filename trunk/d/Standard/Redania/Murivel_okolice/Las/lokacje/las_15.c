/**
 * Las Komanda
 *
 * autor Cairell
 * opisy Edrain & Aladriel
 * data  styczen 2009
 */

#include <stdproperties.h>
#include <mudtime.h>
#include "dir.h"
inherit LAS_ELFOW_RZEKA;

string dlugi_opis();
string opis_wierzby();

void
create_las()
{
    set_short("Wierzba nad brzegiem rzeki.");
    set_long(&dlugi_opis());

    add_exit(LAS_ELFOW_LOKACJE+ "las_14.c","w");
    add_exit(TRAKT_LOKACJE+ "trakt_33.c","nw");
    add_exit(LAS_ELFOW_LOKACJE+ "las_13.c","se");
    add_exit(LAS_ELFOW_LOKACJE+ "las_17.c","ne");


    add_item(({"wierzb^e","p^lacz^ac^a wierzb^e", 
        "wierzb^e p^lacz^ac^a"}), &opis_wierzby());
}

string dlugi_opis()
{
    string str = ""; 

    if(pora_roku() == MT_ZIMA)
    {
        str += "Wolno p^lyn^aca rzeka tworzy delikatne zakole "+
            "rz^lobi^ac brzeg w wysmuk^ly ^luk, nad kt^orym g^ruje "+
            "pot^e^zna stara wierzba p^lacz^aca. Witki drzewa "+
            "dotykaj^a ziemi, p^lawi^acej si^e w puszystym "+
            "niekazitelnie bia^lym ^sniegu, sp^lywaj^ac niby "+
            "wodospad z korony. D^ugie trzciny porastaj^ace "+
            "brzeg szumi^a cicho przy najl^zejszym podmuchu "+
            "wiatru, tworz^ac melancholijn^a atmosfer^e.";
    }

    else //inne pory roku
    {
        str += "Wolno p^lyn^aca rzeka tworzy delikatne zakole "+
            "rz^lobi^ac brzeg w wysmuk^ly ^luk, nad kt^orym "+
            "g^oruje pot^e^zna stara wierzba p^lacz^aca. "+
            "Witki drzewa dotykaj� ziemi, sp^lywaj�c niby wodospad "+
            "z korony. D^lugie trzciny porastaj^ace brzeg szumi^a "+
            "cicho przy najl^zejszym podmuchu wiatru, tworz^ac "+
            "melancholijn^a atmosfer^e.";
    }

str+="\n";
return str;
}

string opis_wierzby()
{
    string strr;

    if (pora_roku() == MT_ZIMA)
    {
        strr = "Pot^e^zny pie^n i konary, stanowi^a ca^lkowite "+
            "przeciwie^nstwo do cieniutkich, d^lugich witek "+
            "zwieszaj^acych si^e w kierunku pokrytej ^sniegiem "+
            "ziemi. Dodaj^a one subtelno^sci i wdzi^eku tej "+
            "wisz^acej kurtynie, kt^ora dr^zy wraz z wiatrem.\n";
        return strr;
    }
    else
    {
        strr = "Pot^e^zny pie^n i konary, stanowi^a ca^lkowite "+
            "przeciwie^nstwo do cieniutkich, d^lugich witek "+
            "zwieszaj^acych si� w kierunku ziemi. Dodaj^a one "+
            "subtelno^sci i wdzi^eku tej wisz^acej kurtynie, "+
            "kt^ora dr^zy wraz z wiatrem.\n";
        return strr;
    }
}

public string
exits_description() 
{
    return "Drog^e mi^edzy drzewami mo^zna odnale^s^c ruszaj^ac "+
        "na p^o^lnocny wsch^od, zach^od, lub po^ludniowy zach^od, "+
        "za^s na p^olnocnycm zachodzie majaczy mi^edzy drzewami "+
        "trakt.\n";
}