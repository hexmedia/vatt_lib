#include "dir.h"

inherit REDANIA_STD;
#include "/d/Standard/Redania/dir.h"
#include <macros.h>
#include <mudtime.h>
#include <language.h>
#include <stdproperties.h>
#include <filter_funs.h>

string dlugi_opis();

void
create_bagienko()
{
}

nomask void
create_redania()
{
    set_long(&dlugi_opis());

    add_event("&event_lasu:"+file_name(TO)+"");

    set_event_time(300.0);

    add_sit("pod drzewem","pod jednym z drzew","spod drzewa",0);
    add_prop(ROOM_I_TYPE, ROOM_FOREST);
    add_prop(ROOM_I_INSIDE,0);

    dodaj_ziolo(({"czarny_bez-lisc.c",
                                "czarny_bez-kwiaty.c",
                                "czarny_bez-owoce.c"}));
        dodaj_ziolo("arcydziegiel.c");
        dodaj_ziolo("babka_lancetowata.c");
        dodaj_ziolo("piolun.c");

    add_prop(ROOM_I_WSP_Y, WSP_Y_LAS_ELFOW); //Wsp�rz�dne, do systemu pogodowego.
    add_prop(ROOM_I_WSP_X, WSP_X_LAS_ELFOW);

    create_bagienko();
}
public int
unq_no_move(string str)
{
    notify_fail("Podmok^la ziemia zapada si^e pod twoimi stopami, "+
        "zmuszaj^ac do zaniechania marszu w tym kierunku.\n");

    string vrb = query_verb();

    if(vrb ~= "g�ra" || vrb ~= "d�")
        notify_fail("Nie mo�esz tam si� uda�!\n");

    return 0;
}

string
event_lasu()
{
    switch (pora_dnia())
    {
        case MT_RANEK:
            return process_string(({"Na powierzchni bagna pojawi^l si^e "+
                "pojedynczy b^abel powietrza, p^ekaj^ac nieomal "+
                "bezd^xwi^ecznie.\n","Gdzie^s w trzcinach cichym rechotem "+
                "odzywaj^a si^e ^zaby.\n",
                "Promienie s^lo^nca pr^obuj^a rozproszyc panujacy p^o^lmrok, "+
                "przez chwil^e jest jakby ja^sniej...\n",
                "W oddali pracowity dzi^ecio^l wystukuje sw^oj rytm.\n",
            "@@pora_roku_dzien@@"})[random(4)]);
        case MT_POLUDNIE:
            return process_string(({"Na powierzchni bagna pojawi^l si^e "+
                "pojedynczy b^abel powietrza, p^ekaj^ac nieomal "+
                "bezd^xwi^ecznie.\n",
                "W oddali pracowity dzi^ecio^l wystukuje sw^oj rytm.\n",
            "@@pora_roku_dzien@@"})[random(2)]);
        case MT_POPOLUDNIE:
            return process_string(({"Promyki ^swiat^la nie^smia^lo "+
            "przedzieraj^a si^e przez ga^l^ezie.\n",
            "Promienie s^lo^nca pr^obuj^a rozproszyc panujacy p^o^lmrok, "+
            "przez chwil^e jest jakby ja^sniej...\n",
            "W oddali pracowity dzi^ecio^l wystukuje sw^oj rytm.\n",
            "@@pora_roku_dzien@@"})[random(3)]);
        default: // wieczor, noc
            return process_string(({"K^atem oka dostrzegasz jaki^s ruch, ale "+
            "gdy odwracasz g^low^e las jest cichy i pusty.\n",
            "Wrona z zapa^lem wyskrzekuje swoj^a pie^s^n.\n",
            "Masz wra^zenie, ^ze obserwuje ci^e jakie^s zwierz^e.\n",
            "W oddali s^lyszysz ryk dzikiego zwierz^ecia.\n",
            "Promienie ksi^e^zyca przebijaj� si^e przez mg^l^e, "+
            "rozja^sniaj^ac czarne barwy odrobin^a srebra.\n",
            "Gdzie^s w trzcinach cichym rechotem odzywaj^a si^e ^zaby.\n",
            "@@pora_roku_noc@@"})[random(6)]);
    }
}
string
pora_roku_dzien()
{
    switch (pora_roku())
    {
         case MT_LATO:
            return ({
                "Drzewa szumi^a ko^lysane wiatrem, pr^obuj�c ponurym "+
                "szeptem opowiedzie^c swoj^a smutn^a histori^e.\n",
                "Z g^estwiny nagle zrywa si^e ptak i z gwa^ltownym krzykiem "+
                "odlatuje w przestworza.\n",
                "Z g^l^ebi lasu dobiega ci^e melodyjny ^swiergot ptak^ow.\n",
                "Ciep^ly wiatr muska twoj^a twarz.\n","Drzewa wyci^agaj^a "+
                "swoje przegni^le ga^l^ezie w upiornym ge^scie.\n"
                })[random(3)];
         case MT_WIOSNA:
             return ({
                "Drzewa szumi^a ko^lysane wiatrem, pr^obuj�c ponurym "+
                "szeptem opowiedzie^c swoj^a smutn^a histori^e.\n",
                "Z g^estwiny nagle zrywa si^e ptak i z gwa^ltownym krzykiem "+
                "odlatuje w przestworza.\n",
                "Z g^l^ebi lasu dobiega ci^e melodyjny ^swiergot ptak^ow.\n",
                "Ciep^ly wiatr muska twoj^a twarz.\n","Drzewa wyci^agaj^a "+
                "swoje przegni^le ga^l^ezie w upiornym ge^scie.\n"
                })[random(3)];
         case MT_JESIEN:
             return ({
                "Bezlistne ga^l^ezie muskaj^a tw^oj policzek.\n",
                "Wysoko w g^orze wiatr porusza ga^leziami.\n",
                "Zimny wiatr muska ci^e po twarzy.\n",
                "Poruszane wiatrem ga^lezie szeleszcz^a suchymi li^s^cmi.\n",
                "Porwane przez wiatr li^s^cie zaczynaj^a unosi^c si^e ku "+
                "g^orze.\n"
                })[random(4)];
         case MT_ZIMA:
             return ({
                "Bezlistne ga^l^ezie muskaj^a tw^oj policzek.\n",
                "Zimny wiatr muska ci^e po twarzy.\n",
                "Lodowaty wiatr muska ci^e po twarzy.\n",
                "Wysoko w g^orze wiatr porusza ga^leziami.\n",
                "Niedaleko ciebie kupka ^sniegu spada z ga^l^ezi i upada na "+
                "ziemi^e.\n",
                "Jaka^s zagubiona wrona przysiad^la na ga^l^ezi i "+
                "kracze zapami^etale.\n",
                "Z ga^l^ezi spada grudka ^sniegu, "+
                "niemal trafiaj^ac ci^e w g^low^e.\n"
                })[random(6)];
    }
}
string
pora_roku_noc()
{
    switch (pora_roku())
    {
         case MT_LATO:
         case MT_WIOSNA:
             return ({"Li^scie drzew delikatnie poruszaj^a si^e na "+
             "wietrze.\n","Drzewa wyci^agaj^a swoje przegni^le ga^l^ezie "+
             "w upiornym ge^scie.\n",
             "Butwiej^acy dywan li^sci ugina si^e lekko pod twoim ci^e^zarem.\n"})
             [random(2)];
         case MT_JESIEN:
             return ({"Bezlistne ga^l^ezie muskaj^a tw^oj policzek.\n",
             "Wysoko w g^orze wiatr porusza ga^leziami.\n",
             "Zimny wiatr muska ci^e po twarzy.\n","Drzewa wyci^agaj^a "+
             "swoje przegni^le ga^l^ezie w upiornym ge^scie.\n",
             "Poruszane wiatrem ga^lezie szeleszcz^a suchymi li^s^cmi.\n",
             "Porwane przez wiatr li^s^cie zaczynaj^a unosi^c si^e ku "+
             "g^orze.\n"})
             [random(4)];
         case MT_ZIMA:
             return ({"Bezlistne ga^l^ezie muskaj^a tw^oj policzek.\n",
             "Z ga^l^ezi spada grudka ^sniegu, niemal "+
             "trafiaj^ac ci^e w g^low^e.\n","Zimny wiatr muska ci^e po "+
             "twarzy.\n","Lodowaty wiatr muska ci^e po twarzy.\n",
             "Wysoko w g^orze wiatr porusza ga^leziami.\n",
             "Niedaleko ciebie kupka ^sniegu spada z ga^l^ezi i upada na "+
             "ziemi^e.\n"})[random(4)];
    }
}
string dlugi_opis()
{
    return "B��d z opisem lokacji. Prosz� zg�o� to wklejaj�c t� wiadomo��.\n";
}
