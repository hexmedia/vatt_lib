/*
 * BYLO:
 *
 * Autor: Rantaur
 * Data 25.05.2006
 * Opis: Milutki golabek do postawienia na rynku, wcina pieczywo, mozna
 * go straszyc i wogle :P
 * Long pierwotnie napisany przez Tinardan (08.08.05) i
 * nieco przerobiony przez Rantaura.
 *
 * JEST:
 * Kura do wioski na zadupiu. Opis Faeve.
 */

inherit "/std/creature";
inherit "/std/act/action";
inherit "/std/combat/unarmed";

#include "dir.h"

#include <macros.h>
#include <pl.h>
#include <stdproperties.h>
#include <object_types.h>
#include <cmdparse.h>
#include <mudtime.h>

int nakarm(string str);
int przeplosz(string str);
int emoty(string str);
void remove_me();
void eat_it(object food, int all);
void random_przym(string str, int n);

string *jedzenie = ({"bu�ka", "bu^leczka", "chleb", "ciasto", "ciastko", 
    "ciasteczko", "placek", "piernik", "pierniczek", "kawa^lek chleba"});

void
create_creature()
{
  ustaw_odmiane_rasy("kura");

  random_przym("pstrokaty:pstrokaci bia^ly:biali czarny:czarni nakrapiany:nakrapiani||"
        +"niewielki:niewielcy t�usty:t�u�ci wychudzony:wychudzeni||m�ody:m�odzi"
        +" stary:starzy||ruchliwy:ruchliwi niemrawy:niemrawi", 2);

  set_long("Pi^orka kury poprzetykane s^a gdzieniegdzie be^zowymi i " +
      "czarnymi pasemkami. Do^s^c d^lugie, ^z^o^ltawe nogi pokrywa zgrubia^ly " +
      "nask^orek. Jako ptak nielot posiada niewielkie skrzyd^la, jednak i tak " +
      "co i rusz nimi wymachuje, jakby pr^obuj�c wzbi^c si^e do lotu lub po " +
      "prostu zwr^oci^c na siebie uwag^e.\n");
  add_prop(LIVE_I_NEVERKNOWN, 1);

  set_stats (({1, 35, 1, 5, 1}));
  set_hitloc_unarmed(0, ({1, 1, 1}), 40, "g�ow�", PL_ZENSKI);
  set_hitloc_unarmed(1, ({1, 1, 1}), 60, "cia�o", PL_MESKI_NOS_NZYW);

  add_prop(CONT_I_WEIGHT, random(200) + 1000);
  add_prop(CONT_I_VOLUME, query_prop(CONT_I_WEIGHT) + 200);
  add_prop(OBJ_M_NO_ATTACK, "@@zaatakowany@@");

  set_act_time(14);

  add_act("emote trzepocze skrzyd^lami, jakby chcia^la zerwa^c si^e do lotu.");
  add_act("emote gdaka w najlepsze.");
  add_act("emote dziobie w ziemi, szukaj^ac ziaren.");
  add_act("emote siada wygodnie na grz^edzie.");
  add_act("emote spaceruje ochoczo po kurniku.");

  set_alarm_every_hour(&remove_me());
}

init()
{
  ::init();
  add_action(&nakarm(), "nakarm");
  add_action(&przeplosz(), "przep�osz");
  add_action(&emoty(), "kopnij");
  add_action(&emoty(), "opluj");
  add_action(&emoty(), "poca�uj");
  add_action(&emoty(), "pociesz");
  add_action(&emoty(), "pog�aszcz");
  add_action(&emoty(), "pog�ad�");
  add_action(&emoty(), "przytul");
  add_action(&emoty(), "szturchnij");
}

void eat_it(object food, int all)
{
  if(food->query_type() != O_JEDZENIE)
    {
      write("Ale� "+food->query_nazwa(PL_MIA)+" nie nadaje si� do jedzenia!\n");
      return;
    }

  if(member_array(food->query_nazwa(), jedzenie) == -1)
    {
      if(all)
	{
	  write("Rzucasz kurom nieco "+food->short(PL_DOP)+". Zaciekawione"
		+" ptaki szybko przydreptuj� do jedzenia i dziobi� je kilka razy,"
		+" jednak po chwili rozchodz� si� pozostawiaj�c "+food->query_nazwa(3)
		+" niemal nietkni�t"+food->koncowka("y", "�", "e")+". Widocznie nie jest"
		+" to ich przysmak.\n");
	  saybb(QCIMIE(this_player(), PL_MIA)+" rzuca kurom nieco "+food->short(PL_DOP)
		+". Zaciekawione ptaki szybko przydreptuj� do jedzenia i dziobi� je kilka"
		+" razy, jednak po chwili rozchodz� si� pozostawiaj�c "+food->query_nazwa(3)
		+" niemal nietkni�t"+food->koncowka("y", "�", "e")+". Widocznie nie jest"
		+" to ich przysmak.\n");
	  return;
	}
      else
	{
	  write("Rzucasz "+this_object()->short(PL_CEL)+" nieco "+food->short(PL_DOP)
		+". Zaciekawiony ptak szybko przydreptuje do jedzenia i dziobie je kilka"
		+" razy, jednak po chwili odchodzi, rozgladaj�c sie za czym� innym. Widocznie "
		+food->query_nazwa(PL_MIA)+" nie jest przysmakiem kur.\n");
	  saybb(QCIMIE(this_player(), PL_MIA)+" rzuca "+this_object()->short(PL_CEL)
		+" nieco "+food->short(PL_DOP)+". Zaciekawiony ptak szybko przydreptuje do"
		+" jedzenia i dziobie je kilka razy, jednak po chwili odchodzi, rozgladaj�c"
		+" si� za czym� innym. Widocznie "+food->query_nazwa(PL_MIA)+" nie jest"
		+" przysmakiem kur.\n");
	  return;
	}
    }

  int amount = food->query_amount();
  if(all)
    {
      write("Rzucasz kurom nieco "+food->short(PL_DOP)+". Ptaki"
	    +" niemal natychmiast zbiegaj^a si� w jedno miejsce i przepychaj�c"
	    +" mi�dzy sob�, zaciekle walcz� o ka�dy k�s pokarmu. Po chwili"
	    +" na ziemi nie wida^c ju� ani �ladu rzucone"
	    +food->koncowka("go", "j")+" im "+food->query_nazwa(1)+".\n");
      saybb(QCIMIE(this_player(), PL_MIA)+" rzuca kurom nieco "
	    +food->short(PL_DOP)+". Ptaki niemal natychmiast zbiegaj^a si� w"
	    +"jedno miejsce i przepychaj�c mi�dzy sob�, zaciekle walcz� o ka�dy"
	    +" k�s pokarmu. Po chwili na ziemi nie wida^c ju� ani �ladu"
	    +" rzucone"+food->koncowka("go", "j")+" im "
	    +food->query_nazwa(1)+".\n");
    }
  else
    {
      write("Rzucasz "+ this_object()->short(PL_CEL)+" nieco "+
	    food->short(PL_DOP)+". Ptak �wawo przydreptuje do jedzenia i"
	    +" rado�nie pogdakuj^ac, zaczyna energicznie dzioba� "+
	    food->query_nazwa(3)+". Po chwili na ziemi nie wida� ju� ani"+
	    " jednego okruszka.\n");
      saybb(QCIMIE(this_player(), PL_MIA)+" rzuca "+this_object()->short(PL_CEL)
	    +" nieco "+food->short(PL_DOP)+". Ptak �wawo przydreptuje do"
	    +" jedzenia i rado�nie pogdakuj^ac, zaczyna energicznie dzioba� "
	    +food->query_nazwa(3)+". Po chwili na ziemi nie wida� ju� ani"
	    +" jednego okruszka.\n");
    }
  if(amount - 30 <= 0)
    food->remove_object();
  else
    food->set_amount(amount - 30);
}

int nakarm(string str)
{
  object food;
  object *obj;
  if(!str)
    {
      notify_fail("Nakarm kogo czym?\n");
      return 0;
    }

  if(!parse_command(lower_case(str), environment(this_player()),"%i:"+PL_BIE+" %o:"+ PL_NAR, obj, food))
    {
      notify_fail("Nakarm kogo czym?\n");
      return 0;
    }
	  obj = CMDPARSE_STD->normal_access(obj, 0, 0);
	  if(sizeof(obj) == 0)
	    {
	      notify_fail("Kt�r^a kur^e chcesz nakarmi�?\n");
	      return 0;
	    }

	  if(sizeof(obj) > 1)
	    {
	      eat_it(food, 1);
	      return 1;
	    }

	  if(obj[0] == this_object())
	    {
	      eat_it(food, 0);
	      return 1;
	    }
	  else return 0;
}

int przeplosz(string str)
{
  if(!str)
    {
      notify_fail("Co chcesz przep^loszy�?\n");
      return 0;
    }
  mixed golebie;
  if(!parse_command(lower_case(str), environment(this_player()), "%i:"+PL_BIE, golebie))
    {
      notify_fail("Co chcesz przep^loszy�?\n");
      return 0;
    }
  golebie = CMDPARSE_STD->normal_access(golebie, 0, 0);
  int szansa = random(101);
  if(szansa < 40)
    {
      if(sizeof(golebie) > 1)
	{
	  write("Tupi�c g�o�no, podbiegasz do kr^ec^acych si^e po kurniku"
		+" kur. Wystraszone ptaki natychmiast zaczynaj^a g^lo^sno gdaka^c" +
        " i biega^c w k^o^lko, po czym jak gdyby nigdy nic wracaj^a do " +
        "swoich zaj^e^c.\n");
	  saybb(QCIMIE(this_player(), PL_MIA)+" g�o�no tupi�c podbiega do kr^ec^acych si^e po kurniku"
		+" kur. Wystraszone ptaki natychmiast zaczynaj^a g^lo^sno gdaka^c" +
        " i biega^c w k^o^lko, po czym jak gdyby nigdy nic wracaj^a do " +
        "swoich zaj^e^c.\n");
	  return 1;
	}
      if(golebie[0] == this_object())
	{

	    {
	      write("Tupi�c g�o�no, podbiegasz do "+this_object()->short(PL_DOP)
		    +". Wystraszony ptak natychmiast zaczyna g^lo^sno gdaka^c" +
        " i biega^c w k^o^lko, po czym jak gdyby nigdy nic wraca do " +
        "swoich zaj^e^c.\n");
	      saybb(QCIMIE(this_player(), PL_MIA)+" g�o�no tupi�c podbiega do "
		    +this_object()->short(PL_DOP)+". Wystraszony ptak natychmiast " +
            "zaczyna g^lo^sno gdaka^c" +
        " i biega^c w k^o^lko, po czym jak gdyby nigdy nic wraca do " +
        "swoich zaj^e^c.\n");
	      return 1;
	    }

	} else return 0;
    }
  else // Jezeli nie przestraszylismy...
    {
      if(sizeof(golebie) > 1)
	{
	  write("Tupi�c g�o�no, podbiegasz do kr^ec^acych si^e po kurniku"
		+" kur, te jednak uciekaj� zaledwie o kilka krok^ow i po chwili wracaj� do swoich zaj��.\n");
	  saybb(QCIMIE(this_player(), PL_MIA)+" g�o�no tupi�c podbiega"
		+" do kr^ec^acych si^e po kurniku kur, te jednak uciekaj� od "
		+this_player()->koncowka("niego", "niej")+" zaledwie o kilka"
		+" krok^ow, by za chwil^e zupe�nie "+this_player()->koncowka("go", "j�")
		+" zignorowa^c i powr�ci� do swoich zaj��.\n");
	  return 1;
	}
      if(golebie[0] == this_object())
	{
	  write("Tupi�c g�o�no podbiegasz do "+this_object()->short(PL_DOP)
		+", ta odwraca si� gwa�townie w twoj� stron�, jednak najwyra�niej"
		+" jest ju� przyzwyczajona do takich dziwnych zachowa�, gdy� po"
		+" chwili, zupe�nie ci� ignoruj�c, wraca do swoich zaj��.\n");
	  saybb(QCIMIE(this_player(), PL_MIA)+" g�o�no tupi�c podbiega do "
		+this_object()->short(PL_DOP)+", ta odwraca sie gwa�townie w "
			+this_player()->koncowka("jego", "jej")+" stron�, jednak"
		+" najwyra�niej ptak jest ju� przyzwyczajony do takich dziwnych"
		+" zachowa�, gdy� po chwili, zupe�nie ignoruj�c "
		+QCIMIE(this_player(), PL_BIE)+" wraca do swoich zaj��.\n");
	  return 1;
	} else return 0;
    }
}

int emoty(string str)
{
  object golab;
  if(!str)
    return 0;
  if(!parse_command(lower_case(str), environment(this_player()), "%o:"+PL_BIE, golab))
    return 0;
  if(golab != this_object())
    return 0;
  string verb = query_verb();
  string *verb_array = ({"kopnij","poca�uj","pociesz","pog�ad�","pog�aszcz","przytul","szturchnij"});
  if(verb == "opluj")
    {
      int i = random(10);
      if(i > 7)
	{
	  return 0;
	}
      else
	{
	  write("Pr�bujesz oplu� kur^e, ale ta w ostatniej chwili uskakuje przed tob�.\n");
	  saybb(QCIMIE(this_player(), PL_MIA)+" pr�buje oplu� kur^e, ale ta w ostatniej chwili odskakuje od "+this_player()->koncowka("niego", "niej")+".\n");
	  return 1;
	}
    }
  if(member_array(verb, verb_array) != -1)
    {
      write("Pr�bujesz zbli�y� si� do "+this_object()->short(PL_DOP)+", ale ta natychmiast od ciebie odskakuje.\n");
      return 1;
    }
}

string zaatakowany()
{
  set_alarm(0.1, 0.0, &write("Pr�bujesz zaatakowa� "+
			     this_object()->short(PL_BIE)+", lecz ta"
			     +" w ostatniej chwili zaczyna op^eta^nczo trzepota^c skrzyd^lami i wzbiwszy si^e na kilka st^op w g^or^e,"
			     +" przysiada ci^e^zko na grz^edzie.\n"));
  saybb(capitalize(this_object()->short(PL_MIA))+" w ostatniej chwili zaczyna op^eta^nczo trzepota^c skrzyd^lami i wzbiwszy si^e na kilka st^op w g^or^e,"
			     +" przysiada ci^e^zko na grz^edzie.\n");
  return "";
}

void remove_me()
{
  if(environment(this_object())->dzien_noc())
    this_object()->remove_object();
}

void random_przym(string str, int n)
{
	mixed* groups = ({ });
        groups = explode(str, "||");
        int groups_size = sizeof(groups);
        if(n > groups_size)
                return;
        for(int i =0; i < groups_size; i++)
                groups[i] = explode(groups[i], " ");

        for(int a = 0; a < n; a++)
	{
                int group_index = random(sizeof(groups));
                int adj_index = random(sizeof(groups[group_index]));
                string* tmp = explode(groups[group_index][adj_index], ":");
                dodaj_przym(tmp[0], tmp[1]);
                groups = groups-({groups[group_index]});
        }
}

public string
query_auto_load()
{
    return 0;
}
