/**
 * \file /d/Standard/Redania/Wioska_BM/npc/krowa.c
 *
 * Krowa dojna, wersja z mlekiem
 *
 * @author Enkin
 * @date do 3 listopada 2007
 */

inherit "/std/zwierze";
inherit "/std/act/action";
inherit "/std/act/trigaction.c";

#pragma unique

#include <macros.h>
#include <stdproperties.h>
#include <money.h>
#include <pl.h>
#include <formulas.h>
#include <std.h>
#include <ss_types.h>
#include <wa_types.h>
#include <object_types.h>
#include <composite.h>
#include <filter_funs.h>
#include <cmdparse.h>
#include "dir.h"

object  dojona = 0;
object  pojemnik, dojenie, pastuch = 0;
int     alarm_id;
int     zdenerwowanie = 0;

int     Dojenia = 0;

void
red_Dojenia()
{
    Dojenia -= 1;
}

void dojenie_alarm();

void create_zwierze()
{
    add_leftover("/std/skora", "sk^ora", 1, 0, 1, 1, 9 + random(5), O_SKORY);
    add_leftover("/std/leftover", "z^ab", 2+random(4), 0, 1, 0, 0, O_KOSCI);

    ustaw_odmiane_rasy("krowa");
    set_gender(G_FEMALE);

    set_long("Czarna krowa w niewielkie bia^le ^laty ot^epia^lym wzrokiem "+
        "wpatruje si^e w otoczenie. Wok^o^l du^zego czarnego ^lba zwierz^ecia "+
        "lataj^a niesforne muchy, kt^ore uciekaj^a za ka^zdym razem, gdy "+
        "krowa poruszy uszami. Kr^otka ^laciata sier^s^c pokrywa jej ko^scisty "+
        "tu^l^ow, pod kt^orym dyndaj^a r^o^zowe wymiona.\n");

    dodaj_przym("stary", "starzy");
    dodaj_przym("^laciaty", "^laciaci");

    set_act_time(30);
    add_act("emote muczy.");
    add_act("emote rozgl^ada si^e powoli.");
    add_act("emote ^zuje co^s w pysku.");
    add_act("emote muczy g^lo^sno.");
    add_act("emote odgania muchy ogonem.");
    add_act("emote potrz�sa ^lbem.");

    set_stats ( ({ 30, 7, 30, 11, 8 }) );

    set_skill(SS_DEFENCE, 10 + random(4));
    set_skill(SS_UNARM_COMBAT, 15 + random(5));

    set_attack_unarmed(0, 5, 5, W_IMPALE,   50, "rogi");
    set_attack_unarmed(1, 5, 5, W_BLUDGEON, 20, "kopyto", ({"lewy",  "przedni"}));
    set_attack_unarmed(2, 5, 5, W_BLUDGEON, 20, "kopyto", ({"prawy", "przedni"}));
    set_attack_unarmed(3, 5, 5, W_BLUDGEON,  5, "kopyto", ({"lewy",  "tylny"}));
    set_attack_unarmed(4, 5, 5, W_BLUDGEON,  5, "kopyto", ({"prawy", "tylny"}));

    set_hitloc_unarmed(0, ({ 1, 1, 1 }), 10, "^leb");
    set_hitloc_unarmed(1, ({ 1, 1, 1 }), 30, "tu^l^ow");
    set_hitloc_unarmed(2, ({ 1, 1, 1 }), 20, "noga", ({"przedni", "lewy"}));
    set_hitloc_unarmed(3, ({ 1, 1, 1 }), 20, "noga", ({"przedni", "prawy"}));
    set_hitloc_unarmed(4, ({ 1, 1, 1 }), 10, "noga", ({"lewy", "tylny"}));
    set_hitloc_unarmed(5, ({ 1, 1, 1 }), 10, "noga", ({"prawy", "tylny"}));

    add_prop(LIVE_I_NEVERKNOWN, 1);
    add_prop(CONT_I_WEIGHT, 500 + random(100));
    add_prop(CONT_I_HEIGHT, 135 + random(10));

    ustaw_stan_oswojenia(11); //Krowa domowa wi^ec oswojona.
}

int query_krowa()
{
    return 1;
}

void emote_hook(string emote, object wykonujacy, string przyslowek)
{
    switch (emote)
    {
        case "pog^laszcz":
            zdenerwowanie = ftoi(sqrt(zdenerwowanie));
            if (wykonujacy->query_skill(SS_ANI_HANDL) > 40)
            pastuch = wykonujacy;
            break;

        case "kopnij":
            set_alarm(1.0, 0.0, "zly", wykonujacy);
            break;

        case "szturchnij":
            set_alarm(1.0, 0.0, "taki_sobie", wykonujacy);
            break;
    }
}

void zly(object kto)
{
    switch (random(5))
    {
        case 0:
            command("emote muczy gro^xnie."); break;
        case 1:
            command("emote muczy g^lo^sno."); break;
        case 2:
            command("emote odwraca swoj^a du^z^a g^low^e w twoj^a stron^e."); break;
        case 3:
            command("emote ryczy g^lo�no."); break;
        case 4:
            if (random(20) < zdenerwowanie)
                command("zaatakuj " + OB_NAME(kto));
    }

    zdenerwowanie += 3;
}

void taki_sobie(object kto)
{
    switch (random(3))
    {
        case 0:
            command("emote obraca swoj^a du^z^a g^low^e w twoj^a stron^e."); break;
        case 1:
            command("emote ^zuje siano, nie zwracaj^ac na nikogo najmniejszej uwagi."); break;
        case 2:
            command("emote muczy.");
    }

    zdenerwowanie++;
}

void zakoncz_dojenie()
{
    dojenie->stop_paralyze();
    dojona = 0;
}

void attacked_by(object wrog)
{
    ::attacked_by(wrog);
    zdenerwowanie += 10;
    dojona->write("Przestajesz doi^c krow^e.\n");
    zakoncz_dojenie();
}

int wydoj(string cmd)
{
    mixed *obj;

    if (!cmd || !parse_command(lower_case(cmd), environment(this_player()), "%l:" + PL_BIE + " 'do' %o:" + PL_DOP, obj, pojemnik))
    {
        notify_fail("Wyd^oj krow^e do czego?\n");
        return 0;
    }

    obj = NORMAL_ACCESS(obj, 0, 0);

    if (sizeof(obj) > 1)
    {
        notify_fail("Chyba ^latwiej doi^c krowy pojedynczo" + sizeof(obj) + ".\n");
        return 0;
    }

    if (obj[0] != TO)
        return 0;

    if (environment(pojemnik) != TP)
    {
        notify_fail("Naj^latwiej wydoi^c krow^e do pojemnika, kt^ory masz przy sobie.\n");
        return 0;
    }

    if (pojemnik->query_beczulka() == 0)
    {
        write(capitalize(pojemnik->short(TP, PL_DOP)) + " nie da si^e nape^lni^c mlekiem.\n");
        return 1;
    }

    if (pojemnik->query_ilosc_plynu() != 0)
    {
        write("W " + pojemnik->short(TP, PL_MIE) + " znajduje si^e " +
            (pojemnik->query_ilosc_plynu() > 1000 ? "pe^lno " : "nieco ") + pojemnik->query_opis_plynu() + ".\n");
        return 1;
    }

    if (dojona != 0)
    {
        write(capitalize(short(TP, PL_MIA)) + " jest w tej chwili dojona przez " + QIMIE(dojona, PL_BIE) + ".\n");
        return 1;
    }

    if (query_attack() != 0)
    {
        write("Krowa jest w tej chwili atakowana.\n");
        return 1;
    }

    if(!HAS_FREE_HANDS(TP) && !pojemnik->query_wielded())
    {
        write("Musisz mie^c wolne r^ece, by m^oc to zrobi^c.\n");
        return 1;
    }

    if ((random(20) + TP->query_skill(SS_ANI_HANDL) < zdenerwowanie && random(4) != 0) || Dojenia > 2)
    {
        switch (random(3))
        {
        case 0:
            write("Pr^obujesz chwyci^c wymiona " + short(TP, PL_DOP) +
                " jednak ta w ostatniej chwili rusza par^e krok^ow do przodu, niwecz^ac twoje plany.\n");
            saybb(QIMIE(TP, PL_MIA) + " pr^obuje chwyci^c wymiona " + short(TP, PL_DOP) +
                " jednak ta w ostatniej chwili rusza par^e krok^ow do przodu, niwecz^ac " +
                TP->koncowka("jego", "jej") + " plany.\n");
            zdenerwowanie++;
            break;
        case 1:
            write("Pr^obujesz zbli^zy^c si^e do " + short(TP, PL_DOP) + ", jednak ta muczy g^lo�no " +
                "i ucieka.\n");
            saybb(QIMIE(TP, PL_MIA) + " pr^obuje zbli^zy^c si^e do " + short(TP, PL_DOP) + ", jednak " +
                "ta muczy g^lo�no i ucieka.\n");
            zdenerwowanie++;
            break;
        case 2:
            write("Gdy pr^obujesz wydoi^c " + short(TP, PL_BIE) + ", rozjuszone zwierz^e wydaje z siebie " +
                "g^lo�ny ryk i ucieka.\n");
            saybb("Gdy " + QIMIE(TP, PL_MIA) + " pr^obuje wydoi^c " + short(TP, PL_BIE) + ", rozjuszone " +
                "zwierz^e wydaje z siebie g^lo�ny ryk i ucieka.\n");
            zdenerwowanie++;
        }
    }
    else if (random(100) < TP->query_skill(SS_ANI_HANDL) + 30)
    {
        write("Umiej^etnie zbli^zasz si^e do " + short(TP, PL_DOP) + ", chwytasz j� za wymiona i zaczynasz doi^c.\n");
        saybb(QIMIE(TP, PL_MIA) + " umiej^etnie zbli^za si^e do " + short(TP, PL_DOP) + ", chwyta j� za " +
            "wymiona i zaczyna doi^c.\n");

        Dojenia += 1;
        set_alarm(12800.0, 0.0, red_Dojenia);
        dojona = TP;

        pojemnik->set_vol(0);
        pojemnik->set_opis_plynu("�wie^zego mleka prosto od krowy");
        pojemnik->set_nazwa_plynu_dop("mleka");
        pojemnik->set_ilosc_plynu(1);

        dojenie = clone_object("/std/paralyze");
        dojenie->set_fail_message(QIMIE(TP, PL_MIA) + " przestaje doi^c " + short(TP, PL_BIE) + ".\n");
        dojenie->set_standard_paralyze("doi^c");
        dojenie->set_stop_fun("koniec_dojenia");
        dojenie->set_stop_object(TO);
        dojenie->move(TP);

        alarm_id = set_alarm(4.0, 0.0, dojenie_alarm);
    }
    else
    {
        switch (random(3))
        {
        case 0:
            write("Pr^obujesz chwyci^c wymiona " + short(TP, PL_DOP) +
                " jednak ta w ostatniej chwili rusza par^e krok^ow do przodu, niwecz^ac twoje plany.\n");
            saybb(QIMIE(TP, PL_MIA) + " pr^obuje chwyci^c wymiona " + short(TP, PL_DOP) +
                " jednak ta w ostatniej chwili rusza par^e krok^ow do przodu, niwecz^ac " +
                TP->koncowka("jego", "jej") + " plany.\n");
            zdenerwowanie++;
            break;
        case 1:
            write("Nieumiej^etnie chwytasz wymiona " + short(TP, PL_DOP) +
                " i pr^obujesz je ^sciska^c i ci^agn^a^c na wszystkie strony, jednak" +
                " niewzruszona krowa nie daje ani kropli mleka.\n");
            saybb(QIMIE(TP, PL_MIA) + " nieumiej^etnie chwyta wymiona " + short(TP, PL_DOP) +
                " i pr^obuje je ^sciska^c i ci^agn^a^c na wszystkie strony, jednak" +
                " niewzruszona krowa nie daje ani kropli mleka.\n");
            zdenerwowanie++;
            break;
        case 2:
            write("Zbli^zasz si^e z " + pojemnik->short(TP, PL_NAR) + " w r^eku do " + short(TP, PL_DOP) +
                " chc^ac j^a wydoi^c, jednak zirytowana twoj^a opiesza^lo^sci^a i nieporadno^sci^a krowa" +
                " zaczyna g^lo^sno mucze^c i mocno kopie kopytem " + pojemnik->short(TP, PL_BIE) + ", kt^or" +
                pojemnik->koncowka("y", "a", "e") + " l^aduje par^e s^a^zni dalej.\n");
            saybb(QIMIE(TP, PL_MIA) + " zbli^za si^e z " + pojemnik->short(TP, PL_NAR) + " w r^eku do" +
                short(TP, PL_DOP) + " chc^ac j^a wydoi^c, jednak zirytowana " + TP->koncowka("jego", "jej") +
                " opiesza^lo^sci^a i nieporadno^sci^a krowa zaczyna g^lo^sno mucze^c i mocno kopie kopytem " +
                pojemnik->short(TP, PL_BIE) + ", kt^or" + pojemnik->koncowka("y", "a", "e") + " l^aduje par^e" +
                " s^a^zni dalej.\n");
            pojemnik->move(environment(TP));
            zdenerwowanie += 2;
        }
    }

    return 1;
}

void init()
{
    ::init();

    add_action(&wydoj(), "wyd^oj");
}

void koniec_dojenia()
{
    //write("Przestajesz doi^c " + short(TP, PL_BIE) + ".\n");
    //saybb(QIMIE(TP, PL_MIA) + " przestaje doi^c " + short(TP, PL_BIE) + ".\n");
    remove_alarm(alarm_id);
    dojona = 0;
}

void wylanie_mleka()
{
    string zachowanie = (random(2) ? "g^lo�no mucze^c" : "nerwowo si^e porusza^c");
    string wywraca = (random(2) ? "przewraca " : "wywraca ");
    write(capitalize(short(TP, PL_MIA)) + " nagle zaczyna " + zachowanie + " i " +
        wywraca + pojemnik->short(TP, PL_BIE) + ", wylewaj�c ca^le " +
        "mleko na ziemi^e.\n");
    saybb("Dojona przez " + QIMIE(dojona, PL_BIE) + " krowa nagle zaczyna " +
        zachowanie + " i " + wywraca + pojemnik->short(TP, PL_BIE) +
        ", wylewaj�c ca^le mleko na ziemi^e.\n");
    pojemnik->move(environment(TP));
    pojemnik->set_ilosc_plynu(0);
    zdenerwowanie += 2;
    zakoncz_dojenie();
}

void dojenie_alarm()
{
	if (random(100) < zdenerwowanie)
	{
		wylanie_mleka();
		return;
	}
	else if (random(60) > dojona->query_skill(SS_ANI_HANDL) + 30)
	{
		switch (random(3))
		{
		case 0:
			write("Zdenerwowana twoimi nieporadnymi ruchami krowa wierzga nerwowo kopytami.\n");
			saybb(capitalize(short(TP, PL_MIA)) + " zdenerwowana nieporadnymi ruchami " +
				QIMIE(dojona, PL_MIA) + " wierzga nerwowo kopytami.\n");
			pojemnik->set_ilosc_plynu(pojemnik->query_ilosc_plynu() + 200);
			zdenerwowanie++;
			break;
		case 1:
			write("Z ca^lych si^l uciskasz wymiona " + short(TP, PL_DOP) + ", jednak sp^lywa z nich " +
				"ledwo par^e kropel mleka.\n");
			saybb(QIMIE(dojona, PL_MIA) + " z ca^lych si^l uciska wymiona " + short(TP, PL_DOP) +
				", jednak sp^lywa z nich ledwo par^e kropel mleka.\n");
			pojemnik->set_ilosc_plynu(pojemnik->query_ilosc_plynu() + 10);
			zdenerwowanie += 2;
			break;
		case 2:
			wylanie_mleka();
			return;
		}
	}
	else
	{
		switch (random(4))
		{
		case 0:
			write("Ci�gniesz mocno " + short(TP, PL_BIE) + " za wymiona, z kt^orych " +
				"leje si^e �wie^ze mleko prosto do " + pojemnik->short(TP, PL_DOP) + ".\n");
			saybb(QIMIE(dojona, PL_MIA) + " ci�gnie mocno " + short(TP, PL_BIE) +
				" za wymiona, z kt^orych leje si^e �wie^ze mleko prosto do " +
				pojemnik->short(TP, PL_DOP) + ".\n");
			break;
		case 1:
			write("Sprawnie uciskasz wymiona " + short(TP, PL_DOP) +
				", z kt^orych leje si^e ciep^le mleko.\n");
			saybb(QIMIE(dojona, PL_MIA) + " sprawnie uciska wymiona " +
				short(TP, PL_DOP) + ", z kt^orych leje si^e mleko.\n");
			break;
		case 2:
			write("Z wpraw� doisz " + short(TP, PL_BIE) + ", kt^orej mleko �cieka do " +
				pojemnik->short(TP, PL_DOP) + ".\n");
			saybb(QIMIE(dojona, PL_MIA) + " z wpraw� doi " + short(TP, PL_BIE) +
				", kt^orej mleko �cieka do " + pojemnik->short(TP, PL_DOP) + ".\n");
			break;
		case 3:
			write("Z zapa^lem doisz " + short(TP, PL_BIE) + ".\n");
			saybb(QIMIE(dojona, PL_MIA) + " z zapa^lem doi " + short(TP, PL_BIE) + ".\n");
			break;
		}

		pojemnik->set_ilosc_plynu(pojemnik->query_ilosc_plynu() + 400);
	}

	if (pojemnik->query_ilosc_plynu() >= pojemnik->query_pojemnosc())
	{
		write("Nape^lniwszy " + pojemnik->short(TP, PL_BIE) + " mlekiem" +
			" przestajesz doi^c " + short(TP, PL_BIE) + ".\n");
		saybb(QIMIE(TP, PL_MIA) + " nape^lniwszy " + pojemnik->short(TP, PL_BIE) +
			" mlekiem przestaje doi^c " + short(TP, PL_BIE) + ".\n");
		pojemnik->set_ilosc_plynu(pojemnik->query_pojemnosc());
		zakoncz_dojenie();
	}
	else
		alarm_id = set_alarm(4.0, 0.0, dojenie_alarm);
}

