#include "dir.h"

inherit WIOSKA_BM_STD;

#include <stdproperties.h>
#include <mudtime.h>
#include <pogoda.h>
#include <macros.h>

/*STRUMYK DO ZABRANIA. JEGO TU NIE MA BY�.
string
strum()
{
 string str = "Na po^ludniu " +
    "wida^c wst^a^zk^e ";

    if (TEMPERATURA(TO) < 0)
        str += "skutego lodem ";

    str += "strumienia oddzielaj^acego wiosk^e od traktu.\n";

    return str;
}*/

void
create_wioska()
{
}

nomask void
create_wioska_bm()
{
    set_long("@@dlugi_opis@@");
    set_polmrok_long("@@opis_nocy@@");
    add_sit("na ziemi", "na ziemi", "z ziemi", 0);

 // add_prop(ROOM_I_TYPE, jaki?);

    create_wioska();

    add_prop(ROOM_I_WSP_Y, WSP_Y_WIOSKA_BM);	//bezposrednio na W od Rinde...
    add_prop(ROOM_I_WSP_X, WSP_X_WIOSKA_BM);

    add_item(({"budynki", "chaty", "chatki", "budynki gospodarcze",
    "wiosk^e"}), "Dooko^la roztacza si^e widok ubogich, wiejskich chat.\n");
    add_item(({"rzek^e", "rzeczk^e", "strumie^n", "strumyk"}), "@@strum@@");

    set_event_time(150.0);

    add_event("Z oddali dobiega ci^e ujadanie psa.\n");
    if (!jest_dzien()) add_event("Tu^z obok ciebie przelecia^l nietoperz.\n");
    else add_event("W powietrzu rozlega si^e ^spiew s^lowika.\n");
    if (CO_PADA(TO) == 1) add_event("Padaj^acy deszcz powoli zmienia drog^e w b^loto.\n");
    if (CO_PADA(TO) == 2) add_event("Padaj^acy ^snieg pokrywa domy warstw^a bia^lego puchu.\n");

	/*STRUMYK DO ZABRANIA. JEGO TU NIE MA BY�.
    if (TEMPERATURA(TO) >= 0 && ZACHMURZENIE(TO) < 3 && jest_dzien())
    add_event("Promienie s^loneczne ta^ncz^a na powierzchni strumienia na po^ludniu.\n");*/
    if (pora_roku() != MT_ZIMA && CO_PADA(TO) == 0 && TEMPERATURA(TO) >= 10 && pora_dnia() - 15 / 3 == 0)
    add_event("Gdzie^s w trawie koncertuj^a ^swierszcze.\n");
    if (CZY_JEST_SNIEG(TO)) add_event("Pod warstw^a bia^lego puchu wioska wygl^ada jak z ba^sni.\n");
    if (!jest_dzien() && wildmatch("[34567890]", file_name(TO)) && MOC_OPADOW(TO) < 2)
    add_event("Kot miauczy g^lo^sno na dachu kt^orego^s z budynk^ow.\n");
    add_event("Z okolicznych p^ol sp^loszy^lo si^e stado wron.\n");
}
