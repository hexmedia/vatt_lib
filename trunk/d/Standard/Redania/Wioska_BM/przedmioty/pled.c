#include <stdproperties.h>
#include <materialy.h>
#include <object_types.h>
#include "dir.h"

inherit "/std/object";

create_object()
{
    ustaw_nazwe("pled");
    dodaj_przym("we^lniany", "we^lniani");
    
    set_long("Szary, poplamiony pled uszyty z dw^och warstw bardzo " +
    "grubej owczej we^lny. Mimo i^z miejscami jest popruty i dziurawy, " +
    "wci^a^z zdaje si^e stanowi^c niez^l^a ochron^e przed ch^lodem zar^ow" +
    "no jako dodatkowe okrycie dla zzi^ebni^etych w^edrowc^ow, jak i os^ob " +
    "nocuj^acych pod go^lym niebem.\n");
    
    ustaw_material(MATERIALY_WELNA);
    
    add_prop(OBJ_I_WEIGHT, 4000);
    add_prop(OBJ_I_VOLUME, 6000);
    add_prop(OBJ_I_VALUE, 26);
    
    set_type(O_INNE);
}
