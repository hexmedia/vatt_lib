/* Jajo do kurnika wioski przy BM.
   autorzy: gjoef i vera ;)
 */

#include <macros.h>
#include <stdproperties.h>
#include "dir.h"

inherit "/lib/keep";
inherit "/std/object";

int Zbuk = 86400;
int zbuk_alarm;

void
create_object()
{
    ustaw_nazwe("jajko");

    dodaj_przym("kurzy", "kurzy");

    set_long("Prawdziwe kurze jajko prosto z wiejskiej hodowli.\n");

    add_prop(OBJ_I_WEIGHT, 54 + random(12));
    add_prop(OBJ_I_VOLUME, query_prop(OBJ_I_WEIGHT) + 10);

    set_keep(1);
}

void
zbuk()
{
    Zbuk = 0;
}

void
set_zbuk(int i)
{
    Zbuk = i;
    remove_alarm(zbuk_alarm);
    zbuk_alarm = set_alarm(itof(i), 0.0, &zbuk());
}

int
query_zbuk()
{
    return Zbuk;
}

void
enter_env(object dest, object old)
{
    if (Zbuk < 86400 || (interactive(dest) && random(2)))
        zbuk_alarm = set_alarm(itof(Zbuk), 0.0, &zbuk());
}

void
grrr(object dest,object player)
{
    player->catch_msg("Starasz si^e wyj^a^c kurze jajko " +
        "z " + QSHORT(dest, 1) + ", jednak nieszcz^e^sliwie p^ek^a ci " +
        "w d^loni, zalepiaj^ac wszystko g^est^a mazi^a.\n");
    saybb(QCIMIE(player, 0) + " stara si^e wyj^a^c kurze jajko " +
        "z " + QSHORT(dest, 1) + ", jednak nieszcz^e^sliwie p^ek^a " +
    player->koncowka("mu", "jej") + " w d^loni, zalepiaj^ac wszystko g^est^a mazi^a.\n");

    destruct();
    return;

}

void
leave_env(object dest, object old)
{

    if (dest->query_name() ~= "plecak" ||
        dest->query_name() ~= "worek" ||
        dest->query_name() ~= "woreczek" ||
        dest->query_name() ~= "sakwa" ||
        dest->query_name() ~= "sakiewka" ||
        dest->query_name() ~= "torebka" ||
        dest->query_name() ~= "torba")
    {
        set_alarm(0.1,0.0,"grrr",dest,TP);
    }

}

void
throw_effect(object cel)
{
    if(cel->query_prop(ROOM_I_IS))
    {
        int room_type = ENV(ENV(TO))->query_prop(ROOM_I_TYPE);
        if(room_type & ROOM_IN_WATER != ROOM_IN_WATER               &&
           room_type & ROOM_UNDER_WATER != ROOM_UNDER_WATER         &&
           room_type & ROOM_IN_AIR != ROOM_IN_AIR                   &&
           room_type & ROOM_SWAMP != ROOM_SWAMP)
        {
            if(Zbuk)
                tell_room(cel,"Kurze jajko rozbija si� z plaskiem.\n");
            else
                tell_room(cel,"Kurze jajko rozbija si� z plaskiem, "+
                "roztaczaj�c dooko�a straszny od�r.\n");
                destruct();
        }
    }
    else
    {
        if (Zbuk)
        {
            saybb("Kurze jajko trafia w " + QIMIE(cel, 3) +
                ", a lepka ma^x znajduj" +
                "^aca si^e w ^srodku powoli si^e po ni" + cel->koncowka("m", "ej","im") +
                " rozp^lywa.\n", ({cel,TP}));
            TP->catch_msg("Kurze jajko trafia w " + QIMIE(cel, 3) +
                ", a lepka ma^x znajduj" +
                "^aca si^e w ^srodku powoli si^e po ni" + cel->koncowka("m", "ej","im") +
                " rozp^lywa.\n");
            cel->catch_msg("Kurze jajko trafia ci^e, a lepka ma^x znajduj" +
                "^aca si^e w ^srodku powoli si^e rozp^lywa si^e po twoim ubraniu.\n");
        }
        else
        {
            saybb("Kurze jajko trafia w " + QIMIE(cel, 3) + ", a mocno ju^z zepsuta " +
                "lepka ma^x znajduj^aca si^e w ^srodku powoli si^e po ni" +
                cel->koncowka("m", "ej","im") +
                " rozp^lywa, roztaczaj^ac dooko^la straszny od^or.\n", ({cel,TP}));
            TP->catch_msg("Kurze jajko trafia w " + QIMIE(cel, 3) + ", a mocno ju^z "+
                "zepsuta lepka ma^x znajduj^aca si^e w ^srodku powoli si^e po ni" +
                cel->koncowka("m", "ej","im") +
                " rozp^lywa, roztaczaj^ac dooko^la straszny od^or.\n");
            cel->catch_msg("Kurze jajko trafia ci^e, a mocno ju^z zepsuta lepka "+
                "ma^x znajduj" +
                "^aca si^e w ^srodku powoli si^e rozp^lywa si^e po twoim ubraniu, " +
                "roztaczaj^ac dooko^la straszny od^or.\n");
        }

        destruct();
    }
}

public string
query_jajko_auto_load()
{
    mixed alarm = get_alarm(zbuk_alarm);
    return "#JAJKO#" + (pointerp(alarm) && sizeof(alarm) > 3 ? ftoi(alarm[2]) : Zbuk) + "#JAJKO#";
}


public string
init_jajko_arg(string arg)
{
    string pre, post;

    if(sscanf(arg, "%s#JAJKO#%d#JAJKO#%s", pre, Zbuk, post) != 3)
        return arg;

    return pre + post;
}

public string
query_auto_load()
{
    return ::query_auto_load() + query_jajko_auto_load();
}

public string
init_arg(string arg)
{
    return init_jajko_arg(::init_arg(arg));
}
