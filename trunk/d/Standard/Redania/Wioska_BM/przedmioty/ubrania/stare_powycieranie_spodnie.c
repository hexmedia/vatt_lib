/*
 * do wioski ko^lo BM
 * opis by Vortak lub  kogo^s ze ^swity jego
 * zepsu^la faeve
 * dn. 18.05.2007
 *
 */

inherit "/std/armour";

#include <wa_types.h>
#include <formulas.h>
#include <stdproperties.h>
#include <macros.h>
#include <object_types.h>
#include <materialy.h>

void
create_armour()
{
    ustaw_nazwe("spodnie");
    ustaw_nazwe_glowna("para");

    dodaj_przym("stary", "starzy");
    dodaj_przym("powycierany", "powycierani");

    set_long("Pami^etaj^ace ju^z wiele wiosen portki, skrojone niezbyt "+
		"wprawn^a r^ek^a z kilku kawa^lk^ow niewiadomego pochodzenia i "+
		"r^ownie trudnego do okre^slenia koloru materia^lu. W pasie wszysto "+
		"w nie kawa^lek konopnego sznura pe^lni^acego rol^e paska. Mniej "+
		"wi^ecej w po^lowie d^lugo^sci, na wysoko^sci kolan w obydwu "+
		"nogawkach materia^l powyciera^l si^e, w wyniku czego powsta^ly "+
		"imponuj^acyh rozmiar^ow dziury.\n");
    set_ac(A_THIGHS | A_HIPS | A_STOMACH, 2, 2, 2);
    ustaw_material(MATERIALY_LEN);
    set_type(O_UBRANIA);
    add_prop(OBJ_I_VOLUME, 1000);
    add_prop(OBJ_I_WEIGHT, 509);
    add_prop(OBJ_I_VALUE, 13);
    add_prop(ARMOUR_I_D_ROZCIAGLIWOSC, 30);
    add_prop(ARMOUR_I_U_ROZCIAGLIWOSC, 30);
    add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    set_size("M");

    set_likely_cond(28);
}