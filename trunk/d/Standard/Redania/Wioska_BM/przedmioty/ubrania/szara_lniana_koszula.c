/*
 * do wioski ko^lo BM
 * opis by Vortak lub  kogo^s ze ^swity jego
 * zepsu^la faeve
 * dn. 18.05.2007
 *
 */

#include <stdproperties.h>
#include <macros.h>
#include <materialy.h>
#include <object_types.h>

inherit "/std/armour.c";

void
create_armour()
{
    ustaw_nazwe("koszula");
    
    dodaj_przym("szary", "szarzy");
    dodaj_przym("lniany", "lniani");
    
    set_long("Prosta w kroju koszula uszyta zosta^la z grubego lnu. Szorstki "+
		"materia^l nie jest mo^ze najwygodniejszy, ale jego wytrzyma^lo^s^c "+
		"a tak^ze niski koszt niweluj^a wszelkie wady. D^lugie r^ekawy "+
		"zako^nczone s^a wszytymi w nie sznurkami, a naszyta na piersi "+
		"kiesze^n umo^zliwia ukrycie w niej niewielkich przedmiot^ow. \n");

    ustaw_material(MATERIALY_TK_LEN);
        
    add_prop(OBJ_I_VOLUME, 550);
    add_prop(OBJ_I_WEIGHT, 200);
    add_prop(OBJ_I_VALUE, 7);
    
    set_slots(A_TORSO, A_FOREARMS, A_ARMS); 
    set_type(O_UBRANIA);
    add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    set_size("M");

	    add_prop(CONT_I_MAX_VOLUME, 1500);
    add_prop(CONT_I_MAX_WEIGHT, 1200);
    
    add_subloc(({"kiesze^n", "kieszeni", "kieszeni", "kiesze^n", "kieszeni^a",
"kieszeni"}));
    add_subloc_prop("kiesze^n", SUBLOC_S_OB_GDZIE, "wewn^atrz kieszeni");
    add_subloc_prop("kiesze^n", SUBLOC_I_OB_PRZYP, PL_DOP);
    add_item("kiesze^n w", "Jest to niedu^za kiesze^n, naszyta na piersi koszuli" +
        "@@opis_sublokacji| zawieraj^aca |kiesze^n|.|.|" + PL_BIE + "@@\n",
PL_MIE);
}
