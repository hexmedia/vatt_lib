#include <stdproperties.h>
#include <macros.h>
#include "dir.h"

inherit "/std/container";

create_container()
{
    ustaw_nazwe("skrzynia");
    
    set_long("Niski, zbity z kilku desek kosz. W tej chwili @@opis_sublokac" +
    "ji|||.|jest pusty.|0|znajduje si^e w nim |znajduj^a si^e w nim |znajdu" +
    "je si^e w nim @@\n");
    
    add_prop(OBJ_M_NO_GET, "Skrzynia jest przymocowana do ^l^o^zka.\n");
    add_prop(CONT_I_MAX_VOLUME, 56000);
    add_prop(OBJ_I_VOLUME, 25000);
    add_prop(CONT_I_MAX_WEIGHT, 60000);
    add_prop(OBJ_I_WEIGHT, 15000);
    add_prop(CONT_I_DONT_SHOW_CONTENTS, 1);
    
    set_no_show();
}

int
wysun(string str)
{
    if (str ~= "skrzyni^e" || str ~= "skrzynie spod ^l^o^zka")
    {
        if (TO->query_prop("wysunieta"))
        {
        TP->catch_msg("Skrzynia jest ju^z wysuni^eta spod ^l^o^zka.\n");
        return 1;
        }
        else
        {
        TP->catch_msg("Pochylasz si^e i szybkim ruchem wysuwasz skrzyni^e " +
        "spod ^l^o^zka.\n");
        saybb(QCIMIE(TP, 0) + " pochyla si^e i szybkim ruchem wysuwa skrzyni^e " +
        "spod ^l^o^zka.\n");
        
        TO->add_prop("wysunieta", 1);
        TO->unset_no_show();
        
        return 1;
        }
    }
    else
    {
        notify_fail("Wysu^n co?\n");
        return 0;
    }
}

int
wsun(string str)
{
    if (str ~= "skrzyni^e" || str ~= "skrzynie pod ^l^o^zko")
    {
        if (TO->query_prop("wysunieta") == 0)
        {
            TP->catch_msg("Skrzynia jest ju^z wsuni^eta pod ^l^o^zko.\n");
            return 1;
        }
        else
        {
        TP->catch_msg("Pochylasz si^e i szybkim ruchem wsuwasz skrzyni^e " +
        "pod ^l^o^zko.\n");
        saybb(QCIMIE(TP, 0) + " pochyla si^e i szybkim ruchem wysuwa skrzyni^e " +
        "pod ^l^o^zko.\n");

        TO->add_prop("wysunieta", 0);
        TO->set_no_show();
        }

        return 1;
    }
    else
    {
        notify_fail("Wsu^n co?\n");
        return 0;
    }
}

void
init()
{
    ::init();
    add_action("wysun", "wysu^n");
    add_action("wsun", "wsu^n");
}
