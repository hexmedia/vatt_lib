/*
 * Opis: Vortak
 * Kod: Gjoef
 *     Maj 2007
 */

#include <macros.h>
#include <stdproperties.h>
#include <mudtime.h>
#include <pogoda.h>
#include "dir.h"

inherit WIOSKA_STD;

string
zywoplot()
{
    string str = "";

    if (TO->pora_roku() == MT_ZIMA)
        if (CZY_JEST_SNIEG(TO))
            str += "Schowane pod warstw^a bia^lego puchu, pozbawione " +
            "li^sci ga^l^azki s^a tak ze sob^a spl^atane, ^ze tworz^a " +
            "nieprzebyt^a ^scian^e.\n";
        else
            str += "Pozbawione li^sci ga^l^azki s^a tak ze sob^a spl^atane, " +
            "^ze tworz^a nieprzebyt^a ^scian^e.\n";
    else
        if (TO->pora_roku() != MT_JESIEN)
            str += "G^este, zielone zaro^sla tworz^a " +
            "nieprzebyt^a ^scian^e.\n";
        else
            str += "Pozbawione li^sci ga^l^azki s^a tak ze sob^a spl^atane, " +
            "^ze tworz^a nieprzebyt^a ^scian^e.\n";
            
    return str;
}

void
create_wioska()
{
    set_short("Na placu");

    add_exit("wioska7.c", "n");
    add_exit("wioska6.c", "e");
    add_exit("studnia.c", "ne");
    add_exit("wioska4.c", "s");
    
    add_item("^zywop^lot", "@@^zywop^lot@@");
    
    add_object(WIOSKA_PRZEDMIOTY + "laweczka");
    dodaj_rzecz_niewyswietlana("ma^la ko^slawa ^laweczka", 1);
}

string
exits_description()
{
    return "Pozosta^la cz^e^s^c placu widoczna jest na p^o^lnocy i wschodzi" +
    "e, za^s na p^o^lnocnym-wschodzie dostrzec mo^zna studni^e. Biegnie " +
    "st^ad tak^ze ^scie^zka w kierunku po^ludniowym.\n";
}

string
dlugi_opis()
{
    string str = "Oto po^ludniowo-zachodni kraniec " +
    "niewielkiego, stanowi^acego centrum wioski placu. ";

    //zdania o sloncu sie KURWA komus zachcialo...
    switch(TO->pora_dnia())
    {
        case 11..13:
            if (ZACHMURZENIE(TO) > 2)
                str += "Wschodz^ace s^lo^nce schowane jest za chmurami, " +
                "topi^ac okolic^e w ponurych odcieniach szaro^sci. ";
            else
            {
                str += "Wschodz^ace s^lo^nce przyjemnie grzeje twoj^a twarz";
                
                if (TEMPERATURA(TO) > 18)
                    str += ", jednak z minuty na minut^e robi si^e coraz " +
                    "bardziej gor^aco";
                    
                str += ". ";
            }
            break;
            
        case 14:
             switch (TEMPERATURA(TO))
             {
                 case 26..666:
                     str += "Stoj^ace w zenicie s^lo^nce piecze " +
                     "niemi^losiernie, rozgrzewaj^ac plac jak patelni^e. ";
                     break;
                 case 10..25:
                     str += "Stoj^ace w zenicie s^lo^nce przyjemnie grzeje " +
                     "twoj^a twarz. ";
                     break;
                 case 0..9:
                     str += "Cho^c s^lo^nce stoi w zenicie, dooko^la panuje " +
                     "ch^l^od. ";
                     break;
                 case -1..-273:
                     str += "Cho^c s^lo^nce stoi w zenicie, dooko^la panuje " +
                     "nieprzyjemny mr^oz. ";
                     break;
            }
            break;

        case 15..16:
            str += "Powoli zmierzaj^ace ku zachodowi s^lo^nce skry^lo si^e " +
            "ju^z za okalaj^acym od zachodu plac ^zywop^lotem, kt^ory " +
            "rzuca teraz przyjemny cie^n na ca^l^a okolic^e. ";
            break;
    }
    
    if (present("^laweczka"))
    {
        switch(TO->pora_dnia())
        {
            case 15..16:
                str += "Kto^s postawi^l tam ko^slaw^a ^laweczk^e zapraszaj^ac^a, " +
                "by usi^a^s^c na niej i rozprostowa^c nogi. ";
                break;

            default:
                str += "Kto^s postawi^l tu, pod os^lon^a okalaj^acego od " +
                "zachodu plac ^zywop^lotu, ko^slaw^a ^laweczk^e, na kt^orej " +
                "mo^zna usi^a^s^c i rozprostowa^c nogi. ";
                break;
        }
    }

    str += "Na p^o^lnocnym-wschodzie wida^c drewnian^a, zadaszon^a studni^e, "+
    "za^s na p^o^lnocy rozci^aga si^e widok na kilka wiejskich cha^lup i " +
    "zabudowa^n gospodarczych.\n";
    
    return str;
}
