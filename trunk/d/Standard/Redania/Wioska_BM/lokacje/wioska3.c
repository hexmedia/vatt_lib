/*
 * Opis: Vortak
 * Kod: Gjoef
 *     Maj 2007
 */

#include <macros.h>
#include <stdproperties.h>
#include <mudtime.h>
#include <pogoda.h>
#include "dir.h"

inherit WIOSKA_STD;

int Pies = 0;

int
query_pies()
{
    return Pies;
}

string
karczma()
{
    string str = "Jedyny w okolicy pi^etrowy budynek zbudowany z " +
    "pot^e^znych drewnianych bali. Nad wej^sciem wisi zbity z kilku " +
    "nieoheblowanych desek szyld. ";
    
    if (MT_GODZINA < 23 || MT_GODZINA >= 5)
        str += "Okute drzwi sprawiaj^a wra^zenie solidnych, jednak wydaje " +
        "si^e, ^ze lokal powinien by^c o tej porze otwarty.\n";
    else
        str += "Okute drzwi sprawiaj^a wra^zenie solidnych i trudnych do " +
        "sforsowania, dlatego lepiej poczeka^c do rana, a^z karczma zostani" +
        "e otwarta.\n";
        
    return str;
}

string
domek()
{
    string str = "";
    
    if (CZY_JEST_SNIEG(TO))
        str = "Schowany w bia^lym puchu";
    else
        str = "Ukryty w^sr^od zieleni";
        
    str += " domek sprawia wra^zenie schludnego i zadbanego. Prowadzi do^n " +
    "w^aska, dok^ladnie zamieciona ^scie^zka. Posiad^lo^s^c otacza solidny " +
    "p^lot.\n";
    
    return str;
}

/*string
pies()
{
    if (Pies)
        return "Jedyne co dostrzegasz zza p^lotu to b^lyskaj^ace w ciemno^s" +
        "ci oczy, wielki, kud^laty ^leb i ostre z^eby, kt^ore potrafi^a sku" +
        "tecznie wybi^c z g^lowy pr^ob^e sforsowania ogrodzenia.\n";
    else
        return "Nie zauwa^zasz niczego takiego.\n";
}

void
karel_rano()
{
    tell_room(TO, "Z niewielkiego domku wychodzi " +
    QIMIE(find_living("karel"), 0) + ", kt^ory mieszanin^a krzyk^ow i " +
    "raz^ow zadawanych trzymanym w d^loni kosturem zagania psa do " +
    "specjalnie w tym celu skonstruowanej zagrody, otwiera furtk^e, a na " +
    "ko^ncu znika we wn^etrzu mieszkania.\n");
    
    Pies = 0;
}

void
karel_wieczor()
{
    tell_room(TO, "Z niewielkiego domku wychodzi " +
    QIMIE(find_living("karel"), 0) + ", kt�ry zamkn^awszy furtk^e " +
    "wypuszcza pot^e^znego, kud^latego psa na podw^orko, za^s sam znika " +
    "we wn^etrzu mieszkania.\n");

    Pies = 1;
}

void
karel()
{
    if (jest_dzien())
    {
        if (Pies)
            set_alarm(63.0, 0.0, "karel_rano");
    }
    else
        if (!Pies)
            set_alarm(63.0, 0.0, "karel_wieczor");
}*/

void
create_wioska()
{
    set_short("Przed karczm^a");

    add_exit("wioska4.c", "ne");
	add_exit(TRAKT_BIALY_MOST_OKOLICE_LOKACJE+"s01.c", "sw",0,5,0);
    
    add_object(WIOSKA_PRZEDMIOTY + "szyld");
    add_object(WIOSKA_DRZWI + "do_karczmy");
    add_object(WIOSKA_DRZWI + "do_ogrodu");
    
    add_item(({"karczm^e", "budynek", "pi^etrowy budynek", "drewniany budynek",
    "drewniany pi^etrowy budynek"}), "@@karczma@@");
    
    add_item(({"domek", "niewielki domek", "zadbany domek", "niewielki " +
    "zadbany domek", "domek na p^o^lnocy"}), "@@domek@@");
    
 /* add_item(({"psa", "psa na p^o^lnocy", "psa za p^lotem", "wielkiego psa"}),
    "@@pies@@"); */
    
    add_item(({"p^lot", "ogrodzenie"}), "Wysoki na jakie^s sze^s^c st^op " +
    "p^lot otacza zadban^a posiad^lo^s^c.\n");
    
 /* set_alarm_every_hour("karel");
    set_alarm(1.0, 0.0, "karel");*/
}

string
exits_description()
{
    return "^Scie^zka rozci^aga si^e na po^ludniowy-zach^od i p^o^lnocny-wsch^od. Tu " +
    "za^s znajduje si^e wej^scie do karczmy i furtka prowadz^aca do ogrodu.\n";
}

string
dlugi_opis()
{
    string str = "Udeptana ^scie^zka biegnie od widocznego niedaleko st^ad " +
    "niewielkiego placyku, stanowi^acego zapewne centrum wioski, do " +
    "znajduj^acego si^e kawa^lek na po^ludnie kupieckiego duktu. Na zachodzie, tu^z przy drodze stoi drewniany " +
    "pi^etrowy budynek";
    
    if (MT_GODZINA >= 23 || MT_GODZINA > 5)
        str += ", nad kt�rym leniwie snuje si� wst��ka unosz�cego si� z komina dymu";

    str += ". Nad drzwiami wej^sciowymi wisi zbity z kilku krzywych, " +
    "nieoheblowanych desek szyld. ";

    if (MT_GODZINA >= 23 || MT_GODZINA > 6)
        str += "S^adz^ac po ha^lasach dobiegaj^acych ze ^srodka m";
    else
       str += "M";

    str += "usi to by^c lokalna karczma, w gdzie okoliczni mieszka^ncy " +
    "wypoczywaj^a po ci^e^zkim dniu pracy. Na p^o^lnoc st^ad wida^c " +
    "niewielki zadbany domek, do kt^orego prowadzi w^aska";

    if (CZY_JEST_SNIEG(TO))
        str += ", pieczo^lowicie od^snie^zona";

    str += " ^scie^zka.\n";
    
    return str;
}
