#include <stdproperties.h>
#include <materialy.h>
#include "dir.h"

inherit "/std/door";

void
create_door()
{
    ustaw_nazwe("drzwi");
    dodaj_przym("ci^e^zki", "ci^e^zcy");
    dodaj_przym("d^ebowy", "d^ebowi");
    set_other_room(WIOSKA_LOKACJE + "karczma.c");
    set_door_id("WIOSKA_KARCZMA");
    set_door_desc("Ci^e^zkie, d^ebowe drzwi prezentuj^a si^e " +
    "naprawd^e porz^adnie. Drewno, pokryte obfit^a warstw^a " +
    "ciemnobr^azowej bejcy, wygl^ada na dosy^c m^lode.\n");

    set_pass_command("karczma", "do karczmy", "z zewn^atrz");
    set_open_desc("");
    set_closed_desc("");

    ustaw_material(MATERIALY_DR_DAB);

    set_key("WIOSKA_KARCZMA_KLUCZ");
}
