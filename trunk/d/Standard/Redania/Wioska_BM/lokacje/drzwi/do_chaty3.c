#include <stdproperties.h>
#include <materialy.h>
#include "dir.h"

inherit "/std/door";

void
create_door()
{
    ustaw_nazwe("drzwi");
    dodaj_przym("solidny", "solidni");
    dodaj_przym("d^ebowy", "d^ebowi");
    set_other_room(WIOSKA_LOKACJE + "chata3.c");
    set_door_id("WIOSKA_CHATA3");
    set_door_desc("Solidne drzwi prezentuj^a si^e wcale porz^adnie. Mimo " +
        "up^lywu lat d^ebowe drewno nie p^eka, jedynie pociemnia^lo troch^e " +
        "od s^lo^nca.\n");

    set_pass_command("chata", "do chaty", "z zewn^atrz");
    set_open_desc("");
    set_closed_desc("");

    ustaw_material(MATERIALY_DR_DAB);

    set_open(0);
    set_locked(1);
}
