/*
 * Opis: Vortak
 * Kod: Gjoef
 *     Maj 2007
 */

#include <stdproperties.h>
#include <macros.h>
#include <mudtime.h>
#include "dir.h"

inherit WIOSKA_BM_STD;
inherit "/lib/pub";
inherit "/lib/pub_new";

string
fristajlo()
{
    string str = "UMRZESZ";

    if (pora_dnia() < 16)
        str = "Jest tu du^zo ciemniej ni^z na dworze. Okna, nie do^s^c " +
            "^ze wygl^adaj^a jakby by^ly myte ca^le wieki temu, dodatkowo " +
            "zakryte s^a od zewn^atrz drewnianymi okiennicami. ";
    else
        str = "Jest tu niewiele ja^sniej ni^z na dworze. ";

    str += "Jedynie kilka olejnych lamp przymocowanych do ^scian daje " +
        "troch^e ^swiat^la. Ca^le pomieszczenie zasnute jest gryz^acym w oczy " +
        "dymem wydobywaj^acym si^e ze stoj^acego pod ^scian^a paleniska. W " +
        "p^o^lmroku dostrzegasz wisz^ace na ^scianach wyprawione sk^ory " +
        "zwierz^at";

    if (present("^lawa"))
        str += " oraz ustawione na wzd^lu^z sali masywne sto^ly, " +
            "przy kt^orych si^a^s^c mog^a go^scie tego przybytku";

    str += ". W naro^zniku pomieszczenia znajduje si^e mocno porysowana " +
        "przez lata u^zytkowania lada. Nad blatem, za kt^orego os^lon^a zwyk^l " +
        "kry^c si^e w^la^sciciel karczmy, umieszczono tabliczk^e ze spisem potraw " +
        "serwowanych w lokalu. \n";

    return str;
}

create_wioska_bm()
{
    set_short("Karczma");

    set_long("@@fristajlo@@");

    add_object(WIOSKA_PRZEDMIOTY + "stol", 3);
    add_object(WIOSKA_PRZEDMIOTY + "tabliczka");
    add_object(WIOSKA_PRZEDMIOTY + "lada");
    add_object(WIOSKA_DRZWI + "z_karczmy");

    add_sit("przy ladzie", "przy ladzie", "od lady", 4);
    add_sit("na pod^lodze", "na pod^lodze", "z pod^logi", 0);
    add_sit("przy pierwszym stole", "przy pierwszym stole", "od pierwszego sto^lu", 8);
    add_sit("przy drugim stole", "przy drugim stole", "od drugiego sto^lu", 8);
    add_sit("przy trzecim stole", "przy trzecim stole", "od trzeciego sto^lu", 8);

    add_prop(ROOM_I_INSIDE, 1);
    add_prop(ROOM_I_LIGHT, 1);

    add_item(({"okno", "okna"}), "Okna zas^loni^ete s^a drewnianymi " +
        "okiennicami, jednak nawet bez nich ci^e^zko by^loby dojrze^c przez " +
        "nie cokolwiek. Pokryte grub^a warstw^a t^luszczu, sadzy i strach " +
        "pomy^sle^c czym jeszcze, wygl^adaj^a jakby najstarsi bywalcy karczmy " +
        "nie wiedzieli, kiedy ostatnio by^ly myte.\n");

    add_item(({"sk^ory", "sk^or^e"}), "To najpewniej trofea ^lowieckie " +
        "jakiego^s zapalonego ^lowczego. Ju^z na pierwszy rzut oka mo^zna " +
        "jednak pozna^c, i^z polowanie, na kt^orym zosta^ly zdobyte odby^lo " +
        "si^e wiele lat temu. S^a tak poniszczone, ^ze w^la^sciwie nie da si^e " +
        "rozpozna^c, z jakiego zwierz^ecia zosta^ly ^sci^agni^ete - s^a poszarpane," +
        " miejscami pozbawione w�osia, a tu i ^owdzie porozcinane.\n");

    add_item(({"lamp^e", "lampy", "olejn^a lamp^e", "olejne lampy"}), "Kilka " +
        "olejnych lamp przymocowanych do ^scian o^swietla pomieszczenie.\n");

    add_item("palenisko", "Tak jak wielkie jest palenisko, tak i ogie^n huc" +
        "z^acy wewn^atrz, tak^ze r^ownie wiele dymu wydobywa si^e ze ^srodka. " +
         "Komin musi by^c nieczyszczony od lat, bowiem spora cz^e^s^c szarych, " +
         "gryz^acych w oczy k^l^ebow zostaje w karczmie, zasnuwaj^ac ca^l^a izb^e.\n");

    add_food("groch^owka", 0, "g^estej, ch^lopskiej groch^owki", 550, 0, 8,
        "miska", "groch^owki", "Stara drewniana miska tu i tam nosi " +
        "jeszcze zaschni^ete ^slady innych potraw.", 200);   //co to za parametry,
    add_food("^zur", 0, "^zuru", 550, 0, 10,              //tamto 0 i 200?
        "miska", "groch^owki", "Stara drewniana miska tu i tam nosi " +
        "jeszcze zaschni^ete ^slady innych potraw.", 200);
    add_food("bigos", 0, "wspaniale pachn^acego bigosu",
        460, 0, 15, "talerz", "bigosu", "Drewniany talerz kiepskiej jako^sci " +
        "wygl^ada, jakby nigdy by^l myty.", 200);

    add_drink("mleko", 0, "^swie^zego krowiego mleka", 250, 0, 8, "kubek",
        "mleka", "Ot, zwyk^ly kubek z drewna.");
    add_drink("kompot", ({"jab^lkowy", "jab^lkowi"}), "jab^lkowego kompotu",
        250, 0, 9, "kubek", "kompotu", "Ot, zwyk^ly kubek z drewna.");
    add_drink("piwo", 0, "jasnego piwa o md^lym aromacie", 600, 4, 14,
        "kufel", "piwa", "Du^zy kufel, jak to w bywa w karczmach, utraci^l " +
         "przez lata sw^a przejrzysto^s^c.");
    add_drink(({"piwo","piwo z wkladka"}), 0, "jasnego piwa o wyra^xnym "+
       "posmaku alkoholu", 600, 23, 17, "kufel", "piwa", "Du^zy kufel, jak to "+
       "w bywa w karczmach, utraci^l przez lata sw^a przejrzysto^s^c.");
    add_drink("^sliwowica", 0, "piekielnie mocnego alkoholu ze ^sliwek",
        60, 70, 200, "kieliszek", "^sliwowicy", "Ma^ly kieliszeczek ze szk^la.");
    add_drink("w^odka", 0, "zimnej w^odki",
        60, 45, 180, "kieliszek", "w^odki", "Ma^ly kieliszeczek ze szk^la.");
    add_drink("specjalnosc zakladu", 0, "czy^sciutkiego spirytusu, zna" +
        "nego tu jako specjalnosc zakladu", 60, 100, 250, "kieliszek",
        "spirytusu", "Ma^ly kieliszeczek ze szk^la.");

    add_npc(WIOSKA_NPC + "karczmarz", 1, "czy");
}

int czy()
{
    if (find_living("johann") == 0)
    return 1;
    return 0;
}

string
exits_description()
{
    return "Wyj^scie z karczmy prowadzi na zewn^atrz.\n";
}

void
init()
{
    ::init();
    init_pub();
}
