/*
 * Opis: Vortak
 * Kod: Gjoef
 *     Maj 2007
 */

#include <macros.h>
#include <stdproperties.h>
#include <mudtime.h>
#include <pogoda.h>
#include "dir.h"

inherit WIOSKA_STD;

void
create_wioska()
{
    set_short("Na placu");

    add_exit("wioska5.c", "s");
    add_exit("wioska6.c", "se");
    add_exit("studnia.c", "e");
    add_exit("wioska9.c", "w");

    add_npc(WIOSKA_NPC + "siwy_zgarbiony_mezczyzna",1, "czy");
}

int czy()
{
    if (find_living("staruszek_wioska") == 0)
    return 1;
    return 0;
}

string
exits_description()
{
    return "Pozosta^la cz^e^s^c placu widoczna jest na po^ludniu i po^ludni" +
    "owym-wschodzie, za^s na wschodzie dostrzec mo^zna studni^e. Z tego mie" +
    "jsca biegnie r^ownie^z ^scie^zka na zach^od.\n";
}

string
dlugi_opis()
{
    string str = "Znajdujesz si^e na p^o^lnocno-zachodnim kra^ncu " +
    "niewielkiego, stanowi^acego centrum wioski placyku. Kilka w^askich " +
    "^scie^zek odchodz^acych z tego miejsca wiedzie do widocznych " +
    "kawa^lek st^ad cha^lup i zabudowa^n gospodarczych. Na wschodzie " +
    "wida^c drewnian^a, zadaszon^a studni^e";

    if (present("^laweczka", find_object(WIOSKA_LOKACJE + "wioska5.c")))
    {
        str  += ", za^s na po^ludniu ko^slaw^a ^laweczk^e";
        if (!CO_PADA(TO))
            str += ". Ustawiona pod os^lon^a otaczaj^acego od zachodu plac " +
            "^zywop^lotu zdaje si^e by^c idealnym miejscem, by nieco " +
            "odpocz^a^c po trudach dnia";
        else
            str += ", ustawion^a pod ^scian^a ^zywop^lotu otaczaj^acego " +
            "plac od zachodu";
    }
    
    str += ".\n";
    
    return str;
}
