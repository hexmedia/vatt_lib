/*
 * Opis: Vortak
 * Kod: Gjoef
 *     Maj 2007
 */

#include <macros.h>
#include <stdproperties.h>
#include <mudtime.h>
#include <pogoda.h>
#include "dir.h"

inherit WIOSKA_STD;

void
create_wioska()
{
    set_short("Na p^o^lnoc od placu");

    add_exit("wioska7.c", "s");
    
    add_object(WIOSKA_DRZWI + "do_chaty3");
}

string
exits_description()
{
    return "Kieruj^ac si^e na po^ludnie, dotrzesz do placu. Tu za^s znajduj" +
    "^a si^e drzwi jednej z wiejskich chat.\n";
}

string
dlugi_opis()
{
    string str = "Rasta.\n";
    
    return str;
}
