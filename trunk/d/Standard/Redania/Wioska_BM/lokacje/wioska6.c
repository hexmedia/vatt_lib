/*
 * Opis: Vortak
 * Kod: Gjoef
 *     Maj 2007
 */

#include <macros.h>
#include <stdproperties.h>
#include <mudtime.h>
#include <pogoda.h>
#include "dir.h"

inherit WIOSKA_STD;

string
zywoplot()
{
    string str = "";

    if (TO->pora_roku() == MT_ZIMA)
        if (CZY_JEST_SNIEG(TO))
            str += "Schowane pod warstw^a bia^lego puchu, pozbawione " +
            "li^sci ga^l^azki s^a tak ze sob^a spl^atane, ^ze tworz^a " +
            "na zachodzie nieprzebyt^a ^scian^e.\n";
        else
            str += "Pozbawione li^sci ga^l^azki s^a tak ze sob^a spl^atane, " +
            "^ze tworz^a na zachodzie nieprzebyt^a ^scian^e.\n";
    else
        if (TO->pora_roku() != MT_JESIEN)
            str += "G^este, zielone zaro^sla tworz^a na zachodzie " +
            "nieprzebyt^a ^scian^e.\n";
        else
            str += "Pozbawione li^sci ga^l^azki s^a tak ze sob^a spl^atane, " +
            "^ze tworz^a na zachodzie nieprzebyt^a ^scian^e.\n";

    return str;
}

void
create_wioska()
{
    set_short("Na placu");

    add_exit("wioska5.c", "w");
    add_exit("wioska7.c", "nw");
    add_exit("studnia.c", "n");
    
    add_item(({"^zywop^lot", "^zywop^lot na zachodzie"}), "@@zywoplot@@");
}

string
exits_description()
{
    return "Pozosta^la cz^e^s^c placu widoczna jest na zachodzie i p^o^lnoc" +
    "ym-zachodzie, za^s na p^o^lnoc dostrzec mo^zna studni^e.\n";
}

string
dlugi_opis()
{
    string str = "Znajdujesz si^e w po^ludniowo-wschodnim kra^ncu " +
    "niewielkiego, stanowi^acego centrum wioski placu. Wzd^lu^z jego " +
    "zachodniej granicy ro^snie pas ";

    if (CZY_JEST_SNIEG(TO))
        str += "ukrytego pod warstw^a ^sniegu";
    else
        str += "daj^acego nieco przyjemnego cienia";

    str += " ^zywop^lotu. ";

    if (present("^laweczka", find_object(WIOSKA_LOKACJE + "wioska5.c")))
        str += "Tam te^z kt^ory^s z mieszka^nc^ow ustawi^l ko^slaw^a " +
        "^laweczk^e, na kt^orej mo^zna na moment przysi^a^s^c i odpocz^a^c. ";
    else
        str += "Przyjemnie by^loby tam przysi^a^s^c i troch^e odpocz^a^c, " +
        "gdyby tylko jaki^s uprzejmy cz^lowiek postara^l si^e o ^lawk^e. ";

    str += "Kilka krok^ow na p^o^lnoc wida^c drewnian^a, zadaszon^a " +
    "studni^e, za^s nieco bardziej na zachodzie rozci^aga si^e widok na " +
    "kilka wiejskich cha^lupek i zabudowa^n gospodarczych. Kawa^lek dalej, " +
    "na po^ludniu, musi przebiega^c dukt kupiecki. ";
    
    str += "\n";
    
    return str;
}
