//dokonczyc to i tamto. palenisko jest pojebane i nie wiem co jeszcze

#include <stdproperties.h>
#include <pogoda.h>
#include "dir.h"

inherit WIOSKA_BM_STD;

string
dlugij()
{
    string str = "";

    str += "Nieprzyjemna wo^n st^echlizny unosz^aca si^e w powietrzu zdaje " +
    "si^e dope^lnia^c rozpaczliwego obrazu pomieszczenia, w kt^orym " +
    "jedynie olbrzymie, czarne karaluchy i ruchliwe skorki zwinnie " +
    "przemieszaj^ace si^e w^sr^od zakamark^ow izby, mog^a znale^x^c " +
    "schronienie. ^Sciany, zbudowane z niedok^ladnie ociosanych " +
    "bali drewna uszczelnionych mocno ju^z nadgni^l^a s^lom^a, porasta w " +
    "kilku miejscach czarna ple^s^n, kt^orej w^lochate zarodnie " +
    "nieznacznie ko^lysz^a si^e na wietrze, dostaj^acym si^e tu przez dwa " +
    "niewielkie otwory przy stropie stanowi^ace zarazem jedyne ^xr^od^lo " +
    "naturalnego ^swiat^la. W jednym z rog^ow izby, na glinianej pod^lodze " +
    "le^zy kilka grubych snop^ow siana, ";

    if (jest_rzecz_w_sublokacji("siano", "we^lniany pled"))
        str += "niechlujnie przykrytych grubym, we^lnianym pledem, ";

    str += "s^lu^z^acych za wyj^atkowo prymitywne le^zysko " +
    "mieszka^nca tej chaty. Na ^srodku pomieszczenia znajduje si^e " +
    "otoczone kamieniami palenisko";

    if (jest_rzecz_w_sublokacji("palenisko", "ma^ly blaszany kocio^lek"))
        str += ", nad kt^orym wisi niewielki blaszany kocio�ek";

    str += ".\n";

    return str;
}

string
siano()
{
    string str = "W jednym z rog^ow izby, na glinianej pod^lodze le^zy " +
    "kilka grubych snop^ow siana, ";

    if (jest_rzecz_w_sublokacji("siano", "we^lniany pled"))
       str += "niechlujnie przykrytych grubym, we^lnianym pledem, ";

    str += "s^lu^z^acych za wyj�tkowo prymitywne le^zysko " +
    "mieszka^nca tej chaty. ";

    str += opis_sublokacji("", "siano", ".","",0,"Znajduje si^e na " +
    "nim ", "Znajduj^a si^e na nim ", "Znajduje si^e na nim ");

    str += "\n";

    return str;
}

string
otwory()
{
    return "W sypi^acym si^e ju^z stropie powsta^ly dwie niewielkie dziury.\n";
}

void
create_wioska_bm()
{
    set_short("Obskurna chata");

    set_long("@@dlugij@@");

    add_prop(ROOM_I_INSIDE, 1);
    add_prop(ROOM_I_LIGHT, 1);

    add_object(WIOSKA_DRZWI + "z_chaty1");
    add_object(WIOSKA_PRZEDMIOTY + "pled");
    add_object(WIOSKA_PRZEDMIOTY + "kociolek");

    dodaj_rzecz_niewyswietlana("we^lniany pled", 1);
    dodaj_rzecz_niewyswietlana("ma^ly blaszany kocio^lek", 1);
    dodaj_rzecz_niewyswietlana_plik(WIOSKA_PRZEDMIOTY + "kamien");

    add_subloc(({"siano", "siana", "sianu",
    "siano", "sianem", "sianie", "na"}));

    add_subloc_prop("siano", SUBLOC_I_MOZNA_POLOZ, 1);
    add_subloc_prop("siano", SUBLOC_I_MOZNA_ODLOZ, 1);
    add_subloc_prop("siano", CONT_I_MAX_VOLUME, 200000);
    add_subloc_prop("siano", CONT_I_MAX_WEIGHT, 350000);

    add_subloc(({"palenisko", "paleniska", "palenisku",
    "palenisko", "paleniskiem", "palenisku", "nad"}));

    add_subloc_prop("palenisko", SUBLOC_I_MOZNA_POWIES, 1);
    add_subloc_prop("palenisko", SUBLOC_I_MOZNA_POLOZ, 0);
    add_subloc_prop("palenisko", CONT_I_MAX_VOLUME, 15000);
    add_subloc_prop("palenisko", CONT_I_MAX_WEIGHT, 10000);

    add_sit("na pod^lodze", "na glinianej pod^lodze", "z pod^logi", 0);
    add_sit("na sianie", "na sianie", "z siana", 3);

    add_item("palenisko", "Pomi^edzy dwoma wide^lkowato rozszerzaj^acymi " +
    "si^e u g^ory kijkami przytrzymywanymi w swej pionowej pozycji przez " +
    "kilka kamieni u^lo^zono gruby patyk.\n");

    add_item(({"siano", "le^zysko", "snopy siana", "snopy"}), "@@siano@@");

    add_item(({"^sciany", "ple^s^n", "drewniane bele"}), "Drewniane ^sciany " +
    "chaty poros^la czarna, w^lochata ple^s^n.\n");

    add_item(({"otw^or w stropie", "otwory w stropie", "otwory", "otw^or",
    "sufit", "strop"}), "@@otwory@@");
}

string
exits_description()
{
    return "Wyj^scie z chaty prowadzi na zewn^atrz.\n";
}

void
kamien(string str)
{
    object kam;

    if (str ~= "kamie^n" || str ~= "kamienie")
    {
        kam = clone_object(WIOSKA_PRZEDMIOTY + "kamien");
        kam->move(this_player());
        this_player()->catch_msg("Bierzesz kamie^n z paleniska.\n");
    }

    return;
}

void
init()
{
    ::init();

    add_action("kamien", "we^x");
}
