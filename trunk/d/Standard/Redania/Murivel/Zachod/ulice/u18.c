/* Autor: 
   Opis : 
   Data : */

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"
#include <pogoda.h>

inherit MURIVEL_STD;

void create_ulica() 
{
    set_short("Ulica");

    add_exit(MURIVEL_ZACHOD_ULICE + "u20","nw",0,1,0);
    add_exit(MURIVEL_ZACHOD_ULICE + "u19","n",0,1,0);
    add_exit(MURIVEL_ZACHOD_ULICE + "u17","s",0,1,0);
    add_exit(MURIVEL_ZACHOD_ULICE + "u16","sw",0,1,0);

    add_object(MURIVEL_ZACHOD_DRZWI + "na_poczte");

//    add_object(""); drzwi na poczte



    add_prop(ROOM_I_INSIDE,0);
    add_object(MURIVEL_PRZEDMIOTY + "lampa_uliczna");
}

public string
exits_description() 
{
    return "Uliczka prowadzi na p^o^lnocny zach^od, p^o^lnoc, po^ludnie "+
        "oraz po^ludniowy zach^od, natomiast na wschodzie znajduje si^e "+
        "wej^scie na poczt^e.\n";
}

string
dlugi_opis()
{
    string str;
    if(jest_dzien())
    {
        str = "Dzie^n.";
    }
    else
    {
        str = "Noc.";
    }
    str += "\n";
    return str;
}
