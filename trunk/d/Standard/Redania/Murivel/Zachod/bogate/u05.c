/* Autor: 
   Opis : 
   Data : */

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"
#include <pogoda.h>

inherit MURIVEL_STD;

void create_ulica() 
{
    set_short("Ulica");

    add_exit(MURIVEL_ZACHOD_BOGATE + "u04","n",0,1,0);
    add_exit(MURIVEL_ZACHOD_BOGATE + "u06","s",0,1,0);

    add_object(MURIVEL_ZACHOD_DRZWI + "do_jubilera");

    //na nw wejscie do jubilera

    add_prop(ROOM_I_INSIDE,0);

    add_object(MURIVEL_PRZEDMIOTY + "lampa_uliczna");
}

public string
exits_description() 
{
    return "Uliczka wiedzie z po^ludnia na p^o^lnoc. Na p^o^lnocnym "+
        "zachodzie za^s ma sw^oj zak^lad miejscowy jubiler. \n";
}

string
dlugi_opis()
{
    string str;
    if(jest_dzien())
    {
        str = "Dzie^n.";
    }
    else
    {
        str = "Noc.";
    }
    str += "\n";
    return str;
}
