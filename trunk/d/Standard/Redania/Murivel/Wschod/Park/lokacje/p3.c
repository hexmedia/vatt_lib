/* Autor: 
   Opis : 
   Data : */

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"

inherit "/std/room";

void create_room() 
{
    set_short("W parku.");

    add_exit(MURIVEL_WSCHOD_PARK +"lokacje/p4.c","nw",0,1,0);
    add_exit(MURIVEL_WSCHOD_PARK +"lokacje/p2.c","sw",0,1,0);
    add_exit(MURIVEL_WSCHOD_PARK +"lokacje/fontanna.c","w",0,1,0);

}

public string
exits_description() 
{
    return "Alejka prowadzi na p^o^lnocny zach^od oraz po^ludniowy zach^od, "+
        "^scie^zka na zachodzie prowadzi do fontanny. \n";
}
//ten opis wyjscia trzeba poprawic, tak tylko napisalam ;p

string
dlugi_opis()
{
    string str;
    
    str = "\n";

    return str;
}
