/*
 * Opis:
 * Autor: 
 * Data:
 * 
 */

inherit "/std/door";

#include <pl.h>
#include "dir.h"

void
create_door()
{
    ustaw_nazwe("drzwi"); 
    
    set_other_room(MURIVEL_WSCHOD_ULICE + "u15.c"); 
    set_door_id("DRZWI_DO_STRAZNICY_MURIVEL_WSCHOD");
    set_door_desc("\n");

    set_open_desc("");
    set_closed_desc("");
        
    set_pass_command(({"","",""}));
    
    set_lock_command("zamknij");
    set_unlock_command("otworz");

    set_open(0);
    set_locked(1);

//    set_key("");
    set_lock_name("zamek");
}