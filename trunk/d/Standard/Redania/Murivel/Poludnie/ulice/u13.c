/* Autor: 
   Opis : 
   Data : */

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"
#include <pogoda.h>

inherit MURIVEL_STD;

void
create_ulica()
{
    set_short("Ulica");
    add_exit(MURIVEL_POLUDNIE_ULICE + "u12.c", "ne");

    add_object(MURIVEL_POLUDNIE_DRZWI + "do_karczmy");

    add_object(MURIVEL_PRZEDMIOTY + "lampa_uliczna");

}

public string
exits_description() 
{
    return "Uliczka prowadzi na p^o^lnocny wsch^od, na zachodzie znajduje "+
        "si^e karczma, za^s na wschodzie - jaki^s ciemny i nieprzyjemny "+
        "zau^lek. \n";
}

string
dlugi_opis()
{
    string str;
    if(jest_dzien())
    {
        str = "Dzie^n.";
    }
    else
    {
        str = "Noc.";
    }
    str += "\n";
    return str;
}