/* Autor: 
   Opis : 
   Data : */

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"
#include <pogoda.h>

inherit MURIVEL_STD;

void
create_ulica()
{
    set_short("Uliczka");
    add_exit(MURIVEL_POLUDNIE_ULICE + "u05.c", "w",0,1,0);
    add_exit(MURIVEL_POLUDNIE + "studnia.c", "e",0,1,0);

    add_object(MURIVEL_POLUDNIE_DRZWI + "do_piekarni");

    add_object(MURIVEL_PRZEDMIOTY + "lampa_uliczna");

}

public string
exits_description() 
{
    return "Uliczka biegnie z zachodu ku placyku ze studni^a na "+
        "wschodzie. Na p^o^lnocy znajduje si^e wej^scie do piekarni. \n"; 
    //z tym placykiem to strzelam
    // nie wiem jaki opis jest, to si^e to zmieni, ot.
}

string
dlugi_opis()
{
    string str;
    if(jest_dzien())
    {
        str = "Dzie^n.";
    }
    else
    {
        str = "Noc.";
    }
    str += "\n";
    return str;
}