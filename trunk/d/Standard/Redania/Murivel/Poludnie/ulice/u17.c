/* Autor: 
   Opis : 
   Data : */

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"
#include <pogoda.h>

inherit MURIVEL_STD;

void
create_ulica()
{
    set_short("Uliczka");
    add_exit(MURIVEL_POLUDNIE_ULICE + "u14.c", "nw",0,1,0);

    add_object(MURIVEL_POLUDNIE_DRZWI + "do_zabawek");

    add_object(MURIVEL_PRZEDMIOTY + "lampa_uliczna");
}


public string
exits_description() 
{
    return "Uliczka prowadzi na p^o^lnocny zach^od, na wschodzie znajduje "+
        "si^e wej^scie do sklepu. \n";
}

string
dlugi_opis()
{
    string str;
    if(jest_dzien())
    {
        str = "Dzie^n.";
    }
    else
    {
        str = "Noc.";
    }
    str += "\n";
    return str;
}