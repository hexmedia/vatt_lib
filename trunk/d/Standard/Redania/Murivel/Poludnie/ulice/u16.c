/* Autor: 
   Opis : 
   Data : */

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"
#include <pogoda.h>

inherit MURIVEL_STD;

void
create_ulica()
{
    set_short("Uliczka");
    add_exit(MURIVEL_POLUDNIE_ULICE + "u15.c","sw",0,1,0);
    add_exit(MURIVEL_POLUDNIE + "moste","n",0,1,0);

add_object(MURIVEL_PRZEDMIOTY + "lampa_uliczna");
}

public string
exits_description() 
{
    return "Uliczka prowadzi na po^ludniowy zach^od, za^s na p^o^lnocy "+
        "znajduje si^e most na Pontarze. \n";
}

string
dlugi_opis()
{
    string str;
    if(jest_dzien())
    {
        str = "Dzie^n.";
    }
    else
    {
        str = "Noc.";
    }
    str += "\n";
    return str;
}