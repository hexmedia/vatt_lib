/* Autor: 
   Opis : 
   Data : */

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"
#include <pogoda.h>

inherit MURIVEL_STD;

void
create_ulica()
{
    set_short("Uliczka.");
    add_exit(MURIVEL_POLUDNIE_ULICE + "u04.c", "nw",0,1,0);
    add_exit(MURIVEL_POLUDNIE_ULICE + "u06.c", "e",0,1,0);

    add_object(MURIVEL_PRZEDMIOTY + "lampa_uliczna");

}

public string
exits_description() 
{
    return "Uliczka prowadzi z p^o^lnocnego zachodu na wsch^od. \n";
}

string
dlugi_opis()
{
    string str;
    if(jest_dzien())
    {
        str = "Dzie^n.";
    }
    else
    {
        str = "Noc.";
    }
    str += "\n";
    return str;
}