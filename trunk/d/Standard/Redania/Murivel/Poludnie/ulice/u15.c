/* Autor: 
   Opis : 
   Data : */

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"
#include <pogoda.h>

inherit MURIVEL_STD;

void
create_ulica()
{
    set_short("Uliczka");
    add_exit(MURIVEL_POLUDNIE_ULICE + "u16.c", "ne");
    add_exit(MURIVEL_POLUDNIE_ULICE + "u14.c", "sw");

    add_object(MURIVEL_POLUDNIE_DRZWI + "do_rzezni");
    add_object(MURIVEL_POLUDNIE_DRZWI + "do_rybnego");
      

    add_object(MURIVEL_PRZEDMIOTY + "lampa_uliczna");
}

public string
exits_description() 
{
    return "Na wschodzie znajduje si� rze�nia, a na p�nocnym zachodzie - "+
        "zak�ad rybny. Uliczka prowadzi z po^ludnowego zachodu na "+
        "p^o^lnocny wsch^od. \n";
}

string
dlugi_opis()
{
    string str;
    if(jest_dzien())
    {
        str = "Dzie^n.";
    }
    else
    {
        str = "Noc.";
    }
    str += "\n";
    return str;
}