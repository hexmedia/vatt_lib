/* Autor: Avard
   Data : 08.04.07
   Opis : Valor i Faeve */

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <pogoda.h>
#include "dir.h"

inherit RINDE + "std/kanaly.c";

void 
create_kanaly() 
{
    set_short("Kana^ly");
    add_prop(ROOM_I_INSIDE,1);
    add_exit(KANALY_LOKACJE + "kanaly8.c","s",0,1,0);
    add_exit(KANALY_LOKACJE + "kanaly11.c","n",0,1,0);
    add_npc(RINDE_NPC + "szczur.c");
    add_prop(ROOM_I_LIGHT, 0);
}
public string
exits_description() 
{
    return "Kana^ly ci^agn^a si^e na p^o^lnocy i po^ludniu.\n";
}
string
dlugi_opis()
{
    string str;
    str = "Poruszanie si^e po tym tunelu, wyj^atkowo utrudnia gruba "+
        "warstwa b^lota, o do^s^c dziwnym kolorze. Zapach unosz^acy "+
        "si^e w powietrzu jest mniej intensywny ni^z w g^lebi kana^l^ow "+
        "jednak nadal kr^eci w nosie. Korytarz zw^e^za si^e prowadz^ac w "+
        "kierunku p^o^lnocnym lecz przedostanie si^e tam jest troche "+
        "urudnione z "+
        "powodu zawa^lu jednej ze ^scian. \n"; 
    return str;
}
