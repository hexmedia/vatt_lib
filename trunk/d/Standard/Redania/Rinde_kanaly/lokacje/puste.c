/* Autor: Avard
   Data : 08.04.07
   Opis : */

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <pogoda.h>
#include "dir.h"

inherit "/d/Aretuza/avard/kanaly/kanaly.c";

void 
create_kanaly() 
{
    set_short("Ej");
    add_prop(ROOM_I_INSIDE,1);
    add_exit("/d/Aretuza/avard/kanaly/lokacje/kanaly8.c","w",0,1,0);
    add_object("/d/Aretuza/avard/kanaly/drzwi/furtka_z");
}

string
dlugi_opis()
{
    string str;
    str = "Nie powinno Ci� tu by�, zg�o� b��d.\n";    
    return str;
}
