/* Autor: Avard
   Data : 08.04.07
   Opis : Valor i Faeve */

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <pogoda.h>
#include "dir.h"

inherit RINDE + "std/kanaly.c";

void 
create_kanaly() 
{
    set_short("Przy kracie");
    add_prop(ROOM_I_INSIDE,1);
    add_exit(KANALY_LOKACJE + "kanaly8.c","w",0,1,0);
    add_object(KANALY_DRZWI + "furtka_z");
    add_npc(RINDE_NPC + "szczur.c");
    add_npc(RINDE_NPC + "szczur.c");
}
public string
exits_description() 
{
    return "Kana^ly ci^agn^a si^e na zachodzie.\n";
}
string
dlugi_opis()
{
    string str;
    str = "P^o^lokr^ag^ly tunel szeroki na dwa krasnoludy oraz tak wysoki, "+
        "^ze spokojnie zmie^sci si^e tu bardzo wysoki elf. W tym miejscu "+
        "przedzielony jest stalow^a krat^a. Mrok roz^swietlony przez "+
        "niesione z zza kraty ^swiat^lo pozwala ci zauwa^zy^c pokryte "+
        "mchem ^sciany oraz ma^ly strumyk nieczysto^sci p^lyn^acy przez "+
        "^srodek korytarza. Zapach unosz^acy si^e w powietrzu przyprawia "+
        "ci^e o md^lo^sci.\n";    
    return str;
}
