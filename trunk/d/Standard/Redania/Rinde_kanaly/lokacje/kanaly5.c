/* Autor: Avard
   Data : 08.04.07
   Opis : Valor i Faeve */

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <pogoda.h>
#include "dir.h"

inherit RINDE + "std/kanaly.c";

void 
create_kanaly() 
{
    set_short("Komnata");
    add_prop(ROOM_I_INSIDE,1);
    add_object(KANALY_OBIEKTY + "oltarz.c");
    add_object(KANALY_OBIEKTY + "sarkofag",3);
    dodaj_rzecz_niewyswietlana("zdobiony kamienny sarkofag",3);
    add_exit(KANALY_LOKACJE + "kanaly11.c","e",0,1,0);
    add_npc(RINDE_NPC + "szczur.c");

    add_item(({"malunki","rysunki","obrazy","freski"}),"Wykute w kamieniu, "+
        "kiedy^s zapewne pi^ekne, obecnie zniszczone, poobijane i pokryte "+
        "kurzem. Przedstawiaj^a sylwetki elf^ow w r^o^znych sytuacjach. "+
        "Najmiej zniszczona, a zarazem najwi^eksza pokazuje rytua^l "+
        "poch^owku. Stoj^ace w okr^egu elfy pochylaj^a si^e nad cia^lem "+
        "zmar^lego, kt^ory le^zy na kamiennej p^lycie trzymaj^ac na "+
        "piersiach d^lugi ^luk.\n");
    add_prop(ROOM_I_LIGHT, 0);

}
public string
exits_description() 
{
    return "Kana^ly ci^agn^a si^e na wschodzie.\n";
}
string
dlugi_opis()
{
    string str;
    str = "Kwadratowe pomieszczenie ma tylko jedno wyj^scie prowadz^ace "+
        "na wsch^od. We wszystkich ^scianach wyrze^xbione zosta^ly "+
        "p^o^lokr^ag^le wn^eki, a w ka^zdej z nich le^zy sarkofag. "+
        "Ca^l^a pod^log^e przykrywaj^a porozrzucane ko^sci, a na "+
        "samym ^srodku stoi kamienny, rze^xbiony o^ltarz pokryty "+
        "wyj^atkowo starannie wykonanymi ornamentami. Na suficie "+
        "b^ed^acym do^s^c p^lytk^a kopu^l^a dostrzec mo^zna niewyra^xne "+
        "freski przedstawiaj^ace sceny z ^zycia elf^ow. \n";    
    return str;
}
