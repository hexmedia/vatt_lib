/* Autor: Avard
   Data : 08.04.07
   Opis : Sniegulak */

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <pogoda.h>
#include "dir.h"
inherit RINDE + "std/kanaly.c";

void 
create_kanaly() 
{
    set_short("Kana^ly");
    add_prop(ROOM_I_INSIDE,1);
    add_exit(KANALY_LOKACJE + "kanaly4.c","s",0,1,0);
    add_exit(KANALY_LOKACJE + "kanaly5.c","w",0,1,0);
    add_exit(KANALY_LOKACJE + "kanaly6.c","e",0,1,0);
    add_exit(KANALY_LOKACJE + "kanaly12.c","n",0,1,0);
    add_npc(RINDE_NPC + "szczur.c");
    add_prop(ROOM_I_LIGHT, 0);
}
public string
exits_description() 
{
    return "Kana^ly ci^agn^a si^e na p^o^lnocy, po^ludniu, wschodzie "+
        "i zachodzie.\n";
}

string
dlugi_opis()
{
    string str;
    str = "Znajdujesz si^e w komnacie pe^lni^acej funkcj^e skrzy^zowania. "+
        "^L^acz^a si^e tu cztery korytarze i dwa r^o^zni^ace si^e od siebie "+
        "style budowy. Jeden prosty i bez zdobe^n, zastosowany zosta^l przy "+
        "budowie korytarza p^o^lnocnego. Jest on szerszy ni^z pozosta^le i "+
        "troche ni^zszy, kamie^n zosta^l mizernie okuty, a spoina wypada "+
        "przy dotyku. Trzy korytarze - wschodni, zachodni i po^ludniowy s^a "+
        "nieco wy^zsze, ale w^e^zsze. Wykute ^sciany s^a zdobione i bardzo "+
        "solidnie wykonane. Gdyby przyjrze^c si^e dok^ladnie mo^zna "+
        "zauway^c i^z korytarze te s^a starsze, lecz jednocze^snie lepiej "+
        "zachowane. Stare i cuchn^ace powietrze przy g^l^ebszym wdechu "+
        "pozostawia niemi^ly posmak w ustach. \n"; 
    return str;
}
