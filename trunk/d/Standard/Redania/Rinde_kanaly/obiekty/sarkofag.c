/* Autor: Avard
   Opis : Faeve
   Data : 9.04.07 */

inherit "/std/object";

#include "/sys/stdproperties.h"
#include "/sys/macros.h"
#include "/sys/pl.h"
#include "/sys/materialy.h"
#include "/sys/object_types.h"

int otworz(string str);
void
create_object()
{
    ustaw_nazwe("sarkofag");
    dodaj_przym("zdobiony", "zdobieni");
    dodaj_przym("kamienny", "kamienni");
    ustaw_material(MATERIALY_MARMUR);
    set_type(O_INNE);
    set_long("Kamienna trumna ustawiona na do^s^c wysokim cokole jest "+
        "bogato zdobiona jakimi^s p^laskorze^xbami, przedstawiaj^acmi "+
        "sceny z ^zycia codziennego. Wieko sarkofagu przypomina nieco "+
        "^lo^ze, na kt^orym u^lo^zone s^a zw^loki. Rze^xbiarz musia^l "+
        "by^c nie lada artyst^a - posta^c wykuto wyj^atkowo starannie. "+
        "Delikatne rysy twarzy, na wp^o^l przymkni^ete oczy, lekko "+
        "rozchylone usta. Posta^c wygl^ada prawie jak ^zywa, jakgdyby "+
        "mia^la zaraz powsta^c. Jedynie sino^s^c g^lazu, przypomina o "+
        "tym, ^ze jest tylko rze^xb^a.\n");
             
    add_prop(OBJ_I_WEIGHT, 50000); 
    add_prop(OBJ_I_VOLUME, 100000); 
    add_prop(OBJ_I_VALUE, 200);

    make_me_sitable("na","na sarkofagu","z sarkofagu",2);


}
init()
{
   ::init();
   add_action(otworz, "otw^orz");
}

int
otworz(string str)
{
    object sarkofag;
    if (!str) 
    return 0;
    if(parse_command(str,environment(this_object()),"%o:" + PL_BIE,sarkofag))
    {

            write("Z ca^lej sily pr^obujesz odepchn^a^c wieko sarkofagu - "+
                "niestety nieskutecznie.\n");
            saybb(QCIMIE(this_player(), PL_MIA) +" z ca^lej si^ly probuje "+
                "odepchn^a^c wieko sarkofagu - niestety nieskutecznie.\n");
            return 1;
    }
    notify_fail("Otw^orz co?\n");
    return 0;
}