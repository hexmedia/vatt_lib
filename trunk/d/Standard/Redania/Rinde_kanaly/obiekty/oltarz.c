/* Autor: Avard
   Opis : Sniegulak
   Data : 9.04.07 */

inherit "/std/object";

#include "/sys/stdproperties.h"
#include "/sys/macros.h"
#include "/sys/pl.h"
#include "/sys/materialy.h"
#include "/sys/object_types.h"

void
create_object()
{
    ustaw_nazwe("o^ltarz");
    dodaj_przym("grawerowany", "grawerowani");
    dodaj_przym("obsydianowy", "obsydianowi");
    ustaw_material(MATERIALY_MARMUR);
    set_type(O_INNE);
    set_long("Czarny obsydian wykorzystany do budowy tego o^ltarza, lekko "+
        "opalizuje w ^swietle. Powierzchnia bogata w ornamenty, dzi^eki "+
        "poruszaj^acym si^e  cieniom, wygl^ada jak ^zywa i przyci^aga "+
        "wzrok. Nie jeste^s w stanie ich odczyta^c, gdy^z grawerowaniana "+
        "o^ltarza zosta^ly ju^z nadwyr^e^zone i w wi^ekszo^sci zatarte "+
        "przez czas. Gdzieniegdzie mo^zna odr^o^znic na niej ma^le "+
        "piktogramy przedstawiaj^ace humanoidalne czaszki.\n");
             
    add_prop(OBJ_I_WEIGHT, 50000); 
    add_prop(OBJ_I_VOLUME, 100000); 
    add_prop(OBJ_I_VALUE, 200);    
    add_prop(OBJ_I_DONT_SHOW_IN_LONG, 1);
    add_prop(OBJ_I_NO_GET,"Nie dasz rady tego podnie^s^c.\n");

    make_me_sitable("na","na o^ltarzu","z o^ltarza",6);
}
