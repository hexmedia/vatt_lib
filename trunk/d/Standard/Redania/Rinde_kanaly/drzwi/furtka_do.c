
/* Autor: Avard
   Opis : Faeve
   Data : 10.04.07*/

inherit "/std/door";

#include <pl.h>
#include "dir.h"
void
create_door()
{
    ustaw_nazwe("furtka");
    dodaj_przym("stalowy","stalowi");

    set_other_room(KANALY_LOKACJE + "pusta.c");
    set_door_id("FURTKA_DO_KANALOW_RINDE");
    set_door_desc("Przed sob^a widzisz ^zelazn^a krat^e zagradzaj^ac^a "+
        "wej^scie do kana^l^ow. Postawiona stosunkowo niedawno, ma zapewne "+
        "choni^c przed ^latwym dost^epem do kana^l^ow i miasta. Po ^srodku "+
        "zauwa^zasz ma^l^a furtk^e. Zamek w niej tkwi^acy wygl^ada na "+
        "solidny i trwa^ly - wystarczaj^aco mocny, by nawet dobrze "+
        "wykwalifikowany rzezimieszek nie poradzi^l sobie z nim.\n");

    set_open_desc("");
    set_closed_desc("");
    set_open(0);
    set_locked(1);

    set_pass_command(({"furtka","stalowa furtka","wyj�cie"}),"przez furtk�",
        "wychodz^ac z kana��w");

    set_key("KLUCZ_FURTKA_DO_KANALOW_RINDE");
    set_lock_name(({"zamek", "zamka", "zamkowi", "zamek", "zamkiem", "zamku"}), 0, PL_MESKI_NOS_NZYW);
}