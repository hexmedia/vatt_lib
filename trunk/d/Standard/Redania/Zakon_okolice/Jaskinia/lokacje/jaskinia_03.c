//inherit "/std/room";
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <pogoda.h>
#include "dir.h"
inherit JASKINIA_U_STD;

void
dodaj_wyjscia_na_02()
{
//	remove_exit("ty^l");
//	remove_exit("lewo");
//	remove_exit("prawo");
	add_exit(JASKINIA_LOKACJE+"jaskinia_02.c",({"ty^l","do ty^lu",
		   "z jednego z korytarzy" 	}), &check("02","02"),J_FA,&check("02","02"));
	add_exit(JASKINIA_LOKACJE+"jaskinia_02.c",({"lewo","w lewo",
		   "z jednego z korytarzy" 	}), &check("02","06"),J_FA,&check("02","06"));
//	ugly_update_action("ty^l",1);
//	ugly_update_action("lewo",1);
}



void
create_jaskinia()
{
    set_short("^Slepy korytarz");
    add_prop(ROOM_I_INSIDE,1);

	add_exit(JASKINIA_LOKACJE+"jaskinia_06.c",({"ty^l","do ty^lu",
			   "z jednego z korytarzy" 	}), &check("06","06"),J_FA,&check("06","06"));
	add_exit(JASKINIA_LOKACJE+"jaskinia_06.c",({"prawo","w prawo",
			   "z jednego z korytarzy" 	}), &check("06","02"),J_FA,&check("06","02"));


    add_npc(JASKINIA_LIVINGI+"utopiec.c");
    add_npc(JASKINIA_LIVINGI+"utopiec.c");
    add_prop(ROOM_I_LIGHT, 0);
    add_item("^sciany",
        "^Sciany, wy^z^lobione z biegiem lat, przez zalegaj^ac^a tutaj "+
        "prze^smierd^l^a wod^e, pokryte s^a dziwn^a ro^slinno^sci^a "+
        "przypominaj^ac^a glony. Nasi^akni^ete wod^a i zwieszaj^ace si^e "+
        "ze stropu, wygladaj^a na wyj^atkowo o^slizg^le i niemi^le w "+
        "dotyku. \n");

    add_item("szcz^atki",
        "Te szcz^atki, unosz^ace si^e na powierzchni wody, przypominaj^a "+
        "kszta^ltem, barw^a, faktur^a, czy te^z po prostu smrodem, resztki "+
        "ubra^n, buty, deski, a nawet co^s, na wzr^or cia^la jakiego^s "+
        "nieszcz^e^sliwego badacza owej jaskini. Zalegaj^ace gdzieniegdzie "+
        "truch^la drobnych zwierz^at pot^eguj^a wo^n padliny. \n");


        set_event_time(250.0+itof(random(100)));
    add_event("Jaki^s obrzydliwy zapach zdaje si^e dobiega^c z g^l^ebi "+
			"jaskini.\n");
    add_event("Kilka kropel wody spad^lo z sufitu. \n");
    add_event("Powierzchnia wody zafalowa^la lekko. \n");

}

string
dlugi_opis()
{
    string str;
    str = "W^aski korytarz ko^nczy si^e w tym miejscu ^scian^a, pokryt^a "+
        "^sluzowatymi ro^slinami, czy glonami, za^s mulista woda zalewa "+
        "jaskini^e na niemal dwie stopy. Ponadto go^sci liczne szcz^atki i "+
        "odpady, w g^l^ownej mierze podarte ubrania i deski. Z niskiego "+
        "stropu zwieszaj^a si^e wapienne stalaktyty, ociekaj^ace wod^a, "+
        "kt^orej drobne kropelki z pluskiem rozbijaj^a si^e o wystaj^ace "+
        "gdzieniegdzie ska^ly. \n";

    return str;
}
