//inherit "/std/room";
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <pogoda.h>
#include "dir.h"
inherit JASKINIA_U_STD;

void
dodaj_wyjscia_na_05()
{
//	remove_exit("ty�");
//	remove_exit("lewo");
//	remove_exit("prawo");
	add_exit(JASKINIA_LOKACJE+"jaskinia_05.c",({"ty�","do ty�u",
		   "z jednego z korytarzy" 	}), &check("05","05"),J_FA,&check("05","05"));
	add_exit(JASKINIA_LOKACJE+"jaskinia_05.c",({"lewo","w lewo",
		   "z jednego z korytarzy" 	}), &check("05","11"),J_FA,&check("05","11"));
	add_exit(JASKINIA_LOKACJE+"jaskinia_05.c",({"prawo","w prawo",
		   "z jednego z korytarzy" 	}), &check("05","10"),J_FA,&check("05","10"));
/*
	ugly_update_action("ty�",1);
	ugly_update_action("lewo",1);
	ugly_update_action("prawo",1);*/
}




void
create_jaskinia()
{
    set_short("Du^za grota");
    add_prop(ROOM_I_INSIDE,1);

	add_exit(JASKINIA_LOKACJE+"jaskinia_10.c",({"prz�d","do przodu",
		   "z jednego z korytarzy" 	}), &check("10","11"),J_FA,&check("10","11"));
	add_exit(JASKINIA_LOKACJE+"jaskinia_11.c",({"prz�d","do przodu",
		   "z jednego z korytarzy" 	}), &check("11","10"),J_FA,&check("11","10"));

	add_exit(JASKINIA_LOKACJE+"jaskinia_10.c",({"ty�","do ty�u",
		   "z jednego z korytarzy" 	}), &check("10","10"),J_FA,&check("10","10"));
	add_exit(JASKINIA_LOKACJE+"jaskinia_11.c",({"ty�","do ty�u",
		   "z jednego z korytarzy" 	}), &check("11","11"),J_FA,&check("11","11"));

	add_exit(JASKINIA_LOKACJE+"jaskinia_10.c",({"lewo","w lewo",
		   "z jednego z korytarzy" 	}), &check("10","05"),J_FA,&check("10","05"));

	add_exit(JASKINIA_LOKACJE+"jaskinia_11.c",({"prawo","w prawo",
		   "z jednego z korytarzy" 	}), &check("11","05"),J_FA,&check("11","05"));


	add_prop(ROOM_I_LIGHT, 0);
    add_item("^sciany",
        "^Sciany, wy^z^lobione z biegiem lat, przez zalegaj^ac^a tutaj "+
        "prze^smierd^l^a wod^e, pokryte s^a dziwn^a ro^slinno^sci^a "+
        "przypominaj^ac^a glony. Nasi^akni^ete wod^a i zwieszaj^ace si^e "+
        "ze stropu, wygladaj^a na wyj^atkowo o^slizg^le i niemi^le w "+
        "dotyku. \n");

    add_item("szcz^atki",
        "Te szcz^atki, unosz^ace si^e na powierzchni wody, przypominaj^a "+
        "kszta^ltem, barw^a, faktur^a, czy te^z po prostu smrodem, resztki "+
        "ubra^n, buty, deski, a nawet co^s, na wzr^or cia^la jakiego^s "+
        "nieszcz^e^sliwego badacza owej jaskini. Zalegaj^ace gdzieniegdzie "+
        "truch^la drobnych zwierz^at pot^eguj^a wo^n padliny. \n");


        set_event_time(250.0+itof(random(100)));
    add_event("Jaki^s obrzydliwy zapach zdaje si^e dobiega^c z g^l^ebi "+
			"jaskini.\n");
    add_event("Kilka kropel wody spad^lo z sufitu. \n");
    add_event("Powierzchnia wody zafalowa^la lekko. \n");

}

string
dlugi_opis()
{
    string str;
    str = "Korytarz dzieli si^e w tym miejscu na dwa pomiejsze, r^ownie "+
        "ziej^ace ch^lodem i wilgotne. Woda zalewaj^aca przej^scie jest "+
        "zupe^lniebrudna i mulista, a pojej  powierzchni p^lywaj^a "+
        "szcz^atki, niezbyt jasnego pochodzenia. Nagie, wapienne ^sciany "+
        "jaskini prezentuj^a w pe^lni swe majestatyczne ^z^lobienia i "+
        "formacje, za^s z wysokiego sklepienia zwieszaj^a si^e kolosalnych "+
        "rozmiar^ow stalaktyty. \n";

    return str;
}
