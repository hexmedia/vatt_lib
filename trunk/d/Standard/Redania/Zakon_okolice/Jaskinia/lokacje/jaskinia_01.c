//inherit "/std/room";
#include "dir.h"
inherit JASKINIA_U_STD;
#include <stdproperties.h>
#include <macros.h>
#include <ss_types.h>
#include <pl.h>
#include <object_types.h>
string dlugi_opis();

void
create_jaskinia()
{
    LOAD_ERR(JASKINIA_LOKACJE+"jaskinia_05");

    set_short("Zalane wej^scie");
    set_long(&dlugi_opis());

    add_exit("jaskinia_02.c", ({"prz^od","do przodu","z jednego z korytarzy"}),&check("02","xxx",1),
        J_FA,&check("02","xxx",1));
    add_exit("jaskinia_02.c",({"ty^l","do ty^lu","z jednego z korytarzy"}),&check("02","02"),
        J_FA,&check("02","02"));

    add_item("otw^or",
        "Ta szczelina w stropie jaskini jest do^s^c d^luga i niezbyt "+
        "szeroka, cho^c przy sporym wysi^lku najprawdopodobniej mo^zna "+
        "by by^lo si^e przez ni^a przecisn^a^c. Najwyra^xniej prowadzi "+
        "na zew^atrz, na co wskazuj^a lekkie powiewy ^swie^zego "+
        "powietrza.\n");
    add_item("^sciany",
        "^Sciany, wy^z^lobione z biegiem lat, przez zalegaj^ac^a tutaj "+
        "prze^smierd^l^a wod^e, pokryte s^a dziwn^a ro^slinno^sci^a "+
        "przypominaj^ac^a glony. Nasi^akni^ete wod^a i zwieszaj^ace si^e "+
        "ze stropu, wygladaj^a na wyj^atkowo o^slizg^le i niemi^le w "+
        "dotyku. \n");
    add_item("szcz^atki",
        "Te szcz^atki, unosz^ace si^e na powierzchni wody, przypominaj^a "+
        "kszta^ltem, barw^a, faktur^a, czy te^z po prostu smrodem, resztki "+
        "ubra^n, buty, deski, a nawet co^s, na wzr^or cia^la jakiego^s "+
        "nieszcz^e^sliwego badacza owej jaskini. Zalegaj^ace gdzieniegdzie "+
        "truch^la drobnych zwierz^at pot^eguj^a wo^n padliny. \n");

    set_event_time(250.0+itof(random(100)));

    add_event("Powiew ^swie^zego powietrza dochodzi ci^e od "+
        "strony stropu.\n");
    add_event("Jaki^s obrzydliwy zapach zdaje si^e dobiega^c z g^l^ebi "+
        "jaskini.\n");
    add_event("Kilka kropel wody spad^lo z sufitu.\n");
    add_event("Powierzchnia wody zafalowa^la lekko.\n");

    add_prop(ROOM_I_INSIDE,1);
    remove_prop(ROOM_I_LIGHT);
    add_prop(ROOM_S_DARK_LONG, "W ciemnym korytarzu.\n");
}

string
dlugi_opis()
{
    string str;

    str="Jedynym ^xr^od^lem ^swiat^la tej ciemnej, cuchn^acej padlin^a "+
        "jaskini jest niewielki otw^or u g^ory, b^ed^acy w^ask^a szczelin^a "+
        "w wapiennym sklepieniu. Zalana do po^lowy wod^a na mniej wi^ecej "+
        "trzy stopy zdaje si^e by^c w tym miejscu pocz^atkiem korytarza, "+
        "ci^agn^acego si^e w jednym kierunku, cho^c z czasem nikn^acego w "+
        "ca^lkowitym mroku. Jaskinia jest dosy^c szeroka by min^e^ly si^e "+
        "w niej trzy osoby i wysoka na pi^e^c ^lokci, co pozwala na do^s^c "+
        "du^z^a swobod^e ruch^ow. ^Sciany niemal zupe^lnie pokry^ly si^e "+
        "^sluzowatymi ro^slinami, czy glonami, za^s po powierzchni "+
        "^smierdz^acej, m^etnej wody p^lywaj^a marne szcz^atki niejasnego "+
        "pochodzenia.\n";
    return str;
}

void
wyjdz(object player)
{
        player->move(TRAKT_ZAKON_OKOLICE_LOKACJE +"sj17");
        player->do_glance(2);
        saybb(QCIMIE(TP,PL_MIA)+" wygrzebuje si^e z g^l^ebokiej dziury "+
            "ukrytej pod zwalonym pniem. \n");
        return;
}

int
przejdz(string str)
{
    if(TP->query_prop(PLAYER_M_SIT_SIEDZACY) || TP->query_prop(PLAYER_M_SIT_LEZACY))
        return notify_fail("Musisz najpierw wsta�!\n");

    if(query_verb() ~= "przeci^snij")
    {
        notify_fail("Przeci^snij sie przez co?\n");

        if(!(str ~= "si^e przez otw^or" || str~="si^e przez dziur^e" ||
            str ~="si^e przez otw^or na d^o^l" ||
            str ~="si^e przez dziur^e na d^o^l" ||
            str ~= "sie na d^o^l"))
        {
            return 0;
        }
    }
    else if(query_verb() ~= "przejd^x")
    {
        notify_fail("Przejd^x przez co?\n");

        if(!(str ~= "przez otw^or" || str ~= "przez dziur^e"))
            return 0;
    }
    else if(query_verb() ~= "wejd�")
    {
        notify_fail("Wejd� gdzie?\n");

        if(!(str ~= "w otw�r" || str ~= "w dziur�"))
            return 0;
    }
    else
        return 0;

    object paraliz=clone_object("/std/paralyze");
    paraliz->set_fail_message("");
    paraliz->set_finish_object(this_object());
    paraliz->set_finish_fun("wyjdz");
    paraliz->set_remove_time(4);
    paraliz->setuid();
    paraliz->seteuid(getuid());
    paraliz->move(TP);
    write("Zaczynasz przeciska^c si^e przez otw^or.\n");
    saybb(QCIMIE(TP,PL_MIA)+" zaczyna przeciska^c si^e przez otw^or.\n");
    return 1;
}

void
init()
{
    ::init();
    add_action(przejdz, "przeci^snij");
    add_action(przejdz, "przejd^x");
    add_action(przejdz, "wejd�");
}
