//inherit "/std/room";
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <pogoda.h>
#include "dir.h"
inherit JASKINIA_U_STD;
int czy = random(2);

void
dodaj_wyjscia_na_03_prawo()
{
	add_exit(JASKINIA_LOKACJE+"jaskinia_03.c",({"prawo","w prawo",
			   "z jednego z korytarzy" 	}), &check("03","05"),J_FA,&check("03","05"));
}
void
dodaj_wyjscia_na_03_lewo()
{
	add_exit(JASKINIA_LOKACJE+"jaskinia_03.c",({"lewo","w lewo",
			   "z jednego z korytarzy" 	}), &check("03","01"),J_FA,&check("03","01"));
}
void
dodaj_wyjscia_na_03_tyl()
{
	add_exit(JASKINIA_LOKACJE+"jaskinia_03.c",({"ty�","do ty�u",
			   "z jednego z korytarzy" 	}), &check("03","03"),J_FA,&check("03","03"));

}
void
dodaj_wyjscia_na_03_przod()
{
	add_exit(JASKINIA_LOKACJE+"jaskinia_03.c",({"prz�d","do przodu",
			   "z jednego z korytarzy" 	}), &check("03","04"),J_FA,&check("03","04"));
}

void
create_jaskinia()
{
    set_short("Skrzy�owanie korytarzy");
    add_prop(ROOM_I_INSIDE,1);

	add_exit(JASKINIA_LOKACJE+"jaskinia_01.c",({"prz�d","do przodu",
			   "z jednego z korytarzy" 	}), &check("01","05"),J_FA,&check("01","05"));
	if(czy) dodaj_wyjscia_na_03_przod();
	add_exit(JASKINIA_LOKACJE+"jaskinia_04.c",({"prz�d","do przodu",
			   "z jednego z korytarzy" 	}), &check("04","03"),J_FA,&check("04","03"));
	add_exit(JASKINIA_LOKACJE+"jaskinia_05.c",({"prz�d","do przodu",
			   "z jednego z korytarzy" 	}), &check("05","01"),J_FA,&check("05","01"));

	add_exit(JASKINIA_LOKACJE+"jaskinia_01.c",({"ty�","do ty�u",
			   "z jednego z korytarzy" 	}), &check("01","01"),J_FA,&check("01","01"));
	if(czy) dodaj_wyjscia_na_03_tyl();
	add_exit(JASKINIA_LOKACJE+"jaskinia_04.c",({"ty�","do ty�u",
			   "z jednego z korytarzy" 	}), &check("04","04"),J_FA,&check("04","04"));
	add_exit(JASKINIA_LOKACJE+"jaskinia_05.c",({"ty�","do ty�u",
			   "z jednego z korytarzy" 	}), &check("05","05"),J_FA,&check("05","05"));

	add_exit(JASKINIA_LOKACJE+"jaskinia_01.c",({"lewo","w lewo",
			   "z jednego z korytarzy" 	}), &check("01","04"),J_FA,&check("01","04"));
	if(czy) dodaj_wyjscia_na_03_lewo();
	add_exit(JASKINIA_LOKACJE+"jaskinia_04.c",({"lewo","w lewo",
			   "z jednego z korytarzy" 	}), &check("04","05"),J_FA,&check("04","05"));
	add_exit(JASKINIA_LOKACJE+"jaskinia_05.c",({"lewo","w lewo",
			   "z jednego z korytarzy" 	}), &check("05","03"),J_FA,&check("05","03"));

	add_exit(JASKINIA_LOKACJE+"jaskinia_01.c",({"prawo","w prawo",
			   "z jednego z korytarzy" 	}), &check("01","03"),J_FA,&check("01","03"));
	if(czy) dodaj_wyjscia_na_03_prawo();
	add_exit(JASKINIA_LOKACJE+"jaskinia_04.c",({"prawo","w prawo",
			   "z jednego z korytarzy" 	}), &check("04","01"),J_FA,&check("04","01"));
	add_exit(JASKINIA_LOKACJE+"jaskinia_05.c",({"prawo","w prawo",
			   "z jednego z korytarzy" 	}), &check("05","04"),J_FA,&check("05","04"));

	if(czy)
	{
		LOAD_ERR(JASKINIA_LOKACJE+"jaskinia_03.c");
		find_object(JASKINIA_LOKACJE+"jaskinia_03.c")->dodaj_wyjscia_na_02();
	}


    add_npc(JASKINIA_LIVINGI+"utopiec.c");
    add_prop(ROOM_I_LIGHT, 0);
	add_prop(ROOM_I_TYPE,ROOM_CAVE);
    add_item("^sciany",
        "^Sciany, wy^z^lobione z biegiem lat, przez zalegaj^ac^a tutaj "+
        "prze^smierd^l^a wod^e, pokryte s^a dziwn^a ro^slinno^sci^a "+
        "przypominaj^ac^a glony. Nasi^akni^ete wod^a i zwieszaj^ace si^e "+
        "ze stropu, wygladaj^a na wyj^atkowo o^slizg^le i niemi^le w "+
        "dotyku. \n");

    add_item("szcz^atki",
        "Te szcz^atki, unosz^ace si^e na powierzchni wody, przypominaj^a "+
        "kszta^ltem, barw^a, faktur^a, czy te^z po prostu smrodem, resztki "+
        "ubra^n, buty, deski, a nawet co^s, na wzr^or cia^la jakiego^s "+
        "nieszcz^e^sliwego badacza owej jaskini. Zalegaj^ace gdzieniegdzie "+
        "truch^la drobnych zwierz^at pot^eguj^a wo^n padliny. \n");



    set_event_time(250.0+itof(random(100)));
    add_event("Intensywniejszy smr^od dochodzi ci^e "+
			"najprawdopodobniej z lewej strony. \n");
    add_event("Intensywniejszy smr^od dochodzi ci^e "+
			"najprawdopodobniej z prawej strony. \n");
	add_event("Kilka kropel wody spad^lo z sufitu. \n");
    add_event("Ka^zdy odg^los, nawet ten najcichszy rozbrzmiewa tu echem. \n");
    add_event("Powierzchnia wody zafalowa^la lekko. \n");
	add_event("�mierdzi tu czym� zgni�ym.\n");
}

string
dlugi_opis()
{
    string str;
    str = "Ta sporych rozmiar�w grota wy��obiona przez wod� w wapiennej "+
        "skale rozdziela si� na cztery, gin�ce w ciemno�ciach, korytarze. "+
        "Brudna, mulista woda zalewa jaskini� na jakie� dwie stopy, a po "+
        "jej powierzchni p�ywaj� rozmaite szcz�tki, b�d�ce zapewne "+
        "pozosta�o�ciami po ��dnych przyg�d odkrywcach. �ciany niemal "+
        "ca�kowicie pokry�y si� �luzowatymi ro�linami, czy glonami i "+
        "dodatkowo ociekaj� wod�, ��obi�c� w nich przedziwne formacje "+
        "skalne. \n";
    return str;
}
