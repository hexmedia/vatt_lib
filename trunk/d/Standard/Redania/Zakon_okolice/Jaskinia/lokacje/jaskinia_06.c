//inherit "/std/room";
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <pogoda.h>
#include "dir.h"
inherit JASKINIA_U_STD;


void
create_jaskinia()
{
    set_short("^Slepy korytarz");
    add_prop(ROOM_I_INSIDE,1);

	add_exit(JASKINIA_LOKACJE+"jaskinia_10.c",({"prz^od","do przodu",
		   "z jednego z korytarzy" 	}), &check("10","03"),J_FA,&check("10","03"));
	add_exit(JASKINIA_LOKACJE+"jaskinia_03.c",({"prz^od","do przodu",
		   "z jednego z korytarzy" 	}), &check("03","10"),J_FA,&check("03","10"));

	add_exit(JASKINIA_LOKACJE+"jaskinia_10.c",({"ty^l","do ty^lu",
		   "z jednego z korytarzy" 	}), &check("10","10"),J_FA,&check("10","10"));
	add_exit(JASKINIA_LOKACJE+"jaskinia_03.c",({"ty^l","do ty^lu",
		   "z jednego z korytarzy" 	}), &check("03","03"),J_FA,&check("03","03"));
	add_exit(JASKINIA_LOKACJE+"jaskinia_05.c",({"ty^l","do ty^lu",
		   "z jednego z korytarzy" 	}), &check("05","05"),J_FA,&check("05","05"));


	add_exit(JASKINIA_LOKACJE+"jaskinia_03.c",({"lewo","w lewo",
		   "z jednego z korytarzy" 	}), &check("03","05"),J_FA,&check("03","05"));
	add_exit(JASKINIA_LOKACJE+"jaskinia_05.c",({"lewo","w lewo",
		   "z jednego z korytarzy" 	}), &check("05","10"),J_FA,&check("05","10"));

	add_exit(JASKINIA_LOKACJE+"jaskinia_10.c",({"prawo","w prawo",
		   "z jednego z korytarzy" 	}), &check("10","05"),J_FA,&check("10","05"));
	add_exit(JASKINIA_LOKACJE+"jaskinia_05.c",({"prawo","w prawo",
		   "z jednego z korytarzy" 	}), &check("05","03"),J_FA,&check("05","03"));


    add_npc(JASKINIA_LIVINGI+"utopiec.c");
    add_npc(JASKINIA_LIVINGI+"utopiec.c");
    add_prop(ROOM_I_LIGHT, 0);
    add_item("^sciany",
        "^Sciany, wy^z^lobione z biegiem lat, przez zalegaj^ac^a tutaj "+
        "prze^smierd^l^a wod^e, pokryte s^a dziwn^a ro^slinno^sci^a "+
        "przypominaj^ac^a glony. Nasi^akni^ete wod^a i zwieszaj^ace si^e "+
        "ze stropu, wygladaj^a na wyj^atkowo o^slizg^le i niemi^le w "+
        "dotyku. \n");

    add_item("szcz^atki",
        "Te szcz^atki, unosz^ace si^e na powierzchni wody, przypominaj^a "+
        "kszta^ltem, barw^a, faktur^a, czy te^z po prostu smrodem, resztki "+
        "ubra^n, buty, deski, a nawet co^s, na wzr^or cia^la jakiego^s "+
        "nieszcz^e^sliwego badacza owej jaskini. Zalegaj^ace gdzieniegdzie "+
        "truch^la drobnych zwierz^at pot^eguj^a wo^n padliny. \n");


        set_event_time(250.0+itof(random(100)));
    add_event("Jaki^s obrzydliwy zapach zdaje si^e dobiega^c z g^l^ebi "+
			"jaskini.\n");
    add_event("Kilka kropel wody spad^lo z sufitu. \n");
    add_event("Powierzchnia wody zafalowa^la lekko. \n");

}

string
dlugi_opis()
{
    string str;
    str = "^Sciany tego ^lukowatego rozwidlenia cz^e^sciowo pokry^ly si^e "+
        "o^slizg^lymi, cuchn^acymi glonami, cho^c w po^lowie ukazuj^a "+
        "przedziwne formacje skalne. Dwa dalsze korytarze zdaj^a si^e by^c "+
        "jeszcze w^e^zsze i ni^zsze, cho^c nieco unosz^a si^e ku g^orze, "+
        "przez co zapewne woda nie jest tam tak g^l^eboka jak tutaj. Z "+
        "niskiego stropu zwieszaj^a si^e wapienne stalaktyty, tak^ze "+
        "ociekaj^ace wod^a, kt^orej drobne kropelki z pluskiem rozbijaj^a "+
        "si^e o wystaj^ace gdzieniegdzie ska^ly. \n";

    return str;
}
