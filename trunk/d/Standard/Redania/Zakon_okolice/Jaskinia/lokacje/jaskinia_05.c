//inherit "/std/room";
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <pogoda.h>
#include "dir.h"
inherit JASKINIA_U_STD;

void
dodaj_wyjscia_na_07()
{
	add_exit(JASKINIA_LOKACJE+"jaskinia_07.c",({"prawo","w prawo",
			   "z jednego z korytarzy" 	}), &check("07","02"),J_FA,&check("07","02"));
	add_exit(JASKINIA_LOKACJE+"jaskinia_07.c",({"lewo","w lewo",
			   "z jednego z korytarzy" 	}), &check("07","09"),J_FA,&check("07","09"));
	add_exit(JASKINIA_LOKACJE+"jaskinia_07.c",({"prz�d","do przodu",
			   "z jednego z korytarzy" 	}), &check("07","06"),J_FA,&check("07","06"));
	add_exit(JASKINIA_LOKACJE+"jaskinia_07.c",({"ty�","do ty�u",
			   "z jednego z korytarzy" 	}), &check("07","07"),J_FA,&check("07","07"));
}

void
dodaj_wyjscia_na_09()
{
	add_exit(JASKINIA_LOKACJE+"jaskinia_09.c",({"prawo","w prawo",
			   "z jednego z korytarzy" 	}), &check("09","07"),J_FA,&check("09","07"));
	add_exit(JASKINIA_LOKACJE+"jaskinia_09.c",({"lewo","w lewo",
			   "z jednego z korytarzy" 	}), &check("09","06"),J_FA,&check("09","06"));
	add_exit(JASKINIA_LOKACJE+"jaskinia_09.c",({"prz�d","do przodu",
			   "z jednego z korytarzy" 	}), &check("09","02"),J_FA,&check("09","02"));
	add_exit(JASKINIA_LOKACJE+"jaskinia_09.c",({"ty�","do ty�u",
			   "z jednego z korytarzy" 	}), &check("09","09"),J_FA,&check("09","09"));
}

void
create_jaskinia()
{
    set_short("Skrzy�owanie korytarzy");
    add_prop(ROOM_I_INSIDE,1);

	add_exit(JASKINIA_LOKACJE+"jaskinia_02.c",({"prz�d","do przodu",
			   "z jednego z korytarzy" 	}), &check("02","09"),J_FA,&check("02","09"));
	add_exit(JASKINIA_LOKACJE+"jaskinia_06.c",({"prz�d","do przodu",
			   "z jednego z korytarzy" 	}), &check("06","07"),J_FA,&check("06","07"));

	add_exit(JASKINIA_LOKACJE+"jaskinia_02.c",({"ty�","do ty�u",
			   "z jednego z korytarzy" 	}), &check("02","02"),J_FA,&check("02","02"));
	add_exit(JASKINIA_LOKACJE+"jaskinia_06.c",({"ty�","do ty�u",
			   "z jednego z korytarzy" 	}), &check("06","06"),J_FA,&check("06","06"));

	add_exit(JASKINIA_LOKACJE+"jaskinia_06.c",({"lewo","w lewo",
			   "z jednego z korytarzy" 	}), &check("06","02"),J_FA,&check("06","02"));
	add_exit(JASKINIA_LOKACJE+"jaskinia_02.c",({"lewo","w lewo",
			   "z jednego z korytarzy" 	}), &check("02","07"),J_FA,&check("02","07"));

	add_exit(JASKINIA_LOKACJE+"jaskinia_02.c",({"prawo","w prawo",
			   "z jednego z korytarzy" 	}), &check("02","06"),J_FA,&check("02","06"));
	add_exit(JASKINIA_LOKACJE+"jaskinia_06.c",({"prawo","w prawo",
			   "z jednego z korytarzy" 	}), &check("06","09"),J_FA,&check("06","09"));

	if(random(2))
	{
		LOAD_ERR(JASKINIA_LOKACJE+"jaskinia_07");
		dodaj_wyjscia_na_07();
		find_object(JASKINIA_LOKACJE+"jaskinia_07.c")->dodaj_wyjscia_na_05();
	}
	else
	{
		LOAD_ERR(JASKINIA_LOKACJE+"jaskinia_09");
		dodaj_wyjscia_na_09();
		find_object(JASKINIA_LOKACJE+"jaskinia_09.c")->dodaj_wyjscia_na_05();
	}


    add_npc(JASKINIA_LIVINGI+"utopiec.c");
    add_prop(ROOM_I_LIGHT, 0);
	add_prop(ROOM_I_TYPE,ROOM_CAVE);
    add_item("^sciany",
        "^Sciany, wy^z^lobione z biegiem lat, przez zalegaj^ac^a tutaj "+
        "prze^smierd^l^a wod^e, pokryte s^a dziwn^a ro^slinno^sci^a "+
        "przypominaj^ac^a glony. Nasi^akni^ete wod^a i zwieszaj^ace si^e "+
        "ze stropu, wygladaj^a na wyj^atkowo o^slizg^le i niemi^le w "+
        "dotyku. \n");

    add_item("szcz^atki",
        "Te szcz^atki, unosz^ace si^e na powierzchni wody, przypominaj^a "+
        "kszta^ltem, barw^a, faktur^a, czy te^z po prostu smrodem, resztki "+
        "ubra^n, buty, deski, a nawet co^s, na wzr^or cia^la jakiego^s "+
        "nieszcz^e^sliwego badacza owej jaskini. Zalegaj^ace gdzieniegdzie "+
        "truch^la drobnych zwierz^at pot^eguj^a wo^n padliny. \n");

        set_event_time(250.0+itof(random(100)));
    add_event("Intensywniejszy smr^od dochodzi ci^e "+
			"najprawdopodobniej z lewej strony. \n");
    add_event("Intensywniejszy smr^od dochodzi ci^e "+
			"najprawdopodobniej z prawej strony. \n");
	add_event("Kilka kropel wody spad^lo z sufitu. \n");
    add_event("Ka^zdy odg^los, nawet ten najcichszy rozbrzmiewa tu echem. \n");
    add_event("Powierzchnia wody zafalowa^la lekko. \n");
	add_event("�mierdzi tu czym� zgni�ym.\n");
}
/*exits_description()
{
    return "Do jaskini dochodz^a cztery korytarze: "+
			"p^o^lnocny, po^ludniowy, wschodni i zachodni.\n";
}*/

string
dlugi_opis()
{
    string str;
    str = "Sporych rozmiar^ow grota wy^z^lobiona przez wod^e w "+
        "wapiennej skale rozdziela si^e na trzy korytarze. "+
        "Jedne z przej^s^c s^a mniejsze i ni^zsze od pozosta�ych. "+
        "Woda zalewaj^aca przej^scie jest brudna i mulista, a po "+
        "powierzchni p^lywaj^a jakie^s szcz^atki, kt^orych nie mo^zesz "+
        "rozpozna^c. ^Sciany pokryte ^sluzowatymi ro^slinami s^a niemi^le "+
        "w dotyku i dodatkowo ociekaj^a wod^a, ^z^lobi^ac^a w nich "+
		"przedziwne formacje skalne.\n";
    return str;
}
