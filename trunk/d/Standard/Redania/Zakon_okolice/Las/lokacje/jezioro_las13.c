/*
 * Opis i kod OHM
 * Data: Monday 27th of August 2007 01:29:01 PM
 * Ostatnio edytowane: Avard
 * Data: 29.06.2009
 */

#include <pogoda.h>
#include <stdproperties.h>
#include <mudtime.h>
#include "dir.h"

inherit LAS_ZAKON_OKOLICE_STD;

void
create_las()
{
    set_short("W g^l^ebi wiekowego lasu.");
    set_long("@@dlugi_opis@@");
    add_exit(LAS_ZAKON_OKOLICE_LOKACJE + "jezioro_las14.c","e",0,LAS_FATIGUE,0);
    add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "sj04","s",0,SCIEZKA_ZO_FATIG,0);
    add_exit(LAS_ZAKON_OKOLICE_LOKACJE + "jezioro_las18.c","se",0,LAS_FATIGUE,0);
    add_exit(LAS_ZAKON_OKOLICE_LOKACJE + "jezioro_las9.c","ne",0,LAS_FATIGUE,0);
    add_exit(LAS_ZAKON_OKOLICE_LOKACJE + "jezioro_las8.c","nw",0,LAS_FATIGUE,0);
    add_npc(LAS_ZAKON_OKOLICE_LIVINGI + "lis");
	
    add_prop(ROOM_I_INSIDE,0);
}

public string
exits_description()
{
    return "Id�c w kierunku wschodnim, po^ludniowo-wschodnim, p^o^lnocno-wschodnim"+
    "i p^o^lnocno-zachodnim zag^l^ebisz si^e w las, natomiast pod^a^zajac "+
    "na poludnie dotrzesz do le^snego traktu.\n";
}

string
dlugi_opis()
{
    string str = "";

    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="Wsz^edzie w ok^o^l rosn^a pot^e^zne, wiekowe drzewa o grubej"+
             " pooranej bruzdami korze. Te wspania^le okazy o roz^lozyst"+
             "ych koronach przes^laniaj^a ca^le niebo pokrywaj^ac poszyci"+
             "e wiecznym mrokiem. Mieszany las, kt^orego wysokie drzewa o"+
             "taczaj^a mrokiem cale poszycie, zaslaniaj^a niewysokim ro^"+
             "slinom jakiekolwiek ^swiat^lo. Nie przeszkodzi^lo to jedna"+
             "k rozrosn^a^c si^e co bardziej odpornym gatunkom krzew^ow t"+
             "worz^acych trudn^a do przebycia g^estwine. Ro^slinno^s^c po"+
             "kryta jest szronem, a ziemi mo^zna zauwa^zy^c ^slady odbite "+
             "na ^sniegu.";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str+="Wsz^edzie w ok^o^l rosn^a pot^e^zne, wiekowe drzewa o grubej"+
             " pooranej bruzdami korze. Te wspania^le okazy o roz^lozyst"+
             "ych koronach przes^laniaj^a ca^le niebo pokrywaj^ac poszyci"+
             "e wiecznym mrokiem. Mieszany las, kt^orego wysokie drzewa o"+
             "taczaj^a mrokiem cale poszycie, zaslaniaj^a niewysokim ro^"+
             "slinom jakiekolwiek ^swiat^lo. Nie przeszkodzi^lo to jedna"+
             "k rozrosn^a^c si^e co bardziej odpornym gatunkom krzew^ow t"+
             "worz^acych trudn^a do przebycia g^estwine.";
    }
    else if(pora_roku() == MT_LATO)
    {
        str+="Wsz^edzie w ok^o^l rosn^a pot^e^zne, wiekowe drzewa o grubej"+
             " pooranej bruzdami korze. Te wspania^le okazy o roz^lozyst"+
             "ych koronach przes^laniaj^a ca^le niebo pokrywaj^ac poszyci"+
             "e wiecznym mrokiem. Mieszany las, kt^orego wysokie drzewa o"+
             "taczaj^a mrokiem cale poszycie, zaslaniaj^a niewysokim ro^"+
             "slinom jakiekolwiek ^swiat^lo. Nie przeszkodzi^lo to jedna"+
             "k rozrosn^a^c si^e co bardziej odpornym gatunkom krzew^ow t"+
             "worz^acych trudn^a do przebycia g^estwine.";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str+="Wsz^edzie w ok^o^l rosn^a pot^e^zne, wiekowe drzewa o grubej"+
             " pooranej bruzdami korze. Te wspania^le okazy o roz^lozyst"+
             "ych koronach przes^laniaj^a ca^le niebo pokrywaj^ac poszyci"+
             "e wiecznym mrokiem. Mieszany las, kt^orego wysokie drzewa o"+
             "taczaj^a mrokiem cale poszycie, zaslaniaj^a niewysokim ro^"+
             "slinom jakiekolwiek ^swiat^lo. Nie przeszkodzi^lo to jedna"+
             "k rozrosn^a^c si^e co bardziej odpornym gatunkom krzew^ow t"+
             "worz^acych trudn^a do przebycia g^estwine. Ro^slinno^s^c p"+
             "okryta jest ma^lymi kropelkami wody, a pomiedzy drzewami mo^"+
             "zna zauwa^zyc porozci^agane paj^eczyny.";
    }

    str+="\n";
    return str;
}
