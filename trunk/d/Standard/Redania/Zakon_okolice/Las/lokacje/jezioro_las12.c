/*
 * Opis i kod OHM
 * Data: Monday 27th of August 2007 01:29:01 PM
 * Ostatnio edytowane: Avard
 * Data: 29.06.2009
 */

#include <pogoda.h>
#include <stdproperties.h>
#include <mudtime.h>
#include "dir.h"

inherit LAS_ZAKON_OKOLICE_STD;

void
create_las()
{
    set_short("Pomi^edzy starymi drzewami.");
    set_long("@@dlugi_opis@@");
    add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "sj05","se",0,SCIEZKA_ZO_FATIG,0);
    add_exit(LAS_ZAKON_OKOLICE_LOKACJE+ "jezioro_las8.c","ne",0,LAS_FATIGUE,0);
    add_exit(LAS_ZAKON_OKOLICE_LOKACJE+"jezioro_las17.c","sw",0,LAS_FATIGUE,0);

    add_prop(ROOM_I_INSIDE,0);
}

public string
exits_description()
{
    return "Id�c w kierunku p^o^lnocno-wschodnim i po^ludniowo-zachodnim "+
    "zag^l^ebisz si^e w las, natomiast pod^a^zajac na po^ludniowy-wsch^od "+
    "dotrzesz do le^snego traktu.\n";
}

string
dlugi_opis()
{
    string str = "";

    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="Wysokie, stare i grube drzewa, tworz^ace dach nad ca^lym pod"+
             "^lo^zem, ukrywaj^a prawie wszystko w p^o^lmroku. Mieszany l"+
             "as, kt^orego wysokie drzewa otaczaj^a mrokiem cale poszyci"+
             "e, zaslaniaj^a niewysokim ro^slinom jakiekolwiek ^swiat^lo"+
             ". Nie przeszkodzi^lo to jednak rozrosn^a^c si^e co bardzie"+
             "j odpornym gatunkom krzew^ow tworz^acych trudn^a do przeby"+
             "cia g^estwine. Pod^lo^ze pokryte zesch^lymi li^s^cmi, twor"+
             "zy mi^ekki kobierzec szeleszcz^acy pod ka^zdym krokiem podr"+
             "^o^znego. Wszystko dooko^la pokryte jest, b^lyszcz^acym szr"+
             "onem i delikatnym bia^lym ^sniegiem. ";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str+="Wysokie, stare i grube drzewa, tworz^ace dach nad ca^lym pod"+
             "^lo^zem, ukrywaj^a prawie wszystko w p^o^lmroku. Mieszany l"+
             "as, kt^orego wysokie drzewa otaczaj^a mrokiem cale poszyci"+
             "e, zaslaniaj^a niewysokim ro^slinom jakiekolwiek ^swiat^lo"+
             ". Nie przeszkodzi^lo to jednak rozrosn^a^c si^e co bardzie"+
             "j odpornym gatunkom krzew^ow tworz^acych trudn^a do przeby"+
             "cia g^estwine. Pod^lo^ze pokryte zesch^lymi li^s^cmi, twor"+
             "zy mi^ekki kobierzec szeleszcz^acy pod ka^zdym krokiem podr"+
             "^o^znego.";
    }
    else if(pora_roku() == MT_LATO)
    {
        str+="Wysokie, stare i grube drzewa, tworz^ace dach nad ca^lym pod"+
             "^lo^zem, ukrywaj^a prawie wszystko w p^o^lmroku. Mieszany l"+
             "as, kt^orego wysokie drzewa otaczaj^a mrokiem cale poszyci"+
             "e, zaslaniaj^a niewysokim ro^slinom jakiekolwiek ^swiat^lo"+
             ". Nie przeszkodzi^lo to jednak rozrosn^a^c si^e co bardzie"+
             "j odpornym gatunkom krzew^ow tworz^acych trudn^a do przeby"+
             "cia g^estwine. Pod^lo^ze pokryte zesch^lymi li^s^cmi, twor"+
             "zy mi^ekki kobierzec szeleszcz^acy pod ka^zdym krokiem podr"+
             "^o^znego.";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str+="Wysokie, stare i grube drzewa, tworz^ace dach nad ca^lym pod"+
             "^lo^zem, ukrywaj^a prawie wszystko w p^o^lmroku. Mieszany l"+
             "as, kt^orego wysokie drzewa otaczaj^a mrokiem cale poszyci"+
             "e, zaslaniaj^a niewysokim ro^slinom jakiekolwiek ^swiat^lo"+
             ". Nie przeszkodzi^lo to jednak rozrosn^a^c si^e co bardzie"+
             "j odpornym gatunkom krzew^ow tworz^acych trudn^a do przeby"+
             "cia g^estwine. Pod^lo^ze pokryte zesch^lymi li^s^cmi, twor"+
             "zy mi^ekki kobierzec szeleszcz^acy pod ka^zdym krokiem podr"+
             "^o^znego. Z g^ory od czasu do czasu spada niewielka kropelk"+
             "a wody, rozbijaj^ac si^e na mi^ekkim runie.";
    }

    str+="\n";
    return str;
}
