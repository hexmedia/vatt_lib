/*
 * Opis i kod OHM
 * Data: Monday 27th of August 2007 01:29:01 PM
 * Ostatnio edytowane: Avard
 * Data: 29.06.2009
 */

#include <pogoda.h>
#include <stdproperties.h>
#include <mudtime.h>
#include "dir.h"

inherit LAS_ZAKON_OKOLICE_STD;

void
create_las()
{
    set_short("W g^l^ebi wiekowego lasu.");
    set_long("@@dlugi_opis@@");
    add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "t12","w",0,TRAKT_ZO_FATIG,0);
    add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "t10","se",0,TRAKT_ZO_FATIG,0);
    add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "t11","s",0,TRAKT_ZO_FATIG,0);

    add_prop(ROOM_I_INSIDE,0);
}

public string
exits_description()
{
    return "Id�c w kierunku po^ludniowo-zachodnim zag^l^ebisz si^e w las, "+
            "natomiast pod^a^zajac na zach^od, po^ludniowy-wschod oraz po^ludnie "+
            "dotrzesz do le^snego traktu.\n";

}
string
dlugi_opis()
{
    string str = "";

    if(CZY_JEST_SNIEG(this_object()))
    {
        str+=" Wysokie, stare i grube drzewa, tworz^ace dach nad ca^lym t"+
             "erenem, ukrywaj^a prawie wszystko w p^o^lmroku. Pod^lo^ze p"+
             "okryte zesch^lymi li^s^cmi, tworzy mi^ekki kobierzec szele"+
             "szcz^acy pod ka^zdym krokiem podr^o^znego, a lodowate powi"+
             "etrze stoi w miejscu, nieporuszane nawet najdelikatniejszy"+
             "m podmuchem wiatru. Na ziemii gdzieniegdzie wida^c bia^ly "+
             "^snieg, na kt^orym mozna spostrzec ^slady zwierz^at.";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str+="Wysokie, stare i grube drzewa, tworz^ace dach nad ca^lym ter"+
             "enem, ukrywaj^a prawie wszystko w p^o^lmroku. Pod^lo^ze pok"+
             "ryte zesch^lymi li^s^cmi, tworzy mi^ekki kobierzec szelesz"+
             "cz^acy pod ka^zdym krokiem podr^o^znego, a przesi^akni^ete"+
             " zapachem zbutwia^lego drewna i podgni^^lych li^sci gor^ac"+
             "e powietrze, stoi w miejscu, nieporuszane nawet najdelikat"+
             "niejszym podmuchem wiatru.";
    }
    else if(pora_roku() == MT_LATO)
    {
        str+="Wysokie, stare i grube drzewa, tworz^ace dach nad ca^lym ter"+
             "enem, ukrywaj^a prawie wszystko w p^o^lmroku. Pod^lo^ze pok"+
             "ryte zesch^lymi li^s^cmi, tworzy mi^ekki kobierzec szelesz"+
             "cz^acy pod ka^zdym krokiem podr^o^znego, a przesi^akni^ete"+
             " zapachem zbutwia^lego drewna i podgni^^lych li^sci gor^ac"+
             "e powietrze, stoi w miejscu, nieporuszane nawet najdelikat"+
             "niejszym podmuchem wiatru.";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str+="Wysokie, stare i grube drzewa, tworz^ace dach nad ca^lym ter"+
             "enem, ukrywaj^a prawie wszystko w p^o^lmroku. Pod^lo^ze pok"+
             "ryte zesch^lymi li^s^cmi, tworzy mi^ekki kobierzec szelesz"+
             "cz^acy pod ka^zdym krokiem podr^o^znego, a ch^lodne powiet"+
             "rze stoi w miejscu, nieporuszane nawet najdelikatniejszym "+
             "podmuchem wiatru. Od czasu do czasu z drzewa spada, wiruj^"+
             "ac w ok^o^l siebie r^o^znokolorowy li^s^c.";
    }

    str+="\n";
    return str;
}
