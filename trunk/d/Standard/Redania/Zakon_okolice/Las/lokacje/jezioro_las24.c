/*
 * Opis i kod OHM
 * Data: Monday 27th of August 2007 01:29:01 PM
 * Ostatnio edytowane: Avard
 * Data: 29.06.2009
 */

#include <pogoda.h>
#include <stdproperties.h>
#include <mudtime.h>
#include "dir.h"

inherit LAS_ZAKON_OKOLICE_STD;

void
create_las()
{
    set_short("W g^l^ebi wiekowego lasu.");
    set_long("@@dlugi_opis@@");
    add_exit(LAS_ZAKON_OKOLICE_LOKACJE + "jezioro_las25.c","e",0,LAS_FATIGUE,0);
    add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "sj04","ne",0,SCIEZKA_ZO_FATIG,0);
    add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "sj05","n",0,SCIEZKA_ZO_FATIG,0);
    add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "sj06","w",0,SCIEZKA_ZO_FATIG,0);

    add_prop(ROOM_I_INSIDE,0);
}

public string
exits_description()
{
    return "Id^ac w kierunku wschodnim zag^l^ebisz si^e w las, "+
    "natomiast pod^a^zajac na zach^od, p^o^lnoc oraz p^o^lnocny wsch^od "+
    "dotrzesz do le^snego traktu.\n";
}

string
dlugi_opis()
{
    string str = "";

    if(CZY_JEST_SNIEG(this_object()))
    {
        str+=" Wsz^edzie wok^o^l rosn^a pot^e^zne, wiekowe drzewa o grube"+
             "j pooranej bruzdami korze. Te wspania^le okazy o roz^lo^zys"+
             "tych koronach przes^laniaj^a ca^le niebo pokrywaj^ac poszyc"+
             "ie wiecznym mrokiem. Dooko^la, poza owymi olbrzymami, ros"+
             "n^a tak^ze g^este, suche krzewy, kt^orych ma^le ga^l^azki p"+
             "okryte s^a d^lugimi kolcami. Na ziemii gdzieniegdzie wida^c"+
             " bia^ly ^snieg, na kt^orym mozna spostrzec ^slady zwierz^a"+
             "t.";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str+="Wsz^edzie wok^o^l rosn^a pot^e^zne, wiekowe drzewa o grubej "+
             "pooranej bruzdami korze. Te wspania^le okazy o roz^lo^zysty"+
             "ch koronach przes^laniaj^a ca^le niebo pokrywaj^ac poszycie"+
             " wiecznym mrokiem. Dooko^la, poza owymi olbrzymami, rosn^"+
             "a tak^ze g^este, suche krzewy, kt^orych ma^le ga^l^azki pok"+
             "ryte s^a d^lugimi kolcami.";
    }
    else if(pora_roku() == MT_LATO)
    {
        str+="Wsz^edzie wok^o^l rosn^a pot^e^zne, wiekowe drzewa o grubej "+
             "pooranej bruzdami korze. Te wspania^le okazy o roz^lo^zysty"+
             "ch koronach przes^laniaj^a ca^le niebo pokrywaj^ac poszycie"+
             " wiecznym mrokiem. Dooko^la, poza owymi olbrzymami rosn^"+
             "a tak^ze g^este, suche krzewy, kt^orych ma^le ga^l^azki pok"+
             "ryte s^a d^lugimi kolcami.";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str+="Wsz^edzie wok^o^l rosn^a pot^e^zne, wiekowe drzewa o grubej "+
             "pooranej bruzdami korze. Te wspania^le okazy o roz^lo^zysty"+
             "ch koronach przes^laniaj^a ca^le niebo pokrywaj^ac poszycie"+
             " wiecznym mrokiem. Dooko^la, poza owymi olbrzymami rosn^"+
             "a tak^ze g^este, suche krzewy, kt^orych ma^le ga^l^azki pok"+
             "ryte s^a d^lugimi kolcami. Czasami z g^ory spada r^o^znokol"+
             "orowy li^s^c, krecacy sie w k^o^lko, nast^epnie spokojnie o"+
             "siadaj^acy na ziemii.";
    }

    str+="\n";
    return str;
}
