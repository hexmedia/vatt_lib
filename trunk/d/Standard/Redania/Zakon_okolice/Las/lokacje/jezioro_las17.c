/*
 * Opis i kod OHM
 * Data: Monday 27th of August 2007 01:29:01 PM
 * Ostatnio edytowane: Avard
 * Data: 29.06.2009
 */

#include <pogoda.h>
#include <stdproperties.h>
#include <mudtime.h>
#include "dir.h"

inherit LAS_ZAKON_OKOLICE_STD;

void
create_las()
{
    set_short("W g^l^ebi wiekowego lasu.");
    set_long("@@dlugi_opis@@");
    add_exit(LAS_ZAKON_OKOLICE_LOKACJE + "jezioro_las12.c","ne",0,LAS_FATIGUE,0);
    add_exit(LAS_ZAKON_OKOLICE_LOKACJE + "jezioro_las11.c","nw",0,LAS_FATIGUE,0);
    add_exit(LAS_ZAKON_OKOLICE_LOKACJE + "jezioro_las16.c","w",0,LAS_FATIGUE,0);
    add_exit(LAS_ZAKON_OKOLICE_LOKACJE + "jezioro_las23.c","s",0,LAS_FATIGUE,0);
    add_prop(ROOM_I_INSIDE,0);
}

public string
exits_description()
{
    return "G^esto rosn^ace krzaki i drzewa, pozwalaj^a ci kontynuowa^c "+
    "podr^o^z w kierunku p^o^lnocno wschodnim, p^o^lnocno zachodnim, "+
    "zachodnim oraz po^ludniowym. \n";
}

string
dlugi_opis()
{
    string str = "";

    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="Stare i omsza^le drzewa, najprawdopodobniej znaj^ace histori"+
             "e tego lasu wiele lat w stecz, pn^a si^e ku g^orze w kierun"+
             "ku nieba. Pousychane z braku s^lo^nca, dolne ga^l^ezie ^lam"+
             "i^a si^e pod mocniejszym dotykiem, wydaj^ac z siebie g^luc"+
             "hy trzask. Nagie pnie i grube pousychane konary, nadaj^a t"+
             "ej okolicy tajemniczy a zarazem straszny widok, kt^ory doda"+
             "tkowo pot^eguje niezm^acona niczym cisza. Chlodne zimowe p"+
             "owietrze, intensyfikuje zapach ^zywicy i iglak^ow rosn^acyc"+
             "h w tym miejscu.";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str+="Stare i omsza^le drzewa, najprawdopodobniej znaj^ace histori"+
             "e tego lasu wiele lat w stecz, pn^a si^e ku g^orze w kierun"+
             "ku nieba. Pousychane z braku s^lo^nca, dolne ga^l^ezie ^lam"+
             "i^a si^e pod mocniejszym dotykiem, wydaj^ac z siebie g^luc"+
             "hy trzask. Nagie pnie i grube pousychane konary, nadaj^a t"+
             "ej okolicy tajemniczy a zarazem straszny widok, kt^ory doda"+
             "tkowo pot^eguje niezm^acona niczym cisza. Ci^ezkie, duszne"+
             " i gor^ace powietrze stoi w miejscu wzmacniaj^ac zapach las"+
             "u.";
    }
    else if(pora_roku() == MT_LATO)
    {
        str+="Stare i omsza^le drzewa, najprawdopodobniej znaj^ace histori"+
             "e tego lasu wiele lat w stecz, pn^a si^e ku g^orze w kierun"+
             "ku nieba. Pousychane z braku s^lo^nca, dolne ga^l^ezie ^lam"+
             "i^a si^e pod mocniejszym dotykiem, wydaj^ac z siebie g^luc"+
             "hy trzask. Nagie pnie i grube pousychane konary, nadaj^a t"+
             "ej okolicy tajemniczy a zarazem straszny widok, kt^ory doda"+
             "tkowo pot^eguje niezm^acona niczym cisza. Ci^ezkie, duszne"+
             " i gor^ace powietrze stoi w miejscu wzmacniaj^ac zapach las"+
             "u.";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str+="Stare i omsza^le drzewa, najprawdopodobniej znaj^ace histori"+
             "e tego lasu wiele lat w stecz, pn^a si^e ku g^orze w kierun"+
             "ku nieba. Pousychane z braku s^lo^nca, dolne ga^l^ezie ^lam"+
             "i^a si^e pod mocniejszym dotykiem, wydaj^ac z siebie g^luc"+
             "hy trzask. Nagie pnie i grube pousychane konary, nadaj^a t"+
             "ej okolicy tajemniczy a zarazem straszny widok, kt^ory doda"+
             "tkowo pot^eguje niezm^acona niczym cisza. Ch^lodne powietr"+
             "ze osiada na paj^eczynach porozwieszanych pomiedzy pniami.";
    }

    str+="\n";
    return str;
}
