/*
 * Opis i kod OHM
 * Data: Monday 27th of August 2007 01:29:01 PM
 * Ostatnio edytowane: Avard
 * Data: 29.06.2009
 */

#include <pogoda.h>
#include <stdproperties.h>
#include <mudtime.h>
#include "dir.h"

inherit LAS_ZAKON_OKOLICE_STD;

void
create_las()
{
    set_short("Pomi^edzy starymi drzewami.");
    set_long("@@dlugi_opis@@");
    add_exit(LAS_ZAKON_OKOLICE_LOKACJE + "jezioro_las15.c","sw",0,LAS_FATIGUE,0);
    add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "t09","e",0,TRAKT_ZO_FATIG,0);
    add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "t08","se",0,TRAKT_ZO_FATIG,0);
    add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "t10","n",0,TRAKT_ZO_FATIG,0);
    add_prop(ROOM_I_INSIDE,0);
}

public string
exits_description()
{
    return "Id�c w kierunku po^ludniowo-zachodnim ag^l^ebisz "+
    "si^e w las, natomiast pod^a^zajac na wsch^od, po^ludniowy-wschod oraz "+
    "p^o^lnoc dotrzesz do le^snego traktu.\n";
}

string
dlugi_opis()
{
    string str = "";

    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="Stare i omsza^le drzewa, najprawdopodobniej znaj^ace histori"+
             "e tego lasu wiele lat w stecz, pn^a si^e ku g^orze w kierun"+
             "ku nieba. Przesi^akni^ete zapachem zbutwia^lego drewna i po"+
             "dgni^lych li^sci powietrze, stoi w miejscu nieporuszane na"+
             "wet najdelikatniejszym podmuchem wiatru. Gdzieniegdzie na l"+
             "e^snym runie, mo^zna zauwa^zy^c ma^le zasypane ^sniegiem k"+
             "^epki traw, reszta ro^slinnosci jest sucha, kar^lowata i o"+
             "szroniona, ze wzgl^edu na wiecznie panuj^acy w tym miejscu"+
             " p^o^lmrok.\n";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str+="Stare i omsza^le drzewa, najprawdopodobniej znaj^ace histori"+
             "e tego lasu wiele lat w stecz, pn^a si^e ku g^orze w kierun"+
             "ku nieba. Przesi^akni^ete zapachem zbutwia^lego drewna i po"+
             "dgni^^lych li^sci powietrze, stoi w miejscu nieporuszane na"+
             "wet najdelikatniejszym podmuchem wiatru. Gdzieniegdzie na l"+
             "e^snym runie, mo^zna zauwa^zy^c ma^le zielonkawe k^epki tr"+
             "aw, reszta ro^slinnosci jest sucha i kar^lowata, ze wzgl^e"+
             "du na wiecznie panuj^acy w tym miejscu p^o^lmrok.\n";
    }
    else if(pora_roku() == MT_LATO)
    {
        str+="Stare i omsza^le drzewa, najprawdopodobniej znaj^ace histori"+
             "e tego lasu wiele lat w stecz, pn^a si^e ku g^orze w kierun"+
             "ku nieba. Przesi^akni^ete zapachem zbutwia^lego drewna i po"+
             "dgni^^lych li^sci powietrze, stoi w miejscu nieporuszane na"+
             "wet najdelikatniejszym podmuchem wiatru. Gdzieniegdzie na l"+
             "e^snym runie, mo^zna zauwa^zy^c ma^le zielonkawe k^epki tr"+
             "aw, reszta ro^slinnosci jest sucha i kar^lowata, ze wzgl^e"+
             "du na wiecznie panuj^acy w tym miejscu p^o^lmrok.\n";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str+="Stare i omsza^le drzewa, najprawdopodobniej znaj^ace histori"+
             "e tego lasu wiele lat w stecz, pn^a si^e ku g^orze w kierun"+
             "ku nieba. Przesi^akni^ete zapachem zbutwia^lego drewna i po"+
             "dgni^^lych li^sci powietrze, stoi w miejscu nieporuszane na"+
             "wet najdelikatniejszym podmuchem wiatru. Gdzieniegdzie na l"+
             "e^snym runie, mo^zna zauwa^zy^c ma^le pousychane k^epki tr"+
             "aw, reszta ro^slinnosci jest kar^lowata, ze wzgl^edu na wi"+
             "ecznie panuj^acy w tym miejscu p^o^lmrok. Ch^lodne powietr"+
             "ze powoli osiada na porozwieszanych paj^eczynach, zostawiaj"+
             "^ac na nich kropelki wody.\n";
    }

    str+="\n";
    return str;
}
