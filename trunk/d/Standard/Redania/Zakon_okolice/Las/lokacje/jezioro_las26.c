/*
 * Opis i kod OHM
 * Data: Monday 27th of August 2007 01:29:01 PM
 * Ostatnio edytowane: Avard
 * Data: 29.06.2009
 */

#include <pogoda.h>
#include <stdproperties.h>
#include <mudtime.h>
#include "dir.h"

inherit LAS_ZAKON_OKOLICE_STD;

void
create_las()
{
    set_short("W g^l^ebi wiekowego lasu.");
    set_long("@@dlugi_opis@@");
    add_exit(LAS_ZAKON_OKOLICE_LOKACJE + "jezioro_las31.c","sw",0,LAS_FATIGUE,0);
    add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "sj01","ne",0,SCIEZKA_ZO_FATIG,0);
    add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "sj02","n",0,SCIEZKA_ZO_FATIG,0);
    add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "sj03","w",0,SCIEZKA_ZO_FATIG,0);

    add_prop(ROOM_I_INSIDE,0);
}

public string
exits_description()
{
    return "Id�c w kierunku po^ludniowo-zachodnim zag^l^ebisz si^e w las, natomiast pod^a^zajac "+
    "na p^o^lnocny-wsch^od, zach^od i p^olnoc dotrzesz do le^snego traktu.\n";
}

string
dlugi_opis()
{
    string str = "";

    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="Wysokie, stare i grube drzewa, tworz^ace dach nad ca^lym pod"+
             "^lo^zem, ukrywaj^a prawie wszystko w p^o^lmroku. Nagie pnie"+
             " i grube pousychane konary, nadaj^a tej okolicy tajemniczy"+
             " i straszny widok, kt^ory dodatkowo pot^eguje niezm^acona "+
             "niczym cisza. Pod^lo^ze jest wolne od zielonkawych ro^slin"+
             ", znajduje si^e tu tylko wysci^o^lka z opad^lych li^sci i "+
             "igie^l, pokryta w niekt^orych miejscach bia^lym ^sniegiem.";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str+="Wysokie, stare i grube drzewa, tworz^ace dach nad ca^lym pod"+
             "^lo^zem, ukrywaj^a prawie wszystko w p^o^lmroku. Nagie pnie"+
             " i grube pousychane konary, nadaj^a tej okolicy tajemniczy"+
             " i straszny widok, kt^ory dodatkowo pot^eguje niezm^acona "+
             "niczym cisza. Pod^lo^ze jest wolne od zielonkawych ro^slin"+
             ", znajduje si^e tu tylko wysci^o^lka z opad^lych li^sci i "+
             "igie^l, po kt^orej ^zwawo poruszaj^a si^e r^o^zne ma^le owad"+
             "y";
    }
    else if(pora_roku() == MT_LATO)
    {
        str+="Wysokie, stare i grube drzewa, tworz^ace dach nad ca^lym pod"+
             "^lo^zem, ukrywaj^a prawie wszystko w p^o^lmroku. Nagie pnie"+
             " i grube pousychane konary, nadaj^a tej okolicy tajemniczy"+
             " i straszny widok, kt^ory dodatkowo pot^eguje niezm^acona "+
             "niczym cisza. Pod^lo^ze jest wolne od zielonkawych ro^slin"+
             ", znajduje si^e tu tylko wysci^o^lka z opad^lych li^sci i "+
             "igie^l, po kt^orej ^zwawo poruszaj^a si^e r^o^zne ma^le owad"+
             "y";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str+="Wysokie, stare i grube drzewa, tworz^ace dach nad ca^lym pod"+
             "^lo^zem, ukrywaj^a prawie wszystko w p^o^lmroku. Nagie pnie"+
             " i grube pousychane konary, nadaj^a tej okolicy tajemniczy"+
             " i straszny widok, kt^ory dodatkowo pot^eguje niezm^acona "+
             "niczym cisza. Pod^lo^ze jest wolne od zielonkawych ro^slin"+
             ", znajduje si^e tu tylko wysci^o^lka z opad^lych li^sci i "+
             "igie^l, po kt^orej ^zwawo poruszaj^a si^e r^o^zne ma^le ow"+
             "ady. Czasami z g^ory spada r^o^znokolorowy li^s^c, krecacy"+
             " sie w k^o^lko spokojnie osiada na ziemii.";
    }

    str+="\n";
    return str;
}
