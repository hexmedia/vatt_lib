/*
 * Opis i kod OHM
 * Data: Monday 27th of August 2007 01:29:01 PM
 * Ostatnio edytowane: Avard
 * Data: 29.06.2009
 */

#include <pogoda.h>
#include <stdproperties.h>
#include <mudtime.h>
#include "dir.h"

inherit LAS_ZAKON_OKOLICE_STD;
void
create_las()
{
    set_short("W okolicy scie^zki.");
    set_long("@@dlugi_opis@@");

    add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "t15","n",0,TRAKT_ZO_FATIG,0);
    add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "t13","e",0,TRAKT_ZO_FATIG,0);
    add_exit(LAS_ZAKON_OKOLICE_LOKACJE + "jezioro_las2.c","se",0,LAS_FATIGUE,0);
    add_prop(ROOM_I_INSIDE,0);

}

public string
exits_description()
{
    return "Id�c w kierunku po^ludniowo-wschodnim zag^l^ebisz "+
    "si^e w las, natomiast pod^a^zajac na p^o^lnoc i wsch^od dotrzesz do "+
    "le^snego traktu.\n";
}

string
dlugi_opis()
{
    string str = "";

    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="Rzadkie drzewa i ma^la ilo^s^c krzew^ow, najprawdopodobniej "+
             "mog^a ^swiadczy^c, ^ze teren ten znajduje si^e gdzies "+
             "niedaleko ^scie^zki. Pomimo tego, ^ze drzewa w tym miejscu "+
             "rosn^a rzadziej ich roz^lo^zyste korony zas^laniaj^a prawi"+
             "e ca^le ^swiat^lo, i niewiele z niego dostaje si^e tutaj n"+
             "a d^o^l. Pod^lo^ze pokryte zesch^lymi li^s^cmi, tworzy mi^"+
             "ekki kobierzecszeleszcz^acy pod ka^zdym krokiem podr^o^zne"+
             "go. Wszystko dooko^la pokryte jest, b^lyszcz^acym szronem "+
             "i delikatnym bia^lym ^sniegiem. ";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str+="Rzadkie drzewa i ma^la ilo^s^c krzew^ow, najprawdopodobniej "+
             "mog^a ^swiadczy^c, ^ze teren ten znajduje si^e gdzies "+
             "niedaleko ^scie^zki. Pomimo tego, ^ze drzewa w tym miejscu "+
             "rosn^a rzadziej ich roz^lo^zyste korony zas^laniaj^a prawi"+
             "e ca^le ^swiat^lo, i niewiele z niego dostaje si^e tutaj n"+
             "a d^o^l. Pod^lo^ze pokryte zesch^lymi li^s^cmi, tworzy mi^"+
             "ekki kobierzec szeleszcz^acy pod ka^zdym krokiem podr^o^zn"+
             "ego.";
    }
    else if(pora_roku() == MT_LATO)
    {
        str+="Rzadkie drzewa i ma^la ilo^s^c krzew^ow, najprawdopodobniej "+
             "mog^a ^swiadczy^c, ^ze teren ten znajduje si^e gdzies "+
             "niedaleko ^scie^zki. Pomimo tego, ^ze drzewa w tym miejscu "+
             "rosn^a rzadziej ich roz^lo^zyste korony zas^laniaj^a prawi"+
             "e ca^le ^swiat^lo, i niewiele z niego dostaje si^e tutaj n"+
             "a d^o^l. Pod^lo^ze pokryte zesch^lymi li^s^cmi, tworzy mi^"+
             "ekki kobierzec szeleszcz^acy pod ka^zdym krokiem podr^o^zn"+
             "ego.";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str+="Rzadkie drzewa i ma^la ilo^s^c krzew^ow, najprawdopodobniej "+
             "mog^a ^swiadczy^c, ^ze teren ten znajduje si^e gdzies "+
             "niedaleko ^scie^zki. Pomimo tego, ^ze drzewa w tym miejscu "+
             "rosn^a rzadziej ich roz^lo^zyste korony zas^laniaj^a prawi"+
             "e ca^le ^swiat^lo, i niewiele z niego dostaje si^e tutaj n"+
             "a d^o^l. Pod^lo^ze pokryte zesch^lymi li^s^cmi, tworzy mi^"+
             "ekki kobierzec szeleszcz^acy pod ka^zdym krokiem podr^o^zn"+
             "ego. Z g^oryod czasu do czasu spada niewielka kropelka wod"+
             "y, rozbijaj^ac si^e na mi^ekkim runie.";
    }

    str+="\n";
    return str;
}

