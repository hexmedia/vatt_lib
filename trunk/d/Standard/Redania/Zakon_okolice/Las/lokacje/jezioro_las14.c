/*
 * Autor: Avard
 * Opis : Samaia
 * Data : 15.02.2009
 */

#include <pogoda.h>
#include <stdproperties.h>
#include <mudtime.h>
#include <macros.h>
#include "dir.h"

inherit LAS_ZAKON_OKOLICE_STD;

void
create_las()
{
    set_short("Pomi^edzy starymi drzewami.");
    set_long("@@dlugi_opis@@");
    add_exit(LAS_ZAKON_OKOLICE_LOKACJE + "jezioro_las18.c","s",0,LAS_FATIGUE,0);
    add_exit(LAS_ZAKON_OKOLICE_LOKACJE + "jezioro_las15.c","e",0,LAS_FATIGUE,0);
    add_exit(LAS_ZAKON_OKOLICE_LOKACJE + "jezioro_las13.c","w",0,LAS_FATIGUE,0);
    add_exit(LAS_ZAKON_OKOLICE_LOKACJE + "jezioro_las9.c","n",0,LAS_FATIGUE,0);
    add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "sj02.c","se",0,SCIEZKA_ZO_FATIG,0);
    add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "sj04","sw",0,SCIEZKA_ZO_FATIG,0);
    add_object(LAS_ZAKON_OKOLICE_OBIEKTY + "pien",1);
    dodaj_rzecz_niewyswietlana("pot^e^zny konar",1);
    
    add_prop(ROOM_I_INSIDE,0);

    LOAD_ERR(LAS_ZAKON_OKOLICE_LOKACJE + "w_konarze.c");
}

public string
exits_description()
{
    return "Id�c w kierunku po^ludniowym, wschodnim, zachodnim, p^o^lnocnym oraz "+
    "po^ludniowo-wschodnim zag^l^ebisz si^e w las, za^s pod^a^zajac "+
    "na po^ludniowy-zach^od dotrzesz do le^snego traktu.\n";
}

string
dlugi_opis()
{
    string str = "";

    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="W tym fragmencie lasu, a raczej na polanie w otoczeniu "+
            "pot^e^znych drzew, w oczy rzuca si^e ogromny zwalony pie^n. "+
            "Gdyby nie zimowa pora, kt^ora ogo^loci^la wszelkie zaro^sla z "+
            "listowia, ci^e^zko by^loby go zauwa^zy^c, mimo tak poka^xnych "+
            "rozmiar^ow. ^Sci^o^lka, pokryta puszystym ^sniegiem, pe^lna "+
            "jest ^slad^ow podr^o^znik^ow, ale i tak^ze zwierz^at, gdy^z "+
            "niekt^ore z odcisk^ow bez w^atpienia nale^z^a do sarny, "+
            "borsuka a nawet lisa. Mimo tych niezaprzeczalnych oznak "+
            "^zycia, ca^la okolica zdaje si^e by^c zupe^lnie u^spiona i "+
            "senna, cho^c to zapewne za spraw^a monotonni kolor^ow - "+
            "wok^o^l bowiem panuje tylko biel ^sniegu i czer^n konar^ow "+
            "drzew. ";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str+="W tym fragmencie lasu, a raczej na polanie w otoczeniu "+
            "pote^znych drzew, nic nie rzuca si^e w oczy, mo^ze poza "+
            "zwalonym pniem, ukrytym w g^estych zaro^slach. Ca^la natura "+
            "wok^o^l zdaje si^e raczej odci^aga^c potencjalny ciekawski "+
            "wzrok od tego konaru. Ma za^s do dyspozycji ca^le pi^ekno "+
            "wiosny i rozwijaj^acej si^e przyrody. W mi^ekkiej, "+
            "ods^loni^etej gdzieniegdzie ziemi, dostrzec mo^zna odciski "+
            "st^op podr^o^znik^ow, ale i tak^ze zwierz^at, gdy^z niekt^ore "+
            "ze ^slad^ow bez wapienia nale^z^a do sarny, borsuka, a nawet "+
            "i lisa. ";
    }
    else if(pora_roku() == MT_LATO)
    {
        str+="W tym fragmencie lasu, a raczej na polanie w otoczeniu "+
            "pote^znych drzew, nic nie rzuca si^e w oczy, mo^ze poza "+
            "zwalonym pniem, ukrytym w g^estych zaro^slach. Ca^la natura "+
            "wok^o^l zdaje si^e raczej odci^aga^c potencjalny ciekawski "+
            "wzrok od tego konaru. Ma za^s do dyspozycji ca^le pi^ekno tego "+
            "lasu w pe^lni lata. Przyt^laczaj^aca wr^ecz ziele^n i "+
            "intensywne zapachy lasu niemal odurzaj^a. W mi^ekkiej, "+
            "ods^loni^etej gdzieniegdzie ziemi, dostrzec mo^zna odciski "+
            "st^op podr^o^znik^ow, ale i tak^ze zwierz^at, gdy^z niekt^ore "+
            "ze ^slad^ow bez wapienia nale^z^a do sarny, borsuka, a nawet i "+
            "lisa. ";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str+="W tym fragmencie lasu, a raczej na polanie w otoczeniu "+
            "pot^e^znych drzew, nic nie rzuca si^e oczy, mo^ze poza "+
            "zwalonym pniem, ukrytym w g^estych zaro^slach. Ca^la natura "+
            "wok^o^l zdaje si^e raczej odci^aga^c potencjalny ciekawski "+
            "wzrok od tego konaru. Ma za^s do dyspozycji niesamowite "+
            "barwy, od soczystej zieleni, poprzez ^z^o^l^c i z^loto, po "+
            "gor^acy pomara^ncz. W mi^ekkiej, ods^loni^etej gdzieniegdzie "+
            "ziemi, dostrzec mo^zna odciski st^op podr^o^znik^ow, ale i "+
            "tak^ze zwierz^at, gdy^z niekt^ore ze ^slad^ow bez wapienia "+
            "nale^z^a do sarny, borsuka, a nawet i lisa. ";
    }

    str+="\n";
    return str;
}