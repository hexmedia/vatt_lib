/*
 * Opis i kod OHM
 * Data: Monday 27th of August 2007 01:29:01 PM
 * Ostatnio edytowane: Avard
 * Data: 29.06.2009
 */

#include <pogoda.h>
#include <stdproperties.h>
#include <mudtime.h>
#include "dir.h"

inherit LAS_ZAKON_OKOLICE_STD;

void
create_las()
{
    set_short("Pomi^edzy starymi drzewami.");
    set_long("@@dlugi_opis@@");
    add_exit(LAS_ZAKON_OKOLICE_LOKACJE + "jezioro_las14.c","n",0,LAS_FATIGUE,0);
    add_exit(LAS_ZAKON_OKOLICE_LOKACJE + "jezioro_las15.c","ne",0,LAS_FATIGUE,0);
    add_exit(LAS_ZAKON_OKOLICE_LOKACJE + "jezioro_las13.c","nw",0,LAS_FATIGUE,0);
    add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "sj02","e",0,SCIEZKA_ZO_FATIG,0);
    add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "sj03","s",0,SCIEZKA_ZO_FATIG,0);
    add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "sj04","w",0,SCIEZKA_ZO_FATIG,0);
    
    add_prop(ROOM_I_INSIDE,0);
    add_npc(LAS_ZAKON_OKOLICE_LIVINGI + "dzik", 1);
}

public string
exits_description()
{
    return "Id�c w kierunku p^o^lnocnym, p^o^lnocno wschodnim oraz "+
    "p^o^lnocno zachodnim zag^l^ebisz si^e w las, natomiast pod^a^zajac "+
    "na wsch^od i zach^od dotrzesz do le^snego traktu.\n";
}

string
dlugi_opis()
{
    string str = "";

    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="Pot^e^zne i wysokie drzewa, pn^a si^e do g^ory zas^laniaj^ac"+
             " ca^lkowicie niebo, powoduj^ac jednocze^snie, ^ze wszystko "+
             " tonie w lekkim mroku. Pousychane z braku s^lo^nca, dolne "+
             "ga^l^ezie ^lami^a si^e pod mocniejszym dotykiem, wydaj^ac "+
             "z siebie g^luchy trzask. G^esty, sk^apany w mroku, wiekowy"+
             " las, wygl^ada nader tajemniczo, a odczucie to pot^eguj^a "+
             "ro^sliny, pokryte szronem i delikatnychm puchem.";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str+="Pot^e^zne i wysokie drzewa, pn^a si^e do g^ory zas^laniaj^ac"+
             " ca^lkowicie niebo, powoduj^ac jednocze^snie, ^ze wszystko "+
             " tonie w lekkim mroku. Pousychane z braku s^lo^nca, dolne "+
             "ga^l^ezie ^lami^a si^e pod mocniejszym dotykiem, wydaj^ac "+
             "z siebie g^luchy trzask. G^esty, sk^apany w mroku, wiekowy"+
             " las, wygl^ada nader tajemniczo, a odczucie to pot^eguje z"+
             "aduch i zapach le^snego runa.";
    }
    else if(pora_roku() == MT_LATO)
    {
        str+="Pot^e^zne i wysokie drzewa, pn^a si^e do g^ory zas^laniaj^ac"+
             " ca^lkowicie niebo, powoduj^ac jednocze^snie, ^ze wszystko "+
             " tonie w lekkim mroku. Pousychane z braku s^lo^nca, dolne "+
             "ga^l^ezie ^lami^a si^e pod mocniejszym dotykiem, wydaj^ac "+
             "z siebie g^luchy trzask. G^esty, sk^apany w mroku, wiekowy"+
             " las, wygl^ada nader tajemniczo, a odczucie to pot^eguje z"+
             "aduch i zapach le^snego runa.";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str+="Pot^e^zne i wysokie drzewa, pn^a si^e do g^ory zas^laniaj^ac"+
             " ca^lkowicie niebo, powoduj^ac jednocze^snie, ^ze wszystko "+
             " tonie w lekkim mroku. Pousychane z braku s^lo^nca, dolne "+
             "ga^l^ezie ^lami^a si^e pod mocniejszym dotykiem, wydaj^ac "+
             "z siebie g^luchy trzask. G^esty, sk^apany w mroku, wiekowy"+
             " las, wygl^ada nader tajemniczo, a odczucie to pot^eguj^a "+
             "spadaj^ace li^scie i liczne paj^eczny porozwieszane prawie "+
             "w ka^zdym miejscu. ";
    }

    str+="\n";
    return str;
}
