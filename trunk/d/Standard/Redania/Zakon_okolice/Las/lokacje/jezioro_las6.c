/*
 * Opis i kod OHM
 * Data: Monday 27th of August 2007 01:29:01 PM
 * Ostatnio edytowane: Avard
 * Data: 29.06.2009
 */

#include <pogoda.h>
#include <stdproperties.h>
#include <mudtime.h>
#include "dir.h"

inherit LAS_ZAKON_OKOLICE_STD;

void
create_las()
{
    set_short("Ciemny stary las.");
    set_long("@@dlugi_opis@@");
    add_exit(LAS_ZAKON_OKOLICE_LOKACJE + "jezioro_las2.c","nw",0,LAS_FATIGUE,0);
    add_exit(LAS_ZAKON_OKOLICE_LOKACJE + "jezioro_las9.c","s",0,LAS_FATIGUE,0);
    add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "t11","e",0,TRAKT_ZO_FATIG,0);
    add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "t12","n",0,TRAKT_ZO_FATIG,0);
    add_prop(ROOM_I_INSIDE,0);
}

public string
exits_description()
{
    return "Id�c w kierunku p^o^lnocno-zachodnim i p^o^lnocno-wschodnim zag^l^ebisz "+
            "si^e w las, natomiast pod^a^zajac na wsch^od i p^o^lnoc dotrzesz do "+
            "le^snego traktu.\n";
}

string
dlugi_opis()
{
    string str = "";

    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="Rosn^ace blisko siebie grube drzewa, pokryte mchem i naro^sl"+
             "ami, utrudniaj^a podr^oz przez ten wiekowy las rozci^agaj^a"+
             "cy sie w ka^zdym kierunku. Gdzieniegdzie na le^snym runie,"+
             " mo^zna zauwa^zy^c ma^le zielonkawe kepki traw, reszta ro^"+
             "slinnosci jest sucha i karlowata, ze wzgl^edu na wiecznie "+
             "panuj^acy w tym lesie p^o^lmrok. Pod^lo^ze pokryte zesch^l"+
             "ymi li^s^cmi, tworzy mi^ekki kobierzec szeleszcz^acy pod k"+
             "a^zdym krokiem podr^o^znego. Wszystko dooko^la pokryte jes"+
             "t, b^lyszcz^acym szronem i delikatnym bia^lym ^sniegiem .";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str+="Rosn^ace blisko siebie grube drzewa, pokryte mchem i naro^sl"+
             "ami, utrudniaj^a podr^oz przez ten wiekowy las rozci^agaj^a"+
             "cy sie w ka^zdym kierunku. Gdzieniegdzie na le^snym runie,"+
             " mo^zna zauwa^zy^c ma^le zielonkawe kepki traw, reszta ro^"+
             "slinnosci jest sucha i karlowata, ze wzgl^edu na wiecznie "+
             "panuj^acy w tym lesie p^o^lmrok. Pod^lo^ze pokryte zesch^l"+
             "ymi li^s^cmi, tworzy mi^ekki kobierzec szeleszcz^acy pod k"+
             "a^zdym krokiem podr^o^znego.";
    }
    else if(pora_roku() == MT_LATO)
    {
        str+="Rosn^ace blisko siebie grube drzewa, pokryte mchem i naro^sl"+
             "ami, utrudniaj^a podr^oz przez ten wiekowy las rozci^agaj^a"+
             "cy sie w ka^zdym kierunku. Gdzieniegdzie na le^snym runie,"+
             " mo^zna zauwa^zy^c ma^le zielonkawe kepki traw, reszta ro^"+
             "slinnosci jest sucha i karlowata, ze wzgl^edu na wiecznie "+
             "panuj^acy w tym lesie p^o^lmrok. Pod^lo^ze pokryte zesch^l"+
             "ymi li^s^cmi, tworzy mi^ekki kobierzec szeleszcz^acy pod k"+
             "a^zdym krokiem podr^o^znego.";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str+="Rosn^ace blisko siebie grube drzewa, pokryte mchem i naro^sl"+
             "ami, utrudniaj^a podr^oz przez ten wiekowy las rozci^agaj^a"+
             "cy sie w ka^zdym kierunku. Gdzieniegdzie na le^snym runie,"+
             " mo^zna zauwa^zy^c ma^le zielonkawe kepki traw, reszta ro^"+
             "slinnosci jest sucha i karlowata, ze wzgl^edu na wiecznie "+
             "panuj^acy w tym lesie p^o^lmrok. Pod^lo^ze pokryte zesch^l"+
             "ymi li^s^cmi, tworzy mi^ekki kobierzec szeleszcz^acy pod k"+
             "a^zdym krokiem podr^o^znego. Z g^ory od czasu do czasu spa"+
             "da niewielka kropelka wody, rozbijaj^ac si^e na mi^ekkim r"+
             "unie.";
    }

    str+="\n";
    return str;
}
