/*
 * Opis i kod OHM
 * Data: Monday 27th of August 2007 01:29:01 PM
 * Ostatnio edytowane: Avard
 * Data: 29.06.2009
 */

#include <pogoda.h>
#include <stdproperties.h>
#include <mudtime.h>
#include "dir.h"

inherit LAS_ZAKON_OKOLICE_STD;

void
create_las()
{
    set_short("Ciemny stary las.");
    set_long("@@dlugi_opis@@");
    add_exit(LAS_ZAKON_OKOLICE_LOKACJE + "jezioro_las29.c","w",0,LAS_FATIGUE,0);
    add_exit(LAS_ZAKON_OKOLICE_LOKACJE + "jezioro_las22.c","nw",0,LAS_FATIGUE,0);
    add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "sj07","e",0,SCIEZKA_ZO_FATIG,0);
    add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "sj08","s",0,SCIEZKA_ZO_FATIG,0);

    add_prop(ROOM_I_INSIDE,0);
    add_npc(LAS_ZAKON_OKOLICE_LIVINGI + "dzik");
}

public string
exits_description()
{
    return "Id�c w kierunku zachodnim i p^o^lnocno-zachodnim zag^l^ebisz "+
	   "si^e w las, natomiast pod^a^zajac na po^ludniowy-zachod i po^ludnie "+
	   "dotrzesz do le^snego traktu.\n";
}

string
dlugi_opis()
{
    string str = "";

    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="Pote�ne i wysokie drzewa, pn� sie do g�ry zas�aniaj�c "+
        "ca�kowicie niebo, powoduj�c jednocze�nie, �e wszystko tonie w "+
        "lekkim mroku.  W tej cz�ci zdaj� si� przewa�a� buki i d�by, mo�na "+
        "jednak dostrzec poza nimi wiele innych gatunk�w drzew od brzozy po "+
        "leszczyn�. Ciche pohukiwanie za�, wydawane przez jakiego� ptaka "+
        "ukrytego w koronach drzew, pot�guje nastr�j wiekowego, ponurego "+
        "lasu. Na bia�ym puchu dostrzec mo�na kilka �lad�w st�p, jak i "+
        "�lad�w pozostawionych przez zwierzyn�.. ";
    }
    else //opis uniwersalny, pasuje do kazdej pory roku jak nie ma sniegu
    {
        str+="Pote�ne i wysokie drzewa, pn� sie do g�ry zas�aniaj�c "+
        "ca�kowicie niebo, powoduj�c jednocze�nie, �e wszystko tonie w "+
        "lekkim mroku.  W tej cz�ci zdaj� si� przewa�a� buki i d�by, "+
        "mo�na jednak dostrzec poza nimi wiele innych gatunk�w drzew od "+
        "brzozy po leszczyn�. Ciche pohukiwanie za�, wydawane przez "+
        "jakiego� ptaka ukrytego w koronach drzew, pot�guje nastr�j "+
        "wiekowego, ponurego lasu. Mi�kka, czarna ziemia, widoczna "+
        "spod gdzieniegdzie odgarni�tej �ci�ki, nosi �lady kilku "+
        "odcisk�w st�p. ";
    }

    str+="\n";
    return str;
}
