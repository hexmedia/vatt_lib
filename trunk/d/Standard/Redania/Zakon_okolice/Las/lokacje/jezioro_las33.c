/*
 * Opis i kod OHM
 * Data: Monday 27th of August 2007 01:29:01 PM
 * Ostatnio edytowane: Avard
 * Data: 29.06.2009
 */

#include <pogoda.h>
#include <stdproperties.h>
#include <mudtime.h>
#include "dir.h"

inherit LAS_ZAKON_OKOLICE_STD;

void
create_las()
{
    set_short("Ciemny stary las.");
    set_long("@@dlugi_opis@@");
    add_exit(LAS_ZAKON_OKOLICE_LOKACJE + "jezioro_las34.c","e",0,LAS_FATIGUE,0);
    add_exit(LAS_ZAKON_OKOLICE_LOKACJE + "jezioro_las36.c","sw",0,LAS_FATIGUE,0);
    add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "sj12.c","w",0,SCIEZKA_ZO_FATIG,0);
    add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "sj11.c","n",0,SCIEZKA_ZO_FATIG,0);
    add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "sj10.c","ne",0,SCIEZKA_ZO_FATIG,0);

    add_prop(ROOM_I_INSIDE,0);    
}

public string
exits_description()
{
    return "Id�c w kierunku wschodnim oraz po^ludniowo-zachodnim zag^l^ebisz si^e "+
            "w las, natomiast pod^a^zajac na zach^od, p^o^lnoc i p^o^lnocny-wsch^od "+
            "dotrzesz do le^snego traktu.\n";
}

string
dlugi_opis()
{
    string str = "";

    str += "Rosn�ce tu drzewa, pn�ce si� w odwiecznej walce ku g�rze, "+
    "wygl�daj� na wyj�tkowo wiekowe, o czym �wiadczy� mo�e obecno�� "+
    "kilku zupe�nie pustych w �rodku - stanowi�cych teraz schronienie "+
    "insekt�w, czy nietoperzy. Ich ga��zie, d���ce do uzyskania cho�by "+
    "najmniejszej szczypty s�onecznego �wiat�a, uwi�y w tej walce "+
    "roz�o�ysty baldachim. Pod�o�e pokryte zesch�ymi li��mi, tworzy "+
    "mi�kki kobierzec szeleszcz�cy pod ka�dym krokiem podr�nego.";

    str+="\n";
    return str;
}
