/*
 * Opis i kod OHM
 * Data: Monday 27th of August 2007 01:29:01 PM
 * Ostatnio edytowane: Avard
 * Data: 29.06.2009
 */

#include <pogoda.h>
#include <stdproperties.h>
#include <mudtime.h>
#include "dir.h"

inherit LAS_ZAKON_OKOLICE_STD;

void
create_las()
{
    set_short("W g^l^ebi wiekowego lasu.");
    set_long("@@dlugi_opis@@");
    add_exit(LAS_ZAKON_OKOLICE_LOKACJE + "jezioro_las15.c","se",0,LAS_FATIGUE,0);
    add_exit(LAS_ZAKON_OKOLICE_LOKACJE + "jezioro_las14.c","s",0,LAS_FATIGUE,0);
    add_exit(LAS_ZAKON_OKOLICE_LOKACJE + "jezioro_las13.c","sw",0,LAS_FATIGUE,0);
    add_exit(LAS_ZAKON_OKOLICE_LOKACJE + "jezioro_las6.c","n",0,LAS_FATIGUE,0);
    add_prop(ROOM_I_INSIDE,0);
    add_npc(LAS_ZAKON_OKOLICE_LIVINGI + "los");
}

public string
exits_description()
{
     return "G^esto rosn^ace krzaki i drzewa, pozwalaj^a Ci kontynuowa^c "+
	    "podr^o^z w kierunku po^ludniowo-wschodnim, po^ludniowym oraz "+
	    "po^ludniowo-zachodnim.\n";
}

string
dlugi_opis()
{
    string str = "";

    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="Blisko rosn^ace drzewa i krzewy, tworza w tym lesie istny la"+
            "birynt, a dodatkowy p^o^lmrok okrywaj^acy okolice, powoduje"+
            " ^ze znalezienie ^latwego przej^scia mo�e by� dla "+
            "pocz�tkuj�cego podr�nika dosy� trudne. Gd"+ 
            "zieniegdzie na le^snym runie, mo^zna zauwa^zy^c ma^le zielo"+
            "nkawe kepki traw, reszta ro^slinnosci jest sucha i karlowa"+
            "ta, ze wzgl^edu na wiecznie panuj^acy w tym lesie p^o^lmro"+
            "k. Pod^lo^ze pokryte zesch^lymi li^s^cmi, tworzy mi^ekki k"+
            "obierzec szeleszcz^acy pod ka^zdym krokiem podr^o^znego. W"+
            "szystko dooko^la pokryte jest, b^lyszcz^acym szronem i del"+
            "ikatnym bia^lym ^sniegiem.";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str+="Blisko rosn^ace drzewa i krzewy, tworza w tym lesie istny la"+
            "birynt, a dodatkowy p^o^lmrok okrywaj^acy okolice, powoduje"+
            " ^ze znalezienie ^latwego przej^scia mo�e by� dla "+
            "pocz�tkuj�cego podr�nika dosy� trudne. Gd"+
            "zieniegdzie na le^snym runie, mo^zna zauwa^zy^c ma^le ziel"+
            "onkawe kepki traw, reszta ro^slinnosci jest sucha i karlow"+
            "ata, ze wzgl^edu na wiecznie panuj^acy w tym lesie p^o^lmr"+
            "ok. Pod^lo^ze pokryte zesch^lymi li^s^cmi, tworzy mi^ekki "+
            "kobierzec szeleszcz^acy pod ka^zdym krokiem podr^o^znego.";
    }
    else if(pora_roku() == MT_LATO)
    {
        str+="Blisko rosn^ace drzewa i krzewy, tworza w tym lesie istny la"+
            "birynt, a dodatkowy p^o^lmrok okrywaj^acy okolice, powoduje"+
            " ^ze znalezienie ^latwego przej^scia mo�e by� dla "+
            "pocz�tkuj�cego podr�nika dosy� trudne. Gd"+
            "zieniegdzie na le^snym runie, mo^zna zauwa^zy^c ma^le ziel"+
            "onkawe kepki traw, reszta ro^slinnosci jest sucha i karlow"+
            "ata, ze wzgl^edu na wiecznie panuj^acy w tym lesie p^o^lmr"+
            "ok. Pod^lo^ze pokryte zesch^lymi li^s^cmi, tworzy mi^ekki "+
            "kobierzec szeleszcz^acy pod ka^zdym krokiem podr^o^znego.";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str+="Blisko rosn^ace drzewa i krzewy, tworza w tym lesie istny la"+
            "birynt, a dodatkowy p^o^lmrok okrywaj^acy okolice, powoduje"+
            " ^ze znalezienie ^latwego przej^scia mo�e by� dla "+
            "pocz�tkuj�cego podr�nika dosy� trudne. Gd"+
            "zieniegdzie na le^snym runie, mo^zna zauwa^zy^c ma^le ziel"+
            "onkawe kepki traw, reszta ro^slinnosci jest sucha i karlow"+
            "ata, ze wzgl^edu na wiecznie panuj^acy w tym lesie p^o^lmr"+
            "ok. Pod^lo^ze pokryte zesch^lymi li^s^cmi, tworzy mi^ekki "+
            "kobierzec szeleszcz^acy pod ka^zdym krokiem podr^o^znego. "+
            "Z g^ory od czasu do czasu spada niewielka kropelka wody, r"+
            "ozbijaj^ac si^e na mi^ekkim runie.";
    }

    str+="\n";
    return str;
}
