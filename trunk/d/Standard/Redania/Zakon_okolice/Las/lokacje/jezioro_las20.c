/*
 * Opis i kod OHM
 * Data: Monday 27th of August 2007 01:29:01 PM
 * Ostatnio edytowane: Avard
 * Data: 29.06.2009
 */

#include <pogoda.h>
#include <stdproperties.h>
#include <mudtime.h>
#include "dir.h"

inherit LAS_ZAKON_OKOLICE_STD;

void
create_las()
{
    set_short("Ciemny stary las.");
    set_long("@@dlugi_opis@@");
    add_exit(LAS_ZAKON_OKOLICE_LOKACJE + "jezioro_las28.c","se",0,LAS_FATIGUE,0);
    add_exit(LAS_ZAKON_OKOLICE_LOKACJE + "jezioro_las27.c","sw",0,LAS_FATIGUE,0);
    add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "sj16","w",0,SCIEZKA_ZO_FATIG,0);
    add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "sj15","s",0,SCIEZKA_ZO_FATIG,0);

    add_prop(ROOM_I_INSIDE,0);
}

public string
exits_description()
{
    return "Id�c w kierunku po^ludniowo-wschodnim oraz "+
    "po^ludniowo-zachodnim zag^l^ebisz si^e w las, natomiast pod^a^zajac "+
    "na zach^od i poludnie dotrzesz do le^snego traktu.\n";
}

string
dlugi_opis()
{
    string str = "";

    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="Blisko rosn^ace drzewa i krzewy, tworza w tym lesie istny la"+
             "birynt, a dodatkowy p^o^lmrok okrywaj^acy okolice, powoduje"+
             " ^ze znalezienie ^latwego przej^scia jest bardzo trudne. Po"+
             "d^lo^ze pokryte zesch^lymi li^s^cmi, tworzy mi^ekki kobierz"+
             "ec szeleszcz^acy pod ka^zdym krokiem podr^o^znego, a przesi"+
             "^akni^ete zapachem zbutwia^lego drewna i podgni^lych li^s"+
             "ci powietrze, stoi w miejscu nieporuszane nawet najdelikat"+
             "niejszym podmuchem wiatru. Na ziemii gdzieniegdzie wida^c "+
             "bia^ly ^snieg, na kt^orym mozna spostrzec ^slady zwierz^at.";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str+="Blisko rosn^ace drzewa i krzewy, tworza w tym lesie istny la"+
             "birynt, a dodatkowy p^o^lmrok okrywaj^acy okolice, powoduje"+
             " ^ze znalezienie ^latwego przej^scia jest bardzo trudne. Po"+
             "d^lo^ze pokryte zesch^lymi li^s^cmi, tworzy mi^ekki kobierz"+
             "ec szeleszcz^acy pod ka^zdym krokiem podr^o^znego, a przesi"+
             "^akni^ete zapachem zbutwia^lego drewna i podgni^lych li^s"+
             "ci powietrze, stoi w miejscu nieporuszane nawet najdelikatn"+
             "iejszym podmuchem wiatru.";
    }
    else if(pora_roku() == MT_LATO)
    {
        str+="Blisko rosn^ace drzewa i krzewy, tworza w tym lesie istny la"+
             "birynt, a dodatkowy p^o^lmrok okrywaj^acy okolice, powoduje"+
             " ^ze znalezienie ^latwego przej^scia jest bardzo trudne. Po"+
             "d^lo^ze pokryte zesch^lymi li^s^cmi, tworzy mi^ekki kobierz"+
             "ec szeleszcz^acy pod ka^zdym krokiem podr^o^znego, a przesi"+
             "^akni^ete zapachem zbutwia^lego drewna i podgni^lych li^s"+
             "ci powietrze, stoi w miejscu nieporuszane nawet najdelikatn"+
             "iejszym podmuchem wiatru.";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str+="Blisko rosn^ace drzewa i krzewy, tworza w tym lesie istny la"+
             "birynt, a dodatkowy p^o^lmrok okrywaj^acy okolice, powoduje"+
             " ^ze znalezienie ^latwego przej^scia jest bardzo trudne. Po"+
             "d^lo^ze pokryte zesch^lymi li^s^cmi, tworzy mi^ekki kobierz"+
             "ec szeleszcz^acy pod ka^zdym krokiem podr^o^znego, a przesi"+
             "^akni^ete zapachem zbutwia^lego drewna powwietrze nieporus"+
             "zane jest nawet najdelikatniejszym podmuchem wiatru. Od cz"+
             "asu do czasu z drzewa spada, wiruj^ac w ok^o^l siebie r^o^"+
             "znokolorowy li^s^c.";
    }

    str+="\n";
    return str;
}
