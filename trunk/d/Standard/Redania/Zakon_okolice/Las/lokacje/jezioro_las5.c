/*
 * Opis i kod OHM
 * Data: Monday 27th of August 2007 01:29:01 PM
 * Ostatnio edytowane: Avard
 * Data: 29.06.2009
 */

#include <pogoda.h>
#include <stdproperties.h>
#include <mudtime.h>
#include "dir.h"

inherit LAS_ZAKON_OKOLICE_STD;

void
create_las()
{
    set_short("W g^l^ebi wiekowego lasu.");
    set_long("@@dlugi_opis@@");
    add_exit(LAS_ZAKON_OKOLICE_LOKACJE + "jezioro_las2.c","ne",0,LAS_FATIGUE,0);
    add_exit(LAS_ZAKON_OKOLICE_LOKACJE + "jezioro_las8.c","s",0,LAS_FATIGUE,0);
    add_prop(ROOM_I_INSIDE,0);
    add_npc(LAS_ZAKON_OKOLICE_LIVINGI + "lis");
}

public string
exits_description()
{
         return "G^esto rosn^ace krzaki i drzewa, pozwalaj^a ci kontynuowa^c "+
	 "podr^o^z w kierunku p^o^lnocno-wschodnim i po^ludniowym.\n";
}

string
dlugi_opis()
{
    string str = "";

    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="Mieszany las, kt^orego wysokie drzewa otaczaj^a mrokiem cale"+
             " poszycie, zaslaniaj^a niewysokim ro^slinom jakiekolwiek ^"+
             "swiat^lo. Nie przeszkodzi^lo to jednak rozrosn^a^c si^e co"+
             " bardziej odpornym gatunkom krzew^ow tworz^acych trudn^a do"+
             " przebycia g^estwin^e. Dooko^la poza pot^e^znymi drzewami "+
             "rosn^a tak^ze g^este, suche krzewy, kt^orych ma^le ga^l^az"+
             "ki pokryte s^a d^lugimi kolcami. Na ziemii gdzieniegdzie w"+
             "ida^c bia^ly ^snieg, na kt^orym mozna spostrzec ^slady zwi"+
             "erz^at.";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str+="Mieszany las, kt^orego wysokie drzewa otaczaj^a mrokiem cale"+
             " poszycie, zaslaniaj^a niewysokim ro^slinom jakiekolwiek ^"+
             "swiat^lo. Nie przeszkodzi^lo to jednak rozrosn^a^c si^e co"+
             " bardziej odpornym gatunkom krzew^ow tworz^acych trudn^a do"+
             " przebycia g^estwin^e. Dooko^la poza pot^e^znymi drzewami "+
             "rosn^a tak^ze g^este, suche krzewy, kt^orych ma^le ga^l^azk"+
             "i pokryte s^a d^lugimi kolcami.";
    }
    else if(pora_roku() == MT_LATO)
    {
        str+="Mieszany las, kt^orego wysokie drzewa otaczaj^a mrokiem cale"+
             " poszycie, zaslaniaj^a niewysokim ro^slinom jakiekolwiek ^"+
             "swiat^lo. Nie przeszkodzi^lo to jednak rozrosn^a^c si^e co"+
             " bardziej odpornym gatunkom krzew^ow tworz^acych trudn^a do"+
             " przebycia g^estwin^e. Dooko^la poza pot^e^znymi drzewami "+
             "rosn^a tak^ze g^este, suche krzewy, kt^orych ma^le ga^l^azk"+
             "i pokryte s^a d^lugimi kolcami.";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str+="Mieszany las, kt^orego wysokie drzewa otaczaj^a mrokiem cale"+
             " poszycie, zaslaniaj^a niewysokim ro^slinom jakiekolwiek ^"+
             "swiat^lo. Nie przeszkodzi^lo to jednak rozrosn^a^c si^e co"+
             " bardziej odpornym gatunkom krzew^ow tworz^acych trudn^a do"+
             " przebycia g^estwin^e. Dooko^la poza pot^e^znymi drzewami "+
             "rosn^a tak^ze g^este, suche krzewy, kt^orych ma^le ga^l^azk"+
             "i pokryte s^a d^lugimi kolcami. Czasami z g^ory spada r^o^z"+
             "nokolorowy li^s^c, krecacy sie w k^o^lko, nast^epnie spokoj"+
             "nie osiadaj^acy na ziemii.";
    }

    str+="\n";
    return str;
}

