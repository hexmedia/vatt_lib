/*
 * Opis i kod OHM
 * Data: Monday 27th of August 2007 01:29:01 PM
 * Ostatnio edytowane: Avard
 * Data: 29.06.2009
 */

#include <pogoda.h>
#include <stdproperties.h>
#include <mudtime.h>
#include "dir.h"

inherit LAS_ZAKON_OKOLICE_STD;

void
create_las()
{
    set_short("W g^l^ebi wiekowego lasu.");
    set_long("@@dlugi_opis@@");
    add_exit(LAS_ZAKON_OKOLICE_LOKACJE + "jezioro_las27.c","se",0,LAS_FATIGUE,0);
    add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "sj17","n",0,SCIEZKA_ZO_FATIG,0);
    add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "sj16","e",0,SCIEZKA_ZO_FATIG,0);
    
    add_prop(ROOM_I_INSIDE,0);
}

public string
exits_description()
{
    return "Id�c w kierunku po^ludniowo-wschodnim zag^l^ebisz "+
    "si^e w las, natomiast pod^a^zajac na p^o^lnoc i wsch^od "+
    "dotrzesz do le^snego traktu.\n";
}

string
dlugi_opis()
{
    string str = "";

    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="Rosn^ace blisko siebie grube drzewa, pokryte mchem i naro^sl"+
             "ami, poprzedzielane s^a krzakami o grubych ciemnobr^azowych"+
             " li^sciach, i ma^lymi kar^lowatymi krzakami je^zyn, utrudni"+
             "aj^acymi podr^oz przez ten wiekowy las, rozci^agaj^acy sie"+
             " w ka^zdym kierunku. Pod^lo^ze jest wolne od zielonkawych"+
             " ro^slin, znajduje si^e tu tylko wysci^o^lka z opad^lych l"+
             "i^sci i igie^l, pokryta w niekt^orych miejscach bia^lym ^sn"+
             "iegiem.";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str+="Rosn^ace blisko siebie grube drzewa, pokryte mchem i naro^sl"+
             "ami, poprzedzielane s^a krzakami o grubych ciemnobr^azowych"+
             " li^sciach, i ma^lymi kar^lowatymi krzakami je^zyn, utrudni"+
             "aj^acymi podr^oz przez ten wiekowy las, rozci^agaj^acy sie"+
             " w ka^zdym kierunku. Pod^lo^ze jest wolne od zielonkawych"+
             " ro^slin, znajduje si^e tu tylko wysci^o^lka z opad^lych l"+
             "i^sci i igie^l, po kt^orej ^zwawo poruszaj^a si^e r^o^zne "+
             "ma^le owady.";
    }
    else if(pora_roku() == MT_LATO)
    {
        str+="Rosn^ace blisko siebie grube drzewa, pokryte mchem i naro^sl"+
             "ami, poprzedzielane s^a krzakami o grubych ciemnobr^azowych"+
             " li^sciach, i ma^lymi kar^lowatymi krzakami je^zyn, utrudni"+
             "aj^acymi podr^oz przez ten wiekowy las, rozci^agaj^acy sie"+
             " w ka^zdym kierunku. Pod^lo^ze jest wolne od zielonkawych"+
             " ro^slin, znajduje si^e tu tylko wysci^o^lka z opad^lych l"+
             "i^sci i igie^l, po kt^orej ^zwawo poruszaj^a si^e r^o^zne "+
             "ma^le owady.";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str+="Rosn^ace blisko siebie grube drzewa, pokryte mchem i naro^sl"+
             "ami, poprzedzielane s^a krzakami o grubych ciemnobr^azowych"+
             " li^sciach, i ma^lymi kar^lowatymi krzakami je^zyn, utrudni"+
             "aj^acymi podr^oz przez ten wiekowy las, rozci^agaj^acy sie"+
             " w ka^zdym kierunku. Pod^lo^ze jest wolne od zielonkawych"+
             " ro^slin, znajduje si^e tu tylko wysci^o^lka z opad^lych l"+
             "i^sci i igie^l, po kt^orej ^zwawo poruszaj^a si^e r^o^zne "+
             "ma^le owady. Czasami z g^ory spada r^o^znokolorowy li^s^c, "+
             "krecacy sie w k^o^lko spokojnie osiada na ziemii.";
    }

    str+="\n";
    return str;
}
