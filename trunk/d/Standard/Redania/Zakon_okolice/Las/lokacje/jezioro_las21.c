/*
 * Opis i kod OHM
 * Data: Monday 27th of August 2007 01:29:01 PM
 * Ostatnio edytowane: Avard
 * Data: 29.06.2009
 */

#include <pogoda.h>
#include <stdproperties.h>
#include <mudtime.h>
#include "dir.h"

inherit LAS_ZAKON_OKOLICE_STD;

void
create_las()
{
    set_short("W g^l^ebi wiekowego lasu.");
    set_long("@@dlugi_opis@@");
    add_exit(LAS_ZAKON_OKOLICE_LOKACJE + "jezioro_las22.c","e",0,LAS_FATIGUE,0);
    add_exit(LAS_ZAKON_OKOLICE_LOKACJE + "jezioro_las29.c","se",0,LAS_FATIGUE,0);
    add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "sj11","sw",0,SCIEZKA_ZO_FATIG,0);
    add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "sj10","s",0,SCIEZKA_ZO_FATIG,0);

    add_prop(ROOM_I_INSIDE,0);
    add_npc(LAS_ZAKON_OKOLICE_LIVINGI + "wilk");
}

public string
exits_description()
{
    return "Id�c w kierunku wschodnim oraz poludniowo zachodnim "+
    "zag^l^ebisz si^e w las, natomiast pod^a^zajac na po^ludnie i "+
    "po^ludniowy wsch^od dotrzesz do lesnego traktu.\n";
}

string
dlugi_opis()
{
    string str = "";

    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="Pot^e^zne i wysokie drzewa, pn^a si^e do g^ory zas^laniaj^ac"+
             " ca^lkowicie niebo, powoduj^ac jednocze^snie, ^ze wszystko "+
             " tonie w lekkim mroku. G^esto usytuowane pokryte mchem pnie,"+
             " poprzedzielane s^a krzakami o grubych ciemnobr^azowych li"+
             "^sciach, i ma^lymi kar^lowatymi krzakami je^zyn. Przesi^ak"+
             "ni^ete zapachem zbutwia^lego drewna i podgni^^lych li^sci "+
             "lodowate powietrze, stoi w miejscu, nieporuszane nawet n"+
             "ajdelikatniejszym podmuchem wiatru. Na ziemii gdzieniegdzie "+
             " wida^c bia^ly ^snieg, na kt^orym mozna spostrzec ^slady zwi"+
             "erz^at.";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str+=" Pot^e^zne i wysokie drzewa, pn^a si^e do g^ory zas^laniaj^"+
             "ac ca^lkowicie niebo, powoduj^ac jednocze^snie, ^ze wszystk"+
             "o tonie w lekkim mroku. G^esto usytuowane pokryte mchem pni"+
             "e, poprzedzielane s^a krzakami o grubych ciemnobr^azowych "+
             "li^sciach, i ma^lymi kar^lowatymi krzakami je^zyn. Przesi^"+
             "akni^ete zapachem zbutwia^lego drewna i podgni^^lych li^sc"+
             "i gor^ace powietrze, stoi w miejscu, nieporuszane nawet "+
             "najdelikatniejszym podmuchem wiatru.";
    }
    else if(pora_roku() == MT_LATO)
    {
        str+=" Pot^e^zne i wysokie drzewa, pn^a si^e do g^ory zas^laniaj^"+
             "ac ca^lkowicie niebo, powoduj^ac jednocze^snie, ^ze wszystk"+
             "o tonie w lekkim mroku. G^esto usytuowane pokryte mchem pni"+
             "e, poprzedzielane s^a krzakami o grubych ciemnobr^azowych "+
             "li^sciach, i ma^lymi kar^lowatymi krzakami je^zyn. Przesi^"+
             "akni^ete zapachem zbutwia^lego drewna i podgni^^lych li^sc"+
             "i gor^ace powietrze, stoi w miejscu, nieporuszane nawet "+
             "najdelikatniejszym podmuchem wiatru.";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str+="Pot^e^zne i wysokie drzewa, pn^a si^e do g^ory zas^laniaj^ac"+
             " ca^lkowicie niebo, powoduj^ac jednocze^snie, ^ze wszystko "+
             " tonie w lekkim mroku. G^esto usytuowane pokryte mchem pnie,"+
             " poprzedzielane s^a krzakami o grubych ciemnobr^azowych li"+
             "^sciach, i ma^lymi kar^lowatymi krzakami je^zyn. Przesi^ak"+
             "ni^ete zapachem zbutwia^lego drewna i podgni^^lych li^sci "+
             "ch^lodne powietrze, stoi w miejscu, nieporuszane nawet naj"+
             "delikatniejszym podmuchem wiatru. Od czasu do czasu z drze"+
             "wa spada, wiruj^ac w ok^o^l siebie r^o^znokolorowy li^s^c.";
    }

    str+="\n";
    return str;
}
