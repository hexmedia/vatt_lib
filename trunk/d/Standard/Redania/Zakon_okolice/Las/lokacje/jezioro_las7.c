/*
 * Opis i kod OHM
 * Data: Monday 27th of August 2007 01:29:01 PM
 * Ostatnio edytowane: Avard
 * Data: 29.06.2009
 */

#include <pogoda.h>
#include <stdproperties.h>
#include <mudtime.h>
#include "dir.h"

inherit LAS_ZAKON_OKOLICE_STD;

void
create_las()
{
    set_short("W g^l^ebi wiekowego lasu.");
    set_long("@@dlugi_opis@@");
    add_exit(LAS_ZAKON_OKOLICE_LOKACJE + "jezioro_las4.c","nw",0,LAS_FATIGUE,0);
    add_exit(LAS_ZAKON_OKOLICE_LOKACJE + "jezioro_las11.c","se",0,LAS_FATIGUE,0);
    add_prop(ROOM_I_INSIDE,0);
}

public string
exits_description()
{
     return "G^esto rosn^ace krzaki i drzewa, pozwalaj^a Ci kontynuowa^c "+
	    "podr^o^z w kierunku p^o^lnocno-zachodnim i po^ludniowo-wschodnim.\n";
}

string
dlugi_opis()
{
    string str = "";

    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="Rosn^ace blisko siebie grube drzewa, pokryte mchem i naro^sl"+
             "ami, utrudniaj^a podr^oz przez ten wiekowy las rozci^agaj^a"+
             "cy sie w ka^zdym kierunku. Gdzieniegdzie na le^snym runie,"+
             " mo^zna zauwa^zy^c ma^le zielonkawe kepki traw, reszta ro^"+
             "slinnosci jest sucha i karlowata, ze wzgl^edu na wiecznie "+
             "panuj^acy w tym lesie p^o^lmrok. Pod^lo^ze pokryte zesch^l"+
             "ymi li^s^cmi, tworzy mi^ekki kobierzec szeleszcz^acy pod k"+
             "a^zdym krokiem podr^o^znego. Wszystko dooko^la pokryte jes"+
             "t, b^lyszcz^acym szronem i delikatnym bia^lym ^sniegiem .";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str+="Rosn^ace blisko siebie grube drzewa, pokryte mchem i naro^slami "+
	     "utrudniaj^a podr^oz przez ten wiekowy las rozci^agaj^acy sie "+
	     "w ka^zdym kierunku. Pousychane z braku s^lo^nca, dolne ga^l^ezie "+
	     "^lami^a si^e pod mocniejszym dotykiem, wydaj�c z siebie "+
	     "g^luchy trzask. Nagie pnie i grube pousychane konary, nadaj^a "+
             "tej okolicy tajemniczy i straszny widok, a odczucie to "+
	     "pot^eguje zaduch i niezm^acona niczym cisza. ";
    }
    else if(pora_roku() == MT_LATO)
    {
        str+="Rosn^ace blisko siebie grube drzewa, pokryte mchem i naro^slami "+
	     "utrudniaj^a podr^oz przez ten wiekowy las rozci^agaj^acy sie "+
	     "w ka^zdym kierunku. Pousychane z braku s^lo^nca, dolne ga^l^ezie "+
	     "^lami^a si^e pod mocniejszym dotykiem, wydaj�c z siebie "+
	     "g^luchy trzask. Nagie pnie i grube pousychane konary, nadaj^a "+
             "tej okolicy tajemniczy i straszny widok, a odczucie to "+
	     "pot^eguje zaduch i niezm^acona niczym cisza. ";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str+="Rosn^ace blisko siebie grube drzewa, pokryte mchem i naro^slami "+
	     "utrudniaj^a podr^oz przez ten wiekowy las rozci^agaj^acy sie "+
	     "w ka^zdym kierunku. Pousychane z braku s^lo^nca, dolne ga^l^ezie "+
	     "^lami^a si^e pod mocniejszym dotykiem, wydaj�c z siebie "+
	     "g^luchy trzask. Nagie pnie i grube pousychane konary, nadaj^a "+
	     "tej okolicy tajemniczy i straszny widok, a odczucie to "+
	     "pot^eguj^a spadaj^ace li^scie i liczne paj�czny"+
	     "porozwieszane prawie w ka^zdym miejscu. ";
    }

    str+="\n";
    return str;
}
