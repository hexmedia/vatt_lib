/*
 * Opis i kod OHM
 * Data: Monday 27th of August 2007 01:29:01 PM
 * Ostatnio edytowane: Avard
 * Data: 29.06.2009
 */

#include <pogoda.h>
#include <stdproperties.h>
#include <mudtime.h>
#include "dir.h"

inherit LAS_ZAKON_OKOLICE_STD;

void
create_las()
{
    set_short("W g^l^ebi wiekowego lasu.");
    set_long("@@dlugi_opis@@");
    add_exit(LAS_ZAKON_OKOLICE_LOKACJE + "jezioro_las14.c","w",0,LAS_FATIGUE,0);
    add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "sj02","s",0,SCIEZKA_ZO_FATIG,0);
    add_exit(LAS_ZAKON_OKOLICE_LOKACJE + "jezioro_las10.c","ne",0,LAS_FATIGUE,0);
    add_exit(LAS_ZAKON_OKOLICE_LOKACJE + "jezioro_las9.c","nw",0,LAS_FATIGUE,0);
    add_exit(LAS_ZAKON_OKOLICE_LOKACJE + "jezioro_las18.c","sw",0,LAS_FATIGUE,0);
    add_prop(ROOM_I_INSIDE,0);
}

public string
exits_description()
{
    return "Id�c w kierunku zachodnim. p^o^lnocno-wschodnim, p^o^lnocno "+
    "zachodnim oraz po^ludniowo-zachodnim zag^l^ebisz si^e w las, natomiast "+
    "pod^a^zajac na po^ludnie i po^ludniowy-wsch^od dotrzesz do "+
    "le^snego traktu.\n";
}

string
dlugi_opis()
{
    string str = "";

    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="Stare i omsza^le drzewa, najprawdopodobniej znaj^ace histori"+
             "e tego lasu wiele lat w stecz, pn^a si^e ku g^orze w kierun"+
             "ku nieba. Gdzieniegdzie na le^snym runie, mo^zna zauwa^zy^c"+
             " ma^le zielonkawe k^epki traw, reszta ro^slinnosci jest such"+
             "a i kar^lowata, ze wzgl^edu na wiecznie panuj^acy p^o^lmrok"+
             ", w tym zawieszonym w czasie miejscu. Pod^lo^ze pokryte ze"+
             "sch^lymi li^s^cmi, tworzy mi^ekki kobierzec szeleszcz^acy p"+
             "od ka^zdym krokiem podr^o^znego. Wszystko dooko^la pokryte "+
             "jest, b^lyszcz^acym szronem i delikatnym bia^lym ^sniegiem."+
             " ";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str+="Stare i omsza^le drzewa, najprawdopodobniej znaj^ace histori"+
             "e tego lasu wiele lat w stecz, pn^a si^e ku g^orze w kierun"+
             "ku nieba. Gdzieniegdzie na le^snym runie, mo^zna zauwa^zy^c"+
             " ma^le zielonkawe k^epki traw, reszta ro^slinnosci jest such"+
             "a i kar^lowata, ze wzgl^edu na wiecznie panuj^acy w tym mie"+
             "jscu p^o^lmrok. Pod^lo^ze pokryte zesch^lymi li^s^cmi, two"+
             "rzy mi^ekki kobierzec szeleszcz^acy pod ka^zdym krokiem po"+
             "dr^o^znego. ";
    }
    else if(pora_roku() == MT_LATO)
    {
        str+="Stare i omsza^le drzewa, najprawdopodobniej znaj^ace histori"+
             "e tego lasu wiele lat w stecz, pn^a si^e ku g^orze w kierun"+
             "ku nieba. Gdzieniegdzie na le^snym runie, mo^zna zauwa^zy^c"+
             " ma^le zielonkawe k^epki traw, reszta ro^slinnosci jest such"+
             "a i kar^lowata, ze wzgl^edu na wiecznie panuj^acy w tym mie"+
             "jscu p^o^lmrok. Pod^lo^ze pokryte zesch^lymi li^s^cmi, two"+
             "rzy mi^ekki kobierzec szeleszcz^acy pod ka^zdym krokiem po"+
             "dr^o^znego.";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str+="Stare i omsza^le drzewa, najprawdopodobniej znaj^ace histori"+
             "e tego lasu wiele lat w stecz, pn^a si^e ku g^orze w kierun"+
             "ku nieba. Gdzieniegdzie na le^snym runie, mo^zna zauwa^zy^c"+
             " ma^le zielonkawe k^epki traw, reszta ro^slinnosci jest such"+
             "a i kar^lowata, ze wzgl^edu na wiecznie panuj^acy w tym mie"+
             "jscu p^o^lmrok. Pod^lo^ze pokryte zesch^lymi li^s^cmi, two"+
             "rzy mi^ekki kobierzec szeleszcz^acy pod ka^zdym krokiem po"+
             "dr^o^znego. Z g^ory od czasu do czasu spada niewielka krop"+
             "elka wody, rozbijaj^ac si^e na mi^ekkim runie.";
    }

    str+="\n";
    return str;
}
