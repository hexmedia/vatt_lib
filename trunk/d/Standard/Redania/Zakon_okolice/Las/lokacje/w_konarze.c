/*
 * Autor: Avard
 * Opis : Samaia
 * Data : 14.02.2009
 */

#include <pogoda.h>
#include <stdproperties.h>
#include <mudtime.h>
#include "dir.h"

inherit "/std/room.c";

void
create_room()
{
    set_short("W konarze");
    set_long("@@dlugi_opis@@");
    add_exit(LOKACJA_Z_KONAREM,({"wyj�cie","na zewn�trz","wychodz�c z konaru"}),0,1,0);
    
    add_prop(ROOM_I_INSIDE,0);
}

public string
exits_description()
{
    return "Wyj^scie prowadzi na zewn^atrz.\n";
}

string
dlugi_opis()
{
    string str;
    str = "St^ech^ly od^or, wilgotne powietrze, ale tak^ze i odg^losy "+
        "daj^a do zrozumienia, ^ze jest to miejsce jak najbardziej "+
        "zamieszka^le. Prawie ca^la powierzchnia spr�chnia^lego pnia "+
        "pokryta jest r^o^znego rodzaju insektami, od ston^og po paj^aki";
    if(pora_roku() == MT_ZIMA)
    {
        str += ", niekt^ore, zwa^zywszy na zimow^a por^e roku, skry^ly "+
            "si^e w kokonach, przypominaj^acych ma^le kolonie skryte w "+
            "zag^l^ebieniach nier^ownej powierzchni pnia";
    }
    str+=".\n";
    return str;
}
