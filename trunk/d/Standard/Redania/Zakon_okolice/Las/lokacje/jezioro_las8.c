/*
 * Opis i kod OHM
 * Data: Monday 27th of August 2007 01:29:01 PM
 * Ostatnio edytowane: Avard
 * Data: 29.06.2009
 */

#include <pogoda.h>
#include <stdproperties.h>
#include <mudtime.h>
#include "dir.h"

inherit LAS_ZAKON_OKOLICE_STD;

void
create_las()
{
    set_short("Ciemny stary las.");
    set_long("@@dlugi_opis@@");
    add_exit(LAS_ZAKON_OKOLICE_LOKACJE + "jezioro_las5.c","n",0,LAS_FATIGUE,0);
    add_exit(LAS_ZAKON_OKOLICE_LOKACJE + "jezioro_las12.c","sw",0,LAS_FATIGUE,0);
    add_exit(LAS_ZAKON_OKOLICE_LOKACJE + "jezioro_las13.c","se",0,LAS_FATIGUE,0);
    add_prop(ROOM_I_INSIDE,0);
}

public string
exits_description()
{
     return "G^esto rosn^ace krzaki i drzewa, pozwalaj^a ci kontynuowa^c "+
	    "podr^o^z w kierunku p^o^lnocnym, po^ludnowo-zachodnim oraz po^ludniowo-wschodnim.\n";
}

string
dlugi_opis()
{
    string str = "";

    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="Stare i omsza^le drzewa, najprawdopodobniej znaj^ace histori"+
             "e tego lasu wiele lat w stecz, pn^a si^e ku g^orze w kierun"+
             "ku nieba. G^este krzaki, wyrastaj^ace gdzieniegdzie mi^edzy"+
             " pniami, o suchych i ma^lych ga^l^eziach, pokrytych ko^lcam"+
             "i, zdecydowanie utrudniaj^a chodzenie po tym terenie. Ciemn"+
             "y i g^esty, zastyg^ly jakby w czasie las, stoi w bezruchu "+
             "i ciszy. Nie s^lycha^c tu nawet pobrz^ekiwania owad^ow,ani"+
             " szumu li^sci. Ch^lodne zimowe powietrze, intensyfikuje za"+
             "pach ^zywicy i iglak^ow rosn^acych w tym miejscu.";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str+="Stare i omsza^le drzewa, najprawdopodobniej znaj^ace histori"+
             "e tego lasu wiele lat w stecz, pn^a si^e ku g^orze w kierun"+
             "ku nieba. G^este krzaki, wyrastaj^ace gdzieniegdzie mi^edzy"+
             " pniami, o suchych i ma^lych ga^l^eziach, pokrytych ko^lcam"+
             "i, zdecydowanie utrudniaj^a chodzenie po tym terenie. Ciemn"+
             "y i g^esty, zastyg^ly jakby w czasie las, stoi w bezruchu "+
             "i ciszy. Nie s^lycha^c tu nawet pobrz^ekiwania owad^ow,ani"+
             " szumu li^sci.";
    }
    else if(pora_roku() == MT_LATO)
    {
        str+="Stare i omsza^le drzewa, najprawdopodobniej znaj^ace histori"+
             "e tego lasu wiele lat w stecz, pn^a si^e ku g^orze w kierun"+
             "ku nieba. G^este krzaki, wyrastaj^ace gdzieniegdzie mi^edzy"+
             " pniami, o suchych i ma^lych ga^l^eziach, pokrytych ko^lcam"+
             "i, zdecydowanie utrudniaj^a chodzenie po tym terenie. Ciemn"+
             "y i g^esty, zastyg^ly jakby w czasie las, stoi w bezruchu "+
             "i ciszy. Nie s^lycha^c tu nawet pobrz^ekiwania owad^ow,ani"+
             " szumu li^sci.";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str+="Stare i omsza^le drzewa, najprawdopodobniej znaj^ace histori"+
             "e tego lasu wiele lat w stecz, pn^a si^e ku g^orze w kierun"+
             "ku nieba. G^este krzaki, wyrastaj^ace gdzieniegdzie mi^edzy"+
             " pniami, o suchych i ma^lych ga^l^eziach, pokrytych ko^lcam"+
             "i, zdecydowanie utrudniaj^a chodzenie po tym terenie. Ciemn"+
             "y i g^esty, zastyg^ly jakby w czasie las, stoi w bezruchu "+
             "i ciszy. Nie s^lycha^c tu nawet pobrz^ekiwania owad^ow,ani"+
             " szumu li^sci, tylko odczasu do czasu zagubiony r^o^znokolo"+
             "rowy li^sc, spada zg^ory wiruj^ac w k^o^lko.";
    }

    str+="\n";
    return str;
}

