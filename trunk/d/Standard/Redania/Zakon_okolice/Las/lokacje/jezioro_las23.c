/*
 * Opis i kod OHM
 * Data: Monday 27th of August 2007 01:29:01 PM
 * Ostatnio edytowane: Avard
 * Data: 29.06.2009
 */

#include <pogoda.h>
#include <stdproperties.h>
#include <mudtime.h>
#include "dir.h"

inherit LAS_ZAKON_OKOLICE_STD;

void
create_las()
{
    set_short("Ciemny stary las.");
    set_long("@@dlugi_opis@@");
    add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "sj06","e",0,SCIEZKA_ZO_FATIG,0);
    add_exit(LAS_ZAKON_OKOLICE_LOKACJE + "jezioro_las17.c","n",0,LAS_FATIGUE,0);
    add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "sj07","s",0,SCIEZKA_ZO_FATIG,0);
    add_prop(ROOM_I_INSIDE,0);
}

public string
exits_description()
{
    return "Id�c w kierunku p^o^lnocnym zag^l^ebisz si^e w las, "+
    "natomiast pod^a^zajac na wsch^od i po^ludnie dotrzesz do le^snego "+
    "traktu.\n";
}

string
dlugi_opis()
{
    string str = "";

    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="Pot^e^zne i wysokie drzewa, pn^a si^e do g^ory zas^laniaj^ac"+
             " ca^lkowicie niebo, powoduj^ac jednocze^snie, ^ze wszystko "+
             " tonie w lekkim mroku. Przesi^akni^ete zapachem zbutwia^lego"+
             " drewna i podgni^lych li^sci zimne powietrze, stoi w miej"+
             "scu nieporuszane nawet najdelikatniejszym podmuchem wiatru"+
             ". Pod^lo^ze pokryte zesch^lymi li^s^cmi, tworzy mi^ekki kob"+
             "ierzec szeleszcz^acy pod ka^zdym krokiem podr^o^znego. Wszy"+
             "stko dooko^la pokryte jest, b^lyszcz^acym szronem i delikat"+
             "nym bia^lym ^sniegiem. ";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str+="Pot^e^zne i wysokie drzewa, pn^a si^e do g^ory zas^laniaj^ac"+
             " ca^lkowicie niebo, powoduj^ac jednocze^snie, ^ze wszystko "+
             " tonie w lekkim mroku. Przesi^akni^ete zapachem zbutwia^lego"+
             " drewna i podgni^lych li^sci gor^ace powietrze, stoi w mi"+
             "ejscu nieporuszane nawet najdelikatniejszym podmuchem wiat"+
             "ru. Pod^lo^ze pokryte zesch^lymi li^s^cmi, tworzy mi^ekki k"+
             "obierzec szeleszcz^acy pod ka^zdym krokiem podr^o^znego.";
    }
    else if(pora_roku() == MT_LATO)
    {
        str+="Pot^e^zne i wysokie drzewa, pn^a si^e do g^ory zas^laniaj^ac"+
             " ca^lkowicie niebo, powoduj^ac jednocze^snie, ^ze wszystko "+
             " tonie w lekkim mroku. Przesi^akni^ete zapachem zbutwia^lego"+
             " drewna i podgni^lych li^sci gor^ace powietrze, stoi w mi"+
             "ejscu nieporuszane nawet najdelikatniejszym podmuchem wiat"+
             "ru. Pod^lo^ze pokryte zesch^lymi li^s^cmi, tworzy mi^ekki k"+
             "obierzec szeleszcz^acy pod ka^zdym krokiem podr^o^znego.";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str+="Pot^e^zne i wysokie drzewa, pn^a si^e do g^ory zas^laniaj^ac"+
             " ca^lkowicie niebo, powoduj^ac jednocze^snie, ^ze wszystko "+
             " tonie w lekkim mroku. Przesi^akni^ete zapachem zbutwia^lego"+
             " drewna i podgni^lych li^sci ch^lodne powietrze, stoi w m"+
             "iejscu nieporuszane nawet najdelikatniejszym podmuchem wia"+
             "tru. Pod^lo^ze pokryte zesch^lymi li^s^cmi, tworzy mi^ekki "+
             "kobierzec szeleszcz^acy pod ka^zdym krokiem podr^o^znego. Z"+
             " g^ory od czasu do czasu spada niewielka kropelka wody, roz"+
             "bijaj^ac si^e na mi^ekkim runie.";
    }

    str+="\n";
    return str;
}
