/*
 * Opis i kod OHM
 * Data: Monday 27th of August 2007 01:29:01 PM
 * Ostatnio edytowane: Avard
 * Data: 29.06.2009
 */

#include <pogoda.h>
#include <stdproperties.h>
#include <mudtime.h>
#include "dir.h"

inherit LAS_ZAKON_OKOLICE_STD;

void
create_las()
{
    set_short("Ciemny stary las.");
    set_long("@@dlugi_opis@@");
    add_exit(LAS_ZAKON_OKOLICE_LOKACJE + "jezioro_las16.c","ne",0,LAS_FATIGUE,0);
    add_exit(LAS_ZAKON_OKOLICE_LOKACJE + "jezioro_las21.c","w",0,LAS_FATIGUE,0);
    add_exit(LAS_ZAKON_OKOLICE_LOKACJE + "jezioro_las30.c","se",0,LAS_FATIGUE,0);
    add_exit(LAS_ZAKON_OKOLICE_LOKACJE + "jezioro_las29.c","s",0,LAS_FATIGUE,0);
    add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "sj10","sw",0,SCIEZKA_ZO_FATIG,0);
    add_npc(LAS_ZAKON_OKOLICE_LIVINGI + "lis");

    add_prop(ROOM_I_INSIDE,0);
}

public string
exits_description()
{
    return "Id�c w kierunku p^o^lnocno-wschodnim, zachodnim, "+
    "po^ludniowo wschodnim oraz po^ludniowym zag^l^ebisz si^e w las, "+
    "natomiast pod^a^zajac na po^ludniowy zach^od dotrzesz do le^snego "+
    "traktu.\n";
}

string
dlugi_opis()
{
    string str = "";

    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="Mieszany las, kt^orego wysokie drzewa otaczaj^a mrokiem cale"+
             " poszycie, zaslaniaj^a niewysokim ro^slinom jakiekolwiek ^"+
             "swiat^lo. Nie przeszkodzi^lo to jednak rozrosn^a^c si^e co"+
             " bardziej odpornym gatunkom krzew^ow tworz^acych trudn^a do"+
             " przebycia g^estwine. Pod^lo^ze jest wolne od zielonkawych"+
             " ro^slin, znajduje si^e tu tylko wysci^o^lka z opad^lych l"+
             "i^sci i igie^l, pokryta w niekt^orych miejscach bia^lym ^s"+
             "niegiem.";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str+="Mieszany las, kt^orego wysokie drzewa otaczaj^a mrokiem cale"+
             " poszycie, zaslaniaj^a niewysokim ro^slinom jakiekolwiek ^"+
             "swiat^lo. Nie przeszkodzi^lo to jednak rozrosn^a^c si^e co"+
             " bardziej odpornym gatunkom krzew^ow tworz^acych trudn^a do"+
             " przebycia g^estwine. Pod^lo^ze jest wolne od zielonkawych"+
             " ro^slin, znajduje si^e tu tylko wysci^o^lka z opad^lych l"+
             "i^sci i igie^l, po kt^orej ^zwawo poruszaj^a si^e r^o^zne "+
             "ma^le owady.";
    }
    else if(pora_roku() == MT_LATO)
    {
        str+="Mieszany las, kt^orego wysokie drzewa otaczaj^a mrokiem cale"+
             " poszycie, zaslaniaj^a niewysokim ro^slinom jakiekolwiek ^"+
             "swiat^lo. Nie przeszkodzi^lo to jednak rozrosn^a^c si^e co"+
             " bardziej odpornym gatunkom krzew^ow tworz^acych trudn^a do"+
             " przebycia g^estwine. Pod^lo^ze jest wolne od zielonkawych"+
             " ro^slin, znajduje si^e tu tylko wysci^o^lka z opad^lych l"+
             "i^sci i igie^l, po kt^orej ^zwawo poruszaj^a si^e r^o^zne "+
             "ma^le owady.";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str+="Mieszany las, kt^orego wysokie drzewa otaczaj^a mrokiem cale"+
             " poszycie, zaslaniaj^a niewysokim ro^slinom jakiekolwiek ^"+
             "swiat^lo. Nie przeszkodzi^lo to jednak rozrosn^a^c si^e co"+
             " bardziej odpornym gatunkom krzew^ow tworz^acych trudn^a do"+
             " przebycia g^estwine. Pod^lo^ze jest wolne od zielonkawych"+
             " ro^slin, znajduje si^e tu tylko wysci^o^lka z opad^lych l"+
             "i^sci i igie^l, po kt^orej ^zwawo poruszaj^a si^e r^o^zne "+
             "ma^le owady. Czasami z g^ory spada r^o^znokolorowy li^s^c,"+
             " krecacy sie w k^o^lko spokojnie osiada na ziemii.";
    }

    str+="\n";
    return str;
}
