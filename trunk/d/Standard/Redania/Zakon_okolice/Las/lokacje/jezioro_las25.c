/*
 * Opis i kod OHM
 * Data: Monday 27th of August 2007 01:29:01 PM
 * Ostatnio edytowane: Avard
 * Data: 29.06.2009
 */

#include <pogoda.h>
#include <stdproperties.h>
#include <mudtime.h>
#include "dir.h"

inherit LAS_ZAKON_OKOLICE_STD;

void
create_las()
{
    set_short("Ciemny stary las.");
    set_long("@@dlugi_opis@@");
    add_exit(LAS_ZAKON_OKOLICE_LOKACJE + "jezioro_las24.c","w",0,LAS_FATIGUE,0);
    add_exit(LAS_ZAKON_OKOLICE_LOKACJE + "jezioro_las31.c","se",0,LAS_FATIGUE,0);
    add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "sj03","e",0,SCIEZKA_ZO_FATIG,0);
    add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "sj04","n",0,SCIEZKA_ZO_FATIG,0);
    add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "sj05","nw",0,SCIEZKA_ZO_FATIG,0);
    
    add_prop(ROOM_I_INSIDE,0);
}

public string
exits_description()
{
    return "Id�c w kierunku zachodnim i po^ludniowo-wschodnim zag^l^ebisz "+
	   "si^e w las, natomiast pod^a^zajac na p^o^lnoc i wsch^od dotrzesz do "+
	   "le^snego traktu.\n";
}

string
dlugi_opis()
{
    string str = "";

    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="G^esto rosn^ace pokryte mchem drzewa, poprzedzielane s^a krz"+
             "akami G^esto rosn^ace pokryte mchem drzewa, poprzedziela"+
             "ne s^a krzakami o grubych ciemnobr^azowych li^sciach, i ma"+
             "^lymi kar^lowatymi krzakami je^zyn. Pod^lo^ze jest wolne od"+
             " zielonkawych ro^slin, znajduje si^e tu tylko wysci^o^lka "+
             "z opad^lych li^sci i igie^l, pokryta w niekt^orych miejsca"+
             "ch bia^lym ^sniegiem.";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str+="G^esto rosn^ace pokryte mchem drzewa, poprzedzielane s^a krz"+
             "akami o grubych ciemnobr^azowych li^sciach, i ma^lymi kar^"+
             "lowatymi krzakami je^zyn. Pod^lo^ze jest wolne od zielonkaw"+
             "ych ro^slin, znajduje si^e tu tylko wysci^o^lka z opad^lych"+
             " li^sci i igie^l, po kt^orej ^zwawo poruszaj^a si^e r^ozne "+
             "ma^le owady.";
    }
    else if(pora_roku() == MT_LATO)
    {
        str+="G^esto rosn^ace pokryte mchem drzewa, poprzedzielane s^a krz"+
             "akami o grubych ciemnobr^azowych li^sciach, i ma^lymi kar^"+
             "lowatymi krzakami je^zyn. Pod^lo^ze jest wolne od zielonkaw"+
             "ych ro^slin, znajduje si^e tu tylko wysci^o^lka z opad^lych"+
             " li^sci i igie^l, po kt^orej ^zwawo poruszaj^a si^e r^ozne "+
             "ma^le owady.";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str+="G^esto rosn^ace pokryte mchem drzewa, poprzedzielane s^a krz"+
             "akami o grubych ciemnobr^azowych li^sciach, i ma^lymi kar^"+
             "lowatymi krzakami je^zyn. Pod^lo^ze jest wolne od zielonkaw"+
             "ych ro^slin, znajduje si^e tu tylko wysci^o^lka z opad^lych"+
             " li^sci i igie^l, po kt^orej ^zwawo poruszaj^a si^e r^o^zn"+
             "e ma^le owady. Czasami z g^ory spada r^o^znokolorowy li^s^c,"+
             " krecacy sie w k^o^lko spokojnie osiada na ziemii.";
    }

    str+="\n";
    return str;
}
