/*
 * Opis i kod OHM
 * Data: Monday 27th of August 2007 01:29:01 PM
 * Ostatnio edytowane: Avard
 * Data: 29.06.2009
 */

#include <pogoda.h>
#include <stdproperties.h>
#include <mudtime.h>
#include "dir.h"

inherit LAS_ZAKON_OKOLICE_STD;

void
create_las()
{
    set_short("Ciemny stary las.");
    set_long("@@dlugi_opis@@");
    add_exit(LAS_ZAKON_OKOLICE_LOKACJE + "jezioro_las19.c","nw",0,LAS_FATIGUE,0);
    add_exit(LAS_ZAKON_OKOLICE_LOKACJE + "jezioro_las20.c","ne",0,LAS_FATIGUE,0);
    add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "sj16","n",0,SCIEZKA_ZO_FATIG,0);
    add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "sj15","e",0,SCIEZKA_ZO_FATIG,0);
    add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "sj14","se",0,SCIEZKA_ZO_FATIG,0);
    
    add_prop(ROOM_I_INSIDE,0);
}

public string
exits_description()
{
    return "Id�c w kierunku p^o^lnocnym oraz wschodnim zag^l^ebisz si^e w las, natomiast "+
            "pod^a^zajac na p^o^lnocny-wsch^od i p^o^lnocny-zach^od dotrzesz do le^snego "+
            "traktu.\n";
}
string
dlugi_opis()
{
    string str = "";

    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="Mieszany las, kt^orego wysokie drzewa otaczaj^a mrokiem cale"+
             " poszycie, zaslaniaj^a niewysokim ro^slinom jakiekolwiek ^"+
             "swiat^lo. Nie przeszkodzi^lo to jednak rozrosn^a^c si^e co"+
             " bardziej odpornym gatunkom krzew^ow tworz^acych trudn^a do"+
             " przebycia g^estwine. Ciemny i g^esty, zastyg^ly jakby w c"+
             "zasie las, stoi w bezruchu i ciszy. Nie s^lycha^c tu nawet "+
             "pobrz^ekiwania owad^ow, ani szumu li^sci. Gdzieniegdzie na "+
             "le^snym runie, mo^zna zauwa^zy^c ma^le zasypane ^sniegiem "+
             "k^epki traw, reszta ro^slinnosci jest sucha, kar^lowata i "+
             "oszroniona, ze wzgl^edu na wiecznie panuj^acy w tym miejsc"+
             "u p^o^lmrok.";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str+="Mieszany las, kt^orego wysokie drzewa otaczaj^a mrokiem cale"+
             " poszycie, zaslaniaj^a niewysokim ro^slinom jakiekolwiek ^"+
             "swiat^lo. Nie przeszkodzi^lo to jednak rozrosn^a^c si^e co"+
             " bardziej odpornym gatunkom krzew^ow tworz^acych trudn^a do"+
             " przebycia g^estwine. Ciemny i g^esty, zastyg^ly jakby w c"+
             "zasie las, stoi w bezruchu i ciszy. Nie s^lycha^c tu nawet "+
             "pobrz^ekiwania owad^ow, ani szumu li^sci. Gdzieniegdzie na "+
             "le^snym runie, mo^zna zauwa^zy^c ma^le zielonkawe kepki tra"+
             "w, reszta ro^slinnosci jest sucha i karlowata, ze wzgl^edu "+
             " na wiecznie panuj^acy w tym miejscu p^o^lmrok.";
    }
    else if(pora_roku() == MT_LATO)
    {
        str+="Mieszany las, kt^orego wysokie drzewa otaczaj^a mrokiem cale"+
             " poszycie, zaslaniaj^a niewysokim ro^slinom jakiekolwiek ^"+
             "swiat^lo. Nie przeszkodzi^lo to jednak rozrosn^a^c si^e co"+
             " bardziej odpornym gatunkom krzew^ow tworz^acych trudn^a do"+
             " przebycia g^estwine. Ciemny i g^esty, zastyg^ly jakby w c"+
             "zasie las, stoi w bezruchu i ciszy. Nie s^lycha^c tu nawet "+
             "pobrz^ekiwania owad^ow, ani szumu li^sci. Gdzieniegdzie na "+
             "le^snym runie, mo^zna zauwa^zy^c ma^le zielonkawe kepki tra"+
             "w, reszta ro^slinnosci jest sucha i karlowata, ze wzgl^edu "+
             " na wiecznie panuj^acy w tym miejscu p^o^lmrok.";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str+=" Mieszany las, kt^orego wysokie drzewa otaczaj^a mrokiem ca"+
             "le poszycie, zaslaniaj^a niewysokim ro^slinom jakiekolwiek"+
             " ^swiat^lo. Nie przeszkodzi^lo to jednak rozrosn^a^c si^e "+
             "co bardziej odpornym gatunkom krzew^ow tworz^acych trudn^a "+
             "do przebycia g^estwine. Ciemny i g^esty, zastyg^ly jakby w"+
             " czasie las, stoi w bezruchu i ciszy. Nie s^lycha^c tu nawe"+
             "t pobrz^ekiwania owad^ow, ani szumu li^sci. Gdzieniegdzie n"+
             "a le^snym runie, mo^zna zauwa^zy^c ma^le pousychane k^epki"+
             " traw, reszta ro^slinnosci jest kar^lowata, ze wzgl^edu na"+
             " wiecznie panuj^acy w tym miejscu p^o^lmrok. Ch^lodne powi"+
             "etrze powoli osiada na porozwieszanych paj^eczynach, zosta"+
             "wiaj^ac na nich kropelki wody.";
    }

    str+="\n";
    return str;
}
