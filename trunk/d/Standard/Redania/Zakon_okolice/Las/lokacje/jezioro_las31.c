/*
 * Opis i kod OHM
 * Data: Monday 27th of August 2007 01:29:01 PM
 * Ostatnio edytowane: Avard
 * Data: 29.06.2009
 */

#include <pogoda.h>
#include <stdproperties.h>
#include <mudtime.h>
#include "dir.h"

inherit LAS_ZAKON_OKOLICE_STD;

void
create_las()
{
    set_short("W okolicy scie^zki.");
    set_long("@@dlugi_opis@@");
    add_exit(LAS_ZAKON_OKOLICE_LOKACJE + "jezioro_las26.c","ne",0,LAS_FATIGUE,0);
    add_exit(LAS_ZAKON_OKOLICE_LOKACJE + "jezioro_las25.c","nw",0,LAS_FATIGUE,0);
    add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "sj03","n",0,SCIEZKA_ZO_FATIG,0);
    add_prop(ROOM_I_INSIDE,0);
    add_npc(LAS_ZAKON_OKOLICE_LIVINGI + "wilk");
}

public string
exits_description()
{
    return "Id�c w kierunku p^o^lnocno-wschodnim oraz p^o^lnocno-zachodnim "+
            "zag^l^ebisz si^e w las, natomiast pod^a^zajac na p^o^lnoc dotrzesz "+
            "do le^snego traktu.\n";
}

string
dlugi_opis()
{
    string str = "";

    str+="Rzadkie drzewa i ma�a ilo�� krzew�w, najprawdopodobniej mog� "+
    "�wiadczy�, �e teren ten znajduje si� gdzie� niedaleko �cie�ki. I "+
    "istotnie - nieco na p�noc dostrzec mo�na "+
    "wydeptan� drog�, kt�rej granic� tworzy niezbyt zwarta, acz ci�g�a "+
    "linia krzew�w. W tej cz�ci lasu zdaj� si� przewa�a� buki i d�by, "+
    "mo�na jednak dostrzec poza nimi wiele innych gatunk�w drzew od brzozy "+
    "po leszczyn�. Ga��zie za� poruszaj� si� lekko w rytm wyj�cego w "+
    "konarach wiatru.";

    str+="\n";
    return str;
}