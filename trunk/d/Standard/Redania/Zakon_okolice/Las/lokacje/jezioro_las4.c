/*
 * Opis i kod OHM
 * Data: Monday 27th of August 2007 01:29:01 PM
 * Ostatnio edytowane: Avard
 * Data: 29.06.2009
 */

#include <pogoda.h>
#include <stdproperties.h>
#include <mudtime.h>
#include "dir.h"

inherit LAS_ZAKON_OKOLICE_STD;

void
create_las()
{
    set_short("Ciemny stary las.");
    set_long("@@dlugi_opis@@");
	//plus jezioro na w
    add_exit(LAS_ZAKON_OKOLICE_LOKACJE + "jezioro_las7.c","se",0,LAS_FATIGUE,0);
    add_prop(ROOM_I_INSIDE,0);
}

public string
exits_description()
{
    return "Id�c w kierunku po^ludniowo-wschodnim z^l^ebisz si^e w las, natomiast "+
            "pod^a^zajac na zach^od dotrzesz do wielkiego jeziora.\n";
}

string
dlugi_opis()
{
    string str = "";

    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="G^esto rosn^ace pokryte mchem drzewa, poprzedzielane s^a krz"+
             "akami o grubych ciemnobr^azowych li^sciach, i ma^lymi kar^"+
             "lowatymi krzakami je^zyn. Nagie pnie i grube pousychane kon"+
             "ary, nadaj^a tej okolicy tajemniczy i straszny widok, kt^o"+
             "ry dodatkowo pot^eguj^a ro^sliny, pokryte szronem i delika"+
             "tnychm puchem.";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str+="G^esto rosn^ace pokryte mchem drzewa, poprzedzielane s^a krz"+
             "akami o grubych ciemnobr^azowych li^sciach, i ma^lymi kar^"+
             "lowatymi krzakami je^zyn. Nagie pnie i grube pousychane kon"+
             "ary, nadaj^a tej okolicy tajemniczy i straszny widok, kt^o"+
             "ry dodatkowo pot^eguje niezm^acona niczym cisza.";
    }
    else if(pora_roku() == MT_LATO)
    {
        str+="G^esto rosn^ace pokryte mchem drzewa, poprzedzielane s^a krz"+
             "akami o grubych ciemnobr^azowych li^sciach, i ma^lymi kar^"+
             "lowatymi krzakami je^zyn. Nagie pnie i grube pousychane kon"+
             "ary, nadaj^a tej okolicy tajemniczy i straszny widok, kt^o"+
             "ry dodatkowo pot^eguje niezm^acona niczym cisza.";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str+="G^esto rosn^ace pokryte mchem drzewa, poprzedzielane s^a krz"+
             "akami o grubych ciemnobr^azowych li^sciach, i ma^lymi kar^"+
             "lowatymi krzakami je^zyn. Nagie pnie i grube pousychane kon"+
             "ary, nadaj^a tej okolicy tajemniczy i straszny widok, kt^o"+
             "ry dodatkowo pot^eguj^a spadaj^ace li^scie i liczne paj^ec"+
             "zny porozwieszane prawie w ka^zdym miejscu.";
    }

    str+="\n";
    return str;
}
