/*
 * Opis i kod OHM
 * Data: Monday 27th of August 2007 01:29:01 PM
 * Ostatnio edytowane: Avard
 * Data: 29.06.2009
 */

#include <pogoda.h>
#include <stdproperties.h>
#include <mudtime.h>
#include "dir.h"

inherit LAS_ZAKON_OKOLICE_STD;

void
create_las()
{
    set_short("Ciemny stary las.");
    set_long("@@dlugi_opis@@");
    add_exit(LAS_ZAKON_OKOLICE_LOKACJE + "jezioro_las38.c","e",0,LAS_FATIGUE,0);
    add_exit(LAS_ZAKON_OKOLICE_LOKACJE + "jezioro_las34.c","nw",0,LAS_FATIGUE,0);
    add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "sj08","ne",0,SCIEZKA_ZO_FATIG,0);
    add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "sj09","n",0,SCIEZKA_ZO_FATIG,0);
    
    add_prop(ROOM_I_INSIDE,0);
    add_npc(LAS_ZAKON_OKOLICE_LIVINGI + "wilk");
}

public string
exits_description()
{
     return "G^esto rosn^ace krzaki i drzewa, pozwalaj^a Ci kontynuowa^c "+
	    "podr^o^z w kierunku p^o^lnocnym oraz p^o^lnocno-wschodnim. Kieruj^ac "+
	    "sie na wsch^od i p^o^lnocny-zachod dotrzesz w okolice ^scie^zki.\n";
}

string
dlugi_opis()
{
    string str = "";

    str+="Rzadkie drzewa i ma�a ilo�� krzew�w, najprawdopodobniej mog� "+
    "�wiadczy�, �e teren ten znajduje si� gdzie� niedaleko �cie�ki. I "+
    "istotnie - zar�wno nieco na p�noc jak i p�nocny wsch�d dostrzec mo�na "+
    "wydeptan� drog�, kt�rej granic� tworzy niezbyt zwarta, acz ci�g�a "+
    "linia krzew�w. W tej cz�ci lasu zdaj� si� przewa�a� buki i d�by, "+
    "mo�na jednak dostrzec poza nimi wiele innych gatunk�w drzew od brzozy "+
    "po leszczyn�. Ga��zie za� poruszaj� si� lekko w rytm wyj�cego w "+
    "konarach wiatru.";

    str+="\n";
    return str;
}
