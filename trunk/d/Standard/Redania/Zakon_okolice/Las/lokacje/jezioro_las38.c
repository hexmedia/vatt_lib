/*
 * Opis i kod OHM
 * Data: Monday 27th of August 2007 01:29:01 PM
 * Ostatnio edytowane: Avard
 * Data: 29.06.2009
 */

#include <pogoda.h>
#include <stdproperties.h>
#include <mudtime.h>
#include "dir.h"

inherit LAS_ZAKON_OKOLICE_STD;

void
create_las()
{
    set_short("Ciemny stary las.");
    set_long("@@dlugi_opis@@");
    add_exit(LAS_ZAKON_OKOLICE_LOKACJE + "jezioro_las35.c","ne",0,LAS_FATIGUE,0);
    add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "sj08.c","n",0,SCIEZKA_ZO_FATIG,0);
    add_exit(LAS_ZAKON_OKOLICE_LOKACJE + "jezioro_las37.c","w",0,LAS_FATIGUE,0);
    add_prop(ROOM_I_INSIDE,0);
}

public string
exits_description()
{
     return "G^esto rosn^ace krzaki i drzewa, pozwalaj^a Ci kontynuowa^c "+
	    "podr^o^z w kierunku p^o^lnocnym. Kieruj^ac sie na p^o^lnocny-wsch^od "+
	    "i zach^od dotrzesz w okolice ^scie^zki.\n";
}

string
dlugi_opis()
{
    string str = "";

    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="Wsz^edzie wok^o^l rosn^a pot^e^zne, wiekowe drzewa o grubej "+
             "pooranej bruzdami korze. Te wspania^le okazy o roz^lozysty"+
             "ch koronach przes^laniaj^a ca^le niebo pokrywaj^ac poszycie"+
             " wiecznym mrokiem. G^este krzaki, wyrastaj^ace gdzieniegdzi"+
             "e mi^edzy drzewami, o suchych i ma^lych ga^l^eziach, pokryt"+
             "ych ko^lcami, zdecydowanie utrudniaj^a chodzenie po tym les"+
             "ie. Ro^slinno^s^c pokryta jest szronem , a ziemi mo^zna za"+
             "uwa^zy^c ^slady odbite na ^sniegu";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str+="Wsz^edzie wok^o^l rosn^a pot^e^zne, wiekowe drzewa o grubej "+
             "pooranej bruzdami korze. Te wspania^le okazy o roz^lozysty"+
             "ch koronach przes^laniaj^a ca^le niebo pokrywaj^ac poszycie"+
             " wiecznym mrokiem. G^este krzaki, wyrastaj^ace gdzieniegdzi"+
             "e mi^edzy drzewami, o suchych i ma^lych ga^l^eziach, pokryt"+
             "ych ko^lcami, zdecydowanie utrudniaj^a chodzenie po tym les"+
             "ie.";
    }
    else if(pora_roku() == MT_LATO)
    {
        str+="Wsz^edzie wok^o^l rosn^a pot^e^zne, wiekowe drzewa o grubej "+
             "pooranej bruzdami korze. Te wspania^le okazy o roz^lozysty"+
             "ch koronach przes^laniaj^a ca^le niebo pokrywaj^ac poszycie"+
             " wiecznym mrokiem. G^este krzaki, wyrastaj^ace gdzieniegdzi"+
             "e mi^edzy drzewami, o suchych i ma^lych ga^l^eziach, pokryt"+
             "ych ko^lcami, zdecydowanie utrudniaj^a chodzenie po tym les"+
             "ie.";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str+="Wsz^edzie wok^o^l rosn^a pot^e^zne, wiekowe drzewa o grubej "+
             "pooranej bruzdami korze. Te wspania^le okazy o roz^lozysty"+
             "ch koronach przes^laniaj^a ca^le niebo pokrywaj^ac poszycie"+
             " wiecznym mrokiem. G^este krzaki, wyrastaj^ace gdzieniegdzi"+
             "e mi^edzy drzewami, o suchych i ma^lych ga^l^eziach, pokryt"+
             "ych ko^lcami, zdecydowanie utrudniaj^a chodzenie po tym les"+
             "ie. Ro^slinno^s^c pokryta jest ma^lymi kropelkami wody, a p"+
             "omiedzy drzewami mo^zna zauwa^zyc porozci^agane paj^eczyny.";
    }

    str+="\n";
    return str;
}