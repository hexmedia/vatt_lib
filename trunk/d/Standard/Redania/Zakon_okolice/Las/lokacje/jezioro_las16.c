/*
 * Opis i kod OHM
 * Data: Monday 27th of August 2007 01:29:01 PM
 * Ostatnio edytowane: Avard
 * Data: 29.06.2009
 */

#include <pogoda.h>
#include <stdproperties.h>
#include <mudtime.h>
#include "dir.h"

inherit LAS_ZAKON_OKOLICE_STD;

void
create_las()
{
    set_short("Pomi^edzy starymi drzewami.");
    set_long("@@dlugi_opis@@");
    add_exit(LAS_ZAKON_OKOLICE_LOKACJE + "jezioro_las17.c","e",0,LAS_FATIGUE,0);
    add_exit(LAS_ZAKON_OKOLICE_LOKACJE + "jezioro_las11.c","n",0,LAS_FATIGUE,0);
    add_exit(LAS_ZAKON_OKOLICE_LOKACJE + "jezioro_las22.c","sw",0,LAS_FATIGUE,0);

    add_prop(ROOM_I_INSIDE,0);
}

public string
exits_description()
{
     return "G^esto rosn^ace krzaki i drzewa, pozwalaj^a ci kontynuowa^c "+
	    "podr^o^z w kierunku wschodnim, p^o^lnocnym i po^ludniowo-zachodnim.\n";
}

string
dlugi_opis()
{
    string str = "";

    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="Pomimo tego, ^ze drzewa w tym miejscu rosn^a rzadziej ich ro"+
             "z^lo^zyste korony zas^laniaj^a prawie ca^le ^swiat^lo, i ni"+
             "ewiele z niego dostaje si^e tutaj na d^o^l. Pod^lo^ze jest "+
             "wolne od zielonkawych ro^slin, znajduje si^e tu tylko wysc"+
             "i^o^lka z opad^lych li^sci i igie^l, pokryta w niekt^orych"+
             " miejscach bia^lym ^sniegiem.";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str+="Pomimo tego, ^ze drzewa w tym miejscu rosn^a rzadziej ich ro"+
             "z^lo^zyste korony zas^laniaj^a prawie ca^le ^swiat^lo, i ni"+
             "ewiele z niego dostaje si^e tutaj na d^o^l. Pod^lo^ze jest "+
             "wolne od zielonkawych ro^slin, znajduje si^e tu tylko wysc"+
             "i^o^lka z opad^lych li^sci i igie^l, po kt^orej ^zwawo por"+
             "uszaj^a si^e r^o^zne ma^le owady.";
    }
    else if(pora_roku() == MT_LATO)
    {
        str+="Pomimo tego, ^ze drzewa w tym miejscu rosn^a rzadziej ich ro"+
             "z^lo^zyste korony zas^laniaj^a prawie ca^le ^swiat^lo, i ni"+
             "ewiele z niego dostaje si^e tutaj na d^o^l. Pod^lo^ze jest "+
             "wolne od zielonkawych ro^slin, znajduje si^e tu tylko wysc"+
             "i^o^lka z opad^lych li^sci i igie^l, po kt^orej ^zwawo por"+
             "uszaj^a si^e r^o^zne ma^le owady.";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str+="Pomimo tego, ^ze drzewa w tym miejscu rosn^a rzadziej ich ro"+
             "z^lo^zyste korony zas^laniaj^a prawie ca^le ^swiat^lo, i ni"+
             "ewiele z niego dostaje si^e tutaj na d^o^l. Pod^lo^ze jest "+
             "wolne od zielonkawych ro^slin, znajduje si^e tu tylko wysc"+
             "i^o^lka z opad^lych li^sci i igie^l, po kt^orej ^zwawo por"+
             "uszaj^a si^e r^o^zne ma^le owady. Czasami z g^ory spada r^"+
             "o^znokolorowy li^s^c, krecacy sie w k^o^lko spokojnie osia"+
             "da na ziemii.";
    }

    str+="\n";
    return str;
}
