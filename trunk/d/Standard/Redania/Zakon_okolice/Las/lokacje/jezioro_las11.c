/*
 * Opis i kod OHM
 * Data: Monday 27th of August 2007 01:29:01 PM
 * Ostatnio edytowane: Avard
 * Data: 29.06.2009
 */

#include <pogoda.h>
#include <stdproperties.h>
#include <mudtime.h>
#include "dir.h"

inherit LAS_ZAKON_OKOLICE_STD;

void
create_las()
{
    set_short("W g^l^ebi wiekowego lasu.");
    set_long("@@dlugi_opis@@");
    add_exit(LAS_ZAKON_OKOLICE_LOKACJE + "jezioro_las17.c","se",0,LAS_FATIGUE,0);
    add_exit(LAS_ZAKON_OKOLICE_LOKACJE + "jezioro_las16.c","s",0,LAS_FATIGUE,0);
    add_exit(LAS_ZAKON_OKOLICE_LOKACJE + "jezioro_las7.c","nw",0,LAS_FATIGUE,0);
    add_prop(ROOM_I_INSIDE,0);
    add_npc(LAS_ZAKON_OKOLICE_LIVINGI + "los");
}

public string
exits_description()
{
     return "G^esto rosn^ace krzaki i drzewa, pozwalaj^a Ci kontynuowa^c "+
     "podr^o^z w kierunku po^ludniowo-wschodnim i po^ludniowym oraz "+
     "p^o^lnocno-zachodnim.\n";
}
string
dlugi_opis()
{
    string str = "";

    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="Rosn^ace blisko siebie grube drzewa, pokryte mchem i naro^sl"+
             "ami utrudniaj^a podr^oz przez ten wiekowy las rozci^agaj^ac"+
             "y sie w ka^zdym kierunku. Pousychane z braku s^lo^nca, dol"+
             "ne ga^l^ezie ^lami^a si^e pod mocniejszym dotykiem, wydaj^a"+
             "c z siebie g^luchy trzask. Nagie pnie i grube pousychane k"+
             "onary, nadaj^a tej okolicy tajemniczy i straszny widok, a "+
             "odczucie to pot^eguj^a ro^sliny, pokryte szronem i delikat"+
             "nychm puchem.";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str+="Rosn^ace blisko siebie grube drzewa, pokryte mchem i naro^sl"+
             "ami utrudniaj^a podr^oz przez ten wiekowy las rozci^agaj^ac"+
             "y sie w ka^zdym kierunku. Pousychane z braku s^lo^nca, dol"+
             "ne ga^l^ezie ^lami^a si^e pod mocniejszym dotykiem, wydaj^a"+
             "c z siebie g^luchy trzask. Nagie pnie i grube pousychane k"+
             "onary, nadaj^a tej okolicy tajemniczy i straszny widok, a "+
             "odczucie to pot^eguje zaduch i niezm^acona niczym cisza.";
    }
    else if(pora_roku() == MT_LATO)
    {
        str+="Rosn^ace blisko siebie grube drzewa, pokryte mchem i naro^sl"+
             "ami utrudniaj^a podr^oz przez ten wiekowy las rozci^agaj^ac"+
             "y sie w ka^zdym kierunku. Pousychane z braku s^lo^nca, dol"+
             "ne ga^l^ezie ^lami^a si^e pod mocniejszym dotykiem, wydaj^a"+
             "c z siebie g^luchy trzask. Nagie pnie i grube pousychane k"+
             "onary, nadaj^a tej okolicy tajemniczy i straszny widok, a "+
             "odczucie to pot^eguje zaduch i niezm^acona niczym cisza.";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str+="Rosn^ace blisko siebie grube drzewa, pokryte mchem i naro^sl"+
             "ami utrudniaj^a podr^oz przez ten wiekowy las rozci^agaj^ac"+
             "y sie w ka^zdym kierunku. Pousychane z braku s^lo^nca, dol"+
             "ne ga^l^ezie ^lami^a si^e pod mocniejszym dotykiem, wydaj^a"+
             "c z siebie g^luchy trzask. Nagie pnie i grube pousychane k"+
             "onary, nadaj^a tej okolicy tajemniczy i straszny widok, a "+
             "odczucie to pot^eguj^a spadaj^ace li^scie i liczne paj^ecz"+
             "ny porozwieszane prawie w ka^zdym miejscu.";
    }

    str+="\n";
    return str;
}
