inherit "/std/zwierze";
inherit "/std/act/action";
inherit "/std/act/trigaction.c";

#include <macros.h>
#include <stdproperties.h>
#include <pl.h>
#include <formulas.h>
#include <std.h>
#include <composite.h>
#include <filter_funs.h>
#include <cmdparse.h>
#include <mudtime.h>
#include <wa_types.h>
#include <ss_types.h>
#include <object_types.h>
#include "dir.h"

void
create_zwierze()
{
    add_prop(CONT_I_WEIGHT, 540000+random(155000));
    add_prop(CONT_I_HEIGHT, 140+random(60));
    set_stats (({ 60, 30, 60, 20, 20 }));
    set_skill(SS_DEFENCE, 25);
    set_skill(SS_UNARM_COMBAT, 60);
    set_skill(SS_AWARENESS, 34);
    set_aggressive(0);
    ustaw_odmiane_rasy("^lo^s");
    set_gender(G_MALE);

    random_przym(({"brunatny","brunatni", "ciemnoszary","ciemnoszarzy",
        "br^azowy", "br^azowi", "ciemnobr^azowy", "ciemnobr^azowi","szarawy",
        "szarawi"}),({"nerwowy","nerwowi", "muskularny","muskularni", "pot^e^zny", 
        "pot^e^zni", "wychudzony","wychudzeni"}),2);

    set_long("Wyd^lu^zona, szeroka, mi^esista i ruchliwa g^orna warga "+
        "w po^l^aczeniu z szerokim, masywnym uz^ebieniem i ma^lymi, uszami "+
        "wskazuj^a na to, i^z zwierz^e na, kt^ore patrzysz to ^lo^s. "+
        "Ten pot^e^zny, kopytny ssak, posiada sko^sne, skierowane do do^lu "+
        "nozdrza, kr^otk^a i masywn^a szyje umo^zliwiaj^ac^a obracanie "+
        "g^lowy w bardzo szerokim zakresie. Pod ^lbem, na kt^orym wida^c "+
        "^lopaty brunatnego rozga^l^ezionego poro^za, zwisa mu naro^sl "+
        "t^luszczowa, obro^sni^eta d^lugim czarnym w^losem. "+
        "Silne i bardzo d^lugie nogi zako^nczone s^a masywnymi, szeroko "+
        "rozsuwalnymi racicami u^latwiaj^acymi chodzenie po ^sniegu, "+
        "bagnach i mokrad^lach.\n");

    set_attack_unarmed(0, 10, 10, W_BLUDGEON,          20, "racica", ({"lewy",  "przedni"}));
    set_attack_unarmed(1, 10, 10, W_BLUDGEON,          20, "racica", ({"prawy", "przedni"}));
    set_attack_unarmed(2, 10, 10, W_BLUDGEON,           5, "racica", ({"lewy",  "tylny"}));
    set_attack_unarmed(3, 10, 10, W_BLUDGEON,           5, "racica", ({"prawy", "tylny"}));
    set_attack_unarmed(5, 10, 10, W_BLUDGEON|W_IMPALE, 50, "poro^ze", ({"masywne", "szerokie"}));

    set_hitloc_unarmed(0, ({ 1, 1, 1 }), 10, "^leb");
    set_hitloc_unarmed(1, ({ 1, 1, 1 }), 30, "tu^l^ow");
    set_hitloc_unarmed(2, ({ 1, 1, 1 }), 20, "noga", ({"lewy", "przedni"}));
    set_hitloc_unarmed(3, ({ 1, 1, 1 }), 20, "noga", ({"prawy", "przedni"}));
    set_hitloc_unarmed(4, ({ 1, 1, 1 }), 10, "noga", ({"tylny", "lewy"}));
    set_hitloc_unarmed(5, ({ 1, 1, 1 }), 10, "noga", ({"tylny", "przedni"}));
    
    set_cact_time(10);
    add_cact("warknij");
    add_cact("emote wyje przeci^agle.");

    set_act_time(30);
    add_act("emote podnosi do g^ory g^low^e i wyje g^lo^sno.");
    add_act("emote rozgl^ada si^e niespokojnie.");
    add_act("emote podnosi g^low^e i w^eszy uwa^znie.");

    add_leftover("/std/skora", "sk^ora", 1, 0, 1, 1, 45 + random(5), O_SKORY); 
    add_leftover("/std/leftover", "poro�e", 1, 0, 1, 1, 0, O_KOSCI);//FIXME
    // Z tego co wiem Krun chcial robic oddzielny leftover do porozy.

    set_restrain_path(LAS_ZAKON_OKOLICE_LOKACJE);
    set_random_move(100);
}