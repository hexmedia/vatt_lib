inherit "/std/zwierze";
inherit "/std/act/action";
inherit "/std/act/trigaction.c";

#include <macros.h>
#include <stdproperties.h>
#include <pl.h>
#include <formulas.h>
#include <std.h>
#include <composite.h>
#include <filter_funs.h>
#include <cmdparse.h>
#include <mudtime.h>
#include <wa_types.h>
#include <ss_types.h>
#include <object_types.h>
#include "dir.h"

void
create_zwierze()
{

    add_prop(CONT_I_WEIGHT, 540000+random(155000));
    add_prop(CONT_I_HEIGHT, 140+random(60));
    set_stats (({ 40+random(10), 30+random(10), 60, 20, 20 }));
    set_skill(SS_DEFENCE, 25);
    set_skill(SS_UNARM_COMBAT, 40+random(20));
    set_skill(SS_AWARENESS, 34);
    set_aggressive(1);
    set_attack_chance(40);

    ustaw_odmiane_rasy("dzik");
    set_gender(G_MALE);
    random_przym(({"brunatny","brunatni", "ciemnoszary","ciemnoszarzy",
        "czarny", "czarni", "ciemnobr^azowy", "ciemnobr^azowi","szarawy",
        "szarawi"}),({"gruby","grubi", "ob^locony","ob^loceni", "masywny", 
        "masywni", "wychudzony","wychudzeni"}),2);
    
    set_long("Szorstkie w^losie porastaj^ace cia^lo tego osobnika "+
         "jest ob^locone i posklejane ^zywic^a. Ma^le czarne "+
         "i swiec^ace oczka, czujnie przeczesuj^a okolice, "+
         "d^lugie i szerokie k^ly wyrastaj^ace z bok^ow szczeki "+
         "wygl^adaj^a nadzwyczaj gro^xnie i niebezpiecznie. "+
         "Kr^otkie n^o^zki, poruszaj^ace tego dzika zako^nczone "+
         "s^a mocnymi kopytami, s^lu^z^acymi tak^ze do wykopywania "+
         "r^o^znorakiego rodzaju jedzenia z ziemi.\n");

    set_attack_unarmed(5, 10, 10, W_BLUDGEON|W_IMPALE, 50, "k^ly", 
        ({"d^lugie", "ostre"}));

    set_hitloc_unarmed(0, ({ 1, 1, 1 }), 10, "^leb");
    set_hitloc_unarmed(1, ({ 1, 1, 1 }), 30, "tu^l^ow");
    set_hitloc_unarmed(2, ({ 1, 1, 1 }), 20, "noga", ({"lewy", "przedni"}));
    set_hitloc_unarmed(3, ({ 1, 1, 1 }), 20, "noga", ({"prawy", "przedni"}));
    set_hitloc_unarmed(4, ({ 1, 1, 1 }), 10, "noga", ({"tylny", "lewy"}));
    set_hitloc_unarmed(5, ({ 1, 1, 1 }), 10, "noga", ({"tylny", "przedni"}));
    
    set_cact_time(10);
    add_cact("emote kwiczy g^lo^sno.");
    add_cact("emote charczy przez nozdrza.");
    add_cact("emote rusza na ciebie z impetem.");

    set_act_time(30);
    add_act("emote podnosi do g^ory g^low^e i kwiczy g^lo^sno.");
    add_act("emote rozgl^ada si^e niespokojnie.");
    add_act("emote podnosi g^low^e i w^eszy uwa^znie.");
    add_act("emote kopie racic^a w ziemi, w poszukiwaniu jedzenia.");

    add_leftover("/std/skora", "sk^ora", 1, 0, 1, 1, 35 + random(5), O_SKORY); 
    add_leftover("/std/leftover", "kie^l", 2 + random(2), 0, 1, 1, 0, O_KOSCI);

    set_restrain_path(LAS_ZAKON_OKOLICE_LOKACJE);
    set_random_move(100);
}

