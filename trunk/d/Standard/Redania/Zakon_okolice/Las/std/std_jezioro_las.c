/* Autor: OHM bazowane na std lasu rinde
   Data : 28.09.07 
   Info : Std do lasu kolo jeziora 
          Zmiany - Avard 26.07.09        */
inherit "/std/room";

#include <mudtime.h>
#include <language.h>
#include <macros.h>
#include <stdproperties.h>
#include "dir.h"

string event_lasu();
string dlugi_opis();
string opis_nocy();

void
create_las()
{
}

nomask void
create_room()
{
    set_long(&dlugi_opis());
    set_polmrok_long(&opis_nocy());

    /*add_item(({"drzewa","las"}), "Tak, tutaj powinien byc jakis opis :P\n");
      add_item(({"krzewy","krzaki"}), "Tak, tutaj powinien byc jakis opis :P\n");

OPISY
DRZEWA:

Stare i powykr^ecane drzewa, pokryte s^a ciemnozielonym mchem, i 
dlugimi suchymi porostami. Dolne partie ga^l^ezi s^a suche, z 
powodu braku dostatecznej ilo^sci promieni s^lonecznych.

Liczne kar^lowate, oraz te wysokie strzelaj^ace w gor^e, w stron^e
nieba, pot^ezne drzewa tworz^a tutaj ciemny g^aszcz.

Stare i wiekowe drzewa iglaste i li^sciaste pn^a si^e do g^ory, tworz^ac
nad g^low^a prawie nieprzenikalny dach z li^sci.

KRZEWY:

Bujne, cho^c wyschni^ete, s^a prawie ca^le pokryte d^lugimi kolcami,
kt^ore z ^latwo^sci^a mog^a przebi^c si^e przez ubranie podr^o^znego.

G^este krzaki, tworz^a ^zywop^loty uniemozliwiaj^ace przemieszczanie
si^e w niekt^orych kierunkach.

Podesch^le, z d^lugimi kolcami krzewy stanowi^a naturaln^a barier^e
dla wiekszych zwierz^at i podr^o^znych.
*/    
    set_event_time(300.0);
    add_event(&event_lasu());

    add_sit(({"na trawie","na ziemi"}),"na trawie","z trawy",0);
    add_sit("pod drzewem","pod jednym z drzew","spod drzewa",0);
    add_sit("na pniu","na jednym z pni","z pniaka",1);
    add_prop(ROOM_I_TYPE, ROOM_FOREST); 
    add_prop(ROOM_I_WSP_Y, WSP_Y_ZAKON_OKOLICE);
    add_prop(ROOM_I_WSP_X, WSP_X_ZAKON_OKOLICE);

    add_subloc(({"drzewo", "drzewa", "drzewu", "drzewo",
        "drzewem", "drzewie"}));
    add_subloc_prop("drzewo", SUBLOC_I_MOZNA_POLOZ, 1);
    add_subloc_prop("drzewo", SUBLOC_I_MOZNA_ODLOZ, 1);
    add_subloc_prop("drzewo", SUBLOC_I_TYP_POD, 1); 

    ustaw_liste_drzew(({"dab", "klon", "brzoza"}));
    dodaj_ziolo(({"czarny_bez-lisc.c","czarny_bez-kwiaty.c",
    "czarny_bez-owoce.c"}));
    dodaj_ziolo("arcydziegiel.c");
    dodaj_ziolo("babka_lancetowata.c");
    dodaj_ziolo("piolun.c");

    create_las();
}
public int
unq_no_move(string str)
{
    notify_fail("Niestety, g^este zaro^sla nie pozwalaj^a ci "+
        "p^oj^s^c w tym kierunku.\n");
    return 0;
}
string
event_lasu()
{
    switch (pora_dnia())
    {
    case MT_RANEK:
    case MT_POLUDNIE:
    case MT_POPOLUDNIE:
        return process_string(({"Nad twoj^a g^low^a przelecia^l jaki^s "+
            "niedu^zy ptak.\n",
            "Wysoko nad tob^a z szeroko rozpostartymi skrzyd^lami "+
            "przelatuje jastrz^ab.\n",
            "Malutkie muszki otoczy^ly ci^e z ka^zdej strony. \n",
            "Ma^la wysuszona ga^l^azka p^ek^la ci pod stop^a. \n",
            "Jaki^s owad przelecia^l ci ko^lo ucha. \n",
            "Powietrze stoi w miejscu, nieporuszone nawet "+
            "najdelikatniejszym podmuchem wiatru. \n",
            "Niespodziewanie co^s poruszy^lo si^e w krzakach. \n",
            "Cisz^e panuj^aca w lesie przerwa^l stukot dzi^ecio^la. \n",
            "Gdzies z oddali wydobywa sie wycie dzikiego zwierz^ecia, "+
            "przerywajace wszechobecna cisz^e. \n",
            "@@pora_roku_dzien@@"})[random(10)]);
    default: // wieczor, noc
        return process_string(({"Masz wra^zenie, ^ze obserwuje ci^e jakie^s "+
            "zwierz^e.\n",
            "W oddali s^lyszysz ryk dzikiego zwierz^ecia.\n",
            "Ma^la wysuszona ga^l^azka p^ek^la ci pod stop^a. \n",
            "Co^s przebieg^lo ci mi^edzy nogami. \n",
            "Jaki^s owad przelecia^l ci ko^lo ucha. \n",
            "Powietrze stoi w miejscu, nieporuszone nawet "+
            "najdelikatniejszym podmuchem wiatru. \n",
            "Niespodziewanie co^s poruszy^lo si^e w krzakach. \n",
            "Gdzies z oddali wydobywa sie wycie dzikiego zwierz^ecia, "+
            "przerywajace wszechobecna cisz^e. \n",
            "@@pora_roku_noc@@"})[random(10)]);
    }
}
string
pora_roku_dzien()
{
    switch (pora_roku())
    {
         case MT_LATO:
         case MT_WIOSNA:
             return ({"Z lasu wychyli^la si^e smuk^la sarna, jednak "+
             "sp^loszona twoj^a obecno^sci^a zaszy^la si^e w g^estwinie.\n",
             "Dobiega do ciebie cichy, spokojny szum drzew.\n",
             "Li^scie w koronach drzew, zaszumia^ly na wietrze. \n",
             "Ciep^ly wiatr muska twoj^a twarz.\n",
             "Podmuch wiatru w koronach drzew, zrzuci^l na "+
             "twoj^a g^low^e kilka li^sci i igie^l. \n",
             "Z jedej z ga^l^ezi, posypa^ly si^e li^scie. \n"})[random(6)];
         case MT_JESIEN:
             return ({"Ch^l^od i wilgo^c przyprawia ci^e o dreszcze.\n",
             "Korony drzew zaszumia^ly pod mu^sni^eciem wiatru.\n"})
             [random(2)];
         case MT_ZIMA:
             return ({"Mr^oz szczypie ci^e w policzki.\n",
             "Z twoich ust unosz^a si^e ob^loczki pary.\n"})[random(2)];
    }
}
string
pora_roku_noc()
{
    switch (pora_roku())
    {
         case MT_LATO:
         case MT_WIOSNA:
             return ({"Dobiega do ciebie cichy, spokojny szum drzew.\n",
             "Ciep^ly wiatr muska twoj^a twarz.\n",
             "Li^scie w koronach drzew, zaszumia^ly na wietrze. \n",
             "Podmuch wiatru w koronach drzew, zrzuci^l na twoj^a "+
             "g^low^e kilka li^sci i igie^l. \n",
             "Z jedej z ga^l^ezi, posypa^ly si^e li^scie. \n"})[random(5)];
         case MT_JESIEN:
             return ({"Ch^l^od i wilgo^c przyprawia ci^e o dreszcze.\n",
             "Korony drzew zaszumia^ly pod mu^sni^eciem wiatru.\n"})
             [random(2)];
         case MT_ZIMA:
             return ({"Mr^oz szczypie ci^e w policzki.\n",
             "Z twoich ust unosz^a si^e ob^loczki pary.\n"})[random(2)];
    }
}

string
dlugi_opis() 
{
}

string
opis_nocy()
{
}

int
filter_trees(object ob)
{
    if (function_exists("create_object", ob) == "/std/drzewo")
        return 1;
    return 0;
}

void odnow_drzewa()
{
    ::odnow_drzewa();
}