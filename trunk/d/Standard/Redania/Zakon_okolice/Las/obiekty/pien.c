/* Autor: Avard 
   Data : 14.02.2009
   Opis : Samaia */

inherit "/std/object.c";

#include <pl.h>
#include <stdproperties.h>
#include <macros.h>
#include <materialy.h>
#include <object_types.h>
#include <filter_funs.h>
#include <mudtime.h>
#include <language.h>
#include "dir.h"

int wejdz(string str);
int wyciagnij(string str);
string kto_jest();
object *kto;

void
create_object()
{
    ustaw_nazwe("konar");
    dodaj_nazwy("pie�");
    dodaj_przym("pot^e^zny","pot^e^zni");

    set_long("Pot^e^zny konar, zwalony zapewne za spraw^a okrutnego czasu, "+
        "zdaje si^e by^c zupe^lnie pusty w ^srodku, co mo^zna wywnioskowa^c "+
        "po wychodz^acych ze^n insekt^ow. Z jednej strony, b^ed^acej "+
        "dawniej podstaw^a silnego drzewa, znajduje si^e dziura, ci^agn^aca "+
        "wg^l^ab pnia i gin^aca w mroku. Wygl^ada to tak, jakby da^lo si^e "+
        "tam w^slizgn^a^c. @@kto_jest@@");

    add_prop(OBJ_I_WEIGHT, 100000);
    add_prop(OBJ_I_VOLUME, 100000);
    add_prop(OBJ_I_VALUE, 0);
    add_prop(OBJ_I_NO_GET, "To ci si^e chyba nie uda.\n");
    add_prop(OBJ_M_NO_SELL, 1);
    ustaw_material(MATERIALY_DR_DAB);
    set_type(O_INNE);
    make_me_sitable(({"przy","pod"}),"przy konarze","spod konaru", 10);
    make_me_sitable("na","na konarze","z konaru",10);
}

void
init()
{
     ::init();
     add_action(wejdz, "wejd^x");
     add_action(wejdz, "w^slizgnij");
     add_action(wyciagnij, "wyci^agnij");
}

string
kto_jest()
{
    string str = "";
    kto = FILTER_LIVE(AI(find_object(LAS_ZAKON_OKOLICE_LOKACJE+"w_konarze")));
    if(sizeof(kto)>0)
    {
        str += "W ^srodku zdaje si^e by^c "+COMPOSITE_LIVE(kto, PL_MIA)+".";
    }
    str += "\n";
    return str;
}
int
wejdz(string str)
{
    object konar;
    object *ile;
    ile = FILTER_LIVE(AI(find_object(LAS_ZAKON_OKOLICE_LOKACJE+"w_konarze")));

    if(query_verb() ~= "wejd�")
        notify_fail("Wejd^x gdzie?\n");
    else
        notify_fail("W^slizgnij si^e gdzie?\n");

    if (!str) return 0;
    if (!strlen(str)) return 0;

    if(query_verb() ~= "wejd�")
    {
        if(!parse_command(str,environment(TP),"'do' %o:" + PL_DOP, konar))
        return 0;
    }
    else if(query_verb() ~= "w�lizgnij")
    {
        if(!parse_command(str,environment(TP),"'si^e' 'do' %o:"+PL_DOP, konar))
        return 0;
    }

    if(sizeof(ile) > 0)
    {
        write("Nie zmie^scisz si^e, kto^s ju^z tam jest.\n");
        return 1;
    }
    if(TP->query_prop(CONT_I_VOLUME) > 200000)
    {
        write("Nie zmie^scisz si^e tam.\n");
        return 1;
    }
write(file_name(TP)+"\n");
    write("Kl^ekasz przed otworem w starym zwalonym konarze, po "+
        "czym wchodzisz do ^srodka.\n");
write(file_name(TP)+"\n");
    saybb(QCIMIE(TP, PL_MIA)+" kl^eka przed otworem w starym "+
        "zwalonym konarze, po czym znika w jego ciemnym wn^etrzu.\n");
    TP->move_living("M",LAS_ZAKON_OKOLICE_LOKACJE+"w_konarze");
    return 1;
}

int
wyciagnij(string str)
{
    object konar;
    kto = FILTER_LIVE(AI(find_object(LAS_ZAKON_OKOLICE_LOKACJE+"w_konarze")));

    if(query_verb() ~= "wyci�gnij")
        notify_fail("Wyci^agnij kogo sk�d?\n");

    if (!str) return 0;
    if (!strlen(str)) return 0;

    if(!parse_command(str,kto,"%l:" + PL_BIE + " 'z' 'konaru'"))
        return 0;

    if(sizeof(kto) == 0)
    {
        write("Nikogo tam nie ma.\n");
        return 1;
    }
    if(!HAS_FREE_HANDS(TP))
    {
       write("Musisz mie^c obie d^lonie wolne, aby m^oc to zrobi^c.\n");
       return 1;
    }

    /* Tu pokrecone wszystko, mozna wyciagac przy pomocy imienia nie znajac 
    go, wyswietla imie jak nie znasz i w ogole. */
//     TP->catch_msg("Wsadzasz r^ece do konaru, ^lapiesz " + QIMIE(kto,PL_BIE)+
//     " za nogi i wyci^agasz "+koncowka("go", "j^a")+" na zewn^atrz.\n");
//  FIXME: Tu jest b��d: QIMIE, odnosi si� jedynie do pojedynczego obiektu, a ty tu chcesz tego u�y�
//         w stosunku do tablicy obiekt�w. Proponuje przejrzenie tego, bo nie wiem czy mo�esz �apa�
//         kilka os�b na raz za nogi, a je�li nie no to jako� trzeba wybra� t� osob� z tablicy wszystkich
//         os�b. Chyba nawet wiem o co ci chodzi - przeczytaj sobie manual do parse_command, zw�aszcza
//         jak wyci�ga� z niego obiekty.

    saybb(QCIMIE(TP, PL_MIA)+" wk^lada r^ece do konaru i zdecydowanym ruchem "+
        "wyci^aga z niego "+COMPOSITE_LIVE(kto,PL_BIE)+".\n");
    kto->catch_msg("Kto� �apie ci� za nogi i gwa�townym szarpni�ciem wyci�ga "+
        "na zewn�trz.\n");
    kto->move_living("M",LOKACJA_Z_KONAREM);
    return 1;
}