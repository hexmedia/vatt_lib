#include "../dir.h"
#define JASKINIA (ZAKON_OKOLICE+"Jaskinia/")
#define JASKINIA_LOKACJE (JASKINIA+"lokacje/")
#define TRAKT_ZAKON_OKOLICE_STD (TRAKT_ZAKON_OKOLICE + "std/trakt.c")
#define LAS_ZAKON_OKOLICE_STD (LAS_ZAKON_OKOLICE+"std/std_jezioro_las.c")

#define JEZIORKO_ZAKON					(ZAKON_OKOLICE+"Jeziorko/")
#define JEZIORKO_ZAKON_LOKACJE			(JEZIORKO_ZAKON+"lokacje/")
#define JEZIORKO_ZAKON_OBIEKTY          (JEZIORKO_ZAKON+"obiekty/")
#define JEZIORKO_ZAKON_LIVINGI          (JEZIORKO_ZAKON+"livingi/")
#define JEZIORKO_ZAKON_STD				(JEZIORKO_ZAKON+"std/")
#define JEZIORKO_ZAKON_BRZEG_STD		(JEZIORKO_ZAKON_STD+"jeziorko_brzeg.c")
#define JEZIORKO_ZAKON_SRODEK_STD		(JEZIORKO_ZAKON_STD+"jeziorko_srodek.c")

#ifdef SLEEP_PLACE
#undef SLEEP_PLACE
#endif SLEEP_PLACE

#define SLEEP_PLACE                     (RINDE + "Polnoc/Karczma/lokacje/start.c")


/* Zmeczenie dla traktu */
#define TRAKT_ZO_FATIG          4
#define SCIEZKA_ZO_FATIG        5

/* Zmeczenie dla lasu */
#define LAS_FATIGUE             7