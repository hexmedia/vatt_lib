
#include <sit.h>
string
evencik()
{
	string str="";
	switch(pora_roku())
	{
		case MT_LATO:
		case MT_WIOSNA:
		case MT_JESIEN:
			switch(random(18))
			{
				case 0: str="Jaki� drobny py�ek wpad� ci do oka.\n";
						break;
				case 1: str="Unosz�cy si� dooko�a piach jest do�� uci��liwy.\n";
						break;
				case 2: str="Gdzie� nad tob� przelecia� jaki� ptak, g�o�no "+
						"informuj�c o swojej obecno�ci.\n";
						break;
				case 3: str="W powietrzu unosi si� przyjemny zapach "+
						"kwiat�w dochodz�cy od strony ��ki.\n";
						break;
				case 4: str="Ta�cz�cy dooko�a wiatr, szumi cicho pomi�dzy trawami.\n";
						break;
				case 5: str="Drobny py�ek wpad� ci do oka wywo�uj�c "+
						"nieprzyjemne pieczenie.\n"; break;
				case 6: str="Unosz�cy si� dooko�a kurz jest do�� uci��liwy.\n";
						break;
				case 7: str="Trawy szumi� cicho.\n"; break;
				case 8: str="Z daleka dotar� do twych uszu ptasi trel.\n"; break;
				case 9: str="Nad twoj� g�ow� przelecia�o stado g�si.\n"; break;
				case 10: str="Trawa leniwie zafalowa�a pod wp�ywem wiatru.\n";
						break;
				case 11: str="Co� zapiszcza�o w trawie.\n"; break;
				case 12:
					if(jest_dzien() && random(2))
						str="Stado wr�bli poderwa�o si� nagle do lotu.\n";
					else if(jest_dzien())
						str="�agodny ptasi trel rozleg� si� gdzie� w pobli�u.\n";
					else
						str="Gdzie� w oddali s�ycha� wycie wilka.\n";
					break;
				case 13:
					str="Od strony lasu s�ycha� ciche pohukiwanie.\n";
				case 14:
					if(jest_dzien())
					{
						if(random(2))
							str="Przez drog� przemkn�a mysz.\n";
						else
							str="Nagle przez drog� przebieg� lis.\n";
					}
					else
						str="Przez drog� szybko przebieg�o jakie� zwierz�.\n";
					break;
				case 15:
					//str="Potykasz"+/*"Potkn"+TP->koncowka("��e�","�a�")+*/
					//" si� o jaki� konar, z trudem utrzymuj�c r�wnowag�.\n";
					break;
				case 16:
					str="Przez pole przebiega stado saren kieruj�c si� w "+
					"stron� lasu.\n"; break;
				case 17:
					str="W uszach s�yszysz pi�kny trel, kt�rego� z ptak�w.\n";
					break;
				case 18:
					if(jest_dzien())
					str="Wiewi�rki skacz� po ga��ziach nad twoj� g�ow�.\n";
					else
					str="Gdzie� nieopodal rozleg�o si� wilcze wycie.\n";
					break;
			}
			break;
		case MT_ZIMA:
			switch(random(12))
			{
				case 0: str="Gdzie� nad tob� przelecia� jaki� ptak, g�o�no "+
						"informuj�c o swojej obecno�ci.\n";
						break;
				case 1: str="Ta�cz�cy dooko�a wiatr, szumi cicho pomi�dzy trawami.\n";
						break;
				case 2: str="Trawy szumi� cicho.\n"; break;
				case 3: str="Co� zaszele�ci�o w trawie.\n"; break;
				case 4:
					//po�lizg randomowego livinga
					object *liv = FILTER_LIVE(all_inventory(TO));

					if(!sizeof(liv))
						return "";

					object delikwent = liv[random(sizeof(liv))];

					if(delikwent->query_prop(SIT_SIEDZACY))
					{
					}
					else
					{
					set_this_player(delikwent);
					write("Po�lizg na oblodzonej "+
					"powierzchni drogi, nieomal spowodowa� tw�j upadek!\n");
					 saybb(QCIMIE(TP,PL_MIA)+" po�lizgn"+
					TP->koncowka("��","�a","�o")+" si� na oblodzonej "+
					"powierzchni drogi i nieomal upad�"+
					TP->koncowka("","a","o")+" na ziemi�.\n");
					}
					break;
				case 5:
					if(CZY_JEST_SNIEG(TO))
						str="Ciemny �wistak stan�� s�upka na �niegu "+
						"by zaraz da� nurka w bia�y puch.\n";
					break;
				case 6:
					if(jest_dzien())
						str="Stado wr�bli poderwa�o si� nagle do lotu.\n";
					else
						str="Gdzie� w oddali s�ycha� wycie wilka.\n";
					break;
				case 7:
					str="Od strony lasu s�ycha� ciche pohukiwanie.\n";
					break;
				case 8:
					if(jest_dzien())
						str="Nagle przez drog� przebieg� lis.\n";
					else
						str="Przez drog� szybko przebieg�o jakie� zwierz�.\n";
					break;
// 				case 9:
// 					str="Potkn"+TP->koncowka("��e�","�a�")+
// 					" si� o jaki� konar, z trudem utrzymuj�c r�wnowag�.\n";
// 					break;

				case 10..11: str=""; break;
			}

	}
	return str;
}
