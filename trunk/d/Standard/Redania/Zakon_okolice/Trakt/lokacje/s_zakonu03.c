/* Autor: Vera
   Opis : Praca zbiorowa ludzi z forum Zakonu, g��wnie Sniegulak
   Data : Tuesday 01 May 2007 */ 
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"
#include <pogoda.h>

inherit TRAKT_ZAKON_OKOLICE_STD;
#include "../std/eventy_sciezki.c"
void create_trakt() 
{
    set_short("�cie�ka");
    add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "s_zakonu02.c","sw",0,SCIEZKA_ZO_FATIG,0);
	add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "s_zakonu04.c","n",0,SCIEZKA_ZO_FATIG,0);
    add_prop(ROOM_I_INSIDE,0);
}
public string
exits_description() 
{
    return "�cie�ka ci�gnie si� z po�udniowego zachodu na p�noc.\n";
}


string
dlugi_opis()
{
    string str = "";

    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="Drzewa i okolica spowite s� bia�ym puchem, a mocny mr�z p"+
             "owoduje, ze �nieg skrzypi pod butami. Wydeptana pomi�dzy d"+
             "rzewami �cie�ka jest bardzo kr�ta i w�ska. Drzewa rosn�"+
             "ce na p�nocy pn� si� w g�r� tworz�c jakby naturalny "+
             "p�ot, broni�cy las przed intruzami, jednocze�nie drzewa t"+
             "e, dzi�ki roz�o�ystym koronom daj� spore zacienienie prz"+
             "ez co w�dr�wka t� �cie�yn� nie wydaje si� by� a� ta"+
             "k m�cz�ca. ";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str+="Wszystko dooko�a jest przyjemnie zazielenione, a cisz� pan"+
             "uj�c� w lesie od czasu do czasu przerywa jaki� �wiergot "+
             "ptaka. Wydeptana pomi�dzy drzewami �cie�ka jest bardzo kr"+
             "�ta i w�ska. Drzewa rosn�ce na p�nocy pn� si� w g�r"+
             "� tworz�c jakby naturalny p�ot, broni�cy las przed intru"+
             "zami, jednocze�nie drzewa te, dzi�ki roz�o�ystym koronom"+
             " daj� spore zacienienie przez co w�dr�wka t� �cie�yn�"+
             " nie wydaje si� by� a� tak m�cz�ca. ";
    }
    else if(pora_roku() == MT_LATO)
    {
        str+="Wszystko dooko�a jest przyjemnie zazielenione, a cisz� pan"+
             "uj�c� w lesie od czasu do czasu przerywa jaki� �wiergot "+
             "ptaka. Wydeptana pomi�dzy drzewami �cie�ka jest bardzo kr"+
             "�ta i w�ska. Drzewa rosn�ce na p�nocy pn� si� w g�r"+
             "� tworz�c jakby naturalny p�ot, broni�cy las przed intru"+
             "zami, jednocze�nie drzewa te, dzi�ki roz�o�ystym koronom"+
             " daj� spore zacienienie przez co w�dr�wka t� �cie�yn�"+
             " nie wydaje si� by� a� tak m�cz�ca. ";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str+="Suche opadni�te na ziemi� li�cie szeleszcz� pod krokami,"+
             " zwierz�ta szykuj� si� do zimowego snu. Wydeptana pomi�d"+
             "zy drzewami �cie�ka jest bardzo kr�ta i w�ska. Drzewa ro"+
             "sn�ce na p�nocy pn� si� w g�r� tworz�c jakby natura"+
             "lny p�ot, broni�cy las przed intruzami, jednocze�nie drze"+
             "wa te, dzi�ki roz�o�ystym koronom daj� spore zacienienie"+
             " przez co w�dr�wka t� �cie�yn� nie wydaje si� by� a�"+
             " tak m�cz�ca. ";
    }

    str+="\n";
    return str;
}
string
opis_polmroku()
{
    string str;
	//Noo, po traktach si� chodzi z lampami!
	//I to o ka�dej porze roku! Nie wiedzieli�cie? :)

    if(CZY_JEST_SNIEG(this_object()))
        str="W mroku jeste� w stanie dostrzec jedynie biel �niegu "+
            "le��cego na �cie�ce.\n";
    else
        str="Nad �cie�k� zapad� mrok, dlatego nie jeste� w stanie dostrzec "+
            "zbyt wiele.\n";

    return str;
}
string
opis_nocy()
{
    string str="";

     str +="\n";
     return str;
}