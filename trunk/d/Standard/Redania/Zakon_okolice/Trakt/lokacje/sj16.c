/* Autor: Vera
   Opis : Praca zbiorowa ludzi z forum Zakonu, g��wnie Sniegulak
   Data : Tuesday 01 May 2007 */ 
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"
#include <pogoda.h>

inherit TRAKT_ZAKON_OKOLICE_STD;
#include "../std/eventy_sciezki.c"
void create_trakt() 
{
    set_short("przy jeziorze");
    add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "sj15.c","se",0,SCIEZKA_ZO_FATIG,0);
	add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "sj17.c","nw",0,SCIEZKA_ZO_FATIG,0);
    add_exit(LAS_ZAKON_OKOLICE_LOKACJE + "jezioro_las20.c","e",0,LAS_FATIGUE,0);
    add_exit(LAS_ZAKON_OKOLICE_LOKACJE + "jezioro_las27.c","s",0,LAS_FATIGUE,0);
    add_exit(LAS_ZAKON_OKOLICE_LOKACJE + "jezioro_las19.c","w",0,LAS_FATIGUE,0);
    add_prop(ROOM_I_INSIDE,0);
    add_item("jezioro","@@jezioro@@");
}
public string
exits_description() 
{
    return "�cie�yna ci�gnie si� z p�nocnego zachodu na po�udniowy wsch�d.\n";
}

string
dlugi_opis()
{
    string str = "";

    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="Na po�udniu rozci�gaj� si� niezmierzone po�acie p�l, k"+
             "t�re przykryte bia�ym �niegiem wygl�daj� jak tafla ogro"+
             "mnego jeziora, na kt�rym panuje ca�kowita cisza i spok�j."+
             " "+
			"Na p�nocy, gdy podniesiemy wzrok ponad martwe i zmarz�e "+
			"konary i krzewy dostrze�emy oblodzon� tafl� rozleg�ego "+
			"jeziora, za� na po�udniu swe miejsce znalaz�y g�sto rosn�ce "+
			"drzewa, teraz przykryte �niegiem. "+
			"Oblodzona �cie�ka wije si� falist� wst�g� ze wschodu "+
             "na zach�d, a gdzie nie spojrze� dooko�a zalega bia�y puc"+
             "h. Zwarta �ciana drzew li�ciasto iglastych przeszkadza pod"+
             "r�nemu w zboczeniu ze �cie�ki, a masywne i g�ste korony"+
             " przys�aniaj� widok nieba. G�sto rosn�ce praktycznie ze "+
             "wszystkich stron wysokie drzewa przejmuj� groz�, a powykr�"+
             "cane krzewy i pnie dodaj� na niesamowito�ci otoczenia. ";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str+="Na po�udniu rozci�gaj� si� niezmierzone po�acie p�l, k"+
             "t�re b�d�c pi�knie zazielenione, s� prawdziwym rajem dl"+
             "a okolicznych ptak�w i zwierz�t, kt�rych piski, skrzeki i"+
             " mruczenia s�ycha� nawet z takiej odleg�o�ci. "+
			"Na p�nocy, za rzadk� �cian� ro�lin i krzew�w rozpo�ciera "+
			"si� widok na b��kitn� to� jeziora, za� na po�udniu okazale "+
			"prezentuje si� zielona �ciana mieszanych drzew. "+
			"Le�na �c"+
             "ie�ka wije si� falist� wst�g� ze wschodu na zach�d, ot"+
             "oczona zewsz�d natur�. Zwarta �ciana drzew li�ciasto igl"+
             "astych przeszkadza podr�nemu w zboczeniu ze �cie�ki, a "+
             "masywne i g�ste korony przys�aniaj� widok nieba. G�sto ro"+
             "sn�ce praktycznie ze wszystkich stron wysokie drzewa przejm"+
             "uj� groz�, a powykr�cane krzewy i pnie dodaj� na niesamo"+
             "wito�ci otoczenia. ";
    }
    else if(pora_roku() == MT_LATO)
    {
        str+="Na po�udniu rozci�gaj� si� niezmierzone po�acie p�l, k"+
             "t�re b�d�c pi�knie zazielenione, s� prawdziwym rajem dl"+
             "a okolicznych ptak�w i zwierz�t, kt�rych piski, skrzeki i"+
             " mruczenia s�ycha� nawet z takiej odleg�o�ci. "+
			"Na p�nocy, za rzadk� �cian� ro�lin i krzew�w rozpo�ciera "+
			"si� widok na b��kitn� to� jeziora, za� na po�udniu okazale "+
			"prezentuje si� zielona �ciana mieszanych drzew. "+
			"Le�na �c"+
             "ie�ka wije si� falist� wst�g� ze wschodu na zach�d, ot"+
             "oczona zewsz�d natur�. Zwarta �ciana drzew li�ciasto igl"+
             "astych przeszkadza podr�nemu w zboczeniu ze �cie�ki, a "+
             "masywne i g�ste korony przys�aniaj� widok nieba. G�sto ro"+
             "sn�ce praktycznie ze wszystkich stron wysokie drzewa przejm"+
             "uj� groz�, a powykr�cane krzewy i pnie dodaj� na niesamo"+
             "wito�ci otoczenia. ";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str+="Na po�udniu rozci�gaj� si� niezmierzone po�acie p�l, n"+
             "ad kt�rymi snuje si� sie lekka mg�a nadaj�ca im do�� n"+
             "iepokoj�cy wygl�d, a z otaczaj�cych ci� drzew powoli spa"+
             "daj� r�nokolorowe li�cie, kt�re po swobodnym locie, de"+
             "likatnie opadaj� na ziemi�. "+
			"Na p�nocy, za rzadk� �cian� ro�lin i krzew�w rozpo�ciera "+
			"si� widok na b��kitn� to� jeziora, za� na po�udniu okazale "+
			"prezentuje si� zielona �ciana mieszanych drzew. "+
			"Le�na �cie�ka wije si� fal"+
             "ist� wst�g� ze wschodu na zach�d, otoczona zewsz�d natu"+
             "r�. Zwarta �ciana drzew li�ciasto iglastych przeszkadza p"+
             "odr�nemu w zboczeniu ze �cie�ki, a masywne i g�ste koro"+
             "ny przys�aniaj� widok nieba. G�sto rosn�ce praktycznie z"+
             "e wszystkich stron wysokie drzewa przejmuj� groz�, a powyk"+
             "r�cane krzewy i pnie dodaj� na niesamowito�ci otoczenia. ";
    }

    str+="\n";
    return str;
}
string
opis_polmroku()
{
    string str;
	//Noo, po traktach si� chodzi z lampami!
	//I to o ka�dej porze roku! Nie wiedzieli�cie? :)

    if(CZY_JEST_SNIEG(this_object()))
        str="W mroku jeste� w stanie dostrzec jedynie biel �niegu "+
            "le��cego na �cie�ce.\n";
    else
        str="Nad �cie�k� zapad� mrok, dlatego nie jeste� w stanie dostrzec "+
            "zbyt wiele.\n";

    return str;
}
string
opis_nocy()
{
    string str="";

     str +="\n";
     return str;
}

string
jezioro()
{
    string str;
    str = "Rozci^agaj^aca si^e na p^o^lnoc stalowoszara to^n jeziora "+
        "zdaje si^e by^c idealnie g^ladka i niezm^acona, jednak co^s raz "+
        "po raz narusza idealn^a harmoni^e, burz^ac g^ladk^a powierzchni^e "+
        "wody. Mimo to otaczaj^ace je drzewa przegl^adaj^a si^e we^n niczym "+
        "w czystym zwierciadle, zanurzaj^ac nieliczne ga^lezie, czy "+
        "korzenie w jej topieli. ";
    if(pora_roku() == MT_ZIMA)
    {
        str += "Ca^ly brzeg znieruchomia^l ca^lkowicie, pokrywaj^ac si^e "+
            "cienk^a wartw^a jasnob^l^ekitnego lodu, za^s po powierzchni "+
            "wody p^lywaj^a drobne od^lamki kry. ";
    }
    else
    {
        str += "Trzciny porastaj^ace brzeg poruszaj^a si^e lekko, "+
            "potrz^asaj^ac delikatnie wiechami, a tatarak kiwa monotonnie "+
            "w rytm ^swiszcz^acego wiatru. ";
    }
    str += "\n";
    return str;
}
