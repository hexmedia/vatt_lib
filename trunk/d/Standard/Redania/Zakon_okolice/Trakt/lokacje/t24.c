/* Autor: Vera
   Opis : Praca zbiorowa ludzi z forum Zakonu, g��wnie Sniegulak
   Data : Tuesday 01 May 2007 */ 
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"
#include <pogoda.h>

inherit TRAKT_ZAKON_OKOLICE_STD;

void create_trakt() 
{
    set_short("Na polnym trakcie");
    add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "t25.c","ne",0,TRAKT_ZO_FATIG,0);
    add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "t23.c","s",0,TRAKT_ZO_FATIG,0);
    add_prop(ROOM_I_INSIDE,0);
}

public string
exits_description() 
{
    return "Trakt prowadzi z po�udnia na p�nocny wsch�d.\n";
}

string
dlugi_opis()
{
    string str="";
	str+="Wok� jak okiem si�gn�� rozpo�ciera sie pofa�dowany "+
	"�agodnymi wzg�rzami krajobraz. Droga wychodz�ca z lasu na po�udniu "+
	"i ��cz�ca miasto widoczne na p�nocy wije si� miedzy nimi, a jej "+
	"powierzchnia usiana jest koleinami - wida� trakt jest mocno "+
	"ucz�szczany i ludzie nie nad��aj� z jego napraw�.";
	if(pora_roku() == MT_WIOSNA)
		str+="Otaczaj�ca ci� przyroda budzi si� do �ycia po zimowym "+
		"�nie, a lekki wietrzyk przynosi do ciebie �wie�y zapach ro�lin.";
	else if(pora_roku() == MT_LATO)
		str+="Ca�a przyroda w pe�nym rozkwicie, zieleni otaczaj�cy ci� "+
		"krajobraz, a w powietrzu mo�esz wyczu� intensywny zapach ro�lin.";
	else if(pora_roku() == MT_JESIEN)
		str+="Krajobraz spowija lekka mgie�ka i dymy z ognisk "+
		"palonych na polach.";
	else if(CZY_JEST_SNIEG(TO))
		str+="Prawie wszystko przykryte jest spor� warstw� bielutkiego "+
		"�niegu, kt�ry przysypa� okolice.";

	str+=" Na p�nocy majacz� wysokie mury otaczaj�ce miasto, z kt�rego "+
	"bij� w g�r� wysokie wie�e.";

    str += "\n";
    return str;
}
string
opis_nocy()
{
    string str="";

     str +="\n";
     return str;
}
