/* Autor: Vera
   Opis : Praca zbiorowa ludzi z forum Zakonu, g��wnie Sniegulak
   Data : Tuesday 01 May 2007 */ 
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"
#include <pogoda.h>

inherit TRAKT_ZAKON_OKOLICE_STD;
#include "../std/eventy_sciezki.c"
void create_trakt() 
{
    set_short("przy rozpadlinie");
    add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "sj17.c","e",0,SCIEZKA_ZO_FATIG,0);
	//add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "sj19.c","w",0,SCIEZKA_ZO_FATIG,0);
//KONIEC!!!!!!!!
    add_prop(ROOM_I_INSIDE,0);


	add_item("most",
	"Ma�y lecz d�ugi most kiedy� ��cz�cy dwie strony rozpadliny w "+
	"obecnej chwili jest roztrzaskany na drobne kawa�ki. Patrz�c "+
	"na jego stan mo�na wywnioskowa�, �e powodem tej sytuacji by�o "+
	"zapewne jakie� drzewo, kt�re wyrwane z korzeniami przez "+
	"wichur� spad�o na mostek.\n");
	add_item("rozpadlin�",
	"Szeroka i g��boka rozpadlina musia�a powsta� po jakim� "+
	"zapadni�ciu terenu, tudzie� po jakim� trz�sieniu ziemi. "+
	"Kiedy� prze�o�ony przez rozpadlin� mostek, obecnie jest "+
	"roztrzaskany. Nie widzisz �adnej mo�liwo�ci na przedostanie "+
	"si� na drug� stron�.\n");



}
public string
exits_description() 
{
	return "�cie�yna ci�gnie si� na wsch�d.\n";
    //return "�cie�yna ci�gnie si� z zachodu na wsch�d.\n";
}

string
dlugi_opis()
{
    string str = "";

/*    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="Natura u�piona w czasie zimy, jest cicha i spokojna, nie s�"+
             "ycha� tutaj prawie �adnych d�wi�k�w, a prawie wszystko"+
             " przykryte jest grub� warstwa czystego �niegu. Wszystko wo"+
             "k� wprost rozbrzmiewa odg�osami przyrody - szumem wody"+
             ", �piewem ptak�w, czy szelestem ro�lin. Zwarta �ciana dr"+
             "zew li�ciasto iglastych przeszkadza podr�nemu w zboczeni"+
             "u ze �cie�ki, a masywne i g�ste korony przys�aniaj� wido"+
             "k nieba. W�ska wydeptana ludzkimi stopami �cie�ka prowadz"+
             "�ca przez las wije si� mi�dzy drzewami. ";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str+="W powietrzu unosz� sie stada ma�ych muszek, ptaki �piewaj"+
             "� swoje piosenki, a flora rozkwita na ka�dym kroku. Wszyst"+
             "ko wok� wprost rozbrzmiewa odg�osami przyrody - szumem"+
             " wody, �piewem ptak�w, czy szelestem ro�lin. Zwarta �cia"+
             "na drzew li�ciasto iglastych przeszkadza podr�nemu w zbo"+
             "czeniu ze �cie�ki, a masywne i g�ste korony przys�aniaj�"+
             " widok nieba. W�ska wydeptana ludzkimi stopami �cie�ka pr"+
             "owadz�ca przez las wije si� mi�dzy drzewami. ";
    }
    else if(pora_roku() == MT_LATO)
    {
        str+="W powietrzu unosz� sie stada ma�ych muszek, ptaki �piewaj"+
             "� swoje piosenki, a flora rozkwita na ka�dym kroku. Wszyst"+
             "ko wok� wprost rozbrzmiewa odg�osami przyrody - szumem"+
             " wody, �piewem ptak�w, czy szelestem ro�lin. Zwarta �cia"+
             "na drzew li�ciasto iglastych przeszkadza podr�nemu w zbo"+
             "czeniu ze �cie�ki, a masywne i g�ste korony przys�aniaj�"+
             " widok nieba. W�ska wydeptana ludzkimi stopami �cie�ka pr"+
             "owadz�ca przez las wije si� mi�dzy drzewami. ";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str+="Pod stopami s�yszysz chrz�st patyk�w i szum suchych opadn"+
             "i�tych li�ci. Wszystko wok� wprost rozbrzmiewa odg�osa"+
             "mi przyrody - szumem wody, �piewem ptak�w, czy szelestem"+
             " ro�lin. Zwarta �ciana drzew li�ciasto iglastych przeszka"+
             "dza podr�nemu w zboczeniu ze �cie�ki, a masywne i g�ste"+
             " korony przys�aniaj� widok nieba. W�ska wydeptana ludzkim"+
             "i stopami �cie�ka prowadz�ca przez las wije si� mi�dzy d"+
             "rzewami. ";
    }
*/

	str+="W�ska �cie�ka urywa si� w tym miejscu przy g��bokiej "+
	"rozpadlinie. Jedyny spos�b na przej�cie na drug� stron� to "+
	"mostek. Niestety jakie� drzewo wyrwane z korzeniami, przez "+
	"pot�n� wichur�, musia�o go uszkodzi� i teraz nie jest zdatny "+
	"do u�ytku. Drzewa i krzewy otaczaj� ci� zewsz�d zas�aniaj�c "+
	"niebo i ca�y horyzont. ";

	if(CZY_JEST_SNIEG(TO))
		str+="Ca�a okolica spowita jest bia�ym puchem, a mocny "+
		"mr�z powoduje, ze �nieg skrzypi pod butami.";
	else if(pora_roku() == MT_WIOSNA || pora_roku()==MT_LATO)
		str+="Wszystko dooko�a jest przyjemnie zazielenione, a "+
		"cisz� panuj�c� w lesie od czasu do czasu przerywa �wiergot "+
		"jakiego� ptaka.";
	else
		str+="Suche opadni�te na ziemi� li�cie szeleszcz� pod "+
		"krokami, a zwierz�ta szykuj� si� do zimowego snu.";

    str+="\n";
    return str;
}
string
opis_polmroku()
{
    string str;
	//Noo, po traktach si� chodzi z lampami!
	//I to o ka�dej porze roku! Nie wiedzieli�cie? :)

    if(CZY_JEST_SNIEG(this_object()))
        str="W mroku jeste� w stanie dostrzec jedynie biel �niegu "+
            "le��cego na �cie�ce.\n";
    else
        str="Nad �cie�k� zapad� mrok, dlatego nie jeste� w stanie dostrzec "+
            "zbyt wiele.\n";

    return str;
}
string
opis_nocy()
{
    string str="";

     str +="\n";
     return str;
}