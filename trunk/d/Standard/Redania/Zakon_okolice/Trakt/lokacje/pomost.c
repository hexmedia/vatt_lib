/* Autor: Vera.
   Taki sobie pomost z ��dkami. :)
   Opis : Praca zbiorowa ludzi z forum Zakonu, g��wnie Sniegulak
   Data : Tuesday 13 May 2007 */ 
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <sit.h>
#include "dir.h"
#include <pogoda.h>
#include <mudtime.h>

inherit "/std/room.c";
inherit "/std/ryby/lowisko";
inherit "/std/woda/kapielisko";

string opis_jeziorka();
string event_od_pory_roku();

//tylko ��dkami mo�na 'i��' na p�noc :)
int
czy_mozna()
{
    if(ENV(TP)->query_lodka_in())
        return 0;
    else
        return 11;
}

void create_room() 
{
    set_short("na pomo�cie");
    add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "sj17.c","sw",0,10,0);
    add_prop(ROOM_I_INSIDE,0);
    add_prop(ROOM_I_TYPE,ROOM_BEACH);
    add_event(&event_od_pory_roku());
    add_event("Spory, drapie^zny ptak zatacza ko^lo nad jeziorem, by po chwili "+
        "skierowa^c si^e nad las.\n");
    add_event("Lekki wietrzyk zawia� od strony jeziora.\n");
    if(pora_roku() != MT_ZIMA)
    {
        add_event("Spod pomostu dohodzi rechot ^zab.\n");
        add_event("W powietrzu unosi si^e zapach czego^s zbutwia^lego.\n");
        add_event("Co� plusn�o w wodzie.\n");
    }
    add_sit("na pomo�cie","na pomo�cie","z pomostu",0);
    
    //tajne jeziorko :)
    add_item(({"jeziorko","jezioro","wod�"}),
            "@@opis_jeziorka@@");
            
            
    add_exit(JEZIORKO_ZAKON_LOKACJE+"jb01.c","n","@@czy_mozna@@",5,1);

    dodaj_lowisko("w jeziorze",
                        (["ploc":       1.4,
                          "ukleja":     1.4,
                          "okon":       1.3,
                          "szczupak":   0.4, //przy brzegu rzadziej szczupak. troszk�.
                          "galaz":      1.7, //przy brzegu cz�ciej zielska i ga��zie
                          "zielsko":    1.5 ,
                          "leszcz":     0.1, //tylko w g��bokiej wodzie
                         "jazgarz":     0.1, //tylko w g��bokiej wodzie
                          //"sum":        0.7, //tylko w g��bokiej wodzie
                          "mietus":     0.1, //tylko w g��bokiej wodzie
                          "obunog_ryba":0.1  //tylko w g��bokiej wodzie 
                          ]));
                         
     ustaw_kapielisko(JEZIORKO_ZAKON_LOKACJE+"jb01.c");
     set_cumowanie("do pomostu");
     set_exits_woda_description("Jezioro rozci�ga si� na p�nocy.\n");
     
     if(pora_roku() != MT_ZIMA)
        add_object(JEZIORKO_ZAKON_OBIEKTY + "baczek.c", 3);
     
     dodaj_rzecz_niewyswietlana("ma�a drewniana ��dka", 3); //trzy ��dki, wi�cej nie :P
     
}
public string
exits_description() 
{
    return "�cie�yna ci�gnie si� gdzie� na po�udniowym-zachodzie.\n";
}


string
event_od_pory_roku()
{
    if(pora_roku() != MT_ZIMA)
    {
        switch(random(3))
        {
            case 0 : return "Spory, drapie^zny ptak zatacza ko^lo nad jeziorem, by po chwili "+
            "spa^sc  na co^s znajduj^acego si^e na wodzie.\n"; break;
            case 1: return "Jaka� ryba rzuci�a si� w jeziorze.\n"; break;
            case 2: return  "Dochodzi ci^e nagle cichy plusk wody.\n"; break;
        }
    }
    
    return "";
}

string
opis_jeziorka()
{
    string str = "";

    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="Jeziorko pokryte jest tafl^a lodu. ";
    }
    else // chce, zeby przez 4 pory roku byl taki sam opis
    {    // tylko, jak zimno, to zeby bylo zamarzniete
        str+="Na p�nocy rozci^aga si^e ";


        if (MOC_WIATRU(TO) == 0)
        {
            str+="niemal nieskazitelnie g^ladka tafla jeziora";
        }
        else if (MOC_WIATRU(TO) == 1)
        {
            str+="jezioro, delikatnie pomarszczone leciutkimi powiewami "+
                "ch^lodnego wietrzyka";
        }
        else if (MOC_WIATRU(TO) == 2)
        {
            str+="jezioro, poznaczone leniwie ko^lysz^acymi si^e, "+
                "niedu^zymi falami";
        }
        else if (MOC_WIATRU(TO) == 3)
        {
            str+="jezioro, poznaczone do^s^c du^zymi i silnymi falami";
        }
        else
        {
            str+="jezioro o silnie wzburzonych wodach";
        }

        
        if (ZACHMURZENIE(TO) == 0)
        {
            str+=", w kt^orej, niczym w zwierciadle, odbija si^e b^l^ekit "+
                "nieba. "; 
        }
        else if (ZACHMURZENIE(TO) == 1)
        {
            str+=", w kt^orej, niczym w zwierciadle, odbija si^e b^l^ekit "+
                "nieba z gdzieniegdzie przep^lywaj^acymi po nim chmurkami. ";
        }
        else if (ZACHMURZENIE(TO) == 2)
        {
            str+=", w kt^orej odbija si^e niebo, pokryte spor^a ilo^sci^a "+
                "bia^lych i puszystych jak baranki chmur. ";
        }
        else if (ZACHMURZENIE(TO) == 3)
        {
            str+=", w kt^orej odbija si^e niemal ca^lkowicie pokryte chmurami "+
                "niebo. ";
        }
        else
        {
            str+=", w kt^orej odbija si^e szare, ca^lkowicie przykryte "+
                "chmurami niebo. ";
        }


        if (CO_PADA(TO) == 1)
        if (MOC_OPADOW(TO) == 0)
        {
            str+="";
        }
        else if (MOC_OPADOW(TO) == 1)
        {
            str+="Powierzchni^e wody znacz^a niedu^ze kropelki deszczu, "+
                "leniwie spadaj^ace z szarych chmur. ";
        }
        else if (MOC_OPADOW(TO) == 2)
        {
            str+="Powierzchni^e wody znacz^a krople deszczu spadaj^ace z "+
                "szarych chmur. ";
        }
        else if (MOC_OPADOW(TO) == 3)
        {
            str+="Powierzchni^e wody znacz^a krople deszczu energicznie "+
                "spadaj^ace z szarych chmur. ";
        }
        else
        {
            str+="Powierzchni^e wody znacz^a du^ze krople deszczu "+
                "energicznie spadaj^ace z ciemnoszarych chmur. ";
        }
        
        str+="Przed tob^a, niemal po horyzont rozci^aga si^e jezioro, "+
            "jedynie gdzie^s w oddali majacz^a jakie^s drzewa na drugim "+
            "jego kra^ncu.";
        
    }
    

    str+="\n";
    return str;
}
    

string
dlugi_opis()
{
    string str="";
    int ilosc_lodek = sizeof(filter(AI(TO),&->query_lodka()));

        //ze wzgl�du na ��dki nie mo�e by� takich opis�w jak by�y... musia�em zmieni�. [v]
        str+="Jeste� przy";
/*    else if(TP->query_prop(SIT_SIEDZACY))
		str+="Siedzisz na";
    else if(TP->query_prop(SIT_LEZACY))
        str+="Le�ysz na";
	else 
		str+="Stoisz na";*/
	
	str+=" drewnianym pomo�cie kt�ry lekko wcina si� w tafl� jeziora. ";

	if(pora_roku() == MT_ZIMA)
		str+="Powierzchnia jeziora, pokryta grub� warstw� lodu"+
		(CZY_JEST_SNIEG(TO) ? " i �wie�ego �niegu":"")+", odcina si� kolorystycznie od "+
		"szarozielonej linii drzew. Okoliczne zwierz�ta zapad�y ju�"+
		" najprawdopodobniej w zimowy sen, dlatego te� okolica jest "+
		"bardzo cicha i spokojna. ";
	else
		str+="Po powierzchni p�ywaj� kaczki i �ab�dzie, a ma�e "+
		"muszki lataj�ce w powietrzu stanowi� �er dla okolicznego "+
		"ptactwa. Od czasu do czasu g�adka powierzchnia jeziora "+
		"marszczy si� lekko pod podmuchami wiatru. ";

	str+="Drewniana konstrukcja pomostu s�u�y te� okolicznym rybakom "+
	"do przywi�zywania tu swoich ��dek, na kt�rych wyruszaj� w g��b "+
	"jeziora w poszukiwaniu ryb. ";

	if(ilosc_lodek)
        str += "Do pomostu za pomoc� sznur�w, przywi�zanych jest "+
        "kilka ma�ych ��deczek nale��cych do miejscowych rybak�w.";
    else
        str += "Do pomostu za pomoc� sznur�w zwykle przywi�zane s� ��dki "+
        "nale��ce do miejscowych rybak�w, lecz w obecnej chwili wszystkie "+
        "najprawdopodobniej wyp�yn�y na jezioro.";

    

    str += "\n";
    return str;
}
string
opis_nocy()
{
    string str="";

     str +="\n";
     return str;
}



init()
{
    ::init();
    init_kapielisko();
}

string
query_short_zew()
{
    return "Przy pomo�cie";
}


/*

ob pomost
Zbudowany z du�ych, nieociosanych bali pomost, wcina si� lekko w jezioro. Grube, wbite w dno podpory s� solidnie po��czone z pomostem zapewniaj�c tym samym stabilno�� konstrukcji tak, �e platforma ani drgnie pod wp�ywem twych krok�w. W kilku miejscach zauwa�asz do�� szerokie szpary, w kt�rych mo�e utkn�� stopa nieuwa�nej osoby. 1 albo 2

ob ��dki
Male wios�owe ��dki.




*/
