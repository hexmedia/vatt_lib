/* Autor: Vera
   Opis : Praca zbiorowa ludzi z forum Zakonu, g��wnie Sniegulak
   Data : Tuesday 01 May 2007 */ 
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"
#include <pogoda.h>

inherit TRAKT_ZAKON_OKOLICE_STD;

void create_trakt() 
{
    set_short("Rozstaje w lesie");
    add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "t15.c","w",0,TRAKT_ZO_FATIG,0);
    add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "t13.c","s",0,TRAKT_ZO_FATIG,0);
	add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "s_zakonu01.c","ne",
				0,SCIEZKA_ZO_FATIG,0);
    add_prop(ROOM_I_INSIDE,0);
}

public string
exits_description() 
{
    return "Trakt prowadzi z po�udnia na zach�d, za� w kierunku "+
	"p�nocno wschodnim mo�na zej�� na le�n� �cie�k�.\n";
}



//For dummies
zejdz(string str)
{
	notify_fail("Zejd� gdzie?\n");
	if(str ~= "na le�n� �cie�k�" || str~="na �cie�k�")
	{
		TP->command("ne");
		return 1;
	}
	return 0;
}
init()
{
	::init();
	add_action(zejdz,"zejd�");
}

string
dlugi_opis()
{
    string str="";

	if(CZY_JEST_SNIEG(TO))
		str+="Trakt prowadz�cy z po�udnia na p�noc w tym miejscu "+
		"��czy si� z w�sk� �cie�k�, na kt�rej mo�e si� zmie�ci� co "+
		"najwy�ej ma�ych rozmiar�w w�z. W�ski szlak kieruje si� w "+
		"stron� wschodu, przedziera sie przez krzaki i wysokie, "+
		"stare drzewa, kt�re w ca�o�ci przykrywa bia�y puch. "+
		"G��wny trakt rozjechany przez ko�a pojazd�w, pokryty jest "+
		"warstw� �niegu, kt�ra bardzo utrudnia podr� tym szlakiem. "+
		"Gdzieniegdzie w grz�skim b�ocie wida� tak�e �lady "+
		"ko�skich kopyt i ludzkich st�p. W niekt�rych miejscach "+
		"koleiny s� tak g��bokie, i� w�z jad�cy t�dy musi bardzo "+
		"zwolni� by nie z�ama� zawieszenia.";
	else
		str+="Trakt prowadz�cy z po�udnia na p�noc w tym miejscu "+
		"��czy si� z w�sk� �cie�k�, na kt�rej mo�e si� zmie�ci� co "+
		"najwy�ej ma�ych rozmiar�w w�z. W�ski szlak kieruje si� w "+
		"stron� wschodu, przedziera si� przez krzaki i wysokie, stare "+
		"drzewa. Ro�linno�� niezwykle bujna przes�ania ca�kowicie "+
		"widok nieba, powoduje r�wnie�, �e okolice spowija mrok. "+
		"G��wny trakt jest rozjechany przez ko�a pojazd�w, wida� "+
		"na nim gdzieniegdzie tak�e �lady ko�skich kopyt i ludzkich "+
		"st�p. W niekt�rych miejscach koleiny s� tak g��bokie, i� w�z "+
		"jad�cy t�dy musi bardzo zwolni� by nie z�ama� zawieszenia.";

    str += "\n";
    return str;
}
string
opis_nocy()
{
    string str="";

     str +="\n";
     return str;
}