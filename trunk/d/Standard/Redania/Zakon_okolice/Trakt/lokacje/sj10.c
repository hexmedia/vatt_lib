/* Autor: Vera
   Opis : Praca zbiorowa ludzi z forum Zakonu, g��wnie Sniegulak
   Data : Tuesday 01 May 2007 */ 
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"
#include <pogoda.h>

inherit TRAKT_ZAKON_OKOLICE_STD;
#include "../std/eventy_sciezki.c"
void create_trakt() 
{
    set_short("le�na �cie�ka");
    add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "sj09.c","se",0,SCIEZKA_ZO_FATIG,0);
	add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "sj11.c","w",0,SCIEZKA_ZO_FATIG,0);
    add_exit(LAS_ZAKON_OKOLICE_LOKACJE + "jezioro_las22.c","ne",0,LAS_FATIGUE,0);
    add_exit(LAS_ZAKON_OKOLICE_LOKACJE + "jezioro_las21.c","n",0,LAS_FATIGUE,0);
    add_exit(LAS_ZAKON_OKOLICE_LOKACJE + "jezioro_las34.c","s",0,LAS_FATIGUE,0);
    add_exit(LAS_ZAKON_OKOLICE_LOKACJE + "jezioro_las33.c","sw",0,LAS_FATIGUE,0);
    add_prop(ROOM_I_INSIDE,0);
}
public string
exits_description() 
{
    return "�cie�yna ci�gnie si� z zachodu na po�udniowy wsch�d.\n";
}

string
dlugi_opis()
{
    string str = "";

    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="Natura u�piona w czasie zimy, jest cicha i spokojna, nie s�"+
             "ycha� tutaj prawie �adnych d�wi�k�w, a prawie wszystko"+
             " przykryte jest grub� warstwa czystego �niegu. Oblodzona �"+
             "cie�ka wije si� falist� wst�g� ze wschodu na zach�d, "+
             "a gdzie nie spojrze� dooko�a zalega bia�y puch. G�sty las"+
             " przez kt�ry prowadzi w�ska �cie�ka, po kt�rej z du�ym"+
             "i problemami porusza�by si� nawet ma�ych rozmiar�w w�z,"+
             " wygl�da na bardzo stary. ";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str+="W powietrzu unosz� sie stada ma�ych muszek, ptaki �piewaj"+
             "� swoje piosenki, a flora rozkwita na ka�dym kroku. Le�na"+
             " �cie�ka wije si� falist� wst�g� ze wschodu na zach�d"+
             ", otoczona zewsz�d natur�. G�sty las przez kt�ry prowadzi"+
             " w�ska �cie�ka, po kt�rej z du�ymi problemami porusza�"+
             "by si� nawet ma�ych rozmiar�w w�z, wygl�da na bardzo st"+
             "ary. ";
    }
    else if(pora_roku() == MT_LATO)
    {
        str+="W powietrzu unosz� sie stada ma�ych muszek, ptaki �piewaj"+
             "� swoje piosenki, a flora rozkwita na ka�dym kroku. Le�na"+
             " �cie�ka wije si� falist� wst�g� ze wschodu na zach�d"+
             ", otoczona zewsz�d natur�. G�sty las przez kt�ry prowadzi"+
             " w�ska �cie�ka, po kt�rej z du�ymi problemami porusza�"+
             "by si� nawet ma�ych rozmiar�w w�z, wygl�da na bardzo st"+
             "ary. ";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str+="Pod stopami s�yszysz chrz�st patyk�w i szum suchych opadn"+
             "i�tych li�ci. Le�na �cie�ka wije si� falist� wst�g�"+
             " ze wschodu na zach�d, otoczona zewsz�d natur�. G�sty las"+
             " przez kt�ry prowadzi w�ska �cie�ka, po kt�rej z du�ym"+
             "i problemami porusza�by si� nawet ma�ych rozmiar�w w�z,"+
             " wygl�da na bardzo stary. ";
    }

    str+="\n";
    return str;
}
string
opis_polmroku()
{
    string str;
	//Noo, po traktach si� chodzi z lampami!
	//I to o ka�dej porze roku! Nie wiedzieli�cie? :)

    if(CZY_JEST_SNIEG(this_object()))
        str="W mroku jeste� w stanie dostrzec jedynie biel �niegu "+
            "le��cego na �cie�ce.\n";
    else
        str="Nad �cie�k� zapad� mrok, dlatego nie jeste� w stanie dostrzec "+
            "zbyt wiele.\n";

    return str;
}
string
opis_nocy()
{
    string str="";

     str +="\n";
     return str;
}