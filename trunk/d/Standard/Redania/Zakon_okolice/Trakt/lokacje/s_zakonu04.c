/* Autor: Vera
   Opis : Ty� Vera
   Data : Tuesday 01 May 2007 */ 
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"
#include <pogoda.h>

inherit TRAKT_ZAKON_OKOLICE_STD;
#include "../std/eventy_sciezki.c"
void create_trakt() 
{
    set_short("przy bramie");
    add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "s_zakonu03.c","s",0,SCIEZKA_ZO_FATIG,0);

    add_object(ZAKON_DRZWI + "brama_do");
    add_prop(ROOM_I_INSIDE,0);

	add_item("palisad�",
	"Wysoka na jakie� pi�tna�cie �okci palisada ma za zadanie "+
	"chroni� teren znajduj�cy si� za ni� przed atakami pieszych "+
	"jednostek. Drzewa u�yte do jej budowy zosta�y ociosane z "+
	"ga��zi, potem zaostrzone u g�ry, a nast�pnie g��boko wkopane "+
	"w pod�o�e.\n");
}
public string
exits_description() 
{
    return "�cie�ka ci�gnie si� na po�udnie.\n";
}


string
dlugi_opis()
{
    string str="";


	str+="Wbite na rubie�ach polany drewniane ko�ki palisady "+
	"okalaj� kompleks budynk�w, kt�rych proste sklepienia - "+
	"jedyne co st�d dostrzegasz - zdradzaj� jej u�ytkowe "+
	"przeznaczenie. ";
	if(jest_dzien())
		str+="Fala zapachu, jaki do ciebie dociera oraz jasny, "+
		"nieomsza�y i �wie�y kolor drewna ka�� wnioskowa� o "+
		"bardzo kr�tkim �ywocie tych zabudowa�. ";

	str+="Zza g�ucho zamkni�tych skrzyde� bramy dobiegaj� raz "+
		"po raz kr�tkie wojskowe okrzykiwania";

	if(jest_dzien())
		str+=" t�umione przez zwart� �cian� lasu, z kt�rego rodz�ca "+
		"si� u st�p bramy �cie�ka wije si� zakr�tami";
	else
		str+=", a blade, niespokojne widmo �uczywa przedziera "+
		"si� tu i �wdzie mi�dzy szczelinami belek rzucaj�c "+
		"nik�y cie� na udeptan� drog� nikn�c� gdzie� w lesie";


    str += ".\n";
    return str;
}
string
opis_polmroku()
{
    string str;
	//Noo, po traktach si� chodzi z lampami!
	//I to o ka�dej porze roku! Nie wiedzieli�cie? :)

    if(CZY_JEST_SNIEG(this_object()))
        str="W mroku jeste� w stanie dostrzec jedynie biel �niegu "+
            "le��cego na �cie�ce.\n";
    else
        str="Nad �cie�k� zapad� mrok, dlatego nie jeste� w stanie dostrzec "+
            "zbyt wiele.\n";

    return str;
}
string
opis_nocy()
{
    string str="";

     str +="\n";
     return str;
}