/* Autor: Vera
   Opis : Praca zbiorowa ludzi z forum Zakonu, g��wnie Sniegulak
   Data : Tuesday 01 May 2007 */ 
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"
#include <pogoda.h>

inherit TRAKT_ZAKON_OKOLICE_STD;
#include "../std/eventy_sciezki.c"
void create_trakt() 
{
    set_short("^Scie�ka");
    add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "t14.c","sw",0,TRAKT_ZO_FATIG,0);
	add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "s_zakonu02.c","e",0,SCIEZKA_ZO_FATIG,0);
    add_prop(ROOM_I_INSIDE,0);
}
public string
exits_description() 
{
    return "�cie�ka ci�gnie si� na wschodzie, za� na po�udniowym zachodzie "+
		"mo�na wej�� na trakt.\n";
}

//For dummies
wejdz(string str)
{
	notify_fail("Wejd� gdzie?\n");
	if(str == "na trakt")
	{
		TP->command("sw");
		return 1;
	}
	return 0;
}
init()
{
	::init();
	add_action(wejdz,"wejd�");
}

string
dlugi_opis()
{
    string str = "";

    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="W�ska wydeptana ludzkimi stopami �cie�ka prowadz�ca prze"+
             "z las wije si� mi�dzy drzewami. Zwarta �ciana drzew li�ci"+
             "asto iglastych przeszkadza podr�nemu w zboczeniu ze �cie"+
             "�ki, a masywne i g�ste korony przys�aniaj� widok nieba. B"+
             "ia�y puch przykrywaj�cy ro�liny jest do�� gruby i skrzy"+
             "pi pod krokami, a gdzieniegdzie mo�na zauwa�y� na nim dro"+
             "bne �lady ma�ych zwierz�t. ";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str+="W�ska wydeptana ludzkimi stopami �cie�ka prowadz�ca prze"+
             "z las wije si� mi�dzy drzewami. Zwarta �ciana drzew li�ci"+
             "asto iglastych przeszkadza podr�nemu w zboczeniu ze �cie"+
             "�ki, a masywne i g�ste korony przys�aniaj� widok nieba. S"+
             "oczysta ziele�, i wszechobecne odg�osy zamieszkuj�cych te"+
             "n las zwierz�t, powoduj� i� w�dr�wka t� �cie�yn� je"+
             "st naprawd� przyjemna. ";
    }
    else if(pora_roku() == MT_LATO)
    {
        str+="W�ska wydeptana ludzkimi stopami �cie�ka prowadz�ca prze"+
             "z las wije si� mi�dzy drzewami. Zwarta �ciana drzew li�ci"+
             "asto iglastych przeszkadza podr�nemu w zboczeniu ze �cie"+
             "�ki, a masywne i g�ste korony przys�aniaj� widok nieba. S"+
             "oczysta ziele�, i wszechobecne odg�osy zamieszkuj�cych te"+
             "n las zwierz�t, powoduj� i� w�dr�wka t� �cie�yn� je"+
             "st naprawd� przyjemna. ";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str+="W�ska wydeptana ludzkimi stopami �cie�ka prowadz�ca prze"+
             "z las wije si� mi�dzy drzewami. Zwarta �ciana drzew li�ci"+
             "asto iglastych przeszkadza podr�nemu w zboczeniu ze �cie"+
             "�ki, a masywne i g�ste korony przys�aniaj� widok nieba. O"+
             "pad�e z drzew li�cie powoduj� i� na pod�o�u utworzy� "+
             "sie dywan szumi�cy i szeleszcz�cy pod ka�dym krokiem podr"+
             "�nego. ";
    }

    str+="\n";
    return str;
}

string
opis_polmroku()
{
    string str;
	//Noo, po traktach si� chodzi z lampami!
	//I to o ka�dej porze roku! Nie wiedzieli�cie? :)

    if(CZY_JEST_SNIEG(this_object()))
        str="W mroku jeste� w stanie dostrzec jedynie biel �niegu "+
            "le��cego na �cie�ce.\n";
    else
        str="Nad �cie�k� zapad� mrok, dlatego nie jeste� w stanie dostrzec "+
            "zbyt wiele.\n";

    return str;
}
string
opis_nocy()
{
    string str="";

     str +="\n";
     return str;
}