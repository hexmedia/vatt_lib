/* Autor: Vera
   Opis : Praca zbiorowa ludzi z forum Zakonu, g��wnie Sniegulak
   Data : Tuesday 01 May 2007 */ 
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"
#include <pogoda.h>

inherit TRAKT_ZAKON_OKOLICE_STD;
#include "../std/eventy_sciezki.c"
void create_trakt() 
{
    set_short("kr�ta �cie�ka");
    add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "sj12.c","ne",0,SCIEZKA_ZO_FATIG,0);
	add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "sj14.c","nw",0,SCIEZKA_ZO_FATIG,0);
    add_exit(LAS_ZAKON_OKOLICE_LOKACJE + "jezioro_las36.c","e",0,LAS_FATIGUE,0);
    add_exit(LAS_ZAKON_OKOLICE_LOKACJE + "jezioro_las32.c","n",0,LAS_FATIGUE,0);
    add_prop(ROOM_I_INSIDE,0);
}
public string
exits_description() 
{
    return "�cie�yna ci�gnie si� z p�nocnego zachodu na p�nocny wsch�d.\n";
}

string
dlugi_opis()
{
    string str = "";

    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="Zwarta �ciana drzew li�ciasto iglastych przeszkadza podr�"+
             "�nemu w zboczeniu ze �cie�ki, a masywne i g�ste korony pr"+
             "zys�aniaj� widok nieba. G�sto rosn�ce praktycznie ze wsz"+
             "ystkich stron wysokie drzewa przejmuj� groz�, a powykr�ca"+
             "ne krzewy i pnie dodaj� na niesamowito�ci otoczenia. Drzew"+
             "a rosn�ce na p�nocy pn� si� w g�r� tworz�c jakby na"+
             "turalny p�ot, broni�cy las przed intruzami, jednocze�nie "+
             "drzewa te, dzi�ki roz�o�ystym koronom daj� spore zacieni"+
             "enie przez co w�dr�wka t� �cie�yn� nie wydaje si� by�"+
             " a� tak m�cz�ca. Bia�y puch przykrywaj�cy ro�liny jes"+
             "t do�� gruby i skrzypi pod krokami, a gdzieniegdzie mo�na"+
             " zauwa�y� na nim drobne �lady ma�ych zwierz�t. ";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str+="Zwarta �ciana drzew li�ciasto iglastych przeszkadza podr�"+
             "�nemu w zboczeniu ze �cie�ki, a masywne i g�ste korony pr"+
             "zys�aniaj� widok nieba. G�sto rosn�ce praktycznie ze wsz"+
             "ystkich stron wysokie drzewa przejmuj� groz�, a powykr�ca"+
             "ne krzewy i pnie dodaj� na niesamowito�ci otoczenia. Drzew"+
             "a rosn�ce na p�nocy pn� si� w g�r� tworz�c jakby na"+
             "turalny p�ot, broni�cy las przed intruzami, jednocze�nie "+
             "drzewa te, dzi�ki roz�o�ystym koronom daj� spore zacieni"+
             "enie przez co w�dr�wka t� �cie�yn� nie wydaje si� by�"+
             " a� tak m�cz�ca. Soczysta ziele�, i wszechobecne odg�o"+
             "sy zamieszkuj�cych ten las zwierz�t, powoduj� i� w�dr�"+
             "wka t� �cie�yn� jest naprawd� przyjemna. ";
    }
    else if(pora_roku() == MT_LATO)
    {
        str+="Zwarta �ciana drzew li�ciasto iglastych przeszkadza podr�"+
             "�nemu w zboczeniu ze �cie�ki, a masywne i g�ste korony pr"+
             "zys�aniaj� widok nieba. G�sto rosn�ce praktycznie ze wsz"+
             "ystkich stron wysokie drzewa przejmuj� groz�, a powykr�ca"+
             "ne krzewy i pnie dodaj� na niesamowito�ci otoczenia. Drzew"+
             "a rosn�ce na p�nocy pn� si� w g�r� tworz�c jakby na"+
             "turalny p�ot, broni�cy las przed intruzami, jednocze�nie "+
             "drzewa te, dzi�ki roz�o�ystym koronom daj� spore zacieni"+
             "enie przez co w�dr�wka t� �cie�yn� nie wydaje si� by�"+
             " a� tak m�cz�ca. Soczysta ziele�, i wszechobecne odg�o"+
             "sy zamieszkuj�cych ten las zwierz�t, powoduj� i� w�dr�"+
             "wka t� �cie�yn� jest naprawd� przyjemna. ";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str+="Zwarta �ciana drzew li�ciasto iglastych przeszkadza podr�"+
             "�nemu w zboczeniu ze �cie�ki, a masywne i g�ste korony pr"+
             "zys�aniaj� widok nieba. G�sto rosn�ce praktycznie ze wsz"+
             "ystkich stron wysokie drzewa przejmuj� groz�, a powykr�ca"+
             "ne krzewy i pnie dodaj� na niesamowito�ci otoczenia. Drzew"+
             "a rosn�ce na p�nocy pn� si� w g�r� tworz�c jakby na"+
             "turalny p�ot, broni�cy las przed intruzami, jednocze�nie "+
             "drzewa te, dzi�ki roz�o�ystym koronom daj� spore zacieni"+
             "enie przez co w�dr�wka t� �cie�yn� nie wydaje si� by�"+
             " a� tak m�cz�ca. Opad�e z drzew li�cie powoduj� i� n"+
             "a pod�o�u utworzy� sie dywan szumi�cy i szeleszcz�cy po"+
             "d ka�dym krokiem podr�nego. ";
    }

    str+="\n";
    return str;
}
string
opis_polmroku()
{
    string str;
	//Noo, po traktach si� chodzi z lampami!
	//I to o ka�dej porze roku! Nie wiedzieli�cie? :)

    if(CZY_JEST_SNIEG(this_object()))
        str="W mroku jeste� w stanie dostrzec jedynie biel �niegu "+
            "le��cego na �cie�ce.\n";
    else
        str="Nad �cie�k� zapad� mrok, dlatego nie jeste� w stanie dostrzec "+
            "zbyt wiele.\n";

    return str;
}
string
opis_nocy()
{
    string str="";

     str +="\n";
     return str;
}