//vera

inherit "/std/woda/lodka_in.c";
#include <stdproperties.h>
#include "dir.h"

void create_wnetrze()
{
	//set_short("W b�czku");
	opis_wnetrza("Zbudowany najprawdopodobniej przez jakiego� domoros�ego cie�le b�czek, zbity "+
    "jest z nieheblowanych desek, kt�re zosta�y z zewn�trz poci�gni�te smo��. By ��dka nie "+
    "przecieka�a szpary mi�dzy deskami, wype�niono namoczonymi w smole szmatami. W poprzek "+
    "ci�gnie si� szeroka deska, na kt�rej mo�na usi��� i zacz�� wios�owa�. Na burtach "+
    "�odzi nie ma �adnych dulek tak wi�c wios�owanie zanosi si� by� do�� mozolne i "+
    "pracoch�onne. Pokrycie �opat smo�� ma za zadanie nie pozwoli� wodzie na wnikni�cie w "+
    "drewno. Konstrukcja s�abo wywa�ona chybocze sie znacznie na boki, co czasami powoduje "+
    "wlewanie sie wody do �rodka.\n");
	
	add_sit("na b�czku", "na b�czku", "z deski b�czka",2);
	add_prop(ROOM_I_HIDE, -1);
	
	dodaj_komende("zejd�", "schodzi", "zej��");
	dodaj_komende("zeskocz", "zeskakuje", "zeskoczy�");
    
    dodaj_wioslo(1);
    
    set_event_time(370.0);
    add_event("Fale delikatnie i miarowo ko�ysz� b�czkiem.\n");
    add_event("Malutka ka�u�a na �rodku ��dki w�a�nie powi�kszy�a si� nieco o kolejn� porcj� wody, kt�ra wlewa si� od czasu do czasu przez dulki.\n");
    add_event("Jedna z desek b�czka skrzypn�a przeci�gle.\n");
    add_event("B�czek chybocze si� na boki.\n");
    add_event("Dochodzi ci� g�uche pukni�cie. Chyba co� z zewn�trz obi�o si� o chwiejn� konstrukcj� ��deczki.\n");
    add_event("Stare deski sk�adaj�ce si� na szkielet ��dki skrzypi� budz�c w tobie lekki niepok�j.\n");
    add_event("");
}
