/**
 * Jest to zwyk�a ma�a ��dka, tzw. b�czek.
 * Operacja Jeziorko :)
 * @author Vera
 * @date 18.07.2009
 */

inherit "/std/woda/lodka";

#include <macros.h>
#include <pl.h>
#include <stdproperties.h>
#include <materialy.h>
#include "dir.h"

void
create_lodka()
{
    dodaj_nazwy("b�czek");
    dodaj_przym("ma�y","mali");
    dodaj_przym("drewniany","drewniani");
    set_long("Zbudowane najprawdopodobniej przez jakiego� domoros�ego cie�le b�czki, zbite "+
    "s� z nieheblowanych desek, kt�re zosta�y z zewn�trz poci�gni�te smo��. By ��dki nie "+
    "przecieka�y szpary mi�dzy deskami, wype�niono namoczonymi w smole szmatami. W poprzek "+
    "ci�gnie si� szeroka deska, na kt�rej mo�na usi��� i zacz�� wios�owa�. Na burtach "+
    "�odzi nie ma �adnych dulek tak wi�c wios�owanie zanosi si� by� do�� mozolne i "+
    "pracoch�onne. Pokrycie �opat smo�� ma za zadanie nie pozwoli� wodzie na wnikni�cie w "+
    "drewno. Konstrukcja s�abo wywa�ona chybocze sie znacznie na boki, co czasami powoduje "+
    "wlewanie sie wody do �rodka.\n");
    
    dodaj_komende(({"wskocz", "wskakuje","wskoczy�","do",PL_DOP}));
    dodaj_komende(({"wejd�", "wchodzi","wej��","na",PL_BIE}));
    
    add_prop(CONT_I_MAX_VOLUME, 11200000);
    add_prop(CONT_I_MAX_WEIGHT, 11200000);
    add_prop(OBJ_I_WEIGHT, 120240);
    add_prop(OBJ_I_VOLUME, 137500);
    add_prop(OBJ_I_VALUE, 326);
    ustaw_material(MATERIALY_DR_DAB,100);
    
    //�aduje sie zacumowana
    zacumuj("do pomostu");
    ustaw_wnetrze(JEZIORKO_ZAKON_OBIEKTY + "baczek_in");
}

