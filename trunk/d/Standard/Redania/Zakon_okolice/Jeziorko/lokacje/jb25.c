/* Autor: 
   Data : 
   Opis : */

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <pogoda.h>
#include "dir.h"
inherit JEZIORKO_ZAKON_BRZEG_STD;

void create_jezioro_brzeg() 
{
    set_short("Przy brzegu");
    add_exit(JEZIORKO_ZAKON_LOKACJE + "js13","w");
    add_exit(JEZIORKO_ZAKON_LOKACJE + "jb27","nw");
    add_exit(JEZIORKO_ZAKON_LOKACJE + "jb22","s");
    add_exit(JEZIORKO_ZAKON_LOKACJE + "js12","sw");
    
    add_prop(ROOM_I_INSIDE,0);
}

string
dlugi_opis()
{
    string str = "";

    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="Jeziorko pokryte jest tafl^a lodu. ";
    }
    else // chce, zeby przez 4 pory roku byl taki sam opis
    {    // tylko, jak zimno, to zeby bylo zamarzniete
        str+="Wok^o^l rozci^aga si^e ";


        if (MOC_WIATRU(TO) == 0)
        {
            str+="niemal nieskazitelnie g^ladka tafla jeziora";
        }
        else if (MOC_WIATRU(TO) == 1)
        {
            str+="jezioro, delikatnie pomarszczone leciutkimi powiewami "+
                "ch^lodnego wietrzyka";
        }
        else if (MOC_WIATRU(TO) == 2)
        {
            str+="jezioro, poznaczone leniwie ko^lysz^acymi si^e, "+
                "niedu^zymi falami";
        }
        else if (MOC_WIATRU(TO) == 3)
        {
            str+="jezioro, poznaczone do^s^c du^zymi i silnymi falami";
        }
        else
        {
            str+="jezioro o silnie wzburzonych wodach";
        }

        
        if (ZACHMURZENIE(TO) == 0)
        {
            str+=", w kt^orej, niczym w zwierciadle, odbija si^e b^l^ekit "+
                "nieba. "; 
        }
        else if (ZACHMURZENIE(TO) == 1)
        {
            str+=", w kt^orej, niczym w zwierciadle, odbija si^e b^l^ekit "+
                "nieba z gdzieniegdzie przep^lywaj^acymi po nim chmurkami. ";
        }
        else if (ZACHMURZENIE(TO) == 2)
        {
            str+=", w kt^orej odbija si^e niebo, pokryte spor^a ilo^sci^a "+
                "bia^lych i puszystych jak baranki chmur. ";
        }
        else if (ZACHMURZENIE(TO) == 3)
        {
            str+=", w kt^orej odbija si^e niemal ca^lkowicie pokryte chmurami "+
                "niebo. ";
        }
        else
        {
            str+=", w kt^orej odbija si^e szare, ca^lkowicie przykryte "+
                "chmurami niebo. ";
        }


        if (CO_PADA(TO) == 1)
        if (MOC_OPADOW(TO) == 0)
        {
            str+="";
        }
        else if (MOC_OPADOW(TO) == 1)
        {
            str+="Powierzchni^e wody znacz^a niedu^ze kropelki deszczu, "+
                "leniwie spadaj^ace z szarych chmur. ";
        }
        else if (MOC_OPADOW(TO) == 2)
        {
            str+="Powierzchni^e wody znacz^a krople deszczu spadaj^ace z "+
                "szarych chmur. ";
        }
        else if (MOC_OPADOW(TO) == 3)
        {
            str+="Powierzchni^e wody znacz^a krople deszczu energicznie "+
                "spadaj^ace z szarych chmur. ";
        }
        else
        {
            str+="Powierzchni^e wody znacz^a du^ze krople deszczu "+
                "energicznie spadaj^ace z ciemnoszarych chmur. ";
        }


        str+="Na zachodzie i po^ludniu rozci^aga si^e jezioro, natomiast na "+
            "p^o^lnocy i wschodzie rozci^aga si^e poro^sni^ety nadwodn^a "+
            "ro^slinno^sci^a brzeg, kt^ora uniemo^zliwia przedostanie si^e "+
            "tam. ";

        
    }
    

    str+="\n";
    return str;
}