/* Autor: 
   Data : 
   Opis : */

#include <files.h>
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <pogoda.h>
#include "dir.h"
inherit JEZIORKO_ZAKON_BRZEG_STD;


int
czy_mozna()
{
    //if(ENV(TP)->query_lodka_in())
        return 0;
    /*else
        return 11;*/
}

void create_jezioro_brzeg() 
{
    set_short("Przy pomo�cie");
    add_exit(JEZIORKO_ZAKON_LOKACJE + "jb03","nw");
    add_exit(JEZIORKO_ZAKON_LOKACJE + "jb04","n");
    add_exit(JEZIORKO_ZAKON_LOKACJE + "jb05","ne");
    add_exit(JEZIORKO_ZAKON_LOKACJE + "jb02","e");
    
    
    //tajne jeziorko - a w^la^sciwie to pomost :)
    add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE+"pomost.c",({"pomost","w stron� pomostu","z jeziora"}),
                "@@czy_mozna@@",5,0);

    add_prop(ROOM_I_INSIDE,0);
    
}

public string
exits_description()
{
    return "Jezioro rozci^aga si^e w kierunkach: p^o^lnocny-zach^od, "+
        "p^o^lnoc, p^o^lnocny-wsch^od i wsch^od. Mo^zesz st^ad tak^ze "+
        "dop^lyn^a^c do pomostu.\n";
}

string
dlugi_opis()
{
    string str = "";

    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="Jeziorko pokryte jest tafl^a lodu. ";
    }
    else // chce, zeby przez 4 pory roku byl taki sam opis
    {    // tylko, jak zimno, to zeby bylo zamarzniete
        str+="Wok^o^l rozci^aga si^e ";


        if (MOC_WIATRU(TO) == 0)
        {
            str+="niemal nieskazitelnie g^ladka tafla jeziora";
        }
        else if (MOC_WIATRU(TO) == 1)
        {
            str+="jezioro, delikatnie pomarszczone leciutkimi powiewami "+
                "ch^lodnego wietrzyka";
        }
        else if (MOC_WIATRU(TO) == 2)
        {
            str+="jezioro, poznaczone leniwie ko^lysz^acymi si^e, "+
                "niedu^zymi falami";
        }
        else if (MOC_WIATRU(TO) == 3)
        {
            str+="jezioro, poznaczone do^s^c du^zymi i silnymi falami";
        }
        else
        {
            str+="jezioro o silnie wzburzonych wodach";
        }

        
        if (ZACHMURZENIE(TO) == 0)
        {
            str+=", w kt^orej, niczym w zwierciadle, odbija si^e b^l^ekit "+
                "nieba. "; 
        }
        else if (ZACHMURZENIE(TO) == 1)
        {
            str+=", w kt^orej, niczym w zwierciadle, odbija si^e b^l^ekit "+
                "nieba z gdzieniegdzie przep^lywaj^acymi po nim chmurkami. ";
        }
        else if (ZACHMURZENIE(TO) == 2)
        {
            str+=", w kt^orej odbija si^e niebo, pokryte spor^a ilo^sci^a "+
                "bia^lych i puszystych jak baranki chmur. ";
        }
        else if (ZACHMURZENIE(TO) == 3)
        {
            str+=", w kt^orej odbija si^e niemal ca^lkowicie pokryte chmurami "+
                "niebo. ";
        }
        else
        {
            str+=", w kt^orej odbija si^e szare, ca^lkowicie przykryte "+
                "chmurami niebo. ";
        }


        if (CO_PADA(TO) == 1)
        if (MOC_OPADOW(TO) == 0)
        {
            str+="";
        }
        else if (MOC_OPADOW(TO) == 1)
        {
            str+="Powierzchni^e wody znacz^a niedu^ze kropelki deszczu, "+
                "leniwie spadaj^ace z szarych chmur. ";
        }
        else if (MOC_OPADOW(TO) == 2)
        {
            str+="Powierzchni^e wody znacz^a krople deszczu spadaj^ace z "+
                "szarych chmur. ";
        }
        else if (MOC_OPADOW(TO) == 3)
        {
            str+="Powierzchni^e wody znacz^a krople deszczu energicznie "+
                "spadaj^ace z szarych chmur. ";
        }
        else
        {
            str+="Powierzchni^e wody znacz^a du^ze krople deszczu "+
                "energicznie spadaj^ace z ciemnoszarych chmur. ";
        }
        
        str+="Przed tob^a, niemal po horyzont rozci^aga si^e jezioro, "+
            "jedynie gdzie^s w oddali majacz^a jakie^s drzewa na drugim "+
            "jego kra^ncu. Na po^ludniu rozci^aga si^e brzeg poro^sni^ety "+
            "trzcin^a i krzewami, spomi^edzy kt^orych wychyla si^e "+
            "drewniany pomost. ";
        
    }
    

    str+="\n";
    return str;
}