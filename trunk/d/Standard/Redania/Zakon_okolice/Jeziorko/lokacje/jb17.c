/* Autor: 
   Data : 
   Opis : */

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <pogoda.h>
#include "dir.h"
inherit JEZIORKO_ZAKON_BRZEG_STD;

void create_jezioro_brzeg() 
{
    set_short("Niedaleko brzegu");
    add_exit(JEZIORKO_ZAKON_LOKACJE + "jb16","w");
    add_exit(JEZIORKO_ZAKON_LOKACJE + "jb20","nw");
    add_exit(JEZIORKO_ZAKON_LOKACJE + "jb21","n");
    add_exit(JEZIORKO_ZAKON_LOKACJE + "js11","ne");
	add_exit(JEZIORKO_ZAKON_LOKACJE + "js09","e");
	add_exit(JEZIORKO_ZAKON_LOKACJE + "js06","se");
	add_exit(JEZIORKO_ZAKON_LOKACJE + "jb14","s");
    
    add_prop(ROOM_I_INSIDE,0);
}

string
dlugi_opis()
{
    string str = "";

    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="Jeziorko pokryte jest tafl^a lodu. ";
    }
    else // chce, zeby przez 4 pory roku byl taki sam opis
    {    // tylko, jak zimno, to zeby bylo zamarzniete
        str+="Wok^o^l rozci^aga si^e ";


        if (MOC_WIATRU(TO) == 0)
        {
            str+="niemal nieskazitelnie g^ladka tafla jeziora";
        }
        else if (MOC_WIATRU(TO) == 1)
        {
            str+="jezioro, delikatnie pomarszczone leciutkimi powiewami "+
                "ch^lodnego wietrzyka";
        }
        else if (MOC_WIATRU(TO) == 2)
        {
            str+="jezioro, poznaczone leniwie ko^lysz^acymi si^e, "+
                "niedu^zymi falami";
        }
        else if (MOC_WIATRU(TO) == 3)
        {
            str+="jezioro, poznaczone do^s^c du^zymi i silnymi falami";
        }
        else
        {
            str+="jezioro o silnie wzburzonych wodach";
        }

        
        if (ZACHMURZENIE(TO) == 0)
        {
            str+=", w kt^orej, niczym w zwierciadle, odbija si^e b^l^ekit "+
                "nieba. "; 
        }
        else if (ZACHMURZENIE(TO) == 1)
        {
            str+=", w kt^orej, niczym w zwierciadle, odbija si^e b^l^ekit "+
                "nieba z gdzieniegdzie przep^lywaj^acymi po nim chmurkami. ";
        }
        else if (ZACHMURZENIE(TO) == 2)
        {
            str+=", w kt^orej odbija si^e niebo, pokryte spor^a ilo^sci^a "+
                "bia^lych i puszystych jak baranki chmur. ";
        }
        else if (ZACHMURZENIE(TO) == 3)
        {
            str+=", w kt^orej odbija si^e niemal ca^lkowicie pokryte chmurami "+
                "niebo. ";
        }
        else
        {
            str+=", w kt^orej odbija si^e szare, ca^lkowicie przykryte "+
                "chmurami niebo. ";
        }


        if (CO_PADA(TO) == 1)
        if (MOC_OPADOW(TO) == 0)
        {
            str+="";
        }
        else if (MOC_OPADOW(TO) == 1)
        {
            str+="Powierzchni^e wody znacz^a niedu^ze kropelki deszczu, "+
                "leniwie spadaj^ace z szarych chmur. ";
        }
        else if (MOC_OPADOW(TO) == 2)
        {
            str+="Powierzchni^e wody znacz^a krople deszczu spadaj^ace z "+
                "szarych chmur. ";
        }
        else if (MOC_OPADOW(TO) == 3)
        {
            str+="Powierzchni^e wody znacz^a krople deszczu energicznie "+
                "spadaj^ace z szarych chmur. ";
        }
        else
        {
            str+="Powierzchni^e wody znacz^a du^ze krople deszczu "+
                "energicznie spadaj^ace z ciemnoszarych chmur. ";
        }


        str+="Pobliski zachodni brzeg jest niemal ca^lkowicie poro^sni^ety "+
            "trzcinami, na nieco bardziej oddalonym brzegu p^o^lnocnym "+
            "^z^o^lci si^e piasek dzikiej pla^zy, natomiast na wschodzie i "+
            "po^ludniu roztaczaj^a si^e wody jeziora, a odleg^ly brzeg "+
            "oznaczony jest jedynie ma^lymi z tej odleg^lo^sci drzewami. ";

        
    }
    

    str+="\n";
    return str;
}