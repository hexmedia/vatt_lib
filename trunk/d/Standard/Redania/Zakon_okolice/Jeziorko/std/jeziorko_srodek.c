
/*
 * STD jeziorka, lokacje na srodku jeziora
 *
 * Faeve, lipiec 2009  
 *                                       */

#include "dir.h"

//inherit REDANIA_STD; dla jeziorka robimy wyj�tek:
inherit "/std/woda/woda_powierzchnia";

inherit "/std/ryby/lowisko";

#include <mudtime.h>
#include <language.h>
#include <stdproperties.h>
#include <pogoda.h>
#include <filter_funs.h>
#include <macros.h>
#include <sit.h>
#include <woda.h>

string opis_polmroku();
string dlugi_opis();

void
create_jezioro_srodek()
{
}

string
event_jezioro_srodek()
{
    return "";
}

string
evencik()
{
	string str="";
	switch(pora_roku())
	{
		case MT_LATO:
		case MT_WIOSNA:
		case MT_JESIEN:
			switch(random(16))
			{
				case 0: str="Wp�ywasz w ch�odniejszy pr�d wodny.\n";
                        break;
                case 1: str="Co� plusn�o w wodzie, tworz�c rozchodz�ce si� kr�gi wok�.\n";
                        break;
                case 2: str="Masz wra�enie, �e co� przep�yn�o tu� obok ciebie.\n";
                        break;
                case 3: str="W�r�d przybrze�nych trzcin rozbrzmiewa �abi rechot.\n";
                        break;
                case 4: if(jest_dzien()) str="Po tafli jeziora d�ugimi susami skacz� ma�e, podobne do paj�k�w owady.\n";
                        else str="Masz wra�enie, �e w�r�d przybrze�nych drzew co�, a mo�e kto� si� tobie przygl�da.\n";
                        break;
                case 5: if(jest_dzien()) str="Tu� obok ciebie przelatuje wielka niebieska wa�ka.\n";
                        else str="Jaki� ciemny kszta�t przelecia� ci tu� nad g�ow�.";
                        break;
                case 6: if(jest_dzien()) str="Jakie� wielkie muszysko pr�buje usi��� ci na g�owie.\n";
                        break;
			}
			break;
		case MT_ZIMA:
			switch(random(12))
			{/*
				case 0: str="event0\n";
						break;
				case 1: str="event1\n";
						break;*/
				
			}

	}
	return str;
}

nomask void
create_woda_powierzchnia()
{
    set_short("na �rodku jeziorka");
    set_long("@@dlugi_opis@@");
    set_polmrok_long("@@opis_polmroku@@");

    set_start_place(0);

    add_event("@@event_jeziorkowy:"+file_name(TO)+"@@");
	add_prop(ROOM_I_TYPE, ROOM_IN_WATER);
    set_event_time(410.0);
	add_event("@@evencik:"+file_name(TO)+"@@");

		dodaj_lowisko("w jeziorze",
                        (["ploc":       1.1,
                          "ukleja":     1.4,
                          "okon":       1.0,
                          "szczupak":   0.5,
                          "galaz":      1.0,
                          "zielsko":    1.3,
                          "leszcz":     1.0, //tylko w g��bokiej wodzie
                         "jazgarz":     1.2, //tylko w g��bokiej wodzie
                          "sum":        0.7, //tylko w g��bokiej wodzie
                          "mietus":     1.4, //tylko w g��bokiej wodzie
                          "obunog_ryba":0.4  //tylko w g��bokiej wodzie
                          ]));
                          
    add_prop(ROOM_I_WSP_Y, WSP_X_ZAKON_JEZIORKO); //Wsp�rz�dne, do systemu pogodowego.
    add_prop(ROOM_I_WSP_X, WSP_X_ZAKON_JEZIORKO); //Jeziorko niedaleko zakonu, wiec niech bedzie ;)

    add_prop(IL_POZIOMOW, 3); //liczba poziom�w lokacji 'pod wod�'

    create_jezioro_srodek();
}


string
dlugi_opis()
{
	return "Ups! Zg�o� b��d!\n";
}

string
opis_polmroku()
{
   // tez powinien byc zalezny od pogody chyba, czy sie ksiezyc
   //odbija w wodzie, czy nie ;) czy woda gladka
   // musze sobie notatki robic, bo mam skleroze ;)

   return "B^l^a^a^ad!\n";
}

string
query_auto_load()
{
    return 0;
}
