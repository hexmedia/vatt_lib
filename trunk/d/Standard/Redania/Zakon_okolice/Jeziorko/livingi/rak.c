/**
 * Rak. skupowany w karczmie wyspiarskiej rinde, oraz na targu rybnym BM
 *
 * autor Cairell , poprawki vera.
 * opisy Tabakista
 * data   2009
 */

inherit "/std/zwierze.c";
inherit "/std/combat/unarmed";
inherit "/std/act/domove";
#include <std.h>
#include <ss_types.h>
#include <object_types.h>
#include <macros.h>
#include <stdproperties.h>
#include <pl.h>

void zwiewa(object CO);
void zdychaj();
string zaatakowany();

void
create_zwierze()
{
    int do_obj= 100+random(300);
    ustaw_odmiane_rasy("rak");
    set_gender(G_MALE);

    random_przym(({"kostropaty", "brunatny", 
        "czarny", "ciemny"}), ({"niedu^zy", "niewielki", "ma^ly", "spory", 
        "wyro^sni^ety", "du^zy"}), 2);

    set_long("Wyci^agni^ete przed siebie grube szczypce i d^lugi, zako^nczony "+
        "wachlarzem ogon nie pozwalaj^a pomyli^c tego zwierz^ecia z rzadnym "+
        "innym. Barw^e porowatego pancerza tego raka nie spos^ob nawet "+
        "por^owna^c do potocznej czerwieni, lecz stworzeniu musi i tak nie "+
        "najgorzej s^lu^zy^c, gdy^z w kilku miejscach nosi on drobne, "+
        "r^ownoleg^le sznyty. Rak miarowo kurczy liczne, pokrywaj^ace "+
        "sp^od tu^lowia i ogona odn^o^za. Poza tym zachowuje ca^lkowity "+
        "spok^oj patrz^ac t^epo przed siebie malutkimi, osadzonymi pod "+
        "d^lugimi czu^lkami oczkami.\n");

    set_stats (({2+random(4),1+random(2),5+random(3),1,1}));
    add_prop(CONT_I_VOLUME, do_obj);
    add_prop(CONT_I_WEIGHT, do_obj+50);
    
    set_type(O_RYBY); //a co ;p
    
    int val = random(6) - random(3);
    switch(do_obj)
    {
        case 0..100:   val += 8; break;
        case 101..200: val += 11; break;
        case 201..300: val += 13; break;
        default:       val += 15; break;
    }
    add_prop(OBJ_I_VALUE, val);
    
    
    add_prop(CONT_I_HEIGHT, 10+random(15));
    add_prop(NPC_I_NO_RUN_AWAY, 1);
    add_prop(LIVE_I_NEVERKNOWN, 1);
    //add_prop(LIVE_I_LIVE_UNDER_WATER, 1);
    add_prop(LIVE_I_SEE_DARK, 1);
    add_prop(OBJ_M_NO_ATTACK, &zaatakowany());
    remove_prop(OBJ_I_NO_GET);
    
    set_random_move(30);

    set_skill(SS_UNARM_COMBAT,  5 + random(10));
    set_skill(SS_DEFENCE,       3 + random(10));
    set_skill(SS_AWARENESS,     30 + random(10));
    set_skill(SS_BLIND_COMBAT,  40 + random(10));
    set_skill(SS_HIDE,          30 + random(20));
    set_skill(SS_SNEAK,         30 + random(20));
    set_skill(SS_SWIM,          50 + random(20));

    set_attack_unarmed(0, 20, 22, W_SLASH,  100, "szczypce");

    set_hitloc_unarmed(0, ({ 1, 1, 1 }), 10, "g^lowa");
    set_hitloc_unarmed(1, ({ 1, 1, 4 }), 40, "tu^l^ow");
    set_hitloc_unarmed(2, ({ 1, 1, 1 }), 30, "ogon");
    set_hitloc_unarmed(2, ({ 1, 1, 1 }), 20, "szczypce");
    
    set_m_out("drepcze niezgrabnie");
    set_m_in("przydreptuje niezgrabnie");
    
    //raki po jakim� czasie zdychaj� :P
    set_alarm(3000.0 + itof(random(500)), 0.0, "zdychaj");
}

void
zdychaj()
{
    //tajemniczo, bez komunikatu... ;]
    remove_object();
}
    

void enter_env(object dest, object old)
{
    int testowanie_rak = 0;
    int testowanie_gracz = 0;
    
    if (interactive(ENV(TO)) && 
        old->query_prop(ROOM_I_TYPE) == ROOM_UNDER_WATER)
    {
        testowanie_rak = TO->query_stat(SS_STR) + 
            TO->query_skill(SS_SWIM) + random(10);
        testowanie_gracz = ENV(TO)->query_stat(SS_STR) + 
            ENV(TO)->query_skill(SS_SWIM) + random(20);

        if (testowanie_rak >= testowanie_gracz)
        {
            TO->move(ENV(ENV(TO)));
            set_alarm(0.1, 0.0, &write("Pr�bujesz z^lapa^c "+
                this_object()->short(PL_BIE)+", lecz ten b^lyskawicznym "+
                "uderzeniem ogona wzbija z dna mu^l i odp^lywa kawa^lek "+
                "pod jego os^lon^a.\n"));
            saybb(capitalize(this_object()->short(PL_MIA))+"b^lyskawicznym "+
                "uderzeniem ogona wzbija z dna mu^l i odp^lywa kawa^lek "+
                "pod jego os^lon^a.\n");
            return;
        }
    }
    
    if(interactive(ENV(TO)))
    {
        set_alarm(30.0+itof(random(60)), 0.0, "zwiewa");
        return;
    }
    
    if(ENV(TO)->query_prop(ROOM_I_TYPE) != ROOM_UNDER_WATER)
    {
        foreach(string str : dest->query_exit_rooms())
        {
            if(find_object(str)->query_prop(ROOM_I_TYPE) == ROOM_IN_WATER || 
                find_object(str)->query_prop(ROOM_I_TYPE) == ROOM_UNDER_WATER)
            {
                set_alarm(30.0+itof(random(60)), 0.0, "zwiewa", TO);
                return;
            }
        }
    }
}

void zwiewa(object CO)
{
    if(!objectp(ENV(CO)))
        return;

    if(query_attack())
    {
        set_alarm(30.0+itof(random(60)), 0.0, "zwiewa", CO);
        return;
    }
    
    if(ENV(TO)->query_lodka_in())
    {
        TO->command("wyskocz do wody");
        return;
    }

    if(function_exists("create_container", ENV(CO)) == "/std/room")
    {
        tell_roombb(ENV(CO), capitalize(this_object()->short(PL_MIA))+
            " nad podziw szybko przebierajac odn^o^zami kieruje sie w "+
            "stron^e wody i ju^z po chwili znika w jej toni.\n", 0, CO);
        remove_object();
        return;
    }
    if(interactive(ENV(CO)))
    {
        if(!objectp(ENV(ENV(CO))))
            return;

        ENV(TO)->catch_msg("W najmniej spodziewanym momencie "+
            "rak zwija si^e wy^slizguj^ac ci si^e z r^ak.\n");
        tell_room(ENV(ENV(CO)), "Rak nagle zwija si^e wy^slizguj^ac z "+
            "r^ak "+QCIMIE(ENV(TO), PL_CEL) +".\n", ({ENV(CO)}));
        CO->move(ENV(ENV(CO)));
    }
}

void zaatakowany()
{
    if (ENV(TO)->query_prop(ROOM_I_TYPE) == ROOM_IN_WATER || 
             ENV(TO)->query_prop(ROOM_I_TYPE) == ROOM_UNDER_WATER)
    {
        set_alarm(0.1, 0.0, &write("Pr�bujesz zaatakowa� "+
            this_object()->short(PL_BIE)+", lecz ten b^lyskawicznym "+
            "uderzeniem ogona wzbija z dna mu^l i odp^lywa kawa^lek "+
            "pod jego os^lon^a.\n"));
        saybb(capitalize(this_object()->short(PL_MIA))+"b^lyskawicznym "+
            "uderzeniem ogona wzbija z dna mu^l i odp^lywa kawa^lek "+
            "pod jego os^lon^a.\n");
    }
    return ;
}

void ploszymy_sie(object przez_kogo)
{
     return;
}

int
jestem_rakiem()//nieborakiem
{
    return 1;
}

string
query_auto_load() //raki nie mog� pozosta� przy zaka�czaniu, zbyt �atwym zarobkiem by by�y...
{
    return 0;
}
