/**
 * Obunog - gdy jest creature
 * Mozna go zlowic na wedke jak rybe, ale potem zmienia sie i gryzie;)
 *
 * autor Cairell
 * opisy Tabakista
 * data   2009
 */

inherit "/std/creature";

inherit "/std/combat/unarmed";
inherit "/std/act/attack";
inherit "/std/act/action";
inherit "/std/act/domove";
#include <stdproperties.h>
#include <std.h>
#include <ss_types.h>
#include <macros.h>
#include <stdproperties.h>
#include <pl.h>

int czy_atakowac();
void zwiewa(object CO);

void
create_creature()
{
    ustaw_odmiane_rasy("obun^og");
    dodaj_nazwy("potw^or", "stw^or"); 
    
    random_przym(({"kostropaty", "zielonkawy", "zgni^lozielony", "brunatny"}), 
        ({"niedu^zy", "niewielki", "ma^ly", "niewyro^sni^ety"}), 2);
    
    set_long("To zbli^zone kszta^ltem do raka stworzenie znacznie przerasta "+
        "swego w^atpliwego kuzyna. W zasadzie obun^og ten, przerasta nawet "+
        "sporego bobra. Para d^lugich na prawie dwie p^edzi szczypiec wydaje "+
        "si^e bardziej smuk^la ni^z u raka, lecz niema^le rozmiary czyni^a "+
        "dra^znienie stworzenia niezbyt bezpiecznym zaj^eciem. Jego korpus "+
        "oraz segmentowany, zako^nczony szerokim wachlarzem odw^lok pokrywa "+
        "porowaty, poro^sni^ety glonami pancerz brunatnej, przechodz^acej "+
        "w ziele^n barwy. wyrastaj^ace z ^lba czu^lki poruszaj^a si^e "+
        "bezustannie, podobnie jak znajduj^ace si^e ni^zej, pod pozbawionymi "+
        "wyrazu, jednolicie czarnymi ^slepiami, szcz^eki. Ostre ^zuwaczki "+
        "l^sni^a, trudno powiedzie^c od wilgoci, czy jakiego^s rodzaju "+
        "^sliny. Po bokach pancernego korpusu wyrastaj^a paj^akowate nogi, "+
        "po pi^e^c z ka^zdej strony. Je tak^ze pokrywaj^a wodorosty i "+
        "mu^l.\n");
    
    set_gender(G_MALE);
    
    set_stats (({20+random(15),10+random(8),40+random(15),10+random(10),
                30+random(10)}));
    add_prop(CONT_I_HEIGHT, 30+random(10));
    add_prop(LIVE_I_NEVERKNOWN, 1);
    add_prop(LIVE_I_SEE_DARK, 1);
    //add_prop(LIVE_I_LIVE_UNDER_WATER, 1);
    set_random_move(60);
    set_aggressive(&czy_atakowac());
    set_attack_chance(41+random(10));
    set_npc_react(1);

    set_skill(SS_UNARM_COMBAT,  20 + random(10));
    set_skill(SS_DEFENCE,       30 + random(10));
    set_skill(SS_AWARENESS,     60 + random(10));
    set_skill(SS_BLIND_COMBAT,  70 + random(10));
    set_skill(SS_HIDE,          40 + random(10));
    set_skill(SS_SNEAK,         30 + random(10));
    
    set_attack_unarmed(0, 20, 20, W_SLASH,  100, "szczypce");
    set_attack_unarmed(0, 20, 20, W_IMPALE,  100, "szcz^eki");

    set_hitloc_unarmed(0, ({ 1, 1, 1 }), 10, "^leb");
    set_hitloc_unarmed(1, ({ 1, 1, 4 }), 40, "tu^l^ow");
    set_hitloc_unarmed(2, ({ 1, 1, 1 }), 30, "ogon");
    set_hitloc_unarmed(2, ({ 1, 1, 1 }), 20, "szczypce");
}

int
czy_atakowac()
{
    if(TP->query_rasa() != "obun^og")
        return 1;
    else
        return 0;
}

void enter_env(object dest, object old)
{
     foreach(string str : dest->query_exit_rooms())
     {
         if(find_object(str)->query_prop(ROOM_I_TYPE) == ROOM_IN_WATER || 
             find_object(str)->query_prop(ROOM_I_TYPE) == ROOM_UNDER_WATER)
         {
             set_alarm(30.0+itof(random(60)), 0.0, "zwiewa", TO);
         }
     }
}

void zwiewa(object CO)
{
    if(!objectp(ENV(CO)))
        return;

    if(query_attack())
    {
        set_alarm(30.0+itof(random(60)), 0.0, "zwiewa", CO);
        return;
    }
    
    if(ENV(TO)->query_lodka_in())
    {
        TO->command("wyskocz do wody");
        return;
    }

    if(function_exists("create_container", ENV(CO)) == "/std/room")
    {
        tell_roombb(ENV(CO), capitalize(this_object()->short(PL_MIA))+
            " nad podziw szybko przebierajac odn^ozami kieruje sie w "+
            "stron^e wody i ju^z po chwili znika w jej toni.\n", 0, CO);
        {
            remove_object();
            return;
        }
    }
}

void ploszymy_sie(object przez_kogo)
{
     return;
}
