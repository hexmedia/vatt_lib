
/* Tabliczka, made by Avard 28.05.06 */
inherit "/std/object.c";

#include "dir.h"
#include <pl.h>
#include <stdproperties.h>
#include <macros.h>
#include <filter_funs.h>
#include <materialy.h>
#include <object_types.h>
int przeczytaj_tabliczke();

create_object()
{
    ustaw_nazwe("tabliczka");
    dodaj_przym("niewielki","niewielcy");
    dodaj_przym("drewniany","drewniani");

    set_long("Napis na tabliczce g�osi: \n\"Ja, Nikodemus var Estewen, "+
        "kupiec z miasta Gors Velen, ufundowa�em ten szpital w podzi�ce "+
        "nieznanemu mi medykowi, kt�ry od niechybnej �mierci mi� wybawi�, "+
        "gdym si�, ranion i wykrwawiony, do bram miejskich ostatkiem si� "+
        "dowl�k�.\"\n");

    add_item("napis na", "Napis na tabliczce g�osi: \n\"Ja, Nikodemus var Estewen, "+
        "kupiec z miasta Gors Velen, ufundowa�em ten szpital w podzi�ce "+
        "nieznanemu mi medykowi, kt�ry od niechybnej �mierci mi� wybawi�, "+
        "gdym si�, ranion i wykrwawiony, do bram miejskich ostatkiem si� "+
        "dowl�k�.\"\n", PL_MIE);
    add_cmd_item("napis na tabliczce", "przeczytaj",
        "Napis na tabliczce g�osi: \n\"Ja, Nikodemus var Estewen, "+
        "kupiec z miasta Gors Velen, ufundowa�em ten szpital w podzi�ce "+
        "nieznanemu mi medykowi, kt�ry od niechybnej �mierci mi� wybawi�, "+
        "gdym si�, ranion i wykrwawiony, do bram miejskich ostatkiem si� "+
        "dowl�k�.\"\n");

    add_prop(OBJ_I_WEIGHT, 500);
    add_prop(OBJ_I_VOLUME, 1000);
    add_prop(OBJ_I_VALUE, 50);
    add_prop(OBJ_M_NO_SELL, 1);
    add_prop(OBJ_I_NO_GET, "Niestety tabliczka jest zbyt mocno "+
        "przybita do ^sciany aby^s "+
        "mog^l"+this_player()->koncowka("","a")+
        " j^a wzi^a^c.\n");
    ustaw_material(MATERIALY_DR_BUK);
    set_type(O_INNE);
    
    add_cmd_item("tabliczk^e", "przeczytaj", przeczytaj_tabliczke, "Przeczytaj co?\n");
}

int
przeczytaj_tabliczke()
{
   return long();
}