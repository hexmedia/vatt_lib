/* Menu karczmy Baszta.
   Lil 06.07.2005               */

inherit "/std/object.c";

#include "dir.h"
#include <pl.h>
#include <stdproperties.h>
#include <macros.h>
#include <filter_funs.h>
int przeczytaj_menu();

create_object()
{
    ustaw_nazwe("cennik");

    set_long(
        "\t    ______________ \n"+
 "\t   /            | \n"+
 "\t   | ELFY       | \n"+
 "\t   | WON Z      _  \n"+
 "\t   | RINDE!   \\(_)/    <--- Miesza krew!\n"+
 "\t   |            |              Mord�je nam dziecka!\n"+
 "\t   |           /|\\          Napada na karawany!\n"+
 "\t   |          / | \\          Zabija niewinnych!\n"+
 "\t   |            |            To yLF! TO yLF!\n"+
 "\t   |           / \\ \n"+
 "\t   |          /   \\          SZYSZKI ZPIERDALA� DO LASU!!!!!\n"+
 "\t   |         /     \\ \n"+
 "\t   | \n"+
 "\t   | \n"+
 "\t  / \\ \n"+
 "\t /   \\ \n\n"+
             "     _________________________________________\n    "+
             "/  \t\t    \t\t\t      \\\n   |"+
                      "\tGARNIZONOWA SERW�JE:\t\t      |\n"+
                      "   |\tbaranina\t\t49 grosz�w    |\n"+
                      "   |\tg�lasz\t\t\t26 grosz�w    |\n"+
                      "   |\twontrupka\t\t22 grosz�w    |\n"+
                      "   |\tNAPITKI:\t\t\t      |\n"+
                      "   |\twuda\t\t\t1 grosz�w     |\n"+
                      "   |\twino (CIENKUSZ!)\t4 groszy      |\n"+
                      "   |\tpiwo\t\t\t9 grosz�w     |\n"+
                      "   |\tsiepacz\t\t\t7 grosz�w     |\n"+
                      "   \\_________________________________________/\n"
                      );

    add_prop(OBJ_I_WEIGHT, 485);
    add_prop(OBJ_I_NO_GET, "Tabliczka zosta^la mocno przymocowana do �ciany.\n");
    add_prop(OBJ_I_DONT_GLANCE,1);

    add_cmd_item(({"menu","tabliczk^e","cennik"}), "przeczytaj", przeczytaj_menu,
                   "Przeczytaj co?\n");
}

int
przeczytaj_menu()
{
   return long();
}
