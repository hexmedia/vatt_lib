inherit "/std/gate.c";
#include <stdproperties.h>
#include <macros.h>
#include "dir.h"

string dlugasny();

void
create_gate()
{
    set_other_room(WSCHOD_LOKACJE+"bramae");
    set_open(0); //bo alfa..
    set_locked(1); //alfa..
    set_gate_id("wschod_rinde");
    set_pass_command(({"zach�d","w"}));
    godzina_zamkniecia(4); //alfa
    godzina_otwarcia(4); //alfa
    set_security_lvl(3); //alfa
    set_skad("z Rinde");
    dodaj_przym("pot�ny","pot�ni");
    set_long(&dlugasny());
}

string dlugasny()
{
    string str;

    str="Jest to du�a dwuskrzyd�owa brama miejska. ";

    str+= opened_or_closed_desc();
    return str+"\n";

}
