inherit "/std/door";

#include <pl.h>
#define KOD_DRZWI "piekarnia_rinde"
#define KOD_KLUCZA "drzwi_do_piekarni_rinde"
#include "dir.h"

void
create_door()
{
    ustaw_nazwe( ({ "drzwi", "drzwi", "drzwiom", "drzwi", "drzwiami",
        "drzwiach" }), PL_NIJAKI_OS);

    dodaj_przym("drewniany", "drewniani");

    set_other_room(WSCHOD_LOKACJE + "ulica5.c");
    set_door_id(KOD_DRZWI);

    set_door_desc("Drewniane drzwi prowadz� do wyj�cia z piekarni.\n");

    set_open_desc("Na po�udniowej �cianie daj� si� zauwa�y� otwarte "+
        "drewniane drzwi.\n");
    set_closed_desc("Na po�udniowej �cianie daj� si� zauwa�y� zamkni�te "+
        "drewniane drzwi.\n");

    set_pass_command("wyj�cie","przez drewniane drzwi na zewn�trz",
                       "z piekarni");

    set_key(KOD_KLUCZA);
    set_lock_name(({"zamek", "zamku", "zamkowi", "zamek", "zamkiem", "zamku"}), 0, PL_MESKI_NOS_NZYW);
}

