inherit "/std/gate.c";
#include <stdproperties.h>
#include <macros.h>
#include "dir.h"

string dlugasny();

void
create_gate()
{
    set_other_room(WSCHOD_LOKACJE+"aaa"); // alfa ;)
    set_open(0); //bo alfa..
    set_locked(1); //alfa..
    add_prop(GATE_IS_INSIDE,1);
    set_gate_id("wschod_rinde");
    set_pass_command(({"wsch�d","e"}));
    godzina_zamkniecia(4); //alfa
    godzina_otwarcia(4); //alfa
    set_security_lvl(3); //alfa
    set_skad("z Rinde");
    dodaj_przym("pot�ny","pot�ni");
    set_long(&dlugasny());
}

string dlugasny()
{
    string str;

    str="Masywne deski s� ze sob� zbite niezwykle starannie. Tylko"
        +" gdzieniegdzie zauwa�asz w�sk� szczelin�, lecz nawet przez"
        +" ni� trudno ci dostrzec co znajduje si� po drugiej stronie bramy. "
        +" Brama posiada doprawdy imponuj�ce rozmiary, z pewno�ci�"
        +" trzeba nielada si�y, aby j� otworzy�.";

    str+= opened_or_closed_desc();
    return str+"\n";

}