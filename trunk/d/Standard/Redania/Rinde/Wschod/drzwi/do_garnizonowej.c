
/* Autor: Avard
   Data : 10.08.06
   Info : Drzwi do karczmy Garnizonowej */

inherit "/std/door";

#include <pl.h>
#include "dir.h"

void
create_door()
{
    ustaw_nazwe("drzwi");
    dodaj_przym("drewniany", "drewnani");
    dodaj_przym("zniszczony","zniszczeni");

    set_other_room(WSCHOD_LOKACJE+"karczma");
    set_door_id("DRZWI_DO_KARCZMY_GARNIZONOWEJ_W_RINDE");
    set_door_desc("Stare i wyra^xnie zniszczone drzwi prowadz^a do "+
        "karczmy. Przybito do nich jak�� tabliczk�.\n");

    set_open_desc("S^a tutaj otwarte drewniane zniszczone drzwi.\n");
    set_closed_desc("S^a tutaj zamkni^ete drewniane zniszczone drzwi.\n");

    set_pass_command(({"drzwi","karczma","wej^scie do karczmy",
        "wejd^z do karczmy","przejd^z przez drzwi do karczmy"}),
        "przez drzwi do karczmy","z zewn^atrz");

    set_key("KLUCZ_DRZWI_DO_KARCZMY_GARNIZONOWEJ_W_RINDE");
    set_lock_name("zamek");
}
