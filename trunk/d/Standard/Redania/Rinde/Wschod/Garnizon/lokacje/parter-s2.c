#include "dir.h"

inherit RINDE_STD;

#include <stdproperties.h>
#include <pl.h>
#include <macros.h>

void
create_rinde()
{
    set_short("Korytarz");

    add_exit("parter-s1.c","zach�d");

    add_prop(ROOM_I_INSIDE,1);

    add_object(GARNIZON_DRZWI + "parter-s2-sala");
    add_object(GARNIZON_DRZWI + "parter-s2-podworze");
}

string
opis_drzwi1()
{
    return (present("drzwi", TO)->query_open() ? "otwarte" : "zamkni�te");
}

string
opis_drzwi2()
{
    if (present("wrota", TO)->query_open()) {
        return "Przez otwarte, drewniane wrota wida� rozleg�y teren wewn�trznego podw�rza. ";
    }
    return "W p�nocn� �cian� wprawione s� solidne, drewniane wrota. W tej chwili s� one zamkni�te. ";
}

string
dlugi_opis()
{
    string str = "Bielone �ciany tego przestronnego korytarza s� w tym miejscu utrzymane w " +
        "najlepszym porz�dku. Podobnie rzecz si� ma z po�cieran� ju� od intensywnego " +
        "u�ytkowania pod�og�, kt�ra musia�a jednak by� niedawno zamiatana. " + opis_drzwi2() +
        "W kierunku wschodnim korytarz jakby si� jeszcze troch� rozszerza�. Na jego ko�cu " +
        "znajduj� si� " + opis_drzwi1() + " naprawd� ogromne, dwuskrzyd�owe drzwi.\n";

    return str;
}
