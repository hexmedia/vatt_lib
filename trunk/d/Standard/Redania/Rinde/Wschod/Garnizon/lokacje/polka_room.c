#include "dir.h"

inherit RINDE_STD;

#include <stdproperties.h>

void
create_rinde()
{
    set_short("Pomieszczenie p�ki");

    add_prop(CONT_I_CANT_WLOZ_DO, 1);
    add_subloc("na");

    add_prop(ROOM_I_INSIDE,1);
}

string
dlugi_opis()
{
    string str = "Wn�trze p�ki. Zdecydowanie nie powiniene� si� tutaj znale��. Zg�o� b��d!!!\n";
    return str;
}
