
#include "dir.h"

inherit RINDE_STD;

#include <stdproperties.h>

string dlugi_opis();

void
create_rinde()
{
    set_short("Sto��wka");

    add_exit("kuchnia.c","kuchnia");
    add_exit("parter-n2.c","wyj�cie");

    add_prop(ROOM_I_INSIDE,1);
}

string
dlugi_opis()
{
    string str = "Pod�u�ne pomieszczenie wype�niaj� stoliki i krzes�a. "
        + "Pod wschodni� �cian� stoi bar";

    if(jest_rzecz_w_sublokacji(0, "wysoki brodaty m�czyzna"))
        str += ", a za jego lad� krz�ta si� brodaty m�czyzna. ";
    else
        str += ". ";

    if(jest_dzien())
        str += "Dzi�ki du�ym, rozmieszczonym wzd�u� po�udniowej �ciany "
            + "oknom jest tu zupe�nie jasno, a nieliczni go�cie maj� "
            + "widok na podw�rze.";
    else
        str += "Za dnia �wiat�o wpada tu przez wysokie okna rozmieszczone "
            + "wzd�u� po�udniowej �ciany, za� o tej porze jest tu po prostu ciemno.";

    str += "\n";

    return str;
}
