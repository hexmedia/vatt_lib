#include "dir.h"

inherit RINDE_STD;

#include <stdproperties.h>
#include <pl.h>

void
create_rinde()
{
    set_short("Korytarz");

    add_exit("parter-n1.c","zach�d");
    add_exit("stolowka.c","sto��wka");
    add_exit("pietro-hall.c","g�ra");
    add_exit("piwnica-hall.c","d�");

    add_prop(ROOM_I_INSIDE,1);

    add_object(GARNIZON_DRZWI + "parter-n2-kuchnia");
    add_object(GARNIZON_DRZWI + "parter-n2-podworze");
}

string
dlugi_opis()
{
    string str = "";

    // TODO: drzwi na zewn�trz

    return str;
}
