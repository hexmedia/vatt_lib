#include "dir.h"

inherit RINDE_STD;

#include <stdproperties.h>
#include <pl.h>
#include <macros.h>

void
create_rinde()
{
    set_short("Sekretariat.");
    add_prop(ROOM_I_INSIDE,1);

    add_object(GARNIZON_DRZWI + "parter-sekretariat-s1");

    dodaj_rzecz_niewyswietlana_plik(GARNIZON_OBIEKTY + "biurko");
    add_object(GARNIZON_OBIEKTY + "biurko");
    add_npc(RINDE_NPC + "kwatermistrz");
}
public string
exits_description()
{
    return "Jest tutaj wyj^scie prowadz^ace do przedsionka.\n";
}
string
opis_drzwi()
{
    if (present("drzwi", TO)->query_open()) {
        return "otwarte";
    }
    return "zamkni�te";
}

string
dlugi_opis()
{
    string str = "Niewielkie, dobrze o�wietlone pomieszczenie. Jego �ciany pomalowane s� " +
        "w ciep�ym, koralowym kolorze, nieco mo�e zbyt intensywnym i jakby nie " +
        "pasuj�cym do charakteru tego miejsca. Pod�oga u�o�ona jest z ma�ych " +
        "deseczek jasnego drewna, z ledwie widocznymi s�ojami w kolorze br�zowym. " +
       /* "We wschodniej �cianie wykuty jest prostok�tny otw�r wielko�ci drzwi " +
        "wej�ciowych. W otw�r ten " +
        "nie ma wprawionych jednak �adnych drzwi, tylko zas�oni�ty on jest " +
        "zwisaj�cymi z sufitu koralikami w kolorze ciemnoszarym, wyra�nie " +
        "kontrastuj�cym z otoczeniem. */ "Na �rodku pomieszczenia stoi ogromnych " +
        "rozmiar�w d�bowe biurko, kt�re wydaje si� by� wbite w pod�og�.\n";

    // TODO: jakie� szafki
    // TODO: fotel za biurkiem
    // TODO: dwa krzes�a dla interesant�w

    return str;
}
