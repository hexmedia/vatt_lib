#include "dir.h"

inherit RINDE_STD;

#include <stdproperties.h>
#include <pl.h>
#include <macros.h>

#define SUBLOC_SCIANA ({"�ciana", "�ciany", "�cianie", "�cian�", "�cian�", "�cianie"})

void
create_rinde()
{
    set_short("Korytarz.");

    add_exit("parter-hall.c","p�noc");
    add_exit("parter-s2.c","wsch�d");

    add_prop(ROOM_I_INSIDE,1);

    add_object(GARNIZON_DRZWI + "parter-s1-sekretariat");

    add_subloc(SUBLOC_SCIANA);

    add_subloc_prop("�ciana", SUBLOC_I_MOZNA_ODLOZ, 1);
    add_subloc_prop("�ciana", SUBLOC_I_MOZNA_POLOZ, 1);
    add_subloc_prop("�ciana", SUBLOC_I_TYP_POD, 1);

    add_object(MEBLE + "lawka_prosta_drewniana", 2, 0, SUBLOC_SCIANA);
}

int
policz_rzeczy(object* arr, string nazwa)
{
 int i, il, ile;

  ile = 0;
  il = sizeof(arr);
  for (i = 0; i < il; ++i) {
    if (arr[i]->short() ~= nazwa) {
      ++ile;
    }
  }
  return ile;
}

string
opis_pod_sciana()
{
  object *obarr = subinventory("�ciana");

  if (sizeof(obarr) != 0) {
    return " Pod �cian� widzisz " + COMPOSITE_DEAD(obarr, PL_BIE) + ".";
  }
  return "";
}

string
opis_drzwi()
{
  return (present("drzwi", TO)->query_open() ? "otwartych" : "zamkni�tych") + " drewnianych drzwi";
}

string
dlugi_opis()
{
		string str = "Korytarz odchodzi od tego miejsca w kierunkach p�nocnym i wschodnim. �ciany " +
                 "pokryte s� pop�kanym tynkiem, w niekt�rych miejscach na tyle zniszczonym, �e " +
                 "wida� pod nim czerwone, palone ceg�y. Po�udniowa �ciana wydaje si� jednak jakby " +
                 "lepiej utrzymana. Wok� " + opis_drzwi() + " bia�y tynk musia� zosta� po�o�ony " +
                 "ca�kiem niedawno, gdy� nie dostrzegasz na nim �adnych, nawet najmniejszych plamek." +
                 opis_pod_sciana() + "\n";

		return str;
}
