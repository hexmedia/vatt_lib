inherit "/std/door";

#include <pl.h>
#include "dir.h"
#include <materialy.h>
#include <object_types.h>

void
create_door()
{
    ustaw_nazwe( ({ "drzwi", "drzwi", "drzwiom", "drzwi", "drzwiami",
        "drzwiach" }), PL_NIJAKI_OS);

    dodaj_przym("prosty", "pro�ci");

    set_other_room(GARNIZON_LOKACJE + "parter-n1.c");

    set_door_id(KOD_DRZWI_D_N);

    set_door_desc("Proste, drewniane drzwi zbite z d�bowych, grubych desek. Na obrze�ach " +
                  "oraz w poprzek drzwi umocowane s� stalowe wzmocnienia.\n");

    set_open_desc("");
    set_closed_desc("");

    set_pass_command("wsch�d", "przez drewniane drzwi na wsch�d", "zza drewnianych drzwi");
//     set_pass_mess("przez drewniane drzwi na wsch�d");

    set_key(KOD_KLUCZA_D_N);
    set_lock_name(({"zamek", "zamku", "zamkowi", "zamek", "zamkiem", "zamku"}), 0, PL_MESKI_NOS_NZYW);

    ustaw_material(MATERIALY_STAL, 3);
    ustaw_material(MATERIALY_DR_DAB, 97);
    set_type(O_INNE);
}
