#include <stdproperties.h>
#include "dir.h"

inherit STREET_STD;

void create_street()
{
    set_short("Nie widzisz tam nic ciekawego");
    add_exit("bramae.c","zach�d");
    add_object(WSCHOD_DRZWI+"bramae_out.c");
}

string
dlugi_opis()
{
    string str;
    str = "W tym bardzo dziwnym miejscu czujesz, �e nie powin"+
    this_player()->koncowka("iene�","na�")+" przebywa�. W zasadzie to..."+
    " jak tu si� dosta�"+this_player()->koncowka("e�","a�")+"?\n";
    return str;
}
