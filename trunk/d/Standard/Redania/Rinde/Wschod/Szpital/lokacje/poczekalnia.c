#include "dir.h"

inherit RINDE_STD;

#include <stdproperties.h>

void create_rinde()
{
    set_short("Niewielka schludna poczekalnia");
    set_long("@@dlugi_opis");

    add_object(SZPITAL_DRZWI + "drzwi_ze_szpitala");
    add_object(SZPITAL_DRZWI + "do_gabinetu");
    add_object(SZPITAL_DRZWI + "drzwi_na_sale");

    add_exit("sala2.c","po�udnie");

    add_prop(ROOM_I_INSIDE,1);
    add_prop(ROOM_I_HIDE, -1);
}

public string
exits_description()
{
    return "Wyj^scie z poczekalni prowadzi na ulice, za^s na po^ludniu "+
           "znajduje si^e sala sypialna. S^a tutaj tak^ze drzwi do "+
           "gabinetu oraz drzwi na sale operacyjn^a.\n";
}

string dlugi_opis()
{
    string str;

    str = "Wbrew pozorom w szpitalu panuje cisza i porz�dek. Drewniana pod�oga "
          + "jest wyszorowana do czysta, a pod �cianami ustawiono �awki "
	   + "dla pacjent�w i interesant�w. Niemal ca�� wschodni� �cian� "
          + "zajmuj� du�e, dwuskrzyd�owe, wahad�owe drzwi. ";

    str += "Obok nich stoi eleganckie biurko, ";

    if(jest_rzecz_w_sublokacji(0, "energiczna siwow�osa kobieta"))
    {
        str += "przy kt�rym siedzi starsza kobieta o siwych w�osach "
           + "spi�tych w ciasny w�ze�. Pochylona nad biurkiem, tylko od "
           + "czasu do czasu zerka w stron� pacjent�w, b�d� zagadni�ta "
           + "odpowiada na pytania. Na lewo od biurka ";
    }
    else
    {
        str += "za� wcze�niej jeszcze, na lewo od biurka, ";
    }

   str += "wysokie d�bowe drzwi prowadz� do gabinetu medyka. ";
   str += "Korytarz na po�udniu prowadzi do dormitorium.\n";

   return str;
}
