inherit "/std/door";

#include <pl.h>
#include "dir.h"

void
create_door()
{
    set_open(0);
    ustaw_nazwe( ({"kotara", "kotary","kotarze","kotar�","kotar�","kotarze"}),
            ({"kotary","kotar","kotarom","kotary","kotarami","kotarach"}), PL_ZENSKI);

    dodaj_przym("ci^e^zki", "ci^e^zcy");
    set_other_room(KARCZMA_LOKACJE + "sala.c");
    set_door_id("kotara_w_sali");
    set_door_desc("Masz przed sob� ci^e^zk^a kotar^e.\n");
    set_open_desc("");
    set_closed_desc("");
    set_pass_command(({"kotara","wyj�cie"}), "do sali g��wnej", "zza kotary");

    set_pass_mess("Pod��asz na wsch^od przez kotar�.\n");
    set_fail_pass("Odsu� najpierw kotar^e.\n");

    set_open_command("odsu^n", "odsun��");
    set_close_command("zasu^n", "zasun��");

    set_open_mess("odsuwa kotar^e.\n", "Odsuwasz kotar^e.\n", "Kto^s odsuwa kotar^e z drugiej strony.\n");
    set_fail_open("Kotara jest ju� odsuni^eta.\n");

    set_close_mess("zasuwa kotar^e.\n", "Zasuwasz kotar^e.\n", "Kto^s zasuwa kotar^e z drugiej strony.\n");
    set_fail_close("Kotara jest ju^z zasuni^eta.\n");
}
