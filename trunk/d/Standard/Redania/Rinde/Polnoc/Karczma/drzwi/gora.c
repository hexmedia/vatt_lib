inherit "/std/door";

#include <pl.h>
#define KOD_DRZWI "gorakarczmy"
#include "dir.h"

void
create_door()
{
    ustaw_nazwe( ({ "drzwi", "drzwi", "drzwiom", "drzwi", "drzwiami",
        "drzwiach" }), PL_NIJAKI_OS);

    dodaj_przym("drewniany", "drewniani");

    set_other_room(KARCZMA_LOKACJE + "pokoj.c");

    set_door_id(KOD_DRZWI);

    set_door_desc("Masz przed sob^a drewniane drzwi. Niezbyt dobrze zbita "+
       "kupa desek i nic wi^ecej. Nie spostrzegasz niczego, co s^lu^zy^loby "+
       "za zamek. Te drzwi pewnie s^a zamykane tylko od jednej strony - "+
       "tej przeciwnej.\n");

    set_open_desc("");
    set_closed_desc("");

    set_pass_command("drzwi","przez drzwi do pokoju","z zewn�trz");

    //FIXME
    set_pick(88);
    set_breakdown(90);

    set_no_unlock(1);
    set_no_lock(1);
}
