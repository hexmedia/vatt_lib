/* Lil & Gregh
   02.2005     */

#include "dir.h"

inherit RINDE_STD;

#include <macros.h>
#include <stdproperties.h>
#include <filter_funs.h>
#include <ss_types.h>
#include <wa_types.h>
#include <cmdparse.h>
#include <pl.h>
#include <options.h>
#include <formulas.h>

string sprawdz_opis_okna();
string sprawdz_zawartosc_misy();

void
create_rinde()
{
//    LOAD_ERR(RINDE+"straz/admin"); admin si� zmieni�, nie wiem czy jest potrzeba �adowania tego tu..
//    jak ju� musi by� za�adowane to proponuje doda� do preloadingu.
 set_short("Sypialnia");
 set_long("Do^s^c du^zych rozmiar^ow pomieszczenie urz^adzone "+
          "zosta^lo bardzo skromnie. Nie ma si^e co dziwi^c, skoro "+
	  "zamys^lem w^la^sciciela by^lo pewnie to, by s^lu^zy^lo "+
	  "strudzonym w^edrowcom jedynie do odpoczynku. Tu^z pod "+
	  "^scianami ustawiono w rz^edzie identyczne ^l^o^zka, a przy zas^lonach "+
	  "@@sprawdz_opis_okna@@ okna "+
	  "ma^l^a szafk^e nocn^a. Po przeciwnej stronie, na wschodniej ^scianie "+
      "znajduje si^e "+
	  "wyj^scie z pomieszczenia. " +
	  "@@reszta_opisu@@");

   add_prop(ROOM_I_INSIDE,1);
    add_prop(ROOM_I_HIDE, -1);

   add_exit("gora", ({"wschod", "przez wyj^scie", "z pokoju na zachodzie"}), 0, 0);
   add_exit("gora", ({"wyj^scie", "przez wyj^scie", "z pokoju na zachodzie"}), 0, 0);
   add_object(KARCZMA_OBIEKTY+"szafka_nocna");

   add_object(KARCZMA_OBIEKTY+"okno");
   add_object(KARCZMA_OBIEKTY+"stol_spory_debowy.c");
   add_object(KARCZMA_OBIEKTY+"ksiega");  //buu lipa, nie? Powinna by� na stole, ale
                                          //wtedy nie dzia�aj� jej akcje :((((
   add_object(KARCZMA_OBIEKTY+"misa");
   add_object(KARCZMA_OBIEKTY + "lozko_proste", 4);
   dodaj_rzecz_niewyswietlana("spory d�bowy st�",1);
   dodaj_rzecz_niewyswietlana("bia�a obt�uczona misa",1);
   dodaj_rzecz_niewyswietlana("proste ^l^o^zko", 6);
}

public string
exits_description()
{
    return "Wyj^scie prowadz^ace na korytarz znajduje si^e na wschodzie.\n";
}

string
sprawdz_opis_okna()
{
    if (present("okno", TO)->query_open())
        return "otwartego";

    return "zamkni^etego";
}

string
sprawdz_zawartosc_misy()
{
    object misa = present("misa", TO);
    if (misa->query_ilosc_plynu())
        return (" " + misa->query_nazwa_plynu_dop() + ".");

    return ".";
}

string
reszta_opisu()
{
    string str;
    if(jest_rzecz_w_sublokacji(0,"spory d�bowy st�"))
    {
        str="Na ^srodku drewnianego parkietu stoi d^ebowy "+
	  "st^o^l.";
	/*if(sizeof(stol->subinventory("na")) > 0)
	{
	    str+=", na kt�rym co� le�y.";
	}
	else
	    str+=".";*/
	 // str+=", na kt�rym co� le�y."; //ehhh
    }
    else
        str="";

    if(jest_rzecz_w_sublokacji(0,"bia�a obt�uczona misa") &&
       jest_rzecz_w_sublokacji(0,"spory d�bowy st�"))
    {
        str+=" Obok niego le�y na pod�odze "+
             "misa"+sprawdz_zawartosc_misy();
    }
    else if(jest_rzecz_w_sublokacji(0,"bia�a obt�uczona misa"))
    {
        str+="Na �rodku drewnianego parkietu le�y "+
             "misa"+sprawdz_zawartosc_misy();
    }

/*	, na kt^orym spoczywa jaka^s ksi^ega. Stoi tutaj tak^ze "+
	  "misa@@sprawdz_zawartosc_misy@@\n");*/


    str+="\n";
    return str;
}
