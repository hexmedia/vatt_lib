/* Gregh & Lil 03.05 */
#include "dir.h"

inherit RINDE_STD;
inherit "/lib/pub_new";
inherit "/lib/peek";
#include <filter_funs.h>
#include <ss_types.h>
#include <stdproperties.h>
#include <wa_types.h>
#include <cmdparse.h>
#include <macros.h>
#include <pl.h>
#include <options.h>
#include <macros.h>
#include <formulas.h>
//#include <pub.h>

#include "dir.h"

string sprawdz_opis();

void
create_rinde()
{

    set_short("Za kotar^a");
    set_long("Maly pokoj, od gwaru rozmow w karczmie mozna sie tu"
			+" odgrodzic @@sprawdz_opis@@ w tej chwili kotar^a. W rogu"
			+" pomieszczenia stoi okr�g�y d�bowy st�.\n");
   //add_exit(RINDE+"polnoc/karczma/sala","kotara");
   add_peek("przez okno", POLNOC_LOKACJE + "ulica3.c");
    add_object(KARCZMA_DRZWI+"kotara.c");
	add_object(KARCZMA_OBIEKTY+"stol_za_kotara");
    add_prop(ROOM_I_INSIDE,1);

	dodaj_rzecz_niewyswietlana("d�bowy st�");

    add_drink("mleko", 0, "�wie�ego mleka", 400, 0, 8, "kubek", "wina", "Drewniany kubek.");
    add_drink("sok wi�niowy", 0, "soku wi�niowego", 400, 0, 8,
              "kubek", "wina", "Drewniany kubek.");
    add_drink("woda", 0, "krystalicznie czystej wody", 400, 0, 5, "kubek", "wina","Drewniany kubek.");
    add_drink("piwo", ({ "jasny", "ja�ni"}), "jasno-bursztynowego, " +
	"wspaniale pieni�cego si� piwa", 700, 12, 8, "kufel", "piwa",
	"Jest to kufel wykonany z krystalicznego szk�a, kt�rego uchwyt " +
	"wykonano z dobrego d�bowego drewna.",10000);
    add_drink("piwo", ({ "reda�ski", "reda�scy" }), "jasno-bursztynowego, " +
	"wspaniale pieni�cego si� piwa", 750, 17, 9, "kufel", "piwa",
	"Jest to kufel wykonany z krystalicznego szk�a, kt�rego uchwyt " +
	"wykonano z dobrego d�bowego drewna.");
    add_drink("wino", ({ "czerwony", "czerwoni" }), "czerwonego wina",
	350, 15, 21, "kubek", "wina", "Drewniany kubek.", 1500);
    add_drink("jab�ecznik", 0, "jab�ecznika, s�ynnego w ca�ej Redanii",
	400, 12, 9, "kubek", "wina", "Drewniany kubek.", 2000);
    add_drink("w�dka", ({ "zimny", "zimni" }), "w�dki", 40, 45, 215,
	"kieliszek", "w�dki", "Niewielki, wykonany ze szk�a kieliszek.", 1000);

    add_food("jajecznica z serem", 0, "jajecznicy z ��tym serem", 450, 45, 12,
	"talerz", "jajecznicy", "Jest to w miar� czysty, lekko obt�uczony "+
	                        "talerz.", 200);
     add_food("ry�", ({ "ciemny", "ciemni" }), "ry�u", 500, 45, 13,
	"miska", "ry�u", "Jest to zwyk�a, drewniana miska.", 200);
    add_food("bigos", 0, "bigosu", 500, 45, 12,
	"talerz", "ry�u", "Jest to zwyk�a, drewniana miska.", 200);
    add_food("sa�atka owocowa", 0, "sa�atki owocowej", 400, 45, 15,
	"miseczka", "sa�atki", "Jest to zwyk�a miseczka.", 200);
    add_food("zupa pomidorowa", 0, "zupy pomidorowej", 770, 45, 12,
	"miska", "zupy", "Jest to zwyk�a, drewniana miska.", 200);

    add_wynos_food("kie�basa", ({ ({ "ja�owcowy" }), ({ "ja�owcowi" }) }),
	300, 5, "Zrobiona z jakiego� biednego zwierzaka.\n", 2400, ({ "zepsuty", "zepsuci" }));
    add_wynos_food("chleb", ({ ({"�ytni"}), ({"�ytni"}) }), 800, 6, "Bochenek chleba.\n",
        2500, ({ "sple�nia�y", "sple�niali" }));

}
public string
exits_description()
{
    return "Wyj^scie st^ad prowadzi do g^l^ownej sali.\n";
}

string
sprawdz_opis()
{
    if (present("kotara", TO)->query_open())
        return "odsuni^eta";

    return "zasuni^eta";
}


 void init()
{
    ::init();
    init_peek();
        init_pub();
}
