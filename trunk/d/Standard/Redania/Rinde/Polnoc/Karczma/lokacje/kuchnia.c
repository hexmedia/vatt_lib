#include "dir.h"

inherit RINDE_STD;
#include <macros.h>
#include <stdproperties.h>

void
create_rinde()
{
    set_short("Kuchnia.");
    set_long("W kuchni jak to w kuchni. Jak jest ka^zdy widzi.\n");

    add_exit("sala", "poludnie", 0, 0);
    add_exit("mieszkanie", "zachod", 0, 0);
    add_exit("spizarnia", "dol", 0, 0);
    add_prop(ROOM_I_INSIDE,1);
    add_object(RINDE_NPC + "pomagacz_damira");
}
public string
exits_description()
{
    return "Na po^ludniu znajduje si^e wej^scie do g^l^ownej sali karczmy"+
           ", na zachodzie mieszkanie gospodarza, za^s na dole spi^zarnia.\n";
}

