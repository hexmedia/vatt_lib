/*    Taras karczmy Baszta, Rinde.
              Lil & Alcyone 16.6.2005 */

//Chujowy opis, wiem. L.

#include "dir.h"

inherit RINDE_STD;

#include <stdproperties.h>
#include <mudtime.h>
#include <macros.h>


int wyskocz_przez_taras(string str);

void
create_rinde()
{
    int ind;
    mixed exits;

    set_short("Ma�y taras");
    set_long("@@dlugi_opis@@");

// add_sit("przy stole", "z hukiem przy stole", "od sto^lu", 2);
    add_sit("na pod�odze", "na pod�odze", "z pod�ogi", 0);
    add_exit("gora", "polnoc", 0, 0);
    //add_exit(POLNOC_LOKACJE + "ulica4", "d", 0, 1, 0);
    //Po co przeskakiwac barierke jezeli mozna zejsc bez straty hp na dol?
    // Avard
    //By ci co siedza w glownej sali nie widzieli
    //Cairell
    add_prop(ROOM_I_INSIDE,0);

    add_object(MEBLER+"krzeslo_piekarnia");
    add_object(MEBLER+"krzeslo_piekarnia");
    add_object(MEBLER+"stolik_balkonowy");
    dodaj_rzecz_niewyswietlana("drewniane krzes�o", 2);
    dodaj_rzecz_niewyswietlana("niewielki stolik", 1);

    add_item(({"barierk^e", "balustrad^e"}), "Proste, zgrabnie wyci^ete "+
        "deseczki utrzymywane s^a przez poprzeczny, rozpi^etym mi^edzy "+
        "kamiennymi s^lupkami dr^ag. Zdrowe drewno lekko tylko zd^azy^lo "+
        "pociemnie^c odk^ad znosi^c musi kaprysy pogody. Same s^lupki "+
        "obt^luczone i upstrzone tym co zostawi^ly ptaki, wydaj^a si^e "+
        "bardziej leciwe, lecz nadal ^swietnie pe^lni^a sw^a rol^e. W dole za "+
        "barierk^a dostrzec mo^zna wcale nie tak odleg^le kamienie ulicznego "+
        "bruku.\n");
        
    add_item(({"wzory"}), "P^ekni^ecia i rysy znacz^a leiwe kamienie "+
         "cierpliwie znosz^ace niepogod^e oraz buty mieszka^nc^ow i "+
         "przybysz^ow. S^adz^ac po tym co owe szczeliny wype^lnia, "+
         "oszcz^edzono im przynajmiej cz^estej konieczno^sci wytrzymywnia "+
         "zabieg^ow szmaty czy miot^ly.\n");

   // wyciag z szyszki ]:>
 /*   exits = find_object(POLNOC_LOKACJE + "ulica4")->query_exit();
    if (!sizeof(exits)) {
        return;
    }
    for (ind = 0; ind < sizeof(exits); ind += 3) {
        if ((exits[ind+1] ~= "p�noc") ||
                (exits[ind+1] ~= "p�nocny-wsch�d") ||
                (exits[ind+1] ~= "wsch�d") ||
                (exits[ind+1] ~= "po�udniowy-wsch�d") ||
                (exits[ind+1] ~= "po�udnie") ||
                (exits[ind+1] ~= "po�udniowy-zach�d") ||
                (exits[ind+1] ~= "zach�d") ||
                (exits[ind+1] ~= "p�nocny-zach�d")) {
            add_exit(exits[ind], exits[ind+1], 1, 1, 1);
                    }
    }*/


}
public string
exits_description()
{
    return "Wyj^scie z balkonu jest na p^o^lnocy i prowadzi do korytarza "+
           "karczmy.\n";
}

public void
init()
{
    ::init();
    add_action(wyskocz_przez_taras,"przeskocz");
    add_action(wyskocz_przez_taras,"wyskocz");

}

int
wyskocz_przez_taras(string str)
{
    object lok = (POLNOC_LOKACJE + "ulica4");
    notify_fail("Przez co chcesz przeskoczy�?\n");

    if (!str) return 0;
    if (!strlen(str)) return 0;

    if(!parse_command(str,environment(TP),"'przez' 'barierk�'"))
    {
        return 0;
    }

    saybb(QCIMIE(TP, PL_MIA) + " przeskakuje przez barierk^e "+
        "i znika.\n");
    TP->catch_msg("Szybkim skokiem pokonujesz barierk� i l�dujesz "+
        "pokracznie na bruku.\n");
    filter(AI(find_object(POLNOC_LOKACJE+"ulica4")), 
        &interactive())->catch_msg(QCIMIE(TP, PL_MIA) + " przeskakuje "+
        "przez barierk^e balkonu karczmy i l^aduje na bruku tu^z "+
        "przed tob^a.\n");
    TP->move(POLNOC_LOKACJE + "ulica4");
    TP->do_glance(2);
    
    //TODO: uzale�ni� ilo�� zabieranych hpk�w od zr�czno�ci/akrobatyki 
    //+dodawa� expa do tej cechy i uma
    //TP->reduce_hp(0, 2000 + random(1000), 10);
    return 1;

/*
    notify_fail("Gdzie chcesz wyskoczy�?\n");
write(str+"\n");
    if(str !="przez taras" && str !="na ulic�" && str !="przez barierk�")
	return 0;


    saybb(QCIMIE(TP, PL_MIA) + " przeskakuje przez barierk^e "+
        "i znika.\n");
    write("Szybkim skokiem pokonujesz barierk� i l�dujesz pokracznie na bruku.\n");
    TP->move(POLNOC_LOKACJE + "ulica4");
    saybb(QCIMIE(TP, PL_MIA) + " przeskakuje przez barierk^e "+
        "balkonu karczmy i l^aduje na bruku tu^z przed tob^a.\n");
    TP->do_glance(TP->query_option(2));
    //TODO: uzale�ni� ilo�� zabieranych hpk�w od zr�czno�ci/akrobatyki +dodawa� expa do tej cechy i uma
    TP->reduce_hp(0, 2000 + random(1000), 10);

    return 1;*/
}

string
dlugi_opis()
{
    string str;
    int i, il, il_krzesel, il_stolikow;
    object *inwentarz;

    str =  "Metalowa por�cz, kt�r� zdobi� proste i skromne "+
        "ornamenty, dok�adnie ogradza niewielki taras. "+
        "Na jego kamiennej pod�odze znajduj� si� wyblak�e ju� "+
        "wzory. ";

    inwentarz = all_inventory();
    il_krzesel = 0;
    il_stolikow = 0;
    il = sizeof(inwentarz);

    for (i = 0; i < il; ++i) {
		if (inwentarz[i]->czy_jestem_stolikiem()) {
			++il_stolikow;
		}
		if (inwentarz[i]->test_rwopk1()) {
			++il_krzesel;
		}
	}

   if ((il_stolikow == 0) && (il_krzesel == 1))
    {str += "Pod �cian� stoi drewniane krzes�o. ";}
    if ((il_stolikow == 0) && (il_krzesel == 2))
    {str += "Pod �cian� stoj� dwa drewniane krzes�a. ";}
    if ((il_stolikow == 1) && (il_krzesel == 1))
    {str += "Pod �cian� stoi niewielki stolik i drewniane krzes�o. ";}
    if ((il_stolikow == 1) && (il_krzesel == 2))
    {str += "Pod �cian� stoi niewielki stolik i dwa drewniane krzes�a. ";}
    if ((il_stolikow == 1) && (il_krzesel == 0))
    {str += "Pod �cian� stoi niewielki stolik. ";}


   /*if (il_stolikow == 1) {
			str += "Pod �cian� stoi male�ki stolik ";
		}
   if (il_krzesel == 1)  {str += "i drewniane krzes�o. ";}
   if (il_krzesel == 2)  {str += "i dwa drewniane krzes�a. ";}*/


	  str += "Wok^o^l roztacza si^e widok na niezbyt "+
	         "zadban� i cicha ulic�, na ktor� wychodzi balkon.\n";


	return str;
}
