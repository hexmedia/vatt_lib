//opis: rina

inherit "/std/receptacle.c";

#include "dir.h"
#include <pl.h>
#include <stdproperties.h>
#include <macros.h>
#include <filter_funs.h>
#include <object_types.h>
#include <materialy.h>
#include <mudtime.h>
#include <composite.h>
#include <sit.h>

void
create_receptacle()
{
    ustaw_nazwe("kufer");
    dodaj_przym("d^ebowy", "d^ebowi");
    
    set_long("@@dlugi_opis@@");
    
    add_cmd_item(({"litery na kufrze", "napis na kufrze"}), ({"przeczytaj",
    "obejrzyj", "ob"}), "Z trudem daje si^e odczyta^c napis jakiego^s " +
    "dowcipnisia: \"Tu by^lem\".\n");

    if (this_object()->query_prop(CONT_I_CLOSED) == 1)
    make_me_sitable("na", "na niewielkim d^ebowym kufrze",
        "z niewielkiego d^ebowego kufra", 1, PL_MIE, "na");

    ustaw_material(MATERIALY_DR_MAHON);
    
    add_prop(OBJ_I_WEIGHT, 15000);
    add_prop(OBJ_I_VOLUME, 12000);
    add_prop(CONT_I_MAX_VOLUME, 110000);
    add_prop(CONT_I_MAX_WEIGHT, 100000);
    add_prop(CONT_I_DONT_SHOW_CONTENTS, 1);
    add_prop(CONT_I_RIGID, 1);
    set_owners(({DAMIR,RINDE_NPC+"pomagacz_damira"}));

}

public int
query_type()
{
    return O_MEBLE;
}

string
dlugi_opis()
{
    string str = "D^ebowe, zaokr^aglone wieko kufra przeci^ete po^srodku " +
    "czarnym okuciem, przykrywa niewielkich rozmiar^ow kwadratow^a " +
    "skrzyni^e, kt^ora tak^ze zosta^la porz^adnie wzmocniona. Zamykany jest "+
    "jedynie na niewielk^a zasuwk^e ze stali. Przy zamku kto^s wydrapa^l " +
    "malutkie, ko^slawe litery. ";

    if (this_object()->query_prop(CONT_I_CLOSED) == 0)
    {
    switch (sizeof(all_inventory(this_object())))
    {
        case 0:
            str += "\n";
            break;
            
        case 1:
            str += "W ^srodku znajduje si^e " +
            this_object()->subloc_items()[..-1] + ".\n";
            break;
            
        default:
            str += "W ^srodku znajduj^a si^e " +
            this_object()->subloc_items()[..-1] + ".\n";
            break;
    }
    }
    
    return str;
}
