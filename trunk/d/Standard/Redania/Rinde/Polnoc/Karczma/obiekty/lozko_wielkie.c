//opis: rina

inherit "/std/object.c";

#include "dir.h"
#include <pl.h>
#include <stdproperties.h>
#include <macros.h>
#include <filter_funs.h>
#include <materialy.h>
#include <object_types.h>

create_object()
{
    ustaw_nazwe("^lo^ze");

    dodaj_przym("wielki", "wielcy");
    dodaj_przym("d^ebowy", "d^ebowi");
   set_type(O_MEBLE);
   ustaw_material(MATERIALY_WELNA, 60);
   ustaw_material(MATERIALY_DR_DAB, 40);
   make_me_sitable("na","na wielkim d^ebowym ^lo^zu","z wielkiego d^ebowego ^lo^za",7, PL_MIE, "na");
    set_long("Masywne d^ebowe ^lo^ze kr^oluje wielko^sci^a w ca^lym " +
    "pomieszczeniu. Pewnie stoi na grubych, prostych i solidnych nogach. " +
    "Puszysty materac nakryty jest ciemnoczerwon^a narzut^a ze z^lotawym " +
    "haftem li^sci.\n");
    
    add_prop(OBJ_I_WEIGHT, 250000);
    add_prop(OBJ_I_VOLUME, 120000);
    set_owners(({DAMIR,RINDE_NPC+"pomagacz_damira"}));

}
