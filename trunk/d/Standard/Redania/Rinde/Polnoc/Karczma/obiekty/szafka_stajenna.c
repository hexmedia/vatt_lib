/* Szafka w stajni gospody Baszta, Rinde.
   Alcyone & Lil   */

#pragma strict_types

inherit "/std/container";

#include <pl.h>
#include <macros.h>
#include <filter_funs.h>
#include <stdproperties.h>
#include <composite.h>                                                         
#include <cmdparse.h>
#include "dir.h"

string czy_cos_jest_na_szafce();

static int newLine;

nomask void
create_container()

{
   ustaw_nazwe( ({"szafka","szafki","szafce","szafke","szafka","szafce"}),  
        ({"szafki","szafek","szafkom","szafki","szafkami","szafkach"}),
                                       PL_ZENSKI);
	           			       
   set_long("Drewniana szafka wykonana jest z bardzo jasnego drewna, "+
            "kt�re niezwykle intensywnie pachnie przywodz�c na my�l las.\n" +  
            "@@czy_cos_jest_na_szafce@@");

   setuid();
   seteuid(getuid());

   //add_prop(CONT_I_CLOSED, 1);
   //add_prop(CONT_I_RIGID, 1);
   add_prop(CONT_I_MAX_VOLUME, 24000);
   add_prop(CONT_I_MAX_WEIGHT, 24000);
   add_prop(CONT_I_VOLUME, 16800);
   add_prop(CONT_I_WEIGHT, 10000);
   add_prop(OBJ_I_NO_GET, "Jest zbyt ci�ka.\n");
   add_prop(OBJ_I_DONT_GLANCE,1);
   add_prop(CONT_I_DONT_SHOW_CONTENTS, 1);
   add_prop(CONT_I_CANT_WLOZ_DO, 1);
   
   add_subloc("na");
   add_subloc_prop("na", CONT_I_MAX_WEIGHT, 8300);

   add_object(RINDE+"przedmioty/wiadro_stajenne", 1, 0, "na");
   add_object(RINDE+"przedmioty/lampa_stajenna", 1, 0, "na");
   add_object(RINDE+"przedmioty/rekawice_stajenne", 1, 0, "na");
    set_owners(({DAMIR,RINDE_NPC+"pomagacz_damira"}));

}

string
czy_cos_jest_na_szafce()
{
	newLine = 0;
   if (sizeof(subinventory("na")) > 0) {
       newLine = 1;
       return "Na ma�ym i szerokim blacie pouk�adano: "
              + this_object()->subloc_items("na") + ".\n";
   }
      if (newLine)
       {
	 return "\n";
       }
   return "";
}
/* Zmienilam plany. Niczego pod nia nie bedzie. Ale zostawiam to dla przykladu.
    Lil.
    
string
czy_cos_jest_pod_szafka()
{
   if(sizeof(subinventory("pod")) > 0) {
      newLine = 1;
      return (newLine ? " " : "") + "Za� pod szafk� dostrzegasz: "
	      + this_object()->subloc_items("pod") + ".\n";
   }
   if (newLine) {
	   return "\n";
   }
   return "";
}*/
