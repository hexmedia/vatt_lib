inherit "/std/object.c";

#include "dir.h"
#include <pl.h>
#include <stdproperties.h>
#include <macros.h>
#include <filter_funs.h>
#include <materialy.h>
#include <object_types.h>

create_object()
{
    ustaw_nazwe("^l^o^zko");

    dodaj_przym("prosty", "pro^sci");
   set_type(O_MEBLE);
   ustaw_material(MATERIALY_SLOMA, 70);
   ustaw_material(MATERIALY_DR_SOSNA, 30);
   make_me_sitable("na","na prostym ^l^o^zku","z prostego ^l^o^zka",4, PL_MIE, "na");
    set_long("W swoim wykonaniu jest do tego stopnia proste, ^ze wydaje " +
    "si^e pochodzi^c raczej z wojskowego garnizonu ni^z z pokoju karczemnego. "+
    "Twardy, w wielu miejscach po^latany siennik nakryto grubym szarym kocem. "+
    "Ca^lo^s^c mo^ze nie przedstawia si^e nader elegancko, jednak posiada " +
    "pewn^a istotn^a zalet^e - na takim ^l^o^zku nijak nie da si^e zaspa^c.\n");
    
    add_prop(OBJ_I_WEIGHT, 80000);
    add_prop(OBJ_I_VOLUME, 50000);
    set_owners(({DAMIR,RINDE_NPC+"pomagacz_damira"}));

}
