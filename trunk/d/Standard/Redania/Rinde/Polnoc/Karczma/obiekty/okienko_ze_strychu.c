inherit "/std/window";

#include <pl.h>
#include <macros.h>
#include <formulas.h>
#define KOD_OKNA "takiesobieokienkomalutkiewkarczmiebasztawmiescierindegdziegrasujawiewiorki"
#include "dir.h"

void
create_window()
{
    ustaw_nazwe("okienko");
    dodaj_przym("malutki", "malutcy");

    set_other_room(KARCZMA_LOKACJE + "podworze.c");
    set_window_id(KOD_OKNA);
    set_open(0);
    set_open_desc("");
    set_closed_desc("");
    set_window_desc("Malutkie, drewniane okienko pomalowane na br^azowy kolor. \n");

    ustaw_pietro(1);
    set_oneside(1); //�e okno jest jednostronne - z drugiej strony nie da si� przez nie wej��
    set_pass_mess("Wyskakujesz przez niedu^ze okno na zewn^atrz.");
}
