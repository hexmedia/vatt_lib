inherit "/std/window";

#include <pl.h>
#include <macros.h>
#include <formulas.h>
#define KOD_OKNA "takiesobieokienkowkarczmiebasztawmiescierindegdziegrasujawiewiorki"
#include "dir.h"

void
create_window()
{
    ustaw_nazwe("okno");
    dodaj_przym("niedu�y", "nieduzi");

    set_other_room(KARCZMA_LOKACJE + "podworze.c");
    set_window_id(KOD_OKNA);
    set_open(0);
    set_open_desc("");
    set_closed_desc("");
    set_window_desc("Niedu^ze okno ozdobionio zwiewnymi niebieskimi zaslonkami. \n");

    ustaw_pietro(1);

    set_pass_command("okno");
    set_pass_mess("Wyskakujesz przez niedu�e okno na zewn�trz.");
}
