//opis: eria

inherit "/std/object.c";

#include "dir.h"
#include <pl.h>
#include <stdproperties.h>
#include <macros.h>
#include <filter_funs.h>
#include <materialy.h>
#include <object_types.h>

create_object()
{
    ustaw_nazwe("krzes�o");

    dodaj_przym("smuk^ly", "smukli");
    dodaj_przym("elegancki", "eleganccy");
   set_type(O_MEBLE);
   ustaw_material(MATERIALY_DR_MAHON);
   make_me_sitable("na","na smuk^lym eleganckim krze�le","ze smuk^lego eleganckiego krzes�a",1, PL_MIE, "na");
    set_long("G^ladkie, filigranowe krzes^lo sprawia wra^zenie niezwykle " +
    "wygodnego mebla. Siedzenie obite jest mi^ekkim czerwonym materia^lem, " +
    "kt^ory doskonale pasuje do jego ciemnobr^azowej drewna. Wysokie, " +
    "dok^ladnie wyprofilowane oparcie jedynie na g^orze obite jest t^a sam^a "+
    "tkanin^a.\n");
    
    add_prop(OBJ_I_WEIGHT, 6000);
    add_prop(OBJ_I_VOLUME, 4000);
    set_owners(({DAMIR,RINDE_NPC+"pomagacz_damira"}));

}
