//jakas toaletka, opis: rina.

inherit "/std/container.c";

#include <pl.h>
#include <stdproperties.h>
#include <macros.h>
#include <filter_funs.h>
#include <object_types.h>
#include <materialy.h>
#include <mudtime.h>
#include <composite.h>
#include <sit.h>

#include "dir.h"

void
create_container()
{
    ustaw_nazwe("toaletka");
    dodaj_przym("jesionowy", "jesionowi");

    set_long("@@dlugi_opis@@");

    ustaw_material(MATERIALY_DR_JESION);
    
    add_prop(OBJ_I_WEIGHT, 50000);
    add_prop(OBJ_I_VOLUME, 30000);
    add_prop(CONT_I_MAX_VOLUME, 45000);
    add_prop(CONT_I_CANT_WLOZ_DO, 1);
    add_prop(CONT_I_MAX_WEIGHT, 100000);
    add_prop(CONT_I_DONT_SHOW_CONTENTS, 1);

    add_subloc("na");
    
    //FIXME dopisac dzbanek add_object(KARCZMA_OBIEKTY + "dzbanek", 1, 0, "na");
    //FIXME dopisac miske add_object(KARCZMA_OBIEKTY + "miska", 1, 0, "na");
    set_owners(({DAMIR,RINDE_NPC+"pomagacz_damira"}));

}

public int
query_type()
{
    return O_MEBLE;
}

string
dlugi_opis()
{
    string str = "Toaletka z bardzo jasnego drewna ma raczej niewielkie " +
    "rozmiary jak na mebel tego rodzaju. Wykonany jest z jesionu, " +
    "doskonale wyko^nczony i przyozdobiony z boku malunkiem " +
    "przedstawiaj^acym k^api^ac^a si^e rusa^lk^e. ";

    if ((member_array("prosta metalowa miska", all_inventory(TO)) != -1) &&
        (member_array("dzbanek", all_inventory(TO)) != -1))
    {
        str += "Na malutkim blacie ledwie mie^sci si^e prosta metalowa " +
        "miska i dzbanek. ";
        
        if (sizeof(all_inventory(TO)) > 2)
            str += "Ponadto l";
    }
    else
        if (sizeof(all_inventory(TO)) > 0)
            str += "L";

    switch (sizeof(all_inventory(this_object())))
    {
        case 0:
            str += "\n";
            break;
            
        case 1:
            str += "e^zy na nim " +
            this_object()->subloc_items("na")[..-1] + ".\n";
            break;
            
        default:
            str += "e^z^a na nim " +
            this_object()->subloc_items("na")[..-1] + ".\n";
            break;
    }
    
    return str;
}
