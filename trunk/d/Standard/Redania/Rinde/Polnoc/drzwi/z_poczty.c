/*
 * Drzwi z poczte w Rinde
 * Wykonane przez Avarda, dnia 31.05.06
 */

inherit "/std/door";

#include <pl.h>
#include "dir.h"

void
create_door()
{
    ustaw_nazwe("drzwi");
    dodaj_przym("drewniany", "drewniani");

    set_other_room(POLNOC_LOKACJE + "ulica1.c");
    set_door_id("DRZWI_NA_POCZTE_RINDE");

    set_door_desc("Zwyk^le, drewniane drzwi.\n");

    set_pass_command("wyj^scie","do wyj�cia","z poczty");
    //set_pass_mess("do wyj^scia");
    set_open_desc("");
    set_closed_desc("");

    set_key("KLUCZ_DRZWI_NA_POCZTE_RINDE");
    set_lock_name(({"zamek", "zamku", "zamkowi", "zamek", "zamkiem", "zamku"}), 0, PL_MESKI_NOS_NZYW);
}
