inherit "/std/gate.c";

#include <stdproperties.h>
#include <macros.h>
#include "dir.h"

string dlugasny();

void
create_gate()
{
    set_other_room(TRAKT_RINDE_LOKACJE + "trakt37.c");
    set_open(1);
    set_locked(0);
    add_prop(GATE_IS_INSIDE,1);
    set_gate_id("polnoc_rinde");
    set_pass_command(({"p�noc","n"}));
    godzina_zamkniecia(21);
    godzina_otwarcia(5);
    set_skad("z Rinde");
    dodaj_przym("pot�ny","pot�ni");
    set_long(&dlugasny());
}

string dlugasny()
{
    string str;

    str = "Jest to du�a dwuskrzyd�owa brama miejska. ";

    str += opened_or_closed_desc();
    return str+"\n";

}
