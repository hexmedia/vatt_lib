#include <stdproperties.h>
#include "dir.h"

inherit STREET_STD;

void create_street()
{
    set_short("Dziwne miejsce");
    add_exit("braman.c","po^ludnie");
    add_object(POLNOC_DRZWI+"braman_out.c");
}

string
dlugi_opis()
{
    string str;
    str = "W tym bardzo dziwnym miejscu czujesz, �e nie powin"+
    this_player()->koncowka("iene�","na�")+" przebywa�. W zasadzie to..."+
    " jak tu si� dosta�"+this_player()->koncowka("e�","a�")+"?\n";
    return str;
}
