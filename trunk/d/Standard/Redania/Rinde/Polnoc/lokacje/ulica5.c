/*  Skromna ulica, skromny opis... ;)
 *                                    Lil.
 */
#include <stdproperties.h>
#include <object_types.h>
#include "dir.h"

inherit STREET_STD;

void create_street()
{
	set_short("W�ska uliczka przy zak�adzie fryzjerskim");
	add_exit("ulica4.c","p^o^lnoc");
	add_exit("fryzjer.c",({"fryzjer","do zak�adu fryzjerskiego",
	                       "z zewn�trz"})); /* Tu maja byc drzwi. */
	add_exit(CENTRUM_LOKACJE + "placne.c","po^ludnie");
	add_prop(ROOM_I_INSIDE,0);

    add_sit("pod �cian�","pod �cian�","spod �ciany",0);
    add_sit("na bruku","na bruku","z bruku",0);
    add_object(POLNOC+"obiekty/tabliczka_fryzjera.c", 1, 0, "na drzwiach");
    dodaj_rzecz_niewyswietlana("niewielka drewniana tabliczka",1);

   add_subloc("na drzwiach", 0, "z drzwi");
   add_subloc_prop("na �cianie", CONT_I_CANT_ODLOZ, 1);
   add_subloc_prop("na �cianie", CONT_I_CANT_POLOZ, 1);
   add_subloc_prop("na �cianie", SUBLOC_I_MOZNA_POWIES, 1);
   add_subloc_prop("na �cianie", SUBLOC_I_DLA_O, O_SCIENNE);
   add_subloc_prop("na �cianie", CONT_I_MAX_RZECZY, 1);

}
public string
exits_description()
{
    return "Ulica prowadzi na p^o^lnoc, za^s na po^ludniu znajduje si^e "+
           "plac. Jest tutaj tak^ze wej^scie do fryzjera.\n";
}


string dlugi_opis()
{
    string str;

    str="Od placu g��wnego prosto w kierunku gospody "+
        "na p�nocy wiedzie kr�tka i bardzo w�ska "+
	"kamienna uliczka, na kt�r� pi�trowe kamienice "+
	"rzucaj� d�ugie cienie. Nieca�y s��e� dzieli "+
	"obie �ciany kamienic, przez co na sta�e zadomowi� "+
	"si� tu p�mrok. Na drzwiach w zachodniej �cianie napis "+
	"na przybitej tabliczce zach�ca ci� do skorzystania "+
	"z us�ug zak�adu fryzjerskiego.";

    str+="\n";
    return str;
}
