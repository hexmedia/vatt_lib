#include "dir.h"
inherit RINDE_STD;
inherit "/lib/store_support";
#include <stdproperties.h>
#include <macros.h>
#define REP_MAT "/d/Standard/items/materialy/"

void create_rinde()
{
    set_short("Tajny magazyn");
    set_long("Jeste� w tajnym magazynie. Nie ma st�d �adnego wyj�cia!\n");
    add_prop(ROOM_I_INSIDE,1);
    add_prop(ROOM_I_ALWAYS_LOAD, 1);
/*
    add_object(REP_MAT + "sk_swinska",4);
    add_object(REP_MAT + "stal_sztabka",4);
    add_object(REP_MAT + "zelazo_sztabka",4);
    add_object(REP_MAT + "sk_cieleca",4);
    add_object(REP_MAT + "tk_pikowa",4);
    add_object(REP_MAT + "debowe_drewno",4);
    add_object(REP_MAT + "sk_bydle",4);*/

    //add_object(RINDE_NARZEDZIA + "duzy_ciezki_mlot.c"); <--szukaj w Inv. craftsmana!
}
void                       /* wywolywane za kazdym razem, jak jakis */
enter_inv(object ob, object skad)  /* obiekt wchodzi do wnetrza tego obiektu */
{                  /* ...czyli do pokoju */

    ::enter_inv(ob, skad); /* trzeba ZAWSZE wywolac gdy redefiniujemy enter_inv */
    
    store_update(ob); /* dba o to, zeby magazynie nie bylo za duzo rzeczy */
}
void
leave_inv(object ob, object to)
{
    //::leave_env(ob,to);
    store_add_item(ob);
}
