inherit "/std/pattern";

#include <pattern.h>
#include "dir.h"

public void
create()
{
    set_pattern_domain(PATTERN_DOMAIN_BLACKSMITH);
    set_item_filename(RINDE_ZBROJE + "helmy/duzy_czarny_Mc.c");
    set_estimated_price(3045);
    set_time_to_complete(3400);
    enable_want_sizes();
}
