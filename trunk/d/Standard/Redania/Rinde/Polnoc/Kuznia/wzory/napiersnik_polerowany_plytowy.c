inherit "/std/pattern";

#include <pattern.h>
#include "dir.h"

public void
create()
{
    set_pattern_domain(PATTERN_DOMAIN_BLACKSMITH);
    set_item_filename(RINDE_ZBROJE + "napiersniki/polerowany_plytowy_Lc.c");
    set_estimated_price(6905);
    set_time_to_complete(5500);
    enable_want_sizes();
}
