inherit "/std/pattern";

#include <pattern.h>
#include "dir.h"

public void
create()
{
    set_pattern_domain(PATTERN_DOMAIN_BLACKSMITH);
    set_item_filename(BIALY_MOST_ZBROJE + "przeszywanice/pikowana_brazowa_Mc");
    set_estimated_price(1350);
    set_time_to_complete(4000);
    enable_want_sizes();
}
