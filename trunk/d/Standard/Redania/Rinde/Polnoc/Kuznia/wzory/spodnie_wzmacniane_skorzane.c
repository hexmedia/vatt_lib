inherit "/std/pattern";

#include <pattern.h>
#include "dir.h"

public void
create()
{
    set_pattern_domain(PATTERN_DOMAIN_BLACKSMITH);
    set_item_filename(RINDE_ZBROJE + "spodnie/wzmacniane_skorzane_M");
    set_estimated_price(690);
    set_time_to_complete(1500);
    enable_want_sizes();
}

