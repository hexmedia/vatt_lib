inherit "/std/pattern";

#include <pattern.h>
#include "dir.h"

public void
create()
{
    set_pattern_domain(PATTERN_DOMAIN_BLACKSMITH);
    set_item_filename(RINDE_ZBROJE + "/kolczugi/brzeczaca_czerniona_Lc.c");
    set_estimated_price(6411);
    set_time_to_complete(5000);
    enable_want_sizes();
}
