inherit "/std/pattern";

#include <pattern.h>
#include "dir.h"

public void
create()
{
    set_pattern_domain(PATTERN_DOMAIN_BLACKSMITH);
    set_item_filename(RINDE_BRONIE + "topory/krotki_zdobiony");
    set_estimated_price(3100);
    set_time_to_complete(2000);
    //enable_want_sizes();
}
