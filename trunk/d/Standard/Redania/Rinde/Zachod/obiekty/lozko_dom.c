/*
 * Autor: Rantaur
 * Data: 18.06.06
 * Opis: Lozko do domu swiatynnego w Rinde
 */

inherit "/std/object";

#include "dir.h"
#include <pl.h>
#include <macros.h>
#include <object_types.h>
#include <materialy.h>
#include <stdproperties.h>

void create_object()
{
  ustaw_nazwe("�o�e");
  dodaj_przym("du�y", "duzi");
  dodaj_przym("mi�kki", "mi�kcy");

  set_long("Stoj�ce w odleg�ym k�cie pomieszczenia �o�e, jest do��"
	   +" imponuj�cych rozmiar�w - z pewno�ci� zmiesci�y by si� na nim"
	   +" co najmniej dwie osoby. Nakryte jest bordow�, at�asow� p�achta,"
	   +" kt�ra zdaje si� odbija� ka�dy promie� �wiat�a, daj�c przy tym"
	   +" wra�enie, ze ca�a tkanina delikatnie faluje.\n");

  add_prop(OBJ_I_VALUE, 5520);
  add_prop(OBJ_I_WEIGHT, 41300);
  add_prop(OBJ_I_VOLUME, 13000);
  add_prop(OBJ_M_NO_GET, "Nie mo�esz podnie�� �o�a.\n");

  set_type(O_MEBLE);
  ustaw_material(MATERIALY_DR_DAB);

  make_me_sitable("na", "na �o�u", "z �o�a", 4);

    set_owners(({RINDE_NPC+"krepp"}));
}

init()
{
  ::init();
}

