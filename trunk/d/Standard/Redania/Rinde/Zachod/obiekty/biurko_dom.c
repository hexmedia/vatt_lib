/*
 * Autor: Rantaur
 * Data: n-ty.06.06
 * Opis: Biurko do domu swiatynnego w Rinde
 *
 * Pozdrawiam mame i wszystkich swoich znajomych! :P
 */

inherit "/std/container";

#include "dir.h"
#include <composite.h>
#include <materialy.h>
#include <macros.h>
#include <pl.h>
#include <stdproperties.h>
#include <object_types.h>

string na_biurku();

void
create_container()
{
  ustaw_nazwe("biurko");
  dodaj_przym("ciemny", "ciemni");
  dodaj_przym("drewniany", "drewniani");
  set_long("Wykonane z ciemnego drewna biurko wygl�da nader elegancko -"
	   +" z pewno�ci� nie powstydzi�by go si� �aden burmistrz. Na jego"
	   +" lewym brzegu le�y niewielki stos bialych zwoj�w, za� po"
	   +" przeciwnej stronie stoi ka�amarz wype�niony czarnym jak noc"
	   +" atramentem. Obok niego w specjalnej podstawce majestatycznie" 
	   +" spoczywa d�ugie g�sie pi�ro.\n"
	   +"@@na_biurku@@");

  add_item(({"kartki na", "zwoje na"}), "Zgrabnie u�o�one w kupk�"
	   +" pergaminy zapisane s� drobnym, ko�lawym pismem, kt�rego"
	   +" nijak jeste� w stanie odczyta�. Zdaje si�, �e to jakie�"
	   +" pisma urz�dowe lub co� w tym rodzaju...\n", PL_MIE);
  add_item("ka�amarz na", "Pe�ny ciemnogranatowego inkaustu ka�amarz"
	   +" wykonany jest z grubego szk�a uformowanego w p�aski,"
	   +" kanciasty kszta�t. Za pomoc� cienkiego rzemyka"
	   +" do��czono do� nasycony woskiem, drewniany korek,"
	   +" dzi�ki czemu ka�amarz mo�na zabra� w podr� bez obawy,"
	   +" i� jego zawarto�� wyleje si�.\n" , PL_MIE);
  add_item("pi�ro na", "Zadziwiajaco d�ugie pi�ro spoczywa"
	   +" wdzi�cznie w srebrzystej podstawce ozdobionej"
	   +" ro�linnymi wzorami. Niegdy� musia�o si� ono pi�knie prezentowa�,"
	   +" lecz teraz jest ju� nieco przy��k�e,"
	   +" a w niekt�rych miejscach dostrzegasz drobne plamki"
	   +" granatowego atramentu.\n", PL_MIE);

  add_prop(CONT_I_WEIGHT, 14600);
  add_prop(CONT_I_VOLUME, 42000);
  add_prop(OBJ_M_NO_GET, "Biurko jest zdecydowanie zbyt niepor�czne.\n");
  add_prop(CONT_I_CANT_WLOZ_DO, 1);
  add_prop(CONT_I_DONT_SHOW_CONTENTS, 1);

  add_subloc("na");
  add_subloc_prop("na", CONT_I_MAX_WEIGHT, 7000);
  add_subloc_prop("na", CONT_I_MAX_VOLUME, 1200);
  
  set_type(O_MEBLE);

  ustaw_material(MATERIALY_DR_DAB);
    set_owners(({RINDE_NPC+"krepp"}));

}

init()
{
  ::init();
}

string na_biurku()
{
  object *obiekty = this_object()->subinventory("na");
  if(sizeof(obiekty) == 0)
    return "";
  string str = "Na biurku ";
  if(sizeof(obiekty) > 1)
    str += "le�� ";
  else
    str += "le�y ";
  str += COMPOSITE_DEAD(obiekty, PL_MIA);
  str += ".\n";
  return str;
}
