/* Autor: Avard
   Opis : Arivia
   Data : 31.03.07 */

inherit "/std/key";

#include <stdproperties.h>
#include <pl.h>
#include "dir.h"

void
create_key()
{
    ustaw_nazwe("klucz");
    dodaj_przym("du^zy", "duzi");
    dodaj_przym("metalowy", "metalowi");
    set_long("Sporych rozmiar^ow metalowy klucz, zako^nczony niewielkim "+
        "otworem, jest zupelnie g^ladki, a jedyne wg^l^ebienie stanowi "+
        "male^nka grawerka, do z^ludzenia przypominaj^aca k^l^ebek nici "+
        "i ig^l^e.\n");
    set_key("KLUCZ_DRZWI_DO_KRAWCA_W_RINDE");

    set_owners(({RINDE_NPC + "krawiec"}));
}