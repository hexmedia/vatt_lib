#include <stdproperties.h>
#include "dir.h"
#include <sit.h>

inherit STREET_STD;

void
create_street()
{
    set_short("Ulica");

    add_exit("ulica4", "zach�d");
    add_exit(CENTRUM_LOKACJE + "placnw", "wsch�d");
    add_object(ZACHOD_DRZWI+"na_podworze_n.c");
    dodaj_rzecz_niewyswietlana("drewniana dwudrzwiowa brama");


    add_item(({"wie��","strzelist� wie��","�wi�tynn� wie��","bia�� wie��",
               "strzelist� bia�� wie��","bia�� strzelist� wie��",
               "�wi�tynn� bia�� wie��","�wi�tynn� strzelist� wie��"}),
               "�wi�tynna w�ska i bia�a wie�a wybija si� ponad dachy "+
               "innych budynk�w i wygl�da bez trudu przez fortyfikacj� miasta.\n");
    add_item(({"wie^z^e","smuk^l^a wie^z^e","wie^z^e ratusza",
               "smuk^la wie^z^e ratusza"}),
               "Smuk^la wie^za wyrasta dumnie spo^sr^od kamienic, "+
               "niczym palec, wskazuj^acy centrum tego miasta.\n");
}
public string
exits_description()
{
    return "Ulica prowadzi na zach^od, natomiast na wschodzie znajduje si^e "+
           "plac. Znajduje si^e tu tak^ze brama prowadz^aca na po^ludnie.\n";
}



string
dlugi_opis()
{
    string str;

    str = "Od p�nocnej cz�ci obszernego placu g��wnego biegnie ta ulica w " +
        "kierunku �wi�tynnej dzwonnicy, a przed ni� zakr�ca na po�udnie, " +
	"omijaj�c �ukiem szare kamienice ci�gn�ce si� po obu stronach. ";

    if (this_player()->query_prop(SIT_SIEDZACY))
	str += "Siedzisz";
    else
	str += "Stoisz";

    str += " dok�adnie pomi�dzy dwiema, jedynymi w mie�cie wie�ami. Na " +
        "zachodzie majaczy przed tob�";

    if (!dzien_noc())
      str += " doskonale widoczna o tej porze";

    str += " bia�a, smuk�a wie�a �wi�tynna, a na wschodzie, na �rodku placu " +
         "strzelista, ceglana wie�a, zwie�czaj�ca okaza�y budynek " +
	 "miejskiego ratusza. Obdrapane kamienice po po�udniowej " +
	 "stronie ��czy stara, skrzypi�ca brama na podw�rze, od kt�rej " +
	 "dochodzi nieprzyjemna wo� za�atwianych napr�dce potrzeb " +
	 "fizjologicznych.\n";

    return str;
}
