#include <stdproperties.h>
#include "dir.h"

inherit STREET_STD;

void
create_street()
{
	set_short("Ulica");

	add_exit("ulica4.c","p^o^lnoc");
	add_exit("ulica2.c","po^ludnie");

	add_prop(ROOM_I_INSIDE,0);
       add_item(({"wie��","strzelist� wie��","�wi�tynn� wie��","bia�� wie��",
               "strzelist� bia�� wie��","bia�� strzelist� wie��",
               "�wi�tynn� bia�� wie��","�wi�tynn� strzelist� wie��"}),
               "�wi�tynna w�ska i bia�a wie�a wybija si� ponad dachy "+
               "innych budynk�w i wygl�da bez trudu przez fortyfikacj� "+
               "miasta.\n");
       add_item(({"wie^z^e","smuk^l^a wie^z^e","wie^z^e ratusza",
               "smuk^la wie^z^e ratusza"}),
               "Smuk^la wie^za wyrasta dumnie spo^sr^od kamienic, "+
               "niczym palec, wskazuj^acy centrum tego miasta.\n");

       add_item(({"mur","ceglany mur"}), "Wysoki na oko�o dwadzie�cia "+
           "st�p, ceglany mur zatacza kr�g wok� ca�ego miasta.\n");
}
public string
exits_description()
{
    return "Ulica prowadzi na p^o^lnoc i po^ludnie.\n";
}


string
dlugi_opis()
{
    string str;

    str = "Droga prowadz�ca od placu widocznego po�udniu biegnie prosto " +
	"na niewielkim odcinku, na p�nocy gwa�townie skr�caj�c na wsch�d " +
	"i nikn�c w�r^od widocznych st�d niewysokich zabudowa�. Najbardziej " +
	"zauwa�alnym elementem krajobrazu jest tutaj okaza�a �wi�tynia " +
	"znajduj�ca si� tu� przy drodze, po zachodniej stronie. Zbudowana " +
	"z bia�ych kamiennych blok�w i zwie�czona strzelist� wie�� z " +
	"dzwonnic�, wygl�da niezwykle imponuj�co i niew�tpliwie jest jednym " +
	"z najbardziej reprezentacyjnych budynk�w miasta. Droga w tym " +
	"miejscu obsadzona jest niewielkimi, dobrze zadbanymi drzewkami i " +
	"krzewami. Nieco bardziej na zachodzie za �wi�tyni� wida� obrze�a " +
	"miasta w postaci do�� wysokiego szarego pasu mur�w miejskich. " +
	"Na p�nocy i wschodzie wznosz� si� niewysokie kamieniczki.\n";

    return str;
}
