/* Lokacja zwyk^lej alejki przed �wi�tyni� w Rinde.
 * Kamienie clonuj� sie na bie^z�co, przez add_action we�, bo chyba tak
 * lepiej jest, ni^z clone_object x999999999 :P
 *
 * Opis troch^e by Solothurn, reszta Lil. 24.08.2005 */

#include <stdproperties.h>
#include "dir.h"
#include <macros.h>

inherit STREET_STD;

int wez(string str);
int sa_kamienie = 1;

void
create_street()
{
    set_short("Alejka przed �wi�tyni�");
    set_long("Przepi^ekna prosta alejka"+
        " prowadzi do �wi�tyni boga o imieniu Kreve."+
        " Bo^znica majaczy przed tob� w ca^lej swej okaza^lo�ci,"+
        " ukazuj�c swoj� strzelist� wie^z^e oraz pos�g w kszta^lcie ognia"+
        " utkwiony kilkana�cie st^op przed masywnymi wrotami."+
        " Sama alejka jest usypana r^o^znokolorowymi kamykami, a kar^lowate,"+
        " starannie przystrzy^zone drzewka czyni� z niej przeromantyczne"+
        " miejsce dla schadzek r^o^znych par. ");

   add_event("Wiatr igra z ga^l^eziami kar^lowatych drzewek.\n");

   add_exit("ulica7.c","po^ludnie");

   add_item("alejk^e", "Usypana kolorowymi kamykami alejka ci�gnie si^e "+
		   "do wr^ot �wi�tyni Kreve. Tu^z przed ni� rozszerza si^e nieco"+
		   " na lewo i prawo od pos�gu w kszta^lcie ognia "+
		   "utkwionego na samym �rodku alejki.\n");
    add_item(({"wie^z^e","strzelist� wie^z^e","�wi�tynn� wie^z^e","bia^l� wie^z^e",
	           "strzelist� bia^l� wie^z^e","bia^l� strzelist� wie^z^e",
		   "�wi�tynn� bia^l� wie^z^e","�wi�tynn� strzelist� wie^z^e"}),
		   "�wi�tynna w�ska i bia^la wie^za wybija si^e ponad dachy "+
		   "innych budynk^ow i wygl�da bez trudu przez fortyfikacj^e miasta.\n");

    add_item(({"wie^z^e","smuk^l^a wie^z^e","wie^z^e ratusza",
               "smuk^la wie^z^e ratusza"}),
           "Smuk^la wie^za wyrasta dumnie spo^sr^od kamienic, "+
           "niczym palec, wskazuj^acy centrum tego miasta.\n");

	add_item("pos^ag","Tu^z nad masywnymi wrotami, prowadz^acymi do "+
        "wn^etrza strzelistej ^swi^atyni, rozpo^sciera swe ogniste "+
        "skrzyd^la monumentalny pos^ag b^ostwa. Owo dzie^lo kunsztu "+
        "rze^xbiarskiego przedstawia zawi^l^a scen^e wy^laniaj^acego si^e z "+
        "p^lomieni m^e^zczyzn^e o surowych rysach, zapewne samego Kreve. "+
        "Dynamiczna poza, wymagaj^aca od tw^orcy nie lada wysi^lku, "+
        "idealnie oddaje charakter patrona energii, ekspansji i "+
        "zdecydowania w dzia^laniu. Mimo i^z pos^ag wykonany zosta^l z "+
        "ch^lodnego marmuru zdaje si^e p^lon^a^c i liza^c gor^acem ^sciany "+
        "budowli, za^s ca^lo^s^c sprawia osza^lamiaj^ace wra^zenie swoim "+
        "misternym wykonaniem. \n");

    add_object(SWIATYNIA_DRZWI+"do_swiatyni.c");
    dodaj_rzecz_niewyswietlana("wielkie wrota", 1);
    add_npc(RINDE_NPC + "sonni",1);
    add_npc(RINDE_NPC + "darla",1);

}
public string
exits_description()
{
    return "Ulica prowadzi na po^ludnie, za^s na p^o^lnocy znajduje si^e "+
           "wej^scie do ^swi^atyni.\n";
}


public void
init()
{
    ::init();
    add_action(wez,"we�");
}

/*Tak, tandetnie, ale jednak znacznie spowalnia branie kamieni
 *w niesko^nczono�^c ;) */
int
daj_kamienie()
{
    sa_kamienie = 1;
    return 1;
}

int wez(string str)
{
    if((str ~= "kamie^n" || str ~= "kamie^n z alejki") && sa_kamienie)
    {
        write("Bierzesz kamie^n z alejki.\n");
        saybb(QCIMIE(this_player(), PL_MIA) + " bierze kamie^n z alejki.\n");
        clone_object(RINDE+"przedmioty/kamien")->move(this_player());
        sa_kamienie=0;
        set_alarm(2.0,0.0,&daj_kamienie());
        return 1;
    }

    return 0;
}
