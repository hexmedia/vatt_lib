inherit "/std/pattern";

#include <pattern.h>
#include "dir.h"

public void
create()
{
  set_pattern_domain(PATTERN_DOMAIN_TAILOR);
  set_item_filename(BIALY_MOST_UBRANIA +"spodnie/gustowne_czarne_Lc");
  set_estimated_price(180);
  set_time_to_complete(2000);
  enable_want_sizes();
}