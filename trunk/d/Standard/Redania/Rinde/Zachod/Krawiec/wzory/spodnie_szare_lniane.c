inherit "/std/pattern";

#include <pattern.h>
#include "dir.h"

public void
create()
{
  set_pattern_domain(PATTERN_DOMAIN_TAILOR);
  set_item_filename(RINDE_UBRANIA +"spodnie/szare_lniane_Mc.c");
  set_estimated_price(68);
  set_time_to_complete(1600);
  enable_want_sizes();
}
