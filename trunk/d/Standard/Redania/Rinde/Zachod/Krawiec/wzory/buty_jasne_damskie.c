inherit "/std/pattern";

#include <pattern.h>
#include "dir.h"

public void
create()
{
  set_pattern_domain(PATTERN_DOMAIN_TAILOR);
  set_item_filename(RINDE_UBRANIA +"buty/jasne_damskie_Sc");
  set_estimated_price(100);
  set_time_to_complete(2000);
  enable_want_sizes();
}
