inherit "/std/pattern";

#include <pattern.h>
#include "dir.h"

public void
create()
{
  set_pattern_domain(PATTERN_DOMAIN_TAILOR);
  set_item_filename(RINDE_UBRANIA +"cizemki/miekkie_skorzane_Mc");
  set_estimated_price(75);
  set_time_to_complete(1900);
  enable_want_sizes();
}
