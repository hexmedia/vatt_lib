inherit "/std/pattern";

#include <pattern.h>
#include "dir.h"

public void
create()
{
  set_pattern_domain(PATTERN_DOMAIN_TAILOR);
  set_item_filename(RINDE_UBRANIA +"onuce/czyste_biale_Mc");
  set_estimated_price(20);
  set_time_to_complete(300);
  enable_want_sizes();
}