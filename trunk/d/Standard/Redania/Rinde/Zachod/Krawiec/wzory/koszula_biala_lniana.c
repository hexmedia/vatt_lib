inherit "/std/pattern";

#include <pattern.h>
#include "dir.h"

public void
create()
{
  set_pattern_domain(PATTERN_DOMAIN_TAILOR);
  set_item_filename(RINDE_UBRANIA +"koszule/biala_lniana_Mc.c");
  set_estimated_price(63);
  set_time_to_complete(1800);
  enable_want_sizes();
}
