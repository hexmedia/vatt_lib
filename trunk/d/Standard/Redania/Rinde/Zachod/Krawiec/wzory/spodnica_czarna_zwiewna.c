inherit "/std/pattern";

#include <pattern.h>
#include "dir.h"

public void
create()
{
  set_pattern_domain(PATTERN_DOMAIN_TAILOR);
  set_item_filename(RINDE_UBRANIA +"spodnice/czarna_zwiewna_Mc.c");
  set_estimated_price(116);
  set_time_to_complete(3300);
  enable_want_sizes();
}
