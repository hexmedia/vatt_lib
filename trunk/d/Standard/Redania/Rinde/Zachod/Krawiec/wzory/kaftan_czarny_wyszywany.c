inherit "/std/pattern";

#include <pattern.h>
#include "dir.h"

public void
create()
{
  set_pattern_domain(PATTERN_DOMAIN_TAILOR);
  set_item_filename(RINDE_UBRANIA +"kaftany/czarny_wyszywany_M");
  set_estimated_price(95);
  set_time_to_complete(2100);
  enable_want_sizes();
}
