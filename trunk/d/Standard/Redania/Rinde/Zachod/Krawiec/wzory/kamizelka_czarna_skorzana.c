inherit "/std/pattern";

#include <pattern.h>
#include "dir.h"

public void
create()
{
  set_pattern_domain(PATTERN_DOMAIN_TAILOR);
  set_item_filename(RINDE_UBRANIA +"kamizelki/czarna_skorzana_Mc.c");
  set_estimated_price(189);
  set_time_to_complete(2400);
  enable_want_sizes();
}
