
/* Posag rycerza do swiatyni w Rinde
Made by Avard 02.06.06 
Opis by Tinardan*/
inherit "/std/object.c";

#include "dir.h"
#include <pl.h>
#include <stdproperties.h>
#include <macros.h>
#include <filter_funs.h>
#include <materialy.h>
#include <object_types.h>

create_object()
{
    ustaw_nazwe("posag");
    dodaj_przym("marmurowy","marmurowi");
    dodaj_przym("bia^ly","biali");

    set_long("Oblicze pos^agu jest srogie i pe^lne powagi, zupe^lnie jakby "+
        "rycerz sk^lada^l w^la^snie przysi^eg^e wierno^sci swemu bogu. "+
        "Sumiasty w^as zwiesza si^e nisko na warg^e, oczy s^a pos^epne, "+
        "zdaj^ace si^e wypatrywa^c z oddali nadci^agaj^acego wroga. Jedn^a "+
        "r^ek^a wspar^l si^e na pot^e^znym, dwur^ecznym mieczu, drug^a "+
        "dotyka delikatnie wyhaftowanego na ramieniu symbolu - bia^lej "+
        "r^o^zy na tle karmazynowego p^laszcza. Ca^la jego posta^c tchnie "+
        "zaci^eto^sci^a i zdecydowaniem, przera^za srogo^sci^a zmarszczonego "+
        "czo^la i zachwyca wspania^lo^sci^a postawy i harmonijn^a lini^a "+
        "sylwetki.\n");

    add_cmd_item(({"pos^agu","bi^alego pos^agu","marmurowego pos^agu",
        "marmurowego bia^lego pos^agu"}), "dotknij", "Dotykasz pos^agu. "+
        "Marmur wydaje si^e nadzwyczaj zimny i chropowaty.\n");

    add_prop(OBJ_I_WEIGHT, 1000000);
    add_prop(OBJ_I_VOLUME, 100000);
    add_prop(OBJ_I_VALUE, 1000);
    add_prop(OBJ_M_NO_SELL, 1);
    add_prop(OBJ_I_NO_GET, "Niestety pos^ag jest zbyt ci^e^zki. \n");
    ustaw_material(MATERIALY_MARMUR);
    set_type(O_INNE);
}