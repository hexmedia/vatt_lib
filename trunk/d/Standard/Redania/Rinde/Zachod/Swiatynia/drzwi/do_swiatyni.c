
/*
 * Drzwi prowadzace do swiatyni Kreve
 * Wykonane przez Avarda, dnia 14.03.06
 */

inherit "/std/door";

#include "dir.h"

#include <pl.h>

void
create_door()
{
    ustaw_nazwe("wrota");
    dodaj_przym("wielki", "wielcy");

    set_other_room(SWIATYNIA_LOKACJE +"przedsionek.c");
    set_door_id("DRZWI_DO_SWIATYNI");
    set_door_desc("Wielkie wrota �wi^atynne wykonano z kilku warstw grubej "+
        "stali, po czym pokryto inskrypcjami oraz rycinami "+
        "przedstawiaj�cymi siedem dni pobytu Kreve w�r^od ludzi. \n");

    set_open_desc("Na p^o^lnocnej ^scianie znajduj^a si^e otwarte "+
        "wielkie wrota.\n");
    set_closed_desc("Na p^o^lnocnej ^scianie znajduj^a si^e zamkni^ete "+
        "wielkie wrota.\n");

    set_pass_command(({"^swi^atynia", "wrota"}), "przez wielkie wrota do �wi�tyni", "ze �wi�tyni");
//     set_pass_mess("przez wielkie wrota do ^swi^atyni");

    set_pass_command(({"^swi^atynia","wrota","wejd^x do ^swi^atyni",
     "przejd^x przez wrota"}),"przez wielke wrota do �wi�tyni",
     "przez wielkie wrota z zewn�trz");

    set_key("KLUCZ_DRZWI_DO_SWIATYNI");
    set_lock_name(({"zamek", "zamku", "zamkowi", "zamek", "zamkiem", "zamku"}), 0, PL_MESKI_NOS_NZYW);
}
