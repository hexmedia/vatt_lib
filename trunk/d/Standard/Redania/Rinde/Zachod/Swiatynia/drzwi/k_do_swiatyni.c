
/* 
 * Duzy stalowy klucz
 * Do drzwi_do_swiatyni Kreve
 * Wykonany przez Avarda, dnia 14.03.06
 */

inherit "/std/key";

#include <stdproperties.h>
#include <pl.h>
#include "dir.h"

void
create_key()
{
    ustaw_nazwe("klucz");

    set_long("Dosy� du�y klucz o rze^xbionym oku i dziwacznym ksztalcie. \n");
    
    dodaj_przym("du^zy", "duzi");
    dodaj_przym("dziwaczny", "dziwaczni");
    
    set_key("KLUCZ_DRZWI_DO_SWIATYNI");

set_owners(({RINDE_NPC + "krepp"}));
}