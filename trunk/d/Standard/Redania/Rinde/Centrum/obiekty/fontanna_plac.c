inherit "/std/object.c";

#include "dir.h"
#include <pl.h>
#include <stdproperties.h>


create_object()
{
    ustaw_nazwe("fontanna");

    set_long("Cembrowina fontanny jest szeroka i omsza�a, ale "+
	     "znajduj�ca si^e w niej woda kusi ch�odem, �wie�o�ci� "+
	     "i przezroczysto�ci�. Centralnie ustawiona figurka "+
             "fauna o obna�onej m�sko�ci, unosz�cego jedn� nog� ku "+
	     "g�rze i lej�cego wod� z rogu trzymanego nad g�ow� "+
	     "nadaje fontannie nieco egzotyczny wygl�d. Na w�osach "+
	     "i r�kach fauna osiad� mech, a kawa�ek ogona odpad�, "+
	     "niemniej jednak fontanna wygl�da na zadban� i na "+
	     "pierwszy rzut oka mo�na si� przekona�, �e "+
             "wykona� j� nie lada artysta.\n");

    add_item("figurk^e",
             "Figurka fauna zosta^la ustawiona w samym ^srodku fontanny. Czas "+
            "pozostawi^l na niej swoje pi^etno - kawa^lek ogona po prostu "+
            "odpad^l, a na w^losach i r^ekach ch^lopca osiad^l mech. Male^nka "+
            "posta^c o obna^zonej m^esko^sci unosi w gor^e jedn^a nog^e, jak "+
            "gdyby chcia^la dosi^egn^ac kogo^s rogiem trzymanym w d^loni "+
            "i obla^c wytryskuj^ac^a z niego wod^a.\n"); 

    add_prop(OBJ_I_WEIGHT, 191255);
    add_prop(OBJ_I_VOLUME, 212553);
    add_prop(OBJ_I_VALUE, 19593);
    add_prop(OBJ_I_NO_GET, "Chyba kpisz.\n");
}

public int
jestem_fontanna_na_placu_rinde()
{
        return 1;
}
