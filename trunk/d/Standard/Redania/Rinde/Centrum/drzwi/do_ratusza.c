inherit "/std/door";

#include <pl.h>
#define KOD_DRZWI "drzwirinderatusz"
#define KOD_KLUCZA "drzwirinderatuszklucz"
#include "/d/Standard/Redania/Rinde/dir.h"
#include <stdproperties.h>

string otwarte();

void
create_door()
{
    ustaw_nazwe("drzwi");

    dodaj_przym("wielki", "wielcy");
    dodaj_przym("dwuskrzyd^lowy", "dwuskrzyd^lowi");

    set_other_room(RINDE+"Centrum/Ratusz/lokacje/korytarz.c");

    set_door_id(KOD_DRZWI);

    set_door_desc("Wielkie, ci^e^zkie drzwi z mocnego d^ebu prowadz^a " +
                  "do ^srodka ratusza. W tej chwili s^a @@otwarte:"+file_name(this_object())+"@@.\n");

    set_open_desc("");
    set_closed_desc("");

    set_pass_command("ratusz","do ratusza","z zewn�trz");

    set_key(KOD_KLUCZA);
    set_lock_name(({"zamek", "zamku", "zamkowi", "zamek", "zamkiem", "zamku"}), 0, PL_MESKI_NOS_NZYW);
    //FIXME: TU WY�AMYWANIE - tu te� by�o set_pick
    set_pick(69);
    set_breakdown(71);

    set_open(0);
    set_locked(0);
    add_prop(DOOR_I_HEIGHT, 240);
}

string
otwarte()
{
    if (this_object()->query_open())
        return "otwarte";
    else
        return "zamkni^ete";
}
