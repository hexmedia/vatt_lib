inherit "/std/object.c";

#include "dir.h"
#include <pl.h>
#include <stdproperties.h>
#include <macros.h>
#include <filter_funs.h>
#include <materialy.h>
#include <object_types.h>

create_object()
{
    ustaw_nazwe("krzes�o");

    dodaj_przym("zniszczony", "zniszczeni");
    dodaj_przym("drewniany", "drewniani");
   set_type(O_MEBLE);
   ustaw_material(MATERIALY_DR_DAB);
   make_me_sitable("na","na drewnianym krze�le","z drewnianego krzes�a",1, PL_MIE, "na");
    set_long("Drewniane krzes^lo - nie wyr^o^znia si^e niczym specjalnym "+
	  "i z pewno^sci^a zrobiono je do^s^c dawno, gdy^z br^azowa farba "+
	  "w wielu miejscach ju^z poodpada^la, zostawiaj^ac po sobie tylko brzydki "+
	  "^slad. Przez powycierany i przybrudzony materia^l przebija si^e twarda "+
	  "poduszka. Wida^c, ^ze mebel nie jest ju^z pierwszej nowo^sci i ma swoje "+
	  "lata, ale mo^zna bez wi^ekszej obawy na nim usi^a^s^c.\n");
    add_prop(OBJ_I_WEIGHT, 6300);
    add_prop(OBJ_I_VOLUME, 5990);
    add_prop(OBJ_I_VALUE, 2);
    add_prop(OBJ_M_NO_SELL, 1);

    set_owners(({RINDE_NPC + "sekretarz"}));
}
