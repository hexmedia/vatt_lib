//opis: eria

inherit "/std/object.c";

#include "dir.h"
#include <pl.h>
#include <stdproperties.h>
#include <macros.h>
#include <filter_funs.h>
#include <materialy.h>
#include <object_types.h>

create_object()
{
    ustaw_nazwe("krzes�o");

    dodaj_przym("elegancki", "eleganccy");
    dodaj_przym("d^ebowy", "d^ebowi");
   set_type(O_MEBLE);
   ustaw_material(MATERIALY_DR_DAB);
   make_me_sitable("na","na eleganckim d^ebowym krze�le","z eleganckiego d^ebowego krzes�a",1, PL_MIE, "na");
    set_long("To krzes^lo pomalowano na jasnobr^azowy kolor, spod kt^orego "+
    "miejscami przebija naturalna, ciemna barwa wysokogatunkowego d^ebu. " +
    "Posiada wygodne oparcie pokryte rze^xbionymi motywami ro^slinnymi, " +
    "z czego wi^ekszo^s^c to pn^acza, a gdzieniegdzie mo^zna dostrzec " +
    "drobne kwiaty. Mebel wygl^ada na stabilny i bez wi^ekszych obaw mozna " +
    "si^e na nim rozsi^a^s^c.\n");
    
    add_prop(OBJ_I_WEIGHT, 6000);
    add_prop(OBJ_I_VOLUME, 5000);
    add_prop(OBJ_I_VALUE, 2);
    add_prop(OBJ_M_NO_SELL, 1);

    set_owners(({RINDE_NPC + "sekretarz"}));
}
