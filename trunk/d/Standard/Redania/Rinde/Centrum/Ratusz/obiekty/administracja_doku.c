// wlasciwie to dziwna ta tresc troche... ma ktos pomysl?

#include "dir.h"
#include <macros.h>
#include <stdproperties.h>

inherit "/std/object.c";
inherit "/lib/keep.c";

create_object()
{
    ustaw_nazwe("dokument");
    set_long("Jaki^s dokument napisany niedba^lym pismem. Tylko pojedyncze " +
    "wyrazy s^a mo^zliwe do odczytania.\n");
    
    set_keep(1);
}

void
init()
{
    ::init();
    add_action("przeczytaj", "przeczytaj");
}

int
przeczytaj(string str)
{
    object kartka;

    notify_fail("Przeczytaj co?\n");

    if (!str || !parse_command(str, this_player(), "%o:" + PL_BIE, kartka) || kartka != this_object())
	return 0;

    this_player()->more("Udaje ci si^e wy^lowi^c jedynie kilka s^l^ow: " +
    "\"Neville\", \"Rinde\", \"kupieckiego\", \"niezw^locznie\"...\n");
    return 1;
}
