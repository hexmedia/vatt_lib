inherit "/std/door";

#include <pl.h>
#define KOD_DRZWI "drzwirinderatuszadministracja"
#define KOD_KLUCZA_ADMINISTRACJA "drzwirinderatuszadministracjaklucz"
#include "dir.h"

void
create_door()
{
    ustaw_nazwe("drzwi");
    dodaj_przym("d^ebowy", "d^ebowi");
    dodaj_przym("solidny","solidni");

    set_other_room(RINDE + "Centrum/Ratusz/lokacje/korytarz.c");

    set_door_id(KOD_DRZWI);

    set_door_desc("Solidne drzwi z d^ebu, zabezpieczone mocnym zamkiem.\n");

    set_open_desc("");
    set_closed_desc("");

    set_pass_command(({"wyj^scie", "drzwi","korytarz"}),"przez drzwi na korytarz",
                        "przez otwarte drzwi");

    set_key(KOD_KLUCZA_ADMINISTRACJA);
    set_lock_name(({"zamek", "zamku", "zamkowi", "zamek", "zamkiem", "zamku"}), 0, PL_MESKI_NOS_NZYW);

    set_lock_desc("Spory, stalowy zamek od drzwi wygl^ada na do^s^c mocny.\n");

    //FIXME: TU WY�AMYWANIE - tu te� by�o set_pick
    set_pick(59);
    set_breakdown(65);

    set_open(0);
}
