inherit "/std/door";

#include <pl.h>
#define KOD_DRZWI "drzwirinderatuszburmistrz"
#define KOD_KLUCZA_BURMISTRZ "drzwirinderatuszburmistrzklucz"
#include "dir.h"

void
create_door()
{
    ustaw_nazwe("drzwi");

    dodaj_przym("drewniany", "drewniani");
    dodaj_przym("solidny","solidni");

    set_other_room(RINDE + "Centrum/Ratusz/lokacje/korytarz_gora.c");

    set_door_id(KOD_DRZWI);

    set_door_desc("D�bowe drzwi wzmacniane metalowymi okuciami wygl�daj�"+
                  " na do�� mocne.\n");

    set_open_desc("");
    set_closed_desc("");

    set_pass_command( ({({"drzwi", "wyj^scie"}), "przez drzwi do wyj�cia",
                     "z gabinetu burmistrza"}));

    set_key(KOD_KLUCZA_BURMISTRZ);
    set_lock_name(({"zamek", "zamku", "zamkowi", "zamek", "zamkiem", "zamku"}), 0, PL_MESKI_NOS_NZYW);

    set_pick(78);
    set_breakdown(80);

    set_open(0);
    set_locked(1);

    set_lock_desc("Stalowy zamek od drzwi, cho^c ma^ly, wygl^ada na do^s^c mocny.\n");
}
