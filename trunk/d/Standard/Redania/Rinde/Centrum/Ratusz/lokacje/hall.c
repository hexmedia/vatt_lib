/* Hall ratusza w Rinde.
 * Lil, a szkielet opisu napisany przez Rivo.
 */

#include "dir.h"

inherit RINDE_STD;

#include <stdproperties.h>
#include <filter_funs.h>

string dlugi_dlugasny();

void create_rinde()
{
    set_short("Miejski ratusz");
    set_long(&dlugi_dlugasny());
    add_exit("korytarz.c",({({"korytarz", "wyj�cie"}),"na korytarz","z G^l^ownego Hallu"}));
    add_prop(ROOM_I_INSIDE,1);

    add_object(RINDE+"Centrum/Ratusz/obiekty/hall_podium");
    add_object(RINDE+"Centrum/Ratusz/obiekty/hall_lawa", 8);

    /*nie dodaje rzeczy niewyswietlanych, bo one maja propa DONT_GLANCE
     * dlatego, ze i tak maja NO_GET :P */
    add_item(({"okno","okna"}),
        "Umieszczone tu� pod sufitem okna maj� kszta�t prostok�t�w. "+
        "S� zbyt wysoko, by mo�na by�o przez nie wyjrze�.\n");
    add_item("�yrandol",
        "Rzemie�lnik tworz�cy owe dzie�o musia� mie� "+
        "dusz� artysty, gdy� �yrandol w ca�o�ci pokryty "+
        "jest przepi�knymi, mosi�nymi zdobieniami. "+
        "Pocz�wszy od najmniejszego ko�a na samej g�rze, ko�cz�c "+
        "na obwodzie tego najwi�kszego i najni�ej usytuowanego.\n");
}

public string
exits_description()
{
    return "Wyj^scie st^ad prowadzi na korytarz.\n";
}


string
dlugi_dlugasny()
{
    string str;

    str="Ta przestronna izba zwana jest przez mieszczan "+
        "G��wnym Hallem. Ci�gn�ce si� rz�dami �awy "+
	"si�gaj� a� do kra�ca sali, gdzie ust�puj� "+
	"miejsca szerokiej konstrukcji stanowi�cej podium. "+
	"Ca�o�� utrzymana jest bardzo schludnie. ";

    if(!dzien_noc())
       str+="Promienie s�oneczne wlewaj� si� do "+
            "wn�trza poprzez liczne okna umieszczone pod "+
	    "sufitem w bia�awych �cianach. Ca�ej skromnej "+
	    "kompozycji hallu sprzeciwia si� bogato zdobiony "+
	    "�yrandol w kszta�cie "+
	    "ko�a, wisz�cy na samym �rodku sklepienia.";
    else
       str+="Ciemne k�ty sali roz�wietla �yrandol w kszta�cie "+
            "ko�a. Wraz z nik�ym �wiat�em zza okien tworzy "+
	    "wspania�� gr� cieni.";

    str+=" W tej chwili ";

    if(sizeof(FILTER_LIVE(all_inventory()))>=16)
      str+="sala jest wype�niona po brzegi.";
    if(sizeof(FILTER_LIVE(all_inventory()))>=3 &&
       sizeof(FILTER_LIVE(all_inventory()))<=15)
      str+="kr�ci si� tutaj kilka os�b.";
    if(sizeof(FILTER_LIVE(all_inventory()))<=2)
      str+="jest tu zupe�nie pusto.";

    str+="\n";
    return str;
}