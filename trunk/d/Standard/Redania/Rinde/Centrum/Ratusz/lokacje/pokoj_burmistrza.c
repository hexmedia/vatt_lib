// opis: faeve + yran

#include "dir.h"
inherit RINDE_STD;
inherit "/lib/peek";

#include <macros.h>
#include <stdproperties.h>


#define SCIEZKA "/d/Standard/Redania/Rinde/Centrum/Ratusz/obiekty/"
#define JEST(x) jest_rzecz_w_sublokacji(0, x)

string dlugi_opis();

void create_rinde()
{
    set_short("Pok^oj burmistrza");
    set_long(&dlugi_opis());

    add_prop(ROOM_I_INSIDE, 1);
    add_prop(ROOM_I_HIDE, -1);

    add_object("/d/Standard/Redania/Rinde/Centrum/Ratusz/drzwi/pokoj_burmistrza");
    add_object(SCIEZKA + "pokoj_burmistrza_biurko");
    add_object(SCIEZKA + "pokoj_burmistrza_fotel");
    add_object(SCIEZKA + "pokoj_burmistrza_krzeslo");
    add_object(SCIEZKA + "pokoj_burmistrza_swiecznik_m");
    add_object(SCIEZKA + "pokoj_burmistrza_swiecznik_d");
    add_object(SCIEZKA + "pokoj_burmistrza_okno");
    add_object(SCIEZKA + "pokoj_burmistrza_regal");
 // add_object(SCIEZKA + "pokoj_burmistrza_wazon");
 // add_object(SCIEZKA + "pokoj_burmistrza_szafka");

    dodaj_rzecz_niewyswietlana("wielkie d^ebowe biurko", 1);
    dodaj_rzecz_niewyswietlana("eleganckie d^ebowe krzes^lo", 1);
    dodaj_rzecz_niewyswietlana("ciemny sk^orzany fotel", 1);
    dodaj_rzecz_niewyswietlana("ma^ly srebrny ^swiecznik", 3);
    dodaj_rzecz_niewyswietlana("du^zy pi^ecioramienny ^swiecznik", 3);
    dodaj_rzecz_niewyswietlana("wysokie okno", 1);
    dodaj_rzecz_niewyswietlana("wielki hebanowy rega^l", 1);

    add_item("^zyrandol",
        "Roz^lo^zysty ^zyrandol wisi wysoko nad twoj^a g^low^a i " +
        "roz^swietla izb^e.\n");

    add_cmd_item(({"^zyrandol"}), ({"we^x", "we^x ^zyrandol"}),
        ({"^Zyrandol wisi zbyt wysoko.\n", "^Zyrandol wisi zbyt wysoko.\n"}));

    add_item(({"pod^log^e", "klepk^e", "dywan"}),
        "Sosnowy parkiet pokoju zosta^l po^srodku przys^loni^ety " +
        "be^zowo-br^azowym dywanem.");

    add_item(({"sufit", "belki"}),
        "Znajduj^acy si^e na sporej wysoko^sci sufit podbity jest belkami, " +
        "co sprawia, ^ze pomieszczenie wydaje si^e by^c do^s^c wysokie i " +
        "obszerne, ale tak^ze wyj^atkowo przytulne.\n");

    add_item(({"^sciany", "^scian^e"}),
        "Ca^la zachodnia ^sciana pomieszczenia zastawiona jest wielkim " +
        "hebanowym rega^lem, na kt^orym burmistrz gromadzi obszerne teczki " +
        "dokument^ow.\n");

    add_sit("na parkiecie", "na parkiecie", "z parkietu", 0);
    add_sit("na dywanie", "na be^zowo-br^azowym dywanie", "z br^azowo-be^zowego dywanu", 10);

    set_event_time(240.0);
    add_event("Pod^loga poskrzypuje cicho pod twoimi stopami.\n");
    add_event("Zza okna dobiegaj^a ci^e nawo^lywania straganiarzy.\n");
    add_event("Jaki^s w^oz przeje^zd^za pod ratuszem, stukaj^ac ko^lami o kocie ^lby.\n");

    add_peek("przez okno", "/d/Standard/Redania/Rinde/Centrum/lokacje/placs.c");
    add_peek("przez okna", "/d/Standard/Redania/Rinde/Centrum/lokacje/placs.c");
}

public string
exits_description()
{
    return "Wyj^scie z gabinetu prowadzi na korytarz.\n";
}

string
dlugi_opis()
{
    string str = "Przestronna izba, w kt�rej si^e znajdujesz, jest na tyle du^za, " +
        "by pomie^sci^c grupk^e kilku interesant^ow. Znajduj^acy si^e na " +
        "sporej wysoko^sci sufit podbity jest belkami, co sprawia, ^ze " +
        "pomieszczenie wydaje si^e by^c do^s^c wysokie i obszerne, ale " +
        "tak^ze wyj^atkowo przytulne. ";

    if (JEST("wielkie d^ebowe biurko"))
    {
        str += "Naprzeciwko drzwi ma swoje miejsce wielkie, d^ebowe biurko";

        if (JEST("ciemny sk^orzany fotel") && JEST("eleganckie d^ebowe krzes^lo"))
            str += ", a po jego przeciwnych stronach stoj^a obity sk^or^a " +
                "fotel oraz eleganckie d^ebowe krzes^lo";
        else
        {
            if (JEST("ciemny sk^orzany fotel"))
                str += ", a za nim stoi obity sk^or^a sk^orzany fotel";

            if (JEST("eleganckie d^ebowe krzes^lo"))
                str += ", a przed nim stoi eleganckie d^ebowe krzes^lo";
        }
    }
    else
    {
        if (JEST("ciemny sk^orzany fotel") && JEST("eleganckie d^ebowe krzes^lo"))
            str += ". Naprzeciwko drzwi stoj^a obity sk^or^a fotel oraz " +
                "eleganckie d^ebowe krzes^lo";
        else
        {
            if (JEST("ciemny sk^orzany fotel"))
                str += "Naprzeciwko drzwi stoi obity sk^or^a fotel";

            if (JEST("eleganckie d^ebowe krzes^lo"))
                str += "Naprzeciwko drzwi stoi eleganckie d^ebowe krzes^lo";
        }
    }

    str += ". Sosnowy parkiet pokoju zosta^l po^srodku " +
        "przys^loni^ety be^zowo-br^azowym dywanem. Ca^la " +
        "zachodnia ^sciana zastawiona jest wielkim hebanowym rega^lem, " +
        "na kt^orym burmistrz gromadzi obszerne teczki dokument^ow. ";

    if (JEST("ma^ly srebrny ^swiecznik") && JEST("du^zy pi^ecioramienny ^swiecznik"))
        str += "Mimo ^ze utrzymany w dosy^c ciemnej tonacji gabinet nie " +
            "jest wcale ponury, to i tak we wszystkich mo^zliwych miejscach " +
            "kto^s poustawia^l ^swieczniki, kwiaty i inne bibeloty. ";

    if(!dzien_noc())
    {
        str += "Ponadto pomieszczenie jest bardzo dobrze o^swietlone: nie " +
            "do^s^c, ^ze u sufitu wisi roz^lo^zysty ^zyrandol, to promienie " +
            "s^lo^nca wpadaj^a tu wielkie okno na ^scianie " +
            "naprzeciw drzwi. ";

        if (JEST("niska szafka"))
        {
            str += "Pod nim za^s";

            if (JEST("wielkie d^ebowe biurko"))
                str += ", niedaleko biurka,";

            str += " stoi niewysoka szafeczka";

            if (JEST("wazon suszonych kwiat^ow"))
                str += ", na kt^orej ustawiono wazon szuszonych kwiat^ow";

            str += ". ";
        }
    }
    else
        str += "Pomieszczenie wype^lnia przyjazne ^swiat^lo zawieszonego u " +
            "sufitu roz^lo^zystego ^zyrandola. ";

    str += "\n";

    return str;
}


void
init()
{
    ::init();
    init_peek();
}
