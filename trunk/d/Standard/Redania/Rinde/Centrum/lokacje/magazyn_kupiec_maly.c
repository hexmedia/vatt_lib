#include "dir.h"

#include <stdproperties.h>

inherit RINDE_STD;
inherit "/lib/store_support";
void create_rinde()
{
    set_short("Kiesze� straganiarza");
    set_long("Jeste� w kieszeni bez dna.\n");

    add_prop(ROOM_I_INSIDE,1);

//dopoki luki niedzialaja! FIXME
//    add_object("/d/Standard/items/bronie/luki/dlugi_cisowy.c", 5);
    add_object("/d/Standard/items/bronie/strzaly/wysmukla_czerwonopiora.c", 200);
    add_object("/d/Standard/items/bronie/noze/maly_poreczny.c", 10);
    add_neverending_object("/d/Standard/items/bronie/noze/maly_poreczny.c");
    add_object("/d/Standard/Redania/Rinde/przedmioty/bronie/miecze/dlugi_stalowy.c", 3);
    add_object("/d/Standard/Redania/Rinde/przedmioty/bronie/topory/poreczny_stalowy.c", 20);
    add_object("/d/Standard/items/bronie/dzidy/prosta_jesionowa.c", 3);
    add_object("/d/Standard/items/bronie/wlocznie/prymitywna_dluga.c", 2);
}
void                       /* wywolywane za kazdym razem, jak jakis */
enter_inv(object ob, object skad)  /* obiekt wchodzi do wnetrza tego obiektu */
{                  /* ...czyli do pokoju */

    ::enter_inv(ob, skad); /* trzeba ZAWSZE wywolac gdy redefiniujemy enter_inv */
    
    store_update(ob); /* dba o to, zeby magazynie nie bylo za duzo rzeczy */
}
void
leave_inv(object ob, object to)
{
    //::leave_env(ob,to);
    store_add_item(ob);
}