#include "dir.h"

inherit RINDE_STD;
inherit "/lib/store_support";
#include <stdproperties.h>
#include <macros.h>
#define BRONIER (RINDE + "przedmioty/bronie/")
#define BRONIEO "/d/Standard/items/bronie/"
#define ZBROJEO "/d/Standard/items/zbroje/"
#define LAMPA "/d/Standard/items/narzedzia/lampa_niewielka_poreczna"

void create_rinde()
{
    set_short("Tajny magazyn");
    set_long("Jeste� w tajnym magazynie. Nie ma st�d �adnego wyj�cia!\n");
    add_prop(ROOM_I_INSIDE,1);

    /* Domy�lne rzeczy, kt�rymi handluje sklepikarz... */
    add_object("/d/Standard/items/sloik.c",30);
    add_neverending_object("/d/Standard/items/sloik.c");
    add_object(ZBROJEO+"kurtki/skorzana_mocna_Lc",3);
    add_object(ZBROJEO+"kurtki/skorzana_mocna_Lk",2);
    add_object(ZBROJEO+"kurtki/skorzana_mocna_Mc",2);
    add_object(ZBROJEO+"kurtki/skorzana_mocna_Mk",2);
    add_object(ZBROJEO+"kurtki/skorzana_mocna_Sk",2);
    add_object(ZBROJEO+"kurtki/skorzana_mocna_XLk");
    add_object(ZBROJEO+"kurtki/skorzana_mocna_Lc",3);
    add_object(ZBROJEO+"kurtki/skorzana_mocna_Lk",2);
    add_object(ZBROJEO+"kurtki/skorzana_mocna_Mc",2);
    add_object(ZBROJEO+"kurtki/skorzana_mocna_Mk",2);
    add_object(ZBROJEO+"kurtki/skorzana_mocna_Sk",2);
    add_object(ZBROJEO+"kurtki/skorzana_mocna_XLk");

    add_object(BRONIER+"miecze/miecz_szeroki_poreczny",9);
    add_object(BRONIER+"mizerykordie/zdobiona_waska",5);
    add_object(BRONIEO+"sztylety/maly_stalowy",14);
    add_object(BRONIER+"palka_debowa_gruba",9);
    add_object(BRONIER+"miecze/polyskliwy_zdobiony",4);
    add_object(BRONIER+"topory/krotki_zdobiony",3);
    add_object(BRONIER+"topory/prosty_jednoreczny.c",6);
    add_object(BRONIER+"mloty/ciezki_zelazny.c",4);
    add_object(BRONIER+"mloty/poreczny_zelazny_nadziak.c",3);
    add_object(BRONIEO+"noze/maly_poreczny",7);
    add_object(LAMPA,13);
    add_object("/std/torch",31);
    add_neverending_object("/std/torch");
    add_object("/std/olej",25);
    add_object("/std/krzemien",40);
	add_object("/std/parchment",30);
    add_neverending_object("/std/krzemien");
    add_object(RINDE+"przedmioty/narzedzia/zwykla_stalowa_siekiera.c",8);
    add_object(RINDE+"przedmioty/narzedzia/dluga_zebata_pila.c",6);

}
void                       /* wywolywane za kazdym razem, jak jakis */
enter_inv(object ob, object skad)  /* obiekt wchodzi do wnetrza tego obiektu */
{                  /* ...czyli do pokoju */

    ::enter_inv(ob, skad); /* trzeba ZAWSZE wywolac gdy redefiniujemy enter_inv */
    
    store_update(ob); /* dba o to, zeby magazynie nie bylo za duzo rzeczy */
}
void
leave_inv(object ob, object to)
{
    //::leave_env(ob,to);
    store_add_item(ob);
}