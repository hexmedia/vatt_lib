/* opis by Tinardan
               10.10.2005 Lil */
#include "dir.h"

#include <stdproperties.h>
#include <mudtime.h>
#include <macros.h>
#include <pogoda.h>

inherit STREET_STD;
inherit "/lib/drink_water";

void odnow_golebie_kwiaciarke();

string
ob_krzewy()
{
    string str = "";

    if(pora_roku() == MT_ZIMA)
    {
        str+="Wzd�u� ^scian ratusza, niczym u�pione, niewielkie stworki";
        if (CZY_JEST_SNIEG(this_object()))
            str += " przysypane warstw^a ^sniegu";
        str +=", kul� si� r�wno przyci�te krzewy.\n";
    }

    if(pora_roku() == MT_LATO || pora_roku() == MT_WIOSNA)
      str+="Wzd�u� ^scian ratusza zieleni� si� niewielkie krzewy, "+
           "r�wniutko przyci�te i poprzetykane gdzieniegdzie "+
	   "kwieciem najr�niejszego koloru.\n";

    if(pora_roku() == MT_JESIEN)
      str+="Wzd�u� ^scian ratusza odbijaj� si� br�zowo niewielkie, "+
           "r�wniutko przyci�te krzewy, w�r�d kt�rych, niczym "+
	   "ostatni stra�nik, wychyla g��wk� jaki� jesienny kwiat.\n";
}

void create_street()
{
   set_short("Przed miejskim ratuszem");

    add_item(({"plac", "rynek"}),
        "Starannie wybrukowany plac, na ^srodku kt^orego wznosi si^e " +
    "zbudowany z czerwonej ceg�y gmach ratusza, stanowi centrum tego " +
    "miasta.\n");
    add_item("ratusz",
        "Budynek ten, cho^c prezentuje si^e dumnie, nie wyr^o^znia si^e " +
    "bynajmniej architektur^a g^ornych lot^ow i nie by^lby nijak " +
    "wyj^atkowym, gdyby nie budz^aca podziw smuk^la wie^za pn^aca si^e " +
	"wysoko ponad dachy innych budowli. Ponadto szczeg^olno^sci temu " +
    "gmachowi dodaje fakt, i^z spe^lnia on niebanaln^a rol^e w ^zyciu " +
    "miasta.\n");
    add_item(({"wie^z^e", "smuk^l^a wie^z^e", "wie^z^e ratusza"}),
        "Smuk^la wie^za wyrasta dumnie spo^sr^od kamienic "+
	"niczym palec, wskazuj^acy centrum tego miasta.\n");
    add_item(({"kamienice", "stare kamienice", "pi^etrowe kamienice",
        "stare pi^etrowe kamienice"}),
	"Stare, ponure kamienice wybudowane tutaj zosta^ly na planie kwadratu, " +
    "pozostawiaj^ac wewn^atrz niema^l^a cz^e^s^c terenu, na kt^orej " +
    "powsta^l ^ow plac - miejsce spotka^n, handlu i wszelkich miejskich " +
    "uroczysto^sci.\n");
    add_item("bruk",
        "Ten, a mo^ze raczej ci, kt^orzy wbijali w ziemi^e te kocie ^lby, " +
    "musieli wyla^c z siebie si^odme poty, by wybrukowa^c plac tak " +
    "starannie. Jednak^ze g^ladko^s^c ich powierzchni oraz dok^ladne " +
    "wyprofilowanie kamieni nie jest ju^z zas^lug^a budowniczych a " +
    "jedynie przechodni�w, bezustannie krz^ataj^acych si^e po rynku.\n");
    add_item("krzewy",
        "@@ob_krzewy@@");

   add_sit("na bruku","na bruku","z bruku",0);
   add_sit("pod fontann^a","pod fontann^a","spod fontanny",10);

   set_drink_places("z fontanny");

   add_exit("placnw.c","p^o^lnoc");
   add_exit("placsw.c","po^ludnie");

   add_prop(ROOM_I_INSIDE,0);

   add_object(CENTRUM_OBIEKTY + "lawa_plac", 3);

   add_object(CENTRUM_OBIEKTY+"fontanna_plac");

   add_object(RINDE+"przedmioty/lampa_uliczna");

   dodaj_rzecz_niewyswietlana("elegancka kamienna �awa", 3);
   dodaj_rzecz_niewyswietlana("fontanna", 1);

   odnow_golebie_kwiaciarke();
   set_alarm_every_hour(&odnow_golebie_kwiaciarke());

   add_object(CENTRUM_DRZWI+"do_ratusza");
}

int
czy_klonowac_kwiaciarke()
{
    LOAD_ERR(RINDE_NPC + "kwiaciarka.c");

    if (sizeof(object_clones(find_object(RINDE_NPC+"kwiaciarka.c"))) > 0)
        return 0;

    switch (pora_roku())
    {
	case MT_LATO:
	case MT_WIOSNA:
	    return MT_GODZINA >= 9 && MT_GODZINA <= 21;
	default: // jesien, zima
	    return MT_GODZINA >= 11 && MT_GODZINA <= 19;
    }
}

void
init()
{
    ::init();
    init_drink_water();
}

public void
attempt_drink(string skad)
{
    if (pora_roku() == MT_ZIMA)
        TP->catch_msg("W zim� fontanna jest nieczynna, niestety nie"
                +" mo�na si� z niej napi�.\n");
    else if(TEMPERATURA(TO) < 0)
        TP->catch_msg("Woda w fontannie jest zamarzni�ta!\n");
    else
        ::attempt_drink(skad);
}

void
drink_effect(string skad)
{
    write("Zanurzasz d�onie w ch�odn� wod� i podnosisz j� do ust. "+
          "Smakuje wilgotnym kamieniem i czym� jeszcze, czego nie "+
	  "jeste� w stanie okre�li�.\n");
    saybb(QCIMIE(this_player(), PL_MIA) + " zanurza d�onie w ch�odn� wod� "+
          "i pije �yk wody z fontanny.\n");
}

string dlugi_opis()
{
    string str;

    str="Nie wiadomo, czy to ze wzgl�du na budynki otaczaj�ce "+
        "to miejsce, czy mo�e na staranne brukowanie, plac "+
	"przedstawia si� naprawd� dostojnie. Mimo sporego "+
	"t�umu ";

    if (!dzien_noc())
        str += "nieustannie t^edy przeci^agaj^acych ludzi";
    else
        str += "przeci^agaj^acych t^edy dzie^n w dzie^n ludzi";

    str += ", panuje tu atmosfera spokoju";

    if (!dzien_noc())
    {
        if (pora_roku() != MT_ZIMA)
        {
            if (TEMPERATURA(this_object()) > 0 && (CO_PADA(this_object()) == 0 ||
                CO_PADA(this_object()) == 2))
                str += ", a cichy szum fontanny i gruchanie go��bi, "+
	            "b��dz�cych po placu tylko sobie znanymi �cie�kami, "+
	            "pot�guj� to wra�enie. ";
            else
                if (CO_PADA(this_object()) == 1 || CO_PADA(this_object()) == 3)
                    str += ", a cichy szum fontanny pot^eguje to " +
                    "wra^zenie. ";
                else
                    if (TEMPERATURA(this_object()) <= 0)
                        str += ", a gruchanie go^l^ebi, b^l^adz^acych po " +
                        "placu tylko sobie znanymi ^scie^zkami, pot^eguje " +
                        "to wra^zenie. ";
                    else
                        str += ". ";
        }
        else
            if (CO_PADA(this_object()) == 0 || CO_PADA(this_object()) == 3)
                str += ", a gruchanie go^l^ebi, b^l^adz^acych po placu " +
                "tylko sobie znanymi ^scie^zkami, pot^eguje to wra^zenie. ";
            else
                str += ". ";
    }
    else
        str += ". Zalegaj^acej doko^la ciszy nie przerywaj^a r^ownie^z " +
        "zawsze ruchliwe go^l^ebie, opu^sciwszy to miejsce na noc. "; //ok?

    str += "Wok� ";

    if (pora_roku() == MT_ZIMA)
        str += "nieczynej w zimie ";
    else
        if (dzien_noc())
            str += "nieczynnej w nocy ";
        else
            if (TEMPERATURA(this_object()) <= 0)
                str += "nieczynnej z powodu mrozu ";

    str += "fontanny ustawione s� trzy "+
	"kamienne �awy, pozwalaj^ace przysi^a^s^c zm�czonym "+
	"przechodniom i zabieganym interesantom z ratusza, "+
	"wznosz�cego si� na samym �rodku placu. ";

	if (CZY_JEST_SNIEG(this_object()))
        str += "Wzd^lu^z jego ^scian, niczym u^spione, niewielkie stworki " +
        "przysypane warstw^a ^sniegu, kul^a si^e r^owno przyci^ete krzewy";
    else
    {
    if(pora_roku() == MT_ZIMA)
    {
        str+="Wzd�u� jego ^scian, niczym u�pione, niewielkie stworki";
        if (CZY_JEST_SNIEG(this_object()))
            str += " przysypane warstw^a ^sniegu";
        str +=", kul� si� r�wno przyci�te krzewy";
    }
    if(pora_roku() == MT_LATO || pora_roku() == MT_WIOSNA)
      str+="Wzd�u� jego �cian zieleni� si� niewielkie krzewy, "+
           "r�wniutko przyci�te i poprzetykane gdzieniegdzie "+
	   "kwieciem najr�niejszego koloru";
    if(pora_roku() == MT_JESIEN)
      str+="Wzd�u� jego �cian odbijaj� si� br�zowo niewielkie, "+
           "r�wniutko przyci�te krzewy, w�r�d kt�rych, niczym "+
	   "ostatni stra�nik, wychyla g��wk� jaki� jesienny kwiat";
    }

    str+=". W samym brzegu stoi wysoka";

    if(present("lampa", TO)->query_prop(OBJ_I_LIGHT))
      str+=", lampa rozpraszaj�ca swym �wiat�em "+
            "nocne ciemno�ci";
    else
      str+=" lampa uliczna";

    str+=".\n";
    return str;
}

void odnow_golebie_kwiaciarke()
{
    if(this_object()->jest_dzien() && !present("go��b", this_object()))
    {
        int n = random(4)+2;
        for(int i = 0; i < n; i++)
            clone_object(RINDE_NPC+"golab_plac.c")->move(TO);
    }
    if (!sizeof(filter(all_inventory(TO), &->is_kwiaciarka_rinde())) &&
            czy_klonowac_kwiaciarke()) {
        object kwiaciarka = clone_object(RINDE_NPC+"kwiaciarka.c");
        kwiaciarka->init_arg(0);
        kwiaciarka->start_me();
        kwiaciarka->move(TO);
    }
}

public string
exits_description()
{
    return "Dalsza cz^e^s^c placu widoczna jest na p^o^lnocy i po^ludniu, "+
           "znajduje si^e tu tak^ze wej^scie do ratusza.\n";
}
