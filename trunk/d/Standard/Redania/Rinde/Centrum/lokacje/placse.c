#include "dir.h"

#include <stdproperties.h>

inherit STREET_STD;

void
create_street()
{
    set_short("Miejski rynek");

    add_item(({"plac", "rynek"}),
        "Starannie wybrukowany plac, na ^srodku kt^orego wznosi si^e " +
    "zbudowany z czerwonej ceg�y gmach ratusza, stanowi centrum tego " +
    "miasta.\n");
    add_item("ratusz",
        "Budynek ten, cho^c prezentuje si^e dumnie, nie wyr^o^znia si^e " +
    "bynajmniej architektur^a g^ornych lot^ow i nie by^lby nijak " +
    "wyj^atkowym, gdyby nie budz^aca podziw smuk^la wie^za pn^aca si^e " +
	"wysoko ponad dachy innych budowli. Ponadto szczeg^olno^sci temu " +
    "gmachowi dodaje fakt, i^z spe^lnia on niebanaln^a rol^e w ^zyciu " +
    "miasta.\n");
    add_item(({"wie^z^e", "smuk^l^a wie^z^e", "wie^z^e ratusza"}),
        "Smuk^la wie^za wyrasta dumnie spo^sr^od kamienic "+
	"niczym palec, wskazuj^acy centrum tego miasta.\n");
    add_item(({"kamienice", "stare kamienice", "pi^etrowe kamienice",
        "stare pi^etrowe kamienice"}),
	"Stare, ponure kamienice wybudowane tutaj zosta^ly na planie kwadratu, " +
    "pozostawiaj^ac wewn^atrz niema^l^a cz^e^s^c terenu, na kt^orej " +
    "powsta^l ^ow plac - miejsce spotka^n, handlu i wszelkich miejskich " +
    "uroczysto^sci.\n");
    add_item("bruk",
        "Ten, a mo^ze raczej ci, kt^orzy wbijali w ziemi^e te kocie ^lby, " +
    "musieli wyla^c z siebie si^odme poty, by wybrukowa^c plac tak " +
    "starannie. Jednak^ze g^ladko^s^c ich powierzchni oraz dok^ladne " +
    "wyprofilowanie kamieni nie jest ju^z zas^lug^a budowniczych a " +
    "jedynie przechodni�w, bezustannie krz^ataj^acych si^e po rynku.\n");
    add_item(({"^swi^atyni^e", "dzwonnic^e", "dzwonnic^e ^swi^atyni"}),
        "Na zachodzie, nieco przys�oni�ta przez ratusz, widnieje " +
    "okaza�a �wi�tynia.\n");

    add_sit("na bruku","na bruku","z bruku",0);

    add_exit("place.c","p^o^lnoc");
    add_exit(POLUDNIE_LOKACJE + "ulica3.c","po^ludnie");
    add_exit("placs.c","zach^od");
    add_exit(WSCHOD_LOKACJE + "ulica4.c","wsch^od");

    add_object(RINDE+"przedmioty/lampa_uliczna");

    add_object(CENTRUM_OBIEKTY + "rynsztok");
    dodaj_rzecz_niewyswietlana("rynsztok");
}

string
dlugi_opis()
{
    string str;

    str	 = "Po�udniowo-wschodni kraniec du�ego placu wybrukowany jest " +
	"drobn� granitow� kostk�. Najbardziej rzuca si� w oczy wielki i " +
	"okaza�y budynek miejskiego ratusza, znajduj�cego sie w samym " +
	"�rodku. Odrapane szare kamieniczki otaczaj� plac ze wszystkich " +
	"stron. Tu� przed pasmem zabudowa� dostrzec mo�na ci�gn�ce si� " +
	"wg��bienie rynsztoka, z kt�rego dochodzi nieprzyjemny, trudny do " +
	"zidentyfikowania zapach. Ulica ci�gn�ca si� na wsch�d prowadzi do " +
	"widocznej st�d poka�nej bramy miejskiej. ";

	if (!dzien_noc())
        str += "Na zachodzie, nieco przys�oni�ta przez ratusz, widnieje " +
        "okaza�a �wi�tynia.";

    str += "\n";

    return str;
}

public string
exits_description()
{
    return "Dalsza cz^e^s^c placu widoczna jest na p^o^lnocy i zachodzie, "+
           "za^s ulice prowadz^a st^ad na po^ludnie i wsch^od.\n";
}
