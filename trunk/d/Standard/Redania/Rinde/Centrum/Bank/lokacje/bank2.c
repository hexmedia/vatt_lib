// molder 12.2005

/* Poprawione i dodane przez Adepta Bare
 * Data: 04 01 06
 * 1.Rzeczy w banku ktore mozna wziasc:
 *   - spore biorko
 *   - fotel
 * 2.Drzwi na wschodzie przez ktore bedzie wybiegac straz
 *   w razie awantury
 * 3.Npc bankowy
 * 4.Kilku npc klijetow (beda losowani randomowo)
 * 5. Tabliczka
 */
#include "dir.h"

inherit RINDE_STD;
inherit "/lib/bank.c";

#include <stdproperties.h>
#include <macros.h>
#include <ss_types.h>
#include <pl.h>
#include <object_types.h>

static int ile_foteli = 0;
string dlugi_opis();

void
create_rinde()
{
    set_short("Male zakurzone pomieszczenie");
    set_long("Niskie pomieszczenie, ktorego skrzypiaca podloge pokrywa " +
                "spora warstwa kurzu, sprawia wrazenie ubogiej, wiejskiej chatki. " +
                "W powietrzu unosi sie duszacy zapach dymu, drazniacy nozdrza. " +
                "Pod jedna ze scian ustawione jest spore biurko, ktore przezylo juz " +
                "swoje lata co widac po resztkach ciemnej farby, jaka zostalo "+
                "pomalowane. @@dlugi_opis@@ Po lewej stronie "+
                "znajduja sie spore, debowe drzwi, zza ktorych dobiega cichy, "+
                "rytmiczny brzek przeliczanych monet. Przy suficie wprawiono "+
                "male okienko wpuszczajace do srodka niewiele swiatla, " +
                "gdyz przysloniete jest ciemna zaslona.\n");

    dodaj_rzecz_niewyswietlana("wielki drewniany fotel");

    add_object(RINDE+"przedmioty/meble/fotel_wielki_drewniany.c");

    add_item("podloge",
                "Drewniana podloga, ktora skrzypi cicho pod naporem twoich stop, " +
                "pokryta jest spora warstwa kurzu.\n");

    add_item("biurko",
                "Sporej wielkosci, drewniane biurko ustawione jest pod jedna ze " +
                "scian. Po resztach ciemnej farby, ktora zostalo pomalowane " +
                "widac, iz przezylo juz ono swoje najlepsze lata. Blat " +
                "zawalony jest spora iloscia papierow i roznych dokumentow, " +
                "starannie na nim poukladanych.\n");

    add_item("drzwi",
                "Do jednej ze scian wprawione sa spore, debowe drzwi, zza "+
                "ktorych dobiega cichy, rytmiczny brzek przeliczanych monet.\n");

    add_item( ({"okienko","zaslone" }),
                "Ma^le okienko wprawione zosta^lo tu^z przy suficie. Wpuszcza do " +
                "srodka niewiele swiatla, gdyz przysloniete jest stara, "+
                "ciemna zaslona.\n");

    add_event("Zza drzwi dochodzi cichy brzek przeliczanych monet.\n");
    add_event("Podloga skrzypi cicho pod naporem twoich stop.\n");
    add_event("Drzwi z trzaskiem obily sie o futryne.\n");
    add_event("Zaslona zakolysala sie lekko, poruszona przez wiatr.\n");

    add_exit("place",({"wyjscie", "do wyj^scia"}));

    // nikt nie lubi tabliczek :P
    add_item("tabliczke", "@@standard_bank_sign@@");

    add_prop(ROOM_I_INSIDE,1);
    add_prop(ROOM_I_HIDE, -1);

    set_bank_fee(1);
    config_default_trade();
    config_trade_data();
}
public string
exits_description()
{
    return "Wyj^scie st^ad prowadzi na plac.\n";
}


void
init()
{
        ::init();
        bank_init();
}

public void
enter_inv(object ob, object from)
{
        ::enter_inv(ob, from);
        if (!objectp(ob)) {
                return;
        }
        if (ob->query_type() == O_MEBLE) {
                if ((ob->short() ~= "krysztalowy stol")) {
                        ++ile_foteli;
                        if (ile_foteli == 1) {
                                return;
                        }
                }
                dodaj_rzecz_niewyswietlana(ob->short());
        }
}

public void
leave_inv(object ob, object to)
{
        ::leave_inv(ob, to);
        if (!objectp(ob)) {
                return;
        }
        if (ob->query_type() == O_MEBLE) {
                if ((ob->short() ~= "krysztalowy stol")) {
                        --ile_foteli;
                        if (ile_foteli <= 0) {
                                ile_foteli = 0;
                                return;
                        }
                }
                usun_rzecz_niewyswietlana(ob->short());
        }
}

string
dlugi_opis()
{
        int i, il, ile;
        object *obarr;

        obarr = subinventory(0);
        obarr = filter(obarr, &operator(==)(, O_MEBLE) @ &->query_type());


        if ((il = sizeof(obarr))) {
                for (i = 0; i < il; ++i) {
                        if (obarr[i]->short() ~= "krysztalowy stol") {
                                obarr -= ({ obarr[i] });
                                break;
                        }
                }
        }
        if ((il = sizeof(obarr))) {
                ile = 0;
                for (i = 0; i < il; ++i) {
                        if (obarr[i]->short() ~= obarr[0]->short()) {
                                ++ile;
                        }
                }
                return " Posrodku pomieszczenia " + ilosc(ile, "stoi", "stoja",
"stoi") + " " + COMPOSITE_DEAD(obarr, PL_MIA) + ".";
        }
        return "";
}
