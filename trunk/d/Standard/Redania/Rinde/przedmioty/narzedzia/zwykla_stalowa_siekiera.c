inherit "/std/weapon";

#include <stdproperties.h>
#include <object_types.h>
#include <materialy.h>
#include <wa_types.h>

void create_weapon()
{
	ustaw_nazwe("siekiera");
       dodaj_nazwy("topor");
	dodaj_przym("zwyk�y", "zwykli");
	dodaj_przym("stalowy", "stalowi");
	set_long("Lekko sp�aszczony drzewec oraz nasadzone na� stalowe ostrze -"
			+" ot, zwyczajna siekiera. Niestety ostrze nie zosta�o w jakikolwiek"
			+" spos�b zabezpieczone przez co istnieje mo�liwo��, �e przy kt�rym�"
			+" uderzeniu zsunie si� ono z r�koje�ci. C�, pozostaje jedynie mie�"
			+" nadziej�, �e tak si� nie stanie.\n");
	
	set_hit(20);
	set_pen(22);

	add_prop(OBJ_I_WEIGHT, 3150);
	add_prop(OBJ_I_VOLUME, 1040);
	add_prop(OBJ_I_VALUE, 840);

	set_wt(W_AXE);
	set_dt(W_SLASH);
	
	set_hands(A_ANY_HAND);
	ustaw_material(MATERIALY_STAL, 80);
	ustaw_material(MATERIALY_DR_SOSNA, 20);
	set_type(O_BRON_TOPORY | O_NARZEDZIA);
}
