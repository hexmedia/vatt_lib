/*
 * Kolczuga dla gwardii w Rinde
 * Autor: Rantaur
 * 03.11.06
 * [jakosc w zamierzeniu ma byc srednia]
 */
inherit "/std/armour";

#include <formulas.h>
#include <macros.h>
#include <materialy.h>
#include <stdproperties.h>
#include <wa_types.h>
#include <object_types.h>
#include <materialy.h>

void create_armour()
{
    ustaw_nazwe("kolczuga");
    dodaj_przym("szary", "szarzy");
    dodaj_przym("matowy","matowi");
    set_long("Szarawa kolczuga wygl�da na ca�kiem solidnie wykonan�."+
    " Tysi�ce drobnych, nadgryzionych juz z�bem czasu, k�eczek"+
    " splataj� si� ze sob� tworz�c wygodn�, zapewniaj�c� du��"+
    " swodob� ruch�w zbroj�. Mimo, i� niegdy� l�ni�cy metal teraz"+
    " zmatowia� i pokry� si� ciemnym nalotem, to kolczuga z pewno�ci�"+
    " b�dzie dobrze spe�nia� sw� rol� jeszcze przez d�ugi czas.\n");

    ustaw_material(MATERIALY_STAL, 100);

    set_slots(A_TORSO, A_ARMS, A_FOREARMS, A_THIGHS);
    set_ac(A_TORSO, 12,13,4, A_ARMS, 12,13,4, A_FOREARMS, 12,13,4,
         A_THIGHS, 11,12,3);
    set_size("M");
    add_prop(OBJ_I_WEIGHT, 15000);
    add_prop(OBJ_I_VOLUME, 15000);
    add_prop(OBJ_I_VALUE, 4800);
    set_type(O_ZBROJE);
    set_likely_cond(11);
}
