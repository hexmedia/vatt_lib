
/* Autor: Avard
   Opis : Artonul
   Data : 22.07.05 */

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <macros.h>
#include <wa_types.h>
#include <materialy.h>
#include <pl.h>
#include <object_types.h>


void
create_armour()
{
    ustaw_nazwe("kolczuga");
    dodaj_przym("brz^ecz^acy","brz^ecz^acy");
    dodaj_przym("czerniony","czernieni");
    set_long("Kolczug^e t^e wykonano z k^o^lek poczernionej stali pod kt^or^a "+
        "umieszczono warstwy p^l^otna i sk^ory. Kolcza siatka obejmuje "+
        "ca^ly tu^l^ow i ramiona a w najbardziej czu^lych miejscach jest na "+
        "dodatek wzmocniona kawa^lkami plyt. Do po^laczenia wszystkiego w "+
        "ca^lo^s^c s^lu^zy kilkana^scie r^ownie^z poczernionych rzemieni z "+
        "dobrze wyprawionej sk^ory. Ca^lo^s^c w miare dobrze mo^ze si^e "+
        "sprawdzi^c w boju, ale ze wzgl^edu na wag^e i sztywno^s^c nie "+
        "zapewnia zbytniej wygody podr^o^znikom.\n"); 

    ustaw_material(MATERIALY_STAL, 60);
    ustaw_material(MATERIALY_SK_SWINIA, 40);
    set_type(O_ZBROJE);
    set_slots(A_TORSO, A_ARMS, A_STOMACH, A_FOREARMS, A_THIGHS);
    add_prop(OBJ_I_VOLUME, 15000); 
    add_prop(OBJ_I_VALUE, 6560);   
    add_prop(OBJ_I_WEIGHT, 15000);
    set_ac(A_TORSO,12,13,4, A_ARMS,12,13,4, A_STOMACH,12,13,4, 
        A_FOREARMS,12,13,4, A_THIGHS,11,12,3);
    add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    set_size("L");

    set_likely_cond(9);
    
}
