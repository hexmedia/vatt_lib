
/* Obraz na pietrze karczmy Baszta.
   Lil 09.07.2005               */

inherit "/std/object.c";

#include <pl.h>
#include <stdproperties.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"
#include <object_types.h>
#include <materialy.h>

create_object()
{
    ustaw_nazwe("obraz");
    dodaj_przym("ma�y","mali");
    dodaj_przym("amatorski","amatorscy");
    set_long("Niewielki obraz przedstawia kr�p�, mile u�miechni�t� "+
            "kasztanowow�os� kobiet� siedz�c� na le�nej polanie "+
	    "w�r�d zielonych traw. Szacowany przez ciebie wiek tej�e "+
	    "postaci sugeruje, �e mog�aby ona by� pr�dzej c�rk� karczmarza "+
	    "ni�li jego matk�. Za� po technice malowania (co wida� "+
	    "ju� na pierwszy rzut oka) z �atwo�ci� mo�na okre�li�, "+
	    "i� artysta, kt�ry sp�odzi� �w obraz by� raczej artyst� "+
	    "niedzielnym. Nie wygl�da to na dzie�o sztuki, a wr�cz przeciwnie "+
	    "- ra�� niedok�adna linia i nienaturalne miejscami kszta�ty postaci. "+
	    "W lewym dolnym rogu dostrzegasz malutki bazgro�, kt�ry stw�rca "+
	    "mazn�� pewnikiem wzoruj�c si� na innych wyszukanych czcionkach, "+
	    "kt�rymi zwykli podpisywa� swe Dzieci prawdziwi arty�ci. "+
	    "M�wi on, �e wykonawc� jest Damir. P��tno oprawia pomalowana na "+
	    "��to drewniana zwyk�a ramka, kt�ra do wizerunku ca�o�ci "+
	    "dodaje od siebie lekk� nutk� kiczu. Ten przedmiot, to raczej "+
	    "nic warto�ciowego.\n");

    add_prop(OBJ_I_WEIGHT, 485);
    add_prop(OBJ_I_VALUE, 0);
    add_prop(OBJ_I_VOLUME, 500);
    set_type(O_SCIENNE);
    ustaw_material(MATERIALY_LEN);
    set_owners(({DAMIR,RINDE_NPC+"pomagacz_damira"}));
}
string
query_auto_load()
{
   return ::query_auto_load() + ":";
}
