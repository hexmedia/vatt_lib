inherit "/std/object";
inherit "/lib/kwiat";

#include "/sys/stdproperties.h"
#include "/sys/macros.h"
#include "/sys/pl.h"
#include "/sys/cmdparse.h"
#include "/sys/ss_types.h"
#include <object_types.h>

/*int powachaj(string str);
     A zamiast tego robimy: */
string zapach(string str);
string reakcja(string str);
int zgniec(string str);

int globalny = -1; // bo tak

void
create_object()
{
 ustaw_nazwe("roza");
 dodaj_przym("szkar�atny", "szkar�atni");
 dodaj_nazwy("kwiat");
 set_long("Wyj�tkowo g��boka i intensywna barwa jest " +
          "pierwsz� rzecz�, kt�ra rzuca sie w oczy kiedy " +
          "spogl�dasz na ten kwiat. �odyg� zdobi kilka ostrych " +
          "kolc�w, na kt�re trzeba uwa�a�, je�li nie chce si� " +
          "pokaleczy�.\n");

 add_prop(OBJ_I_WEIGHT, 50);
 add_prop(OBJ_I_VOLUME, 65);
 add_prop(OBJ_I_VALUE, 20);

 set_type(O_KWIATY);
 set_wplatalny(1);


 /*jak_pachnie="Pachnie cudownie.";
   jak_wacha_jak_sie_zachowuje="mruzy oczy z luboscia.";  <--- Cos takiego
       wlasnie by bylo, gdyby to byl prosty przedmiot, bez randomowych "zapachow" */

   jak_pachnie="@@zapach@@";
   jak_wacha_jak_sie_zachowuje="@@reakcja@@";
}

init()
{
 ::init();
 init_kwiat();
 //add_action(powachaj, "powachaj");
 add_action(zgniec, "zgnie�");
}
/*
int
powachaj(string str)
{
 if (str == "roze" || str == "szkarlatna roze" || str == "kwiat")
 {
  switch (random(7))
  {
   case 0..5:
        write("Zamykajac oczy unosisz powoli kwiat ku swej twarzy i " +
        "delektujesz sie jego delikatnym niczym dotyk jedwabiu " +
        "zapachem, ktory zdaje sie ogarniac wszystko wokol ciebie.\n");
   saybb(QCIMIE(this_player(), PL_MIA)+" zbliza do twarzy szkarlatna roze " +
          "i napawa sie jej delikatnym zapachem.\n");
   return 1;
   break;
   case 6:
        write("Unosisz roze ku twarzy napawajac sie jej pieknym " +
              "zapachem, jednak przyjemnosc zostaje brutalnie " +
              "przerwana przez nagle uklucie bolu. Spogladajac na " +                         "swa dlon zauwazasz sciekajaca po niej powoli struzke " +
              "krwi.\n");
        saybb(QCIMIE(this_player(), PL_MIA)+" zbliza do twarzy " +
           "szkarlatna roze i napawa sie jej delikatnym zapachem, " +
           "jednak nagle syczy z bolu spogladajac na dlon skaleczona " +
           "kolcami pieknego kwiatu.\n");
   return 1;
   break;
  }
 }
 notify_fail("Co chcesz powachac? Moze jakis kwiat?\n");
 return 0;
}*/

int
zgniec(string str)
{
 object lilia;

 if (parse_command(str, all_inventory(this_player()), "%o:" + PL_BIE, lilia) &&
    lilia == this_object())
 {
  write("Zamykasz w d�oni delikatne p�atki szkar�atnej r�y " +
        "i gwa�townym ruchem mia�d�ysz kwiat w palcach " +
        "wy�adowywuj�c sw� w�ciek�o��. " +
        "Chwil� po tym lekkim ruchem r�ki odrzucasz zniszczony " +
        "kwiat na bok.\n");
  saybb(QCIMIE(this_player(), PL_MIA)+" mru�y oczy z w�ciek�o�ci� " +
        "mia�d��c w d�oni delikatne p�atki szkar�atnej r�y. " +
        "Chwil� potem zniszczony kwiat l�duje na ziemi, a jego " +
        "pomi�te p�atki wiruj� przez chwil� w powietrzu" +
        (environment(this_player())->query_prop(ROOM_I_INSIDE) ?
        " i opadaj� na ziemi�.\n" : " po czym odlatuj� w dal niesione " +
        "podmuchem wiatru.\n"));
  remove_object();
  return 1;
 }
 notify_fail("Zgnie� co ?\n");
 return 0;
}


string
zapach(string str)
{
  switch (random(7, time()))
  {
   case 0..5:
        str = "Zamykaj�c oczy unosisz powoli kwiat ku swej twarzy i " +
        "delektujesz si� jego delikatnym niczym dotyk jedwabiu " +
        "zapachem, kt�ry zdaje si� ogarnia� wszystko wok� ciebie.";
   break;
   case 6:
        str = "Unosisz r�� ku twarzy napawaj�c si� jej pi�knym " +
              "zapachem, jednak przyjemno�� zostaje brutalnie " +
              "przerwana przez nag�e uk�ucie b�lu. Spogladaj�c na " +
	      "sw� d�o� zauwa�asz �ciekaj�c� po niej powoli stru�k� " +
              "krwi.";
   break;
  }
  return str;
}

string
reakcja(string str)
{
  switch (random(7, time()))
  {
   case 0..5:
   str = "zbli�a do twarzy szkar�atn� r�� " +
          "i napawa si� jej delikatnym zapachem.";
   break;
   case 6:
   str = "zbli�a do twarzy " +
           "szkar�atn� r�� i napawa si� jej delikatnym zapachem, " +
           "jednak nagle syczy z b�lu spogladaj�c na d�on skaleczon� " +
           "kolcami pi�knego kwiatu.";
   break;
  }
  return str;
}
