/* Takie tam pierdo�y le��ce na szafce w stajni karczmy Baszta.
                           Lil & Alcyone 04.2005*/

inherit "/std/object.c";

#include "dir.h"
#include <pl.h>
#include <stdproperties.h>
#include <macros.h>
#include <filter_funs.h>

int zaloz_rekawice();

create_object()
{
    ustaw_nazwe(({"r�kawice","r�kawic","r�kawicom","r�kawice","r�kawicami","r�kawicach"}),
               ({"r�kawice","r�kawic","r�kawicom","r�kawice","r�kawicami","r�kawicach"}),
                  PL_NIJAKI_NOS);
                 
    dodaj_przym("je�dziecki","je�dzieccy");
    dodaj_przym("zniszczony","zniszczeni");

    set_long("Poprzecierane w wielu miejscach gotowe s� rozpa�� si� "+
             "na kawa�ki w dowolnym momencie. Siegaj� a� za �okcie. "+
	     "Czarne, d�ugie r�kawice niegdy� mog�y dobrze s�u�y� "+
	     "jakiemu� je�d�cowi, lecz ostatnio wykorzystywane by�y, "+
	     "nie masz w�tpliwo�ci, do mniej szczytnych cel�w. "+
	     "Wnioskujesz to po zapachu ko�skiego �ajna.\n");

    add_prop(OBJ_I_WEIGHT, 94);
    add_prop(OBJ_I_VOLUME, 194);
    add_prop(OBJ_I_VALUE, 0);


    add_cmd_item(({"r�kawice","je�dzieckie r�kawice","zniszczone r�kawice",
                   "je�dzieckie zniszczone r�kawice"}), "za��", zaloz_rekawice,
                   "Za�� co?\n"); 
}

int
zaloz_rekawice()
{
   write("Pr�bujesz za�o�y� "+this_object()->short(PL_BIE)+
         ", jednak s� one kompletnie zniszczone i nie nadaj� si� ju� do niczego.\n");
   saybb(QCIMIE(this_player(), PL_MIA) + " pr�buje za�o�y� "+this_object()->short(PL_BIE)+
         ", jednak s� one kompletnie zniszczone i nie nadaj� si� ju� do niczego.\n");
   return 1;
}