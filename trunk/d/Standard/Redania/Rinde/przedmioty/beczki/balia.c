inherit "/std/beczulka.c";

#include "dir.h"
#include <pl.h>
#include <cmdparse.h>
#include <stdproperties.h>
#include <wa_types.h>
#include <formulas.h>
#include <macros.h>
#include <composite.h>

#define W_BALII "_w_balii"

public int wyjdz(string str);
public int wejdz(string str);

public int umyj(string str);
public int napelnij(string str);

public int no_command();
public int blok(string str);

int sukces(object obj1, object obj2);

void
create_beczulka()
{
    set_pojemnosc(200000);
    set_opis_plynu("krystalicznie czystej wody");
    set_nazwa_plynu_dop("wody");
    set_vol(0);
    set_ilosc_plynu(5000);
    set_dlugi_opis("Wielka, drewniana balia.");
        
    ustaw_nazwe( ({ "balia", "balii", "balii",
                    "bali^e", "bali^a", "balii" }),
                 ({ "balie", "balii", "baliom", "balie",
                    "baliami", "baliach" }), PL_ZENSKI);
    dodaj_przym( "wielki", "wielcy" );
    dodaj_przym( "d^ebowy", "d^ebowi" );
}

public void
init()
{
     ::init();

     add_action(wejdz, "wejd^x");
     add_action(wyjdz, "wyjd^x");
     add_action(umyj, "umyj");
     add_action(napelnij, "nape^lnij");
     add_action("blok", "", 1); 
}

private int
napij(string str)
{
    object *obs;
    int ret;
    
    notify_fail("Napij si^e z czego?\n");
    
    if (!strlen(str))
        return 0;
    
    if (!parse_command(str, environment(this_player()), 
        "'sie' 'z' %i:" + PL_DOP, obs))
        return 0;
    
    obs = NORMAL_ACCESS(obs, 0, 0);
    
    if (!sizeof(obs))
        return 0;
        
    if (sizeof(obs) > 1)
    {
        notify_fail("Mo^zesz si^e napi^c z tylko jednej rzeczy naraz.\n");
        return 0;
    }
    
    ret = obs[0]->drink_from_it(this_player());
    if (ret)
    this_player()->set_obiekty_zaimkow(obs);
    return ret;
}

public int
wylej(string str)
{
    object *obs;
    int ret;
    
    notify_fail("Wylej co? Moze zawarto^s^c " + query_nazwa(PL_DOP) + ".\n");
    
    if (!strlen(str))
        return 0;
        
    if (!parse_command(str, environment(this_player()), 
        "'zawartosc' %i:" + PL_DOP, obs))
        return 0;
    
    obs = NORMAL_ACCESS(obs, 0, 0);
    
    if (!sizeof(obs))
        return 0;
        
    if (sizeof(obs) > 1)
    {
        notify_fail("Mo^zesz wylac zawartosc tylko jednej rzeczy naraz.\n");
        return 0;
    }

    ret = obs[0]->wylej_from_it();
    if (ret)
    this_player()->set_obiekty_zaimkow(obs);

    return ret;
}


public int
wylej_from_it()
{
    if (!query_ilosc_plynu())
    {
        write(capitalize(short()) + " jest zupe^lnie pust" + 
            koncowka("y", "a", "e") + ".\n");
        return 1;
    }
    
    write("Wyci^agasz korek z dna " + short(PL_DOP) + " i patrzysz jak ca^la zawarto^s^c " + 
          "wyp^lywa po kamiennej pod^lodze w kierunku otworu w ^scianie. Po chwili wtykasz " +
          "korek na miejsce.\n");
    say(QCIMIE(this_player(), PL_MIA) + " wyci^aga korek z " + 
        QSHORT(this_object(), PL_DOP) + " pozwalaj^ac aby ca^la zawarto^s^c wyp^Syn^e^la " + 
        "po kamiennej pod^lodze do otworu w ^scianie. Po chwili wtyka korek na miejsce.\n");
        
    set_ilosc_plynu(0);
    set_vol(0);
    set_opis_plynu("");
    set_nazwa_plynu_dop("");
    
    return 1;
}

public int
umyj(string str)
{
    int myj;
    int nowa;

    if (!str || str != "si^e w balii")
    {
        notify_fail("Chcesz mo^ze umy^c sie w balii?\n");
        return 0;
    }  
    if (!query_ilosc_plynu())
    {
        write(capitalize(short()) + " jest zupe^lnie pust" + 
            koncowka("y", "a", "e") + ".\n");
        return 1;
    }

    myj = MIN(query_ilosc_plynu(), 550);
    nowa = query_ilosc_plynu() - myj;

    write("Wk^ladasz d^lonie do " + query_opis_plynu() + " i kilkoma szybkimi " + 
          "ruchami ochlapujesz sobie twarz. Przecierasz oczy i gwaltownym skr^etem " +
          "g^lowy strzepujesz krople " + query_nazwa_plynu_dop() + " na pod^log^e.\n");
    say(QCIMIE(this_player(), PL_MIA) + " zanurza rec^e w " +
        QSHORT(this_object(), PL_MIE) + " i kilkoma ruchami ochlapuje sobie " + 
        "twarz. Szybko przeciera oczy i gwa^ltownym skr^etem g^lowy strzepuje " +
        "krople " + query_nazwa_plynu_dop() + " na pod^log^e.\n");
        
    if (nowa == 0)
    {
        set_vol(0);
        set_opis_plynu("");
        set_nazwa_plynu_dop("");
    }
    set_ilosc_plynu(nowa);
    return 1;
}


private int
napelnij(string str)
{
    object *obs1;
    object *obs2;
    object obj1;
    object obj2;

    notify_fail("Nape^lnij co z czego?\n");
    
    if (!strlen(str))
        return 0;
    
    if (!parse_command(str, deep_inventory(environment(this_player())), 
        "%i:" + PL_BIE + " 'z' %i:" + PL_DOP, obs1, obs2))
        return 0;
    
    obs1 = NORMAL_ACCESS(obs1, 0, 0);
    obs2 = NORMAL_ACCESS(obs2, 0, 0);

    this_player()->set_obiekty_zaimkow(obs1, obs2);

    if (!sizeof(obs1))
        return 0;
    if (!sizeof(obs2))
        return 0;
        
    if (sizeof(obs1) > 1)
    {
        notify_fail("Mo^zesz nape^lnic tylko jedn^a rzecz naraz.\n");
        return 0;
    }
    if (sizeof(obs2) > 1)
    {
        notify_fail("Mo^zesz nape^lniac tylko z jednej rzeczy naraz.\n");
        return 0;
    }
    obj1 = obs1[0];
    obj2 = obs2[0];

    if (obj1 == obj2)
    {
        notify_fail("Do nape^lniania potrzebne ci b^ed^a dwa ro^zne przedmioty.\n");
        return 0;
    }
    if (living(obj1))
    {
        return 0;
    }
    if (living(obj2))
    {
        return 0;
    }
    if (!obj1->query_pojemnosc())
    {
        return 0;
    }
    if (!obj2->query_nazwa_plynu_dop())
    {
        return 0;
    }
    if (obj2->query_nazwa_plynu_dop()=="")
    {
        notify_fail(capitalize(obj2->short()) + " nie zawiera nic" +
                    " czym mo^zna by cokolwiek nape^lnic.\n");
        return 0;
    }
    if (obj1->query_ilosc_plynu() == obj1->query_pojemnosc())
    {
        notify_fail(capitalize(obj1->short()) + " jest juz pe^ln" +
                    obj1->koncowka("y", "a", "ne", "ni", "ne") + "\n");
        return 0;
    }
    if (obj1->query_ilosc_plynu())
    {
        if (obj1->query_vol()==obj2->query_vol()&&
            obj1->query_opis_plynu()==obj2->query_opis_plynu()&&
            obj1->query_nazwa_plynu_dop()==obj2->query_nazwa_plynu_dop())
        {
            sukces(obj1,obj2);
            return 1;
        }

        notify_fail("Musisz najpierw opr^o^znic " + obj1->short(PL_BIE) + ".\n");
        return 0;
    }

    sukces(obj1,obj2);
    return 1;
}

int 
sukces(object obj1, object obj2)
{
    int nowa;

    write("Nape^lniasz " + obj1->short(PL_BIE) + " z " + obj2->short(PL_DOP) + ".\n");
    say(QCIMIE(this_player(), PL_MIA) + " nape^lnia " +
        QSHORT(obj1, PL_BIE) + " z " + QSHORT(obj2, PL_DOP) + ".\n");

    nowa = MIN(obj1->query_pojemnosc() - obj1->query_ilosc_plynu(), obj2->query_ilosc_plynu()); 

    obj1->set_ilosc_plynu(obj1->query_ilosc_plynu() + nowa);
    obj1->set_vol(obj2->query_vol());
    obj1->set_opis_plynu(obj2->query_opis_plynu());
    obj1->set_nazwa_plynu_dop(obj2->query_nazwa_plynu_dop());

    if (nowa == obj2->query_ilosc_plynu())
    {
        obj2->set_ilosc_plynu(0);
        obj2->set_vol(0);
        obj2->set_opis_plynu("");
        obj2->set_nazwa_plynu_dop("");
        return 1;
    }

    obj2->set_ilosc_plynu(obj2->query_ilosc_plynu() - nowa);

    return 1;
}

int
f_filter(object ktory)
{
    if((ktory->query_prop(OBJ_M_NO_DROP) == 0))
        return 1;                         
    return 0;                          
}

/*public string
//wejdz()
z_toba()
{
    if(this_player()->query_prop(W_BALII)==1)
    {
        return "z tob^a";
    }
    return "";
} */

public int
wejdz(string str)
{
    int myj;
    int nowa;
    object *rzeczy;


    if (!str || str != "do balii")
    {
        notify_fail("Chcesz mo^ze wej^s^c do balii?\n");
        return 0;
    }

    if(this_player()->query_prop(W_BALII)==1)
    {
        write("Przecie^z ju^z jeste^s w balii.\n");
        return 1;
    }  

    if (!query_ilosc_plynu())
    {
        write(capitalize(short()) + " jest zupe^lnie pust" + 
            koncowka("y", "a", "e") + ". Nie ma sensu do niej wchodzi^c.\n");
        return 1;
    }

    rzeczy = all_inventory(this_player());
    rzeczy = filter(rzeczy, f_filter); 

    if (sizeof(rzeczy) != 0)
    {
        write("Po^loz wszystko gdzie^s z boku bo ci si^e zamoczy.\n");
        return 1;
    }

    this_player()->add_prop(W_BALII,1);
    if(this_player()->query_prop(W_BALII)==1)
    this_player()->add_prop(OBJ_S_EXTRA_DESC, "%s siedz�c[y/a/e/y " +
                   "razem/e razem] z tob� w balii");
    else this_player()->add_prop(OBJ_S_EXTRA_DESC, "%s siedz�c[y/a/e/y " +
                   "razem/e razem] w balii");
    write("Wchodzisz do balii i siadasz wygodnie na dnie.\n");
    say(QCIMIE(this_player(),PL_MIA)+" wchodzi do balii i siada na dnie.\n");
        
    return 1;
}

public int
blok(string str)
{
    string verb;
    string *nie_dozwolone;
    string *nie_dozwolone2;

    nie_dozwolone=({"n","p^o^lnoc","s","po^ludnie","w","zach^od","e","wsch^od","nw",
        "p^o^lnocny-zach^od","sw","po^ludniowy-zach^od","se","po^ludniowy-wsch^od",
        "p^o^lnocny-wsch^od","ne","trans","u","g^ora","d","dol","goto","home"});
    nie_dozwolone2=({"podskocz","zata^ncz","nadepnij","kopnij","podrepcz",
        "tupnij","we^x"});
    verb = query_verb();
    if (this_player()->query_prop(W_BALII)==1)
    {
    if ((member_array(verb,nie_dozwolone)==-1)&&(member_array(verb,nie_dozwolone2)==-1))
        return 0;
    else 
        return no_command();
    }

    return 0; 
}

public int
no_command()
{
    write("Wyjd^z najpierw z balii.\n");
    return 1;
}

int
wyjdz(string str)
{
    
    
    notify_fail("Sk^ad chcesz wyj^s^c?\n");

    if (this_player()->query_prop(W_BALII)==0)
        return 0;
    if(str!="z balii")
    {
        write("Wyjd^z sk^ad?\n");
        return 1;
    }

    this_player()->remove_prop(W_BALII);
    this_player()->remove_prop(OBJ_S_EXTRA_DESC);


    write("Wychodzisz z balii.\n");
    say(QCIMIE(this_player(),PL_MIA) + " wychodzi z balii.\n");

    return 1;
}
