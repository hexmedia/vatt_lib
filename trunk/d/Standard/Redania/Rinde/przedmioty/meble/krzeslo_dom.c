/*
 * Autor: Rantaur
 * Data: 18.06.06
 * Opis: Krzeslo do domu swiatynnego w Rinde
 */

inherit "/std/object";

#include "dir.h"
#include <macros.h>
#include <materialy.h>
#include <pl.h>
#include <stdproperties.h>
#include <object_types.h>

void create_object()
{
  ustaw_nazwe("krzes�o");

  dodaj_przym("wygodny", "wygodni");
  dodaj_przym("rze�biony", "rze�bieni");
  
  set_long("Obicie krzes�a wykonane jest z kremowego materia�u"
	   +" na kt�rym mieni� si� wyszyte cienk� nicia wzory kwiat�w"
	   +" i winoro�li. Ciemne, drewniane nogi przypominaj� ci"
	   +" troch� smuk�e, lekko zgi�te odn�a modliszki dodatkowo"
	   +" ozdobione nieregularnymi, spiralnymi liniami."
	   +" Na siedzisku zauwa�asz kilka blado-niebieskich plamek,"
	   +" b�d�cych zapewne efektem niefortunnego chlapni�cia"
	   +" umoczonego w atramencie pi�ra.\n" );

  add_prop(OBJ_I_VALUE, 655);
  add_prop(OBJ_I_WEIGHT, 6200);
  add_prop(OBJ_I_VOLUME, 8100);

  make_me_sitable("na", "na krze�le", "z krzes�a", 1);

  set_type(O_MEBLE);

  ustaw_material(MATERIALY_WELUR, 30);
  ustaw_material(MATERIALY_DR_DAB, 70);
  set_owners(({RINDE_NPC+"krepp"}));
}

init()
{
  ::init();
}



