#pragma strict_types

inherit "/std/receptacle";

#include "dir.h"
#include <pl.h>
#include <macros.h>
#include <filter_funs.h>
#include <stdproperties.h>
#include <composite.h>
#include <cmdparse.h>
#include <object_types.h>
#include "/sys/materialy.h"
#include "/sys/object_types.h"

string opis_zawartosci();

nomask void
create_container()
{
  ustaw_nazwe("szafa");

  set_long("Prosta, drewniana szafa. Z grubsza ociosane deski zbite s� dosy� solidnie, " +
      "przez co mebel wygl�da na bardzo wytrzyma�y.@@opis_zawartosci@@" +
      "W jednoskrzyd�owe drzwiczki wprawiony jest metalowy zamek. Ca�o�� pokrywaj� " +
      "�elazne wzmocnienia.@@opis_sublokacji| Na szafie widzisz |na|.|||" + PL_BIE + "@@\n");

  add_prop(CONT_I_MAX_VOLUME, 100000);
  add_prop(CONT_I_VOLUME, 16800);
  add_prop(CONT_I_WEIGHT, 10000);
  add_prop(OBJ_I_VALUE, 180);
  add_prop(CONT_I_MAX_WEIGHT, 150000);
  add_prop(CONT_I_DONT_SHOW_CONTENTS, 1);
  add_prop(CONT_I_RIGID, 1);

  add_subloc("na");
  add_subloc_prop("na", CONT_I_MAX_VOLUME, 3200);
  add_subloc_prop("na", CONT_I_MAX_WEIGHT, 30000);

  add_subloc_prop(0, CONT_I_MAX_VOLUME, 80000);
  add_subloc_prop(0, CONT_I_MAX_WEIGHT, 110000);

  ustaw_material(MATERIALY_DR_DAB, 80);
  ustaw_material(MATERIALY_STAL, 20);
  set_type(O_MEBLE);

  set_key("gr:szafa w dy�urce");
  set_pick(60);
   set_owners(({RINDE + "straz/straznik",RINDE+"straz/oficer"}));
}

string
opis_zawartosci()
{
  return (query_prop(CONT_I_CLOSED) ? " " : opis_sublokacji(" W �rodku widzisz ",0,". ", " ", PL_BIE));
}
