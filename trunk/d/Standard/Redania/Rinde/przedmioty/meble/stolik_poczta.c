inherit "/std/container";

#include "dir.h"
#include "/sys/stdproperties.h"
#include "/sys/macros.h"
#include "/sys/pl.h"
#include "/sys/materialy.h"
#include "/sys/object_types.h"

void
create_container()
{
    ustaw_nazwe("stolik");
    dodaj_przym("ma�y", "ma�y");
    dodaj_przym("kwadratowy", "kwadratowi");
    ustaw_material(MATERIALY_DR_DAB);

    set_type(O_MEBLE);
    set_long("@@dlugasny@@\n"+
             "@@opis_sublokacji|Dostrzegasz na nim |na|.\n||" + PL_BIE+ "@@");

    add_prop(CONT_I_WEIGHT, 8321);
    add_prop(CONT_I_VOLUME, 9123);
    add_prop(OBJ_I_VALUE, 9);
    add_subloc("na");
    add_subloc_prop("na", CONT_I_MAX_WEIGHT, 2000);
    add_subloc_prop("na", CONT_I_MAX_VOLUME, 5000);

    add_prop(CONT_I_DONT_SHOW_CONTENTS, 1);
    set_owners(({RINDE_NPC+"pracownik_poczty"}));
}

string
dlugasny()
{
    string str;

    if(environment(this_object())->jestem_poczta_w_rinde())
    {
        str="Przy �cianie stoi ma�y, kwadratowy stolik";
        if(environment(this_object())->jest_rzecz_w_sublokacji(0,"drewniane proste krzes�o"))
            str+=", a do niego dosuni�te s� krzes�a na kt�rych mo�na usi��� i napisa� "+
                "list w spokoju";
    }
    else
         str="Jest to ma�y, kwadratowy stolik";

    str+=".";

    return str;
}
