/* Autor: Avard
   Opis : Arivia
   Data : 31.03.07 */
inherit "/std/container";

#include "dir.h"
#include "/sys/stdproperties.h"
#include "/sys/macros.h"
#include "/sys/pl.h"
#include "/sys/materialy.h"
#include "/sys/object_types.h"

void
create_container()
{
    ustaw_nazwe("lada");
    dodaj_przym("niski", "niscy");
    dodaj_przym("pod^lu^zny", "pod^lu^zni");
    ustaw_material(MATERIALY_DR_DAB);
    set_type(O_MEBLE);
    set_long("Niewielka wysoko^s^c tej drewniany lady, czyni j^a niezwykle "+
        "u^zyteczn^a, gdy^z nawet osoba niewielkiego wzrostu dosi^egnie do "+
        "niej bez wi^ekszych problem^ow. Mimo wszystko, mebel jest do^s^c "+
        "du^zych rozmiar^ow, ze wzgl^edu na jej d^lugo^s^c, kt^ora "+
        "gwarantuje obszern^a powierzchni^e. "+
        "@@opis_sublokacji|Dostrzegasz na niej |na|.\n|\n|" + PL_BIE+ "@@");
         
    add_prop(CONT_I_WEIGHT, 40000);
    add_prop(CONT_I_VOLUME, 40000);
    add_prop(OBJ_I_VALUE, 56);
    
    add_subloc("na");
    add_subloc_prop("na", CONT_I_MAX_WEIGHT, 100000);
    add_subloc_prop("na", CONT_I_MAX_VOLUME, 150000);
    add_prop(CONT_I_DONT_SHOW_CONTENTS, 1);
    add_prop(CONT_I_CANT_WLOZ_DO, 1);
    set_owners(({RINDE_NPC+"krawiec"}));

}

