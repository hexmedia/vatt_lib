/* stolik do karczmy Baszta, Rinde,
      Lil*/


#pragma strict_types

inherit "/std/container";

#include "dir.h"
#include <pl.h>
#include <macros.h>
#include <filter_funs.h>
#include <stdproperties.h>
#include <composite.h>
#include <cmdparse.h>
#include <object_types.h>

nomask void
create_container()
{
    ustaw_nazwe("stolik");
    dodaj_przym("niewielki","niewielcy");

    set_long("�w niewielki, chwiejny stolik s�u�y raczej jako dekoracja wn�trza. Niewiele "+
             "pomie�ci na swoim okr�g�ym blacie, a n�ka, na kt�rej stoi na pewno "+
             "nie zniesie wi�kszego ci�aru ni� naczynia. "+
             "@@opis_sublokacji| Na blacie |na|.|||le�y |le�� |le�y @@\n");

    /*add_item(({ "blat", "powierzchni� sto�u" }), "S�kata powierzchnia nie jest zbyt r�wna, jednak "+
                                  "na pewno utrzyma niejeden ci�ar.\n");*/

    setuid();
    seteuid(getuid());
    add_prop(CONT_I_MAX_VOLUME, 3000);
    add_prop(CONT_I_VOLUME, 1680);

    add_prop(CONT_I_CANT_WLOZ_DO, 1); /* By nie mo�na by�o nic do niego w�o�y� komend� 'wloz do' */

    add_prop(CONT_I_WEIGHT, 1000);
    add_prop(CONT_I_MAX_WEIGHT, 1500);

    add_prop(CONT_I_DONT_SHOW_CONTENTS, 1);

    /* dodajemy sublokacj� 'na', by mo�na by�o co� na st� k�a�� */
    add_subloc("na");

    make_me_sitable("przy","przy niewielkim stoliku","od niewielkiego stolika",4, PL_MIE, "przy");
    set_owners(({DAMIR,RINDE_NPC+"pomagacz_damira"}));

}

public int
query_type()
{
  return O_MEBLE;
}

public int
czy_jestem_stolikiem()
{
  return 1;
}
