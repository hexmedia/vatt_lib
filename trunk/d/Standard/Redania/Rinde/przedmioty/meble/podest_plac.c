/*
 *    Ot, zwyk�y pode�cik dla �piewaka na placu Rinde :)
 *                               Delvert&Lil&Jeremian. wrzesie�2005
 */

#pragma strict_types

inherit "/std/object";

#include "dir.h"
#include <pl.h>
#include <macros.h>
#include <filter_funs.h>
#include <stdproperties.h>
#include <composite.h>
#include <cmdparse.h>
#include <sit.h>
#include <object_types.h>
#include <materialy.h>

#define LIVE_O_ON_HORSE "_live_o_on_horse"


object na_podescie;


nomask void
create_object()
{
    ustaw_nazwe("podest");
    dodaj_przym("niedu�y","nieduzi");

    set_long("Kilka szerokich desek, skleconych napr�dce razem tworzy co� "+
             "w rodzaju prowizorycznej sceny. Deski wygl�daj� na stare, "+
	     "ale s� jeszcze mocne i zapewne utrzymaj� jeszcze niejeden "+
	     "ci�ar. Podest idealnie nadaje si� dla ulicznych herold�w, "+
	     "m�wc�w czy te� w�drownych grajk�w.\n");

    //add_item("deski podestu", "\n");
    
    
    ustaw_material(MATERIALY_DR_BUK);
    setuid();
    seteuid(getuid());
    add_prop(OBJ_I_VOLUME, 268000);
    add_prop(OBJ_I_VALUE, 1);
    add_prop(OBJ_I_WEIGHT, 100000);
    set_owners(({RINDE_NPC + "sekretarz"}));//FIXME dodac burmistrza
    na_podescie = 0;
}

public int
query_type()
{
	return O_MEBLE;
}

public int
jestem_nieduzym_podestem()
{
	return 1;
}

void
init()
{
    add_action("wejdz", "wejd�");
    add_action("zejdz", "zejd�");
    add_action("zepchnij","zepchnij");
    
    add_action("komendy", "", 1);
}

int
komendy(string str)
{
    string verb=query_verb();

    switch(verb)
    {
        case "usi�d�": 
                       if(na_podescie==TP)
                       {
                           write("Zejd� wpierw z podestu!\n");
                           return 1;
                       }
                       return 0;
        default: return 0;
    }
    return 0;
}

int
wejdz(string str)
{
    object *podest;
    int ret;

    notify_fail("Wejd� gdzie?\n");
    
    if (!strlen(str))
        return 0;
	
    if (this_player()->query_prop(LIVE_O_ON_HORSE))
    {
        notify_fail("Dosiadzasz przecie^z wierzchowca!\n");
        return 0;
    }

    if(this_player()->query_prop(SIT_SIEDZACY))
    {
        notify_fail("Przecie^z ju^z gdzie^s siedzisz.\n");
        return 0;
    }

    if (!parse_command(str, all_inventory(this_player()) +
	all_inventory(environment(this_player())),
        "'na' %i:"+ PL_BIE, podest))
        return 0;

    podest = NORMAL_ACCESS(podest, 0, 0);

    if (!sizeof(podest))
        return 0; 

    if (na_podescie == this_player())
    {
	notify_fail("Przecie^z jeste^s ju^z na pode^scie.\n");
	return 0;
    }

    if (na_podescie)
    {
	notify_fail("Na pode^scie ju^z kto^s jest.\n");
	return 0;
    }

    if (environment(this_player()) != environment(this_object()))
    {
	notify_fail("Musisz pierw od^lo^zy^c ten podest.\n");
	return 0;
    }

    na_podescie = this_player();
//    this_player()->add_prop(OBJ_I_DONT_GLANCE,1);
    this_player()->add_prop(LIVE_S_EXTRA_SHORT, " stoj^ac" +
	this_player()->koncowka("y", "a", "e") + " na pode^scie");
    this_player()->add_prop(LIVE_M_NO_MOVE, "Zejd^x najpierw z podestu.\n");
    this_object()->add_prop(OBJ_M_NO_GET,
	"Podest jest zaj^ety, nie mo^zesz go wzi^a^c.\n");
    write("Wchodzisz na "+this_object()->query_short(PL_BIE)+".\n");
    saybb(QCIMIE(this_player(),PL_MIA)+" wchodzi na "+
          this_object()->query_short(PL_BIE)+".\n");

    return 1;
}

int
zejdz(string str)
{
    object *podest;
    int ret;

    notify_fail("Zejd� sk�d?\n");
    
    if (!strlen(str))
        return 0;

    if (!parse_command(str, all_inventory(environment(this_player())),
        "'z' %i:"+ PL_DOP, podest))
        return 0;

    podest = NORMAL_ACCESS(podest, 0, 0);

    if (!sizeof(podest))
        return 0; 

    if (na_podescie != this_player())
    {
	notify_fail("Przecie^z nie jeste^s na pode^scie.\n");
	return 0;
    }

    na_podescie = 0;
//    this_player()->remove_prop(OBJ_I_DONT_GLANCE);
    this_player()->remove_prop(LIVE_S_EXTRA_SHORT);
    this_player()->remove_prop(LIVE_M_NO_MOVE);
    this_object()->remove_prop(OBJ_M_NO_GET);
    write("Schodzisz z "+this_object()->query_short(PL_DOP)+".\n");
    saybb(QCIMIE(this_player(),PL_MIA)+" schodzi z "+
          this_object()->query_short(PL_DOP)+".\n");

    return 1;    
}




int
zepchnij(string str)
{
    object* kogo;
    int ret;
    string kon;

    notify_fail("Zepchnij kogo sk�d?\n");
    
    if (!strlen(str))
        return 0;
	
    if (this_player()->query_prop(LIVE_O_ON_HORSE))
    {
        notify_fail("Je�li chcesz to uczyni�, musisz wpierw zej�� z wierzchowca.\n");
        return 0;
    }

    if(this_player()->query_prop(SIT_SIEDZACY))
    {
        notify_fail("Je�li chcesz to uczyni�, musisz wpierw wsta�.\n");
        return 0;
    }

    if (!parse_command(str, environment(this_player()),
        "%l:"+PL_BIE + " %s", kogo, kon))
        return 0;

    kogo = NORMAL_ACCESS(kogo, 0, 0);

    if (kon != "z podestu")
    {
            notify_fail("Zepchnij kogo sk�d?\n");
	    return 0;
    }

    if (!sizeof(kogo))
        return 0; 

    if (na_podescie == this_player())
    {
	notify_fail("Przecie� to ty jeste� na pode�cie!\n");
	return 0;
    }

    if (sizeof(kogo) != 1)
    {
	    notify_fail("Zdecyduj si�, kogo chcesz zepchn��!\n");
	    return 0;
    }

    if (na_podescie != kogo[0])
    {
	notify_fail("Ale� " + kogo[0]->short(PL_MIA) + " nie stoi na tym pode�cie.\n");
	return 0;
    }

    write("Spychasz "+kogo[0]->short(PL_BIE)+" z podestu.\n");
    tell_object(kogo[0], this_player()->query_Imie(kogo[0], PL_MIA) + " spycha ci� z " + this_object()->short(PL_DOP) + "!\n");
    saybb(QCIMIE(this_player(),PL_MIA)+" spycha "+kogo[0]->short(PL_BIE)+ ".\n", ({ kogo[0], this_player() }));
    na_podescie = 0;
    kogo[0]->remove_prop(LIVE_S_EXTRA_SHORT);
    kogo[0]->remove_prop(LIVE_M_NO_MOVE);
    this_object()->remove_prop(OBJ_M_NO_GET);

    return 1;
}





/*
void
signal_leave(object ob)
{
    if (ob == na_podescie)
    {
	set_this_player(na_podescie);
	this_player()->remove_prop(OBJ_I_DONT_GLANCE);
	this_object()->remove_prop(OBJ_M_NO_GET);
	write("Schodzisz z "+this_object()->query_short(PL_DOP)+".\n");
	tell_roombb(environment(this_object()), QCIMIE(this_player(),PL_MIA)+" schodzi z "+
    	    this_object()->query_short(PL_DOP)+".\n");
	na_podescie = 0;
    }
}
*/
