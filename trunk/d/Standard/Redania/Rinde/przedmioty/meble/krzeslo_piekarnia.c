inherit "/std/object.c";

#include "dir.h"
#include <pl.h>
#include <stdproperties.h>
#include <macros.h>
#include <filter_funs.h>
#include <materialy.h>

create_object()
{
    ustaw_nazwe("krzes�o");
                 
    dodaj_przym("drewniany","drewniani");
   
   make_me_sitable("na","na drewnianym krze�le","z krzes�a",1);
   ustaw_material(MATERIALY_DR_DAB);
   
    set_long("Zbite gwo�dziami deski tworz� te najzwyklejsze w �wiecie krzes�o.\n");
    add_prop(CONT_I_WEIGHT, 630);
    add_prop(OBJ_I_VALUE, 55);
  	add_prop(OBJ_I_WEIGHT, 6200);
  	add_prop(OBJ_I_VOLUME, 8100);
    set_owners(({RINDE_NPC+"piekarz"}));
}

public int
test_rwopk1()
{
        return 1;
}

