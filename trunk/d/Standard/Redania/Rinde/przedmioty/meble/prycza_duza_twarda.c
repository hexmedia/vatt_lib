/* Lozko do szpitala Rinde
   Made by Avard 10.06.06 */

inherit "/std/object.c";

#include "dir.h"
#include <pl.h>
#include <stdproperties.h>
#include <macros.h>
#include <filter_funs.h>
#include <materialy.h>
#include <object_types.h>

create_object()
{
    ustaw_nazwe("prycza");
                 
    dodaj_przym("du^zy","duzi");
    dodaj_przym("twardy","twardzi");
   
    make_me_sitable("na","na pryczy","z pryczy",4);

    set_long("Du^za, szpitalna prycza na kt^orej ka^zdy powinien si^e "+
        "zmie^sci^c. Nikt jednak nie powiedzia^l, ^ze b^edzie mu tutaj "+
        "wygodnie, poniewa^z jest wyj^atkowo twarde. Gdyby nie czysta, "+
        "bia^la po^sciel mo^znaby pomy^sle^c, ^ze jest to st^o^l.\n");
    add_prop(OBJ_I_WEIGHT, 10000);
    add_prop(OBJ_I_VOLUME, 10000);
    add_prop(OBJ_I_VALUE, 240);
    ustaw_material(MATERIALY_DR_BUK, 90);
    ustaw_material(MATERIALY_LEN, 10);
    set_type(O_MEBLE);
    set_owners(({RINDE_NPC + "sekretarz"}));//FIXME dodac burmistrza
                                            //i lekarza jakiegos?
}
public int
jestem_prycza_szpitalna()
{
        return 1;
}
