/* Artonul
 */
inherit "/std/armour";

#include <pl.h>
#include <wa_types.h>
#include <stdproperties.h>
#include <materialy.h>
#include <macros.h>
#include <object_types.h>

void
create_armour()
{
    ustaw_nazwe("spodnie");
   ustaw_nazwe_glowna("para");

    dodaj_przym("szary", "szarzy");
    dodaj_przym("prosty", "pro�ci");

    set_long("Spodnie, kt�re masz przed sob� s� na pewno dzie�em " +
	"do�wiadczonego krawca. Mimo i� wykonano je z niskiej jako�ci " +
	"jedwabiu i lnu to jednak dzi�ki temu, �e zastosowano przy ich " +
	"szyciu bardzo dobry kr�j b�d� na pewno dobrze i d�ugo s�u�y� " +
	"swemu u�ytkownikowi.\n");

    set_slots(A_LEGS);
   add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
   add_prop(OBJ_I_VALUE, 57);
   add_prop(OBJ_I_WEIGHT, 1440);
   add_prop(OBJ_I_VOLUME, 800);
   add_prop(ARMOUR_I_D_ROZCIAGLIWOSC, 30);
   set_type(O_UBRANIA);
   ustaw_material(MATERIALY_JEDWAB);
//    set_ac();
set_likely_cond(21);
}