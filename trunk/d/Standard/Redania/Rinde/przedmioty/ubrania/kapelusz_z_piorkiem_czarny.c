inherit "/std/armour";

#include <wa_types.h>
#include <formulas.h>
#include <stdproperties.h>
#include <macros.h>

void
create_armour()
{
    ustaw_nazwe("kapelusz z pi�rkiem");
    dodaj_nazwy("kapelusz");

    dodaj_przym("czarny", "czarni");

    set_long("Kapelusz wykonany zosta� z czarnego, zamszowego materia�u, " +
	"nieco usztywnionego od spodu. Do�� du�y jego rozmiar pozwala si� " +
	"domy�li�, �e tworzony by� dla os�b o sporych g�owach, lecz " +
	"czarny kolor optycznie go pomniejsza, sprawiaj�c �e wydaje si� " +
	"smuklejszy. Dodatkowo tu� nad jego rondkiem przeprowadzono " +
	"turkusow� wst��eczk�, a po prawej stronie wetkni�to jeszcze " +
	"d�ugie pr�gowane pi�rko.\n");

    set_slots(A_HEAD);
//    set_ac();
}