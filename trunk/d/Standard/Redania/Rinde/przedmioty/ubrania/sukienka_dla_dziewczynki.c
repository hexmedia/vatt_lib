/*zrobione przez Valora
z pomoca Faeve
*/

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <pl.h>
#include <macros.h>
#include <materialy.h>
#include <object_types.h>

void
create_armour()
{

ustaw_nazwe("sukienka");

    dodaj_przym("b^l^ekitny","b^l^ekitni");
    dodaj_przym("koronkowy", "koronkowi");
    set_long("Sukieneczk^e t^e uszyto ze zwiewnego, delikatnego materia^lu o "+
		"b^l^ekitnym kolorze. Przyozdobiona zosta^la licznymi, kolorowymi "+
		"koraliczkami uk^ladaj^acymi si^e w chaotyczne wzorki z przodu "+
		"sukienki. Idealnie skrojony str^oj zako^nczony jest bia^lymi "+
		"falbankami, kt^ore sprawiaj^a wra^zenie, jakby wiecznie falowa^ly na "+
		"wietrze.\n");

    ustaw_material(MATERIALY_JEDWAB, 85);
	ustaw_material(MATERIALY_DR_WIERZBA, 5);
	ustaw_material(MATERIALY_BAWELNA, 10);

    set_slots(A_TORSO, A_ARMS, A_LEGS, A_BODY);
    add_prop(OBJ_I_WEIGHT, 50);
    add_prop(OBJ_I_VOLUME, 60);
    add_prop(OBJ_I_VALUE, 300);
    add_prop(ARMOUR_I_D_ROZCIAGLIWOSC, 40);
    add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
    add_prop(ARMOUR_I_DLA_PLCI, 1);

    set_size("XS");
    set_type(O_UBRANIA);
}