
/* Autor: Avard
   Opis : Adves
   Data : 07.06.06 */

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <pl.h>
#include <macros.h>
#include <materialy.h>
#include <object_types.h>

void
create_armour()
{
    ustaw_nazwe("spodnie");
    ustaw_nazwe_glowna("para");

    dodaj_przym("d^lugi","d^ludzy");
    dodaj_przym("zielony","zieloni");
 

    set_long("Gruby materia^l, z jakiego wykonano te spodnie, jest koloru "+
        "g^lebokiej zieleni. D^lugie nogawki nie posiadaj^a niemal ^zadnych "+
        "zdobie^n, a jedynym urozmaiceniem tego ubrania jest nieco "+
        "niestarannie wykonany szew, ci^agn^acy si^e w miejscu, w kt^orym "+
        "krawiec zszy^l dwa kawa^lki materia^lu. W^sciek^l^ozielona nitka "+
        "zabawnie odznacza si^e na tle ciemniejszej zieleni, a tym samym "+
        "idealnie wsp^o^lgra z dolnym wyko^nczeniem spodni. Obszyte na dole "+
        "r^ownie jaskrawym kolorem prezentuj^a si^e do^s^c ciekawie "+
        "i z pewno^sci^a nie nadaj^a si^e na oficjalne przyj^ecia. Spodnie "+
        "te s^a bardzo d^lugie, dlatego te^z nogawki na dole s^a "+
        "poszarpane, a mn^ostwo cieniutkich niteczek nieustannie dotyka "+
        "ziemi.\n");

    set_slots(A_LEGS, A_HIPS);
    add_prop(OBJ_I_VOLUME, 1000);
    add_prop(OBJ_I_WEIGHT, 837);
    add_prop(OBJ_I_VALUE, 75);
    ustaw_material(MATERIALY_LEN, 100); 
    set_type(O_UBRANIA);
    add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    set_size("M");
}
