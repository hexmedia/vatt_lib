inherit "/std/armour";

#include <wa_types.h>
#include <formulas.h>
#include <stdproperties.h>
#include <macros.h>
#include <object_types.h>
#include <materialy.h>

void
create_armour()
{
    ustaw_nazwe("spodnie");
    ustaw_nazwe_glowna("para");

    dodaj_przym("ciemny", "ciemni");
    dodaj_przym("zwyk^ly", "zwykli");

    set_long("Spodnie wykonane zosta^ly z ciemnego " +
	"materia^lu, nieco szorstkiego w dotyku. Ma typowo m^eski kr^oj, " +
	"jednocze�nie nie posiada ^zadnych udziwnie^n ani zdobie^n, " +
	"sprawiaj�c ^ze spodnie te s� najzwyklejsze. Posiadaj� d^lugie " +
	"nogawki, a u g^ory umieszczono kilka szlufek na ewentualny pasek. " +
	"Dostrzegasz na nich liczne przetarcia, �wiadcz�ce o tym, jak " +
	"cz^esto by^ly noszone.\n");
    set_slots(A_LEGS, A_HIPS);
    ustaw_material(MATERIALY_LEN);
    set_type(O_UBRANIA);
    add_prop(OBJ_I_VOLUME, 1200);
    add_prop(OBJ_I_WEIGHT, 669);
    add_prop(OBJ_I_VALUE, 13);
    add_prop(ARMOUR_I_D_ROZCIAGLIWOSC, 30);
    add_prop(ARMOUR_I_U_ROZCIAGLIWOSC, 30);
    add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    set_size("M");
    set_likely_cond(22);
}
