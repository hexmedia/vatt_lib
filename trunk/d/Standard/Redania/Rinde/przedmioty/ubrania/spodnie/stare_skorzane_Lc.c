/*
 * spodnie dla pana Swiatynnego Straznika
 * Faevus w Halloween '06
 * 
 */ 

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <pl.h>
#include <macros.h>
#include <materialy.h>
#include <object_types.h>

void
create_armour()
{
    ustaw_nazwe("spodnie");
    ustaw_nazwe_glowna("para");
	
    dodaj_przym("stary","starzy");
    dodaj_przym("sk^orzany","sk^orzani");
	
    set_long("By^c mo^ze te sk^orzane spodnie kiedy^s by^ly "+
	"b^lyszcz^ace. By^c mo^ze - to chyba w^la^sciwe okre^slenie, bo "+
	"teraz s^a w wielu miejscach powycierane i pozszywane. Te "+
	"mankamenty oraz powypychane kolana i po^sladki pot^eguj^a "+
	"wra^zenie niechlujno^sci stroju. Nogawki zw^e^zono u do^lu tak, by "+
	"przylegaj^ac do ^lydek, mog^ly zosta^c wsuni^ete w buty, w pasie "+
	"za^s niedbale przyszyto kilka szlufek. Zapi^ecie tworz^a trzy "+
	"niedu^ze guziczki, przy czym ka^zdy inny jest od poprzedniego. "+
	"Jedno jest pewne - szanuj^aca si^e i dbaj^aca o sw^oj wizerunek "+
	"osoba nie za^lo^zy^laby takiego przyodziewku. \n");
	
    set_slots(A_LEGS, A_HIPS);
    add_prop(OBJ_I_VOLUME, 1000);
    add_prop(OBJ_I_WEIGHT, 837);
    add_prop(OBJ_I_VALUE, 75);
	
    ustaw_material(MATERIALY_SK_SWINIA, 100);
    set_type(O_UBRANIA);
    add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    set_size("L");
    set_likely_cond(23);
}