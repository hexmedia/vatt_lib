/*zrobione przez valor dnia 4 listopada 2006
*/
inherit "/std/armour";
#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <macros.h>
#include <materialy.h>
#include <object_types.h>
void
create_armour() 
{
     ustaw_nazwe_glowna("para");
     ustaw_nazwe("spodnie");
     dodaj_przym("p^l^ocienny","p^l^ocienni");
     dodaj_przym("granatowy","granatowi");
      
     set_long("Te zwyk^le, mieszcza^nskie spodnie posiadaj^a widoczne ^slady "+
         "u^zytkowania, lecz nadal wygl^adaj^a w miare schludnie, zapewne "+
         "dzi^eki sporej klamrze umieszczonej przy pasie oraz gustownie "+
         "podwini^etym nogawkom. Sporej wielko^sci zapi^ecie, wykonane z "+
         "miedzi nie^smia^lo b^lyszczy na tle granatowego materia^lu. "+
         "Na prawym udzie przyszyta zosta^la niewielka kieszonka zapinana na "+
         "guziczek, maj^aca za zadanie by^c g^l^ownie ozdob^a.\n");
     set_slots(A_LEGS, A_HIPS);
     add_prop(OBJ_I_VOLUME, 200);
     add_prop(OBJ_I_VALUE, 100);
     add_prop(OBJ_I_WEIGHT, 100);
     add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
     add_prop(ARMOUR_I_DLA_PLCI, 0);
     
     set_size("M");
     ustaw_material(MATERIALY_LEN, 100);
     set_type(O_UBRANIA);
}