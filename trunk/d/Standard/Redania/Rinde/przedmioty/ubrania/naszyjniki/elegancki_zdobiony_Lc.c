
/* elegancki zdobiony naszyjnik zielarza z Rinde
opis popelniony przez Tabakiste, na forum vattgherna 18.02.2007
zakodowany przez Seda 19.02.2007
*/

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <macros.h>
#include <materialy.h>
#include <object_types.h>
#include <pl.h>

void
create_armour()
{
    ustaw_nazwe("naszyjnik");
          
    dodaj_przym("elegancki","eleganccy");
    dodaj_przym("zdobiony","zdobieni");

    set_long("Medalion wykonany z ciemnego z^lota wykonano na kszta^lt klamry "
	+"obejmuj^acej niewielki, czerwony kamie^n. Dzi^eki niezwykle starannemu "
	+"oszlifowaniu na jego kraw^edziach pojawiaj^a si^e liczne refleksy ^swietlne."
	+" Delikatny ^la^ncuch, na kt^orym zawieszony jest wisior wykonano "
	+"z tego samego kruszcu, lecz jest on o ton ja^sniejszy. Ca^lo^s^c "
	+"zgrabnie si^e ze sob^a komponuje i z pewno^sci^a doda elegancji nosz^acej "
	+"go osobie. \n");

    set_slots(A_NECK);
    set_type(O_BIZUTERIA);
    add_prop(OBJ_I_WEIGHT, 50);
    add_prop(OBJ_I_VOLUME, 50);
    add_prop(OBJ_I_VALUE, 3000);
	add_prop(ARMOUR_I_NECKLACE, 1);
    add_prop(ARMOUR_I_U_ROZCIAGLIWOSC, 40);
    add_prop(ARMOUR_I_D_ROZCIAGLIWOSC, 40);
    ustaw_material(MATERIALY_ZLOTO, 80);
	ustaw_material(MATERIALY_RUBIN, 20);
    add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    set_size("M");

}
