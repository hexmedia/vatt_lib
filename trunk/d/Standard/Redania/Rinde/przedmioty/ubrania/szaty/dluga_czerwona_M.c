/* 
opis popelniony przez Faeve, zakodowany przez Seda 10.03.2007
*/

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <macros.h>
#include <materialy.h>

void
create_armour()
{  
   ustaw_nazwe("sukmana");
    dodaj_przym("d^lugi","dludzy");
    dodaj_przym("czerwony", "czerwoni");

    set_long("D^luga, si^egaj^aca nieco poni^zej kolan sukmana, zosta^la uszyta "
	+"z czerwonego, dobrej jako^sci, grubego jedwabiu. R^ekawy lekko "
	+"rozszerzaj^a si^e u do^lu, a d^lugo^s^c maj^a tak^a, by si^ega^c po^lowy palc^ow. "
	+"Na piersi materia^l jest dopasowany do cia^la, a od pasa w d^o^l - nieco "
	+"lu^xniejsza, tak, by nie kr^epowa^c ruch^ow. Szata nie jest w ^zaden spos^ob "
	+"zdobiona, ale mimo to wida^c, ^ze kosztowa^la niema^lo. Misternie "
	+"wyko^nczona nitk^a w tym samym odcieniu, co delikatna materia, nie wida^c "
	+"^zadnych niedoci^agni^e^c. Krawiec, kt^ory uszy^l odzienie, musia^l by^c "
	+"mistrzem w swoim fachu. \n");

    set_slots(A_TORSO | A_FOREARMS | A_ARMS);
    add_prop(OBJ_I_WEIGHT, 500);
    add_prop(OBJ_I_VOLUME, 2100);
    add_prop(OBJ_I_VALUE, 2000);

	set_size("M");
    add_prop(ARMOUR_F_PRZELICZNIK, 20.0);
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    ustaw_material(MATERIALY_TK_JEDWAB);
    set_likely_cond(18);
}