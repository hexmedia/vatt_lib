/*A to juz ciut wyzszej klasy spodnie^^ Lil */

inherit "/std/armour";

#include <pl.h>
#include <wa_types.h>
#include <stdproperties.h>
#include <materialy.h>
#include <macros.h>
#include <object_types.h>

void
create_armour()
{
   ustaw_nazwe("spodnie");
   ustaw_nazwe_glowna("para");

   dodaj_nazwy("para spodni");

   dodaj_przym("zielony", "zieloni" );
   dodaj_przym("obcis�y", "obci�li" );

   set_long("Pierwsze co rzuca si� w oczy to ich intensywna ziele�. Jakby "
          + "t�tni�ca �yciem oraz zaskakuj�ca sw� g��bi�, przywodz�ca na "
          + "my�l ��ki, drzewa, sk�pane w letnim s�o�cu. Drug� natomiast "
          + "ich zalet� jest kr�j tych�e. Wykonane s� bowiem z cienkiej "
          + "sk�ry mocno przylegaj�cej do cia�a, wr�cz opinaj�cej nogi i "
          + "wydawa�oby si�, �e kr�puj�cej nieznacznie ruchy. S� z pewno�ci� "
          + "wykonane specjalnie dla tych dam, kt�re nie preferuj� zakrywania "
          + "swych wdzi�k�w pod szerokimi sukniami.\n");

   set_ac(A_LEGS, 1, 2, 0);
   add_prop(OBJ_I_VOLUME, 407);
   add_prop(OBJ_I_VALUE, 134);
   add_prop(OBJ_I_WEIGHT, 330);
   add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
   set_size("M");

   set_type(O_UBRANIA);
   ustaw_material(MATERIALY_LEN);

   set_likely_cond(19);
   
}  
