inherit "/std/armour";

#include <pl.h>
#include <wa_types.h>
#include <stdproperties.h>
#include <materialy.h>
#include <macros.h>
#include <object_types.h>

void
create_armour()
{
    ustaw_nazwe("spodnie");
   ustaw_nazwe_glowna("para");

    dodaj_przym("jasny", "ja�ni");
    dodaj_przym("bufiasty", "bufia�ci");

    set_long("W miar� d�ugie spodnie zosta�y zrobione na osob� wysok�, " +
	"o d�ugich nogach. Ich miodowy kolor wraz z bufiastym krojem " +
	"sprawia, �e na ustach wielu os�b wykwita u�miech. Wykonane z " +
	"mi�kkiego w dotyku materia�u, zapewne z my�l� o osobach " +
	"lubi�cych odmian� i nieco finezyjny styl ubierania. Bufiaste " +
	"na udach, stercz� figlarnie, za� ich nogawki rozszerzaj� si� " +
	"u do�u nieznacznie. Dodatkowo wzd�u� bok�w ci�gnie si� ciemny " +
	"paseczek, b�d�cy jakby ozdobnikiem.\n");

    set_slots(A_LEGS);
//    set_ac();
   add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
   set_size("L");
   add_prop(OBJ_I_VALUE, 74);
   add_prop(OBJ_I_WEIGHT, 1476);
   add_prop(OBJ_I_VOLUME, 830);
   add_prop(ARMOUR_I_D_ROZCIAGLIWOSC, 30);
   set_type(O_UBRANIA);
   ustaw_material(MATERIALY_LEN);

   set_likely_cond(19);
}