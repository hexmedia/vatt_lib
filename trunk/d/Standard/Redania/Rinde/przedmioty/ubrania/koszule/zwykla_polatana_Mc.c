
/* Autor: Avard
   Opis : Arivia
   Data : 31.03.07 */

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <macros.h>
#include <materialy.h>
#include <object_types.h>
#include <pl.h>

void
create_armour()
{
    ustaw_nazwe("koszula");
    dodaj_przym("zwyk^ly","zwykli");
    dodaj_przym("po^latany","po^latani");
    set_long("Szary materia^l, z jakiego uszyto t^e koszul^e, nie "+
        "wyr^o^znia si^e niczym szczeg^olnym. Dosy^c przewiewne, bo "+
        "wykonane z bawe^lny ubranie, zosta^lo miejscami obszyte "+
        "niewielkimi ^latami, kt^ore zapewne kryj^a dziury w materiale. "+
        "Tuz pod szyj^a zwisaj^a swobodnie dwa sznureczki, kt^orymi "+
        "mo^zna zwi^aza^c koszul^e. \n");

    set_slots(A_TORSO, A_FOREARMS, A_ARMS);
    add_prop(OBJ_I_WEIGHT, 300);
    add_prop(OBJ_I_VOLUME, 200);
    add_prop(OBJ_I_VALUE, 45);
    set_type(O_UBRANIA);
    ustaw_material(MATERIALY_LEN, 100);
    add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    set_size("M");
    set_likely_cond(22);
}
