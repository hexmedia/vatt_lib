/*
 * D�ugi �nie�nobia�y szal.
 *
 * Autor: Alcyone
 *
 * Uzywany w:
 * - jako szal na zime dla kwiaciarki z placu Rinde.
 */

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <macros.h>
#include <materialy.h>
#include <object_types.h>

void
create_armour()
{ 
   ustaw_nazwe("szal");
    dodaj_przym("�nie�nobia�y","�nie�nobiali");
    dodaj_przym("d�ugi", "d�udzy");
    set_long("Delikatny jedwab, z kt�rego zosta� zrobiony, odbija "+
             "promienie �wiat�a niczym tafla wody. Nieskazitelna biel "+
             "mieni sie przy ka�dym ruchu, dzi�ki dziwnym kryszta�kom, "+
             "kt�re nieustannie na nim skrza. Jego d�ugo�� pozwala na "+
             "swobodne otulenie nim szyi, a przez to materia� lu�no opadnie "+
             "na plecy nosz�cej go osoby. Cieniutka srebrzysta ni�, kt�r� "+
             "obszyto szal, dodaje mu uroku i elegancji. Opr�cz tego nie "+
             "posiada ju� �adnych zdobie�, co wcale nie jest potrzebne, "+
             "gdy� i bez tego prezentuje si� niezwykle subtelnie.\n");

    add_item(({"kryszta�ki na szalu", "dziwne kryszta�ki na szalu"}),
               "Dziwne kryszta�ki pokrywaj� calutki szal, skrz�c " +
	       "si� niesamowicie przy ka�dym padaj�cym na� promieniu �wiat�a.\n");

    set_slots(A_NECK);
    set_type(O_UBRANIA);
    add_prop(OBJ_I_WEIGHT, 20);
    add_prop(OBJ_I_VOLUME, 40);
    add_prop(OBJ_I_VALUE, 130);
    set_likely_cond(15);

    /* ustawiamy rozci�gliwo�� w d� na 95% */
    add_prop(ARMOUR_I_D_ROZCIAGLIWOSC, 95);

    /* Ustawiamy materia�y, z kt�rego jest wykonany szal */
    ustaw_material(MATERIALY_JEDWAB); // defaultowo b�dzie to 100%
    
}