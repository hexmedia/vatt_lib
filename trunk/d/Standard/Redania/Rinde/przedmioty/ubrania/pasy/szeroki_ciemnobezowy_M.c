/*
opis popelniony przez Faeve, zakodowany przez Seda 05.03.2007
 */


inherit "/std/belt";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <macros.h>
#include <materialy.h>

void
create_belt()
{
    ustaw_nazwe("pas");
    dodaj_przym("szeroki","szerocy");
    dodaj_przym("ciemnobe^zowy", "ciemnobe^zowi");
    set_long("Ten szeroki, sk^orzany pas chyba mo^zna nazwa^c dzie^lem sztuki " +
        "kaletniczej. Ciemnobe^zowa sk^ora jest doskonale wyprawiona " +
        "i wyko^nczona. Pas zdobiony jest haftem - jakie^s artystyczne " +
        "esy-floresy wyszyte z^lot^a nici^a. Zapi^ecie stanowi prosta, ale " +
        "elegancko wykonana, mosi^e^zna klamra.\n");

    set_size("M");
    add_prop(OBJ_I_WEIGHT, 200);
    add_prop(OBJ_I_VOLUME, 200);
    add_prop(OBJ_I_VALUE, 300);

    /* ustawiamy rozci^agliwo^s^c w d^o^l na 80% */
    add_prop(ARMOUR_I_D_ROZCIAGLIWOSC, 80);

    /* Ustawiamy materia^ly, z kt^orych jest wykonany pas */
    ustaw_material(MATERIALY_RZ_MOSIADZ, 10);
    ustaw_material(MATERIALY_SK_SWINIA, 90);

    set_max_slots_zat(2);
    set_max_slots_prz(1);
    set_max_weight_zat(3800);
    set_max_weight_prz(900);
    set_max_volume_zat(2000);
    set_max_volume_prz(2000);
}
