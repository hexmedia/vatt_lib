
/* Autor: Avard
   Opis : Rantaur
   Data : 30.12.06 */

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <pl.h>
#include <macros.h>
#include <materialy.h>
#include <object_types.h>
void
create_armour()
{  
    ustaw_nazwe_glowna("para");
    ustaw_nazwe("buty");

    dodaj_przym("br^azowy","br^azowi");
    dodaj_przym("sznurowany","sznurowani");

    set_long("Na pierwszy rzut oka buty wydaj^a si^e by^c ca^lkiem "+
        "eleganckie, jasnobr^azowa sk^ora delikatnie rozprasza padaj^ace "+
        "na^n ^swiat^lo, sprawiaj^ac wra^zenie, i^z obuwie wygl^ada jak "+
        "nowe. Jednak gdy przygl^adasz im si^e dok^ladniej, dostrzegasz "+
        "liczne otarcia i zarysowania, tak^ze sznur^owki s^a ju^z nieco "+
        "poprzecierane. Najwyra^xniej w^la^sciwiel but^ow dba o nie, jak "+
        "gdyby by^ly one jego jedyna par^a, lecz to chyba dzieki temu "+
        "zachowa^ly one sw^oj dobry wygl^ad i u^zyteczno^s^c do tego "+
        "czasu.\n");
        
    set_slots(A_FEET);
    add_prop(OBJ_I_VOLUME, 1000);
    add_prop(OBJ_I_WEIGHT, 500);
    add_prop(OBJ_I_VALUE, 26);
    add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    set_size("M");
    ustaw_material(MATERIALY_SK_CIELE, 100);
    set_type(O_UBRANIA);
    set_likely_cond(21);
}