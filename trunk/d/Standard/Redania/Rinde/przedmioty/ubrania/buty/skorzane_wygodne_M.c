
/*
opis popelniony przez Faeve, zakodowany przez Seda 05.03.2007
*/

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <macros.h>
#include <materialy.h>


void
create_armour()
{  
    ustaw_nazwe_glowna("para");
    ustaw_nazwe("sandaly");

    dodaj_przym("sk�rzany","sk�rzani");
    dodaj_przym("wygodny","wygodni");

    set_long("Zwyk^le sk^orzane sanda^ly na korkowej, dobrze wyprofilowanej "
	+"podeszwie. Z pewno^sci^a ^swietnie sprawuj^a si^e podczas d^lugich "
	+"letnich w^edr^owek, podczas kt^orych nogom niezb^edna jest wygoda. "
	+"Stop^e utrzymuj^a rzemyki, na kt^ore buty s^a zawi^azane. Mo^zna "
	+"wi^aza^c je na r^o^zne sposoby - wok^o^l kostki lub te^z, jak kto "
	+"woli - a^z pod kolano, gdy^z d^lugo^s^c rzemienia i na to pozwala. \n");

    set_slots(A_FEET);
    add_prop(OBJ_I_WEIGHT, 120);
    add_prop(OBJ_I_VOLUME, 2400);
    add_prop(OBJ_I_VALUE, 400);
    
    /* buty na nodze tylko w niewielkim stopniu zwi^ekszaj^a swoj^a obj^eto^s^c */
    add_prop(ARMOUR_F_PRZELICZNIK, 10.0);
    /* ustalamy, ^ze buciki s^a dla mezczyzn */
	add_prop(ARMOUR_I_DLA_PLCI, 0);
    /* ustawiamy rozmiar */
	set_size("M");
	/* ustawiamy materia^l */
	ustaw_material(MATERIALY_SK_SWINIA, 90);
	ustaw_material(MATERIALY_SK_CIELE, 10);
    /* buciki troch^e bardziej powi^ekszaj^a rozmiar ni^z normalne ubrania */
    add_prop(ARMOUR_F_POW_DL_STOPA, 1.05);
    add_prop(ARMOUR_F_POW_SZ_STOPA, 1.05);
}