
/* Autor: Avard
   Opis : Tinardan
   Data : 21.05.06 */

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <pl.h>
#include <macros.h>
#include <materialy.h>
#include <object_types.h>
void
create_armour()
{
    ustaw_nazwe_glowna("para");
    ustaw_nazwe("buty");

    dodaj_przym("d^lugi","d^ludzy");
    dodaj_przym("czarny","czarni");

    set_long("Mi^ekka sk^ora, du^za wytrzyma^lo^s^c i staranne wyko^nczenie "+
        "^swiadcz^a, ^ze te buty uszy^l kto^s, kto naprawd^e zna^l si^e na "+
        "rzeczy. Ich cholewka si^ega przeci^etnemu cz^lowiekowi a^z do "+
        "po^lowy uda. U g^ory zdobi j^a pi^e^c srebrzonych klamr o "+
        "kszta^lcie prostok^ata pokrytymi delikatnym grawerowaniem. Po "+
        "wewn^etrznej stronie biegnie rzemienne wi^azanie. Podeszwa jest na "+
        "tyle cienka, ^ze stopa mo^ze si^e swobodnie porusza^c, ale "+
        "jednocze^snie wytrzyma^la i specjalnie wzmacniana.\n");

    set_slots(A_FEET);
    add_prop(OBJ_I_VOLUME, 1000);
    add_prop(OBJ_I_WEIGHT, 500);
    add_prop(OBJ_I_VALUE, 85);
    add_prop(ARMOUR_I_D_ROZCIAGLIWOSC, 30);
    add_prop(ARMOUR_I_U_ROZCIAGLIWOSC, 40);
    set_size("M");
    ustaw_material(MATERIALY_SK_SWINIA, 85);
    ustaw_material(MATERIALY_SREBRO, 15);
    set_type(O_UBRANIA);
    add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
    add_prop(ARMOUR_I_DLA_PLCI, 0);
}