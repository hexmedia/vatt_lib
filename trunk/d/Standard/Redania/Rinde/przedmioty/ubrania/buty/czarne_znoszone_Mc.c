
/* Autor: Avard
   Opis : Arivia
   Data : 31.03.07 */

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <pl.h>
#include <macros.h>
#include <materialy.h>
#include <object_types.h>

void
create_armour()
{  
    ustaw_nazwe_glowna("para");
    ustaw_nazwe("buty");

    dodaj_przym("czarny","czarni");
    dodaj_przym("znoszony","znoszeni");

    set_long("Wykonane z czarnej sk^ory buty wygl^adaj^a na bardzo solidne, "+
        "pomimo tego, ^ze zdaj^a si^e by^c w u^zytku od d^lugiego czasu. "+
        "Miejscami powycierane i zmatowia^le, zachowa^ly do^s^c zgrabny "+
        "kszta^lt, a gruba podeszwa nadal jest twarda i mocna. Ci^e^zkie "+
        "obuwie nadaje si^e do d^lugich wypraw, chroni^ac stopy przed "+
        "mrozem i znojem podr^o^zy.\n");
		
	set_slots(A_FEET);
	add_prop(OBJ_I_VOLUME, 1000);
	add_prop(OBJ_I_WEIGHT, 500);
    add_prop(OBJ_I_VALUE, 85);
    add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    add_prop(ARMOUR_I_D_ROZCIAGLIWOSC, 30);
    add_prop(ARMOUR_I_U_ROZCIAGLIWOSC, 40);
    set_size("M");
	ustaw_material(MATERIALY_SK_SWINIA);
	set_type(O_UBRANIA);
    set_likely_cond(23);
}
