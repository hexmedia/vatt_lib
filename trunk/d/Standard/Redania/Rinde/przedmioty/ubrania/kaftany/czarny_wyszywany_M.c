inherit "/std/armour";

//#include "/sys/formulas.h"
#include <macros.h>
#include <materialy.h>
#include <object_types.h>
#include <pl.h>
#include <stdproperties.h>
#include <wa_types.h>

void create_armour()
{               
    ustaw_nazwe("kaftan");
    dodaj_przym("czarny","czarni"); 
    dodaj_przym("wyszywany","wyszywani");

    set_long("Dolna cz�� kaftanu jest bogato pokryta srebrzystymi"
	+" zdobieniami przedstawiaj�cymi ga��zki ro�lin gdzieniegdzie"
	+" upstrzone drobnymi kwiatami. Wzory stopniowo przerzedzaj� si�"
	+" tak, �e ju� nieco powy�ej bioder zostaj� z nich jedynie dwie,"
	+" r�wnoleg�e linie. Kaftan z pozoru wygl�da na do�� lekki, jednak w"
	+" w rzeczywisto�ci wa�y ca�kiem sporo, by� mo�e wewn�trz"
	+" wzmocniony jest jakimi� metalowymi elementami.\n");
             
    set_slots(A_TORSO, A_FOREARMS, A_ARMS);
    set_ac(A_BODY | A_ARMS, 5, 10, 3);
    add_prop(OBJ_I_WEIGHT, 4300);
    add_prop(OBJ_I_VOLUME, 3150);
    add_prop(OBJ_I_VALUE, 80);
    set_type(O_UBRANIA);
    ustaw_material(MATERIALY_SK_SWINIA, 100);

    set_size("M");
    set_likely_cond(19);
}