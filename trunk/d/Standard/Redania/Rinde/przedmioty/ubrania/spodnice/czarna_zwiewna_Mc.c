/* Autor: Valor
   Opis: Arivia
   Dnia: 30.03.07 */
inherit "/std/armour";
#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <pl.h>
#include <macros.h>
#include <materialy.h>
#include <object_types.h>
void
create_armour()
{
    ustaw_nazwe("sp^odnica");
    dodaj_przym("czarny","czarni");
    dodaj_przym("zwiewny","zwiewni");
    set_long("Delikatny materia^l, przypominaj^acy zwiewn^a mgie^lk^e, "+
        "zdaje si^e mieni^c r^o^znymi odcieniami czerni przy ka^zdym, "+
        "nawet najdrobniejszym ruchu. Zwiewna sp^odnica si^ega do kostek, "+
        "rozszerzaj^ac si^e nieznacznie ku do^lowi, przez co nie kr^epuje "+
        "ruch^ow nosz^acej j^a osoby. Ciekawy kr^oj, podkre^slaj^acy kobiece "+
        "kszta^lty, dodaje strojowi elegancji i wdzi^eku, czyni^ac "+
        "w^la^scicielk^e bardziej atrakcyjn^a.  \n");
    set_slots(A_LEGS | A_HIPS);
    add_prop(OBJ_I_VOLUME, 1000);
    add_prop(OBJ_I_WEIGHT, 50);
    add_prop(OBJ_I_VALUE, 90);
    add_prop(ARMOUR_I_D_ROZCIAGLIWOSC, 30);
    add_prop(ARMOUR_I_U_ROZCIAGLIWOSC, 40);
    set_size("M");
    ustaw_material(MATERIALY_JEDWAB, 100);
    add_prop(ARMOUR_I_DLA_PLCI, 1);
    set_type(O_UBRANIA);
}
