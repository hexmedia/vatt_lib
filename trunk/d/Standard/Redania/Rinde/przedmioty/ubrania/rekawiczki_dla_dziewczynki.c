/*zrobione przez Valora 
wspolpraca z Faeve
*/

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <pl.h>
#include <macros.h>
#include <materialy.h>
#include <object_types.h>

void
create_armour()
{  
    
    ustaw_nazwe("r^ekawiczki");
    dodaj_przym("bia^ly","biali");
    dodaj_przym("aksamitny", "aksamitni");
    set_long("Te ma^le, aksamitne r^ekawiczki s^a uszyte na drobn^a kobiec^a "+
		"r^ek^e. Wykonano je ze ^snie^znobia^lej tkaniny, kt^ora ^latwo brudzi "+
		"si^e i niszczy, dlatego te^z nosz^a je g^l^ownie arystokratki. "+
		"Delikatny materia^l jest ^sliski i g^ladki, dzi^eki czemu r^ownie^z "+
		"mi^ly w dotyku, przyjemnie ^laskocz^ac sk^or^e d^loni.\n");
     
	ustaw_material(MATERIALY_AKSAMIT, 100);
	
    set_slots(A_HANDS);
    add_prop(OBJ_I_WEIGHT, 15);
    add_prop(OBJ_I_VOLUME, 10);
    add_prop(OBJ_I_VALUE, 100);
    add_prop(ARMOUR_I_D_ROZCIAGLIWOSC, 30);
    add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
    add_prop(ARMOUR_I_DLA_PLCI, 1);

    set_size("XS");
    set_type(O_UBRANIA);
    set_likely_cond(19);

}