/*
opis popelniony przez Faeve, zakodowany przez Seda 12.03.2007
*/

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <macros.h>
#include <materialy.h>
#include <object_types.h>
#include <pl.h>

void
create_armour()
{
    ustaw_nazwe("sygnet");
          
    dodaj_przym("du�y","duzi");
    dodaj_przym("z^loty","z^loci");

    set_long("Ten do^s^c pot^e^zny sygnet wykonany jest ze z^lota pierwszej pr^oby. "
	+"Jednak trzeba przyzna^c, ^ze wykonany jest "
	+"po mistrzowsku i z niebywa^l^a precyzj^a. Sama obr^aczka jest prosta, "
	+"natomiast jej zwie^nczeniem jest do^s^c du^zej wielko^sci turkus, na kt^orym "
	+"misternie wykonano monogram przedstawiaj^acy znak cechu kupieckiego, "
	+"kt^ory dodatkowo poz^locono.\n");

    set_slots(A_ANY_FINGER);
    set_type(O_BIZUTERIA);
    add_prop(OBJ_I_WEIGHT, 40);
    add_prop(OBJ_I_VOLUME, 25);
    add_prop(OBJ_I_VALUE, 25);
    add_prop(ARMOUR_I_U_ROZCIAGLIWOSC, 5);
    add_prop(ARMOUR_I_D_ROZCIAGLIWOSC, 5);
    ustaw_material(MATERIALY_ZLOTO, 80);
	ustaw_material(MATERIALY_SZ_TURKUS, 20);
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    set_size("L");
}