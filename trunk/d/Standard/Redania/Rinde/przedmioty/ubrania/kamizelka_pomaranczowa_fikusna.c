inherit "/std/armour";

#include <wa_types.h>
#include <formulas.h>
#include <stdproperties.h>
#include <macros.h>

void
create_armour()
{
    ustaw_nazwe("kamizelka");

    dodaj_przym("pomara�czowy", "pomara�czowi");
    dodaj_przym("fiku�ny", "fiku�ni");

    set_long("Kamizelka nie by�a by jaka� tam nadzwyczajna, gdyby nie " +
	"jej do�� niezwyk�y kolor. Mianowicie zabarwiona zosta�a na " +
	"jaskrawy, rzucaj�cy si� w oczy pomara�cz. Smuk�y kr�j, zgrabnie " +
	"dopasowuje si� do sylwetki w�a�ciciela, sprytnie uwidaczniaj�c " +
	"barki, optycznie czyni�c je bardziej atrakcyjnymi. Kamizelka " +
	"nieco rozszerza si� u do�u, by lu�no opada� przy pasie. " +
	"Dodatkowo przyszyto do niej czarne guziczki z fiku�nymi wzorami " +
	"narysowanymi na nich, r�wnie� pomara�czowym barwnikiem.\n");

    set_slots(A_BODY);
//    set_ac();
}