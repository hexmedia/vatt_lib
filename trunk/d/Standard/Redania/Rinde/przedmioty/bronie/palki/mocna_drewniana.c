/*
 * dla stra�nika swi�tynnego
 * by faeve
 */

inherit "/std/weapon";

#include <formulas.h>
#include <stdproperties.h>
#include <wa_types.h>
#include <object_types.h>
#include <materialy.h>
#include <pl.h>


void
create_weapon()
{
    ustaw_nazwe("pa�ka");
       
    dodaj_przym("mocny","mocni");
    dodaj_przym("drewniany","drewniani");
    
set_long("Ten do^s^c niedbale ociosany kawa^lek drewna mo^ze pos^lu^zy^c "+
"za ca^lkiem dobr^a bro^n - jeden z ko^nc^ow - ten grubszy, dla wzmocnienia "+
"si^ly ciosu, obito kawa^lkiem metalu, przymocowanym do pa^lki jakimi^s "+
"gwo^xdziami, stercz^acymi na boki niczym ^cwieki. Aby zapewni^c dobry chwyt, "+
"cz^e^s^c s^lu^z^aca za \"r^ekoje^s^c\" zosta^la poznaczona poprzecznymi "+
"wci^eciami, dzi^eki kt^orym bro^n nie wy^slizgnie si^e tak ^latwo z silnej "+
"r^eki. \n");
  
    
    set_hit(17); 
    set_pen(19); 
    
    set_wt(W_CLUB);
    set_dt(W_BLUDGEON);
    
    set_hands(A_ANY_HAND);
    ustaw_material(MATERIALY_ZELAZO, 10);
    ustaw_material(MATERIALY_DR_LIPA, 90);
    set_type(O_BRON_MACZUGI);
    add_prop(OBJ_I_WEIGHT, 1000); 
    add_prop(OBJ_I_VOLUME, 9000);
    add_prop(OBJ_I_VALUE, 107);    
    
}

