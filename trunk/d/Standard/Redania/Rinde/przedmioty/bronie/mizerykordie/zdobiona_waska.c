
/* Zdobiony waski sztylet
Wykonany dnia 06.06.06 przez Avarda 
Opis by Fenek z BO */
/* Uzyte w:
Straganiarz z Rinde (/d/Standard/Redania/Rinde/npc/straganiarz.c)
*/

inherit "/std/weapon";

#include "/sys/formulas.h"
#include <stdproperties.h>
#include <wa_types.h>
#include <pl.h>
#include <materialy.h>
#include <object_types.h>

void
create_weapon()
{
	ustaw_nazwe("mizerykordia");

     dodaj_przym("zdobiony","zdobieni");
     dodaj_przym("w^aski","w^ascy");

     set_long("To doskona^ly okaz sztyletu ^l^acz^acy w sobie zar^owno "+
         "walory estetyczne jak i u^zytkowe. Wyj^atkowo cienkie, d^lugie, "+
         "ale mocne ostrze sprawia wra^zenie, jakby w^la^sciciel chcia^l "+
         "szybko i bezbole^snie zada^c ofierze ^smiertelne pchni^ecie. "+
         "Dodatkowo smuk^la, wywa^zona rekoje^s^c z wygrawerowanym "+
         "wizerunkiem w^e^za i ozdobiona trzema krwistoczerwonymi "+
         "rubinami skladaj^a si^e na obraz pi^eknej i ^smiertelnie "+
         "skutecznej broni.\n");
	 
	 set_hit(23);
	 set_pen(14);
	 set_hands(A_ANY_HAND);

        add_prop(OBJ_I_WEIGHT, 700);
        add_prop(OBJ_I_VOLUME, 700);
        add_prop(OBJ_I_VALUE, 4600);

	 ustaw_material(MATERIALY_STAL, 75);
	 ustaw_material(MATERIALY_SK_CIELE, 10);
     ustaw_material(MATERIALY_RUBIN, 15);
	 set_type(O_BRON_SZTYLETY);
     set_wt(W_KNIFE);
	 set_dt(W_IMPALE);
}