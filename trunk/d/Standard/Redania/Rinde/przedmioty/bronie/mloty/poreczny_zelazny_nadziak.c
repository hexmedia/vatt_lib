inherit "/std/weapon";

#include <stdproperties.h>
#include <object_types.h>
#include <materialy.h>
#include <wa_types.h>

void create_weapon()
{
	ustaw_nazwe("nadziak");
	dodaj_przym("por�czny", "por�czni");
	dodaj_przym("�elazny", "�ela�ni");
	set_long("To na co teraz spogl�dasz nie jest niczym innym jak nadziakiem."
		+" Kr�tki zaostrzony szpikulec z jednej strony oraz �redniej wielkosci obuszek,"
		+" sprawiaja, �e owa bro� jest bardzo por�czna i praktyczna. Za pomoc� nadziaka"
		+" mo�na przebija� wszelkie kolczugi, natomiast obuszek mo�e przyda� si� w"
		+" rozbijanu co l�ejszych helmow.\n");
	
	set_hit(22);
	set_pen(25);

	add_prop(OBJ_I_WEIGHT, 7805);
	add_prop(OBJ_I_VOLUME, 4320);
	add_prop(OBJ_I_VALUE, 2340);

	set_wt(W_WARHAMMER);
	set_dt(W_BLUDGEON | W_IMPALE);
	
	set_hands(A_HANDS);
	ustaw_material(MATERIALY_ZELAZO, 100);
	set_type(O_BRON_MLOTY);
}
