/* made by valor 11 lipiec 2006  */

inherit "/std/weapon";

#include "/sys/formulas.h"
#include <stdproperties.h>
#include <wa_types.h>
#include <pl.h>
#include <object_types.h>
#include <materialy.h>

void
create_weapon()
{
    ustaw_nazwe("mieczyk");
   dodaj_przym("drewniany","drewniani");
   dodaj_przym("zabawkowy","zabawkowi");
   set_long("Drewniany mieczyk zbity zosta^l z dw^och p^laskich desek. "+
   "R^ekoje^s^c miecza owini^eta jest mi^ekk^a sk^ork^a, a cz^e^s^c "+
   "imituj^aca ostrze zastrugana na kszta^lt tr^ojk^ata.\n");

   set_hit(15);
   set_pen(1);

   set_wt(W_SWORD);
   set_dt(W_SLASH);
   set_hands(A_ANY_HAND);
   add_prop(OBJ_I_WEIGHT, 30);
   add_prop(OBJ_I_VALUE, 20);
   add_prop(OBJ_I_VOLUME, 5);
   ustaw_material(MATERIALY_DR_DAB, 100);
   set_type(O_BRON_MIECZE);
}

