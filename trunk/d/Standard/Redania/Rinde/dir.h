#include "../dir.h"

#define POCHODZENIE                 "z Rinde"
#define BURMISTRZ                   "Silvio"
#define BURMISTRZA                  "Silvia"
#define BURMISTRZ_N                 "Neville"
#define BURMISTRZA_N                "Neville'a"
#define BURMISTRZ_OPT               "M. "

#define RINDE_STD                   (RINDE + "std/rinde.c")
#define RINDE_NPC_STD               (RINDE + "std/npc.c")
#define STREET_STD                  (RINDE + "std/street.c")

//#define RINDE_PATROL        (RINDE + "patrol/admin.c") a po co to?
#define RINDE_PATROL        (RINDE + "patrol/admin.c")
//a �eby� si� kurwa pyta�, dupo! Vera.



#define RINDE_BRONIE                (RINDE + "przedmioty/bronie/")
#define RINDE_ZBROJE                (RINDE + "przedmioty/zbroje/")
#define RINDE_POCHWY                (RINDE + "przedmioty/pochwy/")
#define RINDE_NARZEDZIA             (RINDE + "przedmioty/narzedzia/")
#define MEBLE                       ("/d/Standard/items/meble/")
#define MEBLER                      (RINDE + "przedmioty/meble/")
#define RINDE_PRZEDMIOTY            (RINDE + "przedmioty/")

#define DAMIR                       (RINDE_NPC + "karczmarz_baszty")

#define CENTRUM                     (RINDE + "Centrum/")
#define CENTRUM_LOKACJE             (CENTRUM + "lokacje/")
#define CENTRUM_DRZWI               (CENTRUM + "drzwi/")
#define CENTRUM_OBIEKTY             (CENTRUM + "obiekty/")

#define POLNOC                      (RINDE + "Polnoc/")
#define POLNOC_LOKACJE              (POLNOC + "lokacje/")
#define POLNOC_DRZWI                (POLNOC + "drzwi/")
#define POLNOC_OBIEKTY              (POLNOC + "obiekty/")

#define POLUDNIE_DRZWI              (POLUDNIE + "drzwi/")
#define POLUDNIE_OBIEKTY            (POLUDNIE + "obiekty/")

#define WSCHOD                      (RINDE + "Wschod/")
#define WSCHOD_LOKACJE              (WSCHOD + "lokacje/")
#define WSCHOD_DRZWI                (WSCHOD + "drzwi/")
#define WSCHOD_OBIEKTY              (WSCHOD + "obiekty/")

#define ZACHOD                      (RINDE + "Zachod/")
#define ZACHOD_LOKACJE              (ZACHOD + "lokacje/")
#define ZACHOD_DRZWI                (ZACHOD + "drzwi/")
#define ZACHOD_OBIEKTY              (ZACHOD + "obiekty/")

#define SWIATYNIA                   (ZACHOD + "Swiatynia/")

#define GARNIZON                    (WSCHOD + "Garnizon/")
#define GARNIZON_LOKACJE            (GARNIZON + "lokacje/")
#define GARNIZON_DRZWI              (GARNIZON + "drzwi/")
#define GARNIZON_OBIEKTY            (GARNIZON + "obiekty/")

#define SZPITAL                     (WSCHOD + "Szpital/")
#define SZPITAL_LOKACJE             (SZPITAL + "lokacje/")
#define SZPITAL_DRZWI               (SZPITAL + "drzwi/")
#define SZPITAL_OBIEKTY             (SZPITAL + "obiekty/")

#define BIBLIOTEKA                  (POLUDNIE + "Biblioteka/")
#define RINDE_BIBLIOTEKA            (BIBLIOTEKA + "lokacje/")
#define RINDE_BIBLIOTEKA_DRZWI      (RINDE_BIBLIOTEKA + "drzwi/")
#define RINDE_BIBLIOTEKA_PRZEDMIOTY (BIBLIOTEKA + "przedmioty/")
#define RINDE_BIBLIOTEKA_UBRANIA    (BIBLIOTEKA + "ubrania/")

#define RINDE_SLEEP_PLACE           (POLNOC + "Karczma/lokacje/start.c")
