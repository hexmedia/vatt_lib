/* Opisy by Alcyone */
#include <stdproperties.h>
#include "dir.h"

inherit STREET_STD;

void
create_street()
{
    set_short("Szeroka ulica");

    add_item(({"budynki", "domki"}),
        "Wznosz^ace si^e dooko^la ma^le domki mieszkalne nie przyci^agaj^a " +
        "zbytnio niczyjej uwagi. Wi^eksze zainteresowanie budz^a wysokie " +
        "budynki, kt^ore wyra^xnie odznaczaj^a si^e w^sr^od niskich dach^ow. " +
        "Pe^lni^a one funkcj^e wa^zniejszych o^srodk^ow miasta.\n");
    add_item("placyk",
        "Id^ac w kierunku wschodnim mo^zna doj^s^c do jednego z mniejszych " +
        "placyk^ow Rinde, na kt^orym za dnia panuje spory t^lok.\n");
    add_item(({"bram^e", "mury"}),
        "Wznosz^aca si^e na po^ludniu pot^e^zna brama miasta pilnowana jest " +
        "przez kilku ros^lych stra^znik^ow. Ca^le Rinde otoczone jest wysokimi " +
        "murami z ciemnej ceg^ly, kt^ore przyt^laczaj^a swoj^a wielko^sci^a i " +
        "izoluj^a miasto przed wrogimi najazdami.\n");
    add_item("drog^e",
        "Drog^e wy^lo^zono w tym miejscu ciemnymi kamieniami. Zrobiono to " +
        "do^s� starannie, gdy^z u^lo^zono je r^owno i z widoczn^a, rzucaj^ac^a si^a " +
        "w oczy dok^ladno^sci^a.\n");

    add_event("Obok ciebie przesz^lo kilka os^ob zataczaj^acych si^e lekko.\n");
    add_event("Z oddali dochodz^a g^lo^sne krzyki.\n");
    add_event("Z oddali dochodzi g^lo^sne nawo^lywanie.\n");
    add_event("Ma^le dziecko przebieg^lo obok ciebie szturchaj^ac ci^e przypadkiem.\n");
    add_event("Pod twoimi nogami przebieg^la szara mysz.\n");
    add_event("Mijaj^acy ci^e ros^ly m^e^zczyzna zmierzy^l ci^e od niechcenia wzrokiem.\n");
    add_event("W oddali rozleg^ly sie g^lo^sne chichoty.\n");

    add_exit("ulica1","p^o^lnoc");
    add_exit("bramas","po^ludnie");

    add_object(RINDE+"przedmioty/lampa_uliczna");
}
public string
exits_description()
{
    return "Ulica prowadzi na p^o^lnoc oraz na po^ludnie w stron^e "+
           "bramy miasta.\n";
}

string
dlugi_opis()
{
    string str;

    str = "Brukowana ciemnym kamieniem ulica w tym miejscu ponownie si^e " +
        "poszerza. Biegnie z p^o^lnocy od strony g^l^ownego i zat^loczonego " +
        "za dnia placu Rinde. Ko^nczy si^e biegn^ac na po^ludnie w stron^e " +
        "wysokiej bramy, kt^orej zarys maluje si^e w oddali jak i pot^e^zne " +
        "mury otaczaj^ace ca^le miasto. Niedaleko, na p^o^lnocnym-wschodzie " +
        "mo^zna dostrzec do^s� spory budynek wyr^o^zniaj^acy si^e spo^sr^od " +
        "licznych, drobnych zabudowa^n. Nawet tutaj mo^zna dostrzec, ^ze to " +
        "miejsce jest do^s� cz^esto odwiedzane, co mo^zna stwierdzi� po " +
        "wiecznie otwieraj^acych si^e drzwiach, kt^ore s^a na tyle du^ze, i^z " +
        "mo^zna dostrzec tu ich kszta^lty. Uliczka ta nie wyr^o^znia si^e " +
        "niczym specjalnym. Jedyne co przyci^aga wzrok w tym miejscu to " +
        "do^s� starannie wy^lo^zona ciemnymi kamieniami droga.\n";

    return str;
}
