// do przejrzenia
// opisy zenujace, do sth
// ktora to juz fontanna w rinde??
/*
 * BO, zwierzaczek
 */
#include <stdproperties.h>
#include "dir.h"
#include <mudtime.h>
#include <macros.h>

inherit STREET_STD;
inherit "/lib/drink_water";

string dlugi_opis();

void create_street()
{
    set_short("Placyk");

    set_long(&dlugi_opis());

    add_item("fontann^e",
        "Niewielkich rozmiar^ow fontanna ma wystarczaj�c� ilo�^c " +
        "wody, by ka^zdy przechodzie^n m^og^l si^e z niej napi^c. " +
        "Na jej �rodku wyrze�biono niewielki pos�^zek " +
        "przedstawiaj�cy smuk^l� rybk^e, z kt^orej tryska " +
        "swie^za woda.\n");
    add_item("bibliotek^e","Na pierwszy rzut oka nie wyr^o^znia si^e niczym "+
        "spo^sr^od innych kamienic. ^Sciany otynkowane na bia^lo, jedynie "+
        "na rogach pomalowany na jaki^s mleczny odcie^n be^zu, dach pokryty "+
        "czerwon^a dach^owk^a. Jednak tym, co rzuca si^e do^s^c mocno w "+
        "oczy s^a pot^e^zne, dwuskrzyd^lowe drzwi wykonane z pomalowanego "+
        "na ciemny br^az drewna, po ich lewej stronie znajduje si^e szyld "+
        "informuj^acy o tym, i^z budynek ten nie jest zwyk^lym domkiem, a "+
        "gmachem Biblioteki Miejskiej.\n");
    add_item("szyld","Ot - tablica wykonana z po^l^aczonych ze sob^a kilku "+
        "desek, pomalowanych na ciemny br^az - tak, by pasowa^la do drzwi i "+
        "okien budynku, na kt^orym wisi. Misternie wylane z mosi^adzu "+
        "litery uk^ladaj^a si^e w napis: Biblioteka Miejska w Rinde.\n");

    add_event("Woda w fontannie tryska weso^lo zach^ecaj�c do " +
        "napicia si^e jej.\n");
    add_event("Niewielki ptaszek przylecia^l, by napi^c si^e " +
        "troszk^e wody.\n");
    add_event("Jeden z mieszka^nc^ow podszed^l do fontanny i nabra^l " +
        "troch^e wody do wiaderka, po czym odszed^l.\n");
    add_event("Grupka m^lodzie^zy na chwil^e przystan^e^la niedaleko " +
        "ciebie, g^lo�no �miej�c si^e i rozmawiaj�c.\n");
    add_event("Kupiec z dostaw� towar^ow przebieg^l obok, ci^e^zko " +
        "przy tym dysz�c.\n");
    set_drink_places("z fontanny");

    add_exit("placne.c","p^o^lnoc");
    add_exit("placnw.c","p^o^lnocny-zach^od");
    add_exit("placsw.c","zach^od");
    add_exit("ulica9.c","wsch^od");
    add_exit(BIBLIOTEKA + "lokacje/przedsionek.c",
             ({({"przedsionek", "biblioteka"}), "wchodzi do przedsionku biblioteki", "przybywa z przedsionka biblioteki"}), 0, 0);

    add_object(RINDE_BIBLIOTEKA_DRZWI + "do_biblioteki");
    add_object("/std/lampa_uliczna.c");

    add_prop(ROOM_I_INSIDE,0);
}

public string
exits_description()
{
    return "Dalsza cz^e^s^c placu widoczna jest na zachodzie, "+
           "p^o^lnocy i p^o^lnocnym-zachodzie, za^s na wschodzie znajduje "+
           "si^e ulica. Na po^ludniu widniej� niewielkie drzwi nad, kt^orymi " +
           "wiatr ko^lysze szyldem oznajmiaj�cym, i^z jest to biblioteka.\n";
}
string
dlugi_opis()
{
    return "Bez trudu z tego miejsca mo^zesz " +
        "ogarn�^c wzrokiem reszt^e placu. Dostrzegasz " +
        "na p^o^lnocy ciemny pos�g oraz mniej wi^ecej naprzeciw " +
        "niego ca^lkiem poka�ny skalniaczek z ro^slinami. Po stronie " +
        "wschodniej za�, placyk przeistacza si^e w miejsk� uliczk^e " +
        "prowadz�c^a w g^l�b miasta. Niedaleko dostrzegasz niewielk� " +
        "fontann^e, tryskaj�c� �wie^z� wod�. Od czasu do czasu jaki� " +
        "zb^l�kany ptaszek siada na jej brzegu i pije troch^e, " +
        "odlatuj�c po chwili rado�nie. Zapewne nie tylko on gasi " +
        "przy niej swoje pragnienie.\n";
}

void
init()
{
    ::init();

    init_drink_water();
}
void
drink_effect(string str)
{
    write("Pijesz nieco wody z fontanny. \n");
    saybb(QCIMIE(this_player(), PL_MIA) + " pije nieco wody z fontanny. \n");
}
