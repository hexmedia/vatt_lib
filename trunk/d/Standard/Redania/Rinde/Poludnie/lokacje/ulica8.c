/* Ot, typowa ulica w rinde :P
                                Lil. 3.10.2005 */

#include <stdproperties.h>
#include "dir.h"
#include <mudtime.h>

inherit STREET_STD;

void create_street()
{
	set_short("Krzywa zaniedbana ulica");

	add_exit(ZACHOD_LOKACJE + "ulica2.c","p^o^lnoc");
	add_exit("ulica7.c","po^ludnie");

	add_prop(ROOM_I_INSIDE,0);
	add_sit("na ziemi","na brudnej ziemi","z ziemi",0);

	add_item(({"ulic�","uliczk�","ziemi�","zab�ocon� ziemi�",
	          "brudn� ziemi�"}), "Smr�d bij�cy od niej jest "+
		  "wprost niewyobra�alny. Ca�y brud mieszka�c�w "+
		  "wy�azi przez te ich drewniane, krzywe domki "+
		  "i tutaj w�a�nie go�ci. Ulica wiedzie od "+
		  "po�udniowej bramy, a� tutaj, do miejsca "+
		  "zetkni�cia si� z poprzeczn�, wybrukowan� ju� ulic�.\n");
	add_item(({"chatki","chaty","domy"}), "Niekt�re z "+
	           "drewnianych, starych i sple�nia�ych "+
		   "domk�w nieumiej�tnie zbito deskami, "+
		   "lub nad�arto z�bem czasu. B�ony tych�e "+
		   "chat ju� chyba sa tak brudne, �e "+
		   "nie dotrze do wn�trza �aden promie� s�o�ca.\n");
	add_item(({"�ciany","�ciany dom�w","�ciany chat","�ciany chatek",
	           "�ciany drewnianych chatek","�ciany drewnianych chat",
		   "�ciany drewnianych dom�w"}), "Krzywe �ciany domostw "+
		   "nad�era od spodu szarawa ple��.\n");
	add_item(({"ple��","ple�� na �cianach"}), "Szara, miekka ple�� "+
	           "pokrywa te �ciany i zdaje si� nie zaprzestawa� nigdy "+
		   "swej ekspansji, pn�c si� coraz wy�ej i wy�ej. Mo�e jak "+
		   "odwiedzisz te miejsce za kilka lat chaty b�d� ju� ca�kowicie "+
		   "z�arte przez t� zaraz�.\n");
	add_item(({"zasmolone b�ony","zasmolone b�ony domostw",
	           "zasmolone b�ony dom�w","zasmolone b�ony chat",
		   "zasmolone b�ony chatek","b�ony domostw", "b�ony dom�w",
		   "b�ony","b�ony chat","b�ony chatek"}), "Niewielkie, "+
		   "pojedyncze b�onki s� ju� prawie ca�kowicie czarne i "+
		   "skutecznie blokuj� promieniom s�onecznym dost�p do "+
		   "wn�trza chat.\n");
}
public string
exits_description()
{
    return "Ulica prowadzi na p^o^lnoc i po^ludnie.\n";
}

string
dlugi_opis()
{
    string str;
    str ="Nier�wna w�ska uliczka jeszcze kawa�ek wiedzie na "+
         "p�noc mi�dzy skromnymi, ciasno umiejscowionymi "+
	 "chatkami, by na ko�cu wp�yn�� do znacznie wi�kszej, "+
	 "biegn�cej prostopadle, wybrukowanej ulicy. �ciany "+
	 "drewnianych chat ogarnia od ziemi szara ple��, co "+
	 "razem z zasmolonymi b�onami maj�cymi chyba za "+
	 "zadanie dostarcza� �wiat�o do wn�trz sk�ada si� "+
	 "na wizerunek najubo�szych zamieszkiwanych dom�w "+
	 "w ca�ym Rinde. ";
    if(pora_roku() != MT_ZIMA)
      str+="Na domiar tego, marno�ci dodaje b�otnista, "+
           "brudna ziemia, od kt�rej bije niesamowity wr�cz od�r "+
	   "ekskrement�w. Mieszka�cy tej uliczki zdaje si� "+
	   "nie maj� w�asnych wychodk�w ponadto, wyra�nie "+
	   "widoczne jest tu ich upodobanie do oddawania "+
	   "ka�u przed, a nie za domem. C�, chc�c niechc�c, "+
	   "to, co dociera do ciebie przez zmys� wzroku i "+
	   "w�chu pobudza twoj� wyobra�ni� - Czym oni si� "+
	   "�ywi� do cholery?";
    else
      str+="Na domiar tego, marno�ci dodaje zmarzni�ta, "+
           "brudna ziemia, od kt�rej, nawet o tej porze roku "+
	   "bije niesamowity wr�cz od�r "+
	   "ekskrement�w. Mieszka�cy tej uliczki zdaje si� "+
	   "nie maj� w�asnych wychodk�w ponadto, wyra�nie "+
	   "widoczne jest tu ich upodobanie do oddawania "+
	   "ka�u przed, a nie za domem. C�, chc�c niechc�c, "+
	   "to, co dociera do ciebie przez zmys� wzroku i "+
	   "w�chu pobudza twoj� wyobra�ni� - Czym oni si� "+
	   "�ywi� do cholery?";

    str+="\n";
    return str;
}
