// do przejrzenia
/*
 * BO, zwierzaczek
 */
#include "dir.h"

inherit RINDE_STD;

#include <stdproperties.h>

void create_rinde()
{
    set_short("Zakurzone obszerne pomieszczenie");
    set_long("Znajdujesz si� w ca�kiem sporych rozmiar�w " +
            "pomieszczeniu, a s�dz�c po og�lnym jego wystroju i " +
            "umeblowaniu, bez trudu mo�na domy�li� si�, �e jest to " +
            "miejscowy lombard. Na licznych p�kach porozwieszanych " +
            "na dw�ch �cianach wida� r�ne ciekawe rzeczy, zapewne " +
            "oddane pod zastaw przez jakiego� osobnika w potrzebie. " +
            "Wysokie, o masywnych kszta�tach szafki, r�wnie� pe�ne " +
            "s� jakich� rupieci, jednak za szybami pokrytymi grub� " +
            "warstw� kurzu nie wida� zbyt wiele, a nie spieszy ci " +
            "si� do kontaktu z tym nieco przyprawiaj�cym o ciarki " +
            "brudem. Pod�og� tworzy drewniany parkiet, zapewne " +
            "wykonany z drzewa d�bowego, lecz gdzieniegdzie " +
            "dostrzegasz przegryzienia i przypalenia, przez co wida�, " +
            "�e niejedna osoba t�dy przesz�a.\n");

    add_item(({"szafki", "p�ki"}),
            "Ca�e pomieszczenie wype�nione jest r�nego rodzaju " +
            "p�kami i szafkami, praktycznie w wi�kszosci " +
            "zape�nionymi rupieciami. Trudno b^edzie znale^x^c "+
            "tutaj co^s warto^sciowego.\n");
    add_item("rupiecie",
            "Przez grub^a warstw^e kurzu trudno "+
            "odr^o^zni^c przedmioty znajduj^ace si^e na p^o^lkach, "+
            "jednak po dlu^zszych ogl^edzinach udaje ci si^e "+
            "dostrzec jakie^s stare misy, sztu^cce i inne "+
            "ma^lowarto^sciowe rupiecie.\n");

    add_event("Pod�oga zaskrzypia�a pod tob� nieznacznie.\n");
    add_event("Drzwiczki od jednej z szafek otworzy�y si� lekko.\n");
    add_event("Jaki� przedmiot obsun�� si� z innego i z cichym " +
            "brzd�kni�ciem upad� na ziemi�.\n");
    add_event("W powietrzu unosz� si� py�ki kurzu.\n");
    add_event("Dochodz� ci� jakie� rozmowy z zewn�trz.\n");

    add_npc(RINDE_NPC + "lombardziarz");

    add_prop(ROOM_I_INSIDE,1);
    add_object(POLUDNIE_DRZWI + "z_lombardu.c");
}

public string
exits_description()
{
    return "Wyj^scie prowadzi na ulice.\n";
}

