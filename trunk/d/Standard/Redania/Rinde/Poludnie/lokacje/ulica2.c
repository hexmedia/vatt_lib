/* Opisy by Alcyone */
#include <stdproperties.h>
#include "dir.h"

inherit STREET_STD;

void create_street()
{
    set_short("Ulica przed karczm�");

    add_item("placyk",
	"Id�c w kierunku po�udniowym mo�na doj�� do jednego z mniejszych " +
	"placyk�w Rinde, na kt�rym za dnia panuje spory t�ok.\n");

    add_item(({"budynek", "karczm�"}),
	"Na p�nocy wznosi si� sporej wielko�ci budynek, z kt�rego " +
	"dochodz� g�o�ne �miechy, piski i czasem mo�na us�ysze� dziwnie " +
	"brzmi�ce wrzaski. Unosz�cy si� dooko�a zapach dochodzi od " +
	"niedomkni�tych drzwi, zach�caj�c by przez nie przej�� i zosta� " +
	"tam d�u�ej. Tu� nad nimi leniwie ko�ysze si� drewniany szyld, na " +
	"kt�rym co� napisano.\n");

    add_item("szyld",
	"Drewniany szyld leniwie ko�ysze si� nad drzwiami budynku. " +
	"Namalowano na nim jaki� du�y napis, kt�ry m�wi, i� jest to " +
	"karczma \"Pod Weso�ym D�inem\".\n");

    add_event("Obok ciebie przesz�o kilka os�b zataczaj�cych si� lekko.\n");
    add_event("Dooko�a unosi si� przyjemny zapach pieczonego mi�sa.\n");
    add_event("Szyld zaskrzypia� cicho poruszony przez wiatr.\n");
    add_event("Z karczmy wybieg�o kilka os�b potykaj�cych si� o siebie.\n");
    add_event("Z p�nocy dochodz� g�osne �piewy.\n");
    add_event("Z oddali dobieg� g�o�ny krzyk.\n");
    add_event("Obok ciebie przeszed� wysoki m�czyzna szturchaj�c ci� " +
	"przypadkiem.\n");

    add_exit("placnw.c","po^ludnie");
    add_exit("ulica1.c","zach^od");
    add_exit("ulica3.c","wsch^od");
    add_exit("karczma.c",({"karczma", "do karczmy", "z zewn�trz"}));

    add_sit("pod �cian�","pod �cian�","od �ciany",0);

}
public string
exits_description()
{
    return "Dalsza cz^e^s^c placu widoczna jest na po^ludniu. "+
           "Ulica prowadzi na wsch^od i zach^od, znajduje si^e tu tak^ze "+
           "wej^scie do karczmy.\n";
}

string
dlugi_opis()
{
    string str;

    str = "Szeroka w tym miejscu ulica biegnie z zachodu na wsch�d, a jej " +
	"g��wn� atrakcj� jest spory budynek wznosz�cy si� na p�nocy. " +
	"Dochodz� stamt�d g�o�ne �miechy, piski i czasem mo�na us�ysze� " +
	"dziwnie brzmi�ce wrzaski. Jednak drzwi tego zabudowania, nad " +
	"kt�rymi leniwie ko�ysze si� drewniany szyld, praktycznie ci�gle " +
	"obijaj� si� o futryn� witaj�c i �egnaj�c przer�nych klient�w. " +
	"Zapach pieczonego mi�sa unosz�cy si� dooko�a zach�ca by odwiedzi� " +
	"to miejsce i zosta� w nim na d�u�ej. Brukowana ulica zat�oczona " +
	"jest ca�� dob�, gdy� karczma cieszy si� du�� popularno�ci�. Na " +
	"po�udniu mo�na dostrzec jeden z mniejszych placyk^ow " +
	"Rinde.\n";

    return str;
}
