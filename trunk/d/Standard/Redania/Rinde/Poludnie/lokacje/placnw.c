/* Maly placyk Rinde. Opis zaczerpniety z BO
Nie Lip 24, 2005 7:20 pm przez Zwierzaczek,
 przerobiony przeze mnie - Lil.              */

 /*Eventy:
 Na jednej z lawek przysiadla trzymajaca sie za rece para.(trzeba zrobic 2 npce)
Jakis kupiec przeszedl placykiem rozgladajac sie za potencjalnym klientem.(kupca)
Goniec pocztowy przebiegl tuz obok ciebie pedzac z jakims listem.
Niewielki ptaszek na chwile usiadl na jednej z roslinek.(ptaszka w longu skalniaczka?)
Lekki powiew wiatru poruszyl roslinkami w skalniaczku.
Przechodzacy obok mieszczanin potracil cie niechcacy.(mieszczanina)*/



#include <stdproperties.h>
#include "dir.h"
inherit STREET_STD;

void create_street()
{
    set_short("P�nocno-zachodnia cz�� placyku");
    set_long("@@dlugi_opis@@");
    add_exit("ulica2.c","p^o^lnoc");
    add_exit("placsw.c","po^ludnie");
    add_exit("placse.c","po^ludniowy-wsch^od");
    add_exit("placne.c","wsch^od");

    add_prop(ROOM_I_INSIDE,0);

    add_object(MEBLER + "lawka_drewniana_zdobiona.c", 4);

    dodaj_rzecz_niewyswietlana("drewniana zdobiona ^lawka", 4);

    add_item(({"skalniak","skalniaczek","ro�liny"}),
            "Przygl�daj�c si� bli�ej skalniaczkowi pe�nym najr�niejszych "+
            "ro�lin, znajdujesz pow�d ich wiecznego rozkwitu i soczystej "+
            "zieleni. One s� sztuczne!\n");
}
public string
exits_description()
{
    return "Dalsza cz^e^s^c placu widoczna jest na po^ludniu, "+
           "po^ludniowym-wschodzie i wschodzie, za^s na p^o^lnocy znajduje "+
           "si^e ulica.\n";
}

string
dlugi_opis()
{
    string str;
    int i, il, il_law;
    object *inwentarz;

    inwentarz = all_inventory();
    il_law = 0;
    il = sizeof(inwentarz);

    for (i = 0; i < il; ++i) {
         if (inwentarz[i]->jestem_lawa_placu_poludniowego()) {
                        ++il_law; }}

    str = "Z tego miejsca bez k�opotu rozgl�daj�c si� mo�esz dostrzec "+
	"reszt� placyku, z racji jego niezbyt wielkich rozmiar�w. Ziemi� "+
	"wy�o�ono r�wnym kamieniem, kt�ry czyni w�drowk� placykiem "+
	"wygodniejsz� i przyjemniejsz�. Przy zachodnich jego kra�cach "+
	"dostrzegasz ca�kiem poka�ny skalniaczek, pe�en r�nych "+
	"wiecznie kwitn�cych i soczy�cie zielonych ro�lin. Nieco dziwnym "+
	"si� wydaje fakt, �e ka�dym razem ten niejaki bukiet kwietny "+
	"wygl�da tak samo. ";

   if (il_law == 1)
   {
       str += "W jego pobli^zu cichutko nawo^luje zm^eczonych "+
       "przechodni^ow drewniana, zgrabnie wyko^nczona ^lawka.\n";
   }
   else if (il_law > 1)
   {
       str += "W jego pobli�u cichutko nawo�uj� zm�czonych "+
	"przechodni�w drewniane, zgrabnie wyko�czone �awki.\n";
   }
   else
       str+="\n";

   return str;
}
