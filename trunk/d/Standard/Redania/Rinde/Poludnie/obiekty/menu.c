/* Menu karczmy 'Pod Weso�ym D�inem'
   Jeremian 19.03.2007               */

inherit "/std/object.c";

#include "dir.h"
#include <pl.h>
#include <stdproperties.h>
#include <macros.h>
#include <filter_funs.h>
int przeczytaj_menu();

create_object()
{
    ustaw_nazwe("zw�j");
    dodaj_nazwy("menu");
    dodaj_nazwy("jad^lospis");

    set_long("    __________________________________________\n   / o\t\t       MENU\t\t    o \\\n"+
              "   |\t\tZak^aski:\t\t      |\n"+
              "   |\tPieczony go��b\t        15 groszy     |\n"+
              "   |\tDuszony go��bek\t        13 groszy     |\n   |\t\t\t\t\t      |\n"+
		      "   |\t\tNapoje:\t\t\t      |\n"+
		      "   |\tJasne piwo\t\t10 groszy     |\n"+
		      "   |\t w beczu^lce 10l\t        180 groszy    |\n"+
		      "   | o\tReda^nskie piwo\t        12 groszy   o |\n"+
		      "   \\_________________________________________/\n"
		      );

    add_prop(OBJ_I_WEIGHT, 485);
    add_prop(OBJ_I_NO_GET, "Nie mo�esz wzi�� namalowanego na blacie rysunku.\n");
    add_prop(OBJ_I_DONT_GLANCE,1);

    add_cmd_item(({"menu","zw�j"}), "przeczytaj", przeczytaj_menu,
                   "Przeczytaj co?\n"); 
}

int
przeczytaj_menu()
{
   return long();
}
