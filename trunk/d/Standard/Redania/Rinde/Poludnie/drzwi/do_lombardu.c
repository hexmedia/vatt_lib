
/*
 * Drzwi prowadzace do lombardu
 * Wykonane przez Avarda, dnia 08.06.06
 */

inherit "/std/door";

#include <pl.h>
#include "dir.h"

void
create_door()
{
    ustaw_nazwe("drzwi");
    dodaj_przym("drewniany", "drewniani");

    set_other_room(POLUDNIE_LOKACJE +"lombard.c");
    set_door_id("DRZWI_DO_LOMBARDU_RINDE");
    set_door_desc("Nie wyr^ozniaj^ace si^e niczym nadzwyczajnym drzwi "+
        "pomalowane na ciemny br^az.\n");

    set_open_desc("");
    set_closed_desc("");

    set_pass_command("lombard","do lombardu","z zewn�trz");
    //set_pass_mess("przez drewniane drzwi do lombardu");

    set_key("KLUCZ_DRZWI_DO_LOMBARDU_RINDE");
    set_lock_name(({"zamek", "zamku", "zamkowi", "zamek", "zamkiem", "zamku"}), 0, PL_MESKI_NOS_NZYW);
}
