/*
 * Drzwi z czytelni
 * Faeve, 27.03.08
 *
 */

inherit "/std/door";

#include <pl.h>
#include "dir.h"

void
create_door()
{
    ustaw_nazwe("drzwi");
    dodaj_przym("niski", "niscy");

    set_other_room(POLUDNIE +"Biblioteka/lokacje/przedsionek");
    set_door_id("DRZWI_DO_CZYTELNI_RINDE");
    set_door_desc("Te drewniane drzwi cho^c szerokie - s^a te^z stosunkowo "+
        "niskie. Osoba wzrostu wy^zszego ni^z przeci^etny cz^lowiek nie "+
        "b^edzie mog^la przej^s^c przez nie, nie schylaj^ac g^lowy\n");

    set_open_desc("");
    set_closed_desc("");

    set_pass_command(({"drzwi","przedsionek","wyj^scie"}),"do przedsiona","z czytelni");
    //set_pass_mess("przez drewniane drzwi do lombardu");

    set_key("KLUCZ_DRZWI_DO_BIBLIOTEKI_RINDE");
    set_lock_name(({"zamek", "zamku", "zamkowi", "zamek", "zamkiem", "zamku"}), 0, PL_MESKI_NOS_NZYW);
}
