/**
 * \file /d/Standard/Redania/Rinde/Poludnie/Biblioteka/lokacje/glowna.c
 *
 * Rindyjska biblioteka, pierwsza biblioteka na mudzie.
 *
 * R�wnocze�nie jest to miejsce gdzie mo�na trenowa� kilka umiej�tno�ci
 * teoretycznych. Co do samych umiej�tno�ci jakie mo�na trenowa� to s� to:
 * - nooo. zerknijcie ni�ej to zobaczycie. vera.
 *      I NIE BALANSOWA� BEZ MOJEGO POZWOLENIA!.
 *
 *
 * @author Krun - kod
 * @author Tabakista - opis
 * @date listopad 2007
 */

inherit "/std/room.c";
inherit "/lib/library.c";
inherit "/lib/training_library.c";

#include "dir.h"
#include <macros.h>
#include <stdproperties.h>
#include <object_types.h>
#include <ss_types.h>

#define SUBLOC_SCIANA   "_subloc_sciana"

string dlugi();
string opis_okna();

void create_room()
{
    set_short("Czytelnia");

    set_long(&dlugi());
    set_event_time(300.0);
    add_event("S�ycha� jakie� niezrozumia�e pokrzykiwania zza okna.\n");
    add_event("Drewniana pod�oga trzeszczy pod twoimi stopami.\n");
    add_event("Od zalegaj�cego wsz�dzie kurzu kr�ci ci� w nosie.\n");


    add_sit("na pod�odze", "na pod�odze", "z pod�ogi", 0);

    /* dodajemy sublokacj� typu 'na', jednak dosy� specyficzn� */
    add_subloc(SUBLOC_SCIANA);
    /* nie chcemy, by mo�na by�o na �cian� odk�ada� ani k�a�� rzeczy */
    add_subloc_prop(SUBLOC_SCIANA, CONT_I_CANT_ODLOZ, 1);
    add_subloc_prop(SUBLOC_SCIANA, CONT_I_CANT_POLOZ, 1);
    /* oczywi�cie mo�na na �cianie wiesza� przedmioty */
    add_subloc_prop(SUBLOC_SCIANA, SUBLOC_I_MOZNA_POWIES, 1);
    /* ale tylko typu �ciennego */
    add_subloc_prop(SUBLOC_SCIANA, SUBLOC_I_DLA_O, O_SCIENNE);
    add_subloc_prop(SUBLOC_SCIANA, CONT_I_MAX_RZECZY, 1);

    add_object(RINDE_BIBLIOTEKA_PRZEDMIOTY+"lampa.c", 1, 0, SUBLOC_SCIANA);

    add_object(RINDE_BIBLIOTEKA_PRZEDMIOTY+"stol.c", 2, 0, 0);

    add_object(RINDE_BIBLIOTEKA_DRZWI + "z_glownej");

    add_item("okno", &opis_okna());

    add_exit(RINDE_BIBLIOTEKA + "przedsionek.c", ({"przedsionek", "do przedsionka", "z przedsionka"}), 0, 0);
    add_exit(RINDE_BIBLIOTEKA + "przedsionek.c", ({"wyj�cie", "do przedsionka", "z przedsionka"}), 0, 0);

    dodaj_rzecz_niewyswietlana("prosty d�bowy st�", 2);

    add_prop(ROOM_I_INSIDE, 1);

    //Standardowa biblioteka:
    set_book_directory(BOOK_DIR);
    set_borrow_required(1);

    config_library();

    //Biblioteka ucz�ca:
    config_training_library();

    //Dodajemy pergaminy z kt�rych mo�na si� b�dzie.

    //Kolejne argumenty to:
    //numer umiej�tno�ci, cena, jako��, maksymalny poziom wyuczenia,
    //minimalny poziom wyuczenia potrzeby do korzystania z pergaminu,
    //czas czytania pergaminu, dlugi opis pergaminu, przymiotniki pergaminu,
    //nazwa pergaminu, suffix po nazwie uma
    dodaj_pergamin(SS_PARRY, 6550, 8000, 38, 0, 80, "Delikatny pergamin pokryty "+
        "jest ciasno linijkami drobnych, nieco nier�wnych run. Si�gaj� one niemal "+
        "kraw�dzi karty, przez co te na skraju s� nieco zatarte, bowiem sk�ra z "+
        "kt�rej wykonano pergamin jest niezwykle wr�cz cienka i delikatna. Pod "+
        "palcami wyczu� mo�na, �e zosta�a dodatkowo zeszlifowana co nada�o jej "+
        "g�adko�ci i usun�o skazy. Przez �rodek biegn� jedynie dwa krzy�uj�ce "+
        "si� �lady znacz�ce miejsca w kt�rych karta by�a z�o�ona.\n",
        ({"g�adki", "g�adcy", "cienki", "ciency"}), "pergamin", "dla pocz�tkuj�cych");
    dodaj_pergamin(SS_HERBALISM, 10353, 6800, 45, 0, 70, "Pergamin z grubo wyprawonej, "+
        "wo�owej sk�ry zapisany jest rz�dami nieco topornych, lecz jak najbardziej "+
        "czytelnych run. Pomi�dzy nimi, w kilku miejscach dostrzec mo�na niewielkie "+
        "plamy inkaustu, nie na tyle jednak du�e by przes�oni� pismo. R�wno przyci�te "+
        "kraw�dzie s� lekko zeszlifowane, dzi�ki czemu karta jest g�adka i "+
        "nie strz�pi si�.\n", ({"jasny", "ja�ni", "cie�ki", "ciency"}));
    dodaj_pergamin(SS_WEP_SWORD, 8909, 5055, 48, 0, 90, "G�adka, pod�u�na karta "+
        "pergaminu pokryta jest niemal ca�a r�wnymi linijkami r�wnego, "+
        "lekko pochy�ego pisma. Wprawnie stawiane runy daj� si� �atwo odczyta�, "+
        "za� nieliczne, drobne ryciny s� czyste i czytelne. Cienka sk�ra z kt�rej "+
        "wykonano pergamin zosta�a starannie wyprawiona i jedynie w kilku miejscach "+
        "nosi niewielkie skazy.\n", ({"du�y", "duzi", "jasny", "ja�ni"}));
    dodaj_pergamin(SS_WEP_AXE, 8909, 5055, 48, 0, 60, "Delikatny pergamin pokryty "+
        "jest ciasno linijkami drobnych, nieco nier�wnych run. Si�gaj� one niemal "+
        "kraw�dzi karty, przez co te na skraju s� nieco zatarte, bowiem sk�ra z "+
        "kt�rej wykonano pergamin jest niezwykle wr�cz cienka i delikatna. Pod "+
        "palcami wyczu� mo�na, �e zosta�a dodatkowo zeszlifowana co nada�o jej "+
        "g�adko�ci i usun�o skazy. Przez �rodek biegn� jedynie dwa krzy�uj�ce "+
        "si� �lady znacz�ce miejsca w kt�rych karta by�a z�o�ona.\n",
        ({"g�adki", "g�adcy", "cienki", "ciency"}));
    dodaj_pergamin(SS_WEP_POLEARM, 8909, 5055, 48, 0, 90, "G�adka, pod�u�na karta "+
        "pergaminu pokryta jest niemal ca�a r�wnymi linijkami r�wnego, "+
        "lekko pochy�ego pisma. Wprawnie stawiane runy daj� si� �atwo odczyta�, "+
        "za� nieliczne, drobne ryciny s� czyste i czytelne. Cienka sk�ra z kt�rej "+
        "wykonano pergamin zosta�a starannie wyprawiona i jedynie w kilku miejscach "+
        "nosi niewielkie skazy.\n", ({"du�y", "duzi", "jasny", "ja�ni"}));
    dodaj_pergamin(SS_WEP_KNIFE, 8909, 5055, 48, 0, 60, "Delikatny pergamin pokryty "+
        "jest ciasno linijkami drobnych, nieco nier�wnych run. Si�gaj� one niemal "+
        "kraw�dzi karty, przez co te na skraju s� nieco zatarte, bowiem sk�ra z "+
        "kt�rej wykonano pergamin jest niezwykle wr�cz cienka i delikatna. Pod "+
        "palcami wyczu� mo�na, �e zosta�a dodatkowo zeszlifowana co nada�o jej "+
        "g�adko�ci i usun�o skazy. Przez �rodek biegn� jedynie dwa krzy�uj�ce "+
        "si� �lady znacz�ce miejsca w kt�rych karta by�a z�o�ona.\n",
        ({"g�adki", "g�adcy", "cienki", "ciency"}));
    dodaj_pergamin(SS_WEP_CLUB, 8909, 5055, 48, 0, 90, "G�adka, pod�u�na karta "+
        "pergaminu pokryta jest niemal ca�a r�wnymi linijkami r�wnego, "+
        "lekko pochy�ego pisma. Wprawnie stawiane runy daj� si� �atwo odczyta�, "+
        "za� nieliczne, drobne ryciny s� czyste i czytelne. Cienka sk�ra z kt�rej "+
        "wykonano pergamin zosta�a starannie wyprawiona i jedynie w kilku miejscach "+
        "nosi niewielkie skazy.\n", ({"du�y", "duzi", "jasny", "ja�ni"}));
    dodaj_pergamin(SS_WEP_WARHAMMER, 8909, 5055, 48, 0, 60, "Delikatny pergamin pokryty "+
        "jest ciasno linijkami drobnych, nieco nier�wnych run. Si�gaj� one niemal "+
        "kraw�dzi karty, przez co te na skraju s� nieco zatarte, bowiem sk�ra z "+
        "kt�rej wykonano pergamin jest niezwykle wr�cz cienka i delikatna. Pod "+
        "palcami wyczu� mo�na, �e zosta�a dodatkowo zeszlifowana co nada�o jej "+
        "g�adko�ci i usun�o skazy. Przez �rodek biegn� jedynie dwa krzy�uj�ce "+
        "si� �lady znacz�ce miejsca w kt�rych karta by�a z�o�ona.\n",
        ({"g�adki", "g�adcy", "cienki", "ciency"}));
    //Kolejne argumenty to:
    //numer umiej�tno�ci, cena, jako��, maksymalny poziom wyuczenia,
    //minimalny poziom wyuczenia potrzeby do korzystania z pergaminu,
    //czas czytania pergaminu, dlugi opis pergaminu, przymiotniki pergaminu,
    //nazwa pergaminu, suffix po nazwie uma
    dodaj_pergamin(SS_WEP_MISSILE, 11909, 5555, 48, 0, 90, "G�adka, pod�u�na karta "+
        "pergaminu pokryta jest niemal ca�a r�wnymi linijkami r�wnego, "+
        "lekko pochy�ego pisma. Wprawnie stawiane runy daj� si� �atwo odczyta�, "+
        "za� nieliczne, drobne ryciny s� czyste i czytelne. Cienka sk�ra z kt�rej "+
        "wykonano pergamin zosta�a starannie wyprawiona i jedynie w kilku miejscach "+
        "nosi niewielkie skazy.\n", ({"du�y", "duzi", "jasny", "ja�ni"}));
    dodaj_pergamin(SS_THROWING, 10919, 5555, 48, 0, 80, "Delikatny pergamin pokryty "+
        "jest ciasno linijkami drobnych, nieco nier�wnych run. Si�gaj� one niemal "+
        "kraw�dzi karty, przez co te na skraju s� nieco zatarte, bowiem sk�ra z "+
        "kt�rej wykonano pergamin jest niezwykle wr�cz cienka i delikatna. Pod "+
        "palcami wyczu� mo�na, �e zosta�a dodatkowo zeszlifowana co nada�o jej "+
        "g�adko�ci i usun�o skazy. Przez �rodek biegn� jedynie dwa krzy�uj�ce "+
        "si� �lady znacz�ce miejsca w kt�rych karta by�a z�o�ona.\n",
        ({"g�adki", "g�adcy", "cienki", "ciency"}));


    dodaj_pergamin(SS_CLIMB, 6253, 4060, 40, 0, 77, "Pergamin z grubo wyprawonej, "+
        "wo�owej sk�ry zapisany jest rz�dami nieco topornych, lecz jak najbardziej "+
        "czytelnych run. Pomi�dzy nimi, w kilku miejscach dostrzec mo�na niewielkie "+
        "plamy inkaustu, nie na tyle jednak du�e by przes�oni� pismo. R�wno przyci�te "+
        "kraw�dzie s� lekko zeszlifowane, dzi�ki czemu karta jest g�adka i "+
        "nie strz�pi si�.\n", ({"jasny", "ja�ni", "cie�ki", "ciency"}));

    dodaj_pergamin(SS_SWIM, 19554, 3230, 34, 0, 83, "G�adka, pod�u�na karta "+
        "pergaminu pokryta jest niemal ca�a r�wnymi linijkami r�wnego, "+
        "lekko pochy�ego pisma. Wprawnie stawiane runy daj� si� �atwo odczyta�, "+
        "za� nieliczne, drobne ryciny s� czyste i czytelne. Cienka sk�ra z kt�rej "+
        "wykonano pergamin zosta�a starannie wyprawiona i jedynie w kilku miejscach "+
        "nosi niewielkie skazy.\n", ({"du�y", "duzi", "jasny", "ja�ni"}));

    dodaj_pergamin(SS_APPR_OBJ, 4332, 3640, 35, 0, 75, "Delikatny pergamin pokryty "+
        "jest ciasno linijkami drobnych, nieco nier�wnych run. Si�gaj� one niemal "+
        "kraw�dzi karty, przez co te na skraju s� nieco zatarte, bowiem sk�ra z "+
        "kt�rej wykonano pergamin jest niezwykle wr�cz cienka i delikatna. Pod "+
        "palcami wyczu� mo�na, �e zosta�a dodatkowo zeszlifowana co nada�o jej "+
        "g�adko�ci i usun�o skazy. Przez �rodek biegn� jedynie dwa krzy�uj�ce "+
        "si� �lady znacz�ce miejsca w kt�rych karta by�a z�o�ona.\n",
        ({"g�adki", "g�adcy", "cienki", "ciency"}));

    dodaj_pergamin(SS_TRACKING, 8343, 3000, 35, 0, 70, "Pergamin z grubo wyprawonej, "+
        "wo�owej sk�ry zapisany jest rz�dami nieco topornych, lecz jak najbardziej "+
        "czytelnych run. Pomi�dzy nimi, w kilku miejscach dostrzec mo�na niewielkie "+
        "plamy inkaustu, nie na tyle jednak du�e by przes�oni� pismo. R�wno przyci�te "+
        "kraw�dzie s� lekko zeszlifowane, dzi�ki czemu karta jest g�adka i "+
        "nie strz�pi si�.\n", ({"jasny", "ja�ni", "cie�ki", "ciency"}));

    dodaj_pergamin(SS_HUNTING, 4342, 4000, 40, 0, 65, "Delikatny pergamin pokryty "+
        "jest ciasno linijkami drobnych, nieco nier�wnych run. Si�gaj� one niemal "+
        "kraw�dzi karty, przez co te na skraju s� nieco zatarte, bowiem sk�ra z "+
        "kt�rej wykonano pergamin jest niezwykle wr�cz cienka i delikatna. Pod "+
        "palcami wyczu� mo�na, �e zosta�a dodatkowo zeszlifowana co nada�o jej "+
        "g�adko�ci i usun�o skazy. Przez �rodek biegn� jedynie dwa krzy�uj�ce "+
        "si� �lady znacz�ce miejsca w kt�rych karta by�a z�o�ona.\n",
        ({"g�adki", "g�adcy", "cienki", "ciency"}));

    dodaj_pergamin(SS_DEFENCE, 4342, 4020, 40, 0, 65, "Delikatny pergamin pokryty "+
        "jest ciasno linijkami drobnych, nieco nier�wnych run. Si�gaj� one niemal "+
        "kraw�dzi karty, przez co te na skraju s� nieco zatarte, bowiem sk�ra z "+
        "kt�rej wykonano pergamin jest niezwykle wr�cz cienka i delikatna. Pod "+
        "palcami wyczu� mo�na, �e zosta�a dodatkowo zeszlifowana co nada�o jej "+
        "g�adko�ci i usun�o skazy. Przez �rodek biegn� jedynie dwa krzy�uj�ce "+
        "si� �lady znacz�ce miejsca w kt�rych karta by�a z�o�ona.\n",
        ({"g�adki", "g�adcy", "cienki", "ciency"}));

     dodaj_pergamin(SS_APPR_VAL, 5022, 2940, 20, 0, 65, "Delikatny pergamin pokryty "+
        "jest ciasno linijkami drobnych, nieco nier�wnych run. Si�gaj� one niemal "+
        "kraw�dzi karty, przez co te na skraju s� nieco zatarte, bowiem sk�ra z "+
        "kt�rej wykonano pergamin jest niezwykle wr�cz cienka i delikatna. Pod "+
        "palcami wyczu� mo�na, �e zosta�a dodatkowo zeszlifowana co nada�o jej "+
        "g�adko�ci i usun�o skazy. Przez �rodek biegn� jedynie dwa krzy�uj�ce "+
        "si� �lady znacz�ce miejsca w kt�rych karta by�a z�o�ona.\n",
        ({"g�adki", "g�adcy", "cienki", "ciency"}),"pergamin","dla poczatkuj�cych");
     dodaj_pergamin(SS_APPR_VAL, 5322, 6140, 50, 19, 65, "Delikatny pergamin pokryty "+
        "jest ciasno linijkami drobnych, nieco nier�wnych run. Si�gaj� one niemal "+
        "kraw�dzi karty, przez co te na skraju s� nieco zatarte, bowiem sk�ra z "+
        "kt�rej wykonano pergamin jest niezwykle wr�cz cienka i delikatna. Pod "+
        "palcami wyczu� mo�na, �e zosta�a dodatkowo zeszlifowana co nada�o jej "+
        "g�adko�ci i usun�o skazy. Przez �rodek biegn� jedynie dwa krzy�uj�ce "+
        "si� �lady znacz�ce miejsca w kt�rych karta by�a z�o�ona.\n",
        ({"g�adki", "g�adcy", "cienki", "ciency"}),"pergamin","dla zaawansowanych");

}

public string exits_description()
{
    return "Jedyne wyj�cie prowadzi do przedsionka biblioteki.\n";
}

string query_pomoc()
{
    return library_help() + "\n" + training_library_help();
}

void init()
{
    ::init();
    init_training_library();
    init_library();

}

string dlugi()
{
    return "Dwa spore, d�bowe sto�y zajmuj� wi�ksz� cz�� ciasnego "+
        "pomieszczenia czytelni. " +
        (jest_dzien() ? "Pada na nie �wiat�o z du�ego, zakratowanego, "+
            "wychodz�cego na po�udnie okna." :  "Wychodz�ce na po�udnie, "+
            "zakratowane okno, pozostaje teraz ca�kowicie czarne.") +
        "Puste, od niechcenia pobielone �ciany pot�guj� wra�enie zimna "+
        "panuj�ce w pomieszczeniu. Ju� po kilku chwilach tutaj "+
        "robi si� nieprzyjemnie ch�odno. Drewniana, pe�na szczelin "+
        "i p�kni�� pod�oga poskrzypuje cicho w rytm krok�w. Panuje "+
        "tutaj niemal ca�kowita cisza, m�cona jedynie sporadycznymi, "+
        "dochodz�cymi z ulicy d�wi�kami.\n";
}

public string opis_okna()
{
    return "Pomi�dzy grubymi, matowymi kratami kurz� si� ma�e szybki" +
        (jest_dzien() ? " przez kt�re wpadaj� blade promienie s�o�ca. " :
            ", nocn� por� zupe�nie czarne. " ) +
        "W�ski parapet ca�y pokryty jest niewielkimi dziurkami.\n";
}