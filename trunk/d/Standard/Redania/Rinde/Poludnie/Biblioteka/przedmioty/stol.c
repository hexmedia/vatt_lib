/**
 * \file /d/Standard/Redania/Rinde/Poludnie/Biblioteka/przedmioty/stol.c
 *
 * St� do biblioteki w rinde.
 *
 * @author Krun - kod
 * @author Tabakista - opis
 * @date listopad 2007
 */

inherit "/std/container.c";

#include <pl.h>
#include <macros.h>
#include <materialy.h>
#include <stdproperties.h>

void
create_container()
{
    ustaw_nazwe("stol");

    set_long("Niezbyt wysoki st�, pozbawiony wszelkich zdobie� nosi �lady "+
        "niezliczonych zadrapa� i sznyt�w. Nogi, ju� nieco pokrzywione, "+
        "nadal podtrzymuj� do�� pewnie matowy blat. D�bowe drewno zciemnia�o "+
        "ju� zdradzaj�c znaczny wiek mebla.\n");

    dodaj_przym("prosty", "pro�ci");
    dodaj_przym("d�bowy", "d�bowi");

    make_me_sitable("przy", "przy " + TO->short(PL_MIE), "od " + TO->short(PL_DOP), 4, PL_MIE, "przy");

    add_prop(CONT_I_MAX_VOLUME, 20000);
    add_prop(CONT_I_VOLUME, 16800);

    add_prop(OBJ_I_NO_GET, "Nie da si� go podnie��.\n");
    add_prop(CONT_I_CANT_WLOZ_DO, 1); /* By nie mo�na by�o nic do niego w�o�y� komend� 'wloz do' */

    add_prop(CONT_I_WEIGHT, 10000);
    add_prop(CONT_I_MAX_WEIGHT, 20000);
    add_prop(CONT_I_DONT_SHOW_CONTENTS, 1);

    add_subloc("na");
    add_subloc_prop("na", CONT_I_MAX_WEIGHT, 44641);

    //W 100% z drewna d�bowego
    ustaw_material(MATERIALY_DR_DAB);
}