/**
 * \file /d/Standard/Redania/Rinde/Poludnie/Biblioteka/przedmioty/lampa.c
 *
 * Lampa do biblioteki w rinde.
 *
 * @author Krun - kod
 * @author Tabakista - opis
 * @date listopad 2007
 */

inherit "/std/lampa.c";

#include <macros.h>
#include <materialy.h>
#include <object_types.h>

void
create_lamp()
{
    ustaw_nazwe("lampa");

    set_long("Zwyk�a, lekka lampa z miedzianej blachy zaopatrzona "+
        "zosta�a w klosz z lekko przymglonego, a za to mocno "+
        "okopconego sadz� szk�a. Mie� mocno ju� za�niedzia�a, "+
        "b�yskaj�c metalicznie jedynie w tych kilku miejscach "+
        "gdzie jest uderzona, czy zarysowana oraz tam gdzie zwykle "+
        "trzymaj� j� d�onie w�a�ciciela.\n");

    dodaj_przym("wys�u�ony", "wys�u�eni");
    dodaj_przym("miedziany", "miedziani");

    set_type(O_SCIENNE);

    ustaw_material(MATERIALY_MIEDZ, 60);
    ustaw_material(MATERIALY_SZKLO, 40);
}

//A to przepisane, �eby j� bibliotekarz nape�nia�.
public void
burned_out()
{
    ENV(TO)->poinformuj_bibliotekrza_o_wypalonej_lampie();
    ::burned_out();
}