/**
 * \file /d/Standard/Redania/Rinde/Poludnie/ubrania/obszerna_przybrudzona_szata.c
 *
 * Szata bibliotekarza z Rinde.
 *
 * @author Krun - kod
 * @author Tabakista - opisy
 * @date Grudzie� 2007
 */

inherit "/std/armour.c";

#include <wa_types.h>
#include <materialy.h>
#include <object_types.h>
#include <stdproperties.h>

void
create_armour()
{
    ustaw_nazwe("szata");

    dodaj_przym("obaszerny", "obszerni");
    dodaj_przym("przybrudzony", "przybrudzeni");

    set_long("Workowate odzienie jest jakby szyte z my�l� by by�o "+
        "niegustowne i niewygodne. Pozbawione wszelkich zdobie� i "+
        "lu�no opadaj�ca z ramion, bez wzgl�du na to kto j� nosi, "+
        "wprawnie maskuje wszelkie przymioty budowy cia�a nosz�cego. "+
        "Ciemnobr�zowy kolor lnianego materia�u wydaje si� nijaki, "+
        "za� plamy od inkatustu na r�kawach uzupe�niaj� odpychaj�cy "+
        "wygl�d. Brak kieszeni oraz niewygodne wi�zanie przecz� tak�e "+
        "jej praktyczno�ci. Najlepsze co mo�na o niej powiedzie� to, "+
        "�e mocny, gruby len niepr�dko si� zniszczy, czy wytrze. Cho� "+
        "w�a�ciciel niekoniecznie musi poczyta� to jako zalet�.\n");

    ustaw_material(MATERIALY_LEN);

    set_type(O_UBRANIA);
    add_prop(ARMOUR_I_D_ROZCIAGLIWOSC, 100); // zeby sie nie srac...
    add_prop(ARMOUR_I_U_ROZCIAGLIWOSC, 100);
    set_slots(A_ROBE);
    add_prop(OBJ_I_VOLUME, 1800);
    add_prop(OBJ_I_VALUE, 30);
    add_prop(OBJ_I_WEIGHT, 886);
}