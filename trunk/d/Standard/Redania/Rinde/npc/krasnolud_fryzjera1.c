/*Jeden z krasnali - ochrony fryzjera Rinde. Pojawiaja sie tylko
  wtedy, kiedy ktos zaatakuje fryzjera.
  Lil.*/

#include <stdproperties.h>
#include <macros.h>
#include <money.h>
#include <pl.h>
#include "dir.h"

inherit RINDE_NPC_STD;

int walka = 0;

void
create_rinde_humanoid()
{
    dodaj_przym("pot�ny","pot�ni");
    dodaj_przym("rudobrody","rudobrodzi");
    ustaw_odmiane_rasy(PL_KRASNOLUD);
    set_gender(G_MALE);
    set_long("Ubrany w sk�rzan� kurt� i kolcze "+
             "spodnie, sprawia wra�enie niezbyt "+
             "gro�nego, ale jeden rzut oka na "+
             "przepe�nione spokojn� pewno�ci� siebie "+
             "oczy, na spos�b trzymania broni a i na "+
             "sam ci�ar broni, przekonuje, �e jest to "+
             "osoba, kt�rej walki s� nieobce.\n");
    add_prop(CONT_I_WEIGHT, 115000);
    add_prop(CONT_I_HEIGHT, 162);
    add_prop(NPC_I_NO_RUN_AWAY, 1);
    add_prop(LIVE_I_NEVERKNOWN, 1);
    add_prop(NPC_I_NO_FEAR, 1);

    set_stats(({ 79, 42, 79, 30, 77}));
    setuid();
    seteuid(getuid());

    //add_object(RINDE_BRONIE+"miecz_szeroki_poreczny.c");
    add_weapon(RINDE_BRONIE + "miecze/miecz_szeroki_poreczny.c");
    add_armour(RINDE_ZBROJE + "spodenki/szerokie_zabrudzone_Mk.c");
    add_armour(RINDE_ZBROJE + "kurtki/skorzana_mocna_Mk.c");

    set_cchat_time(1);
    add_cchat("Bracia! Do boju!");
    add_cchat("Do boju kurwaaaaa!");
    add_cchat("Waaaaa! Waaa! Waaaaaaaaaaaa!");
    add_cchat("Jest m�j!");
    add_cchat("Aaaa!");
    add_cchat("Osz ty!");
    add_cchat("Ty psie!");
}

void
attacked_by(object wrog)
{
    if(!sizeof(query_attacked_by()))
    {
        this_object()->command("dobadz broni");

        return ::attacked_by(wrog);
    }
    else
    {
        return ::attacked_by(wrog);
    }
}

void
odejdz_mnie()
{
    command("emote wychodzi przez ukryte drzwi.");
    remove_object();
}

void
signal_stop_fight(object ob, mixed en)
{
    if(!ob)
        return;
    if(!en)
        return;
    if(pointerp(en) && !sizeof(en))
        return;
    if(!pointerp(en))
        en = ({en});

    if(ob == TO)
    {
        command("kopnij cialo");
        command("opusc bron");

        set_alarm(frandom(3, 1), 0.0, &odejdz_mnie());
    }
}

void
wesprzyj_fryzjera(object fr)
{
    set_alarm(frandom(3, 1), 0.0, "command_present", fr, "wesprzyj " + OB_NAME(fr));
}