
/* Autor: Avard
   Opis : Tinardan
   Data : 27.07.06
   Info : Darla, spaceruje z Sonnim po Rinde */

#pragma unique

#include <std.h>
#include <ss_types.h>
#include <macros.h>
#include <stdproperties.h>
#include <pl.h>
#include "dir.h"

inherit RINDE_NPC_STD;

 void
 buzi_sonni();
 void
 zle_sonni();
/* To do emote_hook */

int czy_walka();
int walka = 0;
int pomoc();
int koniec_walki(object ob);
int i;

void
create_rinde_humanoid()
{
    set_living_name("darla");
    ustaw_odmiane_rasy("kobieta");
    set_gender(G_FEMALE);
    ustaw_imie(({"darla","darli","darli","darl^e","darl^a",
        "darli"}), PL_ZENSKI);
    dodaj_przym("drobny","drobni");
    dodaj_przym("zielonooki","zielonoocy");
    set_long("@@dlugasny@@");

    set_stats (({ 40, 60, 60, 30, 80 }));
    add_prop(CONT_I_WEIGHT, 47000);
    add_prop(CONT_I_HEIGHT, 165);

    set_act_time(40);
    add_act("@@eventy@@");

    set_cact_time(10);
    add_cact("krzyknij Niech mi kto^s pomo^ze!");
    add_cact("emote pr^obuje zas^loni^c si^e r^ekoma.");

    add_armour(RINDE_UBRANIA +"bluzki/skromna_kremowa_Sc.c");
    add_armour(RINDE_UBRANIA +"spodnice/pomarszczona_dluga_Sc.c");
    add_armour(RINDE_UBRANIA +"buty/jasne_damskie_Sc.c");

    add_ask(({"ratusz"}), VBFC_ME("pyt_o_ratusz"));
    add_ask(({"^swi^atynie"}), VBFC_ME("pyt_o_swiatynie"));
    add_ask(({"prace"}), VBFC_ME("pyt_o_prace"));
    add_ask(({"garnizon"}), VBFC_ME("pyt_o_garnizon"));
    add_ask(({"burmistrza","Nevilla","nevilla"}),VBFC_ME("pyt_o_burmistrza"));
    add_ask(({"sonniego","Sonniego","kr^otkow^losego m^e^zczyzn^e",
        "kr^otkow^losego ciemnookiego","ciemnookiego m^e^zczyzn^e"}),
        VBFC_ME("pyt_o_sonniego"));
    set_default_answer(VBFC_ME("default_answer"));

   // set_alarm_every_hour("co_godzine");
}
/*
int
co_godzine()
{
    if (MT_GODZINA == 17)
    {
        set_alarm(0.1,0.0, "do_sonniego");
    }
    if (MT_GODZINA == 22)
    {
        set_alarm(0.1,0.0, "do_domu");
    }
}
*/
void
powiedz_gdy_jest(object player, string tekst)
{
    if (environment(this_object()) == environment(player))
    this_object()->command(tekst);
}

string
pyt_o_ratusz()
{
    if(!query_attack())
    {
        set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
        "'Jest w samym ^srodku miasta.");
        return "";
    }
    else
    {
        set_alarm(0.5,0.0,"powiedz_gdy_jest",TP,"'Zostaw mnie!");
        return "";
    }
}
string
pyt_o_swiatynie()
{
    if(!query_attack())
    {
        set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
        "'To du^za budowla z wie^z^a na zachodzie miasta.");
        return "";
    }
    else
    {
        set_alarm(0.5,0.0,"powiedz_gdy_jest",TP,"'Zostaw mnie!");
        return "";
    }
}
string
pyt_o_garnizon()
{
    if(!query_attack())
    {
        set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
        "'Garnizon jest na wschodzie.");
        return "";
    }
    else
    {
        set_alarm(0.5,0.0,"powiedz_gdy_jest",TP,"'Zostaw mnie!");
        return "";
    }
}
string
pyt_o_burmistrza()
{
    if(!query_attack())
    {
        set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
        "'Neville jest naszym burmistrzem, siedzi ca^lymi "+
        "dniami w ratuszu");
        return "";
    }
    else
    {
        set_alarm(0.5,0.0,"powiedz_gdy_jest",TP,"'Zostaw mnie!");
        return "";
    }
}

string
pyt_o_prace()
{
    if(!query_attack())
    {
        set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
        "'Nie, niestety, nie mam dla ciebie pracy.");
        return "";
    }
    else
    {
        set_alarm(0.5,0.0,"powiedz_gdy_jest",TP,"'Zostaw mnie!");
        return "";
    }
}

string
pyt_o_sonniego()
{
    if(!query_attack())
    {
        if(!present("sonni", environment()))
        {
            set_alarm(0.5,0.0,"powiedz_gdy_jest",TP,"'Pewnie "+
                "jest gdzie^s na trakcie.");
            set_alarm(2.5,0.0,"powiedz_gdy_jest",TP,"'Neville "+
                "nie daje mu spokoju z tymi pos^laniami.");
        }
        else
        {
            find_living("sonni")->pyt_o_sonniego();
        }
    }
    else
    {
        set_alarm(0.5,0.0,"powiedz_gdy_jest",TP,"'Zostaw mnie!");
        return "";
    }
}
string
default_answer()
{
    if(!query_attack())
    {
        set_alarm(2.0, 0.0, "powiedz_gdy_jest", this_player(),
        "pokrec powoli");
        return "";
    }
    else
    {
        set_alarm(0.5,0.0,"powiedz_gdy_jest",TP,"'Zostaw mnie!");
        return "";
    }
}

void
add_introduced(string imie_mia, string imie_bie)
{
    set_alarm(2.0, 0.0, "return_introduce", find_player(imie_mia));
}
void
return_introduce(object ob)
{
    command("przedstaw sie "+ OB_NAME(ob));
}


void
emote_hook(string emote, object wykonujacy)
{
    switch (emote)
    {
        case "kopnij": set_alarm(1.5, 0.0, "nienajlepszy", wykonujacy);
                    break;
        case "spoliczkuj": set_alarm(1.5, 0.0, "nienajlepszy", wykonujacy);
                    break;
        case "opluj" : set_alarm(2.5, 0.0, "nienajlepszy", wykonujacy);
                    break;
        case "poca^luj":
              {
              if(wykonujacy->query_gender() == 1)
                switch(random(1))
                 {
                 case 0:command("usmiechnij sie cieplo "+
                     "do "+OB_NAME(wykonujacy)); break;
                }
              else
                {
                   switch(random(1))
                   {
                   case 0:buzi_sonni; break;
                   }
                }

                break;
                }
        case "prychnij":
              {
                switch(random(1))
                 {
                 case 0:command("spojrzyj lekcewazaco "+
                     "na "+ OB_NAME(wykonujacy)); break;
                 }
                break;
               }
        case "przytul":
            {
              if(wykonujacy->query_gender() == 1)
                switch(random(1))
                {
                  case 0: command("usmiechnij sie slodko "+
                      "do "+ OB_NAME(wykonujacy));break;
                }
                else
                {
                  switch(random(1))
                    {
                      case 0:
                      this_player()->catch_msg(QCIMIE(this_object(),PL_MIA)+
                      " przytula si^e do ciebie oboj^etna, sztywna i zimna.\n");
                      saybb(QCIMIE(this_object(),PL_MIA)+" przytula "+
                      "si^e do "+ QIMIE(this_player(),PL_BIE)+" oboj^etnie.\n");
                      break;
                    }
                }break;
            }

    }
}

void
nienajlepszy(object wykonujacy)
{
   switch(random(1))
   {
      case 0: zle_sonni(); break;
   }
}

string
dlugasny()
{
    string str;
    str = "Jej w^losy s^a rozpuszczone i lu^xn^a, br^azow^a kaskad^a "+
         "opadaj^a a^z na plecy. Wiatr bawi si^e ich pasmami, mi^ekkimi "+
         "niczym jedwab i g^estymi. Na okr^ag^lej, dziewcz^ecej buzi "+
         "l^sni^a wielkie, zielone oczy o niesamowitym odcieniu morskich "+
         "g^l^ebi albo lasu o^swietlonego b^lyskawic^a. Niekt^orzy m^owi^a, "+
         "^ze s^a zielone. Inni, ^ze to pokryte cieniutk^a warstw^a srebra "+
         "szmaragdy. Jej drobna budowa i wiotka kibi^c przyci^agaj^a "+
         "spojrzenia wielu m^e^zczyzn";
    if(find_living("sonni")&&environment(this_object())==environment
        (find_living("sonni")))
    {
        str += ", ale ona wpatrzona jest tylko w tego jednego, kt^ory w^la^snie "+
            "kroczy obok niej. ";
    }
    else
    {
        str += ". ";
    }
    str += "Ubi^or nosi raczej skromny, ale nie biedny � ot, po prostu "+
        "wida^c, ^ze stroje nie s^a najwa^zniejsz^a cz^e^sci^a jej "+
        "^zycia i dba raczej o to, ^zeby wygl^ada^c schludnie ni^z "+
        "wytwornie. Wygl^ada troch^e niczym zagubiona w mie^scie nimfa "+
        "albo m^loda driada, ale jej maniery ^swiadcz^a o tym, ^ze "+
        "wychowywa^la si^e w tym miejscu od dziecka.\n";
    return str;
}

string
eventy()
{
    if(!present("sonni", environment()))
    {
        switch(random(3))
        {
            case 0: command("emote spogl^ada gdzie^s w dal st^esknionym "+
                "wzrokiem.");
            case 1: saybb("Wiatr wzdyma "+
                "sp^odnic^e "+QIMIE(this_object(),PL_DOP)+" i bawi si^e "+
                "w^losami, ale ona nie zwraca na to uwagi.\n");
            case 2: command("':nagle: On wr^oci, on "+
                "zawsze wraca. Ale ja si^e tak o niego boj^e, m^owi^a, "+
                "^ze na drogach grasuj^a zb^ojcy.");
        }
    }
}

buzi_sonni()
{
    if(find_living("darla")&&environment(this_object())==environment(find_living("darla")))
    {
        this_object()->command("':marszcz^ac brwi i odsuwaj^ac si^e: "+
            "Bardzo prosz^e tego nie robi^c.");
    }
    else
    {
        this_object()->command("':u^smiechaj^ac si^e ^lagodnie: "+
                "Przykro mi, ale ja mam ju^z ukochan^a.");
    }
}

zle_sonni()
{
  if(present("sonni", environment()))
    {
      this_player()->catch_msg(QCIMIE(find_living("sonni"),PL_MIA)+" staje "+
          "pomi^edzy tob^a a "+QIMIE(this_object(),PL_NAR)+".\n");
      saybb(QCIMIE(find_living("sonni"),PL_MIA)+" staje "+
          "pomi^edzy "+QIMIE(this_player(),PL_NAR)+
          " a "+QIMIE(this_object(),PL_NAR)+".\n");
    }
    else
    {
        this_object()->command("emote marszczy czo^lo, a w jej wielkich, "+
            "zielonych oczach pojawia si^e b^lysk niezrozumienia i krzywdy.");
    }
}

void
attacked_by(object wrog)
{
    if(walka==0)
    {
      if(present("sonni", environment()))
        {
    this_player()->catch_msg(QCIMIE(find_living("sonni"),PL_MIA)+
        " staje pomi^edzy tob^a, a "+QIMIE(this_object(),PL_NAR)+", "+
        "spogl^adaj^ac na ciebie gro^xnie.\n");
    saybb(QCIMIE(find_living("sonni"),PL_MIA)+
        " staje pomi^edzy "+QIMIE(this_player(),PL_NAR)+", a "+
        QIMIE(this_object(),PL_NAR)+", "+
        "spogl^adaj^ac na "+this_player()->koncowka("niego","ni^a")+
        " gro^xnie.\n");
    set_alarm(0.5, 0.0, "pomoc", 1);
    set_alarm(1.5, 0.0, &koniec_walki(this_player()));
    set_alarm(5.0, 0.0, "czy_walka", 1);
    walka = 1;
        }
        else
        {
    command("krzyknij Na pomoc, morduj^a!");

    set_alarm(5.0, 0.0, "czy_walka", 1);
    walka = 1;
        }
    return ::attacked_by(wrog);
    }

    else
    if(walka == 1)
    {
      set_alarm(1.0, 0.0, "command", "krzyknij Niech mi kto^s pomo^ze!");
      set_alarm(10.0, 0.0, "czy_walka", 1);
      walka = 1;

      return ::attacked_by(wrog);
    }

}

int
czy_walka()
{
    if(!query_attack())
    {

       this_object()->command("zalkaj");

       walka = 0;
       return 1;
    }
       else
       {
       set_alarm(5.0, 0.0, "czy_walka", 1);
       }

}
int
pomoc()
{
    find_living("sonni")->command("wesprzyj darle");
}
int
koniec_walki(object ob)
{
    ob->stop_fight();
    this_object()->stop_fight();
}
/*
void
do_sonniego()
{
    if(query_attack())
    {
        set_alarm(10.0,0.0,"do_sonniego");
        return;
    }
    else
    {
        int i = 0;
        mixed droga=znajdz_sciezke(environment(this_object()),
            ZACHOD_LOKACJE + "ulica8");
        this_object()->command(droga[i]);
        if(i==sizeof(droga)-1)
        {
            set_alarm(300.0,0.0,"zerujemy");
            return;
        }
        else
        {
            i++;
            set_alarm(15.0,0.0,"do_sonniego");
            return;
        }
    }
}

void
do_domu()
{
    if(query_attack())
    {
        set_alarm(10.0,0.0,"do_domu");
        return;
    }
    else
    {
        int i = 0;
        mixed droga=znajdz_sciezke(environment(this_object()),
            POLUDNIE_LOKACJE + "placne");
        this_object()->command(droga[i]);
        if(i==sizeof(droga)-1)
        {
            set_alarm(300.0,0.0,"zerujemy");
            return;
        }
        else
        {
            i++;
            set_alarm(15.0,0.0,"do_domu");
            return;
        }
    }
}
*/