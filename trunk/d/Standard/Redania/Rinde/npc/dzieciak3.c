
/* Dzieciak z Rinde        *
 * Na podst. dzieciakow    *
 * Avarda - Gjoef.         *
 * Opis by Tinardan        */

#pragma unique

#include "dir.h"
#include <std.h>
#include <ss_types.h>
#include <macros.h>
#include <stdproperties.h>
#include <pl.h>

inherit RINDE_NPC_STD;

int czy_walka();
int walka = 0;

void pocaluj(object kto);

void
create_rinde_humanoid()
{
    set_living_name("dzieciak3");
    ustaw_odmiane_rasy("dziewczynka");
    set_gender(G_FEMALE);
    dodaj_nazwy("dziecko");
    dodaj_przym("^sliczny", "^sliczni");
    dodaj_przym("rudow^losy", "rudow^losi");

    set_long("Ubrana tak, jakby w^la^snie wysz^la z zak^ladu krawieckiego " +
    "i to najlepszego w mie^scie. D^lug^a, b^l^ekitn^a jak jej oczy " +
    "sukienk^e ozdobi^la koronkami i koralikami, a na r^ekach nosi " +
    "bielutkie r^ekawiczki. Skacze ^smiesznie ponad ka^lu^zami, staraj^ac " +
    "si^e nie ubrudzi^c nowiutkich, eleganckich bucik^ow. Rude w^losy " +
    "upi^ete ma w d^lugi warkocz ozdobiony wspania^l^a wst^a^zk^a, a " +
    "g^low^e nosi dumnie zadart^a do g^ory, tak �eby wszyscy zauwa^zyli " +
    "jak bogato i elegancko jest ubrana.\n");

    set_stats(({10, 10, 10, 10, 10}));
    add_prop(CONT_I_WEIGHT, 40000);
    add_prop(CONT_I_HEIGHT, 150);
    add_prop(CONT_I_VOLUME, 40000);
    add_prop(LIVE_I_NEVERKNOWN, 1);

    set_act_time(10);
    add_act("emote strzepuje niewidoczny puszek z sukienki.");
	add_act("emote poprawia wst^a^zk^e we w^losach.");
	add_act("fuknij cicho");
	add_act("popatrz przelotnie na mezczyzne"); // heheh, nie
	add_act("popatrz przelotnie na kobiete");   // moj patent
	add_act("rozejrzyj sie powoli");

    set_chat_time(14);
    add_chat("M^oj tata jest kr^olem, a moja mama ksi^e^zniczk^a.");
    add_chat("Jak b^ed^e chcia^la, to wtr^ac^e was wszystkich do wi^ezienia " +
        "i utn^e wam g^lowy.");

	set_cact_time(10);
	add_cact("emote zanosi si^e p^laczem.");
	add_cact("krzyknij Przesta^n!");

    add_armour(RINDE + "przedmioty/ubrania/" +
        "rekawiczki_dla_dziewczynki.c");
    add_armour(RINDE + "przedmioty/ubrania/" +
        "sukienka_dla_dziewczynki.c");
    add_armour(RINDE + "przedmioty/ubrania/" +
        "buciki_dla_dziewczynki.c");

	set_default_answer(VBFC_ME("default_answer"));

	set_restrain_path(({
        CENTRUM_LOKACJE + "placn.c",
        CENTRUM_LOKACJE + "placne.c",
        CENTRUM_LOKACJE + "place.c"}));
    set_monster_home(
        CENTRUM_LOKACJE + "placn.c");
}

void
powiedz_gdy_jest(object player, string tekst)
{
	if (environment(this_object()) == environment(player))
	this_object()->command(tekst);
}

string
default_answer()
{
	set_alarm(1.0, 0.0, "wzrusz ramionami");

    return "";
}

void
add_introduced(string imie_mia, string imie_bie)
{
    set_alarm(2.0, 0.0, "return_introduce", find_player(imie_mia));
}

void
return_introduce(object ob)
{
    command("fuknij cicho");
}


void
emote_hook(string emote, object wykonujacy)
{
    switch (emote)
    {
        case "kopnij":
        case "spoliczkuj": set_alarm(1.5, 0.0, "nienajlepszy", wykonujacy);
            break;

        case "opluj" : set_alarm(2.5, 0.0, "nienajlepszy", wykonujacy);
            break;

        case "przytul":
		case "poca^luj": set_alarm(1.5, 0.0, "pocaluj", wykonujacy);
            break;

        case "pog^laszcz": set_alarm(2.5, 0.0, "command", "prychnij .");
            break;
    }
}


void
nienajlepszy(object wykonujacy)
{
    command("powiedz do " + wykonujacy->query_name(1) + " M^oj tata jest " +
    "kr^olem, kupi ci^e, a potem powiesi na haku za brzuch!");
}

void
attacked_by(object wrog)
{
    if(walka==0)
    {
	    write("^Sliczna rudow^losa dziewczynka p^lacze rozpaczliwie i wo^la o pomoc.\n");
	    saybb("^Sliczna rudow^losa dziewczynka p^lacze rozpaczliwie i wo^la o pomoc.\n");

        set_alarm(5.0, 0.0, "czy_walka", 1);
        walka = 1;

        return ::attacked_by(wrog);
    }

    else if(walka == 1)
    {
        set_alarm(1.0, 0.0, "command", "rozplacz sie");
        set_alarm(10.0, 0.0, "czy_walka", 1);
        walka = 1;

        return ::attacked_by(wrog);
    }
}

int
czy_walka()
{
    if(!query_attack())
    {

        this_object()->command("emote rozgl^ada si^e ze ^lzami w oczach.");
        walka = 0;

        return 1;
    }
    else
        set_alarm(5.0, 0.0, "czy_walka", 1);

}

void
pocaluj(object kto)
{
    if (kto->query_prop("pocalowal_rudowlosa"))
        command("':z obrzydzeniem: Oj przesta^n, i tak si^e musz^e "+
        "dzisiaj porz^adnie umy^c!");
    else
    {
        switch (kto->query_race_name())
        {
            case "elf":
                command("powiedz do " + kto->query_name(1) + " Mama m^owi, " +
                "^ze elfy to pod^le stworzenia.");
                break;

            case "krasnolud":
                command("powiedz do " + kto->query_name(1) + " Wujek mi " +
                "powiedzia^l, ^ze krasnoludy to zawszone ma^le gnojki.");
                break;

            case "nizio^lek":
                command("powiedz do " + kto->query_name(1) + " Ciocia " +
                "m^owi, ^ze nizio^lki s^a g^lupie i leniwe. Czy to prawda?");
                break;

            case "cz^lowiek":
                command("powiedz do " + kto->query_name(1) + " Nie wolno mi " +
                "si^e zadawa^c z posp^olstwem. Mama m^owi, ^ze mo^zna od " +
                "tego pche^l dosta^c.");
                break;

            case "gnom":
                command("powiedz do " + kto->query_name(1) + " Ty si^e taki " +
                "urodzi^le^s, czy kto^s ci krzywd^e zrobi^l?");
                break;

            case "p^o^lelf":
                command("powiedz do " + kto->query_name(1) + " To ja ju^z " +
                "nie wiem czym jeste^s. Ani g^lupi cz^lowiek, ani parszywy " +
                "elf.");
                break;

            case "vran":
                command("powiedz do " + kto->query_name(1) + " Takie " +
                "potwory powinno si^e na smyczy trzyma^c!");
                break;

            case "bobo^lak":
                command("powiedz do " + kto->query_name(1) + " Mama m^owi, " +
                "^ze jak si^e spotka bobo^laka, to si^e go powinno " +
                "wypatroszy^c, bo ^smierdz^a strasznie i roznosz^a choroby.");
                break;
        }

        kto->add_prop("pocalowal_rudowlosa", 1);
    }
}
