/* kupiec z niespodzianka jako element miejskiej siatki przestepczej.
   ten czlowiek da ci nadpilowane strzaly, jesli wiesz co zrobic, jednak
   na razie zaden smiertelnik nie ma prawa wiedziec. opisy Faeve,
   zrobil gjoef */

#pragma unique

#include <macros.h>
#include <stdproperties.h>
#include <money.h>
#include <pl.h>
#include <formulas.h>
#include <std.h>
#include <ss_types.h>
#include <wa_types.h>
#include <composite.h>
#include <filter_funs.h>
#include <pogoda.h>
#include <object_types.h>
#include "dir.h"

inherit RINDE_NPC_STD;
inherit "/lib/sklepikarz.c";

int walka = 0;
string *otrzymali = ({});

int
co_godzine();

void
doko();

void
create_rinde_humanoid()
{
    set_living_name("kupieczrynku");

    ustaw_odmiane_rasy("m^e^zczyzna");
    set_gender(G_MALE);

    dodaj_nazwy("kupiec");
    dodaj_nazwy("kramarz");
    dodaj_nazwy("straganiarz");

    set_long("M^e^zczyzna ten jest niewysoki i lekko przygarbiony. Ju^z " +
    "na pierwszy rzut oka wida^c, ^ze jest kupcem. Chytry u^smieszek " +
    "nieustannie b^l^adzi po jego ustach, a ma^le kaprawe oczka lustruj^a " +
    "wszystkich wok^o^l. Tylko patrzy, kogo by tu nabra^c i oszuka^c. " +
    "Ubrany jest w d^lug^a sukman^e kupieck^a, uszyt^a z czerwonego, " +
    "grubego jedwabiu. Spod d^lugiego r^ekawa, ukrywaj^acego nieco praw^a " +
    "d^lo^n, b^lyszczy z^loty sygnet. Przy pasie ma zaczepion^a sk^orzan^a " +
    "sakw^e, do kt^orej nieustannie si^ega, jakby chc^ac sprawdzi^c, czy " +
    "ta nadal jest na swoim miejscu.\n");

    dodaj_przym("niski", "niscy");
    dodaj_przym("chudy", "chudzi");

    set_act_time(30);
    add_act("rozejrzyj sie niecierpliwie");
    add_act("emote bezwiednie si^ega r^ek^a w stron^e zaczepionej przy " +
    "pasie sakiewki.");
    add_act("potrzyj policzek z namyslem");
    add_act("popatrz blogo na sygnet");
    add_act("krzyknij Panie i panowie, wy^smienita bro^n tylko u mnie!");
    add_act("krzyknij Miecze, ^luki, no^ze, topory! Zapraszam, zapraszam!");
    add_act("krzyknij Panie i panowie, wy^smienita bro^n tylko u mnie!");
    add_act("krzyknij Miecze, ^luki, no^ze, topory! Zapraszam, zapraszam!");
    add_act("krzyknij Hej, nie b^ed^e tu sta^l do jutra!");

    set_cchat_time(15);
    add_cchat("Cholera, zostaw mnie!");
    add_cchat("Stra^z!");
    add_cchat("Nie dostaniesz mojego z^lota!");

    set_default_answer(VBFC_ME("default_answer"));
    add_ask("prac^e", VBFC_ME("pyt_praca"));
    add_ask("pomoc", VBFC_ME("pyt_pomoc"));
    add_ask("towar", VBFC_ME("pyt_towar"));
    add_ask("niespodziank^e", VBFC_ME("pyt_niespodzianka"));
	add_ask(({"wysokiego brodatego m^e^zczyzn^e", "brodatego wysokiego m^e^zczyzn^e",
    "wysokiego brodatego cz^lowieka", "brodatego wysokiego cz^lowieka"}),
	VBFC_ME("pyt_milon"));

    set_stats (({30, 60, 28, 55, 35}));

    set_skill(SS_APPR_OBJ, 70 + random(4));
    set_skill(SS_APPR_VAL, 83 + random(10));
    set_skill(SS_AWARENESS, 70 + random(10));
    set_skill(SS_UNARM_COMBAT, 30 + random(5));

    add_armour(RINDE_UBRANIA + "szaty/dluga_czerwona_M.c");
    add_armour(RINDE_UBRANIA + "pasy/szeroki_ciemnobezowy_M.c");
    add_armour(RINDE_UBRANIA + "buty/skorzane_wygodne_M.c");
    add_armour(RINDE_UBRANIA + "pierscienie/duzy_zloty_L.c");

    add_prop(CONT_I_WEIGHT, 70000);
    add_prop(CONT_I_HEIGHT, 165);

    set_alarm_every_hour("co_godzine");
    co_godzine();

    config_default_sklepikarz();
    //set_money_greed_buy(111);
    //set_money_greed_sell(110);

    set_money_greed_buy(211);
    set_money_greed_sell(511);

    set_money_greed_change(103);
    set_co_skupujemy(O_BRON_MIECZE|O_BRON_MIECZE_2H|O_BRON_SZABLE|
    				O_BRON_SZTYLETY|O_BRON_TOPORY|O_BRON_TOPORY_2H|
    				O_BRON_DRZEWCOWE_D|O_BRON_DRZEWCOWE_K|O_BRON_MACZUGI|O_BRON_MLOTY|
    				O_BRON_MIOTANE|O_BRON_STRZELECKIE|O_BRON_STRZALY);

    set_store_room("/d/Standard/Redania/Rinde/Centrum/lokacje/magazyn_kupiec_maly.c");

    set_alarm(3.0, 0.0, "command", "stan za pierwszym straganem");
    set_alarm(8.0, 0.0, "command", "emote k^ladzie towar na straganie.");

    add_prop(NPC_M_NO_ACCEPT_GIVE, 0);

    set_czy_handlujemy(1);
}

void
shop_hook_tego_nie_skupujemy(object ob)
{
    command("powiedz Skupuj� tylko bronie.");
}

void
powiedz_gdy_jest(object player, string tekst)
{
    if ((environment(this_object()) == environment(player)) && CAN_SEE(this_object(),
    player))
	this_object()->command(tekst);
	else
    this_object()->command("wzrusz ramionami");
}

string
default_answer()
{
    string odp = " ";

    switch(random(3))
    {
        case 0:
            odp += "Nie mam teraz czasu na my^slenie.";
            break;
        case 1:
            odp += "Nie teraz, jestem zaj^ety.";
            break;
        case 2:
            odp += "Doprawdy, p^o^xniej porozmawiamy.";
            break;
    }

    set_alarm(2.0, 0.0, "powiedz_gdy_jest", this_player(), "powiedz do " +
        OB_NAME(this_player()) + odp);
    return "";
}

string
pyt_milon()
{
    if (TP->query_prop("pytal_kupca_o_milona_do_cholery") == 0)
    {
         TP->add_prop("pytal_kupca_o_milona_do_cholery", 1);
	    set_alarm(2.0, 0.0, "powiedz_gdy_jest", this_player(),
    	"powiedz do " + OB_NAME(TP) + " Widzia^lem takiego jednego, " +
        "poszed^l tam!");
    	set_alarm(4.0, 0.0, "powiedz_gdy_jest", this_player(),
    	"wskaz n");
	    set_alarm(7.0, 0.0, "powiedz_gdy_jest", this_player(),
    	"powiedz do " + OB_NAME(TP) + " A mo^ze tam..?");
    	set_alarm(9.0, 0.0, "powiedz_gdy_jest", this_player(),
    	"wskaz");
    }
    else
        set_alarm(2.0, 0.0, "powiedz_gdy_jest", this_player(),
    	"powiedz do " + OB_NAME(TP) + " W ka^zdym razie tu go nie ma, nie");

    return "";
}

string
pyt_praca()
{
    if (this_player()->query_prop("pytal_kupcazrynku_praca") == 0)
    {
        set_alarm(1.0, 0.0, "powiedz_gdy_jest", TP, "powiedz do " + OB_NAME(TP) +
        " Dzi^ekuj^e, sam sobie radz^e.");
        TP->add_prop("pytal_kupcazrynku_praca", 1);
    }
    else
        set_alarm(1.0, 0.0, "powiedz_gdy_jest", TP, "powiedz do " + OB_NAME(TP) +
        " M^owi^lem ju^z, poszukaj gdzie indziej.");

    return "";
}

string
pyt_pomoc()
{
    set_alarm(1.0, 0.0, "powiedz_gdy_jest", TP, "powiedz do " + OB_NAME(TP) +
        " Nie, nie potrzebuj^e pomocy.");

    return "";
}

string
pyt_towar()
{
    set_alarm(1.0, 0.0, "powiedz_gdy_jest", TP, "powiedz do " + OB_NAME(TP) +
        " Ca^ly towar jest tu wy^lo^zony.");
    set_alarm(2.0, 0.0, "powiedz_gdy_jest", TP, "wskaz stragan");
}

string
pyt_niespodzianka()
{
    object niespodzianka;

    this_object()->command("rozejrzyj sie szybko");

    if (sizeof(FILTER_LIVE(deep_inventory(environment(this_player()))) -
        ({this_player(), this_object()})))
        default_answer();
    else
    {
        if (member_array(TP->query_name(), otrzymali) != -1)
            set_alarm(1.5, 0.0, "powiedz_gdy_jest", TP, "powiedz do " +
            OB_NAME(TP) + " Daj mi spok^oj.");
        else
        {
            seteuid(getuid());

            LOAD_ERR("/d/Standard/Redania/Rinde/przedmioty/dumdumkit");
            niespodzianka = clone_object("/d/Standard/Redania/Rinde/przedmioty/dumdumkit");
            niespodzianka->move(this_object());

            otrzymali += ({TP->query_name()});
            write_file("/d/Aretuza/gjoef/log/niespodzianka",
            this_player()->query_name(0) + "    " + ctime(time()) + "\n");
            save_object("/d/Aretuza/gjoef/kupiec");

            set_alarm(1.5, 0.0, "command", "daj " + OB_NAME(niespodzianka) +
                " " + OB_NAME(this_player()));
            set_alarm(2.5, 0.0, "doko");
        }
    }

    return "";
}

void
return_introduce(object ob)
{
    if (environment() == environment(ob))
    {
        set_alarm(1.0, 0.0, "command", "powiedz do " +
        OB_NAME(this_player()) + " Mi^lo mi, mo^ze wybierze pan" +
        this_player()->koncowka("", "i") + " co^s z mojego towaru?");
        set_alarm(2.0, 0.0, "command", "wskaz " +
        this_object()->query_prop("stoi_za_straganem"));
    }

    return;
}

void
emote_hook(string emote, object wykonujacy, string przyslowek)
{
    switch (emote)
    {
        case "kopnij":
        case "spoliczkuj":
        case "opluj":
    	case "nadepnij":
	    set_alarm(2.0, 0.0, "zly", wykonujacy);
            break;

        case "szturchnij":
	    set_alarm(2.0, 0.0, "taki_sobie", wykonujacy);
            break;

        case "za�miej":
        case "poca�uj":
        case "przytul":
        case "pog�aszcz":
	    set_alarm(2.0, 0.0, "dobry", wykonujacy);
            break;
    }
}

void
zly(object kto)
{
    if (kto->query_prop("wkurzyl_kupcazrynku_blabla") < 2)
    {
        powiedz_gdy_jest(kto, "powiedz do " + OB_NAME(kto) + " Nie b^ed^e " +
        "tolerowa^l takiego zachowania! Jeszcze raz i wezw^e stra^z!");
        kto->add_prop("wkurzyl_kupcazrynku_blabla", kto->query_prop("wkurzy" +
        "l_kupcazrynku_blabla") + 1);
    }
    else
    {
        command("krzyknij Stra^zeee!");
        set_alarm(1.0, 0.0, "command", "sapnij ciezko");
        set_alarm(2.0, 0.0, "powiedz_gdy_jest", kto, "powiedz do " +
        OB_NAME(kto) + " No to teraz masz przesrane.");
    }

    return;
}

void
taki_sobie(object kto)
{
    command("popatrz spode lba na " + OB_NAME(kto));
}

void
dobry(object kto)
{
    switch(random(3))
    {
        case 0:
            powiedz_gdy_jest(kto, "powiedz do " + OB_NAME(kto) +
            " Prosz^e natychmiast mnie zostawi^c!");
            break;
        case 1:
             powiedz_gdy_jest(kto, "powiedz do " + OB_NAME(kto) +
             " Brr, bo zawo^lam stra^z!");
             break;
        case 2:
             command("powiedz :cicho: Cholera, co za ludzie.");
             break;
    }

    set_alarm(1.0, 0.0, "command", "emote odsuwa si^e z obrzydzeniem.");
    return;
}

void
attacked_by(object wrog)
{
    if(walka==0)
    {
	    command("wyjdz zza straganu");

        set_alarm(5.0, 0.0, "czy_walka", 1);
        walka = 1;

        return ::attacked_by(wrog);
    }

    else if(walka == 1)
    {
        set_alarm(1.0, 0.0, "command", "sapnij ciezko");
        set_alarm(10.0, 0.0, "czy_walka", 1);
        walka = 1;

        return ::attacked_by(wrog);
    }
}

int
czy_walka()
{
    if(!query_attack())
    {
        set_alarm(1.0, "command", "sapnij ciezko");
        go_to("/d/Standard/Redania/Rinde/Centrum/lokacje/placne",
            &command("stan za pierwszym straganem"));
        walka = 0;

        return 1;
    }
    else
        set_alarm(5.0, 0.0, "czy_walka", 1);
}

void
hook_zabr_podrz_wlas_drog(object kto, object co)
{
    set_alarm(1.0, 0.0, "command", "westchnij cichutko");
}

void
hook_zabr_podrz_wlas_tan(object kto, object co)
{
    set_alarm(1.0, 0.0, "powiedz_gdy_jest", kto, "powiedz do " + OB_NAME(kto)+
    " No, a nast^epnym razem to niech pan" + kto->koncowka("", "i") +
    " pilnuje swojego!");
}

void
hook_zabr_podrz_niewl_wljest(object kto, object co, string czyje)
{
    set_alarm(1.0, 0.0, "powiedz_gdy_jest", kto, "powiedz do " + OB_NAME(kto)+
    " Hej, zaraz! T" + co->koncowka("en", "a", "o", "e", "e") + " " +
    co->query_nazwa() + " jest te" + find_player(lower_case(co->query_prop("podrzucon" +
    "e_na_stragan")))->koncowka("go pana", "ej pani", "go pana") + "!");
    set_alarm(1.5, 0.0, "command", "wskaz " +
    OB_NAME(find_player(co->query_prop("podrzucone_na_stragan"))));
    co->add_prop("podrzucone_na_stragan", czyje);
}

void
hook_zabr_podrz_niewl_wlniema(object kto, object co)
{
    set_alarm(1.0, 0.0, "powiedz_gdy_jest", kto, "powiedz do " + OB_NAME(kto)+
    " Hej, to nie pan" + kto->koncowka("a", "i", "a") + "!");
}

void
hook_zabr_pierwsza_rzecz(object kto, object co)
{
    set_alarm(1.0, 0.0, "powiedz_gdy_jest", kto, "powiedz do " + OB_NAME(kto)+
    " Prosz^e natychmiast od^lo^zy^c! To nie jest do macania!");
}

void
hook_zabr_pierwsza_rzecz_vip(object kto, object co)
{
    set_alarm(1.0, 0.0, "command", "popatrz niechetnie na " + OB_NAME(kto));
}

void
hook_zabr_druga_rzecz(object kto, object co)
{
    set_alarm(1.0, 0.0, "command", "krzyknij Oddawaj to, pieprzon" +
    kto->koncowka("y z^lodzieju", "a z^lodziejko", "y z^lodzieju") +
    "! Straaa^ze!");
    set_alarm(2.0, 0.0, "command", "wyjdz zza straganu");
    set_alarm(3.0, 0.0, "command", "zabij " + OB_NAME(kto));
}

void
hook_zabr_podrz_wlas_x(object kto, object co)
{
    set_alarm(1.0, 0.0, "powiedz_gdy_jest", kto, "powiedz do " + OB_NAME(kto)+
    " Wrrr, oddawaj to, co zabra^l" + kto->koncowka("e", "a", "o") + "^s!");
}

void
hook_pol_swoj_tan(object kto, object co)
{
    set_alarm(1.0, 0.0, "powiedz_gdy_jest", kto, "powiedz do " + OB_NAME(kto)+
    " No i co mi tu pan" + kto->koncowka("", "i") + " k^ladzie, co? ^Zeby " +
    "jeszcze kto^s gwizdn^a^l?");
}

void
hook_pol_swoj_drog_wljest(object kto, object co)
{
    set_alarm(1.0, 0.0, "command", "wytrzeszcz oczy");
    set_alarm(2.0, 0.0, "powiedz_gdy_jest", kto, "powiedz do " + OB_NAME(kto)+
    " To przecie^z... to przecie^z musi sporo kosztowa^c... prawda?");
    set_alarm(5.0, 0.0, "command", "popatrz tesknie na " + OB_NAME(co));
}

void
hook_pol_swoj_drog_wlniema(object kto, object co)
{
    set_alarm(1.0, 0.0, "command", "powiedz :cicho: Kto^s musi si^e tym... " +
    "zaopiekowa^c...");
    set_alarm(2.0, 0.0, "command", "wez " + OB_NAME(co));
    set_alarm(3.0, 0.0, "command", "rozejrzyj sie szybko");
}

void
hook_pol_zabrane_nieten(object kto, object co)
{
    set_alarm(1.0, 0.0, "command", "powiedz O! No i si^e znalaz^la zguba.");
    set_alarm(2.0, 0.0, "command", "podziekuj szybko " + OB_NAME(kto));
}

void
hook_pol_zabrane_ten(object kto, object co)
{
    set_alarm(1.0, 0.0, "powiedz_gdy_jest", kto, "powiedz do " + OB_NAME(kto)+
    " No! A jak nast^epnym razem b^edziesz chcia^l co^s zobaczy^c, to " +
    "^ladnie popro^s.");
    set_alarm(2.0, 0.0, "command", "popatrz wymownie na " + OB_NAME(kto));
}

void
hook_pol_swoj_ten(object kto, object co)
{
    set_alarm(1.0, 0.0, "powiedz_gdy_jest", kto, "powiedz do " + OB_NAME(kto)+
    " Co to jest? No co to, do cholery ci^e^zkiej, jest?! Ju^z mi oddawaj " +
    "moj^a w^lasno^s^c!");
    set_alarm(2.0, 0.0, "command", "sapnij ciezko");
}

void
hook_pol_zabrane_zlodziej(object kto, object co)
{
    set_alarm(1.0, 0.0, "powiedz_gdy_jest", kto, "powiedz do " + OB_NAME(kto)+
    " Wszystko, wszystko ma do mnie wr^oci^c!");
}

void
hook_oddal_zabr_ten(object kto, object co)
{
    hook_pol_zabrane_ten(kto, co);
}

void
hook_oddal_zabr_nieten(object kto, object co)
{
    hook_pol_zabrane_nieten(kto, co);
}

void
hook_dal_ten(object kto, object co)
{
    set_alarm(1.0, 0.0, "powiedz_gdy_jest", kto, "powiedz do " + OB_NAME(kto)+
    " Wrrr, bierz to pan " + kto->koncowka("", "i") + " i oddawaj moj^a " +
    "w^lasno^s^c!");
    if (environment() == environment(kto))
    {
        co->remove_prop("podrzucone_na_stragan");
        set_alarm(2.0, 0.0, "command", "daj " + OB_NAME(co) + " " +
        OB_NAME(kto));
    }
    else
        set_alarm(2.0, 0.0, "command", "poloz " + OB_NAME(co) + " na " +
        this_object()->query_prop("stoi_za_straganem"));
}

void
hook_dal_ktos(object kto, object co)
{
    if(!co->query_coin_type())
    {

    set_alarm(1.0, 0.0, "powiedz_gdy_jest", kto, "powiedz do " + OB_NAME(kto)+
            " No i co mi tu pan" + kto->koncowka("", "i") + " daje, co?");

    if (environment() == environment(kto))
    {
        co->remove_prop("podrzucone_na_stragan");
        set_alarm(2.0, 0.0, "command", "daj " + OB_NAME(co) + " " +
        OB_NAME(kto));
    }
    else
        set_alarm(2.0, 0.0, "command", "poloz " + OB_NAME(co) + " na " +
        this_object()->query_prop("stoi_za_straganem"));
    }
}

void
enter_inv(object ob, object from)
{
    ::enter_inv(ob, from);

    if (from->is_humanoid() == 0)
        return;

    if (ob->query_prop("zabrane_ze_straganu"))
    {
        if (from->query_prop("zabral_ze_straganu"))
        {
            hook_oddal_zabr_ten(from, ob);
            ob->remove_prop("zabrane_ze_straganu");
            from->remove_prop("zabral_ze_straganu");
            return;
        }
        else
        {
            hook_oddal_zabr_nieten(from, ob);
            ob->remove_prop("zabrane_ze_straganu");
            return;
        }
    }
    else
    {
        if (from->query_prop("zabral_ze_straganu"))
        {
            ob->add_prop("podrzucone_na_stragan", from->query_name());
            hook_dal_ten(from, ob);
            return;
        }
        else
        {
            ob->add_prop("podrzucone_na_stragan", from->query_name());
            hook_dal_ktos(from, ob);
            return;
        }
    }
}

void
leave_inv(object ob, object to)
{
    ::leave_inv(ob, to);
}

void
znik()
{
    set_alarm(5.0, 0.0, "command", "emote przechodzi przez bram^e.");
    remove_object();
}

void
do_domu()
{
    set_czy_handlujemy(0);

    if(query_attack())
    {
        set_alarm(10.0,0.0,"do_domu");
        return;
    }
    else
    {
        int k = 0;
        mixed droga2=znajdz_sciezke(environment(this_object()),
            POLUDNIE + "lokacje/bramas");
        command(droga2[k]);
        if(k==sizeof(droga2)-1)
        {
            znik();
            return;
        }
        else
        {
            k++;
            set_alarm(15.0,0.0,"do_domu");
            return;
        }
    }
}

int
co_godzine()
{
    if (environment(TO)->pora_dnia() >= MT_WIECZOR)
    {
        set_alarm(4.0, 0.0, "command", "krzyknij Jeszcze chwila i ko^ncz^e!");
        set_alarm(250.0, 0.0, "command", "emote zdejmuje towar ze straganu.");
        set_alarm(253.0, 0.0, "command", "wyjdz zza straganu");
        set_alarm(255.0, 0.0, "do_domu");
    }
}

void
zeruj_otrzymali()
{
    otrzymali = ({});
    save_object("/d/Aretuza/gjoef/kupiec");
    return;
}

void
pokaz_otrzymali()
{
    tell_object(find_player("gjoef"), dump_array(otrzymali));
}

void
doko()
{
    if (sizeof(FILTER_LIVE(deep_inventory(environment(this_player()))) -
        ({this_player(), this_object()})))
        powiedz_gdy_jest(TP, "powiedz do " + OB_NAME(TP) + " ^Swietny " +
        "wyb^or, gratuluj^e pan" + TP->koncowka("u", "i") + "!");
    else
        powiedz_gdy_jest(TP, "powiedz do " + OB_NAME(TP) + " I pami^etaj - " +
        "nigdy ci^e nie widzia^lem!");

    set_alarm(2.0, 0.0, "command", "popatrz nerwowo na " + OB_NAME(TP));
}

void
init()
{
    ::init();
    init_sklepikarz();
}
