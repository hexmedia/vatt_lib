#pragma unique

#include <std.h>
#include <ss_types.h>
#include <macros.h>
#include <stdproperties.h>
#include <pl.h>
#include "dir.h"

inherit RINDE_NPC_STD;

int czy_walka();
int walka = 0;

void
create_rinde_humanoid()
{
    ustaw_odmiane_rasy(PL_MEZCZYZNA);
    set_living_name("plecisz");
    set_gender(G_MALE);
    ustaw_imie(({"p^ecisz","p^ecisza","p^eciszowi","p^ecisza","p^eciszem",
        "p^eciszu"}),PL_MESKI_OS);
    //set_title(", ");

    set_long("Od tego rozgl^adaj^acego si^e m^etnym wzrokiem m^e^zczyzny "+
        "bochodz^a ci^e do^s^c wyra^xne zapach piwa i potu. Pierwszy jest "+
        "tylko jedn^a z oznak, ^ze wypi^l dzi^s nie jedno i nie dwa, drugi "+
        "za^s niew^atpliwie to efekt jego do^s^c poka^xnej tuszy. Wydatne "+
        "brzuszysko nie daje si^e opasa^c ca^lkiem po^z^adnym, ale niechlujnie"+
        "utrzymanym kubrakiem z czarnej sk^ory. Jego mocne, ^zylaste d^lonie "+
        "niespokojnie b^l^adz^a po nim, to zn^ow zag^l^ebiaj^a si^e w "+
        "kieszenie. Bystre, szaroniebieskie oczy niespokojnie lustruj^a "+
        "okolic^e, a szeroki u^smiech rozja^snia nalan^a twarz z byle, a "+
        "nieraz wr^ecz bez powodu. Czarne, lekko szpakowate na skroniach "+
        "w^losy nosi kr^otko zgolone, tak ^ze przyj^zywszy si^e z bliska "+
        "mo^zna dostrzec kropl^e potu sp^lywaj^ac^a powolo po potyliy, a "+
        "potem coraz szybciej po karku, by wreszcie znikn^a^c za wymi^etym "+
        "ko^lnierzem.\n");

    dodaj_przym("spasiony","spasieni");
    dodaj_przym("podpity","podpici");

    set_stats (({ 65, 35, 50, 60, 65 }));

    add_prop(CONT_I_WEIGHT, 130000);
    add_prop(CONT_I_HEIGHT, 175);

    set_act_time(20);
    add_act("'Dzi^s ^swi^etujemy! Za miastem b^edzie wielkie ognisko. Mog^e "+
	"wyt^lumaczy^c drog^e.");
    add_act("'Pono^c ma by^c darmowe piwo!");
    add_act("'^Swi^etujemy dzi^s! Ognisko przy po^ludniowej bramie!");
    /*Te sa na po-evencie:
    add_act("'Chce kto co^s ciekawego us^lysze^c?");
    add_act("'A ja co^s wiem!");
    add_act("emote grzebie po kieszeniach.");
    add_act("emote przeci^aga si^e mocno ziewaj^ac przy tym pot^e^znie.");
    */

    set_cchat_time(10);
    add_cchat("Stra^z! Nigdy ich nie ma, jak s^a, psia ich ma^c potrzebni! ");
    add_cchat("Kurw... To teraz zobaczymy... ");
    add_cchat("Zdychaj do kurwy n^edzy! ");

    add_armour(RINDE_UBRANIA + "kaftany/czarny_wyszywany_M.c");
    add_armour(RINDE_UBRANIA + "buty/wysokie_skorzane_M.c");
    add_armour(RINDE_UBRANIA + "spodnie_los_los_biedaka.c");

    add_ask(({"rinde"}), VBFC_ME("pyt_o_rinde"));
    add_ask(({"miasto"}), VBFC_ME("pyt_o_rinde"));
    add_ask(({"karczme"}), VBFC_ME("pyt_o_karczme"));
    add_ask(({"prac^e"}), VBFC_ME("pyt_o_prace"));
    add_ask(({"zadanie"}), VBFC_ME("pyt_o_zadanie"));
	add_ask(({"ognisko","drog^e","drog^e na ognisko"}), VBFC_ME("pyt_o_ognisko"));
    //add_ask(({"plotki"}), VBFC_ME("pyt_o_plotki")); TODO: dodac po evencie!
                                                   //i ma chciec piwo za info!
    set_default_answer(VBFC_ME("default_answer"));

    set_alarm_every_hour("co_godzine");
}

void
start_me()
{
    set_alarm(1.0, 0.0, "command", "usiadz przy stole");
}

void
powiedz_gdy_jest(object player, string tekst)
{
    if (environment(this_object()) == environment(player))
        this_object()->command(tekst);
}

string
pyt_o_rinde()
{
    if(!query_attack())
    {
        set_alarm(1.0, 0.0, "powiedz_gdy_jest", this_player(),
        "powiedz Mi^le miasteczko. Troche elf^ow si^e kr^eci po lasach, "+
        "ale idzie ^zy^c. I nawet piwo nienajgorsze maj^a.");
    }
    else
    {
        set_alarm(1.0,0.0,"powiedz_gdy_jest", this_player(),
            "powiedz Chwila, tylko kto^s tu w ryj dostanie!");
    }
    return "";

}
string
pyt_o_karczme()
{
    if(!query_attack())
    {
    set_alarm(1.0, 0.0, "powiedz_gdy_jest", this_player(),
        "powiedz Noo... to tu, nie? ");
    }
    else
    {
        set_alarm(1.0,0.0,"powiedz_gdy_jest", this_player(),
            "powiedz Chwila, tylko kto^s tu w ryj dostanie!");
    }
    return "";

}

string
pyt_o_prace()
{
    if(!query_attack())
    {
    set_alarm(1.0, 0.0, "powiedz_gdy_jest", this_player(),
        "powiedz Moja robota to moja sprawa.");
    }
else
    {
        set_alarm(1.0,0.0,"powiedz_gdy_jest", this_player(),
            "powiedz Chwila, tylko kto^s tu w ryj dostanie!");
    }
    return "";

}

string
pyt_o_zadanie()
{
    if(!query_attack())
    {
    set_alarm(1.0, 0.0, "powiedz_gdy_jest", this_player(),
        "powiedz Na twoim miejscu pyta^lbym Pod Weso^lym D�inem. ");
    }
    else
    {
        set_alarm(1.0,0.0,"powiedz_gdy_jest", this_player(),
            "powiedz Chwila, tylko kto^s tu w ryj dostanie!");
    }
    return "";
}

string
pyt_o_ognisko()
{
    if(!query_attack())
    {
    set_alarm(1.0, 0.0, "powiedz_gdy_jest", this_player(),
        "powiedz Wyjd^x przez po^ludniow^a bram^e i skr^e^c na zach^od.");
    }
    else
    {
        set_alarm(1.0,0.0,"powiedz_gdy_jest", this_player(),
            "powiedz Chwila, tylko kto^s tu w ryj dostanie!");
    }
    return "";
}

string
default_answer()
{
    if(!query_attack())
    {
    switch(random(2))
    {
    case 0:set_alarm(2.0, 0.0, "powiedz_gdy_jest", this_player(),
        "'Jak piwko postawisz, to bym m^og^l paroma plotkami si^e "+
		"podzieli^c."); break;
    case 1:set_alarm(2.0, 0.0, "powiedz_gdy_jest", this_player(),
        "':szeptem: Chcesz plotek? My^l^e, ^ze co^s si^e da zrobi^c."); break;
    }
    }
    else
    {
        set_alarm(1.0,0.0,"powiedz_gdy_jest", this_player(),
            "powiedz Chwila, tylko kto^s tu w ryj dostanie!");
    }
    return "";
}

void
add_introduced(string imie_mia, string imie_bie)
{
    set_alarm(2.0, 0.0, "return_introduce", find_player(imie_mia));
}
void
return_introduce(object ob)
{
    command("przedstaw sie " + OB_NAME(ob));

}

void
emote_hook(string emote, object wykonujacy)
{
    switch (emote)
    {
        case "kopnij": set_alarm(1.5, 0.0, "nienajlepszy", wykonujacy);
                    break;
        case "spoliczkuj": set_alarm(1.5, 0.0, "nienajlepszy", wykonujacy);
                    break;
        case "opluj" : set_alarm(2.5, 0.0, "nienajlepszy", wykonujacy);
                    break;
        case "poca^luj":
              {
              if(wykonujacy->query_gender() == 1)
                  {
              switch(random(3))
                        {
                 case 0:command("emote u^smiecha si^e oble^snie. "); break;
                 case 1:command("popatrz ^lakomie na "+OB_NAME(wykonujacy));
                        break;
                 case 2:command("popatrz z usmiechem na "+
                        OB_NAME(wykonujacy));
                        break;
                        }
                  }
                     else
                  {
                   switch(random(3))
                        {
                   case 0:command("'To jaki^s g^lupi ^zart?"); break;
                   case 1:command("'Poszo^l won!");
                            break;
                   case 2:command("spojrzyj z obrzydzeniem na "+
                        OB_NAME(wykonujacy));
                            break;
                        }
                  }


               }
               break;
        case "przytul": set_alarm(2.5, 0.0, "malolepszy", wykonujacy);
                   break;
        case "pog^laszcz": set_alarm(2.5, 0.0, "malolepszy", wykonujacy);
                   break;
    }
}

void
malolepszy(object wykonujacy)
{
    if(wykonujacy->query_gender() == 1)
           switch(random(2))
           {
            case 0: set_alarm(0.5, 0.0, "powiedz_gdy_jest", wykonujacy,
                "No ju^z zostaw, zostaw mnie. "); break;
            case 1: set_alarm(0.5, 0.0, "powiedz_gdy_jest", wykonujacy,
                "':szeptem: Potem przyjd^x, teraz mam tu pare soczystych "+
                "nowin do rozpuszczenia."); break;
           }
           else
            {
               switch(random(2))
                {
                   case 0: set_alarm(0.5, 0.0, "powiedz_gdy_jest", wykonujacy,
                    "'Won pijaku. �eb w koryto wsad^x. "); break;
                   case 1: set_alarm(0.5, 0.0, "powiedz_gdy_jest", wykonujacy,
                       "'Zaraz kto^s tu dostanie w pysk."); break;
                }
            }

}

void
nienajlepszy(object kto)
{
   switch(random(3))
   {
      case 0: set_alarm(0.5, 0.0, "powiedz_gdy_jest", kto,
          "'Zostaw, bo stra^z zawo^lam! ");
              break;
      case 1: set_alarm(0.5, 0.0, "powiedz_gdy_jest", kto,
              "'No zostaw oberwa^ncu. ");
              break;
      case 2: set_alarm(0.5, 0.0, "powiedz_gdy_jest", kto,
          "'Wezwij kto^s stra^z!.");
              break;
   }
}

void attacked_by(object wrog)
{
    if(walka == 0)
    {
        walka = 1;
        set_alarm(0.5,0.0,"command","krzyknij Zaraz si^e posypi^a czyje^s z^eby!");
        set_alarm(15.0, 0.0, "czy_walka", 1);
        return ::attacked_by(wrog);
    }
    else
    {
        return ::attacked_by(wrog);
    }
}
int
czy_walka()
{
    if(!query_attack())
    {
        set_alarm(0.5,0.0,"command","powiedz Niech tak b^edzie.");
        walka = 0;
        return 1;
    }
    else
    {
        set_alarm(15.0, 0.0, "czy_walka", 1);
    }
}

nomask int
query_reakcja_na_walke()
{
    return 1;
}