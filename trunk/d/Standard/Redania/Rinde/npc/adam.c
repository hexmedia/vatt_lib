
#pragma unique

#include <macros.h>
#include <stdproperties.h>
#include <money.h>
#include <ss_types.h>
#include "dir.h"

inherit RINDE_NPC_STD;

object obj;
void misa(int faza);
void sedno_sprawy(int faza);
void woda(int faza);
int obsluga();
int s;
int a;

void
create_rinde_humanoid()
{
    ustaw_odmiane_rasy(PL_CZLOWIEK);
    set_gender(G_MALE);

    ustaw_imie (({"adam", "adama", "adamowi", "adama", "adamem", "adamie"}), PL_MESKI_OS);

    set_long("Pierwszy mezczyzna czyli ADAM, jego jedynym przeznaczeniem "+
             "jest bycie obiektem badan naukowych.\n");

    add_ask(({"wode", "mise", "miske"}), VBFC_ME("odpowiedz"));

}

int
obsluga()
{
    if (s == 0)
    {
        s = 1;
        obj = environment();

        if(file_name(obj) != "/d/Redania/gregh/wioska/chata3")
        {

            if(file_name(obj) == "/d/Redania/gregh/wioska/stodola")
            {
                set_alarm(5.0,0.0,"misa",2);
                return 1;
            }

        set_alarm(20.0,0.0,"misa",0);
        return 1;
        }



        set_alarm(15.0,0.0,"misa",1);
        return 1;
    }
}



void
misa(int faza)
{
    switch(faza)
    {
    case 0:
    if(query_attack()){s = 0;break;}
    command("emote wychodzi, mamroczac cos pod nosem nerwowo.");
    move("/d/Redania/gregh/wioska/chata3.c");
    command("emote przybywa, klnac cos pod nosem.");
    a = set_alarm(5.0,0.0,"misa",1);
    break;

    case 1:
    if(query_attack()){s = 0;break;}
    command("se");
    a = set_alarm(2.0,0.0,"misa",2);
    break;

    case 2:
    if(query_attack()){s = 0;break;}
    command("powiedz Czego???");
    a = set_alarm(15.0,0.0,"misa",3);
    break;

    case 3:
    if(query_attack()){s = 0;break;}
    command("tupnij");
    a = set_alarm(10.0,0.0,"misa",4);
    break;

    case 4:
    if(query_attack()){s = 0;break;}
    command("emote mruczy cos pod nosem.");
    a = set_alarm(2.0,0.0,"misa",5);
    break;

    case 5:
    if(query_attack()){s = 0;break;}
    command("nw");
    s = 0;
    break;

   }
}


string
odpowiedz()
{
    remove_alarm(a);
    sedno_sprawy(0);

}

void
sedno_sprawy(int faza)
{

    switch(faza)
    {
    case 0:
    if(query_attack()){s = 0;break;}

    if(file_name(environment()) != "/d/Redania/gregh/wioska/stodola")
    {
    woda(0);
    break;
    }
    if (s != 2)
    {
        s = 2;
        set_alarm(2.0,0.0,"sedno_sprawy",1);
        break;
    }
    break;

    case 1:
    if(query_attack()){s = 0;break;}
    command("splun");
    set_alarm(2.0,0.0,"sedno_sprawy",2);
    break;

    case 2:
    if(query_attack()){s = 0;break;}
    command("powiedz A gdzie ta miska co tu miala byc???");
    set_alarm(3.0,0.0,"sedno_sprawy",3);
    break;

    case 3:
    if(query_attack()){s = 0;break;}
    command("nw");
    s = 0;
    break;
    }


}

void
woda(int faza)
{

    switch(faza)
    {
    case 0:
    if (s != 2)
    {
        s = 2;
        set_alarm(2.0,0.0,"woda",1);
        break;
    }
    break;

    case 1:
    if(query_attack()){s = 0;break;}
    command("powiedz Taka mam robote, ze wode roznosze po pokojach...");
    s = 0;
    break;
    }


}
