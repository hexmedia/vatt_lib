
/*
 * Trubadur na ulicy Rinde
 * Wykonany przez Avarda dnia 07.06.06
 * Opis: Tinardan
 */

#pragma unique

#pragma strict types
#include <std.h>
#include <ss_types.h>
#include <macros.h>
#include <stdproperties.h>
#include <pl.h>
#include "dir.h"

inherit RINDE_NPC_STD;

int czy_walka();
int walka = 0;

void
create_rinde_humanoid()
{
     ustaw_odmiane_rasy(PL_MEZCZYZNA);
     set_gender(G_MALE);
     ustaw_imie(({"kaemir","kaemira","kaemirowi","kaemira","kaemirem",
         "kamirze"}), PL_MESKI_OS);
     set_title(", Trubadur");
     dodaj_nazwy("trubadur");


     set_long("Nie wiadomo co wyr^o^znia go z t^lumu najbardziej: barwny, "+
         "krzykliwy str^oj ulicznego trubadura, czerwony kapelusik z trzema "+
         "farbowanymi pi^orkami, lutnia czy mo^ze pi^ekny g^los, kt^ory "+
         "rozlega si^e co jaki^s czas na zat^loczonej ulicy. M^e^zczyzna "+
         "ma wyra^xne cechy miesza�ca - uszy o lekko wyd^lu^zonym "+
         "kszta^lcie, migda^lowe, intensywnie niebieskie oczy i ludzkie "+
         "kie^lki, kt^ore ukazuje w u^smiechu. A u^smiecha si^e cz^esto, "+
         "snuj^ac swoje pie^sni, tr^acaj^ac smuk^lymi palcami struny lutni "+
         "i spogl^adaj^ac wok^o^l rozmarzonym wzrokiem. Jego delikatne rysy "+
         "maj^a w sobie co^s szlachetnego, co przyci^aga uwag^e kobiet, "+
         "zar^owno prostych wie^sniaczek i mieszczek, jak i dostojnych "+
         "c^orek kupc^ow, a nawet i szlachcianek. \n");

    dodaj_przym("wysoki","wysocy");
    dodaj_przym("jasnow^losy","jasnow^losi");

    set_stats (({ 45, 80, 60, 80, 20 }));

    add_prop(CONT_I_WEIGHT, 75000);
    add_prop(CONT_I_HEIGHT, 182);

    set_act_time(30);
    add_act("Tr^aca delikatnie struny lutni, wydobywaj^ac z niej "+
        "prze^sliczne d^xwi^eki. ");
    add_act("U^smiecha si^e do przechodz^acej dziewczyny. ");
    add_act("Intonuje jak^a^s znan^a, skoczn^a piosenk^e ");
    add_act("Wskakuje na podwy^zszenie i zaczyna ^spiewa^c mocnym, "+
        "jasnym g^losem. ");
    add_act("Poprawia r^ekaw barwnego kubraka. ");

    //add_act("Jakie^s dzieci podbiegaj^a do grajka i ta�cz^a wok^o^l niego,
    //trzymaj^ac si^e za r^ece.");
    //add_act("Jaka^s dziewczyna rumieni si^e gwa^ltownie, gdy grajek spogl^ada
    //na ni^a jasnymi oczyma. ");
    /* Jesli kobieta:
     Puszcza do ciebie oko.
     ^spiewa prze^sliczn^a, smutn^a pie^s�, spogl^adaj^ac ci prosto w oczy.
     */


    set_cact_time(10);
    add_cact("krzyknij Na pomoc!");

    add_armour(RINDE_UBRANIA + "kubraki/kolorowy_ekstrawagancki_Mc.c");
    add_armour(RINDE_UBRANIA + "spodnie/czerwone_plocienne_Mc.c");
    add_armour(RINDE_UBRANIA + "kapelusze/czerwony_elegancki_Mc.c");
    add_armour(RINDE_UBRANIA + "buty/wysokie_eleganckie_Mc.c");
    //add_object("item");


/*
     add_ask(({"swiatynie"}), VBFC_ME("pyt_o_swiatynie"));
     add_ask(({"yennefer"}), VBFC_ME("pyt_o_yennefer"));
     add_ask(({"geralta"}), VBFC_ME("pyt_o_geralta"));
     set_default_answer(VBFC_ME("default_answer"));


}

void
powiedz_gdy_jest(object player, string tekst)
        {
           if (environment(this_object()) == environment(player))
           this_object()->command(tekst);
        }

        string
        pyt_o_swiatynie()
        {
           set_alarm(2.0, 0.0, "powiedz_gdy_jest", this_player(),
             "':wskazuj^ac r^ek^a otoczenie: Rozejrzyj si^e troch^e
po
mojej "+
             "^swi^atyni i pochyl g^low^e kiedy stoisz przede mn^a. ");
                return "";
        }

        string
        pyt_o_yennefer()
        {
                set_alarm(2.0, 0.0, "powiedz_gdy_jest", this_player(),
                "':krzywi^ac si^e: Nie wspominaj mi o tej
wied^xmie.");
                return "";


        }
        string
        pyt_o_geralta()
        {
                set_alarm(2.0, 0.0, "powiedz_gdy_jest", this_player(),
                   "powiedz Wied^xmin? Poszed^l gdzie^s w swoj^a droge i lepiej
"+

                        "niech me oczy nie ogladaj^a ju^z tego bia^low^losego.");
                return "";
        }
        string
        default_answer()
        {


                set_alarm(2.0, 0.0, "powiedz_gdy_jest", this_player(),
        "powiedz do "+ this_player()->query_name(PL_BIE) +
        "Niestety nie wiem o co ci chodzi.");
                return "";
        }
 */
}
void
    add_introduced(string imie_mia, string imie_bie)

{
        set_alarm(2.0, 0.0, "return_introduce", find_player(imie_mia));
}
void
    return_introduce(object ob)
{
    command("przedstaw sie " + OB_NAME(ob));

}

void
emote_hook(string emote, object wykonujacy)
{
    switch (emote)
    {
        case "kopnij": set_alarm(1.5, 0.0, "nienajlepszy", wykonujacy);
                                break;
        case "spoliczkuj": set_alarm(1.5, 0.0, "nienajlepszy", wykonujacy);
                    break;
        case "opluj" : set_alarm(2.5, 0.0, "nienajlepszy", wykonujacy);
               break;
        case "poca^luj":
              {
              if(wykonujacy->query_gender() == G_FEMALE)
                switch(random(1))
                 {
                 case 0:command("Przygarnia ci^e lekko jedn^a r^ek^a, a "+
                     "drug^a g^ladzi ci^e po w^losach.  ");
                        break;
                 }
              else
                {
                   switch(random(1))
                   {
                   case 0:command("emote wybucha ^smiechem i mruga do "+
                       "ciebie z rozbawieniem.");
                        break;
                   }
                }

                break;
                }
        case "przytul":set_alarm(2.5, 0.0, "malolepszy", wykonujacy);
                   break;

        case "pog^laszcz": set_alarm(2.5, 0.0, "malolepszy", wykonujacy);
                   break;
    }
}

void
malolepszy(object wykonujacy)
{
     if(wykonujacy->query_gender() == G_FEMALE)
              switch(random(3))
              {
                case 0:command("powiedz Dla pani prze^slicznych oczu "+
                    "u^lo^z^e specjaln^a piosenk^e."); break;
                case 1:command("emote u^smiecha si^e i k^lania z "+
                    "gracj^a. "); break;
                case 2:command("Zna^lem kiedy^s melodie, godn^a pani "+
                    "urody. Gdybym tylko m^og^l j^a sobie "+
                    "przypomnie^c."); break;
              }
      else
              {
                switch(random(1))
                {
                case 0:command("powiedz Bardzo mi mi^lo, ale wola^lbym "+
                    "aby pan tego nie powtarza^l."); break;
                }
              }
}
void
nienajlepszy(object wykonujacy)
{
    if(wykonujacy->query_gender() == G_FEMALE)
        switch(random(2))
    {
      case 0: command("powiedz Pani jest bardzo ^ladna, gdy si^e "+
          "u^smiecha, ale skrzywiona mina pani nie s^lu^zy. ");
              break;
    }
    else
    {
        switch(random(2))
        {
            case 0:command("emote krzywi si^e z pogard^a."); break;
            case 1:command("powiedz Pie^sni mog^a nie tylko chwali^c, "+
                "ale i uwiecznia^c haniebne post^epki. "); break;
        }
    }
}

void
attacked_by(object wrog)
{
    if(walka==0)
    {

    command("krzyknij Na pomoc!");
    command("emote robi zr^eczny ruch r^ek^a i w jednej chwili w jego "+
        "d^loni b^lyska d^lugi sztylet. ");
    command("dobadz broni");

    set_alarm(5.0, 0.0, "czy_walka", 1);
    walka = 1;
    return ::attacked_by(wrog);
    }

    else if(walka == 1)
    {
      set_alarm(1.0, 0.0, "command", "krzyknij Pomocy!");
      walka = 1;

      return ::attacked_by(wrog);
    }

}


int
czy_walka()
{
    if(!query_attack())
    {

       this_object()->command("emote rozgl^ada si^e czujnie.");
       this_object()->command("opusc bron");

       walka = 0;
       return 1;
    }
       else
       {
       set_alarm(5.0, 0.0, "czy_walka", 1);
       }

}