
/* Autor: Avard
   Opis : Faeve i Yoshua
   Data : 14.02.07
   Info : Sekretarz w garnizonie Rinde, sprzedaje
          skonfiskowane bronie*/

#pragma unique

#include <std.h>
#include <ss_types.h>
#include <macros.h>
#include <stdproperties.h>
#include <pl.h>
#include "dir.h"

inherit RINDE_NPC_STD;
inherit "/lib/sklepikarz.c";

int czy_walka();
int walka = 0;

void
create_rinde_humanoid()
{
    ustaw_odmiane_rasy("m�czyzna");
    set_gender(G_MALE);
    ustaw_imie(({"heglert", "heglerta", "heglertowi", "heglerta",
    "heglertem", "heglercie"}), PL_MESKI_OS);
    set_origin("z Rinde");
    set_title(", Sekretarz");
    dodaj_nazwy("sekretarz");
    dodaj_przym("wynios^ly","wynio^sli");
    dodaj_przym("barczysty", "barczy^sci");

    set_long("@@longer@@");

    set_stats ( ({100, 62, 110, 50, 71 }) );

    add_prop(CONT_I_WEIGHT, 91000);
    add_prop(CONT_I_HEIGHT, 187);
    add_prop(SELLER_I_NO_BARGAIN, 1); //Nie uwzgl�dniamy targowania graczy
        //przy sprzedawaniu im przedmiot�w.

    set_act_time(10);
    add_act("emote rozgl^ada sie dooko^la, jakby czego^s szuka^l.");
    add_act("emote mlaska g^lo^sno, daj^ac upust swemu znudzeniu.");
    add_act("emote ziewa z wyra^xnym znudzeniem.");
    add_act("emote przest^epuje z nogi na nog^e.");

    set_chat_time(10);
    add_chat("Ech, stare dobre czasy...");
    add_chat("Cholera, dawno si^e tu nic nie dzia^lo...");

    set_cchat_time(5);
    add_cchat("Gi^n, sukinsynu!");
    add_cchat("Jak ^smiesz podnosi^c r^ek^e na mnie!");
    add_cchat("Srogo za to zap^lacisz!");

    set_skill(SS_DEFENCE, 20 + random(6));
    set_skill(SS_PARRY, 40 + random(8));
    set_skill(SS_WEP_SWORD, 70 + random(11));

    add_armour(RINDE_ZBROJE + "kolczugi/szara_matowa_M.c");
    add_armour(RINDE_UBRANIA + "buty/wysokie_skorzane_M.c");
    add_armour(RINDE_ZBROJE + "spodnie/wzmacniane_skorzane_M.c");
    add_object(RINDE_BRONIE + "miecze/polyskliwy_zdobiony.c");
    add_object(RINDE_POCHWY + "krotka_prosta.c");

    config_default_sklepikarz();
    //set_money_greed_buy(-60);
    //set_money_greed_sell(-60);

    set_money_greed_buy(60);
    set_money_greed_sell(1300);

    set_store_room(GARNIZON_LOKACJE + "magazyn_kwatermistrza.c");

    add_ask(({"garnizon"}), VBFC_ME("pyt_o_garnizon"));
    add_ask(({"prace"}), VBFC_ME("pyt_o_prace"));
    add_ask(({"pomoc"}), VBFC_ME("pyt_o_pomoc"));
    add_ask(({"zadanie"}), VBFC_ME("pyt_o_zadanie"));
    add_ask(({"bronie","bro�","odbi�r","depozyt","odbi�r broni",
    		"bro� z depozytu","skonfiskowanie broni","odbi�r broni z depozytu",
    		"depozyt broni"}), VBFC_ME("pyt_o_depozyt"));
    set_default_answer(VBFC_ME("default_answer"));
}
void
start_me()
{
    command("przypnij pochwe w pasie po lewej stronie");
    command("wloz miecz do pochwy");
}

string
longer()
{
    string str;
    if(!query_attack())
    {
        str="Ten barczysty m�czyzna wygl�da na prawdziwego wojskowego "+
        "weterana. �wiadczy� o tym mog� spr�yste ruchy i doskona�a "+
        "dyscyplina, z jak� poddaje si� wszelkim rozkazom dow�dc�w. Mimo "+
        "i� jest jeszcze do�� m�ody - walczy� ju� nie mo�e - utyka na "+
        "jedn� nog� oraz podczas jednej z bitew pozbawiono go oka - "+
        "dlatego te� zosta� sekretarzem. Od czasu do czasu, jego "+
        "jedyne oko ogarnia pomieszczenie smutnym i t�sknym spojrzeniem.\n";
    }
    else
    {
        str="Ten barczysty m�czyzna wygl�da na prawdziwego wojskowego "+
        "weterana. �wiadczy� o tym mog� spr�yste ruchy i doskona�a "+
        "dyscyplina, z jak� poddaje si� wszelkim rozkazom dow�dc�w. Mimo "+
        "i� jest jeszcze do�� m�ody - walczy� ju� nie mo�e - utyka na "+
        "jedn� nog� oraz podczas jednej z bitew pozbawiono go oka - "+
        "dlatego te� zosta� sekretarzem. \n";
    }
    return str;
}
void
powiedz_gdy_jest(object player, string tekst)
{
    if (environment(this_object()) == environment(player))
    this_object()->command(tekst);
}

string
default_answer()
{
    if(!query_attack())
    {
        if (TP->query_prop("pytal_heglerta_default"))
        {
            set_alarm(1.0, 0.0, "komenda_gdy_jest", this_player(), command
            ("powiedz do "+ OB_NAME(this_player())+" Zamknij ju^z ryj!\n"));
            return "";
        }

        TP->add_prop("pytal_heglerta_default", 1);

        set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
        "powiedz do " + OB_NAME(this_player()) +
        " Cicho b^ad^x, jestem na s^lu^zbie!");
        return "";
    }
    else
    {
        set_alarm(0.5,0.0,"powiedz_gdy_jest",TP,"powiedz do "+OB_NAME(TP)+
        "Chyba kpisz!");
        return "";
    }
}

string
pyt_o_garnizon()
{
    if(!query_attack())
    {
        if (TP->query_prop("pytal_heglerta_garnizon"))
        {
            set_alarm(1.0, 0.0, "komenda_gdy_jest", this_player(), command
            ("powiedz do "+ OB_NAME(this_player())+" Zamknij ju^z ryj!\n"));
            return "";
        }

        TP->add_prop("pytal_heglerta_garnizon", 1);
        set_alarm(0.5, 0.0, "komenda_gdy_jest", this_player(),
        "'Tak! W^la^snie si^e w nim znajdujesz!");
        return "";
    }
    else
    {
        set_alarm(0.5,0.0,"powiedz_gdy_jest",TP,"powiedz do "+OB_NAME(TP)+
        "Chyba kpisz!");
        return "";
    }
}

string
pyt_o_prace()
{
    if(!query_attack())
    {
        if (TP->query_prop("pytal_heglerta_praca"))
        {
            set_alarm(1.0, 0.0, "komenda_gdy_jest", this_player(), command
            ("powiedz do "+ OB_NAME(this_player())+" Zamknij ju^z ryj!\n"));
            return "";
        }

        TP->add_prop("pytal_heglerta_praca", 1);

        set_alarm(0.5, 0.0, "komenda_gdy_jest", this_player(),
        command("powiedz do " + OB_NAME(this_player()) +
        " Szukasz pracy? Id� i przynie� mi co� do jedzenia, ale nie oczekuj "+
        "zap^laty."));
        set_alarm(0.5, 0.0, "komenda_gdy_jest", this_player(),
        command("popatrz z rozbawieniem na " + OB_NAME(this_player())));
        return "";
    }
    else
    {
        set_alarm(0.5,0.0,"powiedz_gdy_jest",TP,"powiedz do "+OB_NAME(TP)+
        "Chyba kpisz!");
        return "";
    }
}

string
pyt_o_pomoc()
{
    if(!query_attack())
    {
        if (TP->query_prop("pytal_heglerta_pomoc"))
        {
            set_alarm(1.0, 0.0, "komenda_gdy_jest", this_player(), command
            ("powiedz do "+ OB_NAME(this_player())+" Zamknij ju^z ryj!\n"));
            return "";
        }

        TP->add_prop("pytal_heglerta_pomoc", 1);

        set_alarm(0.5, 0.0, "komenda_gdy_jest", this_player(),
        command("powiedz do " + OB_NAME(this_player()) +
        " Niee... Ja nie potrzebuj^e pomocy, czuj^e si^e ^swietnie."));
        set_alarm(0.5, 0.0, "komenda_gdy_jest", this_player(),
        command("usmiechnij sie chytrze do " + OB_NAME(this_player())));
        return "";
    }
    else
    {
        set_alarm(0.5,0.0,"powiedz_gdy_jest",TP,"powiedz do "+OB_NAME(TP)+
        "Chyba kpisz!");
        return "";
    }
}

string
pyt_o_zadanie()
{
    if(!query_attack())
    {
        if (TP->query_prop("pytal_heglerta_zadanie"))
        {
            set_alarm(1.0, 0.0, "komenda_gdy_jest", this_player(),
            command("powiedz do " + OB_NAME(this_player()) +
            " Zamknij ju^z ryj!"));
            return "";
        }

        TP->add_prop("pytal_heglerta_zadanie", 1);

        set_alarm(0.5, 0.0, "komenda_gdy_jest", this_player(),
        command("powiedz do " + OB_NAME(this_player()) +
        " Tak! Mam dla ciebie zadanie! Wyno^s si^e st^ad!"));
        set_alarm(0.5, 0.0, "komenda_gdy_jest", this_player(),
        command("emote wraca do pracy."));
        return "";
    }
    else
    {
        set_alarm(0.5,0.0,"powiedz_gdy_jest",TP,"powiedz do "+OB_NAME(TP)+
        "Chyba kpisz!");
        return "";
    }
}

string
pyt_o_depozyt()
{
	set_alarm(0.5,0.0,"powiedz_gdy_jest",TP,command("powiedz do "+OB_NAME(TP) +
        " O, czy�by moje ch�opaki zabrali ci bro�? He, he. Owszem, gdzie� "+
        "tu powinna by�. Przejrzyj to, co mam, mo�e j� znajdziesz."));
	return "";
}


void
add_introduced(string imie_mia, string imie_bie)
{
        set_alarm(2.0, 0.0, "return_introduce", find_player(imie_mia));
}
void
return_introduce(object ob)
{
    command("przedstaw sie " + OB_NAME(ob));
}

void
emote_hook(string emote, object wykonujacy)
{
    switch (emote)
    {
        case "kopnij": set_alarm(1.5, 0.0, "nienajlepszy", wykonujacy);
            break;
        case "spoliczkuj": set_alarm(1.5, 0.0, "nienajlepszy", wykonujacy);
            break;
        case "opluj" : set_alarm(2.5, 0.0, "nienajlepszy", wykonujacy);
            break;
        case "poca^luj":
        {
            if(wykonujacy->query_gender() == 1)
            switch(random(2))
            {
                case 0:command("szturchnij "+OB_NAME(wykonujacy));
                       command("mrugnij");break;
                case 1:command("zarechocz rubasznie");break;
            }
            else
            {
                switch(random(3))
                {
                    case 0:command("warknij");
                           command("Wyno� si�!");
                    break;
                    case 1:command("spojrzyj niepewnie na "+
                         OB_NAME(wykonujacy));
                    break;
                    case 2:command("'To wcale nie by^lo "+
                        "przyjemne.");
                    break;
                }
            }
            break;
        }
        case "przytul": set_alarm(2.5, 0.0, "malolepszy", wykonujacy);
                   break;
        case "pog^laszcz": set_alarm(2.5, 0.0, "malolepszy", wykonujacy);
                   break;
    }
}

void
malolepszy(object wykonujacy)
{

   switch(random(4))
   {
     case 0: command("usmiechnij sie niepewnie"); break;
     case 1: command("usmiechnij sie nieznacznie"); break;
     case 2: command("hmm"); break;
     case 3: command("namysl sie ."); break;

   }
}

void
nienajlepszy(object kto)
{
   switch(random(3))
   {
      case 0: command("powiedz Zr^ob to jeszcze raz, a wezwe stra^z.");
              break;
      case 1: set_alarm(0.5, 0.0, "powiedz_gdy_jest", kto,
              "prychnij");
              break;
      case 2: command("powiedz Wyjd^z st^ad.");
              break;
   }
}

void attacked_by(object wrog)
{
    if(walka == 0)
    {
        walka = 1;
        command("dobadz miecza");
        set_alarm(15.0, 0.0, "czy_walka", 1);
        return ::attacked_by(wrog);
    }
    else if(walka == 1)
    {
        return ::attacked_by(wrog);
    }
}
int
czy_walka()
{
    if(!query_attack())
    {
        command("wloz miecz do pochwy");
        command("splun");
        walka = 0;
        return 1;
    }
    else
    {
        set_alarm(15.0, 0.0, "czy_walka", 1);
    }
}

public void
do_die(object killer)
{
    command("powiedz Nie ujdzie ci to na sucho.");
    ::do_die(killer);
}

void
init()
{
    ::init();
    init_sklepikarz();
}
void
shop_hook_niczego_nie_skupujemy()
{
    command("powiedz Nie potrzebuj� tego, ja tu jedynie bro� wydaj�.");
    notify_fail("");
}

void
shop_hook_list_empty_store(string str)
{
    command("powiedz Aktualnie nie mam niczego w depozycie do odebrania.");
}

//Redefiniujemy buy_it, je�li jaki� przedmiot ma w�a�ciciela to tylko w�a�ciciel mo�e go odkupi�.
object *
buy_it(object *ob, string str2, string str3)
{
    for(int i = 0; i < sizeof(ob); i++)
    {
        if(sizeof(ob[i]->query_owners()) &&
            member_array(TP->query_real_name(), ob[i]->query_owners()) == -1)
        {
                ob = exclude_array(ob, i, i);
        }
    }
    if(!sizeof(ob))
    {
        command("powiedz do " + OB_NAME(TP) + " Nie mog� ci tego sprzeda�, "+
            "to nale�a�o do kogo� innego.");
        notify_fail("");
        return 0;
    }

    return ::buy_it(ob, str2, str3);
}


