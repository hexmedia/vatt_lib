/**************
 * Koles z ktorym mozna zagrac w trzy kubki
 * Autor: Avard i Veila
 * Opis : Veila
 * Data : 29.12.2010
 *************/

#pragma unique

#include <std.h>
#include <ss_types.h>
#include <macros.h>
#include <stdproperties.h>
#include <pl.h>
#include "/d/Standard/Redania/Rinde/npc/dir.h"
#include <money.h>

inherit RINDE_NPC_STD;

int czy_walka();
int walka = 0;

int powitanie_gracza(string str);
int wybor_kubka(string str);

void
create_rinde_humanoid()
{
    ustaw_odmiane_rasy(PL_MEZCZYZNA);
    set_living_name("edwin");
    set_gender(G_MALE);
    ustaw_imie(({"edwin","edwinaa","edwinowi","edwina","edwinem",
        "edwinie"}),PL_MESKI_OS);
    //set_title(", ");

    set_long("@@dluuugi@@");

    dodaj_przym("zmarzni�ty","zmarzni�ci");
    dodaj_przym("niewysoki","niewysocy");

    set_stats (({ 65, 35, 50, 60, 65 }));

    add_prop(CONT_I_WEIGHT, 130000);
    add_prop(CONT_I_HEIGHT, 175);

    set_act_time(20);
    add_act("emote potrz^asa kubeczkami a co^s znajduj^acego si^e w ^srodku obija si^e o ^scianki wydaj^ac przy tym g^luchy odg^los.");
    add_act("emote przek^lada kubeczki z jednej r^eki do drugiej.");
    add_act("emote stawia kubeczki na stoliku, unosi r^ece do twarzy po czym przez par^e chwil na przemian chucha w d^lonie i pociera nimi o siebie.");
    add_act("emote poprawia spadaj^ac^a mu na oczy, obszern^a czapk^e. ");
    add_act("emote szczelniej opatula si^e szalikami.");
    add_act("emote drepcze w miejscu tupi^ac przy tym g^lo^sno.");


    set_chat_time(20);
    add_chat("Co za zima, zamarzn^e tutaj przecie^z.");
    add_chat("Mo^ze chce kto^s zagra^c? Nudno, zimno, potrzeba troch^e rozrywki!");    
    add_chat("Wype^lni^le^s dobry uczynek na dzisiaj? Jak nie, to zagraj i przegraj, troch^e groszy zarobi^e przynajmniej...");
    add_chat("Nie zazi^ebi^c si^e w tak^a zimnic^e to wi^ecej ni^z bym oczekiwa^l.");
    add_chat("Zapraszam, zapraszam do gry, szybko, szybko. P^oki jeszcze krew mi nie zamarz^la w ^zy^lach.");


    set_cchat_time(10);
    add_cchat("Stra^z! Stra^z! Co to ma by�?!");
    //a nie wiem co tu

    add_armour(RINDE_UBRANIA + "kaftany/czarny_wyszywany_M.c");
    add_armour(RINDE_UBRANIA + "buty/wysokie_skorzane_M.c");
    add_armour(RINDE_UBRANIA + "spodnie_los_los_biedaka.c");

    add_ask(({"gr^e", "kubeczki", "kubeczek", "granie"}), VBFC_ME("pyt_o_gre"));
    set_default_answer(VBFC_ME("default_answer"));

    set_alarm_every_hour("co_godzine");
}

void
start_me()
{
    set_alarm(1.0, 0.0, "command", "usiadz przy stole");
}

void
powiedz_gdy_jest(object player, string tekst)
{
    if (environment(this_object()) == environment(player))
        this_object()->command(tekst);
}

string
dluuugi()
{
    string str;
    str ="^Sredniego wzrostu m^e^zczyzna opatulony jest po czubek "+
    "nosa w kolorowe chusty i szale, a spod wielkiej, czerwonawej czapki "+
    "kt^or^a przywdzia^l na g^low^e wymykaj^a si^e kosmyki ciemnobr^azowych, "+
    "kr^econych w^los^ow. W jego r^ownie ciemnych oczach rozpala si^e "+
    "przebieg^ly b^lysk gdy tylko jego wzrok spocznie na przechodz^acym "+
    "m^e^zczy^xnie czy kobiecie. Ciemny, szarobury p^laszcz w kt^ory "+
    "osobnik ten zawini^ety jest od szyi po kolana, zdaje si^e by� bardzo "+
    "gruby, podobnie jak wystaj^ace spod niego nogawki we^lnianych spodni "+
    "o r^ownie nieokre^slonym kolorze. Dziwnym dope^lnieniem ubioru s^a "+
    "ekstrawaganckie, wykonane z b^lyszcz^acej, jasno br^azowej sk^orki "+
    "buty ze z^lotymi klamrami, r^o^zni^ace si^e zar^owno stanem zu^zycia "+
    "jak i jako^sci^a od ka^zdej innej sztuki odzie^zy i budz^ace "+
    "w^atpliwo^sci co do ^xr^od^la z kt^orego w^la^sciciel je pozyska^l. ";

    /*if(nie ma teraz akurat gry)
     {
    str += "W zawini^etych w pasy materia^lu dla ochorony przed zimnem "+
    "d^loniach m^e^zczyzna trzyma w^lo^zone jednen w drugi kubeczki. "
     } 
    */
    str+="\n";
    return str;
}


string
pyt_o_gre()
{
    if(!query_attack())
    {
        set_alarm(0.0, 0.0, "powiedz_gdy_jest", this_player(),
        "wzrusz ramionami");
        set_alarm(1.0, 0.0, "powiedz_gdy_jest", this_player(),
        "'Najprostsza gra na ^swiecie. Najpierw postaw pieni^adze, "+
        "stawka to jeden denar.");
        set_alarm(3.5, 0.0, "powiedz_gdy_jest", this_player(),
        "emote przerywa i rozciera zamarzn^ete d^lonie.");
        set_alarm(5.5, 0.0, "powiedz_gdy_jest", this_player(),
        "emote stawia kubeczki na stoliku przed sob^a.");
        set_alarm(7.5, 0.0, "powiedz_gdy_jest", this_player(),
        "'To s^a trzy kubeczki. Pod jednym z nich znajdziesz ten kamyk.");
        set_alarm(9.5, 0.0, "powiedz_gdy_jest", this_player(),
        "emote podnosi ^srodkowy kubeczek ods^laniaj^ac szary okr^ag^ly kamie^n.");
        set_alarm(12.0, 0.0, "powiedz_gdy_jest", this_player(),
        "'Ja zamieniam je miejscami, ty je^sli zgadniesz pod kt^orym go "+
        "schowa^lem - wygrywasz.");
        set_alarm(15.0, 0.0, "powiedz_gdy_jest", this_player(),
        "chrz^aknij .");
        set_alarm(17.0, 0.0, "powiedz_gdy_jest", this_player(),
        "'Je^sli wygrasz daj^e ci dwa razy tyle ile postawi^le^s, "+
        "je^sli przegrasz pieni^adze zostaj^a u mnie.");
        set_alarm(19.5, 0.0, "powiedz_gdy_jest", this_player(),
        "'To co, chcesz zagra^c w trzy kubki?");


    }
    else
    {
        set_alarm(1.0,0.0,"powiedz_gdy_jest", this_player(),
            "powiedz Chyba nie ma nastroju na rozmowy tutaj...");
    }
    return "";

}


string
default_answer()
{
    if(!query_attack())
    {
    switch(random(2))
    {
    case 0:set_alarm(2.0, 0.0, "powiedz_gdy_jest", this_player(),
        "'Nie wiem o co ci chodzi, ale mo^ze chcesz zagra^c ze mn^a?"); break;
    case 1:set_alarm(2.0, 0.0, "powiedz_gdy_jest", this_player(),
        "'Tak, te^z s^adz^e, ^ze takie temperatury powinny by^c zakazane!"); break;
    }
    }
    else
    {
        set_alarm(1.0,0.0,"powiedz_gdy_jest", this_player(),
            "powiedz To chyba nie s^a warunki do rozmowy!");
    }
    return "";
}

void
add_introduced(string imie_mia, string imie_bie)
{
    set_alarm(2.0, 0.0, "return_introduce", find_player(imie_mia));
}
void
return_introduce(object ob)
{
    command("przedstaw sie " + OB_NAME(ob));

}

void
emote_hook(string emote, object wykonujacy)
{
    switch (emote)
    {
        case "kopnij": set_alarm(1.5, 0.0, "nienajlepszy", wykonujacy);
                    break;
        case "spoliczkuj": set_alarm(1.5, 0.0, "nienajlepszy", wykonujacy);
                    break;
        case "opluj" : set_alarm(2.5, 0.0, "nienajlepszy", wykonujacy);
                    break;
        case "poca^luj":
              {
              if(wykonujacy->query_gender() == 1)
                  {
              switch(random(2))
                        {
                 case 0:command("'Nie wiem o co chodzi, i tak jest za zimno bym cokolwiek poczu^l. "); break;
                 case 1:command("popatrz uwa^znie na "+OB_NAME(wykonujacy));
                        break;
              
                        }
                  }
                     else
                  {
                   switch(random(3))
                        {
                   case 0:command("'To jaki^s g^lupi ^zart?"); break;
                   case 1:command("'^ze ci si^e chce ^zartowa� w takie zimno.");
                            break;
                   case 2:command("spojrzyj dziwnie na "+
                        OB_NAME(wykonujacy));
                            break;
                        }
                  }


               }
               break;
        case "przytul": set_alarm(2.5, 0.0, "malolepszy", wykonujacy);
                   break;
        case "pog^laszcz": set_alarm(2.5, 0.0, "malolepszy", wykonujacy);
                   break;
    }
}

void
malolepszy(object wykonujacy)
{
    if(wykonujacy->query_gender() == 1)
           switch(random(2))
           {
            case 0: set_alarm(0.5, 0.0, "powiedz_gdy_jest", wykonujacy,
                "No ju^z, ju^z . "); break;
            case 1: set_alarm(0.5, 0.0, "powiedz_gdy_jest", wykonujacy,
                "'Mo^ze jednak zagramy, zamiast traci^c czas?"); break;
           }
           else
            {
               switch(random(2))
                {
                   case 0: set_alarm(0.5, 0.0, "powiedz_gdy_jest", wykonujacy,
                    "'Wszystko z tob^a w porz^adku? "); break;
                   case 1: set_alarm(0.5, 0.0, "powiedz_gdy_jest", wykonujacy,
                       "westchnij cie^zko"); break;
                }
            }

}

void
nienajlepszy(object kto)
{
   switch(random(2))
   {
      case 0: set_alarm(0.5, 0.0, "powiedz_gdy_jest", kto,
          "'Zaraz stra^z zawo^lam. ");
              break;
      case 1: set_alarm(0.5, 0.0, "powiedz_gdy_jest", kto,
              "'Zostawi^lby^s w spokoju zamarzaj^acego. ");
              break;

   }
}

void attacked_by(object wrog)
{
    if(walka == 0)
    {
        walka = 1;
        set_alarm(0.5,0.0,"command","krzyknij Zaraz si^e posypi^a czyje^s z^eby!");
        set_alarm(15.0, 0.0, "czy_walka", 1);
        return ::attacked_by(wrog);
    }
    else
    {
        return ::attacked_by(wrog);
    }
}
int
czy_walka()
{
    if(!query_attack())
    {
        set_alarm(0.5,0.0,"command","powiedz Niech tak b^edzie.");
        walka = 0;
        return 1;
    }
    else
    {
        set_alarm(15.0, 0.0, "czy_walka", 1);
    }
}

nomask int
query_reakcja_na_walke()
{
    return 1;
}

// GRA TRZY KUBKI

void
init()
{
     ::init();
     add_action(powitanie_gracza, "zagraj");
     add_action(powitanie_gracza, "graj");
     
     add_action(wybor_kubka, "wybierz");
}

object Gracz;
int juzGramy = 0;
int moznaWybierac = 0;

//pomocnicza funkcja
void
ustaw_mozliwosc_wybierania(int i)
{
    moznaWybierac = i;
}

void
ustaw_czy_gramy(int i)
{
    juzGramy = i;
}

int
powitanie_gracza(string str)
{
    if(query_verb() ~= "zagraj")
        notify_fail("Zagraj w co?\n");
    else
        notify_fail("Graj w co?\n");
        
    if (!str) return 0;
    if (!strlen(str)) return 0;
    
    if(!parse_command(str,environment(TP),"'w' 'trzy' 'kubki'")) return 0;
    
    if(juzGramy == 0)
    {
        if (MONEY_ADD(this_player(),-20))
        {
            ustaw_czy_gramy(1);
            TP->catch_msg("Stawiasz denara i zaczynasz gr^e w trzy kubki.\n");
            saybb(QCIMIE(TP,PL_MIA)+" stawia denara i zaczyna gr^e w trzy kubki.\n");
            Gracz = TP;
            set_alarm(1.0,0.0,"command","powiedz do "+OB_NAME(Gracz)+" Zaczynamy gr�!");
            set_alarm(3.0,0.0,"command","emote kreci kubkami.");
            set_alarm(4.0,0.0,"command","powiedz do "+OB_NAME(Gracz)+" Wybierz kubek: lewy, ^srodkowy lub prawy");
            set_alarm(4.1,0.0,"ustaw_mozliwosc_wybierania", 1);
            return 1;
        }
        else
        {
            TP->catch_msg("Nie masz denara kt�rego "+koncowka("m�g�by�","mog�aby�")+" postawi� w grze.\n");
            return 1;
        }
    }
    else
    {
        if(TP == Gracz) 
        {    
           set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
            "'Ale przecie^z ju� gramy..?"); 
           return 1;//TUTAJ JEZELI GRACZ GRA I CHCE ZACZAC GRE
        }            
        else 
        {
            set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
            "'Poczekaj chwil^e, zaraz zagram z tob^a.");
            return 1;//TUTAJ ODPOWIEDZ JAK KTOS CHCE ZACZAC GRAC GDY GRA KTOS INNY
        }    
    }
}



int
wybor_kubka(string str)
{
    int wybranyKubek;
    if(moznaWybierac == 0)
    {
        TP->catch_msg("Za wcze�nie na wybieranie, kubki wiruj�!\n");
        return 1;
    }
    if(juzGramy == 0)
    {
        set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
        "'Podobaj^a ci si^e moje kubeczki? To zagrajmy porz^adnie, zapraszam, a nie...");
        return 1;
    }
    if(TP != Gracz)
    {
        set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
        "'Poczekaj chwil^e, zaraz zagram z tob^a.");
        return 1;
    }
    
    notify_fail("Wybierz co?\n");
    
    if (!str) return 0;
    if (!strlen(str)) return 0;
    
    if(!parse_command(str,environment(TP),"'lewy' / 'prawy' / '^srodkowy' / 'srodkowy' [kubek] ")) return 0;
    
    if(parse_command(str,environment(TP),"'lewy' [kubek]"))
    {
        Gracz->catch_msg("Wybierasz lewy kubek.\n");
        saybb(QCIMIE(Gracz, PL_MIA)+" wybiera lewy kubek.\n");
        wybranyKubek = 0;
    }
        
    if(parse_command(str,environment(TP),"'prawy' [kubek]"))
    {
        Gracz->catch_msg("Wybierasz prawy kubek.\n");
        saybb(QCIMIE(Gracz, PL_MIA)+" wybiera prawy kubek.\n");
        wybranyKubek = 1;
    }
        
    if(parse_command(str,environment(TP),"'srodkowy' / '^srodkowy' [kubek]"))
    {
        Gracz->catch_msg("Wybierasz �rodkowy kubek.\n");
        saybb(QCIMIE(Gracz, PL_MIA)+" wybiera �rodkowy kubek.\n");
        wybranyKubek = 2;
    }
	ustaw_mozliwosc_wybierania(0);
    
    int wylosowanyKubek = random(2);
    if(wybranyKubek == wylosowanyKubek)
    {
        set_alarm(1.0,0.0,"command","emote k^ladzie r^ek^e na wybranym kubeczku, wzdycha ci�ko i podnosi go powoli...");
        set_alarm(4.0,0.0,"command","emote spogl^ada z zaskoczeniem na ods^loni^ety kamyczek.");
        set_alarm(6.0,0.0,"command","skrzyw sie z niesmakiem");
        set_alarm(8.0,0.0,"command","'nie do^s^c, ^ze marzn^e, to jeszcze resztki grosza mi zabior^a.");
        set_alarm(10.5,0.0,"command","pokrec glowa z niedowierzaniem");
        set_alarm(13.0,0.0,"command","'wygl^ada na to, ^ze wygra^le^s!");
        MONEY_ADD(TO,40);
        set_alarm(13.5,0.0,"command","daj dwa denary "+OB_NAME(Gracz));
        set_alarm(13.6,0.0,"ustaw_czy_gramy", 0);
        
    }
    else
    {
        set_alarm(1.0,0.0,"command","emote k^ladzie r^ek^e na wybranym kubeczku, wzdycha ci^e^zko i podnosi go powoli... Nic pod nim nie ma!");
		set_alarm(4.0,0.0,"command","usmiechnij sie triumfalnie");
		set_alarm(6.0,0.0,"command","'No c^o^z, tym razem moja wygrana!");
		set_alarm(8.0,0.0,"command","zatrzyj rece");
		set_alarm(8.1,0.0,"ustaw_czy_gramy", 0);
		set_alarm(8.1,0.0,"ustaw_mozliwosc_wybierania", 0); 
    }
    return 1;    
}