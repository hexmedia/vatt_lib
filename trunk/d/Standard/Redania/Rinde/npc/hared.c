#pragma unique

#include <cmdparse.h>
#include <macros.h>
#include <money.h>
#include <pl.h>
#include <ss_types.h>
#include <stdproperties.h>
#include <mudtime.h>
#include "dir.h"

inherit RINDE_NPC_STD;
inherit "/lib/sklepikarz.c";

#define SPODNIE RINDE+"przedmioty/ubrania/spodnie/czarne_skorzane_Mc.c"
#define PLASZCZ RINDE+"przedmioty/ubrania/plaszcze/czarny_elegancki_M.c"
#define KAFTAN  RINDE+"przedmioty/ubrania/kaftany/czarny_wyszywany_M.c"
#define BUTY RINDE+"przedmioty/ubrania/buty/dlugie_czarne_M.c"
#define SZTYLET RINDE+"przedmioty/bronie/sztylety/krotki_posrebrzany.c"

int sprawdz_straz();

void
create_rinde_humanoid()
{
	set_living_name("hared");
	set_surname("Lanorn");
	ustaw_odmiane_rasy("m�czyzna");
	dodaj_nazwy("handlarz");
	ustaw_imie( ({"hared", "hareda", "haredowi", "hareda", "haredem", "haredzie"}), PL_MESKI_OS);
	set_gender(G_MALE);

	dodaj_przym("czujny", "czujni");
	dodaj_przym("niepozorny", "niepozorni");

	set_long("Rozbiegane oczka m�czyzny �ypi� raz w jedn�, raz w drug�"
	+" stron�, jak gdyby stara�y si� wypatrze� kogo� w ciemno�ciach"
	+" uliczki. Smuk�a sylwetka, czarne odzienie i ostro�ny, mi�kki"
	+" krok cz�owieka pozwalaj� s�dzi�, �e wola�by on pozosta�"
	+" w ukryciu, ujawniaj�c si� jedynie tym, z kt�rymi m�g�by ubi� jaki�"
	+" interes. Masz wra�enie, �e od czasu do czasu w jego d�oni"
	+" b�yska ostrze sztyletu i nie by�oby w tym nic dziwnego, gdy� po"
	+" zmroku kr�ci si� tutaj wiele podejrzanych typk�w.\n");

	add_armour(SPODNIE);
	add_armour(PLASZCZ);
	add_armour(KAFTAN);
	add_armour(BUTY);

	add_prop(CONT_I_WEIGHT, 68000);
	add_prop(CONT_I_HEIGHT, 168);

	set_stats( ({60, 70, 55, 70, 55}) );

	set_act_time(40);
	add_act("emote nagle wbija wzrok w g��b uliczki.");
	add_act("rozejrzyj sie podejrzliwie");
	add_act("':pod nosem: Cholerni celnicy...");
	add_act("emote przystaje na chwil� i wyra�nie czego� nas�uchuje.");
	add_act("splun");
	add_act("':cicho: Takich towar�w nigdzie indziej nie u�wiadczysz...");
	add_act("emote zamienia kilka s�ow z jakim� podejrzanym typem, po czym"
		+" wr�cza mu dziwny pakunek.");
	add_act("zmruz oczy");


	set_cact_time(10);
	add_cact("emote syczy: Po�a�ujesz tego!");
	add_cact("emote zaciska zawzi�cie wargi.");

	MONEY_MAKE_K(1)->move(this_object());
	MONEY_MAKE_D(15)->move(this_object());

	add_ask(({"towar", "towary"}), "@@pyt_towar@@");
	add_ask(({"handel", "interes", "interesy", "sprzeda�"}), "@@pyt_handel@@");
	add_ask(({"c�o", "przemyt"}), "@@pyt_clo@@");
	add_ask(({"celnik�w", "stra�"}), "@@pyt_celnikow@@");
	add_ask(({"rinde"}), "@@pyt_rinde@@");
	set_default_answer("@@default_answer@@");

	config_default_sklepikarz();
	set_money_greed_sell(106);
	set_store_room(POLUDNIE_LOKACJE+"magazyn_przemytnika.c");

    add_object(SZTYLET, 2);

	set_alarm_every_hour("czy_juz_isc");
}

int czy_juz_isc()
{
	if(MT_GODZINA >= 7 && MT_GODZINA < 21)
	{
		command("powiedz No...na dzi� starczy.");
		tell_roombb(environment(), QCIMIE(this_object(), PL_MIA)
		+" oddala si� szybkim krokiem i za chwile znika gdzie� mi�dzy"
		+" kamienicami.\n");
		remove_object();
	}
}

void powiedz_gdy_jest(object player, string tekst)
{
    if (environment(this_object()) == environment(player))
        this_object()->command(tekst);
}

void pokaz_gdy_jest(object player)
{
	if(environment(this_object()) == environment(player))
		this_object()->try_list();
}

int sprawdz_straz()
{
	/* WAZNY KOMENTARZ!!! :P
	 *   w przysz�o�ci mo�na da� tu te� sprawdzanie,
	 *   czy gracz nie nale�y do jakiej� organizacji typu
	 *   straz/urzednik
	 */
	if(present("stra�nik", environment(this_object())) != 0)
	{
		set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
			"powiedz do "+OB_NAME(this_player())+" Mo�e"
			+" przyjd� za jaki� czas...");
		set_alarm(0.8, 0.0, "powiedz_gdy_jest", this_player(),
			"spojrzyj dyskretnie na straznika");
		return 1;
	}
	return 0;
}

string pyt_towar()
{
	if(sprawdz_straz()) return "";
	set_alarm(0.3, 0.0, "powiedz_gdy_jest", this_player(),
        "powiedz do "+OB_NAME(this_player())+" Naturalnie...");
	set_alarm(0.7, 0.0, &saybb(QCIMIE(this_object(), PL_MIA)+" pokazuje "+
		QIMIE(this_player(), PL_CEL)+" jak�� liste.\n"));
	set_alarm(0.7, 0.0, "pokaz_gdy_jest", this_player());

	return "";
}

string pyt_handel()
{
	if(sprawdz_straz()) return "";
	set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
		"rozejrzyj sie szybko");
	set_alarm(1.0, 0.0, "powiedz_gdy_jest", this_player(),
		"powiedz :do "+OB_NAME(this_player())+" powoli:"+
		" Tak...mam kilka rzeczy do zaoferowania, je�li chcesz mog� pokaza�"+
		" ci sw�j towar.");
	set_alarm(3.7, 0.0, "powiedz_gdy_jest", this_player(),
		" szepnij "+OB_NAME(this_player())+
		" Ale tylko spr�buj pisn�� co� kr�lewskim, to ostro po�a�ujesz.");
	set_alarm(4.3, 0.0, "powiedz_gdy_jest", this_player(),
		"skrzyw sie chlodno");
	return "";
}

string pyt_clo()
{
	if(sprawdz_straz()) return "";
	set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
		"powiedz Ech, kr�lewscy - psie syny - zdzieraj� jak si� tylko da,"
		+" a cz�ek przecie� z czego� musi �y�.");
	set_alarm(1.0, 0.0, "powiedz_gdy_jest", this_player(),
		"usmiech krzywo");
	return "";
}

string pyt_celnikow()
{
	if(sprawdz_straz()) return "";
	set_alarm(0.6, 0.0, "powiedz_gdy_jest", this_player(),
		"powiedz Szlag by ich wszystkich strzeli�, wpycha to to wsz�dzie"
		+" te swoje t�uste �apska i nic cz�owiekowi zarobi� nie da. A z tymi"
		+" c�ami nowymi to ju� ca�kiem poszaleli!");
	return "";
}

string pyt_rinde()
{
	if(sprawdz_straz()) return "";
	set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
		"wzrusz ramionami");
	set_alarm(1.0, 0.0, "powiedz_gdy_jest", this_player(),
		"powiedz :mrukliwie: Miasto, jak miasto. Wa�ne, �eby towar"
		+" dobrze schodzi�.");
}

string default_answer()
{
	if(sprawdz_straz()) return "";
	set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
		"spojrzyj wnikliwie na "+OB_NAME(this_player()));
	set_alarm(1.0, 0.0, "powiedz_gdy_jest", this_player(),
		"powiedz do "+OB_NAME(this_player())+
		" Je�li przys�ali cie tu urz�dowi, to mo�esz im przekaza�, �e"
		+" g�wno na mnie maj�.");
	set_alarm(1.8, 0.0, "powiedz_gdy_jest", this_player(),
		"usmiech z przekasem");
}

void add_introduced(string imie_mia, string imie_bie)
{
	set_alarm(2.0, 0.0, "return_introduce", find_player(imie_mia));
}

void return_introduce(object ob)
{
	command("przedstaw sie "+ OB_NAME(ob));
}

void attacked_by(object ob)
{
	if(sizeof(subinventory("wielded")) < 2)
	{
	command("emote szybkim ruchem dobywa wyci�gni�tych niewiadomo sk�d sztylet�w.");
	add_weapon(SZTYLET);
	add_weapon(SZTYLET);
	}
}

void init()
{
	::init();
	add_action(try_buy,   "kup");
	add_action(help_buy,   "?kup");
	//add_action(try_sell,  "sprzedaj");
	//add_action(try_value, "wyce�");
	add_action(try_show,  "poka�");
	add_action(do_store, "store");
}

int do_show(string str)
{
    object *item_arr;
    int i, *arr;

    if (obiekty_sprzedawane == 0)
    {
        shop_hook_niczego_nie_sprzedajemy();
        return 0;
    }

   if (query_magazyn_jest_pusty())
    {
        shop_hook_list_empty_store(str);
        return 0;
    }

    if (!store_room->is_room() && store_room != this_object() && environment(store_room) != this_object())
	    return 0;

    if (stringp(store_room))
	    item_arr = all_inventory(find_object(store_room));
    else
	    item_arr = all_inventory(store_room);

    item_arr = FIND_STR_IN_ARR(str, item_arr, PL_BIE);

    if (sizeof(item_arr) < 1)
    {
        shop_hook_buy_no_match(str);
        return 0;
    }

    //shop_hook_appraise_object(item_arr[0]);

    this_player()->catch_msg(QCIMIE(this_object(), PL_MIA)+" znika w ciemnym zau�ku, by za"
		+" chwile wr�ci� i dyskretnie pokaza� ci wybrany towar.\n\n");
    saybb(QCIMIE(this_object(), PL_MIA)+" na kr�tk� chwil� znika w ciemnym"+
    " zau�ku i wraca pokazuj�c dyskretnie "+QIMIE(this_player(), PL_CEL)+
    " jaki� przedmiot.\n");
    item_arr[0]->appraise_object();

    return 1;
}

void
emote_hook(string emote, object wykonujacy, string przyslowek)
{
    switch (emote)
    {
        case "uk�o�":
            set_alarm(0.5, 0.0, "command", "pokiwaj powitalnie do "
			+ OB_NAME(wykonujacy));
            break;
        case "poca�uj":
        case "przytul":
	case "pog�aszcz":
	case "szturchnij":
            set_alarm(0.5, 0.0, "command", "powiedz :do "
			+OB_NAME(wykonujacy)+" ze zloscia: Odch�do� si�.");
            break;
        case "kopnij":
        case "opluj":
            set_alarm(0.5, 0.0, "command", "kopnij "+OB_NAME(wykonujacy));
	    set_alarm(1.0, 0.0, "command", "spojrzyj zimno na "+OB_NAME(wykonujacy));
            break;
    }
}



