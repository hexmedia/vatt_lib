/**
 * pan sekretarz z ratusza rinde.
 *
 * opis: faeve
 * kod: delvert, potem gj
 *
 * bledy zglaszac do mnie, czyli do gjoefa.
 */

#pragma unique

#include "dir.h"
#include <std.h>
#include <wa_types.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <stdproperties.h>
#include "/d/Standard/Redania/Rinde/Centrum/Ratusz/admin/admin.h"

inherit RINDE_NPC_STD;
inherit "/lib/straz/odkupianie_win.c";

int i = 0;

int
zamknij();

int
co_godzine();

void
create_rinde_humanoid()
{
    ustaw_odmiane_rasy(PL_MEZCZYZNA);
    set_gender(G_MALE);

    dodaj_przym("wysoki", "wysocy");
    dodaj_przym("szczup^ly", "szczupli");

    dodaj_nazwy("sekretarz");

    set_living_name("rindesekretarz");

    ustaw_imie(({"rindesekretarz", "rindesekretarz", "rindesekretarz",
    "rindesekretarz", "rindesekretarz", "rindesekretarz"}), PL_MESKI_OS);

    set_long("Jak na cz^lowieka, m^e^zczyzna ten jest do^s^c wysokiego " +
        "wzrostu, co pot^eguje jeszcze fakt, ^ze jest on niebywale chudy i " +
        "ko^scisty. Jego ziemista cera przywo^luje na my^sl jak^a^s " +
        "nieznan^a chorob^e. Sprawia wra^zenie, jakby ci^agle siedzia^l w " +
        "zamkni^etym pomieszczeniu, nie wy^sciubiaj^ac nosa na zewn^atrz. " +
        "Twarz ma niczym pos�g - nieruchom^a, a oczy, zamy^slone, powoli i " +
        "jakby bezwiednie przesuwaj^a si^e po pomieszczeniu.\n");
                                                           /* Za uchem, " +
        "przykryte nied^lugimi, kruczoczarnymi w^losami ma zatkni^ete " +
        "g^esie pi^oro.\n"); */

    set_stats(({40, 40, 40, 70, 20000}));

    set_skill(SS_APPR_OBJ, 50 + random(4));
    set_skill(SS_LANGUAGE, 39 + random(10));
    set_skill(SS_AWARENESS, 33);

    add_prop(CONT_I_WEIGHT, 75000);
    add_prop(CONT_I_HEIGHT, 187);

    add_armour("/d/Standard/Redania/Rinde/przedmioty/ubrania/szaty/luzna_szara.c");

    set_chat_time(159);
    add_chat("A mog^lem zosta^c bardem.");
    add_chat("Pan " + BURMISTRZ_N + " ma ostatnio moc zaj^e^c. Ja zreszt^a te^z.");
    add_chat("Wsz^edzie te podania. Co si^e sta^lo, ^ze ka^zdy ma jak^a^s spraw^e do burmistrza?");

    set_act_time(133);
    add_act("westchnij .");
    add_act("emote poprawia swoje kruczoczarne w^losy.");
    add_act("rozejrzyj sie lekko");
    add_act("potrzyj policzek");

    set_cchat_time(18);
    add_cchat("Ja ci pok^a^z^e!");
    add_cchat("Stra^zeee!");

    set_cact_time(7);
    add_cact("emote ryczy w^sciekle.");
    add_cact("emote sapie gro^xnie.");

    set_default_answer(VBFC_ME("pyt_default"));
    add_ask(({"podanie", "podania","sk�adanie podania"}), VBFC_ME("pyt_podanie"));
    add_ask(({"burmistrza", LC(BURMISTRZ), LC(BURMISTRZA), LC(BURMISTRZ_N),
        LC(BURMISTRZA_N)}), VBFC_ME("pyt_burmistrz"));
    add_ask("prac^e", VBFC_ME("pyt_praca"));

    set_alarm(8.0, 8.0, "zamknij");
    set_alarm(1.0, 0.0, "command", "usiadz przy biurku");

    seteuid(getuid());

    set_alarm_every_hour("co_godzine");
    set_alarm(1.0, 0.0, "co_godzine");

    //Czemu o ma drzwi??? (KRUN)
    add_object(RINDE + "Centrum/Ratusz/drzwi/k_pokoj_burmistrza.c");
    add_object(RINDE + "Centrum/drzwi/k_ratusz.c");

    //A teraz konfigurujemu mo�liwo�� odkupiania win:
    config_odkupiciel();
}

void
powiedz_gdy_jest(mixed player, string tekst)
{
    if (pointerp(player))
    {
        if (sizeof(FILTER_PRESENT_LIVE(player)) > 0)
            this_object()->command(tekst);
        else
        switch (random(3))
        {
            case 0:
                this_object()->command("rozejrzyj sie szybko");
                break;

            case 1:
                this_object()->command("powiedz :cicho: Kto to by^l?");
                break;

            case 2:
                this_object()->command("ob sie");
                break;

            case 3:
                this_object()->command("powiedz :cicho: Chyba ju^z wariuj^e...");
                break;
        }
    }


    if (environment(this_object()) == environment(player))
        this_object()->command(tekst);
    else
        switch (random(3))
        {
            case 0:
                this_object()->command("rozejrzyj sie szybko");
                break;

            case 1:
                this_object()->command("powiedz :cicho: Kto to by^l?");
                break;

            case 2:
                this_object()->command("ob sie");
                break;

            case 3:
                this_object()->command("powiedz :cicho: Chyba ju^z wariuj^e...");
                break;
        }
}

void
emote_hook(string emote, object wykonujacy, string przyslowek = 0)
{
    switch (emote)
    {
    case "uk^lo^n":
      command("kiwnij glowa");
      break;
    case "zata^ncz":
    case "u^sciskaj":
    case "szturchnij":
    case "pogr^o^x":
    case "po^laskocz":
    case "obejmij":
    case "klepnij":
    case "gr^o^x":
    case "b^lagaj":
    case "pog^laszcz":
    case "poca^luj":
    case "przytul":
      switch (random(3))
      {
      case 0:
        command("skrzyw sie");
        break;
      case 1:
        command("fuknij");
        break;
      case 2:
        command("zacisnij piesci");
        break;
      }
      break;
    case "spoliczkuj":
    case "kopnij":
    case "opluj":
      set_alarm(1.0, 0.0, "wkop_mu", wykonujacy);
      break;
    case "za^smiej":
      set_alarm(1.0, 0.0, "return_laugh", wykonujacy);
      break;
    case "przywitaj":
    case "powitaj":
      set_alarm(1.0, 0.0, "przywitaj", wykonujacy);
      break;
    }
}

void
wkop_mu(object kto)
{
    switch (random(3))
    {
    case 0:
      powiedz_gdy_jest(TP, "powiedz do " + OB_NAME(TP) +
      " Doprawdy, niech^ze si^e pan" + kto->koncowka("", "i") + " uspokoi!");
      break;
    case 1:
      command("sapnij ciezko");
      break;
    case 2:
      powiedz_gdy_jest(TP, "powiedz do " + OB_NAME(TP) + " Prosz^e st^ad " +
      "wyj^s^c. Natychmiast.");
    }
}

void
przywitaj(object kto)
{
     powiedz_gdy_jest(TP, "powiedz do " + OB_NAME(TP) + " Witam, w czym " +
     "mog^e pom^oc?");
}

string
jakie_podanie()
{
    object kto = this_player();
    mixed *podanie = (MAIN)->wczytaj_podanie(kto->query_real_name());
    string plec = kto->koncowka("Pa^nskie", "Pani", "Wasze");

    if (podanie)
    {
        if(podanie[0]==NOWE_P || podanie[0] == AKTUALNE_P)
        {
            powiedz_gdy_jest(TP, "powiedz do " + OB_NAME(TP) +
            " Aha! " + plec + " podanie jest w trakcie rozpatrywania.");
                return "";
        }

        if(podanie[0] == ZAAKCEPTOWANE_P)
        {
                     command("usmiechnij sie .");
                     powiedz_gdy_jest(TP, "powiedz do " + OB_NAME(TP) +
                     " " + plec + " podanie zosta^lo pomy^slnie rozpatrzone!");
                     if (file_size(LPATH + "exec/" + lower_case(this_player()->query_name()) + ".c") > -1)
                     {
                         seteuid(getuid());

                         LOAD_ERR(LPATH + "exec/" + lower_case(this_player()->query_name()) + ".c");
                         object plik = find_object(LPATH + "exec/" + lower_case(this_player()->query_name()) + ".c");
                         LOAD_ERR(plik);
                         call_other(plik, "faka");
                         call_other(plik, "oco");

                         int oco = plik->oco();

                         string przedst = TP->query_name();
                         if (TP->query_surname())
                         przedst += " " + TP->query_surname();
                         if (TP->query_origin())
                         przedst += " " + TP->query_origin();

                         string wiad = "";
                         wiad += "\n\tNa mocy praw mi^lo^sciwie nam panuj^acego " +
                         "Ja^snie Pana Vizimira I, z woli bo^zej i ludzkiej " +
                         "kr^ola i jedynego prawowitego w^ladcy Redanii, a z " +
                         "decyzji w^ladz miasta Rinde niniejszym og^lasza si^e, " +
                         "i^z z dniem dzisiejszym\n\n";
                         wiad += sprintf("%80|s\n\n", przedst);

                         switch (oco)
                         {
                             case 2:
                                 wiad += "zostaje mieszka^ncem " +
                                 "miasta Rinde, wobec czego otrzymuje wszelkie prawa "+
                                 "tego^z. Jednocze^snie zobowi^azuje si^e " +
                                 TP->koncowka("go", "j^a") + " do sumiennego " +
                                 "przestrzegania kodeksu miasta pod kar^a " +
                                 "cofni^ecia praw.\n\n\n";
                                 break;

                             case 1:
                                 wiad += "otrzymuje pe^lne prawo do u^zywania " +
                                 "nazwiska " + plik->jakie() + " na ziemiach " +
                                 "Ja^snie Pana.\n\n\n";
                                 break;

                             case 4: // tu potem zrobic cos z adresem, zeby wygladalo zreczniej
                                 wiad += "zostaje w^la^scicielem " + plik->gdzie() +
                                 " i otrzymuje wszelkie prawa do rozporz^adza"+
                                 "nia swoim nabytkiem. Tym samym zobowi^azuje "+
                                 "si^e " + TP->koncowka("go", "j^a") + " do " +
                                 "uiszczenia op^laty skarbowej w kwocie dziesi^eciu " +
                                 "koron reda^nskich.\n\n\n";
                                 break;

                             case 3:
                                 wiad += "otrzymuje zgod^e na prowadzenie " +
                                 "handlu na ziemiach Ja^snie Pana za wyj^atkiem " +
                                 "dni ^swi^etych. Tym samym zobowi^azuje si^e " +
                                 "go do uiszczania comiesi^ecznego podatku " +
                                 "od handlu w kwocie dziesi^eciu koron " +
                                 "reda^nskich pod kar^a cofni^ecia zezwolenia.\n\n\n";
                                 break;

                             case 5:
                                 wiad += plik->tresc() + "\n\n\n";
                                 break;

                             case 9:
                                 break;

                             default:
                                 wiad += "B^L^AD!\n\n\n";
                                 find_player("gjoef")->catch_msg("B^L^AD: " + file_name(plik) + "->oco()\n");
                                 break;
                         }

                         wiad += sprintf("%70s", BURMISTRZ + " " + BURMISTRZ_OPT + BURMISTRZ_N+",\n");
                         wiad += sprintf("%70s", "burmistrz Rinde.\n");

                         if (plik->oco() != 9)
                         {
                             string polka = "";
                             switch (random(2))
                             {
                                 case 0:
                                 polka = "pierwsza p^o^lka";
                                 break;
                                 case 1:
                                 polka = "pierwsza p^o^lka";
                                 break;
                                 case 2:
                                 polka = "pierwsza p^o^lka";
                                 break;
                             }
                             object doku = clone_object(SCIEZKA + "admin/dokument.c");
                             doku->set_message(wiad);
                             doku->init_doku_arg(doku->query_doku_auto_load());
                             doku->move(this_object());

                             this_object()->command("daj dokument " + OB_NAME(TP));
                         }
                         plik->ustaw();
                         rm(file_name(plik) + ".c");
                         plik->destruct();
                         (MAIN)->usun_podanie(0, podanie[1]);
                     }
                     else
                         TP->catch_msg("Wyst^api^l pewien b^l^ad. Zg^lo^s " +
                         "go niezw^locznie kt^oremu^s z czarodziej^ow!\n");
                     return "";
        }

        if(podanie[0]==ODRZUCONE_P)
        {
            powiedz_gdy_jest(TP, "powiedz do " + OB_NAME(TP) +
            " Hmmm... Ot^o^z" + lower_case(plec) + " podanie zosta^lo odrzucone.");
                return "";
        }

        powiedz_gdy_jest(TP, "szepnij " + OB_NAME(TP) + " Aardwolf! Wywo^la^lo si^e?");
        return "";
    }
    else
    {
        powiedz_gdy_jest(TP, "powiedz do " + OB_NAME(TP) +" Czy^zby " + kto->koncowka("chcia^l pan", "chcia^la pani") + " z^lo^zy^c podanie?");
        return "";
    }
}

string
pyt_default()
{
    powiedz_gdy_jest(TP, "powiedz do " + OB_NAME(TP) +
    " Nie wiem o co " + TP->koncowka("panu", "pani") + " chodzi. Ja tu " +
    "jestem tylko sekretarzem.");

    return "";
}

string
pyt_podanie()
{
    object kto = this_player();
    mixed *podanie = (MAIN)->wczytaj_podanie(kto->query_real_name());
    string odpo = " ";
    string plec = kto->koncowka("Pa^nskie", "Pani", "Wasze");

    if (podanie)
    {
        if (this_player()->query_prop("zapytal_o_podanie") == 0)
        {
            command("powiedz Sprawd^xmy, sprawd^xmy...");
            set_alarm(1.0, 0.0, "command", "emote przerzuca szybko papiery " +
                "na biurku.");
			if(POCHODZENIE ~= podanie[6])
            	set_alarm(6.0, 0.0, "jakie_podanie");
			else
			{
				command("rozloz rece ");
				command("'Ale� ja nie mam �adnego podania od "+kto->koncowka("Pana",
						"Pani","Wa�ci")+"!");
				return "";
			}

            this_player()->add_prop("zapytal_o_podanie", podanie[0] + 1);
            return "";
        }
        else
            switch (this_player()->query_prop("zapytal_o_podanie"))
            {
                case 2:
                case 3:
                    if (sizeof(podanie[2])>=POPARCIE)
                        odpo += "Tak, tak, gratuluj^e pan" +
                        kto->koncowka("u", "i") + ".";
                    else
                        odpo += "Podanie jest w trakcie rozpatrywania.";
                    break;

                case 4:
                    odpo += "Burmistrz odrzuci^l wniosek, przykro mi.";
                    break;

                case 5:
                    odpo += "Tak, tak, gratuluj^e pan" +
                    kto->koncowka("u", "i") + ".";
                    break;

                default:
                    odpo += "Podanie zostanie rozpatrzone w swoim czasie.";
                    break;
            }
    }
    else
        odpo += "Czy^zby chcia^l" + kto->koncowka(" pan", "a pani") +
            " z^lo^zy^c podanie?";

    powiedz_gdy_jest(TP, "powiedz do " + OB_NAME(TP) + odpo);
}

string
pyt_burmistrz()
{
    string odpo = " Pan " + BURMISTRZ_N + "? ";

    if (!environment(TO)->dzien_noc())
    {
        if (present(find_living(LC(BURMISTRZ)), find_object("/d/Standard/Redania/Rinde/Centrum/Ratusz/lokacje/pokoj_burmistrza")))
            odpo += "Jest teraz w swoim gabinecie.";
        else
            odpo += "Musia^l w pewnej sprawie wyjecha^c. Najpewniej wr^oci "+
                "niebawem.";
    }
    else
    {
        if (present(find_living(LC(BURMISTRZ)), find_object("/d/Standard/Redania/Rinde/Centrum/Ratusz/lokacje/pokoj_burmistrza")))
            odpo += "Chyba jeszcze jest w swoim gabinecie.";
        else
            odpo += "Nie pracuje o tej porze.";
    }

    powiedz_gdy_jest(TP, "powiedz do " + OB_NAME(TP) + odpo);

    return "";
}

string
pyt_praca()
{
    command("wskaz");
    set_alarm(1.0, 0.0, "command", "powiedz To jest moja praca.");

    return "";
}

//uwaga - nie uzywaj tej funkcji ;)
int
podanie_awaryjne_z_palca(string x)
{
if (file_size(LPATH + "exec/" + lower_case(x) + ".c") > -1)
            {
                         seteuid(getuid());

                         LOAD_ERR(LPATH + "exec/" + lower_case(x) + ".c");
                         object plik = find_object(LPATH + "exec/" + lower_case(x) + ".c");
                         LOAD_ERR(plik);

                         int oco = plik->oco();

                         string przedst = x;
                         if (TP->query_surname())
                         przedst += " " + TP->query_surname();
                         if (TP->query_origin())
                         przedst += " " + TP->query_origin();
x=upper_case(x);
                         string wiad = "";
                         wiad += "\n\tNa mocy praw mi^lo^sciwie nam panuj^acego " +
                         "Ja^snie Pana Vizimira I, z woli bo^zej i ludzkiej " +
                         "kr^ola i jedynego prawowitego w^ladcy Redanii, a z " +
                         "decyzji w^ladz miasta Rinde niniejszym og^lasza si^e, " +
                         "i^z z dniem dzisiejszym\n\n";
                         wiad += sprintf("%80|s\n\n", przedst);

                         switch (oco)
                         {
                             case 2:
                                 wiad += "zostaje mieszka^ncem " +
                                 "miasta Rinde, wobec czego otrzymuje wszelkie prawa "+
                                 "tego^z. Jednocze^snie zobowi^azuje si^e " +
                                 TP->koncowka("go", "j^a") + " do sumiennego " +
                                 "przestrzegania kodeksu miasta pod kar^a " +
                                 "cofni^ecia praw.\n\n\n";
                                 break;

                             case 1:
                                 wiad += "otrzymuje pe^lne prawo do u^zywania " +
                                 "nazwiska " + plik->jakie() + " na ziemiach " +
                                 "Ja^snie Pana.\n\n\n";
                                 break;

                             case 4: // tu potem zrobic cos z adresem, zeby wygladalo zreczniej
                                 wiad += "zostaje w^la^scicielem " + plik->gdzie() +
                                 " i otrzymuje wszelkie prawa do rozporz^adza"+
                                 "nia swoim nabytkiem. Tym samym zobowi^azuje "+
                                 "si^e " + TP->koncowka("go", "j^a") + " do " +
                                 "uiszczenia op^laty skarbowej w kwocie dziesi^eciu " +
                                 "koron reda^nskich.\n\n\n";
                                 break;

                             case 3:
                                 wiad += "otrzymuje zgod^e na prowadzenie " +
                                 "handlu na ziemiach Ja^snie Pana za wyj^atkiem " +
                                 "dni ^swi^etych. Tym samym zobowi^azuje si^e " +
                                 "go do uiszczania comiesi^ecznego podatku " +
                                 "od handlu w kwocie dziesi^eciu koron " +
                                 "reda^nskich pod kar^a cofni^ecia zezwolenia.\n\n\n";
                                 break;

                             case 5:
                                 wiad += plik->tresc() + "\n\n\n";
                                 break;

                             case 9:
                                 break;

                             default:
                                 wiad += "B^L^AD!\n\n\n";
                                 find_player("gjoef")->catch_msg("B^L^AD: " + file_name(plik) + "->oco()\n");
                                 break;
                         }

                         wiad += sprintf("%70s", BURMISTRZ + " " + BURMISTRZ_OPT + BURMISTRZ_N+",\n");
                         wiad += sprintf("%70s", "burmistrz Rinde.\n");

                         if (plik->oco() != 9)
                         {
                             string polka = "";
                             switch (random(2))
                             {
                                 case 0:
                                 polka = "pierwsza p^o^lka";
                                 break;
                                 case 1:
                                 polka = "pierwsza p^o^lka";
                                 break;
                                 case 2:
                                 polka = "pierwsza p^o^lka";
                                 break;
                             }
                             object doku = clone_object(DOKUMENT);
                             doku->set_message(wiad);
                             doku->init_doku_arg(doku->query_doku_auto_load());
                             doku->move(this_object());

                             set_alarm(5.0, 0.0, "command", "daj dokument " + OB_NAME(TP));
							//just in case
							set_alarm(6.0, 0.0, "command", "odloz dokument ");
                         }
                         plik->ustaw();
                         rm(file_name(plik) + ".c");
                         plik->destruct();
            }
            else
                TP->catch_msg("Wyst^api^l pewien b^l^ad. Zg^lo^s " +
                    "go niezw^locznie kt^oremu^s z czarodziej^ow!\n");
return 1;
}

int
zloz(string str)
{
    	mixed *podanie;
        object kto=TP;
        string plec = kto->koncowka("Pa^nskie", "Pani", "Wasze");

    if (!str || str != "podanie")
    {
        notify_fail("Co chcesz z^lo^zy^c? Podanie?\n");
        return 0;
    }


 	if ((TP->query_age() < 216000) &&
        (!TP->query_wiz_level()) &&	(!TP->query_npc()))
    {
	write("Niestety, osoby o wieku mniejszym ni� 5 dni nie mog� " +
	    "sk�ada� poda�. Obejrzyj troch� " +
	    "�wiata, zanim zaczniesz si� ubiega� o dokumenty.\n");
	return 1;
    }


    podanie = (MAIN)->wczytaj_podanie(kto->query_real_name());

    if (this_player()->query_prop("napisal_teraz_podanie") == 0)
    {
        if (podanie && podanie[0] == 1)
        {
            if (this_player()->query_prop("zapytal_o_podanie") == 0)
            {
                powiedz_gdy_jest(TP, "powiedz do " + OB_NAME(TP) +
                    " Chwileczk^e...");
                set_alarm(1.0, 0.0, "command", "emote przerzuca szybko papiery " +
                    "na biurku.");
                set_alarm(4.0, 0.0, "powiedz_gdy_jest", TP, "powiedz do " +
                    OB_NAME(TP) + " No w^la^snie. Wygl^ada na to, ^ze " +
                    "jedno " + lower_case(plec) + " podanie jest ju^z w trakcie " +
                    "rozpatrywania.");
                this_player()->add_prop("zapytal_o_podanie", 1);
                return 1;
            }
            else
            {
                powiedz_gdy_jest(TP, "powiedz do " + OB_NAME(TP) +
                    " Prosz^e poczeka^c na rozpatrzenie pierwszego podania.");
                return 1;
            }
        }

        if (podanie && podanie[0] == 3)
        {
            set_alarm(5.0, 0.0, "powiedz_gdy_jest", TP, "powiedz do " +
            OB_NAME(TP) + " A w^lasnie, " + lower_case(plec) + " poprzednie " +
            "podanie zosta^lo odrzucone.");
            this_player()->add_prop("zapytal_o_podanie", 1);
        }

        if (podanie && podanie[0] == 4)
        {
            set_alarm(5.0, 0.0, "powiedz_gdy_jest", TP, "powiedz do " +
            OB_NAME(TP) + " By^lbym zapomnia^l! " + plec + " poprzednie " +
            "podanie zosta^lo pomy^slnie rozpatrzone.");
            if (file_size(LPATH + "exec/" + lower_case(this_player()->query_name()) + ".c") > -1)
            {
                         seteuid(getuid());

                         LOAD_ERR(LPATH + "exec/" + lower_case(this_player()->query_name()) + ".c");
                         object plik = find_object(LPATH + "exec/" + lower_case(this_player()->query_name()) + ".c");
                         LOAD_ERR(plik);

                         int oco = plik->oco();

                         string przedst = TP->query_name();
                         if (TP->query_surname())
                         przedst += " " + TP->query_surname();
                         if (TP->query_origin())
                         przedst += " " + TP->query_origin();

                         string wiad = "";
                         wiad += "\n\tNa mocy praw mi^lo^sciwie nam panuj^acego " +
                         "Ja^snie Pana Vizimira I, z woli bo^zej i ludzkiej " +
                         "kr^ola i jedynego prawowitego w^ladcy Redanii, a z " +
                         "decyzji w^ladz miasta Rinde niniejszym og^lasza si^e, " +
                         "i^z z dniem dzisiejszym\n\n";
                         wiad += sprintf("%80|s\n\n", przedst);

                         switch (oco)
                         {
                             case 2:
                                 wiad += "zostaje mieszka^ncem " +
                                 "miasta Rinde, wobec czego otrzymuje wszelkie prawa "+
                                 "tego^z. Jednocze^snie zobowi^azuje si^e " +
                                 TP->koncowka("go", "j^a") + " do sumiennego " +
                                 "przestrzegania kodeksu miasta pod kar^a " +
                                 "cofni^ecia praw.\n\n\n";
                                 break;

                             case 1:
                                 wiad += "otrzymuje pe^lne prawo do u^zywania " +
                                 "nazwiska " + plik->jakie() + " na ziemiach " +
                                 "Ja^snie Pana.\n\n\n";
                                 break;

                             case 4: // tu potem zrobic cos z adresem, zeby wygladalo zreczniej
                                 wiad += "zostaje w^la^scicielem " + plik->gdzie() +
                                 " i otrzymuje wszelkie prawa do rozporz^adza"+
                                 "nia swoim nabytkiem. Tym samym zobowi^azuje "+
                                 "si^e " + TP->koncowka("go", "j^a") + " do " +
                                 "uiszczenia op^laty skarbowej w kwocie dziesi^eciu " +
                                 "koron reda^nskich.\n\n\n";
                                 break;

                             case 3:
                                 wiad += "otrzymuje zgod^e na prowadzenie " +
                                 "handlu na ziemiach Ja^snie Pana za wyj^atkiem " +
                                 "dni ^swi^etych. Tym samym zobowi^azuje si^e " +
                                 "go do uiszczania comiesi^ecznego podatku " +
                                 "od handlu w kwocie dziesi^eciu koron " +
                                 "reda^nskich pod kar^a cofni^ecia zezwolenia.\n\n\n";
                                 break;

                             case 5:
                                 wiad += plik->tresc() + "\n\n\n";
                                 break;

                             case 9:
                                 break;

                             default:
                                 wiad += "B^L^AD!\n\n\n";
                                 find_player("gjoef")->catch_msg("B^L^AD: " + file_name(plik) + "->oco()\n");
                                 break;
                         }

                         wiad += sprintf("%70s", BURMISTRZ + " " + BURMISTRZ_OPT + BURMISTRZ_N+",\n");
                         wiad += sprintf("%70s", "burmistrz Rinde.\n");

                         if (plik->oco() != 9)
                         {
                             string polka = "";
                             switch (random(2))
                             {
                                 case 0:
                                 polka = "pierwsza p^o^lka";
                                 break;
                                 case 1:
                                 polka = "pierwsza p^o^lka";
                                 break;
                                 case 2:
                                 polka = "pierwsza p^o^lka";
                                 break;
                             }
                             object doku = clone_object(DOKUMENT);
                             doku->set_message(wiad);
                             doku->init_doku_arg(doku->query_doku_auto_load());
                             doku->move(this_object());

                             set_alarm(5.0, 0.0, "command", "daj dokument " + OB_NAME(TP));
							//just in case
							set_alarm(6.0, 0.0, "command", "odloz dokument ");
                         }
                         plik->ustaw();
                         rm(file_name(plik) + ".c");
                         plik->destruct();
            }
            else
                TP->catch_msg("Wyst^api^l pewien b^l^ad. Zg^lo^s " +
                    "go niezw^locznie kt^oremu^s z czarodziej^ow!\n");
        }

        if (podanie)
            kto->add_prop("zapytal_o_podanie", podanie[0] + 1);
        else
            kto->add_prop("zapytal_o_podanie", 1);

        if (podanie && podanie[0] > 1)
            (MAIN)->usun_podanie(0, podanie[1]);

     	write("Zaczynasz pisa^c podanie.\n");
        say(QCIMIE(kto,PL_MIA) + " zaczyna pisa^c podanie.\n");
        clone_object("/obj/edit.c")->edit("napisz");
        return 1;
    }
    else
    {
        powiedz_gdy_jest(TP, "powiedz do " + OB_NAME(TP) + " Chyba " +
        "niedawno pisa^l" + kto->koncowka(" pan", "a pani") + " jedno...");
        return 1;
    }
}

int
napisz(string str)
{
    object kto=TP;

        if (!str || str=="")
        {
                write("Rezygnujesz z pisania.\n");
                return 0;
        }

(MAIN)->dodaj_nowe_podanie(str, kto,POCHODZENIE);

        write("Ko^nczysz pisa^c podanie i oddajesz je " + query_imie(kto,PL_CEL)+".\n");
        say(QCIMIE(kto,PL_MIA) + " ko^nczy pisa^c podanie i oddaje je " +QIMIE(this_object(),PL_CEL) + ".\n");
        kto->add_prop("napisal_teraz_podanie", 1);

        set_alarm(1.5, 0.0, "powiedz_gdy_jest", kto, "powiedz do " + OB_NAME(kto) +
        " Za kilka dni mo^ze pan" + kto->koncowka("", "i") + " wr^oci^c i zapyta^c " +
        "o podanie.");

        return 1;
}

public int
wyswietl_pomoc()
{
	write("//\t\tpomoc do sk�adania poda�\t\t\\\\\n\n"+
		"W tym miejscu mo�na sk�ada� podania. Pisz�c je musisz "+
		"uwzgl�dni� temat podania - tj. o co chcesz si� "+
		"ubiega�, a ubiega� mo�na si� tutaj o: nadanie nazwiska "+
		"rodowego, pochodzenie (obywatelstwo - dot. tylko miejsca, w kt�rym "+
		"si� obecnie znajdujesz), zezwolenie na handel lub te� o prawo "+
		"w�asno�ci nieruchomo�ci lub gruntu.\n"+
		"Gdy sko�czysz, wr�� tu za jaki� czas i zapytaj o podanie, by "+
		"dowiedzie� si�, czy zosta�o ono pozytywnie rozpatrzone, czy te� nie.\n\n"+
		"\\\\\t\t                        \t\t//\n");

	return 1;
}

public int
cala_pomoc()
{
    wyswietl_pomoc();
    write(::query_pomoc());
    //odkupiciel_help();
}

public int
pomoc(string str)
{
    object ob;
    notify_fail("Nie ma pomocy na ten temat.\n");
    if (!str) return 0;
    if (!parse_command(str, ENV(TP), "%o:" + PL_MIA, ob))
        return 0;
    if (!ob) return 0;
    if (ob != TO)
        return 0;

    wyswietl_pomoc();
    return 1;
}

void
init_living()
{
    object kto;

    ::init_living();

    add_action("zloz","z^l^o^z");
    add_action("pomoc", "?", 2);
    add_action("cala_pomoc","?");
    add_action("wyswietl_pomoc","?z��");

    init_odkupiciel();
}

int
zamknij()
{
    object *okna = object_clones(find_object("/d/Standard/Redania/Rinde/Centrum/Ratusz/obiekty/administracja_okno.c"));
    object okno = sizeof(okna) == 1 ? okna[0] : 0;

    if(okno)
    {
        if(okno->query_open())
        {
            command("wstan");
            set_alarm(2.0, 0.0, "command", "pokrec ciezko");
            set_alarm(3.5, 0.0, "command", "zamknij okno");
            set_alarm(4.5, 0.0, "command", "usiadz przy biurku");
        }
    }

    return 1;
}


void
return_introduce(object ob) // dlaczego powiedz_gdy_jest tu nie dzialalo?
{
    this_object()->command("powiedz do " + OB_NAME(this_player()) +
    " Tak, mi^lo mi. W czym mog^e pom^oc?");
}

void
znik()
{
    command("emote znika w jednym z zau^lk^ow.");
    remove_object();
}

void
do_domu()
{
    if(query_attack())
    {
        set_alarm(10.0,0.0,"do_domu");
        return;
    }
    else
    {
        int k = 0;
        mixed droga2=znajdz_sciezke(environment(this_object()),
            POLNOC + "lokacje/ulica2");
        command(droga2[k]);
        if(k==sizeof(droga2)-1)
        {
            set_alarm(5.0,0.0,"znik");
            return;
        }
        else
        {
            k++;
            set_alarm(15.0,0.0,"do_domu");
            return;
        }
    }
}

void
wypadaja(object *kto, object lok, object drzwi)
{
    switch(sizeof(kto))
    {
        case 0:
            return;
            break;
        case 1:
            find_player("gjoef")->catch_msg("wypadaja: 1\n");
            FILTER_LIVE(deep_inventory(lok))->catch_msg(capitalize(
                COMPOSITE_LIVE(filter(kto, &->check_seen(TO)), 0)) +
                " wpada z hukiem przez " + drzwi->short(3) + ".\n");
            return;
            break;
        default:
            find_player("gjoef")->catch_msg("wypadaja: >1\n");
            FILTER_LIVE(deep_inventory(lok))->catch_msg(capitalize(
                COMPOSITE_LIVE(filter(kto, &->check_seen(TO)), 0)) +
                " wpadaj^a z hukiem przez " + drzwi->short(3) + ".\n");
            return;
            break;
    }
}

int
czy_ktos_zostal()
{
    object *oni = ({});
    object dokad, drzwi;

    if (FILTER_LIVE(deep_inventory(ENV(TO)) - ({TO})))
        oni = FILTER_LIVE(deep_inventory(ENV(TO)) - ({TO}));
    else
        return 1;

    if (member_array(1, FILTER_LIVE(deep_inventory(ENV(TO))-({TO}))->check_seen(TO)) != -1)
    {
        switch(i)
        {
            case 0:
                 i = 1;
                oni = FILTER_LIVE(deep_inventory(ENV(TO))) - ({TO});
                set_alarm(2.0, 0.0, "powiedz_gdy_jest", filter(oni,
                    &->check_seen(TO)), "powiedz do " + COMPOSITE_LIVE(filter(
                    oni, &->check_seen(TO)), 1) + " Ja ju^z zamykam, dobranoc.");
                set_alarm(12.0, 0.0, "czy_ktos_zostal");
                return 1;
                 break;

            case 1:
                i = 2;
                TO->command("tupnij");
                set_alarm(2.0, 0.0, "powiedz_gdy_jest", filter(oni,
                    &->check_seen(TO)), "powiedz do " + COMPOSITE_LIVE(filter(
                    oni, &->check_seen(TO)), 1) + " Prosz^e st^ad wyj^s^c, " +
                    "bo chc^e teraz zamkn^a^c te drzwi i wr^oci^c jak norma" +
                    "lny cz^lowiek do domu.");
                set_alarm(9.0, 0.0, "czy_ktos_zostal");
                return 1;
                break;

            case 2:
                i = 3;
                oni = FILTER_LIVE(deep_inventory(ENV(TO))) - ({TO});
                TO->command("skrzyw sie .");
                set_alarm(2.0, 0.0, "powiedz_gdy_jest", filter(oni,
                    &->check_seen(TO)), "powiedz do " + COMPOSITE_LIVE(filter(
                    oni, &->check_seen(TO)), 1) + " Ostatni raz m^owi^e, " +
                    "prosz^e wyj^s^c!");
                set_alarm(7.0, 0.0, "czy_ktos_zostal");
                set_alarm(5.5, 0.0, "powiedz_gdy_jest", filter(oni,
                    &->check_seen(TO)), "powiedz do " + COMPOSITE_LIVE(filter(
                    oni, &->check_seen(TO)), 1) + " No do jasnej cholery!");
                return 1;
                break;

            case 3:
                i = 0;
                oni = FILTER_CAN_SEE(FILTER_LIVE(deep_inventory(ENV(TO))) - ({TO}), TO);
                if (wildmatch("*tracja", file_name(ENV(TO))))
                {
                    dokad = find_object(CENTRUM + "Ratusz/lokacje/korytarz");
                    drzwi = find_object(CENTRUM + "Ratusz/drzwi/korytarz");
                }
                else if (wildmatch("*tarz", file_name(ENV(TO))))
                {
                    dokad = find_object(CENTRUM_LOKACJE + "placw");
                    drzwi = find_object(CENTRUM + "drzwi/do_ratusza");
                }
                else
                {
                    dokad = find_object(TP->query_default_start_location());
                    TP->catch_msg("Nast^api^l nieprzewidziany b^l^ad. Niez" +
                    "w^locznie zg^lo^s go czarodziejom!\n");
                    find_player("gjoef")->catch_msg(set_color(1) +
                        set_color(5) + set_color(31) + "\n***B^L^AD!***\n\n");
                    return 1;
                }

                oni->catch_msg(QCIMIE(TO, 0) + " szybkim ruchem wyrzuca " +
                "ci^e za drzwi.\n");
                saybb(QCIMIE(TO, 0) + " szybkim ruchem wyrzuca " + COMPOSITE_LIVE(filter(
                    oni, &->check_seen(TO)), 3) + " za drzwi.\n", oni);

                seteuid(getuid());

                wypadaja(filter(oni, &->check_seen(TO)), dokad, drzwi);

                oni->move(dokad);

                TO->command("zgas lampke");  //taki ciag jest chyba konieczny,
                TO->command("otworz drzwi"); //zeby w miedzyczasie jakis
                TO->command("wyjscie");      //idiota nie wbil do srodka
                TO->command("zamknij " + OB_NAME(drzwi));
                TO->command("zamknij " + OB_NAME(drzwi) + " kluczem");

                if (wildmatch("*tarz", file_name(ENV(TO))))
                    set_alarm(3.0, 0.0, "czy_ktos_zostal");
                else
                {
                    if (wildmatch("*plac*", file_name(ENV(TO))))
                    {
                        TO->command("pokrec ciezko");
                        do_domu();
                        return 1;
                    }
                    else
                    {
                        find_player("gjoef")->catch_msg(set_color(1) +
                        set_color(5) + set_color(31) + "\n***B^L^AD!***\n\n");
                        set_alarm(3.0, 0.0, "czy_ktos_zostal");
                        return 1;
                    }
                }
                return 1;
                break;
        }
    }
        else
        {
            if (wildmatch("*acja", file_name(ENV(TO))))
            {
                TO->command("zgas lampke");
                set_alarm(2.0, 0.0, "command", "otworz drzwi");
                set_alarm(3.0, 0.0, "command", "wyjscie");
                set_alarm(4.0, 0.0, "command", "zamknij debowe drzwi");
                set_alarm(6.0, 0.0, "command", "zamknij debowe drzwi kluczem");
                set_alarm(8.0, 0.0, "czy_ktos_zostal");
                return 1;
            }
            else
            {
                if (wildmatch("*rytarz", file_name(ENV(TO))))
                {
                    set_alarm(2.0, 0.0, "command", "otworz wielkie drzwi");
                    set_alarm(3.0, 0.0, "command", "wyjscie");
                    set_alarm(4.0, 0.0, "command", "zamknij wielkie drzwi");
                    set_alarm(6.0, 0.0, "command", "zamknij wielkie drzwi kluczem");
                    set_alarm(6.0, 0.0, "do_domu");
                    return 1;
                }
                else
                {
                    do_domu();
                    return 1;
                }
            }
        }
    i = 0;
    return 1;
}

int
co_godzine()
{
 /* if (MT_GODZINA >= 22)
    {
        set_alarm(1.0, 0.0, "command", "zatrzyj rece");
        set_alarm(3.0, 0.0, "command", "powiedz Uff! No to do domciu.");
        set_alarm(4.0, 0.0, "command", "wstan");
        set_alarm(6.0, 0.0, "command", "rozejrzyj sie uwaznie");
        set_alarm(8.0, 0.0, "czy_ktos_zostal");
    } */

    if(environment(this_object())->pora_roku() == MT_LATO ||
       environment(this_object())->pora_roku() == MT_WIOSNA)
    {
        if (ENV(TO)->pora_dnia() >= MT_POZNY_WIECZOR)
        {
            if (environment(this_object()) == find_object(SCIEZKA +
                "lokacje/administracja") &&
                find_object(SCIEZKA + "obiekty/administracja_biurko")->
                jest_rzecz_w_sublokacji("na", find_object(SCIEZKA +
                "obiekty/administracja_lampka")->short()) || present(SCIEZKA +
                "obiekty/administracja_lampka", find_object(SCIEZKA + "lokacje/administracja")))
            {
                if (find_object(SCIEZKA + "obiekty/administracja_lampka")->query_prop(OBJ_I_LIGHT) == 0)
                {
                    find_object(SCIEZKA + "obiekty/administracja_lampka")->add_prop(OBJ_I_LIGHT, 1);
    	            set_alarm(3.0, 0.0, "command", "emote zapala ma^l^a " +
                        "olejn^a lampk^e i stawia j^a na biurku.");
                }
            }
            else
            {
                set_alarm(4.0, 0.0, "command", "powiedz Noc nadchodzi, " +
                    "a lampk^e diabli wzi^eli! Mam tu teraz siedzie^c po " +
                    "ciemku?");
                set_alarm(1.0, 0.0, "command", "pokrec ciezko");
            }
        }
    }
    else
    {
        if (ENV(TO)->pora_dnia() >= MT_WIECZOR)
        {
            if (find_object(SCIEZKA + "obiekty/administracja_biurko")->
                jest_rzecz_w_sublokacji("na", find_object(SCIEZKA +
                "obiekty/administracja_lampka")->short()) || present(SCIEZKA +
                "obiekty/administracja_lampka", find_object(SCIEZKA + "lokacje/administracja")))
            {
                if (environment(this_object()) == find_object(SCIEZKA + "lokacje/administracja") &&
                    find_object(SCIEZKA + "obiekty/administracja_lampka")->query_prop(OBJ_I_LIGHT) == 0)
                {
                    find_object(SCIEZKA + "obiekty/administracja_lampka")->add_prop(OBJ_I_LIGHT, 1);
    	            set_alarm(3.0, 0.0, "command", "emote zapala ma^l^a " +
                        "olejn^a lampk^e i stawia j^a na biurku.");
                }
            }
            else
            {
                set_alarm(4.0, 0.0, "command", "powiedz Noc nadchodzi, " +
                    "a lampk^e diabli wzi^eli! Mam tu teraz siedzie^c po " +
                    "ciemku?");
                set_alarm(1.0, 0.0, "command", "pokrec ciezko");
            }
        }
    }

    return 1;
}

public string
query_auto_load()
{
    return 0;
}
