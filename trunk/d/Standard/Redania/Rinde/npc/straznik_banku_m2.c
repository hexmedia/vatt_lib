
/* Straznik - mezczyzna2 z banku w Rinde  *
 * Made by Sed 04.06.07                   *
 * Opis by Tinardan & Sed                 */


#include <std.h>
#include <ss_types.h>
#include <macros.h>
#include <stdproperties.h>
#include <pl.h>
#include "dir.h"

inherit RINDE_NPC_STD;

void
create_rinde_humanoid()
{
    ustaw_odmiane_rasy(PL_MEZCZYZNA);
    set_gender(G_MALE);
    dodaj_nazwy("straznik");
    dodaj_przym("^smierdz^acy","^smierdz^acy");
    dodaj_przym("rudy","rudzi");

    set_long("Opryszek o paskudnej g^ebie i jeszcze paskudniejszych, "+
        "nieustannie ruszaj^acych si� r^ekach. Rude w^losy przyci^ete "+
        "ma tu^z przy sk^orze, a g^low^e ods^loni^et^a. W r^eku trzyma "+
        "wzmocnion� ^zelazem maczug^e. Jego rozbiegane oczy nieustannie "+
        "czego^s szukaj^a, zupe^lnie niezale^znie od r^ak, a z gard^la "+
        "wyrywa si� pochrz^akiwanie. Smr^od, jaki od niego bije, jest "+
        "niemal^ze nie do zniesienia. ");

    set_stats (({ 80, 45, 80, 35, 80 }));
    add_prop(CONT_I_WEIGHT, 100000);
    add_prop(CONT_I_HEIGHT, 175);
    add_prop(LIVE_I_NEVERKNOWN, 1);

    set_act_time(30);
    add_act("emote chrzaka g^lo^sno.");
    add_act("rozejrzyj sie nerwowo.");
    add_act("emote spluwa sobie na buta.");
    add_act("emote porusza nerwowo r^ekoma.");
    add_act("emote wybucha gard^lowym ^smiechem.");

    set_cchat_time(8);
    add_cchat("Ha! Umrzesz!");
    add_cchat("Nie prze^zyjesz tego spotkania!");
    add_cchat("Nawet nie wiesz, ^ze ju^z jeste^s martwy!");

   //add_armour(cos);
   //add_armour(cos);
   //clone_object(sdf)->move(npc);

    add_ask(({"bank"}), VBFC_ME("pyt_o_bank"));
    add_ask(({"ruygena","bankiera"}), VBFC_ME("pyt_o_bankiera"));
    add_ask(({"stra^znik^ow","stra^z"}), VBFC_ME("pyt_o_straz"));
    set_default_answer(VBFC_ME("default_answer"));

    }

    void
    powiedz_gdy_jest(object player, string tekst)
    {
    if (environment(this_object()) == environment(player))
    this_object()->command(tekst);
    }

    string
    pyt_o_bank()
    {
    set_alarm(2.0, 0.0, "powiedz_gdy_jest", this_player(),
    "':spogl^adaj^ac na ciebie ze zdziwieniem: Jeste^s w nim "+
    "przecie^z.");
    return "";
    }

   string
   pyt_o_bankiera()
   {
   set_alarm(2.0, 0.0, "powiedz_gdy_jest", this_player(),
   "'Jest obok, wyno^s si^e.");
   return "";
   }
   string
   pyt_o_straz()
   {
   set_alarm(2.0, 0.0, "powiedz_gdy_jest", this_player(),
   "':prostuj^ac si^e dumnie: Ja jestem stra^z^a!");
   return "";
   }
   string
   default_answer()
   {
   set_alarm(2.0, 0.0, "powiedz_gdy_jest", this_player(),
       "powiedz do "+ this_player()->query_name(PL_BIE) +
       "G^owno ci do tego, wyno^s si^e.");
   return "";
   }



   void
   add_introduced(string imie_mia, string imie_bie)
   {
       set_alarm(2.0, 0.0, "return_introduce", find_player(imie_mia));
   }
   void
   return_introduce(object ob)
   {
   command("powiedz Wyno^s si^e, jestem zaj^ety.");
   }


   void
emote_hook(string emote, object wykonujacy)
{
   switch (emote)
   {
       case "kopnij": set_alarm(1.5, 0.0, "nienajlepszy", wykonujacy);
       break;
       case "spoliczkuj": set_alarm(1.5, 0.0, "nienajlepszy", wykonujacy);
                   break;
       case "opluj" : set_alarm(2.5, 0.0, "nienajlepszy", wykonujacy);
break;
case "poca^luj":
             {
             if(wykonujacy->query_gender() == 1)
               switch(random(2))
                {
                case 0:command("pocaluj "+OB_NAME(wykonujacy)); break;
                case 1:command("pocaluj "+OB_NAME(wykonujacy)); break;
                       command("powiedz Przyjd^z do mnie wieczorem."); break;
               }
             else
               {
                  switch(random(2))
                  {
                  case 0:command("emote czerwienieje ze z^lo^sci.");
                         command("zabij "+OB_NAME(wykonujacy)); break;
                  case 1:command("spojrzyj z obrzydzeniem na "+
                       OB_NAME(wykonujacy));
                         command("zabij "+OB_NAME(wykonujacy)); break;
                  }
}

               break;
               }
       case "przytul": set_alarm(2.5, 0.0, "malolepszy", wykonujacy);
                  break;
       case "pog^laszcz": set_alarm(2.5, 0.0, "malolepszy", wykonujacy);
                  break;
   }
}

void
malolepszy(object wykonujacy)
{

  switch(random(2))
  {
    case 0: command("spojrzyj krzywo na "+OB_NAME(wykonujacy)); break;
case 1: command("powiedz Nie podcieraj dupy szk^lem!"); break;
  }
}

void
nienajlepszy(object wykonujacy)
{
  switch(random(1))
  {
     case 0: command("zabij "+OB_NAME(wykonujacy)); break;
  }
}

