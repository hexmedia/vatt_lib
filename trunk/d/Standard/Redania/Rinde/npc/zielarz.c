/* Autor: Sedzik
   Opis : Yran + poprawia� Molder
   Data : 21.01.07
   Info : Zielarz, zielarzuje w zielarni

    Edit by Vera: 17.12.07
    Doda�em mu mini zadania, za kt�re p�aci.
    Zielarz ma czasem faz� na jaki� leftover lub inny tajny
    sk�adnik, kt�rego potrzebuje, gracz mu przynosi i dostaje
    troch� grosza.

    Oto lista przedmiot�w, na kt�re zielarz mo�e mie� faz�:
    leftovery: utopca, niedzwiedzia(np. z jamy), poludnic,
     zwierzat lesnych, ghoula, szczurow, ludzi(!), wilk�w
    inne: jezyny z traktu przy BM, robaki etc.
    TODO: Doda� faz� na rzeczy z moczar jak b�d�.

    No, my�l�, �e wi�cej nie trzeba t�umaczy�.
    Ofkors eventy i odpowiedzi na odpowiednie pytania ty� doda�em.

 */

#pragma unique

#include <std.h>
#include <ss_types.h>
#include <macros.h>
#include <stdproperties.h>
#include <pl.h>
#include <money.h>
#include "dir.h"
#include <object_types.h>

//w switch ustaw_faze() jest losowanie do tylu wlasnie: (liczone od zera!)
#define L_FAZ 10

#define DBG(x) find_player("vera")->catch_msg(x+"\n");

inherit RINDE_NPC_STD;
inherit "/lib/sklepikarz.c";

/*jako index imie gracza robiacego, jako value numer fazy-to czego
    chce zielarz*/
mapping robiacy = ([ ]);
/* alarm czy zielarz akurat czego� potrzebuje */
int zadanko_alarm = 0;
/*na co mamy fazk�. -1 znaczy, �e nie mamy fazki ;p */
int faza_na = -1;

/*ustawiamy na jaka faze ma zielarz

zwracamy tablice:
0 arg to nazwa jaka mowi
1 arg to rasa
2 arg to item
3 arg to cena ile dajemy za to
*/
mixed *
query_fazka(int x)
{
    mixed *co=({});

    switch(x)
    {
        //wpierw leftovery:
        case 0:
            co=({"serce utopca","utopiec","serce",33+random(24)}); break;
        case 1:
            co=({"kocie oko","kot","oko",3+random(6)}); break;
        case 2:
            co=({"kie� wilka","wilk","kie�",38+random(35)}); break;
        case 3:
            co=({"nied�wiedzi� czaszk�","nied�wied�","czaszka",41+random(25)}); break;
        case 4:
            co=({"pazur po�udnicy","po�udnica","pazur",20+random(25)}); break;
        case 5:
            co=({"ogon wiewi�rki","wiewi�rka","ogon",15+random(5)}); break;
        case 6:
            co=({"ludzkiej czaszki","cz�owiek","czaszka",17+random(42)}); break;
        case 7:
            co=({"szczura, mo�e by� martwy, oby �wie�y","szczur","cia�o",
                1+random(11)}); break;
        //koniec leftover�w, reszta:
        case 8: //skora
            co=({"sk�r� kota","kot","sk�ra",1+random(13)}); break;
        case 9: //robal
            co=({"d�d�ownic�","r�owawy","robak",1+random(3)}); break;
        case 10: //food - owoc
            co=({"owoc dzikiej r�y","","owoc",4+random(12)}); break;

        default:
            write("Wyst�pi� b��d w zielarzu (nr 4lk2mf2! Zg�o� to natychmiast, prosz�!\n");
            break;
    }
    return co;
}

void
odloz(object ob)
{
    set_alarm(1.0, 0.0, "command", "odloz " + OB_NAME(ob));
}

void
kasa_dla(object o,int ile)
{
    o->catch_msg(QCIMIE(TO, PL_MIA)+" daje ci "+MONEY_TEXT(({ile,0,0}),PL_BIE)+"\n");
        tell_room(ENV(o), QCIMIE(TO, PL_MIA)+" daje "+
            QIMIE(o, PL_CEL)+" jakie� monety.\n", ({o}));
}

void
enter_inv(object ob, object from)
{
    ::enter_inv(ob, from);

    if(!interactive(from))
        return;

//     //je�li nie mamy fazy to niczego nie przyjmujemy,
//     //chyba, ze wczesniej co juz zamowilismy u tego gracza
//     if(faza_na == -1)
//     {
//         odloz(ob);
//         return;
//     }

    if(member_array(from->query_real_name(),//je�li nie dostali�my
            m_indices(robiacy)) == -1)              // zlecenia to te�
    {
        odloz(ob);
        return;
    }

    mixed tmp = robiacy[from->query_real_name()];

    //je�li mamy faze na co innego ni� nam wciska, to te� odrzucamy
    if(ob->query_name() != tmp[2])
    {
        odloz(ob);
        return;
    }

    //a teraz to ju� r�zne r�niste wariacje,
    //sprawdzamy, czy wszystko si� zgadza.

    if(ob->query_leftover())
    {
        switch(faza_na)
        {
            case 0: //serce utopca
            case 1: //oko kota
            case 2: //kie� wilka
            case 3: //czaszka niedzwiedzia
            case 4: //pazur poludnicy
            case 5: //ogon wiewiorki

                if(ob->query_race() != tmp[1])
                {
                    odloz(ob);
                    return;
                }
                break;

            case 6: //czaszka czlowieka
                if(ob->query_race() != tmp[1] ||
                    ob->query_race() != "m�czyzna" ||
                    ob->query_race() != "kobieta")
                {
                    odloz(ob);
                    return;
                }
                break;

            case 7: //cialo szczura
                if(!ob->query_corpse() || ob->query_race() != "szczur")
                {
                    odloz(ob);
                    return;
                }
                break;

            default: //jesli to jest leftover,
                    //a mamy inna faze, to odkladamy tyz
                   odloz(ob);
                    return;
        }
    }

    if((faza_na == 8 && !ob->query_skora()) || //skora kota
        faza_na == 9 && !ob->query_dzdzownica() || //chyba oczywiste
        faza_na == 10 && !ob->query_owoc_dzikiej_rozy()) // --||--
    {
        odloz(ob);
        return;
    }

    //no, uffffff, to juz chyba przelecielismy wszystko co sie dalo!

    set_alarm(1.0, 0.0, "command_present", from, "powiedz do " +
                OB_NAME(from) + " Dzi�ki ci! W�a�nie tego potrzebowa�em!"+
                " Oto twoja nagroda.");

    m_delete(robiacy,from->query_real_name());

    if(MONEY_ADD(from,tmp[3]))
    {
        set_alarm(1.0, 0.0, "kasa_dla",from,tmp[3]);
    }

    ob->remove_me();

    return;
}

void
fazka_alarm()
{
    if(random(2))
        faza_na = random(L_FAZ);
    else
        faza_na = -1;

    set_alarm(itof(random(3000))+1.0,0.0,"fazka_alarm");
}

void
create_rinde_humanoid()
{
    dodaj_nazwy("zielarz");
    dodaj_nazwy("aptekarz");
    ustaw_odmiane_rasy("mezczyzna");
    set_gender(G_MALE);
    ustaw_imie(({"wawrzynosek","wawrzynoska","wawrzynoskowi","wawrzynoska",
        "wawrzynoskiem","wawrzynosku"}), PL_MESKI_OS);
    dodaj_przym("krzywonosy","krzywonosi");
    dodaj_przym("wysoki","wysocy");

    set_long("Wysoki, niem�ody m�czyzna bacznie, cho� z pewn� nerwowo�ci� �widruje " +
        "ci� spojrzeniem swych ciemnych oczu. Ma lekko przekrzywiony nos, co mo�e by� pami�tk� " +
        "po jakiej� m�odzie�czej b�jce, kr�tkie lecz g�ste, jasne w�osy oraz du�e i zdecydowanie " +
        "odstaj�ce uszy. Jego pomarszczona twarz i kozia br�dka jednoznacznie wskazuj� na " +
        "s�dziwy wiek. Pomimo wysokiego wzrostu jest bardzo chudy, a obszerna " +
        "ciemnozielona szata przewi�zana w pasie w�skim sznurkiem wisi na nim jak, nie " +
        "przymierzaj�c, worek po ziemniakach. Kr�j szaty jest jednak elegancki, a noszony " +
        "przez m�czyzn� z�oty naszyjnik oraz pier�cie� z b�yszcz�cym brylantem wskazuj�, " +
        "�e aptekarz jest osob� bogat� i wp�ywow�.\n");

    set_stats (({ 70, 60, 60, 35, 100 }));
    add_prop(CONT_I_WEIGHT, 70000);
    add_prop(CONT_I_HEIGHT, 185);

    set_act_time(60);
    add_act("@@eventy@@");

    set_cact_time(10);
    add_cact("krzyknij Zostaw mnie! Stra^z! Do jasnej Cholery! Stra^z!");
    add_cact("krzyknij Psia ma^c! Jeszcze jeden z^lodziej! Stra^z!");

    MONEY_MAKE_D(15)->move(this_object());
    MONEY_MAKE_G(30)->move(this_object());

    add_ask(({"ratusz"}), VBFC_ME("pyt_o_ratusz"));
    add_ask(({"^swi^atynie"}), VBFC_ME("pyt_o_swiatynie"));
    add_ask(({"swoj� prac�"}), VBFC_ME("pyt_o_swoja_prace"));
    add_ask(({"garnizon"}), VBFC_ME("pyt_o_garnizon"));
    add_ask(({"zio^la"}), VBFC_ME("pyt_o_ziola"));
    add_ask(({"burmistrza","Nevilla","nevilla"}),VBFC_ME("pyt_o_burmistrza"));
    add_ask(({"karczm^e","baszt^e","garnizonow^a"}),VBFC_ME("pyt_o_karczme"));
    add_ask(({"wawrzynoska","Wawrzynoska","krzywonosego wysokiego m^e^zczyzne",
        "krzywonosego m^e^zczyzn^e","wysokiego m^e^zczyzn^e"}),
        VBFC_ME("pyt_o_wawrzynoska"));
    add_ask("yennefer", VBFC_ME("pyt_o_yen"));
    add_ask(({"geralta","wied^xmina"}), VBFC_ME("pyt_o_geralta"));
    add_ask(({"trening", "treningi", "umiej�tno�ci", "trenowanie umiej�tno�ci"}), VBFC_ME("pyt_o_trening"));

    //do zadanek:
    add_ask(({"prac�","zadanie","misj�"}),
                VBFC_ME("pyt_o_prace"));

    set_default_answer(VBFC_ME("default_answer"));

    add_armour(RINDE_UBRANIA + "pierscienie/gustowny_zloty_Lc.c");
    add_armour(RINDE_UBRANIA + "szaty/obszerna_ciemnozielona_Lc.c");
    add_armour(RINDE_UBRANIA + "buty/zadbane_zamszowe_Lc.c");
    add_armour(RINDE_UBRANIA + "naszyjniki/elegancki_zdobiony_Lc.c");

    config_default_sklepikarz();

    set_money_greed_buy(204);
    set_money_greed_sell(511);

    set_money_greed_change(104);

    set_store_room(POLUDNIE_LOKACJE + "magazyn_apteki.c");
    set_co_skupujemy(O_ZIOLA);
    set_alarm(2.0,0.0,"fazka_alarm");
    add_prop(NPC_M_NO_ACCEPT_GIVE, 0);
}

public string
pyt_o_trening()
{
    TO->command("powiedz do " + OB_NAME(TP) + " Hmm.. Poszukaj w miejskiej bibliotece.");
    return "";
}

string
pyt_o_prace()
{
    if(!query_attack())
    {
        //sprawd�my wpierw, czy co� ju� graczowi dali�my
        if(member_array(TP->query_real_name(),m_indices(robiacy)) != -1)
        {
            set_alarm(1.0, 0.0, "command_present", TP, "powiedz do " +
                OB_NAME(TP) + " No co? Przecie� ju� ci m�wi�em, �e "+
                "pilnie potrzebuj� "+
                robiacy[TP->query_real_name()][0]+".");
            return "";
        }

        //ok, nie dalismy, wiec sprawdzmy czy mamy na cos faze.
        if(faza_na != -1)
        {
            set_alarm(1.0, 0.0, "command_present", TP, "powiedz do " +
                OB_NAME(TP) + " Hmmm... Tak, m"+TP->koncowka("�g�by�","og�aby�")+" "+
                "mi pom�c! Pilnie potrzebuj� "+
                query_fazka(faza_na)[0]+"."+
                (random(2)?" Je�li mi to dostarczysz sowicie ci� wynagrodz�.":""));

            robiacy += ([ TP->query_real_name():query_fazka(faza_na)]);

            return "";
        }
        else //nie mamy obecnie na nic fazy.
        {
            set_alarm(1.0, 0.0, "command_present", TP, "powiedz do " +
                OB_NAME(TP) + " Nie dzi�kuj�, na teraz niczego nie "+
                "potrzebuj�. Zajrzyj do mnie p�niej.");
            return "";
        }

    }
    else
    {
        set_alarm(0.5,0.0,"command_present", TP, "powiedz do "  + OB_NAME(TP) + " Zostaw mnie w "+
            "spokoju!");
    }
    return "";
}

string
pyt_o_ratusz()
{
    if(!query_attack())
    {
        set_alarm(0.5, 0.0, "command_present", TP,
        "powiedz do "  + OB_NAME(TP) + " Jest w centrum miasta, nie spos^ob nie trafi^c.");
    }
    else
    {
        set_alarm(0.5,0.0,"command_present", TP, "powiedz do "  + OB_NAME(TP) + " Zostaw mnie w "+
            "spokoju!");
    }
    return "";
}
string
pyt_o_swiatynie()
{
    if(!query_attack())
    {
        set_alarm(0.5, 0.0, "command_present", TP,
        "powiedz do "  + OB_NAME(TP) + " To du^za budowla z wie^z^a na zachodzie miasta.");
    }
    else
    {
        set_alarm(0.5,0.0,"command_present", TP, "powiedz do "  + OB_NAME(TP) + " Zostaw mnie w "+
            "spokoju!");
    }
    return "";
}
string
pyt_o_swoja_prace()
{
    if(!query_attack())
    {
        set_alarm(0.5, 0.0, "command_present", TP,
            "powiedz do "  + OB_NAME(TP) + " O tak, bycie zielarzem to ci^e^zka praca, ale "+
            "to r^ownie^z moja pasja.");
    }
    else
    {
        set_alarm(0.5,0.0,"command_present", TP, "powiedz do "  + OB_NAME(TP) + " Zostaw mnie w "+
            "spokoju!");
    }
    return "";
}

string
pyt_o_garnizon()
{
    if(!query_attack())
    {
        set_alarm(0.5, 0.0, "command_present", TP,
            "powiedz do "  + OB_NAME(TP) + " Garnizon jest we wschodniej cz^e^sci miasta.");
    }
    else
    {
        set_alarm(0.5,0.0,"command_present", TP, "powiedz do "  + OB_NAME(TP) + " Zostaw mnie w "+
            "spokoju!");
    }
    return "";
}

string
pyt_o_burmistrza()
{
    if(!query_attack())
    {
        set_alarm(0.5, 0.0, "command_present", TP,
            "powiedz do "  + OB_NAME(TP) + " Neville nie jest z^lym burmistrzem, mo^zesz "+
            "go spotka^c w ratuszu.");
    }
    else
    {
        set_alarm(0.5,0.0,"command_present", TP, "powiedz do "  + OB_NAME(TP) + " Zostaw mnie w "+
            "spokoju!");
    }
    return "";
}

string
default_answer()
{
    set_alarm(2.0, 0.0, "command_present", TP,
        "powiedz do "  + OB_NAME(TP) + " Przykro mi, ale na ten temat niczego ci nie powiem.");
    return "";
}

string
pyt_o_karczme()
{
    if(!query_attack())
    {
        set_alarm(0.5, 0.0, "command_present", TP,
            "powiedz do "  + OB_NAME(TP) + " Je^sli chodzi o karczmy to polecam Baszt^e, jest "+
            "na p^o^lnocy miasta, prosta droga od ratusza.");
    }
    else
    {
         set_alarm(0.5,0.0,"command_present", TP, "powiedz do "  + OB_NAME(TP) + " Zostaw mnie w "+
            "spokoju!");
    }
    return "";
}
string
pyt_o_wawrzynoska()
{
    if(!query_attack())
    {
        set_alarm(0.5, 0.0, "command_present", TP,
            "powiedz do "  + OB_NAME(TP) + " To ja, o co chodzi?");
        return "";
    }
    else
    {
        set_alarm(0.5,0.0,"command_present", TP, "powiedz do "  + OB_NAME(TP) + " Zostaw mnie w "+
            "spokoju!");
        return "";
    }
}

string
pyt_o_ziola()
{
    if(!query_attack())
    {
        set_alarm(0.5, 0.0, "command_present", TP,
            "powiedz do "  + OB_NAME(TP) + " Wszystko co mam do sprzedania jest wystawione.");
        return "";
    }
    else
    {
        set_alarm(0.5,0.0,"command_present", TP, "powiedz do "  + OB_NAME(TP) + " Zostaw mnie w "+
            "spokoju!");
        return "";
    }
}

string
pyt_o_yen()
{
    if(!query_attack())
    {
        set_alarm(0.5, 0.0, "command_present", TP,
            "powiedz do "  + OB_NAME(TP) + " Ta wied^xma? Gdzie ona jest? Za to, co zrobi^la czeka j^a pal.");
        set_alarm(0.5, 0.0, "command_present", TP,
            "usmiech msciwie");
        return "";
    }
    else
    {
        set_alarm(0.5,0.0,"command_present", TP, "powiedz do "  + OB_NAME(TP) + " Zostaw mnie w "+
            "spokoju!");
        return "";
    }
}

string
pyt_o_geralta()
{
    if(!query_attack())
    {
        set_alarm(0.5, 0.0, "command_present", TP,
            "powiedz do "  + OB_NAME(TP) + " Wied^xmin Geralt? Ten zadufany psubrat? Kochanek tej "+
            "kurwy Yennefer? Powiedz mi, gdzie jest, a ka^z^e go powiesi^c.");
        set_alarm(0.5, 0.0, "command_present", TP,
            "usmiech msciwie");
        return "";
    }
    else
    {
        set_alarm(0.5,0.0,"command_present", TP, "powiedz do "  + OB_NAME(TP) + " Zostaw mnie w "+
            "spokoju!");
        return "";
    }
}

void
add_introduced(string imie_mia, string imie_bie)
{
    set_alarm(2.0, 0.0, "return_introduce", find_player(imie_mia));
}

void
return_introduce(object ob)
{
    command("przedstaw sie "+ OB_NAME(ob));
}

void
emote_hook(string emote, object wykonujacy)
{
    switch (emote)
    {
        case "kopnij":
        case "spoliczkuj":
        case "opluj":
            switch(random(2))
            {
                case 0:
                    set_alarm(1.5, 0.0, "command_present", TP,
                        "powiedz do "  + OB_NAME(wykonujacy) + " Wyno^s si^e kmiocie!");
                    break;

                case 1:
                    set_alarm(1.5, 0.0, "command_present", TP,
                        "powiedz do "  + OB_NAME(wykonujacy) + " Czego? Zreszt^a, nie b^ed^e rozmawia^l "+
                        "z takim chamem.");
                    break;
            }
            break;

        case "poca^luj":
            if(wykonujacy->query_gender() == 1)
                set_alarm(1.5, 0.0, "command_present", TP,
                    "emote u^smiecha si^e mi^lo.");
            else
                set_alarm(1.5, 0.0, "command_present", TP,
                    "powiedz do "  + OB_NAME(wykonujacy) + " Uznam to za przyjacielski gest.");
            break;

        case "prychnij":
            set_alarm(1.5, 0.0, "command_present", TP,
                "spojrzyj lekcewazaco na " + OB_NAME(wykonujacy));
            break;

        case "przytul":
            set_alarm(1.5, 0.0, "command_present", this_player(),
                "powiedz do "  + OB_NAME(wykonujacy) + " Dzi^ekuje, to mi^le.");
            break;
    }
}

void
init()
{
    ::init();
    init_sklepikarz();
}

