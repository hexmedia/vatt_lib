inherit "/std/zwierze.c";
inherit "/std/act/action.c";
inherit "/std/act/trigaction.c";

#include <macros.h>
#include <stdproperties.h>
#include <money.h>
#include <pl.h>
#include <formulas.h>
#include <std.h>
#include <ss_types.h>
#include <wa_types.h>
#include <composite.h>
#include <filter_funs.h>
#include <mudtime.h>
#include <object_types.h>
#include "dir.h"

void
create_zwierze()
{
    add_leftover("/std/skora", "sk�ra", 1, 0, 1, 1, 3 + random(5), O_SKORY);
    add_leftover("/std/leftover", "z^ab", 1, 0, 1, 1, 0, O_KOSCI);

    ustaw_odmiane_rasy("pies");
    set_gender(G_MALE);

    set_long("Pies, a raczej szczeniak nie si�ga ci nawet do �ydki. " +
        "Male�kie zwierz� wygl�da raczej jak pi�ka, kt�ra skacze ci�gle " +
        "przez nikogo nie ruszona. Piesek biega nieustannie dooko�a " +
        "twoich n�g szczekaj�c zawzi�cie i merdaj�c energicznie ogonem. " +
        "Czarna sier�� jest niezwykle puszysta i mi�kka w dotyku, a du�e, " +
        "br�zowe oczy �ypi� na wszystkie strony.\n");

    dodaj_przym("ma�y", "mali");
    dodaj_przym("ha�a�liwy", "ha�a�liwi");

    set_act_time(30);
    add_act("emote szczeka zawzi�cie.");
    add_act("emote biega pomi�dzy twoimi nogami.");
    add_act("emote merda energicznie ogonem.");
    add_act("emote biega w k�ko.");
    add_act("emote warczy na sw�j cie�.");

    set_stats ( ({ 65, 31, 70, 25, 58 }) );

    set_skill(SS_DEFENCE, 10 + random(4));
    set_skill(SS_WEP_CLUB, 33 + random(10));
    set_skill(SS_PARRY, 10 + random(10));
    set_skill(SS_UNARM_COMBAT, 15 + random(5));

    set_attack_unarmed(0,   10,  10 , W_IMPALE, 100, "z�by");

    set_hitloc_unarmed(0, ({ 0, 0, 5 }), 20, "g�ow�");
    set_hitloc_unarmed(1, ({ 0, 0, 5 }), 80, "brzuch");

    add_prop(LIVE_I_NEVERKNOWN, 1);
//    add_prop(CONT_I_WEIGHT, 91000);
//    add_prop(CONT_I_HEIGHT, 172);
    /*set_random_move(100);
    set_monster_home(POLNOC+"Karczma/lokacje/podworze");
    set_restrain_path(({POLNOC+"Karczma/lokacje/stajnia",
                        POLNOC_LOKACJE+"ulica6",
                        WSCHOD_LOKACJE+"ulica1",
                        WSCHOD_LOKACJE+"ulica3",
                        WSCHOD_LOKACJE+"ulica2"}));*/
    //set_alarm_every_hour("co_godzine");
}
/*
void
co_godzine()
{
    if(environment(this_object())->pora_roku() == MT_LATO||
       environment(this_object())->pora_roku() == MT_WIOSNA)
    {
        if (MT_GODZINA == 21)
            set_alarm(4.0,0.0, "destroooy");
    }
    else
    {
        if (MT_GODZINA == 18)
            set_alarm(3.0,0.0, "destroooy");
    }
}
void
destroooy()
{
    tell_roombb(environment(), this_object()->short(PL_MIA)+" czmycha "+
        "gdzie� w kierunku stajni.\n");
        remove_object();
}*/

void
emote_hook(string emote, object wykonujacy, string przyslowek)
{
    switch (emote)
    {
        case "kopnij":
        case "spoliczkuj":
        case "opluj":
	case "nadepnij":
	    set_alarm(1.0, 0.0, "zly", wykonujacy);
            break;

        case "szturchnij":
	    set_alarm(1.0, 0.0, "taki_sobie", wykonujacy);
            break;

        case "za�miej":
        case "poca�uj":
        case "przytul":
        case "pog�aszcz":
	    set_alarm(1.0, 0.0, "dobry", wykonujacy);
            break;
    }
}

void
zly(object kto)
{
    command("emote warczy gro�nie.");
}

void
taki_sobie(object kto)
{
}

void
dobry(object kto)
{
    command("emote macha weso�o ogonem.");
}

void
attacked_by(object wrog)
{
    ::attacked_by(wrog);
}
