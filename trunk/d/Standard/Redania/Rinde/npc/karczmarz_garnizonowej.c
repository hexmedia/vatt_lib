/* Przerobi� Vera.
 */

#pragma unique

#include <macros.h>
#include <stdproperties.h>
#include <money.h>
#include <pl.h>
#include <formulas.h>
#include <std.h>
#include <ss_types.h>
#include <wa_types.h>
#include <composite.h>
#include <filter_funs.h>
#include "dir.h"

inherit RINDE_NPC_STD;

#define HACZYKOWY "/d/Standard/Redania/Piana/npc/krawiec"
#define ZROBILI "/d/Standard/questy/utopce/zrobili"
#define ROBIACY "/d/Standard/questy/utopce/robiacy"
#define MEDALION "/d/Standard/Redania/Piana/quest/medalion"
#define WZIELI "/d/Standard/questy/utopce/wzieli"
#define WZIELI_LIST "/d/Standard/questy/utopce/wzielil"
#define LIST "/d/Standard/Redania/Piana/quest/list"

int czy_walka();
int walka = 0;
object bron;

void
create_rinde_humanoid()
{
    set_living_name("gross");

    ustaw_imie( ({"gross", "grossa", "grossowi", "grossa", "grossem",
	"grossie"}), PL_MESKI_OS );
    set_surname("Margenor");

    ustaw_odmiane_rasy("m�czyzna");
    dodaj_nazwy("karczmarz");
    set_gender(G_MALE);

    set_long("Osoba na kt�r� obecnie spogl�dasz jest bez w�tpienia " +
	"tutejszym karczmarzem. M�czyzna ten cho� w podesz�ym ju� wieku, " +
	"ci�gle bez najmniejszych problem�w obs�uguje swoich klient�w " +
	"tylko czasami wyr�czaj�c si� kelnerkami. Jego kasztanowe w�osy " +
	"s� w poniekt�rych miejscach przypr�szone odrobin� siwizny, kt�rej " +
	"nie zwalcza chyba tylko dlatego �eby posiada� wi�ksze powa�anie. " +
	"Jego twarz dok�adnie w po�owie przecina w�ska blizna, " +
	"prawdopodobnie pami�tka z zamierzch�ych lat m�odo�ci. Wci�� " +
	"rozgl�da si� po karczmie u�miechaj�c si� do klient�w.\n");

    dodaj_przym("niski", "niscy");
    dodaj_przym("zgrabny", "zgrabni");

    set_chat_time(30);
    add_chat("Witaj u mnie!");
    add_chat("Mo�e skosztujesz naszej specjalno�ci?");
    add_chat("Eh te elfy ch�do�one!");
    add_chat("Ch�tnie popatrzy�bym, jak elfa wbijaj� na pal.");

    set_act_time(40);
    add_act("emote rozgl�da si� woko�o sprawdzaj�c czy wszyscy klienci " +
	"s� nale�ycie traktowani.");
    add_act("emote narzeka na sw�j los.");
    add_act("emote czy�ci swoje ubranie.");
    add_act("emote przysypia na chwil� przy stole po czym budzi si� " +
	"wykrzykuj�c co� niezrozumia�ego o wojnie.");

    set_default_answer(VBFC_ME("default_answer"));

    set_stats ( ({ 85, 31, 80, 25, 78 }) );

    set_skill(SS_DEFENCE, 10 + random(4));
    set_skill(SS_WEP_CLUB, 33 + random(10));
    set_skill(SS_PARRY, 10 + random(10));
    set_skill(SS_UNARM_COMBAT, 15 + random(5));

    add_armour(RINDE_UBRANIA + "fartuch_prosty_szary");
    add_armour(RINDE_UBRANIA + "spodnie_szare_proste");
    add_armour(RINDE_UBRANIA + "buty_blyszczace_skorzane");

//    add_prop(CONT_I_WEIGHT, 91000);
//    add_prop(CONT_I_HEIGHT, 172);
    //Vera.
    add_ask(({"specja�", "specjalno��", "danie specjalne"}),
            VBFC_ME("pyt_o_specjalnosc"));
    add_ask(({"elfy", "szyszki", "elf�w","elfa","w�skodupce"}),
            VBFC_ME("pyt_o_elfy"));
	add_ask(({"wysokiego brodatego m^e^zczyzn^e", "brodatego wysokiego m^e^zczyzn^e",
    "wysokiego brodatego cz^lowieka", "brodatego wysokiego cz^lowieka",
    "diabelnie wysokiego m^e^zczyzn^e", "bardzo wysokiego m^e^zczyzn^e",
    "diabelnie wysokiego brodatego m^e^zczyzn^e", "bardzo wysokiego brodatego m^e^zczyzn^e",
    "diabelnie wysokiego cz^lowieka", "bardzo wysokiego cz^lowieka",
    "diabelnie wysokiego brodatego cz^lowieka", "bardzo wysokiego brodatego cz^lowieka",
    "milona"}),VBFC_ME("pyt_milon"));
}

int
query_karczmarz()
{
	return 1;
}

void
powiedz_gdy_jest(object player, string tekst)
{
    if ((environment(this_object()) == environment(player)) && CAN_SEE(this_object(),
    player))
	this_object()->command(tekst);
	else
    this_object()->command("wzrusz ramionami");
}

string
pyt_o_specjalnosc()
{
    set_alarm(2.0, 0.0, "command_present", this_player(),
	"powiedz do "+this_player()->query_name(PL_DOP) + " Mamy najlepsz� "+
	"w�tr�bk�!");
    return "";
}
string
pyt_o_elfy()
{
    set_alarm(2.0, 0.0, "command_present", this_player(),
	"powiedz do "+this_player()->query_name(PL_DOP) +
	" Cieeeee, choroba! Co elfy? Co elfy u licha? W rzyci ich mam "+
	"psiekrwie jebane!");
    return "";
}

string
pyt_milon()
{
    if (TP->query_prop("pytal_karczmarza_gar_o_milona_do_cholery") == 0)
    {
         TP->add_prop("pytal_karczmarza_gar_o_milona_do_cholery", 1);
	    set_alarm(2.0, 0.0, "powiedz_gdy_jest", this_player(),
    	"powiedz do " + OB_NAME(TP) + " By^l tu taki niedawno, ale nie mia^l " +
        "pieni^edzy na nocleg. Mo^ze siedzi w innej");
    	set_alarm(4.0, 0.0, "powiedz_gdy_jest", this_player(),
    	"wzrusz ramionami");
    }
    else
        set_alarm(2.0, 0.0, "powiedz_gdy_jest", this_player(),
    	"powiedz do " + OB_NAME(TP) + " M^owi^lem ci ju^z, tu go nie ma");

    return "";
}

string
default_answer()
{
     return "";
}

void
return_introduce(string imie)
{
    object osoba = present(imie, environment());

    if(osoba->query_race() == "elf" || osoba->query_race()=="p�elf")
    {
    }
    else if(osoba)
    {
        command("przedstaw sie " + OB_NAME(osoba));
    }
}

void
emote_hook(string emote, object wykonujacy, string przyslowek)
{
    switch (emote)
    {
        case "kopnij":
        case "spoliczkuj":
        case "opluj":
	case "nadepnij": set_alarm(2.0, 0.0, "zly", wykonujacy);
            break;

        case "poca�uj":
        case "przytul":
        case "pog�aszcz": set_alarm(2.0, 0.0, "dobry", wykonujacy);
            break;
    }
}

void
zly(object kto)
{
    if(kto->query_race() == "elf")
    {
        command("powiedz Spierdalaj d�ugouchy!");
        environment(this_object())->wywalamy(kto);
    }
    else
    {
        switch(random(4))
        {
            case 0: command("powiedz Ch�do� si� durniu."); break;
            case 1: command("zarechocz"); break;
            case 2: command("powiedz Ale z ciebie b�azen."); break;
            case 3: command("powiedz Dosy� tego!");
                    environment(this_object())->wypad(kto);
                    break;
        }
    }


}
void
dobry(object kto)
{
    if(kto->koncowka("chlop","baba")=="chlop")
       zly(kto);
    else
      {
        switch (random(4))
        {
          case 0 : command("klepnij " +OB_NAME(kto)+" w posladek");
                        break;
          case 1 : command("powiedz Hehe, a dupy mi te� dasz?"); break;
          case 2 : set_alarm(2.0, 0.0, "command_present", kto, "powiedz do " +OB_NAME(kto) +
                                        " Mrau, kotku, mo�e si� zabawimy co?");
                   command("obliz sie oblesnie");
                   break;
          case 3: command("obliz sie oblesnie");break;
        }
      }
}

void
attacked_by(object wrog)
{
    if(walka==0) {
    command("emote dobywa szerokiego por�cznego miecza.");
    bron = add_weapon(RINDE_BRONIE + "miecze/miecz_szeroki_poreczny");
    set_alarm(4.0, 0.0, "command", "krzyknij Stra�! Pomocy! Psia ma�!");
    set_alarm(15.0, 0.0, "czy_walka", 1);
    walka = 1;
    return ::attacked_by(wrog); }

    else if(walka == 1)
    {
      set_alarm(7.0, 0.0, "command", "krzyknij Straz! Pomocy! Psia ma�!");
      set_alarm(15.0, 0.0, "czy_walka", 1);
      walka = 1;
      return ::attacked_by(wrog);
    }
}

int
czy_walka()
{
    if(!query_attack())
    {
       this_object()->command("opusc bron");
       bron->remove_object();
       this_object()->command("powiedz Psie krwie!");
       this_object()->command("splun");
       walka = 0;
       return 1;
    }
    else
    {
       set_alarm(15.0, 0.0, "czy_walka", 1);
    }
}

public void
do_die(object killer)
{
        command("powiedz O ty " +
			 this_player()->koncowka("kurwi synu", "kurwo") +
			 ".");
        ::do_die(killer);
}