/* Karczmarz "Baszty" w Rinde.
   Fragment operacji "Karczma startowa"
            by Lil 01.05               */

#pragma unique
#pragma strict types

#include <macros.h>
#include <stdproperties.h>
#include <money.h>
#include <pl.h>
#include <formulas.h>
#include <std.h>
#include <ss_types.h>
#include <wa_types.h>
#include <mudtime.h>
#include <composite.h>
#include <filter_funs.h>
#include "dir.h"
#define SIT_SIEDZACY "_sit_siedzacy"

inherit RINDE_NPC_STD;

int czy_walka();
int walka = 0;
int blok(string str);
object palka;

void
create_rinde_humanoid()
{
    set_living_name("damir");

    ustaw_odmiane_rasy("m�czyzna");

    dodaj_nazwy("karczmarz");

    set_gender(G_MALE);
    set_title(", Karczmarz");

    ustaw_imie(({"damir","damira","damirowi","damira",
        "damirem","damirze"}),PL_MESKI_OS);

    set_long("Lekko zaczerwieniony na licach, cho� pr�dzej od zm�czenia, "+
        "ni�li jakich trunk�w spo�ywania. Odziany jest w szar� "+
        "koszul�, lu�ne spodnie oraz d�ugi poplamiony fartuch, "+
        "kt�ry niegdy� m�g� by� bia�y, lecz teraz przybiera "+
        "jedynie odcienie ��ci z ciemniejszymi plamami w "+
        "kilku miejscach. Jego oblicze mieni si� od potu, dlatego "+
        "te� nader cz�sto przeciera je noszona za pasem szmat�, "+
        "czy ramieniem, gdy d�onie jego zaj�te. A zaj�� ma "+
        "sporo - biega za kontuarem niczym oszala�y, czy�ci "+
        "z czci� ka�de naczynie, polewa do kufli i kieliszk�w, "+
        "zdarzy si�, �e rzuci jakie zdawkowe has�o, li to rozkaz "+
        "w stron� kuchni, a w wolnej chwili, je�li si� takowa "+
        "nadarzy - wys�uchuje cierpliwie zwierze� tych co bardziej "+
        "upojonych trunkami klient�w.\n");

    dodaj_przym("pulchny","pulchni");
    dodaj_przym("spocony","spoceni");

    set_act_time(30);
    add_act("emote wyciera szklanki.");
    add_act("emote ociera pot z czo�a.");
    add_act("emote kr�ci si� z przej�ciem po karczmie.");
    add_act("popatrz nieufnie na elfke");
    add_act("emote rozgl�da si� po klienteli.");
    add_act("emote przeciera kufle.");
    add_act("emote wzdycha ci�ko.");
    add_act("emote m�wi co� w kierunku kuchni.");
    add_act("zagwizdz cicho");
    add_act("otrzyj pot z czola");
    add_act("popatrz nieufnie na elfa");
    add_act("skrzyw sie lekko na elfa");
    add_act("emote mamrocze co� do siebie pod nosem.");

    add_ask(({"pomoc", "prac�", "zadanie"}),VBFC_ME("pyt_o_pomoc"));
    add_ask("plecy",VBFC_ME("pyt_o_plecy"));
    add_ask("pogod�", VBFC_ME("pyt_o_pogode"));
    add_ask(({"nocleg", "miejsce"}),VBFC_ME("pyt_o_nocleg"));
    add_ask(({"menu", "jad�o", "jedzenie", "strawe"}),VBFC_ME("pyt_o_menu"));
    add_ask(({"karczm�", "gospod�", "baszt�"}),VBFC_ME("pyt_o_karczme"));
    add_ask(({"obraz", "kobiet� na obrazie",
                     "portret"}),VBFC_ME("pyt_o_obraz"));
    add_ask(({"specjalno�� dnia", "danie dnia",
                     "potraw� dnia"}),VBFC_ME("pyt_o_danie_dnia"));
    add_ask(({"�on�", "dzieci", "ojca", "rodzin�"}),VBFC_ME("pyt_o_rodzine"));
    set_default_answer(VBFC_ME("default_answer"));

    set_cchat_time(15);
    add_cchat("Zaraza!");
    add_cchat("Nielicz, �e ujdzie ci to p�azem!");

    set_stats ( ({ 65, 31, 70, 25, 58 }) );
    set_skill(SS_DEFENCE, 10 + random(4));
    set_skill(SS_WEP_CLUB, 33 + random(10));
    set_skill(SS_PARRY, 10 + random(10));
    set_skill(SS_UNARM_COMBAT, 15 + random(5));
    add_armour("/d/Standard/items/ubrania/fartuchy/losowy_rzemieslniczy.c");
    add_armour(RINDE_UBRANIA + "spodnie_los_los_biedaka.c");

    add_prop(NPC_I_NO_RUN_AWAY, 1);
    add_prop(CONT_I_WEIGHT, 91000);
    add_prop(CONT_I_HEIGHT, 172);

    set_alarm_every_hour("czy_dorzucac_do_kominka_al");
    set_alarm(100.0,0.0,"czyscimy_naczynka_al");
}

int
query_karczmarz()
{
    return 1;
}

void
czy_dorzucac_do_kominka_al()
{
    switch(random(5))
    {
        case 0: set_alarm(5.0,0.0,"czy_dorzucac_do_kominka"); break;
        case 0: set_alarm(50.0,0.0,"czy_dorzucac_do_kominka"); break;
        case 0: set_alarm(120.0,0.0,"czy_dorzucac_do_kominka"); break;
        case 0: set_alarm(300.0,0.0,"czy_dorzucac_do_kominka"); break;
        case 0: set_alarm(488.0,0.0,"czy_dorzucac_do_kominka"); break;
    }
}

int
czy_dorzucac_do_kominka()
{
    object *kominek;
    kominek=filter(all_inventory(environment(this_object())),
             &operator(!=)(0,) @ &->jestem_komineczkiem_w_karczemce());


    if((environment(TO)->pora_roku()==MT_LATO) && environment(TO)->dzien_noc())
    {
        switch(random(7))
        {
            case 0..3: break;
            case 4..6: command("emote dorzuca troch� drewna do kominka.");
                       kominek->palimy(); break;
        }
    }
    else if((environment(TO)->pora_roku()==MT_JESIEN) &&
             environment(TO)->dzien_noc())
    {
        switch(random(7))
        {
            case 0..2: break;
            case 3..6: command("emote dorzuca troch� drewna do kominka.");
                       kominek->palimy(); break;
        }
    }
    else if((environment(TO)->pora_roku()==MT_ZIMA))
    {
        switch(random(7))
        {
            case 0..1: break;
            case 2..6: command("emote dorzuca troch� drewna do kominka.");
                       saybb("Kominek zaczyna p�on�� i powoli "+
                             "ogrzewa� powietrze.\n");
                       kominek->palimy(); set_alarm(500.0,0.0,
                       "czy_dorzucac_do_kominka"); break;
        }
    }
    else if((environment(TO)->pora_roku()==MT_WIOSNA) &&
             environment(TO)->dzien_noc())
    {
        switch(random(7))
        {
            case 0..4: break;
            case 5..6: command("emote dorzuca troch� drewna do kominka.");
                       kominek->palimy(); break;
        }
    }

    return 1;
}

public void
init()
{
     ::init();

     add_action("blok", "", 1);
}

int
blok(string str)
{
    string verb = query_verb();

    if(!this_player()->query_pomagacz())
         string *nie_dozwolone = ({"kuchnia","polnoc","p�noc"});

    if(!sizeof(FILTER_IS_SEEN(TP,({TO}))))
        return 0;

    if(member_array(verb,nie_dozwolone) == -1)
        return 0;

        switch (random(4))
        {
            case 0:
                write("Karczmarz zastawia ci drog�.\n");
                saybb("Karczmarz zastawia drog� " +
                    QIMIE(this_player(),PL_CEL) + " do kuchni.\n");
                set_alarm(2.0, 0.0, "command_present", this_player(),
                   "powiedz do "+OB_NAME(TP) +
                   " A dok�d to?");
                break;

            case 1:
                write("Karczmarz zastawia ci drog�.\n");
                saybb("Karczmarz zastawia drog� " +
                    QIMIE(this_player(),PL_CEL) + " do kuchni.\n");
                set_alarm(1.0, 0.0, "command_present", this_player(),
                    "powiedz do "+OB_NAME(TP) +
                    " Przykro mi, wst�p tylko dla personelu.");
                break;

            case 2:
                write("Karczmarz zastawia ci drog�.\n");
                saybb("Karczmarz zastawia drog� " +
                    QIMIE(this_player(),PL_CEL) + " do kuchni.\n");
                set_alarm(1.0, 0.0, "command_present", this_player(),
                    "powiedz do "+OB_NAME(TP) +
                    " Odch�do� si� od mojej kuchni!");
                break;

            case 3:
                command("':z oburzeniem: Sam" +
                    this_player()->koncowka("", "a") +
                    " si� tego prosi�" + this_player()->koncowka("e�","a�") + "!\n");
                command("otworz drzwi");
                command("otworz drzwi");
                write("Karczmarz wo�a pr�dko swego pomocnika i wraz z nim, "+
                    "jak i pa�k� dzier�on� w d�oni wypraszaj� ci� "+
                    "z karczmy.\n");
                saybb("Karczmarz wo�a pr�dko swego pomocnika i wraz z nim, "+
                    "jak i pa�k� dzier�on� w d�oni wypraszaj� "+
                    QIMIE(this_player(),PL_BIE) + " z karczmy.\n");

                this_player()->reduce_hp(0, 3000 + random(1000), 10);

                tell_room(POLNOC_LOKACJE + "ulica4", "Z karczmy w�a�nie "+
                    "wyrzucono " + QIMIE(this_player(),PL_BIE) + ".\n");
                this_player()->remove_prop(SIT_SIEDZACY);
                this_player()->remove_prop(OBJ_I_DONT_GLANCE);
                this_player()->move_living("M",POLNOC_LOKACJE+"ulica4",1,0);
                break;
    }

     return 1;
}

string
pyt_o_pomoc()
{
    set_alarm(2.0, 0.0, "command_present", this_player(),
        "powiedz do "+OB_NAME(TP) + " Bol� mnie plecy.");
    return "";
}

string
pyt_o_plecy()
{
    set_alarm(2.0, 0.0, "command_present", this_player(),
        "powiedz do "+OB_NAME(TP) + " No! Bol� jak cholera!");
    set_alarm(2.5, 0.0, "command_present", this_player(),
        "zarechocz");
    return "";
}

string
pyt_o_pogode()
{
    set_alarm(2.0, 0.0, "command_present", this_player(), "powiedz do "+
        OB_NAME(TP) + " Wyjd� na zewn�trz i sprawd�.");
    return "";
}

string
pyt_o_nocleg()
{
    set_alarm(2.0, 0.0, "command_present", this_player(), "powiedz do "+
        OB_NAME(TP) + " Naturalnie, znajdzie si� miejsce.");
    return "";
}

string
pyt_o_menu()
{
    command("wskaz na menu");
    set_alarm(2.0, 0.0, "command_present", this_player(), "powiedz do "+
        OB_NAME(TP) + " Polecam nasz� specjalno��! Bigos! He, he.");
    return "";
}

string
pyt_o_karczme()
{
    set_alarm(2.0, 0.0, "command_present", this_player(), "powiedz do "+
        OB_NAME(TP) + " Karczm� t� otworzy� m�j ojciec, a ja po nim przej��em.");
    return "";
}

string
pyt_o_obraz()
{
    set_alarm(1.0, 0.0, "command", "westchnij cicho");
    set_alarm(2.0, 0.0, "command_present", this_player(), "powiedz do "+
        OB_NAME(TP) + " Masz na my�li t� pi�kn� kobiet� "+
        "na obrazie na pi�trze? To moja by�a �ona. Niech jej ziemia "+
        "lekk� b�dzie...");
    return "";
}

string
pyt_o_danie_dnia()
{
    set_alarm(2.0, 0.0, "command_present", this_player(), "powiedz do "+
        OB_NAME(TP) + " Robi� najlepszy bigos w ca�ej Redanii!");
    return "";
}

string
pyt_o_rodzine()
{
    set_alarm(2.0, 0.0, "command_present", this_player(), "powiedz do "+
        OB_NAME(TP) + " Nie tw�j to interes!");
    return "";
}

void
return_introduce(string imie)
{
    object osoba;
    osoba = present(imie, environment());

    if(osoba->query_race() == "elf")
    {
        command("przedstaw sie " + osoba->query_nazwa(PL_CEL));
        command("szepnij " + osoba->query_nazwa(PL_CEL) +
            " Usi�d� sobie w k�cie, coby ci� go�cie nie widzieli.");
        command("emote wskazuje odleg�y k�t sali.");
              return;
    }

    if(osoba)
    {
        command("przedstaw sie " + osoba->query_nazwa(PL_CEL));
        command("powiedz do "+ OB_NAME(TP) + " Witaj w mej "+
            "ober�y.");
    }
}
string
default_answer()
{
     set_alarm(2.0, 0.0, "command_present", this_player(),
        "powiedz do " + OB_NAME(TP) + " Przykro mi. Pom�g� bym ci, ale teraz nie mam czasu.");
     return "";
}

void
emote_hook(string emote, object wykonujacy, string przyslowek)
{
    switch(emote)
    {
        case "kopnij":
            set_alarm(2.0, 0.0, "zly", wykonujacy);
            break;

        case "spoliczkuj":
            set_alarm(2.0, 0.0, "zly", wykonujacy);
            break;

        case "opluj":
            set_alarm(2.0, 0.0, "zly", wykonujacy);
            break;

        case "szturchnij":
            set_alarm(2.0, 0.0, "taki_sobie", wykonujacy);
            break;

        case "zasmiej":
            set_alarm(2.0, 0.0, "dobry", wykonujacy);
            break;

        case "pocaluj":
            set_alarm(2.0, 0.0, "dobry", wykonujacy);
            break;

        case "przytul":
            set_alarm(2.0, 0.0, "dobry", wykonujacy);
            break;

        case "poglaszcz":
            set_alarm(2.0, 0.0, "dobry", wykonujacy);
            break;
    }
}

void
zly(object kto)
{
    if(kto->query_race() == "elf")
    {
        command("':z oburzeniem: Psia jucha! Nie b�dzie "+
            "mn� odmieniec pomiata�!");

        command("otworz drzwi");command("otworz drzwi");

        write("Karczmarz wo�a pr�dko swego pomocnika i wraz z nim, "+
            "jak i pa�k� dzier�on� w d�oni wypraszaj� ci� "+
            "z karczmy.\n");

        saybb("Karczmarz wo�a pr�dko swego pomocnika i wraz z nim, "+
            "jak i pa�k� dzier�on� w d�oni wypraszaj� "+
            QCIMIE(kto,PL_BIE) + " z karczmy.\n");

        kto->reduce_hp(0, 3000 + random(1000), 10);

        tell_room(POLNOC_LOKACJE + "ulica4", "Z karczmy w�a�nie "+
            "wyrzucono " + QIMIE(kto,PL_BIE) + ".\n");

        kto->remove_prop(SIT_SIEDZACY);
        kto->remove_prop(OBJ_I_DONT_GLANCE);
        kto->move_living("M", POLNOC_LOKACJE + "ulica4", 1, 0);

        return;
    }

    switch(random(7))
    {
        case 0:
            set_alarm(2.0, 0.0, "command_present", kto, "powiedz do "+OB_NAME(kto) + " Pies ci mord� liza�, chamie!");
            break;

        case 1:
            set_alarm(2.0, 0.0, "command_present", kto, "powiedz do "+OB_NAME(kto) + " Pies ci mord� liza�!");
            break;

        case 2:
            command("opluj " + OB_NAME(kto)); break;

        case 3:
            command("emote wskazuje w kierunku wyj�cia."); break;

        case 4:
            command("emote klnie siarczy�cie."); break;

        case 5:
            set_alarm(2.0, 0.0, "command_present", kto, "powiedz do "+OB_NAME(kto) + " Dosy� tego!");
            command("otworz drzwi");command("otworz drzwi");
            write("Karczmarz wo�a pr�dko swego pomocnika i wraz z nim, "+
                "jak i pa�k� dzier�on� w d�oni 'delikatnie' wypraszaj� ci� "+
                "z karczmy.\n");
            saybb("Karczmarz wo�a pr�dko swego pomocnika i wraz z nim, jak i pa�k� dzier�on� w d�oni 'delikatnie' wypraszaj� " +
                QCIMIE(kto,PL_BIE) + " z karczmy.\n");

            TP->reduce_hp(0, 3000 + random(1000), 10);

            tell_room(POLNOC_LOKACJE + "ulica4", "Z karczmy w�a�nie "+
                "wyrzucono " + QIMIE(kto,PL_BIE) + ".\n");
            kto->remove_prop(SIT_SIEDZACY);
            //this_player()->remove_prop(obj_i_dont_show_in_glance);
            kto->remove_prop(OBJ_I_DONT_GLANCE);
            kto->move_living("M",POLNOC_LOKACJE+"ulica4",1,0);
            break;

        default:
            command("westchnij cicho");
    }
}

void
taki_sobie(object kto)
{
    if(random(2))
    {
        set_alarm(2.0, 0.0, "command_present", kto, "powiedz do " +OB_NAME(kto) + " S�ucham?");
        set_alarm(2.0, 0.0, "command_present", kto, "powiedz do " +OB_NAME(kto) + " Czego?");
    }
}

void
dobry(object kto)
{
    if(kto->query_gender() == G_MALE)
        zly(kto);
    else
    {
        switch (random(3))
        {
            case 0:
                command("usmiechnij sie niesmialo do " +OB_NAME(kto));
                break;
            case 1:
                command("emote czerwieni si� lekko."); break;
            case 2:
                set_alarm(2.0, 0.0, "command_present", kto, "powiedz do " +OB_NAME(kto) +
                    " To mi�e, ale jak widzisz, jestem zaj�ty.");
            break;
        }
    }
}

void attacked_by(object wrog)
{
    if(!walka)
    {
        command("emote wyci�ga spod kontuaru ci�k� pa�k� i dobywa jej.");
        palka = add_weapon(RINDE_BRONIE + "palki/debowa_gruba.c");
        set_alarm(7.0, 0.0, "command", "krzyknij Stra�! Pomocy!");
        set_alarm(15.0, 0.0, "czy_walka", 1);
        walka = 1;
        return ::attacked_by(wrog);
    }

    else if(walka)
    {
          set_alarm(7.0, 0.0, "command", "krzyknij Straz! Pomocy!");
          set_alarm(15.0, 0.0, "czy_walka", 1);
          walka = 1;
          return ::attacked_by(wrog);
    }

}


int
czy_walka()
{
    if(!query_attack() && walka)
    {
        this_object()->command("opusc bron");
        palka->remove_object();
        this_object()->command("powiedz Psie krwie!");
        this_object()->command("splun");
        walka = 0;
        return 1;
    }
    else
    {
        set_alarm(15.0, 0.0, "czy_walka", 1);
    }
}

public void
do_die(object killer, int og = 0)
{
    command("powiedz Oby�... Oby� szcz�z�" + this_player()->koncowka("", "a") + " marnie, " +
        this_player()->koncowka("kurwi synu", "kurwo") + ".");
    ::do_die(killer, og);
}

int
signal_leave(object kto, object dokad)
{
    object *naczynia;

    if(dokad->query_pub())
        return 0;

    naczynia = filter(deep_inventory(kto), &->is_naczynie());

    if(sizeof(naczynia) > 0)
    {
        tell_object(kto, "Oddajesz wszystkie naczynia karczmarzowi.\n");
        naczynia->remove_object();
        return 0;
    }
}
