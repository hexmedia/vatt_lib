/* Kowal z Rinde.
      by Lil 19.08.05               */

#pragma unique
#pragma strict types

#include <macros.h>
#include <stdproperties.h>
#include <money.h>
#include <pl.h>
#include <formulas.h>
#include <std.h>
#include <ss_types.h>
#include <wa_types.h>
#include <composite.h>
#include <filter_funs.h>
#include <pattern.h>
#include "dir.h"

inherit RINDE_NPC_STD;
inherit "/lib/craftsman.c";

#define WZORY (POLNOC + "Kuznia/wzory/")
#define REP_MAT "/d/Standard/items/materialy/"
#define MAGAZYN (POLNOC + "Kuznia/lokacje/magazyn.c")

void
create_rinde_humanoid()
{
    set_living_name("harr");

    ustaw_odmiane_rasy(PL_MEZCZYZNA);
    dodaj_nazwy(({"kowal", "kowala", "kowalowi", "kowala",
                  "kowalem", "kowalu"}),
                ({"kowale", "kowali", "kowalom", "kowali",
                  "kowalami", "kowalach"}), PL_MESKI_OS);

    set_surname("Garath");
    set_origin("z Rinde");

    set_gender(G_MALE);
    set_title(", Kowal");

    ustaw_imie(({"harr","harra","harrowi","harra", "harrem","harze"}), PL_MESKI_OS);

    set_long("Silne ramiona, pot�na sylwetka i d�onie, kt�rych sk�ra "+
        "jest wyrobiona niemal jak rzemie� wzbudzaj� respekt i "+
        "szacunek nie tylko w�r�d klient�w. Jego ku�nia to jego "+
        "w�asne kr�lestwo. Ponadto �w jasnow�osy olbrzym, o nieodgadnionym "+
        "spojrzeniu niebieskich oczu jest obiektem westchnie� wielu "+
        "okolicznych dziewcz�t i panienek. Nie zwyk� nosi� koszuli, "+
        "jego nagi, doskonale wyrze�biony tors po�yskuje w �wietle "+
        "padaj�cym z paleniska. Na nogach ma lu�ne, p��cienne spodnie "+
        "wsadzone w cholewy wysokich but�w. Jego policzek przecina "+
        "d�uga, szpec�ca blizna, a na tu�owiu i ramionach wida� "+
        "pozosta�o�ci po wielu innych ranach.\n");
    dodaj_przym("pot�ny","pot�ni");
    dodaj_przym("jasnow�osy","jasnow�osi");

    set_stats ( ({ 80, 45, 80, 25, 84 }) );
    set_skill(SS_DEFENCE, 50 + random(4));
    set_skill(SS_WEP_CLUB, 33 + random(10));
    set_skill(SS_PARRY, 10 + random(10));
    set_skill(SS_UNARM_COMBAT, 55 + random(15));
    set_skill(SS_CRAFTING_BLACKSMITH, 80);

    add_armour("/d/Standard/items/ubrania/fartuchy/losowy_rzemieslniczy");
    add_armour(RINDE_UBRANIA + "spodnie_los_los_biedaka.c");
    add_armour(RINDE_UBRANIA + "/buty/wygodne_czarne_Mc.c");
    add_weapon(RINDE_NARZEDZIA + "duzy_ciezki_mlot.c");

    set_act_time(5);
    add_act("@@kowal_pracuje@@");

    add_prop(NPC_I_NO_RUN_AWAY, 1);
    add_prop(CONT_I_WEIGHT, 91000);
    add_prop(CONT_I_HEIGHT, 172);

    config_default_craftsman();
    craftsman_set_domain(PATTERN_DOMAIN_BLACKSMITH);
    craftsman_set_sold_item_patterns( ({
                WZORY + "helm_duzy_czarny.c",
                WZORY + "kolczuga_brzeczaca_czerniona.c",
                WZORY + "helm_lekki_oksydowany",
                WZORY + "kirys_srebrny_elegancki",
                WZORY + "miecz_masywny_stalowy",
                WZORY + "miecz_obosieczny_krotki",
                WZORY + "mizerykordia_zdobiona_waska",
                WZORY + "mlot_ciezki_zelazny",
                WZORY + "przeszywanica_pikowana_brazowa",
                WZORY + "spodnie_wzmacniane_skorzane",
                WZORY + "topor_krotki_zdobiony",
                WZORY + "topor_prosty_jednoreczny",
                WZORY + "napiersnik_polerowany_plytowy.c"
                }) );

    set_money_greed_buy(218);
    set_money_greed_sell(495);

    set_money_greed_change(104);

    //Ustawiamy kowalowi store_room
    set_store_room(MAGAZYN);
}

void
init()
{
    ::init();
    craftsman_init();
}

public string
query_auto_load()
{
    return ::query_auto_load() + query_craftsman_auto_load();
}

public string
init_arg(string arg)
{
    if (arg == 0) {
//         add_object(REP_MAT + "sk_swinska");
//         add_object(REP_MAT + "sk_swinska");
//         add_object(REP_MAT + "stal_sztabka");
//         add_object(REP_MAT + "stal_sztabka");
        force_expand_patterns_items();
    }
    return init_craftsman_arg(::init_arg(arg));
}

string
kowal_pracuje()
{
    if (craftsman_is_busy()) {
        switch (random(3)) {
            case 0: return "emote uderza masywnym m�otem w kawa� rozgrzanej stali.";
            case 1: return "emote st�ka z wysi�ku.";
            case 2: return "emote ociera pot z czo�a.";
        }
    }
    return "";
}

void
emote_hook(string emote, object wykonujacy)
{
    switch (emote)
    {
        case "kopnij": command("'Won st^ad, bo ^zelazem rzy^c oznacze!");
                    break;
        case "spoliczkuj": command("warknij");
                    break;
        case "opluj" : command("emote napina musku^ly.");
                       command("'Zaraz si^e z rynsztokiem przywitasz.");
                    break;
        case "poca^luj":
              {
              if(wykonujacy->query_gender() == 1)
                switch(random(2))
                 {
                 case 0:command("mrugnij do "+
                        OB_NAME(wykonujacy));break;
                 case 1:command("popatrz z usmiechem na "+
                        OB_NAME(wykonujacy));
                        break;
                }
              else
                {
                   switch(random(2))
                   {
                   case 0:command("popatrz ci^e^zko na "+
                        OB_NAME(wykonujacy));
                          command("'Nie powtarzaj tego.");
                            break;
                   case 1:command("spojrzyj z obrzydzeniem na "+
                        OB_NAME(wykonujacy));
                            break;
                   }
                }

                break;
                }
        case "przytul": command("'Nie przeszkadzaj w robocie. Musz^e "+
                   "dwa lemiesze wyklepa^c.");
                   break;
        case "przytul": command("popatrz dziwnie na "+OB_NAME(wykonujacy));
                   break;
    }
}
