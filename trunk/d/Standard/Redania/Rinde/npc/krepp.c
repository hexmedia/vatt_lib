
/* Autor: Avard
   Data : 12.03.06
   Opis : Avard i Alcyone *
   Info : Trzeba mu przebudowac te funkcje do lazenia */

#pragma unique

#include <std.h>
#include <ss_types.h>
#include <macros.h>
#include <stdproperties.h>
#include <pl.h>
#include "dir.h"

inherit RINDE_NPC_STD;

int czy_walka();
int walka = 0;
object klucz;
object klucz2;

void
create_rinde_humanoid()
{
    ustaw_odmiane_rasy(PL_MEZCZYZNA);
    set_living_name("krepp");
    set_gender(G_MALE);
    ustaw_imie(({"krepp","kreppa","kreppowi","kreppa","kreppem","kreppie"}),
        PL_MESKI_OS);
    set_title(", Najwy^zszy Kap^lan Boga Kreve, Powiernik Zb^l^akanych Dusz,"+
        " Stra^znik Porz^adku");
    dodaj_nazwy(({"kap^lan","kap^lana","kap^lanowi","kap^lana","kap^lanem",
        "kap^lanie"}), ({"kap^lani","kap^lan^ow","kap^lanom","kap^lan^ow",
        "kap^lanami","kap^lanach"}), PL_MESKI_OS);

    set_long("Ju^z na pierwszy rzut oka wida^c, ^ze w ^swi^atyni nie jest "+
        "on po^sledni^a postaci^a, ale person^a postawion^a najwy^zej jak "+
        "to tylko mo^zliwe. Ze swego posterunku u st^op pos^agu obserwuje "+
        "wiernych, obrzucaj^ac ich spojrzeniem niemal r^ownie surowym i "+
        "pe^lnym pogardy, co b^og, u kt^orego st^op stoi. Brunatna szata "+
        "jest wykonana z drogiego materia^lu, cho^c brakuje jej zdobie^n i "+
        "starannego wyko^nczenia. Jednak jej lu^xny kszta^lt znakomicie "+
        "podkre^sla mocarna sylwetk^e kap^lana, jego szerokie barki i "+
        "pot^e^zne ramiona, szczup^ly brzuch, kt^ory jest ^swiadectwem "+
        "skromno^sci i wstrzemi^e^xliwo^sci kap^lana. Na jego szyi "+
        "po^lyskuje ci^e^zki, wykuty z metalu medalion w kszta^lcie "+
        "b^lyskawicy o siedmiu z^abkach - symbol okresu, w kt^orym "+
        "b^og Kreve pojawi^l si^e w^sr^od ludzi pod t^a w^la^snie "+
        "postaci^a i pozosta^l z nimi przez siedem d^lugich dni. W "+
        "pasie szata obj^eta jest ciasno przewi^azanym sznurem, na "+
        "kt^orym zawieszono klucz o rze^xbionym oku i dziwacznym "+
        "kszta^lcie. \n");

    dodaj_przym("szczup^ly","szczupli");
    dodaj_przym("wysoki","wysocy");

    set_stats (({ 50, 30, 45, 80, 20 }));

    add_prop(CONT_I_WEIGHT, 100000);
    add_prop(CONT_I_HEIGHT, 180);
    add_prop(NPC_I_NO_FEAR, 1);

    set_act_time(30);
    add_act("emote rozgl^ada si^e po otoczeniu.");
    add_act("emote przystaje na chwil^e w bezruchu, by po chwili ruszy^c "+
        "dalej.");
    add_act("rozejrzyj sie leniwie");
    add_act("ziewnij dyskretnie");
    add_act("emote modli si^e cicho spogl^adaj^ac w oblicze pos^agu.");

    set_cchat_time(10);
    add_cchat("Odejd^x z tego miejsca!");
    add_cchat("Precz!");
    add_cchat("Nie ujdzie ci to na sucho!");

    //add_armour(RINDE_UBRANIA + "majtki/zwykle_biale_Mc.c");
    add_armour(RINDE_UBRANIA + "onuce/czyste_biale_Mc.c");
    add_armour(RINDE_UBRANIA + "cizemki/miekkie_skorzane_Mc.c");
    add_armour(RINDE_UBRANIA + "szaty/brazowa_haftowana_Mc.c");
    //clone_object(TUTAJ POTRZEBNY SZTYLET TODO)->move(this_object());

    add_ask(({"�wi�tyni^e"}), VBFC_ME("pyt_o_swiatynie"));
    add_ask(({"yennefer","wied^zme","czarownice"}),
        VBFC_ME("pyt_o_yennefer"));
    add_ask(({"geralta","wied^zmina","bia^low^losego"}),
        VBFC_ME("pyt_o_geralta"));
    add_ask(({"kreve","b^ostwo"}), VBFC_ME("pyt_o_kreve"));
    set_default_answer(VBFC_ME("default_answer"));

    //set_alarm_every_hour("co_godzine");
}

void
powiedz_gdy_jest(object player, string tekst)
{
    if (environment(this_object()) == environment(player))
        this_object()->command(tekst);
}

string
pyt_o_swiatynie()
{
    set_alarm(2.0, 0.0, "powiedz_gdy_jest", this_player(),
        "':wskazuj^ac r^ek^a otoczenie: Rozejrzyj si^e troch^e po "+
        "^swi^atyni Kreve i pochyl g^low^e kiedy stoisz przede mn^a. ");
    return "";
}
string
pyt_o_yennefer()
{
    set_alarm(2.0, 0.0, "powiedz_gdy_jest", this_player(),
        "':krzywi^ac si^e: Nie wspominaj mi o tej wied^xmie.");
    return "";
}
string
pyt_o_geralta()
{
    set_alarm(2.0, 0.0, "powiedz_gdy_jest", this_player(),
        "powiedz Wied^xmin? Poszed^l gdzie^s w swoj^a drog^e i lepiej "+
        "niech me oczy nie ogladaj^a ju^z tego bia^low^losego.");
    return "";
}
string
pyt_o_kreve()
{
    set_alarm(2.0, 0.0, "powiedz_gdy_jest", this_player(),
        "'Jeste^s w jego ^swi^atyni. ");
    return "";
}
string
default_answer()
{
    set_alarm(2.0, 0.0, "powiedz_gdy_jest", this_player(),
        "powiedz do "+ this_player()->query_name(PL_DOP) +
        " Niestety nie wiem o co ci chodzi.");
    return "";
}

void
add_introduced(string imie_mia, string imie_bie)
{
    set_alarm(2.0, 0.0, "return_introduce", find_player(imie_mia));
}
void
return_introduce(object ob)
{
    command("przedstaw sie " + OB_NAME(ob));

}

void
emote_hook(string emote, object wykonujacy)
{
    switch (emote)
    {
        case "kopnij": set_alarm(1.5, 0.0, "nienajlepszy", wykonujacy);
                    break;
        case "spoliczkuj": set_alarm(1.5, 0.0, "nienajlepszy", wykonujacy);
                    break;
        case "opluj" : set_alarm(2.5, 0.0, "nienajlepszy", wykonujacy);
                    break;
        case "poca^luj":
              {
              if(wykonujacy->query_gender() == 1)
                switch(random(2))
                 {
                 case 0:command("'Mi nie wolno... ");
                        command("emote dodaje po chwili: Publicznie.");break;
                 case 1:command("popatrz z usmiechem na "+
                        OB_NAME(wykonujacy));
                        break;
                }
              else
                {
                   switch(random(3))
                   {
                   case 0:command("emote czerwienieje ze z^lo^sci.");
                          command("emote krzyczy: Odejd^x st^ad i wych^ed^o^z "+
                          "si^e sam!");
                            break;
                   case 1:command("spojrzyj z obrzydzeniem na "+
                        OB_NAME(wykonujacy));
                            break;
                   case 2:command("'Odejd^x i nie wracaj.");
                            break;
                   }
                }

                break;
                }
        case "przytul": set_alarm(2.5, 0.0, "malolepszy", wykonujacy);
                   break;
        case "pog^laszcz": set_alarm(2.5, 0.0, "malolepszy", wykonujacy);
                   break;
    }
}

void
malolepszy(object wykonujacy)
{

   switch(random(4))
   {
     case 0: command("usmiechnij sie niepewnie"); break;
     case 1: command("usmiechnij sie nieznacznie"); break;
     case 2: command("hmm"); break;
     case 3: command("namysl sie ."); break;

   }
}

void
nienajlepszy(object kto)
{
   switch(random(3))
   {
      case 0: command("'Ch^edo^z si^e durniu.");
              break;
      case 1: set_alarm(0.5, 0.0, "powiedz_gdy_jest", kto,
              "prychnij");
              break;
      case 2: command("powiedz Wyno^s si^e.");
              break;
   }
}

void attacked_by(object wrog)
{
    if(walka == 0)
    {
        walka = 1;
        set_alarm(0.5,0.0,"command","krzyknij Stra^z! Pomocy!");
        set_alarm(1.0,0.0,"command","dobadz broni");
        set_alarm(15.0, 0.0, "czy_walka", 1);
        return ::attacked_by(wrog);
    }
    else
    {
        return ::attacked_by(wrog);
    }
}
int
czy_walka()
{
    if(!query_attack())
    {
        set_alarm(0.5,0.0,"command","powiedz Niech tak b^edzie.");
        set_alarm(1.0,0.0,"command","opusc bron.");
        walka = 0;
        return 1;
    }
    else
    {
        set_alarm(15.0, 0.0, "czy_walka", 1);
    }
}
/*
int
co_godzine()
{
    if (MT_GODZINA == 22)
    {
        set_alarm(3300.0,0.0, "zamykamy");
    }
    if (MT_GODZINA == 23)
    {
        set_alarm(0.1,0.0, "wychodzimy");
    }
    if (MT_GODZINA == 6)
    {
        set_alarm(0.1,0.0, "otwieramy");
    }

}
void
zamykamy()
{
    if(!query_attack())
    {
    set_alarm(0.0,0.0, "command", "powiedz Za chwil� zamykam "+
        "^swi^atyni^e.");
    set_alarm(1.0,0.0, "command", "w");
    set_alarm(2.0,0.0, "command", "powiedz Za chwil^e zamykam "+
        "^swi^atyni^e.");
    set_alarm(5.0,0.0, "command", "e");
    set_alarm(6.0,0.0, "command", "e");
    set_alarm(7.0,0.0, "command", "powiedz Za chwil^e zamykam "+
        "^swi^atyni^e.");
    set_alarm(9.0,0.0, "command", "w");
    set_alarm(10.0,0.0, "command", "s");
    set_alarm(11.0,0.0, "command", "powiedz Za chwil^e zamykam "+
        "^swi^atyni^e.");
    set_alarm(13.0,0.0, "command", "n");
    }
    else
    {
        if(MT_GODZINA < 23)
        {
        set_alarm(10.0,0.0, "zamykamy");
        }
        else
        {
        set_alarm(10.0,0.0, "wychodzimy");
        }
    }
}
void
wychodzimy()
{
    if(!query_attack())
    {
    klucz = clone_object(SWIATYNIA+"drzwi/k_do_swiatyni");
    klucz->move(this_object());
    command("zamknij wrota");
    command("zamknij wrota dziwacznym kluczem");
    set_alarm(0.5,0.0, "zabieramy_klucze");
    set_alarm(3.0,0.0, "command", "s");
    set_alarm(6.0,0.0, "command", "e");
    set_alarm(9.0,0.0, "command", "n");
    set_alarm(12.0,0.0, "command", "n");
    klucz2 = clone_object(ZACHOD_DRZWI+"k_do_domu");
    klucz2->move(this_object());
    set_alarm(15.0,0.0, "command", "otworz drzwi kluczem");
    set_alarm(15.0,0.0, "command", "otworz drzwi");
    set_alarm(15.0,0.0, "command", "dom");
    set_alarm(15.0,0.0, "command", "zamknij drzwi");
    set_alarm(15.0,0.0, "command", "zamknij drzwi kluczem");
    set_alarm(15.5,0.0, "zabieramy_klucze2");
    }
    else
    {
        set_alarm(10.0,0.0, "wychodzimy");
    }
}
void
otwieramy()
{
    if(!query_attack())
    {
    klucz2 = clone_object(ZACHOD_DRZWI+"k_do_domu");
    klucz2->move(this_object());
    command("otworz drzwi kluczem");
    command("otworz drzwi");
    command("wyjscie");
    command("zamknij drzwi");
    command("zamknij drzwi kluczem");
    set_alarm(1.0,0.0, "zabieramy_klucze2");
    set_alarm(3.0,0.0, "command", "s");
    set_alarm(6.0,0.0, "command", "s");
    set_alarm(9.0,0.0, "command", "w");
    set_alarm(12.0,0.0, "command", "n");
    klucz = clone_object(SWIATYNIA+"drzwi/k_do_swiatyni");
    klucz->move(this_object());
    set_alarm(12.0,0.0, "command", "otworz wrota kluczem");
    set_alarm(12.0,0.0, "command", "otworz wrota");
    set_alarm(12.5,0.0, "zabieramy_klucze");
    set_alarm(15.0,0.0, "command", "swiatynia");
    set_alarm(18.0,0.0, "command", "n");
    }
    else
    {
        set_alarm(10.0,0.0, "otwieramy");
    }
}

void
zabieramy_klucze()//TODO To trzeba bedzie zamienic, tak zeby Krepp mial
{                 //ciagle klucze przy sobie i reagowal na ich kradziez
    klucz->remove_object();
}

void
zabieramy_klucze2()//TODO To trzeba bedzie zamienic, tak zeby Krepp mial
{                 //ciagle klucze przy sobie i reagowal na ich kradziez
    klucz2->remove_object();
}

public void
do_die(object killer)
{
    klucz2 = clone_object(ZACHOD_DRZWI+"k_do_domu");
    klucz2->move(this_object());
    klucz = clone_object(SWIATYNIA+"drzwi/k_do_swiatyni");
    klucz->move(this_object());

        ::do_die(killer);
}
*/