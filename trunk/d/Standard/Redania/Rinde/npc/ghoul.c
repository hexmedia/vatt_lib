
/* Autor: Avard
   Opis : Faeve
   Data : 7.04.07 */

inherit "/std/monster.c";

#include <std.h>
#include <ss_types.h>
#include <macros.h>
#include <stdproperties.h>
#include <object_types.h>
#include <pl.h>
#include "dir.h"

int czy_atakowac();

void
create_monster()
{
    ustaw_odmiane_rasy("ghoul");
    set_gender(G_MALE);
    random_przym("oszpecony:oszpeceni brudny:brudni "+
        "^smierdz^acy:^smierdz^acy zaniedbany:zaniedbani "+
        "zakrwawiony:zakrwawieni || "+
        "agresywny:agresywni nerwowy:nerwowi wysoki:wysocy "+
        "chudy:chudzi wychudzony:wychudzeni",2);

    set_long("Kreatura kt^orej si^e przygl^adasz wygl^ada na humanoida, "+
        "gdy^z budowa cia^la przypomina starca z powykrecanymi ko^nczynami. "+
        "Jego d^lugie r^ece zaopatrzone s^a w szerokie pazury s^lu^z^ace do "+
        "rozgrzebywania mogi^l i ^swie^zej ziemi. Dziwna substancja kt^or^a "+
        "ociekaja pazury potwora budzi w tobie obawy. Ostre jak brzytwa "+
        "z^eby i d^lugi j^ezyk pozwalaj^a mia^zd^zy^c ko^sci, a nast^epnie "+
        "wysysa^c z nich szpik kostny. Stw^or ten pojawia si^e na "+
        "powierzchni tylko noc^a, w dzie^n za^s przesiaduje w mrocznych "+
        "miejscach by ukry^c si^e przed ra^z^acym go s^lo^ncem. Zwykle nie "+
        "poluje, zadowalajac si^e tylko padlin^a b^ad^x cia^lami "+
        "pozostawionymi po bitwie. Lecz gdy zobaczy s^labego przeciwnika "+
        "lub gdy zostanie zaskoczony b^edzie walczy^l z pe^ln^a "+
        "zaci^eto^sci^a staj^ac si^e naprawde gro^xnym przeciwnikiem.\n");
    set_stats (({70,60,140,40,100}));
    add_prop(CONT_I_WEIGHT, 60000);
    add_prop(CONT_I_HEIGHT, 185);
    add_prop(LIVE_I_NEVERKNOWN, 1);
    set_aggressive(&czy_atakowac());
    set_attack_chance(100);
    set_npc_react(1);

   /* set_cact_time(10);
    add_cact("jeknij");
    add_cact("emote spogl^ada na ciebie wrogo.");*/

    set_attack_unarmed(0, 25, 30, W_SLASH,  50, "d^lo^n", "lewy",  "pazury");
    set_attack_unarmed(1, 25, 30, W_SLASH,  50, "d^lo^n", "prawy", "pazury");

    set_hitloc_unarmed(A_HEAD,          ({ 1, 1, 1 }),  4, "g^lowa");
    set_hitloc_unarmed(A_CHEST,         ({ 1, 1, 1 }), 14, "klatka piersiowa");
    set_hitloc_unarmed(A_STOMACH,       ({ 1, 1, 1 }), 13, "brzuch");
    set_hitloc_unarmed(A_BACK,          ({ 1, 1, 1 }),  3, "plecy");
    set_hitloc_unarmed(A_L_SHOULDER,    ({ 1, 1, 1 }),  5, "bark",       "lewy");
    set_hitloc_unarmed(A_R_SHOULDER,    ({ 1, 1, 1 }),  5, "bark",       "prawy");
    set_hitloc_unarmed(A_L_ARM,         ({ 1, 1, 1 }),  6, "rami^e",      "lewy");
    set_hitloc_unarmed(A_R_ARM,         ({ 1, 1, 1 }),  6, "rami^e",      "prawy");
    set_hitloc_unarmed(A_L_FOREARM,     ({ 1, 1, 1 }),  5, "przedrami^e", "lewy");
    set_hitloc_unarmed(A_R_FOREARM,     ({ 1, 1, 1 }),  5, "przedrami^e", "prawy");
    set_hitloc_unarmed(A_L_HAND,        ({ 1, 1, 1 }),  3, "d^lo^n",       "lewy");
    set_hitloc_unarmed(A_R_HAND,        ({ 1, 1, 1 }),  3, "d^lo^n",       "prawy");
    set_hitloc_unarmed(A_L_THIGH,       ({ 1, 1, 1 }),  6, "udo",        "lewy");
    set_hitloc_unarmed(A_R_THIGH,       ({ 1, 1, 1 }),  6, "udo",        "prawy");
    set_hitloc_unarmed(A_L_SHIN,        ({ 1, 1, 1 }),  5, "^lydka",      "lewy");
    set_hitloc_unarmed(A_R_SHIN,        ({ 1, 1, 1 }),  5, "^lydka",      "prawy");
    set_hitloc_unarmed(A_L_FOOT,        ({ 1, 1, 1 }),  3, "stopa",      "lewy");
    set_hitloc_unarmed(A_R_FOOT,        ({ 1, 1, 1 }),  3, "stopa",      "prawy");

    add_prop(LIVE_I_SEE_DARK, 1);

    add_leftover("/std/leftover", "oko", 2, 0, 0, 0, 0, O_JEDZENIE);
    add_leftover("/std/leftover", "z^ab", random(5) + 2, 0, 1, 0, 0, O_KOSCI);
    add_leftover("/std/leftover", "czaszka", 1, 0, 1, 1, 0, O_KOSCI);
    add_leftover("/std/leftover", "ko^s^c", random(3) + 2, 0, 1, 1, 0, O_KOSCI);
    add_leftover("/std/leftover", "serce", 1, 0, 0, 1, 0, O_JEDZENIE);
}

int
czy_atakowac()
{
    if(TP->is_humanoid())
        return 1;
    else
        return 0;
}
