#include <macros.h>
#include <stdproperties.h>
#include <money.h>
#include <pl.h>
#include <formulas.h>
#include <std.h>
#include <ss_types.h>
#include <wa_types.h>
#include <composite.h>
#include <filter_funs.h>
#include "dir.h"

inherit RINDE_NPC_STD;

void
create_rinde_humanoid()
{
    set_living_name("damir");

    ustaw_odmiane_rasy();
    set_gender(G_MALE);

    dodaj_nazwy();

    set_title();

    ustaw_imie(({}), PL_MESKI_OS);

    set_long();

    dodaj_przym();
    dodaj_przym();

    set_act_time(30);
    add_act();
    add_act();
    add_act();
    add_act();
    add_act();
    add_act();

    set_cchat_time(15);
    add_cchat();
    add_cchat();

    set_default_answer(VBFC_ME("default_answer"));

    set_stats ( ({ 65, 31, 70, 21, 30, 58 }) );

    set_skill(SS_DEFENCE, 10 + random(4));
    set_skill(SS_WEP_CLUB, 33 + random(10));
    set_skill(SS_PARRY, 10 + random(10));
    set_skill(SS_UNARM_COMBAT, 15 + random(5));

//    add_armour(RINDE_UBRANIA + "fartuch_los_los_rzemieslniczy.c");
//    add_armour(RINDE_UBRANIA + "spodnie_los_los_biedaka.c");

    add_prop(LIVE_I_NEVERKNOWN, 1);
//    add_prop(CONT_I_WEIGHT, 91000);
//    add_prop(CONT_I_HEIGHT, 172);
}

string
default_answer()
{
     set_alarm(2.0, 0.0, "command_present", this_player(),
             "powiedz do " + OB_NAME(this_player()) +
                  " Przykro mi. Pom�g� bym ci, ale teraz nie mam czasu.");
     return "";
}

void
return_introduce(string imie)
{
    object osoba = present(imie, environment());

    if (osoba)
    {
        command("przedstaw sie " + OB_NAME(osoba));
    }
}

void
emote_hook(string emote, object wykonujacy, string przyslowek)
{
    switch (emote)
    {
        case "kopnij":
        case "spoliczkuj":
        case "opluj":
	case "nadepnij":
	    set_alarm(2.0, 0.0, "zly", wykonujacy);
            break;

        case "szturchnij":
	    set_alarm(2.0, 0.0, "taki_sobie", wykonujacy);
            break;

        case "za�miej":
        case "poca�uj":
        case "przytul":
        case "pog�aszcz":
	    set_alarm(2.0, 0.0, "dobry", wykonujacy);
            break;
    }
}

void
zly(object kto)
{
}

void
taki_sobie(object kto)
{
}

void
dobry(object kto)
{
}

void
attacked_by(object wrog)
{
    ::attacked_by(wrog);
}
