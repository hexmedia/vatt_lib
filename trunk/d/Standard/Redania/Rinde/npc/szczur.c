/* Autor: Avard
   Opis : Sniegulak
   Data : 10.04.07 */

inherit "/std/zwierze.c";

#include <macros.h>
#include <stdproperties.h>
#include <pl.h>
#include <formulas.h>
#include <std.h>
#include <composite.h>
#include <filter_funs.h>
#include <cmdparse.h>
#include <mudtime.h>
#include <wa_types.h>
#include <ss_types.h>
#include <object_types.h>
#include "dir.h"

void
create_zwierze()
{
    ustaw_odmiane_rasy("szczur");
    set_gender(G_MALE);

    set_long("Czerwone oczka, brudna sier^s^c i d^lugi ogon. To "+
        "przedstawiciel szczurzej populacji, kt^ora tak uwielbia "+
        "ciemne zakamarki i resztki jedzenia. Okaz ten jest "+
        "wyj^atkowo chudy i ruchliwy.\n");

    random_przym("wychudzony:wychudzeni niewielki:niewielcy drobny:drobni "+
        "ko^scisty:ko^sci^sci ma^ly:mali ||brudny:brudni "+
        "szorstkow^losy:szorstkow^losi||czerwonooki:czerwonoocy "+
        "p^lochliwy:p^lochliwi ruchliwy:ruchliwi",2);


    set_stats(({5, 30, 5, 20, 10}));

    add_prop(LIVE_I_NEVERKNOWN, 1);
    add_prop(CONT_I_WEIGHT, 800);
    add_prop(CONT_I_VOLUME, 800);
    add_prop(CONT_I_HEIGHT, 10);

    set_hitloc_unarmed( 1, ({ 1, 1, 1 }), 10, "g�owa");
    set_hitloc_unarmed( 2, ({ 1, 1, 1 }), 30, "tu��w");
    set_hitloc_unarmed( 4, ({ 1, 1, 1 }), 20, "�apka", ({"lewy", "przedni"}));
    set_hitloc_unarmed( 8, ({ 1, 1, 1 }), 20, "�apka", ({"prawy", "przedni"}));
    set_hitloc_unarmed(16, ({ 1, 1, 1 }), 10, "�apka", ({"lewy", "tylny"}));
    set_hitloc_unarmed(32, ({ 1, 1, 1 }), 10, "�apka", ({"prawy", "tylny"}));

    set_random_move(1);
    set_restrain_path(KANALY_LOKACJE);
    add_leftover("/std/skora", "sk^ora", 1, 0, 1, 1, 0, O_SKORY);
    add_leftover("/std/leftover", "ogon", 1, 0, 1, 1, 1, O_KOSCI);
}

void
emote_hook(string emote, object wykonujacy, string przyslowek)
{
    switch (emote)
    {
        case "kopnij":
        case "spoliczkuj":
        case "opluj":
        case "nadepnij":
        case "poca^luj":
        case "przytul":
        case "pog^laszcz":
        case "szturchnij":
            set_alarm(1.0, 0.0, "zly", wykonujacy);
            break;
    }
}

void
zly(object kto)
{
   TO->run_away();
}
