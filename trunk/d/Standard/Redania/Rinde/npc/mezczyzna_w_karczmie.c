#pragma unique
#include <macros.h>
#include <stdproperties.h>
#include <money.h>
#include <pl.h>
#include <formulas.h>
#include <std.h>
#include <ss_types.h>
#include <wa_types.h>
#include <composite.h>
#include <filter_funs.h>
#include "dir.h"

inherit RINDE_NPC_STD;

void
create_rinde_humanoid()
{
    ustaw_odmiane_rasy("m�czyzna");
    set_gender(G_MALE);

    set_long("Z ust tego m�czyzny co chwil� da si� us�ysze� jakie� " +
        "nieprzyjemne s�owa. Wiecznie zmru�one oczy rozgl�daj� si� chytrze " +
        "po okolicy, jakby w ka�dym widz�c nieprzyjaciela. Jego nieco " +
        "szczurzy i zdecydowanie odpychaj�cy wyraz twarzy dodaje mu nieco " +
        "do wizerunku osobnika niezbyt przyja�nie nastawionego do �ycia i " +
        "innych. Spogl�daj�c spode �ba na innych mamrocze pod nosem jakie� " +
        "s�owa, kt�rych jednak nie da si� zrozumie�. Siedzi samotnie " +
        "na jednym z krzese� przy ladzie, popijaj�c z kieliszka jaki� " +
        "mocniejszy trunek.\n");

    dodaj_przym("ponury", "ponurzy");
    dodaj_przym("ciemnow�osy", "ciemnow�osi");

    set_act_time(30);
    add_act("emote mamrocze co� pod nosem.");
    add_act("emote rozgl�da si� spode �ba.");
    add_act("emote ze znudzeniem stuka palcami o blat lady.");
    add_act("emote wypija kolejny kieliszek jakiego� mocnego trunku.");
    add_act("emote patrzy na ciebie k�tem oka, jednak gdy odwzajemniasz " +
	"spojrzenie, szybko wbija sw�j wzrok w kieliszek.");

    set_default_answer(VBFC_ME("default_answer"));

    set_stats ( ({ 65, 31, 70, 25, 58 }) );

    set_skill(SS_DEFENCE, 10 + random(4));
    set_skill(SS_WEP_CLUB, 33 + random(10));
    set_skill(SS_PARRY, 10 + random(10));
    set_skill(SS_UNARM_COMBAT, 15 + random(5));

    add_armour(RINDE_UBRANIA + "plaszcz_z_kapturem_szary_znoszony");
    add_armour(RINDE_UBRANIA + "spodnie/ciemne_zwykle_Mc.c");

    add_prop(LIVE_I_NEVERKNOWN, 1);
    add_prop(LIVE_I_INTRODUCING, 1);
//    add_prop(CONT_I_WEIGHT, 91000);
//    add_prop(CONT_I_HEIGHT, 172);
}

void
start_me()
{
    command("usiadz przy ladzie");
}

string
default_answer()
{
     set_alarm(1.0, 0.0, "command_present", this_player(),
        "':nieprzyjemnym g�osem: Znajd� innego kompana do rozmowy.");
     return "";
}

void
return_introduce(string imie)
{
    object osoba = present(imie, environment());

    if (osoba)
    {
        command("emote nie zwraca na ciebie uwagi.");
    }
}

void
emote_hook(string emote, object wykonujacy, string przyslowek)
{
    switch (emote)
    {
        case "kopnij":
        case "spoliczkuj":
        case "opluj":
	case "nadepnij":
	    if (random(2))
		set_alarm(1.5, 0.0, "command_present", wykonujacy,
		    "emote mru�y oczy ostrzegawczo.");
	    else
		set_alarm(1.5, 0.0, "command_present", wykonujacy,
		    "'Zr�b tak jeszcze raz, a inaczej porozmawiamy.");
            break;

        case "za�miej":
        case "poca�uj":
        case "przytul":
        case "pog�aszcz":
	    if (random(2))
		set_alarm(1.5, 0.0, "command_present", wykonujacy,
		    "emote spogl�da gdzie� z odraz�.");
	    else
		set_alarm(1.5, 0.0, "command_present", wykonujacy,
		    "'Nie r�b tak wi�cej.");
            break;
    }
}

void
attacked_by(object wrog)
{
    command("emote wydziera si� na ca�y g�os: �cierwo!");
    ::attacked_by(wrog);
}
