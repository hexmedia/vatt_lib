
/* Autor: Avard
   Opis : Kotra i Avard
   Data : 04.08.06
   Info : Straznik Rinde */

#include <std.h>
#include <ss_types.h>
#include <macros.h>
#include <stdproperties.h>
#include <pl.h>
#include "dir.h"

inherit RINDE_NPC_STD;

void
create_rinde_humanoid()
{
     ustaw_odmiane_rasy("m^e^zczyzna");
     set_gender(G_MALE);
     dodaj_nazwy("straznik");
     dodaj_przym("niski","niscy");
     dodaj_przym("barczysty","barczy^sci");

     set_long("Wysoki na pi^e^c i p^o^l stopy, a szeroki na prawie trzy "+
         "stopy, typ ubrany jest raczej prostacko. Sk^orzany mundur na "+
         "kt^ory sk^lada si^e kurtka, w dodatku poplamiona, pas, bez "+
         "jakichkolwiek dodatkowych przedmiot^ow, wytarte spodnie oraz "+
         "buty. Stra^znik nie nosi ^zadnego he^lmu kt^ory m^og^lby "+
         "zas^lania^c jego ^lysin^e, w d^loniach za^s dzier^zy prost^a "+
         "pik^e. M^etne, b^l^ekitnookie oczy kt^orymi ci^egle omiata "+
         "otoczenie nie okazuj^a ^zadnych emocji.\n");

     set_stats (({ 70, 60, 60, 35, 80 }));//FIXME
     set_skill()//FIXME
     add_prop(CONT_I_WEIGHT, 80000);
     add_prop(CONT_I_HEIGHT, 165);
     add_prop(LIVE_I_NEVERKNOWN, 1);

    set_act_time(40);
    add_act("emote g^lo^sno zbiera flegm^e i pluje na ulic^e.");
    add_act("podlub w nosie");
    add_act("ziewnij");
    add_act("emote spogl�da na niebo.");

    set_cact_time(10);
    add_cact("krzyknij Hyyyyr na niego!");
    add_cact("krzyknij Bij, zabij!");

    //add_armour("/d/Standard/items/zbroje/kirysy/srebrny_elegancki_XLc.c");
    //TODO add_armour(cos);
    //TODO clone_object(sdf)->move(npc);

    add_ask(({"ratusz"}), VBFC_ME("pyt_o_ratusz"));
    add_ask(({"^swi^atynie"}), VBFC_ME("pyt_o_swiatynie"));
    add_ask(({"stra^znik^ow","stra^z"}), VBFC_ME("pyt_o_straz"));
    add_ask(({"garnizon"}), VBFC_ME("pyt_o_garnizon"));
    add_ask(({"burmistrza","Nevilla","nevilla"}),VBFC_ME("pyt_o_burmistrza"));
    set_default_answer(VBFC_ME("default_answer"));

 }

    void
    powiedz_gdy_jest(object player, string tekst)
    {
        if (environment(this_object()) == environment(player))
        this_object()->command(tekst);
    }

    string
    pyt_o_ratusz()
    {
        set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
            "powiedz do "+ this_player()->query_name(PL_BIE) +" Jak mo^zna "+
            "nie wiedzie^c "+
            "pan"+this_player()->koncowka("ie","i")+" gdzie ratusz stoi! "+
            "Przecie to samo centrum miasta z t^a ceglan^a wie^z^a ino.");
        return "";
    }
    string
    pyt_o_swiatynie()
    {
        set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
            "powiedz do "+ this_player()->query_name(PL_BIE) +
            "Pan"+this_player()->koncowka("ie","i")+" widzi "+
            "pan"+this_player()->koncowka("","i")+" ino tamt^a bia^l^a "+
            "wie^ze. To ino tam.");
        return "";
    }
    string
    pyt_o_straz()
    {
        set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
            "powiedz do "+ this_player()->query_name(PL_BIE) +" Tak, to ja!");
        return "";
    }
    string
    pyt_o_garnizon()
    {
        set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
            "powiedz do "+ this_player()->query_name(PL_BIE) +
            "Garnizon pan"+this_player()->koncowka("ie","i")+" to "+
            "ten du^zy kamienny budynek na ws... wsch... Tam gdzie s^lo^nce "+
            "si^e pojawia!");
        return "";
    }
    string
    pyt_o_burmistrza()
    {
        set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
            "powiedz do "+ this_player()->query_name(PL_BIE) +" To ten... "+
            "jak mu tam bylo... Neville!");
        return "";
    }
    string
    default_answer()
    {
        set_alarm(2.0, 0.0, "powiedz_gdy_jest", this_player(),
        "rozloz rece");
        return "";
    }


    void
    add_introduced(string imie_mia, string imie_bie)
    {
        set_alarm(2.0, 0.0, "return_introduce", find_player(imie_mia));
    }
    void
    return_introduce(object ob)
    {
    command("podrap sie po glowie");
    command("powiedz I tak nie zapami^etam.");
    }


    void
emote_hook(string emote, object wykonujacy)
{
    switch (emote)
    {
        case "kopnij": set_alarm(1.5, 0.0, "nienajlepszy", wykonujacy);
                    break;
        case "spoliczkuj": set_alarm(1.5, 0.0, "nienajlepszy", wykonujacy);
                    break;
        case "opluj" : set_alarm(2.5, 0.0, "nienajlepszy", wykonujacy);
                    break;
        case "poca^luj":
              {
              if(wykonujacy->query_gender() == 1)
                switch(random(2))
                 {
                 case 0:command("zarechocz"); break;
                 case 1:command("powiedz Jeszcze raz!"); break;
                }
              else
                {
                   switch(random(2))
                   {
                   case 0:command("emote czerwienieje ze z^lo^sci."); break;
                   case 1:command("kopnij "+
                        OB_NAME(wykonujacy)); break;
                   }
                }

                break;
                }
        case "prychnij":
              {
                switch(random(1))
                 {
                 case 0:command("spojrzyj lekcewazaco "+
                     "na "+ OB_NAME(wykonujacy)); break;
                 }
                break;
               }
        case "przytul": set_alarm(2.5, 0.0, "malolepszy", wykonujacy);
                   break;
        case "pog^laszcz": set_alarm(2.5, 0.0, "malolepszy", wykonujacy);
                   break;
    }
}

void
malolepszy(object wykonujacy)
{

   switch(random(2))
   {
     case 0: command("powiedz Hola, hola!"); break;
     case 1: command("powiedz Nie r�b tak!"); break;

   }
}

void
nienajlepszy(object wykonujacy)
{
   switch(random(5))
   {
      case 0..3: command("kopnij "+OB_NAME(wykonujacy));
              command("powiedz Wyno^s si^e!");break;
      case 4: command("powiedz Wynocha!"); break;
   }
}

