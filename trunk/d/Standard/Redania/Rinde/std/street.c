#include "dir.h"

inherit RINDE_STD;

#include <mudtime.h>
#include <language.h>
#include <stdproperties.h>


void
create_street()
{
}

nomask void
create_rinde()
{
    set_long("@@dlugi_opis@@"); // no i teraz nie trzeba dawac set_long
        // wystarczy tylko zrobic funkcje dlugi_opis
        // zwracajaca opis lokacji

    // tutaj np. eventy i inne rzeczy wspolne dla lokacji rinde
    add_item(({"miasto","miasteczko"}), "Tak, to jest w�a�nie Rinde!\n");

    add_event("@@event_ulicy_rinde:"+file_name(this_object())+"@@");
    set_event_time(300, 200);

    add_sit("pod �cian�", "pod �cian�", "spod �ciany", 0);
    add_sit("na bruku", "na bruku", "z bruku", 0);

    add_prop(ROOM_I_TYPE,ROOM_IN_CITY);

    create_street();

    add_prop(ROOM_I_WSP_Y, WSP_Y_RINDE); //Wsp�rz�dne, do systemu pogodowego.
    add_prop(ROOM_I_WSP_X, WSP_X_RINDE);
}

string
event_ulicy_rinde()
{
    switch (pora_dnia())
    {
        case MT_RANEK:
        case MT_POLUDNIE:
        case MT_POPOLUDNIE:
            return ({ "Z pobliskiego domostwa dobiegaj� ci� odg�osy k��tni. Po chwili cichn�.\n",
                "Przez ulic� przebiega wrzeszcz�cy dzieciak.\n",
                "Przez ulic� przebiega banda wrzeszcz�cych dzieciak�w.\n",
                "Jaki� dzieciak ci�gle przypatruje ci si� z okna.\n",
                "Ulic� spokojnie przechodzi pies.\n",
                "Przez ulic� galopuje jaki� je�dziec.\n",
                "Stado go��bi poderwa�o si� z dachu kamieniczki.\n",
                "Chwiejnym krokiem mija ci� pijany m�czyna.\n",
                "Gdzie� niedaleko skowycz� psy.\n",
                "Zamo�nie ubrany rycerz przeje�d�a konno ulic� wzbudzaj�c boja�liwe i zazdrosne spojrzenia mieszczan.\n",
                "Ulic� przechodzi schlany krasnolud ko�ysz�c si� na boki. Widz�cy to ludzie szemraj� mi�dzy sob�.\n",
                "Goniec pocztowy szybkim truchtem przebiega ulic�.\n",
                "Nas miastem przelatuje sok�.\n",
                "Czujesz zapach gotowanych ziemniak�w.\n",
                "Powolnym krokiem przemierza ulic� stary podr�nik.\n",
                "Wyj�tkowo umi�niony m�czyzna zmierza w stron� najbli�szej karczmy.\n",
                "Panuje tu potworny gwar.\n"})[random(17)];
        default: // wieczor, noc
            return ({ "Mija ci� jaki� podejrzany typ.\n",
                "Przez ulic� przeszed� u�miechni�ty podchmielony m�czyzna.\n",
                "Czarny kot przebiega ci drog�.\n",
                "Z oddali dochodzi ci� nag�y wrzask.\n",
                "Dochodzi ci� zapach przypalonej dziczyzny.\n",
                "Gdzie� w okolicy wyje pies.\n",
                "Jaki� mieszczanin wrzeszczy, �e zosta� obrabowany.\n",
                "S�yszysz weso�e �piewy pijaczk�w.\n",
                "Z okolicznych budynk�w dochodzi ci� d�wi�k skrzypi�cych desek.\n",
                "Czujesz si� jako� nieswojo.\n",
                "Zza okna jednego z budynk�w kto� ci si� przygl�da.\n",
                "Czujesz smr�d odchod�w.\n",
                "Chodz� tu jacy� podejrzani ludzie.\n",
                "Nagle nastaje absolutna cisza.\n" })[random(14)];
    }
}

