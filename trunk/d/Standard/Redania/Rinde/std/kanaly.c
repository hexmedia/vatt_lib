#include "dir.h"

inherit REDANIA_STD;

#include <mudtime.h>
#include <language.h>
#include <stdproperties.h>

void
create_kanaly()
{
}

nomask void
create_redania()
{
    set_long("@@dlugi_opis@@");
    add_sit("pod �cian�", "pod �cian�", "spod �ciany", 0);

    add_prop(ROOM_I_TYPE,ROOM_CAVE);

    create_kanaly();
}
