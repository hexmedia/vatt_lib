/*
 * Opis oraz kod automatycznie wygenerowany za pomoca VattGen,
 * Data: Saturday 03rd of November 2007 11:31:16 AM
 */

#include <pogoda.h>
#include <stdproperties.h>
#include <mudtime.h>
#include "dir.h"

inherit PIANA_OKOLICE_TRAKT_STD;

void
create_trakt()
{
    set_short("Brukowany trakt");
    set_long("@@dlugi_opis@@");
    add_exit(PIANA_OKOLICE_TRAKT_LOKACJE + "trakt_04.c","nw",0,FATIGUE,0);
    add_exit(PIANA_OKOLICE_TRAKT_LOKACJE + "trakt_06.c","e",0,FATIGUE,0);
    add_prop(ROOM_I_INSIDE,0);

}

public string
exits_description()
{
    return "Trakt prowadzi na p^o^lnocny zach^od i wsch^od.\n";
}

string
dlugi_opis()
{
    string str = "";

    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="Na p^o^lnocy ci^agn^a si^e pokryte ^sniegiem pola uprawne. T"+
             "rakt bez w^atpienia zosta^l niedawno wybudowany, czego dowod"+
             "zi r^owno u^lo^zony, jeszcze nie zniszczony przez ko^la i ko"+
             "pyta, kamie^n. . Trakt, bez wyrw czy kolein, wygl^ada na wy"+
             "j^atkowo zadbany i dobrze utrzymany. Krakanie wron, zamieszk"+
             "uj^acych spl^atany g^aszcz ga^l^ezi i g^este kolczaste zaro"+
             "^sla odgradzaj^ace dukt od bezkresnych r^ownin ci^agn^acych s"+
             "i^e a^z po horyzont, nape^lnia powietrze irytuj^acym dla uch"+
             "a d^xwi^ekiem. ";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str+="Na p^o^lnocy ci^agn^a si^e ^swie^zo zaorane pola uprawne . T"+
             "rakt bez w^atpienia zosta^l niedawno wybudowany, czego dowod"+
             "zi r^owno u^lo^zony, jeszcze nie zniszczony przez ko^la i ko"+
             "pyta, kamie^n. Wzd^lu^z traktu ci^agnie si^e niezbyt g^l^ebo"+
             "ki r^ow poro^sni^ety traw^a. Trakt, bez wyrw czy kolein, wyg"+
             "l^ada na wyj^atkowo zadbany i dobrze utrzymany. Brz^eczenie "+
             "owad^ow i szczebiot ptak^ow, zamieszkuj^acych spl^atany g^as"+
             "zcz ga^l^ezi i g^este kolczaste zaro^sla odgradzaj^ace dukt "+
             "od bezkresnych r^ownin ci^agn^acych si^e a^z po horyzont, na"+
             "pe^lnia powietrze mi^lym dla ucha brz^eczeniem. ";
    }
    else if(pora_roku() == MT_LATO)
    {
        str+="Na p^o^lnocy ci^agn^a si^e pola uprawne poros^le dojrzewaj^a"+
             "cym zbo^zem . Trakt bez w^atpienia zosta^l niedawno wybudowa"+
             "ny, czego dowodzi r^owno u^lo^zony, jeszcze nie zniszczony p"+
             "rzez ko^la i kopyta, kamie^n. Wzd^lu^z traktu ci^agnie si^e "+
             "niezbyt g^l^eboki r^ow poro^sni^ety traw^a. Trakt, bez wyrw "+
             "czy kolein, wygl^ada na wyj^atkowo zadbany i dobrze utrzyman"+
             "y. Brz^eczenie owad^ow i szczebiot ptak^ow, zamieszkuj^acych"+
             " spl^atany g^aszcz ga^l^ezi i g^este kolczaste zaro^sla odgr"+
             "adzaj^ace dukt od bezkresnych r^ownin ci^agn^acych si^e a^z "+
             "po horyzont, nape^lnia powietrze mi^lym dla ucha brz^eczenie"+
             "m. ";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str+="Na p^o^lnocy ci^agn^a si^e puste ju^z po ^zniwach pola upraw"+
             "ne . Trakt bez w^atpienia zosta^l niedawno wybudowany, czego"+
             " dowodzi r^owno u^lo^zony, jeszcze nie zniszczony przez ko^l"+
             "a i kopyta, kamie^n. Wzd^lu^z traktu ci^agnie si^e niezbyt g"+
             "^l^eboki r^ow poro^sni^ety traw^a. Trakt, bez wyrw czy kolei"+
             "n, wygl^ada na wyj^atkowo zadbany i dobrze utrzymany. Brz^ec"+
             "zenie owad^ow i szczebiot ptak^ow, zamieszkuj^acych spl^atan"+
             "y g^aszcz ga^l^ezi i g^este kolczaste zaro^sla odgradzaj^ace"+
             " dukt od bezkresnych r^ownin ci^agn^acych si^e a^z po horyzo"+
             "nt, nape^lnia powietrze mi^lym dla ucha brz^eczeniem. ";
    }

    str+="\n";
    return str;
}