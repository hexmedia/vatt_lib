/* Autor: Avard
   Opis : Samaia + Faeve
   Data : 5.12.07
   Info : Stad ma kursowac prom do Oxenfurtu, a w przyszlosci do Novigradu
          i innych ciekawych miejsc. ;) */

#include <pogoda.h>
#include <stdproperties.h>
#include <mudtime.h>
#include "dir.h"

inherit PIANA_OKOLICE_TRAKT_STD;

void
create_trakt()
{
    set_short("Przy molo");
    set_long("@@dlugi_opis@@");
    add_exit(PIANA_OKOLICE_TRAKT_LOKACJE + "droga_09.c","se",0,FATIGUE,0);
    add_exit(PIANA_OKOLICE_BAGNO_LOKACJE + "bagno_02.c","s",0,FATIGUE,0);
    add_prop(ROOM_I_WSP_Y, WSP_Y_PIANA_BAGNO); //Wsp^o^lrz^edne, do systemu pogodowego.
    add_prop(ROOM_I_WSP_X, WSP_X_PIANA_BAGNO);
    add_item(({"zaro^sla","krzaki"}),"Zaro^sla, ci^agn^ace si^e po obu "+
        "stronach traktu, zdaj^a si^e narzuca^c ewentualnemu podr^o^znikowi "+
        "w^edr^owk^e jedynie wzd^lu^z drogi, jednak w^aska, odbiegaj^aca w "+
        "bok ^scie^zka pomi^edzy zbitymi k^epami krzew^ow, zdradza "+
        "mo^zliwo^s^s przedostania si^e z tej drogi na rozleg^le bagna.\n");
    add_item(({"bagna","rozleg^le bagna","^scie^zka","^scie^zki","boczna ^scie^zka"}),
        "Niewielkie, ledwo widoczne ^scie^zyny zdradzaj^a, mo^zliwo^s^c przedarcia "+
        "si^e st^ad w kierunku po^ludniowym.\n");
}

public string
exits_description()
{
    return "Trakt prowadzi na po^ludniowy wsch^od, "+
        "natomiast na po^ludniu dostrzegasz ^scie^zk^e mi^edzy zaro^slami.\n";
}

string
dlugi_opis()
{
    string str;
    str ="Leniwie szemrz^acy Pontar rozlewa si^e tutaj szeroko, gubi^ac "+
        "daleko na po^ludniu sw^oj ^zwawy, bystry bieg, w wyniku czego "+
        "wzd^lu^z brzegu ci^agn^a si^e ogromne b^lotniste mokrad^la. "+
        "Przywiewana raz po raz bagienna wo^n przywodzi na my^sl torf i "+
        "zgni^le ro^sliny, za^s wystaj^ace tu i ^owdzie z zaro^sli "+
        "spr^ochnia^le pnie drzew goszcz^a dzikie zwierz^eta i insekty, "+
        "s^lu^z^ac im za schronienie. ";
    //Tu^z do brzegu dobudowano szerokie, drewniane molo, do niego za^s
    //przycumowano poka^xny prom.
    str+=" Pomost od bagniska rozdziela do^s^c szeroki pas wysokiej, "+
        "soczy^scie zielonej trzciny, b^ed^acej schronieniem ptactwa, i "+
        "ko^lysz^acej si^e bardziej lub mniej leniwie w takt szumi^acego "+
        "wiatru i fal wywo^lywanych przez przep^lywaj^ace ^lodzie.";
    str+="\n";
    return str;
}