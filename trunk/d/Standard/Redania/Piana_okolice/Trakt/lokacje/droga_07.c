/*
 * Opis oraz kod automatycznie wygenerowany za pomoca VattGen,
 * Data: Wednesday 05th of December 2007 07:40:21 PM
 */

#include <pogoda.h>
#include <stdproperties.h>
#include <mudtime.h>
#include "dir.h"

inherit PIANA_OKOLICE_TRAKT_STD;

void
create_trakt()
{
    set_short("Utwardzony trakt");
    set_long("@@dlugi_opis@@");

    add_exit(PIANA_OKOLICE_TRAKT_LOKACJE + "droga_06.c","se",0,FATIGUE,0);
    add_exit(PIANA_OKOLICE_TRAKT_LOKACJE + "droga_08.c","nw",0,FATIGUE,0);
    add_exit(PIANA_OKOLICE_BAGNO_LOKACJE + "bagno_08.c","w",0,FATIGUE,0);
    add_prop(ROOM_I_WSP_Y, WSP_Y_PIANA_BAGNO); //Wsp^o^lrz^edne, do systemu pogodowego.
    add_prop(ROOM_I_WSP_X, WSP_X_PIANA_BAGNO);
    add_item(({"zaro^sla","krzaki"}),"Zaro^sla, ci^agn^ace si^e po obu "+
        "stronach traktu, zdaj^a si^e narzuca^c ewentualnemu podr^o^znikowi "+
        "w^edr^owk^e jedynie wzd^lu^z drogi, jednak w^aska, odbiegaj^aca w "+
        "bok ^scie^zka pomi^edzy zbitymi k^epami krzew^ow, zdradza "+
        "mo^zliwo^s^s przedostania si^e z tej drogi na rozleg^le bagna.\n");
    add_item(({"bagna","rozleg^le bagna","^scie^zka","^scie^zki","boczna ^scie^zka"}),
        "Niewielkie, ledwo widoczne ^scie^zyny zdradzaj^a, mo^zliwo^s^c przedarcia "+
        "si^e st^ad w kierunku zachodnim.\n");

}

public string
exits_description()
{
    return "Trakt prowadzi na p^o^lnocny zach^od i po^ludniowy wsch^od, "+
        "natomiast na zachodzie dostrzegasz ^scie^zk^e mi^edzy zaro^slami.\n";
}

string
dlugi_opis()
{
    string str = "";

    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="Po obu stronach drogi rosn^a krzaki, teraz pozbawione li^sci"+
             " i przykryte grub^a warstw^a ^sniegu. W stertach pokruszonyc"+
             "h kamieni wznosz^acych si^e po obu stronach drogi z trudem m"+
             "o^zna rozpozna^c pozosta^lo^sci po niewysokich kamiennych mu"+
             "rkach zbudowanych niegdy^s z bry^l piaskowca a obecnie znisz"+
             "czonych przez wiatry i deszcze, poro^sni^etych przez ma^le k"+
             "rzaczki i ruderalne zielsko. Przydro^zne k^epy, pokryte drob"+
             "niutkimi sopelkami i ^sniegiem, leszczyn rosn^a coraz g^e^sc"+
             "iej si^egaj^ac wiotkimi ga^l^azkami na drog^e. ";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str+="Po obu stronach drogi g^esto rosn^a rozkwitaj^ace krzaki. W "+
             "stertach pokruszonych kamieni wznosz^acych si^e po obu stron"+
             "ach drogi z trudem mo^zna rozpozna^c pozosta^lo^sci po niewy"+
             "sokich kamiennych murkach zbudowanych niegdy^s z bry^l piask"+
             "owca a obecnie zniszczonych przez wiatry i deszcze, poro^sni"+
             "^etych przez ma^le krzaczki i ruderalne zielsko. Przydro^zne"+
             " k^epy, pokryte m^lodymi li^s^cmi, leszczyn rosn^a coraz g^e"+
             "^sciej si^egaj^ac wiotkimi ga^l^azkami na drog^e. ";
    }
    else if(pora_roku() == MT_LATO)
    {
        str+="Po obu stronach drogi rosn^a w pe^lni rozwini^ete, g^este kr"+
             "zaki. W stertach pokruszonych kamieni wznosz^acych si^e po o"+
             "bu stronach drogi z trudem mo^zna rozpozna^c pozosta^lo^sci "+
             "po niewysokich kamiennych murkach zbudowanych niegdy^s z bry"+
             "^l piaskowca a obecnie zniszczonych przez wiatry i deszcze, "+
             "poro^sni^etych przez ma^le krzaczki i ruderalne zielsko. Prz"+
             "ydro^zne k^epy, pokryte li^s^cmi i zielonymi orzechami, lesz"+
             "czyn rosn^a coraz g^e^sciej si^egaj^ac wiotkimi ga^l^azkami "+
             "na drog^e. ";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str+="Po obu stronach drogi rosn^a zielone krzaki o jeszcze niezwi"+
             "^edni^etych li^sciach. W stertach pokruszonych kamieni wznos"+
             "z^acych si^e po obu stronach drogi z trudem mo^zna rozpozna"+
             "^c pozosta^lo^sci po niewysokich kamiennych murkach zbudowany"+
             "ch niegdy^s z bry^l piaskowca a obecnie zniszczonych przez w"+
             "iatry i deszcze, poro^sni^etych przez ma^le krzaczki i ruder"+
             "alne zielsko. Przydro^zne k^epy, pokryte ^z^o^ltymi li^s^cmi"+
             " i dojrza^lymi orzechami, leszczyn rosn^a coraz g^e^sciej si"+
             "^egaj^ac wiotkimi ga^l^azkami na drog^e. ";
    }

    str+="\n";
    return str;
}