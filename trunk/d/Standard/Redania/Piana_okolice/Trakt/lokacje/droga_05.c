/*
 * Opis oraz kod automatycznie wygenerowany za pomoca VattGen,
 * Data: Wednesday 05th of December 2007 06:56:22 PM
 */

#include <pogoda.h>
#include <stdproperties.h>
#include <mudtime.h>
#include "dir.h"

inherit PIANA_OKOLICE_TRAKT_STD;

void
create_trakt()
{
    set_short("Utwardzony trakt");
    set_long("@@dlugi_opis@@");

    add_exit(PIANA_OKOLICE_TRAKT_LOKACJE + "droga_04.c","s",0,FATIGUE,0);
    add_exit(PIANA_OKOLICE_TRAKT_LOKACJE + "droga_06.c","ne",0,FATIGUE,0);
    add_exit(PIANA_OKOLICE_BAGNO_LOKACJE + "bagno_15.c","w",0,FATIGUE,0);
    add_exit(PIANA_OKOLICE_BAGNO_LOKACJE + "bagno_11.c","n",0,FATIGUE,0);
    add_prop(ROOM_I_WSP_Y, WSP_Y_PIANA_BAGNO); //Wsp^o^lrz^edne, do systemu pogodowego.
    add_prop(ROOM_I_WSP_X, WSP_X_PIANA_BAGNO);
    add_item(({"zaro^sla","krzaki"}),"Zaro^sla, ci^agn^ace si^e po obu "+
        "stronach traktu, zdaj^a si^e narzuca^c ewentualnemu podr^o^znikowi "+
        "w^edr^owk^e jedynie wzd^lu^z drogi, jednak w^aska, odbiegaj^aca w "+
        "bok ^scie^zka pomi^edzy zbitymi k^epami krzew^ow, zdradza "+
        "mo^zliwo^s^s przedostania si^e z tej drogi na rozleg^le bagna.\n");
    add_item(({"bagna","rozleg^le bagna","^scie^zka","^scie^zki","boczna ^scie^zka"}),
        "Niewielkie, ledwo widoczne ^scie^zyny zdradzaj^a, mo^zliwo^s^c przedarcia "+
        "si^e st^ad w kierunku p^o^lnocnym i zachodnim.\n");
}

public string
exits_description()
{
    return "Trakt prowadzi na p^o^lnocny wsch^od i po^ludnie, natomiast na "+
        "zachodzie i p^o^lnocy dostrzegasz ^scie^zki mi^edzy zaro^slami.\n";
}

string
dlugi_opis()
{
    string str = "";

    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="Ze stromej skarpy przy drodze zwisa kilka sopli. Droga staje"+
             " si^e w tym miejscu nieco podmok^la, za^s krzaki po obu jej "+
             "stronach ust^epuj^a miejsca suchym patykom po pokrzywach, kt"+
             "^ore czekaj^a wiosny by ponownie m^oc oparzy^c jakiego^s bez"+
             "troskiego, lub nieopatrznego w^edrowca. Pomi^edzy kamykami p"+
             "o^lo^zonymi, by wzmocni^c nawierzchnie traktu wida^c pozamar"+
             "zane ka^lu^ze. ";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str+="Ze stromej skarpy przy drodze sp^lywaj^a ma^lymi strumyczkam"+
             "i krople wody. Droga staje si^e w tym miejscu nieco podmok^l"+
             "a, za^s krzaki po obu jej stronach ust^epuj^a miejsca m^lody"+
             "m pokrzywom, kt^ore ju^z czyhaj^a by oparzy^c beztroskiego, "+
             "lub nieopatrznego w^edrowca. Pomi^edzy kamykami po^lo^zonymi"+
             ", by wzmocni^c nawierzchnie traktu, zaczyna kie^lkowa^c szor"+
             "stka trawa i jakie^s inne pospolite zielska. ";
    }
    else if(pora_roku() == MT_LATO)
    {
        str+="Ze stromej skarpy przy drodze sp^lywaj^a d^lugie nitki pn^ac"+
             "zy. Droga staje si^e w tym miejscu nieco podmok^la, za^s krz"+
             "aki po obu jej stronach ust^epuj^a miejsca g^estym pokrzywom"+
             ", kt^ore czyhaj^a by oparzy^c beztroskiego, lub nieopatrzneg"+
             "o w^edrowca. Pomi^edzy kamykami po^lo^zonymi, by wzmocni^c n"+
             "awierzchnie traktu, ro^snie bujnie szorstka trawa i jakie^s "+
             "inne pospolite zielska. ";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str+="Ze stromej skarpy przy drodze sp^lywaj^a ma^lymi strumyczkam"+
             "i krople wody. Droga staje si^e w tym miejscu nieco podmok^l"+
             "a, za^s krzaki po obu jej stronach ust^epuj^a miejsca podsyc"+
             "haj^acym pokrzywom, kt^ore nadal czyhaj^a by oparzy^c beztro"+
             "skiego, lub nieopatrznego w^edrowca. Pomi^edzy kamykami po^l"+
             "o^zonymi, by wzmocni^c nawierzchnie traktu, ro^snie trawa i "+
             "jakie^s inne wysuszone, pospolite zielska. ";
    }

    str+="\n";
    return str;
}