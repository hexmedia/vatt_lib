/*
 * Opis oraz kod automatycznie wygenerowany za pomoca VattGen,
 * Data: Sunday 11th of November 2007 09:48:02 AM
 */

#include <pogoda.h>
#include <stdproperties.h>
#include <mudtime.h>
#include "dir.h"

inherit PIANA_OKOLICE_TRAKT_STD;

void
create_trakt()
{
    set_short("Trakt w^sr^od p^ol");
    set_long("@@dlugi_opis@@");
    add_exit(PIANA_OKOLICE_TRAKT_LOKACJE + "trakt_11.c","w",0,FATIGUE,0);
    add_exit(PIANA_OKOLICE_TRAKT_LOKACJE + "trakt_13.c","ne",0,FATIGUE,0);
    add_prop(ROOM_I_INSIDE,0);

}

public string
exits_description()
{
    return "Trakt prowadzi na zach^od i p^o^lnocny wsch^od.\n";
}

string
dlugi_opis()
{
    string str = "";

    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="Trakt, bezspornie ^swie^zo u^lo^zony, pozbawiony jakichkolwi"+
             "ek skaz i dziur, wije si^e ^lagodnie, to wznosz^ac si^e po w"+
             "zg^orzu, to opadaj^ac w dolinie. Trakt bez w^atpienia zosta"+
             "^l niedawno wybudowany, czego dowodzi r^owno u^lo^zony, jeszc"+
             "ze nie zniszczony przez ko^la i kopyta, kamie^n. Trakt wije "+
             "si^e omijaj^ac rozrzucone w^sr^od p^ol k^epy niskich drzew, "+
             "wygl^adaj^a one niczym wyspy w^sr^od grud po jesiennych pokr"+
             "ywaj^acych pola. Ca^la przyroda wok^o^l ucich^la zupe^lnie, "+
             "pozbawiona roz^spiewanego ptactwa. ";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str+="Trakt, bezspornie ^swie^zo u^lo^zony, pozbawiony jakichkolwi"+
             "ek skaz i dziur, wije si^e ^lagodnie, to wznosz^ac si^e po w"+
             "zg^orzu, to opadaj^ac w dolinie. Trakt bez w^atpienia zosta"+
             "^l niedawno wybudowany, czego dowodzi r^owno u^lo^zony, jeszc"+
             "ze nie zniszczony przez ko^la i kopyta, kamie^n. Trakt wije "+
             "si^e omijaj^ac rozrzucone w^sr^od p^ol k^epy niskich drzew, "+
             "wygl^adaj^a one niczym wyspy w^sr^od p^ol poro^sni^etych wsc"+
             "hodz^acym dopiero zb^o^zami. Dobiega ci^e radosny ^swiergot "+
             "skowronka, informuj^acy o rozkwit^lej w pe^lni wio^snie. ";
    }
    else if(pora_roku() == MT_LATO)
    {
        str+="Trakt, bezspornie ^swie^zo u^lo^zony, pozbawiony jakichkolwi"+
             "ek skaz i dziur, wije si^e ^lagodnie, to wznosz^ac si^e po w"+
             "zg^orzu, to opadaj^ac w dolinie. Trakt bez w^atpienia zosta"+
             "^l niedawno wybudowany, czego dowodzi r^owno u^lo^zony, jeszc"+
             "ze nie zniszczony przez ko^la i kopyta, kamie^n. Trakt wije "+
             "si^e omijaj^ac rozrzucone w^sr^od p^ol k^epy niskich drzew, "+
             "wygl^adaj^a one niczym wyspy w^sr^od p^ol poro^sni^etych doj"+
             "rzewaj^acymi zbo^zami. Wszystkie okoliczne zaro^sla rozbrzmi"+
             "ewaj^a wrzaw^a, nierzadko wr^ecz kakofoni^a ptasich treli. ";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str+="Trakt, bezspornie ^swie^zo u^lo^zony, pozbawiony jakichkolwi"+
             "ek skaz i dziur, wije si^e ^lagodnie, to wznosz^ac si^e po w"+
             "zg^orzu, to opadaj^ac w dolinie. Trakt bez w^atpienia zosta"+
             "^l niedawno wybudowany, czego dowodzi r^owno u^lo^zony, jeszc"+
             "ze nie zniszczony przez ko^la i kopyta, kamie^n. Trakt wije "+
             "si^e omijaj^ac rozrzucone w^sr^od p^ol k^epy niskich drzew, "+
             "wygl^adaj^a one niczym wyspy w^sr^od p^ol pokrytych ^swie^zy"+
             "mi r^zyskami. Po okolicy niesie si^e, raz po raz, krzyk nawo"+
             "^luj^acych si^e do lotu w przestworza ^zurawi. ";
    }

    str+="\n";
    return str;
}