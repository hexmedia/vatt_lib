/*
 * Opis oraz kod automatycznie wygenerowany za pomoca VattGen,
 * Data: Saturday 03rd of November 2007 11:13:47 AM
 */

#include <pogoda.h>
#include <stdproperties.h>
#include <mudtime.h>
#include "dir.h"

inherit PIANA_OKOLICE_TRAKT_STD;

void
create_trakt()
{
    set_short("Brukowany trakt");
    set_long("@@dlugi_opis@@");
    add_exit(PIANA_OKOLICE_TRAKT_LOKACJE + "trakt_01.c","w",0,FATIGUE,0);
    add_exit(PIANA_OKOLICE_TRAKT_LOKACJE + "trakt_03.c","ne",0,FATIGUE,0);
    add_prop(ROOM_I_INSIDE,0);

}

public string
exits_description()
{
    return "Trakt prowadzi na zach^od i p^o^lnocny wsch^od.\n";
}

string
dlugi_opis()
{
    string str = "";

    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="Krakanie wron, zamieszkuj^acych spl^atany g^aszcz ga^l^ezi i"+
             " g^este kolczaste zaro^sla odgradzaj^ace dukt od bezkresnych"+
             " r^ownin ci^agn^acych si^e a^z po horyzont, nape^lnia powiet"+
             "rze irytuj^acym dla ucha d^xwi^ekiem. Na le^z^acym nieopodal"+
             " traktu polu jedynie wiatr g^lucho dmie po^sr^od niewysokich"+
             " zasp. Trakt jest zadbany i od^snie^zony. Pola uprawne otacz"+
             "aj^ace trakt, ci^agn^ace sie prawie po horyzont, pokryte s^a"+
             " gestym ^sniegiem gdzieniegdzie tworz^acym zaspy. ";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str+="Brz^eczenie owad^ow i szczebiot ptak^ow, zamieszkuj^acych sp"+
             "l^atany g^aszcz ga^l^ezi i g^este kolczaste zaro^sla odgradz"+
             "aj^ace dukt od bezkresnych r^ownin ci^agn^acych si^e a^z po "+
             "horyzont, nape^lnia powietrze mi^lym dla ucha brz^eczeniem. "+
             "Na le^z^acym nieopodal traktu polu grupka ch^lop^ow mozolnie"+
             " zbiera dokonuje wiosennej orki. Trakt jest zadbany, a spomi"+
             "^edzy kostek usuni^eto wi^ekszo^s^c kwitn^acych chwast^ow. P"+
             "ola uprawne otaczaj^ace trakt, ci^agn^ace sie prawie po hory"+
             "zont, pokryte s^a ju^z ^swie^zo zasian^a, kie^lkuj^ac^a ro^s"+
             "linno^sci^a. ";
    }
    else if(pora_roku() == MT_LATO)
    {
        str+="Brz^eczenie owad^ow i szczebiot ptak^ow, zamieszkuj^acych sp"+
             "l^atany g^aszcz ga^l^ezi i g^este kolczaste zaro^sla odgradz"+
             "aj^ace dukt od bezkresnych r^ownin ci^agn^acych si^e a^z po "+
             "horyzont, nape^lnia powietrze mi^lym dla ucha brz^eczeniem. "+
             "Na le^z^acym nieopodal traktu polu grupka ch^lop^ow mozolnie"+
             " kosi i zbiera jakie^s zbo^ze. Trakt jest zadbany i tylko w "+
             "niewielu miejscach spomi^edzy kostek wyrastaj^a nieliczne ch"+
             "wasty. Pola uprawne otaczaj^ace trakt, ci^agn^ace sie prawie"+
             " po horyzont, pokryte s^a ju^z ^z^o^ltymi k^losami zb^o^z. ";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str+="Brz^eczenie owad^ow i szczebiot ptak^ow, zamieszkuj^acych sp"+
             "l^atany g^aszcz ga^l^ezi i g^este kolczaste zaro^sla odgradz"+
             "aj^ace dukt od bezkresnych r^ownin ci^agn^acych si^e a^z po "+
             "horyzont, nape^lnia powietrze mi^lym dla ucha brz^eczeniem. "+
             "Na le^z^acym nieopodal traktu polu grupka ch^lop^ow mozolnie"+
             " dokonuje jesiennej orki. Trakt jest zadbany, cho^c g^esto p"+
             "okryty listowiem opadni^etym z drzew. Pola uprawne otaczaj^a"+
             "ce trakt, ci^agn^ace sie prawie po horyzont, s^a br^azowe od"+
             " nagiej ziemi. ";
    }

    str+="\n";
    return str;
}