/*
 * Opis oraz kod automatycznie wygenerowany za pomoca VattGen,
 * Data: Wednesday 21st of November 2007 07:19:55 PM
 */

#include <pogoda.h>
#include <stdproperties.h>
#include <mudtime.h>
#include "dir.h"

inherit PIANA_OKOLICE_TRAKT_STD;

void
create_trakt()
{
    set_short("Trakt w^sr^od ^l^ak");
    set_long("@@dlugi_opis@@");
    add_exit(PIANA_OKOLICE_TRAKT_LOKACJE + "trakt_12","sw",0,FATIGUE,0);
    add_exit(PIANA_OKOLICE_TRAKT_LOKACJE + "trakt_14.c","e",0,FATIGUE,0);
    add_prop(ROOM_I_INSIDE,0);

}

public string
exits_description()
{
    return "Trakt prowadzi na po^ludniowy zach^od i wsch^od.\n";
}

string
dlugi_opis()
{
    string str = "";

    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="Trakt jest zadbany i od^snie^zony. Trakt zmierza ^lagodnym s"+
             "padem w d^o^l, po^sr^od pag^ork^ow pokrytych sk^apana w biel"+
             "i ^l^ak^a, by dalej rozpocz^a^c sw^a m^ecz^ac^a w^edr^owk^e "+
             "ostrym podej^sciem pod g^or^e i znikn^a^c za du^z^a k^ep^a z"+
             "ieleni znajduj^acej si^e na szczycie wzniesienia. Na niewiel"+
             "kiej ^l^ace, tu^z przy trakcie wida^c wyra^xne ^slady obozow"+
             "ania wi^ekszej liczby os^ob, ^snieg st^luczony jest ko^lami "+
             "woz^ow i ko^nskimi kopytami, a w chronionym od wiatru miejsc"+
             "u czerni^a si^e dwa kr^egi po sporych ogniskach. Ca^la przyr"+
             "oda wok^o^l ucich^la zupe^lnie, pozbawiona roz^spiewanego pt"+
             "actwa. Na zakr^ecie drogi w rowie le^zy kilka kamieni wielko"+
             "^sci ludzkiej g^lowy, przykryte s^a cienk^a warstw^a ^sniegu"+
             ". ";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str+="Trakt jest zadbany, a spomi^edzy kostek usuni^eto wi^ekszo^s"+
             "^c kwitn^acych chwast^ow. Trakt zmierza ^lagodnym spadem w d"+
             "^o^l, po^sr^od pag^ork^ow pokrytych jasnozielon^a ^l^ak^a, b"+
             "y dalej rozpocz^a^c sw^a m^ecz^ac^a w^edr^owk^e ostrym podej"+
             "^sciem pod g^or^e i znikn^a^c za du^z^a k^ep^a zieleni znajd"+
             "uj^acej si^e na szczycie wzniesienia. Na niewielkiej ^l^ace,"+
             " tu^z przy trakcie wida^c wyra^xne ^slady obozowania wi^eksz"+
             "ej liczby os^ob, m^loda trawa st^luczona jest ko^lami woz^ow"+
             " i ko^nskimi kopytami, a w chronionym od wiatru miejscu czer"+
             "ni^a si^e dwa kr^egi po sporych ogniskach. Dobiega ci^e rado"+
             "sny ^swiergot skowronka, informuj^acy o rozkwit^lej w pe^lni"+
             " wio^snie. Na zakr^ecie drogi w rowie le^zy kilka kamieni wi"+
             "elko^sci ludzkiej g^lowy, nie^smia^lo wspinaj^a si^e po nich"+
             " m^lode p^edy szybko rosn^acych chwast^ow i innych ro^slin. ";
    }
    else if(pora_roku() == MT_LATO)
    {
        str+="Trakt jest zadbany i tylko w niewielu miejscach spomi^edzy k"+
             "ostek wyrastaj^a nieliczne chwasty. Trakt zmierza ^lagodnym "+
             "spadem w d^o^l, po^sr^od pag^ork^ow pokrytych soczysto-zielo"+
             "n^a ^l^ak^a, by dalej rozpocz^a^c sw^a m^ecz^ac^a w^edr^owk"+
             "^e ostrym podej^sciem pod g^or^e i znikn^a^c za du^z^a k^ep^a"+
             " zieleni znajduj^acej si^e na szczycie wzniesienia. Na niewi"+
             "elkiej ^l^ace, tu^z przy trakcie wida^c wyra^xne ^slady oboz"+
             "owania wi^ekszej liczby os^ob, wysoka trawa st^luczona jest "+
             "ko^lami woz^ow i ko^nskimi kopytami, a w chronionym od wiatr"+
             "u miejscu czerni^a si^e dwa kr^egi po sporych ogniskach. Wsz"+
             "ystkie okoliczne zaro^sla rozbrzmiewaj^a wrzaw^a, nierzadko "+
             "wr^ecz kakofoni^a ptasich treli. Na zakr^ecie drogi w rowie "+
             "le^zy kilka kamieni wielko^sci ludzkiej g^lowy, s^a prawie c"+
             "a^lkowicie zaro^sni^ete przez chwasty i inne ro^sliny. ";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str+="Trakt jest zadbany, cho^c g^esto pokryty listowiem opadni^et"+
             "ym z drzew. Trakt zmierza ^lagodnym spadem w d^o^l, po^sr^od"+
             " pag^ork^ow pokrytych ^l^ak^a, by dalej rozpocz^a^c sw^a m^e"+
             "cz^ac^a w^edr^owk^e ostrym podej^sciem pod g^or^e i znikn^a"+
             "^c za du^z^a k^ep^a zieleni znajduj^acej si^e na szczycie wzn"+
             "iesienia. Na niewielkiej ^l^ace, tu^z przy trakcie wida^c wy"+
             "ra^xne ^slady obozowania wi^ekszej liczby os^ob, przyschni^e"+
             "ta trawa st^luczona jest ko^lami woz^ow i ko^nskimi kopytami"+
             ", a w chronionym od wiatru miejscu czerni^a si^e dwa kr^egi "+
             "po sporych ogniskach. Po okolicy niesie si^e, raz po raz, kr"+
             "zyk nawo^luj^acych si^e do lotu w przestworza ^zurawi. Na za"+
             "kr^ecie drogi w rowie le^zy kilka kamieni wielko^sci ludzkie"+
             "j g^lowy, cz^e^sciowo pokrywaj^a je wi^edn^ace ju^z chwasty "+
             "i inne ro^sliny. ";
    }

    str+="\n";
    return str;
}