/*
 * Opis oraz kod automatycznie wygenerowany za pomoca VattGen,
 * Data: Wednesday 05th of December 2007 06:49:27 PM
 */

#include <pogoda.h>
#include <stdproperties.h>
#include <mudtime.h>
#include "dir.h"

inherit PIANA_OKOLICE_TRAKT_STD;

void
create_trakt()
{
    set_short("Utwardzony trakt");
    set_long("@@dlugi_opis@@");
    add_exit(PIANA_OKOLICE_TRAKT_LOKACJE + "droga_08.c","s",0,FATIGUE,0);
    add_exit(PIANA_OKOLICE_TRAKT_LOKACJE + "droga_10.c","nw",0,FATIGUE,0);
    add_exit(PIANA_OKOLICE_BAGNO_LOKACJE + "bagno_05.c","sw",0,FATIGUE,0);
    add_exit(PIANA_OKOLICE_BAGNO_LOKACJE + "bagno_02.c","w",0,FATIGUE,0);
    add_prop(ROOM_I_WSP_Y, WSP_Y_PIANA_BAGNO); //Wsp^o^lrz^edne, do systemu pogodowego.
    add_prop(ROOM_I_WSP_X, WSP_X_PIANA_BAGNO);
    add_item(({"zaro^sla","krzaki"}),"Zaro^sla, ci^agn^ace si^e po obu "+
        "stronach traktu, zdaj^a si^e narzuca^c ewentualnemu podr^o^znikowi "+
        "w^edr^owk^e jedynie wzd^lu^z drogi, jednak w^aska, odbiegaj^aca w "+
        "bok ^scie^zka pomi^edzy zbitymi k^epami krzew^ow, zdradza "+
        "mo^zliwo^s^s przedostania si^e z tej drogi na rozleg^le bagna.\n");
    add_item(({"bagna","rozleg^le bagna","^scie^zka","^scie^zki","boczna ^scie^zka"}),
        "Niewielkie, ledwo widoczne ^scie^zyny zdradzaj^a, mo^zliwo^s^c przedarcia "+
        "si^e st^ad w kierunku zachodnim.\n");

}

public string
exits_description()
{
    return "Trakt prowadzi na p^o^lnocny zach^od i po^ludnie, "+
        "natomiast na zachodzie i po^ludniowym zachodzie dostrzegasz "+
        "^scie^zki mi^edzy zaro^slami.\n";
}

string
dlugi_opis()
{
    string str = "";

    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="Na oblodzonej, a gdzieniegdzie grz^askiej powierzchni drogi "+
             "trudno jest stawia^c nogi, aby si^e nie zapa^s^c w b^locie, "+
             "czy nie po^slizgn^a^c, a zapewne niejeden wo^xnica, czy je^x"+
             "dziec przekona^l si^e tak^ze, ^ze r^ownie trudno jest sterow"+
             "a^c w takich warunkach wozem lub wierzchowcem. Wzd^lu^z drog"+
             "i ci^agn^a si^e zaro^sla w postaci nagich ga^l^ezi drzew i k"+
             "rzew^ow. Ca^la okoliczna flora zastyg^la w bezruchu, okrywaj"+
             "^ac si^e pierzyn^a puszystego ^sniegu. Trakt jest bardzo zan"+
             "iedbany, pe^lno tu zastygni^etych w zmarzni^etej ziemi kolei"+
             "n i zasp na kraw^edziach drogi. ";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str+="W miernie utwardzonej, grz^askiej powierzchni drogi swoje mi"+
             "ejsce znalaz^ly liczne koleiny - pozosta^lo^sci po przeje^"+
             "zd^zaj^acych t^edy wozach, gdzieniegdzie da si^e tak^ze zauw"+
             "a^zy^c odciski podk^ow, kt^ore wraz z koleinami wype^lni^ly "+
             "si^e wod^a tworz^ac razem rozleg^le ka^lu^ze. Wzd^lu^z drogi"+
             " ci^agn^a si^e zaro^sla rozstaczaj^ac s^lodk^a wo^n wiosenne"+
             "go kwiecia. Ca^la okoliczna flora zieleni si^e soczy^scie, w"+
             "dzieraj^ac na trakt i obsypuj^ac wszystko wok^o^l barwnym kw"+
             "ieciem. Trakt jest bardzo zaniedbany, pe^lno tu kolein i ros"+
             "n^acych prosto na drodze chwast^ow. ";
    }
    else if(pora_roku() == MT_LATO)
    {
        str+="W miernie utwardzonej, grz^askiej powierzchni drogi swoje mi"+
             "ejsce znalaz^ly liczne koleiny - pozosta^lo^sci po przeje^"+
             "zd^zaj^acych t^edy wozach, gdzieniegdzie da si^e tak^ze zauw"+
             "a^zy^c odciski podk^ow, kt^ore wraz z koleinami wype^lni^ly "+
             "si^e wod^a tworz^ac razem rozleg^le ka^lu^ze. Wzd^lu^z drogi"+
             " ci^agn^a si^e g^este zaro^sla daj^ace w^edrowcom odrobin^e "+
             "cienia w upalne, letnie dni. Ca^la okoliczna flora zieleni s"+
             "i^e soczy^scie, wdzieraj^ac na trakt, niemal zarastaj^ac go "+
             "zupe^lnie. Trakt jest bardzo zaniedbany, pe^lno tu kolein i "+
             "rozplenionych na drodze chwast^ow.";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str+="W miernie utwardzonej, grz^askiej powierzchni drogi swoje mi"+
             "ejsce znalaz^ly liczne koleiny - pozosta^lo^sci po przeje^"+
             "zd^zaj^acych t^edy wozach, gdzieniegdzie da si^e tak^ze zauw"+
             "a^zy^c odciski podk^ow, kt^ore wraz z koleinami wype^lni^ly "+
             "si^e wod^a tworz^ac razem rozleg^le ka^lu^ze. Wzd^lu^z drogi"+
             " ci^agn^a si^e g^este zaro^sla o i^scie jesiennych, ^zywych "+
             "barwach. Ca^la okoliczna flora szykuje si^e do nadej^scia zi"+
             "my, za^s trakt skry^l si^e pod dywanem barwnych li^sci. Trak"+
             "t jest bardzo zaniedbany, pe^lno tu kolein, li^sci i ga^l^az"+
             "ek na drodze.";
    }

    str+="\n";
    return str;
}