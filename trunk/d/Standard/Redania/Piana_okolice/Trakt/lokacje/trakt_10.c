/*
 * Opis oraz kod automatycznie wygenerowany za pomoca VattGen,
 * Data: Sunday 11th of November 2007 09:26:10 AM
 */

#include <pogoda.h>
#include <stdproperties.h>
#include <mudtime.h>
#include "dir.h"

inherit PIANA_OKOLICE_TRAKT_STD;

void
create_trakt()
{
    set_short("Trakt w^sr^od p^ol");
    set_long("@@dlugi_opis@@");
    add_exit(PIANA_OKOLICE_TRAKT_LOKACJE + "trakt_09.c","sw",0,FATIGUE,0);
    add_exit(PIANA_OKOLICE_TRAKT_LOKACJE + "trakt_11.c","se",0,FATIGUE,0);
    add_prop(ROOM_I_INSIDE,0);

}

public string
exits_description()
{
    return "Trakt prowadzi na po^ludniowy zach^od i po^ludniowy wsch^od.\n";
}

string
dlugi_opis()
{
    string str = "";

    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="Krakanie wron, zamieszkuj^acych spl^atany g^aszcz ga^l^ezi i"+
             " g^este kolczaste zaro^sla odgradzaj^ace dukt od bezkresnych"+
             " r^ownin ci^agn^acych si^e a^z po horyzont, nape^lnia powiet"+
             "rze irytuj^acym dla ucha d^xwi^ekiem. Na otaczaj^acych trakt"+
             " polach uprawnych, wida^c jedynie ^slady drobnych zwierz^at,"+
             " zostawionych na mi^ekkim puchu okrywaj^acym ca^l^a okolice."+
             " Trakt, bezspornie ^swie^zo u^lo^zony, pozbawiony jakichkolw"+
             "iek skaz i dziur, wije si^e ^lagodnie, to wznosz^ac si^e po "+
             "wzg^orzu, to opadaj^ac w dolinie. ";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str+="Brz^eczenie owad^ow i szczebiot ptak^ow, zamieszkuj^acych sp"+
             "l^atany g^aszcz ga^l^ezi i g^este kolczaste zaro^sla odgradz"+
             "aj^ace dukt od bezkresnych r^ownin ci^agn^acych si^e a^z po "+
             "horyzont, nape^lnia powietrze mi^lym dla ucha brz^eczeniem. "+
             "Na otaczaj^acych trakt polach uprawnych, dostrzec mo^zna pra"+
             "cuj^acych w pocie czo^la ch^lop^ow, przygotowuj^acych ziemie"+
             " pod zasiew. Trakt, bezspornie ^swie^zo u^lo^zony, pozbawion"+
             "y jakichkolwiek skaz i dziur, wije si^e ^lagodnie, to wznosz"+
             "^ac si^e po wzg^orzu, to opadaj^ac w dolinie. ";
    }
    else if(pora_roku() == MT_LATO)
    {
        str+="Brz^eczenie owad^ow i szczebiot ptak^ow, zamieszkuj^acych sp"+
             "l^atany g^aszcz ga^l^ezi i g^este kolczaste zaro^sla odgradz"+
             "aj^ace dukt od bezkresnych r^ownin ci^agn^acych si^e a^z po "+
             "horyzont, nape^lnia powietrze mi^lym dla ucha brz^eczeniem. "+
             "Na otaczaj^acych trakt polach uprawnych, dostrzec mo^zna pra"+
             "cuj^acych w pocie czo^la ch^lop^ow, piel^egnuj^acych zasiane"+
             " pola. Trakt, bezspornie ^swie^zo u^lo^zony, pozbawiony jaki"+
             "chkolwiek skaz i dziur, wije si^e ^lagodnie, to wznosz^ac si"+
             "^e po wzg^orzu, to opadaj^ac w dolinie. ";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str+="Brz^eczenie owad^ow i szczebiot ptak^ow, zamieszkuj^acych sp"+
             "l^atany g^aszcz ga^l^ezi i g^este kolczaste zaro^sla odgradz"+
             "aj^ace dukt od bezkresnych r^ownin ci^agn^acych si^e a^z po "+
             "horyzont, nape^lnia powietrze mi^lym dla ucha brz^eczeniem. "+
             "Na otaczaj^acych trakt polach uprawnych, dostrzec mo^zna pra"+
             "cuj^acych w pocie czo^la ch^lop^ow, zbieraj^acych ju^z ostat"+
             "nie plony w tym roku. Trakt, bezspornie ^swie^zo u^lo^zony, "+
             "pozbawiony jakichkolwiek skaz i dziur, wije si^e ^lagodnie, "+
             "to wznosz^ac si^e po wzg^orzu, to opadaj^ac w dolinie. ";
    }

    str+="\n";
    return str;
}