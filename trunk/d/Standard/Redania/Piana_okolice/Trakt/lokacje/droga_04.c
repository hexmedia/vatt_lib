/*
 * Opis oraz kod automatycznie wygenerowany za pomoca VattGen,
 * Data: Wednesday 05th of December 2007 06:59:59 PM
 */

#include <pogoda.h>
#include <stdproperties.h>
#include <mudtime.h>
#include "dir.h"

inherit PIANA_OKOLICE_TRAKT_STD;

void
create_trakt()
{
    set_short("Utwardzony trakt");
    set_long("@@dlugi_opis@@");

    add_exit(PIANA_OKOLICE_TRAKT_LOKACJE + "droga_03.c","s",0,FATIGUE,0);
    add_exit(PIANA_OKOLICE_TRAKT_LOKACJE + "droga_05.c","n",0,FATIGUE,0);
    add_exit(PIANA_OKOLICE_BAGNO_LOKACJE + "bagno_18.c","w",0,FATIGUE,0);
    add_exit(PIANA_OKOLICE_BAGNO_LOKACJE + "bagno_15.c","nw",0,FATIGUE,0);
    add_prop(ROOM_I_WSP_Y, WSP_Y_PIANA_BAGNO); //Wsp�rz^edne, do systemu pogodowego.
    add_prop(ROOM_I_WSP_X, WSP_X_PIANA_BAGNO);
    add_item(({"zaro^sla","krzaki"}),"Zaro^sla, ci^agn^ace si^e po obu "+
        "stronach traktu, zdaj^a si^e narzuca^c ewentualnemu podr^o^znikowi "+
        "w^edr^owk^e jedynie wzd^lu^z drogi, jednak w^aska, odbiegaj^aca w "+
        "bok ^scie^zka pomi^edzy zbitymi k^epami krzew^ow, zdradza "+
        "mo^zliwo^s^s przedostania si^e z tej drogi na rozleg^le bagna.\n");
    add_item(({"bagna","rozleg�e bagna","^scie^zka","^scie^zki","boczna ^scie^zka"}),
        "Niewielkie, ledwo widoczne ^scie^zyny zdradzaj^a, mo^zliwo^s^c przedarcia "+
        "si^e st^ad w kierunku zachodnim.\n");
}

public string
exits_description()
{
    return "Trakt prowadzi na p^o^lnoc i po^ludnie, natomiast na zachodzie "+
        "i p^o^lnocnym zachodzie dostrzegasz ^scie^zki mi^edzy zaro^slami.\n";
}

string
dlugi_opis()
{
    string str = "";

    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="Wzd^lu^z drogi ci^agn^a si^e zaro^sla w postaci nagich ga^l"+
             "^ezi drzew i krzew^ow. Trakt jest bardzo zaniedbany, pe^lno t"+
             "u zastygni^etych w zmarzni^etej ziemi kolein i zasp na kraw"+
             "^edziach drogi. Trakt zosta^l dok^ladnie wydeptany przez setk"+
             "i przechodz^acych t^edy st^op i gdzieniegdzie tylko pokrywa "+
             "go kilka plam szarego ^sniegu. Pod ^sniegiem widniej^a zarys"+
             "y korzeni, kt^ore wyci^agn^e^ly si^e a^z na drog^e. ";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str+="Wzd^lu^z drogi ci^agn^a si^e zaro^sla rozstaczaj^ac s^lodk^a"+
             " wo^n wiosennego kwiecia. Trakt jest bardzo zaniedbany, pe^l"+
             "no tu kolein i rosn^acych prosto na drodze chwast^ow. Trakt "+
             "zosta^l dok^ladnie wydeptany przez setki przechodz^acych t^e"+
             "dy st^op i gdzieniegdzie tylko porasta go kilka k^ep m^lodej"+
             " trawy. Mi^edzy zieleniej^ac^a si^e traw^a widniej^a korzeni"+
             "e, kt^ore wyci^agn^e^ly si^e a^z na drog^e. ";
    }
    else if(pora_roku() == MT_LATO)
    {
        str+="Wzd^lu^z drogi ci^agn^a si^e g^este zaro^sla daj^ace w^edrow"+
             "com odrobin^e cienia w upalne, letnie dni. Trakt jest bardzo"+
             " zaniedbany, pe^lno tu kolein i rozplenionych na drodze chwa"+
             "st^ow. Trakt zosta^l dok^ladnie wydeptany przez setki przech"+
             "odz^acych t^edy st^op i gdzieniegdzie tylko porasta go kilka"+
             " k^ep niezbyt wysokiej trawy. Mi^edzy wybuja^l^a traw^a widn"+
             "iej^a korzenie, kt^ore wyci^agn^e^ly si^e a^z na drog^e. ";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str+="Wzd^lu^z drogi ci^agn^a si^e g^este zaro^sla o i^scie jesien"+
             "nych, ^zywych barwach. Trakt jest bardzo zaniedbany, pe^lno "+
             "tu kolein, li^sci i ga^l^azek na drodze. Trakt zosta^l dok^l"+
             "adnie wydeptany przez setki przechodz^acych t^edy st^op i gd"+
             "zieniegdzie tylko porasta go kilka k^ep zszarza^lej trawy. M"+
             "i^edzy opadni^etymi li^s^cmi widniej^a zarysy korzeni, kt^or"+
             "e wyci^agn^e^ly si^e a^z na drog^e. ";
    }

    str+="\n";
    return str;
}