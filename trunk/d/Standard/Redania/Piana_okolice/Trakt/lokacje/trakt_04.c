/*
 * Opis oraz kod automatycznie wygenerowany za pomoca VattGen,
 * Data: Saturday 03rd of November 2007 11:31:16 AM
 */

#include <pogoda.h>
#include <stdproperties.h>
#include <mudtime.h>
#include "dir.h"

inherit PIANA_OKOLICE_TRAKT_STD;

void
create_trakt()
{
    set_short("Brukowany trakt");
    set_long("@@dlugi_opis@@");
    add_exit(PIANA_OKOLICE_TRAKT_LOKACJE + "trakt_03.c","nw",0,FATIGUE,0);
    add_exit(PIANA_OKOLICE_TRAKT_LOKACJE + "trakt_05.c","se",0,FATIGUE,0);
    add_prop(ROOM_I_INSIDE,0);

}

public string
exits_description()
{
    return "Trakt prowadzi na p^o^lnocny zach^od i po^ludniowy wsch^od.\n";
}

string
dlugi_opis()
{
    string str = "";

    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="Du^ze kamienne p^lyty u^zyte do wy^lo^zenia nawierzchni trak"+
             "tu, prawie ca^le pokryte s^a grub^a warstwa zamarzni^etego "+
             "^sniegu. Okoliczne pola uprawne pokryte s^a bia^lym, g^estym "+
             "puchem, przykrywaj^acym wszystko delikatn^a ko^lderk^a. W st"+
             "ertach pokruszonych kamieni wznosz^acych si^e po obu stronac"+
             "h drogi z trudem mo^zna rozpozna^c pozosta^lo^sci po niewyso"+
             "kich kamiennych murkach zbudowanych niegdy^s z bry^l piaskow"+
             "ca a obecnie zniszczonych przez wiatry i deszcze, poro^sni^e"+
             "tych przez ma^le krzaczki i ruderalne zielsko. ";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str+="Du^ze kamienne p^lyty u^zyte do wy^lo^zenia nawierzchni trak"+
             "tu, gdzieniegdzie nosz^a na sobie ma^le nadtopione przez wio"+
             "senne deszcze kupki ^sniegu. Okoliczne pola uprawne pokryte "+
             "s^a kie^lkuj^ac^a ro^slinno^sci^a, gdzieniegdzie przebijaj^a"+
             "c^a sie spod cienkich warstewek stopionego ^sniegu. W sterta"+
             "ch pokruszonych kamieni wznosz^acych si^e po obu stronach dr"+
             "ogi z trudem mo^zna rozpozna^c pozosta^lo^sci po niewysokich"+
             " kamiennych murkach zbudowanych niegdy^s z bry^l piaskowca a"+
             " obecnie zniszczonych przez wiatry i deszcze, poro^sni^etych"+
             " przez ma^le krzaczki i ruderalne zielsko. ";
    }
    else if(pora_roku() == MT_LATO)
    {
        str+="Du^ze kamienne p^lyty u^zyte do wy^lo^zenia nawierzchni trak"+
             "tu, pokryte s^a kurzem i py^lem naniesionym przez ciep^ly le"+
             "tni wiatr. Okoliczne pola uprawne pokryte s^a bujn^a ro^slin"+
             "no^sci^a, w ^sr^od kt^orej zapewne kwitnie tak^ze ^zycie dro"+
             "bnych zwierz^at. W stertach pokruszonych kamieni wznosz^acyc"+
             "h si^e po obu stronach drogi z trudem mo^zna rozpozna^c pozo"+
             "sta^lo^sci po niewysokich kamiennych murkach zbudowanych nie"+
             "gdy^s z bry^l piaskowca a obecnie zniszczonych przez wiatry "+
             "i deszcze, poro^sni^etych przez ma^le krzaczki i ruderalne z"+
             "ielsko. ";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str+="Du^ze kamienne p^lyty u^zyte do wy^lo^zenia nawierzchni trak"+
             "tu, pokryte s^a b^lotem naniesionym przez potoki wody sp^lyw"+
             "aj^ace z okolicznych teren^ow. Okoliczne pola uprawne pokryt"+
             "e s^a wysuszon^a ro^slinno^sci^a, kt^or^a powoli zaczyna obu"+
             "miera^c. W stertach pokruszonych kamieni wznosz^acych si^e p"+
             "o obu stronach drogi z trudem mo^zna rozpozna^c pozosta^lo^s"+
             "ci po niewysokich kamiennych murkach zbudowanych niegdy^s z "+
             "bry^l piaskowca a obecnie zniszczonych przez wiatry i deszcz"+
             "e, poro^sni^etych przez ma^le krzaczki i ruderalne zielsko. ";
    }

    str+="\n";
    return str;
}