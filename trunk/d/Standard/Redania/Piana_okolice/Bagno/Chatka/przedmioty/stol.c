
// kod popelniony przez Seda, opisy popelnione przez Aine 
// 04.01.2008

inherit "/std/container";

#include <pl.h>
#include <macros.h>
#include <filter_funs.h>
#include <stdproperties.h>
#include <composite.h>
#include <cmdparse.h>
#include <object_types.h>
#include <materialy.h>

void
create_container()
{
    dodaj_przym("ci^e^zki","ci^e^zcy");
    dodaj_przym("drewniany","drewniani");
    ustaw_nazwe("st^o^l");

    set_long("Ci^e^zki drewniany st^o^l wykonany zosta^l najprawdopodobniej "
    +"z bukowego drewna. Na jego blacie wida^c ogromne s^loje, "
    +"kt^ore dodaj^a temu meblowi pewnego uroku. Nogi sto^lu w "
    +"misterny spos^ob wyrze^xbione zosta^ly na wz^or wrastaj^acych "
    +"w ziemi^e korzeni. Czas jednak odbi^l na nim swoje pi^etno "
    +"w postaci licznych obi^c i zarysowa^n, a tak^ze slad^ow ucztowania "
    +"kornik^ow."
    +"@@opis_sublokacji| Na blacie |na|.|||lezy |leza |lezy @@\n");
    
    add_prop(CONT_I_MAX_VOLUME, 40000);
    add_prop(CONT_I_VOLUME, 20000);
    add_prop(CONT_I_CANT_WLOZ_DO, 1);
    add_prop(CONT_I_WEIGHT, 15000);
    add_prop(CONT_I_MAX_WEIGHT, 50000);
    add_prop(CONT_I_DONT_SHOW_CONTENTS, 1);
    add_prop(OBJ_I_VALUE, 20);
    
    set_type(O_MEBLE); 
    add_subloc("na");
    ustaw_material(MATERIALY_DR_BUK);
}

