/* Autor: Avard
   Opis : Aine
   Data : 15.01.08 */

inherit "/std/door";

#include <pl.h>
#include <macros.h>
#include "dir.h"

void
create_door()
{
    ustaw_nazwe("drzwi");

    set_other_room(PIANA_OKOLICE_BAGNO_CHATKA_LOKACJE + "chatka.c");
    set_door_id("DRZWI_DO_CHATKI_NA_BAGNACH_PIANY");
    set_door_desc("Ma^le, pomalowane na zielono drzwi spokojnie komponuj^a "+
        "si^e w ciemny kolor drewnianych belek chatki. Niczym by si^e nie "+
        "wyr^o^znia^ly gdyby nie wykonana na ich powierzchni ma^la "+
        "p^laskorze^xba przedstawiaj^aca kobiete stoj^ac^a samotnie na "+
        "wzg^orzu. Targane wiatrem, nieokie^lznane pasma w^los^ow niemal "+
        "ca^lkowicie przys^laniaj^a jej twarz nie zas^laniaj^ac tylko oczu. "+
        "Ich hipnotyzuj^ace spojrzenie przelewa ogrom ^zalu i sprawia, ^ze "+
        "^zal ten jest tak realny jak powietrze kt^orym oddychasz. Jej "+
        "uniesiona do g^ory d^lo^n przecina otaczaj^ac^a j^a pustk^e i "+
        "smutek w nieznacznym ge^scie po^zegnania. \n");

    set_open_desc("");
    set_closed_desc("");

    set_pass_command(({"chatka","drzwi"}),"przez drzwi",
        "z zewn^atrz");

    add_prop(OBJ_I_NIE_WCIAGANY, 1);

    set_open(0);
    set_locked(0);

    set_lock_name("zamek");
}