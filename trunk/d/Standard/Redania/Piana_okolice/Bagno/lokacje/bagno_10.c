/**
 * \file /d/Standard/Redania/Piana_okolice/Bagno/lokacje/bagno_10.c
 *
 * Bagno opodal Piany.
 *
 * @author Aine
 * @date   2008
 */

#include "dir.h"
#include <macros.h>
#include <pogoda.h>
#include <mudtime.h>
#include <stdproperties.h>

inherit PIANA_OKOLICE_BAGNO_STD;

void
create_piana_bagno()
{
    set_short("Cuchn^ace bagnisko");

    add_t_exit("07", "nw");
    add_t_exit("08", "ne");
    add_t_exit("14", "s");
    add_t_exit("13", "sw");

    ustaw_trudnosc_bagna(2);
}

public string
exits_description()
{
    return "Ledwo widoczna dr^o^zka prowadzi na wsch^od, po^ludnie i po^ludniowy-wsch^od.\n";
}

string
dlugi_opis()
{
    string str = "";

    if (CZY_JEST_SNIEG(this_object()))
    {
        str += "Cisza i spok^oj jak^a wprowadzi^la panuj^aca tu zima co chwil^e zostaj^e"+
            " zm^acona przez przera^xliwy krzyk jakiego^s zb^l^akanego ptaka. Zmro^zona"+
            " zimowym ch^lodem ziemia w tej cz^e^sci bagien wydaje si^e by^c pewniejszym"+
            " gruntem, jednak z ka^zdym krokiem nale^zy zachowa^c ostro^zno^s^c. Z pewno"+
            "^sci^a bagna te poch^lon^e^ly ju^z niejednego nieroztropnego podr^o^znika."+
            " Smr^od w tym miejscu nieco ust^api^l daj^ac mo^zliwo^s^c zaczerpni^ecia"+
            " pe^lniejszego oddechu bez nara^zania si^e na nudno^sci. ";
    }
    else if (pora_roku() == MT_WIOSNA)
    {
        str += "Martwe krzewy, przegni^le pnie, zb^l^akany krzyk ptaka - to obraz"+
            " niegdy^s rosn^acego tu, pot^e^znego boru, w kt^orym wiosna rozpo^sciera^la"+
            " obficie swoje dobra, a w kt^orym teraz tylko w nielicznych miejscach"+
            " mo^zna odnale^x^c jej ciche i delikatne dzia^lania. Cuchn^aca, wilgotna"+
            "  ziemia niczym spragniona czyjego^s ^zycia ^lapie ka^zdy tw^oj krok i zdaje si^e"+
            " go poch^lania^c. Smr^od w tym miejscu nieco ust^api^l daj^ac mo^zliwo^s^c"+
            " zaczerpni^ecia pe^lniejszego oddechu bez nara^zania si^e na nudno^sci. ";
    }
    else if (pora_roku() == MT_LATO)
    {
        str += "Smr^od w tym miejscu nieco ust^api^l daj^ac mo^zliwo^s^c zaczerpni^ecia"+
            " pe^lniejszego oddechu bez nara^zania si^e na nudno^sci. Cuchn^aca, wilgotna"+
            " ziemia niczym spragniona czyjego^s ^zycia ^lapie ka^zdy tw^oj krok i zdaje si^e"+
            " go poch^lania^c. Para unosz^aca si^e nad ziemi^a wij^e si^e w swoim gro^teskowym"+
            " ta^ncu mami^ac z^ludnie twoje zmys^ly i jakby zapraszaj^acc by^s wraz z ni^a odda^l"+
            " si^e ta^ncu na tej nios^acej nie^bezpiecze^nstwo przestrzeni... ";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str += "Martwe krzewy, przegni^le pnie, zb^l^akany krzyk ptaka - to obraz"+
            " niegdy^s rosn^acego tu, pot^e^znego boru, w kt^orym natura rozpo^sciera^la"+
            " obficie swoje dobra, a w kt^orym teraz tylko w nielicznych miejscach"+
            " mo^zna odnale^x^c jej ciche i delikatne dzia^lania. Cuchn^aca, wilgotna"+
            " ziemia niczym spragniona czyjego^s ^zycia ^lapie ka^zdy tw^oj krok i zdaje si^e"+
            " go poch^lania^c. Smr^od w tym miejscu nieco ust^api^l daj^ac mo^zliwo^s^c"+
            " zaczerpni^ecia pe^lniejszego oddechu bez nara^zania si^e na nudno^sci. Brunatne"+
            " fa^ldy b^lota zlewaj^a si^e ze sob^a czyni^ac w^edr^owk^e t^edy jeszcze trudniejsz^a"+
            " i jeszcze bardziej zdradliw^a. ";
    }

    return str + "\n";
}
