/**
 * \file /d/Standard/Redania/Piana_okolice/Bagno/lokacje/bagno_16.c
 *
 * Bagno opodal Piany.
 *
 * @author Aine
 * @date   2008
 */

#include "dir.h"
#include <macros.h>
#include <pogoda.h>
#include <mudtime.h>
#include <stdproperties.h>

inherit PIANA_OKOLICE_BAGNO_STD;
inherit "/lib/drink_water";

void
create_piana_bagno()
{
    set_short("Martwy, bagnisty las");

    add_t_exit("12", "n");
    add_t_exit("13", "ne");
    add_t_exit("19", "se");

    set_drink_places(({"z rzeki","z pontaru","z Pontaru"}));
    add_sit(({"nad rzek^a","nad pontarem","nad Pontarem"}),
        "nad rzek^a","z nad rzeki",0);

    add_item(({"Pontar","pontar","rzek^e"}),"P^lyn^acy tu od tysi^ecy lat "+
        "szeroki Pontar zdaje si^e by^c wci^a^z nieujarzmionym i niezdobytym, "+
        "wyznacza nie tylko granice mi^edzy kr^olestwami Redanii i Temeri, "+
        "ale i skutecznie ogranicza ludzkie zap^edy w wyrywaniu z r^ak "+
        "matki Natury wszystkiego, co sie jej nale^zy. Gdzieniegdzie "+
        "pojawiaj^ace si^e z nienacka niewielkie zawirowania wody na "+
        "spokojnie, jakby leniwie poruszaj^acej si^e m^etnej tafli swiadcz^a "+
        "o nieprzywidywalno^sci i zdradzieckiej naturze nurtu, kt^ory zapewne "+
        "pochlon^a^l niejedno istnienie bezmy^slnie probuj^ace zmierzy^c si^e z "+
        "pot^eg^a rzeki.\n");

    set_alarm_every_hour("utopce");
    add_prop(ROOM_I_WCIAGA, 0);
}

void
init()
{
    ::init();
    init_drink_water();
}

void
utopce()
{
    if (MT_GODZINA >= 18)
        add_npc(PIANA_OKOLICE_BAGNO_LIVINGI + "utopiec");
}

public string
exits_description()
{
    return "Wszechogarniaj^aca ciemno^s^c sprawia, ^ze nie jeste^s"+
        " w stanie dostrzec st^ad ^zadnego wyj^scia.\n";

}

string
dlugi_opis()
{
    string str = "";

    if (CZY_JEST_SNIEG(this_object()))
    {
        str += "Martwa cisza wydaje si^e by^c w tej cz^e^sci niezm^acona. "+
            "Zwykle g^lo^sne i wzburzone wody rzeki Pontar, p^lyn^acej gdzie^s "+
            "na zachodzie nie s^a w stanie przedrze^c si^e przez scian^e ciszy "+
            "tego miejsca."+
            " Nawet twoj oddech zostaje g^lucho wch^loni^ety przez g^est^a"+
            " pl^atanine martwych konar^ow drzew. Unosz^aca si^e nad powierzchni^a"+
            " bagien mg^la wij^e si^e z^lowrogo i pochyla nad zatopionymi w b^locie"+
            " cia^lami, kt^orych kszta^lty - tak zdeformowane ju^z przez post^epuj^acy"+
            " proces rozk^ladu - poma^lu przestaj^a si^e wyr^o^znia^c na tle gnij^acych"+
            " pni. Z ka^zdym podmuchem wiatru ga^l^ezie drzew nad tob^a zacie^sniaj^a"+
            " swoje sploty tak, jakby na zawsze chcia^ly zatrzyma^c ci^e w swoich"+
            " szponach. ";
    }
    else if (pora_roku() == MT_WIOSNA)
    {
        str += "Martwa cisza wydaje si^e by^c w tej cz^e^sci niezm^acona. "+
            "Zwykle g^lo^sne i wzburzone wody rzeki Pontar, p^lyn^acej gdzie^s "+
            "na zachodzie nie s^a w stanie przedrze^c si^e przez scian^e ciszy "+
            "tego miejsca."+
            " Nawet twoj oddech zostaje g^lucho wch^loni^ety przez g^est^a"+
            " pl^atanine, martwych konar^ow drzew. Unosz^aca si^e nad powierzchni^a"+
            " bagien mg^la wij^e si^e z^lowrogo i pochyla nad zatopionymi w b^locie"+
            " cia^lami, kt^orych kszta^lty - tak zdeformowane ju^z przez post^epuj^acy"+
            " proces rozk^ladu - poma^lu przestaj^a si^e wyr^o^znia^c na tle gnij^acych"+
            " pni. Z ka^zdym podmuchem wiatru ga^l^ezie drzew nad tob^a zacie^sniaj^a"+
            " swoje sploty tak, jakby na zawsze chcia^ly zatrzyma^c ci^e w swoich"+
            " szponach. ";
    }
    else if (pora_roku() == MT_LATO)
    {
        str += "Martwa cisza wydaje si^e by^c w tej cz^e^sci niezm^acona. "+
            "Zwykle g^lo^sne i wzburzone wody rzeki Pontar, p^lyn^acej gdzie^s "+
            "na zachodzie nie s^a w stanie przedrze^c si^e przez scian^e ciszy "+
            "tego miejsca."+
            " Nawet twoj oddech zostaje g^lucho wch^loni^ety przez g^est^a"+
            " pl^atanine, martwych konar^ow drzew. Unosz^aca si^e nad powierzchni^a"+
            " bagien mg^la wij^e si^e z^lowrogo i pochyla nad zatopionymi w b^locie"+
            " cia^lami, kt^orych kszta^lty - tak zdeformowane ju^z przez post^epuj^acy"+
            " proces rozk^ladu - poma^lu przestaj^a si^e wyr^o^znia^c na tle gnij^acych"+
            " pni. Z ka^zdym podmuchem wiatru ga^l^ezie drzew nad tob^a zacie^sniaj^a"+
            " swoje sploty tak, jakby na zawsze chcia^ly zatrzyma^c ci^e w swoich"+
            " szponach. ";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str += "Martwa cisza wydaje si^e by^c w tej cz^e^sci niezm^acona. "+
            "Zwykle g^lo^sne i wzburzone wody rzeki Pontar, p^lyn^acej gdzie^s "+
            "na zachodzie nie s^a w stanie przedrze^c si^e przez scian^e ciszy "+
            "tego miejsca."+
            " Nawet twoj oddech zostaje g^lucho wch^loni^ety przez g^est^a"+
            " pl^atanine, martwych konar^ow drzew. Unosz^aca si^e nad powierzchni^a"+
            " bagien mg^la wij^e si^e z^lowrogo i pochyla nad zatopionymi w b^locie"+
            " cia^lami, kt^orych kszta^lty - tak zdeformowane ju^z przez post^epuj^acy"+
            " proces rozk^ladu - poma^lu przestaj^a si^e wyr^o^znia^c na tle gnij^acych"+
            " pni. Z ka^zdym podmuchem wiatru ga^l^ezie drzew nad tob^a zacie^sniaj^a"+
            " swoje sploty tak, jakby na zawsze chcia^ly zatrzyma^c ci^e w swoich"+
            " szponach. ";
    }

    return str + "\n";
}
