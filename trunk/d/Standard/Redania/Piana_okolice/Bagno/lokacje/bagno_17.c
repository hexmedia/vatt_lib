/**
 * \file /d/Standard/Redania/Piana_okolice/Bagno/lokacje/bagno_17.c
 *
 * Bagno opodal Piany.
 *
 * @author Aine
 * @date   2008
 */

#include "dir.h"
#include <macros.h>
#include <pogoda.h>
#include <mudtime.h>
#include <stdproperties.h>

inherit PIANA_OKOLICE_BAGNO_STD;
inherit "/lib/drink_water";

void
create_piana_bagno()
{
    set_short("Przy bajorze");

    add_t_exit("13", "nw");
    add_t_exit("14", "n");
    add_t_exit("15", "ne");
    add_t_exit("18", "e");
    add_t_exit("19", "sw");
    add_t_exit("20", "s");

    set_drink_places(({"z bajora","z bajorka"}));
    ustaw_trudnosc_bagna(2);
    //WARNING: Mo�e przyda�o by si� opisa� bajorko?
}

void
init()
{
    ::init();
    init_drink_water();
}

public string
exits_description()
{
    return "Ledwo widoczna dr^o^zka prowadzi na p^o^lnocny-zach^od, p^o^lnoc,"+
        " p^o^lnocny-wsch^od, po^ludniowy-zach^od, po^ludnie i wsch^od.\n";
}

string
dlugi_opis()
{
    string str = "";

    if (CZY_JEST_SNIEG(this_object()))
    {
        str += "Krzewy i drzewa niemal ca^lkowicie ust^api^ly miejsce wolnej prze"+
            "strzeni, tworz^ac jakby b^lotnist^a polanke, na ^kt^orej ^srodku znajduje"+
            " sie ogromne, brudne bajoro. Jego brzegi pokryte s^a przegni^lymi li^s^cmi"+
            " i pojed^ynczymi k^lodami rosn^acych tu niegdy^s drzew. Pod^lo^ze jest tutaj"+
            " do^s^c ^sliskie dlatego z ka^zdym krokiem nale^zy zachowa^c ostro^zno^s^c."+
            " Z pewno^sci^a bagna te poch^lon^e^ly ju^z niejednego nieroztropnego"+
            " podr^o^znika, a niepozorne bajoro gdyby tylko potrafi^lo m^owi^c opowie"+
            "dzia^loby niejedn^a histori^e o tajemnicach jakie spoczywaj^a na jego dnie. ";
    }
    else if (pora_roku() == MT_WIOSNA)
    {
        str += "Krzewy i drzewa niemal ca^lkowicie ust^api^ly miejsce wolnej prze"+
            "strzeni, tworz^ac jakby b^lotnist^a polanke, na ^kt^orej ^srodku znajduje"+
            " sie ogromne, brudne bajoro. Jego brzegi pokryte s^a przegni^lymi li^s^cmi"+
            " i pojed^ynczymi k^lodami rosn^acych tu niegdy^s drzew. Pod^lo^ze jest tutaj"+
            " do^s^c ^sliskie dlatego z ka^zdym krokiem nale^zy zachowa^c ostro^zno^s^c."+
            " Z pewno^sci^a bagna te poch^lon^e^ly ju^z niejednego nieroztropnego"+
            " podr^o^znika, a niepozorne bajoro gdyby tylko potrafi^lo m^owi^c opowie"+
            "dzia^loby niejedn^a histori^e o tajemnicach jakie spoczywaj^a na jego dnie. ";
    }
    else if (pora_roku() == MT_LATO)
    {
        str += "Krzewy i drzewa niemal ca^lkowicie ust^api^ly miejsce wolnej prze"+
            "strzeni, tworz^ac jakby b^lotnist^a polanke, na ^kt^orej ^srodku znajduje"+
            " sie ogromne, brudne bajoro. Jego brzegi pokryte s^a przegni^lymi li^s^cmi"+
            " i pojed^ynczymi k^lodami rosn^acych tu niegdy^s drzew. Pod^lo^ze jest tutaj"+
            " do^s^c ^sliskie dlatego z ka^zdym krokiem nale^zy zachowa^c ostro^zno^s^c."+
            " Z pewno^sci^a bagna te poch^lon^e^ly ju^z niejednego nieroztropnego"+
            " podr^o^znika, a niepozorne bajoro gdyby tylko potrafi^lo m^owi^c opowie"+
            "dzia^loby niejedn^a histori^e o tajemnicach jakie spoczywaj^a na jego dnie. ";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str += "Krzewy i drzewa niemal ca^lkowicie ust^api^ly miejsce wolnej prze"+
            "strzeni, tworz^ac jakby b^lotnist^a polanke, na ^kt^orej ^srodku znajduje"+
            " sie ogromne, brudne bajoro. Jego brzegi pokryte s^a przegni^lymi li^s^cmi"+
            " i pojed^ynczymi k^lodami rosn^acych tu niegdy^s drzew. Pod^lo^ze jest tutaj"+
            " do^s^c ^sliskie dlatego z ka^zdym krokiem nale^zy zachowa^c ostro^zno^s^c."+
            " Z pewno^sci^a bagna te poch^lon^e^ly ju^z niejednego nieroztropnego"+
            " podr^o^znika, a niepozorne bajoro gdyby tylko potrafi^lo m^owi^c opowie"+
            "dzia^loby niejedn^a histori^e o tajemnicach jakie spoczywaj^a na jego dnie. ";
    }

    return str + "\n";
}
