/**
 * \file /d/Standard/Redania/Piana_okolice/Bagno/lokacje/bagno_04.c
 *
 * Bagno opodal Piany.
 *
 * @author Aine
 * @date   2008
 */

#include "dir.h"
#include <macros.h>
#include <pogoda.h>
#include <mudtime.h>
#include <stdproperties.h>

inherit PIANA_OKOLICE_BAGNO_STD;

void
create_piana_bagno()
{
    set_short("Przy ogromnym korzeniu");

    add_t_exit("01", "n");
    add_t_exit("02", "ne");
    add_t_exit("03", "w");
    add_t_exit("05", "e");
    add_t_exit("06", "sw");
    add_t_exit("07", "s");

    add_item("korze^n",
        "Ogromny, si^l^a wyrwany z ziemi korze^n z^lowieszczo"+
        " rozpostar^l swoje szpony nad grz^ask^a ziemi^a."+
        " Jego podgnite drewno op^lywa cuchn^acym szlamem"+
        " daj^ac u^zywke pe^lzaj^acemu po nim robactwu.\n");

    add_sit(({"na korzeniu","na wyrwanym korzeniu",
        "na wyrwanym z ziemi korzeniu"}),4);
}

public string
exits_description()
{
    return "Ledwo widoczna dr^o^zka prowadzi na p^o^lnoc, zach^od,"+
        " wsch^od, po^ludnie oraz na po^ludniowy-zach^od.\n";
}

string
dlugi_opis()
{
    string str = "";

    if (CZY_JEST_SNIEG(this_object()))
    {
        str += "Ogromne, przewr^ocone drzewo wyr^o^znia si^e na bieli"+
            "^sniegu. Jego si^l^a wyrwany korze^n wznosi si^e z^lowieszczo"+
            " nad bagnem, przypominaj^ac rozwarte i gotowe do uk^aszenia"+
            " szcz^eki. W tej cz^e^sci moczar, drzewo to jest jedyn^a pozo"+
            "sta^lo^sci^a po rosn^acym tu niegdy^s, ogromnym borze. Zmro^z"+
            "ona zimowym ch^lodem ziemia na tej cz^e^sci terenu wydaje si^"+
            "e by^c pewniejszym gruntem, jednak z ka^zdym krokiem nale^zy"+
            " zachowa^c ostro^zno^s^c. Zdradliwe, zamarzniete ka^lu^ze a^"+
            "z kusz^a by po nich przej^s^c i zaoszcz^edzi^c sobie drogi. ";
    }
    else if (pora_roku() == MT_WIOSNA)
    {
        str += "Ogromne, przewr^ocone drzewo wbi^lo si^e g^lucho w czer^n"+
            " b^lota. Jego si^l^a wyrwany korze^n wznosi si^e z^lowieszczo n"+
            "ad bagnem, przypominaj^ac rozwarte i gotowe do uk^aszenia szcz^"+
            "eki. W tej cz^e^sci moczar, drzewo to jest jedyn^a pozosta^lo^s"+
            "ci^a po rosn^acym tu niegdy^s, ogromnym borze. Nieco mniej wodn"+
            "ista, ziemia na tej cz^e^sci terenu wydaje si^e by^c pewniejszy"+
            "m gruntem, jednak z ka^zdym krokiem nale^zy zachowa^c ostro^zno"+
            "^s^c. Zdradliwe, pokryte podgnitym listowiem ka^lu^ze a^z kusz^"+
            "a by po nich przej^s^c i zaoszcz^edzi^c sobie drogi. ";
    }
    else if (pora_roku() == MT_LATO)
    {
        str += "Ogromne, przewr^ocone drzewo wbi^lo si^e g^lucho w czer^n b^"+
            "lota. Jego si^l^a wyrwany korze^n wznosi si^e z^lowieszczo nad ba"+
            "gnem, przypominaj^ac rozwarte i gotowe do uk^aszenia szcz^eki. W"+
            " tej cz^e^sci moczar, drzewo to jest jedyn^a pozosta^lo^sci^a po"+
            " rosn^acym tu niegdy^s, ogromnym borze. Podsuszona letnim upa^lem"+
            " ziemia na tej cz^e^sci terenu wydaje si^e by^c pewniejszym grunt"+
            "em, jednak z ka^zdym krokiem nale^zy zachowa^c ostro^zno^s^c. Zdra"+
            "dliwe, pokryte podgnitym listowiem ka^lu^ze a^z kusz^a by po nich"+
            " przej^s^c i zaoszcz^edzi^c sobie drogi. ";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str += "Ogromne, przewr^ocone drzewo wbi^lo si^e g^lucho w czer^n b^l"+
            "ota. Jego si^l^a wyrwany korze^n wznosi si^e z^lowieszczo nad bag"+
            "nem, przypominaj^ac rozwarte i gotowe do uk^aszenia szcz^eki. W t"+
            "ej cz^e^sci moczar, drzewo to jest jedyn^a pozosta^lo^sci^a po ro"+
            "sn^acym tu niegdy^s, ogromnym borze. Zmro^zona ch^lodem ziemia na"+
            " tej cz^e^sci terenu wydaje si^e by^c pewniejszym gruntem, jednak"+
            " z ka^zdym krokiem nale^zy zachowa^c ostro^zno^s^c. Zdradliwe, pok"+
            "ryte podgnitym listowiem ka^lu^ze a^z kusz^a by po nich przej^s^c"+
            " i zaoszcz^edzi^c sobie drogi. ";
    }

    return str + "\n";
}
