/**
 * \file /d/Standard/Redania/Piana_okolice/Bagno/lokacje/bagno_14.c
 *
 * Bagno opodal Piany.
 *
 * @author Aine
 * @date   2008
 */

#include "dir.h"
#include <macros.h>
#include <pogoda.h>
#include <mudtime.h>
#include <stdproperties.h>

inherit PIANA_OKOLICE_BAGNO_STD;

void
create_piana_bagno()
{
    set_short("Las po^sr^od bagien");

    add_t_exit("09", "nw");
    add_t_exit("10", "n");
    add_t_exit("13", "w");
    add_t_exit("17", "s");
    add_t_exit("18", "se");

    add_prop(ROOM_I_TYPE, (ROOM_SWAMP|ROOM_FOREST));
    add_prop(ROOM_I_WCIAGA, 0);
}

public string
exits_description()
{
    return "Ledwo widoczna dr^o^zka prowadzi na p^o^lnoc, po^ludnie "+
        "i po^ludniowy-wsch^od, zach^od i p^o^lnocny-zach^od.\n";
}

string
dlugi_opis()
{
    string str = "";

    if (CZY_JEST_SNIEG(this_object()))
    {
        str += "Mro^xne powietrze z ka^zdym oddechem przynosi ulge i "+
            "pewnego rodzaju oczyszczenie po czasie sp^edzonym w innych "+
            "cz^e^sciach bagien. Nieco twardsza ziemia w tym miejscu "+
            "pokryta zosta^la mi^ekkim ^snie^znym puchem, na k^torego "+
            "powierzchni zwierz^ece tropy poprzez zawi^l^a pl^atanine "+
            "utworzy^ly pi^ekn^a mozaik^e. Martwy krajobraz dominuj^acy "+
            "na terenach dooko^la zosta^l tutaj wyparty przez pragn^ac^a "+
            "^zycia przyrode, teraz spokojnie drzemi^ac^a pod ^sniegow^a "+
            "pierzyn^a. Spok^oj i cisze tego miejsca podkre^sla wij^aca "+
            "si^e mi^edzy drzewami mg^la, kt^ora delikatnie muska "+
            "wystaj^ace spod ^sniegu ro^sliny, jakby chcia^la utula^c "+
            "je do snu. ";
    }
    else if (pora_roku() == MT_WIOSNA)
    {
        str += "Ch^lodne powietrze z ka^zdym oddechem przynosi ulge i "+
            "pewnego rodzaju oczyszczenie po czasie sp^edzonym w innych "+
            "cz^e^sciach bagien. Nieco twardsza ziemia w tym miejscu "+
            "pokryta zosta^la wzbijaj^acymi si^e do nieba g^l^owkami "+
            "ro^slin, kt^ore ju^z niebawem rozwin^a swoje listki by m^oc w "+
            "pe^lni manifestowa^c swoj^a ch^e^c istnienia. Martwy krajobraz "+
            "dominuj^acy na terenach dooko^la zosta^l tutaj wyparty przez "+
            "pragn^ac^a ^zycia przyrode, teraz t^etni^ac^a ^zyciem. ";
    }
    else if (pora_roku() == MT_LATO)
    {
        str += "Twardszy grunt w tym miejscu pokryty zosta^l zieleni^a "+
            "t^etni^acej ^zyciem natury. Spokojne kwilenie jakiego^s ptaka "+
            "i szum ko^lysanych na wietrze, licznych li^sci wydaje si^e "+
            "wyrywa^c i walczy^c z ponurym i ciemnym krajobrazem bagien. "+
            "Para unosz^aca si^e nad ziemi^awij^e si^e w swoim gro^teskowym "+ 
            "ta^ncu przenikaj^ac miedzy drzewami i krzewami delikatnie "+
            "muskaj^ac je, jakby chcia^la pomo^c im w zmaganiach z zastan^a "+
            "rzeczywisto^sci^a. ";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str += "Ch^lodne powietrze z ka^zdym oddechem przynosi ulge i "+
            "pewnego rodzaju oczyszczenie po czasie sp^edzonym w innych "+
            "cz^e^sciach bagien. Nieco twardsza ziemia w tym miejscu "+
            "pokryta zosta^la szarymi, po^z^o^lk^lymi li^s^cmi. Martwy "+
            "krajobraz dominuj^acy na terenach dooko^la zosta^l tutaj "+
            "wyparty przez pragn^ac^a ^zycia przyrode, teraz spokojnie "+
            "u^spion^a. Spok^oj i cisze tego miejsca podkre^sla wij^aca "+
            "si^e mi^edzy drzewami mg^la, kt^ora delikatnie muska "+
            "drzemi^ace ro^sliny, jakby chcia^la utula^c je do snu. ";
    }
    return str + "\n";
}
