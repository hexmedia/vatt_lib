/**
 * \file /d/Standard/Redania/Piana_okolice/Bagno/lokacje/bagno_19.c
 *
 * Bagno opodal Piany.
 *
 * @author Aine
 * @date   2008
 */

#include "dir.h"
#include <macros.h>
#include <pogoda.h>
#include <mudtime.h>
#include <stdproperties.h>

inherit PIANA_OKOLICE_BAGNO_STD;

void
create_piana_bagno()
{
    set_short("Martwy, bagnisty las");

    add_t_exit("16", "nw");
    add_t_exit("17", "ne");
    add_t_exit("20", "e");

    add_prop(ROOM_I_TYPE, (ROOM_SWAMP|ROOM_FOREST));
    add_prop(ROOM_I_WCIAGA, 0);
}


public string
exits_description()
{
    return "Wszechogarniaj^aca ciemno^s^c sprawia, ^ze nie jeste^s"+
        " w stanie dostrzec st^ad ^zadnego wyj^scia.\n";
}

string
dlugi_opis()
{
    string str = "";

    if (CZY_JEST_SNIEG(this_object()))
    {
        str += "Wiecznie panuj^aca tu ciemno^s^c podst^epnie zataja niebezpiecze"+
            "^nstwa tego podmok^lego terenu. Ta^ncz^ace na wietrze, martwe ga^l^ezie"+
            " krzew^ow i drzew tn^a g^lucho powietrze, zostawiaj^ac po sobie tylko"+
            " ^slad tego bezg^lo^snego, ^za^losnego gestu. Pojedy^ncze k^epki trawy"+
            " rozci^agni^ete na powierzchni bagna w pewien spos^ob wyznaczaj^a drog^e"+
            " gdzie powinien by^c postawiony krok ka^zdego, kto odwa^zy^l si^e zapu^s"+
            "ci^c a^z tutaj, a grz^askie bagna zdaj^a si^e tylko czeka^c na ka^zdy"+
            " b^ledny ruch. Zapach gnij^acych ro^slin i rozk^ladaj^acych si^e zw^lok"+
            " ofiar tego miejsca z ka^zd^a chwil^a staj^a si^e coraz bardziej nie do"+
            " zniesienia. ";
    }
    else if (pora_roku() == MT_WIOSNA)
    {
        str += "Wiecznie panuj^aca tu ciemno^s^c podst^epnie zataja niebezpiecze"+
            "^nstwa tego podmok^lego terenu. Ta^ncz^ace na wietrze, martwe ga^l^ezie"+
            " krzew^ow i drzew tn^a g^lucho powietrze, zostawiaj^ac po sobie tylko"+
            " ^slad tego bezg^lo^snego, ^za^losnego gestu. Pojedy^ncze k^epki trawy"+
            " rozci^agni^ete na powierzchni bagna w pewien spos^ob wyznaczaj^a drog^e"+
            " gdzie powinien by^c postawiony krok ka^zdego, kto odwa^zy^l si^e zapu^s"+
            "ci^c a^z tutaj, a grz^askie bagna zdaj^a si^e tylko czeka^c na ka^zdy"+
            " b^ledny ruch. Zapach gnij^acych ro^slin i rozk^ladaj^acych si^e zw^lok"+
            " ofiar tego miejsca z ka^zd^a chwil^a staj^a si^e coraz bardziej nie do"+
            " zniesienia. ";
    }
    else if (pora_roku() == MT_LATO)
    {
        str += "Wiecznie panuj^aca tu ciemno^s^c podst^epnie zataja niebezpiecze"+
            "^nstwa tego podmok^lego terenu. Ta^ncz^ace na wietrze, martwe ga^l^ezie"+
            " krzew^ow i drzew tn^a g^lucho powietrze, zostawiaj^ac po sobie tylko"+
            " ^slad tego bezg^lo^snego, ^za^losnego gestu. Pojedy^ncze k^epki trawy"+
            " rozci^agni^ete na powierzchni bagna w pewien spos^ob wyznaczaj^a drog^e"+
            " gdzie powinien by^c postawiony krok ka^zdego, kto odwa^zy^l si^e zapu^s"+
            "ci^c a^z tutaj, a grz^askie bagna zdaj^a si^e tylko czeka^c na ka^zdy"+
            " b^ledny ruch. Zapach gnij^acych ro^slin i rozk^ladaj^acych si^e zw^lok"+
            " ofiar tego miejsca z ka^zd^a chwil^a staj^a si^e coraz bardziej nie do"+
            " zniesienia. ";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str += "Wiecznie panuj^aca tu ciemno^s^c podst^epnie zataja niebezpiecze"+
            "^nstwa tego podmok^lego terenu. Ta^ncz^ace na wietrze, martwe ga^l^ezie"+
            " krzew^ow i drzew tn^a g^lucho powietrze, zostawiaj^ac po sobie tylko"+
            " ^slad tego bezg^lo^snego, ^za^losnego gestu. Pojedy^ncze k^epki trawy"+
            " rozci^agni^ete na powierzchni bagna w pewien spos^ob wyznaczaj^a drog^e"+
            " gdzie powinien by^c postawiony krok ka^zdego, kto odwa^zy^l si^e zapu^s"+
            "ci^c a^z tutaj, a grz^askie bagna zdaj^a si^e tylko czeka^c na ka^zdy"+
            " b^ledny ruch. Zapach gnij^acych ro^slin i rozk^ladaj^acych si^e zw^lok"+
            " ofiar tego miejsca z ka^zd^a chwil^a staj^a si^e coraz bardziej nie do"+
            " zniesienia. ";
    }

    str += "\n";

    return str;
}
