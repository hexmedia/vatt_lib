/**
 * \file /d/Standard/Redania/Piana_okolice/Bagno/lokacje/bagno_07.c
 *
 * Bagno opodal Piany.
 *
 * @author Aine
 * @date   2008
 */

#include "dir.h"
#include <macros.h>
#include <pogoda.h>
#include <mudtime.h>
#include <stdproperties.h>

inherit PIANA_OKOLICE_BAGNO_STD;

void
create_piana_bagno()
{
    set_short("Cuchn^ace bagnisko");

    add_t_exit("03", "nw");
    add_t_exit("04", "n");
    add_t_exit("05", "ne");
    add_t_exit("06", "w");
    add_t_exit("10", "se");

    set_alarm_every_hour("wij");
    ustaw_trudnosc_bagna(2);
}

void
wij()
{
    // Mo�e by� tylko jeden wij na ca�ych bagnach, tak dla bajeru ;)
    if (find_living("wijzbagienobokpiany") == 0 && random(2))
        add_npc(PIANA_OKOLICE_BAGNO_LIVINGI + "wij");
}


public string
exits_description()
{
    return "Ledwo widoczna dr^o^zka prowadzi na p^o^lnoc, p^o^lnocny-wsch^od,"+
        " p^o^lnocny-zach^od, zach^od, po^ludniowy-wsch^od.\n";
}

string
dlugi_opis()
{
    string str = "";

    if (CZY_JEST_SNIEG(this_object()))
    {
        str += "Biel spowijaj^aca szaro^s^c tego ponurego miejsca zdaje si^e celowo"+
            " ukrywa^c brzydote jak^a niesie ze sob^a bagnisko, jednak brunatne, namokni^ete"+
            " szlamem plamy bynajmniej nie chc^a da^c o sobie zapomnie^c. Otaczaj^ace cie"+
            " z ka^zdej strony bagnisko bez wzgl^edu na pore roku czy pore dnia sk^apane jest"+
            " w p^o^lmroku, kt^ory zdradliwie zataja niebezpiecze^nstwa tego podmok^lego terenu."+
            " Uci^a^zliwo^s^c podr^o^zowania t^edy podtrzymuj^a chmary komar^ow i innego robactwa,"+
            " kt^ore nawet w zime nie daj^a za wygran^a. ";
    }
    else if (pora_roku() == MT_WIOSNA)
    {
        str += "Nie^smiale p^aczki li^sci gdzieniegdzie pr^obuj^a wyrwa^c si^e z tej nieko^ncz^acej"+
            " si^e szaro^sci i brudu, jednak te ma^le akcenty zieleni nikn^a w ciemno^sci tego"+
            " miejsca zanim jeszcze zdo^laj^a zaistnie^c. Otaczaj^ace cie z ka^zdej strony bagnisko"+
            " bez wzgl^edu na pore roku czy pore dnia sk^apane jest w p^o^lmroku, kt^ory zdradliwie zataja"+
            " niebezpiecze^nstwa tego podmok^lego terenu. Uci^a^zliwo^s^c podr^o^zowania t^edy podtrzymuj^a"+
            " chmary komar^ow i innego robactwa, kt^ore pobudzone przez wiosn^e do ^zycia zaw^ladn^e^ly"+
            " ka^zdym skrawkiem tego miejsca. ";
    }
    else if (pora_roku() == MT_LATO)
    {
        str += "Gor^ace, duszne powietrze dra^zni twoje nozdrza i p^luca. Z ka^zdym g^l^ebszym oddechem"+
        " parszywo^s^c tego miejsca pr^obuje wedrze^c si^e ju^z nawet w twoj organizm. Drzewa i krzewy"+
        " sk^apane szaro^sci^a pochylaj^a si^e nad sm^etn^a, grz^ask^a ziemi^a w ge^scie rozpaczy"+
        " lub te^z politowania. Otaczaj^ace cie z ka^zdej strony bagnisko bez wzgl^edu na pore roku czy"+
        " pore dnia sk^apane jest w p^o^lmroku, kt^ory zdradliwie zataja niebezpiecze^nstwa tego podmok^lego"+
        " terenu. Uci^a^zliwo^s^c podr^o^zowania t^edy podtrzymuj^a chmary komar^ow i innego robactwa, kt^ore"+
        " pobudzone przez nature do dzia^lania zaw^ladn^e^ly ka^zdym skrawkiem tego miejsca. ";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str += "Drzewa i krzewy sk^apane szaro^sci^a pochylaj^a si^e nad sm^etn^a, grz^ask^a ziemi^a w"+
            " ge^scie rozpaczy lub te^z politowania. Cuchn^aca, wilgotna ziemia niczym spragniona czyjego^s"+
            " ^zycia ^lapie ka^zdy tw^oj krok i zdaje si^e go poch^lania^c. Otaczaj^ace cie z ka^zdej strony"+
            " bagnisko bez wzgl^edu na pore roku czy pore dnia sk^apane jest w p^o^lmroku, kt^ory zdradliwie"+
            " zataja niebezpiecze^nstwa tego podmok^lego terenu. Uci^a^zliwo^s^c podr^o^zowania t^edy podtrzymuj^a"+
            " chmary komar^ow i innego robactwa, kt^ore pobudzone przez nature do dzia^lania zaw^ladn^e^ly ka^zdym"+
            " skrawkiem tego miejsca. ";
    }

    return str + "\n";
}
