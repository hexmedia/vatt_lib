/**
 * \file /d/Standard/Redania/Piana_okolice/Bagno/lokacje/bagno_06.c
 *
 * Bagno opodal Piany.
 *
 * @author Aine
 * @date   2008
 */

#include "dir.h"
#include <macros.h>
#include <pogoda.h>
#include <mudtime.h>
#include <stdproperties.h>

inherit PIANA_OKOLICE_BAGNO_STD;
inherit "/lib/drink_water";

void
create_piana_bagno()
{
    set_short("Grz^askie moczary");

    add_t_exit("03", "n");
    add_t_exit("04", "ne");
    add_t_exit("07", "e");
    add_t_exit("09", "se");

    set_drink_places(({"z rzeki","z pontaru","z Pontaru"}));
    add_sit(({"nad rzek^a","nad pontarem","nad Pontarem"}),
        "nad rzek^a","z nad rzeki",0);

    add_item(({"Pontar","pontar","rzek^e"}),"P^lyn^acy tu od tysi^ecy lat "+
        "szeroki Pontar zdaje si^e by^c wci^a^z nieujarzmionym i niezdobytym, "+
        "wyznacza nie tylko granice mi^edzy kr^olestwami Redanii i Temeri, "+
        "ale i skutecznie ogranicza ludzkie zap^edy w wyrywaniu z r^ak "+
        "matki Natury wszystkiego, co sie jej nale^zy. Gdzieniegdzie "+
        "pojawiaj^ace si^e z nienacka niewielkie zawirowania wody na "+
        "spokojnie, jakby leniwie poruszaj^acej si^e m^etnej tafli swiadcz^a "+
        "o nieprzywidywalno^sci i zdradzieckiej naturze nurtu, kt^ory zapewne "+
        "pochlon^a^l niejedno istnienie bezmy^slnie probuj^ace zmierzy^c si^e z "+
        "pot^eg^a rzeki.\n");
    add_prop(ROOM_I_WCIAGA, 0);
}

void
init()
{
    ::init();
    init_drink_water();
}

public string
exits_description()
{
    return "Ledwo widoczna dr^o^zka prowadzi na wsch^od, po^lnoc, "+
        "po^ludniowy-wsch^od oraz na p^o^lnocny-wsch^od.\n";
}

string
dlugi_opis()
{
    string str = "";

    if (CZY_JEST_SNIEG(this_object()))
    {
        str += "Mro^zne, zimowe powietrze w po^l^aczeniu z nieustaj^acym"+
            " smrodem staje si^e coraz bardziej uci^a^zliwe. Ro^slinno^s^c tu"+
            " jest bardzo przerzedzona, co sprawia, ^ze twoim oczom co krok uk"+
            "azuj^a si^e zatajane przez b^loto sekrety moczar. Szcz^atki rozk^"+
            "ladaj^acych si^e zwierz^at, stare ko^sci - to tylko nieliczne ost"+
            "^rze^zenia jakie mo^zna wyczyta^c w tym parszywym krajobrazie, kt"+
            "^orego szaro^s^c i cze^r^n z ka^zd^a chwil^a jeszcze bardziej prz"+
            "yt^laczaj^a. Wszystkiego dope^lniaj^a ciemne i zm^acone wody "+
            "Pontaru, kt^ore bulgocz^a z^lowieszczo gdzie^s na zachodzie.";
    }
    else if (pora_roku() == MT_WIOSNA)
    {
        str += "Wilgotne, zimne powietrze w po^l^aczeniu z nieustaj^acym smro"+
            "dem staje si^ coraz bardziej uci^a^zliwe. Ro^slinno^s^c tu jest bar"+
            "dzo przerzedzona, co sprawia, ^ze twoim oczom co krok ukazuj^a si^e"+
            " zatajane przez b^loto sekrety moczar. Szcz^atki rozk^ladaj^acych s"+
            "i^e zwierz^at, stare ko^sci - to tylko nieliczne ost^rze^zenia jaki"+
            "e mo^zna wyczyta^c w tym parszywym krajobrazie, kt^orego szaro^s^c"+
            " i cze^r^n z ka^zd^a chwil^a jeszcze bardziej przyt^laczaj^a. "+
            "Wszystkiego dope^lniaj^a ciemne i zm^acone wody Pontaru, kt^ore "+
            "bulgocz^a z^lowieszczo gdzie^s na zachodzie.";
    }
    else if (pora_roku() == MT_LATO)
    {
        str += "Suche, gor^ace powietrze w po^l^aczeniu z nieustaj^acym sm"+
        "rodem staje si^e coraz bardziej uci^a^zliwe. Ro^slinno^s^c tu je"+
        "st bardzo przerzedzona, co sprawia, ^ze twoim oczom co krok ukaz"+
        "uj^a si^e zatajane przez b^loto sekrety moczar. Szcz^atki rozk^l"+
        "adaj^acych si^e zwierz^at, stare ko^sci - to tylko nieliczne ost"+
        "^rze^zenia jakie mo^zna wyczyta^c w tym parszywym krajobrazie, kt"+
        "^orego szaro^s^c i cze^r^n z ka^zd^a chwil^a jeszcze bardziej pr"+
        "zyt^laczaj^a. Wszystkiego dope^lniaj^a ciemne i zm^acone wody "+
        "Pontaru, kt^ore bulgocz^a z^lowieszczo gdzie^s na zachodzie.";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str += "Wilgotne, zimne powietrze w po^l^aczeniu z nieustaj^acym sm"+
            "rodem staje si^e coraz bardziej uci^a^zliwe. Ro^slinno^s^c tu je"+
            "st bardzo przerzedzona, co sprawia, ^ze twoim oczom co krok ukaz"+
            "uj^a si^e zatajane przez b^loto sekrety moczar. Szcz^atki rozk^l"+
            "adaj^acych si^e zwierz^at, stare ko^sci - to tylko nieliczne ost^"+
            "rze^zenia jakie mo^zna wyczyta^c w tym parszywym krajobrazie, kt"+
            "^orego szaro^s^c i cze^r^n z ka^zd^a chwil^a jeszcze bardziej przyt"+
            "^laczaj^a. Wszystkiego dope^lniaj^a ciemne i zm^acone wody "+
            "Pontaru, kt^ore bulgocz^a z^lowieszczo gdzie^s na zachodzie.";
    }

    return str + "\n";
}
