/**
 * \file /d/Standard/Redania/Piana_okolice/Bagno/lokacje/bagno_18.c
 *
 * Bagno opodal Piany.
 *
 * @author Aine
 * @date   2008
 */

#include "dir.h"
#include <macros.h>
#include <pogoda.h>
#include <mudtime.h>
#include <stdproperties.h>

inherit PIANA_OKOLICE_BAGNO_STD;

void
create_piana_bagno()
{
    set_short("Grz^askie bagnisko");

    add_t_exit("14", "nw");
    add_t_exit("15", "n");
    add_t_exit("17", "w");
    add_t_exit("20", "sw");
    add_exit(PIANA_OKOLICE_TRAKT_LOKACJE + "droga_05.c", "ne", 0, FATIGUE, 0);
    add_exit(PIANA_OKOLICE_TRAKT_LOKACJE + "droga_04.c", "e",  0, FATIGUE, 0);
    add_exit(PIANA_OKOLICE_TRAKT_LOKACJE + "droga_03.c", "se", 0, FATIGUE, 0);
}

public string
exits_description()
{
    return "Ledwo widoczna dr^o^zka prowadzi na zach^od, p^o^lnocny-zach^od,"+
        "p^o^lnoc i po^ludniowy-zach^od. Mo^zesz st^ad tak^ze dostrzec ^scie^zke"+
        " prowadz^ac^a na p^o^lnocny-wsch^od, wsch^od i po^ludniowy-wsch^od.\n";
}

string
dlugi_opis()
{
    string str = "";

    if (CZY_JEST_SNIEG(this_object()))
    {
        str += "Okryte ^snie^zn^a pierzyn^a bagno wydaje si^e by^c jakby nieco u^spione."+
            "Mro^zne, zimowe powietrze w po^l^aczeniu z nieustaj^acym smrodem staje si^e"+
            " coraz bardziej uci^a^zliwe. Ro^slinno^s^c tu jest bardzo prze^zedzona, co sprawia,"+
            " ^ze twoim oczom co krok ukazuj^a si^e zatajnione przez b^loto sekrety moczar. Szcz^atki"+
            " rozk^ladaj^acych si^e zwierz^at, stare ko^sci - to tylko nieliczne ost^rze^zenia"+
            " jakie mo^zna wyczyta^c w tym parszywym krajobrazie, kt^orego szaro^s^c i cze^r^n z ka^zd^a"+
            " chwil^a jeszcze bardziej przyt^laczaj^a. ";
    }
    else if (pora_roku() == MT_WIOSNA)
    {
        str += "Martwe krzewy, przegni^le pnie, zb^l^akany krzyk ptaka - to obraz niegdy^s rosn^acego tu,"+
            " pot^e^znego boru, w kt^orym wiosna rozpo^sciera^la obficie swoje dobra, a w kt^orym teraz tylko"+
            " w nielicznych miejscach mo^zna odnale^x^c jej niemal niewidoczne dzia^lania."+
            " Wilgotne, zimne powietrze w po^l^aczeniu z nieustaj^acym smrodem staje si^e"+
            " coraz bardziej uci^a^zliwe. Ro^slinno^s^c tu jest bardzo prze^zedzona, co sprawia,"+
            " ^ze twoim oczom co krok ukazuj^a si^e zatajnione przez b^loto sekrety moczar. Szcz^atki"+
            " rozk^ladaj^acych si^e zwierz^at, stare ko^sci - to tylko nieliczne ost^rze^zenia"+
            " jakie mo^zna wyczyta^c w tym parszywym krajobrazie, kt^orego szaro^s^c i cze^r^n z ka^zd^a"+
            " chwil^a jeszcze bardziej przyt^laczaj^a. ";
    }
    else if (pora_roku() == MT_LATO)
    {
        str += "Para unosz^aca si^e nad ziemi^a wij^e si^e w swoim gro^teskowym ta^ncu mami^ac z^ludnie"+
            " twoje zmys^ly i jakby zapraszaj^ac by wraz z ni^a odda^c si^e ta^ncu na tej nios^acej"+
            " nie^bezpiecze^nstwo przestrzeni... Suche, gor^ace powietrze w po^l^aczeniu z nieustaj^acym"+
            " smrodem staje si^e coraz bardziej uci^a^zliwe. Ro^slinno^s^c tu jest bardzo prze^zedzona,"+
            " co sprawia, ^ze twoim oczom co krok ukazuj^a si^e zatajnione przez b^loto sekrety moczar."+
            " Szcz^atki rozk^ladaj^acych si^e zwierz^at, stare ko^sci - to tylko nieliczne ost^rze^zenia"+
            " jakie mo^zna wyczyta^c w tym parszywym krajobrazie, kt^orego szaro^s^c i cze^r^n z ka^zd^a"+
            " chwil^a jeszcze bardziej przyt^laczaj^a. ";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str += "Wilgotne, zimne powietrze w po^l^aczeniu z nieustaj^acym smrodem staje si^e"+
            " coraz bardziej uci^a^zliwe. Ro^slinno^s^c tu jest bardzo prze^zedzona, co sprawia,"+
            " ^ze twoim oczom co krok ukazuj^a si^e zatajnione przez b^loto sekrety moczar. Szcz^atki"+
            " rozk^ladaj^acych si^e zwierz^at, stare ko^sci - to tylko nieliczne ost^rze^zenia"+
            " jakie mo^zna wyczyta^c w tym parszywym krajobrazie, kt^orego szaro^s^c i cze^r^n z ka^zd^a"+
            " chwil^a jeszcze bardziej przyt^laczaj^a. ";
    }

    return str + "\n";
}
