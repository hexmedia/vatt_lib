/**
 * \file /d/Standard/Redania/Piana_okolice/Bagno/lokacje/bagno_01.c
 *
 * Bagno opodal Piany.
 *
 * @author Aine
 * @date   2008
 */

#include "dir.h"
#include <macros.h>
#include <pogoda.h>
#include <mudtime.h>
#include <stdproperties.h>

inherit PIANA_OKOLICE_BAGNO_STD;
inherit "/lib/drink_water";

void
create_piana_bagno()
{
    set_short("Cuchn^ace bagnisko");

    add_t_exit("02", "e");
    add_t_exit("04", "s");
    add_t_exit("05", "se");

    set_drink_places(({"z rzeki","z pontaru","z Pontaru"}));
    add_sit(({"nad rzek^a","nad pontarem","nad Pontarem"}),
        "nad rzek^a","z nad rzeki",0);

    add_item(({"Pontar","pontar","rzek^e"}),"P^lyn^acy tu od tysi^ecy lat "+
        "szeroki Pontar zdaje si^e by^c wci^a^z nieujarzmionym i niezdobytym, "+
        "wyznacza nie tylko granice mi^edzy kr^olestwami Redanii i Temeri, "+
        "ale i skutecznie ogranicza ludzkie zap^edy w wyrywaniu z r^ak "+
        "matki Natury wszystkiego, co sie jej nale^zy. Gdzieniegdzie "+
        "pojawiaj^ace si^e z nienacka niewielkie zawirowania wody na "+
        "spokojnie, jakby leniwie poruszaj^acej si^e m^etnej tafli swiadcz^a "+
        "o nieprzywidywalno^sci i zdradzieckiej naturze nurtu, kt^ory zapewne "+
        "pochlon^a^l niejedno istnienie bezmy^slnie probuj^ace zmierzy^c si^e z "+
        "pot^eg^a rzeki.\n");
}

void
init()
{
    ::init();
    init_drink_water();
}

public string
exits_description()
{
    return "Ledwo widoczna dr^o^zka prowadzi na wsch^od,"+
        " po^ludnie i po^ludniowy-wsch^od.\n";
}

string
dlugi_opis()
{
    string str = "";

    if (CZY_JEST_SNIEG(this_object()))
    {
        str += "Okryte ^snie^zn^a pierzyn^a bagno wydaje si^e by^c"+
            " jakby nieco u^spione. Biel spowijaj^aca szaro^s^c tego p"+
            "onurego miejsca zdaje si^e celowo ukrywa^c brzydote jak^"+
            "a niesie ze sob^a bagnisko, jednak brunatne, namokni^ete"+
            " szlamem plamy bynajmniej nie chc^a da^c o sobie zapomnie"+
            "^c. Miekkie podlo^ze w zdradliwy spos^ob prowadzi twoje k"+
            "rok i miedzy martwymi, przegnitymi krzewami, kt^orych cie"+
            "nie rozciagaj^a si^e na pod^lo^zu w zawi^lych, na sw^oj s"+
            "pos^b groteskowych mozaikach. Stare konary drzew, kt^ore n"+
            "iegdy^s mog^ly zachwyca^c swoim ogromem, teraz stoj^a poje"+
            "dy^nczo i spogladaj^a na rozleg^le tereny dooko^la, niczym"+
            " zakl^eci, u^spieni stra^znicy. Na zachodzie s^lycha^c "+
            "niespokojny i gwa^ltowny szum Pontaru. ";
    }
    else if (pora_roku() == MT_WIOSNA)
    {
        str += "Budz^aca si^e do ^zycia ro^slinno^s^c bynajmniej nie do"+
            "da^la uroku i ^swie^zo^sci miejscu, w kt^orym si^e znajdujesz."+
            " Niedawny jeszcze ^snieg spowijaj^acy te tereny po roztopieniu,"+
            " sprawi^l, i^z ziemia jest jeszcze bardziej grz^aska, a co za"+
            " tym idzie... jeszcze bardziej niebezpieczna. Mi^ekkie podlo^ze"+
            " w zdradliwy spos^ob prowadzi twoje kroki miedzy martwymi, przeg"+
            "nitymi krzewami, kt^orych cienie rozciagaj^a si^e na pod^lo^zu w"+
            " zawi^lych, na sw^oj spos^b groteskowych mozaikach. Stare konary"+
            " drzew, kt^ore niegdy^s mog^ly zachwyca^c swoim ogromem, teraz s"+
            "toj^a pojedy^nczo i spogladaj^a na rozleg^le tereny dooko^la, ni"+
            "czym zakl^eci, u^spieni stra^znicy. Na zachodzie s^lycha^c "+
            "niespokojny i gwa^ltowny szum Pontaru. ";
    }
    else if (pora_roku() == MT_LATO)
    {
        str += "Powietrze w tym miejscu zdaje si^e by^c znacznie g^estsze"+
            " od duchoty i smrodu rozk^ladaj^acych si^e ro^slin. Gor�c "+
			"panuj�cy tutaj za dnia wydaj� si� nie ust�powa� nawet w nocy. "+
			"Podtrzymywany ci�kim oddechem paruj�cej i grz�skiej ziemi "+
			"prowadz�cej w zdradliwy spos�b  twe kroki mi�dzy martwymi, "+
			"przegnitymi krzewami, kt�rych cienie rozciagaj� si� na podlo�u "+
			"w zawi�ych i na sw�j spos�b groteskowych mozaikach. Stare "+
			"konary drzew  niegdy� zachwycajace swoim ogromem, teraz stoj� "+
			"pojedy�czo i spogladaj� na rozleg�e tereny dooko�a, niczym "+
			"zakl�ci, u�pieni stra�nicy.  ";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str += "Powietrze w tym miejscu zdaje si^e by^c znacznie g^estsze"+
            " od duchoty i smrodu rozk^ladaj^acych si^e ro^slin. Mi^ekkie pod"+
            "^lo^ze w zdradliwy spos^ob prowadzi twoje kroki mi^edzy martwymi,"+
            " przegnitymi krzewami, kt^orych cienie rozciagaj^a si^e na pod^lo"+
            "^zu w zawi^lych, na sw^oj spos^ob groteskowych mozaikach. Stare k"+
            "onary drzew, kt^ore niegdy^s mog^ly zachwyca^c swoim ogromem, te"+
            "raz stoj^a pojedy^nczo i spogladaj^a na rozleg^le tereny dooko^l"+
            "a, niczym zakl^eci, u^spieni stra^znicy. Na zachodzie s^lycha^c "+
            "niespokojny i gwa^ltowny szum Pontaru.";
    }

    return str + "\n";
}
