/**
 * \file /d/Standard/Redania/Piana_okolice/Bagno/lokacje/bagno_15.c
 *
 * Bagno opodal Piany.
 *
 * @author Aine
 * @date   2008
 */

#include "dir.h"
#include <macros.h>
#include <pogoda.h>
#include <mudtime.h>
#include <stdproperties.h>

inherit PIANA_OKOLICE_BAGNO_STD;

void
create_piana_bagno()
{
    set_short("Cuchn^ace bagnisko");

    add_t_exit("11", "ne");
    add_t_exit("17", "sw");
    add_t_exit("18", "s");
    add_exit(PIANA_OKOLICE_TRAKT_LOKACJE + "droga_05.c", "e",  0, FATIGUE, 0);
    add_exit(PIANA_OKOLICE_TRAKT_LOKACJE + "droga_04.c", "se", 0, FATIGUE, 0);

    add_npc(PIANA_OKOLICE_BAGNO_LIVINGI + "los");
    add_prop(ROOM_I_WCIAGA, 0);
}

public string
exits_description()
{
    return "Ledwo widoczna dr^o^zka prowadzi na p^o^lnocny-wch^od,"+
        " po^ludniowy-zach^od oraz na po^ludnie. Mo^zesz tak^ze dostrzec"+
        " wydeptan^a scie^zk^e na wschodzie i na po^ludniowym wschodzie.\n";
}

string
dlugi_opis()
{
    string str = "";

    if (CZY_JEST_SNIEG(this_object()))
    {
        str += "Zmro^zona zimowym ch^lodem ziemia w tej cz^e^sci bagien wydaje si^e by^c"+
            " pewniejszym gruntem, jednak z ka^zdym krokiem nale^zy zachowa^c ostro^zno^s^c."+
            " Okryte ^snie^zn^a pierzyn^a bagno jest jakby nieco u^spione."+
            " Smr^od w tym miejscu nieco ust^api^l daj^ac mo^zliwo^s^c zaczerpni^ecia"+
            " pe^lniejszego oddechu bez nara^zania si^e na nudno^sci. Cienie padaj^ace z"+
            " pojedy^nczych, martwo wystaj^acych z ziemi pni igraj^a z Twoim wzrokiem..."+
            " Drzewa i krzewy sk^apane szaro^sci^a pochylaj^a si^e nad sm^etn^a, grz^ask^a"+
            " ziemi^a w ge^scie rozpaczy lub te^z politowania. ";
    }
    else if (pora_roku() == MT_WIOSNA)
    {
        str += "Nieco r^owniejsza i mniej b^lotnista ziemia w tej cz^e^sci bagien wydaje"+
            " si^e by^c pewniejszym gruntem, jednak z ka^zdym krokiem nale^zy zachowa^c"+
            " ostro^zno^s^c. Budz^aca si^e do ^zycia ro^slinno^s^c bynajmniej nie dodaje uroku"+
            " temu miejscu. Smr^od nieco ust^api^l daj^ac mo^zliwo^s^c zaczerpni^ecia pe^lniejszego oddechu"+
            " bez nara^zania si^e na nudno^sci. Cienie padaj^ace z pojedy^nczych, martwo wystaj^acych"+
            " z ziemi pni igraj^a z twoim wzrokiem... Drzewa i krzewy sk^apane szaro^sci^a pochylaj^a"+
            " si^e nad sm^etn^a, grz^ask^a ziemi^a w ge^scie rozpaczy lub te^z politowania. ";
    }
    else if (pora_roku() == MT_LATO)
    {
        str += "Nieco r^owniejsza i mniej b^lotnista ziemia w tej cz^e^sci bagien wydaje"+
            " si^e by^c pewniejszym gruntem, jednak z ka^zdym krokiem nale^zy zachowa^c"+
            " ostro^zno^s^c. Ledwo widoczna, ^zywa ro^slinno^s^c bynajmniej nie dodaje uroku"+
            " i ^swie^zo^sci miejscu, w kt^orym si^e znajdujesz. Na szcz^e^scie smr^od"+
            " nieco ust^api^l daj^ac mo^zliwo^s^c zaczerpni^ecia pe^lniejszego oddechu"+
            " bez nara^zania si^e na nudno^sci. Cienie padaj^ace z pojedy^nczych, martwo wystaj^acych"+
            " z ziemi pni igraj^a z twoim wzrokiem... Drzewa i krzewy sk^apane szaro^sci^a pochylaj^a"+
            " si^e nad sm^etn^a, grz^ask^a ziemi^a w ge^scie rozpaczy lub te^z politowania. ";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str += "Nieco r^owniejsza i mniej b^lotnista ziemia w tej cz^e^sci bagien wydaje"+
            " si^e by^c pewniejszym gruntem, jednak z ka^zdym krokiem nale^zy zachowa^c"+
            " ostro^zno^s^c, poniewa^z rozk^ladaj^ace si^e, namokni^ete li^scie i trawy uczyni^ly"+
            " j^a ^slisk^a. Na szcz^e^scie smr^od w tym miejscu nieco ust^api^l daj^ac mo^zliwo^s^c"+
            " zaczerpni^ecia pe^lniejszego oddechu bez nara^zania si^e na nudno^sci. Cienie padaj^ace"+
            " z pojedy^nczych, martwo wystaj^acych z ziemi pni igraj^a z twoim wzrokiem... Drzewa"+
            " i krzewy sk^apane szaro^sci^a pochylaj^a si^e nad sm^etn^a, grz^ask^a ziemi^a w ge^scie"+
            " rozpaczy lub te^z politowania. ";
    }

    return str + "\n";
}
