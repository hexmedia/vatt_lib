/**
 * \file /d/Standard/Redania/Rinde/Piana_okolice/Bagno/std/bagno.c
 *
 * Bagna ko�o traktu do Piany. Expowisko. Made by Aine.
 *
 * @note
 *   Moskity pope�ni� Verek.
 *   Jakby kto� szuka� - W Sapku nie by�o nigdzie mowy o krwio�erczych
 *   moskitach, tylko o takich zwyk�ych komarach. Ale ponios�a
 *   mnie fantazja no i mamy takie madafaki, kt�re:
 *      -gryz� w niechronione miejsca na ciele
 *        +bonus: gryz� po twarzy, kt�r� zabezpieczy� mo�na jedynie
 *         moskitier� zakupion� w sklepie.
 *      -im wi�cej razy ugryz� tym wi�ksza szansa na chor�bsko bagienne :)
 *       (to te� m�j wymys� :P nic takiego - ot,
 *       b�ble wyskakuj�, gor�czka, pocimy si� i m�czymy.
 *       Remedium na to to.... TODO? Nie ma remedium,
 *       samo mija. :)
 *
 * @author Vera
 * @date   2008
 */

inherit "/std/bagno";

#include "dir.h"
#include <mudtime.h>
#include <language.h>
#include <stdproperties.h>
#include <pogoda.h>
#include <filter_funs.h>
#include <macros.h>
#include <sit.h>
#include <wa_types.h>

#define UPIERDOLIL_MOSKITO "_upierdolil_moskito"

#define TWARZ -69
#define SLOTY_DO_UGRYZIENIA A_ALL_SLOTS - ({A_ARMS,A_SHOULDERS,A_FOREARMS,\
                  A_TORSO,A_ROBE, A_HANDS,A_LEGS,A_THIGHS,A_SHINS,A_FEET,\
                    A_R_LEG, A_L_LEG}) + ({TWARZ})

#define JAK ({"nieprzyjemnie","dokuczliwie","dr�cz�co",\
             "dotkliwie","dra�ni�co","uci��liwie","niezno�nie"})

string moskity();
string evencik_pierdolnik();
string dlugi_opis();
string opis_polmroku();

int moskit_alrm;

void
create_piana_bagno()
{
}

nomask void
create_bagno()
{
    set_long(&dlugi_opis());
    set_polmrok_long(&opis_polmroku());

    add_prop(ROOM_I_WSP_Y, WSP_Y_PIANA_BAGNO);
    add_prop(ROOM_I_WSP_X, WSP_X_PIANA_BAGNO);
    create_piana_bagno();

    set_event_time(7,20);
    add_event("@@moskity:"+file_name(TO)+"@@");
    //i kilka innych o komarach, ale ju� zwyk�ych:
    add_event("@@evencik_pierdolnik:"+file_name(TO)+"@@");
}

/**
 * Funkcja dodaje lokalne wyj�cie, poniewa� we wszystkich wyj�ciach r�ni si� tylko
 * numerek i kierunek wi�c po co za du�o pisa�?
 *
 * @param lok   Numer lokacji
 * @param kier  Kierunek w jakim wyj�cie prowadzi
 */
nomask void
add_t_exit(string lok, string kie)
{
    add_exit(PIANA_OKOLICE_BAGNO_LOKACJE + "bagno_" + lok + ".c", kie, 0, FATIGUE, 0);
}

string
dlugi_opis()
{
    return "Ups! Zg�o� b��d!\n";
}

string
opis_polmroku()
{
    string str;

    if (CZY_JEST_SNIEG(this_object()))
    {
        str = "W mroku jeste^s w stanie dostrzec jedynie lekk^a biel "+
            "^sniegu le^z^acego na bagnach.\n";
    }
    else
    {
        str = "Nad bagnem zapad� mrok, dlatego nie jeste� w stanie dostrzec "+
            "zbyt wiele.\n";
    }

    return str;
}

string
moskity()
{
    object *gracze = FILTER_PLAYERS(all_inventory(TO));

    if (!sizeof(gracze))
        return "";

    if (get_alarm(moskit_alrm) != 0)
        return "";

    moskit_alrm = set_alarm(3.0,0.0,"x");

    int *wolne_sloty, losowy_slot, dmg;

    //dla ka�dego gracza osobno
    foreach (object x : gracze)
    {
        //czasem jakiemu� graczowi darujemy...
        if (sizeof(gracze) > 1 && !random(3))
           continue;

        wolne_sloty = ({ });
        losowy_slot = -1;

        //sprawdzamy, jakie ma wolne sloty
        foreach (int i : SLOTY_DO_UGRYZIENIA)
            if (!sizeof(x->query_armour(i)))
                wolne_sloty += ({i});

        //specjalny slot "twarz":
        if (sizeof(x->query_armour(A_HEAD)) &&
            filter(x->query_armour(A_HEAD),&->query_moskitiera()))
            wolne_sloty -= ({TWARZ});

        //ustawiamy gdzie upierdoli�
        if (sizeof(wolne_sloty))
            losowy_slot = wolne_sloty[random(sizeof(wolne_sloty))];
        else //chyba, �e gracz jest zas�oni�ty
            continue;

        //teraz wylosujmy jak bardzo :P
        dmg = random(600);

        //TODO: Je�li k�uj� po twarzy to powinno odbiera� hp z twarzy, i to troche mniejsz� ilo�� bo na g�owie
        //      jest naprawde niewiele:P Bo teraz mo�e by� tak troche dziwnie, k�uj� ci� w twarz a spada Ci
        //      hp na nogach np:P [Krun]
        //Kuj! kuj! Au�! :)
        x->reduce_hp(0, dmg, 10);
     //   write("dmg:"+dmg+"\n"); Zakomentowalem, bo zapomnimy o tym i gracze beda widzieli jakis dmg. [Avard]
        //opisik
        x->catch_msg("Moskity k�uj� ci� "+JAK[random(sizeof(JAK))]+" "+
            (losowy_slot==TWARZ?"po twarzy":"w "+SLOT_NAME(losowy_slot,PL_BIE))+
            ".\n");
        tell_roombb(TO,"Moskity dokuczaj� "+QIMIE(x,PL_CEL)+".\n",({x}));

        //dodajmy kt�ry to ju� raz gryziemy...
        x->add_prop(UPIERDOLIL_MOSKITO,x->query_prop(UPIERDOLIL_MOSKITO)+1);

        //sprawd�my, czy wylosujemy chor�bsko
        if (x->query_prop(UPIERDOLIL_MOSKITO) > 5+random(10))
        {
            if (member_array("choroba",all_inventory(x)->query_name()) == -1)
            {
                object choroba = clone_object("/std/efekty/choroba_moskitow_bagiennych.c");
                choroba->zachoruj(x);
            }
            x->remove_prop(UPIERDOLIL_MOSKITO);
        }
    }

    return "";
}

void
evencik_pierdolnik_powrot()
{
    tell_room(TO,"...by po chwili zn�w powr�ci� do swego ulubionego "+
        "zadr�czania innych.\n");
}

string
evencik_pierdolnik()
{
    switch (random(6))
    {
        case 0: return "Chmara moskit�w lata tu� nad twoj� g�ow�.\n";
        case 1: return "Twych uszu dobiega nieprzyjemne bzyczenie owad�w.\n";
        case 2: return "R�j brz�cz�cych moskit�w lata przed tob� utrudniaj�c "+
                            "widoczno��.\n";
        case 3: return "Wszechobecne komary daj� o sobie zna�.\n";
        case 4: return "Natr�tne moskity lataj� wok� ciebie domagaj�c si� "+
                        "cho� jednej kropli twej krwi.\n";
        case 5:
                if(get_alarm(moskit_alrm) != 0)
                    return "";
                moskit_alrm = set_alarm(3.0,0.0,"x");
                tell_room(TO,"Moskity jakby na chwil� gdzie� znikn�y...\n");
                set_alarm(1.0+itof(random(3)),0.0,"evencik_pierdolnik_powrot");
                return "";
    }
}
