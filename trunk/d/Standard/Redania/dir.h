#define REDANIA "/d/Standard/Redania/"
#define ZIOLA "/d/Standard/ziola/"
#define UBRANIA_GLOBAL "/d/Standard/items/ubrania/"
#define RYBY_GLOBAL    "/d/Standard/ryby/"

#define DYLEK_PIANA_RINDE (REDANIA+"dylek_piana_rinde/")

#define RINDE                           (REDANIA + "Rinde/")
#define RINDE_NPC                       (RINDE + "npc/")

#define POLUDNIE                        (RINDE + "Poludnie/")
#define POLUDNIE_LOKACJE                (POLUDNIE + "lokacje/")

#define TRAKT_RINDE                     (REDANIA + "Rinde_okolice/Trakt/")
#define TRAKT_RINDE_LOKACJE             (TRAKT_RINDE + "lokacje/")
#define TRAKT_RINDE_NPC                 (TRAKT_RINDE + "npc/")

#define BIALY_MOST                      (REDANIA + "Bialy_Most/")

#define BIALY_MOST_LOKACJE              (BIALY_MOST + "lokacje/")
#define BIALY_MOST_LOKACJE_ULICE        (BIALY_MOST_LOKACJE+"ulice/")

#define BIALY_MOST_PRZEDMIOTY           (BIALY_MOST+"przedmioty/")
#define BIALY_MOST_BRONIE               (BIALY_MOST_PRZEDMIOTY + "bronie/")
#define BIALY_MOST_ZBROJE               (BIALY_MOST_PRZEDMIOTY + "zbroje/")
#define BIALY_MOST_UBRANIA              (BIALY_MOST + "przedmioty/ubrania/")
#define RINDE_UBRANIA                   (RINDE + "przedmioty/ubrania/")

#define BIALY_MOST_OKOLICE              (REDANIA + "Bialy_Most_okolice/")
#define TRAKT_BIALY_MOST_OKOLICE        (BIALY_MOST_OKOLICE+"Trakt/")
#define TRAKT_BIALY_MOST_OKOLICE_LOKACJE    (TRAKT_BIALY_MOST_OKOLICE + "lokacje/")
#define BRZEG_BIALY_MOST_OKOLICE        (BIALY_MOST_OKOLICE+"Brzeg/")
#define BRZEG_BIALY_MOST_LOKACJE        (BRZEG_BIALY_MOST_OKOLICE+"lokacje/")

#define WIOSKA_BIALY_MOST_OKOLICE       (REDANIA+"Wioska_BM/")
#define WIOSKA_BIALY_MOST_OKOLICE_LOKACJE (WIOSKA_BIALY_MOST_OKOLICE+"lokacje/")

#define KANALY                          (REDANIA + "Rinde_kanaly/")
#define KANALY_LOKACJE                  (KANALY + "lokacje/")
#define KANALY_DRZWI                    (KANALY + "drzwi/")
#define KANALY_OBIEKTY                  (KANALY + "obiekty/")

#define ZAKON                           (REDANIA + "Zakon/")
#define ZAKON_LOKACJE                   (ZAKON + "lokacje/")
#define ZAKON_DRZWI                     (ZAKON + "drzwi/")

#define ZAKON_OKOLICE                   (REDANIA+"Zakon_okolice/")
#define TRAKT_ZAKON_OKOLICE             (ZAKON_OKOLICE+"Trakt/")
#define TRAKT_ZAKON_OKOLICE_LOKACJE     (TRAKT_ZAKON_OKOLICE+"lokacje/")
#define LAS_ZAKON_OKOLICE               (ZAKON_OKOLICE+"Las/")
#define LAS_ZAKON_OKOLICE_LOKACJE       (LAS_ZAKON_OKOLICE+"lokacje/")
#define LAS_ZAKON_OKOLICE_LIVINGI       (LAS_ZAKON_OKOLICE+"livingi/")

#define TRETOGOR                        (REDANIA + "Tretogor/")
#define TRETOGOR_LOKACJE                (TRETOGOR + "lokacje/")
#define TRETOGOR_DRZWI                  (TRETOGOR + "drzwi/")

#define PIANA                           (REDANIA + "Piana/")
#define PIANA_LOKACJE                   (PIANA + "lokacje/")
#define PIANA_NPC                       (PIANA + "npc/")
#define PIANA_LOKACJE_ULICE             (PIANA_LOKACJE + "ulice/")
#define PIANA_OKOLICE                   (REDANIA + "Piana_okolice/")
#define PIANA_OKOLICE_TRAKT             (PIANA_OKOLICE + "Trakt/")
#define PIANA_OKOLICE_TRAKT_LOKACJE     (PIANA_OKOLICE_TRAKT + "lokacje/")
#define PIANA_OKOLICE_BAGNO             (PIANA_OKOLICE + "Bagno/")
#define PIANA_OKOLICE_BAGNO_LOKACJE     (PIANA_OKOLICE_BAGNO + "lokacje/")

#define MURIVEL                         (REDANIA + "Murivel/")
#define MURIVEL_ZACHOD                  (MURIVEL + "Zachod/")
#define MURIVEL_ZACHOD_ULICE            (MURIVEL_ZACHOD + "ulice/")

#define MURIVEL_OKOLICE                 (REDANIA + "Murivel_okolice/")
#define LAS_WIEWIOREK                   (MURIVEL_OKOLICE + "Las_wiewiorek/")
#define LAS_WIEWIOREK_LOKACJE           (LAS_WIEWIOREK + "lokacje/")
#define LAS_WIEWIOREK_STD               (LAS_WIEWIOREK + "std/las.c")
#define LAS_WIEWIOREK_OBIEKTY           (LAS_WIEWIOREK + "obiekty/")
#define LAS_WIEWIOREK_LIVINGI           (LAS_WIEWIOREK + "livingi/")

#define NOVIGRAD                        (REDANIA + "Novigrad/")
#define NOVIGRAD_PORTOWA                (NOVIGRAD+ "Portowa/")

#define REDANIA_STD                     (REDANIA + "std/redania.c")

//Ustawiamy og�lny reda�ski SLEEP_PLACE
#ifdef SLEEP_PLACE
#undef SLEEP_PLACE
#endif SLEEP_PLACE
#define SLEEP_PLACE                     (RINDE + "Polnoc/Karczma/lokacje/start.c")

//quest zielarza z bagien piany
#define QUESTY                          "/d/Standard/questy/"
#define ZIELARZ_ZROBILI                 QUESTY + "zielarz/zrobili"
#define ZIELARZ_ROBIACY                 QUESTY + "zielarz/robiacy"





//wsp�rz�dne do planu pogodowego
//notka: jak dodajemy nowy teren, to: je�li ma on mie� sw�j w�asny
//plan pogodowy, nie by� wyliczanym z plan�w s�siednich
//(robimy tak np. dla du�ych miast) to: musimy te� doda� do /obsluga/other/pogody
//tam ustawiamy miasto w realu jakie ma by�.
//musimy te� si� upewni� czy w /Standard/pogoda/warunki.c ten nowy obszar te�
//jest dodawany w funkcji odnow(). V
//Rinde to centrum
//+-+ NIE ZMIENIA� KOMENTARZY! +-+
//=== POGODA ===
//Rinde
#define WSP_X_RINDE			0
#define WSP_Y_RINDE			0
//Las mi�dzy Rinde a Bia�ym Mostem
#define WSP_X_RINDE_BM_LAS		-2
#define WSP_Y_RINDE_BM_LAS		-1
//Bia�y Most
#define WSP_X_BIALY_MOST		-4
#define WSP_Y_BIALY_MOST		-2
//Las wiewi�rek mi�dzy Rinde a Murivel
#define WSP_X_LAS_RINDE_MURIVEL		2
#define WSP_Y_LAS_RINDE_MURIVEL		0
//Murivel
#define WSP_X_MURIVEL			4
#define WSP_Y_MURIVEL			-2
//Piana
#define WSP_X_PIANA			-5
#define WSP_Y_PIANA			2
//Bagno przy Pianie
#define WSP_X_PIANA_BAGNO		-5
#define WSP_Y_PIANA_BAGNO		3
//Trakt Piana - Bia�y Most
#define WSP_X_TRAKT_PIANA_BM		-4
#define WSP_Y_TRAKT_PIANA_BM		0
//Wioska na wsch�d od BM
#define WSP_X_WIOSKA_BM			-3
#define WSP_Y_WIOSKA_BM			0
//Okolice Zakonu (trakt,lasy,utopce)
#define WSP_X_ZAKON_OKOLICE		0
#define WSP_Y_ZAKON_OKOLICE		2
//Zakon
#define WSP_X_ZAKON			1
#define WSP_Y_ZAKON			2
//Jeziorko niedaleko Zakonu
#define WSP_X_ZAKON_JEZIORKO 1
#define WSP_Y_ZAKON_JEZIORKO 3
//Las komanda
#define WSP_X_LAS_ELFOW 2
#define WSP_Y_LAS_ELFOW 0
//Wawoz i ruiny
#define WSP_X_RUINY_I_WAWOZ 1
#define WSP_Y_RUINY_I_WAWOZ 3
//Wioska i cmentarz nieopodal oxenfurtu
#define WSP_X_GRABOWA_BUCHTA -6
#define WSP_Y_GRABOWA_BUCHTA 0
//Novigrad
#define WSP_X_NOVIGRAD -7
#define WSP_Y_NOVIGRAD 4
//=== POGODA ===
//+-+ NIE ZMIENIA� TEGO KOMENTARZA! +-+
