#include "dir.h"

inherit WIOSKA_OXEN_STD;

#include <stdproperties.h>
#include <mudtime.h>
#include <pogoda.h>
#include <macros.h>


void
create_wioska()
{
}

nomask void
create_wioska_oxen()
{
    set_long("@@dlugi_opis@@");
    set_polmrok_long("@@opis_nocy@@");
    add_sit("na ziemi", "na ziemi", "z ziemi", 0);
    set_event_time(300.0);

 // add_prop(ROOM_I_TYPE, jaki?);

    create_wioska();

    add_prop(ROOM_I_WSP_Y, WSP_Y_GRABOWA_BUCHTA);
    add_prop(ROOM_I_WSP_X, WSP_X_GRABOWA_BUCHTA);

    add_item(({"rzek^e"}), "Duuu^za rzeka.\n");
}
