
/* By Veli 04.12.09
Kostnica wioska Oxen*/

#pragma strict_types

#include "dir.h"
#include <pl.h>
#include <stdproperties.h>
#include <macros.h>
#include <cmdparse.h>
#include <filter_funs.h>
#include <materialy.h>
#include <object_types.h>
#include <mudtime.h>
inherit "/std/container";

void create_container()
{
    ustaw_nazwe("st^o^l");
    dodaj_przym("zimny","zimni");
    dodaj_przym("stalowy","stalowi");
    set_long("Wykuty z kawa^la stali blat tego metalowego i wyj^atkowo zimnego"+
        " w dotyku sto^lu opiera si^e na czterech odlanych ze stali n^ogach."+
        " Raczej nie da rady go przestawi�, czy cho�by nim poruszy�, ze"+
        " wzgl�du na solidne nity, zespalaj�ce go z kamienn� posadzk�.\n");
    add_prop(OBJ_I_WEIGHT,17000);
    add_prop(OBJ_I_VOLUME,90000);
    add_prop(OBJ_I_VALUE,500);
    add_prop(OBJ_I_NO_GET, "St^o^l jest na sta^le z^l^aczony z pod^log^a"+
        " za pomoc^a ca^lkiem solidnych nit^ow. Bez odpowiednich narz^edzi"+
        " nie da rady go z niej wyrwa^c.\n");
    ustaw_material(MATERIALY_STAL);
    set_type(O_MEBLE);
    add_subloc("na",0,"ze");
    add_subloc_prop("na", SUBLOC_I_TYP_W,10);
    make_me_sitable("na","na g^ladkim, zimnym stole","ze sto^lu",2);
    //add_object("/d/Aretuza/veli/Wioska_Oxen/przedmioty/obiekty/cialo_ca.c",2);
    clone_object(WIOSKA_OXEN_PRZEDMIOTY+"/obiekty/cialo_ca.c")->move(this_object(),"na stole");
    /*smell_effect()
   {
        "Wydziela md�y metaliczny zapach, typowy dla"+
        " przesi�kni�tej wilgoci� stali. Dodatkowo wyra�nie wyczuwasz"+
        " ostry zapach formaliny i jakby zetla�y spirytus.\n");
    } nie wiem jak */
    /*set_event_time(300, 200);
    add_event("Dochodzi ci� md�a wo� zetla�ego spirytusu przemieszana"+
        " z jakim� s�odkawym, trudnym do okreslenia odorem rozk�adu.\n");*/
}

public int
is_stol_veli()
{
   return 1;
}