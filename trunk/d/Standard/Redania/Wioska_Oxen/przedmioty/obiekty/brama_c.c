
#pragma strict_types
#include "/d/Standard/Redania/dir.h"
#include <stdproperties.h>
#include <mudtime.h>
#include <pl.h>
#include <filter_funs.h>
#include <macros.h>
#include "dir.h"

inherit "/std/gate.c";

//string info_security_lvl0();
string dlugi_skad();

void
create_gate()
{
    set_other_room(WIOSKA_OXEN_LOKACJE+"cmentarz/c_2");
    set_open(1);
    set_locked(0);
    set_gate_id("cmentarz_buchta");
    set_pass_command(({"brama","cmentarz"}));
    set_pass_mess("wchodzi na cmentarzysko");
    godzina_zamkniecia(21);
    godzina_otwarcia(5);
    set_skad("@@dlugi_skad@@");
    dodaj_przym("pot^e^zny","pot^e^zni");
    dodaj_przym("stalowy","stalowi");
    set_long("@@dlugasny@@");
}

string dlugasny()
{
    string str;

    str = "Wysoka, dwuskrzyd^lowa brama starannie wykuta ze stali. Jedyn^a"+
        " ozdob^e stanowi du^ze, miedziane s^lonce, teraz ju^z zupe^lnie"+
        " zzielenia^lej, kt^orego promienie wykonano z kawa^lk^ow"+
        " po^lamanych ostrzy flamberg^ow.";

    str += opened_or_closed_desc();

    return str += "Skrzyd^lo pot^e^znej stalowej bramy jest uchylone.\n";

}

string dlugi_skad()
{
    string str;

    str = "";


    switch(MT_PORA_DNIA)
    {
	    case MT_WCZESNY_RANEK:
            str += "Wychodzi przez brame z cmentarzyska.\n."; 
            break;

	    case MT_RANEK:
            str += "Wychodzi przez brame z cmentarzyska.\n."; 
            break;

	    case MT_POLUDNIE:
            str += "Wychodzi przez brame z cmentarzyska.\n."; 
            break;

	    case MT_POPOLUDNIE:
            str += "Wychodzi przez brame z cmentarzyska.\n."; 
            break;

	    case MT_WIECZOR:
            str += "Wychodzi przez brame z cmentarzyska.\n."; 
            break;

        case MT_SWIT:
            str += "Co^s po drugiej stronie dobija si^e do bramy, usi^luj^ac"+
            " wyj^s^c z cmentarza!.";
            break;

        case MT_POZNY_WIECZOR:
            str += "Co^s po drugiej stronie dobija si^e do bramy, usi^luj^ac"+
            " wyj^s^c z cmentarza!.";
            break;

        case MT_NOC:
            str += "Co^s po drugiej stronie dobija si^e do bramy, usi^luj^ac"+
            " wyj^s^c z cmentarza!.";
            break;
    }
}

/*Bezskutecznie szarpiesz si^e z brama, usi^luj^ac j^a otworzy^c.
* bezskutecznie szarpie si^e z brama, usi^luj^ac j^a otworzy^c.


Brama moze si^e zamoistnie zamkn^ac jesli jest wiatr"
Jedno ze skrzyde^l bramy zatrzas^lo si^e niespodziewanie.

slonce wschodzi:
"Nagle cisz^e zak^l^oca g^lo^sne, przeciag^le skrzypni^ecie rdzewiej^acych"+
" zawias^ow! Uff, to tylko brama zosta^la w^la^snie otwarta przez grabarza.\n" 

slonce zachodzi:
"Wtem pojawia sie grabarz i z przeciaglym skrzypnieciem zamyka bram^e,"+
" a mosi^e^zne s^lo^nce jeszcze przez chwil^e odbija gasn^ace promienie"+
" ^swiat^la.\n" */