/* By Veli 08.12.09
Ga^l^azka z krzaka cmentarnego wioska Oxen
wachanie by cadron*/

#pragma strict_types
#include "/d/Standard/Redania/dir.h"
#include <pl.h>
#include <stdproperties.h>
#include <macros.h>
#include <mudtime.h>
#include <pogoda.h>
#include <filter_funs.h>
#include <object_types.h>
#include <materialy.h>
inherit "/std/holdable_object.c";

string dlugi_opis();

void
create_holdable_object()
{
    ustaw_nazwe(({"ga^l^azka", "ga^l^azki", "ga^l^azce", "ga^l^azk^e",
        "ga^l^azce", "ga^l^azk^a"}),({"ga^l^azki", "ga^l^azek", "ga^l^azkom",
        "ga^l^azki","ga^l^azkami","ga^l^azkach"}), 3);
    dodaj_przym("kolczasty","kolcza^sci");
    set_long("@@dlugi_opis");
    add_prop(OBJ_I_WEIGHT, 100);
    add_prop(OBJ_I_VOLUME, 50);
    add_prop(OBJ_I_VALUE, 5);
    ustaw_material(MATERIALY_IN_ROSLINA);
    set_type(O_KWIATY);

}

string
dlugi_opis()
{
    string str = "";


    switch(MT_PORA_ROKU)
    {
	case MT_WIOSNA:
            str += "Niewielka ga^l^azka obsypana drobnymi purpurowymi listkami"+
            " o ciemnozielonych spodach oraz wystaj^acymi spomi^edzy drobnych"+
            " kolc^ow gronami nie do ko^nca rozwini^etych p^aczk^ow"+
            " blado^z^o^ltych i nieco ^smierdz^acych kwiatuszk^ow.\n";
            break;
        case MT_LATO:
            str += "Niewielka ga^l^azka obsypana drobnymi purpurowymi listkami"+
            " o ciemnozielonych spodach oraz wystaj^acymi spomi^edzy drobnych"+
            " kolc^ow gronami w pe^lni rozkwit^lych p^aczk^ow blado^z^o^ltych"+
            " i nieco ^smierdz^acych kwiatuszk^ow.\n";
            break;
        case MT_JESIEN:
            str += "Niewielka ga^l^azka obsypana drobnymi, nieco ju^z zesch^lymi"+
            " purpurowymi listkami o ciemnozielonych spodach. Gdzieniegdzie"+
            " widzisz jeszcze resztki pozosta^lo^sci po malutkich,"+
            " czerwonawych owockach zebranych w ma^le grona.\n";
            break;
        case MT_ZIMA:
        default:
            str += "Naga, zdrewnia^la i nieco zbutwia^la ga^l^azka obsypana"+
            " wystaj^acycmi ostrymi kolcami.\n";
	    break;
    }

    return str;
}

int
smell_effect()
{
    string str = "";

    switch(MT_PORA_ROKU)
    {
	case MT_WIOSNA:
            str += "Unosisz kolczast^a ga^l^azk^e bli^zej twarzy i wci^agasz"+
            " nozdrzami lekko md^ly, s^lodkawy zapach z ledwie rozwini^etych"+
            " p^aczk^ow ^z^o^ltwaych kwiat^ow.\n";
            break;

        case MT_LATO:
            str += "Unosisz kolczast^a ga^l^azk^e bli^zej twarzy i wci^agasz"+
            " nozdrzami md^ly, s^lodkawy zapach z rozwini^etych p^aczk^ow"+
            " ^z^o^ltwaych kwiat^ow.\n";
            break;

        case MT_JESIEN:
            str += "Unosisz niemal pozbawion^a purpurowych list^ow ga^l^azk^e"+
            " i wci^agasz nozdrzami jej zetla^ly, nieco zbutwia^ly"+
            " zapaszek.\n";
            break;

        case MT_ZIMA:
        default:
            str += "Unosisz zdrewnia^l^a ga^l^azk^e bli^zej twarzy i wci^agasz"+
            " nozdzrzami nieco zbutwia^ly zapaszek gnij^acej ro^sliny.\n";
    }    

    saybb(QCIMIE(TP, PL_MIA) + " podnosi kolczast^a ga^l^azk^e bli^zej"+
        " twarzy i marszcz^ac nos ostro^znie j^a obw^achuje.\n");

    write(str);

    return 1;

}  

public int
is_galazka_berberys_veli()
{
   return 1;
}