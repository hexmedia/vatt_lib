
/* By Veli 04.12.09
Kostnica wioska Oxen*/

#pragma strict_types

#include "dir.h"
#include <pl.h>
#include <stdproperties.h>
#include <macros.h>
#include <filter_funs.h>
#include <materialy.h>
#include <object_types.h>
inherit "/std/object.c";

void create_object()
{
    ustaw_nazwe("cia�o");
    dodaj_przym("martwy","martwi");
    set_long("Jakby nie patrze� jest to trup. Martwe szcz�tki jakiego�"+
        " nieszcz�nika, kt�ry s�dz�c po stanie cia�a zmar� �mierci�"+
        " zupe�nie naturaln� do�ywaj�c p�nej staro�ci. Je�li oczywi�cie"+
        " uznamy, �e paskudna rana na piersi, wyra�nie pochodz�ca"+
        " od miecza, to naturalny spos�b, zej�cia z tego �wiata...\n");
    add_prop(OBJ_I_WEIGHT, 50000);
    add_prop(OBJ_I_VOLUME, 5000);
    add_prop(OBJ_I_VALUE, 0);
    set_type(O_INNE);
}

public int
is_cialo_veli()
{
   return 1;
}