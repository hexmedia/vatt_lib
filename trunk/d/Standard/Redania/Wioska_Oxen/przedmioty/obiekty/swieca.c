
/* By Veli 16.12.09
Swieca cmentarz, Grabowa Buchta*/

#pragma save_binary
#pragma strict_types
#include <stdproperties.h>
#include <std.h>
#include <wa_types.h>
#include <macros.h>
#include <materialy.h>
#include <object_types.h>
inherit "/lib/light.c";
inherit "/std/torch.c";

string dlugi_opis();

void
create_torch()
{
    ustaw_nazwe(({"^swieca", "^swiecy", "^swiecy", "^swiec^e","^swiec^a",
        "^swiecy"}),({"^swiece", "^swiec", "^swiecom","^swiece","^swiecami",
        "^swiecach"}), 3);
    random_przym(({"okopcony", "okopceni", "nad^lamany", "nad^lamani",
        "nadpalony", "nadpaleni","gruby", "grubi", "cienki","cie^ncy",
        "woskowy","woskowi"}),({"^z^o^ltawy","^z^o^ltawi", "zielonkawy",
        "zielonkawi","zwyk^ly","zwykli","czarny","czarni","wygi^ety",
        "wygi^eci","prosty","pro^sci"}), 2);
    set_long(&dlugi_opis());
    add_prop(OBJ_I_WEIGHT, 100);
    add_prop(OBJ_I_VOLUME, 90);
    add_prop(OBJ_I_VALUE, 20);
    ustaw_material(MATERIALY_GLINA);
    set_type(O_POCHODNIE);

    set_time(600+random(100));
    set_strength(2);
    set_value(13);
    set_time_left(600);

    config_light();
}

string
dlugi_opis()
{
    string str;

    str = "Ma^la zwyk^la ^swieca. Daje bardzo niewiele ^swiat^la i nawet"+
        " najl^zejszy podmuch wiatru mo^ze w ka^zdej chwili zgasi^c jej p^lomie^n.";

   	if(this_object()->query_prop(OBJ_I_LIGHT))
        {
        str += " Jarzy si^e w tej chwili niskim, jasnym p^lomieniem,"+
            " daj^acymi jednak wi^ecej dymu ni^z ^swiat^la.\n";
        }
    else
        {
        str += " W tej chwili to ju^z bardziej malutki ogarek ni^z zdatna"+
            " do zapalenia ^swieca, wi^ec raczej nie ma szans by"+
            " roz^swietli^c ni^a jak^akolwiek ciemno^s^c.\n";
        }

    return str;
}

public int
is_swieca_cmentarz()
{
   return 1;
}