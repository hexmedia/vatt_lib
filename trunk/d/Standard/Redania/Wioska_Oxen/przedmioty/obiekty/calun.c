
/* By Veli 04.12.09
Kostnica wioska Oxen*/

#pragma strict_types

#include "dir.h"
#include <pl.h>
#include <stdproperties.h>
#include <macros.h>
#include <filter_funs.h>
#include <materialy.h>
#include <object_types.h>
inherit "/std/object.c";

void create_object()
{
    ustaw_nazwe(({"ca�un", "ca�unu", "ca�unowi", "ca�un", "ca�unem", 
          "ca�unie"}),({"ca�uny", "ca�un�w", "ca�unom", "ca�uny",
          "ca�unami","ca�unach"}), PL_MESKI_NOS_NZYW);
    dodaj_przym("p��cienny","p�ocienni");
    set_long("Zwyczajny kawa�ek szarego, szorstkiego w dotyku p�otna"+
        " wykorzystywany do obwini�cia zw�ok przed poch�wkiem, b�dz"+
        " okrycia trumny.\n");
    add_prop(OBJ_I_WEIGHT, 300);
    add_prop(OBJ_I_VOLUME, 600);
    add_prop(OBJ_I_VALUE, 50);
    set_type(O_INNE);
}

public int
is_calun_veli()
{
   return 1;
}