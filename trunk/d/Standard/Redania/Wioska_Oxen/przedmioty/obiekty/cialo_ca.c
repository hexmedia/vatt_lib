
/* By Veli 04.12.09
Kostnica wioska Oxen*/

#pragma strict_types
#include "dir.h"
#include <pl.h>
#include <stdproperties.h>
#include <macros.h>
#include <filter_funs.h>
#include <materialy.h>
#include <object_types.h>
inherit "/std/object.c";

int odwin(string str);

void create_object()
{
    ustaw_nazwe("cia^lo");
    dodaj_przym("martwy","martwi");
    set_long("Jakby nie patrze^c jest to trup. Dodatkowo szczelnie zawini^ety"+
        " w po^smietny ca^lun trup, tak wi^ec trudno ci b^edzie stwierdzi^c, bez"+
        " odwini^ecia p^lotna kim by^l nieboszczyk oraz czemu zmar^l...\n");
    add_prop(OBJ_I_WEIGHT, 50000);
    add_prop(OBJ_I_VOLUME, 5000);
    add_prop(OBJ_I_VALUE, 0);
    set_type(O_INNE);
    add_cmd_item("cia^lo", "odwi^n", odwin, "Odwi^n co?\n");
}    

void init()
{
    ::init();
    add_action(odwin, "odwi^n");
}

int odwin(string str)
{

    if(query_verb() == "odwi^n")
       notify_fail("Odwi^n ca^lun?\n");

    if(!HAS_FREE_HANDS(TP))
    {
       write("Musisz mie^c obie d^lonie wolne, aby m^oc to zrobi^c.\n");
       return 1;
    }

    write("Ostro^znie rozk^ladasz zw^loki na ziemi i pochylaj^ac si^e nad nimi"+
        " zdejmujesz p^locienny ca^lun.\n");
    saybb(QCIMIE(TP, PL_MIA) + " rozk^lada ostro^znie zw^loki na ziemi i"+
        " pochylaj^ac si^e nad nimi zdejmuje okrywaj^acy je ca^lun.\n");

    clone_object(WIOSKA_OXEN_PRZEDMIOTY+"obiekty/cialo")->move(ENV(TP));
    clone_object(WIOSKA_OXEN_PRZEDMIOTY+"obiekty/calun")->move(TP);
    remove_object();

        return 1;
}

public int
is_cialo_ca_veli()
{
   return 1;
}