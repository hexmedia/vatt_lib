
/*By Veli 11.12.09
Olcha czarna - tereny podmok^le - lasy ^l^egowe*/

#include <materialy.h>
#include <mudtime.h>
#include <object_types.h>
#include <pl.h>
#include <stdproperties.h>
inherit "/std/drzewo";

//string dlugi_opis();

void create_tree()
{
        ustaw_nazwe(({"olcha", "olchy", "olsze", "olch^e", "olch^a", 
            "olsze"}),({"olchy", "olch", "olchom", "olchy", "olchami",
            "olchach"}),3);
        random_przym(({"czarny", "czarni", "sp^ekany", "sp^ekani", "omsza^ly",
            "omszali"}),({"spchuchnia^ly", "spruchniali", "smuk�y",
            "smukli"}), 2);
        set_long("@@dlugi@@");
        set_type(O_DREWNO);
        ustaw_material(MATERIALY_DR_DAB, 100);
        set_gestosc(0.75);
        add_prop(OBJ_I_WEIGHT, random(1450000)+50000);
        set_sciezka_galezi("/d/Aretuza/veli/Wioska_Oxen/przedmioty/obiekty/drzewa/olcha/galaz.c");
        set_sciezka_klody("/d/Aretuza/veli/Wioska_Oxen/przedmioty/obiekty/drzewa/olcha/kloda.c");
        set_cena(4.95);
                    
        setuid();
        seteuid(getuid());
}

string dlugi()
{
        string opis;
        
         opis = "";

        if(query_mlode())
        {
               opis += "Niewielki, rozkrzewiony krzaczek czarnej olchy"+
                    " wybija si^e strzeli^scie ku g^orze, dzwigaj^ac na"+
                    " sobie cie^zar gi^etkich nagich ga^l^azek z"+
                    " wyra^xnymi, ja^sniejszymi przetchlinkami.";
        }
        else
        {
                opis += "Rozro^sni^ete, wysokie na oko^lo dwadzie^scia metr^ow"+
                    " drzewo o ciemnoszarej, prawie czarnej sp^ekanej korze."+
                    " Grubsze, czarne ga^l^ezie odchodz^a niemal poziomo"+
                    " od g^lownego pnia, tworz^ac niezbyt skupion^a koron^e"+
                    " z ca^l^a mas^a drobnych odrost^ow.";
        }

    switch(MT_PORA_ROKU)
    {
	    case MT_WIOSNA:
            opis += "Zieleni^ace si^e lepkie p^aczki wyrastaj^a zen na"+
            " d^lugich szypu^lkach mi^edzy zdrewnia^lymi, zesz^lorocznymi"+
            " owocostanami, za^s jajowate drobne listki o falowanej"+
            " b^lyszcz^acej powierzchni szumi^a cicho poruszane"+
            " niewidocznymi podmuchami wiatru.";
            break;
        case MT_LATO:
            opis += "Mlode p^edy rozwijaj^a si^e pr^e^znie, spijaj^ac ka^zd^a"+
            " kropl^e dost^epnej wilgoci, a jajowate drobne listki o"+
            " falowanej b^lyszcz^acej powierzchni szumi^a cicho poruszane"+
            " niewidocznymi podmuchami wiatru.";
            break;
        case MT_JESIEN:
            opis += "Szarej^ace, lepkie li^scie o falowanej powierzchni"+
            " zaczynaj^a obsypywa^c si^e wok^o^l ust^epuj^ac miejsca"+
            " wytwarzanym na zim^e czerwonawym, zebranym w kotki"+
            " kwiatostanom.";
            break;
        case MT_ZIMA:
        default:
            opis += "Bezlistne, zczernia^le ga^l^ezie by^lyskaj^a"+
            " gdzieniegdzie zieleni^acymi si^e nie^smia^lo, zebranym"+
            " w czerwonawe kotki kwiatami wyzieraj^acymi spomi^edzy"+
            " zesz^lorocznych, szyszkowatych owocostan^ow.";
	    break;
    }

       {
         if(query_sciete())
             {
             opis += " Jest sobie sciete drzewo\n";
                }
         else
                {
             opis += " Nie jest sobie sciete?\n";
                }
        }


        return opis;

}