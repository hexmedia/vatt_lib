
/*By Veli 11.12.09
Olcha czarna kloda - teren podmokly - lasy ^l^egowe*/

inherit "/std/drzewo/kloda";

#include <materialy.h>
#include <object_types.h>

void create_kloda()
{
        ustaw_nazwe("kloda");
        dodaj_przym("smuk^ly", "pop^ekany");
        set_long("Smuk^ly pie^n ^sci^etej czarnej olchy, o pop^ekanej"+
            " czarnawej korze nosz^acej na sobie resztki mchu oraz"+
            " odcietych konar^ow i wi^eszkych ga^l^ezi.\n");
        ustaw_material(MATERIALY_DR_DAB, 100);
        set_type(O_DREWNO);
        
        setuid();
        seteuid(getuid());
}