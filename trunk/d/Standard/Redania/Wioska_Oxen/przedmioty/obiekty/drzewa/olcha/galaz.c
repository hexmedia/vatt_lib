
/*By Veli 11.12.09
Olcha czarna galaz - teren podmokly - lasy ^l^egowe*/

#include <materialy.h>
#include <mudtime.h>
#include <object_types.h>
inherit "/std/drzewo/galaz";

string dlugi_opis();

void create_galaz()
{
        ustaw_nazwe("galaz");
        random_przym("prosty:pro�ci wykr^econy:wykr^eceni ^lukowaty:^lukowaci"
                +" szponiasty:szponia�ci||omsza^ly:omszali chropowaty:chropowaci"
                +" pomarszczony:pomarszczeni", 2);
        set_long(&dlugi_opis());
        ustaw_material(MATERIALY_DR_DAB, 100);
        set_type(O_DREWNO);
}

string 
dlugi_opis()
{
        string str = "";

    switch(MT_PORA_ROKU)
    {
	    case MT_WIOSNA:
            str += "Ga^l^azk^e pokrywaj^a lepkie, zielonkawe p^aki"+
            " wyrastaj^ace spomi^edzy, zdrewnia^lych, zesz^lorocznych"+
            " owocostan^ow w kszta^lcie drobnych szyszek.\n";
            break;
        case MT_LATO:
            str += "Ga^l^ezie obsypane s^a ciemnozielonymi, jajowatymi"+
            " li^s^cmi. Ich lepka, pofalowana powierzchnia zdaje si^e"+
            " b^lyszcze^c.\n";
            break;
        case MT_JESIEN:
            str += "Szarej^ace, lepkie li^scie o falowanej powierzchni"+
            " zaczynaj^a obsypywa^c si^e wok^o^l ust^epuj^ac miejsca"+
            " wytwarzanym na zim^e czerwonawym, zebranym w kotki"+
            " kwiatostanom.\n";
            break;
        case MT_ZIMA:
        default:
            str += "Bezlistne, zczernia^le ga^l^ezie by^lyskaj^a"+
            " gdzieniegdzie zieleni^acymi si^e nie^smia^lo, zebranym"+
            " w czerwonawe kotki kwiatami wyzieraj^acymi spomi^edzy"+
            " zesz^lorocznych, szyszkowatych owocostan^ow.\n";
	    break;
    }
 
        return str;
}