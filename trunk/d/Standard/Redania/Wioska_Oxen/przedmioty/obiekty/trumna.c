
/* By Veli 04.12.09
Kostnica wioska Oxen*/

#pragma strict_types
inherit "/std/container";

#include "../dir.h"
#include <pl.h>
#include <stdproperties.h>
#include <macros.h>
#include <cmdparse.h>
#include <filter_funs.h>
#include <materialy.h>
#include <object_types.h>

void create_container()
{
    ustaw_nazwe(({"trumna", "trumny", "trumnie", 
        "trumn^e", "trumn^a", "trumnie"}), ({"trumny", "trumien", "trumnom", 
        "trumny", "trumnami", "trumnach"}),3);
    dodaj_przym("d^lugi","d^ludzy");
    dodaj_przym("sosnowy","sosnowi");
    set_long("Zwyczajna, topornie zbita z sosnowych dech i solidnych"+
        " gwo^xdzi trumna. Ta konkretna jest mniej wi^ecej d^lugo^sci"+
        " doros^lego cz^lowieka i wydaje si^e by^c stworzona do niczego"+
        " innego jak do wyprawienia czyich^s martwych doczesnych"+
        " szcz^atk^ow wprost do ziemi.\n");
    add_prop(OBJ_I_WEIGHT,7000);
    add_prop(OBJ_I_VOLUME,10000);
    add_prop(OBJ_I_VALUE,300);
    ustaw_material(MATERIALY_DR_SOSNA);
    set_type(O_MEBLE);
    /*add_subloc("na",0,"z");
    add_subloc_prop("na", SUBLOC_I_TYP_W,10);
    make_me_sitable("na","na trumnie","z trumny",2);*/
    add_subloc("w",0,"z");
    add_subloc_prop("w", SUBLOC_I_TYP_W,10);
    make_me_sitable("w","w trumnie","z trumny",2);
}

public int
is_trumna_veli()
{
   return 1;
}
