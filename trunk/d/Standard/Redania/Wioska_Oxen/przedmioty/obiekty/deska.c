
/* By Veli 04.12.09
Kostnica wioska Oxen*/

#pragma strict_types

#include "dir.h"
#include <pl.h>
#include <stdproperties.h>
#include <macros.h>
#include <filter_funs.h>
#include <materialy.h>
#include <object_types.h>
inherit "/std/holdable_object.c";

void create_holdable_object()
{
    ustaw_nazwe("deska");
    dodaj_przym("oheblowany","oheblowani");
    dodaj_przym("sosnowy","sosnowi");
    set_long("Przyci^eta na d^lugo^s^c oko^lo metra oheblowana deska sosnowa"+
        " pachnie wci^a^z ^zywic^a i lasem. Z pewno^sci^a tak przygotowany"+
        " materia^l mo^zna bez problemu u^zy^c w pracach stolarskich.\n");
    add_prop(OBJ_I_WEIGHT, 3000);
    add_prop(OBJ_I_VOLUME, 5000);
    add_prop(OBJ_I_VALUE, 50);
    ustaw_material(MATERIALY_DR_SOSNA);
    set_type(O_DREWNO);
}

public int
is_deska_veli()
{
   return 1;
}
