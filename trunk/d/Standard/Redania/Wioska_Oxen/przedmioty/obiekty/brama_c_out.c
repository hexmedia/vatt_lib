
inherit "/std/gate.c";
#include <stdproperties.h>
#include <macros.h>
#include <mudtime.h>
#include "dir.h"

void
create_gate()
{
    set_other_room(WIOSKA_OXEN_LOKACJE+"cmentarz/c_1");
    set_open(1);
    set_locked(0);
    add_prop(GATE_IS_INSIDE,1);
    set_gate_id("cmentarz_buchta");
    set_pass_command("brama");
    godzina_zamkniecia(21);
    godzina_otwarcia(5);
    set_skad("wychodzi z cmentarzyska");
    dodaj_przym("pot^e^zny","pot^e^zni");
    dodaj_przym("stalowy","stalowi");
    set_long("@@dlugasny@@");
}

string dlugasny()
{
    string str;

    str = "Wysoka, dwuskrzyd^lowa brama starannie wykuta ze stali. Jedyn^a"+
        " ozdob^e stanowi du^ze, miedziane s^lonce, teraz ju^z zupe^lnie"+
        " zzielenia^lej, kt^orego promienie wykonano z kawa^lk^ow"+
        " po^lamanych ostrzy flamberg^ow.";

    str += opened_or_closed_desc();

    return str + "Skrzyd^lo pot^e^znej stalowej bramy jest uchylone.\n";
}