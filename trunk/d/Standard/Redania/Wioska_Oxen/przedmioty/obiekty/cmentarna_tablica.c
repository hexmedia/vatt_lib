
/* By veli 05.12.09
Tablica na murze cmentarzyska
Kod tabliczka_szpitalna by Avard 28.05.06 */

//#pragma strict_types

#include "dir.h"
#include <pl.h>
#include <stdproperties.h>
#include <macros.h>
#include <filter_funs.h>
#include <materialy.h>
#include <object_types.h>
inherit "/std/object.c";

string odczytaj_gryzmoly();

create_object()
{
    ustaw_nazwe("tabliczka");
    dodaj_przym("wmurowany","wmurowani");
    dodaj_przym("mosi^e^zny","mosi^e^zni");

    set_long("Niewielka mosi^e^zna tabliczka nosz^aca na sobie ^slady"+
        " rdzy przytwierdzona do muru tu^z ko^lo bramy wej^sciowej na"+
        " cmentarzysko. Niemal ca^l^a jej powierzchni^e pokrwaj^a jakie^s"+
        " trudne do odczytania gryzmo^ly, spod kt^orych wy^lania si^e ledwie"+
        " widoczny, wytrawiony kwasem napis:\n\"Otwarte codziennie\n\Od"+
        " ^switu do zmierzchu\"\n");
    add_item("gryzmo^ly na", "Z niema^lym trudem udaje ci si^e odcyfrowa^c"+
        " wymazane czarn^a farb^a gryzmo^ly zmieniaj^ace ogryginalny"+
        " napis - Cmentarzysko Jutrznia otwarte codziennie od ^switu do"+
        " zmierzchu - na: \n\"TEarzysko poJutrze.\n\ Otwarte"+
        " codziennie\n\ Od ^switu do zmierzchu.\"\n", PL_MIE);

    add_prop(OBJ_I_WEIGHT, 500);
    add_prop(OBJ_I_VOLUME, 1000);
    add_prop(OBJ_I_VALUE, 50);
    add_prop(OBJ_M_NO_SELL, 1);

    add_prop(OBJ_I_NO_GET, "Niestety tabliczka jest nadzwyczaj solidnie "+
        "wmurowana mi^edzy ceg^ly by^s mog^l"+this_player()->koncowka("","a")+
        " j^a sobie tak po prostu wzi^a^c.\n");
    ustaw_material(MATERIALY_MOSIADZ);
    set_type(O_INNE);
    
    add_cmd_item(({"gryzmo^ly","gryzmo^ly na tabliczce"}), "odczytaj",
        odczytaj_gryzmoly, "Odczytaj co?\n");

}

string
odczytaj_gryzmoly()
{
    string str = "Z niema^lym trudem udaje ci si^e odcyfrowa^c wymazane "+
        "czarn^a farb^a gryzmo^ly zmieniaj^ace ogryginalny napis - "+
        " - Cmentarzysko Jutrznia otwarte codziennie od ^switu do"+
        " zmierzchu - na: \n\"TEarzysko poJutrze.\n\ Otwarte"+
        " codziennie\n\ Od ^switu do zmierzchu.\"\n";

   return str;
}