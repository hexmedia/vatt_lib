
/* By Veli 04.12.09
Sarkofag w grobowcu - cmentarz Grabowa Buchta*/

#pragma strict_types

#include "dir.h"
#include <pl.h>
#include <stdproperties.h>
#include <macros.h>
#include <cmdparse.h>
#include <filter_funs.h>
#include <materialy.h>
#include <object_types.h>
inherit "/std/container";

string dlugi_opis();
//string dlugi_trup();

void create_container()
{
    ustaw_nazwe("sarkofag");
    dodaj_przym("masywny","masywni");
    dodaj_przym("ciemnoszary","ciemnoszarzy");
    set_long(&dlugi_opis());
    add_prop(OBJ_I_WEIGHT,17000);
    add_prop(OBJ_I_VOLUME,90000);
    add_prop(OBJ_I_VALUE,500);
    add_prop(OBJ_I_NO_GET, "Sarkofag wydaj^e si^e by^c bardzo ci^e^zki i"+
        " raczej na pewno na sta^le zespojony z piedesta^lem.\n");
    ustaw_material(MATERIALY_MARMUR);
    set_type(O_MEBLE);
    add_subloc("w",1,"w grobowcu");
    add_subloc_prop("w", SUBLOC_I_TYP_W, 1);
    make_me_sitable("na","na g^ladkim marmurowym sarkofagu","z sarkofagu",2);
    //clone_object(WIOSKA_OXEN_PRZEDMIOTY+"/obiekty/plyta_g.c");
    clone_object(WIOSKA_OXEN_PRZEDMIOTY+"/obiekty/cialo_ca.c")->move(this_object(),"w sarkofagu");
}

string
dlugi_opis()
{
    string str;

    str = "Ci^ete w marmurze kwietne wzory ozdabiaj^a sarkofag d^lugo^sci"+
        " doros^lego cz^lowieka. Pogrzeb musia^l odby^c si^e bardzo"+
        " niedawno, a napisy na p^lycie sa wyra^xne i wygladaj^a na ^swie^zo"+
        " wykute.";

   	/*if(this_object()->if_subloc_closed
    {
        str += " Ci^e^zka marmurowa p^lyta szczelnie zaslania wn^etrze"+
            " sarkofagu.\n";
    }
   	if(this_object()->if_subloc_not_closed
    {
        str += " Ci^e^zka marmurowa p^lyta stoi obok otwartego sarkofagu.\n";
    }*/

    return str;
}

public int
is_sarkofag_cmentarz()
{
   return 1;
}