
/* By Veli 12.12.09
Znicz grobowy cmentarz, Grabowa Buchta*/

#pragma save_binary
#pragma strict_types
#include <stdproperties.h>
#include <std.h>
#include <wa_types.h>
#include <macros.h>
#include <materialy.h>
#include <object_types.h>
inherit "/lib/light.c";
inherit "/std/torch.c";

string dlugi_opis();

void
create_torch()
{
    ustaw_nazwe("znicz");
    random_przym(({"okopcony", "okopceni", "p^ekni^ety", "p^ekni^eci",
        "^smierdz^acy", "smierdz^acy","szeroki", "szerocy", "nadpalony",
        "nadpaleni"}),({"niewielki","niewielcy", "skromny", "skromni",
        "zczernia^ly", "zczerniali", "gliniany", "gliniani", "tandetny",
        "tandetni"}), 2);
    set_long(&dlugi_opis());
    add_prop(OBJ_I_WEIGHT, 100);
    add_prop(OBJ_I_VOLUME, 90);
    add_prop(OBJ_I_VALUE, 20);
    ustaw_material(MATERIALY_GLINA);
    set_type(O_POCHODNIE);

    set_time(900+random(100));
    set_strength(2);
    set_value(13);
    set_time_left(900);

    config_light();
}

string
dlugi_opis()
{
    string str;

    str = "Niewielka gliniana miseczka wype^lniona mieszani^a zastyg^lego"+
        " ^loju i wosku, zabarwioną na czerwonawo koszelin^a z wetkni^etym"+
        " we^n konopnym sznurkiem.";

   	if(this_object()->query_prop(OBJ_I_LIGHT))
    {
        str += " Jarzy si^e w tej chwili niskim niebieskawym p^lomieniem,"+
            " daj^acymi jednak wi^ecej dymu ni^z ^swiat^la.\n";
    }
    else
    {
        str += " Obecnie jest zgaszony, jednak zosta^lo we^n jeszcze sporo"+
            " ^loju, wi^ec wygaszony knot mo^zna w ka^zdej chwili"+
            " podpali^c.\n";
    }

    return str;
}

public int
is_znicz_cmentarz()
{
   return 1;
}