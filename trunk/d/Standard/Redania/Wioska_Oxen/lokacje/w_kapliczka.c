
/*By Veli 17.12.09
Kapliczka wioska Oxen*/

#pragma strict_types

#include "dir.h"
#include "/d/Standard/Redania/dir.h"
#include <stdproperties.h>
#include <std.h>
#include <mudtime.h>
#include <pogoda.h>
#include <macros.h>
inherit WIOSKA_OXEN_WIOSKA_STD;

string dlugi_opis();

void
create_wioska()
{
    set_short("Kapliczka");
    set_long(&dlugi_opis());
    add_prop(ROOM_I_INSIDE, 1);
    add_item("pod^log^e","Kamienne ^sciany pobielone wapnem nosz^a na sobie"+
        " ^slady ple^sni i brudnych zaciek^ow.\n");
    add_item("lampy","Kilka prymitywnych, podwieszonych wysoko pod"+
        " sufitem lamp roz^swietla md^lym blaskiem obskurn^a kostnic^e."+
        " Spalaj^acy si^e w nich smierdz^acy zwierz^ecy ^l^oj skwierczy"+
        " cicho, jednak kopci si^e niezmiennie nie pozwalaj^ac sali"+
        " pogr^a^zy^c si^e w ciemno^sci.\n");
    add_sit("pod ^scian^a","pod ^scian^a","spod ^sciany",6);
    add_event("Deski na suficie trzeszcz^a cicho. Chyba kto^s chodzi po"+
        " strychu...\n");
    add_event("Kurz k^l^ebi si^e w md^lym ^swietle lampy.\n");
    add_event("P^lomie^n lampy zadrza^l nagle, po czym pochyli^l si^e w"+
        " jedn^a stron^e, zupe^lnie tak, jakby kto^s przeszed^l obok.\n");
    add_exit(WIOSKA_OXEN_LOKACJE + "w_6",({"^scie^zka","na ^scie^zk^e",
        "z kapliczki"}));
}

string 
dlugi_opis()
{
    string str;

    str = "LONG";

    if (jest_dzien() == 1)

    {
        str += "^Swiat^lo dnia zdaje si^e ^saczy^c leniwie do wn^etrza.\n";
    }
    if (jest_dzien() == 0)
    {
        str += "Ksi^e^zycowa po^swiata zdaje si^e s^aczy^c leniwie do"+
            " wn^etrza.\n";
    }
 
    return str;
}

public string
exits_description() 
{
    return "Zauwa^zasz jedynie w^askie wyj^scie, sklepione grub^a"+
        " rze^xbion^a belk^a.\n";
}
