
/*By Veli 17.12.09
Nabrzeze kolo karczmy - wioska Oxen*/

#pragma strict_types
#include "dir.h"
#include "/d/Standard/Redania/dir.h"
#include <stdproperties.h>
#include <mudtime.h>
#include <pogoda.h>
#include <files.h>
#include <macros.h>
#include <filter_funs.h>
inherit WIOSKA_OXEN_WIOSKA_STD;

//string krotki_opis();
string dlugi_opis();
string dlugi_wzgorze();
string event_wioska();
string pora_roku_pogoda();
string pora_roku_dzien();
string pora_roku_noc();
string event_wioska();

void
create_wioska()
{
    set_short("Nabrzeze kolo karczmy");
    //set_short(&krotki_opis());
    set_long(&dlugi_opis());
    add_prop(ROOM_I_INSIDE, 0);
    /*add_object(WIOSKA_OXEN_PRZEDMIOTY+"obiekty/cmentarna_tablica.c",1);
    add_object(WIOSKA_OXEN_PRZEDMIOTY+"obiekty/brama_c.c",1);
    add_item("placyk","Niewielki placyk okolony g^estym ^l^egowym zagajnikiem"+
        " przed bram^a cmentarn^a wysypano drobnymi otoczakami, kt^ore"+
        " skrzypi^ac przy ka^zdym twoim kroku przywodz^a na my^sl ziarna"+
        " piasku w klepsydrze, kt^ore bezg^lo^snie przesypuj^ac si^e w do^l"+
        " odliczaj^a czas jaki jeszcze pozosta^l, nim znajdziemy si^e po"+
        " drugiej stronie - w za^swiatach.\n");*/
    add_item("wzg^orze",&dlugi_wzgorze());
    add_event(&event_wioska());
    add_sit("pod murem","pod murem","spod muru",6);
    add_exit(WIOSKA_OXEN_LOKACJE+"w_4","ne");
    add_exit(WIOSKA_OXEN_LOKACJE+"w_karczma","karczma");
    add_exit(WIOSKA_OXEN_LOKACJE+"w_2","e");
    add_exit(WIOSKA_OXEN_LOKACJE+"w_5","n");
}

/*string
krotki_opis()
{

    string str;

    if (jest_dzien() == 1)
    {
        str = "Placyk przy bramie\n.";
    }

    if (jest_dzien() == 0)
    {
        str = "Pod zamkni^et^a bram^a\n";
    }

    return str;

}*/

string
dlugi_opis()
{
    string str;

    str = "LONG";
    
    return str;
}

string
dlugi_wzgorze()
{
    string str;

    str = "Wzg^orze, na kt^orym stoisz to zarazem najwy^zej po^lo^zone"+
        " miejsce na ca^lej, ulokowanej w delcie Pontaru wysepki."+
        " Chocia^z samo wzniesienie nie nale^zy mo^ze do zbyt okaza^lych"+
        " to i tak roztarcza si^e st^ad otwarty widok na szeroki i"+
        " nadzwyczaj leniwy bieg";

        if (pora_roku() == MT_WIOSNA)
    {
        str += " wezbra^lej po zimie rzeki, zazielenione, poro^sni^ete"+
            " sitowiem brzegi, trz^esawiska oraz budz^ac^a si^e do ^zycia"+
            " male^nk^a wioseczk^e zwan^a Grabowa Buchta, majacz^ac^a"+
            " gdzie^s u podn^o^za, po^sr^od ^l^eg^ow.\n";
    }

        if (pora_roku() == MT_LATO)
    {
        str += " kipi^acej z gor^aca i niemi^losiernie ^smierdz^acej w upale"+
            " rzeki, w kt^orej wodach rozk^ladaj^a si^e teraz wszelakie"+
            " mo^zliwe ^smieci i fekalia z dw^och wielkich miast - "+
            " Oxenfurtu i Novigradu.\n";//DO ZMIANY!!!!!!!!!!
    }

        if (pora_roku() == MT_JESIEN)
    {
        str += " sp^lywaj^acej spiesznie ku morzu rzeki, poz^locone"+
            " jesiennymi li^scmi ^l^egowe zagajniki oraz przycupni^et^a u"+
            " podn^o^za wiosk^e, zwan^a Grabowa Buchta. W powietrzu czu^c"+
            " ju^z wyra^xnie butwiej^ac^a roslinno^s^c oraz zapach palonego"+
            " w piecach w osadzie, drewna.\n";
    }

        if (pora_roku() == MT_ZIMA)
    {
        str += " paruj^acej w ch^lodnym, zimowym powietrzu rzeki oraz"+
            " przypruszone ^sniegiem i poro^sni^ete zesch^lym sitowiem"+
            " oraz bezlistnymi ^legowymi zagajnikami brzegi. Z komin^ow"+
            " przycupni^etej u podn^o^za wioseczki zwanej Grabow^a Bucht^a"+
            " unosz^a si^e bia^lawe dymy o czystym drzewnym zapachu"+
            " spowijaj^ac okolic^e niby mg^la.\n";
    }

    return str;
}

public string
exits_description() 
{
    string str;

    str =  "Nabrze^ze ci^agnie si^e dalej na wsch^od, prowadz^ac w stron^e"+
        " magazynu rybackiego, stoj^acego podobnie jak karczma, do kt^orej"+
        " mo^zesz st^ad wej^s^c, cz^e^sciowo na brzegu, a cz^esciowo na"+
        " grubych, zanurzonych w Pontarze belkach. Na p^olnocnym wschodzie"+
        " rozci^aga si^e niewielki placyk, kt^ory najpewniej jest te^z"+
        " wioskowym targowiskiem, za^s na p^o^lnoc, mi^edzy chatami idzie"+
        " si^e w stron^e wzgorza.\n"; 

    return str;
}

public int
unq_no_move(string str)
{
    notify_fail("Chyba nie chcesz skonczyc w Pontarze co?\n");

    string vrb = query_verb();

    if(vrb ~= "g^ora" || vrb ~= "d^o^l")
        notify_fail("Nie mo^zesz tam si^e uda^c!\n");

    return 0;
}

public int
unq_move(string str)
{

    ::unq_move();

    string move_cmd = query_verb();

    if (str == "karczma")
    {
        str = "Wlazisz do karczmy\n";
        saybb(QCIMIE(TP, PL_MIA) + " wlazi do karczmy.\n");
    }

    if (str == "n")
    {
        str = "Idziesz wglab wiochy.\n";
        saybb(QCIMIE(TP, PL_MIA) + " idzie wglab wiochy.\n");
    }

    if (str == "e")
    {
        str = "Idziesz sobie nabrzezem.\n";
        saybb(QCIMIE(TP, PL_MIA) + " idzie sobie dalej nabrzezem.\n");
    }

    if (str == "ne")
    {
        str = "Idziesz sobie na targ.\n";
        saybb(QCIMIE(TP, PL_MIA) + " idzie sobie na targ.\n");
    }


    /*add_exit(WIOSKA_OXEN_LOKACJE+"w_4",({"ne","wg^l^ab wioski","na plac"+
        " targowy","z nabrze^za"}));
    add_exit(WIOSKA_OXEN_LOKACJE+"w_karczma",({"karczma","do gwarnej"+
        " karczmy","z nabrze^za do ^srodka"}));
    add_exit(WIOSKA_OXEN_LOKACJE+"w_2",({"e","dalej nabrze^zem",
        "nabrze^zem"}));
    add_exit(WIOSKA_OXEN_LOKACJE+"w_5",({"n","wg^l^ab wioski","ku"+
        " wzniesieniu","z nabrze^za"}));*/

    return 1;
}

string
event_wioska()
{

    switch (MT_PORA_DNIA)
    {
        case MT_SWIT:
            return process_string(({"Mg^ly k^l^ebi^a si^e nad ciemn^a"+
            " powierzchni^a rzeki.\n","@@pora_roku_noc@@",
            "@@pora_roku_pogoda@@"})[random(3)]);
        case MT_RANEK:
        case MT_POLUDNIE:
        case MT_POPOLUDNIE:
            return process_string(({"Rzeka smierdzi.\n","@@pora_roku_dzien@@",
            "@@pora_roku_pogoda@@"})[random(3)]);
        case MT_WIECZOR:
            return process_string(({"Mg^ly k^l^ebi^a si^e nad ciemn^a"+
            " powierzchni^a rzeki.\n","@@pora_roku_dzien@@",
            "@@pora_roku_pogoda@@"})[random(3)]);
        default: // noc
            return process_string(({"Smierdzi, smierdzi strasznie noca.\n",
            "@@pora_roku_noc@@","@@pora_roku_pogoda@@"})[random(3)]);
    }
}
string
pora_roku_dzien()
{
    switch (MT_PORA_ROKU)
    {
         case MT_LATO:
         case MT_WIOSNA:
             return ({"Rzeka smierdzi zajebiscie mocno.\n"})[random(1)];
         case MT_JESIEN:
             return ({"Rzeka cuchnie jak nie wiem co.\n","Rzeka potwornie"+
             " smierdzi zgnilizna.\n","Rzeka strzasznie smierdzi!\n"})[random(3)];
         case MT_ZIMA:
             return ({"Rzeka zamarzla.\n","Rzeka zamarzla srogo.\n"})[random(2)];
    }
}
string
pora_roku_noc()
{
    switch (MT_PORA_ROKU)
    {
         case MT_LATO:
         case MT_WIOSNA:
             return ({"Smierdzi smierdzi strasznie smierdzi.\n"})[random(1)];
         case MT_JESIEN:
             return ({"Rzeka smierdzi liscmi.\n","Rzeka gnije.\n","Rzeka"+
             " gnije jak cholera.\n"})[random(3)];
         case MT_ZIMA:
             return ({"Rzeka zamarzla i smierdzi.\n","Rzeka srogo zamiarzla"+
             " i smierdzi.\n","Lod lod lod lod smierdzi.\n"})[random(3)];
    }
}

string
pora_roku_pogoda()
{
    switch(POGODA_OPADY)
    {
    case POG_OP_D_L:
    case POG_OP_D_S:        
        return ({"Leje.\n"})[random(1)];
    case POG_OP_S_S:
    case POG_OP_S_L:
    case POG_OP_S_C:
        return ({"Snieg pada.\n"})[random(1)];
    case POG_OP_G_S:
        return ({"Grad pada\n"})[random(1)];
    }

    switch (POGODA_WIATR)
    {
    case POG_ZACH_ZEROWE:
    case POG_ZACH_LEKKIE:
    case POG_ZACH_DUZE:
    case POG_ZACH_CALKOWITE:
        return ({"Chmury duze jak cholera.\n"})[random(1)];        
    }
}