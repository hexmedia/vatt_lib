
/*By Veli 04.12.09
Kostnica wioska Oxen*/

//#pragma strict_types

#include "dir.h"
#include "/d/Standard/Redania/dir.h"
#include <stdproperties.h>
#include <std.h>
#include <mudtime.h>
#include <pogoda.h>
#include <macros.h>
inherit WIOSKA_OXEN_CMENTARZ_STD;

string dlugi_opis();
int il_desek; //zlicza deski
int il_trumien; //zlicza trumny

void
create_cmentarz()
{
    set_short("Zat^ech^la kostnica");
    set_long(&dlugi_opis());
    add_prop(ROOM_I_INSIDE, 1);
    add_object(WIOSKA_OXEN_PRZEDMIOTY+"obiekty/stol_metal.c",2);
    add_object(WIOSKA_OXEN_PRZEDMIOTY+"obiekty/trumna.c",4);
    add_object(WIOSKA_OXEN_PRZEDMIOTY+"obiekty/deska.c",6);
    dodaj_rzecz_niewyswietlana("d^luga sosnowa trumna",4);
    dodaj_rzecz_niewyswietlana("zimny stalowy st^o^l",2);
    dodaj_rzecz_niewyswietlana("oheblowana sosnowa deska",6);
    add_item("pod^log^e","Kamienne ^sciany pobielone wapnem nosz^a na sobie"+
        " ^slady ple^sni i brudnych zaciek^ow.\n");
    add_item("lampy","Kilka prymitywnych, podwieszonych wysoko pod"+
        " sufitem lamp roz^swietla md^lym blaskiem obskurn^a kostnic^e."+
        " Spalaj^acy si^e w nich smierdz^acy zwierz^ecy ^l^oj skwierczy"+
        " cicho, jednak kopci si^e niezmiennie nie pozwalaj^ac sali"+
        " pogr^a^zy^c si^e w ciemno^sci.\n");
    add_sit("pod ^scian^a","pod ^scian^a","spod ^sciany",6);
    add_event("Deski na suficie trzeszcz^a cicho. Chyba kto^s chodzi po"+
        " strychu...\n");
    add_event("Kurz k^l^ebi si^e w md^lym ^swietle lampy.\n");
    add_event("P^lomie^n lampy zadrza^l nagle, po czym pochyli^l si^e w"+
        " jedn^a stron^e, zupe^lnie tak, jakby kto^s przeszed^l obok.\n");
    add_exit(WIOSKA_OXEN_LOKACJE + "cmentarz/c_2",({"drzwi","drzwiami na"+
        " zewn^atrz","z budynku"}));
    add_exit(WIOSKA_OXEN_LOKACJE + "cmentarz/c_strych",({"stryszek","po"+
        " drabinie na strych","z do^lu"}));
}

string 
dlugi_opis()
{
    string str;

    str = "Pomieszczenie to z pewno^sci^a s^lu^zy do przechowywania oraz"+
        " przygotowywania zw^lok przed poch^owkiem. Nagie, pobielone wapnem"+
        " ^sciany nosz^a na sobie ^slady ple^sni, zupelnie jakby wn^etrze nie"+
        " by^lo zbyt cz^esto wietrzone. Calkiem pozbawiona okien izba tonie"+
        " w p^o^lmroku roz^swietlanym kopc^acym si^e w lampach ^lojem.";

int d, id, il_desek;
object *inwentarz;
inwentarz=all_inventory();
il_desek=0;
id=sizeof(inwentarz);
for (d = 0; d < id; ++d)
{
     if (inwentarz[d]->is_deska_veli())
     {
     ++il_desek;
     }
}
  
    if(il_desek == 0)
{
    str += " Nieopodal zauwazasz pozosta^lo^s^ci po deskach s^lu^zacych"+
        " najpewniej do wyrobu trumien."; 
}
    if(il_desek == 1)
{
    str += " Nieopodal, w k^acie le^zy sosnowa deska";
}
    if(il_desek == 2)
{
    str += " Nieopodal, w k^acie le^z^a dwie sosnowe deski";
}
    if(il_desek == 3)
{
    str += " Nieopodal zauwa^zasz ma^l^a stert^e sosnowych desek";
}
    if(il_desek == 4)
{
    str += " Nieopodal zauwa^zasz ma^l^a stert^e sosnowych desek";
}
    if(il_desek >= 5)
{
    str += " Nieopodal zauwa^zasz spor^a stert^e sosnowych desek";
}

    str += " oraz";

int t, it, il_trumien;
il_trumien=0;
it=sizeof(inwentarz);
for (t = 0; t < it; ++t)
{
     if (inwentarz[t]->is_trumna_veli())
     {
     ++il_trumien;
     }
}
  
    if(il_trumien == 0)

    {
    str += "woln^a przestrze^n, gdzie zwykle stoj^a gotowe trumny"; 
    }

    if(il_trumien == 1)

    {
    str += " zamkniet^a, ju^z gotow^a trumn^e";
    }

    if(il_trumien == 2)
    {
    str += " dwie zamkni^ete, ju^z gotowe trumny";
    }

    if(il_trumien == 3)

    {
    str += " trzy zamkni^ete, ju^z gotowe trumny";
    }

    if(il_trumien >= 4)

    {
    str += " kilka zamkni^etych, ju^z gotowych trumien";
    }

    if (present ("martwe cia^lo", inwentarz) == 0)

    {
        str += ", za^s na sto^le le^z^a owini^ete w ca^lun zw^loki"; 
    }

    if (present ("martwe cia^lo", inwentarz) == 1)

    {
        str += ", za^s na sto^le nie le^z^a w tej chwili ^zadne zw^loki";
    }

        str += ", a roznosz^acy si^e wok^o^l md^ly od^or palonego t^luszczu"+
            " po^l^aczony ze s^lodkaw^a nut^a wysuszonej sk^ory niemal"+
            " osadza si^e na tobie, zdaje si^e dociera^c w najg^l^ebsze"+
            " zakamarki twojego cia^la.";

    if (jest_dzien() == 1)

    {
        str += "^Swiat^lo dnia zdaje si^e ^saczy^c leniwie do wn^etrza.\n";
    }
    if (jest_dzien() == 0)
    {
        str += "Ksi^e^zycowa po^swiata zdaje si^e s^aczy^c leniwie do"+
            " wn^etrza.\n";
    }
 
    return str;
}

public string
exits_description() 
{
    return "Tu^z obok drzwi wyj^sciowych stoi drewniana drabina"+
        " prowadz^aca gdzie^a na stryszek, zapewne do mieszkania grabarza.\n";
}
