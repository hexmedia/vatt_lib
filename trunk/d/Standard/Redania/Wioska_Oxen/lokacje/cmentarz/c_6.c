
/*By Veli 08.12.09
Wejscie do cmentarnych katakumb - wioska Oxen*/

#pragma strict_types
#include "dir.h"
#include "/d/Standard/Redania/dir.h"
#include <stdproperties.h>
#include <std.h>
#include <mudtime.h>
#include <pogoda.h>
#include <macros.h>
inherit WIOSKA_OXEN_CMENTARZ_STD;

//string krotki_opis();
string dlugi_opis();
//string rozkop_grob(); z tym duuuzo roboty
string dlugi_mogily();
//string dlugi_male_mogily();
string dlugi_trawy();
string dlugi_dol();

void
create_cmentarz()
{
    set_short("Po^sr^od zaro^sni^etych mogi^l");
    //set_short(&krotki_opis());
    set_long(&dlugi_opis());
    add_prop(ROOM_I_INSIDE, 0);
    /*add_object(); subloc D^o^l!
    dodaj_rzecz_niewyswietlana("",1); - moze tu cos bedzie.*/
    add_item("mogi^ly",&dlugi_mogily());
    //add_item("malutkie mogi^ly",&dlugi_male_mogily());
    add_item("trawy",&dlugi_trawy());
    add_item("d^o^l",&dlugi_dol());
    add_item("kolumn^e","Z^lamana, smuk^la kolumna odcina si^e biel^a"+
        " marmuru od otaczaj^acego wzniesienie cmentarne muru. Wszystkie"+
        " groby zdaj^a si^e by^c skierowane w jej stron^e, zupe^lnie jakby"+
        " kolumna stanowi^la swoiste centrum tego miejsca.\n");
    add_sit("w^sr^od traw","po^sr^od wysokich","z traw",6);
    add_event("Mdl^acy zapach dzikiego bzu unosi si^e w powietrzu.\n");//WIOSNA
    add_event("Wiatr porusza ^lanami traw, wprawiaj^ac je w cichy"+
        " szelest.\n");
    add_event("Gar^s^c ziemi zsun^e^la si^e do otwartego do^lu.\n");
    add_event("Nadeptujesz na skryt^a w trawach mogi^l^e, a ziemia zapada"+
        " si^e lekko pod twymi stopami.\n"); //DESZCZ
    add_event("Wok^o^l roznosi si^e charakterystyczna wo^n wilgotnej ziemi,"+
        " po^l^aczona z czym^s jeszcze...\n");//DESZCZ
    add_event("G^luchy odg^los zblizaj^acej si^e burzy przetoczy^l sie nad"+
        " wzg^orzem.\n");//DZESZCZ
    add_event("Obci^a^zone nadmiarem wody trawy uk^ladaj^a si^e p^lasko na"+
        " mogi^lach.\n");//DESZCZ
    add_event("Nagle cisz^e przerywa przeci^ag^le westchnienie. Co^z, pewnie"+
        " to tylko wiatr...\n");//NOC
    add_event("Mg^ly zasnuwaj^a okolic^e wilgotnym, szarawym p^laszczem.\n");//NOC
    add_event("Nik^ly sw^ad palonych ^swiec miesza si^e z woni^a ^swie^zo"+
        " rozkopanej ziemi.\n");//NOC
    add_exit(WIOSKA_OXEN_LOKACJE+"cmentarz/c_4",({"nw","cos","cos"}));
    add_exit(WIOSKA_OXEN_LOKACJE+"cmentarz/c_5",({"sw","cos","cos"}));
    add_exit(WIOSKA_OXEN_LOKACJE+"cmentarz/c_7",({"s","cos","cos"}));
    //wyjscia zale^zne od warunk^ow - patrz na ko^ncu
    //add_cmd_item("gr^ob", "rozkop", rozkop_grob, "Rozkop co?\n");
}

/*string 
krotki_opis()
{

    string str;

    if (jest_dzien() == 1)
    {
        str = "Po^sr^od zaro^sni^etych mogi^l\n.";
    }

    if (jest_dzien() == 0)
    {
        str = "Po^sr^od cichych mogi^l\n";
    }

    return str;

}*/

string 
dlugi_opis()
{
    string str;

    if (jest_dzien() == 1)
    {

        str = "Docierasz do cz^e^sci cmentarza niew^atpliwie przeznaczonej"+
            " dla ubo^zszej warstwy spo^lecznej. Wyrastaj^ace spo^sr^od"+
            " szumi^acych traw mogi^ly, jedna tu^z obok drugiej to"+
            " najzwyczajniejsze w ^swiecie kopczyki, niekt^ore prawie"+
            " zupe^lnie zr^ownane z ziemi^a, a niekt^ore jeszcze ca^lkiem"+
            " ^swie^ze. Cho^c s^a pozbawione jakichkolwiek ozd^ob, to"+
            " gdzieniegdzie le^z^z miedziane tabliczki upamietniaj^ace,"+
            " tych, kt^orzy odeszli. Niecodziennym mo^ze wyda^c si^e fakt,"+
            " i^z du^za cz^e^s^c mogi^l nosi na sobie ^slady rozkopywania,"+
            " i to bynajmniej nie przez krety! Niedaleko od miejsca, w"+
            " kt^orym jeste^s, za grup^a malutkich mogi^l wykopano do^l, a"+
            " jego otwarte czelu^sci oczekuj^a na czyje^s przybycie..."+
            " Ca^lo^s^c spowija cie^n muru, do kt^orego przylega ^zalnik."+
            " Wzd^lu^z jego ceglanej sciany ci^agnie si^e w^aska ^scie^zka"+
            " wydeptana w stron^e uschni^etego drzewa majacz^acego na"+
            " po^ludniu.\n";
    }

    if (jest_dzien() == 0)
    {
        str = " St^apasz po mi^ekkim morzu wysokich, szumi^acych traw,"+
            " kt^ore z ka^zdym twym krokiem zdaj^a si^e jeszcze bardziej"+
            " sk^lania^c ku ziemi. W ciemno^sci nocy i g^estej mgle"+
            " spowijaj^acej cmentarz nie jeste^s w stanie dostrzec"+
            " jakichkolwiek detali czy te^z nawet mogi^l, jednak^ze"+
            " stawiaj^ac nieostro^zny krok, potykasz si^e o ^swie^ze,"+
            " jeszcze nie osiad^le groby, a stopy co rusz zapadaj^ace si^e"+
            " w miekk^e ziemi^e powinny rozwik^la^c wszelkie w^atpliwo^sci"+
            " co do natury miejsca, w kt^orym si^e znajdujesz. Niezwyk^la"+
            " cisza zdaje si^e k^l^ebic wraz z mleczn^a zas^lon^a mg^ly,"+
            " pot^eguj^ac poczucie zagubienia i niepewno^sci. Nieco na"+
            " uboczu zauwa^zasz kilka malutkich mogi^l rozja^snionych ^lun^a"+
            " ^swiec p^lon^acych nieopodal drzewa na po^ludniu.\n";
    }

    return str;
}

string
dlugi_trawy()
{

    string str; 

    if (pora_roku() == MT_WIOSNA)
    {
        str = "^Lany zazielenionych traw porastaj^a ca^l^a wschodni^a"+
            " cz^e^s^c cmentarza. Od^zywione i nasi^akni^ete wilgoci^a"+
            " wygladaj^a zupe^lnie niczym szmaragdowe ostrza, w^sr^od"+
            " kt^orych, nieco bardziej na po^ludniu wyrasta samotne,"+
            " umieraj^ace drzewo./n";
    }

        if (pora_roku() == MT_LATO)
    {
        str = "^Lany zazielenionych traw porastaj^a ca^l^a wschodni^a"+
            " cz^e^s^c cmentarza. Od^zywione i nasi^akni^ete wilgoci^a"+
            " wygladaj^a zupe^lnie niczym szmaragdowe ostrza, w^sr^od"+
            " kt^orych, nieco bardziej na po^ludniu wyrasta samotne,"+
            " umieraj^ace drzewo./n";
    }

            if (pora_roku() == MT_JESIEN)
    {
        str = "^Lany wysuszonych przez ch^l^od i s^lo^nce wysokich traw"+
            " porastaj^a ca^l^a wschodni^a cz^e^s^c cmentarza. O tej porze"+
            " wygladaj^a zupe^lnie niczym szare morze ostrych igie^l,"+
            " w^sr^od kt^orych, nieco bardziej na po^ludniu wyrasta samotne,"+
            " wygladaj^ace st^ad na martwe, drzewo.\n";
    }

            if (pora_roku() == MT_ZIMA)
    {
        str = "^Lany wysuszonych przez ch^l^od i s^lo^nce wysokich traw"+
            " porastaj^a ca^l^a wschodni^a cz^e^s^c cmentarza. O tej porze"+
            " wygladaj^a zupe^lnie niczym szare morze ostrych igie^l,"+
            " w^sr^od kt^orych, nieco bardziej na po^ludniu wyrasta samotne,"+
            " wygladaj^ace st^ad na martwe, drzewo.\n";
    }

    return str;
}

string
dlugi_dol()
{
    string str;

    if (jest_dzien() == 1)
    {
        str = "^Swie^zo wykopany d^o^l, g^l^eboki na oko^lo dwa metry, z"+
            " pewno^scia oczekuje na jakiego^s pechowego w^edrowca.\n";  
    }

    if (jest_dzien() == 0)
    {
        str = "";
    }

    return str;
}


string
dlugi_mogily()
{
    string str;

    if (jest_dzien() == 1)
    {
        str = "Ciche ziemne kopce rozsiane s^a po ca^lej przestrzeni,"+
            " skrywaj^ac w swych wn^etrzach szcz^atki ludzi, elf^ow,"+
            " miesza^nc^ow, bad^x krasnolud^ow. Bezsprzecznie stwierdzasz,"+
            " ^ze cz^e^s^c z nich by^la rozkopana i niezbyt dok^ladnie"+
            " zasypana na nowo. Mo^ze to oznacza^c kilka rzeczy, nad"+
            " kt^orymi chyba bezpieczniej b^edzie si^e nie zastanawia^c...\n";  
    }

    if (jest_dzien() == 0)
    {
        str = "Jest zbyt ciemno, by zauwa^zy^c jakiekolwiek groby, jednak"+
            " na po^ludniu majacz^a kszta^lty malutkich mogi^l nieznacznie"+
            " pod^swietlonych ^lun^a ^swiec p^lon^acych nieopodal drzewa."+
            " Mo^ze to nik^le swiat^lo wystarczy, by dok^ladniej si^e im"+
            " przyjrze^c.\n";
    }

    return str;
}


/*string
dlugi__male_mogily()
{
    string str;

    if (jest_dzien() == 1)
    {
        str = "S^adz^ac po rozmiarach kopczykow pochowano w nich dzieci."+
            " Ka^zda z grupy czterech mogi^l nosi ^slady ekshumacji."+
            " Zastanawiaj^acym mo^ze by^c to, po c^o^z ktokolwiek mia^lby"+
            " rozkopa^c gr^ob dziecka, a jeszcze bardziej intryguje inny"+
            " fakt... Czy cia^lo ci^agle le^zy w miejscu, w kt^orym"+
            " powinno?\n";  
    }

if (jest_dzien() == 0)
    {

ob malutkie mogily/NOC/WPADANIE DO DOLU/ (random zalezny od sposta)
osoba 1.
Starasz sie wytezyc wzrok jak tylko mozesz, lecz niestety wciaz widzisz jedynie kontury. Pewnie lepiej bedzie podejsc blizej, stawiasz wiec krok naprzod...
op 2 sek
Oho! Zza zaslony ciemnosci zaczyna wyzierac coraz wiecej i wiecej! Przenosisz ciezar ciala nieco w przod, jednak zbyt pozno zauwazasz, ze spod twej stopy osuwa sie troche ziemi... Tylko gdzie? Nagle w ulamku sekundy, nie widzac kompletnie nic, czujesz uderzenie czegos twardego o siedzenie. Nasilona won ziemi i wilgoc po kilkakroc wieksza, niz wczesniej sugeruja ci tylko jedno miejsce, w ktorym mozesz sie znajdowac... Ajajaj, nie lepiej bylo bardziej uwazac..?
osoba 3. na zewnatrz
* zaczyna sie czemus przygladac coraz bardziej intensywnie i robi krok naprzod...
op 2 sek
* wpada do grobu!
osoba 3. w srodku
* nagle laduje tuz obok ciebie!

ob malutkie mogily/NOC/PROBA UDANA/
Starasz sie wytezyc wzrok jak tylko mozesz, lecz niestety wciaz widzisz jedynie kontury. Pewnie lepiej bedzie podejsc blizej, stawiasz wiec krok naprzod...
op 2 sek
Rozsadnie omijajac dol podchodzisz nieco blizej. Choc wciaz niewyraznie, to teraz jestes w stanie dostrzec, iz kazda z mogil nosi na sobie slady ekshumacji. Jedna z nich zdaje sie szczegolnie niepokoic, bo to wlasnie przy niej zauwazasz jakas tabliczke. Niestety, ciemnosc nie pozwala na odczytanie slow, jakie nan wyryto, moze warto by bylo sprobowac za dnia?

    }

    return str;
}*/

/*string
rozkop_grob()
{
    string str;

    str = "\n";

    saybb(QCIMIE(TP, PL_MIA) + " \n");

    return str;

Mozna rozkopac grob - tylko w nocy i tylko jesli posiada sie lopate
rozkop grob
Chyba nie chcesz tego robic w bialy dzien?

rozkop grob (postac nie posiada lopaty)
Mozna to wprawdzie robic golymi rekami, ale chyba grzebanie w ziemi oraz szczatkach nie nalezy do najwiekszych przyjemnosci...

rozkop grob (postac posiada lopate, jest noc )
osoba 1.
Upewniwszy sie, iz nikogo niepozadanego nie ma w poblizu zaglebiasz lopate w ziemie i zaczynasz kopac. 
op 2 sek
Wciaz kopiesz...
op 3 sek
Kopiesz... Kopiesz... 
op 2 sek
Nagle zauwazasz, ze wykopales/wykopalas dosc pokazny kawal ziemi nie natrafiajac zupelnie na nic! Gdzie podzialy sie zwloki? Moze sa glebiej..? Porzucasz jednak te bezsensowna mysl, gdy tylko twoja dlon natrafia na cos bardzo malego i zarazem miekkiego... Przejmujacy dreszcz grozy paralizuje na krotka chwile twoje cialo, gdy podnosisz tajemniczy przedmiot ku mdlemu swiatlu ksiezyca...
op 3 sek
Okrzyk wieznie ci w gardle, albowiem ow przedmiot okazuje sie byc odcietym, na wpol zgnilym ludzkim palcem, w dodatku tak malym, ze zapewne nalezacym do noworodka! Czym predzej odrzucasz odrazajaca wskazowke jak najdalej od siebie, jednak zaraz potem niby grad spadaja pytania - kto, a przede wszystkim po co mialby wykradac niemowlece ciala i ciac je na kawalki?
osoba 3.
* rozejrzywszy sie uwaznie wokol zaglebia lopate w ziemi i zaczyna kopac. 
op 2 sek
* wciaz zawziecie kopie...
op 3 sek
* ciagle kopie, ale chyba nie znajduje tego, czego szuka...
op 2 sek
* nagle spoglada na dosc pokazych rozmiarow kopiec, a nastepnie na dol. Chyba ktos sie nieco zapomnial... Jednak chwile potem * zdaje sie podnosic cos w gore, by nastepnie ze zgroza to odrzucic. Ciekawe, co takiego tam znalazl/znalazla...


}*/


public string
exits_description() 
{
    return "Mi^edzy g^estymi krzakami zauwa^zasz n^edzne zarysy ^scie^zki"+
        " wiod^acej g^l^ebiej w zaro^sla na p^o^lnocnym zachodzie oraz pod"+
        " murem na po^ludnie. Dodatkowo mo^zesz te^z przej^s^c mi^edzy"+
        " grobami na po^ludniowy zach^od, w stron^e smuk^lej kolumny.\n";
}

/*[specjalne]

zmiana por dnia
zachod slonca
Ostatnie okruchy jasno^sci dnia nikna wraz z zachodz^acym s^lo^ncem, a cmentarzysko nie^spiesznie spowija g^esty cie^n. 
wschod slonca
Pierwsze promienie s^lo^nca przecinaj^a ciemnogranatowe nocne niebo i powoli roz^swietlaja okolice.

OPIS NOCNY
Wyjscia:
w jakimkolwiek kierunku - Znikasz w cieniu.
* znika w cieniu.
* wylania sie z cienia.

nw-c4 - Przeciskasz sie przez rzadsze zarosla.
* przeciska sie przez rzadsze zarosla.
* wylania sie z zarosli.
sw-c5 - Podazasz w strone smuklej kolumny.
* podaza w strone smuklej kolumny.
* przybywa od strony smuklej kolumny.
s-c7 - Podazasz waska sciezka pod murem, kierujac sie w strone samotnego drzewa.
* podaza waska sciezka pod murem, kierujac sie w strone samotnego drzewa.
* nadchodzi waska sciezka pod murem.


[zdarzenia]
*Dochodzi cie ciche skrobanie o drewno.
*Wiatr porusza lanami traw, wprawiajac je w cichy szelest.
*Garsc ziemi zsunela sie do otwartego dolu.
*Nadeptujesz na skryta w trawach mogile, a ziemia zapada sie lekko pod twymi stopami.

Zdarzenia deszczowe:
- Wokol roznosi sie charakterystyczna won wilgotnej ziemi, polaczona z czyms jeszcze...
- Gluchy odglos zblizajacej sie burzy przetoczyl sie nad miastem.
- Obciazone nadmiarem wody trawy ukladaja sie plasko na mogilach.


Zdarzenia nocne:
- Dochodzi cie ciche skrobanie o drewno.
- Wiatr porusza lanami traw, wprawiajac je w cichy szelest.
- Nadeptujesz na skryta w trawach mogile, a ziemia zapada sie lekko pod twymi stopami.
- Nagle cisze przerywa przeciagle westchnienie. Coz, pewnie to tylko wiatr...
- Mgly zasnuwaja okolice wilgotnym, szarawym plaszczem.
- Nikly swad palonych swiec miesza sie z wonia swiezo rozkopanej ziemi.


|#D
short: W grobie
long: Ponure sciany ziemi wyrastajace wokol ciebie zamykaja cie w klatce, a fakt, iz nawet one sa zasloniete przez ciemnosc tylko poteguje klaustrofobiczne wrazenie. Jedyne swiatlo dociera do ciebie z otworu na gorze i z pewnoscia mozna sie stad wydostac, choc niekoniecznie musi byc to tak proste, jak sie wydaje, znajdujesz sie bowiem w otwartym grobie glebokim na nieco ponad dwa metry. Dol ten najpewniej stanie sie w niedlugim czasie miejscem wiecznego spoczynku jakiegos wyjatkowego pechowca. Coz, doswiadczenie to pewnie nie nalezy do najprzyjemniejszych, ale moze przynajmniej nauczy nieco wiecej ostroznosci. 
ob niebo
Och, teraz zdaje sie byc ono bardziej odlegle niz kiedykolwiek, choc z drugiej strony moze jednak blizsze... 
Zdarzenia:
- Przez chwile masz nikle wrazenie, ze cos poruszylo ziemia jednej ze scian grobu. 
- Odglosy dochodzace co jakis czas z gory napawaja watpliwosciami, czy lepiej wyjsc, czy moze jednak zostac.
Extrasy:
Lokacja dostepna tylko w nocy!

usiadz/poloz sie
Pewnie jest to mozliwe, tylko potem mozesz juz nie wstac... 

wyjdz z grobu (proba nieudana)
osoba 1.
Wyciagasz ramiona i chwytasz sie krawedzi grobu, jednak gdy probujesz sie podciagnac, zostaja ci w dloniach tylko grudki ziemi i ladujesz tam, gdzie wczesniej. 
osoba 3. na zewnatrz
Cos probuje wydostac sie z grobu!
osoba 3. rowniez w dole
* nieudolnie usiluje wydostac sie z grobu. 

wyjdz z grobu (proba udana)
osoba 1.
Wyciagasz ramiona i chwytasz sie krawedzi grobu, po czym zrecznie podskakujesz i podciagasz sie. Po kilku sekundach jestes na powierzchni.
osoba 3. w srodku
* zrecznie wydostaje sie z grobu.
osoba 3. na zewnatrz
* wychodzi z grobu!*/