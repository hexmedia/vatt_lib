
/*By Veli 08.12.09
Przy zapasowym przejscu na cmentarz - pnacza - Grabowa Buchta*/

#pragma strict_types
#include "dir.h"
#include "/d/Standard/Redania/dir.h" 
#include <stdproperties.h>
#include <std.h>
#include <mudtime.h>
#include <pogoda.h>
#include <macros.h>
#include <ss_types.h>
#include <sit.h>

inherit WIOSKA_OXEN_CMENTARZ_STD;

string dlugi_opis();
string dlugi_pnacza();
string rozkop_grob();
string przeczytaj_napisy();
int wejdz_do_grobowca(string str);

void
create_cmentarz()
{
    set_short("Przy omsza^lym murze");
    set_long(&dlugi_opis());
    add_prop(ROOM_I_INSIDE, 0);
    /*add_object();
    dodaj_rzecz_niewyswietlana("",1); - moze tu cos bedzie.*/
    add_item(({"iglaki","skalniaki","ro^slinno^s^c"}),"Starannie dobrane"+
        " najmodniejsze i najrzadsze odmiany tych^ze ro^slin porastaj^a"+
        " wiekszo^s^c bogatych grobowc^ow, cz^esciowo przys^laniaj^ac"+
        " zdobi^ace je napisy i p^laskorze^xby.\n");
    add_item(({"grobowce","grobowiec"}),"Zewsz^ad wyrastaj^a szlachetne"+
        " oraz... znacznie mniej szlachetne, pozbawione wr^ecz ksztyny"+
        " dobrego smaku grobowce. Niekt^ore to zwyk^le p^laskorze^xby, inne"+
        " stylizowane na altanki wygl^adaj^a jak wille w miniaturze. Przy"+
        " wej^sciu do jednego z takich po^smiertnych domk^ow nie zauwa^zasz"+
        " k^l^odki ani ^zadnego innego zamkni^ecia.\n");
    add_item("mur","Niski ceglany mur odgradzaj^acy zbocza wzniesienia od"+
        " brzeg^ow kapry^snego Pontaru niemal w ca^lo^sci pora^sni^ety mchem"+
        " oraz sieci^a grubych, zdrewnia^lych pn^aczy.\n");
    add_item("pn^acza",&dlugi_pnacza());
    add_sit("na grobowcu","na marmurowym grobowcu","z grobowca",4);
    add_sit("ko^lo pudla","ko^lo fosforyzuj^acego pomnika w kszta^lcie"+
        " pudla","spod, pozbawionego jakiegokolwiek wyczucia gustu pomnika"+
        " w kszta^lcie pudla",4);
    add_event("Mdl^acy zapach dzikiego bzu unosi si^e w powietrzu.\n");//WIOSNA
    add_event("Mg^ly zasnuwaj^a okolic^e wilgotnym, szarawym p^laszczem.\n");//NOC
    add_event("Z muru zrywa si^e nagle stadko wron i z dono^snym"+
        " krakaniem odlatuje w stron^e rzeki.\n");//NOC
    add_event("Z krzaka zerwa^l si^e jaki^s ptak.\n");//NOC
    add_event("Co^s poruszy^lo si^e mi^edzy grobowcami! Chyba...\n");//NOC
    add_event("Z oddali dochodzi ci^e ponure krakanie.\n");
    add_event("^Zeliwna furta grobowca skrzypn^e^la ostrzegawczo.\n");
    add_event("S^lyszysz czyje^s kroki dochodz^ace od strony alejki,"+
        " kt^ore po chwili milkn^a, zupelnie tak, jakby kto^s zobaczywszy"+
        " ciebie zawr^oci^l, b^ad^x przyczai^l si^e gdzie^s.\n");  
    add_exit(WIOSKA_OXEN_LOKACJE+"cmentarz/c_2",({"alejka","cos","cos"}));
    add_exit(WIOSKA_OXEN_LOKACJE+"cmentarz/c_5",({"n","cos","cos"}));
    add_exit(WIOSKA_OXEN_LOKACJE+"cmentarz/c_7",({"^scie^zka","cos","cos"}));
    //add_exit(WIOSKA_OXEN_LOKACJE+"cmentarz/c_grobowiec.c");
    //wyjscia zale^zne od warunk^ow - patrz na ko^ncu
    add_cmd_item("gr^ob", "rozkop", rozkop_grob, "Rozkop co?\n");
    add_cmd_item("napisy", "przeczytaj", przeczytaj_napisy, "Przeczytaj"+
        " co?\n");
}

string 
dlugi_opis()
{
    string str;

        str = "Stoisz nieopodal okaza^lego grobowca, przytulonego do"+
            " omsza^lego, niewysokiego muru oddzielaj^acego cmentarz"+
            " oraz samo wzniesienie od grz^askich brzeg^ow Ponstaru i"+
            " od razu zwraca uwage kontrast pomi^edzy tym miejscem a innymi"+
            " cz^e^sciami cmentarzyska. Las monument^ow ozdobionych w"+
            " najwymy^slniejszy, cho^c nie zawsze smaczny spos^ob otacza"+
            " ci^e i zniewala sw^a krzykliw^a form^a. Sadz^ac po wielko^sci"+
            " oraz przepychu pomnik^ow, przysz^lo ci si^e znale^s^c w"+
            " sektorze przeznaczonym dla nowobogackiej elity.";
        
    if (jest_dzien() == 1)
    {
        str += " Niemal ka^zdy gr^ob obrastaj^a przyci^ete na kszta^lt"+
            " sztylet^ow, sakw a nawet nagich kobiet iglaki oraz modne"+
            " rodzaje skalniak^ow, upodobniajac miejsca spoczynku do"+
            " przypa^lacowych ogr^odk^ow. Zewsz^ad, nawet z odlanego z"+
            " jakiej^s ^swiec^acej materii grobowca w ksztalcie pudla,"+
            " uderza kunszt rzemie^slniczy i nie ma w^atpliwo^sci, ^ze"+
            " pochowano tu nie tyle trupy, co ca^le trumny z^lota.";
    }

    if (jest_dzien() == 0)
    {
        str += " Dziwnie powykrzywiane kontury przypominaj^ace swym"+
            " kszta^ltem pokraczne istoty to zwyk^le krzewy przyci^ete"+
            " po prostu w niecodzienny spos^ob i nie wiadomo, co bardziej"+
            " przera^za - ich groteskowy nocny wygl^ad, czy te^z to, czym"+
            " mog^a okaza^c si^e za dnia. Najbardziej jednak zadziwia"+
            " stoj^acy nieco na uboczu pomnik w kszta^lcie pudla, kt^orego"+
            " fosforyzujaca materia zalewa okolic^e mlecznobia^lym"+
            " ^swiat^lem.";
    }

    return str;
}

string
dlugi_pnacza()
{
    string str;

    str = "Zdrewnia^le i poskr^ecane grube pn^acza glicynii wij^a si^e po"+
        " murze tworz^ac swoist^a sie^c, mo^zliwe, ^ze stabiln^a na tyle by"+
        " utrzyma^c nawet wi^ekszy ci^e^zar.";
    
        if (pora_roku() == MT_WIOSNA)
    {
        str += " Delikatne fioletowe kwiaty opadaj^a kaskad^a i mieszaj^ac"+
            " si^e z ga^l^azkami bluszczu niemal calkowicie zas^laniaj^a"+
            " ceglan^a ^scian^e.\n";
    }

        if (pora_roku() == MT_LATO)
    {
        str += " Delikatne fioletowe kwiaty opadaj^a kaskad^a i mieszaj^ac"+
            " si^e z ga^l^azkami bluszczu niemal calkowicie zas^laniaj^a"+
            " ceglan^a ^scian^e.\n";
    }

            if (pora_roku() == MT_JESIEN)
    {
        str += " Pokryte resztkami list^ow i uschni^etych kwiat^ow opadaj^a"+
            " kaskad^a i mieszaj^ac si^e z ga^l^azkami bluszczu niemal"+
            " calkowicie zas^laniaj^a ceglan^a ^scian^e.\n";
    }

            if (pora_roku() == MT_ZIMA)
    {
        str += " Nagie ga^l^ezie s^a oblodzone i pokryte gdzieniegdzie"+
            " warstw^a ^sniegu niemal ca^lkowicie zas^laniaj^a ceglan^a"+
            " ^scian^e.\n";
    }
}    


string
rozkop_grob()
{
    string str;

    str = "Zdaje si^e, ^ze nie jest to najlepszy pomys^l, gdy^z s^a to w"+
        " wi^ekszo^sci murowane bad^x kute w kamieniu grobowce. Zauwa^zasz"+
        " jednak, ^ze drzwiczki prowadz^ace do jednego z nich nie s^a"+
        " zamkni^ete na k^l^odk^e.\n";

        saybb(QCIMIE(TP, PL_MIA) + " rozgl^ada si^e woko^lo z krytyczn^a"+
        " min^a i zawiesza wzrok na jednym z grobowc^ow.\n");

    return str;
}

string
przeczytaj_napisy()
{
    string str;

    str = "Czy na pewno chcesz przeczyta^c:\n\Tu"+
        " le^zy Walentian Z^lotoz^eby. ^Zy^l, pi^l, rzyga^l, ch^edo^zy^l, a"+
        " w przerwie da^l si^e obrabowa^c?\n\Czy te^z wolisz dowiedzie^c"+
        " si^e, ^ze:\n\Tu le^zy m^oj ma^l^zonek Gwidor i jego wierny"+
        " ko^n?\n\A mo^ze zainteresuje ci^e:\n\Pipulek. Wierny pies i"+
        " przyjaciel naszego ukochanego synka. - na fosforyzuj^acym grobie"+
        " pudla?\n";

        saybb(QCIMIE(TP, PL_MIA) + " przygl^ada si^e napisom na pomnikach.\n");

   return str;
}


void
init()
{
    ::init();
    add_action(wejdz_do_grobowca,"wejd^x");
}

int wejdz_do_grobowca(string str)
{

    if(query_verb() == "wejdz")
        notify_fail("Gdzie chcesz wej^s^c? Do grobowca?\n");

        if(TP->query_prop(SIT_LEZACY) || TP->query_prop(SIT_SIEDZACY))
        {
            write("Przemieszczanie si^e na siedz^aco mo^ze by^c ciekawym"+
                " wyzwaniem, ale mo^ze lepiej wsta^n.\n");
            return 1;
        }

        write("Upewniaj^ac si^e, i^z grabarza nie ma nigdzie w pobli^zu"+
            " ostro^znie uchylasz zardzewia^le drzwiczki i znikasz we"+
            " wn^etrzu grobowca.\n");
        saybb(QCIMIE(TP, PL_MIA) + " rozejrzywszy si^e ukradkiem znika we"+
            " wn^etrzu grobowca.\n");       

    this_player()->move_living(WIOSKA_OXEN_LOKACJE+"cmentarz/c_grobowiec.c", 0);

    return 0;
}

public string
exits_description() 
{
    return "Mi^edzy g^estymi krzakami zauwa^zasz przej^scie pod kolumn^e"+
        " na po^lnoc st^ad oraz wij^ac^a si^e mi^edzy grobowcami alejk^e."+
        " Nieco na uboczu, pod samym murem biegnie te^z ^scie^zka"+
        " prowadz^aca na p^o^lnocny wsch^od cmentarza.\n";
}


 /*
 * @return Kod okre�laj�cy wynik przeniesienia:
 *         <ul>
 *           <li> 0 Sukces</li>
 *           <li> 1 Zbyt ci�ki dla miejsca docelowego</li>
 *           <li> 2 Nie mo�e by� opuszczony</li>
 *           <li> 3 Nie mo�na przenie�� z obecnego pomieszczania </li>
 *           <li> 4 Obiekt nie mo�e by� umieszczany w torbach, etc.</li>
 *           <li> 5 Miejsce docelowe nie przyjmuje obiekt�w</li>
 *           <li> 6 Obiekt nie mo�e by� podniesiony</li>
 *           <li> 7 Inne (Tekst zosta� wypisany w funkcji move())</li>
 *           <li> 8 Zbyt du�y dla miejsca docelowego</li>
 *           <li> 9 Dotychczasowy kontener jest zamkni�ty</li>
 *           <li> 10 Docelowy kontener jest zamkni�ty</li>
 *           <li> 11 Jest za du�o rzeczy w docelowym kontenerze</li>
 *         </ul>

[specjalne]
zmiana por dnia
zachod slonca
Ostatnie okruchy jasno^sci dnia nikna wraz z zachodz^acym s^lo^ncem, a cmentarzysko nie^spiesznie spowija g^esty cie^n. 
wschod slonca
Pierwsze promienie s^lo^nca przecinaj^a ciemnogranatowe nocne niebo i powoli roz^swietlaja okolice.


Wyjscia:
n-c5 - Podazasz w strone kolumny.
* podaza w strone kolumny.
* nadchodzi od strony kolumny.

OPIS NOCNY
Wyjscia:
w jakimkolwiek kierunku - Znikasz w cieniu.
* znika w cieniu.
* wylania sie z cienia.

pokonywanie muru - przejscie z lokacji na lokacje za murem

wespnij sie po pnaczach 
Na lokacji na ktora sie wchodzi
* uzywajac stwardnialego pnacza niczym sznura, zsuwa sie po ceglanym murze na ulice.*/