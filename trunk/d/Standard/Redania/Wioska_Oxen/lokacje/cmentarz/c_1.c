
/*By Veli 05.12.09
Przed bram^a cmentarza - wioska Oxen*/

#pragma strict_types
#include "dir.h"
#include "/d/Standard/Redania/dir.h"
#include <stdproperties.h>
#include <mudtime.h>
#include <pogoda.h>
#include <macros.h>
#include <filter_funs.h>
inherit WIOSKA_OXEN_CMENTARZ_STD;

//string krotki_opis();
string dlugi_opis();
string dlugi_wzgorze(); //opis wzgorza
string dlugi_zagajnik(); //opis laskow na wyspie
string event_cmentarz(); //kontrola eventow zaleznych
string pora_roku_pogoda();
string pora_roku_dzien();
string pora_roku_noc();

void
create_cmentarz()
{
    set_short("Placyk przy bramie");
    //set_short(&krotki_opis());
    set_long(&dlugi_opis());
    add_prop(ROOM_I_INSIDE, 0);
    add_object(WIOSKA_OXEN_PRZEDMIOTY+"obiekty/cmentarna_tablica.c",1);
    add_object(WIOSKA_OXEN_PRZEDMIOTY+"obiekty/brama_c.c",1);
    add_item("placyk","Niewielki placyk okolony g^estym ^l^egowym zagajnikiem"+
        " przed bram^a cmentarn^a wysypano drobnymi otoczakami, kt^ore"+
        " skrzypi^ac przy ka^zdym twoim kroku przywodz^a na my^sl ziarna"+
        " piasku w klepsydrze, kt^ore bezg^lo^snie przesypuj^ac si^e w do^l"+
        " odliczaj^a czas jaki jeszcze pozosta^l, nim znajdziemy si^e po"+
        " drugiej stronie - w za^swiatach.\n");
    add_item("zagajnik",&dlugi_zagajnik());
    add_item("wzg^orze",&dlugi_wzgorze());
    add_sit("pod murem","pod murem","spod muru",6);
    add_sit("przy ^scie^zce","przy ^scie^zce, w cieniu olszynowego"+
        " zagajnika","z ziemi",4);
    add_event(&event_cmentarz());
    add_exit(WIOSKA_OXEN_LOKACJE+"w_8",({"^scie^zka","scie^zk^a w d^o^l",
        "ze wzg^orza"}));
}

/*string
krotki_opis()
{
    string str = "";

    if (jest_dzien() == 1)
    {
        str = "Placyk przy bramie\n.";
    }

    if (jest_dzien() == 0)
    {
        str = "Pod zamkni^et^a bram^a\n";
    }

    return str;

}*/

string
dlugi_opis()
{
    string str;

    str = "Tu stroma ^scie^zka prowadz^aca na cmentarne wzg^orze ko^nczy si^e,"+
        " by ust^api^c miejsca niewielkiemu placykowi o kszta^lcie po^lkola."+
        " G^esto porastaj^ace ca^l^a wysepk^e olsowe zagajniki i podmok^le"+
        " zaro^sla przerzedzaj^a si^e nieco, by przepu^sci^c troch^e"+
        " wi^ecej";
  
    if (jest_dzien() == 1)

    {
        str += "dziennego ^swiat^la,";
    }

    if (jest_dzien() == 0)
    {
        str += "nik^lego ksi^e^zycowego ^swiat^la,";
    }
 
        str += " kt^ore i tak nie jest w stanie odeprze^c dziwnego uczucia"+
        " pustki i smutku. Ca^le wzg^orze otacza niewysoki mur z czerwonej"+
        " ceg^ly, schodz^acy miejscami, a^z po sam zaro^sni^ety"+
        " k^l^ebowiskiem olszyn, grab^ow, wi^az^ow i sitowia, brzeg."+
        " Jedynym bezpiecznym i og^olnodost^epnym wej^sciem na cmentarzysko"+
        " wydaje si^e by^c solidna stalowa brama. Licznie naznaczona rdz^a"+
        " pnie si^e majestatycznie ku g^orze, a jej strzeliste wyko^nczenia"+
        " zdaj^e si^e b^laga^c bog^ow w marnej modlitwie o nadziej^e.";
    
    return str;
}

string
dlugi_wzgorze()
{
    string str;

    str = "Wzg^orze, na kt^orym stoisz to zarazem najwy^zej po^lo^zone"+
        " miejsce na ca^lej, ulokowanej w delcie Pontaru wysepki."+
        " Chocia^z samo wzniesienie nie nale^zy mo^ze do zbyt okaza^lych"+
        " to i tak roztarcza si^e st^ad otwarty widok na szeroki i"+
        " nadzwyczaj leniwy bieg";

        if (pora_roku() == MT_WIOSNA)
    {
        str += " wezbra^lej po zimie rzeki, zazielenione, poro^sni^ete"+
            " sitowiem brzegi, trz^esawiska oraz budz^ac^a si^e do ^zycia"+
            " male^nk^a wioseczk^e zwan^a Grabowa Buchta, majacz^ac^a"+
            " gdzie^s u podn^o^za, po^sr^od ^l^eg^ow.\n";
    }

        if (pora_roku() == MT_LATO)
    {
        str += " kipi^acej z gor^aca i niemi^losiernie ^smierdz^acej w upale"+
            " rzeki, w kt^orej wodach rozk^ladaj^a si^e teraz wszelakie"+
            " mo^zliwe ^smieci i fekalia z dw^och wielkich miast - "+
            " Oxenfurtu i Novigradu.\n";//DO ZMIANY!!!!!!!!!!
    }

        if (pora_roku() == MT_JESIEN)
    {
        str += " sp^lywaj^acej spiesznie ku morzu rzeki, poz^locone"+
            " jesiennymi li^scmi ^l^egowe zagajniki oraz przycupni^et^a u"+
            " podn^o^za wiosk^e, zwan^a Grabowa Buchta. W powietrzu czu^c"+
            " ju^z wyra^xnie butwiej^ac^a roslinno^s^c oraz zapach palonego"+
            " w piecach w osadzie, drewna.\n";
    }

        if (pora_roku() == MT_ZIMA)
    {
        str += " paruj^acej w ch^lodnym, zimowym powietrzu rzeki oraz"+
            " przypruszone ^sniegiem i poro^sni^ete zesch^lym sitowiem"+
            " oraz bezlistnymi ^legowymi zagajnikami brzegi. Z komin^ow"+
            " przycupni^etej u podn^o^za wioseczki zwanej Grabow^a Bucht^a"+
            " unosz^a si^e bia^lawe dymy o czystym drzewnym zapachu"+
            " spowijaj^ac okolic^e niby mg^la.\n";
    }

    return str;
}

string
dlugi_zagajnik()
{
    string str;

    str = "To co wydawa^lo si^e na pierwszy rzut oka niczym wi^ecej ni^z"+
        " ^estymi zaro^slami to tak napraw^e nic innego jak zastoiskowe"+
        " olsy. Okresowo zalewane nadrzeczne lasy ^l^egowe w dolinie rzeki."+
        " Niemal ca^l^a wysepk^e w delcie Pontaru, zagubi^a gdzie^s"+
        " pomi^edzy Oxenfurem a Pian^a ciasno porastaj^a, lubuj^ace si^e"+
        " w wilgotnym pod^lo^zu";
    
        if (pora_roku() == MT_WIOSNA)
    {
        str += " zieleni^ace si^e wiosennie po^sr^od wezbra^lych w^od olsze,"+
            " wi^azy, graby i kar^lowate topole. Tu^z przy podmok^lym,"+
            " trudnym w tej chwili do wyznacznia u brzegu rzeki, zauwa^zasz"+
            " nawet wybuja^le d^eby oraz cenne jesiony wyrywaj^ace si^e ku"+
            " g^orze z g^aszczu sitowia i niskich wierzb.\n";
    }

        if (pora_roku() == MT_LATO)
    {
        str += " kwitn^ace olsze, wi^azy, graby i kar^lowate,"+
            " rozpoczynaj^ace w^lasnie pylenie topole. Tu^z przy podmok^lym"+
            " brzegu zauwa^zasz nawet wybuja^le d^eby oraz cenne jesiony"+
            " wyrywaj^ace si^e ku g^orze z g^aszczu sitowia i niskich"+
            " wierzb.\n";
    }

        if (pora_roku() == MT_JESIEN)
    {
        str += " zrzucaj^ace przed zim^a listowie olsze, wi^azy, graby i "+
            " kar^lowate, srebrzyste topole. Tu^z przy zabagnionym"+
            " brzegu zauwa^zasz nawet wybuja^le d^eby oraz cenne jesiony"+
            " wyrywaj^ace si^e ku g^orze z g^aszczu butwiej^acego sitowia"+
            " i niskich wierzb.\n";
    }

        if (pora_roku() == MT_ZIMA)
    {
        str += " nagie korony krzewiastych olsz, wi^az^ow, grab^ow i "+
            " kar^lowate, srebrzyste topole. Tu^z przy zamarzni^etym obecnie"+
            " brzegu zauwa^zasz nawet wybuja^le d^eby oraz cenne jesiony"+
            " wyrywaj^ace si^e ku g^orze z g^aszczu pokrytego szronem"+
            " sitowia i k^lebowiskach bezlistnych ga^l^azek wierzb.\n";
    }

    return str;
}

string
event_cmentarz()
{

    switch (MT_PORA_DNIA)
    {
        case MT_SWIT:
            return process_string(({"Mg^ly k^l^ebi^a si^e w oddali ponad"+
            " ciemn^a powierzchni^a rzeki.\n","Z uschni^etego"+
            " krzaka zerwa^l si^e jaki^s ptak.\n","Z k^epy olszyn"+
            " s^lycha^c skrzeczenie sroki.\n","@@pora_roku_noc@@",
            "@@pora_roku_pogoda@@"})[random(5)]);
        case MT_RANEK:
            return process_string(({"Z wioski u podn^o^za wzniesienia"+
            " dochodz^a ci^e pokrzykiwania rybak^ow.\n","Zarumieniony"+
            " dzieciak wygl^ada z cmentarza na placyk przez uchylone"+
            " skrzyd^lo bramy i zobaczywszy ci^e szybko zmyka.\n","K^atem"+
            " oka zauwa^zasz jaki^s ruch na cmentarzysku.\n","Z k^epy olszyn"+
            " s^lycha^c skrzeczenie sroki.\n","@@pora_roku_dzien@@",
            "@@pora_roku_pogoda@@"})[random(6)]);
        case MT_POLUDNIE:
            return process_string(({"Nieopodal bramy przemnk^e^la"+
            " ciemnoodziana ^za^lobnica.\n","Gdzie^s ze ^scie^zki"+
            " prowadz^acej w d^o^l wzniesienia s^lyszysz st^lumione,"+
            " niezbyt wybredne przekle^nstwo.\n","Z k^epy olszyn"+
            " s^lycha^c skrzeczenie sroki.\n","@@pora_roku_dzien@@",
            "@@pora_roku_pogoda@@"})[random(5)]);
        case MT_POPOLUDNIE:
            return process_string(({"Widzisz jak w oddali, do nabrze^za"+
            " Grabowej Buchty dobija flisacka tratwa.\n","Z k^epy olszyn"+
            " s^lycha^c skrzeczenie sroki.\n","W do^l Pontaru, ku delcie"+
            " sp^lywa ca^ly szereg flisackich tratw.\n","@@pora_roku_dzien@@",
            "@@pora_roku_pogoda@@"})[random(5)]);
        case MT_WIECZOR:
            return process_string(({"Do nabrze^za le^z^acej w dole wioski"+
            " dobija przed noc^a kilka rybackich ^lajb.\n","Z k^epy olszyn"+
            " s^lycha^c skrzeczenie sroki.\n","Zwolna nadchodz^aca noc"+
            " k^ladzie si^e coraz wi^ekszym cieniem na okoliczne zaro^sla"+
            " i sam placyk.\n","@@pora_roku_dzien@@",
            "@@pora_roku_pogoda@@"})[random(5)]);
        default: // po zmierzchu i w nocy
            return process_string(({"S^lyszysz jakie^s skrobanie po drugiej"+
             " stronie bramy cmentarzyska.\n","Na ^scie^zce, pomi^edzy"+
             " drzewami zamigota^lo przez chwil^e jasne ^swiat^lo pochodni.\n",
             "Gdzie^s z g^ory s^lycha^c g^lo^sne nawo^lywanie zagubionej"+
             " mewy.\n","@@pora_roku_noc@@","@@pora_roku_pogoda@@"})[random(5)]);
    }
}

string
pora_roku_dzien()
{
    switch (MT_PORA_ROKU)
    {
         case MT_WIOSNA:
             return ({"Dochodzi ci^e nik^ly sw^ad p^lon^acych^swiec i"+
             " wilgotnej ziemi.\n","Co^s plusn^e^lo dono^snie w"+
            " podmok^lych zaro^slach. Pewnie bobry.\n","Nadrdzewia^le"+
             " skrzyd^lo bramy skrzypi irytuj^aco.\n","Stadko wr^obli"+
             " przysad^lo pod jednym z krzak^ow i rozdziobuje"+
             " kie^lkuj^ac^a pod nim traw^e.\n","Klucz dzikich g^esi"+
             " przelatuje nad rz^ek^a","W oddali zauwa^zasz szaraw^a czapl^e"+
             " brodz^ac^a w^sr^od nabrze^znych zaro^sli.\n"})[random(6)];
         case MT_LATO:
             return ({"Dochodzi ci^e nik^ly sw^ad p^lon^acych^swiec.\n",
             "Co^s plusn^e^lo dono^snie w podmok^lych zaro^slach. Pewnie"+
             " bobry.\n","Nadrdzewia^le, poro^sni^ete dzikim chmielem"+
             " skrzyd^lo bramy skrzypi irytuj^aco.\n","W oddali zauwa^zasz"+
             " szaraw^a czapl^e brodz^ac^a w^sr^od nabrze^znych zaro^sli.\n",
             "Znad rzeki unosi si^e md^ly fetor nieczysto^sci.\n"})[random(5)];
         case MT_JESIEN:
             return ({"Dochodzi ci^e nik^ly sw^ad p^lon^acych^swiec"+
             " przemieszany z woni^a gnij^acych li^sci.\n","Co^s plusn^e^lo"+
             " dono^snie w podmok^lych zaro^slach. Pewnie bobry.\n",
             "Nadrdzewia^le, poro^sni^ete z^loc^acym si^e jesiennie dzikim"+
             " chmielem skrzyd^lo bramy skrzypi irytuj^aco.\n","Klucz"+
             " dzikich g^esi przelatuje nad rz^ek^a"})[random(4)];
         case MT_ZIMA:
             return ({"Dochodzi ci^e nik^ly sw^ad p^lon^acych^swiec"+
             " roznosz^acy si^e w mro^xnym powietrzu.\n","Co^s plusn^e^lo"+
             " dono^snie w podmok^lych zaro^slach. Pewnie bobry.\n",
             "Nadrdzewia^le, poro^sni^ete zesch^l^a pl^atanin^a pn^aczy"+
             " skrzyd^lo bramy skrzypi irytuj^aco.\n"})[random(3)];
    }
}
string
pora_roku_noc()
{
    switch (MT_PORA_ROKU)
    {
         case MT_WIOSNA:
             return ({"Dochodzi ci^e nik^ly sw^ad p^lon^acych^swiec i"+
             " wilgotnej ziemi.\n","Rzek^a niesie si^e echo"+
             " czyjego^s krzyku.\n","Co^s poruszy^lo si^e w ciemnych"+
             " zaro^slach!\n"})[random(3)];
         case MT_LATO:
             return ({"Dochodzi ci^e nik^ly sw^ad p^lon^acych^swiec.\n",
             "Rzek^a niesie si^e echo czyjego^s krzyku.\n","Gdzie^s w oddali"+
             " zaskrzecza^ly ^zurawie.\n","Co^s poruszy^lo si^e w ciemnych"+
             " zaro^slach!\n"})[random(3)];
         case MT_JESIEN:
             return ({"Dochodzi ci^e nik^ly sw^ad p^lon^acych^swiec"+
             " przemieszany z woni^a gnij^acych li^sci.\n","Rzek^a niesie"+
             " si^e echo czyjego^s krzyku.\n","Co^s poruszy^lo si^e w ciemnych"+
             " zaro^slach!\n"})[random(3)];
         case MT_ZIMA:
             return ({"Dochodzi ci^e nik^ly sw^ad p^lon^acych^swiec"+
             " roznosz^acy si^e w mro^xnym powietrzu.\n","Rzek^a niesie"+
             " si^e echo czyjego^s krzyku.\n","^Snieg skrzypi cicho przy"+
             " ka^zdym twoim poruszeniu.\n","Co^s poruszy^lo si^e w ciemnych,"+
             " za^snie^zonych zaro^slach!\n"})[random(4)];
    }
}

string
pora_roku_pogoda()
{
    switch(POGODA_OPADY)
    {
    case POG_OP_ZEROWE:
        return ({"Na niebie nie dostrzegasz nawet jednej, mog^acej"+
        " zwiastowa^c deszcz chmury.\n","Intensywnie nas^aczone rzeczn^a"+
        " wilgoci^a powietrze, przypomina nieco smrodliwe miejskie zau^lki."+
        " Wida^c dawno nie pada^l deszcz.\n"})[random(2)];
    case POG_OP_D_L:
        return ({"Z ciemnoszarych, sun^acych ci^e^zko nad wzg^orzem chmur"+
        " zaczynaj^a pada^c pierwsze krople deszczu, kt^ore po chwili"+
        " zamieniaj^a si^e w paskudn^a ci^ag^l^a m^zawk^e.\n"})[random(1)];
    case POG_OP_D_S:        
        return ({"Strugi deszczu zmieniaj^a si^e w ciemn^e ^scian^e wody,"+
        " prawie ca^lkiem ograniczaj^ac widoczno^s^c.\n"})[random(1)];
    case POG_OP_S_S:
    case POG_OP_S_L:
        return ({"Z szarawanych chmur ponad wzg^orzem i szerok^a rzek^a"+
        " sypie drobny ^snieg.\n","Niewielkie p^latki ^sniegu wiruj^a"+
        " wok^o^l ciebie osadzaj^ac si^e na odzieniu, pobliskich"+
        " zaro^slach i placyku.\n"})[random(2)];
    case POG_OP_S_C:
        return ({"Wok^o^l rozp^eta^la si^e prawdziwa ^snie^zyca!","Szale^ncze"+
        " podmuchy lodowatego wiatru rozwiewaj^a wok^o^l tumany ^sniegu i nie"+
        " zanosi si^e by zamie^c szybko odpu^sci^la.\n"})[random(2)];
    }

    switch (POGODA_ZACHMURZENIE)
    {
    case POG_ZACH_ZEROWE:
        return ({"Na niebie nie dostrzegasz nawet jednej, mog^acej"+
        " zwiastowa^c deszcz chmury.\n","Czyste niebo zdaje si^e by^c jak"+
        " wielki bezkres. Podobnie wygl^ada chyba tylko bezkres"+
        " morza.\n"})[random(2)];
    case POG_ZACH_LEKKIE:
        return ({"Ledwie kilka chmurek odbija si^e w pob^lyskuj^acej tafli"+
        " rzeki. Czy raczej odbija^loby si^e gdyby owa tafla nie by^la"+
        " Pontarem, a ju^z zw^laszcza jego zanieczyszczon^a"+
        " delt^a.\n"})[random(1)];       
    case POG_ZACH_DUZE:
        return ({"Ciemnoszare chmury przesuwaj^a si^e nad wzg^orzem i"+
        " rozlewiskiem Pontaru.\n"})[random(1)];   
    case POG_ZACH_CALKOWITE:
        return ({"Ciemnoszare ci^ezkie chmury przesuwaj^a si^e nad"+
        " wzg^orzem, szczelnie zakrywaj^ac niebo.\n"})[random(1)];        
    }
    
    switch (POGODA_WIATR)
    {
    case POG_WI_ZEROWE:
        return ({"Nawet jeden leciutki powiew wiatru nie przenika"+
        " zasta^lego, ^smierdz^acego rzek^a powietrza.\n"})[random(1)]; 
    case POG_WI_LEKKIE:
        return ({"Cichy podmuch wiatru poruszyl ga^l^eziami drzew i niemal"+
        " natychiast zanikn^a^l.\n","S^laby wiatr zawia^l od rzeki, nieco"+
        " ^lagodz^ac zaduch.\n"})[random(2)]; 
    case POG_WI_SREDNIE:
        return ({"Wiatr niesie znad rzeki kolejne smrodliwe wonie....\n Na"+
        " szcz^e^scie kolejny mocny podmuch szybko porywa je"+
        " dalej.\n"})[random(1)]; 
    case POG_WI_DUZE: 
        return ({"Silne podmuchy wiatru smagaj^ace szczyt wziesienia"+
        " zaczynaj^a doskwiera^c coraz bardziej.\n"})[random(1)]; 
    case POG_WI_BDUZE:
        return ({"Wicher wyje op^eta^nczo pomi^edzy konarami"+
        " olch.\n"})[random(1)]; 
    }
}

public string
exits_description() 
{
    string str;

    str = "";

        if (jest_dzien() == 1)

    {
        return "Mo^zesz st^ad pod^a^zyc, biegn^ac^a mi^edzy olszynami"+
            " ^scie^zk^a w do^l wzniesienia ku wiosce, b^adz wej^s^c"+
            " przez uchylone skrzyd^lo bramy na cmentarzysko.\n."; 
    }
    
    if (jest_dzien() == 0)
    {
        return "Mo^zesz st^ad pod^a^zyc biegn^ac^a mi^edzy olszynami"+
            " ^scie^zk^a w do^l wzniesienia ku wiosce. Brama prowadz^aca"+
            " na cmentarzysko jest w tej chwili zamkni^eta. Wida^c trzeba"+
            " poczeka^c az do ^switu.\n.";
    }
}

/*[specjalne]
dzien: Skrzyd^lo pot^e^znej stalowej bramy jest uchylone.
noc: Pot^e^zna stalowa brama jest zamkni^eta.

*/
