
/*By Veli 08.12.09
Na srodku cmentarza - pod kolumna - wioska Oxen*/

#pragma strict_types
#include "dir.h"
#include "/d/Standard/Redania/dir.h"
#include <stdproperties.h>
#include <std.h>
#include <mudtime.h>
#include <pogoda.h>
#include <macros.h>
inherit WIOSKA_OXEN_CMENTARZ_STD;

string dlugi_opis();
string rozkop_grob();
string dlugi_trawy();
string dlugi_krzaki();

void
create_cmentarz()
{
    set_short("Pod smuk^l^a kolumn^a");
    set_long(&dlugi_opis());
    add_prop(ROOM_I_INSIDE, 0);
    /*add_object();
    dodaj_rzecz_niewyswietlana("",1); - moze tu cos bedzie.*/
    add_item(({"krzaki","zaro^sla"}),&dlugi_krzaki());
    add_item("trawy",&dlugi_trawy());
    add_item("alejk^e","Przesmyk mi^edzy masywnymi grobowcami majacz^acymi"+
        " na po^ludniu.\n");
    add_item("kolumn^e","Stoj^aca na niewielkim piedestale marmurowa"+
        " kolumna, wyrze^xbiona z wielkim artyzmem prezentuje si^e"+
        " nadzwyczaj okazale i g^oruje nad ca^l^a przestrzeni^a"+
        " cmentarzyska. Klasyczne ^z^lobienia czyni^a j^a strzelist^a i"+
        " jednocze^snie nadaj^a smuk^lo^sci, jednak jej harmoni^e burzy"+
        " pewien szczeg^o^l - mniej wiecej na dw^och trzecich wysoko^sci,"+
        " czyli oko^lo czterech metr^ow ponad tob^a kolumna jest z^lamana."+
        " Wokol nie wida^c ^zadnych oznak zniszczenia, mo^zliwe wi^ec, ^ze"+
        " w my^sl dawnych zwyczaj^ow, kolumna zosta^la ju^z tak"+
        " wyrze^xbiona, mo^zliwe jest te^z, ^ze zosta^la zniszczona"+
        " p^o^xniej - dla ciebie to, jak rownie^z jej prawdziwe znaczenie"+
        " oraz funkcja pozostan^a tajemnic^a.\n");
    add_item("drzewa","Wykute w kamieniu drzewa, imponuj^ace swymi"+
        " rozmiarami i misternym wykonaniem najpewniej by^ly cz^e^sci^a"+
        " dawnych zwyczaj^ow pogrzebowych, kt^ore z czasem uleg^ly"+
        " zapomnieniu, podobnie jak i ci, kt^orzy odeszli. Tak naprawd^e"+
        " granitowe oraz marmurowe pomniki, zwane przez okolicznych"+
        " zwalonymi drzewami to tylko pnie z poobcinanymi ga^l^eziami,"+
        " pozbawione zupe^lnie koron. Zauwa^zasz, ^ze dokladni"+
        " rzemie^slnicy szczeg^oln^a uwag^e po^swi^ecili niezwykle"+
        " wyra^xnemu zaznaczeniu kory oraz s^loj^ow, a zw^laszcza ilo^sci"+
        " tych drugich.\n");
    add_item("s^loje","Mistrzowsko wykute s^loje s^a tak wyra^xnie, ^ze bez"+
        " problemu mo^zesz je policzy^c. Drzewo stoj^ace najbli^zej ciebie"+
        " liczy sobie dwa tuziny s^loj^ow, niekt^ore maja ich znacznie"+
        " mniej, a jeszcze inne prawdopodobnie maja ich grubo ponad sto.\n");
    add_sit("pod kolumn^a","na piedestale pod kolumn^a","spod kolumny",4);
    add_event("Mdl^acy zapach dzikiego bzu unosi si^e w powietrzu.\n");
    add_event("Ma^ly wr^obel przysiad^l na chwil^e na o^ltarzu, po"+
        " czym oczy^sciwszy sobie skrzyde^lko odlecia^l.\n");//DZIEN
    add_event("Z muru zrywa si^e nagle stadko wron i z dono^snym"+
        " krakaniem odlatuje w stron^e rzeki.\n");//NOC
    add_event("Z uschni^etego krzaka zerwa^l si^e jaki^s ptak.\n");//NOC
    add_event("Nagle zza piedesta^lu dochodzi ci^e ciche syczenie.\n");
    add_event("Dwa gawrony przysiad^ly na jednym ze zwalonych drzew.\n");
    add_event("S^lyszysz czyje^s kroki dochodz^ace od strony alejki,"+
        " kt^ore po chwili milkn^a, zupelnie tak, jakby kto^s zobaczywszy"+
        " ciebie zawr^oci^l, b^ad^x przyczai^l si^e gdzie^s.\n"); 
    add_event("Szum traw zdaje si^e wzmaga^c, by po chwili ucichn^a^c"+
        " zupe^lnie.\n"); 
    add_event("Kolumna ciemnieje od lej^acych si^e z nieba strug"+
        " deszczu.\n");
    add_event("Przemokni^ety wr^obel przysiad^l na kamiennym pniu"+
        " w poszukiwaniu schronienia, jednak po chwili odlecia^l.\n"); 
    add_exit(WIOSKA_OXEN_LOKACJE+"cmentarz/c_6",({"ne","cos","cos"}));
    add_exit(WIOSKA_OXEN_LOKACJE+"cmentarz/c_8",({"alejka","cos","cos"}));
    add_exit(WIOSKA_OXEN_LOKACJE+"cmentarz/c_7",({"se","cos","cos"}));
    add_exit(WIOSKA_OXEN_LOKACJE+"cmentarz/c_4",({"scie^zka","cos","cos"}));
    //wyjscia zale^zne od warunk^ow - patrz na ko^ncu
    add_cmd_item("gr^ob", "rozkop", rozkop_grob, "Rozkop co?\n");
}

string 
dlugi_opis()
{
    string str;

    if (jest_dzien() == 1)
    {

        str = "Zewsz^ad jak okiem siegn^a^c otacza ci^e las kamiennych"+
            " drzew, wyrastaj^acy dooko^la zlamanej marmurowej kolumy."+
            " Niekt^ore wyzieraj^a nie^smia^lo z g^estych zaro^sli pod"+
            " p^o^lnocn^a cz^e^sci^a muru, inne za^s ton^a w morzu wysokich"+
            " traw porastaj^acych wschodnie po^lacie cmentarzyska. Sama"+
            " kolumna, ustawiona na niewielkim piedestale zdaje si^e z"+
            " jakiego^s powodu stanowi^c centrum ca^lego cmentarza,"+
            " jednocze^snie nad nim g^oruj^ac. Aura w tym miejscu nie"+
            " przypomina tej, jaka unosi si^e w innych cz^e^sciach, by^c"+
            " mo^ze powodem jest niezwyk^le pod^lo^ze wy^lo^zone du^zymi"+
            " p^lytami z kamienia, tak ^ze nie masz szansy dotkn^a^c go^lej"+
            " ziemi. Przygl^adaj^ac si^e im bli^zej dochodzisz do wniosku,"+
            " ^ze niew^atpliwie znajdujesz si^e w najstarszym miejscu"+
            " poch^owku w okolicy, jednak nie widza^c ^zadnych napis^ow ni"+
            " symboli nie jeste^s w stanie stwierdzi^c, kogo lub co"+
            " upamietniaj^a 'zwalone drzewa'. Ktokolwiek to by^l,"+
            " prawdopodobnie odszed^l na d^lugo przed dniem twoich urodzin.\n";
    }

    if (jest_dzien() == 0)
    {
        str = "Zewsz^ad jak okiem siegn^a^c w ciemno^sci ja^snieje las"+
            " kamiennych drzew, wyrastaj^acy dooko^la z^lamanej marmurowej"+
            " kolumy. Niekt^ore wyzieraj^a nie^smia^lo z g^estych zaro^sli"+
            " pod p^o^lnocna cz^e^sci^a muru, inne za^s ton^a w morzu"+
            " wysokich traw i mgie^l na wschodzie. Sama kolumna, ustawiona"+
            " na niewielkim piedestale zdaje si^e z jakiego^s powodu"+
            " stanowi^c centrum ca^lego cmentarza, jednocze^snie nad nim"+
            " g^oruj^ac. Aura w tym miejscu nie przypomina tej, jaka unosi"+
            " si^e w innych czesciach ^zalnika, by^c mo^ze powodem jest"+
            " niezwyk^le pod^lo^ze wylozone duzymi plytami z kamienia, tak"+
            " ^ze nie masz szansy dotkn^a^c go^lej ziemi. Przygl^adaj^ac"+
            " si^e im bli^zej dochodzisz do wniosku, ^ze niew^atpliwie"+
            " znajdujesz si^e w najstarszym miejscu poch^owku w okolicy,"+
            " jednak nie widz^ac ^zadnych napis^ow ni symboli nie jeste^s w"+
            " stanie stwierdzi^c, kogo lub co upamietniaj^a 'zwalone drzewa'."+
            " Ktokolwiek to by^l, prawdopodobnie odszed^l dawno temu...\n";
    }

    return str;
}

string
dlugi_trawy()
{

    string str; 

    if (pora_roku() == MT_WIOSNA)
    {
        str = "^Lany zazielenionych traw porastaj^a ca^l^a wschodni^a"+
            " cz^e^s^c cmentarza. Od^zywione i nasi^akni^ete wilgoci^a"+
            " wygladaj^a zupe^lnie niczym szmaragdowe ostrza, w^sr^od"+
            " kt^orych, nieco bardziej na po^ludniu wyrasta samotne,"+
            " umieraj^ace drzewo.\n";
    }

        if (pora_roku() == MT_LATO)
    {
        str = "^Lany zazielenionych traw porastaj^a ca^l^a wschodni^a"+
            " cz^e^s^c cmentarza. Od^zywione i nasi^akni^ete wilgoci^a"+
            " wygladaj^a zupe^lnie niczym szmaragdowe ostrza, w^sr^od"+
            " kt^orych, nieco bardziej na po^ludniu wyrasta samotne,"+
            " umieraj^ace drzewo.\n";
    }

            if (pora_roku() == MT_JESIEN)
    {
        str = "^Lany wysuszonych przez ch^l^od i s^lo^nce wysokich traw"+
            " porastaj^a ca^l^a wschodni^a cz^e^s^c cmentarza. O tej porze"+
            " wygladaj^a zupe^lnie niczym szare morze ostrych igie^l,"+
            " w^sr^od kt^orych, nieco bardziej na po^ludniu wyrasta samotne,"+
            " wygladaj^ace st^ad na martwe, drzewo.\n";
    }

            if (pora_roku() == MT_ZIMA)
    {
        str = "^Lany wysuszonych przez ch^l^od i s^lo^nce wysokich traw"+
            " porastaj^a ca^l^a wschodni^a cz^e^s^c cmentarza. O tej porze"+
            " wygladaj^a zupe^lnie niczym szare morze ostrych igie^l,"+
            " w^sr^od kt^orych, nieco bardziej na po^ludniu wyrasta samotne,"+
            " wygladaj^ace st^ad na martwe, drzewo.\n";
    }

    return str;
}

string
dlugi_krzaki()
{
    string str;

        if (jest_dzien() == 1)
    {
        str = "Cho^c najbli^zsza okolica pozbawiona jest zieleni, to"+
            " wyra^xnie dostrzegasz g^este krzewy odcinaj^ace si^e od"+
            " czerwieni muru na p^o^lnocy. Mog^a one jednak okaza^c si^e"+
            " ci^e^zkie do przebycia.\n.";
    }

        if (jest_dzien() == 0)
    {
        str = "Cho^c najbli^zsza okolica pozbawiona jest zieleni, to"+
            " w^sr^od mgie^l dostrzegasz g^este krzewy odcinaj^ace si^e od"+
            " ciemnego muru na p^o^lnocy. Mog^a one jednak okaza^c si^e"+
            " ci^e^zkie do przebycia.\n.";
    }
}

string
rozkop_grob()
{
    string str;

    str = "Chyba nie chcesz kopa^c w kamieniu?\n";

    saybb(QCIMIE(TP, PL_MIA) + " rozgl^ada si^e woko^lo z krytyczn^a"+
        " min^a.\n");

    return str;
}


public string
exits_description() 
{
    return "Mi^edzy g^estymi krzakami zauwa^zasz n^edzne zarysy ^scie^zki"+
        " wiod^acej g^l^ebiej w zaro^sla na p^o^lnocy, biegn^ac^a ku"+
        " po^ludniu w^ask^a alejk^e oraz dwa przej^scia na po^lnocny"+
        " i poludniowy wsch^od.\n";
}

/*[specjalne]

zmiana por dnia
zachod slonca
Ostatnie okruchy jasno^sci dnia nikna wraz z zachodz^acym s^lo^ncem, a cmentarzysko nie^spiesznie spowija g^esty cie^n. 
wschod slonca
Pierwsze promienie s^lo^nca przecinaj^a ciemnogranatowe nocne niebo i powoli roz^swietlaja okolice.

OPIS NOCNY
Wyjscia:
w jakimkolwiek kierunku - Znikasz w cieniu.
* znika w cieniu.
* wylania sie z cienia.


*Nagle zza piedestalu dochodzi cie ciche sykniecie.
*Dwa gawrony przysiadly na jednym ze zwalonych drzew.
*Czarny gawron stroszy piora i po chwili zrywa sie do lotu.
*Cien kolumny wedruje wraz z ruchem slonca. 
*Slyszysz czyjes kroki dochodzace od strony alejki, ktore po chwili oddalaja sie, zupelnie tak, jakby ktos zobaczywszy ciebie zawrocil. 
*Szum traw zdaje sie wzmagac, by po chwili ucichnac zupelnie. 

Zdarzenia deszczowe:
- Kolumna ciemnieje pod strugami deszczu.
- Przemokniety wrobel przysiadl na kamiennym pniu w poszukiwaniu schronienia, jednak po chwili odlecial. 
- Gluchy odglos zblizajacej sie burzy przetoczyl sie nad miastem.


Wyjscia:
w-c2 - Przestepujac peknieta plyte jednego z grobow podazasz ku bramie cmentarza.
* nastepuje na peknieta plyte jednego z grobow i niknie w gaszczu kamiennych drzew.
* wylania sie zza kamiennego drzewa.
s-c8 - Ruszasz w strone muru okalajacego cmentarz.
* rusza w strone muru okalajacego cmentarz.
* nadchodzi od strony muru. 
se-c7 - Podazasz w strone samotnego drzewa.
* podaza w strone samotnego drzewa.
* przybywa od strony samotnego drzewa.

OPIS NOCNY

Zdarzenia:
- Nagle zza piedestalu dochodzi cie ciche sykniecie.
- Slyszysz czyjes kroki dochodzace od strony alejki, ktore po chwili cichna.
- Szum traw zdaje sie wzmagac, by po chwili ucichnac zupelnie. 
- Mgly zasnuwaja okolice wilgotnym, szarawym plaszczem.
- Swad palonych swiec miesza sie z wonia wilgotnego kamienia.

Extrasy:
Wyjscia:
w jakimkolwiek kierunku - Znikasz w cieniu.
* znika w cieniu.
* wylania sie z cienia.*/