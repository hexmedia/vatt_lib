
/*By Veli 08.12.09
Wejscie do cmentarnych katakumb - wioska Oxen*/

#pragma strict_types
#include "dir.h"
#include "/d/Standard/Redania/dir.h"
#include <stdproperties.h>
#include <std.h>
#include <mudtime.h>
#include <pogoda.h>
#include <macros.h>
inherit WIOSKA_OXEN_CMENTARZ_STD;

//string krotki_opis();
string dlugi_opis();
string rozkop_mogile();

void
create_cmentarz()
{
    set_short("Pod uschni^etym klonem");
    //set_short(&krotki_opis());
    set_long(&dlugi_opis());
    add_prop(ROOM_I_INSIDE, 0);
    /*add_object();
    dodaj_rzecz_niewyswietlana("",1); - moze tu cos bedzie.*/
    //add_item(({"krzaki","zaro^sla"}),&dlugi_krzaki);
    //add_item("trawy",&dlugi_trawy);
    add_item("alejk^e","Przesmyk mi^edzy masywnymi grobowcami majacz^acymi"+
        " na po^ludniu.\n");
    add_sit("pod kolumn^a","na piedestale pod kolumn^a","spod kolumny",4);
    add_event("Z muru zrywa si^e nagle stadko wron i z dono^snym"+
        " krakaniem odlatuje w stron^e rzeki.\n");
    add_event("Z uschni^etego krzaka zerwa^l si^e jaki^s ptak.\n");//noc
    add_event("Jeden z wisz^acych na ga^l^ezi sznurk^ow deliktnie otar^l"+
        " si^e o twoje czo^lo.\n");
    add_event("Po d^lu^zszej chwili dociera do ciebie, ^ze szum traw usta^l"+
        " zupe^lnie.\n");
    add_event("Konary uschni^etego klonu trzeszcz^a ostrzegawczo.\n");
    add_event("Cienka ^swieca przewraca si^e na bok i natychmiast ga^snie.\n");
    add_event("Przez kilka chwil zdawa^lo ci si^e, ^ze wrony wcale nie ^spia"+
        " i patrz^a prosto na ciebie.\n"); 
    add_event("Nagle cisz^e przerywa przeci^ag^le westchnienie. Co^z, pewnie"+
        " to tylko wiatr...\n");
    add_event("Mg^ly zasnuwaj^a okolic^e wilgotnym, szarawym plaszczem.\n");
    add_exit(WIOSKA_OXEN_LOKACJE+"cmentarz/c_6",({"n","cos","cos"}));
    add_exit(WIOSKA_OXEN_LOKACJE+"cmentarz/c_5",({"nw","cos","cos"}));
    add_exit(WIOSKA_OXEN_LOKACJE+"cmentarz/c_8",({"sw","cos","cos"}));
    //wyjscia zale^zne od warunk^ow - patrz na ko^ncu
    add_cmd_item("mogi^la", "rozkop", rozkop_mogile, "Rozkop co?\n");
}

/*string
krotki_opis()
{

    string str;

    if (jest_dzien() == 1)
    {
        str = "Pod uschni^etym klonem";
    }

    if (jest_dzien() == 0)
    {
        str = "Po^sr^od gasn^acych ^swiec\n";
    }

    return str;

}*/

string 
dlugi_opis()
{
    string str;

    if (jest_dzien() == 1)
    {

        str = "Zewsz^ad jak okiem siegn^a^c otacza ci^e las kamiennych"+
            " drzew, wyrastaj^acy dooko^la zlamanej marmurowej kolumy."+
            " Niekt^ore wyzieraj^a nie^smia^lo z g^estych zaro^sli pod"+
            " p^o^lnocn^a cz^e^sci^a muru, inne za^s ton^a w morzu wysokich"+
            " traw porastaj^acych wschodnie po^lacie cmentarzyska. Sama"+
            " kolumna, ustawiona na niewielkim piedestale zdaje si^e z"+
            " jakiego^s powodu stanowi^c centrum ca^lego cmentarza,"+
            " jednocze^snie nad nim g^oruj^ac. Aura w tym miejscu nie"+
            " przypomina tej, jaka unosi si^e w innych cz^e^sciach, by^c"+
            " mo^ze powodem jest niezwyk^le pod^lo^ze wy^lo^zone du^zymi"+
            " p^lytami z kamienia, tak ^ze nie masz szansy dotkn^a^c go^lej"+
            " ziemi. Przygl^adaj^ac si^e im bli^zej dochodzisz do wniosku,"+
            " ^ze niew^atpliwie znajdujesz si^e w najstarszym miejscu"+
            " poch^owku w okolicy, jednak nie widza^c ^zadnych napis^ow ni"+
            " symboli nie jeste^s w stanie stwierdzi^c, kogo lub co"+
            " upamietniaj^a 'zwalone drzewa'. Ktokolwiek to by^l,"+
            " prawdopodobnie odszed^l na d^lugo przed dniem twoich urodzin.\n";
    }

    if (jest_dzien() == 0)
    {
        str = "Zewsz^ad jak okiem siegn^a^c w ciemno^sci ja^snieje las"+
            " kamiennych drzew, wyrastaj^acy dooko^la z^lamanej marmurowej"+
            " kolumy. Niekt^ore wyzieraj^a nie^smia^lo z g^estych zaro^sli"+
            " pod p^o^lnocna cz^e^sci^a muru, inne za^s ton^a w morzu"+
            " wysokich traw i mgie^l na wschodzie. Sama kolumna, ustawiona"+
            " na niewielkim piedestale zdaje si^e z jakiego^s powodu"+
            " stanowi^c centrum ca^lego cmentarza, jednocze^snie nad nim"+
            " g^oruj^ac. Aura w tym miejscu nie przypomina tej, jaka unosi"+
            " si^e w innych czesciach ^zalnika, by^c mo^ze powodem jest"+
            " niezwyk^le pod^lo^ze wylozone duzymi plytami z kamienia, tak"+
            " ^ze nie masz szansy dotkn^a^c go^lej ziemi. Przygl^adaj^ac"+
            " si^e im bli^zej dochodzisz do wniosku, ^ze niew^atpliwie"+
            " znajdujesz si^e w najstarszym miejscu poch^owku w okolicy,"+
            " jednak nie widz^ac ^zadnych napis^ow ni symboli nie jeste^s w"+
            " stanie stwierdzi^c, kogo lub co upamietniaj^a 'zwalone drzewa'."+
            " Ktokolwiek to by^l, prawdopodobnie odszed^l dawno temu...\n";
    }

    return str;
}


string
rozkop_mogile()//tylko w nocy, tylko z lopata do poprawki
{
    string str;

    str = "Chyba nie chcesz tego robic w bialy dzien?\n";

    saybb(QCIMIE(TP, PL_MIA) + " rozgl^ada si^e woko^lo z krytyczn^a"+
        " min^a.\n");

/*
rozkop grob
Nagle cisze przerywa przeciagle westchnienie dochodzace jakby z kazdej strony, a kilka swiec natychmiast gasnie! Cichy pisk przeszywa powietrze, cienie zaczynaja wirowac wokol ciebie zlewajac sie w jedna calosc...
op 2 sek.
Trawy uginaja sie, jakby pod naporem czyichs krokow, a mgla rozstepuje sie przepuszczajac cos ledwie widzialnego, zblizajacego sie ku tobie...
op 1 sek.
JAKIS POTWOR - CIEN, ZJAWA, COS INNEGO pojawia sie przed toba!*/

    return str;
}


public string
exits_description() 
{
    return "Prowadzi st^ad kilka ^scie^zek. Jedna biegn^aca pod murem na"+
        " po^lnoc, oraz dwie rozchodz^ace si^e ku centrum cmentarz"+
        " na po^lnocny i po^ludniowy zach^od.\n";
}

/*[specjalne]

zmiana por dnia
zachod slonca
Ostatnie okruchy jasno^sci dnia nikna wraz z zachodz^acym s^lo^ncem, a cmentarzysko nie^spiesznie spowija g^esty cie^n. 
wschod slonca
Pierwsze promienie s^lo^nca przecinaj^a ciemnogranatowe nocne niebo i powoli roz^swietlaja okolice.

OPIS NOCNY
Wyjscia:
w jakimkolwiek kierunku - Znikasz w cieniu.
* znika w cieniu.
* wylania sie z cienia.

[lokacja]
c7
[short]
Pod uschnietym klonem
[long]
Szumiace lany traw porastajacych tak obficie to miejsce gdzieniegdzie skrywaja jakas pojedyncza mogile, otulajac ziemie klujacym dywanem. Porozrzucane w nieladzie kamienie, byc moze szczatki jakichs pomnikow mieszaja sie z kawalkami drewna, zapewne bedacymi niegdys czescia jakiejs konstrukcji. Wszystko to zdaje sie koncentrowac wokol jedynego drzewa, jakie rosnie na cmentarzu. Podchodzac do niego blizej zauwazasz, iz jest ono calkiem uschniete, zas cien rzucany przez mur dziwnie sie poglebia. Jedynym dzwiekiem zdaje sie byc donosne i, z niewiadomego powodu niezwykle drazniace krakanie wron obsiadujacych drzewo. Cale miejsce owiewa jakas zagadkowa, niepokojaca aura, podsycana jeszcze bardziej przez setki naczyn z lojem oraz zwyklych swiec poustawianych w roznych miejscach. Czesc z nich wyglada na swiezo zapalona, lecz wiele wlasnie dogasa pograzajac to miejsce w cieniu, ktorego raczej nic nie jest w stanie rozmyc.
[itemy]
:mogily/groby:
Sposrod tych ledwie kilku mogil jestes w stanie wyroznic tylko jedna, bo najlepiej zachowana. Jest to kopiec o wielkosci doroslego czlowieka, jednak nic wiecej nie jestes w stanie powiedziec, grob bowiem pozbawiony jest czegokolwiek, co mogloby pomoc w jego identyfikacji.
:drzewo/klon:
Stary, calkiem juz martwy klon, zupelnie pozbawiony lisci. Zblizajac sie do pnia czujesz, jak niepokojace wrazenie nieco sie wzmaga, a gdy zadzierasz glowe twym oczom ukazuja sie skrawki brudnego materialu oraz kilka zerwanych sznurkow powiewajacych swobodnie na wietrze. Nigdzie jednak nie zauwazasz zadnej tabliczki, ni chociazby jakiegokolwiek znaku, zupelnie tak, jakby mieszkancy obawiali sie czegos i woleli nie zaklocac za bardzo tej przestrzeni. Sadzac po rozmieszczeniu grobow, nikt nie chce chowac swoich bliskich w okolicy, byc moze wiec drzewo jest przeklete i lepiej nie sprawdzac, czy chocby stanie pod nim przynosi pecha... 
:swiece/naczynia:
Cale setki swiec oraz naczyn z lojem poustawiano w kazdym mozliwym zakamarku - na kamieniach, pomiedzy kawalkami drewna, a takze pod samym drzewem, choc akurat tam jest ich ledwie kilka. Najprawdopodobniej nie sprzatano ich od lat; po prostu te wypalone staja sie podstawa dla nowych. Sprawia to wrazenie, jakby mieszkancy czuli sie poniekad zobowiazani do palenia ich w tym miejscu, byc moze z obawy przed czyms lub kims...

OPIS NOCNy
short: Posrod gasnacych swiec
long: Szumiace lany traw porastajacych obficie to miejsce tona w morzu mdlawego swiatla, ktore w polaczeniu z mgla tworzy swego rodzaju mleczna osnowe. Choc widocznosc jest przez to znacznie ograniczona, to jednak jestes w stanie dostrzec, iz zrodlem owego swiatla sa setki naczyn z lojem oraz zwyklych swiec poustawianych w roznych miejscach. Czesc z nich wyglada na zapalona calkiem niedawno, lecz wiele wlasnie dogasa pograzajac to miejsce w cieniu, ktory zdaje sie pochlaniac, poza swiatlem, takze jakiekolwiek dzwieki. Jedyne szelesty zaklocajace te nieprzyjemna cisze pochodza od strony uschnietego drzewa, zupelnie tak, jakby to ono mialo wylacznosc na swobodne przebywanie w tej okolicy, a stadko halasliwych wron obsiadujacych jego konary teraz jest calkowicie pograzone we snie. Choc moze tylko pozornie... Bo przeciez dziwna atmosrefa otulajaca to miejsce nie jest jedynie iluzja i prawdopodobnie wiecej w niej energii, niz mogloby sie zdawac. 
ob groby/mogily
Sposrod mgiel wyzieraja kontury jedynej mogily, jaka jestes w stanie zauwazyc, nie widzisz jednak nic wiecej. 
ob drzewo/klon
Stary, calkiem juz martwy klon, zupelnie pozbawiony lisci. Zblizajac sie do pnia czujesz, jak niepokojace wrazenie nieco sie wzmaga, a gdy zadzierasz glowe twym oczom ukazuja sie skrawki brudnego materialu oraz kilka zerwanych sznurkow powiewajacych swobodnie na wietrze. W ciemnosci nie zauwazasz jednak zadnej tabliczki, ni chociazby jakiegokolwiek znaku, zas czerniejaca kora zdaje sie ostrzegac przed zbytnim zaklocaniem tej przestrzeni.
ob swiece/naczynia
Cale dziesiatki swiec oraz naczyn z lojem poustawiano w kazdym mozliwym zakamarku - przynajmniej tyle jestes w stanie dostrzec poprzez przedziwna mieszanine mgly, duszacego dymu oraz slabego swiatla. Wiekszosc z nich, prawdopodobnie palona tylko za dnia zdaje sie wlasnie dogasac pograzajac okolice w coraz glebszym cieniu.*/


 

