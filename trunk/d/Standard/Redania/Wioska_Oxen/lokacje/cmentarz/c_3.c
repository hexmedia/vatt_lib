
/*By Veli 08.12.09
Zaro^sla na cmentarzu - wioska Oxen*/

#pragma strict_types
#include "dir.h"
#include "/d/Standard/Redania/dir.h"
#include <stdproperties.h>
#include <std.h>
#include <mudtime.h>
#include <pogoda.h>
#include <macros.h>
inherit WIOSKA_OXEN_CMENTARZ_STD;

//string krotki_opis();
string dlugi_opis();
string rozkop_grob();
string dlugi_berberys();
int zerwij_galazke(string str);

void
create_cmentarz()
{
    //set_short(&krotki_opis());
    set_short("Zaro^sni^ety cmentarz");
    set_long(&dlugi_opis());
    add_prop(ROOM_I_INSIDE, 0);
    /*add_object();
    dodaj_rzecz_niewyswietlana("",4); - moze tu cos bedzie.*/
    add_item(({"krzaki","krzewy","ro^slinno^s^c"}),"Najr^o^zniejsze odmiany"+
        " krzak^ow, zasadzone niegdy^s r^ek^a odwiedzaj^acych cmentarz, z"+
        " czasem wyrwa^ly si^e spod kontroli i karmione sokami ziemi"+
        " zdzik^ly kompletnie. G^aszczem kolczastych i kwitn^acych ga^l^ezi"+
        " splataly sie ze soba tworzac trudny do przebycia mur i niemal"+
        " zupe^lnie zakry^ly ubogie groby.\n");
    add_item("groby","Gdzieniegdzie po^sr^od ro^slinno^sci wyzieraj^a"+
        " niewielkie figurki i porozbijane p^lyty grob^ow. S^adz^ac po"+
        " jako^sci wykonania jak i materiale, ta cz^e^s^c cmentarza musi"+
        " by^c zarezerwowana dla najubo^zszej spo^leczno^sci. Cho^c zdaje"+
        " si^e, ^ze niezbyt dba ona o rodzaj jak te^z i kompletno^s^c"+
        " poch^owku. Czasami zd^arzy ci si^e ujrze^c po^sr^od krzak^ow"+
        " nieco bardziej zadban^a mogi^l^e jednak czestszym widokiem jest"+
        " niewielki kopczyk, skrywaj^acy ledwie przysypane ziemi^a zw^loki."+
        " Oczywi^scie mo^zna ten fakt t^lumaczy^c do^s^c wysok^a"+
        " ^smiertelno^sci^a w okolicy... Jednak tak czy owak, wszystko"+
        " wskazuje na to, ^ze znajdujesz si^e na swego rodzaju ^smietnisku"+
        " cmentarnym.\n");
    add_item(({"^scie^zk^e","dr^o^zk^e"}),"W^aska, ledwie wydeptana ^scie^zka"+
        " wije si^e w g^aszczu berberys^ow i grob^ow prowadz^ac spod bramy"+
        " w coraz to g^l^ebsze zaro^sla na p^o^lnocy.\n");
    add_item(({"trupa","cia^lo","zw^loki"}),"Kt^orego trupa? Z chwila gdy"+
        " przygl^adasz sie uwa^zniej ziemi zauwa^zasz wi^ecej oznak niczym"+
        " nie zakrytego rozk^ladu - kawa^lki ko^sci, resztki ubra^n i"+
        " ludzkie cz^lonki niedostatecznie dobrze zasypane piaskiem..."+
        " Zw^loki pod berberysami zdaj^a si^e by^c najlepiej zachowane i"+
        " s^adz^ac po odorze stosunkowo naj^swie^zsze. Po zakrwawionym"+
        " stroju wnioskujesz, ^ze najprawdopodobniej by^la to kobieta, z"+
        " kt^orej teraz zosta^la tylko milcz^aca padlina z szeroko"+
        " rozp^latanym podbrzuszem.\n");
    add_item("berberysy",&dlugi_berberys());
    add_sit("pod krzakiem","pod krzakiem","spod krzaka",6);
    add_event("Z uschni^etego krzaka zerwa^l si^e jaki^s ptak.\n");//NOC
    add_event("Ga^l^azka akacji uderzy^la ci^e w twarz pozostawiaj^ac na"+
        " niej cieniutk^a krwist^a rys^e.\n");//DZIEN
    add_event("Nadeptujesz na co^s niezwykle mi^ekkiego, ledwie przysypanego"+
        " ziemi^a...\n");
    add_event("Rozro^sni^ete berberysy bezustannie zahaczaj^a kolcami o"+
        " twoje ubranie.\n");
    add_event("S^laby wiatr zawia^l od wschodu, nieco ^lagodz^ac zaduch.\n");
    add_event("Strugi deszczu zmieniaj^a si^e w ciemn^e ^scian^e wody, prawie"+
        " ca^lkiem ograniczaj^ac widoczno^s^c.\n");//DESZCZ
    add_event("Ciemnoszare chmury przesuwaj^a si^e nad wzg^orzem.\n");//DZESZCZ
    add_event("G^luchy odg^los zbli^zaj^acej si^e burzy przetoczy^l si^e"+
        " nad wzg^orzem.\n");//DESZCZ 
    add_event("Szalej^acy wiatr wyje upiornie smagaj^ac bezlito^snie"+
        " g^estwiny krzew^ow.\n");//NOC
    add_event("Powietrze przesycone jest wilgoci^a i md^lym zapachem"+
        " kwitn^acych berberys^ow.\n");//WIOSNA DESZCZ
    add_event("Niechc^acy ocierasz si^e o jeden z krzew^ow i natychmiast"+
        " spada na ciebie deszcz ci^e^zkich kropel.\n");//LATO 
    add_event("Mdl^acy zapach dzikiego bzu unosi si^e w powietrzu.\n");//WIOSNA
    add_exit(WIOSKA_OXEN_LOKACJE+"cmentarz/c_4",({"ne","cos","cos"}));
    add_exit(WIOSKA_OXEN_LOKACJE+"cmentarz/c_2",({"sw","cos","cos"}));
    //wyjscia zale^zne od warunk^ow - patrz na ko^ncu
    //wyjscie do wioski - brama - patrz na koncu
    
    add_cmd_item("gr^ob", "rozkop", rozkop_grob, "Rozkop co?\n");
    add_cmd_item("ga^l^azk^e", "zerwij", zerwij_galazke, "Zerwij co?\n");
}

/*string
krotki_opis()
{

    string str;

    if (jest_dzien() == 1)
    {
        str = "Zaro^sni^ety cmentarz\n.";
    }

    if (jest_dzien() == 0)
    {
        str = "Ciemne zasro^sla\n";
    }

    return str;

}*/

string 
dlugi_opis()
{
    string str;

    if (jest_dzien() == 1)
    {

    str = "Zag^l^ebiaj^ac si^e coraz bardziej w labirynt grob^ow i"+
        " zdzicza^lych krzak^ow docierasz... W^la^sciwie to nie docierasz"+
        " nigdzie, bowiem nat^lok cmentarnej ro^slinno^sci zdaje si^e ani"+
        " odrobin^e nie rzedn^a^c. Ledwie widzisz zarysy ^scie^zki, jak^a"+
        " przysz^lo ci si^e przedziera^c, nie wspominaj^ac o dr^o^zce"+
        " wij^acej si^e w kierunku p^o^lnocy. Chyba, ^ze wolisz ju^z teraz"+
        " zawr^oci^c...? Panuj^acy tu spok^oj zdaje si^e w jaki^s spos^ob"+
        " ochrania^c i lagodzi^c zm^eczenie zgie^lkiem codzienno^sci.";
  
    if (pora_roku() == MT_WIOSNA)
    {
        str += "By^c mo^ze to powietrze przepe^lnione woni^a, nasi^akni^etej"+
            " wod^a, budz^acej si^e do cyklu ziemi, ";
    }

        if (pora_roku() == MT_LATO)
    {
        str += "By^c mo^ze to powietrze przepe^lnione woni^a wygrzanej"+
            " ziemi rozbuchanej pe^lni^a ^zycia i rozkwitu, ";
    }

            if (pora_roku() == MT_JESIEN)
    {
        str += "By^c mo^ze to powietrze przepe^lnione cie^zk^a woni^a"+
            " obumieraj^acej, gnij^acej przebrzmia^l^a zieleni^a ziemi, ";
    }

            if (pora_roku() == MT_ZIMA)
    {
        str += "By^c mo^ze to powietrze przepe^lnione gryz^acym ch^lodem"+
            " zion^acym od zalegaj^acych wsz^edzie zwa^l^ow sniegu, ";
    }

        str += " a by^c mo^ze widok podniszczonych, zatopionych w g^estych"+
            " krzakach grob^ow sprawia, ^ze cisza nie jest tu ju^z tak"+
            " dra^zni^aca... Cisza ta jednak niepokoi, wibruje w twym"+
            " umy^sle wprowadzaj^ac niepewno^s^c i poczucie zagubienia. Ona"+
            " i oblapiajacy ci^e zewsz^ad sza^l wybuja^lej ro^slinno^sci"+
            " powoduj^a, i^z z ka^zd^a chwil^a atakuj^a ci^e gor^aczkowe"+
            " wizje i pod^swiadome l^eki. Cierpki zapach czego^s, o czym"+
            " lepiej w tym miejscu nie my^sle^c miesza si^e";

    if (pora_roku() == MT_WIOSNA)
    {
        str += " z ostr^a woni^a kwitn^acych krzew^ow i zdaje si^e wzmaga^c"+
            " przy grupce purpurowych berberys^ow.\n";
    }

        if (pora_roku() == MT_LATO)
    {
        str += " z ostr^a woni^a kwitn^acych krzew^ow i zdaje si^e wzmaga^c"+
            " przy grupce purpurowych berberys^ow.\n";
    }

            if (pora_roku() == MT_JESIEN)
    {
        str += " z ostr^a woni^a gnij^acych li^sci i zdaje si^e wzmaga^c"+
            " przy grupce obumieraj^acych berberys^ow.\n";
    }

            if (pora_roku() == MT_ZIMA)
    {
        str += " z ostr^a woni^a brudnego ^sniegu, butwiej^acych szcz^atk^ow"+
            " i pozosta^lo^sci gnij^acych li^sci i zdaje si^e wzmaga^c"+
            " przy grupce kolczastych, bezlistnych berberys^ow.\n";
    }
    }

    if (jest_dzien() == 0)
    {
        str = "Zag^l^ebiaj^ac si^e coraz bardziej w labirynt grob^ow i"+
        " zdzicza^lych krzak^ow docierasz... W^la^sciwie to nie docierasz"+
        " nigdzie, bowiem nat^lok cmentarnej ro^slinno^sci zdaje si^e ani"+
        " odrobin^e nie rzedn^a^c.Wszechobecna ciemno^s^c w po^l^aczeniu z"+
        " wybuja^lymi krzewami, kt^orych na dodatek nie widzisz, a jedynie"+
        " czujesz sprawia, ze miejsce to zdaje si^e poch^lania^c wszelkie,"+
        " nawet najmarniejsze ^swiat^lo. Kolczaste rosliny wci^a^z"+
        " ocieraj^a si^e o twoje ubranie, wczepiaj^ac si^e niekiedy w"+
        " sk^or^e oraz w^losy. Od czasu do czasu swiat^lo ksie^zyca"+
        " o^swietla kontury jakich^s mogi^l, jednak dostrze^zenie"+
        " jakiejkolwiek ^scie^zki czy drogi jest w tych warunkach"+
        " niemo^zliwe. Panuj^aca tu nieprzerwanie cisza niepokoi, wibruje"+
        " w twym umy^sle wprowadzaj^ac niepewnosc i poczucie zagubienia. Ona"+
        " i oblapiajacy ci^e zewsz^ad sza^l wybuja^lej ro^slinno^sci"+
        " powoduj^a, i^z z ka^zda chwil^a atakuj^a ci^e goraczkowe wizje i"+
        " pod^swiadome l^eki. Cierpki zapach czegos, o czym lepiej w tym"+
        " miejscu nie my^sle^c miesza si^e";

    if (pora_roku() == MT_WIOSNA)
    {
        str += " z ostr^a woni^a kwitn^acych krzew^ow i zdaje si^e wzmaga^c"+
            " przy grupce purpurowych berberys^ow.\n";
    }

        if (pora_roku() == MT_LATO)
    {
        str += " z ostr^a woni^a kwitn^acych krzew^ow i zdaje si^e wzmaga^c"+
            " przy grupce purpurowych berberys^ow.\n";
    }

            if (pora_roku() == MT_JESIEN)
    {
        str += " z ostr^a woni^a gnij^acych li^sci i zdaje si^e wzmaga^c"+
            " przy grupce obumieraj^acych berberys^ow.\n";
    }

            if (pora_roku() == MT_ZIMA)
    {
        str += " z ostr^a woni^a brudnego ^sniegu, butwiej^acych szcz^atk^ow"+
            " i pozosta^lo^sci gnij^acych li^sci i zdaje si^e wzmaga^c"+
            " przy grupce kolczastych, bezlistnych berberys^ow.\n";
    }
    }

    return str;
}

string 
dlugi_berberys()
{

    string str;

    str = "Zdzicza^le z braku wystarczaj^acej ilo^sci ^swiat^la krzewy"+
    " odcinaj^a si^e od";
    
        if (pora_roku() == MT_WIOSNA)
    {
        str += " zielonych ro^slin swymi purpurowymi, drobnymi listkami i"+
            " delikatnymi ale niezwykle ostrymi kolcami. Na kilku"+
            " ga^l^azkach, tych najbli^zej gruntu zauwa^zasz pierwsze"+
            " p^aczki blado^z^o^ltych kwiat^ow bezwiednie opadaj^acych na"+
            " zasinia^ly i oblepiony zakrzep^l^a krwi^a policzek"+
            " zmasakrowanego trupa.\n";
    }

        if (pora_roku() == MT_LATO)
    {
        str += " zielonych ro^slin swymi purpurowymi, drobnymi listkami i"+
            " delikatnymi ale niezwykle ostrymi kolcami. Na kilku"+
            " ga^l^azkach, tych najbli^zej gruntu zauwa^zasz p^aczki"+
            " blado^z^o^ltych kwiat^ow bezwiednie opadaj^acych na zasinia^ly"+
            " i oblepiony zakrzep^l^a krwi^a policzek zmasakrowanego trupa.\n";
    }

            if (pora_roku() == MT_JESIEN)
    {
        str += " zgni^lozielonych ro^slin swymi purpurowymi listkami i"+
            " delikatnymi ale niezwykle ostrymi kolcami. Na kilku"+
            " ga^l^azkach, tych najbli^zej gruntu zauwa^zasz przegnite"+
            " resztki blado^z^o^ltych kwiat^ow bezwiednie opadaj^acych na"+
            " zasinia^ly i oblepiony zakrzep^l^a krwi^a policzek"+
            " zmasakrowanego trupa.\n";
    }

            if (pora_roku() == MT_ZIMA)
    {
        str += " bieli ^sniegu swymi zdrewnia^lymi, czarnymi galeziami i"+
            " delikatnymi ale niezwykle ostrymi kolcami. Kilka ga^l^azek, "+
            " tych najbli^zej gruntu z^lama^lo si^e pod zwa^lami"+
            " zalegaj^acego ^sniegu opadaj^ac bezwiednie na zasinia^ly i"+
            " oblepiony zakrzep^l^a krwi^a policzek zmasakrowanego trupa.\n";
    }    

    return str;
}

string 
rozkop_grob()
{
    string str = "";

    if (jest_dzien() == 1)
    {
        str += "Przecie^z cia^la le^z^a prawie na wierzchu!\n.";
    }

    if (jest_dzien() == 0)
    {
        str += "W takiej ciemnicy i ciasnocie, gdy co chwila potykasz si^e"+
            " z o w^lasne nogi, gal^ezie i bogowie racz^a wiedzie^c co jeszcze"+
            " srednio da si^e kopa^c.\n";
    }

    saybb(QCIMIE(TP, PL_MIA) + " rozgl^ada si^e woko^lo z krytyczn^a"+
        " min^a.\n");

    return str;
}

void init()
{
    ::init();
    add_action(zerwij_galazke, "zerwij ga^l^azk^e");
}

int 
zerwij_galazke(string str)
{

    if(query_verb() == "zerwij")
       notify_fail("Zerwij ga^l^azk^e berberysu?\n");

    if(!HAS_FREE_HANDS(TP))
    {
       write("Musisz mie^c obie d^lonie wolne, aby m^oc to zrobi^c.\n");
       return 1;
    }

    write("Ostro^znie zrywasz kolczast^a ga^l^azk^e z krzaka berberysu.\n");
    saybb(QCIMIE(TP, PL_MIA) + " ostro^znie zrywa kolczast^a ga^l^azk^e z"+
        " krzaka berberysu.\n");

    clone_object(WIOSKA_OXEN_PRZEDMIOTY+"obiekty/galazka_berberys")->move(TP);

        return 1;
}

public string
exits_description() 
{
    return "Mi^edzy g^estymi krzakami zauwa^zasz n^edzne zarysy ^sciezki"+
        " wiod^acej g^l^ebiej w zaro^sla na polnocnym wchodzie i"+
        " po^ludniowym zachodzie.\n";
}

/*[specjalne]
zmiana por dnia
zachod slonca
Ostatnie okruchy jasno^sci dnia nikna wraz z zachodz^acym s^lo^ncem, a cmentarzysko nie^spiesznie spowija g^esty cie^n. 
wschod slonca
Pierwsze promienie s^lo^nca przecinaj^a ciemnogranatowe nocne niebo i powoli roz^swietlaja okolice.

Wyjscia:
sw-c2 - Z trudem zaglebiasz sie w zarosla.
* z trudem zaglebia sie w zarosla.
* wylania sie z zarosli.
ne-c4 - Z trudem zaglebiasz sie w zarosla.
* z trudem zaglebia sie w zarosla.
* wylania sie z zarosli.
se-c5 - Z trudem zaglebiasz sie w zarosla.
* z trudem zaglebia sie w zarosla.
* wylania sie z zarosli.

OPIS NOCNY
Wyjscia:
w jakimkolwiek kierunku - Znikasz w cieniu.
* znika w cieniu.
* wylania sie z cienia.*/
