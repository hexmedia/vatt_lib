
/*By Veli 08.12.09
Wejscie do cmentarnych katakumb - wioska Oxen*/

#pragma strict_types
#include "dir.h"
#include "/d/Standard/Redania/dir.h"
#include <stdproperties.h>
#include <std.h>
#include <mudtime.h>
#include <pogoda.h>
#include <macros.h>
inherit WIOSKA_OXEN_CMENTARZ_STD;

//string krotki_opis();
string dlugi_opis();
string rozkop_grob();
//int odgarnij_mech(string str);

void
create_cmentarz()
{
    set_short("Pod kamiennym ^lukiem");
    //set_short(&krotki_opis());
    set_long(&dlugi_opis());
    add_prop(ROOM_I_INSIDE, 0);
    /*add_object();
    dodaj_rzecz_niewyswietlana("",1); - moze tu cos bedzie.*/
    add_item(({"krzaki","krzewy","ro^slinno^s^c"}),"Wszechobecna"+
        " ro^Slinno^s^c szczelnie otula kamienny postument, po ^srodku"+
        " kt^orego si^e znajdujesz, jednak krzaki zdaj^a si^e rzedn^a^c"+
        " od wschodniej i po^ludniowej strony.\n");
    add_item("^luk","Rze^xbiony w kamieniu ^luk, szeroki na niemal trzy"+
        " metry bez w^atpienia wyszed^l spod reki elfiego rzemie^slnika."+
        " Po licznych p^eknieciach, grubej warstwie mchu porastajacej"+
        " kamie^n i cz^e^sciowo zatartych symbolach dochodzisz do wniosku,"+
        " i^z konstrukcja ta jest z pewno^sci^a bardzo wiekowa.\n");
    add_item("kolumn^e","Przysloni^eta przez zdzicza^le krzewy kolumna"+
        " widnieje gdzie^s na po^ludniu. Rozgladaj^ac si^e wok^o^l"+
        " spostrzegasz, i^z nawet symbole wyryte na ^luku s^a skierowane"+
        " w jej stron^e.\n");
    add_item("mur","Niski ceglany mur odgradzaj^acy zbocza wzniesienia od"+
        " brzeg^ow kapry^snego Pontaru niemal ca^lkowicie porasta fioletowy"+
        " bluszcz g^esto pl^acz^acy si^e z kolczastymi krzewami.\n");
    add_item("symbole","Cho^c zatarte przez czas, deszcze, wiatry oraz wiele"+
        " innych, by^c mo^ze tysi^ackro^c gorszych rzeczy, symbole starszych"+
        " run wci^a^z jeszcze mo^zna rozpozna^c.\n");
    add_sit("na o^ltarzu","na p^ltarzu","z o^ltarza",4);
    add_event("Mdl^acy zapach dzikiego bzu unosi si^e w powietrzu.\n");//WIOSNA
    add_event("Ma^ly wr^obel przysiad^l na chwil^e na o^ltarzu, po"+
        " czym oczy^sciwszy sobie skrzyde^lko odlecia^l.\n");//DZIEN
    add_event("Z muru zrywa si^e nagle stadko wron i z dono^snym"+
        " krakaniem odlatuje w stron^e rzeki.\n");//NOC
    add_event("Z uschni^etego krzaka zerwa^l si^e jaki^s ptak.\n");//NOC
    add_exit(WIOSKA_OXEN_LOKACJE+"/cmentarz/c_3",({"sw","cos","cos"}));
    add_exit(WIOSKA_OXEN_LOKACJE+"/cmentarz/c_5",({"s","cos","cos"}));
    add_exit(WIOSKA_OXEN_LOKACJE+"/cmentarz/c_6",({"se","cos","cos"}));
    //wyjscia zale^zne od warunk^ow - patrz na ko^ncu
    add_cmd_item("gr^ob", "rozkop", rozkop_grob, "Rozkop co?\n");
}

/*string krotki_opis()
{

    string str;

    if (jest_dzien() == 1)
    {
        str = "Pod kamiennym ^lukiem\n.";
    }

    if (jest_dzien() == 0)
    {
        str = "W cieniu kamiennego ^luku\n";
    }

    return str;

}*/

string 
dlugi_opis()
{
    string str;

    if (jest_dzien() == 1)
    {

        str = "Wci^az uparcie walcz^ac z pl^atanin^a zdzicza^lych krzew^ow"+
        " bezlito^snie atakuj^acych tw^a odzie^z, starasz si^e znale^s^c"+
        " jakie^s w miar^e wolne od niedogodno^sci miejsce, z kt^orego"+
        " mo^zna si^e sensownie rozejrze^c po okolicy. Jedynie tu^z przy"+
        " niskim murze otaczaj^acym wzg^orze mo^zna stan^ac swobodnie."+
        " Uwag^e przykuwa teraz, wyrastaj^acy tu^z nad toba szeroki ^luk"+
        " oparty na czterech kolumnach, tworz^acy tu swego rodzaju"+
        " sklepienie. Zdaje si^e, ^ze znajdujesz si^e w czym^s w rodzaju"+
        " kaplicy, gdy^z pod^lo^ze w obr^ebie ^luku wy^lo^zone jest"+
        " kamieniem, nieopodal za^s znajduje si^e granitowe podwy^zszenie,"+
        " do z^ludzenia przypominaj^ace o^ltarz. K^atem oka dostrzegasz"+
        " porzucon^a w nie^ladzie p^lyt^e, zapewne skradzion^a z jakiego^s"+
        " grobowca. Co dziwne, panuje tu wi^ekszy porz^adek, cho^c"+
        " ro^slinno^s^c jest nie mniej zdzicza^la, a brak jakichkolwiek"+
        " grob^ow w najbli^zszej okolicy nie oznacza wcale, ^ze znik^ad"+
        " nie wyziera biel ko^sci czy te^z zniszczone cz^e^sci garderoby.";
    }

    if (jest_dzien() == 0)
    {
        str = "OPIS NOCNY";
    }

    return str;
}


string
rozkop_grob()
{
    string str;

    str = "Jak okiem siegnac nie widac zdatnej do tego celu mogily.\n";

    saybb(QCIMIE(TP, PL_MIA) + " rozgl^ada si^e woko^lo z krytyczn^a"+
        " min^a.\n");

    return str;
}


public string
exits_description() 
{
    return "Mi^edzy g^estymi krzakami zauwa^zasz n^edzne zarysy ^scie^zek"+
        " wiod^acych g^l^ebiej w zaro^sla na po^ludniowym zachodzie,"+
        " po^ludniowym wchodzie i po^ludniu.\n";
}

/*[specjalne]

zmiana por dnia
zachod slonca
Ostatnie okruchy jasno^sci dnia nikna wraz z zachodz^acym s^lo^ncem, a cmentarzysko nie^spiesznie spowija g^esty cie^n. 
wschod slonca
Pierwsze promienie s^lo^nca przecinaj^a ciemnogranatowe nocne niebo i powoli roz^swietlaja okolice.

przesun plyte (zbyt slaby)
Pewnie lapiesz za uchwyt i ciagniesz z calej sily probujac przesunac plyte, jednak po chwili ze zdziwieniem zdajesz sobie sprawe, ze ciezki marmur nawet nie drgnal.
* lapie za uchwyt i z pewna mina ciagnie probujac przesunac plyte, jednak dopiero chwile pozniej ze zmieszaniem spostrzega, iz ciezki marmur nawet nie drgnal.
przesun plyte (odpowiednia sila)
Pewnie lapiesz za uchwyt i ciagniesz z calej sily, a plyta ze zgrzytem przesuwa sie, odslaniajac ziejace ciemnoscia zejscie do katakumb. 
opoznienie 2 sek.
Cieplawe powietrze stopniowo rozchodzi sie zatruwajac swa zatechla wonia najblizsza okolice.
* pewnie lapie za uchwyt i ciagnie z calej sily, a plyta ze zgrzytem przesuwa sie, odslaniajac ziejace ciemnoscia zejscie do katakumb.
opoznienie 2 sek.
Cieplawe powietrze stopniowo rozchodzi sie zatruwajac swa zatechla wonia najblizsza okolice.

Gdy plyta jest odslonieta zmienia sie ostatnie zdanie opisu plyty:
Jest przesunieta i odslania ziejace ciemnoscia zejscie na dol.

Ponadto aktywna jest wtedy komenda:
zejdz na dol - k_1

:plyte:
Z pozoru zwykla plyta grobowa, po blizszych ogledzinach okazuje sie byc misternie przytwierdzona do podloza klapa. Wykonana z porosnietego mchem marmuru nosi na sobie slady rzezbien ukladajacych sie w niemal calkiem juz zatarte piszczele. Przymocowano don metalowy uchwyt, ktory z pewnoscia ulatwi przesuniecie plyty. 


Wyjscia:
sw-c3 - Z trudem zaglebiasz sie w zarosla.
* z trudem zaglebia sie w zarosla.
* wylania sie z zarosli.
s-c5 - Podazasz w strone kolumny.
* podaza w strone kolumny.
* nadchodzi od strony kolumny.
se-c6 - Przeciskasz sie przez rzadsze zarosla.
* przeciska sie przez rzadsze zarosla.
* wylania sie z zarosli.

OPIS NOCNY
Wyjscia:
w jakimkolwiek kierunku - Znikasz w cieniu.
* znika w cieniu.
* wylania sie z cienia.*/