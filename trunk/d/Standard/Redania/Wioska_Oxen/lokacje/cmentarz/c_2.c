
/*By Veli 06.12.09
Wejscie na cmentarz - wioska Oxen*/

#pragma strict_types
#include "dir.h"
#include "/d/Standard/Redania/dir.h"
#include <stdproperties.h>
#include <std.h>
#include <mudtime.h>
#include <pogoda.h>
#include <macros.h>
inherit WIOSKA_OXEN_CMENTARZ_STD;

//string krotki_opis();
string dlugi_opis();
string rozkop_grob();
string przeczytaj_napisy();

void
create_cmentarz()
{
    set_short("Przy skrzypi^acej bramie");
    //set_short(&krotki_opis());
    set_long(&dlugi_opis());
    add_prop(ROOM_I_INSIDE, 0);
    add_object(WIOSKA_OXEN_PRZEDMIOTY+"obiekty/brama_c_out.c",1);
    //dodaj_rzecz_niewyswietlana("",4); - moze tu cos bedzie.
    add_item("kolumn^e","Z^lamana, smuk^la kolumna odcina si^e biel^a marmuru"+
        " od otoczaj^acego cmentarz, niskiego muru. Wszystkie groby zdaj^a"+
        " si^e by^c skierowane w jej stron^e, zupe^lnie jakby stanowi^la"+
        " swoiste centrum tego miejsca.\n");
    add_item("budynek","Niski, jednopi^etrowy budynek wzniesiony z jasnego"+
        " kamienia tu^z przy samym murze cmentarza wygl^ada jakby by^l do^n"+
        " przyklejony. S^adz^ac po jako^sci spoiwa budowla nie nale^zy do"+
        " wiekowych i prawdopodobnie wykorzystywany jest jako magazny b^adz"+
        " kostnica.\n");
    add_item("zaro^sla","Do^s^c g^este krzaki kolczastych ro^slin rozrastaj^a"+
        " si^e tu^z za domkiem, skrywaj^ac w swych g^l^ebinach dalsz^a"+
        " cz^e^s^c cmentarza.\n");
    add_item("groby","Przygl^adaj^ac si^e mo^zna doj^s^c do wniosku, i^z"+
        " cmentarz ten przeznaczony jest g^lownie dla ludzi, przynajmniej"+
        " ta jego cz^e^s^c. Niczym si^e nie wyr^o^zniaj^ace, w wi^ekszo^sci"+
        " kamienne, rzadziej granitowe grobowce zwr^ocone s^a w kierunku"+
        " z^lamanej kolumny majacz^acej na wchodzie. Na kilku p^lytach"+
        " jeste^s w stanie zauwa^zy^c prawie nie zatarte napisy.\n");
    add_sit("pod murem","pod murem","spod muru",6);
    add_event("Brama skrzypi przera^xliwe bezskutecznie opieraj^ac si^a sile"+
        " wiatru.\n");//DZIEN
    add_event("Na jednej z p^lyt dogasa ogarek ^swiecy.\n");
    add_event("Cisz^e przerywa zduszony potok przekle^nstw dochodz^acy"+
        " z budynku.\n");//DZIEN
    add_event("Szare strugi deszczu wsi^akaj^a pomi^edzy kamyczki alejki.\n");//DESZCZ
    add_event("Strugi deszczu zmieniaj^a si^e w ciemn^e ^scian^e wody, prawie"+
        " ca^lkiem ograniczaj^ac widoczno^s^c.\n");//DZESZCZ
    add_event("Ciemnoszare chmury przesuwaj^a si^e nad wzg^orzem.\n");
    add_event("G^luchy odg^los zbli^zaj^acej si^e burzy przetoczy^l si^e"+
        " nad wzg^orzem.\n");//LATO
    add_exit(WIOSKA_OXEN_LOKACJE+"cmentarz/c_kostnica",({"drzwi","do"+
        " wn^etrza budynku","z budynku"}));
    add_exit(WIOSKA_OXEN_LOKACJE+"cmentarz/c_3",({"^scie^zka","cos","cos"}));
    add_exit(WIOSKA_OXEN_LOKACJE+"cmentarz/c_8",({"alejka","cos","cos"}));
    add_exit(WIOSKA_OXEN_LOKACJE+"cmentarz/c_1",({"brama","przez"+
        " bram^e","przez bram^e"}));
    //wyjscia zale^zne od warunk^ow - patrz na ko^ncu
    //wyjscie do wioski - brama - patrz na koncu  
    add_cmd_item("gr^ob", "rozkop", rozkop_grob, "Rozkop co?\n");
    add_cmd_item("napisy", "przeczytaj", przeczytaj_napisy,
        "Przeczytaj co?\n");
}

/*string
krotki_opis()
{

    string str;

    if (jest_dzien() == 1)
    {
        str = "Przy skrzypi^acej bramie\n.";
    }

    if (jest_dzien() == 0)
    {
        str = "Przy zamkni^etej bramie\n";
    }

    return str;

}*/

string
dlugi_opis()
{
    string str;

    str = "Znajdujesz si^e niew^atpliwie na terenie ca^lkiem sporego i"+
        " zdaje si^e starego cmentarza, kt^ory z pozoru nie r^o^zni si^e"+
        " zbytnio od innych miejsc tego typu. Panuj^acy tu spok^oj wydaje"+
        " si^e w jaki^s spos^ob ochraniac i lagodzi^c zmeczenie doczesnym"+
        " zgie^lkiem ^zycia. "; 
  
    if (pora_roku() == MT_WIOSNA)
    {
        str += "By^c mo^ze to powietrze przepe^lnione woni^a, nasi^akni^etej"+
            " wod^a, budz^acej si^e do cyklu ziemi, ";
    }

        if (pora_roku() == MT_LATO)
    {
        str += "By^c mo^ze to powietrze przepe^lnione woni^a wygrzanej"+
            " ziemi rozbuchanej pe^lni^a ^zycia i rozkwitu, ";
    }

            if (pora_roku() == MT_JESIEN)
    {
        str += "By^c mo^ze to powietrze przepe^lnione cie^zk^a woni^a"+
            " obumieraj^acej, gnij^acej przebrzmia^l^a zieleni^a ziemi, ";
    }

            if (pora_roku() == MT_ZIMA)
    {
        str += "By^c mo^ze to powietrze przepe^lnione gryz^acym ch^lodem"+
            " zion^acym od zalegaj^acych wsz^edzie zwa^l^ow sniegu, ";
    }

        str += "a by^c mo^ze widok milcz^acych grob^ow sprawia, ^ze cisza nie"+
            " jest tu ju^z tak dra^zni^aca... A by^c mo^ze to po prostu"+
            " bezradno^s^c oraz narastaj^aca ^swiadomosc tego, ^ze dos^lownie"+
            " nic nie pozwoli nikomu z nas uciec przed ^smierci^a. S^adz^ac"+
            " po do^s^c ^swie^zych, licz^acych sobie oko^lo dwudziestu lat"+
            " grobowcach, w tej cz^esci cmentarza spoczywaj^a zwykli"+
            " mieszczanie, kt^orzy za ^zycia byli w wi^ekszosci porz^adnymi"+
            " obywatelami i poza pos^lusznym p^laceniem podatkow nie"+
            " ws^lawili si^e niczym szczeg^olnym. Jak okiem si^egn^a^c, na"+
            " ca^lym cmentarzu nie wida^c prawie ^zadnych drzew. Zdaje si^e,"+
            " ^ze w celu zwi^ekszenia przestrzeni wyci^eto je wszystkie, poza"+
            " jednym, majacz^acym gdzie^s na wschodzie. Tu^z przy bramie,"+
            " przyklejony do muru stoi niewielki budynek, od kt^orego ";

    if (jest_dzien() == 1)
    {
        str += "odchodzi niewielka, ledwie wydeptana dr^o^zka, nikn^aca"+
            " gdzie^s w zaro^slach na po^lnocno wschodniej cz^e^sci wzg^orza.\n.";
    }

    if (jest_dzien() == 0)
    {
        str += "chyba odchodzi jaka^s, prowadz^aca w krzaki ^scie^zka..."+
            " A przynajmniej co^s w stylu ^scie^zki... Mimo, i^z przy"+
            " bramie stale kopc^a si^e dwie spore latarnie, to wci^a^z jest"+
            " zbyt ciemno by dojrze^c co^s w krzakach.\n";
    }

    return str;
}

string
rozkop_grob()
{
    string str = "Chyba nie jest to najlepszy pomys^l... W okolicy mo^ze"+
        " kr^eci^c si^e grabarz. Takie typy zwykle widz^a i s^lysz^a wi^ecej"+
        " ni^z si^e spodziewasz.\n";
    return str;
}
 
string przeczytaj_napisy()
{
    string str = "Z kilku, po^lo^zonych najbli^zej ^scie^zki pomnik^ow"+
    " odczytujesz:"; 

    if (jest_dzien() == 1)
    {
        str += "\n\"Tu spoczywa m^oj zbyt wierny ma^l^zonek,"+
        " Maggot Ballywan.\"\n \n\"Szaro^sci^a zakryty, w cisz^e spowity,"+
        " cho^c nie mniej zimny ni^z za ^zycia - Savir, kt^ory odszed^l"+
        " w ^slad za...\"\n \n\"Gorf Szczery. ^Zy^l lat 34. Oko^lo...\"\n";
    }

    if (jest_dzien() == 0)
    {
        str += " \n\"Szaro^sci^a zakryty, w cisz^e spowity,"+
        " cho^c nie mniej zimny ni^z za ^zycia - Savir, kt^ory odszed^l"+
        " w ^slad za...\"\n Niestety by odczyta^c inne jest stanowczo zbyt"+
        " ciemno.\n";
    }

    return str;
}


public string
exits_description() 
{
    
        if (jest_dzien() == 1)

    {
        return "Skrzyd^lo bramy prowadz^acej z cmentarza jest uchylone podobnie"+
            " jak drzwi do budynku tu^z przy murze. Za budynkiem zauwa^zasz"+
            " g^este zaro^sla i nikn^ac^a w nich ^scie^zk^e wiod^ac^a"+
            " gdzie^s na ty^ly ^zalnika, za^s na po^ludniowy wsch^od"+
            "  prowadzi ca^lkiem wygodna alejka wij^ac^a si^e wsr^od"+
            " grobowc^ow.\n";
    }
    
    if (jest_dzien() == 0)
    {
        return "Brama wyj^sciowa z cmentarza, podobnie"+
            " jak drzwi do budynku tu^z przy murze. Za budynkiem zauwa^zasz"+
            " g^este zaro^sla i nikn^ac^a w nich ^scie^zk^e wiod^ac^a"+
            " gdzie^s na ty^ly ^zalnika, za^s na po^ludniowy wsch^od"+
            "  prowadzi ca^lkiem wygodna alejka wij^ac^a si^e wsr^od"+
            " grobowc^ow.\n";
    }
 
}

/*[specjalne]
zmiana por dnia
zachod slonca
Ostatnie okruchy jasno^sci dnia nikna wraz z zachodz^acym s^lo^ncem, a cmentarzysko nie^spiesznie spowija g^esty cie^n. 
wschod slonca
Pierwsze promienie s^lo^nca przecinaj^a ciemnogranatowe nocne niebo i powoli roz^swietlaja okolice.

Wyjscia:
n-DG - Podazasz do wnetrza domku.
* podaza do wnetrza domku.
* wychodzi z domku.
brama-c1 - Przekraczasz brame cmentarna.
* przekracza brame cmentarna.
* przybywa przez brame.
ne-c3 - Z trudem omijajac groby zaglebiasz sie w zarosla.
* z trudem omija groby i niknie gdzies w zaroslach.
* wylania sie z zarosli.
se-8 - Zwirowa alejka podazasz w strone okazalszych grobowcow.
* podaza alejka w strone okazalszych grobowcow.
* nadchodzi alejka.

Wyjscia nocne:
wyjscia nieaktywne w nocy
n-DG Drzwi sa zamkniete.
brama-c1-Jak przez mgle przypomina ci sie napis na tablicy po drugiej stronie bramy - "Otwarte od switu do zmroku"...
* z niepewna mina przypatruje sie bramie cmentarnej.
Ktos z drugiej strony dobija sie do bramy.

wyjscia aktywne
ne-c3 - Z trudem omijajac groby zaglebiasz sie w zarosla.
* znika w cieniu.
* wylania sie z cienia.
e-5 - Przestepujac peknieta plyte jednego z grobow podazasz ku centrum cmentarza.
* znika w cieniu.
* wylania sie z cienia.
se-8 - Ruszasz ku okazalszym, lepiej oswietlonym grobowcom.
* znika w cieniu.
* wylania sie z cienia.*/