
/*By Veli 17.12.09
Przed bram^a cmentarza - wioska Oxen*/

#pragma strict_types
#include "dir.h"
#include "/d/Standard/Redania/dir.h"
#include <stdproperties.h>
#include <mudtime.h>
#include <pogoda.h>
#include <macros.h>
inherit WIOSKA_OXEN_WIOSKA_STD;

//string krotki_opis();
string dlugi_opis();
string dlugi_wzgorze();
string dlugi_zagajnik(); //opis laskow na wyspie

void
create_wioska()
{
    set_short("Sciezka 1");
    //set_short(&krotki_opis());
    set_long(&dlugi_opis());
    add_prop(ROOM_I_INSIDE, 0);
    /*add_object(WIOSKA_OXEN_PRZEDMIOTY+"obiekty/cmentarna_tablica.c",1);
    add_object(WIOSKA_OXEN_PRZEDMIOTY+"obiekty/brama_c.c",1);
    add_item("placyk","Niewielki placyk okolony g^estym ^l^egowym zagajnikiem"+
        " przed bram^a cmentarn^a wysypano drobnymi otoczakami, kt^ore"+
        " skrzypi^ac przy ka^zdym twoim kroku przywodz^a na my^sl ziarna"+
        " piasku w klepsydrze, kt^ore bezg^lo^snie przesypuj^ac si^e w do^l"+
        " odliczaj^a czas jaki jeszcze pozosta^l, nim znajdziemy si^e po"+
        " drugiej stronie - w za^swiatach.\n");*/
    add_item("zagajnik",&dlugi_zagajnik());
    add_item("wzg^orze",&dlugi_wzgorze());
    add_sit("pod murem","pod murem","spod muru",6);
    add_event("Cichy podmuch wiatru poruszyl ga^l^eziami drzew i niemal"+
        " natychiast zanikn^a^l.\n");  
    add_event("Wicher wyje op^eta^nczo pomi^edzy nagimi konarami olch.\n");//ZIMA
    add_event("Znad rzeki unosz^a si^e g^este, wilgotne opary.\n");//ZIMA
    add_event("Strugi deszczu zmieniaj^a si^e w ciemn^e ^scian^e wody, prawie"+
        " ca^lkiem ograniczaj^ac widoczno^s^c.\n");//DESZCZ
    add_event("Ciemnoszare chmury przesuwaj^a si^e nad wzg^orzem.\n");
    add_exit(WIOSKA_OXEN_LOKACJE+"w_7",({"d","scie^zk^a w d^o^l",
        "ze wzg^orza"}));
    add_exit(WIOSKA_OXEN_LOKACJE+"c_1",({"u","scie^zk^a w g^or^e",
        "^scie^zk^a"}));
}

/*string
krotki_opis()
{

    string str;

    if (jest_dzien() == 1)
    {
        str = "Placyk przy bramie\n.";
    }

    if (jest_dzien() == 0)
    {
        str = "Pod zamkni^et^a bram^a\n";
    }

    return str;

}*/

string
dlugi_opis()
{
    string str;

    str = "LONG";
    
    return str;
}

string
dlugi_wzgorze()
{
    string str;

    str = "Wzg^orze, na kt^orym stoisz to zarazem najwy^zej po^lo^zone"+
        " miejsce na ca^lej, ulokowanej w delcie Pontaru wysepki."+
        " Chocia^z samo wzniesienie nie nale^zy mo^ze do zbyt okaza^lych"+
        " to i tak roztarcza si^e st^ad otwarty widok na szeroki i"+
        " nadzwyczaj leniwy bieg";

        if (pora_roku() == MT_WIOSNA)
    {
        str += " wezbra^lej po zimie rzeki, zazielenione, poro^sni^ete"+
            " sitowiem brzegi, trz^esawiska oraz budz^ac^a si^e do ^zycia"+
            " male^nk^a wioseczk^e zwan^a Grabowa Buchta, majacz^ac^a"+
            " gdzie^s u podn^o^za, po^sr^od ^l^eg^ow.\n";
    }

        if (pora_roku() == MT_LATO)
    {
        str += " kipi^acej z gor^aca i niemi^losiernie ^smierdz^acej w upale"+
            " rzeki, w kt^orej wodach rozk^ladaj^a si^e teraz wszelakie"+
            " mo^zliwe ^smieci i fekalia z dw^och wielkich miast - "+
            " Oxenfurtu i Novigradu.\n";//DO ZMIANY!!!!!!!!!!
    }

        if (pora_roku() == MT_JESIEN)
    {
        str += " sp^lywaj^acej spiesznie ku morzu rzeki, poz^locone"+
            " jesiennymi li^scmi ^l^egowe zagajniki oraz przycupni^et^a u"+
            " podn^o^za wiosk^e, zwan^a Grabowa Buchta. W powietrzu czu^c"+
            " ju^z wyra^xnie butwiej^ac^a roslinno^s^c oraz zapach palonego"+
            " w piecach w osadzie, drewna.\n";
    }

        if (pora_roku() == MT_ZIMA)
    {
        str += " paruj^acej w ch^lodnym, zimowym powietrzu rzeki oraz"+
            " przypruszone ^sniegiem i poro^sni^ete zesch^lym sitowiem"+
            " oraz bezlistnymi ^legowymi zagajnikami brzegi. Z komin^ow"+
            " przycupni^etej u podn^o^za wioseczki zwanej Grabow^a Bucht^a"+
            " unosz^a si^e bia^lawe dymy o czystym drzewnym zapachu"+
            " spowijaj^ac okolic^e niby mg^la.\n";
    }

    return str;
}

string
dlugi_zagajnik()
{

    string str;

    str = "To co wydawa^lo si^e na pierwszy rzut oka niczym wi^ecej ni^z"+
        " ^estymi zaro^slami to tak napraw^e nic innego jak zastoiskowe"+
        " olsy. Okresowo zalewane nadrzeczne lasy ^l^egowe w dolinie rzeki."+
        " Niemal ca^l^a wysepk^e w delcie Pontaru, zagubi^a gdzie^s"+
        " pomi^edzy Oxenfurem a Pian^a ciasno porastaj^a, lubuj^ace si^e"+
        " w wilgotnym pod^lo^zu";
    
        if (pora_roku() == MT_WIOSNA)
    {
        str += " zieleni^ace si^e wiosennie po^sr^od wezbra^lych w^od olsze,"+
            " wi^azy, graby i kar^lowate topole. Tu^z przy podmok^lym,"+
            " trudnym w tej chwili do wyznacznia u brzegu rzeki, zauwa^zasz"+
            " nawet wybuja^le d^eby oraz cenne jesiony wyrywaj^ace si^e ku"+
            " g^orze z g^aszczu sitowia i niskich wierzb.\n";
    }

        if (pora_roku() == MT_LATO)
    {
        str += " kwitn^ace olsze, wi^azy, graby i kar^lowate,"+
            " rozpoczynaj^ace w^lasnie pylenie topole. Tu^z przy podmok^lym"+
            " brzegu zauwa^zasz nawet wybuja^le d^eby oraz cenne jesiony"+
            " wyrywaj^ace si^e ku g^orze z g^aszczu sitowia i niskich"+
            " wierzb.\n";
    }

        if (pora_roku() == MT_JESIEN)
    {
        str += " zrzucaj^ace przed zim^a listowie olsze, wi^azy, graby i "+
            " kar^lowate, srebrzyste topole. Tu^z przy zabagnionym"+
            " brzegu zauwa^zasz nawet wybuja^le d^eby oraz cenne jesiony"+
            " wyrywaj^ace si^e ku g^orze z g^aszczu butwiej^acego sitowia"+
            " i niskich wierzb.\n";
    }

        if (pora_roku() == MT_ZIMA)
    {
        str += " nagie korony krzewiastych olsz, wi^az^ow, grab^ow i "+
            " kar^lowate, srebrzyste topole. Tu^z przy zamarzni^etym obecnie"+
            " brzegu zauwa^zasz nawet wybuja^le d^eby oraz cenne jesiony"+
            " wyrywaj^ace si^e ku g^orze z g^aszczu pokrytego szronem"+
            " sitowia i k^lebowiskach bezlistnych ga^l^azek wierzb.\n";
    }

    return str;
}

public string
exits_description() 
{
    string str;

    str =  "Mo^zesz st^ad pod^a^zyc biegn^ac^a mi^edzy olszynami"+
            " ^scie^zk^a w do^l wzniesienia ku wiosce, b^adz wej^s^c na"+
            " szczyt wzniesienia.\n."; 

    return str;
}