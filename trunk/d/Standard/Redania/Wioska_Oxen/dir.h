#include "/d/Standard/Redania/dir.h"

#define WIOSKA_OXEN            ("/d/Aretuza/veli/Wioska_Oxen/")

#define WIOSKA_OXEN_LOKACJE      (WIOSKA_OXEN + "lokacje/")
#define WIOSKA_OXEN_NPC          (WIOSKA_OXEN + "npc/")
#define WIOSKA_OXEN_PRZEDMIOTY   (WIOSKA_OXEN + "przedmioty/")
#define WIOSKA_OXEN_DRZWI        (WIOSKA_OXEN + "lokacje/drzwi/")
#define WIOSKA_OXEN_UBRANIA      (WIOSKA_OXEN + "przedmioty/ubrania/")

#define WIOSKA_OXEN_STD          (WIOSKA_OXEN + "std/wioska_oxen.c")
#define WIOSKA_OXEN_WIOSKA_STD   (WIOSKA_OXEN + "std/wioska.c")

#define WIOSKA_OXEN_CMENTARZ_STD (WIOSKA_OXEN + "std/cmentarz.c")
#define WIOSKA_OXEN_NPC_STD      (WIOSKA_OXEN + "std/npc.c")
