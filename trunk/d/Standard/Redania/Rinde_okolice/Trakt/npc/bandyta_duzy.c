
/* Autor: Avard
   Opis : Valor
   Data : 15.03.07 */

inherit "/std/humanoid.c";

#include <std.h>
#include <ss_types.h>
#include <macros.h>
#include <stdproperties.h>
#include <pl.h>
#include "dir.h"

int czy_walka();
int walka = 0;

void
create_humanoid()
{
    ustaw_odmiane_rasy("m^e^zczyzna");
    set_gender(G_MALE);
    dodaj_nazwy("bandyta");
    dodaj_nazwy("zb^oj");

    random_przym("wielki:wielcy pot^e^zny:pot^e^zni ogromny:ogromni "+
       "umi^e^sniony:umi^e^snieni||agresywny:agresywni ^lysy:^lysi "+
       "ogolony:ogoleni",2);

    set_long("Ogromne mi^e^snie i wygolona g^lowa to dwie, najbardziej "+
        "charakterystyczne cechy tego m^e^zczyzny. Spl^atana w dwa warkocze "+
        "czarna broda swobodnie opada na pot^e^zn^a klatk^e piersiow^a, "+
        "kt^ora przepasana jest tylko dwoma szerokimi pasami krzy^zuj^acymi "+
        "si^e na piersi. Spodnie kt^ore ma na sobie s^a odrobine za ma^le "+
        "przez co dok^ladnie przylegaj^a do n^og podkre^slaj^ac ka^zdy z "+
        "mi^e^sni. Jego d^lonie wygl^adaj^a jakby mog^ly zmia^zd^zy^c "+
        "g^low^e cz^lowieka bez najmniejszego problemu, a w jednej z nich "+
        "dzier^zy morgenstern.\n");
    set_stats (({130,30,120,20,100}));//FIXME balans przed otwarciem
    add_prop(CONT_I_WEIGHT, 100000);
    add_prop(CONT_I_HEIGHT, 200);
    add_prop(LIVE_I_NEVERKNOWN, 1);
    set_aggressive(1);
    set_attack_chance(100);
    set_npc_react(0);

    set_cact_time(10);
    add_cact("krzyknij");
    add_cact("krzyknij Gi^n!");
    add_cact("warknij");

    add_armour(TRAKT_RINDE_OBIEKTY +"czarne_luzne_Lc.c");
    add_armour(TRAKT_RINDE_OBIEKTY +"znoszone_skorzane_Lc.c");
    add_weapon(TRAKT_RINDE_OBIEKTY +"prosty_stalowy.c");
    add_armour(TRAKT_RINDE_OBIEKTY +"lekkie_skorzane_Lc.c");

    set_skill(SS_DEFENCE, 20 + random(6));
    set_skill(SS_PARRY, 40 + random(8));
    set_skill(SS_WEP_CLUB, 80 + random(11));

    set_default_answer(VBFC_ME("default_answer"));
}
void
powiedz_gdy_jest(object player, string tekst)
{
    if (environment(this_object()) == environment(player))
        this_object()->command(tekst);
}

string
default_answer()
{
    set_alarm(2.0, 0.0, "powiedz_gdy_jest", this_player(),
        "powiedz do "+ this_player()->query_name(PL_BIE) +
        "Chyba nie my^slisz, ^ze b^ede odpowiada^l na twoje pytania?");
    return "";
}

void
add_introduced(string imie_mia, string imie_bie)
{
    set_alarm(2.0, 0.0, "return_introduce", find_player(imie_mia));
}

void
return_introduce(object ob)
{
    command("'A co mnie to obchodzi?");
}

void
attacked_by(object wrog)
{
    if(walka==0)
    {
        TO->command("krzyknij");
        set_alarm(1.0, 0.0, "czy_walka", 1);
        walka = 1;
        return ::attacked_by(wrog);
    }
    else if(walka == 1)
    {
        set_alarm(1.0, 0.0, "czy_walka", 1);
        walka = 1;
        return ::attacked_by(wrog);
    }
}

int
czy_walka()
{
    if(!query_attack())
    {
        walka = 0;
        return 1;
    }
    else
        set_alarm(1.0, 0.0, "czy_walka", 1);
}
