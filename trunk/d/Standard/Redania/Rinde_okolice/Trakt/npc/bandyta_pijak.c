
/* Autor: Sedzik
   Opis : Valor
   Data : 19.03.07 */

inherit "/std/humanoid.c";

#include <std.h>
#include <ss_types.h>
#include <macros.h>
#include <stdproperties.h>
#include <pl.h>
#include <money.h>
#include "dir.h"

void
create_humanoid()
{
    ustaw_odmiane_rasy("m^e^zczyzna");
    set_gender(G_MALE);
    dodaj_nazwy("bandyta");
    dodaj_nazwy("zb^oj");
    random_przym("oszpecony:oszpeceni brudny:brudni "+
        "^smierdz^acy:^smierdz^acy zaniedbany:zaniedbani "+
        "zaro^sni^ety:zaro^sni^eci||agresywny:agresywni "+
        "nerwowy:nerwowi niski:niscy blady:bladzi "+
        "krzywonosy:krzywonosi",2);

    set_long("Ten m�czyzna chyba wi�kszo�� swojego �ycia sp�dzi� "
	+"w rynsztoku, albo na pewno niedawno w nim przebywa�. Okropny fetor "
	+"unosz�cy si� wok� jego postaci wraz z cuchn�cym oddechem "
	+"i zniszczonymi pozosta�o�ciami po z�bach tworz� z niego raczej ma�o "
	+"atrakcyjnego osobnika. Towarzyszy mu r�wnie� smr�d starego alkoholu "
	+"sugeruj�cy, �e w�dka to jego ulubiony nap�j. Pokraczna postura, "
	+"niezwylke niski wzrost oraz otaczaj�cy go zapach przywodz� na my�l "
	+"bardziej ulicznego pijaczyn� ni� bandyt� mog�cego kogokolwiek "
    +"zabi�.\n");

    set_stats (({60,20,90,5,100}));
    add_prop(CONT_I_WEIGHT, 70000);
    add_prop(CONT_I_HEIGHT, 165);

    set_cact_time(10);
    add_cact("krzyknij Uee! Martwy�!");
    add_cact("krzyknij Psia ma^c!");
	add_cact("powiedz Psie syny! Ja was wszystkich...!");
	add_cact("krzyknij Niech ja was! Matka was nie pozna!");
	add_cact("krzyknij To moje miejsce, wynocha!");

    set_default_answer(VBFC_ME("default_answer"));

    add_armour(TRAKT_RINDE_OBIEKTY +"czarne_luzne_Lc.c");
    add_armour(TRAKT_RINDE_OBIEKTY +"stary_cwiekowany_Lc.c");
    add_armour(TRAKT_RINDE_OBIEKTY +"znoszone_skorzane_Lc.c");
    add_armour(TRAKT_RINDE_OBIEKTY +"lekkie_skorzane_Lc.c");
    add_weapon(TRAKT_RINDE_OBIEKTY +"prosta_ciemna.c");

    set_skill(SS_DEFENCE, 20 + random(6));
    set_skill(SS_PARRY, 40 + random(8));
    set_skill(SS_WEP_CLUB, 70 + random(11));

    add_prop(LIVE_I_NEVERKNOWN, 1);

	set_aggressive(1);
    set_attack_chance(100);
    set_npc_react(0);

}


void
powiedz_gdy_jest(object player, string tekst)
{
    if (environment(this_object()) == environment(player))
        this_object()->command(tekst);
}

string
default_answer()
{
    set_alarm(2.0, 0.0, "powiedz_gdy_jest", this_player(),
        "powiedz Zdychaj psie!");
    return "";
}



void
add_introduced(string imie_mia, string imie_bie)
{
    set_alarm(2.0, 0.0, "return_introduce", find_player(imie_mia));
}
void
return_introduce(object ob)
{
    command("powiedz Jeszcze czego! Psi synu!");
}

void
emote_hook(string emote, object wykonujacy)
{
    switch (emote)
    {
        case "kopnij": set_alarm(1.5, 0.0, "nienajlepszy", wykonujacy);
                    break;
        case "spoliczkuj": set_alarm(1.5, 0.0, "nienajlepszy", wykonujacy);
                    break;
        case "opluj" : set_alarm(2.5, 0.0, "nienajlepszy", wykonujacy);
                    break;
        case "poca^luj":
              {
              if(wykonujacy->query_gender() == 1)
                switch(random(1))
                 {
                 case 0: command("emote u^smiecha si^e obrzydliwie."); break;
                }
              else
                {
                   switch(random(1))
                   {
                   case 0:command("'Zdychaj!."); break;
                   }
                }

                break;
                }
        case "prychnij":
              {
                switch(random(1))
                 {
                 case 0:command("spojrzyj metnie "+
                     "na "+ OB_NAME(wykonujacy)); break;
                 }
                break;
               }
        case "przytul": set_alarm(0.5, 0.0, "malolepszy", wykonujacy);
                   break;
    }
}

void
malolepszy(object wykonujacy)
{
    switch(random(1))
    {
        case 0: set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
			"'Chyba ci sw�dzi!");
    }
}

void
nienajlepszy(object wykonujacy)
{
  switch(random(2))
    {
    case 0:
      command("'Zdychaj!");
      break;
    case 1:
      command("'Psi synu ty!");
      break;
    }
}
