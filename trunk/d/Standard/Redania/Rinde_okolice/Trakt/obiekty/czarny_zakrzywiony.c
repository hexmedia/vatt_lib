
/* Autor: Avard
   Opis : Khaes
   Data : 15.03.07 */
inherit "/std/weapon";

#include "/sys/formulas.h"
#include <stdproperties.h>
#include <wa_types.h>
#include <pl.h>
#include <materialy.h>
#include <object_types.h>


void
create_weapon()
{
    ustaw_nazwe("n^o^z");
                    
    dodaj_przym("czarny","czarni");
    dodaj_przym("zakrzywiony","zakrzywiony");

    set_long("W lekko zakrzywionym, kilkunasto centymetrowym ostrzu mo^zna "+
        "si^e przejrze^c niczym w lustrze. Kr^otka, drewnian^a r^aczk^e "+
        "kto^s nieumiej^etnie pomalowa^l na czarny kolor, tak ^ze w paru "+
        "miejscach spod farby wyziera si^e naturalny br^az drewna. U nasady "+
        "ostrza wygrawerowana zosta^la ma^la, o^smioramienna gwiazdka.\n");

    add_prop(OBJ_I_WEIGHT, 900);
    add_prop(OBJ_I_VOLUME, 900);
    add_prop(OBJ_I_VALUE, 1200);
   
    set_hit(21);
    set_pen(14);

    set_wt(W_KNIFE);
    set_dt(W_IMPALE);
    ustaw_material(MATERIALY_STAL, 80);
    ustaw_material(MATERIALY_DR_BUK, 20);
    set_type(O_BRON_SZTYLETY);

    set_hands(A_ANY_HAND);
}
