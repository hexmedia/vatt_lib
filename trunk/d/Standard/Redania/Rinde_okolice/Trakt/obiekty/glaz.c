inherit "/std/object.c";

#include <pl.h>
#include <stdproperties.h>
#include <macros.h>
#include <filter_funs.h>
#include <mudtime.h>

void create_object()
{
    ustaw_nazwe("g�az");
set_long("@@dlugi_opis@@");
    make_me_sitable("na","na g�azie","z g�azu",4);

    add_prop(OBJ_I_WEIGHT, 50000000);

}

string 
dlugi_opis()
{
string str;

if(environment(TO)->pora_roku() != MT_ZIMA)
  {
str = "Ten sporych rozmiar�w g�az wygl�da, jakby by� dostosowany do " +
             "potrzeb w�drowc�w - kszta�tem przypomina nieco kanap�. " +
             "Zar�wno \"siedzisko\", jak i \"oparcie\" s� za� na tyle du�e, " +
             "by pomie�ci� kilka os�b na raz. Gdyby si� uprze�, to mo�na by " +
             "si� tu nawet po�o�y�.";
  }
else
  {
str = "Ten sporych rozmiar�w g�az wygl�da, jakby by� dostosowany do potrzeb "+
"w�drowc�w - kszta�tem przypomina nieco kanap�. Zar�wno \"siedzisko\", jak i "+
"\"oparcie\" s� za� na tyle du�e, by pomie�ci� kilka os�b na raz. Gdyby si� "+
"uprze�, mo�na by si� tu nawet po�o�y�. Teraz, co prawda, g�az pokryty jest "+
"�niegiem, ale c� to za problem ten �nieg odgarn��? ";
  }

str += "\n";

return str;
}