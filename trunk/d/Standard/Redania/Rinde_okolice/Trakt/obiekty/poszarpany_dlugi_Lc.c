
/* Autor: Sedzik
   Opis : Zwierzaczek
   Data : 18.03.07 */

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <macros.h>
#include <materialy.h>

void
create_armour()
{  
   ustaw_nazwe("p^laszcz");
    dodaj_przym("poszarpany","poszarpani");
    dodaj_przym("d^lugi", "d^ludzy");

    set_long("Ubranie to zosta^lo wykonane z szorstkiego materia^lu. Poszarpane "
	+"kraw^edzie swiadcz^a o wieloletnim noszeniu, a przynajmniej bardzo "
	+"cz^estym. W jednej z kieszen dostrzec mo^zna dziur^e, a jesli jeszcze si^e "
	+"w innych nie zrobi^ly, to bez w^atpienia lada chwila sie zrobi^a. O dziwo, "
	+"zachowa^ly si^e wszystkie guziki, pozwalaj^ace na dok^ladne zapi^ecie po^l^ow "
	+"p^laszcza. \n");

    set_slots(A_ROBE);
    add_prop(OBJ_I_WEIGHT, 500);
    add_prop(OBJ_I_VOLUME, 2100);
    add_prop(OBJ_I_VALUE, 90);

    add_prop(ARMOUR_F_PRZELICZNIK, 20.0);
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    add_prop(ARMOUR_S_DLA_RASY, "czlowiek"); 
    ustaw_material(MATERIALY_WELNA);
    set_size("L");
}


