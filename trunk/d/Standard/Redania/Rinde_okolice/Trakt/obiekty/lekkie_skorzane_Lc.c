/* Autor: Avard
   Opis : Khaes
   Data : 19.03.07 */
inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <macros.h>
#include <materialy.h>
#include <object_types.h>
#include <pl.h>

void
create_armour()
{
    ustaw_nazwe("karwasze");

    dodaj_przym("lekki","lekcy");
    dodaj_przym("sk^orzany","sk^orzani");

    set_long("Karwasze sk^ladaj^a si^e z dw^och wyprawionych i "+
        "usztywnionych kawa^lk^ow wo^lowej sk^ory idealnie wr^ecz "+
        "nachodz^acych na siebie i zmy^slnie ukrywaj^acych przedrami^e "+
        "nosz^acej je osoby przed nieprzyjaznym or^e^zem. Przez w^askie "+
        "dziurki w p^latach sk^ory przepleciona jest czerwona krajka, "+
        "kt^ora po ciasnym zwi^azaniu na pewno nie pozwoli ze^slizgn^a^c "+
        "si^e opancerzeniu z nale^zytego miejsca.\n");

    set_slots(A_FOREARMS);
    set_ac(A_FOREARMS, 2,2,2);
    
    add_prop(OBJ_I_WEIGHT, 500); 
    add_prop(OBJ_I_VOLUME, 500);
    add_prop(OBJ_I_VALUE, 70);
    
    set_type(O_ZBROJE);
    
    ustaw_material(MATERIALY_SK_BYDLE);
    
    add_prop(ARMOUR_S_DLA_RASY, "czlowiek"); 
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    set_size("L");
}