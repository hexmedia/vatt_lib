  /* Autor: Alcyone ; 21.06.05
   * Opis : Alcyone & Faeve
   * przerobka na dzisiejsze standardy-Lil. 2006-05-07 22:44:02
   */

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <macros.h>
#include <materialy.h>
#include <object_types.h>


void
create_armour() 
{

    ustaw_nazwe("spodnie");
    ustaw_nazwe_glowna("para");
    dodaj_przym("czarny","czarni");
    dodaj_przym("lu�ny","lu�ni");
    set_long("@@dlugi@@\n");
    set_type(O_UBRANIA);
    ustaw_material(MATERIALY_LEN);
    set_ac(A_THIGHS | A_HIPS | A_STOMACH, 2, 2, 2);
    add_prop(OBJ_I_VOLUME, 1500);
    add_prop(OBJ_I_WEIGHT, 669);
    add_prop(OBJ_I_VALUE, 19);
    add_prop(ARMOUR_I_D_ROZCIAGLIWOSC, 30);
    add_prop(ARMOUR_I_U_ROZCIAGLIWOSC, 30);
    add_prop(ARMOUR_S_DLA_RASY, "czlowiek"); 
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    set_size("L");
}

string dlugi()
{
    string str;
    str="Lu�ne spodnie ";

    if(this_object()->query_worn())
      str+="si^egaj� d^lugo�ci� ";
    else
      str+="mog� si^ega^c d^lugo�ci� ";

    str+="a^z do ziemi. Obszerne kieszenie mog^ly niegdy^s pomie^sci^c "+
        "wiele rzeczy, teraz jednak widniej^a w nich tylko du^ze dziury. "+
        "Spodnie na ca^lej d^lugo^sci s^a szerokie i przewiewne, dzieki "+
        "czemu nie kr^epuj^a ruch^ow. Kr^oj - prosty i niezbyt gustowny "+
        "zupe^lnie nie przyci^aga wzroku. Jednak du^zym plusem spodni jest "+
        "czarny materia^l, z kt^orego zosta^ly uszyte - praktycznie nie "+
        "wida^c na nim plam i zabrudze^n";
    return str;
}