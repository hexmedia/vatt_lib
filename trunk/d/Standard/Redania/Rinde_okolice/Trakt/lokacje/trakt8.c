/* Autor: Avard
   Opis : 30.12.06
   Data : Brzozek */
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"
#include <pogoda.h>

inherit TRAKT_RINDE_STD;

void create_trakt() 
{
    set_short("Trakt na skraju wzg^orza");
    add_exit(TRAKT_RINDE_LOKACJE + "trakt6.c","e",0,TRAKT_RO_FATIG,0);
    add_exit(TRAKT_RINDE_LOKACJE + "trakt9.c","sw",0,TRAKT_RO_FATIG,0);
    add_exit(LAKA_RINDE_LOKACJE + "laka28.c","n",0,TRAKT_RO_FATIG,0);
    add_exit(LAKA_RINDE_LOKACJE + "laka29.c","nw",0,TRAKT_RO_FATIG,0);
    add_exit(LAKA_RINDE_LOKACJE + "laka34.c","w",0,TRAKT_RO_FATIG,0);
    add_sit("na murku","na murku","z murku",10);

    add_prop(ROOM_I_INSIDE,0);
    add_prop(ROOM_I_WSP_Y, WSP_Y_RINDE); //Wsp�rz�dne, do systemu pogodowego.
    add_prop(ROOM_I_WSP_X, WSP_X_RINDE);
}
public string
exits_description() 
{
    return "Trakt prowadzi na po^ludniowy-zach^od i wsch^od.\n";
}

string
dlugi_opis()
{
    string str;
    if(jest_dzien() == 1)
    {
        if(pora_roku() != MT_ZIMA)
        {
            str = "Szeroki, wygodny trakt wy^lo^zony dopasowanymi "+
                "kamieniami, rozszerzaj^acy si^e w kierunku wschodnim w "+
                "obszerny placyk ^lagodnie opada na zach^od, skrajem "+
                "piaszczystego wzg^orza wznosz^acego si^e nad bagnistym "+
                "rozlewiskiem Pontaru. W stertach pokruszonych kamieni "+
                "wznosz^acych si^e po obu stronach drogi z trudem mo^zna "+
                "rozpozna^c pozosta^lo^sci po niewysokich kamiennych "+
                "murkach zbudowanych niegdy^s z bry^l piaskowca, a obecnie "+
                "zniszczonych przez wiatry i deszcze, poro^sni^etych przez "+
                "ma^le krzaczki i ruderalne zielsko, a i nie oszcz^edzanych "+
                "przez w^edrowc^ow przysiadaj^acych tutaj w oczekiwaniu na "+
                "otwarcie bram miasta. Widoczne na skraju ^lagodnego "+
                "wzg^orza na po^ludniowym-wschodzie podobne kamieniste "+
                "pozosta^lo^sci ci^agn^a si^e wzd^lu^z le^z^acego tam "+
                "traktu do Muriviel i Ellander, a za rozci^agaj^acym si^e "+
                "na po^ludniu u st^op stromej skarpy mokrad^lem "+
                "poro^sni^etym przez bagienne trawy i rachityczne drzewka ";
            if(MOC_OPADOW(TO) == NIC_NIE_PADA)
            {
                str += "po^lyskuj^a w oddali wody majestatycznej rzeki";
            }
            else
            {
                str += "przewalaj^a si^e hucz^ac bure fale majestatycznej "+
                    "rzeki";
            }
            str += ". G^este ga^l^ezie k^epy drzew rosn^acych w^sr^od "+
                "kamieni na p^o^lnocnym-zachodzie, pokryte delikatnymi "+
                "zielonymi listkami przes^laniaj^a ci^agn^ace si^e a^z po "+
                "ciemn^a kres^e lasu na zachodzie i horyzont na p^o^lnocy, ";
            if(pora_roku() == MT_LATO || pora_roku() == MT_WIOSNA)
            {
                str += "trawiastych r^ownin.";
            }
            if(pora_roku() == MT_JESIEN)
            {
                str += "jasnozielonych trawiastych r^ownin.";
            }
            if(CO_PADA(TO) == PADA_DESZCZ)
            {
                str += "zrudzia^lych od deszcz^ow trawiastych r^ownin.";
            }
        }
        else
        {
            str = "Szeroki, wygodny trakt wy^lo^zony dopasowanymi "+
                "kamieniami, rozszerzaj^acy si^e w kierunku wschodnim w "+
                "obszerny placyk ^lagodnie opada na zach^od skrajem "+
                "wzg^orza przypominaj^acego o tej porze roku olbrzymi^a "+
                "^snie^zn^a zasp^e wznosz^ac^a si^e nad zamarzni^etym "+
                "rozlewiskiem Pontaru. Dosy^c du^ze, pokryte warstw^a "+
                "^sniegu i lodu pod^lu^zne wzg^orki ci^agn^a si^e po obu "+
                "stronach drogi, a podobne twory przyrody wida^c te^z "+
                "wzd^lu^z ciemnej kresy traktu do Muriviel i Ellander, "+
                "widocznej na skraju za^snie^zonego wzg^orza na "+
                "po^ludniowym-wschodzie. Za rozci^agaj^ac^a si^e na "+
                "po^ludniu, u st^op stromej skarpy, zlodowacia^l^a "+
                "powierzchni^a mokrad^la ";
            if(MOC_OPADOW(TO) == NIC_NIE_PADA)
            {
                str += "po^lyskuj^a wody majestatycznej rzeki";
            }
            else
            {
                str += "przewalaj^a si^e hucz^ac bure fale majestatycznej "+
                    "rzeki";
            }
            str += "G^este ga^l^ezie k^epy drzew rosn^acych w^sr^od kamieni "+
                "na p^o^lnocnym-zachodzie, pokryte ";
            if(CO_PADA(TO) != PADA_SNIEG)
            {
                str += "lodow^a skorup^a ";
            }
            else
            {
                str += "grub^a warstw^a ^sniegu ";
            }
            str += "przes^laniaj^a ci^agn^ace si^e a^z po ciemn^a kres^e "+
                "lasu na zachodzie i horyzont na p^o^lnocy ^snie^zne pola.";
        }
    }
    if(jest_dzien() == 0)
    {
        if(pora_roku() != MT_ZIMA)
        {
            str = "Szeroki, wygodny trakt wy^lo^zony dopasowanymi "+
                "kamieniami, rozszerzaj^acy si^e w kierunku wschodnim w "+
                "obszerny placyk ^lagodnie opada na zach^od, skrajem "+
                "piaszczystego wzg^orza wznosz^acego si^e nad bagnistym "+
                "rozlewiskiem Pontaru. W stertach pokruszonych kamieni "+
                "wznosz^acych si^e po obu stronach drogi z trudem mo^zna "+
                "rozpozna^c pozosta^lo^sci po niewysokich kamiennych "+
                "murkach zbudowanych niegdy^s z bry^l piaskowca, a obecnie "+
                "zniszczonych przez wiatry i deszcze, poro^sni^etych przez "+
                "ma^le krzaczki i ruderalne zielsko, a i nie oszcz^edzanych "+
                "przez w^edrowc^ow przysiadaj^acych tutaj w oczekiwaniu na "+
                "otwarcie bram miasta. Widoczne na skraju ^lagodnego "+
                "wzg^orza na po^ludniowym-wschodzie podobne kamieniste "+
                "pozosta^lo^sci ci^agn^a si^e wzd^lu^z le^z^acego tam "+
                "traktu do Muriviel i Ellander, a za rozci^agaj^acym si^e "+
                "na po^ludniu u st^op stromej skarpy mokrad^lem "+
                "poro^sni^etym przez bagienne trawy i rachityczne drzewka ";
            if(MOC_OPADOW(TO) == NIC_NIE_PADA)
            {
                str += "po^lyskuj^a w oddali wody majestatycznej rzeki";
            }
            else
            {
                str += "przewalaj^a si^e hucz^ac bure fale majestatycznej "+
                    "rzeki";
            }
            str += ". G^este ga^l^ezie k^epy drzew rosn^acych w^sr^od "+
                "kamieni na p^o^lnocnym-zachodzie, pokryte delikatnymi "+
                "zielonymi listkami przes^laniaj^a ";
            if(ZACHMURZENIE(TO) != POG_ZACH_ZEROWE)
            {
                str += "majacz^ace w ciemno^sciach ";
            }
            else
            {
                str += "l^sni^ace w blasku ksi^e^zyca ";
            }
            if(pora_roku() == MT_LATO || pora_roku() == MT_WIOSNA)
            {
                str += "trawiastych r^ownin.";
            }
            if(pora_roku() == MT_JESIEN)
            {
                str += "jasnozielonych trawiastych r^ownin.";
            }
            if(CO_PADA(TO) == PADA_DESZCZ)
            {
                str += "zrudzia^lych od deszcz^ow trawiastych r^ownin.";
            }
        }
        else
        {
            str = "Szeroki, wygodny trakt wy^lo^zony dopasowanymi "+
                "kamieniami, rozszerzaj^acy si^e w kierunku wschodnim w "+
                "obszerny placyk ^lagodnie opada na zach^od skrajem "+
                "wzg^orza przypominaj^acego o tej porze roku olbrzymi^a "+
                "^snie^zn^a zasp^e wznosz^ac^a si^e nad zamarzni^etym "+
                "rozlewiskiem Pontaru. Dosy^c du^ze, pokryte warstw^a "+
                "^sniegu i lodu pod^lu^zne wzg^orki ci^agn^a si^e po obu "+
                "stronach drogi a podobne twory przyrody wida^c te^z "+
                "wzd^lu^z ciemnej kresy traktu do Muriviel i Ellander "+
                "widocznej na skraju za^snie^zonego wzg^orza na "+
                "po^ludniowym-wschodzie. Za rozci^agaj^ac si^e na "+
                "po^ludniu, u st^op stromej skarpy zlodowacia^l^a "+
                "powierzchni^a mokrad^la ";
            if(MOC_OPADOW(TO) == NIC_NIE_PADA)
            {
                str += "po^lyskuj^a wody majestatycznej rzeki";
            }
            else
            {
                str += "przewalaj^a si^e hucz^ac bure fale majestatycznej "+
                    "rzeki";
            }
            str += "G^este ga^l^ezie k^epy drzew rosn^acych w^sr^od "+
                "kamieni na p^o^lnocnym-zachodzie, pokryte ";
            if(CO_PADA(TO) != PADA_SNIEG)
            {
                str += "lodow^a skorup^a ";
            }
            else
            {
                str += "grub^a warstw^a ^sniegu ";
            }
            str += "przes^laniaj^a ";
            if(ZACHMURZENIE(TO) != POG_ZACH_ZEROWE)
            {
                str += "majacz^ace w ciemno^sciach ";
            }
            else
            {
                str += "l^sni^ace w blasku ksi^e^zyca ";
            } 
            str += "^snie^zne pola. ";
        }
    }
    str += "\n";
    return str;
}
string
opis_nocy()
{
    string str;
    if(pora_roku() != MT_ZIMA)
        {
            str = "Szeroki, wygodny trakt wy^lo^zony dopasowanymi "+
                "kamieniami, rozszerzaj^acy si^e w kierunku wschodnim w "+
                "obszerny placyk ^lagodnie opada na zach^od, skrajem "+
                "piaszczystego wzg^orza wznosz^acego si^e nad bagnistym "+
                "rozlewiskiem Pontaru. W stertach pokruszonych kamieni "+
                "wznosz^acych si^e po obu stronach drogi z trudem mo^zna "+
                "rozpozna^c pozosta^lo^sci po niewysokich kamiennych "+
                "murkach zbudowanych niegdy^s z bry^l piaskowca, a obecnie "+
                "zniszczonych przez wiatry i deszcze, poro^sni^etych przez "+
                "ma^le krzaczki i ruderalne zielsko, a i nie oszcz^edzanych "+
                "przez w^edrowc^ow przysiadaj^acych tutaj w oczekiwaniu na "+
                "otwarcie bram miasta. Widoczne na skraju ^lagodnego "+
                "wzg^orza na po^ludniowym-wschodzie podobne kamieniste "+
                "pozosta^lo^sci ci^agn^a si^e wzd^lu^z le^z^acego tam "+
                "traktu do Muriviel i Ellander, a za rozci^agaj^acym si^e "+
                "na po^ludniu u st^op stromej skarpy mokrad^lem "+
                "poro^sni^etym przez bagienne trawy i rachityczne drzewka ";
            if(MOC_OPADOW(TO) == NIC_NIE_PADA)
            {
                str += "po^lyskuj^a w oddali wody majestatycznej rzeki";
            }
            else
            {
                str += "przewalaj^a si^e hucz^ac bure fale majestatycznej "+
                    "rzeki";
            }
            str += ". G^este ga^l^ezie k^epy drzew rosn^acych w^sr^od "+
                "kamieni na p^o^lnocnym-zachodzie, pokryte delikatnymi "+
                "zielonymi listkami przes^laniaj^a ";
            if(ZACHMURZENIE(TO) != POG_ZACH_ZEROWE)
            {
                str += "majacz^ace w ciemno^sciach ";
            }
            else
            {
                str += "l^sni^ace w blasku ksi^e^zyca ";
            }
            if(pora_roku() == MT_LATO || pora_roku() == MT_WIOSNA)
            {
                str += "trawiastych r^ownin ";
            }
            if(pora_roku() == MT_JESIEN)
            {
                str += "jasnozielonych trawiastych r^ownin";
            }
            if(CO_PADA(TO) == PADA_DESZCZ)
            {
                str += "zrudzia^lych od deszcz^ow trawiastych r^ownin ";
            }
        }
        else
        {
            str = "Szeroki, wygodny trakt wy^lo^zony dopasowanymi "+
                "kamieniami, rozszerzaj^acy si^e w kierunku wschodnim w "+
                "obszerny placyk ^lagodnie opada na zach^od skrajem "+
                "wzg^orza przypominaj^acego o tej porze roku olbrzymi^a "+
                "^snie^zn^a zasp^e wznosz^ac^a si^e nad zamarzni^etym "+
                "rozlewiskiem Pontaru. Dosy^c du^ze, pokryte warstw^a "+
                "^sniegu i lodu pod^lu^zne wzg^orki ci^agn^a si^e po obu "+
                "stronach drogi a podobne twory przyrody wida^c te^z "+
                "wzd^lu^z ciemnej kresy traktu do Muriviel i Ellander "+
                "widocznej na skraju za^snie^zonego wzg^orza na "+
                "po^ludniowym-wschodzie. Za rozci^agaj^ac si^e na "+
                "po^ludniu, u st^op stromej skarpy zlodowacia^l^a "+
                "powierzchni^a mokrad^la ";
            if(MOC_OPADOW(TO) == NIC_NIE_PADA)
            {
                str += "po^lyskuj^a wody majestatycznej rzeki";
            }
            else
            {
                str += "przewalaj^a si^e hucz^ac bure fale majestatycznej "+
                    "rzeki";
            }
            str += "G^este ga^l^ezie k^epy drzew rosn^acych w^sr^od "+
                "kamieni na p^o^lnocnym-zachodzie, pokryte ";
            if(CO_PADA(TO) != PADA_SNIEG)
            {
                str += "lodow^a skorup^a ";
            }
            else
            {
                str += "grub^a warstw^a ^sniegu ";
            }
            str += "przes^laniaj^a ";
            if(ZACHMURZENIE(TO) != POG_ZACH_ZEROWE)
            {
                str += "majacz^ace w ciemno^sciach ";
            }
            else
            {
                str += "l^sni^ace w blasku ksi^e^zyca ";
            } 
            str += "^snie^zne pola. ";
        }
    str += "\n";
    return str;
}