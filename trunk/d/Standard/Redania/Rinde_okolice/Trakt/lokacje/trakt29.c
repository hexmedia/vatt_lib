/* Autor: Faevcia
   Opis : Faevcia
   Data : 11.1.2007 */
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"

inherit TRAKT_RINDE_STD;
inherit "/lib/drink_water";
void create_trakt() 
{
    set_short("Trakt");
    add_exit(TRAKT_RINDE_LOKACJE + "trakt28.c","n",0,TRAKT_RO_FATIG,0);
    add_exit(TRAKT_RINDE_LOKACJE + "trakt30.c","w",0,TRAKT_RO_FATIG,0);
    //add_exit(LAS_RINDE_LOKACJE + "las60.c","nw",0,TRAKT_RO_FATIG,0);
    //add_exit(LAS_RINDE_LOKACJE + "las67.c","e",0,TRAKT_RO_FATIG,0);
    set_drink_places(({"z rzeki","z pontaru","z Pontaru"}));

    add_prop(ROOM_I_INSIDE,0);

    add_item(({"Pontar","pontar","rzek^e"}),"P^lyn^acy tu od tysi^ecy lat "+
    "szeroki Pontar zdaje si^e by^c wci^a^z nieujarzmionym i niezdobytym, "+
    "wyznacza nie tylko granice mi^edzy kr^olestwami Redanii i Temeri, "+
    "ale i skutecznie ogranicza ludzkie zap^edy w wyrywaniu z r^ak "+
    "matki Natury wszystkiego, co sie jej nale^zy. Gdzieniegdzie "+
    "pojawiaj^ace si^e z nienacka niewielkie zawirowania wody na "+
    "spokojnie, jakby leniwie poruszaj^acej si^e m^etnej tafli swiadcz^a "+
    "o nieprzywidywalno^sci i zdradzieckiej naturze nurtu, kt^ory zapewne "+
    "pochlon^a^l niejedno istnienie bezmy^slnie probuj^ace zmierzy^c si^e z "+
    "pot^eg^a rzeki.\n");
}
public string
exits_description() 
{
    return "Trakt prowadzi na zach^od i p^o^lnoc.\n";
}
void
init()
{
    ::init();
    init_drink_water(); 
} 

string
dlugi_opis()
{
    string str;
    if(jest_dzien() == 1)
    {
        if(pora_roku() != MT_ZIMA)
        {
str = "Trakt w tym miejscu przypomina nieco slalom - ledwie "+
"min^e^lo si^e jeden zakr^et, a ju^z widac nast^epny. Droga "+
"nie jest zbyt dobra - zwyk^la, mocno ubita ziemia, pokryta "+
"niezliczon^a iloci^a kolein i wg^l^ebie^n. Szale^nczy galop "+
"wierzchem po takiej trasie musi by^c nie lada prze^zyciem. "+
"Trasa, kt^or^a przemierzasz niemal ze wszystkich stron "+
"otoczona jest drzewami. Las przerzedza si^e jedynie na "+
"po^ludniu, a mi^edzy smuk^lymi pniami brz^oz i sosen "+
"przeb^lyskuj^a b^l^ekitne wody Pontaru. Wzd^lu^z traktu "+
"rosn^a przer^o^zne krzewy - od pot^e^znych berberys^ow, "+
"poprzez do^c wysokie i wysmuk^le niczym kolumny ja^lowce, "+
"a^z po niewielkie, acz dorodne krzewinki bor^owek.";
        }
        else
        {
str = "Trakt ten przypomina nieco slalom - ledwie min^e^lo si^e "+
"jeden zakr^et, a ju^z wida^c nast^epny. Droga nie jest zbyt "+
"dobra - zwyk^la, mocno ubita ziemia, nie do^s^c, ^ze pokryta "+
"niezliczon^a iloci^e kolein i wg^l^ebie^n, to jeszcze w "+
"dodatku sniegiem. Szale^nczy galop wierzchem po takiej trasie "+
"musi  by^c ne lada prze^zyciem. Trasa, kt^or^a przemierzasz, "+
"niemal ze wszystkich stron otoczona jest drzewami. Widok jest "+
"zaiste przepi^ekny - wszystko wok^o^l pokryte jest jednakowej "+
"grubo^sci warstw^a bielutkiego ^sniegu. Na po^ludniu, mi^edzy "+
"brzozami i sosnami toczy swe wody pokryty pot^e^znymi krami "+
"Pontar. Wzd^lu^z traktu rosn^a przer^o^zne krzewy - g^l^ownie "+
"berberysy i ja^lowce, ich ga^l^ezie s^a oblodzone, a gdzieniegdzie "+
"zwisaj^a niewielkie sopelki.";
        }
    }
    if(jest_dzien() == 0)
    {
        if(pora_roku() != MT_ZIMA)
        {
str = "Trakt w tym miejscu przypomina nieco slalom - ledwie "+
"min^e^lo si^e jeden zakr^et, a ju^z wida^c nast^epny. Droga "+
"nie jest zbyt dobra - zwyk^la, mocno ubita ziemia, pokryta "+
"niezliczon^a iloci^a kolein i wg^l^ebie^n, kt^ore, ze "+
"wzgl^edu jako^sci o^swietlenia - nie s^a mo^ze zbyt "+
"widoczne, ale za to doskonale wyczuwalne. Szale^nczy galop "+
"wierzchem po takiej trasie musi by^c nie lada prze^zyciem, "+
"^swiadcz^acym o odwadze, a jednocze^snie beztrosce i "+
"g^lupocie je^xd^xca. Trasa, kt^or^a przemierzasz, chyba ze "+
"wszystkich stron otoczona jest drzewami, jednak od po^ludnia "+
"s^lycha^c szum przelewajacych si^e ogromnych mas wody. "+
"Granic^e traktu doskonale wyznaczaj^a g^esto rosn^ace "+
"wzd^lu^z niego krzewy. Jednak jest tak ciemno, ^ze trudno "+
"rozpozna^c ich gatunki.";
        }
        else
        {
str = "Trakt w tym miejscu przypomina nieco slalom - ledwie "+
"min^e^lo si^e jeden zakr^et - ju^z wida^c nast^epny. Droga "+
"nie jest zbyt dobra - zwyk^la, mocno ubita ziemia, nie "+
"do^s^c, ^ze pokryta ^sniegiem, to jeszcze niezliczon^a "+
"ilo^sci^a kolein i wg^l^ebie^n, kt^ore, ze wzgl^edu na "+
"por^e i jako^s^c o^swietlenia nie s^a zbyt widoczne, ale za "+
"to doskonale wyczuwalne. Szale^nczy galop wierzchem po takiej "+
"trasie i w takich warunkach musi by^c nie lada prze^zyciem "+
"^swiadcz^acym o odwadze, a jednocze^snie o beztrosce i "+
"g^lupocie je^xd^xca. Trasa, kt^or^a przemierzasz, chyba, "+
"^ze wszystkich stron otoczona jest drzewami, jednak od "+
"po^ludnia dochodzi ci^e szumi^acy odg^los przelewaj^acych "+
"si^e ogromnych mas wody, a panuj^ac^a wok^o^l cisz^e od "+
"czasu do czasu przerywa odg^los ^lami^acej si^e kry. "+
"Granic^e traktu doskonale wyznaczaj^a g^esto rosn^ace "+
"wzd^lu^z niego jakie^s krzewy, wygl^adaj^ace w "+
"ciemno^sciach do^s^c mrocznie - ich ga^l^ezie - pokryte "+
"lodem i ^sniegiem wygl^adaj^a troch^e jak szpony jakiego^s "+
"ba^sniowego stwora.";
        }
    }
    str += "\n";
    return str;
}
string
opis_nocy()
{
    string str;
    if(pora_roku() != MT_ZIMA)
    {
str = "Trakt w tym miejscu przypomina nieco slalom - ledwie min^e^lo "+
"si^e jeden zakr^et, a ju^z wida^c nast^epny. Droga nie jest "+
"zbyt dobra - zwyk^la, mocno ubita ziemia, pokryta niezliczon^a "+
"ilo^sci^a kolein i wg^l^ebie^n, kt^ore nie s^a mo^ze zbyt "+
"widoczne, ale za to doskonale wyczuwalne. Szale^nczy galop "+
"wierzchem po takiej trasie musi by^c nie lada prze^zyciem, "+
"^swiadcz^acym o odwadze, a jednocze^snie o beztrosce i g^lupocie "+
"je^xd^xca. Trasa, kt^or^a przemierzasz, chyba ze wszystkich "+
"stron otoczona jest drzewami, jednak od po^ludnia s^lycha^c "+
"szum przelewaj^acych si^e ogromnych mas wody. Granic^e traktu "+
"doskonale wyznaczaj^a g^esto rosn^ace wzd^lu^z niego krzewy. "+
"Jednak jest tak ciemno, ^ze wiadomo o nich tylko tyle, ^ze s^a. ";
  }
    else
    {
 str = "Trakt w tym miejscu przypomina nieco slalom - ledwie "+
"min^e^lo si^e jeden zakr^et - ju^z wida^c nast^epny. Droga "+
"nie jest zbyt dobra - zwyk^la, mocno ubita ziemia, nie "+
"do^s^c, ^ze pokryta ^sniegiem i diabelsko ^sliskie, to "+
"jeszcze niezliczon^a ilo^sci^a kolein i wg^l^ebie^n, kt^ore, "+
"nie s^a mo^ze zbyt widoczne, ale za to doskonale wyczuwalne. "+
"Szale^nczy galop wierzchem po takiej trasie i w takich "+
"warunkach musi by^c nie lada prze^zyciem ^swiadcz^acym o "+
"odwadze, a jednocze^snie o beztrosce i g^lupocie je^xd^xca. "+
"Trasa, kt^or^a przemierzasz, chyba, ze wszystkich stron "+
"otoczona jest drzewami, jednak od po^ludnia dochodzi ci^e "+
"szumi^acy odg^los przelewaj^acych si^e ogromnych mas wody, "+
"a panuj^ac^a wok^o^l cisz^e od czasu do czasu przerywa "+
"odg^los ^lami^acej si^e kry. Granic^e traktu doskonale "+
"wyznaczaj^a g^esto rosn^ace wzd^lu^z niego jakie^s krzewy "+
"- pokryte lodem i ^sniegiem wygl^adaj^a troch^e jak szpony "+
"jakiego^s ba^sniowego stwora.";
    }
    str += "\n";
    return str;
}
