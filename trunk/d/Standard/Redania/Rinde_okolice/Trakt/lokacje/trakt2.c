/* Autor: 
   Opis : 
   Data : */
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"

inherit TRAKT_RINDE_STD;

void create_trakt() 
{
    set_short("Trakt");
    add_exit(TRAKT_RINDE_LOKACJE + "trakt3.c","nw",0,TRAKT_RO_FATIG,0);
    add_exit(TRAKT_RINDE_LOKACJE + "trakt1.c","se",0,TRAKT_RO_FATIG,0);

    add_prop(ROOM_I_INSIDE,0);
    add_prop(ROOM_I_WSP_Y, WSP_Y_RINDE); //Wsp�rz�dne, do systemu pogodowego.
    add_prop(ROOM_I_WSP_X, WSP_X_RINDE);
}
public string
exits_description() 
{
    return "Trakt prowadzi na p^o^lnocny-zach^od i po^ludniowy-wsch^od.\n";
}

string
dlugi_opis()
{
    string str;

    if(jest_dzien())
    {
        if(pora_roku() != MT_ZIMA)
        {
    str = "Opis traktu w dzien, !zima";
        }
        else
        {
    str = "Opis traktu w dzien, zima";
        }
    }
    else
    {
        if(pora_roku() != MT_ZIMA)
        {
    str = "Opis traktu noc, !zima.";
        }
        else
        {
    str = "Opis traktu noc, zima.";
        }
    }
    str += "\n";

    return str;
}