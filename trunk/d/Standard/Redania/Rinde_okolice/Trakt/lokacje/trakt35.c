/* Autor: Avard
   Opis : Yran
   Data : 29.11.06 */
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"

inherit TRAKT_RINDE_STD;
inherit "/lib/drink_water";
#define DEF_DIRS ({"po^ludniowy-zach^od"})
void create_trakt() 
{
    set_short("Na trakcie");
	add_exit(TRAKT_BIALY_MOST_OKOLICE_LOKACJE + "t18.c","w",0,TRAKT_RO_FATIG,0);
    add_exit(TRAKT_RINDE_LOKACJE + "trakt34.c","e",0,TRAKT_RO_FATIG,0);
    add_exit(LAS_RINDE_LOKACJE + "las70.c","n",0,TRAKT_RO_FATIG,0);
    add_exit(LAS_RINDE_LOKACJE + "las69.c","ne",0,TRAKT_RO_FATIG,0);

    set_drink_places(({"z rzeki","z pontaru","z Pontaru"}));
    add_prop(ROOM_I_INSIDE,0);
    
    add_item(({"Pontar","pontar","rzek^e"}),"P^lyn^acy tu od tysi^ecy lat "+
    "szeroki Pontar zdaje si^e by^c wci^a^z nieujarzmionym i niezdobytym, "+
    "wyznacza nie tylko granice mi^edzy kr^olestwami Redanii i Temeri, "+
    "ale i skutecznie ogranicza ludzkie zap^edy w wyrywaniu z r^ak "+
    "matki Natury wszystkiego, co sie jej nale^zy. Gdzieniegdzie "+
    "pojawiaj^ace si^e z nienacka niewielkie zawirowania wody na "+
    "spokojnie, jakby leniwie poruszaj^acej si^e m^etnej tafli swiadcz^a "+
    "o nieprzywidywalno^sci i zdradzieckiej naturze nurtu, kt^ory zapewne "+
    "pochlon^a^l niejedno istnienie bezmy^slnie probuj^ace zmierzy^c si^e z "+
    "pot^eg^a rzeki.\n");
    
}

void
init()
{
    ::init();
    init_drink_water(); 
} 
public string
exits_description() 
{
    return "Trakt prowadzi na zach^od i wsch^od.\n";
}

string
dlugi_opis()
{
    string str;
    if(jest_dzien() == 1)
    {
        if(pora_roku() != MT_ZIMA)
        {
            str = "Scie^zka rozpoczyna w tym miejscu swoj^a wspinaczk^e "+
                "na jednym z okolicznych pag^ork^ow. Cho^c nie jest wysoki "+
                "to niemal ca^lkowicie przes^lania wszystko, co znajduje "+
                "si^e na zachodzie. Jak okiem si^egn^a^c na p^o^lnocy "+
                "rozpo^sciera si^e widok na masywn^a scian^e li^sciastego "+
                "lasu, kt^orego ogromne, majestatycznie pn^ace si^e ku "+
                "g^orze drzewa rzucaj^a cie^n na spore po^lacie okolicznego "+
                "terenu. Na poludniu wyra^xnie widoczne s^a leniwie "+
                "p^lyn^ace wody Pontaru, kt^ore z cichym szumem rozbijaj^a "+
                "si^e o wystaj^ace ponad tafle wody kamienie. Nieco szersza "+
                "droga, udeptane pod^lo^ze i wykarczowane przydro^zne "+
                "ro^sliny ^swiadcz^a, ^ze trakt ten jest codziennie "+
                "ucz^eszczany.";
        }
        else
        {
            str = "Pokryta warstw^a bia^lego puchu ^scie^zka rozpoczyna w "+
                "tym miejscu swoj^a wspinaczk^e na jeden z okolicznych "+
                "pag^ork^ow. Cho^c nie jest wysoki to niemal ca^lkowicie "+
                "przes^lania wszystko, co znajduje si^e na zachodzie. Jak "+
                "okiem si^egn^a^c na p^o^lnocy rozpo^sciera si^e widok na "+
                "masywn^a scian^e nagiego, bezlistnego lasu, kt^orego "+
                "ogromne, majestatycznie pn^ace si^e ku g^orze drzewa "+
                "rzucaj^a cie^n na spore po^lacie okolicznego terenu. Na "+
                "poludniu wyra^xnie widoczne s^a leniwie p^lyn^ace wody "+
                "Pontaru. Kra bezw^ladnie dryfuj^aca wraz z biegiem rzeki "+
                "rozbija si^e na mniejsze kawa^lki o wystaj^ace ponad "+
                "tafle wody kamienie. Nieco szersza droga i udeptany "+
                "^snieg zalegaj^acy na pod^lo^zu ^swiadcz^a, ^ze trakt "+
                "ten jest codziennie ucz^eszczany.";
        }    
    }
    if(jest_dzien() == 0)
    {
        if(pora_roku() != MT_ZIMA)
        {
            str = "Scie^zka rozpoczyna w tym miejscu swoj^a wspinaczk^e "+
                "na jednym z okolicznych pag^ork^ow. Cho^c nie jest wysoki "+
                "to niemal ca^lkowicie przes^lania wszystko, co znajduje "+
                "si^e na zachodzie. Jak okiem si^egn^a^c na p^o^lnocy "+
                "rozpo^sciera si^e widok na masywn^a scian^e li^sciastego "+
                "lasu, kt^orego ogromne, majestatycznie pn^ace si^e ku "+
                "g^orze drzewa rzucaj^a cie^n na spore po^lacie okolicznego "+
                "terenu. Na poludniu wyra^xnie widoczne s^a leniwie "+
                "p^lyn^ace wody Pontaru, kt^ore z cichym szumem rozbijaj^a "+
                "si^e o wystaj^ace ponad tafle wody kamienie. Nieco szersza "+
                "droga, udeptane pod^lo^ze i wykarczowane przydro^zne "+
                "ro^sliny ^swiadcz^a, ^ze trakt ten jest codziennie "+
                "ucz^eszczany.";
        }
        else
        {
            str = "Pokryta warstw^a bia^lego puchu ^scie^zka rozpoczyna w "+
                "tym miejscu swoj^a wspinaczk^e na jeden z okolicznych "+
                "pag^ork^ow. Cho^c nie jest wysoki to niemal ca^lkowicie "+
                "przes^lania wszystko, co znajduje si^e na zachodzie. Jak "+
                "okiem si^egn^a^c na p^o^lnocy rozpo^sciera si^e widok na "+
                "masywn^a scian^e nagiego, bezlistnego lasu, kt^orego "+
                "ogromne, majestatycznie pn^ace si^e ku g^orze drzewa "+
                "rzucaj^a cie^n na spore po^lacie okolicznego terenu. Na "+
                "poludniu wyra^xnie widoczne s^a leniwie p^lyn^ace wody "+
                "Pontaru. Kra bezw^ladnie dryfuj^aca wraz z biegiem rzeki "+
                "rozbija si^e na mniejsze kawa^lki o wystaj^ace ponad "+
                "tafle wody kamienie. Nieco szersza droga i udeptany "+
                "^snieg zalegaj^acy na pod^lo^zu ^swiadcz^a, ^ze trakt "+
                "ten jest codziennie ucz^eszczany.";
        }      
    }
    str +="\n";
    return str;
}
string
opis_nocy()
{
    string str;
    if(pora_roku() != MT_ZIMA)
    {
        str = "Scie^zka rozpoczyna w tym miejscu swoj^a wspinaczk^e "+
            "na jednym z okolicznych pag^ork^ow. Cho^c nie jest wysoki "+
            "to niemal ca^lkowicie przes^lania wszystko, co znajduje "+
            "si^e na zachodzie. Jak okiem si^egn^a^c na p^o^lnocy "+
            "rozpo^sciera si^e widok na masywn^a scian^e li^sciastego "+
            "lasu, kt^orego ogromne, majestatycznie pn^ace si^e ku "+
            "g^orze drzewa rzucaj^a cie^n na spore po^lacie okolicznego "+
            "terenu. Na poludniu wyra^xnie widoczne s^a leniwie "+
            "p^lyn^ace wody Pontaru, kt^ore z cichym szumem rozbijaj^a "+
            "si^e o wystaj^ace ponad tafle wody kamienie. Nieco szersza "+
            "droga, udeptane pod^lo^ze i wykarczowane przydro^zne "+
            "ro^sliny ^swiadcz^a, ^ze trakt ten jest codziennie "+
            "ucz^eszczany.";
    }
    else
    {
        str = "Pokryta warstw^a bia^lego puchu ^scie^zka rozpoczyna w "+
            "tym miejscu swoj^a wspinaczk^e na jeden z okolicznych "+
            "pag^ork^ow. Cho^c nie jest wysoki to niemal ca^lkowicie "+
            "przes^lania wszystko, co znajduje si^e na zachodzie. Jak "+
            "okiem si^egn^a^c na p^o^lnocy rozpo^sciera si^e widok na "+
            "masywn^a scian^e nagiego, bezlistnego lasu, kt^orego "+
            "ogromne, majestatycznie pn^ace si^e ku g^orze drzewa "+
            "rzucaj^a cie^n na spore po^lacie okolicznego terenu. Na "+
            "poludniu wyra^xnie widoczne s^a leniwie p^lyn^ace wody "+
            "Pontaru. Kra bezw^ladnie dryfuj^aca wraz z biegiem rzeki "+
            "rozbija si^e na mniejsze kawa^lki o wystaj^ace ponad "+
            "tafle wody kamienie. Nieco szersza droga i udeptany "+
            "^snieg zalegaj^acy na pod^lo^zu ^swiadcz^a, ^ze trakt "+
            "ten jest codziennie ucz^eszczany.";
    }      
    str +="\n";
    return str;
}
