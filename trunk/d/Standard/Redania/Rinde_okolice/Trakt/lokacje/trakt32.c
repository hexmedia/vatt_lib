/* Autor: Avard
   Opis : Yran
   Data : 03.12.06 */
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"

inherit TRAKT_RINDE_STD;
inherit "/lib/drink_water";
void create_trakt() 
{
    set_short("Trakt");
    add_exit(TRAKT_RINDE_LOKACJE + "trakt31.c","ne",0,TRAKT_RO_FATIG,0);
    add_exit(TRAKT_RINDE_LOKACJE + "trakt33.c","w",0,TRAKT_RO_FATIG,0);
    add_exit(LAS_RINDE_LOKACJE + "las62.c","nw",0,TRAKT_RO_FATIG,0);
    add_exit(LAS_RINDE_LOKACJE + "las61.c","n",0,TRAKT_RO_FATIG,0);
    //add_exit(LAS_RINDE_LOKACJE + "las68.c","e",0,TRAKT_RO_FATIG,0);
    set_drink_places(({"z rzeki","z pontaru","z Pontaru"}));

    add_prop(ROOM_I_INSIDE,0);

    add_item(({"Pontar","pontar","rzek^e"}),"P^lyn^acy tu od tysi^ecy lat "+
    "szeroki Pontar zdaje si^e by^c wci^a^z nieujarzmionym i niezdobytym, "+
    "wyznacza nie tylko granice mi^edzy kr^olestwami Redanii i Temeri, "+
    "ale i skutecznie ogranicza ludzkie zap^edy w wyrywaniu z r^ak "+
    "matki Natury wszystkiego, co sie jej nale^zy. Gdzieniegdzie "+
    "pojawiaj^ace si^e z nienacka niewielkie zawirowania wody na "+
    "spokojnie, jakby leniwie poruszaj^acej si^e m^etnej tafli swiadcz^a "+
    "o nieprzywidywalno^sci i zdradzieckiej naturze nurtu, kt^ory zapewne "+
    "pochlon^a^l niejedno istnienie bezmy^slnie probuj^ace zmierzy^c si^e z "+
    "pot^eg^a rzeki.\n");
}
public string
exits_description() 
{
    return "Trakt prowadzi na zach^od i p^o^lnocny-wsch^od.\n";
}
void
init()
{
    ::init();
    init_drink_water(); 
} 
string
dlugi_opis()
{
    string str;
    if(jest_dzien() == 1)
    {
        if(pora_roku() != MT_ZIMA)
        {
            str = "Nieopodal rozci^aga si^e ^sciana li^sciastego lasu "+
                "zajmuj^acego po^lnocne po^lacie. Lekki wiatr wiej^acy z "+
                "zachodu ko^lysze pn^acymi si^e majestatycznie drzewami "+
                "g^oruj^acymi nad ca^la okolic^a. Boki ubitego traktu "+
                "porastaj^a liczne krzewy i k^epy zielonych traw, kt^ore "+
                "p^lynnie ^l^acz^a si^e z okalaj^acym drog^e lasem. Nad "+
                "suchym pod^lo^zem unosi si^e py^l dra^zni^acy nozdrza. "+
                "Bez trudu mo^zna dostrzec w nim ^slady licznych but^ow i "+
                "k^o^l karawan co jest tylko dowodem na cz^este "+
                "u^zytkowanie owej drogi. Do twych uszu dochodzi ^spiew "+
                "ptak^ow, kt^ore przysiad^ly na jednej z ga^l^ezi "+
                "rosn^acego tu^z przy ^scie^zce roz^lo^zystego d^ebu.";
        }
        else
        {
            str = "Nieopodal rozci^aga si^e ^sciana, bezlistnego, "+
                "zapad^lego w zimowy sen lasu zajmuj^acego po^lnocne "+
                "po^lacie. Lekki wiatr wiej^acy z zachodu ko^lysze "+
                "pn^acymi si^e majestatycznie drzewami g^oruj^acymi nad "+
                "ca^la okolic^a. Boki pokrytego ubitym ^sniegiem trakty "+
                "porastaj^a kolczaste krzewy i nieliczne k^epy traw, "+
                "kt^ore zdo^la^ly przebi^c poprzez zalegaj^aca na ziemi "+
                "warstwe bia^lego puchu. W ^sniegu bez trudu mo^zna "+
                "dostrzec ^slady but^ow i k^o^l karawan co jest tylko "+
                "dowodem na cz^este u^zytkowanie owej drogi. Do twych "+
                "uszu dochodzi ^spiew ptak^ow, kt^ore przysiad^ly na "+
                "jednej z ga^l^ezi rosn^acego tu^z przy ^scie^zce "+
                "roz^lo^zystego d^ebu.";
        }
    }
    if(jest_dzien() == 0)
    {
        if(pora_roku() != MT_ZIMA)
        {
            str = "Nieopodal rozci^aga si^e ^sciana li^sciastego lasu "+
                "zajmuj^acego po^lnocne po^lacie. Lekki wiatr wiej^acy z "+
                "zachodu ko^lysze pn^acymi si^e majestatycznie drzewami "+
                "g^oruj^acymi nad ca^la okolic^a. Boki ubitego traktu "+
                "porastaj^a liczne krzewy i k^epy zielonych traw, kt^ore "+
                "p^lynnie ^l^acz^a si^e z okalaj^acym drog^e lasem. Nad "+
                "suchym pod^lo^zem unosi si^e py^l dra^zni^acy nozdrza. "+
                "Bez trudu mo^zna dostrzec w nim ^slady licznych but^ow i "+
                "k^o^l karawan co jest tylko dowodem na cz^este "+
                "u^zytkowanie owej drogi. ";
        }
        else
        {
            str = "Nieopodal rozci^aga si^e ^sciana, bezlistnego, "+
                "zapad^lego w zimowy sen lasu zajmuj^acego po^lnocne "+
                "po^lacie. Lekki wiatr wiej^acy z zachodu ko^lysze "+
                "pn^acymi si^e majestatycznie drzewami g^oruj^acymi nad "+
                "ca^la okolic^a. Boki pokrytego ubitym ^sniegiem trakty "+
                "porastaj^a kolczaste krzewy i nieliczne k^epy traw, "+
                "kt^ore zdo^la^ly przebi^c poprzez zalegaj^aca na ziemi "+
                "warstwe bia^lego puchu. W ^sniegu bez trudu mo^zna "+
                "dostrzec ^slady but^ow i k^o^l karawan co jest tylko "+
                "dowodem na cz^este u^zytkowanie owej drogi. ";
        }
    }
    str += "\n";
    return str;
}
string
opis_nocy()
{
    string str;
    if(pora_roku() != MT_ZIMA)
        {
            str = "Nieopodal rozci^aga si^e ^sciana li^sciastego lasu "+
                "zajmuj^acego po^lnocne po^lacie. Lekki wiatr wiej^acy z "+
                "zachodu ko^lysze pn^acymi si^e majestatycznie drzewami "+
                "g^oruj^acymi nad ca^la okolic^a. Boki ubitego traktu "+
                "porastaj^a liczne krzewy i k^epy zielonych traw, kt^ore "+
                "p^lynnie ^l^acz^a si^e z okalaj^acym drog^e lasem. Nad "+
                "suchym pod^lo^zem unosi si^e py^l dra^zni^acy nozdrza. "+
                "Bez trudu mo^zna dostrzec w nim ^slady licznych but^ow i "+
                "k^o^l karawan co jest tylko dowodem na cz^este "+
                "u^zytkowanie owej drogi. ";
        }
        else
        {
            str = "Nieopodal rozci^aga si^e ^sciana, bezlistnego, "+
                "zapad^lego w zimowy sen lasu zajmuj^acego po^lnocne "+
                "po^lacie. Lekki wiatr wiej^acy z zachodu ko^lysze "+
                "pn^acymi si^e majestatycznie drzewami g^oruj^acymi nad "+
                "ca^la okolic^a. Boki pokrytego ubitym ^sniegiem trakty "+
                "porastaj^a kolczaste krzewy i nieliczne k^epy traw, "+
                "kt^ore zdo^la^ly przebi^c poprzez zalegaj^aca na ziemi "+
                "warstwe bia^lego puchu. W ^sniegu bez trudu mo^zna "+
                "dostrzec ^slady but^ow i k^o^l karawan co jest tylko "+
                "dowodem na cz^este u^zytkowanie owej drogi. ";
        }
    str += "\n";
    return str;
}