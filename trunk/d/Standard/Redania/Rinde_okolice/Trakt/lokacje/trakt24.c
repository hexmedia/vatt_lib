/* Autor: Avard
   Opis : Tinardan 
   Data : 14.10.06 */
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"

inherit TRAKT_RINDE_STD;

void create_trakt() 
{
    set_short("Szeroki trakt");
    add_exit(TRAKT_RINDE_LOKACJE + "trakt23.c","e",0,TRAKT_RO_FATIG,0);
    add_exit(TRAKT_RINDE_LOKACJE + "trakt25.c","nw",0,TRAKT_RO_FATIG,0);
    add_exit(LAS_RINDE_LOKACJE + "las66.c",({"przedrzyj si^e przez krzaki",
        ({"przedziera si^e przez krzaki", "przedzieraj^a si^e przez krzaki",
        "przemyka pomi^edzy krzakami", "przedzierasz si^e przez krzaki"}),
        ({"wychodzi z krzak^ow", "wychodz^a z krzak^ow", 
        "wy^slizguje si^e z krzak^ow"})}));
    add_exit(LAS_RINDE_LOKACJE + "las58.c","w",0,TRAKT_RO_FATIG,0);
    add_exit(LAS_RINDE_LOKACJE + "las49.c","n",0,TRAKT_RO_FATIG,0);
    add_exit(LAS_RINDE_LOKACJE + "las48.c","ne",0,TRAKT_RO_FATIG,0);
    add_exit(LAS_RINDE_LOKACJE + "las66.c","s",0,TRAKT_RO_FATIG,0);

    add_prop(ROOM_I_INSIDE,0);
}
public string
exits_description() 
{
    return "Trakt prowadzi na p^o^lnocny-zach^od i wsch^od.\n";
}

string
dlugi_opis()
{
    string str;
    if(jest_dzien() == 1)
    {
    if(pora_roku() != MT_ZIMA)
    {
        str = "Trakt wije si^e niczym szary w^a^z po^sr^od zielonego "+
            "lasu. Ziemia jest ubita niemal^ze na kamie^n tysi^acami "+
            "ko^nskich kopyt, bosych i obutych st^op. Nier^owne "+
            "koleiny, ci^agn^ace si^e po obu stronach traktu ^swiadcz^a "+
            "o tym, ^ze handel jest w tych okolicach do^s^c ^zywy. "+
            "Trakt sunie pomi^edzy drzewami, ubity, cho^c nie "+
            "brukowany, ale wygodny i szeroki. Drzewa pochylaj^a si^e "+
            "nad drog^a, swymi d^lugimi ga^l^eziami muskaj^ac co "+
            "wy^zsze pud^la przeje^zdzaj^acych tutaj powoz^ow. Na po^ludniu "+
            "wida^c g^este krzaki, a gdzie^s dalej wzbiera niewyra^xny szum "+
            "rzeki, ^l^acz^ac i splataj^ac si^e z gwarem le^snego traktu.";
    }
    else
    {
        str = "Trakt wije si^e niczym szary w^a^z po^sr^od obsypanego "+
            "^sniegiem lasu. ^Snieg le^z^acy na drodze jest ubity i "+
            "szary. W niekt^orych miejscach pojawi^l si^e l^od "+
            "wy^slizgany tysi^acami ko^nskich kopyt, podeszwami but^ow "+
            "i ko^lami woz^ow. Nier^owne koleiny, ci^agn^ace si^e po "+
            "obu stronach traktu ^swiadcz^a o tym, ^ze handel jest w "+
            "tych okolicach do^s^c ^zywy. Trakt sunie pomi^edzy "+
            "drzewami, ubity, cho^c nie brukowany, ale wygodny i "+
            "szeroki. Drzewa pochylaj^a si^e nad drog^a, swymi d^lugimi "+
            "ga^l^eziami muskaj^ac co wy^zsze pud^la przeje^zdzaj^acych "+
            "tutaj powoz^ow. Na po^ludniu wida^c g^este krzaki, a gdzie^s "+
            "dalej wzbiera niewyra^xny szum rzeki, ^l^acz^ac i splataj^ac "+
            "si^e z gwarem le^snego traktu.";
    }
    }
    if(jest_dzien() == 0)
    {
    if(pora_roku() != MT_ZIMA)
    {
        str = "Trakt wije si^e niczym szary w^a^z po^sr^od zielonego "+
            "lasu. Ziemia jest ubita niemal^ze na kamie^n tysi^acami "+
            "ko^nskich kopyt, bosych i obutych st^op. Nier^owne "+
            "koleiny, ci^agn^ace si^e po obu stronach traktu ^swiadcz^a "+
            "o tym, ^ze handel jest w tych okolicach do^s^c ^zywy. "+
            "Trakt sunie pomi^edzy drzewami, ubity, cho^c nie "+
            "brukowany, ale wygodny i szeroki. Drzewa pochylaj^a si^e "+
            "nad drog^a, swymi d^lugimi ga^l^eziami muskaj^ac co "+
            "wy^zsze pud^la powoz^ow. Na po^ludniu wida^c g^este krzaki, "+
            "a gdzie^s dalej wzbiera niewyra^xny szum rzeki, ^l^acz^ac i "+
            "splataj^ac si^e z gwarem le^snego traktu.";
    }
    else
    {
        str = "Trakt wije si^e niczym szary w^a^z po^sr^od obsypanego "+
            "^sniegiem lasu. ^Snieg le^z^acy na drodze jest ubity i "+
            "szary. W niekt^orych miejscach pojawi^l si^e l^od "+
            "wy^slizgany tysi^acami ko^nskich kopyt, podeszwami but^ow "+
            "i ko^lami woz^ow. Nier^owne koleiny, ci^agn^ace si^e po "+
            "obu stronach traktu ^swiadcz^a o tym, ^ze handel jest w "+
            "tych okolicach do^s^c ^zywy. Trakt sunie pomi^edzy "+
            "drzewami, ubity, cho^c nie brukowany, ale wygodny i "+
            "szeroki. Drzewa pochylaj^a si^e nad drog^a, swymi d^lugimi "+
            "ga^l^eziami muskaj^ac co wy^zsze pud^la przeje^zdzaj^acych "+
            "tutaj powoz^ow. Na po^ludniu zachodzie wida^c g^este krzaki, "+
            "a gdzie^s dalej wzbiera niewyra^xny szum rzeki, ^l^acz^ac i "+
            "splataj^ac si^e z gwarem le^snego traktu.";
    }
    }
    str += "\n";
    return str;
}
opis_nocy()
{
    string str;
    if(pora_roku() != MT_ZIMA)
    {
        str = "Trakt wije si^e niczym szary w^a^z po^sr^od zielonego "+
            "lasu. Ziemia jest ubita niemal^ze na kamie^n tysi^acami "+
            "ko^nskich kopyt, bosych i obutych st^op. Nier^owne "+
            "koleiny, ci^agn^ace si^e po obu stronach traktu ^swiadcz^a "+
            "o tym, ^ze handel jest w tych okolicach do^s^c ^zywy. "+
            "Trakt sunie pomi^edzy drzewami, ubity, cho^c nie "+
            "brukowany, ale wygodny i szeroki. Drzewa pochylaj^a si^e "+
            "nad drog^a, swymi d^lugimi ga^l^eziami muskaj^ac co "+
            "wy^zsze pud^la powoz^ow. Na po^ludniu wida^c g^este krzaki, "+
            "a gdzie^s dalej wzbiera niewyra^xny szum rzeki, ^l^acz^ac i "+
            "splataj^ac si^e z gwarem le^snego traktu.";
    }
    else
    {
        str = "Trakt wije si^e niczym szary w^a^z po^sr^od obsypanego "+
            "^sniegiem lasu. ^Snieg le^z^acy na drodze jest ubity i "+
            "szary. W niekt^orych miejscach pojawi^l si^e l^od "+
            "wy^slizgany tysi^acami ko^nskich kopyt, podeszwami but^ow "+
            "i ko^lami woz^ow. Nier^owne koleiny, ci^agn^ace si^e po "+
            "obu stronach traktu ^swiadcz^a o tym, ^ze handel jest w "+
            "tych okolicach do^s^c ^zywy. Trakt sunie pomi^edzy "+
            "drzewami, ubity, cho^c nie brukowany, ale wygodny i "+
            "szeroki. Drzewa pochylaj^a si^e nad drog^a, swymi d^lugimi "+
            "ga^l^eziami muskaj^ac co wy^zsze pud^la przeje^zdzaj^acych "+
            "tutaj powoz^ow. Na po^ludniu zachodzie wida^c g^este krzaki, "+
            "a gdzie^s dalej wzbiera niewyra^xny szum rzeki, ^l^acz^ac i "+
            "splataj^ac si^e z gwarem le^snego traktu.";
    }
    str += "\n";
    return str;
}