/*
 * Przybrudzone lniane spodnie, takie dla biednych npcow np.
 *
 * Autor: Lil, Opis: Dinrahia (tudzie�: Aneta)
 */

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <macros.h>
#include <materialy.h>
#include <object_types.h>

void
create_armour()
{
     ustaw_nazwe("spodnie");
     ustaw_nazwe_glowna("para");
     dodaj_przym("przybrudzony","przybrudzeni");
     dodaj_przym("lniany","lniani");
     set_long("Wykonane z ciemnego lnu spodnie sprawiaj� nadzwyczaj "+
              "odpychaj�ce wra�enie. Nieproporcjonalnie d�ugie nogawki "+
              "trzeba kilkakrotnie podwija�, �eby w spodniach da�o si� "+
              "w og�le chodzi�. U g�ry zwisa sm�tnie na wp� oderwana "+
              "kiesze� - tylko patrze�, jak odpadnie zupe�nie.\n");
     set_slots(A_THIGHS | A_HIPS | A_STOMACH);
     add_prop(OBJ_I_WEIGHT, 100);
     add_prop(OBJ_I_VOLUME, 1700);
     add_prop(OBJ_I_VALUE, 7);
     ustaw_material(MATERIALY_LEN);
     set_type(O_UBRANIA);
     set_size("M");
     add_prop(ARMOUR_I_D_ROZCIAGLIWOSC, 40); //takie biedne spodnie niech beda...
     add_prop(ARMOUR_I_U_ROZCIAGLIWOSC, 50);

     set_likely_cond(23);
}
string
query_auto_load()
{
   return ::query_auto_load();
}

