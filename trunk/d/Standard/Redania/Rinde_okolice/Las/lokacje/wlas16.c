
/* Autor: Avard
   Data : 26.09.06
   Opis : vortak */
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <pogoda.h>
#include "dir.h"

inherit WLAS_RINDE_STD;

void create_las() 
{
    set_short("Na wsch^od od tartaku");
    add_exit(LAS_RINDE_LOKACJE + "wlas14.c","nw",0,WLAS_RO_FATIG,1);
    add_exit(LAS_RINDE_LOKACJE + "wlas13.c","n",0,WLAS_RO_FATIG,1);
    add_exit(LAS_RINDE_LOKACJE + "wlas12.c","ne",0,WLAS_RO_FATIG,1);
    add_exit(LAS_RINDE_LOKACJE + "tartak.c","w",0,WLAS_RO_FATIG,1);
    add_exit(LAS_RINDE_LOKACJE + "wlas17.c","sw",0,WLAS_RO_FATIG,1);
    add_exit(TRAKT_RINDE_LOKACJE + "trakt15.c","e",0,WLAS_RO_FATIG,1);
    add_exit(TRAKT_RINDE_LOKACJE + "trakt16.c","s",0,WLAS_RO_FATIG,1);

    add_prop(ROOM_I_INSIDE,0);
}
public string
exits_description() 
{
    return "Wykarczowany las wida^c na po^ludniowym-zachodzie, "+
        "p^o^lnocnym-zachodzie, p^o^lnocy i p^o^lnocnym-wschodzie. "+
        "Trakt znajduje si^e na wschodzie i po^ludniu, za^s tartak na "+
        "zachodzie.\n";
}

string
dlugi_opis()
{
    string str;
    if(jest_dzien() == 1)
    {
        if(pora_roku() != MT_ZIMA)
        {
            str = "Raptem kilka krok^ow na zach^od wyrasta niewielki "+
            "p^lotek, za kt^orym kryj^a si^e drewniane budynki tartaku. ";
        /*if drwale i woz
        {
            G^lo^sne krzyki pracj^acych tam drwali mieszaj^a si^e z gwarem 
            rozm^ow i stukotem kupieckich woz^ow docieraj^acym z 
            przebiegaj^acego w pobli^zu traktu.
        }*/
            str += "Na p^o^lnoc, jak okiem si^egn^a^c, ci^agn^a si^e "+
            "po^lacie "+
            "wykarczowanego lasu. Tak^ze w miejscu, w kt^orym stoisz ^zaden "+
            "pie^n nie wywin^a^l si^e spod ostrza drwalskiego topora. "+
            "Rozrzucone bez^ladnie masywne pniaki swoj^a obecno^sci^a "+
            "przypominaj^a, ^ze od wiek^ow r^os^l tu g^esty las, kt^ory "+
            "teraz musi si^e cofn^a^c zapewniaj^ac prac^e okolicznym "+
            "mieszka^ncom. Jedynie rachityczne krzaki porastaj^ace skraj "+
            "traktu mog^a da^c nieco cienia znu^zonym w^edrowcom. W oddali, "+
            "na p^o^lnocnym wschodzie majacz^a ledwo widoczne mury miejskie.";
        }
        else
        {
            str = "Raptem kilka krok^ow na zach^od wyrasta niewielki "+
            "p^lotek, "+
            "za kt^orym kryj^a si^e drewniane budynki tartaku. ";
        /*if drwale i woz
        {
            G^lo^sne krzyki pracj^acych tam drwali mieszaj^a si^e z gwarem 
            rozm^ow i stukotem kupieckich woz^ow docieraj^acym z 
            przebiegaj^acego w pobli^zu traktu.
         }*/
             str += "Na p^o^lnoc, jak okiem si^egn^a^c, ci^agn^a si^e "+
             "po^lacie "+
             "wykarczowanego lasu. Tak^ze w miejscu, w kt^orym stoisz "+
             "^zaden pie^n nie wywin^a^l si^e spod ostrza drwalskiego "+
             "topora. Rozrzucone bez^ladnie masywne pniaki swoj^a "+
             "obecno^sci^a przypominaj^a, ^ze od wiek^ow r^os^l tu g^esty "+
             "las, kt^ory teraz musi si^e cofn^a^c zapewniaj^ac prac^e "+
             "okolicznym mieszka^ncom. Jedynie rachityczne krzaki "+
             "porastaj^ace skraj traktu mog^a da^c nieco cienia znu^zonym "+
             "w^edrowcom. W oddali, na p^o^lnocnym wschodzie majacz^a ledwo "+
             "widoczne mury miejskie.";
        }
    }
    if(jest_dzien() == 0)
    {
        if(pora_roku() != MT_ZIMA)
        {
            str = "Raptem kilka krok^ow na zach^od wyrasta niewielki "+
            "p^lotek, "+
            "za kt^orym kryj^a si^e drewniane budynki tartaku. "+
            "Na p^o^lnoc, jak okiem si^egn^a^c, ci^agn^a si^e po^lacie "+
            "wykarczowanego lasu. Tak^ze w miejscu, w kt^orym stoisz ^zaden "+
            "pie^n nie wywin^a^l si^e spod ostrza drwalskiego topora. "+
            "Rozrzucone bez^ladnie masywne pniaki swoj^a obecno^sci^a "+
            "przypominaj^a, ^ze od wiek^ow r^os^l tu g^esty las, kt^ory "+
            "teraz musi si^e cofn^a^c zapewniaj^ac prac^e okolicznym "+
            "mieszka^ncom. Jedynie rachityczne krzaki porastaj^ace skraj "+
            "traktu mog^a da^c nieco cienia znu^zonym w^edrowcom. W oddali, "+
            "na p^o^lnocnym wschodzie majacz^a ledwo widoczne mury miejskie.";
        }
        else
        {
            str = "Raptem kilka krok^ow na zach^od wyrasta niewielki "+
            "p^lotek, "+
            "za kt^orym kryj^a si^e drewniane budynki tartaku. "+
            "Na p^o^lnoc, jak okiem si^egn^a^c, ci^agn^a si^e po^lacie "+
            "wykarczowanego lasu. Tak^ze w miejscu, w kt^orym stoisz "+
            "^zaden pie^n nie wywin^a^l si^e spod ostrza drwalskiego "+
            "topora. Rozrzucone bez^ladnie masywne pniaki swoj^a "+
            "obecno^sci^a przypominaj^a, ^ze od wiek^ow r^os^l tu g^esty "+
            "las, kt^ory teraz musi si^e cofn^a^c zapewniaj^ac prac^e "+
            "okolicznym mieszka^ncom. Jedynie rachityczne krzaki "+
            "porastaj^ace skraj traktu mog^a da^c nieco cienia znu^zonym "+
            "w^edrowcom. W oddali, na p^o^lnocnym wschodzie majacz^a ledwo "+
            "widoczne mury miejskie.";
        }
    }
    str += "\n";
    return str;
}
string
opis_nocy()
{
    string str;
    if(pora_roku() != MT_ZIMA)
    {
        str = "Raptem kilka krok^ow na zach^od wyrasta niewielki p^lotek, "+
            "za kt^orym kryj^a si^e drewniane budynki tartaku. "+
            "Na p^o^lnoc, jak okiem si^egn^a^c, ci^agn^a si^e po^lacie "+
            "wykarczowanego lasu. Tak^ze w miejscu, w kt^orym stoisz ^zaden "+
            "pie^n nie wywin^a^l si^e spod ostrza drwalskiego topora. "+
            "Rozrzucone bez^ladnie masywne pniaki swoj^a obecno^sci^a "+
            "przypominaj^a, ^ze od wiek^ow r^os^l tu g^esty las, kt^ory "+
            "teraz musi si^e cofn^a^c zapewniaj^ac prac^e okolicznym "+
            "mieszka^ncom. Jedynie rachityczne krzaki porastaj^ace skraj "+
            "traktu mog^a da^c nieco cienia znu^zonym w^edrowcom. W oddali, "+
            "na p^o^lnocnym wschodzie majacz^a ledwo widoczne mury miejskie.";
    }
    else
    {
        str = "Raptem kilka krok^ow na zach^od wyrasta niewielki p^lotek, "+
            "za kt^orym kryj^a si^e drewniane budynki tartaku. "+
            "Na p^o^lnoc, jak okiem si^egn^a^c, ci^agn^a si^e po^lacie "+
            "wykarczowanego lasu. Tak^ze w miejscu, w kt^orym stoisz "+
            "^zaden pie^n nie wywin^a^l si^e spod ostrza drwalskiego "+
            "topora. Rozrzucone bez^ladnie masywne pniaki swoj^a "+
            "obecno^sci^a przypominaj^a, ^ze od wiek^ow r^os^l tu g^esty "+
            "las, kt^ory teraz musi si^e cofn^a^c zapewniaj^ac prac^e "+
            "okolicznym mieszka^ncom. Jedynie rachityczne krzaki "+
            "porastaj^ace skraj traktu mog^a da^c nieco cienia znu^zonym "+
            "w^edrowcom. W oddali, na p^o^lnocnym wschodzie majacz^a ledwo "+
            "widoczne mury miejskie.";
    }
    str += "\n";
    return str;
}
