/* Autor: Avard
   Data : 27.11.06
   Opis : Tinardan */

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <pogoda.h>
#include "dir.h"

inherit LAS_RINDE_STD;

void create_las() 
{
    set_short("G^aszcz");
    add_exit(LAS_RINDE_LOKACJE + "las10.c","ne",0,LAS_RO_FATIG,1);
    add_exit(LAS_RINDE_LOKACJE + "las18.c","w",0,LAS_RO_FATIG,1);
    add_exit(LAS_RINDE_LOKACJE + "las28.c","s",0,LAS_RO_FATIG,1);

    add_prop(ROOM_I_INSIDE,0);
}

string
dlugi_opis()
{
    string str;
    if(jest_dzien() == 1)
    {
        if(pora_roku() != MT_ZIMA)
        {
            str = "Najprawdopodobniej t^edy przechodz^a tylko najbardziej "+
                "wytrwa^le zwierz^eta. Tu^z przy ziemi, poskr^ecanymi "+
                "^lody^zkami, pn^a si^e w g^or^e ku ^swiat^lu m^lode p^edy, "+
                "zawzi^ecie i niestrudzenie. Tu^z ponad nimi wyrastaj^a "+
                "wybuja^le krzewy g^logu, spl^atane i niemal nie do "+
                "przebycia. Niemal, bo jednak, gdy si^e przyjrze^c nieco "+
                "uwa^zniej, mo^zna dostrzec w^sr^od nich niewielkie "+
                "^scie^zki w^askie i kr^ete, ale na tyle wydeptane, by "+
                "m^og^l si^e nimi przecisn^a^c kto^s dosy^c postawny. Nad "+
                "t^a ca^l^a pl^atanin^a i chaosem kr^oluj^a, dostojnie i z "+
                "gracj^a, buki o g^ladkiej korze, rosn^ace z rzadka, ale "+
                "silnie wpijaj^ac si^e korzeniami w ziemi^e i bez "+
                "w^atpienia b^ed^ac zwyci^ezcami w tej walce o woln^a "+
                "przestrze^n.";
        }
        else
        {
            str = "Najprawdopodobniej t^edy przechodz^a tylko najbardziej "+
                "wytrwa^le zwierz^eta. Tu^z przy ziemi, poskr^ecanymi "+
                "^lody^zkami, pn^a si^e w g^or^e ku ^swiat^lu m^lode p^edy, "+
                "zawzi^ecie i niestrudzenie, teraz s^a ogo^locone z li^sci "+
                "i okryte cienk^a warstw^a ^sniegu, czekaj^a cicho by od "+
                "wiosny rozpocz^a^c walk^e o przetrwanie. Tu^z ponad nimi "+
                "wyrastaj^a wybuja^le krzewy g^logu, spl^atane i niemal nie "+
                "do przebycia. Niemal, bo jednak, gdy si^e przyjrze^c nieco "+
                "uwa^zniej, mo^zna dostrzec w^sr^od nich niewielkie "+
                "^scie^zki w^askie i kr^ete, ale na tyle wydeptane, by "+
                "m^og^l si^e nimi przecisn^a^c kto^s dosy^c postawny. Nad "+
                "t^a ca^l^a pl^atanin^a i chaosem kr^oluj^a, dostojnie i z "+
                "gracj^a, buki o g^ladkiej korze, rosn^ace z rzadka, ale "+
                "silnie wpijaj^ac si^e korzeniami w ziemi^e i bez "+
                "w^atpienia b^ed^ac zwyci^ezcami w tej walce o woln^a "+
                "przestrze^n.";
        }
    }
    if(jest_dzien() == 0)
    {
        if(pora_roku() != MT_ZIMA)
        {
            str = "Najprawdopodobniej t^edy przechodz^a tylko najbardziej "+
                "wytrwa^le zwierz^eta. Tu^z przy ziemi, poskr^ecanymi "+
                "^lody^zkami, pn^a si^e w g^or^e m^lode p^edy, "+
                "zawzi^ecie i niestrudzenie. Tu^z ponad nimi wyrastaj^a "+
                "wybuja^le krzewy g^logu, spl^atane i niemal nie do "+
                "przebycia. Niemal, bo jednak, gdy si^e przyjrze^c nieco "+
                "uwa^zniej, mo^zna dostrzec w^sr^od nich niewielkie "+
                "^scie^zki w^askie i kr^ete, ale na tyle wydeptane, by "+
                "m^og^l si^e nimi przecisn^a^c kto^s dosy^c postawny. Nad "+
                "t^a ca^l^a pl^atanin^a i chaosem kr^oluj^a, dostojnie i z "+
                "gracj^a, buki o g^ladkiej korze, rosn^ace z rzadka, ale "+
                "silnie wpijaj^ac si^e korzeniami w ziemi^e i bez "+
                "w^atpienia b^ed^ac zwyci^ezcami w tej walce o woln^a "+
                "przestrze^n.";
        }
        else
        {
            str = "Najprawdopodobniej t^edy przechodz^a tylko najbardziej "+
                "wytrwa^le zwierz^eta. Tu^z przy ziemi, poskr^ecanymi "+
                "^lody^zkami, pn^a si^e w g^or^e m^lode p^edy, "+
                "zawzi^ecie i niestrudzenie, teraz s^a ogo^locone z li^sci "+
                "i okryte cienk^a warstw^a ^sniegu, czekaj^a cicho by od "+
                "wiosny rozpocz^a^c walk^e o przetrwanie. Tu^z ponad nimi "+
                "wyrastaj^a wybuja^le krzewy g^logu, spl^atane i niemal nie "+
                "do przebycia. Niemal, bo jednak, gdy si^e przyjrze^c nieco "+
                "uwa^zniej, mo^zna dostrzec w^sr^od nich niewielkie "+
                "^scie^zki w^askie i kr^ete, ale na tyle wydeptane, by "+
                "m^og^l si^e nimi przecisn^a^c kto^s dosy^c postawny. Nad "+
                "t^a ca^l^a pl^atanin^a i chaosem kr^oluj^a, dostojnie i z "+
                "gracj^a, buki o g^ladkiej korze, rosn^ace z rzadka, ale "+
                "silnie wpijaj^ac si^e korzeniami w ziemi^e i bez "+
                "w^atpienia b^ed^ac zwyci^ezcami w tej walce o woln^a "+
                "przestrze^n.";
        }
    }
    str +="\n";
    return str;
}
string
opis_nocy()
{
    string str;
    if(pora_roku() != MT_ZIMA)
        {
            str = "Najprawdopodobniej t^edy przechodz^a tylko najbardziej "+
                "wytrwa^le zwierz^eta. Tu^z przy ziemi, poskr^ecanymi "+
                "^lody^zkami, pn^a si^e w g^or^e m^lode p^edy, "+
                "zawzi^ecie i niestrudzenie. Tu^z ponad nimi wyrastaj^a "+
                "wybuja^le krzewy g^logu, spl^atane i niemal nie do "+
                "przebycia. Niemal, bo jednak, gdy si^e przyjrze^c nieco "+
                "uwa^zniej, mo^zna dostrzec w^sr^od nich niewielkie "+
                "^scie^zki w^askie i kr^ete, ale na tyle wydeptane, by "+
                "m^og^l si^e nimi przecisn^a^c kto^s dosy^c postawny. Nad "+
                "t^a ca^l^a pl^atanin^a i chaosem kr^oluj^a, dostojnie i z "+
                "gracj^a, buki o g^ladkiej korze, rosn^ace z rzadka, ale "+
                "silnie wpijaj^ac si^e korzeniami w ziemi^e i bez "+
                "w^atpienia b^ed^ac zwyci^ezcami w tej walce o woln^a "+
                "przestrze^n.";
        }
        else
        {
            str = "Najprawdopodobniej t^edy przechodz^a tylko najbardziej "+
                "wytrwa^le zwierz^eta. Tu^z przy ziemi, poskr^ecanymi "+
                "^lody^zkami, pn^a si^e w g^or^e m^lode p^edy, "+
                "zawzi^ecie i niestrudzenie, teraz s^a ogo^locone z li^sci "+
                "i okryte cienk^a warstw^a ^sniegu, czekaj^a cicho by od "+
                "wiosny rozpocz^a^c walk^e o przetrwanie. Tu^z ponad nimi "+
                "wyrastaj^a wybuja^le krzewy g^logu, spl^atane i niemal nie "+
                "do przebycia. Niemal, bo jednak, gdy si^e przyjrze^c nieco "+
                "uwa^zniej, mo^zna dostrzec w^sr^od nich niewielkie "+
                "^scie^zki w^askie i kr^ete, ale na tyle wydeptane, by "+
                "m^og^l si^e nimi przecisn^a^c kto^s dosy^c postawny. Nad "+
                "t^a ca^l^a pl^atanin^a i chaosem kr^oluj^a, dostojnie i z "+
                "gracj^a, buki o g^ladkiej korze, rosn^ace z rzadka, ale "+
                "silnie wpijaj^ac si^e korzeniami w ziemi^e i bez "+
                "w^atpienia b^ed^ac zwyci^ezcami w tej walce o woln^a "+
                "przestrze^n.";
        }
    str +="\n";
    return str;
}

