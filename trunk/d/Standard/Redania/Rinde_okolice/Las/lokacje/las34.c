/* Autor: Avard
   Data : 21.06.06
   Opis : Tinardan
   Wykonano ku chwale Swiatowida! */
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <pogoda.h>
#include "dir.h"

inherit LAS_RINDE_STD;

void create_las() 
{
    set_short("^Scie^zka do tartaku");
    add_exit(LAS_RINDE_LOKACJE + "las23.c","ne",0,WLAS_RO_FATIG,1);
    add_exit(LAS_RINDE_LOKACJE + "las35.c","w",0,WLAS_RO_FATIG,1);

    add_prop(ROOM_I_INSIDE,0);

    add_item(({"^smieci","^smiecie"}),"@@smiecie@@");

    add_object("/d/Standard/items/ubrania/onuce/stare_szarawe_Lc",3);
    add_object(LAS_RINDE_OBIEKTY+"but_z_odlepiona_podeszwa",3);
    add_object(LAS_RINDE_OBIEKTY+"but_bez_cholewki",3);
    //add_object(LAS_RINDE_OBIEKTY+"kapusta.c");
    //add_object(LAS_RINDE_OBIEKTY+"kapusta.c");
    //add_object(LAS_RINDE_OBIEKTY+"kapusta.c");
    dodaj_rzecz_niewyswietlana("dziurawy buk^lak", 3);
    dodaj_rzecz_niewyswietlana("but z odlepion^a podeszw^a", 3);
    dodaj_rzecz_niewyswietlana("but bez cholewki", 3);
    dodaj_rzecz_niewyswietlana("para starych szarawych onuc", 3);
    //dodaj_rzecz_niewyswietlana("gnij^aca kapusta", 3);
    add_npc(LAS_RINDE_LIVINGI+"borsuk");

}
public string
exits_description() 
{
    return "^Scie^zka prowadzi na zach^od i p^o^lnocny-wsch^od.\n";
}

string
dlugi_opis()
{
    string str;
    if(jest_dzien() == 1)
    {
    if(pora_roku() != MT_ZIMA)
    {
    str = "^Scie^zka biegnie prosto, z obu stron ograniczana przez las. "+
        "Drzewa rosn^a wysoko, cho^c s^a do^s^c mocno przerzedzone  "+
        "wida^c, ^ze podr^o^zni nie stroni^a od obozowania w tych "+
        "okolicach";
    if(jest_rzecz_w_sublokacji(0, "dziurawy buklak") || 
        jest_rzecz_w_sublokacji (0, "but z odlepion^a podeszw^a") || 
        jest_rzecz_w_sublokacji(0, "but bez cholewki") || 
        jest_rzecz_w_sublokacji(0, "para starych szarawych onuc") ||
        jest_rzecz_w_sublokacji(0, "zgni^la kapusta"))
          {
        str +=", a i nie bywaj^a tak^ze zbyt porz^adni.";
          }
          else
          {
              str +=".";
          }
        str +=" Droga jest "+
        "do^s^c w^aska, wi^ekszy w^oz mia^lby spory problem, aby t^edy "+
        "przejecha^c, cho^c l^zejsze powoziki zmie^sci^lyby si^e, pod "+
        "warunkiem, ^ze nie musia^lyby si^e mija^c. Z po^ludnia wiatr "+
        "przynosi gwar i ha^las - zapewne biegnie tamt^edy jaki^s wi^ekszy "+
        "trakt. Cho^c ta ^scie^zka jest pe^lna spokoju i do^s^c ma^lo "+
        "ucz^esczana, za drzewami wida^c czyje^s sylwetki i wielkie, "+
        "ciemniej^ace kopu^ly powoz^ow i wi^ekszych fur. Ale tutaj gdzie^s "+
        "wysoko, ukryty w^sr^od ga^l^ezi pod^spiewuje weso^lo drozd.";
    }
    else
    {
    str = "^Scie^zka biegnie prosto, z obu stron ograniczana przez las. "+
        "Drzewa rosn^a wysoko, cho^c s^a do^s^c mocno przerzedzone  "+
        "wida^c, ^ze podr^o^zni nie stroni^a od obozowania w tych "+
        "okolicach";
    if(jest_rzecz_w_sublokacji(0, "dziurawy buklak") || 
        jest_rzecz_w_sublokacji (0, "but z odlepion^a podeszw^a") || 
        jest_rzecz_w_sublokacji(0, "but bez cholewki") || 
        jest_rzecz_w_sublokacji(0, "para starych szarawych onuc") ||
        jest_rzecz_w_sublokacji(0, "zgni^la kapusta"))
          {
        str +=", a i nie bywaj^a tak^ze zbyt porz^adni.";
          }
          else
          {
              str +=".";
          }
        str +=" Droga jest "+
        "do^s^c w^aska, wi^ekszy w^oz mia^lby spory problem, aby t^edy "+
        "przejecha^c, cho^c l^zejsze powoziki zmie^sci^lyby si^e, pod "+
        "warunkiem, ^ze nie musia^lyby si^e mija^c. Z po^ludnia wiatr "+
        "przynosi gwar i ha^las - zapewne biegnie tamt^edy jaki^s wi^ekszy "+
        "trakt. Cho^c ta ^scie^zka jest pe^lna spokoju i do^s^c ma^lo "+
        "ucz^esczana, za drzewami wida^c czyje^s sylwetki i wielkie, "+
        "ciemniej^ace kopu^ly powoz^ow i wi^ekszych fur. Ale tutaj gdzie^s "+
        "wysoko, ukryta w^sr^od ga^l^ezi kracze ponuro wrona.";
      }
      }
//Tu sie noc zaczyna. ;)
      if(jest_dzien() == 0)
      {
      if(pora_roku() != MT_ZIMA)
    {
    str = "^Scie^zka biegnie prosto, z obu stron ograniczana przez las. "+
        "Drzewa rosn^a wysoko, cho^c s^a do^s^c mocno przerzedzone  "+
        "wida^c, ^ze podr^o^zni nie stroni^a od obozowania w tych "+
        "okolicach";
    if(jest_rzecz_w_sublokacji(0, "dziurawy buklak") || 
        jest_rzecz_w_sublokacji (0, "but z odlepion^a podeszw^a") || 
        jest_rzecz_w_sublokacji(0, "but bez cholewki") || 
        jest_rzecz_w_sublokacji(0, "para starych szarawych onuc") ||
        jest_rzecz_w_sublokacji(0, "zgni^la kapusta"))
          {
        str +=", a i nie bywaj^a tak^ze zbyt porz^adni.";
          }
          else
          {
              str +=".";
          }
        str +=" Droga jest "+
        "do^s^c w^aska, wi^ekszy w^oz mia^lby spory problem, aby t^edy "+
        "przejecha^c, cho^c l^zejsze powoziki zmie^sci^lyby si^e, pod "+
        "warunkiem, ^ze nie musia^lyby si^e mija^c. Gdzie^s wysoko, "+
        "ukryta w^sr^od ga^l^ezi kracze ponuro wrona.";
    }
    else
    {
    str = "^Scie^zka biegnie prosto, z obu stron ograniczana przez las. "+
        "Drzewa rosn^a wysoko, cho^c s^a do^s^c mocno przerzedzone  "+
        "wida^c, ^ze podr^o^zni nie stroni^a od obozowania w tych "+
        "okolicach";
    if(jest_rzecz_w_sublokacji(0, "dziurawy buklak") || 
        jest_rzecz_w_sublokacji (0, "but z odlepion^a podeszw^a") || 
        jest_rzecz_w_sublokacji(0, "but bez cholewki") || 
        jest_rzecz_w_sublokacji(0, "para starych szarawych onuc") ||
        jest_rzecz_w_sublokacji(0, "zgni^la kapusta"))
          {
        str +=", a i nie bywaj^a tak^ze zbyt porz^adni.";
          }
          else
          {
              str +=".";
          }
        str +=" Droga jest "+
        "do^s^c w^aska, wi^ekszy w^oz mia^lby spory problem, aby t^edy "+
        "przejecha^c, cho^c l^zejsze powoziki zmie^sci^lyby si^e, pod "+
        "warunkiem, ^ze nie musia^lyby si^e mija^c. Gdzie^s wysoko, ukryty "+
        "w^sr^od ga^l^ezi kracze ponuro wrona.";
    }
    }
      str += "\n";
      return str;
}
string
opis_nocy()
{
    string str;
    if(pora_roku() != MT_ZIMA)
    {
    str = "^Scie^zka biegnie prosto, z obu stron ograniczana przez las. "+
        "Drzewa rosn^a wysoko, cho^c s^a do^s^c mocno przerzedzone  "+
        "wida^c, ^ze podr^o^zni nie stroni^a od obozowania w tych "+
        "okolicach";
    if(jest_rzecz_w_sublokacji(0, "dziurawy buklak") || 
        jest_rzecz_w_sublokacji (0, "but z odlepion^a podeszw^a") || 
        jest_rzecz_w_sublokacji(0, "but bez cholewki") || 
        jest_rzecz_w_sublokacji(0, "para starych szarawych onuc") ||
        jest_rzecz_w_sublokacji(0, "zgni^la kapusta"))
          {
        str +=", a i nie bywaj^a tak^ze zbyt porz^adni.";
          }
          else
          {
              str +=".";
          }
        str +=" Droga jest "+
        "do^s^c w^aska, wi^ekszy w^oz mia^lby spory problem, aby t^edy "+
        "przejecha^c, cho^c l^zejsze powoziki zmie^sci^lyby si^e, pod "+
        "warunkiem, ^ze nie musia^lyby si^e mija^c. Gdzie^s wysoko, "+
        "ukryta w^sr^od ga^l^ezi kracze ponuro wrona.";
    }
    else
    {
    str = "^Scie^zka biegnie prosto, z obu stron ograniczana przez las. "+
        "Drzewa rosn^a wysoko, cho^c s^a do^s^c mocno przerzedzone  "+
        "wida^c, ^ze podr^o^zni nie stroni^a od obozowania w tych "+
        "okolicach";
    if(jest_rzecz_w_sublokacji(0, "dziurawy buklak") || 
        jest_rzecz_w_sublokacji (0, "but z odlepion^a podeszw^a") || 
        jest_rzecz_w_sublokacji(0, "but bez cholewki") || 
        jest_rzecz_w_sublokacji(0, "para starych szarawych onuc") ||
        jest_rzecz_w_sublokacji(0, "zgni^la kapusta"))
          {
        str +=", a i nie bywaj^a tak^ze zbyt porz^adni.";
          }
          else
          {
              str +=".";
          }
        str +=" Droga jest "+
        "do^s^c w^aska, wi^ekszy w^oz mia^lby spory problem, aby t^edy "+
        "przejecha^c, cho^c l^zejsze powoziki zmie^sci^lyby si^e, pod "+
        "warunkiem, ^ze nie musia^lyby si^e mija^c. Gdzie^s wysoko, ukryty "+
        "w^sr^od ga^l^ezi kracze ponuro wrona.";
    }
    str += "\n";
    return str;
}
string
pora_rok()
{
    switch (pora_roku())
    {
         case MT_LATO:
         case MT_WIOSNA:
             return ({"Wa^zka przelecia^la tu^z ko^lo twojego nosa.\n",
             "W wodzie trzepocze przez chwil^e jaka^s ryba, po czym niknie w g^l^ebinach.\n",
             "Fale Pontaru szumi^a cicho."})[random(3)];
         case MT_JESIEN:
             return ("Bezlistne ga^l^ezie muskaj^a tw^oj policzek.\n");
         case MT_ZIMA:
             return ({"Fale Pontaru hucz^a gro^xnie.\n",
             "Bezlistne ga^l^ezie muskaj^a tw^oj policzek.\n",
             "Kra z trzaskiem od^lamuje si^e od brzegu i odp^lywa niesiona silnym nurtem.\n"})[random(3)];
    }
}

string
smiecie()
{
    string str;
    if(jest_rzecz_w_sublokacji(0, "dziurawy buklak") || 
        jest_rzecz_w_sublokacji (0, "but z odlepion^a podeszw^a") || 
        jest_rzecz_w_sublokacji(0, "but bez cholewki") || 
        jest_rzecz_w_sublokacji(0, "para starych szarawych onuc") ||
        jest_rzecz_w_sublokacji(0, "zgni^la kapusta"))
           {
        str = "Walaj^a si^e tu r^o^znego rodzaju ^smiecie";
           }
           else
           {
               str = "Nie zauwa^zasz niczego takiego";
           }
     
    if(jest_rzecz_w_sublokacji(0, "dziurawy buklak"))
           {
        str += ", dziurawe buk^laki";
           }
    if(jest_rzecz_w_sublokacji(0, "but z odlepion^a podeszw^a") || 
    jest_rzecz_w_sublokacji(0, "but bez cholewki"))  
           {
        str += ", zu^zyte buty";
           }
    if(jest_rzecz_w_sublokacji(0, "zgni^la kapusta"))
           {
        str +=", zgni^le kapusty";
           }
    if(jest_rzecz_w_sublokacji(0, "para starych szarawych onuc"))
           {
        str += " i przetarte onuce nie pierwszej ^swie^zo^sci, a niekt^orzy "+
            "nie zawahaliby si^e u^zy^c stwierdzenia, ^ze tak^ze i nie "+
            "dziesi^atej";
           }
    str += ".\n";

    return str;
}