/* Autor: Avard
   Data : 29.11.06
   Opis : Tinardan

   Wed Apr 4 02:29:29 2007 doda�em jam�. Vera
   */
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <pogoda.h>
#include "dir.h"
inherit LAS_RINDE_STD;

void create_las()
{
    LOAD_ERR(LAS_RINDE_LOKACJE+"na_skalce");

    set_short("Pod ska^lk^a");
    add_exit(LAS_RINDE_LOKACJE + "las14.c","sw",0,LAS_RO_FATIG,1);
    add_exit(LAS_RINDE_LOKACJE + "las13.c","se",0,LAS_RO_FATIG,1);
    add_exit(LAS_RINDE_LOKACJE + "na_skalce.c", "na ska^lk^e", 1, 1, 1);
    add_npc(LAS_RINDE_LIVINGI+"wiewiorka");

    add_item(({"jam�","niewielk� jam�","ciemn� jam�",
        "niewielk� ciemn� jam�"}),
        "U podn�a ska�ki dostrzegasz ciemn� jam� prowadz�c� "+
        "pod skosem w d�, tu� pod wzg�rze.\n");

    add_prop(ROOM_I_INSIDE,0);
    add_object(LAS_RINDE_OBIEKTY+"skalka");

    dodaj_rzecz_niewyswietlana_plik(LAS_RINDE_OBIEKTY+"skalka", 1);
}

string
dlugi_opis()
{
    string str;
    if(jest_dzien() == 1)
    {
        if(pora_roku() != MT_ZIMA)
        {
            str = "Na ^srodku niewielkiego przerzedzenia, na niewielkim "+
                "wzg^orzu stoi szarawa ska^lka. Po^sr^od lasu wygl^ada "+
                "dziwacznie i zupe^lnie nie pasuje do otaczaj^acego j^a "+
                "krajobrazu. Drzewa rosn^a w pewnej odleg^lo^sci od niej, "+
                "jakby i one by^ly zawstydzone jej niespodziewan^a "+
                "obecno^sci^a tutaj. Tylko mech wspina si^e po jej "+
                "^scianach, ciemnozielony i zach^lanny. U podn^o^za k^l^ebi "+
                "si^e trawa i drobne krzewy, mi�dzy kt�rymi dostrzegasz "+
                "niewielk� ciemn� jam�. Ska^la powyginana jest w "+
                "niezwyk^le kszta^lty, ^luki i zag^l^ebienia, przez ca^le "+
                "lata dr^a^zona przez wod^e i owiewana ostrym podmuchem "+
                "wiatru.";
        }
        else
        {
            str = "Na ^srodku niewielkiego przerzedzenia, na niewielkim "+
                "wzg^orzu stoi szarawa ska^lka. Po^sr^od lasu wygl^ada "+
                "dziwacznie i zupe^lnie nie pasuje do otaczaj^acego j^a "+
                "krajobrazu. Drzewa rosn^a w pewnej odleg^lo^sci od niej, "+
                "jakby i one by^ly zawstydzone jej niespodziewan^a "+
                "obecno^sci^a tutaj. Tylko mech wspina si^e po jej "+
                "^scianach, ciemnozielony i zach^lanny. U podn^o^za zalega "+
                "gruba warstwa ^sniegu, odgarni�ta niedbale od "+
                "niewielkiej ciemnej jamy. Ska^la powyginana jest w niezwyk^le "+
                "kszta^lty, ^luki i zag^l^ebienia, przez ca^le lata "+
                "dr^a^zona przez wod^e i owiewana ostrym podmuchem wiatru.";
       }
    }
    if(jest_dzien() == 0)
    {
        if(pora_roku() != MT_ZIMA)
        {
            str = "Na ^srodku niewielkiego przerzedzenia, na niewielkim "+
                "wzg^orzu stoi szarawa ska^lka. Po^sr^od lasu wygl^ada "+
                "dziwacznie i zupe^lnie nie pasuje do otaczaj^acego j^a "+
                "krajobrazu. Drzewa rosn^a w pewnej odleg^lo^sci od niej, "+
                "jakby i one by^ly zawstydzone jej niespodziewan^a "+
                "obecno^sci^a tutaj. Tylko mech wspina si^e po jej "+
                "^scianach, ciemnozielony i zach^lanny. U podn^o^za k^l^ebi "+
                "si^e trawa i drobne krzewy, mi�dzy kt�rymi dostrzegasz "+
                "niewielk� ciemn� jam�. Ska^la powyginana jest w "+
                "niezwyk^le kszta^lty, ^luki i zag^l^ebienia, przez ca^le "+
                "lata dr^a^zona przez wod^e i owiewana ostrym podmuchem "+
                "wiatru.";
        }
        else
        {
            str = "Na ^srodku niewielkiego przerzedzenia, na niewielkim "+
                "wzg^orzu stoi szarawa ska^lka. Po^sr^od lasu wygl^ada "+
                "dziwacznie i zupe^lnie nie pasuje do otaczaj^acego j^a "+
                "krajobrazu. Drzewa rosn^a w pewnej odleg^lo^sci od niej, "+
                "jakby i one by^ly zawstydzone jej niespodziewan^a "+
                "obecno^sci^a tutaj. Tylko mech wspina si^e po jej "+
                "^scianach, ciemnozielony i zach^lanny. U podn^o^za zalega "+
                "gruba warstwa ^sniegu, odgarni�ta niedbale od "+
                "niewielkiej ciemnej jamy. Ska^la powyginana jest w niezwyk^le "+
                "kszta^lty, ^luki i zag^l^ebienia, przez ca^le lata "+
                "dr^a^zona przez wod^e i owiewana ostrym podmuchem wiatru.";
       }
    }
    str += "\n";
    return str;
}
string
opis_nocy()
{
    string str;
    if(pora_roku() != MT_ZIMA)
        {
            str = "Na ^srodku niewielkiego przerzedzenia, na niewielkim "+
                "wzg^orzu stoi szarawa ska^lka. Po^sr^od lasu wygl^ada "+
                "dziwacznie i zupe^lnie nie pasuje do otaczaj^acego j^a "+
                "krajobrazu. Drzewa rosn^a w pewnej odleg^lo^sci od niej, "+
                "jakby i one by^ly zawstydzone jej niespodziewan^a "+
                "obecno^sci^a tutaj. Tylko mech wspina si^e po jej "+
                "^scianach, ciemnozielony i zach^lanny. U podn^o^za k^l^ebi "+
                "si^e trawa i drobne krzewy, mi�dzy kt�rymi dostrzegasz "+
                "niewielk� ciemn� jam�. Ska^la powyginana jest w "+
                "niezwyk^le kszta^lty, ^luki i zag^l^ebienia, przez ca^le "+
                "lata dr^a^zona przez wod^e i owiewana ostrym podmuchem "+
                "wiatru.";
        }
        else
        {
            str = "Na ^srodku niewielkiego przerzedzenia, na niewielkim "+
                "wzg^orzu stoi szarawa ska^lka. Po^sr^od lasu wygl^ada "+
                "dziwacznie i zupe^lnie nie pasuje do otaczaj^acego j^a "+
                "krajobrazu. Drzewa rosn^a w pewnej odleg^lo^sci od niej, "+
                "jakby i one by^ly zawstydzone jej niespodziewan^a "+
                "obecno^sci^a tutaj. Tylko mech wspina si^e po jej "+
                "^scianach, ciemnozielony i zach^lanny. U podn^o^za zalega "+
                "gruba warstwa ^sniegu, odgarni�ta niedbale od "+
                "niewielkiej ciemnej jamy. Ska^la powyginana jest w niezwyk^le "+
                "kszta^lty, ^luki i zag^l^ebienia, przez ca^le lata "+
                "dr^a^zona przez wod^e i owiewana ostrym podmuchem wiatru.";
       }
    str += "\n";
    return str;
}

void
przenies_do_jamy(object player)
{
	write("Uff, na szcz�cie trwa�o to bardzo "+
		"kr�tko.\n");
		saybb(QCIMIE(TP,PL_MIA)+" wchodzi do ciemnej jamy.\n");
	player->move(LAS_RINDE_LOKACJE+"jama");
	player->do_glance(2);

	return;
}

int
wejdz(string str)
{
	notify_fail("Gdzie chcesz wej��?\n");

	if(str~="do jamy" || str~="do niewielkiej jamy" || str~="do ciemnej jamy"
		|| str~="do niewielkiej ciemnej jamy")
	{
		object paraliz=clone_object("/std/paralyze");
		paraliz->set_fail_message("");
    	paraliz->set_finish_object(this_object());
    	paraliz->set_finish_fun("przenies_do_jamy");
    	paraliz->set_remove_time(2);
    	paraliz->setuid();
    	paraliz->seteuid(getuid());
    	paraliz->move(TP);

		write("Ostro�nie wchodzisz do niewielkiej zaciemnionej jamy i...\n"+
		"Nagle zaczynasz zje�d�a� w d�!\n");

		return 1;
	}

	return 0;
}

void
init()
{
    ::init();
    add_action(wejdz,"wejd�");
}
