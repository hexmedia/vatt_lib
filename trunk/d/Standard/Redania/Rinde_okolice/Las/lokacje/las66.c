/* Autor: Avard
   Data : 19.09.06
   Opis : Tinardan */
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <pogoda.h>
#include "dir.h"

inherit LAS_RINDE_STD;
inherit "/lib/drink_water";
void create_las() 
{
    set_short("^L^aczka nad brzegiem rzeki");
    add_exit(LAS_RINDE_LOKACJE + "las72.c","se",0,LAS_RO_FATIG,1);
    add_exit(LAS_RINDE_LOKACJE + "las58.c","nw",0,LAS_RO_FATIG,1);
    add_exit(TRAKT_RINDE_LOKACJE + "trakt24.c","n",0,TRAKT_RO_FATIG,0);
    add_exit(TRAKT_RINDE_LOKACJE + "trakt23.c","ne",0,TRAKT_RO_FATIG,0);
    add_exit(TRAKT_RINDE_LOKACJE + "trakt22.c","e",0,TRAKT_RO_FATIG,0);
    add_npc(LAS_RINDE_LIVINGI + "sarna");
    
    set_drink_places(({"z rzeki","z pontaru","z Pontaru"}));
    add_prop(ROOM_I_INSIDE,0);
    add_event("@@pora_rok:"+file_name(TO)+"@@");

    add_item(({"Pontar","pontar","rzek^e"}),"P^lyn^acy tu od tysi^ecy lat "+
    "szeroki Pontar zdaje si^e by^c wci^a^z nieujarzmionym i niezdobytym, "+
    "wyznacza nie tylko granice mi^edzy kr^olestwami Redanii i Temeri, "+
    "ale i skutecznie ogranicza ludzkie zap^edy w wyrywaniu z r^ak "+
    "matki Natury wszystkiego, co sie jej nale^zy. Gdzieniegdzie "+
    "pojawiaj^ace si^e z nienacka niewielkie zawirowania wody na "+
    "spokojnie, jakby leniwie poruszaj^acej si^e m^etnej tafli swiadcz^a "+
    "o nieprzywidywalno^sci i zdradzieckiej naturze nurtu, kt^ory zapewne "+
    "pochlon^a^l niejedno istnienie bezmy^slnie probuj^ace zmierzy^c si^e z "+
    "pot^eg^a rzeki.\n");
}

void
init()
{
    ::init();
    init_drink_water(); 
} 

string
dlugi_opis()
{
    string str;
    if(jest_dzien() == 1)
    {
        if(pora_roku() != MT_ZIMA)
        {
            str = "Niezwykle mi^ekka trawa, porasta runo le^sne, okrywaj^ac "+
            "zielonym kocem wszelkie butwiej^ace li^scie, choinowe igie^lki "+
            "i kopczyki drobnych mrowisk. To Pontar, kt^orego wody "+
            "niebieszczej^a na po^ludniu, u^zy^xnia ziemi^e. B^l^ekitne "+
            "fale obmywaj^a wystrz^epiony brzeg. Korzenie drzew i co "+
            "wi^ekszych ro^slin zwieszaj^a si^e nad powierzchni^a rzeki, "+
            "tworz^ac niezwyk^l^a pl^atanin^e wielu odcieni br^azu i "+
            "szaro^sci. Same drzewa natomiast strzelaj^a w g^or^e dumnie "+
            "i pr^e^znie, ich kora jest gruba i zdrowa, a li^scie "+
            "szeleszcz^a delikatnie muskane przez wiatr. Gdzie^s w g^aszczu "+
            "trawy zagubi^l si^e ^swierszcz i teraz wygrywa swoj^a cich^a "+
            "melodi^e, wpl^atuj^ac j^a w gam^e le^snych odg^los^ow. Na "+
            "p^o^lnocy , p^o^lnocnym wschodzie i wschodzie, ukryta za "+
            "rz^edem g^estych krzak^ow, czernieje ^scie^zka traktu. "+
            "Na szcz^e^scie jednak ^l^aczka jest nie zadeptana i zupe^lnie "+
            "^swie^za, wida^c, ^ze kolumny podr^o^znych ci^agn^acych tym "+
            "traktem, niemal nigdy o ten zak^atek nie zahaczaj^a.";
        }
        else
        {
            str = "Niezwykle mi^ekki ^snieg, okrywa puchatym, bia^lym kocem "+
            "runo le^sne, grzebi^ac pod sw^a pokryw^a wszelkie butwiej^ace "+
            "li^scie, choinowe igie^lki i kopczyki drobnych mrowisk. Wody "+
            "Pontaru przelewaj^a si^e granatow^a fal^a, nios^ac ze sob^a "+
            "kry i kawa^lki po^lamanych ga^l^ezi, zabranych wzd^lu^z "+
            "wy^zszego biegu rzeki. Korzenie drzew i co wi^ekszych ro^slin "+
            "zwieszaj^a si^e nad powierzchni^a rzeki, tworz^ac niezwyk^l^a "+
            "pl^atanin^e wielu odcieni br^azu i szaro^sci. Drzewa natomiast "+
            "strzelaj^a w g^or^e dumnie i pr^e^znie, ich kora jest gruba i "+
            "zdrowa a ga^l^ezie, przykryte grub^a poduch^a ^sniegu "+
            "poskrzypuj^a cicho w rytm wiatru. Na p^o^lnocy , p^o^lnocnym "+
            "wschodzie i wschodzie, ukryta za rz^edem g^estych krzak^ow, "+
            "czernieje ^scie^zka traktu. Na szcz^e^scie jednak ^snieg na "+
            "^l^aczce jest ^swie^zy i nie zadeptany, wida^c, ^ze kolumny "+
            "^podr^o^znych ci^agn^acych tym traktem, niemal nigdy o ten "+
            "^zak^atek nie zahaczaj^a.";
        }
    }
    if(jest_dzien() == 0)
    {
        if(pora_roku() != MT_ZIMA)
        {
            str = "Niezwykle mi^ekka trawa, porasta runo le^sne, okrywaj^ac "+
            "zielonym kocem wszelkie butwiej^ace li^scie, choinowe igie^lki "+
            "i kopczyki drobnych mrowisk. To Pontar, kt^orego wody "+
            "niebieszczej^a na po^ludniu, u^zy^xnia ziemi^e. B^l^ekitne "+
            "fale obmywaj^a wystrz^epiony brzeg. Korzenie drzew i co "+
            "wi^ekszych ro^slin zwieszaj^a si^e nad powierzchni^a rzeki, "+
            "tworz^ac niezwyk^l^a pl^atanin^e wielu odcieni br^azu i "+
            "szaro^sci. Same drzewa natomiast strzelaj^a w g^or^e dumnie i "+
            "pr^e^znie, ich kora jest gruba i zdrowa, a li^scie szeleszcz^a "+
            "delikatnie muskane przez wiatr. Gdzie^s w g^aszczu trawy "+
            "zagubi^l si^e ^swierszcz i teraz wygrywa swoj^a cich^a "+
            "melodi^e, wpl^atuj^ac j^a w gam^e le^snych odg^los^ow. Na "+
            "p^o^lnocy , p^o^lnocnym wschodzie i wschodzie, ukryta za "+
            "rz^edem g^estych krzak^ow, czernieje ^scie^zka traktu. "+
            "Na szcz^e^scie jednak ^l^aczka jest nie zadeptana i zupe^lnie "+
            "^swie^za, wida^c, ^ze kolumny podr^o^znych ci^agn^acych tym "+
            "traktem, niemal nigdy o ten zak^atek nie zahaczaj^a.";
        }
        else
        {
            str = "Niezwykle mi^ekki ^snieg, okrywa puchatym, bia^lym kocem "+
            "runo le^sne, grzebi^ac pod sw^a pokryw^a wszelkie butwiej^ace "+
            "li^scie, choinowe igie^lki i kopczyki drobnych mrowisk. Wody "+
            "Pontaru przelewaj^a si^e granatow^a fal^a, nios^ac ze sob^a "+
            "kry i kawa^lki po^lamanych ga^l^ezi, zabranych wzd^lu^z "+
            "wy^zszego biegu rzeki. Korzenie drzew i co wi^ekszych ro^slin "+
            "zwieszaj^a si^e nad powierzchni^a rzeki, tworz^ac niezwyk^l^a "+
            "pl^atanin^e wielu odcieni br^azu i szaro^sci. Drzewa natomiast "+
            "strzelaj^a w g^or^e dumnie i pr^e^znie, ich kora jest gruba i "+
            "zdrowa a ga^l^ezie, przykryte grub^a poduch^a ^sniegu "+
            "poskrzypuj^a cicho w rytm wiatru. Na p^o^lnocy , p^o^lnocnym "+
            "wschodzie i wschodzie, ukryta za rz^edem g^estych krzak^ow, "+
            "czernieje ^scie^zka traktu. Na szcz^e^scie jednak ^snieg na "+
            "^l^aczce jest ^swie^zy i nie zadeptany, wida^c, ^ze kolumny "+
            "^podr^o^znych ci^agn^acych tym traktem, niemal nigdy o ten "+
            "^zak^atek nie zahaczaj^a.";
        }
    }
    str += "\n";
    return str;
}
string
opis_nocy()
{
    string str;
    if(pora_roku() != MT_ZIMA)
    {
    str = "Niezwykle mi^ekka trawa, porasta runo le^sne, okrywaj^ac "+
        "zielonym kocem wszelkie butwiej^ace li^scie, choinowe igie^lki i "+
        "kopczyki drobnych mrowisk. To Pontar, kt^orego wody niebieszczej^a "+
        "na po^ludniu, u^zy^xnia ziemi^e. B^l^ekitne fale obmywaj^a "+
        "wystrz^epiony brzeg. Korzenie drzew i co wi^ekszych ro^slin "+
        "zwieszaj^a si^e nad powierzchni^a rzeki, tworz^ac niezwyk^l^a "+
        "pl^atanin^e wielu odcieni br^azu i szaro^sci. Same drzewa "+
        "natomiast strzelaj^a w g^or^e dumnie i pr^e^znie, ich kora jest "+
        "gruba i zdrowa, a li^scie szeleszcz^a delikatnie muskane przez "+
        "wiatr. Gdzie^s w g^aszczu trawy zagubi^l si^e ^swierszcz i teraz "+
        "wygrywa swoj^a cich^a melodi^e, wpl^atuj^ac j^a w gam^e le^snych "+
        "odg^los^ow. Na p^o^lnocy , p^o^lnocnym wschodzie i wschodzie, "+
        "ukryta za rz^edem g^estych krzak^ow, czernieje ^scie^zka traktu. "+
        "Na szcz^e^scie jednak ^l^aczka jest nie zadeptana i zupe^lnie "+
        "^swie^za, wida^c, ^ze kolumny podr^o^znych ci^agn^acych tym "+
        "traktem, niemal nigdy o ten zak^atek nie zahaczaj^a.";
    }
    else
    {
    str = "Niezwykle mi^ekki ^snieg, okrywa puchatym, bia^lym kocem runo "+
        "le^sne, grzebi^ac pod sw^a pokryw^a wszelkie butwiej^ace li^scie, "+
        "choinowe igie^lki i kopczyki drobnych mrowisk. Wody Pontaru "+
        "przelewaj^a si^e granatow^a fal^a, nios^ac ze sob^a kry i kawa^lki "+
        "po^lamanych ga^l^ezi, zabranych wzd^lu^z wy^zszego biegu rzeki. "+
        "Korzenie drzew i co wi^ekszych ro^slin zwieszaj^a si^e nad "+
        "powierzchni^a rzeki, tworz^ac niezwyk^l^a pl^atanin^e wielu "+
        "odcieni br^azu i szaro^sci. Drzewa natomiast strzelaj^a w g^or^e "+
        "dumnie i pr^e^znie, ich kora jest gruba i zdrowa a ga^l^ezie, "+
        "przykryte grub^a poduch^a ^sniegu poskrzypuj^a cicho w rytm "+
        "wiatru. Na p^o^lnocy , p^o^lnocnym wschodzie i wschodzie, ukryta "+
        "za rz^edem g^estych krzak^ow, czernieje ^scie^zka traktu. Na "+
        "szcz^e^scie jednak ^snieg na ^l^aczce jest ^swie^zy i nie "+
        "zadeptany, wida^c, ^ze kolumny podr^o^znych ci^agn^acych tym "+
        "traktem, niemal nigdy o ten zak^atek nie zahaczaj^a.";
    }
    str += "\n";
    return str;
}
string
pora_rok()
{
    switch (pora_roku())
    {
        case MT_LATO:
        case MT_WIOSNA:
            return ({"Wa^zka przelecia^la tu^z ko^lo twojego nosa.\n",
            "W wodzie trzepocze przez chwil^e jaka^s ryba, po czym niknie w "+
            "g^l^ebinach.\n",
            "Fale Pontaru szumi^a cicho.\n",
            "Gdzie^s od strony traktu dochodz^a ci^e nawo^lywania.\n"})
            [random(3)];
         case MT_JESIEN:
             return ({"Bezlistne ga^l^ezie muskaj^a tw^oj policzek.\n",
             "Gdzie^s od strony traktu dochodz^a ci^e nawo^lywania.\n"})
             [random(2)];
         case MT_ZIMA:
             return ({"Fale Pontaru hucz^a gro^xnie.\n",
             "Bezlistne ga^l^ezie muskaj^a tw^oj policzek.\n",
             "Kra z trzaskiem od^lamuje si^e od brzegu i odp^lywa niesiona "+
             "silnym nurtem.\n","Gdzie^s od strony traktu dochodz^a ci^e "+
             "nawo^lywania.\n","Granatowe fale Pontaru podchodz^a wysoko "+
             "pod wystrz^epiony brzeg.\n","Pontar huczy gro^xnie.\n",})
             [random(4)];
    }
}
