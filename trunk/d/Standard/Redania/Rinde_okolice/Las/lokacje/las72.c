/* Autor: Avard
   Data : 21.06.06
   Opis : Tinardan 
   Info : Wykonano ku chwale Swiatowida */

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <pogoda.h>
#include "dir.h"

inherit LAS_RINDE_STD;
inherit "/lib/drink_water";

void create_las() 
{
    set_short("Las nad brzegiem rzeki");
    add_exit(LAS_RINDE_LOKACJE + "las71.c","e",0,LAS_RO_FATIG,1);
    add_exit(LAS_RINDE_LOKACJE + "las66.c","nw",0,LAS_RO_FATIG,1);
    add_exit(TRAKT_RINDE_LOKACJE + "trakt22.c","n",0,TRAKT_RO_FATIG,0);
    add_exit(TRAKT_RINDE_LOKACJE + "trakt21.c","ne",0,TRAKT_RO_FATIG,0);

    add_prop(ROOM_I_INSIDE,0);
    add_event("@@pora_rok:"+file_name(TO)+"@@");

    set_drink_places(({"z rzeki","z pontaru","z Pontaru"}));
    add_sit(({"nad rzek^a","nad pontarem","nad Pontarem"}),
        "nad rzek^a","z nad rzeki",0);

    add_item(({"Pontar","pontar","rzek^e"}),"P^lyn^acy tu od tysi^ecy lat "+
    "szeroki Pontar zdaje si^e by^c wci^a^z nieujarzmionym i niezdobytym, "+
    "wyznacza nie tylko granice mi^edzy kr^olestwami Redanii i Temeri, "+
    "ale i skutecznie ogranicza ludzkie zap^edy w wyrywaniu z r^ak "+
    "matki Natury wszystkiego, co sie jej nale^zy. Gdzieniegdzie "+
    "pojawiaj^ace si^e z nienacka niewielkie zawirowania wody na "+
    "spokojnie, jakby leniwie poruszaj^acej si^e m^etnej tafli swiadcz^a "+
    "o nieprzywidywalno^sci i zdradzieckiej naturze nurtu, kt^ory zapewne "+
    "pochlon^a^l niejedno istnienie bezmy^slnie probuj^ace zmierzy^c si^e z "+
    "pot^eg^a rzeki.\n");

}

void
init()
{
    ::init();
    init_drink_water(); 
} 


string
dlugi_opis()
{
    string str;
    if(jest_dzien() == 1)
    {
        if(pora_roku() != MT_ZIMA)
        {
            str = "To t^edy w^la^snie Pontar toczy swe leniwe wody, "+
            "obdarowuj^ac dolin^e urodzajem i bogactwem. B^l^ekitna, "+
            "spokojna woda obmywa piaszczysty brzeg, na kt^orym, tu^z-tu^z, "+
            "rosn^a drzewa. D^lugie ga^l^ezie pochylaj^a si^e nad "+
            "b^l^ekitnymi wodami i lekko muskaj^a delikatnymi li^s^cmi "+
            "aksamitn^a powierzchni^e rzeki. W lesie panuje szczeg^olne "+
            "wra^zenie spokoju, ptaki po^spiewuj^a weso^lo, na grubym "+
            "pok^ladzie butwiej^acych li^sci widoczne s^a tropy jaki^s "+
            "zwierz^at, a ch^lodna woda rzeki szemrze cicho i "+
            "uspokajaj^aco. Drzewa tu s^a zdrowe i wysokie, wyros^le "+
            "bujnie i dumnie we wspania^lej okolicy. Gdzie^s na p^o^lnocy "+
            "majaczy w^aska linia traktu, niczym szarawy prze^swit "+
            "pomi^edzy drzewami. ";
        }
        else
        {
            str = "To t^edy w^la^snie Pontar toczy swe leniwe wody, "+
            "obdarowuj^ac dolin^e urodzajem i bogactwem. L^od sku^l "+
            "powierzchni^e rzeki i uwi^ezi^l jej fale pod bia^l^a kr^a, "+
            "ale te przebijaj^a si^e co jaki^s czas, pot^e^zne i granatowe, "+
            "gro^xne niczym rozw^scieczony olbrzym. Drzewa, smuk^le i "+
            "ciemne na tle ^sniegu, pochylaj^a swe bezlistne ga^l^ezie ku "+
            "powierzchni lodu, jakby w niemej t^esknocie za o^zywcz^a wod^a, "+
            "ciep^lem i wiosn^a. Ich korzenie, a tak^ze dolna cz^e^s^c pni, "+
            "okryte s^a, niczym pierzyn^a, grub^a warstw^a ^sniegu, na "+
            "kt^orej wyra^xnie odbijaj^a si^e tropy le^snej zwierzyny. "+
            "Gdzie^s na p^o^lnocy majaczy trakt, w^aski i odcinaj^acy "+
            "si^e od ^sniegu czerni^a zamarzni^etego b^lota.";
         }
    }
    if(jest_dzien() == 0)
    {
        if(pora_roku() != MT_ZIMA)
        {
            str = "To t^edy w^la^snie Pontar toczy swe leniwe wody, "+
            "obdarowuj^ac dolin^e urodzajem i bogactwem. B^l^ekitna, "+
            "spokojna woda obmywa piaszczysty brzeg, na kt^orym, tu^z-tu^z, "+
            "rosn^a drzewa. D^lugie ga^l^ezie pochylaj^a si^e nad "+
            "b^l^ekitnymi wodami i lekko muskaj^a delikatnymi li^s^cmi "+
            "aksamitn^a powierzchni^e rzeki. W lesie panuje szczeg^olne "+
            "wra^zenie spokoju, ptaki po^spiewuj^a weso^lo, na grubym "+
            "pok^ladzie butwiej^acych li^sci widoczne s^a tropy jaki^s "+
            "zwierz^at, a ch^lodna woda rzeki szemrze cicho i "+
            "uspokajaj^aco. Drzewa tu s^a zdrowe i wysokie, wyros^le "+
            "bujnie i dumnie we wspania^lej okolicy. Gdzie^s na p^o^lnocy "+
            "majaczy w^aska linia traktu, niczym szarawy prze^swit "+
            "pomi^edzy drzewami. ";
        }
        else
        {
            str = "To t^edy w^la^snie Pontar toczy swe leniwe wody, "+
            "obdarowuj^ac dolin^e urodzajem i bogactwem. L^od sku^l "+
            "powierzchni^e rzeki i uwi^ezi^l jej fale pod bia^l^a kr^a, "+
            "ale te przebijaj^a si^e co jaki^s czas, pot^e^zne i granatowe, "+
            "gro^xne niczym rozw^scieczony olbrzym. Drzewa, smuk^le i "+
            "ciemne na tle ^sniegu, pochylaj^a swe bezlistne ga^l^ezie ku "+
            "powierzchni lodu, jakby w niemej t^esknocie za o^zywcz^a wod^a, "+
            "ciep^lem i wiosn^a. Ich korzenie, a tak^ze dolna cz^e^s^c pni, "+
            "okryte s^a, niczym pierzyn^a, grub^a warstw^a ^sniegu, na "+
            "kt^orej wyra^xnie odbijaj^a si^e tropy le^snej zwierzyny. "+
            "Gdzie^s na p^o^lnocy majaczy trakt, w^aski i odcinaj^acy "+
            "si^e od ^sniegu czerni^a zamarzni^etego b^lota.";
         }
    }
    str += "\n";
    return str;
}

string
opis_nocy()
{
    string str;
    if(pora_roku() != MT_ZIMA)
    {
        str = "To t^edy w^la^snie Pontar toczy swe leniwe wody, "+
            "obdarowuj^ac dolin^e urodzajem i bogactwem. B^l^ekitna, "+
            "spokojna woda obmywa piaszczysty brzeg, na kt^orym, tu^z-tu^z, "+
            "rosn^a drzewa. D^lugie ga^l^ezie pochylaj^a si^e nad "+
            "b^l^ekitnymi wodami i lekko muskaj^a delikatnymi li^s^cmi "+
            "aksamitn^a powierzchni^e rzeki. W lesie panuje szczeg^olne "+
            "wra^zenie spokoju, ptaki po^spiewuj^a weso^lo, na grubym "+
            "pok^ladzie butwiej^acych li^sci widoczne s^a tropy jaki^s "+
            "zwierz^at, a ch^lodna woda rzeki szemrze cicho i "+
            "uspokajaj^aco. Drzewa tu s^a zdrowe i wysokie, wyros^le bujnie "+
            "i dumnie we wspania^lej okolicy. Gdzie^s na p^o^lnocy majaczy "+
            "w^aska linia traktu, niczym szarawy prze^swit pomi^edzy "+
            "drzewami.";
    }
    else
    {
        str = "To t^edy w^la^snie Pontar toczy swe leniwe wody, "+
            "obdarowuj^ac dolin^e urodzajem i bogactwem. L^od sku^l "+
            "powierzchni^e rzeki i uwi^ezi^l jej fale pod bia^l^a kr^a, "+
            "ale te przebijaj^a si^e co jaki^s czas, pot^e^zne i granatowe, "+
            "gro^xne niczym rozw^scieczony olbrzym. Drzewa, smuk^le i "+
            "ciemne na tle ^sniegu, pochylaj^a swe bezlistne ga^l^ezie ku "+
            "powierzchni lodu, jakby w niemej t^esknocie za o^zywcz^a "+
            "wod^a, ciep^lem i wiosn^a. Ich korzenie, a tak^ze dolna "+
            "cz^e^s^c pni, okryte s^a, niczym pierzyn^a, grub^a warstw^a "+
            "^sniegu, na kt^orej wyra^xnie odbijaj^a si^e tropy le^snej "+
            "zwierzyny. Gdzie^s na p^o^lnocy majaczy trakt, w^aski i "+
            "odcinaj^acy si^e od ^sniegu czerni^a zamarzni^etego b^lota.";
    }
    str += "\n";
    return str;
}
string
pora_rok()
{
    switch (pora_roku())
    {
         case MT_LATO:
         case MT_WIOSNA:
             return ({"Wa^zka przelecia^la tu^z ko^lo twojego nosa.\n",
             "W wodzie trzepocze przez chwil^e jaka^s ryba, po czym "+
             "niknie w g^l^ebinach.\n",
             "Fale Pontaru szumi^a cicho.\n"})[random(3)];
         case MT_JESIEN:
             return ("Bezlistne ga^l^ezie muskaj^a tw^oj policzek.\n");
         case MT_ZIMA:
             return ({"Fale Pontaru hucz^a gro^xnie.\n",
             "Bezlistne ga^l^ezie muskaj^a tw^oj policzek.\n",
             "Kra z trzaskiem od^lamuje si^e od brzegu i odp^lywa "+
             "niesiona silnym nurtem.\n"})[random(3)];
    }
}


