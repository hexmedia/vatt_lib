/* Autor: Avard
   Data : 04.12.06
   Opis : Yran */

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <pogoda.h>
#include "dir.h"

inherit LAS_RINDE_STD;

void create_las() 
{
    set_short("W lesie");
    add_exit(LAS_RINDE_LOKACJE + "las42.c","e",0,LAS_RO_FATIG,1);
    add_exit(LAS_RINDE_LOKACJE + "las53.c","s",0,LAS_RO_FATIG,1);
    add_exit(LAS_RINDE_LOKACJE + "las54.c","sw",0,LAS_RO_FATIG,1);

    add_prop(ROOM_I_INSIDE,0);
}

string
dlugi_opis()
{
    string str;
    if(jest_dzien() == 1)
    {
        if(pora_roku() != MT_ZIMA)
        {
            str = "Las jest w tym miejscu pe^len wszelakich chaszczy, "+
                "kolczastych krzak^ow i g^esto rosn^acych drzew. Niskie "+
                "brz^ozki o bia^lej korze pn^a swe m^lode, li^sciaste "+
                "ga^l^azki w g^ore ku ^zyciodajnym promieniom s^lonecznym "+
                "jednak skutecznie utrudniaj^a im to unosz^ace si^e "+
                "monumentalnie ponad nimi dostojnie ko^lysz^ace si^e na "+
                "lekkim wietrze roz^lo^zyste d^eby, kt^orych ga^l^ezie "+
                "zwisaja niemal nad sam^a ziemie, jakby swoimi d^lugimi "+
                "r^ekoma chcia^ly podnie^s^c co^s z ziemi. Na pod^lo^zu "+
                "le^zy mn^ostwo ma^lych patyczk^ow, kt^ore zosta^ly "+
                "brutalnie wyrwane z drzewa podczas jakiego^s porywistego "+
                "podmuchu wiatru. W wielu miejscach owe kijki sa po^lamane, "+
                "ale nie ma czemu si^e dziwi^c, wszak w okolicy jest wielu "+
                "drapie^znik^ow przemierzaj^acych las w poszukiwaniu "+
                "po^zywienia, kt^ore pod wp^lywem swojego ci^e^zaru bez "+
                "trudu s^a w stanie je po^lama^c.";
        }
        else
        {
            str = "Las jest w tym miejscu pe^len wszelakich chaszczy, "+
                "kolczastych krzak^ow i g^esto rosn^acych drzew. Niskie "+
                "brz^ozki o bia^lej korze pn^a swe m^lode, bezlistne, "+
                "zapadni^ete w zimowy sen ga^l^azki w g^ore ku ^zyciodajnym "+
                "promieniom s^lonecznym jednak skutecznie utrudniaj^a im "+
                "to unosz^ace si^e monumentalnie ponad nimi dostojnie "+
                "ko^lysz^ace si^e na lekkim wietrze roz^lo^zyste d^eby, "+
                "kt^orych ga^l^ezie zwisaja niemal nad sam^a ziemie, jakby "+
                "swoimi d^lugimi r^ekoma chcia^ly podnie^s^c co^s z ziemi. "+
                "Na pod^lo^zu zalega warstwa bia^lego puchu, na kt^orej "+
                "spoczywa mn^ostwo ma^lych patyczk^ow, kt^ore zosta^ly "+
                "brutalnie wyrwane z drzewa podczas jakiego^s porywistego "+
                "podmuchu wiatru. W wielu miejscach owe kijki sa po^lamane, "+
                "ale nie ma czemu si^e dziwi^c, wszak w okolicy jest wielu "+
                "drapie^znik^ow przemierzaj^acych las w poszukiwaniu "+
                "po^zywienia, kt^ore pod wp^lywem swojego ci^e^zaru bez "+
                "trudu s^a w stanie je po^lama^c. ^Snieg zacinaj^acy "+
                "jeszcze przed chwil^a z zachodu delikatnie obsypa^l "+
                "okolic^e mi^ekkim puchem. ";
        }
    }
    if(jest_dzien() == 0)
    {
        if(pora_roku() != MT_ZIMA)
        {
            str = "Las jest w tym miejscu pe^len wszelakich chaszczy, "+
                "kolczastych krzak^ow i g^esto rosn^acych drzew. Niskie "+
                "brz^ozki o bia^lej korze pn^a swe m^lode, li^sciaste "+
                "ga^l^azki w g^ore ku ^zyciodajnym promieniom s^lonecznym "+
                "jednak skutecznie utrudniaj^a im to unosz^ace si^e "+
                "monumentalnie ponad nimi dostojnie ko^lysz^ace si^e na "+
                "lekkim wietrze roz^lo^zyste d^eby, kt^orych ga^l^ezie "+
                "zwisaja niemal nad sam^a ziemie, jakby swoimi d^lugimi "+
                "r^ekoma chcia^ly podnie^s^c co^s z ziemi. Na pod^lo^zu "+
                "le^zy mn^ostwo ma^lych patyczk^ow, kt^ore zosta^ly "+
                "brutalnie wyrwane z drzewa podczas jakiego^s porywistego "+
                "podmuchu wiatru. W wielu miejscach owe kijki sa po^lamane, "+
                "ale nie ma czemu si^e dziwi^c, wszak w okolicy jest wielu "+
                "drapie^znik^ow przemierzaj^acych las w poszukiwaniu "+
                "po^zywienia, kt^ore pod wp^lywem swojego ci^e^zaru bez "+
                "trudu s^a w stanie je po^lama^c.";
        }
        else
        {
            str = "Las jest w tym miejscu pe^len wszelakich chaszczy, "+
                "kolczastych krzak^ow i g^esto rosn^acych drzew. Niskie "+
                "brz^ozki o bia^lej korze pn^a swe m^lode, bezlistne, "+
                "zapadni^ete w zimowy sen ga^l^azki w g^ore ku ^zyciodajnym "+
                "promieniom s^lonecznym jednak skutecznie utrudniaj^a im "+
                "to unosz^ace si^e monumentalnie ponad nimi dostojnie "+
                "ko^lysz^ace si^e na lekkim wietrze roz^lo^zyste d^eby, "+
                "kt^orych ga^l^ezie zwisaja niemal nad sam^a ziemie, jakby "+
                "swoimi d^lugimi r^ekoma chcia^ly podnie^s^c co^s z ziemi. "+
                "Na pod^lo^zu zalega warstwa bia^lego puchu, na kt^orej "+
                "spoczywa mn^ostwo ma^lych patyczk^ow, kt^ore zosta^ly "+
                "brutalnie wyrwane z drzewa podczas jakiego^s porywistego "+
                "podmuchu wiatru. W wielu miejscach owe kijki sa po^lamane, "+
                "ale nie ma czemu si^e dziwi^c, wszak w okolicy jest wielu "+
                "drapie^znik^ow przemierzaj^acych las w poszukiwaniu "+
                "po^zywienia, kt^ore pod wp^lywem swojego ci^e^zaru bez "+
                "trudu s^a w stanie je po^lama^c. ^Snieg zacinaj^acy "+
                "jeszcze przed chwil^a z zachodu delikatnie obsypa^l "+
                "okolic^e mi^ekkim puchem. ";
        }
    }
    str +="\n";
    return str;
}
string
opis_nocy()
{
    string str;
    if(pora_roku() != MT_ZIMA)
        {
            str = "Las jest w tym miejscu pe^len wszelakich chaszczy, "+
                "kolczastych krzak^ow i g^esto rosn^acych drzew. Niskie "+
                "brz^ozki o bia^lej korze pn^a swe m^lode, li^sciaste "+
                "ga^l^azki w g^ore ku ^zyciodajnym promieniom s^lonecznym "+
                "jednak skutecznie utrudniaj^a im to unosz^ace si^e "+
                "monumentalnie ponad nimi dostojnie ko^lysz^ace si^e na "+
                "lekkim wietrze roz^lo^zyste d^eby, kt^orych ga^l^ezie "+
                "zwisaja niemal nad sam^a ziemie, jakby swoimi d^lugimi "+
                "r^ekoma chcia^ly podnie^s^c co^s z ziemi. Na pod^lo^zu "+
                "le^zy mn^ostwo ma^lych patyczk^ow, kt^ore zosta^ly "+
                "brutalnie wyrwane z drzewa podczas jakiego^s porywistego "+
                "podmuchu wiatru. W wielu miejscach owe kijki sa po^lamane, "+
                "ale nie ma czemu si^e dziwi^c, wszak w okolicy jest wielu "+
                "drapie^znik^ow przemierzaj^acych las w poszukiwaniu "+
                "po^zywienia, kt^ore pod wp^lywem swojego ci^e^zaru bez "+
                "trudu s^a w stanie je po^lama^c.";
        }
        else
        {
            str = "Las jest w tym miejscu pe^len wszelakich chaszczy, "+
                "kolczastych krzak^ow i g^esto rosn^acych drzew. Niskie "+
                "brz^ozki o bia^lej korze pn^a swe m^lode, bezlistne, "+
                "zapadni^ete w zimowy sen ga^l^azki w g^ore ku ^zyciodajnym "+
                "promieniom s^lonecznym jednak skutecznie utrudniaj^a im "+
                "to unosz^ace si^e monumentalnie ponad nimi dostojnie "+
                "ko^lysz^ace si^e na lekkim wietrze roz^lo^zyste d^eby, "+
                "kt^orych ga^l^ezie zwisaja niemal nad sam^a ziemie, jakby "+
                "swoimi d^lugimi r^ekoma chcia^ly podnie^s^c co^s z ziemi. "+
                "Na pod^lo^zu zalega warstwa bia^lego puchu, na kt^orej "+
                "spoczywa mn^ostwo ma^lych patyczk^ow, kt^ore zosta^ly "+
                "brutalnie wyrwane z drzewa podczas jakiego^s porywistego "+
                "podmuchu wiatru. W wielu miejscach owe kijki sa po^lamane, "+
                "ale nie ma czemu si^e dziwi^c, wszak w okolicy jest wielu "+
                "drapie^znik^ow przemierzaj^acych las w poszukiwaniu "+
                "po^zywienia, kt^ore pod wp^lywem swojego ci^e^zaru bez "+
                "trudu s^a w stanie je po^lama^c. ^Snieg zacinaj^acy "+
                "jeszcze przed chwil^a z zachodu delikatnie obsypa^l "+
                "okolic^e mi^ekkim puchem. ";
        }
    str +="\n";
    return str;
}

