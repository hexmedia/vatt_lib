
/* Autor: Avard
   Data : 26.09.06
   Opis : vortak */
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <pogoda.h>
#include "dir.h"

inherit WLAS_RINDE_STD;

void create_las() 
{
    set_short("Na skraju wykarczowanego lasu");
    add_exit(LAS_RINDE_LOKACJE + "las1.c","nw",0,WLAS_RO_FATIG,1);
    add_exit(LAS_RINDE_LOKACJE + "wlas8.c","n",0,WLAS_RO_FATIG,1);
    add_exit(LAS_RINDE_LOKACJE + "wlas7.c","ne",0,WLAS_RO_FATIG,1);
    add_exit(LAS_RINDE_LOKACJE + "wlas10.c","e",0,WLAS_RO_FATIG,1);
    add_exit(LAS_RINDE_LOKACJE + "wlas13.c","se",0,WLAS_RO_FATIG,1);
    add_exit(LAS_RINDE_LOKACJE + "wlas14.c","s",0,WLAS_RO_FATIG,1);
    add_exit(LAS_RINDE_LOKACJE + "wlas15.c","sw",0,WLAS_RO_FATIG,1);
    add_npc(LAS_RINDE_LIVINGI + "sarna");

    add_prop(ROOM_I_INSIDE,0);
}
public string
exits_description() 
{
    return "Wykarczowany las wida^c na p^o^lnocy, p^o^lnocnym-wschodzie, "+
        "wschodzie, po^ludniowym-wschodzie, po^ludniu i "+
        "po^ludniowym-zachodzie. Na p^o^lnocnym-zachodzie znajduje si^e "+
        "wej^scie do lasu.\n";
}

string
dlugi_opis()
{
    string str;
    if(jest_dzien() == 1)
    {
        if(pora_roku() != MT_ZIMA)
        {
            str = "^Sciana lasu widoczna na zachodzie zdaje si^e by^c tak "+
            "g^esta, ^ze o pr^obach jej sforsowania nie mo^ze by^c mowy. "+
            "W^la^snie tu pot^e^zne, zielone drzewa tocz^a nier^own^a "+
            "walk^e z ostrzami drwalskich topor^ow. Z zalegaj^acej na "+
            "pod^lo^zu grubej warstwy brunatnych li^sci stercz^a grube "+
            "pniaki - pozosta^lo^sci po rosn^acym tu od niepami^etnych "+
            "lat lesie. Wyra^xne ^slady na ziemi prowadz^a do widocznego "+
            "na po^ludniu tartaku. Z tej w^la^snie strony dobiega coraz "+
            "wyra^xniejszy gwar pracuj^acego pe^ln^a par^a zak^ladu. "+
            "Daleko na p^o^lnocnym-wschodzie majacz^a, ledwo st^ad "+
            "widoczne miejskie mury.";
        }
        else
        {
            str = "^Sciana lasu widoczna na zachodzie zdaje si^e by^c tak "+
            "g^esta, ^ze o pr^obach jej sforsowania nie mo^ze by^c mowy. "+
            "W^la^snie tu pot^e^zne, pokryte ^snie^znobia^lym puchem "+
            "drzewa tocz^a nier^own^a walk^e z ostrzami drwalskich "+
            "topor^ow. Z grubej warstwy ^sniegu zalegaj^acego na pod^lo^zu "+
            "stercz^a grube pniaki- pozosta^lo^sci po rosn^acym tu od "+
            "niepami^etnych lat lesie. Wyra^xnie widoczne na ziemi ^slady "+
            "prowadz^a do widocznego na po^ludniu tartaku. Z tej w^la^snie "+
            "strony dobiega coraz wyra^xniejszy gwar pracuj^acego pe^ln^a "+
            "par^a zak^ladu. Daleko na p^o^lnocnym-wschodzie majacz^a, "+
            "ledwo st^ad widoczne miejskie mury.";
        }
    }
    if(jest_dzien() == 0)
    {
        if(pora_roku() != MT_ZIMA)
        {
            str = "^Sciana lasu widoczna na zachodzie zdaje si^e by^c tak "+
            "g^esta, ^ze o pr^obach jej sforsowania nie mo^ze by^c mowy. "+
            "W^la^snie tu pot^e^zne, zielone drzewa tocz^a nier^own^a "+
            "walk^e z ostrzami drwalskich topor^ow. Z zalegaj^acej na "+
            "pod^lo^zu grubej warstwy brunatnych li^sci stercz^a grube "+
            "pniaki - pozosta^lo^sci po rosn^acym tu od niepami^etnych "+
            "lat lesie. Wyra^xne ^slady na ziemi prowadz^a do widocznego "+
            "na po^ludniu tartaku. "+
            "Daleko na p^o^lnocnym-wschodzie majacz^a, ledwo st^ad "+
            "widoczne miejskie mury.";
        }
        else
        {
            str = "^Sciana lasu widoczna na zachodzie zdaje si^e by^c tak "+
            "g^esta, ^ze o pr^obach jej sforsowania nie mo^ze by^c mowy. "+
            "W^la^snie tu pot^e^zne, pokryte ^snie^znobia^lym puchem "+
            "drzewa tocz^a nier^own^a walk^e z ostrzami drwalskich "+
            "topor^ow. Z grubej warstwy ^sniegu zalegaj^acego na pod^lo^zu "+
            "stercz^a grube pniaki- pozosta^lo^sci po rosn^acym tu od "+
            "niepami^etnych lat lesie. Wyra^xnie widoczne na ziemi ^slady "+
            "prowadz^a do widocznego na po^ludniu tartaku. Daleko na "+
            "p^o^lnocnym-wschodzie majacz^a, "+
            "ledwo st^ad widoczne miejskie mury.";
        }
    }
    str += "\n";
    return str;
}
string
opis_nocy()
{
    string str;
    if(pora_roku() != MT_ZIMA)
    {
        str = "^Sciana lasu widoczna na zachodzie zdaje si^e by^c tak "+
            "g^esta, ^ze o pr^obach jej sforsowania nie mo^ze by^c mowy. "+
            "W^la^snie tu pot^e^zne, zielone drzewa tocz^a nier^own^a "+
            "walk^e z ostrzami drwalskich topor^ow. Z zalegaj^acej na "+
            "pod^lo^zu grubej warstwy brunatnych li^sci stercz^a grube "+
            "pniaki - pozosta^lo^sci po rosn^acym tu od niepami^etnych "+
            "lat lesie. Wyra^xne ^slady na ziemi prowadz^a do widocznego "+
            "na po^ludniu tartaku. "+
            "Daleko na p^o^lnocnym-wschodzie majacz^a, ledwo st^ad "+
            "widoczne miejskie mury.";
    }
    else
    {
        str = "^Sciana lasu widoczna na zachodzie zdaje si^e by^c tak "+
            "g^esta, ^ze o pr^obach jej sforsowania nie mo^ze by^c mowy. "+
            "W^la^snie tu pot^e^zne, pokryte ^snie^znobia^lym puchem "+
            "drzewa tocz^a nier^own^a walk^e z ostrzami drwalskich "+
            "topor^ow. Z grubej warstwy ^sniegu zalegaj^acego na pod^lo^zu "+
            "stercz^a grube pniaki- pozosta^lo^sci po rosn^acym tu od "+
            "niepami^etnych lat lesie. Wyra^xnie widoczne na ziemi ^slady "+
            "prowadz^a do widocznego na po^ludniu tartaku. Daleko na "+
            "p^o^lnocnym-wschodzie majacz^a, "+
            "ledwo st^ad widoczne miejskie mury.";
    }
    str += "\n";
    return str;
}
