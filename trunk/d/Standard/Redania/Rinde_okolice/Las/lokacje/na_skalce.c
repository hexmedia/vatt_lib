/* Autor: Avard
   Data : 11.11.06
   Opis : Tinardan */
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <pogoda.h>
#include "dir.h"

inherit "/std/room";

void create_room()
{
    set_short("Na ska^lce");
    add_exit(LAS_RINDE_LOKACJE + "las7.c","d",0,LAS_RO_FATIG,1);
    add_prop(ROOM_I_INSIDE,0);
    set_polmrok_long("@@opis_nocy@@");
    set_long("@@dlugi_opis@@");
    if(pora_roku() != MT_ZIMA)
    {
        add_sit("na piasku","na piasku","z piasku",2);
    }
    else
    {
        add_sit("na ^sniegu","na ^sniegu","z ^sniegu",2);
    }
}
public string
exits_description() 
{
    string str;
    object *kto;
    str = "Na dole znajduje si^e niewielka polanka";
    kto = FILTER_CAN_SEE(FILTER_LIVE(all_inventory(find_object(LAS_RINDE_LOKACJE + "las7"))), this_player());
    if (sizeof(kto) > 0)
    {
	    str +=", na kt�rej widzisz " + COMPOSITE_LIVE(kto, PL_BIE) + ".\n";
    }
    else
    {
        str +=".\n";
    }
    return str;
}
string
dlugi_opis()
{
    string str;
    str = "Szczyt ska^ly jest p^laski i wr^ecz idealny na chwil^e odpoczynku. ";
    if(pora_roku() != MT_ZIMA)
    {
        str += "Drobniusie^nki piasek ";
    }
    else
    {
        str += "Gruba warstwa ^sniegu ";
    }
    str += "pokrywa ca^l^a powierzchni^e. Wok^o^l wida^c jedynie korony "+
        "drzew i niebo, ska^lka jest zbyt niska by zobaczy^c cokolwiek "+
        "wi^ecej. Jednak kiedy si^e spojrzy w d^o^l, ziemia nie jest tak "+
        "blisko jak mog^loby si^e wydawa^c, cho^c z pewno^sci^a istniej^a "+
        "na ^swiecie g^ory i ska^ly sporo wy^zsze i trudniejsze do zdobycia.";
    str += "\n";
    return str;
}
string
opis_nocy()
{
    string str;
    str = "Szczyt ska^ly jest p^laski i wr^ecz idealny na chwil^e odpoczynku. ";
    if(pora_roku() != MT_ZIMA)
    {
        str += "Drobniusie^nki piasek ";
    }
    else
    {
        str += "Gruba warstwa ^sniegu ";
    }
    str += "pokrywa ca^l^a powierzchni^e. Wok^o^l wida^c jedynie korony "+
        "drzew i niebo, ska^lka jest zbyt niska by zobaczy^c cokolwiek "+
        "wi^ecej. Jednak kiedy si^e spojrzy w d^o^l, ziemia nie jest tak "+
        "blisko jak mog^loby si^e wydawa^c, cho^c z pewno^sci^a istniej^a "+
        "na ^swiecie g^ory i ska^ly sporo wy^zsze i trudniejsze do zdobycia.";
    str += "\n";
    return str;
}


