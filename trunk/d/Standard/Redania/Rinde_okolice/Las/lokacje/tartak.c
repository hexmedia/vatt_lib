/* doda�em kod do czasowego zapami�tywania kto atakowa� Raghola -
 * tym nie wyp�aca kasy przez pewien czas (co apok� random czy pami�ta czy nie)
 *  verk. Tuesday 23 September 2008
 */

inherit "/std/room";

#include <stdproperties.h>
#include <dir.h>
#include <macros.h>

mapping zaplaty = ([ ]);
mapping zabrali = ([ ]);

//imiona tych, co atakowali Raghola. Co jaki� czas czy�cimy t� tablic�. V.
string *wrogowie = ({ });

void create_room()
{
    set_short("W tartaku");

    set_long("@@dlugasny@@");
    add_item(({"maszyne", "urzadzenie"}), "Pot^e^zne urz�dzanie wygl�da na bardzo skomplikowane,"
            +" wiele r^o^znych przek^ladni, d�wigni i z^ebatych k^o^l sprawia, ^ze ci^e^zko zrozumie^c jak w^la�ciwie ta ca^la"
            +" machineria dzia^la. W rzeczywisto�ci obs^luga jej jest wprost banalna. Z^ebate ostrze, okr�g^lej"
            +" pi^ly wystaj�cej z p^laskiego blatu maszyny nap^edzane jest si^la mi^e�ni poprzez kr^ecenie korb�"
            +" znajduj�c� si^e z boku maszyny, kt^ora wprawia metalowe ostrze w zawrotn� pr^edko�^c obrot^ow,"
            +" kt^ore bez najmniejszego trudu jest w stanie przeci�c przesuwane po powierzchni urz�dzenia drewno.\n");

    add_item(({"kolo", "pile", "ostrze"}), "Metalowe, cie^nkie ko^lo naje^zone jest wieloma ostrymi z�bkami, kt^ore"
            +" wprawione w ruch bez najmniejszego trudu przecinaj� nawet najtwardsze drewno. Pi^la przymocowana"
            +" jest do stalowego pr^eta, ktore przykrecono �rubkami do dalszej cz^e�ci maszyny.\n");

    add_item(({"trociny", "py^l"}), "Posadzk^e pomieszczenia pokrywaj� jasne, dorbne trociny i wsz^edobylski py^l"
            +" nieustannie wiruj�cy w powietrzu.\n");

    add_item("kominek", "Dosy^c poka�nych rozmiar^ow kominek jest ustawiony w rogu tartaku."
            +" Nie wygl�da on na nowy - powierzchnia kamienia z kt^orego zosta^l wykonany"
            +" jest ju^z mocno po�cierana i poznaczona rysami. Tak czy owak z pewno�ci�"
            +" b^edzie on s^lu^zy^c swemu w^la�cicielowi jeszcze przez d^lgui czas.\n");

    add_item("skrzynie", "Pot^e^zne skrzynie s� wype^lnione kawa^lkami drewna o najr^o^znieszych rozmiarach.\n");

    add_item("drzwi", "Dwuskrzyd^lowe drzwi zaopatrzone s� w maswyny zamek oraz dwa stalowe pr^ety s^lu^z�ce"
            +" zapewne do przek^ladania przeze^n rygla. Niestety nie dostrzegasz nigdzie w okolicy samego rygla,"
            +" by^c mo^ze w^la�ciciel skrz^etnie go gdzie� ukry^l, cho^c bardziej prawdopodobnym jest, i^z ten prosty"
            +" mechanizm to pozosta^lo�^c po poprzednim miejscu w kt^orym owe drzwi musia^ly s^lu^zy^c.\n");

    add_item("oselke", "Owalny kawa^lek szarego, chropowatego kamienia s^lu^zy do ostrzenia st^epia^lych"
            +" ostrzy topor^ow i siekier.\n");

    add_prop(ROOM_I_INSIDE,1);

    add_exit(LAS_RINDE_LOKACJE + "las23", ({"w","na zach^od opuszczaj�c tartak"}));
    add_exit(LAS_RINDE_LOKACJE + "wlas15", ({"nw","na p^o^lnocny-zach^od opuszczaj�c tartak"}));
    add_exit(LAS_RINDE_LOKACJE + "wlas14", ({"n","na p^o^lnoc opuszczaj�c tartak"}));
    add_exit(LAS_RINDE_LOKACJE + "wlas13", ({"ne","na p^o^lnocny-wsch^od opuszczaj�c tartak"}));
    add_exit(LAS_RINDE_LOKACJE + "wlas16", ({"e","na wsch^od opuszczaj�c tartak"}));
    add_exit(TRAKT_RINDE_LOKACJE + "trakt16", ({"se","na po^ludniowy-wsch^od opuszczaj�c tartak"}));
    add_exit(LAS_RINDE_LOKACJE + "wlas17", ({"s","na po^ludnie opuszczaj�c tartak"}));

    dodaj_rzecz_niewyswietlana("wysoki metalowy kosz");

    add_npc(LAS_RINDE+"npc/raghol");
    add_npc(LAS_RINDE + "npc/drwal");
    add_object(LAS_RINDE_OBIEKTY+"kosz");
}


void
dodaj_do_wrogow(string kto)
{
    if(member_array(kto,wrogowie) == -1)
        wrogowie += ({kto});
}

void
czysc_wrogow()
{
    int roz = sizeof(wrogowie);
    int ran = (roz == 1 ? 2 : random(roz));

    if(!roz)
        return;

    //A co? :) Raghol to bardzo z�o�ona osobowo�� i nie mo�na
    //tak �atwo okre�li� kiedy i kogo zapomni :P
    if(random(2))
        wrogowie = wrogowie[ran..ran];
}

int
query_wrog(string kto)
{
    if(member_array(kto,wrogowie) != -1)
        return 1;

    return 0;
}

void dodaj_do_zaplaty(string kto, int ile)
{
    zaplaty[kto] += ile;
}

int query_zaplata(string kto)
{
    return zaplaty[kto];
}

void reset_zaplaty()
{
    zaplaty = ([]);
}

void usun_zaplate(string str)
{
    zaplaty = m_delete(zaplaty, str);
}

void
set_zabral(string name, int num)
{
    zabrali[name] = num;
}

int
query_zabral(string name)
{
    return zabrali[name];
}

void decrease_zabral(string name)
{
    if(!zabrali[name])
        return;

    zabrali[name] -= 1;
}

string
dlugasny()
{
    string str;
    str = "Znajdujesz si^e w pomieszczeniu miejscowego tartaku, niemal w "+
        "ca^lo�ci zbudowanego z drewna. Jedynie fundamenty i szary kominek "+
        "w rogu wykonane s� z kamienia. Na �rodku pomieszczenia stoi spora "+
        "maszyna, z pewno�ci� stanowi ona nieocenion� pomoc dla "+
        "pracownik^ow tartaku. Woko^l niej le^zy ogromna ilo�^c jasnych, "+
        "drobnych trocin i py^lu pokrywaj�cego niemal ca^l� posadzk^e. "+
        "Pod frontow� �cian� dostrzegasz kilka du^zych, masywnych skrzy^n, "+
        "mieszcz�cych w sobie poci^ete na kawa^lki drewno, zar^owno pod "+
        "postaci� desek, jak i grubszych fragment^ow po^lokr�g^lych "+
        "klock^ow znakomicie nadaj�cych si^e na opa^l. Obok obszernych, "+
        "dwuskrzyd^lowych drzwi stoi metalowy kosz";
    if(member_array("siekiera",
        flatten(all_inventory(TO)->prase_command_id_list())) >= 0)
    {
        str += ", z kt^orego wystaj� trzonki jaki� narz^edzi";
    }
    str += ", za� nieopodal niego le^zy ma^la ose^lka, s^lu^z�ca do "+
        "ostrzenia st^epionych na twardych drzewach topor^ow. W "+
        "pomieszczeniu unosi si^e mn^ostwo dokuczliwego, drzewnego "+
        "py^lu, a twoje nozdrza nieustannie wype^lnia charakterystyczny "+
        "dla tartak^ow zapach �wie^zego drzewa.\n";
    return str;
}
