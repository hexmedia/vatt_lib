/* Autor: Avard
   Data : 11.11.06
   Opis : Tinardan */
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <pogoda.h>
#include "dir.h"

inherit LAS_RINDE_STD;

void create_las() 
{
    set_short("Las na skraju por�by");
    add_exit(LAS_RINDE_LOKACJE + "wlas6.c","nw",0,LAS_RO_FATIG,1);
    add_exit(LAS_RINDE_LOKACJE + "wlas5.c","n",0,LAS_RO_FATIG,1);
    add_exit(LAS_RINDE_LOKACJE + "wlas4.c","ne",0,TRAKT_RO_FATIG,0);
    add_exit(LAS_RINDE_LOKACJE + "wlas8.c","e",0,TRAKT_RO_FATIG,0);
    add_exit(LAS_RINDE_LOKACJE + "wlas11.c","se",0,TRAKT_RO_FATIG,0);
    add_exit(LAS_RINDE_LOKACJE + "las8.c","sw",0,TRAKT_RO_FATIG,0);
    add_prop(ROOM_I_INSIDE,0);

}

string
dlugi_opis()
{
    string str;
    if(jest_dzien() == 1)
    {
        if(pora_roku() != MT_ZIMA)
        {
            str = "Drzewa rosn� tu wysoko, cho� coraz rzadziej. Wyra�nie "+
            "wida�, �e jest to ostatni bastion lasu, ostatni posterunek. "+
            "Niemal wsz�dzie dooko�a wida� ju� prze�wity i �yse pola - "+
            "por�by, upstrzone ciemnymi plamami �ci�tych pni. ";
            /* if sa drwale
            {
                str += "Na wschodzie siekiery furkocz� a� mi�o, �piewaj�c "+
                "swoj� nieustann�, bojow� pie��. Skrzypienie umieraj�cych "+
                "drzew miesza si� ze �wiergotem uciekaj�cych ptak�w, "+
                "szelestem sun�cych w�r�d le�nego runa zwierz�t i pot�n�, :_
                "gromk� melodi� wydobywaj�c� si� z ust samych drwali.";
            } */
            // A tu jesli drwali nie ma
            str += "Drwali nigdzie teraz nie wida�, cho� zwykle zapewne "+
            "praca tutaj wre - mo�na to pozna� po charakterystycznym, "+
            "gorzkawym zapachu �ywicy i drzewnych sok�w. ";
            str += "Trawa porastaj�ca le�ne runo jest zdeptana i "+
            "pognieciona i cho� wci�� jeszcze tu kr�luje las, mo�na si� "+
            "spodziewa�, �e cz�owiek ju� nied�ugo we�mie ten teren w swoje "+
            "ca�kowite posiadanie. Jeszcze s�o�ce musi wdziera� si� tu "+
            "poprzez li�ciasty dach, jeszcze panuje tu wzgl�dny spok�j, "+
            "ale zmiany zbli�aj� si� wielkimi krokami i mo�na si� "+
            "spodziewa�, �e ju� wkr�tce pot�ne pnie drzew zwal� si� "+
            "ci�ko na ziemi�, ugn� przed pot�g� stalowych ostrzy i "+
            "znikn� st�d by� mo�e na zawsze.";
        }
        else
        {
            str = "Drzewa rosn� tu wysoko, cho� coraz rzadziej. Wyra�nie "+
            "wida�, �e jest to ostatni bastion lasu, ostatni posterunek. "+
            "Niemal wsz�dzie dooko�a wida� ju� prze�wity i �yse pola - "+
            "por�by, upstrzone ciemnymi plamami �ci�tych pni. ";
            /* if sa drwale
            {
                str += "Na wschodzie siekiery furkocz� a� mi�o, �piewaj�c "+
                "swoj� nieustann�, bojow� pie��. Skrzypienie umieraj�cych "+
                "drzew miesza si� ze �wiergotem uciekaj�cych ptak�w, "+
                "szelestem sun�cych w�r�d �niegu zwierz�t i pot�n�, :_
                "gromk� melodi� wydobywaj�c� si� z ust samych drwali.";
            } */
            // A tu jesli drwali nie ma
            str += "Drwali nigdzie teraz nie wida�, cho� zwykle zapewne "+
            "praca tutaj wre - mo�na to pozna� po charakterystycznym, "+
            "gorzkawym zapachu �ywicy i drzewnych sok�w. ";
            str += "�nieg jest szary i zadeptany i cho� wci�� jeszcze "+
            "tu kr�luje las, mo�na si� spodziewa�, �e cz�owiek ju� "+
            "nied�ugo we�mie ten teren w swoje ca�kowite posiadanie. "+
            "Jeszcze s�o�ce musi wdziera� si� tu poprzez li�ciasty dach, "+
            "jeszcze panuje tu wzgl�dny spok�j, ale zmiany zbli�aj� si� "+
            "wielkimi krokami i mo�na si� spodziewa�, �e ju� wkr�tce "+
            "pot�ne pnie drzew zwal� si� ci�ko na ziemi�, ugn� przed "+
            "pot�g� stalowych ostrzy i znikn� st�d by� mo�e na zawsze.";
        }
    }
    if(jest_dzien() == 0)
    {
        if(pora_roku() != MT_ZIMA)
        {
            str = "Drzewa rosn� tu wysoko, cho� coraz rzadziej. Wyra�nie "+
            "wida�, �e jest to ostatni bastion lasu, ostatni posterunek. "+
            "Niemal wsz�dzie dooko�a wida� ju� prze�wity i �yse pola - "+
            "por�by, upstrzone ciemnymi plamami �ci�tych pni. ";
            /* if sa drwale
            {
                str += "Na wschodzie siekiery furkocz� a� mi�o, �piewaj�c "+
                "swoj� nieustann�, bojow� pie��. Skrzypienie umieraj�cych "+
                "drzew miesza si� ze �wiergotem uciekaj�cych ptak�w, "+
                "szelestem sun�cych w�r�d le�nego runa zwierz�t i pot�n�, :_
                "gromk� melodi� wydobywaj�c� si� z ust samych drwali.";
            } */
            // A tu jesli drwali nie ma
            str += "Drwali nigdzie teraz nie wida�, cho� zwykle zapewne "+
            "praca tutaj wre - mo�na to pozna� po charakterystycznym, "+
            "gorzkawym zapachu �ywicy i drzewnych sok�w. ";
            str += "Trawa porastaj�ca le�ne runo jest zdeptana i "+
            "pognieciona i cho� wci�� jeszcze tu kr�luje las, mo�na si� "+
            "spodziewa�, �e cz�owiek ju� nied�ugo we�mie ten teren w swoje "+
            "ca�kowite posiadanie. Jeszcze ksi�yc musi wdziera� si� tu "+
            "poprzez li�ciasty dach, jeszcze panuje tu wzgl�dny spok�j, "+
            "ale zmiany zbli�aj� si� wielkimi krokami i mo�na si� "+
            "spodziewa�, �e ju� wkr�tce pot�ne pnie drzew zwal� si� "+
            "ci�ko na ziemi�, ugn� przed pot�g� stalowych ostrzy i "+
            "znikn� st�d by� mo�e na zawsze.";
        }
        else
        {
            str = "Drzewa rosn� tu wysoko, cho� coraz rzadziej. Wyra�nie "+
            "wida�, �e jest to ostatni bastion lasu, ostatni posterunek. "+
            "Niemal wsz�dzie dooko�a wida� ju� prze�wity i �yse pola - "+
            "por�by, upstrzone ciemnymi plamami �ci�tych pni. ";
            /* if sa drwale
            {
                str += "Na wschodzie siekiery furkocz� a� mi�o, �piewaj�c "+
                "swoj� nieustann�, bojow� pie��. Skrzypienie umieraj�cych "+
                "drzew miesza si� ze �wiergotem uciekaj�cych ptak�w, "+
                "szelestem sun�cych w�r�d �niegu zwierz�t i pot�n�, :_
                "gromk� melodi� wydobywaj�c� si� z ust samych drwali.";
            } */
            // A tu jesli drwali nie ma
            str += "Drwali nigdzie teraz nie wida�, cho� zwykle zapewne "+
            "praca tutaj wre - mo�na to pozna� po charakterystycznym, "+
            "gorzkawym zapachu �ywicy i drzewnych sok�w. ";
            str += "�nieg jest szary i zadeptany i cho� wci�� jeszcze "+
            "tu kr�luje las, mo�na si� spodziewa�, �e cz�owiek ju� "+
            "nied�ugo we�mie ten teren w swoje ca�kowite posiadanie. "+
            "Jeszcze ksi�yc musi wdziera� si� tu poprzez li�ciasty dach, "+
            "jeszcze panuje tu wzgl�dny spok�j, ale zmiany zbli�aj� si� "+
            "wielkimi krokami i mo�na si� spodziewa�, �e ju� wkr�tce "+
            "pot�ne pnie drzew zwal� si� ci�ko na ziemi�, ugn� przed "+
            "pot�g� stalowych ostrzy i znikn� st�d by� mo�e na zawsze.";
        }
    }
    str +="\n";
    return str;
}
string
opis_nocy()
{
    string str;
    if(pora_roku() != MT_ZIMA)
    {
        str = "Drzewa rosn� tu wysoko, cho� coraz rzadziej. Wyra�nie "+
            "wida�, �e jest to ostatni bastion lasu, ostatni posterunek. "+
            "Niemal wsz�dzie dooko�a wida� ju� prze�wity i �yse pola - "+
            "por�by, upstrzone ciemnymi plamami �ci�tych pni. ";
            /* if sa drwale
            {
                str += "Na wschodzie siekiery furkocz� a� mi�o, �piewaj�c "+
                "swoj� nieustann�, bojow� pie��. Skrzypienie umieraj�cych "+
                "drzew miesza si� ze �wiergotem uciekaj�cych ptak�w, "+
                "szelestem sun�cych w�r�d le�nego runa zwierz�t i pot�n�, :_
                "gromk� melodi� wydobywaj�c� si� z ust samych drwali.";
            } */
            // A tu jesli drwali nie ma
            str += "Drwali nigdzie teraz nie wida�, cho� zwykle zapewne "+
            "praca tutaj wre - mo�na to pozna� po charakterystycznym, "+
            "gorzkawym zapachu �ywicy i drzewnych sok�w. ";
            str += "Trawa porastaj�ca le�ne runo jest zdeptana i "+
            "pognieciona i cho� wci�� jeszcze tu kr�luje las, mo�na si� "+
            "spodziewa�, �e cz�owiek ju� nied�ugo we�mie ten teren w swoje "+
            "ca�kowite posiadanie. Jeszcze ksi�yc musi wdziera� si� tu "+
            "poprzez li�ciasty dach, jeszcze panuje tu wzgl�dny spok�j, "+
            "ale zmiany zbli�aj� si� wielkimi krokami i mo�na si� "+
            "spodziewa�, �e ju� wkr�tce pot�ne pnie drzew zwal� si� "+
            "ci�ko na ziemi�, ugn� przed pot�g� stalowych ostrzy i "+
            "znikn� st�d by� mo�e na zawsze.";
    }
    else
    {
        str = "Drzewa rosn� tu wysoko, cho� coraz rzadziej. Wyra�nie "+
            "wida�, �e jest to ostatni bastion lasu, ostatni posterunek. "+
            "Niemal wsz�dzie dooko�a wida� ju� prze�wity i �yse pola - "+
            "por�by, upstrzone ciemnymi plamami �ci�tych pni. ";
            /* if sa drwale
            {
                str += "Na wschodzie siekiery furkocz� a� mi�o, �piewaj�c "+
                "swoj� nieustann�, bojow� pie��. Skrzypienie umieraj�cych "+
                "drzew miesza si� ze �wiergotem uciekaj�cych ptak�w, "+
                "szelestem sun�cych w�r�d �niegu zwierz�t i pot�n�, :_
                "gromk� melodi� wydobywaj�c� si� z ust samych drwali.";
            } */
            // A tu jesli drwali nie ma
            str += "Drwali nigdzie teraz nie wida�, cho� zwykle zapewne "+
            "praca tutaj wre - mo�na to pozna� po charakterystycznym, "+
            "gorzkawym zapachu �ywicy i drzewnych sok�w. ";
            str += "�nieg jest szary i zadeptany i cho� wci�� jeszcze "+
            "tu kr�luje las, mo�na si� spodziewa�, �e cz�owiek ju� "+
            "nied�ugo we�mie ten teren w swoje ca�kowite posiadanie. "+
            "Jeszcze ksi�yc musi wdziera� si� tu poprzez li�ciasty dach, "+
            "jeszcze panuje tu wzgl�dny spok�j, ale zmiany zbli�aj� si� "+
            "wielkimi krokami i mo�na si� spodziewa�, �e ju� wkr�tce "+
            "pot�ne pnie drzew zwal� si� ci�ko na ziemi�, ugn� przed "+
            "pot�g� stalowych ostrzy i znikn� st�d by� mo�e na zawsze.";
    }
    str +="\n";
    return str;
}


