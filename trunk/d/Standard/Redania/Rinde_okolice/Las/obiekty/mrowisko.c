
/* Autor: Avard 
   Data : 13.12.06
   Opis : Tinardan */

inherit "/std/object.c";

#include <pl.h>
#include <stdproperties.h>
#include <macros.h>
#include <materialy.h>
#include <object_types.h>
#include <mudtime.h>
#include <language.h>

int zniszcz(string str);
int czy_zniszczone;

create_object()
{
    ustaw_nazwe("mrowisko");
    dodaj_przym("pot^e^zny","pot^e^zni");

    set_long("@@mrowisko_long@@");
    add_prop(OBJ_I_WEIGHT, 10000);
    add_prop(OBJ_I_VOLUME, 10000);
    add_prop(OBJ_I_VALUE, 0);
    add_prop(OBJ_I_NO_GET, "To ci si^e chyba nie uda.\n");
    add_prop(OBJ_M_NO_SELL, 1);
    ustaw_material(MATERIALY_DR_BUK, 20);
    ustaw_material(MATERIALY_DR_DAB, 20);
    ustaw_material(MATERIALY_DR_TOPOLA, 10);
    ustaw_material(MATERIALY_DR_WIERZBA, 10);
    ustaw_material(MATERIALY_DR_LIPA, 10);
    ustaw_material(MATERIALY_DR_KLON, 20);
    ustaw_material(MATERIALY_DR_JARZEBINA, 10);
    set_type(O_INNE);
    make_me_sitable("na","na mrowisku","z mrowiska", 1); 
    //Siedzenie na mrowisku z efektem - TODO

    czy_zniszczone = 0;
}

void
init()
{
     ::init();
     add_action(zniszcz, "zniszcz");
}
int
zniszcz(string str)
{
    object mrowisko;
    if (!str) 
    return 0;

    if(czy_zniszczone == 0)
    {
        if(parse_command(str,environment(this_object()),"%o:" + PL_BIE, mrowisko))
        {
            write("Niszczysz mrowisko!\n");
            saybb(QCIMIE(this_player(), PL_MIA) +" niszczy mrowisko!\n");
            remove_adj("pot^ezny");
            dodaj_przym("zniszczony","zniszczeni");
            odmien_short();
            odmien_plural_short();
            czy_zniszczone = 1;
            return 1;
        }
    }
    else
    {
        if(parse_command(str,environment(this_object()),"%o:" + PL_BIE, mrowisko))
        {
            write("Przeciez juz jest zniszczone.\n");
            return 1;
        }
    }
    notify_fail("Zniszcz co?\n");
    return 0;

}

string
mrowisko_long()
{
    string str;
    if(czy_zniszczone == 0)
    {
        if(environment(this_object())->pora_roku() != MT_ZIMA)
        {
            str = "Na pierwszy rzut oka jest to jedynie spory stos li^sci, "+
                "igliwia, ga^l^azek i najr^o^zniejszych le^snych ^smieci, "+
                "jakie mo^zna znale^x^c na ziemi. Dopiero po chwili "+
                "dostrzegasz, ^ze ca^la jego powierzchnia drga i pulsuje "+
                "- ^zyje w^lasnym ^zyciem. Setki, tysi^ace i setki tysi^ecy "+
                "pracowitych mr^owek codziennie rozbudowuje swoje "+
                "kr^olestwo, udoskonalaj^ac je z ka^zd^a chwil^a. \n";
        }
        else
        {
            str = "Sporych rozmiar^ow kopczyk, teraz doszcz^etnie zasypany "+
                "^sniegiem, cichy i spokojny.\n";
        }
    }
    else
    {
        str = "Sporej wielko^sci mrowisko zosta^lo kompletnie zniszczone. "+
            "Ledwie rozpoznajesz czym by^lo ono w przesz^lo^sci, a z "+
            "ka^zd^a chwil^a ro^snie ^zal uciskaj^acy twe serce, gdy "+
            "my^slisz o setkach biednych, bezbronnych mr^owek pozbawionych "+
            "domowego ogniska.\n";
    }
    return str;

}