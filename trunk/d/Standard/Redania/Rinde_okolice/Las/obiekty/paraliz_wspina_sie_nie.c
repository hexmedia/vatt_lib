#pragma strict_types

#include <exp.h>
#include <macros.h>
#include <ss_types.h>
#include "dir.h"

inherit "/std/paralyze";

static int alarm_id;

void
create_paralyze()
{
    set_name("wspinasie");
    set_stop_verb("przesta^n");
    set_stop_message("Przestajesz si^e wspina^c na ska^lk^e.\n");
    set_fail_message("Jeste� teraz zaj^et"+TP->koncowka("y","a")+
                     " wspinaniem si^e na ska^lke. Musisz wpisa^c "+
                "'przesta^n' by m^oc zrobi^c, to co chcesz.\n");
    set_finish_object(this_object());
    set_finish_fun("zatrzymanie");
    set_remove_time(3);
    setuid();
    seteuid(getuid());
}

int
zatrzymanie(string str)
{
    write("Ju^z po chwili oceniasz, ^ze nie dasz rady si^e tam wspi�^c.\n");
    saybb(QCIMIE(this_player(), PL_MIA) + " spostrzeg^lszy, ^ze ska^lka jest "+
         "jak dla nie"+this_player()->koncowka("go","j")+" zbyt du^zym "+
	 "wyzwaniem zmienia swoj� decyzj^e i przestaje si^e wspina^c.\n");
    TP->add_old_fatigue(-30);
    TP->increase_ss(SS_CLIMB,EXP_WSPINAM_NIEUDANY_CLIMB);
    TP->increase_ss(SS_STR,EXP_WSPINAM_NIEUDANY_CLIMB_STR);
    TP->increase_ss(SS_DEX,EXP_WSPINAM_NIEUDANY_CLIMB_DEX);
    TP->increase_ss(SS_CON,EXP_WSPINAM_NIEUDANY_CLIMB_CON);
    remove_object();
    return 1;
}
