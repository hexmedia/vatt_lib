
/* But bez cholewki
Wykonano dnia 12.07.06 przez Avarda. 
Opis by Tinardan z poprawkami Erii */

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <pl.h>
#include <macros.h>
#include <materialy.h>
#include <object_types.h>
void
create_armour()
{  
    ustaw_nazwe("but bez cholewki");
    dodaj_nazwy("but");

    set_long("Z niegdy^s eleganckiego, dobrze zrobionego buta pozosta^la "+
        "tylko dolna cz^e^s^c. Sk^orka dobrze wyprawiona, mimo d^lugiego "+
        "le^zakowania w niesprzyjaj^acych warunkach, l^sni czerni^a. "+
        "Podeszwa co prawda trzyma si^e ca^lkiem dobrze, ale g^orna "+
        "kraw^ed^x buta straszy oberwanymi ni^cmi i poobrywanymi klamrami. "+
        "Same klamry zapewne by^ly wykonane ze srebra b^ad^x innego "+
        "warto^sciowego materia^lu, gdy^z nigdzie w okolicy nie ma po nich "+
        "nawet najmniejszego ^sladu.\n");
		
    set_slots(A_R_FOOT);
    add_prop(OBJ_I_VOLUME, 230);
    add_prop(OBJ_I_VALUE, 1);
    add_prop(OBJ_I_WEIGHT, 230);
    add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    set_size("M");
    ustaw_material(MATERIALY_SK_SWINIA, 100);
    set_type(O_UBRANIA);
}