
/* Autor: Avard
   Data : 03.09.06
   Info : Drzwi do domu Avarda */

inherit "/std/door";

#include <pl.h>
#include <macros.h>
#include <cmdparse.h>
#include <stdproperties.h>
#include "dir.h"
int zastukaj(string str);
void
init()
{
    int i;

    ::init();
    add_action(zastukaj, "zastukaj");
}
void
create_door()
{
    ustaw_nazwe("drzwi");
    dodaj_przym("masywny","masywni");
    dodaj_przym("ciemnobr^azowy", "ciemnobr^azowi");

    set_other_room(DWOREK_LOKACJE + "salon.c");
    set_door_id("DRZWI_DO_DOMU_AVARDA");
    set_door_desc("Masywne drzwi wykonano z mocnego drewna i polakierowano "+
        "na ciemny br^az. Pod okr^ag^l^a ^zeliwn^a klamk^a znajduje sie "+
        "ma^la dziurka od klucza, na ^srodku drzwi wisi finezyjna "+
        "mosi^e^zna ko^latka w kszta^lcie smoczego pyska.\n");

    set_open_desc("");
    set_closed_desc("");
    set_open(0);
    set_locked(1);

    set_pass_command(({"drzwi","masywne drzwi","ciemnobr^azowe drzwi",
        "masywne ciemnobr^azowe drzwi","dom"}),"przez masywne drzwi",
        "z zewn^atrz");

    set_key("KLUCZ_DRZWI_DO_DOMU_AVARDA_I_FAEVE");
    set_lock_name(({"zamek", "zamka", "zamkowi", "zamek", "zamkiem", "zamku"}), 0, PL_MESKI_NOS_NZYW);
}

int
zastukaj(string str)
{
    object *drzwi;

    notify_fail("Zastukaj czym?\n");

    if (!str)
        return 0;

    if (!parse_command(str, all_inventory(environment(this_player())),
        " 'ko^latk^a' 'do' %i:" + PL_DOP, drzwi))
        return 0;

    drzwi = DIRECT_ACCESS(drzwi);

    if (!sizeof(drzwi))
        return 0;

    if (sizeof(drzwi) > 1)
    {
        notify_fail("Nie mo^zesz jednocze^snie stuka^c dwiema ko^latkami.\n");
        return 0;
    }

    if(!drzwi[0]->is_door())
        return 0;

    write("Stukasz ko^latka w " + drzwi[0]->short(PL_BIE) + ".\n");
    say(QCIMIE(this_player(),PL_MIA) + " stuka ko^latk^a w " +
        drzwi[0]->short(PL_BIE) + ".\n");
    tell_room(drzwi[0]->query_other_room(), "Kto^s stuka ko^latk^a w "+
        drzwi[0]->query_other_door()->short(PL_BIE) + ".\n");
    return 1;
}