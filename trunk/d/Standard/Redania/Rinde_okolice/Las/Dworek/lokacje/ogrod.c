
/* Autor: Avard i Faeve
  Opis : Ivrin, nieco poprawiony przez Faeve
  Data : 02.09.06 */

inherit "/std/room";

#include <stdproperties.h>
#include <macros.h>
#include <pogoda.h>
#include "dir.h"

void
create_room()
{
    set_short("Wiekowy ogr^od");
    set_long("@@dlugi_opis@@");
    add_prop(ROOM_I_INSIDE, 0);
    add_object(DWOREK_DRZWI + "drzwi_do_domu.c");
    add_object(DWOREK_DRZWI + "wrota_do_stajni.c");
    dodaj_rzecz_niewyswietlana("masywne ciemnobr^azowe drzwi",1);
    dodaj_rzecz_niewyswietlana("dwudzielne zdobione wrota",1);

    add_exit(TRAKT_RINDE_LOKACJE + "trakt28b.c","se",0,5,0);

   add_item(({"stajnie","niewielki budynek","niewielki budynek stajni",
       "budynek stajenny","niewielki budynek stajenny",
       "niewielk^a stajnie"}),"Niewielki murowany budynek do kt^orego "+
       "prowadz^a dwudzielne wrota na ozdobnych zawiasach z ciemnej stali, "+
       "zamykane na k^l^odke - mo^zna otworzy^c ich ca^lo^s^c lub tylko "+
       "g^orn^a cz^e^s^c. Przed budynkiem wbito w ziemi^e barierk^e z "+
       "drewnianych bali do przywi^azywania wierzchowc^ow.\n");
   add_item(({"dom","dworek","stary dworek","stary dom","wiekowy dom",
       "wiekowy dworek"}),"Za ogrodem, w otoczeniu kilku pot^e^znych, "+
       "roz^lo^zystych drzew wznosi si^e stary, pi^etrowy dworek z ceg^ly, "+
       "kt^orego ^sciany, mimo up^lywu lat nadal s^a wybielone. Do "+
       "wn^etrza prowadz^a masywne, ciemnolakierowane drzwi ozdobione "+
       "nieco zatart^a p^laskorze^xba z motywem smoka. Prostok^atne okna o "+
       "ramach z ciemnego drewna rozmieszczone s^a w r^ownych "+
       "odleg^lo^sciach i zaopatrzone w solidne okiennice. Jeden z "+
       "naro^znik^ow domu opleciony jest zielon^a zas^lon^a bluszczu, "+
       "kt^orego pn^acza si^egaj^a a^z do wysoko^sci pi^etra. Ca^lo^s^c "+
       "wie^nczy dach pokryty dobrze utrzymanym, czernionym smo^l^a "+
       "gontem. Wok^o^l domu wysypano bia^ly piasek i drobny ^zwir, "+
       "dzi^eki czemu obej^scie jest czyste i schludne. \n");
    add_item(({"drzewo","wierzb^e"}),
        "Od pot^e^znego pnia odchodz^a trzy do^s^c grube konary. Kazdy z "+
        "nich stopniowo dzieli si^e na coraz to mniejsze ga^l^azki, z "+
        "kt^orych zwisaj^a d^lugie, gi^etkie, zielono^z^o^lte witki, "+
        "wzd^lu^z kt^orych rosn^a parzy^scie w^askie, pod^lu^zne, lekko "+
        "pi^lkowane na brzegach li^scie. Kora drzewa jest szaro-br^azowa, "+
        "poznaczona bruzdami. Od p^o^lnocnej strony pokry^l j^a mech. Ciekawa "+
        "rzecz, ^ze jeden z konar^ow znajduj^acy si^e stosunkowo niewysoko, "+
        "ro^snie prawie r^ownolegle do ziemi, przypominaj^ac swego rodzaju "+
        "^laweczk^e. Poszukuj^acy ustronnego miejsca mog^a schroni^c si^e "+
        "tutaj, w zaciszu drzewa, mi^edzy g^estym, soczy^scie zielonym "+
        "listowiem wierzby p^lacz^acej.\n");
    add_item(({"drzewka","drzewka owocowe"}),"@@drzewka@@");
    
   add_sit("pod drzewem","w cieniu drzewa, wygodnie opieraj^ac si^e o jego "+
       "pie^n","spod drzewa",4);
   add_sit("na trawie","na trawie","z trawy",0);
   add_sit("pod ^scian^a","pod ^scian^a","spod ^sciany",0);
}
public string
exits_description()
{
   return "";
}

string
dlugi_opis()
{
    string str;
    if(jest_dzien() == 1)
    {
        if(pora_roku() != MT_ZIMA)
        {
            str = "Honorowe miejsce, w samym centrum ogrodu, zajmuje stara, "+
                "roz^lo^zysta wierzba o d^lugich, zwisaj^acych a^z do ziemi "+
                "witkach, poruszanych lekkimi podmuchami wiatru. Jej "+
                "pot^e^zny pie^n, o sp^ekanej korze od p^o^lnocnej "+
                "strony por^os^l zielonoszary mech. Doko^la rosn^a to tu, to "+
                "tam wybuja^le, zdzicza^le ju^z drzewka owocowe, kt^orych "+
                "okres ^swietno^sci obradzania dawno ju^z przemin^a^l. Pod "+
                "nimi rozes^lany jest zielony dywan trawy i koniczyny, "+
                "poprzetykany gdzieniegdzie jej kwiatami, jasnor^o^zowymi "+
                "stokrotkami i powojem wspinaj^acym si^e na pnie drzew. "+
                "Przez ^srodek ogrodu wiedzie ^scie^zka, kt^ora prowadzi wprost na "+
                "niewysokie, kamienne schodki, u szczytu kt^orych znajduj^a "+
                "si^e masywne drzwi domu na wschodzie, natomiast na "+
                "p^o^lnocy mo^zna dostrzec niedu^zy, drewniany budynek ju^z "+
                "na pierwszy rzut oka wygl^adaj^acy na wcale zadban^a "+
                "stajni^e.";
        }
        else
        {
            str = "Honorowe miejsce, w samym centrum ogrodu, zajmuje stara, "+
"roz^lo^zysta wierzba o d^lugich, zwisaj^acych a^z do ziemi witkach, poruszanych "+
"lekkimi podmuchami wiatru. Jej pot^e^zny pie^n, o sp^ekanej korze, teraz "+
"pokryty jest p^laszczem ^sniegu, spod kt^orego miejscami wygl^ada szarozielony "+
"mech. Doko^la rosn^a to tu, to tam wybuja^le, zdzicza^le ju^z drzewka owocowe, "+
"kt^orych ga^l^ezie uginaj^a si^e pod ci^e^zarem ^sniegu. Pod nimi rozci^aga si^e "+
"^snie^znobia^ly dywan skrz^acego si^e jak brylanciki puchu, poprzetykany "+
"gdzieniegdzie ^sladami ptak^ow odwiedzaj^acych ogr^od w poszukiwaniu resztek "+
"owoc^ow. Przez ^srodek ogrodu wiedzie zasypana ^sniegiem ^scie^zka, kt^ora "+
"prowadzi wprost na niewysokie, kamienne schodki, u szczytu kt^orych znajduj^a "+
"si^e masywne drzwi domu na wschodzie, natomiast na p^o^lnocy mo^zna dostrzec "+
"niedu^zy, drewniany budynek ju^z na pierwszy rzut oka wygl^adaj^acy na wcale "+
"zadban^a stajni^e.";
        }
    }
    if(jest_dzien() == 0)
    {
        if(pora_roku() != MT_ZIMA)
        {
        str = "Honorowe miejsce, w samym centrum ogrodu, zajmuje stara, "+
"roz^lo^zysta wierzba o d^lugich, zwisaj^acych a^z do ziemi witkach. Jej pot^e^zny "+
"pie^n, o sp^ekanej korze teraz pokryty jest p^laszczem ^sniegu. Doko^la rosn^a to "+
"tu, to tam wybuja^le, zdzicza^le ju^z drzewka owocowe, kt^orych okres "+
"^swietno^sci obradzania dawno ju^z przemin^a^l. Pod nimi rozes^lany jest dywan "+
"trawy i koniczyny, przez kt^orego ^srodek wiedzie wysypana drobnym ^zwirem "+
"^scie^zka, kt^ora prowadzi wprost do domu na wschodzie, natomiast na p^o^lnocy "+
"mo^zna dostrzec cie^n jakiego^s innego budynku.";
        }
       else
       {
       str = "Honorowe miejsce, w samym centrum ogrodu, zajmuje stara, "+
"roz^lo^zysta wierzba o d^lugich, zwisaj^acych a^z do ziemi witkach. Jej pot^e^zny "+
"pie^n, o sp^ekanej korze od p^o^lnocnej strony por^os^l zielonoszary mech. Doko^la "+
"rosn^a to tu, to tam wybuja^le, zdzicza^le ju^z drzewka owocowe, kt^orych "+
"ga^l^ezie uginaj^a si^e pod ci^e^zarem ^sniegu. Pod nimi rozci^aga si^e ^snie^znobia^ly "+
"dywan skrz^acego si^e jak brylanciki puchu, przez kt^orego ^srodek wiedzie "+
"zasypana ^sniegiem ^scie^zka, prowadz^aca wprost do domu na wschodzie, "+
"natomiast na p^o^lnocy mo^zna dostrzec cie^n jakiego^s innego budynku.";
       }
    }
    str += "\n";
    return str;
}

string
opis_nocy()
{
   string str;
    if(pora_roku() != MT_ZIMA)
    {
        str = "Honorowe miejsce, w samym centrum ogrodu, zajmuje stara, "+
"roz^lo^zysta wierzba o d^lugich, zwisaj^acych a^z do ziemi witkach. Jej pot^e^zny "+
"pie^n, o sp^ekanej korze teraz pokryty jest p^laszczem ^sniegu. Doko^la rosn^a to "+
"tu, to tam wybuja^le, zdzicza^le ju^z drzewka owocowe, kt^orych okres "+
"^swietno^sci obradzania dawno ju^z przemin^a^l. Pod nimi rozes^lany jest dywan "+
"trawy i koniczyny, przez kt^orego ^srodek wiedzie wysypana drobnym ^zwirem "+
"^scie^zka, kt^ora prowadzi wprost do domu na wschodzie, natomiast na p^o^lnocy "+
"mo^zna dostrzec cie^n jakiego^s innego budynku.";
    }
    else
    {
        str = "Honorowe miejsce, w samym centrum ogrodu, zajmuje stara, "+
"roz^lo^zysta wierzba o d^lugich, zwisaj^acych a^z do ziemi witkach. Jej pot^e^zny "+
"pie^n, o sp^ekanej korze od p^o^lnocnej strony por^os^l zielonoszary mech. Doko^la "+
"rosn^a to tu, to tam wybuja^le, zdzicza^le ju^z drzewka owocowe, kt^orych "+
"ga^l^ezie uginaj^a si^e pod ci^e^zarem ^sniegu. Pod nimi rozci^aga si^e ^snie^znobia^ly "+
"dywan skrz^acego si^e jak brylanciki puchu, przez kt^orego ^srodek wiedzie "+
"zasypana ^sniegiem ^scie^zka, prowadz^aca wprost do domu na wschodzie, "+
"natomiast na p^o^lnocy mo^zna dostrzec cie^n jakiego^s innego budynku.";
    }
    str += "\n";
    return str;
}
string
drzewka()
{
    string str;
    str = "Niedu^ze, zdzicza^le ju^z drzewka owocowe - grusze, jab^lonki, "+
        "wi^snie. ";
    if(pora_roku() != MT_ZIMA)
    {
        str += "Mi^edzy soczy^scie zielonymi, dorbnymi listkami mo^zna "+
            "dojrze^c niedu^ze owocki.";
    }
    else
    {
        str += "Drobne ga^l^ezie uginaj^a si^e pod ci^e^zarem ^sniegu. ";
    }
    str += "Pod ka^zdym z drzewek daj^a si^e zauwa^zy^c resztki "+
       "owoc^ow, kt^ore - nie zjedzone - opad^ly jesieni^a na ziemi^e.\n";
    return str;
}
