
/* Autor: Avard
   Opis : Ivrin
   Data : 02.09.06 */

inherit "/std/room";

#include <stdproperties.h>
#include <macros.h>
#include "dir.h"

void
create_room() 
{
    set_short("Salon");
    set_long("@@dlugi_opis@@");
    add_prop(ROOM_I_INSIDE, 1);
    add_object(DWOREK_OBIEKTY + "fotel_salon.c",2);
    add_object(DWOREK_OBIEKTY + "kominek.c");
    add_object(DWOREK_OBIEKTY + "dywan.c");
    add_object(DWOREK_OBIEKTY + "kanapa.c");
    add_object(DWOREK_DRZWI + "drzwi_z_domu.c");
    add_object(DWOREK_DRZWI + "drzwi_do_avarda.c");
    add_object(DWOREK_DRZWI + "drzwi_do_faeve.c");
    dodaj_rzecz_niewyswietlana("stare machoniowe drzwi",1);
    dodaj_rzecz_niewyswietlana("mi^ekki du^zy dywan",1);
    dodaj_rzecz_niewyswietlana("murowany kominek",1);
    dodaj_rzecz_niewyswietlana("du^zy sk^orzany fotel",2);
    dodaj_rzecz_niewyswietlana("d^luga sk^orzana kanapa",1);
    dodaj_rzecz_niewyswietlana("masywne ciemnobr^azowe drzwi",1);

    add_prop(ROOM_I_INSIDE,1);
}
public string
exits_description() 
{
    return "Znajduje si� tutaj wyj�cie na zewn�trz, drzwi na wschodniej "+
    "�cianie oraz drzwi na zachodniej ��ianie.\n";
}

string
dlugi_opis()
{
    int i, il, il_foteli;
    object *inwentarz;
   
    inwentarz = all_inventory();
    il_foteli = 0;
    il = sizeof(inwentarz);
   
    for (i = 0; i < il; ++i) {
         if (inwentarz[i]->jestem_fotelem_salonu_avarda()) {
                        ++il_foteli; }}
    string str;
    str = "W pokoju tym gospodarze zapewne przyjmuj^a go^sci. Pierwsze, co "+
        "rzuca si^e w oczy to zajmuj^acy tu najwa^zniejsze miejsce murowany "+
        "kominek";
    if(jest_rzecz_w_sublokacji(0,"brunatna nied^xwiedzia sk^ora"))
    {
        str +=", przed kt^orym le^zy brunatna nied^xwiedzia sk^ora. ";
    }
    else
    {
        str +=". ";
    }
    if(jest_rzecz_w_sublokacji(0,"niewielki rze^xbiony stolik"))
    {
        str +="W zasi^egu ciep^la bij^acego z paleniska ustawiono "+
            "niewielki stolik na rze^xbionych lwich nogach";
        if(il_foteli == 0)
        {
            str += "";
            if(jest_rzecz_w_sublokacji(0,"d^luga sk^orzana kanapa"))
            {
                str +=", przy kt^orym stoi kanapa. ";
            }
            else
            {
                str +=". ";
            }
        }
        if(il_foteli == 1)
        {
            str += ", przy kt^orym stoi elegancki fotel";
            if(jest_rzecz_w_sublokacji(0,"d^luga sk^orzana kanapa"))
            {
                str +=", oraz kanapa. ";
            }
            else
            {
                str +=".";
            }
        }
        if(il_foteli >= 2)
        {
            str += " otoczony dwoma fotelami";
            if(jest_rzecz_w_sublokacji(0,"d^luga sk^orzana kanapa"))
            {
                str +=", oraz kanapa. ";
            }
            else
            {
                str +=".";
            }
        }
    }
    else
    {
        if(il_foteli == 0)
        {
            if(jest_rzecz_w_sublokacji(0,"d^luga sk^orzana kanapa"))
            {
                str +="W zasi^egu ciep^la bij^acego z paleniska ustawiono "+
                    "eleganck^a kanap^e. ";
            }
        }
        if(il_foteli == 1)
        {
            if(jest_rzecz_w_sublokacji(0,"d^luga sk^orzana kanapa"))
            {
                str +="W zasi^egu ciep^la bij^acego z paleniska ustawiono "+
                    "eleganck^a kanap^e i fotel. ";
            }
            else
            {
                str +="W zasi^egu ciep^la bij^acego z paleniska ustawiono "+
                    "elegancki fotel. ";
            }
        }
        if(il_foteli >= 2)
        {
            if(jest_rzecz_w_sublokacji(0,"d^luga sk^orzana kanapa"))
            {
                str +="W zasi^egu ciep^la bij^acego z paleniska ustawiono "+
                    "eleganck^a kanap^e i dwa fotele. ";
            }
            else
            {
                str +="W zasi^egu ciep^la bij^acego z paleniska ustawiono "+
                    "dwa eleganckie fotele. ";
            }
        }
    }
    if(jest_rzecz_w_sublokacji(0,"mi^ekki du^zy dywan"))
    {
        str += "Spogl^adaj^ac pod nogi mo^zesz podziwia^c mi^ekki dywan "+
            "o niezwykle ciekawym wzorze - przedstawia scen^e z "+
            "polowania, konnych my^sliwych z ^lukami i umykaj^aca "+
            "zwierzyn^e. ";
    }
    str += "W oknach zawieszono grube ciemne zas^lony. \n"; 
    return str;
}
