
/* Autor: Avard
   Opis : Ivrin
   Data : 02.09.06*/

inherit "/std/object.c";

#include <pl.h>
#include <stdproperties.h>
#include <macros.h>
#include <filter_funs.h>
#include <materialy.h>
#include <object_types.h>

create_object()
{
    ustaw_nazwe("biurko");
    dodaj_przym("stary","starzy");
    dodaj_przym("ci^ezki","ci^ezcy");

    set_long("Masywne biurko musi by^c wyj^atkowo stare - ^z^lobiony kant "+
        "blatu jest w wielu miejscach wyszczerbiony, a szuflady wysuwaj^a "+
        "si^e z przeci^ag^lym skrzypieniem. Na biurku le^z^a jakie^s "+
        "papiery, w niewielkim ka^lamarzu tkwi g^esie pi^oro, obok stoi "+
        "za^sniedzia^ly lichtarz ze swiecami. Za biurkiem znajduje sie "+
        "jasny fotel.\n");
    make_me_sitable("za","za biurkiem","zza biurka",1, 0, PL_NAR);
    add_prop(OBJ_I_WEIGHT, 4000);
    add_prop(OBJ_I_VOLUME, 4000);
    add_prop(OBJ_I_VALUE, 260);
    add_prop(OBJ_M_NO_SELL, 1);
    ustaw_material(MATERIALY_DR_DAB);
    set_type(O_MEBLE);
}

