
/* Tabliczka, made by Avard 28.05.06 */
inherit "/std/object.c";

#include <pl.h>
#include <stdproperties.h>
#include <macros.h>
#include <filter_funs.h>
#include <materialy.h>
#include <object_types.h>

create_object()
{
    ustaw_nazwe("obraz");
    dodaj_przym("du^zy","duzi");

    set_long("Obraz przedstawia pejza^z g^or widzianych z wysokiego "+
        "szczytu - ich skaliste masywy gin^a perspektywicznie w "+
        "niebieskiej po^swiacie schy^lku dnia, a u ich le^snych "+
        "podn^o^zy snuj^a si^e strz^epki mlecznej mg^ly.\n");
    
    add_prop(OBJ_I_WEIGHT, 500);
    add_prop(OBJ_I_VOLUME, 1000);
    add_prop(OBJ_I_VALUE, 500);
    add_prop(OBJ_M_NO_SELL, 1);
    ustaw_material(MATERIALY_DR_BUK);
    set_type(O_SCIENNE);
}
