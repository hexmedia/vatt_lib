/* Autor: Avard
   Opis : Ivrin
   Data : 02.09.06 */

inherit "/std/object.c";

#include <pl.h>
#include <stdproperties.h>
#include <macros.h>
#include <filter_funs.h>
#include <materialy.h>
#include <object_types.h>

create_object()
{
    ustaw_nazwe("dywan");
                 
    dodaj_przym("mi^ekki","mi^ekcy");
    dodaj_przym("du^zy","duzi");
   
    set_long("Dywan utkany jest z mi^ekkiej we^lny, jego wzor przedstawia "+
        "scen^e z polowania - wida^c tu ciemny las, pi^ekne drzewa w^sr^od "+
        "kt^orych ukrywaj^a si^e sarny i jelenie, oraz czujnych my^sliwych "+
        "gotowych w ka^zdej chwili pu^sci^c strza^ly z napi^etych ^lukow.\n");
    make_me_sitable("na","na dywanie","z dywanu",6);
    add_prop(OBJ_I_WEIGHT, 10000);
    add_prop(OBJ_I_VOLUME, 10000);
    add_prop(OBJ_I_VALUE, 1240);
    ustaw_material(MATERIALY_WELNA, 100);
    set_type(O_INNE);
}
