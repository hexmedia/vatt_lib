/* Autor: Avard
   Opis : Ivrin
   Data : 02.09.06 */

inherit "/std/object.c";

#include <pl.h>
#include <stdproperties.h>
#include <macros.h>
#include <filter_funs.h>
#include <materialy.h>
#include <object_types.h>

create_object()
{
    ustaw_nazwe("�o�e");
                 
    dodaj_przym("du^zy","duzi");
    dodaj_przym("elegancki","eleganccy");
   
    make_me_sitable("na","na ^l^o^zu","z ^lo^za",4);

    set_long("Wielkie �o�e wykonano z jasnego d�bowego drewna. Pos�anie "+
        "wy�cielone jest baranimi sk�rami, na kt�rych z�o�ono lnian� "+
        "po�ciel. Zwie�czenie stanowi zdobiony srebrn� nici� baldachim "+
        "oparty na smuk�ych kolumnach.\n");
    add_prop(OBJ_I_WEIGHT, 10000);
    add_prop(OBJ_I_VOLUME, 10000);
    add_prop(OBJ_I_VALUE, 1240);
    ustaw_material(MATERIALY_DR_BUK, 90);
    ustaw_material(MATERIALY_LEN, 10);
    set_type(O_MEBLE);
}
