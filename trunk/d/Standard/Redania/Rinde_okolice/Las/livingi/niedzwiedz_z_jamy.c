/*
 * Jest to uroczy mi� siedz�cy sobie w jamie w lesie okolic Rinde. ;)
 * Nie wychodzi stamt�d, �pi na zim^e, a jama ta to chyba nasze pierwsze
 * expowisko!
 *
 * Misia napisa^l Rantaur, popraweczki Very.
 */

inherit "/std/zwierze";
inherit "/std/act/action";

#include <macros.h>
#include <mudtime.h>
#include <ss_types.h>
#include <filter_funs.h>
#include <object_types.h>
#include <stdproperties.h>

int spi=1;

void create_zwierze()
{
    ustaw_odmiane_rasy("niedzwiedz");
    set_gender(G_MALE);

    dodaj_przym("wielki", "wielcy");
    dodaj_przym("br�zowy", "br�zowi");

    set_long("@@dlugi@@");

    set_act_time(30);
    add_act("@@eventy@@");

    set_cact_time(25);
    add_cact("emote ryczy przera�liwie.");
    add_cact("emote na kr^otk� chwil^e staje na dw^och ^lapach prezentuj�c swoj� wielko�^c.");
    add_cact("emote charczy nerwowo.");
    add_cact("emote miota si^e w�ciekle.");
    add_cact("emote wycofuje si^e o kilka krok^ow.");

    add_prop(CONT_I_WEIGHT, 600000);
    add_prop(CONT_I_VOLUME, 450000);
    add_prop(NPC_I_NO_RUN_AWAY,1);
    add_prop(LIVE_I_SEE_DARK,1);

    set_aggressive("@@czy_atakujemy@@");

    set_stats(({64+random(15), 30+random(10), 80+random(20), 25, 82+random(7)}));
    set_skill(SS_DEFENCE, 25+random(5));
    set_skill(SS_UNARM_COMBAT, 50+random(15));
    set_skill(SS_BLIND_COMBAT, 59+random(10));
    set_skill(SS_AWARENESS, 44+random(4));

    set_attack_unarmed(0, 15, 15, W_SLASH,  40, "^lapa", "lewy",  "pazury");
    set_attack_unarmed(1, 15, 15, W_SLASH,  40, "^lapa", "prawy", "pazury");
    set_attack_unarmed(2, 15, 15, W_IMPALE, 20, "k^ly");

    set_hitloc_unarmed(0, ({ 1, 1, 1 }), 10, "^leb");
    set_hitloc_unarmed(1, ({ 1, 1, 1 }), 30, "tu^l^ow");
    set_hitloc_unarmed(2, ({ 1, 1, 1 }), 20, "^lapa", ({"lewy", "przedni"}));
    set_hitloc_unarmed(3, ({ 1, 1, 1 }), 20, "^lapa", ({"prawy", "przedni"}));
    set_hitloc_unarmed(4, ({ 1, 1, 1 }), 10, "^lapa", ({"tylny", "lewy"}));
    set_hitloc_unarmed(5, ({ 1, 1, 1 }), 10, "^lapa", ({"tylny", "przedni"}));

    set_alarm(1.0, 0.0, "czy_isc_spac");
    set_alarm_every_hour("czy_isc_spac");

    add_leftover("/std/leftover", "kie^l", 4, 0, 1, 1, 0, O_KOSCI);
    add_leftover("/std/leftover", "czaszka", 1, 0, 1, 1, 0, O_KOSCI);
    add_leftover("/std/leftover", "serce", 1, 0, 0, 0, 0, O_JEDZENIE);
    add_leftover("/std/skora", "sk^ora", 1, 0, 1, 1, 25 + random(8), O_SKORY);
}

string 
dlugi()
{
    if(spi)
        return "Masywne cielsko porusza si^e miarowo"
                +" w rytm oddechu zwierz^ecia. �pi�cy"
                +" nied�wied� przypomina, z pozoru niegro�n�,"
                +" kup^e furta, jednak wolisz nie my�le^c jak"
                +" zachowa^lby si^e gdyby kto� raczy^l przeszkodzi^c"
                +" mu w wypoczynku. Jego kr^otkie, p^o^lokr�g^le uszy"
                +" od czasu do czasu poruszaj� si^e zupe^lnie jakby"
                +" nas^luchiwa^ly przybycia owego wichrzyciela.\n";
    else
        return "L�ni�ce, br�zowe futro mieni si^e refleksami przy"
                +" ka^zdym ruchu zwierz^ecia. Mimo, i^z sprawiaj� one"
                +" wra^zenie powolnych i leniwych to z pewno�ci�"
                +" nied�wied� jest te^z zdolny do sprawnego ataku na"
                +" upatrzon� ofiar^e, b�dz co b�dz wielu ju^z zgin^e^lo"
                +" rozszarpanych w^la�nie nied�wiedzimi pazurami."
                +" Jego czarne oczy wodz� niespokojnie po okolicy, a"
                +" z masywnego pyska od czasu do czasu wydobywa si^e"
                +" cichy pomruk.\n";
}

string eventy()
{
    string *spi = ({"emote przewraca si^e na drugi bok.",
                    "emote pomrukuje cicho.",
                    "emote przekr^eca lekko ^leb i wydaje z siebie"
                    +" seri^e rytmicznych chrapni^e^c.",
                    "emote nieznacznie poruszy^l uszami."});

    string *niespi = ({"emote leniwie grzebie ^lap� w ziemi.",
                    "emote mruczy niespokojnie.",
                    "emote zatrzymuje nagle wzrok w sobie tylko znanym punkcie.",
                    "emote kilkakrotnie ociera sw^oj pysk ^lap�."});

    if(spi)
    {
        int index = random(sizeof(spi));
        return spi[index];
    }
    else
    {
        int index = random(sizeof(niespi));
        return niespi[index];
    }
}

int czy_atakujemy()
{
   return !spi;
}

void set_spi(int i)
{
    spi = i;
}

void zaatakuj(object kogo)
{
    kogo->catch_msg(capitalize(short(kogo))+" nagle rzuca si^e na ciebie!\n");
    saybb(capitalize(short(kogo))+" nagle rzuca si^e na "+QIMIE(kogo, PL_BIE)+"!\n",
            kogo);
    attack_object(kogo);
}

void wstan()
{
    if(!spi)
        return;

    spi = 0;
    command("emote wydaje z siebie chrapliwy ryk, po czym leniwie staje na ^lapach.");

    if(query_attack() != 0)
        return;

    object *livingi = FILTER_LIVE(all_inventory(ENV(TO)));
    livingi -= ({TO});
    livingi = FILTER_CAN_SEE(livingi, TO);

    int s;
    if((s = sizeof(livingi)) > 0)
    {
        int i = random(s);
        set_alarm(2.0, 0.0, "zaatakuj", livingi[i]);
    }
}

void idz_spac()
{
    if(!spi)
        return;

    spi = 1;
    command("emote kr^eci si^e przez chwil^e po okolicy, po czym uk^lada si^e do snu.");
}

void czy_isc_spac()
{
    if( (environment()->pora_roku() == MT_ZIMA) || (environment()->dzien_noc()) && (query_attack() == 0) )
        idz_spac();
    else
        wstan();
}

void attacked_by(object wrog)
{
    wstan();
    ::attacked_by(wrog);
}

void
emote_hook(string emote, object wykonujacy, string przyslowek)
{
    switch (emote)
    {
        case "kopnij":
        case "spoliczkuj":
        case "opluj":
        case "nadepnij":
        case "poca^luj":
        case "przytul":
        case "pog^laszcz":
        case "szturchnij":
            set_alarm(1.0, 0.0, "zly", wykonujacy);
            break;
    }
}

void zly(object kto)
{
    if(kto->query_skill(SS_ANI_HANDL) > 25+random(11))
        command(eventy());
    else
        wstan();
}
