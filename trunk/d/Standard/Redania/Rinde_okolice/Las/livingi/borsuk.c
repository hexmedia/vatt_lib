/* Autor: Avard
   Opis : Faeve
   Data : 11.03.07 */

inherit "/std/zwierze";
inherit "/std/act/action";
inherit "/std/act/trigaction.c";

#include <macros.h>
#include <stdproperties.h>
#include <pl.h>
#include <formulas.h>
#include <std.h>
#include <composite.h>
#include <filter_funs.h>
#include <cmdparse.h>
#include <mudtime.h>
#include <wa_types.h>
#include <ss_types.h>
#include <object_types.h>
#include "dir.h"

void
create_zwierze()
{
    ustaw_odmiane_rasy("borsuk");
    set_gender(G_MALE);

    set_long("Zwierz^e to ma do^s^c charakterystyczn^a kr^ep^a budow^e oraz "+
        "r^ownie charakterystyczne ubrawienie - wierzch cia^la jest szary, "+
        "a po bokach bia^lej g^lowy, rozpoczynaj^ac od nosa, biegn^a "+
        "szerokie pasy czarnej sier^sci. Jego ko^nczyny zaopatrzone sa w "+
        "ostre pazury, kt^ore zdaj^a si^e by^c tak mocne, ^ze przy ich "+
        "pomocy mo^zna by rozora^c nielich^a po^la^c ziemi. Jednak "+
        "sympatyczny wyraz jego pyszczka i m^adre oczy zdradzaj^a, ^ze "+
        "borsuk nie jest zwierz^eciem drapie^znym.\n");

    random_przym("okr^aglutki:okr^aglutcy wychudzony:wychudzeni "+
        "drobny:drobni grubiutki:grubiutcy||szary:szarzy "+
        "wylinia^ly:wyliniali||m^lody:m^lodzi stary:starzy||"+
        "ruchliwy:ruchliwi spokojny:spokojni",2);

    set_act_time(30);
    add_act("emote w^eszy w poszukiwaniu po^zywienia.");
    add_act("emote grzebie ^lap� w ziemi.");
    add_act("emote unosi ryjek i czego� wypatruje.");
    add_act("emote podskakuje gwa^ltownie.");

    set_stats (({10, 10, 10, 20, 15}));

    add_prop(LIVE_I_NEVERKNOWN, 1);
    add_prop(CONT_I_WEIGHT, 15000 + random(4000));
    add_prop(CONT_I_HEIGHT, 30);

    set_attack_unarmed(0, 15, 15, W_SLASH,  50, "^lapa", "lewy", "pazury");
    set_attack_unarmed(1, 15, 15, W_SLASH,  50, "^lapa", "lewy", "pazury");

    set_hitloc_unarmed(0, ({ 1, 1, 1 }), 10, "g^lowa");
    set_hitloc_unarmed(1, ({ 1, 1, 1 }), 30, "tu^l^ow");
    set_hitloc_unarmed(2, ({ 1, 1, 1 }), 20, "^lapa", ({"lewy", "przedni"}));
    set_hitloc_unarmed(3, ({ 1, 1, 1 }), 20, "^lapa", ({"prawy", "przedni"}));
    set_hitloc_unarmed(4, ({ 1, 1, 1 }), 10, "^lapa", ({"tylny", "lewy"}));
    set_hitloc_unarmed(5, ({ 1, 1, 1 }), 10, "^lapa", ({"tylny", "przedni"}));

    set_random_move(100);
    set_restrain_path(LAS_RINDE_LOKACJE);

    add_leftover("/std/skora", "sk^ora", 1, 0, 1, 1, 7 + random(5), O_SKORY);
    add_leftover("/std/leftover", "z^ab", 1, 0, 1, 1, 0, O_KOSCI);
}
void
emote_hook(string emote, object wykonujacy, string przyslowek)
{
    switch (emote)
    {
        case "kopnij":
        case "spoliczkuj":
        case "opluj":
        case "nadepnij":
        case "poca^luj":
        case "przytul":
        case "pog^laszcz":
        case "szturchnij":
            set_alarm(1.0, 0.0, "zly", wykonujacy);
            break;
    }
}

void
zly(object kto)
{
   TO->run_away();
}

void
attacked_by(object wrog)
{
    ::attacked_by(wrog);
}
