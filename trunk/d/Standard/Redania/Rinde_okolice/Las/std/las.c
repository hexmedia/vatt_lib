/* Std do lokacji lasu w poblizu Rinde
   Made by Avard, 21.06.06, ku chwale Swiatowida! */

#include "dir.h"

inherit REDANIA_STD;

#include <macros.h>
#include <mudtime.h>
#include <language.h>
#include <stdproperties.h>
#include <filter_funs.h>

#define TARTAK "/d/Standard/Redania/Rinde_okolice/Las/lokacje/tartak.c"

int zwolaj(string str);
void raghol_quest();

string *zwolali; // Ludzie oczekujacy na transport

void
create_las()
{
}

nomask void
create_redania()
{
    set_long("@@dlugi_opis@@");
    set_polmrok_long("@@opis_nocy@@");


	//NO I CO TO KURNA MA BY�? Vera warczy i jest z�y.
    //add_item(({"drzewa","las"}), "Tak, tutaj powinien byc jakis opis :P\n");

    add_event("@@event_lasu:"+file_name(TO)+"@@");

    set_event_time(300.0);

    add_sit(({"na trawie","na ziemi"}),"na trawie","z trawy",0);
    add_sit("pod drzewem","pod jednym z drzew","spod drzewa",0);
    add_prop(ROOM_I_TYPE, ROOM_FOREST);

    add_subloc(({"drzewo", "drzewa", "drzewu", "drzewo",
        "drzewem", "drzewie"}));
    add_subloc_prop("drzewo", SUBLOC_I_MOZNA_POLOZ, 1);
    add_subloc_prop("drzewo", SUBLOC_I_MOZNA_ODLOZ, 1);
    add_subloc_prop("drzewo", SUBLOC_I_TYP_POD, 1);

    ustaw_liste_drzew(({"dab", "klon", "brzoza"}));

    dodaj_ziolo(({"czarny_bez-lisc.c","czarny_bez-kwiaty.c",
    "czarny_bez-owoce.c"}));
    dodaj_ziolo("arcydziegiel.c");
    dodaj_ziolo("babka_lancetowata.c");
    dodaj_ziolo("piolun.c");
    add_prop(ROOM_I_WSP_Y, WSP_Y_RINDE_BM_LAS); //Wsp�rz�dne, do systemu pogodowego.
    add_prop(ROOM_I_WSP_X, WSP_X_RINDE_BM_LAS);
    create_las();
}

init()
{
    ::init();
    add_action(&zwolaj(), "zwo^laj");
}
public int
unq_no_move(string str)
{
    notify_fail("Niestety, g^este zaro^sla nie pozwalaj^a ci "+
        "p^oj^s^c w tym kierunku.\n");

    string vrb = query_verb();

    if(vrb ~= "g�ra" || vrb ~= "d�")
        notify_fail("Nie mo�esz tam si� uda�!\n");

    return 0;
}

string
event_lasu()
{
    switch (pora_dnia())
    {
        case MT_RANEK:
        case MT_POLUDNIE:
        case MT_POPOLUDNIE:
            return process_string(({"Promyki ^swiat^la nie^smia^lo "+
            "przedzieraj^a si^e przez ga^l^ezie.\n",
            "@@pora_roku_dzien@@"})[random(2)]);
        default: // wieczor, noc
            return process_string(({"K^atem oka dostrzegasz jaki^s ruch, ale "+
            "gdy odwracasz g^low^e las jest cichy i pusty.\n",
            "Wrona z zapa^lem wyskrzekuje swoj^a pie^s^n.\n",
            "Masz wra^zenie, ^ze obserwuje ci^e jakie^s zwierz^e.\n",
            "W oddali s^lyszysz ryk dzikiego zwierz^ecia.\n",
            "@@pora_roku_noc@@"})[random(5)]);
    }
}
string
pora_roku_dzien()
{
    switch (pora_roku())
    {
         case MT_LATO:
         case MT_WIOSNA:
             return ({
                "Li^scie drzew delikatnie poruszaj^a si^e na wietrze.\n",
                "Gdzie^s wysoko w koronach drzew ptaki weso^lo ^cwierkaj^a.\n",
                "Tu^z nad tob^a dwie wiewi^orki ^scigaj^a si^e po drzewach.\n",
                "Z g^estwiny nagle zrywa si^e ptak i z gwa^ltownym krzykiem odlatuje w przestworza.\n",
                "Trawa ko^lysze si^e pod mu^sni^eciem wiatru.\n",
                "Ptasi trel wznosi si^e w weso^lym crescendo, po czym znowu cichnie i uspokaja si^e.\n",
                "Z g^l^ebi lasu dobiega ci^e melodyjny ^swiergot ptak^ow.\n",
                "Ciep^ly wiatr muska twoj^a twarz.\n"
                })[random(8)];
         case MT_JESIEN:
             return ({
                "Bezlistne ga^l^ezie muskaj^a tw^oj policzek.\n",
                "Wysoko w g^orze wiatr porusza ga^leziami.\n",
                "Zimny wiatr muska ci^e po twarzy.\n",
                "Poruszane wiatrem ga^lezie szeleszcz^a suchymi li^s^cmi.\n",
                "Porwane przez wiatr li^s^cie zaczynaj^a unosi^c si^e ku "+
                "g^orze.\n",
                "Trawa ko^lysze si^e pod mu^sni^eciem wiatru.\n"
                })[random(6)];
         case MT_ZIMA:
             return ({
                "Bezlistne ga^l^ezie muskaj^a tw^oj policzek.\n",
                "Zimny wiatr muska ci^e po twarzy.\n",
                "Lodowaty wiatr muska ci^e po twarzy.\n",
                "Wysoko w g^orze wiatr porusza ga^leziami.\n",
                "Niedaleko ciebie kupka ^sniegu spada z ga^l^ezi i upada na "+
                "ziemi^e.\n",
                "Jaka^s zagubiona wrona przysiad^la na ga^l^ezi i "+
                "kracze zapami^etale.\n",
                "Z ga^l^ezi spada grudka ^sniegu, "+
                "niemal trafiaj^ac ci^e w g^low^e.\n"
                })[random(7)];
    }
}
string
pora_roku_noc()
{
    switch (pora_roku())
    {
         case MT_LATO:
         case MT_WIOSNA:
             return ({"Li^scie drzew delikatnie poruszaj^a si^e na "+
             "wietrze.\n","Trawa ko^lysze si^e pod mu^sni^eciem wiatru.\n"})
             [random(2)];
         case MT_JESIEN:
             return ({"Bezlistne ga^l^ezie muskaj^a tw^oj policzek.\n",
             "Wysoko w g^orze wiatr porusza ga^leziami.\n",
             "Zimny wiatr muska ci^e po twarzy.\n",
             "Poruszane wiatrem ga^lezie szeleszcz^a suchymi li^s^cmi.\n",
             "Porwane przez wiatr li^s^cie zaczynaj^a unosi^c si^e ku "+
             "g^orze.\n","Trawa ko^lysze si^e pod mu^sni^eciem wiatru.\n"})
             [random(5)];
         case MT_ZIMA:
             return ({"Bezlistne ga^l^ezie muskaj^a tw^oj policzek.\n",
             "Z ga^l^ezi spada grudka ^sniegu, niemal "+
             "trafiaj^ac ci^e w g^low^e.\n","Zimny wiatr muska ci^e po "+
             "twarzy.\n","Lodowaty wiatr muska ci^e po twarzy.\n",
             "Wysoko w g^orze wiatr porusza ga^leziami.\n",
             "Niedaleko ciebie kupka ^sniegu spada z ga^l^ezi i upada na "+
             "ziemi^e.\n"})[random(7)];
    }
}

int zwolaj(string str)
{
    if(!str)
    {
        notify_fail("Co chcesz zwo�a�?\n");
        return 0;
    }

    if ((str != "transport")  && (str != "drwali"))
    {
        notify_fail("Co chcesz zwo�a�?\n");
        return 0;
    }

    if(member_array(TP->query_real_name(), zwolali) > -1)
    {
        notify_fail("Transport jest ju� zapewne w drodze!\n");
        return 0;
    }

    /* Zabieramy klody i galezie */
    object *inv = all_inventory(TO);
    object *klody = ({ });
    object *galezie = ({ });
    int inv_size = sizeof(inv);             // Ilosc itemkow na lokacji
    int cena;                               // Cena za wszystkie klody na lokacji
    int tmp_cena;
    string func="";

    for(int i =0; i < inv_size; i++)
    {
        func = function_exists("create_object", inv[i]);

        if(func == "/std/drzewo/kloda")
        {
            if(inv[i]->query_kloda_owner() == TP || inv[i]->query_kloda_owner() == 0)
            {
                klody += ({inv[i]});
                cena += inv[i]->query_real_cena();
            }
        }

        if(func == "/std/drzewo/galaz")
            galezie += ({inv[i]});
    }

    if(!sizeof(klody))
    {
        notify_fail("Niestety w pobli�u nie ma �adnej k�ody, kt�r� w�asnor�cznie ociosa�"
            +TP->koncowka("e�", "a�")+".\n");
        return 0;
    }
    else
    {
        TP->catch_msg("Gromkimi okrzykami starasz si� zwo�a� ekip� drwali.\n");
        set_alarm(5.0, 0.0, "zbieramy", klody, galezie, TP, cena);

        if(!zwolali)
            zwolali = ({ });

        zwolali += ({TP->query_real_name()});
    }

    return 1;
}

void zbieramy(object *klody, object *galezie, object kto, int cena)
{
    object loc = find_object(TARTAK);
    loc->dodaj_do_zaplaty(kto->query_real_name(), cena);

    tell_roombb(TO, "Na miejsce �wawo przybywa grupka ros�ych m�czyzn i w mgnieniu oka, pe�nymi energii"+
        " ruchami tn� i �aduj� na w�z "+COMPOSITE_DEAD(klody, PL_BIE)+".\n");

    if(ENV(kto) == TO)
    {
        kto->catch_msg("Jeden z m�czyzn zwraca si� do ciebie: Zap�at� mo�na w ka�dej"+
            " chwili u mo�ci Raghola w tartaku odebra�.\n");
        saybb("Jeden z m�czyzn mruczy co� do "+QIMIE(kto, PL_DOP)+".\n");
    }

    tell_roombb(TO, "Po kr�tkiej przerwie transport wyrusza dalej w las.\n");

    klody->remove_object();
    galezie->remove_object();

    zwolali -= ({TP->query_real_name()});
}

int
filter_trees(object ob)
{
    if (function_exists("create_object", ob) == "/std/drzewo")
        return 1;
    return 0;
}

void odnow_drzewa()
{
    ::odnow_drzewa();
    raghol_quest();
}

void
raghol_quest()
{
    object sygnet;
    object nadrzewie;
    object* trees = filter(all_inventory(TO), &filter_trees());

    // quest: sygnet dla raghola
    LOAD_ERR("/d/Standard/Redania/Rinde_okolice/Las/obiekty/sygnet_raghola");
    sygnet = find_object("/d/Standard/Redania/Rinde_okolice/Las/obiekty/sygnet_raghola");
    if (sygnet && (ENV(sygnet) == 0))
    {
        foreach (object tree : trees)
        {
            if (!random(10))
            {
                nadrzewie = tree->query_na_drzewie();

                if (!objectp(nadrzewie))
                {
                    nadrzewie = clone_object("/std/drzewo/nadrzewie");
                    nadrzewie->set_drzewo(tree);
                    tree->set_na_drzewie(nadrzewie);
                }

                if (!sizeof(FILTER_LIVE(all_inventory(nadrzewie))))
                {
                    sygnet->move(nadrzewie);
                    break;
                }
            }
        }
    }

}

public string
init_arg(string arg)
{
    string ret = ::init_arg(arg);

    raghol_quest();

    return ret;
}
