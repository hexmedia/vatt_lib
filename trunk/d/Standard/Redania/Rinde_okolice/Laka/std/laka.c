/**
 * Std do lokacji ��ki w pobli�u RINDE
 *
 * @author Avard
 * @date 17.07.2006
 *
 * @author Vera
 * @date 10.03.2007
 *
 * @author Krun
 * @date 2.10.2008
 */
#include "dir.h"

inherit REDANIA_STD;

#include <mudtime.h>
#include <language.h>
#include <macros.h>
#include <stdproperties.h>
#include <pogoda.h>

//string losuj_kurwa_long();
string opis_polmroku();

string dlugi;

void
create_laka()
{
}

nomask void
create_redania()
{
    set_polmrok_long("@@opis_polmroku@@");

    add_event("@@event_laki:"+file_name(TO)+"@@");

    set_event_time(300.0);

    add_sit(({"na trawie","na ziemi"}),"na trawie","z trawy",0);
    add_prop(ROOM_I_TYPE, ROOM_FIELD);

    dodaj_ziolo("babka_lancetowata.c");
    dodaj_ziolo("chaber.c");
    dodaj_ziolo(({"czarny_bez-lisc.c",
                  "czarny_bez-kwiaty.c",
                  "czarny_bez-owoce.c"}));
    dodaj_ziolo(({"krwawnik_ziele.c",
                  "krwawnik_kwiatostan.c"}));
    dodaj_ziolo("piolun.c");
    add_prop(ROOM_I_WSP_Y, WSP_Y_RINDE); //Wsp�rz�dne, do systemu pogodowego.
    add_prop(ROOM_I_WSP_X, WSP_X_RINDE);
    create_laka();
}

string
event_laki()
{
    switch (pora_dnia())
    {
        case MT_RANEK:
        case MT_POLUDNIE:
        case MT_POPOLUDNIE:
            return process_string("@@pora_roku_dzien@@");
        default: // wieczor, noc
            return ({process_string("@@pora_roku_noc@@"),
                "Nietoperz muska mi�kkim skrzyd�em tw�j policzek po czym "+
                "przera�ony odlatuje czym pr�dzej.\n",
                "Cicho pohukuj�ca ma�a s�wka przelatuje tu� obok twojej g�owy.\n"})[random(4)];
    }
}

string
pora_roku_dzien()
{
    switch (pora_roku())
    {
         case MT_LATO:
         case MT_WIOSNA:
             return ({"Motyl przysiada na kwiecie mlecza, kt^ory ko^lysze "+
                "si^e lekko pod jego ci^e^zarem.\n",
                "Motyl przysiada na kwiecie stokrotki, kt^ory ko^lysze si^e "+
                "lekko pod jego ci^e^zarem.\n",
                "Wysoki trel skowronka d^xwi^ecz^ac wibruje w powietrzu.\n",
                "Drapie�ny ptak nagle pikuje w d� atakuj�c upatrzon� "+
                "zwierzyn�.\n",
                "Ma�a mr�wka pracowicie wspina si� po �d�ble trawy, ale nag�y "+
                "podmuch wiatru str�ca j� z powrotem na ziemi�.\n",
                "W powietrzu unosz� si� chmary drobnych muszek, kt�re "+
                "zaczynaj� k�sa� bardzo bole�nie.\n",
                "Wielka mucha przelecia^la ci tu^z ko^lo nosa.\n",
                "Polna myszka przebieg^la niedaleko twoich st^op.\n",
                "Dochodzi ci^e ciche popiskiwanie. Czy^zby jaka^s mysz?\n",
                "Ma^la pszcz^o^lka przeskakuje rado^snie z kwiatka na kwiatek.\n",
                "Niewielki je^z przetupta^l niedaleko ciebie i znikn^a^l "+
                "pomi^edzy trawami.\n",
                "Du^zy b^ak niebezpiecznie kr^eci si^e ko^lo twojego ucha.\n",
                "W powietrzu rozchodzi si^e przyjemny zapach kwiat^ow i trawy.\n",
                "Bogatokolorowy motyl beztrosko przelecia^l nad trawami.\n",
                "W^sr^od kwiat^ow dostrzegasz krz^ataj^ace si^e pszczo^ly.\n"})[random(15)];
         case MT_JESIEN:
             return ({"Drapie�ny ptak nagle pikuje w d� atakuj�c upatrzon� "+
                "zwierzyn�.\n",
                "Ma�a mr�wka pracowicie wspina si� po �d�ble trawy, ale nag�y "+
                "podmuch wiatru str�ca j� z powrotem na ziemi�.\n",
                "W powietrzu unosz� si� chmary drobnych muszek, kt�re "+
                "zaczynaj� k�sa� bardzo bole�nie.\n",
                "Klucz dzikich g�si osiada na ��kach aby odpocz�� przed "+
                "dalszym lotem na po�udnie.\n",
                "Stado dzikich ptak�w zbiera si� do odlotu na po�udnie "+
                "nawo�uj�c si� kr�tkim pokrzykiwaniem.\n",
                "Wielka mucha przelecia^la ci tu^z ko^lo nosa.\n",
                "Polna myszka przebieg^la niedaleko twoich st^op.\n",
                "Dochodzi ci^e ciche popiskiwanie. Czy^zby jaka^s mysz?\n",
                "Niewielki je^z przetupta^l niedaleko ciebie i znikn^a^l "+
                "pomi^edzy trawami.\n"})[random(9)];
         case MT_ZIMA:
             return ({"Stado kracz�cych wron siada na �niegu i przygl�da "+
                "ci si� �akomym wzrokiem.\n",
                "S�ycha� skrzypienie i trzaskanie marzn�cych drzew.\n"})[random(2)];
    }
}
string
pora_roku_noc()
{
    switch (pora_roku())
    {
         case MT_LATO:
         case MT_WIOSNA:
             return ({"Wielka mucha przelecia^la ci tu^z ko^lo nosa.\n",
             "Polna myszka przebieg^la niedaleko twoich st^op.\n",
             "Dochodzi ci^e ciche popiskiwanie. Czy^zby jaka^s mysz?\n",
             "S�ycha� nieprzyjemne wysokie brz�czenie i nim orientujesz si� "+
             "co to jest kilka komar�w gryzie ci� bole�nie w kark.\n"})[random(4)];
         case MT_JESIEN:
             return ({"Wielka mucha przelecia^la ci tu^z ko^lo nosa.\n",
             "Polna myszka przebieg^la niedaleko twoich st^op.\n",
             "Dochodzi ci^e ciche popiskiwanie. Czy^zby jaka^s mysz?\n"})[random(3)];
         case MT_ZIMA:
             return ({"S�ycha� skrzypienie i trzaskanie marzn�cych drzew.\n"})[random(1)];
    }
}

/*----------Opis�w by�o chyba z pi��. W tym opis Faeve i Tinardan,
 * wi�cej grzech�w nie pami�tam,
 * Vera
 */
string
opis_polmroku()
{
    string str;

    if(pora_roku() == MT_ZIMA || pora_roku() == MT_JESIEN)
    {
        if(CZY_JEST_SNIEG(this_object()))
            str="W mroku jeste� w stanie dostrzec jedynie biel �niegu "+
                "le��cego na ziemi.\n";
        else
            str="Nad ��k� zapad� mrok, dlatego nie jeste� w stanie dostrzec "+
                "zbyt wiele.\n";
    }
    else
        str="Nad ��k� zapad� mrok, dlatego nie jeste� w stanie dostrzec "+
                "zbyt wiele.\n";

    return str;
}

/**
 * Opis nocy jest na wszystkich lokacjach taki sam, wi�c nie ma sensu go trzyma� na ka�dej z lokacji
 * dlatego te� zrobi�em tu.
 *
 * @return Og�lny opis nocy.
 *
 * @author Krun
 * @date 2.10.2008
 */
string
noc_ogolny()
{
    return "Ciemno�� tylko pot�guje "+
        "monotoni� tego miejsca. Jest tak "+
        "ciemno, �e nic praktycznie nie wida�, jedynie w odleg�o�ci nie "+
        "wi�kszej ni� wyci�gni�cie r�ki - jakie� ro�liny i pozamykane "+
        "na noc p�czki kwiat�w. Nietrudno wdepn�� w dziur�, czy te� "+
        "odchody dzikich zwierz�t. O tym, �e jeste� na ��ce "+
        "przypominaj� jedynie pl�cz�ce si� wok� n�g �odygi ro�lin, "+
        "jednak w ciemno�ciach nie spos�b rozpozna� ich gatunk�w. "+
        "Gdzie� na wschodzie majacz� mury miejskie, od "+
        "kt�rych bije niedu�a �una pochodni i latar� ulicznych. ";
}

/**
 * Funkcja pozwalaj�ca na skr�cenie @see add_exit() kt�re z wyj�tkiem lokacji
 * na kt�re prowadzi - i to tylko numerku lokacji - oraz kierunku nie r�ni� si�
 * niczym.
 *
 * @author Krun
 * @date 2.10.2008
 */
nomask void
add_t_exit(int lok, string kier)
{
    add_exit(LAKA_RINDE_LOKACJE + "laka" + lok + ".c", kier, 0, LAKA_RO_FATIG, 1);
}