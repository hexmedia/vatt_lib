/**
 * \file /d/Standard/Redania/Rinde_okolice/Laka/lokacje/laka20.c
 *
 * Autor zbiorwoy, przerobione z losowego systemu Very.
 *
 * @author Veila
 * @date 25.09.2008
 */

#include "dir.h"
#include <macros.h>
#include <pogoda.h>
#include <mudtime.h>
#include <ss_types.h>
#include <filter_funs.h>
#include <stdproperties.h>

inherit LAKA_RINDE_STD;

string dlugi_opis();

void
create_laka()
{
    set_short("Gdzie^s w^sr^od ^l^ak.");
    set_long(&dlugi_opis());

    add_t_exit(15, "ne");
    add_t_exit(16, "n");
    add_t_exit(17, "nw");
    add_t_exit(19, "e");
    add_t_exit(21, "w");
    add_t_exit(25, "se");
    add_t_exit(26, "s");
    add_t_exit(27, "sw");

    set_alarm_every_hour("co_godzine");
}

string
dlugi_opis()
{
    string str = "";

    if (jest_dzien())
    {
        if(CZY_JEST_SNIEG(this_object()))
        {
            str += "Monotonia ��ki zim� jest beznami�tna. "+
                "W chwilach, gdy na niebie pojawia si� s�o�ce, jego promienie "+
                "dodaj� temu miejscu nieco rado�ci i �ycia - odbijaj� si� weso�o "+
                "na �niegu, skrz�c niczym ma�e brylanciki. " +
                "Gruba warstwa zlodowacia^lego ^sniegu pokrywa ^l^aki poczynaj^ac "+
                "od muru Rinde na wschodzie, a^z do lasu ciemniej^acego na "+
                "po^ludniowym-zachodzie. " +
                "Co jaki^s czas na twoj^a twarz pada cie^n pochodz^acy od "+
                "przelatuj^acych ptak^ow ^spiesz^acych do pobliskiego lasu, "+
                "kt^orego zarys maluje si^e gdzie^s daleko na zachodzie. W przeciwnym "+
                "kierunku, ponad trawami i pojedynczymi krzakami, wy�aniaj^a "+
                "si^e mury miasta. " +
                "Pokryte ^sniegiem ro^sliny i wysokie trawy, kt^orych suche ko^nce "+
                "wystaj^a ponad bia^ly ko�uch, z niecierpliwo^sci^a oczekuj^ac na "+
                "pierwsze oznaki wiosny. " +
                "Mroczny zarys pobliskiego "+
                "pobliskiego lasu maluje si^e gdzie^s daleko na zachodzie. ";
        }
        else if(pora_roku() == MT_WIOSNA)
        {
            str += "To miejsce jest paradoksalne - przenikaj� si� tu "+
                "monotonia i r�norodno��. Nie jest w �aden spos�b "+
                "charakterystyczne, wr�cz przeciwnie - wszystko jest takie same "+
                "- wok� rozpo�ciera si� ogromna ��ka, a dopiero gdzie� na "+
                "po�udniowo-zachodniej cz�ci horyzontu widnieje las. "+
                "R�norodno�� za� zawiera si� w niezwyk�ej rozmaito�ci ro�lin "+
                "i barw przeplataj�cych si� nawzajem. Jest tu ca�e mn�stwo "+
                "mak�w i b�awatk�w, poprzetykanych ��t� naw�oci� i "+
                "zielonkawymi k�osami traw. "+
                "Niewysokie, spl^atane zaro^sla pokryte zaledwie "+
                "mgie^lk^a zieleni tworz^a na po^ludniowym-zachodzie, na "+
                "skraju por^eby "+
                "zwarty mur daj^acy schronienie wielu ptakom i drobnej "+
                "zwierzynie. "+
                "Rz^ad dostojnych, grubych drzew okrytych "+
                "drobnymi, zielonymi listeczkami wyznacza ju^z z daleka "+
                "tras^e traktu majacz^acego w oddali na po^ludniowym-"+
                "wschodzie.";
        }
        else if(pora_roku() == MT_LATO)
        {
            str += "Wsz^edzie, gdzie nie si^egniesz wzrokiem widzisz zielone po^lacie "+
                "bezkresnych ^l^ak. Kolorowe kwiaty, szumi^a cicho poruszone "+
                "podmuchem ciep^lego wiatru, kt^ory delikatnie muska twoj^a twarz, "+
                "przypominaj^ac o nieustaj^acym upale. "+
                "Co jaki^s czas na twoj^a twarz pada cie^n pochodz^acy "+
                "od przelatuj^acych ptak^ow ^spiesz^acych do pobliskiego lasu, "+
                "kt^orego zarys maluje si^e gdzie^s daleko na zachodzie. "+
                "Drzewa rosn^ace wzd^lu^z widocznego "+
                "na po^ludniowym-wschodzie traktu pokryte g^estym listowiem "+
                "i kwiatami wydzielaj^a s^lodki intensywny zapach "+
                "docieraj^acy chwilami z podmuchami wiatru a^z tu. "+
                "Kolczaste zaro^sla na po^ludniowym-"+
                "zachodzie, "+
                "obsypane bia^lymi kwiatkami i czerwonymi owocami ca^le a^z "+
                "dr^z^a od bezustannego ptasiego ^swiergotu milkn^acego "+
                "tylko wtedy gdy na niebie pojawia si^e majestatyczna "+
                "sylwetka myszo^lowa. ";
        }
        else
        {
            str += "Wsz^edzie, gdzie nie si^egniesz wzrokiem widzisz ^z^o^lte po^lacie "+
                "bezkresnych ^l^ak. "+
                "Jak olbrzymie zwierz^e przyczajone w ciemno^sci majaczy po "+
                "wschodniej stronie r^owniny ciemny, zamglony "+
                "mur Rinde. Rozmi^ek^l^a, b^lotnist^a ziemi^e mlaszcz^ac^a "+
                "pod stopami przy najdrobniejszym poruszeniu pokrywa jaka^s "+
                "o^slizg^la mi^ekka masa. "+
                "Ja^sniejsz^a plam^a majacz^a na "+
                "tle ciemnego lasu kolczaste zaro^sla le^z^ace na skraju "+
                "por^eby. Dostojne drzewa chroni^ace drapie^znymi, "+
                "bezlistnymi paluchami ga^l^ezi trakt majacz^a za "+
                "butwiej^acymi ^l^akami na po�udniu. "+
                "Co jaki^s czas na twoj^a twarz pada cie^n pochodz^acy "+
                "od przelatuj^acych ptak^ow ^spiesz^acych do pobliskiego lasu, "+
                "kt^orego zarys maluje si^e gdzie^s daleko na zachodzie. W "+
                "przeciwnym kierunku, ponad trawami i pojedynczymi krzakami, "+
                "wy�aniaj^a si^e mury miasta. ";
        }
    }
    else
    {
        if(CZY_JEST_SNIEG(this_object()))
        {
            str += "Monotonia ��ki zim� jest beznami�tna. "+
                "Gruba warstwa zlodowacia^lego ^sniegu pokrywa ^l^aki poczynaj^ac "+
                "od muru Rinde na wschodzie, a^z do lasu ciemniej^acego na "+
                "po^ludniowym-zachodzie. "+
                "Nieliczne tropy zwierz^at, kieruj^ace si^e g^l^ownie w stron^e "+
                "niewielkiej zaspy le^z^acej na skraju lasu tworz^a na tej "+
                "ol^sniewaj^acej powierzchni asymetryczny wz^or. "+
                "Wok� roztacza si� ogromna �nie�na pustynia. "+
                "Pomijaj�c mury miasta w dali na "+
                "wschodzie, od kt�rych bije niedu�a �una pochodni i "+
                "latar� ulicznych, okolica wygl�da na wymar��.";
        }
        else if(pora_roku() == MT_WIOSNA)
            str += noc_ogolny();
        else if(pora_roku() == MT_LATO)
            str += noc_ogolny();
        else
        {
            str +=noc_ogolny() + "Jak olbrzymie zwierz^e przyczajone w ciemno^sci majaczy po "+
                "wschodniej stronie r^owniny ciemny, zamglony "+
                "mur Rinde. Rozmi^ek^l^a, b^lotnist^a ziemi^e mlaszcz^ac^a "+
                "pod stopami przy najdrobniejszym poruszeniu pokrywa jaka^s "+
                "o^slizg^la mi^ekka masa. ";
        }
    }

    str += "\n";

    return str;
}

//FIXME: To powinno by� zrobione z u�yciem informatora i konkretnej godziny, a nie co godzine
//       sprawdza� czy jest godzina 11.
int
co_godzine()
{
    if (MT_GODZINA == 11)
    {
        set_alarm(1.0,0.0, "odnow_poludnice");
    }
}
void
odnow_poludnice()
{
    LOAD_ERR(LAKA_RINDE_LIVINGI + "poludnica");

    object poludnica;
    poludnica = clone_object(LAKA_RINDE_LIVINGI + "poludnica");
    tell_roombb("^Swiat rozp^lywa ci si^e przed oczami, a z powsta^ej mg^ly "+
        "materializuje si^e "+QCIMIE(poludnica, PL_MIA)+".\n");
    poludnica->init_arg(0);
    poludnica->start_me();
    poludnica->move(TO);
}
