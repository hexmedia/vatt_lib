/**
 * \file /d/Standard/Redania/Rinde_okolice/Laka/lokacje/laka19.c
 *
 * Autor zbiorwoy, przerobione z losowego systemu Very.
 *
 * @author Veila
 * @date 25.09.2008
 */

#include "dir.h"
#include <macros.h>
#include <pogoda.h>
#include <mudtime.h>
#include <ss_types.h>
#include <filter_funs.h>
#include <stdproperties.h>

inherit LAKA_RINDE_STD;

string dlugi_opis();

void
create_laka()
{
    set_short("Gdzie^s w^sr^od ^l^ak.");
    set_long(&dlugi_opis());

    add_t_exit(14, "ne");
    add_t_exit(15, "n");
    add_t_exit(16, "nw");
    add_t_exit(18, "e");
    add_t_exit(20, "w");
    add_t_exit(24, "se");
    add_t_exit(25, "s");
    add_t_exit(26, "sw");
}

string
dlugi_opis()
{
    string str = "";

    if (jest_dzien())
    {
        if(CZY_JEST_SNIEG(this_object()))
        {
            str += "Dooko^la s^lycha^c ciche popiskiwanie dzikich zwierz^atek, "+
                "kt^ore zwinnie poruszaj^a si^e pomi^edzy wysokimi zaro^slami, "+
                "sprawnie omijaj^ac przer^o^zne przeszkody. "+
                "Na po^ludniowym-wschodzie ten monotonny krajobraz przecina ciemna "+
                "kresa traktu okolonego rz^edami dostojnych, pot^e^znych drzew. "+
                "Co jaki^s czas na twoj^a twarz pada cie^n pochodz^acy od "+
                "przelatuj^acych ptak^ow ^spiesz^acych do pobliskiego lasu, "+
                "kt^orego zarys maluje si^e gdzie^s daleko na zachodzie. W przeciwnym "+
                "kierunku, ponad trawami i pojedynczymi krzakami, wy�aniaj^a "+
                "si^e mury miasta. "+
                "Nieliczne tropy zwierz^at, kieruj^ace si^e g^l^ownie w stron^e "+
                "niewielkiej zaspy le^z^acej na skraju lasu tworz^a na tej "+
                "ol^sniewaj^acej powierzchni asymetryczny wz^or. "+
                "Pomijaj�c mury miasta w dali na "+
                "wschodzie, od kt�rych bije niedu�a �una pochodni i "+
                "latar� ulicznych, okolica wygl�da na wymar��.";
        }
        else if(pora_roku() == MT_WIOSNA)
        {
            str += "Wsz^edzie, gdzie nie si^egniesz wzrokiem widzisz zielone "+
                "po^lacie bezkresnych ^l^ak. "+
                "Niska, jasnozielona murawa pokrywaj^aca delikatnym "+
                "kobiercem ziemi^e, przetykana gdzieniegdzie plamami "+
                "ciemniejszej zieleni k^ep jakiego^s zielska skrzy si^e "+
                "ca^la od rosy pokrywaj^acej ka^zd^a trawk^e i barwne "+
                "g^l^owki kwitn^acych kwiat^ow. W tej soczystej obfito^sci "+
                "ro^slin chroni^a si^e liczne owady, kt^orych jednostajne "+
                "brz^eczenie i cykanie wraz ze ^spiewem bujaj^acych wysoko "+
                "na niebie skowronk^ow tworzy niepowtarzaln^a muzyk^e "+
                "bezkresnych, zielonych ^l^ak rozci^agaj^acych si^e na "+
                "p^o^lnocy a^z po horyzont. Bezkres tej r^owniny m^aci "+
                "tylko majacz^acy na wschodzie ciemny mur "+
                "Rinde. "+
                "Rz^ad dostojnych, grubych drzew okrytych "+
                "drobnymi, zielonymi listeczkami wyznacza ju^z z daleka "+
                "tras^e traktu majacz^acego w oddali na po^ludniowym-"+
                "wschodzie.";
        }
        else if(pora_roku() == MT_LATO)
        {
            str += "Rozci^agaj^ace si^e a^z po p^o^lnocny horyzont morze coraz "+
                "wy^zszych traw faluje w rytm powiew^ow wiatru, a z jego "+
                "trawiastych fal wy^lania si^e na wschodzie jak "+
                "okr^et mur Rinde. "+
                "Dooko^la s^lycha^c ciche "+
                "popiskiwanie dzikich zwierz^atek, kt^ore zwinnie poruszaj^a si^e "+
                "pomi^edzy wysokimi zaro^slami, sprawnie omijaj^ac przer^o^zne "+
                "przeszkody. "+
                "Co jaki^s czas na twoj^a twarz pada cie^n pochodz^acy "+
                "od przelatuj^acych ptak^ow ^spiesz^acych do pobliskiego lasu, "+
                "kt^orego zarys maluje si^e gdzie^s daleko na zachodzie. W "+
                "przeciwnym kierunku, ponad trawami i pojedynczymi krzakami, "+
                "wy�aniaj^a si^e mury miasta. "+
                "Kolczaste zaro^sla na po^ludniowym-"+
                "zachodzie, "+
                "obsypane bia^lymi kwiatkami i czerwonymi owocami ca^le a^z "+
                "dr^z^a od bezustannego ptasiego ^swiergotu milkn^acego "+
                "tylko wtedy gdy na niebie pojawia si^e majestatyczna "+
                "sylwetka myszo^lowa. ";
        }
        else
        {
            str += "Rozci^agaj^aca si^e a^z po horyzont, bezkresna r^ownina "+
                "pokryta namok^lymi trawami wydziela wo^n butwiej^acych "+
                "ro^slin, a wyp^lowia^le od wiatru i deszczu k^epy suchych "+
                "badyli, kt^ore opar^ly si^e apetytowi pas^acych si^e tutaj "+
                "zwierz^at chwiej^a si^e sm^etnie w podmuchach wiatru. "+
                "Dooko^la s^lycha^c ciche popiskiwanie "+
                "dzikich zwierz^atek, kt^ore zwinnie poruszaj^a si^e pomi^edzy "+
                "wysokimi zaro^slami, sprawnie omijaj^ac przer^o^zne przeszkody. "+
                "Dostojne drzewa chroni^ace drapie^znymi, "+
                "bezlistnymi paluchami ga^l^ezi trakt majacz^a za "+
                "butwiej^acymi ^l^akami gdzie^s na po^ludniu. "+
                "Co jaki^s czas na twoj^a twarz pada cie^n pochodz^acy "+
                "od przelatuj^acych ptak^ow ^spiesz^acych do pobliskiego lasu, "+
                "kt^orego zarys maluje si^e gdzie^s daleko na zachodzie. W "+
                "przeciwnym kierunku, ponad trawami i pojedynczymi krzakami, "+
                "wy�aniaj^a si^e mury miasta. ";
        }
    }
    else
    {
        if(CZY_JEST_SNIEG(this_object()))
        {
            str += "Monotonia ��ki zim� jest beznami�tna. "+
                "Gruba warstwa zlodowacia^lego ^sniegu pokrywa ^l^aki poczynaj^ac "+
                "od muru Rinde na wschodzie, a^z do lasu ciemniej^acego na "+
                "po^ludniowym-zachodzie. "+
                "Nieliczne tropy zwierz^at, kieruj^ace si^e g^l^ownie w stron^e "+
                "niewielkiej zaspy le^z^acej na skraju lasu tworz^a na tej "+
                "ol^sniewaj^acej powierzchni asymetryczny wz^or. "+
                "Wok� roztacza si� ogromna �nie�na pustynia. "+
                "Pomijaj�c mury miasta w dali na "+
                "wschodzie, od kt�rych bije niedu�a �una pochodni i "+
                "latar� ulicznych, okolica wygl�da na wymar��.";
        }
        else if(pora_roku() == MT_WIOSNA)
            str += noc_ogolny();
        else if(pora_roku() == MT_LATO)
        {
            str += noc_ogolny() + "Kolczaste zaro^sla na "+
                "wschodzie, majacz^ace ciemn^a plam^a na tle ponurego lasu "+
                "na po^ludniowym-zachodzie ^spi^a teraz spokojnie. Drzewa "+
                "rosn^ace "+
                "wzd^lu^z widocznego na po^ludniowym-wschodzie traktu "+
                "b^lyszcz^ace jak srebrne kolumny w blasku ksi^e^zyca "+
                "wydzielaj^a s^lodki intensywny zapach docieraj^acy "+
                "chwilami z podmuchami wiatru a^z tu. ";
        }
        else
            str +=noc_ogolny();
    }

    str += "\n";

    return str;
}
