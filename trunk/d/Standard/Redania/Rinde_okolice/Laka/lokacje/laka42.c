/**
 * \file /d/Standard/Redania/Rinde_okolice/Laka/lokacje/laka42.c
 *
 * Autor zbiorwoy, przerobione z losowego systemu Very.
 *
 * @author Veila
 * @date 25.09.2008
 */

#include "dir.h"
#include <macros.h>
#include <pogoda.h>
#include <mudtime.h>
#include <ss_types.h>
#include <filter_funs.h>
#include <stdproperties.h>

inherit LAKA_RINDE_STD;

string dlugi_opis();

void
create_laka()
{
    set_short("Gdzie^s w^sr^od ^l^ak.");
    set_long(&dlugi_opis());

    add_t_exit(35, "ne");
    add_t_exit(36, "n");
    add_t_exit(37, "nw");
    add_t_exit(41, "e");
    add_t_exit(43, "w");
    add_t_exit(47, "se");
    add_t_exit(48, "s");
    add_t_exit(49, "sw");
}

string
dlugi_opis()
{
    string str = "";

    if (jest_dzien())
    {
        if(CZY_JEST_SNIEG(this_object()))
        {
            str += "Monotonia ��ki zim� jest beznami�tna. "+
                "W chwilach, gdy na niebie pojawia si� s�o�ce, jego promienie "+
                "dodaj� temu miejscu nieco rado�ci i �ycia - odbijaj� si� weso�o "+
                "na �niegu, skrz�c niczym ma�e brylanciki. " +
                "Gruba warstwa zlodowacia^lego ^sniegu pokrywa ^l^aki poczynaj^ac "+
                "od muru Rinde na wschodzie, a^z do lasu ciemniej^acego na "+
                "po^ludniowym-zachodzie. " +
                "Co jaki^s czas na twoj^a twarz pada cie^n pochodz^acy od "+
                "przelatuj^acych ptak^ow ^spiesz^acych do pobliskiego lasu, "+
                "kt^orego zarys maluje si^e gdzie^s daleko na zachodzie. W przeciwnym "+
                "kierunku, ponad trawami i pojedynczymi krzakami, wy�aniaj^a "+
                "si^e mury miasta. " +
                "Pokryte ^sniegiem ro^sliny i wysokie trawy, kt^orych suche ko^nce "+
                "wystaj^a ponad bia^ly ko�uch, z niecierpliwo^sci^a oczekuj^ac na "+
                "pierwsze oznaki wiosny. " +
                "Mroczny zarys pobliskiego "+
                "pobliskiego lasu maluje si^e gdzie^s daleko na zachodzie. ";
        }
        else if(pora_roku() == MT_WIOSNA)
        {
            str += "Wsz^edzie, gdzie nie si^egniesz wzrokiem widzisz zielone "+
                "po^lacie bezkresnych ^l^ak. "+
                "Niska, jasnozielona murawa pokrywaj^aca delikatnym "+
                "kobiercem ziemi^e, przetykana gdzieniegdzie plamami "+
                "ciemniejszej zieleni k^ep jakiego^s zielska skrzy si^e "+
                "ca^la od rosy pokrywaj^acej ka^zd^a trawk^e i barwne "+
                "g^l^owki kwitn^acych kwiat^ow. W tej soczystej obfito^sci "+
                "ro^slin chroni^a si^e liczne owady, kt^orych jednostajne "+
                "brz^eczenie i cykanie wraz ze ^spiewem bujaj^acych wysoko "+
                "na niebie skowronk^ow tworzy niepowtarzaln^a muzyk^e "+
                "bezkresnych, zielonych ^l^ak rozci^agaj^acych si^e na "+
                "p^o^lnocy a^z po horyzont. Bezkres tej r^owniny m^aci "+
                "tylko majacz^acy na wschodzie ciemny mur "+
                "Rinde. "+
                "Rz^ad dostojnych, grubych drzew okrytych "+
                "drobnymi, zielonymi listeczkami wyznacza ju^z z daleka "+
                "tras^e traktu majacz^acego w oddali na po^ludniowym-"+
                "wschodzie.";
        }
        else if(pora_roku() == MT_LATO)
        {
            str += "Rozci^agaj^ace si^e a^z po p^o^lnocny horyzont morze coraz "+
                "wy^zszych traw faluje w rytm powiew^ow wiatru, a z jego "+
                "trawiastych fal wy^lania si^e na wschodzie jak "+
                "okr^et mur Rinde. "+
                "Dooko^la s^lycha^c ciche "+
                "popiskiwanie dzikich zwierz^atek, kt^ore zwinnie poruszaj^a si^e "+
                "pomi^edzy wysokimi zaro^slami, sprawnie omijaj^ac przer^o^zne "+
                "przeszkody. "+
                "Co jaki^s czas na twoj^a twarz pada cie^n pochodz^acy "+
                "od przelatuj^acych ptak^ow ^spiesz^acych do pobliskiego lasu, "+
                "kt^orego zarys maluje si^e gdzie^s daleko na zachodzie. W "+
                "przeciwnym kierunku, ponad trawami i pojedynczymi krzakami, "+
                "wy�aniaj^a si^e mury miasta. "+
                "Kolczaste zaro^sla na po^ludniowym-"+
                "zachodzie, "+
                "obsypane bia^lymi kwiatkami i czerwonymi owocami ca^le a^z "+
                "dr^z^a od bezustannego ptasiego ^swiergotu milkn^acego "+
                "tylko wtedy gdy na niebie pojawia si^e majestatyczna "+
                "sylwetka myszo^lowa. ";
        }
        else
        {
            str += "To miejsce jest paradoksalne - przenikaj� si� tu "+
                "monotonia i r�norodno��. Nie jest w �aden spos�b "+
                "charakterystyczne, wr�cz przeciwnie - wszystko jest takie same "+
                "- wok� rozpo�ciera si� ogromna ��ka, a dopiero gdzie� na "+
                "po�udniowo-zachodniej cz�ci horyzontu widnieje las. "+
                "R�norodno�� za� zawiera si� w niezwyk�ej rozmaito�ci ro�lin i "+
                "barw przeplataj�cych si� nawzajem. Jest tu ca�e mn�stwo "+
                "��tawych i fioletowawych drobniutkich fio�k�w, "+
                "fioletowo-niebieskich b�awatk�w, ��tych naw�oci i wrotyczy, "+
                "a gdzieniegdzie biel� si� i r�owi� zebrane w grona male�kie "+
                "kwiatki krwawnika. "+
                "Jak olbrzymie zwierz^e przyczajone w ciemno^sci majaczy po "+
                "wschodniej stronie r^owniny ciemny, zamglony "+
                "mur Rinde. "+
                "Ja^sniejsz^a plam^a majacz^a na "+
                "tle ciemnego lasu kolczaste zaro^sla le^z^ace na skraju "+
                "por^eby. ";
        }
    }
    else
    {
        if(CZY_JEST_SNIEG(this_object()))
        {
            str += "Wsz^edzie, gdzie nie si^egniesz wzrokiem widzisz pokryte puchow^a "+
                "pierzyn^a po^lacie bezkresnych ^l^ak. "+
                "Na po^ludniowym-wschodzie ten monotonny krajobraz przecina ciemna "+
                "kresa traktu okolonego rz^edami dostojnych, pot^e^znych drzew. "+
                "Pokryte ^sniegiem ro^sliny i wysokie trawy, kt^orych suche ko^nce "+
                "wystaj^a ponad bia^ly ko�uch, z niecierpliwo^sci^a oczekuj^ac na "+
                "pierwsze oznaki wiosny. "+
                "Mroczny zarys pobliskiego "+
                "pobliskiego lasu maluje si^e gdzie^s daleko na zachodzie. "+
                "W przeciwnym kierunku, ponad trawami i pojedynczymi krzakami, "+
                "wy�aniaj^a si^e mury miasta.";
        }
        else if(pora_roku() == MT_WIOSNA)
        {
            str += noc_ogolny();
        }
        else if(pora_roku() == MT_LATO)
        {
            str += noc_ogolny() + "Kwiaty szumi^a cicho, poruszone podmuchem "+
                "ch^lodnego wiatru, kt^ory delikatnie muska twoj^a twarz, "+
                "och^ladzaj^ac przy tym cia^lo po nieustaj^acym upale. "+
                "Zapach siana, zi^o^l i kwiat^ow przesyca lekko, "+
                "drgaj^ace ponad ^l^akami, rozgrzane powietrze pe^lne "+
                "cykania wsz^edobylskich owad^ow i pohukiwania s^ow. ";
        }

        else
        {
            str +=noc_ogolny();
        }
    }

    str += "\n";

    return str;
}
