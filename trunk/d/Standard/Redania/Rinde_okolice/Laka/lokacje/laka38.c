/**
 * \file /d/Standard/Redania/Rinde_okolice/Laka/lokacje/laka38.c
 *
 * Autor zbiorwoy, przerobione z losowego systemu Very.
 *
 * @author Veila
 * @date 25.09.2008
 */

#include "dir.h"
#include <macros.h>
#include <pogoda.h>
#include <mudtime.h>
#include <ss_types.h>
#include <filter_funs.h>
#include <stdproperties.h>
inherit LAKA_RINDE_STD;

string dlugi_opis();

void
create_laka()
{
    set_short("^L^aka na skraju por^eby");
    set_long(&dlugi_opis());

    add_t_exit(32, "ne");
    add_t_exit(33, "n");
    add_t_exit(37, "e");
    add_t_exit(43, "se");
    add_exit(LAS_RINDE_LOKACJE +"wlas3.c", "nw", 0, LAKA_RO_FATIG, 1);
    add_exit(LAS_RINDE_LOKACJE +"wlas7.c", "w", 0, LAKA_RO_FATIG, 1);
    add_exit(LAS_RINDE_LOKACJE +"wlas10.c", "sw", 0, LAKA_RO_FATIG, 1);
    add_exit(LAS_RINDE_LOKACJE +"wlas9.c", "s", 0, LAKA_RO_FATIG, 1);
}

string
dlugi_opis()
{
    string str = "";

    if (jest_dzien())
    {
        if(CZY_JEST_SNIEG(this_object()))
        {
            str += "Monotonia ��ki zim� jest beznami�tna. "+
                "W chwilach, gdy na niebie pojawia si� s�o�ce, jego promienie "+
                "dodaj� temu miejscu nieco rado�ci i �ycia - odbijaj� si� weso�o "+
                "na �niegu, skrz�c niczym ma�e brylanciki. " +
                "Gruba warstwa zlodowacia^lego ^sniegu pokrywa ^l^aki poczynaj^ac "+
                "od muru Rinde na wschodzie, a^z do lasu ciemniej^acego na "+
                "po^ludniowym-zachodzie. " +
                "Co jaki^s czas na twoj^a twarz pada cie^n pochodz^acy od "+
                "przelatuj^acych ptak^ow ^spiesz^acych do pobliskiego lasu, "+
                "kt^orego zarys maluje si^e gdzie^s daleko na zachodzie. W przeciwnym "+
                "kierunku, ponad trawami i pojedynczymi krzakami, wy�aniaj^a "+
                "si^e mury miasta. " +
                "Pokryte ^sniegiem ro^sliny i wysokie trawy, kt^orych suche ko^nce "+
                "wystaj^a ponad bia^ly ko�uch, z niecierpliwo^sci^a oczekuj^ac na "+
                "pierwsze oznaki wiosny. " +
                "Mroczny zarys pobliskiego "+
                "pobliskiego lasu maluje si^e gdzie^s daleko na zachodzie. ";
        }
        else if(pora_roku() == MT_WIOSNA)
        {
            str += "Wsz^edzie, gdzie nie si^egniesz wzrokiem widzisz zielone "+
                "po^lacie bezkresnych ^l^ak. Bezwiednie ko^lysz^ace si^e na "+
                "wietrze ro^sliny u^spione i skulone spokojnie czekaj^a na "+
                "powr^ot s^lo^nca wraz z ^zyciodajnym ^swiat^lem. ",
                "Niska, jasnozielona murawa pokrywaj^aca delikatnym "+
                "kobiercem ziemi^e, przetykana gdzieniegdzie plamami "+
                "ciemniejszej zieleni k^ep jakiego^s zielska skrzy si^e "+
                "ca^la od rosy pokrywaj^acej ka^zd^a trawk^e i barwne "+
                "g^l^owki kwitn^acych kwiat^ow. W tej soczystej obfito^sci "+
                "ro^slin chroni^a si^e liczne owady, kt^orych jednostajne "+
                "brz^eczenie i cykanie wraz ze ^spiewem bujaj^acych wysoko "+
                "na niebie skowronk^ow tworzy niepowtarzaln^a muzyk^e "+
                "bezkresnych, zielonych ^l^ak rozci^agaj^acych si^e na "+
                "p^o^lnocy a^z po horyzont. "+
                "Dooko^la s^lycha^c ciche popiskiwanie "+
                "dzikich zwierz^atek, kt^ore zwinnie poruszaj^a si^e pomi^edzy "+
                "wysokimi zaro^slami, sprawnie omijaj^ac przer^o^zne przeszkody. ";
        }
        else if(pora_roku() == MT_LATO)
        {
            str += "To miejsce jest paradoksalne - przenikaj� si� tu "+
                "monotonia i r�norodno��. Nie jest w �aden spos�b "+
                "charakterystyczne, wr�cz przeciwnie - wszystko jest takie same "+
                "- wok� rozpo�ciera si� ogromna ��ka, a dopiero gdzie� na "+
                "po�udniowo-zachodniej cz�ci horyzontu widnieje las. "+
                "R�norodno�� za� zawiera si� w niezwyk�ej rozmaito�ci ro�lin "+
                "i barw przeplataj�cych si� nawzajem. Jest tu ca�e mn�stwo "+
                "mak�w i b�awatk�w, poprzetykanych ��t� naw�oci� i "+
                "zielonkawymi k�osami traw. "+
                "Dooko^la s^lycha^c ciche "+
                "popiskiwanie dzikich zwierz^atek, kt^ore zwinnie poruszaj^a si^e "+
                "pomi^edzy wysokimi zaro^slami, sprawnie omijaj^ac przer^o^zne "+
                "przeszkody. "+
                "Drzewa rosn^ace wzd^lu^z widocznego "+
                "na po^ludniowym-wschodzie traktu pokryte g^estym listowiem "+
                "i kwiatami wydzielaj^a s^lodki intensywny zapach "+
                "docieraj^acy chwilami z podmuchami wiatru a^z tu.";
        }
        else
        {
            str += "Wsz^edzie, gdzie nie si^egniesz wzrokiem widzisz ^z^o^lte po^lacie "+
                "bezkresnych ^l^ak. Zwi^edni^ete i po^z�^lk^le ju^z ro^sliny, "+
                "szumi^a cicho poruszone ch^lodnym wiatrem, kt^ory przypomina o "+
                "zbli^zaj^acej si^e zimie. "+
                "Dooko^la s^lycha^c ciche popiskiwanie "+
                "dzikich zwierz^atek, kt^ore zwinnie poruszaj^a si^e pomi^edzy "+
                "wysokimi zaro^slami, sprawnie omijaj^ac przer^o^zne przeszkody. "+
                "Ja^sniejsz^a plam^a majacz^a na "+
                "tle ciemnego lasu kolczaste zaro^sla le^z^ace na skraju "+
                "por^eby. Dostojne drzewa chroni^ace drapie^znymi, "+
                "bezlistnymi paluchami ga^l^ezi trakt majacz^a za "+
                "butwiej^acymi ^l^akami na po�udniu. "+
                "Co jaki^s czas na twoj^a twarz pada cie^n pochodz^acy "+
                "od przelatuj^acych ptak^ow ^spiesz^acych do pobliskiego lasu, "+
                "kt^orego zarys maluje si^e gdzie^s daleko na zachodzie. ";
        }
    }
    else
    {
        if(CZY_JEST_SNIEG(this_object()))
        {
            str += "Wsz^edzie, gdzie nie si^egniesz wzrokiem widzisz pokryte puchow^a "+
                "pierzyn^a po^lacie bezkresnych ^l^ak. "+
                "Na po^ludniowym-wschodzie ten monotonny krajobraz przecina ciemna "+
                "kresa traktu okolonego rz^edami dostojnych, pot^e^znych drzew. "+
                "Pokryte ^sniegiem ro^sliny i wysokie trawy, kt^orych suche ko^nce "+
                "wystaj^a ponad bia^ly ko�uch, z niecierpliwo^sci^a oczekuj^ac na "+
                "pierwsze oznaki wiosny. "+
                "Mroczny zarys pobliskiego "+
                "pobliskiego lasu maluje si^e gdzie^s daleko na zachodzie. "+
                "W przeciwnym kierunku, ponad trawami i pojedynczymi krzakami, "+
                "wy�aniaj^a si^e mury miasta.";
        }
        else if(pora_roku() == MT_WIOSNA)
        {
            str += noc_ogolny();
        }
        else if(pora_roku() == MT_LATO)
        {
            str += noc_ogolny() + "Kwiaty szumi^a cicho, poruszone podmuchem "+
                "ch^lodnego wiatru, kt^ory delikatnie muska twoj^a twarz, "+
                "och^ladzaj^ac przy tym cia^lo po nieustaj^acym upale. "+
                "Zapach siana, zi^o^l i kwiat^ow przesyca lekko, "+
                "drgaj^ace ponad ^l^akami, rozgrzane powietrze pe^lne "+
                "cykania wsz^edobylskich owad^ow i pohukiwania s^ow. ";
        }

        else
        {
            str +=noc_ogolny();
        }
    }
    str += "\n";
    return str;
}
