/**
 * \file /d/Standard/Redania/Rinde_okolice/Laka/lokacje/laka54.c
 *
 * Autor zbiorwoy, przerobione z losowego systemu Very.
 *
 * @author Veila
 * @date 25.09.2008
 */

#include "dir.h"
#include <macros.h>
#include <pogoda.h>
#include <mudtime.h>
#include <ss_types.h>
#include <filter_funs.h>
#include <stdproperties.h>

inherit LAKA_RINDE_STD;

string dlugi_opis();

void
create_laka()
{
    set_short("Gdzie^s w^sr^od ^l^ak.");
    set_long(&dlugi_opis());

    add_t_exit(48, "ne");
    add_t_exit(49, "n");
    add_exit(LAS_RINDE_LOKACJE + "wlas12.c",    "nw", 0, LAKA_RO_FATIG, 1);
    add_exit(TRAKT_RINDE_LOKACJE + "trakt12.c", "e",  0, LAKA_RO_FATIG, 1);
    add_exit(TRAKT_RINDE_LOKACJE + "trakt13.c", "se", 0, LAKA_RO_FATIG, 1);
    add_exit(TRAKT_RINDE_LOKACJE + "trakt14.c", "s",  0, LAKA_RO_FATIG, 1);
    add_exit(TRAKT_RINDE_LOKACJE + "trakt15.c", "w",  0, LAKA_RO_FATIG, 1);

    set_alarm_every_hour("co_godzine");
}

string
dlugi_opis()
{
    string str = "";

    if (jest_dzien())
    {
        if(CZY_JEST_SNIEG(this_object()))
        {
            str += "Dooko^la s^lycha^c ciche popiskiwanie dzikich zwierz^atek, "+
                "kt^ore zwinnie poruszaj^a si^e pomi^edzy wysokimi zaro^slami, "+
                "sprawnie omijaj^ac przer^o^zne przeszkody. "+
                "Na po^ludniowym-wschodzie ten monotonny krajobraz przecina ciemna "+
                "kresa traktu okolonego rz^edami dostojnych, pot^e^znych drzew. "+
                "Co jaki^s czas na twoj^a twarz pada cie^n pochodz^acy od "+
                "przelatuj^acych ptak^ow ^spiesz^acych do pobliskiego lasu, "+
                "kt^orego zarys maluje si^e gdzie^s daleko na zachodzie. W przeciwnym "+
                "kierunku, ponad trawami i pojedynczymi krzakami, wy�aniaj^a "+
                "si^e mury miasta. "+
                "Nieliczne tropy zwierz^at, kieruj^ace si^e g^l^ownie w stron^e "+
                "niewielkiej zaspy le^z^acej na skraju lasu tworz^a na tej "+
                "ol^sniewaj^acej powierzchni asymetryczny wz^or. "+
                "Pomijaj�c mury miasta w dali na "+
                "wschodzie, od kt�rych bije niedu�a �una pochodni i "+
                "latar� ulicznych, okolica wygl�da na wymar��.";
        }
        else if(pora_roku() == MT_WIOSNA)
        {
            str += "Wsz^edzie, gdzie nie si^egniesz wzrokiem widzisz zielone "+
                "po^lacie bezkresnych ^l^ak. "+
                "Niska, jasnozielona murawa pokrywaj^aca delikatnym "+
                "kobiercem ziemi^e, przetykana gdzieniegdzie plamami "+
                "ciemniejszej zieleni k^ep jakiego^s zielska skrzy si^e "+
                "ca^la od rosy pokrywaj^acej ka^zd^a trawk^e i barwne "+
                "g^l^owki kwitn^acych kwiat^ow. W tej soczystej obfito^sci "+
                "ro^slin chroni^a si^e liczne owady, kt^orych jednostajne "+
                "brz^eczenie i cykanie wraz ze ^spiewem bujaj^acych wysoko "+
                "na niebie skowronk^ow tworzy niepowtarzaln^a muzyk^e "+
                "bezkresnych, zielonych ^l^ak rozci^agaj^acych si^e na "+
                "p^o^lnocy a^z po horyzont. Bezkres tej r^owniny m^aci "+
                "tylko majacz^acy na wschodzie ciemny mur "+
                "Rinde. "+
                "Rz^ad dostojnych, grubych drzew okrytych "+
                "drobnymi, zielonymi listeczkami wyznacza ju^z z daleka "+
                "tras^e traktu majacz^acego w oddali na po^ludniowym-"+
            "wschodzie.";
        }
        else if(pora_roku() == MT_LATO)
        {
            str += "Rozci^agaj^ace si^e a^z po p^o^lnocny horyzont morze coraz "+
                "wy^zszych traw faluje w rytm powiew^ow wiatru, a z jego "+
                "trawiastych fal wy^lania si^e na wschodzie jak "+
                "okr^et mur Rinde. "+
                "Dooko^la s^lycha^c ciche "+
                "popiskiwanie dzikich zwierz^atek, kt^ore zwinnie poruszaj^a si^e "+
                "pomi^edzy wysokimi zaro^slami, sprawnie omijaj^ac przer^o^zne "+
                "przeszkody. "+
                "Co jaki^s czas na twoj^a twarz pada cie^n pochodz^acy "+
                "od przelatuj^acych ptak^ow ^spiesz^acych do pobliskiego lasu, "+
                "kt^orego zarys maluje si^e gdzie^s daleko na zachodzie. W "+
                "przeciwnym kierunku, ponad trawami i pojedynczymi krzakami, "+
                "wy�aniaj^a si^e mury miasta. "+
                "Kolczaste zaro^sla na po^ludniowym-"+
                "zachodzie, "+
                "obsypane bia^lymi kwiatkami i czerwonymi owocami ca^le a^z "+
                "dr^z^a od bezustannego ptasiego ^swiergotu milkn^acego "+
                "tylko wtedy gdy na niebie pojawia si^e majestatyczna "+
                "sylwetka myszo^lowa. ";

        }
        else
        {
            str += "Wsz^edzie, gdzie nie si^egniesz wzrokiem widzisz ^z^o^lte po^lacie "+
                "bezkresnych ^l^ak. "+
                "Jak olbrzymie zwierz^e przyczajone w ciemno^sci majaczy po "+
                "wschodniej stronie r^owniny ciemny, zamglony "+
                "mur Rinde. Rozmi^ek^l^a, b^lotnist^a ziemi^e mlaszcz^ac^a "+
                "pod stopami przy najdrobniejszym poruszeniu pokrywa jaka^s "+
                "o^slizg^la mi^ekka masa. "+
                "Ja^sniejsz^a plam^a majacz^a na "+
                "tle ciemnego lasu kolczaste zaro^sla le^z^ace na skraju "+
                "por^eby. Dostojne drzewa chroni^ace drapie^znymi, "+
                "bezlistnymi paluchami ga^l^ezi trakt majacz^a za "+
                "butwiej^acymi ^l^akami na po�udniu. "+
                "Co jaki^s czas na twoj^a twarz pada cie^n pochodz^acy "+
                "od przelatuj^acych ptak^ow ^spiesz^acych do pobliskiego lasu, "+
                "kt^orego zarys maluje si^e gdzie^s daleko na zachodzie. W "+
                "przeciwnym kierunku, ponad trawami i pojedynczymi krzakami, "+
                "wy�aniaj^a si^e mury miasta. ";
        }
    }
    else
    {
        if(CZY_JEST_SNIEG(this_object()))
        {
            str += "Monotonia ��ki zim� jest beznami�tna. "+
                "Gruba warstwa zlodowacia^lego ^sniegu pokrywa ^l^aki poczynaj^ac "+
                "od muru Rinde na wschodzie, a^z do lasu ciemniej^acego na "+
                "po^ludniowym-zachodzie. "+
                "Nieliczne tropy zwierz^at, kieruj^ace si^e g^l^ownie w stron^e "+
                "niewielkiej zaspy le^z^acej na skraju lasu tworz^a na tej "+
                "ol^sniewaj^acej powierzchni asymetryczny wz^or. "+
                "Wok� roztacza si� ogromna �nie�na pustynia. "+
                "Pomijaj�c mury miasta w dali na "+
                "wschodzie, od kt�rych bije niedu�a �una pochodni i "+
                "latar� ulicznych, okolica wygl�da na wymar��.";
        }
        else if(pora_roku() == MT_WIOSNA)
        {
            str += noc_ogolny();
        }
        else if(pora_roku() == MT_LATO)
        {
            str += noc_ogolny() + "Kwiaty szumi^a cicho, poruszone podmuchem "+
                "ch^lodnego wiatru, kt^ory delikatnie muska twoj^a twarz, "+
                "och^ladzaj^ac przy tym cia^lo po nieustaj^acym upale. "+
                "Zapach siana, zi^o^l i kwiat^ow przesyca lekko, "+
                "drgaj^ace ponad ^l^akami, rozgrzane powietrze pe^lne "+
                "cykania wsz^edobylskich owad^ow i pohukiwania s^ow. ";
        }

        else
        {
            str += noc_ogolny() + "Jak olbrzymie zwierz^e przyczajone w ciemno^sci majaczy po "+
                "wschodniej stronie r^owniny ciemny, zamglony "+
                "mur Rinde. Rozmi^ek^l^a, b^lotnist^a ziemi^e mlaszcz^ac^a "+
                "pod stopami przy najdrobniejszym poruszeniu pokrywa jaka^s "+
                "o^slizg^la mi^ekka masa. ";
        }
    }
    str += "\n";
    return str;
}

int
co_godzine()
{
    if (MT_GODZINA == 11)
    {
        set_alarm(1.0,0.0, "odnow_poludnice");
    }
}
void
odnow_poludnice()
{
    LOAD_ERR(LAKA_RINDE_LIVINGI + "poludnica");

    object poludnica;
    poludnica = clone_object(LAKA_RINDE_LIVINGI + "poludnica");
    tell_roombb("^Swiat rozp^lywa ci si^e przed oczami, a z powsta^ej mg^ly "+
        "materializuje si^e "+QCIMIE(poludnica, PL_MIA)+".\n");
    poludnica->init_arg(0);
    poludnica->start_me();
    poludnica->move(TO);
}
