/* Autor: Avard
   Data : 24.04.07
   Opis : Sniegulak */

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <pogoda.h>
#include "dir.h"

inherit ZAKON_STD;

object dywan;

void create_zakon() 
{
    set_short("Korytarz");
    add_exit(ZAKON_LOKACJE + "lok3.c","sw",0,1,0);
    add_exit(ZAKON_LOKACJE + "lok6.c","nw",0,1,0);
    add_object(ZAKON_PRZEDMIOTY + "lampa_srodek");
    dywan = clone_object(ZAKON_PRZEDMIOTY + "dywan");
	dywan->move(TO);
    add_object(ZAKON_PRZEDMIOTY + "makata");
    add_object(ZAKON_PRZEDMIOTY + "stojak_t");
    dodaj_rzecz_niewyswietlana("gruba we^lniana makata",1);
    dodaj_rzecz_niewyswietlana("gruby we^lniany dywan",1);
    dodaj_rzecz_niewyswietlana("drewniany prosty stojak");
	
	add_sit("na pod�odze","na pod�odze","z pod�ogi",0);

}

public string
exits_description() 
{
    return "Korytarz prowadzi na p^o^lnocny-zach^od "+
        "oraz na po^ludniowy-zach^od.\n";
}

string
dlugi_opis()
{
    string str;
    str = "Znajdujesz sie w korytarzu prowadz^acym od wej^scia w g^l^ab "+
        "budynku nale^z^acego do Zakonu Karmazynowych Ostrzy. Drewniane "+
        "^sciany z grubymi br^azowymi makatami, maj^acymi za zadanie "+
        "ociepla^c budynek nie s^a przyozdobione niczym innym, poza "+
        "zawieszonymi na nich dwiema lampami. ";
    if(present(dywan,TO))
    {
        if(dywan->query_prop("_siedzonko"))
		{
			str += "Pod^log^e korytarza przykryto dywanem w kolorze ciemnej "+
            "czerwieni, kt^ory wycisza kroki st^apaj^acych po nim os^ob";
		}
		else
		{
			str += "Na pod�odze korytarza le�y zwini�ty dywan w kolorze "+
			"ciemnej czerwieni";
		}
        if(jest_rzecz_w_sublokacji(0,"drewniany prosty stojak"))
        {
            str += ", a pod jedn^a ze ^scian ustawiono drewniany stojak.";
        }
		else str+=".";
    }
    else
    {
        if(jest_rzecz_w_sublokacji(0,"drewniany prosty stojak"))
        {
            str += "Pod jedn^a ze ^scian ustawiono drewniany stojak.";
        }
    }
    str += "\n";
    return str;
}

