/* Autor: Avard
   Data : 24.04.07
   Opis : Sniegulak */

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <pogoda.h>
#include <cmdparse.h>
#include "dir.h"

inherit ZAKON_STD;
inherit "/lib/guild_support";

void create_zakon() 
{
    set_short("Niewielka kaplica");
    add_exit(ZAKON_LOKACJE + "lok9.c","w",0,1,0);
    add_object(ZAKON_PRZEDMIOTY + "lawa_kaplica",4);
    add_object(ZAKON_PRZEDMIOTY + "oltarz");
    dodaj_rzecz_niewyswietlana("d^luga d^ebowa ^lawa",4);
    add_event("P^lomie^n spokojnie podryguje na o^ltarzu.\n");
    add_event("Przenikliwy ch^l^od bij^acy od nagich ^scian i pod^logi "+
        "przyprawia ci^e o dreszcz.\n");
    add_event("P^lomie^n zaprasza ci^e w kr^ag ^swiat^la, w miejsce "+
        "bezpieczne od wszechobecnego mroku.\n");
    add_npc(ZAKON_NPC + "eriel");
	add_sit("na pod�odze","na pod�odze","z pod�ogi",0);
    create_guild_support();
}

public string
exits_description() 
{
    return "Na zachodzie znajduje si^e jaka^s sala.\n";  
}

string
dlugi_opis()
{
    string str;
    str = "Ju^z od progu ch^l^od emanuj^acy ze ^scian skrytych w mroku "+
        "ogarnia twe cia^lo. Jedynym ^xr^od^lem ^swiat^la jest wielki, "+
        "zdobiony znicz stoj^acy na marmurowym o^ltarzu, zwie^nczony "+
        "strzelaj^acym wysoko p^lomieniem. ";
    if(jest_rzecz_w_sublokacji(0,"d^luga d^ebowa ^lawa"))
    {
        str += "Poza o^swietlonym kr^egiem dostrzec mozna jedynie mroczne "+
            "kontury ^law";
        if(jest_rzecz_w_sublokacji(0,"kl^ecznik"))
        {
            str += "i kl^ecznik^ow. ";
        }
        else
        {
            str += ". ";
        }
    }
    else
    {
        if(jest_rzecz_w_sublokacji(0,"kl^ecznik"))
        {
            str += "Poza o^swietlonym kr^egiem dostrzec mo^zna jedynie "+
                "mroczne kontury kl^ecznik^ow. ";
        }
    }
    str += "Nie ma tu okien lub zosta^ly one szczelnie zas^loni^ete, przez "+
        "co czas wydaje si^e sta^c w miejscu.\n";
    return str;
}

int przyjmowanie(string arg);

void
init()
{
    ::init();
    init_guild_support();
    add_action(przyjmowanie, "przyjmij");
}

private static string
nazwa_pliku(object ob)
{
    return MASTER_OB(ob);
}

int
przyjmowanie(string arg)
{
    if(TP->query_ranga_gildiowa(GUILD_NAME) < 7)
        return 0;
    
    NF("Kogo chcesz przyj��?\n");
    
    if(!arg)
        return 0;
    
    object *c;
    if(!parse_command(arg, all_inventory(ENV(TP)), "%l:" + PL_BIE, c))
        return 0;
    
    if(!sizeof(c))
        return 0;
    
    c = NORMAL_ACCESS(c, 0, 0);
    
    if(!sizeof(c))
        return 0;
    
    if(sizeof(c) > 1)
    {
        NF("Nie mo�esz przyjmowa� kilku os�b r�wnocze�nie.\n");
        return 0;
    }
    
    if(!interactive(c[0]))
    {
        NF("Nie mo�esz go przyj�� do zakonu.\n");
        return 0;
    }
    
    if(c[0]->query_ranga_gildiowa(GUILD_NAME))
    {
        NF("Nie mo�esz przyj�� " + COMPOSITE_LIVE(c, PL_DOP) + " poniewa� "+
            "jest on ju� cz�onkiem zakonu.\n");
        return 0;
    }
    
    if(member_array(KARMAZYNOWY_MIECZ, map(all_inventory(TP), nazwa_pliku)) == -1)
    {
        NF("Musisz mie� przy sobie Karmazynowy Miecz aby m�c rozpocz�� ceremoni�.\n");
        return 0;
    }  
    
    int i;
    object kaplan;
    if((i = member_array(KAPLAN_ZAKONU, map(all_inventory(ENV(TP)), nazwa_pliku))) == -1)
    {
        NF("Kap�an jest nieobecny. Nie mo�esz przeprowadzi� ceremonii.\n");
        return 0;
    }
    else
        kaplan = all_inventory(ENV(TP))[i];
    
    if(member_array(OBJ_PRZYJMOWANIA, map(all_inventory(ENV(TP)), nazwa_pliku)) != -1)
    {
        NF("W tej chwili przeprowadzana jest ju� ceremonia przyjmowania.\n");
        return 0;
    }
    
    write("Unosz�c d�onie do g�ry rozpoczynasz ceremoni� przyjmowania "+
        COMPOSITE_LIVE(c, PL_DOP) + " na pazia Zakonu Karmazynowych Ostrzy.\n");
    saybb(QCIMIE(TP, PL_MIA) + " unosi d�onie i rozpoczyna ceremoni� "+
        "przyjmowania " + QCOMPDEAD(PL_DOP) + " na pazia Zakonu Karmazynowych Ostrzy.\n");
    
    object przyjm = clone_object(OBJ_PRZYJMOWANIA);
    przyjm->start_process(TP, c[0], kaplan); 
    przyjm->move(TO, 1);
    
    return 1;
}



