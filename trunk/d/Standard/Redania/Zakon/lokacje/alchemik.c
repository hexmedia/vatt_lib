#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <pogoda.h>

#include "dir.h"

inherit ZAKON_STD;

void
create_zakon() 
{
    set_short("Alchemik");
    add_object(ZAKON_DRZWI + "drzwi_z_alchemika");

    add_prop(ROOM_I_INSIDE, 0);
}

string
dlugi_opis()
{
    string str = "Nie powinno ci� tutaj by�, zg�o� b��d, prosz�.\n";
    
    str += "";
    
    return str;
}
