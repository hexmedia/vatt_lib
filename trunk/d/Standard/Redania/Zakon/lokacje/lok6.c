/* Autor: Avard
   Data : 24.04.07
   Opis : Sniegulak */

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <pogoda.h>
#include "dir.h"

inherit ZAKON_STD;

object dywan;

void create_zakon() 
{
    set_short("Korytarz");
    add_exit(ZAKON_LOKACJE + "lok4.c","sw",0,1,0);
    add_exit(ZAKON_LOKACJE + "lok5.c","se",0,1,0);
    add_exit(ZAKON_LOKACJE + "lok7.c","s",0,1,0);
    add_exit(ZAKON_LOKACJE + "kuchnia.c","w",0,1,0);
    add_object(ZAKON_PRZEDMIOTY + "makata");
    dywan = clone_object(ZAKON_PRZEDMIOTY + "dywan");
	dywan->move(TO);
    add_object(ZAKON_PRZEDMIOTY + "lampa_srodek");
    add_object(ZAKON_PRZEDMIOTY + "gong");
    add_object(ZAKON_DRZWI + "drzwi_do_alchemika");
    dodaj_rzecz_niewyswietlana("gruba we^lniana makata",1);
    dodaj_rzecz_niewyswietlana("gruby we^lniany dywan",1);
    dodaj_rzecz_niewyswietlana("miedziany wypolerowany gong",1);
	
	add_sit("na pod�odze","na pod�odze","z pod�ogi",0);
}

public string
exits_description() 
{
    return "Korytarz prowadzi na po^ludniowy-zach^od i po^ludniowy-wsch^od, "+
        "na zachodzie wida^c kuchni^e, znajduj^a si^e tu tak^ze drzwi do "+
        "alchemika, za^s na po^ludniu mo^zna dostrzec pomieszczenie ze "+
        "schodami.\n";
}

string
dlugi_opis()
{
    string str;
    str = "Przebywasz obecnie w komnacie przechodniej, z zachodu dochodz^a "+
        "ci^e smakowite zapachy, na wschodzie wida^c ma^le, drewniane drzwi,"+
        " zza kt^orych dochodzi wo^n zi^o^l, za^s kieruj^ac swe kroki "+
        "korytarzami prowadz^acymi na po^ludniowy wsch^od i po^ludniowy "+
        "zach^od dojdziesz do wyj^scia. ^Sciany, tak jak i w innych "+
        "komnatach, pokryte s^a grubymi makatami, maj^acymi za zadanie "+
        "ociepla^c pomieszczenie. W ka^zdym rogu pokoju, na drewnianych "+
        "stojakach zawieszono lampy oliwne. ";
    if(present(dywan,TO))
    {
		if(dywan->query_prop("_siedzonko"))
		{
			str += "Pod^log^e korytarza przykryto dywanem w kolorze ciemnej "+
            "czerwieni, kt^ory wycisza kroki st^apaj^acych po nim os^ob.";
		}
		else
		{
			str += "Na pod�odze korytarza le�y zwini�ty dywan w kolorze "+
			"ciemnej czerwieni.";
		}
    }
    str += "Po ^srodku sali stoi du^zych rozmiar^ow, miedziany gong. Na "+
        "po^ludniu mo^zesz zauwa^zy^c fragment schod^ow prowadz^acych na "+
        "g^or^e.\n";
    return str;
}

