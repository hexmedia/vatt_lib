/* Autor: Avard
   Data : 24.04.07
   Opis : Sniegulak */

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <pogoda.h>
#include "dir.h"

inherit ZAKON_STD;

void create_zakon() 
{
    set_short("Przy schodach");

    add_exit(ZAKON_LOKACJE + "lok6.c","n",0,1,0);
    add_exit(ZAKON_LOKACJE + "lok8.c",({({"u","schody"}),"po schodach"}),0,1,0);
    add_object(ZAKON_PRZEDMIOTY + "lampa_srodek");
    add_item(({"schody"}),"Szerokie drewniane schody prowadz^ace w g^or^e, "+
        "zosta^ly zrobione z sosnowych desek zbitych ze sob^a du^zymi "+
        "gwo^xdziami. Stopnie szerokie na cztery ^lokcie i g^l^ebokie na "+
        "dwie stopy, nawracaj^a w po^lowie wysoko^sci i dalej pn^a si^e w "+
        "g^or^e. Zamiecione i nasmarowane pszczelim woskiem wygl^adaj^a na "+
        "bardzo zadbane.\n");
		
	add_sit("na pod�odze","na pod�odze","z pod�ogi",0);
}

public string
exits_description() 
{
    return "Na p^o^lnocy znajduje si^e jakie^s pomieszczenie, tutaj za^s "+
        "wida^c schody prowadz^ace na g^or^e.\n";
}

string
dlugi_opis()
{
    string str;
    str = "Znajdujesz si^e w ma^lym, s^labo o^swietlonym pomieszczeniu, "+
        "kt^ore w ca^lo^sci zajmuj^a szerokie na cztery ^lokcie schody "+
        "prowadz^ace w g^or^e. Mrok roz^swietlaj^a proste lampy olejne, "+
        "zawieszone na por^eczy, a go^le ^sciany bez jakichkolwiek ozd^ob "+
        "ziej^a pustk^a. Patrz^ac na p^o^lnoc mo^zesz zauwa^zy^c du^zy, "+
        "jasno o^swietlony pok^oj, b^ed^acy najprawdopodobniej "+
        "pomieszczeniem przechodnim do innych cz^e^sci budynku.\n";
    return str;
}

