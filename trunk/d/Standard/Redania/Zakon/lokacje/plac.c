/* Autor: Avard
   Data : 24.04.07
   Opis : Sniegulak 
   Info : POPRAWIC OPIS STOJAKOW W LONGU */

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <pogoda.h>
#include "dir.h"

inherit ZAKON_STD_D;

void create_zakon() 
{
    set_short("Plac treningowy");
    add_exit(ZAKON_LOKACJE + "lok2.c","e",0,1,0);

    add_item(({"budynek","stajni^e"}),"@@stajnia@@");
    add_item(({"drugi budynek","wi^ekszy budynek","dwupi^etrowy budynek",
        "fort","warowni^e"}),"@@warownia@@");
    
    if(pora_roku() == MT_ZIMA)
    {
        add_item(({"zasp^e","^snie^zn^a zasp^e"}),"Wielki zwa^l ubitego "+
            "^sniegu poznaczony ^lopatami i szuflami przes^lania "+
            "cz^e^sciowo znajduj^acy si^e za nim budynek.\n");
    }
    add_item(({"wa^l","wa^l obronny","palisad^e","mury","mur"}),"Wysoka na "+
        "dziesi^e^c ^lokci palisada ma za zadanie chroni^c teren "+
        "znajduj^acy si^e za ni^a przed atakami pieszych jednostek. "+
        "Drzewa u^zyte do jej budowy zosta^ly ociosane z ga^l^ezi, "+
        "potem zaostrzone u g^ory, a nast^epnie g^l^eboko wkopane w "+
        "pod^lo^ze.\n");
    add_object(ZAKON_PRZEDMIOTY + "lampa_uliczna");
	
	add_sit("na ziemi","na ziemi","z ziemi",0);
}
public string
exits_description() 
{
    return "Na wschodzie wida^c dalsz^a cz^e^s^c dziedzi^nca.\n";  
}
string
dlugi_opis()
{
    string str;
    if(jest_dzien() == 1)
    {
        if(pora_roku() != MT_ZIMA)
        {
            str = "Nier^owne p^lyty z szarego piaskowca pokrywaj^a ca^l^a "+
                "powierzchni^e placu rozci^agaj^acego si^e pomi^edzy sporym,"+
                " krytym strzech^a budynkiem na po^ludniu, a drugim, du^zo "+
                "wi^ekszym, a najpewniej pi^etrowym na p^o^lnocy. W kilku "+
                "miejscach placu dostrzegasz na bruku brunatne plamy.\n";
        }
        else
        {
            str = "Nier^owne p^lyty z szarego piaskowca wyzieraj^a spod "+
                "cieniutkiej warstewki ^swierzego ^sniegu. Du^ze jego "+
                "zwa^ly pi^etrz^a si^e pod ^scian^a sporego, krytego "+
                "strzech^a budynku na po^ludniu. Po przeciwnej stronie "+
                "plac ogranicza drugi, du^zo wi^ekszy, a najpewniej "+
                "pi^etrowy. \n";
        }
    }
    else
    {
        if(pora_roku() != MT_ZIMA)
        {
            str = "Du^zy plac tonie w mroku i ciszy. Nier^owne p^lyty z "+
                "szarego piaskowca, pokrywaj^a ca^l^a jego powierzchni^e "+
                "od sporego, nieo^swietlonego budynku na po^ludniu, po "+
                "drugi, kt^orego wielki czarny kontur wyrasta na p^o^lnocy. "+
                "Jedynie w kilku oknach wida^c md^le ^swiat^la.\n";
        }
        else
        {
            str = "Du^zy plac tonie w mroku i ciszy. Nier^owne p^lyty z "+
                "szarego piaskowca, wyzieraj^a spod cienkiej warstwy "+
                "^sniegu powierzchni^e od wiekiej zaspy przes^laniaj^acej "+
                "spory budynek na po^ludniu, po drugi, kt^orego wielki "+
                "czarny kontur wyrasta na p^o^lnocy. Jedynie w kilku "+
                "oknach wida^c md^le ^swiat^la. \n";
        }
    }
    return str;
}
string
stajnia()
{
    string str;
    if(jest_dzien() == 1)
    {
        if(pora_roku() == MT_ZIMA)
        {
            str = "Za wielim zwa^lem ^sniegu wznosi si^e drugi szczyt, "+
                "dach du^zego, drewnianego budynku cz^e^sciowo skrytego "+
                "za zasp^a.\n";
        }
        else
        {
            str = "Du^zy, kryty strzech^a budynek zbity jest z "+
                "nieokorowanych bali. Z pod powa^ly co chwila wylatuj^a, "+
                "lub wlatuj^a tam jask^o^lki.\n";
        }
    }
    else
    {
        if(pora_roku() == MT_ZIMA)
        {
            str = "Zza wielkiego zwa^lu ^sniegu dostrzec mo^zna jedynie "+
                "mroczny kontur dachu przes^laniaj^acy granatowe niebo.\n";
        }
        else
        {
            str = "Du^zy, wroczny kontur budowli przes^lania cz^e^sc gwiazd "+
                "po^ludniowego niebosk^lonu. Najmniejsze ^swiate^lko nie "+
                "ukazuje si^e na jego tle.\n";
        }
    }
    return str;
}
string
warownia()
{
    string str;
    if(jest_dzien() == 1)
    {
        str =  "Wysoka, pi^etrowa budowla z grubych d^ebowych bali g^oruje "+
            "ponad placem i ca^l^a okolic^a. Nad p^laskim dachem wznosi "+
            "si^e maszt, lecz obecnie bez ^zadnych sztandar^ow, czy "+
            "proporc^ow. W^askie okienka uieszczono o wiele za wysoko by "+
            "zajrze^c nimi do wn^etrza.\n";
    }
    else
    {
        str = "Wielka czarna bry^la g^oruje nad placem i ca^l^a okolic^a. "+
            "Jedynym co wida^c na jego powierzchni jest kilka w^askich "+
            "okienek z kt^orych s^aczy si^e md^le ^swiate^lko.\n";
    }
    return str;
}