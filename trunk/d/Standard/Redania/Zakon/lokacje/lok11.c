/* Autor: Avard
   Data : 24.04.07
   Opis : Sniegulak */

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <pogoda.h>
#include "dir.h"

inherit ZAKON_STD;

void create_zakon() 
{
    set_short("Korytarz");
    add_exit(ZAKON_LOKACJE + "lok12.c","sw",0,1,0);
    add_exit(ZAKON_LOKACJE + "lok9.c","nw",0,1,0);
    add_object(ZAKON_PRZEDMIOTY + "lampa_srodek");
    add_object(ZAKON_PRZEDMIOTY + "makata");
    dodaj_rzecz_niewyswietlana("gruba we^lniana makata",1);
	
	add_sit("na pod�odze","na pod�odze","z pod�ogi",0);
}

public string
exits_description() 
{
    return "Korytarz prowadzi na p^o^lnocny-zach^od "+
        "oraz na po^ludniowy-zach^od.\n";
}

string
dlugi_opis()
{
    string str;
        str = "Znajdujesz si^e w s^labo o^swietlonym korytarzu ^l^acz^acym "+
        "p^o^lnocn^a cz^e^s^c budynku z przeciwleg^l^a, po^ludniow^a. "+
        "Przej^scie to jest tak w^askie, ^ze ledwo mog^a si^e tu zmie^sci^c "+
        "dwie id^ace obok siebie osoby. Na ^scianach pokrytych br^azowymi, "+
        "grubymi i we^lnianymi makatami, kt^orych zadaniem jest ociepla^c "+
        "nieco to pomieszczenie, zawieszono dodatkowo daj^ace do^s^c s^labe "+
        "^swiat^lo lampy. Patrz^ac na p^o^lnoc i po^ludnie mo^zna "+
        "zauwa^zy^c dwa du^ze pokoje znajduj^ace si^e na ko^ncach "+
        "korytarza. Pod^loga zdrobiona z drewnianych desek, nosi na sobie "+
        "liczne wytarcia, lecz jest schludna i zaimpregnowana.\n";
    return str;
}

