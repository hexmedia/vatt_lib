/*
 * kaplan Zakony Karmazynowych Ostrzy
 * opis: Sniegulak (i tabakista?)
 * zepsu^la faeve
 * dn. 6 maja 2007
 *
 */

#pragma unique

#include <std.h>
#include <ss_types.h>
#include <macros.h>
#include <stdproperties.h>
#include <pl.h>
#include "dir.h"

inherit ZAKON_STD_NPC;

int czy_walka();
int walka = 0;

void
create_zakon_humanoid()
{
    ustaw_odmiane_rasy(PL_MEZCZYZNA);
    set_living_name("eriel");
    set_gender(G_MALE);
    ustaw_imie(({"eriel","eriela","erielowi","eriela","erielem","erielu"}),
        PL_MESKI_OS);
    set_title(", Kap^lan Zakonu Karmazynowych Ostrzy");
    dodaj_nazwy(({"kap^lan","kap^lana","kap^lanowi","kap^lana","kap^lanem",
        "kap^lanie"}), ({"kap^lani","kap^lan^ow","kap^lanom","kap^lan^ow",
        "kap^lanami","kap^lanach"}), PL_MESKI_OS);

    set_long("Pogr^a^zony w modlitwie cz^lowiek, przykurczony na kl^eczniku "+
        "by^lby ca^lkowicie zamar^ly gdyby nie bezg^lo^sne poruszenia "+
        "widocznych z pod kaptura warg. Ca^le jego cia^lo skryte jest w "+
        "habicie, kt^ory w tym ^swietle wydaje si^e by^c niemal czarny. Z "+
        "oczu, kt^ore pojawiaj^a si^e tylko od czasu do czasu i zatrzymuj^a "+
        "si^e na tobie, bije jaka^s dziwna moc. Jakby m^e^zczyzna po jednym "+
        "spojrzeniu wiedzia^l wszystko o ka^zdym wyst^epku, kt^orego si^e "+
        "dopu^sci^l"+ TP->koncowka("e^s","a^s")+".\n");

    dodaj_przym("zgarbiony","zgarbieni");
    dodaj_przym("zakapturzony","zakapturzeni");

    set_stats (({ 65, 45, 50, 90, 35 }));

    add_prop(CONT_I_WEIGHT, 80000);
    add_prop(CONT_I_HEIGHT, 170);

    set_act_time(30);
    add_act("emote rozgl^ada si^e po otoczeniu.");
    add_act("emote szepcze cicho jakie^s s^lowa, kt^ore nik^lym jak szelest "+
        "echem odbijaj^a si^e od ^scian kaplicy.");
    add_act("emote rzuca ci kr^otkie, wnikliwe spojrzenie spod kaptura. ");
    add_act("emote kl^eka i zaczyna sie modli^c. ");

    set_cchat_time(10);
    add_cchat("Tw^oj wyst^epek zostanie zauwa^zony przez Wieczny Ogie^n! ");
    add_cchat("Naruszasz spok^oj tego ^swi^etego miejsca! ");
    add_cchat("Przeklinam ci^e heretyku! ");

    add_armour(ZAKON_UBRANIA + "sandaly/jasne_drewniane_Lc.c");
    add_armour(ZAKON_UBRANIA + "habity/prosty_ciemnoczerwony_Lc.c");

    add_ask(({"zakon"}), VBFC_ME("pyt_o_zakon"));
    add_ask(({"wiar^e"}), VBFC_ME("pyt_o_wiare"));
    add_ask(({"prac^e"}), VBFC_ME("pyt_o_prace"));
    add_ask(({"zadanie"}), VBFC_ME("pyt_o_zadanie"));
    add_ask(({"wieczny ogie^n","ogie^n"}), VBFC_ME("pyt_o_ogien"));
    set_default_answer(VBFC_ME("default_answer"));

    set_alarm_every_hour("co_godzine");
}

void
powiedz_gdy_jest(object player, string tekst)
{
    if (environment(this_object()) == environment(player))
        this_object()->command(tekst);
}

string
pyt_o_zakon()
{
    if(!query_attack())
    {
        set_alarm(1.0, 0.0, "powiedz_gdy_jest", this_player(),
        "powiedz Wieczny Ogie^n chroni nas r^ek^a Zakonu Karmazynowych "+
        "Ostrzy. ");
    }
    else
    {
        set_alarm(1.0,0.0,"powiedz_gdy_jest", this_player(),
            "powiedz Jak wida� mam teraz wa�niejsz� spraw� na g�owie!");
    }
    return "";

}
string
pyt_o_wiare()
{
    if(!query_attack())
    {
    set_alarm(1.0, 0.0, "powiedz_gdy_jest", this_player(),
        "powiedz Je^sli naprawd^e wierzysz, a serce masz czyste wiara "+
        "pomo^ze przezwyci^e^zy^c ci ka^zde z^lo. ");
    }
    else
    {
        set_alarm(1.0,0.0,"powiedz_gdy_jest", this_player(),
            "powiedz Jak wida� mam teraz wa�niejsz� spraw� na g�owie!");
    }
    return "";

}

string
pyt_o_prace()
{
    if(!query_attack())
    {
    set_alarm(1.0, 0.0, "powiedz_gdy_jest", this_player(),
        "powiedz Pracuj nad swoj^a dusz^a "+TP->koncowka("synu","c�rko")+".");
    }
else
    {
        set_alarm(1.0,0.0,"powiedz_gdy_jest", this_player(),
            "powiedz Jak wida� mam teraz wa�niejsz� spraw� na g�owie!");
    }
    return "";

}

string
pyt_o_zadanie()
{
    if(!query_attack())
    {
    set_alarm(1.0, 0.0, "powiedz_gdy_jest", this_player(),
        "powiedz Pan stawia przed nami r^o^zna zadania. ");
    }
    else
    {
        set_alarm(1.0,0.0,"powiedz_gdy_jest", this_player(),
            "powiedz Jak wida� mam teraz wa�niejsz� spraw� na g�owie!");
    }
    return "";
}
pyt_o_ogien()
{
    if(!query_attack())
    {
    set_alarm(1.0, 0.0, "powiedz_gdy_jest", this_player(),
        "powiedz P^lomie^n wiary i nadziei tli si^e w ka^zdej istocie. "+
        "Trzeba go tylko odnale^x^c.  ");
    }
    else
    {
        set_alarm(1.0,0.0,"powiedz_gdy_jest", this_player(),
            "powiedz Jak wida� mam teraz wa�niejsz� spraw� na g�owie!");
    }
    return "";
}
string
default_answer()
{
    if(!query_attack())
    {
    switch(random(2))
    {
    case 0:set_alarm(2.0, 0.0, "powiedz_gdy_jest", this_player(),
        "':szeptem: Oprzyj nadziej^e w Wiecznym Ogniu. On wska^ze "+
        "ci drog^e. "); break;
    case 1:set_alarm(2.0, 0.0, "powiedz_gdy_jest", this_player(),
        "':szeptem: Odpowiedzi szukaj w swojej duszy. "); break;
    }
    }
    else
    {
        set_alarm(1.0,0.0,"powiedz_gdy_jest", this_player(),
            "powiedz Jak wida� mam teraz wa�niejsz� spraw� na g�owie!");
    }
    return "";
}

void
add_introduced(string imie_mia, string imie_bie)
{
    set_alarm(2.0, 0.0, "return_introduce", find_player(imie_mia));
}
void
return_introduce(object ob)
{
    command("przedstaw sie " + OB_NAME(ob));

}

void
emote_hook(string emote, object wykonujacy)
{
    switch (emote)
    {
        case "kopnij": set_alarm(1.5, 0.0, "nienajlepszy", wykonujacy);
                    break;
        case "spoliczkuj": set_alarm(1.5, 0.0, "nienajlepszy", wykonujacy);
                    break;
        case "opluj" : set_alarm(2.5, 0.0, "nienajlepszy", wykonujacy);
                    break;
        case "poca^luj":
              {
              if(wykonujacy->query_gender() == 1)
                  {
              switch(random(3))
                        {
                 case 0:command("':szeptem: Niestety musz^e "+
                     "zachowa^c czysto^s^c cia^la i umys^lu. "); break;
                 case 1:command("popatrz spokojnie na "+OB_NAME(wykonujacy));
                        break;
                 case 2:command("popatrz z usmiechem na "+
                        OB_NAME(wykonujacy));
                        break;
                        }
                  }
                     else
                  {
                   switch(random(3))
                        {
                   case 0:command("':szeptem: B^l^adzisz w "+
                       "ciemno^sciach."); break;
                   case 1:command("':szeptem: Oby Wieczny Ogie^n "+
                       "wskaza^l ci w^la^sciw^a drog^e. ");
                            break;
                   case 2:command("spojrzyj z obrzydzeniem na "+
                        OB_NAME(wykonujacy));
                            break;
                        }
                  }


               }
               break;
        case "przytul": set_alarm(2.5, 0.0, "malolepszy", wykonujacy);
                   break;
        case "pog^laszcz": set_alarm(2.5, 0.0, "malolepszy", wykonujacy);
                   break;
    }
}

void
malolepszy(object wykonujacy)
{
    if(wykonujacy->query_gender() == 1)
           switch(random(2))
           {
            case 0: set_alarm(0.5, 0.0, "powiedz_gdy_jest", wykonujacy,
                "':szeptem: Nie po^z^adam cielesnych "+
                "przyjemno^sci. "); break;
            case 1: set_alarm(0.5, 0.0, "powiedz_gdy_jest", wykonujacy,
                "':szeptem: Lepiej odejd^x st^ad czym "+
                "pr^edzej."); break;
           }
           else
            {
               switch(random(2))
                {
                   case 0: set_alarm(0.5, 0.0, "powiedz_gdy_jest", wykonujacy,
                    "':szeptem: Grzeszysz bracie. "); break;
                   case 1: set_alarm(0.5, 0.0, "powiedz_gdy_jest", wykonujacy,
                       "':szeptem: Lepiej odejd^x "+
                       "st^ad czym pr^eczej."); break;
                }
            }

}

void
nienajlepszy(object kto)
{
   switch(random(3))
   {
      case 0: set_alarm(0.5, 0.0, "powiedz_gdy_jest", kto,
          "':spokojnym, ale mocnym g^losem: Nie podno^s r^eki na "+
          "s^lug^e Wiecznego Ognia. ");
              break;
      case 1: set_alarm(0.5, 0.0, "powiedz_gdy_jest", kto,
              "':stanowczo: Chroni mnie Wieczny Ogie^n, tarcz^a "+
          "m^a jest i opiekunem. ");
              break;
      case 2: set_alarm(0.5, 0.0, "powiedz_gdy_jest", kto,
          "powiedz Burzysz spok^oj tego miejsca, odejd^x czym "+
          "pr^edzej.");
              break;
   }
}

void attacked_by(object wrog)
{
    if(walka == 0)
    {
        walka = 1;
        set_alarm(0.5,0.0,"command","krzyknij Zak^l^ocasz spok^oj "+
            "^swi^etego miejsca! Zaraz tego po^za^lujesz!");
        set_alarm(15.0, 0.0, "czy_walka", 1);
        return ::attacked_by(wrog);
    }
    else
    {
        return ::attacked_by(wrog);
    }
}
int
czy_walka()
{
    if(!query_attack())
    {
        set_alarm(0.5,0.0,"command","powiedz Niech tak b^edzie.");
        walka = 0;
        return 1;
    }
    else
    {
        set_alarm(15.0, 0.0, "czy_walka", 1);
    }
}

nomask int
query_reakcja_na_walke()
{
    return 1;
}