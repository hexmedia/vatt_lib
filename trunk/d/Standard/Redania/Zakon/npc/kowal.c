/**
 * \file /d/Standard/Redania/Zakon/npc/kowal.c
 *
 * kowal zakonny
 * opis by Sniegulak
 * ma^le poprawki wprowadzi^la i zakodowa^la Faevka.
 * 5.5.2007
 */

#pragma unique
#pragma strict types

#include <macros.h>
#include <stdproperties.h>
#include <money.h>
#include <pl.h>
#include <formulas.h>
#include <std.h>
#include <ss_types.h>
#include <wa_types.h>
#include <composite.h>
#include <filter_funs.h>
#include <pattern.h>
#include "dir.h"

inherit ZAKON_STD_NPC;
inherit "/lib/craftsman.c";

int czy_walka();
int walka = 0;

#define WZORY (ZAKON + "wzory/")
#define REP_MAT "/d/Standard/items/materialy/"

void
create_zakon_humanoid()
{
    set_living_name("thor");

    ustaw_odmiane_rasy("krasnolud");

    dodaj_nazwy(({"kowal", "kowala", "kowalowi", "kowala",
        "kowalem", "kowalu"}),
        ({"kowale", "kowali", "kowalom", "kowali",
        "kowalami", "kowalach"}), PL_MESKI_OS);

    set_surname("Verthan");
    set_origin("z Mahakamu");

    set_gender(G_MALE);
    set_title(", Kowal Zakonny");
    ustaw_imie(({"thor","thora","thorowi","thora", "thorem","thorze"}),PL_MESKI_OS);
    set_long("Krasnolud, na kt^orego patrzysz jest bardzo masywnej budowy. "+
        "Przy ka^zdym ruchu mi^e^snie na jego ramionach t^e^zej^a, a na "+
        "twarzy pojawia si^e lekki grymas. B^lyszcz^acy wzrok, i spokojny "+
        "oddech ^swiadczy o pe^lnym zaanga^zowaniu rzemie^slnika, nie zwraca "+
        "on najmniejszej uwagi na otoczenie. Pewne ruchy, i precyzja, z "+
        "kt^or^a wykonuje ka^zdy ruch ^swiadczy i^z nale^zy on do mistrz^ow "+
        "w swoim fachu. Na sobie ma lekka lniana koszule bez r^ekaw^ow, "+
        "brunatne spodnie, par^e sanda^l^ow, i sk^orzany fartuch, chroni^acy "+
        "jego cia^lo przed oparzeniami.\n");
    dodaj_przym("masywny","masywni");
    dodaj_przym("pracowity","pracowici");

    set_stats ( ({ 120+random(20), 80+random(30), 95+random(35), 50, 84+random(20) }) );
    set_skill(SS_DEFENCE, 55 + random(4));
    set_skill(SS_WEP_CLUB, 38 + random(10));
    set_skill(SS_PARRY, 15 + random(10));
    set_skill(SS_UNARM_COMBAT, 55 + random(15));
    set_skill(SS_CRAFTING_BLACKSMITH, 80);

    add_armour(ZAKON_UBRANIA + "spodnie/ciemnobrazowe_skorzane_XLk.c");
    add_armour(ZAKON_UBRANIA + "koszule/szara_lniana_XLk.c");
    add_armour(ZAKON_UBRANIA + "fartuchy/skorzany_dlugi_XLc.c");
    add_armour(ZAKON_UBRANIA + "sandaly/jasne_drewniane_XLk.c");
    add_weapon(ZAKON_BRONIE + "mloty/duzy_ciezki.c");

    set_act_time(5);
    add_act("@@kowal_pracuje@@");

    /*set_cchat_time(10);
    add_cchat("krzyknij Rycerze na pomoc!");
    add_cchat("krzyknij Mnie? Ju^z ja ci poka^ze!");
    add_cchat("krzyknij Niez^la bitka, jak za dawnych czas^ow!");*/

    add_ask(({"ku^xni^e"}),VBFC_ME("pyt_o_kuznie"));
    add_ask(({"zbroje"}),VBFC_ME("pyt_o_zbroje"));
    add_ask(({"bronie"}),VBFC_ME("pyt_o_bronie"));
    add_ask(({"prace"}),VBFC_ME("pyt_o_prace"));
    add_ask(({"zadanie"}),VBFC_ME("pyt_o_zadanie"));
    set_default_answer(VBFC_ME("default_answer"));

    add_prop(NPC_I_NO_RUN_AWAY, 1);
    add_prop(CONT_I_WEIGHT, 70000);
    add_prop(CONT_I_HEIGHT, 150);

    config_default_craftsman();
    craftsman_set_domain(PATTERN_DOMAIN_BLACKSMITH);
    craftsman_set_sold_item_patterns( ({
                WZORY + "miecz_dlugi_stalowy",
                WZORY + "mlot_maly_prosty",
                WZORY + "sztylet_krotki_zakrzywiony",
                WZORY + "przeszywanica_zolta_dluga",
                WZORY + "przeszywanica_czerwona_pikowana",
                WZORY + "przeszywanica_niebieska_gruba",
                WZORY + "sztylet_poreczny_trojkatny",
                WZORY + "buty_mocne_wysokie",
                WZORY + "kaptur_obszerny_kolczy",
                WZORY + "kirys_stalowy_blyszczacy",
                WZORY + "kolczuga_luskowa_rycerska",
                WZORY + "naramienniki_czernione_lekkie",
                WZORY + "spodnie_lekkie_kolcze",
                WZORY + "szyszak_lekki_stalowy",
                WZORY + "topor_poreczny_zdobiony",
                WZORY + "helm_ciezki_garnczkowy",
                WZORY + "nagolenniki_rynienkowe_lekkie",
                WZORY + "zbroja_gruba_lamelkowa",
                WZORY + "koszulka_krotka_kolcza",
                WZORY + "brygantyna_blyszczaca_ciezka"
                }) );

    set_money_greed_buy(210);
    set_money_greed_sell(541);

    set_money_greed_change(102);

    set_store_room(ZAKON_LOKACJE + "magazyn_kowala");
}

void
init()
{
    ::init();
    craftsman_init();
}

public string
query_auto_load()
{
    return ::query_auto_load() + query_craftsman_auto_load();
}


string
kowal_pracuje()
{
    if (craftsman_is_busy())
    {
        switch (random(4))
        {
            case 0: return "emote przeciera r^ek^a spocon^a twarz.";
            case 1: return "emote mocnymi ruchami uderza w rozgrzany do "+
                "czerwono^sci kawa^lek metalu.";
            case 2: return "emote wsadza rozgrzany do czerwono^sci pr^et do "+
                "beczki z olejem. ";
            case 3: return "emote mruczy co^s pod nosem, a nast^epnie "+
                "zaczyna ostrzy^c bro^n. ";
        }
    }
    return "";
}

void
emote_hook(string emote, object wykonujacy)
{
    switch (emote)
    {
        case "kopnij": set_alarm(1.5, 0.0, "nienajlepszy", wykonujacy);
                    break;
        case "spoliczkuj": set_alarm(1.5, 0.0, "nienajlepszy", wykonujacy);
                    break;
        case "opluj" : set_alarm(2.5, 0.0, "nienajlepszy", wykonujacy);
                    break;
        case "poca^luj":
              {
              if(wykonujacy->query_gender() == 1)
                switch(random(2))
                 {
                 case 0:command("powiedz Poca^lunek od pi^eknej Panny zawsze "+
                     "mile widziany!"); break;
                 case 1:command("emote u^smiecha si^e szelmowsko, i puszcza "+
                     "do ciebie oczko."); break;
                }
              else
                {
                   switch(random(2))
                   {
                   case 0:command("powiedz Kruca! Nie ca^luj! "); break;
                   case 1:command("powiedz Facet... Faceta? No co^s ty, "+
                       "upi^l si^e?"); break;
                   }
                }

                break;
                }
        case "prychnij":
              {
                switch(random(1))
                 {
                 case 0:command("spojrzyj lekcewazaco "+
                     "na "+ OB_NAME(wykonujacy)); break;
                 }
                break;
               }
        case "przytul": set_alarm(0.5, 0.0, "malolepszy", wykonujacy);
                   break;
        case "pog^laszcz": set_alarm(0.5, 0.0, "malolepszy", wykonujacy);
                   break;
    }
}

void
malolepszy(object wykonujacy)
{
    switch(random(2))
    {
        case 0:command("powiedz To^c g^laskanie mi^le ale na pracy musz^e "+
            "si^e skupi^c."); break;
        case 1:command("emote wzdycha: ^Lapy przy sobie zbere^xniku!"); break;
    }
}

void
nienajlepszy(object wykonujacy)
{
  switch(random(3))
    {
    case 0:command("powiedz Bo, kruca, zawo^lam Zakonnych!");
      break;
    case 1:
      command("emote czerwieni si^e na twarzy robi krok w ty^l i m^owi "+
        "dobitnie: Ciebie ju^z nie obs^lu^z^e."); break;
    case 2:
      command("popatrz z poblazaniem na "+OB_NAME(wykonujacy));
      break;
    }
}
void
powiedz_gdy_jest(object player, string tekst)
{
    if (environment(this_object()) == environment(player))
        this_object()->command(tekst);
}
string
pyt_o_kuznie()
{
    if(!query_attack())
    {
        set_alarm(1.0, 0.0, "powiedz_gdy_jest", this_player(),
            "powiedz Tak jestem z mej ku^xni bardzo dumny. ");
    }
    else
    {
         set_alarm(0.5,0.0,"powiedz_gdy_jest", TP, "powiedz Zostaw mnie w "+
            "spokoju!");
    }
    return "";
}

string
pyt_o_zbroje()
{
    if(!query_attack())
    {
        set_alarm(1.0, 0.0, "powiedz_gdy_jest", this_player(),
        "powiedz W mej ku^xni wykonuje przer^o^zne zbroje.  ");
    }
    else
    {
         set_alarm(0.5,0.0,"powiedz_gdy_jest", TP, "powiedz Zostaw mnie w "+
            "spokoju!");
    }
    return "";
}

string
pyt_o_bronie()
{
    if(!query_attack())
    {
        set_alarm(1.0, 0.0, "powiedz_gdy_jest", this_player(),
        "powiedz W mej ku^xni wykonuje przer^o^zne bronie. ");
    }
    else
    {
         set_alarm(0.5,0.0,"powiedz_gdy_jest", TP, "powiedz Zostaw mnie w "+
            "spokoju!");
    }
    return "";
}

string
pyt_o_prace()
{
    if(!query_attack())
    {
        set_alarm(1.0, 0.0, "powiedz_gdy_jest", this_player(),
        "powiedz Nie potrzebuj^e czeladnika! ");
    }
    else
    {
         set_alarm(0.5,0.0,"powiedz_gdy_jest", TP, "powiedz Zostaw mnie w "+
            "spokoju!");
    }
    return "";
}

string
pyt_o_zadanie()
{
    if(!query_attack())
    {
        set_alarm(1.0, 0.0, "powiedz_gdy_jest", this_player(),
        "powiedz Tak, mo^zesz mi pomoc... Id^x i wysprz^ataj dziedziniec. ");
    }
    else
    {
         set_alarm(0.5,0.0,"powiedz_gdy_jest", TP, "powiedz Zostaw mnie w "+
            "spokoju!");
    }
    return "";
}

string
default_answer()
{
    if(!query_attack())
    {

     switch(random(5))
     {
     case 0: set_alarm(1.0, 0.0, "command_present", this_player(),
             "powiedz do "+ this_player()->query_name(PL_DOP) +
                  " Doprawdy... Nie wiem o co ci chodzi...");
             break;

     case 1: set_alarm(1.0, 0.0, "command_present", this_player(),
             "rozloz rece");
             break;
     case 2: set_alarm(1.0, 0.0, "command_present", this_player(),
             "powuedz Mam wa^zniejsze rzeczy do roboty. ");
            break;
    case 3: set_alarm(1.0, 0.0, "command_present", this_player(),
             "wzrusz ramionami");
            break;
     case 4: set_alarm(1.0, 0.0, "command_present", this_player(),
             "pokrec powoli");
             break;
     }
     }
    else
    {
         set_alarm(0.5,0.0,"powiedz_gdy_jest", TP, "powiedz Zostaw mnie w "+
            "spokoju!");
    }
    return "";
}

void attacked_by(object wrog)
{
    if(walka == 0)
    {
        walka = 1;
        set_alarm(0.5,0.0,"command","krzyknij Rycerze! Na pomoc!");
        set_alarm(15.0, 0.0, "czy_walka", 1);
        return ::attacked_by(wrog);
    }
    else
    {
        return ::attacked_by(wrog);
    }
}
int
czy_walka()
{
    if(!query_attack())
    {
        set_alarm(0.5,0.0,"command","krzyknij Kto^s jeszcze ch^etny?");
        walka = 0;
        return 1;
    }
    else
    {
        set_alarm(15.0, 0.0, "czy_walka", 1);
    }
}

public string
init_arg(string arg)
{
    if (arg == 0) {
        force_expand_patterns_items();
    }
    return init_craftsman_arg(::init_arg(arg));
}

nomask int
query_reakcja_na_walke()
{
    return 1;
}
