/* Autor: Avard
   Opis : Sniegulak
   Data : 28.04.07 */
inherit "/std/window";
inherit "/lib/peek";

#include <pl.h>
#include <macros.h>
#include <formulas.h>

#include "dir.h"

void
create_window()
{
    ustaw_nazwe("okno");
    dodaj_przym("du^zy", "duzi");

    set_other_room(ZAKON_LOKACJE + "plac");
    set_window_id("OKNO_W_ZBROJOWNI_ZAKONU_KARMAZYNOWYCH_OSTRZY_OBOK_RINDE");
    set_open(0);
    set_open_desc("");
    set_closed_desc("");
    set_window_desc("Du^za drewniana rama, z dwoma skrzyd^lami, "+
        "wy^lo^zonymi p^lytkami szklanymi pozwala s^lonecznemu "+
        "^swiat^lu na dotarcie do tego pomieszczenia. \n");

    ustaw_pietro(1);

    /**
     * A jak to kurwa ma dzia�a�?:) Ja pierdole ludziska... Kurwa ma�...
     * Ehhh szkoda s��w.. Przy create po pierwsze nie ma tp.. po drugie
     * odbieramy hp jak kto� skacze a nie jak kto�.... No w�a�nie co?
     * wchodzi do pokoju i okno si� �aduje.. nie no.. Musia�bym wypi�
     * jeszcze z 10 piw, �eby si� nie oburzy�....
     * Naprawi�em to ale jak mi kto� jeszcze raz co� takiego zorbi to
     * wypatrosze! [KRUN]
     */
//     this_player()->set_hp(this_player()->query_hp()-90-random(50));

    set_pass_mess("Wyskakujesz przez niedu^ze okno na zewn^atrz.\n");
}