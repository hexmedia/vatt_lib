/* Autor: Avard
   Opis : Sniegulak
   Data : 28.04.07 */

inherit "/std/window";

#include <pl.h>
#include <macros.h>
#include <formulas.h>

#include "dir.h"

void
create_window()
{
    ustaw_nazwe("okno");
    dodaj_przym("du^zy", "duzi");

    set_other_room(ZAKON_LOKACJE + "plac");
    set_window_id("OKNO_W_KWATERZE_ZAKONU_KARMAZYNOWYCH_OSTRZY_OBOK_RINDE");
    set_open(0);
    set_locked(1);
    set_open_desc("");
    set_closed_desc("");
    set_window_desc("Du^za drewniana rama, z dwoma skrzyd^lami, "+
        "wy^lo^zonymi p^lytkami szklanymi pozwala s^lonecznemu "+
        "^swiat^lu na dotarcie do tego pomieszczenia. \n");

    ustaw_pietro(1);

    set_pass_command("okno");
    set_pass_mess("przez niedu^ze okno na zewn^atrz");
}