
/* Autor: Avard
   Data : 24.04.07
   Opis : Sniegulak */

inherit "/std/door";

#include <pl.h>
#include "dir.h"

void
create_door()
{
    ustaw_nazwe("drzwi");
    dodaj_przym("solidny","solidni");
    dodaj_przym("okuty", "okuci");

    set_other_room(ZAKON_LOKACJE + "kwatera.c");
    set_door_id("DRZWI_DO_KWATERY_ZAKONU_KARMAZYNOWYCH_OSTRZY");
    set_door_desc("Masywne drewniane drzwi bez ^zadnych zdobie^n zamykaj^a "+
        "dost^ep do prywatnej kwatery nale^z^acej do Kapitana Zakonu. "+
        "Drzwi wykonane z drewnianych desek obitych metalowymi okuciami "+
        "wygl^adaj^a na solidne i porz^adnie wykonane.\n");

    set_open_desc("");
    set_closed_desc("");

    set_pass_command(({"drzwi","kwatera","prywatna kwatera","pok^oj",
        "kwatera kapitana"}),"przez solidne okute drzwi",
        "wychodz^ac z pokoju");

    set_open(0);
    set_locked(1);

    set_key("KLUCZ_DRZWI_DO_KWATERY_ZAKONU_KARMAZYNOWYCH_OSTRZY");
    set_lock_name(({"zamek", "zamku", "zamkowi", "zamek", "zamkiem", "zamku"}), 0, PL_MESKI_NOS_NZYW);
}