/* Autor: Avard
   Data : 25.04.07
   Info : Klucz do kwatery kapitana Zakonu i jego biurka. */

inherit "/std/key.c";
inherit "/lib/guild_key.c";

#include <pl.h>
#include <macros.h>
#include <stdproperties.h>

#include "dir.h"

void
create_key()
{
    ustaw_nazwe("klucz");
    dodaj_przym("solidny", "solidni");
    dodaj_przym("stalowy", "stalowi");
    set_long("Jest to metalowy klucz o d^lugim j^ezyku powyginanym w r^ozne "+
        "dziwne kszta^lty. Solidna robota i dba^lo^s^c o szczeg^o^ly mog^a "+
        "^swiadczy^c. i^z otwierany nim zamek prawdopodobnie zamyka co^s "+
        "wa^znego i drogocennego.\n");

    ustaw_nazwe_gildii(GUILD_NAME);

    config_guild_key();

    set_key("KLUCZ_DRZWI_DO_KWATERY_ZAKONU_KARMAZYNOWYCH_OSTRZY");
}