/* Autor: Avard
   Data : 25.04.07
   Info : Klucz do zbrojowni zakonu Karmazynowych */

inherit "/std/key.c";
inherit "/lib/guild_key.c";

#include <pl.h>
#include <macros.h>
#include <stdproperties.h>

#include "dir.h"

void
create_key()
{
    ustaw_nazwe("klucz");
    dodaj_przym("du^zy", "duzi");
    dodaj_przym("mosi^e^zny", "mosi^e^zni");
    set_long("Sporych rozmiar^ow mosi^e^zny klucz z dziwnymi z^abkami, "+
        "wykuty przez jakiego^s kowala. Skomplikowana budowa klucza, "+
        "powoduje i^z zamek kt^ory on zamyka, b^edzie nader trudny do "+
        "otworzenia przy u^zyciu wytrych^ow, ma to te^z zapobiec "+
        "dorobieniu kolejnych egzemplarzy przez osoby niepowo^lane.\n");

    set_key("KLUCZ_DRZWI_DO_ZBROJOWNI_ZAKONU_KARMAZYNOWYCH_OSTRZY");

    ustaw_nazwe_gildii(GUILD_NAME);

    config_guild_key();
}