
/* Autor: Avard
   Data : 24.04.07
   Opis : Sniegulak */

inherit "/std/door";

#include <pl.h>
#include "dir.h"

void
create_door()
{
    ustaw_nazwe("drzwi");
    dodaj_przym("gruby","grubi");
    dodaj_przym("solidny", "solidni");

    set_other_room(ZAKON_LOKACJE + "lok12.c");
    set_door_id("DRZWI_DO_ZBROJOWNI_ZAKONU_KARMAZYNOWYCH_OSTRZY");
    set_door_desc("Masywne drewniane drzwi z miedzianymi okuciami, pokryte "+
        "warstewk^a pszczelego wosku zamykaj^a dost^ep niepowo^lanym osobom "+
        "do zbrojowni Zakonu. Drube deski dodatkowo zmocnione stanowi^a "+
        "doskona^le zabezpieczenie, a zamek w drzwiach umo^zliwia "+
        "zamkni^ecie pomieszczenia.\n");

    set_open_desc("");
    set_closed_desc("");

    set_pass_command(({"drzwi","wyj^scie","sala"}),"przez grube "+
        "solidne drzwi","wychodz^ac ze zbrojowni");

    set_key("KLUCZ_DRZWI_DO_ZBROJOWNI_ZAKONU_KARMAZYNOWYCH_OSTRZY");
    set_lock_name(({"zamek", "zamku", "zamkowi", "zamek", "zamkiem", "zamku"}), 0, PL_MESKI_NOS_NZYW);
}