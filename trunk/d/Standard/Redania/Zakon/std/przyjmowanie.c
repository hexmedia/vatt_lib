inherit "/std/object.c";

#include <pl.h>
#include "dir.h"
#include <macros.h>  

object przyjmowany, przyjmujacy, kaplan, envir;

void
create_object()
{
    set_no_show();
}

static private string
nazwa_pliku(object ob)
{
    return MASTER_OB(ob);
}

void next_step(int sn);

void 
next_alarm(int i)
{
    set_alarm((5.0 + itof(random(7))), 0.0, &next_step(i+1));
}

void
next_step(int sn)
{
    if(MASTER_OB(ENV(przyjmowany)) != KAPLICA)
        return; //cicho si� konczy bo uciek�.
    if(MASTER_OB(ENV(przyjmujacy)) != KAPLICA)
        return;
    if(MASTER_OB(ENV(kaplan)) != KAPLICA)
        return;
    
    switch(sn)
    {
        case 0:
            przyjmujacy->catch_msg("Zwracasz sie w stron� kap�ana prosz�c go o wsparcie w prowadzeniu ceremonii.\n");
            tell_roombb(envir, QCIMIE(przyjmujacy, PL_MIA) + " zwraca si� w stron� "+
                QIMIE(kaplan, PL_DOP) + " prosz�c go o wsparcie "+
                "w prowadzeniu ceremonii.\n", ({przyjmujacy}), ({przyjmujacy, kaplan, przyjmowany}));
            next_alarm(0);
            break;
        case 1:
            tell_roombb(envir, QCIMIE(kaplan, PL_MIA) + " kiwa powoli g�ow� i bez "+
                "�adnego s�owa podchodzi kierunku "+ QIMIE(przyjmowany, PL_DOP) + ", "+
                "k�adzie d�onie na jego ramionach i spogl�da mu g��boko w oczy.\n", ({przyjmowany}), 
                ({przyjmujacy, kaplan, przyjmowany}));
            przyjmowany->catch_msg(QCIMIE(kaplan, PL_MIA) + " kiwa powoli g�ow� i bez �adnego "+
                "s�owa podchodzi w Twoim kierunku. Kap�an k�adzie d�onie na Twoich "+
                 "ramionach i spogl�da Ci g��boko w oczy.\n");
            next_alarm(1);
            break;
        case 2:
            tell_roombb(envir, "Wzrok " + QIMIE(przyjmowany, PL_DOP) + " " + 
                "staje sie m�tny, a jego oczy zaczynaj� lekko �zawi�.\n", ({przyjmowany}), 
                ({przyjmujacy, kaplan, przyjmowany}));
            przyjmowany->catch_msg("Przez chwile czujesz mrowienie w g�owie, twoje oczy "+
                "�zawi�, a jaka� dziwna si�a sonduje tw�j umys�. Po chwili wszystko "+
                "zdaje si� wraca� do normy, a ty z lekkim niepokojem na twarzy kierujesz "+
                "sw�j wzrok w stron� ognia �wiec.\n");
            next_alarm(2);
            break;
        case 3:
            kaplan->command("powiedz do " + OB_NAME(przyjmowany) + " Przez ca�e �ycie "+
                "stara�e� sie pod��a� �cie�k� prawa, mo�esz wst�pi� do bractwa Zakonu "+
                "Karmazynowych Ostrzy");
            kaplan->command("emote u�miecha si� nieznacznie, i kiwni�ciem g�owy zezwala" +
                 "na dalsze prowadzenie ceremonii, po czym odchodzi na bok.");
            next_alarm(3);
            break;
        case 4:
            //Kapitan/Mistrz pozby� si� w mi�dzy czasie miecza.. Przerywamy ceremonie.
            int i;
            if((i = member_array(KARMAZYNOWY_MIECZ, map(all_inventory(TP), nazwa_pliku))) == -1)
            {
                przyjmujacy->catch_msg("Musisz mie� przy sobie Karmazynowy Miecz aby m�c "+
                    "poprowadzi� dalej ceremonie.\n");
                return;
            }  
            object miecz = all_inventory(TP)[i];
            
            if(!miecz->query_wielded())
                przyjmujacy->command("dobadz " + OB_NAME(miecz));
            
            przyjmujacy->catch_msg("Podchodzisz do " + QCIMIE(przyjmowany, PL_DOP) + " i z powag� "+
                "na twarzy dotykasz jego ramion p�azem swojego " + QSHORT(miecz, PL_DOP) + " pasujesz go "+
                "na pazia Zakonu Karmazynowych Ostrzy.\n");
            przyjmowany->catch_msg(QCIMIE(przyjmujacy, PL_MIA) + " podchodzi do "+ 
                "Ciebie i powag� na twarzy dotyka Twoich ramion p�azem "+
                QSHORT(miecz, PL_DOP) + " pasuje Ci� na pazia Zakonu Karmazynowych Ostrzy.\n");
            tell_roombb(envir, QCIMIE(przyjmujacy, PL_MIA) + " podchodzi do "+
                QCIMIE(przyjmowany, PL_DOP) + " i z powag� na twarzy dotyka jego ramion "+
                "p�azem " + QSHORT(miecz, PL_DOP) + " pasuj�c go na pazia Zakonu "+
                "Karmazynowych Ostrzy.\n", ({przyjmujacy, przyjmowany}), ({przyjmujacy, przyjmowany}));
            
            przyjmowany->dodaj_gildie(SILNIK_GILDII);
            next_alarm(4);
            break;
        case 5:
            tell_room(envir, "W sali na chwil� jakby zrobi�o si� troszk� ja�niej.\n");
            remove_object();
            break;
        default:
            notify_wizards(TO, "B��D W PRZYJMOWANIU, Za du�o wywo�a� next_step.\n");      
            remove_object();        
    }
    
}

void
start_process(object ob1, object ob2, object ob3)
{
    przyjmujacy = ob1;
    przyjmowany = ob2;
    kaplan      = ob3;
    envir       = ENV(ob1);
    
    set_alarm((10.0 + itof(random(5))), 0.0, &next_step(0));
}