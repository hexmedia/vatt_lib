
#include "dir.h"
#include <macros.h>

inherit "/std/humanoid.c";

void
create_zakon_humanoid()
{
}

nomask void
create_humanoid()
{
    create_zakon_humanoid();
    set_wzywanie_strazy(0);
    set_reakcja_na_walke(1);
    set_spolecznosc("Zakon Karmazynowych Ostrzy");
}

