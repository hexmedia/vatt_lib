#include "dir.h"

inherit REDANIA_STD;

#include <mudtime.h>
#include <language.h>
#include <stdproperties.h>

void
create_zakon()
{
}

nomask void
create_redania()
{
    set_long("@@dlugi_opis@@");
    set_polmrok_long("@@opis_nocy@@");
    add_sit("pod �cian�", "pod �cian�", "spod �ciany", 0);

    add_prop(ROOM_I_TYPE,ROOM_IN_CITY);
    add_prop(ROOM_I_INSIDE,0);
    add_prop(ROOM_I_WSP_Y, WSP_Y_ZAKON);	//bezposrednio na W od Rinde...
    add_prop(ROOM_I_WSP_X, WSP_X_ZAKON);
    set_event_time(500.0);
    add_event("Podmuch wiatru owiewa tw^a twarz.\n");

    set_start_place(0);

    create_zakon();
}

public int
prevent_enter(object ob)
{
#ifdef SLEEP_PLACE
    if(ob->query_gildia(GUILD_NAME))
        ob->set_temp_start_location(SLEEP_PLACE);
#endif SLEEP_PLACE
    return ::prevent_enter(ob);
}