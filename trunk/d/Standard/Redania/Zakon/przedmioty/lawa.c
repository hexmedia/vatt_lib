/* Autor: Avard
   Opis : Sniegulak
   Data : 25.04.07 */

inherit "/std/object";

#include "dir.h"
#include <cmdparse.h>
#include <pl.h>
#include <macros.h>
#include <stdproperties.h>
#include <object_types.h>
#include <materialy.h>

void create_object()
{
    ustaw_nazwe("^lawa");
    dodaj_przym("ciemny", "ciemni");
    dodaj_przym("rze^xbiony", "rze^xbieni");

    set_long("Zrobiona z ciemnego drewna i lekko rze^xbiona wygl^ada "+
        "schludnie i solidnie. Posiada obite materia^lem oparcie co "+
        "zwi^eksza komfort os^ob na niej odpoczywaj^acych. Siedzisko jest "+
        "do^s^c szerokie by mo^zna przysi^a^s^c na nim wygodnie.\n");
    add_prop(OBJ_I_WEIGHT, 5000);
    add_prop(OBJ_I_VOLUME, 5000);
    add_prop(OBJ_I_VALUE, 1240);
    set_type(O_MEBLE);
    ustaw_material(MATERIALY_DR_MAHON);
    make_me_sitable("na", "na ^lawie", "z ^lawy", 2);
    //set_owners(({RINDE_NPC + "krepp"}));TODO FIXME TRALALA ACHTUNG!!
}
