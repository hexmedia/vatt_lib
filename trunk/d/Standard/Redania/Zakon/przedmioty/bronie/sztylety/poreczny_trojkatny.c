/* autor: Valor
   opis: Sniegulak
   wykonane: 06.05.07
*/
inherit "/std/weapon";
#include <macros.h>
#include <stdproperties.h>
#include <wa_types.h>
#include <pl.h>
#include <materialy.h>
#include <object_types.h>
    
void create_weapon() 
{
        ustaw_nazwe("sztylet");
        dodaj_przym("por^eczny", "por^eczni");
        dodaj_przym("tr^ojk^atny", "tr^ojk^atni");
                    
        set_long("Widzisz sztylet o najbardziej typowym kszta^lcie. "+
            "Tr^ojk^atne ostrze szerokie u nasady i bardzo w^askie u "+
            "szczytu wygl^ada na idealne do walki w zwarciu, gdzie jeden "+
            "cios mo^ze przes^adzi^c o wyniku. Bardzo sztywne i precyzyjnie "+
            "wykute bez problemu przebije kolczug^e, a jego d^lugo^s^c "+
            "pozwala nawet na si^egni^ecie cia^la w wypadku walki z "+
            "ci^e^zko opancerzonym przeciwnikiem. Jednoczesny brak jelca "+
            "powoduje i^z parowanie jest niemal niemo^zliwe. Bron ta jest "+
            "bardzo dobrze wywa^zona i pozwala na szybkie i celne "+
            "rzucenie w kierunku wroga.\n");
        
        set_hit(24);
        set_pen(15);
        set_wt(W_KNIFE);
        set_dt(W_IMPALE | W_SLASH);
        set_hands(A_ANY_HAND);
     
        add_prop(OBJ_I_VALUE, 960);
        add_prop(OBJ_I_WEIGHT, 500);
        add_prop(OBJ_I_VOLUME, 500);   
        set_type(O_BRON_SZTYLETY);
        ustaw_material(MATERIALY_STAL, 90);
        ustaw_material(MATERIALY_SK_BYDLE, 10);
}
 