
/* Autor: Avard
   Opis : Sniegulak
   Data : 25.04.07 */
inherit "/std/weapon";

#include "/sys/formulas.h"
#include <stdproperties.h>
#include <wa_types.h>
#include <pl.h>
#include <materialy.h>
#include <object_types.h>

void
create_weapon()
{
    ustaw_nazwe("m^lot");
                    
    dodaj_przym("ma^ly","mali");
    dodaj_przym("prosty","pro^sci");

    set_long("M^lot, kt^ora ogl^adasz z uwag^a jest do^s^c lekka i "+
        "por^eczna. Obuch z jednej strony g^ladki, z drugiej za^s "+
        "rozszerzony zw^e^za si^e w szpikulec. Taka budowa broni pozwala "+
        "na zadawanie ran k^lutych jak i mia^zd^zonych. Trzon broni stanowi "+
        "wyprofilowany kawa^lek d^ebowego drewna,a na ko^ncu zaraz przy "+
        "uchwycie zamocowano sk^orzan^a p^etl^e dzi^eki kt^orej mo^zna "+
        "pewniej chwyci^c bron, i nie ba^c si^e o jej upuszczenie w "+
        "ferworze walki.\n");

    add_prop(OBJ_I_WEIGHT, 1500);
    add_prop(OBJ_I_VOLUME, 1500);
    add_prop(OBJ_I_VALUE, 2400);
   
    set_hit(24);
    set_pen(25);

    set_wt(W_WARHAMMER);
    set_dt(W_BLUDGEON);
    ustaw_material(MATERIALY_STAL, 80);
    ustaw_material(MATERIALY_DR_DAB, 20);
    set_type(O_BRON_MLOTY);

    set_hands(A_ANY_HAND);
}
