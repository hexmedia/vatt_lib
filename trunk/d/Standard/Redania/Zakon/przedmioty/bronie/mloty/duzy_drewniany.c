/* zrobione: Valor
   data: 5 maj 07
   opis: tabakista
*/
inherit "/std/weapon";
#include "/sys/formulas.h"
#include <stdproperties.h>
#include <wa_types.h>
#include <pl.h>
#include <object_types.h>
#include <materialy.h>
void
create_weapon()
{
    ustaw_nazwe("m^lot");
   dodaj_przym("du^zy","duzi");
   dodaj_przym("drewniany","drewniani");
   set_long("Solidna treningowa bro^n wykonana zosta^la z d^ebowego drewna. "+
       "Obuch nie zosta^l nawet ocheblowany, za^s r^ekoje^s^c okr^econo "+
       "sk^or^a zapezpieczon^a przed zsuni^eciem miedzianym drutem.\n");
   set_hit(23);
   set_pen(1);
   set_wt(W_WARHAMMER);
   set_dt(W_BLUDGEON);
   set_hands(W_BOTH);
   add_prop(OBJ_I_WEIGHT, 2000);
   add_prop(OBJ_I_VALUE, 5);
   add_prop(OBJ_I_VOLUME, 2000);
   ustaw_material(MATERIALY_DR_DAB, 100);
   set_type(O_BRON_MLOTY);
}
