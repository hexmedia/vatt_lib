/* autor: Valor
   opis: Sniegulak
   wykonane: 06.05.07
*/
inherit "/std/weapon";
#include <macros.h>
#include <stdproperties.h>
#include <wa_types.h>
#include <pl.h>
#include <materialy.h>
#include <object_types.h>
    
void create_weapon() 
{
        ustaw_nazwe("top^or");
        dodaj_przym("por^eczny", "por^eczni");
        dodaj_przym("zdobiony", "zdobieni");
                    
        set_long("Ci^e^zki jednor^eczny top^or, posiada na g^lowni wyryte "+
            "zdobienia. Stal b^lyszcz^aca i hartowana zosta^la wykonana w "+
            "hutach Mahakamu, nast^epnie ju^z nie pod g^or^a Carbon poddana"+
            "obr^obce kowala, zosta^la u^zyta do wykonania tej broni. "+
            "Ostrze p^o^lokr^ag^le, i ostre niczym brzytwa po^lyskuje przy "+
            "najdrobniejszym ^swietle. Stylisko wykonane z d^ebowego drewna "+
            "oheblowane, a nast^epnie wyszlifowane jest d^lugie na ^lokie^c "+
            "i zako^nczone rzemienn^a p^etl^a. Dodatkowe obicie r^ekoje^sci "+
            "sk^ora powoduje popraw^e chwytu na broni. \n");
        
        set_hit(25);
        set_pen(26);
        set_wt(W_AXE);
        set_dt(W_SLASH);
        set_hands(A_ANY_HAND);
     
        add_prop(OBJ_I_VALUE, 4800);
        add_prop(OBJ_I_WEIGHT, 3000);
        add_prop(OBJ_I_VOLUME, 3000);   
        set_type(O_BRON_TOPORY);
        ustaw_material(MATERIALY_STAL, 80);
        ustaw_material(MATERIALY_SK_BYDLE, 10);
        ustaw_material(MATERIALY_DR_DAB, 10);
}