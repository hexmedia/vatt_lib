/* Autor: Avard
   Opis : Sniegulak
   Data : 25.04.07 */

inherit "/std/container";

#include "dir.h"
#include <cmdparse.h>
#include <pl.h>
#include <macros.h>
#include <stdproperties.h>
#include <object_types.h>
#include <materialy.h>

void create_container()
{
    ustaw_nazwe("st^o^l");
    dodaj_przym("olbrzymi", "olbrzymi");
    dodaj_przym("okr^ag^ly", "okr^agli");

    set_long("Ogromny st^o^l z czarnego d^ebu ozdobiono delikatnymi rytami "+
        "wyobra^zaj^acymi li^scie i owoce winogron. Sze^s^c masywnych n^og, "+
        "r^ownie^z zdobionych, wydaje si^e mimo solidno^sci z trudem "+
        "utrzymywa^c gruby blat, na kt^orego ^srodku znajduje si^e herb "+
        "Zakonu � trzy skrzy^zowane miecze.\n");
    add_prop(OBJ_I_WEIGHT, 25000);
    add_prop(OBJ_I_VOLUME, 25000);
    add_prop(OBJ_I_VALUE, 10000);
    set_type(O_MEBLE);
    add_prop(OBJ_M_NO_GET, "St^o^l jest za ci^e^zki ^zeby go podnie^s^c.\n");
    add_prop(OBJ_I_DONT_SHOW_IN_LONG,1);
    add_subloc("na", 0, "ze");
    add_subloc_prop("na", SUBLOC_I_MOZNA_POLOZ, 1);

    //set_owners(({RINDE_NPC + "krepp"}));TODO FIXME TRALALA ACHTUNG!!
}
