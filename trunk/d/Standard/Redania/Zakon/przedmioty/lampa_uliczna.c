/* Autor: Avard
   Opis : Sniegulak
   Data : 25.04.07 */

inherit "/std/lampa_uliczna";
#include <stdproperties.h>
#include "dir.h"

void
create_street_lamp()
{
    set_long("Wysoka, ^zelazna, oliwna lampa, s^lu^zy do o^swietlania "+
        "dziedz^nca.\n");
    set_light_message("Jeden z giermk^ow zapala lamp^e.\n");
    set_extinguish_message("Jeden z giermk^ow gasi lamp^e.\n");

}