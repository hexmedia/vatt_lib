/* Autor: Avard
   Opis : Sniegulak
   Data : 06.05.07 */

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <macros.h>
#include <materialy.h>
#include <object_types.h>

void
create_armour()
{
    ustaw_nazwe("szyszak");
    dodaj_przym("lekki", "lekcy");
    dodaj_przym("stalowy","stalowi");

    set_long("He^lm ten wykonany zosta^l z dw^och cz^e^sci - pierwsza to "+
        "sk^orzane okrycie g^lowy, sk^ladaj^ace si^e z kilku warstw "+
        "garbowanej skory. Nast^epnie po^l^aczone z kolcz^a plecionk^a. "+
        "Dok^ladno^s^c i staranno^s^c wykonanego przedmiotu, powoduje, "+
        "^ze egzemplarz ten mo^ze sprawowa^c si^e lepiej ni^z inne modele "+
        "tej lekkiej zbroi. Ochrona, kt^or^a zapewnia ten he^lm jest "+
        "wystarczaj^aca do zatrzymania s^labszych cios^ow.\n");

    set_slots(A_HEAD);
    add_prop(OBJ_I_WEIGHT, 2000);
    add_prop(OBJ_I_VOLUME, 700);
    add_prop(OBJ_I_VALUE, 1800);

	set_size("L");
    add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    ustaw_material(MATERIALY_STAL);
    set_type(O_ZBROJE);
    set_ac(A_HEAD,9,10,10);

    set_likely_cond(13);
}


