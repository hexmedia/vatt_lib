/* autor: Valor
   opis: ^Sniegulak
   dnia: 9 maj 07
*/
inherit "/std/armour";
#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <macros.h>
#include <materialy.h>
#include <object_types.h>
#include <pl.h>
void
create_armour()
{
    ustaw_nazwe("koszulka");
    dodaj_przym("kr^otki","kr^otcy");
    dodaj_przym("kolczy","kolcze");
    set_long("Setki b^lyszcz^acych k^o^leczek mieni ci si^e przed oczami. "+
        "Lekka i nie kr^epuj^aca ruch^ow zbroja chroni tu^l^ow i ramiona "+
        "przed ciosami. Kowal wykona^l ^swietn^a robot^e tworz^ac "+
        "t^a koszulk^e bowiem uda^lo mu si^e zrobi^c plecionk^e z "+
        "naprawd^e ma^lych k^o^leczek, dzi^eki czemu powinna zatrzyma^c "+
        "impet pchni^ecia sztyletu, jak r^ownie^z niekt^ore ci^ecia "+
        "mieczem. \n");
    set_slots(A_TORSO, A_ARMS, A_FOREARMS);
    
    add_prop(OBJ_I_WEIGHT, 11000); 
    add_prop(OBJ_I_VOLUME, 11000);
    add_prop(OBJ_I_VALUE, 2160);
    
    set_type(O_UBRANIA);
    
    ustaw_material(MATERIALY_STAL, 100);
    set_size("L");
    add_prop(ARMOUR_S_DLA_RASY, "krasnolud");
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    set_ac(A_TORSO,10,11,2, A_ARMS, 11,12,3, A_FOREARMS, 11,12,3);

    set_likely_cond(11);
}
