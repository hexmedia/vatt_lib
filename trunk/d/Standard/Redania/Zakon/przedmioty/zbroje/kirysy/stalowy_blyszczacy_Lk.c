
/* Autor: Avard
   Opis : Sniegulak
   Data : 06.05.07 */

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <macros.h>
#include <wa_types.h>
#include <materialy.h>
#include <pl.h>
#include <object_types.h>

void
create_armour()
{
    ustaw_nazwe("kirys");
    dodaj_przym("stalowy","stalowi");
    dodaj_przym("b^lyszcz^acy","b^lyszcz^acy");
    set_long("Do^s^c ci^e^zka zbroja zapewniaj^aca ^swietn^a ochron^e "+
        "plec^ow i klatki piersiowej. Szerokie pasy stali zosta^ly "+
        "po^l^aczone ze sob^a, zachodz^ac na siebie nieznacznie. Boczne "+
        "pasy ^l^acz^a ze sob^a przedni^a i tylna cz^e^s^c pozwalaj^ac na "+
        "lepsze dopasowanie. Robiony na miar^e, przez co ma powierzchni^e "+
        "tak wyprofilowan^a, ^ze idealnie pasuje do sylwetki osoby kt^ora "+
        "j^a zam^owi^la. Mocna wy^sci^o^lka chroni cia^lo od ^srodka, "+
        "przed zimnem i otarciami.\n"); 

    ustaw_material(MATERIALY_STAL, 80);
    ustaw_material(MATERIALY_SK_CIELE, 20);
    set_type(O_ZBROJE);
    set_slots(A_TORSO);
    add_prop(OBJ_I_VOLUME, 9000); 
    add_prop(OBJ_I_VALUE, 10800);   
    add_prop(OBJ_I_WEIGHT, 9000);
    add_prop(ARMOUR_S_DLA_RASY, "krasnolud");
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    set_size("L");
    set_ac(A_TORSO,16,16,13);
    set_likely_cond(6);
}
