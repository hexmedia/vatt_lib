/* Acheron */
inherit "/std/armour";

#include <stdproperties.h>
#include <wa_types.h>
#include <formulas.h>
#include <macros.h>
#include </sys/materialy.h>
#include <object_types.h>

void
create_armour()
{
    ustaw_nazwe("przeszywanica");
    set_type(O_ZBROJE);

    dodaj_przym("czerwony","czerwoni");
    dodaj_przym("pikowany","pikowani");

    ustaw_material(MATERIALY_BAWELNA, 90);
    ustaw_material(MATERIALY_ZELAZO, 10);

    set_long("Ta czerwona przeszywanica jest rodzajem mi^ekkiej zbroi, " +
             "kt^ora ma posta^c grubego, pikowanego kaftana, wykonanego z  " +
             "wielu warstw p^l^otna bawe^lnianego ciasno zszytego ze sob�. " +
             "Jest ona twarda, a co za tym idzie wytrzyma^la, za� fakt i^z " +
             "jest ona dodatkowo pikowana, pomaga nieznacznie przy " +
             "spotkaniu z kling� miecza, jednak g^lownie nadaje si^e ona " +
             "jedynie do poch^laniania obra^ze^n zadanych broni� obuchow�, " +
             "naostrzon� kling� miecza.\n");
             
    set_slots(A_TORSO, A_FOREARMS, A_ARMS);
    set_ac(A_TORSO | A_ARMS | A_FOREARMS, 3,4,8);

    add_prop(OBJ_I_VOLUME,4000);
    add_prop(OBJ_I_VALUE,1400);
    add_prop(OBJ_I_WEIGHT, 4000);
    add_prop(ARMOUR_F_POW_PIERSI, 1);
    add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    set_size("M");

    set_likely_cond(12);
}

