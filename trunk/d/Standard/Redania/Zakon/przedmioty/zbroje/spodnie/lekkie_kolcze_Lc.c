/* Autor: Avard
   Opis : Sniegulak
   Data : 06.05.07 */

inherit "/std/armour";
#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <macros.h>
#include <materialy.h>
#include <object_types.h>

void
create_armour() 
{
    ustaw_nazwe_glowna("para");
    ustaw_nazwe("spodnie kolcze");
    dodaj_nazwy("spodnie");
    dodaj_przym("lekki","lekcy");
      
    set_long("Dok^ladnie ogl^adasz t^a cz^esto spotykan^a zbroj^e, kt^ora "+
        "jest u^zywana przez wojownik^ow, kt^orym zale^zy na ochronie i "+
        "jednocze^snie swobodzie ruch^ow. Do sk^orzanej pary spodni "+
        "zosta^la do^l^aczona plecionka kolcza, dzi^eki kt^orej ciosy s^a "+
        "cz^e^sciowo parowane. Spodnie si^egaj^a do po^lowy ^lydki i "+
        "przystosowane s^a do zak^ladania pod ci^e^zsze zbroje, tak by "+
        "jeszcze dodatkowo poprawi^c ochron^e cia^la nosz^acego. \n");

    set_slots(A_LEGS, A_HIPS);
    add_prop(OBJ_I_VOLUME, 2500);
    add_prop(OBJ_I_VALUE, 6000);
    add_prop(OBJ_I_WEIGHT, 2500);
    add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    set_size("L");
    ustaw_material(MATERIALY_SK_SWINIA, 50);
    ustaw_material(MATERIALY_STAL, 50);
    set_type(O_ZBROJE);

    set_ac(A_LEGS,11,12,3, A_HIPS,11,12,3);

    set_likely_cond(14);
}
