/* Autor: Avard
   Opis : Sniegulak
   Data : 25.04.07 */

inherit "/std/object";

#include "dir.h"
#include <cmdparse.h>
#include <pl.h>
#include <macros.h>
#include <stdproperties.h>
#include <object_types.h>
#include <materialy.h>

void create_object()
{
    ustaw_nazwe("makata");
    dodaj_przym("gruby", "grubi");
    dodaj_przym("we^lniany", "we^lniani");

    set_long("Grube we^lniane makaty, utkane bardzo mocno i zafarbowane "+
        "na kolor br^azowy przyozdabiaj^a i ocieplaj^a ^sciany tego "+
        "pomieszczenia. Prostota ich wykonania i brak r^o^znorodno^sci "+
        "kolor^ow nadaj^a temu miejscu lekko ponury wygl^ad.\n");
    add_prop(OBJ_I_WEIGHT, 1050);
    add_prop(OBJ_I_VOLUME, 900);
    add_prop(OBJ_I_VALUE, 860);
    add_prop(OBJ_M_NO_GET, "Makata jest zbyt solidnie przymocowana do "+
        "^sciany ^zeby^s m^og^l j^a st^ad zabra^c.\n");
    add_prop(OBJ_I_DONT_SHOW_IN_LONG,1);
    set_type(O_MEBLE);
    ustaw_material(MATERIALY_WELNA);
    //set_owners(({RINDE_NPC + "krepp"}));TODO FIXME TRALALA ACHTUNG!!
}