
/* 
opis popelniony przez Sniegulaka, zakodowany przez Seda 12.03.2007
*/

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <macros.h>
#include <materialy.h>

void
create_armour()
{  
   ustaw_nazwe("czepek");
    dodaj_przym("haftowany", "haftowani");
    dodaj_przym("bia^ly","biali");


    set_long("Uszyty ze ^snie^znobia^lego materia^lu czepek, s^lu^zy do "
	+"przys^laniania w^los^ow nosz^acej go osoby. Delikatne niebieskie hafty "
	+"pokrywaj^a ca^la jego powierzchni^e a niebieskie tasiemki pozwalaj^a na "
	+"zawi^azanie czepka pod brod^a. \n");

    set_slots(A_HEAD);
    add_prop(OBJ_I_WEIGHT, 100);
    add_prop(OBJ_I_VOLUME, 1200);
    add_prop(OBJ_I_VALUE, 1000);

	set_size("L");
    add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
    add_prop(ARMOUR_F_PRZELICZNIK, 10.0);
    add_prop(ARMOUR_I_DLA_PLCI, 1);
    ustaw_material(MATERIALY_LEN);

    set_likely_cond(21);
}


