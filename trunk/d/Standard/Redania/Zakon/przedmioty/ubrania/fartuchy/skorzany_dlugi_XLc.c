
/* 
opis popelniony przez Grypina, zakodowany przez Seda 06.04.2007
*/

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <macros.h>
#include <materialy.h>

void
create_armour()
{  
   ustaw_nazwe("fartuch");
    dodaj_przym("sk^orzany","sk^orzani");
    dodaj_przym("d^lugi", "d^ludzy");

    set_long("Bardzo pospolity i spotykany u ka^zdego rzemie^slnika wi^azany na "
	+"plecach fartuch, zrobiony jest z bardzo mocnej i do^s^c sztywnej sk^ory. "
	+"Pe^lni rol^e ochronn^a w warsztatach i pracowniach.  Egzemplarz ten wygl^ada "
	+"na do^s^c stary i cz^esto u^zywany. Rzemyki, kt^orymi wi^a^ze si^e ubranie "
	+"dostosowuj^a wielko^s^c tak by mog^lo pasowa^c na ka^zd^a osob^e.\n");

    set_slots(A_CHEST | A_THIGHS | A_STOMACH);
    add_prop(OBJ_I_WEIGHT, 2000);
    add_prop(OBJ_I_VOLUME, 3100);
    add_prop(OBJ_I_VALUE, 4000);

	set_size("M");
    add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    add_prop(ARMOUR_F_PRZELICZNIK, 20.0);
    add_prop(ARMOUR_I_DLA_PLCI, 0);
	add_prop(ARMOUR_I_D_ROZCIAGLIWOSC, 70);
	add_prop(ARMOUR_I_U_ROZCIAGLIWOSC, 70);
    ustaw_material(MATERIALY_SK_SWINIA);

    set_likely_cond(21);
}


