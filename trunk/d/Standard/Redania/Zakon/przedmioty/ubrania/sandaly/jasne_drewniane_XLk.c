
/* 
opis popelniony przez Grypina, zakodowany przez Seda 06.04.2007
*/

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <macros.h>
#include <materialy.h>

void
create_armour()
{  
   ustaw_nazwe("sanda^ly");
    dodaj_przym("jasny", "ja^sni");
    dodaj_przym("drewniany","drewniani");


    set_long("Typowe pospolite sanda^ly, u^zywane przez wi^ekszo^s^c mieszka^nc^ow w "
	+"czasie codziennej pracy. Podeszwa zrobiona z sosnowego drewna jest "
	+"dobrze wystrugana i wyprofilowana na kszta^lt stopy. Ciemne rzemienie "
	+"z mo^zliwo^sci^a skracania, pozwalaj^a na lepsze dopasowanie obuwia do "
	+"nogi, dzieki czemu chodzenie w nich jest wygodniejsze i powoduje "
	+"mniejsz^a ilo^s^c odcisk^ow i obtar^c podczas d^lugich w^edr^owek.\n");

    set_slots(A_FEET);
    add_prop(OBJ_I_WEIGHT, 700);
    add_prop(OBJ_I_VOLUME, 2300);
    add_prop(OBJ_I_VALUE, 5);

	set_size("XL");
    add_prop(ARMOUR_S_DLA_RASY, "krasnolud");
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    add_prop(ARMOUR_F_PRZELICZNIK, 20.0);
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    ustaw_material(MATERIALY_SK_SWINIA, 20);
	ustaw_material(MATERIALY_DR_DAB, 80);

    set_likely_cond(21);
}


