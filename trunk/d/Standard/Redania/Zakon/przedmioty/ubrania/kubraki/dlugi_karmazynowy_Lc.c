/* Autor: Avard
   Opis : Tabakista
   Data : 17.05.07 */
   
inherit "/std/armour";
#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <object_types.h>
#include <macros.h>
#include <materialy.h>
void
create_armour()
{  
    ustaw_nazwe("kubrak");
    dodaj_przym("d^lugi","d^ludzy");
    dodaj_przym("karmazynowy", "karmazynowi");
    set_long("Kaftan kt^oremu si^e przygl^adasz zosta^l wykonany z grubego "+
        "ciemnoczerwonego materia^lu, dodatkowo obszytego na ramionach "+
        "sk^or^a. Na piersi wida^c wyszyty wizerunek ogromnej czarnej "+
        "tarczy z trzema skrzy^zowanymi mieczami. R^ekoje^sci broni s^a "+
        "srebrne, ostrza za^s maj^a kolor karmazynowy. Sznurowany jest pod "+
        "pachami mocnymi, grubymi rzemieniami. Ko^nce stroju zosta^ly "+
        "skromnie ozdobione srebrna nici^a. Dzi^eki odpowiedniemu krojowi "+
        "ubranie to idealnie przylega do cia^la, i b^edzie chroni^c "+
        "w^la^sciciela przed przed mrozem i deszczem. \n");
 
    set_slots(A_TORSO, A_FOREARMS, A_ARMS);
    add_prop(OBJ_I_WEIGHT, 500); 
    add_prop(OBJ_I_VOLUME, 500);
    add_prop(OBJ_I_VALUE, 190);
    add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    set_size("L");
    ustaw_material(MATERIALY_WELNA, 100);
    set_type(O_UBRANIA);

    set_likely_cond(16);
}