/* Autor: Avard
   Opis : Sniegulak
   Data : 8.05.07 */

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <pl.h>
#include <macros.h>
#include <materialy.h>
#include <object_types.h>

int popraw(string str);

void
create_armour()
{ 

    ustaw_nazwe("plaszcz");

    dodaj_przym("karmazynowy","karmazynowi");
    dodaj_przym("wykwintny", "wykwintni");
    set_long("P^laszcz kt^oremu sie przygl^adasz zosta^l wykonany z grubego "+
        "ciemnoczerwonego materia^lu, dodatkowo obszytego na ramionach "+
        "sk^or^a. Na ^srodku plec^ow wida^c wyszyty wizerunek ogromnej "+
        "czarnej tarczy z trzema skrzy^zowanymi mieczami. R^ekoje^sci "+
        "broni s^a srebrne, ostrza za^s maj^a kolor karmazynowy. P^laszcz "+
        "dodatkowo posiada du^z^a, platynow^a, wysadzana rubinami klamr^e "+
        "w kszta^lcie"+
        " s^lo^nca, dzi^eki kt^orej mo^zna spi^a^c ubranie pod szyj^a. "+
        "Ko^nce p^laszcza zosta^ly wzmocnione gruba srebrna nici^a. Dzi^eki "+
        "odpowiedniemu krojowi ubranie to idealnie przylega do cia^la, i "+
        "b^edzie chroni^c w^la^sciciela przed przed mrozem i deszczem.\n"); 

    ustaw_material(MATERIALY_AKSAMIT, 85);
    ustaw_material(MATERIALY_SK_NIEDZWIEDZ, 10);
    ustaw_material(MATERIALY_RUBIN, 5);
    set_type(O_UBRANIA);
    set_slots(A_CLOAK);
    add_prop(OBJ_I_WEIGHT, 1000);
    add_prop(OBJ_I_VOLUME, 1000);
    add_prop(OBJ_I_VALUE, 500);
    add_prop(ARMOUR_S_DLA_RASY, "krasnolud");
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    add_prop(ARMOUR_I_D_ROZCIAGLIWOSC, 50);
    add_prop(ARMOUR_I_U_ROZCIAGLIWOSC, 50);
    set_size("L");

    set_likely_cond(15);

}

init()
{
   ::init();
   add_action(popraw, "popraw");
}

int
popraw(string str)
{
    object plaszcz;
    if (!str) 
    return 0;
    if (TO->query_worn() == 0)
    return 0;
    if(parse_command(str,environment(this_object()),"%o:" + PL_BIE,plaszcz))
    {
        write("Szybkimi ruchami poprawiasz p^laszcz na swych ramionach.\n");
        saybb(QCIMIE(this_player(), PL_MIA) +" szybkimi ruchami poprawia "+
            "p^laszcz na swych ramionach.\n");
        return 1;
    }
    notify_fail("Popraw co [jak] ?\n");
    return 0;
}