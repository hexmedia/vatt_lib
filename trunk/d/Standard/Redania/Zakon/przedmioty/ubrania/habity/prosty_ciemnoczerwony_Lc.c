/* made by Faeve, 11 maja '07 */

inherit "/std/armour";
#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <pl.h>
#include <macros.h>
#include <materialy.h>
#include <object_types.h>

void
create_armour()
{               
    ustaw_nazwe("habit");
    dodaj_przym("prosty","pro^sci"); 

    dodaj_przym("ciemnoczerwony","ciemnoczerwoni");

    set_long("Utkany z lekkiego, przewiewnego, a zarazem mocnego materia^lu "+
		"habit nie posiada ^zadnych zdobie^n, a jedynym dodatkiem do niego "+
		"jest srebrzysty sznur, kt^ory s^lu^zy do przewi^azywania sie w "+
		"pasie. D^lugi si^egaj^acy kostek materia^l, po za^lo^zeniu b^edzie "+
		"na dwa palce uniesiony nad ziemi^e. \n");
             
    set_slots(A_ROBE);
    add_prop(OBJ_I_WEIGHT, 3000);
    add_prop(OBJ_I_VOLUME, 2000);
    add_prop(OBJ_I_VALUE, 180);
    set_type(O_UBRANIA);
    ustaw_material(MATERIALY_ATLAS, 100);
    add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    add_prop(ARMOUR_I_U_ROZCIAGLIWOSC, 40);

    set_size("L");
}