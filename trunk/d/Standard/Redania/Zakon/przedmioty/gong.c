/* Autor: Avard
   Opis : Sniegulak
   Data : 25.04.07 */

inherit "/std/object";

#include "dir.h"
#include <cmdparse.h>
#include <pl.h>
#include <macros.h>
#include <stdproperties.h>
#include <object_types.h>
#include <materialy.h>

int uderz_w_gong(string str);

void create_object()
{
    ustaw_nazwe("gong");
    dodaj_przym("miedziany", "miedziani");
    dodaj_przym("wypolerowany", "wypolerowani");

    set_long("Jest to ustawiony, na wystruganych w sosnowym drewnie nogach, "+
        "du^zy miedziany talerz. Ten kawa^lek blachy jest idealnie "+
        "wypolerowany i ozdobiony ornamentami w kszta^lcie smoczej ^luski. "+
        "Do jednej z n^og stojaka przyczepiony jest ma^ly dr^a^zek, "+
        "zako^nczony materia^low^a kulka, kt^ory najprawdopodobniej s^lu^zy "+
        "do uderzania w talerz.\n");
    add_prop(OBJ_I_WEIGHT, 20000);
    add_prop(OBJ_I_VOLUME, 20000);
    add_prop(OBJ_I_VALUE, 5000);
    add_prop(OBJ_M_NO_GET, "Miedziany wypolerowany gong jest przytwierdzony "+
        "do ziemi.\n");
    set_type(O_MEBLE);
    ustaw_material(MATERIALY_WELNA);
    //set_owners(({RINDE_NPC + "krepp"}));TODO FIXME TRALALA ACHTUNG!!
}

init()
{
    ::init();
    add_action(&uderz_w_gong(), "uderz");
}

int uderz_w_gong(string str)
{
    object obj;

    if(!str) return 0;

    if(!parse_command(lower_case(str), environment(this_player()), "'w' %o:"+PL_BIE, obj))
        return 0;

    if (obj != this_object())
    return 0;

    write("Podchodzisz do gongu i mocnym uderzeniem wydajesz z niego "+
        "g^lo^sny niski d^xwi^ek.\n");
    saybb(QCIMIE(this_player(), PL_MIA)+" podchodzi do "+
        this_object()->short(PL_DOP)
        +" i uderza w niego wywo^luj^ac g^lo^sny niski d^xwi^ek.\n");
    set_alarm(0.1,0.0, "walim");
    return 1;
}
void
walim()
{
    tell_room(ZAKON_LOKACJE + "lok1", "Gdzie^s z p^o^lnocy dochodzi ci^e "+
        "przyt�umiony, niski dzwi^ek gongu.\n");
    tell_room(ZAKON_LOKACJE + "lok2", "Gdzie^s z p^o^lnocy dochodzi ci^e "+
        "przyt�umiony, niski dzwi^ek gongu.\n");
    tell_room(ZAKON_LOKACJE + "lok3", "Z p^o^lnocy dochodzi ci^e g^lo^sny, "+
        "niski dzwi^ek gongu.\n");
    tell_room(ZAKON_LOKACJE + "lok4", "Z p^o^lnocnego-wschodu dochodzi "+
        "ci^e g^lo^sny, niski dzwi^ek gongu.\n");
    tell_room(ZAKON_LOKACJE + "lok5", "Z p^o^lnocnego-zachodu dochodzi "+
        "ci^e g^lo^sny, niski dzwi^ek gongu.\n");    
    tell_room(ZAKON_LOKACJE + "lok7", "Z p^o^lnocy dochodzi ci^e "+
        "g^lo^sny, niski dzwi^ek gongu.\n");
    tell_room(ZAKON_LOKACJE + "lok8", "Z do�u dochodzi "+
        "ci^e g^lo^sny, niski dzwi^ek gongu.\n");
    tell_room(ZAKON_LOKACJE + "lok12", "Z do�u dochodzi "+
        "ci^e g^lo^sny, niski dzwi^ek gongu.\n");
    tell_room(ZAKON_LOKACJE + "lok10", "Z do�u dochodzi "+
        "ci^e g^lo^sny, niski dzwi^ek gongu.\n");
    tell_room(ZAKON_LOKACJE + "lok11", "Z do�u dochodzi "+
        "ci^e g^lo^sny, niski dzwi^ek gongu.\n");
    tell_room(ZAKON_LOKACJE + "lok9", "Z do�u dochodzi "+
        "ci^e przyt�umiony, niski dzwi^ek gongu.\n");
    tell_room(ZAKON_LOKACJE + "kwatera", "Z do�u dochodzi "+
        "ci^e g^lo^sny, niski dzwi^ek gongu.\n");
    tell_room(ZAKON_LOKACJE + "kaplica", "Z do�u dochodzi "+
        "ci^e g^lo^sny, niski dzwi^ek gongu.\n");
    tell_room(ZAKON_LOKACJE + "zbrojownia", "Z do�u dochodzi "+
        "ci^e g^lo^sny, niski dzwi^ek gongu.\n");
    tell_room(ZAKON_LOKACJE + "sypialnia", "Z do�u dochodzi "+
        "ci^e g^lo^sny, niski dzwi^ek gongu.\n");
    tell_room(ZAKON_LOKACJE + "kuchnia", "Ze wschodu dochodzi "+
        "ci^e g^lo^sny, niski dzwi^ek gongu.\n");
    tell_room(ZAKON_LOKACJE + "alchemik", "Z zachodu dochodzi "+
        "ci^e g^lo^sny, niski dzwi^ek gongu.\n");
    tell_room(ZAKON_LOKACJE + "kowal", "Gdzie^s z p^o^lnocy dochodzi ci^e "+
        "przyt�umiony, niski dzwi^ek gongu.\n");
    tell_room(ZAKON_LOKACJE + "plac", "Gdzie^s z p^o^lnocy dochodzi ci^e "+
        "przyt�umiony, niski dzwi^ek gongu.\n");
    tell_room(ZAKON_LOKACJE + "stajnia", "Gdzie^s z p^o^lnocy dochodzi ci^e "+
        "przyt�umiony, niski dzwi^ek gongu.\n");
}