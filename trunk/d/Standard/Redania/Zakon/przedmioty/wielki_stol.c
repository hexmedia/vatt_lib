/* Autor: Avard
   Opis : Sniegulak
   Data : 25.04.07 */

inherit "/std/container";

#include "dir.h"
#include <cmdparse.h>
#include <pl.h>
#include <macros.h>
#include <stdproperties.h>
#include <object_types.h>
#include <materialy.h>

void create_container()
{
    ustaw_nazwe("st^o^l");
    dodaj_przym("wielki", "wielcy");
    dodaj_przym("drewniany", "drewniani");

    set_long("Wielkich rozmiar^ow drewniany, stary st^o^l znajduje si^e w "+
        "centrum tego pokoju. Powierzchnia blatu jest gdzieniegdzie "+
        "zarysowana i posiada liczne szpary oraz wg^l^ebienia, lecz ca^ly "+
        "mebel zrobiony z d^ebowego drewna, pokrytego woskiem wygl^ada na "+
        "wypiel^egnowany i zadbany. Po ^srodku sto^lu namalowano znak "+
        "Zakonu: du^z^a, czarn^a tarcz^e ze skrzy^zowanymi na niej trzema "+
        "mieczami o srebrzystych r^ekoje^sciach i karmazynowych ostrzach.\n");
    add_prop(OBJ_I_WEIGHT, 25000);
    add_prop(OBJ_I_VOLUME, 25000);
    add_prop(OBJ_I_VALUE, 10000);
    set_type(O_MEBLE);
    add_prop(OBJ_M_NO_GET, "St^o^l jest za ci^e^zki ^zeby go podnie^s^c.\n");
    add_prop(OBJ_I_DONT_SHOW_IN_LONG,1);
    add_subloc("na", 0, "ze");
    add_subloc_prop("na", SUBLOC_I_MOZNA_POLOZ, 1);
    ustaw_material(MATERIALY_DR_DAB);

    //set_owners(({RINDE_NPC + "krepp"}));TODO FIXME TRALALA ACHTUNG!!
}
