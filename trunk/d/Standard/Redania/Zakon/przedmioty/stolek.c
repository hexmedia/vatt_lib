/* Autor: Avard
   Opis : Sniegulak
   Data : 25.04.07 */

inherit "/std/object";

#include "dir.h"
#include <cmdparse.h>
#include <pl.h>
#include <macros.h>
#include <stdproperties.h>
#include <object_types.h>
#include <materialy.h>

void create_object()
{
    ustaw_nazwe("sto^lek");
    dodaj_przym("niski", "niscy");
    dodaj_przym("nieoheblowany", "nieoheblowani");

    set_long("Sto^leki ten zrobiony jest z nieoheblowanego drewna, kt^ore "+
        "jeszcze pachnie ^zywic^a. Siedzisko przykryte zosta^lo sk^orami, "+
        "tak by przez przypadek nie ubrudzi^c ubrania sokiem z drzew. Jego "+
        "wielko^s^c i budowa daje wiele do ^zyczenia, ale przynajmniej "+
        "jest gdzie odpocz^a^c.\n");
    add_prop(OBJ_I_WEIGHT, 1500);
    add_prop(OBJ_I_VOLUME, 1500);
    add_prop(OBJ_I_VALUE, 40);
    set_type(O_MEBLE);
    ustaw_material(MATERIALY_DR_SOSNA);
    make_me_sitable("na", "na sto^lku", "ze sto^lka", 1);
    //set_owners(({RINDE_NPC + "krepp"}));TODO FIXME TRALALA ACHTUNG!!
}
public int
jestem_stolkiem_zakonu()
{
        return 1;
}
