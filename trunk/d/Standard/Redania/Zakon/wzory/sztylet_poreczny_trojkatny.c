inherit "/std/pattern";

#include <pattern.h>
#include "dir.h"

public void
create()
{
    set_pattern_domain(PATTERN_DOMAIN_BLACKSMITH);
    set_item_filename(ZAKON_BRONIE + "sztylety/poreczny_trojkatny.c");
    set_estimated_price(960);
    set_time_to_complete(2000);
   // enable_want_sizes();
}
