inherit "/std/pattern";

#include <pattern.h>
#include "dir.h"

public void
create()
{
    set_pattern_domain(PATTERN_DOMAIN_BLACKSMITH);
    set_item_filename(ZAKON_ZBROJE + "kirysy/stalowy_blyszczacy_Lc.c");
    set_estimated_price(2600);
    set_time_to_complete(3600);
    enable_want_sizes();
}
