inherit "/std/pattern";

#include <pattern.h>
#include "dir.h"

public void
create()
{
    set_pattern_domain(PATTERN_DOMAIN_BLACKSMITH);
    set_item_filename(ZAKON_BRONIE + "topory/poreczny_zdobiony.c");
    set_estimated_price(4800);
    set_time_to_complete(3600);
   // enable_want_sizes();
}
