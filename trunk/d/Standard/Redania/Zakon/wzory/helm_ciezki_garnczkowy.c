inherit "/std/pattern";

#include <pattern.h>
#include "dir.h"

public void
create()
{
    set_pattern_domain(PATTERN_DOMAIN_BLACKSMITH);
    set_item_filename(ZAKON_ZBROJE + "helmy/ciezki_garnczkowy_Lc.c");
    set_estimated_price(2400);
    set_time_to_complete(1700);
    enable_want_sizes();
}
