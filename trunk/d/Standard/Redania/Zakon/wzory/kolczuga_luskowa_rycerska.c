inherit "/std/pattern";

#include <pattern.h>
#include "dir.h"

public void
create()
{
    set_pattern_domain(PATTERN_DOMAIN_BLACKSMITH);
    set_item_filename(ZAKON_ZBROJE + "kolczugi/luskowa_rycerska_Lc.c");
    set_estimated_price(7200);
    set_time_to_complete(2600);
    enable_want_sizes();
}
