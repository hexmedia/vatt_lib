inherit "/std/pattern";

#include <pattern.h>
#include "dir.h"

public void
create()
{
    set_pattern_domain(PATTERN_DOMAIN_BLACKSMITH);
    set_item_filename(ZAKON_BRONIE + "sztylety/zabkowany_dlugi.c");
    set_estimated_price(1600);
    set_time_to_complete(2400);
   // enable_want_sizes();
}
