/* Autor: Faeve
   Opis : Jasmina
   Data : 15.07.07


   robic opis wyjsc czy zostawic tylko te w longu? */

#include <stdproperties.h>
#include <mudtime.h>
#include <macros.h>
#include "dir.h"

inherit PIANA_STD;
inherit "/lib/peek";

string dlugi_opis();

void
create_piana()
{
    set_short("Korytarzyk na pi^etrze.");
    set_long("@@dlugi_opis@@");

    add_prop(ROOM_I_INSIDE, 1);
    add_exit(PIANA_KARCZMA + "lokacje/sala",({({"d","schody","sala",
        "sala g^l^owna"}),"po schodach"}),0,1,0);

    add_item("schody","Zej^scie prowadzi na w^askie i strome schody w "+
        "d^o^l, kt^ore skrzypi^a przy ka^zdym st^apni^eciu. \n");

    add_object(PIANA_KARCZMA + "przedmioty/okno_korytarzyk");
    add_object(PIANA_KARCZMA + "przedmioty/zaslony");
    add_object(PIANA_KARCZMA + "przedmioty/lampka_korytarzyk");
    dodaj_rzecz_niewyswietlana("lampka oliwna");

    add_door(PIANA_KARCZMA + "lokacje/drzwi/do_wspolnego");
    add_door(PIANA_KARCZMA + "lokacje/drzwi/do_pokoju_e");
    add_door(PIANA_KARCZMA + "lokacje/drzwi/do_pokoju_w");

    set_event_time(80.0);
    add_event("@@evenciki:"+file_name(TO)+"@@");
}

string
dlugi_opis()
{
    string str;

    str = "W tym w^askim, ciasnym korytarzyku dwie osoby o poka^xnej tuszy "+
        "zapewne mia^lyby spory problem z mini^eciem si^e. Ca^ly wy^lo^zony "+
        "ciemnymi deskami, na po^ludniowej ^scianie zwie^nczony jest "+
        "drzwiami do sali wsp^olnej, krzywo wisz^acymi na zawiasach. "+
        "P^o^lnocna ^sciana ^l^aczy si^e ze sko^snie ^sci^etym dachem, "+
         "tam te^z znajduje si^e niewielkie, wykuszowe okno, obecnie ";

    object zaslony = present("zas^lony");
     if(zaslony->query_prop("odslonieta") == 0)
        {
        str += "zas^loni^ete bur^a zas^lon^a. ";
        }
        else
    {
            str += "ods^loni^ete na ";
            if(jest_dzien())
        {
                str += "^swiat^lo dnia. ";
        }
        else
        {
            str += "mrok nocy. ";
        }
    }

    str += "Dok^ladnie pod oknem widnieje wej^scie na prowadz^ace w d^o^l "+
        "w^askie i strome schody. ^Sciany wschodnia i zachodnia nie "+
        "r^o^zni^a si^e niemal niczym - pozbawione ozd^ob, z identycznymi, "+
        "niewielkimi drzwiczkami umieszczonymi po ^srodku, dok^ladnie "+
        "naprzeciw siebie. Drewniana pod^loga skrzypi przy ka^zdym kroku. "+
        "Zamiast sufitu wysoko w g^or^e wznosz^a si^e belki i ^zerdzie "+
        "wspieraj^ace spadzisty dach. Do jednej z nich przywi^azano sznur "+
        "konopny - na nim wisi niedu^za, oliwna lampka, stanowi^aca jedyne "+
        "o^swietlenie tego pomieszczenia po zmroku, rozja^sniaj^aca jedynie "+
        "niewielk^a przestrze^n pod soba, gdy reszta korytarza tonie w "+
        "cieniu. ";

    str += "\n";
    return str;
}

public string
exits_description()
{
    return "Drzwi na p^o^lnocy prowadz^a do sali wsp^olnej, za^s te na "+
        "wschodzie i zachodzie do pokoj^ow do wynaj^ecia. Strome schody "+
        "za^s prowadz^a wprost do sali g^l^ownej karczmy. \n";
}

string
evenciki()
{
   switch(pora_dnia())
   {
       case MT_SWIT:
       case MT_WCZESNY_RANEK:
       case MT_RANEK:
       case MT_POLUDNIE:
       case MT_POPOLUDNIE:
           return ({"Mysz przebieg^la szybko pod ^scian^a.\n","Pod^loga "+
           "skrzypn^e^la cicho.\n","Sznur z lampk^a zako^lysa^l si^e "+
           "lekko.\n","Z jednego z pokoi dosz^lo twych uszu ciche "+
           "stukni^ecie.\n","W kt^orym^s z pokoi stukn^e^lo zamykane okno.\n"})
           [random(5)];
       default:
           return ({"Zdawa^lo ci si^e, ^ze co^s poruszy^lo si^e pomi^edzy "+
           "^zerdziami nad twoj^a g^low^a.\n","Lampka przygas^la na "+
           "chwil^e.\n","Ma^la myszka przemkn^e^la ci szybko pod nogami.\n",
           "Gdzie^s ze wschodu doszed^l twych uszu cichy stuk.\n"})[random(4)];
   }
}