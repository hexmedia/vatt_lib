/*
 * Karczma w BM
 * Opis:  Jasmina
 * Autor: Faeve
 * Data:  08.07.2007
 */

#include "dir.h"

inherit PIANA_STD;
inherit "/lib/peek";
inherit "/lib/pub_new";

#define SIEDZACY "_siedzacy"
#include <macros.h>
#include <filter_funs.h>
#include <ss_types.h>
#include <stdproperties.h>
#include <wa_types.h>
#include <pl.h>
#include <options.h>
#include <formulas.h>

int zasiedzenie;

void
create_piana()
{
    set_short("Sala g^l^owna");
    set_long("@@dlugi_opis@@");

    add_sit("przy ladzie", "przy ladzie", "od lady", 3);

    add_prop(ROOM_I_INSIDE,1);

    add_item("schody","W^askie i strome, skrzypi^a przy ka^zdym na nie "+
        "st^apni^eciu. Por^ecz jest u^lamana w jednym miejscu, dalej jednak "+
        "g^ladko wspina si^e w g^or^e.\n");
    add_item(({"pod^log^e","klepisko"}),"Zwyk^le klepisko, niezwykle brudne "+
        "i zaniedbane. \n");

    add_npc(PIANA_NPC + "karczmarz.c");

    add_exit(PIANA_KARCZMA + "lokacje/korytarzyk","g^ora",0,1,0);
    add_object(PIANA_KARCZMA + "lokacje/drzwi/z_karczmy");

    add_object(PIANA_KARCZMA + "przedmioty/lawa", 3);
    dodaj_rzecz_niewyswietlana("stara d^ebowa ^lawa",3);
    add_object(PIANA_KARCZMA + "przedmioty/zyrandol");
    dodaj_rzecz_niewyswietlana("^zyrandol");
    add_object(PIANA_KARCZMA + "przedmioty/stolek",24);
    dodaj_rzecz_niewyswietlana("prosty drewniany sto^lek",24);
    add_object(PIANA_KARCZMA + "przedmioty/lampka_wnetrze",8);
    dodaj_rzecz_niewyswietlana("lampka",8);
    add_object(PIANA_KARCZMA + "przedmioty/lada");
    dodaj_rzecz_niewyswietlana("lada");
    add_object(PIANA_KARCZMA + "przedmioty/okno");
    add_object(PIANA_KARCZMA + "przedmioty/zaslony");
    add_object(PIANA_KARCZMA + "przedmioty/tabliczka");

    set_event_time(90.0);
    add_event("@@evenciki:"+file_name(TO)+"@@");

    add_drink("mleko",0,"^swie^zego, zimnego mleka",400, 0, 8,"kubek","mleka",
        "Jest to zwyk^ly, drewniany kubek.");

    add_drink("piwo",({"jasny","ja�ni"}),"do^s^c mocno spienionego piwa, o "+
        "wspania^lym bursztynowym kolorze",700, 5, 16,"kufel","jasnego piwa",
        "Jest to prosty, cynowy kufel.");

    add_drink("piwo",({"ciemny","ciemni"}),"lekko spienionego, o ciemnej jak "+
        "palisander barwie",650, 4, 15,"kufel","janego piwa",
        "Jest to prosty, cynowy kufel.");

    add_food(({"kasza","kasza ze skwarkami"}), 0, "kaszy ze skwarkami. Mimo "+
        "i^z wygl^ada nieszczeg^olnie - szara ma^x upstrzona kilkoma "+
        "czerwonawymi kulkami - pachnie wy^smienicie. Wida^c, ^ze karczmarz "+
        "nie ^za^lowa^l s^loniny i skraja^l grube i t^luste p^laty. Kasza, "+
        "pulchna i nap^ecznia^la suto okraszona jest te^z t^luszczykiem, "+
        "ale wyj^atkowo ^swie^zym i wygl^adaj^acym na ca^lkiem dobry. Co "+
        "prawda micha wype^lniona jest po brzegi, ale nie jest to wybitnie "+
        "du^za miska, cho^c ^ladna i prawie w og^ole niepoobt^lukiwana. "+
        "Widocznie karczmarz woli przyoszcz^edzi^c na ilo^sci, jako^s^c "+
        "jednak pozostawiaj^ac najwy^zsz^a",
        500,45,6,"miska","kaszy","Jest to "+
        "prosta, wykonana z drewna miska.", 200);

    add_food("jajecznica",0,"dobrze ^sci^etej jajecznicy, z du^z^a "+
        "ilo^sci^a lekko podsma^zonej cebulki", 500,45,8,"miska","jajecznicy",
        "Jest to prosta, wykonana z drewna miska.", 200);

    add_food("gulasz",({"rybny","rybni"}),"gulaszu rybnego. Znad miski "+
        "unosi si^e ostry zapach papryczki, za^s w wok^o^l kawa^lk^ow ryby "+
        "p^lywaj^a ca^lkiem smakowicie wygl^adaj^ace ^smieci - do^s^c "+
        "grubo pokrojone kawa^lki warzyw, skwarki oraz majeranek",
        600,45,15,"miska","gulaszu rybnego","Jest to prosta, "+
        "wykonana z drewna miska.",200);

}

void
init()
{
    ::init();
    init_pub();
    init_peek();

}


string dlugi_opis()
{
    string str;

    int i, il, il_stolkow;
    object *inwentarz;

    inwentarz = all_inventory();
    il_stolkow = 0;
    il = sizeof(inwentarz);

    for (i = 0; i < il; ++i)
         if (inwentarz[i]->jestem_stolkiem_karczmy_piana())
            ++il_stolkow;

    str = "Sala - na pierwszy rzut oka ciasna i zadymiona - okazuje si^e "+
        "by^c wcale obszerna, zastawiona jednak szeregiem ^law";

    if(il_stolkow > 10)
    {
        str += ", wzd^lu^z kt^orych ustawionorz^edami kilkana^scie prostych, "+
            "pozbawionych opar^c, okr^ag^lych sto^lk^ow. ";
    }

    if(il_stolkow >= 5 && il_stolkow <= 10)
    {
        str += ", wzd^lu^z kt^orych ustawiono rz^edami kilka prostych, "+
            "pozbawionych opar^c, okr^ag^lych sto^lk^ow. ";
    }

    if(il_stolkow == 4)
    {
        str += ", wzd^lu^z kt^orych ustawiono cztery proste, pozbawione "+
            "opar^c, okr^ag^le sto^lki. ";
    }

    if(il_stolkow == 3)
    {
        str += ", przy kt^orych kt^orych ustawiono trzy proste, pozbawione "+
            "opar^c, okr^ag^le sto^lki. ";
    }

    if(il_stolkow == 2)
    {
        str += ", przy kt^orych ustawiono dwa proste, pozbawione opar^c, okr^ag^le "+
            "sto^lki. ";
    }

    if(il_stolkow == 1)
    {
        str += ", przy kt^orych postawiono jeden prosty, pozbawiony "+
            "oparcia, okr^ag^ly sto^lek. ";
    }

    str += "Na po^ludniowej ^scianie widniej^a d^ebowe, okute drzwi. "+
        "Wzd^lu^z zachodniej ^sciany ci^agnie si^e niezbyt d^luga lada, "+
        "obok kt^orej przywieszono jad^lospis, za^s dok^ladnie na przeciw "+
        "drzwi, za szeregami ^law, poprzez kurz i dym dostrzec mo^zna "+
        "w^askie i strome schodki prowadz^ace na pi^etro. ^Sciany, "+
        "wykonane z solidnych drewnianych desek s^a przyozdobione szeregiem "+
        "niezbyt du^zych okien zaopatrzonych w bure, niegdy^s zapewne "+
        "br^azowe zas^lony. Pomi^edzy oknami rozmieszczone zosta^ly oliwne "+
        "lampki, os^loni^ete przybrudzonym, odymionym szk^lem. Wraz z "+
        "prostym, zrobionym z ko^la od wozu ^zyrandolem na ^swiece "+
        "stanowi^a one jedyne o^swietlenie sali, roz^swietlone dok^ladnie - "+
        "zalewaj^a pomieszczenie zar^owno blaskiem, jak i kopc^acym dymem. "+
        "Pod^loga, jak mo^zna zauwa^zy^c po chwili, jest tu zwyk^lym "+
        "klepiskiem, brudnym i rzadko traktowanym miot^l^a. \n";

   return str;
}

public string
exits_description()
{
    return "Schody prowadz^a na korytarz na g^orze, za^s w po^ludniowej "+
        "cz^e^sci sali znajduj^a si^e drzwi prowadz^ace na podw^orze przed "+
        "karczm^a. \n";
}

string
evenciki()
{
   switch(pora_dnia())
   {
       case MT_SWIT:
       case MT_WCZESNY_RANEK:
       case MT_RANEK:
       case MT_POLUDNIE:
       case MT_POPOLUDNIE:
           return ({"Drzwi skrzypn^e^ly pod naporem wiatru z zewn^atrz.\n",
           "Mucha przelecia^la ci ko^lo nosa i usiad^la na ladzie.\n",
           "^Zyrandol zako^lysa^l si^e lekko.\n","Zdawa^lo ci si^e, ^ze "+
           "przez pod^log^e przebieg^la mysz.\n","Na pi^etrze stukn^e^lo "+
           "zamykane okno.\n"})[random(5)];
       default:
           return ({"Schody na pi^etro skrzypn^e^ly cicho.\n","^Cma kr^a^zy "+
           "wok^o^l jednej z lampek.\n","Zdawa^lo ci si^e, ^ze przez "+
           "pod^log^e przebieg^l jaki^s drobny cie^n.\n","^Zyrandol "+
           "zako^lysa^l si^e, rozsy^laj^ac cienie po ^scianach.\n","Jedna z "+
           "lampek na chwil^e przygas^la.\n"})[random(5)];
   }
}
