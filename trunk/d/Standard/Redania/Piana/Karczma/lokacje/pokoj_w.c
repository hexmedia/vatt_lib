/* Autor: Faeve
   Opis : Jasmina
   Data : 16.07.07 
   
   jeden z pokoi do wynajecia w karczmie w Pianie
   ten na zachod od korytarza */

#include <stdproperties.h>
#include <mudtime.h>
#include <macros.h>
#include <object_types.h>
#include "dir.h"

inherit "/std/room";
inherit "/lib/peek";

void
create_room()
{
    set_short("Niedu^zy pok^oj.");
    set_long("@@dlugi_opis@@");

    add_prop(ROOM_I_INSIDE, 1);
    add_prop(ROOM_I_HIDE, -1);

    add_item("pod^log^e","Zakurzona i skrzypi^aca pod^loga u^lo^zona "+
        "zosta^la z niezbyt r^owno dopasowanych desek.\n");
    add_item("dr^agi","Nied^lugie dr^agi przymocowane do po^ludniowej i "+
        "p^o^lnocnej ^sciany. Wisz^a na nich dwie oliwne lampki.\n");

    add_object(PIANA_KARCZMA + "przedmioty/stoliczek_pokoj");
    dodaj_rzecz_niewyswietlana("niedu^zy kwadratowy stoliczek");
    add_object(PIANA_KARCZMA + "przedmioty/lozko_pokoj");
    dodaj_rzecz_niewyswietlana("^l^o^zko");
    add_object(PIANA_KARCZMA + "przedmioty/przescieradlo");
    dodaj_rzecz_niewyswietlana("szare poplamione prze^scierad^lo");
    add_object(PIANA_KARCZMA + "przedmioty/krzeslo");
    dodaj_rzecz_niewyswietlana("stare krzes^lo");
    add_object(PIANA_KARCZMA + "przedmioty/szafa");
    dodaj_rzecz_niewyswietlana("szafa");
    add_object(PIANA_KARCZMA + "przedmioty/lampka_wspolny",2);
    add_object(PIANA_KARCZMA + "przedmioty/zaslony_pokoj");
    add_object(PIANA_KARCZMA + "przedmioty/okno_pokoj");
    add_object(PIANA_KARCZMA + "przedmioty/dywanik");
    dodaj_rzecz_niewyswietlana("dziurawy brunatny dywanik");

    add_object(PIANA_KARCZMA + "lokacje/drzwi/z_pokoju_w");

    set_niewyswietlane_mask(O_MEBLE);

   set_event_time(80.0);
   add_event("@@evenciki:"+file_name(TO)+"@@");

}

string
dlugi_opis()
{
	string str;

    str = "Pok^oj jest niewielki, za^s jego powierzchni^e umniejsza jeszcze "+
        "uko^snie biegn^acy dach, ^l^acz^acy si^e z zachodni^a ^scian^a "+
        "niewiele powy^zej pod^logi. Na ^srodku wschodniej ^sciany "+
        "widniej^a niewielkie drzwiczki prowadz^ace na korytarz, teraz ";
    
     if(present(" drzwiczki", TO)->query_open())
        {
        str += "rozwarte na o^scierz, utrudniaj^ac poruszanie si^e w i tak "+
                "niezbyt obszernym pokoju. ";
        }
        else
            {
            str += "szczelnie zamkni^ete. ";
            }
            
    str += "Przy po^ludniowym kra^ncu pomieszczenia stoi du^ze, zbite z "+
        "solidnych desek ^l^o^zko, wyposa^zone w siennik ";
    
    if(jest_rzecz_w_sublokacji(0,"szare poplamione prze^scierad^lo"))
    {
        str += " i szare, poplamione prze^scierad^lo. ";
    }
    else
    {
        str += ". ";
    }

    str +=  "Tu^z obok niego, na wschodniej ^scianie ustawiono p^ekat^a "+
        "szaf^e z ciemnego drewna. Po przeciwnej stronie pokoju ";

    if(jest_rzecz_w_sublokacji(0,"niedu^zy kwadratowy stoliczek"))
        str += "postawiono niedu^zy, kwadratowy stoliczek";
    {
        if(jest_rzecz_w_sublokacji(0,"stare krzes^lo"))
        {
            str += "przy kt^orym znajduje si^e stare, niepewnie stoj^ace na "+
                "nier^ownych nogach krzes^lo. ";
        }
        else
        {
            str += ". ";
        }
    }
    if(jest_rzecz_w_sublokacji(0,"stare krzes^lo"))
            str += "postawiono stare, niepewnie stoj^ace na nier^ownych "+
                "nogach krzes^lo. ";
                   
    str += "Do p^o^lnocnej i po^ludniowej ^sciany przymocowano tak^ze "+
        "nied^lugie dr^agi, na kt^orych wisz^a oliwne lampki. W uko^snym "+
        "dachu mie^sci si^e niedu^ze, wykuszowe okno ";
    
    object zaslony = present("zas^lony");
    if(zaslony->query_prop("odslonieta") == 0)
        {
        str += "przes^loni^ete brunatn^a zas^lon^a. ";
        }
        else
        {
            str += "ods^loni^ete,";

            if(jest_dzien())
            {
                str += "wpuszczaj^ac dziennie ^swiat^lo przez niezbyt "+
                    "czyst^a szyb^e. ";
            }
            else
            {
                str += "ukazuj^ac przez niezbyt czyst^a szyb^e ciemno^s^c "+
                    "nocy. ";
            }
        }

    str += "Pod^loga jest zakurzona i skrzypi^aca, przy ^l^o^zku ";

    if(jest_rzecz_w_sublokacji(0,"dziurawy brunatny dywanik"))
    {
        str += "przykryta pe^lnym dziur dywanikiem. ";
    }
    else
    {
        str += "prezentuj^aca czystszy prostok^at, wcze^sniej zapewne "+
            "czym^s zakryty. ";
    }
    
    if(jest_dzien())
    {
        str += "W jednym z rog^ow pomieszczenia dostrzec mo^zna spor^a "+
            "szczelin^e, w kt^ora zmie^sci^laby si^e nawet do^s^c opas^la "+
            "mysz. ";
    }
    else
    {
        str += "K^aty pomieszczenia, jak i wszelkie zakamarki ton^a w mroku. ";
    }
        
    str += "\n";
    return str;
}



public string
exits_description() 
{
    return "Na wschodniej ^scianie znajduj^a si^e drzwiczki prowadz^ace na "+
        "korytarz.\n";
}

string
evenciki()
{
   switch(pora_dnia())
   {
       case MT_SWIT:
       case MT_WCZESNY_RANEK:
       case MT_RANEK:
       case MT_POLUDNIE:
       case MT_POPOLUDNIE:
           return ({"Mysz przemkn^e^la spod ^l^o^zka wprost do szczeliny w "+
           "rogu pokoju.\n","Paj^ak przebieg^l po ^scianie przy ^l^o^zku.\n",
           "Wiatr zastuka^l w okiennic^e.\n","Pod^loga skrzypn^e^la cicho.\n"})
           [random(4)]; 
       default:
           return ({"Co^s przemkn^e^lo przez pok^oj.\n","Z korytarza "+
           "dochodzi ci^e jaki^s kr^otki stuk.\n","^L^o^zko zaskrzypia^lo "+
           "cicho.\n","Zdawa^lo ci si^e, ^ze zas^lona poruszy^la si^e "+
           "lekko.\n"})[random(4)]; 
   }
}
