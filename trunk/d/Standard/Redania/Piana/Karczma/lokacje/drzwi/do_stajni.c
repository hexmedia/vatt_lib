/*
 * drzwi do karczmy w Pianie
 * opis: Jasmina
 * popsu^la: faeve
 * dn. 07.07.07
 */

inherit "/std/door";

#include <pl.h>
#include <macros.h>
#include "dir.h"

string wrota();

void
create_door()
{
    ustaw_nazwe("wrota");
    dodaj_przym("drewniany","drewniani");

    set_other_room(PIANA_KARCZMA + "lokacje/stajnia.c");
    set_door_id("DRZWI_DO_STAJNI_PIANA");
    set_door_desc(&wrota());

    set_open_desc("");
    set_closed_desc("");

    set_pass_command(({"stajnia","wrota","wsch�d"}),"przez wrota do stajni","ze stajni");

    set_open(0);
    set_locked(1);

    set_key("KLUCZ_DRZWI_DO_STAJNI_PIANA");
    set_lock_name(({"zamek", "zamku", "zamkowi", "zamek", "zamkiem", "zamku"}), 0, PL_MESKI_NOS_NZYW);
}

public int
jestem_drzwiami_karczmyp()
{
    return 1;
}

string
wrota()
{
    string str;

    str = "Wrota do stajni zbite zosta^ly z nie do ko^nca "+
        "dopasowanych do siebie desek. Teraz s^a ";

    if(TO->query_open() == 1)
    {
        str += "otwarte.";
    }
    else
    {
        str += "zamkni^ete.";
    }

    str += "\n";

    return str;
}
