/*
 * drzwi do sali wspolnej w karczmie w Pianie
 * opis: Jasmina
 * popsu^la: faeve
 * dn. 15.07.07
 */

inherit "/std/door";

#include <pl.h>
#include <macros.h>
#include "dir.h"

string drzwi();

void
create_door()
{
    ustaw_nazwe("drzwiczki");
    dodaj_przym("niewielki","niewielcy");
    dodaj_przym("solidny","solidni");

    set_other_room(PIANA_KARCZMA + "lokacje/pokoj_e.c");
    set_door_id("DRZWI_POKOJ_E_KARCZMA_PIANA");
    set_long(&drzwi());

    set_open_desc("");
    set_closed_desc("");

    set_pass_command(({"e","drzwiczki","wschodnie drzwiczki"}),"do pokoju na wschodzie");

    set_open(0);
    set_locked(0);

    set_key("KLUCZ_DRZWI_POKOJE_KARCZMA_PIANA");
    set_lock_name(({"zamek", "zamku", "zamkowi", "zamek", "zamkiem", "zamku"}), 0, PL_MESKI_NOS_NZYW);
}

string
drzwi()
{
    string str;

    str = "Niewielkie drzwiczki prowadz^ace do pokoju na wschodzie zbito z "+
        "solidnych, mocnych desek. W tej chwili s^a ";

    if(TO->query_open() == 1)
        str += "otwarte.";
    else
        str += "zamkni^ete.";

    str += "\n";

    return str;
}