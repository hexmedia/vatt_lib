/*
 * drzwi z pokoju na zachodzie w karczmie w Pianie
 * opis: Jasmina
 * popsu^la: faeve
 * dn. 16.07.07
 */

inherit "/std/door";

#include <pl.h>
#include <macros.h>
#include "dir.h"

string drzwi();

void
create_door()
{
    ustaw_nazwe("drzwiczki");

    set_other_room(PIANA_KARCZMA + "lokacje/korytarzyk");
    set_door_id("DRZWI_POKOJ_E_KARCZMA_PIANA");
    set_long(&drzwi());

    dodaj_przym("niewielki","niewielcy");
    dodaj_przym("solidny","solidni");

    set_open_desc("");
    set_closed_desc("");

    set_pass_command(({"w","drzwiczki","wyj^scie","korytarz"}),
        "na korytarz");

    set_open(0);
    set_locked(0);

    set_key("KLUCZ_DRZWI_POKOJE_KARCZMA_PIANA");
    set_lock_name(({"zamek", "zamku", "zamkowi", "zamek", "zamkiem", "zamku"}), 0, PL_MESKI_NOS_NZYW);
}

string
drzwi()
{
    string str;

    str = "Niewielkie drzwiczki zbite z solidnych, mocnych desek. W tej "+
        "chwili s^a ";

    if(TO->query_open() == 1)
        str += "otwarte.";
    else
        str += "zamkni^ete.";

    str += "\n";

    return str;
}
