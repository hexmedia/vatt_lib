/*
 * drzwi do karczmy w Pianie
 * opis: Jasmina
 * popsu^la: faeve
 * dn. 07.07.07
 */

inherit "/std/door";

#include <pl.h>
#include <macros.h>
#include "dir.h"

string drzwi();

void
create_door()
{
    ustaw_nazwe("drzwi");
    dodaj_przym("mocny","mocni");
    dodaj_przym("okuty", "okuci");

    set_other_room(PIANA_KARCZMA + "lokacje/sala.c");
    set_door_id("DRZWI_DO_KARCZMY_PIANA");
    set_door_desc(&drzwi());

    set_open_desc("");
    set_closed_desc("");

    set_pass_command(({"drzwi","zajazd","karczma"}),"przez mocne okute "+
        "drzwi","do karczmy");

    set_open(0);
    set_locked(0);

    set_key("KLUCZ_DRZWI_DO_ZAJAZDU_BM");
    set_lock_name(({"zamek", "zamku", "zamkowi", "zamek", "zamkiem", "zamku"}), 0, PL_MESKI_NOS_NZYW);
}

string
drzwi()
{
    string str;

    str = "Mocne, okute drzwi wykonane z d^ebowego drewna. Teraz s^a ";

    if(TO->query_open() == 1)
        str += "otwarte.";
    else
        str += "zamkni^ete.";

    str += "\n";

    return str;
}
