/*
 * drzwi do sali wspolnej w karczmie w Pianie
 * opis: Jasmina
 * popsu^la: faeve
 * dn. 15.07.07
 */

inherit "/std/door";

#include <pl.h>
#include <macros.h>
#include "dir.h"

void
create_door()
{
    ustaw_nazwe("drzwi");
    dodaj_przym("stary","starzy");
    dodaj_przym("zniszczony", "zniszczeni");

    set_other_room(PIANA_KARCZMA + "lokacje/wspolny.c");
    set_door_id("DRZWI_DO_KARCZMY_PIANA");
    set_door_desc(VBFC_ME("drzwi"));

    set_open_desc("");
    set_closed_desc("");

    set_pass_command(({"n","sala wsp^olna","sala sypialna","drzwi"}),"do sali sypialnej", "z sali wsp�lnej");

    set_open(0);
    set_locked(0);

    set_key("KLUCZ_DRZWI_DO_ZAJAZDU_BM");
    set_lock_name(({"zamek", "zamku", "zamkowi", "zamek", "zamkiem", "zamku"}), 0, PL_MESKI_NOS_NZYW);
}

string
drzwi()
{
    string str;

    str = "Zbite z desek, pe^lne dziur po s^ekach i niezidentyfikowanych "+
        "bruzd, drzwi prowadz^ace do wsp^olnej sali sypialnej zosta^ly "+
        "niestarannie, krzywo powieszone na zawiasach. W tej chwili s^a ";

    if(TO->query_open() == 1)
        str += "otwarte.";
    else
        str += "zamkni^ete.";

    str += "\n";

    return str;
}