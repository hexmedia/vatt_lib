/*
 * podworze karczmy w Pianie
 * opis by Jasmina
 * zepsula Faeve
 *
 */

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <pogoda.h>
#include "dir.h"

inherit PIANA_STD;
inherit "/lib/drink_water";

void
create_piana()
{
    set_short("Podw^orze karczmy.");
    set_long("@@dlugi_opis@@\n");

    add_exit(PIANA_LOKACJE_ULICE + "u07.c","s",0,1,0);
    add_object(PIANA_KARCZMA + "lokacje/drzwi/do_karczmy");
    add_object(PIANA_KARCZMA + "lokacje/drzwi/do_stajni");
    add_object(PIANA_KARCZMA + "przedmioty/lampka",2);
    dodaj_rzecz_niewyswietlana("lampka",2);

add_item("wiadro","Du^ze wiadro wykonano z powyginanej ju^z, miejscami "+
    "dziurawej blachy. I cho^c nie da si^e go nape^lni^c do pe^lna z powodu "+
    "kilku niedu^zych otwor^ow, na pewno mo^ze s^lu^zy^c okolicznym "+
    "mieszka^ncom do nabierania wody w celu ugaszenia pragnienia. \n");
    set_drink_places(({"ze studni","z wiadra"}));


add_item("karczm^e","Niewysoki budynek, podmurowany fundamentem u^lo^zonym "+
    "z niestarannie dopasowanych kamieni. ^Sciany wzniesiono z grubych, "+
    "solidnych desek, kt^orych jeden rz^ad, tu^z pod oknami zdobiony "+
    "zosta^l prostym, ^z^lobionym w drewnie wzorkiem, w kt^orym osiad^l "+
    "brud i kurz podw^orca. Okna s^a szczelnie zas^loni^ete burymi "+
    "zas^lonami. Pi^etro ca^le przykryte zosta^lo spadzistym dachem "+
    "u^lo^zonym z brunatnych dach^owek. Zdobi^a go ma^le, wykuszowe okna. "+
    "Wej^scia broni^a mocne, d^ebowe drzwi, a po ich bokach zawieszono "+
    "dwie, niewielkie lampki oliwne, zapewne maj^ace o^swietla^c wej^scie "+
    "ciemn^a noc^a. \n");

add_item("stajni^e","Budynek stajni wzniesiono w ca^lo^sci z drewna, w "+
    "dodatku z niezbyt dobrze dopasowanych desek. Pomi^edzy szczelinami i w "+
    "dziurach po wielu s^ekach dostrzec mo^zna stogi siana i przegrody dla "+
    "koni znajduj^ace si^e wewn^atrz budynku. Dach pokryto s^lom^a, za^s "+
    "wej^scia broni^a proste, zbite z desek wrota. \n");

add_item("studni^e","Murowana studnia wzniesiona zosta^la z r^o^znorakich "+
    "polnych kamieni o nie zawsze pasuj^acych do siebie kszta^ltach. Z "+
    "kilku nie do ko^nca prostych ga^l^ezi sklecono ko^lowr^ot, na kt^ory "+
    "nawini^ety zosta^l mocny, konopny sznur. Na cembrowinie stoi "+
    "przywi^azane do niego sporej wielko^sci powyginane wiadro. \n");

add_sit("na ziemi","na ziemi","z ziemi",4);

set_event_time(50.0);
add_event("@@evenciki:"+file_name(TO)+"@@");
}

void
init()
{
    ::init();
    init_drink_water();
}

string
dlugi_opis()
{
    string str;

    str = "Podw^orzec przed gospod^a nie zachwyca ni wielko^sci^a, ni "+
        "specjalnie czystym utrzymaniem. Jest to w^la^sciwie otwarty z "+
        "dw^och stron, niewielki placyk, na ^srodku kt^orego wzniesiono "+
        "murowan^a studni^e. Na jej cembrowinie stoi du^ze, powyginane "+
        "wiadro. Na p^o^lnocy wznosi si^e g^l^owny budynek karczmy, "+
        "stoj^acy na podmurowanym kamieniem fundamencie. ^Sciany jednak "+
        "postawiono ju^z z grubych, solidnych desek. Parter budowli zdobi "+
        "szereg okien, pi^etro za^s posiada ^sci^ete spadzistym dachem "+
        "^sciany, w kt^orych tkwi kilka niedu^zych, wykuszowych okienek. "+
        "Wschodni^a granic^e podw^orza stanowi budynek niewielkiej stajni, "+
        "postawionej w ca^lo^sci z drewna, do kt^orej dost^epu broni^a "+
        "zbite z desek wrota. Na zachodzie dostrzegasz inne zabudowania i "+
        "wij^ace si^e pomi^edzy nimi uliczki, od kt^orych podw^orza nie "+
        "dzieli ^zaden p^lot czy murek. Podobnie jest na po^ludniu, gdzie "+
        "uliczka wiod^aca do karczmy wchodzi bezpo^srednio na podw^orze, "+
        "bez ^zadnej broni^acej wej^scia bramy. ";

    if(jest_dzien())
        {
        str += "Stoj^ace na niebie s^lo^nce bezlito^snie obna^za widoczny w "+
            "obej^sciu brud i zaniedbanie widoczne na odrapanych budynkach. ";
        }
        else
            {
            str += "Ciemno^s^c spowi^la ca^le obej^scie i tylko dwie zawieszone "+
                "przy drzwiach gospody lampki rzucaj^a nieco swiat^la na "+
                "podw^orzec. ";
            }

    return str;

}

public string
exits_description()
{
    return "Znajduj^ace si^e na p^o^lnocy drzwi prowadz^a do karczmy, wrota "+
        "na wschodzie - do stajni, za^s na po^ludniu wida^c brukowan^a "+
        "uliczk^e. \n";
    }

string
drzwi()
{
    string str;

    str = "Mocne, okute drzwi wykonane z d^ebowego drewna. Teraz s^a ";

    if(TO->query_open() == 1)
        {
        str += "otwarte.";
        }
        else
            {
            str += "zamkni^ete.";
            }

    return str;
}

string
wrota()
{
    string str;

    str = "Mocne, okute drzwi wykonane z d^ebowego drewna. Teraz s^a ";

	if(TO->query_open() == 1)
	{
		str += "otwarte.";
	}
	else
	{
		str += "zamkni^ete.";
	}

	return str;
}

string
evenciki()
{
   switch(pora_dnia())
   {
       case MT_SWIT:
       case MT_WCZESNY_RANEK:
       case MT_RANEK:
       case MT_POLUDNIE:
       case MT_POPOLUDNIE:
           return ({"Co^s plusn^e^lo w g^l^ebi studni.\n","Wiatr wzbi^l "+
           "tuman kurzu na podw^orcu.\n","Od strony stajni dochodzi "+
           "ci^e ciche r^zenie.\n","Od strony gospody dochodzi ci^e brz^ek "+
           "t^luczonego szk^la.\n","Jedna z lampek zako^lysa^la si^e na "+
           "wietrze.\n"})[random(5)];
       default:
           return ({"Co^s plusn^e^lo w ciemno^sci.\n","Od wschodu dochodzi "+
           "ci^e ciche r^zenie.\n","Od strony gospody dochodzi ci^e brz^ek "+
           "t^luczonego szk^la.\n","^Swiat^lo mign^e^lo w jednym z okien "+
           "gospody.\n","Jedna z lampek zamigota^la i przygas^la "+
           "na chwil^e.\n"})[random(5)];
   }
}