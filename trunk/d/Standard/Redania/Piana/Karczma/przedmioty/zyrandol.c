inherit "/std/object.c";

#include "dir.h"
#include <pl.h>
#include <stdproperties.h>
#include <macros.h>
#include <filter_funs.h>

create_object()
{
    ustaw_nazwe("^zyrandol");

    set_long("Zrobiony ze zwyk^lego ko^la od wozu ^zyrandol mie^sci na swym "+
        "obwodzie osiem ^swiec, kt^ore pal^a si^e jasnym ^swiat^lem. \n");


    add_prop(OBJ_I_WEIGHT, 1152);
    add_prop(OBJ_I_VOLUME, 743);
    add_prop(OBJ_I_VALUE, 0);
    add_prop(OBJ_M_NO_GET, "Niby jak chcesz to wzi��? �yrandol przyczepiony "+
        "jest do sufitu.");
}