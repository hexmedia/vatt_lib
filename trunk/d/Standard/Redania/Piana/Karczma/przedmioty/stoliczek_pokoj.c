/*
 * stoliczek do pokojow w karczmie w Pianie
 * Opis: Jasmina
 * zepsula faeve
 * dn. 16.07.07
 *
 */

#pragma strict_types

inherit "/std/container";

#include "dir.h"
#include <pl.h>
#include <macros.h>
#include <filter_funs.h>
#include <stdproperties.h>
#include <composite.h>
#include <cmdparse.h>
#include <object_types.h>
#include <materialy.h>

nomask void
create_container()
{
    ustaw_nazwe("stoliczek");
    dodaj_przym("niedu^zy","nieduzi");
	    dodaj_przym("kwadratowy","kwadratowi");

    set_long("Niedu^zy, kwadratowy stoliczek o porysowanym i poplamionym "+
        "blacie. \n");


    ustaw_material(MATERIALY_DR_DAB);
    setuid();
    seteuid(getuid());
    add_prop(CONT_I_CANT_WLOZ_DO, 1);
    add_prop(CONT_I_MAX_VOLUME, 13000);
    add_prop(CONT_I_VOLUME, 5800);

    add_prop(CONT_I_WEIGHT, 3000);
    add_prop(CONT_I_MAX_WEIGHT, 10000);

    add_prop(CONT_I_DONT_SHOW_CONTENTS, 1);

    add_subloc("na");
    add_subloc_prop("na", CONT_I_MAX_VOLUME, 10700);
    add_subloc_prop("na", CONT_I_MAX_WEIGHT, 8000);
    
    
    set_type(O_MEBLE);
    set_owners(({PIANA_STD_NPC + "karczmarz.c"}));

}


public int
jestem_stoliczkiem_karczmy_piana()
{
        return 1;
}
