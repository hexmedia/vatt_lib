/* Autor: Faeve
  Opis : Jasmina
  Data : sierpie^n 2007
*/

inherit "/std/holdable_object";

#include "/sys/macros.h"
#include "/sys/pl.h"
#include "/sys/materialy.h"
#include "/sys/object_types.h"
#include "/sys/formulas.h"
#include "dir.h"

void
create_holdable_object()
{
    ustaw_nazwe("szpadel");
    dodaj_przym("du^zy","duzi");
    dodaj_przym("u^lamany", "u^lamani");

    dodaj_nazwy("narz^edzie");

    set_long("Du^zy szpadel z nieco przykr^otkim, u^lamanym niegdy^s "+
        "trzonkiem. \n");
         
    add_prop(OBJ_I_WEIGHT, 1700);
    add_prop(OBJ_I_VOLUME, 1700);
    add_prop(OBJ_I_VALUE, 11);
    add_prop(OBJ_I_DONT_GLANCE, 1);
    set_type(O_NARZEDZIA);

    ustaw_material(MATERIALY_RZ_STAL, 25);
    ustaw_material(MATERIALY_DR_DAB, 75);
    set_owners(({PIANA_NPC + "karczmarz"}));

    

}
public int
jestem_narzedziem_karczmyp()
{
        return 1;
}