inherit "/std/object.c";

#include "dir.h"
#include <pl.h>
#include <stdproperties.h>
#include <macros.h>
#include <filter_funs.h>

create_object()
{
    ustaw_nazwe("lampka oliwna");
    dodaj_nazwy("lampka");

    set_long("Niedu^za, oliwna lampka wisi na przywi^azanym do belki w "+
        "stropie sznurze. Cho^c zapalona - daje niewiele ^swiat^la, przez "+
        "co k^aty pomieszczenia ton^a w mroku. \n");


    add_prop(OBJ_I_WEIGHT, 764);
    add_prop(OBJ_I_VOLUME, 521);
    add_prop(OBJ_I_VALUE, 0);
    add_prop(OBJ_M_NO_GET, "Nic z tego - lampka przywiązana jest do belki "+
        "stropowej. ");
}