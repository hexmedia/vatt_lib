/* Autor: Faeve
  Opis : Samaia
  Data : 24 marca 2008
*/

inherit "/std/holdable_object";

#include "/sys/macros.h"
#include "/sys/pl.h"
#include "/sys/materialy.h"
#include "/sys/object_types.h"
#include "/sys/formulas.h"
#include "dir.h"

void
create_holdable_object()
{
    ustaw_nazwe("worek");
    dodaj_przym("pojemny","pojemni");
    dodaj_przym("jutowy", "jutowi");

    set_long("Worek, wykonany z ciasno zwartych w^l^okien juty, wype^lniony "+
        "zosta^l po brzegi wonn^a pasz^a dla zwierz^at. S^a ni^a przede "+
        "wszystkim suche siano i s^loma, acz zza  z^locistych ^lodyg "+
        "wystaj^a tak^ze drobne ga^l^azki obsypane kruchymi, wysuszonymi co "+
        "do kropli wody, li^scmi. Ca^ly worek, wielkosci niskiego wzrostu "+
        "krasnoluda, z pewno^sci^a jest w stanie zaspokoic g^l^od ka^zdego "+
        "wyczerpanego podr^o^z^a wierzchowca, po ca^lym dniu ci^e^zkiej "+
        "pracy.\n");
         
    add_prop(OBJ_I_WEIGHT, 10000);
    add_prop(OBJ_I_VOLUME, 7000);
    add_prop(OBJ_I_VALUE, 25);

    make_me_sitable("na","na worku","z worka",1, 
       PL_MIE, "na");

    ustaw_material(MATERIALY_JUTA, 100); 
    set_owners(({PIANA_NPC + "karczmarz"}));

    add_prop(OBJ_I_DONT_GLANCE, 1);;

}