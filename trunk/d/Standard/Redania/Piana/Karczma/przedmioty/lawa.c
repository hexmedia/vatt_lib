/*
 * stol w karczmie w Pianie
 * Opis: Jasmina
 * zepsula faeve
 * dn. 8.07.07
 *
 */

#pragma strict_types

inherit "/std/container";

#include "dir.h"
#include <pl.h>
#include <macros.h>
#include <filter_funs.h>
#include <stdproperties.h>
#include <composite.h>
#include <cmdparse.h>
#include <object_types.h>
#include <materialy.h>

nomask void
create_container()
{
    ustaw_nazwe("^lawa");
    dodaj_przym("stary","starzy");
	    dodaj_przym("d^ebowy","d^ebowi");

    set_long("Prosta, d^ebowa ^lawa z porysowanym i poplamionym blatem, "+
        "kt^ory zapewne od dawna nie widzia^l ^scierki. \n");


    ustaw_material(MATERIALY_DR_DAB);
    setuid();
    seteuid(getuid());
    add_prop(CONT_I_MAX_VOLUME, 20000);
    add_prop(CONT_I_VOLUME, 15800);

    add_prop(CONT_I_WEIGHT, 15000);
    add_prop(CONT_I_MAX_WEIGHT, 90000);

    add_prop(CONT_I_DONT_SHOW_CONTENTS, 1);

    add_subloc("na");
//    set_owners(({"karczmarz.c"}));

}

public int
query_type()
{
	return O_MEBLE;
}

