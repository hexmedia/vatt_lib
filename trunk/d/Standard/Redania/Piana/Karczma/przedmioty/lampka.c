/* Autor: faeve
 * Opis : Jasmina
 * Data : 7.07.07 */

inherit "/std/lampa_uliczna";
#include <stdproperties.h>
#include <macros.h>
#include "dir.h"
void
create_street_lamp()
{
	ustaw_nazwe("lampka");
   set_long("@@dlugasny@@\n");
   set_extinguish_message("Jeden ze s^lug gasi lampk^e.\n");
}

string
dlugasny()
{
	string str;

	str = "Niewielka lampka olejna wisz^aca na kr^otkim dr^agu "+
		"przymocowanym tu^z obok drzwi. ";
	
	if(environment(TO)->jest_dzien() == 1)
    {
		str += "Zgaszona za dnia ko^lysze si^e sm^etnie przy ka^zdym "+
			"podmuchu wiatru. ";
	}
	else
	{
		str += "Rozpalona, nie daje zbyt wiele ^swiat^la, do^s^c jednak, "+
			"by nie potkn^a^c si^e o pr^og gospody. ";
	}

	return str;
}
