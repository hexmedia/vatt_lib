/* zaslona super-Gjoefowa!
 * do karczmy w Pianie
 * opis napisaaa Jasmina
 * a wkleila do edytora faeve :p
 * dn. 16.07.07
 * chwa^la Swarozyca!
 */

#include "dir.h"
#include <stdproperties.h>
#include <macros.h>

inherit "/std/object";
inherit "/lib/peek";


create_object()
{
    ustaw_nazwe("zas^lony");

    dodaj_nazwy("zas^lony");
    
    set_long("Brunatne, pruj^ace si^e u do^lu zas^lony. \n");
    
    add_prop(OBJ_M_NO_GET, "Jak chcesz je niby zabra^c? S^a przyczepione do "+
        "okna i raczej nie uda si^e ich odczepi^c!\n");
    add_prop(CONT_I_MAX_VOLUME, 30000);
    add_prop(OBJ_I_VOLUME, 70000);

    set_no_show_composite(1);
}

int
odsun(string str)
{
    if (str ~= "zas^lon^e" || str ~= "zas^lony")
    {
        if (TO->query_prop("odslonieta"))
        {
        TP->catch_msg("Zas^lona jest ju^z odsuni^eta.\n");
        return 1;
        }
        else
        {
        TP->catch_msg("Chwytaj^ac zas^lon^e szybkim ruchem ods^laniasz "+
            "okno.\n");
        saybb(QCIMIE(TP, 0) + " chwytaj^ac zas^lon^e szybkim ruchem "+
            "ods^lania okno.\n");
        
        TO->add_prop("odslonieta", 1);
        TO->add_peek("przez okno", PIANA_KARCZMA + "lokacje/podworze.c");

        
        return 1;
        }
    }
    else
    {
        notify_fail(capitalize(query_verb()) + " co?\n");
        return 0;
    }
}

int
zasun(string str)
{
    if (str ~= "zas^lon^e" || str ~= "zas^lony")
    {
        if (TO->query_prop("odslonieta") == 0)
        {
            TP->catch_msg("Ale^z zas^lona jest ju^z zasuni^eta.\n");
            return 1;
        }
        else
        {
        TP->catch_msg("Szybkim ruchem zasuwasz zas^lon^e.\n");
        saybb(QCIMIE(TP, 0) + " szybkim ruchem zasuwa zas^lon^e.\n");

        TO->add_prop("odslonieta", 0);
        TO->remove_peek("przez okno", PIANA_KARCZMA + "podworze.c");

        }

        return 1;
    }
    else
    {
        notify_fail(capitalize(query_verb()) + " co?\n");
        return 0;
    }
}

void
init()
{
    ::init();
    init_peek();
    add_action("odsun", "odsu^n");
    add_action("zasun", "zasu^n");
    add_action("odsun", "ods^lo^n");
    add_action("zasun", "zas^lo^n");
}