   /* 
   
   Autor: Faeve
   Opis : Jasmina
   Data : 16.07.07 
   poprawka, co by dywan klonowal sie rozwiniety - ok 10.06.09
   
   */

inherit "/std/object";

#include "dir.h"
#include <cmdparse.h>
#include <pl.h>
#include <macros.h>
#include <filter_funs.h>
#include <stdproperties.h>
#include <object_types.h>
#include <materialy.h>

int zwin_dywan(string str);
int rozwin_dywan(string str);
int czy_siedzi_na_dywanie(object kto);

string opis_zwiniety();

void create_object()
{
    ustaw_nazwe("dywanik");
    dodaj_przym("dziurawy", "dziurawi");
    dodaj_przym("brunatny", "brunatni");

    set_long("Brunatny i niewyszukany, pe^len jest przetar^c i dziur - w "+
        "niekt^ore ^smia^lo zmie^sci si^e nawet bardzo gruby paluch. \n"+
		"@@opis_zwiniety@@\n");
    add_prop(OBJ_I_WEIGHT, 400);
    add_prop(OBJ_I_VOLUME, 250);
    add_prop(OBJ_I_VALUE, 5);
    add_prop(OBJ_M_NO_GET, "Dywanik jest troch^e niepor^eczny. Mo^ze lepiej"
            +" by^loby go najpierw zwin^a^c?\n");
    make_me_sitable("na", "wygodnie na dywaniku", "leniwie z dywanika", 2);

    set_type(O_MEBLE);
    ustaw_material(MATERIALY_SK_NIEDZWIEDZ);
    //set_owners(({ZAJAZD + "karczmarz"})); !!!!!!!!!! TODO
}

init()
{
    ::init();
    add_action(&zwin_dywan(), "zwi^n");
    add_action(&rozwin_dywan(), "rozwi^n");
}

int zwin_dywan(string str)
{
    object obj;
    object *siedzacy;

    if(!str) return 0;

    if(!parse_command(lower_case(str), environment(this_player()), "%o:"+PL_BIE, obj))
        return 0;

    if (obj != this_object())
        return 0;

    if(!this_object()->query_prop(OBJ_M_NO_GET))
    {
        notify_fail("Przecie^z dywanik jest ju^z zwini^ety!\n");
        return 0;
    }

    siedzacy = filter(FILTER_LIVE(all_inventory(environment(this_object()))), czy_siedzi_na_dywanie);

    if(sizeof(siedzacy) > 0)
    {
        notify_fail("Kto^s siedzi na dywaniku - nie mo�esz go zwin^a�^c.\n");
        return 0;
    }

    write("Zwijasz dywanik w nieco niezgrabny rulon.\n");
    saybb(QCIMIE(this_player(), PL_MIA)+" zwija "+this_object()->short(PL_BIE)
            +" w niezgrabny rulon.\n");
    remove_prop(OBJ_M_NO_GET);
    remove_prop("_siedzonko");
    if((ENV(TO)->query_prop(ROOM_I_IS)) == 1)
        ENV(TO)->remove_sit(sit_cmd, sit_space, ({ this_object() }));
    return 1;
}

int rozwin_dywan(string str)
{
    object obj;

    if(!str)
        return 0;

    if(!parse_command(lower_case(str), environment(this_player()), "%o:"+PL_BIE, obj))
        return 0;

    if(obj != this_object())
        return 0;

    if(function_exists("create_container", environment(this_object())) != "/std/room")
    {
        notify_fail("Mo^ze najpierw go gdzie^s po^lo^zysz?\n");
        return 0;
    }

    if(!this_object()->query_prop(OBJ_M_NO_GET))
    {
        write("Kilkoma sprawnymi ruchami rozwijasz dywanik.\n");
        saybb(QCIMIE(this_player(), PL_MIA)+" kilkoma sprawnymi ruchami rozwija "
            +this_object()->short(PL_BIE)+".\n");
        add_prop(OBJ_M_NO_GET, "Dywanik jst troch^e niepor^eczny. Mo^ze lepiej"
            +" by^loby go najpierw zwin^a^c?\n");
        make_me_sitable("na", "wygodnie na dywaniku", "leniwie z dywanika", 2);
        if((ENV(TO)->query_prop(ROOM_I_IS)) == 1)
            ENV(TO)->add_sit(sit_cmd, sit_long, sit_up, sit_space, ({ TO }), sit_przyp, sit_przyimek);
        return 1;
    }
    notify_fail("Przecie^z dywanik jest ju^z rozwini^ety!\n");
    return 0;
}

string opis_zwiniety()
{
  if(!this_object()->query_prop(OBJ_M_NO_GET))
    return "W tej chwili jest zwini^ety.";
  return "W tej chwili jest rozwini^ety.";
}


public nomask string
query_dywan_auto_load()
{
    if (!this_object()->query_prop(OBJ_M_NO_GET))
    {
    return " ~DYWAN_ZW~ ";
    }

    return "";
}

public nomask string
init_dywan_arg(string arg)
{
    string toReturn, foobar;

    if (arg == 0) {
        return 0;
    }

    if (wildmatch("*~DYWAN_ZW~*", arg))
    {
        sscanf(arg, "%s~DYWAN_ZW~ %s", foobar, toReturn);
        foreach (string curstr : explode(foobar, ""))
            if (curstr != " ")
                return arg;

        remove_prop(OBJ_M_NO_GET);
        remove_prop("_siedzonko");
        return toReturn;
    }
    return arg;
}

public string
query_auto_load()
{
    return ::query_auto_load() + query_dywan_auto_load();
}

public string
init_arg(string arg)
{
    return init_dywan_arg(::init_arg(arg));
}

int czy_siedzi_na_dywanie(object kto)
{
    if(kto->query_prop(PLAYER_M_SIT_SIEDZACY) == this_object() || kto->query_prop(PLAYER_M_SIT_LEZACY))
        return 1;
    return 0;
}
