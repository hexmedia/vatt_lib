/* Autor: Faeve
  Opis : Jasmina
  Data : sierpie^n 2007
*/

inherit "/std/holdable_object";

#include "/sys/macros.h"
#include "/sys/pl.h"
#include "/sys/materialy.h"
#include "/sys/object_types.h"
#include "/sys/formulas.h"
#include "dir.h"

void
create_holdable_object()
{
    ustaw_nazwe("wid^ly");
    dodaj_przym("zwyk^ly","zwykli");
    dodaj_przym("prosty", "pro^sci");

    dodaj_nazwy("narz^edzie");

    set_long("Osadzone na d^lugim trzonku dwa gro^xnie wygl^adaj^ace z^eby "+
        "to nic innego, jak wid^ly - s^lu^z^a najcz^e^sciej do przerzucania "+
        "siana. \n");
         
    add_prop(OBJ_I_WEIGHT, 1200);
    add_prop(OBJ_I_VOLUME, 1200);
    add_prop(OBJ_I_VALUE, 10);
    set_type(O_NARZEDZIA);

    ustaw_material(MATERIALY_STAL, 30); 
    ustaw_material(MATERIALY_DR_BUK, 70);
    set_owners(({PIANA_NPC + "karczmarz"}));

    add_prop(OBJ_I_DONT_GLANCE, 1);
}
public int
jestem_narzedziem_karczmyp()
{
        return 1;
}