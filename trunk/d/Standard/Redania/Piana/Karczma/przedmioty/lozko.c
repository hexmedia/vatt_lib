/*
 * lozko do sypialni w karczmie w Pianie
 * Opis: Jasmina
 * zepsula faeve
 * dn. 16.07.07
 *
 */

#pragma strict_types

inherit "/std/container";

#include "dir.h"
#include <pl.h>
#include <macros.h>
#include <filter_funs.h>
#include <stdproperties.h>
#include <composite.h>
#include <cmdparse.h>
#include <object_types.h>
#include <materialy.h>

nomask void
create_container()
{
    ustaw_nazwe("^l^o^zko");

    set_long("Proste, zbite z desek ^l^o^zko wyposa^zone zosta^lo jedynie w "+
        "stary, brudny siennik. \n");


    ustaw_material(MATERIALY_DR_DAB);
    setuid();
    seteuid(getuid());
    add_prop(CONT_I_MAX_VOLUME, 3000);
    add_prop(CONT_I_WEIGHT, 3000);
    add_prop(OBJ_M_NO_GET, "To jednak, mimo wszystko, troch^e niewygodne do "+
        "noszenia - niech lepiej zostanie w miejscu, w kt^orym stoi. \n");

add_subloc("na");
    add_subloc_prop("na", CONT_I_MAX_WEIGHT, 100000);
    add_subloc_prop("na", CONT_I_MAX_VOLUME, 150000);
    add_prop(CONT_I_DONT_SHOW_CONTENTS, 1);
    add_prop(CONT_I_CANT_WLOZ_DO, 1);


    make_me_sitable("na","na ^l^o^zku","z ^l^o^zka",2);
//    set_owners(({"karczmarz.c"}));

}

public int
query_type()
{
	return O_MEBLE;
}
