#include "../dir.h"

#define PIANA_LOKACJE_BRZEG     (PIANA_LOKACJE + "brzeg/")
#define PIANA_DRZWI             (PIANA_LOKACJE + "drzwi/")
#define PIANA_PRZEDMIOTY        (PIANA + "przedmioty/")
#define PIANA_NARZEDZIA         (PIANA_PRZEDMIOTY + "narzedzia/")
#define PIANA_UBRANIA           (PIANA_PRZEDMIOTY + "ubrania/")
#define PIANA_BRONIE            (PIANA_PRZEDMIOTY + "bronie/")
#define PIANA_ZBROJE            (PIANA_PRZEDMIOTY + "zbroje/")
#define PIANA_STD               (PIANA + "std/piana")
#define STREET_STD              (PIANA + "std/street")
#define PIANA_STD_B             (PIANA + "std/piana_b")
#define PIANA_STD_NPC           (PIANA + "std/npc")
#define PIANA_KARCZMA           (PIANA + "Karczma/")

#define PIANA_SLEEP_PLACE             (PIANA_KARCZMA + "lokacje/wspolny.c")

#define POCHODZENIE		  "z Piany"

#define BURMISTRZ         "Ardgal"
#define BURMISTRZA        "Ardgala"
