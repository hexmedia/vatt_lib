inherit "/std/pattern";

#include <pattern.h>
#include "dir.h"

public void
create()
{
  set_pattern_domain(PATTERN_DOMAIN_TAILOR);
  set_item_filename(PIANA_UBRANIA +"koszule/dluga_blekitna_Mc");
  set_estimated_price(80);
  set_time_to_complete(1500);
  enable_want_sizes();
}