inherit "/std/pattern";

#include <pattern.h>
#include "dir.h"

public void
create()
{
  set_pattern_domain(PATTERN_DOMAIN_TAILOR);
  set_item_filename(RINDE_UBRANIA +"bluzki/skromna_kremowa_Sc");
  set_estimated_price(65);
  set_time_to_complete(2000);
  enable_want_sizes();
}