inherit "/std/pattern";

#include <pattern.h>
#include "dir.h"

public void
create()
{
  set_pattern_domain(PATTERN_DOMAIN_TAILOR);
  set_item_filename(PIANA_UBRANIA +"spodnie/szare_welniane_Mc");
  set_estimated_price(35);
  set_time_to_complete(1500);
  enable_want_sizes();
}
