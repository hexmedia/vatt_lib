inherit "/std/pattern";

#include <pattern.h>
#include "dir.h"

public void
create()
{
  set_pattern_domain(PATTERN_DOMAIN_TAILOR);
  set_item_filename(BIALY_MOST_UBRANIA +"sandaly/jasne_drewniane_XLc");
  set_estimated_price(10);
  set_time_to_complete(500);
  enable_want_sizes();
}