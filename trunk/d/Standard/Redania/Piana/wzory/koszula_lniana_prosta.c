inherit "/std/pattern";

#include <pattern.h>
#include "dir.h"

public void
create()
{
  set_pattern_domain(PATTERN_DOMAIN_TAILOR);
  set_item_filename(BIALY_MOST_UBRANIA +"koszule/lniana_prosta_Mc");
  set_estimated_price(20);
  set_time_to_complete(1700);
  enable_want_sizes();
}