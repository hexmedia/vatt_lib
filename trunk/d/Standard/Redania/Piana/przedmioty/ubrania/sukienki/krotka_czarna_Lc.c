/**
 * 06.08.2007
 *  
 * Autor: Daarth
 * Opis : Samaia
 */

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <pl.h>
#include <macros.h>
#include <materialy.h>
#include <object_types.h>
#include <wa_types.h>

void
create_armour()
{  
  ustaw_nazwe("sukienka");
  
  dodaj_przym("kr^otki","kr^otcy");
  dodaj_przym("czarny", "czarni");
  
  set_long(
    "Prosta sukienka zosta^la uszyta z dw^och kawa^lkow g^ladkiego, "+
    "lej�cego materia^lu - marszczonego, okrywaj�cego dekolt, oraz "+
    "plisowanego sp^lywaj�cego od biustu po kolana. Na piersi wyhaftowano "+
    "czerwon� nici� rozwinet� r^o^z^e, za� spod czarnej falbany wystaje "+
    "karminowa, koronkowa halka. Regulowane, cieniutkie rami�czka, a tak^ze "+
    "zupe^lnie niewidoczny guzik z boku u^latwiaj� za^lo^zenie jej na cia^lo.\n"
  );

  ustaw_material(MATERIALY_JEDWAB, 85);
	ustaw_material(MATERIALY_SATYNA, 15);

  set_slots(A_TORSO, A_LEGS, A_BODY);
  
  add_prop(OBJ_I_WEIGHT, 400);
  add_prop(OBJ_I_VOLUME, 400);
  add_prop(OBJ_I_VALUE, 72);
  add_prop(ARMOUR_I_D_ROZCIAGLIWOSC, 40);
  add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
  add_prop(ARMOUR_I_DLA_PLCI, 1);

  set_size("L");
  set_type(O_UBRANIA);
  set_likely_cond(19);
}
  