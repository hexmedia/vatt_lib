inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <macros.h>
#include <materialy.h>
#include <object_types.h>
#include <pl.h>

void
create_armour()
{
    ustaw_nazwe("sukienka");
    dodaj_przym("prymitywny","prymitywni");
    dodaj_przym("jutowy","jutowi");

    set_long("Prosta sukienka, przypominaj^aca raczej fartuch, "
		+"zosta^la zszyta z dw^och prostok^atnych kawa^lk^ow sztywnej juty. "
		+"Odzienie z pewno^sci^a nie nale^zy do najwygodniejszych, "
		+"przeciwnie, materia^l zdaje si^e by^c jednym z najmniej "
		+"odpowiednich do wykonywania ubra^n. Tworzywo jest wyj^atkowo "
		+"szorstkie, sztywne, oraz niewygodne i mimo grubo^sci dzianiny "
		+"z pewno^sci^a nie ochroni w^la^sciciela przed zimnem, a ju^z na "
		+"pewno nie przed opadami. Ca^la sukienka trzyma si^e dzi^eki "
		+"dw^om rami^aczkom, b^ed^acymi w rzeczywisto^sci zwyk^lymi "
		+"rzemieniami, a tak^ze prowizorycznemu trokowi z r^ownie "
		+"drapi^acej juty.\n");

    set_slots(A_TORSO, A_HIPS, A_THIGHS, A_BODY);
    add_prop(OBJ_I_WEIGHT, 700);
    add_prop(OBJ_I_VOLUME, 1000);
    add_prop(OBJ_I_VALUE, 7);
    add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
    add_prop(ARMOUR_I_DLA_PLCI, 1);
    set_type(O_UBRANIA);
    ustaw_material(MATERIALY_JUTA, 98);
    ustaw_material(MATERIALY_SK_CIELE, 2);

    set_size("S");
    set_likely_cond(21);
}