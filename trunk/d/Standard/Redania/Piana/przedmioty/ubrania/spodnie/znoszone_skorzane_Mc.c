inherit "/std/armour";
        
#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <macros.h>
#include <materialy.h>
#include <object_types.h>
#include <pl.h>
void
create_armour()
{
	ustaw_nazwe("spodnie");
	dodaj_przym("znoszony","znoszeni");
	dodaj_przym("sk^orzany","sk^orzani");
	
	set_long("To nietypowe odzienie sk^lada si^e z kr^otkich "
		+"spodenek z grubego, szarego lnu oraz przyszytych na sta^le "
		+"nogawic z ciemnej, bydl^ecej sk^ory. Jest ona w kilku miejscach "
		+"lekko poprzecierana, za^s charakterystychne zmarszczki "
		+"pokrywaj^a wszystkie miejsca gdzie wielkokrotnie by^la zaginana." 
		+"Cz^e^s^c z materia^lu tak^ze nosi slady d^lugotrwa^lego noszenia, "
		+"lecz ca^lo^s^c wykonana solidnie i z dobrych materia^l^ow, "
		+"nie zdradza niczego co mog^loby ^swiadczy^c, ^z^e nie pos^lu^zy "
		+"jeszcze kilku lat. \n");

	set_slots(A_LEGS, A_HIPS);
	add_prop(OBJ_I_WEIGHT, 1000);
	add_prop(OBJ_I_VOLUME, 1000);
	add_prop(OBJ_I_VALUE, 12);

	add_prop(ARMOUR_S_DLA_RASY, "cz^lowiek");
	add_prop(ARMOUR_I_DLA_PLCI, 0);

	ustaw_material(MATERIALY_LEN, 20);
	ustaw_material(MATERIALY_SK_CIELE, 80);

	set_type(O_UBRANIA);

	set_size("M");
    set_likely_cond(22);
}