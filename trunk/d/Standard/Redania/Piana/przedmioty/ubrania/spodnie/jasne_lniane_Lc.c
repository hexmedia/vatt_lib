
/* S^a to spodnie dla piekarza */

inherit "/std/armour";
        
#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <macros.h>
#include <materialy.h>
#include <object_types.h>
#include <pl.h>
void
create_armour()
{
	ustaw_nazwe("spodnie");
	dodaj_przym("jasny","ja^sni");
	dodaj_przym("lniany","lniani");
	
	set_long("Najzwyklejsze pod s^lo^ncem spodnie jakie mo^zna "
		+"zobaczy^c na nogach wielu ^srednio zamo^znych mieszka^nc^ow "
		+"miast, miasteczek i wsi. Standardowy materia^l, kt^orego "
		+"nie barwiono, standardowy kr^oj, standardowy rozmiar, "
		+"czyli nic specjalnego. W zamian za to przyzwoita "
		+"jako^s^c i niska cena, co sprawia, ^ze s^a dobre do codziennego "
		+"u^zytku. Te konkretne s^a ca^le pokryte m^ak^a. \n");

	set_slots(A_LEGS, A_HIPS);
	add_prop(OBJ_I_WEIGHT, 900);
	add_prop(OBJ_I_VOLUME, 1100);
	add_prop(OBJ_I_VALUE, 24);

	add_prop(ARMOUR_S_DLA_RASY, "cz^lowiek");
	add_prop(ARMOUR_I_DLA_PLCI, 0);

	ustaw_material(MATERIALY_LEN, 100);

	set_type(O_UBRANIA);

	set_size("L");

}