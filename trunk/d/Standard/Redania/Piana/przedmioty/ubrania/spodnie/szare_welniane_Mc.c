inherit "/std/armour";
        
#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <macros.h>
#include <materialy.h>
#include <object_types.h>
#include <pl.h>
void
create_armour()
{
	ustaw_nazwe("spodnie");
	dodaj_przym("szary","szarzy");
	dodaj_przym("we^lniany","we^lniani");
	
	set_long("Te zwyk^lego kroju spodnie, nieco zw^e^zone przy nogawce, "
		+"wykonane zosta^ly z ciep^lej, grubej we^lny, dzi^eki czemu bez "
		+"problemu mog^a ochroni^c w^la^sciciela przed jesiennym ch^lodem. "
		+"Brak kieszeni, wszelkiego rodzaju ozd^ob, czy te^z ^sci^agaczy, "
		+"lub sznurowa^n sprawia, i^z zdaj^a si^e by^c idealnym elementem "
		+"z^lo^zonego, warstwowego stroju. Wygl^adaj^a r^ownie^z na "
		+"wyj^atkowo wygodne i nieograniczaj^ace ruch^ow osoby je "
		+"nosz^acej.\n");

	set_slots(A_LEGS, A_HIPS);
	add_prop(OBJ_I_WEIGHT, 1100);
	add_prop(OBJ_I_VOLUME, 1100);
	add_prop(OBJ_I_VALUE, 31);

	add_prop(ARMOUR_S_DLA_RASY, "cz^lowiek");
	add_prop(ARMOUR_I_DLA_PLCI, 0);

	ustaw_material(MATERIALY_WELNA, 100);

	set_type(O_UBRANIA);

	set_size("M");

}