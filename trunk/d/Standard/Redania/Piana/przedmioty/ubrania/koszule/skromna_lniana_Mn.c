inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <macros.h>
#include <materialy.h>
#include <object_types.h>
#include <pl.h>

void
create_armour()
{
    ustaw_nazwe("koszula");

    dodaj_przym("skromny","skromni");
    dodaj_przym("lniany","lniani");

    set_long("Najprostsza, a jednocze^snie najbardziej estetyczna "
		+"koszula, jak^a mo^zna by wykona^c z pewno^sci^a wygl^ada^laby "
		+"w^la^snie tak. Czysty, ^snie^znobia^ly len, prosty kr^oj "
		+"i uniwersalne wymiary charakteryzuj^a w^la^snie ten element "
		+"garderoby. Niewielkie wci^ecie przy szyi, oraz kraw^edzie "
		+"r^ekaw^ow tradycyjnie zosta^ly obszyte szar^a krajk^a. "
		+"Koszula wygl^ada r^ownie^z na tak^a, kt^ora s^lu^zy^c mo^ze "
		+"nawet przez par^e dobrych lat. \n");

    set_slots(A_TORSO, A_FOREARMS, A_ARMS);

    add_prop(OBJ_I_WEIGHT, 400);
    add_prop(OBJ_I_VOLUME, 300);
    add_prop(OBJ_I_VALUE, 21);

    set_type(O_UBRANIA);

    ustaw_material(MATERIALY_LEN, 100);

    add_prop(ARMOUR_S_DLA_RASY, "niziolek");
    add_prop(ARMOUR_I_DLA_PLCI, 0);

    set_size("M");
}