inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <macros.h>
#include <materialy.h>
#include <object_types.h>
#include <pl.h>

void
create_armour()
{
    ustaw_nazwe("koszula");

    dodaj_przym("cienki","ciency");
    dodaj_przym("lu^xny","lu^xni");

    set_long("Utkana z bawe^lny koszula jest mi^ekka w dotyku "
		+"i bardzo przewiewna. Nie przylega mocno do cia^la, "
		+"co przy kr^otkim r^ekawie sprawia, ^ze ^swietnie nadaje "
		+"si^e na upalne dni, zw^laszcza do prac na ^swie^zym "
		+"powietrzu. Kr^oj sprawia, ^ze przeznaczona jest "
		+"raczej dla os^ob p^lci m^eskiej. \n");

    set_slots(A_TORSO, A_FOREARMS, A_ARMS);

    add_prop(OBJ_I_WEIGHT, 800);
    add_prop(OBJ_I_VOLUME, 1000);
    add_prop(OBJ_I_VALUE, 20);

    set_type(O_UBRANIA);

    ustaw_material(MATERIALY_BAWELNA, 100);

    add_prop(ARMOUR_S_DLA_RASY, "cz^lowiek");
    add_prop(ARMOUR_I_DLA_PLCI, 0);

    set_size("M");
}