inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <macros.h>
#include <materialy.h>
#include <object_types.h>
#include <pl.h>

void
create_armour()
{
    ustaw_nazwe("koszula");

    dodaj_przym("gruby","grubi");
    dodaj_przym("przepocony","przepoceni");

    set_long("Prosta koszula z jasnego lnu ozdobiona jest "
			+"olbrzymimi, zaschni^etymi plamami potu pod pachami "
			+"i na plecach. D^lugie r^ekawy, poszarpane nieco na "
			+"brzegach, nosz^a ^slady jakiego^s lepkiego mazid^la. "
			+"Materia^l mimo i^z gruby poprzeciera^l si^e ju^z "
			+"troch^e, ^swiec^ac nawet kilkoma niewielkimi "
			+"dziurkami na ^lokciach. \n");

    set_slots(A_TORSO, A_FOREARMS, A_ARMS);

    add_prop(OBJ_I_WEIGHT, 400);
    add_prop(OBJ_I_VOLUME, 800);
    add_prop(OBJ_I_VALUE, 5);

    set_type(O_UBRANIA);

    ustaw_material(MATERIALY_LEN, 100);

    add_prop(ARMOUR_S_DLA_RASY, "cz^lowiek");
    add_prop(ARMOUR_I_DLA_PLCI, 0);

    set_size("M");
    set_likely_cond(22);
}