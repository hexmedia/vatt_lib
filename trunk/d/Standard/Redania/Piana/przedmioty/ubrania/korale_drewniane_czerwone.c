/*
opis popelniony przez Samaie, zakodowany przez Seda 23.12.2007
 */


inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <macros.h>
#include <materialy.h>

void
create_armour()
{  

    ustaw_nazwe("korale");
    dodaj_przym("drewniany","drewniani");
    dodaj_przym("czerwony", "czerwoni");
    set_long("Ten sznur b^lyszcz^acych, krwistoczerwonych korali jest tak "
    +"d^lugi, i^z mo^zna by nim si^e opasa^c conajmniej dwa razy. Korale "
    +"wygl^adaj^a na bardzo wytrzyma^le, gdy^z zosta^ly wykonane z drewna, "
    +"oraz pomalowane grub^a warstw^a karminowej farby, kt^ora nadaje im "
    +"pi^ekny po^lysk i czyni odpornymi na wszelakie zadrapania. S^a r^ownie^z "
    +"solidne, dzi^eki temu, i^z zosta^ly nawleczony na mocny sznur, "
    +"dodatkowo zwi^azywany supe^lkami po ka^zdym paciorku. \n");

    set_slots(A_NECK);
    set_size("L");
    add_prop(OBJ_I_WEIGHT, 200);
    add_prop(OBJ_I_VOLUME, 300);
    add_prop(OBJ_I_VALUE, 10);
        
    /* ustawiamy rozci^agliwo^s^c w d^o^l na 80% */
    add_prop(ARMOUR_I_D_ROZCIAGLIWOSC, 80);

    /* Ustawiamy materia^ly, z kt^orych jest wykonany pas */
    ustaw_material(MATERIALY_LEN, 10);
    ustaw_material(MATERIALY_DR_BUK, 90);

}


