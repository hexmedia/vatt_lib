/* Autor: Faeve
   Data : 19.12.2007 */

inherit "/std/container";

#include "/sys/stdproperties.h"
#include "/sys/macros.h"
#include "/sys/pl.h"
#include "/sys/materialy.h"
#include "/sys/object_types.h"
#include <mudtime.h>
#include "dir.h"

void
create_container()
{
    ustaw_nazwe("biurko");
    dodaj_przym("ogromny", "ogromni");
    dodaj_przym("d^ebowy", "d^ebowi");
    ustaw_material(MATERIALY_DR_JESION);
    set_type(O_MEBLE);

    set_long("Biurko, cho^c stare - trzyma si^e wcale nie^zle, stolarz, "+
	 "kt^ory je wykona^l musia^l by^c nie lada mistrzem w swym fachu. "+
	 "Masywne, na grubych nogach, zapewne jest w stanie utrzyma^c "+
	 "nawet ogromne ilo^sci papieru - dokument^ow i list^ow, kt^orych "+
 	 "na poczcie nie brak. Niesamowitej wielko^sci blat pe^len jest "+
	 "stert kopert i kart, a niekt^re z nich musz^a le^ze^c tu ju^z "+
	 "ca^lkiem d^lugo - z pocz^atku zapewne bia^le - teraz pokryte "+
	 "s^a warstw^a kurzu. "+
        "@@opis_sublokacji|Opr^ocz papierzysk na blacie biurka "+
	 "dostrzegasz tak^ze |na|.\n|\n|" + PL_BIE+ "@@");
         
    add_prop(CONT_I_WEIGHT, 20000);
    add_prop(CONT_I_VOLUME, 15000);
    add_prop(OBJ_I_VALUE, 456);

    add_prop(OBJ_I_NO_GET, "Nie da rady tego podnie^s^c - nie do^s^c, "+
	 "^ze ci^e^zkie i pe^lne papier^ow, to jeszcze przytwierdzono "+
	 "je do pod^logi. \n");
    
    add_subloc("na");
    add_subloc_prop("na", CONT_I_MAX_WEIGHT, 100000);
    add_subloc_prop("na", CONT_I_MAX_VOLUME, 150000);
    add_prop(CONT_I_DONT_SHOW_CONTENTS, 1);
    add_prop(CONT_I_CANT_WLOZ_DO, 1);

    //set_owners(({PIANA_NPC + "pocztowiec"}));
}