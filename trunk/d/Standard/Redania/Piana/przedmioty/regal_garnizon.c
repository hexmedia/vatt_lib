/* Autor: Avard
   Opis : vortak
   Data : 6.08.2007 */

inherit "/std/container";

#include "/sys/stdproperties.h"
#include "/sys/macros.h"
#include "/sys/pl.h"
#include "/sys/materialy.h"
#include "/sys/object_types.h"
#include <mudtime.h>
#include "dir.h"

void
create_container()
{
    ustaw_nazwe("rega^l");
    dodaj_przym("prosty", "pro^sci");
    dodaj_przym("solidny", "solidni");
    ustaw_material(MATERIALY_DR_DAB);
    set_type(O_MEBLE);

    set_long("Zbity z kilku prostych, oheblowanych desek rega^l przybito "+
        "na sta^le do ^sciany. Szerokie p^o^lki sprawiaj^a wra^zenie "+
        "solidnych, bez obaw mo^zna na nich po^lo^zy^c nawet ci^e^zkie "+
        "przedmioty. "+
        "@@opis_sublokacji|Dostrzegasz na nim |na|.\n|\n|" + PL_BIE+ "@@");
         
    add_prop(CONT_I_WEIGHT, 10000);
    add_prop(CONT_I_VOLUME, 10000);
    add_prop(OBJ_I_VALUE, 456);
    
    add_subloc("na");
    add_subloc_prop("na", CONT_I_MAX_WEIGHT, 100000);
    add_subloc_prop("na", CONT_I_MAX_VOLUME, 150000);
    add_prop(CONT_I_DONT_SHOW_CONTENTS, 1);
    add_prop(CONT_I_CANT_WLOZ_DO, 1);

    add_prop(OBJ_M_NO_GET,"Rega^l jest przymocowany do ^sciany na sta^le.\n");

    //set_owners(({BIALY_MOST_NPC + "karczmarz_bialy_most"}));
}