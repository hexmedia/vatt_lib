/* Autor: Avard
   Opis : Arivia
   Data : 31.03.07 */
inherit "/std/container";

#include "dir.h"
#include "/sys/stdproperties.h"
#include "/sys/macros.h"
#include "/sys/pl.h"
#include "/sys/materialy.h"
#include "/sys/object_types.h"

void
create_container()
{
    ustaw_nazwe("stoliczek");
    dodaj_przym("rozchybotany", "rozchybotani");
    dodaj_przym("drewniany", "drewniani");
    ustaw_material(MATERIALY_DR_DAB);
    set_type(O_MEBLE);
    set_long("Ten stoliczek, kolebi^acy si^e na lewo i prawo za ka^zdym "+
        "dotkni^eciem, jest tak ma^ly, i^z ledwo mie^sci na sobie "+
        "pude^leczo z drobnymi monetami i kilka krawieckich przybor^ow. "+
        "Jego blat zupe^lnie pokry^l si^e grub^a warstw^a kurzu. "+
        "@@opis_sublokacji|Dostrzegasz na nim |na|.\n|\n|" + PL_BIE+ "@@");
         
    add_prop(CONT_I_WEIGHT, 3000);
    add_prop(CONT_I_VOLUME, 3000);
    add_prop(OBJ_I_VALUE, 10);
    
    add_subloc("na");
    add_subloc_prop("na", CONT_I_MAX_WEIGHT, 10000);
    add_subloc_prop("na", CONT_I_MAX_VOLUME, 10000);
    add_prop(CONT_I_DONT_SHOW_CONTENTS, 1);
    add_prop(CONT_I_CANT_WLOZ_DO, 1);
    set_owners(({PIANA_NPC+"krawiec"}));

}

