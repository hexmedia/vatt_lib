inherit "/std/board";

#include "dir.h"
#include <materialy.h>

create_board()
{
    dodaj_przym("spory","sporzy");
    set_long("Na tej ca^lkiem sporej tablicy, okutej w mocn^a, drewnian^a "+
        "ram^e, wisi mn^ostwo karteczek z ogloszeniami, mniej lub bardziej "+
        "interesuj^acymi, cho^c znale^x^c tu mo^zna tak^ze r^o^zne oferty, "+
        "gro^xby i obelgi, wa^zne og^loszenia, czy te^z zwyk^le reklamy. "+
        "Na niekt^orych notkach podopisywano z^lo^sliwe komentarze, zwykle "+
        "anonimowe, lub wykonano szydercze obrazki.\n");

    set_board_name(PIANA + "notki");
    set_silent(1);
    set_num_notes(100);
    ustaw_material(MATERIALY_DR_DAB, 90);
    ustaw_material(MATERIALY_MOSIADZ, 10);
    set_owners(({PIANA_NPC + "pocztowiec"}));
}
