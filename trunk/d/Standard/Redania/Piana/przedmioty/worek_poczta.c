/*w�r do pracy na poczcie. zaniesc go trza do piany. Vera */
#include "dir.h"
#include <macros.h>
#include <stdproperties.h>
#include <materialy.h>
#include <object_types.h>
#include <pl.h>

inherit "/std/object";

create_object()
{
    dodaj_przym("p^l^ocienny","p^l^ocienni");
    dodaj_przym("szary","szary");
    ustaw_nazwe("worek");

    set_long("P^l^ocienny przetarty w^or szczelnie zawi^azany kawa^lem mocnego "+
        "sznura ca^ly wypchany jest szeleszcz^acymi papierami.\n R^owna, "+
        "szablonowa czcionka m^owi o tym, ^ze adresatem owego wora jest "+
        "Urz^ad Pocztowy w Rinde, mie^sciny Wielkiego Kr^olestwa Redania.\n");

    add_prop(OBJ_I_WEIGHT, 7000+random(1000));
    add_prop(OBJ_I_VOLUME, 10000+random(1000));
    add_prop(OBJ_M_NO_SELL,"Ten worek nie mo^ze by^c sprzedany.\n");
    ustaw_material(MATERIALY_TK_LEN);
}
void
zniszcz()
{
  destruct();
}
int
query_worek_z_piany()
{
    return 1;
}
public string
query_auto_load()
{
    return 0;
}
