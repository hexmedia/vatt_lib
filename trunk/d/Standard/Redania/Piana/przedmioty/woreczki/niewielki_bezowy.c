/* Autor: Avard
   Opis : Samaia
   Data : 9.12.07 */

inherit "/std/sakiewka.c";

#include <pl.h>
#include <macros.h>
#include <filter_funs.h>
#include <stdproperties.h>
#include <composite.h>
#include <cmdparse.h>
#include <object_types.h>
#include <materialy.h>

void
create_sakiewka()
{
    ustaw_nazwe("woreczek");
    dodaj_przym("niewielki","niewielcy");
    dodaj_przym("be^zowy","be^zowi");
    set_long("Ma^ly woreczek przeznaczony g^l^ownie do przechowywania "+
        "zi^o^l, mo^ze mie^c tak^ze inne zastosowania, jak cho^cby "+
        "trzymanie w nim monet, kamieni, bi^zuterii i innych drobiazg^ow. "+
        "Uszyty z be^zowego, mi^ekkiego materia^lu odpowiednio t^lumi "+
        "brz^ecz^ac^a zawarto^s^c, lub zapobiega wysuszeniu ro^slin. "+
        "Wyposa^zony w sznureczek, ^latwo daje si^e zacisn^a^c i "+
        "przywi^aza^c do pasa. @@opis_sublokacji|Dostrzegasz w niej "+
        "|w|.\n|\n||"+ PL_BIE+ "@@");


    add_prop(CONT_I_MAX_VOLUME, 4000);
    add_prop(CONT_I_VOLUME, 760);

    add_prop(CONT_I_WEIGHT, 650);
    add_prop(CONT_I_MAX_WEIGHT, 3550);

    ustaw_material(MATERIALY_TK_LEN);
    
    add_prop(OBJ_I_VALUE, 350);

}
