/* Autor: Faeve
   Data : 19.12.07 */

inherit "/std/receptacle";

#include "/sys/stdproperties.h"
#include "/sys/macros.h"
#include "/sys/pl.h"
#include "/sys/materialy.h"
#include "/sys/object_types.h"
#include "dir.h"

void
create_receptacle()
{
    ustaw_nazwe("szafa");
    dodaj_przym("wielki", "wielcy");
    dodaj_przym("jesionowy", "jesionowi");
    ustaw_material(MATERIALY_DR_JESION);
    set_type(O_MEBLE);
    set_long("Zajmuj^aca praktycznie ca^l^a ^scian^e szafa, cho^c stara - "+
	 "trzyma si^e ca^lkiem solidnie. Co prawda miejscami lakier ju^z "+
	 "si^e wytar^l i nie do ko^nca spe^lnia sw^a rol^e - ale kt^o^z "+
	 "zwraca^lby na to uwag^e, gdy mebel prezentuje si^e wcale nienagannie? "+
	 "R^owno zawieszone drzwiczki otwieraj^a si^e bez najmniejszego nawet "+
	 "oporu, czasem tylko cicho poskrzypuj^ac. "+
	 "@@opis_zawartosci@@"\n");
	     
    add_prop(OBJ_I_VALUE, 10);
    add_prop(CONT_I_RIGID, 1);
    add_prop(CONT_I_MAX_VOLUME, 250000);
    add_prop(CONT_I_MAX_WEIGHT, 500000);
    add_prop(CONT_I_VOLUME, 10000);
    add_prop(CONT_I_WEIGHT, 20000);
    add_prop(CONT_I_NO_OPEN_DESC, 1);
        add_prop(OBJ_I_NO_GET, "Ha, a niby jak chcesz podnie^s^c co^s, co "+
	 "zajmuje prawie ca^l^a ^scian^e?");
    set_owners(({PIANA_NPC + "burmistrz"}));
}

string
opis_zawartosci()
{
  return (query_prop(CONT_I_CLOSED) ? " " : opis_sublokacji(" W ^srodku widzisz ",0,". ", " ", PL_BIE));
}
