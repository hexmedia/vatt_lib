inherit "/std/object";
inherit "/lib/kwiat";

#include "/sys/stdproperties.h"
#include "/sys/macros.h"
#include "/sys/pl.h"
#include "/sys/cmdparse.h"
#include "/sys/ss_types.h"
#include <object_types.h>


int zgniec(string str);

int globalny = -1; // bo tak

void
create_object()
{
    ustaw_nazwe("dalia");
    dodaj_przym("r^o^zowy", "r^o^zowi");
    dodaj_nazwy("kwiat");
    set_long("Szerokie, blador^o^zowe p^latki tego kwiatu uk^ladaj^a si^e "+
        "wianuszkiem wok^o^l jego z^locistego oka pr^ecik^ow, wabi^ac swym "+
        "zapachem pszczo^ly i trzmiele. Wi^ekoszo^s^c li^sci ro^sliny "+
        "zosta^lo oberwanych, by u^latwi^c tym samym tworzenie bukiet^ow, "+
        "czy te^z wsuwanie dalii we w^losy. Te kt^ore pozosta^ly, tu^z przy "+
        "kwiecie, a tak^ze trzy, czy cztery wzd^lu^z ^lodygi, rosn^a na "+
        "kszta^lt troch^e postrz^epionego serca. \n");
    add_prop(OBJ_I_WEIGHT, 50);
    add_prop(OBJ_I_VOLUME, 65);
    add_prop(OBJ_I_VALUE, 22);

    set_type(O_KWIATY);
    set_wplatalny(1);


    jak_pachnie="Zamykaj�c oczy unosisz powoli kwiat ku swej twarzy i " +
        "delektujesz si^e jego delikatnym niczym dotyk jedwabiu " +
        "zapachem, kt^ory zdaje si^e ogarnia^c wszystko wok^o^l ciebie.";
    jak_wacha_jak_sie_zachowuje="zbli^za do twarzy r^o^zow^a dali^e " +
          "i napawa si^e jego delikatnym zapachem."; 
}
init()
{
    ::init();
    init_kwiat();
    add_action(zgniec, "zgnie^c");
}

int
zgniec(string str)
{
    object lilia;
    if (parse_command(str,all_inventory(this_player()), "%o:"+PL_BIE, lilia)&&
        lilia == this_object())
    {
        write("Zamykasz w d^loni delikatne p^latki r^o^zowej dalii " +
        "i gwa^ltownym ruchem mia^zd^zysz kwiat w palcach " +
        "wy^ladowywuj�c sw� w�ciek^lo�^c. " +
        "Chwil^e po tym lekkim ruchem r^eki odrzucasz zniszczony " +
        "kwiat na bok.\n");
        saybb(QCIMIE(this_player(), PL_MIA)+" mru^zy oczy z w�ciek^lo�ci� " +
        "mia^zd^z�c w d^loni delikatne p^latki r^o^zowej dalii. " +
        "Chwil^e potem zniszczony kwiat l�duje na ziemi, a jego " +
        "pomi^ete p^latki wiruj� przez chwil^e w powietrzu" +
        (environment(this_player())->query_prop(ROOM_I_INSIDE) ?
        " i opadaj� na ziemi^e.\n" : " po czym odlatuj� w dal niesione " +
        "podmuchem wiatru.\n"));
        remove_object();
        return 1;
    }
    notify_fail("Zgnie^c co ?\n");
    return 0;
}
