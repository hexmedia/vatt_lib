inherit "/std/object";
inherit "/lib/kwiat";

#include "/sys/stdproperties.h"
#include "/sys/macros.h"
#include "/sys/pl.h"
#include "/sys/cmdparse.h"
#include "/sys/ss_types.h"
#include <object_types.h>


int zgniec(string str);

int globalny = -1; // bo tak

void
create_object()
{
    ustaw_nazwe("tulipan");
    dodaj_przym("^z^o^lty", "^z^o^lci");
    dodaj_nazwy("kwiat");
    set_long("Ten urodziwy kwiat, o ciep^lej, z^locistej barwie, sk^lada "+
        "^lagodnie zaokr^aglone p^latki, tworz^ac ci^e^zki, zwarty kielich, "+
        "skrywaj^acy w sobie okaza^ly s^lupek. Jak^ze charakterystyczne dla "+
        "tulipan^ow  poci^ag^le, ostro zako^nczone li^scie wyrastaj^a z "+
        "ciemnozielonej ^lodygi naprzemiennie, wyci^agaj^ac si^e ku "+
        "g^orze. \n");
    add_prop(OBJ_I_WEIGHT, 50);
    add_prop(OBJ_I_VOLUME, 65);
    add_prop(OBJ_I_VALUE, 19);

    set_type(O_KWIATY);
    set_wplatalny(1);


    jak_pachnie="Zamykaj�c oczy unosisz powoli kwiat ku swej twarzy i " +
        "delektujesz si^e jego delikatnym niczym dotyk jedwabiu " +
        "zapachem, kt^ory zdaje si^e ogarnia^c wszystko wok^o^l ciebie.";
    jak_wacha_jak_sie_zachowuje="zbli^za do twarzy z^o^lty tulipan " +
          "i napawa si^e jego delikatnym zapachem."; 
}
init()
{
    ::init();
    init_kwiat();
    add_action(zgniec, "zgnie^c");
}

int
zgniec(string str)
{
    object lilia;
    if (parse_command(str,all_inventory(this_player()), "%o:"+PL_BIE, lilia)&&
        lilia == this_object())
    {
        write("Zamykasz w d^loni delikatne p^latki ^z^o^ltego tulipana " +
        "i gwa^ltownym ruchem mia^zd^zysz kwiat w palcach " +
        "wy^ladowywuj�c sw� w�ciek^lo�^c. " +
        "Chwil^e po tym lekkim ruchem r^eki odrzucasz zniszczony " +
        "kwiat na bok.\n");
        saybb(QCIMIE(this_player(), PL_MIA)+" mru^zy oczy z w�ciek^lo�ci� " +
        "mia^zd^z�c w d^loni delikatne p^latki ^z^o^ltego tulipana. " +
        "Chwil^e potem zniszczony kwiat l�duje na ziemi, a jego " +
        "pomi^ete p^latki wiruj� przez chwil^e w powietrzu" +
        (environment(this_player())->query_prop(ROOM_I_INSIDE) ?
        " i opadaj� na ziemi^e.\n" : " po czym odlatuj� w dal niesione " +
        "podmuchem wiatru.\n"));
        remove_object();
        return 1;
    }
    notify_fail("Zgnie^c co ?\n");
    return 0;
}
