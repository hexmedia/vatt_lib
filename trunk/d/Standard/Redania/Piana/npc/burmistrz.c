
/* Autor: Avard
   Opis : Samaia
   Data : 23.08.07 */

#pragma unique

#include <std.h>
#include <ss_types.h>
#include <macros.h>
#include <stdproperties.h>
#include <filter_funs.h>
#include <pl.h>
#include <money.h>
#include <object_types.h>
#include "dir.h"

inherit PIANA_STD_NPC;
inherit "/lib/straz/odkupianie_win.c";

//int czy_walka();
//int walka = 0;

void
create_piana_humanoid()
{
    ustaw_odmiane_rasy("mezczyzna");
    dodaj_nazwy("burmistrz");
    set_gender(G_MALE);
    dodaj_przym("puszysty","puszy^sci");
    dodaj_przym("jasnow^losy","jasnow^losi");
    ustaw_imie(({"ardgal","ardgala","ardgalowi","ardgala","ardgalem",
        "ardgalu"}));
    set_origin("z Piany");

    set_long("@@dlugasny@@");

    set_stats (({ 40, 30, 40, 55, 0 }));
    set_skill(SS_UNARM_COMBAT, 50);
    add_prop(CONT_I_WEIGHT, 90000);
    add_prop(CONT_I_HEIGHT, 160);

    set_act_time(60);
    add_act("zaspiewaj wesolo: ^Ze niewinna, ^ze dziewica, prawda taka - "+
        "rozpustnica! ");
    add_act("zanuc niewyra^xnie");
    add_act("zaspiewaj wesolo: Tedy wierzysz? Masz na pie^nku! Wnet ci^e "+
        "wiedzie do o^zenku! ");
    add_act("zaspiewaj Koniec twojej to swawoli, sam^ze^s skin^a^l do "+
        "niewoli!");
    add_act("emote podryguje lekko, potrz^asaj^ac brzuszyskiem.");
    add_act("emote odgarnia lekko be^zow^a zas^lon^e, wygl^adaj^ac przez "+
        "okno na zewn^atrz. ");
    add_act("@@elf@@");

    set_cact_time(10);
    add_cact("spanikuj");
    add_cact("krzyknij Ratujcie mnie!");
    add_cact("krzyknij Pomocy!");
    add_cact("':roztrz^esionym g^losem: Co ja ^zem ci zrobi^l?!");
    add_cact("':roztrz^esionym g^losem: Daruj! To^c dziatki i ^zon^e mam!");
    add_cact("emote zas^lania si^e r^ekami.");
    add_cact("':^lami^acym si^e g^losem: B^lagam! B^lagam, lito^sci!");

    add_ask(({"pian^e","Pian^e","miasto","mie�cin�"}), VBFC_ME("pyt_o_piane"));
    add_ask(({"Rinde","Tretogor","Novigrad","Oxenfurt","Bia^ly Most",
        "Wyzima","Murivel","Ellander","Carbon","Vangerberg","Cintr^e",
        "Nilfgaard","Hagge","Maribor","Gors Valen","Blaviken","Ho^lopole"}),
        VBFC_ME("pyt_o_miasta"));
    add_ask(({"karczm^e"}), VBFC_ME("pyt_o_karczme"));
    add_ask(({"garnizon"}), VBFC_ME("pyt_o_garnizon"));
    add_ask(({"krawca"}), VBFC_ME("pyt_o_krawca"));
    add_ask(({"rynek"}), VBFC_ME("pyt_o_rynek"));
    add_ask(({"most"}), VBFC_ME("pyt_o_most"));
    add_ask(({"pogod^e","deszcz","s^lo^nce","chmury","^snieg","grad","burz^e",
        "^snie^zyc^e","ksi^e^zyc"}), VBFC_ME("pyt_o_pogode"));
    add_ask(({"zdrowie"}),VBFC_ME("pyt_o_zdrowie"));
    add_ask(({"^spiew","^spiewanie","muzyk^e"}),VBFC_ME("pyt_o_muzyke"));
    add_ask(({"^zon^e","dzieci","dziecko","rodzin^e"}),VBFC_ME("pyt_o_rodzine"));
    add_ask(({"elfy","elfki","elfa","elfk^e","nieludzi"}),VBFC_ME("pyt_o_elfy"));
    add_ask(({"krasnoludy","gnomy","nizio^lki","krasnoluda","gnoma",
        "nizio^lka"}),VBFC_ME("pyt_o_facetow"));
    add_ask(({"krasnoludki","gnomki","krasnoludk^e","gnomk^e","nizio^lk^e"}),
        VBFC_ME("pyt_o_kobiety"));
    add_ask(({"ludzi","kobiety","m^e^zczyzn","kobiet^e","m^e^zczyzn^e"}),
        VBFC_ME("pyt_o_ludzi"));
    add_ask(({"p^o^lelfy","p^o^lelfki","p^o^lelfa","p^o^lelfki"}),
        VBFC_ME("pyt_o_polelfy"));

    add_ask(({"pochodzenie","pochodzenia","zostanie mieszka�cem",
                "zostanie mieszka�cem piany","bycie mieszka�cem",
              "zapisanie si� do mieszka�c�w"}),
             VBFC_ME("pyt_pochodzenie"));

    set_default_answer(VBFC_ME("default_answer"));

	add_armour(PIANA_UBRANIA + "buty/czarne_zamszowe_XLc.c");
	add_armour(PIANA_UBRANIA + "spodnie/jasne_lniane_Lc.c");
	add_armour(PIANA_UBRANIA + "koszule/cienka_luzna_Mc.c");

	//Trzebaby da� mu jaki� n� chyba:P
	//add_weapon(PIANA_BRONIE +);

    //A teraz konfigurujemu mo�liwo�� odkupiania win:
    config_odkupiciel();
}

void
powiedz_gdy_jest(object player, string tekst)
{
    if (environment(this_object()) == environment(player))
        this_object()->command(tekst);
}

string
pyt_o_pochodzenie()
{
    if(!query_attack())
    {
        //elfow nie przyjmujemy ;p
        if(TP->query_race_name() ~= "elf")
        {
             set_alarm(0.5, 0.0 , "powiedz_gdy_jest", this_player(),
            "powiedz do "+ this_player()->query_name(PL_DOP) +
            " Nie, nie. Przykro mi bardzo, ale nie chc� mie� "+
            "�adnych k�opot�w. Nie mo�esz tu zamieszka�.");
            return "";
        }

        //warunek jeszcze jest taki, ze quest u krawca musi byc zrobiony
        //to test na poznanie miesciny
        if (member_array(lower_case(TP->query_name(0)),
                explode(read_file(ZROBILI), "#"))== -1)
        {
            set_alarm(0.5, 0.0 , "powiedz_gdy_jest", this_player(),
            "powiedz do "+ this_player()->query_name(PL_DOP) +
            " Hmmm...Zapoznaj si� nieco z nasz� mie�cin� i wr�� "+
            "do mnie, to pomy�limy.");
            return "";
        }

         set_alarm(0.5, 0.0 , "powiedz_gdy_jest", this_player(),
        "powiedz do "+ this_player()->query_name(PL_DOP) +
        " O, �wietnie! Chcesz zosta� mieszka�cem Piany? "+
        "Je�li tak musisz mi jeszcze wp�aci� dwie "+
        "z�ote reda�skie korony. Pieni�dze id� dla tych "+
        " najbardziej potrzebuj�cych...");
        set_alarm(0.5, 0.0 , "powiedz_gdy_jest", this_player(),
        "usmiechnij sie serdecznie");
    }
    else
    {
        set_alarm(0.5,0.0,"powiedz_gdy_jest", TP, "'Zostaw mnie!");
    }
    return "";
}
string
pyt_o_piane()
{
    if(!query_attack())
    {
        switch(random(2))
        {
            case 0: set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
                "'Ach, m^og^lbym m^owi^c w niesko^nczono^s^c. Ale po co? W "+
                "sumie nie lepiej porozmawia^c o czym^s innym?"+
                TP->query_origin()!="z Piany"?
                " A mo�e chcesz zosta� mieszka�cem Piany?":"");
            set_alarm(1.5,0.0,"powiedz_gdy_jest", TP,"westchnij cicho");break;
            case 1: set_alarm(0.5,0.0,"powiedz_gdy_jest", TP,
                "'Piana, Piana, Piana... w zasadzie ma^lo kto o nas "+
                "s^lysza^l, ale kiedy^s co^s przyci^agnie tu t^lumy."+
                TP->query_origin()!="z Piany"?
                " A mo�e chcesz zosta� mieszka�cem Piany?":"");
            set_alarm(1.5,0.0,"powiedz_gdy_jest", TP,"zatrzyj rece");break;
        }
    }
    else
    {
        set_alarm(0.5,0.0,"powiedz_gdy_jest", TP, "'Zostaw mnie!");
    }
    return "";
}
string
pyt_o_miasta()
{
    if(!query_attack())
    {
        set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
        "':^smiej^ac si^e serdecznie: Ja si^e st^ad nie ruszam!");
    }
    else
    {
        set_alarm(0.5,0.0,"powiedz_gdy_jest", TP, "'Zostaw mnie!");
    }
    return "";
}
string
pyt_o_pogode()
{
    if(!query_attack())
    {
        set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
        "':wygl^adaj^ac przez okno: Tak, tak...");
    }
    else
    {
        set_alarm(0.5,0.0,"powiedz_gdy_jest", TP, "'Zostaw mnie!");
    }
    return "";
}
string
pyt_o_karczme()
{
    if(!query_attack())
    {
        set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
        "wskaz e");
    }
    else
    {
        set_alarm(0.5,0.0,"powiedz_gdy_jest", TP, "'Zostaw mnie!");
    }
    return "";
}
string
pyt_o_garnizon()
{
    if(!query_attack())
    {
        set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
        "wskaz se");
    }
    else
    {
        set_alarm(0.5,0.0,"powiedz_gdy_jest", TP, "'Zostaw mnie!");
    }
    return "";
}
string
pyt_o_rynek()
{
    if(!query_attack())
    {
        set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
        "wskaz se");
    }
    else
    {
        set_alarm(0.5,0.0,"powiedz_gdy_jest", TP, "'Zostaw mnie!");
    }
    return "";
}
string
pyt_o_krawca()
{
    if(!query_attack())
    {
        set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
        "wskaz n");
    }
    else
    {
        set_alarm(0.5,0.0,"powiedz_gdy_jest", TP, "'Zostaw mnie!");
    }
    return "";
}
string
pyt_o_most()
{
    if(!query_attack())
    {
        set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
        "wskaz w");
    }
    else
    {
        set_alarm(0.5,0.0,"powiedz_gdy_jest", TP, "'Zostaw mnie!");
    }
    return "";
}
string
pyt_o_zdrowie()
{
    if(!query_attack())
    {
        set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
        "':u^smiechaj^ac si^e weso^lo: Dzi^ekuj^e, nie narzekam.");
    }
    else
    {
        set_alarm(0.5,0.0,"powiedz_gdy_jest", TP, "'Zostaw mnie!");
    }
    return "";
}
string
pyt_o_muzyke()
{
    if(!query_attack())
    {
        switch(random(2))
        {
            case 0: set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
                "':spogl^adaj^ac gdzie^s z rozmarzeniem: Oj, chcia^lem by^c "+
                "kiedys bardem...");
            set_alarm(1.5, 0.0, "powiedz_gdy_jest", this_player(),
                "':wzdychaj^ac ci^e^zko: Ale zosta^lem burmistrzem.");break;
            case 1: set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
                "':z entuzjamem: To co^s wspanialego! ");break;
        }
    }
    else
    {
        set_alarm(0.5,0.0,"powiedz_gdy_jest", TP, "'Zostaw mnie!");
    }
    return "";
}
string
pyt_o_rodzine()
{
    if(!query_attack())
    {
        set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
        "':z namys^lem: Ona jest mi najwa^zniejsza. ");
        set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
        "pokiwaj z namyslem");
    }
    else
    {
        set_alarm(0.5,0.0,"powiedz_gdy_jest", TP, "'Zostaw mnie!");
    }
    return "";
}
string
pyt_o_elfy()
{
    if(!query_attack())
    {
        set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
        "wzdrygnij si^e");
        set_alarm(1.5, 0.0, "powiedz_gdy_jest", this_player(),
        "':rozgl^adaj^ac si^e szybko: Gdzie? Gdzie?");
    }
    else
    {
        set_alarm(0.5,0.0,"powiedz_gdy_jest", TP, "'Zostaw mnie!");
    }
    return "";
}
string
pyt_o_facetow()
{
    if(!query_attack())
    {
        set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
        "':u^smiechaj^ac si^e szeroko: Kto jak kto, r^owne ch^lopy.");
    }
    else
    {
        set_alarm(0.5,0.0,"powiedz_gdy_jest", TP, "'Zostaw mnie!");
    }
    return "";
}
string
pyt_o_kobiety()
{
    if(!query_attack())
    {
        set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
        "':u^smiechaj^ac si^e krzywo: To na szcz^e^scie niecz^esty widok.");
    }
    else
    {
        set_alarm(0.5,0.0,"powiedz_gdy_jest", TP, "'Zostaw mnie!");
    }
    return "";
}
string
pyt_o_polelfy()
{
    if(!query_attack())
    {
        set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
        "':cicho: Elfy wszystkiemu winne.");
        set_alarm(1.5, 0.0, "powiedz_gdy_jest", this_player(),
        "wzrusz ramionami");
    }
    else
    {
        set_alarm(0.5,0.0,"powiedz_gdy_jest", TP, "'Zostaw mnie!");
    }
    return "";
}
string
pyt_o_ludzi()
{
    if(!query_attack())
    {
        set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
        "spojrzyj");
        set_alarm(1.5, 0.0, "powiedz_gdy_jest", this_player(),
        "pokiwaj powoli");
    }
    else
    {
        set_alarm(0.5,0.0,"powiedz_gdy_jest", TP, "'Zostaw mnie!");
    }
    return "";
}
string
default_answer()
{
    if(!query_attack())
    {
        switch(random(4))
        {
            case 0: set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
                "pokiwaj niepewnie");break;
            case 1: set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
                "emote spogl^ada gdzie^s z zainteresowaniem, udaj^ac, ^ze "+
                "nic nie s^lysza^l.");break;
            case 2: set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
                "'Tak, tak!");break;
            case 3: set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
                "usmiechnij sie niezrecznie");break;
        }
    }
    else
    {
        set_alarm(0.5,0.0,"powiedz_gdy_jest", TP, "'Zostaw mnie!");
    }
    return "";
}

void
add_introduced(string imie_mia, string imie_bie)
{
    set_alarm(2.0, 0.0, "return_introduce", find_player(imie_mia));
}
void
return_introduce(object ob)
{
    command("przedstaw sie");
}

void
emote_hook(string emote, object wykonujacy)
{
    switch (emote)
    {
        case "kopnij": set_alarm(0.5, 0.0, "kopie", wykonujacy);
                    break;
        case "spoliczkuj": set_alarm(0.5, 0.0, "policzkuje", wykonujacy);
                    break;
        case "opluj" : set_alarm(0.5, 0.0, "nienajlepszy", wykonujacy);
                    break;
        case "szturchnij" : set_alarm(0.5, 0.0, "nienajlepszy", wykonujacy);
                    break;
        case "poca^luj": set_alarm(0.5,0.0, "caluje",wykonujacy);
                    break;
        case "prychnij" : set_alarm(0.5, 0.0, "nienajlepszy", wykonujacy);
                    break;
        case "przytul": set_alarm(0.5,0.0, "malolepszy",wykonujacy);
                    break;
        case "poglaszcz": set_alarm(0.5, 0.0, "malolepszy", wykonujacy);
                    break;
        case "poklep": set_alarm(0.5, 0.0, "malolepszy", wykonujacy);
                    break;
        case "usmiech": set_alarm(0.5, 0.0, "dobre", wykonujacy);
                    break;
        case "mrugnij": set_alarm(0.5, 0.0, "dobre", wykonujacy);
                    break;
        case "oczko": set_alarm(0.5, 0.0, "dobre", wykonujacy);
                    break;
    }
}
void
nienajlepszy(object wykonujacy)
{
    switch(random(3))
    {
        case 0: command("skrzyw si^e lekko");break;
        case 1: command("'No, co jest? ^Ladnie to tak?");break;
        case 2: command("'Prosz^e, przesta^n.");break;
    }
}
void
kopie(object wykonujacy)
{
    switch(random(3))
    {
        case 0: command("podskocz .");break;
        case 1: command("'Za co to?");break;
        case 2: command("'Com ci zrobi^l? ");break;
        case 3: command("'Przesta^n natychmiast! ");break;
    }
}
void
policzkuje(object wykonujacy)
{
    switch(random(3))
    {
        case 0: command("':ze zdziwieniem: Ej^ze! Co jest?");break;
        case 1: command("':^lapi^ac si^e za policzek: A^la! ");break;
        case 2: command("spojrzyj ze zdumieniem na "+OB_NAME(wykonujacy));
        break;
    }
}
void
dobre(object wykonujacy)
{
    switch(random(5))
    {
        case 0: command("usmiechnij sie milo");break;
        case 1: command("usmiechnij sie uprzejmie do "+OB_NAME(wykonujacy));
        break;
        case 2: command("emote ^smieje si^e rubasznie, zacieraj^ac r^ece.");
        break;
        case 3: command("mrugnij wesolo");break;
        case 4: command("poklep "+OB_NAME(wykonujacy)+" przyjacielsko w "+
            "ramie");break;
    }
}
void
malolepszy(object wykonujacy)
{
    if(wykonujacy->query_gender() == 1)
    {
        switch(random(4))
        {
            case 0: command("'Nie spoufalaj si^e, prosz^e...");break;
            case 1: command("'Nie pozwalaj sobie.");break;
            case 2: command("emote u^smiecha si^e nie^smia^lo i oddala o "+
               "krok.");break;
            case 3: command("':kiwaj^ac pulchnym palcem: No, no, no!");break;
        }
    }
    else
    {
        switch(random(4))
        {
            case 0: command("'Nie spoufalaj si^e, prosz^e...");break;
            case 1: command("'Nie pozwalaj sobie.");break;
            case 2: command("'Znajd^x se bab^e, a nie mnie tu b^edziesz "+
                "maca^l!");break;
            case 3: command("'^Lapy precz.");break;
        }
    }
}
void
caluje(object wykonujacy)
{
    if(wykonujacy->query_gender() == 1)
    {
        switch(random(5))
        {
            case 0: command("'Ach, gdyby nie to, ^zem ^zonaty...");break;
            case 1: command("usmiechnij sie marzycielsko");break;
            case 2: command("'No, no, ale bez przesady. ");break;
            case 3: command("usmiechnij sie niesmialo");break;
            case 4: command("emote oblewa si^e rumie^ncem.");break;
        }
    }
    else
    {
        switch(random(4))
        {
            case 0: command("emote wyciera usta i krzywi si^e z "+
                "obrzydzeniem.");break;
            case 1: command("'No co za czasy, co by mnie ch^lop ca^lowa^l.");
            break;
            case 2: command("pokrec glowa z niedowierzaniem");break;
            case 3: command("'A won mi st^ad!");break;
        }
    }
}
/*
void
attacked_by(object wrog)
{
    if(walka==0)
    {
        set_alarm(10.0, 0.0, "czy_walka", 1);
        walka = 1;
        return ::attacked_by(wrog);
    }
    else
    {
        walka = 1;
        return ::attacked_by(wrog);
    }
}

int
czy_walka()
{
    if(!query_attack())
    {
        command("zadrzyj z przera^zenia.");
        walka = 0;
        return 1;
    }
    else
    {
       set_alarm(5.0, 0.0, "czy_walka", 1);
    }

}*/

string
elf()
{
    object *livingi = FILTER_LIVE(all_inventory(environment(this_object())));
    livingi = filter(livingi, is_humanoid);
    livingi -= ({ this_object() });

    if (!sizeof(livingi)) {
        return "";
    }

    object adresat = livingi[random(sizeof(livingi))];
    if(adresat->query_race == elf)
    {
        switch(random(2))
        {
            case 0: tell_roombb(environment(TO), QCIMIE(TO, PL_MIA) +
                " spogl^ada ukradkiem na " + QIMIE(adresat, PL_BIE) + ".\n",
                ({ this_object(),adresat}));
            adresat->catch_msg(QCIMIE(TO, PL_MIA)+ " spogl^ada na ciebie "+
                "ukradkiem.\n");break;
            case 1: tell_roombb(environment(TO), QCIMIE(TO, PL_MIA) +
                " wodzi wzrokiem po d^lugich uszach "+QIMIE(adresat, PL_DOP)+
                ", po czym szybko udaje, ^ze na ni"+
                adresat->koncowka("ego","^a")+" nie patrzy^l.\n",
                ({ this_object(),adresat}));
            adresat->catch_msg(QCIMIE(TO, PL_MIA)+ " wodzi wzrokiem po "+
                "twoich d^lugich uszach, po czym szybko udaje, ^ze na "+
                "ciebie nie patrzy^l.\n");break;
        }
    }
    else
    {
        command("emote drepcze sobie w miejscu, nudz^ac si^e wyra^xnie.");
    }
}

string
dlugasny()
{
    string str;
    str = "Wysoki, puszysty m^e^zczyzna o rozanielonym wr^ecz "+
        "wyrazie twarzy z pewno^sci^a nie wygl^ada na kogo^s wa^znego, "+
        "a ju^z w^laszcza na burmistrza miasta. Opas^le brzuszysko i "+
	 "jasne w^losy ufryzowane w k^edziorki upodabiaj^a go raczej do "+
	 "piekarza lub cukiernika. ";
    if(!query_attack())
    {
        str += "M^e^zczyzna co jaki^s czas, daj^ac upust swej "+
        "weso^lo^sci, nuci pod nosem radosn^a piosenk^e lub wygl^ada "+
        "przez okno rozmarzonym wzrokiem. ";
    }
    str += "Jego ciemne, b^lyszcz^ace oczy "+
        "o niezbyt bystrym spojrzeniu zdradzaj^a, ^ze raczej nie "+
	 "nale^zy on do tych, kt^orzy mogliby ^swiadomie wyrz^adza^c "+
        "krzywd^e. \n ";
    return str;
}

int
wplac(string str)
{
    if(query_verb() ~= "wp�a�")
        notify_fail("Za co chcesz wp�aci�?\n");
    else
        notify_fail("Za co chcesz zap�aci�?\n");

    if(!str)
        return 0;

    if(str != "za pochodzenie")
        return 0;

    if(TP->query_origin() ~= "z Piany")
    {
        set_alarm(0.5, 0.0 , "powiedz_gdy_jest", this_player(),
            "powiedz do "+ this_player()->query_name(PL_DOP) +
            " Ej�e, przecie my si� znamy s�siedzie. Jak�e to tak?");
        return 1;
    }
    if(TP->query_origin())
    {
        set_alarm(0.5, 0.0 , "powiedz_gdy_jest", this_player(),
            "powiedz do "+ this_player()->query_name(PL_DOP) +
            " Ho ho, przykro mi. Musia�"+TP->koncowka("by�","aby�","oby�")+
            " by� pariasem, bym m�g� ci� przyj��. Teraz to ju� pozamiatane.");
        return 1;
    }

    //elfow nie przyjmujemy ;p
        if(TP->query_race_name() ~= "elf")
        {
             set_alarm(0.5, 0.0 , "powiedz_gdy_jest", this_player(),
            "powiedz do "+ this_player()->query_name(PL_DOP) +
            " Nie, nie. Przykro mi bardzo, ale nie chc� mie� "+
            "�adnych k�opot�w. Nie mo�esz tu zamieszka�.");
            return 1;
        }

        //warunek jeszcze jest taki, ze quest u krawca musi byc zrobiony
        //to test na poznanie miesciny
        if (member_array(lower_case(TP->query_name(0)),
                explode(read_file(ZROBILI), "#"))== -1)
        {
            set_alarm(0.5, 0.0 , "powiedz_gdy_jest", this_player(),
            "powiedz do "+ this_player()->query_name(PL_DOP) +
            " Hmmm...Zapoznaj si� nieco z nasz� mie�cin� i wr�� "+
            "do mnie, to pomy�limy.");
            return 1;
        }

    if(!MONEY_ADD(TP,-480))
    {
        command("'Och, chyba ci� na to nie sta�.");
        return 1;
    }

    string wiad = "";
    string przedst = TP->query_name();

    if(TP->query_surname())
        przedst+= " "+TP->query_surname();

    wiad += "\n\tNa mocy praw mi^lo^sciwie nam panuj^acego " +
        "Ja^snie Pana Vizimira I, z woli bo^zej i ludzkiej " +
        "kr^ola i jedynego prawowitego w^ladcy Redanii, a z " +
        "decyzji w^ladz miasta Piana niniejszym og^lasza si^e, " +
        "i^z z dniem dzisiejszym ";
    wiad += przedst;
    wiad+=" zostaje mieszka^ncem " +
        "miasta Piana, wobec czego otrzymuje wszelkie prawa "+
        "tego^z. Jednocze^snie zobowi^azuje si^e " +
        TP->koncowka("go", "j^a") + " do sumiennego " +
        "przestrzegania kodeksu miasta pod kar^a " +
        "cofni^ecia praw.\n\n\n";

    wiad += sprintf("%70s", BURMISTRZ+",\n");
    wiad += sprintf("%70s", "burmistrz Piany.\n");

    object doku = clone_object(PIANA_PRZEDMIOTY+"dokument_pochodzenia.c");
    doku->set_message(wiad);
    doku->init_doku_arg(doku->query_doku_auto_load());
    doku->move(this_object());
    set_alarm(1.0, 0.0, "command", "daj dokument " + OB_NAME(TP));
    set_alarm(1.5,0.0,"command","odloz dokument");
    TP->set_origin(POCHODZENIE);
    command("usmiechnij sie szeroko");
    command("'Doskonale. Prosz�, oto Wasz dokument.");

    return 1;
}


void
init_living()
{
    ::init_living();
    add_action(wplac,"wp�a�");
    add_action(wplac,"zap�a�");
    //add_action(odkupiciel_help, "pomoc");
    init_odkupiciel();
}
