/* Autor: Avard
   Opis : Samaia
   Data : 25.07.07 */

inherit "/std/zwierze.c";
inherit "/std/act/action";
inherit "/std/act/trigaction.c";

#include <macros.h>
#include <stdproperties.h>
#include <money.h>
#include <pl.h>
#include <formulas.h>
#include <std.h>
#include <ss_types.h>
#include <wa_types.h>
#include <object_types.h>
#include <composite.h>
#include <filter_funs.h>
#include "dir.h"

void
create_zwierze()
{
    add_leftover("/std/skora", "sk^ora", 1, 0, 1, 1, 1 + random(5), O_SKORY);
    add_leftover("/std/leftover", "pazur", random(3) + 2, 0, 1, 0, 0, O_KOSCI);
    add_leftover("/std/leftover", "z^ab", random(3) + 2, 0, 1, 0, 0, O_KOSCI);

    ustaw_odmiane_rasy("kot");
    set_gender(G_MALE);

    set_long("To zwierz^e o mi^ekkim, burym, zadbanym futerku w pr^egi i ciep^lym, z^lotym spojrzeniu "+
        "ani na jot^e nie przypomina kota z opowie^sci zabobonnych ch^lop^ow, rzekomo zwi^azanego z "+
        "magi^a i strasznymi morderczymi zap^edami. Przeciwnie, kociak mrucz^ac cichutko kr^eci si^e "+
        "po okolicy, ocieraj^ac o nogi przechodni^ow i domagaj^ac pieszczot. \n");

    dodaj_przym("puszysty", "puszy�ci");
    dodaj_przym("pr^egowany","pr^egowani");

    set_act_time(30);
    add_act("emote mruczy cicho.");
    add_act("emote rozgl�da si^e leniwie mru^z�c lekko oczy.");
    add_act("emote ^lasi si^e o twoj� nog^e.");
    add_act("emote miauczy cicho.");
    add_act("emote li^ze swoj� ^lapk^e.");
    add_act("emote z zainteresowaniem �ledzi jaki� szybko poruszaj�cy " +
        "si^e punkt.");

    set_stats ( ({ 1, 10, 10, 25, 58 }) );

    set_skill(SS_UNARM_COMBAT, 50 + random(5));

    set_attack_unarmed(0, 5, 5, W_SLASH, 100, "pazurki");

    set_hitloc_unarmed(0, ({ 0, 0, 5 }), 20, "g^lowa");
    set_hitloc_unarmed(1, ({ 0, 0, 5 }), 80, "brzuch");

    add_prop(LIVE_I_NEVERKNOWN, 1);
    add_prop(CONT_I_WEIGHT, 1000);
    add_prop(CONT_I_HEIGHT, 45);
    add_prop(NPC_I_NO_RUN_AWAY, 1);

}
void
emote_hook(string emote, object wykonujacy, string przyslowek)
{
    switch (emote)
    {
        case "kopnij":
        case "spoliczkuj":
        case "opluj":
        case "nadepnij":
            set_alarm(1.0, 0.0, "zly", wykonujacy);
            break;

        case "szturchnij":
            set_alarm(1.0, 0.0, "taki_sobie", wykonujacy);
            break;

        case "poca^luj":
        case "przytul":
        case "pog^laszcz":
            set_alarm(1.0, 0.0, "dobry", wykonujacy);
            break;

        case "usmiechnij":
        case "oczko":
        case "mrugnij":
            set_alarm(1.0,0.0, "ke", wykonujacy);
    }
}

void
zly(object kto)
{
    TO->run_away();
}

void
taki_sobie(object kto)
{
}

void
dobry(object kto)
{
    switch(random(3))
    {
        case 0: command("zamrucz z zadowoleniem");
        case 1: command("emote pr^e^zy si^e rozkosznie");
        case 2: command("emote k^ladzie si^e na plecach, oczekuj^ac "+
            "dalszych pieszczot.");
    }
}
void
ke(object kto)
{
    command("spojrzyj z zainteresowaniem na "+OB_NAME(kto));
}
void
attacked_by(object wrog)
{
    ::attacked_by(wrog);
}
