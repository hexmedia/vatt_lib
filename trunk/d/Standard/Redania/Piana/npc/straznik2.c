/* Autor: Sedzik i Avard
   Opis : Vayneela
   Data : 29.08.07 */

inherit "/std/humanoid.c";
inherit "/lib/straz/straznik.c";

#include "dir.h"
#include <std.h>
#include <ss_types.h>
#include <macros.h>
#include <stdproperties.h>
#include <pl.h>
#include <money.h>
#define MIASTO "z Piany"

#define BRON PIANA_BRONIE + "miecze/obosieczny_krotki.c"
#define ZBRO PIANA_ZBROJE + "przeszywanice/pikowana_brazowa_Mc.c"
#define BUTY PIANA_UBRANIA + "buty/czarne_zolnierskie_Mc.c"
#define SPOD PIANA_UBRANIA + "spodnie/grube_lniane_Mc.c"

object admin=TO->query_admin();

void
create_humanoid()
{
    ustaw_odmiane_rasy("m^e^zczyzna");
    dodaj_nazwy("stra積ik");

    random_przym(({"do鈍iadczony", "do鈍iadczeni", "czujny", "czujni", "spokojny", "spokojni"}),
        ({"zielonooki", "zielonoocy", "niebieskooki", "niebieskoocy", "br您owooki",
        "br您owocy", "czarnooki", "czarnoocy"}), ({"dobrze zbudowany", "dobrze zbudowani",
        "wysoki", "wysocy"}), ({"czarnow這sy", "czarnow這si", "blondw這sy", "blondw這si"}), 2);

    set_gender(G_MALE);
    set_spolecznosc("Piana");

    set_long("Tej, tak charakterystycznej twarzy, nie da si^e d^lugo zapomnie^c, "
        +"nawet je^sli nale^zy do najbardziej pospolitego ^zo^lnierza jeszcze "
        +"bardziej pospolitej stra^zy miejskiej. Szeroka blizna przecina policzek "
        +"m^e^zczyznym wykrzywiaj^ac usta tak, ^ze zdaj^a si^e ci^agle u^lo^zone w "
        +"szyderczy u^smiech. Nie tylko policzek znacz^a liczne szramy, a ca^lo^s^c "
        +"pozwala s^adzi^c, ^ze to zaprawiony w bojach wojownik. By^c mo^ze w gwardii "
        +"pokutuje za swe poprzednie przewinienia, a mo^ze poprostu szuka nowych "
        +"przyg^od. \n");

    set_stats(({130, 150,150, 150, 150}));

    set_attack_chance(100);
    set_aggressive(&check_living());

    add_prop(CONT_I_WEIGHT, 70000);
    add_prop(CONT_I_HEIGHT, 185);

    set_act_time(60);
    add_act("emote obserwuje uwaznie ruch na ulicach. ");
    add_act("emote sledzi wzrokiem przechodzaca dziewke. ");
    add_act("emote sennie m^owi cos o koszarach. ");
    add_act("emote spluwa leniwie na ziemie. ");
    add_act("emote spoglada na ciebie ukradkiem. ");

    set_cact_time(10);
    add_cact("emote rusza na ciebie zdecydowanym krokiem, marszczac brwi w "
        +"skupieniu. ");
    add_cact("emote usiluje zajsc cie z boku. ");
    add_cact("emote powarkuje pod nosem cos o zbrodniczym elemencie. ");

    add_weapon(BRON);
    add_armour(ZBRO);
    add_armour(BUTY);
    add_armour(SPOD);

    add_prop(LIVE_I_NEVERKNOWN, 1);


    add_ask(({"zadanie", "prace"}), VBFC_ME("pyt_o_zadanie"));
    set_default_answer(VBFC_ME("default_answer"));

    set_skill(SS_WEP_SWORD,75);
    set_skill(SS_UNARM_COMBAT, 50);
    set_skill(SS_PARRY, 52);
    set_skill(SS_DEFENCE,32);
    set_skill(SS_AWARENESS,64);
    set_skill(SS_BLIND_COMBAT,20);

    create_straznik();

}

void
powiedz_gdy_jest(object player, string tekst)
{
    if (environment(this_object()) == environment(player))
        this_object()->command(tekst);
}
string
default_answer()
{
    set_alarm(2.0, 0.0, "powiedz_gdy_jest", this_player(),
        "emote ostentacyjnie ignoruje pytanie.");
    return "";
}

string
pyt_o_zadanie()
{
    if(!query_attack())
    {
        set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
        "emote skinieniem d^loni wskazuje oficera. ");
    }
    else
        set_alarm(0.5,0.0,"powiedz_gdy_jest", TP, "emote wzrusza ramionami. ");
    return "";
}

void
add_introduced(string imie_mia, string imie_bie)
{
    set_alarm(2.0, 0.0, "return_introduce", find_player(imie_mia));
}
void
return_introduce(object ob)
{
    command("emote wzrusza ramionami. ");
}

void
emote_hook(string emote, object wykonujacy)
{
    switch (emote)
    {
        case "kopnij":
            set_alarm(1.5, 0.0, "nienajlepszy", wykonujacy);
            break;
        case "spoliczkuj":
            set_alarm(1.5, 0.0, "nienajlepszy", wykonujacy);
            break;
        case "opluj" :
            set_alarm(2.5, 0.0, "nienajlepszy", wykonujacy);
            break;
        case "poca^luj":
            if(wykonujacy->query_gender() == 1)
            {
                switch(random(2))
                {
                    case 0:
                        command("emote mamrocze co^c o przytulnej alkowie.");
                        break;
                    case 1:
                        command("emote u^smiecha si^e szelmowsko.");
                        break;
                }
            }
            else
            {
                switch(random(2))
                {
                    case 0:
                        command("emote warczy gro^xnie, szczerz^ac krzywe z^eby.");
                        break;
                    case 1:
                        TP->catch_msg(QCIMIE(TO, PL_MIA)+" odpowiada Ci brutalnym kopniakiem.\n");
                        saybb(QCIMIE(TO, PL_MIA)+" odpowiada " +QIMIE(TP, PL_CEL)+" brutalnym kopniakiem.\n");
                        break;
                }
            }

            break;
        case "przytul":
            if(wykonujacy->query_gender() == 1)
            {
                switch(random(2))
                {
                    case 0:
                        command("emote mamrocze co^c o przytulnej alkowie.");
                        break;
                    case 1:
                        command("emote u^smiecha si^e szelmowsko.");
                        break;
                }
            }
            else
            {
                switch(random(2))
                {
                    case 0:
                        command("emote warczy gro^xnie, szczerz^ac krzywe z^eby.");
                        break;
                    case 1:
                        TP->catch_msg(QCIMIE(TO, PL_MIA)+" odpowiada Ci brutalnym kopniakiem.\n");
                        saybb(QCIMIE(TO, PL_MIA)+" odpowiada " +QIMIE(TP, PL_CEL)+" brutalnym kopniakiem.\n");
                        break;
                }
            }
            break;
    }
}


void
nienajlepszy(object wykonujacy)
{
    switch(random(2))
    {
        case 0:
            TP->catch_msg(QCIMIE(TO, PL_MIA)+" odpowiada Ci brutalnym kopniakiem.\n");
            saybb(QCIMIE(TO, PL_MIA)+" odpowiada " +QIMIE(TP, PL_CEL)+" brutalnym kopniakiem.\n");
            break;
        case 1:
            TP->catch_msg(QCIMIE(TO, PL_MIA)+" odpycha Ci^e i przechodzi dalej.\n");
            saybb(QCIMIE(TO, PL_MIA)+" odpycha " +QIMIE(TP, PL_CEL)+" i przechodzi dalej.\n");
            break;
    }
}