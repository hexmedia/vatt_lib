/* Autor: Sedzik i Avard
   Opis : Vayneela
   Data : 29.08.07 */

inherit "/std/humanoid.c";
inherit "/lib/straz/straznik.c";

#include "dir.h"
#include <std.h>
#include <ss_types.h>
#include <macros.h>
#include <stdproperties.h>
#include <pl.h>
#include <money.h>

#define MIASTO "z Piany"

#define BRON PIANA_BRONIE + "miecze/obosieczny_krotki.c"
#define ZBRO PIANA_ZBROJE + "przeszywanice/pikowana_brazowa_Mc.c"
#define BUTY PIANA_UBRANIA + "buty/czarne_zolnierskie_Mc.c"
#define SPOD PIANA_UBRANIA + "spodnie/grube_lniane_Mc.c"

object admin=TO->query_admin();

void
create_humanoid()
{
    dodaj_nazwy("stra積ik");
    dodaj_nazwy("oficer");
    ustaw_odmiane_rasy("m^e^zczyzna");

    random_przym(({"dumny", "dumni", "spokojny", "spokojni", "uwa積y", "uwa積i",
        "czujny", "czujni"}), ({"br您owooki", "br您owoocy"}), ({"m這dy", "m這dzi",
        "wysoki", "wysocy"}), ({"czarnow這sy", "czarnow這si", "blondw這sy",
        "blondw這si"}), 2);

    set_gender(G_MALE);
    set_spolecznosc("Piana");

    set_long("Postaw^e tego cz^lowieka mo^zna por^ownac do zachowania koguta we "
        +"w^lasnym kurniku. Pewny siebie, zaznajomiony z otoczeniem, wodz^acy "
        +"dookola w^ladczym spojrzeniem. Nienaganny ubi^or, przygotowany zapewne "
        +"przez kogo^s du^zo ni^zej postawionego, l^sni niczym nowy, odbijaj^ac "
        +"wszelkie ^swiatlo na^n padaj^ace. Co rusz wydaje swym podw^ladnym "
        +"polecenia, niekiedy zupe^lnie bezsensowne, tylko po to, by podkre^sli^c "
        +"swe mo^zliwosci i po^lechta^c spragnione pochwa^l ego. Bujna czupryna "
        +"zdradza m^lody jeszcze wiek oficera, co t^lumaczy^c mo^ze jego zap^edy do "
        +"wy^zywania si^e na innych. \n");

    set_stats (({ 120, 150, 150, 150, 150 }));
    add_prop(CONT_I_WEIGHT, 70000);
    add_prop(CONT_I_HEIGHT, 185);

    set_act_time(60);
    add_act("emote strzepuje z r^ekawa jaki^s niewidoczny py^lek. ");
    add_act("emote poprawia u^lo^zenie swej fryzury. ");
    add_act("emote rozgl^ada sie w^ladczo dooko^la. ");
    add_act("emote sprawdza ostro^s^c swej broni. ");
    add_act("emote wydaje kilka polece^n podw^ladnym. ");
    add_act("'Spok^oj ma by^c! ");

    set_cact_time(10);
    add_cact("'P^oki ja pilnuj^e tu porz^adku nie pozwol^e ci na to! ");
    add_cact("'Wycofaj si^e natychmiast, a czeka ci^e tylko areszt! ");
    add_cact("emote klnie si^e na swego boga, prosz^ac go o b^logos^lawienstwo. ");
    add_cact("emote robi szybki wypad w prz^od, by zaraz si^e cofn^a^c! ");

    add_weapon(BRON);
    add_armour(ZBRO);
    add_armour(BUTY);
    add_armour(SPOD);

    add_prop(LIVE_I_NEVERKNOWN, 1);

    add_ask(({"zadanie", "prace"}), VBFC_ME("pyt_o_zadanie"));
    set_default_answer(VBFC_ME("default_answer"));

    set_skill(SS_WEP_SWORD,70);
    set_skill(SS_UNARM_COMBAT, 50);
    set_skill(SS_PARRY, 65);
    set_skill(SS_DEFENCE,35);
    set_skill(SS_AWARENESS,67);
    set_skill(SS_BLIND_COMBAT,22);

    create_straznik();
}

void
powiedz_gdy_jest(object player, string tekst)
{
    if (environment(this_object()) == environment(player))
        this_object()->command(tekst);
}
string
default_answer()
{
    set_alarm(2.0, 0.0, "powiedz_gdy_jest", this_player(),
        "powiedz Nie marnuj mojego czasu w^l^ocz^ego!");
    return "";
}

string
pyt_o_zadanie()
{
    if(!query_attack())
    {
        set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
        "powiedz Z drogi ofermo! ");
    }
    else
        set_alarm(0.5,0.0,"powiedz_gdy_jest", TP, " powiedz Odejd^x, nie zajmuj^e si^e tym. ");
    return "";
}

void
add_introduced(string imie_mia, string imie_bie)
{
    set_alarm(2.0, 0.0, "return_introduce", find_player(imie_mia));
}
void
return_introduce(object ob)
{
    command("emote nie zwraca na to najmniejszej uwagi. ");
}

void
emote_hook(string emote, object wykonujacy)
{
    switch (emote)
    {
        case "kopnij":
            set_alarm(1.5, 0.0, "nienajlepszy", wykonujacy);
            break;
        case "spoliczkuj":
            set_alarm(1.5, 0.0, "nienajlepszy", wykonujacy);
            break;
        case "opluj" :
            set_alarm(2.5, 0.0, "nienajlepszy", wykonujacy);
            break;
        case "poca^luj":
            if(wykonujacy->query_gender() == 1)
                command("emote odchodzi niewzruszony.");
            else
            {
                switch(random(2))
                {
                    case 0:
                        TP->catch_msg(QCIMIE(TO, PL_MIA)+" nie zwraca na Ciebie najmniejszej uwagi.\n");
                        saybb(QCIMIE(TO, PL_MIA)+" nie zwraca na " +QIMIE(TP, PL_BIE)+" najmniejszej uwagi.\n");
                        break;
                    case 1:
                        TP->catch_msg(QCIMIE(TO, PL_MIA)+" patrzy na Ciebie z pogard^a\n");
                        saybb(QCIMIE(TO, PL_MIA)+" patrzy na "+QIMIE(TP, PL_BIE)+" z pogard^a.\n");
                        break;
                }
            }
            break;
        case "przytul":
            if(wykonujacy->query_gender() == 1)
                command("emote odchodzi niewzruszony.");
            else
            {
                switch(random(2))
                {
                    case 0:
                        TP->catch_msg(QCIMIE(TO, PL_MIA)+" nie zwraca na Ciebie najmniejszej uwagi.\n");
                        saybb(QCIMIE(TO, PL_MIA)+" nie zwraca na " +QIMIE(TP, PL_BIE)+" najmniejszej uwagi.\n");
                        break;
                    case 1:
                        TP->catch_msg(QCIMIE(TO, PL_MIA)+" patrzy na Ciebie z pogard^a\n");
                        saybb(QCIMIE(TO, PL_MIA)+" patrzy na "+QIMIE(TP, PL_BIE)+" z pogard^a.\n");
                        break;
                }
            }
        break;
    }
}

void
nienajlepszy(object wykonujacy)
{
   command("emote warczy gro^xnie. ");
}