/* Autor: Avard
   Opis : Samaia
   Data : 08.09.07

 * W listopadzie 2007 czy grudniu nawet bezczelnie wykorzystany do zadania
 * opisanego kiedys przez Ohma. Zreszta moze sie dobrze sklada, bo facet
 * musi umiec czytac, co nie pasowalo do wymyslonego pierwotnie rybaka. (gj)
 *
 * checkpointy
 * 1 - dostanie zlecenia
 * 2 - wyciagniecie informacji od sklepikarza
 * 3 - wykupienie medalionu
 * 4 - przekazanie slow karczmarza
 * 5 - dostarczenie medalionu
 * 7 - odnalezienie listu
 * 8 - przekazanie, fin.
 */

#pragma unique
#pragma strict_types

#include <macros.h>
#include <stdproperties.h>
#include <money.h>
#include <pl.h>
#include <formulas.h>
#include <std.h>
#include <ss_types.h>
#include <wa_types.h>
#include <composite.h>
#include <filter_funs.h>
#include <pattern.h>
#include <exp.h>
#include "dir.h"

inherit PIANA_STD_NPC;
inherit "/lib/craftsman.c";

#define WZORY (PIANA + "wzory/")
#define REP_MAT "/d/Standard/items/materialy/"


//int czy_walka();
//int walka = 0;
string dlugasny();

int
nowy_robiacy(object kto)
{
    if (ENV(TO)==ENV(kto))
    {
        write_file(ROBIACY, lower_case(kto->query_name()) + "#10000000#"); //8 checkpointow
     // TP->catch_msg(set_color(42) + "***CHECKPOINT 1***\n" + clear_color());
        return 1;
    }
    else
        return 0;
}

void
usun_robiacego(object kto)
{
    int i;
    string *arr;

    arr = explode(read_file(ROBIACY), "#");
    i = member_array(lower_case(kto->query_name()), arr);

    if (i != -1)
        arr = exclude_array(arr, i, i+1);

    rm(ROBIACY);
    write_file(ROBIACY, implode(arr, "#") + "#");
}

void
create_piana_humanoid()
{
    ustaw_odmiane_rasy("mezczyzna");
    dodaj_nazwy("krawiec");
    dodaj_nazwy("staruszek");
    set_gender(G_MALE);
    dodaj_przym("bezz^ebny","bezz^ebni");
    dodaj_przym("pomarszczony","pomarszczeni");

    set_spolecznosc("Piana");

    set_long("@@dlugasny@@");

    set_stats (({ 25, 30, 30, 55, 0 }));

    set_skill(SS_UNARM_COMBAT, 50);

    add_prop(CONT_I_WEIGHT, 90000);
    add_prop(CONT_I_HEIGHT, 160);

    set_act_time(90);
    add_act("emote kaszle w pi^e^s^c. ");
    add_act("emote zanosi si^e chrapliwym kaszlem. ");
    add_act("emote podrzuca leciutko w d^loniach ma^ly igielnik.  ");
    add_act("emote wygl^ada przez wystaw^e na zewn^atrz.");
    add_act("emote podryguje lekko, potrz^asaj^ac brzuszyskiem.");
    add_act("emoteprzeciera oczy ze zm^eczenia. ");
    add_act("emote kaszle przeci^agle.");

    set_cact_time(10);
    add_cact("'Wyno^s si^e! ");
    add_cact("'Wynocha!");
    add_cact("'Id^x sobie, zostaw mnie!");

    add_ask(({"prac^e"}), VBFC_ME("pyt_o_prace"));
    add_ask(({"ubrania"}), VBFC_ME("pyt_o_ubrania"));
    add_ask(({"zdrowie"}),VBFC_ME("pyt_o_zdrowie"));

    setuid();
    seteuid(getuid());

    set_default_answer(VBFC_ME("default_answer"));

    set_skill(SS_CRAFTING_TAILOR, 80);
    config_default_craftsman();
    craftsman_set_domain(PATTERN_DOMAIN_TAILOR);
    craftsman_set_sold_item_patterns( ({
                WZORY + "bluzka_skromna_kremowa",
                WZORY + "koszula_lniana_prosta",
                WZORY + "koszula_prosta_jedwabna",
                WZORY + "sandaly_jasne_drewniane",
                WZORY + "spodnie_szare_lniane",
                WZORY + "buty_czarne_zamszowe",
                WZORY + "buty_zwykle_brazowe",
                WZORY + "koszula_cienka_luzna",
                WZORY + "koszula_dluga_blekitna",
                WZORY + "koszula_jasna_sznurowana",
                WZORY + "koszula_skromna_lniana",
                WZORY + "spodnie_jasne_lniane",
                WZORY + "spodnie_skromne_czarne",
                WZORY + "spodnie_szare_niewyszukane",
                WZORY + "spodnie_szare_welniane",
                WZORY + "spodnie_szorstkie_welniane",
                WZORY + "sukienka_prymitywna_jutowa"
                }) );

    set_store_room(PIANA_LOKACJE + "magazyn_krawca");

	set_money_greed_buy(208);
	set_money_greed_sell(495);
	set_money_greed_change(125);

	add_armour(PIANA_UBRANIA + "buty/zwykle_brazowe_Lc.c");
	add_armour(PIANA_UBRANIA + "koszule/czarna_wiazana_XLc.c");
	add_armour(PIANA_UBRANIA + "spodnie/skromne_czarne_Sc.c");

    add_weapon(PIANA_NARZEDZIA + "nozyczki");

  //// questowe
  	add_act("emote mruczy: I do tego on - jakbym mia^l za ma^lo problem^ow...");
	add_act("emote mruczy: A mo^ze sam go poszukam?");
	add_act("emote mruczy: Gdybym tylko zna^l tam kogo^s...");
	add_act("emote mruczy: Czemu ten g^lupiec nie m^og^l zosta^c tu, w domu?");

    add_ask(({"problem", "problemy", "k^lopot", "k^lopoty", "strapienie",
    "pomoc", "zadanie", "zmartwienia", "zmartwienie"}), VBFC_ME("pyt_pomoc"));

    add_prop(NPC_M_NO_ACCEPT_GIVE, 0);
  ////
}

void
powiedz_gdy_jest(object player, string tekst)
{
    if (environment(this_object()) == environment(player))
        this_object()->command(tekst);
}
string
pyt_o_prace()
{
    if(!query_attack())
    {
        set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
        "'Praca, jak praca... z czego^s ^zy^c trzeba.");
    }
    else
    {
        set_alarm(0.5,0.0,"powiedz_gdy_jest", TP, "'Zostaw mnie!");
    }
    return "";
}
string
pyt_o_ubrania()
{
    if(!query_attack())
    {
        set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
        "'Przejrzyj, znajdziesz co^s dla siebie.");
    }
    else
    {
        set_alarm(0.5,0.0,"powiedz_gdy_jest", TP, "'Zostaw mnie!");
    }
    return "";
}
string
pyt_o_zdrowie()
{
    if(!query_attack())
    {
        set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
        "'Oj, nienajlepiej...");
        set_alarm(1.0,0.0,"powiedz_gdy_jest",TP,"westchnij starczo");
    }
    else
    {
        set_alarm(0.5,0.0,"powiedz_gdy_jest", TP, "'Zostaw mnie!");
    }
    return "";
}

string
pyt_pomoc()
{
    if (member_array(lower_case(TP->query_name(0)), explode(read_file(ZROBILI), "#"))!= -1)
    {
        set_alarm(1.0, 0.0, "powiedz_gdy_jest", TP, "powiedz do " + OB_NAME(TP) +
        " Nie b^ed^e ci znowu zawraca^l g^lowy swoimi problemami... niech ci^e bogowie maj^a w opiece.");
        set_alarm(2.5, 0.0, "powiedz_gdy_jest", TP, "usmiech gorzko do " + OB_NAME(TP));
    }
    else
    {
        if (member_array(lower_case(TP->query_name()), explode(read_file(ROBIACY), "#"))!= -1)
        {
            set_alarm(1.0, 0.0, "powiedz_gdy_jest", TP, "powiedz do " + OB_NAME(TP) +
            " Nic si^e nie zmieni^lo. Co by si^e mia^lo zmieni^c? Problemy si^e same nie rozwiazuj^a, do cholery.");
            set_alarm(4.5, 0.0, "powiedz_gdy_jest", TP, "machnij reka ciezko");
            set_alarm(7.0, 0.0, "powiedz_gdy_jest", TP, "powiedz do " + OB_NAME(TP) +
            " Nazywa si^e Milon. Wysoki i brodaty cz^lowiek - z nikim go nie pomylisz. " +
            "Przeka^z mu, ^ze brat pro... ^ze brat uwa^za go za durnia i ^z^ada znaku ^zycia.");
            set_alarm(12.0, 0.0, "powiedz_gdy_jest", TP, "powiedz :cicho: I ^ze si^e martwi...");
            set_alarm(16.0, 0.0, "powiedz_gdy_jest", TP, "pociagnij nosem szybko");
            set_alarm(18.5, 0.0, "powiedz_gdy_jest", TP, "powiedz do " + OB_NAME(TP) +
            " Wr^o^c i opowiedz mi o wszystkim. No ju^z, id^x st^ad, nie mam dla ciebie prezentu!");
        }
        else
        {
            set_alarm(2.0, 0.0, "powiedz_gdy_jest", TP, "namysl sie");
            set_alarm(5.0, 0.0, "powiedz_gdy_jest", TP, "powiedz do " + OB_NAME(TP) +
            " Mam pewne, nazwijmy to, w^atpliwo^sci... Chodzi o mojego m^lodszego brata.");
            set_alarm(9.5, 0.0, "powiedz_gdy_jest", TP, "powiedz do " + OB_NAME(TP) +
            " Pewien czas temu wybra^l si^e do Rinde w poszukiwaniu grosza. Jak " +
            "si^e dowiedzia^lem, szybko naj^a^l go jeden z tamtejszych kupc^ow czy sklepikarzy - " +
            "i wiod^lo mu si^e wcale dobrze. I nie by^lbym zawraca^l sobie " +
            "g^lowy gdyby nie to, ^ze... od miesi^aca nie mam od niego ^zadnych " +
            "wiadomo^sci.");
            set_alarm(18.5, 0.0, "powiedz_gdy_jest", TP, "powiedz do " + OB_NAME(TP) +
            " Mo^ze w og^ole nie powinienem zwraca^c na to uwagi, ale chyba " +
            "poczuj^e si^e lepiej je^sli si^e dowiem, co ten wariat wyrabia");
            set_alarm(21.0, 0.0, "powiedz_gdy_jest", TP, "skrzyw sie lekko");
            set_alarm(26.0, 0.0, "powiedz_gdy_jest", TP, "powiedz do " + OB_NAME(TP) +
            " Nazywa si^e Milon. Szybko go poznasz, bo jest diabelnie wysoki i " +
            "ma bujn^a brod^e, a pewnie niewielu takich w Rinde. Jak ju^z go spotkasz, " +
            "wr^o^c i opowiedz mi o nim");
            set_alarm(33.0, 0.0, "powiedz_gdy_jest", TP, "powiedz do " + OB_NAME(TP) +
            " I zar^eczam ci, nie zapominam o oddanych mi przys^lugach");
            set_alarm(35.0, 0.0, "powiedz_gdy_jest", TP, "pokiwaj w zamysleniu");
            set_alarm(35.0, 0.0, "nowy_robiacy", TP);
        }
    }

    return "";
}

string
default_answer()
{
    if(!query_attack())
    {
        switch(random(3))
        {
            case 0: set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
                "emote wygl^ada przez okno.");break;
            case 1: set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
                "'Wybacz, nie chc^e o tym rozmawia^c.");break;
            case 2: set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
                "wzrusza ramionami oboj^etnie.");break;
        }
    }
    else
    {
        set_alarm(0.5,0.0,"powiedz_gdy_jest", TP, "'Zostaw mnie!");
    }
    return "";
}

void
exp_dla(object kto, int ile)
{
    /* Komentarz: zmienna ile przyjmuje wartosci calkowite do 6, wskazujac
     * ile z mozliwych punktow spelnil gracz. W przypadku wzorowo wykonane-
     * go questa nagroda jest 6000 punktow (6*1000)
     */
    kto->increase_ss(3, ile*EXP_QUEST_UTOPCE_INT);

    kto->catch_msg("Czujesz si^e teraz bardziej do^swiadczon" + kto->koncowka("y", "a") + ".\n");
}

void
kasa_dla(object old)
{
    if (interactive(old) && (ENV(old) == ENV(TO)))
    {
        old->catch_msg(QCIMIE(TO, PL_MIA)+" daje ci gar�� monet.\n");
        tell_room(ENV(old), QCIMIE(TO, PL_MIA)+" daje "+QIMIE(old, PL_CEL)+" jakie� monety.\n", old);
        MONEY_ADD(old, 1000);
    }
}

int
opowiedz(string str)
{
    string oczym, *cp, *x;
    object obkomu;
    int y = 0;

    if (member_array(lower_case(TP->query_name(0)), explode(read_file(ROBIACY), "#")) == -1)
    {
        notify_fail("S^lucham?\n");
        return 0;
    }

    if (!str || str == "")
    {
        notify_fail("Komu i o czym chcesz opowiedzie^c?\n");
        return 0;
    }

    if (!parse_command(str, all_inventory(ENV(TP)), "%o:" + PL_CEL + " 'o' %s", obkomu, oczym))
    {
        notify_fail("Komu i o czym chcesz opowiedzie^c?\n");
        return 0;
    }

    if (obkomu != TO)
    {
        notify_fail("Komu i o czym chcesz opowiedzie^c?\n");
        return 0;
    }

    oczym = LC(oczym);
    saybb(QCIMIE(TP, 0) + " opowiada " + QIMIE(obkomu, 2) + " o czym^s.\n");

    if (oczym ~= "milonie" ||
        oczym ~= "bracie" ||
        oczym ~= "losach brata" ||
        oczym ~= "losach milona" ||
        oczym ~= "jego bracie")
    {
        cp = explode(read_file(ROBIACY), "#");
        if (member_array(lower_case(TP->query_name()), cp)!= -1)
        {
          if (explode(cp[member_array(lower_case(TP->query_name()), cp) + 1], "")[6] == "0")
          {
            switch (explode(cp[member_array(lower_case(TP->query_name()), cp) + 1], "")[2])
            {
                case "0":
                    notify_fail("Nie dowiedzia^le^s si^e niczego znacz^acego na temat Milona.\n");
                    return 0;
                    break;
                case "X":
                case "1":
                    x = explode(cp[member_array(lower_case(TP->query_name()), cp) + 1], "");
                    if (x[3] != "1")
                    {
                        x[3] = "1";
                        cp[member_array(lower_case(TP->query_name()), cp) + 1] = implode(x, "");
                        write_bytes(ROBIACY, 0, implode(cp, "#"));
                     // TP->catch_msg(set_color(42) + "***CHECKPOINT 4***\n" + clear_color());
                    }
                    TP->catch_msg("Opowiadasz " + QIMIE(obkomu, 2) + " o wszystkim " +
                    "czego dowiedzia^le^s si^e na temat jego brata.\n");
                    set_alarm(3.0, 0.0, "command", "namysl sie");
                    set_alarm(5.0, 0.0, "powiedz_gdy_jest", TP, "powiedz do " +
                    OB_NAME(TP) + " Wspomnia^l" + TP->koncowka("e", "a") + "^s o medalionie... Bardzo zale^zy " +
                    "mi na tym, by go odzyska^c");
                    return 1;
                    break;
            }
          }
          else
          {
                if (TP->query_prop("dal_mi_list_okurwa") == 0)
                {
                    TP->catch_msg("Mo^ze najpierw daj mu list?\n");
                    return 1;
                }

                x = explode(cp[member_array(lower_case(TP->query_name()), cp) + 1], "");
                for (int i = 0; i < sizeof(x); i++)
                {
                    if (x[i] == "1")
                        y++;
                }

                write_file(ZROBILI, lower_case(TP->query_name()) + "#");
             // TP->catch_msg(set_color(42) + "***FIN***\n" + clear_color());
                TP->catch_msg("Opowiadasz " + QIMIE(obkomu, 2) + " o wszystkim " +
                "czego dowiedzia^le^s si^e na temat jego brata.\n");
                set_alarm(3.0, 0.0, "command", "zwies glowe .");
                set_alarm(7.0, 0.0, "powiedz_gdy_jest", TP, "powiedz do " +
                OB_NAME(TP) + " Id^x ju^z st^ad.");
                set_alarm(10.0, 0.0, "kasa_dla", TP);
                set_alarm(14.0, 0.0, "exp_dla", TP, y);
                usun_robiacego(TP);
                return 1;
          }

        }
    }
    else
    {
        TP->catch_msg("Opowiadasz " + QIMIE(obkomu, 2) + " o czym^s.\n");
        set_alarm(3.0, 0.0, "command", "wzrusz ramionami");
        set_alarm(5.0, 0.0, "powiedz_gdy_jest", TP, "powiedz do " +
            OB_NAME(TP) + " Lepiej powiedz o moim bracie, zamiast zawraca^c " +
            "mi g^low^e takimi pierdo^lami!");
        return 1;
    }
}

int
medalion(int chu = 0)
{
    string *x, *cp;

    set_alarm(1.0, 0.0, "command", "emote uwa^znie przygl^ada si^e niewielkiemu srebrnemu medalionowi.");

    if (chu)
    {
        set_alarm(4.0, 0.0, "powiedz_gdy_jest", TP, "powiedz do " + OB_NAME(TP) +
        " To... sk^ad to masz? Sk^ad ty to do cholery masz, z^lodzieju?");
        set_alarm(6.0, 0.0, "powiedz_gdy_jest", TP, "command", "emote odsuwa si^e od ciebie nieufnie.");
    }
    else
    {
        cp = explode(read_file(ROBIACY), "#");
        x = explode(cp[member_array(lower_case(TP->query_name()), cp) + 1], "");
        if (x[4] != "1")
        {
            x[4] = "1";
            cp[member_array(lower_case(TP->query_name()), cp) + 1] = implode(x, "");
            write_bytes(ROBIACY, 0, implode(cp, "#"));
         // TP->catch_msg(set_color(42) + "***CHECKPOINT 5***\n" + clear_color());
        }
        set_alarm(4.0, 0.0, "powiedz_gdy_jest", TP, "powiedz do " + OB_NAME(TP) +
        " To ten... na bog^ow, gdzie jest m^oj brat? Znajd^x go, prosz^e!");
        set_alarm(6.0, 0.0, "powiedz_gdy_jest", TP, "tupnij");
    }
}

int
list(int chu = 0)
{
    string *x, *cp;

    set_alarm(1.0, 0.0, "command", "emote czyta stary przemoczony list.");

    if (chu)
    {
        set_alarm(5.0, 0.0, "powiedz_gdy_jest", TP, "powiedz do " + OB_NAME(TP) +
        " Co to ma by^c? Kpisz sobie ze mnie?");
        set_alarm(6.5, 0.0, "powiedz_gdy_jest", TP, "command", "emote odsuwa si^e od ciebie nieufnie.");
    }
    else
    {
     /* cp = explode(read_file(ROBIACY), "#");
        x = explode(cp[member_array(lower_case(TP->query_name()), cp) + 1], "");
        if (x[7] != "1")
        {
            x[7] = "1";
            cp[member_array(lower_case(TP->query_name()), cp) + 1] = implode(x, "");
            write_bytes(ROBIACY, 0, implode(cp, "#"));
            TP->catch_msg(set_color(42) + "***CHECKPOINT 8***\n" + clear_color());
        } */

        set_alarm(5.0, 0.0, "command", "popatrz");
        set_alarm(7.0, 0.0, "powiedz_gdy_jest", TP, "powiedz :do " +
        OB_NAME(TP) + " cicho: Op... opowiedz mi o wszystkim");
        TP->add_prop("dal_mi_list_okurwa", 1);
    }
}

void
add_introduced(string imie_mia, string imie_bie)
{
    set_alarm(2.0, 0.0, "return_introduce", find_player(imie_mia));
}
void
return_introduce(object ob)
{
    command("skin");
}

void
emote_hook(string emote, object wykonujacy)
{
    switch (emote)
    {
        case "kopnij": set_alarm(0.5, 0.0, "zle", wykonujacy);
                    break;
        case "spoliczkuj": set_alarm(0.5, 0.0, "zle", wykonujacy);
                    break;
        case "opluj" : set_alarm(0.5, 0.0, "zle", wykonujacy);
                    break;
        case "szturchnij" : set_alarm(0.5, 0.0, "zle", wykonujacy);
                    break;
        case "poca^luj": set_alarm(0.5,0.0, "caluje",wykonujacy);
                    break;
        case "przytul": set_alarm(0.5,0.0, "dobre",wykonujacy);
                    break;
        case "poglaszcz": set_alarm(0.5, 0.0, "dobre", wykonujacy);
                    break;
        case "poklep": set_alarm(0.5, 0.0, "dobre", wykonujacy);
                    break;
        case "usmiech": set_alarm(0.5, 0.0, "dobre", wykonujacy);
                    break;
        case "mrugnij": set_alarm(0.5, 0.0, "dobre", wykonujacy);
                    break;
        case "oczko": set_alarm(0.5, 0.0, "dobre", wykonujacy);
                    break;
    }
}

void
zle(object wykonujacy)
{
    switch(random(3))
    {
        case 0: command("westchnij ciezko");break;
        case 1: command("'Zostaw mnie w spokoju.");break;
        case 2: command("emote kuli si^e trwo^znie.");break;
    }
}

void
dobre(object wykonujacy)
{
    switch(random(4))
    {
        case 0: command("usmiech lekko");break;
        case 1: command("usmiechnij sie do "+OB_NAME(wykonujacy));
        break;
        case 2: command("mrugnij weso^lo do "+OB_NAME(wykonujacy));
        break;
        case 3: command("zachichocz krotko");break;
    }
}

void
caluje(object wykonujacy)
{
    if(wykonujacy->query_gender() == 1)
    {
        switch(random(2))
        {
            case 0: command("Oj, stary ja jestem...");break;
            case 1: command("usmiechnij sie do "+OB_NAME(wykonujacy));break;
        }
    }
    else
    {
        switch(random(4))
        {
            case 0: command("emote wyciera usta wierzchem d^loni.");break;
            case 1: command("spojrzyj z obrzydzeniem "+
                "na "+OB_NAME(wykonujacy));break;
            case 2: command("pokrec glowa z niedowierzaniem");break;
            case 3: command("'A won mi st^ad!");break;
        }
    }
}
/*
void
attacked_by(object wrog)
{
    if(walka==0)
    {
        set_alarm(10.0, 0.0, "czy_walka", 1);
        walka = 1;
        return ::attacked_by(wrog);
    }
    else
    {
        walka = 1;
        return ::attacked_by(wrog);
    }
}

int
czy_walka()
{
    if(!query_attack())
    {
        command("zadrzyj z przera^zenia.");
        walka = 0;
        return 1;
    }
    else
    {
       set_alarm(5.0, 0.0, "czy_walka", 1);
    }

}*/

string
dlugasny()
{
    string str;
    str = "Ten stary, przygarbiony m^e^zczyzna, bez przerwy mam^laj^acy "+
        "co^s w ustach, raz po raz u^smiecha si^e uprzejmie ukazuj^ac "+
        "ca^lkowity brak z^eb^ow. Bezustannie poprawia resztki w^los^ow na "+
        "swojej g^lowie, przeczesuj^ac palcami siwe pasma. Jego przeorana "+
        "liniami zmarszczek twarz, lecz mimo tak podesz^lego wieku, wci^a^z "+
        "zdradza pogod^e ducha i rado^s^c jasnymi, acz nieco m^etnymi "+
        "oczami. ";
    if(!query_attack())
    {
        str += "Staruszek, najwyra^xniej z nud^ow, mi^etosi w d^loniach "+
            "p^l^ocienny igielnik pe^len powbijanych we^n szpil i igie^l. ";
    }
    str += "\n ";
    return str;
}

void
init()
{
    ::init();
    craftsman_init();
    add_action("opowiedz", "opowiedz");
}

public string
query_auto_load()
{
    return ::query_auto_load() + query_craftsman_auto_load();
}

public string
init_arg(string arg)
{
    return init_craftsman_arg(::init_arg(arg));
}

string
krawiec_pracuje()
{
    if (craftsman_is_busy()) {
        switch (random(5)) {
            case 0: return "emote przebiera mi^edzy materia^lami.";
            case 1: return "emote szyje co^s zawzi^ecie.";
            case 2: return "emote wycina co^s zr^ecznie.";
            case 3: return "emote wyci^aga z szuflady jakie^s przybory do "+
                "szycia.";
            case 4: return "emote ociera kropelki potu z czo^la.";
        }
    }
    return "";
}
void
start_me()
{
    command("usiadz na stolku");
}

void
enter_inv(object ob, object from)
{
    ::enter_inv(ob, from);

    if (MASTER_OB(ob) == MEDALION)
    {
        if (member_array(lower_case(TP->query_name()), explode(read_file(ROBIACY), "#")) == -1)
            set_alarm(1.0, 0.0, "medalion", 1);
        else
            set_alarm(1.0, 0.0, "medalion");
        return;
    }

    else if (MASTER_OB(ob) == LIST)
    {
        if (member_array(lower_case(TP->query_name()), explode(read_file(ROBIACY), "#")) == -1)
            set_alarm(1.0, 0.0, "list", 1);
        else
            set_alarm(1.0, 0.0, "list");
        return;
    }

    if(!ob->query_coin_type() && interactive(from))
    {

    set_alarm(1.0, 0.0, "powiedz_gdy_jest", from, "powiedz do " + OB_NAME(from)+
    " Nie, dzi�kuj�. Po co mi to?");
    if (environment() == environment(from))
    {
        set_alarm(2.0, 0.0, "command", "daj " + OB_NAME(ob) + " " +
        OB_NAME(from));
    }
    else
        set_alarm(2.0, 0.0, "command", "odloz " + OB_NAME(ob));
    }
}
