
/* Autor: Avard
   Opis : Samaia
   Data : 3.11.07

    dnia 26.03.2008 dodalem prace z noszeniem workow. Vera.
 */
#pragma strict_types
#pragma unique

#include <std.h>
#include <ss_types.h>
#include <macros.h>
#include <stdproperties.h>
#include <pl.h>
#include <money.h>
#include <object_types.h>
#include "dir.h"

inherit PIANA_STD_NPC;
inherit "/lib/sklepikarz.c";
//int czy_walka();
//int walka = 0;

//do ceny jest dodawany jeszcze random
#define ZAPLATA (10+random(25))
#define LISTA (PIANA+"lista_pracownika_poczty")

void
create_piana_humanoid()
{
    ustaw_odmiane_rasy("p^o^lelf");
    dodaj_nazwy("mieszczanin");
    dodaj_nazwy("pracownik");
    set_gender(G_MALE);
    dodaj_przym("pos^epny","pos^epni");
    dodaj_przym("szarooki","szaroocy");
    set_spolecznosc("Piana");

    set_long("Odstaj^ace, nieco spiczaste uszy, podkr^a^zone oczy i "+
        "poci^ag^le rysy charakteryzuj^a tego p^o^lelfa, kt^ory "+
        "najwidoczniej jest urz^ednikiem pocztowym. Nie wygl^ada on na "+
        "zbyt szcz^e^sliwego, jego smutny wyraz twarzy i przepe^lnione "+
        "pust^a oczy zdaj^a si^e m^owi^c same za siebie. Jego w^at^la "+
        "sylwetka i postura raczej jakiego^s niedorostka niezbyt godzi "+
        "si^e ze sinym zarostem na twarzy, prawdopodobnie odziedziczonym "+
        "po ojcu, cz^lowieku, a tak^ze kilkoma zmarszczkami na czole. "+
        "Ciemne w^losy, zwi^azane na karku sp^lywaj^a na nieco zgarbione "+
        "od ci^ag^lej pracy plecy, by ostatecznie skr^eci^c si^e w kilka "+
        "subtelnych lok^ow.\n");

    set_stats (({ 25, 60, 40, 70, 70 }));

    set_skill(SS_UNARM_COMBAT, 50);

    add_prop(CONT_I_WEIGHT, 94000);//zeby mogl przyjac duzy worek
    add_prop(CONT_I_HEIGHT, 180);
    add_prop(NPC_I_NO_RUN_AWAY, 1);

    set_act_time(60);
    add_act("emote wygl^ada przez okno.");
    add_act("emote wzdycha t^esknie.");
    add_act("emote ze znudzeniem przegl^ada raz po raz te same dokumenty.");
    add_act("emote przybija piecz^atk^e na jednym z list^ow.");
    add_act("emote mruczy co^s do siebie pod nosem.");
    add_act("'Przyda�by si� kto� do pomocy.");

    set_cact_time(10);
    add_cact("krzyknij Stra^z, pomocy!");
    add_cact("krzyknij Cholera jasna! Stra^z! Bij^a!");
    add_cact("spanikuj");

    set_default_answer(VBFC_ME("default_answer"));

    add_armour(PIANA_UBRANIA + "buty/zwykle_brazowe_Lc");
    add_armour(PIANA_UBRANIA + "koszule/czarna_wiazana_XLc");
    add_armour(PIANA_UBRANIA + "spodnie/skromne_czarne_Sc");

    add_ask(({"prac^e","zadani^e","zlecenie","pomoc"}),
        VBFC_ME("pyt_o_prace"));
    add_ask(({"burmistrza","sklepikarza","kwiaciark^e","karczmarza",
        "ardgala","jorrisa","garnizonowego","codogana"}),
        VBFC_ME("pyt_o_ludzi"));

    config_default_sklepikarz();

    set_money_greed_buy(211);
    set_money_greed_sell(505);

    set_co_skupujemy(0);

    set_store_room(PIANA_LOKACJE + "magazyn_poczty.c");
    add_prop(NPC_M_NO_ACCEPT_GIVE, 0);
}

void
powiedz_gdy_jest(object player, string tekst)
{
    if (environment(this_object()) == environment(player))
        this_object()->command(tekst);
}
string
default_answer()
{
    if(!query_attack())
    {
        set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
            "'Zaj^ety jestem, dumam...");
    }
    else
    {
        set_alarm(0.5,0.0,"powiedz_gdy_jest", TP, "krzyknij Zostaw mnie!");
    }
    return "";
}

void
usun_z_listy(string x)
{
    int i;
    string *arr;

    arr = explode(read_file(LISTA), "#");
    i = member_array(x, arr);

    if (i != -1)
        arr = exclude_array(arr, i, i+2);

    rm(LISTA);
    write_file(LISTA, implode(arr, "#") + "#");
}

string
pyt_o_prace()
{
    if(!query_attack())
    {
      int i = member_array(TP->query_real_name(),
            explode(read_file(LISTA), "#"));
      if (i != -1)
      {
        if(explode(read_file(LISTA), "#")[i+1] == "z_piany")
            set_alarm(1.0, 0.0, "command_present", this_player(),
                   "powiedz do "+OB_NAME(TP) +
                   " Hmm, zdaje si�, �e ju� da�em ci worek. Je�li go zgubi^l"+
                  TP->koncowka("e^s","a^s","e^s")+", to niedobrze! Oj, oj!");
        else if(explode(read_file(LISTA), "#")[i+1] == "z_rinde")
            set_alarm(1.0, 0.0, "command_present", this_player(),
                   "powiedz do "+OB_NAME(TP) +
                   " Hmm, zdaje si�, �e mia�e� mi dostarczy� jaki� worek. Je�li go zgubi^l"+
                  TP->koncowka("e^s","a^s","e^s")+", to niedobrze! Oj, oj!");

        set_alarm(2.0, 0.0, "command_present", this_player(),
                   "powiedz do "+OB_NAME(TP) +
                   " Zap^la^c mi za to i postaram si� to jako� za�atwi�. Trzy korony.");
        return "";
      }

        object worek = clone_object(PIANA_PRZEDMIOTY+"worek_poczta");
        command("powiedz do "+OB_NAME(TP) +
                   " Tak, mam dla ciebie prac^e! Bierz worek i "+
                "zanie^s go czym pr^edzej na "+
                "poczt^e w Rinde.");
        worek->move(TP,1);//jako ze to praca dla zoltkow, nie robmy testow
        //dostali_worek+=({TP->query_real_name()});
        write_file(LISTA,TP->query_real_name()+ "#"+"z_piany"+"#");
        TP->catch_msg(QCIMIE(TO,PL_MIA)+" wr^ecza ci "+worek->short(PL_BIE)+".\n");
        tell_room(ENV(TO),QCIMIE(TO,PL_MIA)+" wr^ecza jaki^s worek "+QIMIE(TP,PL_BIE)+".\n",({TP}));

    }
    else
    {
        set_alarm(0.5,0.0,"powiedz_gdy_jest", TP, "'Zostaw mnie!");
    }
    return "";
}

int
zaplac(string str)
{
    //if(member_array(TP->query_real_name(),dostali_worek) == -1)
    if (member_array(TP->query_real_name(),
            explode(read_file(LISTA), "#")) == -1)
    {
       command("'Nie, nie trzeba...");
      return 1;
    }

    if(!MONEY_ADD(TP,-720))
    {
      set_alarm(1.0, 0.0, "command_present", this_player(),
                   "powiedz do "+OB_NAME(TP) +
                   " Chyba nie masz tyle. Trzy korony jeno.");
      return 1;
    }


    //dostali_worek-=({TP->query_real_name()});
    usun_z_listy(TP->query_real_name());
    write("P^lacisz pracownikowi poczty za zgubiony worek.\n");
    set_alarm(1.0, 0.0, "command_present", this_player(),
                   "powiedz do "+OB_NAME(TP) +
                   " No dobrze. Ale nie r^ob ju^z tak wi^ecej, bo strac^e "+
                    "posad^e.");
    return 1;
}
void
udalo_sie(object from, int ile,object ob)
{
    command("powiedz ^Swietnie! Oto twoja zap^lata.");
    from->catch_msg(QCIMIE(TO, PL_MIA)+" wyp^laca ci "+MONEY_TEXT(({ile,0,0}),PL_BIE)+".\n");
    tell_room(ENV(TO), QCIMIE(TO, PL_MIA)+" daje "+
        QIMIE(from, PL_CEL)+" jakie� monety.\n", from);

   ob->zniszcz();
}
void
nieudalo_sie(object ob,object kto)
{
    command("powiedz Dobra, nic nie szkodzi. Kto inny to zrobi.");
    ob->zniszcz();
}
void
enter_inv(object ob, object from)
{
    ::enter_inv(ob, from);

    if(!interactive(from))
        return;

    if(ob->query_worek_z_rinde())
    {
      int ile = ZAPLATA;
      if(MONEY_ADD(from,ile))
      {
//         object npc= find_object(DRUGI_POCZTOWIEC);
//         if(objectp(npc)) npc->wykonal(from->query_real_name());
            usun_z_listy(from->query_real_name());
        set_alarm(1.0,0.0,"udalo_sie",from,ile,ob);
      }
      return;
    }
    else if(ob->query_worek_z_piany())
    {
        usun_z_listy(from->query_real_name());
        set_alarm(1.0,0.0,"nieudalo_sie",ob,from);
        return;
    }
    else
      set_alarm(1.0, 0.0, "command", "odloz " + OB_NAME(ob));

      return;
}

string
pyt_o_ludzi()
{
    if(!query_attack())
    {
        switch(random(2))
        {
            case 0: set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
                "'W Pianie mieszkaj^a sami dobrzy ludzie.");break;
            case 1: set_alarm(0.5,0.0,"powiedz_gdy_jest", TP,
                "spojrzyj w zamysleniu");break;
        }
    }
    else
    {
        set_alarm(0.5,0.0,"powiedz_gdy_jest", TP, "'Zostaw mnie!");
    }
    return "";
}

void
add_introduced(string imie_mia, string imie_bie)
{
    set_alarm(2.0, 0.0, "return_introduce", find_player(imie_mia));
}
void
return_introduce(object ob)
{
    command("'Mi^lo mi.");
}

void
emote_hook(string emote, object wykonujacy)
{
    switch (emote)
    {
        case "kopnij": set_alarm(1.5, 0.0, "kopie", wykonujacy);
                    break;
        case "opluj" : set_alarm(2.5, 0.0, "kopie", wykonujacy);
                    break;
        case "spoliczkuj": set_alarm(1.5, 0.0, "policzkuje", wykonujacy);
                    break;
        case "poca^luj": set_alarm(1.5,0.0, "caluje",wykonujacy);
                    break;
        case "przytul": set_alarm(1.5,0.0, "malolepszy",wykonujacy);
                    break;
        case "poglaszcz": set_alarm(0.5, 0.0, "malolepszy", wykonujacy);
                    break;
        case "poklep": set_alarm(0.5, 0.0, "malolepszy", wykonujacy);
                    break;
        case "usmiech": set_alarm(0.5, 0.0, "malolepszy", wykonujacy);
                    break;
        case "mrugnij": set_alarm(0.5, 0.0, "malolepszy", wykonujacy);
                    break;
    }
}

void
kopie(object wykonujacy)
{
    switch(random(4))
    {
        case 0: command("'Przesta^n!");break;
        case 1: command("'Bo zawo^lam stra^z!");break;
        case 2: command("'Ej!");break;
        case 3: command("zdziw sie");break;
    }
}

void
policzkuje(object wykonujacy)
{
    switch(random(3))
    {
        case 0: command("syknij cicho");break;
        case 1: command("emote pociera policzek");break;
        case 2: command("'Za co to?");break;
    }
}

void
caluje(object wykonujacy)
{
    if(wykonujacy->query_gender() == 1)
    {
        switch(random(2))
        {
            case 0: command("'Jestem ju^z stary i brzydki.");break;
            case 1: command("westchnij ciezko");break;
        }
    }
    else
    {
        switch(random(2))
        {
            case 0: command("emote wyciera usta.");break;
            case 1: command("spojrzyj z obrzydzeniem na "+
                OB_NAME(wykonujacy));break;
        }
    }
}

void
malolepszy(object wykonujacy)
{
    switch(random(1))
    {
        case 0: command("usmiechnij sie blado");break;
        case 1: command("usmiechnij sie lekko do "+OB_NAME(wykonujacy));break;
        case 2: command("pokiwaj w zadumie");break;
        case 3: command("'To mi^lo z twojej strony, ale starczy ju^z.");break;
        case 4: command("wzrusz ramionami");break;
    }
}

void
shop_hook_niczego_nie_skupujemy()
{
    command("powiedz Ale� ja niczego nie skupuj�! Ja tylko go��bie "+
        "pocztowe sprzedaj�!");
}

void
shop_hook_list_empty_store(string str)
{
    command("powiedz Obecnie nie mam ju� �adnych go��bi na sprzeda�.");
}

void
init()
{
    ::init();
    init_sklepikarz();
    add_action(zaplac,"zap�a�");
}
/*
void
attacked_by(object wrog)
{
    if(walka==0)
    {
        set_alarm(10.0, 0.0, "czy_walka", 1);
        walka = 1;
        return ::attacked_by(wrog);
    }
    else
    {
        walka = 1;
        return ::attacked_by(wrog);
    }
}

int
czy_walka()
{
    if(!query_attack())
    {
        command("powiedz Po walce.");
        walka = 0;
        return 1;
    }
    else
    {
       set_alarm(5.0, 0.0, "czy_walka", 1);
    }

}*/