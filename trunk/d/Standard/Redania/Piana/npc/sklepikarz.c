
/* Autor: Avard
   Opis : Samaia
   Data : 3.11.07 */

#pragma unique

#include <std.h>
#include <ss_types.h>
#include <macros.h>
#include <stdproperties.h>
#include <pl.h>
#include <money.h>
#include <object_types.h>
#include "dir.h"

inherit PIANA_STD_NPC;
inherit "/lib/sklepikarz.c";


//int czy_walka();
//int walka = 0;

void
create_piana_humanoid()
{
    ustaw_odmiane_rasy("m^e^zczyzna");
    dodaj_nazwy("mieszczanin");
    dodaj_nazwy("pracownik");
    dodaj_nazwy("sklepikarz");
    set_gender(G_MALE);
    dodaj_przym("jasnow^losy","jasnow^losi");
    dodaj_przym("chudy","chudzi");
    set_spolecznosc("Piana");

     ustaw_imie(({"codogan","codogana","codoganowi","codogana","codoganem",
        "codoganie"}));

    set_long("Pospolite rysy twarzy, skromny ubi^or i nieco zgarbiona "+
        "sylwetka niemal przemawiaj^a za tym, i^z ten m^e^zczyzna jest "+
        "najprawdziwszym mieszka^ncem Piany. Spracowane d^lonie, "+
        "podkr^a^zone oczy, a mimo to wci^a^z jasne i pogodne spojrzenie, "+
        "oraz specyficzna wo^n ryb dowodz^a, i^z poza tym sklepem zajmuje "+
        "si^e te^z zapewne rybo^l^owstwem. Jego jasne roztrzepane w^losy "+
        "przys^laniaj^a ca^lkowicie brwi, tym samym pozbawiaj^ac twarz "+
        "reszt^e wyrazu, a usta znikaj^a zupe^lnie pod zaniedbanym "+
        "zarostem. ");

    set_stats (({ 25, 60, 40, 70, 70 }));
    set_skill(SS_UNARM_COMBAT, 50);

    add_prop(CONT_I_WEIGHT, 65000);
    add_prop(CONT_I_HEIGHT, 170);
    add_prop(NPC_I_NO_RUN_AWAY, 1);

    set_act_time(60);
    add_act("'Czym mog^e s^lu^zy^c, khe?");
    add_act("emote kaszle przerywanie.");
    add_act("emote otrzepuje si^e z jasno^z^o^ltej sier^sci.");
    add_act("emote spogl^ada gdzie^s, pocieraj^ac z namys^lem czo^lo.");

    set_cact_time(10);
    add_cact("krzyknij Stra^z, pomocy!");
    add_cact("krzyknij Cholera jasna! Stra^z! Bij^a!");
    add_cact("spanikuj");

    set_default_answer(VBFC_ME("default_answer"));

    add_armour(PIANA_UBRANIA + "buty/zwykle_brazowe_Lc");
    add_armour(PIANA_UBRANIA + "koszule/czarna_wiazana_XLc");
    add_armour(PIANA_UBRANIA + "spodnie/skromne_czarne_Sc");

    add_ask(({"prac^e","zadani^e","zlecenie","pomoc"}),
        VBFC_ME("pyt_o_prace"));
    add_ask(({"asortyment","ofert^e","wyb^or"}),
        VBFC_ME("pyt_o_rzeczy"));
    add_ask(({"zwierz^e","kota","psa","misk^e","sier^s^c","zwierz^atko"}),
        VBFC_ME("pyt_o_zwierze"));

    config_default_sklepikarz();

    set_money_greed_buy(220);
    set_money_greed_sell(680);

    set_co_skupujemy(O_UBRANIA|O_BUTY|O_KUCHENNE|O_LINY|O_LAMPY|O_POCHODNIE|
    				O_ZBROJE|O_BRON_MIECZE|O_BRON_MIECZE_2H|O_BRON_SZABLE|
    				O_BRON_SZTYLETY|O_BRON_TOPORY|O_BRON_TOPORY_2H|
    				O_BRON_DRZEWCOWE_D|O_BRON_DRZEWCOWE_K|O_BRON_MACZUGI|O_BRON_MLOTY|O_INNE);


    set_store_room(PIANA_LOKACJE + "magazyn_sklepowy.c");
}

void
powiedz_gdy_jest(object player, string tekst)
{
    if (environment(this_object()) == environment(player))
        this_object()->command(tekst);
}
string
default_answer()
{
    if(!query_attack())
    {
        set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
            "wzrusz ramionami");
    }
    else
    {
        set_alarm(0.5,0.0,"powiedz_gdy_jest", TP, "krzyknij Zostaw mnie!");
    }
    return "";
}

string
pyt_o_prace()
{
    if(!query_attack())
    {
        set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
            "'Dzi^ekuj^e, dzi^ekuj^e.");
        set_alarm(1.5, 0.0, "powiedz_gdy_jest", TP,
            "pokiwaj wdziecznie");
    }
    else
    {
        set_alarm(0.5,0.0,"powiedz_gdy_jest", TP, "'Zostaw mnie!");
    }
    return "";
}

string
pyt_o_rzeczy()
{
    if(!query_attack())
    {
        set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
            "emote wskazuje rega^ly.");
        set_alarm(1.5,0.0,"powiedz_gdy_jest", TP,
            "'Prosz^e przejrze^c, ^smia^lo.");
    }
    else
    {
        set_alarm(0.5,0.0,"powiedz_gdy_jest", TP, "'Zostaw mnie!");
    }
    return "";
}

string
pyt_o_zwierze()
{
    if(!query_attack())
    {
        set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
            "hmm");
        set_alarm(1.5,0.0,"powiedz_gdy_jest", TP,
            "'Taki ma�y, bury kot... uciek� gdzie�.");
        set_alarm(2.5,0.0,"powiedz_gdy_jest", TP,
            "':dodaj�c: ale wr�ci. Zawsze wraca.");
        set_alarm(3.0,0.0,"powiedz_gdy_jest", TP,
            "pokiwaj pewnie");
    }
    else
    {
        set_alarm(0.5,0.0,"powiedz_gdy_jest", TP, "'Zostaw mnie!");
    }
    return "";
}

void
add_introduced(string imie_mia, string imie_bie)
{
    set_alarm(2.0, 0.0, "return_introduce", find_player(imie_mia));
}
void
return_introduce(object ob)
{
    command("przedstaw sie");
}

void
emote_hook(string emote, object wykonujacy)
{
    switch (emote)
    {
        case "kopnij": set_alarm(1.5, 0.0, "kopie", wykonujacy);
                    break;
        case "opluj" : set_alarm(2.5, 0.0, "kopie", wykonujacy);
                    break;
        case "spoliczkuj": set_alarm(1.5, 0.0, "kopie", wykonujacy);
                    break;
        case "poca^luj": set_alarm(1.5,0.0, "caluje",wykonujacy);
                    break;
        case "przytul": set_alarm(1.5,0.0, "malolepszy",wykonujacy);
                    break;
        case "poglaszcz": set_alarm(0.5, 0.0, "malolepszy", wykonujacy);
                    break;
        case "poklep": set_alarm(0.5, 0.0, "malolepszy", wykonujacy);
                    break;
        case "usmiech": set_alarm(0.5, 0.0, "malolepszy", wykonujacy);
                    break;
        case "mrugnij": set_alarm(0.5, 0.0, "malolepszy", wykonujacy);
                    break;
    }
}

void
kopie(object wykonujacy)
{
    switch(random(5))
    {
        case 0: command("'Wypraszam sobie! Prosz� st�d wyj��!");break;
        case 1: command("'Wypraszam sobie!");break;
        case 2: command("spojrzyj zlowrogo na "+OB_NAME(wykonujacy));break;
        case 3: command("zmarszcz brwi gniewnie");break;
        case 4: command("wskaz drzwi");break;
    }
}


void
caluje(object wykonujacy)
{
    switch(random(2))
    {
        case 0: command("wzdrygnij sie");break;
        case 1: command("emote wyciera okradkiem usta.");break;
    }
}

void
malolepszy(object wykonujacy)
{
    switch(random(2))
    {
        case 0: command("usmiechnij sie z zaklopotaniem");break;
        case 1: command("'Czym mog� s�u�y�?");break;
    }
}

void
shop_hook_list_empty_store(string str)
{
    command("powiedz Wszystko wykupili!");
}

void
init()
{
    ::init();
    init_sklepikarz();
}
/*
void
attacked_by(object wrog)
{
    if(walka==0)
    {
        set_alarm(10.0, 0.0, "czy_walka", 1);
        walka = 1;
        return ::attacked_by(wrog);
    }
    else
    {
        walka = 1;
        return ::attacked_by(wrog);
    }
}

int
czy_walka()
{
    if(!query_attack())
    {
        command("powiedz Po walce.");
        walka = 0;
        return 1;
    }
    else
    {
       set_alarm(5.0, 0.0, "czy_walka", 1);
    }

}*/