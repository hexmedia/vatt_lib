
/* Autor: Avard
   Opis : Samaia
   Data : 14.09.07 */

#pragma unique

#include "dir.h"
#include <std.h>
#include <ss_types.h>
#include <macros.h>
#include <stdproperties.h>
#include <pl.h>
#include <money.h>

inherit PIANA_STD_NPC;
inherit "/lib/trener.c"; //a tak, garnizonowy trenuje drwalstwo! :P jak to na pakera ;) V.

#define MIASTO "z Piany"
#define BRON PIANA_BRONIE + "miecze/obosieczny_krotki.c"
#define ZBRO PIANA_ZBROJE + "przeszywanice/pikowana_brazowa_Mc.c"
#define BUTY PIANA_UBRANIA + "buty/czarne_zolnierskie_Mc.c"
#define SPOD PIANA_UBRANIA + "spodnie/grube_lniane_Mc.c"

//int czy_walka();
//int walka = 0;

void
create_piana_humanoid()
{
    dodaj_nazwy("stra^znik");
    dodaj_nazwy("oficer");
    ustaw_odmiane_rasy("m^e^zczyzna");
    dodaj_przym("ogromny","ogromni");
    dodaj_przym("czarnow^losy","czarnow^losi");
    set_gender(G_MALE);
    set_spolecznosc("Piana");
    ustaw_imie(({"jorris","jorrisa","jorrisowi","jorrisa","jorrisem",
        "jorrisie"}));

    set_long("Niew^atpliwie wielka krzepa, musku^ly i umi^esnione cia^lo od "+
        "razu zdradzaj^a, ^ze ten m^e^zczyzna nie przez przypadek pracuje w "+
        "garnizonie, a tak^ze, i^z nie brakuje mu do^swiadczenia, o ile "+
        "chodzi o sukcesy wi^a^z^ace si^e z rozwojem fizycznym. Je^sli "+
        "chodzi o sam^a twarz, oblicze kolosa raczej nie przypomina "+
        "wyk^ladowcy akademii w Oxenfurtu - wielki, czerwony nos, wodniste, "+
        "rozbiegane oczka, krzaczaste brwi, a na domiar z^lego z p^o^l "+
        "tuzina blizn i znamion. M^e^zczyzna raz po raz napina musku^ly, "+
        "jak gdyby chc^ac si^e ponownie nacieszy^c ich, w jego mniemaniu, "+
        "zniewalaj^acym widokiem. \n");

    set_stats (({ 120, 150, 150, 150, 150 }));
    add_prop(CONT_I_WEIGHT, 85000);
    add_prop(CONT_I_HEIGHT, 185);

    set_act_time(60);
    add_act("rozejrzyj sie bacznie");
    add_act("podrepcz ");
    add_act("emote przeklina cicho. ");
    add_act("emote spluwa siarczy^scie. ");
    add_act("podrap sie w ucho");

    set_cact_time(10);
    add_cact("powiedz Noo, teraz ci poka^z^e!");
    add_cact("warknij groznie");

    add_weapon(BRON);
    add_armour(ZBRO);
    add_armour(BUTY);
    add_armour(SPOD);

    add_ask(({"Pian^e","pian^e"}), VBFC_ME("pyt_o_piane"));
    add_ask(({"imi^e"}), VBFC_ME("pyt_o_imie"));
    add_ask(({"zadanie", "prace"}), VBFC_ME("pyt_o_zadanie"));
    add_ask(({"ratusz", "dom burmistrza", "krawca","karczm^e",
        "rynek","most"}), VBFC_ME("pyt_o_miejsce"));
    set_default_answer(VBFC_ME("default_answer"));

    set_skill(SS_WEP_SWORD,70);
    set_skill(SS_UNARM_COMBAT, 50);
    set_skill(SS_PARRY, 65);
    set_skill(SS_DEFENCE,35);
    set_skill(SS_AWARENESS,67);
    set_skill(SS_BLIND_COMBAT,22);
    
    //jakie� tam eventy trzeba by�o napisa�, bo kt� inny to zrobi. Mam nadziej�, �e b�d� ok. [v]
    dodaj_trening(SS_WOODCUTTING, 11053, 6800, 45, 0, 70, ({"Oficer zapoznaje ci� z tajnikami swojego by�ego fachu.\n",
                                               "Olbrzym t�umaczy ci jak r�ba� siekier�.\n",
                                              "Zdobywasz wiedz� z zakresu �cinania drzew i ociosywania pni.\n",
                                              "Wyrwid�b pr�y swe musku�y wskazuj�c, �e to one s� najwazniejsze w fachu drwala.\n",
                                              "Dowiadujesz si� z kt�rej strony chwyci� siekier�, �eby si� \"nie zachlasta�\".\n",
                                              "Oficer stra�y t�umaczy ci "+
                                              "jak traktowa� le�ne ludy wrogie ludziom oraz �e wcale nie r�ni si� to nazbyt "+
                                              "od klasycznego r�bania siekier�.\n",
                                              "Przy okazji dowiadujesz wiele o pogl�dach stra�nika na temat \"le�nych kurewek\", jak "+
                                              "raczy� nazwa� d�ugouchych.\n",
                                              "Uczysz si� o zagro�eniach wynikaj�cych z siedzenia na ga��zi, kt�r� sam pi�ujesz.\n"
                                                        }));
    add_ask(({"treningi","trening","nauki","udzielenie treningu"}), VBFC_ME("pyt_trening"));
}

void
powiedz_gdy_jest(object player, string tekst)
{
    if (environment(this_object()) == environment(player))
    this_object()->command(tekst);
}


string
pyt_trening()
{
        set_alarm(1.0, 0.0, "powiedz_gdy_jest", TP, "powiedz do " + OB_NAME(TP) +
        " Hehe, nooo, by�o si� kiedy� drwalem. Co� tam potrafi�...");
        set_alarm(2.5, 0.0, "powiedz_gdy_jest", TP, "powiedz do " + OB_NAME(TP) +
        " Mog� spr�bowa� ciebie czego� nauczy�. Odp�atnie oczywi�cie.");
 
    return "";
}

string
default_answer()
{
    if(!query_attack())
    {
        switch(random(3))
        {
            case 0: set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
                "wzrusz ramionami");break;
            case 1: set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
                "'Nie wiem.");break;
            case 2: set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
                "'Daj mi spok^oj.");break;
        }
    }
    else
    {
        set_alarm(0.5,0.0,"powiedz_gdy_jest", TP, "'Zostaw mnie!");
    }
    return "";
}

string
pyt_o_zadanie()
{
    if(!query_attack())
    {
        set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
            "'A co ja jestem, s^lup og^loszeniowy?");
    }
    else
    {
        set_alarm(0.5,0.0,"powiedz_gdy_jest", TP, "'Zostaw mnie!");
    }
    return "";
}

string
pyt_o_miejsce()
{
    if(!query_attack())
    {
        set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
            "'Dziura tu, znajdziesz.");
        set_alarm(1.5, 0.0, "powiedz_gdy_jest", TP, "splun");
    }
    else
    {
        set_alarm(0.5,0.0,"powiedz_gdy_jest", TP, "'Zostaw mnie!");
    }
    return "";
}

string
pyt_o_piane()
{
    if(!query_attack())
    {
        set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
            "'To tutej jest.");
        set_alarm(1.5, 0.0, "powiedz_gdy_jest", TP, "podrap sie po glowie");
    }
    else
    {
        set_alarm(0.5,0.0,"powiedz_gdy_jest", TP, "'Zostaw mnie!");
    }
    return "";
}

string
pyt_o_imie()
{
    if(!query_attack())
    {
        set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
            "przedstaw sie");
    }
    else
    {
        set_alarm(0.5,0.0,"powiedz_gdy_jest", TP, "'Zostaw mnie!");
    }
    return "";
}

void
add_introduced(string imie_mia, string imie_bie)
{
   set_alarm(2.0, 0.0, "return_introduce", find_player(imie_mia));
}
void
return_introduce(object ob)
{
   command("przedstaw sie "+OB_NAME(ob));
}

void
emote_hook(string emote, object wykonujacy)
{
    switch (emote)
    {
        case "kopnij": set_alarm(0.5, 0.0, "kopie", wykonujacy);
                    break;
        case "spoliczkuj": set_alarm(0.5, 0.0, "kopie", wykonujacy);
                    break;
        case "opluj" : set_alarm(0.5, 0.0, "kopie", wykonujacy);
                    break;
        case "szturchnij" : set_alarm(0.5, 0.0, "nienajlepszy", wykonujacy);
                    break;
        case "poca^luj": set_alarm(0.5,0.0, "caluje",wykonujacy);
                    break;
        case "prychnij" : set_alarm(0.5, 0.0, "nienajlepszy", wykonujacy);
                    break;
        case "przytul": set_alarm(0.5,0.0, "malolepszy",wykonujacy);
                    break;
        case "poglaszcz": set_alarm(0.5, 0.0, "malolepszy", wykonujacy);
                    break;
        case "poklep": set_alarm(0.5, 0.0, "malolepszy", wykonujacy);
                    break;
        case "usmiech": set_alarm(0.5, 0.0, "dobre", wykonujacy);
                    break;
        case "mrugnij": set_alarm(0.5, 0.0, "dobre", wykonujacy);
                    break;
        case "oczko": set_alarm(0.5, 0.0, "dobre", wykonujacy);
                    break;
        case "polaskocz": set_alarm(0.5, 0.0, "dobre", wykonujacy);
                    break;
    }
}
void
nienajlepszy(object wykonujacy)
{
    switch(random(2))
    {
        case 0: command("'Bo zaraz sobie po^za^lujesz tego!");break;
        case 1: command("spojrzyj z odraza na "+OB_NAME(wykonujacy));break;
    }
}
void
kopie(object wykonujacy)
{
    switch(random(1))
    {
        case 0: command("'Nie zmuszaj mnie...");break;
    }
}
void
malolepszy(object wykonujacy)
{
    switch(random(1))
    {
        case 0: command("'Wygl^adam ci ja na psa?");break;
    }
}
void
dobre(object wykonujacy)
{
    switch(random(1))
    {
        case 0: command("emote mamrocze co^s pod nosem.");break;
        case 1: command("spojrzyj z antypatia na "+OB_NAME(wykonujacy));break;
    }
}

void
caluje(object wykonujacy)
{
    switch(random(1))
    {
        case 0: command("'A won!");break;
        case 1: command("warknij na "+OB_NAME(wykonujacy));break;
    }
}
/*
void
attacked_by(object wrog)
{
    if(walka==0)
    {
        set_alarm(10.0, 0.0, "czy_walka", 1);
        walka = 1;
        return ::attacked_by(wrog);
    }
    else
    {
        walka = 1;
        return ::attacked_by(wrog);
    }
}

int
czy_walka()
{
    if(!query_attack())
    {
        command("zadrzyj z przera^zenia.");
        walka = 0;
        return 1;
    }
    else
    {
       set_alarm(5.0, 0.0, "czy_walka", 1);
    }

}*/