inherit "/std/object";

#include <colors.h>
#include <stdproperties.h>
#include <macros.h>

#define ZROBILI "/d/Standard/questy/utopce/zrobili"
#define ROBIACY "/d/Standard/questy/utopce/robiacy"
#define MEDALION "/d/Standard/Redania/Piana/quest/medalion"
#define WZIELI "/d/Standard/questy/utopce/wzieli"
#define WZIELI_LIST "/d/Standard/questy/utopce/wzielil"
#define LIST "/d/Standard/Redania/Piana/quest/list"

void
create_object()
{
    ustaw_nazwe(({"kupa starych pognitych ubra^n",
                  "kupy starych pognitych ubra^n",
                  "kupie starych pognitych ubra^n",
                  "kup^e starych pognitych ubra^n",
                  "kup^a starych pognitych ubra^n",
                  "kupie starych pognitych ubra^n"}), PL_ZENSKI);
                  
    dodaj_nazwy(({"kupa starych ubra^n",
                  "kupy starych ubra^n",
                  "kupie starych ubra^n",
                  "kup^e starych ubra^n",
                  "kup^a starych ubra^n",
                  "kupie starych ubra^n"}), PL_ZENSKI);
                  
    dodaj_nazwy(({"kupa pognitych ubra^n",
                  "kupy pognitych ubra^n",
                  "kupie pognitych ubra^n",
                  "kup^e pognitych ubra^n",
                  "kup^a pognitych ubra^n",
                  "kupie pognitych ubra^n"}), PL_ZENSKI);
                  
    dodaj_nazwy(({"kupa ubra^n",
                  "kupy ubra^n",
                  "kupie ubra^n",
                  "kup^e ubra^n",
                  "kup^a ubra^n",
                  "kupie ubra^n"}), PL_ZENSKI);

    set_long("Le^z^aca od dawna w wilgoci kupa zniszczonych ubra^n ju^z dawno zacz^e^la " +
    "gni^c. Tu i ^owdzie na podartym materiale nadal mo^zna odnale^x^c plamy krwi.\n");

    add_prop(OBJ_I_WEIGHT, 3921);
    add_prop(OBJ_I_VOLUME, 3794);
    add_prop(OBJ_I_VALUE, 0);
    add_prop(OBJ_M_NO_GET, "Ilekro^c pr^obujesz wzi^a^c w r^ece kup^e ubra^n, " +
    "te rozpadaj^a si^e i zn^ow zanurzaj^a w zalegaj^acej tu wodzie.\n");
}

void
search_now(object searcher, string str, int trail)
{
    string *x, *cp;
    
    if (member_array(lower_case(TP->query_name()), explode(read_file(WZIELI_LIST), "#")) == -1)
    {
    TP->catch_msg("W^sr^od przegni^lych resztek tkaniny odnajdujesz stary przemoczony list.\n");
    clone_object(LIST)->move(this_player());
    
    cp = explode(read_file(ROBIACY), "#");
    if (member_array(lower_case(TP->query_name()), cp) != -1)
    {
        x = explode(cp[member_array(lower_case(TP->query_name()), cp) + 1], "");
        if (x[6] != "1")
        {
            x[6] = "1";
            cp[member_array(lower_case(TP->query_name()), cp) + 1] = implode(x, "");
            write_bytes(ROBIACY, 0, implode(cp, "#"));
         // TP->catch_msg(set_color(42) + "***CHECKPOINT 7***\n" + clear_color());
        }
    }
    write_file(WZIELI_LIST, lower_case(TP->query_name()) + "#");
    //TP->catch_msg("panie prezesie, zadanie wykonane\n");
    }
    else
       TP->catch_msg("Nie uda^lo ci si^e znale^x^c niczego interesuj^acego.\n");
}

void
search_object(string str, int trail)
{
    int time, search_alarm;
    object obj;

    time = query_prop(OBJ_I_SEARCH_TIME) + 5;
    if (time < 1)
        search_now(this_player(), str, trail);
    else
    {
        remove_alarm(search_alarm);
        search_alarm = set_alarm(itof(time), 0.0,
                                 &search_now(this_player(), str, trail));
        seteuid(getuid(this_object()));
        obj = clone_object("/std/paralyze");
        obj->set_standard_paralyze(trail ? "przeszukiwa^c "
          + (query_prop(ROOM_I_IS) ? str :
                this_object()->short(this_player(), PL_BIE)) :
            "szuka^c" + (str ? " " + str : ""));
        obj->set_stop_fun("stop_search");
                obj->set_stop_object(this_object());
        obj->set_remove_time(time);
        obj->move(this_player(), 1);
    }
}
