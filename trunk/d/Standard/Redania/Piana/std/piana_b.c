
#include "dir.h"

inherit PIANA_STD;

#include <mudtime.h>
#include <language.h>
#include <stdproperties.h>
#include <macros.h>

void
create_brzeg()
{
}



nomask void
create_piana()
{
    set_long("@@dlugi_opis@@");
    set_polmrok_long("@@opis_nocy@@");
    add_sit("na brzegu", "na brzegu", "z brzegu", 0);

	add_prop(ROOM_I_TYPE,ROOM_IN_CITY);
    set_start_place(PIANA_SLEEP_PLACE);

    create_brzeg();

	set_event_time(300.0);

    add_prop(ROOM_I_WSP_Y, WSP_Y_PIANA); //Wsp�rz�dne, do systemu pogodowego.
    add_prop(ROOM_I_WSP_X, WSP_X_PIANA);
}
/*
public void
enter_inv(object ob, object from)
{
    ::enter_inv(ob, from);

    if (interactive(ob) && !ob->query_wiz_level()) {
        ob->set_default_start_location(PIANA_LOKACJE + "");
    }
}*/