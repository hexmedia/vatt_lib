/* Autor: Avard
   Opis : Samaia
   Data : 26.08.07 */

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"
#include <pogoda.h>

inherit STREET_STD;

void create_ulica()
{
    set_short("Rynek");
    add_exit(PIANA_LOKACJE_ULICE + "u02","w",0,1,0);
    add_exit(PIANA_LOKACJE_ULICE + "u04","n",0,1,0);
    add_exit(PIANA_LOKACJE_ULICE + "u05","s",0,1,0);
    add_exit(PIANA_LOKACJE_ULICE + "u07","e",0,1,0);

    set_event_time(340.0);

    add_prop(ROOM_I_INSIDE,0);
    add_object(PIANA_PRZEDMIOTY + "lampa_uliczna");
    add_object(PIANA_PRZEDMIOTY + "drogowskaz_piana");
    add_object(PIANA_DRZWI + "drzwi_do_poczty");

    add_npc(PIANA_NPC + "kwiaciarka");

    add_item(({"poczt^e","budynek poczty","urz^ad pocztowy",
        "budynek urz^edu pocztowego"}),"Niewielki budynek, ca^lkiem podobny "+
        "do reszty okolicznych dom^ow, r^o^zni si^e w zasadzie jedynie "+
        "wielkim szyldem, kt^ory g^losi, i^z jest to urz^ad pocztowy. "+
        "Wybielone ^sciany, czyste framugi i okna, oraz starannie wypielony "+
        "ogr^odek zach^ecaj^a i zapraszaj^a do odwiedzin, a starannie "+
        "u^lo^zona kostka prowadzi go^scia od chodnika po same drzwi "+
        "poczty. Na niewielkiej werandzie, nad kt^or^a unosi si^e czerwony "+
        "dach oparty na drewnianych kolumnach, ustawiono w^ask^a ^lawk^e, "+
        "za^s tu^z nad ni^a ko^lysze si^e oliwna lampa. Okna zosta^ly "+
        "zas^loni^ete po^z^o^lk^lymi firanami, tote^z ci^e^zko stwierdzi^c, "+
        "co dzieje si^e wewn^atrz budynku. \n");
}

public string
exits_description()
{
    return "Ulica biegnie st^ad na zach^od, p^o^lnoc, wsch^od i po^ludnie, "+
        "znajduje si^e tutaj tak^ze wej^scie do urz^edu pocztowego.\n";
}

string
dlugi_opis()
{
    string str;
    if(jest_dzien())
    {
        str = "Ma^ly rynek, b^ed^acy centrum ca^lej mie^sciny, zdaje si^e "+
            "by^c du^zo obszerniejszy ni^z jest w rzeczywisto^sci, dzi^eki "+
            "licznym kupcom, g^lo^sno zach^ecaj^acym do kupna ich "+
            "towaru, barwnemu t^lumowi klient^ow i przechodni^ow, a tak^ze "+
            "trzodzie czyni^acej og^olny harmider i rozgardiasz. Unosz^ace "+
            "si^e wsz^edzie kurze pierze, a tak^ze smr^od byd^la wzmagaj^a "+
            "efekt totalnego zgie^lku, podobnie jak wozy kupieckie, raz po "+
            "raz toruj^ace sobie drog^e poprzez og^oln^a ludno^s^c. Mimo to "+
            "wszystkie budynki okalaj^ace plac s^a schludne i zadbane, a "+
            "ka^zdy ogr^odek wypiel^egnowany. Kilka stragan^ow, ustawionych "+
            "wzd^lu^z rynku, zosta^lo po brzegi wype^lnionych rybami, "+
            "sprz^etem w^edkarskim, naczyniami, wiklinowymi wyrobami i "+
            "innymi, niekoniecznie przydatnymi rzeczami. Nieco na zach^od "+
            "rynek zwi^e^za si^e, prowadz^ac do domu burmistrza, za^s w "+
            "kierunku wschodnim do karczmy. ";
    }
    else
    {
        str = "Ma^ly rynek, b^ed^acy centrum ca^lej mie^sciny, cho^c za "+
            "dnia powinien zdawa^c si^e by^c du^zo wi^ekszym, dzi^eki "+
            "t^lumom jakie zwykle bywaj^a na g^l^ownych placach, obecnie "+
            "jest ju^z niemal zupe^lnie wyludniony i spokojny. Wszystkie "+
            "budynki okalaj^ace plac s^a schludne i zadbane, a ka^zdy "+
            "ogr^odek wypiel^egnowany. Kilka stragan^ow, ustawionych "+
            "wzd^lu^z rynku, ^swieci pustkami, a wra^zenie opustoszenia "+
            "dope^lniaj^a walaj^ace si^e tu i ^owdzie ^smieci i unoszone "+
            "przez podmuchy wiatru kurze pi^ora. Nieco na zach^od rynek "+
            "zw^e^za si^e, prowadz^ac do domu burmistrza, za^s w kierunku "+
            "wschodnim do karczmy. ";
    }
    str += "\n";
    return str;
}
