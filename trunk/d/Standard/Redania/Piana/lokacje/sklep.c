/* Autor: Avard
   Opis : Samaia
   Data : 29.11.07 */

#include "dir.h"

inherit PIANA_STD;

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <pogoda.h>

void
create_piana()
{
    set_short("Ciasne pomieszczenie");
    add_prop(ROOM_I_INSIDE,1);
    add_object(PIANA_DRZWI + "drzwi_ze_sklepu");
    add_object(PIANA_PRZEDMIOTY + "lada_sklep");
    add_object(PIANA_PRZEDMIOTY + "miska");
    dodaj_rzecz_niewyswietlana("prosta solidna lada");

    add_item(({"szafki","rega^ly","p^o^lki"}),"Zwyk^le, drewniane rega^ly, "+
	 "szafki i p^o^lki - specjalnie ustawione za lad^a tak, by u^lo^zone na "+
	 "nich przedmioty nie kusi^ly potencjalnych z^lodziejaszk^ow. \n");

    dodaj_rzecz_niewyswietlana("niewielka ceramiczna miska");
    add_npc(PIANA_NPC + "sklepikarz");

    add_item(({"lamp^e","lampy"}),"Zwyk^la olejna lampa, na tyle ma^la by "+
    "porusza^c si^e przy mocniejszym przeci^agu, wywo^lanym cho^cby "+
    "otwarciem drzwi. Nie daje zbyt du^zo ^swiat^la, a jej w^at^le, "+
    "^z^o^lte przeb^lyski nie s^a w stanie ca^lkiem o^swietli^c "+
    "pomieszczenia.\n");

}

public string
exits_description()
{
    return "Wyj^scie prowadzi na ulic^e.\n";
}

string
dlugi_opis()
{
    string str;
    str = "Ciasna, zaduszna izba, s^labo o^swietlona, gdy^z jedynie za "+
        "pomoc^a dw^och okien oraz lampy, jest nad wyraz zaba^laganiona. "+
        "Dodatkow^a woln^a przestrze^n odbiera lada, oddzielajaca "+
        "powierzchni^e do u^zytku klient^ow, od tej u^zytkowanej przez "+
        "sprzedawc^e. Na szafkach, rega^lach i p^o^lkach przesypuj^a si^e "+
        "liczne krzemienie, korale, pochodnie, za^s po pod^lodze walaj^a "+
        "si^e butelki oleju, a tak^ze innych podejrzanych cieczy. Pod "+
        "lad^a, dodatkowo, znale^x^c mo^zna niewielk^a, obdrapan^a misk^e, "+
        "zapewne wi^ec w^la^sciciel sklepu nie pracuje tutaj sam, goszcz^ac "+
        "jakie^s zwierz^atko. ";
    str += "\n";
    return str;
}

