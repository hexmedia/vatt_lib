#include <stdproperties.h>
#include <object_types.h>
#include <pl.h>
#include <macros.h>
#include <mudtime.h>
#include "dir.h"
#include "/sys/filter_funs.h"

inherit PIANA_STD;

object piwniczka =  PIANA_LOKACJE + "piwniczka.c";
object chalupa =  PIANA_LOKACJE + "chalupa.c";
int test_dach();
int w_otwor(string str);
int podniesione = 0;
object paraliz;
object paraliz2;
int testuj_sile();
int flaga = 0;
void flaga_on();
void flaga_off();
int get_flaga();
void set_podniesione();

create_piana()
{
    set_short("Spalona cha^lupa");
    set_long(
        "Zgliszcza niedu^zej, drewnianej chaty sprawiaj^a do^s^c "+
        "przygn^ebiaj^ace wra^zenie. Dramat rozegra^l si^e tu ju^z dawno "+
        "temu - spomi^edzy nadpalonych desek, tworz^acych niegdy^s ^sciany "+
        "budynku nie^smia^lo zaczynaj^a wyrasta^c rachityczne drzewka. "+
        "Jedynie po^ludniowa ^sciana jakim^s cudem ocala^la i wspiera "+
        "jeszcze niewielki fragment stropu. Ca^lo^s^c ci^e^zko opiera "+
        "si^e na osmolonym, murowanym kominie. Wszystko sprawia wra^zenie, "+
        "jakby mia^lo si^e lada chwila rozsypa^c jak domek z kart, tak^ze "+
        "lepiej zachowa^c dalece posuni^et^a ostro^zno^s^c. W k^acie le^zy "+
        "du^zy, dobrze zachowany fragment dachu, za^s na pod^lodze, "+
        "niegdy^s b^ed^acej zapewne wygodnym klepiskiem teraz walaj^a "+
        "si^e mniej lub bardziej spalone deski, okopcone pozosta^lo^sci "+
        "mebli i przedmiot^ow codziennego u^zytku.\n"
        );
    add_item(
        "komin",
        "Wysoki komin zbudowany z du^zych kamieni - dzi^eki temu opar^l "+
        "si^e sile ognia, kt^ora strawi^la ca^ly budynek. Stoi tu a^z po "+
        "dzi^s dzie^n stanowi^ac podpor^e dla po^ludniowej ^sciany i "+
        "mizernych resztek stropu.\n" 
        );
    add_item(
        ({"desk^e","deski"}),
        "D^luga, prosta deska. Ca^la osmolona, wyra^xnie wida^c ^ze kto^s "+
        "pr^obowa^l j^a spali^c, jednak dzia^lo si^e to dawno temu.\n"
        );
    add_item(
        "fragment dachu",
        "Zaskakuj^aco du^zy, bior^ac pod uwag^e zniszczenia jakich dokona^l "+
        "ogie^n, zachowany praktycznie w ca^lo^sci fragment dachu. Zdaje "+
        "si^e, ^ze nie do^s^c, i^z jest poka^xnych rozmiar^ow to zapewne i "+
        "wa^zy niema^lo. To by t^lumaczy^lo, dlaczego wci^a^z tutaj le^zy, "+
        "niewykorzystany przez zaradnych mieszka^nc^ow Piany.\n"
        );
    add_cmd_item("fragment dachu","podnies",test_dach);

    add_prop(ROOM_I_INSIDE, 1);  
    add_exit(PIANA_LOKACJE_ULICE + "przed_spalona_chalupa", 
        ({({"wyj^scie","ulica"}), "z cha^lupy"}),0,1,1);


}

int
test_dach()
{
    podniesione = 0;
    object *tab = all_inventory();
    object *arr = FILTER_OTHER_LIVE(tab);
    //dump_array(arr);
    int i;
    for(i=0; i<sizeof(arr) ;i++)
    {
        if (arr[i]->query_prop("PODNOSI") == 1)
        {
            
            podniesione = 1;
        }
    }
    if (podniesione == 1)
    {
        write("Juz kto^s podnosi fragment dachu!\n");
    }
    else
    {
        
        paraliz = clone_object(PIANA_LOKACJE + "paraliz.c");
        paraliz->create_paralyze();
        TP->add_prop("PODNOSI", 1);
        paraliz->move(TP);
        write("Chwytasz za kraw^ed^x i z ca^lych si^l starasz si^e unie^s^c fragment "+
        "dachu ku g^orze.\n");
        saybb(QCIMIE(this_player(), PL_MIA) + " chwyta za kraw^ed^x i z ca^lych si^l "+
        "stara si^e unie^s^c fragment dachu ku g^orze.\n");

        
    }
    
    return 1;
}

init()
{
    ::init();
    add_action(w_otwor, "wejdz");
}
int
testuj_sile()
{
   if(TP->query_stat(0) > 60)
   {
       TP->add_prop("_live_s_extra_short", " podnosz^acy fragment dachu");
       write("Unosisz fragment dachu ku g^orze, a twoim oczom ukazuje si^e "+
           "g^l^eboki, ciemny otw^or. Zdaje si^e, ^ze bez wi^ekszych "+
           "problem^ow uda ci si^e przeze^n prze^slizgn^a^c. \n");
       saybb(QCIMIE(this_player(), PL_MIA) +" unosi fragment dachu ku "+
           "g^orze, a twoim oczom ukazuje si^e g^l^eboki, ciemny otw^or. "+
           "Zdaje si^e, ^ze bez wi^ekszych problem^ow uda ci si^e przeze^n "+
           "prze^slizgn^a^c.\n");  
        flaga = 1;
        TP->remove_prop("PIWNICZKA");
        tell_room(piwniczka, "Fragment dachu nagle podnosi sie do gory!\n");
        paraliz2 = clone_object(PIANA_LOKACJE + "paraliz2.c");
        paraliz2->create_paralyze();
        paraliz2->move(TP);
        TP->add_prop("PODNOSI", 1);
   }
   else
    {
        TP->add_prop("PODNOSI", 0);

        write("Fragment dachu jest na tyle ci^e^zki, ^ze nie uda^lo ci si^e go "+
           "ruszy^c nawet na milimetr.\n");
        saybb(QCIMIE(this_player(), PL_MIA) +" puszcza fragment dachu - nie "+
           "uda^lo mu si^e ruszy^c go nawet na milimetr.\n");
    }
   
   return 1;
}

int w_otwor(string str)
{

    if (flaga == 1)
        {
            if (str != "do otworu" && str != "w otw�r" && str != "przez otw�r")
            {

                write("Gdzie chcesz wejsc?\n");

                
            }
            else
            {
                
                if (TP->query_prop("PODNOSI"))
                {
                    paraliz2->remove_paralyze();
                    podniesione = 0;
                    flaga = 0;
                    TP->remove_prop("_live_s_extra_short");
                    piwniczka->remove_exit("otwor");
                    write("Z trudem przeciskasz si^e przez w^aski otw^or. W ostatniej "+
                    "chwili puszczasz fragment dachu, kt^ory z hukiem opada na "+
                    "ziemi^e zas^laniaj^ac wyj^scie. \n");
                
                    saybb(QCIMIE(this_player(), PL_MIA) +" z trudem przeciska si^e przez "+
                    "w^aski otw^or. W momencie gdy ca^lkiem w nim niknie na ziemi^e "+
                    "z hukiem opada trzymany przez niego fragment dachu.\n");
                    TP->add_prop("PODNOSI", 0);
                }    
                else
                {
                    write("Z trudem przeciskasz si^e przez w^aski otw^or.\n");
                    saybb(QCIMIE(this_player(), PL_MIA) +" z trudem przeciska "+
                   "si^e przez otw^or by po chwili ca^lkiem w nim znikn^a^c.\n");
                }
                
                tell_room(piwniczka, QCIMIE(this_player(), PL_MIA)+
                    " wychodzi z otworu.\n"); 
                TP->move(piwniczka);
                TP->do_glance(2);
                
            }
            return 1;
        }
        else
    {
        return 0;
    }
}
void flaga_on()
{
    //write("on.\n");
    flaga = 1;
}
void flaga_off()
{
    //write("off.\n");
    flaga = 0;
}
int get_flaga()
{
    //write("aktualna flaga: "+flaga+".\n");
    return flaga;
}

void set_podniesione()
{
    podniesione = 1;
}
