#include <stdproperties.h>

#include "dir.h"

inherit PIANA_STD;
inherit "/lib/store_support";

#include <stdproperties.h>
#include <macros.h>

#define LAMPA "/d/Standard/items/narzedzia/lampa_niewielka_poreczna"

void create_piana()
{
    set_short("Magazyn sklepikarza Piany");
    set_long("Nie ma st�d wyj�cia. DUPA!\n");

    add_prop(ROOM_I_INSIDE,1);

    add_object("/d/Standard/items/sloik.c", 30);
    add_object(LAMPA,30);
    add_object("/std/torch",40);
    add_object("/std/olej",40);
    add_object("/std/krzemien",40);
    add_object(PIANA_UBRANIA + "karminowa_haftowana_chusta", 30);
    add_object(PIANA_PRZEDMIOTY + "woreczki/niewielki_bezowy", 40);
    add_object(PIANA_UBRANIA + "korale_drewniane_czerwone", 30);

    add_neverending_object("/std/torch");
    add_neverending_object("/std/krzemien");
    add_neverending_object("/std/krzemien");
    add_neverending_object("/d/Standard/items/sloik.c");

    add_neverending_object(PIANA_UBRANIA + "moskitiera.c");

}
void                       /* wywolywane za kazdym razem, jak jakis */
enter_inv(object ob, object skad)  /* obiekt wchodzi do wnetrza tego obiektu */
{                  /* ...czyli do pokoju */

    ::enter_inv(ob, skad); /* trzeba ZAWSZE wywolac gdy redefiniujemy enter_inv */
    
    store_update(ob); /* dba o to, zeby magazynie nie bylo za duzo rzeczy */
}
void
leave_inv(object ob, object to)
{
    //::leave_env(ob,to);
    store_add_item(ob);
}