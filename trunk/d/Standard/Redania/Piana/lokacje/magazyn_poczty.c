#include <stdproperties.h>
#include "dir.h"

inherit PIANA_STD;
inherit "/lib/store_support";
void create_piana()
{
    set_short("Magazyn go^l^ebi");
    set_long("Magazyn go^l^ebi pocztowych.\n");

    add_prop(ROOM_I_INSIDE,1);

    add_object("/std/golab.c", 30);
    add_neverending_object("/std/golab.c");
}
void                       /* wywolywane za kazdym razem, jak jakis */
enter_inv(object ob, object skad)  /* obiekt wchodzi do wnetrza tego obiektu */
{                  /* ...czyli do pokoju */

    ::enter_inv(ob, skad); /* trzeba ZAWSZE wywolac gdy redefiniujemy enter_inv */
    
    store_update(ob); /* dba o to, zeby magazynie nie bylo za duzo rzeczy */
}
void
leave_inv(object ob, object to)
{
    //::leave_env(ob,to);
    store_add_item(ob);
}