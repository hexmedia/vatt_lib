/* Autor: Avard
   Opis : Samaia
   Data : 31.08.07 */

#include "dir.h"

inherit PIANA_STD;

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <pogoda.h>

void
create_piana()
{
    set_short("Gabinet burmistrza");
    add_prop(ROOM_I_INSIDE,1);
    add_object(PIANA_DRZWI + "drzwi_z_burmistrza");
    add_object(PIANA_DRZWI + "drzwi_do_sypialni_burmistrza");
    add_object(PIANA_PRZEDMIOTY + "biurko_burmistrz");
    add_npc(PIANA_NPC + "burmistrz");
    dodaj_rzecz_niewyswietlana("jasne jesionowe biurko");

    add_item(({"zas^lony","kremoe zas^lony"}),
        "Zupe^lnie po^z^o^lk^la zas^lona, miejscami "+
    "zaci^agni^eta i podziurawiona, przys^lania niedbale widok na "+
    "zewn^atrz zwieszaj^ac si^e z drewnianych karniszy, umocowanych "+
    "tu^z nad sufitem. \n");
}

public string
exits_description()
{
    return "Do dalszej cz^esci domu prowadz^a jasne, "+
        "w^askie drzwi, za^s wyj^scie prowadzi na ulic^e.\n";
}

string
dlugi_opis()
{
    string str;
    str = "To pomieszczenie, b^ed^ace jednocze^snie przedpokojem jak i "+
        "gabinetem burmistrza Piany, zosta^lo urz^adzone w bardzo "+
        "skromnym stylu. Promienie ";
    if(jest_dzien() == 1)
    {
        str += "s^lo^nca ";
    }
    else
    {
        str += "ksi^e^zyca ";
    }
    str += "s^acz^ace si^e przez kremowe zas^lony tworz^a przytulny, "+
        "sielankowy nastr^oj formuj^ac na bia^lej ^scianie dziwne wzory "+
        "z plam swiat^la. ";
    if(jest_rzecz_w_sublokacji(0,"jasne jesionowe biurko"))
    {
        str += "Stoj^ace naprzeciw drzwi frontowych jasne, jesionowe "+
            "biurko po brzegi zosta^lo wype^lnione arkuszami papieru, "+
            "naczyniami, ka^lamarzami i innymi nieca^lkiem potrzebnymi "+
            "do pracy rzeczami. ";
    }
    if(jest_rzecz_w_sublokacji(0,"wielka jesionowa szafa"))
    {
        str += "Zachodnia ^sciana zosta^la zakryta wielk^a szaf^a.";
    }
    str += "\n";
    return str;
}
