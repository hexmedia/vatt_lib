/* Autor: Avard
   Opis : Samaia
   Data : 21.09.07 */

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"
#include <pogoda.h>

inherit STREET_STD;

void create_ulica()
{
    set_short("Ulica");
    add_exit(PIANA_LOKACJE_ULICE + "u08","e",0,1,0);
    add_exit(PIANA_LOKACJE + "rynek","w",0,1,0);
    add_exit(PIANA_KARCZMA+ "lokacje/podworze","n",0,1,0);

    add_prop(ROOM_I_INSIDE,0);
    add_object(PIANA_PRZEDMIOTY + "lampa_uliczna");
}

public string
exits_description()
{
    return "Ulica ci^agnie si^e ze wschodu na zach^od, w stron^e rynku, "+
        "za^s na p^o^lnocy znajduje si^e podw^orze karczmy.\n";
}

string
dlugi_opis()
{
    string str;
    str = "Brukowana ulica, na tyle szeroka by spokojnie mog^ly si^e na "+
        "niej wymija^c liczne wozy kupieckie, si^ega od rozleg^lego rynku "+
        "na zachodzie, po daleki wsch^od, zmieniaj^acy si^e z czasem w "+
        "trakt posr^od ^l^ak. Po okolicy";
    if(jest_dzien() == 0)
    {
        str += ", nawet mimo p^o^xnej pory,";
    }
    str += " p^eta si^e beztrosko sporo mieszka^nc^ow Piany, handlarzy i "+
        "podr^o^znik^ow, ze wzgl^edu na usytuowan^a nieopodal, na "+
        "p^o^lnocy, karczm^e. Pozosta^le budynki ci^agn^ace si^e wzd^lu^z "+
        "drogi, g^l^ownie domy mieszkalne, s^a utrzymane estetycznie, przed "+
        "ka^zdym za^s znajduj^a si^e rabatki i ogr^odki warzywne, "+
        "odgrodzone od przechodni^ow d^lugim, drewnianym p^lotem, "+
        "sporadycznie przerywany prowizorycznymi furtkami. Nieliczne "+
        "wyrwy w bruku, b^ed^ace wynikiem ruchu i zamieszania, wype^lni^ly "+
        "si^e ";
    if(pora_roku() == MT_WIOSNA)
    {
        str += "wod^a, tworz^acym niewielkie ka^lu^ze";
    }
    if(pora_roku() == MT_LATO)
    {
        str += "b^lotem i przywianym sk^ad^s piachem";
    }
    if(pora_roku() == MT_JESIEN)
    {
        str += "po brzegi b^lotem";
    }
    if(pora_roku() == MT_ZIMA)
    {
        str += "brudnym ^sniegiem";
    }
    str += ". \n";
    return str;
}
