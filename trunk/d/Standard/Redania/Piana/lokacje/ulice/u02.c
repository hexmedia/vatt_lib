/* Autor: Avard
   Opis : Samaia
   Data : 8.09.07 */

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <sit.h>
#include "dir.h"
#include <pogoda.h>

inherit STREET_STD;

void odnow_golebie();


int
wespnij(string str)
{
    object *drzef;

    if(query_verb() == "wespnij")
        notify_fail("Wespnij si� gdzie?\n");
    else
        notify_fail("Wejd� gdzie?\n");

    if(!strlen(str))
       return 0;

    if(!HAS_FREE_HANDS(this_player()))
    {
        write("Musisz mie� obie d�onie wolne, aby m�c to zrobi�.\n");
        return 1;
    }
    if(TP->query_prop(SIT_SIEDZACY))
    {
        write("Czujesz, �e wspinaczka na siedz�co nie jest twoj� domen�.\n");
        return 1;
    }
    if(TP->query_prop(SIT_LEZACY))
    {
        write("Czujesz, �e wspinaczka na le��co nie jest twoj� domen�.\n");
        return 1;
    }
    if(TP->query_old_fatigue() < 50)
    {
       write("Nie masz ju� na to si�y.\n");
       return 1;
    }

    if(TP->move(PIANA_LOKACJE+"nadrzewie", 0, 0, 1) == 8)
    {
        write("Wygl�da na to, �e ju� si� tam nie zmie�cisz.\n");
        return 1;
    }

    if(!parse_command(str, environment(TP), "'si^e' 'na' 'klon' / 'drzewo' ") &&
        !parse_command(str, environment(TP), "'na' 'klon' / 'drzewo' "))
        return 0;


    TP->set_old_fatigue(TP->query_old_fatigue()-(random(20)+10 ));
    write("Zaczynasz wspina� si� na drzewo.\n");
    saybb(QCIMIE(this_player(), PL_MIA) + " zaczyna wspina� si� na drzewo.\n");

	if(this_player()->query_skill(SS_CLIMB) <= 43)
	{
        clone_object(PIANA_PRZEDMIOTY+"paraliz_wspina_sie_niedarady")->move(this_player());
	return 1;
	}
	if(this_player()->query_skill(SS_CLIMB) >= 44 &&
	   this_player()->query_skill(SS_CLIMB) <= 66)
	{
	clone_object(PIANA_PRZEDMIOTY+"paraliz_wspina_sie_prawie")->move(this_player());
	return 1;
	}

    clone_object(PIANA_PRZEDMIOTY+"paraliz_wspina_sie")->move(this_player());
    return 1;
}

string drzefko(string str)
{
    object *kto;

     str="�w stary, roz�o�ysty klon posiada gruby, sp�kany "+
        "ju� nieco pien. A ga��zie z niego wyrastaj�ce bij� "+
        "ku niebu nieco ponad okoliczne budowle. ";

    if(jest_rzecz_w_sublokacji(0,"drewniana zdobiona ^lawka"))
        str+="Pod jego ga��ziami skrywa si� zdobiona �awka. ";



    kto = FILTER_LIVE(all_inventory(find_object(PIANA_LOKACJE+"nadrzewie")));

    if (sizeof(kto) == 1)
	str+="Na ga^l^ezi siedzi " + COMPOSITE_LIVE(kto, PL_MIA) + ".";
    if (sizeof(kto) > 1)
	str+="Na ga^l^ezi siedz^a " + COMPOSITE_LIVE(kto, PL_MIA) + ".";

   str+="\n";
   return str;
}


void create_ulica()
{
    set_short("Ulica");
    add_exit(PIANA_LOKACJE_ULICE + "u01","w",0,1,0);
    add_exit(PIANA_LOKACJE + "rynek","e",0,1,0);

    add_object(PIANA_DRZWI + "drzwi_do_burmistrza");

    add_prop(ROOM_I_INSIDE,0);
    add_object(PIANA_PRZEDMIOTY + "lampa_uliczna");
    add_object(PIANA_PRZEDMIOTY + "lawka_drewniana_zdobiona");
    dodaj_rzecz_niewyswietlana("drewniana zdobiona ^lawka");

    add_npc(PIANA_NPC + "mieszczanin");
    add_item(({"drzewo","klon","roz�o�ysty klon"}), "@@drzefko@@");
    add_item(({"dom","dom burmistrza"}),"Na pierwszy rzut oka wida^c, "+
        "i^z ten dom pe^lni tak^ze inn^a fukncj^e poza mieszkaln^a, ze "+
        "wzgl^edu na jego postawne rozmiary, wysokie, drewniane ogrodzenie, "+
        "dodatkowo okolone r^o^zanymi klombami, oraz szerokie, ci^e^zkie "+
        "drzwi broni^ace dost^epu do wewn^atrz nieproszonym go^sciom. Trzy "+
        "okna widoczne na fronowej ^scianie, dwa po obu stronach drzwi, a "+
        "tak^ze jedno na pierwszym pi^etrze, zosta^ly zas^loni^ete od "+
        "^srodka kremowymi zas^lonami. Czyste ^sciany domu s^a starannie "+
        "pomalowane ^snie^znobia^l^a, w przeciwie^nstwie do innych dom^ow, "+
        "nie odpryskuj^ac^a farb^a. Ca^ly budynek sprawia wra^zenie "+
        "zadbanego i go^scinnego.\n");

    add_item(({"skalniak","skalniaczek"}),"@@skalniak@@");

    odnow_golebie();
    set_alarm_every_hour(&odnow_golebie());
}

public string
exits_description()
{
    return "Ulica ci^agnie si^e z zachodu na wsch^od, w stron^e rynku, "+
        "za^s na p^o^lnocy dostrzec mo^zna du^zy dom.\n";
}

string
dlugi_opis()
{
    string str;
    str = "Brukowana uliczka, zmieniaj^aca si^e tutaj w ma^ly placyk, otacza "+
        "p^o^lkolem ma^ly skalniak ";
    if(pora_roku() == MT_WIOSNA)
    {
        str += "soczysto^z^o^ltych ^zonkili i tulipan^ow. ";
    }
    if(pora_roku() == MT_LATO)
    {
        str += "zaro^sni^ety wszelkiego rodzaju zielskiem, od pokrzyw po "+
            "dzikie r^o^ze. ";
    }
    if(pora_roku() == MT_JESIEN)
    {
        str += "pe^len fioletowych fio^lk^ow. ";
    }
    if(pora_roku() == MT_ZIMA)
    {
        str += "przykryty czap^a bia^lego ^sniegu. ";
    }

    if(jest_rzecz_w_sublokacji(0,"drewniana zdobiona ^lawka"))
    {
        str += "Tu^z obok stoi drewniana zdobiona ^lawka, skrywaj^aca si^e "+
            "w cieniu roz^lo^zystego klonu. ";
    }
    else
    {
        str += "Tu^z obok wyrasta poka^xny roz^lo^zysty klon. ";
    }

    str += "Braki w bruku i koleiny po jego obu stronach ^swiadcz^a o tym, "+
        "i^z to miejsce raczej nie nale^zy do spokojnych, ma^lo ruchliwych "+
        "miejsc^owek, w kt^orych to mo^zna nie spotka^c przez ca^ly dzie^n "+
        "^zywej duszy. ";
    if(jest_dzien() == 1)
    {
        str += "Wr^ecz przeciwnie. Kupcy pod^a^zaj^acy na wsch^od, w "+
            "kierunku rynku, a tak^ze zwykli mieszkacy Piany zwykle mijaj^a "+
            "to miejsce w po^spiechu pod^a^zaj^ac do pracy. ";
    }
    if(present("go^l^ab", TO))
    {
        str += "Dodatkowego zamieszania czyni^a gruchaj^ace, wszelkiej "+
            "ma^sci go^l^ebie zawzi^ecie wydziobuj^ace co^s z bruku i "+
            "dreptaj^ace tu i ^owdzie w poszukiwaniu pokarmu. ";
    }
    str += "Zdaje si^e, ^ze ca^ly plac nie znajduje si^e tu przypadkiem, "+
        "zapewne zosta^l wybudowany w^la^snie w tym miejscu ze wzgl^edu "+
        "na dom burmistrza stoj^acy w pobli^zu. ";
    str += "\n";
    return str;
}
void odnow_golebie()
{
  if(this_object()->jest_dzien() && !present("go^l�b", this_object()))
    {
      int n = random(4)+2;
      for(int i = 0; i < n; i++)
        clone_object(PIANA_NPC+"golab_plac.c")->move(TO);
    }
}

string
skalniak()
{
    string str;
    if(pora_roku() == MT_WIOSNA)
    {
        str = "Skalniak usypany z kamieni i niewielkich g^laz^ow, otoczony "+
            "swoistym, niskim p^lotkiem, ^z^o^lci si^e pi^eknie kwiatami "+
            "tulipan^ow i ^zonkili, przechodz^ac w soczyst^a czerwie^n "+
            "dzi^eki dzwoneczkom szachownicy i wyr^o^zniaj^ac si^e "+
            "gdzieniegdzie ^snie^zn^a biel^a delikatnych irys^ow. Absolutny "+
            "brak chwast^ow i czarna, ^zyzna gleba ^swiadcz^a o tym, i^z "+
            "kto^s troskliwie opiekuje si^e owym skalniakiem, czyni^ac go "+
            "tym samym tak barwnym i ^zywym. ";
    }
    if(pora_roku() == MT_LATO)
    {
        str = "Skalniak usypany z kamieni i niewielkich g^laz^ow, otoczony "+
            "swoistym, niskim p^lotkiem, niemal sp^lywa kaskad^a barw "+
            "licznych kwiat^ow, od soczystych lilii, z^locieni, poprzez "+
            "delikatne pos^lonki i wielokolorowe dzwonki, po wszelkiego "+
            "rodzaju odmiany i krzy^z^owki kwiat^ow, nierzadko "+
            "zaskakuj^acych swym oryginalym wygl^adem. Absolutny brak "+
            "chwast^ow i czarna, ^zyzna gleba ^swiadcz^a o tym, i^z kto^s "+
            "troskliwie opiekuje si^e owym skalniakiem, czyni^ac go tym "+
            "samym tak barwnym i ^zywym. ";
    }
    if(pora_roku() == MT_JESIEN)
    {
        str = "Skalniak usypany z kamieni i niewielkich g^laz^ow, otoczony "+
            "swoistym, niskim p^lotkiem, przykry^l si^e purpurowym dywanem "+
            "rozchodnika, przetykanym gdzieniegdzie k^epami r^o^zowych "+
            "zawilc^ow. Wolne przestrzenie wype^lniaj^a ozdobne trawy "+
            "uginaj^ace si^e lekko na wietrze i potrz^asaj^ace swymi "+
            "k^loskami. Absolutny brak chwast^ow i czarna, ^zyzna gleba "+
            "^swiadcz^a o tym, i^z kto^s troskliwie opiekuje si^e owym "+
            "skalniakiem, czyni^ac go tym samym tak barwnym i ^zywym. ";
    }
    if(pora_roku() == MT_ZIMA)
    {
        str = "Skalniak usypany z kamieni i niewielkich g^laz^ow, otoczony "+
            "swoistym, niskim p^lotkiem, pokry^l si^eiemal ca^lkiem bia^lym "+
            "^sniegiem, odbijaj^acym ";
        if(jest_dzien() == 1)
        {
            str += "s^loneczne ";
        }
        else
        {
            str += "ksi^ezycowe ";
        }
        str += "promienie, mieni^ac si^e przy tym i skrz^ac drobnymi "+
            "kryszta^lkami. W niekt^orych miejscach wybijaj^a si^e ku "+
            "g^orze ciemne, zmartwia^le ga^l^azki i p^edy ro^slin, jak "+
            "gdyby w oczekiwaniu na nadchodz^ac^a wiosn^e. ";
    }
    str += "\n";
    return str;
}

public void
init()
{
    ::init();
    add_action(wespnij,"wespnij");
    add_action(wespnij,"wejd�");
}
