/* Autor: Avard
   Opis : Samaia
   Data : 21.09.07 */

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"
#include <pogoda.h>

inherit STREET_STD;


void create_ulica()
{
    set_short("Ulica");
    add_exit(PIANA_LOKACJE_ULICE + "u07","w",0,1,0);
    add_exit(PIANA_LOKACJE_ULICE + "przed_spalona_chalupa","e",0,1,0);
    

    add_prop(ROOM_I_INSIDE,0);
    add_object(PIANA_PRZEDMIOTY + "lampa_uliczna");
    add_object(PIANA_DRZWI + "drzwi_do_sklepu");

    add_npc(PIANA_NPC + "kot");

    add_item(({"cha^lupk^e","sklep","skromny sklep","sklepik"}),"Skromn^a "+
        "cha^lupk^e przykryt^a strzech^a i trzymaj^ac^a si^e zupe^lnie na "+
        "uboczu nie^latwo uto^zsami^c ze sklepem, cho^c na to w^la^snie "+
        "wskazuje spora tabliczka, przybita nad drzwiami. Do nich za^s, od "+
        "ulicy, prowadzi wydeptana ^scie^zka, po czym mo^zna wnioskowa^c, "+
        "i^z mieszka^ncy Piany cz^esto odwiedzaj^a owe miejsce. Dwa "+
        "frontowe okna, robi^ace za skromn^a wystaw^e, eksponuj^a raczej "+
        "ubogi asortyment, w postaci pochodni, s^loik^ow i innych mniej "+
        "lub bardziej przydatnych przedmiot^ow. \n");

    add_item("r^ow","@@rowik@@");
}

public string
exits_description()
{
    return "Ulica ci^agnie si^e ze wschodu na zach^od. Znajduje si^e tutaj "+
        "tak^ze wej^scie do sklepu.\n";
}

string
dlugi_opis()
{
    string str;
    str = "Szeroka, brukowana droga miasta Piany zamienia si^e tutaj w "+
        "zadbany trakt, prowadz^acy na wsch^od wprost do stolicy Redanii "+
        "- Tretogoru. Granica miejscowo^sci nie jest zbyt widoczna, "+
        "g^l^ownie ze wzgl^edu na brak bramy, a tak^ze stopniowo "+
        "przerzedzaj^ace si^e budynki, "+
        "nieco przypominaj^ace baraki, nie okre^slaj^ace jednoznacznie "+
        "kresu terenu zabudowa^n. Na zachodzie maluje si^e Piana niemal "+
        "w ca^lej okaza^lo^sci, prezentuj^ac ju^z z oddali sw^oj "+
        "rynek, czy osobliw^a budowl^e garnizonu. Rynsztok ci^agn^acy si^e "+
        "od miasta zamienia si^e tutaj w zwykly ";
    if(CZY_JEST_SNIEG(this_object()))
    {
        str += "r^ow, pe^len odgarni^et^ego z ulcy z ^sniegu";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str += "obrastaj^acy swie^z^a, wiosenn^a flor^a r^ow";
    }
    else if(pora_roku() == MT_LATO)
    {
        str += "g^esto zaro^sni^ety r^ow";
    }
    else
    {
        str += "zaro^sni^ety, wype^lniony po^z^o^lk^lymi li^scmi r^ow";
    }

    str += ". \n";
    return str;
}

string
rowik()
{
    string str;
    if(CZY_JEST_SNIEG(this_object()))
    {
        str = "Szeroki r^ow biegnie od strony miasta nikn^ac miejscami "+
            "pod grub^a warstw^a ^sniegu, maj^acego w tym miejscu dziwnie "+
            "brunatno-^z^o^lty odcie^n. Ponad jego kraw^edziami unosi sie "+
            "nieprzyjemny, lecz niezbyt natarczywy smr^od koja^z^acy si^e "+
            "z czym^s co od do^s^c dawna nie ^zyje. Gdzieniegdzie wystaj^a "+
            "tylko suche patyki p zesz^lorocznych zio^lach i pokrzywach.";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str = "Szeroki r^ow biegnie od strony miasta nios^ac wszystko co "+
            "wyp^lyn^e^lo z runsztok^ow. M^etna woda z trudem dnajduje "+
            "drog^e pomi^edzy porastaj^acymi dno wiosennymi k^epami traw, "+
            "na kt^orych zatrzymuja si^e odpadki, resztki jedzenia i "+
            "^smieci. Ponad spadzistymi kraw^edziami unosi sie "+
            "nieprzyjemny, lecz niezbyt natarczywy smr^od koja^z^acy "+
            "si^e z czym^s co od do^s^c dawna nie ^zyje.";
    }
    else if(pora_roku() == MT_LATO)
    {
        str = "Szeroki r^ow biegnie od strony miasta nios^ac wszystko "+
            "co wyp^lyn^e^lo z runsztok^ow. M^etna woda z trudem dnajduje "+
            "drog^e pomi^edzy porastaj^acymi dno k^epami traw i wysokimi "+
            "pokrzywami, na kt^orych zatrzymuja si^e odpadki, resztki "+
            "jedzenia i ^smieci. Ponad spadzistymi kraw^edziami unosi sie "+
            "nieprzyjemny, lecz niezbyt natarczywy smr^od koja^z^acy si^e "+
            "z czym^s co od do^s^c dawna nie ^zyje.";
    }
    else
    {
        str = "Szeroki r^ow biegnie od strony miasta nios^ac wszystko co "+
            "wyp^lyn^e^lo z runsztok^ow. M^etna woda z trudem dnajduje "+
            "drog^e pomi^edzy porastaj^acymi dno suchymi k^epami traw i "+
            "stertami li^sci, na kt^orych zatrzymuja si^e odpadki, resztki "+
            "jedzenia i ^smieci. Ponad spadzistymi kraw^edziami unosi sie "+
            "nieprzyjemny, lecz niezbyt natarczywy smr^od koja^z^acy si^e z "+
            "czym^s co od do^s^c dawna nie ^zyje.";
    }
    str += "\n";
    return str;
}
