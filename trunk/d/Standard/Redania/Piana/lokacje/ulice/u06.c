/* Autor:
   Opis :
   Data : */

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"
#include <pogoda.h>

inherit STREET_STD;

void create_ulica()
{
    set_short("Na rozwidleniu dr^og");
    add_exit(PIANA_LOKACJE_ULICE + "u05","n",0,1,0);
    add_exit(PIANA_LOKACJE_ULICE + "u03","nw",0,1,0);
    add_exit(PIANA_LOKACJE_BRZEG + "b03","w",0,1,0);
    add_exit(TRAKT_BIALY_MOST_OKOLICE_LOKACJE + "t51","s",0,3,0);

    add_prop(ROOM_I_INSIDE,0);
    add_object(PIANA_PRZEDMIOTY + "lampa_uliczna");

    add_npc(PIANA_NPC + "dziewczynka");
}

public string
exits_description()
{
    return "Ulicia ci^agnie si^e na p^o^lnoc i p^o^lnocny zach^od, na "+
        "zachodzie znajduje si^e nabrze^ze, za^s na po^ludniu trakt.\n";
}

string
dlugi_opis()
{
    string str;
    str = "W miejscu, w kt^orym aktualnie si^e znajdujesz zbiega si^e "+
        "kilka trakt^ow. Z p^o^lnocy na po^ludnie ci^agnie si^e "+
        "najszerszy z nich, sprawiaj^acy zarazem wra^zenie "+
        "najcz^e^sciej u^zytkowanego. Wygodny go^sciniec, szeroki na "+
        "tyle, by bez wi^ekszych problem^ow mog^ly si^e na nim mija^c "+
        "kupieckie wozy ^l^aczy rozci^agaj^ace si^e na wyci^agni^ecie "+
        "r^eki st^ad niewielkie miasteczko zwane Pian^a z niezbyt "+
        "odleg^lym Bia^lym Mostem. Kamienie, kt^orymi zosta^l przed "+
        "laty wy^lo^zony s^a idealnie wyg^ladzone setkami, wr^ecz "+
        "tysi^acami kopyt i n^og. ";
    if(pora_roku() == MT_WIOSNA || pora_roku() == MT_JESIEN)
    {
        str += "P^lytkie koleiny wy^z^lobione ko^lami ci^e^zkich woz^ow "+
            "wype^lnione s^a ^smierdz^ac^a mieszanin^a wody i ko^nskich "+
            "odchod^ow. Kawa^lek st^ad, na zachodzie, widzisz rw^acy "+
            "nurt niebezpiecznego o tej porze roku Pontaru, p^lyn^acego "+
            "z g^lo^snym pomrukiem na p^o^lnoc. ";
    }
    if(pora_roku() == MT_LATO)
    {
        str += "Jedynie p^lytkie koleiny wy^z^lobione ko^lami "+
            "ci^e^zkich woz^ow nieco utrudniaj^a podr^o^z duktem. "+
            "Kawa^lek st^ad, na zachodzie widzisz b^l^ekitne wody "+
            "spokojnego o tej porze roku Pontaru, p^lyn^acego z "+
            "wyra^xnie dobiegaj^acym do twych uszu szumem na p^o^lnoc. ";
    }
    if(pora_roku() == MT_ZIMA)
    {
       str += "P^lytkie koleiny wy^z^lobione ko^lami ci^e^zkich "+
            "woz^ow wype^lnione s^a zamarzni^et^a na "+
            "kamie^n brej^a. Kawa^lek st^ad, na zachodzie widzisz "+
            "skut^a grub^a warstw^a lodu tafl^e Pontaru, tocz^acego "+
            "swe wody na p^o^lnoc. ";
    }
    str += "Brzeg rzeki wzmocniony jest wyra^xnie widocznym st^ad "+
        "drewnianym pomostem. Na p^o^lnocny zach^od wiedzie w^aska, "+
        "miejska uliczka, za^s pod^a^zaj^ac na p^o^lnoc dotrzec mo^zna "+
        "do centrum mie^sciny.";
    str += "\n";
    return str;
}
