/* Autor:
   Opis :
   Data : */

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"
#include <pogoda.h>
inherit STREET_STD;


void create_ulica()
{
    set_short("Ulica");
    add_exit(PIANA_LOKACJE + "rynek","s",0,1,0);
    add_exit(PIANA_OKOLICE_TRAKT_LOKACJE + "droga_01","n",0,8,0);

    add_prop(ROOM_I_INSIDE,0);
    add_object(PIANA_PRZEDMIOTY + "lampa_uliczna");
    add_object(PIANA_DRZWI + "drzwi_do_krawca");

    add_item(({"dom","domek","chatk^e","krawca","wystaw^e"}),"Malutka "+
        "chatka, pomalowana jasno^z^o^lt^a, odpryskuj^ac^a ju^z troch^e "+
        "farb^a, jest bez w^atpienia siedzib^a krawca, na co wskazuje "+
        "spory szyld wisz^acy nad drzwiami, a tak^ze towar "+
        "znajduj^acy si^e na wystawie. S^a to g^l^ownie koszule, spodnie "+
        "i sp^odnice na drewnianych manekinach, obci^agni^etych p^l^otnem, "+
        "ale mo^zna te^z dostrzec tam onuce, czy buty. Dach zosta^l pokryty "+
        "dach^owkami, r^ownie czerwonymi, co drzwi prowadz^ace do ^srodka, "+
        "a tak^ze ramy okien.\n");

    add_item(({"chwasty","trawnik"}),"@@chwasty@@");
}

public string
exits_description()
{
    return "Ulica pod^a^za na po^ludnie w stron^e rynku, wida^c tutaj "+
        "tak^ze wej^scie do zak^ladu krawieckiego, "+
        "za^s na p^o^lnocy znajduje si^e trakt.\n";
}

string
dlugi_opis()
{
    string str;
    if(jest_dzien())
    {
        str = "Brukowany rynek z po^ludnia zw^e^za si^e w tym miejscu w "+
            "niezbyt szerok^a drog^e, kt^ora dalej, na p^o^lnoc zamienia "+
            "si^e w b^lotnisty trakt. ";
        if(pora_roku() != MT_ZIMA)
        {
            str += "Kraw^e^zniki obros^ly wszelkiego rodzaju chwastami i "+
                "nikt najwyra^xniej nie my^sli o tym, by je wyplewi^c. ";
        }
        str += "Ze wszystkich widocznych tutaj domk^ow wyr^o^znia si^e "+
            "jeden, pozbawiony z przodu zwyk^lych okien, a wyposa^zony w "+
            "skromn^a wystaw^e. Wszystkie za^s, jednakowo, pokryte zosta^ly "+
            "czerwonymi dach^owkami, a ich ^sciany pomalowane na ciep^le "+
            "odcienie ^z^o^lci lub bieli. Gdzieniegdzie farba odpad^la, "+
            "ukazuj^ac drewnian^a struktur^e co niekt^orych dom^ow.";
    }
    else
    {
        str = "Brukowany rynek z po^ludnia zw^e^za si^e w tym miejscu w "+
            "niezbyt szerok^a drog^e, kt^ora dalej, na p^o^lnoc zamienia "+
            "si^e w b^lotnisty trakt. ";
        if(pora_roku() != MT_ZIMA)
        {
            str += "Kraw^e^zniki obros^ly wszelkiego rodzaju chwastami i "+
                "nikt najwyra^xniej nie my^sli o tym, by je wyplewi^c. ";
        }
        str += "Ze wszystkich widocznych tutaj domk^ow wyr^o^znia si^e "+
            "jeden, pozbawiony z przodu zwyk^lych okien, a wyposa^zony w "+
            "skromn^a wystaw^e. Wszystkie za^s, jednakowo, pokryte zosta^ly "+
            "czerwonymi dach^owkami, a ich ^sciany pomalowane na ciep^le "+
            "odcienie ^z^o^lci lub bieli. Gdzieniegdzie farba odpad^la, "+
            "ukazuj^ac drewnian^a struktur^e co niekt^orych dom^ow.";
    }
    str += "\n";
    return str;
}

string
chwasty()
{
    string str;
    if(pora_roku() != MT_ZIMA)
    {
        str = "Trawnik przed domem ca^lkowicie obr^os^l chwastami, w "+
            "g^l^ownej mierze mleczem i powojem.\n";
    }
    else
    {
        str = "Trawnik przed domem zosta^l ca^lkowicie przys^loni^ety "+
            "pierzyn^a ^snie^znego puchu\n";
    }
    return str;
}


/*
Dla potomnych, jak beda chcieli przesunac blokade gdzie indziej. Zostawiam :)
---------------blokada----
int unq_no_move(string str)
{
    ::unq_no_move(str);

    if (member_array(query_verb(), ({"p^o^lnoc})) != -1)
        notify_fail("Stra�nicy blokuj^a ci drog^e i ka�� "+
            "zawr�ci� t�umacz�c si� rzekom� przebudow� traktu...\n");
    return 0;
}
//--------------------------
*/

