// Autor nieznany, poprawki - Vera.
// Ludzie piszcie ITEMY!!!

#include <stdproperties.h>
#include <object_types.h>
#include <pl.h>
#include <macros.h>
#include <mudtime.h>
#include "dir.h"

inherit PIANA_STD;

string sprawdzmy_pore();
int wejdz(string str);
string temp;
create_piana()
{
    set_short("Przed spalon^a cha^lup^a");
    set_long(
        "Stoisz na wy^lo^zonej g^ladkimi kamieniami do^s^c szerokiej "+
        "miejskiej uliczce. Ko^la kupieckich woz^ow licznie od lat "+
        "przybywaj^acych do widocznego na zachodzie niewielkiego "+
        "miasteczka wy^z^lobi^ly w twardym granicie g^l^ebokie koleiny "+
        "@@sprawdzmy_pore@@"+
        "Kawa^lek dalej, na wschodzie trakt niknie mi^edzy drzewami "+
        "zapewniaj^acymi odrobin^e cienia podr^o^zuj^acym mi^edzy Pian^a, "+
        "a odleg^lym Tretogorem. Kilka krok^ow na po^ludnie, za zrujnowanym "+
        "p^lotkiem widzisz ruiny spalonej, drewnianej cha^lupy. O jej "+
        "^swietno^sci ^swiadczy jedynie murowany komin stercz^acy w^sr^od "+
        "zgliszczy. Niedaleko st^ad, na zachodzie widzisz niewielki, "+
        "drewniany zajazd, za^s id^ac dalej dotrzesz do centrum miasteczka.\n"
        );
    add_item(
        ({"cha^lup^e","spalon� cha�up�","chat�","spalon� chat�"}),
        "Za niezbyt wysokim, w tej chwili praktycznie ca^lkowicie "+
        "zrujnowanym p^lotem widzisz zgliszcza niedu^zej, drewnianej "+
        "cha^lupy. W tej chwili zachowany jest jedynie fragment jednej z "+
        "^scian i fragment stropu - gdyby nie murowany komin stercz^acy "+
        "w^sr^od wypalonych desek ca^lo^s^c zawali^la by si^e ju^z dawno "+
        "temu.\n"
        );
    add_item("koleiny", "Ko�a przeje�dzaj�cych t�dy woz�w wy��obi�y w "+
    "b�otnistej drodze wysokie na ponad p� stopy koleiny, utrudniaj�ce "+
    "teraz sprawny przejazd.\n");
    add_item(({"p�ot", "p�otek"}), "Pojedyncze sztachety otaczaj�cego niegdy�"+
      "domostwo p�otu ledwo trzymaj� si� prz�se�. Furtka, a raczej to co z "+
    "niej pozosta�o, zawis�a tylko na jednym zawiasie i przy gwa�towniejszych "+
    "powiewach wiatru uchyla si�, skrzypi�c przy tym przera�liwie.\n");
    add_item("furtk�", "S� to marne szcz�tki furtki. Jest w op�akanym stanie. "+
    "Ledwo wisi na jednym zawiasie i przy gwa�towniejszych "+
    "powiewach wiatru uchyla si�, skrzypi�c przy tym przera�liwie.\n");
    add_item(({"komin", "murowany komin"}), "S�dz�c po pozosta�o�ciach, "+
    "okaza�y komin znajdowa� si� niegdy� po �rodku drewnianego domostwa, "+
    "stanowi�c jednocze�nie wsparcie dla ca�ej konstrucji. Jedynie on "+
    "osta� si� po po�arze, podrzymuj�c resztki wal�cych si� �cian.\n");

    add_event("Furtka skrzypi na wietrze.\n");
    add_event("");

    add_prop(ROOM_I_INSIDE, 0);  
    add_exit(PIANA_LOKACJE + "chalupa.c", 
        ({"cha^lupa","do cha^lupy","z zewn�trz"}),0,1,1);
    add_exit(PIANA_LOKACJE_ULICE + "u08","w",0,1,0);
    add_exit(PIANA_OKOLICE_TRAKT_LOKACJE + "trakt_01","e",0,8,0);
}
public string
exits_description()
{
     return "Ulica ci^agnie si^e na zach^od, za^s na wschodzie znajduje "+
        "si^e trakt. Znajduje si^e tutaj tak^ze wej^scie do spalonej "+
         "cha^lupy.\n";
}

init()
{
    ::init();
    add_action(wejdz, "wejd^z");
}
int
wejdz(string str)
{
    notify_fail("Gdzie dok^ladnie chcesz wej^s^c?\n");
    
    if(!str)
        return 0;
    
    if(str ~= "do cha^lupy")
    {
        TP->command("cha�upa");
        return 1;
    }
    
    
    return 0;
}
string
sprawdzmy_pore()
{
        if (pora_roku() == MT_WIOSNA || pora_roku() == MT_JESIEN)
        {
            temp = "wype^lnione teraz po brzegi czarn^a, ^smierdz^ac^a brej^a - "+
            "mieszanin^a b^lota i ko^nskich odchod^ow.";
        }
        if (pora_roku() == MT_ZIMA)
        {
            temp = 
            "wype^lnione teraz po brzegi zamarzni^et^a mieszanin^a b^lota "+
            "i ko^nskich odchod^ow.";
        }
        if (pora_roku() == MT_LATO)
        {
            temp =
            "na dnie kt^orych zalega niezbyt g^l^eboka warstwa "+
            "^smierdz^acego, g^estego b^lota.";
        }
        
        return temp;
}