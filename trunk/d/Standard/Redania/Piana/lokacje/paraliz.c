inherit "/std/paralyze";
 
#include <pl.h>
#include <stdproperties.h>
#include <macros.h>
#include "dir.h"

object chalupa = PIANA_LOKACJE + "chalupa.c";
object piwniczka = PIANA_LOKACJE + "piwniczka.c";
object paraliz2;

void create_paralyze()
{
       set_name("dach_paraliz");
       set_fail_message("Narazie skoncentrowany jeste^s na "+
        "podnoszeniu fragmentu dachu.\n");
       
       set_remove_time(2);
       set_finish_fun("dach_finish");
       set_finish_object(TO);
}

void remove_paralyze()
{
	remove_object();
}

int
dach_finish()
{
    if (TP->query_prop("PIWNICZKA") == 1)
    {
        piwniczka->testuj_sile();
    }
    else
    {
        chalupa->testuj_sile();
    }
    
    return 0;
}

