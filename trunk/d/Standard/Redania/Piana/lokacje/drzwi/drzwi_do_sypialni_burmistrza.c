/* Autor: Avard
   Opis : Samaia
   Data : 3.08.07 */

inherit "/std/door";

#include <pl.h>
#include "dir.h"

void
create_door()
{
    ustaw_nazwe("drzwi");
    dodaj_przym("jasny","ja^sni");
    dodaj_przym("w^aski", "w^ascy");

    set_other_room(PIANA_LOKACJE + "sypialnia_b.c");
    set_door_id("DRZWI_DO_SYPIALNI_BURMISTRZA_PIANY");
    set_door_desc("Drewniane, proste drzwi broni^ace dost^epu do dalszej "+
        "cz^e^sci domu zosta^ly pokryte drobnymi, ozdobnymi malunkami.\n");

    set_open_desc("");
    set_closed_desc("");

    set_pass_command(({"drzwi","sypialnia","pok^oj"}),
        "przez jasne w^askie drzwi","z gabinetu");

    set_open(0);
    set_locked(1);

    set_key("KLUCZ_DRZWI_DO_SYPIALNI_BURMISTRZA_PIANY");
    set_lock_name(({"zamek", "zamku", "zamkowi", "zamek", "zamkiem", "zamku"}), 0, PL_MESKI_NOS_NZYW);
}