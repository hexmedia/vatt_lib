/* Autor: Avard
   Opis : Samaia
   Data : 29.11.07 */

inherit "/std/door";

#include <pl.h>
#include "dir.h"

void
create_door()
{
    ustaw_nazwe("drzwi");
    dodaj_przym("szeroki", "szerocy");
    dodaj_przym("dwuskrzyd^lowy","dwuskrzyd^lowi");

    set_other_room(PIANA_LOKACJE + "sklep.c");
    set_door_id("DRZWI_DO_SKLEPU_PIANY");
    set_door_desc("Szerokie dwuskrzyd^lowe drzwi by^lby w stanie "+
        "pomie^sci^c trojga ludzi, kt^orzy by staneli jeden obok "+
        "drugiego, przez co zajmuj^a wi^eksz^a cz^e^s^c i tak niewielkiej "+
        "^sciany. ^Swie^za, ^snie^znobia^la farba nie chce zdadzi^c gatunku "+
        "drewna, za^s otworzy^c je pozwala kulista, mosi^e^zna klamka. \n");

    set_open_desc("");
    set_closed_desc("");

    set_pass_command(({"drzwi","sklep"}),
        "przez szerokie dwuskrzyd^lowe drzwi","z zewn^atrz");

    set_open(0);
    set_locked(0);

    set_key("KLUCZ_DRZWI_DO_SKLEPU_PIANY");
    set_lock_name(({"zamek", "zamku", "zamkowi", "zamek", "zamkiem", "zamku"}), 0, PL_MESKI_NOS_NZYW);
}