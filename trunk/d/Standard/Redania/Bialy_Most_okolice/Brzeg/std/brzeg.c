
/* Std brzegu przy Bia�ym Mo�cie
   Vera */

#include "dir.h"

inherit REDANIA_STD;
inherit "/std/ryby/lowisko";

#include <mudtime.h>
#include <language.h>
#include <stdproperties.h>
#include <pogoda.h>
#include <macros.h>

string opis_polmroku();
string dlugasny();

void
create_brzeg()
{
}

nomask void
create_redania()
{
    set_long("@@dlugasny@@");
    set_polmrok_long("@@opis_polmroku@@");

    set_start_place(0);

    set_event_time(450.0);
    add_event("@@events:"+file_name(TO)+"@@");
    add_prop(ROOM_I_TYPE, ROOM_BEACH);
    add_prop(ROOM_I_WSP_Y, WSP_Y_BIALY_MOST); //Wsp�rz�dne, do systemu pogodowego.
    add_prop(ROOM_I_WSP_X, WSP_X_BIALY_MOST); //Rinde jest p�pkiem wszech�wiata ;)
    					//A Bia�y Most_okolice na zach�d od niego...
    /*

    add_sit("na ziemi","na ziemi","z ziemi",0);


	dodaj_ziolo(({"czarny_bez-lisc.c",
				"czarny_bez-kwiaty.c",
				"czarny_bez-owoce.c"}));
	*/

    add_item("rzek�","@@rzeka@@");

	dodaj_lowisko("w rzece",
			(["jazgarz":1.0, "ploc":1.5, "leszcz":1.0,
			 "ukleja":1.5, "okon":1.0, "szczupak":0.7]));

	create_brzeg();
}

string
filary()
{
	if(jest_dzien())
		return "Smuk�e, bia�e filary wynurzaj� si� z nurtu, po czym ��cz� "+
		"delikatnymi �ukami by w ko�cu si�gn�� kunsztownych, lecz mocno ju� "+
		"zniszczonych barierek. Elfia budowla przetrwa�a wiele lat u�ytkowania "+
		"przez ludzi, co zaowocowa�o do�� posuni�t� dewastacj�. W tej chwili "+
		"odbywaj� si� na nim dora�ne remonty oszpecaj�ce most sw� "+
		"prowizoryczno�ci� i toporno�ci�.\n";
	else
		return "W takim mroku niewiele zobaczysz.\n";

}

string
rzeka()
{
	if(jest_dzien())
		return "Senny nurt szeroko rozlewaj�cej swe wody rzeki burzony "+
		"jest od czasu do czasu g��boko zanurzonymi barkami lub niewielkimi "+
		"rybackimi ��dkami. Niekiedy rzuci si� ryba, plu�nie w wodzie wydra, "+
		"czy zerwie kaczka. Przeciwny brzeg jest wprawdzie odleg�y, lecz w "+
		"miar� wyra�nie mo�na dostrzec rozci�gaj�ce si� tam pola i wioski.\n";
	else
		return "Nie jeste� w stanie dostrzec nic, pr�cz ciek�ej czarnej "+
		"otch�ani.\n";
}


string
events()
{
	if(jest_dzien())
	{
		switch(random(9))
		{
			case 0:
				return "Co� z g�o�nym pluskiem spad�o z mostu do rzeki.\n";
			case 1:
				return "Rybitwy rozpoczynaj� gwa�towny b�j o jakie� resztki.\n";
			case 2:
				return "Leciwa barka spokojnie p�ynie d� rzeki.\n";
			case 3:
				return "Leciwa barka spokojnie p�ynie g�r� rzeki.\n";
			case 4:
				return "Smuk�y barkas tnie nurt zmierzaj�c w d� rzeki.\n";
			case 5:
				return "Smuk�y barkas tnie nurt zmierzaj�c w g�r� rzeki.\n";
			case 6:
				return "Nurt niesie ogryzione przez ryby i raki truch�o "+
						"jakiego� sporego zwierz�cia.\n";
			case 7:
				return "Tu� przy brzegu rzuci�a si� jaka� du�a ryba.\n";
			case 8:
				return "Muchy staj� si� coraz bardziej natarczywe.\n";
		}
	}
	else
	{
		switch(random(5))
		{
			case 0:
				return "Co� z g�o�nym pluskiem spad�o z mostu do rzeki.\n";
			case 1:
				return "Nurt niesie ogryzione przez ryby i raki truch�o "+
						"jakiego� sporego zwierz�cia.\n";
			case 2:
				return "Tu� przy brzegu rzuci�a si� jaka� du�a ryba.\n";
			case 3:
				return "Gdzie� blisko zaszczeka� pies.\n";
			case 4:
				return "Z niemal ka�dego ciemniejszego k�ta s�yszysz "+
						"drapanie i piski szczur�w.\n";
		}
	}

	return "";
}

string
dlugasny()
{
	return "Ups! Zg�o� b��d!\n";
}

string
opis_polmroku()
{
    string str;
	//Noo, po traktach si� chodzi z lampami!
	//I to o ka�dej porze roku! Nie wiedzieli�cie? :)

    if(CZY_JEST_SNIEG(this_object()))
        str="W mroku jeste� w stanie dostrzec jedynie biel �niegu "+
            "le��cego na trakcie.\n";
    else
        str="Nad traktem zapad� mrok, dlatego nie jeste� w stanie dostrzec "+
            "zbyt wiele.\n";

    return str;
}

