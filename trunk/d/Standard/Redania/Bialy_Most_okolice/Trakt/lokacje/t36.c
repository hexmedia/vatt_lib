/*
 * Opis oraz kod automatycznie wygenerowany za pomoca VattGen,
 * Data: Friday 02nd of November 2007 09:33:52 PM
 */

#include <pogoda.h>
#include <stdproperties.h>
#include <mudtime.h>
#include "dir.h"

inherit TRAKT_DO_PIANY_STD;

void
create_trakt()
{
    set_short("Trakt w^sr^od ^l^ak");
    set_long("@@dlugi_opis@@");
    add_exit(TRAKT_BIALY_MOST_LOKACJE + "t35.c","se",0,FATIGUE_TRAKT,0);
    add_exit(TRAKT_BIALY_MOST_LOKACJE + "t37.c","w",0,FATIGUE_TRAKT,0);
    add_prop(ROOM_I_INSIDE,0);

}

public string
exits_description()
{
    return "Trakt prowadzi na zach^od i po^ludniowy wsch^od.\n";
}

string
dlugi_opis()
{
    string str = "";

    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="Pomi^edzy kamykami po^lo^zonymi, by wzmocni^c nawierzchnie t"+
             "raktu wida^c pozamarzane ka^lu^ze. Szron, kt^ory pokrywa ^l^"+
             "aki dodaje surowo^sci krajobrazowi otaczaj^acemu trakt. W^as"+
             "ki, zryty koleinami trakt wije sie po^sr^od ^lagodnych wzg^o"+
             "rz. Wystaj^ace dooko^la z ziemi kikuty ro^slin pokry^ly p^la"+
             "tki ^sniegu. W zag^l^ebieniu drogi wiatr nawia^l spor^a ilo^"+
             "s^c ^sniegu tworz^ac do^s^c utrudniaj^ac^a przejazd zasp^e. ";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str+="Pomi^edzy kamykami po^lo^zonymi, by wzmocni^c nawierzchnie t"+
             "raktu, zaczyna kie^lkowa^c szorstka trawa i jakie^s inne pos"+
             "polite zielska. P^aki polnych kwiat^ow, rosn^acych wok^o^l t"+
             "raktu, powoli przygotowuj^a si^e do rozkwitu. W^aski, zryty "+
             "koleinami trakt wije sie po^sr^od ^lagodnych wzg^orz. Dooko^"+
             "la rosn^a spragnione s^lo^nca m^lode ro^slinki. W zag^l^ebie"+
             "niu drogi nazbiera^lo si^e sporo wody tworz^ac rozleg^l^a ka"+
             "^lu^z^e. ";
    }
    else if(pora_roku() == MT_LATO)
    {
        str+="Pomi^edzy kamykami po^lo^zonymi, by wzmocni^c nawierzchnie t"+
             "raktu, ro^snie bujnie szorstka trawa i jakie^s inne pospolit"+
             "e zielska. Wok^o^l traktu roztaczaj^a si^e ^l^aki, bujnie oz"+
             "dobione barwn^a flor^a. W^aski, zryty koleinami trakt wije s"+
             "ie po^sr^od ^lagodnych wzg^orz. Dooko^la bujnie rosn^a przer"+
             "^o^zne ro^sliny. W zag^l^ebieniu drogi nazbiera^lo si^e spor"+
             "o wody, kt^ora nie zd^a^zy^la wyparowa^c w ca^lo^sci i utwor"+
             "zy^la niezbyt rozleg^l^a ka^lu^z^e. ";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str+="Pomi^edzy kamykami po^lo^zonymi, by wzmocni^c nawierzchnie t"+
             "raktu, ro^snie trawa i jakie^s inne wysuszone, pospolite zie"+
             "lska. Nieliczne kwiaty na ^l^akach wok^o^l traktu przygotowu"+
             "j^a si^e do nadchodz^acej zimy. W^aski, zryty koleinami trak"+
             "t wije sie po^sr^od ^lagodnych wzg^orz. Rosn^ace dooko^la ro"+
             "^sliny wi^edn^a i trac^a intensywne kolory. W zag^l^ebieniu "+
             "drogi nazbiera^lo si^e sporo wody tworz^ac rozleg^l^a ka^lu^"+
             "z^e. ";
    }

    str+="\n";
    return str;
}