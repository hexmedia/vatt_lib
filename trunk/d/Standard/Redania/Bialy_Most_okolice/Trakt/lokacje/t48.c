/*
 * Opis oraz kod automatycznie wygenerowany za pomoca VattGen,
 * Data: Friday 02nd of November 2007 11:18:50 PM
 */

#include <pogoda.h>
#include <stdproperties.h>
#include <mudtime.h>
#include "dir.h"

inherit TRAKT_DO_PIANY_STD;

void
create_trakt()
{
    set_short("Ubity trakt");
    set_long("@@dlugi_opis@@");
    add_exit(TRAKT_BIALY_MOST_LOKACJE + "t47.c","se",0,FATIGUE_TRAKT,0);
    add_exit(TRAKT_BIALY_MOST_LOKACJE + "t49.c","nw",0,FATIGUE_TRAKT,0);
    add_prop(ROOM_I_INSIDE,0);

}

public string
exits_description()
{
    return "Trakt prowadzi na p^o^lnocny zach^od i po^ludniowy wsch^od.\n";
}

string
dlugi_opis()
{
    string str = "";

    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="Ca^le pole przykrywa gruba warstwa bia^lego puchu. W stertac"+
             "h pokruszonych kamieni wznosz^acych si^e po obu stronach dro"+
             "gi z trudem mo^zna rozpozna^c pozosta^lo^sci po niewysokich "+
             "kamiennych murkach zbudowanych niegdy^s z bry^l piaskowca a "+
             "obecnie zniszczonych przez wiatry i deszcze, poro^sni^etych "+
             "przez ma^le krzaczki i ruderalne zielsko. Ma^le kamyczki pok"+
             "rywaj^ace trakt, s^a g^l^eboko wbite w zamarzni^et^a ubit^a "+
             "ziemi^e. ";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str+="Pole uprawne niedawno zosta^lo zorane i zasiane zbo^zem . W "+
             "stertach pokruszonych kamieni wznosz^acych si^e po obu stron"+
             "ach drogi z trudem mo^zna rozpozna^c pozosta^lo^sci po niewy"+
             "sokich kamiennych murkach zbudowanych niegdy^s z bry^l piask"+
             "owca a obecnie zniszczonych przez wiatry i deszcze, poro^sni"+
             "^etych przez ma^le krzaczki i ruderalne zielsko. Ma^le kamyc"+
             "zki pokrywaj^ace trakt, s^a g^l^eboko wbite w rozmoczon^a ub"+
             "it^a ziemi^e. ";
    }
    else if(pora_roku() == MT_LATO)
    {
        str+="Na polu bujnie ro^snie zbo^ze z^lotego koloru . W stertach p"+
             "okruszonych kamieni wznosz^acych si^e po obu stronach drogi "+
             "z trudem mo^zna rozpozna^c pozosta^lo^sci po niewysokich kam"+
             "iennych murkach zbudowanych niegdy^s z bry^l piaskowca a obe"+
             "cnie zniszczonych przez wiatry i deszcze, poro^sni^etych prz"+
             "ez ma^le krzaczki i ruderalne zielsko. Ma^le kamyczki pokryw"+
             "aj^ace trakt, s^a g^l^eboko wbite w wysuszon^a ubit^a ziemi^"+
             "e. ";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str+="Na polu zosta^ly wystaj^ace ko^nc^owki ^sci^etego zbo^za jak"+
             "o ^slad po ^zniwach . W stertach pokruszonych kamieni wznosz"+
             "^acych si^e po obu stronach drogi z trudem mo^zna rozpozna^c"+
             " pozosta^lo^sci po niewysokich kamiennych murkach zbudowanyc"+
             "h niegdy^s z bry^l piaskowca a obecnie zniszczonych przez wi"+
             "atry i deszcze, poro^sni^etych przez ma^le krzaczki i rudera"+
             "lne zielsko. Ma^le kamyczki pokrywaj^ace trakt, s^a g^l^ebok"+
             "o wbite pokryta nadgni^l^a traw^a, ubit^a ziemi^e. ";
    }

    str+="\n";
    return str;
}