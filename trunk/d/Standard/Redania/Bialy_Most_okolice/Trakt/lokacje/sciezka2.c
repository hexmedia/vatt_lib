/*
 * Opis oraz kod automatycznie wygenerowany za pomoca VattGen,
 * Data: Thursday 18th of June 2009 03:28:33 PM
 */

#include <pogoda.h>
#include <stdproperties.h>
#include <mudtime.h>
#include "dir.h"

inherit TRAKT_BIALY_MOST_STD;

void
create_trakt()
{
    set_short("^Scie^zka w^sr^od p^ol");
    set_long("@@dlugi_opis@@");
    add_exit(TRAKT_BIALY_MOST_LOKACJE + "sciezka3.c","n",0,FATIGUE_TRAKT,0);
    add_exit(TRAKT_BIALY_MOST_LOKACJE + "sciezka1.c","sw",0,FATIGUE_TRAKT,0);
    add_prop(ROOM_I_INSIDE,0);

    add_item(({"s^lom^e","stert^e s^lomy","stert^e"}),
               "Zbutwia^la s^loma le^zy tutaj co najmniej od zesz^lego roku, "+
               "cho^c zapewne d^lu^zej, zaczernia^la od deszczu sta^la si^e "+
               "siedliskiem myszy oraz przer^o^znej ma^sci robactwa i "+
               "owad^ow\n");
}

public string
exits_description()
{
    return "^Scie^zka prowadzi na p^o^lnoc i na po^ludniowy zach^od.\n";
}

string
dlugi_opis()
{
    string str = "";

    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="Na zboczach traktu znajduj^a si^e krzewy bez li^sci, pokryte"+
                " ^sniegiem. Pola uprawne rozci^agaj^ace si^e dooko^la traktu"+
                ", pokrywa ^snie^znobia^ly puch po kt^orym od czasu do czasu "+
                "przebiega jakie^s ma^le zwierz^atko. Za zakr^etem drogi wida"+
                "^c kolejne nagie pola. Na skraju pola, w pewnej odleg^lo^sci"+
                " od drogi le^zy sterta zesz^lorocznej s^lomy, ^Sniegowa czap"+
                "a czyni j^a trudno dostrzegaln^a w niemal ca^lkowicie bia^le"+
                "j okolicy. Tu^z przy drodze wystaje spod ^sniegu pie^n ^sci^"+
                "etego drzewa. ";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str+="Na zboczach traktu pojawiaj^a si^e s^labo rozwini^ete kwiaty"+
                " polne oraz krzewy. Pola uprawne rozci^agaj^ace si^e dooko^l"+
                "a traktu, pokrywa delikatna m^loda ziele^n, w kt^orej buszuj"+
                "^a obudzone z zimowego snu zwierz^eta. Za zakr^etem drogi wi"+
                "da^c kolejne pola. Na skraju pola, w pewnej odleg^lo^sci od "+
                "drogi le^zy sterta zesz^lorocznej s^lomy, Przyci^aga wzrok w"+
                "yr^o^zniaj^ac si^e na tle m^lodej, soczystej trawy. Tu^z prz"+
                "y drodze znajduje si^e pie^n ^sci^etego drzewa, kt^ory zaczy"+
                "na zas^lania^c rosn^aca szybko trawa. ";
    }
    else if(pora_roku() == MT_LATO)
    {
        str+="Na zboczach traktu wida^c dorodne krzewy oraz barwne kwiaty."+
                " Pola uprawne rozci^agaj^ace si^e dooko^la traktu, pokrywa c"+
                "iemnozielona bujna ro^slinno^s^c, w kt^orej buszuj^a obudzon"+
                "e z zimowego snu zwierz^eta. Za zakr^etem drogi wida^c kolej"+
                "ne ^lany zbo^za na polach. Na skraju pola, w pewnej odleg^lo"+
                "^sci od drogi le^zy sterta zesz^lorocznej s^lomy, Przyci^aga"+
                " wzrok wyr^o^zniaj^ac si^e na tle wysokiej, soczystej trawy."+
                " Tu^z przy drodze znajduje si^e pie^n ^sci^etego drzewa, obe"+
                "cnie niemal ca^lkowicie zas^loni^etego traw^a. ";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str+="Zbocza traktu zape^lniaj^a powoli wi^edn^ace kwiaty oraz krz"+
                "ewy. Pola uprawne rozci^agaj^ace si^e dooko^la traktu, s^a z"+
                "aorane i przygotowane do zimy, a od czasu do czasu mo^zna za"+
                "uwa^zy^c jak przebiega po nich jakie^s drobne zwierz^atko. Z"+
                "a zakr^etem drogi wida^c kolejne zaorane pola. Na skraju pol"+
                "a, w pewnej odleg^lo^sci od drogi le^zy sterta zesz^loroczne"+
                "j s^lomy, Przyci^aga wzrok wyrastaj^ac ponad wysokie, przysy"+
                "chaj^ace trawy. Tu^z przy drodze znajduje si^e pie^n ^sci^et"+
                "ego drzewa. ";
    }

    str+="\n";
    return str;
} 
