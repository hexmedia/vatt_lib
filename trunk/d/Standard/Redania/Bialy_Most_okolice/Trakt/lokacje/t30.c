/*
 * Opis oraz kod automatycznie wygenerowany za pomoca VattGen,
 * Data: Friday 02nd of November 2007 03:45:28 PM
 */

#include <pogoda.h>
#include <stdproperties.h>
#include <mudtime.h>
#include "dir.h"

inherit TRAKT_DO_PIANY_STD;

void
create_trakt()
{
    set_short("Trakt w^sr^od p^ol");
    set_long("@@dlugi_opis@@");
    add_exit(TRAKT_BIALY_MOST_LOKACJE + "t31.c","nw",0,FATIGUE_TRAKT,0);
    add_exit(TRAKT_BIALY_MOST_LOKACJE + "t29.c","se",0,FATIGUE_TRAKT,0);
    add_prop(ROOM_I_INSIDE,0);

}

public string
exits_description()
{
    return "Trakt prowadzi na p^o^lnocny zach^od i po^ludniowy wsch^od.\n";
}

string
dlugi_opis()
{
    string str = "";

    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="Wok^o^l jak okiem si^egn^a^c rozpo^sciera sie pofa^ldowany ^"+
             "lagodnymi wzg^orzami krajobraz. Na oblodzonej, a gdzieniegdz"+
             "ie grz^askiej powierzchni drogi trudno jest stawia^c nogi, a"+
             "by si^e nie zapa^s^c w b^locie, czy nie po^slizgn^a^c, a zap"+
             "ewne niejeden wo^xnica, czy je^xdziec przekona^l si^e tak^ze"+
             ", ^ze r^ownie trudno jest sterowa^c w takich warunkach wozem"+
             " lub wierzchowcem. Na tle bia^lej okolicy trakt odcina si^e "+
             "wyra^xnie, ciemny, ubity i twardy, pokryty koleinami woz^ow,"+
             " tysi^acem ^slad^ow ko^nskich kopyt i ludzkich st^op. Trakt "+
             "wij^acy si^e mi^edzy ma^lymi pag^orkami, przecina w tym miej"+
             "scu pola uprawne przykryte grub^a pokryw^a ^snie^zna, na kt^"+
             "orych gdzieniegdzie mo^zna zauwa^zy^c ^slady drobnej zwierzy"+
             "ny. Ca^le pole przykrywa gruba warstwa bia^lego puchu. ";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str+="Wok^o^l jak okiem si^egn^a^c rozpo^sciera sie pofa^ldowany ^"+
             "lagodnymi wzg^orzami krajobraz. W miernie utwardzonej, grz^a"+
             "skiej powierzchni drogi swoje miejsce znalaz^ly liczne kolei"+
             "ny - pozosta^lo^sci po przeje^zd^zaj^acych t^edy wozach, g"+
             "dzieniegdzie da si^e tak^ze zauwa^zy^c odciski podk^ow, kt^o"+
             "re wraz z koleinami wype^lni^ly si^e wod^a tworz^ac razem ro"+
             "zleg^le ka^lu^ze. Na tle jasnej zieleni trakt odcina si^e wy"+
             "ra^xnie, ciemny, ubity i twardy, pokryty koleinami woz^ow, t"+
             "ysi^acem ^slad^ow ko^nskich kopyt i ludzkich st^op. Trakt wi"+
             "j^acy si^e mi^edzy ma^lymi pag^orkami, przecina w tym miejsc"+
             "u lekko ju^z zazielenione pola uprawne, dostarczaj^ace okoli"+
             "cznym mieszka^ncom po^zywienia. Pole uprawne niedawno zosta^"+
             "lo zorane i zasiane zbo^zem. ";
    }
    else if(pora_roku() == MT_LATO)
    {
        str+="Wok^o^l jak okiem si^egn^a^c rozpo^sciera sie pofa^ldowany ^"+
             "lagodnymi wzg^orzami krajobraz. W miernie utwardzonej, grz^a"+
             "skiej powierzchni drogi swoje miejsce znalaz^ly liczne kolei"+
             "ny - pozosta^lo^sci po przeje^zd^zaj^acych t^edy wozach, g"+
             "dzieniegdzie da si^e tak^ze zauwa^zy^c odciski podk^ow, kt^o"+
             "re wraz z koleinami wype^lni^ly si^e wod^a tworz^ac razem ro"+
             "zleg^le ka^lu^ze. Na tle jasnej zieleni trakt odcina si^e wy"+
             "ra^xnie, ciemny, ubity i twardy, pokryty koleinami woz^ow, t"+
             "ysi^acem ^slad^ow ko^nskich kopyt i ludzkich st^op. Trakt wi"+
             "j^acy si^e mi^edzy ma^lymi pag^orkami, przecina w tym miejsc"+
             "u g^esto obsiane pola uprawne, dostarczaj^ace okolicznym mie"+
             "szka^ncom po^zywienia. Na polu bujnie ro^snie zbo^ze z^loteg"+
             "o koloru. ";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str+="Wok^o^l jak okiem si^egn^a^c rozpo^sciera sie pofa^ldowany ^"+
             "lagodnymi wzg^orzami krajobraz. W miernie utwardzonej, grz^a"+
             "skiej powierzchni drogi swoje miejsce znalaz^ly liczne kolei"+
             "ny - pozosta^lo^sci po przeje^zd^zaj^acych t^edy wozach, g"+
             "dzieniegdzie da si^e tak^ze zauwa^zy^c odciski podk^ow, kt^o"+
             "re wraz z koleinami wype^lni^ly si^e wod^a tworz^ac razem ro"+
             "zleg^le ka^lu^ze. Na tle szaro^sci trakt odcina si^e wyra^xn"+
             "ie, ciemny, ubity i twardy, pokryty koleinami woz^ow, tysi^a"+
             "cem ^slad^ow ko^nskich kopyt i ludzkich st^op. Trakt wij^acy"+
             " si^e mi^edzy ma^lymi pag^orkami, przecina w tym miejscu prz"+
             "ygotowane ju^z do zimy pola uprawne, dostarczaj^ace okoliczn"+
             "ym mieszka^ncom po^zywienia. Na polu zosta^ly wystaj^ace ko^"+
             "nc^owki ^sci^etego zbo^za jako ^slad po ^zniwach. ";
    }

    str+="\n";
    return str;
}