/*
 * Opis oraz kod automatycznie wygenerowany za pomoca VattGen,
 * Data: Saturday 16th of June 2007 06:45:41 PM
 */

#include <pogoda.h>
#include <stdproperties.h>
#include <mudtime.h>
#include "dir.h"

inherit TRAKT_DO_PIANY_STD;
#define DEF_DIRS ({"p^o^lnoc"})
void
create_trakt()
{
    set_short("Trakt w^sr^od p^ol");
    set_long("@@dlugi_opis@@");
    add_exit(TRAKT_BIALY_MOST_LOKACJE + "t25.c","s",0,FATIGUE_TRAKT,0);
    add_exit(TRAKT_BIALY_MOST_LOKACJE + "t27.c","n",0,FATIGUE_TRAKT,0);

    add_prop(ROOM_I_INSIDE,0);
//     add_npc(TRAKT_RINDE_NPC+"bandyta_duzy");
//     add_npc(TRAKT_RINDE_NPC+"bandyta_krasnolud");
//     add_npc(TRAKT_RINDE_NPC+"bandyta_krasnolud");
//     add_npc(TRAKT_RINDE_NPC+"bandyta_nozownik");
//     add_npc(TRAKT_RINDE_NPC+"bandyta_nozownik");
//     add_npc(TRAKT_RINDE_NPC+"bandyta_pijak");
//     add_npc(TRAKT_RINDE_NPC+"bandyta_tepy");
//     add_npc(TRAKT_RINDE_NPC+"bandyta_zlodziej");
//     add_npc(TRAKT_RINDE_NPC+"bandyta_zlodziej");

}

public string
exits_description()
{
    return "Trakt prowadzi na p^o^lnoc i po^ludnie.\n";
}
// int unq_no_move(string str)
// {
//     ::unq_no_move(str);
// 
//     if (member_array(query_verb(), DEF_DIRS) != -1)
//         notify_fail("Bandyci blokuj^a ci drog^e.\n");
//     return 0;
// }

string
dlugi_opis()
{
    string str = "";

    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="Prawie wszystko przykryte jest spor^a warstw^a bielutkiego ^"+
             "sniegu, kt^ory przysypa^l okolic^e. Na otaczaj^acych trakt p"+
             "olach uprawnych, wida^c jedynie ^slady drobnych zwierz^at, z"+
             "ostawionych na mi^ekkim puchu okrywaj^acym ca^l^a okolice. C"+
             "a^la okolica usiana jest du^zymi kwadratami p^ol uprawnych o"+
             "raz ^l^ak, wykorzystywanych przez okolicznych mieszka^nc^ow."+
             " ";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str+="Otaczaj^aca ci^e przyroda budzi sie do ^zycia po zimowym ^sn"+
             "ie, a lekki wietrzyk przynosi do ciebie ^swie^zy zapach ro^s"+
             "lin. Na otaczaj^acych trakt polach uprawnych, dostrzec mo^zn"+
             "a pracuj^acych w pocie czo^la ch^lop^ow, przygotowuj^acych z"+
             "iemie pod zasiew. Ca^la okolica usiana jest du^zymi kwadrata"+
             "mi p^ol uprawnych oraz ^l^ak, wykorzystywanych przez okolicz"+
             "nych mieszka^nc^ow. ";
    }
    else if(pora_roku() == MT_LATO)
    {
        str+="Ca^la przyroda w pe^lnym rozkwicie, zieleni otaczaj^acy ci^e"+
             " krajobraz, a w powietrzu mo^zesz wyczu^c intensywny zapach "+
             "ro^slin. Na otaczaj^acych trakt polach uprawnych, dostrzec m"+
             "o^zna pracuj^acych w pocie czo^la ch^lop^ow, piel^egnuj^acyc"+
             "h zasiane pola. Ca^la okolica usiana jest du^zymi kwadratami"+
             " p^ol uprawnych oraz ^l^ak, wykorzystywanych przez okoliczny"+
             "ch mieszka^nc^ow. ";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str+="Krajobraz spowija lekka mgie^lka i dymy z ognisk palonych na"+
             " polach. Na otaczaj^acych trakt polach uprawnych, dostrzec m"+
             "o^zna pracuj^acych w pocie czo^la ch^lop^ow, zbieraj^acych j"+
             "u^z ostatnie plony w tym roku. Ca^la okolica usiana jest du^"+
             "zymi kwadratami p^ol uprawnych oraz ^l^ak, wykorzystywanych "+
             "przez okolicznych mieszka^nc^ow. ";
    }

    str+="\n";
    return str;
}