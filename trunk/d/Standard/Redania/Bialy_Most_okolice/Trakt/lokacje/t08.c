
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"
#include <pogoda.h>

inherit TRAKT_BIALY_MOST_STD;


void create_trakt() 
{
    set_short("Ubity trakt");
    set_long("@@dlugasny@@");
    add_exit(TRAKT_BIALY_MOST_LOKACJE + "t07.c","sw",0,FATIGUE_TRAKT,0);
    add_exit(TRAKT_BIALY_MOST_LOKACJE + "t09.c","e",0,FATIGUE_TRAKT,0);

    add_prop(ROOM_I_INSIDE,0);
}
public string
exits_description() 
{
    return "Trakt ci�gnie si� na po�udniowy-zach�d oraz na wsch�d.\n";
}

string
dlugasny()
{
	string str="";

	str+="Pod��asz ubitym traktem ci�gn�cym si� od le�acego na "+
	"wschodzie miasta, a� po mie�cin� na zachodzie. ";

	if(!CZY_JEST_SNIEG(TO) && !CO_PADA(TO))
		str+="Wy�o�on� dopasowanymi kamieniami powierzchni� traktu "+
		"pokrywa gruba warstwa szarego py�u nie "+
		"pozwalaj�ca wypatrze� nawet najbardziej bystremu oku "+
		"licznych dziur w nawierzchni. ";

	if((pora_roku() == MT_WIOSNA || pora_roku() == MT_LATO) && jest_dzien())
	{	str+="Ponad szczytem le��cego na wschodzie wzg�rza "+
			"zieleniej� delikatne li�cie drzew. ";
	}
	else if(pora_roku() == MT_JESIEN && jest_dzien())
		str+="Ponad szczytem le��cego na wschodzie wzg�rza "+
		"czerwieni� si� i z�oc� jesienne szczyty drzew. ";
	else if(pora_roku() == MT_ZIMA)
	{
		if(CZY_JEST_SNIEG(TO))
			str+="Ponad szczytem le��cego na wschodzie wzg�rza "+
			"stercz� ozdobione l�ni�cymi soplami ga��zie drzew. ";
		else
			str+="Ponad szczytem le��cego na wschodzie wzg�rza "+
			"stercz� ogo�ocone z li�ci ga��zie drzew. ";
	
	}
	else if(!jest_dzien())
		str+="Ponad szczytem le��cego na wschodzie wzg�rza majacz� "+
		"na tle rozgwie�d�onego nieba czarne ga��zie drzew. ";


	str+="\n";
	return str;
}
