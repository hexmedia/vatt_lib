/*
 *�cie�ka mi�dzy Bia�ym Mostem a Rinde (w Bia�y_Most_okolice) prowadz�ca
 *do wioski
 *Vera
 *opis alcyone
 */

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"
#include <pogoda.h>

inherit TRAKT_BIALY_MOST_STD;

void create_trakt() 
{
    set_short("�cie�yna mi�dzy ��kami");
    set_long("@@dlugasny@@");
    add_exit(TRAKT_BIALY_MOST_LOKACJE + "t18.c","se",0,FATIGUE_TRAKT,0);
	add_exit(TRAKT_BIALY_MOST_LOKACJE + "s04.c","nw",0,FATIGUE_SCIEZKA,0);
	add_event("Od strony lasu dochodzi ci� weso�y �wiergot jakiego� ptaka.\n");
	add_event("Od p�nocy s�ycha� jakies pokrzykiwania.\n");
	add_event("Cisz� panuj�c� wok� przerywa natarczywe ujadanie psa.\n");
    add_prop(ROOM_I_INSIDE,0);


}
public string
exits_description() 
{
    return "Trakt ci�gnie si� na po�udniowym-wschodzie, za� �cie�ka na "+
		"p�nocnym-zachodzie.\n";
}


string
dlugasny()
{
	string str="";

	str+="Znajdujesz si� w miejscu, gdzie opr�cz rozleg�ych ��k, kt�re "+
	"porastaj� najbli�sz� okolic� w zasi�gu twojego wzroku, maluje si� tak�e "+
	"kontur wioski, kt�ra po�o�ona jest niedaleko na p�nocnym-wschodzie. "+
	"W�ska �cie�ka ";

	if(pora_roku()==MT_WIOSNA || pora_roku() == MT_LATO)
		str+="starannie obro�ni�ta przez zielon� traw�";
	else if(pora_roku() == MT_ZIMA && CZY_JEST_SNIEG(TO))
		str+="starannie udekorowana, przykryt� �nie�n� pierzyn� traw�";
	else
		str+="starannie udekorowana, u�o�on� do zimowego snu traw�";
 
	str+=", na pierwszy rzut wygl�da na idealne pod�o�e do podr�y";

	if(!CZY_JEST_SNIEG(TO))
		str+=", jednak suchy piasek z �atwo�ci� unoszony przez wiatr, mo�e "+
		"dosta� si� do oczu i wywo�a� nieprzyjemne sw�dzenie. ";
	else
		str+=". ";

	str+="Drobne kamyczki, kt�re pokrywaj� �cie�k�, mog� wbija� si� w "+
	"podeszwy but�w, co mo�e by� nieco uci��liwe. Id�c na po�udniowy-wsch�d, "+
	"mo�na doj�� do traktu, kt�ry wy�ania si� spomi�dzy wysokich traw.";

	str+="\n";
	return str;
}
