/*
 * Opis oraz kod automatycznie wygenerowany za pomoca VattGen,
 * Data: Friday 02nd of November 2007 11:12:08 PM
 */

#include <pogoda.h>
#include <stdproperties.h>
#include <mudtime.h>
#include "dir.h"

inherit TRAKT_DO_PIANY_STD;

void
create_trakt()
{
    set_short("Trakt w^sr^od ^l^ak");
    set_long("@@dlugi_opis@@");
    add_exit(TRAKT_BIALY_MOST_LOKACJE + "t42.c","s",0,FATIGUE_TRAKT,0);
    add_exit(TRAKT_BIALY_MOST_LOKACJE + "t44.c","w",0,FATIGUE_TRAKT,0);
    add_prop(ROOM_I_INSIDE,0);

}

public string
exits_description()
{
    return "Trakt prowadzi na zach^od i po^ludnie.\n";
}

string
dlugi_opis()
{
    string str = "";

    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="Du^ze zaspy ^sniegu, kt^ore niemal zamazuj^a ^slad ^scie^zki"+
             " ^swiadcz^a, ^ze droga ta jest bardzo rzadko u^zywana. W zag"+
             "^l^ebieniu drogi wiatr nawia^l spor^a ilo^s^c ^sniegu tworz^"+
             "ac do^s^c utrudniaj^ac^a przejazd zasp^e. W niekt^orych miej"+
             "scach koleiny s^a tak g^l^ebokie, i^z w^oz jad^acy t^edy mus"+
             "i bardzo zwolnic by nie z^lama^c zawieszenia. Boki pokrytego"+
             " ubitym ^sniegiem traktu porastaj^a kolczaste krzewy i nieli"+
             "czne k^epy traw, kt^ore zdo^la^ly przebi^c poprzez zalegaj^a"+
             "ca na ziemi warstw^e bia^lego puchu. Szeroki, zas^lany przez"+
             " ^sniegowy puch oraz brudne b^locko trakt ci^agnie si^e ^lag"+
             "odnymi ^lukami. ";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str+="Ga^l^azki i zgni^le li^scie zalegaj^ace na drodze ^swiadcz^a"+
             ", ^ze droga ta bardzo rzadko jest u^zywana. W zag^l^ebieniu "+
             "drogi nazbiera^lo si^e sporo wody tworz^ac rozleg^l^a ka^lu^"+
             "z^e. W niekt^orych miejscach koleiny s^a tak g^l^ebokie, i^z"+
             " w^oz jad^acy t^edy musi bardzo zwolnic by nie z^lama^c zawi"+
             "eszenia. Boki ubitego traktu porastaj^a liczne krzewy i k^ep"+
             "y zielonych traw, kt^ore p^lynnie ^l^acz^a si^e z okalaj^acy"+
             "mi ^l^akami. Szeroki, zas^lany z rzadka drobnymi kamykami tr"+
             "akt ci^agnie si^e ^lagodnymi ^lukami. ";
    }
    else if(pora_roku() == MT_LATO)
    {
        str+="Ga^l^azki, zesz^loroczne resztki li^sci i bujnie rozro^sni^e"+
             "ta trawa ^swiadcz^a, ze droga ta bardzo rzadko jest u^zywana"+
             ". W zag^l^ebieniu drogi nazbiera^lo si^e sporo wody, kt^ora "+
             "nie zd^a^zy^la wyparowa^c w ca^lo^sci i utworzy^la niezbyt r"+
             "ozleg^l^a ka^lu^z^e. W niekt^orych miejscach koleiny s^a tak"+
             " g^l^ebokie, i^z w^oz jad^acy t^edy musi bardzo zwolnic by n"+
             "ie z^lama^c zawieszenia. Boki ubitego traktu porastaj^a licz"+
             "ne krzewy i k^epy zielonych traw, kt^ore p^lynnie ^l^acz^a s"+
             "i^e z okalaj^acymi ^l^akami. Szeroki, zas^lany z rzadka drob"+
             "nymi kamykami trakt ci^agnie si^e ^lagodnymi ^lukami. ";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str+="Ga^l^azki, opadni^ete li^scie i wi^edn^aca ro^slinno^s^c, kt"+
             "^ora zaros^la ca^l^a drog^e ^swiadcz^a, ^ze jest ona bardzo "+
             "rzadko u^zywana. W zag^l^ebieniu drogi nazbiera^lo si^e spor"+
             "o wody tworz^ac rozleg^l^a ka^lu^z^e. W niekt^orych miejscac"+
             "h koleiny s^a tak g^l^ebokie, i^z w^oz jad^acy t^edy musi ba"+
             "rdzo zwolnic by nie z^lama^c zawieszenia. Boki ubitego trakt"+
             "u porastaj^a liczne krzewy i k^epy zielonych traw, kt^ore p^"+
             "lynnie ^l^acz^a si^e z okalaj^acymi ^l^akami. Szeroki, zas^l"+
             "any z rzadka drobnymi kamykami trakt ci^agnie si^e ^lagodnym"+
             "i ^lukami. ";
    }

    str+="\n";
    return str;
}