
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"
#include <pogoda.h>

inherit TRAKT_BIALY_MOST_STD;

void create_trakt() 
{
    set_short("Trakt po�r�d ��k");
    set_long("@@dlugasny@@");
    add_exit(TRAKT_BIALY_MOST_LOKACJE + "t02.c","w",0,FATIGUE_TRAKT,0);
    add_exit(TRAKT_BIALY_MOST_LOKACJE + "t04.c","se",0,FATIGUE_TRAKT,0);
    add_exit(TRAKT_BIALY_MOST_LOKACJE + "t19.c","nw",0,FATIGUE_TRAKT,0);

    add_prop(ROOM_I_INSIDE,0);
}
public string
exits_description() 
{
    return "Trakt ci�gnie si� na zach�d oraz na po�udniowy-wsch�d, znajduje "+
    "si� tu te� �cie�ka prowadz�ca na p�nocny zach�d.\n";
}

string
dlugasny()
{
	string str="";

	str+="Drog� wy�o�ono g�adkimi kamieniami, niestety nawierzchnia "+
	"lata swojej �wietno�ci ma ju� za sob�. Ju� na pierwszy rzut "+
	"oka dostrzec mo�na na niej licznie zag��bienia, nier�wno�ci i "+
	"koleiny �wiadcz�ce o tym, i� droga od dawna nie by�a remontowana. ";


	if(MOC_WIATRU(TO) > POG_WI_SREDNIE)
	{
		if(pora_roku() == MT_WIOSNA)
			str+="Tocz�ce si� z w�ciek�ym rykiem wody Pontaru cz�ciowo "+
			"zas�aniaj� rosn�ce po po�udniowej stronie traktu niewysokie "+
			"spl�tane, zaro�la pokryte delikatnymi bia�ymi kwiatami. ";
		else if(pora_roku() == MT_LATO)
			str+="Tocz�ce si� z w�ciek�ym rykiem wody Pontaru cz�ciowo "+
			"zas�aniaj� rosn�ce po po�udniowej stronie traktu niewysokie "+
			"spl�tane, zaro�la pokryte jakimi� owocami. ";
		else
			str+="Tocz�ce si� z w�ciek�ym rykiem wody Pontaru cz�ciowo "+
			"zas�aniaj� rosn�ce po po�udniowej stronie traktu niewysokie "+
			"spl�tane, zaro�la pokryte mgie�k� zielonych li�ci. ";
	}
	else
	{
		if(pora_roku() == MT_WIOSNA)
			str+="Leniwie p�yn�ce wody Pontaru cz�ciowo zas�aniaj� rosn�ce "+
			"po po�udniowej stronie traktu niewysokie spl�tane, zaro�la "+
			"pokryte delikatnymi bia�ymi kwiatami. ";
		else if(pora_roku() == MT_LATO)
			str+="Leniwie p�yn�ce wody Pontaru cz�ciowo zas�aniaj� rosn�ce "+
			"po po�udniowej stronie traktu niewysokie spl�tane, zaro�la "+
			"pokryte jakimi� owocami. ";
		else
			str+="Leniwie p�yn�ce wody Pontaru cz�ciowo zas�aniaj� rosn�ce "+
			"po po�udniowej stronie traktu niewysokie spl�tane, zaro�la "+
			"pokryte mgie�k� zielonych li�ci. ";
	}






	str+="\n";
	return str;
}
