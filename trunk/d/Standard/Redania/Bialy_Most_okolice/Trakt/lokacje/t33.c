/*
 * Opis oraz kod automatycznie wygenerowany za pomoca VattGen,
 * Data: Friday 02nd of November 2007 08:53:00 PM
 */

#include <pogoda.h>
#include <stdproperties.h>
#include <mudtime.h>
#include "dir.h"

inherit TRAKT_DO_PIANY_STD;

void
create_trakt()
{
    set_short("Trakt w^sr^od pag^ork^ow");
    set_long("@@dlugi_opis@@");
    add_exit(TRAKT_BIALY_MOST_LOKACJE + "t32","sw",0,FATIGUE_TRAKT,0);
    add_exit(TRAKT_BIALY_MOST_LOKACJE + "t34.c","n",0,FATIGUE_TRAKT,0);
    add_prop(ROOM_I_INSIDE,0);

}

public string
exits_description()
{
    return "Trakt prowadzi na p^o^lnoc i po^ludniowy zach^od.\n";
}

string
dlugi_opis()
{
    string str = "";

    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="Szeroki, ubity trakt pokrywa gruba warstwa puszystego ^snieg"+
             "u. Trakt zmierza ^lagodnym spadem w d^o^l, po^sr^od pag^ork^"+
             "ow pokrytych sk^apana w bieli ^l^ak^a, by dalej rozpocz^a^c "+
             "sw^a m^ecz^ac^a w^edr^owk^e ostrym podej^sciem pod g^or^e i "+
             "znikn^a^c za du^z^a k^ep^a zieleni znajduj^acej si^e na szcz"+
             "ycie wzniesienia. Ca^la okolica jest pokryta ^l^akami i nigd"+
             "zie nie wida^c ^sladu po ludzkich osadach a jedynymi ^zywymi"+
             " osobami jakie napotykasz s^a podr^o^zuj^acy ze swym towarem"+
             " kupcy. Droga, kt^or^a w^edrujesz, przebiega zygzakami miedz"+
             "y wzg^orzami, omijaj^ac wi^eksze wzniesienia i doliny. Boki "+
             "pokrytego ubitym ^sniegiem traktu porastaj^a kolczaste krzew"+
             "y i nieliczne k^epy traw, kt^ore zdo^la^ly przebi^c poprzez "+
             "zalegaj^aca na ziemi warstw^e bia^lego puchu. ";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str+="Szeroki, ubity trakt poro^sni^ety jest gdzieniegdzie wznosz^"+
             "acymi si^e ro^slinami. Trakt zmierza ^lagodnym spadem w d^o^"+
             "l, po^sr^od pag^ork^ow pokrytych jasnozielon^a ^l^ak^a, by d"+
             "alej rozpocz^a^c sw^a m^ecz^ac^a w^edr^owk^e ostrym podej^sc"+
             "iem pod g^or^e i znikn^a^c za du^z^a k^ep^a zieleni znajduj^"+
             "acej si^e na szczycie wzniesienia. Ca^la okolica jest pokryt"+
             "a ^l^akami i nigdzie nie wida^c ^sladu po ludzkich osadach a"+
             " jedynymi ^zywymi osobami jakie napotykasz s^a podr^o^zuj^ac"+
             "y ze swym towarem kupcy. Droga, kt^or^a w^edrujesz, przebieg"+
             "a zygzakami miedzy wzg^orzami, omijaj^ac wi^eksze wzniesieni"+
             "a i doliny. Boki ubitego traktu porastaj^a liczne krzewy i k"+
             "^epy zielonych traw, kt^ore p^lynnie ^l^acz^a si^e z okalaj^"+
             "acymi ^l^akami. ";
    }
    else if(pora_roku() == MT_LATO)
    {
        str+="Szeroki, ubity trakt porasta miejscami soczysta, zielona tra"+
             "wa. Trakt zmierza ^lagodnym spadem w d^o^l, po^sr^od pag^ork"+
             "^ow pokrytych soczysto-zielon^a ^l^ak^a, by dalej rozpocz^a^"+
             "c sw^a m^ecz^ac^a w^edr^owk^e ostrym podej^sciem pod g^or^e "+
             "i znikn^a^c za du^z^a k^ep^a zieleni znajduj^acej si^e na sz"+
             "czycie wzniesienia. Ca^la okolica jest pokryta ^l^akami i ni"+
             "gdzie nie wida^c ^sladu po ludzkich osadach a jedynymi ^zywy"+
             "mi osobami jakie napotykasz s^a podr^o^zuj^acy ze swym towar"+
             "em kupcy. Droga, kt^or^a w^edrujesz, przebiega zygzakami mie"+
             "dzy wzg^orzami, omijaj^ac wi^eksze wzniesienia i doliny. Bok"+
             "i ubitego traktu porastaj^a liczne krzewy i k^epy zielonych "+
             "traw, kt^ore p^lynnie ^l^acz^a si^e z okalaj^acymi ^l^akami."+
             " ";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str+="Szeroki, ubity trakt pokryty jest stert^a r^o^znobarwnych li"+
             "^sci. Trakt zmierza ^lagodnym spadem w d^o^l, po^sr^od pag^o"+
             "rk^ow pokrytych ^l^ak^a, by dalej rozpocz^a^c sw^a m^ecz^ac^"+
             "a w^edr^owk^e ostrym podej^sciem pod g^or^e i znikn^a^c za d"+
             "u^z^a k^ep^a zieleni znajduj^acej si^e na szczycie wzniesien"+
             "ia. Ca^la okolica jest pokryta ^l^akami i nigdzie nie wida^c"+
             " ^sladu po ludzkich osadach a jedynymi ^zywymi osobami jakie"+
             " napotykasz s^a podr^o^zuj^acy ze swym towarem kupcy. Droga,"+
             " kt^or^a w^edrujesz, przebiega zygzakami miedzy wzg^orzami, "+
             "omijaj^ac wi^eksze wzniesienia i doliny. Boki ubitego traktu"+
             " porastaj^a liczne krzewy i k^epy zielonych traw, kt^ore p^l"+
             "ynnie ^l^acz^a si^e z okalaj^acymi ^l^akami. ";
    }

    str+="\n";
    return str;
}