/*
 *�cie�ka mi�dzy Bia�ym Mostem a Rinde (w Bia�y_Most_okolice) prowadz�ca
 *do wioski
 *Vera, opis Faeve
 */

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"
#include <pogoda.h>

inherit TRAKT_BIALY_MOST_STD;

void create_trakt() 
{
    set_short("W�ska �cie�yna");
    set_long("@@dlugasny@@");
    add_exit(TRAKT_BIALY_MOST_LOKACJE + "s04.c","s",0,FATIGUE_SCIEZKA,0);
	add_exit(TRAKT_BIALY_MOST_LOKACJE + "s02.c","nw",0,FATIGUE_SCIEZKA,0);
	add_event("Od strony lasu dochodzi ci� weso�y �wiergot jakiego� ptaka.\n");
	add_event("Od p�nocy s�ycha� jakies pokrzykiwania.\n");
	add_event("Cisz� panuj�c� wok� przerywa natarczywe ujadanie psa.\n");
    add_prop(ROOM_I_INSIDE,0);
    add_prop(ROOM_I_WSP_Y, WSP_Y_WIOSKA_BM);	//bezposrednio na W od Rinde...
    add_prop(ROOM_I_WSP_X, WSP_X_WIOSKA_BM);
}
public string
exits_description() 
{
    return "�cie�ka ci�gnie si� na po�udnie oraz na "+
		"p�nocnym-zachodzie.\n";
}


string
dlugasny()
{
	string str="";

	if(CZY_JEST_SNIEG(TO))
		str+="W�ska �ciezyna gubi si� w�r�d o�nie�onych p�l. "+
		"Lichym urozmaiceniem krajobrazu s� wystaj�ce "+
		"gdzieniegdzie spod warstw srebrzystego puchu "+
		"nagie kikuty traw i ga��zek. Od strony wschodniej "+
		"przestrze� ograniczona jest ciemn� �cian� lasu, "+
		"na po�udniu, w oddali mo�na dojrze� trakt, a kawa�ek "+
		"za nim - szerokie pasmo w�d Pontaru.";
	else
		str+="W�ska �cie�yna gubi si� po�r�d p�l. Podr�uj�cych "+
		"�askocz� sk�aniaj�ce si� ku drodze k�osy traw i zb�, "+
		"malownicza mozaika roztacza si� na wschodzie i "+
		"zachodzie, od strony wschodniej ogranicza j� �ciana "+
		"lasu. Na po�udniu, w oddali, mo�na dojrze� trakt, a "+
		"kawa�ek za nim - szerokie pasmo w�d Pontaru. ";

	str+="Od p�nocy za� dobiegaj� od czasu do czasu jakie� krzyki";

	if(pora_dnia() > MT_POPOLUDNIE && pora_dnia() < MT_SWIT)
		str+=", a znad ledwie widocznych strzech unosz� si� "+
			"niedu�e chmury siwego dymu.";
	else
		str+=".";

	str+="\n";
	return str;
}
