/*
 *�cie�ka mi�dzy Bia�ym Mostem a Rinde (w Bia�y_Most_okolice) prowadz�ca
 *do wioski
 *Vera
 *opis alcyone
 */
 
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"
#include <pogoda.h>

inherit TRAKT_BIALY_MOST_STD;

void create_trakt() 
{
    set_short("�cie�yna mi�dzy ��kami");
    set_long("@@dlugasny@@");
    add_exit(TRAKT_BIALY_MOST_LOKACJE + "s05.c","se",0,FATIGUE_SCIEZKA,0);
	add_exit(TRAKT_BIALY_MOST_LOKACJE + "s03.c","n",0,FATIGUE_SCIEZKA,0);
	add_event("Od strony lasu dochodzi ci� weso�y �wiergot jakiego� ptaka.\n");
	add_event("Od p�nocy s�ycha� jakies pokrzykiwania.\n");
	add_event("Cisz� panuj�c� wok� przerywa natarczywe ujadanie psa.\n");
    add_prop(ROOM_I_INSIDE,0);
    add_prop(ROOM_I_WSP_Y, WSP_Y_WIOSKA_BM);	//bezposrednio na W od Rinde...
    add_prop(ROOM_I_WSP_X, WSP_X_WIOSKA_BM);
}
public string
exits_description() 
{
    return "�cie�ka ci�gnie si� na p�noc st�d oraz na po�udniowy-wsch�d.\n";
}


string
dlugasny()
{
	string str="";

	str+="Dooko�a ciebie nie rozpo�ciera si� nic wi�cej pr�cz rozleg�ych, ";

	if(pora_roku() == MT_WIOSNA)
		str+="kwitn�cych";
	else if(pora_roku() == MT_LATO)
		str+="zielonych";
	else if(pora_roku() == MT_JESIEN)
		str+="powoli zapadaj�cych w zimowy sen";
	else if(CZY_JEST_SNIEG(TO))
		str+="pokrytych �nie�n� pierzyn�";

	str+=" ��k, kt�re wygl�daj� tak, jakby nie mia�y ko�ca. ";

	if(pora_roku() == MT_WIOSNA || pora_roku() == MT_LATO)
		str+="Ubita �cie�ka, kt�ra wydaje si� idealna do podr�y, mo�e "+
		"niejednego zmyli�, gdy� kurz i piach unosz�cy si� przy "+
		"ka�dym kroku, mo�e wyda� si� niezwykle uci��liwy. ";

	str+="Natomiast podczas deszczu, owe pod�o�e zamieni si� w lepkie "+
		"b�oto, kt�re ozdobi podeszw� niejednej osoby. Z pewno�ci� taka "+
		"ozdoba, nie b�dzie prezentowa�a si� szczeg�lnie efektownie. "+
		"Na po�udniowym-wschodzie maluje si� trakt, a �cie�ka biegnie dalej, "+
		"ku p�nocnemu-zachodowi w kierunku, wioski, kt�rej niewyra�ny "+
		"zarys mo�na dostrzec ju� tutaj.";


	str+="\n";
	return str;
}
