/*
 * Opis oraz kod automatycznie wygenerowany za pomoca VattGen,
 * Data: Thursday 18th of June 2009 03:28:33 PM
 */

#include <pogoda.h>
#include <stdproperties.h>
#include <mudtime.h>
#include "dir.h"

inherit TRAKT_BIALY_MOST_STD;

void
create_trakt()
{
    set_short("^Scie^zka w^sr^od p^ol");
    set_long("@@dlugi_opis@@");
    add_exit(TRAKT_BIALY_MOST_LOKACJE + "sciezka2.c","ne",0,FATIGUE_TRAKT,0);
    add_exit(TRAKT_BIALY_MOST_LOKACJE + "t21.c","sw",0,FATIGUE_TRAKT,0);
    add_prop(ROOM_I_INSIDE,0);

}

public string
exits_description()
{
    return "^Scie^zka prowadzi na p^o^lnocny wsch^od, na po^ludniowym "+
    "zachodzie znajduje si^e za^s trakt.\n";
}

string
dlugi_opis()
{
    string str = "";

    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="Trakt jest bardzo zaniedbany, pe^lno tu zastygni^etych w zma"+
                "rzni^etej ziemi kolein i zasp na kraw^edziach drogi. Okolicz"+
                "ne pola uprawne pokryte s^a bia^lym, g^estym puchem, przykry"+
                "waj^acym wszystko delikatn^a ko^lderk^a. Obok ciebie stoi ro"+
                "ztrzaskane przez piorun drzewo, rozpo^lowione przez nieoblic"+
                "zalny ^zywio^l, kompletnie nagie i przypr^oszone ^sniegiem, "+
                "sprawia niemi^le i smutne wra^zenie. Na zakr^ecie drogi w ro"+
                "wie le^zy kilka kamieni wielko^sci ludzkiej g^lowy, przykryt"+
                "e s^a cienk^a warstw^a ^sniegu. W^aski, zryty koleinami trak"+
                "t wije sie po^sr^od ^lagodnych wzg^orz. ";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str+="Trakt jest bardzo zaniedbany, pe^lno tu kolein i rosn^acych "+
                "prosto na drodze chwast^ow. Okoliczne pola uprawne pokryte s"+
                "^a kie^lkuj^ac^a ro^slinno^sci^a, gdzieniegdzie przebijaj^ac"+
                "^a sie spod cienkich warstewek stopionego ^sniegu. Obok cieb"+
                "ie stoi roztrzaskane przez piorun drzewo, mimo, ^ze piorun r"+
                "ozczepi^l tego kasztana niemal na p^o^l, wypuszcza on nadal "+
                "p^aczki i listki - najwidoczniej uderzenie nie zdo^la^lo zab"+
                "i^c drzewa. Na zakr^ecie drogi w rowie le^zy kilka kamieni w"+
                "ielko^sci ludzkiej g^lowy, nie^smia^lo wspinaj^a si^e po nic"+
                "h m^lode p^edy szybko rosn^acych chwast^ow i innych ro^slin."+
                " W^aski, zryty koleinami trakt wije sie po^sr^od ^lagodnych "+
                "wzg^orz. ";
    }
    else if(pora_roku() == MT_LATO)
    {
        str+="Trakt jest bardzo zaniedbany, pe^lno tu kolein i rozpleniony"+
                "ch na drodze chwast^ow. Okoliczne pola uprawne pokryte s^a b"+
                "ujn^a ro^slinno^sci^a, w ^sr^od kt^orej zapewne kwitnie tak^"+
                "ze ^zycie drobnych zwierz^at. Obok ciebie stoi roztrzaskane "+
                "przez piorun drzewo, mimo, ^ze piorun rozczepi^l tego kaszta"+
                "na niemal na p^o^l, pokryty jest on zielonym listowiem - naj"+
                "widoczniej uderzenie nie zdo^la^lo zabi^c drewa. Na zakr^eci"+
                "e drogi w rowie le^zy kilka kamieni wielko^sci ludzkiej g^lo"+
                "wy, s^a prawie ca^lkowicie zaro^sni^ete przez chwasty i inne"+
                " ro^sliny. W^aski, zryty koleinami trakt wije sie po^sr^od "+
                "^lagodnych wzg^orz. ";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str+="Trakt jest bardzo zaniedbany, pe^lno tu kolein, li^sci i ga^"+
                "l^azek na drodze. Okoliczne pola uprawne pokryte s^a wysuszo"+
                "n^a ro^slinno^sci^a, kt^or^a powoli zaczyna obumiera^c. Obok"+
                " ciebie stoi roztrzaskane przez piorun drzewo, mimo, ^ze pio"+
                "run rozczepi^l tego kasztana niemal na p^o^l, pokryty jest o"+
                "n ^z^o^ltymi i czerwonym listowiem - najwidoczniej uderzenie"+
                " nie zdo^la^lo zabi^c drzewa. Na zakr^ecie drogi w rowie le"+
                "^zy kilka kamieni wielko^sci ludzkiej g^lowy, cz^e^sciowo pok"+
                "rywaj^a je wi^edn^ace ju^z chwasty i inne ro^sliny. W^aski, "+
                "zryty koleinami trakt wije sie po^sr^od ^lagodnych wzg^orz. ";
    }

    str+="\n";
    return str;
}