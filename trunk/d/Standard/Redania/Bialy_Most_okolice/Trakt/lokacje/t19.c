/*
 * Opis oraz kod automatycznie wygenerowany za pomoca VattGen,
 * Data: Saturday 16th of June 2007 06:45:41 PM
 */

#include <pogoda.h>
#include <stdproperties.h>
#include <mudtime.h>
#include "dir.h"

inherit TRAKT_BIALY_MOST_STD;

void
create_trakt()
{
    set_short("Trakt");
    set_long("@@dlugi_opis@@");
    set_polmrok_long("@@mrok@@");
    add_exit(TRAKT_BIALY_MOST_LOKACJE + "t03.c","se",0,FATIGUE_TRAKT,0);
    add_exit(TRAKT_BIALY_MOST_LOKACJE + "t20.c","nw",0,FATIGUE_TRAKT,0);
    add_prop(ROOM_I_INSIDE,0);

}

public string
exits_description()
{
    return "Trakt prowadzi na po^ludniowy-wsch^od i p^o^lnocny-zach^od.\n";
}

string
dlugi_opis()
{
    string str = "";

    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="Tylko w najbli^zszym s^asiedztwie utwardzonej drogi pokryte "+
             "^sniegowym puchem ro^sliny nie potrafi^a przetrwa^c i wybi^c"+
             " si^e nad ziemi^e, a wyj^atki kt^orym to si^e uda^lo zosta^l"+
             "y zadeptane przez licznych w^edrowc^ow korzystaj^acych z teg"+
             "o traktu. Wok^o^l jak okiem si^egn^a^c rozpo^sciera sie pofa"+
             "^ldowany ^lagodnymi wzg^orzami krajobraz. Trakt jest wy^z^lo"+
             "biony ko^lami pojazd^ow, wida^c na nim tak^ze ^slady kopyt i"+
             " podk^ow. ";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str+="Niska, polna ro^slinno^s^c, przerzedza si^e w miar^e przybli"+
             "^zania do go^sci^nca, za^s ziele^n rosn^aca w najbli^zszym s"+
             "^asiedztwie drogi wydaje si^e marna i niedojrza^la, a ju^z n"+
             "a pewno zadeptana. Wok^o^l jak okiem si^egn^a^c rozpo^sciera"+
             " sie pofa^ldowany ^lagodnymi wzg^orzami krajobraz. Trakt jes"+
             "t wy^z^lobiony ko^lami pojazd^ow, wida^c na nim tak^ze ^slad"+
             "y kopyt i podk^ow. ";
    }
    else if(pora_roku() == MT_LATO)
    {
        str+="Tylko w najbli^zszym s^asiedztwie utwardzonej drogi ro^sliny"+
             " nie potrafi^a przetrwa^c i wybi^c si^e nad ziemi^e, a wyj^a"+
             "tki kt^orym to si^e uda^lo zosta^ly zadeptane przez licznych"+
             " w^edrowc^ow korzystaj^acych z tego traktu. Wok^o^l jak okie"+
             "m si^egn^a^c rozpo^sciera sie pofa^ldowany ^lagodnymi wzg^or"+
             "zami krajobraz. Trakt jest wy^z^lobiony ko^lami pojazd^ow, w"+
             "ida^c na nim tak^ze ^slady kopyt i podk^ow. ";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str+="Niska, polna ro^slinno^s^c, przerzedza si^e w miar^e przybli"+
             "^zania do go^sci^nca, za^s ziele^n rosn^aca w najbli^zszym s"+
             "^asiedztwie drogi wydaje si^e marna i niedojrza^la, a ju^z n"+
             "a pewno zadeptana. Wok^o^l jak okiem si^egn^a^c rozpo^sciera"+
             " sie pofa^ldowany ^lagodnymi wzg^orzami krajobraz. Trakt jes"+
             "t wy^z^lobiony ko^lami pojazd^ow, wida^c na nim tak^ze ^slad"+
             "y kopyt i podk^ow. ";
    }

    str+="\n";
    return str;
}

string
mrok()
{
    string str = "";

    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="Tylko w najbli^zszym s^asiedztwie utwardzonej drogi pokryte "+
             "^sniegowym puchem ro^sliny nie potrafi^a przetrwa^c i wybi^c"+
             " si^e nad ziemi^e, a wyj^atki kt^orym to si^e uda^lo zosta^l"+
             "y zadeptane przez licznych w^edrowc^ow korzystaj^acych z teg"+
             "o traktu. Wok^o^l jak okiem si^egn^a^c rozpo^sciera sie pofa"+
             "^ldowany ^lagodnymi wzg^orzami krajobraz. Trakt jest wy^z^lo"+
             "biony ko^lami pojazd^ow, wida^c na nim tak^ze ^slady kopyt i"+
             " podk^ow. ";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str+="Niska, polna ro^slinno^s^c, przerzedza si^e w miar^e przybli"+
             "^zania do go^sci^nca, za^s ziele^n rosn^aca w najbli^zszym s"+
             "^asiedztwie drogi wydaje si^e marna i niedojrza^la, a ju^z n"+
             "a pewno zadeptana. Wok^o^l jak okiem si^egn^a^c rozpo^sciera"+
             " sie pofa^ldowany ^lagodnymi wzg^orzami krajobraz. Trakt jes"+
             "t wy^z^lobiony ko^lami pojazd^ow, wida^c na nim tak^ze ^slad"+
             "y kopyt i podk^ow. ";
    }
    else if(pora_roku() == MT_LATO)
    {
        str+="Tylko w najbli^zszym s^asiedztwie utwardzonej drogi ro^sliny"+
             " nie potrafi^a przetrwa^c i wybi^c si^e nad ziemi^e, a wyj^a"+
             "tki kt^orym to si^e uda^lo zosta^ly zadeptane przez licznych"+
             " w^edrowc^ow korzystaj^acych z tego traktu. Wok^o^l jak okie"+
             "m si^egn^a^c rozpo^sciera sie pofa^ldowany ^lagodnymi wzg^or"+
             "zami krajobraz. Trakt jest wy^z^lobiony ko^lami pojazd^ow, w"+
             "ida^c na nim tak^ze ^slady kopyt i podk^ow. ";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str+="Niska, polna ro^slinno^s^c, przerzedza si^e w miar^e przybli"+
             "^zania do go^sci^nca, za^s ziele^n rosn^aca w najbli^zszym s"+
             "^asiedztwie drogi wydaje si^e marna i niedojrza^la, a ju^z n"+
             "a pewno zadeptana. Wok^o^l jak okiem si^egn^a^c rozpo^sciera"+
             " sie pofa^ldowany ^lagodnymi wzg^orzami krajobraz. Trakt jes"+
             "t wy^z^lobiony ko^lami pojazd^ow, wida^c na nim tak^ze ^slad"+
             "y kopyt i podk^ow. ";
    }

    str+="\n";
    return str;
}