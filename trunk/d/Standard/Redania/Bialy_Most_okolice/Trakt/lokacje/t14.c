
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"
#include <pogoda.h>

inherit TRAKT_BIALY_MOST_STD;

void create_trakt() 
{
    set_short("Ubity trakt");
    set_long("@@dlugasny@@");
    add_exit(TRAKT_BIALY_MOST_LOKACJE + "t15.c","e",0,FATIGUE_TRAKT,0);
    add_exit(TRAKT_BIALY_MOST_LOKACJE + "t13.c","nw",0,FATIGUE_TRAKT,0);

    add_prop(ROOM_I_INSIDE,0);
}
public string
exits_description() 
{
    return "Trakt prowadzi na p�nocny-zach�d i na wsch�d.\n";
}

string
dlugasny()
{
	string str="";

	str+="Trakt wiedzie od po�o�onego na wschodzie miasta, ledwo "+
	"widocznego z tej odleg�o�ci, a� po niewielk� mie�cin� na zachodzie. ";

	if(CZY_JEST_WODA(TO))
		str+="Wy�o�on� dopasowanymi kamieniami powierzchni� traktu "+
		"pokrywa gruba warstwa brunatnego b�ocka. ";
	else
		str+="Wy�o�on� dopasowanymi kamieniami powierzchni� traktu "+
		"pokrywa gruba warstwa gruba warstwa zaskorupia�ego b�ocka. ";


	if(dzien_noc())
	{
		if(ZACHMURZENIE(TO) <= POG_ZACH_SREDNIE)
			str+="Jasna krecha biegn�cej w kierunku p�nocnym piaszczystej, "+
			"niezbyt szerokiej �cie�ki oddziela rozci�gaj�ce si� na zach�d "+
			"od niej l�ni�ce w blasku ksi�yca pola uprawne od ciemnej "+
			"zieleni lasu pokrywaj�cej obszary na wschodzie. ";
		else
			str+="Jasna krecha biegn�cej w kierunku p�nocnym piaszczystej, "+
			"niezbyt szerokiej �cie�ki oddziela rozci�gaj�ce si� na zach�d "+
			"od niej ciemne pola uprawne od lasu pokrywaj�cego obszary na "+
			"wschodzie. ";
	}
	else
	{
		if(pora_roku() == MT_ZIMA)
			str+="Jasna krecha biegn�cej w kierunku p�nocnym piaszczystej, "+
			"niezbyt szerokiej �cie�ki oddziela rozci�gaj�ce si� na zach�d "+
			"od niej poro�ni�te jasnozielon� ozimin� od ciemnej zieleni "+
			"lasu pokrywaj�cej obszary na wschodzie. ";
		else if(pora_roku() == MT_WIOSNA)
			str+="Jasna krecha biegn�cej w kierunku p�nocnym piaszczystej, "+
			"niezbyt szerokiej �cie�ki oddziela rozci�gaj�ce si� na zach�d "+
			"od niej �wie�o zaorane pola uprawne od ciemnej zieleni "+
			"lasu pokrywaj�cej obszary na wschodzie. ";
		else if(pora_roku() == MT_LATO)
			str+="Jasna krecha biegn�cej w kierunku p�nocnym piaszczystej, "+
			"niezbyt szerokiej �cie�ki oddziela rozci�gaj�ce si� na zach�d "+
			"od niej faluj�ce z�ocistym �anem dojrza�ego zbo�a od ciemnej zieleni lasu pokrywaj�cej obszary na wschodzie. ";
		else
			str+="Jasna krecha biegn�cej w kierunku p�nocnym piaszczystej, "+
			"niezbyt szerokiej �cie�ki oddziela rozci�gaj�ce si� na zach�d "+
			"od niej ciemne pola uprawne od lasu pokrywaj�cego obszary na "+
			"wschodzie. ";
	}


	str+="\n";
	return str;
}
