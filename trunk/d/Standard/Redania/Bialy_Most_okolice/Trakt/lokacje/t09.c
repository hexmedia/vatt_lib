
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"
#include <pogoda.h>

inherit TRAKT_BIALY_MOST_STD;

void create_trakt() 
{
    set_short("Trakt po�r�d ��k");
    set_long("@@dlugasny@@");
    add_exit(TRAKT_BIALY_MOST_LOKACJE + "t08.c","w",0,FATIGUE_TRAKT,0);
    add_exit(TRAKT_BIALY_MOST_LOKACJE + "t10.c","n",0,FATIGUE_TRAKT,0);

    add_prop(ROOM_I_INSIDE,0);
}
public string
exits_description() 
{
    return "Trakt ci�gnie si� na zach�d oraz na p�noc.\n";
}

string
dlugasny()
{
	string str="";


	str+="Trakt wij�cy si� nad brzegiem rzeki jest na tyle szeroki, i� "+
	"bez najmniejszego trudu mog� wymin�� si� na nim nie tylko "+
	"podr�ni przemierzaj�cy szlaki na w�asnych nogach, ale i "+
	"tak�e wi�ksze, wy�adowane towarami wozy. ";

	if(MOC_WIATRU(TO) > POG_WI_SREDNIE)
	{
		if(pora_roku() == MT_WIOSNA)
			str+="Tocz�ce si� z w�ciek�ym rykiem wody Pontaru cz�ciowo "+
			"zas�aniaj� rosn�ce po po�udniowej stronie traktu niewysokie "+
			"spl�tane, zaro�la pokryte delikatnymi bia�ymi kwiatami. ";
		else if(pora_roku() == MT_LATO)
			str+="Tocz�ce si� z w�ciek�ym rykiem wody Pontaru cz�ciowo "+
			"zas�aniaj� rosn�ce po po�udniowej stronie traktu niewysokie "+
			"spl�tane, zaro�la pokryte jakimi� owocami. ";
		else
			str+="Tocz�ce si� z w�ciek�ym rykiem wody Pontaru cz�ciowo "+
			"zas�aniaj� rosn�ce po po�udniowej stronie traktu niewysokie "+
			"spl�tane, zaro�la pokryte mgie�k� zielonych li�ci. ";
	}
	else
	{
		if(pora_roku() == MT_WIOSNA)
			str+="Leniwie p�yn�ce wody Pontaru cz�ciowo zas�aniaj� rosn�ce "+
			"po po�udniowej stronie traktu niewysokie spl�tane, zaro�la "+
			"pokryte delikatnymi bia�ymi kwiatami. ";
		else if(pora_roku() == MT_LATO)
			str+="Leniwie p�yn�ce wody Pontaru cz�ciowo zas�aniaj� rosn�ce "+
			"po po�udniowej stronie traktu niewysokie spl�tane, zaro�la "+
			"pokryte jakimi� owocami. ";
		else
			str+="Leniwie p�yn�ce wody Pontaru cz�ciowo zas�aniaj� rosn�ce "+
			"po po�udniowej stronie traktu niewysokie spl�tane, zaro�la "+
			"pokryte mgie�k� zielonych li�ci. ";
	}



	str+="\n";
	return str;
}
