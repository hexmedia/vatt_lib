
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"
#include <pogoda.h>

inherit TRAKT_BIALY_MOST_STD;

void create_trakt() 
{
    set_short("Przy wej^sciu do miasta");
    set_long("@@dlugasny@@");
    add_exit(TRAKT_BIALY_MOST_LOKACJE + "t02.c","e",0,FATIGUE_TRAKT,0);
	add_exit(BRZEG_BIALY_MOST_LOKACJE + "b04.c","s",0,6,0);
	add_exit(BIALY_MOST_LOKACJE + "ulice/u01.c",({"w","do miasta","z traktu"}),0,8,0);

    add_prop(ROOM_I_INSIDE,0);
}
public string
exits_description() 
{
    return "Trakt ci�gnie si� na wsch�d, za� na zachodzie wej�cie do miasta, "+
			"a id�c na po�udnie dotrzesz do brzegu w�d Pontaru.\n";
}


string
dlugasny()
{
	string str="";

	str+="Drog� wy�o�ono g�adkimi kamieniami, niestety nawierzchnia "+
	"lata swojej �wietno�ci ma ju� za sob�. Ju� na pierwszy rzut "+
	"oka dostrzec mo�na na niej licznie zag��bienia, nier�wno�ci i "+
	"koleiny �wiadcz�ce o tym, i� droga od dawna nie by�a remontowana. ";



	str+="\n";
	return str;
}
