/*
 * Opis oraz kod automatycznie wygenerowany za pomoca VattGen,
 * Data: Friday 02nd of November 2007 10:27:51 PM
 */

#include <pogoda.h>
#include <stdproperties.h>
#include <mudtime.h>
#include "dir.h"

inherit TRAKT_DO_PIANY_STD;

void
create_trakt()
{
    set_short("Trakt w^sr^od ^l^ak");
    set_long("@@dlugi_opis@@");
    add_exit(TRAKT_BIALY_MOST_LOKACJE + "t41.c","ne",0,FATIGUE_TRAKT,0);
    add_exit(TRAKT_BIALY_MOST_LOKACJE + "t39.c","s",0,FATIGUE_TRAKT,0);
    add_prop(ROOM_I_INSIDE,0);

}

public string
exits_description()
{
    return "Trakt prowadzi na p^o^lnocny wsch^od i po^ludnie.\n";
}

string
dlugi_opis()
{
    string str = "";

    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="Pomi^edzy kamykami po^lo^zonymi, by wzmocni^c nawierzchnie t"+
             "raktu wida^c pozamarzane ka^lu^ze. Droga, kt^or^a w^edrujesz"+
             ", przebiega zygzakami miedzy wzg^orzami, omijaj^ac wi^eksze "+
             "wzniesienia i doliny. . Na tle bia^lej okolicy trakt odcina"+
             " si^e wyra^xnie, ciemny, ubity i twardy, pokryty koleinami w"+
             "oz^ow, tysi^acem ^slad^ow ko^nskich kopyt i ludzkich st^op. ";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str+="Pomi^edzy kamykami po^lo^zonymi, by wzmocni^c nawierzchnie t"+
             "raktu, zaczyna kie^lkowa^c szorstka trawa i jakie^s inne pos"+
             "polite zielska. Droga, kt^or^a w^edrujesz, przebiega zygzaka"+
             "mi miedzy wzg^orzami, omijaj^ac wi^eksze wzniesienia i dolin"+
             "y. Wzd^lu^z traktu ci^agnie si^e niezbyt g^l^eboki r^ow poro"+
             "^sni^ety traw^a. Na tle jasnej zieleni trakt odcina si^e wyr"+
             "a^xnie, ciemny, ubity i twardy, pokryty koleinami woz^ow, ty"+
             "si^acem ^slad^ow ko^nskich kopyt i ludzkich st^op. ";
    }
    else if(pora_roku() == MT_LATO)
    {
        str+="Pomi^edzy kamykami po^lo^zonymi, by wzmocni^c nawierzchnie t"+
             "raktu, ro^snie bujnie szorstka trawa i jakie^s inne pospolit"+
             "e zielska. Droga, kt^or^a w^edrujesz, przebiega zygzakami mi"+
             "edzy wzg^orzami, omijaj^ac wi^eksze wzniesienia i doliny. Wz"+
             "d^lu^z traktu ci^agnie si^e niezbyt g^l^eboki r^ow poro^sni^"+
             "ety traw^a. Na tle jasnej zieleni trakt odcina si^e wyra^xni"+
             "e, ciemny, ubity i twardy, pokryty koleinami woz^ow, tysi^ac"+
             "em ^slad^ow ko^nskich kopyt i ludzkich st^op. ";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str+="Pomi^edzy kamykami po^lo^zonymi, by wzmocni^c nawierzchnie t"+
             "raktu, ro^snie trawa i jakie^s inne wysuszone, pospolite zie"+
             "lska. Droga, kt^or^a w^edrujesz, przebiega zygzakami miedzy "+
             "wzg^orzami, omijaj^ac wi^eksze wzniesienia i doliny. Wzd^lu^"+
             "z traktu ci^agnie si^e niezbyt g^l^eboki r^ow poro^sni^ety t"+
             "raw^a. Na tle szaro^sci trakt odcina si^e wyra^xnie, ciemny,"+
             " ubity i twardy, pokryty koleinami woz^ow, tysi^acem ^slad^o"+
             "w ko^nskich kopyt i ludzkich st^op. ";
    }

    str+="\n";
    return str;
}