/*
 * Opis oraz kod automatycznie wygenerowany za pomoca VattGen,
 * Data: Thursday 18th of June 2009 03:59:59 PM
 */

#include <pogoda.h>
#include <stdproperties.h>
#include <mudtime.h>
#include <macros.h>
#include "dir.h"

inherit TRAKT_BIALY_MOST_STD;

int wez(string str);
string opis_leszczyny();

void
create_trakt()
{
    set_short("Tu^z przy m^lynie");
    set_long("@@dlugi_opis@@");
    add_exit(TRAKT_BIALY_MOST_LOKACJE + "sciezka2.c","s",0,FATIGUE_TRAKT,0);
    add_door(MLYN_DRZWI + "do_mlyna.c");
    add_prop(ROOM_I_INSIDE,0);

    add_item(({"m�yn","stary m�yn","zniszczony m�yn","budynek","stary budynek",
    "zniszczony budynek"}),"Ten stary, opuszczony budynek zdaje si� wr�cz "+
    "emanowa� �alem czy t�sknot� za dawnym w�a�cicielem. Takie odczucia "+
    "sprawia� mog� sm�tnie wy�amane drzwi, puste okna czy ponuro zwisaj�ce "+
    "ramiona wiatraka, kt�re wygl�daj� zupe�nie, jakby opad�y z si� w walce "+
    "z nieub�agana natur�. To ona sprawia, �e m�yn raz po razie wydaje z "+
    "siebie cichy j�k wywo�any przeci�gami, szw�daj�cymi sie pomiedzy "+
    "wszelkimi szczelnami, jakich ten m�yn posiada wiele. Kilka drewnianych "+
    "stopni, kt�re niegdy� prowadzi�y do wej�cia m�yna, teraz przypomina "+
    "zwyk�� stert� opa�u: zwa� spruchnia�ych i po�amanych desek.\n");
    
    add_item(({"leszczyn�","krzak","krzew","krzak leszczyny",
        "krzew leszczyny"}), &opis_leszczyny());

}

public string
exits_description()
{
    return "^Scie^zka prowadzi na po^ludnie, znajduje sie tu tak^ze wej^"+
            "scie do m^lyna.\n";
}

string
dlugi_opis()
{
    string str = "";

    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="Szeroki, ubity trakt pokrywa gruba warstwa puszystego ^snieg"+
                "u. Zamarzni^ete b^loto pokrywa ca^lkowicie ziemi^e, tworz^ac"+
                " ^slisk^a nawierzchni^e. Przydro^zne k^epy, pokryte drobniut"+
                "kimi sopelkami i ^sniegiem, leszczyn rosn^a coraz g^e^sciej "+
                "si^egaj^ac wiotkimi ga^l^azkami na drog^e. Ca^la okolica usi"+
                "ana jest du^zymi kwadratami p^ol uprawnych oraz ^l^ak, wykor"+
                "zystywanych przez okolicznych mieszka^nc^ow. Na lekkim wznie"+
                "sieniu stoi stary, opuszczony m^lyn.";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str+="Szeroki, ubity trakt poro^sni^ety jest gdzieniegdzie wznosz^"+
                "acymi si^e ro^slinami. Grz^askie b^loto pokrywa ca^lkowicie "+
                "ziemi^e, utrudniaj^ac drog^e. Przydro^zne k^epy, pokryte m^l"+
                "odymi li^s^cmi, leszczyn rosn^a coraz g^e^sciej si^egaj^ac w"+
                "iotkimi ga^l^azkami na drog^e. Ca^la okolica usiana jest du^"+
                "zymi kwadratami p^ol uprawnych oraz ^l^ak, wykorzystywanych "+
                "przez okolicznych mieszka^nc^ow. Na lekkim wzniesieniu stoi "+
                "stary, opuszczony m^lyn.";
    }
    else if(pora_roku() == MT_LATO)
    {
        str+="Szeroki, ubity trakt porasta miejscami soczysta, zielona tra"+
                "wa. Grz^askie b^loto pokrywa ca^lkowicie ziemi^e, utrudniaj^"+
                "ac drog^e. Przydro^zne k^epy, pokryte li^s^cmi i zielonymi o"+
                "rzechami, leszczyn rosn^a coraz g^e^sciej si^egaj^ac wiotkim"+
                "i ga^l^azkami na drog^e. Ca^la okolica usiana jest du^zymi k"+
                "wadratami p^ol uprawnych oraz ^l^ak, wykorzystywanych przez "+
                "okolicznych mieszka^nc^ow. Na lekkim wzniesieniu stoi stary,"+
                " opuszczony m^lyn.";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str+="Szeroki, ubity trakt pokryty jest stert^a r^o^znobarwnych li"+
                "^sci. Grz^askie b^loto pokrywa ca^lkowicie ziemi^e, utrudnia"+
                "j^ac drog^e. Przydro^zne k^epy, pokryte ^z^o^ltymi li^s^cmi "+
                "i dojrza^lymi orzechami, leszczyn rosn^a coraz g^e^sciej si^"+
                "egaj^ac wiotkimi ga^l^azkami na drog^e. Ca^la okolica usiana"+
                " jest du^zymi kwadratami p^ol uprawnych oraz ^l^ak, wykorzys"+
                "tywanych przez okolicznych mieszka^nc^ow. Na lekkim wzniesie"+
                "niu stoi stary, opuszczony m^lyn.";
    }

    str+="\n";
    return str;
}

string
opis_leszczyny()
{
    string str;
    if(CZY_JEST_SNIEG(this_object))
    {
        str = "est to niewysoka forma drzewiasta krzewu o �redniej wielko�ci, "+
        "kt�rego pie� w barwie szarobr�zowej do�� g�sto przetykany jest "+
        "bia�awymi przetchlinkami, kt�re s� jedynym elementem naruszaj�cym "+
        "niesamowit� g�adko�� kory. Obecnie na krzewie dostrzegasz tylko "+
        "pojedyncze, obumar�e li�cie, kt�re nie spad�y na jesieni, a teraz "+
        "trzymaj� si� tylko przez dzia�anie mrozu.\n";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str = "Jest to niewysoka forma drzewiasta krzewu o �redniej "+
        "wielko�ci, kt�rego pie� w barwie szarobr�zowej do�� g�sto "+
        "przetykany jest bia�awymi przetchlinkami, kt�re s� jedynym "+
        "elementem naruszaj�cym niesamowit� g�adko�� kory. Krzew ten bogaty "+
        "jest w do�� du�e, okr�g�awe li�cie, u nasady sercowate, kr�tko "+
        "zaostrzone, o barwie soczystej zieleni, aktualnie g�sto przetykany "+
        "br�zowo ��tymi kotami, kt�re zwisaj� z ga��zi wystawione na "+
        "dzia�anie wiatru rozsiewaj�cego py�ek po okolicy.\n";
    }
    else if(pora_roku() == MT_LATO)
    {
        str = "Jest to niewysoka forma drzewiasta krzewu o �redniej "+
        "wielko�ci, kt�rego pie� w barwie szarobr�zowej do�� g�sto "+
        "przetykany jest bia�awymi przetchlinkami, kt�re s� jedynym "+
        "elementem naruszaj�cym niesamowit� g�adko�� kory. Krzew ten bogaty "+
        "jest w do�� du�e, okr�g�awe li�cie, u nasady sercowate, kr�tko "+
        "zaostrzone, o barwie soczystej zieleni.\n";
    }
    else
    {  
        str = "Jest to niewysoka forma drzewiasta krzewu o �redniej "+
        "wielko�ci, kt�rego pie� w barwie szarobr�zowej do�� g�sto "+
        "przetykany jest bia�awymi przetchlinkami, kt�re s� jedynym "+
        "elementem naruszaj�cym niesamowit� g�adko�� kory. Krzew ten bogaty "+
        "jest w do�� du�e, okr�g�awe li�cie, u nasady sercowate, kr�tko "+
        "zaostrzone, o barwie ��tozielonej. Mi�dzy nimi dostrzec mo�na "+
        "pojedyncze orzeszki.\n";
    }
    return str;
}

int wez(string str)
{
    if(query_verb() ~= "we^x")
        notify_fail("We^x co?\n");
    if(query_verb() ~= "zbieraj")
        notify_fail("Zbieraj co?\n");
    if(query_verb() ~= "zbierz")
        notify_fail("Zbierz co?\n");
        
    if (!str) return 0;
    if (!strlen(str)) return 0;
    
    if(pora_roku() == MT_ZIMA)
        return 0;
    
    if(query_verb() ~= "we^x")
    {
        if(!parse_command(str,environment(TP),"'orzech' / 'orzechy'"))
            return 0;
    }
    else 
    {
        if(!parse_command(str,environment(TP),"'orzechy'"))
            return 0;
    }
    write("Zaczynasz zbiera� orzechy.\n");
    saybb(QCIMIE(TP,PL_MIA)+" zaczyna zbiera� orzechy.\n");
    clone_object(TRAKT_BIALY_MOST_OKOLICE+"obiekty/paraliz_orzechy_laskowe.c")->move(TP);
    return 1;
}