/*
 * Opis oraz kod automatycznie wygenerowany za pomoca VattGen,
 * Data: Saturday 16th of June 2007 06:45:41 PM
 */

#include <pogoda.h>
#include <stdproperties.h>
#include <mudtime.h>
#include "dir.h"

inherit TRAKT_BIALY_MOST_STD;

void
create_trakt()
{
    set_short("Trakt w^sr^od p^ol");
    set_long("@@dlugi_opis@@");
    add_exit(TRAKT_BIALY_MOST_LOKACJE + "t19.c","se",0,FATIGUE_TRAKT,0);
    add_exit(TRAKT_BIALY_MOST_LOKACJE + "t21.c","nw",0,FATIGUE_TRAKT,0);
    add_prop(ROOM_I_INSIDE,0);

}

public string
exits_description()
{
    return "Trakt prowadzi na po^ludniowy-wsch^od i p^o^lnocny-zach^od.\n";
}

string
dlugi_opis()
{
    string str = "";

    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="Ca^la okolica usiana jest du^zymi kwadratami p^ol uprawnych "+
             "oraz ^l^ak, wykorzystywanych przez okolicznych mieszka^nc^ow"+
             ". Pola uprawne rozci^agaj^ace si^e dooko^la traktu, pokrywa "+
             "^snie^znobia^ly puch po kt^orym od czasu do czasu przebiega "+
             "jakie^s ma^le zwierz^atko. ^L^aki i pola uprawne, pokryte sr"+
             "ebrzystobia^lym puchem, wygl^adaj^a na opuszczone przez praw"+
             "ie wszelak^a zwierzyn^e. ";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str+="Ca^la okolica usiana jest du^zymi kwadratami p^ol uprawnych "+
             "oraz ^l^ak, wykorzystywanych przez okolicznych mieszka^nc^ow"+
             ". Pola uprawne rozci^agaj^ace si^e dooko^la traktu, pokrywa "+
             "delikatna m^loda ziele^n, w kt^orej buszuj^a obudzone z zimo"+
             "wego snu zwierz^eta. ^L^aki i pola uprawne, pokryte kie^lkuj"+
             "^aca ro^slinno^sci^a, zaczynaj^a zape^lnia^c si^e budz^ac^a "+
             "si^e do ^zycia faun^a. ";
    }
    else if(pora_roku() == MT_LATO)
    {
        str+="Ca^la okolica usiana jest du^zymi kwadratami p^ol uprawnych "+
             "oraz ^l^ak, wykorzystywanych przez okolicznych mieszka^nc^ow"+
             ". Pola uprawne rozci^agaj^ace si^e dooko^la traktu, pokrywa "+
             "ciemnozielona bujna ro^slinno^s^c, w kt^orej buszuj^a obudzo"+
             "ne z zimowego snu zwierz^eta. ^L^aki i pola uprawne, pokryte"+
             " bujnymi plonami, s^a wype^lnione d^xwi^ekami przer^o^znych "+
             "zwierz^at. ";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str+="Ca^la okolica usiana jest du^zymi kwadratami p^ol uprawnych "+
             "oraz ^l^ak, wykorzystywanych przez okolicznych mieszka^nc^ow"+
             ". Pola uprawne rozci^agaj^ace si^e dooko^la traktu, s^a zaor"+
             "ane i przygotowane do zimy, a od czasu do czasu mo^zna zauwa"+
             "^zy^c jak przebiega po nich jakie^s drobne zwierz^atko. ^L^a"+
             "ki i pola uprawne, ogo^locone z ro^slinno^sci, powoli cichn^"+
             "a wraz ze zwierz^etami k^lad^acymi si^e do zimowego snu. ";
    }

    str+="\n";
    return str;
}