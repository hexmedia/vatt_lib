/*
 * Opis oraz kod automatycznie wygenerowany za pomoca VattGen,
 * Data: Friday 02nd of November 2007 11:06:29 PM
 */

#include <pogoda.h>
#include <stdproperties.h>
#include <mudtime.h>
#include "dir.h"

inherit TRAKT_DO_PIANY_STD;

void
create_trakt()
{
    set_short("Trakt w^sr^od ^l^ak");
    set_long("@@dlugi_opis@@");
    add_exit(TRAKT_BIALY_MOST_LOKACJE + "t41.c","s",0,FATIGUE_TRAKT,0);
    add_exit(TRAKT_BIALY_MOST_LOKACJE + "t43.c","n",0,FATIGUE_TRAKT,0);
    add_prop(ROOM_I_INSIDE,0);

}

public string
exits_description()
{
    return "Trakt prowadzi na p^o^lnoc i po^ludnie.\n";
}

string
dlugi_opis()
{
    string str = "";

    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="Znajdujesz si^e na szerokim trakcie, wybudowanym na zboczu n"+
             "iezbyt wysokiego wzg^orza, kt^ore w tym momencie jest pokryt"+
             "e bia^la pierzyn^a ze ^sniegu. Pomi^edzy kamykami po^lo^zony"+
             "mi, by wzmocni^c nawierzchnie traktu wida^c pozamarzane ka^l"+
             "u^ze. . Szeroki, ubity trakt pokrywa gruba warstwa puszyste"+
             "go ^sniegu. ";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str+="Znajdujesz si^e na szerokim trakcie, wybudowanym na zboczu n"+
             "iezbyt wysokiego wzg^orza, kt^ore w tym momencie jest pokryt"+
             "e drobn^a, jasnozielon^a traw^a. Pomi^edzy kamykami po^lo^zo"+
             "nymi, by wzmocni^c nawierzchnie traktu, zaczyna kie^lkowa^c "+
             "szorstka trawa i jakie^s inne pospolite zielska. Wzd^lu^z tr"+
             "aktu ci^agnie si^e niezbyt g^l^eboki r^ow poro^sni^ety traw^"+
             "a. Szeroki, ubity trakt poro^sni^ety jest gdzieniegdzie wzno"+
             "sz^acymi si^e ro^slinami. ";
    }
    else if(pora_roku() == MT_LATO)
    {
        str+="Znajdujesz si^e na szerokim trakcie, wybudowanym na zboczu n"+
             "iezbyt wysokiego wzg^orza, kt^ore w tym momencie jest pokryt"+
             "e g^est^a, ciemnozielon^a traw^a. Pomi^edzy kamykami po^lo^z"+
             "onymi, by wzmocni^c nawierzchnie traktu, ro^snie bujnie szor"+
             "stka trawa i jakie^s inne pospolite zielska. Wzd^lu^z traktu"+
             " ci^agnie si^e niezbyt g^l^eboki r^ow poro^sni^ety traw^a. S"+
             "zeroki, ubity trakt porasta miejscami soczysta, zielona traw"+
             "a. ";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str+="Znajdujesz si^e na szerokim trakcie, wybudowanym na zboczu n"+
             "iezbyt wysokiego wzg^orza, kt^ore w tym momencie jest pokryt"+
             "e drobn^a, wysuszon^a traw^a. Pomi^edzy kamykami po^lo^zonym"+
             "i, by wzmocni^c nawierzchnie traktu, ro^snie trawa i jakie^s"+
             " inne wysuszone, pospolite zielska. Wzd^lu^z traktu ci^agnie"+
             " si^e niezbyt g^l^eboki r^ow poro^sni^ety traw^a. Szeroki, u"+
             "bity trakt pokryty jest stert^a r^o^znobarwnych li^sci. ";
    }

    str+="\n";
    return str;
}