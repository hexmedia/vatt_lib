/*
 * Opis oraz kod automatycznie wygenerowany za pomoca VattGen,
 * Data: Friday 02nd of November 2007 03:34:06 PM
 */

#include <pogoda.h>
#include <stdproperties.h>
#include <mudtime.h>
#include "dir.h"

inherit TRAKT_DO_PIANY_STD;

void
create_trakt()
{
    set_short("Trakt w^sr^od p^ol");
    set_long("@@dlugi_opis@@");
    add_exit(TRAKT_BIALY_MOST_LOKACJE + "t27.c","se",0,FATIGUE_TRAKT,0);
    add_exit(TRAKT_BIALY_MOST_LOKACJE + "t29.c","n",0,FATIGUE_TRAKT,0);
    add_prop(ROOM_I_INSIDE,0);

}

public string
exits_description()
{
    return "Trakt prowadzi na p^o^lnoc i po^ludniowy wsch^od.\n";
}

string
dlugi_opis()
{
    string str = "";

    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="Ca^le pole przykrywa gruba warstwa bia^lego puchu. Prawie ws"+
             "zystko przykryte jest spor^a warstw^a bielutkiego ^sniegu, k"+
             "t^ory przysypa^l okolic^e. W stertach pokruszonych kamieni w"+
             "znosz^acych si^e po obu stronach drogi z trudem mo^zna rozpo"+
             "zna^c pozosta^lo^sci po niewysokich kamiennych murkach zbudo"+
             "wanych niegdy^s z bry^l piaskowca, a obecnie zniszczonych pr"+
             "zez wiatry i deszcze, poro^sni^etych przez ma^le krzaczki i "+
             "ruderalne zielsko. ^Sci^o^lk^e na le^snej ^scie^zce pokrywa "+
             "zmarzni^ete b^loto, kt^ore jest pokryte miejscami ^sniegiem."+
             " Na zboczach traktu znajduj^a si^e krzewy bez li^sci, pokryt"+
             "e ^sniegiem. ";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str+="Pole uprawne niedawno zosta^lo zorane i zasiane zbo^zem. Ota"+
             "czaj^aca ci^e przyroda budzi sie do ^zycia po zimowym ^snie,"+
             " a lekki wietrzyk przynosi do ciebie ^swie^zy zapach ro^slin"+
             ". W stertach pokruszonych kamieni wznosz^acych si^e po obu s"+
             "tronach drogi z trudem mo^zna rozpozna^c pozosta^lo^sci po n"+
             "iewysokich kamiennych murkach zbudowanych niegdy^s z bry^l p"+
             "iaskowca a obecnie zniszczonych przez wiatry i deszcze, poro"+
             "^sni^etych przez ma^le krzaczki i ruderalne zielsko. ^Sci^o^"+
             "lka na le^snej ^scie^zce us^lana jest ^zywymi kolorami ro^sl"+
             "in. Na zboczach traktu pojawiaj^a si^e s^labo rozwini^ete kw"+
             "iaty polne oraz krzewy. ";
    }
    else if(pora_roku() == MT_LATO)
    {
        str+="Na polu bujnie ro^snie zbo^ze z^lotego koloru. Ca^la przyrod"+
             "a w pe^lnym rozkwicie, zieleni otaczaj^acy ci^e krajobraz, a"+
             " w powietrzu mo^zesz wyczu^c intensywny zapach ro^slin. W st"+
             "ertach pokruszonych kamieni wznosz^acych si^e po obu stronac"+
             "h drogi z trudem mo^zna rozpozna^c pozosta^lo^sci po niewyso"+
             "kich kamiennych murkach zbudowanych niegdy^s z bry^l piaskow"+
             "ca, a obecnie zniszczonych przez wiatry i deszcze, poro^sni^"+
             "etych przez ma^le krzaczki i ruderalne zielsko. ^Sci^o^lka n"+
             "a le^snej ^scie^zce jest sucha od wszechobecnego skwaru. Na "+
             "zboczach traktu wida^c dorodne krzewy oraz barwne kwiaty. ";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str+="Na polu zosta^ly wystaj^ace ko^nc^owki ^sci^etego zbo^za jak"+
             "o ^slad po ^zniwach . Krajobraz spowija lekka mgie^lka i dym"+
             "y z ognisk palonych na polach. W stertach pokruszonych kamie"+
             "ni wznosz^acych si^e po obu stronach drogi z trudem mo^zna r"+
             "ozpozna^c pozosta^lo^sci po niewysokich kamiennych murkach z"+
             "budowanych niegdy^s z bry^l piaskowca a obecnie zniszczonych"+
             " przez wiatry i deszcze, poro^sni^etych przez ma^le krzaczki"+
             " i ruderalne zielsko. ^Sci^o^lka na le^snej ^scie^zce jest p"+
             "odmok^la i podgni^la. Zbocza traktu zape^lniaj^a powoli wi^e"+
             "dn^ace kwiaty oraz krzewy. ";
    }

    str+="\n";
    return str;
}