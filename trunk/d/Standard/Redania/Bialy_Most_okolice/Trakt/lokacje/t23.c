/*
 * Opis oraz kod automatycznie wygenerowany za pomoca VattGen,
 * Data: Saturday 16th of June 2007 06:45:41 PM
 */

#include <pogoda.h>
#include <stdproperties.h>
#include <mudtime.h>
#include "dir.h"

inherit TRAKT_BIALY_MOST_STD;

void
create_trakt()
{
    set_short("Trakt w^sr^od p^ol");
    set_long("@@dlugi_opis@@");
    add_exit(TRAKT_BIALY_MOST_LOKACJE + "t22.c","e",0,FATIGUE_TRAKT,0);
    add_exit(TRAKT_BIALY_MOST_LOKACJE + "t24.c","w",0,FATIGUE_TRAKT,0);
    add_prop(ROOM_I_INSIDE,0);

}

public string
exits_description()
{
    return "Trakt prowadzi na wsch^od i zach^od.\n";
}

string
dlugi_opis()
{
    string str = "";

    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="Ca^la okolica usiana jest du^zymi kwadratami p^ol uprawnych "+
             "oraz ^l^ak, wykorzystywanych przez okolicznych mieszka^nc^ow"+
             ". Szeroki, zas^lany przez ^sniegowy puch oraz brudne b^locko"+
             " trakt ci^agnie si^e ^lagodnymi ^lukami. Na oblodzonej, a gd"+
             "zieniegdzie grz^askiej powierzchni drogi trudno jest stawia^"+
             "c nogi, aby si^e nie zapa^s^c w b^locie, czy nie po^slizgn^a"+
             "^c, a zapewne niejeden wo^xnica, czy je^xdziec przekona^l si"+
             "^e tak^ze, ^ze r^ownie trudno jest sterowa^c w takich warunk"+
             "ach wozem lub wierzchowcem. ";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str+="Ca^la okolica usiana jest du^zymi kwadratami p^ol uprawnych "+
             "oraz ^l^ak, wykorzystywanych przez okolicznych mieszka^nc^ow"+
             ". Szeroki, zas^lany z rzadka drobnymi kamykami trakt ci^agni"+
             "e si^e ^lagodnymi ^lukami. W miernie utwardzonej, grz^askiej"+
             " powierzchni drogi swoje miejsce znalaz^ly liczne koleiny ?"+
             "O pozosta^lo^sci po przeje^zd^zaj^acych t^edy wozach, gdzien"+
             "iegdzie da si^e tak^ze zauwa^zy^c odciski podk^ow, kt^ore wr"+
             "az z koleinami wype^lni^ly si^e wod^a tworz^ac razem rozleg^"+
             "le ka^lu^ze. ";
    }
    else if(pora_roku() == MT_LATO)
    {
        str+="Ca^la okolica usiana jest du^zymi kwadratami p^ol uprawnych "+
             "oraz ^l^ak, wykorzystywanych przez okolicznych mieszka^nc^ow"+
             ". Szeroki, zas^lany z rzadka drobnymi kamykami trakt ci^agni"+
             "e si^e ^lagodnymi ^lukami. W miernie utwardzonej, grz^askiej"+
             " powierzchni drogi swoje miejsce znalaz^ly liczne koleiny ?"+
             "O pozosta^lo^sci po przeje^zd^zaj^acych t^edy wozach, gdzien"+
             "iegdzie da si^e tak^ze zauwa^zy^c odciski podk^ow, kt^ore wr"+
             "az z koleinami wype^lni^ly si^e wod^a tworz^ac razem rozleg^"+
             "le ka^lu^ze. ";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str+="Ca^la okolica usiana jest du^zymi kwadratami p^ol uprawnych "+
             "oraz ^l^ak, wykorzystywanych przez okolicznych mieszka^nc^ow"+
             ". Szeroki, zas^lany z rzadka drobnymi kamykami trakt ci^agni"+
             "e si^e ^lagodnymi ^lukami. W miernie utwardzonej, grz^askiej"+
             " powierzchni drogi swoje miejsce znalaz^ly liczne koleiny ?"+
             "O pozosta^lo^sci po przeje^zd^zaj^acych t^edy wozach, gdzien"+
             "iegdzie da si^e tak^ze zauwa^zy^c odciski podk^ow, kt^ore wr"+
             "az z koleinami wype^lni^ly si^e wod^a tworz^ac razem rozleg^"+
             "le ka^lu^ze. ";
    }

    str+="\n";
    return str;
}