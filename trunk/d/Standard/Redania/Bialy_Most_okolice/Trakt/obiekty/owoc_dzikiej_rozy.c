/*Owoc dzikiej r�y, do zerwania w odpowiedniej lokacji na trakcie
 na E od Bia�ego Mostu. Opis by Faeve 
 
 Vera, Fri Apr 6 03:10:08 2007
 */

#include <macros.h>
#include <stdproperties.h>
inherit "/std/food.c";

create_food()
{
	dodaj_przym("czerwonawy","czerwonawi");
	dodaj_przym("kulisty","kuli�ci");
	ustaw_nazwe("owoc");

	set_long("W zasadzie owoc jest czerwono-pomara�czowy, barwy te "+
	"przeplataj� si� nawzajem, niczym farbki na palecie malarza. "+
	"Owoc jest kulisty, ale od g�ry i do�u lekko sp�aszczony. Z jednej "+
	"strony odstaje zielonkawa szypu�ka, z drugiej za� br�zowawa "+
	"g��wka - pozosta�o�� po kielichu kwiatowym.\n");

	set_amount(5);
	add_prop(OBJ_I_VOLUME,5);
	set_decay_time(5000);
	
	//set_decay_adj(({"sple�nia�y","sple�niali"}));
	
}

//zielarz ma czasem chcice na ten owoc, wiec musimy byc pewni,
//ze to wlasnie ten ;)
int
query_owoc_dzikiej_rozy()
{
    return 1;
}
