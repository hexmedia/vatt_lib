/** 
 * Autor: Avard
 * Info : Parali� nak�adany na gracza przy zbieraniu orzech�w w lesie
 */

#pragma strict_types
#include <macros.h>
#include <pogoda.h>
#include <mudtime.h>
#include <ss_types.h>
#include "dir.h"

inherit "/std/paralyze";

void
create_paralyze()
{
    set_name("zbieram_orzechy_w_lesie_wiewiorek");
    set_fail_message("Nie mo^zesz tego zrobi^c, zbierasz orzechy.\n");
    set_finish_object(this_object());
    set_finish_fun("zebrane_orzechy");
    set_remove_time(3);
    set_block_all();
    setuid();
    seteuid(getuid());
}

void
zebrane_orzechy(object player)
{
	int ile = random(3)+3; //przynajmniej 3, zeby bylo kilka ;)
	write("Uda�o ci si� zebra� kilka orzech�w.\n");
	saybb(QCIMIE(player,PL_MIA)+" zebra�"+player->koncowka("","a")+
    " kilka orzech�w.\n");
	for(int i = 0; i < ile; i++)
		clone_object(TRAKT_BIALY_MOST_OKOLICE+"obiekty/orzech_laskowy")->move(player);
    remove_object();
    return;
}
