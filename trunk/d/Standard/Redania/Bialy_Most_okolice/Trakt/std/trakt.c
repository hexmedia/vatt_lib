
/* Trakt mi�dzy Bia�ym Mostem, a Rinde.
   Vera */

#include "dir.h"

inherit REDANIA_STD;

#include <mudtime.h>
#include <language.h>
#include <stdproperties.h>
#include <pogoda.h>
#include <macros.h>

string opis_polmroku();
string dlugasny();

void
create_trakt()
{
}

string
event_traktu()
{
    return "";
}

string
evencik()
{
    string str;
    switch(pora_roku())
    {
        case MT_LATO:
        case MT_WIOSNA:
        case MT_JESIEN:
            switch(random(9))
            {
                case 0: str="Jaki� drobny py�ek wpad� ci do oka.\n";
                        break;
                case 1: str="Unosz�cy si� dooko�a piach jest do�� uci��liwy.\n";
                        break;
                case 2: str="Gdzie� nad tob� przelecia� jaki� ptak, g�o�no "+
                        "informuj�c o swojej obecno�ci.\n";
                        break;
                case 3: str="W powietrzu unosi si� przyjemny zapach "+
                        "kwiat�w dochodz�cy od strony ��ki.\n";
                        break;
                case 4: str="Ta�cz�cy dooko�a wiatr, szumi cicho pomi�dzy trawami.\n";
                        break;
                case 5: str="Drobny py�ek wpad� ci do oka wywo�uj�c "+
                        "nieprzyjemne pieczenie.\n"; break;
                case 6: str="Unosz�cy si� dooko�a kurz jest do�� uci��liwy.\n";
                        break;
                case 7: str="Trawy szumi� cicho.\n"; break;
                case 8: str=""; break;
            }
            break;
        case MT_ZIMA:
            switch(random(9))
            {
                case 0: str="Gdzie� nad tob� przelecia� jaki� ptak, g�o�no "+
                        "informuj�c o swojej obecno�ci.\n";
                        break;
                case 1: str="Ta�cz�cy dooko�a wiatr, szumi cicho pomi�dzy trawami.\n";
                        break;
                case 2: str="Trawy szumi� cicho.\n"; break;
                case 3..8: str=""; break;
            }

    }
    return str;
}

nomask void
create_redania()
{
    set_long("@@dlugasny@@");
    set_polmrok_long("@@opis_polmroku@@");

    set_start_place(0);

    add_event("@@event_traktu:"+file_name(TO)+"@@");
    add_prop(ROOM_I_TYPE, ROOM_TRACT);
    set_event_time(410.0);
    add_event("@@evencik:"+file_name(TO)+"@@");

    add_sit("na ziemi","na ziemi","z ziemi",0);
    add_prop(ROOM_I_WSP_Y, WSP_Y_BIALY_MOST); //Wsp�rz�dne, do systemu pogodowego.
    add_prop(ROOM_I_WSP_X, WSP_X_BIALY_MOST); //Rinde jest p�pkiem wszech�wiata ;)
                        //A Bia�y Most_okolice na zach�d od niego...

    dodaj_ziolo(({"czarny_bez-lisc.c",
                "czarny_bez-kwiaty.c",
                "czarny_bez-owoce.c"}));
    dodaj_ziolo("arcydziegiel.c");
    dodaj_ziolo("babka_lancetowata.c");
    dodaj_ziolo("piolun.c");
    dodaj_ziolo(({"krwawnik_ziele.c",
                "krwawnik_kwiatostan.c"}));

    create_trakt();
}

string
dlugasny()
{
    return "Ups! Zg�o� b��d!\n";
}

string
opis_polmroku()
{
    string str;
    //Noo, po traktach si� chodzi z lampami!
    //I to o ka�dej porze roku! Nie wiedzieli�cie? :)

    if(CZY_JEST_SNIEG(this_object()))
        str="W mroku jeste� w stanie dostrzec jedynie biel �niegu "+
            "le��cego na trakcie.\n";
    else
        str="Nad traktem zapad� mrok, dlatego nie jeste� w stanie dostrzec "+
            "zbyt wiele.\n";

    return str;
}
