/* Autor: Avard
   Opis : Samaia
   Data : 30.05.09 */

inherit "/std/object";

#include "/sys/stdproperties.h"
#include "/sys/macros.h"
#include "/sys/pl.h"
#include "/sys/materialy.h"
#include "/sys/object_types.h"

void
create_object()
{
    ustaw_nazwe("legowisko");
    dodaj_nazwy("szmaty");
    ustaw_material(MATERIALY_JUTA);
    set_type(O_INNE);
    set_long("Te k��by szmat i ga�gan�w to najwidoczniej miejsce spoczynku "+
    "jednego ze zwierz�t, zamieszkuj�cych ten opuszczony budynek. \n");
             
    add_prop(OBJ_I_WEIGHT, 2000); 
    add_prop(OBJ_I_VOLUME, 2000); 
    add_prop(OBJ_I_VALUE, 0);    
    add_prop(OBJ_I_DONT_SHOW_IN_LONG, 1);
    add_prop(OBJ_I_NO_GET,"Tych szmat jest ca^la masa, w dodatku nie "+
        "nadaj^a si^e do niczego, wi^ec po co je w og^ole rusza^c?\n");

    make_me_sitable("na","na stercie szmat","ze sterty szmat",3);
}
 
