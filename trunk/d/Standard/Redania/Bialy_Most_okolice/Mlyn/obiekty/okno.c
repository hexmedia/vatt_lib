/* Autor: Avard
   Opis : Samaia
   Data : 25.07.09 */

inherit "/std/window";

#include <pl.h>
#include <formulas.h>
#include "dir.h"

void
create_window()
{
    ustaw_nazwe("okno");

    dodaj_przym("niewielki", "niewielcy");

    set_window_id("oknonagorzewmlynieobokbialegomostu");
    set_open(0);
    set_open_desc("");
    set_closed_desc("");
    set_window_desc("Niewielkie okno, b^ed�ce w istocie zabit� deskami dziur� "+
    "w �cianie, wpuszcza do �rodka niewiele �wiat^la. Z pewno�ci� jednak nie "+
    "stanowi ^zadnej przeszkody dla insekt^ow, czy ma^lych, sprytnych gryzoni, "+
    "w dostaniu si^e do �rodka m^lyna. Niegdy� musia^lo by^c zas^loni^ete b^lon� z "+
    "p^echerza jakiego� zwierz^ecia, teraz zosta^ly ju^z po niej tylko powbijane "+
    "dooko^la otworu gwo�dzie.\n");

    ustaw_pietro(0);

    set_other_room(TRAKT_BIALY_MOST_LOKACJE + "sciezka3.c");
}
