/* Autor: Avard
   Opis : Samaia
   Data : 27.05.09 */

inherit "/std/object";

#include <cmdparse.h>
#include <pl.h>
#include <macros.h>
#include <stdproperties.h>
#include <object_types.h>
#include <materialy.h>

void create_object()
{
    ustaw_nazwe("beczka");
    dodaj_przym("zaple^snia^ly", "zaple^sniali");
    dodaj_przym("du^zy", "duzi");

    set_long("Stara beczka, typowego cylindrycznego kszta^ltu, zdaje "+
        "si^e by^c ^zywym przyk^ladem dla pewnego przys^lowia - brakuje "+
        "jej kilku klepek. Prawd^e m^owi^ac wi^ecej ich nie posiada, ni^z "+
        "ma, a spinaj^ace je obr^ecze ledwo trzymaj^a si^e na swoim "+
        "miejscu. Sama za^s beczka zawiera ca^lkiem grub^a warstw^e kurzu "+
        "i wszelkiego brudu, na tyle grub^a, by mog^l w niej kopa^c nor^e "+
        "spory kret z rodzin^a. \n");

    add_prop(OBJ_I_WEIGHT, 4000);
    add_prop(OBJ_I_VOLUME, 5000);
    add_prop(OBJ_I_VALUE, 0);
    set_type(O_MEBLE);
    ustaw_material(MATERIALY_DR_LIPA);
    make_me_sitable("na", "na kraw^edzi beczki", "na kraw^edzi beczki", 1);
}