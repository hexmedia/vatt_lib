/* Autor: Avard
   Opis : Samaia
   Data : 30.05.09 */

inherit "/std/object";

#include "/sys/stdproperties.h"
#include "/sys/macros.h"
#include "/sys/pl.h"
#include "/sys/materialy.h"
#include "/sys/object_types.h"

void
create_object()
{
    ustaw_nazwe("sterta");
    dodaj_nazwy("worki");
    ustaw_material(MATERIALY_JUTA);
    set_type(O_INNE);
    set_long("Te szmaty niegdy^s by^ly jutowymi workami, mog^acymi "+
        "pomie^sci^c wielkie ilo^sci m^aki czy zb^o^z. Teraz jednak ich "+
        "zaszczytna fukncja min^e^la, sta^ly si^e podartymi ^lachmanami "+
        "zalegaj^acymi pod ^scian^a, do tego pokrytymi sier^sci^a i "+
        "kurzem. \n");
             
    add_prop(OBJ_I_WEIGHT, 2000); 
    add_prop(OBJ_I_VOLUME, 2000); 
    add_prop(OBJ_I_VALUE, 0);    
    add_prop(OBJ_I_DONT_SHOW_IN_LONG, 1);
    add_prop(OBJ_I_NO_GET,"Tych szmat jest ca^la masa, w dodatku nie "+
        "nadaj^a si^e do niczego, wi^ec po co je w og^ole rusza^c?\n");

    make_me_sitable("na","na stercie szmat","ze sterty szmat",3);
}
