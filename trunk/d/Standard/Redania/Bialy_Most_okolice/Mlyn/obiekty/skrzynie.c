/* Autor: Avard
   Opis : Samaia
   Data : 30.05.09 */

inherit "/std/object";

#include "/sys/stdproperties.h"
#include "/sys/macros.h"
#include "/sys/pl.h"
#include "/sys/materialy.h"
#include "/sys/object_types.h"

void
create_object()
{
    ustaw_nazwe("skrzynie");
    dodaj_nazwy("worki");
    ustaw_material(MATERIALY_DR_BUK);
    set_type(O_INNE);
    set_long("Drewniane skrzynie, przys^laniaj^ace ^sciany wzd^lu^z obu "+
        "stron, teraz s^a pe^lne jedynie paj^eczyn i kurzu. Wida^c to "+
        "dzi^eki sporemu ubytkowi w deskach. \n");
             
    add_prop(OBJ_I_WEIGHT, 20000); 
    add_prop(OBJ_I_VOLUME, 20000); 
    add_prop(OBJ_I_VALUE, 0);    
    add_prop(OBJ_I_DONT_SHOW_IN_LONG, 1);
    add_prop(OBJ_I_NO_GET,"Wygl^adaj^a jakby mia^ly by si^e "+
        "zaraz rozpa^s^c, lepiej ich nie dotyka^c.\n");

}
