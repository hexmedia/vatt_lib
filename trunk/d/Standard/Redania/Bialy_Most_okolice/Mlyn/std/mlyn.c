
/* Mlyn przy Bialym Moscie.
    Avard                   */

#include "dir.h"

inherit REDANIA_STD;

#include <mudtime.h>
#include <language.h>
#include <stdproperties.h>
#include <pogoda.h>
#include <macros.h>

string dlugi_opis();

void
create_mlyn()
{
}

nomask void
create_redania()
{
    set_long("@@dlugi_opis@@");

    set_start_place(0);

    add_prop(ROOM_I_TYPE, ROOM_NORMAL);

    add_sit("na pod這dze","na pod這dze","z pod這gi",0);
    add_prop(ROOM_I_WSP_Y, WSP_Y_BIALY_MOST); //Wsp馧rz璠ne, do systemu pogodowego.
    add_prop(ROOM_I_WSP_X, WSP_X_BIALY_MOST); //Rinde jest p瘼kiem wszech鈍iata ;)
                        //A Bia造 Most_okolice na zach鏚 od niego...
    create_mlyn();
}

string
dlugi_opis()
{
    return "Ups! Zg這� b陰d!\n";
}
