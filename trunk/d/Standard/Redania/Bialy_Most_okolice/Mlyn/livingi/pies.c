

/* Autor: Avard
   Data : 22.06.09
   Opis : Samaia */

inherit "/std/zwierze";
inherit "/std/act/action";
inherit "/std/act/trigaction.c";

#include <macros.h>
#include <stdproperties.h>
#include <pl.h>
#include <formulas.h>
#include <std.h>
#include <composite.h>
#include <filter_funs.h>
#include <cmdparse.h>
#include <mudtime.h>
#include <wa_types.h>
#include <ss_types.h>
#include <object_types.h>
#include "dir.h"

int czy_atakowac();

void
create_zwierze()
{
    add_prop(CONT_I_WEIGHT, 35000+random(15000));
    add_prop(CONT_I_HEIGHT, 40+random(20));
    add_prop(NPC_I_NO_RUN_AWAY, 0);
    set_stats (({ 30+random(30), 50+random(40), 40+random(40), 20+random(20), 50+random(10)}));
    set_skill(SS_DEFENCE, 25);
    set_skill(SS_UNARM_COMBAT, 40);
    set_skill(SS_AWARENESS, 34);
    set_aggressive(&czy_atakowac());
    set_attack_chance(75);

    ustaw_odmiane_rasy("pies");
    set_gender(G_MALE);

    random_przym("zdzicza�y:zdziczali gro�ny:gro�ni agresywny:agresywni "+
    "nerwowy:nerwowi || wychudzony:wychudzeni wielki:wielcy "+
    "wyro�ni�ty:wyro�ni�ci chudy:chudzi brudny:brudni",2);
    
    set_long("Na poz�r pospolity, podw�rkowy pies, zdaje si� nie domaga� w "+
    "tej chwili pieszczot za uchem. Wyszczerzone k�y, nastroszona sier�� i "+
    "g�o�ne warczenie �wiadcz� o czym� zgo�a przeciwnym. Zapadni�te boki i "+
    "wyg�odnia�y wzrok, a raczej ich przyczyna, zmusi�y zdzicza�e zwierz� "+
    "do przej�cia roli �owcy po�ywienia. Jego siwe, brudne uw�osienie, "+
    "pozbawione ca�ych k��bk�w sierci oraz �lady zaschni�tej krwi wskazuj� "+
    "na niezbyt �atwe �ycie, kt�re popchn�o psa do takiego zachowania.\n");


    set_attack_unarmed(0, 20, 22, W_SLASH,  33, "^lapa", "lewy", "pazury");
    set_attack_unarmed(1, 20, 22, W_SLASH,  33, "^lapa", "prawy", "pazury");
    set_attack_unarmed(1, 20, 22, W_IMPALE, 34, "szcz^eki");

    set_hitloc_unarmed(0, ({ 1, 1, 1 }), 10, "^leb");
    set_hitloc_unarmed(1, ({ 1, 1, 1 }), 30, "tu^l^ow");
    set_hitloc_unarmed(2, ({ 1, 1, 1 }), 20, "^lapa", ({"przedni", "lewy"}));
    set_hitloc_unarmed(3, ({ 1, 1, 1 }), 20, "^lapa", ({"przedni", "prawy"}));
    set_hitloc_unarmed(4, ({ 1, 1, 1 }), 10, "^lapa", ({"lewy", "tylny"}));
    set_hitloc_unarmed(5, ({ 1, 1, 1 }), 10, "^lapa", ({"prawy", "tylny"}));
    
    set_cact_time(10);
    add_cact("warknij");

    set_act_time(30);
    add_act("emote rozgl^ada si^e niespokojnie.");
    add_act("emote podnosi g^low^e i w^eszy uwa^znie.");

    add_leftover("/std/skora", "sk^ora", 1, 0, 1, 1, 25 + random(5), O_SKORY); 
    add_leftover("/std/leftover", "kie^l", 2 + random(2), 0, 1, 1, 0, O_KOSCI);
    add_leftover("/std/leftover", "pazur", 4 + random(4), 0, 1, 0, 0, O_KOSCI);

    set_restrain_path(LAS_ZAKON_OKOLICE_LOKACJE);
    set_random_move(100);
}

int
czy_atakowac()
{
    
    if(TP->query_rasa() != "pies" && TP->query_humanoid() == 1)
        return 1;
    else
        return 0;
} 
