/* Autor: Avard
   Data : 26.05.09
   Opis : Samaia */

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <pogoda.h>
#include "dir.h"

inherit MLYN_STD;


void create_mlyn()
{
    set_short("Ciemna izba");

    add_prop(ROOM_I_INSIDE,1);
    add_item(({"kopczyk","gniazdo","mrowisko","sterta"}),"Szarobrunatny "+
        "kopczyk z bli^zej niezidentyfikowanego materia^lu pi^etrzy si^e "+
        "w k^acie, wype^lniaj^ac ca^l^a izb^e prawdziwie intensywnym odorem "+
        "ple^sni, padliny i... dawno niepranej bielizny?\n");

    add_item("pod^log^e", "Pod^loga, pokryta imponuj^acej grubo^sci "+
        "warstw^a brudu, prawdopodobnie jest drewniana. ^Swiadczy^c mog^a o "+
        "tym br^azowopodobne prze^swity spod kurzu, lub po prostu fakt, ^ze "+
        "lekko uginaj^a si^e pod wszelkim ci^e^zarem. Wydeptane miejsca "+
        "za^s stanowi^a ewidentne ^slady psich lub wilczych ^lap.\n");

    add_object(MLYN_OBIEKTY + "beczka.c");
    dodaj_rzecz_niewyswietlana("zaple�nia^la du^za beczka");
    add_exit(MLYN_LOKACJE+"mlyn5.c","ne");
    add_exit(MLYN_LOKACJE+"mlyn7.c","nw");
    add_door(MLYN_DRZWI + "z_mlyna.c");
}

public string
exits_description()
{
    return "Mo^zna st^ad wyj^s^c na zewn^atrz, s� tu tak^ze przej�cia w g^l�b"+
        " m^lyna - na p^o^lnocnym wschodzie i p^o^lnocnym zachodzie.\n";
}

string
dlugi_opis()
{
    string str;
    str = "Gdyby ktokolwiek chcia^l co^s skra^s^c z tego miejsca, nie "+
        "mia^lby wielkiego wyboru. Zosta^ly tu ju^z bowiem tylko pod^loga";
    if(jest_rzecz_w_sublokacji(0,"zaple^snia^la du^za beczka"))
    {
        str += ", go^le ^sciany i drewniana, zaple^snia^la beczka"+
            " poka^xnych rozmiar^ow. ";
    }
    else
    {
        str += " i go^le ^sciany. ";
    }
    str += "W istocie, ca^le pomieszczenie prze^smiard^lo odorem zgnilizmy "+
        "i wszechobecnego grzyba, czyhaj^acego na wszelkiej p^laszczy^xnie. "+
        "W samym rogu izby";
    if(jest_rzecz_w_sublokacji(0,"zaple^snia^la du^za beczka"))
    {
        str += ", a przeciwleg^lym k^acie od ponuro usadowionej beczki,";
    }
    str += " znajduje si^e co^s na kszta^lt gniazda lub mrowiska - "+
        "ciemnobury, niewielki kopczyk by^c mo^ze stanowi dom jakich^s "+
        "instekt^ow, lub po prostu rozk^ladaj^ac^a si^e stert^e starej "+
        "bielizny m^lynarza. Zar^owno po lewej, jak i po prawej stronie "+
        "znajduj^a si^e pary drzwi.";

    str += "\n";
    return str;
}