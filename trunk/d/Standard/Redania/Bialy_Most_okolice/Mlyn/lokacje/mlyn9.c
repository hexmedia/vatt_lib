/* Autor: Avard
   Data : 26.05.09
   Opis : Samaia */

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <pogoda.h>
#include "dir.h"

inherit MLYN_STD;

void create_mlyn()
{
    set_short("Poddasze");

    add_prop(ROOM_I_INSIDE,1);

    add_item("pod^log^e", "Pod^loga, pokryta imponuj^acej grubo^sci "+
        "warstw^a brudu, prawdopodobnie jest drewniana. ^Swiadczy^c mog^a o "+
        "tym br^azowopodobne prze^swity spod kurzu, lub po prostu fakt, ^ze "+
        "lekko uginaj^a si^e pod wszelkim ci^e^zarem. Wydeptane miejsca "+
        "za^s stanowi^a ewidentne ^slady psich lub wilczych ^lap.\n");

    add_item("schody","Te schody zdaj^a si^e nie utrzyma^c na sobie kota,"+
        "a co dopiero doros^lego cz^lowieka. Kolejne stopnie s^a "+
        "nad^lamane, za^s por^eczy nie ma wcale. Prowadz^a na d^o^l.\n");

    add_item("paj^eczyny","I^scie ^lowieckie sieci zastawi^ly po k^atach "+
        "pomieszkuj^ace w m^lynie paj^aki. Lepkie nici i misterne sploty"+
        " zwisaj^a z sufitu, polamanych desek i szcz^atk^ow zepsutych"+
        " mechanizm^ow.\n");

    add_item("okna","A raczej same otwory, dumne pozosta^lo^sci po"+
        " wpuszczaj^acych do wn^etrza ^swiat^lo wywietrznikach,"+
        " znajduj^a si^e zbyt wysoko by mo^c przez nie wyjrze^c na"+
        " zewn^atrz.\n");

    add_exit(MLYN_LOKACJE+"mlyn5.c", ({({"schody","d^o^l","d","na d^o^l"}),
    "na d^o^l po schodach"}),0,1,0);
}

public string
exits_description()
{
    return "Schody prowadz^a na d^o^l.\n";
}

string
dlugi_opis()
{
    string str;
    str = "W przeciwie^nstwie do poprzednich, to pomieszczenie jest dosy^c "+
        "rozleg^le, cho^c nie w lepszym stanie. Okno po ka^zdej ze ^scian "+
        "wpuszczaj^a do^s^c ^swiat^la, by m^oc zobaczy^c schody, prowadz^ace "+
        "na d^o^l i nie zabi^c si^e przy schodzeniu po nich. Ca^ly sufit "+
        "usiany jest podejrzanymi ciemnymi plamami, a do tego i^scie "+
        "przyzdobiony srebrzyst^a sieci^a, kt^ora musi by^c dzie^lem "+
        "naprawd^e du^zego paj^aka. Skrzypi^aca pod^loga to tak naprawd^e "+
        "deski przykryte warstw^a kurzu.";

    str += "\n";
    return str;
}