/* Autor: Avard
   Data : 22.06.09
   Opis : Samaia */

#include <stdproperties.h>
#include <materialy.h>
#include "dir.h"

inherit "/std/door";

void
create_door()
{
    ustaw_nazwe("drzwi");
    dodaj_przym("stary", "starzy");
    dodaj_przym("nadpr^ochnia^ly", "nadpr^ochniali");

    set_other_room(TRAKT_BIALY_MOST_LOKACJE + "sciezka3.c");
    set_door_id("MLYN_OBOK_BM_STARY_JEST_I_BRZYDKI");
    set_door_desc("Kilka prowizorycznie sklecionych desek udaje takie "+
    "w�a�nie drzwi z kiepskiej jako�ci drewna, pe�nego drzazg. Zawieraj� "+
    "metalowy haczyk, s�u�acy do ich zamkni�cia, ten jednak tak zupe�nie "+
    "przyrdzewia� do ich powierzchni, �e teraz to raczej niemo�liwe. \n");

    set_pass_command("wyj�cie", "na zewn�trz", "z m�yna");
    set_open_desc("");
    set_closed_desc("");

    ustaw_material(MATERIALY_DR_SOSNA);
}
