/* Autor: Avard
   Data : 26.05.09
   Opis : Samaia */

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <pogoda.h>
#include "dir.h"

inherit MLYN_STD;

void create_mlyn()
{
    set_short("Magazyn produkt^ow");

    add_prop(ROOM_I_INSIDE,1);

    add_item("pod^log^e", "Pod^loga, pokryta imponuj^acej grubo^sci "+
        "warstw^a brudu, prawdopodobnie jest drewniana. ^Swiadczy^c mog^a o "+
        "tym br^azowopodobne prze^swity spod kurzu, lub po prostu fakt, ^ze "+
        "lekko uginaj^a si^e pod wszelkim ci^e^zarem. Wydeptane miejsca "+
        "za^s stanowi^a ewidentne ^slady psich lub wilczych ^lap.\n");

    add_item("paj^eczyny","I^scie ^lowieckie sieci zastawi^ly po k^atach "+
        "pomieszkuj^ace w m^lynie paj^aki. Lepkie nici i misterne sploty"+
        " zwisaj^a z sufitu, polamanych desek i szcz^atk^ow zepsutych"+
        " mechanizm^ow.\n");

    add_item("elementy","W tej chwili trudno rozpozna^c co i jak niegdy^s"+
        " dzia^la^lo, bowiem teraz dawne m^ly^nskie ko^la, d^xwignie i"+
        " ^zarna to tylko kupa bezwarto^sciowych, zbytwia^lych i wyra^xnie"+
        " przez co^s lub kogo^s pogryzionych ^smieci.\n");

    add_object(MLYN_OBIEKTY + "skrzynie.c");
    add_object(MLYN_OBIEKTY + "legowisko.c");
    add_exit(MLYN_LOKACJE+"mlyn6.c", ({({"schody","g^ora","u","na g^or^e"}),
    "na g^or^e po schodach"}),0,1,0);

    add_npc(MLYN_LIVINGI + "pies.c");
}

public string
exits_description()
{
    return "Jedynym wyj^sciem z tego pomieszczenia s^a prowadz^ace na"+
        " g^or^e schody.\n";
}

string
dlugi_opis()
{
    string str;
    str = "Jest to miejsce, w kt^orym czeka^la gotowa ju^z m�ka, po ca^lym "+
    "procesie mielenia. Teraz to jedynie obraz rozpaczy. Sterty kamieni, "+
    "drewna, metalowych element^ow, wszystko to wala si^e po ca^lej pod^lodze "+
    "prezentuj�c si^e jako istne pobojowisko. Ze wszystkich �cian zwieszaj� "+
    "si^e ogromne paj^eczyny, a za kolumn� skrzy^n mo^zna dostrzec legowisko z "+
    "ga^lgan^ow i szmat. Z g^ory zwiesza si^e pod^lu^zna belka - jedyna rzecz, "+
    "kt^ora zosta^la na w^la�ciwym miejscu, mimo wszelich katastrof, kt^ore "+
    "dotkn^e^ly to miejsce.";

    str += "\n";
    return str;
}