/* Autor: Avard
   Data : 26.05.09
   Opis : Samaia */

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <pogoda.h>
#include "dir.h"

inherit MLYN_STD;

void create_mlyn()
{
    set_short("Wn^etrze m^lyna");

    add_prop(ROOM_I_INSIDE,1);

    add_item("pod^log^e", "Pod^loga, pokryta imponuj^acej grubo^sci "+
        "warstw^a brudu, prawdopodobnie jest drewniana. ^Swiadczy^c mog^a o "+
        "tym br^azowopodobne prze^swity spod kurzu, lub po prostu fakt, ^ze "+
        "lekko uginaj^a si^e pod wszelkim ci^e^zarem. Wydeptane miejsca "+
        "za^s stanowi^a ewidentne ^slady psich lub wilczych ^lap.\n");

    add_item(({"schody","schodki"}),"Kilka stopni prowadz^acych w d^o^l, "+
        "nigdy^s pod ca^l^a machin^a tego m^lyna, prowadzi do magazynu "+
        "produkt^ow - miejsca, w kt^orym czeka^la gotowa ju^z m^aka.\n");

    add_item("urz^adzenia","W tej chwili trudno rozpozna^c co i jak niegdy^s"+
        " dzia^la^lo, bowiem teraz dawne m^ly^nskie ko^la, d^xwignie i"+
        " ^zarna to tylko kupa bezwarto^sciowych, zbytwia^lych i wyra^xnie"+
        " przez co^s lub kogo^s pogryzionych ^smieci.\n");

    add_object(MLYN_OBIEKTY + "sterta.c");
    
    add_exit(MLYN_LOKACJE+"mlyn7.c","sw");
    add_exit(MLYN_LOKACJE+"mlyn5.c","se");
    add_exit(MLYN_LOKACJE+"mlyn8.c", ({({"schody","d^o^l","d","na d^o^l"}),"na d^o^l po schodach"}),0,1,0);

    add_npc(MLYN_LIVINGI + "pies.c");
    add_npc(MLYN_LIVINGI + "pies.c");
    add_npc(MLYN_LIVINGI + "pies.c");
}

public string
exits_description()
{
    return "Wyj^scia z tego pomieszczenia prowadz^a na po^ludniowy "+
        "zach^od i po^ludniowy wsch^od, znajduj^a si^e tu te^z schody "+
        "prowadz^ace na d^o^l.\n";
}

string
dlugi_opis()
{
    string str;
    str = "Pomieszczenie, kt^ore jest sercem tego m^lyna, jest miejscem, z "+
        "kt^orego mo^znaby by^lo zaobserwowa^c ca^ly proces tworzenia m^aki "+
        "ze zb^o^z. Mo^znaby - gdy^z teraz nie ma tu ^zadnych urz^adze^n, "+
        "pr^ocz rozbitych blok^ow kamienia i po^lamanych desek, "+
        "zalegaj^acych w magazynie produkt^ow, do kt^orego da si^e zej^s^c "+
        "po obecnych tu schodkach. Pod ^scian^a ci^agn^a si^e ca^le sterty "+
        "rozdartych na strz^epy jutowych work^ow. Wy^lamana balustrada, "+
        "wybite okienko, skrzypi^ace drzwi i grzyb oblegaj^acy ^sciany "+
        "stanowi^a dope^lnienie ^za^lo^snie ponurej atmosfery w tym "+
        "miejscu. Tylko nik^le ^swiat^lo";
    if(jest_dzien())
    {
        str += " s^loneczne";
    }
    else
    {
        str += " ksi^e^zyca";
    }
    str += " przedostaj^ace si^e przez ten niewielki otw^or w ^scianie "+
        "jest czym^s, co u^swiadamia, ^ze nie s^a to katakumby czy "+
        "podziemna jaskinia, a jedynie bardzo stary, opuszczony m^lyn. ";

    str += "\n";
    return str;
}