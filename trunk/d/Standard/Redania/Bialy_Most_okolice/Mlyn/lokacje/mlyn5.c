/* Autor: Avard
   Data : 26.05.09
   Opis : Samaia */

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <pogoda.h>
#include "dir.h"

inherit MLYN_STD;

void create_mlyn()
{
    set_short("W^askie pomieszczenie");

    add_prop(ROOM_I_INSIDE,1);

    add_item("pod^log^e", "Pod^loga, pokryta imponuj^acej grubo^sci "+
        "warstw^a brudu, prawdopodobnie jest drewniana. ^Swiadczy^c mog^a o "+
        "tym br^azowopodobne prze^swity spod kurzu, lub po prostu fakt, ^ze "+
        "lekko uginaj^a si^e pod wszelkim ci^e^zarem. Wydeptane miejsca "+
        "za^s stanowi^a ewidentne ^slady psich lub wilczych ^lap.\n");

    add_item("schody","Te schody zdaj^a si^e nie utrzyma^c na sobie kota,"+
        "a co dopiero doros^lego cz^lowieka. Kolejne stopnie s^a "+
        "nad^lamane, za^s por^eczy nie ma wcale. Prowadz^a do g^ory, "+
        "prawdopodobnie do magazynu.\n");

    add_window(MLYN_OBIEKTY+"okno.c",1);

    add_exit(MLYN_LOKACJE+"mlyn6","nw");
    add_exit(MLYN_LOKACJE+"mlyn4","sw");
    add_exit(MLYN_LOKACJE+"mlyn9.c", ({({"schody","g�ra","u","na g�r�"}),
    "na g�r� po schodach"}),0,1,0);
    add_npc(MLYN_LIVINGI + "pies.c");
}

public string
exits_description()
{
    return "Wyj^scia z tego pomieszczenia prowadz^a na po^ludniowy "+
        "zach^od i p^o^lnocny zach^od, znajduj^a si^e tu te^z schody "+
        "prowadz^ace na g^or^e.\n";
}

string
dlugi_opis()
{
    string str;
    str = "Mimo ma^lego okna i wpadaj^acego przeze^n ^swiat^la ";
    if(jest_dzien())
    {
        str += "s^lonecznego";
    }
    else
    {
        str += "ksi^e^zyca";
    }
    str += " niewiele wida^c w tym pomieszeniu. Pierwsze, albo nawet "+
        "jedyne, co si^e rzuca w oczy, to w^atpliwej jako^sci drewniane "+
        "schody prowadz^ace na g^or^e, przypominaj^ace drabin^e. Opr^ocz "+
        "nich pomieszczenie posiada jedynie dwie pary drzwi - na "+
        "po^ludniowym zachodzie oraz p^o^lnocnym zachodzie - prowadz^ace "+
        "do dalszych izb. Ca^la pod^loga, pokryta brudem, nosi na sobie "+
        "^slady psich, czy te^z mo^ze wilczych ^lap, a w niekt^orych "+
        "miejscach mo^zna dostrzec br^azowe, zaschni^ete plamy. ";

    str += "\n";
    return str;
}