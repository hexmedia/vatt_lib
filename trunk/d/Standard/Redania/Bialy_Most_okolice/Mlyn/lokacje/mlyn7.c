/* Autor: Avard
   Data : 26.05.09
   Opis : Samaia */

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <pogoda.h>
#include "dir.h"

inherit MLYN_STD;

void create_mlyn()
{
    set_short("Ma^la ^smierdz^aca komora");

    add_prop(ROOM_I_INSIDE,1);

    add_item("pod^log^e", "Pod^loga, pokryta imponuj^acej grubo^sci "+
        "warstw^a brudu, prawdopodobnie jest drewniana. ^Swiadczy^c mog^a o "+
        "tym br^azowopodobne prze^swity spod kurzu, lub po prostu fakt, ^ze "+
        "lekko uginaj^a si^e pod wszelkim ci^e^zarem. Wydeptane miejsca "+
        "za^s stanowi^a ewidentne ^slady psich lub wilczych ^lap.\n");

    add_item("paj^eczyny","I^scie ^lowieckie sieci zastawi^ly po k^atach "+
        "pomieszkuj^ace w m^lynie paj^aki. Lepkie nici i misterne sploty"+
        " zwisaj^a z sufitu, polamanych desek i szcz^atk^ow zepsutych"+
        " mechanizm^ow.\n");

    add_object(MLYN_OBIEKTY + "skrzynie.c");
    
    add_exit(MLYN_LOKACJE+"mlyn6.c","ne");
    add_exit(MLYN_LOKACJE+"mlyn4.c","se");
}

public string
exits_description()
{
    return "Wyj^scia z tego pomieszczenia prowadz^a na po^ludniowy "+
        "wsch^od i p^o^lnocny wsch^od.\n";
}

string
dlugi_opis()
{
    string str;
    str = "Bardzo ciasna komora, kt^ora najmniejszego klaustrofoba by^laby "+
        "w stanie doprowadzi^c do prawdziwej paniki, zape^lniona jest "+
        "r^o^znych rozmiar^ow skrzyniami. Ci^e^zko by^loby tu znale^x^c "+
        "tak^a, kt^orej nie brakowa^loby jakiej^s deski. Obdrapane ^sciany, "+
        "ciemny sufit i uginaj^aca si^e pod najmniejszym ci^e^zarem "+
        "pod^loga nie s^a te^z czym^s, co ucieszy^loby potencjalnego "+
        "zwiedzaj^acego przebywaj^acego w tym miejscu, o ile by by^l o "+
        "zdrowych zmys^lach. Wszystkie skrzynie oplecione zosta^ly "+
        "delikatn^a sieci^a paj^eczyn.";

    str += "\n";
    return str;
}