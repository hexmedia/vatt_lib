#include "../dir.h"

#define TRAKT_BIALY_MOST_STD        (TRAKT_BIALY_MOST_OKOLICE +"std/trakt.c")
#define TRAKT_DO_PIANY_STD			(TRAKT_BIALY_MOST_OKOLICE +"std/trakt_do_piany.c")
#define TRAKT_BIALY_MOST_LOKACJE    TRAKT_BIALY_MOST_OKOLICE_LOKACJE
//#define BRZEG_BIALY_MOST	(BIALY_MOST_OKOLICE+"Brzeg/")
//#define BRZEG_BIALY_MOST_LOKACJE (BRZEG_BIALY_MOST+"lokacje/")
#define BRZEG_BIALY_MOST_STD        (BRZEG_BIALY_MOST_OKOLICE +"std/brzeg.c")

#define MLYN                        (BIALY_MOST_OKOLICE + "Mlyn/")
#define MLYN_LOKACJE                (MLYN + "lokacje/")
#define MLYN_OBIEKTY                (MLYN + "obiekty/")
#define MLYN_LIVINGI                (MLYN + "livingi/")
#define MLYN_STD                    (MLYN + "std/mlyn.c")
#define MLYN_DRZWI                  (MLYN_LOKACJE + "drzwi/")

#ifdef SLEEP_PLACE
#undef SLEEP_PLACE
#endif SLEEP_PLACE
