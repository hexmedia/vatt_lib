/* Autor: Vera
   Data : 7.04.07
   Opis : Tinardan */

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <pogoda.h>
#include "dir.h"

inherit BIALY_MOST_STD;

string
korale()
{
    if(jest_rzecz_w_sublokacji(0, "Lollo") ||
        jest_rzecz_w_sublokacji(0, "niski siwawy m�czyzna"))
    {
        return "Sznur korali, dyndaj�cy na brzegu p�ki, zsuwa "+
            "si� i ze stukiem spada na pod�og�. Sprzedawca "+
            "natychmiast podnosi go i uk�ada tak, by ju� nie zlatywa�.\n";
    }
    else
        return "";
}


void create_bm_room()
{
    set_short("Sklep");
    add_npc(BIALY_MOST_NPC + "sklepikarz");

    add_object(BIALY_MOST_LOKACJE_DRZWI +"ze_sklepu.c");

    set_event_time(500.0);
    add_event("Z zewn�trz dochodz� ci� g�osy przechodni�w.\n");
    add_event("Lampka oliwna przygasa na moment tylko po to, by "+
    			"za chwil� zn�w buchn�� jasnym p�omykiem.\n");
    add_event("Drzwi otwieraj� si� i do �rodka wsuwa si� jaki� klient.\n");
    add_event("@@korale:"+file_name(TO)+"@@");

    add_prop(ROOM_I_INSIDE,1);
    add_object("/d/Standard/items/kosz_na_smieci");
}

public string
exits_description()
{
	return "Wyj�cie st�d prowadzi na ulic�.\n";
}

string
dlugi_opis()
{
    string str;
    str = "Pomimo mroku, roz�wietlanego jedynie kilkoma oliwnymi "+
    "lampkami mo�na wyra�nie dostrzec, jak zadbane jest to pomieszczenie. "+
    "Na drewnianej pod�odze nie osta�a si� nawet okruszynka kurzu, "+
    "towary na p�kach pouk�adane s� r�wniutko. Posegregowano je tak, by "+
    "wszystko mo�na by�o dobrze zobaczy�. Po lewej stoj� wieszaki ob�o�one "+
    "r�nego rodzaju torbami i plecakami. Ubrania, posk�adane i �wie�e, "+
    "zosta�y opatrzone karteczkami, na kt�rych ko�lawo i nieortograficznie "+
    "wypisano ich nazwy. Wida� wi�c \"spodnie tanie\" i \"bl�ski kobiece\", "+
    "\"spudnice zwyk�e\" i \"spudnice eleg�ckie\" czy \"suknie na zapleczu\". "+
    "Na ni�szych pouk�adano najr�niejsze przedmioty bardziej i mniej "+
    "niezb�dne w �yciu codziennym, ale ka�de na swojej kupce, b�d� w swoim "+
    "k�ciku, tak by �atwo by�o je znale�� czy po nie si�gn��. ";

    if(jest_rzecz_w_sublokacji(0, "Lollo") ||
        jest_rzecz_w_sublokacji(0, "niski siwawy m�czyzna"))
    {
        str+="Za grubym, d�bowym blatem siedzi siwawy jegomo�� o "+
        "krzaczastych brwiach i pykaj�c fajeczk� spogl�da na ciebie "+
        "z zainteresowaniem. Pachnie tu dymem i zio�ami.";
    }

    str += "\n";
    return str;
}



