/*
Made by Eleb.
*/

inherit "/std/door";
#include "dir.h"
#include <pl.h>

void create_door(){
    ustaw_nazwe("drzwi");
    dodaj_przym("drewniany","drewniani");

    set_door_desc("Masz przed sob� zwyk�e drewniane drzwi. Zamykane s� na zamek."+
                  " Nie posiadaj� �adnych ozd�b.\n");
    set_door_id("DRZWI_DO_SYPIALNI_BURMISTRZA_BIALEG_MOSTU");

    set_key("KLUCZ_DRZWI_DO_SYPIALNI_BURMISTRZA_BIALEG_MOSTU");

    set_pass_mess("Przechodzisz przez drewniane drzwi.\n");

    set_lock_mess("przekr�ca klucz w zamku.\n", "Przekr�casz klucz w zamku.\n",
        "Slyszysz jaki� szcz�k zamka, jak by kto� przekr�ca� w nim klucz.\n");

    set_unlock_mess("przekr�ca klucz w zamku.\n", "Przekr�casz klucz w zamku.\n",
        "Slyszysz jaki� szcz�k zamka, jak by kto� przekr�ca� w nim klucz.\n");

    set_open_desc("");
    set_closed_desc("");

    set_other_room(BIALY_MOST_LOKACJE + "gabinet.c");
    set_pass_command(({"drzwi","gabinet","wyj^scie"}),"przez drewniane drzwi",
        "z gabinetu");

    set_lock_name(({"zamek od drzwi", "zamku od drzwi", "zamkowi od drzwi",
        "zamek od drzwi", "zamkniem od drzwi", "zamku do drzwi"}), 0, PL_MESKI_NOS_NZYW);
    set_lock_desc("Sporej wielko�ci zamek. Ani ozdobny ani sk�plikowany.\n");

    set_open(1);
    set_locked(1);

}
