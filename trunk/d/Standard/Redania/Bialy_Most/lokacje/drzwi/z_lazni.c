/*
 * drzwi do lazni w BM
 * opis by Sniegulak
 * popsu^la faeve
 * dn. 21 maja 2005
 */

inherit "/std/door";

#include <pl.h>
#include "dir.h"

void
create_door()
{
    ustaw_nazwe("drzwi");
    dodaj_przym("zwyk^ly","zwykli");
    dodaj_przym("drewniany", "drewniani");

    set_other_room(BIALY_MOST_LOKACJE + "sala.c");
    set_door_id("DRZWI_DO_LAZNI_W_BM");
    set_door_desc("Drewniane, zbite z desek, po^l^aczone poprzecznymi "+
        "^latami, wygl^adaj^a solidnie i w miar^e estetycznie. ^Srednio "+
        "masywna budowa i zastosowanie zamka, zapewnia odpowiednie "+
        "zamkni^ecie pomieszczenia i ochron^e przed niechcianymi go^s^cmi.\n");

    set_open_desc("");
    set_closed_desc("");

    set_pass_command(({"drzwi","wyj^scie","sala"}),"przez zwyk^le drewniane drzwi",
        "wychod^ac z ^la^xni");

    set_open(0);
    set_locked(0);

    set_lock_mess("zamyka drzwi na haczyk.\n", "Zamykasz drzwi na haczyk.\n", "");
    set_unlock_mess("wyci�ga haczyk i odblokowuje drzwi.\n", "Wyci�gasz haczyk i odblokowujesz drzwi.\n", "");

    set_lock_name(({"haczyk", "haczyka", "haczykowi", "haczyk", "haczykiem", "haczyku"}), 0, PL_MESKI_NOS_NZYW);
    set_lock_desc(""); //FIXME

}