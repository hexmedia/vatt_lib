inherit "/std/key";

#include <stdproperties.h>
#include <pl.h>

void
create_key()
{
    ustaw_nazwe("klucz");
    dodaj_przym("zwyk^ly", "zwykli");
    dodaj_przym("metalowy", "metalowi");
    set_long("Jest to zwyk^ly metalowy klucz, kt^orego powierzchnia zosta^la "+
        "nieznacznie porysowana.\n");

    set_key("KLUCZ_DRZWI_DO_KARCZMY_BIALY_MOST");

}