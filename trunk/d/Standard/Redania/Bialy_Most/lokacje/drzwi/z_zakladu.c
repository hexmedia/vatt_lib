
/*
 * Drzwi z pochwiarza w Bia�ym Mo�cie
 * Vera
 */

inherit "/std/door";
#include "dir.h"

#include <pl.h>

void
create_door()
{
    ustaw_nazwe("drzwi");
    dodaj_przym("zwyk^ly","zwykli");

    set_other_room(BIALY_MOST_LOKACJE_ULICE + "u09.c");
    set_door_id("DRZWI_DO_ZAKLADU_W_BIALYM_MOSCIE");
    set_door_desc("Solidne, d^ebowe drzwi z jednego z miejskich " +
        "sklep^ow. Wiekowe ju^z drewno mocno pociemnia^lo od s^lo^nca.\n");

    set_pass_command("wyj^scie","do wyj�cia","z zak�adu snycerskiego");
//     set_pass_mess("przez zwyk^le drzwi na zewn^atrz");
    set_open_desc("");
    set_closed_desc("");

    set_key("KLUCZ_DRZWI_DO_ZAKLADU_SNYCERSKIEGO_BIALY_MOST");
    set_lock_name(({"zamek", "zamku", "zamkowi", "zamek", "zamkiem", "zamku"}), 0, PL_MESKI_NOS_NZYW);
}
