
/*
 * Niewielki stalowy klucz
 * Do drzwi_do_sklepu
 * Do Bia�ego Mostu. vera.
 */

inherit "/std/key";

#include <stdproperties.h>
#include <pl.h>

void
create_key()
{
    ustaw_nazwe("klucz");

    set_long("Sporych rozmiar�w, stalowy klucz.\n");

    dodaj_przym("ogromny", "ogromny");
    dodaj_przym("stalowy", "stalowi");

    set_key("KLUCZ_DRZWI_DO_SKLEPU_BIALY_MOST");
}