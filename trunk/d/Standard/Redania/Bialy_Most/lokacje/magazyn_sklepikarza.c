#include <stdproperties.h>
#include "dir.h"

inherit BIALY_MOST_STD;
inherit "/lib/store_support";

void create_bm_room()
{
    set_short("Kiesze� sklepikarza");
    set_long("Jeste� w kieszeni bez dna.\n");

    add_prop(ROOM_I_INSIDE,1);

    add_object(BIALY_MOST_PRZEDMIOTY+"wachlarz.c",4);
    add_object(BIALY_MOST_PRZEDMIOTY+"sakiewki/pojemna_materialowa.c",40);
    add_object(BIALY_MOST_PRZEDMIOTY+"sakiewki/szara_polatana.c",40);
    add_object(BIALY_MOST_PRZEDMIOTY+"sakiewki/mala_niebieska.c",40);
    add_object(BIALY_MOST_PRZEDMIOTY+"pasy/szary_parciany.c",40);
    add_object(BIALY_MOST_PRZEDMIOTY+"pasy/waski_czerwony.c",40);
    add_object(BIALY_MOST_PRZEDMIOTY+"pasy/mocny_zielonkawy.c",40);
    add_object(BIALY_MOST_PRZEDMIOTY+"pasy/mocny_skorzany.c",40);
    add_object(BIALY_MOST_PRZEDMIOTY+"pasy/matowy_czarny.c",40);
    add_object(BIALY_MOST_PRZEDMIOTY+"pasy/czerwony_zdobiony.c",30);
    add_object(BIALY_MOST_PRZEDMIOTY+"plecaki/brazowy.c",30);
    add_object(BIALY_MOST_PRZEDMIOTY+"plecaki/czarny_skorzany.c",40);
    add_object("/d/Standard/items/sloik.c",40);
    add_neverending_object("/d/Standard/items/sloik.c");

}
void                       /* wywolywane za kazdym razem, jak jakis */
enter_inv(object ob, object skad)  /* obiekt wchodzi do wnetrza tego obiektu */
{                  /* ...czyli do pokoju */

    ::enter_inv(ob, skad); /* trzeba ZAWSZE wywolac gdy redefiniujemy enter_inv */
    
    store_update(ob); /* dba o to, zeby magazynie nie bylo za duzo rzeczy */
}
void
leave_inv(object ob, object to)
{
    //::leave_env(ob,to);
    store_add_item(ob);
}

