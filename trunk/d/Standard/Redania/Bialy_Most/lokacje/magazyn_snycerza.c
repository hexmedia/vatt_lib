#include "dir.h"

inherit BIALY_MOST_STD;
inherit "/lib/store_support";
#include <stdproperties.h>
#include <macros.h>
#define REP_MAT "/d/Standard/items/materialy/"
void create_bm_room()
{
    set_short("Tajny magazyn");
    set_long("Jeste� w tajnym magazynie. Nie ma st�d �adnego wyj�cia!\n");
    add_prop(ROOM_I_INSIDE,1);
    add_prop(ROOM_I_ALWAYS_LOAD, 1);

    //add_object(BIALY_MOST_PRZEDMIOTY + "narzedzia/dluto.c");<--szukaj w Inv. craftsmana!
}

void                       /* wywolywane za kazdym razem, jak jakis */
enter_inv(object ob, object skad)  /* obiekt wchodzi do wnetrza tego obiektu */
{                  /* ...czyli do pokoju */

    ::enter_inv(ob, skad); /* trzeba ZAWSZE wywolac gdy redefiniujemy enter_inv */
    
    store_update(ob); /* dba o to, zeby magazynie nie bylo za duzo rzeczy */
}
