/*
 *Ulica Bia�ego Mostu
 *Vera
 */

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"
#include <pogoda.h>
#define DEF_DIRS ({"po^ludnie"})
inherit ULICA_BIALY_MOST_STD;

void create_ulica() 
{
    set_short("Przy mo�cie");
    set_long("@@dlugasny@@");
	add_exit(BIALY_MOST_LOKACJE_ULICE + "u04.c","n",0,1,0);
	add_exit(BRZEG_BIALY_MOST_LOKACJE + "b05.c","w",0,7,0);
	add_exit(BRZEG_BIALY_MOST_LOKACJE + "b01.c","e",0,7,0);

    add_prop(ROOM_I_INSIDE,0);
	add_object(BIALY_MOST_PRZEDMIOTY+"lampa_uliczna.c");
}
public string
exits_description() 
{
    return "Na zach�d oraz na wsch�d st�d mo�na zej�� na brzeg, za� "+
	"na p�nocy wej�� wg��b miasta. Droga na po�udnie przez most jest "+
	"w trakcie budowy i nie jest dost�pna.\n";
}

int unq_no_move(string str)
{
    ::unq_no_move(str);

    if (member_array(query_verb(), DEF_DIRS) != -1)
        notify_fail("Droga na po�udnie przez most jest "+
	"w trakcie budowy i nie jest dost�pna.\n");
    return 0;
}
string
dlugasny()
{
	string str="";

	if(jest_dzien())
	str+="Wozy "+
	"zapakowane przer�nymi towarami regularnie przebywaj� j� w obie "+
	"strony kieruj�c si� na most, lub w d� ku przystani barek. Po obu "+
	"stronach wa�u rozci�ga si� widok na brzeg i nieliczne chaty "+
	"podgrodzia, nieregularnie "+
	"rozrzucone pomi�dzy miasteczkiem, a sennie p�yn�c� rzek�, "+
	"nad kt�r� rozpi�ty jest s�ynny Bia�y Most, wspania�a, lecz "+
	"nadszarpni�ta z�bem czasu budowla.";
	else
	str+="Mroczny kontur mostu jest dobrze widoczny na tle �uny "+
	"rozpo�cieraj�cej si� nad miastem. Po obu stronach wa�u na kt�rym "+
	"jeste� rozci�ga si� mroczne nabrze�e, z gdzieniegdzie widocznymi "+
	"bladymi po�wiatami lamp, czy �uczyw. Z po�udnia dochodzi cichy "+
	"szept wolno p�yn�cej rzeki, przeci�tej ledwie widocznym w "+
	"ciemno�ciach mostem.";

	str+="\n";
	return str;
}

public void
enter_inv(object ob, object from)
{
    ::enter_inv(ob, from);

    if (interactive(ob) && !ob->query_wiz_level()) {
        ob->set_default_start_location(BIALY_MOST_LOKACJE+"strych.c");
    }
}
