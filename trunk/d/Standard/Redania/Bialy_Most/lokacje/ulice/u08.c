/*
 *Ulica Bia�ego Mostu
 *Vera
 */

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"
#include <pogoda.h>

inherit ULICA_BIALY_MOST_STD;
int zamkniety=0;

string
sklep()
{
	if(!zamkniety)
	return "Pierwszy z brzegu sklepik zaprasza szeroko otwartymi "+
		"drzwiami, cho� front niewidoczny jest zza �mierdz�cego "+
		"kapust� straganu.\n";
	else
	return "O tej porze sklep jest ju� zamkni�ty.\n";
}

void create_ulica() 
{
    set_short("Przed sklepem");
    set_long("@@dlugasny@@");
	add_exit(BIALY_MOST_LOKACJE_ULICE + "u10.c","nw",0,1,0);
	add_exit(BIALY_MOST_LOKACJE_ULICE + "u06.c","sw",0,1,0);
	add_exit(BIALY_MOST_LOKACJE_ULICE + "u09.c","e",0,1,0);
	add_object(BIALY_MOST_LOKACJE_DRZWI +"do_sklepu.c");

	add_item("sklep","@@sklep@@");

    add_prop(ROOM_I_INSIDE,0);
	add_object(BIALY_MOST_PRZEDMIOTY+"lampa_uliczna.c");
}
public string
exits_description() 
{
    return "Ulica ci�gnie si� z p�nocnego-zachodu na wsch�d, za� "+
    "na po�udniowym-zachodzie widnieje plac. Jest tu tak�e wej�cie "+
    "do sklepu.\n";
}

void
zamkniety_sklep()
{
	zamkniety=1;

}
void
otwarty_sklep()
{
	zamkniety=0;
}

string
dlugasny()
{
	string str="";

	if(jest_dzien())
	str+="W�ska uliczka daje nieco wytchnienia przed t�okiem "+
	"g��wnych trakt�w, czy plac�w. Schludne kamieniczki "+
	"ci�gn�ce si� z obu stron dowodz� maj�tno�ci mieszka�c�w, "+
	"za� sklepy i kramy stoj� tutaj lu�nej, co pozwala w "+
	"spokoju dokona� zakup�w. Cho�by ten najbli�ej ciebie "+
	"wydaje si� nieszczeg�lnie oblegany. Blaszane lampy "+
	"umieszczone gdzieniegdzie na �cianach s� nowe i nie�le "+
	"wykonane.";
	else
	{
	str+="W�ska uliczka wydaje si� niemal wymar�a. Schludne "+
	"kamieniczki ci�gn�ce si� z obu stron dowodz� maj�tno�ci "+
	"mieszka�c�w, za� nieliczne sklepy i kramy s� o tej porze "+
	"puste i ciche. ";

	if(zamkniety)
		str+="Blaszane lampy umieszczone "+
	"gdzieniegdzie na �cianach roz�wietlaj� mroki nocnych godzin.";
	else
		str+="W jednym z nich dostrzegasz jednak �wiat�o, "+
	"najwyra�niej nie jest zamkni�te. Blaszane lampy umieszczone "+
	"gdzieniegdzie na �cianach roz�wietlaj� mroki nocnych godzin.";

	}

	str+="\n";
	return str;
}
