/*
 *Ulica Bia�ego Mostu
 *Vera
 */

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"
#include <pogoda.h>

inherit ULICA_BIALY_MOST_STD;

void create_ulica() 
{
    set_short("Szeroka uliczka");
    set_long("@@dlugasny@@");
	add_exit(BIALY_MOST_LOKACJE_ULICE + "u03.c","w",0,1,0);
	add_exit(BIALY_MOST_LOKACJE_ULICE + "u01.c","ne",0,1,0);

    add_prop(ROOM_I_INSIDE,0);
	add_object(BIALY_MOST_PRZEDMIOTY+"lampa_uliczna.c");
}
public string
exits_description() 
{
    return "Ulica ci�gni� si� z zachodu na p�nocny-wsch�d.\n";
}


string
dlugasny()
{
	string str="";

	if(jest_dzien())
	str+="Znajdujesz si� na g��wnej uliczce miasta ��cz�cej po�udniowy "+
		"i wschodni kraniec miasteczka. Wozy zapakowane przer�nymi "+
		"towarami niemal bez przerwy mijaj� ci� poruszaj�c si� w obu "+
		"kierunkach. Bogate kamieniczki ci�gn�ce si� wzd�u� ulicy s� "+
		"tutaj wyj�tkowo okaza�e. Nawet blaszane lampy umieszczone "+
		"regularnie na �cianach s� nowe i nie�le wykonane.\n";
	else
	str+="Znajdujesz si� na g��wnej uliczce miasta ��cz�cej po�udniowy "+
		"i wschodni kraniec miasteczka. Nawet o tej porze pojawiaj� si� "+
		"tu pojedynczy przechodnie. Bogate kamieniczki ci�gn�ce si� "+
		"wzd�u� ulicy s� tutaj wyj�tkowo okaza�e. Blaszane lampy "+
		"umieszczone regularnie na �cianach roz�wietlaj� mroki nocnych "+
		"godzin.\n";

	return str;
}
