/*
 *Ulica Bia�ego Mostu
 *Vera
 */

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"
#include <pogoda.h>

inherit ULICA_BIALY_MOST_STD;

void create_ulica() 
{
    set_short("Skrzy�owanie");
    set_long("@@dlugasny@@");
	add_exit(BIALY_MOST_LOKACJE_ULICE + "u04.c","sw",0,1,0);
	add_exit(BIALY_MOST_LOKACJE_ULICE + "u06.c","nw",0,1,0);
	add_exit(BIALY_MOST_LOKACJE_ULICE + "u02.c","e",0,1,0);

	add_npc(BIALY_MOST_NPC + "mieszczanin");

    add_prop(ROOM_I_INSIDE,0);
	add_object(BIALY_MOST_PRZEDMIOTY+"lampa_uliczna.c");
}
public string
exits_description() 
{
    return "Ulice wiod� w kierunkach: p�nocny-zachod, po�udniowy-zach�d oraz "+
			"wsch�d.\n";
}


string
dlugasny()
{
	string str="";

	if(jest_dzien())
	str+="Znajdujesz si� w miejscu gdzie ��cz� si� trakty "+
		"biegn�ce na p�noc, po�udnie i wsch�d. Wozy zapakowane "+
		"przer�nymi towarami niemal bez przerwy mijaj� ci� "+
		"poruszaj�c si� we wszystkich kierunkach, za� wszechobecna "+
		"mieszanina d�wi�k�w i zapach�w dope�nia obrazu panuj�cego tu "+
		"zgie�ku. Bogate kamieniczki widoczne ponad wozami s� tutaj "+
		"wyj�tkowo okaza�e. Nawet blaszane lampy umieszczone "+
		"regularnie na �cianach s� nowe i nie�le wykonane.";
	else
	str+="Znajdujesz si� w miejscu gdzie ��cz� si� trakty biegn�ce na "+
		"p�noc, po�udnie i wsch�d. Nawet o tej porze pojawiaj� si� "+
		"tu pojedynczy przechodnie, czy sp�niony w�z. Bogate "+
		"kamieniczki ci�gn�ce si� dooko�a placu s� tutaj wyj�tkowo "+
		"okaza�e. Blaszane lampy umieszczone regularnie na �cianach "+
		"roz�wietlaj� mroki nocnych godzin.";


	str+="\n";
	return str;
}
