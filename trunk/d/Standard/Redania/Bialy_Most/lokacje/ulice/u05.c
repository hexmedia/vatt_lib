/*
 *Ulica Bia�ego Mostu
 *Vera
 */

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"
#include <pogoda.h>

inherit ULICA_BIALY_MOST_STD;

void create_ulica() 
{
    set_short("W�ska ulica");
    set_long("@@dlugasny@@");
	add_exit(BIALY_MOST_LOKACJE + "kramy1.c","w",0,1,0);
	add_exit(BIALY_MOST_LOKACJE_ULICE + "u06.c","ne",0,1,0);
	add_exit(BIALY_MOST_LOKACJE_ULICE + "u04.c","se",0,1,0);

    add_prop(ROOM_I_INSIDE,0);
	//tu nie ma by� lampy!
}
public string
exits_description() 
{
    return "Ulice wiod� na p�nocny-wsch�d oraz po�udniowy-wsch�d, za� "+
		"na zachodzie ci�gnie si� w�ska uliczka z kramami rybnymi.\n";
}


string
dlugasny()
{
	string str="";

	if(jest_dzien())
	str+="Znajdujesz si� u wylotu zau�ka wyj�tkowo mocno woniej�cego "+
	"rybami. Schludne w ca�ym mie�cie kamieniczki tutaj s� nieco "+
	"skromniejsze, za� sklep�w i kram�w jest jakby mniej. Z "+
	"widocznego na po�udniu placu dobiegaj� g�osy wielu ludzi i "+
	"stukot woz�w.";
	else
	str+="W tym miejscu znajduje si� wylot zau�ka wyj�tkowo mocno "+
		"woniej�cego rybami. Schludne w ca�ym mie�cie kamieniczki "+
		"tutaj s� nieco skromniejsze, za� sklep�w i kram�w jest "+
		"jakby mniej. Na po�udniu lepiej o�wietlony plac z celuj�cym "+
		"w niebo konturem �urawia jest cichy i spokojny.";



	str+="\n";
	return str;
}
