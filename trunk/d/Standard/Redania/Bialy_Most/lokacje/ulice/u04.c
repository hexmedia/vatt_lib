/*
 *Ulica Bia�ego Mostu
 *Vera
 */

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"
#include <pogoda.h>

inherit ULICA_BIALY_MOST_STD;
inherit "/lib/drink_water";

void create_ulica() 
{
    set_short("Przy studni");
    set_long("@@dlugasny@@");
	add_exit(BIALY_MOST_LOKACJE_ULICE + "u11.c","s",0,1,0);
	add_exit(BIALY_MOST_LOKACJE_ULICE + "u03.c","ne",0,1,0);
	add_exit(BIALY_MOST_LOKACJE_ULICE + "u05.c","nw",0,1,0);

	add_item(({"sklep","sklepy","kram","kramy"}),
			"Wszelkiej ma�ci budki, kramy, sklepiki i stragany "+
			"poutykane s� wsz�dzie gdzie znalaz� si� cho� s��e� "+
			"miejsca. Zbiegaj�ce si� w tym mie�cie szlaki kupieckie "+
			"zasilaj� je towarami z takich miast jak Wyzima czy Novigrad. "+
			"Jak zwykle przy nat�oku d�br, trzeba sporo si� naszuka� "+
			"by znale�� co� konkretnego.\n");

	add_item(({"studni�","du�� studni�","kryt� studni�"}),
		"Du�a, wymurowana z otoczak�w studnia z sporym daszkiem z jednej, "+
		"a �urawiem z drugiej strony stanowi centrum placu na kt�rym "+
		"odpoczywaj� podr�ni i spotykaj� si� miejscowi. Stoj�cy obok "+
		"niej ceber jest niemal pe�en czystej wody.\n");


    add_prop(ROOM_I_INSIDE,0);
	add_object(BIALY_MOST_PRZEDMIOTY+"lampa_uliczna.c");

	//if (pora_roku() != MT_ZIMA && TEMPERATURA(TO) > 0)
       set_drink_places("z cebra");

	add_npc(BIALY_MOST_NPC + "mieszczanin2");
}
public string
exits_description() 
{
    return "Ulice wiod� na p�nocny-zach�d oraz p�nocny-wsch�d oraz na po�udnie.\n";
}


void
init()
{
    ::init();
    init_drink_water(); 
} 

void
drink_effect(string skad)
{
	if(pora_roku() == MT_LATO)
	{
	write("Zanurzasz d�onie w ciep�� wod� z cebra i podnosisz j� do ust.\n");
    saybb(QCIMIE(this_player(), PL_MIA) + " zanurza d�onie w ciep�� wod� "+
          "i pije �yk wody z cebra.\n");

	}
	else
	{
    write("Zanurzasz d�onie w ch�odn� wod� z cebra i podnosisz j� do ust.\n");
    saybb(QCIMIE(this_player(), PL_MIA) + " zanurza d�onie w ch�odn� wod� "+
          "i pije �yk wody z fontanny.\n");
	}
}

string
dlugasny()
{
	string str="";

	if(jest_dzien())
	str+="W tym miejscu zbiegaj� si� dwie spore ulice oraz trakt "+
		"wiod�cy przez most na po�udnie. Na �rodku placu znajduje si� "+
		"du�a, kryta studnia z �urawiem okupowana przez przejezdnych. "+
		"Wozy zapakowane przer�nymi towarami regularnie przebywaj� "+
		"j� w obie strony. Schludne kamieniczki otaczaj�ce placyk "+
		"dowodz� maj�tno�ci mieszka�c�w, za� w�a�ciciele sklep�w i "+
		"kram�w uwijaj� si� w�r�d nat�oku. Nawet blaszane lampy "+
		"umieszczone regularnie na �cianach s� nowe i nie�le wykonane.";
	else
	str+="W tym miejscu zbiegaj� si� dwie spore ulice oraz trakt wiod�cy "+
		"przez most na po�udnie. Na �rodku placu stoi w ciszy du�a, "+
		"kryta studnia z �urawiem. Schludne kamieniczki otaczaj�ce placyk "+
		"dowodz� maj�tno�ci mieszka�c�w. Liczne sklepy i kramy s� o tej "+
		"porze puste i ciche. Blaszane lampy umieszczone regularnie na "+
		"�cianach roz�wietlaj� mroki nocnych godzin.";

	str+="\n";
	return str;
}
