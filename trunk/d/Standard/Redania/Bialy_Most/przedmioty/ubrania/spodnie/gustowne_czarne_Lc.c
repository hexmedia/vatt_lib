/*zrobione przez valor dnia 17 lipca
* Przerobki by Eleb.
*/

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <macros.h>
#include <materialy.h>
#include <object_types.h>



void
create_armour() 
{
      ustaw_nazwe_glowna("para");
      ustaw_nazwe("spodnie");
      
     dodaj_przym("gustowny","gustowni");
     dodaj_przym("czarny","czarni");
      
     set_long("Te niezwykle gustowne spodnie uszyto z idealnie czarnego "+
		 "materia^lu. Cienka, szorska tkanina zaprasowana zosta^la na r^owny "+
		 "kant, podkre^slaj^ac wykwintno^s^c odzienia. D^lugie nogawki "+
		 "nieznacznie podwini^eto i zaszyto dodaj^ac tym ubiorowi elegancji. "+
		 "Spodnie posiadaj^a dwie niewielkie kieszenie po bokach oraz du^zy "+
		 "czarny guzik s^lu^z^acy do zapi^ecia spodni. \n");

     
    
     set_slots(A_LEGS, A_HIPS);
     add_prop(OBJ_I_VOLUME, 300);
     add_prop(OBJ_I_VALUE, 150);
     add_prop(OBJ_I_WEIGHT, 150);
     add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
     add_prop(ARMOUR_I_DLA_PLCI, 0);

     set_size("L");
     ustaw_material(MATERIALY_LEN, 100);
     

     set_type(O_UBRANIA);
}