inherit "/std/object";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include "/sys/object_types.h"
#include <macros.h>
#include "dir.h"

void
create_object() 
{
    ustaw_nazwe("obraz");
    dodaj_przym("ma�y","mali");
    dodaj_przym("stary","starzy");
    set_long("Obraz ten, namalowany farbami olejnymi, pokryty jest cienka "+
        "warstewka kurzu. Przedstawia s�dziwego cz�owieka, o d�ugiej siwej "+
        "brodzie, z sumiastymi w�sami i bujna prawie bia�� czupryna. "+
        "U�miech jego jest lekki i jakby ironiczny, wzrok skierowany jest "+
        "na ciebie, a malarz dzi�ki swemu kunsztowi spowodowa�, i� zdaje "+
        "si�, �e pod��a on za tob� krok w krok. Ubranie m�czyzny jest "+
        "schludne, i do�� kosztowe, w ko�cu nie ka�dy m�g� sobie pozwoli� "+
        "na portret swojej osoby.\n");    
    set_owners(({BIALY_MOST_NPC + "burmistrz"}));      
	set_type(O_SCIENNE);
    add_prop(OBJ_I_WEIGHT, 1000);
    add_prop(OBJ_I_VOLUME, 2000);
    add_prop(OBJ_I_VALUE, 200);
}
