/*
 * ^Lawka do aresztu BM
 * opis by Khaes
 * zepsula faeve
 * dn. 21 maja 2007
 */

inherit "/std/object.c";

#include "dir.h"
#include <pl.h>
#include <stdproperties.h>
#include <macros.h>
#include <filter_funs.h>
#include <object_types.h>
#include <materialy.h>


create_object()
{
    ustaw_nazwe("^lawa");

    make_me_sitable("na","na ^lawce","z ^lawki",3, PL_MIE,
"na");

    ustaw_material(MATERIALY_DR_SOSNA);

    set_long("^Lawa, o ile tak mo^zna nazwa^c ten przedmiot jest masywn^a "+
		"desk^a umieszczon^a na dw^och kamiennych wyst^epach. Na jednej z "+
		"ko^nc^ow deski, kto^s wyry^l nad wyraz starannie spro^sny obrazek "+
		"przedstawiaj^acy m^eski narz^ad rozrodczy. \n");
    
    add_prop(OBJ_I_WEIGHT, 10000);
    add_prop(OBJ_I_VOLUME, 1513);
    add_prop(OBJ_I_VALUE, 30);
	add_prop(OBJ_I_NO_GET, "To raczej niewykonalne - sp^ojrz - deski s^a "+
		"przytwierdzone do kamiennych wypustk^ow, ca^lo^sci nie da rady "+
		"ruszy^c z miejsca. \n");
    set_owners(({BIALY_MOST_NPC + "boldrik"}));
}

public int
jestem_lawa_aresztu_BM()
{
        return 1;
}

public int
query_type()
{
    return O_MEBLE;
}
