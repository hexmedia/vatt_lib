
inherit "/std/object";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include "/sys/object_types.h"
#include <macros.h>
#include "dir.h"

void
create_object() 
{
      ustaw_nazwe("obraz");
      dodaj_przym("spory","sporzy");
      dodaj_przym("stary","starzy");
      set_long("Dzieci�ca twarz o roze�mianym wzroku, i pyzatych policzkach patrzy na"+
               " ciebie z tego obrazu. Szeroki u�miech pokazuj�cy ma�e, bia�e z�bki, i lekki" +
               " wicherek na g�owie pasuj� idealnie do tego dziecka. Cos w og�lnym wygl�dzie, po"+
               " z�o�eniu wszystkich cz�ci powoduje i� ch�opiec ten wygl�da na urwisa i na pewno"+ 
               " przysporzy� wielu utrapie� rodzicom.\n");

      set_owners(({BIALY_MOST_NPC + "burmistrz"}));   
      set_type(O_SCIENNE);
      add_prop(OBJ_I_WEIGHT, 1000);
      add_prop(OBJ_I_VOLUME, 2000);
      add_prop(OBJ_I_VALUE, 200);


}
