/* Autor: Duana
  Opis : sniegulak
  Data : 23.05.2007
*/

inherit "/std/object";

#include "/sys/macros.h"
#include "/sys/pl.h"
#include "/sys/materialy.h"
#include "/sys/object_types.h"
#include "/sys/formulas.h"
#include "dir.h"

void
create_object()
{
    ustaw_nazwe("pojemnik");
    dodaj_przym("szczelny", "szczelni");

    set_long("Szczelny pojemnik zrobiony ze stali zamykany klapk^a z "+
        "zatrzaskiem, posiada na przedzie napis: S^ol.\n");
         
    add_prop(OBJ_I_WEIGHT, 3000);
    add_prop(OBJ_I_VOLUME, 3000);
    add_prop(OBJ_I_VALUE, 130);
    set_type(O_KUCHENNE);

    ustaw_material(MATERIALY_RZ_STAL);

    set_owners(({BIALY_MOST_NPC + "karczmarz_bialy_most"}));

}