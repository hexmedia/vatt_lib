/* Jest to zwykly wachlarz, sluzy do wachlowania siebie lub kogos :P
                   Lil 28.09.2005  */

inherit "/std/holdable_object";

#include <pl.h>
#include <macros.h>
#include <cmdparse.h>
#include <materialy.h>
#include <filter_funs.h>
#include <object_types.h>
#include <stdproperties.h>

#include "dir.h"

#define ROZPOSTARTY_WACHLARZ    "_rozpostarty_wachlarz"

string czy_rozpostarty();
string jak_pachnie, jak_wacha_jak_sie_zachowuje;

create_holdable_object()
{
    ustaw_nazwe("wachlarz");
    dodaj_przym("zielony","zieloni");
    dodaj_przym("zdobiony","zdobieni");

    add_prop(OBJ_I_WEIGHT, 130);
    add_prop(OBJ_I_VALUE, 734); //exclusiv!
    add_prop(OBJ_I_VOLUME, 170);
    ustaw_material(MATERIALY_KORDONEK, 90);
    ustaw_material(MATERIALY_DR_CIS, 10);

    set_long("Wachlarz ma drewniane uchwyty, a mi�dzy nimi @@czy_rozpostarty@@ "+
             "zielony materia� z misternie wychaftowanymi li�cmi. Ca�y materia� otacza "+
             "wij�cy si� dooko�a bluszcz, natomiast delikatne deseczki zdobi bogaty motyw "+
             "kwiatowy.\n");

}

string
czy_rozpostarty()
{
    if(this_object()->query_prop(ROZPOSTARTY_WACHLARZ))
        return "pi�knie rozpo�ciera si�, niczym pawi ogon";

    return "z�o�ony jest";
}

public int
rozloz(string str)
{
//    string str;
    object wachlarzyk;
    notify_fail("Roz�� co?\n");

    // temu! :P
    if (!str) return 0;

    if (!parse_command(str, all_inventory(this_player()), " %o:" + PL_BIE, wachlarzyk))
        return 0;

    if (wachlarzyk != this_object())
        return 0;

    if (wachlarzyk->query_prop(ROZPOSTARTY_WACHLARZ))
    {
        write("Ale� ten wachlarz ju� jest roz�o�ony!\n");
        return 1;
    }
    if (!wachlarzyk->query_wielded())
    {
        write("Musisz wpierw chwyci� ten wachlarz.\n");
        return 1;
    }

    wachlarzyk->add_prop(ROZPOSTARTY_WACHLARZ, 1);
    write("Rozk�adasz majestatycznie "+wachlarzyk->short(PL_BIE)+" ukazuj�c "+
          "jego malownicze ro�linne wzory na tkaninie.\n");
    saybb(QCIMIE(this_player(),PL_MIA) + " rozk�ada majestatycznie " +
          wachlarzyk->short(PL_BIE)+" ukazuj�c jego malownicze ro�linne wzory "+
          "na tkaninie.\n");
    return 1;

}

public int
zloz(string str)
{
//    string str;
    object wachlarzyk;
    notify_fail("Z�� co?\n");

    if (!str) return 0;

    if (!parse_command(str, all_inventory(this_player()), " %o:" + PL_BIE, wachlarzyk))
        return 0;

    if (wachlarzyk != this_object())
        return 0;
    if (!wachlarzyk->query_prop(ROZPOSTARTY_WACHLARZ))
        notify_fail("Ale� ten wachlarz ju� jest z�o�ony!\n");

    wachlarzyk->remove_prop(ROZPOSTARTY_WACHLARZ);
    write("Sk�adasz "+wachlarzyk->short(PL_BIE)+".\n");
    saybb(QCIMIE(this_player(),PL_MIA) + " sk�ada "+
          wachlarzyk->short(PL_BIE)+".\n");
    return 1;
}

public int
wachluj(string str)
{
//    string str;
    object wachlarzyk;
    object *cele;
    notify_fail("Powachluj si� lub kogo�, czym?\n");

	if(!str)
		return 0;

    if (parse_command(str, all_inventory(this_player()), " 'si�' %o:" + PL_NAR, wachlarzyk))
    {
        if (wachlarzyk != this_object())
            return 0;
        write("Wachlujesz si� "+wachlarzyk->short(PL_NAR)+
            ". Delikatny wiaterek muska tw� twarz.\n");
        saybb(QCIMIE(this_player(),PL_MIA) + " wachluje si� "+
        wachlarzyk->short(PL_NAR)+".\n");
        return 1;
    }

    if (!parse_command(str, all_inventory(environment(this_player())) +
		all_inventory(this_player()), "%l:"+PL_BIE+" %o:" + PL_NAR, cele, wachlarzyk))
        return 0;

    cele = NORMAL_ACCESS(cele, 0, 0);

    if (sizeof(cele) >= 2)
    {
        notify_fail("Potrafisz wachlowa� tylko jedn� istot� naraz.\n");
        return 0;
    }

    if(sizeof(cele)==0)
    {
        notify_fail("Kogo chcesz powachlowa�?\n");
        return 0;
    }

    if (wachlarzyk != this_object())
        return 0;

    if(!wachlarzyk->query_wielded())
    {
        notify_fail("Musisz wpierw chwyci� ten wachlarz.\n");
        return 0;
    }
    if(!wachlarzyk->query_prop(ROZPOSTARTY_WACHLARZ))
    {
        notify_fail("Musisz wpierw roz�o�y� ten wachlarz.\n");
        return 0;
    }

    write("Wachlujesz "+cele[0]->short(PL_BIE)+" "+
        wachlarzyk->short(PL_NAR)+".\n");
    saybb(QCIMIE(this_player(),PL_MIA) + " wachluje "+
        cele[0]->short(PL_BIE)+" "+wachlarzyk->short(PL_NAR)+".\n", ({ this_player(), cele[0] }));
    cele[0]->catch_msg(QCIMIE(this_player(),PL_MIA) + " zaczyna ci� wachlowa�. "+
        "Delikatny wiaterek muska tw� twarz.\n");

    return 1;
}

public int
pomoc(string str)
{
    object ob;
    notify_fail("Nie ma pomocy na ten temat.\n");
    if (!str) return 0;
    if (!parse_command(str, TP, "%o:" + PL_MIA, ob))
        return 0;
    if (!ob) return 0;
    if (ob != TO)
        return 0;

    write("Spr�buj si� nim powachlowa�, lub, je�li chcesz - powachluj nim kogo� innego.\n");
    return 1;
}

init()
{
    ::init();
    add_action(rozloz,  "roz��");
    add_action(zloz,    "z��");
    add_action(wachluj, "powachluj");
    add_action(pomoc,"?",2);
}
