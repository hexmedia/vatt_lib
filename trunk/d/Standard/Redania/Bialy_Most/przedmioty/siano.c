 /*
 Made by Eleb.
 */


inherit "/std/object.c";

#include "/sys/stdproperties.h"
#include "/sys/macros.h"
#include "/sys/pl.h"
#include "/sys/materialy.h"
#include "/sys/object_types.h"
#include "dir.h"


void
create_object(){

	ustaw_nazwe("siano");
	set_long("Spora g�rka siana usypana pod jedn� ze �cian.\n");
	make_me_sitable("na","na sianie","z siana",2);
	ustaw_material(MATERIALY_SLOMA, 100);
	add_prop(OBJ_I_NO_GET,1);
	add_prop(CONT_I_NO_OPEN_DESC, 1);
	add_prop(CONT_I_CANT_OPENCLOSE, 1);
	add_prop(CONT_I_DONT_SHOW_CONTENTS, 1);
}
