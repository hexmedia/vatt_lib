/*
 * krzes^lo garnizonowe
 * opis Khaesa
 * zepsu^la faeve
 * 12 maja 2007
 *
 */

inherit "/std/object";

#include "/d/Standard/Redania/Bialy_Most/przedmioty/dir.h"
#include <macros.h>
#include <materialy.h>
#include <pl.h>
#include <stdproperties.h>
#include <object_types.h>
#include "dir.h"

void create_object()
{
  ustaw_nazwe("krzes^lo");

  dodaj_przym("prosty", "pro^sci");
  dodaj_przym("drewniany", "drewniani");
  
  set_long("Zwyk^le drewniane krzes^lo, jest do^s^c porz^adnie zbite, ale "+
	  "prze^zy^lo ju^z chyba swoje najlepsze lata, teraz czeka^c tylko a^z "+
	  "si^e rozpadnie. Farba, miejscami ca^lkiem starta, gdzie indziej "+
	  "^luszczy si^e, odchodz^ac ca^lymi p^latami. Siedzisko i oparcie nie "+
	  "maj^a ^zadnych zdobie^n, za to zosta^ly do^s^c standardowo "+
	  "wyprofilowane, tak by by^ly w miar^e wygodne dla ka^zdego. A "+
	  "przynajmniej by nie powodowa^ly odcisk^ow w nieodpowiednich "+
	  "miejscach. \n");

  add_prop(OBJ_I_VALUE, 40);
  add_prop(OBJ_I_WEIGHT, 3200);
  add_prop(OBJ_I_VOLUME, 6100);

  make_me_sitable("na", "na krze^sle", "z krzes^la", 1);

  set_type(O_MEBLE);

  ustaw_material(MATERIALY_DR_DAB, 100);
    set_owners(({BIALY_MOST_NPC + "boldrik"}));
}

init()
{
  ::init();
}

public int
jestem_krzeslem_garnizonu()
{
        return 1;
}