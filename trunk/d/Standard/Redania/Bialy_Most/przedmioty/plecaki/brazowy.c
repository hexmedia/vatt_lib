/* By Ohm */ 
 
inherit "/std/plecak.c";

#include "/sys/formulas.h"
#include <stdproperties.h>
#include <wa_types.h>
#include <pl.h>
#include <materialy.h>
#include <object_types.h>

void
create_backpack()
{
	ustaw_nazwe("plecak");
    dodaj_przym("spory","sporzy");
	dodaj_przym("br^azowy","sk^orzany");
	odmien_short();
	set_long("Do^s^c sporych rozmiar^ow pojemnik zosta^l wykonany z grubej, "+
		"wytrzyma^lej sk^ory bydl^ecej, kt^ora po porz^adnym wygarbowaniu i "+
		"zaimpregnowaniu nabra^la ciep^lego jasnobr^azowego koloru. Proste a "+
		"zarazem wytrzyma^le szwy, ^swiadcz^a o solidno^sci rzemie^slnika, "+
		"kt^ory wykona^l ten przedmiot. Wzmocnione sk^orzane pasy "+
		"przytwierdzone do g^l^ownej cz^e^sci za pomoc^a miedzianych nit^ow "+
		"wygl^adaj^a na dosy^c wygodne, a mi^ekki materia^l po wewn^etrznej "+
		"stronie, zapewnia wygod^e i brak obtar^c, nawet podczas d^lugich "+
		"podr^o^zy, z du^zym obci^a^zeniem. \n");

		set_keep(1);

		add_prop(OBJ_I_VALUE, 230);
		add_prop(CONT_I_CLOSED, 0);
		add_prop(CONT_I_WEIGHT, 1500);
		add_prop(CONT_I_VOLUME, 1500);
		add_prop(CONT_I_MAX_WEIGHT, 15000);
		add_prop(CONT_I_MAX_VOLUME, 17000);
		add_prop(CONT_I_REDUCE_VOLUME, 125);

		remove_prop(CONT_I_RIGID);
}
