/*
 * faevka, dn. 13.06.07
 * opis Anonima (pewnie Tinardan ;))
 *
 */

inherit "/std/sakiewka.c";

#include <pl.h>
#include <macros.h>
#include <filter_funs.h>
#include <stdproperties.h>
#include <composite.h>
#include <cmdparse.h>
#include <object_types.h>
#include <materialy.h>

void
create_sakiewka()
{
	ustaw_nazwe("sakiewka");
    dodaj_przym("szary","szarzy");
    dodaj_przym("po^latany","po^latani");
	set_long("Na pierwszy rzut oka, sakiewka ta nie wygl^ada na zbyt okaza^l^a "+
		"i wytrzyma^l^a. Jednak zosta^la zrobiona w ten spos^ob, by w^la^snie "+
		"takie wra^zenie sprawia^c. Niby to zniszczona i stara, a jednak "+
		"przy bli^zszych ogl^edzinach jest wbrew pozorom bardzo wytrzyma^la. "+
		"Wykonana z grubego materia^lu, na kt^orym kto^s specjalnie naszy^l "+
		"kilka ^lat, a szary kolor dope^lnia obrazu n^edzy i rozpaczy. "+
		"Posiada tak^ze gruby, czarny rzemie^n, kt^orym z ^latwo^sci^a "+
		"mo^zna sakw^e zabezpieczy^c. "+
		"@@opis_sublokacji|Dostrzegasz w niej |w|.\n|\n||" + PL_BIE+ "@@");


    add_prop(CONT_I_MAX_VOLUME, 4400);
    add_prop(CONT_I_VOLUME, 630);

    add_prop(CONT_I_WEIGHT, 750);
    add_prop(CONT_I_MAX_WEIGHT, 3550);

	ustaw_material(MATERIALY_TK_LEN , 95);
	ustaw_material(MATERIALY_SK_SWINIA, 5);
	
	add_prop(OBJ_I_VALUE, 342);

}
