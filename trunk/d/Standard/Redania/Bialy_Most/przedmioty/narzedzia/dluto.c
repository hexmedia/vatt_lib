/* Autor: Valor
   Opis:  Faeve
   Data:  12.06.07
*/
#include <macros.h>
#include <stdproperties.h>
#include <materialy.h>
#include <object_types.h>
#include <pl.h>
#include <tools.h>

inherit "/std/holdable_object";
inherit "/lib/tool";
create_holdable_object()
{
    ustaw_nazwe("d^luto");
    dodaj_przym("stalowy","stalowi");
    dodaj_przym("niewielki","niewielcy");
  
    set_long("Umieszczone na por^ecznym drewnianym trzonku ostrze "+
        "d^luta wykonane jest z mocnej stali. Ma ono do^s^c "+
        "popularny kszta^lt litery U. Pociemnia^la stal rozja^snia "+
        "si^e nieco u wylotu, tu^z przy ostrej jak brzytwa kraw^edzi "+
        "tn^acej. \n");
    add_prop(OBJ_I_WEIGHT, 100);
    add_prop(OBJ_I_VOLUME, 200);
    add_prop(OBJ_I_VALUE, 25);
    ustaw_material(MATERIALY_RZ_STAL);
    add_tool_domain(TOOL_DOMAIN_CARVING);
    set_tool_diffculty(60);
}
