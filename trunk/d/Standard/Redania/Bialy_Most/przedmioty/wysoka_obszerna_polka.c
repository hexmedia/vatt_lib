inherit "/std/container";

#include "dir.h"
#include <pl.h>
#include <macros.h>
#include <filter_funs.h>
#include <stdproperties.h>
#include <composite.h>
#include <cmdparse.h>
#include <object_types.h>
#include <materialy.h>

nomask void
create_container()
{
    ustaw_nazwe("p^o^lka");

    dodaj_przym("wysoki", "wysocy");
    dodaj_przym("obszerny", "obszerni");

    set_long("Wysoka pod sufit p^o^lka, sk^ladaj^aca si^e z kilku "+
		"kondygnacji. Ka^zdy kolejny jej poziom zape^lniony jest niemal "+
		"ca^lkowicie przez mniej lub bardziej u^lo^zone sterty dokument^ow. \n");
        
    ustaw_material(MATERIALY_DR_HEBAN);

add_item("dokumenty","Sterty dokument^ow u^lo^zone mniej lub bardziej na "+
	"poszczeg^olnych kondygnacjach p^o^lki.");

    setuid();
    seteuid(getuid());
    add_prop(CONT_I_MAX_VOLUME, 200000);
    add_prop(CONT_I_VOLUME, 150000);

    add_prop(OBJ_I_NO_GET, "W ^zaden spos^ob nie uda ci si^e jej podnie^s^c.\n");

    add_prop(CONT_I_WEIGHT, 100000);
    add_prop(CONT_I_MAX_WEIGHT, 200000);
    add_prop(CONT_I_CANT_WLOZ_DO, 1);

    add_subloc(({"pierwsza kondygnacja", "pierwszej kondygnacji", 
		"pierwszej kondygnacji","pierwsz^a kondygnacj^e", 
		"pierwsz^a kondygnacj^a", "pierwszej kondygnacji", "na"}));
    add_subloc(({"druga kondygnacja", "drugiej kondygnacji", 
		"drugiej kondygnacji","drug^a kondygnacj^e", "drug^a kondygnacj^a", 
		"drugiej kondygnacji", "na"}));
    add_subloc(({"trzecia kondygnacja", "trzeciej kondygnacjii", 
		"trzeciej kondygnacji","trzeci^a kondygnacj^e", 
		"trzeci^a kondygnacj^a", "trzeciej kondygnacji","na"}));

    add_subloc("pod");
    add_subloc("na");

    add_subloc_prop("pod", CONT_I_MAX_VOLUME, 500);
    add_subloc_prop("na", CONT_I_MAX_WEIGHT, 5000);

    add_prop(CONT_I_DONT_SHOW_CONTENTS, 1);

    add_item(({"kondygnacj^e", "pierwsz^a kondygnacj^e"}), 
		"@@opis_sublokacji|" +
    "Na pierwszej kondygnacji wysokiej obszernej p^o^lki |pierwsza "+
		"kondygnacja|.|"+
    "Pierwsza kondygnacja wysokiej obszernej p�ki jest pusta.|0|le^zy |le^z" +
    "^a |le^zy @@\n", PL_DOP);
    add_item("drug^a kondygnacj�", "@@opis_sublokacji|" +
    "Na drugiej kondygnacji wysokiej obszernej p�ki |druga kondygnacja|.|" +
    "Druga kondygnacja wysokiej obszernej p�ki jest pusta.|0|le^zy |le^z" +
    "^a |le^zy @@\n", PL_DOP);
    add_item("trzeci^a kondygnacj�", "@@opis_sublokacji|" +
    "Na trzeciej kondygnacji wysokiej obszernej p�ki |trzecia kondygnacja|.|"+
    "Trzecia kondygnacja wysokiej drewnianej p�ki jest pusta.|0|le^zy | le^z"+
    "^a |le^zy @@\n", PL_DOP);
    
    add_subloc_prop("pierwsza kondygnacja", 
		SUBLOC_S_OB_GDZIE, "na pierwszej kondygnacji");
    add_subloc_prop("pierwsza kondygnacja", SUBLOC_I_OB_PRZYP, PL_DOP);
    add_subloc_prop("druga kondygnacja", 
		SUBLOC_S_OB_GDZIE, "na drugiej kondygnacji");
    add_subloc_prop("druga kondygnacja", SUBLOC_I_OB_PRZYP, PL_DOP);
    add_subloc_prop("trzecia kondygnacja", 
		SUBLOC_S_OB_GDZIE, "na trzeciej kondygnacji");
    add_subloc_prop("trzecia kondygnacja", SUBLOC_I_OB_PRZYP, PL_DOP);

    set_owners(({BIALY_MOST_NPC + "boldrik"}));
}

public int
query_type()
{
    return O_MEBLE;
}