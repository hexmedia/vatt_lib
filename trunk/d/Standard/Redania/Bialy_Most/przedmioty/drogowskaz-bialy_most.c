inherit "/std/object.c";

#include <pl.h>
#include <stdproperties.h>
#include <macros.h>
#include <filter_funs.h>

create_object()
{

    ustaw_nazwe("drogowskaz");
    
    dodaj_przym("du^zy","duzi");
    dodaj_przym("zabrudzony","zabrudzeni");

    set_long("    **************************\n"+
             "    *                        *\n"+
             "    *  OXENFURT, TRETOGOR    *\n"+
             "    *           |            *\n"+
             "    *           |            *\n"+
             "    *         PIANA          *\n"+
             "    *           |            *\n"+
             "    *           |            *\n"+
             "    *           O---RINDE    *\n"+
             "    *           |            *\n"+
             "    *           |            *\n"+
             "    *           |            *\n"+
             "    *        WYZIMA          *\n"+
             "    *                        *\n"+
             "    **************************\n"+
             "              |    |          \n"+
             "              |    |          \n"+
             "              |    |          \n");
    add_prop(OBJ_I_NO_GET, "Ten drogowskaz jest mocno wbity "+
                           "w ziemi^e, nie masz do^s^c si^ly by go "+
                           "wyrwa^c.\n");

}
