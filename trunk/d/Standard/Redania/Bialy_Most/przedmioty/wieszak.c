/*
 * taki sobie wieszak
 * do ^la^xni w BM
 */

inherit "/std/container";

#include "dir.h"
#include "/sys/stdproperties.h"
#include "/sys/macros.h"
#include "/sys/pl.h"
#include "/sys/materialy.h"
#include "/sys/object_types.h"
#include <filter_funs.h>


void
create_container()
{
    ustaw_nazwe("wieszak");

    ustaw_material(MATERIALY_DR_DAB);
    set_type(O_MEBLE);
   set_long("Ma^le, drewniane bolce wystaj^ace ze ^sciany, s^lu^z^a zapewne "+
	   "do wieszania ubra^n przed k^apiel^a. "
   +"@@opis_sublokacji|Na wieszaku powieszono |na|.\n|\n|" + PL_BIE+ "@@");
	     
    add_prop(CONT_I_WEIGHT, 100);
    add_prop(CONT_I_VOLUME, 100);
    add_prop(OBJ_I_VALUE, 56);
	add_prop(OBJ_M_NO_GET, "Ko^lki s^a wbite w ^scian^e, wi^ec jak chcesz "+
		"je wzi^a^c?");
	
    add_subloc_prop("na", CONT_I_MAX_WEIGHT, 100000);
    add_subloc_prop("na", CONT_I_MAX_VOLUME, 150000);

	set_type(O_MEBLE);
    add_subloc("na", 0, "ze");
    add_subloc_prop("na", SUBLOC_I_MOZNA_POWIES, 1);
    add_subloc_prop("na", SUBLOC_I_DLA_O, O_UBRANIA);
    add_subloc_prop("na", CONT_I_MAX_RZECZY, 8);
    add_prop(CONT_I_CANT_WLOZ_DO, 1);
    add_prop(CONT_I_DONT_SHOW_CONTENTS, 1);

}

