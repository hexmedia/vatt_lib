/* Autor: Valor (poprawki: Sed)
   Opis : �niegulak
   Data : 10.04.07 */
inherit "/std/pochwa";
#include "/sys/formulas.h"
#include <stdproperties.h>
#include <object_types.h>
#include <materialy.h>
#include <macros.h>
#include <cmdparse.h>
#include <pl.h>
#define POCHWA_PD_PLECY         1
#define POCHWA_PD_UDA           2
#define POCHWA_PD_GOLENIE       4
#define POCHWA_PD_PAS           8
#define POCHWA_NA_PLECACH       "_pochwa_na_plecach"
#define POCHWA_PRZY_UDZIE_P     "_pochwa_przy_udzie_p"
#define POCHWA_PRZY_UDZIE_L     "_pochwa_przy_udzie_l"
#define POCHWA_PRZY_GOLENI_P    "_pochwa_przy_goleni_p"
#define POCHWA_PRZY_GOLENI_L    "_pochwa_przy_goleni_l"
#define POCHWA_W_PASIE_P        "_pochwa_w_pasie_p"
#define POCHWA_W_PASIE_L        "_pochwa_w_pasie_l"
#define POCHWA_SUBLOC           "_pochwa_subloc"
void
create_pochwa()
{
    ustaw_nazwe("pochwa");
    dodaj_przym("ma�y","mali");
    dodaj_przym("kunsztowny","kunsztowni");
    set_long("Ogl^adasz ma^la kunsztown^a pochw^e w kt^orej mo^zesz "+
        "schowa^c sztylet. Ju^z na pierwszy rzut oka widzisz, ^ze nie jest "+
        "to przeci^etna i zwyczajna pochwa. Stalowy korpus zosta^l pokryty "+
        "cienk^a warstw^a srebra, na kt^orej nast^epnie umieszczono zawi^le "+
        "zdobienia przypominaj^ace p^edy winoro^sli. Wsuwanie broni do "+
        "pochwy u^latwia ma^la prowadnica i wy^lo^zony perkalem otw^or "+
        "wycieraj^acy bro^n oraz pozwala w^la^scicielowi pochwy na ciche i "+
        "^latwe wysuwanie broni. Sk^orzane paski dodatkowo obszyte srebrn^a "+
        "nici^a umo^zliwiaj^a przypi^ecie jej do pasa lub nogi. \n");

    set_przypinane_do(POCHWA_PD_GOLENIE | POCHWA_PD_UDA | POCHWA_PD_PAS );
    add_subloc_prop(0, SUBLOC_I_DLA_O, O_BRON_SZTYLETY);
    set_type(O_POCHWY);
    add_prop(OBJ_I_WEIGHT, 1800);
    add_prop(OBJ_I_VOLUME, 1300);
    add_prop(CONT_I_REDUCE_VOLUME, 125);
    add_prop(CONT_I_MAX_WEIGHT, 5000);
    add_prop(CONT_I_MAX_VOLUME, 5000);
    add_prop(CONT_I_MAX_RZECZY, 1);

   ustaw_material(MATERIALY_RZ_STAL, 75);
	ustaw_material(MATERIALY_SZ_SREBRO, 5);
	ustaw_material(MATERIALY_SK_SWINIA, 15);
ustaw_material(MATERIALY_TK_BAWELNA, 5);

add_prop(OBJ_I_VALUE, 1240);

}

