/** @package 

        jednoosobowe_lozko.c
        
        Copyright(c) ppp 2000
        
        Author: Eleb
        Created: E   2007-04-18 01:24:04
Last change: E 2007-04-18 01:52:25
*/
inherit "/std/container";

#include "/sys/stdproperties.h"
#include "/sys/macros.h"
#include "/sys/pl.h"
#include "/sys/materialy.h"
#include "/sys/object_types.h"
#include "dir.h"

void
create_container(){
    ustaw_nazwe("��ko",PL_NIJAKI_NOS);
    dodaj_przym("prosty", "pro�ci");
    dodaj_przym("jednoosobowy","jednoosobowi");
    ustaw_material(MATERIALY_DR_BUK, 70);
    ustaw_material(MATERIALY_ZELAZO, 10);
    ustaw_material(MATERIALY_LEN, 20);

    set_type(O_MEBLE);

    set_long("Widac iz to ��ko zrobione jest dla os�b, kt�re raczej"+
             " ma�o spi�."+
             " Prostacka"+
             " konstrukcja, niewygodny �iennik, oraz inne zaniedbania"+
             " na pewno nie"+
             " uczyni� snu"+
             " na tym meblu rzecz� przyjemn�.\n"+
        "@@opis_sublokacji|Dostrzegasz pod nim |pod|.\n||" +
        PL_BIE+ "@@");

    set_owners(({BIALY_MOST_NPC + "burmistrz"}));
    make_me_sitable("na","na ��ku","z ��ka",2);
    
    add_prop(CONT_I_WEIGHT, 100000);
    add_prop(CONT_I_VOLUME, 200000);
    add_prop(OBJ_I_VALUE, 100);
    


    add_subloc("pod");
    add_subloc_prop("pod", CONT_I_MAX_WEIGHT, 5000);
    add_subloc_prop("pod", CONT_I_MAX_VOLUME, 5000);
    add_prop(CONT_I_DONT_SHOW_CONTENTS, 1);
}

