/* 
 * faeve, 13.06.07
 * opis: Oldzio
 *
 */

inherit "/std/belt";
#include <macros.h>
#include <stdproperties.h>
#include <materialy.h>

create_belt()
{
    ustaw_nazwe("pas");
    dodaj_nazwy("pasek");
    dodaj_przym("w^aski", "w^ascy");
    dodaj_przym("czerwony", "czerwoni");
    
    set_long("B^lyszcz^acy, przebarwiony na czerwono pasek, zosta^l "+
		"niew^atpliwie wykonany ze sk^ory jakiego^s gada. Dumnie skrzy si^e "+
		"wszystkimi kolorami t^eczy, ze wzgl^edu na liczne kamyczki "+
		"uk^ladaj^ace si^e w zawi^ly wzr^or na ca^lej jego d^lugo^sci. "+
		"Dumnie wie�czy go poz^lacana klamra w kszta^lcie kwiatu r^o^zy o "+
		"rozwini^etych p^latkach. \n");
    
    set_max_slots_zat(4);
    set_max_slots_prz(4);
    set_max_weight_zat(2000);
    set_max_weight_prz(3800);
    set_max_volume_zat(2000);
    set_max_volume_prz(2000);
    
    add_prop(OBJ_I_WEIGHT, 1800);
    add_prop(OBJ_I_VOLUME, 1350);
	add_prop(OBJ_I_VALUE, 1103);
    
    ustaw_material(MATERIALY_SK_SWINIA, 85);
    ustaw_material(MATERIALY_RZ_MOSIADZ, 10);
	ustaw_material(MATERIALY_SZ_ZLOTO, 5);

}
