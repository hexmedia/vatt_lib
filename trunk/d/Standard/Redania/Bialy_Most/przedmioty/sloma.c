 /*
 Made by Eleb.
 */


inherit "/std/object.c";

#include "/sys/stdproperties.h"
#include "/sys/macros.h"
#include "/sys/pl.h"
#include "/sys/materialy.h"
#include "/sys/object_types.h"
#include "dir.h"


void
create_object(){

	ustaw_nazwe("s^loma",PL_NIJAKI_NOS);
	set_long("Spora g^orka s^lomy usypana pod jedn^a ze ^scian.\n");
	make_me_sitable("na","na s^lomie","ze s^lomy",2);
	ustaw_material(MATERIALY_SLOMA, 100);
	add_prop(OBJ_I_NO_GET,1);
	add_prop(CONT_I_NO_OPEN_DESC, 1);
	add_prop(CONT_I_CANT_OPENCLOSE, 1);
	add_prop(CONT_I_DONT_SHOW_CONTENTS, 1);
}
