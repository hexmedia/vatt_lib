/* Autor: Valor (poprawki: Sed)
   Opis : �niegulak
   Data : 10.04.07 */
inherit "/std/pochwa";
#include "/sys/formulas.h"
#include <stdproperties.h>
#include <object_types.h>
#include <materialy.h>
#include <macros.h>
#include <cmdparse.h>
#include <pl.h>
#define POCHWA_PD_PLECY         1
#define POCHWA_PD_UDA           2
#define POCHWA_PD_GOLENIE       4
#define POCHWA_PD_PAS           8
#define POCHWA_NA_PLECACH       "_pochwa_na_plecach"
#define POCHWA_PRZY_UDZIE_P     "_pochwa_przy_udzie_p"
#define POCHWA_PRZY_UDZIE_L     "_pochwa_przy_udzie_l"
#define POCHWA_PRZY_GOLENI_P    "_pochwa_przy_goleni_p"
#define POCHWA_PRZY_GOLENI_L    "_pochwa_przy_goleni_l"
#define POCHWA_W_PASIE_P        "_pochwa_w_pasie_p"
#define POCHWA_W_PASIE_L        "_pochwa_w_pasie_l"
#define POCHWA_SUBLOC           "_pochwa_subloc"
void
create_pochwa()
{
    ustaw_nazwe("uprzaz");
    dodaj_przym("kunsztowny","kunsztowni");
    dodaj_przym("sk^orzany","sk^orzani");
    set_long("Ogl^adasz kunsztown^a, sk^orzan^a uprz^a^z, w kt^orej mo^zesz "+
        "umie^sci^c kr^otk^a bro^n drzewcow^a lub dwureczny miecz. "+
        "Pi^ekny okaz roboty rzemie^slnika natychmiast daje o sobie zna^c "+
        "gdy przygl^adasz si^e stalowemu korpusowi pokrytego cienk^a "+
        "warstw^a srebra. Zauwa^zasz na nim grawerowania w kszta^lcie "+
        "winoro^sli pn^acej w g^ore. Ca^la konstukcja przemy^slnie "+
        "po^l^aczona sk^orzanymi, obszytymi srebrn^a nici^a, pasami "+
        "wyg^lada lekko a zarazem trwale. Uprz^a^z t^a mo^zna "+
        "przewiesi^c przez plecy. \n");
    set_przypinane_do(POCHWA_PD_PLECY);
    add_subloc_prop(0, SUBLOC_I_DLA_O, O_BRON_DRZEWCOWE_K | O_BRON_MIECZE_2H);
    set_type(O_POCHWY);
    add_prop(OBJ_I_WEIGHT, 1500);
    add_prop(OBJ_I_VOLUME, 1500);
    add_prop(CONT_I_REDUCE_VOLUME, 150);
    add_prop(CONT_I_MAX_WEIGHT, 10000);
    add_prop(CONT_I_MAX_VOLUME, 10000);
    add_prop(CONT_I_MAX_RZECZY, 1);

   ustaw_material(MATERIALY_RZ_STAL, 55);
	ustaw_material(MATERIALY_SZ_SREBRO, 5);
	ustaw_material(MATERIALY_SK_SWINIA, 40);

		add_prop(OBJ_I_VALUE, 1550);

}
