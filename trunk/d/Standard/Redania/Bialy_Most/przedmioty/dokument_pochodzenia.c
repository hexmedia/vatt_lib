// vera

inherit "/std/object";
inherit "/lib/keep.c";
#include <pl.h>
#include <stdproperties.h>

string message;

void
create_object()
{
    ustaw_nazwe("dokument");
	dodaj_przym("niewielki","niewielcy");
    set_long("Niewielki kawa�ek ��tawego i chropowatego papieru, na kt�rym "+
		"zdobionymi literami jest co� napisane.\n");
    
    add_prop(OBJ_I_WEIGHT, 1);
    add_prop(OBJ_I_VOLUME, 10);
    add_prop(OBJ_M_NO_DROP,1);

    set_keep(1);
}

void
init()
{
    ::init();
    add_action("przeczytaj", "przeczytaj");
}

int
przeczytaj(string str)
{
    object kartka;

    notify_fail("Przeczytaj co?\n");

    if (!str || !parse_command(str, this_player(), "%o:" + PL_BIE, kartka) || kartka != this_object())
	return 0;

    this_player()->more("Czytasz tre^s^c dokumentu:\n" + message);
    return 1;
}

public void
set_message(string m)
{
    message = m;
}

public string
query_message()
{
    return message;
}

string
query_doku_auto_load()
{
    return "#DOKUMENT_BIALY_MOST_POCHO#" + query_message() + "##" + query_long() + "#DOKUMENT_BIALY_MOST_POCHO#";
}


public void
init_doku_arg(string arg)
{
    string reszt;
    string wiad;
    string long;
    
    sscanf(arg, "%s#DOKUMENT_BIALY_MOST_POCHO#%s##%s#DOKUMENT_BIALY_MOST_POCHO#%s", reszt, wiad, long, reszt);
    
    set_message(wiad);
    set_long(long);
}

public string
query_auto_load()
{
    return ::query_auto_load() + query_doku_auto_load();
}

public void
init_arg(string arg)
{
    ::init_arg(arg);
    init_doku_arg(arg);
}
