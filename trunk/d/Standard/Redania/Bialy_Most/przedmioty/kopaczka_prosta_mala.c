/* Autor: Duana
  Opis : sniegulak
  Data : 23.05.2007
*/

inherit "/std/holdable_object";

#include "/sys/macros.h"
#include "/sys/pl.h"
#include "/sys/materialy.h"
#include "/sys/object_types.h"
#include "/sys/formulas.h"
#include "dir.h"

void
create_holdable_object()
{
    ustaw_nazwe("kopaczka");
    dodaj_przym("prosty", "pro^sci");
    dodaj_przym("ma^ly", "mali");

    set_long("Drewniany trzonek z oheblowanego drewna zosta^l zako^nczony "+
        "ma^lymi kolcami przechodz^acymi z drugiej strony w p^laska p^lytk^e.\n");
         
    add_prop(OBJ_I_WEIGHT, 6000);
    add_prop(OBJ_I_VOLUME, 6000);
    add_prop(OBJ_I_VALUE, 560);
    set_type(O_NARZEDZIA);

    ustaw_material(MATERIALY_DR_DAB, 70);
    ustaw_material(MATERIALY_RZ_ZELAZO, 30);
    set_owners(({BIALY_MOST_NPC + "karczmarz_bialy_most"}));

}