
inherit "/std/object";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include "/sys/object_types.h"
#include <macros.h>
#include "dir.h"

void
create_object() 
{
      ustaw_nazwe("obraz");
      dodaj_przym("zakurzony","zakurzeni");
      dodaj_przym("stary","starzy");
      set_long("Kobieta o wynios�ej twarzy i zimnym wzroku przedstawiona na tym"+
               " obrazie, wygl�da na bardzo charyzmatyczna osob�, kt�rej osobowo�� na pewno"+
               " przyt�acza�a innych. Usta zaci�ni�te w cienka linie oraz ostre i wystaj�ce ko�ci policzkowe"+
               " po��czone z bardzo jasna cera, nadaj� jej trupi wygl�d. Wypiel�gnowane"+
               " d�onie o d�ugich palcach upstrzonych pier�cieniami z�o�y�a na eleganckiej" +
               " ciemnozielonej sukience. Gruba d�bowa rama otaczaj�ca obraz jest zdobiona w delikatne"+
               " wzory, kt�re dodatkowo zosta�y pokryte cieniutka warstewka srebra.\n");

      set_owners(({BIALY_MOST_NPC + "burmistrz"}));   
      set_type(O_SCIENNE);
      add_prop(OBJ_I_WEIGHT, 1000);
      add_prop(OBJ_I_VOLUME, 2000);
      add_prop(OBJ_I_VALUE, 200);


}
