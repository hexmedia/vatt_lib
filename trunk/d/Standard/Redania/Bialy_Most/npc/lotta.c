
/* Autor: Avard
   Opis : Tinardan
   Data : 6.04.07
   Info : NIE JEST SKONCZONA! */

#pragma unique

#include <std.h>
#include <ss_types.h>
#include <macros.h>
#include <stdproperties.h>
#include <pl.h>
#include <money.h>
#include <object_types.h>
#include "dir.h"

inherit BIALY_MOST_STD_NPC;
inherit "/lib/sklepikarz";


int czy_walka();
int walka = 0;

void
create_bm_humanoid()
{
    ustaw_odmiane_rasy("kobieta");
    dodaj_nazwy("straganiarka");
    dodaj_nazwy("handlarka");
    set_gender(G_FEMALE);
    ustaw_imie(({"lotta","lotty","lottcie","lott�","lott�","lottcie"}), PL_ZENSKI);
    dodaj_przym("ospa�y","ospali");
    dodaj_przym("brudny","brudni");

    set_long("@@dlugaaasny@@");

    set_stats (({ 50, 60, 50, 30, 70 }));
    add_prop(CONT_I_WEIGHT, 90000);
    add_prop(CONT_I_HEIGHT, 160);

    set_act_time(60);
    add_act("emote wpatruje si� bezmy�lnie w przestrze�.");
    add_act("emote podnosi n� i zaczyna patroszy� ryb�.");
    add_act("emote przestaje patroszy� ryb� i gapi si� w jeden punkt.");
    add_act("emote spogl�da na ciebie spod byka.");
    add_act("emote wrzuca ryb� do kub�a.");
    add_act("emote si�ga po kolejn� ryb�.");

    set_cact_time(10);
    add_cact("emote piszczy cienko.");

        add_armour(BIALY_MOST_UBRANIA+"suknie/szara_dluga_Mc");


    add_ask(({"ryby","ryb�","rybki"}), VBFC_ME("pyt_o_ryby"));
    set_default_answer(VBFC_ME("default_answer"));

    config_default_sklepikarz();

	set_co_skupujemy(O_RYBY);
    set_store_room(BIALY_MOST_LOKACJE + "magazyn_lotty.c");

    //set_money_greed_buy(106);
	//set_money_greed_sell(100+random(20)); //hihi! :D

	set_money_greed_buy(206);
	set_money_greed_sell(500+random(20)); //hihi! :D
	set_money_greed_change(114);
}

void
powiedz_gdy_jest(object player, string tekst)
{
    if (environment(this_object()) == environment(player))
        this_object()->command(tekst);
}

string
pyt_o_ryby()
{
    if(!query_attack())
    {
        set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
        "emote niepewnie wskazuje palcem na sw�j kramik.");
    }
    else
    {
        set_alarm(0.5,0.0,"powiedz_gdy_jest", TP, "pokrec szybko");
    }
    return "";
}

string
default_answer()
{
    set_alarm(2.0, 0.0, "powiedz_gdy_jest", this_player(),
        "emote odwraca g�ow�, udaj�c, �e nie s�yszy.");
    return "";
}

void
add_introduced(string imie_mia, string imie_bie)
{
    set_alarm(2.0, 0.0, "return_introduce", find_player(imie_mia));
}
void
return_introduce(object ob)
{
    command("usmiechnij sie milo do"+ OB_NAME(ob));
}

void
emote_hook(string emote, object wykonujacy)
{
    switch (emote)
    {
        case "kopnij": set_alarm(1.5, 0.0, "nienajlepszy", wykonujacy);
                    break;
        case "spoliczkuj": set_alarm(1.5, 0.0, "nienajlepszy", wykonujacy);
                    break;
        case "opluj" : set_alarm(2.5, 0.0, "nienajlepszy", wykonujacy);
                    break;
        case "poca^luj": set_alarm(1.5,0.0, "zly",wykonujacy);
        case "prychnij":
              {
                switch(random(1))
                 {
                 case 0:command("spojrzyj lekcewazaco "+
                     "na "+ OB_NAME(wykonujacy)); break;
                 }
                break;
               }
        case "przytul": set_alarm(1.5,0.0, "zly",wykonujacy);
                   break;
        case "poglaszcz": set_alarm(0.5, 0.0, "malolepszy", wykonujacy);
                   break;
        case "poklep": set_alarm(0.5, 0.0, "malolepszy", wykonujacy);
                   break;
        case "usmiech": set_alarm(0.5, 0.0, "malolepszy", wykonujacy);
                   break;
        case "mrugnij": set_alarm(0.5, 0.0, "mrugnij", wykonujacy);
                   break;
    }
}

void
malolepszy(object wykonujacy)
{
    switch(random(2))
    {
        case 0: command("emote rumieni si� i u�miecha g�upawo.");break;
        case 1: command("emote ujmuje podart� sp�dnic� w d�onie i dyga niezgrabnie.");break;
        case 2: command("zachichocz cicho");break;
    }
}
void
zly(object wykonujacy)
{
    if(wykonujacy->query_gender() == 1)
    {
        switch(random(3))
        {
            case 0: command("emote chichocze g�upio i mruga rz�sami"); break;
            case 1: command("emote wpatruje si� w ciebie ciel�cym wzrokiem.");break;
            case 2: command("emote ca�uje ci� nie�mia�o.");break;
        }
    }
    else
    {
        switch(random(2))
        {
            case 0: command("emote spogl�da na ciebie podejrzliwie i spluwa na ziemi�."); break;
            case 1: command("emote chwyta mocniej ryb� i zamierza si� ni�. Jej oczy b�yszcz� ostrzegawczo.");break;
        }
    }
}

void
nienajlepszy(object wykonujacy)
{
    switch(random(2))
    {
        case 0: command("emote twiera usta w niemym prote�cie.");break;
        case 1: saybb("Szare oczy "+QCIMIE(this_object(),PL_DOP)+" nape�niaj� si� �zami.\n");break;
    }
}

void
attacked_by(object wrog)
{
    if(walka==0)
    {
        set_alarm(0.1,0.0,"command","emote piszczy cienko.");
        set_alarm(0.5,0.0,"command","emote unosi obronnie n� do patroszenia ryb.");
        set_alarm(10.0, 0.0, "czy_walka", 1);
        walka = 1;
        return ::attacked_by(wrog);
    }
    else
    {
        walka = 1;
        return ::attacked_by(wrog);
    }
}

int
czy_walka()
{
    if(!query_attack())
    {
       walka = 0;
       return 1;
    }
    else
    {
       set_alarm(5.0, 0.0, "czy_walka", 1);
    }

}
void
init()
{
    ::init();
    init_sklepikarz();
}
void
shop_hook_tego_nie_skupujemy(object ob)
{
    set_alarm(0.1,0.0,"command","pokrec powoli");
}
string
dlugaaasny()
{
    string str;
    str = "Nie mo�e by� zbyt stara, przezieraj�ca spod warstwy brudu sk�ra "+
        "jest �wie�a i nie pomarszczona. Szare oczy wpatruj� si� w "+
        "przestrze� bezmy�lnie, a na twarzy dziewczyny zastyg� wyraz nudy. "+
        "W jednej r�ce trzyma ryb�, w drugiej n� do patroszenia, ale przez "+
        "wi�ksz� cz�� czasu narz�dzie pozostaje bezczynne. Szara, podarta "+
        "suknia dziewczyny i czepek, kt�rego koloru mo�na si� tylko "+
        "domy�la� pot�guj� wra�enie beznadziei bij�ce od tej os�bki. "+
        "Stopy ma ";
    if(environment(TO)->pora_roku() == MT_LATO ||
       environment(TO)->pora_roku() == MT_WIOSNA)
    {
        str += "bose, pokryte warstw� b�ota";
    }
    else
    {
        str += "okryte cienkimi, przetartymi �apciami";
    }
    str += ", a w�osy t�uste i rozpuszczone w nie�adzie.\n ";
    return str;
}
