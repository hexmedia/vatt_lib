/**
 * Autor: Duana, zamawianie kapieli i wszystko zwiazane z laznia - Vera.
 * Opis : sniegulak
 * Data : 28.05.2007
 *
 * Poprawki w zamawianiu k�pieli, dodanie liba handlu, dodanie mo�liwo�c przekupienia
 * karczmarza, je�li si� mu da odpowiedni� ilo�� monet to nas wpu�ci do
 * �a�ni nawet je�li kto� tam jest.
 *
 * Dodanie limitu czasowego na przebywanie w �a�ni, je�li si� ten limit przekroczy karczmarz
 * narazie wyprasza, p�niej mo�e dorobi si�, �e b�dzie prosi� o dop�ate, ale nie chce
 * mi si� teraz tego robi�.
 */

#pragma unique

#include "dir.h"

inherit BIALY_MOST_STD_NPC;
inherit "/lib/trade";

#include <macros.h>
#include <stdproperties.h>
#include <money.h>
#include <pl.h>
#include <formulas.h>
#include <std.h>
#include <ss_types.h>
#include <wa_types.h>
#include <composite.h>
#include <filter_funs.h>
#include <object_types.h>

//tj. 5 denarow
#define CENA_KAPIELI                100
//z Bia�ego Mostu p�ac� tylko 1 denara
#define CENA_KAPIELI_MIESZKANCA     20
//przekupienie karczmarza kosztuje 4 korony
#define PRZEKUPIENIE                960
//maksymalny czas kompieli wynosi 15 minut
#define MAX_CZAS_KAPIELI            900.0

#define PYTAL_O_LAZNIE              "_pytal_o_laznie"

object laznia;
mapping otrzymane = ([]);
mapping alarmy_wyrzucenia = ([]);

int sprawdz_czy_zajete();

void
create_bm_humanoid()
{
    LOAD_ERR(BIALY_MOST_LOKACJE + "laznia");
    laznia = find_object(BIALY_MOST_LOKACJE + "laznia");

    set_living_name("nestir");

    ustaw_odmiane_rasy("m^e^zczyzna");
    set_gender(G_MALE);

    ustaw_imie(({"nestir","nestira","nestirowi","nestira",
        "nestirem","nestirze"}),PL_MESKI_OS);

    dodaj_nazwy("karczmarz");

    set_long("Sporej postury m^e^zczyzna, r^ownie sporym brzuchem, wygl^ada "+
        "na osob^e, kt^ora zajmuje si^e tym zajazdem. Pulchna twarz z ma^lymi "+
        "niebieskimi oczami, i w^asami zakr^econymi do g^ory, sprawia wra^zenie "+
        "bardzo przyjaznej. Zmarszczki powsta^le dooko^la ust wskazuj^a, i^z "+
        "cz^lowiek ten u^smiecha si^e nader cz^esto i ma przyjazne nastawienie "+
        "do podr^o^znych. Ubrany w ciemnobr^azowy zapinany na przedzie ^zupan, "+
        "przewi^azany bia^lym czystym fartuchem, co chwila rzuca spojrzenia "+
        "na go^sci. Czujny wzrok wy^lawia klient^ow, kt^orzy chcieliby co^s "+
        "zam^owi^c.\n");

    dodaj_przym("gruby",  "grubi");
    dodaj_przym("w�saty", "w�saci");

    set_stats(({ 55, 40, 52, 50, 40}));
    set_skill(SS_PARRY, 45);
    set_skill(SS_DEFENCE, 50);
//     set_skill(32, 60);   Po kij ten skill? On do wycofania

    config_default_trade();

    add_prop(CONT_I_WEIGHT, 100000);
    add_prop(CONT_I_HEIGHT, 160);
    add_prop(LIVE_I_NEVERKNOWN, 0);

    set_act_time(30);
    add_act("emote przeciera kufel fartuchem.");
    add_act("emote czujnym wzrokiem szuka klienta.");
    add_act("emote podchodzi do jednego ze sto^l^ow i przeciera go szmat^a.");
    add_act("emote sapie przeci^agle.");
    add_act("emote w zadumie zaczyna d^luba^c sobie w z^ebach.");

//     add_armour(BIALY_MOST_UBRANIA + "fartuchy/fartuch_bialy_czysty.c");
    add_armour(BIALY_MOST_UBRANIA + "zupany/zupan_ciemnobrazowy_dlugi.c");

    add_object("/std/brzytwa");
    MONEY_ADD(TO, 88);

    add_ask(({"karczm^e"}), VBFC_ME("pyt_o_karczme"));
    add_ask(({"potrawy"}), VBFC_ME("pyt_o_potrawy"));
    add_ask(({"prac^e"}), VBFC_ME("pyt_o_prace"));
    add_ask(({"zadanie"}), VBFC_ME("pyt_o_zadanie"));
    add_ask(({"�a�nie"}), VBFC_ME("pyt_o_laznie"));
    add_ask(({"bali^e","^la^xnie","k^apiel"}), VBFC_ME("pyt_o_kapiel"));

    set_default_answer(VBFC_ME("default_answer"));

    set_alarm_every_hour("co_godzine");
    set_wzywanie_strazy(1);
    set_reakcja_na_walke(0);

    remove_prop(NPC_M_NO_ACCEPT_GIVE);
}

int
query_karczmarz()
{
    return 1;
}

string
pyt_o_karczme()
{
    set_alarm(0.5, 0.0, "command_present", TP,
        "powiedz Ta karczma to wszystko co posiadam. ");
    return "";
}
string
pyt_o_potrawy()
{
    set_alarm(1.0, 0.0, "command_present", TP,
        "powiedz Wszystko co mog^e ci poda^c mo^zesz znale^x^c wypisane na "+
        "tablicy. ");
    return "";
}

string 
pyt_o_laznie() 
{
    set_alarm(1.0, 0.0, "command_present", TP,
        "powiedz do " + TP->query_imie(PL_CEL) + " �a�nia jak �a�nia, za 5 " +
        "denar�w mo�esz z niej skorzysta�. " + (sprawdz_czy_zajete() ? 
        "Niestety w tej chwili kto� ju� tam jest." : ""));
}

string
pyt_o_prace()
{
    set_alarm(1.0, 0.0, "command_present", TP,
        "powiedz Nie trza mi tu nikogo do pracy. ");
    return "";
}

string
pyt_o_zadanie()
{
    set_alarm(1.0, 0.0, "command_present", TP,
        "powiedz ^Zadnego zadania u mnie nie znajdziesz. ");
    return "";
}

string
pyt_o_kapiel()
{
    set_alarm(1.0, 0.0, "command_present", TP,
        "powiedz A i owszem, wyk^apa^c si^e u nas mo^zna. Zam^owienie k^apieli jeno "+
        "pi^e^c reda^nskich denar^ow bedzie kosztowa^c.");
    return "";
}

string
default_answer()
{
    set_alarm(1.0, 0.0, "command_present", TP,
        "powiedz Mam zbyt du^zo pracy by z tob^a rozmawia^c o jaki^s "+
        "pierdo^lach. ");
    return "";
}


void
sprawdz_czy_wszedl(object kto)
{
    if(ENV(kto) == laznia)
        command("zamknij drzwi");
}

int
sprawdz_czy_zajete()
{
    if(sizeof(FILTER_PLAYERS(all_inventory(laznia))))
        return 1;
    else
        return 0;
}

int
sprawdz_czy_przekupil(object p = TP)
{
    string imie = p->query_real_name();

    if(is_mapping_index(imie, otrzymane) && otrzymane[imie] >= PRZEKUPIENIE)
        return 1;
    else
        return 0;
}

void
emote_hook(string emote, object wykonujacy, string przyslowek)
{
    switch (emote)
    {
        case "kopnij":
        case "spoliczkuj":
        case "opluj":
        case "nadepnij":
            set_alarm(1.0, 0.0, "command_present", wykonujacy,
                "':z oburzeniem: Nie pozwol^e sob^a tak pomiata^c!");
            break;

        case "poca^luj":
        case "przytul":
        case "pog^laszcz":
            set_alarm(1.0, 0.0, "command_present", wykonujacy,
                "emote rozgl^ada si^e szybko, oblewaj^ac si^e rumie^ncem.");
            break;
    }
}

void
signal_give(object *obs, object old)
{
    object *oobs = obs;

    obs = filter(obs, &operator(==)(O_MONETY, ) @ &->query_type());
    oobs -= obs;

    if(!interactive(old))
    {
        (oobs - obs)->move(old, 1);
        return;
    }

    if(sizeof(obs))
    {
        int value = fold(map(obs, &->query_value()), &operator(+)(,), 0);

        otrzymane[TP->query_real_name()] += value;

        if(sprawdz_czy_przekupil())
        {
            command("hmm");
            command("powiedz do " + OB_NAME(TP) + " A dzi�kuje dzi�kuje. "+
                "Mo�e mog� si� jako� odwdzi�czy�?");
        }
        else
        {
            command("dygnij");
            command("powiedz Dzi^ekuj^e.");
        }
    }
    else if(sizeof(oobs) && (ENV(old) == ENV(TO)))
    {
        command_present(old, "powiedz do " + OB_NAME(old) + "Nie interesuj^a mnie takie rzeczy.");

        command_present(old, "daj " + OBS_NAMES(oobs) + " " + OB_NAME(old));

        if(ENV(old) == ENV(TO))
            command("od�� " + OBS_NAMES(oobs));

        return;
    }

    if(sizeof(oobs))
    {
        command_present(old, "powiedz do " + OB_NAME(old) + " Ale to mi nie potrzebne.");

        command_present(old, "daj " + OBS_NAMES(oobs) + " " + OB_NAME(old));

        if(ENV(old) == ENV(TO))
            command("od�� " + OBS_NAMES(oobs));
    }
}

void
add_introduced(string imie, string biernik)
{
    set_alarm(1.0, 0.0, "return_introduce", imie);
}

//od zamawiania kapieli
int
zamawiamy(string str)
{
    int czy_pochodzimy = 0;

    if(stringp(str) && str ~= "k^apiel")
    {
        if(!objectp(laznia))
        {
            command_present(TP, "powiedz do " + OB_NAME(TP) + " Przykro mi ale �a�nia nieczynna.");

            filter(users(), &operator(>=)(,WIZ_MAGE) @ &(SECURITY)->query_wiz_rank(TP->query_real_name()))->
                catch_msg(capitalize(TP->query_real_name()) + " pr�bowa� wynaj�� �a�nie ale ta si� zablokowa�a.");

            return 1;
        }

        if(sprawdz_czy_zajete())
        {
            if(sprawdz_czy_przekupil())
                command_present(TP, "powiedz do " + OB_NAME(TP) + " A co mi tam, p�a� i w�a�.\n");
            else
            {
                if(TP->query_prop(PYTAL_O_LAZNIE) > 3)
                {
                    set_alarm(1.5, 0.0, "command_present", TP,
                        "szepnij " + OB_NAME(TP) + " A sakiewka wa�pan" + TP->koncowka("a", "ny") + " "+
                        "aby nie za ci�ka? Mo�e i uda nam si� jako� pogodzi� interesa.");
                }
                else
                {
                    set_alarm(0.5, 0.0, "command_present", TP,
                        "powiedz do " + OB_NAME(TP) + " Och, w^la^snie kto^s bierze k^apiel. "+
                        "Dy^c zara wyjdzie, to i wa^s^c si^e umyje.");
                }
                TP->add_prop(PYTAL_O_LAZNIE, TP->query_prop(PYTAL_O_LAZNIE)+1);

                return 1;
            }
        }

        int cena;

        if(TP->query_origin() ~= "z Bia^lego Mostu")
        {
            cena = CENA_KAPIELI_MIESZKANCA;
            czy_pochodzimy = 1;
        }
        else
            cena = CENA_KAPIELI;

        //JAKIE KRUCA MONEY ADD?! PO CO KTO� PISA� LIBA TRADE'A,
        //CHYBA NIE POTO, �EBY MI TU TAKIE OBEJ�CIA ROBI�.

        int cp = can_pay(cena, TP);

        if(!cp)
        {
            if(czy_pochodzimy)
            {
                set_alarm(0.5,0.0, "command_present", TP,
                    "powiedz Pani" + TP->koncowka("e","","e") +
                    " przykro mi, ale nie sta^c Was na k^apiel. " +
                    "Dla Was to kosztuje tylko jednego denara.");
            }
            else
            {
                set_alarm(0.5, 0.0, "command_present", TP,
                    "powiedz Pani"+TP->koncowka("e","","e")+", co mi tu pokazujecie? "+
                    "Grzanie wody kosztuje! Pi^e^c bitych reda^nskich denar^ow i "+
                    "ani grosza mniej.");
            }
            return 1;
        }

        object drzwi_lazni = find_object("/d/Standard/Redania/Bialy_Most/lokacje/drzwi/do_lazni");

        if(!objectp(drzwi_lazni))
        {
            command_present(TP, "powiedz do " + OB_NAME(TP) + " Przykro mi ale �a�nia nieczynna.");

            filter(users(), &operator(>=)(,WIZ_MAGE) @ &(SECURITY)->query_wiz_rank(TP->query_real_name()))->
                catch_msg(capitalize(TP->query_real_name()) + " pr�bowa� wynaj�� �a�nie ale ta si� zablokowa�a.");

            return 1;
        }

        mixed r;
        r = pay(cena, TP);

        write("P�acisz " + text(r[0..2], PL_BIE) +
            (fold(r[3..5], &operator(+)(), 0) ? " i otrzymujesz " + text(r[3..5], PL_DOP) + " reszty" : "") + ".\n");

        TP->add_prop(ZAPLACIL_ZA_LAZNIE, 1);
        set_alarm(60.0, 0.0, &(TP)->remove_prop(ZAPLACIL_ZA_LAZNIE));

        //np. ktos zakonczyl w lazni, a wczesniej sie zamknal.
        if(drzwi_lazni->query_locked())
        {
            write("Karczmarz doskakuje szybko do drzwi prowadz^acych do ^la^xni "+
                "i gmera co^s przy nich. Po chwili od strony drzwi s^lycha^c ciche "+
                "klikni^ecie.\n");
            drzwi_lazni->do_unlock_door("");
        }

        set_alarm(0.5, 0.0, "command_present", TP, "otworz zwykle drewniane drzwi");
        set_alarm(1.0, 0.0, "command_present", TP, "wskaz na zwykle drewniane drzwi");

        if(sprawdz_czy_zajete() && sprawdz_czy_przekupil())
        {
            set_alarm(1.5, 0.0, "command_present", TP, "powiedz do " + OB_NAME(TP) +
                " Tylko mi tu �adnych burd nie wstrzynaj!\n");
        }
        else
        {
            set_alarm(0.5, 0.0, "command_present", TP, "powiedz do " + OB_NAME(TP) +
                " A prosim, prosim do ^la^xni. Wody ju^z nalano!");
        }

        set_alarm(5.0,0.0, "sprawdz_czy_wszedl",TP);
        return 1;
    }

    return 0;
}

nomask int
blok(string str)
{
    string verb = query_verb();
    string *nie_dozwolone = ({"�a�nia","drzwi do �a�ni"});

    if(member_array(verb, nie_dozwolone) == -1)
        return 0;

    if(TP->query_leader())
        if((TP->query_leader())->query_prop(ZAPLACIL_ZA_LAZNIE))
            return 0;
/*
3 najwieksze paradoksy:
1. Ch*j nie ma nogi, a stoi.
2.Ci*a mokra, a nie rdzewieje.
3. Swiat jest okragly, a ludzie pie**ola sie po katach!
*/
    if(sprawdz_czy_zajete())
    {
        if(sprawdz_czy_przekupil())
        {
            command("machnij r�k�");
            command("powiedz do " + OB_NAME(TP) + " A co mi tam, w�a�.");

            otrzymane = m_delete(otrzymane, TP->query_real_name());

            return 0;
        }

        write("Karczmarz jednym wielkim susem doskakuje do ciebie i "+
            "zagradza ci drog�.\n");
        saybb("Karczmarz jednym wielkim susem doskakuje do "+
            QIMIE(TP,PL_DOP) + " i zagradza "+
            TP->koncowka("mu","jej","temu")+" drog� do �a�ni.\n");
        set_alarm(2.0, 0.0, "command_present", TP,
            "powiedz Przykro mi, w^la^snie kto^s bierze k^apiel. "+
            "Dy^c zara wyjdzie, to i wa^s^c si^e umyje.");
        return 1;
    }

    if(!TP->query_prop(ZAPLACIL_ZA_LAZNIE))
    {
        write("Karczmarz zastawia ci drog� do �a�ni.\n");
        saybb("Karczmarz zastawia drog� " +
              QIMIE(TP,PL_CEL) + " do �a�ni.\n");

        if(TP->query_prop(LIVE_O_LAST_ROOM) == laznia)
        {
            set_alarm(1.0, 0.0, "command_present", TP,
                "powiedz do "+TP->query_name(PL_DOP) +
                " Ej�e! Pan"+TP->koncowka("a","i","a")+" czas"+
                " ju� si� sko�czy�, nast�pni klienci czekaj�.");
        }
        else
        {
            set_alarm(1.0, 0.0, "command_present", TP,
                "powiedz do "+TP->query_name(PL_DOP) +
                " Ej�e! Pan"+TP->koncowka("","i","")+" nie zamawia�"+
                TP->koncowka("","a","o")+
                " k�pieli u mnie. Prosz�, prosz�! Jedynie pi�� denar�w. "+
                "Dla s�siad�w jeden.");
        }

        return 1;
    }

    return 0;
}

void
init_living()
{
    add_action(blok, "", 1);
    add_action(zamawiamy, "zam�w");
    ::init_living();
}

nomask void
signal_enter(object kto, object skad)
{
    if(!skad || !laznia)
        return;

    if(skad == laznia)
    {
        if(is_mapping_index(kto->query_real_name(), alarmy_wyrzucenia))
            remove_alarm(alarmy_wyrzucenia[kto->query_real_name()]);

        set_alarm(2.0, 0.0, "command", "zamknij drzwi");
    }
}

int
signal_leave(object kto, object dokad)
{
    if (dokad->query_pub())
        return 0;

    if(sizeof(filter(deep_inventory(kto), &->is_naczynie())) > 0 )
    {
        //kto->command("poloz naczynia na stole");
        tell_object(kto, "Oddajesz wszystkie naczynia karczmarzowi.\n");
        filter(deep_inventory(kto), &->is_naczynie())->remove_object();
        return 0;
    }

    if(dokad == laznia)
        alarmy_wyrzucenia[kto->query_real_name()] = set_alarm(MAX_CZAS_KAPIELI, 0.0, "wyrzuc_z_kapieli", kto);
}

varargs void wyrzuc_z_kapieli(object kogo, int def=0)
{
    if(ENV(kogo) == laznia)
    {
        if(!def)
        {
            command("�a�nia");
            set_alarm(1.0, 0.0, "command_present", kogo, "powiedz do " + OB_NAME(kogo) + " Pan" + kogo->koncowka("a", "i") +
                "czas si� sko�czy�, prosz� si� ubra� i wyj��.");
            set_alarm(2.0, 0.0, "command", "wyj�cie");
            set_alarm(60.0, 0.0, "wyrzuc_z_kapieli", kogo, 1);
        }
        else
        {
            command("�a�nia");

            //Do tych co maj� wi�ksze staty b�dzie si� zwraca� �adnie, i prosi�.
            if(kogo->query_average_stat() <= query_average_stat())
            {
                command_present(kogo, "powiedz do " + OB_NAME(kogo) + " No przeciesz m�wi�em Ci, �eby� wyszed�.");

                write(query_Imie(TP, PL_MIA) + " wypycha ci� z �a�ni.\n");
                saybb(QCIMIE(TO, PL_MIA) + " wypycha " + QCIMIE(TP, PL_BIE) + " do sali g��wnej.\n");
                TP->move(ENV(TP)->query_exit_room()[0], 1);
                move(ENV(TO)->query_exit_rooms()[0], 1);
                saybb(QCIMIE(TP, PL_MIA) + " zostaje tu wepchany przez " + QCIMIE(TO, PL_BIE) + " z �a�ni.\n");

                TP->add_fatigue(-100);
                add_fatigue(-200);
            }
            else
            {
                command("�a�nia");
                command("emote staje w progu i macha z rezygnacj� r�k�.");
                set_alarm(1.0, 0.0, "command_present", kogo, "powiedz do " + OB_NAME(kogo) +
                    "Echh, i jak ja mam sobie z takimi klientami poradzi�.");
                set_alarm(2.0, 0.0, "command", "wyj�cie");
            }
        }
    }
}
void
start_me()
{
    set_alarm(1.0, 0.0, "sprzatamy");
}

void
sprzatamy()
{
    int i;
    object *wszystko=all_inventory(this_object());
    command("wez puste naczynia");

    set_alarm(1.0,      0.0,    "command",  "zdejmij puste naczynia ze stolu");
    set_alarm(3.0,      0.0,    "command"," zdejmij puste naczynia z drugiego stolu");
    set_alarm(7.0,      0.0,    "myjemy");
    set_alarm(600.0,    0.0,    "sprzatamy");
}

void
myjemy()
{
    int i;
    object *wszystko=all_inventory(this_object());

    for(i=0;i<sizeof(wszystko);i++)
    {
        if(wszystko[i]->is_naczynie())
            wszystko[i]->remove_object();
    }
}

