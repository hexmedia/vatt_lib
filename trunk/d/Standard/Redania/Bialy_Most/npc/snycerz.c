
/* Autor: Avard
   Opis : Khaes
   Data : 6.04.07

*/

#pragma unique

#include <std.h>
#include <ss_types.h>
#include <macros.h>
#include <stdproperties.h>
#include <pl.h>
#include <filter_funs.h>
#include <money.h>
#include <pattern.h>
#include "dir.h"

inherit BIALY_MOST_STD_NPC;
inherit "/lib/craftsman.c";

int czy_walka();
int walka = 0;

void
create_bm_humanoid()
{
    ustaw_odmiane_rasy("m^e^zczyzna");
    set_gender(G_MALE);
    ustaw_imie(({"dobromir","dobromira","dobromirowi","dobromira","dobromirem",
        "dobromirze"}), PL_MESKI_OS);
    dodaj_przym("^zylasty","^zyla^sci");
    dodaj_przym("brodaty","brodaci");

    set_long("Gdy si^e na niego spogl^ada - pierwsz^a rzecz^a, kt^ora rzuca "+
        "si^e w oczy jest jego g^esta, jasna broda. Za ni^a kryje si^e "+
        "szczup^la, dojrza^la twarz z wymalowanym na niej szczerym "+
        "u^smiechem i weso^lo skrz^acymi si^e, b^l^ekitnymi oczami. "+
        "Rozchylone po^ly koszuli ukazuj^a jego solidn^a muskulatur^e, "+
        "a zdecydowane ruchy ^swiadcz^a o drzemi^acej w nim sile.\n");
    set_stats (({ 130, 60, 90, 35, 100 }));
    add_prop(CONT_I_WEIGHT, 90000);
    add_prop(CONT_I_HEIGHT, 190);

    set_act_time(60);
    add_act("emote spogl^ada gdzie^s w zamy^sleniu.");
    add_act("emote powoli wyciera sciereczk^a jeden ze swoich wyrob^ow.");
    add_act("emote wodzi zamy^slonym spojrzeniem po pomieszczeniu.");
    add_act("emote chodzi po pomieszczeniu, potykaj^ac si^e o jeden z "+
        "walaj^acych si^e na pod^lodze drewnianych kloc^ow.");
    add_act("emote przeci�ga si� powoli, przerywaj�c na chwil� "+
    	"wyka�czanie pochwy na miecz.");
	add_act("':drapi�c si� po nosie: Pokrowiec chcesz "+
		"zam�wi� zapewne?");
	add_act("':z u�miechem: Robi� tu najlepsze pochwy, na "+
	"p�noc od Pontaru, zapami�taj sobie!");
	add_act("emote krz�ta si� po pomieszczeniu bez po�piechu, "+
			"zagarniaj�c w r�ce wszelakie komponenty potrzebne "+
			"do wykonania nowej pochwy.");


    set_cact_time(10);
    add_cact("'Nie dostaniesz mnie tak ^latwo!");
    add_cact("'Wol^e zgin^a^c, ni^z podda^c si^e bez walki!");
    add_cact("'Poprzestawiam Ci wszystkie ko^sci!");

    add_armour(BIALY_MOST_UBRANIA + "koszule/szara_lniana_XLc.c");
    add_armour(BIALY_MOST_UBRANIA + "spodnie/ciemnobrazowe_skorzane_XLc.c");
    add_armour(BIALY_MOST_UBRANIA + "fartuchy/skorzany_dlugi_XLc.c");
    add_armour(BIALY_MOST_UBRANIA + "rekawice/grube_skorzane_XLc.c");
    add_armour(BIALY_MOST_UBRANIA + "sandaly/jasne_drewniane_XLc.c");
    add_weapon(BIALY_MOST_PRZEDMIOTY + "narzedzia/dluto.c");

    set_store_room(BIALY_MOST_LOKACJE + "magazyn_snycerza");
    set_skill(SS_CRAFTING_CARVING, 80);
    config_default_craftsman();
    craftsman_set_domain(PATTERN_DOMAIN_CARVING);
    craftsman_set_sold_item_patterns( ({
                BIALY_MOST + "wzory/pochwa_duza_drewniana",
                BIALY_MOST + "wzory/pochwa_mala_kunsztowna",
                BIALY_MOST + "wzory/pochwa_mala_miedziana",
                BIALY_MOST + "wzory/pochwa_mala_stalowa",
                BIALY_MOST + "wzory/pochwa_posrebrzana_kunsztowna",
                BIALY_MOST + "wzory/pochwa_prosta_drewniana",
                BIALY_MOST + "wzory/pochwa_zwykla_miedziana",
                BIALY_MOST + "wzory/pochwa_zwykla_stalowa",
                BIALY_MOST + "wzory/temblak_duzy_drewniany",
                BIALY_MOST + "wzory/temblak_duzy_miedziany",
                BIALY_MOST + "wzory/temblak_duzy_stalowy",
                BIALY_MOST + "wzory/temblak_duzy_zdobiony",
                BIALY_MOST + "wzory/temblak_porzadny_zdobiony",
                BIALY_MOST + "wzory/temblak_prosty_drewniany",
                BIALY_MOST + "wzory/temblak_prosty_miedziany",
                BIALY_MOST + "wzory/temblak_zwykly_stalowy",
                BIALY_MOST + "wzory/uprzaz_kunsztowna_skorzana",
                BIALY_MOST + "wzory/uprzaz_prosta_drewniana",
                BIALY_MOST + "wzory/uprzaz_skorzana_cwiekowa"
                }) );

	//set_money_greed_sell(105);
	//set_money_greed_buy(110);

   	set_money_greed_sell(205);
	set_money_greed_buy(511);

	set_money_greed_change(101);

    add_ask(({"warsztat"}), VBFC_ME("pyt_o_warsztat"));
    add_ask(({"pochwy","pochw^e"}), VBFC_ME("pyt_o_pochwy"));
    set_default_answer(VBFC_ME("default_answer"));
}

void
init()
{
    ::init();
    craftsman_init();
}

public string
query_auto_load()
{
    return ::query_auto_load() + query_craftsman_auto_load();
}

void
powiedz_gdy_jest(object player, string tekst)
{
    if (environment(this_object()) == environment(player))
        this_object()->command(tekst);
}

string
pyt_o_warsztat()
{
    if(!query_attack())
    {
        set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
        "'Tak, to m^oj warsztat, chcesz co^s zam^owi^c?");
    }
    else
    {
        set_alarm(0.5,0.0,"powiedz_gdy_jest", TP, "powiedz Wynocha!");
    }
    return "";
}


string
pyt_o_pochwy()
{
    if(!query_attack())
    {
        set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
        "'Tak, tym w^la^snie si^e zajmuj^e.");
        set_alarm(1.5, 0.0, "powiedz_gdy_jest", this_player(),
        "'Wykonuj^e pochwy, chcesz jak^a^s zam^owi^c?");
    }
    else
    {
        set_alarm(0.5,0.0,"powiedz_gdy_jest", TP, "powiedz Wynocha!");
    }
    return "";
}
string
default_answer()
{
    set_alarm(2.0, 0.0, "powiedz_gdy_jest", this_player(),
        "rozloz rece bezradnie");
    return "";
}

void
add_introduced(string imie_mia, string imie_bie)
{
    set_alarm(2.0, 0.0, "return_introduce", find_player(imie_mia));
}
void
return_introduce(object ob)
{
    command("przedstaw sie "+ OB_NAME(ob));
}

void
emote_hook(string emote, object wykonujacy)
{
    switch (emote)
    {
        case "kopnij": set_alarm(1.5, 0.0, "nienajlepszy", wykonujacy);
                    break;
        case "spoliczkuj": set_alarm(1.5, 0.0, "nienajlepszy", wykonujacy);
                    break;
        case "opluj" : set_alarm(2.5, 0.0, "nienajlepszy", wykonujacy);
                    break;
        case "poca^luj":
              {
              if(wykonujacy->query_gender() == 1)
              {
                switch(random(2))
                 {
                 case 0: command("':czerwieni^ac si^e lekko: No "+
                     "wie panienka, pochlebia mi to, ale..."); break;
                 case 1: command("':odwracaj^ac lekko g^low^e: "+
                     "Ten, praca czeka.");break;
                }
              }
              else
                {
                   switch(random(2))
                   {
                   case 0: this_player()->catch_msg(QCIMIE(TO,PL_MIA)+
                       " m^owi odpychaj^ac ci^e stanowczo: ^Ze^s "+
                       "zg^lupia^l?\n");
                       saybb(QCIMIE(this_object(),PL_MIA)+" m^owi "+
                           "odpychaj^ac "+QIMIE(TP,PL_DOP)+" stanowczo: "+
                           "^Ze^s zg^lupia^l?\n");break;
                   case 1: this_player()->catch_msg(QCIMIE(TO,PL_MIA)+
                       " m^owi odsuwaj^ac si^e od ciebie "+
                       "gwa^ltownie: Zostaw mnie ty, ty...\n");

                       saybb(QCIMIE(this_object(),PL_MIA)+" m^owi "+
                           "odsuwaj^ac si^e od "+QIMIE(TP,PL_DOP)+
                           " gwa^ltownie: Zostaw mnie ty, ty...\n");break;
                   }
                }
                break;
              }
        case "prychnij":
              {
                switch(random(1))
                 {
                 case 0:command("spojrzyj lekcewazaco "+
                     "na "+ OB_NAME(wykonujacy)); break;
                 }
                break;
               }
        case "przytul": set_alarm(0.5, 0.0, "malolepszy", wykonujacy);
                   break;
        case "poglaszcz": set_alarm(0.5, 0.0, "malolepszy", wykonujacy);
                   break;
        case "poklep": set_alarm(0.5, 0.0, "malolepszy", wykonujacy);
                   break;
    }
}

void
malolepszy(object wykonujacy)
{
    if(wykonujacy->query_gender() == 1)
    {
        switch(random(2))
        {
            case 0: command("':z u^smiechem: No, no..."); break;
            case 1: command("':kiwaj^ac g^low^a z lekkim "+
                "u^smiechem: Czym^ze sobie zas^lu^zy^lem?");break;
        }
    }
    else
    {
        switch(random(2))
        {
            case 0: command("poklep "+ OB_NAME(wykonujacy) +" przyjacielsko "+
                "po ramieniu");break;
            case 1: command("usmiechnij sie lekko");break;
        }
    }
}

void
nienajlepszy(object wykonujacy)
{
    if(wykonujacy->query_gender() == 1)
    {
        switch(random(2))
        {
            case 0: command("':marszcz^ac brwi: Masz szcz^e^scie, "+
                "^ze^s kobieta, bo inaczej...");break;
            case 1: this_player()->catch_msg(QCIMIE(this_object(),
                PL_MIA)+" m^owi odpychaj^ac ci^e stanowczo: Co ty sobie "+
                "my^slisz?\n");

                saybb(QCIMIE(this_object(),PL_MIA)+" m^owi "+
                    "odpychaj^ac "+QIMIE(TP,PL_BIE)+" stanowczo: "+
                    "Co ty sobie my^slisz?\n");break;
        }
    }
    else
    {
        switch(random(2))
        {
            case 0: this_player()->catch_msg(QCIMIE(this_object(),
                PL_MIA)+" m^owi odpychaj^ac ci^e stanowczo: Bo inaczej "+
                "pogadamy!\n");

                saybb(QCIMIE(this_object(),PL_MIA)+" m^owi "+
                    "odpychaj^ac "+QIMIE(TP,PL_BIE)+" stanowczo: "+
                    "Co ty sobie my^slisz?\n");break;
            case 1: command("':napinaj^ac gro^xnie musku^ly: "+
                "Mo^zemy inaczej pogada^c.");break;
        }
    }
}

void
attacked_by(object wrog)
{
    if(walka==0)
    {
        set_alarm(0.5,0.0,"command","krzyknij Ty... ty!");
        set_alarm(10.0, 0.0, "czy_walka", 1);
        walka = 1;
        return ::attacked_by(wrog);
    }
    else
    {
        //set_alarm(1.0, 0.0, "command", "krzyknij I tak zginiecie!");
        walka = 1;
        return ::attacked_by(wrog);
    }
}

int
czy_walka()
{
    if(!query_attack())
    {

       this_object()->command("pokiwaj lekko");
       walka = 0;
       return 1;
    }
    else
    {
       set_alarm(5.0, 0.0, "czy_walka", 1);
    }

}

public string
init_arg(string arg)
{
    if (arg == 0) {
        force_expand_patterns_items();
    }
    return init_craftsman_arg(::init_arg(arg));
}






/*

Proces zamawiania pochwy:
Kod:
.Z�� zam�wienie:
1.(M�czyzna) M�wi zacieraj�c d�onie: Oho, zam�wienie? No, dobra, dobra? Co chcesz? Pochw� na miecz, a mo�e na sztylet? Uprz�� na bro� drzewcow�, h�? Temblak na m�ot, albo top�r?

Komendy:
Pochwa na miecz; Pochwa na sztylet; Uprz��; Temblak;

2. (M�czyzna) U�miecha si� ze zrozumieniem: Od razu powinienem si� domy�li�, ha! A wi�c <wybrany pokrowiec>. Niech b�dzie?

(W zale�no�ci od wybranego pokrowca rozmowa toczy si� tak)
- Dla pochwy na miecz lub na sztylet:
3. (M�czyzna) Rozgl�da si� powoli po pomieszczeniu: No tak, �yczysz sobie, aby z czego pokrowiec by� wykonany? Drewno czy sk�ra?

 Komendy:
Drewno; Sk�ra;

4. (M�czyzna) Bierze wskazany przez ciebie surowiec i przygl�da mu si�: Niech tak b�dzie, �yczysz sobie, aby przyprawi� pochwie metalowe zdobienia?(Sk�ra)/, aby wyrze�bi� w pochwie ozdoby?(Drewno)

Komendy:
Tak; Nie;

5. (M�czyzna) Kiwa powoli g�ow� i odk�ada materia�y: No to wszystko jasne.. Ah, bym zapomnia�, mo�e jaka� ma�a inskrypcyjka, ha?

Komendy:
Tak (Nast�pnie podanie tre�ci napisu, my�l� �e do 10 znak�w); Nie;

6. (M�czyzna) Spogl�da na ciebie z u�miechem: To wszystko co chc� wiedzie�, wr�� tu i odbierz zam�wienie za czas jaki�, o.

- Rozmowa dla temblaka i uprz�y:
3.(M�czyzna) M�czyzna u�miecha si� kiwaj�c g�ow�, �ci�ga ze �cian jedn� ze sk�r i przygl�da si� jej krytycznie: Ten, no? Zdobienia metalowe przyprawi� pokrowcowi?

Komendy:
Tak; Nie;

4. (M�czyzna) Kiwa powoli g�ow� i odk�ada materia�y: No to wszystko jasne? Ah, bym zapomnia�, mo�e jaka� ma�a inskrypcyjka, ha?

Komendy:
Tak (Nast�pnie podanie tre�ci napisu, my�l� �e do 10 znak�w); Nie;

5. (M�czyzna) Spogl�da na ciebie z u�miechem: To wszystko co chc� wiedzie�, wr�� tu i odbierz zam�wienie za czas jaki�, o.

Eventy podczas wykonywania pokrowca:
Kod:
- (M�czyzna) nuci co� pod nosem przygotowuj�c materia�.
- (M�czyzna) kr�ci si� po pomieszczeniu zbieraj�c narz�dzia.
- (M�czyzna) marszczy czo�o z namys�em.
- (M�czyzna) mru��c oczy skraja no�em sk�ry.
- (M�czyzna) z u�miechem na twarzy wykonuje sw� robot�.
- (M�czyzna) kawa�kiem w�gla z zapa�em kre�li co� na materiale.
- (M�czyzna) ma�ym d�utkiem formuje drewno.
- (M�czyzna) podrzuca w g�r� swoje narz�dzia z weso�ym u�miechem na twarzy.



*/
