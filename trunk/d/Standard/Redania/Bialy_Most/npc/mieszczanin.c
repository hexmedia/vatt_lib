/*
 * mieszczanin BM
 * sysko faevki ^___^
 * dn. 14 czerwca 2007
 *
 */


#include <std.h>
#include <ss_types.h>
#include <macros.h>
#include <stdproperties.h>
#include <pl.h>
#include "dir.h"

inherit BIALY_MOST_STD_NPC;

int czy_walka();
int walka = 0;

void
create_bm_humanoid()
{
    ustaw_odmiane_rasy(PL_MEZCZYZNA);
    set_gender(G_MALE);
    dodaj_nazwy("mieszczanin");

    set_long("M^e^zczyzna ten, na oko trzydziestoletni, o jasnej twarzy, "+
		"nieskalanej promieniami s^lo^nca, jak przysta^lo na cz^lowieka "+
		"wy^zszego stanu ubrany jest schludnie i bogato. Elegancka koszula "+
		"uszyta z jakiego^s drogiego materia^lu chowa si^e pod czarn^a "+
		"peleryn^a. Spodnie - o do^s^c szerokich nogawkach, zwe^zaj^acych "+
		"si^e w okolicach ^lydek wpuszczone ma w ci^e^zkie, wypolerowane na "+
		"b^lysk buty. M^e^zczyzna dumnym krokiem przechadza si^e uliczkami "+
		"Bia^lego Mostu, co i rusz rozgl^adaj^ac si^e powoli i z "+
		"wy^zszo^sci^a, jakby chc^ac ca^lemu ^swiatu pokaza^c klas^e i "+
		"^swiatowe obycie. \n");

    dodaj_przym("wysoki","wysocy");
    dodaj_przym("bladosk^ory","bladosk^orzy");

    set_stats (({ 70, 58, 60, 50, 55 }));

	add_prop(LIVE_I_NEVERKNOWN, 1);

    add_prop(CONT_I_WEIGHT, 100000);
    add_prop(CONT_I_HEIGHT, 185);

    set_act_time(30);
    add_act("popatrz powoli na mezczyzne");
    add_act("popatrz powoli na kobiete");
    add_act("rozejrzyj sie lekko");
    add_act("emote wolnym krokiem przechadza si^e w t^e i z powrotem. ");
	add_act("rozejrzyj sie z wyzszoscia");
	add_act("popatrz w zamysleniu na niebo");

    set_cchat_time(10);
    add_cchat("Lepiej zostaw mnie w spokoju, bo gorzko tego po^za^lujesz! ");
    add_cchat("Zostaw mnie! To spokojne miasto, nie chcemy tu takich jak ty! ");
    add_cchat("Odczep si^e, s^lyszysz? ");

    add_armour(BIALY_MOST_UBRANIA+"koszule/prosta_jedwabna_Lc");
    add_armour(BIALY_MOST_UBRANIA+"spodnie/satynowe_wisniowe_Lc");
	add_armour(BIALY_MOST_UBRANIA+"peleryny/czarna_aksamitna_Lc");
	add_armour(BIALY_MOST_UBRANIA+"buty/wysokie_eleganckie_Lc");

    add_ask(({"rinde"}), VBFC_ME("pyt_o_rinde"));
    add_ask(({"kram","kramy","kramy rybne","ryby"}), VBFC_ME("pyt_o_ryby"));
    add_ask(({"burmistrza"}), VBFC_ME("pyt_o_burmistrza"));
    add_ask(({"oxenfurt"}), VBFC_ME("pyt_o_oxenfurt"));
    add_ask(({"Bia^ly Most","miasto"}), VBFC_ME("pyt_o_miasto"));
	add_ask(({"elfy"}), VBFC_ME("pyt_o_elfy"));
	add_ask(({"sklepikarza","Lollo"}), VBFC_ME("pyt_o_lolla"));
	add_ask(({"zadanie"}), VBFC_ME("pyt_o_zadanie"));
	add_ask(({"prac^e"}), VBFC_ME("pyt_o_prace"));
	add_ask(({"pomoc"}), VBFC_ME("pyt_o_pomoc"));

    set_default_answer(VBFC_ME("default_answer"));

    set_alarm_every_hour("co_godzine");
    set_random_move(100);
    set_restrain_path(BIALY_MOST_LOKACJE_ULICE);
}

void
powiedz_gdy_jest(object player, string tekst)
{
    if (environment(this_object()) == environment(player))
        this_object()->command(tekst);
}

string
pyt_o_rinde()
{
    if(!query_attack())
    {
        set_alarm(1.0, 0.0, "powiedz_gdy_jest", this_player(),
        "powiedz Ot, ca^lkiem spore miasto, tam na wschodzie - wykonuje "+
			"zamaszysty ruch d^loni^a. Ale nie warto tam si^e kr^eci^c, "+
			"Wiewi^orki pono^c tam buszuj^a. ");
    }
    else
    {
        set_alarm(1.0,0.0,"powiedz_gdy_jest", this_player(),
            "powiedz Mam teraz wa^zniejsze sprawy na g^lowie ni^z "+
			"odpowiada^c na twoje g^lupie pytania!");
    }
    return "";

}
string
pyt_o_ryby()
{
    if(!query_attack())
    {
    set_alarm(1.0, 0.0, "powiedz_gdy_jest", this_player(),
        "powiedz Ryby? Ach, tak, mo^xna je kupi^c w zachodniej cz^e^sci "+
		"miasta. Ale to niezbyt ciekawa okolica. ");
    }
    else
    {
        set_alarm(1.0,0.0,"powiedz_gdy_jest", this_player(),
            "powiedz Mam teraz wa^zniejsze sprawy na g^lowie ni^z "+
			"odpowiada^c na twoje g^lupie pytania!");
    }
    return "";

}

string
pyt_o_burmistrza()
{
    if(!query_attack())
    {
    set_alarm(1.0, 0.0, "powiedz_gdy_jest", this_player(),
        "powiedz A tak, mamy burmistrza - Migaaila. Jego dom znajdziesz w "+
		"centrum miasta.");
    }
else
    {
        set_alarm(1.0,0.0,"powiedz_gdy_jest", this_player(),
           "powiedz Mam teraz wa^zniejsze sprawy na g^lowie ni^z "+
			"odpowiada^c na twoje g^lupie pytania!");
    }
    return "";

}

string
pyt_o_oxenfurt()
{
    if(!query_attack())
    {
    set_alarm(1.0, 0.0, "powiedz_gdy_jest", this_player(),
        "powiedz Oxenfurt to du^ze miasto uniwersyteckie, znajduje si^e na "+
		"p^o^lnocy, ale nie spos^ob si^e tam dosta^c w tej chwili - wzd^lu^z "+
		"traktu w^edruj^a jakie^s zb^ojeckie bandy. ");
    }
    else
    {
        set_alarm(1.0,0.0,"powiedz_gdy_jest", this_player(),
            "powiedz Mam teraz wa^zniejsze sprawy na g^lowie ni^z "+
			"odpowiada^c na twoje g^lupie pytania!");
    }
    return "";
}
pyt_o_miasto()
{
    if(!query_attack())
    {
    set_alarm(1.0, 0.0, "command_present", this_player(),
        "wskaz");
    set_alarm(2.0, 0.0, "powiedz_gdy_jest", this_player(),
		"powiedz W^lasnie w nim jeste^s, rozejrzyj si^e - zobacz jak tu "+
		"^ladnie i bogato. ");
	set_alarm(2.5, 0.0, "command_present", this_player(),
		"u^smiech dumnie");

    }
    else
    {
        set_alarm(1.0,0.0,"powiedz_gdy_jest", this_player(),
            "powiedz Mam teraz wa^zniejsze sprawy na g^lowie ni^z "+
			"odpowiada^c na twoje g^lupie pytania!");
    }
    return "";
}

pyt_o_elfy()
{
    if(!query_attack())
    {
    set_alarm(1.5, 0.0, "powiedz_gdy_jest", this_player(),
		"powiedz Panosz^a si^e wsz^edzie pieprzone w^askodupce. ");
	set_alarm(1.0, 0.0, "command_present", this_player(),
		"prychnij");
	}
    else
    {
        set_alarm(1.0,0.0,"powiedz_gdy_jest", this_player(),
            "powiedz Mam teraz wa^zniejsze sprawy na g^lowie ni^z "+
			"odpowiada^c na twoje g^lupie pytania!");
    }
    return "";
}

pyt_o_lolla()
{
    if(!query_attack())
    {
    set_alarm(2.0, 0.0, "powiedz_gdy_jest", this_player(),
		"powiedz Nie lubi nieludzi i wcale mu si^e nie dziwi^e. Tylko "+
		"domagaliby si^e swoich praw. Jakie tam prawa mog^a mie^c niby? ");
	}
    else
    {
        set_alarm(1.0,0.0,"powiedz_gdy_jest", this_player(),
            "powiedz Mam teraz wa^zniejsze sprawy na g^lowie ni^z "+
			"odpowiada^c na twoje g^lupie pytania!");
    }
    return "";
}

pyt_o_zadanie()
{
    if(!query_attack())
    {
    set_alarm(2.0, 0.0, "powiedz_gdy_jest", this_player(),
		"powiedz Je^sli bardzo chcesz, to mo^zesz przynie^s^c mi co^s "+
		"smakowitego. ");
	set_alarm(1.0, 0.0, "command_present", this_player(),
		"usmiech pogodnie");
	}
    else
    {
        set_alarm(1.0,0.0,"powiedz_gdy_jest", this_player(),
            "powiedz Mam teraz wa^zniejsze sprawy na g^lowie ni^z "+
			"odpowiada^c na twoje g^lupie pytania!");
    }
    return "";
}

pyt_o_prace()
{
    if(!query_attack())
    {
    set_alarm(2.0, 0.0, "powiedz_gdy_jest", this_player(),
		"powiedz Hmmmm... Praca? Jaka praca? ");
	}
    else
    {
        set_alarm(1.0,0.0,"powiedz_gdy_jest", this_player(),
            "powiedz Mam teraz wa^zniejsze sprawy na g^lowie ni^z "+
			"odpowiada^c na twoje g^lupie pytania!");
    }
    return "";
}

pyt_o_pomoc()
{
    if(!query_attack())
    {
    set_alarm(2.0, 0.0, "powiedz_gdy_jest", this_player(),
		"powiedz Nie, dzi^ekuj^e, nie jest mi potrzebna. ");
	}
    else
    {
        set_alarm(1.0,0.0,"powiedz_gdy_jest", this_player(),
            "powiedz Mam teraz wa^zniejsze sprawy na g^lowie ni^z "+
			"odpowiada^c na twoje g^lupie pytania!");
    }
    return "";
}
string
default_answer()
{
    if(!query_attack())
    {
    switch(random(4))
    {
    case 0:set_alarm(2.0, 0.0, "powiedz_gdy_jest", this_player(),
        "powiedz Nie przeszkadzaj mi, w^lasnie odpoczywam. "); break;
    case 1:set_alarm(2.0, 0.0, "powiedz_gdy_jest", this_player(),
        "powiedz Zostaw mnie w spokoju. "); break;
	case 2:set_alarm(2.0, 0.0, "command_present", this_player(),
		"pokrec lekko"); break;
	case 3:set_alarm(2.0, 0.0, "command_present", this_player(),
		"prychnij"); break;

    }
    }
    else
    {
        set_alarm(1.0,0.0,"powiedz_gdy_jest", this_player(),
           "powiedz Mam teraz wa^zniejsze sprawy na g^lowie ni^z "+
			"odpowiada^c na twoje g^lupie pytania!");
    }
    return "";
}

void
add_introduced(string imie_mia, string imie_bie)
{
    set_alarm(2.0, 0.0, "return_introduce", find_player(imie_mia));
}
void
return_introduce(object ob)
{
    command("powiedz Mi^lo mi");

}

void
emote_hook(string emote, object wykonujacy)
{
    switch (emote)
    {
        case "kopnij": set_alarm(1.5, 0.0, "nienajlepszy", wykonujacy);
                    break;
        case "spoliczkuj": set_alarm(1.5, 0.0, "nienajlepszy", wykonujacy);
                    break;
        case "opluj" : set_alarm(2.5, 0.0, "nienajlepszy", wykonujacy);
                    break;
        case "poca^luj":
		case "przytul":
		case "pog^laszcz":
              {
              if(wykonujacy->query_gender() == 1)
                  {
              switch(random(3))
                        {
                 case 0:command("powiedz Mo^ze spotkamy si^e wieczorem...? "); break;
                 case 1:command("emote mru^zy oczy weso^lo.");
                        break;
                 case 2:command("popatrz z usmiechem na "+
                        OB_NAME(wykonujacy));
                        break;
                        }
                  }
                     else
                  {
                   switch(random(3))
                        {
                   case 0: set_alarm(1.0, 0.0, "command_present", wykonujacy,
					   "skrzyw sie"); break;
                   case 1:command("':wykrzywiaj^ac si^e z "+
					   "obrzydzeniem: Zostaw mnie. ");
                            break;
                   case 2:command("spojrzyj z obrzydzeniem na "+
                        OB_NAME(wykonujacy));
                            break;
                        }
                  }


               }
               break;

    }
}

void
nienajlepszy(object kto)
{
   switch(random(3))
   {
      case 0: set_alarm(1.0, 0.0, "powiedz_gdy_jest", kto,
          "krzyknij Odejd^x, bo zawo^lam stra^z! ");
              break;
      case 1: set_alarm(1.0, 0.0, "powiedz_gdy_jest", kto,
          "powiedz Zostaw mnie w spokoju. Inaczej gorzko tego po^za^lujesz. ");
              break;
      case 2: set_alarm(1.0, 0.0, "powiedz_gdy_jest", kto,
          "powiedz Lepiej id^x st^ad, to spokojne miasto, nie chcemy tu "+
		  "takich jak ty.");
              break;
   }
}

void attacked_by(object wrog)
{
    if(walka == 0)
    {
        walka = 1;
        set_alarm(0.5,0.0,"command","krzyknij Wara st^ad, zb^oju!");
        set_alarm(15.0, 0.0, "czy_walka", 1);
        return ::attacked_by(wrog);
    }
    else
    {
        return ::attacked_by(wrog);
    }
}
int
czy_walka()
{
    if(!query_attack())
    {
        set_alarm(0.5,0.0,"command","powiedz Tch^orze.");
		set_alarm(0.5,0.0,"command","splun");

        walka = 0;
        return 1;
    }
    else
    {
        set_alarm(15.0, 0.0, "czy_walka", 1);
    }
}

int
co_godzine()
{
    if (MT_GODZINA == 23)
    {
        set_alarm(0.1,0.0, "do_domu");
    }
}

void
do_domu()
{
    if(query_attack())
    {
        set_alarm(10.0,0.0,"do_domu");
    }
    else
    {
        tell_roombb(environment(), QCIMIE(TO,PL_MIA)+" wchodzi "+
            "do jednego z budynk^ow.\n");
        remove_object();
    }
}