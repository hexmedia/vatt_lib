#include "../dir.h"

#define POCHODZENIE		  "z Bia�ego Mostu"

#define BURMISTRZ         "Migaail"
#define BURMISTRZA        "Migaaila"
#define BURMISTRZ_N       "Rubespier"
#define BURMISTRZA_N      "Rubespiera"
#define BURMISTRZ_OPT     "G. "

#define BIALY_MOST_NPC                  (BIALY_MOST + "npc/")
#define ULICA_BIALY_MOST_STD            (BIALY_MOST + "std/most.c")
#define BIALY_MOST_STD_NPC              (BIALY_MOST + "std/npc.c")
#define BIALY_MOST_LOKACJE_DRZWI        (BIALY_MOST_LOKACJE+"drzwi/")


#define SNYCERZ                         (BIALY_MOST_NPC+"snycerz.c")

#define BIALY_MOST_STD                  (BIALY_MOST + "std/bialy_most.c")

#define ZAPLACIL_ZA_LAZNIE              "_zaplacil_za_laznie"

#define BM_SLEEP_PLACE                     (BIALY_MOST + "lokacje/strych.c")