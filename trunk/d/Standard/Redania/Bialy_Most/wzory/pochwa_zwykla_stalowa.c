inherit "/std/pattern";
#include <pattern.h>
#include "dir.h"
public void
create()
{
    set_pattern_domain(PATTERN_DOMAIN_CARVING);
    set_item_filename(BIALY_MOST_PRZEDMIOTY + "pochwy/zwykla_stalowa.c");
    set_estimated_price(850);
    set_time_to_complete(5000);
}