/*
 * opis Brz^ozka
 * wrzuci^la faevia :>
 * 22.06.07
 *
 */

inherit "/std/herb";

#include <pl.h>
#include <herb.h>
#include <mudtime.h>
#include <materialy.h>
#include <stdproperties.h>

void
create_herb()
{
    ustaw_nazwe_ziola("krwawnik");

    ustaw_nazwe("kwiatostan");
    dodaj_przym("p^laski", "p^lascy");
    dodaj_przym("baldachokszta^ltny", "baldachokszta^ltni");

    ustaw_nazwe_glowna_ziola("kwiatostan");

    ustaw_id_nazwe_calosci(({"kwiatostan krwawnika","kwiatostanu krwawnika",
		"kwiatostanowi krwawnika","kwiatostan krwawnika",
		"kwiatostanem krwawnika","kwiatostanie krwawnika"}),
		({"kwiatostany krwawnika","kwiatostan^ow krwawnika",
		"kwiatostanom krwawnika","kwiatostany krwawnika",
		"kwiatostanami krwawnika","kwiatostanach krwawnika"}));

    ustaw_unid_nazwe_calosci("ro^slina");
    dodaj_unid_przym_calosci("niewysoki", "niewysocy");
    dodaj_unid_przym_calosci("jasnozielony", "jasnozieloni");

    set_id_long("Drobniutkie, bia^le lub r^o^zowe kwiatki krwawnika osadzone "+
		"na matowych, seledynowych szypu^lkach skupiaj^a si^e w "+
		"baldachokszta^ltne kwiatostany o charakterystycznej lekko gorzkawej "+
		"woni. Nawet nieuczone znachorki stosuj^a ^swie^ze kwiaty do "+
		"sporz^adzania ok^lad^ow na rany, owrzodzenia i st^luczenia wiedz^ac "+
		"o ich dzia^laniu przeciwkrwotocznym i przeciwzapalnym. Napar z "+
		"kwiat^ow krwawnika stosowany jest przez uczonych medyk^ow w "+
		"leczeniu wrzod^ow ^zo^l^adka, gdy^z dzia^la przeciwkrwotocznie a "+
		"tak^ze rozkurczowo i przeciwzapalnie. Natomiast k^apiele z dodatkiem "+
		"tego kwiecia ^lagodz^a b^ole reumatyczne i koj^a stargane nerwy. "+
		"Podobne dzia^lanie lecz s^labsze wykazuje ziele krwawnika. \n");

    set_unid_long("Drobniutkie, bia^le lub r^o^zowe kwiatki krwawnika "+
		"osadzone na matowych, seledynowych szypu^lkach skupiaj^a si^e w "+
		"baldachokszta^ltne kwiatostany o charakterystycznej lekko gorzkawej "+
		"woni. \n");

    dodaj_komende_zbierania("zerwij", "zrywasz", "zrywa", "zerwa^c");

    ustaw_czas_psucia(350, 400); // schniecie niszczenie
    ustaw_trudnosc_identyfikacji(10);
    ustaw_czestosc_wystepowania(85);

    ustaw_trudnosc_znalezienia(10);

    ustaw_przym_zepsutego("ususzony", "ususzeni");

    set_amount(3); //wartosc od^zywcza

    add_prop(OBJ_I_VOLUME, 40);
    add_prop(OBJ_I_WEIGHT, 90);
    add_prop(OBJ_I_VALUE, 9);

    ustaw_material(MATERIALY_IN_ROSLINA);

    ustaw_pore_roku(MT_LATO|MT_JESIEN);
}