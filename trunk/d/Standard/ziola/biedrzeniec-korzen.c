/*
 * miedze, wzg�rza, lasy, laki
 * pospolity
 *
 * Faeve, 12.02.08
 */


inherit "/std/herb";

#include <pl.h>
#include <herb.h>
#include <mudtime.h>
#include <materialy.h>
#include <stdproperties.h>

void
create_herb()
{
    ustaw_nazwe("korze^n");
    dodaj_przym("niebieskawy", "niebieskawi");
    dodaj_przym("poskr^ecany", "poskr^ecani");    

    ustaw_nazwe_glowna_ziola("korze^n");
    ustaw_nazwe_ziola("biedrzeniec");

    dodaj_id_nazwy_calosci("biedrzeniec");
    ustaw_id_short_calosci(({"ziele biedrze^nca",
        "ziela biedrze^nca", "zielu biedrze^nca",
        "ziele biedrze^nca", "zielem biedrze^nca",
        "zielu biedrzenca"}),
       ({"zio^la biedrze^nca", "zio^la biedrze^nca",
        "zio^lom biedrze^nca", "zio^la biedrze^nca",
        "zio^lami biedrze^nca", "zio^lach biedrze^nca"}),
        PL_NIJAKI_NOS); 

    dodaj_id_nazwy_calosci("ro^slina");
    dodaj_id_przym_calosci("delikatny", "delikatni");
    dodaj_id_przym_calosci("wysmuk^ly", "wysmukli");

    ustaw_unid_nazwe_calosci("ro^slina");
    dodaj_unid_przym_calosci("delikatny", "delikatni");
    dodaj_unid_przym_calosci("wysmuk^ly", "wysmukli");

    set_id_long("Do^s^c pot^e^zny i masywny, jak na tak niedu^z^a "+
        "ro^slin^e, nieregularnie poskr^ecany korze^n o niebieskawej "+
        "barwie. Z boku gdzieniegdzie wyrastaj^a male^nkie odn^o^zki, a z "+
        "lekko odartych fragment^ow tr^aci ostrym, do^s^c nieprzyjemnym "+
        "zapachem. Korze^n biedrze^nca jest doskona^lym ^srodkiem "+
        "wykrztu^snym oraz rozkurczaj^acym - ususzony nieco pokruszy^c, "+
        "niewielk^a ilo^s^c zala^c wrz^atkiem i pi^c w niewielkich dawkach. "+
        "Nale^zy uwa^za^c, bo wi^eksza ilo^s^c mo^ze niekorzystnie "+
        "wp^lywa^c na nerki. \n");

    set_unid_long("Do^s^c pot^e^zny i masywny, jak na tak niedu^z^a "+
        "ro^slin^e, nieregularnie poskr^ecany korze^n o niebieskawej "+
        "barwie. Z boku gdzieniegdzie wyrastaj^a male^nkie odn^o^zki, a z "+
        "lekko odartych fragment^ow tr^aci ostrym, do^s^c nieprzyjemnym "+
        "zapachem. \n");

    ustaw_czas_psucia(350, 400); // schniecie niszczenie
    ustaw_trudnosc_identyfikacji(18);
    ustaw_czestosc_wystepowania(80);
    ustaw_sposob_wystepowania(HERBS_WYST_BMALE_GRUPY);

    ustaw_ilosc_w_calosci(1);

    ustaw_trudnosc_znalezienia(6); 

    ustaw_przym_zepsutego("ususzony", "ususzeni");

    set_amount(5); //wartosc od^zywcza

    add_prop(OBJ_I_VOLUME, 60); 
    add_prop(OBJ_I_WEIGHT, 53); 
    add_prop(OBJ_I_VALUE, 14);

    ustaw_material(MATERIALY_IN_ROSLINA);

    ustaw_pore_roku(MT_WIOSNA | MT_JESIEN);
}
