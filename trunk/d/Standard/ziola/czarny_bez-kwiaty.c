/**
 * wyst�powanie: ��gi, ale og�lnie to wsz�dzie
 *
 * by Brz�zek
 *
 * Poprawki  -  Krun 2007
 */


inherit "/std/herb";

#include <pl.h>
#include <herb.h>
#include <mudtime.h>
#include <materialy.h>
#include <stdproperties.h>

void
create_herb()
{
    ustaw_nazwe("kwiatostan");
    dodaj_przym("bia�y", "biali");
    dodaj_przym("baldachokszta�tny", "baldachokszta�tni");

    ustaw_nazwe_glowna_ziola("kwiatostan");
    ustaw_nazwe_ziola("bez");
    dodaj_przym_ziola("czarny", "czarni");

    dodaj_id_nazwy_calosci("bez");
    ustaw_id_short_calosci(({"wysoki drzewiasty krzew czarnego bzu",
        "wysokiego drzewiastego krzewu czarnego bzu", "wysokiemu drzewiastemu krzewowi czarnego bzu",
        "wysoki drzewiasty krzew czarnego bzu", "wysokim drzewiastym krzewem czarnego bzu",
        "wysokim drzewiastym krzewie czarnego bzu"}),
       ({"wysokie drzewiaste krzewy czarnego bzu", "wysokich drzewiastych krzew�w czarnego bzu",
        "wysokim drzewiastym krzewom czarnego bzu", "wysokie drzewiaste krzewy czarnego bzu",
        "wysokimi drzewiastymi krzewami czarnego bzu", "wysokich drzewiastych krzewach czarnego bzu"}),
        PL_MESKI_NOS_NZYW);

    dodaj_id_nazwy_calosci("krzew");
    dodaj_id_przym_calosci("wysoki", "wysocy");
    dodaj_id_przym_calosci("czarny", "czarni");
    dodaj_id_przym_calosci("drzewiasty", "drzewiasci");

    ustaw_unid_nazwe_calosci("krzew");
    dodaj_unid_przym_calosci("wysoki", "wysocy");
    dodaj_unid_przym_calosci("drzewiasty", "drzewiasci");

    set_id_long("Bia�e, obsypane ��tym py�kiem, promieniste kwiatuszki "+
        "osadzone na kruchych szypu�kach tworz�ce roz�o�yste, p�askie baldachy "+
        "czarnego bzu odznaczaj� si� niezbyt przyjemn� woni�. Ale o ich w�a�ciwo�ciach "+
        "od�ywczych i przeciwprzezi�bieniowych wiedz� nawet wiejskie kobiety ch�tnie "+
        "przyrz�dzaj�ce z nich orze�wiaj�cy nap�j lub te� podaj�ce je sma�one w cie�cie."+
        "Ceni� owe niepozorne kwiatki r�wnie� medycy sporz�dzaj�cy mieszank� "+
        "napotn� o dzia�aniu przeciwgor�czkowym z�o�on� z wymieszanych w r�wnych "+
        "cz�ciach kwiat�w bzu czarnego, kwiat�w lipy i ziela fio�ka "+
        "tr�jbarwnego. Odwar z tej mieszanki doprawiony sokiem z malin to "+
        "niezast�piony lek stosowany przy grypie i zazi�bieniu. Niekt�rzy medycy "+
        "podaj� te� napar z kwiat�w, li�ci i owoc�w czarnego bzu cukrzykom. \n");

    set_unid_long("Bia�e, obsypane ��tym py�kiem, promieniste kwiatuszki osadzone "+
        "na kruchych szypu�kach tworz�ce roz�o�yste, p�askie baldachy czarnego bzu "+
        "odznaczaj� si� niezbyt przyjemn� woni�. Ale o ich w�a�ciwo�ciach od�ywczych "+
        "i przeciwprzezi�bieniowych wiedz� nawet wiejskie kobiety ch�tnie przyrz�dzaj�ce "+
        "z nich orze�wiaj�cy nap�j lub te� podaj�ce je sma�one w cie�cie. \n");

    ustaw_czas_psucia(350, 400); // schniecie niszczenie
    ustaw_trudnosc_identyfikacji(17);
    ustaw_czestosc_wystepowania(70);
    ustaw_sposob_wystepowania(HERBS_WYST_POJEDYNCZO);

    ustaw_ilosc_w_calosci(3);

    ustaw_trudnosc_znalezienia(10); //Du�y krzak, �atwo znale��

    ustaw_przym_zepsutego("ususzony", "ususzeni");

    set_amount(10); //wartosc od�ywcza

    add_prop(OBJ_I_VOLUME, 17); //FIXME
    add_prop(OBJ_I_WEIGHT, 15); //FIXME
    add_prop(OBJ_I_VALUE, 11);

    ustaw_material(MATERIALY_IN_ROSLINA);

    ustaw_pore_roku(MT_WIOSNA | MT_LATO );
}
