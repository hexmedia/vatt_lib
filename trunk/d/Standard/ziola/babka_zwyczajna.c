/*
 *
 * Babka pospolita - lisc
 * Wystepowanie: W Redanii kurewsko jej duzo. Na lakach i traktach.
 *
 * Zbieranie: od maja do wrze�nia; na mudzie - powiedzmy lato, jesie�.
 * 
 * opis by Brz�zek
 * 
 */

inherit "/std/herb";

#include <pl.h>
#include <herb.h>
#include <mudtime.h>
#include <materialy.h>
#include <stdproperties.h>

void
create_herb()
{
    ustaw_nazwe("li^s^c");
    dodaj_przym("du�y", "duzi");
    dodaj_przym("jajowaty", "jajowaci");      

    ustaw_nazwe_glowna_ziola("li^s^c");
    ustaw_nazwe_ziola("babka zwyczajna");
    dodaj_nazwy("babka");

    dodaj_id_nazwy_calosci("babka zwyczajna");
    ustaw_id_short_calosci(({"li^s^c babki zwyczajnej",
        "li^scia babki zwyczajnej", "li^sciowi babki zwyczajnej",
        "li^s^c babki zwyczajnej", "li^sciem babki zwyczajnej",
        "li^sciu babki zwyczajnej"}),
       ({"li^scie babki zwyczajnej", "li^sci babki zwyczajnej",
        "li^sciom babki zwyczajnej", "li^scie babki zwyczajnej",
        "li^s^cmi babki zwyczajnej", "li^sciach babki zwyczajnej"}),
        PL_ZENSKI); 

    dodaj_id_nazwy_calosci("li^s^c");
    dodaj_id_przym_calosci("du�y", "duzi");
    dodaj_id_przym_calosci("jajowaty", "jajowaci"); 

    ustaw_unid_nazwe_calosci("ro^slina");
    dodaj_unid_przym_calosci("du�y", "duzi");
    dodaj_unid_przym_calosci("jajowaty", "jajowaci"); 

    set_id_long("Do^s^c pot^e^zny i masywny, jak na tak niedu^z^a "+
        "ro^slin^e, nieregularnie poskr^ecany korze^n o niebieskawej "+
        "barwie. Z boku gdzieniegdzie wyrastaj^a male^nkie odn^o^zki, a z "+
        "lekko odartych fragment^ow tr^aci ostrym, do^s^c nieprzyjemnym "+
        "zapachem. Korze^n biedrze^nca jest doskona^lym ^srodkiem "+
        "wykrztu^snym oraz rozkurczaj^acym - ususzony nieco pokruszy^c, "+
        "niewielk^a ilo^s^c zala^c wrz^atkiem i pi^c w niewielkich dawkach. "+
        "Nale^zy uwa^za^c, bo wi^eksza ilo^s^c mo^ze niekorzystnie "+
        "wp^lywa^c na nerki. \n");

    set_unid_long("Do^s^c pot^e^zny i masywny, jak na tak niedu^z^a "+
        "ro^slin^e, nieregularnie poskr^ecany korze^n o niebieskawej "+
        "barwie. Z boku gdzieniegdzie wyrastaj^a male^nkie odn^o^zki, a z "+
        "lekko odartych fragment^ow tr^aci ostrym, do^s^c nieprzyjemnym "+
        "zapachem. \n");

    ustaw_czas_psucia(350, 400); // schniecie niszczenie
    ustaw_trudnosc_identyfikacji(15);
    ustaw_czestosc_wystepowania(90);
    ustaw_sposob_wystepowania(HERBS_WYST_MALE_GRUPY);

    ustaw_ilosc_w_calosci(4);

    ustaw_trudnosc_znalezienia(23); 

    ustaw_przym_zepsutego("ususzony", "ususzeni");

    set_amount(5); //wartosc od^zywcza

    add_prop(OBJ_I_VOLUME, 13); //FIXME
    add_prop(OBJ_I_WEIGHT, 6); //FIXME
    add_prop(OBJ_I_VALUE, 10); //FIXME

    ustaw_material(MATERIALY_IN_ROSLINA); 

    ustaw_pore_roku(MT_LATO | MT_JESIEN);
}
