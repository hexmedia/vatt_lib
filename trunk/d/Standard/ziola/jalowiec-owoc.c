/*
 * Lasy, ponoc glownie gorskie.
 * I ogolnie tam, gdzie gleba nie jest zbyt fajna,
 * u mnie jest duzo jalowcow, a wszedzie mamy piasek
 * - to taka mala wskazowka od favci.
 *
 * Natchniona zjazdem Faeve, 11.02.08!
 */


inherit "/std/herb";

#include <pl.h>
#include <herb.h>
#include <mudtime.h>
#include <materialy.h>
#include <stdproperties.h>

void
create_herb()
{
    ustaw_nazwe("owoc");
    dodaj_przym("niedu^zy", "nieduzi");
    dodaj_przym("czarnobrunatny", "czarnobrunatni");    

    ustaw_nazwe_glowna_ziola("krzew");
    ustaw_nazwe_ziola("ja^lowiec");

    dodaj_id_nazwy_calosci("ja^lowiec");
    ustaw_id_short_calosci(({"smuk^ly ciemnozielony krzew ja^lowca",
        "smuk^lego ciemnozielonego krzewu ja^lowca", 
        "smuk^lemu ciemnozielonemu krzewowi ja^lowca",
        "smuk^ly ciemnozielony krzew ja^lowca", 
        "smuk^lym ciemnozielonym krzewem ja^lowca",
        "smuk^lym ciemnozielonym krzewie ja^lowca"}),
       ({"smuk^le ciemnozielone krzewy ja^lowca", 
        "smuk^lych ciemnozielonych krzew^ow ja^lowca",
        "smuk^lym ciemnozielonym krzewom ja^lowca", 
        "smuk^le ciemnozielone krzewy ja^lowca",
        "smuk^lymi ciemnozielonymi krzewami ja^lowca", 
        "smuk^lych ciemnozielonych krzew^ow ja^lowca"}),
        PL_MESKI_NOS_NZYW); 

    dodaj_id_nazwy_calosci("krzew ja^lowca");
    dodaj_id_przym_calosci("smuk^ly", "smukli");
    dodaj_id_przym_calosci("ciemnozielony", "ciemnozieleni");

    ustaw_unid_nazwe_calosci("krzew");
    dodaj_unid_przym_calosci("smuk^ly", "smukli");
    dodaj_unid_przym_calosci("ciemnozielony", "ciemnozieleni");

    set_id_long("Niedu^zy, wielko^sci ziarnka grochu, owoc o "+
        "czarnobrunatnej barwie pokryty jest sinawoniebieskim nalotem. "+
        "Gdyby nie jego do^s^c nieregularny, cho^c lekko okr^ag^ly kszta^lt "+
        "oraz twardo^s^c niewprawny zielarz m^og^lby pomy^sle^c, i^z ma do "+
        "czynienia z jagod^a, ale profesjonalista ju^z na pierwszy rzut oka "+
        "wie, ^ze to nibyjagoda ja^lowca o silnym dzia^laniu "+
        "bakteriob^ojczym oraz moczop^ednym. ^Ly^zeczka naparu z owoc^ow "+
        "ja^lowca wypita po ka^zdym posi^lku ma dobry wp^lyw na przemian^e "+
        "materii oraz zapobiega wzd^eciom. \n");

    set_unid_long("Niedu^zy, wielko^sci ziarnka grochu, owoc o "+
        "czarnobrunatnej barwie pokryty jest sinawoniebieskim nalotem. "+
        "Gdyby nie jego do^s^c nieregularny, cho^c lekko okr^ag^ly kszta^lt "+
        "oraz twardo^s^c niewprawny zielarz m^og^lby pomy^sle^c, i^z ma do "+
        "czynienia z jagod^a.\n");

    ustaw_czas_psucia(350, 400); // schniecie niszczenie
    ustaw_trudnosc_identyfikacji(17);
    ustaw_czestosc_wystepowania(80);
    ustaw_sposob_wystepowania(HERBS_WYST_MALE_GRUPY);

    ustaw_ilosc_w_calosci(7);

    ustaw_trudnosc_znalezienia(11); //Du^zy krzak, ^latwo znale�^c

    ustaw_przym_zepsutego("ususzony", "ususzeni");

    set_amount(3); //wartosc od^zywcza

    add_prop(OBJ_I_VOLUME, 4); //FIXME
    add_prop(OBJ_I_WEIGHT, 3); //FIXME
    add_prop(OBJ_I_VALUE, 20);

    ustaw_material(MATERIALY_IN_ROSLINA);

    ustaw_pore_roku(MT_JESIEN | MT_ZIMA); 
}
