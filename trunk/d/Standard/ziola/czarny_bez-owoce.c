/**
 * wyst^epowanie: ^l^egi, ale og^olnie to wsz^edzie
 *
 * by Brz^ozek
 *
 * Poprawki - Krun 2007
 */


inherit "/std/herb";

#include <pl.h>
#include <herb.h>
#include <mudtime.h>
#include <materialy.h>
#include <stdproperties.h>

void
create_herb()
{
    ustaw_nazwe_glowna_ziola("ki^s^c owoc^ow");
    ustaw_nazwe_ziola("bez");
    dodaj_przym_zident("czarny", "czarni");
    dodaj_nazwy("ki^s^c");
    dodaj_nazwy("owoce");

    ustaw_nazwe("owoce");
    ustaw_nazwe_glowna("ki�^c");
    dodaj_przym("ciemnofioletowy", "ciemnofioletowi");

    dodaj_id_nazwy_calosci("bez");
    ustaw_id_short_calosci(({"wysoki drzewiasty krzew czarnego bzu",
        "wysokiego drzewiastego krzewu czarnego bzu", "wysokiemu drzewiastemu krzewowi czarnego bzu",
        "wysoki drzewiasty krzew czarnego bzu", "wysokim drzewiastym krzewem czarnego bzu",
        "wysokim drzewiastym krzewie czarnego bzu"}),
        ({"wysokie drzewiaste krzewy czarnego bzu", "wysokich drzewiastych krzew^ow czarnego bzu",
        "wysokim drzewiastym krzewom czarnego bzu", "wysokie drzewiaste krzewy czarnego bzu",
        "wysokimi drzewiastymi krzewami czarnego bzu", "wysokich drzewiastych krzewach czarnego bzu"}),
        PL_MESKI_NOS_NZYW);

    dodaj_id_nazwy_calosci("krzew");
    dodaj_id_przym_calosci("wysoki", "wysocy");
    dodaj_id_przym_calosci("czarny", "czarni");
    dodaj_id_przym_calosci("drzewiasty", "drzewiasci");

    ustaw_unid_nazwe_calosci("krzew");
    dodaj_unid_przym_calosci("wysoki", "wysocy");
    dodaj_unid_przym_calosci("drzewiasty", "drzewiasci");

    set_id_long("Ma^le, kuliste pestkowce barwy tak ciemnofioletowej, ^ze prawie "+
        "czarnej zawieszone na kruchych szypu^lkach tworz� ci^e^zkie baldachokszta^ltne "+
        "ki�cie owoc^ow czarnego bzu. Owoce te wykorzystywane s� przez wie�niaczki do "+
        "produkcji sok^ow i marmolad zapewniaj�cych witaminy w czasie zimowych ch^lod^ow. "+
        "Co bardziej dba^le o urod^e niewiasty u^zywaj� te^z tego soku do farbowania "+
        "w^los^ow. Te wykorzystywane przez wszystkich w gospodarstwie domowym niepozorne "+
        "owocki kryj� w sobie medyczne skarby, a szczeg^olnie skuteczne s� w zwalczaniu "+
        "b^olu r^o^znego pochodzenia nie powoduj�c uzale^znie^n. Odwar z suszonych owoc^ow "+
        "lub sok ze �wie^zych pity 2-4 razy dziennie przynosi ulg^e w przypadkach "+
        "ischiasu, nerwob^oli i migreny. Dodatkowo ^lagodnie przeczyszcza co ma "+
        "dzia^lanie odtruwaj�ce. W niekt^orych karczmach szklaneczka soku o poranku "+
        "przewidziana jest gratisowo dla klient^ow, kt^orzy nadu^zyli w nocy trunk^ow. "+
        "Niekt^orzy medycy podaj� te^z napar z kwiat^ow, li�ci i owoc^ow czarnego bzu "+
        "cukrzykom. \n");

    set_unid_long("Ma^le, kuliste pestkowce barwy tak ciemnofioletowej, ^ze prawie "+
        "czarnej, zawieszone na kruchych szypu^lkach tworz� ci^e^zkie "+
        "baldachokszta^ltne ki�cie owoc^ow czarnego bzu. Owoce te wykorzystywane s� "+
        "przez wie�niaczki do produkcji sok^ow i marmolad zapewniaj�cych witaminy "+
        "w czasie zimowych ch^lod^ow. Co bardziej dba^le o urod^e niewiasty u^zywaj� "+
        "te^z tego soku do farbowania w^los^ow. \n");

    ustaw_czas_psucia(350, 400); //schniecie, niszczenie
    ustaw_trudnosc_identyfikacji(17); // Trza miec uma powyzej 17 coby to zidentyfikowa^c.. oko^lo 10 coby znale�^c.
    ustaw_czestosc_wystepowania(70); // Bedzie wystepowac na 70% lokacji
    ustaw_sposob_wystepowania(HERBS_WYST_POJEDYNCZO); //Rosnie pojedynczo

    ustaw_ilosc_w_calosci(4); //W jednym du^zym ziole mo^ze by^c do 4 owoc^ow... Mo^ze te^z nie by^c
                              //^zadnego.

    ustaw_trudnosc_znalezienia(10); //Du^zy krzak, ^latwo znale�^c

    set_amount(13 + random(4)); //po^zywno�^c

    ustaw_przym_zepsutego("zgni^ly", "zgnili");

    add_prop(OBJ_I_VALUE, 2);
    add_prop(OBJ_I_VOLUME, 90 + random(10));
    add_prop(OBJ_I_WEIGHT, 90 + random(10));

    ustaw_material(MATERIALY_IN_ROSLINA);

    ustaw_pore_roku(MT_WIOSNA | MT_LATO);
}
