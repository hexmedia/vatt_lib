/**
 * \file /cmd/live/items.c
 *
 * Komendy zdefiniowane w tym pliku
 *
 * - chwy�
 * - dob�d�
 * - ociosaj
 * - odepnij
 * - od�am
 * - otw�rz
 * - przeczytaj
 * - rozbierz
 * - �ci�gnij
 * - ubierz
 * - uderz
 * - u�am
 * - urwij
 * - walnij
 * - w��
 * - wy�am
 * - wypij
 * - wyryj
 * - wywa�
 * - za��
 * - zamknij
 * - zapal
 * - zapnij
 * - zapukaj
 * - zastukaj
 * - zetnij
 * - zga�
 * - zjedz
 *
 * TODO:
 * - trzeba przenie�� wyryj i u�am do standardu drzewa:) (krun)
 */

#pragma no_clone
#pragma no_inherit
#pragma save_binary
#pragma strict_types

inherit "/cmd/std/command_driver";

#include <files.h>
#include <macros.h>
#include <cmdparse.h>
#include <language.h>
#include <composite.h>
#include <stdproperties.h>

//Te dwa definy nizej potrzebne byly do chwyc/pusc. Lil.
/* Use an item in the actor's inventory */
#define USE_INV(str, func, silent, one, formy, przyp) \
    use_described_items(str, all_inventory(this_player()), func, silent, one, formy, przyp)

#define USE_INV2(str, func, solent, one, formy, przyp, patt) \
    use_described_items(str, all_inventory(this_player()), func, silent, one, formy, przyp, patt)

/* Use an item in the actor's inventory or environment */
#define USE_ENV(str, func, silent, one, formy, przyp) \
    use_described_items(str, all_inventory(this_player()) + \
    all_inventory(environment(this_player())), func, silent, one, formy, przyp)

#define USE_ENV2(str, func, silent, one, formy, przyp, patt) \
    use_described_items(str, all_inventory(this_player()) + \
    all_inventory(environment(this_player())), func, silent, one, formy, przyp, 0, 0, patt)

/*
 * Use the #if 0 trick to fool the document maker. This way, we can here define
 * a description of all the functions this soul may call in items.
 */

/* **************************************************************************
 * Return a proper name of the soul in order to get a nice printout.
 */
string
get_soul_id()
{
    return "items";
}

/* **************************************************************************
 * This is a command soul.
 */
int
query_cmd_soul()
{
    return 1;
}

/*
 * Function name: using_soul
 * Description:   Called once by the living object using this soul. Adds
 *                sublocations responsible for extra descriptions of the
 *                living object.
 */
public void
using_soul(object live)
{
}

/* **************************************************************************
 * The list of verbs and functions. Please add new in alfabetical order.
 */
mapping
query_cmdlist()
{
    return
        ([
         "chwy�"        : "chwyc",

         "dob�d�"       : "dobadz",

         "ociosaj"      : "ociosaj",
         "odepnij"      : "odepnij",
         "od�am"        : "ulam",
         "opu��"        : "opusc",
         "otw�rz"       : "otworz",

         "przypnij"     : "zapnij",
         "przeczytaj"   : "przeczytaj",

         "rozbierz"     : "zdejmij",

         "�ci�gnij"     : "zdejmij",

         "ubierz"       : "zaloz",
         "uderz"        : "walnij",
         "u�am"         : "ulam",
         "urwij"        : "urwij",

         "walnij"       : "walnij",
    /*   "w��"         : "zaloz",   Ehh...Krun, nie �wiruj. Vera. */
         "wy�am"        : "wylam",
         "wypij"        : "wypij",
         "wyryj"        : "wyryj",
         "wywa�"        : "wylam",

         "zamknij"      : "zamknij",
         "za��"        : "zaloz",
         "zapal"        : "zapal",
         "zapnij"       : "zapnij",
         "zapukaj"      : "zapukaj",
         "zastukaj"     : "zapukaj",
         "zetnij"       : "zetnij",
         "zdejmij"      : "zdejmij",
         "zga�"         : "zgas",
         "zjedz"        : "zjedz"
         ]);
}

//A takze ta funkcja: use_items.  Lil.

/*
 * Function name: use_items
 * Description:   Cause the actor to "use" the given items
 * Arguments:     object *items - the items to use
 *                function f - the function to call in the objects found to
 *                             use them.
 *                int silent - suppress the default message given when items
 *                             are successfully used.
 * Returns:       object * - an array consisting of the objects that were
 *                           successfully used
 */
varargs public object *
use_items(object *items, function f, int silent, string *formy, int przyp = PL_BIE, string patt=0)
{
    mixed res;
    object *used = ({ });
    string fail_msg = "";
    int fail, i;

    foreach(object ob : items)
    {
        /* Call the function to "use" the item */
        res = f(ob);

        if(!res)
        {
            /* The item cannot be used in this way */
            continue;
        }

        if(stringp(res))
        {
            /* The attempt to use the item failed */
            fail = 1;
            fail_msg += res;
        }
        else
        {
            /* The item was successfully used */
            used += ({ ob });
        }
    }

    if(!sizeof(used))
    {
        /* Nothing could be used.  Say why. */

        if(!fail)
        {
            if(patt)
            {
                notify_fail("Nie mo�esz " + (sprintf(patt,
                    ((sizeof(items) > 1) ? "nich" : (przyp == PL_DOP ? "tego" : "to")))) +
                    " " + formy[0] + ".\n");
                return 0;
            }
            else
            {
                notify_fail("Nie mo�esz " +
                    ((sizeof(items) > 1) ? "ich" : "tego") + " " + formy[0] + ".\n");
                return 0;
            }
        }

        notify_fail(fail_msg, 4);//write(fail_msg);
        return 0;//return ({});
    }

    if(!silent)
    {
        if(patt)
        {
            write(capitalize(formy[1]) + " " + sprintf(patt, COMPOSITE_DEAD(used, przyp)) + ".\n");
            saybb(QCTNAME(this_player()) + " " + formy[2] + " " + sprintf(patt,  QCOMPDEAD(przyp)) + ".\n");
        }
        else
        {
            write(capitalize(formy[1]) + " " + COMPOSITE_DEAD(used, przyp) + ".\n");
            saybb(QCTNAME(this_player()) + " " + formy[2] + " " + QCOMPDEAD(przyp) + ".\n");
        }
    }

    this_player()->set_obiekty_zaimkow(used);

    return used;
}

/**
 * Funkcja filtruje ca�o�� tak, a�eby warto�ci kt�re
 * zgadzaj� si� z formu�� zosta�y.
 *
 * @param ob    obiekt na kt�rym wywo�ywana jest funkcja
 * @param fun   funkcja wywo�ywana na obiekcie.
 * @param args  argumenty
 */
varargs int
filter_all_items(mixed ob, string fun, mixed args = ({}))
{
    if(!objectp(ob))
        return 1;

    if(sizeof(args))
    {
        if(call_otherv(ob, fun, args) != 0)
            return 1;
    }
    else
    {
        if(call_other(ob, fun) != 0)
            return 1;
    }

    return 0;
}

/**
 * Funkcja sortuje itemy w odpowiedniej kolejno�ci.
 *
 * @param items itemy do posortowania
 * @param fun   funkcja u�ywana przy sortowaniu
 * @param verb  czasownik u�yty do wywo�ania
 * @param kier  kierunek sortowania
 * @param ...   argumenty do funkcji u�ywanej przy sortowaniu
 *
 * @return posortowana tablica
 */
private static varargs mixed
sort_items(object *items, string fun, string verb, int kier, ...)
{
    object *x, *y;

    x = filter(items, &filter_all_items(, fun, argv));
    y = items;
    y -= x;

    items = (kier ? y + x : x + y);

    return items;
}

/**
 * Funkcja sprawdzaj�ca czy wywo�ana komenda jest danego typu.
 * @param kom wywo�ana komenda.
 */
public int
check_verb(string str)
{
    mapping cmdl = query_cmdlist();
    string *ind = m_indexes(cmdl);
    string *val = m_values(cmdl);

    int i = member_array(str, val);

    if(i == -1)
        return 0;

    if(ind[i] == query_verb())
        return 1;
}

/**
 * Ta funckja wykorzystywana jest tylko w momencie kiedy mamy doczynienia ze zmienionym
 * patternem podanym do use_described_items. Wtedy musimy odopwiednio sformatowa�
 * komunikaty i w�a�nie temu celowi s�u�y niniejsza funkcja:)
 *
 * @param patt pattern przekazany do use_described_items
 */
private string
change_patt_to_notify_patt(string patt)
{
    mixed tmp;
    if(sizeof(tmp=explode(implode(explode(patt, "'"), ""), "%")) == 2)
    {
        if(wildmatch("?:?", tmp[1]))
            return tmp[0] + "%s" + tmp[1][3..];
        else
            return tmp[0] + "%s";
    }
    return 0;
}

/**
 * Function name: use_described_items
 * Description:   Given a string, cause the actor to "use" the items described
 *                by the string.
 * Arguments:     string str  - the string describing what to use
 *                object *obs - the items to be matched with the string
 *                function f  - the function to call in the objects found to
 *                              use them.
 *                int silent  - suppress the default message given when items are
 *                              successfully used.
 *                int use_one - Allow only one item to be used
 * Returns:       0 - No items found that matched the string describer
 *                object * - an array consisting of the objects that were
 *                           successfully used
 */
varargs public object *
use_described_items(string str, object *obs, function f, int silent,
    int use_one, string *formy, int przyp = PL_BIE, string acsfunc = 0, object acsobj = 0, string patt = 0)
{
    mixed items;

    if(patt)
        string patt2 = change_patt_to_notify_patt(patt);

    if(patt2)
    {
        notify_fail(capitalize(sprintf(patt2, (przyp == PL_DOP ? "czego" : "co"))) +
            " chcesz " + formy[0] + "?\n", 0);
    }
    else
    {
        notify_fail((przyp == PL_DOP ? "Czego" : "Co") + " chcesz " +
            formy[0] + "?\n", 0);
    }

    if(!str || !strlen(str))
        return 0;

    patt = patt || "%i:" + przyp;

    //Tu robimy troche sortowania, tak, �eby system by� troche bardziej domy�lny [Krun]
    if(check_verb("zdejmij") || check_verb("zaloz"))
    {   //rzeczy zdj�te czy za�o�one
        obs = sort_items(obs, "query_worn", query_verb(), check_verb("zaloz"));
    }
    else if(check_verb("dobadz") || check_verb("opusc"))
    {   //rzeczy dobyte czy opuszczone
        obs = sort_items(obs, "query_wielded", query_verb(), check_verb("dobadz"));
    }
    else if(check_verb("zapnij") || check_verb("przypnij") || check_verb("odepnij"))
    {   //rzeczy zapiete czy odpiete
        obs = sort_items(obs, "query_prop", query_verb(), (check_verb("zapnij") || check_verb("przypnij")), OBJ_I_ZAPIETY);
    }
    else if(check_verb("otworz") || check_verb("zamknij") || check_verb("wylam"))
    {   //rzeczy otwarte czy zamkni�te
        obs = sort_items(obs, "query_lock", query_verb(), !check_verb("otworz"));
        obs = sort_items(obs, "query_open", query_verb(), check_verb("otworz"));
    }

    if(!parse_command(str, obs, patt, items))
        return 0;

    items = NORMAL_ACCESS(items, acsfunc, acsobj);

    if(!sizeof(items))
        return 0;

    if(use_one && (sizeof(items) > 1))
    {
        //A tu zn�w k�opot z patternami.
        if(patt2)
        {
            NF("Mo�esz " + formy[0] + " naraz tylko " +
                sprintf(patt2, (przyp == PL_DOP ? "jednej rzeczy" : "jedn� rzecz")) + ".\n");
            return 0;
        }

        notify_fail("Mo�esz " + formy[0] + " naraz tylko jedn� rzecz.\n");
        return 0;
    }

    if(patt2)
        return use_items(items, f, silent, formy, przyp, patt2);

    return use_items(items, f, silent, formy, przyp);
}

public int
chwyc(string str)
{
    //Doda�em mo�liwo�� dobywania konkretn� r�k� [Krun].
    if(!USE_INV(str, &->hold_me(), 1, 0, ({"chwyci�", "chwytasz", "chwyta"}), PL_BIE))
    {
        string czym;
        object *co;

        if(!str)
        {
            NF("Co chcesz chwyci�?\n");
            return 0;
        }

        if(!parse_command(str, AI(TP), "%i:" + PL_BIE + " %s", co, czym))
        {
            NF("Co chcesz chwyci�?\n");
            return 0;
        }

        co = NORMAL_ACCESS(co, 0, 0);

        if(!czym)
        {
            NF("Co chcesz chwyci�?\n");
            return 0;
        }

        if(!co)
        {
            NF("Co chcesz chwyci�?\n");
            return 0;
        }

        if(sizeof(co) > 1)
        {
            NF("Nie mo�esz chwyci� wi�cej ni� jednej rzeczy jednocze�nie.\n");
            return 0;
        }

        return !!use_items(co, &->hold_me(0, czym), 1, ({"chwyci�", "chwyta", "chwytasz"}), PL_BIE);
    }
    else
        return 1;
}

nomask int
is_pochwa_in_inv(object ob)
{
    if (!objectp(ob))
        return 0;
    if (environment(ob) == this_player())
        return 1;
    if (environment(ob) == environment(this_player()))
        return 1;
    if (environment(ob)->query_is_pochwa() && (environment(environment(ob)) == this_player()))
        return 1;
    return 0;
}

public int
dobadz(string str)
{
    //Doda�em mo�liwo�� dobywania konkretn� r�k� [Krun].
    if(!use_described_items(str, deep_inventory(this_player()), &->wield_me(), 1, 0,
        ({"doby�", "dobywasz", "dobywa"}), PL_DOP, "is_pochwa_in_inv", TO))
    {
        string czym;
        object *co;

        if(!str)
        {
            NF("Czego chcesz doby�?\n");
            return 0;
        }

        if(!parse_command(str, deep_inventory(TP), "%i:" + PL_DOP + " %s", co, czym))
        {
            NF("Czego chcesz doby�?\n");
            return 0;
        }

        co = NORMAL_ACCESS(co, "is_pochwa_in_inv", TO);

        if(!czym)
        {
            NF("Czego chcesz doby�?\n");
            return 0;
        }

        if(!co)
        {
            NF("Czego chcesz doby�?\n");
            return 0;
        }

        if(sizeof(co) > 1)
            NF("Nie mo�esz doby� wi�cej ni� jednej rzeczy jednocze�nie.\n");

        return !!use_items(co, &->wield_me(0, czym), 1, ({"doby�", "dobywasz", "dobasz"}), PL_DOP);
    }
    else
        return 1;
}


public int
ociosaj(string str)
{
    return !!USE_ENV(str, &->try_ociosaj(), 1, 1, ({"ociosa�", "ociosujesz", "ociosuje"}), PL_BIE);
}

public int
odepnij(string str)
{
    return !!USE_INV(str, &->unbutton_me(), 1, 0, ({"odpi��", "odpinasz", "odpina"}), PL_BIE);
}

public int
opusc(string str)
{
    return !!USE_INV(str, &->unwield_me(), 1, 0, ({"opu�ci�", "opuszczasz", "opuszcza"}), PL_BIE);
}

public int
otworz(string str)
{
    //Musimy zrobi� wyj�tek na otwieranie kluczem

    NF("Co chcesz otworzy�?\n");

    if(!str)
        return 0;

    object *inv = all_inventory(ENV(TP));

    inv = sort_items(inv, "query_lock", query_verb(), !check_verb("otworz"));
    inv = sort_items(inv, "query_open", query_verb(), !check_verb("otworz"));

    object *co, *czym;
    string subloc, rest;

    //hhmmm musia�em troche przerobi� ca�o��.
    if(!!USE_ENV(str, &->open_me(), 1, 0, ({"otworzy�", "otwierasz", "otwiera"}), PL_BIE))
        return 1;
    else if(parse_command(str, inv, "%i:" + PL_BIE + " %s", co, rest))
    {
        object *inv2;
        co = NORMAL_ACCESS(co, 0, 0);

        inv2 = sort_items(all_inventory(TP), "is_picklock", query_verb(), 1);
        inv2 = sort_items(inv2, "is_key", query_verb(), 1);

        if(parse_command(rest, inv2, "%i:" + PL_NAR, czym))
            czym = NORMAL_ACCESS(czym, 0, 0);
        else
            return 0;
    }
    else if(parse_command(str, inv, "%s %i:" + PL_BIE, subloc, co))
        co = NORMAL_ACCESS(co, 0, 0);
    else if(parse_command(str, inv, "%s %i:" + PL_BIE + " %s", subloc, co, rest))
    {
        object *inv2;

        co = NORMAL_ACCESS(co, 0, 0);

        inv2 = sort_items(all_inventory(TP), "is_picklock", query_verb(), 1);
        inv2 = sort_items(inv2, "is_key", query_verb(), 1);

        if(parse_command(rest, inv2, "%i:" + PL_NAR, czym))
            czym = NORMAL_ACCESS(czym, 0, 0);
        else
            return 0;
    }
    else
        return 0;

    //Je�li nie standard to niestety musimy sie ju� trosze wysili�.

    if(!sizeof(co))
        return 0;

    mixed ret;
    if(subloc && sizeof(czym) > 0 && objectp(czym[0]) && czym[0]->is_picklock())
        ret = co[0]->pick_subloc(subloc, czym[0]);
    else if(subloc && sizeof(czym) > 0 && czym[0])
        ret = co[0]->unlock_sublock(subloc, czym[0]);
    else if(subloc)
        ret = co[0]->open_subloc(subloc);
    else if(sizeof(czym) > 0 && objectp(czym[0]) && czym[0]->is_picklock())
        ret = co[0]->pick_me(czym[0]);
    else
        ret = co[0]->unlock_me(czym[0]);

    if(ret &&  stringp(ret))
        return NF(ret);
    else if(!ret && subloc)
        return NF("Co czym chcesz otworzy�?\n");
    else if(!ret)
        return NF("Co chcesz otworzy�?\n");

    return 1;
}

public int
przeczytaj(string str)
{
    return !!USE_ENV(str, &->command_read(), 1, 1, ({"przeczyta�", "czytasz", "czyta"}), PL_BIE);
}

public int
ulam(string str)
{
    notify_fail("Co i sk�d chcesz u�ama�?\n");
    if(!str)
        return 0;

    object *objs;
    if(!parse_command(lower_case(str), environment(TP), " 'ga���' 'z'  %i:"+PL_DOP , objs))
        return 0;

    objs = NORMAL_ACCESS(objs, 0, 0);

    if(!objs || !sizeof(objs))
        return 0;

    if(sizeof(objs) > 1)
        return 0;

    if(function_exists("create_object", objs[0]) != "/std/drzewo")
        return 0;

    mixed ret = objs[0]->try_ulam();

    if(stringp(ret))
        write(ret);

    return 1;
}

public int
walnij(string str)
{
    if(wildmatch("w *",str))
        return !!USE_ENV2(str, &->bang_me(), 1, 0, ({"waln��", "walisz", "wali"}), PL_BIE, "'w' %i:"+PL_BIE);
    else
        return !!USE_ENV2(str, &->bang_me(), 1, 0, ({"waln��", "walisz", "wali"}), PL_DOP, "'do' %i:"+PL_DOP);
}

public int
wylam(string str)
{
    NF("Co chcesz wy�ama�?\n");

    if(!str)
        return 0;

    string subloc, rest;
    object *co, *czym;

    object *inv2, *inv = AI(TP) + AIE(TP);

    inv = sort_items(inv, "query_lock", query_verb(), !check_verb("otworz"));
    inv = sort_items(inv, "query_open", query_verb(), check_verb("otworz"));

    //hhmmm musia�em troche przerobi� ca�o��.
    if(!!USE_ENV(str, &->breakdown_me(), 1, 0, ({"wy�ama�", "wy�amujesz", "wy�amuje"}), PL_BIE))
        return 1;
    else if(parse_command(str, inv, "%i:" + PL_BIE + " %s", co, rest))
    {
        co = NORMAL_ACCESS(co, 0, 0);

        inv2 = sort_items(all_inventory(TP), "is_crowbar", query_verb(), 1);

        if(parse_command(rest, inv2, "%i:" + PL_NAR, czym))
            czym = NORMAL_ACCESS(czym, 0, 0);
        else
            return 0;
    }
    else if(parse_command(str, inv, "%s %i:" + PL_BIE, subloc, co))
        co = NORMAL_ACCESS(co, 0, 0);
    else if(parse_command(str, inv, "%s %i:" + PL_BIE + " %s", subloc, co, rest))
    {
        co = NORMAL_ACCESS(co, 0, 0);

        inv2 = sort_items(all_inventory(TP), "is_crowbar", query_verb(), 1);

        if(parse_command(rest, inv2, "%i:" + PL_NAR, czym))
            czym = NORMAL_ACCESS(czym, 0, 0);
        else
            return 0;
    }
    else
        return 0;

    //Je�li nie standard to niestety musimy sie ju� trosze wysili�.

    if(!sizeof(co))
        return 0;

    mixed ret;
    if(subloc && sizeof(czym) > 0 && czym[0])
        ret = co[0]->breakdown_subloc(subloc, czym[0]);
    else if(subloc)
        ret = co[0]->breakdown_subloc(subloc);
    else if(sizeof(czym) > 0 && czym[0])
        ret = co[0]->breakdown_me(czym[0]);

    if(ret &&  stringp(ret))
        return NF(ret);
    else if(!ret && subloc)
        return NF("Co czym chcesz wy�ama�?\n");
    else if(!ret)
        return NF("Co chcesz wy�ama�?\n");

    return 1;
}

public int
wypij(string str)
{
    object *drinkable;

    if (!(drinkable = USE_INV(str, &->command_drink(), 0, 0, ({"wypi�", "pijesz", "pije"}), PL_BIE)))
    {
        return 0;
    }

    /* Remove the drinks after we are through */
    drinkable->remove_drink();

    return 1;
}

public int
wyryj(string str)
{
    notify_fail("Chyba wyryj <na czym> napis <tekst>?\n");

    if(!str)
        return 0;

    object *objs;
    string tekst="";
    if(!parse_command(str, environment(TP), " 'na' %i:"+PL_MIE+" 'napis' %s ", objs, tekst))
        return 0;

    objs = NORMAL_ACCESS(objs, 0, 0);

    if(!objs)
        return 0;

    if(sizeof(objs) > 1)
        return 0;

    if(strlen(tekst) < 1)
        return 0;

    mixed ret = objs[0]->try_wyryj(tekst);

    if(stringp(ret))
        write(ret);

    return 1;
}

public int
zaloz(string str)
{
    //Doda�em zak�adanie na konkretny hitloc [Krun].
    if(!USE_INV(str, &->wear_me(), 1, 0, ({"za�o�y�", "zak�adasz", "zak�ada"}), PL_BIE))
    {
        if(!str)
        {
            NF("Co takiego chcesz za�o�y�?\n");
            return 0;
        }

        object *co;
        string na_co;
        if(!parse_command(str, AI(TP), "%i:" + PL_BIE + " 'na' %s", co, na_co))
            return 0;

        co = NORMAL_ACCESS(co, 0, 0);

        if(!co)
        {
            NF("Co takiego chcesz za�o�y�?\n");
            return 0;
        }

        if(sizeof(co) > 1)
        {
            NF("Nie mo�esz na jedno miejsce za�o�y� wi�cej ni� jedn� rzecz na raz.\n");
            return 0;
        }

        return !!use_items(co, &->wear_me(0, 0, na_co), 1, ({"za�o�y�", "zak�ada", "zak�adasz"}), PL_BIE);
    }
    else
        return 1;
}

public int
zamknij(string str)
{
    //Musimy zrobi� wyj�tek na zamykanie kluczem

    NF("Co chcesz zamkn��?\n");

    if(!str)
        return 0;

    string subloc, rest;
    object *co, *czym;

    object *inv2, *inv = AI(TP) + AIE(TP);

    //hhmmm musia�em troche przerobi� ca�o��.
    if(!!USE_ENV(str, &->close_me(), 1, 0, ({"zamkn��", "zamykasz", "zamyka"}), PL_BIE))
        return 1;
    else if(parse_command(str, inv, "%i:" + PL_BIE + " %s", co, rest))
    {
        co = NORMAL_ACCESS(co, 0, 0);

        inv2 = sort_items(all_inventory(TP), "is_picklock", query_verb(), 1);
        inv2 = sort_items(inv2, "is_key", query_verb(), 1);

        if(parse_command(rest, inv2, "%i:" + PL_NAR, czym))
            czym = NORMAL_ACCESS(czym, 0, 0);
        else
            return 0;
    }
    else if(parse_command(str, inv, "%s %i:" + PL_BIE, subloc, co))
        co = NORMAL_ACCESS(co, 0, 0);
    else if(parse_command(str, inv, "%s %i:" + PL_BIE + " %s", subloc, co, rest))
    {
        co = NORMAL_ACCESS(co, 0, 0);

        inv2 = sort_items(all_inventory(TP), "is_picklock", query_verb(), 1);
        inv2 = sort_items(inv2, "is_key", query_verb(), 1);

        if(parse_command(rest, inv2, "%i:" + PL_NAR, czym))
            czym = NORMAL_ACCESS(czym, 0, 0);
        else
            return 0;
    }
    else
        return 0;

    //Je�li nie standard to niestety musimy sie ju� trosze wysili�.

    if(!sizeof(co))
        return 0;

    mixed ret;
    if(subloc && sizeof(czym) > 0 && czym[0])
        ret = co[0]->lock_subloc(subloc, czym[0]);
    else if(subloc)
        ret = co[0]->close_subloc(subloc);
    else if(sizeof(czym) > 0 && czym[0])
        ret = co[0]->lock_me(czym[0]);

    if(ret &&  stringp(ret))
        return NF(ret);
    else if(!ret && subloc)
        return NF("Co czym chcesz zamkn��?\n");
    else if(!ret)
        return NF("Co chcesz zamkn��?\n");

    return 1;

}

public int
zapal(string str)
{
    return !!USE_ENV(str, &->light_me(), 1, 0, ({"zapali�", "zapalasz", "zapala"}), PL_BIE);
}

public int
zapnij(string str)
{
    return !!USE_INV(str, &->button_me(), 1, 0, ({"zapi��", "zapinasz", "zapina"}), PL_BIE);
}

public int
zapukaj(string str)
{
    if(wildmatch("w *", str))
        return !!USE_ENV2(str, &->knock_me(), 1, 0, ({"zapuka�", "pukasz", "puka"}), PL_BIE, "'w' %i:" + PL_BIE);
    else
        return !!USE_ENV2(str, &->knock_me(), 1, 0, ({"zapuka�", "pukasz", "puka"}), PL_DOP, "'do' %i:" + PL_DOP);
}

public int
zetnij(string str)
{
    return !!USE_ENV(str, &->try_zetnij(), 1, 1, ({"�ci��", "�cinasz", "�cina"}), PL_BIE);
}

public int
zdejmij(string str)
{
    return !!USE_INV(str, &->remove_me(), 1, 0, ({"zdj��", "zdejmujesz", "zdejmuje"}), PL_BIE);
}

public int
zgas(string str)
{
    return !!USE_ENV(str, &->extinguish_me(), 1, 0, ({"zgasi�", "gasisz", "gasi"}), PL_BIE);
}

public int
zjedz(string str)
{
    object *eatable;

    if (!(eatable = USE_INV(str, &->command_eat(), 0, 0, ({"zje��", "zjadasz", "zjada"}), PL_BIE)))
    {
        return 0;
    }

    /* Remove the food after we are through */
    eatable->remove_food();

    return 1;
}
