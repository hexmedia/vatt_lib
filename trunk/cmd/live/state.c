/**
 * \file /cmd/live/state.c
 *
 * General commands for finding out a livings state.
 * The following commands are:
 *
 * - cechy
 * - email
 * - k(ondycja)
 * - kondycja
 * - opcje
 * - por�wnaj
 * - powr�� (tylko w czasie Apokalipsy)
 * - poziomy
 * - przetrwaj�
 * - stan
 * - um(iej�tno�ci)
 * - umiej�tno�ci
 */

#pragma no_inherit
#pragma no_reset
#pragma save_binary
#pragma strict_types

inherit "/cmd/std/command_driver";

#include <std.h>
#include <time.h>
#include <const.h>
#include <files.h>
#include <macros.h>
#include <colors.h>
#include <mudtime.h>
#include <options.h>
#include <cmdparse.h>
#include <language.h>
#include <ss_types.h>
#include <formulas.h>
#include <composite.h>
#include <state_desc.h>
#include <filter_funs.h>
#include <stdproperties.h>
#include "/config/login/login.h"

#define SUBLOC_MISCEXTRADESC "_subloc_misc_extra"

/*
 * Global constants
 */
private mixed   stat_strings,
                denom_strings,
                brute_fact,
                panic_state,
                fatigue_state,
                intox_state,
                head_state,
                stuff_state,
                soak_state,
                improve_fact,
                skillmap,
                compare_strings,
                health_state,
                mana_state;

private string *stat_names, *stat_names_bie, *enc_weight;

private mapping lev_map;

/*
 * Prototypes
 */
public varargs string get_proc_text(int num, mixed maindesc, int turnpoint, mixed subdesc, int kierunek);

void
create()
{
    seteuid(getuid(this_object()));

    /* These global arrays are created once for all since they are used
       quite often. They should be considered constant, so do not mess
       with them
    */

    skillmap =          SS_SKILL_DESC;
    denom_strings =     SD_STAT_DENOM;

    stat_names =        SD_STAT_NAMES;
    stat_names_bie =    SD_STAT_NAMES_BIE;

    stat_strings =  ({
                ({ SD_STATLEV_STR("y"), SD_STATLEV_DEX("y"),
                   SD_STATLEV_CON("y"), SD_STATLEV_INT("y"),
                   SD_STATLEV_DIS("y"), }),
                ({ SD_STATLEV_STR("a"), SD_STATLEV_DEX("a"),
                   SD_STATLEV_CON("a"), SD_STATLEV_INT("a"),
                   SD_STATLEV_DIS("a") })
                });

    compare_strings = ({
                ({ SD_COMPARE_STR("y"), SD_COMPARE_DEX("y"),
                   SD_COMPARE_CON("y") }), ({ SD_COMPARE_STR("a"),
                   SD_COMPARE_DEX("a"), SD_COMPARE_CON("a") }),
                ({ SD_COMPARE_STR("e"), SD_COMPARE_DEX("e"),
                   SD_COMPARE_CON("e") })
                });

    brute_fact =    ({ SD_BRUTE_FACT("y"), SD_BRUTE_FACT("a") });
    mana_state =    ({ SD_MANA("y"), SD_MANA("a") });
    panic_state =   ({ SD_PANIC("y"), SD_PANIC("a") });
    health_state =  ({ SD_HEALTH("y"), SD_HEALTH("a"), SD_HEALTH("e") });
    fatigue_state = ({ SD_FATIGUE("y"), SD_FATIGUE("a") });
    stuff_state =   ({ SD_STUFF("y"), SD_STUFF("a") });
    intox_state =   ({ SD_INTOX("y"), SD_INTOX("a") });

    soak_state =    SD_SOAK;
    head_state =    SD_HEADACHE;
    improve_fact =  SD_IMPROVE;
    enc_weight =    SD_ENC_WEIGHT;
}

/* **************************************************************************
 * Return a proper name of the soul in order to get a nice printout.
 */
string
get_soul_id()
{
    return "state";
}

/* **************************************************************************
 * This is a command soul.
 */
int
query_cmd_soul()
{
    return 1;
}

/* **************************************************************************
 * The list of verbs and functions. Please add new in alfabetical order.
 */
mapping
query_cmdlist()
{
    return ([
            "cechy"         : "cechy",

            "email"         : "email",
            "k"             : "kondycja",
            "kalendarz"     : "kalendarz",
            "kondycja"      : "kondycja",

            "opcje"         : "opcje",
            "odmie�"        : "odmien",

            "por�wnaj"      : "porownaj",
            "poziomy"       : "poziomy",
            "pozostan�"     : "przetrwaja",
            "przetrwaj�"    : "przetrwaja",

            "stan"          : "stan",

            "um"            : "umiejetnosci",
            "umiej�tno�ci"  : "umiejetnosci",
            ]);
}

/*
 * Function name: using_soul
 * Description:   Called once by the living object using this soul. Adds
 *		  sublocations responsible for extra descriptions of the
 *		  living object.
 */
public void
using_soul(object live)
{
    live->add_subloc(SUBLOC_MISCEXTRADESC, file_name(this_object()));
    live->add_textgiver(file_name(this_object()));
}

public string
show_subloc_size(object on, object for_obj)
{
    if (query_interactive(on))
    {
        string race, ret = " ";
        int gender, val, rval, *proc;

        race = on->query_race();
        gender = on->query_gender();

        if (sizeof(on->query_cecha_szczegolna()))
            ret += on->query_cecha_szczegolna()[2] + ", ";

        if (member_array(race, RACES) >= 0)
        {
            val = on->query_prop(CONT_I_HEIGHT);
            rval = RACEATTR[race][gender][0];
            val = 100 * val / (rval ? rval : val);
            proc = HEIGHTPROC;

            rval = sizeof(proc) - 1;
            while (--rval >= 0)
            {
                if (val >= proc[rval])
                break;
            }

            if (rval < 0)
                rval = 0;

//            ret += HEIGHTDESC(on->koncowka("im", "^a"))[rval] + " i ";
            ret += HEIGHTDESC(on->koncowka("im", "^a"))[rval];
            if(!on->query_cecha_szczegolna())
                ret+=", ";
            else
                ret+=" i ";
        }

        if (on->query_budowa_ciala() && on->query_kolor_oczu())
        {
            if(on->query_prop(EYES_CLOSED))
                ret += on->query_budowa_ciala()[2] + " " + on->query_rasa(PL_NAR) +
                ".\n";
            else
                ret += on->query_budowa_ciala()[2] + " " + on->query_rasa(PL_NAR) +
                    " o " + on->query_kolor_oczu()[2] + " oczach. ";

            //WTF?! Takich rzeczy nie robi si� tutaj! god damnit.
            //Wywalam. Vera.
            /*
            //Samaia jest w ciazyyy
            if (on->query_name(0) == "Samaia")
	    {
                if (on == for_obj)
                    ret += "Ponadto wygl^ada na to, ^ze jeste^s w odmiennym stanie.";
                else
                    ret += "Ponadto wygl^ada na to, ^ze jest w odmiennym stanie.";
            }
            */
            //Zamieniam na:
            if(sizeof(on->query_spec_opis()))
            {
                if (on == for_obj)
                    ret += on->query_spec_opis()[0];
                else
                    ret += on->query_spec_opis()[1];
            }

            ret += "\n";
        }
        else
        {
            ret += HEIGHTDESC(on->koncowka("i", "a"))[rval] + " i "+
                WIDTHDESC(on->koncowka("y", "a"))[rval] + " jak na " +
                on->query_rasa(PL_BIE) + ".\n";
        }

        return ret;
    }
    else
    {
        string race, res, konc1, konc2;
        int val, rval, *proc, gender;

    race = on->query_race();
    gender = on->query_gender();
	konc1 = on->koncowka("y", "a");
	konc2 = (konc1 == "y" ? "i" : "a");

	if (member_array(race, RACES) >= 0)
	{
	    val = on->query_prop(CONT_I_HEIGHT);
	    rval = RACEATTR[race][gender][0];
	    val = 100 * val / (rval ? rval : val);
	    proc = HEIGHTPROC;

	    rval = sizeof(proc) - 1;
	    while (--rval >= 0)
	    {
		if (val >= proc[rval])
		    break;
	    }

	    if (rval < 0)
		rval = 0;

	    res = " " + HEIGHTDESC(konc2)[rval] + " i ";

	    val = on->query_prop(CONT_I_WEIGHT) / on->query_prop(CONT_I_HEIGHT);
	    rval = RACEATTR[race][gender][2];
	    val = 100 * val / (rval ? rval : val);
	    proc = WEIGHTPROC;

	    rval = sizeof(proc) - 1;
	    while (--rval >= 0)
	    {
		if (val >= proc[rval])
		    break;
	    }

	    if (rval < 0)
		rval = 0;

	    res += WIDTHDESC(konc1)[rval] + " jak na " +
		on->query_rasa(PL_BIE) + ".\n";
	}
        else
	    res = "";

	return res;
    }
}

public string
show_subloc_fights(object on, object for_obj)
{
    object eob;

    eob = (object)on->query_attack();

    return (for_obj == on ? "Walczysz" : "Walczy") +
	" z " + (eob == for_obj ? "tob�" :
	 (string)eob->query_imie(for_obj, PL_NAR)) + ".\n";
}

///FIXME: Doda� priorytety obszar�w.
public string
show_subloc_health(object on, object for_obj)
{
    int hp, mhp;

    hp = ftoi(on->query_hp());
    mhp = ftoi(on->query_max_hp());

    if(mhp == 0)
        mhp = 1;

    return get_proc_text((hp * 100) / mhp,
        (on == this_player() ? health_state[this_player()->koncowka(0, 1, 2)] :
        health_state[on->koncowka(0, 1, 2)]), 0, ({}), 1);
}

/*
 * Function name: show_subloc
 * Description:   Shows the specific sublocation description for a living
 */
public string
show_subloc(string subloc, object on, object for_obj)
{
    string res, cap_pronoun, cap_pronoun_verb, tmp;

    if (on->query_prop(TEMP_SUBLOC_SHOW_ONLY_THINGS))
	return "";

    if (for_obj == on)
    {
	res = "Jeste�";
	cap_pronoun_verb = res;
	cap_pronoun = "Jeste� ";
    }
    else
    {
	res = "Jest";
	cap_pronoun_verb = res;
	cap_pronoun = "Zdaje si� by� ";
    }

    if (strlen(tmp = show_subloc_size(on, for_obj)))
	res += tmp;
    else
	res = "";

    if (on->query_attack())
	res += show_subloc_fights(on, for_obj);

    res += cap_pronoun + show_subloc_health(on, for_obj) + ".\n";

    return res;
}

/****************************************************************
 *
 * A textgiver must supply these functions
 */

/*
 * Returns true if this textgiver describes the given stat
 */
public int
desc_stat(int stat)
{
    return (stat >= 0 && stat <= SS_NO_STATS);
}

/*
 * Returns true if this textgiver describes the given skill
 */
public int
desc_skill(int skill)
{
    if (sizeof(skillmap[skill]))
	return 1;
    else
	return 0;
}

/*
 * Function name: query_stat_string
 * Description:   Gives text information corresponding to a stat
 * Parameters:    stat: index in stat array
 *                value: stat value (range 1..135)
 *                negative value gives the statstring with index corresponding
 *                to -value
 * Returns:       string corresponding to stat/value
 */
public string
query_stat_string(int stat, int value, int konc = 0)
{
    if (stat < 0 || stat >= SS_NO_STATS)
        return "";
    
    if (value < 0)
    {
        if (value < -sizeof(stat_strings[stat]))
            value = 0;
        
        return stat_strings[konc][stat][-value];
    }
    
    if (value > F_MAX_STAT_I - 1)
        value = F_MAX_STAT_I - 1;
    
    
    
    return stat_strings[konc][stat][sizeof(stat_strings[konc][stat]) * value / F_MAX_STAT_I];
}

public varargs string
get_proc_text(int num, mixed maindesc, int turnpoint, mixed subdesc, int kierunek=0)
{
    int a, b, c, d,j;
    mixed subs;

    if (!sizeof(maindesc))
        return ">normal<"; //FIXME

    num = max(0, min(99, num));
    setuid();
    seteuid(getuid());
    j = sizeof(maindesc) * num / 100;

    if (!pointerp(subdesc))
	subs = denom_strings;
    else if (sizeof(subdesc))
	subs = subdesc;
    else
    {
        if(kierunek--)
            return KOLORUJ_POZIOM3(maindesc[j], ++j, sizeof(maindesc), kierunek);
        else
            return maindesc[j];
    }

    a = num - (j * 100 / sizeof(maindesc));

    b = (sizeof(subs) * a * sizeof(maindesc)) / 100;

    int poz = j+1 * b+1 + 1;
    if (j < turnpoint)
    {
        poz--;
        b = (sizeof(subs) - 1) - b;
    }

    if (b >= sizeof(subs))
        b = sizeof(subs)-1;


    if(kierunek--)
        return KOLORUJ_POZIOM3(subs[b] + maindesc[j], poz, (sizeof(maindesc)*sizeof(subs)), kierunek);
    else
        return subs[b] + maindesc[j];
}

/* **************************************************************************
 * Here follows some support functions.
 * **************************************************************************/

/*
 * Function name: compare_living
 * Description:   Support function to compare
 */
mixed
compare_living(int stat, object player1, object player2)
{
    int a, b, c, skill, seed1, seed2, swap;
    string konc;

    a  = player1->query_stat(stat);
    b  = player2->query_stat(stat);

    if (player1 != player2 )
    {
	skill = this_player()->query_skill(SS_APPR_MON);
	sscanf(OB_NUM(player1), "%d", seed1);
	sscanf(OB_NUM(player2), "%d", seed2);
	skill = 1000 / (skill + 1);
#if 0
	a += random(skill, seed1 + seed2 + 27 + stat);
	/* 27 is an arbitrarily selected constant */
	b += random(skill, seed1 + seed2 + stat);
#endif
	a += random(skill, seed1 + stat);
	b += random(skill, seed2 + stat);
    }

    if (a > b)
    {
	c = 100 - (80 * b) / a;
	konc = player1->koncowka(0, 1, 2);
    }
    else
    {
	c = 100 - (80 * a) / b;
	swap = 1;
	konc = player2->koncowka(0, 1, 2);
    }

    c = (c * sizeof(compare_strings[konc][stat]) / 100);
    if (c > 3)
	c = 3;
    return ({swap, compare_strings[konc][stat][c]});
}

/* **************************************************************************
 * Here follows the actual functions. Please add new functions in the
 * same order as in the function name list.
 * **************************************************************************/

/*
 * cechy - Pokazuje cechy gracza.
 */
varargs int
cechy(string str)
{
    int a, i, j, konc = this_player()->koncowka(0, 1);
    float c, d;
    object ob;
    string s, s2, t;
    int *learn;

    if (!str)
    {
        ob = this_player();
        s = "Jeste� ";

        a = ob->query_average_stat();
        j = ob->query_exp_stat_delta();
        if (j > 0)
        {
            if (j > SD_IMPROVE_MAX)
                j = SD_IMPROVE_MAX;
            else if (a < SD_IMPROVE_MIN)
                j = SD_IMPROVE_MIN;

            if (a > 99)
                a = 99;
            if (!a)
                a = 1;

            j /= a * 10;
            write("Poczyni�" + (konc ? "a�" : "e�") +
                    " " + get_proc_text(j, improve_fact, 0, ({ }) ) +
                    " post�py, od momentu kiedy " +
                    (konc ? "wesz�a�" : "wszed�e�") + " do gry.\n");
        }
        else
        {
            write("Nie poczyni�" + (konc ? "a�" : "e�") +
                    " �adnych post�p�w, od kiedy " +
                    (konc ? "wesz�a�" : "wszed�e�") + " do gry.\n");
        }
    }
    else
    {
        if(!((ob = find_player(str)) && this_player()->query_wiz_level()))
        {
            notify_fail("Ciekawscy jeste�my.\n");
            return 0;
        }
        s = capitalize(str) + " jest ";
    }

    for (t = s, i = 0; i < SS_NO_STATS; i++)
    {
        s2 = query_stat_string(i, ob->query_stat(i), konc);

        if (i < SS_NO_STATS - 2)
            s2 += ", ";
        else if (i < SS_NO_STATS - 1)
            s2 += " i ";
        else
            s2 += ".";
        t += s2;
    }
    write(t + "\n");

    return 1;
}

/**
 * email - Pokaz swojego emaila.
 */
int
email(string str)
{
	write("Tw�j aktualny adres email, a zarazem nazwa twojego konta, wygl�da nast�puj�co:\n" +
	    "\t" + this_player()->query_mailaddr() + "\n");
	return 1;
}

// #define NAZWA_KALENDARZA(x)     ((x == MT_KALENDARZ_LUDZKI) ? "ludzki" :                       \
//                                 ((x == MT_KALENDARZ_ELFI) ? "elfi" : "elfi i ludzki"))
#define NAZWA_KALENDARZA(x)     ((x == MT_KALENDARZ_LUDZKI) ? "ludzkiego" :                    \
                                ((x == MT_KALENDARZ_ELFI) ? "elfiego" : "elfiego i ludzkiego"))

/**
 * kalendarz - wy�wietla jaki kalendarz mamy wybrany oraz pozwala wybra� kalendarz
 */
int kalendarz(string str)
{
    if(!TP->query_prop(PLAYER_I_KALENDARZ))
        TP->set_default_kalendarz();

    if(!str)
        return !write("U�ywasz kalendarza " + NAZWA_KALENDARZA(TP->query_prop(PLAYER_I_KALENDARZ)) + ".\n");

    NF("Sk�adnia: kalendarz [ludzki|elfi|ludzki i elfi].\n");

    if(str ~= "ludzki")
    {
        write("Kalendarz zmieniony na ludzki.\n");
        TP->change_prop(PLAYER_I_KALENDARZ, MT_KALENDARZ_LUDZKI);
    }
    else if(str ~= "elfi")
    {
        write("Kalendarz zmieniony na elfi.\n");
        TP->change_prop(PLAYER_I_KALENDARZ, MT_KALENDARZ_ELFI);
    }
    else if(str ~= "elfi i ludzki" || str ~= "ludzki i elfi")
    {
        write("Od teraz u�ywasz obu kalendarzy ludzkiego i elfiego.\n");
        TP->change_prop(PLAYER_I_KALENDARZ, MT_KALENDARZ_ELFI|MT_KALENDARZ_LUDZKI);
    }
    else
        return 0;

    return 1;
}

/**
 * kondycja - Wyswietla kondycje wlasna lub innej osoby.
 */
int
kondycja(string str)
{
    object enemy;
    object *oblist;
    int index;
    int size;
    int display_self = 0;

    switch (str ?: "")
    {
	case "":
	    display_self = 1;
	    oblist = ({});
	    break;

	case "przeciwnika":
	    if (!(enemy = this_player()->query_attack()))
	    {
		notify_fail("Z nikim nie walczysz.\n");
		return 0;
	    }
	    else if (!CAN_SEE_IN_ROOM(this_player()) ||
		     !CAN_SEE(this_player(), enemy))
	    {
		notify_fail("Nie widz�c swojego przeciwnika nie mo�esz "
		          + "oceni� jego kondycji.\n");
		return 0;
	    }

	    oblist = ({enemy});
	    break;

	case "dru�yny":
	    if (!sizeof(oblist = this_player()->query_team_others()))
		write("Nie jeste� w �adnej dru�ynie.\n");
	    else if (!CAN_SEE_IN_ROOM(this_player()))
	    {
		write("Nie widzisz przy sobie �adnego z cz�onk�w swojej "
		    + "dru�yny.\n");
		oblist = ({});
	    }
	    else if (!sizeof(oblist =
		FILTER_CAN_SEE(FILTER_PRESENT_LIVE(oblist), this_player())))
		write("Nie widzisz przy sobie �adnego z cz�onkow swojej "
		    + "dru�yny.\n");

	    display_self = 1;
	    break;

	case "wszystkich":
	    display_self = 1;
	    /* Intentionally no "break". We need to catch "default" too. */

	default:
	    oblist = parse_this(str, "%l:" + PL_DOP);
	    if (!sizeof(oblist) && !display_self)
	    {
		notify_fail("Czyj� kondycj� chcesz pozna�?\n");
		return 0;
	    }
    }

    if (display_self)
	write("Jeste� " + show_subloc_health(this_player(), this_player())
	    + ".\n");

    index = -1;
    size = sizeof(oblist);

    while(++index < size)
	write(oblist[index]->query_Imie(this_player(), PL_MIA) + " jest "
	    + show_subloc_health(oblist[index], this_player()) + ".\n");

    return 1;
}

/**
 * opcje - Wlacz / wylacz / obejrzyj jakas opcje
 */
public nomask varargs int
opcje(string arg, int fl=0)
{
    string  rest, title, val;
    mixed   tmp;
    int     wi, plec, opt, ed, op;

    plec = ((this_player()->query_gender() != G_MALE) ? 1 : 0);

    if (!stringp(arg))
    {
        opcje("wysoko�� ekranu", -1);
        opcje("szeroko�� ekranu", -1);
        opcje("kr�tkie opisy", -1);
        opcje("echo", -1);
//         opcje("uciekaj przy", -1);
        opcje("przyjmowanie przedmiot�w", -1);
//         opcje("opisuj walki", -1);
        opcje("mccp2", -1);
        //opcje("msp", -1);
        opcje("kodowanie", -1);
        opcje("kolorowanie", -1);
        opcje("og^luszanie", -1);
        opcje("ukrywanie w kto", -1);
        return 1;
    }

    arg = lower_case(arg);

    switch(arg)
    {
        case "wysoko��":
        case "wysoko�� ekranu":
            title   = "Wysoko�� ekranu";
            val     = itoa(TP->query_option(OPT_MORE_LEN));
            break;

        case "szeroko��":
        case "szeroko�� ekranu":
            int sz = TP->query_option(OPT_SCREEN_WIDTH);
            title   = "Szeroko�� ekranu";
            val     = (!sz ? "Brak ograniczenia" : itoa(this_player()->query_option(OPT_SCREEN_WIDTH)));
            break;

        case "kr�tkie":
        case "kr�tkie opisy":
        case "kr�tkie opisy lokacji":
            title   = "Kr�tkie opisy lokacji";
            val     = (this_player()->query_option(OPT_BRIEF) ? "W��czone" :
                "Wy��czone");
            break;

        case "echo":
        case "echo komend":
            title   = "Echo komend";
            val     = (this_player()->query_option(OPT_ECHO) ? "W��czone" :
                "Wy��czone");
            break;

        case "przyjmowanie":
        case "przyjmowanie przedmiot�w":
        case "przyjmowanie przedmiot�w od graczy":
            title   = "Przyjmowanie przedmiot�w";
            val     = (this_player()->query_option(OPT_RECEIVING) ? "W��czone" :
                "Wy��czone");
            break;

        case "mccp2":
        case "mccp":
        case "kompresja":
        case "kompresja mccp":
        case "kompresja mccp2":
        case "u�ycie kompresjii mccp":
        case "u�ycie kompresjii mccp2":
        case "u�ycie mccp2":
        case "u�ycie mccp":
            title   = "U�ycie kompresji MCCP2";
            val     = (this_player()->query_option(OPT_MCCP2) ?
                ("Auto " + (efun::query_mccp2(this_player()) ?
                "(W��czone)" : "(Wy��czone)")) : "Wy��czone");
            break;
/*
        case "msp":
        case "d�wi�k":
        case "protok� d�wi�ku msp":
        case "u�ycie protoko�u d�wi�ku msp":
            title   = "U�ycie protoko�u d�wi�ku MSP";
            val     = (this_player()->query_option(OPT_MSP) ?
                ("Auto " + (efun::query_msp(this_player()) ?
                "(W��czone)" : "(Wy��czone)")) : "Wy��czone");
            break;*/

        case "czcionka":
        case "pliterki":
        case "kodowanie":
            switch (this_player()->query_option(OPT_KODOWANIE))
            {
                case 1:     val = "iso-8859-2";     break;
                case 2:
                case 3:     val = "cp1250";         break;
                case 4:     val = "utf-8";          break;
                default:    val = "brak";
            }
            title   = "Kodowanie";
            break;

        case "kolorowanie":
        case "kolorki":
        case "kolory":
        case "ansi":
            title   = "Kolorowanie";
            val     = (TP->query_option(OPT_COLORS) ? "W��czone" : "Wy��czone");
            break;

        case "ukrywanie":
        case "ukrywanie w kto":
        case "ukrywanie obecno�ci":
        case "ukrywanie obecno�ci w kto":
        case "kto":
            title   = "Ukrywanie obecno�ci";
            val     = (TP->query_option(OPT_HIDE_IN_WHO) ? "W��czone" : "Wy��czone");
            break;
    }

    if(title && val)
    {
        if(fl == -1)
            write("==>  "+ sprintf("%30-s: %49-s\n", title, val + ","));
        else
            write("==>  "+ title + ": " + val + "\n");
        return 1;
    }

    if(sscanf(arg, "wysoko�� ekranu %d", tmp) == 1 || sscanf(arg, "wysoko�� %d", tmp) == 1)
    {
        if (!this_player()->set_option(OPT_MORE_LEN, tmp))
            return notify_fail("Wysoko�� ekranu powinna mie�ci� si� w przedziale 1 - 150.\n");
        opcje("wysoko�� ekranu");
    }
    else if(sscanf(arg, "szeroko�� ekranu %s", tmp) == 1 || sscanf(arg, "szeroko�� %s", tmp) == 1)
    {
        if(tmp == "brak")
            tmp = 0;
        else
            tmp = atoi(tmp);
        if (!this_player()->set_option(OPT_SCREEN_WIDTH, tmp))
            return notify_fail("Szeroko�� ekranu powinna mie�ci� si� w przedziale 10 - 200 " +
                "lub 0 aby wy��czy� ci�cie tekstu.\n");
        opcje("szeroko�� ekranu");
    }
    else if(sscanf(arg, "kr�tkie opisy lokacji %s", tmp) == 1 || sscanf(arg, "kr�tkie opisy %s", tmp) == 1 ||
        sscanf(arg, "kr�tkie %s", tmp) == 1)
    {
        if (tmp ~= "w��cz" || tmp == "+")
            this_player()->set_option(OPT_BRIEF, 1);
        else if (tmp ~= "wy��cz" || tmp == "-")
            this_player()->set_option(OPT_BRIEF, 0);
        else
            return NF("Nie rozumiem. Mam w��czy� czy wy��czy�?\n");

        opcje("kr�tkie opisy");
    }
    else if(sscanf(arg, "echo komend %s", tmp) == 1 || sscanf(arg, "echo %s", tmp) == 1)
    {
        if (tmp ~= "w��cz" || tmp == "+")
            this_player()->set_option(OPT_ECHO, 1);
        else if (tmp ~= "wy��cz" || tmp == "-")
            this_player()->set_option(OPT_ECHO, 0);
        else
            return NF("Nie rozumiem. Mam w��czy� czy wy��czy� ?\n");

        opcje("echo");
    }
    else if(sscanf(arg, "przyjmowanie przedmiot�w od graczy %s", tmp) == 1 ||
        sscanf(arg, "przyjmowanie przedmiot�w %s", tmp) == 1 ||
        sscanf(arg, "przyjmowanie %s", tmp) == 1)
    {
        if (tmp ~= "w��cz" || tmp == "+")
            this_player()->set_option(OPT_RECEIVING, 1);
        else if (tmp ~= "wy��cz" || tmp == "-")
            this_player()->set_option(OPT_RECEIVING, 0);
        else
            return NF("Nie rozumiem. Mam w��czy� czy wy��czy� ?\n");

        opcje("przyjmowanie przedmiot�w");
    }
    else if(sscanf(arg, "u�ycie kompresji mccp2 %s", tmp) == 1 ||
        sscanf(arg, "u�ycie kompresji mccp %s", tmp) == 1 ||
        sscanf(arg, "u�ycie mccp2 %s", tmp) == 1 ||
        sscanf(arg, "u�ucie mccp %s", tmp) == 1 ||
        sscanf(arg, "kompresja mccp2 %s", tmp) == 1 ||
        sscanf(arg, "kompresja mccp %s", tmp) == 1 ||
        sscanf(arg, "kompresja %s", tmp) == 1 ||
        sscanf(arg, "mccp2 %s", tmp) == 1 ||
        sscanf(arg, "mccp %s", tmp) == 1)
    {
        if (tmp ~= "auto" || tmp == "+")
            this_player()->set_option(OPT_MCCP2, 1);
        else if (tmp ~= "wy��cz" || tmp == "-")
            this_player()->set_option(OPT_MCCP2, 0);
        else
            return NF("Nie rozumiem. Mam w��czy� (auto) czy wy��czy�?\n");

        opcje("mccp2");
    }/*
    else if(sscanf(arg, "u�ycie protoko�u d�wi�ku msp %s", tmp) == 1 ||
        sscanf(arg, "u�ycie protoko�u d�wi�ku %s", tmp) == 1 ||
        sscanf(arg, "u�ycie protoko�u msp %s", tmp) == 1 ||
        sscanf(arg, "protok� d�wi�ku msp %s", tmp) == 1 ||
        sscanf(arg, "protok� d�wi�ku %s", tmp) == 1 ||
        sscanf(arg, "d�wi�k %s", tmp) == 1 ||
        sscanf(arg, "msp %s", tmp) == 1)
    {
        if (tmp ~= "auto" || tmp == "+")
            this_player()->set_option(OPT_MSP, 1);
        else if (tmp ~= "wy��cz" || tmp == "-")
            this_player()->set_option(OPT_MSP, 0);
        else
            return NF("Nie rozumiem. Mam w��czy� (auto) czy wy��czy�?\n");

        opcje("msp");
    }*/
    else if(sscanf(arg, "kodowanie %s", tmp) == 1 ||
        sscanf(arg, "czcionka %s", tmp) == 1)
    {
		int kodowanie;
		
        switch(tmp)
        {
            case "brak":
				kodowanie = 0;
                break;
            case "iso-8859-2":
            case "iso8859-2":
            case "iso88592":
			case "iso":
				kodowanie = 1;
                break;
            case "cp1250":
			case "windows":
			case "cp":
				kodowanie = 2;
                break;
            case "utf-8":
            case "utf8":
			case "utf":
				kodowanie = 4;
                break;
            default:
                return notify_fail("B��d sk�adniowy: " +
                    "Kodowanie przyjmuje jedn� z czterech warto�ci: brak, iso-8859-2, cp1250, utf-8.\n");
        }
        
        TP->set_option(OPT_KODOWANIE, kodowanie);
        
        opcje("kodowanie");
    }
    else if(sscanf(arg, "kolorowanie %s", tmp) == 1 ||
        sscanf(arg, "kolorki %s", tmp) == 1 ||
        sscanf(arg, "kolory %s", tmp) == 1 ||
        sscanf(arg, "ansi %s", tmp) == 1)
    {
        if(tmp == "+" || tmp ~= "w��cz")
            TP->set_option(OPT_COLORS, 1);
        else if(tmp == "-" || tmp ~= "wy��cz")
            TP->set_option(OPT_COLORS, 0);
        else
            return NF("Nie rozumiem. Mam w��czy� czy wy��czy� kolorowanie?\n");

        opcje("kolorowanie");
    }
    else if(sscanf(arg, "ukrywanie obecno�ci w kto %s", tmp) == 1 ||
        sscanf(arg, "ukrywanie w kto %s", tmp) == 1 ||
        sscanf(arg, "ukrywanie %s", tmp) == 1 ||
        sscanf(arg, "kto %s", tmp) == 1)
    {
        if(tmp == "+" || tmp ~= "w��cz")
            TP->set_option(OPT_HIDE_IN_WHO, 1);
        else if(tmp == "-" || tmp ~= "wy��cz")
            TP->set_option(OPT_HIDE_IN_WHO, 0);
        else
            return NF("Nie rozumiem. Mam w��czy� czy wy��czy�?\n");

        opcje("ukrywanie");
    }
    else
        return NF("Nie ma takiej opcji.\n");

    return 1;
}


/**
 * odmien - Sprawdz odmiane jakiegos obiektu / jakiegos gracza.
 */
nomask int
odmien(string str)
{
    object *cele = parse_this(str, "%i:" + PL_MIA);
    object ob;
    string *odmiana = ({});
    int i = -1;

    if (!sizeof(cele))
    {
	    if (str == this_player()->query_real_name())
	        cele = ({ this_player() });
	    else
	    {
	        notify_fail("Odmie� <kto/co>?\n");
	        return 0;
	    }
    }
    else if (sizeof(cele) > 1)
    {
	    notify_fail("Nie mo�esz odmienia� jednocze�nie "
		    + COMPOSITE_DEAD(cele, PL_DOP) + "\n");
	    return 0;
    }

    ob = cele[0];

    if (!living(ob))
	    while (++i <= PL_MIE)
	        odmiana += ({ ob->query_nazwa(i) });
    else if (this_player()->query_met(ob))
	    while (++i <= PL_MIE)
	        odmiana += ({ ob->query_met_name(i) });
    else
	    while (++i <= PL_MIE)
	        odmiana += ({ ob->query_nonmet_name(i) });

    write(capitalize(ob->short(this_player(), PL_MIA)) + " odmienia" +
	    (ob->query_tylko_mn() ? "ja" : "") + " si� nastepuj�co:\n" +
	    "  Mianownik: " + odmiana[0] + ",\n" +
	    " Dope�niacz: " + odmiana[1] + ",\n" +
	    "   Celownik: " + odmiana[2] + ",\n" +
	    "    Biernik: " + odmiana[3] + ",\n" +
	    "  Narz�dnik: " + odmiana[4] + ",\n" +
	    "Miejscownik: " + odmiana[5] + ".\n");

    return 1;
}

/*
 * Function name: porownaj
 * Description:   porownuje cechy dwoch istot zyjacych
 */
int
porownaj(string str)
{
    int i, me;
    string stat, name1, name2, *slowa;
    mixed p1, p2;
    object pl1, pl2, *ob_list;
    mixed *cstr;

    notify_fail("Prawid�owa sk�adnia:\n" +
    		"\t'por�wnaj <cech�> z <osob�>' lub te�\n" +
    		"\t'por�wnaj <cech�> <osoby I> i <osoby II>'\n");

    if (!str)
	    return 0;

    if (!CAN_SEE_IN_ROOM(this_player()))
    {
	    notify_fail("Ty przecie� nic nie widzisz!\n");
	    return 0;
    }

    slowa = explode(str, " ");
    if (sizeof(slowa) < 3)
	    return 0;

    ob_list = FILTER_LIVE(all_inventory(environment(this_player())));
    ob_list = FILTER_CAN_SEE(ob_list, this_player());

    stat = slowa[0];

    // Sprawdzamy czy mozna porownac taka ceche
    int max_try = sizeof(stat_names);

    for (i = 0;;i++)
    {
	    if (i >= max_try)
	    {
	        notify_fail("Nie ma takiej cechy. Masz do wyboru: " +
		    COMPOSITE_WORDS(stat_names_bie) + ".\n");
	        return 0;
	    }

	    if ((stat ~= lower_case(stat_names[i])) ||
	        (stat ~= stat_names_bie[i]))
	        break;
    }

    if (i == SS_INT || i == SS_DIS)
    {
        write("Wiesz, to troch� ci�ko okre�li� na pierwszy rzut "+
            "oka.\n");
        return 1;
    }

    if (slowa[1] == "z" || slowa[1] == "ze")
    {
	str = implode(slowa[2..], " ");

	if (!parse_command(str, ob_list, "%l:" + PL_NAR, p2))
	    return 0;
	p2 = NORMAL_ACCESS(p2, 0, 0);

	if (!sizeof(p2))
	    return 0;

	if (sizeof(p2) > 1)
	{
	    notify_fail("Nie mo�esz por�wnywa� cech z wi�cej, ni� jedn� "+
		"osob� na raz.\n");
	    return 0;
	}

	this_player()->set_obiekty_zaimkow(p2);
	p2 = p2[0];
	p1 = this_player();
    }
    else
    {
	str = implode(slowa[1..], " ");

	if (!parse_command(str, ob_list, "%l:" + PL_DOP + " 'i' %l:" + PL_DOP,
		p1, p2))
	    return 0;

	p1 = NORMAL_ACCESS(p1, 0, 0);
	p2 = NORMAL_ACCESS(p2, 0, 0);

	if (!sizeof(p2) || !sizeof(p1))
	    return 0;

	if ((sizeof(p1) > 1) || (sizeof(p2) > 1))
	{
	    notify_fail("Mo�esz por�wnywa� cechy tylko jednej osoby z " +
		"drug�.\n");
	    return 0;
	}

	if (p1 == p2)
	{
	    notify_fail("Chyba nie chcesz por�wnywa� cech tej samej osoby?\n");
	    return 0;
	}

	this_player()->set_obiekty_zaimkow(p1, p2);
	p1 = p1[0];
	p2 = p2[0];
    }

    cstr = compare_living(i, p1, p2);

/*
 * He he he, czadowo to napisalem, nie? Juz po 5 minutach nie wiem o
 * co chodzi :-)) TO jest przyklad, jak kod _nie_ powinien wygladac.
 * /Alvin
 */
    write((random(2) ? "Wydaje ci si�, �e " : "Masz wra�enie, �e ") +
	(p1 == this_player() ? (!cstr[0] ? "jeste� " : "jest ") :
	(!cstr[0] ? p1->query_imie(this_player(), PL_MIA) :
	p2->query_imie(this_player(), PL_MIA)) + " jest ") +
	cstr[1] + " " + (cstr[0] ? (p1 == this_player() ? "ty" :
	p1->query_imie(this_player(), PL_MIA)) : p2->query_imie(this_player(),
	PL_MIA)) + ".\n");

    return 1;
}

/*
 * poziomy - Wypisuje poziomy roznych rzeczy
 */
public int
poziomy(string str)
{
    string *ix, *levs;

    if (!mappingp(lev_map))
        lev_map = SD_LEVEL_MAP;

    ix = m_indexes(lev_map);

    if (!str)
    {
	notify_fail("Dost�pne listy poziom�w:\n" +
		    break_string(COMPOSITE_WORDS(sort_array(ix)) + ".", 70, 3)
		  + "\n");
	return 0;
    }
    levs = lev_map[str];
    if (!sizeof(levs))
    {
	notify_fail("Nie ma takiej listy poziom�w. Oto dost�pne:\n" +
		    break_string(COMPOSITE_WORDS(sort_array(ix)) + ".", 70, 3)
		  + "\n");
	return 0;
    }
    write("Dost�pne poziomy " + str + ":\n" +
	  break_string(COMPOSITE_WORDS(levs) + ".", 70, 3) + "\n");
    return 1;
}


/*
 * przetrwaja - wywala obiekty, ktore nie pozostana przy graczu po zakonczeniu
 * funkcja ta zostala wywalona kiedys, za czasow gdy wszystko mialo przetrwac,
 * Stan ten sie zmienil, odkad Vera zostal keeperem i pod jego dyktatorskimi
 * rzadami zywot gracza zostal utrudniony ;p
 */
public int
przetrwaja(string str)
{
    if(str)
        write("Ta komenda nie przyjmuje �adnych argument�w.\n");

    object *inv = deep_inventory(TP);
    object *zagina = ({ });
    foreach(object x: inv)
        if(!stringp(x->query_auto_load()) &&
            !x->query_prop(OBJ_M_NO_DROP))
            zagina+=({x});

    if(sizeof(zagina))
        write("Oto co b�dziesz musia�"+TP->koncowka("","a","o")+
        " opu�ci� przed za�ni�ciem: "+COMPOSITE_DEAD(zagina,PL_BIE)+".\n");
    else
        write("Wszystko co posiadasz pozostanie przy tobie po "+
            "przebudzeniu.\n");

    return 1;
}

/**
 * stan - Podaje informacje o stanie gracza.
 */
varargs int
stan(string str)
{
    int a, b, i, j, konc;
    object ob;
    string s, s2, s3, s4, t;

    if(!str)
        ob = this_player();
    else
    	return 0;

    konc = ob->koncowka(0, 1);

    /* height, width, race
     */

    /* Hitpoints and mana
     */
    a = (ob->query_hp() * 100 / ob->query_max_hp());
    t = get_proc_text(a, health_state[konc], 0, ({}), 1);
    b = (ob->query_mana() * 100) / ob->query_max_mana();
    setuid();
    seteuid(getuid());
    write("Jeste� fizycznie " + t + ", za� mentalnie " +
        get_proc_text(b, mana_state[konc], 0, ({}), 1) + ".\n");

    /* panic
     */
    a = (10 + (int)ob->query_stat(SS_DIS) * 3);
    b = ob->query_panic();
    a = 100 * b / (a != 0 ? a : b);
    t = get_proc_text(a, panic_state[konc], 2, "", 2) + " i jeste� ";

    /* fatigue
     */
    a = ob->query_old_max_fatigue();
    b = a - ob->query_old_fatigue();
    a = 100 * b / (a != 0 ? a : b);
    t += get_proc_text(a, fatigue_state[konc], 0, ({}), 2) + ".";

    write("Czujesz si� " + t + "\n");

    /* soaked
     */
    a = ob->query_prop(LIVE_I_MAX_DRINK);
    b = ob->query_soaked();
    a = 100 * b / (a != 0 ? a : b);
    t = get_proc_text(a, soak_state, 0, ({}), 1) + " i ";

    /* stuffed
     */
    a = ob->query_prop(LIVE_I_MAX_EAT);
    b = ob->query_stuffed();
    a = 100 * b / (a != 0 ? a : b);
    t += "jeste� " + get_proc_text(a, stuff_state[konc], 1, "", 1) + ".";


    write(capitalize(t) + "\n");

    /* intox
     */
    a = ob->query_prop(LIVE_I_MAX_INTOX);
    b = ob->query_intoxicated();
    a = 100 * b / (a != 0 ? a : b);
    if (b)
        write("Jeste� " + get_proc_text(a, intox_state[konc], 0, ({}), 1) + ".\n");
    else
    {
        /* headache
        */
        a = ob->query_prop(LIVE_I_MAX_INTOX);
        b = ob->query_headache();
        a = 100 * b / (a != 0 ? a : b);
        if (b)
        {
            if (catch(s2 = get_proc_text(a, head_state, 1, "", 1)))
                SECURITY->log_error("zero_headache.", "GPT: " +
                a + ", " + implode(head_state, ";") + ".\n");
            {
                write("Jeste� trze�w" + (konc ? "a" : "y") +
                    ", ale masz " + s2 + " kaca.\n");
            }
        }
        else
            write("Jeste� trze�w" + (konc ? "a" : "y") + ".\n");
    }

#if 0
    /*
     * Alignment
     */
    write(s + ob->query_align_text() + ".\n");
#endif

    /*
     * Carry
     */
    a = ob->query_encumberance_weight();
    if (a >= 20)
    {
        write(capitalize(get_proc_text((a - 20) * 100 / 75, enc_weight, 0,
            ({ }) )) + ".\n");
    }

    /*
     * Age
     */
    write("Wiek: " + CONVTIME(ob->query_age() * 2) + ".\n");

    return 1;
}

/**
 * Funkcja prywatna sortuj�ca po rutynie a w drugiej kolejno�ci po wielko�ci uma.
 */
private static int
sort_by_rutyna(object o, int t, int x, int y)
{
    int sk1, sk2;
    if(t)
    {
        sk1 = o->query_tskill(x);
        sk2 = o->query_tskill(y);
    }
    else
    {
        sk1 = o->query_skill(x);
        sk2 = o->query_skill(y);
    }

    return (o->query_rutyna(x) > o->query_rutyna(y) ? -10 : (o->query_rutyna(x) < o->query_rutyna(y) ? 10 : 0)) +
        (sk1 > sk2 ? -1 : (sk1 < sk2 ? 1 : 0));
}

/*
 * [um]iejetnosci - Zwraca informacje o umiejetnosciach gracza.
 */
varargs int
umiejetnosci(string str)
{
    int il, wr, i1, i2, num, a, j, tip, konc = this_player()->koncowka(0, 1);
    object ob;
    int *sk, ski;
    mapping skdesc;
    string s, s1, s2 = "";

    wr = 1;
    skdesc = SS_SKILL_DESC;

    if (!stringp(str))
        ob = this_player();
    else if(TP->query_wiz_level())
    {
        if (sscanf(str, "%s %s", s1, s2) != 2)
        {
            if (find_player(str))
            {
                s1 = str;
                s2 = "";
            }
            else
            {
                s1 = this_player()->query_real_name();
                s2 = str;
            }
        }

        if (!(ob = find_player(s1)))
            return notify_fail("Nie ma takiego gracza.\n");

        if (!this_player()->query_wiz_level() && ob != this_player())
            return notify_fail("Ciekawscy jeste�my.\n");
    }

    a = ob->query_sum_skill();
    j = ob->query_exp_skill_delta();

    if (j > 0)
    {
        if (j > SD_IMPROVE_MAX)
            j = SD_IMPROVE_MAX;
        else if (a < SD_IMPROVE_MIN)
            j = SD_IMPROVE_MIN;

        a /= 6;
        if (a > 99)
            a = 99;
        if (!a)
            a = 1;

        j /= a * 10;
        write("Poczyni�" + (konc ? "a�" : "e�") + " " + get_proc_text(j, improve_fact, 0, ({ }) ) +
            " post�py, od momentu kiedy " + (konc ? "wesz�a�" : "wszed�e�") + " do gry.\n");
    }
    else
    {
        write("Nie poczyni�" + (konc ? "a�" : "e�") + " �adnych post�p�w, od kiedy " +
            (konc ? "wesz�a�" : "wszed�e�") + " do gry.\n");
    }

    i1 = 0;
    i2 = 9999999;

    int only_teory = 0;
    string jakich;

    switch (LC(s2))
    {
        case "bojowe":          jakich = "bojowych";        i1 = 10;    i2 = 99;    tip = 1;    break;
        case "rzemie�lnicze":   jakich = "rzemie�lniczych"; i1 = 100;   i2 = 199;   tip = 1;    break;
        case "z�odziejskie":    jakich = "z�odziejskich";   i1 = 200;   i2 = 249;   tip = 1;    break;
//         case "magiczne":        jakich = "magicznych";      i1 = 250;   i2 = 299;   tip = 1;    break;
        case "j�zykowe":        jakich = "j�zykowych";      i1 = 300;   i2 = 349;   tip = 1;    break;
        case "og�lne":          jakich = "og�lnych";        i1 = 350;   i2 = 500;   tip = 1;    break;
        case "teoretyczne":     jakich = "teoretycznych";   only_teory = 1;                     break;
        case "":
        case "praktyczne":      jakich = "praktycznych";                                        break;
        default:
            notify_fail("Nieznana kategoria. Masz do wyboru: og�lne, " +
                "bojowe, j�zykowe, z�odziejskie, rzemie�lnicze lub teoretyczne.\n");
        return 0;
    }

    if(tip)
        sk = ob->query_all_skill_types();
    else if(only_teory)
        sk = ob->query_teo_skill_types();
    else
        sk = ob->query_prt_skill_types();

    SKILL_LIBRARY->sk_init();

    sk = sort_array(sk, &sort_by_rutyna(TP,(!!only_teory)));

    for (il = 0; il < sizeof(sk); il++)
    {
        ski = sk[il];

        if (ski < i1 || ski > i2)
            continue;

        int teoria, praktyka;
        string steoria, spraktyka;

        //Teraz pobieramy warto�ci teoretyczne um�w
        if(only_teory || tip)
        {
            if (!(teoria = ob->query_tskill(sk[il])))
            {
                ob->remove_tskill(sk[il]);
                steoria = "w og�le";
            }
            else
                steoria = SKILL_LIBRARY->sk_rank(teoria);
        }
        //A teraz praktyczne
        if(!only_teory)
        {
            if (!(praktyka = ob->query_skill(sk[il])))
            {
                ob->remove_skill(sk[il]);
                spraktyka = "w og�le";
            }
            else
                spraktyka = SKILL_LIBRARY->sk_rank(praktyka);
        }

        if(!teoria && !praktyka)
            continue;

        if (pointerp(skdesc[sk[il]]))
            str = skdesc[sk[il]][0];
        else
        {
            str = ob->query_skill_name(sk[il]);
            if (!strlen(str))
                continue;
        }

        //A teraz musimy to odpowiednio wy�wietli�.
        if(tip)
        {   //Teraz pokazujemy jeden um w linijce - teorie i praktyke
            write(UC(str) + ": praktyka - " + spraktyka + ", za� teoria - " + steoria + ".\n");
        }
        else
        {   // A teraz dwa umy w linijce. Sama teoria lub praktyka.
            if (++wr % 2)
                write(sprintf("%-18s %s\n", str + ":", (only_teory ? steoria : spraktyka)));
            else
                write(sprintf("%-40s",sprintf("%-18s %s ", str + ":", (only_teory ? steoria : spraktyka))));
        }
    }
    if (wr)
        write("\n");
    else
    {
        //A tu musimy mu jeszcze powiedzie� jakich umiej�tno�ci jesli co� wybra�.
        write("Nie posiadasz �adnych umiej�tno�ci " + jakich + ".\n");
    }

    return 1;
}
