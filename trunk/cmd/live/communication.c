/**
 * \file /cmd/live/communication.c
 *
 * Soul docelowo ma zawiera� wszelkie komendy s�u��ce do bezpo�redniego
 * komunikowania si� z innym graczem.
 *
 * Autor: Krun.
 *
 * TODO:
 * - mo�na by wprowadzi� free emota do krzyczenia i szeptania:)
 */
inherit "/cmd/std/command_driver";

#include <pl.h>
#include <colors.h>
#include <macros.h>
#include <speech.h>
#include <options.h>
#include <cmdparse.h>
#include <filter_funs.h>
#include <stdproperties.h>

//Definicje:
#define CHECK_MOUTH_BLOCKED; \
            string Prop;     \
            if (Prop = TP->query_prop(LIVE_M_MOUTH_BLOCKED)) { \
            notify_fail(stringp(Prop) ? Prop : \
                "Nie jeste� w stanie wydoby� z siebie �adnego d�wi�ku.\n"); \
            return 0; }


//Predeklaracje funkcji:
private static string koloruj_mowe(string str, string str1, mixed i=0);

//Za��czamy plik �piewania
#include "/cmd/live/communication/singing.c"
//i m�wienia
#include "/cmd/live/communication/saying.c"

void
create()
{
    seteuid(getuid(this_object()));
}

/**
 * @return Zwraca nazwe soula.
 */
string
get_soul_id()
{
    return "komunikacja";
}

/**
 * @return zawsze 1 bo to soul.
 */
int
query_cmd_soul()
{
    return 1;
}

/**
 * @return Liste komend zawartych w tym soulu.
 */
mapping
query_cmdlist()
{
    return ([
        "za�piewaj"     : "czy_spiewaj",
        "powiedz"       : "communicate",
        "krzyknij"      : "krzyknij",
        "szepnij"       : "szepnij"]);
}

private static string
koloruj_mowe(string str, string str2, mixed i=0)
{
    if(intp(i) && i == 1)
        return xml_to_ansi(set_color(COLOR_FG_GREEN) + set_color(COLOR_BOLD_ON) + str +
            SET_COLOR(COLOR_BOLD_OFF) + str2 + clear_color(), 1);
    else if(objectp(i) && interactive(i))
        return xml_to_ansi(set_color(COLOR_FG_GREEN) + set_color(COLOR_BOLD_ON) + str +
            set_color(COLOR_BOLD_OFF) + str2 + clear_color(), i);
    else if(!objectp(i))
        return xml_to_ansi(set_color(COLOR_FG_GREEN) + set_color(COLOR_BOLD_ON) + str +
            set_color(COLOR_BOLD_OFF) + str2 + clear_color());
}

/*
 * Function name: find_neighbour
 * Description  : This function will recursively search through the
 *                neighbouring rooms to a particular room to find the
 *                rooms a shout or scream will be heard in.
 * Arguments    : object *found  - the rooms already found.
 *                object *search - the rooms still to search.
 *                int    depth   - the depth still to search.
 * Returns      : object * - the neighbouring rooms.
 */
static object *
find_neighbour(object *found, object *search, int depth)
{
    int index;
    int size;
    int index2;
    int size2;
    mixed *exit_arr;
    object *drzwi;
    object *new_search, *rooms, troom;

    if (!depth)
        return found;

    rooms = found;
    new_search = ({});

    index = -1;
    size = sizeof(search);
    while(++index < size)
    {
        exit_arr = (mixed *) search[index]->query_exit();

        index2 = -3;
        size2 = sizeof(exit_arr);
        while((index2 += 3) < size2)
        {
            if (objectp(exit_arr[index2]))
                troom = exit_arr[index2];
            else if(stringp(exit_arr[index2]))
                troom = find_object(exit_arr[index2]);
            if (objectp(troom) &&
                (member_array(troom, rooms) < 0))
            {
                rooms += ({ troom });
                new_search += ({ troom });
            }
        }
        drzwi = filter(all_inventory(search[index]), &->is_door());
        foreach (object x : drzwi)
        {
            string or = x->query_other_room();
            if(!stringp(or))
                continue;

            LOAD_ERR(x->query_other_room());
            troom = find_object(x->query_other_room());
            if (objectp(troom) && member_array(troom, rooms) == -1)
            {
                rooms += ({ troom });
                new_search += ({ troom });
            }
        }
    }

    return find_neighbour(rooms, new_search, depth - 1);
}

string
shout_name()
{
    /* Odbiorca. Zauwa�my, �e previous_object() to VBFC_OBJECT. */
    object pobj = previous_object(-1);

    if (environment(pobj) == environment(TP) &&
        CAN_SEE_IN_ROOM(pobj) && CAN_SEE(pobj, TP))
        return TP->query_Imie(pobj, PL_MIA);
    if (pobj->query_met(TP))
        return TP->query_name(PL_MIA);
    return TP->koncowka("Jaki�", "Jaka�", "Jakie�", "Jacy�", "Jakie�") + " " + TP->query_rasa(PL_MIA);
}

int
krzyknij(string str)
{
    object env;
    object *lokacje;
    int ile;

    CHECK_MOUTH_BLOCKED;

    if (env = environment(TP))
        lokacje = find_neighbour(({env}), ({env}), SHOUT_DEPTH);
    else
        lokacje = ({});

    object *to_signal = FILTER_OTHER_LIVE(all_inventory(environment(TP)));

    string formated_str = FORMAT_SPEECH(SPEECH_DRUNK(str, TP));

    if(!str)
    {
        write("Krzyczysz g�o�no.\n");
        say(QCIORR(TP, PL_MIA) + " krzyczy g�o�no.\n");
    }
    else
    {
        if(TP->query_option(OPT_ECHO))
            write(koloruj_mowe("Krzyczysz: ", formated_str) + "\n");
        else
            write("Ju�.\n");

        say(koloruj_mowe(QCIMIE(TP, PL_MIA) + " krzyczy: ", formated_str, 1) + "\n");
    }

    object *to_signal_in = ({});
    lokacje -= ({ENV(TP)});
    lokacje -= ({0});
    ile = sizeof(lokacje);
    while(ile--)
    {
        tell_room(lokacje[ile], koloruj_mowe(QCIORR(TP, PL_MIA) + " krzyczy z oddali: ", formated_str, 1) + "\n");

        to_signal_in += FILTER_OTHER_LIVE(all_inventory(lokacje[ile]));
    }

    to_signal->syngal_krzyku(TP, str);
    to_signal_in->sygnal_krzyku(TP, str, ENV(TP));

    return 1;
}

int
szepnij(string str)
{
    object *cele;
    string opis_alko;

    CHECK_MOUTH_BLOCKED;

    NF("Komu co chcesz szepn��?\n");

    if(!str)
        return 0;

    if(!CAN_SEE_IN_ROOM(TP))
    {
        NF("Nie widzisz nikogo, wi�c jak chcesz do kogo� szepn��?\n");
        return 0;
    }

    object *cel;
    if(!parse_command(str, environment(TP), "%l:" + PL_CEL + " %s", cel, str))
        if(!parse_command(str, environment(TP), "'do' %l:" + PL_DOP + " %s", cel, str))
            return 0;

    if(!sizeof(cel))
        return 0;

    cel = NORMAL_ACCESS(cel, 0, 0);

    if(!sizeof(cel))
        return 0;

    if(sizeof(cel) > 1)
    {
        NF("Nie mo�esz szepta� do kilku os�b r�wnocze�nie.\n");
        return 0;
    }

    if(!strlen(str))
    {
        NF("Co chcesz szepn�� " + cel->query_imie(TP, PL_CEL) + "?\n");
        return 0;
    }

    //Usuwamy spacje zrobione przez drivera przed przecinkami.
    str = implode(explode(str, " ,"), ",");

    int jak_bardzo = min(10, 10 * TP->query_intoxicated() / TP->intoxicated_max());
    int jak_bardzo_cel = min(10, 10 * cel[0]->query_intoxicated() / cel[0]->intoxicated_max());

    switch (jak_bardzo - jak_bardzo_cel)
    {
        case -10 .. 0:
            opis_alko = "";
            break;
        case 1..3:
            opis_alko = "\nWyczuwasz od " + TP->koncowka("niego", "niej", "niego") +
                " lekki zapach alkoholu.";
            break;
        case 4..7:
            opis_alko = "\nWyczuwasz od " + TP->koncowka("niego", "niej", "niego") +
                " zapach alkoholu.";
            break;
        default:
            opis_alko = "\nWyczuwasz od " + TP->koncowka("niego", "niej", "niego") + " mocny zapach alkoholu.";
    }

    string formated_str = FORMAT_SPEECH(SPEECH_DRUNK(str, TP));

    if (TP->query_option(OPT_ECHO))
        write(koloruj_mowe("Szepczesz do " + COMPOSITE_LIVE(cel, PL_DOP) + ": ",
            formated_str, TP) + "\n");
    else
        write("Ju�.\n");

    all2actbb("szepcze co� do", cel, PL_DOP);

    //przez kolorowanie nale�y zrezygnowa� z target()
    cel[0]->catch_msg(koloruj_mowe(QCIMIE(TP,PL_MIA)+ " szepcze do ciebie: ",
        formated_str, cel[0]) + opis_alko+clear_color()+"\n");

    //Wysy�amy sygna� do obiektu do kt�rego szepczemy.
    cel[0]->sygnal_szeptu(TP, str);
    //Wysy�amy sygna� do pozosta�ych obiekt�w na lokacji
    (all_inventory(ENV(TP)) - ({TP}) - ({cel[0]}))->sygnal_szeptu(TP, str, cel[0]);

    return 1;
}


