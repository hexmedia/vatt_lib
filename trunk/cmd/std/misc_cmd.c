/*
  misc_cmd.c

  This is now obsolete.

  It must remain as this so that old .o files refering to it will
  get correct new souls.

*/
#pragma save_binary
#pragma strict_types

#include <files.h>

/*
 * These are the souls replacing the old /cmd/std/misc_cmd.c
 */
public string *
replace_soul()
{
    return
    ({
        CMD_LIVE_INFO,
        CMD_LIVE_FIGHT,
        CMD_LIVE_ITEMS,
        CMD_LIVE_STATE,
        CMD_LIVE_SOCIAL,
        CMD_LIVE_THINGS,
        CMD_LIVE_COMMUNICATION,
    });
}
