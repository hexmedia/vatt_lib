/**
 * \file /cmd/std/zasady_parser.c
 *
 * Parser do zasad.
 *
 * @author Krun <krun@vattghern.com.pl>
 * @date 28.7.2008 16:51:39
 */

#pragma no_reset
#pragma strict_types

#include <math.h>
#include <colors.h>

#define DBG(x)  find_player("krun")->catch_msg((x) + "\n");

string      ctag = "",
            mtag = "",
            naglowek = "",
            stopka = "",
           *mtags = ({"zasada", "opis", "stopka", "naglowek"}),
           *in = ({}),
            tresc;
mapping     polaczenia = ([ "p" : ({"w", "l", "z"}), "w" : ({"z"}), "l" : ({"m"}), "z" : ({}), "m" : ({"w", "l"}) ]);
mixed      *rozdzialy = ({});


private void
set_tresc(string t)
{
    tresc = t;
}

private void
add_tresc(string t)
{
    t = t || "";

    if(!stringp(tresc))
        tresc = t;
    else
        tresc += t;
}

private string
get_tresc()
{
    return (tresc ?: "");
}

private void
unset_tresc()
{
    tresc = "";
}

private void
set_opis(string o)
{
    o = o || "";

    rozdzialy[-1]["opis"] = o;
}

private void set_zasada(string z)
{
    if(stringp(z) && strlen(z) > 0)
        rozdzialy[-1]["zasady"] += ({z});
}

private void set_naglowek(string n)
{
    n = n || "";

    naglowek = n;
}

private void
set_stopka(string s)
{
    s = s || "";

    stopka = s;
}

private string
trim(string ret)
{
    ret = str_replace((["&quot;" : "\"", "&lt;" : "<", "&gt;" : ">", "&amp;" : "&"]), ret);

    return ret;
}

private status
sprawdz_pozycje(string nad, string tag)
{
    string *p;

    if(member_array(nad, mtags) >= 0)
    {
        if(tag != "p")
            return 0;
    }
    else
    {
        if(!is_mapping_index(nad, polaczenia))
            return 1;

        p = (pointerp(polaczenia[nad]) ? polaczenia[nad] : ({}));
        p += ({tag});

        if(member_array(tag, p) == -1)
            return 0;
    }

    return 1;
}


void
open_tag(string tag, mapping attr)
{
    status is_mtag = 0;
    string nad;
    string t;

    tag = lower_case(tag);

    if(member_array(tag, mtags) >= 0)
        is_mtag = 1;

    if((!pointerp(in) || !sizeof(in)) && tag != "zasady")
        return;
    else if(tag == "zasady")
    {
        in = ({"zasady",});
        return;
    }

    switch(sizeof(in))
    {
        case 1:
            if(!is_mtag && tag != "rozdzial")
                return;
            break;

        case 2:
            if(tag != "zasada" && !mtag)
                return;
            break;
    }

    //Teraz sprawdzamy co mamy nad tym znacznikiem i czy ten znacznik mo�e pod nim by�
    nad = in[-1];

    if(!sprawdz_pozycje(nad, tag))
        return;

    if(is_mtag)
        mtag = tag;

    switch(tag)
    {
        case "rozdzial":
            rozdzialy += ({ ([ "tytul" : attr["tytul"], "opis" : "", "zasady" : ({}) ]) });
            break;

        case "l":
            if(in[-1] != "p" && in[-1] != "m")
                return;

            t = "\n";
            break;

        case "m":
            t = "* ";
            break;

        case "z":
            t = "\n";
            break;

        case "w":
            t = "";
            break;
    }

    ctag = tag;
    in += ({tag,});

    add_tresc(t);
}

void
close_tag(string tag)
{
    tag = lower_case(tag);

    switch(tag)
    {
        case "m":   add_tresc("\n");    break;
        case "l":   add_tresc("");      break;
        case "w":   add_tresc("");      break;
    }

    if(tag == mtag)
    {
        switch(tag)
        {
            case "opis":        set_opis(get_tresc());      break;
            case "zasada":      set_zasada(get_tresc());    break;
            case "naglowek":    set_naglowek(get_tresc());  break;
            case "stopka":      set_stopka(get_tresc());    break;
        }

        unset_tresc();
    }

    //Aktualizujem ctaga.
    if(sizeof(in) > 2)
        ctag = in[-2];

    if(sizeof(in) > 0)
    {
        if(in[-1] == tag)
            in = in[0..-2];
    }
}

void
handler(string dane)
{
    string tdata;

    if(!sizeof(in) || in[0] != "zasady")
        return;

    tdata = trim(dane);

    if(!strlen(dane))
        return;

    switch(ctag)
    {
        case "p":
            if(member_array(in[-1], mtags + ({"p"})) == -1)
                return;

            add_tresc(tdata);
            break;
        case "m":
            add_tresc(tdata);
            break;

        default:
            return;
    }
}


string zasady()
{
    string zasady = "";

    zasady = sprintf("\n%|79s\n", "ZASADY VATT'GHERNA");

    zasady += "\n      " + break_string(naglowek, 79) + "\n";

    for(int i = 0 ; i < sizeof(rozdzialy) ; i++)
    {
        zasady += "\n" + INT2RZYM(i + 1) + ". " + upper_case(rozdzialy[i]["tytul"]) + ":\n";

        if(rozdzialy[i]["opis"] && strlen(rozdzialy[i]["opis"]))
            zasady += "    " + break_string(rozdzialy[i]["opis"], 79, 4) + "\n\n";

        for(int j = 0 ; j < sizeof(rozdzialy[i]["zasady"]) ; j++)
            zasady += "    $" + (j + 1) + (j + 1 < 10 ? " " : "") +
                " " + break_string(rozdzialy[i]["zasady"][j], 89, 8)[8..] + "\n";
    }

    string *e = explode(stopka, "\n");

    zasady += "\n";
    foreach(string s : e)
        zasady += sprintf("%|79s\n", s);

    return zasady;
}