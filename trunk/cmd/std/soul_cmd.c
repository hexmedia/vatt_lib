/**
 *  \file /cmd/std/soul_cmd.c
 *
 *  spolszczenie - Silvathraec 1997
 *
 *  Tego podstawowego soula dziedzicz� wszystkie soule rasowe. Zawiera on
 *  emoty przeznaczone dla wszystkich graczy. Mo�e r�wnie� s�u�y� jako
 *  przyk�ad soula gildiowego. Je�li zamierzasz robi� w nim jakie� zmiany lub
 *  rozszerzenia, trzymaj si� prosz� pewnych standard�w:
 *
 *  - Komendy emot�w powinny by� czasownikami w formie rozkazuj�cej, wraz ze
 *  swoimi argumentami tworz�cymi poprawne, jak najbardziej naturalne i
 *  sensowne zdanie. W pewnych przypadkach dopuszczalne jest u�ywanie skr�t�w
 *  w formie rzeczownik�w, bez zmiany sk�adni argument�w.
 *  - Warto zwr�ci� uwag� na w�a�ciwe, w zale�no�ci od kontekstu, u�ywanie
 *  funkcji all/allbb, target/targetbb, all2act/all2actbb - nieuzasadnione
 *  ujawnianie obecno�ci ukrytych/niewidzialnych os�b nie jest po��dane.
 *  - Makra SOULDESC() nale�y u�ywa� wy��cznie w wypadku dzia�a�, kt�rych
 *  skutki trwaj� przez pewien czas, takich jak u�miech (w przeciwie�stwie
 *  do, na przyk�ad, uk�onu).
 *  - Nie wszystkie emoty powinny m�c da� si� wykona� na wi�cej ni� jednym
 *  celu jednocze�nie.
 *  - Nale�y sprawdza� takie rzeczy jak LIVE_M_MOUTH_BLOCKED, OPT_ECHO, albo
 *  (wa�ne!) czy gracz jest kobiet�.
 *  - Ka�da funkcja powinna generowa� poprawne komunikaty b��d�w.
 *
 *  Uwaga:
 *  Skutki podejmowanych przez graczy dzia�a� MUSZ� by� wy�wietlane w
 *  nast�puj�cym porz�dku: wykonawca, widzowie, cel. Dzi�ki temu, je�li cel
 *  zareaguje natychmiast, gracze otrzymaj� teksty we w�a�ciwej kolejno�ci.
 *
 *
 *
 *
 *
 *                         ma�a reforma emotek by Lil, Gregh, Delvert 03.05
 *                         ....w�a�ciwie to ci�gle co� dodaj�. Lil. 1.10.05. ;)
 */

#pragma no_clone
#pragma strict_types
#pragma save_binary

inherit "/cmd/std/command_driver";

#include <adverbs.h>
#include <cmdparse.h>
#include <composite.h>
#include <filter_funs.h>
#include <macros.h>
// #include <ss_types.h>
#include <stdproperties.h>
#include <options.h>
#include <speech.h>
#include <sit.h>

#define SUBLOC_SOULEXTRA    "_soul_cmd_extra"
#define SOULDESC(x)         (TP->add_prop(LIVE_S_SOULEXTRA, (x)))
#define LANGUAGE_ALL_RSAY   55  /* Poziom, od ktorego rozumie sie wszystko. */
#define LANGUAGE_MIN_RSAY   15  /* Poziom, do ktorego nie rozumie sie nic. */
#define MAX_FREE_EMOTE      60  /* Maksymalna d�ugo�� wolnego emota. */
#define DUMP_EMOTIONS_OUT   "/doc/help/general/emocje"

#define CHECK_MOUTH_BLOCKED; \
            if (Prop = TP->query_prop(LIVE_M_MOUTH_BLOCKED)) { \
            notify_fail(stringp(Prop) ? Prop : \
                "Nie jeste� w stanie wydoby� z siebie �adnego d�wi�ku.\n"); \
            return 0; }

#define CHECK_MULTI_TARGETS(str, przyp) \
            if (sizeof(cele) > 1) { notify_fail("Nie mo�esz " + str + " " + \
                COMPOSITE_LIVE(cele, przyp) + ".\n"); return 0; }

#define IS_BALD(living) \
            (member_array("�ysy", living->query_przym(1, PL_MIA, 1)) != -1)

#define BUG_FAIL(str) \
            (log_file("bugi", ctime(time()) + ": [" + \
                file_name(TO) + "] " + str + "\n"), \
            "\nWYST�PI� B��D !!! Czarodzieje b�d� ci wdzi�czni za zg�oszenie go, " + \
                TP->query_wolacz() + ".\n\n")

#define NOTIFY_MULTI(oblist, fun, str1, przyp, str2) \
        foreach (object x : oblist) \
                fun(str1 + FO_COMPOSITE_LIVE(oblist, x, przyp) + str2, ({ x }));

#define NOTIFY_MULTI2(oblist, fun, str1, przyp, str2, str3) \
        foreach (object x : oblist) \
                fun(str1 + FO_COMPOSITE_LIVE(oblist, x, przyp) + str2, ({ x }), str3);

private mixed Prop;             /* U�ywane przez CHECK_MOUTH_BLOCKED. */

private string *Kierunki = ({"p�noc", "po�udnie", "wsch�d", "zach�d",
                             "p�nocny wsch�d", "po�udniowy wsch�d",
                             "po�udniowy zach�d", "p�nocny zach�d",
                             "g�r�", "d�"});

private string *Skroty = ({"n", "s", "e", "w", "ne", "se", "sw", "nw", "u",
                           "d"});   /* U�ywane przez 'popatrz', 'wska�'. */

/*
 *  Zwraca nazw� soula.
 */
public string
get_soul_id()
{
    return "emotions";
}

/*
 *  Lista komend. Prosz� dodawa� nowe w kolejno�ci alfabetycznej.
 */
public mapping
query_cmdlist()
{
    return ([
            "beknij"        : "beknij",
            "b�agaj"        : "blagaj",

            "chrz�knij"     : "chrzaknij",
            "cmoknij"       : "cmoknij",
            "czknij"        : "czknij",

            "dygnij"        : "dygnij",

            "fuknij"        : "fuknij",

            "gr�"          : "pogroz",

            "hmm"           : "hmm",

            "j�knij"        : "jeknij",
            "j�zyk"         : "jezyk",

            "kaszlnij"      : "kaszlnij",
            "kichnij"       : "kichnij",
            "kiwnij"        : "pokiwaj",        /* alias */
            "klaszcz"       : "klaszcz",
            "kla�nij"       : "klaszcz",        /* alias */
            "klepnij"       : "poklep",         /* alias */
            "kopnij"        : "kopnij",

            "machnij"       : "machnij",
            "mamrocz"       : "mamrocz",
            "mla�nij"       : "mlasnij",
            "mrugnij"       : "mrugnij",

            "naburmusz"     : "naburmusz",
            "nadepnij"      : "nadepnij",
            "namy�l"        : "namysl",
            "nas�uchuj"     : "nasluchuj",
            "nawi�"         : "nawin",

            "obejmij"       : "obejmij",
            "ob"            : "obejrzyj",       /* skr�t */
            "obejrzyj"      : "obejrzyj",
            "obgry�"        : "obgryz",
            "obli�"         : "obliz",
            "oczko"         : "oczko",
            "odetchnij"     : "odetchnij",
            "opluj"         : "opluj",
            "otrz��nij"     : "otrzasnij",
            "otrzep"        : "otrzep",
            "otrzyj"        : "otrzyj",
            "otw�rz"        : "otworz",         /* oczy */

            "parsknij"      : "parsknij",
            "poca�uj"       : "pocaluj",
            "popatrz"       : "popatrz",
            "pierdnij"      : "pierdnij",
            "pi�nij"        : "zapiszcz",       /*alias*/
            "poci�gnij"     : "pociagnij",
            "pociesz"       : "pociesz",
            "podaj"         : "podaj",
            "pod�ub"        : "podlub",
            "podrap"        : "podrap",
            "podrepcz"      : "podrepcz",
            "podskocz"      : "podskocz",
            "podzi�kuj"     : "podziekuj",
            "pog�ad�"       : "poglaszcz",      /* alias */
            "pog�aszcz"     : "poglaszcz",
            "pogratuluj"    : "pogratuluj",
            "pogr�"        : "pogroz",
            "poka�"         : "pokaz",          /* alias do "j�zyk" */
            "pokiwaj"       : "pokiwaj",
            "poklep"        : "poklep",
            "pok�o�"        : "uklon",          /* alias */
            "pokr��"        : "pokrec",
            "po�askocz"     : "polaskocz",
            "pomachaj"      : "pomachaj",
            "popraw"        : "popraw",
            "popukaj"       : "popukaj",
            "potrz��nij"    : "potrzasnij",
            "potrzyj"       : "potrzyj",
            "potwierd�"     : "potwierdz",
            "pow�chaj"      : "powachaj",
            "powitaj"       : "powitaj",
            "po�egnaj"      : "pozegnaj",
            "prychnij"      : "prychnij",
            "przebacz"      : "wybacz",         /* alias */
            "przebieraj"    : "przebieraj",
            "przeci�gnij"   : "przeciagnij",
            "przeczesz"     : "przeczesz",
            "przekrzyw"     : "przekrzyw",
            "prze�knij"     : "przelknij",
            "przepro�"      : "przepros",
            "przest�p"      : "przestap",
            "przetrzyj"     : "przetrzyj",
            "przewr��"      : "przewroc",
            "prztyknij"     : "prztyknij",
            "przymru�"      : "zmruz",          /* alias */
            "przytul"       : "przytul",
            "przywitaj"     : "powitaj",        /* alias */
            "pstryknij"     : "pstryknij",
            "pu��"          : "pusc",

            "rozejrzyj"     : "rozejrzyj",
            "roze�miej"     : "zasmiej",
            "rozetrzyj"     : "rozetrzyj",
            "roz��"        : "rozloz",
            "rozp�acz"      : "rozplacz",

            "sapnij"        : "sapnij",
            "ski�"          : "skin",
            "skrzyw"        : "skrzyw",
            "skul"          : "skul",
            "spanikuj"      : "spanikuj",
            "splu�"         : "splun",
            "spoliczkuj"    : "spoliczkuj",
            "spojrzyj"      : "popatrz",        /* alias */
            "szturchnij"    : "szturchnij",
            "syknij"        : "syknij",

            "tupnij"        : "tupnij",

            "ucisz"         : "ucisz",
            "uderz"         : "uderz",
            "uk�o�"         : "uklon",
            "unie�"         : "unies",
            "uszczypnij"    : "uszczypnij",
            "u�ciskaj"      : "usciskaj",
            "u�miech"       : "usmiechnij",      /* skr�t */
            "u�miechnij"    : "usmiechnij",
          /* "uszczypnij":"uszczypnij", */

            "warknij"       : "warknij",
            "westchnij"     : "westchnij",
            "wrza�nij"      : "wrzasnij",
            "wska�"         : "wskaz",
            "wybacz"        : "wybacz",
            "wyba�usz"      : "wytrzeszcz",     /* alias */
            "wykrzyw"       : "skrzyw",         /* alias */
            "wyp�acz"       : "wyplacz",
            "wystaw"        : "wystaw",
            "wyszczerz"     : "wyszczerz",
            "wytrzeszcz"    : "wytrzeszcz",
            "wzdrygnij"     : "wzdrygnij",
            "wzrusz"        : "wzrusz",

            "zachichocz"    : "zachichocz",
            "zachlip"       : "zachlip",
            "zachrumkaj"    : "zachrumkaj",
            "zachwy�"       : "zachwyc",
            "zaci�nij"      : "zacisnij",
            "zaczerwie�"    : "zaczerwien",
            "zadr�yj"       : "zadrzyj",
            "zaj�cz"        : "jeknij",         /* alias */
            "zajrzyj"       : "zajrzyj",
            "zagry�"        : "zagryz",
            "zagwi�d�"      : "zagwizdz",
            "zakaszl"       : "kaszlnij",       /* alias*/
            "zaklaszcz"     : "klaszcz",
            "zaklnij"       : "zaklnij",
            "zakrztu�"      : "zakrztus",
            "za�am"         : "zalam",
            "za�kaj"        : "zalkaj",
            "zamamrocz"     : "mamrocz",        /* alias */
            "zamknij"       : "zamknij",        /* oczy */
            "zamrucz"       : "zamrucz",
            "zamy�l"        : "zamysl",
            "zanu�"         : "zanuc",
            "zapiszcz"      : "zapiszcz",
            "zap�acz"       : "zaplacz",
            "zaprzecz"      : "zaprzecz",
            "zarechocz"     : "zarechocz",
            "zarumie�"      : "zarumien",
            "zastan�w"      : "zamysl",
            "zasycz"        : "syknij",         /* alias */
            "za�miej"       : "zasmiej",
            "zaszlochaj"    : "zaszlochaj",
            "zata�cz"       : "zatancz",
            "zatkaj"        : "zatkaj",
            "zatrzepocz"    : "zatrzepocz",
            "zatrzyj"       : "zatrzyj",
            "zawyj"         : "zawyj",
            "zazgrzytaj"    : "zazgrzytaj",
            "zbe�taj"       : "zbeltaj",
            "zblednij"      : "zblednij",
            "zdziw"         : "zdziw",
            "ziewnij"       : "ziewnij",
            "zignoruj"      : "zignoruj",
            "z�orzecz"      : "zlorzecz",
            "zmarszcz"      : "zmarszcz",
            "zmru�"         : "zmruz",
            "zwie�"         : "zwies",
            "zwymiotuj"     : "zbeltaj"         /* alias */
    ]);
}

/*
 * Function name: using_soul
 * Description  : Called once by the living object using this soul. Adds
 *                sublocations responsible for extra descriptions of the
 *                living object.
 * Arguments    : object live - the living using the soul.
 */
public void
using_soul(object live)
{
    live->add_subloc(SUBLOC_SOULEXTRA, file_name(TO));
    live->add_subloc(EYES_CLOSED, file_name(TO));
}

public string
show_subloc_soulextra(object on_obj, object for_obj)
{
    string subloc = SUBLOC_SOULEXTRA; // to kiedys moznaby poprawic, robilem to na szybko

    if ((subloc != SUBLOC_SOULEXTRA) ||
        on_obj->query_prop(TEMP_SUBLOC_SHOW_ONLY_THINGS) ||
        !(subloc = on_obj->query_prop(LIVE_S_SOULEXTRA)))

        return "";

    if (for_obj == on_obj)
    {
        string *wyrazy = explode(subloc, " ");
        string pierwszy = wyrazy[0];

        switch (pierwszy)
        {
            case "jest":
                wyrazy[0] = "jeste�";
                break;
            case "jeste�,":
                wyrazy[0] = "jeste�,";
                break;
            default:
                if (pierwszy[-1..] == ",")
                    wyrazy[0] = pierwszy[..-2] + "sz,";
                else
                    wyrazy[0] = pierwszy + "sz";
        }
        subloc = implode(wyrazy, " ");
    }
    return capitalize(subloc + ".\n");
}

public string
show_subloc_eyes(object on_obj, object for_obj)
{
    if (on_obj->query_prop(EYES_CLOSED))
        return "Ma zamkni�te oczy.\n";
    else
        return "";
}

/*
 * Function name: show_subloc
 * Description  : Shows the specific sublocation description for a living.
 * Arguments    : string subloc  - the subloc to display.
 *                object on      - the object to which the subloc is linked.
 *                object for_obj - the object that wants to see the subloc.
 * Returns      : string - the subloc description.
 */
public string
show_subloc(string subloc, object on_obj, object for_obj)
{
    if (on_obj->query_prop(TEMP_SUBLOC_SHOW_ONLY_THINGS))
        return "";

    switch (subloc) {
        case SUBLOC_SOULEXTRA: return show_subloc_soulextra(on_obj, for_obj);
        case EYES_CLOSED: return show_subloc_eyes(on_obj, for_obj);
    }
}

/*
 * Function name: query_cmd_soul
 * Description  : This is a command soul. This defines it as such.
 * Returns      : int 1 - always.
 */
public int
query_cmd_soul()
{
    return 1;
}

/*
 * Function name: dump_emotions
 * Description  : This function can be used to dump all emotions to a file
 *                in a sorted and formatted way. This dump can be used for
 *                the 'help emotions' document. The output of this function
 *                will be written to the file DUMP_EMOTIONS_OUT.
 * Returns      : int 1 - always.
 */
public nomask int
dump_emotions()
{
    int index = -1;
    int size = strlen(ALPHABET);
    string *words;

    setuid();
    seteuid(getuid());

    rm(DUMP_EMOTIONS_OUT);

    while(++index < size)
    {
        words = filter(m_indices(query_cmdlist()),
                       &wildmatch((ALPHABET[index..index] + "*")));

        if (!sizeof(words))
            continue;

        if (strlen(words[0]) < 14)
            words[0] = (words[0] + "                ")[..13];

        write_file(DUMP_EMOTIONS_OUT,
            sprintf("%-80#s\n\n", implode(sort_array(words), "\n")));
    }
    return 1;
}

/*
 *  Funkcje obs�uguj�ce emoty. Prosz� dodawa� nowe w kolejno�ci alfabetycznej.
 */

int
beknij(string str)
{
    string jak;

    CHECK_MOUTH_BLOCKED;

    jak = check_adverb_with_space(str, "prostacko");
    if (jak != NO_ADVERB_WITH_SPACE)
    {
        write("Bekasz" + jak + ".\n");
        all("beka" + jak + ".", jak);
        return 1;
    }
    notify_fail("Beknij [jak] ?\n");
    return 0;
}

int
blagaj(string str)
{
    object *cele;
    int i;

    CHECK_MOUTH_BLOCKED;

    cele = parse_this(str, "%l:" + PL_BIE + " 'o' 'wybaczenie' / "
                    + "'przebaczenie'");
    if (sizeof(cele))
    {
        if (sizeof(cele) >= 2)
        {
            actor("B�agasz", cele, PL_BIE, " o wybaczenie.");
            all2actbb("b�aga", cele, PL_BIE, " o wybaczenie.");
            NOTIFY_MULTI(cele, target, "b�aga ", PL_BIE, " o wybaczenie.");
            return 1;
        }
        else
        {
            actor("B�agasz", cele, PL_BIE, " o wybaczenie.");
            all2actbb("b�aga", cele, PL_BIE, " o wybaczenie.");
            target("b�aga ci� o wybaczenie.", cele);
            return 1;
        }
    }
    cele = parse_this(str, "%l:" + PL_BIE + " [o] [lito��] / [�ask�]");
    if (sizeof(cele))
    {
        if (sizeof(cele) >= 2)
        {
            actor("Padasz na kolana, b�agaj�c", cele, PL_BIE, " o lito��.");
            all2actbb("pada na kolana, b�agaj�c", cele, PL_BIE, " o lito��.");
            NOTIFY_MULTI(cele, target, "pada na kolana, b�agaj�c ", PL_BIE, " o lito��.");
            return 1;
        }
        else
        {
            actor("Padasz na kolana, b�agaj�c", cele, PL_BIE, " o lito��.");
            all2actbb("pada na kolana, bl�gaj�c", cele, PL_BIE, " o lito��.");
            target("pada na kolana, b�agaj�c ci� o lito��.", cele);
            return 1;
        }
    }
    notify_fail("B�agaj kogo [o co] ?\n");
    return 0;
}

int
chrzaknij(string str)
{
    string jak;

    CHECK_MOUTH_BLOCKED;

    jak = check_adverb_with_space(str, "znacz�co");
    if (jak != NO_ADVERB_WITH_SPACE)
    {
        write("Chrz�kasz" + jak + ".\n");
        all("chrz�ka" + jak + ".", jak);
        return 1;
    }
    notify_fail("Chrz�knij [jak] ?\n");
    return 0;
}

int
cmoknij(string str)
{
    string jak;

    CHECK_MOUTH_BLOCKED;

    jak = check_adverb_with_space(str, "z dezaprobat�");
    if (jak != NO_ADVERB_WITH_SPACE)
    {
        write("Cmokasz" + jak + ".\n");
        all("cmoka" + jak + ".", jak);
        return 1;
    }
    notify_fail("Cmoknij [jak] ?\n");
    return 0;
}

int
czknij(string str)
{
    string jak;

    CHECK_MOUTH_BLOCKED;

    jak = check_adverb_with_space(str, "prostacko");
    if (jak != NO_ADVERB_WITH_SPACE)
    {
        SOULDESC("ma czkawk�");
        write("Czkasz" + jak + ".\n");
        allbb("czka" + jak + ".", jak);
        return 1;
    }
    notify_fail("Czknij [jak] ?\n");
    return 0;
}

int
dygnij(string str)
{
    object *cele;
    string *jak;
    int i;

    /* Zabezpieczenie przed bledna interpretacja 'przed' jako przyslowka. */
    if (str && wildmatch("przed *", str))
        jak = ({str, ADD_SPACE_TO_ADVERB("z gracj�")});
    else
        jak = parse_adverb_with_space(str, "z gracj�", 0);

    if (TP->query_gender() != G_FEMALE)
        jak[1] = ADD_SPACE_TO_ADVERB("niezdarnie");

    if (!jak[0])
    {
        write("Dygasz" + jak[1] + ".\n");
        allbb("dyga" + jak[1] + ".", jak[1]);
        return 1;
    }
    cele = parse_this(jak[0], "'przed' %l:" + PL_NAR);
    if (sizeof(cele))
    {
        if (sizeof(cele) >= 2)
        {
            actor("Dygasz przed", cele, PL_NAR, jak[1] + ".");
            all2actbb("dyga przed", cele, PL_NAR, jak[1] + ".", jak[1]);
            NOTIFY_MULTI2(cele, target, "dyga przed ", PL_NAR, jak[1] + ".", jak[1]);
            return 1;
        }
        else
        {
            actor("Dygasz przed", cele, PL_NAR, jak[1] + ".");
            all2actbb("dyga przed", cele, PL_NAR, jak[1] + ".", jak[1]);
            targetbb("dyga przed tob�" + jak[1] + ".", cele, jak[1]);
            return 1;
        }
    }
    notify_fail("Dygnij [jak] [przed kim] ?\n");
    return 0;
}

int
fuknij(string str)
{
    string jak;

    jak = check_adverb_with_space(str, "ze z�o�ci�");
    if (jak != NO_ADVERB_WITH_SPACE)
    {
        write("Fukasz" + jak + ".\n");
        all("fuka" + jak + ".");
        return 1;
    }
    notify_fail("Fuknij [jak] ?\n");
    return 0;
}


int
pogroz(string str)
{
    object *bronie;
    object *cele;
    int i;

    bronie = filter(all_inventory(TP), &operator(!=)(0,) @ &->query_wielded());
    cele = parse_this(str, "%l:" + PL_CEL);

    if (sizeof(bronie) == 0 && sizeof(cele))
    {
        if (sizeof(cele) >= 2)
        {
            actor("Grozisz palcem", cele, PL_CEL);
            all2actbb("grozi palcem", cele, PL_CEL);
            NOTIFY_MULTI(cele, target, "grozi palcem ", PL_CEL, ".");
            return 1;
        }
        else
        {
            actor("Grozisz palcem", cele, PL_CEL);
            all2actbb("grozi palcem", cele, PL_CEL);
            targetbb("grozi ci palcem.", cele);
            return 1;
        }
    }

    if (sizeof(bronie) > 0 && sizeof(cele))
    {
        string opis_broni = COMPOSITE_WORDS(map(bronie, &->short(PL_NAR)));
        if (sizeof(cele) >= 2)
        {
            actor("Grozisz " + opis_broni, cele, PL_CEL);
            all2actbb("grozi " + opis_broni, cele, PL_CEL);
            NOTIFY_MULTI(cele, target, "grozi ", PL_CEL, opis_broni);
            return 1;
        }
        else
        {
            actor("Grozisz " + opis_broni, cele, PL_CEL);
            all2actbb("grozi " + opis_broni + " ", cele, PL_CEL);
            targetbb("grozi ci " + opis_broni + ".", cele, opis_broni);
            return 1;
        }
    }

    notify_fail("Pogr� [komu] ?\n");
    return 0;
}

int
hmm(string str)
{
    CHECK_MOUTH_BLOCKED;

    if (!str)
    {
        write("Hmmmmm...\n");
        allbb("wydaje z siebie d�ugie hmmmmm...");
        return 1;
    }
    notify_fail("Hmm?\n");
    return 0;
}

int
jeknij(string str)
{
    string jak;

    CHECK_MOUTH_BLOCKED;

    jak = check_adverb_with_space(str, "bole�nie");

    if (jak != NO_ADVERB_WITH_SPACE)
    {
        write("J�czysz" + jak + ".\n");
        all("j�czy" + jak + ".", jak);
        return 1;
    }

    notify_fail(capitalize(query_verb()) + " [jak] ?\n");
    return 0;
}

int
jezyk(string str)
{
    object *cele;
    int i;

    if (!str)
    {
        write("Pokazujesz j�zyk.\n");
        allbb("pokazuje j�zyk.");
        return 1;
    }
    cele = parse_this(str, "%l:" + PL_CEL);
    if (sizeof(cele))
    {
        if (sizeof(cele) >= 2)
        {
            actor("Pokazujesz", cele, PL_CEL, " j�zyk.");
            all2actbb("pokazuje", cele, PL_CEL, " j�zyk.");
            NOTIFY_MULTI(cele, target, "pokazuje j�zyk ", PL_CEL, ".");
            return 1;
        }
        else
        {
            actor("Pokazujesz", cele, PL_CEL, " j�zyk.");
            all2actbb("pokazuje", cele, PL_CEL, " j�zyk.");
            targetbb("pokazuje ci j�zyk.", cele);
            return 1;
        }
    }
    notify_fail("(Poka�) j�zyk [komu] ?\n");
    return 0;
}

int
kaszlnij(string str)
{
    string jak;

    jak = check_adverb_with_space(str, "g�o�no");
    if (jak != NO_ADVERB_WITH_SPACE)
    //if (!str || jak ~= "w d�onie")
    {
        write("Kaszlesz" + jak + ".\n");
        all("kaszle" + jak + ".");
        return 1;
    }
    notify_fail("Kaszlnij [jak] ?\n");
    return 0;
}

int
kichnij(string str)
{
    string jak;

    jak = check_adverb_with_space(str, "g�o�no");
    if (jak != NO_ADVERB_WITH_SPACE)
    {
        write("Na wp� przymykasz oczy, odchylasz g�ow� i kichasz" + jak
            + ".\n");
        all("na wp� przymyka oczy, odchyla g�ow� i kicha" + jak + ".");
        return 1;
    }
    notify_fail("Kichnij [jak] ?\n");
    return 0;
}

int
klaszcz(string str)
{
    string jak;

    if(!HAS_FREE_HANDS(TP))
    {
       write("Musisz mie� obie d�onie wolne, aby m�c to zrobi�.\n");
       return 1;
    }

    jak = check_adverb_with_space(str, "rado�nie");

    if (jak != NO_ADVERB_WITH_SPACE)
    //if (!str || jak ~= "w d�onie")
        {
	write("Klaszczesz w d�onie" + jak + ".\n");
        all("klaszcze w d�onie" + jak + ".");
        return 1;
        }
    notify_fail("Klaszcz [jak] ?\n");
    return 0;
}


int
kopnij(string str)
{
    object *cele;
    string *jak;
    notify_fail("Kopnij [jak] kogo [gdzie] ? lub: kopnij w co ?\n");

    if(!str)
        return 0;

    jak = parse_adverb_with_space(str, "brutalnie", 0,
            ({"k�tem oka","weso�o","rado�nie"})); //itd. itd... ;)

    //taki �arcik, V. ;]
    if(str ~= "w kalendarz")
    {
        write("Umierasz.\nOddalasz si�...�artowa�em!\n");
        return 1;
    }

    cele = parse_this(jak[0], "%l:" + PL_BIE);
    if (sizeof(cele))
    {
        CHECK_MULTI_TARGETS("kopa� jednocze�nie", PL_DOP);
        actor("Kopiesz", cele, PL_BIE, jak[1]+".");
        all2act("kopie", cele, PL_BIE, jak[1] + ".", jak[1]);
        target("kopie ci�" + jak[1] +".", cele, jak[1]);
        return 1;
    }

    cele = parse_this(jak[0], "%l:" + PL_BIE + " 'w' 'kostk�'");
    if (sizeof(cele))
    {
        CHECK_MULTI_TARGETS("kopa� jednocze�nie", PL_DOP);

        actor("Kopiesz", cele, PL_BIE, " po kostkach.");
        all2act("kopie", cele, PL_BIE, " po kostkach.");
        target("kopie ci� po kostkach.", cele);
        cele->add_prop(LIVE_S_SOULEXTRA,
                       "podskakuje, rozcieraj�c obola�e kostki");
        return 1;
    }

    cele = parse_this(jak[0], "%l:" + PL_BIE + " 'w' 'jaja' / 'krocze'");

    if (sizeof(cele))
    {
        CHECK_MULTI_TARGETS("kopa� jednocze�nie", PL_DOP);

        if(cele[0]->query_gender())
        {
            write("Tam chyba nie ma czego� takiego... Chocia�, kto wie?\n");
            return 1;
        }
        actor("Kopiesz", cele, PL_BIE, " w krocze.");
        all2act("kopie", cele, PL_BIE, " w krocze.");
        target("kopie ci� w krocze! Au�!", cele);
        cele->add_prop(LIVE_S_SOULEXTRA,
                       "zwija si� z b�lu, trzymaj�c za krocze");
        return 1;
    }

    //kopanie 'w' co� tzn. przedmiot jaki�
    if (parse_command(str,
        FILTER_DEAD(FILTER_CAN_SEE(all_inventory(ENV(TP)),TP)),
        "[w] %i:" + PL_BIE, cele))
    {
        cele = NORMAL_ACCESS(cele,0,0);

        if (sizeof(cele))
        {
            CHECK_MULTI_TARGETS("kopa� jednocze�nie", PL_DOP);

	    //taki sobie spos�b na obej�cie b��du, ale zawsze jaki� :)
	    //b��d polega� na tym �e composite pokazywa� "kogo�", zamiast nazwy
	    //dla przedmiot�w z set_no_show_composite...
	    int byl_no_show = 0;
	    if(cele[0]->query_no_show_composite())
	    {
		byl_no_show = 1;
		cele[0]->set_no_show_composite(0);
	    }

            if(cele[0]->query_prop(OBJ_I_VOLUME) > 1000)
            {
            actor("Kopiesz w", cele, PL_BIE, ".");
            all2act("kopie w", cele, PL_BIE, ".");
            }
            else
            {
            actor("Kopiesz", cele, PL_BIE, ".");
            all2act("kopie", cele, PL_BIE, ".");
            }

	    if(byl_no_show)
		cele[0]->set_no_show_composite(1);
	    byl_no_show = 0;

            return 1;
        }
    }
    return 0;
}

int
machnij(string str)
{
    object *cele;
    string *jak;
    int i;

    if (str ~= "r�k�")
        str = 0;
    else if (str)
        sscanf(str, "r�k� %s", str);

    if(!HAS_FREE_HAND(TP))
    {
       write("Musisz mie� przynajmniej jedn� d�o� woln�, aby m�c to zrobi�.\n");
       return 1;
    }

    jak = parse_adverb_with_space(str, "z rezygnacj�", 0);
    if (!jak[0])
    {
        write("Machasz r�k�" + jak[1] + ".\n");
        allbb("macha r�k�" + jak[1] + ".", jak[1]);
        return 1;
    }
    cele = parse_this(jak[0], "'na' %l:" + PL_DOP);
    if (sizeof(cele))
    {
        if (sizeof(cele) >= 2)
        {
            actor("Machasz na", cele, PL_DOP, jak[1] + ".");
            all2actbb("macha na", cele, PL_DOP, jak[1] + ".", jak[1]);
            NOTIFY_MULTI2(cele, target, "macha na ", PL_DOP, jak[1] + ".", jak[1]);
            return 1;
        }
        else
        {
            actor("Machasz na", cele, PL_DOP, jak[1] + ".");
            all2actbb("macha na", cele, PL_DOP, jak[1] + ".", jak[1]);
            target("macha na ciebie" + jak[1] + ".", cele, jak[1]);
            return 1;
        }
    }
    notify_fail("Machnij [r�k�] [jak] [na kogo] ?\n");
    return 0;
}

int
mamrocz(string str)
{
    string jak;

    jak = check_adverb_with_space(str, "nerwowo");
    if (jak != NO_ADVERB_WITH_SPACE)
    {
        SOULDESC("Mamrocze co�" + jak);
        write("Mamroczesz co�" + jak + ".\n");
        allbb("mamrocze co�" + jak + ".");
        return 1;
    }
    notify_fail("Mamrocz [jak] ?\n");
    return 0;
}


int
mlasnij(string str)
{
    string *jak;
    jak = parse_adverb_with_space(str, "g�o�no", 0);
    if (!jak[0])
    {
        write("Mlaskasz" + jak[1] + ".\n");
        allbb("mlaska" + jak[1] + ".", jak[1]);
        return 1;
    }
    notify_fail("Mla�nij [jak] ?\n");
    return 0;
}

int
mrugnij(string str)
{
    object *cele;
    string *jak;
    int i;

    jak = parse_adverb_with_space(str, "porozumiewawczo", 0);

    if(!CAN_SEE_IN_ROOM(TP))
    {
       write("Ciemno��...Ciemno�� widzisz!\n");
       return 1;
    }

    if (!jak[0])
    {
        write("Mrugasz" + jak[1] + ".\n");
        allbb("mruga" + jak[1] + ".", jak[1]);
        return 1;
    }
    cele = parse_this(jak[0], "'do' %l:" + PL_DOP);
    if (sizeof(cele))
    {
        if (sizeof(cele) >= 2)
        {
            actor("Mrugasz do", cele, PL_DOP, jak[1] + ".");
            all2actbb("mruga do", cele, PL_DOP, jak[1] + ".", jak[1]);
            NOTIFY_MULTI2(cele, target, "mruga do ", PL_DOP, jak[1] + ".", jak[1]);
            return 1;
        }
        else
        {
            actor("Mrugasz do", cele, PL_DOP, jak[1] + ".");
            all2actbb("mruga do", cele, PL_DOP, jak[1] + ".", jak[1]);
            targetbb("mruga do ciebie" + jak[1] + ".", cele, jak[1]);
            return 1;
        }
    }
    notify_fail("Mrugnij [jak] [do kogo] ?\n");
    return 0;
}

int
naburmusz(string str)
{
    if(str~="si�")
    {
        //SOULDESC("namy�la si� nad czym�");
        write("Naburmuszasz si�.\n");
        allbb("naburmusza si�.");
        return 1;
    }
    notify_fail("Naburmusz si� ?\n");
    return 0;
}

int
nadepnij(string str)
{
    object *cele;

    if(sizeof(cele = parse_this(str, "'na' %l:" + PL_BIE)))
    {
        CHECK_MULTI_TARGETS("depta� jednocze�nie po", PL_MIE);
        if(!cele[0]->is_humanoid())
        {
            write("Nie bardzo wiesz jak si� do tego zabra�.\n");
            return 1;
        }

        actor("Z m�ciw� satysfakcj� nadeptujesz", cele, PL_CEL, " na stop�.");
        all2actbb("nadeptuje", cele, PL_CEL, " na stop�.");
        target("nadeptuje ci na stop�. Auhuhu... ! Co za b�l...", cele);
        cele->add_prop(LIVE_S_SOULEXTRA,
                       "podskakuje, rozcieraj�c obola�� stop�");
        return 1;
    }
    notify_fail("Nadepnij na kogo?\n");
    return 0;
}

int //free emot jest poroniony w tym emocie. lil.
namysl(string str)
{
    //if (str ~= "si�" || (str && wildmatch("si� nad *", str)))
    if(str~="si�")
    {
        //if (strlen(str) > MAX_FREE_EMOTE)
        //    str = "si� nad czym� niezwykle skomplikowanym";

        SOULDESC("namy�la si� nad czym�");
        write("Namy�lasz si�.\n");
        allbb("namy�la si�.");
        return 1;
    }

    //notify_fail("Namy�l si� [nad czym] ?\n");
    notify_fail("Namy�l si�?\n");
    return 0;
}

int
nasluchuj(string str)
{
    if(!str)
    {
        SOULDESC("nas�uchuje uwa�nie.\n");
        write("Nas�uchujesz uwa�nie.\n");
        allbb("nas�uchuje uwa�nie.");
        return 1;
    }

    notify_fail("Nas�uchuj?");
    return 0;
}

int
nawin(string str)
{
    notify_fail("Nawi� w�osy na palec [jak]?\n");

    if(!str)
        return 0;

    string *jak = parse_adverb_with_space(str, "w zamy�leniu", 1);

    if(jak[0] ~= "w�osy na palec")
    {
        if(TP->query_dlugosc_wlosow() > 30.0)
        {
            write("Nawijasz w�osy na palec"+jak[1]+".\n");
            allbb("nawija w�osy na palec"+jak[1]+".");
            return 1;
        }
        else
            notify_fail("Masz za kr�tkie w�osy!\n");
    }

    return 0;
}

int
obejmij(string str)
{
    object *cele;
    string *jak;

    jak  = parse_adverb_with_space(str, "przyjacielsko", 1);
    cele = parse_this(jak[0], "%l:" + PL_BIE);

    if(sizeof(filter(cele, &->is_humanoid())) == 1)
    {
        CHECK_MULTI_TARGETS("obejmowa� jednocze�nie", PL_DOP);

        actor("Obejmujesz" + jak[1], cele, PL_BIE, " w pasie.");
        all2act("obejmuje" + jak[1], cele, PL_BIE, " w pasie.", jak[1]);
        target("obejmuje ci�" + jak[1] + " w pasie.", cele, jak[1]);
        return 1;
    }
    else
    {
       write("Nie bardzo wiesz jak si� do tego zabra�.\n");
       return 1;
    }

    notify_fail("Obejmij kogo [jak] ?\n");
    return 0;
}

int
obejrzyj(string str)
{
    if(str ~= "si�" || str ~= "si� za siebie")
    {
        SOULDESC("�ypie nerwowo za siebie");
        write("Ogl�dasz si� nerwowo za siebie i...\n"
            + "Na szcz�cie nikt za tob� nie sta�.\n");
        allbb("ogl�da si� nerwowo za siebie.");
        return 1;
    }
    /* Notify_fail jest zdefiniowany w /cmd/live/things. */
    return 0;
}

int
obgryz(string str)
{
    if (str == "paznokcie")
    {
        write("Obgryzasz nerwowo paznokcie.\n");
        allbb("obgryza nerwowo paznokcie.");
        return 1;
    }
    notify_fail("Obgry� paznokcie ?\n");
    return 0;
}

int
obliz(string str)
{
   string *jak;
   jak = parse_adverb_with_space(str, "ze smakiem", 1);
   if (jak[0] ~= "si�")
   {
       SOULDESC("oblizuje si�" + jak[1]);
       write("Oblizujesz si�" + jak[1] + ".\n");
       allbb("oblizuje si�" + jak[1] + ".", jak[1]);
       return 1;
   }
   if(jak[0] ~= "palce")
   {
       SOULDESC("oblizuje palce" +jak[1]);
       write("Oblizujesz palce" + jak[1] +".\n");
       allbb("oblizuje palce" + jak[1] + ".", jak[1]);
       return 1;
   }
   notify_fail("Obli� si� [jak] ?\n");
   return 0;
}

int
oczko(string str)
{
    object *cele;
    int i;

    if(!CAN_SEE_IN_ROOM(TP))
    {
       write("Ciemno��...Ciemno�� widzisz!\n");
       return 1;
    }

    if (!str)
    {
        write("Puszczasz dyskretnie oczko.\n");
        allbb("puszcza dyskretnie oczko.");
        return 1;
    }
    cele = parse_this(str, "'do' %l:" + PL_DOP);
    if (sizeof(cele))
    {
        if (sizeof(cele) >= 2)
        {
            actor("Puszczasz dyskretnie oczko do", cele, PL_DOP + ".");
            all2actbb("puszcza dyskretnie oczko do", cele, PL_DOP + ".");
            NOTIFY_MULTI(cele, target, "puszcza dyskretnie oczko do", PL_DOP, ".");
            return 1;
        }
        else
        {
            actor("Puszczasz dyskretnie oczko do", cele, PL_DOP + ".");
            all2actbb("puszcza dyskretnie oczko do", cele, PL_DOP + ".");
            targetbb("puszcza ci dyskretnie oczko.", cele);
            return 1;
        }
    }
    notify_fail("(Pu��) oczko [do kogo] ?\n");
    return 0;
}

int
odetchnij(string str)
{
    string jak;

    CHECK_MOUTH_BLOCKED;

    jak = check_adverb_with_space(str, "pe�n� piersi�");
    if (jak != NO_ADVERB_WITH_SPACE)
    {
        write("Oddychasz" + jak + ".\n");
        all("oddycha" + jak + ".", jak);
        return 1;
    }
    notify_fail("Odetchnij [jak] ?\n");
    return 0;
}

int
opluj(string str)
{
    object *cele;

    cele = parse_this(str, "%l:" + PL_BIE);
    if (sizeof(cele))
    {
        CHECK_MULTI_TARGETS("plu� jednocze�nie na", PL_BIE);

        actor("Opluwasz", cele, PL_BIE);
        all2actbb("opluwa", cele, PL_BIE, ".");
        target("opluwa ci�.", cele);
        return 1;
    }
    notify_fail("Opluj kogo?\n");
    return 0;
}

int
otrzasnij(string str)
{
    if (str ~= "si�")
    {
        if (member_array("przemoczenie_subloc", TP->query_sublocs()) != -1)
        {
            write("Otrz�sasz si�, chlapi�c wod� woko�o. Brrr... !\n");
            allbb("otrz�sa si�, chlapi�c wod� woko�o.");
        }
        else
        {
            write("Otrz�sasz si�. Brrr... !\n");
            allbb("otrz�sa si�.");
        }

        return 1;
    }
    else if (str && wildmatch("si� *my�l*", str))
    {
        if (strlen(str) > MAX_FREE_EMOTE)
            str = "si� na my�l o czym� szczeg�lnie potwornym";

        if (member_array("przemoczenie_subloc", TP->query_sublocs()) != -1)
        {
            write("Otrz�sasz si�, chlapi�c wod� woko�o. Brrr... !\n");
            allbb("otrz�sa si�, chlapi�c wod� woko�o.");
        }
        else
        {
            write("Otrz�sasz " + str + ". Brrr... !\n");
            allbb("otrz�sa " + str + ".");
        }
        return 1;
    }

    notify_fail("Otrz��nij si� [na my�l o czym] ?\n");
    return 0;
}

int
otrzep(string str)
{
    if(str ~= "si�")
    {
        write("Otrzepujesz si� niedbale.\n");
        allbb("otrzepuje si� niedbale.");
        return 1;
    }

    notify_fail("Otrzep si�?\n");
    return 0;
}

int
otrzyj(string str)
{
    if (!str || str ~= "pot z czo�a")
    {
        write("Ocierasz pot z czo�a.\n");
        allbb("ociera pot z czo�a.");
        return 1;
    }

    if(str ~= "usta")
    {
        write("Ocierasz usta d�oni�.\n");
        allbb("ociera usta d�oni�.");
        return 1;
    }

    notify_fail("Otrzyj pot z czo�a lub ust ?\n");
    return 0;
}

int
otworz(string str)
{
    if (str =="oczy" && TP->query_prop(LIVE_I_SEE_DARK) < 0)
    {
        write("Otwierasz oczy.\n");
        allbb("otwiera oczy.");
        TP->add_prop(LIVE_I_SEE_DARK,0);
        TP->remove_prop(EYES_CLOSED,0);
        return 1;
    }
    notify_fail("Otw�rz co ?\n");
    return 0;
}

int
parsknij(string str)
{
    object *cele;
    string *jak;
    int i;

    jak = parse_adverb_with_space(str, "kpi�co", 0);
    if (!jak[0])
    {
        write("Parskasz" + jak[1] + ".\n");
        all("parska" + jak[1] + ".", jak[1]);
        return 1;
    }
    cele = parse_this(jak[0], "'na' %l:" + PL_BIE);

    if(jak[0] ~= "�miechem")
    {
        write("Parskasz �miechem.\n");
        all("parska �miechem.");
        return 1;
    }

    if (sizeof(cele))
    {
        if (sizeof(cele) >= 2)
        {
            actor("Parskasz na", cele, PL_BIE, jak[1] + ".");
            all2act("parska na", cele, PL_BIE, jak[1] + ".", jak[1]);
            NOTIFY_MULTI2(cele, target, "parska na ", PL_BIE, jak[1] + ".", jak[1]);
            return 1;
        }
        else
        {
            actor("Parskasz na", cele, PL_BIE, jak[1] + ".");
            all2act("parska na", cele, PL_BIE, jak[1] + ".", jak[1]);
            target("parska na ciebie" + jak[1] + ".", cele, jak[1]);
            return 1;
        }
    }
    notify_fail("Parsknij [jak] [na kogo] ?\n");
    return 0;
}

int
pierdnij(string str)
{
    if (!str)
    {
        write("Koncentrujesz si� przez chwil�... Achh ! Co za ulga...\n");
        allbb("zamyka oczy i koncentruje si�.");
        tell_room(environment(TP), "Czujesz nagle potworny wr�cz "
                + "smr�d. Chyba kto� pu�ci� tu b�ka...\n");
        return 1;
    }
    notify_fail("Pierdnij?\n");
    return 0;
}
// 1234
int
pocaluj(string str)
{
    object *cele;
    string *jak, gdzie, str2;

    notify_fail("Poca�uj kogo [jak] [gdzie] ?\n");

    if (!str) return 0;
    if (sscanf(str, "%s w %s", str2, gdzie) == 2) {
        switch (gdzie) {
            case "czo�o": gdzie = "czo�o"; break;
            case "cz�ko": gdzie = "cz�ko"; break;
            case "d�o�":  gdzie = "d�o�"; break;
            case "r�k�":
            case "r�czk�": gdzie = "r�k�"; break;
            case "szyj�": gdzie = "szyj�"; break;
			case "ucho":
			case "uszko": gdzie = "ucho"; break;
            case "nos":
			case "kark":
			case "nadgarstek":
            case "usta":
            case "policzek":	break;
            default: return 0;
        }
        jak = parse_adverb_with_space(str2, "czule", 1);
        cele = parse_this(jak[0], "%l:" + PL_BIE);
        if (sizeof(cele))
        {
            CHECK_MULTI_TARGETS("ca�owa� jednocze�nie", PL_DOP);
            actor("Ca�ujesz" + jak[1], cele, PL_BIE, " w " + gdzie + ".");
            all2actbb("ca�uje" + jak[1], cele, PL_BIE,
                " w " + gdzie + ".", jak[1]);
            target("ca�uje ci�" + jak[1] + " w " + gdzie + ".", cele,
                jak[1]);
            return 1;
        }
        return 0;
    }

    jak = parse_adverb_with_space(str, "gor�co", 1);
    cele = parse_this(jak[0], "%l:" + PL_BIE);
    if (sizeof(cele))
    {
        CHECK_MULTI_TARGETS("ca�owa� jednocze�nie", PL_DOP);
        actor("Ca�ujesz" + jak[1], cele, PL_BIE);
        all2actbb("ca�uje" + jak[1], cele, PL_BIE, ".", jak[1]);
        target("ca�uje ci�" + jak[1] + ".", cele, jak[1]);
        return 1;
    }
    return 0;
}

int
pociagnij(string str)
{
    string *jak;

    notify_fail("Poci�gnij nosem [jak] ?\n");
    if (!str) return 0;

    jak = parse_adverb_with_space(str, "smutno", 1);
    if (jak[0] != "nosem") return 0;

    if (jak[1] != NO_ADVERB_WITH_SPACE)
    {
        SOULDESC("poci�ga nosem" + jak[1]);
        write("Poci�gasz nosem" + jak[1] + ".\n");
        allbb("poci�ga nosem" + jak[1] + ".", jak[1]);
        return 1;
    }
    return 0;
}

int
pociesz(string str)
{
    object *cele;

    CHECK_MOUTH_BLOCKED;

    cele = parse_this(str, "%l:" + PL_BIE);
    if (sizeof(cele))
    {
        CHECK_MULTI_TARGETS("pociesza� jednocze�nie", PL_DOP);

#if 0        /* Nie skonczone jeszcze */
        if (TP->query_gender() == G_FEMALE ||
            cele[0]->query_gender() == G_FEMALE)
        {
            actor("Przytulasz", cele, PL_BIE, " pocieszaj�c "
                + cele[0]->koncowka("go", "j�") + " w strapieniu.\n");
        }
#endif 0
        actor("Pocieszasz", cele, PL_BIE, " na miar� swych skromnych "
            + "mo�liwo�ci.");
        all2actbb("pociesza", cele, PL_BIE, " na miar� swych skromnych "
                + "mo�liwo�ci.");
        target("pociesza ci� na miar� swych skromnych mo�liwo�ci.", cele);
        return 1;
    }
    notify_fail("Pociesz kogo?\n");
    return 0;
}

int
podaj(string str)
{
    object *cele;

    cele = parse_this(str, "'r�k�' / 'd�o�' %l:" + PL_CEL);

    if(!HAS_FREE_HAND(TP))
    {
       write("Musisz mie� przynajmniej jedn� d�o� woln�, aby m�c to zrobi�.\n");
       return 1;
    }

    if(sizeof(filter(cele, &->is_humanoid())) == 1)
    {
        CHECK_MULTI_TARGETS("wymienia� jednocze�nie u�cisku d�oni z", PL_NAR);

        actor("Wymieniasz u�cisk d�oni z", cele, PL_NAR);
        all2actbb("wymienia u�cisk d�oni z", cele, PL_NAR);
        target("wymienia z tob� u�cisk d�oni.", cele);
        return 1;
    }
    else
    {
       write("Nie bardzo wiesz jak si� do tego zabra�.\n");
       return 1;
    }

    notify_fail("Podaj r�k� komu?\n");
    return 0;
}

int
podlub(string str)
{
    if (!str)
        str = "w nosie";

    if(!HAS_FREE_HAND(TP))
    {
       write("Musisz mie� przynajmniej jedn� d�o� woln�, aby m�c to zrobi�.\n");
       return 1;
    }

    switch (str)
    {
        case "w nosie":
            SOULDESC("d�ubie w nosie");
            write("D�ubiesz w nosie.\n");
            allbb("d�ubie w nosie.");
            return 1;
        case "w uchu":
            SOULDESC("d�ubie sobie w uchu");
            write("D�ubiesz sobie w uchu.\n");
            allbb("d�ubie sobie w uchu.");
            return 1;
    }
    notify_fail("Pod�ub [w czym] ?\n");
    return 0;
}

int
podrap(string str)
{
    if(!HAS_FREE_HAND(TP))
    {
       write("Musisz mie� przynajmniej jedn� d�o� woln�, aby m�c to zrobi�.\n");
       return 1;
    }

    switch (str)
    {
        case "si�":
        case "si� po g�owie":
        case "si� w g�ow�":
            write("Drapiesz si� po g�owie.\n");
            allbb("drapie si� po g�owie.");
            return 1;
        case "si� po nosie":
        case "si� w nos":
            write("Drapiesz si� w nos.\n");
            allbb("drapie si� w nos.");
            return 1;
        case "si� w ucho":
            write("Drapiesz si� w ucho.\n");
            allbb("drapie si� w ucho.");
            return 1;
        case "si� za uchem":
            write("Drapiesz si� za uchem.\n");
            allbb("drapie si� za uchem.");
            return 1;
        case "si� po brzuchu":
        case "si� w brzuch":
            write("Drapiesz si� po brzuchu.\n");
            allbb("drapie si� po brzuchu.");
            return 1;
        case "si� po zadku":
        case "si� w zadek":
        case "si� po ty�ku":
        case "si� w ty�ek":
        case "si� po po�ladku":
        case "si� w po�ladek":
        case "si� po po�ladkach":
        case "si� w po�ladki":
        case "si� po dupie":
        case "si� w dup�":
            write("Drapiesz si� w zadek.\n");
            allbb("drapie si� w zadek.");
            return 1;
        case "si� po jajach":
        case "si� w jaja":
            if (TP->query_gender() == G_MALE)
            {
                write("Drapiesz si� po jajach.\n");
                allbb("przysiada lekko, rozkracza si� i drapie pomi�dzy "
                    + "nogami, nie bacz�c na twoj� obecno��.");
                return 1;
            }
            write("Ech, chcia�o by si�... Ale ich nie masz!\n");
            return 1;
        case "si� po plecach":
        case "si� w plecy":
            switch (TP->query_race())
            {
                case "cz�owiek":
                case "elf":
				case "p�elf":
                    write("Drapiesz si� po plecach.\n");
                    allbb("drapie si� po plecach.");
                    return 1;
                case "krasnolud":
                case "nizio�ek":
                case "gnom":
                    write("Usi�ujesz podrapa� si� po plecach, ale masz chyba "
                        + "za kr�tkie r�ce.\n");
                    allbb("usi�uje podrapa� si� po plecach, ale ma chyba za "
                        + "kr�tkie r�ce.");
                    return 1;
                default:
                    if (TP->query_race())
                    {
                        write(BUG_FAIL("query_race() = "
                                     + TP->query_race()));
                        return 1;
                    }
                    write("Drapiesz si� po plecach.\n");
                    allbb("drapie si� po plecach.");
                    return 1;
            }
    }
    notify_fail("Podrap si� [gdzie] ?\n");
    return 0;
}

int
podrepcz(string str)
{
    if (!str || str == "w miejscu")
    {
        write("Drepczesz sobie w miejscu.\n");
        allbb("drepcze sobie w miejscu.");
        return 1;
    }
    notify_fail("Podrepcz [w miejscu] ?\n");
    return 0;
}

int
podskocz(string str)
{
    string jak;

    if(TP->query_fatigue() <= 2)
    {
        write("Jeste� na to zbyt zm�czony.\n");
        return 1;
    }

    jak = check_adverb_with_space(str, "weso�o");
    if (jak != NO_ADVERB_WITH_SPACE)
    {
        write("Podskakujesz" + jak + ".\n");
        allbb("podskakuje" + jak + ".", jak);
        //Skoro sprawdzamy zm�czenie to we�my je graczowi. (krun)
        TP->add_fatigue(2);
        return 1;
    }
    notify_fail("Podskocz [jak] ?\n");
    return 0;
}

int
podziekuj(string str)
{
    object *cele;
    string *jak;
    int i;

    CHECK_MOUTH_BLOCKED;

    jak = parse_adverb_with_space(str, "wylewnie", 1, ({"k�tem oka"}));
    cele = parse_this(jak[0], "%l:" + PL_CEL);
    if (sizeof(cele))
    {
        if (sizeof(cele) >= 2)
        {
            actor("Dzi�kujesz" + jak[1], cele, PL_CEL);
            all2actbb("dzi�kuje" + jak[1], cele, PL_CEL, ".", jak[1]);
            NOTIFY_MULTI2(cele, target, "dzi�kuje ", PL_CEL, jak[1] + ".", jak[1]);
            return 1;
        }
        else
        {
            actor("Dzi�kujesz" + jak[1], cele, PL_CEL);
            all2actbb("dzi�kuje" + jak[1], cele, PL_CEL, ".", jak[1]);
            target("dzi�kuje ci" + jak[1] + ".", cele, jak[1]);
            return 1;
        }
    }
    notify_fail("Podzi�kuj komu [jak] ?\n");
    return 0;
}

int
poglaszcz(string str)
{
    object *cele;
    string *jak;
    string *words;
    string gdzie = "";

    if(!HAS_FREE_HAND(TP))
    {
       write("Musisz mie� przynajmniej jedn� d�o� woln�, aby m�c to zrobi�.\n");
       return 1;
    }

    if (!str)
    {
        notify_fail(capitalize(query_verb()) + " kogo [jak] [gdzie] ?\n");
        return 0;
    }

    words = explode(str, " ");
    if (sizeof(words) > 2)
        switch (implode(words[-2..], " "))
        {
            case "po twarzy":
            case "po policzku":
            case "po policzkach":
            case "w policzek":
                gdzie = " po policzku";
                str = implode(words[..-3], " ");
                break;
            case "po w�osach":
            case "po g�owie":
            case "po czaszce":
            case "po �ysinie":
                gdzie = 0;
                str = implode(words[..-3], " ");
                break;
            case "po po^sladku":
            case "w po^sladek":
                gdzie = " po po^sladku";
                str = implode(words[..-3], " ");
                break;
            case "po plecach":
                gdzie = " po plecach";
                str = implode(words[..-3], " ");
                break;
            case "po ramieniu":
            case "po ramionach":
            case "w rami�":
                gdzie = " po ramieniu";
                str = implode(words[..-3], " ");
                break;
            case "po d�oni":
            case "po d�oniach":
            case "po r�ce":
            case "po r�kach":
                gdzie = " po d�oni";
                str = implode(words[..-3], " ");
                break;
        }

    jak = parse_adverb_with_space(str, NO_DEFAULT_ADVERB, 1, ({"k�tem oka"}));
    cele = parse_this(jak[0], "%l:" + PL_BIE);
    if (sizeof(cele))
    {
        CHECK_MULTI_TARGETS("g�aska� jednocze�nie", PL_DOP);

        if((cele[0]->is_humanoid()))
        {
            if (jak[1] == NO_DEFAULT_ADVERB_WITH_SPACE)
                jak[1] = ADD_SPACE_TO_ADVERB("delikatnie");
            if (!gdzie)
                gdzie = IS_BALD(cele[0]) ? " po �ysej czaszce" :
                                           " po w�osach";
        }
        else
        {
            if (jak[1] == NO_DEFAULT_ADVERB_WITH_SPACE)
                jak[1] = ADD_SPACE_TO_ADVERB("powoli");
            gdzie = "";
        }
        actor("G�aszczesz" + jak[1], cele, PL_BIE, gdzie + ".");
        all2actbb("g�aszcze" + jak[1], cele, PL_BIE, gdzie + ".", jak[1]);
        target("g�aszcze ci�" + jak[1] + gdzie + ".", cele, jak[1]);
        return 1;
    }
    notify_fail(capitalize(query_verb()) + " kogo [jak] [gdzie] ?\n");
    return 0;
}

int
pogratuluj(string str)
{
    object *cele;
    string *jak;
    int i;

    CHECK_MOUTH_BLOCKED;

    jak = parse_adverb_with_space(str, "z uznaniem", 1, ({"k�tem oka"}));
    cele = parse_this(jak[0], "%l:" + PL_CEL);
    if (sizeof(cele))
    {
        if (sizeof(cele) >= 2)
        {
            actor("Gratulujesz" + jak[1], cele, PL_CEL);
            all2actbb("gratuluje" + jak[1], cele, PL_CEL, ".", jak[1]);
            NOTIFY_MULTI2(cele, target, "gratuluje ", PL_CEL, jak[1] + ".", jak[1]);
            return 1;
        }
        else
        {
            actor("Gratulujesz" + jak[1], cele, PL_CEL);
            all2actbb("gratuluje" + jak[1], cele, PL_CEL, ".", jak[1]);
            target("gratuluje ci" + jak[1] + ".", cele, jak[1]);
            return 1;
        }
    }
    notify_fail("Pogratuluj komu [jak] ?\n");
    return 0;
}

int
pokaz(string str)
{
    NF("Co komu chcesz pokaza�?\n");

    if(!str)
        return 0;
    else if(str ~= "j�zyk")
        return jezyk(0);
    else if(sscanf(str, "j�zyk %s", str) == 1)
        return jezyk(str);

    return 0;
}

int
pokiwaj(string str)
{
    object *cele;
    string *jak;
    int i;

    if (str ~= "g�ow�")
        str = 0;
    else if (str)
        sscanf(str, "g�ow� %s", str);

    jak = parse_adverb_with_space(str, "ze zrozumieniem", 0, ({"k�tem oka"}));
    if (!jak[0])
    {
        write("Kiwasz g�ow�" + jak[1] + ".\n");
        allbb("kiwa g�ow�" + jak[1] + ".", jak[1]);
        return 1;
    }
    cele = parse_this(jak[0], "'do' %l:" + PL_DOP);
    if (sizeof(cele))
    {
        if (sizeof(cele) >= 2)
        {
            actor("Kiwasz do", cele, PL_DOP, " g�ow�" + jak[1] + ".");
            all2actbb("kiwa do", cele, PL_DOP, " g�ow�" + jak[1] + ".", jak[1]);
            NOTIFY_MULTI2(cele, target, "kiwa do ", PL_DOP, " g�ow�" + jak[1] + ".", jak[1]);
            return 1;
        }
        else
        {
            actor("Kiwasz do", cele, PL_DOP, " g�ow�" + jak[1] + ".");
            all2actbb("kiwa do", cele, PL_DOP, " g�ow�" + jak[1] + ".", jak[1]);
            targetbb("kiwa do ciebie g�ow�" + jak[1] + ".", cele, jak[1]);
            return 1;
        }
    }
    notify_fail("Pokiwaj [g�ow�] [jak] [do kogo] ?\n");
    return 0;
}

int
poklep(string str)
{
    object *cele;
    string *jak, gdzie, str2;

    notify_fail(capitalize(query_verb()) + " kogo [jak] [gdzie] ?\n");

    if(!HAS_FREE_HAND(TP))
    {
       write("Musisz mie� przynajmniej jedn� d�o� woln�, aby m�c to zrobi�.\n");
       return 1;
    }

    if (!str)
        return 0;

    if (sscanf(str, "%s w %s", str2, gdzie) == 2)
    {
        switch (gdzie)
        {
                case "kark":        gdzie = "kark"; break;
                case "rami�":       gdzie = "rami�"; break;
                case "potylic�":    gdzie = "potylic�"; break;
                case "po�ladek":    gdzie = "po�ladek"; break;
                case "ty�ek":       gdzie = "po�ladek"; break;
                default: return 0;
        }

        if(gdzie~="rami�")
          jak = parse_adverb_with_space(str2, "przyjacielsko", 1);
        if(gdzie~="potylic�")
          jak = parse_adverb_with_space(str2, "karc�co", 1);
        if(gdzie~="po�ladek" || gdzie~="ty�ek")
          jak = parse_adverb_with_space(str2, "bezceremonialnie", 1);
        if(gdzie~="kark")
          jak = parse_adverb_with_space(str2, "karc�co", 1);

        cele = parse_this(jak[0], "%l:" + PL_BIE);

        if (sizeof(filter(cele, &->is_humanoid())) == 1)
        {
            CHECK_MULTI_TARGETS("klepa� jednocze�nie", PL_DOP);
            actor("Klepiesz" + jak[1], cele, PL_BIE, " w " + gdzie + ".");
            all2actbb("klepie" + jak[1], cele, PL_BIE, " w " + gdzie + ".",
                      jak[1]);
            target("klepie ci�" + jak[1] + " w " + gdzie + ".", cele, jak[1]);

            return 1;
        }
        else
        {
            write("Nie bardzo wiesz jak si� do tego zabra�.\n");
            return 1;
        }

        return 0;

    }
    else if(sscanf(str, "%s po %s", str2, gdzie) == 2)
    {
        switch (gdzie)
        {
            case "brzuchu":     gdzie = "brzuchu"; break;
            case "policzku":    gdzie = "policzku"; break;
            case "ramieniu":    gdzie = "ramieniu"; break;
            case "udzie":       gdzie = "udzie"; break;

            default: return 0;
        }

        if(gdzie=="ramieniu")
          jak = parse_adverb_with_space(str2, "przyjacielsko", 1, ({"k�tem oka"}));
        if(gdzie=="udzie")
          jak = parse_adverb_with_space(str2, "energicznie", 1, ({"k�tem oka"}));
        if(gdzie=="policzku")
          jak = parse_adverb_with_space(str2, "lekko", 1, ({"k�tem oka"}));
        if(gdzie=="brzuchu")
          jak = parse_adverb_with_space(str2, "lekko", 1, ({"k�tem oka"}));

        if(jak[0] ~= "si�" && ( gdzie == "brzuchu" || gdzie == "udzie" ))
        {
            write("Klepiesz si�"+jak[1]+" po "+gdzie+".\n");
            allbb("klepie si�"+jak[1]+" po "+gdzie+".");

            return 1;
        }
        else
        {
            cele = parse_this(jak[0], "%l:" + PL_BIE);

            if(sizeof(filter(cele, &->is_humanoid())) == 1)
            {
                CHECK_MULTI_TARGETS("klepa� jednocze�nie", PL_DOP);
                actor("Klepiesz" + jak[1], cele, PL_BIE, " po " + gdzie + ".");
                all2actbb("klepie" + jak[1], cele, PL_BIE,
                    " po " + gdzie + ".", jak[1]);
                target("klepie ci�" + jak[1] + " po " + gdzie + ".", cele,
                jak[1]);

                return 1;
            }
            else
            {
                write("Nie bardzo wiesz jak si� do tego zabra�.\n");
                return 1;
            }
        }
    }

    return 0;
}

int
pokrec(string str)
{
    string *jak;
    jak = parse_adverb_with_space(str, "przecz�co", 1, ({"k�tem oka"}));
    if (!str || jak[0] ~= "g�ow�" || jak[0] == 0)
    {
        write("Kr�cisz g�ow�" + jak[1] + ".\n");
        allbb("kr�ci g�ow�" + jak[1] + ".", jak[1]);
        return 1;
    }
    if (str ~= "si�")
    {
        {
        write("Kr�cisz si� w k�ko.\n");
        allbb("kr�ci si� w k�ko.");
        return 1;
        }
        notify_fail("Pokr�� si�?\n");
        return 0;
    }
    notify_fail("Pokr�� [g�ow�] [jak] ?\n");
    return 0;
}

int
polaskocz(string str)
{
    object *cele;

    if(!HAS_FREE_HAND(TP))
    {
       write("Musisz mie� przynajmniej jedn� d�o� woln�, aby m�c to zrobi�.\n");
       return 1;
    }

    if(sizeof(cele = parse_this(str, "%l:" + PL_BIE)))
    {
        CHECK_MULTI_TARGETS("�askota� jednocze�nie", PL_BIE);

        if(!cele[0]->is_humanoid())
        {
            write("Nie bardzo wiesz jak si� do tego zabra�.\n");
            return 1;
        }

        actor("Z �obuzerskim u�miechem na ustach �askoczesz", cele, PL_BIE, ".");
        all2actbb("z �obuzerskim u�miechem na ustach �askocze", cele, PL_BIE, ".");
        targetbb("�askocze ci� z �obuzerskim u�miechem na ustach.", cele);
        return 1;
    }

    notify_fail("Po�askocz kogo?\n");
    return 0;
}

/*
    object *cele;
    string *jak;
    string gdzie;
    int    i;
                        Eee... nie zadziala. ('word', [word])
    cele = parse_this(str, "%l 'w stope' / 'w stopy' / 'pod stopami' / "
                    + "'w piete' / 'w piety' / 'pod pacha' / 'pod pachami' / "
                    + "'w szyje' / 'w podbrodek' / 'w policzek' / "
                    + "'w brzuch' / 'po brzuchu' / 'w bok' / 'po boku'");
    if (sizeof(cele))
    {
        CHECK_MULTI_TARGETS("laskotac jednoczesnie", PL_DOP);

        jak = explode(str, " ");
        switch(gdzie = jak[sizeof(jak) - 1])
        {
            case "szyje":
            case "podbrodek":
            case "policzek":
                cele->add_prop(LIVE_S_SOULEXTRA,
                               "usmiecha sie z rozbawieniem");
                if (sizeof(cele) == 1)
                    actor("Laskoczesz", cele, " w " + gdzie + ", a "
                        + query_zaimek(cele[0]) + "usmiecha sie z ((sizeof(cele) == 1) ?
                    (capitalize(oblist[0]->query_pronoun()) + " smiles") :
                    "They smile") + " sweetly in return.");
            all2act("tickles", oblist, " under the chin. " +
                ((sizeof(oblist) == 1) ? "The latter smiles" :
                    "They smile") + " sweetly in return.");
            return 1;

        case "feet":
        case "foot":
            oblist->add_prop(LIVE_S_SOULEXTRA, "laughing uncontrollably");
            target(" tickles you under your " + location + ". " +
                "You fall down, laughing uncontrollably.", oblist);
            actor("You tickle", oblist, " under the " + location + ". " +
                ((sizeof(oblist) == 1) ?
                    (capitalize(oblist[0]->query_pronoun()) + " falls") :
                    "They fall") + " down, laughing uncontrollably.");
            all2act("tickles", oblist, " under the " + location + ". " +
                ((sizeof(oblist) == 1) ? "The latter falls" :
                    "They fall") + " down, laughing uncontrollably.");
            return 1;

        case "abdomen":
        case "belly":
        case "side":
            oblist->add_prop(LIVE_S_SOULEXTRA, "giggling merrily");
            target(" tickles you in your " + location + ". " +
                "You start giggling merrily.", oblist);
            actor("You tickle", oblist, " in the " + location + ". " +
                ((sizeof(oblist) == 1) ?
                    (capitalize(oblist[0]->query_pronoun()) + " starts") :
                    "They start") + " giggling merrily.");
            all2act("tickles", oblist, " in the " + location + ". " +
                ((sizeof(oblist) == 1) ? "The latter starts" :
                    "They start") + " giggling merrily.");
            return 1;

        default:
            notify_fail("Tickle whom [where / how]? Rather... this should " +
                "not happen. Please make a sysbugreport about this.\n");
            return 0;
        }

        return 1;
    }

    how = parse_adverb_with_space(str, "playfully", 1);

    oblist = parse_this(how[0], "[the] %l");

    if (!sizeof(oblist))
    {
        notify_fail("Tickle whom [how / where]?\n");
        return 0;
    }

    oblist->add_prop(LIVE_S_SOULEXTRA, "laughing");
    actor("You tickle", oblist, how[1] + ". " +
        ((sizeof(oblist) == 1) ?
            (capitalize(oblist[0]->query_pronoun()) + " falls") :
            "They fall") + " down laughing, rolling over in an attempt to " +
            "evade your tickling fingers.");
    all2act("tickles", oblist, how[1] + ". " +
        ((sizeof(oblist) == 1) ? "The latter falls" :
            "They fall") + " down laughing, rolling over in an attempt to " +
            "evade the tickling fingers.", how[1]);
    target(" tickles you" + how[1] + ". You fall down laughing and roll " +
        "over in an attempt to evade those tickling fingers.", oblist,
        how[1]);

    return 1;
}*/

int
pomachaj(string str)
{
    string czym = " r�kami.";
    object *cele;
    string *jak;
    int i;

    if (str)
    {
        string *words = explode(str, " ");

        switch (words[0])
        {
            case "r�k�":
            case "d�oni�":
	        if(!HAS_FREE_HAND(TP))
                {
                  write("Musisz mie� przynajmniej jedn� d�o� woln�, aby m�c to zrobi�.\n");
                  return 1;
                }
                czym = " r�k�.";
            case "r�kami":
            case "d�o�mi":
	        if(!HAS_FREE_HANDS(TP))
                {
                  write("Musisz mie� obie d�onie wolne, aby m�c to zrobi�.\n");
                  return 1;
                }
                str = (sizeof(words) == 1 ? 0 : implode(words[1..], " "));
        }
    }

    jak = parse_adverb_with_space(str, NO_DEFAULT_ADVERB, 0, ({"k�tem oka"}));
    if (!jak[0])
    {
        if (jak[1] == NO_DEFAULT_ADVERB_WITH_SPACE)
            jak[1] = ADD_SPACE_TO_ADVERB("weso�o");
        write("Machasz" + jak[1] + czym + "\n");
        allbb("macha" + jak[1] + czym, jak[1]);
        return 1;
    }
    cele = parse_this(jak[0], "'do' %l:" + PL_DOP);
    if (sizeof(cele))
    {
        if (jak[1] == NO_DEFAULT_ADVERB_WITH_SPACE)
            jak[1] = ADD_SPACE_TO_ADVERB("weso�o");
        if (sizeof(cele) >= 2)
        {
            actor("Machasz do", cele, PL_DOP, jak[1] + ".");
            all2actbb("macha do", cele, PL_DOP, jak[1] + ".", jak[1]);
            NOTIFY_MULTI2(cele, target, "macha do ", PL_DOP, jak[1] + ".", jak[1]);
            return 1;
        }
        else
        {
            actor("Machasz do", cele, PL_DOP, jak[1] + ".");
            all2actbb("macha do", cele, PL_DOP, jak[1] + ".", jak[1]);
            target("macha do ciebie" + jak[1] + ".", cele, jak[1]);
            return 1;
        }
    }
    cele = parse_this(jak[0], "'na' %l:" + PL_DOP);

    if(!HAS_FREE_HAND(TP))
    {
       write("Musisz mie� przynajmniej jedn� d�o� woln�, aby m�c to zrobi�.\n");
       return 1;
    }

    if (sizeof(cele))
    {
        if (jak[1] == NO_DEFAULT_ADVERB_WITH_SPACE)
            jak[1] = ADD_SPACE_TO_ADVERB("zach�caj�co");
        if (sizeof(cele) >= 2)
        {
            actor("Machasz na", cele, PL_DOP, jak[1] + ".");
            all2actbb("macha na", cele, PL_DOP, jak[1] + ".", jak[1]);
            NOTIFY_MULTI2(cele, target, "macha na ", PL_DOP, jak[1] + ".", jak[1]);
            return 1;
        }
        else
        {
            actor("Machasz na", cele, PL_DOP, jak[1] + ".");
            all2actbb("macha na", cele, PL_DOP, jak[1] + ".", jak[1]);
            target("macha na ciebie" + jak[1] + ".", cele, jak[1]);
            return 1;
        }
    }
    notify_fail("Pomachaj [czym] [jak] [do kogo / na kogo] ?\n");
    return 0;
}

int
popatrz(string str)
{
    object *cele;
    string *jak;
    int i;

    jak = parse_adverb_with_space(str, NO_DEFAULT_ADVERB, 0);

    if(!CAN_SEE_IN_ROOM(TP))
    {
       write("Ciemno��...Ciemno�� widzisz!\n");
       return 1;
    }

    if (!jak[0])
    {
        if (jak[1] == NO_DEFAULT_ADVERB_WITH_SPACE)
            jak[1] = ADD_SPACE_TO_ADVERB("nieobecnym wzrokiem");
        write("Spogl�dasz gdzie�" + jak[1] + ".\n");
        allbb("spogl�da gdzie�" + jak[1] + ".", jak[1]);
        return 1;
    }
    if (jak[0] == "na siebie")
    {
        if (jak[1] == NO_DEFAULT_ADVERB_WITH_SPACE)
            jak[1] = ADD_SPACE_TO_ADVERB("uwa�nie");
        write("Spogl�dasz na siebie" + jak[1] + ".\n");
        allbb("spogl�da na siebie" + jak[1] + ".", jak[1]);

        return 1;
    }
    cele = parse_this(jak[0], "'na' %l:" + PL_BIE);
    if (sizeof(cele))
    {
        if (jak[1] == NO_DEFAULT_ADVERB_WITH_SPACE)
            jak[1] = ADD_SPACE_TO_ADVERB("z zainteresowaniem");
        if (sizeof(cele) > 1)
        {
            actor("Spogl�dasz na", cele, PL_BIE, jak[1] + ".");
            all2actbb("spogl�da na", cele, PL_BIE, jak[1] + ".", jak[1]);
            NOTIFY_MULTI2(cele, target, "spogl�da na ", PL_BIE, jak[1] + ".", jak[1]);
            return 1;
        }
        else
        {
            actor("Spogl�dasz na", cele, PL_BIE, jak[1] + ".");
            all2actbb("spogl�da na", cele, PL_BIE, jak[1] + ".", jak[1]);
            targetbb("spogl�da na ciebie" + jak[1] + ".", cele, jak[1]);
            return 1;
        }
    }

    cele = parse_this(jak[0], "%l: 'w oczy'" + PL_CEL);
    if (sizeof(cele))
    {
        if (jak[1] == NO_DEFAULT_ADVERB_WITH_SPACE)
            jak[1] = "";

        actor("Patrzysz ", cele, PL_DOP, jak[1] + " w oczy.");
        all2actbb("patrzysz", cele, PL_DOP, jak[1] + " w oczy.", jak[1]);
        targetbb("patrzy ci " + jak[1] + " w oczy.", cele, jak[1]);

        foreach(object cel : cele)
            write(cel->query_Imie() + " ma " + (cel->query_kolor_oczu()[2][0..-4]) + " oczu .\n");

        return 1;
    }

    cele = parse_this(jak[0], "'na' %i:" + PL_BIE);
    if (sizeof(cele))
    {
        if (jak[1] == NO_DEFAULT_ADVERB_WITH_SPACE)
            jak[1] = ADD_SPACE_TO_ADVERB("z zainteresowaniem");
        write("Spogl�dasz na " + COMPOSITE_DEAD(cele, PL_BIE) + jak[1]
            + ".\n");
        saybb(QCIMIE(TP, PL_MIA) + " spogl�da na "
            + QCOMPDEAD(PL_BIE) + jak[1] + ".\n");

        return 1;
    }

    if (sscanf(jak[0], "na %s", str))
    {
        i = member_array(str, Skroty) + member_array(str, Kierunki);
        if (i++ != -2)
        {
            if (jak[1] == NO_DEFAULT_ADVERB_WITH_SPACE)
                jak[1] = ADD_SPACE_TO_ADVERB("nieobecnym wzrokiem");
            write("Spogl�dasz" + jak[1] + " na " + Kierunki[i] + ".\n");
            allbb("spogl�da" + jak[1] + " na " + Kierunki[i] + ".", jak[1]);

	    return 1;
        }
        if (environment(TP)->item_id(str) &&
            CAN_SEE_IN_ROOM(TP))
        {
            if (jak[1] == NO_DEFAULT_ADVERB_WITH_SPACE)
                jak[1] = ADD_SPACE_TO_ADVERB("z zainteresowaniem");
            write("Spogl�dasz na " + ENV(TP)->item_name(str) + jak[1] + ".\n");
            allbb("spogl�da na " + ENV(TP)->item_name(str) + jak[1] + ".", jak[1]);
            return 1;
        }
    }

    notify_fail("Popatrz [jak] [na kogo / na co] ?\n");
    return 0;
}

int
popraw(string str)
{
  string *jak;

   if(!HAS_FREE_HAND(TP))
    {
       write("Musisz mie� przynajmniej jedn� d�o� woln�, aby m�c to zrobi�.\n");
       return 1;
    }

  notify_fail("Popraw co [jak] ?\n");

  jak = parse_adverb_with_space(str, "od niechcenia", 1, ({"k�tem oka"}));

  if(jak[0]~="w�osy"||jak[0]~="sobie w�osy"||jak[0]~="swoje w�osy"||
     jak[0]~="fryzur�"||jak[0]~="sobie fryzur�")
  {
     if(TP->query_dlugosc_wlosow() < 3.0)
     {
       write("W�osy? Jakie w�osy? Przecie� ty nie masz w�os�w!\n");
       return 1;
     }
     if(sizeof(TP->query_armour(TS_HEAD)))
     {
       write("Masz co� na g�owie.\n");
       return 1;
     }

     write("Poprawiasz"+jak[1]+" swoje "+TP->opis_dlugosci_wlosow()+
          ", "+TP->query_opis_fryzury()+" w�osy.\n");
     allbb("poprawia"+jak[1]+" swoje "+TP->opis_dlugosci_wlosow()+
          ", "+TP->query_opis_fryzury()+" w�osy.");
     return 1;
  }
  //TODO: kiedy� tu mo�naby da� te� "popraw spodnie" czy innego ciucha
  //noszonego na sobie ;) V.
}

int
popukaj(string str)
{
    if(!HAS_FREE_HAND(TP))
    {
       write("Musisz mie� przynajmniej jedn� d�o� woln�, aby m�c to zrobi�.\n");
       return 1;
    }

    if (str ~= "si�" || str ~= "si� w czo�o" || str ~= "si� w g�ow�")
    {
        write("Pukasz si� niedwuznacznym gestem w czo�o.\n");
        allbb("puka si� niedwuznacznym gestem w czo�o.");
        return 1;
    }
    notify_fail("Popukaj si� [gdzie] ?\n");
    return 0;
}

int
potrzasnij(string str)
{
    object *cele;
    string *jak;
    int i;

    if(str~="w�osami")
    {
       if(TP->query_dlugosc_wlosow() < 10.0)
       {
          write("Masz na to zbyt kr�tkie w�osy.\n");
          return 1;
       }
       if(sizeof(TP->query_armour(TS_HEAD)))
       {
          write("Masz co� na g�owie.\n");
          return 1;
       }


       write("Potrz�sasz g�ow� energicznie, pozwalaj�c by twoje "+
         TP->opis_dlugosci_wlosow()+", "+
         TP->query_opis_fryzury()+" w�osy "+
         "zawirowa�y wok�.\n");
       allbb("potrz�sa g�ow� energicznie, pozwalaj�c by "+
         TP->koncowka("jego","jej")+
         " "+TP->opis_dlugosci_wlosow()+", "+
         TP->query_opis_fryzury()+" w�osy "+
         "zawirowa�y wok�.");
       return 1;
    }

    if (str)
        sscanf(str, "g�ow� %s", str);

    jak = parse_adverb_with_space(str, "przecz�co", 0, ({"k�tem oka"}));
    if (!jak[0])
    {
        SOULDESC("potrz�sa g�ow�" + jak[1]);
        write("Potrz�sasz g�ow�" + jak[1] + ".\n");
        allbb("potrz�sa g�ow�" + jak[1] + ".", jak[1]);
        return 1;
    }
    cele = parse_this(jak[0], "'do' %l:" + PL_DOP);
    if (sizeof(cele))
    {
        if (sizeof(cele) >= 2)
        {
            SOULDESC("potrz�sa g�ow�" + jak[1]);
            actor("Potrz�sasz do", cele, PL_DOP, " g�ow�" + jak[1] + ".");
            all2actbb("potrz�sa do", cele, PL_DOP, " g�ow�" + jak[1] + ".", jak[1]);
            NOTIFY_MULTI2(cele, target, "potrz�sa do ", PL_DOP, jak[1] + " g�ow�.", jak[1]);
            return 1;
        }
        else
        {
            SOULDESC("potrz�sa g�ow�" + jak[1]);
            actor("Potrz�sasz do", cele, PL_DOP, " g�ow�" + jak[1] + ".");
            all2actbb("potrz�sa do", cele, PL_DOP, " g�ow�" + jak[1] + ".", jak[1]);
            targetbb("potrz�sa do ciebie g�ow�" + jak[1] + ".", cele, jak[1]);
            return 1;
        }
    }
    notify_fail("Potrz��nij [czym] [jak] [do kogo] ?\n");
    return 0;
}

int
potrzyj(string str)
{
    string *jak;

    if(!HAS_FREE_HAND(TP))
    {
       write("Musisz mie� przynajmniej jedn� d�o� woln�, aby m�c to zrobi�.\n");
       return 1;
    }

    jak = parse_adverb_with_space(str, "w zamy�leniu", 1, ({"k�tem oka"}));
    if (jak[0] ~= "czo�o")
    {
        write("Pocierasz czo�o" + jak[1] + ".\n");
        allbb("pociera czo�o" + jak[1] + ".", jak[1]);
        return 1;
    }
    if (jak[0] == "policzek")
    {
        write("Pocierasz policzek" + jak[1] + ".\n");
        allbb("pociera policzek" + jak[1] + ".", jak[1]);
        return 1;
    }
    notify_fail("Potrzyj [co] [jak] ?\n");
    return 0;
}

int
potwierdz(string str)
{
    object *cele;
    int i;

    CHECK_MOUTH_BLOCKED;

    if (!str)
    {
        write("Potwierdzasz.\n");
        all("potwierdza.");
        return 1;
    }
    //Poniewa�.... :P Dla potwierdzania s��w "kogo� i kogo�"
   //Potrzebny jest zaimek dzier�awczy, a to jaki� hardkor,
  //Wi�c wypierdolilim to i tyla :P          Vera

/*    cele = parse_this(str, "'s�owa' %l:" + PL_DOP);
    if (sizeof(cele))
    {
        if (sizeof(cele) >= 2)
        {
            actor("Potwierdzasz s�owa", cele, PL_DOP);
            all2act("potwierdza s�owa", cele, PL_DOP);
            NOTIFY_MULTI(cele, target, "potwierdza s�owa ", PL_DOP, ".");
            return 1;
        }
        else
        {
            actor("Potwierdzasz s�owa", cele, PL_DOP);
            all2act("potwierdza s�owa", cele, PL_DOP);
            target("potwierdza twoje s�owa.", cele);
            return 1;
        }
    }
    notify_fail("Potwierd� [czyje s�owa] ?\n");*/
    notify_fail("Potwierd� ?\n");
    return 0;
}

int
powitaj(string str)
{
    object *cele;
    string *jak;
    int i;

    jak = parse_adverb_with_space(str, "serdecznie", 1, ({"k�tem oka"}));
    cele = parse_this(jak[0], "%l:" + PL_BIE);
    if (sizeof(cele))
    {
        if (sizeof(cele) >= 2)
        {
            actor("Witasz" + jak[1], cele, PL_BIE);
            all2actbb("wita" + jak[1], cele, PL_BIE, ".", jak[1]);
            NOTIFY_MULTI2(cele, target, "wita ", PL_BIE, jak[1] + ".", jak[1]);
            return 1;
        }
        else
        {
            actor("Witasz" + jak[1], cele, PL_BIE);
            all2actbb("wita" + jak[1], cele, PL_BIE, ".", jak[1]);
            target("wita ci�" + jak[1] + ".", cele, jak[1]);
            return 1;
        }
    }
    notify_fail(capitalize(query_verb()) + " kogo [jak] ?\n");
    return 0;
}

int
pozegnaj(string str)
{
    object *cele;
    string *jak;
    int i;

    jak = parse_adverb_with_space(str, BLANK_ADVERB, 1, ({"k�tem oka"}));
    cele = parse_this(jak[0], "%l:" + PL_BIE);
    if (sizeof(cele))
    {
        if (sizeof(cele) >= 2)
        {
            actor("�egnasz" + jak[1], cele, PL_BIE);
            all2actbb("�egna" + jak[1], cele, PL_BIE, ".", jak[1]);
            NOTIFY_MULTI2(cele, target, "�egna ", PL_BIE, jak[1] + ".", jak[1]);
            return 1;
        }
        else
        {
            actor("�egnasz" + jak[1], cele, PL_BIE);
            all2actbb("�egna" + jak[1], cele, PL_BIE, ".", jak[1]);
            target("�egna ci�" + jak[1] + ".", cele, jak[1]);
            return 1;
        }
    }
    notify_fail("Po�egnaj kogo [jak] ?\n");
    return 0;
}

int
powachaj(string str)
{
    object *cele;
    int i;

//    cele = parse_this(str, "%l:" + PL_BIE);
//    if (sizeof(cele))
//    {
//
//        actor("W�chasz", cele, PL_BIE, ".");
//        all2act("w�cha", cele, PL_BIE, ".");
//        target("w�cha ci�.", cele);
//        return 1;
//    }
    cele = parse_this(str, "%i:" + PL_BIE);
    if (sizeof(cele))
    {

	/*BO NIE MOZNA WYSWIETLAC EFEKTOW WIELU RZECZY NARAZ. LIL*/


        //if (sizeof(cele) >= 2)
        //{
                /*actor("W�chasz", cele, PL_BIE, ".");
                all2act("w�cha", cele, PL_BIE, ".");
            for (i = 0; i < sizeof(cele); i++)
                    target("w�cha ciebie oraz " +
		      COMPOSITE_LIVE(cele - ({ cele[i] }), PL_BIE) + ".", ({ cele[i] }));
                   cele[0]->smell_effect();
                return 1;*/
		CHECK_MULTI_TARGETS("w�cha� jednocze�nie", PL_DOP);
        //}
        //else
        //{
                actor("W�chasz", cele, PL_BIE, ".");
                all2act("w�cha", cele, PL_BIE, ".");
                target("w�cha ci�.", cele);
                cele[0]->smell_effect();
                return 1;
        //}
    }

    if (str == "siebie")
    {
        write("W�chasz sam" + TP->koncowka("", "a", "e")
               + " siebie.\n");
        allbb("w�cha sam" + TP->koncowka("", "a", "e")
               + " siebie.");
	       TP->smell_effect();
        return 1;
    }
    if (!str)
    {
        write("W�szysz dooko�a.\n");
        allbb("w�szy dooko�a.");
	     environment(TP)->smell_effect();
        return 1;
    }
    notify_fail("Pow�chaj [kogo / co] ?\n");
    return 0;
}

int
prychnij(string str)
{
    string *jak;
    object *cele;
    int i;

    CHECK_MOUTH_BLOCKED;

    jak = parse_adverb_with_space(str, "z rozdra�nieniem", 0, ({"k�tem oka"}));
    if (!jak[0])
    {
        write("Prychasz" + jak[1] + ".\n");
        all("prycha" + jak[1] + ".", jak[1]);
        return 1;
    }
    cele = parse_this(jak[0], "'na' %l:" + PL_BIE);
    if (sizeof(cele))
    {
        if (sizeof(cele) >= 2)
        {
            actor("Prychasz na", cele, PL_BIE, jak[1] + ".");
            all2act("prycha na", cele, PL_BIE, jak[1] + ".", jak[1]);
            NOTIFY_MULTI2(cele, target, "prycha na ", PL_BIE, ".", jak[1]);
            return 1;
        }
        else
        {
            actor("Prychasz na", cele, PL_BIE, jak[1] + ".");
            all2act("prycha na", cele, PL_BIE, jak[1] + ".", jak[1]);
            target("prycha na ciebie" + jak[1] + ".", cele, jak[1]);
            return 1;
        }
    }
    notify_fail("Prychnij [jak] [na kogo] ?\n");
    return 0;
}

int
przebieraj(string str)
{
    string *jak;

    jak = parse_adverb_with_space(str, "nerwowo", 1, ({"k�tem oka"}));
    if (jak[0] == "nogami")
    {
        SOULDESC("przebiera" + jak[1] + " nogami");
        write("Przebierasz" + jak[1] + " nogami.\n");
        allbb("przebiera" + jak[1] + " nogami.", jak[1]);
        return 1;
    }
    notify_fail("Przebieraj nogami [jak] ?\n");
    return 0;
}

int
przeciagnij(string str)
{
    string *jak;

    if (str ~= "si�")
    {
        if (TP->query_gender() == G_FEMALE)
        {
            write("Przeci�gasz si� rozkosznie.\n");
            allbb("przeci�ga si� rozkosznie.",
                  ADD_SPACE_TO_ADVERB("rozkosznie"));
            return 1;
        }
        write("Przeci�gasz si�, a� co� chrupie ci w ko�ciach.\n");
        allbb("przeci�ga si�, a� co� chrupie mu w ko�ciach.");
        return 1;
    }
    jak = parse_adverb_with_space(str, NO_DEFAULT_ADVERB, 1, ({"k�tem oka"}));
    if (jak[0] ~= "si�")
    {
        write("Przeci�gasz si�" + jak[1] + ".\n");
        allbb("przeci�ga si�" + jak[1] + ".", jak[1]);
        return 1;
    }
    notify_fail("Przeci�gnij si� [jak] ?\n");
    return 0;
}

int
przeczesz(string str)
{
   notify_fail("Przeczesz co czym ?\n");

   if(!HAS_FREE_HAND(TP))
    {
       write("Musisz mie� przynajmniej jedn� d�o� woln�, aby m�c to zrobi�.\n");
       return 1;
    }

   if(str ~="w�osy palcami" || str ~="w�osy d�oni�" || str~="w�osy r�k�")
   {
      if(TP->query_dlugosc_wlosow() < 3.0)
      {
	 write("W�osy? Jakie w�osy? Ty nie masz w�os�w!\n");
	 return 1;
      }
      if(sizeof(TP->query_armour(TS_HEAD)))
      {
         write("Masz co� na g�owie.\n");
         return 1;
      }

      write("Przeczesujesz swoje "+TP->opis_dlugosci_wlosow()+", "+
            TP->query_opis_fryzury()+" w�osy ko�c�wkami palc�w.\n");
      allbb("przeczesuje swoje "+TP->opis_dlugosci_wlosow()+", "+
            TP->query_opis_fryzury()+" w�osy ko�c�wkami palc�w.");
      return 1;
   }

}

int
przekrzyw(string str)
{
    string *jak;

    jak = parse_adverb(str, "figlarnie", 0, ({"k�tem oka"}));

    if(jak[1] ~= ".")
        jak[1] = "";
    else
        jak[1] += " ";

    if (jak[0] ~= "g�ow�")
    {
        write("Przekrzywiasz " + jak[1] + "g�ow� na bok.\n");
        allbb(jak[1]+ "przekrzywia g�ow� na bok.", jak[1]);
        return 1;
    }

    notify_fail("Przekrzyw [jak] g�ow�?\n");
    return 0;
}

int
przelknij(string str)
{
    if (!str || str ~= "�lin�" || str ~= "g�o�no �lin�")
    {
        write("Prze�ykasz g�o�no �lin�.\n");
        all("g�o�no prze�yka �lin�.");
        return 1;
    }
    notify_fail("Prze�knij [co] ?\n");
    return 0;
}

int
przepros(string str)
{
    object *cele;
    string *jak;
    int i;

    CHECK_MOUTH_BLOCKED;

    jak = parse_adverb_with_space(str, "ze skruch�", 1, ({"k�tem oka"}));
    cele = parse_this(jak[0], "%l:" + PL_BIE);
    if (sizeof(cele))
    {
        if (sizeof(cele) >= 2)
        {
            actor("Przepraszasz" + jak[1], cele, PL_BIE);
            all2actbb("przeprasza" + jak[1], cele, PL_BIE, ".", jak[1]);
            NOTIFY_MULTI2(cele, target, "przeprasza ", PL_BIE, jak[1] + ".", jak[1]);
            return 1;
        }
        else
        {
            actor("Przepraszasz" + jak[1], cele, PL_BIE);
            all2actbb("przeprasza" + jak[1], cele, PL_BIE, ".", jak[1]);
            target("przeprasza ci�" + jak[1] + ".", cele, jak[1]);
            return 1;
        }
    }
    notify_fail("Przepro� kogo [jak] ?\n");
    return 0;
}

int
przestap(string str)
{
    if (!str || str ~= "z nogi na nog�")
    {
        write("Przest�pujesz z nogi na nog�.\n");
        allbb("przest�puje z nogi na nog�.");
        return 1;
    }
    notify_fail("Przest�p [z nogi na nog�] ?\n");
    return 0;
}

int
przetrzyj(string str)
{
    string *jak;

    jak = parse_adverb_with_space(str, "ze zm�czenia", 0, ({"k�tem oka"}));

    if(!HAS_FREE_HAND(TP))
    {
       write("Musisz mie� przynajmniej jedn� d�o� woln�, aby m�c to zrobi�.\n");
       return 1;
    }

    //if (jak != NO_ADVERB_WITH_SPACE)
    if (jak[0] == "oczy")
    {
        write("Przecierasz oczy" + jak[1] + ".\n");
        allbb("przeciera oczy" + jak[1] + ".", jak[1]);
        return 1;
    }
    notify_fail("Przetrzyj oczy [jak] ?\n");
    return 0;
}

int
przewroc(string str)
{
    if (str == "oczami" || str == "oczyma")
    {
        write("Przewracasz oczami.\n");
        allbb("przewraca oczami.");
        return 1;
    }
    notify_fail("Przewr�� czym ?\n");
    return 0;
}

int
prztyknij(string str)
{
    object *cele;

    cele = parse_this(str, "%l:" + PL_BIE);

    if(!HAS_FREE_HAND(TP))
    {
       write("Musisz mie� przynajmniej jedn� d�o� woln�, aby m�c to zrobi�.\n");
       return 1;
    }

    if(!sizeof(cele))
        cele = parse_this(str, "%l:" + PL_BIE + " 'w' 'nos'");

    if(sizeof(cele))
    {
        CHECK_MULTI_TARGETS("prztyka� jednocze�nie", PL_DOP);

        if(!cele[0]->is_humanoid())
        {
            write("Nie bardzo wiesz jak si� do tego zabra�.\n");
            return 1;
        }

        actor("Dajesz", cele, PL_CEL, " prztyczka w nos.");
        all2act("daje ", cele, PL_CEL, " prztyczka w nos.");
        target("daje ci prztyczka w nos.", cele);
        return 1;
    }

    notify_fail("Prztyknij [kogo] ?\n");
    return 0;
}


int
przytul(string str)
{
    object *cele;
    string *jak;

    jak = parse_adverb_with_space(str, "czule", 1, ({"k�tem oka"}));
    cele = parse_this(jak[0], "%l:" + PL_BIE);
    if (sizeof(cele))
    {
        CHECK_MULTI_TARGETS("przytula� jednocze�nie", PL_DOP);

        actor("Przytulasz" + jak[1], cele, PL_BIE);
        all2actbb("przytula" + jak[1], cele, PL_BIE, ".", jak[1]);
        target("przytula ci�" + jak[1] + ".", cele, jak[1]);
        return 1;
    }
    cele = parse_this(jak[0], "'si�' 'do' %l:" + PL_DOP);
    if (sizeof(cele))
    {
        CHECK_MULTI_TARGETS("przytula� si� jednocze�nie do", PL_DOP);

        actor("Przytulasz si�" + jak[1] + " do", cele, PL_DOP);
        all2actbb("przytula si�" + jak[1] + " do", cele, PL_DOP, ".", jak[1]);
        target("przytula si� do ciebie" + jak[1] + ".", cele, jak[1]);
        return 1;
    }
    notify_fail("Przytul [si� do] kogo [jak] ?\n");
    return 0;
}

int
pstryknij(string str)
{
    string jak;

    jak = check_adverb_with_space(str, "od niechcenia", ({"k�tem oka"}));

    if(!HAS_FREE_HAND(TP))
    {
       write("Musisz mie� przynajmniej jedn� d�o� woln�, aby m�c to zrobi�.\n");
       return 1;
    }

    if (jak != NO_ADVERB_WITH_SPACE)
    {
        write("Pstrykasz" + jak + " palcami.\n");
        allbb("pstryka" + jak + " palcami.", jak);
        return 1;
    }
    notify_fail("Pstryknij [jak] ?\n");
    return 0;
}


int zbeltaj(string str); /* Prototyp. */

int
pusc(string str)        /* I tak sie wypieprzy (A wlasnie ze nie!:P Lil)*/
{
    object *cele;

    if (str ~= "b�ka")
    {
        write("Koncentrujesz si� przez chwil�... Achh ! Co za ulga...\n");
        allbb("zamyka oczy i koncentruje si�.");
        tell_room(environment(TP), "Nagle poczu�e� potworny wr�cz "
                + "smr�d. Chyba kto� pu�ci� tu b�ka...\n");
        return 1;
    }
    if (str ~= "be�ta" || str == "pawia")
        return zbeltaj("si�");

    if (str == "oczko" || str == "oko")
    {
          if(!CAN_SEE_IN_ROOM(TP))
          {
           write("Ciemno��...Ciemno�� widzisz!\n");
           return 1;
          }

        write("Puszczasz dyskretnie oczko.\n");
        allbb("puszcza dyskretnie oczko.");
        return 1;
    }
    if (str && (sscanf(str, "oczko %s", str) || sscanf(str, "oko %s", str)))
    {
        cele = parse_this(str, "'do' %l:" + PL_DOP);
        if (sizeof(cele))
        {
            actor("Puszczasz dyskretnie oczko do", cele, PL_DOP, ".");
            all2actbb("puszcza dyskretnie oczko do", cele, PL_DOP, ".");
            targetbb("puszcza do ciebie dyskretnie oczko.", cele);
            return 1;
        }
        notify_fail("Pu�� oczko [do kogo] ?\n");
        return 0;
    }
    notify_fail("Pu�� co?\n");
    return 0;
}

int
rozejrzyj(string str)
{
    string *jak;

    jak = parse_adverb_with_space(str, "dooko�a z wyrazem zagubienia na "
                                + "twarzy", 1);

    if(!CAN_SEE_IN_ROOM(TP))
    {
       write("Ciemno��...Ciemno�� widzisz!\n");
       return 1;
    }

    if (jak[0] ~= "si�" || jak[0] ~= "si� doko�a" || jak[0] ~= "si� dooko�a")
    {
        SOULDESC("rozgl�da si�" + jak[1]);
        write("Rozgl�dasz si�" + jak[1] + ".\n");
        allbb("rozgl�da si�" + jak[1] + ".", jak[1]);
        return 1;
    }
    notify_fail("Rozejrzyj si� [dooko�a] [jak] ?\n");
    return 0;
}

int
rozetrzyj(string str)
{
    if(!HAS_FREE_HAND(TP))
    {
       write("Musisz mie� przynajmniej jedn� d�o� woln�, aby m�c to zrobi�.\n");
       return 1;
    }


    if (str == "biodro")
    {
        SOULDESC("rozciera obola�e biodro");
        write("Rozcierasz obola�e biodro.\n");
        allbb("rozciera obola�e biodro.");
        return 1;
    }
    if (str == "biodra")
    {
        SOULDESC("rozciera obola�e biodra");
        write("Rozcierasz obola�e biodra.\n");
        allbb("rozciera obola�e biodra.");
        return 1;
    }
    if (str == "kolano")
    {
        SOULDESC("rozciera obola�e kolano");
        write("Rozcierasz obola�e kolano.\n");
        allbb("rozciera obola�e kolano.");
        return 1;
    }
    if (str == "kolana")
    {
        SOULDESC("rozciera obola�e kolana");
        write("Rozcierasz obola�e kolana.\n");
        allbb("rozciera obola�e kolana.");
        return 1;
    }
    if (str ~= "r�ke")
    {
        SOULDESC("rozciera obola�� r�ke");
        write("Rozcierasz obola�� r�ke.\n");
        allbb("rozciera obola�� r�ke.");
        return 1;
    }
    if (str ~= "d�o�")
    {
        SOULDESC("rozciera obola�� d�o�");
        write("Rozcierasz obola�� d�o�.\n");
        allbb("rozciera obola�� d�o�.");
        return 1;
    }
    if (str ~= "po�ladki" || str ~= "dup�" || str ~= "zadek")
    {
        SOULDESC("rozciera obola�e po�ladki");
        write("Rozcierasz obola�e po�ladki.\n");
        allbb("rozciera obola�e po�ladki.");
        return 1;
    }

    notify_fail("Rozetrzyj [co] ?\n");
    return 0;
}

int
rozloz(string str)
{
    string *jak;

    jak = parse_adverb_with_space(str, "bezradnie", 1, ({"k�tem oka"}));

    /*if(!HAS_FREE_HANDS(TP))
    {
       write("Musisz mie� obie d�onie wolne, aby m�c to zrobi�.\n");
       return 1;
    }*/

    if (jak[0] ~= "r�ce")
    {
        write("Rozk�adasz r�ce" + jak[1] + ".\n");
        allbb("rozk�ada r�ce" + jak[1] + ".", jak[1]);
        return 1;
    }
    notify_fail("Roz�� [r�ce] [jak] ?\n");
    return 0;
}

int
rozplacz(string str)
{
    if (str ~= "si�")
    {
        SOULDESC("tonie we �zach");
        write("Wybuchasz p�aczem.\n");
        allbb("wybucha p�aczem.");
        return 1;
    }
    notify_fail("Rozp�acz si�?\n");
    return 0;
}

int
sapnij(string str)
{
    string jak;

    CHECK_MOUTH_BLOCKED;

    jak = check_adverb_with_space(str, "desperacko", ({"k�tem oka"}));
    if (jak != NO_ADVERB_WITH_SPACE)
    {
        write("Sapiesz" + jak + ".\n");
        allbb("sapie" + jak + ".", jak);
        return 1;
    }
    notify_fail("Sapnij [jak] ?\n");
    return 0;
}

int
skin(string str)
{
    object *cele;
    int i;

    if (!str || str ~= "g�ow�")
        return pokiwaj("g�ow� .");

    if (str ~= "d�oni�")
    {
//    notify_fail(capitalize(query_verb()) + " d�oni� na kogo?");
        notify_fail("Ski� d�oni� na kogo?\n");
        return 0;
    }
    if (str ~= "r�k�")
    {
//     notify_fail(capitalize(query_verb()) + " r�k� na kogo?");
        notify_fail("Ski� r�k� na kogo?\n");
        return 0;
    }
    cele = parse_this(str, "'g�ow�' 'na' %l:" + PL_BIE);
    if (sizeof(cele))
    {
        actor("Kiwasz g�ow� na", cele, PL_BIE);
        all2actbb("kiwa g�ow� na", cele, PL_BIE);
        targetbb("kiwa na ciebie g�ow�.", cele);
        return 1;
    }
    cele = parse_this(str, "[d�oni�] / [r�k�] 'na' %l:" + PL_BIE);
    if (sizeof(cele))
    {
        actor("Kiwasz d�oni� na", cele, PL_BIE);
        all2actbb("kiwa d�oni� na", cele, PL_BIE);
        targetbb("kiwa na ciebie d�oni�.", cele);
        return 1;
    }
//   notify_fail(capitalize(query_verb()) + " [czym] [na kogo] ?\n");
   notify_fail("Ski� [czym] [na kogo] ?\n");
    return 0;
}

int
skrzyw(string str)
{
    object *cele;
    string *jak;

    if (str ~= "si�")
    {
        SOULDESC("krzywi si� z niesmakiem");
        write("Krzywisz si� z niesmakiem.\n");
        allbb("krzywi si� z niesmakiem.",
              ADD_SPACE_TO_ADVERB("z niesmakiem"));
        return 1;
    }
    if (str && sscanf(str, "si� %s", str))
    {
        jak = parse_adverb_with_space(str, NO_DEFAULT_ADVERB, 0, ({"k�tem oka"}));
        if (!jak[0])
        {
            SOULDESC("krzywi si�" + jak[1]);
            write("Krzywisz si�" + jak[1] + ".\n");
            allbb("krzywi si�" + jak[1] + ".", jak[1]);
            return 1;
        }
        cele = parse_this(jak[0], "'do' %l:" + PL_DOP);
        if (sizeof(cele))
        {
            if (jak[1] == NO_DEFAULT_ADVERB_WITH_SPACE)
                jak[1] = ADD_SPACE_TO_ADVERB("z�o�liwie");
            SOULDESC("krzywi si�" + jak[1]);
            actor("Krzywisz si� do", cele, PL_DOP, jak[1] + ".");
            all2actbb("krzywi si� do", cele, PL_DOP, jak[1] + ".", jak[1]);
            targetbb("krzywi si� do ciebie" + jak[1] + ".", cele, jak[1]);
            return 1;
        }
        cele = parse_this(jak[0], "'na' %l:" + PL_BIE);
        if (sizeof(cele))
        {
            if (jak[1] == NO_DEFAULT_ADVERB_WITH_SPACE)
                jak[1] = ADD_SPACE_TO_ADVERB("oble�nie");
            SOULDESC("krzywi si�" + jak[1]);
            actor("Krzywisz si� na", cele, PL_BIE, jak[1] + ".");
            all2actbb("krzywi si� na", cele, PL_BIE, jak[1] + ".", jak[1]);
            targetbb("krzywi si� na ciebie" + jak[1] + ".", cele, jak[1]);
            return 1;
        }
    }
    notify_fail(capitalize(query_verb())
              + " si� [jak] [do kogo] / [na kogo] ?\n");
    return 0;
}

int
skul(string str)
{
    if(str ~= "si�")
    {
        write("Kulisz si� trwo�liwie.\n");
        allbb("kuli si� trwo�liwie.");

        return 1;
    }

    notify_fail("Skul si�?\n");
    return 0;
}


int
spanikuj(string str)
{
    object *cele;

    if (!str)
    {
        write("Wpadasz w panik�!\n");
        all("wpada w panik�!");
        return 1;
    }
    cele = parse_this(str, "'przed' %l:" + PL_NAR);
    if (sizeof(cele))
    {
        actor("Wpadasz w panik� na widok", cele, PL_DOP, "!");
        all2act("wpada w panik� na widok", cele, PL_DOP, "!");
        target("wpada w panik� na tw�j widok!", cele);
        return 1;
    }
    notify_fail("Spanikuj [przed kim] ?\n");
    return 0;
}

int
splun(string str)
{
    object *cele;

    if (!str || str ~= "na ziemi�" || str ~= "na pod�og�")
    {
        write("Spluwasz z niesmakiem na ziemi�.\n");
        allbb("spluwa z niesmakiem na ziemi�.");
        return 1;
    }
    if (wildmatch("pod *", str))
    {
        cele = parse_this(str, "'pod' 'nogi' %l:" + PL_CEL);
        if (sizeof(cele))
        {
        CHECK_MULTI_TARGETS("spluwa� jednocze�nie pod nogi", PL_CEL);

            actor("Spluwasz z pogard� pod nogi", cele, PL_CEL);
            all2actbb("spluwa z pogard� pod nogi", cele, PL_CEL, ".");
            target("spluwa ci z pogard� pod nogi.", cele);
            return 1;
        }
        notify_fail("Splu� pod nogi komu?\n");
        return 0;
    }
    notify_fail("Splu� [gdzie] ?\n");
    return 0;
}

int
spoliczkuj(string str)
{
    object *cele;

    cele = parse_this(str, "%l:" + PL_BIE);

    if(!HAS_FREE_HAND(TP))
    {
       write("Musisz mie� przynajmniej jedn� d�o� woln�, aby m�c to zrobi�.\n");
       return 1;
    }

    if(sizeof(filter(cele, &->is_humanoid())) == 1)
    {
        CHECK_MULTI_TARGETS("policzkowa� jednocze�nie", PL_DOP);

        actor("Bierzesz rozmach i wymierzasz siarczysty policzek", cele, PL_CEL);
        all2act("bierze rozmach i wymierza siarczysty policzek", cele, PL_CEL);
        target("wymierza ci siarczysty policzek. Auuu !", cele);
        cele->add_prop(LIVE_S_SOULEXTRA, "rozciera obola�y policzek");
        return 1;
    }
    else
    {
        write("Nie bardzo wiesz jak si� do tego zabra�.\n");
        return 1;
    }

    if(str == "siebie")
    {
        write("Policzkujesz sam" + TP->koncowka("", "a", "e") + " siebie.\n");
        allbb("policzkuje sam" + TP->koncowka("", "a", "e") + " siebie.");
        return 1;
    }

    notify_fail("Spoliczkuj kogo?\n");
    return 0;
}

int
szturchnij(string str)
{
    object *cele;

    cele = parse_this(str, "%l:" + PL_BIE);
    if (sizeof(cele))
    {
        CHECK_MULTI_TARGETS("szturcha� jednocze�nie", PL_DOP);

        actor("Szturchasz", cele, PL_BIE);
        all2actbb("szturcha", cele, PL_BIE, ".");
        target("szturcha ci�.", cele);
        return 1;
    }
    notify_fail("Szturchnij kogo?\n");
    return 0;
}

int
syknij(string str)
{
    string *jak;
    jak = parse_adverb_with_space(str, "z b�lu", 0, ({"k�tem oka"}));
    if (!jak[0])
    {
        write("Syczysz" + jak[1] + ".\n");
        allbb("syczy" + jak[1] + ".", jak[1]);
        return 1;
    }
    notify_fail("Syknij [jak] ?\n");
    return 0;
}

int
tupnij(string str)
{
    object *cele;
    string *jak;
    int i;

    jak = parse_adverb_with_space(str, "niecierpliwie", 0, ({"k�tem oka"}));
    if (!jak[0])
    {
        write("Tupiesz" + jak[1] + ".\n");
        allbb("tupie" + jak[1] + ".", jak[1]);
        return 1;
    }
    cele = parse_this(jak[0], "'na' %l:" + PL_BIE);
    if (sizeof(cele))
    {
        if (sizeof(cele) >= 2)
        {
            actor("Tupiesz na", cele, PL_BIE, jak[1] + ".");
            all2actbb("tupie na", cele, PL_BIE, jak[1] + ".", jak[1]);
            NOTIFY_MULTI2(cele, target, "tupie na ", PL_BIE, jak[1] + ".", jak[1]);
            return 1;
        }
        else
        {
            actor("Tupiesz na", cele, PL_BIE, jak[1] + ".");
            all2actbb("tupie na", cele, PL_BIE, jak[1] + ".", jak[1]);
            targetbb("tupie na ciebie" + jak[1] + ".", cele, jak[1]);
            return 1;
        }
    }
    notify_fail("Tupnij [jak] [na kogo] ?\n");
    return 0;
}

int
ucisz(string str)
{
    if(!str || str ~= "wszystkich")
    {
        write("K�adziesz palec na ustach nakazuj�c wszystkim milczenie.\n");
        allbb("k�adzie palec na ustach nakazuj�c milczenie.");

        return 1;
    }

    object *cele;

    if(sizeof(cele = parse_this(str, " %l:" + PL_BIE)))
    {
        CHECK_MULTI_TARGETS("po�o�y� palca na ustach", PL_DOP);

        actor("K�adziesz palec na ustach", cele, PL_DOP, " nakazuj�c "
                +cele[0]->koncowka("mu", "jej")+" milczenie.");
        all2actbb("k�adzie palec na ustach", cele, PL_DOP, ".");
        target("k�adzie palec na twych ustach nakazuj�c ci milczenie.", cele);
        return 1;
    }

    notify_fail("Ucisz [kogo]?\n");
    return 0;
}

int
uderz(string str)
{
    object *cele;
    string *jak, czym, czym_w_koncu;
    notify_fail("Uderz [jak] czym w co ?\n");

    if(!str)
        return 0;

    jak = parse_adverb_with_space(str, "rozpaczliwie", 0, ({"k�tem oka"}));

    cele = parse_this(jak[0], "%l:" + PL_BIE);

    //uderzamy w jaki� przedmiot, odpowiednio du�y - test na to p�niej
    if (parse_command(jak[0],
        FILTER_DEAD(FILTER_CAN_SEE(all_inventory(ENV(TP)),TP)),
        "%s 'w' %i:" + PL_BIE, czym, cele))
    {
        cele = NORMAL_ACCESS(cele,0,0);
        if (sizeof(cele))
        {
            CHECK_MULTI_TARGETS("uderza� jednocze�nie", PL_DOP);

            if(cele[0]->query_prop(OBJ_I_VOLUME) < 1500)
            {
                notify_fail("To jest zbyt ma�y przedmiot.\n");
                return 0;
            }

            switch(czym)
            {
                case "g�ow�":  czym_w_koncu = "g�ow�"; break;
                case "r�k�":   czym_w_koncu = "r�k�"; break;
                case "d�oni�": czym_w_koncu = "d�oni�"; break;
                default: return 0;
            }

            actor("Uderzasz"+jak[1]+" "+czym_w_koncu+" w", cele, PL_BIE);
            all2actbb("uderza"+jak[1]+" "+czym_w_koncu+" w", cele, PL_BIE, ".", jak[1]);
            return 1;
        }
    }
    return 0;
}

int
uklon(string str)
{
    object *cele;
    string *jak;
    int i;

    if (str ~= "si�")
        str = 0;
    else if (str)
        sscanf(str, "si� %s", str);

    jak = parse_adverb_with_space(str, "z gracj�", 1, ({"k�tem oka"}));
    if (!jak[0])
    {
        write("K�aniasz si�" + jak[1] + ".\n");
        allbb("k�ania si�" + jak[1] + ".", jak[1]);
        return 1;
    }
    cele = parse_this(jak[0], "%l:" + PL_CEL);
    if (sizeof(cele))
    {
        if (sizeof(cele) >= 2)
        {
            actor("K�aniasz si�" + jak[1], cele, PL_CEL);
            all2actbb("k�ania si�" + jak[1], cele, PL_CEL, ".", jak[1]);
            NOTIFY_MULTI2(cele, target, "k�ania si� ", PL_CEL, jak[1] + ".", jak[1]);
            return 1;
        }
        else
        {
            actor("K�aniasz si�" + jak[1], cele, PL_CEL);
            all2actbb("k�ania si�" + jak[1], cele, PL_CEL, ".", jak[1]);
            targetbb("k�ania ci si�" + jak[1] + ".", cele, jak[1]);
            return 1;
        }
    }
    notify_fail(capitalize(query_verb()) + " [si�] [komu] [jak] ?\n");
    return 0;
}

int
unies(string str)
{
    NF("Co takiego chcesz unie��?\n");

    if(!str)
        return 0;

    int i;
    string ktora, co, *jak, str2;
    if((i = sscanf(str, "%s %s %s", ktora, co, str2)) < 3)
    {
        if((i=sscanf(str, "%s %s", ktora, co)) != 2)
        {
            if((i = sscanf(str, "%s", co)) != 1)
                return 0;
        }
        else if(!(ktora ~= "lew�" || ktora ~= "praw�" || ktora == "lewe" || ktora == "prawe"))
        {
            str2 = co;
            co = ktora;
            ktora = 0;
        }
        str = 0;
    }

    if(i == 1 && co ~= "nogi")
        return NF("Chcesz unie�� obie nogi?!\n");

    if(!(co == "brew" || co ~= "r�k�" || co ~= "nog�" || co ~= "rami�" ||
        co ~= "d�o�" || ((i==1 || i==2 && str2) && (co ~= "brwi" ||
        co ~= "r�ce" || co ~= "ramiona" || co ~= "d�onie"))))
    {
        return 0;
    }

    if(co ~= "rami�")
    {
        NF("Kt�re ramie chcesz unie��?\n");
        if(ktora != "lewe" && ktora != "prawe")
            return 0;
    }
    else if(i > 1 && ktora)
    {
        NF("Kt�r� " + co + " chcesz unie��?\n");
        if(!(ktora ~= "lew�" || ktora ~= "praw�"))
            return 0;
        if(ktora == "lewa")
            ktora = "lew�";
        else if(ktora == "prawa")
            ktora = "praw�";
    }

    if(co ~= "r�k�")
        co = "r�k�";
    else if(co == "noge")
        co = "nog�";
    else if(co == "ramie")
        co = "rami�";
    else if(co ~= "d�o�")
        co = "d�o�";
    else if(co ~= "r�ce")
        co = "r�ce";
    else if(co ~= "d�onie")
        co = "d�onie";

    if(!str2)
    {
        SOULDESC("unosi " + (ktora ? ktora + " " : "") + co);
        write("Unosisz " + (ktora ? ktora + " " : "") + co + ".\n");
        saybb(QCIMIE(TP, PL_MIA) + " unosi " + (ktora ? ktora + " " : "") + co + ".\n");
        return 1;
    }

    jak = parse_adverb_with_space(str2, BLANK_ADVERB, 0, ({"cicho", "k�tem oka"}));

    NF("Jak chcesz unie�� " + (ktora ? ktora + " " : "") + co + "?\n");

    if(!jak[1] || jak[1] == "")
        return 0;

    SOULDESC("unosi" + jak[1] + (ktora ? " " + ktora : "") + " " +  co);
    write("Unosisz" + jak[1] + (ktora ? " " + ktora : "") + " " + co + ".\n");
    saybb(QCIMIE(TP, PL_MIA) + " unosi" + jak[1] + (ktora ? " " + ktora : "") + " " + co + ".\n");

    return 1;
}

int
uszczypnij(string str)
{
    object *cele;
    string *jak, gdzie, str2;

    notify_fail(capitalize(query_verb()) + " kogo [jak] gdzie ?\n");

    if(!HAS_FREE_HAND(TP))
    {
       write("Musisz mie� przynajmniej jedn� d�o� woln�, aby m�c to zrobi�.\n");
       return 1;
    }

    if (!str) return 0;
    if (sscanf(str, "%s w %s", str2, gdzie) == 2) {
        switch (gdzie) {
                case "rami�": gdzie = "rami�"; break;
		case "po�ladek": gdzie = "po�ladek"; break;
		case "r�k�": gdzie = "r�k�"; break;
		case "rami�": gdzie = "rami�"; break;
            default: return 0;
        }
//        if (!(gdzie ~= "rami�")) return 0;
        jak = parse_adverb_with_space(str2, "lekko", 1, ({"k�tem oka"}));
        cele = parse_this(jak[0], "%l:" + PL_BIE);

        if(sizeof(filter(cele, &->is_humanoid())) == 1)
        {
            CHECK_MULTI_TARGETS("szczypa� jednocze�nie", PL_DOP);
            actor("Szczypiesz" + jak[1], cele, PL_BIE, " w " + gdzie + ".");
            all2actbb("szczypie" + jak[1], cele, PL_BIE,
                " w " + gdzie + ".", jak[1]);
            target("szczypie ci�" + jak[1] + " w " + gdzie + ".", cele,
                jak[1]);
            return 1;
        }
        else
        {
           write("Nie bardzo wiesz jak si� do tego zabra�.\n");
           return 1;
        }
        return 0;
    }

    jak = parse_adverb_with_space(str, "lekko", 1, ({"k�tem oka"}));
    cele = parse_this(jak[0], "%l:" + PL_BIE);
    return 0;
}

int
usciskaj(string str)
{
    object *cele;
    string *jak;

    jak = parse_adverb_with_space(str, "kordialnie", 1, ({"k�tem oka"}));
    cele = parse_this(jak[0], "%l:" + PL_BIE);

    if(!HAS_FREE_HANDS(TP))
    {
       write("Musisz mie� obie d�onie wolne, aby m�c to zrobi�.\n");
       return 1;
    }

    if (sizeof(cele))
    {
        CHECK_MULTI_TARGETS("�ciska� jednocze�nie", PL_DOP);

        actor("�ciskasz" + jak[1], cele, PL_BIE);
        all2actbb("�ciska" + jak[1], cele, PL_BIE, ".", jak[1]);
        target("�ciska ci�" + jak[1] + ".", cele, jak[1]);
        return 1;
    }
    notify_fail("U�ciskaj kogo [jak] ?\n");
    return 0;
}

int
usmiechnij(string str)
{
    object *cele;
    string *jak;
    int i;

    if (query_verb() ~= "u�miech")
        str = str ? "si� " + str : "si�";

    if (str ~= "si�")
    {
        SOULDESC("u�miecha si� weso�o");
        write("U�miechasz si� weso�o.\n");
        allbb("u�miecha si� weso�o.",
              ADD_SPACE_TO_ADVERB("weso�o"));
        return 1;
    }
    if (str && sscanf(str, "si� %s", str))
    {
        jak = parse_adverb_with_space(str, BLANK_ADVERB, 0, ({"z u�miechem", "k�tem oka"}));
        if (!jak[0])
        {
            SOULDESC("u�miecha si�" + jak[1]);
            write("U�miechasz si�" + jak[1] + ".\n");
            allbb("u�miecha si�" + jak[1] + ".", jak[1]);
            return 1;
        }
        cele = parse_this(jak[0], "'do' %l:" + PL_DOP);
        if (sizeof(cele))
        {
            if (sizeof(cele) >= 2)
            {
                SOULDESC("u�miecha si�" + jak[1]);
                actor("U�miechasz si� do", cele, PL_DOP, jak[1] + ".");
                all2actbb("u�miecha si� do", cele, PL_DOP, jak[1] + ".", jak[1]);
                NOTIFY_MULTI2(cele, target, "u�miecha si� do ", PL_DOP, jak[1] + ".", jak[1]);
                return 1;
            }
            else
            {
                SOULDESC("u�miecha si�" + jak[1]);
                actor("U�miechasz si� do", cele, PL_DOP, jak[1] + ".");
                all2actbb("u�miecha si� do", cele, PL_DOP, jak[1] + ".", jak[1]);
                targetbb("u�miecha si� do ciebie" + jak[1] + ".", cele, jak[1]);
                return 1;
            }
        }
    }
    notify_fail("U�miech[nij si�] [jak] [do kogo] ?\n");
    return 0;
}

int
warknij(string str)
{
    object *cele;
    string *jak;
    int i;

    CHECK_MOUTH_BLOCKED;

    jak = parse_adverb_with_space(str, "gro�nie", 0, ({"k�tem oka"}));
    if (!jak[0])
    {
        write("Warczysz" + jak[1] + ".\n");
        allbb("warczy" + jak[1] + ".", jak[1]);
        return 1;
    }
    cele = parse_this(jak[0], "'na' %l:" + PL_BIE);
    if (sizeof(cele))
    {
        if (sizeof(cele) >= 2)
        {
            actor("Warczysz na", cele, PL_BIE, jak[1] + ".");
            all2act("warczy na", cele, PL_BIE, jak[1] + ".", jak[1]);
            NOTIFY_MULTI2(cele, target, "warczy na ", PL_BIE, jak[1] + ".", jak[1]);
            return 1;
        }
        else
        {
            actor("Warczysz na", cele, PL_BIE, jak[1] + ".");
            all2act("warczy na", cele, PL_BIE, jak[1] + ".", jak[1]);
            target("warczy na ciebie" + jak[1] + ".", cele, jak[1]);
            return 1;
        }
    }
    notify_fail("Warknij [jak] [na kogo] ?\n");
    return 0;
}

int
westchnij(string str)
{
    string jak;

    CHECK_MOUTH_BLOCKED;

    jak = check_adverb_with_space(str, "ze smutkiem");
    if (jak != NO_ADVERB_WITH_SPACE)
    {
        write("Wzdychasz" + jak + ".\n");
        allbb("wzdycha" + jak + ".", jak);
        return 1;
    }
    notify_fail("Westchnij [jak] ?\n");
    return 0;
}

int
wrzasnij(string str)
{
    string jak;

    jak = check_adverb_with_space(str, "ze w�ciek�o�ci�", ({"k�tem oka"}));
    if (jak != NO_ADVERB_WITH_SPACE)
    {
        write("Wrzeszczysz" + jak + ".\n");
        all("wrzeszczy" + jak + ".", jak);
        return 1;
    }
    notify_fail("Wrza�nij [jak] ?\n");
    return 0;
}

int
item_access(object ob, string text, int cur_przyp)
{
  if (!objectp(ob))
    return 0;

  return (int) ob->item_id(text, cur_przyp);
}

varargs int
visible(object ob, object cobj)
{
  object env;

  if (!objectp(ob)) {
    return 0;
  }

  if (cobj && (env = (object)cobj->query_room()) &&
      (cobj->query_prop(CONT_I_TRANSP) ||
       ((!cobj->query_prop(CONT_I_CLOSED) && !cobj->is_open_close()) ||
        (cobj->query_open() && cobj->is_open_close()))) &&
      (!cobj->query_subloc_prop(ob->query_subloc(), CONT_M_OPENCLOSE) ||
       cobj->query_subloc_prop(ob->query_subloc(), CONT_I_TRANSP) ||
       !cobj->query_subloc_prop(ob->query_subloc(), CONT_I_CLOSED)))
  {
    return ((env->query_prop(OBJ_I_LIGHT) >
          -(TP->query_prop(LIVE_I_SEE_DARK))) &&
        CAN_SEE(TP, ob));
  }

  env = environment(ob);
  if (env == TP || env == environment(TP)) {
    return CAN_SEE(TP, ob);
  }

  while (objectp(env) && !living(env) &&
      ((ob->query_subloc() == 0) ?
      (env->query_prop(CONT_I_TRANSP) || ((!env->query_prop(CONT_I_CLOSED) && !env->is_open_close()) ||
      (env->query_open() && env->is_open_close()))) :
       (env->query_subloc_prop(ob->query_subloc(), CONT_I_TRANSP) ||
        !env->query_subloc_prop(ob->query_subloc(), CONT_I_CLOSED))))
  {
    env = environment(env);
    if (env == TP || env == environment(TP)) {
      return CAN_SEE(TP, ob);
    }
  }
  return 0;
}


int
wskaz(string str)
{
  object *cele;
  int i;
  string tmpText;

  if (!str)
  {
    write("Wskazujesz nieokre�lony kierunek.\n");
    allbb("wskazuje nieokre�lony kierunek.");
    return 1;
  }
  if (str == "siebie" || str == "na siebie")
  {
    write("Wskazujesz na siebie.\n");
    allbb("wskazuje na siebie.");
    return 1;
  }
  cele = parse_this(str, "[na] %l:" + PL_BIE);
  if (sizeof(cele))
  {
    if (sizeof(cele) >= 2)
    {
      actor("Wskazujesz", cele, PL_BIE);
      all2actbb("wskazuje", cele, PL_BIE);
      NOTIFY_MULTI(cele, target, "wskazuje na ", PL_BIE, ".");
      return 1;
    }
    else
    {
      actor("Wskazujesz", cele, PL_BIE);
      all2actbb("wskazuje", cele, PL_BIE);
      targetbb("wskazuje na ciebie.", cele);
      return 1;
    }
  }
  cele = parse_this(str, "[na] %i:" + PL_BIE);
  if (sizeof(cele))
  {
    write("Wskazujesz " + COMPOSITE_DEAD(cele, PL_BIE) + ".\n");
    saybb(QCIMIE(TP, PL_MIA) + " wskazuje "
        + QCOMPDEAD(PL_BIE) + ".\n");
    return 1;
  }
  sscanf(str, "na %s", str);
  i = member_array(str, Skroty) + member_array(str, Kierunki);
  if (i++ != -2)
  {
    write("Wskazujesz na " + Kierunki[i] + ".\n");
    allbb("wskazuje na " + Kierunki[i] + ".");
    return 1;
  }
  if (environment(TP)->item_id(str) &&
      CAN_SEE_IN_ROOM(TP))
  {
    write("Wskazujesz na " + environment(TP)->item_name(str) + ".\n");
    allbb("wskazuje na " + environment(TP)->item_name(str) + ".");
    return 1;
  }
  for (int cur_przyp = PL_MIA; cur_przyp <= PL_MIE; cur_przyp++) {
    if (parse_command(str, environment(TP), "%s %i:" + cur_przyp, tmpText, cele)) {
      tmpText = lower_case(tmpText);
      cele = NORMAL_ACCESS(cele, "visible", TO);
      cele = filter(cele, &item_access(, tmpText, cur_przyp));
      if (sizeof(cele) > 0)
      {
        write("Wskazujesz na " + cele[0]->item_name(tmpText) + " " + COMPOSITE_DEAD(({ cele[0] }), cur_przyp) + ".\n");
        allbb("wskazuje na " + cele[0]->item_name(tmpText) + " " + COMPOSITE_DEAD(({ cele[0] }), cur_przyp) + ".");
        return 1;
      }
    }
  }
  notify_fail("Wska� [[na] kogo / co] ?\n");
  return 0;
}

int
wybacz(string str)
{
    object *cele;
    string *jak;
    int i;

    CHECK_MOUTH_BLOCKED;

    jak = parse_adverb_with_space(str, "wielkodusznie", 1, ({"k�tem oka"}));
    cele = parse_this(jak[0], "%l:" + PL_CEL);
    if (sizeof(cele))
    {
        if (sizeof(cele) >= 2)
        {
            actor("Wybaczasz" + jak[1], cele, PL_CEL);
            all2act("wybacza" + jak[1], cele, PL_CEL, ".", jak[1]);
            NOTIFY_MULTI2(cele, target, "wybacza ", PL_CEL, jak[1] + ".", jak[1]);
            return 1;
        }
        else
        {
            actor("Wybaczasz" + jak[1], cele, PL_CEL);
            all2act("wybacza" + jak[1], cele, PL_CEL, ".", jak[1]);
            target("wybacza ci" + jak[1] + ".", cele, jak[1]);
            return 1;
        }
    }
    notify_fail("Wybacz komu [jak] ?\n");
    return 0;
}

int
wyplacz(string str)
{
    object *cele;

    if (sizeof(cele = parse_this(str, "'si�' 'na' %l:" + PL_MIE)) ||
        sizeof(cele = parse_this(str, "'si�' 'na' 'ramieniu' %l:" + PL_DOP)))
    {
        CHECK_MULTI_TARGETS("wyp�akiwa� si� jednocze�nie na ramieniu", PL_DOP);

        if(!cele[0]->is_humanoid())
        {
            write("Nie bardzo wiesz jak si� do tego zabra�.\n");
            return 1;
        }

        SOULDESC("tonie we �zach");
        actor("Wyp�akujesz si� na ramieniu", cele, PL_DOP);
        all2act("wyp�akuje si� na ramieniu", cele, PL_DOP, ".");
        target("wyp�akuje si� na twoim ramieniu.", cele);
        return 1;
    }

    notify_fail("Wyp�acz si� na kim / na czyim ramieniu?\n");
    return 0;
}


int
wystaw(string str)
{
    if (str ~= "j�zor")
    {
        write("Wystawiasz j�zor. Bleeeeee!\n");
        allbb("wystawia j�zor. Bleeeeee!");
        return 1;
    }
    notify_fail("Wystaw j�zor?\n");
    return 0;
}

int
wyszczerz(string str)
{
    object *cele;
    string *jak;


    if (str ~= "si^e")
    {
        write("Szczerzysz si^e demonicznie.\n");
        allbb("szczerzy si^e demonicznie.");
        return 1;
    }
    jak = parse_adverb_with_space(str, "demonicznie", 1, ({"k^atem oka"}));
    cele = parse_this(jak[0], "'si^e' 'do' %l:" + PL_DOP);

    if (jak[0] ~= "si^e")
    {
        write("Szczerzysz si^e" + jak[1] + ".\n");
        allbb("szczerzy si^e" + jak[1] + ".", jak[1]);
        return 1;
    }

    if (sizeof(cele))
    {
        if (sizeof(cele) >= 2)
        {
            actor("Szczerzysz si^e do", cele, PL_DOP, jak[1] + ".");
            all2actbb("szczerzy si^e do", cele, PL_DOP, jak[1] + ".", jak[1]);
            NOTIFY_MULTI2(cele, target, "szczerzy si^e do ", PL_DOP, jak[1] + ".", jak[1]);
            return 1;
        }
        else
        {
            actor("Szczerzysz si^e do", cele, PL_DOP, jak[1] + ".");
            all2actbb("szczerzy si^e do", cele, PL_DOP, jak[1] + ".", jak[1]);
            targetbb("szczerzy si^e do ciebie" + jak[1] + ".", cele, jak[1]);
            return 1;
        }
    }

    notify_fail("Wyszczerz si^e [do kogo] [jak] ?\n");
    return 0;
}

int
wytrzeszcz(string str)
{
    object *cele;
    int i;

    if (!str || str ~= "si�" || str == "oczy" || str ~= "�lepia" ||
        str ~= "ga�y")
    {
        SOULDESC("ma wytrzeszczone oczy");
        write("Wytrzeszczasz oczy w niemym zdumieniu.\n");
        allbb("wytrzeszcza oczy w niemym zdumieniu.");
        return 1;
    }

    cele = parse_this(str, "[si�] / [oczy] / [�lepia] / [ga�y] 'na' %l:"
                    + PL_BIE);
    if (sizeof(cele))
    {
        if (sizeof(cele) >= 2)
        {
            SOULDESC("ma wytrzeszczone oczy");
            actor("Wytrzeszczasz oczy na", cele, PL_BIE, " w niemym zdumieniu.");
                all2actbb("wytrzeszcza oczy na", cele, PL_BIE, " w niemym zdumieniu.");
            NOTIFY_MULTI(cele, target, "wytrzeszcza na ", PL_BIE, " oczy w niemym zdumieniu.");
            return 1;
        }
        else
        {
            SOULDESC("ma wytrzeszczone oczy");
            actor("Wytrzeszczasz oczy na", cele, PL_BIE, " w niemym zdumieniu.");
            all2actbb("wytrzeszcza oczy na", cele, PL_BIE, " w niemym zdumieniu.");
            targetbb("wytrzeszcza na ciebie oczy w niemym zdumieniu.", cele);
            return 1;
        }
    }
    notify_fail(capitalize(query_verb()) + " [co] [na kogo] ?\n");
    return 0;
}

int
wzdrygnij(string str)
{
    if (str ~= "si�")
    {
        write("Wzdrygasz si�. Brrr... !\n");
        allbb("wzdryga si�.");
        return 1;
    }

    if(str && wildmatch("si� *my�l*", str))
    {
        if (strlen(str) > MAX_FREE_EMOTE)
            str = "si� na my�l o czym� szczeg�lnie potwornym";

        str = str[4..strlen(str)];

        write("Wzdrygasz si� " + str + ". Brrr... !\n");
        allbb("wzdryga si�" + str + ".");
        return 1;
    }

    notify_fail("Wzdrygnij si� [na my�l o czym] ?\n");
    return 0;
}

int
wzrusz(string str)
{
    switch (str)
    {
        case "si�":
            SOULDESC("jest czym� bardzo wzruszon"
                     + TP->koncowka("y", "a", "e"));
            write("Wzruszasz si�.\n");
            allbb("jest czym� bardzo wzruszon"
                  + TP->koncowka("y", "a", "e") + ".");
            return 1;
        case "ramionami":
            write("Wzruszasz ramionami.\n");
            allbb("wzrusza ramionami.");
            return 1;
    }
    notify_fail("Wzrusz si�? / Wzrusz czym?\n");
    return 0;
}

int
zachichocz(string str)
{
    string jak;

    CHECK_MOUTH_BLOCKED;

    jak = check_adverb_with_space(str, "rado�nie", ({"k�tem oka"}));
    if (jak != NO_ADVERB_WITH_SPACE)
    {
        SOULDESC("chichocze" + jak);
        write("Chichoczesz" + jak + ".\n");
        allbb("chichocze" + jak + ".", jak);
        return 1;
    }
    notify_fail("Zachichocz [jak] ?\n");
    return 0;
}

int
zachlip(string str)
{
    string jak;

    CHECK_MOUTH_BLOCKED;

    jak = check_adverb_with_space(str, "�a�o�nie", ({"k�tem oka"}));
    if (jak != NO_ADVERB_WITH_SPACE)
    {
        SOULDESC("chlipie" + jak);
        write("Chlipiesz" + jak + ".\n");
        allbb("chlipie" + jak + ".", jak);
        return 1;
    }
    notify_fail("Zachlip [jak] ?\n");
    return 0;
}

int
zachrumkaj(string str)
{
    string jak;

    CHECK_MOUTH_BLOCKED;

    jak = check_adverb_with_space(str, "jak �winka", ({"k�tem oka"}));
    if (jak != NO_ADVERB_WITH_SPACE)
    {
        write("Chrumkasz" + jak + ".\n");
        allbb("zadziera ryjek i chrumka" + jak + ".", jak);
        return 1;
    }
    notify_fail("Zachrumkaj [jak] ?\n");
    return 0;
}

int
zachwyc(string str)
{
    object *cele;
    int i;

    cele = parse_this(str, "'si�' %l:" + PL_NAR);
    if (sizeof(cele))
    {
        if (sizeof(cele) >= 2)
        {
            actor("Zachwycasz si�", cele, PL_NAR, ".");
            all2act("zachwyca si�", cele, PL_NAR, ".");
            NOTIFY_MULTI(cele, target, "zachwyca si� ", PL_NAR, ".");
            return 1;
        }
        else
        {
            actor("Zachwycasz si�", cele, PL_NAR, ".");
            all2act("zachwyca si�", cele, PL_NAR, ".");
            target("zachwyca si� tob�.", cele);
            return 1;
        }
    }
    cele = parse_this(str, "'si�' %i:" + PL_NAR);
    if (sizeof(cele))
    {

        actor("Zachwycasz si�", cele, PL_NAR, ".");
        all2act("zachwyca si�", cele, PL_NAR, ".");
        return 1;
    }

    if (str ~= "si� sob�")
    {
        write("Zachwycasz si� sam" + TP->koncowka("", "a", "e")
               + " sob�.\n");
        allbb("popada w samozachwyt.");
        return 1;
    }
    notify_fail("Zachwy� [si�] [kim / czym] ?\n");
    return 0;
}

int
zacisnij(string str)
{
    if(!HAS_FREE_HANDS(TP))
    {
       write("Musisz mie� obie d�onie wolne, aby m�c to zrobi�.\n");
       return 1;
    }
    if (str ~= "pi�ci")
    {
        write("Zaciskasz pi�ci.\n");
        allbb("zaciska pi�ci.");
        return 1;
    }
    notify_fail("Zaci�nij pi�ci?\n");
    return 0;
}

int
zaczerwien(string str)
{
    if (str ~= "si�")
    {
        SOULDESC("jest czerwon" + TP->koncowka("y", "a", "e")
               + " po czubki uszu");
        write("Czerwienisz si� po czubki uszu.\n");
        allbb("czerwieni si� po czubki uszu.");
        return 1;
    }
    notify_fail("Zaczerwie� si�?\n");
    return 0;
}

int
zarumien(string str)
{
    if (str ~= "si�")
    {
        SOULDESC("jest delikatnie zarumienion" + TP->koncowka("y", "a", "e"));
        write("Rumienisz si� delikatnie.\n");
        allbb("rumieni si� delikatnie.");
        return 1;
    }
    notify_fail("Zarumie� si�?\n");
    return 0;
}

int
zadrzyj(string str)
{
    string jak;

    if(str && str ~= "z przera�enia")
        jak = " z przera�enia";
    else
        jak = check_adverb_with_space(str, "z zimna", ({"k�tem oka"}));
    if (jak != NO_ADVERB_WITH_SPACE)
    {
        SOULDESC("dr�y" + jak);
        write("Dr�ysz" + jak + ".\n");
        allbb("dr�y" + jak + ".");
        return 1;
    }

    notify_fail("Zadr�yj [jak] ?\n");
    return 0;
}

int
zagryz(string str)
{
    string *jak;

    jak = parse_adverb_with_space(str, BLANK_ADVERB, 1, ({"k�tem oka"}));
    if (jak[0] == "usta" || jak[0] == "wargi")
    {
        write("Zagryzasz" + jak[1] + " wargi.\n");
        allbb("zagryza" + jak[1] + " wargi.", jak[1]);
        return 1;
    }
    notify_fail("Zagry� wargi [jak] ?\n");
    return 0;
}

int
zagwizdz(string str)
{
    object *cele;
    string *jak;
    int i;

    CHECK_MOUTH_BLOCKED;

    jak = parse_adverb_with_space(str, "z podziwem", 0, ({"k�tem oka"}));
    if (!jak[0])
    {
        write("Gwi�d�esz" + jak[1] + ".\n");
        all("gwi�d�e" + jak[1] + ".");
        return 1;
    }
    cele = parse_this(jak[0], "'na' %l:" + PL_BIE);
    if (sizeof(cele))
    {
        if (sizeof(cele) >= 2)
        {
            actor("Gwi�d�esz na", cele, PL_BIE, jak[1] + ".");
            all2act("gwi�d�e na", cele, PL_BIE, jak[1] + ".", jak[1]);
            NOTIFY_MULTI2(cele, target, "gwi�d�e na ", PL_BIE, jak[1] + ".", jak[1]);
            return 1;
        }
        else
        {
            actor("Gwi�d�esz na", cele, PL_BIE, jak[1] + ".");
            all2act("gwi�d�e na", cele, PL_BIE, jak[1] + ".", jak[1]);
            target("gwi�d�e na ciebie" + jak[1] + ".", cele, jak[1]);
            return 1;
        }
    }
    notify_fail("Zagwi�d� [jak] [na kogo] ?\n");
    return 0;
}

int
zajrzyj(string str)
{
    object *cele;

    if (str && sscanf(str, "w oczy %s", str))
    {
        cele = parse_this(str, "%l:" + PL_CEL);

        if(sizeof(cele))
        {
            CHECK_MULTI_TARGETS("zagl�da� w oczy jednocze�nie", PL_CEL);

            if(cele[0]->query_prop(EYES_CLOSED))
            {
                notify_fail(capitalize(cele[0]->query_imie(TP, PL_MIA))
                           +" ma zamkni�te oczy!\n");
                return 0;
            }

            actor("Zagl�dasz g��boko w oczy", cele, PL_DOP);
            all2actbb("g��boko zagl�da w oczy", cele, PL_DOP, ".");
            target("g��boko zagl�da ci w oczy.", cele);
            return 1;
        }
    }

    notify_fail("Chyba zajrzyj w oczy <komu>\n");
    return 0;
}

int
zaklnij(string str)
{
    string jak;

    jak = check_adverb_with_space(str, "siarczy�cie", ({"k�tem oka"}));
    if (jak != NO_ADVERB_WITH_SPACE)
    {
        SOULDESC("klnie" + jak);
        write("Klniesz" + jak + ".\n");
        all("klnie" + jak + ".", jak);
        return 1;
    }
    notify_fail("Zaklnij [jak] ?\n");
    return 0;
}

int
zakrztus(string str)
{
    if (str ~= "si�")
    {
        SOULDESC("krztusi si�");
        write("Zakrztusi�" + TP->koncowka("e�", "a�")
            +  " si� nagle.\n");
        all("krztusi si� nagle.");
        return 1;
    }
    notify_fail("Zakrztu� si�?\n");
    return 0;
}

int
zalam(string str)
{
    if (str ~= "si�")
    {
        SOULDESC("jest ca�kowicie za�aman"
                 + TP->koncowka("y", "a", "e"));
        write("Za�amujesz si� ca�kowicie.\n");
        allbb("za�amuje si� ca�kowicie.");
        return 1;
    }
    notify_fail("Za�am si�?\n");
    return 0;
}

int
zalkaj(string str)
{
    if (!str)
    {
        SOULDESC("zanosi si� �kaniem");
        write("Zanosisz si� �kaniem.\n");
        allbb("zanosi si� �kaniem.");
        return 1;
    }
    notify_fail("Za�kaj?\n");
    return 0;
}

int
zamknij(string str)
{
    if (str =="oczy" && TP->query_prop(LIVE_I_SEE_DARK) >= 0)
    {
        //SOULDESC zast�pi� czym� innym!!!!!!!
        //SOULDESC("ma zamkni�te oczy");
        write("Zamykasz oczy.\n");
        allbb("zamyka oczy.");
        TP->add_prop(LIVE_I_SEE_DARK,-100);
        TP->add_prop(EYES_CLOSED,1);

        return 1;
    }
    notify_fail("Zamknij co ?\n");
    return 0;
}

int
zamrucz(string str)
{
    string jak;

    CHECK_MOUTH_BLOCKED;

    jak = check_adverb_with_space(str, "z lubo�ci�", ({"k�tem oka"}));
    if (jak != NO_ADVERB_WITH_SPACE)
    {
        SOULDESC("mruczy" + jak);
        write("Mruczysz" + jak + ".\n");
        allbb("mruczy" + jak + ".", jak);
        return 1;
    }
    notify_fail("Zamrucz [jak] ?\n");
    return 0;
}

int
zamysl(string str)
{
    if (str ~= "si�")
    {
        SOULDESC("wygl�da na bardzo zamy�lon"
                 + TP->koncowka("ego", "�", "e"));
        write("Popadasz w g��bok� zadum�.\n");
        allbb("popada w g��bok� zadum�.");
        return 1;
    }
    notify_fail(capitalize(query_verb())+" si� ?\n");
    return 0;
}

int
zanuc(string str)
{
    string jak;

    jak = check_adverb_with_space(str, "melodyjnie", ({"k�tem oka"}));
    if (jak != NO_ADVERB_WITH_SPACE)
    {
        SOULDESC("nuci co�" + jak);
        write("Nucisz co�" + jak + ".\n");
        allbb("nuci co�" + jak + ".", jak);
        return 1;
    }
    notify_fail("Zanu� [jak] ?\n");
    return 0;
}

int
zapiszcz(string str)
{
    string jak;

    jak = check_adverb_with_space(str, "przera�liwie", ({"k�tem oka"}));
    if (jak != NO_ADVERB_WITH_SPACE)
    {
        SOULDESC("piszczy" + jak);
        write("Piszczysz" + jak + ".\n");
        all("piszczy" + jak + ".", jak);
        return 1;
    }
    notify_fail("Zapiszcz [jak] ?\n");
    return 0;
}

int
zaplacz(string str)
{
    string *jak;
    jak = parse_adverb_with_space(str, "cichutko", 0, ({"k�tem oka"}));
    if (!jak[0])
    {
        write("P�aczesz" + jak[1] + ".\n");
        allbb("p�acze" + jak[1] + ".", jak[1]);
        return 1;
    }
    notify_fail("Zap�acz [jak] ?\n");
    return 0;
}

int
zaprzecz(string str)
{
    object *cele;
    int i;

    CHECK_MOUTH_BLOCKED;

    if (!str)
    {
        write("Zdecydowanie zaprzeczasz.\n");
        all("zdecydowanie zaprzecza.");
        return 1;
    }

    string *jak;
    jak = parse_adverb_with_space(str, "zdecydowanie", 0, ({"k�tem oka"}));

    if(!jak[0])
    {
        write("Zaprzeczasz"+jak[1]+".\n");
        all("zaprzecza"+jak[1]+".");
        return 1;
    }

    cele = parse_this(jak[0], "'s�owom' %l:" + PL_DOP);

    if (sizeof(cele))
    {
        if (sizeof(cele) < 2)
        {

        /*{
            actor("Zaprzeczasz"+jak[1]+" s�owom", cele, PL_DOP);
            all2act("zaprzecza"+jak[1]+" s�owom", cele, PL_DOP);
            NOTIFY_MULTI(cele, target, "zaprzecza"+jak[1]+" s�owom ", PL_DOP, ".");
            return 1;
        }
        else*/

            actor("Zaprzeczasz"+jak[1]+" s�owom", cele, PL_DOP);
            all2act("zaprzecza"+jak[1]+" s�owom", cele, PL_DOP);
            target("zaprzecza"+jak[1]+" twoim s�owom.", cele);
            return 1;
        }

    }

    notify_fail("Zaprzecz [czyim s�owom] ?\n");
    return 0;
}

int
zarechocz(string str)
{
    string jak;

    CHECK_MOUTH_BLOCKED;

    jak = check_adverb_with_space(str, "rubasznie", ({"k�tem oka"}));
    if (jak != NO_ADVERB_WITH_SPACE)
    {
        SOULDESC("rechocze" + jak);
        write("Rechoczesz" + jak + ".\n");
        all("rechocze" + jak + ".", jak);
        return 1;
    }
    notify_fail("Zarechocz [jak] ?\n");
    return 0;
}

int
zasmiej(string str)
{
    object *cele;
    string *jak;
    int i;

    CHECK_MOUTH_BLOCKED;

    if (str ~= "si�")
    {
        SOULDESC("�mieje si� rado�nie");
        write("Wybuchasz �miechem.\n");
        all("wybucha �miechem.", NO_ADVERB_WITH_SPACE);
        return 1;
    }
    if (str && sscanf(str, "si� %s", str))
    {
        jak = parse_adverb_with_space(str, "kpi�co", 0, ({"k�tem oka"}));
        if (!jak[0])
        {
            SOULDESC("�mieje si�" + jak[1]);
            write("�miejesz si�" + jak[1] + ".\n");
            all("�mieje si�" + jak[1] + ".", jak[1]);
            return 1;
        }
        cele = parse_this(jak[0], "'z' %l:" + PL_DOP);
        if (sizeof(cele))
        {
            if (sizeof(cele) >= 2)
            {
                SOULDESC("�mieje si�" + jak[1]);
                actor("�miejesz si� z", cele, PL_DOP, jak[1] + ".");
                all2act("�mieje si� z", cele, PL_DOP, jak[1] + ".", jak[1]);
                NOTIFY_MULTI2(cele, target, "�mieje si� z ", PL_DOP, jak[1] + ".", jak[1]);
                return 1;
            }
            else
            {
                SOULDESC("�mieje si�" + jak[1]);
                actor("�miejesz si� z", cele, PL_DOP, jak[1] + ".");
                all2act("�mieje si� z", cele, PL_DOP, jak[1] + ".", jak[1]);
                target("�mieje si� z ciebie" + jak[1] + ".", cele, jak[1]);
                return 1;
            }
        }
        cele = parse_this(jak[0], " %l:" + PL_CEL+" 'w' 'twarz'");
        if (sizeof(cele))
        {
            if (sizeof(cele) >= 2)
            {
                SOULDESC("�mieje si�" + jak[1]);
                actor("�miejesz si�", cele, PL_CEL, jak[1] + " w twarze.");
                all2act("�mieje si�", cele, PL_CEL, jak[1] + " w twarze.", jak[1]);
                NOTIFY_MULTI2(cele, target, "�mieje si� ", PL_CEL, jak[1] +
                              " w twarze.", jak[1]);
                return 1;
            }
            else
            {
                SOULDESC("�mieje si�" + jak[1]);
                actor("�miejesz si�", cele, PL_CEL, jak[1] + " w twarz.");
                all2act("�mieje si�", cele, PL_CEL, jak[1] + " w twarz.", jak[1]);
                target("�mieje si�" + jak[1] + " tobie w twarz.", cele, jak[1]);
                return 1;
            }
        }
    }
    notify_fail("Za�miej si� [jak] [z kogo / komu w twarz] ?\n");
    return 0;
}

int
zaszlochaj(string str)
{
    string *jak;
    jak = parse_adverb_with_space(str, "�a�o�nie", 0, ({"k�tem oka"}));
    if (!jak[0])
    {
        write("Szlochasz" + jak[1] + ".\n");
        allbb("szlocha" + jak[1] + ".", jak[1]);
        return 1;
    }
    notify_fail("Zaszlochaj [jak] ?\n");
    return 0;
}


int
zatancz(string str)
{
    object *cele;

	if(TP->query_fatigue() <= 2)
	{
		write("Jeste� na to zbyt zm�czony.\n");
		return 1;
	}

    if (!str)
    {
        write("Wykonujesz dziki taniec, daj�c upust rozpieraj�cej ci� "
            + "energii.\n");
        all("wykonuje dziki taniec, daj�c upust rozpieraj�cej "
            + TP->koncowka("go", "j�", "je") + " energii.");

        //Sprawdzamy zm�czenie to i zabierzmy!
        TP->add_fatigue(2);
        return 1;
    }

    if(str~="wok� ogniska")
    {
        object ognisko;
        foreach(object ox : FILTER_DEAD(all_inventory(environment(TP))))
        {
            if(ox->query_ognisko())
                ognisko=ox;
        }
        if(!ognisko)
        {
            write("Ale� tu nie ma �adnego ogniska!\n");
            return 1;
        }

        write("Wykonujesz dziki taniec wok� ogniska, daj�c upust rozpieraj�cej ci� "
            + "energii.\n");
        all("wykonuje dziki taniec wok� ogniska, daj�c upust rozpieraj�cej "
            + TP->koncowka("go", "j�", "je") + " energii.");

        //Sprawdzamy zm�czenie to i zabierzmy!
        TP->add_fatigue(2);
        return 1;
    }


    cele = parse_this(str, "'z' %l:" + PL_NAR);

    if(!HAS_FREE_HAND(TP))
    {
       NF("Musisz mie� przynajmniej jedn� d�o� woln�, aby m�c to zrobi�.\n");
       return 0;
    }

    if(sizeof(filter(cele, &->is_humanoid())) == 1)
    {
        if(cele[0]->query_prop(SIT_SIEDZACY) || cele[0]->query_prop(SIT_LEZACY))
        {
            NF("Zdaje si�, �e tw"+cele[0]->koncowka("�j","oja","�j")+
                " partner"+cele[0]->koncowka("","ka","")+" siedzi lub le�y.\n");
                return 0;
        }

        CHECK_MULTI_TARGETS("ta�czy� jednocze�nie z", PL_NAR);

        actor("Porywasz", cele,  PL_BIE, " do dzikiego ta�ca, wycinaj�c "
            + "kilka ho�ubc�w.");
        all2act("porywa", cele, PL_BIE, " do dzikiego tanca, wycinaj�c "
                + "kilka ho�ubc�w.");
        target("porywa ci� do dzikiego ta�ca, wycinaj�c kilka ho�ubc�w.",
               cele);

        //Sprawdzamy zm�czenie to i zabierzmy!
        TP->add_fatigue(2);
        return 1;
    }
    else
    {
        NF("Nie bardzo wiesz jak si� do tego zabra�.\n");
        return 0;
    }

    notify_fail("Zata�cz [z kim] ?\n");
    return 0;
}

int
zatkaj(string str)
{

    if(!HAS_FREE_HANDS(TP))
    {
       write("Musisz mie� obie d�onie wolne, aby m�c to zrobi�.\n");
       return 1;
    }

    if (str ~= "uszy")
    {
        write("Zatykasz uszy.\n");
        allbb("zatyka uszy.");
        return 1;
    }

    if(!HAS_FREE_HAND(TP))
    {
       write("Musisz mie� przynajmniej jedn� d�o� woln�, aby m�c to zrobi�.\n");
       return 1;
    }
    if (str ~= "nos")
    {
        write("Zatykasz nos.\n");
        allbb("zatyka nos.");
        return 1;
    }
    notify_fail("Zatkaj [co] ?\n");
    return 0;
}


int
zatrzepocz(string str)
{
//    object *cele;
    string *jak;
    int i;

    if(!CAN_SEE_IN_ROOM(TP))
    {
       write("Ciemno��...Ciemno�� widzisz!\n");
       return 1;
    }

//    jak = check_adverb_with_space(str, "uroczo");
    /* Zabezpieczenie przed bledna interpretacja 'przed' jako przyslowka. */
    /*if (str && wildmatch("przed *", str))
        jak = ({str, ADD_SPACE_TO_ADVERB("z gracj�")});
    else
        jak = parse_adverb_with_space(str, "z gracj�", 0);*/

//    if (TP->query_gender() != G_FEMALE)
//        jak[1] = ADD_SPACE_TO_ADVERB("niezr�cznie");
 //   else
  //     jak = ADD_SPACE_TO_ADVERB("uroczo");
//    if (!jak[0])

/*
    jak = parse_adverb_with_space(str, "uroczo", 1);
    if (TP->query_gender() != G_FEMALE)
        jak[1] = ADD_SPACE_TO_ADVERB("niezr�cznie");



    if (jak[0] ~= "rz�sami" || !jak[0])
    {
        write("Trzepoczesz rz�sami" + jak[1] + ".\n");
        allbb("trzepocze rz�sami" + jak[1] + ".", jak[1]);
        return 1;
    }
*/

    notify_fail("Zatrzepocz [rz�sami] ?\n");
    if (str && !(str ~= "rz�sami")) return 0;

    if (TP->query_gender() != G_FEMALE)
    {
        write("Trzepoczesz rz�sami niezr�cznie.\n");
        allbb("trzepocze rz�sami niezr�cznie.");
    }
    else
    {
        write("Trzepoczesz rz�sami uroczo.\n");
        allbb("trzepocze rz�sami uroczo.");
    }
    return 1;
 /*       if (jak != NO_ADVERB_WITH_SPACE)
    {
        write("Trzepoczesz rz�sami" + jak + ".\n");
        allbb("trzepocze rz�sami" + jak + ".", jak);
        return 1;
    }*/

}


int
zatrzyj(string str)
{
    if(!HAS_FREE_HANDS(TP))
    {
       write("Musisz mie� obie d�onie wolne, aby m�c to zrobi�.\n");
       return 1;
    }

    if (!str || str ~= "r�ce")
    {
        write("Zacierasz r�ce.\n");
        allbb("zaciera r�ce.");
        return 1;
    }
    notify_fail("Zatrzyj [r�ce] ?\n");
    return 0;
}

int
zawyj(string str)
{
    string jak;

    jak = check_adverb_with_space(str, "z b�lu", ({"k�tem oka"}));
    if (jak != NO_ADVERB_WITH_SPACE)
    {
        SOULDESC("wyje" + jak);
        write("Wyjesz" + jak + ".\n");
        all("wyje" + jak + ".", jak);
        return 1;
    }
    notify_fail("Zawyj [jak] ?\n");
    return 0;
}

int
zazgrzytaj(string str)
{
    object *cele;
    string *jak;

    if (!str || str ~= "z�bami")
    {
        SOULDESC("zgrzyta z�bami");
        write("Zgrzytasz z w�ciek�o�ci� z�bami.\n");
        all("zgrzyta w�ciekle z�bami.");
        return 1;
    }
    notify_fail("Zazgrzytaj [z�bami] ?\n");
    return 0;
}

int
zbeltaj(string str)
{
    object *cele;

    if (query_verb() == "zwymiotuj")
        str = str ? "si� " + str : "si�";

    if (str ~= "si�")
    {
        SOULDESC("jest unurzan" + TP->koncowka("y", "a", "e")
               + " we w�asnych wymiocinach");
        write("Zginasz si� w p�, czuj�c nag�� potrzeb� pozbycia si� "
            + "zawarto�ci �o��dka. Skr�caj�c si� z b�lu w miar� jak twymi "
            + "wn�trzno�ciami targaj� torsje, d�ugo wyrzucasz z siebie na "
            + "wp� przetrawione resztki.\n"
            + "Z trudem znajdujesz w sobie si�y by chwiej�c si� na nogach "
            + "wsta� wreszcie z kl�czek. C� za �a�osny widok...\n");

        all("puszcza be�ta, za�winiaj�c wszystko wok�, a przy okazji "
          + "siebie.");

        TP->set_stuffed(0);
        TP->add_fatigue(-MAX(66,
            2 * TP->query_max_fatigue() / 3));

        return 1;
    }
#if 0
    else if (sizeof(cele = parse_this(str, "'sie' 'na' %l:" + PL_BIE)) ||
        sizeof(cele = parse_this(str, "'sie' 'na' 'buty' %l:" + PL_CEL)))
    {
        CHECK_MULTI_TARGETS("zanieczyszczac jednoczesnie butow", PL_CEL);
        return 1;
    }

    notify_fail((query_verb() == "zwymiotuj" ? "Zwymiotuj" : "Zbeltaj sie")
              + " [na kogo] / [na buty komu] ?\n");
#endif
    notify_fail((query_verb() == "zwymiotuj" ? "Zwymiotuj" : "Zbe�taj si�")
              + "?\n");
    return 0;
}

int
zblednij(string str)
{
    if (!str)
    {
        SOULDESC("jest blad" + TP->koncowka("y", "a")
               + " jak �ciana");
        write("Bledniesz jak �ciana.\n");
        allbb("blednie jak �ciana.");
        return 1;
    }
    notify_fail("Zblednij?\n");
    return 0;
}

int
zdziw(string str)
{
    if (str ~= "si�")
    {
        SOULDESC("wygl�da na bardzo zdziwion"
               + TP->koncowka("ego", "�"));
        write("Unosisz brwi w niemym ge�cie zdumienia.\n");
        allbb("unosi brwi w niemym ge�cie zdumienia.");
        return 1;
    }
    notify_fail("Zdziw si�?\n");
    return 0;
}

int
ziewnij(string str)
{
    string jak;

    jak = check_adverb_with_space(str, "sennie", ({"k�tem oka"}));
    if (jak != NO_ADVERB_WITH_SPACE)
    {
        SOULDESC("ziewa" + jak);
        write("Na wp� przymykasz oczy, otwierasz usta i ziewasz" + jak
            + ".\n");
        allbb("na wp� przymyka oczy, otwiera usta i ziewa" + jak + ". Chyba "
            + "te� masz ochot� sobie ziewn��.", jak);
        return 1;
    }
    notify_fail("Ziewnij [jak] ?\n");
    return 0;
}

int
zignoruj(string str)
{
    object *cele;
    int i;

    cele = parse_this(str, "%l:" + PL_BIE);
    if (sizeof(cele))
    {
        if (sizeof(cele) >= 2)
        {
            actor("Odwr�ciwszy si� plecami, ignorujesz demonstracyjnie", cele, PL_BIE);
            all2actbb(", odwr�ciwszy si� plecami, ignoruje demonstracyjnie", cele, PL_BIE);
            NOTIFY_MULTI(cele, target, "odwraca si� do ", PL_BIE, " plecami, demonstracyjnie was ignoruj�c.");
            return 1;
        }
        else
        {
            actor("Odwr�ciwszy si� plecami, ignorujesz demonstracyjnie", cele, PL_BIE);
            all2actbb(", odwr�ciwszy si� plecami, ignoruje demonstracyjnie", cele, PL_BIE);
            targetbb("odwraca si� do ciebie plecami, demonstracyjnie ci� ignoruj�c.", cele);
            return 1;
        }
    }
    notify_fail("Zignoruj kogo?\n");
    return 0;
}

int
zlorzecz(string str)
{
    //if (str ~= "na" || (str && wildmatch("na *", str)))
    if ((str && wildmatch("na *", str)))
    {
        if (strlen(str) > MAX_FREE_EMOTE)
            str = "na co� niewymownie paskudnego";

        write("Z�orzeczysz " + str + ".\n");
        allbb("z�orzeczy " + str + ".");
        return 1;
    }

    notify_fail("Z�orzecz na [co] ?\n");
    return 0;
}

int
zmarszcz(string str)
{
    object *cele;
    string *jak;
    int i;

    if (str == "brwi")
    {
        SOULDESC("marszczy gniewnie brwi");
        write("Marszczysz gniewnie brwi.\n");
        allbb("marszczy gniewnie brwi.",
              ADD_SPACE_TO_ADVERB("gniewnie"));
        return 1;
    }

    if (str ~= "czo�o")
    {
        SOULDESC("marszczy z namys�em czo�o");
        write("Marszczysz z namys�em czo�o.\n");
        allbb("marszczy z namys�em czo�o.",
              ADD_SPACE_TO_ADVERB("z namys�em"));
        return 1;
    }

    if (str && sscanf(str, "brwi %s", str))
    {
        jak = parse_adverb_with_space(str, "gniewnie", 0, ({"k�tem oka"}));
        if (!jak[0])
        {
            SOULDESC("marszczy" + jak[1] + " brwi");
            write("Marszczysz" + jak[1] + " brwi.\n");
            allbb("marszczy" + jak[1] + " brwi.", jak[1]);
            return 1;
        }

        cele = parse_this(jak[0], "'na' [widok] %l:" + PL_DOP);
        if (sizeof(cele))
        {
            SOULDESC("marszczy" + jak[1] + " brwi");
            actor("Marszczysz" + jak[1] + " brwi na widok", cele, PL_DOP);
            all2actbb("marszczy" + jak[1] + " brwi na widok", cele, PL_DOP,
                      jak[1]);
            targetbb("marszczy" + jak[1] + " brwi na tw�j widok.", cele,
                      jak[1]);
            return 1;
        }
    }

    if (str && sscanf(str, "czo�o %s", str))
    {
        jak = parse_adverb_with_space(str, "z namys�em", 0, ({"k�tem oka"}));
        if (!jak[0])
        {
            SOULDESC("marszczy" + jak[1] + " czo�o");
            write("Marszczysz" + jak[1] + " czo�o.\n");
            allbb("marszczy" + jak[1] + " czo�o.", jak[1]);
            return 1;
        }

        cele = parse_this(jak[0], "'na' [widok] %l:" + PL_DOP);
        if (sizeof(cele))
        {
            if (sizeof(cele) >= 2)
            {
                SOULDESC("marszczy" + jak[1] + " czo�o");
                actor("Marszczysz" + jak[1] + " czo�o na widok", cele, PL_DOP);
                all2actbb("marszczy" + jak[1] + " czo�o na widok", cele, PL_DOP, jak[1]);
                NOTIFY_MULTI2(cele, target, "marszczy" + jak[1] + " czo�o na widok ", PL_DOP, ".", jak[1]);
                return 1;
            }
            else
            {
                SOULDESC("marszczy" + jak[1] + " czo�o");
                actor("Marszczysz" + jak[1] + " czo�o na widok", cele, PL_DOP);
                all2actbb("marszczy" + jak[1] + " czo�o na widok", cele, PL_DOP, jak[1]);
                targetbb("marszczy" + jak[1] + " czo�o na tw�j widok.", cele, jak[1]);
                return 1;
            }
        }
    }

    notify_fail("Zmarszcz co [jak] [na [widok] kogo] ?\n");
    return 0;
}

int
zmruz(string str)
{
    notify_fail(capitalize(query_verb()) + " oczy [jak]?\n");

    if(!str)
        return 0;

	if(TP->query_prop(EYES_CLOSED))
	{
		write("Z zamkni�tymi oczyma?\n");
		return 0;
	}

    string *jak;
    jak = parse_adverb_with_space(str, "jak kot", 1, ({"k�tem oka"}));

    if (jak[0] == "oczy")
    {
        write("Mru�ysz oczy"+jak[1]+".\n");
        allbb("mru�y oczy"+jak[1]+".");
        return 1;
    }

    return 0;
}

int
zwies(string str)
{
    string *jak;
    jak = parse_adverb_with_space(str, "pokornie", 1, ({"k�tem oka"}));
    if (!jak[0] || jak[0] ~= "g�ow�" || !str)
    {
       SOULDESC("ma" + jak[1] + " zwieszon� g�ow�");
        write("Zwieszasz g�ow�" + jak[1] + ".\n");
        allbb("zwiesza g�ow�" + jak[1] + ".", jak[1]);
        return 1;
    }
    notify_fail("Zwie� [g�ow�] [jak] ?\n");
    return 0;
}
/* oryginaly

nomask void
converse_more(string str)
{
    if (str == "**")
    {
        write("Left converse mode.\n");
        return;
    }

    say( ({ METNAME + LD_SAYS + LD_UNDERSTANDS(str) + "\n",
            TART_NONMETNAME + LD_SAYS + LD_UNDERSTANDS(str) + "\n",
            UNSEEN_NAME + LD_SAYS + LD_UNDERSTANDS(str) + "\n" }) );

    write("]");
    input_to(converse_more);
}

int
converse()
{
    write("Entering converse mode.\nGive '**' to stop.\n");
    write("]");
    input_to(converse_more);
    return 1;
}

int
grumble(string str)
{
    object *oblist;
    string *how;

    how = parse_adverb_with_space(str,
        (TP->query_wiz_level() ? "angrily" : "unhappily"), 0);

    if (!stringp(how[0]))
    {
        SOULDESC("grumbling" + how[1]);
        write("You grumble" + how[1] + ".\n");
        all(" grumbles" + how[1] + ".", how[1]);
        return 1;
    }

    oblist = parse_this(how[0], "[at] [the] %l");

    if (!sizeof(oblist))
    {
        notify_fail("Grumble [how] at whom?\n");
        return 0;
    }

    SOULDESC("grumbling" + how[1]);
    actor("You grumble" + how[1] + " at", oblist);
    all2act("grumbles" + how[1] + " at", oblist, 0, how[1]);
    target(" grumbles" + how[1] + " at you.", oblist, how[1]);
    return 1;
}

int
knee(string str)
{
    object *oblist;
    object *femlist;

    oblist = parse_this(str, "[the] %l");

    if (!sizeof(oblist))
    {
        notify_fail("Knee whom?\n");
        return 0;
    }

    femlist = FILTER_GENDER(oblist, G_FEMALE);
    if (sizeof(femlist))
    {
        actor("You try to knee", femlist, ".\nNot very effective though.");
        all2act("tries to knee", femlist, ".\nNot very effective though.");
        target(" tries to knee you, without much effect.", femlist);
    }

    oblist -= femlist;
    if (sizeof(oblist))
    {
        actor("You hit", oblist, " with your knee, sending " +
            ((sizeof(oblist) > 1) ? "them" : "him") +
            " to the ground, writhing in pain!");
        all2act("suddenly raises " + TP->query_possessive() +
            " knee, sending", oblist, " to the floor, writhing in pain!");
        target(" hits you with " + TP->query_possessive() +
            " knee below your belt!\n" +
            "You double over and fall to the ground, writhing in " +
            "excrutiating pain,\nfeeling like you may throw up " +
            "everything you have eaten!", oblist);
    }

    return 1;
}

int
mumble(string str)
{
    object *oblist;

    if (!stringp(str))
    {
        SOULDESC("mumbling about something");
        write("You mumble something about something else.\n");
        all(" mumbles something about something else.");
        return 1;
    }

    if ((strlen(str) > 60) &&
        (!(TP->query_wiz_level())))
    {
        SOULDESC("mumbling about something");
        write("You mumble beyond the end of the line and .\n");
        all(" mumbles something about something else.");
        return 1;
    }

    oblist = parse_this(str, "[about] [the] %l");

    if (!sizeof(oblist))
    {
        SOULDESC("mumbling about something");
        write("You mumble " + str + "\n");
        all(" mumbles " + str);
        return 1;
    }

    SOULDESC("mumbling about someone");
    actor("You mumble something about", oblist);
    all2act("mumbles some about", oblist,
        " and it probably is a good thing you cannot understand it.");
    target(" mumbles about you and it probably is a good thing you cannot " +
        "understand it.", oblist);
    return 1;
}

int
pat(string str)
{
    object *oblist;
    string *zones;
    string one, two;

    zones = ({ "back", "head", "shoulder" });

    notify_fail("Whom are you trying to pat?\n");
    if (!stringp(str))
    {
        str = "head";
    }

    str = lower_case(str);
    if (member_array(str, zones) != -1)
    {
        write("You pat yourself on your " + str + ".\n");
        all(" pats " + TP->query_objective() +
           "self on " + TP->query_possessive() + " " + str + ".");
        return 1;
    }

    if (sscanf(str, "%s %s", one, two) == 2)
    {
        if (member_array(two, zones) == -1)
            return 0;
        str = one;
    }

    if (!stringp(two))
        two = "head";

    oblist = parse_this(str, "[the] %l [on] [the]");
    if (!sizeof(oblist))
    {
        return 0;
    }

    str = ((sizeof(oblist) == 1) ?
           (oblist[0]->query_possessive() + " " + two + ".") :
           ("their " + two + "s."));

    actor("You pat", oblist, " on " + str);
    all2act("pats", oblist, " on " + str);
    target(" pats you on your " + two + ".", oblist);
    return 1;
}

#define PINCH_ZONES ({ "cheek", "ear", "nose", "arm", "bottom" })

int
pinch(string str)
{
    object *oblist;
    string  location;
    string *words;

    if (!stringp(str))
    {
        notify_fail("Pinch whom [where]?\n");
        return 0;
    }

    oblist = parse_this(str, "[the] %l [in] [the] " +
        "[cheek] [ear] [nose] [arm] [bottom]");

    if (!sizeof(oblist))
    {
        notify_fail("Pinch whom [where]?\n");
        return 0;
    }

    words = explode(lower_case(str), " ");
    if (member_array(words[sizeof(words) - 1], PINCH_ZONES) != -1)
    {
        location = words[sizeof(words) - 1];
    }
    else
    {
        location = "cheek";
    }

    actor("You pinch", oblist, "'s " + location + ".");
    all2act("pinches", oblist, "'s " + location + ".");
    target(" pinches your " + location + ".", oblist);
    return 1;
}

#define POKE_ZONES ({ "eye", "ear", "nose", "thorax", "abdomen", \
                "shoulder", "ribs" })

int
poke(string str)
{
    object *oblist;
    string  location;
    string *words;

    if (!stringp(str))
    {
        notify_fail("Poke whom [where]?\n");
        return 0;
    }

    oblist = parse_this(str, "[the] %l [in] [the] [eye] [ear] [nose] " +
        "[thorax] [abdomen] [shoulder] [ribs]");

    if (!sizeof(oblist))
    {
        notify_fail("Poke whom [where]?\n");
        return 0;
    }

    words = explode(str, " ");
    if (member_array(words[sizeof(words) - 1], POKE_ZONES) != -1)
    {
        location = words[sizeof(words) - 1];
    }
    else
    {
        location = "ribs";
    }

    actor("You poke", oblist, " in the " + location + ".");
    all2act("pokes", oblist, " in the " + location + ".");
    target(" pokes you in the " + location + ".", oblist);
    return 1;
}

int
ponder(string str)
{
    object *oblist;

    if (!stringp(str))
    {
        SOULDESC("pondering the situation");
        write("You ponder the situation.\n");
        all(" ponders the situation.");
        return 1;
    }

    if ((strlen(str) > 60) &&
        (!(TP->query_wiz_level())))
    {
        SOULDESC("pondering the situation");
        write("You ponder beyond the end of the line and wake up from " +
            "your reveries.\n");
        all(" ponders the situation.");
        return 1;
    }

    oblist = parse_this(str, "[about] [the] [proposal] [of] [the] %l");

    if (!sizeof(oblist))
    {
        SOULDESC("pondering about something");
        write("You ponder " + str + "\n");
        all(" ponders " + str);
        return 1;
    }

    SOULDESC("pondering about a proposal");
    actor("You ponder about the proposal of", oblist);
    all2act("ponders about the proposal of", oblist);
    target(" ponders about your proposal.", oblist);
    return 1;
}

int
rsay(string str)
{
    int     index;
    int     size;
    object *oblist;
    string  race = TP->query_race_name();
    string  pos = TP->query_possessive();
    int     skill;
    string  *words;
    int     sentence_size;
    int     sentence_index;
    string  to_print;

    if (!objectp(environment(TP)))
        return 0;

    if (!stringp(str))
    {
        notify_fail("Say what in your racial tongue?\n");
        return 0;
    }

    oblist = FILTER_OTHER_LIVE(all_inventory(environment(TP)));
    words = explode(str, " ") - ({ "" });
    sentence_size = sizeof(words);

    index = -1;
    size = sizeof(oblist);
    while(++index < size)
    {
        if ((race == oblist[index]->query_race_name()) ||
            (oblist[index]->query_wiz_level()) ||
            ((skill = oblist[index]->query_skill(SS_LANGUAGE)) >=
                LANGUAGE_ALL_RSAY))
        {
            tell_object(oblist[index],
                TP->query_Imie(oblist[index]) + " says in " +
                pos + " own tongue: " + str + "\n");
            break;
        }

        if (skill < LANGUAGE_MIN_RSAY)
        {
            tell_object(oblist[index],
                TP->query_Imie(oblist[index]) +
                " says something completely incomprehensible.\n");
            break;
        }

        skill -= LANGUAGE_MIN_RSAY;
        to_print = "";
        sentence_index = -1;
        while(++sentence_index < sentence_size)
        {
            if (random(LANGUAGE_ALL_RSAY - LANGUAGE_MIN_RSAY) >= skill)
            {
                to_print += " " + words[sentence_index];
            }
            else
            {
                to_print += (" " +
                    extract("....................", 1,
                            strlen(words[sentence_index])));
            }
        }

        tell_object(oblist[index],
            TP->query_Imie(oblist[index]) +
            " says in " + pos + " own tongue:" + to_print + "\n");
    }

    if (TP->query_option(OPT_ECHO))
        write("You rsay: " + str + "\n");
    else
        write("Ju�.\n");

    return 1;
}

int
scratch(string str)
{
    object *oblist;
    string *zones;
    string one, two;

    zones = ({ "head", "chin", "back", "behind", "nose" });

    if (!stringp(str))
    {
        str = "head";
    }

    if (member_array(str, zones) != -1)
    {
        write("You scratch your " + str + ".\n");
        allbb(" scratches " + TP->query_possessive() +
              " " + str + ".");
        return 1;
    }

    notify_fail("Scratch [whom] where?\n");
    if (sscanf(str, "%s %s", one, two) == 2)
    {
        if (member_array(two, zones) == -1)
        {
            return 0;
        }
    }

    if (!stringp(two))
    {
        two = "head";
    }

    oblist = parse_this(one, "[the] %l [at] [the]");

    if (!sizeof(oblist))
    {
        return 0;
    }

    actor("You scratch", oblist, "'s " + two + ".");
    all2act("scratches", oblist, "'s " + two + ".");
    target(" scratches your " + two + ".", oblist);
    return 1;
}

int
tackle(string str)
{
    object *oblist;

    oblist = parse_this(str, "[the] %l");

    if (!sizeof(oblist))
    {
        notify_fail("Tackle whom?\n");
        return 0;
    }

    if (random(7))
    {
        actor("You tackle", oblist);
        all2act("tackles", oblist, ". " +
            ((sizeof(oblist) > 1) ? "They fall": "The latter falls") +
            " to the ground in a very unflattering way.");
        target(" comes running at you. " +
            capitalize(TP->query_pronoun()) +
            " attempts to tackle you and succeeds. You fall to the ground " +
            "in a very unflattering way.", oblist);
    }
    else
    {
        actor("You try to tackle", oblist, " but fall flat on your face.");
        all2act("tries to tackle", oblist, " but misses and falls flat on " +
            TP->query_possessive() + " face.");
        target(" comes running at you. " +
            capitalize(TP->query_pronoun()) +
            " attempts to tackle you but misses and falls flat on " +
            TP->query_possessive() + " face.", oblist);
    }

    return 1;
}

int
think(string str)
{
    if (!stringp(str))
    {
        write("You try to look thoughtful but fail.\n");
        allbb(" tries to look thoughtful but fails.");
        SOULDESC("trying to look thoughtful");
        return 1;
    }

    if ((strlen(str) > 60) &&
        (!(TP->query_wiz_level())))
    {
        write("Geez.. That is a lot to think about at the same time.\n");
        allbb(" looks like " + TP->query_pronoun() +
            " is trying to think hard about a lot of things.");
        SOULDESC("thinking about a lot of things");
        return 1;
    }

    write("You think hard about " + str + "\n");
    allbb(" looks like " + TP->query_pronoun() +
        " is thinking hard about " + str);
    SOULDESC("thinking hard about something");
    return 1;
}

int
tickle(string str)
{
    object *oblist;
    string *how;
    string location;
    int    i;

    oblist = parse_this(str, "[the] %l [in] [under] [the] " +
        "'feet' / 'foot' / 'chin' / 'abdomen' / 'belly' / 'side'");
    if (sizeof(oblist))
    {
            how = explode(str, " ");
        switch(location = how[sizeof(how) - 1])
        {
        case "chin":
            oblist->add_prop(LIVE_S_SOULEXTRA, "smiling sweetly");
            target(" tickles you under your chin. " +
                "You smile sweetly in return.", oblist);
            actor("You tickle", oblist, " under the chin. " +
                ((sizeof(oblist) == 1) ?
                    (capitalize(oblist[0]->query_pronoun()) + " smiles") :
                    "They smile") + " sweetly in return.");
            all2act("tickles", oblist, " under the chin. " +
                ((sizeof(oblist) == 1) ? "The latter smiles" :
                    "They smile") + " sweetly in return.");
            return 1;

        case "feet":
        case "foot":
            oblist->add_prop(LIVE_S_SOULEXTRA, "laughing uncontrollably");
            target(" tickles you under your " + location + ". " +
                "You fall down, laughing uncontrollably.", oblist);
            actor("You tickle", oblist, " under the " + location + ". " +
                ((sizeof(oblist) == 1) ?
                    (capitalize(oblist[0]->query_pronoun()) + " falls") :
                    "They fall") + " down, laughing uncontrollably.");
            all2act("tickles", oblist, " under the " + location + ". " +
                ((sizeof(oblist) == 1) ? "The latter falls" :
                    "They fall") + " down, laughing uncontrollably.");
            return 1;

        case "abdomen":
        case "belly":
        case "side":
            oblist->add_prop(LIVE_S_SOULEXTRA, "giggling merrily");
            target(" tickles you in your " + location + ". " +
                "You start giggling merrily.", oblist);
            actor("You tickle", oblist, " in the " + location + ". " +
                ((sizeof(oblist) == 1) ?
                    (capitalize(oblist[0]->query_pronoun()) + " starts") :
                    "They start") + " giggling merrily.");
            all2act("tickles", oblist, " in the " + location + ". " +
                ((sizeof(oblist) == 1) ? "The latter starts" :
                    "They start") + " giggling merrily.");
            return 1;

        default:
            notify_fail("Tickle whom [where / how]? Rather... this should " +
                "not happen. Please make a sysbugreport about this.\n");
            return 0;
        }

        return 1;
    }

    how = parse_adverb_with_space(str, "playfully", 1);

    oblist = parse_this(how[0], "[the] %l");

    if (!sizeof(oblist))
    {
        notify_fail("Tickle whom [how / where]?\n");
        return 0;
    }

    oblist->add_prop(LIVE_S_SOULEXTRA, "laughing");
    actor("You tickle", oblist, how[1] + ". " +
        ((sizeof(oblist) == 1) ?
            (capitalize(oblist[0]->query_pronoun()) + " falls") :
            "They fall") + " down laughing, rolling over in an attempt to " +
            "evade your tickling fingers.");
    all2act("tickles", oblist, how[1] + ". " +
        ((sizeof(oblist) == 1) ? "The latter falls" :
            "They fall") + " down laughing, rolling over in an attempt to " +
            "evade the tickling fingers.", how[1]);
    target(" tickles you" + how[1] + ". You fall down laughing and roll " +
        "over in an attempt to evade those tickling fingers.", oblist,
        how[1]);

    return 1;
}
*/

// generic
/*
int
zaklnij(string str)
{
    string jak;

    jak = check_adverb_with_space(str, "sennie");
    if (jak != NO_ADVERB_WITH_SPACE)
    {
        SOULDESC("klnie" + jak);
        write("Klniesz" + jak + ".\n");
        all("klnie" + jak + ".", jak);
        return 1;
    }
    notify_fail("Ziewnij [jak] ?\n");
    return 0;
}
*/
