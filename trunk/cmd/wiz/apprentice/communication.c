/*
 * /cmd/wiz/apprentice/communication.c
 *
 * This is a sub-part of /cmd/wiz/apprentice.c
 *
 * This file contains the commands that allow wizards to communicate.
 *
 * Commands currently included:
 * - audience
 * - busy
 * - emote (short ':')
 * - dwiz
 * - dwize
 * - kwiz
 * - kwize
 * - line
 * - linee
 * - lineconfig
 * - next
 * - rwiz
 * - rwize
 * - tell
 * - send
 * - wiz
 * - wize
 * - wsay
 * - wsend
 */

#include <composite.h>
#include <filter_funs.h>
#include <flags.h>
#include <std.h>
#include <stdproperties.h>
#include <options.h>
#include <colors.h>

//kolorek
#define JMAG set_color(COLOR_FG_MAGENTA) + set_color(COLOR_BOLD_ON)
#define CL clear_color()
#define U set_color(COLOR_UNDERLINE_ON)

//Je�li to wy��czymy to emote b�dzie dzia�a� tak jak dawniej, czyli
//nie b�dzie zamienia� nazw obiektu na obiekty.
#define USE_PARSE_TO_FIND_OBJECTS   1

#define NFW(x)                      notify_wizards(this_object(), (x));

/* **************************************************************************
 * Prototypes.
 */
nomask varargs int line(string str, int emotion);
static nomask mixed mk_item(object ob);

/* **************************************************************************
 * Global variable.
 *
 * channels = ([ (string) lower case channel name :
 *                              ({ (string)   channel name,
 *                                 (string *) users,
 *                                 (string *) hidden users,
 *                                 (int 0/1)  status open/closed,
 *                                 (string)   owner name
 *                               }) ])
 */
static private mapping channels;

#define CHANNEL_OPEN    (0) /* channel is open for all wizard.            */
#define CHANNEL_CLOSED  (1) /* channel is closed unless after invitation. */

#define CHANNEL_NAME    (0) /* the name of the channel.                   */
#define CHANNEL_USERS   (1) /* the users of the channel.                  */
#define CHANNEL_HIDDEN  (2) /* the hidden users of the channel.           */
#define CHANNEL_STATUS  (3) /* the status of the channel (open/closed).   */
#define CHANNEL_OWNER   (4) /* the owner of the channel.                  */

#define CHANNEL_WIZRANK ({ WIZNAME_APPRENTICE, WIZNAME_LORD, WIZNAME_ARCH })

/*
 * Nazwa funkcji: init_line
 * Opis         : This function is called when the soul is created. It
 *                makes sure the mapping with the channels is loaded.
 *                Also, every time this object is created, all members
 *                of the channels are checked to see whether they are
 *                still wizards.
 */
private nomask void
init_line()
{
    int size;
    string *lines;
    string *wizards = SECURITY->query_wiz_list(-1);

    /* Get the channels information from SECURITY. */
    channels = SECURITY->query_channels();

    if (!mappingp(channels))
    {
        channels = ([ ]);
    }

    /* Every time this object is loaded into memory we check whether the
     * people in the channels are still real wizards.
     */
    lines = m_indices(channels);
    size = sizeof(lines);
    while(--size >= 0)
    {
        channels[lines[size]][CHANNEL_USERS] &= wizards;
        channels[lines[size]][CHANNEL_HIDDEN] &= wizards;
    }

    /* It is not likely that this information changes each time, so we
     * do not have to save this information every time we process it.
     */
    if (!random(20))
        SECURITY->set_channels(channels);
}

/* **************************************************************************
 * audience - queue somone for talks.
 */
nomask int
audience(string who)
{
    string *queue_list, name;
    int index, size;
    object ob, *obs;

    queue_list = (string *)this_interactive()->query_queue_list();
    if (!stringp(who))
    {
	if (!sizeof(queue_list))
	{
	    write("Nikt nie oczekuje na audiencje u Ciebie.\n");
	    return 1;
	}

	size = sizeof(queue_list);
	index = -1;
	while(++index < size)
	{
	    write((index + 1) + ": " + capitalize(queue_list[index]) + "\n");
	}
	return 1;
    }

    if (who == "list")
    {
	obs = filter(users(), objectp);

	size = sizeof(obs);
	index = -1;
	while(++index < size)
	{
	    if (member_array(this_interactive()->query_real_name(),
		obs[index]->query_queue_list()) == -1)
	    {
		obs[index] = 0;
	    }
	}
	obs -= ({ 0 });
	if (!sizeof(obs))
	{
	    write("Nie oczekujesz na audiencje u nikogo.\n");
	    return 1;
	}
	write("Oczekujesz na audiencje u: " +
	    COMPOSITE_WORDS(map(obs, capitalize @ &->query_real_name())) +
	    ".\n");
	return 1;
    }

    if (sscanf(who, "anuluj %s", name) == 1)
    {
	name = lower_case(name);
	ob = find_player(name);
	if (ob == this_interactive())
	{
	    write("Nie mo�esz oczekiwa� na audiencje u siebie!\n");
	    return 1;
	}
	if (!ob)
	{
	    write("Nie mog� znale�� wiza:" + name + ".\n");
	    return 1;
	}
	if (ob->pop_queue(this_interactive()->query_real_name()) == "")
	{
	    write("Nie oczekujesz na audiencje u " + ob->query_imie(TP, PL_DOP) +
            capitalize(name) + ".\n");
	    return 1;
	}

	write("Anulowa�e� oczekiwanie na audiencje u " +
	      ob->query_imie(TP, PL_DOP) + ".\n");
	tell_object(ob, this_interactive()->query_cap_name() +
	    " anulowa� " + this_interactive()->query_possessive() +
	    " oczekiwanie na audiencje u Ciebie.\n");
	return 1;
    }

    who = lower_case(who);
    ob = find_player(who);
    if (ob == this_interactive())
    {
	write("No, you are not availble for consultations to yourself.\n");
	return 1;
    }
    if (!objectp(ob))
    {
	write("Couldn't locate " + who + ".\n");
	return 1;
    }
    index = ob->add_queue(this_interactive()->query_real_name());
    if (!index)
    {
	write("You are already requesting an audience with " +
	    capitalize(who) + ".\n");
    }
    else
    {
	write("You have been queued as number " + index + ".\n");
	tell_object(ob, this_interactive()->query_cap_name() +
	    " is requesting an audience with you.\n");
    }
    return 1;
}

/* **************************************************************************
 * busy - set/query your busy status
 */

/*
 * Nazwa funkcji: busy_string
 * Opis         : Converts an the busy flag into a string.
 * Argumenty    : int    - the busy level
 * Zwraca       : string - its string representation.
 */
public nomask string
busy_string(int busy)
{
    string bstring = "";

    if (busy & BUSY_F)
	return "F";

    if (busy & BUSY_W)
	bstring += "W";
    if (busy & BUSY_T)
	bstring += "T";
    if (busy & BUSY_M)
	bstring += "M";
    if (busy & BUSY_P)
	bstring += "P";
    if (busy & BUSY_S)
	bstring += "S";
    if (busy & BUSY_C)
	bstring += "C";
    if (busy & BUSY_G)
	bstring += "G";
    if (busy & BUSY_I)
	bstring += "I";
    if (busy & BUSY_L)
	bstring += "L";

    return bstring;
}

nomask int
busy(string what)
{
    int index;
    int size;
    int busy;

    CHECK_SO_WIZ;

    busy = this_interactive()->query_prop(WIZARD_I_BUSY_LEVEL);

    if (!stringp(what))
    {
	if (!busy)
	{
	    write("You're in an attentive state of mind.\n");
	    return 1;
	}

	write("Busy: " + busy_string(busy) + "\n");
	return 1;
    }

    if (what == "clear")
    {
	if (!busy)
	{
	    write("You are already in an attentive state of mind.\n");
	    return 1;
	}

	this_interactive()->remove_prop(WIZARD_I_BUSY_LEVEL);
	write("Cleared your busy status.\n");
	return 1;
    }

    size = strlen(what);
    index = -1;

    while(++index < size)
    {
	switch(what[index])
	{

	case 'W':
	    busy ^= BUSY_W;
	    break;

	case 'T':
	    busy ^= BUSY_T;
	    break;

	case 'M':
	    busy ^= BUSY_M;
	    break;

	case 'P':
	    busy ^= BUSY_P;
	    break;

	case 'S':
	    busy ^= BUSY_S;
	    break;

	case 'C':
	    busy ^= BUSY_C;
	    break;

	case 'G':
	    busy ^= BUSY_G;
	    break;

	case 'I':
	    busy ^= BUSY_I;
	    break;

	case 'L':
	    busy ^= BUSY_L;
	    break;

	case 'F':
	    busy = BUSY_ALL;
	    break;

	default:
	    write("Strange busy state: " + extract(what, index, index) + "\n");
	    break;
	}
    }

    this_interactive()->add_prop(WIZARD_I_BUSY_LEVEL, busy);

    write("New busy status: " + busy_string(busy) + ".\n");
    return 1;
}

/* **************************************************************************
 * emote, : - emote something
 */
nomask int
emote(string arg)
{
    object  pl, *pls;
    string *args, *nms, *plas, to_write, oarg, pname;
    int     i, j, sz, up;
    mapping emap;
    mixed   przyp;

    CHECK_SO_WIZ;

    pls = FILTER_LIVE(all_inventory(environment(this_player()))) - ({ this_player() });
    oarg = arg;

    if (!stringp(arg))
        return notify_fail("Niepoprawne u�ycie emote. Zobacz do ?emote aby dowiedzie� si� jak poprawnie u�ywa� tej"+
            "komendy.\n");

    arg = implode(explode(arg + ".", "."), " .");

#if USE_PARSE_TO_FIND_OBJECTS
    //Jako, �e nie ma potrzeby sprawdzania tekst�w, kt�re znajduj� si� w "|", bo zostanie
    //to sprawdzone poni�ej. Usuwamy wi�c je wszystkie podstawiaj�c w ich miejsce znak $.
    string *in_l = explode(arg, "|");
    arg = "";
    int size = sizeof(in_l);
    for(i=0,j=0;i<size;i++)
    {
        if((i%2))
        {
            j++;
            arg += "$";
        }
        else
        {
            arg += in_l[j];
            in_l = exclude_array(in_l, j, j);
        }
    }
    //Sprawdzamy do czasu, a� znajdziemy wszystkie obiekty.
    //I sprawdzamy przypadki, mo�e by� troche problem z
    //dok�adnym okre�leniem mianownika i biernika, w przypadku
    //kiedy s� one identyczne, ale nie robi nam to �adnego problemu
    //bo bez r�nicy kt�rego u�yjemy.
    object *emote_ob;
    mixed emote_obs = ({});
    string prefix, postfix;
    for(przyp=0;przyp<6;przyp++)
    {
        while(parse_command(lower_case(arg), ENV(TP), "%s %i:" + przyp + " %s", prefix, emote_ob, postfix))
        {
            emote_ob = NORMAL_ACCESS(emote_ob, 0, 0);

            if(!sizeof(emote_ob))
                continue;

            emote_obs += ({ ({emote_ob, przyp}) });

            arg = prefix + " # " + postfix;
        }
    }

    args = explode(arg, "$");
    if(sizeof(args))
    {
        arg = "";
        to_write = "";
        if(sizeof(args)-1 != sizeof(in_l))
            NFW("Wyst�pi� b��d podczas przetwarzania free emota z |.");
        for(i=0,j=0;i<sizeof(args);i++)
        {
            arg += args[i];
            if(j<sizeof(in_l))
                arg += "|" + in_l[j++] + "|";
        }
    }
#endif USE_PARSE_TO_FIND_OBJECTS

    //Teraz robimy standardowe sprawdzenie.
    args = explode(arg, "|");

    if ((sz = sizeof(args)) > 1)
    {
        arg = "";
        to_write = "";
        for (i = 0 ; i < sz ; i ++)
        {
            nms = explode(args[i], " ");
            up = nms[0] == lower_case(nms[0]) ? 0 : 1;
            nms[0] = lower_case(nms[0]);
            if(wildmatch("*:*", nms[0]))
            {
                plas = explode(nms[0], ":");
                nms[0] = plas[0];
                przyp = lower_case(plas[1]);
                if(wildmatch("pl_*", przyp))
                {
                    switch(przyp[0..5])
                    {
                        case "pl_mia": przyp = PL_MIA; break;
                        case "pl_dop": przyp = PL_DOP; break;
                        case "pl_cel": przyp = PL_CEL; break;
                        case "pl_bie": przyp = PL_BIE; break;
                        case "pl_nar": przyp = PL_NAR; break;
                        case "pl_mie": przyp = PL_MIE; break;
                        default: przyp = PL_MIA;
                    }
                }
                else
                    przyp = max(0, min(5, atoi(przyp)));
            }
            else
                przyp = 0;

            object *ob;
            notify_fail("Nie widzisz tu nigdzie " + nms[0] + ".\n");

            if(parse_command(nms[0], ENV(TP), "%i:" + PL_MIA, ob))
            {
                ob = NORMAL_ACCESS(ob, 0, 0);
                if(!ob)
                    return 0;
                to_write += COMPOSITE_STH(ob, przyp);
                arg += QCOMPSTH(przyp);
            }
            else if (i % 2)
                return 0;
            else
            {
                arg += args[i];
                to_write += args[i];
            }
        }
    }
#if USE_PARSE_TO_FIND_OBJECTS
    //A teraz za znaki # podstawiamy odpowiadaj�ce im obiekty w odpowiednich przypadkach:)
    args = explode(arg, "#");
    if(sizeof(args))
    {
        arg = "";
        to_write = "";
        if(sizeof(args)-1 != sizeof(emote_obs))
            NFW("Wyst�pi� b��d podczas przetwarzania parsa w free emocie.");
        for(i=0,j=0;i<sizeof(args);i++)
        {
            arg += args[i];
            if(j<sizeof(emote_obs))
            {
                to_write += COMPOSITE_STH(emote_obs[j][0], emote_obs[j][1]);
                arg += QCOMPSTH(emote_obs[j++][1]);
            }
        }
    }
    przyp = 0;
#endif USE_PARSE_TO_FIND_OBJECTS

    arg = implode(explode(arg + " .", " ."), ".");
    //Przerobi�em na say'a, nie wiem po kij by�y jakie� kombinacje alpejskie.
    if (this_player()->query_option(OPT_ECHO))
        write("Emotujesz: " + TP->query_name(PL_MIA) + " " + oarg + "\n");
    else
        write("Ju�.\n");

    say(QCIMIE(TP, PL_MIA) + " " + arg + "\n");
    return 1;
}

static nomask mixed
mk_item(object ob)
{
    return ({ ob, "" });
}

nomask varargs int line(string str, int emotion = 0, int busy_level = 0);
/**
 * alias do line 'nazwa domeny wiza'
 */
nomask int
dwiz(string str)
{
    string domain = SECURITY->query_wiz_dom(TP->query_real_name());

    return line(domain + " " + str, (query_verb() == "dwize"));
}

/**
 * alias do line rada
 */
nomask int
kwiz(string str)
{
    if(SECURITY->query_wiz_rank(TP->query_real_name()) < WIZ_ARCH &&
       member_array(TP->query_real_name(), channels["kapitula"][CHANNEL_USERS] +
       channels["kapitula"][CHANNEL_HIDDEN] + ({channels["kapitula"][CHANNEL_OWNER]})) == -1)
    {
        return 0;
    }

    return line("kapitula " + str, (query_verb() == "kwize"));
}

/* **************************************************************************
 * line  - speak on any of the lines.
 * linee - emote on any of the lines.
 */

/*
 * Nazwa funkcji: line_list
 * Opis         : Display the people listening to a certain line. At this
 *                point we are sure that the line in question exists.
 * Argumenty    : string line - the list to display.
 * Zwraca       : int 1/0 - true if the line was displayed to the player.
 */
static nomask int
line_list(string line)
{
    string name = this_interactive()->query_real_name();
    int    access = (SECURITY->query_wiz_rank(name) >= WIZ_ARCH || channels[line][CHANNEL_OWNER] == name);
    /*    || (channels[line][CHANNEL_OWNER] == name); */
    string *names;
    string *nonpresent;
    string str;

    /* Closed channel that the wizard is not entitled to list. */
    if ((channels[line][CHANNEL_STATUS] == CHANNEL_CLOSED) &&
        (!access) &&
        (member_array(name, channels[line][CHANNEL_USERS]) == -1) &&
        (member_array(name, channels[line][CHANNEL_HIDDEN]) == -1))
    {
        return 0;
    }

    names = channels[line][CHANNEL_USERS] & users()->query_real_name();
    nonpresent = (string *)channels[line][CHANNEL_USERS] - names;

    str = sprintf("%-8s: %-1s:", channels[line][CHANNEL_NAME],
    	(channels[line][CHANNEL_STATUS] ? "C" : "O"));
    str += (sizeof(names) ? (" " +
    	implode(map(sort_array(names), capitalize), ", ")) : "");
    str += (sizeof(nonpresent) ? (" NIEOBECNI " +
    	implode(map(sort_array(nonpresent), capitalize), ", ")) : "");

    /* Test for hidden listeners if the person is entitled to do so. */
    if (access)
    {
	names = channels[line][CHANNEL_HIDDEN] & users()->query_real_name();
	nonpresent = (string *)channels[line][CHANNEL_HIDDEN] - names;

	if (sizeof(names) ||
	    sizeof(nonpresent))
	{
	    str += " UKRYCI";
	    str += (sizeof(names) ? (" " +
		implode(map(sort_array(names), capitalize), ", ")) : "");
	    str += (sizeof(nonpresent) ? (" NIEOBECNI" +
		implode(map(sort_array(nonpresent), capitalize), ", ")) : "");
	}
    }
    /* Else see whether the player is hidden himself. */
    else if (member_array(name, channels[line][CHANNEL_HIDDEN]) >= 0)
    {
    	str += " UKRYCI " + capitalize(name);
    }

    str += " W�A�CICIEL " + capitalize(channels[line][CHANNEL_OWNER]);

    if (strlen(str) <= 77)
    {
	write(str + "\n");
    }
    else
    {
	names = explode(break_string(str, 77), "\n");
	write(implode( ({ names[0] }) +
	    (explode(break_string(implode(names[1..], " "), 62), "\n")),
	    ("\n             ")) + "\n");
    }

    return 1;
}

nomask varargs int
line(string str, int emotion = 0, int busy_level = 0)
{
    string *members;
    string  line;
    int     size;
    int     rank;
    int     line_type;
    object  wizard;
    object *receivers;

    CHECK_SO_WIZ;

    if (!stringp(str))
    {
        notify_fail(capitalize(query_verb()) + " co?\n");
        return 0;
    }

    if (sscanf(str, "%s %s", line, str) != 2)
    {
        line = str;
        str = "";
    }
    /* Channel is a wizline-channel. */
    if ((rank = member_array(line, CHANNEL_WIZRANK)) >= 0)
    {
        rank = WIZ_R[member_array(line, WIZ_N)];
        members = filter(users(), &operator(!=)(, "logon") @ &->query_real_name());
        members = map(members, geteuid);
        members = filter(members, &operator( >= )(, rank) @
            SECURITY->query_wiz_rank);
        line = ((line == WIZNAME_APPRENTICE) ? "Wizline" : capitalize(line));
    }
    /* Channel is domain-channel. */
    else if (SECURITY->query_domain_number(line) > -1)
    {
        rank = 1;
        line = capitalize(line);
        members = SECURITY->query_domain_members(line);
        members &= map(users(), geteuid);
    }
    /* Channel is normal type of channel, well, you know what I mean. */
    else if (pointerp(channels[lower_case(line)]))
    {
        line = lower_case(line);
        line_type = 1;
        members = channels[line][CHANNEL_USERS] +
            channels[line][CHANNEL_HIDDEN]; //+ ({channels[line][CHANNEL_OWNER]}); <-w�a�ciciel by widzia� podwojnie.
        members &= map(users(), geteuid);
            line = channels[line][CHANNEL_NAME];
    }
    else
    {
        notify_fail("Kana� o nazwie '" + line + "' nie istnieje.\n");
        return 0;
    }
    /* No message means list the particular line. */
    if (!strlen(str))
    {
        if (rank != -1)
        {
            notify_fail("You cannot list domain or wizard lines this way.\n");
            return 0;
        }

        if (line_list(lower_case(line)))
            return 1;

        notify_fail("Kana� o nazwie '" + line + "' nie istnieje.\n");
        return 0;
    }
    if(line_type)
    {
        if((channels[lower_case(line)][CHANNEL_STATUS] == CHANNEL_CLOSED) &&
            ((SECURITY->query_wiz_rank(TP->query_real_name()) < WIZ_ARCH) &&
            (member_array(TP->query_real_name(), map(members, lower_case)) == -1)))
        {
            notify_fail("Kana� o nazwie '" + line + "' nie istnieje.\n");
            return 0;
        }
    }
    receivers = filter(map(members, find_player), not @ &operator(&)(busy_level | BUSY_F) @
        &->query_prop(WIZARD_I_BUSY_LEVEL)) - ({TP});

    if (!(size = sizeof(receivers)))
    {
        notify_fail("Nie ma nikogo kto mog�by ci� wys�ucha� na " +
            (line == "Wizline" ? "" : "kanale ") + "'" + line + "'.\n");
        return 0;
    }

    //zmianki do kolorowania. Vera
    str = set_color(COLOR_FG_MAGENTA) + set_color(COLOR_BOLD_ON) + (line == "Wizline" ? "@ " : "<" + line + "> ") +
        explode(ctime(time()), " ")[-1][..-4] + " " + capitalize(this_player()->query_real_name()) +
        ((emotion ? emotion : (query_verb() == "linee")) ? " " : ": ") +
        clear_color() + str + "\n";

    if (this_player()->query_option(OPT_ECHO))
        TP->catch_msg(str); //Catch_msg bo chwyta vbfc.
    else
        write("Ju�.\n");

    receivers->catch_msg(str);

    return 1;
}

/* **************************************************************************
 * lineconfig - configurate any of the lines.
 */
nomask int
lineconfig(string str)
{
    string cmd;
    string name = this_player()->query_real_name();
    int    rank = SECURITY->query_wiz_rank(name);
    string line;
    string *lines;
    string target;
    int    hide = 0;
    int    index;
    int    size;

    if (!stringp(str))
    {
        notify_fail("Kt�r� subkomende do lineconfig chcesz u�y�?\n");
        return 0;
    }

    if (sscanf(str, "%s %s", cmd, str) != 2)
    {
        cmd = str;
        str = "";
    }

    switch(cmd)
    {
        case "hadd":
            if (rank < WIZ_ARCH)
            {
                notify_fail("Nie mo�esz doda� do lini nikogo ukrytego.\n");
                return 0;
            }
            hide = 1;
            /* Intentionally no break or return. Fall into the next case: add */

        case "add":
            if (!strlen(str) || (sscanf(str, "%s to %s", target, line) != 2))
            {
                notify_fail("Sk�adnia: lineconfig add <imie> to <kana�>\n");
                return 0;
            }
            if (!pointerp(channels[line = lower_case(line)]))
            {
                notify_fail("Brak lini nazwanej: '" + line + "'.\n");
                return 0;
            }
            if ((rank < WIZ_ARCH) && (channels[line][CHANNEL_OWNER] != name))
            {
                notify_fail("Nie jeste� w�a�cicielem lini nazwanej: '" + line + "'.\n");
                return 0;
            }
            if (!(SECURITY->query_wiz_rank(target = lower_case(target))))
            {
                notify_fail("Nie ma czarodzieja o imieniu '" + capitalize(target) + "'.\n");
                return 0;
            }

            channels[line][(hide ? CHANNEL_HIDDEN : CHANNEL_USERS)] += ({ target });
            SECURITY->set_channels(channels);

            write(capitalize(target) + " zosta� dodany " +
                (hide ? "jako ukryty " : "") + "do kana�u " +
                channels[line][CHANNEL_NAME]  + ".\n");
            return 1;

        case "create":
            if (rank < WIZ_NORMAL)
            {
                notify_fail("Tylko pe�ni wizardzi i czarodzieje o wy�szym poziomie mog� dodawa� swoje linie.\n");
                return 0;
            }
            if (!strlen(str) || (sscanf(str, "%s %s", target, str) != 2))
            {
                notify_fail("Sk�adnia: lineconfig create open/closed <nazwa_kana�u>\n");
                return 0;
            }
            if ((strlen(str) < 2) || (strlen(str) > 8))
            {
                notify_fail("Nazwa kana�u nie mo�e by� kr�trza niz 2 i d�u�sza ni� 8 znak�w..\n");
                return 0;
            }
            if (pointerp(channels[line = lower_case(str)]))
            {
                notify_fail("Kana� o nazwie '" + line + "' ju� istnieje.\n");
                return 0;
            }
            if (SECURITY->query_domain_number(line) > -1)
            {
                notify_fail("Istnieje domena o nazwie '" +
                capitalize(line) + "' nie mo�na utworzy� kana�u z t� nazw�.\n");
                return 0;
            }
            if (member_array(line, WIZ_N) >= 0)
            {
                notify_fail(capitalize(line) + " jest nazw� rankingu czarodziej�w.\n");
                return 0;
            }
            hide = ((target == "closed") ? CHANNEL_CLOSED : CHANNEL_OPEN);

            channels[line] = ({ str, ({ name }), ({ }), hide, name });
            SECURITY->set_channels(channels);

            write("Utworzono " + (hide ? "zamkni�ty" : "otwarty") + " kana� nazwany: '" +
                str + "'.\n");
            return 1;

        case "expel":
            if (!strlen(str) || (sscanf(str, "%s from %s", target, line) != 2))
            {
                notify_fail("Sk�adnia: lineconfig expel <imie> from <kana�>\n");
                return 0;
            }
            if (!pointerp(channels[line = lower_case(line)]))
            {
                notify_fail("Kana� o nazwie '" + line + "' nie istnieje.\n");
                return 0;
            }
            if ((rank < WIZ_ARCH) && (channels[line][CHANNEL_OWNER] != name))
            {
                notify_fail("Nie jeste� w�a�cicielem tego kana�u '" + line + "'.\n");
                return 0;
            }
            if (member_array(target, channels[line][CHANNEL_USERS]) >= 0)
            {
                hide = 0;
            }
            else if ((rank >= WIZ_ARCH) &&
                (member_array(target, channels[line][CHANNEL_HIDDEN]) >= 0))
            {
                hide = 1;
            }
            else
            {
                notify_fail(capitalize(target) +
                " nie jest dyskusantem " +
                channels[line][CHANNEL_NAME] + ".\n");
                return 0;
            }

            channels[line][(hide ? CHANNEL_HIDDEN: CHANNEL_USERS)] -= ({ target });
            SECURITY->set_channels(channels);

            write(UC(target) + " zosta� wyrzucony z listy dyskusant�w kana�u: '" +
                channels[line][CHANNEL_NAME] + "'." + (hide ? " By� on jego ukrytym dyskusantem" : "") + "\n");
            return 1;

        case "hjoin":
            if (rank < WIZ_ARCH)
            {
                notify_fail("Nie mo�esz zosta� ukrytym dyskusantem �adnego kana�u, chyba "+
                        "�e zostaniesz jako taki dodany przez kogo� z administracji.\n");
                return 0;
            }
            hide = 1;
            /* Intentionally no break or return. Use the next case: join */

        case "join":
            if (!pointerp(channels[line = lower_case(str)]))
            {
                notify_fail("Kana� o nazwie '" + line + "' nie istnieje.\n");
                return 0;
            }
            if (member_array(name, channels[line][CHANNEL_USERS]) >= 0)
            {
                notify_fail("Ale� ty ju� jeste� cz�onkiem kana�u o tej nazwie.\n");
                return 0;
            }
            if (member_array(name, channels[line][CHANNEL_HIDDEN]) >= 0)
            {
                notify_fail("Ale� jeste� ju� ukrytym cz�onkiem kana�u o tej nazwie.\n");
                return 0;
            }
            if ((channels[line][CHANNEL_STATUS] == CHANNEL_CLOSED) &&
                (rank < WIZ_ARCH) &&
                (channels[line][CHANNEL_OWNER] != name))
            {
                notify_fail("Kana� o nazwie '" + channels[line][CHANNEL_NAME] +
                "' jest zamkni�tu, jedynie jego w�asciciel - " +
                capitalize(channels[line][CHANNEL_OWNER]) + " mo�e Ci� do "+
                "niego doda�.\n");
                return 0;
            }

            channels[line][(hide ? CHANNEL_HIDDEN : CHANNEL_USERS)] += ({ name });
            SECURITY->set_channels(channels);

            write("Do��czy�e� do kana�u " + channels[line][CHANNEL_NAME] +
                (hide ? " jako jego ukryty dyskusant" : "") + ".\n");
            return 1;

        case "leave":
            if (!pointerp(channels[line = lower_case(str)]))
            {
                notify_fail("Kana� o nazwie '" + line + "' nie istnieje.\n");
                return 0;
            }
            if (member_array(name, channels[line][CHANNEL_USERS]) >= 0)
                hide = 0;
            else if (member_array(name, channels[line][CHANNEL_HIDDEN]) >= 0)
                hide = 1;
            else
            {
                notify_fail("Nie jeste� cz�onkiem kana�u o nazwie '" +
                channels[line][CHANNEL_NAME] + "'.\n");
                return 0;
            }

            channels[line][(hide ? CHANNEL_HIDDEN : CHANNEL_USERS)] -= ({ name });
            SECURITY->set_channels(channels);

            write("Przestajesz by� " + (hide ? "ukrytym " : "") +
                "dyskusantem na kanale  " + channels[line][CHANNEL_NAME] + ".\n");
            return 1;

        case "list":
            if (!strlen(str))
            {
                size = m_sizeof(channels);
                if (!size)
                {
                    notify_fail("W tej chwili nie istniej� �adne kana�y poza domenowymi..\n");
                    return 0;
                }

                lines = m_indices(channels);
                index = -1;
                hide = 0;
                while(++index < size)
                    if (line_list(lines[index]))
                        hide = 1;

                if (hide)
                    return 1;

                notify_fail("Nie istnieje �aden kana� otwarty dla ciebie.\n");
                return 0;
            }

            if (!pointerp(channels[line = lower_case(str)]))
            {
                notify_fail("Kana� o nazwie '" + line + "' nie istnieje.\n");
                return 0;
            }

            if (line_list(line))
                return 1;

            notify_fail("Kana� o nazwie '" + line + "' nie jest dla Ciebie otwarty.\n");
            return 0;

        case "remove":
            if (!pointerp(channels[line = lower_case(str)]))
            {
                notify_fail("Kana� o nazwie '" + line + "' nie istnieje.\n");
                return 0;
            }
            if ((rank < WIZ_ARCH) && (channels[line][CHANNEL_OWNER] != name))
            {
                notify_fail("Nie jeste� uprawniony do usuni�cia kana�u o nazwie: " +
                channels[line][CHANNEL_NAME] + ".\n");
                return 0;
            }

            write("Kana� o nazwie " + channels[line][CHANNEL_NAME] + " zosta� usuni�ty.\n");

            channels = m_delete(channels, line);
            SECURITY->set_channels(channels);

            return 1;
        // --- Zmiany Kruna ---
        case "rename":
            if(sscanf(str, "%s %s", line, str) != 2)
            {
                notify_fail("Sk�adnia: lineconfig rename <stara nazwa> <nowa nazwa>.\n");
                return 0;
            }
            if(!pointerp(channels[line = lower_case(line)]))
            {
                notify_fail("Kana� o nazwie '" + line + "' nie istnieje.\n");
                return 0;
            }
            if(strlen(str) < 2 || strlen(str) > 8)
            {
                notify_fail("Nowa nazwa kana�u musi mie� nie mniej in� 2 i nie wi�cej niz 8 znak�w.\n");
                return 0;
            }
            if((rank < WIZ_ARCH) && (channels[line][CHANNEL_OWNER] != name))
            {
                notify_fail("Nie jestes uprawniony do zmiany nazwy tego kana�u.\n");
                return 0;
            }

            channels[lower_case(str)] = channels[line];
            channels[lower_case(str)][CHANNEL_NAME] = str;

            if(lower_case(str) != line)
            channels = m_delete(channels, line);

            write("Zmieniono nazwe kana�u z " + line + " na " + str + ".\n");
            return 1;
        //-------------------
        default:
            notify_fail("Subkomenda '" + cmd + "'. nie istnieje\n");
            return 0;
    }

    write("Wyst�pi� powa�ny b��d w lineconfig. Zg�o� o tym odpowiednim osobom.\n");
    return 1;
}

/* **************************************************************************
 * next - auidence next person in the list
 */
nomask int
next(string cmd)
{
    string name;
    object ob;

    name = this_interactive()->pop_queue();

    if(!name)
    {
        write("Nikt nie prosi� Ci� o audiencje.\n");
        return 1;
    }
    if (!stringp(name) || name == "")
    {
        write("Nikt nie prosi� Ci� o audiencje.\n");
        return 1;
    }

    if (!objectp(ob = find_player(name)))
    {
        write(capitalize(name) + " prawdopodobie opu�ci� ju� �wiat Vatt'gherna.\n");
        return 1;
    }

    if ((cmd == "refuse") || (cmd == "deny") || (cmd == "cancel") || (cmd ~= "odm�w") ||
        (cmd ~= "anuluj") )
    {
        tell_object(ob, this_interactive()->query_cap_name() +
            " odm�wi� udzielenia Ci audiencji.\n");
        write("Odmiawiasz udzielenia audiencji " + find_player(name)->query_imie(TP, PL_CEL) + ".\n");
    }
    else
    {
        tell_object(ob, this_interactive()->query_cap_name() +
            " zgodzi� si� na udzielenie Ci audiencji.\n");
        write("Zgadzasz si� na udzielenie audiencji " + find_player(name)->query_imie(TP, PL_CEL) + ".\n");
    }
    return 1;
}

/**
 * alias do line rada
 */
nomask int
rwiz(string str)
{
    if(SECURITY->query_wiz_rank(TP->query_real_name()) < WIZ_ARCH &&
        member_array(TP->query_real_name(), channels["rada"][CHANNEL_USERS] +
            channels["rada"][CHANNEL_HIDDEN] + ({channels["rada"][CHANNEL_OWNER]})) == -1)
    {
        return 0;
    }

    return line("rada " + str, (query_verb() == "rwize"));
}

/* **************************************************************************
 * tell - tell something to someone
 */
nomask int
tell(string str)
{
    int idle;
    object ob;
    string who;
    string msg;
    string *names;

    CHECK_SO_WIZ;

    if (!stringp(str) || (sscanf(str, "%s %s", who, msg) != 2))
    {
        notify_fail("Tell <imi�> <wiadomo��> ?\n");
        return 0;
    }

    who = capitalize(who);
    ob = find_living(lower_case(who));
    if (!objectp(ob))
    {
        notify_fail("Nie znaleziono postaci o imieniu " + who + ".\n");
        return 0;
    }

    if (!query_ip_number(ob))
    {
        notify_fail(who + " zerwa� linka.\n");
        return 0;
    }

    if(!(ob->query_wiz_level()) && (SECURITY->query_wiz_rank(TP->query_real_name()) < WIZ_MAGE))
    {
        write("Uczniowie Aretuzy nie mog� przekazywa� nic �miertelnikom!\n");
        return 1;
    }

    if (ob->query_prop(WIZARD_I_BUSY_LEVEL) & (BUSY_S | BUSY_P))
    {
        write(who + " jest zaj�t"+ob->koncowka("y","a","e")+". "+
            "Je�li chcesz si� skontaktowa�, kiedy " +
            ob->query_pronoun() +" b�dzie dost�pn"+ob->koncowka("y","a","e")+
            ", u�yj komendy 'audience'.\n");
        return 1;
    }

    if(ob == TP)
    {
        write("Heh, a po co chcesz telln�� do siebie je�li mo�na wiedzie�? Daj se siana:P\n");
        return 1;
    }

    if ((idle = query_idle(ob)) > 300)
    {
        write(capitalize(ob->query_real_name()) + " paskudnie idluje przez: " +
            CONVTIME(idle) + ", wi�c mo�e nie odpowiedzie� zbyt szybko.\n");
    }

    if (ob->query_wiz_level())
    {
        ob->catch_msg(set_color(COLOR_FG_MAGENTA) + set_color(COLOR_BOLD_ON) +
           explode(ctime(time()), " ")[-1][..-4] + " " + capitalize(TP->query_real_name()) +
           " przemawia do ciebie:" + clear_color() + " " + msg + "\n");
    }
    else if ((environment(ob) != environment(this_player())) || (!CAN_SEE(ob, this_player())))
    {
        ob->catch_msg(set_color(COLOR_FG_CYAN) + "Nagle, tu� " +
            "przed tob^a, z chmury dymu wy^lania si^e " +
            "wizerunek " + TP->query_imie(ob, PL_DOP) + ".\n" +
            "W twym umy^sle d^xwi^eczy " + this_player()->koncowka("jego", "jej") +
            " g^los: " + set_color(COLOR_BOLD_ON) + msg + set_color(COLOR_BOLD_OFF) + "\n" +
            "Dym ponownie osnuwa posta^c i po chwili rozwiewa si^e, " +
            "pozostawiaj^ac tylko wspomnienie po tej dziwnej wizycie.\n" + clear_color());
    }
    else
    {
        ob->catch_msg(set_color(COLOR_FG_CYAN) +
            "Nagle zaczynasz s�ysze� dziwny, jakby bezd�wi�czny g�os: " +
            set_color(COLOR_BOLD_ON) + msg + set_color(COLOR_BOLD_OFF) + "\nNie jeste� w stanie "+
            "zlokalizowa� sk�d �w g�os pochodzi, co� jednak m�wi ci i� nale�y on do " + TP->query_imie(ob, PL_DOP) + ".\n");
    }

    if (this_player()->query_option(OPT_ECHO))
        write(JMAG+U+"Przemawiasz do " + ob->query_name(PL_DOP) + ":" +CL+" "+ msg + "\n");
    else
        write("Ju�.\n");

    names = ob->query_prop(PLAYER_AS_REPLY_WIZARD);
    who = this_player()->query_real_name(PL_CEL);
    if (pointerp(names))
        names = ({ who }) + (names - ({ who }) );
    else
        names = ({ who });
    ob->add_prop(PLAYER_AS_REPLY_WIZARD, names);

    return 1;
}

int
send_pisz(object p, object to, string pocz, string str)
{
    int idle;

    set_this_player(p);

    CHECK_SO_WIZ;
    if(!strlen(str))
    {
        write("Bez tre�ci? Nie wysy�am.\n");
        return 0;
    }

    if((idle = query_idle(to)) > 300)
    {
        write(capitalize(to->query_real_name()) + " paskudnie idluje przez: " +
            CONVTIME(idle) + ", wi�c mo�e nie odpowiedzie� zbyt szybko.\n");
    }

    str = "\t" + implode(explode(str, "\n"), "\n\t") + "\n";
    to->catch_msg(pocz + str);

    write("Wys�a�em do " + to->query_name(PL_DOP) + ".\n");

    return 1;
}

/**
 * Wysy�a ponad jednolinijkow� wiadomo�� do czarodzieja.
 */
int
send(string str)
{
    int s;
    string imie, tekst;
    object p;

    CHECK_SO_WIZ;

    notify_fail("Komu chcesz co� przekaza�?\n");

    if(!str)
        return 0;
    if(!(p=find_player(str)))
    {
        notify_fail("Posta� o imieniu '" + str + "' nie jest zalogowana.\n");
        return 0;
    }

    if(!p->query_wiz_level())
    {
        notify_fail("Aby przekaza� co� graczowi u�yj komendy tell, komenda "+
            "'"+query_verb()+"' s�u�y jedynie do komunikacji pomi�dzy czarodziejami.\n");
        return 0;
    }

    if(p->query_prop(WIZARD_I_BUSY_LEVEL) & (BUSY_S | BUSY_P))
    {
        write(capitalize(p->query_real_name()) + " jest teraz zaj�ty. "+
            "Je�li chcesz si� z nim skontaktowa� kiedy b�dzie dost�pny "+
            "u�yj komendy 'audience'.\n");
        return 1;
    }

    setuid();
    seteuid(getuid());
    clone_object(EDITOR_OBJECT)->edit(&send_pisz(this_player(), p, SET_COLOR(COLOR_FG_RED)+"Wiadomo�� "+
        "od " + capitalize(this_player()->query_name(PL_DOP)) + ":" + clear_color() + "\n"));

    return 1;
}

/* **************************************************************************
 * wiz  - gossip on the wizline
 * wize - emote on the wizline
 */
nomask int
wiz(string str)
{
    int busy;

    if (!stringp(str))
    {
        notify_fail(query_verb() + " co?\n");
        return 0;
    }

    busy = this_interactive()->query_prop(WIZARD_I_BUSY_LEVEL);
    if (busy & BUSY_F)
        write("UWAGA: Masz w��czony status 'busy F'.\n");
    else if (busy & BUSY_W)
    {
        write("UWAGA: Tw�j status 'busy W' zosta� automatycznie wy��czony.\n");
        this_interactive()->add_prop(WIZARD_I_BUSY_LEVEL, (busy ^ BUSY_W));
    }

    return line((WIZNAME_APPRENTICE + " " + str), (query_verb() == "wize"),
        BUSY_W);
}

int
wsay(string str)
{
    object *wizards;

    if (!strlen(str))
    {
        notify_fail("Co chcesz powiedzie� w tajemnej mowie czarodziej�w Vatt'gherna?\n");
        return 0;
    }

    wizards = filter(all_inventory(environment(this_player())),
        &->query_wiz_level()) - ({ this_player() });
    if (!sizeof(wizards))
    {
        notify_fail("Nie ma tu nikogo kto m�g�by wys�ucha� mowy czarodziej�w Vatt'gherna.\n");
        return 0;
    }

	int size = sizeof(wizards);
	object old_player = this_player();
	while(size--)
	{
	set_this_player(wizards[size]);
    write(JMAG+
		capitalize(old_player->query_real_name()) +
        " m�wi w j�zyku czarodziej�w: " + CL +
		str +"\n");
    }
	set_this_player(old_player);
    if (this_player()->query_option(OPT_ECHO))
    {
        write(JMAG+
			"M�wisz w j�zyku czarodziej�w: " + CL +
			 str + /*CL +*/ "\n");
    }
    else
    {
        write("Ju�.\n");
    }
    return 1;
}

/**
 * Funkcja pomocnicza do wsend
 */
nomask void
wsend_pisz(object p, string str)
{
    set_this_player(p);

    CHECK_SO_WIZ;
    if(!strlen(str))
    {
        write("Bez tre�ci? Nie wysy�am.\n");
        return;
    }

    string spaces = "   ";

    int i;
    for(i=3;i<(strlen(TP->query_real_name())+4);i++)
        spaces += " ";

    str = implode(explode(str, "\n"), "\n" + spaces);
//     str = str[0..-strlen(spaces)];
    setuid();
    seteuid(getuid());

    string tmp;
    if(!wiz(str))
    {
        if(tmp=query_notify_fail())
            write(tmp);
        else
            write("S�ucham?\n");
    }
}

/**
 * Komenda wysy�a wiadomo�� ponadjednolinijkow� do wszystkich na wizline
 */
nomask int
wsend(string str)
{
    int busy;

    busy = this_interactive()->query_prop(WIZARD_I_BUSY_LEVEL);
    if (busy & BUSY_F)
        write("UWAGA: Masz w��czony status 'busy F'.\n");
    else if (busy & BUSY_W)
    {
        write("UWAGA: Tw�j status 'busy W' zosta� automatycznie wy��czony.\n");
        this_interactive()->add_prop(WIZARD_I_BUSY_LEVEL, (busy ^ BUSY_W));
    }

    setuid();
    seteuid(getuid());
    clone_object(EDITOR_OBJECT)->edit(&wsend_pisz(this_player()));

    return 1;
}