/**
 * Plik dziedziczony przez klucze gildiowe, przez ten plik nie mog� one zosta� zgubione.
 *
 * @author Krun
 * @date Grudzie� 2007
 */

string nazwa_gildii;

public int check_no_get();

#include <macros.h>
#include <stdproperties.h>

/**
 * Funkcja konfiguruj�ca objekt jako klucz gildiowy.
 * Bez tego b�dzie to najzwyczajniejszy w �wiecie klucz.
 */
public void
config_guild_key()
{
    TO->add_prop(OBJ_M_NO_DROP, "Nie mo�esz tego od�o�y�.\n");
    TO->add_prop(OBJ_M_NO_GIVE, "Nie mo�esz tego nikomu da�.\n");
    TO->add_prop(OBJ_M_NO_GET, &check_no_get());
    TO->add_prop(OBJ_M_NO_SELL, "Nie mo�esz tego sprzeda�.\n");
    TO->add_prop(OBJ_M_NO_STEAL, "Nie mo�esz tego ukra��.\n");
    TO->add_prop(OBJ_M_NO_BUY, "Nie mo�esz tego kupi�.\n");

    set_alarm(2.0, 0.0, "sprawdz_czy_moze_miec");
}

public void
sprawdz_czy_moze_miec()
{
    if(nazwa_gildii)
    {
        if(!ENV(TO)->query_gildia(nazwa_gildii))
        {
            ENV(TO)->catch_msg(UC(TO->short(ENV(TO), PL_MIA)) + " rozsypuje si� w proch.\n");
            TO->remove_object();
        }
    }
}

public string
check_no_get()
{
    if(nazwa_gildii)
    {
        if(!ENV(TO)->query_gildia(nazwa_gildii))
        {
            return "Nie mo�esz tego wzi��.\n";
        }
    }
}

/**
 * Dzi�ki tej funkcji mo�emy ustawi� nazwe gildii, kt�rej cz�onkowie mog�
 * posiada� klucz.
 * @param name nazwa gildii.
 */
public void
ustaw_nazwe_gildii(string name)
{
    nazwa_gildii = name;
}

/**
 * @return nazwe gildii, kt�rej cz�onkowie mog� posiada� klucz.
 */
public string
query_nazwa_gildii()
{
    return nazwa_gildii;
}