/**
 * \file /lib/guild_support.c
 *
 * Plik wspieraj�cy medytacje.
 *
 *
 */

#pragma strict_types
#pragma save_binary

#include <macros.h>
#include <formulas.h>
#include <language.h>
#include <ss_types.h>
#include <composite.h>
#include <state_desc.h>
#include <stdproperties.h>

#define KONC this_player()->koncowka

/*
 * Global variable. It is not saved.
 */
static string *stat_names, *stat_names_mia, *stat_names_bie;

/*
 * Prototypes.
 */
int gs_catch_all(string arg);
int gs_meditate(string str);
int get_skill_number(string name_bie);

/*
 * Function name: create_guild_support
 * Description:   Set up the stat_names variable
 */
void
create_guild_support()
{
    stat_names = SD_STAT_NAMES;
    stat_names_mia = SD_STAT_NAMES_MIA;
    stat_names_bie = SD_STAT_NAMES_BIE;
}

/*
 * Nazwa funkcji : sk_add_in_prefix
 * Opis          : Dodaje przed argumentem odpowiedni dla niego przyimek:
 *                 'w' lub 'we', a przed nim jeszcze spacje.
 * Argumenty     : str: Rzeczownik w miejscowniku.
 */
public string
sk_add_in_prefix(string str)
{
    if (str[0] == 'w')
        switch (str[1])
        {
            case 'a':
            case 'e':
            case 'i':
            case 'o':
            case 'u':
            case 'y':
                return " w " + str;
            default:
                return " we " + str;
        }

    return " w " + str;
}

/*
 * Function name: assess
 * Description:   Allows a player to estimate how far he is from the next
 *                stat description.
 * Parameters:    stat - one of "STR", "DEX", "CON", "INT", "DIS"
 *                lower case is ok as well
 * Returns:       1 - success, 0 - stat not found
 */
int
assess(string stat)
{
    int     prog;           // Kolejne progi punktowe
    int     reszta;         // O ile punkciow wspolczynnik jest ponad progiem
    int     stat_val;       // Wartosc wspolczynnika u gracza
    int     is_skill;       // Czy testujemy skilla


    //troch� tu trzeba by�o pozmienia�, dostosowa� do nowego  systemu i nowej
    //liczby podcech. V.
    string  *a_strings = ({ "du�o", "troch�", "niewiele", "bardzo niewiele" });

    //mixed   stat_strings;
    //object  tgive;

    if (!stat)
    {
        write("Sk�adnia: oce� <wsp�czynnik>\n");
        return 1;
    }

    stat = lower_case(stat);

    int stat_index;

    /* Jesli podany wspolczynnik jest cecha... */
    if((stat_index = member_array(stat, stat_names_bie)) != -1)
    {
        stat_val = this_player()->query_stat(stat_index);

        if (stat_val >= 160)
        {
            write("Twa " + stat_names_mia[stat_index] + " osi�gn�a nadludzki poziom.\n");
            return 1;
        }

        if (stat_val >= ftoi(F_MAX_STAT_I))
        {
            write("Masz epick� " + stat_names_bie[stat_index] + ".\n");
            return 1;
        }
        
        reszta = (stat_val-1) % (F_MAX_STAT_I / SD_DESCR_PER_STAT );

        write("Wydaje ci si�, �e " + a_strings[(reszta * (sizeof(a_strings)*2)) / SD_DESCR_PER_STAT] +
              " ci brakuje, �eby� m�g�" + KONC("", "a") +
              " wy�ej oceni� " + (stat_index == SS_INT ? "sw�j" : "sw�") + " " + stat_names_bie[stat_index] + ".\n");

        return 1;
    }

    /* Jesli wspolczynnik jest umiejetnoscia... */
    if((stat_index = get_skill_number(stat)) != -1)
    {
        stat_val = this_player()->query_skill(stat_index);
        string skill_name_mie = SS_SKILL_DESC[stat_index][2];

        if(!stat_val)
        {
            write("Kompletnie nie znasz si� na "+skill_name_mie+"!\n");
            return 1;
        }

        if(stat_val >= 100)
        {
            write("Wygl�da na to, �e t� umiej�tno�� masz ju� opanowan�"
                 +" absolutnie perfekcyjnie.\n");
            return 1;
        }

	reszta = (stat_val - 1) % (100/sizeof(SD_SKILLEV));
	//write("Wydaje ci si�, �e " + a_strings[(reszta * sizeof(a_strings)) / sizeof(SD_SKILLEV)] +
	write("Wydaje ci si�, �e " + a_strings[(reszta * (sizeof(a_strings)*2)) / 8] +
              " ci brakuje, �eby� m�g�" + KONC("", "a") +
              " wy�ej oceni� sw� bieg�o��" + sk_add_in_prefix(skill_name_mie) + ".\n");

        return 1;
    }

    write("Masz do wyboru: " + COMPOSITE_WORDS(stat_names_bie)
         +" lub kt�r�� z umiej�tno�ci.\n");

    return 1;
}

/*
 * Function name: gs_leave_inv
 * Description:   Should be called if someone leaves the room. if that person
 *		  was meditating, better do something. You should call
 *		  this function from leave_inv() in your room.
 */
void
gs_leave_inv(object ob, object to)
{
    if (ob->query_prop(LIVE_I_MEDITATES))
    {
        ob->remove_prop(LIVE_I_MEDITATES);
        ob->remove_prop(LIVE_S_EXTRA_SHORT);
    }
}

/*
 * Function name: init_guild_support
 * Description:   Add the meditate command to the player
 */
void
init_guild_support()
{
    if (!stat_names)
        create_guild_support();

    add_action(gs_meditate, "medytuj");
}

/*
 * Function name: gs_hook_already_meditate
 * Description:	  Called when player is already meditating
 * Returns:	  Always 1
 */
int
gs_hook_already_meditate()
{
    write("Ju� jeste� pogr��on" + KONC("y", "a") + " w transie. Je�li "+
        "chcesz si� ze� wydosta�, 'powsta�'.\n");
    return 1;
}

/*
 * Function name: gs_hook_start_meditate
 * Description:   Called when player starts to meditate
 */
void
gs_hook_start_meditate()
{
    write("Spokojnie kl�kasz na ziemi, zamykasz oczy i starasz wprowadzi� "+
        "si� w g��boki trans. Po jakim� nieokre�lonym czasie stwierdzasz, " +
        "i� nie docieraj� do ciebie �adne bod�ce z zewn�trz, za� ty sam" +
        KONC("", "a") + " jeste� w stanie skoncentrowa� si� na esencji " +
        "w�asnego jestestwa. Potrafisz teraz 'oceni�' w�asne cechy i umiej�tno�ci. �eby " +
        "spr�bowa� wyrwa� si� z transu, 'powsta�'.\n");
    saybb(QCIMIE(this_player(), PL_MIA) + " kl�ka na ziemi i zaczyna " +
          "medytowa�.\n");
}

/*
 * Function name: gs_hook_rise
 * Description:	  Called when player rises from the meditation
 */
void
gs_hook_rise()
{
    write("Wynurzaj�c si� z otch�ani w�asnego umys�u, odzyskujesz powoli "+
        "zmys�y. Czujesz si� bardzo wypocz�t" + KONC("y", "a") + " i " +
        "zrelaksowan" + KONC("y", "a") +
        ", gdy w ko�cu powstajesz.\n");
    saybb(QCIMIE(this_player(), PL_MIA) + " powstaje.\n");
}

/*
 * Function name: gs_hook_catch_error
 * Description:   Called player tried to do something strange while meditating
 *		  like examin things or leave the room.
 * Arguments:	  str - Argument the player tried to send to his command
 * Returns:	  1 normally.
 */
int
gs_hook_catch_error(string str)
{
    write("Nie mo�esz tego robi� w czasie medytacji.\n");
    return 1;
}

/*
 * Function name: gs_meditate
 * Description:   Player wants to meditate
 */
int
gs_meditate(string str)
{
    string long_str;

    this_player()->add_prop(LIVE_S_EXTRA_SHORT, " medytuje");

    if (this_player()->query_prop(LIVE_I_MEDITATES))
        return gs_hook_already_meditate();

    this_player()->add_prop(LIVE_I_MEDITATES, 1);

    gs_hook_start_meditate();

    add_action(gs_catch_all, "", 1);
    return 1;
}

/*
 * Function name: gs_rise
 * Description:   Player rises
 */
int
gs_rise()
{
    gs_hook_rise();
    this_player()->remove_prop(LIVE_I_MEDITATES);
    return 1;
}

/*
 * Function name: gs_catch_all
 * Description:	  Catch all commands the player makes while meditating
 */
int
gs_catch_all(string arg)
{
    string action;

    if (!this_player()->query_prop(LIVE_I_MEDITATES))
        return 0;

    action = query_verb();

    switch (action)
    {
        case "oce�":
            return assess(arg);

        case "medytuj":
            return gs_meditate("");

        case "powsta�":
            this_player()->remove_prop(LIVE_S_EXTRA_SHORT);
            gs_rise();
            return 1;

        case "cechy":
        case "umiej�tno�ci":
        case "um":
        case "opcje":
        case "walka":
        case "przyzwij":
        case "zg�o�":
        case "odpowiedz":
        case "?":
        case "pomoc":
            return 0;

        default:
            return gs_hook_catch_error(arg);
    }
}

/**
 * Zwraca numerek skilla po podaniu jego nazwy w bierniku
 * @param name_bie Nazwa umiejetnosci w bierniku
 * @return Numer skill lub -1 jesli nie znaleziono skilla o podanej nazwie
 */
int get_skill_number(string name_bie)
{
    if(!name_bie)
        return -1;

    int *indexes = m_indexes(SS_SKILL_DESC);
    int limit = sizeof(indexes);

    for(int i = 0; i < limit; i++)
    {
        if(name_bie ~= SS_SKILL_DESC[indexes[i]][1])
            return indexes[i];
    }

    return -1;
}

/* oczywi�cie, �e pomoc musi by�!!!
 * Vera
 */
string
query_pomoc()
{
    return "W tym miejscu mo�esz podda� si� medytacji, "+
    "dzi�ki kt�rej b�dziesz w stanie oceni� dok�adniej "+
    "swe cechy.\n";
}
