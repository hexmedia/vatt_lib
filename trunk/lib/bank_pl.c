/**
 * Lib do banku, bankiera, czy cokolwiek -  byle dzia�a�o ;)
 *
 * @author Nunas - pierwsza wersja
 *
 * @author Molder - kupa poprawek
 *
 * @author Krun - druga kupa poprawek:P
 */
inherit "/lib/bank";

#include <stdproperties.h> /* standardowe wlasciwosci */
#include <object_types.h> /* standardowe typy objektow */
#include <language.h> /* standardowe typy objektow */
#include <money.h>
#include <macros.h>
#include <files.h>

//To wszystko przyda�oby si� przerobi� na zmienne ustawialne w banku!
//Krun.
/* ile ostatnich operacji */
#define BANK_LOG_FILE   "TRANSAKCJE_BANKOWE"

#define HISTORIA 5

/* oplata za zalozenie rachunku*/
#define OPLATA 860

/* prowizja za wyplate w procentach */
#define PROWIZJA 4

/* prowizja za przelew w procentach */
#define PRZELEW 1

/* kwota od kt�re transakcje zaczynaj� by� logowane */
#define KWOTA_LOGOWANA  20

/* tablica z rachunkami klientow */
mapping rachunki = ([]);
mapping historia = ([]);

int nowe_konto(string str);
int przejrzyj_historie(string str);
int sprawdz_czy_ma_konto(object ob);
int wplata(string str);
int wyplata(string str);
int stan_rachunku(string str);
int przelew(string str);
int ile_wplacic(string str);
int sprawdz_czy_ma_konto_cel(string str);
int * zwroc_monety(int liczba, int rodz);
string kwota(int cena);
string zwroc_nazwe_rachunku(string str);

void wplac_na_konto(int il);
void wyplac_z_konta(int il);
void wypisz_saldo_rachunku();
void przejrzyj_konta();
void wpisz_do_historii(string rodzaj, int kwota);
void wplac_na_konto_osobie(string osoba, int kwota);
void wpisz_do_historii_osobie(string osoba, string rodzaj, int kw);

void
init_bank()
{
    bank_init();
    add_action(nowe_konto, "otw�rz"); // tworzy nowe konto w banku
    //add_action(nowe_konto, "za��"); //alias do otw�rz - za�� jest od skrzynki depozytowej! V.
    add_action(przejrzyj_historie, "przejrzyj"); // przeglada historie rachunku
    add_action(wplata, "wp�a�"); // wplaca na konto
    add_action(wplata, "zdeponuj"); //alias do wp�a�
    add_action(wyplata, "wyp�a�");   // wyplaca z konta
    add_action(stan_rachunku, "sprawd�");   // podaje saldo rachunku
    add_action(przelew, "przelej");   // przelew na konto kogos innego
}

string
player_name(int przyp=0)
{
    return lower_case(TP->query_real_name(przyp));
}

/* ---------------------------------- Funkcje hacz�ce  ---------------------------------------------------- */

/*
 * Description:   NIe mozna przkazac monet z wlasnego konta na wlasne
 */
int
bank_hook_nie_na_wlasny()
{
    notify_fail("Sobie nie mo�esz przela� pieni�dzy.\n");
    return 0;
}

/*
 * Description:   Brak takich monet jakie sie che wplacic
 */
int
bank_hook_brak_monet()
{
    notify_fail("Nie masz przy sobie takich monet.\n");
    return 0;
}

/*
 * Description:   wys. komunikat po tym jak nie mozna bylo przetworzyc stringa
 */
int
bank_hook_nic_niewpisano()
{
    notify_fail("Musisz okre�li� ile chcesz wp�aci�.\n");
    return 0;
}

/*
 * Description:   wys. komunikat o tym iz gracz nie posiada jeszcze konta
 */
int
bank_hook_ta_osoba_niemakonta()
{
    notify_fail("Niestety, ale ta osoba nie posiada u nas rachunku.\n");
    return 0;
}

/*
 * Description:   wys. komunikat o tym iz gracz nie posiada jeszcze konta
 */
int
bank_hook_konto_nie_istnieje()
{
    notify_fail("Niestety, ale nie posiadasz u nas rachunku. "+
        "Mo�esz takowy otworzy�, za drobn� op�at�.\n");
    return 0;
}

/*
 * Description:   wys. komunikat o braku kasiorki przy sobie
 */
int
bank_hook_brak_kasy()
{
    notify_fail("Niestety, ale nie masz przy sobie wystarczaj�cej ilo�ci monet.\n ");
    return 0;
}

/*
 * Description:   wys. komunikat o braku jakiejkolwike kasiorki przy sobie
 */
int
bank_hook_niemasz_kasy()
{
    notify_fail("Co� mi si� wydaje, �e nie masz przy sobie �adnych monet, " +
        "kt�re m" +  TP->koncowka("�g�","og�a") +"by� wp�aci�.\n ");
    return 0;
}

/*
 * Description:   wys. komunikat o ze nie bardoz rozumiemy co gracz cce zroic
 */
int
bank_hook_niezrozumialem()
{
    notify_fail("Nie rozumiem, co chcesz zrobi�?\n");
    return 0;
}

/*
 * Description:   wys. komunikat o tym ze nie ma gracz na koncie tyle ile
 *                  chcia�by zabrac
 */
int
bank_hook_braksrodkow()
{
    notify_fail("Niestety, ale nie posiadasz takiej kwoty na swoim rachunku.\n");
    return 0;
}

/* <------------------- Funkcje wywo�ywane przez komendy -----------------> */

/*
 * Description:   funkcja wywolywana przez komende stworz konto
 * Arguments:     string z wprowadzonym tekstem
 * Returns:       int 1 jezeli sie udalo
 */
int
nowe_konto(string str)
{
    string nazwa;
    int *arr;

    if((str == "rachunek") || (str == "konto"))
    {
        /* nie u�ywam tutaj hook�w, poniewa� one u�ywaj�
            notify_fail i zwracaj� nieobs�u�enie komendy,
            co w przypadku komendy 'otw�rz' przekazuje j� dalej,
            i si� robi niedobrze. zatem wewn�trz tego if'a
            zawsze zwracamy 1 */
        //Hooki mo�na zrobi�, tylko musia�yby odpowiednio zwraca�... Co za problem?(Krun)
        if(sprawdz_czy_ma_konto(TP)==1)
        {
            write("Masz ju� rachunek w naszym banku.\n");
            return 1;
        }

        if (sizeof(arr = pay(OPLATA, TP, "", 0, 0, "")) == 1)
        {
            //ob[i]->move(store_room, 1);
            write("Niestety, ale nie masz przy sobie wystarczaj�cej ilo�ci monet.\n ");
            return 1;
        }

        //dump_array(arr);
        nazwa = player_name();

        rachunki[nazwa] = ({player_name(PL_DOP), 0});
        historia[nazwa] = "Uruchomienie konta " + ctime(time());

        write("Ju�. Za otwarcie rachunku zap�aci�"+  TP->koncowka("e�","a�")  +
             " " + kwota(OPLATA) +".\n");
        saybb(QCIMIE(TP,PL_MIA) + " otworzy�" + TP->koncowka("","a") + " rachunek.\n");

        TO->zapisz_mnie();

        return 1;
    }

    notify_fail("Co chcesz otworzy�?\n");
    return 0;
}

/*
 * Description:   funkcja wywolywana przez komende przejrzyj historie
 * Arguments:     string z pwrowadzonym tekstem
 * Returns:       int 1 jezeli sie udalo
 */
int
przejrzyj_historie(string str)
{
    string nazwa="";
    nazwa = player_name();

    if(str == "konta" || str == "rachunki")
    {
        if(TP->query_wiz_level())
        {
            przejrzyj_konta();
            return 1;
        }
        else
        {
            return bank_hook_niezrozumialem();
        }
    }

   if(str ~= "histori�" || str~="histori� rachunku" ||
      str ~= "histori� swojego rachunku")
    {
        // dla pewnosci sprawdzamy jeszcze raz czy gracz  ma juz konto
        if(sprawdz_czy_ma_konto(TP)==1)
        {
            write("Historia operacji:\n");
            write(historia[nazwa]);
            write("\n");
            wypisz_saldo_rachunku();
            return 1;
        }
        else
        {
            return bank_hook_konto_nie_istnieje();
        }
    }

    notify_fail("Przejrzyj co? Mo�e histori�?\n");
    return 0;
}

/*
 * Description:   funkcja wywolywana przez komende wplac
 * Arguments:     napis wprowazony przez gracza
 * Returns:       int 1 jezeli sie udalo
 */
int
wplata(string str)
{
    int il_kasy, i, *arr, ch;

    if(sprawdz_czy_ma_konto(TP) == 0)
        return bank_hook_konto_nie_istnieje();

    ch=0;

    if (!str || (str == ""))
    {
        return bank_hook_nic_niewpisano();
    }

    // teraz sprawdzamy czy gracz nie pisal czegos takiego
    // wplac grosz albo wplac grosze czy tez denary badz korony
    for (i=0;i<num_of_types ;i++ )
    {
        if(str ~= MONEY_NAMES[i][0][PL_BIE])
        {
            arr=zwroc_monety(0,i);
            ch=1;
        }
        if(str ~= MONEY_NAMES[i][1][PL_BIE])
        {
            arr=zwroc_monety(1,i);
            ch=1;
        }
    }

    if(ch==1)
    {
        if(money_merge(arr) > 0)
        {
            il_kasy=money_merge(arr);
        }
        else
        {
            return bank_hook_brak_monet();
        }
    }
    else
    {
        if(str=="wszystko")
        {
            il_kasy=money_merge(what_coins(TP));
            if(il_kasy==0)
            {
                return bank_hook_niemasz_kasy();
            }
        }
        else
        {
            il_kasy=ile_wplacic(str);

            if(il_kasy == 0)
                return bank_hook_niezrozumialem();
        }
    }

    if(can_pay(il_kasy, TP)==1)
    {
        pay(il_kasy, TP);

        write("Wp�aci�" + TP->koncowka("e�","a�") + ": "+
            kwota(il_kasy)+".\n");
        saybb(QCIMIE(TP, PL_MIA) + " wp�aca jakie� monety.\n");

        wplac_na_konto(il_kasy);
        wpisz_do_historii("Wp�ata" , il_kasy);
        wypisz_saldo_rachunku();

        return 1;
    }
    else
        return bank_hook_brak_kasy();
}

/*
 * Description:   funkcja wywolywana przez komende wyplac
 * Arguments:     napis wprowazony przez gracza
 * Returns:       int 1 jezeli sie udalo
 */
int
wyplata(string str)
{
    int il_kasy, il_prow;
    string name = player_name();

    if(sprawdz_czy_ma_konto(TP) == 0)
        return bank_hook_konto_nie_istnieje();

    if (!str || (str == ""))
    {
        return bank_hook_nic_niewpisano();
    }
    if(str=="wszystko")
    {
        il_kasy=rachunki[name][1];
        il_prow=(il_kasy*PROWIZJA)/100;
        if(il_prow==0) il_prow=1;
        il_kasy-=il_prow;
        if(il_kasy==0) return bank_hook_braksrodkow();
    }
    else
    {
        il_kasy=ile_wplacic(str);
        il_prow=(il_kasy*PROWIZJA)/100;

        if(il_prow == 0)
            il_prow = 1;

        if(il_kasy == 0)
            return bank_hook_niezrozumialem();
    }

    if((il_kasy+il_prow)>rachunki[name][1])
        return bank_hook_braksrodkow();

    if(MONEY_MOVE_COIN_TYPES(split_values(il_kasy), 0, TP) == 0)
    {
        write("Podejmujesz: "+
            kwota(il_kasy)+".\n");
        write("Z czego bank bierze prowizj�: "+
            kwota(il_prow)+".\n");
        saybb(QCIMIE(TP, 0) +
            " wyp�aca jakie� monety.\n");

        wyplac_z_konta(il_kasy+il_prow);
        wpisz_do_historii("Wyp�ata" , il_kasy+il_prow);
        wypisz_saldo_rachunku();

        return 1;
    }
    else
        return bank_hook_brak_kasy();
}

/*
 * Description:   funkcja wywolywana przez komende sprawdz
 * Arguments:     napis wprowazony przez gracza
 * Returns:       int 1 jezeli sie udalo
 */
int
stan_rachunku(string str)
{
    if(sprawdz_czy_ma_konto(TP)==0)
        return bank_hook_konto_nie_istnieje();

    if(str=="stan rachunku" || str == "rachunek" || str == "konto")
    {
        wypisz_saldo_rachunku();
        return 1;
    }

    notify_fail("Sprawd� co? Mo�e stan rachunku?\n");
    return 0;
}

/*
 * Description:   funkcja wywolywana po komedzie przelej
 * Arguments:     napis wprowazony przez gracza
 * Returns:       int 1 jezeli sie udalo
 */
int
przelew(string str)
{
    string osoba, osobam, kasa, tpcel;

    int ile, il_prow;
    tpcel = player_name(PL_CEL);

    if(!str || str == "")
        return bank_hook_niezrozumialem();

    if (sscanf(str, "%s na rachunek %s",  kasa, osoba) != 2)
    {
        if (sscanf(str, "%s na konto %s",  kasa, osoba) != 2)
            return bank_hook_niezrozumialem();
    }

    if(lower_case(osoba) == tpcel)
    {
        return bank_hook_nie_na_wlasny();
    }

    if(sprawdz_czy_ma_konto_cel(osoba) == 0)
    {
        return bank_hook_ta_osoba_niemakonta();
    }

    ile=ile_wplacic(kasa);
    il_prow=(ile*PRZELEW)/100;
    if(il_prow < 1)
        il_prow=1;

    if(ile == 0)
        return bank_hook_niezrozumialem();

    if( (il_prow + ile) > rachunki[player_name()][1] )
        return bank_hook_braksrodkow() ;

    osobam=zwroc_nazwe_rachunku(osoba);
    wplac_na_konto_osobie(osobam, ile);
    wyplac_z_konta(il_prow+ile);

    wpisz_do_historii("przelew do "+UC(osoba), ile);
    wpisz_do_historii_osobie(osobam, "przelew od " +
        UC(TP->query_real_name(PL_DOP)), ile);

    write("Przela�"+TP->koncowka("e�","a�") + " dla " + UC(osoba) +
        " kwot� " + kwota(ile) + " i zap�aci�" +
        TP->koncowka("e�","a�") + " bankowi " +
        kwota(il_prow) + " prowizji.\n");

    return 1;
}

/* ---------------------------- Pozosta�e funkcje -------------------------- */

/*
 * Description:   zwraca nam ladnego stringa z poformatowana kwota
 * Arguments:     int cena - ilosc groszy
 * Returns:       Ladny napis typu 1 korona, 20 denarow i 1 grosz
 */
string
kwota(int cena)
{
    return text(split_values(cena), 0);
}

/*
 * Description:   funkcja zwraca ilosc kasy jaka posiada gracz
 * Arguments:     liczba 0 - pojedyncz 1 - mnoga
 *  rodzaj monety 0 - grosz  1 - denar  2 - korona
 * Returns:       tablice walut
 */
int *
zwroc_monety(int liczba, int rodz)
{
    int *arr, *arr_zwr;
    arr_zwr=allocate(num_of_types);
    arr=what_coins(TP);
    if(liczba==0)
    {
        if(arr[rodz] >0)
            arr_zwr[rodz]=1;
    }
    else if(arr[rodz] >0)
        arr_zwr[rodz]=arr[rodz];

    return arr_zwr;
}

/*
 * Description:   funkcja proboje przeanalizowac stringa i na jego podstawie
                    zrocic ile groszy ktos chce wplacic
 * Arguments:     napis wprowazony przez gracza
 * Returns:       int ilosc gorszy
 */
int
ile_wplacic(string str)
{
    string *m_names;
    int i, *tmp_arr, j, liczba, nast;
    nast=0;

    if (!str)
        return 0;

    tmp_arr = allocate(num_of_types);
    m_names = explode(str, " ");

    for (i = 0; i < sizeof(m_names); i++)
    {
        if(nast==1)
        {
            for (j = 0; j < num_of_types; j++)
            {
                if (wildmatch(money_tematy[j] + "*", m_names[i]))
                tmp_arr[j] += liczba;
            }
        }
        liczba=LANG_NUMS(m_names[i]);

        if(liczba>0)
            nast=1;
        else
            nast=0;
    }
    liczba=money_merge(tmp_arr);
    return liczba;
}

/*
 * Description:   funkcja zwieksza stan konta o podana wartosc
 * Arguments:     int o ile  nalezy zwiekszyc konto
 *                string osoba ktorej chcemy wplacic
 * Returns:       int 1 jezeli sie udalo
 */
void wplac_na_konto_osobie(string osoba, int ile)
{
    rachunki[osoba][1] += ile;

    if(ile > KWOTA_LOGOWANA*240)
    {
        if(player_name() == osoba)
            SECURITY->log_public(BANK_LOG_FILE, capitalize(osoba) + " wp�aci�/a " + kwota(ile) + ".\n");
        else
            SECURITY->log_public(BANK_LOG_FILE, "Na rachunek " + capitalize(osoba) + " wp�yne�o " + kwota(ile) + ".\n");
    }

    TO->zapisz_mnie();
}

/*
 * Description:   funkcja zmniejsza stan konta o podana wartosc
 * Arguments:     int o ile  nalezy zmniejszyc konto
 *                string osoba ktorej chcemy wyplacic
 * Returns:       int 1 jezeli sie udalo
 */
void wyplac_z_konta_osoby(string osoba, int ile)
{
    rachunki[osoba][1] -= ile;

    if(ile > KWOTA_LOGOWANA*240)
        SECURITY->log_public(BANK_LOG_FILE, capitalize(osoba) + " wyp�aci�/a b�d� przela�a " + kwota(ile) + ".\n");

    TO->zapisz_mnie();
}

/*
 * Description:   funkcja zwieksza stan konta o podana wartosc
 * Arguments:     int o ile  nalezy zwiekszyc konto
 * Returns:       int 1 jezeli sie udalo
 */
void wplac_na_konto(int ile)
{
    wplac_na_konto_osobie(player_name(), ile);
}

/*
 * Description:   funkcja zmniejsza stan konta o podana wartosc
 * Arguments:     int o ile  nalezy zmniejszyc konto
 * Returns:       int 1 jezeli sie udalo
 */
void wyplac_z_konta(int ile)
{
    wyplac_z_konta_osoby(player_name(), ile);
}

/*
 * Description:   funkcja wypisuje aktulane saldo rachunku
 */
void wypisz_saldo_rachunku()
{
    write("Obecny stan twojego rachunku to: " + kwota(rachunki[lower_case(TP->query_real_name())][1]) + ".\n");
}

/*
 * Description:   funkcja dodaje do histori nowy wpis jezlei jest juz
 *            historia zapelniona to przesuwa wszystkie wpisy o jeden do gory
 * Arguments:     string z rodzajem operacji
 *                  int z kwota
 */
void
wpisz_do_historii(string rodzaj, int kw)
{
    string *hist, t_nap;
    int i;
    t_nap=ctime(time()) + "\t" + rodzaj + "\t" + kwota(kw);
    hist = explode(historia[player_name()], "\n");

    if(sizeof(hist) < HISTORIA)
        hist += ({t_nap});
    else
    {
        for(i=0; i<HISTORIA-1; i++)
            hist[i]=hist[i+1]; // przesuwamy wszystko w gore

        hist[HISTORIA-1]=t_nap;
    }

    historia[TP->query_real_name()]=implode(hist,"\n");
    TO->zapisz_mnie();
}

/*
 * Description:   funkcja dodaje do histori nowy wpis jezlei jest juz
 *                  historia zapelniona to przesuwa wszystkie wpisy
                    o jeden do gory
 * Arguments:     string z rodzajem operacji
 *                  int z kwota
 *                  osoba osoba korej nalezy wplacic
 */
void
wpisz_do_historii_osobie(string osoba, string rodzaj, int kw)
{
    string *hist, t_nap;
    int i;
    t_nap=ctime(time()) + "\t" + rodzaj + "\t" + kwota(kw);
    hist = explode(historia[osoba], "\n");

    if(sizeof(hist) < HISTORIA)
        hist += ({t_nap});
    else
    {
        for(i=0; i<HISTORIA-1; i++)
            hist[i]=hist[i+1]; // przesuwamy wszystko w gore

        hist[HISTORIA-1]=t_nap;
    }

    historia[osoba]=implode(hist,"\n");
    TO->zapisz_mnie();
}

/*
 * Description:   Funkcja przeglada wszystko kona
 */
void
przejrzyj_konta()
{
    string *konta =m_indices(rachunki);
    int i;

    for(i=0; i<sizeof(konta); i++)
        write(konta[i] + " -- " + rachunki[konta[i]][1] + "\n");
}

/*
 * Description:   funkcja sprawdza czy konto juz istnieje
 * Arguments:     ob - konto gracz
 * Returns:       int 1 jezeli istnieje
 */
int
sprawdz_czy_ma_konto(object ob)
{
    if(member_array(lower_case(ob->query_real_name()), map(m_indices(rachunki), lower_case))==-1)
        return 0;
    else
        return 1;
}

/*
 * Description:   funkcja sprawdza czy konto juz istnieje po celowniku
 * Arguments:     nazwa gracza w celownikou
 * Returns:       int 1 jezeli istnieje
 */
int
sprawdz_czy_ma_konto_cel(string nazwa)
{
    int i;
    mixed *arr;
    nazwa = lower_case(nazwa);
    arr=m_values(rachunki);

    for(i=0; i < sizeof(arr); i++)
        if(lower_case(nazwa)==arr[i][0])
            return 1;
    return 0;
}

/*
 * Description:   funkcja na podstawie nazwy osoby w celowniku zwraca w mianowniku
 * Arguments:     nazwa gracza w celownikou
 * Returns:       string z nazwa gracza w mianowniku
 */
string
zwroc_nazwe_rachunku(string nazwa)
{
    int i;
    mixed *arr, *arr_naz;
    arr=m_values(rachunki);
    arr_naz=m_indices(rachunki);

    for(i=0; i < sizeof(arr); i++)
        if(lower_case(nazwa)==arr[i][0])
            return arr_naz[i];

    return "";
}

// NUNAS -- NADPISANE FUNKCJE

/*
 * Function name: standard_bank_sign
 * Description:   returns a string with a sign message
 * Arguments:     none
 * Returns:       message string
 */
string
standard_bank_sign()
{
    string str=::standard_bank_sign();
    str += "Dodatkowo, je�li posiadasz u nas rachunek mo�esz: \n";

    str += "\twp�aci� monety\n";
    str += "\twyp�aci� monety - prowizja tylko " + PROWIZJA +"%\n";
    str += "\tsprawdzi� stan rachunku\n";
    str += "\tprzejrze� histori� rachunku\n";
    str += "\tprzela� monety na rachunek kogo� innego - prowizja tylko "
            + PRZELEW + "%\n";

    str += "\nJe�li za� nie masz u nas rachunku, mo�esz takowy otworzy� " +
            "za drobn� op�at�: " + kwota(OPLATA) + ".\n";

    return str;
}

/**
 * Dzi�ki tej funkcji wiz mo�e za�o�y� graczowi konto.
 * W razie podania sumy jaka zostanie przelana na konto gracza,
 * zostanie ona pomniejszona o op�ate za za�o�enie konta
 * @param imie imi� gracza kt�remu chcemy za�o�y� konto
 * @param kw kwota jaka ma zostac przelana na konto gracza
 * @return 1/0/-1 sukces/pora�ka/konto ju� istnieje
 */
public int
otworz_konto_gracza(string imie, int kw=0)
{
    object gracz;
    if(imie)
    {
        if(!is_mapping_index(imie, rachunki))
        {
            if((gracz = SECURITY->finger_player(imie)))
            {
                historia[imie] = "Uruchomienie konta przez " +
                    capitalize(player_name(PL_CEL)) + " " + ctime(time()) + ".\n";

                if(kw)
                    kw -= OPLATA;

                kw = max(0, kw);

                if(kw>0)
                    historia[imie] += ctime(time()) + " Przelew na konto w wysoko�ci " + kwota(OPLATA) + ".\n";

                rachunki[imie] = ({lower_case(gracz->query_real_name(PL_DOP)), kw});
                return 1;
            }
            return 0;
        }
        return -1;
    }
    return 0;
}

#if 0
public void
zamien_na_male()
{
    mapping old_rachunki = rachunki;
    mapping old_historia = historia;

    string *rachunki_indexes = m_indexes(rachunki);
    string *historia_indexes = m_indexes(historia);

    dump_array(historia);
    dump_array(rachunki);
    dump_array(historia_indexes);
    dump_array(rachunki_indexes);

    historia = ([]);
    foreach(string x : historia_indexes)
    {
        if(is_mapping_index(x, old_historia))
            historia[lower_case(x)] = old_historia[x];
        else
            historia[lower_case(x)] = "Poprzednia historia konta zosta�a utracona.\n";
    }

    rachunki = ([]);
    foreach(string x : rachunki_indexes)
        rachunki[lower_case(x)] = old_rachunki[x];

    TO->zapisz_mnie();
}

public void
dopisz_historie(string name)
{
    historia[name] = "Porzednia historia konta zosta�a utracona.\n";
}

#endif