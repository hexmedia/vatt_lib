#include <stdproperties.h>
#include <composite.h>
#include <macros.h>
#include <files.h>
#include <filter_funs.h>
#include <pl.h>

string *peek_places = ({ }), *peek_rooms = ({ });

static string
describe_combat(object *livings)
{
    int     index;
    int     size;
    string  text = "";
    object  victim;
    mapping fights = ([ ]);
    mixed tmp_alv;

    if ((size = sizeof(livings)) < 1)
        return "";

    livings += ({ this_object() });
    size++;
    index = -1;
    while(++index < size)
    {
        if (objectp(victim = livings[index]->query_attack()))
        {
            if (pointerp(fights[victim]))
                fights[victim] += ({ livings[index] });
            else
                fights[victim] = ({ livings[index] });
        }
   }

    if (!m_sizeof(fights))
        return "";

    fights = m_delete(fights, this_object());
    livings = m_indices(fights);
    size = sizeof(livings);
    index = -1;
    while(++index < size)
    {
        if (!fights[livings[index]])
            continue;

        if (objectp(victim = livings[index]->query_attack()) &&
            (member_array(victim, fights[livings[index]]) >= 0))
        {
            fights[livings[index]] -= ({ victim });

            if (!fights[victim])
            {
                text += capitalize(victim->short(this_player(), PL_MIA)) +
                    " koncentruje si^e na walce " +
                    z_ze(livings[index]->short(this_player(), PL_NAR)) +
                    ".\n";
            }
            else
            {
                fights[victim] -= ({ livings[index] });
                text += livings[index]->short(this_player(), PL_MIA);

                if (sizeof(fights[victim]))
                {
                    text += ", wraz " + z_ze(FO_COMPOSITE_LIVE(fights[victim],
                        this_object(), PL_NAR));
                }

                text += " walczy " + z_ze(victim->short(this_player(),
                    PL_NAR));

                if (sizeof(fights[livings[index]]))
                {
                    text += ", wspart" + victim->koncowka("ym", "^a")
                        + " przez " + FO_COMPOSITE_LIVE(fights[livings[index]],
                        this_object(), PL_BIE);
                }

                text += ".\n";
            }
        }
        else
        {
            text += capitalize(FO_COMPOSITE_LIVE(fights[livings[index]],
                this_object(), PL_MIA)) + " koncentruj" +
                ((sizeof(fights[livings[index]]) == 1) ? "e" : "^a") +
                " si^e na walce " +
                z_ze(livings[index]->short(this_player(), PL_NAR)) +
                ".\n";
        }

        fights = m_delete(fights, livings[index]);
        fights = m_delete(fights, victim);
    }

    return capitalize(text);
}

int objects_filter(object ob)
{
    if (ob->query_no_show() || ob->query_no_show_composite()) return 0;
    if (ob->query_prop(OBJ_I_DONT_GLANCE)) return 0;
    if (!ob->query_prop(OBJ_I_VOLUME)) return 1;
    return ob->query_prop(OBJ_I_VOLUME) > 50000;
}

int
wyjrzyj(string str, string adv = "")
{
    int		i;
    object *lv, *dd, room;

    if (!stringp(str))
    {
        notify_fail("Gdzie chcesz wyjrze^c?\n");
        return 0;
    }


    if ((i = member_array(str, peek_places)) == -1)
    {
        notify_fail("Nie zauwa^zasz niczego takiego.\n");
        return 0;
    }


    set_auth(this_object(), "root:root");
    LOAD_ERR(peek_rooms[i]);
    room = find_object(peek_rooms[i]);

      //Lil.
    if (!environment(this_player())->query_prop(ROOM_I_INSIDE))
    {
        notify_fail("Przecie� znajdujesz si� na zewn�trz, "+
            "mo�e powin"+this_player()->koncowka("iene�","na�")+
            " spr�bowa� zajrze� gdzie�?\n");
        return 0;
    }

    write("Wygl^adasz " + peek_places[i] + ":\n");
    COMMAND_DRIVER->allbb("wygl^ada " + peek_places[i] + ".");

    if (room->query_prop(OBJ_I_LIGHT) <= -(this_player()->query_prop(LIVE_I_SEE_DARK))) {
	if (stringp(room->query_prop(ROOM_S_DARK_LONG))) {
	    write(room->query_prop(ROOM_S_DARK_LONG));
	    return 1;
	}
	write("Ciemne miejsce.\n");
	return 1;
    }
    str = room->short();
    if (str[-1..-1] != ".") str += ".\n"; else str += "\n";
    write(str);
    lv = FILTER_LIVE(all_inventory(room));
    if (sizeof(lv))
	write(capitalize(COMPOSITE_FILE->desc_live(lv, PL_MIA, 1)) + ".\n");
    dd = FILTER_DEAD(all_inventory(room));
    dd = filter(dd, objects_filter);
    if (sizeof(dd))
	write(capitalize(COMPOSITE_FILE->desc_dead(dd, PL_MIA, 1)) + ".\n");
    describe_combat(lv);

    return 1;
}


int
zajrzyj(string str, string adv = "")
{
    int		i;
    object *lv, *dd, room;

    if (!stringp(str))
    {
	notify_fail("Gdzie chcesz zajrze^c?\n");
	return 0;
    }


    if ((i = member_array(str, peek_places)) == -1)
    {
	notify_fail("Nie zauwa^zasz niczego takiego.\n");
	return 0;
    }


    set_auth(this_object(), "root:root");
    LOAD_ERR(peek_rooms[i]);
    room = find_object(peek_rooms[i]);

          //Lil.
    if (environment(this_player())->query_prop(ROOM_I_INSIDE))
    {
       notify_fail("Przecie� znajdujesz si� w jakim� pomieszczeniu, "+
                   "mo�e powin"+this_player()->koncowka("iene�","na�")+
		   " spr�bowa� wyjrze� gdzie�?\n");
       return 0;
    }

    write("Zagl^adasz " + peek_places[i] + ":\n");
    COMMAND_DRIVER->allbb("zagl^ada " + peek_places[i] + ".");

    if (room->query_prop(OBJ_I_LIGHT) <= -(this_player()->query_prop(LIVE_I_SEE_DARK))) {
	if (stringp(room->query_prop(ROOM_S_DARK_LONG))) {
	    write(room->query_prop(ROOM_S_DARK_LONG));
	    return 1;
	}
	write("Ciemne miejsce.\n");
	return 1;
    }
    str = room->short();
    if (str[-1..-1] != ".") str += ".\n"; else str += "\n";
    write(str);
    lv = FILTER_LIVE(all_inventory(room));
    if (sizeof(lv))
	write(capitalize(COMPOSITE_FILE->desc_live(lv, PL_MIA, 1)) + ".\n");
    dd = FILTER_DEAD(all_inventory(room));
    dd = filter(dd, objects_filter);
    if (sizeof(dd))
	write(capitalize(COMPOSITE_FILE->desc_dead(dd, PL_MIA, 1)) + ".\n");
    write(describe_combat(lv));

    return 1;
}

void
add_peek(string gdzie, string lokacja)
{
    peek_places += ({ gdzie });
    peek_rooms += ({ lokacja });
}

void
remove_peek(string gdzie, string lokacja)
{
    peek_places -= ({ gdzie });
    peek_rooms -= ({ lokacja });
}

void
init_peek()
{
    add_action(wyjrzyj, "wyjrzyj");
    add_action(zajrzyj, "zajrzyj"); /*Lil*/
}
