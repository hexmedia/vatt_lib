/**
 * \file /lib/wearable_item.c
 *
 * Plik ten powinien by� dziedziczony przez wszystkie przedmioty, kt�re mo�na
 * zak�ada�. Jest on standardowo dziedziczony przez <b>/std/armour.c</b>, jednak
 * mog� go dziedziczy� tak�e inne obiekty, jak cho�by <b>/std/object.c</b>, by
 * stworzy� rzeczy, kt�re mo�na nosi� i jednocze�nie nie b�d�ce zbrojami. Na
 * przyk�ad ubrania nie daj�ce ochrony, pier�cienie, broszki, itd...
 *
 * <b>UWAGA</b>
 *
 * Je�eli korzystasz z tego pliku w jakim� obiekcie innym ni�
 * <b>/std/armour.c</b>, to <b>MUSISZ</b> tak�e zdefiniowa� funkcj�
 * <b>leave_env()</b> i wywo�a� z jej poziomu <b>wearable_item_leave_env()</b>.
 * To samo odnosi si� do zdefiniowania <b>appraise_object()</b> i wywo�ania
 * <b>appraise_wearable_item()</b>. Funkcje te nale�y zdefiniowa� w nast�puj�cy
 * spos�b:
 *
 * @example
 * void
 * leave_env(object from, object to)
 * {
 *      ::leave_env(from, to);
 *
 *      wearable_item_leave_env(object from, object to);
 * }
 *
 * void
 * appraise_object(int num)
 * {
 *     ::appraise_object(num);
 *
 *     appraise_wearable_item();
 * }
 *
 * Je�eli nie zdefiniujesz tych funkcji, to obiekt nie b�dzie automatycznie
 * usuwany kiedy gracz go wyrzuci. Ponadto gracze nie b�d� w stanie poprawnie
 * oceni� obiektu.
 */

#include <macros.h>
#include <wa_types.h>
#include <rozmiary.h>
#include <composite.h>
#include <stdproperties.h>
#include "/config/login/login.h"

#define PI 3.14159265

/* tablica trzymaj�ca rozmiary wymagane do za�o�enia zbroi */
float      *rozmiary;

/*
 * Nie wolno zmiania� nazw tych zmiennych, gdy� mog� one by� u�ywane w
 * /std/armour.c b�d� obiektach dziedzicz�cych z niego.
 */
static object wearer, wear_func;
static int  worn, wear_at,
           *wear_at_slots,       /* Na jakim slocie zbroja jest aktualnie ubrana */
            is_last_layer,      /* Na t� zbroje nic nie mo�e by� za�o�one */

            ac_slots,           /* Hitlokacje chronione bez side_bits */
            ac_side_bits,       /* Hitlokacje zale�ne od strony za�o�enia */
            ac_worn,            /* Hitlokacje chronione gdy za�o�ona */

            slot_side_bits,     /* Sloty wyznaczaj�ce stron� zak�adania */
            worn_on_part,
            should_wear;
string      orginal_size;       /* pocz�tkowy rozmiar */


public void set_wf(object ob);
void fryz_zakladanie();
void sprawdzanie_nagosci();
public object query_worn();
public string query_n_size();

public void
config_wearable_item()
{
    TO->add_prop(CONT_I_VOLUME, 1000);
    TO->add_prop(CONT_I_WEIGHT, 1000);
    TO->add_prop(CONT_I_IL_RZECZY, 0);

    TO->add_prop(CONT_I_MAX_WEIGHT, 1000);
    TO->add_prop(CONT_I_MAX_VOLUME, 1000);
}

/**
 * Ustawia, kt�re sloty zajmuje za�o�ona rzecz. Na ka�dym ze slot�w zajmowanych
 * przez rzecz wykonywane s� p�niej testy na dopasowanie rozmiar�w. Mo�na
 * poda�, �e rzecz zajmuje slot dowolnej wolnej np. r�ki, jako warto�� slotu
 * podaj�c ujemny identyfikator prawej strony tej cz�ci cia�a, np. (-A_R_ARM).
 *
 * Przyk�ady dost�pne s� w:
 * <ul>
 *  <li> /doc/examples/ubrania/
 *  <li> /doc/examples/zbroje/
 * </ul>
 *
 * Funkcja przyjmuje dowoln� ilo�� argument�w typu <i>int</i>. Ka�dy z nich
 * reprezentuje jeden, lub sum� binarn� kilku slot�w, kt�re zajmuje za�o�ona
 * rzecz (sloty s� zdefiniowane w <b>/sys/wa_types.h</b>).
 * <b>UWAGA</b> - sloty ujemne nale�y podawa� osobno.
 *
 * @param sloty sloty zajmowane przez dan� rzecz (A_CHEST, A_LEGS, etc.)
 */
public void set_slotsv(int *slots)
{
    int ix = sizeof(slots);

    while(--ix >= 0)
    {
        if(slots[ix] < 0)
        {
            slots[ix] = -slots[ix];
            slot_side_bits |= slots[ix];
            slots[ix] = (slots[ix] | (slots[ix] << 1));
        }
        else
            wear_at |= slots[ix];
    }
}
/**
 * Ustawia, kt�re sloty zajmuje za�o�ona rzecz. Na ka�dym ze slot�w zajmowanych
 * przez rzecz wykonywane s� p�niej testy na dopasowanie rozmiar�w. Mo�na
 * poda�, �e rzecz zajmuje slot dowolnej wolnej np. r�ki, jako warto�� slotu
 * podaj�c ujemny identyfikator prawej strony tej cz�ci cia�a, np. (-A_R_ARM).
 *
 * Przyk�ady dost�pne s� w:
 * <ul>
 *  <li> /doc/examples/ubrania/
 *  <li> /doc/examples/zbroje/
 * </ul>
 *
 * Funkcja przyjmuje dowoln� ilo�� argument�w typu <i>int</i>. Ka�dy z nich
 * reprezentuje jeden, lub sum� binarn� kilku slot�w, kt�re zajmuje za�o�ona
 * rzecz (sloty s� zdefiniowane w <b>/sys/wa_types.h</b>).
 * <b>UWAGA</b> - sloty ujemne nale�y podawa� osobno.
 *
 * @param slot slot zajmowany przez dan� rzecz (A_CHEST, A_LEGS, etc.)
 * @param ... opcjonalne kolejne sloty zajmowane przez rzecz
 */
public void
set_slots(int slot, ...)
{
    int *slots, ix;

    wear_at = 0;
    slot_side_bits = 0;

    if(slot)
        slots = ({ slot });

    if(sizeof(argv))
        slots += argv;

    set_slotsv(slots);
}


/**
 * @return Ustawienie zajmowanych slot�w przez dan� rzecz.
 */
public int
query_slots_setting()
{
    return wear_at;
}

/**
 * Zwraca tablic� ze slotami, jakie rzecz zajmuje, lub mo�e zajmowa�.
 *
 * @param check opcjonalny argument; je�li jest <b>true</b>, to u�ywane jest
 * ustawienie zajmowanych slot�w przez dan� rzecz, a w przeciwnym wypadku
 * aktualnie zajmowane sloty.
 *
 * @return tablica ze slotami zajmowanymi/mog�cymi by� zaj�te przez dan� rzecz.
 */
varargs public int *
query_slots(int check = 0)
{
    int abit, *slots;

    check = (check ? wear_at : worn_on_part);

    /*
     * Je�eli w tej chwili check jest r�wne 0, oznacza to, �e
     * rzecz mo�e by� za�o�ona na wi�cej ni� jeden slot,
     * zwracamy po��czenie obydwu slot�w.
     */
    if(!check && slot_side_bits)
        return ({ slot_side_bits | slot_side_bits << 1 });

    /*
     * Je�eli sloty s� negatywne, to znaczy, �e jest to kt�re� z ustawie�
     * A_ANY_*, czyli zwracamy te w�a�nie sloty, jako �e tak naprawd� zaj�ty mo�e
     * by� dowolny z nich.
     */

    /* zaczynamy sprawdzanie od g�owy */
    slots = ({ });
    for(abit = A_HEAD; abit <= check; abit <<= 1)
        if(check & abit)
            slots = slots + ({ abit });

    return slots;
}

/**
 * Ustawiamy, �e ta zbroja b�dzie ostatni� zbroj� - nie b�dzie mo�na na ni� nic za�o�y�.
 *
 * @param l czy ostatnia warstwa
 */
public void set_last_layer(int l)
{
    is_last_layer = l;
}

/**
 * @return Czy ubranie jest ostatni� warstw�.
 */
public int query_last_layer()
{
    return is_last_layer;
}

/**
 * Oblicza promie� okr�gu maj�c dany obw�d.
 *
 * @param obwod obw�d okr�gu.
 *
 * @return promie� okr�gu.
 */
float
wylicz_promien(float obwod)
{
    return (obwod/(2.0*PI));
}

#define WYMIAR(prop, wym) \
    (TO->is_prop_set(prop) ? TO->query_prop(prop) : wspolczynniki[wym])
/**
 * Oblicza obj�o�� zajmowan� przez dan� cz�� ubrania.
 *
 * @param slot sloty zajmowane przez dan� rzecz.
 * @param wspolczynniki tablica z rozmiarami.
 *
 * @return obj�to�� zajmowana przez dane ubranie.
 */
float
policz_objetosc(int slot, float* wspolczynniki)
{
    float relacja, tmp, dtmp, ttmp;

    /* w zale�no�ci jaka to cz�� cia�a, liczy� musimy troszk� inaczej i korzysta�
     * z innych wsp�czynnik�w oczywi�cie */
    switch (slot)
    {
        case TS_HEAD:
            tmp = wylicz_promien(WYMIAR(PLAYER_F_ROZ_GLOWA, ROZ_OB_GLOWY));
            /* obj�to�� walca o takim samym promieniu i wysoko�ci */
            tmp = PI * tmp * tmp * tmp;
        case TS_NECK:
            tmp = wylicz_promien(WYMIAR(PLAYER_F_ROZ_SZYJA, ROZ_OB_SZYI));
            /* 1/2 * obj�to�� walca o takim samym promieniu i wysoko�ci */
            tmp = 0.5 * PI * tmp * tmp * tmp;
        case TS_R_SHOULDER:
        case TS_L_SHOULDER:
            tmp = wylicz_promien(WYMIAR(PLAYER_F_ROZ_BARKI, ROZ_OB_OKG));
            /* 1/2 * 1/4 * obj�to�� walca o takim samym promieniu i wysoko�ci */
            tmp = 0.125 * PI * tmp * tmp * tmp;
        case TS_CHEST:
            tmp = WYMIAR(PLAYER_F_ROZ_TULOW, ROZ_DL_TULOWIA);
            dtmp = wylicz_promien(WYMIAR(PLAYER_F_ROZ_PIERSI, ROZ_OB_W_KLATCE));
            /* 1/3 * d�ugo�� tu�owia * pole ko�a opisanego na klatce piersiowej */
            tmp =  0.33 * tmp * PI * dtmp * dtmp;

        case TS_STOMACH:
            tmp = WYMIAR(PLAYER_F_ROZ_TULOW, ROZ_DL_TULOWIA);
            dtmp = wylicz_promien(WYMIAR(PLAYER_F_ROZ_PAS, ROZ_OB_W_PASIE));
            ttmp = wylicz_promien(WYMIAR(PLAYER_F_ROZ_BIODRA, ROZ_OB_W_BIODRACH));
            /* 2/3 * pole �ci�tego sto�ka o wysoko�ci tmp i promieniach dtmp i ttmp */
            tmp = 0.66 * 0.33 * PI * tmp * (dtmp * dtmp + dtmp * ttmp + ttmp * ttmp);

        case TS_ROBE:
            /* FIXME: !!! */
            break;
        case TS_R_ARM:
        case TS_L_ARM:
            tmp = WYMIAR(PLAYER_F_ROZ_DL_RAMIE, ROZ_DL_RAMIENIA);
            dtmp = wylicz_promien(WYMIAR(PLAYER_F_ROZ_OB_RAMIE, ROZ_OB_RAMIENIA));
            /* d�ugo�� ramienia * pole ko�a opisanego na ramieniu */
            tmp = tmp * PI * dtmp * dtmp;
        case TS_R_FOREARM:
        case TS_L_FOREARM:
            tmp = WYMIAR(PLAYER_F_ROZ_DL_PRAMIE, ROZ_DL_PRZEDRAMIENIA);
            dtmp = wylicz_promien(WYMIAR(PLAYER_F_ROZ_OB_PRAMIE, ROZ_OB_PRZEDRAMIENIA));
            /* d�ugo�� przedramienia * pole ko�a opisanego na przedramieniu */
            tmp = tmp * PI * dtmp * dtmp;
        case TS_R_HAND:
        case TS_L_HAND:
            tmp = WYMIAR(PLAYER_F_ROZ_DL_DLON, ROZ_DL_DLONI);
            dtmp = WYMIAR(PLAYER_F_ROZ_SZ_DLON, ROZ_SZ_DLONI);
            ttmp = wylicz_promien(WYMIAR(PLAYER_F_ROZ_NADGARSTEK, ROZ_OB_NADGARSTKA));
            /* d�ugo�� d�oni *szeroko�� d�oni *promie� ko�a opisanego na nadgarstku */
            tmp = tmp * dtmp * ttmp;
        case TS_HIPS:
            tmp = wylicz_promien(WYMIAR(PLAYER_F_ROZ_BIODRA, ROZ_OB_W_BIODRACH));
            /* 1/2 * obj�to�� walca o takim samym promieniu i wysoko�ci */
            tmp = 0.5 * PI * tmp * tmp * tmp;
        case TS_R_THIGH:
        case TS_L_THIGH:
            tmp = WYMIAR(PLAYER_F_ROZ_DL_UDO, ROZ_DL_UDA);
            dtmp = wylicz_promien(WYMIAR(PLAYER_F_ROZ_OB_UDO, ROZ_OB_UDA));
            /* d�ugo�� uda * pole ko�a opisanego na udzie */
            tmp = tmp * PI * dtmp * dtmp;
        case TS_R_SHIN:
        case TS_L_SHIN:
            tmp = WYMIAR(PLAYER_F_ROZ_DL_GOLEN, ROZ_DL_GOLENI);
            dtmp = wylicz_promien(WYMIAR(PLAYER_F_ROZ_OB_GOLEN, ROZ_OB_GOLENI));
            /* d�ugo�� golenia * pole ko�a opisanego na goleniu */
            tmp = tmp * PI * dtmp * dtmp;
        case TS_R_FOOT:
        case TS_L_FOOT:
            tmp = WYMIAR(PLAYER_F_ROZ_DL_STOPA, ROZ_DL_STOPY);
            dtmp = WYMIAR(PLAYER_F_ROZ_SZ_STOPA, ROZ_SZ_STOPY);
            ttmp = wylicz_promien(WYMIAR(PLAYER_F_ROZ_KOSTKA, ROZ_OB_KOSTKI));
            /* d�ugo�� stopy * szeroko�� stopy * promie� ko�a opisanego na kostce */
            tmp = tmp * dtmp * ttmp;
        case TS_R_FINGER:
        case TS_L_FINGER:
            tmp = WYMIAR(PLAYER_F_ROZ_SZ_DLON, ROZ_SZ_DLONI);
            tmp = 0.1 * tmp;
            /* PI * (1/10 * szeroko�� d�oni)^3 * 1/2 */
            tmp = PI * tmp * tmp * tmp * 0.5;
        default:
            tmp = 0.0;
    }
    tmp = tmp / 800.0;

    return tmp;
}

/**
 * Wylicza rozmiary zajmowane przez dane ubranie.
 *
 * @param zaslaniane sloty zas�aniane przez dane ubranie.
 * @param for_pl obiekt, dla kt�rego wyliczamy rozmiary.
 */
varargs void
wylicz_rozmiary(int zaslaniane, object for_pl = 0)
{
    string race;
    int gender, il;
    float* wspolczynniki;
    float tmpobj, wsp;

    race = TO->query_prop(ARMOUR_S_DLA_RASY);

    if (!stringp(race))
        race = "cz^lowiek";

    gender = TO->query_prop(ARMOUR_I_DLA_PLCI);

    if (objectp(for_pl))
        wspolczynniki = for_pl->pobierz_rozmiary();
    else
        wspolczynniki = RACEROZMOD[race][gender];

    if (wspolczynniki == 0)
        return;

    tmpobj = 0.0;

    for(il = TS_HEAD; il <= zaslaniane; il <<= 1)
        if(zaslaniane & il)
            tmpobj += policz_objetosc(il, wspolczynniki);

    if (tmpobj == 0.0)
        wsp = 1.0;
    else
    {
        if (objectp(for_pl))
        {
            TO->add_prop(CONT_I_VOLUME, ftoi(tmpobj / ((TO->is_prop_set(ARMOUR_F_PRZELICZNIK)) ?
                TO->query_prop(ARMOUR_F_PRZELICZNIK) : 1.814334328319159)));
        }
        wsp = (itof(TO->query_prop(CONT_I_VOLUME)) / tmpobj) *
            ((TO->is_prop_set(ARMOUR_F_PRZELICZNIK)) ? TO->query_prop(ARMOUR_F_PRZELICZNIK) : 1.814334328319159);
    }

    /* ustawienie rozmiar�w na -1, czyli bez ogranicze� */
    rozmiary = ({-1.0,-1.0,-1.0,-1.0,-1.0,-1.0,-1.0,-1.0,-1.0,-1.0, -1.0,-1.0,-1.0,-1.0,-1.0,-1.0,-1.0,-1.0,-1.0,-1.0,-1.0});

    /* ubranie przykrywa g�ow� */
    if (zaslaniane & TS_HEAD)
        rozmiary[ROZ_OB_GLOWY] = wsp * WYMIAR(PLAYER_F_ROZ_GLOWA, ROZ_OB_GLOWY);

    /* ubranie os�ania szyj� */
    if (zaslaniane & TS_NECK)
        rozmiary[ROZ_OB_SZYI] = wsp * WYMIAR(PLAYER_F_ROZ_SZYJA, ROZ_OB_SZYI);

    /* ubranie przykrywa kt�ry� z bark�w */
    if (zaslaniane & (TS_R_SHOULDER | TS_L_SHOULDER))
        rozmiary[ROZ_OB_OKG] = wsp * WYMIAR(PLAYER_F_ROZ_BARKI, ROZ_OB_OKG);

    /* ubranie przykrywa klatk� */
    if (zaslaniane & TS_CHEST)
    {
        rozmiary[ROZ_DL_TULOWIA] = wsp * WYMIAR(PLAYER_F_ROZ_TULOW, ROZ_DL_TULOWIA);
        rozmiary[ROZ_OB_W_KLATCE] = wsp * WYMIAR(PLAYER_F_ROZ_PIERSI, ROZ_OB_W_KLATCE);
    }

    /* ubranie przykrywa brzuch */
    if (zaslaniane & TS_STOMACH)
    {
        rozmiary[ROZ_DL_TULOWIA] = wsp * WYMIAR(PLAYER_F_ROZ_TULOW, ROZ_DL_TULOWIA);
        rozmiary[ROZ_OB_W_PASIE] = wsp * WYMIAR(PLAYER_F_ROZ_PAS, ROZ_OB_W_PASIE);
        rozmiary[ROZ_OB_W_BIODRACH] = wsp * WYMIAR(PLAYER_F_ROZ_BIODRA, ROZ_OB_W_BIODRACH);
    }

    /* ubranie przykrywa kt�re� z ramion */
    if (zaslaniane & (TS_R_ARM | TS_L_ARM))
    {
        rozmiary[ROZ_DL_RAMIENIA] = wsp * WYMIAR(PLAYER_F_ROZ_DL_RAMIE, ROZ_DL_RAMIENIA);
        rozmiary[ROZ_OB_RAMIENIA] = wsp * WYMIAR(PLAYER_F_ROZ_OB_RAMIE, ROZ_OB_RAMIENIA);
    }

    /* ubranie przykrywa kt�re� z przedramion */
    if (zaslaniane & (TS_R_FOREARM | TS_L_FOREARM))
    {
        rozmiary[ROZ_DL_PRZEDRAMIENIA] = wsp * WYMIAR(PLAYER_F_ROZ_DL_PRAMIE, ROZ_DL_PRZEDRAMIENIA);
        rozmiary[ROZ_OB_PRZEDRAMIENIA] = wsp * WYMIAR(PLAYER_F_ROZ_OB_PRAMIE, ROZ_OB_PRZEDRAMIENIA);
    }

    /* ubranie nak�ada si� na kt�r�� z r�k */
    if (zaslaniane & (TS_R_HAND | TS_L_HAND))
    {
        rozmiary[ROZ_DL_DLONI] = wsp * WYMIAR(PLAYER_F_ROZ_DL_DLON, ROZ_DL_DLONI);
        rozmiary[ROZ_SZ_DLONI] = wsp * WYMIAR(PLAYER_F_ROZ_SZ_DLON, ROZ_SZ_DLONI);
        rozmiary[ROZ_OB_NADGARSTKA] =  wsp * WYMIAR(PLAYER_F_ROZ_NADGARSTEK, ROZ_OB_NADGARSTKA);
    }

    /* ubranie zak�ada si� na biodra */
    if (zaslaniane & TS_HIPS)
        rozmiary[ROZ_OB_W_BIODRACH] = wsp * WYMIAR(PLAYER_F_ROZ_BIODRA, ROZ_OB_W_BIODRACH);

    /* ubranie przykrywa kt�re� z ud */
    if (zaslaniane & (TS_R_THIGH | TS_L_THIGH))
    {
        rozmiary[ROZ_DL_UDA] = wsp * WYMIAR(PLAYER_F_ROZ_DL_UDO, ROZ_DL_UDA);
        rozmiary[ROZ_OB_UDA] = wsp * WYMIAR(PLAYER_F_ROZ_OB_UDO, ROZ_OB_UDA);
    }

    /* ubranie przykrywa kt�ry� z goleni */
    if (zaslaniane & (TS_R_SHIN | TS_L_SHIN))
    {
        rozmiary[ROZ_DL_GOLENI] = wsp * WYMIAR(PLAYER_F_ROZ_DL_GOLEN, ROZ_DL_GOLENI);
        rozmiary[ROZ_OB_GOLENI] = wsp * WYMIAR(PLAYER_F_ROZ_OB_GOLEN, ROZ_OB_GOLENI);
    }

    /* ubranie zak�ada si� na kt�r�� stop� */
    if (zaslaniane & (TS_R_FOOT | TS_L_FOOT))
    {
        rozmiary[ROZ_DL_STOPY] = wsp * WYMIAR(PLAYER_F_ROZ_DL_STOPA, ROZ_DL_STOPY);
        rozmiary[ROZ_SZ_STOPY] = wsp * WYMIAR(PLAYER_F_ROZ_SZ_STOPA, ROZ_SZ_STOPY);
        rozmiary[ROZ_OB_KOSTKI] = wsp * WYMIAR(PLAYER_F_ROZ_KOSTKA, ROZ_OB_KOSTKI);
    }

    /* ubranie zak�ada si� na kt�ry� z palc�w */
    if (zaslaniane & (TS_R_FINGER | TS_L_FINGER))
        rozmiary[ROZ_SZ_DLONI] = wsp * WYMIAR(PLAYER_F_ROZ_SZ_DLON, ROZ_SZ_DLONI);
}


/**
 * Zwraca tablic� rozmiar�w. Je�eli jej nie ma, to najpierw j� wylicza.
 *
 * @param zaslaniane sloty zas�aniane przez dane ubranie.
 * @param for_pl obiekt, dla kt�rego wyliczamy rozmiary.
 *
 * @return tablica z rozmiarami.
 */
varargs float*
pobierz_rozmiary(int zaslaniane, object for_pl = 0)
{
    if (!pointerp(rozmiary))
        wylicz_rozmiary(zaslaniane, for_pl);

    return rozmiary;
}

/**
 * Ustawia tablic� rozmiar�w.
 *
 * @param rozm nowa tablica rozmiar�w.
 */
public void
ustaw_rozmiary(float* rozm)
{
    rozmiary = rozm;
}

/**
 * Generuje opis wy�wietlany przy zak�adaniu rzeczy.
 *
 * @param location sloty zajmowane przez dan� rzecz
 */
public string
wear_how(int location)
{
    return " na " + COMPOSITE_SLOTS(location, PL_BIE);
}

/**
 * Ustala rozmiary ubrania w zale�no�ci od podanego wymiaru.
 *
 * @param wymiar jaki ubranie ma mie� wymiar.
 */
public void
set_size(mixed size)
{
    string race;
    int gender, il;
    float factor;
    float* wspolczynniki;

    if (floatp(size))
    {
        factor = size;
        if(factor <= 0.5)
            size = "XXXXS";
        else if(factor > 0.5 && factor <= 0.6)
            size = "XXXS";
        else if(factor > 0.6 && factor <= 0.7)
            size = "XXS";
        else if(factor > 0.7 && factor <= 0.8)
            size = "XS";
        else if(factor > 0.8 && factor <= 0.9)
            size = "S";
        else if(factor > 0.9 && factor <= 1.0)
            size = "M";
        else if(factor > 1.0 && factor <= 1.1)
            size = "L";
        else if(factor > 1.1 && factor <= 1.2)
            size = "XL";
        else if(factor > 1.2 && factor <= 1.3)
            size = "XXL";
        else if(factor > 1.3 && factor <= 1.4)
            size = "XXXL";
        else
            size = "XXXXL";
    }
    else if (stringp(size))
    {
        size = upper_case(size);
        switch (size)
        {
            case "XXXXS": factor = 0.5; break;
            case "XXXS": factor = 0.6; break;
            case "XXS": factor = 0.7; break;
            case "XS": factor = 0.8; break;
            case "S": factor = 0.9; break;
            case "M": factor = 1.0; break;
            case "L": factor = 1.1; break;
            case "XL": factor = 1.2; break;
            case "XXL": factor = 1.3; break;
            case "XXXL": factor = 1.4; break;
            case "XXXXL": factor = 1.5; break;
        }
    }
    else
        return;

    if(!orginal_size)
        orginal_size = size;

    pobierz_rozmiary(wear_at | slot_side_bits);

    race = TO->query_prop(ARMOUR_S_DLA_RASY);

    if (!stringp(race))
        race = "cz^lowiek";

    gender = TO->query_prop(ARMOUR_I_DLA_PLCI);
    wspolczynniki = RACEROZMOD[race][gender];

    if (wspolczynniki == 0)
        return;
/*
write("race= " + race + ", gender= " + gender + ", factor= " + ftoa(factor) + "\nrozmiary przed:");
dump_array(rozmiary);
write("wspolczynniki:");dump_array(wspolczynniki);*/
    for (il = 0; il <= ROZ_MAX; il++)
        if (rozmiary[il] != -1.0)
            rozmiary[il] = wspolczynniki[il] * factor;
}

/**
 * @return factor rozmiaru ciuszka
 */
public float query_f_size()
{
    int i;
    float factor = 1.0;
    int gender = TO->query_prop(ARMOUR_I_DLA_PLCI);
    string race = TO->query_prop(ARMOUR_S_DLA_RASY);

    if (!stringp(race))
        race = "cz^lowiek";

    pobierz_rozmiary(wear_at | slot_side_bits);

    for (i = 0; i < sizeof(rozmiary); i++)
        if (rozmiary[i] > 0.0)
            break;

    if (i >= sizeof(rozmiary) || i < 0 || rozmiary[i] <= 0.0)
        factor = 1.0;
    else
        factor = rozmiary[i];

    return factor;
}

/**
 * @return zwraca nazwe rozmiaru ubrania - M, XL, etc
 */
public string
query_n_size()
{
    float factor = query_f_size();

    if(factor <= 0.5)
        return "XXXXS";
    else if(factor > 0.5 && factor <= 0.6)
        return "XXXS";
    else if(factor > 0.6 && factor <= 0.7)
        return "XXS";
    else if(factor > 0.7 && factor <= 0.8)
        return "XS";
    else if(factor > 0.8 && factor <= 0.9)
        return "S";
    else if(factor > 0.9 && factor <= 1.0)
        return "M";
    else if(factor > 1.0 && factor <= 1.1)
        return "L";
    else if(factor > 1.1 && factor <= 1.2)
        return "XL";
    else if(factor > 1.2 && factor <= 1.3)
        return "XXL";
    else if(factor > 1.3 && factor <= 1.4)
        return "XXXL";
    else
        return "XXXXL";
}

/**
 * Zwraca s�owny opis rozmiaru ubrania.
 *
 * @return opis rozmiaru ubrania.
 */
string
query_size()
{
    float factor = query_f_size();

    if(factor < 0.6)
        return "malutkie";
    else if(factor < 0.8)
        return "bardzo ma�e";
    else if(factor < 0.95)
        return "ma�e";
    else if(factor < 1.05)
        return "�rednie";
    else if(factor < 1.2)
        return "du�e";
    else if(factor < 1.4)
        return "bardzo du�e";
    else
        return "ogromne";
}

/**
 * Sprawdza, czy dane ubranie mo�e by� za�o�one.
 *
 * @param na_miare czy dana zbroja ma by� zrobiona na miar�.
 * @param slot na kt�ry z dopuszczalnych slot�w
 *
 * @return czy dane ubranie mo�e by� za�o�one.
 */
varargs int
check_can_wear(mixed na_miare = 0, int slot = -1)
{
    int state, miejsca, zal_pier, zal_palec, ix, il, i;
    int rozciagliwosc;
    object *pierscienie;
    float* rozm;

    rozm = wearer->oblicz_rozmiary_osoby();

    if(!pointerp(rozm))
        return 5;

    ix = ((slot_side_bits || ac_side_bits) ? 2 : 1);

    while(--ix >= 0)
    {
        worn_on_part = wear_at;

        if(slot != -1 && (slot_side_bits || ac_side_bits))
            worn_on_part |= slot;
        else
            worn_on_part |= (slot_side_bits << !ix);

        if(na_miare)
            wylicz_rozmiary(worn_on_part, wearer);

        pobierz_rozmiary(worn_on_part, na_miare ? wearer : 0);

        state = 0;

        for(il = 0; il <= ROZ_MAX; ++il)
        {
            if(rozmiary[il] == -1.0)
                continue;

            if(rozm[il] == -1.0)
            {
                state = 4;
                break;
            }

            if(rozmiary[il] * 0.8 > rozm[il])
            {
                if(TO->is_prop_set(ARMOUR_I_D_ROZCIAGLIWOSC))
                {
                    rozciagliwosc = TO->query_prop(ARMOUR_I_D_ROZCIAGLIWOSC);

                    if((rozciagliwosc <= 0) || (rozciagliwosc > 100))
                        rozciagliwosc = 100;

                    if(rozmiary[il] * 0.8 * (itof(100 - rozciagliwosc)/100.0) > rozm[il])
                    {
                        state = 1;
                        break;
                    }
                }
                else
                {
                    state = 1;
                    break;
                }
            }
            if(rozmiary[il] * 1.3 < rozm[il])
            {
                if(TO->is_prop_set(ARMOUR_I_U_ROZCIAGLIWOSC))
                {
                    rozciagliwosc = TO->query_prop(ARMOUR_I_U_ROZCIAGLIWOSC);

                    if(rozciagliwosc <= 0)
                        rozciagliwosc = 100;

                    if(rozmiary[il] * (1.3 + itof(rozciagliwosc)/100.0) < rozm[il])
                    {
                        state = 2;
                        break;
                    }
                }
                else
                {
                    state = 2;
                    break;
                }
            }
        }

        /* pier�cionek zak�adamy na serdeczny palec */
        if((!state) && ((worn_on_part == TS_R_FINGER) || (worn_on_part == TS_L_FINGER)))
        {
            miejsca = ftoi(rozm[ROZ_DL_DLONI] / (0.6 * rozm[ROZ_SZ_DLONI]));
            pierscienie = wearer->query_armour(worn_on_part);
            zal_pier = 0;
            zal_palec = sizeof(pierscienie);
            for(i = 0; i < zal_palec; ++i)
            {
                if((sizeof(pierscienie[i]->query_slots()) == 1) &&
                    pierscienie[i]->query_slots()[0] == worn_on_part)
                {
                    ++zal_pier;
                }
            }

            if(zal_pier >= miejsca)
                state = 3;
        }

        if(!state)
        {
            ac_worn = (ac_slots | (ac_side_bits << (!ix)));
            break;
        }
    }

    return state;
}

/**
 * Zak�ada dan� rzecz.
 *
 * @param cicho     czy komunikaty o b��dzie maj� si� nie pojawia�.
 * @param na_miare  czy dana zbroja ma by� zrobiona na miar�.
 * @param na_co     je�li zbroja mo�e by� ubrana na r�ne hitlokacje, to na kt�r�
 *                  chcemy j� ubra�.
 *
 * @return <ul>
 * <li> string - informacja o b��dzie
 * <li> 1 - sukces
 * </ul>
 */
public varargs mixed
wear_me(int cicho, mixed na_miare = 0, string na_co = 0)
{
    int wret, il, ix, i;
    string wfail, what, how;
    object *przobjtab, *tmpprzobjtab, przobj = 0;
    mixed slociki;

    what = TO->short(this_player(), PL_BIE);

    if(!query_slots(1) || !pointerp(query_slots(1)) || !sizeof(query_slots(1)))
        return "Nie mo�esz tego za�o�y�.\n";

    if(worn)
        return "Ju� masz na sobie " + what + ".\n";
    else if(this_player() != environment(this_object()))
    {
        return "Musisz wpierw wzi�� " + what + " �eby m�c " +
            (TO->query_rodzaj() == PL_ZENSKI ? "j�" :
            (TO->query_rodzaj() >= PL_NIJAKI_OS ? "je" : "go")) +
            " za�o�y�.\n";
    }
    else if(TO->query_prop(OBJ_I_BROKEN))
    {
        return "Ale " + TO->short(PL_MIA) + " " + (TO->query_tylko_mn() ? "s�" :
            "jest") + " kompletnie zniszcz" + TO->koncowka("ony", "ona", "one",
            "eni", "one") + "!\n";
    }

    if(objectp(na_miare))
        wearer = na_miare;
    else
        wearer = this_player();

    slociki = wearer->wear_arm_check_slots(TO, na_co);

    switch(check_can_wear(na_miare, (sizeof(slociki) == 1 ? slociki[0] : -1)))
    {
        case 1:
            return "Ale " + TO->short(PL_MIA) + " " + (TO->query_tylko_mn() ? "s�" :
                "jest") + " zbyt du" +
                TO->koncowka("�y", "�a", "�e", "zi", "�e") + "!\n";
        case 2:
            return "Ale " + TO->short(PL_MIA) + " " + (TO->query_tylko_mn() ? "s�" :
                "jest") + " zbyt ma" +
                TO->koncowka("�y", "�a", "�e", "li", "�e") + "!\n";
        case 3:
            return "Ale nie zmie�cisz ju� " + TO->short(PL_DOP) + " na serdecznym " +
                "palcu " + ((worn_on_part == TS_R_FINGER) ? "prawej" : "lewej") +
                " r�ki!\n";
        case 4:
            /* najpierw musimy znale�� przeszkadzaj�c� rzecz */
            for(il = TS_HEAD; il <= MAX_TS; il <<= 1)
            {
                if(il & worn_on_part)
                {
                    przobjtab = wearer->query_armour(il);
                    tmpprzobjtab = wearer->query_slot(il);
                    if(pointerp(przobjtab))
                    {
                        if(pointerp(tmpprzobjtab))
                            tmpprzobjtab -= przobjtab;
                    }
                    if(pointerp(tmpprzobjtab) && sizeof(tmpprzobjtab))
                        przobj = tmpprzobjtab[-1..][0];
                }
            }
            if(objectp(przobj))
                return "W za�o�eniu " + TO->short(PL_DOP) + " przeszkadza ci " + przobj->short(PL_MIA) + "!\n";
            else
                return "W za�o�eniu " + TO->short(PL_DOP) + " co� ci przeszkadza!\n";
        case 5:
            return "Nie mo�esz za�o�y� tego ubrania, gdy� nie masz postaci!?!\n" +
                "Koniecznie zg�o� b��d o tym.\n";
    }

    if(stringp(wfail = wearer->wear_arm(this_object(), na_co)))
        return wfail;

    wret = 0;

    /*
     * Funkcja do zak�adania w innym obiekcie.
     */
    if((!cicho) && ((!wear_func) || (!(wret = wear_func->wear(this_object())))))
    {
        if(worn_on_part == A_ROBE)
        {
            write("Otulasz si� " + TO->short(this_player(), PL_NAR) + ".\n");
            saybb(QCIMIE(this_player(), PL_MIA) + " otula si� " +
                QSHORT(this_object(), PL_NAR) + ".\n");
        }
        else
        {
            how = (na_co ? " na " + na_co : wear_how(worn_on_part));
            write("Zak�adasz " + what + how + ".\n");
            saybb(QCIMIE(this_player(), PL_MIA) + " zak�ada " +
                QSHORT(this_object(), PL_BIE) + ".\n");
        }
    }

    /*
     * Je�eli funkcja do zak�adania zwr�ci�a warto�� < 0, to nie mo�emy za�o�y� rzeczy
     */
    if(intp(wret) && wret >= 0)
    {
//         TO->add_prop(OBJ_M_NO_GIVE, "Najpierw musisz zdj�� " +short(wearer, PL_BIE)+".\n");
        TO->set_obj_subloc(SUBLOC_WORNA);
        fryz_zakladanie();
        sprawdzanie_nagosci();
        worn = 1;
        return 1;
    }
    else
    {
        wearer->remove_arm(this_object());
        worn = 0;
        if(stringp(wret))
           return wret;
        else
            return "Nie mo�esz za�o�y� " + TO->short(this_player(), PL_DOP) + ".\n";
    }
}

/**
 * Funkcja wywo�ywana do za�o�enia tej rzeczy.
 *
 * @return zobacz <b>wear_me()</b>
 */
public mixed
command_wear()
{
    return wear_me();
}

/**
 * Funkcja zajmuje si� zmienieniem opisu sublokacji fryzura_na_glowie
 */
void
fryz_zakladanie()
{
    int pom = 0;
    int i;
    string *subloki;
    object *inwen_gracza;
    object fryz;
    inwen_gracza = all_inventory(this_player());

    for(i=0;i<sizeof(inwen_gracza);i++)
        if(inwen_gracza[i]->query_short() == "fryzura")
            pom = i + 1;

    if(pom)
    {
        fryz = inwen_gracza[pom - 1];
        pom = 0;
        subloki = this_player()->query_sublocs();
        for(i =0;i<sizeof(subloki);i++)
            if(subloki[i] == "fryzura_na_glowie")
                pom = 1;

        if(this_player()->query_armour(TS_HEAD)->query_short())
        {
            if(pom)
                this_player()->remove_subloc("fryzura_na_glowie");
        }
        else
        {
            if(!pom)
                this_player()->add_subloc("fryzura_na_glowie", fryz);
        }
    }
}

/**
 * Funkcja sprawdza, czy dany gracz ma co� na�o�onego na piersi, brzuch albo biodra.
 * Je�eli ma, to usuwany jest przymiotnik 'nagi'. W przeciwnym wypadku jest on dodawany.
 */
void
sprawdzanie_nagosci()
{
    mixed tmptab;
    mixed tmpe = this_player()->query_przym(1);

    if(!pointerp(tmpe))
        tmpe = ({ tmpe });

    if(!(pointerp(tmptab = this_player()->query_armour(TS_CHEST)) && sizeof(tmptab)) &&
        !(pointerp(tmptab = this_player()->query_armour(TS_STOMACH)) && sizeof(tmptab)) &&
        !(pointerp(tmptab = this_player()->query_armour(TS_HIPS)) && sizeof(tmptab)) &&
        !(pointerp(tmptab = this_player()->query_armour(TS_ROBE)) && sizeof(tmptab)))
    {
        if(member_array("nagi", tmpe) == -1)
        {
            write("Jeste� teraz zupe�nie nag"+this_player()->koncowka("i", "a", "ie")+".\n");
            this_player()->dodaj_przym_do_listy(10,"nagi","nadzy");
        }
    }
    else
        this_player()->usun_przym_z_listy(10,"nagi","nadzy");
}

/**
 * Zdejmuje dan� rzecz.
 *
 * @return <ul>
 * <li> 1 - sukces
 * <li> 0 - pora�ka
 * <li> string - informacja o b��dzie
 * </ul>
 */
public mixed
remove_me(int cicho = 0)
{
    mixed wret;
    int il;
    object ob;

    if(!worn || !wearer)
        return 0;

    int visible = (member_array(TO, wearer->query_worn_visible()) != -1);

    /*
     * Trzeba najpierw sprawdzi�, czy nic nam nie przeszkadza.
     */
    for(il = TS_HEAD; il <= worn_on_part; il <<= 1)
    {
        if(worn_on_part & il)
        {
            if((!visible) && (pointerp(wearer->query_slot(il))) &&
                ((ob = wearer->query_slot(il)[sizeof(wearer->query_slot(il))-1]) !=
                this_object()))
            {
                break;
            }
            else
                ob = 0;

            if((il == TS_R_FINGER) && (pointerp(wearer->query_slot(TS_R_HAND))) &&
                sizeof(wearer->query_slot(TS_R_HAND)) &&
                (member_array((ob = wearer->query_slot(TS_R_HAND)[-1]), wearer->query_slot(-1)) >
                member_array((wearer->query_slot(il)[-1]), wearer->query_slot(-1))))
            {
                break;
            }
            else
                ob = 0;

            if((il == TS_L_FINGER) && (pointerp(wearer->query_slot(TS_L_HAND))) &&
                sizeof(wearer->query_slot(TS_L_HAND)) &&
                (member_array((ob = wearer->query_slot(TS_L_HAND)[-1]), wearer->query_slot(-1)) >
                member_array((wearer->query_slot(il)[-1]), wearer->query_slot(-1))))
            {
                break;
            }
            else
                ob = 0;

            if((il == TS_R_HAND) && (pointerp(wearer->query_slot(TS_R_FINGER))) &&
                sizeof(wearer->query_slot(TS_R_FINGER)) &&
                (member_array((ob = wearer->query_slot(TS_R_FINGER)[-1]), wearer->query_slot(-1)) >
                member_array((wearer->query_slot(il)[-1]), wearer->query_slot(-1))))
            {
                break;
            }
            else
                ob = 0;

            if((il == TS_L_HAND) && (pointerp(wearer->query_slot(TS_L_FINGER))) &&
                sizeof(wearer->query_slot(TS_L_FINGER)) &&
                (member_array((ob = wearer->query_slot(TS_L_FINGER)[-1]), wearer->query_slot(-1)) >
                member_array((wearer->query_slot(il)[-1]), wearer->query_slot(-1))))
            {
                break;
            }
            else
                ob = 0;
        }
        else
            ob = 0;
    }

    if(ob)
    {
        write("W zdj�ciu " + TO->short(this_player(), PL_DOP) +
                " przeszkadza ci " + ob->short(this_player(), PL_MIA) + "!\n");
        return 0;
    }

    /*
     * Funkcja zdejmuj�ca w innym obiekcie.
     */

    if (!cicho && ((!wear_func) || (!(wret = wear_func->remove(this_object())))))
    {
        if (TO->check_seen(this_player()))
            write("Zdejmujesz z siebie " + TO->short(PL_BIE) + ".\n");
        else
            write("Zdejmujesz co�.\n");
        saybb(QCIMIE(this_player(), PL_MIA) + " zdejmuje z siebie " +
                QSHORT(this_object(), PL_BIE) + ".\n");
    }

    if (intp(wret) && (wret >= 0))
    {
//         TO->remove_prop(OBJ_M_NO_GIVE);
        TO->set_obj_subloc(0);
        wearer->remove_arm(this_object());
        fryz_zakladanie();
        sprawdzanie_nagosci();
        worn = 0;
        worn_on_part = 0;
        ac_worn = 0;
        return 1;
    }

    return (stringp(wret) ? wret : "");
}

/**
 * Ustawia obiekt, kt�ry definiuje funkcje <b>wear()</b> i <b>remove()</b>
 * dla tej rzeczy. Funkcje te mog� zwraca� nast�puj�ce warto�ci:
 * <ul>
 * <li>   0 - Nie wp�ywa na to, czy rzecz mo�e by� za�o�ona / zdj�ta
 * <li>   1 - Rzecz mo�e by� za�o�ona / zdj�ta, lecz nic nie powinno
 *            by� wypisane na ekran (odpowiedni tekst zosta� ju� wypisany
 *            przez funkcj�).
 * <li>  -1 - Rzecz nie mo�e by� za�o�ona / zdj�ta. Defaultowa informacja
 *            o b��dzie b�dzie wypisana na ekran.
 * <li>  string - Recz nie mo�e by� za�o�ona / zdj�ta. Zwr�cony napis b�dzie
 *                wypisany na ekran.
 * </ul>
 */
public void
set_wf(object ob)
{
    if (!this_object()->query_lock())
        wear_func = ob;
}

/**
 * @return Obiekt, kt�ry definiuje funkcje <b>wear()</b> i <b>remove()</b>
 *         dla tej rzeczy.
 */
public object
query_wf()
{
    return wear_func;
}

/**
 * Funkcja ta zwraca opis jak si� zak�ada dan� rzecz. Jest on zazwyczaj
 * wypisywany przy ocenianiu przedmiotu. Opis zawiera informacj� o slotach,
 * gdzie dana rzecz powinna by� noszona.
 *
 * @return string - opis.
 */
string
wearable_item_usage_desc()
{
    int parts = 0;

    foreach(int slot : query_slots(1))
        parts |= slot;

    return (this_object()->koncowka("Ten ","T� ","To ") +
        this_object()->short(this_player(),PL_BIE) +
        " powinno si� zak�ada�" + wear_how(parts) + ".\n");
}

/**
 * Funkcja ta zwraca opis, czy dana rzecz mo�e by� za�o�ona przez gracza.
 *
 * @return string - opis.
 */
string
wearable_item_can_wear_desc()
{
    int il, ccw;
    object* przobjtab;
    object* tmpprzobjtab;
    object przobj = 0;

    if(query_worn() == TP)
        return "";

    wearer = this_player();

    ccw = check_can_wear();

    wearer = 0;

    switch(ccw)
    {
        case 1:
            return "Nie " + this_player()->koncowka("m�g�by�", "mog�aby�", "mog�oby�") +
                " za�o�y� " + TO->short(PL_DOP) + ", gdy� " + (TO->query_tylko_mn() ? "s�" : "jest") +
                " " +  TO->koncowka("on", "ona", "ono", "oni", "one") + " zbyt " +
                TO->koncowka("du�y", "du�a", "du�e", "duzi", "du�e") + "!\n";
        case 2:
            return "Nie " + this_player()->koncowka("m�g�by�", "mog�aby�", "mog�oby�") +
                " za�o�y� " + TO->short(PL_DOP) + ", gdy� " + (TO->query_tylko_mn() ? "s�" : "jest") +
                " " +  TO->koncowka("on", "ona", "ono", "oni", "one") + " zbyt " +
                TO->koncowka("ma�y", "ma�a", "ma�e", "mali", "ma�e") + "!\n";
        case 3:
            return "Nie " + this_player()->koncowka("zmie�ci�by�", "zmie�ci�aby�", "zmie�ci�oby�") +
                " ju� " + TO->short(PL_DOP) + " na serdecznym palcu " + ((worn_on_part == TS_R_FINGER) ? "prawej" : "lewej") +
                " r�ki!\n";
        case 4:
            /* najpierw musimy znale�� przeszkadzaj�c� rzecz */
            for(il = TS_HEAD; il <= MAX_TS; il <<= 1)
            {
                if(il & worn_on_part)
                {
                    przobjtab = wearer->query_armour(il);
                    tmpprzobjtab = wearer->query_slot(il);
                    if(pointerp(przobjtab))
                        if(pointerp(tmpprzobjtab))
                            tmpprzobjtab -= przobjtab;

                    if(pointerp(tmpprzobjtab) && sizeof(tmpprzobjtab))
                        przobj = tmpprzobjtab[-1..][0];
                }
            }

            if(objectp(przobj))
                return "W za�o�eniu " + TO->short(PL_DOP) + " przeszkadza�oby ci " + przobj->short() + "!\n";

            return "W za�o�eniu " + TO->short(PL_DOP) + " co� ci by przeszkadza�o!\n";
        case 5:
            return "Nie " + this_player()->koncowka("m�g�by�", "mog�aby�", "mog�oby�") +
                " za�o�y� tego ubrania, gdy� nie masz postaci!?!\n" +
                "Koniecznie zg�o� b��d o tym.\n";
    }

    return capitalize(TO->short(PL_MIA)) + " " + TO->koncowka("pasowa�by", "pasowa�aby", "pasowa�oby", "pasowaliby", "pasowa�yby") +
        " na ciebie.\n";
}

/**
 * Funkcja ta wypisuje jak powinno si� nosi� dan� rzecz. <b>Musisz</b>
 * wywo�ywa� t� funkcj� z przedefiniowanej funkcji <b>appraise_object()</b>
 * w twoim przedmiocie.
 */
void
appraise_wearable_item()
{
    write(wearable_item_usage_desc());
    write(wearable_item_can_wear_desc());
}

/**
 * Trzeba si� upewni�, �e rzecz jest zdejmowana kiedy opuszcza otoczenie. Normalnie
 * powinno si� wywo�ywa� t� funkcj� z <b>leave_env()</b> w dziedzicz�cym obiekcie.
 *
 * @param from opuszczane otoczenie
 * @param to nowe otoczenie
 */
public void
wearable_item_leave_env(object from, object to)
{
    if (!worn)
        return;

    if ((!wear_func || !wear_func->remove(this_object())) && wearer)
    {
        wearer->remove_arm(this_object());
        tell_object(wearer, "Zdejmujesz " + this_object()->short(this_player(),PL_BIE) + ".\n");
    }
    else
        return;

    fryz_zakladanie();
    sprawdzanie_nagosci();

    wearer = 0;
    worn = 0;
}

/**
 * Sprawdza, czy obiekt jest za�o�ony, czy nie.
 *
 * @return Obiekt, kt�ry nosi dan� rzecz, je�eli jest za�o�ona.
 */
public object
query_worn()
{
    return (worn ? wearer : 0);
}

/**
 * Sprawdza, czy rzecz jest widoczna dla danego obiektu.
 *
 * @return Czy dana rzecz jest widoczna dla danego obiektu.
 */
public int
wearable_item_check_seen(object for_obj)
{
    if (query_worn())
        return (member_array(TO, wearer->query_worn_visible()) != -1);

    return 1;
}

public nomask string
query_rozmiary_auto_load()
{
    int first = 1;
    string toReturn = " ~R[";

    foreach (float roz : pobierz_rozmiary())
    {
        if (!first)
            toReturn += ",";

        toReturn += sprintf("%f", roz);
        first = 0;
    }

    return toReturn + "]R~ ";
}

public nomask string
init_rozmiary_arg(string arg)
{
    string rozTab, toReturn, foobar;
    float* newRoz = ({});
    float oneRoz;

    if (arg == 0)
        return 0;

    sscanf(arg, "%s~R[%s]R~ %s", foobar, rozTab, toReturn);
    foreach (string elem : explode(rozTab, ","))
    {
        sscanf(elem, "%f", oneRoz);
        newRoz += ({ oneRoz });
    }
    ustaw_rozmiary(newRoz);

    return foobar + toReturn;
}

public nomask string
query_wearable_auto_load()
{
    if(query_worn())
        return " ~Worn~ ";

    return "";
}

public nomask string
init_wearable_arg(string arg)
{
    string toReturn;

    if (arg == 0)
        return 0;

//    write("init_wearable_arg(" + arg + ")\n");
    if (wildmatch("*~Worn~*", arg))
    {
        string tmp;
        sscanf(arg, "%s~Worn~ %s", tmp, toReturn);
        foreach (string curstr : explode(tmp, ""))
        {
            if (curstr != " ")
                return arg;
        }
        should_wear = 1;
        return toReturn;
    }
    return arg;
}

void
wearable_item_enter_env(object dest, object old)
{
    if(should_wear)
    {
        wear_me(1);
        should_wear = 0;
    }
}

public int
query_slot_side_bits()
{
    return slot_side_bits;
}

void
set_orginal_size(string size)
{
    orginal_size = size;
}

string
query_orginal_size()
{
    if(!orginal_size)
        orginal_size = query_n_size();

    return orginal_size;
}

public void
set_wear_at_slots(int *slots)
{
    wear_at_slots = slots;
}

public int *
query_wear_at_slots()
{
    return wear_at_slots;
}

public int
is_wearable_item()
{
    return 1;
}