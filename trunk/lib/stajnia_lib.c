/*
                Standardowa lokacja stajni. Wystaczy zainheritowac
                Zetar 26.10.2001
        Ortografie poprawial Avber (13.II.02)
*/



inherit "/std/room";
inherit "/lib/trade";
inherit "/cmd/std/command_driver";

#include <macros.h>
#include <stdproperties.h>
#include <pl.h>

//my declarations
private static string nazwa_stajni = "";
private static string jakie_boxy = "";
private static string stajenny_str;
private static string tekst_nieobecnego   = "Nie widzisz w okolicy
stajennego.";
private static object stajenny_obj;
private static int    jest_stajenny = 0;
private static int    cena = 0;

int     zostawianie(string str);
int     odebranie(string str);
void    ustaw_nazwe_stajni(string str);
void    ustaw_boxy(string str);
void    ustaw_stajennego(string str);
void    ustaw_tekst_nieobecnego(string str);
void    ustaw_cene(int i);

string  query_boxy()                  {return jakie_boxy;}
string  query_nazwa_stajni()          {return nazwa_stajni;}
string  query_stajenny()              {return stajenny_str;}
string  query_tekst_nieobecnego()     {return tekst_nieobecnego;}
int     query_cena()                  {return cena;}

#define FILTER_HORSES(x)         filter((x), &->query_horse())
#define WLASCICIEL_O             "_wlascicel_konia_zostawionego_w_stajni_"
#define FILTER_H_OWNER(x, p)     filter((x), &operator(==)(p) @
(&->query_prop(WLASCICIEL_O)))

/*
 * Function : ustaw_cene
 * Description :ustawia cene oplaty za przechwanie wierzchowca w stajni
 * Arguments: int i - wartosc ceny w miedziakach
 * Returns:
 */
void
ustaw_cene(int i)
{
        cena = i;
}

/*
 * Function : ustaw_tekst_nieobecnego
 * Description : ustawia tekst jaki pojawi sie graczowie gdy nie zastanie
stajennego
 *               w miejscu gdzie ten powinien byc
 * Arguments: string str - tekst do wyswietlenia
 * Returns:
 */
void
ustaw_tekst_nieobecnego(string str)
{
        tekst_nieobecnego = str;
}

/*
 * Function : ustaw_nazwe_stajni
 * Description : ustawia unikalna nazwe stajni dla swiata - wazne zeby
 *               zeby sie nie powtarzala
 * Arguments: string str - nazwa stajni
 * Returns:
 */
void
ustaw_nazwe_stajni(string str)
{
        nazwa_stajni = str;
}

/*
 * Function : ustaw_stajennego
 * Description : ustawia ewentualnego stajennego w stajni
 * Arguments: string str - sciezka do pliku stajennego
 * Returns:
 */
void
ustaw_stajennego(string str)
{
        stajenny_str = str;
        jest_stajenny = 1;
}

/*
 * Function : ustaw_boxy
 * Description : ustawia boxy dlka koni to jest lokacje w formie magazynu w
ktorej
 *               beda przechowywane konie
 * Arguments: string str - sciezka do pliku boxa
 * Returns:
 */
void
ustaw_boxy(string str)
{
        jakie_boxy = str;
}

void
reset_room()
{
        if (!stajenny_obj && stajenny_str)
            {
                 stajenny_obj = clone_object(stajenny_str);
                 stajenny_obj -> move(this_object());
            }

         ::reset_room();
}

/*
 * Function : create_stajnia
 * Description : tworzy pomieszczenie stajni
 * Arguments:
 * Returns:
 */
void
create_stajnia()
{
}


nomask void
create_room()
{


    add_cmd_item( ({ "tabliczke" }), ({ "czytaj", "przeczytaj" }),"@@tab");


    config_default_trade();

    add_prop(ROOM_I_INSIDE, 1);
    add_prop(ROOM_I_NO_HORSE_RIDE, 0);
    add_prop(ROOM_I_NO_HORSE_LEAD, 0);



    create_stajnia();

    reset_room();
}

string
tab()
{
        return
                "Witaj przybyszu! W tej stajni mozesz zostawic swego "+
                "wierzchowca, zeby odpoczal i zostal nalezycie
oporzadzony.\n\n"+
                "zostaw <konia>          - zostawienie wierzchowca w stajni\n"+
                "odbierz <konia>         - odebranie zostawionego
wierzchowca\n\n"+
                "Odbierac i zostawiac mozna tylko pojedynczo.\n"+
                "Cena uslugi wynosi :    - "+cena+" miedziakow\n"+
                "Dziekujemy za skorzystanie z naszych uslug.\n\n";


}

void
init()
{
        ::init();
        add_action(zostawianie,"zostaw");
        add_action(odebranie,"odbierz");
}

int
zostawianie(string str)
{
        object *cel;
        int ile=0;

    cel = parse_this(str,"%l:"+PL_BIE);
        if (sizeof(cel))
          {
          if (jest_stajenny)
          if (!present(stajenny_obj,this_object()))
            {
             notify_fail(tekst_nieobecnego + "\n");
             return 0;
            }
        if (jest_stajenny)
           if (!CAN_SEE(stajenny_obj,this_player()))
             {
              stajenny_obj->command("emote rozglada sie z wyrazem zagubienia
na twarzy");
              return 0;
             }
                if (sizeof(cel)>1)
                  {
                        notify_fail("Mozesz zostawic tylko jednego wierzchowca
naraz.\n");
                        return 0;
                  }
            if (!(cel[0]->query_horse()))
              {
                notify_fail("No pieknie, a jakby ktos tak ciebie chcial w
stajni zostawic?\n");
                return 0;
              }
            if ((cel[0]->query_owner())!=(this_player()))
              {
                notify_fail("Ten "+cel[0]->short(this_player(), PL_MIA)+" nie
jest twoja wlasnoscia, wiec "+
                            "nie mozesz zostawic
"+cel[0]->koncowka("go","ja","go")+" w stajni.\n");
                return 0;
              }
            if (cel[0]==(this_player()->query_prop(LIVE_O_ON_HORSE)))
              {
                    notify_fail("Przeciez siedzisz na
"+cel[0]->short(this_player(), PL_MIE)+". Moze "+
                                "najpierw z
"+cel[0]->koncowka("niego","niej","niego")+" zsiadziesz?\n");
            return 0;
          }
        if (cel[0] == (this_player()->query_prop(LIVE_O_HORSE_PROWADZONY)))
              {
                    notify_fail("Przeciez trzymasz
"+cel[0]->short(this_player(), PL_BIE)+". Moze "+
                               "pierw "+cel[0]->koncowka("go","ja","go")+"
puscisz?\n");
            return 0;
          }
        if (!can_pay(cena,this_player()))
          {
                notify_fail("Nie stac cie na oplacenie pobytu
"+cel[0]->short(this_player(), PL_BIE)+" w stajni.\n");
                return 0;
          }
        pay(cena, this_player());

        if (!jest_stajenny)
        {
            this_player()->catch_msg("Placisz i zostawiasz
"+cel[0]->short(this_player(), PL_BIE)+" w stajni, a stajenny "+
              "odprowadza "+cel[0]->koncowka("go","ja","go")+
               " do boksu.\n");
            saybb(QCIMIE(this_player(),PL_MIA)+" placi i zostawia
"+cel[0]->short(this_player(), PL_BIE)+" w stajni, "+
            "a stajenny odprowadza "+cel[0]->koncowka("go","ja","go")+
            " do boksu.\n");
        }
        else
        {
            this_player()->catch_msg("Placisz i zostawiasz
"+cel[0]->short(this_player(), PL_BIE)+" w stajni, a
"+QIMIE(stajenny_obj,PL_MIA)+
              " odprowadza "+cel[0]->koncowka("go","ja","go")+
              " do boksu.\n");
            saybb(QCIMIE(this_player(),PL_MIA)+" placi i zostawia
"+cel[0]->short(this_player(), PL_BIE)+" w stajni, "+
              "a "+QIMIE(stajenny_obj,PL_MIA)+" odprowadza
"+cel[0]->koncowka("go","ja","go")+
              " do boksu.\n");
        }
        cel[0]->add_prop(WLASCICIEL_O, this_player()->query_real_name());
        ile = (this_player()->query_prop("_zostawil_konia_w_"+nazwa_stajni)) +
1;
        this_player()->change_prop("_zostawil_konia_w_"+nazwa_stajni, ile);
        cel[0]->move(jakie_boxy);
        return 1;
       }

    notify_fail("Zostaw co?\n");
    return 0;

}

int
odebranie(string str)
{
    object  *oblist;
    int ile, ktory;
    mixed wartosc;

    if (!str)
      {
        notify_fail("Odbierz co?\n");
            return 0;
      }
    if (!objectp(find_object(jakie_boxy)))
      {
        notify_fail("Zdaje sie, ze zaden wierzchowiec nie przebywa obecnie w
stajni.\n");
        return 0;
      }
    parse_command(str, find_object(jakie_boxy), "%l:" + PL_BIE, oblist);
    if (sizeof(oblist))
      {
        wartosc = oblist[0];
        wartosc = ABS(wartosc);
      }
    if (sizeof(oblist)) oblist =
FILTER_HORSES(all_inventory(find_object(jakie_boxy)));
    if (sizeof(oblist)) oblist = FILTER_H_OWNER(oblist,
this_player()->query_real_name());

        if (sizeof(oblist))
          {
           if (jest_stajenny)
           if (!present(stajenny_obj,this_object()))
           {
            notify_fail(tekst_nieobecnego + "\n");
            return 0;
           }
       if (jest_stajenny)
         if (!CAN_SEE(stajenny_obj,this_player()))
           {
            stajenny_obj->command("emote rozglada sie z wyrazem zagubienia na
twarzy");
            return 0;
           }
          }
        if
((!sizeof(oblist))&&(this_player()->query_prop("_zostawil_konia_w_"+nazwa_stajn
i)))
         {
                notify_fail("Niestety wyglada na to, ze twoj wierzchowiec
zostal skardziony, jednak nasza placowka "+
                            "nie zwraca zadnych kosztow z tym zwiazanych.\n");

                ile =
(this_player()->query_prop("_zostawil_konia_w_"+nazwa_stajni)) - 1;
            this_player()->add_prop("_zostawil_konia_w_"+nazwa_stajni, ile);

                return 0;
         }
        if (!(this_player()->query_prop("_zostawil_konia_w_"+nazwa_stajni)))
              {
               notify_fail("Przeciez nie
zostawial"+this_player()->koncowka("es","as")+" tutaj zadnego wierzchowca, "+
                           "lub wypadki losowe, jakie cie spotkaly spowodowaly
przejecie go przez ta placowke.\n");
           return 0;
          }

        if (!sizeof(oblist))
          {
           notify_fail("Odbierz co?\n");
           return 0;
          }
        if (sizeof(oblist)<wartosc)
          {
           notify_fail("Odbierz co?\n");
           return 0;
          }
        if (!wartosc)
          {
           notify_fail("Mozesz odebrac tylko jednego wierzchowca naraz.\n");
           return 0;
          }
        if (sizeof(oblist))
          {
        ktory = (sizeof(oblist))-wartosc;
        if (ktory<0)
          {
               notify_fail("Odbierz co?\n");
               return 0;
          }
        if (!jest_stajenny)
        {
            this_player()->catch_msg("Stajenny przyprowadza
twoj"+oblist[ktory]->koncowka("ego","a","e")+
              " "+oblist[ktory]->short(this_player(), PL_BIE)+".\n");
            saybb("Stajenny przyprowadza "+oblist[ktory]->short(this_player(),
PL_BIE)+" i "+
              "oddaje go "+QIMIE(this_player(), PL_CEL)+".\n");
        }
        else
        {
            this_player()->catch_msg(QCIMIE(stajenny_obj,PL_MIA)+"
przyprowadza twoj"+oblist[ktory]->koncowka("ego","a","e")+
              " "+oblist[ktory]->short(this_player(), PL_BIE)+".\n");
            saybb(QCIMIE(stajenny_obj,PL_MIA)+" przyprowadza
"+oblist[ktory]->short(this_player(), PL_BIE)+" i "+
              "oddaje "+oblist[ktory]->koncowka("go","ja","je")+"
"+QIMIE(this_player(), PL_CEL)+".\n");
        }
        ile = (this_player()->query_prop("_zostawil_konia_w_"+nazwa_stajni)) -
1;
        this_player()->add_prop("_zostawil_konia_w_"+nazwa_stajni, ile);
        oblist[ktory]->move(this_object());
        return 1;
      }
        notify_fail("Odbierz co?\n");
        return 0;
}



