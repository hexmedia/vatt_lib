/**
 * \file /lib/craftsman.c
 *
 * Plik ze wsparciem dla r�nego rodzaju wykonawc�w przedmiot�w.
 *
 * Przyk�ad u�ycia:
 *
 * inherit "/std/humanoid.c";
 * inherit "/lib/craftsman.c";
 *
 * #include <pattern.h>
 *
 * public void
 * create_humanoid()
 * {
 *   // konfigurujemy rzemie�lnika
 *   config_default_craftsman();
 *
 *   // ustawiamy dziedzin�, kt�r� zajmuje si� rzemie�lnik
 *   craftsman_set_domain(PATTERN_DOMAIN_TAILOR);
 *   // ustawiamy zbi�r wzorc�w, kt�re umie wykona� rzemie�lnik
 *   craftsman_set_sold_item_patterns(({ "/d/examples/crafting/pattern1.c" }));
 * }
 *
 * public void
 * init_living()
 * {
 *   ::init_living();
 *
 *   // inicjalizacja komend
 *   craftsman_init();
 * }
 *
 * !!!!!!UWAGA!!!!!!!!
 * Magazyny craftsman�w musz� by� roomami - w przeciwnym razie si� nie
 * zapami�taj� naprawy i zmiany rozmiar�w - po apoce nie b�dzie po nich
 * �ladu.
 *
 * TODO: - sprawdzenie dlaczego po odnowieniu nie jest w stanie powiedzie�
 *         jakie mam zam�wienia mimo, �e w tablicy nadal s�.
 */

#pragma strict_types

inherit "/lib/sklepikarz";
inherit "/std/act/asking";

#include <language.h>
#include <money.h>
#include <files.h>
#include <pl.h>
#include <const.h>
#include <pattern.h>
#include <cmdparse.h>
#include <stdproperties.h>
#include <macros.h>
#include <time.h>
#include <flags.h>
#include <filepath.h>
#include <composite.h>
#include <rozmiary.h>

#include <filter_funs.h>
#include "/config/login/login.h"

//do definicji slownika:
#include <files.h>

#define FREEZE_MATERIALS 1
/* okre�la ile co najwy�ej obiekt�w zostanie wy�wietlonych przy 'przejrzyj' */
#define MAXLIST 840

#define STATUS_IN_PROGRESS 0
#define STATUS_CONFIGURED  1
#define STATUS_COMPLETED   2

#define ORDER_STATUS           0
#define ORDER_OWNER            1
#define ORDER_ATTRS            2
#define ORDER_SELECTIONS       3
#define ORDER_FILE             4
#define ORDER_PATTERN          5
#define ORDER_TIME             6
#define ORDER_COMPLETION_TIME  7
#define ORDER_COST             8
#define ORDER_HOLD             9
#define ORDER_TIMEOUT          10
#ifdef INPUT_OBJ
#define ORDER_INPUT            11
#define ORDER_TYPE             12
#define ORDER_RS_NS            13
#define ORDER_SIZE             14
#else
#define ORDER_TYPE             11
#define ORDER_RS_NS            12
#define ORDER_SIZE             13
#endif

//Typy zam�wie�
#define ORDER_PURCHASE          0
#define ORDER_REPAIR            1
#define ORDER_RESIZE            2

#define PLAYER_O_LOG_OBJECT "_player_o_log_object"

#define ALLOW_CANCEL 0 /* Narazie nawet nie sko�czone wi�c nie pozwalamy */

static string *patterns_filenames = ({});

static object *sold_items = ({});
static object *used_patterns = ({});
static string *materials = ({});
static int need_expanding = 1;

static int domain = PATTERN_DOMAIN_NONE;

static int completion_alarm;
static int next_completion_time;
static int max_total_orders = 12;
static int max_user_orders = 6;//FIXME
static int selection_timeout = 120;
static float margin = 0.05;
static float prepaid = 0.1;
static int dont_want_materials = 0;
static int only_materials = 0;
static int allow_projects = 1;

mapping m_orders = ([]);

void craftsman_config_attributes(int id, int pos);
void craftsman_completed_config(int id);
void craftsman_completed_order(int id);
void craftsman_reset_completion_time();
void craftsman_clean_completed_orders();
int craftsman_calc_item_cost(object ob, int id);
int craftsman_config_validate_needed_materials(mixed order, int id);
int craftsman_config_validate_needed_tools(mixed order);
string craftsman_get_resize_name(int id);
public int craftsman_query_completion_time(int id);
public string craftsman_query_item_file(string order_name);
public int craftsman_new_order(string owner, int typ=ORDER_PURCHASE);
public void craftsman_configure_order(int id, object pattern);
public void craftsman_cancel_order(int id);
public int craftsman_query_time_to_complete(int id);
public int craftsman_query_hold_time(int id);
public varargs int craftsman_charge_for_item(object ob, int id, int pre = 0, int remove = 1);
public object craftsman_clone_item(string file, int id);
nomask void craftsman_configure_item(object item, mapping attrs, object pattern);
public int craftsman_present_order(int id);
public void craftsman_want_sizes(int id);
mixed query_store_room();
public nomask object find_repair_object(int id);
public nomask int order_is_resize(int id);
public nomask int order_is_repair(int id);
public nomask int order_is_purchase(int id);


int pomoc(string str);
public int pomoc2(string str);
int pomoc3(string str);


/**
 * taka pierd�ka - przy ka�dej komendzie, po kt�rej rzemie�lnik co�
 * do gracza m�wi pokazuje si� wcze�niej w�a�nie to, �eby inni gracze
 * co� tam wiedzieli o akcji gracza, a nie jak by�o wcze�niej, �e rzemie�lnik
 * ot tak, z nik�d co� do niego m�wi�.
 * Vera
 */
public void
hook_interactive()
{
    saybb(QCIMIE(TP,PL_MIA)+ " wymienia z "+QIMIE(TO,PL_NAR)+" par� s��w.\n");
}

/**
 * Funkcja pomocnicza - pozwala przybli�y� czas i rozszerza skr�ty

/**
 * Tekst przy braku odpowiedniej ilo�ci materia��w
 *
 * #param repair Czy rzecz jest naprawiana
 */
public varargs int
craftsman_not_enough_materials_hook(int repair = 0)
{
    notify_fail("");
    command("powiedz do " + OB_NAME(TP) +
            " Przykro mi, ale nie mam odpowiedniej ilo�ci materia��w, by " +
            (repair == 2 ? "zmieni� rozmiar tej rzeczy." :
            (repair == 1 ? "naprawi� dan�" :  "wykona� zam�wion�") + " rzecz."));
    return 0;
}

/**
 * Tekst kiedy nie mo�na anulowa� zam�wienia, poniewa� jest
 * to naprawa i ciuch jest rozpieprzony... Nie oddamy
 * przecie� graczowi porozpr�wanego ciucha - bo nikomu
 * si� go nie chce opisywa�.
 */
public varargs int
craftsman_cant_cancel_resize_hook(int id)
{
    notify_fail("");
    command("powiedz do " + OB_NAME(TP) +
        " Nie oddam " + (TP->query_gender() == G_MALE ? "panu" :
        (TP->query_gender() == G_FEMALE ? "pani" : "ci")) +
        "tego dop�ki nie sko�cz�. Przecie�, �eby zmieni� rozmiar " +
        "musia�em to na kawa�ki poci��.\n");
    return 0;
}

/**
 * Tekst przy braku odpowiednich materia��w
 *
 * @param repair Czy rzecz jest naprawiana
 */
public varargs void
craftsman_missing_tools_hook(int repair = 0)
{
    notify_fail("");
    command("powiedz do " + OB_NAME(TP) +
            " Przykro mi, ale nie mam odpowiednich narz�dzi, by " +
            (repair == 2 ? "zmieni� rozmiar tej rzeczy." :
            (repair == 1 ? "naprawi� dan�" :  "wykona� zam�wion�") + " rzecz."));
}

/**
 * Tekst przy braku odpowiednich umiej�tno�ci
 *
 * #param repair Czy rzecz jest naprawiana
 */
public varargs void
craftsman_not_enough_skills_hook(int repair = 0)
{
    notify_fail("");
    command("powiedz do " + OB_NAME(TP) +
            " Przykro mi, ale nie posiadam odpowiednich umiej�tno�ci, by " +
            (repair == 2 ? "zmieni� rozmiar tej rzeczy." :
            (repair == 1 ? "naprawi� dan�" :  "wykona� zam�wion�") + " rzecz."));
}

/**
 * Wywo�ana je�li nie wykonujemy tego co gracz chce przejrze�.
 *
 * @param     str - string kt�ry gracz wpisa�
 */
void
craftsman_purchase_no_match_hook(string str)
{
    notify_fail("");
    command("powiedz do " + OB_NAME(TP) +
            " Nie wykonuj� niczego takiego.");
}

/*
 * Function name: craftsman_purchase_syntax_failure_hook
 * Description:   Redefine this to change the message given when the user
 *                gives incorrect arguments to the "purchase" command
 * Arguments:     (string) the arguments given to the "purchase" command.
 * Returns:       1/0
 */
public int
craftsman_purchase_syntax_failure_hook(string str)
{
    notify_fail("");
    if (!stringp(str)) {
        command("powiedz do " + OB_NAME(TP) +
                " Musisz dok�adniej sprecyzowa�, o co ci chodzi. Najpierw przejrzyj moj� ofert�.");
    }
    else {
        command("powiedz do " + OB_NAME(TP) +
                " Nie wykonuj� takich rzeczy. Mo�e chcesz przejrze� moj� ofert�?");
    }
    return 0;
}

/**
 * Wywo�ana je�li gracz nie napisa�, co chce naprawi�.
 *
 * @param     str - string kt�ry gracz wpisa�
 */
public int
craftsman_repair_syntax_failure_hook(string str)
{
    notify_fail("");
    command("powiedz do " + OB_NAME(TP) +
            " Musisz dok�adnie sprecyzowa�, jak� rzecz mam dla ciebie naprawi�.");
    return 0;
}

/**
 * Wywo�ana je�li gracz nie opisa� dobrze przedmiotu do naprawienia.
 *
 * @param     str - string kt�ry gracz wpisa�
 */
public int
craftsman_repair_no_match_hook(string str)
{
    notify_fail("");
    command("powiedz do " + OB_NAME(TP) +
            " Musisz dok�adnie sprecyzowa�, jak� rzecz mam dla ciebie naprawi�.");
    return 0;
}

/**
 * Wywo�ana je�li tekst gracza pasuje do kilku przedmiot�w.
 *
 * @param     str - string kt�ry gracz wpisa�
 */
public int
craftsman_repair_too_much_items_hook(string str)
{
    notify_fail("");
    command("powiedz do " + OB_NAME(TP) +
            " Musisz mi podawa� przedmioty do naprawy pojedynczo.");
    return 0;
}

/**
 * Wywo�ana je�li rzemie�lnik nie jest w stanie przyj�� przedmiotu do naprawy.
 */
public void
craftsman_cannot_receive_hook()
{
    notify_fail("");
    command("powiedz do " + OB_NAME(TP) +
            " Nie mog� przyj�� tego przedmiotu.");
    return 0;
}

/**
 * Wywo�ywana je�li rzemie�lnik nic ju� nie mo�e naprawi� wi�cej.
 */
public void
craftsman_nothing_to_repair_hook()
{
    notify_fail("");
    command("powiedz do " + OB_NAME(TP) +
            " Nic wi�cej nie jestem w stanie zrobi� z tym przedmiotem.");
    return 0;
}

/*
 * Function name: craftsman_purchase_exceeded_max_total_orders_hook
 * Description:   Redefine this to change the message given when the user has
 *                exceeded the maximum number of orders allowed for all users.
 * Arguments:     (string) arguments to the order command (the item being
 *                         ordered)
 * Returns:       0/1
 */
public int
craftsman_purchase_exceeded_max_total_orders_hook(string str)
{
    command("powiedz do " + OB_NAME(TP) +
            " Niestety, mam ju� zbyt wiele z�o�onych zam�wie�.");
    return 1;
}

/*
 * Function name: craftsman_purchase_exceeded_max_user_orders_hook
 * Description:   Redefine this to change the message given when the user has
 *                exceeded the maximum number of orders allowed for a single
 *                user.
 * Arguments:     (string) arguments to the order command (the item being
 *                         ordered
 * Returns:       0/1
 */
public int
craftsman_purchase_exceeded_max_user_orders_hook(string str)
{
    command("powiedz do " + OB_NAME(TP) +
            " Niestety, nie mog� ju� przyj�� wi�cej zam�wie�. Przyjd� do mnie p�niej.");
    return 1;
}

/*
 * Function name: craftsman_purchase_hook
 * Description:   Redefine this to change the message given when a user tries
 *                to purchase an item.
 * Arguments:     (string) the name of the item purchased
 */
public void
craftsman_purchase_hook(string str)
{
//    write("How would you like your " + str + " made?\n");
}

/*
 * Function name: craftsman_config_prompt_attribute_hook
 * Description:   Redefine this to change the message given to prompt the user
 *                to enter a attribute value.
 * Arguments:     1. (string)   the attribute name
 *                2. (string *) an array of valid selections
 */
public void
craftsman_config_prompt_attribute_hook(string attr, string *valid_attrs)
{
    write(sprintf("\nProsz� wybra� " + attr +
        " spo�r�d nast�puj�cych (~q �eby przerwa�):\n%-#50s\n:",
        implode(valid_attrs, "\n")));
}

/*
 * Function name: craftsman_config_invalid_attribute_value_hook
 * Description:   Redefine this to change the message given when the user
 *                selects an invalid value for an attribute
 * Arguments:     (string) the attribute name
 *                (string) the value selected for the attribute
 */
public void
craftsman_config_invalid_attribute_value_hook(string attr, string value)
{
    write("Niew�a�ciwy wyb�r.\n");
}

/*
 * Function name: craftsman_config_validate_chosen_attribute_hook
 * Description:   Validate an attribute chosen and give any appropriate
 *                messages.
 * Arguments:     1. (string)   the name of the attribute
 *                2. (string)   the value chosen
 *                3. (string *) list of all the valid values for the attribute
 *                4. (int)      the order id
 * Returns:       1 - value is valid
 *                0 - value is invalid and should be reentered
 *               -1 - valud is invalid, and configuration should be exited
 */
public int
craftsman_config_validate_chosen_attribute_hook(string attr_nm, string value,
    string *attrs, int id)
{
    if (sizeof(attrs) && (member_array(value, attrs) < 0))
    {
        /* Invalid selection.  Prompt and ask again */
        craftsman_config_invalid_attribute_value_hook(attr_nm, value);
        return 0;
    }

    return 1;
}

/*
 * Function name: craftsman_config_validate_chosen_attributes_hook
 * Description:   Validate the attributes chosen and give any appropriate
 *                messages.  This differs from
 *                craftsman_config_validate_chosen_attribute_hook in that
 *                it is only called after all attributes have been set.
 * Arguments:     1. (mapping) the selected attributes
 *                2. (int)     the order id
 * Returns:       1 - values are valid
 *                0 - values are invalid and configuration should be exited
 */
public int
craftsman_config_validate_chosen_attributes_hook(mapping attrs, int id)
{
    return 1;
}

/**
 * Zwraca nazw� danego przedmiotu.
 *
 * @param id identyfikator zam�wienia
 */
public string
craftsman_desc_order_name(int id)
{
  mixed *order = m_orders[id];
  return clone_object(order[ORDER_FILE])->short();
}

/*
 * Function name: craftsman_desc_order
 * Description:   Redefine this to change how individual orders are presented
 * Arguments:     (int) the order id
 * Returns:       the formatted order description
 */
public string
craftsman_desc_order(int id)
{
    mixed *order = m_orders[id];
    string *attrs, str = "";
    int i;
    mapping attr_map = order[ORDER_SELECTIONS];

    attrs = m_indices(attr_map);
    for (i = 0; i < sizeof(attrs); i++)
    {
        /* Attribute names prepended with a '_' are considered to
         * be hidden.
         */
        if (attrs[i][0] == '_')
        {
            continue;
        }

        str += sprintf("%-13s %s\n", attrs[i] + ":",
            attr_map[attrs[i]]);
    }

    return str;
}

/*
 * Function name: craftsman_notify_completion_time_hook
 * Description:   Redefine this to change the message given to notify
 *                the user of the order's completion time
 * Arguments:     1. (int) the order's completion time
 *                2. (int) the order id
 */
public void
craftsman_notify_completion_time_hook(int t, int id)
{
    string str,czas;

    if (t < 60)
    {
        str = "mniej ni� minut�";
    }
    else
    {
		//poprawki by Vera. Uog�lnienie czasu.
		czas = CONVTIME_PL(t, PL_BIE);
		if(sizeof(explode(czas,"godziny")) > 1)
			czas = explode(czas,"godziny")[0] + "godziny";
		else if(sizeof(explode(czas,"godzin�")) > 1)
			czas = explode(czas,"godzin�")[0] + "godzin�";
		else if(sizeof(explode(czas,"godzin")) > 1)
			czas = explode(czas,"godzin")[0] + "godzin";
		else if(sizeof(explode(czas,"minuty")) > 1 ||
			sizeof(explode(czas,"minut")) > 1)
			czas = "godzin�";

        str = "oko�o " + czas;
    }

    command("powiedz do " + OB_NAME(TP) +
            " Zam�wienie b�dzie do odebrania za " + str + ".");
}

/*
 * Function name: craftsman_notify_cost_hook
 * Descriptions:  Redefine this to change the message given to notify the user
 *                of the cost of an item.
 * Arguments:     (int) the order id
 */
public void
craftsman_notify_cost_hook(int id)
{
  int cost;

  if (cost = craftsman_calc_item_cost(0, id))
  {
    if (prepaid != 0.0) {
      int pre, post;
      pre = ftoi(prepaid * itof(cost));
      post = cost - ftoi(prepaid * itof(cost));
      write("Pobrano zaliczk� w wysoko�ci " + MONEY_TEXT_SPLIT(pre, PL_DOP) + ".\n");
      write("Do zap�aty masz jeszcze " + MONEY_TEXT_SPLIT(post, PL_MIA) + ".\n");
    }
    else {
      write("To b�dzie kosztowa�o " + MONEY_TEXT_SPLIT(cost, PL_BIE) + ".\n");
    }
  }
}

/*
 * Function name: craftsman_notify_cancel_hook
 * Description:   Redefine this to change the message given when instructing
 *                the user how to cancel an order.
 * Arguments:     (int) the order id
 */
public void
craftsman_notify_cancel_hook(int id)
{
    write("Mo�esz 'zrezygnowa�' w ka�dej chwili.\n");
}

/*
 * Function name: craftsman_notify_check_hook
 * Description:   Redefine this to change the message given when instructing
 *                the user how to check an order
 * Arguments:     (int) the order id
 */
public void
craftsman_notify_check_hook(int id)
{
  write("Mo�esz w ka�dej chwili 'sprawdzi� zam�wienia', " +
      "�eby dowiedzie� si�, kt�re s� jeszcze w trakcie realizacji.\n" +
      "Odebra� mo�na tylko uko�czone zam�wienia.\n");
}

/*
 * Function name: craftsman_notify_hold_time_hook
 * Description:   Redefine this to change the message given to notify the user
 *                of the amount of time that the completed order will be held.
 * Arguments:     1. (int) the hold time in seconds
 *                2. (int) the order id
 */
public void
craftsman_notify_hold_time_hook(int hold, int id)
{
    if (hold > 0)
    {
        write("Twoje zam�wienie b�dzie do odebrania tylko " +
            "przez " + CONVTIME_PL(hold, PL_BIE) + " po jego realizacji.\n");
    }
}

/*
 * Function name: craftsman_config_completed_hook
 * Description:   Redefine this to change the message given when
 *                the user finishes selecting configuration options.
 * Arguments:     (int) the order id
 */
public void
craftsman_config_completed_hook(int id)
{
    mixed *order = m_orders[id];
    mapping attr_map = order[ORDER_SELECTIONS];

    if (order_is_repair(id)) { // repair
        write("Odda�" + this_player()->koncowka("e�", "a�", "o�") + " do naprawy nast�puj�c� rzecz:\n" +
                find_repair_object(id)->short() + "\n");
    }
    else if(order_is_resize(id)) // resize
    {
        write("Mam zmieni� rozmiar:\n" +
            find_repair_object(id)->short(PL_DOP) + "\n");
    }
    else // purchase
    {
        write("Dokona�" + this_player()->koncowka("e�", "a�", "o�") +
            " nast�puj�cego wyboru:\n" +
            sprintf("%s\n  %=-70s\n", craftsman_desc_order_name(id),
                craftsman_desc_order(id)));
    }

    if (pointerp(attr_map["__sizes"])) {
      if (sizeof(attr_map["__sizes"]) == 21) {
        string race = this_player()->query_race();
        int gender = this_player()->query_gender();
        float factor = attr_map["__sizes"][0] / RACEROZMOD[race][gender][0];
        write("Zdaje si�, �e nosisz ");
        if (factor < 0.6) {
          write("malutkie ubrania.\n");
        }
        else if (factor < 0.8) {
          write("bardzo ma�e ubrania.\n");
        }
        else if (factor < 0.95) {
          write("ma�e ubrania.\n");
        }
        else if (factor < 1.05) {
          write("�rednie ubrania.\n");
        }
        else if (factor < 1.2) {
          write("du�e ubrania.\n");
        }
        else if (factor < 1.4) {
          write("bardzo du�e ubrania.\n");
        }
        else {
          write("ogromne ubrania.\n");
        }
      }
    }

    //troch� randoma. vera.
    int cz = craftsman_query_time_to_complete(id);
    cz += random(cz/4);

    craftsman_notify_completion_time_hook(cz, id);
    craftsman_notify_cost_hook(id);
    craftsman_notify_check_hook(id);
//    craftsman_notify_cancel_hook(id);
    craftsman_notify_hold_time_hook(craftsman_query_hold_time(id), id);
}

/*
 * Function name: craftsman_abort_hook
 * Description:   Redefine this to change the message given when the user
 *                aborts from an input prompt.
 */
public void
craftsman_abort_hook()
{
    write("Przerwano.\n");
}

/*
 * Function name: craftsman_cancel_syntax_failure_hook
 * Description:   Redefine this to change the message given when incorrect
 *                arguments are given to the "cancel" command.
 * Arguments:     (string str) the arguments given to the "cancel" command.
 * Returns:       0/1
 */
public int
craftsman_cancel_syntax_failure_hook(string str)
{
    notify_fail("Zrezygnuj z zam�wienia?\n");
    return 0;
}

/**
 * Tekst wypisywany gdy rzemie�lnik pobiera zaliczk�.
 */
public void
craftsman_collecting_prepaid_hook()
{
  if (prepaid > 0.0) {
    write("  Pami�taj, �e za zam�wione przedmioty pobieram zaliczk�.\n");
  }
}

/**
 * Tekst wypisywany przy z�ej sk�adni odbierania przedmiotu.
 *
 * @param str argument do komendy 'odbierz'
 */
public int
craftsman_receive_syntax_failure_hook(string str)
{
  notify_fail("Odbierz zam�wienie?\n");
  return 0;
}

/**
 * Tekst wypisywany przy z�ej sk�adni sk�adania projektu.
 *
 * @param str argument do komendy 'z��'
 *
 * @return wynik dzia�ania komendy
 */
public int
craftsman_project_syntax_failure_hook(string str)
{
  notify_fail("Z�� projekt?\n");
  return 0;
}

/**
 * Tekst wypisywany, gdy sk�adanie projekt�w jest zabronione.
 *
 * @return wynik dzia�ania komendy
 */
public int
craftsman_project_disallowed_projects_hook()
{
  notify_fail("Przykro mi, ale obecnie nie przyjmuj� �adnych nowych projekt�w.\n");
  return 0;
}

/**
 * Tekst opisuj�cy sk�adanie projektu.
 */
public void
craftsman_project_notify_hook()
{
  write("Sk�adasz nowy projekt.\nProsz� dok�adnie opisa� rzecz, kt�r� mia�" +
      this_object()->koncowka("bym", "abym", "obym") + " zrobi�. Po jakim czasie " +
      this_object()->koncowka("m�g�bym", "mog�abym", "mog�obym") + " wykonywa� opisywan� rzecz " +
      "dla innych klient�w?\nA mo�e ca�a sprawa ma pozosta� tylko mi�dzy nami?\n");
}

/**
 * Tekst wypisywany, gdy gracz zrezygnowa� ze z�o�enia projektu.
 */
public void
craftsman_project_abort_hook()
{
  write("Decydujesz si� nie sk�ada� projektu.\n");
}

/**
 * Tekst wypisywany przy finalizacji sk�adania projektu.
 */
public void
craftsman_project_done_hook()
{
  write("Dzi�kuj� za z�o�enie projektu. Musz� si� teraz chwilk� nad tym zastanowi�.\n" +
      "W ci�gu kilku dni powiniene� otrzyma� list z moj� odpowiedzi�.\n");
}

/**
 * Tekst opisuj�cy skupywane materia�y.
 *
 * @param result wynik dzia�ania funkcji wy�wietlaj�cej materia�y
 *
 * @return wynik dzia�ania ca�ej komendy 'przejrzyj materia�y'
 */
public int
craftsman_list_needed_materials_hook(int result)
{
  if (pointerp(materials) && sizeof(materials)) {
    write((result ? "  " : "") + "Obecnie potrzebuj� takich materia��w jak " +
            COMPOSITE_WORDS(map(materials, &MATERIALY->nazwa_materialu())) + ".\n");
    return 1;
  }
  return 0;
}

/*
 * Function name: craftsman_cancel_unpurchased_item_hook
 * Description:   Redefine this to change the message given when a user who
 *                has purchased nothing tries to cancel an order.
 * Returns:       0/1
 */
public int
craftsman_cancel_unpurchased_item_hook()
{
  write("Nie zamawia�" + this_player()->koncowka("e�", "a�", "o�") +
      " niczego.\n");
  return 1;
}

/**
 * Tekst wypisywany gdy nie ma uko�czonych przedmiot�w.
 */
public int
craftsman_receive_incompleted_item_hook()
{
    TO->command("powiedz do " + OB_NAME(TP) + " Nie wykona�em jeszcze �adnych "+
		"zam�wionych przedmiot�w dla ciebie.");
  return 1;
}

/*
 * Function name: craftsman_cancel_order_hook
 * Description:   Redefine this to change the message given when an order is
 *                cancelled.
 * Arguments:     (int) the order id
 */
public void
craftsman_cancel_order_hook(int id)
{
    if(m_orders[id][ORDER_TYPE] == ORDER_REPAIR)
    {
        write("Rezygnujesz z naprawy i dostajesz spowrotem " +
            find_repair_object(id)->short(TP, PL_BIE) + ".\n");
    }
    else
        write("Rezygnujesz z zam�wienia.\n");
}

/*
 * Function name: craftsman_timeout_hook
 * Description:   Redefine this to change the message given when an order
 *                times out in the selection stage.
 * Arguments:     (int) the order id
 */
public void
craftsman_timeout_hook(int id)
{
    write("Decydujesz si� nie zamawia� niczego...\n");
}

/*
 * Function name: craftsman_list_orders_hook
 * Description:   Redefine this to change how order lists are presented.  It
 *                is important that the ordering of the orders is preserved.
 * Arguments:     (int *) the ids of the orders to list
 */
public void
craftsman_list_orders_hook(int *ids)
{
    int i;

    for (i = 0; i < sizeof(ids); i++)
    {
        if (order_is_repair(ids[i])) // repair
        {
            write(sprintf("%2d) NAPRAWA: %s\n", i + 1,
                find_repair_object(ids[i])->short()+""));//][ORDER_FILE]->short()));
        }
        else if(order_is_resize(ids[i])) // resize
        {
            write(sprintf("%2d) ZMIANA ROZMIARU: %s\n", i + 1,
                find_repair_object(ids[i])->short()+""));
        }
        else // purchase
        {
            write(sprintf("%2d) %s\n    %=-70s\n", i + 1,
                craftsman_desc_order_name(ids[i]), craftsman_desc_order(ids[i])));
        }
    }
}

public int
craftsman_query_object_present(object ob)
{
    return ((environment(ob) == this_object()) ||
        (environment(ob) == environment(this_object())));
}

/*
 * Function name: craftsman_completed_order_hook
 * Descriptions:  Redefine this to change the message a user
 *                receives when an order is completed.
 * Arguments:     1. (int)    the order id
 *                2. (string) the owner
 */
public void
craftsman_completed_order_hook(int id, string owner)
{
    object who;

    if ((who = find_player(owner)) && craftsman_query_object_present(who))
    {
        who->catch_tell("Twoje zam�wienie jest ju� gotowe.\n");
    }
}

/*
 * Function name: craftsman_cancel_menu_hook
 * Description:   Redefine this to change the message given to prompt the user
 *                to select an order to cancel.
 * Arguments:     (int *) the ids for the orders belonging to the user
 */
public void
craftsman_cancel_menu_hook(int *ids)
{
  craftsman_list_orders_hook(ids);
  write("Z kt�rego zam�wienia chcesz zrezygnowa� (1 - " + sizeof(ids) +
      " albo ~q �eby przerwa�): ");
}

/**
 * Tekst przy wy�wietlaniu wyboru, kt�re zam�wienie odebra�.
 *
 * @param ids tablica zam�wie�
 */
public void
craftsman_receive_menu_hook(int *ids)
{
  craftsman_list_orders_hook(ids);
  write("Kt�re zam�wienie chcesz odebra� (1 - " + sizeof(ids) +
      " albo ~q �eby przerwa�): ");
}

/*
 * Function name: craftsman_check_syntax_failure_hook
 * Description:   Redefine this to change the message given when the wrong
 *                arguments are given to the "check" command
 * Arguments:     (string) the argument given to the "check" command
 * Returns:       0/1
 */
public int
craftsman_check_syntax_failure_hook(string str)
{
    notify_fail("Sprawd� <zam�wienie/zam�wienia>?\n");
    return 0;
}

/*
 * Function name: craftsman_check_unpurchased_item_hook
 * Description:   Redefine this to change the message give when a user who has
 *                purchased nothing uses the "check" command.
 * Returns:       0/1
 */
public int
craftsman_check_unpurchased_item_hook()
{
  write("Nic nie zamawia�" + this_player()->koncowka("e�", "a�", "o�") +
      ".\n");
  return 1;
}

/*
 * Function name: craftsman_check_complete_order_hook
 * Description:   Redefine this to change the message given when an order is
 *                complete.
 * Arguments:     (int *) the ids of the user's completed orders
 */
public void
craftsman_check_complete_order_hook(int *ids)
{
  write("Twoje " + ((sizeof(ids) == 1) ? "zam�wienie jest" : "zam�wienia s�") +
      " ju� gotowe:\n");

  craftsman_list_orders_hook(ids);
}

/*
 * Function name: craftsman_check_incomplete_order_hook
 * Description:   Redefine this to change how unfinished orders are presented.
 * Arguments:     (int *) the ids of the user's incomplete orders
 */
public void
craftsman_check_incomplete_order_hook(int *ids)
{
  if (!sizeof(ids))
  {
    write("Nie masz innych zam�wie�.\n");
    return;
  }

  write("Jeszcze " + LANG_SNUM(sizeof(ids), PL_MIA, PL_NIJAKI_NOS) +
      " " + ilosc(sizeof(ids), "twoje zam�wienie jest",
        "twoje zam�wienia s�", "twoich zam�wie� jest") +
      " w trakcie realizacji:\n");

  craftsman_list_orders_hook(ids);
}

/*
 * Function name: craftsman_check_cannot_pay_hook
 * Description:   Redefine this to change the message given when
 *                the user cannot pay for an item.
 * Arguments:     (int)    the cost of the item in cc
 *                (object) the item itself
 *                (int)    the order id
 */
public void
craftsman_check_cannot_pay_hook(int cost, object ob, int id)
{
    write("Zdaje si�, �e nie sta� ci� na to.\n");
}

/*
 * Function name: check_move_player_failed_hook
 * Description:   Redefine this to change the message when the item could not
 *                be moved to the user.
 * Arguments:     (object) the item
 */
public void
craftsman_check_move_player_failed_hook(object ob)
{
    write("Nie dasz rady ud�wign�� " + ob->short(PL_DOP) + ".\n");
}

/*
 * Function name: craftsman_check_move_env_failed_hook
 * Description:   Redefine this to change the message when the item could not
 *                be moved to the user AND it could not be moved to the user's
 *                environment.
 * Arguments:     (object) the item
 */
public void
craftsman_check_move_env_failed_hook(object ob)
{
    write("Nie dasz rady ud�wign�� " + ob->short(PL_DOP) + ".\n");
}

/*
 * Function name: craftsman_check_receive_order_hook
 * Description:   Redefine this to change the message when the user receives
 *                his item.
 * Arguments:     1. (object) the item
 *                2. (int)    the order id
 */
public void
craftsman_check_receive_order_hook(object ob, int id)
{
    int cost = craftsman_calc_item_cost(ob, id);
    cost -= ftoi(prepaid * itof(cost));

    string money_text = MONEY_TEXT_SPLIT(cost, PL_BIE);

    int przec = 0;
    if(sizeof(explode(money_text, " i ")) > 1)
        przec = 1;

    write("P�acisz " + MONEY_TEXT_SPLIT(cost, PL_BIE) + (przec ? "," : "") +
        " i odbierasz " + ob->short(PL_BIE) + ".\n");
}

/**
 * Je�li rozmiar danego obiektu nie mo�e by� edytowany
 * wywo�ywana jest ta funkcja.
 *
 * @param
 */
public int
craftsman_resize_cant_be_resized_hook(object ob, string bezokol, mixed str, string why="")
{
    if(str == 1)
    {
        notify_fail("");
        TO->command("powiedz do " + OB_NAME(TP) + " Nie dam rady " +
            bezokol + " o wi�cej ni� " +
            LANG_SNUM(MAX_RESIZE(ob), PL_MIA, PL_MESKI_NOS_NZYW) + " rozmiary.");
    }
    else
    {
        notify_fail("");
        TO->command("powiedz do " + OB_NAME(TP) + " Nie dam rady " +
            "tego " + bezokol + ".");
    }
    return 0;
}

/**
 * Podstawowy b��d przy zmienianiu rozmiaru przedmiotu
 *
 * @param bezokol bezokolicznik
 * @param str tresc komendy
 */
public int
craftsman_resize_syntax_failure_hook(string bezokol, string str)
{
    notify_fail("");
    TO->command("powiedz do " + OB_NAME(TP) + " Nie rozumiem. " +
        "O co Ci chodzi.");
    return 0;
}

/**
 * �eby kto� 10 na raz nie oddawa�.
 */
public int
craftsman_resize_to_much_hook(string bezokol, string str)
{
    notify_fail("");
    TO->command("powiedz do " + OB_NAME(TP) + " Pojedynczo... Pojedyczno, " +
        "nie spami�tam wszystkiego na raz.");
    return 0;
}

/**
 * Pytanie o to czy w mierzeniu ma by� uwzgl�dniona odzie� za�o�ona
 * na posta�.
 */
public void
craftsman_resize_adjust_hook()
{
    write("Czy w mierzeniu mam uwzgl�dni� to co masz na sobie? (~q aby przerwa�)\n"+
        "[tak - uwzgl�dnij to co mam na sobie,\n" +
        "nie - nie bierz tego pod uwag�.]\n");
}

/**
 * Tego ubrania nie da si� tak bardzo powi�kszy� - ponad wszystkie rozmiary
 */
public int
craftsman_cant_resize_as_will_be_to_small()
{
    notify_fail("");
    TO->command("powiedz do " + OB_NAME(TP) + " A� tak bardzo nie dam rady "+
        "tego pomniejszy�.");
}

/**
 * Tegu ubrania nieda si� tak pomniejszy� - poni�ej wszystkich rozmiar�w
 */
public int
craftsman_cant_resize_as_will_be_to_big()
{
    notify_fail("");
    TO->command("powiedz do " + OB_NAME(TP) + " A� tak bardzo nie dam rady " +
        "tego powi�kszy�.");
}

/**
 * Rozmiar jest teraz odpowiedni - nie ma sensu dopasowywa�.
 */
public int
craftsman_size_is_good()
{
    notify_fail("");
    TO->command("powiedz do " + OB_NAME(TP) + " Przecie� to "+
        "jest ju� do Ciebie dopasowane.");
}

/**************************KONIEC HOOKOW************************/

/**
 * Description:   Get all orders matching a particular status or, optionally,
 *                all of a specified user's orders matching a particular
 *                status.
 * Arguments:     1. (int)    the status
 *                2. (string) optional user name
 * Returns:       An array of order ids which match the status (and user)
 */
public varargs int *
craftsman_query_orders_by_status(int ostatus, string name)
{
    int *ids;

    if (!name)
    {
        ids = m_indices(m_orders);
    }
    else
    {
        // Get the ids of the entries which match the owner name
        ids = m_indices(filter(m_orders,
            &operator(==)(name) @ &operator([])(, ORDER_OWNER)));
    }

    // Get the ids of the entries with match the order status
    return filter(ids, &operator(==)(ostatus) @
        &operator([])(, ORDER_STATUS) @ &operator([])(m_orders));
}

/*
 * Function name: craftsman_query_orders_in_progress
 * Description:   Get all orders in progress or all of a particular user's
 *                orders in progress
 * Arguments:     (string) optional user name
 * Returns:       An array of in-progress order ids
 */
public varargs int *
craftsman_query_orders_in_progress(string name)
{
    return craftsman_query_orders_by_status(STATUS_IN_PROGRESS, name);
}

/*
 * Function name: craftsman_query_orders_configured
 * Description:   Get all configured orders or all of a particular user's
 *                configured orders
 * Arguments:     (string) optional user name
 * Returns:       An array of configured order ids
 */
public varargs int *
craftsman_query_orders_configured(string name)
{
    return craftsman_query_orders_by_status(STATUS_CONFIGURED, name);
}

/*
 * Function name: craftsman_query_orders_completed
 * Description:   Get all completed orders or all of a particular user's
 *                completed orders
 * Arguments:     (string) optional user name
 * Returns:       An array of completed order ids
 */
public varargs int *
craftsman_query_orders_completed(string name)
{
    return craftsman_query_orders_by_status(STATUS_COMPLETED, name);
}

/*
 * Function name: craftsman_set_attribute
 * Description:   Called to set the desired value for each attribute,
 *                depending on the user's input.
 * Arguments:     1. (string)  the value specified for the current attribute
 *                2. (int)     the order id
 *                3. (int)     the attribute number of the current attribute
 * Returns:       1/0 - attribute set/not set
 */
public int
craftsman_set_attribute(string value, int id, int attr_number)
{
    mixed *order = m_orders[id];
    mixed attrs;
    string attr_nm;
    int valid;

    if (value == "~q")
    {
        craftsman_abort_hook();
        craftsman_cancel_order(id);
        return 0;
    }

    if (!sizeof(order))
    {
        return 0;
    }

    attr_nm = order[ORDER_ATTRS][attr_number][0];
    attrs = order[ORDER_ATTRS][attr_number][1];

    if (functionp(attrs))
    {
      attrs(id, attr_number, order[ORDER_SELECTIONS], value);
    }
    else {
      if (pointerp(attrs) && (sizeof(attrs) > 1))
      {
        /* Check if the input value is invalid for some other reason */
        valid = craftsman_config_validate_chosen_attribute_hook(attr_nm, value,
            attrs, id);

        if (valid < 0)
        {
          /* Value is invalid, and we should stop */
          return 0;
        }

        if (valid == 0)
        {
          /* Value is invalid, and we should try again */
          craftsman_config_attributes(id, attr_number);
          return 0;
        }
      }

      /* Add the attribute and value to the mapping */
      order[ORDER_SELECTIONS][attr_nm] = attrs[member_array(value, attrs)];
    }

    if (++attr_number < sizeof(order[ORDER_ATTRS]))
    {
        /* There are still attributes that are unset, so get the next */
        craftsman_config_attributes(id, attr_number);
        return 1;
    }

    /* There are no more attributes to be set, so finish up */
    craftsman_completed_config(id);
    return 1;
}

/*
 * Function name: craftsman_config_attributes
 * Description:   Prompt the user to input a value for an attribute.
 * Arguments:     1. (int) the order id
 *                2. (int) the attribute number of the current attribute
 */
public void
craftsman_config_attributes(int id, int attr_number)
{
  mixed *order = m_orders[id];

  if (attr_number < sizeof(order[ORDER_ATTRS])) {
    /* The first attribute in the array is the one we will set */
    string attr_nm = order[ORDER_ATTRS][attr_number][0];
    mixed attrs = order[ORDER_ATTRS][attr_number][1];

    if (functionp(attrs))
    {
      attrs = attrs(id, attr_number, order[ORDER_SELECTIONS]);
    }
    else {
      craftsman_config_prompt_attribute_hook(attr_nm, attrs);
    }

    /* catch the player's input and set the attribute */

    if (attrs) {
#ifdef INPUT_OBJ
      {
        object input_obj;

        setuid();
        seteuid(getuid());
        input_obj = clone_object(INPUT_OBJ);
        input_obj->get_input(&craftsman_set_attribute(, id, attr_number));

        order[ORDER_INPUT] = input_obj;
      }
#else
      input_to(&craftsman_set_attribute(, id, attr_number));
#endif
    }
  }
}

/*
 * Function name: craftsman_completed_config
 * Description:   Called when the user has finished selecting attributes, this
 *                function adds the item to the list of items being created.
 * Arguments:     (int) the order id
 * Returns:       1/0 - configuration successful/unsuccessful
 */
public int
craftsman_completed_config(int id)
{
    int t;
    object pattern, ob;
    mixed *order = m_orders[id];
    mapping attr_map = order[ORDER_SELECTIONS];

    order[ORDER_STATUS] = STATUS_CONFIGURED;

    if (order[ORDER_TIMEOUT])
    {
        remove_alarm(order[ORDER_TIMEOUT]);
        order[ORDER_TIMEOUT] = 0;
    }

    /* Validate the chosen values */
    if (!craftsman_config_validate_chosen_attributes_hook(attr_map, id))
    {
        craftsman_cancel_order(id);
        return 0;
    }

    /* Sprawdzenie potrzebnych materia��w */
    if (!craftsman_config_validate_needed_materials(order, id)) {
        craftsman_cancel_order(id);
        return 0;
    }

    /* Sprawdzenie potrzebnych narz�dzi */
    if (!craftsman_config_validate_needed_tools(order)) {
        craftsman_cancel_order(id);
        return 0;
    }

    /* Sprawdzanie potrzebnego skilla */
    pattern = clone_object(order[ORDER_PATTERN]);
    if (!(CRAFTING_LIBRARY->check_skills(this_object(), pattern))) {
        craftsman_not_enough_skills_hook();
        craftsman_cancel_order(id);
        return 0;
    }

    /* Zu�ycie materia��w */
    ob = craftsman_clone_item(order[ORDER_FILE], id);
    craftsman_configure_item(ob, order[ORDER_SELECTIONS], pattern);
#ifndef FREEZE_MATERIALS
    CRAFTING_LIBRARY->use_materials(ob, all_inventory(query_store_room()));
#endif

    /* Pobranie zaliczki */
    if (!craftsman_charge_for_item(0, id, 1)) {
      craftsman_cancel_order(id);
      return 0;
    }

    craftsman_config_completed_hook(id);

    t = order[ORDER_COMPLETION_TIME] = craftsman_query_completion_time(id);

    /* Start creation */

    if (!next_completion_time || (t < next_completion_time))
    {
        if (completion_alarm)
        {
            remove_alarm(completion_alarm);
        }

        next_completion_time = t;
        completion_alarm = set_alarm(itof(t - time()), 0.0,
            &craftsman_completed_order(id));
    }

    /* This is a good point to clean up old completed orders */
    craftsman_clean_completed_orders();

    return 1;
}

/*
 * Function name: craftsman_calc_item_cost
 * Descriptions:  Give the cost in cc for an item.  By default, this simply
 *                returns the value given in set_item_cost(), but it can be
 *                redefined to calculate the cost for an item dynamically.
 * Arguments:     1. (object) the item itself.  If the item has not been
 *                            completed this will be 0.
 *                2. (int)    the order id
 * Returns:       (int) the cost of the item in copper
 */
public int
craftsman_calc_item_cost(object ob, int id)
{
    return m_orders[id][ORDER_COST];
}

/*
 * Function name: craftsman_completed_order
 * Description:   Called when creation of an item is complete, this function
 *                adds the item to the list of completed items and starts
 *                creation of the next item.
 * Arguments:     (int) the order id
 */
public void
craftsman_completed_order(int id)
{
    mixed *order = m_orders[id];

    order[ORDER_STATUS] = STATUS_COMPLETED;

    craftsman_completed_order_hook(id, order[ORDER_OWNER]);

    /* Start the alarm for the next item */
    craftsman_reset_completion_time();
}

/*
 * Function name: craftsman_timeout_selection
 * Description:   Cause the attribute selection process to time out.  The
 *                user will have to start over.
 * Arguments:     (int) the order id
 */
public void
craftsman_timeout_selection(int id)
{
    mixed *order = m_orders[id];

    if (sizeof(order) && (order[ORDER_STATUS] == STATUS_IN_PROGRESS))
    {
        craftsman_cancel_order(id);
        craftsman_timeout_hook(id);
    }
}

/**
 * Uaktualnia zbiory wzorc�w i sprzedawanych rzeczy, je�eli jest
 * taka potrzeba.
 *
 * Ponadto uaktualnia zbi�r skupowanych materia��w.
 */
public void
expand_patterns_items()
{
    if (!need_expanding)
        return;

    sold_items = ({});
    used_patterns = ({});
    materials = ({});

    if (domain != PATTERN_DOMAIN_NONE)
    {
        foreach (string pattern : patterns_filenames)
        {
// dump_array(pattern);
            object pattern_obj = clone_object(pattern);
            if (!objectp(pattern_obj))
                continue;

            if (pattern_obj->query_pattern_domain() != domain)
                continue;

            object item_obj = clone_object(pattern_obj->query_item_filename());

            if (!objectp(item_obj))
            {
                continue;
            }

            sold_items += ({ item_obj });
            used_patterns += ({ pattern_obj });

            foreach (string material : pattern_obj->query_possible_materials())
            {
                if (member_array(material, materials) == -1)
                    materials += ({ material });
            }
        }
    }

    need_expanding = 0;
}

/**
 * Wymusza uaktualnienie wzorc�w i sprzedawanych rzeczy w danym
 * momencie.
 */
public void
force_expand_patterns_items()
{
    need_expanding = 1;
    expand_patterns_items();
}

/**
 * Funkcja odpowiedzialna za przetworzenie zg�oszonego projektu.
 *
 * @param str tekst projektu
 */
public void
done_editing_project(string str)
{
  string text;

  if (!strlen(str))
  {
    craftsman_project_abort_hook();
    return;
  }
  craftsman_project_done_hook();

  text = extract(ctime(time(), 1), 3, -11);

  if (extract(text, 0, 0) == " ")
    text = extract(text, 1);

  PROJLOG_OBJECT->post_msg(text, 7, file_name(this_object()), capitalize(this_player()->query_real_name()), str);

  /* info dla wiz�w online wzi�te od verka */
  object *us = filter(users(), &->query_wiz_level());
  int il = -1;
  int size = sizeof(us);
  object env;

  while(++il < size)
  {
    if (query_ip_number(us[il]) &&
        !(us[il]->query_prop(WIZARD_I_BUSY_LEVEL) & BUSY_C))
    {
      tell_object(us[il], "PROJEKT: " + this_player()->query_name(PL_MIA) +
          " z�o�y� projekt u " + this_object()->short(this_player(), PL_DOP)+
          " ("+RPATH(file_name(this_object())) +").\n");
    }
  }

  this_player()->remove_prop(PLAYER_O_LOG_OBJECT);
}

/**
 * Funkcja odpowiedzialna za obs�ug� sk�adania projekt�w.
 *
 * @param str argumenty do komendy 'z��'
 */
public int
craftsman_project(string str)
{


  if (str != "projekt") {
    return craftsman_project_syntax_failure_hook(str);
  }

  hook_interactive();

  if (!allow_projects) {
    return craftsman_project_disallowed_projects_hook();
  }

	if(TP->query_age() < 86400)
    {
		write("Niestety, osoby o wieku mniejszym ni� 2 dni nie mog� " +
	    "sk�ada� specjalnych zam�wie�. Obejrzyj wpierw troch� " +
	    "�wiata.\n");
		return 1;
    }

  craftsman_project_notify_hook();

  this_player()->add_prop(PLAYER_O_LOG_OBJECT, this_object());

  setuid();
  seteuid(getuid());

  clone_object(EDITOR_OBJECT)->edit("done_editing_project", "");
  return 1;
}

/*
 * Function name: craftsman_purchase
 * Description:   the "purchase" command.  Starts off item configuration.
 * Arguments:     (string) argument string to the "purchase" command
 * Returns:       1/0 - success/failure
 */
public int
craftsman_purchase(string str)
{
  int num_orders, id;
  string name;
  object item_to_sold;
  object used_pattern;

  expand_patterns_items();

  hook_interactive();

  if (!stringp(str)) {
    return craftsman_purchase_syntax_failure_hook(str);
  }

  if (!parse_command(str, sold_items, "%o:" + PL_BIE, item_to_sold)) {
    return craftsman_purchase_syntax_failure_hook(str);
  }
  used_pattern = used_patterns[member_array(item_to_sold, sold_items)];

  num_orders = sizeof(craftsman_query_orders_in_progress()) +
    sizeof(craftsman_query_orders_configured());

  if ((max_total_orders >= 0) && (num_orders >= max_total_orders))
  {
    return craftsman_purchase_exceeded_max_total_orders_hook(str);
  }

  name = this_player()->query_real_name();

  num_orders = sizeof(craftsman_query_orders_in_progress(name)) +
    sizeof(craftsman_query_orders_configured(name));

  if ((max_user_orders >= 0) && (num_orders >= max_user_orders))
  {
    return craftsman_purchase_exceeded_max_user_orders_hook(str);
  }

  id = craftsman_new_order(name);

  craftsman_purchase_hook(str);

  craftsman_configure_order(id, used_pattern);

  if (selection_timeout > 0)
  {
    m_orders[id][ORDER_TIMEOUT] = set_alarm(itof(selection_timeout), 0.0,
        &craftsman_timeout_selection(id));
  }

  if (sizeof(m_orders[id][ORDER_ATTRS])) {
    craftsman_config_attributes(id, 0);
  }
  else {
    craftsman_completed_config(id);
  }

  return 1;
}

/*
 * Function name: craftsman_repair
 * Description:   the "repair" command.  Starts off item repair.
 * Arguments:     (string) argument string to the "repair" command
 * Returns:       1/0 - success/failure
 */
public int
craftsman_repair(string str)
{
    object *item;
    int value, num_orders, id, t, toRepair;
    string name;
    object ob;

    hook_interactive();

    if (!stringp(str)) {
        return craftsman_repair_syntax_failure_hook(str);
    }

    item = FIND_STR_IN_OBJECT(str, this_player(), PL_BIE);
    if (!sizeof(item)) {
        return craftsman_repair_no_match_hook(str);
    }

    if (sizeof(item) > 1) {
        return craftsman_repair_too_much_items_hook(str);
    }

    ob = item[0];

    num_orders = sizeof(craftsman_query_orders_in_progress()) +
        sizeof(craftsman_query_orders_configured());

    if ((max_total_orders >= 0) && (num_orders >= max_total_orders))
    {
        return craftsman_purchase_exceeded_max_total_orders_hook(str);
    }

    name = this_player()->query_real_name();

    num_orders = sizeof(craftsman_query_orders_in_progress(name)) +
        sizeof(craftsman_query_orders_configured(name));

    if ((max_user_orders >= 0) && (num_orders >= max_user_orders))
    {
        return craftsman_purchase_exceeded_max_user_orders_hook(str);
    }

    id = craftsman_new_order(name, ORDER_REPAIR);

    mixed *order = m_orders[id];
    mapping attr_map = order[ORDER_SELECTIONS];

    order[ORDER_STATUS] = STATUS_CONFIGURED;

    toRepair = CRAFTING_LIBRARY->estimate_repair_effort(ob);

    /* Sprawdzanie, czy mamy co� do naprawienia */
    if (!toRepair) {
        craftsman_nothing_to_repair_hook();
        craftsman_cancel_order(id);
        return 0;
    }

#ifndef FREEZE_MATERIALS
    /* Sprawdzenie potrzebnych materia��w */
    if (!(CRAFTING_LIBRARY->check_materials(ob, all_inventory(query_store_room()),
                    ob->query_condition() - ob->query_repair()))) {
        craftsman_not_enough_materials_hook(1);
        craftsman_cancel_order(id);
        return 0;
    }
#endif

    /* Sprawdzanie potrzebnego skilla */
    if (!(CRAFTING_LIBRARY->check_skills_r(this_object(), ob))) {
        craftsman_not_enough_skills_hook(1);
        craftsman_cancel_order(id);
        return 0;
    }

    /* Sprawdzenie potrzebnych narz�dzi */
    if (!(CRAFTING_LIBRARY->check_tools_r(TO, ob, all_inventory(TO)))) {
        craftsman_missing_tools_hook(1);
        craftsman_cancel_order(id);
        return 0;
    }

    order[ORDER_COST] = toRepair * 20;
    order[ORDER_TIME] = toRepair * 300;
    order[ORDER_FILE] = MASTER_OB(ob);//file_name(ob);

    /* Sprawdzamy, czy da si� przemie�ci� obiekt w og�le... */
    if (ob->move(query_store_room(), 0, 0, 1)) {
        craftsman_cannot_receive_hook();
        craftsman_cancel_order(id);
        return 0;
    }

    /* Pobranie zaliczki */
    if (!craftsman_charge_for_item(0, id, 1, 0)) {
      craftsman_cancel_order(id);
      return 0;
    }

    /* Przemieszczamy obiekt */
    if (ob->move(query_store_room())) {
        craftsman_cannot_receive_hook();
        craftsman_cancel_order(id);
        return 0;
    }

    ob->add_prop(OBJ_I_IN_REPAIR_BY_CRAFTSMAN, id);

    /* Zu�ycie materia��w */
#ifndef FREEZE_MATERIALS
    CRAFTING_LIBRARY->use_materials(ob, all_inventory(query_store_room()));
#endif

    craftsman_config_completed_hook(id);

    t = order[ORDER_COMPLETION_TIME] = craftsman_query_completion_time(id);

    /* Start repairing */

    if (!next_completion_time || (t < next_completion_time))
    {
        if (completion_alarm)
        {
            remove_alarm(completion_alarm);
        }

        next_completion_time = t;
        completion_alarm = set_alarm(itof(t - time()), 0.0,
            &craftsman_completed_order(id));
    }

    /* This is a good point to clean up old completed orders */
    craftsman_clean_completed_orders();

    return 1;
}

/*
 * Function name: reset_completion_time
 * Description:   Start a new alarm to signal the completion of the next item.
 */
public void
craftsman_reset_completion_time()
{
  int *ids, id, i;
  mixed *order;

  if (get_alarm(completion_alarm))
  {
    remove_alarm(completion_alarm);
  }

  next_completion_time = 0;

  ids = m_indices(m_orders);
  for (i = 0; i < sizeof(ids); i++)
  {
    order = m_orders[ids[i]];

    if (order[ORDER_STATUS] != STATUS_CONFIGURED)
    {
      continue;
    }

    if (!next_completion_time ||
        (order[ORDER_COMPLETION_TIME] < next_completion_time))
    {
      next_completion_time = order[ORDER_COMPLETION_TIME];
      id = ids[i];
    }
  }

  if (next_completion_time)
  {
      if (next_completion_time < time()) {
    completion_alarm = set_alarm(1.0, 0.0, &craftsman_completed_order(id));
      }
      else {
    completion_alarm = set_alarm(itof(next_completion_time - time()),
        0.0, &craftsman_completed_order(id));
      }
  }
  else {
    completion_alarm = set_alarm(15.0, 0.0, &craftsman_reset_completion_time());
  }

  craftsman_clean_completed_orders();
}

/**
 * TODO:
 * - oddanie przedmiotu je�li by� w naprawie lub zmienino jego rozmiar
 * - mo�liwo�� oddania zniszczonego przedmiotu je�li zmieniano jego rozmiar
 *   Albo komunikat, �e nie mo�na odda� poniewa� w�a�nie nad nim craftsman
 *   pracuje.
 *
 * Dzi�ki tej funkcji mo�emy anulowa� zam�wienie
 *
 * @param id Id zam�wienia.
 */
public void
craftsman_cancel_order(int id)
{
    if(!is_mapping_index(id, m_orders))
        return ;

    mixed *order = m_orders[id];

    m_orders = m_delete(m_orders, id);

    if(order[ORDER_TYPE] == ORDER_REPAIR)
    {
        object pl;
        if(present(pl=find_player(order[ORDER_OWNER])))
        {
            //Sprawdzamy czy to npc czy room.
            if(TO->query_npc() || interactive(TO))
                command("daj " + OB_NAME(TO) + " " + OB_NAME(find_repair_object(id)));
            else
                int move = find_repair_object(id)->move(TP);
        }
    }
    else
        find_repair_object(id)->remove_object(); //Nie ma w�a�ciciela - usuwamy obiekt.

    if (order[ORDER_TIMEOUT])
    {
        remove_alarm(order[ORDER_TIMEOUT]);
    }

#ifdef INPUT_OBJ
    if (order[ORDER_INPUT])
    {
        order[ORDER_INPUT]->remove_object();
    }
#endif

    craftsman_reset_completion_time();
}

/*
 * Function name: cancel_order_input
 * Description:   Accept user input and cancel the specified order
 * Arguments:     (string) the order number (position) to cancel
 * Returns:       1/0 - order cancelled/not cancelled
 */
public int
cancel_order_input(string str)
{
    int i, *damids;
    string name;

    if (str == "~q")
    {
        craftsman_abort_hook();
        return 0;
    }

    name = this_player()->query_real_name();
    int *ids = sort_array(craftsman_query_orders_configured(name));

    if (!sscanf(str, "%d", i) || (i < 1) || (i > sizeof(ids)))
    {
        craftsman_cancel_menu_hook(ids);
        input_to(cancel_order_input);
        return 0;
    }

    if(order_is_resize(ids[i-1]))
        return craftsman_cant_cancel_resize_hook(ids[0]);

    craftsman_cancel_order_hook(ids[i - 1]);
    craftsman_cancel_order(ids[i - 1]);

    return 1;
}

/**
 * Funkcja wy�wietlaj�ca menu dla wyboru zam�wienia do odebrania.
 *
 * @param str wpisany przez u�ytkownika tekst
 */
public int
receive_order_input(string str)
{
    int i, *ids;
    string name;

    if (str == "~q")
    {
        craftsman_abort_hook();
        return 0;
    }

    name = this_player()->query_real_name();
    ids = sort_array(craftsman_query_orders_completed(name));

    if (!sscanf(str, "%d", i) || (i < 1) || (i > sizeof(ids)))
    {
        craftsman_receive_menu_hook(ids);
        input_to(receive_order_input);
        return 0;
    }

    if (craftsman_present_order(ids[i-1]))
    {
      craftsman_cancel_order(ids[i-1]);
    }

    return 1;
}

/*
 * Function name: craftsman_cancel
 * Description:   The "cancel" command.  This function cancels the user's
 *                order or prompts the user to select an order to cancil if
 *                he has more than one being made.
 * Arguments:     (string) the arguments to the "cancel" command
 * Returns:       1/0 - success/failure
 */
public int
craftsman_cancel(string str)
{
    int *ids;
    string name;

    if (!(str ~= "z zam�wienia"))
    {
        return craftsman_cancel_syntax_failure_hook(str);
    }

      name = this_player()->query_real_name();

    if (!sizeof(ids = sort_array(craftsman_query_orders_configured(name))))
    {
    return craftsman_cancel_unpurchased_item_hook();
    }

    if (sizeof(ids) == 1)
    {
        if(order_is_resize(ids[0]))
           return craftsman_cant_cancel_resize_hook(ids[0]);

        craftsman_cancel_order(ids[0]);
        craftsman_cancel_order_hook(ids[0]);
        return 1;
    }

    craftsman_cancel_menu_hook(ids);
    input_to(cancel_order_input);
    return 1;
}

/**
 * Ta funkcja pozwala sprawdzi� czy gracz b�dzie robi� ubranie na ubranie
 * czy na siebie.
 */
public int
craftsman_resize_input(object ob, int id, string bezokol, string or, string nr)
{
    int ile;
    string dop;

    if(lower_case(nr) == "~q")
        return 0;
    else if(lower_case(nr) == "tak" || lower_case(nr) ~= "oczywi�cie")
    {
        ile = ABS(member_array(TP->query_size(ob->pobierz_rozmiary(), 1), ROZMIARY) -
                member_array(or, ROZMIARY));/*
write("TP->query_size(ob->pobierz_rozmiary(), 1) = "+TP->query_size(ob->pobierz_rozmiary(), 1)+"\n");
write("or (rozmiar obiektu, wg mnie) = "+or+"\n");
write("ile: "+ile+"\n");*/
        //dopasowywujemy do gracza z ciuchami
        if(ile > MAX_RESIZE(ob))
        {
            craftsman_cancel_order(id);
            return craftsman_resize_cant_be_resized_hook(ob, bezokol, 1);
        }
        nr = "Q";
        m_orders[id][ORDER_RS_NS] = "Q";
    }
    else if(lower_case(nr) == "nie")
    {/*
write("TP->query_size(ob->pobierz_rozmiary(), 0) = "+TP->query_size(ob->pobierz_rozmiary(), 0)+"\n");
write("or (rozmiar obiektu, wg mnie) = "+or+"\n");*/
        ile = ABS(member_array(TP->query_size(ob->pobierz_rozmiary(), 0), ROZMIARY) -
            member_array(or, ROZMIARY));

// write("ile: "+ile+"\n");
        //dopasowywujemy do gracza bez ciuch�w
        if(ile > MAX_RESIZE(ob))
        {
            craftsman_cancel_order(id);
            return craftsman_resize_cant_be_resized_hook(ob, bezokol, 1);
        }
        nr = "D";
        m_orders[id][ORDER_RS_NS] = "D";
    }
    else if(member_array(nr, ROZMIARY) != -1)
    {
        ile = ABS(member_array(nr, ROZMIARY) - member_array(or, ROZMIARY));
        m_orders[id][ORDER_RS_NS] = nr;
    }
    else
        return 0;

    if((nr == "D" || nr == "Q") && CRAFTING_LIBRARY->size_is_good(ob, TP, (nr=="Q")))
    {
        craftsman_cancel_order(id);
        return craftsman_size_is_good();
    }

    if(!CRAFTING_LIBRARY->can_be_resized(ob, ob->query_orginal_size(), nr))
    {
// write("dupa 1\n");
        craftsman_cancel_order(id);
        return craftsman_resize_cant_be_resized_hook(ob, bezokol, 1);
    }

#if FREEZE_MATERIALS
    //Sprawdzamy czy mamy wystarczaj�c� ilo�� materia��w.
    if(!(CRAFTING_LIBRARY->check_materials_repair(ob,
        all_inventory(query_store_room()), nr)))
    {
        craftsman_cancel_order(id);
        return craftsman_not_enough_materials_hook(2);
    }

    //zabieramy materia�y
    CRAFTING_LIBRARY->use_materials(ob, all_inventory(query_store_room()), 1);
#endif

    int t = ftoi(itof(max(1, ABS(ile))) * (frandom(0.5, 3) + 1.0) * 2000.0);

    m_orders[id][ORDER_STATUS] = 1;
    m_orders[id][ORDER_TIME] = t;
    m_orders[id][ORDER_FILE] = MASTER_OB(ob);
    m_orders[id][ORDER_COST] = t / 75;

    /* Pobranie zaliczki */
    if (!craftsman_charge_for_item(0, id, 1, 0)) {
        craftsman_cancel_order(id);
        return 0;
    }

    /* Przemieszczamy obiekt */
    if (ob->move(query_store_room())) {
        craftsman_cannot_receive_hook();
        craftsman_cancel_order(id);
        return 0;
    }

    ob->add_prop(OBJ_I_IN_REPAIR_BY_CRAFTSMAN, id);

    craftsman_config_completed_hook(id);

    t = m_orders[id][ORDER_COMPLETION_TIME] = craftsman_query_completion_time(id);

    /* Start repairing */

    if (!next_completion_time || (t < next_completion_time))
    {
        if (completion_alarm)
        {
            remove_alarm(completion_alarm);
        }

        next_completion_time = t;
        completion_alarm = set_alarm(itof(t - time()), 0.0,
                                     &craftsman_completed_order(id));
    }

    /* This is a good point to clean up old completed orders */
    craftsman_clean_completed_orders();

    return 1;
}

/**
 * Funkcja dopowiedzialna za zmiany rozmiaru sprz�du
 *
 * @param str argument do komeny 'dopasuj', 'zmniejsz', 'pomniejsz', 'zwi�ksz' lub 'powi�ksz'
 */
public int
craftsman_resize(string str)
{
    int num_orders;
    string bezokol, verb, name;
    object *what;
    mixed ile;

    hook_interactive();

    switch(verb = query_verb())
    {
        case "dopasuj":     bezokol = "dopasowa�";      break;
        case "zmniejsz":    bezokol = "zmniejszy�";     break;
        case "pomniejsz":   bezokol = "pomniejszy�";    break;
        case "zwi�ksz":     bezokol = "zwi�kszy�";      break;
        case "powi�kszy�":  bezokol = "powi�kszy�";     break;
    }

    if(!str)
        return craftsman_resize_syntax_failure_hook(bezokol, str);

    if(verb == "dopasuj")
    {
        if(!parse_command(str, all_inventory(TP), "%i:" + PL_BIE, what))
        {
            return craftsman_resize_syntax_failure_hook(bezokol, str);
        }
    }
    else
    {
        if(!parse_command(str, all_inventory(TP), "%i:" + PL_BIE +
            " o %s", what, ile))// 'rozmiary' / 'rozmiar�w'", what, ile))
        {
            if(!parse_command(str, all_inventory(TP), "%i:" + PL_BIE, what))
                return craftsman_resize_syntax_failure_hook(bezokol, str);
            else if(verb ~= "powi�ksz" || verb ~= "zwi�ksz")
                ile = 1;
            else
                ile = -1;
        }

        if(!ile && stringp(ile))
            ile = LANG_NUMS(ile);
        else if(!intp(ile))
            ile = (int)ile;

        if(!ile)
            return craftsman_resize_syntax_failure_hook(bezokol, str);
    }

    if(!what || !sizeof(what))
        return craftsman_resize_syntax_failure_hook(bezokol, str);

    what = NORMAL_ACCESS(what, 0, 0);

    if(!sizeof(what))
        return craftsman_resize_syntax_failure_hook(bezokol, str);

    if(sizeof(what) > 1)
        return craftsman_resize_to_much_hook(bezokol, str);

    //Sprawdzamy czy mo�emy o tyle powi�kszy�/pomniejszy� obiekt
    //czy przypadkiem ju� nie jest odpowiedniej wielko�ci:
    if(sizeof(ROZMIARY) <= ile + member_array(what[0]->query_n_size(), ROZMIARY) &&
       (verb ~= "powi�ksz" || verb ~= "zwi�ksz"))
    {
        return craftsman_cant_resize_as_will_be_to_big();
    }

    if(ile + member_array(what[0]->query_n_size(), ROZMIARY) < 0 &&
       (verb ~= "pomniejsz" || verb ~= "zmniejsz"))
    {
        return craftsman_cant_resize_as_will_be_to_small();
    }
    num_orders = sizeof(craftsman_query_orders_in_progress()) +
            sizeof(craftsman_query_orders_configured());

    if ((max_total_orders >= 0) && (num_orders >= max_total_orders))
        return craftsman_purchase_exceeded_max_total_orders_hook(str);

    name = this_player()->query_real_name();

    num_orders = sizeof(craftsman_query_orders_in_progress(name)) +
            sizeof(craftsman_query_orders_configured(name));

    if ((max_user_orders >= 0) && (num_orders >= max_user_orders))
        return craftsman_purchase_exceeded_max_user_orders_hook(str);

    string a;
    if(a = what[0]->query_prop(ARMOUR_I_CANT_BE_RESIZED))
    {
// write("dupa 2\n");
        return craftsman_resize_cant_be_resized_hook(what[0], bezokol, str, a);}

    int id = craftsman_new_order(TP->query_real_name(), ORDER_RESIZE);

    /* Sprawdzanie potrzebnego skilla */
    if (!(CRAFTING_LIBRARY->check_skills_r(this_object(), what[0]))) {
        craftsman_not_enough_skills_hook(2);
        craftsman_cancel_order(id);
        return 0;
    }

    /* Sprawdzenie potrzebnych narz�dzi */
    if (!(CRAFTING_LIBRARY->check_tools_r(TO, what[0], all_inventory(TO)))) {
        craftsman_missing_tools_hook(2);
        craftsman_cancel_order(id);
        return 0;
    }

    /* Sprawdzamy, czy da si� przemie�ci� obiekt w og�le... */
    if (what[0]->move(query_store_room(), 0, 0, 1)) {
        craftsman_cannot_receive_hook();
        craftsman_cancel_order(id);
        return 0;
    }

    string ar = what[0]->query_n_size();
    string nr;
    if(verb != "dopasuj")
    {
        int iar, inr;

        if((iar=member_array(ar, ROZMIARY)) == -1)
        {
            craftsman_cancel_order(id);
            return craftsman_resize_syntax_failure_hook(bezokol, str);
        }
// write("iar = "+iar+"\n");
        inr = iar + ile;
// write("inr = "+iar+"\n");
        nr = ROZMIARY[inr];
// write("nr = "+iar+"\n");
        return craftsman_resize_input(what[0], id, bezokol, ar, nr);
    }
    else
    {
        craftsman_resize_adjust_hook();
        input_to(&craftsman_resize_input(what[0], id, bezokol, ar,));
    }

    return 1;
}

/**
 * Funkcja odpowiedzialna za obs�ug� odbierania zam�wienia.
 *
 * @param str argument do komendy 'odbierz'
 */
public int
craftsman_receive(string str)
{
  int *ids;
  string name;


  if (!(str ~= "zam�wienie"))
  {
    return craftsman_receive_syntax_failure_hook(str);
  }

  hook_interactive();

  name = this_player()->query_real_name();
  if (!sizeof(ids = sort_array(craftsman_query_orders_completed(name))))
  {
    return craftsman_receive_incompleted_item_hook();
  }

  if (sizeof(ids) == 1)
  {
    if (craftsman_present_order(ids[0]))
    {
      craftsman_cancel_order(ids[0]);
      return 1;
    }
    else
        return 0;
  }

  craftsman_receive_menu_hook(ids);
  input_to(receive_order_input);
  return 1;
}

/**
 * Deleguje konfiguracj� rzeczy do wzorca.
 *
 * @param item przedmiot do skonfigurowania
 * @param attrs wybrane przez u�ytkownika atrybuty
 * @param pattern wzorzec
 */
nomask void
craftsman_configure_item(object item, mapping attrs, object pattern)
{
    pattern->configure_item(item, attrs);
}

/*
 * Function name: craftsman_clone_item
 * Description:   clone the item to be configured.
 * Arguments:     1. (string) the file to clone
 *                2. (int)    the order id
 * Returns:       An instance of the item to be sold.
 */
public object
craftsman_clone_item(string file, int id)
{
    setuid();
    seteuid(getuid());
    return clone_object(file);
}

/*
 * Function name: craftsman_move_item
 * Description:   move the purchased item to the user
 * Arguments:     (object) the item to be moved
 * Returns:       1 - successfully moved to player or player's environment.
 *                0 - could not be moved to player or player's environment.
 */
public int
craftsman_move_item(object ob)
{
    if (ob->move(this_player()))
    {
        if (ob->move(environment(this_player())))
        {
            craftsman_check_move_env_failed_hook(ob);
            ob->remove_object();
            return 0;
        }

        craftsman_check_move_player_failed_hook(ob);
        return 1;
    }

    return 1;
}

/*
 * Function name: craftsman_charge_for_item
 * Description:   Charge for the ordered item
 * Arguments:     1. (object) the item to be sold
 *                2. (int)    the order id
 * Returns:       1/0 - item was paid for/was not paid for
 */
public varargs int
craftsman_charge_for_item(object ob, int id, int pre = 0, int remove = 1)
{
    int cost = craftsman_calc_item_cost(ob, id);
    if (pre) {
      cost = ftoi(prepaid * itof(cost));
    }
    else {
      cost -= ftoi(prepaid * itof(cost));
    }
    if (!MONEY_ADD(this_player(), -cost))
    {
        craftsman_check_cannot_pay_hook(cost, ob, id);
        notify_fail("");
        if (remove) {
            ob->remove_object();
        }
        return 0;
    }

    return 1;
}

/*
 * Function name: craftsman_present_order
 * Description:   Clone a fresh item, configure it, and give it to the user
 * Arguments:     (int) the order id
 * Returns:       1 - purchase was resolved and can be removed
 *                0 - purchase was not resolved
 */
public int
craftsman_present_order(int id)
{
    object ob;
    object pattern;
    mixed *order = m_orders[id];

    setuid();
    seteuid(getuid());

    if (order_is_repair(id)) // repair
    {
        ob = find_repair_object(id);//(order[ORDER_FILE]);
        if (!craftsman_charge_for_item(ob, id, 0, 0))
        {
            return 0;
        }
    }
    else if (order_is_resize(id)) // resize
    {
        ob = find_repair_object(id);
        if(!craftsman_charge_for_item(ob, id, 0, 0))
            return 0;
    }
    else
    {
        ob = craftsman_clone_item(order[ORDER_FILE], id);
        pattern = clone_object(order[ORDER_PATTERN]);
        craftsman_configure_item(ob, order[ORDER_SELECTIONS], pattern);
        if (!craftsman_charge_for_item(ob, id))
        {
            return 0;
        }
    }

    if (!craftsman_move_item(ob))
    {
        return 0;
    }

    ob->remove_prop(OBJ_I_IN_REPAIR_BY_CRAFTSMAN);

    if(!order_is_resize(id))
        CRAFTING_LIBRARY->repair_item(ob);
    else
        CRAFTING_LIBRARY->resize_item(ob, TP, craftsman_get_resize_name(id));

    craftsman_check_receive_order_hook(ob, id);
    return 1;
}

/*
 * Function name: craftsman_check
 * Description:   The "check" command.  Tell the user about the
 *                status of his orders.  If any are complete, let him
 *                have them.
 * Arguments:     (string) the arguments give to the "check" command
 * Returns:       1/0 - success/failure
 */
public int
craftsman_check(string str)
{
    string name;
    int *incomplete_orders, *complete_orders;
    int i;

    if (!(str ~= "zam�wienie" || str ~= "zam�wienia"))
    {
        return craftsman_check_syntax_failure_hook(str);
    }

    hook_interactive();

    name = this_player()->query_real_name();
    incomplete_orders = sort_array(craftsman_query_orders_configured(name));
    complete_orders   = sort_array(craftsman_query_orders_completed(name));

    if (!sizeof(incomplete_orders) && !sizeof(complete_orders))
    {
        return craftsman_check_unpurchased_item_hook();
    }

    if (sizeof(complete_orders))
    {
        craftsman_check_complete_order_hook(complete_orders);

        if (sizeof(incomplete_orders))
        {
            craftsman_check_incomplete_order_hook(incomplete_orders);
        }

        return 1;
    }

    craftsman_check_incomplete_order_hook(incomplete_orders);

    return 1;
}

/*
 * Function name: craftsman_clean_completed_orders
 * Description:   Remove old orders that have not been picked up
 */
public void
craftsman_clean_completed_orders()
{
    int i, t, *ids, hold;
    mixed *order;
    object ob, pattern;

    ids = craftsman_query_orders_completed();
    for (i = 0; i < sizeof(ids); i++)
    {
        order = m_orders[ids[i]];

        t = time();
        hold = craftsman_query_hold_time(ids[i]);
        // Remove orders which have been held long enough
        if ((hold > 0) && ((order[ORDER_COMPLETION_TIME] + hold) < t))
        {
            if (!order[ORDER_PATTERN]) { // repair
                ob = find_repair_object(ids[i]);//(order[ORDER_FILE]);
                ob->remove_prop(OBJ_I_IN_REPAIR_BY_CRAFTSMAN);
            }
            else {
                ob = craftsman_clone_item(order[ORDER_FILE], ids[i]);
                pattern = clone_object(order[ORDER_PATTERN]);
                craftsman_configure_item(ob, order[ORDER_SELECTIONS], pattern);
                ob->move(query_store_room());
            }
            craftsman_cancel_order(ids[i]);
        }
    }
}

/*
 * Function name: craftsman_init
 * Description:   adds the craftsman's commands.  Call this from the
 *                init()/init_living().
 */
public void
craftsman_init()
{
    init_sklepikarz();
    add_action(craftsman_purchase, "zam�w");
    add_action(craftsman_repair, "napraw");

    //Do zmiany rozmiar�w ubra� - zmienia� mo�emy tylko na mniejsze
    // na wi�ksze si� nie da - bo przecie nie ma z czego doszy�:P
    add_action(craftsman_resize, "pomniejsz");
    add_action(craftsman_resize, "zmniejsz");
    add_action(craftsman_resize, "powi�ksz");
    add_action(craftsman_resize, "zwi�ksz");
    add_action(craftsman_resize, "pomniejsz");
    add_action(craftsman_resize, "dopasuj");

    add_action(craftsman_receive, "odbierz");
#if ALLOW_CANCEL
    /* Rezygnujemy z tej mo�liwo�ci na razie */
    add_action(craftsman_cancel,   "zrezygnuj");
#endif ALLOW_CANCEL
    add_action(craftsman_check,    "sprawd�");

    if (allow_projects)
    {
        add_action(craftsman_project, "z��");
    }
    add_action(pomoc,"?");
    add_action(pomoc2,"?",2);
    add_action(pomoc3,"?z��");
    add_action(pomoc3,"?sk�adanie");
}

int
pomoc(string str)
{
    if(strlen(str))
        return 0;

    write("W tym miejscu jest rzemie�lnik. Je�li interesuje ci� pomoc\n"+
          "do niego - u�yj sk�adni pomocy podaj�c po znaku zapytania\n"+
          "osob�, kt�ra jest rzemie�lnikiem. Np. \"?stary mezczyzna\".\n");
    return 1;
}

public int
pomoc2(string str)
{
    object ob;

    notify_fail("Nie ma pomocy na ten temat.\n");

    if (!str)
        return 0;
    if (!parse_command(str, ENV(TP), "%o:" + PL_MIA, ob))
        return 0;
    if (!ob)
        return 0;

    if (ob != this_object())
        return 0;

    write("\nU rzemie�lnika mo�esz:\n"+
        "- zam�wi� - zlecasz rzemie�lnikowi wykonanie danego przedmiotu\n"+
        "- naprawi� - polecenie naprawy danej rzeczy, kt�r� posiadasz,\n"+
        "\ta kt�ra jest ju� podniszczona\n"+
        "- odebra� - zam�wion� ju� wcze�niej przez ciebie rzecz\n"+
        "- sprawdzi� zam�wienia - pozwala ci dowiedzie� si� o przedmiotach,\n"+
        "\tkt�re uprzednio zam�wi�"+TP->koncowka("e�","a�","e�")+",\n"+
        "- dopasowa� - polecenie dopasowania danej rzeczy do swoich\n"+
        "\trozmiar�w,\n" +
        "- zmniejszy� - pozwala zmniejszy� rzecz o jeden b�d� kilka rozmiar�w,\n"+
        "- zwi�kszy� - posiadan� rzecz o jeden lub kilka rozmiar�w - " +
        "\trzemie�lnik musi mie� odpowiednie materia�y");
    if(allow_projects)
    {
        write(",\n- z�o�y� projekt - je�li rzemie�lnik nie wykonuje tego, czego\n"+
            "\tczego potrzebujesz, masz szans� z�o�y� specjalne zam�wienie.\n"+
            "\tPo wi�cej szczeg��w sprawd�: ?z��");
    }
#if ALLOW_CANCEL
    write(",\n- zrezygnowa� - z kt�rego� z zam�wie�");
#endif ALLOW_CANCEL

    write(".\nPonadto u rzemie�lnika funkcjonuj� tak�e inne komendy, podobne "+
        "jak u\nkupc�w, tj: sprzedaj, wyce�, kup, poka�, przejrzyj.\n\n");
    write("Aha i pami�taj, �e mieszka�cy p�ac� mniej za wytwarzane przez "+
        "rzemie�lnika produkty.\n\n");
    return 1;
}

//pomoc do sk�adania projekt�w
int
pomoc3(string str)
{
    if(!allow_projects)
    {
        write("Tu nie mo�na sk�ada� projekt�w.\n");
        return 1;
    }
    if(TP->query_age() < 86400)
    {
        write("Niestety, osoby o wieku mniejszym ni� 2 dni nie mog� " +
            "sk�ada� specjalnych zam�wie�. Obejrzyj wpierw troch� " +
            "�wiata.\n");
        return 1;
    }

    if((query_verb() ~= "sk�adanie" && str != "projektu") ||
        (str && str != "projekt" && query_verb() ~= "z��"))
    {
        return 0;
    }

    write("\nW tym miejscu mo�esz sk�ada� specjalne zam�wienia (tzw. "+
        "projekty), w kt�rych "+
        "dok�adnie opiszesz przedmiot, kt�ry chcesz by rzemie�lnik "+
        "wykona�. Musisz okre�li�: wygl�d przedmiotu (dos�owny - taki, jakim "+
        "widzie� go masz ty, jak i inni), wag�, obj�to�� oraz materia�y "+
        "(w przypadku kilku materia��w trzeba tak�e okre�li� ich procentowy "+
        "udzia�) z jakich ma by� wykonany. Ponadto w przypadku zbroi, ubrania "+
        "lub bi�uterii musisz okre�li� rozmiar przedmiotu. "+
        "W swym projekcie musisz uwzgl�dni� tak�e to, czy ma by� "+
        "on wykonany jednorazowo (specjalnie dla ciebie), czy te� mo�e "+
        "by� on wykonywany dla innych klient�w. Wykonanie projektu "+
        "jednorazowo, specjalnie na twe �yczenie nie jest tanie, a ceny "+
        "na takie zam�wienia nierzadko przekraczaj� 100 koron, z kolei "+
        "z�o�enie projektu dost�pnego dla wszystkich klient�w jest darmowe "+
        "(lecz wtedy oczywi�cie by by� w posiadaniu tego przedmiotu musisz "+
        "jeszcze go zakupi�, gdy ten pojawi si� ju� na li�cie wykonywanych "+
        "przedmiot�w).\n\n");
    return 1;
}

/**
 * Ustawia zbi�r wzorc�w, z kt�rych mo�e korzysta� rzemie�lnik.
 *
 * @param patterns zbi�r wzorc�w
 */
public void
craftsman_set_sold_item_patterns(string *patterns)
{
  patterns_filenames = patterns;
  need_expanding = 1;
  expand_patterns_items();
}

public string *
craftsman_query_sold_item_patterns()
{
    return patterns_filenames + ({ });
}

/*
 * Function name: craftsman_set_selection_timeout
 * Description:   Indicate that the attribute selection process should time
 *                out in the specified number of seconds.  0 timeout indicates
 *                no timeout at all.
 * Arguments:     (int) timeout delay in seconds
 */
public void
craftsman_set_selection_timeout(int seconds)
{
    selection_timeout = seconds;
}

/*
 * Function name: craftsman_set_max_total_orders
 * Description:   Set the max number of orders that can be active at once
 * Arguments:     (int) the max number of orders
 */
public void
craftsman_set_max_total_orders(int i)
{
    max_total_orders = i;
}

/*
 * Function name: craftsman_set_max_user_orders
 * Description:   Set the max number of orders that can be active at once for
 *                a single order.
 * Arguments:     (int) the max number of orders
 */
public void
craftsman_set_max_user_orders(int i)
{
    max_user_orders = i;
}

/**
 * Ustawia mar�� danego rzemie�lnika.
 *
 * @param margin mar�a
 */
public void
craftsman_set_margin(float new_margin)
{
  margin = new_margin;
}

/**
 * Ustawia dziedzin� rzemie�lnika.
 *
 * @param domain dziedzina
 */
public void
craftsman_set_domain(int new_domain)
{
  domain = new_domain;
}

/**
 * Zwraca dziedzin� rzemie�lnika.
 *
 * @return dziedzina
 */
public int
craftsman_query_domain()
{
    return domain;
}

/*
 * Function name: craftsman_set_item_cost
 * Description:   Set the cost of an item
 * Arguments:     1. (int) the order id
 *                2. (int) the cost
 */
public void
craftsman_set_item_cost(int id, int cost)
{
    m_orders[id][ORDER_COST] = cost;
}

public int
craftsman_query_item_cost(int id)
{
    return m_orders[id][ORDER_COST];
}

/*
 * Function name: craftsman_set_hold_time
 * Description:   Set the minimum amount of time that the craftsman will hold a
 *                completed order.  If set to a negative value, orders will
 *                be held indefinitely.
 * Arguments:     1. (int) the order id
 *                2. (int) hold time in seconds
 */
public void
craftsman_set_hold_time(int id, int i)
{
    m_orders[id][ORDER_HOLD] = i;
}

public int
craftsman_query_hold_time(int id)
{
    return m_orders[id][ORDER_HOLD];
}

/*
 * Function name: craftsman_add_attribute
 * Description:   add a configuration option.  The user will be prompted to
 *                select a value from the given values.
 * Arguments:     1. (int)    the order id
 *                2. (string) The name of the attribute.
 *                3. (mixed)  Possible values for the attribute.
 */
public int
craftsman_add_attribute(int id, string attr_name, mixed attrs)
{
    mixed *order = m_orders[id];
    mixed *old_attrs = order[ORDER_ATTRS];

    /* A mapping might seem more appropriate, but it's desirable to maintain
     * the order of the attributes.
     */
    old_attrs += ({ ({ attr_name, attrs }) });
    order[ORDER_ATTRS] = old_attrs;
    return sizeof(old_attrs) - 1;
}

public void
craftsman_remove_attribute(int id, string attribute)
{
    mixed *order = m_orders[id];
    order[ORDER_ATTRS] = filter(order[ORDER_ATTRS],
        &operator(!=)(attribute) @ &operator([])(, 0));
}

public void
craftsman_clear_attributes(int id)
{
    m_orders[id][ORDER_ATTRS] = ({});
}


public void
craftsman_add_selection(int id, string attr_name, mixed selection)
{
    m_orders[id][ORDER_SELECTIONS][attr_name] = selection;
}

/*
 * Function name: craftsman_set_item_file
 * Description:   set the pathname to the item which will be created
 * Arguments:     1. (int)    the order id
 *                2. (string) the filename
 */
public void
craftsman_set_item_file(int id, string f)
{
    m_orders[id][ORDER_FILE] = f;
}

/**
 * Ustawia �cie�k� do wzorca.
 *
 * @param id identyfikator zam�wienia
 * @param f �cie�ka
 */

public void
craftsman_set_item_pattern(int id, string f)
{
  m_orders[id][ORDER_PATTERN] = f;
}

/*
 * Function name: craftsman_query_item_file
 * Description:   Get the item file for the given order
 * Arguments:     (int) the order id
 * Returns:       The item file
 */
public string
craftsman_query_item_file(int id)
{
    return m_orders[id][ORDER_FILE];
}

/**
 * Sprawdzamy spos�b w jaki ma by� resizowany przedmiot.
 */
string
craftsman_get_resize_name(int id)
{
    return m_orders[id][ORDER_RS_NS];
}

/*
 * Function name: craftsman_set_time_to_complete
 * Description:   Give the amount of time in seconds it takes to create
 *                an item.
 * Arguments:     1. (int) the order id
 *                2. (int) completion time in seconds
 */
public void
craftsman_set_time_to_complete(int id, int t)
{
    m_orders[id][ORDER_TIME] = t;
}

/*
 * Function name: craftsman_query_time_to_complete
 * Description:   Get the amount of time in seconds required to create a
 *                given order.
 * Arguments:     (int) the order id
 * Returns:       the completion time
 */
public int
craftsman_query_time_to_complete(int id)
{
    return m_orders[id][ORDER_TIME];
}

public int
craftsman_query_completion_time(int id)
{
    return time() + craftsman_query_time_to_complete(id);
}

public int
craftsman_query_status(int id)
{
    return m_orders[id][ORDER_STATUS];
}

public string
craftsman_query_owner(int id)
{
    return m_orders[id][ORDER_OWNER];
}

public mixed *
craftsman_query_attributes(int id)
{
    return m_orders[id][ORDER_ATTRS] + ({});
}

public string
craftsman_query_attribute_name(int id, int attr_number)
{
    return m_orders[id][ORDER_ATTRS][attr_number][0];
}

public string
craftsman_query_attribute_values(int id, int attr_number)
{
    return m_orders[id][ORDER_ATTRS][attr_number][1];
}

public string
craftsman_query_attribute_selection(int id, int attr_number)
{
    return m_orders[id][ORDER_SELECTIONS][craftsman_query_attribute_name(id, attr_number)];
}

public mapping
craftsman_query_selections(int id)
{
    return m_orders[id][ORDER_SELECTIONS] + ([]);
}

/*
 * Function name: craftsman_new_order
 * Description:   Add a new order and add default values
 * Arguments:     (string) the user
 * Returns:       the order id
 */
public varargs int
craftsman_new_order(string owner, int typ=ORDER_PURCHASE)
{
    mixed *order = allocate(ORDER_SIZE);
    int id;

    // Default values
    order[ORDER_STATUS]      = STATUS_IN_PROGRESS;
    order[ORDER_OWNER]       = owner;
    order[ORDER_TIME]        = 57600;
    order[ORDER_HOLD]        = 86400;
    order[ORDER_ATTRS]       = ({});
    order[ORDER_SELECTIONS]  = ([]);
    order[ORDER_TYPE]        = typ;

    id = (m_sizeof(m_orders) ? applyv(max, m_indices(m_orders)) + 1 : 1);
    m_orders[id] = order;

    return id;
}

/**
 * Deleguje konfiguracj� zam�wienia do wzorca.
 *
 * @param id identyfikator zam�wienia
 * @param pattern wzorzec
 * @param item przedmiot, dla kt�rego konfigurowane jest zam�wienie
 */
public nomask void
craftsman_configure_order(int id, object pattern)
{
  craftsman_set_item_file(id, pattern->query_item_filename());
  craftsman_set_item_pattern(id, explode(file_name(pattern), "#")[0]);
  craftsman_set_time_to_complete(id, pattern->query_time_to_complete());

	int price = pattern->query_estimated_price();

	//sprawdzanko, czy mieszkaniec. Vera. Mieszka�cy maj� 5% taniej.
	string *odmiana = slownik_pobierz(TO->query_spolecznosc());

	if(pointerp(odmiana) && "z " + odmiana[0][1] ~= TP->query_origin())
  		craftsman_set_item_cost(id, price - price / 20);
	else
		craftsman_set_item_cost(id, price);

  foreach (mixed attr : pattern->query_attributes()) {
    craftsman_add_attribute(id, attr[0], attr[1]);
  }
  if (pattern->query_want_sizes()) {
    craftsman_want_sizes(id);
  }
}

/**
 * Ustawia standardowe opcje rzemie�lnika.
 */
void
config_default_craftsman()
{
    add_ask(({"materia�y", "materia�"}), VBFC_ME("pyt_o_materialy"));
    config_default_sklepikarz();
    set_co_sprzedajemy(0);
    expand_patterns_items();
    if(file_size(MASTER_OB(TO)+".o") > 0)
       restore_object(MASTER_OB(TO));
}

string
pyt_o_materialy()
{
  if (pointerp(materials) && sizeof(materials)) {
    set_alarm(0.5, 0.0, "command_present", this_player(),
            "emote m�wi: Obecnie potrzebuj� takich materia��w jak " +
            COMPOSITE_WORDS(map(materials, &MATERIALY->nazwa_materialu())) + ".");
    return "";
  }
  else {
    set_alarm(0.5, 0.0, "command_present", this_player(),
            "emote m�wi: Nie skupuj� �adnych materia��w.");
  }
    return "";
}

/**
 * Pozwala na skup tylko materia��w potrzebnych do wyrobu przedmiot�w.
 *
 * @return czy dana rzecz jest potrzebnym materia�em
 */
int
check_skupujemy(object ob)
{
  mixed mat;
  if (!objectp(ob)) {
    return 0;
  }
  mat = ob->query_materials();
  if (!pointerp(mat)) {
    return 0;
  }
  foreach(string material : mat) {
    if (member_array(material, materials) != -1) {
      return 1;
    }
  }
  return 0;
}

/**
 * Pozwala na sprzeda� rzeczy nie b�d�cych materia�ami.
 *
 * @return czy dana rzecz nie jest materia�em
 */
int
check_sprzedajemy(object ob)
{
  if (!objectp(ob)) {
    return 0;
  }
  if (dont_want_materials && pointerp(ob->query_materials())) {
    return 0;
  }
  if (only_materials) {
    if (pointerp(ob->query_materials())) {
      return 1;
    }
    return 0;
  }
  if (ob->query_worn() || ob->query_wielded()) {
      return 0;
  }
  if (function_exists("create_heap", ob) == "/std/coins") {
      return 0;
  }
  if (member_array(craftsman_query_domain(), ob->query_tool_domains()) != -1) {
      return 0;
  }
  if (ob->query_prop(OBJ_I_IN_REPAIR_BY_CRAFTSMAN)) {
      return 0;
  }
  return 1;
}

/**
 * Sprawdza, czy wystarczy materia��w na wykonanie zam�wienia.
 *
 * @param order zam�wienie do sprawdzenia
 * @param id numer zam�wienia
 *
 * @return <ul>
 *            <li> <b>1</b> je�eli materia��w wystarczy
 *            <li> <b>0</b> je�eli materia��w zabraknie
 *         </ul>
 */
int
craftsman_config_validate_needed_materials(mixed order, int id)
{
  object ob, pattern;

  ob = craftsman_clone_item(order[ORDER_FILE], id);
  pattern = clone_object(order[ORDER_PATTERN]);
  craftsman_configure_item(ob, order[ORDER_SELECTIONS], pattern);

#ifndef FREEZE_MATERIALS
  if (!(CRAFTING_LIBRARY->check_materials(ob, all_inventory(query_store_room())))) {
    craftsman_not_enough_materials_hook();
    return 0;
  }
#endif
  return 1;
}

/**
 * Sprawdza, czy podane narz�dzia wystarcz� do obr�bki przedmiotu.
 *
 * @param order zam�wienie do sprawdzenia
 *
 * @return 1 je�eli narz�dzia wystarcz�
 * @return 0 je�eli narz�dzia nie wystarcz�
 */
int
craftsman_config_validate_needed_tools(mixed order)
{
    object pattern = clone_object(order[ORDER_PATTERN]);
    if (!(CRAFTING_LIBRARY->check_tools(TO, pattern, all_inventory(TO)))) {
        craftsman_missing_tools_hook();
        return 0;
    }
    return 1;
}

/**
 * Wywo�ana gdy gracz u�yje komendy 'przejrzyj'.
 * @param       str - nazwa obiekt�w jakich szukamy
 *                       ( bronie / zbroje albo cokolwiek)
 * @return     0 je�li nie mamy niczego takiego na stanie
 * @return     1 je�li co� znale�li�my
 */
int
do_list(string str, string title = 0)
{
  object *items;
  string *descs = ({});
  int max, other;
  /* zwyk�e 'przejrzyj' od sklepikarza */
  dont_want_materials = 1;
  other = ::do_list(str);
  dont_want_materials = 0;

  
  //nie wiem dlaczego tak, ale samo 'przejrzyj' nie dzia�a, a 'przejrzyj wszystko' owszem, wi�c robi� taki brzydki myk. V.
  if(!str || str == "0")
      str="wszystko";
  
  
  if (str == "bronie")
    items = filter(sold_items, weapon_filter);
  else if (str == "zbroje")
    items = filter(sold_items, armour_filter);
  else if (str ~= "materia�y") {
    only_materials = 1;
    if (::do_list(0, "Materia�y")) {
      only_materials = 0;
      return craftsman_list_needed_materials_hook(1);
    }
    only_materials = 0;
    return craftsman_list_needed_materials_hook(0);
  }
  else if (str)
    items = FIND_STR_IN_ARR(str, sold_items, PL_BIE);
  else
    items = sold_items;

  if (sizeof(items) < 1)
  {
      if (str && !other) {
          craftsman_purchase_no_match_hook(str);
      }
    return 0;
  }

  max = MIN(MAXLIST, sizeof(items));

	int price;
  foreach(object item : items) {
    object pattern = used_patterns[member_array(item, sold_items)];

	price = pattern->query_estimated_price();
	//sprawdzanko, czy mieszkaniec. Vera
	string *odmiana = slownik_pobierz(TO->query_spolecznosc());
	if(pointerp(odmiana) && "z " + odmiana[0][1] ~= TP->query_origin())
  		price = price - price/10;

    descs += ({MASTER_OB(item)+"##"+price});
  }

  write("                                 __________\n");
  write(" _______________________________/ Wykonuj� \\"+
      "__________________________________\n");
  write("/\t\t\t\t\t\t\t\t\t      \\\n"+
      "| przedmiot\t\t\t\t     szacowana cena | kr  | dn  | gr  |\n"+
      "|========================================="+
      "====================================|\n");

  foreach (string desc : descs) {
    string * tstr_arr=explode(desc, "##");
    //write(tstr_arr[0]+" \n");
    wyswietl_liste_pogrupowana(0, tstr_arr[0], atoi(tstr_arr[1]), 0);
  }
  write("\\________________________________________"+
      "_____________________________________/\n");

  if (max < sizeof(items))
  {
    write("Lista zosta�a skr�cona.\n");
  }
  craftsman_collecting_prepaid_hook();

  return 1;
}

/**
 * Wywo�ana gdy gracz u�yje komendy 'poka�'.
 * @param   str - string opisuj�cy co gracz chce obejrze�
 * @returns   1 - je�li uda�o si� obejrze�.

 */
int
do_show(string str)
{
    int num_orders, id;
    string name;
    object item_to_sold;
    object used_pattern;

    if (::do_show(str))
        return 1;

    expand_patterns_items();

    if (!stringp(str))
        return craftsman_purchase_syntax_failure_hook(str);

    if (!parse_command(str, sold_items, "%o:" + PL_BIE, item_to_sold))
        return craftsman_purchase_syntax_failure_hook(str);

    shop_hook_appraise_object(item_to_sold);
    write("Tak oto wygl�da� b�d" + (item_to_sold->query_tylko_mn() ? "�" : "zie") + " " +item_to_sold->short(PL_MIA)+
        ", gdy sko�cz�:\n");
    //zamiast samego longa, dam wszystko, �eby by�a waga i obj�to��: V.
    item_to_sold->appraise_object();

    return 1;
}


#define AVAILABLE_RACE_SEX ([ \
    "kobieta" : ({"cz^lowiek", 1}),\
    "m�czyzna" : ({"cz^lowiek", 0}),\
    "elfka" : ({"elf", 1}),\
    "elf" : ({"elf", 0}),\
    "p�elfka" : ({"p^o^lelf", 1}),\
    "p�elf" : ({"p^o^lelf", 0}),\
    "krasnoludka" : ({"krasnolud", 1}),\
    "krasnolud" : ({"krasnolud", 0}),\
    "nizio�ka" : ({"nizio^lek", 1}),\
    "nizio�ek" : ({"nizio^lek", 0}),\
    "gnomka" : ({"gnom", 1}),\
    "gnom" : ({"gnom", 0}),\
    ])

#define AVAILABLE_SIZES ([ \
    "malutkie" : 0.5,\
    "bardzo ma�e" : 0.7,\
    "ma�e" : 0.9,\
    "�rednie" : 1.0,\
    "du�e" : 1.1,\
    "bardzo du�e" : 1.3,\
    "ogromne" : 1.5,\
    ])

/**
 * Funkcja odpowiedzialna za pobranie nazwy rozmiaru od gracza.
 */
public int
for_what_size(int id, int attr_number, mixed tab, string value = 0)
{
  mixed *order = m_orders[id];
  float* wspolczynniki;
  if (value == 0) {
    write("Jakiego rozmiaru ma by� to ubranie? (~q �eby przerwa�)\n");
    write("[malutkie, bardzo ma�e, ma�e, �rednie\n");
    write("du�e, bardzo du�e, ogromne]\n");
    input_to(&for_what_size(id, attr_number, tab, ));
  }
  else {
    string attr_nm = order[ORDER_ATTRS][attr_number][0];
    if (value == "~q") {
        craftsman_abort_hook();
        craftsman_cancel_order(id);
        return 0;
    }
    if (member_array(value, m_indices(AVAILABLE_SIZES)) != -1) {
      tab[attr_nm] += ({ AVAILABLE_SIZES[value] });
      if (++attr_number < sizeof(order[ORDER_ATTRS])) {
        craftsman_config_attributes(id, attr_number);
        return 0;
      }

      craftsman_completed_config(id);
      return 0;
    }
    else {
      write("Nie rozumiem.\n");
      for_what_size(id, attr_number, tab, 0);
    }
  }
}

/**
 * Funkcja odpowiedzialna za pobranie rasy i p�ci od gracza.
 */
public int
for_what_race_sex(int id, int attr_number, mixed tab, string value = 0)
{
  mixed *order = m_orders[id];
  if (value == 0) {
    write("Jakiej rasy i p�ci jest ta osoba? (~q �eby przerwa�)\n");
    write("[kobieta, m�czyzna, elfka, elf, p�elfka, p�elf\n");
    write("krasnoludka, krasnolud, nizio�ka, nizio�ek, gnomka, gnom]\n");
    input_to(&for_what_race_sex(id, attr_number, tab, ));
  }
  else {
    string attr_nm = order[ORDER_ATTRS][attr_number][0];
    if (value == "~q") {
        craftsman_abort_hook();
        craftsman_cancel_order(id);
        return 0;
    }
    if (member_array(value, m_indices(AVAILABLE_RACE_SEX)) != -1) {
      tab[attr_nm] = AVAILABLE_RACE_SEX[value];
      for_what_size(id, attr_number, tab, 0);
    }
    else {
      write("Nie rozumiem.\n");
      for_what_race_sex(id, attr_number, tab, 0);
    }
  }
  return 0;
}

/**
 * Funkcja odpowiedzialna za wyb�r mi�dzy rozmiarami gracza
 * (go�a klata vs. inne ubrania).
 */
public int
tic_other_clothing(int id, int attr_number, mixed tab, string value = 0)
{
    mixed *order = m_orders[id];
    if (value == 0) {
        write("Czy przy mierzeniu ciebie mam uwzgl�dni� to, co masz teraz na sobie? (~q �eby przerwa�)\n");
        write("[tak - uwzgl�dni� to, co masz w danej chwili na sobie\n");
        write(" nie - pobior� twoje prawdziwe rozmiary]\n");
        input_to(&tic_other_clothing(id, attr_number, tab, ));
    }
    else {
        string attr_nm = order[ORDER_ATTRS][attr_number][0];
        if (value == "~q") {
            craftsman_abort_hook();
            craftsman_cancel_order(id);
            return 0;
        }
        if (value == "tak")
        {
            tab[attr_nm] = this_player()->oblicz_rozmiary_osoby();
            if (++attr_number < sizeof(order[ORDER_ATTRS])) {
                craftsman_config_attributes(id, attr_number);
                return 0;
            }

            craftsman_completed_config(id);

            return 0;
        }
        else if (value == "nie")
        {
            tab[attr_nm] = this_player()->pobierz_rozmiary();
            if (++attr_number < sizeof(order[ORDER_ATTRS])) {
                craftsman_config_attributes(id, attr_number);
                return 0;
            }

            craftsman_completed_config(id);
            return 0;
        }
        else {
            write("Nie rozumiem.\n");
            tic_other_clothing(id, attr_number, tab, 0);
        }
    }
    return 0;
}

/**
 * Funkcja odpowiedzialna za pobranie rozmiar�w od gracza.
 */
public int
for_whom(int id, int attr_number, mixed tab, string value = 0)
{
  mixed *order = m_orders[id];
  if (value == 0) {
    write("Czy zamawiana rzecz ma by� wykonana dla ciebie? [tak/nie] (~q �eby przerwa�)\n");
    input_to(&for_whom(id, attr_number, tab, ));
  }
  else {
    string attr_nm = order[ORDER_ATTRS][attr_number][0];
    if (value == "~q") {
        craftsman_abort_hook();
        craftsman_cancel_order(id);
        return 0;
    }
    if (value == "tak") {
        tic_other_clothing(id, attr_number, tab, 0);
    }
    else if (value == "nie") {
      for_what_race_sex(id, attr_number, tab, 0);
    }
    else {
      write("Nie rozumiem.\n");
      for_whom(id, attr_number, tab, 0);
    }
  }
  return 0;
}

/**
 * Funkcja ustawiaj�ca specjalny atrybut, kt�ry spowoduje pobieranie
 * rozmiar�w od gracza.
 *
 * @param id identyfikator zam�wienia
 */
public void
craftsman_want_sizes(int id)
{
  craftsman_add_attribute(id, "__sizes", &for_whom());
}

/**
 * Funkcja uniemo�liwiaj�ca sk�adanie projekt�w.
 */
public void
disallow_projects()
{
  allow_projects = 0;
}

/**
 * Funkcja sprawdzaj�ca, czy rzemie�lnik jest zaj�ty.
 *
 * @return >0 rzemie�lnik pracuje
 * @return 0 rzemie�lnik jest wolny
 */
public int
craftsman_is_busy()
{
    return next_completion_time;
}

public nomask int
save_craftsman(string filename)
{
    if (!filename)
    {
        return 0;
    }

    seteuid(getuid(this_object()));
    save_map((["m_orders":m_orders]), filename);
    seteuid(getuid(this_object()));

    return 1;
}

/**
 * Funkcja zwraca napis do auto_�adowania.
 *
 * @return napis do auto_�adowania.
 */
public nomask string
query_craftsman_auto_load()
{
    seteuid(0);
    SECURITY->save_craftsman();
    seteuid(getuid(this_object()));
    return " @Craft@" + time() + "," + MASTER_OB(TO) + "@Craft@ ";
}

/**
 * Funkcja odtwarza stan rzemie�lnika na podstawie napisu
 * z funkcji query_craftsman_auto_load.
 *
 * @param arg napis zwr�cony wcze�niej przez query_craftsman_auto_load.
 */
public nomask string
init_craftsman_arg(string arg)
{
    string toReturn, foobar, filename;
    int ptime;
    int *ids, i;
    mixed *order;

    if (arg == 0) {
        craftsman_reset_completion_time();
        return 0;
    }
    if (sscanf(arg, "%s@Craft@%d,%s@Craft@%s", foobar, ptime, filename, toReturn) == 4) {
        foreach (string curstr : explode(foobar, "")) {
            if (curstr != " ")
                return arg;
        }
        int delta = time() - ptime;
        ids = m_indices(m_orders);
        for (i = 0; i < sizeof(ids); i++)
        {
            order = m_orders[ids[i]];
            if (!order[ORDER_PATTERN]) { //naprawa
                foreach (object ob : all_inventory(query_store_room())) {
                    if (ob->query_prop(OBJ_I_IN_REPAIR_BY_CRAFTSMAN) == ids[i]) {
                        order[ORDER_FILE] = MASTER_OB(ob);
                        break;
                    }
                }
            }
            if (order[ORDER_STATUS] != STATUS_CONFIGURED)
            {
                continue;
            }
            order[ORDER_COMPLETION_TIME] += delta;
        }

        craftsman_reset_completion_time();
        return toReturn;
    }
    return arg;
}

/**
 * Sprawdza gdzie jest magazyn z kt�rego korzystamy.
 *
 * @return   obiekt magazynu
 */
mixed
query_store_room()
{
    object toReturn;
    if (stringp(store_room)) {
        store_room->load_me();
        return find_object(store_room);
    }
    return store_room;
}

/**
 * Dzi�ki tej funkcji mo�emy znale�� obiekt przedmiotu kt�ry
 * jest naprawiany.
 *
 * @param id id naprawy
 * @return naprawiany obiekt.
 */
public nomask object
find_repair_object(int id)
{
    object *obs = all_inventory(query_store_room());

    if(!sizeof(obs))
        return 0;

    foreach(object ob : obs)
    {
        if(ob->query_prop(OBJ_I_IN_REPAIR_BY_CRAFTSMAN) == id)
            return ob;
    }
}

/**
 * @return 1 je�li zam�wienie jest zleceniem naprawy,
 *         0 je�li nie istnieje b�d� nie jest napraw�.
 */
public nomask int
order_is_repair(int id)
{
    if(!is_mapping_index(id, m_orders))
        return 0;

    if(m_orders[id][ORDER_TYPE] == ORDER_REPAIR)
        return 1;
}

/**
 * @return 1 je�li zam�wienie jest zleceniem zmiany rozmiaru,
 *         0 je�li nie istnieje b�d� nie jest zmian� rozmiaru.
 */
public nomask int
order_is_resize(int id)
{
    if(!is_mapping_index(id, m_orders))
        return 0;

    if(m_orders[id][ORDER_TYPE] == ORDER_RESIZE)
        return 1;
}

/**
 * @return 1 je�li zam�wienie jest zleceniem wykonania,
 *         0 je�li nie istnieje b�d� nie jest zleceniem wykonania.
 */
public nomask int
order_is_purchase(int id)
{
    if(!is_mapping_index(id, m_orders))
        return 0;

    if(m_orders[id][ORDER_TYPE] == ORDER_PURCHASE)
        return 1;
}

//DO USUNI�CIA
void
reset_orders()
{
    m_orders = ([]);
}