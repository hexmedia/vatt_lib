/**
 * \file /lib/training_library.c
 *
 * Plik pomocny w zdobywaniu do�wiadczenia teoretycznego.
 *
 * @example
 * inherit "/std/room.c";
 *
 * void
 * create_room()
 * {
 *     config_training_library();
 * }
 *
 * void
 * init()
 * {
 *     init_training_library();
 * }
 *
 * @author Krun
 * @date Grudzie� 2007
 */

// inherit "/lib/trade.c";
inherit "/lib/training.c";

#include <pl.h>
#include <macros.h>
#include <cmdparse.h>
#include <composite.h>
#include <language.h>
#include <ss_types.h>

#define P_SS_N      0
#define P_CENA      1
#define P_JAKOSC    2
#define P_MAX       3
#define P_MIN       4
#define P_OPIS      5
#define P_PRZYM     6
#define P_NAZWA     7
#define P_TIME      8
#define P_SUFFIX    9

private static mixed pergaminy;
// mapping pozyczajacy;
int b_id;

public void ustaw_b_id(int i) { b_id = i; }
public int query_b_id() { return b_id; }

void reset_pergaminy()
{
    pergaminy = ({});
}

public varargs void
dodaj_pergamin(int um, int cena, int jakosc, int max, int min,
    int czas, string opis, string *przymiotniki, string nazwa="pergamin", string suffix = "")
{
    if(!pointerp(pergaminy))
        pergaminy = ({});

    pergaminy += ({ ({um, cena, jakosc, max, min, opis, przymiotniki, nazwa, czas, suffix}) });
}

public int
czy_pergamin_istnieje(int nr)
{
    if(nr > 0 && nr <= sizeof(pergaminy))
       return 1;
}

public mixed
query_pergaminy()
{
    return pergaminy;
}

public mixed
query_pergamin(int nr)
{
    return pergaminy[nr-1];
}

void
config_training_library()
{
    config_default_trade();
}

public int pozycz(string arg);
public int oddaj(string arg);
public int przejrzyj(string arg);

void
init_training_library()
{
    add_action(pozycz,      "po�ycz");
    add_action(pozycz,      "wypo�ycz");
    add_action(oddaj,       "oddaj");
    add_action(oddaj,       "zwr��");
    add_action(przejrzyj,   "przejrzyj");
}

static private object sklonuj_pergamin(int nr)
{
    if(nr < 1 || nr > sizeof(pergaminy))
        return 0;

    object pergamin = clone_object(TRAINING_SCROLL_OBJECT);

    int um = pergaminy[nr-1][P_SS_N];
    mixed perg = pergaminy[nr-1];

    pergamin->ustaw_skilla(um);
    pergamin->ustaw_cene(sprawdz_cene(um, perg[P_CENA], perg[P_MIN], perg[P_MAX], TP));
    pergamin->ustaw_max_uma(perg[P_MAX]);
    pergamin->ustaw_min_uma(perg[P_MIN]);
    pergamin->ustaw_jakosc(perg[P_JAKOSC]);
    pergamin->set_long(perg[P_OPIS]);
    pergamin->ustaw_nazwe(perg[P_NAZWA]);
    pergamin->ustaw_czas_czytania(perg[P_TIME]);
    pergamin->ustaw_b_id(query_b_id());

    for(int i=0;i<sizeof(perg[P_PRZYM]);i+=2)
        pergamin->dodaj_przym(perg[P_PRZYM][i], perg[P_PRZYM][i+1]);

    pergamin->odmien_short();
    pergamin->odmien_pshort();

    if(TO->query_npc())
        pergamin->move(TO, 1);

    return pergamin;
}

/**
 * HOOOKI
 */
public int
training_library_hook_pozycz_co()
{
    NF("Co takiego chcesz po�yczy�?\n");
    return 0;
}

public int
training_library_hook_pozycz_nieprecyzyjnie()
{
    NF("Nie wiem o kt�ry dok�adnie pergamin ci chodzi. Spr�buj bardziej sprecyzowa�.\n");
    return 0;
}

public int
training_library_hook_pozycz_pergamin_nie_istnieje()
{
    NF("Taki pergamin nie istnieje.\n");
    return 0;
}

/* bo jeste�my 'zbyt niedo�wiadczeni' lub 'zbyt do�wiadczeni' */
public int
training_library_hook_pozycz_pergamin_nie_mozna()
{
    NF("Ten pergamin do niczego ci si� nie przyda.\n");
    return 0;
}

public int
training_library_hook_pozycz_nie_stac()
{
    NF("Nie sta� ci� na po�yczenie tego pergaminu.\n");
    return 0;
}

public int
training_library_hook_pozycz_pozyczyl_wczesniej()
{
    NF("Ale� ty masz ju� ode mnie jeden pergamin.\n");
    return 0;
}

public void
training_library_hook_pozycz_daje_pergamin(int cena, object pergamin)
{
    write("Po�yczasz " + pergamin->short(TP, PL_BIE) + " i p�acisz " +
        text(split_values(cena), PL_BIE) + ".\n");
    saybb(QCIMIE(TP, PL_MIA) + " po�ycza jaki� pergamin.\n");
}

public int
training_library_hook_oddaj_nie_pergamin()
{
    NF("Nie mo�esz odda� czego� czego nie po�yczy�e�!\n");
    return 0;
}

public int
training_library_hook_oddaj_za_duzo()
{
    NF("Nie mo�esz odda� wiecej ni� jednego pergaminu na raz.\n");
    return 0;
}

public int
training_library_hook_oddaj_nie_stad()
{
    NF("Ten pergamin nie jest z tej biblioteki.\n");
    return 0;
}

public int
training_library_hook_oddaj_co()
{
    NF("Co takiego chcesz odda�?\n");
    return 0;
}

public void
training_library_hook_oddaj_oddane(object p)
{
    write("Oddajesz " + p->short(TP, PL_BIE) + ".\n");
    saybb(QCIMIE(TP, PL_MIA) + " oddaje " + QSHORT(p, PL_BIE) + ".\n");
}

public int
training_library_hook_przejrzyj_co()
{
    NF("Co takiego chcesz przejrze�?\n");
    return 0;
}

public void
training_library_hook_przejrzyj_przegladam()
{
;
}

/**
 * Funkcja wewn�trzna, pomocnicza.
 */
private static int filtruj_pergaminy(object p)
{
    if(p->is_pergamin_treningowy() && p->query_b_id() == query_b_id())
        return 1;
}

public int
pozycz(string arg)
{
    mixed nr;

    if(!arg)
        return training_library_hook_pozycz_co();

    if(sscanf(arg, "pergamin numer %s", nr)!=1 && sscanf(arg, "pergamin %s", nr)!=1)
        return training_library_hook_pozycz_co();

    int num;
    if(stringp(nr))
    {
        num = atoi(nr);

        if(num == 0)
            num = LANG_NUMS(nr);
        //A teraz poszukamy tytu�u
        if(num == 0)
        {
            int znalezione = 0;
            for(int i = 0; i < sizeof(pergaminy); i++)
            {
                string title = SS_SKILL_DESC[pergaminy[i][P_SS_N]][0] + pergaminy[i][P_SUFFIX];
                if(title == nr)
                    num = ++i;
                else if(wildmatch("*" + nr + "*", title) && nr != pergaminy[i][P_SUFFIX])
                {
                    num = ++i;
                    znalezione++;
                }
            }
            if(znalezione > 1)
                return training_library_hook_pozycz_nieprecyzyjnie();
        }
    }
    else if(!intp(num))
        return training_library_hook_pozycz_co();

    if(!czy_pergamin_istnieje(num))
        return training_library_hook_pozycz_pergamin_nie_istnieje();

    if(sizeof(filter(all_inventory(TP), &filtruj_pergaminy())))
        return training_library_hook_pozycz_pozyczyl_wczesniej();

    //sprawdzanie czy przypadkiem nie jestesmy 'zbyt niedoswiadczeni' (wynik: -1)
    //lub 'zbyt doswiadczeni' (wynik: -2). Verk.
    if(sprawdz_cene(pergaminy[num-1][P_SS_N], pergaminy[num-1][P_CENA],
                pergaminy[num-1][P_MIN],pergaminy[num-1][P_MAX], TP) < 0)
        return training_library_hook_pozycz_pergamin_nie_mozna();


    object pergamin = sklonuj_pergamin(num);

    if(!pergamin)
    {
        write("Wyst�pi� b��d. Nie uda�o si� utworzy� pergaminu. "+
            "Zg�o� go opisuj�c okoliczno�ci w jakich do niego "+
            "dosz�o.\n");
        return 1;
    }

    int cena = pergamin->query_cena();

    if(!can_pay(cena, TP))
        return training_library_hook_pozycz_nie_stac();

    mixed r;
    r = pay(cena, TP);

    training_library_hook_pozycz_daje_pergamin(cena, pergamin);

    pergamin->move(TP, 1);
    
    return 1;
}

public int
oddaj(string arg)
{
    object *pergamin;

    if(!arg)
        return training_library_hook_oddaj_co();

    if(!parse_command(arg, all_inventory(TP), "%i:" + PL_BIE, pergamin))
        return training_library_hook_oddaj_co();

    pergamin = NORMAL_ACCESS(pergamin, 0, 0);

    if(!pergamin)
        return training_library_hook_oddaj_co();
    else if(sizeof(pergamin) > 1)
        return training_library_hook_oddaj_za_duzo();

    if(!pergamin[0]->query_pergamin_treningowy())
        return training_library_hook_oddaj_nie_pergamin();

    if(pergamin[0]->query_b_id() != query_b_id())
        return training_library_hook_oddaj_nie_stad();

    training_library_hook_oddaj_oddane(pergamin[0]);

    pergamin[0]->remove_object();

    return 1;
}

public int
przejrzyj(string arg)
{
    if(arg != "pergaminy")
        return training_library_hook_przejrzyj_co();

    if(!sizeof(pergaminy))
    {
        write("Na chwile obecn� w bibliotece nie ma �adnych pergamin�w.\n");
        return 1;
    }

    write("\n");
    for(int i = 0; i < sizeof(pergaminy); i++)
    {
        write(sprintf("%3s %35s %36s\n", (i+1)+".",
            SS_SKILL_DESC[pergaminy[i][P_SS_N]][0] + (stringp(pergaminy[i][P_SUFFIX]) ? " " + pergaminy[i][P_SUFFIX] : ""),
            pokaz_cene(sprawdz_cene(pergaminy[i][P_SS_N], pergaminy[i][P_CENA], pergaminy[i][P_MIN],
                pergaminy[i][P_MAX], TP))));
    }

    training_library_hook_przejrzyj_przegladam();
    return 1;
}

public string
training_library_help()
{
    return "Pomoc biblioteki w kt�rej mo�na si� uczy�:\n\n"+
        " - wypo�ycz pergamin [numer] - Po�yczenie jakiego� pergaminu,\n"+
        " - oddaj                   - Zwrot pergaminu,\n" +
        " - przejrzyj pergaminy     - Lista dost�pnych pergamin�w.\n";
}