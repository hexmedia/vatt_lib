/**
 * \file /lib/psuj.c
 * Jest to lib dla obiekt�w, kt�re si� zu�ywaj� i psuj�.
 * Dzi�ki temu rynek cho� troch� si� nakr�ca i od czasu do czasu
 * trzeba b�dzie wyda� par� grosz� na kolejny krzemie� czy lamp�.
 * Niszczenie si� jest zrobione osobno(!) w ubraniach(tj. zbrojach) i broniach.
 * Ten lib s�u�y wszelkim INNYM obiektom (np. plecaki, lampy, krzemienie, liny),
 * kt�re maj� si� niszczy�, a w efekcie obni�a� ich warto�� oraz ogranicza� 
 * funkcjonalno��.
 *
 * Przy robieniu rzeczy psuj�cej si�, pami�tajmy o:
 * W funkcji create_costam() :
 *     jak_szybko_sie_niszczy(15); //jak sama nazwa wskazuje :)
 *     niszczeje(); //i od razu przy �adowaniu obiektu troch� go niszczymy
 *
 *
 * Ponadto, przy ka�dej akcji z obiektem wywo�ujmy funkcje niszczeje();
 * A przy testach na funkcjonalno�� sprawdzamy if(zniszczona()) to jaki�
 * �adny notify_fail wypisujemy...
 *
 *
 * WYJA�NIENIE pr�dko�ci niszczenia: za ka�dym razem jak wywo�ujemy niszczeje()
 * to obiekt niszczy si� o tyle ile ustawili�my w jak_szybko_niszczeje (+1)
 * a zniszczony b�dzie wtedy, a� osi�gnie zdefiniowany LIMIT_ZNISZCZENIA (patrz kilka
 * linijek ni�ej)
 *
 * Pozdrawiam! :) Ku chwale Vatt'gherna!
 *
 * @author Vera
 * @date Wednesday 8 of July 2009
 */

#include <pl.h>
#include <macros.h>
#include <stdproperties.h>

//si�a mocy szata�skiej ;] czyli limit do ilu podliczamy 'popsute' �eby by�o naprawd� popsute ;]
#define LIMIT_ZNISZCZENIA 1000

// Zmienne globalne
private int     popsute = 0,      /* Jak bardzo co� jest popsiute :) */
                szybkosc_niszczenia = 5;

//Prototypy funkcji:
void niszczeje(); //wywo�ywane w funkcjach obiekt�w, np. przy goleniu si�
                  //brzytw�, wywo�ujemy to, i wtedy brzytwa troche niszczeje.
int zniszczona(); 
void jak_szybko_sie_niszczy(int x = 5); //przy ka�dym u�yciu funkcji niszczy si� (random z x)+1


int
procent_zniszczenia()
{
    return (popsute * 100) / LIMIT_ZNISZCZENIA;
}

void
jak_szybko_sie_niszczy(int x = 5)
{
    szybkosc_niszczenia = x;
}

/* funkcja zwraca 1 je�li obiekt powinien by� popsuty,
 * zero je�li jeszcze nie jest ;)
 */
int
zniszczona()
{
    if(popsute >= LIMIT_ZNISZCZENIA)
        return 1;
    else
        return 0;
}

void
set_popsute(int x)
{
    popsute = x;
}

void
niszczeje()
{
    popsute += random(szybkosc_niszczenia) + 1;
    
    //a tu ju� konsekwencje jakie wynikaj� ze zniszczenia...
    int val = TO->query_prop(OBJ_I_VALUE);
    switch(procent_zniszczenia())
    { //pami�tajmy, �e jeden case b�dzie kilka razy z rz�du wywo�ywany
      //bo np. 20% b�dzie podczas kilku wywo�a� niszczeje(), zanim b�dzie 21%
        case 20:
            TO->add_prop(OBJ_I_VALUE, val - val/15); break;
        case 50:
            TO->add_prop(OBJ_I_VALUE, val - val/13); break;
        case 65:
            TO->add_prop(OBJ_I_VALUE, val - val/8); break;
        case 80:
            TO->add_prop(OBJ_I_VALUE, val/5); break;
        case 100:
            TO->remove_prop(OBJ_I_VALUE); break;
    }
}

string
query_psuj_auto_load()
{
    return "#/LIB/PSUJ#" + popsute + "&&"+szybkosc_niszczenia+ "#/LIB/PSUJ#";
}

public void
init_psuj_arg(string arg)
{
    string reszt, nasze;
    int level, predkosc;
    sscanf(arg, "%s#/LIB/PSUJ#%s#/LIB/PSUJ#%s", reszt, nasze, reszt);
    level = atoi(explode(nasze,"&&")[0]);
    predkosc = atoi(explode(nasze,"&&")[1]);
    
    set_popsute(level);
    jak_szybko_sie_niszczy(predkosc);
}

/**FUNKCJA zer�ni�ta Z /std/armour.c. 
 * nawet opisy te same zostawi�em.
 * Funkcja zwraca opis stanu obiektu
 *
 * @return opis stanu obiektu
 */
string
stan_popsucia()
{
  string str;

  if (TO->query_prop(OBJ_I_BROKEN))
    return (TO->query_tylko_mn() ? "S^a" : "Jest") + " zupe^lnie zniszcz" +
      TO->koncowka("ony", "ona", "one", "eni", "one") + ".\n";

  switch(procent_zniszczenia())
  {
    case 0..19:
      str = "w znakomitym stanie";
      break;
    case 20..39:
      str = "lekko podniszcz" + TO->koncowka("ony", "ona", "one", "eni",
          "one");
      break;
    case 40..59:
      str = "w kiepskim stanie";
      break;
    case 60..79:
      str = "w op^lakanym stanie";
      break;
    default:
      str = "gotow" + TO->koncowka("y", "a", "e", "i", "e") +
        " si^e rozpa^s^c w ka^zdej chwili";
      break;
  }

  return "Wygl^ada na to, ^ze " + (TO->query_tylko_mn() ? "s^a" : "jest") +
    " " + str + ".\n";
}

int
query_psuj()
{
    return 1;
}
