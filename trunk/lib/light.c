/**
 * \file /lib/light.c
 *
 * Jest to biblioteka u�ywana w pochodniach i lampach, w chwili pisania tego
 * opisu tylko tam, ale by� mo�e kiedy� b�dzie u�ywana r�wnie� gdzie� indziej.
 *
 * @author Krun
 * @date Grudzie� 2007
 *
 * Plik wyodr�bniony z /std/torch.c z powodu uczynienia tego przedmiotu
 * kiepskiej jako�ci maczug�, podczas gdy lampa maczug�... baaaa....
 * nawet broni� by� nie mo�e.
 *
 * - Friday 03 of July 2009
 *    doda�em opcje nie ga�ni�cia pochodni/�agwi itepe typu O_SCIENNE
 *    je�li s� od�o�one w subloku,
 *    poza tym jeszcze par� drobnych poprawek do blokad zapalania/ga�ni�cia
 *    innych przedmiot�w. Obecnie: tylko pochodnia od�o�ona normalnie na lok.
 *    ga�nie/nie mo�e by� zapalona. Ponadto wszystkie inne przedmioty mog�
 *    by� zapalone je�li albo s� chwytane/albo w subloku jak lampy - przewieszone
 *    ostatecznie mog� by� te� od�o�one na lok.
 *    Vera.
 */

#include <pl.h>
#include <macros.h>
#include <stdproperties.h>
//do sprawdzania czy pochodnia jest typu O_SCIENNE,wtedy nie ga�nie od�o�ona w sublok
#include <object_types.h>

// Zmienne globalne
private int     Light_Value,    /* Maksymalna warto�� przedmiotu */
                Light_Strength, /* Moc �wiat�a */
                Time_Left;      /* Ile czasu jeszcze po�wieci */
static  int     Burn_Alarm,     /* Alarm u�ywany kiedy �wiat�o �wieci */
                Max_Time,       /* Maksymalny czas jaki mo�e po�wieci� �wiat�o */
                Drop_Alarm;     /* Alarm odpowiadaj�cy za zgaszenie po opuszczeniu */
private object *Fail;

//Prototypy funkcji:
public void set_strength(int strength);
public void set_value(int value);
public void set_time(int time);
public int light_value();
public void burned_out();

/**
 * Funkcja konfiguruj�ca �wiat�o.
 * @author Lil
 */
nomask void
config_light()
{
    this_object()->add_prop(OBJ_I_VALUE,  &light_value());
}

//Dla zgodno�ci z poptrzedni� wersj� pochodni
nomask void
configure_light()
{
    config_light();
}

/**
 * Funkcja sprawdza ile warte jest obecnie to �wiat�o.
 * @return cena za jak� mo�na obiekt spieni�y�.
 */
public int
light_value()
{
    int v;

    if (!Max_Time)
        return 0;

    if (Burn_Alarm && sizeof(get_alarm(Burn_Alarm)))
        v = ftoi(get_alarm(Burn_Alarm)[2]);
    else
        v = Time_Left;

    return (v * (Light_Value - 5)) / Max_Time;
}

/**
 * Funkcja odpowiedzialna za wy�wietlenie pomocy.
 */
string
query_pomoc()
{
    return "Nie baw si� ogniem!\n";
}

void
init_light()
{
}

/**
 * Dzi�ki tej funkcji ustawiamy jak d�ugo pochodnia mo�e si� pali�.
 * Ustawiamy tu zar�wno czas maksymalny jak i pozosta�y.
 *
 * @param time ile sekund b�dzie si� pali� pochodnia
 */
public void
set_time(int time)
{
    Max_Time = time;
    Time_Left = time;
}

/**
 * Dzi�ki tej funkcji ustawiamy jak d�ugo jeszcze pochodnia b�dzie si�
 * pali�. Nie zmieniamy ni� czasu maksymalnego.
 *
 * @param time ile sekund b�dzie si� jeszcze pali� pochodnia.
 */
public void
set_time_left(int time)
{
    Time_Left = min(time, Max_Time);

    /* If lit, then also update the alarm. */
    if (Burn_Alarm)
    {
        remove_alarm(Burn_Alarm);
        Burn_Alarm = set_alarm(itof(Time_Left), 0.0, burned_out);
    }
}

/**
 * Dzi�ki tej funkcji mo�emy zmieni� maksymalny czas p�oniecia pochodni nie
 * zmieniaj�c przy tym czasu jaki pozosta� do jej ca�kowitego wypalenia si�.
 *
 * @param time ile maksymalnie mo�e pali� si� pochodnia
 */
public void
set_max_time(int time)
{
    Max_Time = time;
}

/**
 * @return Maksymalny czas przez jaki pochodnia b�dzie p�on��.
 */
public int
query_max_time()
{
    return Max_Time;
}

public varargs int
query_time(int flag=0)
{
    mixed alarm;

    if(flag && Burn_Alarm && sizeof(alarm = get_alarm(Burn_Alarm)))
        return ftoi(alarm[2]);

    return Time_Left;
}

/**
 * Funkcj� t� ustawiamy moc �wiat�a dawanego przez przedmiot w kt�rym u�ywamy liba.
 *
 * @param strength Moc �wiat�a dawanego przez przedmiot.
 */
public void
set_strength(int strength)
{
    Light_Strength = strength;
}

/**
 * @return Zwraca moc �wiat�a.
 */
public int
query_strength()
{
    return Light_Strength;
}

/**
 * Dzi�ki tej funkcji dowiemy si� czy �wiat�o w tej chwili �wieci.
 * @param flag je�li to jest ustawione wtedy zwracamy id alarmu
 *             �wiecenia(lub 0 je�li takowego nie ma)
 *
 * @return 0        - je�li lampa zgaszona
 * @return 1        - je�li lampa za�wiecona
 * @return alarm id - je�li lampa za�wiecona i flaga ustawiona
 */
public int
query_lit(int flag)
{
    if (flag)
        return Burn_Alarm;
    else
        return (!Burn_Alarm ? 0 : -1);
}

/**
 * Dzi�ki tej funkcji ustawiamy maksymaln� warto��
 * �wiat�a, kiedy jest w pe�ni niewy�wiecone.
 * @param val maxumalna warto��
 */
public void
set_value(int val)
{
    Light_Value = val;
}

/**
 * @return maksymalna warto�� pochodni, u�ywan� wtedy kiedy jest ona niewypalona.
 */
public int
query_value()
{
    return Light_Value;
}

/**
 * Zapalamy �wiat�o, je�li podamy argument to zapalamy od czego�.
 * @param od_czego od czego zapalamy, 0 je�li krzemieniem
 */
public varargs mixed
light_me(object od_czego=0)
{
    if (environment(this_player())->query_prop(ROOM_I_TYPE) == ROOM_UNDER_WATER)
    {
        return "Jeste� w tej chwili pod wod�!\n";
    }

    if (!Time_Left)
        return "Pr^obujesz zapali^c " + TO->short(PL_BIE) + ", ale ci si^e nie "+
                "udaje... " + (TO->query_tylko_mn() ? "s^a" : "jest") +
                " bezu^zyteczn" + TO->koncowka("y", "a", "e", "i", "e") + ".\n";

    if (Burn_Alarm)
        return capitalize(TO->short(PL_MIA)) + " ju^z " +
                (TO->query_tylko_mn() ? "s^a" : "jest") + " zapal" +
                TO->koncowka("ony", "ona", "one", "eni", "one") + ".\n";

    
    //obiekt nie jest chwytany/dobywany
    if(!TO->query_wielded() && !TO->query_chwycony())
    {
	if(ENV(TO) == TP) //obiekt jest w inv gracza
	{
	    //obiekt to lampa, ale nie jest w jakim� subloku gracza (np. na ramieniu)
	    if(TO->query_name() == "lampa" && !TO->query_subloc())
		return "Musisz wpierw sobie przewiesi^c " + TO->koncowka("ten", "t^a", "to") +
		 " " + TO->query_nazwa(PL_BIE) + ".\n";
	    else if(TO->query_name() != "lampa") //obiekt to nie lampa, ale jest w naszym inv, nie dobywany/chwytany.
		return "Musisz wpierw chwyci^c " + TO->koncowka("ten", "t^a", "to") +
		" " + TO->query_nazwa(PL_BIE) + ".\n";
	}
	else if(ENV(TO) == ENV(TP)) //obiekt jest w env gracza
	{    //tylko pochodni nie mo�na zapala� z le��cych swobodnie na lok.
	    if(member_array(TO->query_name(),({"pochodnia","�agiew","�uczywo"})) != -1)
	    {
	        //ale mo�na je zapali� je�li s� w sublocu gdzie�
		if(!sizeof(TO->query_subloc()))
		    return "Musisz wpierw chwyci^c " + TO->koncowka("ten", "t^a", "to") +
		    " " + TO->query_nazwa(PL_BIE) + ".\n";
	    }
	}
    }
    
    if (!sizeof(filter(all_inventory(this_player()), &->krzemieniuj())) && !od_czego)
    {
        return "Nie masz czym wykrzesa^c ognia.\n";
    }

    //to dla obiekt�w z /lib/psuj.c
    if(TO->query_psuj())
    {
        TO->niszczeje(); //u�ywamy obiektu, niszczy si� wi�c.
    
        if(TO->zniszczona())
        {
            NF(TO->koncowka("Ten", "Ta", "To")+" "+TO->query_nazwa(PL_MIA)+" "+
               "jest ju� kompletnie zniszczon"+TO->koncowka("y","a","e")+
                " i nie nadaje si� do niczego.\n");
            return 0;
        }
    }
    

    if(od_czego)
    {
        saybb(QCIMIE(TP, PL_MIA) + " zapala " + QSHORT(TO, PL_BIE) + " od " + QSHORT(od_czego, PL_CEL) + ".\n");
        write("Zapalasz  " + TO->short(TP, PL_BIE) + " od " + od_czego->short(TP, PL_CEL) + ".\n");
    }
    else
    {
        saybb(QCIMIE(this_player(), PL_MIA) + " zapala " +
            QSHORT(this_object(), PL_BIE) + ".\n");
        write("Zapalasz " + TO->short(PL_BIE) + ".\n");
    }

    TO->dodaj_przym("zapalony", "zapaleni");

    TO->odmien_short();
    TO->odmien_plural_short();

    TO->add_prop(OBJ_I_HAS_FIRE, 1);
    TO->add_prop(OBJ_I_LIGHT, Light_Strength);

    //ujawniamy gracza je�li jest ukryty
    if(TP->query_prop(OBJ_I_HIDE))
        TP->command("ujawnij sie");
    
    Burn_Alarm = set_alarm(itof(Time_Left), 0.0, burned_out);
    return 1;
}

/**
 * Gasimy �wiat�o:)
 */
public int
extinguish_me()
{
    mixed   alarm;

    if (!Burn_Alarm || !sizeof(get_alarm(Burn_Alarm)))
    {
        Burn_Alarm = 0;
        return capitalize(TO->short(PL_MIA)) + " nie " + (TO->query_tylko_mn() ? "s^a" :
            "jest") + " zapal" + TO->koncowka("ony", "ona", "one", "eni", "one") +
            ".\n";
    }

    if (sizeof(alarm = get_alarm(Burn_Alarm)))
    {
        Time_Left = ftoi(get_alarm(Burn_Alarm)[2]);
        remove_alarm(Burn_Alarm);
    }
    Burn_Alarm = 0;

    saybb(QCIMIE(this_player(), PL_MIA) + " gasi " +
            QSHORT(this_object(), PL_BIE) + ".\n");
    write("Gasisz " + TO->short(PL_BIE) + ".\n");
    TO->remove_adj("zapalony");
    TO->odmien_short();
    TO->odmien_plural_short();

    TO->remove_prop(OBJ_I_LIGHT);
    TO->remove_prop(OBJ_I_HAS_FIRE);
    return 1;
}

/**
 * A to funkcja wywo�uj�ca si� w momencie kiedy nam si� �wiat�o wyczerpie.
 * (ps. w lampie jest ta funkcja nadpisana, a p�niej ta si� wywo�uje. v.
 */
public void
burned_out()
{
    object ob = environment(), env;

    Time_Left = 0;
    Burn_Alarm = 0;

    TO->remove_adj("zapalony");
   
    
    if(ob)
    {
        TO->odmien_short();
        TO->odmien_plural_short();

        if (living(ob))
        {
            ob->catch_msg(capitalize(TO->short(ob, PL_MIA)) +
                " wypala si^e i ga^snie.\n");

            if (env = environment(ob))
            {
                tell_roombb(env, QCSHORT(this_object(), PL_MIA) + " trzyman"+
                        TO->koncowka("y", "a", "e", "i", "e") + " przez "+
                        QIMIE(ob, PL_BIE) + " wypala si^e i ga^snie.\n", ({ob}), ob);
            }
    }
    else
        tell_roombb(ob, QCSHORT(this_object(), PL_MIA) +
            " wypala si^e i ga^snie.\n", ({}), this_object());
    }

    TO->dodaj_przym("wypalony", "wypaleni");
    TO->odmien_short();
    TO->odmien_plural_short();

    TO->remove_prop(OBJ_I_VALUE);
    TO->remove_prop(OBJ_I_LIGHT);
    TO->remove_prop(OBJ_I_HAS_FIRE);
}

/**
 * A ten kawa�eczek kodu, coby si� nam odpowiednio wsio zapisywa�o.
 */
public string
query_light_auto_load()
{
    int tmp;

    if (Burn_Alarm && sizeof(get_alarm(Burn_Alarm)))
        tmp = ftoi(get_alarm(Burn_Alarm)[2]);
    else
        tmp = Time_Left;

    return "#t_t#" + tmp + "#t_l#" + TO->query_prop(OBJ_I_LIGHT) + "#";
}

/**
 * A to coby si� odczytywa�o pi�knie
 */
public string
init_light_arg(string arg)
{
    string foobar, toReturn;
    int tmp;

    if (arg == 0)
        return 0;

    sscanf(arg, "%s#t_t#%d#%s", foobar, Time_Left, foobar);
    sscanf(arg, "%s#t_l#%d#%s", foobar, tmp, toReturn);

    if (Time_Left == 0)
    {
        TO->dodaj_przym("wypalony", "wypaleni");
        TO->odmien_short();
        TO->odmien_plural_short();
        TO->remove_prop(OBJ_I_VALUE);
        return toReturn;
    }
    if (tmp > 0)
    {
        TO->add_prop(OBJ_I_LIGHT, tmp);
        TO->add_prop(OBJ_I_HAS_FIRE, 1);
        TO->dodaj_przym("zapalony", "zapaleni");
        TO->odmien_short();
        TO->odmien_plural_short();
        Burn_Alarm = set_alarm(itof(Time_Left), 0.0, burned_out);
        config_light();
    }
    return toReturn;
}

int
zgasnij_no()
{
    if (!TO->query_wielded() && query_lit(1))
    {
        extinguish_me();
        tell_room(environment(this_object()), capitalize(TO->short(PL_MIA)) + " ga^snie.\n");
    }
}

void
unwield_light()
{
    if (Drop_Alarm)
        remove_alarm(Drop_Alarm);

    Drop_Alarm = set_alarm(5.0, 0.0, "zgasnij_no");
}

varargs public void
move_light()
{
      //pochodnie, typu sciennego nie gasn� w sublocu
    if(member_array(TO->query_name(), ({"pochodnia","�agiew","�uczywo"})) != -1 &&
	TO->query_subloc() && TO->query_type() == O_SCIENNE)
	return;
  
    if (Drop_Alarm)
        remove_alarm(Drop_Alarm);

    Drop_Alarm = set_alarm(5.0, 0.0, "zgasnij_no");
}