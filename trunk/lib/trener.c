/*
 * plik dodaj�cy mozliwo�� trenowania umiej�tno�ci przez npcy
 *
 *
 * Mini tutorial jak zrobi� trenera:
 * trzeba po primo inheritowa� ten plik :P
 * p�niej w npcku dajemy co� takiego:
 
 na pocz�tku inicjujemy, np tak:
 void
init()
{
    ::init();
    init_trener();
}

 
 dodaj_trening(SS_FISHING, 11053, 6800, 45, 0, 70, ({"event1\n","event2\n","event3\n"}));
 gdzie kolejne argumenty to: int um (np. SS_SWIM),, int cena (podstawa cenowa),, int jako��
 int max. poziom wyuczenia,, int min. poziom wyuczenia potrzebny do korzystania z treningu
 int czas trenowania, *string tablica nastepuj�cych po sobie kolejno event�w podczas trenowania
 
 Poza tym w funkcji attacked_by, jak jest zaatakowany po raz pierwszy to odpalamy mu tam funkcje:
 reakcja_trenera_na_walke(object wrog)
 
 Pami�tajmy te� o tym, by doda� (lub �wiadomie nie dodawa�) odpowiedzi na pytania o treningi/nauki itepe.
 
 Uwaga: max. 1 trener powinien by� na lokacji
 
 Dodatkowa opcja: jest co� takiego jak blokada trenowania. Np. je�li chcemy, �eby npc udziela� trening�w tylko tym,
 co wykonali jakiego� questa czy co�. Wtedy dajemy w npcku funkcj�: jest_blokada_na_treningi() i je�li jest blokada
 zwracamy oczywi�cie 1. Je�li ju� blokada zdj�ta dla gracza, to 0. Pami�tajmy wtedy te� o odpowiednim komunikacie, gdy
 jest blokada.
 
 
                    Vera. 8 czerwca 2010. Zrobione na podst. pergamin�w do trenowania.
 */

inherit "/lib/training.c";

#include <pl.h>
#include <macros.h>
#include <cmdparse.h>
#include <composite.h>
#include <language.h>
#include <ss_types.h>

#define TR_SS_N      0
#define TR_CENA      1
#define TR_JAKOSC    2
#define TR_MAX       3
#define TR_MIN       4
#define TR_TIME      5
#define TR_EVENTY    6

int trenuj(string str);

private static mixed treningi;
int ostatni_event = 0;
int cel = -1;
int czas_rozpoczecia_treningu = 0;
int czy_walcze = 0;
object trenujacy;
string *eventy = ({ });


void
init_trener()
{
    config_default_trade();
    add_action(trenuj,      "trenuj");
}

/*
 * dodajemy opcje trenowania, argumenty (podobne jak do pergamin�w):
 * - int um (np. SS_SWIM)
 * - int cena (podstawa cenowa)
 * - int jako��
 * - int max. poziom wyuczenia
 * - int min. poziom wyuczenia potrzebny do korzystania z treningu
 * - int czas trenowania
 * - *string tablica nastepuj�cych po sobie kolejno event�w podczas trenowania
 */
void
dodaj_trening(int um, int cena, int jakosc, int max_limit, int min_limit, int czas, string *eventy)
{
    if(!pointerp(treningi))
        treningi = ({});

    treningi += ({ ({ um, cena, jakosc, max_limit, min_limit, czas, eventy }) });
    
}

int
pokaz_liste()
{
    TP->catch_msg("\nOto lista teoretycznych nauk, jakie ma tobie do zaoferowania "+QSHORT(TO,PL_MIA)+".\n");
    
    for(int i = 0; i < sizeof(treningi); i++)
        write(sprintf("%3s %35s %36s\n", (i+1)+".",
            SS_SKILL_DESC[treningi[i][TR_SS_N]][0],
            pokaz_cene(sprawdz_cene(treningi[i][TR_SS_N], treningi[i][TR_CENA], treningi[i][TR_MIN],
                treningi[i][TR_MAX], TP))));
                
    return 1;
}


public void
eventy_treningu()
{
    if(!sizeof(eventy)) //wtf? :P
        trenujacy->catch_msg("Uczysz si�.\n");
    else
    {
        trenujacy->catch_msg(eventy[ostatni_event]);
        
        if(random(2)) //niech nie flooduje za bardzo nudnym komunikatem
            saybb(QCIMIE(TP,PL_MIA)+" odbiera nauki od "+QSHORT(TO,PL_DOP)+".\n");
    }
    
    ostatni_event++;
}

public void
koniec_treningu()
{
    int ile_expa, roznica,
        czas_czytania = treningi[cel][TR_TIME],
        jakosc= treningi[cel][TR_JAKOSC];
    //Jesli przestalismy to dostaniemy tylko czesc expa.

    if((roznica = time() - czas_rozpoczecia_treningu) < czas_czytania)
    {
        ile_expa = ftoi(itof(jakosc) * (itof(roznica)/itof(czas_czytania)));
        treningi[cel][TR_TIME] -= roznica;
        jakosc = jakosc - ile_expa;
    }
    else
    {
        ile_expa = jakosc;
    }

    int dodano = dodaj_expa(treningi[cel][TR_SS_N], TP, ile_expa, treningi[cel][TR_MIN], treningi[cel][TR_MAX]);
    
    trenujacy = 0;
    czas_rozpoczecia_treningu = 0;
    cel = -1;
    ostatni_event = 0;
    
    write("Ko�czysz nauk�" + (dodano ? ". Posiad�"+TP->koncowka("e�","a�")+" troch� wiedzy o " +
        SS_SKILL_DESC[treningi[cel][TR_SS_N]][2] : "") + ".\n");

    saybb(QCIMIE(TP, PL_MIA) + " ko�czy nauk�.\n");
    
}

static private void dodaj_paraliz(object ob)
{
    object paraliz = clone_object(PARALYZE_OBJECT);

    paraliz->set_name("paraliz_uczenia_sie");
    paraliz->set_fail_message("W tym momencie zajmujesz si� nauk� umiej�tno�ci.\n"+
        "Nie mo�esz robi� nic innego.\n");

    paraliz->set_stop_message("");
    paraliz->set_stop_fun("koniec_treningu");
    paraliz->set_stop_verb("przesta�");
    paraliz->set_stop_object(TO);

    paraliz->set_event_time(10.0 + frandom(15, 2));
    paraliz->set_event_fun("eventy_treningu");
    paraliz->set_event_object(TO);

    paraliz->set_finish_fun("koniec_treningu");
    czas_rozpoczecia_treningu = time();
    paraliz->set_finish_object(TO);
    paraliz->set_remove_time(treningi[cel][TR_TIME]);

    paraliz->move(TP, 1);
}


int
trenuj(string str)
{       
    if(!sizeof(treningi))
    {
        notify_fail("S�ucham?\n");
        return 0;
    }
    
    if(TO->query_attack())
    {
        write("To nie jest odpowiedni czas na nauki.\n");
        return 1;
    }
    
    //blokada - w npcu funkcja, bo np. gracz nie wype�ni� jakiego� questa, wi�c nie ma mo�liwo�ci treningu
    if(TO->jest_blokada_na_treningi()) //w blokadzie trzeba pami�ta� o jakim� komunikacie.
        return 1;
    
    if(!str)
    {
        pokaz_liste();
        return 1;
    }
    
    cel = -1; //um kt�r� chcemy trenowa�
    
    for(int i = 0; sizeof(treningi) > i; i++)
        if(str ~= SS_SKILL_DESC[treningi[i][0]][0])
            cel = i;    
    
    if(cel == -1)
    {
        write("Co chcesz trenowa�?\n");
        return 1;
    }
    
    //je�li trener w�asnie kogo� trenuje, to te� nie mo�e
    if(objectp(trenujacy))
    {
        write(QCSHORT(TO,PL_MIA)+" w�a�nie jest zaj�ty trenowaniem. Zaczekaj chwil�.\n");
        return 1;
    }
    
    //sprawdzanie czy przypadkiem nie jestesmy 'zbyt niedoswiadczeni' (wynik: -1)
    //lub 'zbyt doswiadczeni' (wynik: -2). Verk.
    if(sprawdz_cene(treningi[cel][TR_SS_N], treningi[cel][TR_CENA],
                treningi[cel][TR_MIN], treningi[cel][TR_MAX], TP) == -1)
    {
        write("Musisz si� najpierw troch� wi�cej podszkoli� gdzie indziej.\n");
        return 1;
    }   
    if(sprawdz_cene(treningi[cel][TR_SS_N], treningi[cel][TR_CENA],
                treningi[cel][TR_MIN], treningi[cel][TR_MAX], TP) == -2)
    {
        write("Posiad�"+TP->koncowka("e�","a�")+" ju� t� wiedz�.\n");
        return 1;
    }   
    
    eventy = treningi[cel][TR_EVENTY];
    trenujacy = TP;
        
    TP->catch_msg(QCSHORT(TO, PL_MIA)+ " rozpoczyna udzielanie ci lekcji w umiej�tno�ci " + SS_SKILL_DESC[treningi[cel][0]][2] + ".\n");
    saybb(QSHORT(TP, PL_MIA) + " rozpoczyna udzielanie " + QIMIE(TP, PL_DOP) + " lekcji w umiej�tno�ci " + SS_SKILL_DESC[treningi[cel][0]][2] + ".\n");


    // dodajemy parali�
    dodaj_paraliz(TP);
    
    return 1;
}

//zaatakowany trener przestaje trenowa�
void
reakcja_trenera_na_walke(object wrog = 0)
{
    //ta funkcja wywo�uje si� z attacked_by w npcku. A �e ona si� wywoluje za ka�dym ciosem, to chcemy, �eby wesz�a tylko raz
    //i dodajemy dalej alarm na sprawdzanie czy jeszcze walka (i tym samym blokada trening�w).
    if(!czy_walcze)
    {
        //if w�a�nie trenuje:
        if(objectp(trenujacy))
        {
            trenujacy->catch_msg(QCSHORT(TO,PL_MIA)+" przerywa nauki.\n");
            koniec_treningu();
        }
    }
        
    if(TO->query_attack())   
    {    
        czy_walcze = 1;
        set_alarm(8.0, 0.0, "reakcja_trenera_na_walke", wrog);
    }
    else
        czy_walcze = 0;
    
}

