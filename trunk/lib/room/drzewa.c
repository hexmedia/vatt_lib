/**
 * \file /lib/room/drzewa.c
 *
 *  Pliczek odpowiedzialny za dodawanie i odnawianie
 *  drzew na lokacji - by Rantaur
 *
 *  Kurde.. Kto� kiedy� b�dzie zachowywa� po��dek w driverze???
 *  To nie jest standard tylko lib... standardy to tylko te pliki
 *  kt�re maj� w sobie funkcje create_costam! [Krun]
 *
 *  Przenios�em do liba, podobnie zrobi�em z zio�ami... Pozatym teraz
 *  plik jest automatycznie inheritowany i wywo�ywany w ka�dym roomie o
 *  typie ROOM_FOREST
 *
 * @author Rantaur
 * @author Krun
 */

#pragma strict_types

#include <files.h>
#include <macros.h>
#include <mudtime.h>

#define TREES_DIR "/d/Standard/drzewa/"

string *lista_drzew;
int liczba_drzew=random(5)+3;

void odnow_drzewa();
void ustaw_liste_drzew(string *s);

// Ustawia liczbe drzew na lokacji
void ustaw_liczbe_drzew(int i)
{
    liczba_drzew = i;
}

int query_liczba_drzew()
{
    return liczba_drzew;
}

/* Ustawia liste drzew jakie moga rosnac na lokacji;
   podajemy nazwy drzew - "buk", "brzoza" itp. */
void ustaw_liste_drzew(string *s)
{
    lista_drzew = s;
}

string *query_lista_drzew()
{
    return lista_drzew;
}

int
filter_uncuted_trees(object ob)
{
    if(function_exists("create_object", ob) == "/std/drzewo")
    {
        if(ob->query_sciete())
            return 0;

        return 1;
    }

    return 0;
}

int
filter_cuted_trees(object ob)
{
    if(function_exists("create_object", ob) == "/std/drzewo")
    {
        if(ob->query_sciete())
            return 1;

        return 0;
    }

    return 0;
}

// Zwraca sciezke do drzewa o nazwie podanej w argumencie
string query_tree_path(string str)
{
    string ret = TREES_DIR;
    ret += lower_case(str);
    ret += "/"+lower_case(str)+".c";

    return ret;
}

// Odnawia drzewa na lokacji
void odnow_drzewa()
{
    if(!sizeof(lista_drzew))
        return;

    object *inv = all_inventory(this_object());
    object *drzewa = filter(inv, &filter_uncuted_trees());
    object *drzewa_sciete = filter(inv, &filter_cuted_trees());

    int do_dodania=liczba_drzew-sizeof(drzewa);
    int lista_size = sizeof(lista_drzew);

    string sciezka;
    object drzewo;
    for(int i=0;i < do_dodania;i++)
    {
        sciezka = query_tree_path(lista_drzew[random(lista_size)]);
        drzewo = clone_object(sciezka);
        drzewo->move(this_object());
    }

    if(sizeof(drzewa_sciete) > 0)
    {
        drzewa_sciete->move("/d/Standard/Redania/Rinde_okolice/Las/lokacje/tartak_magazyn");
        drzewa_sciete->move(TO);
    }
}

/**
 * Ta funkcja pozwala na zbadanie wyr�bu.
 * Funkcja inicjuje alarm, kt�ry jest nak�adany na gracza, po jego up�ywie graczowi
 * wy�wietlany jest ko�cowy komunikat badania wyr�bu.
 *
 * @param who   -   osoba badaj�ca wyr�b
 */
public int zbadaj_wyreb(object who)
{
    if(!TO->query_lista_drzew())
        return 0;

    object paraliz = clone_object(PARALYZE_OBJECT);

    paraliz->set_remove_time(3.0 + frandom(3.0, 3));
    paraliz->set_finish_fun("koniec_badania_wyrebu", who);
    paraliz->set_finish_object(TO);
    paraliz->set_fail_message("W tej chwili badasz wyr�b przesta� aby to zrobi�.\n");

    paraliz->set_stop_verb("przesta�");
    paraliz->set_stop_fun("zaprzestanie_badania_wyrebu", who);
    paraliz->set_stop_object(TO);

    paraliz->move(who, 1);

    write("Zaczynasz bada� wyr�b.\n");
    tell_roombb(TO, QCIMIE(TP, PL_MIA) + " zaczyna bada� wyr�b.\n", ({who}));

    return 1;
}

/**
 * Ta funkcja odpowiada za przepisowe zako�czenie badania wyr�bu, czyli za zako�czenie po odpowiednim czasie.
 * @param who osoba kt�ra bada�a wyr�b.
 */
public void koniec_badania_wyrebu(object who)
{
    //Usuwamy parali�, gdyby przypadkiem si� nie usuno�.
    previous_object()->remove_object();

    object *drzewa = filter(AIE(TP), &->is_drzewo());

    if(!sizeof(drzewa))
    {
        write("W chwili obecnej nie ma tu drzew nadaj�cych si� do �cinki.\n");
        return;
    }

    //A teraz musimy troche sprawdzi� gracza, bo je�li ma drwalstwo na wysokim poziomie, to b�dzie mu �atwo okre�li� cen� za jak�
    //mo�e sprzeda� dane drzewo a tym samym okre�li� drzewo, kt�rego �ci�cie najbardziej mu si� op�aci.

    write("Jest tu " + (sizeof(drzewa) == 1 ? "jedno" : (sizeof(drzewa) == 2 ? "dwa" : "kilka")) + " " +
        "drzew" + (sizeof(drzewa) == 1 ? "o" : "") + ", kt�re twoim zdaniem nadaj� si� do �ci�cia:\n");

    for(int i = 0; i < sizeof(drzewa); i++)
    {
        write((i+1) + ". " + capitalize(drzewa[i]->short(who, PL_MIA)) + ", kt�r" + drzewa[i]->koncowka("y", "a", "e") +
            " wa�y " + drzewa[i]->appraise_weight(0) + ", za� " + drzewa[i]->koncowka("jego", "jej") + " warto�� szacujesz na " +
            drzewa[i]->appraise_tree_value(0) + ".\n");
    }
}
