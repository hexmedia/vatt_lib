/**
 * \file /lib/kwiat.c
 *
 * Plik ma za zadanie dodac do ziol/kwiatow
 * kilka akcji, jak np. wplatanie we wlosy,
 * plecienie wiankow itp.
 *
 * Standardowe wywo�anie:
 *
 *      inherit "/lib/kwiat.c"
 *
 *      void init()
 *      {
 *          ::init();
 *          init_kwiat();
 *      }
 */

#include <cmdparse.h>
#include <macros.h>
#include <pl.h>
#include <stdproperties.h>

#define SUBLOC           "kwiaty_we_wlosach"


int	    wplatalny = 1;		/* Czy rosline mozna wplesc sobie we wlosy */
mixed   wianek = 0;	        /* Czy z rosliny mozna zrobic wianek
                               jest mozna to zmienna zawiera sciezke
                               do obiektu wianka */
int     czy_wpleciony;      /* Czy w danej chwili roslina jest wpleciona
                               we wlosy */


string  *to_remove = ({ }); /* Tablica kwiatkow do usuniecia */

void set_wplatalny(int i)
{
    wplatalny = i;
}

int query_wplatalny()
{
    return wplatalny;
}

void set_wianek(string str)
{
    wianek = str;
}

mixed query_wianek()
{
    return wianek;
}

void set_czy_wpleciony(int i)
{
    czy_wpleciony = i;
}

int query_czy_wpleciony()
{
    return czy_wpleciony;
}

void upleciony(object player)
{
    player->catch_msg("Uplot�"+player->koncowka("e�", "a�")+" wianek z "
            +to_remove[0]->plural_short(PL_DOP)+".\n");

    saybb(QCIMIE(player, PL_MIA)+" upl"+player->koncowka("�t�", "ot�a")
            +" wianek z "+to_remove[0]->plural_short(PL_DOP)+".\n", ({player}));

    clone_object(wianek)->move(player);

    to_remove->remove_object();
}

string filter_kind()
{
    return MASTER;
}

int uplec(string str)
{
    notify_fail("Co i z czego chcesz uplesc?\n");

    if(!str)
	    return 0;

    mixed *z_czego;
    if(!parse_command(lower_case(str), TP, " 'wianek' 'z' %i:"+PL_DOP, z_czego))
	    return 0;

    /* Jesli zdarzy sie ze dopelniacz liczby pojedynczej i mnogiej
     * jest taki sam, to bierzemy liczbe mnoga.
     */
    if(z_czego[0][1]->query_nazwa(PL_DOP) ~= z_czego[0][1]->query_pnazwa(PL_DOP))
        z_czego[0][0] = 0;

    z_czego = NORMAL_ACCESS(z_czego, 0, 0);

    int size = sizeof(z_czego);

    if(!size || !z_czego)
	    return 0;

    /* Sprawdzamy czy wszystkie rosliny sa jednakowe - rozwiazanie
     * raczej tymczasowe */

    object *gatunki = unique_array(z_czego, &->filter_kind(), 0);

    if(sizeof(gatunki) > 1)
    {
        notify_fail("Narazie mo�esz ple�� wianek tylko z jednego gatunku"
                +" kwiat�w.\n", 2);
        return 0;
    }

    if(!(z_czego[0]->query_wianek()))
    {
        notify_fail("Niestety, "+z_czego[0]->short(PL_MIA)
                    +" nie nadaje si� na wianek.\n");
        return 0;
    }

    if(size == 1)
    {
	    notify_fail("Uplesc wianek tylko z jednej rosliny? Nie...to byt ambitne.\n");
	    return 0;
    }

    if(size < 18)
    {
	    notify_fail("Musisz postarac sie o wiecej takich roslin, jesli chcesz"
		            +" uplesc z nich wianek.\n");
	    return 0;
    }

    if(!HAS_FREE_HANDS(TP))
    {
        notify_fail("Aby zacz�� ple�� wianek musisz mie� wolne r�ce.\n");
        return 0;
    }

    TP->catch_msg("Zaczynasz ple�� wianek z "+z_czego[0]->plural_short(PL_DOP)+".\n");
    saybb(QCIMIE(TP, PL_MIA)+" zaczyna ple�� wianek z "+z_czego[0]->plural_short(PL_DOP)
            +".\n");

    setuid();
    seteuid(getuid());

    object paraliz = clone_object("/std/paralyze");
    paraliz->set_standard_paralyze("ple��");
    paraliz->set_remove_time(20+random(5));

    paraliz->set_stop_verb("przesta�");
    paraliz->set_stop_message("Przestajesz ple�� wianek.\n");

    paraliz->set_fail_message("Pleciesz teraz wianek! Aby zrobi� co� innego"
                             +" musisz najpierw 'przesta�'.\n");

    paraliz->set_finish_fun("upleciony");
    paraliz->set_finish_object(TO);

    paraliz->move(TP);

    to_remove = z_czego;

    return 1;
}

void wplec_mnie(object komu)
{
    TO->move(komu, SUBLOC);
    TO->set_obj_subloc(SUBLOC);
    TO->set_czy_wpleciony(1);
    TO->add_prop(OBJ_M_NO_DROP, "Najpierw wyjmij "+TO->koncowka("go", "j�", "je")
                                +" z w�os�w!\n");
    TO->add_prop(OBJ_M_NO_GIVE, "Najpierw wyjmij "+TO->koncowka("go", "j�", "je")
                                +" z w�os�w!\n");
    TO->add_prop(OBJ_M_NO_INS, "Najpierw wyjmij "+TO->koncowka("go", "j�", "je")
                                +" z w�os�w!\n");
}

int
wplec(string str)
{
    string bezokol;

    if(query_verb() ~= "wple�")
        bezokol = "wple��";
    else
        bezokol = "wsun��";

    notify_fail("Co chcesz "+bezokol+" sobie we w�osy?\n");

    if(!str)
        return 0;

    object *kwiaty;
    if(!parse_command(lower_case(str), TP, " %i:"+PL_BIE+" 'we' 'w�osy'", kwiaty))
        return 0;

    kwiaty = NORMAL_ACCESS(kwiaty, 0, 0);

    int size = sizeof(kwiaty);

    if(!size || !kwiaty)
        return 0;

    /* Sprawdzamy czy wszystkie kwiatki sa wplatalne */
    for(int i = 0; i < size; i++)
    {
        if(!(kwiaty[i]->query_wplatalny()))
        {
            notify_fail("Niestety, "+kwiaty[i]->short(PL_DOP)+" nie mo�esz "+bezokol
                    +" sobie we w�osy.\n", 2);
            return 0;
        }

        /* Jesli jakis kwiat jest juz wpleciony, to usuwamy go z tablicy */
        if(kwiaty[i]->query_czy_wpleciony())
            kwiaty -= ({ kwiaty[i] });
    }

    /* Jeszcze raz sprawdzamy czy gracz ma co wple�� */
    if(!sizeof(kwiaty))
    {
        string forma_juz_ma;

        if(size == 1)
            forma_juz_ma = "ten kwiat";
        else
            forma_juz_ma = "te kwiaty";

        notify_fail("Ale� "+forma_juz_ma+" masz juz we w�osach!\n");
        return 0;
    }

    /* Je�li mamy juz maksymalna ilosc kwiatow we wlosach... */
    int ile_wplecionych = sizeof(TP->subinventory(SUBLOC));
    if(ile_wplecionych >= 10)
    {
        notify_fail("Twoja g�owa to nie ogr�dek! Wystarczy ju� tych kwiat�w.\n", 2);
        return 0;
    }

    /* Jesli po wplecieniu przekroczylibysmy maksymalna ilosc */
    if(sizeof(kwiaty)+ile_wplecionych > 10)
    {
        notify_fail("Twoja g�owa to nie ogr�dek! Spr�buj "+bezokol
                +" nieco mniej kwiat�w.\n", 2);
        return 0;
    }

    /* Sprawdzamy dl. wlosow */
    float wloski = TP->query_dlugosc_wlosow();

    if(wloski <= 3.0)
    {
        notify_fail("Przecie� nawet nie masz w�os�w!\n", 2);
        return 0;
    }

    if(wloski < 11.0)
    {
        notify_fail("Niestety, masz za kr�tkie w�osy.\n", 2);
        return 0;
    }

    if(!(HAS_FREE_HANDS(TP)))
    {
        notify_fail("Aby wple�� cokolwiek we w�osy musisz mie� wolne r�ce.\n", 2);
        return 0;
    }

    write("Wplatasz sobie " + COMPOSITE_DEAD(kwiaty, PL_BIE) + " we w�osy.\n");
    saybb(QCIMIE(TP, PL_MIA) + " wplata sobie " + COMPOSITE_DEAD(kwiaty, PL_BIE)
          + " we w�osy.\n");

    kwiaty->wplec_mnie(TP);

    return 1;
}

void wyjmij_mnie()
{
    TO->set_czy_wpleciony(0);
    TO->set_obj_subloc(0);
    TO->remove_prop(OBJ_M_NO_DROP);
    TO->remove_prop(OBJ_M_NO_GIVE);
    TO->remove_prop(OBJ_M_NO_INS);
}

int
wyjmij(string str)
{
    notify_fail("Wyjmij co sk�d?\n");

    if(!str)
        return 0;

    object *kwiaty = TP->subinventory(SUBLOC);

    if(!sizeof(kwiaty))
    {
        notify_fail("Ale� nie masz �adnych kwiat�w we w�osach!\n", 2);
    }

    object *do_wyjecia;
    if(!parse_command(str, kwiaty, " %i:"+PL_BIE+" 'z' 'w�os�w'", do_wyjecia))
        return 0;

    do_wyjecia = NORMAL_ACCESS(do_wyjecia, 0, 0);

    if(!sizeof(do_wyjecia) || !do_wyjecia)
        return 0;

    do_wyjecia->wyjmij_mnie();

    write("Wyjmujesz "+COMPOSITE_DEAD(do_wyjecia, PL_BIE)+ " z w�os�w.\n");
    saybb(QCIMIE(TP, PL_MIA) + " wyjmuje "+COMPOSITE_DEAD(do_wyjecia, PL_BIE)
          + " z w�os�w.\n");

    return 1;
}

void
init_kwiat()
{
    add_action("uplec", "uple�");
    add_action("wplec", "wple�");
    add_action("wplec", "wsu�");
    add_action("wyjmij", "wyjmij");
}

