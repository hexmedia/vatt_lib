/**
 * \file /lib/kowal.c
 *
 * Do dziedziczenia przez npca.
 *
 * W standardowym create_monster() nalezy dla kazdego przedmiotu
 * z asortymentu kowala uzyc funkcji dodaj_przedmiot, jako argument
 * podajac pelna sciezke do pliku z przedmiotem.
 * Po dodaniu przedmiotow, nalezy wywolac funkcje create_kowal();
 *
 * Npc powinien takze wywolywac funkcje init_kowal() w init():
 *
 *  void
 *  init()
 *  {
 *      ::init();
 *      init_kowal();
 *  }
 *
 * @author Jeremian
 */
#pragma strict_types
#pragma save_binary

inherit "/lib/trade";

#include <stdproperties.h>
#include <macros.h>
#include <language.h>
#include <cmdparse.h>
#include <composite.h>
#include <filter_funs.h>
#include <ss_types.h>

// To tylko proba, moze cos z tego bedzie..

#define K_WOLNY  0; // Typy zlecen
#define K_ZAMOW  1;
#define K_NAPRAW 2;

string klient;      // Ten kto zleca

object przedmiot;   // Przedmiot, nad ktorym kowal aktualnie pracuje
int zlecenie;       // Aktualnie wykonywane zlecenie

string *lista = ({});
		    // Przedmioty, ktore kowal wykonuje (sciezka + nazwa pliku)
object *lista2 = ({});
		    // To samo, tylko po sklonowaniu (taka lista wzorcowa..)

mixed gotowe = ({});
		    // Przedmioty, ktore wlasciciel moze juz odebrac

int zamow(string str);
int napraw(string str);
int do_sell(string str);
int do_value(string str);
int do_show(string str);
int do_list(string str);
int armour_filter(object ob);
int weapon_filter(object ob);
void zrobione();
void odbierz();

/*
 *  Dodaje przedmiot do asortymentu kowala.
 */
void
dodaj_przedmiot(string str)
{
    lista += ({ str });
}

void
create_kowal()
{
    int i;
    object ob;

    if (!sizeof(lista))
	return ;

    for (i = 0; i < sizeof(lista); i++)
    {
	ob = clone_object(lista[i]);
	if (ob) lista2 += ({ ob });
    }
}

int
kowal_hook_odbierz(object ob)
{
    saybb(QCIMIE(this_object(), PL_MIA) + " daje " +
	QIMIE(this_player(), PL_CEL) + " " + ob->short(PL_BIE) + ".\n");
    write(this_object()->query_Imie(this_player(), PL_MIA) + " daje ci " +
	ob->short(PL_BIE) + ".\n");
    return 1;
}

int
kowal_hook_zamow(object ob)
{
    saybb(QCIMIE(this_player(), PL_MIA) + " zamawia co� u " +
	QIMIE(this_object(), PL_DOP) + ".\n");
    write("Zamawiasz " + ob->short(PL_BIE) + " u " +
	this_object()->query_imie(this_player(), PL_DOP) + ".\n");
    return 1;
}

int
kowal_hook_napraw(object ob)
{
    saybb(QCIMIE(this_player(), PL_MIA) + " daje " +
	QIMIE(this_object(), PL_CEL) + " " + ob->short(PL_BIE) +
	" do naprawy.\n");
    write("Dajesz " + this_object()->query_imie(this_player(), PL_CEL) + " " +
	ob->short(PL_BIE) + " do naprawy.\n");
    return 1;
}

int
kowal_hook_zajety()
{
    write(this_object()->query_Imie(this_player(), PL_MIA) +
	" jest w tej chwili zaj�t" + this_object()->koncowka("y", "a") +
	" prac�.\n");
    return 1;
}

/*
 * Function name: kowal_hook_allow_sell
 * Description:	  If you want to do more testing on objects the player intend
 *		  to sell.
 * Argument:	  ob - The object player wants to sell
 * Returns:	  1 if we allow player to sell the object (default)
 */
int
kowal_hook_allow_sell(object ob)
{
    // Przyjmuje tylko bronie lub zbroje
    return (weapon_filter(ob) || armour_filter(ob));
}

/*
 * Function name: kowal_hook_sell_no_match
 * Description:   Hook that is called when players tried to sell something
 *		  but we couldn't understand what he wanted to sell
 * Arguments:	  str - The string player tried to sell
 * Returns:	  0
 */
int
kowal_hook_sell_no_match(string str)
{
    notify_fail("Co takiego chcesz sprzeda�?\n");
    return 0;
}

/*
 * Function name: kowal_hook_sold_items
 * Description:   Hook that is called when player sold something
 * Arguments:	  item - The item array player sold
 * Returns:	  1
 */
int
kowal_hook_sold_items(object *item)
{
    write("Sprzedajesz " + COMPOSITE_DEAD(item, PL_BIE) + ".\n");
    saybb(QCIMIE(this_player(), PL_MIA) + " sprzedaje " + QCOMPDEAD(PL_BIE) +
          ".\n");
    return 1;
}

/*
 * Function name: kowal_hook_sold_nothing
 * Description:   Function called if player sold nothing with sell all
 * Returns:	  0
 */
int
kowal_hook_sold_nothing()
{
    notify_fail("Nic nie sprzeda�" + this_player()->koncowka("e�", "a�") +
        ".\n");
    return 0;
}

/*
 * Function name: kowal_hook_sell_no_value
 * Description:   Called if object has no value
 * Arguments:     ob - The object
 */
void
kowal_hook_sell_no_value(object ob)
{
    notify_fail(capitalize(ob->short(PL_MIA)) + " nie ma" +
        (ob->query_tylko_mn() ? "j�" : "") + " �adnej warto�ci.\n");
}

/*
 * Function name: kowal_hook_sell_worn_or_wielded
 * Description:   If object is worn or wielded and player has not said he
 *		  wants to sell such an item
 * Arguments:	  ob - The object
 */
void
kowal_hook_sell_worn_or_wielded(object ob)
{
    notify_fail("Chyba nie chcesz sprzeda� rzeczy, kt�rej dobywasz, lub "+
        "kt�r� masz aktualnie na sobie.\n");
}

/*
 * Function name: kowal_hook_sell_no_sell
 * Description:   An object has the no sell prop set (OBJ_M_NO_SELL)
 * Arguments:	  ob  - The object
 *		  str - Set if object has an own not sell string
 */
void
kowal_hook_sell_no_sell(object ob, string str)
{
    if (stringp(str))
	notify_fail(str);
    else
	notify_fail("Nie chce " + ob->short(PL_DOP) + ".\n");
}

/*
 * Function name: kowal_hook_sell_get_money
 * Description:   Called when player gets money for the stuff he sold
 * Arguments:     str - String describing the money he got
 */
void
kowal_hook_sell_get_money(string str)
{
    write("Dostajesz " + str + ".\n");
}

/*
 * Function name: kowal_hook_order_no_match
 * Description:   Called if we can't find what player wants to order
 * Arguments:	  str - The string the player typed in
 * Returns:	  0
 */
int
kowal_hook_order_no_match(string str)
{
    notify_fail("Nie wyrabiamy niczego takiego.\n");
}

/*
 * Function name: kowal_hook_bought_items
 * Description:   Called when player has bought something
 * Arguments:	  arr - The array of objects
 * Returns: 	  1
 */
int
kowal_hook_bought_items(object *arr)
{
    write("Zamawiasz " + COMPOSITE_DEAD(arr, PL_BIE) + ".\n");
    saybb(QCIMIE(this_player(), PL_MIA) + " zamawia " + QCOMPDEAD(PL_BIE) + ".\n");
    return 1;
}

/*
 * Function name: kowal_hook_order_cant_carry
 * Description:   Player can't carry the object he tried to order
 * Arguments:     ob  - The object
 *		  err - Error code from move()
 */
void
kowal_hook_order_cant_carry(object ob, int err)
{
    notify_fail("Nie mo�esz unie�� " + ob->short(PL_DOP) + ".\n");
}

/*
 * Function name: kowal_hook_order_cant_pay
 * Description:   Called when player can't pay for what he wants to order.
 *		  The /lib/trade.c sets some default error messages but
 *		  perhaps you are not happy with them?
 * Arguments:	  ob - The object
 *		  arr - The error code as it comes from trade.c
 */
void
kowal_hook_order_cant_pay(object ob, int *arr)
{
    write("Nie starczy ci pieni�dzy, aby zap�aci�.\n");
}

/*
 * Function name: kowal_hook_order_magic_money
 * Description:	  Called if we failed to take the money from the player for
 *		  some strange reason (magic money , fool's gold ? )
 * Arguments:	  ob - The object player tried to order
 */
void
kowal_hook_order_magic_money(object ob)
{
    write("Hmm, masz bardzo dziwne pieni�dze! Nie ma mowy!\n");
}

/*
 * Function name: kowal_hook_order_pay_money
 * Description:   Called when player pays the money for something
 * Arguments:     str    - How much he pays in text form
 *		  change - The change he gets back (if any)
 */
void
kowal_hook_order_pay_money(string str, string change)
{
    write("P�acisz " + str);

    if (change)
        write(" i dostajesz " + change + " reszty");

    write(".\n");
}

/*
 * Function name: kowal_hook_value_not_interesting
 * Description:   Called when player values something we don't want to buy
 * Arguments:	  ob - The object
 */
void
kowal_hook_value_not_interesting(object ob)
{
    notify_fail(capitalize(ob->short(PL_MIA)) + " nie jest zbyt " +
        "interesuj�cym towarem.\n");
}

/*
 * Function name: kowal_hook_value_held
 * Description:   Player values an object he's holding
 * Arguments:	  ob   - The object
 *		  text - The price in text form
 */
void
kowal_hook_value_held(object ob, string text)
{
    write("Za " + ob->short(PL_BIE) + " dosta�" +
        this_player()->koncowka("by�", "aby�") + " " + text + ".\n");
}

/*
 * Function name: kowal_hook_value_asking
 * Description:   What other see when someone evaluates something
 * Arguments:     str - The text form what the player is asking about
 */
void
kowal_hook_value_asking(string str)
{
    saybb(QCIMIE(this_player(), PL_MIA) + " pyta si� kowala o jakie� ceny.\n");
}

/*
 * Function name: kowal_hook_appraise_object
 * Description  : Called when a player asks to see an object for sale.
 */
void
kowal_hook_appraise_object(object ob)
{
    saybb(QCIMIE(this_player(), PL_MIA) + " prosi o pokazanie " +
          ob->short(PL_DOP) + ".\n");
}

/*
 * Function name: kowal_hook_value_no_match
 * Description:   Called if there were no match with what the player asked
 *		  about
 * Arguments:     str - The string player asked about
 * Returns:	  0
 */
int
kowal_hook_value_no_match(string str)
{
    notify_fail("Wydaje mi si�, i� nie posiadasz niczego takiego.\n");
}

/*
 * Function name: kowal_hook_list_no_match
 * Description:   Called if player specified the list and no matching
 *		  objects where found
 * Arguments:	  str - The string he asked for
 * Returns:	  0
 */
int
kowal_hook_list_no_match(string str)
{
    notify_fail("Nie wytwarzamy niczego takiego.\n");
}

/*
 * Function name: kowal_hook_list_object
 * Description:   List an object
 * Arguments:	  ob - The object
 */
void
kowal_hook_list_object(object ob, int price)
{
    string str, mess;

    str = sprintf("%-25s", capitalize(ob->short(PL_MIA)));
    if (mess = text(split_values(price), 0))
	write(str + mess + ".\n");
    else
	write(str + "Za ten przedmiot nic by� nie zap�aci�" +
	    this_player()->koncowka("", "a") + ".\n");
}

/*
 * Function name: query_order_price
 * Description:   What price should the player pay
 * Arguments:     ob - The object to test
 * Returns: 	  The price
 */
int
query_order_price(object ob)
{
    int seed;

    sscanf(OB_NUM(ob), "%d", seed);
    return 3 * ob->query_prop(OBJ_I_VALUE) * (query_money_greed_buy() +
	15 - this_player()->query_skill(SS_TRADING) / 4 +
	random(15, seed)) / 200;
}

/*
 * Function name: query_sell_price
 * Description:   What price will the player get when selling an object?
 * Arguments:	  ob - The object
 * Returns:	  The price
 */
int
query_sell_price(object ob)
{
    int seed;

    sscanf(OB_NUM(ob), "%d", seed);
    return ob->query_prop(OBJ_I_VALUE) * 100 / (query_money_greed_sell() +
	15 - this_player()->query_skill(SS_TRADING) / 3 +
	random(15, seed + 1)); /* Use another seed than on buying */
}

/*
 * Function name: init_kowal
 * Description  : This function is called for each living that enters the
 *                room. It adds the necessary commands to the players.
 *                You should call this from the init() function in your
 *                room.
 */
void
init_kowal()
{
    add_action(zamow,     "zam�w");
    add_action(napraw,    "napraw");
    add_action(do_sell,   "sprzedaj");
    add_action(do_value,  "wyce�");
    add_action(do_show,   "poka�");
    add_action(do_list,   "przejrzyj");
    set_alarm(3.0, 0.0, odbierz);
}

/*
 * Function name: standard_kowal_sign
 * Description:   returns a string with a sign message
 * Arguments:     none
 * Returns:       message string
 */
string
standard_kowal_sign()
{
    return
"Oto przyk�ady tego, co mo�esz tu uczyni�:\n" +
"    zam�w miecz za korony  (standardowo najmniejsza denominacja)\n" +
"    zam�w miecz za korony i we� grosze reszty\n" +
"    sprzedaj miecz za grosze  (standardowo najwi�ksza denominacja)\n" +
"    sprzedaj wszystko  - dzi�ki czemu sprzedasz wszystkie przedmioty, poza\n"+
"                         tymi, kt�re dzier�ysz lub masz na sobie. Pami�taj,\n"+
"                         �e gdy masz za du�o przedmiot�w do sprzedania, by�\n"+
"                         mo�e zajdzie konieczno�� ponownego wykonania tej\n"+
"                         komendy.\n"+
"    sprzedaj wszystko! - pozwoli ci sprzeda� WSZYSTKIE przedmioty, kt�re\n"+
"                         masz przy sobie - za wyj�tkiem tych, kt�rych nie\n" +
"                         da si� od�o�y� i z wyj�tkiem monet.\n"+
"                         (patrz ostrze�enie dla 'sprzedaj wszystko')\n"+
"    sprzedaj miecze, sprzedaj drugi miecz, sprzedaj dwa miecze - te komendy\n"+
"                         tak�e dzia�aj�. Pami�taj jednak, �e nigdy\n" +
"                         nie b�dziesz w stanie kupi� wi�cej, ni� jednego\n"+
"                         przedmiotu.\n" +
"    wyce�              - Sprzedawca wyceni ile wart jest dany przedmiot,\n"+
"                         zanim zdecydujesz si� go sprzeda�.\n" +
"    poka�              - Pozwoli ci obejrze� dok�adnie jaki� przedmiot\n"+
"                         z magazynu, zanim podejmiesz decyzj� o zam�wieniu go.\n"+
"    przejrzyj          - Pozwala ci przejrze� zawarto�� magazynu. Mo�esz\n"+
"                         te� 'przejrze� zbroje' i 'przejrze� bronie'.\n";
}

/*
 * Function name: sell_it
 * Description:   Try to let the player sell the item array
 * Arguments:     ob - the object array
 *                check - wheather check for worn or wielded stuff
 *                str - string describing how the money should be paid
 * Returns:	  An array with the objects sold
 */
object *
sell_it(object *ob, string str, int check)
{
    int price, i, j, k, *tmp_arr, *null, *value_arr, *null_arr, err;
    object *sold;
    mixed tmp;

    value_arr = allocate(sizeof(query_money_types()));
    null_arr = value_arr + ({});
    sold = allocate(sizeof(ob));

    for (i = 0; i < sizeof(ob); i++)
    {
	if (!kowal_hook_allow_sell(ob[i]))
	    continue;

        if (ob[i]->query_prop(OBJ_I_VALUE) == 0)
	{
	    kowal_hook_sell_no_value(ob[i]);
	    continue;
        }

	if (check && (ob[i]->query_worn() ||
		      ob[i]->query_wielded()))
	{
	    kowal_hook_sell_worn_or_wielded(ob[i]);
	    continue;
        }

	if (tmp = ob[i]->query_prop(OBJ_M_NO_SELL))
	{
	    kowal_hook_sell_no_sell(ob[i], tmp);
	    continue;
	}

	/* Save price if ob destructed in move */
	price = query_sell_price(ob[i]);

        if (price <= 0)
	{
	    kowal_hook_sell_no_value(ob[i]);
	    continue;
        }

        if (price > 0)
	{
            tmp_arr = calc_change(price, null, str);
            for (k = 0; k < sizeof(value_arr); k++)
                value_arr[k] += tmp_arr[k];

	    sold[j] = ob[i];
            j++;
	    if (j >= 20)
        	break;
    /*
     * Only let people sell 20 objects at once and hopefully we wont get
     * those too long evaluation problems.
     */
	}
    }

    sold = sold - ({ 0 });

    if (sizeof(sold) > 0)
    {
        change_money(null_arr + value_arr, this_player());
	kowal_hook_sell_get_money(text(value_arr, 0));
    }

    return sold;
}

/*
 * Function name: do_sell
 * Description:   Try to let the player sell the_item
 *                Observe there is no message written when sold item
 *                has a value higher than the shop gives out.
 * Returns:       1 on sucess
 * Arguments:     str - string holding name of item, hopefully
 */
int
do_sell(string str)
{
    object *item;
    int value, check;
    string str1, str2;

    if (!str || str =="")
    {
	notify_fail("Sprzedaj co?\n");
	return 0;
    }

    /*  Did player specify how to get the money? */
    if (sscanf(str, "%s za %s", str1, str2) != 2)
    {
 	str1 = str;
	str2 = "";
    }

    if (str1 == "wszystko!")
    {
	str1 = "wszystko";
        check = 0; /* Sell worn or wielded objects. */
    }
    else
    {
	check = 1; /* Don't sell worn or wielded objects. */
    }

    item = FIND_STR_IN_OBJECT(str1, this_player(), PL_BIE);
    if (!sizeof(item))
	return kowal_hook_sell_no_match(str1);

    item = sell_it(item, str2, check);
    if (sizeof(item))
    {
	kowal_hook_sold_items(item);
	for_each(item, &->remove_object()); // ... czy update'uje pozostale ?
	return 1;
    }

    if (str1 == "wszystko")
	return kowal_hook_sold_nothing();

    return 0; /* Player tried to sell a non sellable object. */
}

/*
 * Function name: zamow
 * Description:   Try to let the player order an item
 * Arguments:     string - describing how to pay and get change
 * Returns:       1 on sucess
 */
int
zamow(string str)
{
    object *item;
    int cena, *arr, num, err;

    if (!str || str == "")
    {
	notify_fail("Co chcesz zam�wi�?\n");
	return 0;
    }

    item = FIND_STR_IN_ARR(str, lista2, PL_BIE);

    if (!sizeof(item))
	return kowal_hook_order_no_match(str);

//    if (objectp(item))
//	item = ({ item });

    if (zlecenie)
	return kowal_hook_zajety();

    num = sizeof(query_money_types());

    cena = query_order_price(item[0]);

    if (sizeof(arr = pay(cena, this_player(), "", 0, 0, "")) == 1)
    {
	kowal_hook_order_cant_pay(item[0], arr);
	return 1;
    }

    if (err = arr[sizeof(arr) - 1])
    {
	if (err < -1)
	{
	    kowal_hook_order_magic_money(item[0]);
	    return 1;
	}
    }

    kowal_hook_order_pay_money(text(arr[0 .. num - 1], PL_BIE),
	text(arr[num .. 2 * num - 1], PL_BIE));

    zlecenie = K_ZAMOW;
    przedmiot = clone_object(MASTER_OB(item[0]));
    klient = this_player()->query_real_name();

    kowal_hook_zamow(przedmiot);

    set_alarm(20.0, 0.0, zrobione);

    return 1;
}

int
napraw(string str)
{
    object *item;
    int cena, *arr, num, err;

    if (!str || str == "")
    {
	notify_fail("Co chcesz naprawi�?\n");
	return 0;
    }

    item = FIND_STR_IN_OBJECT(str, this_player(), PL_BIE);

    if (!sizeof(item))
	return kowal_hook_order_no_match(str);

//    if (objectp(item))
//	item = ({ item });

    if (zlecenie)
	return kowal_hook_zajety();

    num = sizeof(query_money_types());

    cena = query_order_price(item[0]);

    if (sizeof(arr = pay(cena, this_player(), "", 0, 0, "")) == 1)
    {
	kowal_hook_order_cant_pay(item[0], arr);
       	return 1;
    }

    if (err = arr[sizeof(arr) - 1])
    {
	if (err < -1)
	{
	    kowal_hook_order_magic_money(item[0]);
	    return 1;
	}
    }

    kowal_hook_order_pay_money(text(arr[0 .. num - 1], PL_BIE),
	text(arr[num .. 2 * num - 1], PL_BIE));

    zlecenie = K_NAPRAW;
    przedmiot = item[0];
    przedmiot->move(this_object(), 1);
    klient = this_player()->query_real_name();

    kowal_hook_napraw(przedmiot);

    set_alarm(20.0, 0.0, zrobione);

    return 1;
}

/*
 * Function name:   do_value
 * Description:     Let the player value an item, carried
 * Returns:         1 on success
 */
int
do_value(string str)
{
    object *item, store;
    int *arr, price, i, j, num, no_inv;

    if (!str || str =="")
    {
	notify_fail("Wyce� co?\n");
	return 0;
    }

    num = sizeof(query_money_types());
    item = FIND_STR_IN_OBJECT(str, this_player(), PL_BIE);
    if (!sizeof(item))
	no_inv = 1;

    for (i = 0; i < sizeof(item); i++)
    {
	if (!kowal_hook_allow_sell(item[i]) ||
		!item[i]->query_prop(OBJ_I_VALUE) ||
		item[i]->query_prop(OBJ_M_NO_SELL))
	{
	    kowal_hook_value_not_interesting(item[i]);
	    continue;
	}

	price = query_sell_price(item[i]);
	arr = give(price, this_player(), "", 1);
	kowal_hook_value_held(item[i], text(arr[num .. 2 * num - 1], 0));
	j++;
    }

    if (no_inv)
	return kowal_hook_value_no_match(str);

    kowal_hook_value_asking(str);

    if (j > 0)
	return 1;

    return 0;
}

/*
 * Function name:   do_list
 * Description:     Provide a list of objects in the store room
 * Returns:         0 if not recognised
 *                  1 otherwise
 * Arguments: 	    str - the name of the objects to search for
 */
int
do_list(string str)
{
    int i, price;

    for (i = 0; i < sizeof(lista2); i++)
    {
	price = query_order_price(lista2[i]);
	kowal_hook_list_object(lista2[i], price);
    }

    return 1;
}

/*
 * Function name: do_show
 * Description  : Allow the player to appraise one of the objects in stock.
 * Returns      : int - 1/0 - true if success.
 * Arguments    : string str - the name of the objects to search for.
 */
int
do_show(string str)
{
    object *item_arr;
    int i, *arr;

    if (!sizeof(lista2))
    {
	kowal_hook_list_no_match(str);
	return 0;
    }

    item_arr = FIND_STR_IN_ARR(str, lista2, PL_BIE);

    if (sizeof(item_arr) < 1)
    {
	return kowal_hook_list_no_match(str);
    }

    kowal_hook_appraise_object(item_arr[0]);
    item_arr[0]->appraise_object();

    return 1;
}

/*
 *
 */
void
odbierz()
{
    int err;
    string *obecni;
    object odbierajacy, ob;
    int i, i2;

    if (!sizeof(gotowe))
	return ;

    obecni = map(FILTER_PLAYERS(all_inventory(environment(this_object()))),
	&->query_real_name());

    if (stringp(obecni))
	obecni = ({ obecni });

    for (i = (sizeof(gotowe) - 1); i > -1; i--)
    {
	i2 = member_array(gotowe[i][0], obecni);
	if (i2 > -1)
	{
	    odbierajacy = find_player(obecni[i2]);
	    ob = gotowe[i][1];

	    if (err = ob->move(odbierajacy))
	    {
		ob->move(environment(odbierajacy));
		kowal_hook_order_cant_carry(ob, err);
		continue;
	    }

	    gotowe = exclude_array(gotowe, i, i);
	    kowal_hook_odbierz(ob);
	}
    }

}

void
zrobione()
{
    zlecenie = K_WOLNY;
    gotowe += ({ ({ klient, przedmiot }) });
    przedmiot = 0;
    odbierz();
}


/*
 * Function name: weapon_filter
 * Description  : Filter out weapons
 * Arguments    : object ob - the object to test
 * Returns      : int - 1/0 - true if it is a weapon.
 */
int
weapon_filter(object ob)
{
    return (function_exists("create_object", ob) == "/std/weapon");
}

/*
 * Function name: armour_filter
 * Description  : Filter out armours
 * Arguments    : object ob - the object to test
 * Returns      : int - 1/0 - true if it is an armour.
 */
int
armour_filter(object ob)
{
    return (ob->is_armour());
}

