
/**
 * \file /lib/pub_new.c
 * Kompletne w�a�ciwie przepisanie kodu od nowa dzi�ki czemu jest bardziej
 * przejrzysty od starszego. Dodanie mo�liwo�ci stawiania komu�, oraz drinki
 * podawane s� w naczyniach zdefiniowanytch w <pub.h>.
 * O wszelkich zmianach w kodzie prosz� mnie informowa�.
 * Pa�dziernik 2003 Mistrz Froan(Simon) dla Dragfor.
 *
 * "I love coding with some music in background, and
 * with my favourite OS Red Hat Linux."
 *
 * Dodano d�ugie opisy jedzenia dla graczy.
 * Grudzien 2003 - Yevaud
 *
 *                                   Vera 26.5.2005
 */

#include <stdproperties.h>
#include <macros.h>
#include <money.h>
#include <language.h>
#include <files.h>
//#include <pub.h>
#include <filter_funs.h>
#include <composite.h>
#include <std.h>
#include <files.h>
#include <cmdparse.h>
#include <sit.h>

/*
#define NAZWA_MIA             0
#define NAZWA_DOP             1
#define NAZWA_BIE             2
#define OPIS_PLYNU_DOP	      3
#define OBJ_POJEMNIKA         4
#define VOL                   5
#define CENA                  6
#define RODZAJ                7
#define RODZAJ_POJEMNIKA      8
#define NAZWA_PRZY_LYKANIU    9
#define DLUGI_OPIS_POJEMNIKA 10
#define OBJ_NA_WYNOS	     11
*/

#define NAZWA                   0
#define PRZYM                   1
#define OPIS_PLYNU_DOP          2
#define OBJ_POJEMNIKA           3
#define VOL                     4
#define CENA                    5
#define RODZAJ_POJEMNIKA        6
#define NAZWA_PRZY_LYKANIU      7
#define DLUGI_OPIS_POJEMNIKA    8
#define OBJ_NA_WYNOS            9

#define WYNOS_NAZWA             0
#define WYNOS_PRZYM             1
#define WYNOS_ILOSC             2
#define WYNOS_CENA              3
#define WYNOS_LONG              4
#define WYNOS_DECAY_TIME        5
#define WYNOS_SHORT             6
#define WYNOS_DECAY_ADJ         7

// ponizsze 6 jest do wywalenia
//#define NAZWA_B             1
//#define OPIS                2
#define WAGA                3
//#define CENA_FOOD           4
#define OPIS_POSTAWIONEGO   5
#define OPISY_JEDZENIA      6

#define NA_MIEJSCU          0
#define BUKLAK              1
#define BECZULKA            2

#define ID_ZARCIA           0
#define ILOSC_ZARCIA	    1
#define PLAYER_POST         "_player_i_postawili_mi_cos_"
//#define PLAYER_JE     "_player_je_teraz_"

static mixed drinks;        // Tablica napojow.
static mixed food;          // Tablica potraw.
static mixed wynos_food;    // Tablica zarcia na wynos (ze starego pub.c)

int         order(string str);
int         stawianie(string str);
int         zwrot(string str);
int         stawianie_drinka(int i, object player, object stawiajacy);
int         postaw_zarcie(int i, object komu, object player);
static void tell_watcher(string str, object stawiajacy, object player);
void        oblicz_sh_drinks(object ob, int i);
void        oblicz_sh_food(object ob, int i);
void        jedzenie(int opisy, int faza, object jedzacy, string short);
int         alco=0;

/*
 * Function name: init_pub
 * Description:   Ta funkcja dodaje komendy kupowania i zamawiania. Wywo�uje si� j�
 *                w funkcji init()
 */
void
init_pub()
{
    add_action(order,       "kup");
    add_action(order,       "zam�w");
    add_action(stawianie,   "postaw");
//    add_action(zjedz,		"zjedz");
    add_action(zwrot,       "oddaj");
    add_action(zwrot,       "zwr��");
}

string
query_pomoc()
{
    string str =
    "Znajdujesz si� w�a�nie w miejscu, kt�re oferuje mo�liwo�� "+
    "spo�ycia jakiego� dania i/lub ugaszenia swojego pragnienia. "+
    "Zapewne gdzie� w okolicy uda ci si� znale�� tabliczk� z menu, "+
    "kt�r� powin"+TP->koncowka("iene�","na�")+" obejrze� lub przeczyta�, "+
    "by wiedzie� co mo�esz tu zam�wi�.\n"+
    "Dost�pne komendy to: kup, zam�w, postaw i zwr��.\n";
    if(TP->query_age() < 200)
        str+="Wskaz�wka: Gdy ju� uda ci si� co� zam�wi� zerknij tak�e na pomoc "+
        "dotycz�c� danego zam�wienia. Robi si� to wpisuj�c po '?' nazw� "+
        "naczynia w kt�rym dostajesz danie (np. '?kubek', '?talerz' itp).\n\n"+
        "Je�li chcesz dowiedzie� si� czego� wi�cej na temat uzyskiwania "+
        "pomocy wpisz: '?pomoc'.\n";

    return str;
}

/*
 * Function name: query_pub
 * Description:   Po prostu sprawdza, czy lokacja
 *                jest pubem...
 */
public int
query_pub()
{
   return 1;
}

public int
query_vol()
{
    return alco;
}

// Wyszukuje w s�owniku nazw� dania/trunku.
mixed
znajdz_nazwe(string str)
{
    mixed odm = secure_var(slownik_pobierz(str));

    if (!odm)
    {
        throw("Nie ma nazwy '" + str + "' w s�owniku.\n");
        return 0;
    }

    if(sizeof(odm) == 2)
    {
        odm += ({odm[1]});
        odm[1] = odm[0];
    }

    return odm;
}

/*
 * Function name: wychodzi_z_karczmy
 * Description:   T� funkcj� wywo�uje si� w funkcji leave_inv w g��wnym
 *		  obiekcie karczmy i przyjmuje ona jeden argument a mianowicie
 *		  obiekt gracza wychodz�cego z karczmy. Wi�cej szuka� w
 *		  przyk�adach.
 */
void
wychodzi_z_karczmy(object kto)
{
    if (living(kto))
        if (member_array(PLAYER_POST, kto->query_props()) >= 0)
            kto->remove_prop(PLAYER_POST);
}

/*
 * Function name: pub_hook_cant_carry
 * Description:   Funkcja zwraca wiadomo�� graczowi, kt�ry co� kupi� ale nie mo�e
 *		  tego unie��
 * Arguments:     ob - obiekt kt�rego nie mo�e unie��
 */
void
pub_hook_cant_carry(object ob)
{
    write(capitalize(ob->short()) + " jest za ci�kie dla ciebie. Upuszczasz to " +
        "na ziemi�.\n");
    say(capitalize(ob->short()) + " okazuje sie za ci�kie, wi�c " + QTNAME(this_player()) +
        " upuszcza to na ziemi�.\n");
}

/*
 * Nazwa funkcji : pub_hook_i_eat_postawione
 * Opis          : Funkcja wypisuj�ca odpowiednie teksty, gdy gracz
 *		   zjada zam�wione dla niego �arcie. Mo�esz j� przedefiniowa�,
 *		   je�li chcesz zast�pi� standardowe teksty swoimi.
 * Argumenty     : string short - kr�tki opis �arcia.
 *		   object p     - obiekt jedz�cego gracza.
 */
public void
pub_hook_i_eat_postawione(string short, object p)
{
//Nie wiem po co ta funkcja skoro nie jest wykorzystywana... - Yevaud

    write("Zjadasz ze smakiem " + short + ".\n");
    saybb(QCIMIE(p, PL_MIA) + " zjada " + short + ".\n");
}

/*
 * Nazwa funkcji : pub_hook_buys_drink_na_miejscu
 * Opis          : Funkcja wypisuj�ca odpowiednie teksty, gdy gracz
 *		   kupuje picie na miejscu. Mo�esz j� przedefiniowa�,
 *		   je�li chcesz zast�pi� standardowe teksty swoimi.
 * Argumenty     : string short - kr�tki opis picia w bierniku,
 *		   int    cena	- cena wyra�ona w groszach.
 */
public void
pub_hook_buys_drink_na_miejscu(string short, int cena)
{
    if(query_vol()>0)
    {
        write("Zamawiasz " + short + ", p�ac�c " + cena + " grosz" +
            ilosc(cena, "", "e", "y") + ".\n" +
            "Po chwili otrzymujesz zam�wiony trunek.\n");
        saybb(QCIMIE(this_player(), PL_MIA) + " zamawia " + short +
           " i po chwili otrzymuje trunek.\n");
    }
    else
    {
        write("Zamawiasz " + short + ", p�ac�c " + cena + " grosz" +
            ilosc(cena, "", "e", "y") + ".\n" +
            "Po chwili otrzymujesz zam�wiony p�yn.\n");
        saybb(QCIMIE(this_player(), PL_MIA) + " zamawia " + short +
           " i po chwili otrzymuje p�yn.\n");
    }
}

/*
 * Nazwa funkcji : pub_hook_stawianie_drinka
 * Opis          : Funkcja wypisuj�ca odpowiednie teksty, gdy gracz
 *		   stawia komu� drinka. Mo�esz j� przedefiniowa�,
 *		   je�li chcesz zast�pi� standardowe teksty swoimi.
 * Argumenty     : string short - kr�tki opis napoju.
 *		   int    cena	- cena wyra�ona w groszach.
 *		   object player - gracz kt�remu stawiamy
 *		   object stawiajacy - gracz stawiaj�cy
 */
public void
pub_hook_stawianie_drinka(string short, int cena, object player, object stawiajacy)
{
    stawiajacy->catch_msg("Stawiasz " + short + " " +
        QIMIE(player, PL_CEL) + ", p�ac�c " + cena + " grosz" +
        ilosc(cena, "", "e", "y") + ".\n");

    player->catch_msg(QIMIE(stawiajacy, PL_MIA) + " stawia ci " + short + ".\n");
    tell_watcher(QIMIE(stawiajacy, PL_MIA) + " stawia " + short + " " +
        QIMIE(player, PL_CEL) + ".\n", stawiajacy, player);
}
/*
 * Nazwa funkcji : pub_hook_stawianie_zarcia
 * Opis          : Funkcja wypisuj�ca odpowiednie teksty, gdy gracz
 *		   stawia komu� �arcie. Mo�esz j� przedefiniowa�,
 *		   je�li chcesz zast�pi� standardowe teksty swoimi.
 * Argumenty     : string short - kr�tki opis �arcia.
 *		   int    cena	- cena wyra�ona w groszach.
 *		   object player - gracz kt�remu stawiamy
 *		   object stawiajacy - gracz stawiaj�cy
 */
public void
pub_hook_stawianie_zarcia(string short, int cena, object player, object stawiajacy)
{
    stawiajacy->catch_msg("Stawiasz " + short + " "+
        QIMIE(player, PL_CEL) + ", p�ac�c " + cena + " grosz" +
        ilosc(cena, "", "e", "y") + ".\n");

    player->catch_msg(QIMIE(stawiajacy, PL_MIA) + " stawia ci " + short + ".\n");
    tell_watcher(QIMIE(stawiajacy, PL_MIA) + " stawia " + short + " " +
        QIMIE(player, PL_CEL) + ".\n", stawiajacy, player);

    //player->add_prop(PLAYER_JE, 1);
    //player->add_prop(SIT_NIE_WSTAN , "Teraz jestes zajety jedzeniem! Nie mozesz wstac w trakcie!\n");

//    set_alarm(6.0, 0.0, "jedzenie", opisy, 0, player, short);
}
/*
 * Nazwa funkcji : pub_hook_buys_food_na_miejscu
 * Opis          : Funkcja wypisuj�ca odpowiednie teksty, gdy gracz
 *		   kupuje jedzenie na miejscu. Mo�esz j� przedefiniowa�,
 *		   je�li chcesz zast�pi� standardowe teksty swoimi.
 * Argumenty     : string short - kr�tki opis jedzenia w bierniku,
 *		   int     cena	- cena wyra�ona w groszach.
 *		   int     opisy - kt�rej potrawy opisy wy�wietlamy.
 */
public void
pub_hook_buys_food_na_miejscu(string short, int cena, int opisy)
{
    write("Zamawiasz " + short + ", p�ac�c " + cena + " grosz" +
        ilosc(cena, "", "e", "y") + ".\n"+
        "Po chwili otrzymujesz zam�wion^a potraw^e.\n");
    saybb(QCIMIE(this_player(), PL_MIA) + " zamawia " + short +
           " i po chwili otrzymuje potraw^e.\n");

  //  this_player()->add_prop(PLAYER_JE, 1);
	//this_player()->add_prop(SIT_NIE_WSTAN ,"Teraz jestes zajety jedzeniem! Nie mozesz wstac w trakcie!\n");

//     set_alarm(6.0, 0.0, "jedzenie", opisy, 0, this_player(), short);
}

/*
 * Nazwa funkcji : pub_hook_buys_food_na_wynos
 * Opis          : Funkcja wypisuj�ca odpowiednie teksty, gdy gracz
 *		   kupuje jedzenie na wynos. Mo�esz j� przedefiniowa�,
 *		   je�li chcesz zast�pi� standardowe teksty swoimi.
 * Argumenty     : object ob    - obiekt zakupionego jedzenia,
 *		   int    cena	- cena wyra�ona w groszach.
 */
public void
pub_hook_buys_food_na_wynos(object ob, int cena)
{
    write("Kupujesz " + ob->short(this_player(), PL_BIE) +
        ", p�ac�c " + cena + " grosz" + ilosc(cena, "", "e", "y") + ".\n");
    saybb(QCIMIE(this_player(), PL_MIA) + " kupuje " + QSHORT(ob, PL_BIE) +
        ".\n");
}

/*
 * Nazwa funkcji : pub_hook_buys_drink_na_wynos
 * Opis          : Funkcja wypisuj�ca odpowiednie teksty, gdy gracz
 *		   kupuje picie na wynos. Mo�esz j� przedefiniowa�,
 *		   je�li chcesz zast�pi� standardowe teksty swoimi.
 * Argumenty     : object ob    - obiekt zakupionej beczu�ki z piciem,
 *		   int    cena	- cena wyra�ona w groszach.
 */
public void
pub_hook_buys_drink_na_wynos(object ob, int cena, int num, string opis)
{
    string str;

    str = ob->short(PL_BIE) + " pe�" + ob->koncowka("en", "n�", "ne") +
        " " + opis;

    write("Nabywasz " + (num == 1 ?  "" : (num + " razy ")) +
        str + ", p�ac�c " + cena + " grosz" +
        ilosc(cena, "", "e", "y") + ".\n");
    saybb(QCIMIE(this_player(), PL_MIA) + " kupuje " + str + ".\n");
}

public int
drink_na_miejscu(int i)
{
    int cena;
    string na_wynos;
    object ob;
    if (query_verb() == "kup")
    {
        if (drinks[i][OBJ_NA_WYNOS] < 5000)
            na_wynos = "buk�ak ";
        else
            na_wynos = "beczu�k� ";

        notify_fail("Chcesz 'zam�wi�' " + drinks[i][NAZWA][0][0][PL_BIE] +
                    " na miejscu, czy mo�e 'kupi�' " + na_wynos +
                    drinks[i][NAZWA][0][0][PL_DOP] + " na wynos?\n");
        return 0;
    }

    cena = drinks[i][CENA] * drinks[i][OBJ_POJEMNIKA] / 1000;
    alco = drinks[i][VOL];

    if (!MONEY_ADD(TP, -cena))
    {
        notify_fail("Nie masz wystarczaj�cej ilo�ci pieni�dzy, " +
            "�eby zap�aci�.\n");
        return 0;
    }

    ob=clone_object("/lib/pub_naczynie_picie");
    ob->ustaw_nazwe(drinks[i][RODZAJ_POJEMNIKA][0],
        drinks[i][RODZAJ_POJEMNIKA][1], drinks[i][RODZAJ_POJEMNIKA][2]);

    //dzi�ki tym parom poni�szym linijkom mamy tak�e nazwy takie, jak:
    //'kieliszek wodki', 'kufel piwa', a nie tylko 'kieliszek' i 'kufel'
    //verek.
    string *tmp_arr1 = ({}), *tmp_arr1_p = ({}); //tablice nazw w l.poj. i l.mn.
    for(int x = 0 ; x < sizeof(drinks[i][RODZAJ_POJEMNIKA][0]) ; x++)
    {
        tmp_arr1+= ({drinks[i][RODZAJ_POJEMNIKA][0][x] +" "+ drinks[i][NAZWA_PRZY_LYKANIU]});
        tmp_arr1_p+= ({drinks[i][RODZAJ_POJEMNIKA][1][x] +" "+ drinks[i][NAZWA_PRZY_LYKANIU]});
    }    //dodajemy zebrane nazwy:
    ob->dodaj_nazwy(tmp_arr1,tmp_arr1_p,drinks[i][RODZAJ_POJEMNIKA][2]);


    ob->set_pojemnosc(drinks[i][OBJ_POJEMNIKA]);
    ob->set_ilosc_plynu(drinks[i][OBJ_POJEMNIKA]);
    ob->set_nazwa_plynu_dop(drinks[i][NAZWA][0][0][PL_DOP]);
    ob->set_opis_plynu(drinks[i][OPIS_PLYNU_DOP]);
    ob->set_vol(alco);
    ob->ustaw_nazwe_plynu(drinks[i][NAZWA_PRZY_LYKANIU]);
    ob->set_dlugi_opis(drinks[i][DLUGI_OPIS_POJEMNIKA]);
    oblicz_sh_drinks(ob,i);
    ob->move(TP,1);
    pub_hook_buys_drink_na_miejscu(drinks[i][RODZAJ_POJEMNIKA][0][PL_BIE] + " " + drinks[i][NAZWA][0][0][PL_DOP], cena);
    return 1;
}


public int
drink_na_wynos(int i, int num, int wynos)
{
    object ob;
    string str, na_wynos;
    int j, cena, cena_pojemnika;

    if (query_verb() ~= "zam�w")
    {
        if (drinks[i][OBJ_POJEMNIKA] < 5000)
            na_wynos = "buklak ";
        else
            na_wynos = "beczulke ";

        notify_fail("Chcesz 'zam�wi�' " + drinks[i][NAZWA][0][0][PL_BIE] +
            " na miejscu, czy mo�e 'kupi�' " + na_wynos +
            drinks[i][NAZWA][0][0][PL_DOP] + " na wynos?\n");
        return 0;
    }

    if (!drinks[i][OBJ_NA_WYNOS])
    {
         notify_fail("Nie sprzedajemy " + drinks[i][NAZWA][0][0][PL_DOP] +
            " na wynos.\n");
         return 0;
    }

    if ( (wynos == BUKLAK   && drinks[i][OBJ_NA_WYNOS] >= 5000) ||
         (wynos == BECZULKA && drinks[i][OBJ_NA_WYNOS]  < 5000) )
//         (wynos == BECZULKA && drinks[i][0][OBJ_NA_WYNOS]  < 5000) )
    {
        notify_fail("Nie sprzedajemy " + drinks[i][NAZWA][0][0][PL_DOP] +
            " w " + (drinks[i][OBJ_NA_WYNOS] >= 5000 ?
            "buklakach, tylko w beczulkach" :
            "beczulkach, tylko w buklakach") + ".\n");
        return 0;
    }


    if ( wynos == BECZULKA )
        cena_pojemnika = 30;
    else
        cena_pojemnika = 10;

    cena = num * ((drinks[i][CENA] * drinks[i][OBJ_NA_WYNOS] / 1000 )+
            cena_pojemnika);

    if (!MONEY_ADD(TP, -cena))
    {
        notify_fail("Nie masz wystarczaj�cej ilo�ci pieni�dzy, " +
            "�eby zap�aci�.\n");
        return 0;
    }

    ob = clone_object(BECZULKA_OBJECT);
    ob->set_pojemnosc(drinks[i][OBJ_NA_WYNOS]);
    ob->set_opis_plynu(drinks[i][OPIS_PLYNU_DOP]);
    ob->set_nazwa_plynu_dop(drinks[i][NAZWA][0][0][PL_DOP]);
    ob->set_vol(drinks[i][VOL]);
    ob->set_ilosc_plynu(drinks[i][OBJ_NA_WYNOS]);

    if ( wynos == BUKLAK )
        ob->ustaw_nazwe("buk�ak");
    else
        ob->ustaw_nazwe("beczu�ka");

    ob->add_prop(OBJ_I_VALUE, cena_pojemnika);

    for (j=0; j<num; j++)
    {
        ob->move(TP, 1);
    }

    pub_hook_buys_drink_na_wynos(ob, cena, num, drinks[i][NAZWA][0][0][PL_DOP]);
    return 1;
}

public int
food_na_miejscu(int i)
{
    int cena;
    object ob;
    if (query_verb() == "kup")
        return notify_fail("Nie sprzedajemy tego na wynos.\n");

    cena = food[i][CENA] * food[i][OBJ_POJEMNIKA] / 1000;
    alco = food[i][VOL];


    if (!MONEY_ADD(TP, -cena))
        return notify_fail("Nie masz wystarczaj�cej ilo�ci pieni�dzy, �eby zap�aci�.\n");

    ob = clone_object("/lib/pub_naczynie_jedzenie");
    ob->ustaw_nazwe(food[i][RODZAJ_POJEMNIKA][0], food[i][RODZAJ_POJEMNIKA][1], food[i][RODZAJ_POJEMNIKA][2]);
    ob->set_pojemnosc(food[i][OBJ_POJEMNIKA]);
    ob->set_ilosc_jedzenia(food[i][OBJ_POJEMNIKA]);

    //dzi�ki tym parom poni�szym linijkom mamy tak�e nazwy takie, jak:
    //'talerz jajecznicy z serem', 'miska ryzu', a nie tylko 'talerz' i 'miska'
    //nazwy 'jajecznica' i 'ryz' uwazam za nieadekwatne, wiec takich nie dodalem
    //verek.
    string *tmp_arr1 = ({}), *tmp_arr1_p = ({}), //tablice nazw w l.poj. i l.mn.
            *tmp_arr2 = ({}), *tmp_arr2_p = ({});//tablice drugi nazw - niektore dania
                                                 //maja dwie nazwy, np. 'jajecznica' i
                                                 //'jajecznica z serem'.
    for(int x = 0 ; x < sizeof(food[i][RODZAJ_POJEMNIKA][0]) ; x++)
    {
        tmp_arr1+= ({food[i][RODZAJ_POJEMNIKA][0][x] +" "+ food[i][NAZWA][0][0][1]});
        tmp_arr1_p+= ({food[i][RODZAJ_POJEMNIKA][1][x] +" "+ food[i][NAZWA][0][0][1]});
    }
    if(sizeof(food[i][NAZWA]) > 1) //je�li mamy wi�cej nazw, np. dwie, to dodajmy drug�:
    {
        for(int x = 0 ; x < sizeof(food[i][RODZAJ_POJEMNIKA][0]) ; x++)
        {
            tmp_arr2+= ({food[i][RODZAJ_POJEMNIKA][0][x] +" "+ food[i][NAZWA][1][0][1]});
            tmp_arr2_p+= ({food[i][RODZAJ_POJEMNIKA][1][x] +" "+ food[i][NAZWA][1][0][1]});
        }
    }
    //dodajemy zebrane nazwy:
    ob->dodaj_nazwy(tmp_arr1,tmp_arr1_p,food[i][RODZAJ_POJEMNIKA][2]);
    if(sizeof(tmp_arr2) && sizeof(tmp_arr2_p)) //dodaj dodatkowe nazwy:
        ob->dodaj_nazwy(tmp_arr2,tmp_arr2_p,food[i][RODZAJ_POJEMNIKA][2]);

    ob->ustaw_nazwy_jedzenia(food[i][NAZWA]);
    ob->set_opis_jedzenia(food[i][OPIS_PLYNU_DOP]);
    ob->set_dlugi_opis(food[i][DLUGI_OPIS_POJEMNIKA]);
    oblicz_sh_food(ob,i);

    ob->move(TP,1);
    pub_hook_buys_food_na_miejscu(food[i][RODZAJ_POJEMNIKA][0][PL_BIE] + " " + food[i][NAZWA][0][0][PL_DOP], cena, 1);

    return 1;
}

public int
food_na_wynos(int i, int num)
{
    int cena, ix;
    object ob;

    if (!wynos_food[i][WYNOS_NAZWA])
    {
        write("Tej potrawy nie mo�na wzi�� na wynos. Mo�esz j� jedynie " +
            "'zam�wi�' i zje�� na miejscu.\n");
        return 1;
    }

    cena = num * wynos_food[i][WYNOS_CENA];

    if (!MONEY_ADD(this_player(), -cena))
    {
        notify_fail("Nie masz wystarczaj�cej ilo�ci pieni�dzy, " +
            "�eby zap�aci�.\n");
        return 0;
    }

    seteuid(getuid(this_object()));
    ob = clone_object(FOOD_OBJECT);


    ix = sizeof(wynos_food[i][WYNOS_NAZWA]);
    while (--ix >= 0)
    {
        ob->ustaw_nazwe(wynos_food[i][WYNOS_NAZWA][0][0],
        wynos_food[i][WYNOS_NAZWA][0][1], wynos_food[i][WYNOS_NAZWA][0][2]);
        //verek dodaje jeszcze dodatkowe nazwy, co by bylo fajniej
        if(ix != 0)
            ob->dodaj_nazwy(wynos_food[i][WYNOS_NAZWA][ix][0],
            wynos_food[i][WYNOS_NAZWA][ix][1], wynos_food[i][WYNOS_NAZWA][ix][2]);
    }

    if (sizeof(wynos_food[i][WYNOS_PRZYM]) == 2)
    {
        ix = sizeof(wynos_food[i][WYNOS_PRZYM][0]);
        while (--ix >= 0)
            ob->dodaj_przym(wynos_food[i][WYNOS_PRZYM][0],
                wynos_food[i][WYNOS_PRZYM][1]);
    }

    if (sizeof(wynos_food[i][WYNOS_SHORT]))
        ob->ustaw_shorty(wynos_food[i][WYNOS_SHORT][0],
            wynos_food[i][WYNOS_SHORT][1], wynos_food[i][WYNOS_SHORT][2]);
    ob->set_amount(wynos_food[i][WYNOS_ILOSC]);
    ob->set_long(wynos_food[i][WYNOS_LONG]);
    ob->add_prop(HEAP_I_UNIT_VOLUME, wynos_food[i][WYNOS_ILOSC]);
    ob->add_prop(HEAP_I_UNIT_WEIGHT, wynos_food[i][WYNOS_ILOSC]);
    ob->add_prop(HEAP_I_UNIT_VALUE, wynos_food[i][WYNOS_CENA]);
    ob->set_decay_time(wynos_food[i][WYNOS_DECAY_TIME]);
    if (sizeof(wynos_food[i][WYNOS_DECAY_ADJ]) == 2)
        ob->set_decay_adj(wynos_food[i][WYNOS_DECAY_ADJ]);

    ob->set_heap_size(num);
    ob->move(this_player(), 1);
    ob->start_decay();

    pub_hook_buys_food_na_wynos(ob, cena);

    return 1;
}

/*
 * Function name: order
 * Description:   The player has ordered something, let's see if we can satisfy
 *		  him.
 * Arguments:	  str - The order from the player
 * Returns:	  0 - jesli nie udalo sie wykonac zamowienia
 *                1 - jesli zamowienie wykonano
 */
int
order(string str)
{
    string *word, text;
    int wynos, i, j, k, size, insize, num, przyp;
    int ilosc = 1, tmp = 0;


    if (!str)
        return notify_fail(capitalize(query_verb()) + " co?\n");

    if(!sizeof(filter(all_inventory(ENV(TP)),&->query_karczmarz())))
        return notify_fail("Zdaje si�, �e nie ma tu nikogo, kto m�g�by ci� obs�u�y�.\n");

    /*
     *  Ile sztuk.
     */

    word = explode(str, " ");
/*    size  = sizeof(word);
    text  = word[0];

    for (i=1; i<size; i++)
    {
        if (num = LANG_NUMS(text))
        {
            ilosc = num;
            text += " " + word[i];
            tmp ++;
        }
        else break;
    }

    if (sscanf(word[0], "%d", num))
    {
        ilosc = num;
        tmp   = 1;
    }

    if (tmp)
    {
        word = word[tmp..];
        str   = implode(word, " ");
    }*/

    /*
     *  Na wynos i w czym.
     */

    if (wildmatch("buk�ak *", str) /*|| wildmatch("buk�aki *", str) ||
         wildmatch("buk�ak�w *", str)*/ ) wynos = BUKLAK;
    else
        if (wildmatch("beczu�k� *", str) /*|| wildmatch("beczu�ki *", str) ||
            wildmatch("beczu�ek *", str)*/ ) wynos = BECZULKA;
    else wynos = NA_MIEJSCU;

    if (!wynos && ilosc > 1)
    {
         notify_fail("Zamawia� mo�na tylko pojedynczo.\n");
         return 0;
    }

    if (wynos)
    {
        word = word[1..];
        str   = implode(word, " ");
        przyp = PL_DOP;
    }
    else
        przyp = PL_BIE;

    /*
     * Jakis napoj.
     */
    size = sizeof(drinks);

    for (i = 0; i < size; i++)
    {
        insize = sizeof(drinks[i][NAZWA]);

        for (j = 0; j < insize; j++)
        {
            if (str ~= drinks[i][NAZWA][j][0][przyp] || (sizeof(drinks[i][PRZYM]) == 2 &&
                str ~= oblicz_przym(drinks[i][PRZYM][0], drinks[i][PRZYM][1],
                przyp, drinks[i][NAZWA][j][2], 0) +
                " " + drinks[i][NAZWA][j][0][przyp]))
            {
                if (wynos)
                    return drink_na_wynos(i, ilosc, wynos);

                return drink_na_miejscu(i);
            }
        }
    }

    /*
     *  Jakis produkt, potrawa.
     */

    int lok = -1;
    int wyn = -1;

    size = sizeof(food);

    for (i = 0; i < size; i++)
    {
        insize = sizeof(food[i][NAZWA]);

        for (j = 0; j < insize; j++)
        {
            if (str ~= food[i][NAZWA][j][0][PL_BIE] || (sizeof(food[i][PRZYM]) == 2 &&
                str ~= oblicz_przym(food[i][PRZYM][0], food[i][PRZYM][1],
                PL_BIE, food[i][NAZWA][j][2], 0) +
                " " + food[i][NAZWA][j][0][PL_BIE]))
            {
                lok = i;
            }
        }
    }


    size = sizeof(wynos_food);

    for (i = 0; i < size; i++)
    {
//        write("1\n");
        for (k = 0; k < sizeof(wynos_food[i][WYNOS_NAZWA]); ++k)
        {
            //if (str ~= wynos_food[i][WYNOS_NAZWA][k][0][PL_BIE])
            if (str ~= wynos_food[i][NAZWA][k][0][PL_BIE] || (sizeof(wynos_food[i][PRZYM]) == 2 &&
                str ~= oblicz_przym(wynos_food[i][PRZYM][0][0], wynos_food[i][PRZYM][1][0],
                PL_BIE, wynos_food[i][NAZWA][k][2], 0) +
                " " + wynos_food[i][NAZWA][k][0][PL_BIE]))
                wyn = i;
        }
    }

    if(wyn != -1 && lok == -1 && query_verb() ~= "zam�w")
        return notify_fail("Mo�esz to jedynie kupi� na wynos.\n");
    if(lok != -1 && wyn == -1 && query_verb() == "kup")
        return notify_fail("Mo�esz to jedynie zam�wi� na miejscu.\n");

    if(lok != -1 && query_verb() ~= "zam�w")
        return food_na_miejscu(lok);
    if(wyn != -1 && query_verb() ~= "kup")
        return food_na_wynos(wyn, 1);

    notify_fail("Nie ma na sk�adzie niczego takiego.\n");
    return 0;
}

/*
 * Nazwa funkcji : stawianie
 * Opis          : Gracz cos postawil sprawdzmy czy mozemy spelnic jego
 *		   zadanie
 * Argumenty     : string str - co zamowil ?
 * Zwraca   	 : 0 - jesli nie udalo sie wykonac jego zadania
 *		   1 - jesli udalo sie spelnic jego zadanie
 */
int
stawianie(string str)
{
    object *komu;
    string co;
    int i;
    int j;
    int size;
    int insize;

    notify_fail("Postaw komu co ?\n");
	if(!str)
	    return 0;


	if(!sizeof(filter(all_inventory(ENV(TP)),&->query_karczmarz())))
	{
		notify_fail("Zdaje si�, �e nie ma tu nikogo, kto m�g�by ci� obs�u�y�.\n");
		return 0;
	}

    if (!parse_command(lower_case(str), environment(this_player()),
		"%l:" + PL_CEL + " %s", komu, co))
	return 0;

    komu = CMDPARSE_STD->normal_access(komu, 0, this_object());
    if (!sizeof(komu))
	return 0;

    size = sizeof(drinks);

    for (i = 0; i < size; i++)
    {
        insize = sizeof(drinks[i][NAZWA]);

        for (j = 0; j < insize; j++)
            if (co ~= drinks[i][NAZWA][j][0][PL_BIE])
                return stawianie_drinka(i, komu[0], this_player());
    }

/*
 *  Jakis produkt, potrawa.
 */
    size = sizeof(food);

    for (i = 0; i < size; i++)
    {
        insize = sizeof(food[i][NAZWA]);

        for (j = 0; j < insize; j++)
	    if (co ~= food[i][NAZWA][j][0][PL_BIE])
                return postaw_zarcie(i, komu[0], this_player());
    }

    notify_fail("Nie ma na sk�adzie niczego takiego.\n");
    return 0;
}

/*
 * Nazwa funkcji : add_drink
 * Opis          : Dodaje trunek serwowany na miejscu i/lub na wynos.
 * Argumenty     : nazwa_mia       - nazwa plynu w mianowniku
 *		   nazwa_dop       - nazwa plynu w dopelniaczu
 *		   nazwa_bie       - nazwa plynu w bierniku
 *		   opis_plynu_dop  - opis plynu w dopelniaczu
 * 		   obj_pojemnika   - objetosc pojemnika na miejscu
 *		   vol             - procent alkoholu
 * 		   cena	           - cena w groszach
 *		   rodzaj          - rodzaj(meski itp.)
 *                 rodzaj_poj	   - rodzaj pojemnika(definicje w /sys/pub.h)
 *		   nazwa_lyk	   - slowo uzywane przy lykaniu(np.wodki)
 *		   adj		   - opcjonalnie przymiotniki pojemnika
 *		   dlugi_opis_pojemnika - dlugi opis pojemnika
 *		   obj_na_wynos    - opcjonalnie objetosc pojemnika na wynos
 *
 *
 */
varargs void
add_drink(mixed nazwa, mixed adj, //mixed nazwa_dop, mixed nazwa_bie,
        string opis_plynu_dop,
        int obj_pojemnika,
        int vol, int cena, /*mixed rodzaj,*/ mixed rodzaj_poj, string nazwa_lyk,
	string dlugi_opis_pojemnika, int obj_na_wynos = 0)
{
    if (!nazwa)
        return ;

    if (!drinks)
        drinks = ({ });

    if (!pointerp(nazwa))
        nazwa = ({ nazwa });
//    if (!pointerp(nazwa_bie))
//        nazwa_bie = ({ nazwa_bie });

//    if (!pointerp(rodzaj))
//        rodzaj = ({ rodzaj });
//    if (!pointerp(rodzaj_poj))
//        rodzaj_poj = ({ rodzaj_poj });


    if (vol > 97) vol = 97;
    if (obj_pojemnika <  0) obj_pojemnika = 0;
    if (obj_pojemnika >  5000) obj_pojemnika = 5000;


//    drinks += ({ ({ nazwa_mia, nazwa_dop, nazwa_bie,
    drinks += ({ ({ map(nazwa, znajdz_nazwe), adj,
                    opis_plynu_dop,
                    obj_pojemnika,
                    vol, cena, /*rodzaj,*/ znajdz_nazwe(rodzaj_poj), nazwa_lyk,
		    dlugi_opis_pojemnika, obj_na_wynos }) });
}

/*
 * Function name: query_drinks
 * Description:   Query the drink array
 * Returns:	  The drink array
 */
mixed
query_drinks()
{
    return drinks;
}


/*
 * Function name: remove_drink
 * Description:   Remove a special drink, identified with id
 * Arguments:	  id - A identifying string
 * Returns:	  1 if removed
 */
int
remove_drink(string id)
{
    int i;

    for (i = 0; i < sizeof(drinks); i++)
	if (member_array(id, drinks[i]) >= 0)
	{
	    drinks = exclude_array(drinks, i, i);
	    return 1;
	}

    return 0;
}

/*
 * Nazwa funkcji : add_food
 * Opis          : Dodaje zarcie serwowane na miejscu i/lub na wynos.
 * Argumenty     : nazwa_mia       - nazwa zarcia w mianowniku
 *		   nazwa_dop       - nazwa zarcia w dopelniaczu
 *		   nazwa_bie       - nazwa zarcia w bierniku
 *		   opis_plynu_dop  - opis zarcia w dopelniaczu
 * 		   obj_pojemnika   - objetosc pojemnika na miejscu
 *		   vol             - procent alkoholu
 * 		   cena	           - cena w groszach
 *		   rodzaj          - rodzaj(meski itp.)
 *		   rodzaj_poj	   - rodzaj pojemnika(definicje w /sys/pub.h)
 *		   nazwa_lyk	   - slowo uzywane przy lykaniu(np.wodki)
 *		   adj		   - opcjonalnie przymiotniki pojemnika
 *		   dlugi_opis_pojemnika - dlugi opis pojemnika
 *		   obj_na_wynos    - opcjonalnie objetosc pojemnika na wynos
 *
 *
 */
varargs void
add_food(mixed nazwa, mixed adj, //mixed nazwa_dop, mixed nazwa_bie,
        string opis_plynu_dop,
        int obj_pojemnika,
        int vol, int cena, /*mixed rodzaj,*/ mixed rodzaj_poj, string nazwa_lyk,
	string dlugi_opis_pojemnika, int obj_na_wynos = 0)
{
    if (!nazwa)
        return;

    if (!food)
        food = ({ });

    if (!pointerp(nazwa))
        nazwa = ({ nazwa });
//    if (!pointerp(nazwa_bie))
//        nazwa_bie = ({ nazwa_bie });

//    if (!pointerp(rodzaj))
//        rodzaj = ({ rodzaj });
//    if (!pointerp(rodzaj_poj))
//        rodzaj_poj = ({ rodzaj_poj });

    if (obj_pojemnika <  0) obj_pojemnika = 0;
    if (obj_pojemnika >  5000) obj_pojemnika = 5000;


//    food += ({ ({ nazwa_mia, nazwa_dop, nazwa_bie,
    food += ({ ({ map(nazwa, znajdz_nazwe), adj,
                    opis_plynu_dop,
                    obj_pojemnika,
                    0, cena,
//		    rodzaj,
		    znajdz_nazwe(rodzaj_poj), nazwa_lyk,
		    dlugi_opis_pojemnika, obj_na_wynos }) });
}

/*
 * Nazwa funkcji : add_wynos_food
 * Opis		 : Dodaje do menu posilek do zjedzenia 'na wynos'.
 * Argumenty     : nazwy     - Odmiana nazw jedzenia w lp
 *		   pnazwy    - Odmiana nazw jedzenia w lmn
 *		   adj	     - Tablica z parami przymiotnikow
 *		   ilosc     - Ilosc zarcia (waga)
 *		   cena      - Cena zarcia (koszt kupna)
 *		   long      - Dlugi opis
 *		   rodzaj    - Rodzaj gramatyczny nazwy jedzenia
 *		   decay     - Czas psucia sie, ususzania sie, itp.
 *			       w sekundach (0 - nie psuje sie)
 *		   decay_adj - Tablica z para przymiotnikow, gdy jedzenia
 *			       nie da sie zjesc (standardowo - 'zepsuty')
 *				(opcjonalne)
 *		   short     - Odmiana krotkiego opisu w lp (opcjonalne)
 *		   pshort    - Odmiana krotkiego opisu w lmn (opcjonalne)
 */
public varargs void
add_wynos_food(mixed nazwa, /*string *pnazwy,*/ mixed *adj,
	 int ilosc, int cena, string long, /*mixed rodzaj,*/ int decay,
	 string *decay_adj, string *short/*, string *pshort*/)
{
    int ix, num;

//    if (sizeof(nazwy) % 6 != 0) return;
//    if (sizeof(pnazwy) % 6 != 0) return;

    if (!pointerp(nazwa))
	nazwa = ({ nazwa });

    if (!wynos_food)
	wynos_food = ({ });

//    if (!pointerp(rodzaj))
//	rodzaj = ({ rodzaj });

    if (!pointerp(adj) || !sizeof(adj))
        adj = 0;

    wynos_food += ({ ({ map(nazwa, znajdz_nazwe), adj, ilosc, cena, long,
	decay, short, decay_adj }) });
}

/*
 * Nazwa funkcji : ustaw_dlugie_opisy
 * Opis          : Ustawia dlugie opisy przy jedzeniu potrawy.
 * Argumenty     : nazwa_mia - nazwa potrawy, ktorej
 *                             opisy ustawiamy w mianowniku
 *		           opisy - tablica z opisami jedzenia
 */
void
ustaw_dlugie_opisy(string mia, string* opisy)
{
    int i, sz;

    for (i = 0, sz = sizeof(food); i < sz; i++)
    {
        if (mia ~= food[i][NAZWA][0][0][PL_MIA])
        {
            food[i][OPISY_JEDZENIA] = opisy;
            break;
        }
    }
}

/*
 * Nazwa funkcji : jedzenie
 * Opis          : Wywolywana do jedzenia potraw.
 * Argumenty     : opisy - opisy ktorego jedenia wyswietlamy
 *                 faza - ktora czesc opisu ma byc wyswietlona
 *                 jedzacy - obiekt gracza spozywajacego jedzenie
 *                 short - krotki opis jedzenia ktore jemy
 */

void
jedzenie(int opisy, int faza, object jedzacy, string short)
{
    if(jedzacy != this_player())
        set_this_player(jedzacy);

    if(jedzacy->query_attack())
    {
        jedzacy->catch_msg("Gwaltownym gestem odrzucasz jedzenie i stajesz do walki!\n");
        saybb(QCIMIE(jedzacy, PL_MIA) + " zrywa sie od stolu stajac do walki.\n");
        //jedzacy->add_prop(PLAYER_JE, 0);
        //jedzacy->add_prop(SIT_NIE_WSTAN ,0);
        jedzacy->command("$wstan");
        return;
    }

    if(!(food[opisy][OPISY_JEDZENIA]))
    {
        jedzacy->catch_msg("Po chwili otrzymujesz i zjadasz zamowiona potrawe.\n");
        saybb(QCIMIE(jedzacy, PL_MIA) + " zamawia i zjada na miejscu " +
	       short + ".\n");
        //jedzacy->add_prop(PLAYER_JE, 0);
        //jedzacy->add_prop(SIT_NIE_WSTAN ,0);
	}
    if(faza < sizeof(food[opisy][OPISY_JEDZENIA]))
    {
        if(faza == 0)
        {
                saybb(QCIMIE(jedzacy, PL_MIA) + " zaczyna jesc " +
                short + ".\n");
        }

        jedzacy->catch_msg(food[opisy][OPISY_JEDZENIA][faza] + "\n");

        if(faza == (sizeof(food[opisy][OPISY_JEDZENIA])-1))
        {
            saybb(QCIMIE(jedzacy, PL_MIA) + " konczy jesc " + short + ".\n");
            //jedzacy->add_prop(SIT_NIE_WSTAN ,0);
            //jedzacy->add_prop(PLAYER_JE, 0);
        }
        else
            set_alarm(6.0, 0.0, "jedzenie", opisy, ++faza, jedzacy, short);
    }
}

/*
 * Function name: query_food
 * Description:   Query the food array
 * Returns:       The food array
 */
mixed
query_food()
{
    return food;
}

/*
 * Function name: remove_food
 * Description:   Remove a special food, identified with id
 * Arguments:     id - A identifying string
 * Returns:       1 if removed
 */
int
remove_food(string id)
{
    int i;

    for (i = 0; i < sizeof(food); i++)
	if (member_array(id, food[i]) >= 0)
	{
	    food = exclude_array(food, i, i);
	    return 1;
	}

    return 0;
}
// RESZTA SZCZERZE MOWIAC WAS NIE OBCHODZI ALE JESLI CHCECIE TO OGLADAJCIE :D


int
stawianie_drinka(int i,object player,object stawiajacy)
{
    int cena;
    string na_wynos;
    object ob;
    cena = drinks[i][CENA] * drinks[i][OBJ_POJEMNIKA] / 1000;
    alco = drinks[i][VOL]  * drinks[i][OBJ_POJEMNIKA] / 100;


    if (!MONEY_ADD(stawiajacy, -cena))
    {
	notify_fail("Nie masz wystarczaj�cej ilo�ci pieni�dzy, " +
                    "�eby zap�aci�.\n");
	return 0;
    }

    ob=clone_object("/lib/pub_naczynie_picie");
    ob->ustaw_nazwe(drinks[i][RODZAJ_POJEMNIKA][0],
        drinks[i][RODZAJ_POJEMNIKA][1], drinks[i][RODZAJ_POJEMNIKA][2]);
    ob->set_pojemnosc(drinks[i][OBJ_POJEMNIKA]);
    ob->set_ilosc_plynu(drinks[i][OBJ_POJEMNIKA]);
    ob->set_nazwa_plynu_dop(drinks[i][NAZWA][0][0][PL_DOP]);
    ob->set_opis_plynu(drinks[i][OPIS_PLYNU_DOP]);
    ob->set_vol(alco);
    ob->ustaw_nazwe_plynu(drinks[i][NAZWA_PRZY_LYKANIU]);
    ob->set_dlugi_opis(drinks[i][DLUGI_OPIS_POJEMNIKA]);
    oblicz_sh_drinks(ob,i);
    ob->move(player,1);
    pub_hook_stawianie_drinka(drinks[i][RODZAJ_POJEMNIKA][0][PL_BIE] + " " + drinks[i][NAZWA][0][0][PL_DOP], cena, player, stawiajacy);
    return 1;
}

int
postaw_zarcie(int i,object player,object stawiajacy)
{
    int cena;
    string na_wynos;
    object ob;
    cena = food[i][CENA] * food[i][OBJ_POJEMNIKA] / 1000;


    if (!MONEY_ADD(stawiajacy, -cena))
    {
	notify_fail("Nie masz wystarczaj�cej ilo�ci pieni�dzy, " +
                    "�eby zap�aci�.\n");
	return 0;
    }

    ob=clone_object("/lib/pub_naczynie_jedzenie");
    ob->ustaw_nazwe(food[i][RODZAJ_POJEMNIKA][0],
	food[i][RODZAJ_POJEMNIKA][1], food[i][RODZAJ_POJEMNIKA][2]);
    ob->set_pojemnosc(food[i][OBJ_POJEMNIKA]);
    ob->set_ilosc_jedzenia(food[i][OBJ_POJEMNIKA]);
    ob->set_nazwa_jedzenia_dop(food[i][NAZWA][0][0][PL_DOP]);
    ob->set_nazwa_jedzenia_bie(food[i][NAZWA][0][0][PL_BIE]);
    ob->set_opis_jedzenia(food[i][OPIS_PLYNU_DOP]);
    ob->ustaw_nazwe_jedzenia(food[i][NAZWA_PRZY_LYKANIU]);
    ob->set_dlugi_opis(food[i][DLUGI_OPIS_POJEMNIKA]);
    oblicz_sh_food(ob,i);
    ob->move(player,1);
    pub_hook_stawianie_zarcia(food[i][RODZAJ_POJEMNIKA][0][PL_BIE] + " " + food[i][NAZWA][0][0][PL_DOP], cena, player, stawiajacy);
    return 1;
}

static void
tell_watcher(string str, object stawiajacy, object player)
{
   object *ob;
   int i;

   ob = FILTER_LIVE(all_inventory(environment(stawiajacy))) - ({ stawiajacy });
   ob -= ({ player });
   for (i = 0; i < sizeof(ob); i++)
   {
      if (CAN_SEE(ob[i], stawiajacy))
         ob[i]->catch_msg(str);
   }
}

/*
int
zjedz(string str)
{
    string co;
    mixed zarcie;
    int index;
    string *props=({});
    int size, insize;
    int j;
    int x;
    int ilosc;

    if(!str)
    {
	notify_fail("Co chcesz zje��?\n");
	return 0;
    }

    props = this_player()->query_props();

    if (member_array(PLAYER_POST, props) < 0)
    {
	notify_fail("Co chcesz zje��?\n");
	return 0;
    }

    zarcie = this_player()->query_prop(PLAYER_POST);

    size = sizeof(zarcie);
    for (x = 0; x < size; x++)
    {
        index = zarcie[x][0];
	insize = sizeof(food[index][NAZWA_BIE]);
        for (j = 0; j < insize; j++)
        {
	    if (str ~= food[index][NAZWA_BIE][j])
    	    {
	    	if (!this_player()->eat_food(food[index][WAGA], 1))
	    	{
		    notify_fail("Nie s�dzisz, aby� by�" +
			this_player()->koncowka("", "a", "o") +
			" w stanie zje�� a� tyle.\n");
		    return 0;
	    	}

		ilosc = zarcie[x][1];
		ilosc -= 1;

		if (ilosc == 0)
		{
		    zarcie = exclude_array(zarcie, x, x);
		    if (!sizeof(zarcie))
			this_player()->remove_prop(PLAYER_POST);
		}
		else
		{
		    zarcie[x][1] = ilosc;
		    this_player()->change_prop(PLAYER_POST, zarcie);
		}
	    	this_player()->eat_food(food[index][WAGA], 0);
		pub_hook_i_eat_postawione(food[index][OPIS_POSTAWIONEGO], this_player());
    	    }
        }
    }
    return 1;
}
*/

void
oblicz_sh_drinks(object ob, int i)
{
    string *lp = ({ });
    string *lm = ({ });
    int ind;
    int size;

    size = sizeof(drinks[i][RODZAJ_POJEMNIKA][0]);
    for (ind = 0; ind < size; ind++)
    {
	lp += ({ drinks[i][RODZAJ_POJEMNIKA][0][ind] + " " +
		    drinks[i][NAZWA][0][0][PL_DOP] });
    }

    size = sizeof(drinks[i][RODZAJ_POJEMNIKA][1]);
    for (ind = 0; ind < size; ind++)
    {
        lm += ({ drinks[i][RODZAJ_POJEMNIKA][1][ind] + " " +
    		    drinks[i][NAZWA][0][0][PL_DOP] });
    }

    ob->ustaw_shorty(lp, lm);
}

void
oblicz_sh_food(object ob, int i)
{
    string *lp = ({ });
    string *lm = ({ });
    int ind;
    int size;

    size = sizeof(food[i][RODZAJ_POJEMNIKA][0]);
    for (ind = 0; ind < size; ind++)
    {
	lp += ({ food[i][RODZAJ_POJEMNIKA][0][ind] + " " +
		    food[i][NAZWA][0][0][PL_DOP] });
    }

    size = sizeof(food[i][RODZAJ_POJEMNIKA][1]);
    for (ind = 0; ind < size; ind++)
    {
        lm += ({ food[i][RODZAJ_POJEMNIKA][1][ind] + " " +
    		    food[i][NAZWA][0][0][PL_DOP] });
    }

    ob->ustaw_shorty(lp, lm);
}

public int
zwrot(string str)
{
    object *obs;
    object *karczmarze = filter(all_inventory(ENV(TP)),&->query_karczmarz());

    notify_fail("Co chcesz zwr�ci�? Mo�e naczynia?\n");

    if(!sizeof(karczmarze))
    {
        notify_fail("Zdaje si�, �e nie ma tu nikogo, kto m�g�by ci� obs�u�y�.\n");
        return 0;
    }

    //notify_fail("Chcesz zwr�ci� jakie� naczynie? Wystarczy, �e od�o�ysz je gdzie�.\n");

    if(!str)
       return 0;

    if (!parse_command(str, all_inventory(this_player()),
        " %i:" + PL_BIE, obs))
    {
        return 0;
    }

    obs = NORMAL_ACCESS(obs, 0, 0);

    if (!sizeof(obs))
       return 0;

    if(sizeof(filter(obs, &->is_naczynie())) != sizeof(obs))
    {
        notify_fail("Ale to nie jest naczyniem.\n");
        return 0;
    }

    //Dopisujemy najpierw opuszczanie.
    foreach(object x : obs)
    {
        if(x->query_wielded())
            TP->command("opu�� " + OB_NAME(x));
    }

    TP->catch_msg("Oddajesz "+COMPOSITE_DEAD(obs,PL_BIE)+
        " "+QIMIE(karczmarze[0],PL_CEL)+".\n");
    saybb(QCIMIE(TP,PL_MIA)+" oddaje "+ COMPOSITE_DEAD(obs,PL_BIE)+
        " "+QIMIE(karczmarze[0],PL_CEL)+".\n");

    foreach(object x : obs)
        x->remove_object();

    return 1;
}

public string
pub_menu()
{
    string ret = "";
    string line;

    ret += "Napoje:\n\n";
    foreach (mixed x : drinks)
    {
	line = "";
	if (sizeof(x[PRZYM]))
	    line += oblicz_przym(x[PRZYM][0], x[PRZYM][1], PL_MIA, x[NAZWA][0][2], 1) + " ";
	line += x[NAZWA][0][0][PL_MIA];
	line = sprintf("%-30s%5d", line, x[CENA]);
	ret += capitalize(line) + "\n";
    }
    ret += "\n\n";

    ret += "Jad^lo:\n\n";
    foreach (mixed x : food)
    {
	line = "";
	if (sizeof(x[PRZYM]))
	    line += oblicz_przym(x[PRZYM][0], x[PRZYM][1], PL_MIA, x[NAZWA][0][2], 1) + " ";
	line += x[NAZWA][0][0][PL_MIA];
	line = sprintf("%-30s%5d", line, x[CENA]);
	ret += capitalize(line) + "\n";
    }
    ret += "\n";

    return ret;
}

