/**
 * \file /lib/material.c
 *
 * Ten plik zawiera wsparcie dla przedmiot�w z materia�ami.
 */

/*
 * Materia�y, kt�re s� udost�pniane przez dan� rzecz.
 */
private mapping mat_zaw;

/**
 * Ustawia materia�y udost�pniane przez dan� rzecz.
 *
 * @param material nazwa materia�u
 * @param amount ustawia ilo�� udost�pnianego materia�u
 * @param fill czy materia� ma zosta� wype�niony
 */

public void
set_material_capacity(string material, int amount, int fill = 1)
{
  if ((!stringp(material)) || (amount < 0)) {
    return;
  }
  if (!mappingp(mat_zaw)) {
    mat_zaw = ([ ]);
  }
  if (pointerp(mat_zaw[material])) {
    mat_zaw[material][1] = amount;
    if (fill) {
      mat_zaw[material][0] = amount;
    }
    if (mat_zaw[material][0] > mat_zaw[material][1]) {
      mat_zaw[material][0] = mat_zaw[material][1];
    }
  }
  else {
    mat_zaw[material] = ({ fill ? amount : 0, amount });
  }
  if (amount == 0) {
	  mat_zaw = m_delete(mat_zaw, material);
  }
}

/**
 * Ustawia aktualn� zawarto�� materia��w udost�pnianych przez dan� rzecz.
 *
 * @param material nazwa materia�u
 * @param amount aktualna zawarto�� materia�u
 */

public void
set_material_amount(string material, int amount)
{
  if ((!stringp(material)) || (amount < 0)) {
    return;
  }
  if (!mappingp(mat_zaw)) {
    mat_zaw = ([ ]);
  }
  if (pointerp(mat_zaw[material])) {
    mat_zaw[material][0] = amount;
  }
  if (mat_zaw[material][0] > mat_zaw[material][1]) {
    mat_zaw[material][0] = mat_zaw[material][1];
  }
}

/**
 * Zwraca limit materia�u udost�pnianego przez dan� rzecz.
 *
 * @param material nazwa materia�u
 *
 * @return limit danego materia�u
 */

public int
query_material_capacity(string material)
{
  if (!stringp(material)) {
    return 0;
  }
  if (!mappingp(mat_zaw) || !pointerp(mat_zaw[material])) {
    return 0;
  }
  return mat_zaw[material][1];
}

/**
 * Zwraca aktualn� zawarto�� materia�u udost�pnianego przez dan� rzecz.
 *
 * @param material nazwa materia�u
 *
 * @return aktualna zawarto�� danego materia�u
 */

public int
query_material_amount(string material)
{
  if (!stringp(material)) {
    return 0;
  }
  if (!mappingp(mat_zaw) || !pointerp(mat_zaw[material])) {
    return 0;
  }
  return mat_zaw[material][0];
}

/**
 * Zwraca materia�y udost�pniane przez dan� rzecz.
 *
 * @return tablica nazwa materia��w
 */
public string*
query_materials()
{
  return m_indices(mat_zaw);
}

/**
 * Funkcja wywo�ywana przy zmianie zawarto�ci materia�u w danej rzeczy.
 *
 * @param material nazwa materia�u
 * @param oldValue stara warto�� materia�u
 * @param newValue nowa warto�� materia�u
 */
public void
notify_change_material_amount(string material, int oldValue, int newValue)
{
}

/**
 * Funkcja zwracaj�ca tekst dla auto_�adowania materia��w.
 *
 * @return tekst dla auto_�adowania materia��w.
 */
public string
query_material_auto_load()
{
    int first = 1;
    string toReturn = " #M#";

    foreach (string material : query_materials()) {
        if (!first) {
            toReturn += ";";
        }
        toReturn += material + "=" + query_material_amount(material) + "," +
            query_material_capacity(material);
    }

    return toReturn + "#M# ";
}

/**
 * Funkcja odtwarzaj�ca obiekt na podstawie tekstu zwr�conego wcze�niej
 * przez query_material_auto_load.
 *
 * @param arg tekst dla auto_�adowania
 *
 * @return pozosta�a cz�� tekstu do auto_�adowania
 */
public string
init_material_arg(string arg)
{
    if (arg == 0) {
        return 0;
    }
    string foobar, materials, rest;
    if (sscanf(arg, "%s#M#%s#M#%s", foobar, materials, rest) == 3) {
        foreach (string material : query_materials()) {
            set_material_capacity(material, 0);
        }
        foreach (string matnum : explode(materials, ";")) {
            string matnam;
            int amount, capacity;
            sscanf(matnum, "%s=%d,%d", matnam, amount, capacity);
            set_material_capacity(matnam, capacity);
            set_material_amount(matnam, amount);
        }
    }
    return rest;
}
