/* lib stra�nika miast. By Vera.
 *
 *
 */

#pragma strict_types
#include <macros.h>
#include <stdproperties.h>
#include <filter_funs.h>
#define MA_SIGNAL_FIGHT "_ma_signal_fight"
#define MIASTO "0"
//CHWILOWO TUTAJ!! TODO!
#define BIURO_ZNALEZIONYCH "/d/Standard/wiz/biuro_rzeczy.c"
#define SCIGA_PATROL "_sciga_patrol"

#define DBG(x) wmsg(x+"\n");

inherit "/std/humanoid";
inherit "/std/act/action";
inherit "/std/act/trigaction.c";

mixed Id_patrolu;
int ma_signal_fight=0;

int komendy();

void
init_straznik()
{
  //add_action(komendy, "", 1);
}

void
create_straznik()
{
}

/*
 * Funkcja pokazuje file_name lokacji do ktorej przeniesione maja byc
 * skonfiskowane bronie etc.
 */
string
query_magazyn()
{
   return "/d/Standard/Redania/Rinde/Wschod/Garnizon/lokacje/magazyn_kwatermistrza";
}

void
set_id_patrolu(mixed id)
{
    Id_patrolu = id;
}

mixed
query_id_patrolu()
{
    return Id_patrolu;
}

nomask void
signal_enter(object ob, object skad)
{
    // Je�li 'ob' jest graczem i jeste�my dow�dc�, w�wczas
    // powiadamiamy engine.
    if (interactive(ob) && !TO->query_leader())
        query_admin()->notify_enter(Id_patrolu, ob);
}

nomask int
signal_leave(object ob, object dokad)
{
    // Je�li 'ob' jest graczem i jeste�my dow�dc�, w�wczas
    // powiadamiamy engine.
    if (interactive(ob) && !TO->query_leader())
        query_admin()->notify_leave(Id_patrolu, ob);

    return 0;
}

nomask void
signal_attack(object kto, object kogo)
{
/*    // Je�li 'kto' jest graczem i jeste�my dow�dc�, w�wczas
    // powiadamiamy engine.
    if (interactive(kto) && !TO->query_leader())
		query_admin()->notify_attack(Id_patrolu, kto, kogo);

/*
	//to jest w notify_attack!
    //Jesli 'kto' jest oficerem lub straznikiem, to wspieramy,
    //jesli sami nie walczymy.
    if((kto->query_oficer() || kto->query_straznik()) && !TO->query_attack())
	    command("wesprzyj " + kto->query_real_name(PL_BIE));
*/

    if((kogo->query_oficer() || kogo->query_straznik()) ||
      (kogo->query_npc() && kogo->is()) && interactive(kto))
    {
        if(!kto->query_prop(SCIGA_PATROL))
        {
            query_admin()->add_enemy(kto->query_real_name(),3);
            query_admin()->log("za atakowanie npca", kto,kogo);
//            query_admin()->log("za walke w miescie", kto);
            query_admin()->rozpetaj_walke(Id_patrolu, kto);
        }
    }
}

int
check_living()
{
    if(!TO->query_leader())
        query_admin()->init_disarm(Id_patrolu, TP);
}

int
komendy()
{
    if(!query_admin()->query_enemy_lvl(TP->query_real_name()))
        return query_admin()->disarm_cmds(TP, query_verb(), Id_patrolu);
}

public void
guard_signal_stop_fight(object kto, object kogo)
{
    if((kogo->query_oficer() /*|| kogo==TO*/) && query_admin()->query_enemy_lvl(kto->query_real_name()) < 3)
    {
//         query_admin()->zeruj_grabiony(); // Zawsze chcieli�my grabi� gracza?
        query_admin()->grabiez(Id_patrolu,kto);
        command("przestan atakowac");
        query_admin()->zaprzestan_walki(Id_patrolu,kto);

        set_alarm(0.1, 0.0, &unset_follow());

    }
}

public void
wesprzyj(object ob)
{
    command("wesprzyj " + ob->query_real_name(PL_BIE));

    if(query_attack())
    {
        switch(random(5))
        {
            case 0: command("warknij"); break;
            case 1: command("krzyknij No to teraz masz w ryja!"); break;
            case 2: command("splun"); break;
        }

        command("wesprzyj " + ob->query_real_name(PL_BIE));
        return ;
    }
}

int
usuwamy_prop_signal_fight()
{
    TO->remove_prop(MA_SIGNAL_FIGHT);
    return 1;
}

int
signal_dobywa_bron(object kogo)
{
    query_admin()->init_disarm(Id_patrolu,kogo);
    return 1;
}

int
kontynuujemy_akcje_pvp(object atakowany)
{
    object cel=atakowany->query_attack();

    command("powiedz Dobra, koniec tego dobrego. Ch�opaki!");

    if(cel) //Czasem to mo�e nie zaskoczy�
    {
        if(cel->query_origin() != MIASTO && atakowany->query_origin() == MIASTO)
        { //je�li atakowany jest z miasta, a atakuj�cy nie, to wspieraj� atakowanego
            query_admin()->init_disarm(Id_patrolu, cel);
            //V("atakowany jest z rinde!");
        }
        else if(cel->query_origin() == MIASTO && atakowany->query_origin() != MIASTO)
        { //Je�li to atakuj�cy jest z miasta, a atakowany nie, to pomagaj� atakuj�cemu
            query_admin()->init_disarm(Id_patrolu, atakowany);
            //V("atakuj�cy jest z rinde!");
        }
        else //je�li obaj nie s�, lub obaj s� z miasta, to bierzemy ich razem :P
        {
            query_admin()->init_disarm(Id_patrolu, atakowany);
            query_admin()->init_disarm(Id_patrolu, cel);
        }
        //set_alarm(1.0,0.0,"take_weapon",atakowany->query_attack());
    }
    else
        query_admin()->init_disarm(Id_patrolu, atakowany); //bo kogo innego, skoro nie ma wroga? ;)
    //set_alarm(2.0,0.0,"take_weapon",atakowany);

    return 1;
}

void
hook_kontynuujemy_akcje()
{
	switch(random(6))
    {
        case 0: command("emote m�wi stanowczo: Sami si� tego kurwa jego ma� "+
                        "prosili�cie! Oddzia�! Bra� ich!"); break;
        case 1: command("krzyknij Oddzia�! Bra� ich!"); break;
        case 2: command("krzyknij Bra� ich! Awanturnik�w zasranych!"); break;
		case 3: command("krzyknij Do ataku!"); break;
        case 4: command("powiedz No to si� zabawimy."); break;
		default: break;
    }
}


int
kontynuujemy_akcje(object atakowany,object gdzie)
{
    hook_kontynuujemy_akcje();

    object atakujacy=atakowany->query_attack();
    /*if(environment(atakujacy) != environment(TO)) //Cel umknal
    {                                      //wiec jest latwiejsze zadanie, bo wiadomo, ze to on zawinil:>
        set_alarm(0.1, 0.0, &set_follow(atakujacy, 0.1,0));
        //TODO...?
    }*/

    //set_alarm(0.1, 0.0, &set_follow(atakujacy, 0.1,0));

    if(atakujacy && atakowany->query_npc() && atakowany->is_humanoid())
        query_admin()->init_disarm(Id_patrolu, atakujacy);
//     else if(atakujacy->query_npc() && !atakowany->is_humanoid())
    else if(atakowany && atakujacy->query_npc() && atakujacy->is_humanoid())
        query_admin()->init_disarm(Id_patrolu, atakowany);

    //ma_signal_fight=0;
    set_alarm(10.0,0.0, "usuwamy_prop_signal_fight");

    return 1;
}

void sprawdzmy_niestojmy_jak_kolki(mixed patrol, object wrog)
{
	set_alarm(20.0,0.0,"sprawdz_czy_jeszcze_walka",patrol,wrog);
}


void
hook_bija_sie_gracze()
{
	switch(random(6))
    {
        case 0: command("powiedz He, he! Ale si� lej�!"); break;
        case 1: command("powiedz Ho ho!");
                       command("klasnij");
                       break;
        case 2: command("powiedz Uuu! Patrzcie ich, jak si� pior�!"); break;
        case 3: command("powiedz He, he! Bijom si� jak baby!"); break;
		case 4: command("powiedz Przesta�cie!"); break;
		case 5: command("powiedz Natychmiast przesta�cie!"); break;
    }
}

void
hook_bija_sie()
{
    switch(random(6))
    {
        case 0: command("krzyknij Sta� kurwiszony jedne!"); break;
        case 1: command("krzyknij Sta�!"); break;
        case 2: command("krzyknij Co tu si� wyrabia?! Sta� m�wi�!"); break;
        case 3: command("krzyknij Sta�! Ale to ju�!"); break;
        case 4: command("krzyknij O�esz, ty! Sta�!"); break;
		case 5: command("powiedz Natychmiast przesta�cie!"); break;
    }
}

//Sygnal o walce rozpoczetej. Przesylany do wszystkich livingow na sasiednich lokacjach, ktorzy to przesylaja
//go czasem dalej...
int
signal_fight(object atakowany, object gdzie)
{

    if(atakowany->query_attack() && !TO->query_prop(MA_SIGNAL_FIGHT)/*ma_signal_fight==0*/ &&
      !TO->query_leader())
    {
        TO->add_prop(MA_SIGNAL_FIGHT,1);//ma_signal_fight=1;

		if(interactive(atakowany->query_attack()))
			(atakowany->query_attack())->add_prop(SCIGA_PATROL,Id_patrolu);
		query_admin()->stop_patrol(Id_patrolu);
		sprawdzmy_niestojmy_jak_kolki(Id_patrolu, atakowany->query_attack());

/*find_player("vera")->catch_msg("OFICER ZLAPAL SYGNAL! Atakowany: "+file_name(atakowany)+", gdzie: "+file_name(gdzie)+"\n");*/
        query_admin()->idz_do_gracza(Id_patrolu,atakowany);

        if(interactive(atakowany) && (interactive(atakowany->query_attack())
           || !atakowany->query_attack())) //Jesli walka jest player vs player
        {
			hook_bija_sie_gracze();

           set_alarm(4.0+itof(random(10)),0.0,"kontynuujemy_akcje_pvp",atakowany);
        }
        else
        {
			hook_bija_sie();
            set_alarm(1.0,0.0,"kontynuujemy_akcje",atakowany,gdzie);
        }

//        set_alarm(5.0,0.0,"signal_fight",atakowany,gdzie);
    }
    return 1;
}

/*Poni�ej: Do systemu reakcji na kradzie�e!*/

/* Ta funkcja przenosi rzeczy do biura rzeczy znalezionych,
 * chyba, �e nale�a�y do jakiego� npca, to od razu przerzuca do jego
 * inventory, automagicznie. Bo tego nie wida� :)
 * Tak samo przenosi rzeczy, kt�re by�y w '0' sublokacji
 * (czyli le�a�y swobodnie na lokacji), i w tej lokacji obecnie nikogo
 * te� nie ma z graczy. :) Automagicznie w ten spos�b pozbywam si� problemu
 * z zalegaj�cymi rzeczami w biurze rzeczy znalezionych, kt�rych �adnemu
 * wizowi p�niej nie chce si� roznosi�.
 * I nie gada� mi tu o reali�mie, bo realizm to jedno, a praktyka robi
 * swoje. Nie ma nigdy komu tego roznosi�.
 * V.
 */
int
przenies_do_biura(object co)
{
    string miejsce = explode(co->query_orig_place()[0],"|")[0];

    if((find_object(miejsce)->query_npc()) ||
    (!sizeof(FILTER_PLAYERS(all_inventory(find_object(miejsce)))) &&
        co->query_orig_place()[1] == "0") )
    {
DBG("Pomy�lnie przeniesiono skradzion� rzecz: "+co->short()+" do :"+QIMIE(find_object(miejsce),0)+"\n");
        co->move(find_object(miejsce),1);
    }
    else
        co->move(BIURO_ZNALEZIONYCH);

	return 1;
}

//wywo�ywane tylko jak wracamy si� spod bramy i innych granic terenu dzia�ania.
//Wi�c nietrzeba tu wywa�a� drzwi etc.
void
wracamy_sie()
{
    mixed powrot=znajdz_sciezke(ENV(TO),
            query_admin()->query_miejsce_startu(Id_patrolu));

    foreach(mixed x:powrot)
    {
        TO->command(x);
    }

    query_admin()->start_patrol(Id_patrolu);
}

void
hook_uciekl_poza_brame()
{
	switch(random(5))
    {
          case 0:
           command("krzyknij Sta�!");
          break;
          case 1:
                command("krzyknij Jeszcze ci� dorwiemy!");
          break;
          case 2:
            command("powiedz Ech, ch�do�one rozb�jniki.");
          break;
          case 3:
             command("splun");
             command("powiedz Tfu! Tch�rze zasrane.");
          case 4:
             command("krzyknij A spr�buj tu tylko wr�ci� mendo!");
          break;
     }
}

public int
prevent_enter_env(object dest)
{
    string dokad;
    if (catch(dokad = explode(file_name(dest),"/")[3]+"/"+explode(file_name(dest),"/")[4]))
        return 0;
    if(dokad != query_admin()->query_teren_dzialania(Id_patrolu))
    {
        if(TO->query_follow())
        {
            if(TO->query_oficer())
            {
				hook_uciekl_poza_brame();
//DBG("0");
                set_alarm(0.1, 0.0, &wracamy_sie());
//DBG("1");
            //query_admin()->usun_wroga_i_kontynuuj_patrol(Id_patrolu,TO->query_attack());
            }
            TO->unset_follow();
            if(TO->query_attack())
                TO->stop_fight(TO->query_attack());
            return 1;
        }
    }
    return 0;
}

void
odpal_kradzione()
{
	query_admin()->sprawdz_kradzione(Id_patrolu);
}

void
enter_env(object dest, object old)
{
    if(TO->query_oficer())
        set_alarm(0.1,0.0,"odpal_kradzione");
}

/*
 *Ta funkcja wywo�ywana przez do_die() jest ostatni�
 *desk� ratunku dla graczy, kt�rzy by� mo�e niezas�u�yli na �mier�,
 *lecz...c�, jako� dziwnie wysz�o :/
 * Zwraca: 1 je�li ma NIE umiera�.
 */
int
czy_napewno_zasluzyl(object kto)
{
	int e_lvl = query_admin()->query_enemy_lvl(kto->query_real_name());

	if(e_lvl <= 2)
	{
DBG("!!!!!prawie umar� "+kto->query_real_name()+" niezas�u�enie od stra�y!\n");
		query_admin()->zaprzestan_walki(Id_patrolu,kto);
		query_admin()->usun_wroga_i_kontynuuj_patrol(Id_patrolu, kto);
		return 1;
	}
	else
		return 0;
}
