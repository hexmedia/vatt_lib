/**
 * \file /lib/straz/odkupianie_win.c
 *
 * Dzi�ki temu plikowi gracze mog� za op�at� odkupi� swoje winy,
 * cena wacha si� od 10 do 500 koron.. Jest lekko modyfikowana przez
 * status mieszka�ca, mieszka�cy Rinde w Rinde zap�ac� mniej itd.
 *
 *
 * @example
 * inherit "/std/monster.c";
 *
 * void create_monster()
 * {
 *     ustaw_admina_strazy("SCIEZKA_DO_ADMINA_STRAZY/admin.c");
 *     config_odkupiciel();
 * }
 *
 * string query_pomoc()
 * {
 *     return "JAKI� TEKST POMOCY\n + ::query_pomoc();
 * }
 *
 * void init()
 * {
 *     init_odkupiciel();
 * }
 *
 *
 * @author Krun
 * @date Listopad 2007
 * By�em tu. Vera. :P
 */

//DEFINICJE KOSZT�W ODKUPIENIA
#define KOSZT_ODKUPIENIA_LVL_1  2400    /* 10 koron */
#define KOSZT_ODKUPIENIA_LVL_2  31200   /* 130 koron */
#define KOSZT_ODKUPIENIA_LVL_3  120000  /* 500 koron */

//Dla prostych dzia�a� finansowych najlepiej u�ywa� tego liba, bo je�li b�dziemy co� zmienia� nie bedzie potrzeby
//zmieniania ca�ego systemu, wystarczy zmieni� ten plik:)
inherit "/lib/trade.c";

#include <pl.h>
#include <macros.h>

public object
find_admin()
{
    return TO->query_admin();
}

public string odkupiciel_hook_odpowiedz_na_pytanie_o_winy()
{
    object admin = find_admin();

    if(!admin)
        return "";

    int lvl = admin->query_enemy_lvl(TP->query_real_name());

    string winy;

    switch(lvl)
    {
        case 0:
            TO->command("powiedz do " + OB_NAME(TP) + " Ale� ty nie masz nic na sumieniu... "+
                "Ale zawsze mo�esz postawi� mi piwo!");
            TO->command("za�miej si�");
            return "";
        case 1:
            switch(random(2))
            {
                case 0:
                    winy = "Masz na sumieniu tylko lekkie przewinienia, "+
                        "ich odkupienie nie b�dzie ci� wiele kosztowa�o.";
                    break;
                case 1:
                    winy = "Nie pochwalamy twoich uczynk�w jednak my�l�, �e "+
                        "za drobn� op�at� mo�na przymkn�� oko na to co do tej pory " +
                        "zwojowa�e�.";
                    break;
            }
            break;
        case 2:
            winy = "Twoje dotychczasowe post�powanie jest karygodne! - �cisza g�os - "+
                "My�l�, �e uda nam si� jako� dogada� co do op�aty za przymkni�cie na nie oka.";
            break;
        case 3:
            winy = "Ty... Ty ju� dawno powiniene� wisie�. Nie wiem czy powinienem da� ci kolejn� " +
                "szans�. Ale niech strac�. Wesprzesz stra� w walce z takimi jak ty, a my "+
                "w zamian pozwolimy ci �y�... - �ciszaj�c znacznie g�os "+
                "dodaje: To b�dzie ci� naprawd� s�ono kosztowa�.";
            break;
    }
    TO->command("powiedz do " + OB_NAME(TP) + " " + winy);
    return "";
}

public string odkupiciel_hook_odpowiedz_na_pytanie_o_koszt()
{
    object admin = find_admin();

    if(!admin)
        return 0;

    int lvl = admin->query_enemy_lvl(TP->query_real_name());

    int koszt;

    switch(lvl)
    {
        case 0:
            TO->command("powiedz do " + OB_NAME(TP) + " Ale� ty nie masz za co p�aci�.. Je�li "+
                "koniecznie chcesz mnie wesprze� to id� przynie� mi piwo!");
            return "";
        case 1:
            koszt = KOSZT_ODKUPIENIA_LVL_1;
            break;
        case 2:
            koszt = KOSZT_ODKUPIENIA_LVL_2;
            break;
        case 3:
            koszt = KOSZT_ODKUPIENIA_LVL_3;
            break;
    }

    TO->command("powiedz do " + OB_NAME(TP) + " Taaaak... Odkupienie win b�dzie ci� kosztowa�... A niech strac� " +
        text(split_values(koszt), 0));
    return "";
}

public int odkupiciel_hook_co_odkupic()
{
    TO->command("powiedz do " + OB_NAME(TP) + " Chcesz odkupi� swoje winy ta? Nie mam nic przeciwko.");
    TO->command("u�miechnij si� chciwie");
    NF("");
    return 0;
}

public int odkupiciel_hook_gracza_nie_stac()
{
    TO->command("powiedz do " + OB_NAME(TP) + " No �adnie �adnie. W balona mnie chcia�e� zrobi�? " +
        "Poszed� won bo zaraz stra� zawo�am i tak ci si� do dupy dobior�, �e ci� rodzona matka "+
        "nie pozna.");
    NF("");
    return 0;
}

public int odkupiciel_hook_nie_chce_przyjac()
{
    TO->command("powiedz do " + OB_NAME(TP) + " A spierdalaj ty mnie st�d!");
    NF("");
    return 0;
}

public int odkupiciel_hook_nie_ma_czego_odkupic()
{
    TO->command("powiedz do " + OB_NAME(TP) + " No tak... Mo�e i warto, �eby� wspar� lokalnych "+
        "str��w prawa, ale nie widze, �adnej winy kt�r� chcia�by� odkupi�. Wesprzyj lepiej... Hmmm. "+
        "A najlepiej to przynie� mi piwo.");
    TO->command("u�miechnij si� do " + OB_NAME(TP));
    NF("");
    return 0;
}

/**
 * Dzi�ki tej funkcji dodajemy mo�liwo�� zadawania pyta� o koszt odkupienia swoich win, itp... itd...
 */
public void config_odkupiciel()
{
    config_default_trade();
    set_money_greed_buy(100);
    set_money_greed_sell(100);
    set_money_greed_change(100);
    TO->add_ask(({"odkupienie win", "swoje winy", "swoje przewinienia", "odkupienie przewinie�", "winy", "przewinienia"}),
        VBFC_ME("odkupiciel_hook_odpowiedz_na_pytanie_o_winy"));
    TO->add_ask(({"koszt odkupienia win", "koszt odkupienie przewinie�", "kar� za grzechy", "koszt odkupienia", "koszt","cen� odkupienia","cen� wykupienia","wykup za przewinienia"}),
        VBFC_ME("odkupiciel_hook_odpowiedz_na_pytanie_o_koszt"));
}

string query_pomoc()
{
    return "U tego " + TO->query_rasa(PL_DOP) + " mo�esz za do�� s�on� op�at� "+
        "'odkupi�' swoje winy, dzi�ki czemu stra� miejska nie b�dzie ju� "+
        "d�u�ej ci� �ciga�... " +
        "Zapytaj go ile b�dzie kosztowa�o odkupienie twoich win a z pewno�ci� odpowie.\n";
}

public int odkup(string co)
{
    if(co !=  "winy" || co!="swoje winy")
        return odkupiciel_hook_co_odkupic();//NF("Co takiego chcesz odkupi�?\n");

    object admin = find_admin();

    if(!admin)
        return NF("Wyst�pi� b��d. Nie uda�o si� za�adowa� admina. Zg�o� go "+
            "opisuj�c okoliczno�ci w jakich do niego dosz�o.\n");

    int lvl = admin->query_enemy_lvl(TP->query_real_name());

    if(!lvl)
        return odkupiciel_hook_nie_ma_czego_odkupic();

    int koszt;

    switch(lvl)
    {
        case 1:
            koszt = KOSZT_ODKUPIENIA_LVL_1;
            break;
        case 2:
            koszt = KOSZT_ODKUPIENIA_LVL_2;
            break;
        case 3:
            koszt = KOSZT_ODKUPIENIA_LVL_3;
            break;
        default:
            koszt = 0;
    }

    if(!can_pay(koszt, TP))
        return odkupiciel_hook_gracza_nie_stac();

    if(pay(koszt, TP)[0] > 0)
        return odkupiciel_hook_nie_chce_przyjac();

    write("P�acisz " + text(split_values(koszt), 0) +
        " za odkupienie swoich win. Od tej pory nie b�dziesz ju� �cigany przez stra�.\n");
    saybb("Przekazuje " + TO->short(PL_CEL) + " jak�� kwot�.\n");

    admin->remove_enemy(TP->query_real_name(), 1);
    //I odkupi� winy... I mo�e biega�.
    //no to jeszcze odnotujmy w logach! Vera.
    admin->log("wykupi�"+TP->koncowka("","a","o")+
            " si� (mia�"+TP->koncowka("","a","o")+" lvl: "+
                lvl+",zap�aci�"+TP->koncowka("","a","o")+": "+koszt+")!",TP);
    return 1;
}

void init_odkupiciel()
{
    add_action(odkup, "odkup");
}