/**
 *
 * VEREK!!!!!!!!!!!!!!! ZR�B DO TEGO DOKUMENTACJE JAK NAJSZYBCIEJ:)
 *
 * \file /lib/straz/admin.c
 *
 * M�zg i serce stra�nik�w miejskich.
 * Dokumentacja b�dzie.
 * Vera.
 *
 * Lista enemy_lvls:
 * 1 Zlodziejaszek.Czyli zasluguje na �ask� ;)
 * 2 Awanturnik. Tak samo, ale traci ca�y inventory.
 * 3 Morderca. Zostaje zabity na �mier�:P ... <<Narazie �mier� to sobie mo�na tylko wybra�:P(KRUN)>>
 *
 * Drobne uwagi:
 * Dodany wr�g jest �cigany przez ka�dego stra�nika, niezale�nie od miasta.
 * <<I to trzebaby zmieni� bo to chyba drobna przesada.. Za grubsze przest�pstwa
 * mogliby �ciga� wsz�dzie, ale za te mniejsze tylko w tym mie�cie w kt�rym sie
 * je pope�ni�o... Pozatym dobrzeby by�o doda� jeszcze kilka lvl'i, tylko nie bardzo
 * mam pomys� jakich(KRUN)>>
 * -> Wi�c po co chcesz na si�� dodawa� nowe? Z�ota zasada: "Nie mn� byt�w ponad
 *    potrzeb�!". Vera.
 *
 * Patrol nie nadaje si� do niczego bez oficera i cho� jednego stra�nika.
 *
 *
 * TODO:
 *  - Sprawdzi� dlaczego zdarza si�, �e patrol nie ma w og�le cz�onk�w.
 */

#pragma strict_types
#pragma no_clone

#include <composite.h>
#include <macros.h>
#include <filter_funs.h>
#include <object_types.h>
#include <stdproperties.h>
#include <files.h>
#include <sit.h>

#define P_SKLAD                 0
#define P_START                 1
#define P_TRASA                 2
#define P_TRASA_POZ             3
#define P_TEREN                 4
#define P_LOKACJA_POWROTU       5
#define P_TRASA_POWROTU         6
#define P_TRASA_POWROTU_POZ     7
#define P_ALARM                 8
#define P_SIZE                  9

#define MA_SIGNAL_FIGHT         "_ma_signal_fight"
#define PRZEWROCONY             "_przewrocony"
#define SCIGA_PATROL            "_sciga_patrol"
#define GRABIONY                "_grabiony"

//tj. itof(PREDKOSC_CHODZENIA)
#define PREDKOSC_CHODZENIA random(10) + 1

#define DBG(x) ({find_player("vera"), find_player("krun")})->catch_msg("STRA� ZG�ASZA B��D:"+(x)+"\n");

static mixed Patrole = ([]);
static int Predkosc_min = 10;
static int Predkosc_max = 20;
//static mixed Wrogowie = ([ ]);
mixed Wrogowie = ([ ]);
mixed Skradzione = ([ ]);

int	chodzenie(mixed patrol);
int akcja=0;
int hook_ma_nieswoja_rzecz_on=0;
void sprawdz_kradzione(mixed patrol,object kogo);
void zaprzestan_walki(mixed patrol,object wrog);
void sprawdzmy_czy_opuscil(mixed patrol, object kogo);
public void log(string powod, object kto, object co = 0);

int reaguj_bija_npca_alarm;

 //Ostrze�eni przez patrol o posiadanie dobytej broni. indeks to
//real_name gracza, a wartosc to numer patrolu,
//przez ktorego gracz dostal ostrzezenie
mapping Ostrzezeni=([]);

//zapisywany tu jest ostatni wpis do loga a w logowaniu porownywanie czy jest inny
//zeby nie floodowac loga.
string last_log = "";

nomask void
create_admin()
{
    //restore_object("admin");



    foreach (mixed key, mixed val : Patrole)
    {
        if (LOAD_ERR(val[P_START]))
        {
            TO->log("ERROR! Patrol nr "+key+" si� nie za�adowa�.", TO);
            notify_wizards(TO, "Komunikat z admina stra�y: Patrol nr "+key+" si� nie za�adowa�!");
            continue;
        }

        val[P_SKLAD]->move(val[P_START]);
        val[P_SKLAD]->set_admin(this_object());
        val[P_SKLAD]->set_id_patrolu(key);

        if (sizeof(val[P_SKLAD]))
            map(val[P_SKLAD][1..], &(val[P_SKLAD][0])->team_join());

        val[P_ALARM] = set_alarm(4.0, 0.0, &chodzenie(key));
    }
}

nomask void
init_admin()
{
    add_action("info", "info");
    add_action("lista", "lista");
}

/*
 *Dla ka�dego patrolu z osobna. �eby ka�dy patrol nie ucieka� poza mury
 * (tj. nie ucieka� np. z /d/Standard/Redania/Rinde do
 *	/d/Standard/Redania/Rinde_okolice )
 */
int
ustaw_teren_dzialania(mixed patrol, string teren)
{
    if (!Patrole[patrol])
        return 0;

    if (!stringp(teren))
        return 0;

    Patrole[patrol][P_TEREN] = teren;

    return 1;
}

string
query_teren_dzialania(mixed patrol)
{
    mixed tmp = Patrole[patrol];

    return tmp[P_TEREN];
}

void
ustaw_predkosc_chodzenia(int min, int max)
{
    Predkosc_min = min;
    Predkosc_max = max;
}

int
dodaj_patrol(mixed patrol, mixed sklad)
{
    string path;
    mixed tmp = ({ });
    int i;

    if (Patrole[patrol])
        return 0;

    foreach (mixed x : sklad)
    {
        if (intp(x))
        {
            if (!stringp(path))
                continue;

            for (i = 0; i < x; i++) {
                object ob = clone_object(path);
                ob->init_arg(0);
                tmp += ({ ob });
            }
        }
        if (stringp(x))
            path = x;
    }

    tmp -= ({ 0 });

    Patrole[patrol] = allocate(P_SIZE);

    Patrole[patrol][P_SKLAD] = tmp;
    return 1;
}

int
ustaw_miejsce_startu(mixed patrol, string path)
{
    if (!Patrole[patrol])
        return 0;

    if (!stringp(path))
        return 0;

    Patrole[patrol][P_START] = path;

    return 1;
}

string
query_miejsce_startu(mixed patrol)
{
    if (!Patrole[patrol])
        return "";

    return Patrole[patrol][P_START];
}


int
ustaw_trase(mixed patrol, mixed trasa)
{
    if (!Patrole[patrol])
        return 0;

    if (!pointerp(trasa))
        return 0;

    foreach (mixed x : trasa)
        if (!stringp(x))
            return 0;

    Patrole[patrol][P_TRASA] = trasa;

    return 1;
}

varargs void
remove_enemy(string str, int odkupil_winy=0)
{
    if (!Wrogowie[str])
        return;

    m_delkey(Wrogowie, str);

    //save_object("admin");
}

void
add_enemy(string str, int lvl)
{
    //ALE JAK JEST WIZEM, TO MA ULG�. �EBY �MIECI W LOGACH MI TU
    //NIE BY�O, JAK WIZ CO� TESTUJE!
    if(find_player(str)->query_wiz_level())
        return;

    //profilaktycznie i niejako przy okazji sprawd�my, czy
    //wszyscy ci, kt�rzy s� �cigani na li�cie jeszcze �yj�.
    //Zdarzy� si� mo�e tak, �e kto� �cigany umrze na �mier�,
    //i to nie z r�ki stra�nika (bo wtedy jest usuwany z listy)
    //Wi�c sprawd�my se...
    object kto;
    foreach(mixed z : m_indices(Wrogowie))
    {
        kto = SECURITY->finger_player(z);
        if(!objectp(kto) || kto->query_final_death())
            remove_enemy(z);
    }

    if(!stringp(str))
        return;

    if(Wrogowie[str])
    {
        //dodane sprawdzanie, czy przypadkiem nie jest wi�kszy lvl...
        if(Wrogowie[str] > lvl)
            return;
    }

    if(!lvl)
        return;

    //A to jest debugger dla problemu, kt�rego wci�� nie
    //potrafi� wykry�...
    string x = str[strlen(str)-1..];
    if(x=="1" || x=="2" || x=="3" || x=="0")
    {
        notify_wizards(this_object(),"KURWA! Vera znowu co� zjeba�! Jak go "+
        "nie ma, to mu przeka�, �e to si� pojawi�o:\n"+
        "pr�ba add_enemy na: "+str+" o poziomie: "+lvl+"! "+
        "A tak�e wklej mu ostatnich kilka linijek z /d/Standard/log/STRAZ"+
        "\nPilne!!!\n");
        return;
    }

    Wrogowie[str] = lvl;

    //save_object("admin");
}

/*
 * Dodaj rzecz i jej zlodzieja do listy kradzionych
 * 1 arg - string rzecz
 * 2 arg - string player(real_name)
 * 3 arg - *string tablica przymiotnikow gracza
 *          (potrzebna do pozniejszych testow czy straz go
 *              rozpozna, czy nie)
 */
void
add_steal(string rzecz, string player, string *przymiotniki)
{
    if(Skradzione[rzecz])
        return;

    Skradzione[rzecz] = ({player,przymiotniki});
}

/*
 * Usun rzecz z listy kradzionych.
 * 1 arg - string rzecz
 */
void
remove_steal(string rzecz)
{
    m_delkey(Skradzione, rzecz);

    find_object(rzecz)->set_who_stole(({ }));
}

/*
 * Zwraca tablice string z przymiotnikami zlodzieja.
 * jako argument podajemy string rzecz
 */
string
*query_przymiotniki_zlodzieja(string rzecz)
{
    return rzecz->query_who_stole()[1..];
}

/*
 * czyscimy cala liste kradzionych rzeczy (!)
 */
void
clear_skradzione()
{
    Skradzione = ([ ]) ;
}

void
set_enemy_lvl(string str, int lvl)
{
    if(!Wrogowie[str] || Wrogowie[str] == lvl)
        return;
    Wrogowie[str] = lvl;
}

int
query_enemy_lvl(string str)
{
    if(!Wrogowie[str])
        return 0;
    else return Wrogowie[str];
}

/*
 * query_lista()
 * Potrzebna do list �ciganych
 */
mixed
query_lista()
{
    return Wrogowie;
}

int
lista(string str)
{
    if(!str)
    {
        write("Brak argumentu. Wybierz list� wrog�w lub list� kradzie�y.\n");
        return 1;
    }

    if(str ~= "wrog�w")
    {
        if(!m_sizeof(Wrogowie))
            write("Lista poszukiwanych jest pusta.\n");
        else
        {
            write("Oto uniwersalna lista wrog�w:\n");
            dump_array(Wrogowie);
        }
        return 1;
    }
    else if(str ~= "kradzie�y")
    {
        if(!m_sizeof(Skradzione))
            write("Lista skradzionych jest pusta.\n");
        else
        {
            write("Oto lista poszukiwanych rzeczy i ich z�odziei:\n");
            for(int i=0;i<sizeof(m_indices(Skradzione));i++)
            {
                string ob=m_indices(Skradzione)[i];
                string imie=UC(Skradzione[ob][0]);
                string *arr_przyms=({});//liczymy przyms

                for(int x=0;x<sizeof(Skradzione[ob][1]);x++)
                    arr_przyms+=({Skradzione[ob][1][x]});
                string przyms=COMPOSITE_WORDS(arr_przyms);

                write("\n"+(i+1)+".\n"+set_color(43)+"Obiekt:"+clear_color()+
                    "\t\t"+find_object(ob)->short(TO, PL_MIA)+" ("+
                    ob+")\n"+set_color(43)+"Z�odziej:"+clear_color()+"\t"+
                    imie+" ("+przyms+")\n");
            }
        }
        return 1;
    }
}


int
info(string str)
{
    int i;

    if (!str)
    {
        if (!m_sizeof(Patrole))
            write("Nie ma �adnych zarejestrowanych patroli.\n");
        else
        {
            write("Zarejestrowane patrole: " +
                COMPOSITE_WORDS(map(m_indexes(Patrole), &operator(+)("",))) +
                ".\n");
        }

        return 1;
    }

    if (sscanf(str, "%d", i) == 1)
    {
        mixed tmp = Patrole[i];

        if (!tmp)
        {
            write("Nie ma takiego patrolu.\n");
            return 1;
        }
        
        if(!ENV(tmp[P_SKLAD][0]))
        {
            write("B�ad! Patrol nie zosta� przeniesiony do krainy.\n");
            return 1;
        }

        write("Patrol " + i + ".\n\n");
        write("Sk�d:\n");
        foreach (object x : tmp[P_SKLAD])
        {
            if (x)
            write("  " + file_name(x) + "\n");
            else
            write("  (brak)\n");
        }
        write("Miejsce startu:\n  " + tmp[P_START] + "\n");

        if(sizeof(tmp[P_TRASA]))
            write("Trasa:\n  " + implode(tmp[P_TRASA], ", ") + ".\n");
        else
            write("Trasa:\n  Siedz� na dupie na jednej lokacji.\n");
        write("Aktualne miejsce pobytu:\n  " + file_name(ENV(tmp[P_SKLAD][0])) + "\n"); //
        write("Teren dzia�ania:\n   " +tmp[P_TEREN]+"\n");

        return 1;
    }

    notify_fail("Info / info <id_patrolu>.\n");
    return 0;
}


// Zatrzymuje patrol
int
stop_patrol(mixed patrol)
{
    mixed tmp = Patrole[patrol];

    if (!tmp)
        return 0;

    if (!tmp[P_ALARM])
        return 0;

    remove_alarm(tmp[P_ALARM]);
    tmp[P_ALARM] = 0;

    return 1;
}

// Startuje patrol
int
start_patrol(mixed patrol)
{
    mixed tmp = Patrole[patrol];

    if (!tmp)
        return 0;

    if (tmp[P_ALARM])
        return 0;

    /*To jest dla patroli, kt�re NIE chodz�. Np. te, kt�re
      stoj� ci�gle przy bramach. Dzi�ki temu b�d� si� wraca�
      do swojej lokacji, po zako�czeniu akcji.*/
    if(!sizeof(tmp[P_TRASA]))
    {
        mixed droga=znajdz_sciezke(ENV(tmp[P_SKLAD][0]),
            find_object(query_miejsce_startu(patrol)));

        int i;

        for(i=0;i<sizeof(droga);i++)
            tmp[P_SKLAD][0]->command(droga[i]);

        return 1;
    }

    tmp[P_ALARM] = set_alarm(itof(PREDKOSC_CHODZENIA),0.0, &chodzenie(patrol));

    return 1;
}

void
usun_wroga_i_kontynuuj_patrol(mixed patrol, object wrog)
{
    mixed tmp = Patrole[patrol];
    wrog->remove_prop(SCIGA_PATROL);
    tmp[P_SKLAD][0]->unset_follow();
    remove_enemy(wrog->query_real_name());
    start_patrol(patrol);
    set_alarm(2.0,0.0,"zeruj_akcje");
}

int
czy_widzimy_playera(mixed patrol, object kto)
{
    mixed tmp = Patrole[patrol];

    if(member_array(kto,FILTER_CAN_SEE(FILTER_LIVE(all_inventory(ENV(tmp[P_SKLAD][0]))),
        tmp[P_SKLAD][0])) != -1)
    {
        return 1;
    }
    else
        return 0;
}
int
chodzenie(mixed patrol)
{
    mixed tmp = Patrole[patrol];

    if (!tmp)
        return 0;

    if(tmp[P_SKLAD][0]->query_prop(MA_SIGNAL_FIGHT)) //Nie chodzimy, jak toczy sie walka
    {
        set_alarm(itof(PREDKOSC_CHODZENIA),0.0,"chodzenie",patrol);
        return 1;
    }

    foreach (object x : tmp[P_SKLAD]) //Nie chodzimy, jesli ktos z patrolu walczy
    {
        if(x->query_attack())
        {
            set_alarm(itof(PREDKOSC_CHODZENIA),0.0,"chodzenie",patrol);
            return 1;
        }
    }

    // Je�li przypadkiem jeste�my tam, gdzie mieli�my wr�ci�
    // to odnotujmy to, ze ju nie trzeba sie nigdzie ruszac
    if (environment(tmp[P_SKLAD][0]) == tmp[P_LOKACJA_POWROTU])
    {
        tmp[P_LOKACJA_POWROTU] = 0;
        tmp[P_TRASA_POWROTU] = 0;
        tmp[P_TRASA_POWROTU_POZ] = 0;
    }

    // Jesli patrol nie jest na trasie (np. jest wlasnie po poscigu),
    //    to musi tam wrocic
    if (tmp[P_LOKACJA_POWROTU])
    {
        // Ustalamy trase powrotu, jesli jeszcze jej nie ma
        if (!tmp[P_TRASA_POWROTU])
        {
            tmp[P_TRASA_POWROTU] = znajdz_sciezke(environment(tmp[P_SKLAD][0]),
                                    tmp[P_LOKACJA_POWROTU]);
            tmp[P_TRASA_POWROTU_POZ] = 0;
        }

        // Ruszamy sie
        tmp[P_SKLAD][0]->command(tmp[P_TRASA_POWROTU][tmp[P_TRASA_POWROTU_POZ]++]);

        /*tmp[P_ALARM] = set_alarm(itof(Predkosc_min) +
                        itof(random(5 * (Predkosc_max - Predkosc_min))) / 20.0,
                    0.0, &chodzenie(patrol));*/
        tmp[P_ALARM] = set_alarm(itof(PREDKOSC_CHODZENIA),0.0, &chodzenie(patrol));

        return 1;
    }

    // Ruszamy sie, je�li mamy tras�:P
    if(sizeof(tmp[P_TRASA]))
    {
        tmp[P_SKLAD][0]->command(tmp[P_TRASA][tmp[P_TRASA_POZ]]);
        tmp[P_TRASA_POZ] = (tmp[P_TRASA_POZ] + 1) % sizeof(tmp[P_TRASA]);

        tmp[P_LOKACJA_POWROTU] = environment(tmp[P_SKLAD][0]);

        /*tmp[P_ALARM] = set_alarm(itof(Predkosc_min) +
            itof(random(5 * (Predkosc_max - Predkosc_min))) / PREDKOSC_CHODZENIA,
            0.0, &chodzenie(patrol));*/
        tmp[P_ALARM] = set_alarm(itof(PREDKOSC_CHODZENIA),0.0, &chodzenie(patrol));
    }

    return 1;
}


/**
 *Pomocnicza prosta funkcyjka do gonienia gracza,
 *np. je�li nam spieprzy�, czy co�...
 *wywa�a te� drzwi! ;)
 */
void
idz_do_gracza(mixed patrol,object do_kogo)
{
    //np. w razie ew. niepowodzenia akcji znajdz_sciezke zeby nie stali jak ko�ki w miejscu
    //gdzie akcja zako�czy�a si� niepowodzeniem ;)
    set_alarm(20.0,0.0,"sprawdz_czy_jeszcze_walka",patrol,do_kogo);

    mixed tmp = Patrole[patrol];
    object straznik=tmp[P_SKLAD][0];
    mixed droga=znajdz_sciezke(ENV(straznik),ENV(do_kogo));
    int i;

    for(i=0;i<sizeof(droga);i++)
    {
        object env = ENV(straznik);
        straznik->command(droga[i]);
        if (ENV(straznik) == env) //nie uda�o si� przej��
        {
            //no to wywa�amy!

            object door = filter(filter(all_inventory(ENV(straznik)),
                &->is_door()), &operator(==)(droga[i]) @
                &operator([])(,0) @ &->query_pass_command())[0];
            //door2 to drzwi po drugiej stronie
            object door2 = door->query_other_door();

            door->do_unlock_door("");
            door2->do_unlock_door("");
            door->open_door(door->short(0));
            door2->open_door(door2->short(0));

            //nadal zamkni�te? To pewnie mamy tu do czynienia ze specjaln� komend� otwierania
            //np. 'odsu� kotar�'
            if(!door->query_open())
            {
                string komenda = pointerp(door->query_open_command()) ? door->query_open_command()[0] :
                    door->query_open_command();
                straznik->command(komenda+" "+door->short(PL_BIE));
            }
            else
            {
                tell_room(ENV(door2),"Nagle z wielkim hukiem "+door2->short(PL_BIE)+" otwieraj� si� "+
                "wywa�one przez "+QIMIE(straznik,PL_DOP)+"!\n");
            }

            straznik->command(droga[i]);
        }
    }
}


void
notify_enter(mixed patrol, object kto)
{ //WYSTARCZY, ZE init_disarm() JEST URUCHAMIANE PRZY WEJSCIU.
}

void
notify_leave(mixed patrol, object kto)
{
    //DBG("notify_leave");
    mixed tmp = Patrole[patrol];

    if (Wrogowie[kto->query_real_name()] &&
        kto->query_prop(SCIGA_PATROL) == tmp[P_SKLAD][0]->query_id_patrolu())
    {

        tmp[P_SKLAD][0]->hook_uciekl_podczas_walki(kto);

        tmp[P_SKLAD][0]->set_follow(kto, 0.1,0);
    }
}

void
notify_attack(mixed patrol, object kto, object kogo)
{
    /*find_player("vera")->catch_msg("notify_attack kto: "+file_name(kto)+" kogo: "+file_name(kogo)+".\n");*/

    mixed tmp = Patrole[patrol];
    if(kto->query_oficer() && tmp[P_SKLAD][0]->query_id_patrolu() == tmp[P_SKLAD][1]->query_id_patrolu())
    {
        int i;
        for(i=0;i<sizeof(tmp[P_SKLAD]);i++)
            tmp[P_SKLAD][i]->wesprzyj(kto);
    }

}


/*void
notify_opusc(mixed patrol, object kto)
{
  if(!sizeof(kogo->subinventory("wielded")))
    {
      string tmp = Patrole[patrol];
      tmp[P_SKLAD][0]->command("kiwnij");
      tmp[P_SKLAD][0]->command("powiedz do"+OB_NAME(kto)+" Zeby mi to sie"
			       +" wiecej nie powtorzylo.");
      start_patrol(patrol);
    }
}*/

void wytrac(mixed patrol, object *bronie, int z,object kogo)
{
    mixed tmp = Patrole[patrol];

    if(!czy_widzimy_playera(patrol,kogo))
        return;

    if(ENV(tmp[P_SKLAD][0]) != ENV(kogo))
        idz_do_gracza(patrol,kogo);

    set_this_player(kogo);

    object *straznicy = tmp[P_SKLAD][0]->query_team();
    int x = random(sizeof(straznicy));
    write(capitalize(straznicy[x]->short()) +
        " zachodzi ci^e od ty^lu i mocnym uderzeniem w plecy powala" +
        " ci^e na kolana.\n");
    say(capitalize(straznicy[x]->short()) + " zachodzi " +
        QIMIE(TP, PL_BIE) + " od ty^lu" +
        " i pot^e^znym uderzeniem w plecy powala " +
        TP->koncowka("go", "j^a") + " na kolana.\n");

    TP->reduce_hp(0, 1000 + random(1000), 10);
    TP->set_old_fatigue(TP->query_old_fatigue()-random(15));

    switch(z) //z = sizeof(dobyte bronie), 1 lub 2
    {
        case 1: tmp[P_SKLAD][0]->command("powiedz do "+TP->short(TO, PL_DOP) +
            " Chyba nie b^edzie ci to potrzebne.");
            //set_alarm(0.5,0.0, "command_present", TP,
            write(capitalize(straznicy[x]->short())+" wytr^aca ci bro^n z r^ak.\n");
            //set_alarm(0.5,0.0, "command_present", TP,
            saybb(capitalize(straznicy[x]->short())+" wytr^aca bro^n "+
                QIMIE(TP, PL_CEL)+".\n");
            bronie[0]->unwield_me();
            bronie[0]->move(tmp[P_SKLAD][0]->query_magazyn());
            if(!TP->query_attack())
                tmp[P_SKLAD][0]->hook_bron_do_odebrania();
            break;
        case 2:
            if(!TP->query_attack())
                tmp[P_SKLAD][0]->command("powiedz do "+TP->query_name(PL_DOP) +
                    " Chyba nie b^edzie ci to potrzebne.");
                //set_alarm(0.5,0.0, "command_present", TP,
                write(capitalize(straznicy[x]->short())+" wytr^aca ci bronie z r^ak.\n");
                //set_alarm(0.5,0.0, "command_present", TP,
                saybb(capitalize(straznicy[x]->short())+" wytr^aca bronie "+
                QIMIE(TP, PL_CEL)+".\n");

                bronie[0]->unwield_me();
                bronie[0]->move(tmp[P_SKLAD][0]->query_magazyn());
                bronie[1]->unwield_me();
                bronie[1]->move(tmp[P_SKLAD][0]->query_magazyn());
                if(!TP->query_attack())
                    tmp[P_SKLAD][0]->hook_bron_do_odebrania();

                break;
         default: //set_alarm(0.5,0.0, "command_present", TP,
                write(capitalize(straznicy[x]->short())+" wytr^aca ci bronie z r^ak.\n");
                //set_alarm(0.5,0.0, "command_present", TP,
                saybb(capitalize(straznicy[x]->short())+" wytr^aca bronie "+
                QIMIE(TP, PL_CEL)+".\n");
                bronie[0]->unwield_me();
                break;
    }

    remove_enemy(TP->query_real_name());

    start_patrol(patrol);
}

int
testujemy_sile_wroga(mixed patrol, object atakujacy)
{
    mixed tmp = Patrole[patrol];
    int sila_druzyny;

    foreach(object x:tmp[P_SKLAD])
        sila_druzyny=sila_druzyny + x->query_stat(0);

    if((atakujacy->query_stat(0) <= sila_druzyny))
        return 1;
    else
        return 0;
}

void
zeruj_grabiony(object komu)
{
    komu->remove_prop(GRABIONY);
}

void
zaprzestan_walki(mixed patrol, object kto=0)
{
    mixed tmp = Patrole[patrol];

    foreach(object x : tmp[P_SKLAD])
    {
        foreach(object w : x->query_enemy(-1))
        {
            x->stop_fight(w);
            w->stop_fight(x);
        }
        x->command("przestan atakowac wszystkich");
        if(kto)
            kto->stop_fight(x);
    }
}

void
grabiez(mixed patrol,object wrog)
{//DBG("0");
    mixed tmp = Patrole[patrol];

    if(!czy_widzimy_playera(patrol,wrog))
        return;

    if(wrog->query_prop(GRABIONY))
        return;

    wrog->add_prop(GRABIONY,1);
    set_alarm(20.0,0.0,"zeruj_grabiony",wrog);

    set_this_player(wrog);

    // DBG("1");
    int lvl=query_enemy_lvl(wrog->query_real_name());
    //object *inv=wrog->subinventory();
    object *inv=deep_inventory(wrog);
    int z;
    object *kradzione=({ }), *zabieramy=({ });
    // DBG("1.1");

    for(z=0;z<sizeof(inv);z++)
    {
        if((inv[z]->query_prop(OBJ_M_NO_DROP) ==0)&&
            (inv[z]->query_prop(OBJ_I_NO_DROP) ==0)&&
            inv[z]->query_no_show() == 0)
        {
            if(sizeof(inv[z]->query_orig_place()))
            {
                if(inv[z]->query_orig_place()[0] != file_name(ENV(tmp[P_SKLAD][0])))
                    zabieramy+=({inv[z]});
            }															//;)
            else if(inv[z]->query_value() >= 100 && inv[z]->query_name()!="majtki")
                zabieramy+=({inv[z]});
        }
    }
    //DBG("1.3");
    if(sizeof(zabieramy))
    {
        //Sprawd�my, czy jest co� kradzionego.
        foreach(object s : zabieramy)
        {
            if(sizeof(s->query_owners()) &&
                member_array(wrog->query_real_name(), s->query_owners()) == -1)
            {
                kradzione+=({s});
                remove_steal(file_name(s));
                tmp[P_SKLAD][0]->przenies_do_biura(s);
            }
        }
        tmp[P_SKLAD][0]->command("powiedz Ale to sobie zachowam.");
        switch(lvl)
        {
            case 0..1:
                //if jest co� do zabrania i nie ma kradzionych
                if(sizeof(zabieramy) && !sizeof(kradzione))
                {
                    object konf=zabieramy[0];

                    for(z=0;z<sizeof(zabieramy);z++)
                        if(zabieramy[z]->query_value() >= konf->query_value())
                            konf=zabieramy[z];

                    //DBG("8");
                    /*unique_array(kompozit, OB_NAME);
                    map(ret, &operator([])(,0));*/

                    if(konf)
                    {
                        wrog->catch_msg(capitalize(tmp[P_SKLAD][0]->query_imie(TP,PL_MIA)) +
                            " przeszukuje ci^e i zabiera ci "+konf->short(PL_BIE)+".\n");
                        tell_roombb(ENV(wrog), QCIMIE(tmp[P_SKLAD][0],PL_MIA)+
                            " przeszukuje "+QIMIE(wrog,PL_BIE)+
                            " i zabiera "+wrog->koncowka("mu","jej","jemu")+
                            " "+konf->short(PL_BIE)+".\n", ({wrog}));
                        if(konf->query_type() != O_MONETY)
                            konf->move(tmp[P_SKLAD][0]->query_magazyn());
                        else
                            konf->destruct();

                    }
                 }
                 else if(sizeof(kradzione))
                 {
                    wrog->catch_msg(QCIMIE(tmp[P_SKLAD][0],PL_MIA) +
                        " przeszukuje ci^e i zabiera ci " + COMPOSITE_DEAD(wrog, PL_MIA) + ".\n");
                    saybb(QCIMIE(tmp[P_SKLAD][0],PL_MIA) + " przeszukuje " + QIMIE(wrog,PL_BIE) +
                        " i zabiera " + wrog->koncowka("mu","jej","jemu") + " " +
                        QCOMPDEAD(PL_MIA) + ".\n", ({wrog}));

                }

                break;
            case 2..3:
                if(sizeof(zabieramy))
                {
                    foreach(object z : zabieramy)
                    {
                        if(z->query_type() != O_MONETY)
                            z->move(tmp[P_SKLAD][0]->query_magazyn());
                        else
                            z->destruct();
                    }

                    wrog->catch_msg(QCIMIE(tmp[P_SKLAD][0],PL_MIA)+
                        " przeszukuje ci^e i zabiera ci "+
                        "wszystko co posiadasz.\n");
                    say(QCIMIE(tmp[P_SKLAD][0],PL_MIA)+
                        " przeszukuje "+QIMIE(wrog,PL_BIE)+
                        " i zabiera mu wszystko co posiada.\n", ({wrog}));

                }
                break;
        }
    }
    else //Skoro nic nie ma, to chociaz skopiemy chama :(
    {
    switch(random(4))
    {
        case 0:
            tmp[P_SKLAD][0]->command("powiedz do "+wrog->query_name(PL_DOP) +
                " Tfu! Psi pomiocie!");
            tmp[P_SKLAD][0]->command("kopnij "+wrog->query_real_name(PL_BIE));

            break;
        case 1:
            tmp[P_SKLAD][0]->command("powiedz do "+wrog->query_name(PL_DOP) +
                " Ehh, nawet nie ma czego ci zajuma^c.");
            tmp[P_SKLAD][0]->command("kopnij "+wrog->query_real_name(PL_BIE));

            break;
        case 2:
            tmp[P_SKLAD][0]->command("kopnij "+wrog->query_real_name(PL_BIE));

            break;
        case 3:
            tmp[P_SKLAD][0]->command("powiedz do "+wrog->query_name(PL_DOP) +
                " Ehh, nawet nie ma czego ci zabra�.");
            tmp[P_SKLAD][0]->command("opluj "+wrog->query_real_name(PL_BIE));

            break;
        }
    }
    //DBG("9");
    //skoro sie juz porachowalimy, to mozemy usunac z listy
    zaprzestan_walki(patrol,wrog);
    usun_wroga_i_kontynuuj_patrol(patrol, wrog);

    tmp[P_SKLAD][0]->unset_follow();
    start_patrol(patrol);
    //DBG("10");
}

void
zeruj_akcje()
{
    akcja=0;
}

void
daj_malutki_paraliz(object komu)
{
    object paraliz=clone_object("/std/paralyze.c");
    paraliz->set_remove_time(2);
    paraliz->move(komu);
}

void
rzucamy_wroga_na_ziemie(mixed patrol, object wrog)
{
    mixed tmp = Patrole[patrol];

    set_this_player(wrog);

    if(!(wrog->query_prop(SIT_LEZACY)))
    {
        if(tmp[P_SKLAD][1]) //a mo�e zosta� ju� tylko oficer? :(
        {
            write(capitalize(tmp[P_SKLAD][1]->short())+" rzuca ci^e na ziemi^e.\n");
            saybb(QCIMIE(tmp[P_SKLAD][1],PL_MIA)+" rzuca "+QIMIE(wrog,PL_BIE)+
                " na ziemi^e.\n");
        }
        else
        {
            write(capitalize(tmp[P_SKLAD][0]->short())+" rzuca ci^e na ziemi^e.\n");
            saybb(QCSHORT(tmp[P_SKLAD][0],PL_MIA)+" rzuca "+QIMIE(wrog,PL_BIE)+
            " na ziemi^e.\n");
        }

        wrog->reduce_hp(0, 1000 + random(1000), 10);

        //A tak jest jeszcze lepiej! Bo sprawdza tablice query_sit()
        //w trybie 'deep', co czasem jest konieczne
        for(int i=0;i<sizeof(ENV(wrog)->query_sit());i++)
        {
            if(pointerp(ENV(wrog)->query_sit()[i]))
            {
                if(member_array("na ziemi", ENV(wrog)->query_sit()[i]) != -1)
                {
                    TP->add_prop(OBJ_I_DONT_GLANCE, 1);
                    TP->remove_prop(SIT_SIEDZACY);
                    TP->add_prop(SIT_LEZACY, ({ "na ziemi" }) );
                    daj_malutki_paraliz(TP);
                    return;
                }
                else if(member_array("na bruku",ENV(wrog)->query_sit()[i]) != -1)
                {
                    TP->add_prop(OBJ_I_DONT_GLANCE, 1);
                    TP->remove_prop(SIT_SIEDZACY);
                    TP->add_prop(SIT_LEZACY, ({ "na bruku" }) );
                    daj_malutki_paraliz(TP);
                    return;
                }
                else if(member_array("na ulicy",ENV(wrog)->query_sit()[i]) != -1)
                {
                    TP->add_prop(OBJ_I_DONT_GLANCE, 1);
                    TP->remove_prop(SIT_SIEDZACY);
                    TP->add_prop(SIT_LEZACY, ({ "na ulicy" }) );
                    daj_malutki_paraliz(TP);
                    return;
                }
            }
            else if(ENV(wrog)->query_sit()[i] == "na ziemi")
            {
                TP->add_prop(OBJ_I_DONT_GLANCE, 1);
                TP->remove_prop(SIT_SIEDZACY);
                TP->add_prop(SIT_LEZACY, ({ "na ziemi" }) );
                daj_malutki_paraliz(TP);
                return;
            }
            else if(ENV(wrog)->query_sit()[i] == "na bruku")
            {
                TP->add_prop(OBJ_I_DONT_GLANCE, 1);
                TP->remove_prop(SIT_SIEDZACY);
                TP->add_prop(SIT_LEZACY, ({ "na bruku" }) );
                daj_malutki_paraliz(TP);
                return;
            }
            else if(ENV(wrog)->query_sit()[i] == "na ulicy")
            {
                TP->add_prop(OBJ_I_DONT_GLANCE, 1);
                TP->remove_prop(SIT_SIEDZACY);
                TP->add_prop(SIT_LEZACY, ({ "na ulicy" }) );
                daj_malutki_paraliz(TP);
                return;
            }
        }

        //A je�li nic, to dajemy ten nieszcz�sny parali�
        object paraliz=clone_object("/std/paralize/lezenie.c");
        paraliz->move(wrog);
        wrog->add_prop(PRZEWROCONY,1);
    }

    zaprzestan_walki(patrol,wrog);
}

void
sprawdz_kondycje_wroga(mixed patrol, object wrog)
{
    mixed tmp = Patrole[patrol];

    //Je�li ma mniej hp ni� po�ow�, to przestajemy walczy�.
    if(wrog->query_hp() <= (wrog->query_max_hp() / 2.0))
    {
        akcja=1;
        int i;

        tmp[P_SKLAD][0]->command("powiedz Oddziaaaa^l! Sta^c!!");

        foreach (object x : tmp[P_SKLAD])
            x->command("przestan atakowac wszystkich");
        foreach (object x : tmp[P_SKLAD])
            x->command("przestan atakowac wszystkich");

        tmp[P_SKLAD][0]->command("powiedz Tym razem darujemy ci ^zycie.");
        tmp[P_SKLAD][0]->unset_follow();

        zaprzestan_walki(patrol,wrog);

        if(!wrog->query_prop(PRZEWROCONY) && !wrog->query_prop(SIT_LEZACY))
            rzucamy_wroga_na_ziemie(patrol,wrog);

        grabiez(patrol,wrog);

        usun_wroga_i_kontynuuj_patrol(patrol, wrog);
    }
    else
        set_alarm(0.5,0.0,"sprawdz_kondycje_wroga",patrol,wrog);

    //Bardzo ma�o wydajne rozwi�zanie, mo�e lepszy by�by shadow zak�adany na gracza kt�ry informowa�by
    //stra�nik�w kiedy poziom expa gracza osi�gnie 50%???(Krun)
}

void
sprawdz_czy_jeszcze_walka(mixed patrol, object wrog)
{
    mixed tmp = Patrole[patrol];

    if(tmp[P_SKLAD][0]->query_attack())
        set_alarm(10.0,0.0,"sprawdz_czy_jeszcze_walka",patrol,wrog);
    else
    {
        tmp[P_SKLAD][0]->unset_follow();
        start_patrol(patrol);
        wrog->remove_prop(SCIGA_PATROL);
    }
}

void
rozpetaj_walke(mixed patrol, object wrog)
{
    string str;
    mixed tmp = Patrole[patrol];

    set_alarm(25.0,0.0,"sprawdz_czy_jeszcze_walka",patrol,wrog);

    if(!czy_widzimy_playera(patrol,wrog))
        return;

    if(wrog->query_prop(SCIGA_PATROL))
        return;

    //just in case...
    if(wrog->query_npc())// || wrog->query_oficer() || wrog->query_straznik())
    {
        usun_wroga_i_kontynuuj_patrol(patrol, wrog);
        return;
    }

    wrog->add_prop(SCIGA_PATROL,tmp[P_SKLAD][0]->query_id_patrolu());

    /*tmp[P_SKLAD][0]->command("powiedz To on"+wrog->koncowka("","a","o")+"! Bra^c "+
                       wrog->koncowka("go","j^a","to")+"!");*/
    tmp[P_SKLAD][0]->command("krzyknij Oddzia�! Bra^c "+
        wrog->koncowka("go","j^a","to")+"!") ;
    //Darujemy takim...
    if(((wrog->query_hp() < 230.0 && wrog->query_max_hp() > 600.0) ||
        (wrog->query_hp() < 180.0 && wrog->query_max_hp() <= 600.0 ))
        && query_enemy_lvl(wrog->query_real_name()) < 3)
    {
        stop_patrol(patrol);
        grabiez(patrol,wrog);
        return;
    }



    tmp[P_SKLAD][0]->command("zabij "+wrog->query_real_name(PL_BIE));
    //dump_array(tmp[P_SKLAD]);
    foreach(object x : tmp[P_SKLAD])
    {
        //DBG("?\n");
        if(!x->query_attack() && x->query_id_patrolu() == wrog->query_prop(SCIGA_PATROL))
        {
            //DBG("ja: "+"wesprzyj "+tmp[P_SKLAD][0]->short(3)+"\n");
            x->command("wesprzyj "+tmp[P_SKLAD][0]->short(3));
            x->command("zabij "+(tmp[P_SKLAD][0]->query_attack())->short(3));
        }
    }

    str=tmp[P_SKLAD][0]->hook_okrzyk_do_walki(wrog);

    if(random(2))
        tmp[P_SKLAD][0]->command(str);

    notify_attack(patrol,tmp[P_SKLAD][0],wrog);

    /*Ta funkcja sprawdza pierdo�y i wywo�uje wytr�canie i konfiskowanie
    broni...Coby si� naszym stra�nikom �atwiej walczy�o, no nie? ;) */
    sprawdzmy_czy_opuscil(patrol, wrog);

    //Inni nie zas�uguj� na �ask� ;)
    if(query_enemy_lvl(wrog->query_real_name())<3)// && !akcja)
    {
        //DBG("sprawdz_kondycje");

        set_alarm(0.5,0.0,"sprawdz_kondycje_wroga",patrol,wrog);
    }

    stop_patrol(patrol);

}

void
reakcja_na_walke(mixed patrol,object wrog)
{
    mixed tmp = Patrole[patrol];
    //wrog->add_prop(SCIGA_PATROL, tmp[P_SKLAD][0]->query_id_patrolu());
    object wrog_wroga=wrog->query_attack();

    if(wrog->query_prop(SCIGA_PATROL))
        return;

    if(query_enemy_lvl(wrog->query_real_name()) == 3)
    {
        rozpetaj_walke(patrol, wrog);
        return;
    }
    else if(query_enemy_lvl(wrog_wroga->query_real_name()) == 3)
    {
        rozpetaj_walke(patrol, wrog_wroga);
        return;
    }

    if(interactive(wrog) && interactive(wrog_wroga)) //Player vs player.
    {
        //find_player("vera")->catch_msg("PVP, czynimy pokoj.\n");
        tmp[P_SKLAD][0]->command("powiedz Dobra juz dobra, dosy^c tego! Spok^oj zak^l^ocacie!");
        wrog_wroga->stop_fight(wrog);
        wrog->stop_fight(wrog_wroga);
        grabiez(patrol,wrog); //A co :) Nie ma lekko :P
        return;
    }

    switch(testujemy_sile_wroga(patrol,wrog))
    {
        case 1: //Obezwladniamy leszcza
            //find_player("vera")->catch_msg("STOP FIGHT.\n");

            wrog_wroga->stop_fight(wrog);
            wrog->stop_fight(wrog_wroga);

            zaprzestan_walki(patrol,wrog);
            zaprzestan_walki(patrol,wrog_wroga);

            rzucamy_wroga_na_ziemie(patrol,wrog);

            //tmp[P_SKLAD][0]->unset_follow();

            grabiez(patrol,wrog); //A co :)

            break;

        case 0:
            rozpetaj_walke(patrol, wrog);
            this_object()->log("za walke", wrog);
            break;
    }

}

void
sprawdz_czy_walka(mixed patrol, object kogo)
{
    if(kogo->query_attack())
        reakcja_na_walke(patrol,kogo);
}

/*
 * usuwamy gracza z listy ostrzezonych o posiadanie broni
 * 1arg - real_name gracza
 */
void
usun_z_ostrzezonych(string kogo)
{
    m_delkey(Ostrzezeni,kogo);
}

void
sprawdzmy_czy_opuscil(mixed patrol, object kogo)
{
    //DBG("sprawdzmy_czy_opuscil");
    mixed tmp = Patrole[patrol];

    object *akt_bronie=({});

    if(sizeof(kogo->subinventory("wielded")))
    {
        object wep1=kogo->subinventory("wielded")[0];
        if(wep1->is_weapon() && !wep1->query_prop(WEAPON_I_LEGAL_WIELD))
            akt_bronie=({wep1});
        //DBG("wep sizeof 1");
    }
    if(sizeof(kogo->subinventory("wielded")) == 2)
    {
        object wep2=kogo->subinventory("wielded")[1];
        if(wep2->is_weapon() && !wep2->query_prop(WEAPON_I_LEGAL_WIELD))
            akt_bronie=akt_bronie+({wep2});
        //DBG("wep sizeof 2");
    }

    //Wpierw sprawdzmy, czy nie spierdolil:)
    if(environment(tmp[P_SKLAD][0]) != environment(kogo))
    {
        //DBG("environment(tmp[P_SKLAD][0]) != environment(kogo))\n");
        //A jednak spierdoli! Dra�
        //Ale mo�e zdy ju� opu�ci� bro�..W takim razie nie gonimy, ale jesli nie... ;)
        if(sizeof(akt_bronie))
        {
            idz_do_gracza(patrol,kogo);
            wytrac(patrol,akt_bronie,sizeof(akt_bronie),kogo);
        }
    }
    else if(sizeof(akt_bronie))
    {
        if(!kogo->query_attack())
            tmp[P_SKLAD][0]->hook_ostrzegalem();
        set_alarm(1.0, 0.0, "wytrac",patrol, akt_bronie,sizeof(akt_bronie),kogo);
    }
    else if(!kogo->query_attack())
    {
        tmp[P_SKLAD][0]->hook_posluszny_gracz(kogo);
        usun_z_ostrzezonych(kogo->query_real_name());
    }

    if(!kogo->query_attack())
    {
        set_alarm(30.0,0.0,"usun_z_ostrzezonych",kogo->query_real_name());
        start_patrol(patrol);
    }
}

void
ostrzezenie(mixed patrol, object kogo)
{
    mixed tmp = Patrole[patrol];
    Ostrzezeni[kogo->query_real_name()]=patrol;
    string str=tmp[P_SKLAD][0]->hook_opusc_bron(kogo);
    if(ENV(kogo) == ENV(tmp[P_SKLAD][0]))
        tmp[P_SKLAD][0]->command("powiedz do "+kogo->query_name(PL_DOP) + " "+str);
    set_alarm(6.0+itof(random(4)),0.0,"sprawdzmy_czy_opuscil",patrol,kogo);
}

/**
 * Funkcja lokalna tylko do wyfiltrowania broni dobywanych przez gracza.
 */
public int
wyfiltruj_bronie(object ob)
{
    if(ob->is_weapon() && !ob->query_prop(WEAPON_I_LEGAL_WIELD))
        return 1;
}

/**
 * Wywo�ywane przy ka�dym wej�ciu patrolu na now� lokacj�, dla ka�dego
 * z player�w na tej lokacji
 */
void
init_disarm(mixed patrol, object kogo)
{
    int czy_wrog = query_enemy_lvl(kogo->query_real_name());

    set_alarm(35.0,0.0,"sprawdz_czy_jeszcze_walka",patrol,kogo);

    //DBG("kogo: "+kogo->query_real_name()+"\n");
    //DBG("init_disarm ("+kogo->short()+"), ostrzezeni:\n");dump_array(ostrzezeni);
    mixed tmp = Patrole[patrol];

    if(!czy_widzimy_playera(patrol,kogo))
        return;

    //nie mieszamy w to innych patroli:
    if(kogo->query_prop(SCIGA_PATROL))
        if(kogo->query_prop(SCIGA_PATROL) != tmp[P_SKLAD][0]->query_id_patrolu())
            return;

    //kr�tka pi�ka, jak naszych npc�w nawalaj�:
    object wrog_kogo = kogo->query_attack();
    if(wrog_kogo->query_npc() && !(wrog_kogo->query_oficer()) &&
        !(wrog_kogo->query_straznik()) && wrog_kogo->is_humanoid() &&
        tmp[P_SKLAD][0]->query_spolecznosc() ~= wrog_kogo->query_spolecznosc())
    {
        //add_enemy(kogo->query_real_name(),3);
        //je�li npc jest zb�jem, czy typem spod ciemnej gwiazdy
        //to nie powinien nalezec do tej spolecznosci. Proste.
        //bo stra� wspiera ka�dego npca z danej spolecznosci
        rozpetaj_walke(patrol,kogo);
        this_object()->log("za lanie npca", kogo,wrog_kogo);
        return;
    }

    //je�li natrafili�my na kogo� poszukiwanego
    if(czy_wrog > 0 &&
        (!tmp[P_SKLAD][0]->query_attack() ||
        (tmp[P_SKLAD][0]->query_attack())==kogo))
    {
        switch(czy_wrog)
        {
            case 1..2:
                if(kogo->query_hp() <= kogo->query_max_hp() / 3.0) //w kiepskiej kondycji jest
                    set_alarm(0.1,0.0,"grabiez",patrol,kogo);
                else
                {
                    set_alarm(0.1,0.0, "rozpetaj_walke",patrol,kogo);
                    this_object()->log("scigany (lvl 1 lub 2)", kogo);
                }

                break;
        case 3:
            set_alarm(0.1,0.0, "rozpetaj_walke",patrol,kogo);

            this_object()->log("scigany (lvl 3!)", kogo);

            break;
        }

        return;
    }

    //DBG("dalej");
    //if(tmp[P_SKLAD][0]->query_attack() != 0 || sizeof(kogo->subinventory("wielded")) == 0)
    //  return;

    object *bronie = ({ });

    //Kurde Veru�... Jak �adnie zacytowa�e�... "Nie mn� byt�w ponad potrzebe."
    //tak to mo�na by�o zrobi�...
    //bronie = kogo->subinventory("wielded"); <- tak? to dlaczego to g�wno nie dzia�a? :P zamiast tego przerobi�em 
    //na makro DOBYTE_BRONIE(ob) kt�r� zamie�ci�em jako uniwersaln� (bo cz�sto to jest potrzebne) macros.h
    //bronie = filter(bronie, &wyfiltruj_bronie());
    bronie = DOBYTE_BRONIE(kogo);
    //if(ob->is_weapon() && !ob->query_prop(WEAPON_I_LEGAL_WIELD))
    //    return 1;
        
        
//vera mowi: faktycznie, zlami�em :P

    //A tak to zrobi�e�!
#if 0
    if(sizeof(kogo->subinventory("wielded")))
    {
        object wep1=kogo->subinventory("wielded")[0];

        if(wep1->is_weapon())
        {
            bronie=({wep1});
//DBG("sizeof wielded 1");
        }
    }
    if(sizeof(kogo->subinventory("wielded")) == 2)
    {
        object wep2=kogo->subinventory("wielded")[1];

        if(wep2->is_weapon())
        {
            bronie=bronie+({wep2});
//DBG("sizeof wielded 2");
        }
    }
#endif
    if(sizeof(bronie) && !kogo->query_attack())
    {
         //Czyli: jesli nie zostal wczesniej ostrzezony
        if(member_array(kogo->query_real_name(),m_indices(Ostrzezeni)) == -1)
        {
            idz_do_gracza(patrol,kogo);
            set_alarm(0.5,0.0,"ostrzezenie",patrol,kogo);
            stop_patrol(patrol);
            return;
        }
        //a je�li zosta� ostrze�ony i to przez nasz patrol, to wytr�camy bro�:
        else if(Ostrzezeni[kogo->query_real_name()] == patrol)
        {
            if(sizeof(bronie)==1)
                if(bronie[0]->is_weapon())
                    set_alarm(1.0, 0.0, "wytrac",patrol, bronie,0,kogo);
            else if(sizeof(bronie)==2)
            {
                if(bronie[1]->is_weapon() && !bronie[0]->is_weapon())
                    set_alarm(1.0, 0.0, "wytrac",patrol, bronie,1,kogo);
                else if(bronie[1]->is_weapon() && bronie[0]->is_weapon())
                    set_alarm(1.0, 0.0, "wytrac",patrol, bronie,2,kogo);
            }
        }
    }
    else
        set_alarm(1.0,0.0,"sprawdz_kradzione",patrol,kogo);

    /* Hmm, to ju� chyba nie jest potrzebne :)
    //jak si� gracze nawalaj� mi�dzy sob�...
     if(kogo->query_attack() && interactive(kogo) && interactive(wrog_kogo))
     {
         set_alarm(0.3, 0.0, "reakcja_na_walke",patrol,kogo);
     }
     else
         set_alarm(2.0,0.0,"sprawdz_czy_walka",patrol,kogo);
*/
}

/*int
disarm_cmds(object kogo, string cmd, mixed patrol)
{
  if(!kogo->query_prop("STRAZ_I_DOBYTA"))
    return 0;

  string *exits;
  exits = environment(kogo)->query_exit_cmds();

  if(member_array(cmd, exits) == -1) return 0;

  if(!kogo->query_prop("STRAZ_I_WARNED"))
    {
      kogo->catch_msg("Ignorowanie strazy chyba nie jest najlepszym"
		      +" pomys^lem. Przemy^sl to jeszcze raz.\n");
      kogo->add_prop("STRAZ_I_WARNED", 1);
      return 1;
    }
  else
    {
      add_enemy(kogo->query_real_name(), 1);
      kogo->remove_prop("STRAZ_I_DOBYTA");
      kogo->remove_prop("STRAZ_I_WARNED");
      return 0;
    }
}*/


int
sprawdz_czy_opis_pasuje(object przedmiot,object kto)
{
    string *admin_przyms = query_przymiotniki_zlodzieja(file_name(przedmiot));
//DBG("admin_przyms: "+COMPOSITE_WORDS(admin_przyms));
    string *akt_przyms=({ });
    int prior,prz;
    for(prior=0;prior<=10;prior++)
    {
        int ile_przym=sizeof(kto->query_lista_przymiotnikow2(prior));
        if(ile_przym)
        {
            foreach(string *przyms:kto->query_lista_przymiotnikow2(prior))
                akt_przyms+=({przyms[0]});
        }
    }
    //DBG("akt_przyms: "+COMPOSITE_WORDS(akt_przyms));
    int ogolna_liczba_przyms = sizeof(admin_przyms);

    string *inne = admin_przyms - akt_przyms;
    //DBG("inne_od_zapisanych_w_adminie: "+COMPOSITE_WORDS(inne));
    if((sizeof(inne) * 2) <= ogolna_liczba_przyms)
        return 1;
    else
        return 0;
}

/**
 * Super rekurencyjna funkcja podaj�ca playera
 * kt�ry jest obecnie w posiadaczu obiektu 'ob'
 */
mixed
podaj_posiadacza(object ob)
{
    if (!objectp(ob))
        return 0;
    if (interactive(ob))
        return ob;
    return podaj_posiadacza(ENV(ob));
}

private int
orig_filter(object ob, string env)
{
    if (!sizeof(ob->query_owners()))
        return 0;
    if (sizeof(ob->query_orig_place()))
        return (ob->query_orig_place()[0] != env);

    return 0;
}

void
hook_ma_nieswoja_rzecz_off()
{
    hook_ma_nieswoja_rzecz_on=0;
}

void
hook_masz_nieswoja_rzecz(mixed patrol,object *kradzione)
{
    mixed tmp = Patrole[patrol];

    if(tmp[P_SKLAD][0]->query_attack())
        return;

    if(sizeof(kradzione)>1)
    {
        switch(random(3))
        {
        case 0:
            tmp[P_SKLAD][0]->command("powiedz Zdaje si�, �e "+
                "jeste� w posiadaniu rzeczy, kt�re "+
                "do ciebie nie nale��! Od�� je!");
            break;
        case 1:
            tmp[P_SKLAD][0]->command("powiedz Dostali�my zg�oszenie "+
                "o kradzie�y, prosz� to od�o�y�!");
            break;
        case 2:
            tmp[P_SKLAD][0]->command("powiedz Hej, to nie twoje! "+
                "Prosz� to od�o�y�.");
            break;
        }
        //coby nie zafludowali...
        hook_ma_nieswoja_rzecz_on=1;
        set_alarm(10.0,0.0,"hook_ma_nieswoja_rzecz_off");
    }
    else if(sizeof(kradzione))
    {
        switch(random(3))
        {
            case 0:
                tmp[P_SKLAD][0]->command("powiedz Zdaje si�, �e "+
                    "jeste� w posiadaniu rzeczy, kt�ra "+
                    "do ciebie nie nale�y! Od�� to!");

                break;
            case 1:
                tmp[P_SKLAD][0]->command("powiedz Dostali�my zg�oszenie "+
                    "o kradzie�y, prosz� to od�o�y�!");

                break;
            case 2:
                tmp[P_SKLAD][0]->command("powiedz Hej, to nie twoje! "+
                    "Prosz� to od�o�y�.");
                break;
        }
    }
}


void
wez_i_przenies_do_biura(mixed patrol, object x)
{
    mixed tmp = Patrole[patrol];

    //Heh Verek nie r�b nic tak emotami, potem sie co� zmieni w komunikacie i bedzie dziko wygl�da�.
    //Stra�nik zawsze mo�e wzi�� a potem dopiero przenosimy:) (KRUN)
    //---chodzi�o mi o to, �e stra�nik mo�e nie m�c unie�� tego (Vera). :P
    //tmp[P_SKLAD][0]->command("emote bierze "+x->short(PL_BIE)+".");
    tmp[P_SKLAD][0]->command("we� " + OB_NAME(x));
    tmp[P_SKLAD][0]->przenies_do_biura(x);
    remove_steal(file_name(x));
}

void
sprawdz_czy_odlozyl_niewinny(mixed patrol, object *kradzione,object kto)
{
    //DBG("2\n");
    mixed tmp = Patrole[patrol];

    object x = kradzione[0]; //TYMCZASOWO?! FIXME ?
    //foreach(object x : kradzione)
    //{
        //prosta sprawa - przedmiot le�y na lokacji.
        if(ENV(x) == ENV(tmp[P_SKLAD][0]) && !(x->query_prop(OBJ_M_NO_GET)))
        {
            wez_i_przenies_do_biura(patrol, x);
            start_patrol(patrol);
            return;
        }
        else
        {
            if(!czy_widzimy_playera(patrol,kto))
                return;

            if(!query_enemy_lvl(kto->query_real_name()))
                add_enemy(kto->query_real_name(),1);
            rozpetaj_walke(patrol, kto);
            this_object()->log("za kradziez", kto, x);
        }
    //}
}

void
zlodzieja_nie_ma_przedmiot_jest(mixed patrol, object *kradzione)
{
//DBG("zlodzieja_nie_ma_przedmiot_jest\n");
    mixed tmp = Patrole[patrol];

    stop_patrol(patrol);

    foreach(object x : kradzione)
    {
        //prosta sprawa - przedmiot le�y na lokacji.
        if(ENV(x) == ENV(tmp[P_SKLAD][0]) && !(x->query_prop(OBJ_M_NO_GET)))
        {
            switch(random(3))
            {
                case 0 :
                    tmp[P_SKLAD][0]->command("powiedz O, tu jest.");
                    break;
                case 1 :
                    tmp[P_SKLAD][0]->command("powiedz Patrzajcie, tu jest.");
                    break;
                case 2 :
                    tmp[P_SKLAD][0]->command("powiedz Ha, znale�li�my to!");
                    break;
            }

            wez_i_przenies_do_biura(patrol, x);
        }//ENV(x) jest w posiadaniu przedmiotu kradzionego (ale nie przez niego), x to ten przedmiot
        else if(ENV(x) && interactive(ENV(x)) && !(x->query_prop(OBJ_M_NO_GET)) &&
            member_array(ENV(x)->query_real_name(), x->query_owners()) == -1)
        {
            object kto=ENV(x);

            if(!czy_widzimy_playera(patrol,kto))
                return;

            if(ENV(kto) != ENV(tmp[P_SKLAD][0]))
            {
                idz_do_gracza(patrol,kto);
            }
            hook_masz_nieswoja_rzecz(patrol,kradzione);
            //DBG("1\n");
            set_alarm(7.0+itof(random(4)),0.0,
                "sprawdz_czy_odlozyl_niewinny",patrol,kradzione,kto);
        }
        //Innych nie ma! (np. je�li obiekt jest w subloku) TODO !!!
    }
}

/**
 * Oficer wchodz�c na lokacje bada deep_inventory,
 * ponadto, funkcja ta wywolana z drugim argumentem bada tylko
 * danego gracza (wywo�ywana np. kiedy wchodzi na lok.)
 */
void sprawdz_kradzione(mixed patrol, object komu = 0)
{
    mixed tmp = Patrole[patrol];
    object *kradzione=({ });

    //TUTAJ 'kradzione' dopisujemy tylko dla query_who_stole()
    if(!komu) //Czyli sprawdzamy wszystko. (wykonywane tu� PRZED wej�ciem)
    {
        kradzione = filter(deep_inventory(ENV(tmp[P_SKLAD][0])),
            &operator(!=)(0) @ sizeof @ &->query_who_stole());
            //(deep_inventory(ENV(TO)),
            //&->query_who_stole());
            //&->query_prop(OBJ_I_KRADZIONE));
            //&operator(==)(1) @ query_prop);
            //DBG("sprawdz_kradzione ca�ej lokacji.\n");
    }
    else //Sprawdzamy tylko danemu graczowi
    {
        if(!czy_widzimy_playera(patrol,komu))
            return;

        kradzione = filter(deep_inventory(ENV(komu)), &operator(!=)(0) @ sizeof @ &->query_who_stole());
        //kradzione = filter(deep_inventory(ENV(komu)),
            //&->sizeof(query_who_stole()));
            //&->query_prop(OBJ_I_KRADZIONE));
            //DBG("sprawdz_kradzione tylko graczowi.\n");

    }

    if(sizeof(kradzione))/* && ENV(TO) ==
        ENV(find_player(kradzione[0]->query_who_stole()[0])))*/
    {
        string arg_player=kradzione[0]->query_who_stole()[0];
        object arg_player_ob=find_player(arg_player);

        // Trzeba sprawdzi�, czy taki gracz w og�le jest w grze i czy jest na tej lokacji
        // W og�le to trzeba by jescze sprawdza� widoczno�� i takie tam.
        //
        // J.
        if(!komu || !arg_player_ob || (ENV(arg_player_ob) != ENV(tmp[P_SKLAD][0])))
        {
            //DBG("z�odzieja nie ma, ale przedmiot jest!");
            zlodzieja_nie_ma_przedmiot_jest(patrol,kradzione);
            return;
        }

        /*Ten warunek si� spe�nia, je�li np. jest z�odziej na lokacji, ale
        *skradziony przedmiot jest u innego livinga (np. z dru�yny) */
        if(member_array(kradzione[0],deep_inventory(arg_player_ob)) == -1)
        {
            //Najpierw zdob�d�my przedmiot(y)...
            object posiadacz = podaj_posiadacza(kradzione[0]);
            object kto_ukradl;
            tmp[P_SKLAD][0]->command("powiedz Bra� ich! Zasranych z�odziejaszk�w!");

            if(sizeof(kradzione[0]->query_who_stole()))
                kto_ukradl = find_player(kradzione[0]->query_who_stole()[0]);

            add_enemy(posiadacz->query_real_name(),1); //na czas operacji
            rzucamy_wroga_na_ziemie(patrol, posiadacz);
            grabiez(patrol,posiadacz);

            /*Okej, zdobyli�my ju� przedmiot, ale mo�e i pobijem
            /*Troch� z�odzieja, skoro ju� si� napatoczy�? ;)	*/
            if(kto_ukradl)
            {
                if(ENV(kto_ukradl) != ENV(tmp[P_SKLAD][0]))
                    idz_do_gracza(patrol,kto_ukradl);

                tmp[P_SKLAD][0]->command("powiedz "+
                    " A teraz z tob� si� rozprawimy, z�odziej"+
                    kto_ukradl->koncowka("u","ko","u")+"!");

                add_enemy(kto_ukradl->query_real_name(),1);
                rozpetaj_walke(patrol, kto_ukradl);
                this_object()->log("za kradziez", kto_ukradl);
            }

            return;
        }

        //DBG("posiadacz: "+arg_player);
        //Je�li opis gracza cho� troszk� si� zgadza
        if(sprawdz_czy_opis_pasuje(kradzione[0],arg_player_ob))
        {
            //just in case...
            if(arg_player_ob->query_prop(SCIGA_PATROL) !=
                tmp[P_SKLAD][0]->query_id_patrolu() ||
                arg_player_ob->query_prop(GRABIONY))
            {
                return;
            }
            //DBG("OPIS PASUJE! MAMY z�odzieja!");
            string gadka=tmp[P_SKLAD][0]->hook_zlapalismy_zlodzieja(arg_player_ob);

            tmp[P_SKLAD][0]->command("powiedz "+gadka);

            //Dodajemy enemy, je�li przypadkiem nie by�o gorzej...
            if(!query_enemy_lvl(arg_player))
                add_enemy(arg_player,1);
            //Idziemy do niego, je�li spieprzy�.
            if(ENV(arg_player_ob) != ENV(tmp[P_SKLAD][0]))
            {
                tmp[P_SKLAD][0]->command("krzyknij �apa� z�odzieja!");
                idz_do_gracza(patrol,arg_player_ob);
            }
            //Je�li player jest silniejszy od ca�ej ekipy patrolu ;) //TODO
            if(!testujemy_sile_wroga(patrol,arg_player_ob)) //trzeba tu zrobi� wsparcie
                tmp[P_SKLAD][0]->command("powiedz O szlag, ale bydle...");

            //No to lecimy z koksem...
            rozpetaj_walke(patrol, arg_player_ob);
            this_object()->log("za kradziez", arg_player_ob);

        }
    }
    else //Nie mamy query_who_stole(), ale trzeba sprawdzi�,
    {    //na podst. query_owners(), czy kto� co� zajuma� nieprzy�apany
        object* skradzione_bez_zlodzieja = filter(deep_inventory(ENV(tmp[P_SKLAD][0])),
            &orig_filter(, MASTER_OB(ENV(tmp[P_SKLAD][0]))));

        //foreach(object x:skradzione_bez_zlodzieja)
        //DBG("skradzione_bez_zlodzieja: "+x->short()+"\n");
        if(sizeof(skradzione_bez_zlodzieja))
        {
            zlodzieja_nie_ma_przedmiot_jest(patrol, skradzione_bez_zlodzieja);
        }
        //DBG("else sprawdz_czy_kradzione");
    }

    //foreach(object x: kradzione)
    //DBG(x->short()+"\n");
    return;
}

mixed
znajdz_wolny_patrol(string skad)
{
    mixed wolne = ({ });

    if(!stringp(skad))
        return 0;

    foreach (mixed x : m_indexes(Patrole))
    if (sizeof(Patrole[x][P_SKLAD]) && !sizeof(filter(Patrole[x][P_SKLAD], &->query_attack())) &&
        Patrole[x][P_SKLAD][0]->query_spolecznosc() ~= skad)
    {
        wolne += ({ x });
    }

    if (!sizeof(wolne))
        return 0;

    return wolne[random(sizeof(wolne))];
}

void
reaguj_bija_npca(object npc, object kto)
{
    //Szukamy wolnego patrolu.
    mixed tmp = znajdz_wolny_patrol(npc->query_spolecznosc());
    //set_alarm(5.0+itof(random(5)),0.0,"idz_do_gracza",tmp,npc);
    if(interactive(kto) && !kto->query_prop(SCIGA_PATROL) && npc->query_npc())
    {
        add_enemy(kto->query_real_name(),3);
        this_object()->log("dodany jako wrog lvl3 za lanie npca",kto,npc);
        idz_do_gracza(tmp,npc);


        if(ENV(tmp[P_SKLAD][0]) == ENV(npc)) //dotarli�my na miejsce!
        {
            //TODO!!! zaslanianie!!

            npc->command("przestan atakowac");
        }
    }

    set_alarm(20.0,0.0,"sprawdz_czy_jeszcze_walka",tmp,kto);

    return;
}

/*
 *Wywo�ane przez npca, kt�ry ma w��czone wzywanie stra�y, �e jest
 *w�a�nie atakowany.
 */
void
atak_na_npca(object npc,object kto)
{
    if (get_alarm(reaguj_bija_npca_alarm) == 0 && !kto->query_prop(SCIGA_PATROL))
    {
        reaguj_bija_npca_alarm = set_alarm(3.0+itof(random(5)),0.0,"reaguj_bija_npca",npc,kto);
    }
}

public void
log(string powod, object kto, object co = 0)
{
    if(kto->query_real_name() + "->" + powod +" "+(co == 0 ? "" : file_name(co)) != last_log)
        SECURITY->log_public("STRAZ", ctime(time()) + " " +
        kto->query_real_name() + "->" + powod +" "+(co == 0 ? "" : file_name(co))+ "\n");

    last_log = kto->query_real_name() + "->" + powod +" "+(co == 0 ? "" : file_name(co));
}
