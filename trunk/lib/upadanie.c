/**
 * Plik jest wsparciem do upadania, w p�niejszym czasie powinno to zosta�
 * przeniesione do standardu rooma lub livinga - jeszcze nie wiem:)
 *
 * @author Krun
 * @date 28-09-2007
 */

#pragma no_clone
#pragma strict_types

#include <sit.h>
#include <files.h>
#include <macros.h>
#include <stdproperties.h>

/**
 * Funkcja losuje odpowiednie miejsce, na kt�rym zostanie po�o�ony
 * gracz.
 * @param sits sitlokacje
 * @return Nazwa sitlokacji.
 */
private static nomask string
losuj_sita(mixed sits)
{
    if(!pointerp(sits))
        return 0;

    int size = sizeof(sits);

    if(!size || size%3 != 0)
        return 0;

    int *chance = allocate(size);
    for(int i = 0; i<size; i+=3)
    {
        if(pointerp(sits[i]))
	{
            foreach(string sit : sits[i])
            {
	        foreach(string dsp : DEFAULT_SIT_PLACES)
	        {
	            if(wildmatch(dsp, sit))
		    {
                        chance[i] = 90;
		        break;
                    }
		    else
                        chance[i] = 10;
	        }
	    }
	}
    }

    int rnd = random(fold(chance, &operator(+)(), 0));

    write(rnd);

    int act = 0, last = 0;
    for(int i = 0; i<sizeof(chance); i++)
    {
        act += chance[i];
        if(rnd > last && rnd < act)
            return sits[i*3];
        last += chance[i];
    }
}

/**
 * Funkcja sprawdza czy wybrana przez nas pozycja jest w podanych sitach,
 * je�li tak to zwraca prawde je�li nie fa�sz.
 * @return prawda je�li sitlokacja znajduje si� w sitach, fa�sz je�li nie.
 */
private static nomask int
sprawdz_czy_jest_w_sitach(string na_co, mixed sits)
{
    if(!pointerp(sits) || !sizeof(sits))
        return 0;

    for(int i = 0; i<sizeof(sits);i+=3)
    {
        if(pointerp(sits[i]))
        {
            foreach(string sit : sits[i])
            {
                if(sit == na_co)
		    return 1;
            }
        }
        else if(sits[i] == na_co)
            return 1;
    }
    return 0;
}

/**
 * Funkcja upadaj�ca gracza.
 * @param kogo Kogo chcemy upadn��:P
 * @param na_co Je�li mamy ju� jak�� sublokacje na kt�r� gracz ma upa��
 *              i ta sublokacja jest na lokacji na kt�rej stoi to w�a�nie
 *              tam upadnie, je�li nie to upadnie na losow� sublokacje.
 * @return <b>0</b> - powodzenie,
 *         <b>2</b> - pora�ka(nie upadli�my go),
 *         <b>3</b> - ju� le�y nie ma kogo przewraca�:),
 *         <b>1</b> - udalo sie, ale przewr�cili�my siedz�cego.
 */
varargs int
upadnij_go(object kogo, mixed na_co=0, int no_hp=0, int paralyze=0)
{
    object env;
    mixed sits;

    if(!kogo)
        return 2;

    if(kogo->query_prop(SIT_LEZACY))
        return 3;

    env = ENV(kogo);

    if(!env)
        return 2;

    sits = env->query_sit();

    if(!sits || !sizeof(sits))
        return 2;

    if(!na_co || !sprawdz_czy_jest_w_sitach(na_co, sits))
        na_co = losuj_sita(sits);

    if(!no_hp)
    {
        //Odejmujemy hpki.. W ko�cu jak si� przewraca to si� t�ucze.
        kogo->reduce_hp(0, 600 + random(600), 10);
    }

    kogo->add_prop(OBJ_I_DONT_GLANCE, 1);
    kogo->add_prop(SIT_LEZACY, ({na_co}));

    //Dodajemy parali�:
    if(paralyze)
    {
        object par = clone_object(PARALYZE_OBJECT);
        par->set_remove_time(paralyze);
        par->move(kogo);
    }

    if(kogo->query_prop(SIT_SIEDZACY))
    {
        kogo->remove_prop(SIT_SIEDZACY);
        return 1;
    }

    return 0;
}
