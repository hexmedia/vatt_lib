/**
 * \file /lib/train.c
 *
 * Plik pomocny w zdobywaniu do�wiadczenia teoretycznego.
 *
 * @author Krun
 * @date Grudzie� 2007
 */

#include <exp.h>
#include <macros.h>
#include <formulas.h>
#include <cmdparse.h>
#include <composite.h>

#define MOD_MIN     50
#define MOD_MAX     1000

inherit "/lib/trade.c"; //wspolne liby - pergaminu i trenera tego potrzebuj�.

/**
 * Funkcja zwraca cene przeliczon� na podstawie ceny podstawowej, zale�n� od umiej�tno�ci.
 *
 * @param um umiej�tno�� kt�rej cene zwr�ci�.
 * @param cena podstawowa cena
 * @param max maksymalny poziom uma do nabycia
 * @param min minimalny poziom uma do nabycia
 * @param ob obiekt dla ktorego cena ma by� zwr�cona
 *
 * @return cene jak� gracz zap�aci za nastepny pergamin od (MIN_MOD% podstawowej do MAX_MOD% -
 *              aktualnie MIN_MOD = 50% a MAX_MOD = 1000%)
 */
varargs static int
sprawdz_cene(int um, int cena, int mn, int mx, object ob=TP)
{
    int cur = ob->query_acc_exp(um, EXP_TEORETYCZNY);

    //Je�li warto�ci graniczne nie s� ustawione to ustawiamy je na
    if(mx)
        mx = ob->skill_to_exp(mx);
    else
        mx = ob->skill_to_exp(F_MAX_STAT);

    if(mn)
        mn = ob->skill_to_exp(mn);
    else
        mn = 0;

    if(cur == -1)
        cur = 0;

    //Je�li minimum jest wi�ksze to zwracamy -1 czyli brak mo�liwo�ci treningu.
    if(cur < mn)
        return -1;

    //Je�li wi�ksze to -2 czyli te� brak mo�liwo�ci treningu
    if(cur > mx)
        return -2;

    cur = ob->query_tskill(um);

    return F_TR_PRZELICZ_CENE(cur, MOD_MAX, MOD_MIN, cena);
}


public string
pokaz_cene(int cena, int przyp=0)
{
    if(cena > 0)
        return text(split_values(cena), przyp);
    else if(cena == -1)
        return "jeste� zbyt niedo�wiadczon"+TP->koncowka("y","a");
    else if(cena == -2)
        return "jeste� zbyt do�wiadczon"+TP->koncowka("y","a");
}


/**
 * Funkcja odpowiada za poprawne dodawanie exp teoretycznego.
 * W�a�ciwie mo�naby to z powodzeniem robi� bez niej, jak w przypadku
 * expa praktycznego, ale tak by�o mi �atwiej:P
 */
public int
dodaj_expa(int um, object ob, int jakosc, int mn, int mx)
{
    int cur = ob->query_acc_exp(um, EXP_TEORETYCZNY);

    if(mx)
        mx = ob->skill_to_exp(mx);
    if(mn)
        mn = ob->skill_to_exp(mn);

    if(cur == -1)
        cur = 0;

    // Nie dodajemy bo nie spe�nia warunk�w.
    if(cur > mx || cur < mn )
        return 0;

    jakosc = min(mx, jakosc);

    if(jakosc > 0)
    {
        ob->increase_ss(um, jakosc, EXP_TEORETYCZNY);
        //no, a czemu by nie? V
        ob->increase_ss(SS_INT, MIN(jakosc/20,70));

        return 1;
    }
}