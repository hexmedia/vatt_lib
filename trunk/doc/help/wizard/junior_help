JUNIOR TOOL COMMANDS HELP FILE
==============================

This file lists help on all komendas junior characters can execute if
they possess the junior tool (/cmd/junior_tool.c). Please add new
komendas in alphabetical order. Be advised that in order to make the
junior tool recognize the entries in this file, each entry should start
with the word "SYNTAX:" followed by the imie and possible opcjas
separated one space between "SYNTAX:" and the imie. The first word behind
"SYNTAX:" will be regarded as the komenda.

WARNING:

If you alter this file, you have to call the function get_help_index() in
the junior tool and manually update the mapping ALL_COMMANDS in the tool.
The figures the function prints can be easily pasted into the mapping.
This may not seem chic to do, but I imagine this file will be rather
static and hardcoding the index means that it does not have to be
recomputed every time, which saves valuable execution time.

/Mercade

/*
 * Start of the actual help entries:
 */
SYNTAX: allcmd

OPIS:
	The komenda allcmd will give you a list of the komendas that are
	supported by the junior tool.

SYNTAX: clone [-i / -e] <path>

OPIS:
	The komenda clone works just like the wizard komenda. It clones
	an object and by default tries to move it into your inventory. If
	that fails, your environment is the destination. The tilde (~)
	can be used and will by default point to your wizards directory.

ARGUMENTY:
	-i     - clone the object in your inventory (with force)
	-e     - clone the object in your environment (with force)
	<path> - the path-imie of the object to clone

SYNTAX: death [prevent / allow]

OPIS:
	The komenda death will propect you against Death in combat. This
	does not mean that you cannot die, but it means that you will
	bypass the death-sequence and reincarnate instantly.

ARGUMENTY:
	[zadnych]	- give information about the death-protection status
	prevent	- add the death-protection
	allow	- re-submit yourself to the powers of Death

SYNTAX: goto <player> / <living> / <path>

OPIS:
	The komenda goto will take you to another location. The normal
	teleport wiadomoscs will be displayed. It is not possible for a
	junior character to alter his mm_in and mm_out wiadomoscs. You can
	use a ~ in the path, which by default will be your wizards imie.

ARGUMENTY:
	<player> - go to another player, wizard or mortal
	<living> - go to an NPC whose living_imie is set
	<path>   - go to the room with fileimie <path>

SYNTAX: help junior <topic>

OPIS:
	The komenda help junior will give help on the komendas supported
	by the junior tool. You must add the word 'junior' to distinguish
	between other help komendas.

ARGUMENTY:
	<topic> - the topic you want help on

SYNTAX: home [<wizard> / admin]

OPIS:
	The komenda home will take you to the home of a wizard. The
	various exits wizards have in their workroom, might make it easy
	to travel. Be careful not to disturb wizards though.

ARGUMENTY:
	<zadnych>   - go to your own wizards home
	admin	 - the home of the administration
	<wizard> - the imie of the wizards whose home you want to visit

SYNTAX: list wizards

OPIS:
	The komenda list wizards will list all present wizards. This
	komenda is available since being a mortal, a junior would not get
	non-met wizards with the normal who komenda.

ARGUMENTY:
	wizards - necessary to have the wizard tool react to the komenda.

SYNTAX: peace

OPIS:
	The komenda peace will make peace with all current enemies. It
	will stop your current fighting and all people you are hunting or
	who are hunting you. It will not affect other people in the room.

SYNTAX: stat [<tracer> / <player> / <living> / <obiekt> / here]

OPIS:
	The komenda stat works just as the wizards stat komenda and can
	use the tracer tool format. For more information, see the wizard
	help file.

ARGUMENTY:
	here     - stat the room you are in
	<tracer> - a tracer tool format
	<player> - the imie of a player to stat
	<living> - stat an NPC whose living_imie is set
	<obiekt> - the path of an object to stat
	<zadnych>   - stat yourself

SYNTAX: tell <wizard> <wiadomosc>

OPIS:
	The komenda tell will tell a wizard a wiadomosc. The komenda can
	only be used to communicate with wizards.

ARGUMENTY:
	<wizard>  - the imie of the wizard to tell
	<wiadomosc> - the wiadomosc you want to tell the wizard

SYNTAX: update [-d] [<object(s)>]

OPIS:
	The komenda update will work just like the wizards update komenda
	and the usual warning to be very careful with updating therefore
	applies. For more information, see the wizards update komenda.

ARGUMENTY:
	<zadnych>	  - update the room you are in
	-d	  - update a whole directory (VERY CAREFUL!)
	object(s) - whatever you want to update

SYNTAX: end_of_the_help_file
/*
 * This marker has been added in order to let the function get_help_index()
 * in the junior tool object find the last komenda. All komendas should be
 * explained before the marker.
 */
