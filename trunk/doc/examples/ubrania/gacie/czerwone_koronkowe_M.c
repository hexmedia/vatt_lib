/*
 * Czerwone koronkowe majteczki.
 *
 * Autor: Alcyone
 */

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <macros.h>
#include <materialy.h>

void
create_armour()
{
    ustaw_nazwe_glowna("para");
    ustaw_nazwe("majteczki");
    
    dodaj_przym("czerwony","czerwoni");
    dodaj_przym("koronkowy", "koronkowi");

    set_long("Do�� mocno wyci�te czerwone koronkowe majteczki. Kszta�ty znajduj�ce "+ 
             "si� na nich przedstawiaj� male�kie rozwini�te czarne r�yczki, kt�re przeplataj� "+ 
             "si� ze sob� tworz�c stylowy wz�r. Nie posiadaj� �adnych zb�dnych ozd�b.\n");

    add_item(({"kszta�ty", "wz�r", "r�yczki", "czarne r�yczki"}), "Male�kie rozwini�te czarne r�yczki " +
		    "odwzorowane zosta�y z niesamowit� precyzj�.\n");
    
    set_slots(A_HIPS);
    add_prop(OBJ_I_WEIGHT, 20);
    add_prop(OBJ_I_VOLUME, 100);
    add_prop(OBJ_I_VALUE, 50);
    
    /* majteczki s� bardzo rozci�gliwe */
    add_prop(ARMOUR_F_PRZELICZNIK, 50.0);
    /* ustalamy, �e majteczki s� dla kobiet */
    add_prop(ARMOUR_I_DLA_PLCI, 1);
    /* ustawiamy materia� */
    ustaw_material(MATERIALY_KORDONEK);
    /* ustawiamy rozci�gliwo�� w d� na 30% */
    add_prop(ARMOUR_I_D_ROZCIAGLIWOSC, 30);
}
string
query_auto_load()
{
   return ::query_auto_load();
} 
