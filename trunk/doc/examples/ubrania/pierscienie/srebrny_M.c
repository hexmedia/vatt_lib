/*
 * Srebrny pier�cie�.
 *
 * Autor: Alcyone
 */

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <macros.h>
#include <materialy.h>

void
create_armour()
{

    ustaw_nazwe(({"pier�cie�","pier�cienia","pier�cieniowi","pier�cie�","pier�cieniem","pier�cieniu"}),
                ({"pier�cienie","pier�cieni","pier�cieniom","pier�cienie","pier�cieniami","pier�cieniach"}),
               PL_MESKI_NOS_NZYW);
    dodaj_przym("srebrny","srebrni");
    set_long("Wykonana ze srebra obr�czka. Doskonale wypolerowana, odbija wszelkie padaj�ce na� �wiat�o. " +
		    "Nie jest ozdobiona �adnym szlachetnym kamieniem, lecz mimo tego prezentuje si� bardzo " +
		    "dostojnie.\n");

    set_slots(A_ANY_FINGER);
    /* Obj�to�� pier�cienia jest malutka. */
    add_prop(OBJ_I_VOLUME, 3);
    /* Troszk� tutaj oszukujemy, poniewa� rzecz nie mo�e przecie� si� zmniejsza� w czasie zak�adania.
     * Jednak przeskok mi�dzy intami jest zbyt du�y jak tego nie zastosujemy. */
    add_prop(ARMOUR_F_PRZELICZNIK, 0.5);
    add_prop(OBJ_I_VALUE, 200);
    /* Ustawiamy materia�y, z kt�rego jest wykonany pier�cie� */
    ustaw_material(MATERIALY_SREBRO); // defaultowo b�dzie to 100%
}

string
query_auto_load()
{
   return ::query_auto_load();
}
