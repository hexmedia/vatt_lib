/*
 * Czarne b�yszczi�ce buty.
 *
 * Autor: Alcyone
 */

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <macros.h>
#include <materialy.h>

string
maly_opis()
{
	if (this_player()->query_gender()) {
		return "Kobieta przy ich zak�adaniu musi sp�dzi� dosy� sporo czasu.";
	}
	else {
		return "Zdaje si�, �e buty zosta�y uszyte z my�l� o kobiecej stopie.";
	}
}

void
create_armour()
{  
    ustaw_nazwe_glowna("para");
    ustaw_nazwe("buty");

    dodaj_przym("czarny","czarne");
    dodaj_przym("blyszcz�cy","blyszcz�ce");

    set_long("Siegaj�ce nieco poni�ej kolana czarne buty zrobiono z g�adkiej i b�yszcz�cej �wi�skiej sk�ry. "+
             "Na �rodku wi�zane s� tak, �e daj� �adny krzy�owy wz�r. @@maly_opis@@ Posiadaj� ma�y obcas, "+
	     "kt�ry nie powinien utrudnia� podr�owania. Idealnie wprost przylegaj� do nogi i dopasowuj� si� "+
             "do jej kszta�tu. Nie posiadaj� �adnych zb�dnych ozd�b, a jednocze�nie wygl�daj� "+
             "bardzo elegancko i gustownie. Buty s� bardzo du�e.\n");

    add_item(({"obcas", "ma�y obcas"}), "Ma�y obcas dosztukowany do tylnej cz�ci podeszwy. Zdaje si� " +
		    "by� na tyle dobrze dobrany, by nie przeszkadza� w czasie podr�y.\n");
    add_item(({"podeszw�"}), "Wykonana z grubej, utwardzanej sk�ry, ma dosztukowany w tylnej cz�ci ma�y " +
		    "obcas.\n");

    set_slots(A_FEET | A_SHINS);
    add_prop(OBJ_I_WEIGHT, 100);
    add_prop(OBJ_I_VOLUME, 1400);
    add_prop(OBJ_I_VALUE, 200);
    
    /* buty na nodze tylko w niewielkim stopniu zwi�kszaj� swoj� obj�to�� */
    add_prop(ARMOUR_F_PRZELICZNIK, 10.0);
    /* ustalamy, �e buciki s� dla kobiet */
    add_prop(ARMOUR_I_DLA_PLCI, 1);
    /* ustawiamy materia� */
    ustaw_material(MATERIALY_SK_SWINIA);
    /* buciki troch� bardziej powi�kszaj� rozmiar ni� normalne ubrania */
    add_prop(ARMOUR_F_POW_DL_STOPA, 3.0);
    add_prop(ARMOUR_F_POW_SZ_STOPA, 3.0);
}
string
query_auto_load()
{
   return ::query_auto_load();
} 
