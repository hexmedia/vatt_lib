
/* Czarne skorzane spodnie
Wykonano dnia 20.06.2005 przez Avarda. Opis zaczerpniety z banku opisow,
napisany przez Ghalleriana w dziale "ubrania i inne przedmioty" 
Poprawki by Avard 20.05.06 */

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <pl.h>
#include <macros.h>
#include <materialy.h>
#include <object_types.h>

void
create_armour()
{
    ustaw_nazwe("spodnie");
    ustaw_nazwe_glowna("para");

    dodaj_przym("czarny","czarni");
    dodaj_przym("sk^orzany","sk^orzani");

    set_long("Sk^ora z kt^orej zosta^ly wykonane spodnie jest przedniego "+
        "rodzaju. Mi^ekko^s� w dotyku za zarazem wytrzyma^lo^s� sprawiaj^a, "+
        "^ze jest ^swietnym materia^lem na ubranie. Spodnie posiadaj^a "+
        "zdobne szwy po obu bokach. Zapewne wplecione zosta^ly w nie srebrne "+
        "nitki. Czer^n i srebro wy^smienicie komponuj^a si^e i sprawiaj^a, "+
        "^ze spodnie wygl^adaj^a wyj^atkowo elegancko. Do spodni do^l^aczony "+
        "jest pasek dzi^eki, kt^oremu mo^zna bezpiecznie je nosi�. Po lewej "+
        "stronie posiadaj^a jedn^a kiesze^n, ale za to bardzo pojemn^a.\n");

    set_slots(A_LEGS, A_HIPS);
    add_prop(OBJ_I_VOLUME, 1000);
    add_prop(OBJ_I_WEIGHT, 837);
    add_prop(OBJ_I_VALUE, 75);
    ustaw_material(MATERIALY_SK_SWINIA, 100); /* Do zmiany, bo za cholere nie
	wiem jaka tu moze byc skora :P */
    set_type(O_UBRANIA);
    add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    set_size("M");

    /* --- dodajemy kiesze�! --- */
    add_prop(CONT_I_MAX_VOLUME, 1500);
    add_prop(CONT_I_MAX_WEIGHT, 1200);
    
    add_subloc(({"kiesze�", "kieszeni", "kieszeni", "kiesze�", "kieszeni�", "kieszeni"}));
    add_subloc_prop("kiesze�", SUBLOC_S_OB_GDZIE, "wewn�trz pojemnej kieszeni");
    add_subloc_prop("kiesze�", SUBLOC_I_OB_PRZYP, PL_DOP);
    add_item("kiesze� w", "Jest to pojemna kiesze�" +
        "@@opis_sublokacji| zawieraj�ca |kiesze�|.|.|" + PL_BIE + "@@\n", PL_MIE);
    /* ------------------------- */
}
string
query_auto_load()
{
   return ::query_auto_load();
}
