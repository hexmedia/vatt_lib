/*
 * Czarne obcis�e spodnie.
 *
 * Autor: Alcyone
 */

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <macros.h>
#include <materialy.h>

void
create_armour() 
{
    ustaw_nazwe_glowna("para");
      ustaw_nazwe("spodnie");

     dodaj_przym("czarny","czarni");
     dodaj_przym("obcis�y","obci�li");
      
     set_long("Czarne obcis�e spodnie zrobione s� z bydl�cej, mi�ej w dotyku b�yszcz�cej sk�ry. "+
              "Prawdopodobnie uszyto je z my�l� o kobietach, gdy� ich kr�j �wiadczy o tym, �e s� "+
	      "niezwykle dopasowane do cia�a. Wygodne, nie kr�puj�ce ruch�w, idealnie nadaj� si� na "+
              "d�ugie podr�e jak i na spotkania ze znajomymi. Si�gaj� tylko troch� "+
              "poni�ej kolan, wi�c w czasie zimnych dni niskie buty nie b�d� stanowi�y odpowiedniej "+
              "ochrony.\n");

     set_ac(A_THIGHS | A_HIPS | A_STOMACH, 2, 2, 2);
     add_prop(OBJ_I_WEIGHT, 100);
     add_prop(OBJ_I_VOLUME, 1800);
     add_prop(OBJ_I_VALUE, 100);

     add_prop(ARMOUR_I_DLA_PLCI, 1);
     ustaw_material(MATERIALY_SK_BYDLE);
}
string
query_auto_load()
{
   return ::query_auto_load();
}  
 
