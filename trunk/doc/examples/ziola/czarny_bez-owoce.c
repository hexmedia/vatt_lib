/*
 * wyst�powanie: ��gi, ale og�lnie to wsz�dzie
 *
 * by Brz�zek
 *
 * Poprawki - Krun 2007
 */


inherit "/std/herb";

#include <pl.h>
#include <herb.h>
#include <materialy.h>
#include <stdproperties.h>

void
create_herb()
{
    ustaw_nazwe_glowna_ziola("ki�� owoc�w");
    ustaw_nazwe_ziola("bez");
    dodaj_przym_zident("czarny", "czarni");
    dodaj_nazwy_ziola("ki��");
    dodaj_nazwy_ziola("owoce");

    ustaw_nazwe("kisc owoc�w");
    dodaj_nazwy("owoce");
    dodaj_przym("ciemnofioletowy", "ciemnofioletowi");

    dodaj_id_nazwy_calosci("bez");
    ustaw_id_short_calosci(({"wysoki drzewiasty krzew czarnego bzu",
        "wysokiego drzewiastego krzewu czarnego bzu", "wysokiemu drzewiastemu krzewowi czarnego bzu",
        "wysoki drzewiasty krzew czarnego bzu", "wysokim drzewiastym krzewem czarnego bzu",
        "wysokim drzewiastym krzewie czarnego bzu"}),
       ({"wysokie drzewiaste krzewy czarnego bzu", "wysokich drzewiastych krzew�w czarnego bzu",
        "wysokim drzewiastym krzewom czarnego bzu", "wysokie drzewiaste krzewy czarnego bzu",
        "wysokimi drzewiastymi krzewami czarnego bzu", "wysokich drzewiastych krzewach czarnego bzu"}),
        PL_MESKI_NOS_NZYW);
    dodaj_nazwy("krzew");
    dodaj_id_przym_calosci("wysoki", "wysocy");
    dodaj_id_przym_calosci("czarny", "czarni");
    dodaj_id_przym_calosci("drzewiasty", "drzewiasci");

    ustaw_unid_nazwe_calosci("krzew");
    dodaj_unid_przym_calosci("wysoki", "wysocy");
    dodaj_unid_przym_calosci("drzewiasty", "drzewiasci");
    
    set_id_long("Ma�e, kuliste pestkowce barwy tak ciemnofioletowej, �e prawie "+
        "czarnej zawieszone na kruchych szypu�kach tworz� ci�kie baldachokszta�tne "+
        "ki�cie owoc�w czarnego bzu. Owoce te wykorzystywane s� przez wie�niaczki do "+
        "produkcji sok�w i marmolad zapewniaj�cych witaminy w czasie zimowych ch�od�w. "+
        "Co bardziej dba�e o urod� niewiasty u�ywaj� te� tego soku do farbowania "+
        "w�os�w. Te wykorzystywane przez wszystkich w gospodarstwie domowym niepozorne "+
        "owocki kryj� w sobie medyczne skarby, a szczeg�lnie skuteczne s� w zwalczaniu "+
        "b�lu r�nego pochodzenia nie powoduj�c uzale�nie�. Odwar z suszonych owoc�w "+
        "lub sok ze �wie�ych pity 2-4 razy dziennie przynosi ulg� w przypadkach "+
        "ischiasu, nerwob�li i migreny. Dodatkowo �agodnie przeczyszcza co ma "+
        "dzia�anie odtruwaj�ce. W niekt�rych karczmach szklaneczka soku o poranku "+
        "przewidziana jest gratisowo dla klient�w, kt�rzy nadu�yli w nocy trunk�w. "+
        "Niekt�rzy medycy podaj� te� napar z kwiat�w, li�ci i owoc�w czarnego bzu "+
        "cukrzykom. \n");
	
    set_unid_long("Ma�e, kuliste pestkowce barwy tak ciemnofioletowej, �e prawie "+
        "czarnej, zawieszone na kruchych szypu�kach tworz� ci�kie "+
        "baldachokszta�tne ki�cie owoc�w czarnego bzu. Owoce te wykorzystywane s� "+
        "przez wie�niaczki do produkcji sok�w i marmolad zapewniaj�cych witaminy "+
        "w czasie zimowych ch�od�w. Co bardziej dba�e o urod� niewiasty u�ywaj� "+
        "te� tego soku do farbowania w�os�w. \n");

    ustaw_czas_psucia(350, 400); //schniecie, niszczenie
    ustaw_trudnosc_identyfikacji(17); // Trza miec uma powyzej 17 coby to znalesc.
    ustaw_czestosc_wystepowania(10); // Bedzie wystepowac na 10% lokacji
    ustaw_sposob_wystepowania(HERBS_WYST_POJEDYNCZO); //Rosnie pojedynczo
    ustaw_ilosc_w_calosci(4);
    set_amount(1); //pozywnosc
   
    ustaw_przym_zepsutego("zgni�y", "zgnili");

    ustaw_ilosc_w_calosci(1); //FIXME

    add_prop(OBJ_I_VALUE, 10); //wartosc
    add_prop(OBJ_I_VOLUME, 100); //FIXME
    add_prop(OBJ_I_WEIGHT, 100); //FIXME

    // ustaw_material(MATERIALY_ROSLINA_ZIELONA);
}
