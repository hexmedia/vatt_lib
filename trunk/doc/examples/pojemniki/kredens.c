/*
 * ma�y kredens
 * (C) 2005 by jeremian
 */

#pragma strict_types

inherit "/std/container";

#include <pl.h>
#include <macros.h>
#include <filter_funs.h>
#include <stdproperties.h>
#include <composite.h>
#include <cmdparse.h>
#include <object_types.h>

nomask void
create_container()
{
    ustaw_nazwe(({"kredens","kredensu","kredensowi","kredens","kredensem",
    "kredensie"}),({"kredensy","kredens�w","kredensom","kredensy","kredensami",
    "kredensach"}),PL_MESKI_NOS_NZYW);

    dodaj_przym("ma�y","ma�e");

    set_long("Oto " + implode(query_przym(1), " ") + " " + query_nazwa() +
	    ". Mo�na wk�ada� rzeczy do pierwszej szuflady, drugiej szuflady, " + 
	    "k�a�� na pierwsz� p�k�, drug� p�k�, trzeci� p�k� " +
	    "i na kredens." +
	    "@@opis_sublokacji| W pierwszej szufladzie widzisz |pierwsza szuflada|.||" + PL_BIE+ "@@" +
	    "@@opis_sublokacji| W drugiej szufladzie widzisz |druga szuflada|.||" + PL_BIE+ "@@" +
	    "@@opis_sublokacji| Na pierwszej p�ce widzisz |pierwsza p�ka|.| Pierwsza" +
	    " p�ka jest pusta.|" + PL_BIE+ "@@" +
	    "@@opis_sublokacji| Na drugiej p�ce widzisz |druga p�ka|.||" + PL_BIE+ "@@" +
	    "@@opis_sublokacji| Na trzeciej p�ce widzisz |trzecia p�ka|.||" + PL_BIE+ "@@" +
	    "@@opis_sublokacji| Na kredensie widzisz |na|.| Kredens pokrywa gruba warstwa kurzu.|"
	    + PL_BIE+ "@@\n");
    setuid();
    seteuid(getuid());
    add_prop(CONT_I_MAX_VOLUME, 20000);
    add_prop(CONT_I_VOLUME, 16800);

    /* ten mebel mo�na dowolnie przenosi� - czemu nie? */
/*    add_prop(OBJ_I_NO_GET, "Nie da sie go podniesc.\n");*/

    add_prop(CONT_I_CANT_WLOZ_DO, 1); /* By nie mo�na by�o nic do niego w�o�y� komend� 'wloz do' */

    add_prop(CONT_I_WEIGHT, 10000);
    add_prop(CONT_I_MAX_WEIGHT, 20000);

    /* dodajemy sublokacje */
    
    add_subloc("na");
    add_subloc(({"pierwsza szuflada", "pierwszej szuflady", "pierwszej szufladzie",
                 "pierwsz� szuflad�", "pierwsz� szuflad�", "pierwszej szufladzie"}));
    add_subloc(({"druga szuflada", "drugiej szuflady", "drugiej szufladzie",
                 "drug� szuflad�", "drug� szuflad�", "drugiej szufladzie"}));
    add_subloc(({"pierwsza p�ka", "pierwszej p�ki", "pierwszej p�ce",
                 "pierwsz� p�k�", "pierwsz� p�k�", "pierwszej p�ce", "na"}));
    add_subloc(({"druga p�ka", "drugiej p�ki", "drugiej p�ce",
                 "drug� p�k�", "drug� p�k�", "drugiej p�ce", "na"}));
    add_subloc(({"trzecia p�ka", "trzeciej p�ki", "trzeciej p�ce",
                 "trzeci� p�k�", "trzeci� p�k�", "trzeciej p�ce", "na"}));

    /* dodajemy rzeczy, by mo�na by�o je ogl�da� */
    
    add_item(({"p�k� w", "pierwsz� p�k� w"}), "Jest to szeroka drewniana p�ka."+
               "@@opis_sublokacji| Widzisz na niej |pierwsza p�ka|.@@\n", PL_MIE);
    add_item("drug� p�k� w", "Jest to w�ska drewniana p�ka."+
               "@@opis_sublokacji| Widzisz na niej |druga p�ka|.@@\n", PL_MIE);
    add_item("trzeci� p�k� w", "Jest to lekko spr�chnia�a drewniana p�ka."+
               "@@opis_sublokacji| Widzisz na niej |trzecia p�ka|.@@\n", PL_MIE);
    add_item(({"szuflad� w", "pierwsz� szuflad� w"}), "Jest to ma�a szuflada" +
	       "@@opis_sublokacji| zawieraj�ca |pierwsza szuflada|.|.@@\n", PL_MIE);
    add_item("drug� szuflad� w", "Jest to obszerna szuflada" +
	       "@@opis_sublokacji| zawieraj�ca |druga szuflada|.|.@@\n", PL_MIE);

    /* ustawiamy odpowiednie propy dla 'obejrzyj' */
   
    add_subloc_prop("pierwsza szuflada", SUBLOC_S_OB_GDZIE, "wewn�trz pierwszej szuflady");
    add_subloc_prop("pierwsza szuflada", SUBLOC_I_OB_PRZYP, PL_DOP);
    add_subloc_prop("druga szuflada", SUBLOC_S_OB_GDZIE, "wewn�trz drugiej szuflady");
    add_subloc_prop("druga szuflada", SUBLOC_I_OB_PRZYP, PL_DOP);
    add_subloc_prop("pierwsza p�ka", SUBLOC_S_OB_GDZIE, "na pierwszej p�ce");
    add_subloc_prop("pierwsza p�ka", SUBLOC_I_OB_PRZYP, PL_DOP);
    add_subloc_prop("druga p�ka", SUBLOC_S_OB_GDZIE, "na drugiej p�ce");
    add_subloc_prop("druga p�ka", SUBLOC_I_OB_PRZYP, PL_DOP);
    add_subloc_prop("trzecia p�ka", SUBLOC_S_OB_GDZIE, "na trzeciej p�ce");
    add_subloc_prop("trzecia p�ka", SUBLOC_I_OB_PRZYP, PL_DOP);

    /* sami opisujemy kontener */
    add_prop(CONT_I_DONT_SHOW_CONTENTS, 1);
}

public int
query_type()
{   
    return O_MEBLE;
}

