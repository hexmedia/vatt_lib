inherit "/std/herb";

#include "ziola.h"

create_herb()
{
    ustaw_nazwe(KORZEN, PL_MESKI_NOS_NZYW);

    dodaj_przym("bialy", "biali");
    dodaj_przym("dlugi", "dludzy");

    ustaw_nazwe_ziola(({"chrzan", "chrzanu", "chrzanowi", "chrzan", "chrzanem", "chrzanie"}), PL_MESKI_NOS_NZYW); 
    
    set_id_long("Jest to nic innego jak zwykly, pospolity chrzan. Nie ma on zadnych specjalnym "+
	"wlasciwosci ani zastosowan poza tym, ze pachnie hmm... niezbyt przyjemnie...\n");

    set_unid_long("Masz przed soba dlugi, bialy korzen od ktorego bije niemily zapach. Coz "+
	"to moze byc...\n");

    set_effect(HERB_SPECIAL, special_effect);       
    set_decay_time(210);
    set_id_diff(23);
    set_find_diff(2);
    set_amount(20);
    set_herb_value(28);
}
