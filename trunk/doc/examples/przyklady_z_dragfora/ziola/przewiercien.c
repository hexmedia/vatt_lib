inherit "/std/herb";

#include "ziola.h"

create_herb()
{
    ustaw_nazwe( KWIAT, PL_MESKI_NOS_NZYW);

    dodaj_przym("czerwony", "czerwoni");
    dodaj_przym("delikatny", "delikatni");
   
    ustaw_nazwe_ziola(({"przewiercien", "przewiercnia", "przewiercieniowi", "przewiercien", 
                        "przewiercieniem", "przewiercieni" }), PL_MESKI_NOS_NZYW); 
    
    set_id_long("Jest to przewiercien, rzadki okaz kwiata wystepujacy w trzech roznych kolorach - "+
	"czerwonym, ktory jest najpospolitszy, zoltym oraz bialym.\n");

    set_unid_long("Jest to dziwna, bezwonna roslina o drobnym kwiecie koloru czerwonego.\n"); 
    
    set_ingest_verb(({"zjedz", "zjadasz", "zjada", "zjesc"}));
    
    set_id_diff(75); 
    set_find_diff(8);

    set_effect(HERB_SPECIAL, special_effect);
    set_amount(5+random(2));   
    set_decay_time(300);
    set_herb_value(487);
}

void
special_effect()
{
    write("Czujesz gorzki smak tego ziola. Zjedzenie go nie bylo najlepszym pomyslem...\n");
}
