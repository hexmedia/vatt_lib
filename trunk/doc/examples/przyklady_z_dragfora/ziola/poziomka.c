inherit "/std/herb";

#include "ziola.h"

create_herb()
{
    ustaw_nazwe(OWOC, PL_MESKI_NOS_NZYW);

    dodaj_przym("maly", "mali");
    dodaj_przym("czerwony", "czerwoni");

    ustaw_nazwe_ziola(({"poziomka", "poziomki", "poziomce", "poziomke","poziomka", "poziomce"}), PL_ZENSKI); 
    
    set_id_long("Jest to zwykla poziomka. Jest ona mala, smaczna.\n");

    set_unid_long("Maly bardzo delikatny czerwony owoc. Bije od niego przyjemny i slodki aromat.\n"); 
        
    set_decay_time(900);
    set_id_diff(5);
    set_find_diff(1);   
    set_amount(4);
    set_herb_value(5);
}