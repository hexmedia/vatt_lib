inherit "/std/herb";

#include "ziola.h"

create_herb()
{
    ustaw_nazwe(KORZEN, PL_MESKI_NOS_NZYW);

    dodaj_przym("pomaranczowy", "pomaranoczowi");
    dodaj_przym("nieduzy", "nieduzi");

    ustaw_nazwe_ziola(({"szalej jadowity", "szaleju jadowitego", "szalejowi jadowitemu", 
                        "szalej jadowity", "szalejem jadowitym", "szaleju jadowitym"}), PL_MESKI_NOS_NZYW); 
    
    set_id_long("Niezbyt duzy, pomaranczowy korzen. Bardzo przypomina zwykla marchewke, "+
	"jednak jest to trujacy szalej jadowity. Ma przyjemny zapach i "+
	"wystepuje czesto w poblizu wszelkich studni, w skutek czego jego trucizna czasem "+
	"dostaje sie do wody.");

    set_unid_long("Jest to tajemniczy korzen przypominajacy wygladem marchewke.\n"); 

    set_ingest_verb(({"zjedz", "zjadasz", "zjada", "zjesc"}));      
    set_effect(HERB_SPECIAL, special_effect); 
 
    set_decay_time(100);
    set_id_diff(25);
    set_find_diff(3);
    set_amount(6);
    set_herb_value(34);
}

void
special_effect()
{
    object trucizna;
    setuid();
    seteuid(getuid());
    trucizna = clone_object(EFEKT+"szalej_effect.c");
    trucizna->move(TP);
    trucizna->set_time(500);
    trucizna->set_interval(20);
    trucizna->start_poison();
}
