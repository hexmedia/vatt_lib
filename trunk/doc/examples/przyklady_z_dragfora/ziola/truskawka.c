inherit "/std/herb";

#include "ziola.h"

create_herb()
{
    ustaw_nazwe(OWOC, PL_MESKI_NOS_NZYW);

    dodaj_przym("nieduzy", "nieduzi");
    dodaj_przym("czerwony", "czerwoni");

    ustaw_nazwe_ziola(({"truskawka", "truskawki", "truskawce", "truskawke", "truskawka", "truskawce"}), PL_ZENSKI); 
    
    set_id_long("Masz przed soba najzwyklejsza w swiecie truskawke.\n");

    set_unid_long("Jest to nieduzy, czerwony owoc od ktorego bije przyjemny i slodki zapach.\n"); 
        
    set_decay_time(1900);
    set_id_diff(3);
    set_find_diff(1);   
    set_amount(16);
    set_herb_value(22);
}


