inherit "/std/herb";

#include "ziola.h"

create_herb()
{
    ustaw_nazwe(KWIATOSTAN, PL_MESKI_NOS_NZYW);

    dodaj_przym("bialy", "biali");
    dodaj_przym("drobny", "drobni");
   
    ustaw_nazwe_ziola(({"konwalia", "konwalii", "konwalii", "konwalie", "konwalia", "konwalii"}), PL_ZENSKI); 
    
    set_id_long("Jest to kwiatostan konwalii, ktory po zjedzeniu moze sie okazac trujacy.\n");

    set_unid_long("Masz przed soba bialy kwiatostan, bardzo drobny. Ma on swoj urok, ktory "+
	"juz od pierwszego spojrzenia cie zachwycil. Bije od kwiatu przyjemny aromat.\n"); 

    set_ingest_verb(({"zjedz", "jesz", "zjada", "zjesc"}));
    set_effect(HERB_SPECIAL, special_effect);

    set_id_diff(25);
    set_find_diff(4);
    set_decay_time(850);
    set_herb_value(300);
}

void
special_effect()
{
    object kwas;
    setuid();
    seteuid(getuid());
    kwas=clone_object(EFEKT+"konwalia_effect.c");
    kwas->move(TP);
    kwas->rozjeb_czache();
}
