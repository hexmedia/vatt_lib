inherit "/std/herb";

#include "ziola.h"

create_herb()
{
    ustaw_nazwe(LODYGA, PL_ZENSKI);

    dodaj_przym("dlugi", "dludzy");
    dodaj_przym("ciemnozielony", "ciemnozieloni");

    ustaw_nazwe_ziola(({"centuria", "centurii", "centurii", "centurie", "centurutia", "centurii"}), PL_ZENSKI); 
    
    set_id_long("Masz przed soba centuria, roslinke o nierozgalezionej, dosyc "+
        "dlugiej lodyzce. Przygladajac sie blizej zauwazasz czarne malutkie "+
        "kropeczki na koncu lodygi. Piekny zapach lodygi uderza do twoich nozdrzy.\n");

    set_unid_long("Jest to pieknie ciemno zielona, lodyga. Zapach ziola dociera do twojego "+
        "nosa. Na koncu tej dlugiej lodyzki zauwazasz czarne, malutenkie kropeczki.\n"); 

    set_decay_time(240);
    set_id_diff(41);
    set_find_diff(9);
    set_amount(1);
    set_herb_value(99);
}
