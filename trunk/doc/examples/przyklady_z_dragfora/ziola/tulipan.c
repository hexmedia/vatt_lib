inherit "/std/herb";

#include "ziola.h"

create_herb()
{
    ustaw_nazwe(KWIAT, PL_MESKI_NOS_NZYW);
  
    dodaj_przym("zolty", "zolci");
    dodaj_przym("pachnacy", "pachnacy");

    ustaw_nazwe_ziola(({"tulipan", "tulipanu", "tulipanowi", "tulipan", "tulipanem", "tulipanie"}), PL_MESKI_NOS_NZYW); 

    set_id_long("Jest to najczesciej oprocz rozy ogrodowej hodowany kwiat we wszelkiego "+
	"rodzaju ogrodach - tulipan. Jego zolciutki kwiat i specyficzny zapach na dlugo "+
	"zapadaja w pamieci.\n");

    set_unid_long("Jest to zolciutki kwiat o specyficznym, bardzo lekkim i niklym zapachu.\n"); 

    set_decay_time(202);
    set_id_diff(25);
    set_find_diff(2);
    set_amount(1);
    set_herb_value(49);
}
