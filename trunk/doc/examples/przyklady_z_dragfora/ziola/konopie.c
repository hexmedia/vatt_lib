inherit "/std/herb";

#include "ziola.h"

create_herb()
{
    ustaw_nazwe(LODYGA, PL_ZENSKI);
  
    dodaj_przym("dlugi", "dludzy");
    dodaj_przym("sztywny", "sztywni");

    ustaw_nazwe_ziola(({"konopie","konopii", "konopiom", "konopie", "konopiami", "konopiach"}), PL_ZENSKI); 
    
    set_id_long("Masz przed soba konopie. Najstarsi znachorowie powiadaja, iz spalenie jej daje roznego rodzaju "+
        "odpornosci magiczne... Ile w tym prawdy? Nie wiadomo.\n");

    set_unid_long("Dziwna, sztywna lodyga. Nie masz ziolonego pojecia coz to moze byc.\n"); 

    set_ingest_verb(({"spal", "spalasz", "spala", "spalic"}));
    set_effect(HERB_SPECIAL, special_effect);

    set_decay_time(900);
    set_id_diff(16);
    set_find_diff(4);
    set_amount(6);
    set_herb_value(29);
}

void
special_effect()
{
    object kwas;
    setuid();
    seteuid(getuid());
    kwas=clone_object(EFEKT+"konopie_effect.c");
    kwas->move(TP);
    kwas->rozjeb_czache();
}