inherit "/std/herb";

#include "ziola.h"

create_herb()
{
    ustaw_nazwe(KWIAT, PL_MESKI_NOS_NZYW);
  
    dodaj_przym("drobniutki", "drobniutcy");
    dodaj_przym("bialy", "biali");

    ustaw_nazwe_ziola(({"stokrotka", "stokrotki", "stokrotce", "stokrotke", "stokrotka", "stokrotce"}), PL_ZENSKI); 
    
    set_id_long("Jest to biala stokrotka. Czasami jej platki sa rozowe, lecz zdarza "+
	"sie to duzo rzadziej niz gdy sa biale. Kwiat nie ma prawie zapachu.\n");

    set_unid_long("Masz przed soba bialy kwiat, ktory nie ma zapachu. Jest tak maly i drobny, "+
	"ze nie wiekszy od twojego palca.\n");

    set_decay_time(500);
    set_id_diff(4);
    set_find_diff(1);   
    set_amount(1);
    set_herb_value(15);
}
