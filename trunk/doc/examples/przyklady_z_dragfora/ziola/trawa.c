inherit "/std/herb";

#include "ziola.h"

create_herb()
{
    ustaw_nazwe(LISC, PL_MESKI_NOS_NZYW);

    dodaj_przym("dlugi", "dludzy");
    dodaj_przym("zielony", "zieloni");

    ustaw_nazwe_ziola(({"trawa", "trawy", "trawie", "trawe", "trawa", "trawie"}), PL_ZENSKI); 

    set_id_long("Masz przed soba zwykla trawe, ktora praktycznie nie nada ci sie do niczego.\n");

    set_unid_long("Dlugi, zielony lisc, ktory dziwnie ci cos przypomina...\n"); 
    
    set_decay_time(1500);
    set_id_diff(4);
    set_find_diff(1);
    set_amount(1);
    set_herb_value(2);
}
