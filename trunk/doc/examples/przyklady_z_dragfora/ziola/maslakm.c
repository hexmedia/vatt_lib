inherit "/std/herb";

#include "ziola.h"

create_herb()
{
    ustaw_nazwe(GRZYB, PL_MESKI_OS);

    dodaj_przym("wilgotny", "wilgotni");
    dodaj_przym("maly", "mali");

    ustaw_nazwe_ziola(({"maslak modrzewiowy", "maslaka modrzewiowego", "maslakiem modrzewiowym", 
                        "maslaka modrzewiowego", "maslakiem modrzewiowym", "maslaku modrzewiowym"}), PL_MESKI_NOS_NZYW); 
    
    set_id_long("Masz przed soba typowego maslaka modrzewiowego. Konce kapelusza polaczone sa zoltawa "+
	"powloczka z trzonem, rowniez zoltym. Kolor kapelusza jest jasnobrazowy, od grzyba bije "+
	"dosc przyjemny zapach. Mozna go spotkac w poblizu modrzewi.\n");

    set_unid_long("Jest to grzyb od ktorego bije calkiem przyjemny zapach. Jego kapelusz polaczony "+
	"jest zolta mazia z trzonem o tym samym kolorze. Jasnobrazowa sliska glowka jest mala "+
	"i dziwisz sie, ze udalo ci sie go znalezc.\n"); 
        
    set_decay_time(69);
    set_id_diff(54);
    set_find_diff(8);   
    set_amount(19);
    set_herb_value(72);
}