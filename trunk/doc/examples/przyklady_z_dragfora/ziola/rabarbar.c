inherit "/std/herb";

#include "ziola.h"

create_herb()
{
    ustaw_nazwe(LISC, PL_MESKI_NOS_NZYW);
  
    dodaj_przym("czerwonawy", "czerwonawi");
    dodaj_przym("dlugi", "dludzy");
   
    ustaw_nazwe_ziola(({"rabarbar", "rabarbaru", "rabarbarowi", "rabarbar", "rabarbarem", "rabarbarze"}), PL_MESKI_NOS_NZYW); 
    
    set_id_long("Pierwszym spojrzeniem rozpoznajesz, iz jest to rabarbar.\n");

    set_unid_long("Jakis dziwny, gruby, miazsisty lisc.\n"); 
        
    set_id_diff(15);   
    set_find_diff(1);    
    set_decay_time(1000);   
    set_herb_value(10);
}
