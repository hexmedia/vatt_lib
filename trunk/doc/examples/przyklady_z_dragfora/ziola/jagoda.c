inherit "/std/herb";

#include "ziola.h"

create_herb()
{
    ustaw_nazwe(OWOC, PL_MESKI_NOS_NZYW);

    dodaj_przym("maly", "mali");
    dodaj_przym("fioletowy", "fioletowi");

    ustaw_nazwe_ziola(({"jagoda", "jagody", "jagodzie", "jagode", "jagoda", "jagodzie"}), PL_ZENSKI); 
    
    set_id_long("Jest to najzwyklejsza jagoda. Spotkac mozna je w lasach.\n");

    set_unid_long("Masz przed soba maly fioletowy owoc. Jego zapach nie jest mocny, "+
	"ale jest za to przyjemny i delikatny.\n"); 
        
    set_decay_time(800);
    set_id_diff(6);
    set_find_diff(1);   
    set_amount(2);
    set_herb_value(8);
}