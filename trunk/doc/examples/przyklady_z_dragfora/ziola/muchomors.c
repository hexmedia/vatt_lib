inherit "/std/herb";

#include "ziola.h"

create_herb()
{
    ustaw_nazwe(GRZYB, PL_MESKI_OS);

    dodaj_przym("zielonawy", "zielonawi");
    dodaj_przym("blaszkowaty", "blaszkowaci");

    ustaw_nazwe_ziola(({"muchomor sromotnikowy", "muchomora sromotnikowego", "muchomorowi sromotnikowemu", 
                        "muchomor sromotnikowy", "muchomorem sromotnikowym", "muchomorze sromotnikowym" }), PL_MESKI_NOS_NZYW); 
    
    set_id_long("Jest to najgrozniejszy z wszystkich muchomorow - muchomor sromotnikowy. "+
	"Jego silna trucizna potrafi zabic. Jest zielonkawy i blaszkowaty. Ma pierscien na trzonie.\n");

    set_unid_long("Masz przed soba blaszkowatego, lekko zielonawego grzyba z pierscieniem "+
	"na trzonie.\n");

    set_ingest_verb(({"zjedz", "zjadasz", "zjada", "zjesc"}));      
    set_effect(HERB_SPECIAL, special_effect);     
  
    set_decay_time(127);
    set_id_diff(55);
    set_find_diff(3);   
    set_amount(12);
    set_herb_value(93);
}
void
special_effect()
{
    object czer;
    setuid();
    seteuid(getuid());
    czer=clone_object(EFEKT+"muchomors_effect.c");
    czer->move(TP);
    czer->rozjeb_czache();
}