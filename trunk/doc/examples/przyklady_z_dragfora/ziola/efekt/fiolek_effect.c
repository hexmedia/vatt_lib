inherit "/std/object";

#include <macros.h>
#include <pl.h>
#include <stdproperties.h>

#define TO this_object()
#define TP this_player()
int jak_widzial = 0;

void
free( object kogo )
{
TP->remove_stun();
TP->add_prop(LIVE_I_SEE_DARK, jak_widzial);
TP->catch_msg("Twoj wzrok wraca do poprzedniego stanu.\n");
TP->set_skill(23, TP->query_skill(23)+20);
TP->set_skill(24, TP->query_skill(24)+20);
TP->catch_msg("Twoje umiejetnosci rosna...\n");
      TO->remove_object();     
}
void
uboczne( object kogo )
{
TP->set_skill(23, TP->query_skill(23)-20);
TP->set_skill(24, TP->query_skill(24)-20);
TP->catch_msg("Czujesz sie mniej przydatny do walki...\n");
}


void
start(int f)
{
        TP->add_stun();
	if (TP->query_prop(LIVE_I_SEE_DARK) > 0)
	jak_widzial = TP->query_prop(LIVE_I_SEE_DARK);
        TP->add_prop(LIVE_I_SEE_DARK,100);
        TP->catch_msg("Twoj zmysl wzroku wyostrza sie.\n");
        set_alarm(40.0,0.0,"uboczne",TP);
        set_alarm(300.0,0.0,"free",TP);

}

void
create_object()
{
    add_prop(OBJ_M_NO_DROP,"");
    add_prop(OBJ_M_NO_GIVE,"");
    add_prop(OBJ_M_NO_GET,"");
    set_no_show();
}

void
rozjeb_czache()
{
    set_alarm(10.5,0.0,"start",0);
}
