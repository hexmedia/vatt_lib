inherit "/std/poison_effect.c";
#include <macros.h>
#include <pl.h>
#include <poison_types.h>

int dzialanie = 0;

create_poison_effect()
{
    set_damage( ({ POISON_HP, 60, POISON_USER_DEF, 30 }) );
    set_strength(15);
    set_poison_type("standard");
}

int
special_damage(int *damage, int i)
{
    poisonee->add_panic(random(20) + 5);
    poisonee->set_hp(poisonee->query_hp()-5-random(10));
    if(!dzialanie)
    {
        tell_object(poisonee, "W jednej chwili dostajesz goraczki, a swiat przed "
	+"twoimi oczami zaczyna sie zamazywac. Po chwili wszystko wyostrza sie, "
	+"jednak wszystko wydaje sie czarnobiale. Zaczynasz sie okropnie pocic "
	+"i trzasc..\n");
            
        dzialanie = 1;
    }

    // Zwierze nie moze tak reagowac, jak slusznie zauwazyl Samobor, ktory 
    // walczyl tym sztyletem z wezem.
    
    if ((!function_exists("create_zwierze",poisonee))&&(!function_exists("create_potwor",poisonee)))
       {
          switch(random(4))
              {
                 case 0:
                    poisonee->catch_msg("Nagle tuz przed toba staje jakas potezna osoba "
	+"mowiac w dziwnym jezyku. Po chwili zaczynasz z nia rozmawiac.\n");
                    tell_room(environment(poisonee), QCIMIE(poisonee, PL_MIA) +
                       " patrzac przed siebie mamrocze cos w bardzo dziwnym jezyku...\n",
                         poisonee);
                    break;
                 case 1:
                    {
                       poisonee->catch_msg("Zaczynasz sie trzasc...\n");
                       tell_room(environment(poisonee), QCIMIE(poisonee, PL_MIA) +
                         " dostaje naglych atakow padaczki.\n",
                         poisonee);
                       poisonee->set_hp(poisonee->query_max_hp()-15-random(2));
                    }
                    break;
                 case 2:
                    {
                        poisonee->catch_msg("Otacza cie mnostwo wezy. Zaczynasz uciekac "
	                 +"w poszukiwaniu bezpiecznego miejsca..\n");
                        tell_room(environment(poisonee), QCIMIE(poisonee, PL_MIA) +
                           " rozgladajac sie dookola zaczyna biec przed siebie nie "
	                 +"patrzac pod nogi....\n", poisonee);
                           poisonee->command("e");
                           poisonee->command("w");
                           poisonee->command("s");
                           poisonee->command("sw");
                           poisonee->command("nw");
                           poisonee->command("s");
                           poisonee->command("e");
                           poisonee->command("w");
                           poisonee->command("w");
                           poisonee->command("s");
                           poisonee->command("sw");
                           poisonee->command("n");
                           poisonee->command("ne");
                           poisonee->command("sw");
                           poisonee->command("nw");
                           poisonee->command("s");
                           poisonee->command("w");

                     }
                     break;
                  case 3:
                     {
                         poisonee->catch_msg("Twoje rece zaczynaja kamieniec...\n");
                        tell_room(environment(poisonee), QCIMIE(poisonee, PL_MIA) +
                           " z przerazeniem spoglada na swoje rece krzyczac "
                           +"przy tym niesamowicie.\n", poisonee);
                     }
                     break;
              }
       }
     return i+2;
}