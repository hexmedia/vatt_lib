inherit "/std/object";

#include <macros.h>
#include <pl.h>
#include <stdproperties.h>

#define TO this_object()
#define TP this_player()

void
dupa( object kogo )
{
    TP->remove_prop(MAGIC_I_RES_FIRE,-5);
    TP->remove_prop(MAGIC_I_RES_AIR,-5);
    TP->remove_prop(MAGIC_I_RES_EARTH,-5);
    TP->remove_prop(MAGIC_I_RES_WATER,-5);
    TP->remove_prop(MAGIC_I_RES_LIFE,-5);
    TP->remove_prop(MAGIC_I_RES_ACID,-5);
    TP->remove_prop(MAGIC_I_RES_DEATH,-5);
    TP->catch_msg("Twe odpornosci magiczne opuszczaja cie...\n");
}


void
start(int f)
{
    TP->add_prop(MAGIC_I_RES_FIRE,TP->add_prop(MAGIC_I_RES_FIRE)+5);
    TP->add_prop(MAGIC_I_RES_AIR,TP->add_prop(MAGIC_I_RES_AIR)+5);
    TP->add_prop(MAGIC_I_RES_EARTH,TP->add_prop(MAGIC_I_RES_EARTH)+5);
    TP->add_prop(MAGIC_I_RES_WATER,TP->add_prop(MAGIC_I_RES_WATER)+5);
    TP->add_prop(MAGIC_I_RES_LIFE,TP->add_prop(MAGIC_I_RES_LIFE)+5);
    TP->add_prop(MAGIC_I_RES_ACID,TP->add_prop(MAGIC_I_RES_ACID)+5);
    TP->add_prop(MAGIC_I_RES_DEATH,TP->add_prop(MAGIC_I_RES_DEATH)+5);
    TP->catch_msg("Czujesz jak jakas magiczna moc nawiedza twe cialo...\n");
    set_alarm(360,0.0,"dupa",0);

}

void
create_object()
{
    add_prop(OBJ_M_NO_DROP,"");
    add_prop(OBJ_M_NO_GIVE,"");
    add_prop(OBJ_M_NO_GET,"");
    set_no_show();
}

void
rozjeb_czache()
{
    set_alarm(10.5,0.0,"start",0);
}
