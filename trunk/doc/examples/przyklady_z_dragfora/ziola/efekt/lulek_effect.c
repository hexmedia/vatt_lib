inherit "/std/object";

#include <macros.h>
#include <pl.h>
#include <stdproperties.h>

#define TO this_object()
#define TP this_player()

int no_way(string what)
{

 write("Niestety, w twojej krwi znajduje sie trucizna nie pozwalajaca zakonczyc gre.\n");
 return 1;
}
void init()
{
   ::init();
   add_action(no_way, "zakoncz");


}
void
po( object kogo )
{
TP->catch_msg("Zaczyna bolec cie zoladek.\n");
TP->set_hp(TP->query_hp()-3);
}
void
za( object kogo )
{

TP->catch_msg("Czujesz sie coraz gorzej.\n");
TP->set_hp(TP->query_hp()-8);
}
void
zza( object kogo )
{

TP->catch_msg("Boli cie glowa.\n");
TP->set_hp(TP->query_hp()-23);
}
void
koniec( object kogo )
{
if(TP->query_hp() > 40)
{
TP->catch_msg("Czujesz sie znacznie lepiej.\n");
TP->set_hp(TP->query_hp()+7);
        TO->remove_object();  
}
else
{
TP->catch_msg("Nagle zdajesz sobie sprawe z tego, ze caly twoj organizm "
	+"zaczyna zamierac. Mozg oraz serce przestaja pracowac. Umierasz...\n");
        this_player()->heal_hp(-(this_player()->query_max_hp()));
        this_player()->do_die();
        TO->remove_object();    
}
}


void
start(int f)
{
        TP->catch_msg("Czujesz sie gorzej. Moze trucizna dostala sie do twego organizmu ?\n");
        set_alarm(40.0,0.0,"po",TP);
        set_alarm(300.0,0.0,"za",TP);
        set_alarm(800.0,0.0,"zza",TP);
        set_alarm(1000.0,0.0,"koniec",TP);
}

void
create_object()
{
    add_prop(OBJ_M_NO_DROP,"");
    add_prop(OBJ_M_NO_GIVE,"");
    add_prop(OBJ_M_NO_GET,"");
    set_no_show();
}
void
rozjeb_czache()
{
    set_alarm(10.5,0.0,"start",0);
}

