inherit "/std/object";

#include <macros.h>
#include <pl.h>
#include <stdproperties.h>

#define TO this_object()
#define TP this_player()
int jak_widzial = 0;

void
dupa( object kogo )
{
    TP->modify_stat(3, TP->query_stat(3)-15);
    TP->catch_msg("Czujesz sie mniej wytrzymal"+TP->koncowka("y", "a")+".\n");
    TO->remove_object();     
}


void
start(int f)
{
    TP->modify_stat(3, TP->query_stat(3)+15);
    TP->catch_msg("Czujesz sie bardziej wytrzymal"+TP->koncowka("y", "a")+".\n");

    set_alarm(600,0.0,"dupa",0);
}

void
create_object()
{
    add_prop(OBJ_M_NO_DROP,"");
    add_prop(OBJ_M_NO_GIVE,"");
    add_prop(OBJ_M_NO_GET,"");
    set_no_show();
}

void
rozjeb_czache()
{
    set_alarm(10.5,0.0,"start",0);
}
