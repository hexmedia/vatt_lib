inherit "/std/object";

#include <macros.h>
#include <pl.h>
#include <stdproperties.h>

#define TO this_object()
#define TP this_player()
int jak_widzial = 0;

void
free( object kogo )
{
TP->remove_stun();
TP->add_prop(LIVE_I_SEE_DARK, jak_widzial);
TP->catch_msg("Zaczynasz widziec!\n");
}
void
rmhp( object kogo )
{
TP->set_hp(TP->query_hp()-15+random(10));
TP->catch_msg("Czujesz sie coraz gorzej...\n");
}


void
start(int f)
{
        TP->add_stun();
	if (TP->query_prop(LIVE_I_SEE_DARK) > 0)
	jak_widzial = TP->query_prop(LIVE_I_SEE_DARK);
        TP->add_prop(LIVE_I_SEE_DARK,-100);
        TP->catch_msg("Tracisz wzrok!\n");
        set_alarm(20.0,0.0,"rmhp",TP);
        set_alarm(60.0,0.0,"free",TP);

}

void
create_object()
{
    add_prop(OBJ_M_NO_DROP,"");
    add_prop(OBJ_M_NO_GIVE,"");
    add_prop(OBJ_M_NO_GET,"");
    set_no_show();
}

void
rozjeb_czache()
{
    set_alarm(10.5,0.0,"start",0);
}
