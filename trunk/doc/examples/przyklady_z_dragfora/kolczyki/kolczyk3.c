#include <pl.h>
#include "/sys/stdproperties.h"
inherit "/d/Faerun/std/kolczyk_std.c";

void
create_kolczyk()
{
set_long("Kolczyk ten sklada sie ze srebrnej, delikatnie "+
	"inkrustowanej zapinki i czerwonego kamienia, blyszczacego "+
	"przy kazdym poruszeniu, jakby zawieral w sobie spory dodatek "+
	"miki. Kamien jest gladko szlifowany i rzniety w lekko nieforemny "+
	"ksztalt ktory jeszcze dodaje mu uroku. Przygladasz sie blizej, "+
	"przystawiajac blyskotke prawie do oka i odnosisz wrazenie ze "+
	"cos w tym kamieniu zyje... wlasnym, wewnetrzym zyciem.\n");
dodaj_przym("czerwony","czerwoni");
dodaj_przym("blyszczacy","blyszczacy");

    add_prop(OBJ_I_VALUE, 200);
    add_prop(OBJ_I_VOLUME, 17);
    add_prop(OBJ_I_WEIGHT, 13);

}
