#include <pl.h>
#include "/sys/stdproperties.h"
inherit "/d/Faerun/std/kolczyk_std.c";

void
create_kolczyk()
{
set_long("Jest to jeden z najprostrzych wzorow kolczyka jakie "+
	"kiedykolwiek wymyslono. W zasadzie sklada sie tylko z "+
	"kawalka stalowego drutu zgietego w ksztalt kola.\n");
dodaj_przym("prosty","prosci");
dodaj_przym("stalowy","stalowi");

    add_prop(OBJ_I_VALUE, 65);
    add_prop(OBJ_I_VOLUME, 10);
    add_prop(OBJ_I_WEIGHT, 5);

}
