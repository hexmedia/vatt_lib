#include <pl.h>
#include "/sys/stdproperties.h"
inherit "/d/Faerun/std/kolczyk_std.c";

void
create_kolczyk()
{
set_long("Jest to wykonany ze stali kolczyk, ktorego glowna "+
	"czescia jest gustownie wyzlobiona w metalu spirala. Ciasno "+
	"przylegajace do siebie zelazne kregi maja jakis "+
	"swoisty urok...\n");
dodaj_przym("spiralny","spiralni");
dodaj_przym("stalowy","stalowi");

    add_prop(OBJ_I_VALUE, 230);
    add_prop(OBJ_I_VOLUME, 27);
    add_prop(OBJ_I_WEIGHT, 25);

}
