#include <pl.h>
#include "/sys/stdproperties.h"
inherit "/d/Faerun/std/kolczyk_std.c";

void
create_kolczyk()
{
set_long("Pieknie wymodelowany, zloty kolczyk, pokryty "+
	"wytrawionymi ornamentami o motywie roslinnym. Niewatpliwie "+
	"jest bardzo ozdobny, a jego wykonanie musialo pochlonac niemalo srodkow.\n");
dodaj_przym("zloty","zloci");
dodaj_przym("zrobiony","zobieni");

    add_prop(OBJ_I_VALUE, 200);
    add_prop(OBJ_I_VOLUME, 17);
    add_prop(OBJ_I_WEIGHT, 13);

}
