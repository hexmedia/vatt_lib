inherit "/std/object";

#include <pl.h>
#include <macros.h>
#include <stdproperties.h>

int czytanie();

create_object()
{

    ustaw_nazwe(        ({"tabliczka","tabliczki","tabliczce","tabliczke","tabliczka","tabliczce"}),
        ({"tabliczki","tabliczek","tabliczkom","tabliczki","tabliczkami","tabliczkach"}),
        PL_ZENSKI);

    dodaj_przym("drewniany","drewniani");

    set_long("Jest to nieduza, drewniana tabliczka. Moze wartalo by zapoznac sie z jej trescia?\n");

    add_cmd_item( ({"tabliczke","drewniana tabliczke"}), "przeczytaj", czytanie);

    add_prop(OBJ_I_NO_GET, 1);

}

int
czytanie()
{
    
    write("+------------------------------+\n"
        + "| ===== D Y L I Z A N S =====  |\n"
	+ "|                              |\n"
	+ "|  Tu zatrzymuje sie dylizans  |\n"
	+ "|      jezdzacy na trasie      |\n"
	+ "|      Beregost - Crimmor      |\n"
	+ "|                              |\n"
	+ "|  Cena przejazdu: 3 zlocisze. |\n"
	+ "|                              |\n"
	+ "+------------------------------+\n");

    say(this_interactive()->query_Imie(this_interactive, PL_MIA) + " czyta drewniana tabliczke.\n");

    return 1;

}

