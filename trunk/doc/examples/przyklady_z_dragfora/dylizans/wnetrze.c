inherit "/d/Faerun/std/dylizanse/dyl_room.c";
inherit "/lib/siedzenie";

#include <stdproperties.h>
#include <pl.h>
#include <macros.h>

create_room()
{

    set_short("Wnetrze czarnego dylizansu");
    
    set_long("Znajdujesz sie w dosyc ciasnym wnetrzu czarnego dylizansu. "
           + "Dostrzegasz tutaj miekkie siedzisko dla podroznych, a takze "
           + "malutkie okienka przesloniete prostymi, szkarlatnymi zaslonami. "
           + "Sadzisz jednak, ze jazda tym srodkiem transportu jest bardzo "
           + "wygodna i nie wymaga od wedrowcow szczegolnie duzej utraty majatku.\n");
           
   set_event_time(30.0,30.0);
	set_events_on(1);
	add_event("Dylizans podskoczyl na wyboju.\n");
	add_event("Slyszysz glos woznicy popedzajacego konie.\n");
	add_event("Dochodzi cie swist konskiego bata.\n");
    
    add_prop(ROOM_I_INSIDE, 1);
    
    add_item("siedzisko", "Jest to typowe siedzisko dla podroznych, ktorzy "
           + "oplacili jazde dylizansem. W kazdej chwili mozesz nan usiasc "
           + "dajac odpoczac nogom.\n");
           
    add_item( ({"okienka","malutkie okienka"}), "Sa to przeszklone, malutkie "
           + "okienka, ktore chociaz zaslonieto zaslonami, to jednak da sie "
           + "przez nie wyjrzec z dylizansu.\n");
           
    add_item( ({"okienko","malutkie okienko"}), "Jest to nieduze okienko calkowicie "
           + "przesloniete przez szkarlatna zaslone.\n");
           
    add_item( ({"zaslony","szkarlatne zaslony"}), "Sa to krotkie zaslonki sluzace "
           + "do zaslaniania okien w czasie jazdy.\n");
           
    add_item( ({"zaslone","szkarlatna zaslone"}), "Jest to krotka zaslona, ktora "
           + "calkowicie przeslania jedno z malutkich okienek dylizansu.\n");
           
    set_sit("na siedzisku","na siedzisku","z siedziska",4);
           
}

leave_inv(object ob, object from)
{

    leave_inv_sit(ob);
    ::leave_inv(ob, from);
    
}

init()
{

    ::init();
    init_sit();
    
}
    


