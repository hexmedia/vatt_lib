#pragma strict_types
inherit "/d/Dragon/std/room";
#include <mudtime.h>
public void create_pal_room()
{
	set_short("Gdzies w Palanthas");
	set_long("Gdzies w miescie Palanthas.\n");
	
	set_event_time(30.0,this_object());
	add_event("Slyszysz krzyki jakiejs przekupki zachwalajacej "
	+"swoj towar.\n");
	add_event("Od strony wiezienia dobiegla cie najbardziej "
	+"pomyslowa wiazanka, jaka tylko mozna sobie wyobrazic.\n");
	add_event("Gdzies w oddali slyszysz dzwieki lutni. To pewnie "
	+"jakis bard probuje zarobic na zycie spiewaniem.\n");
	add_event("Z drugiego konca placu dobiegaja cie odglosy "
	+"przepychanki. Najpewniej ukradziono komus sakiewke.\n");
	
}
public void create_room()
{
	setuid();
        seteuid(getuid());
	create_pal_room();
}

public void reset_pal_room()
{
}

public void reset_room()
{

	reset_pal_room();	
}

string opis_pory_roku()
{
    string opis = "";


    if (pora_roku2() == "zima")
    {
        opis += "Wszystko dokola przykryte jest spora warstwa bialego sniegu,";

        switch(MT_PORA_DNIA_STRB(pora_dnia()))
        {
            case "swit": opis += " ktory lekko rozowi sie pod wplywem slonca "
            +"leniwie wynurzajacego sie zza horyzontu."; break;
                    
            case "wczesny ranek": opis += " skrzacego sie w promieniach " +
                    "wschodzacego slonca."; break;

	    case "ranek": opis += " rozblyskujacego jasno w swietle " +
	            "porannego slonca."; break;
	            
            case "poludnie": opis += " blyszczacego przepieknie w promieniach " +
                    "poludniowego slonca."; break;
                    
            case "popoludnie": opis += " blyszczacego przepieknie w promieniach slonca, " +
                    "ktore powoli przetacza sie na zachodnia czesc niebosklonu."; break;
                    
            case "wieczor": opis += " po ktorym snuja sie dlugie cienie, " +
                    "rozswietlane gdzieniegdzie slabym blaskiem " +
                    "zachodzacego slonca."; break;
                    
            case "pozny wieczor": opis += " rozjasnionego leniwie gasnaca na " +
                    "zachodzie luna."; break;
                    
            case "noc": opis += " skapanego w srebrzystej poswiacie " +
                    "zimowego nieba.";
        }
    }
    else

    if (pora_roku2() == "wiosna")
    {
        if (MT_PORA_DNIA_STRB(pora_dnia()) != "noc") 
            opis += "Wszystko dookola ciebie wydaje sie budzic z zimowego " +
                "letargu i stara sie w szalonym tempie nadrobic stracony czas.";
	switch (MT_PORA_DNIA_STRB(pora_dnia()))
	{
	    case "swit": opis += " Trawa pokryta jest bialym puchem szronu. "+
	        "Na wschodzie, nad horyzontem widac blask powoli " +
	        "wschodzacego slonca."; break;
	        
	    case "wczesny ranek": opis += " Czasami dostrzegasz na trawie " +
	        "slady szronu znikajacego szybko w promieniach wschodzacego " +
	        "slonca."; break;
	        
	    case "ranek": opis += " Czasem, gdy spojrzysz na trawe dostrzegasz " +
	        "krople rosy poblyskujace w porannym sloncu."; break;

	    case "poludnie": opis += " Pomimo, iz powiewa chlodnawy wiaterek "+
	        "jest dosyc cieplo, szczegolnie tam, gdzie padaja promienie "+
	        "wysoko swiecacego slonca."; break;
	        
	    case "popoludnie": opis += " Pomimo, iz powiewa chlodnawy " +
	        "wiaterek jest dosyc cieplo szczegolnie tam, gdzie " +
	        "padaja promienie chylacego sie ku zachodowi slonca."; break;
	         
	    case "wieczor": opis += " Slonce powoli zaczyna sie chowac " +
	        "poza horyzont, spowijajac wszystko w swym czerwonym blasku.";
	        break;
	        
	    case "pozny wieczor": opis += " Na zachodzie powoli gasnie " +
	        "luna odchodzacego dnia."; break;
	        
	    case "noc": opis += " Panuje wlasnie dosyc chlodna wiosenna noc. " +
	        "Robi sie powoli coraz zimniej, nad ranem moze wystapic " +
	        "nawet przymrozek.";
	}
    }
    else

    if (pora_roku2() == "lato")
    {
        switch(MT_PORA_DNIA_STRB(pora_dnia()))
        {
	    case "swit": opis += "Na wschodzie niebo stopniowo sie " +
	        "rozjasnia. Niedlugo slonce znow wychyli sie zza horyzontu " +
	        "i na ziemie poleja sie strumienie goraca. Tymczasem jednak " +
	        "panuje mily chlod."; break;
	        
	    case "wczesny ranek": opis += "Na razie nie jest jeszcze " +
	        "zbyt goraco, jednak plomienna tarcza slonca wznoszaca sie " +
	        "tuz ponad horyzont zapowiada kolejny upalny dzien lata.";
	        break;
	        
	    case "ranek": opis += "Zaczyna sie robic coraz cieplej. " +
	        "Slonce powoli, lecz nieustannie zmierza w kierunku zenitu, "+
	        "niedlugo upal stanie sie nie do zniesienia."; break;
	        
	    case "poludnie": opis += "Pod palacymi promieniami slonca cala " +
	        "przyroda wydaje sie uginac i malec. Wszystko, co znajduje " +
	        "sie w oddali zdaje sie falowac pod wplywem goraca."; break;
	         
	    case "popoludnie": opis += "Pora najwiekszych upalow juz minela, "+
	        "jest jednak jeszcze bardzo goraco. Slonce przekroczylo juz "+
	        "zenit i powoli zaczyna zmierzac ku zachodowi."; break;
	        
	    case "wieczor": opis += "Slonce powoli zaczyna znikac za " +
	        "horyzontem, konczac kolejny upalny dzien lata. Niedlugo " +
	        "nastapi noc, kojaca skutki dnia swoim chlodem."; break;
	        
	    case "pozny wieczor": opis += "Slonce zniknelo za horyzontem, " +
	        "pozostawiajac jedynie lekki poblask na niebie. Ziemia " +
	        "powoli zaczyna stygnac, zas w powietrzu unosi sie " +
	        "przyjemny chlod."; break;
	        
	    case "noc": opis += "Panuje wlasnie ciepla letnia noc. Cala " +
	        "okolica wydaje sie odpoczywac po upalnym dniu w kojacym " +
	        "chlodzie nocy.";
	}
    }
    else

    if (pora_roku2() == "jesien")
    {
        switch(MT_PORA_DNIA_STRB(pora_dnia()))
        {
	    case "swit": opis += "Ciemnosci nocy powoli umykaja przed " +
	        "szarzyzna switu. Wciaz jest chlodno, lecz kiedy slonce " +
	        "wstanie, powinno sie nieco ocieplic. Powoli zaczyna " +
	        "podnosic sie delikatna mgla."; break;
	        
	    case "wczesny ranek": opis += "Jasna sloneczna tarcza wynurzyla " +
	        "sie zza horyzontu. Mrok nocy prysl, a pozostaly jedynie " +
	        "cienie rzucane przez dosyc czesto przeslaniajace slonce " +
	        "chmury, gnane jesiennym wiatrem. Chlod poranka dosyc mocno " +
	        "daje sie we znaki."; break;
	        
	    case "ranek": opis += "Promienie podnoszacego sie nad " +
	        "horyzontem slonca od czasu do czasu przedostaja sie " +
	        "pomiedzy sunacymi po niebie chmurami. Gwaltowne porywy " +
	        "jesiennego, chlodnego wiatru unosza w gore suche liscie " +
	        "i tumany kurzu."; break;
	        
	    case "poludnie": opis += "Slonce, pomimo iz swieci jasno, nie " +
	        "grzeje juz tak mocno jak latem. Jesienny wiatr co jakis " +
	        "czas unosi w gore pozolkle liscie i zdzbla traw. Po " +
	        "niebie przeciagaja nisko lecace chmury, zwiastujace " +
	        "jesienne deszcze.";  break;
	        
	    case "popoludnie": opis += "Jasna tarcza slonca leniwie " +
	        "przemierza niebosklon. Cienie powoli sie wydluzaja. Gnane " +
	        "jesiennym wiatrem chmury zwiastuja deszcze."; break;
	        
	    case "wieczor": opis += "Czerwone slonce powoli chowa sie za " +
	        "horyzontem, ostatnimi promieniami rozjasniajac gruba " +
	        "powloke chmur. Czerwona luna powoli gasnie ustepujac " +
	        "ciemnosciom nocy. Porywisty wiatr niesie ze soba " +
	        "przenikliwy chlod."; break;
	        
	    case "pozny wieczor": opis += "Na niebie stopniowo pojawiaja " +
	        "sie kolejne gwiazdy, ktorych blask co chwile " +
	        "przeslaniany jest przez nisko lecace chmury. " +
	        "Chlodny, porywisty wiatr z polnocy przenika do szpiku " +
	        "kosci wzbudzajac drzenie."; break;
	        
	    case "noc": opis += "Panuje wlasnie noc, rozswietlana miejscami " +
	        "przez gwiazdy, ktorym udaje sie przebic przez rzadkie " +
	        "chmury plynace po niebie. Jest dosyc chlodno, nie do konca " +
	        "z powodu temperatury, lecz raczej zimnego, przejmujacego " +
	        "wiatru z polnocy. Powietrze przesycone jest wilgocia " +
	        "osadzajaca sie na wszystkim w okolicy."; break;
	}
    }

return opis;
}
