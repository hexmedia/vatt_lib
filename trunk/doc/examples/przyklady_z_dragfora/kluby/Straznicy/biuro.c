inherit "/d/Utopia/std/room";

#include "straznicy.h"
#include <cmdparse.h>

void
create_imperium_room()
{

        add_prop (ROOM_I_INSIDE, 1); 
        add_prop (ROOM_I_LIGHT, 1);

        set_short( "Siedziba Straznikow Solamnii");

        set_long( "Znajdujesz sie w niewielkim pokoju sluzacym "+
        "za komende Straznikow Solamnii. Pod sciana wstawiony jest "+
        "duzych rozmiarow debowy stol, na ktorym znajduje sie pelno "+
        "roznych papierow. Tuz obok stolu, w rogu pokoju, stoi srednich "+
        "rozmiarow szafa, zamknieta na klodke. Calosc dopelniaja dwa "+
        "masywne krzesla stojace po obu stronach stolu. "+
        "Przez zaciagniete ciezka kotara okno nie dociera tu zadne "+
        "swiatlo, a blady plomyk saczacy sie z kaganka zawieszonego "+
        "na scianie wystarczy zeby oswietlic ten pokoj. \n");

        add_item ( "stol",
        "Ciezki, okuty po bokach metalowymi okuciami stol, na ktorym "+
        "znajduje sie mnostwo roznych papierow. Zapewne sa to raporty "+
        "skladane przez wracajacych z drogi Straznikow. \n");
         	
        add_item ( "kotare",
        "Ciezka kotara zaslaniajaca okno. \n");
        
        add_item ( "krzesla",
       "Dwa masywne krzesla. Zapewne mozna na nich usiasc. \n");

	add_item ( "szafe",
	"Zamknietna na klodke szafa, w ktorej przechowywane sa wazniejsze "+
	"i poufne sprawy lub raporty. \n");
	    
        add_item ( "kaganek",
        "Swiecacy bladym plomieniem kagan, wiszacy na scianie pod ktora stoi "+
        "stol. \n");       
        
    add_exit(NPATH + "hall", "polnoc", 0, 1);
}

int
awansuj(string str)
{
   mixed kogo;
   notify_fail("Awansuj <kogo> na <jakie stanowisko>?\n");
   if (!strlen(str)) return 0;
   
   parse_command(str,environment(TP),"%l:" + PL_BIE + " 'na' %s",kogo,str);
   
   kogo=NORMAL_ACCESS(kogo,0,0);
   
   kogo=filter(kogo,interactive);
   
   if (!sizeof(kogo)) return 0;
   
   if ((ADMIN->query_komendant())!=TP->query_real_name())
      return notify_fail("Nie jestes komendantem!\n");
   
   if (str!="sierzanta"&&str!="komendanta") 
      return notify_fail("Mozesz awansowac na sierzanta lub komendanta.\n");
   
   if (sizeof(kogo)>1) 
      return notify_fail("Mozesz awansowac co najwyzej jedna osobe naraz.\n");
   
   if (!ADMIN->query_czlonek(kogo[0]->query_real_name()))
      return notify_fail(C(kogo[0]->short(TP,PL_MIA)) + " nie jest Straznikiem.\n");
   
   if (str=="sierzanta")
      {
         if (!ADMIN->set_sierzant(kogo[0]->query_real_name()))
            return notify_fail(C(kogo[0]->short(TP,PL_MIA)) + " juz jest Sierzantem Strazy Krain.\n");
            
         write("Awansowal" +TP->koncowka("es ","as ") + 
            kogo[0]->short(TP,PL_BIE) + " na " +
            "Sierzanta Strazy Krain.\n");
         kogo[0]->catch_msg(C(TP->short(kogo[0],PL_MIA)) + " awansowal" +
            TP->koncowka("","a") + " cie na Sierzanta Strazy Krain!\n");
         return 1;
      }
   if (!ADMIN->set_komandant(kogo[0]->query_real_name()))
      return notify_fail(C(kogo[0]->short(TP,PL_MIA)) + " nie jest Sierzantem Strazy Krain, " +
         "nie mozesz " + kogo[0]->query_zaimek(PL_BIE) + " awansowac na Komendanta.\n");
         
   write(C(kogo[0]->short(TP,PL_MIA)) + "jest nowym Komendantem Strazy Krain!\n");
   kogo[0]->catch_msg("Jestes nowym Komendantem Strazy Krain!\n");
   
   return 1;
}