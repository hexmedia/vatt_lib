inherit "/cmd/std/command_driver";
#include <filter_funs.h>
#include <composite.h>

init_emoty()
{
	add_action("krzyknij","skkrzyknij");
	add_action("deklaruj","skokrzyk");
	add_action("powstrzymaj","skpowstrzymaj");
	add_action("pozdrow","skpowitaj");
	add_action("zagroz","skzagroz");
	add_action("melduj","skmelduj");
	add_action("pomoc","skpomoc");
	add_action("ochron","skochron");
	add_action("przedstaw","skprzedstaw");
}

int
krzyknij(string str)
{
	object *oblist;
	if (!str)
	{
		write("Z grozna mina krzyczysz: Spokoj! Prosze przestrzegac prawa!\n");
		all("z grozna mina krzyczy: Spokoj! Prosze przestrzegac prawa!");
		return 1;
	}
	oblist = parse_this(str,"'do' %l:"+PL_DOP);
	if (!sizeof(oblist)) return notify_fail("Do kogo chcesz krzyknac?\n");
	actor("Krzyczysz do",oblist,PL_DOP,": Spokoj! Prosze przestrzegac prawa!");
	all2act("krzyczy z grozna mina do",oblist,PL_DOP,": Spokoj! Prosze przestrzegac prawa!");
	target("krzyczy do ciebie z grozna mina: Spokoj! Prosze przestrzegac prawa!",oblist);
	return 1;
}

int deklaruj(string str)
{
	if (str) return notify_fail("Ta komenda nie przyjmuje zadnych argumentow.\n"); 
	write("Z dumna mina wydajesz z siebie bojowy okrzyk Straznikow Krain: Na Strazy Prawa stac przysiegl"+TP->koncowka("em","am")+"!\n");
	all("z dumna mina wydaje z siebie bojowy okrzyk Straznikow Krain: Na Strazy Prawa stac przysiegl"+TP->koncowka("em","am")+"!");
	return 1;
}

/*
 * Powstrzymaj to ciekawy, ale dosyc silny emot, podchodzacy poniekad pod 
 * specjala. Jesli kolo nas ktos bije dobrego npca, a my nie walczymy, 
 * mozemy sie dolaczyc do walki, zaslaniajac npca i przejmujac na siebie 
 * ciosy. Od tej pory bija nas, a nie jego. Komenda dziala nie na npca, 
 * tylko na bijacego, co jest spowodowane latwiejszym kodowaniem ;) i 
 * wiekszym realizmem: Mozna komus wskoczyc pod miecz latwiej niz kogos 
 * zaslonic. Po za tym rozwiazuje to problem kilku walczacych, bic cie 
 * moze kilka osob, a ty tylko jedna. 
 * Nie daje to zadnych wymiernych korzysci, wrecz przeciwnie, ale mysle
 * ze maniaki jak Dhor beda tego uzywac.
 * Na rozkaz Silviego usuwam funkcje specjalna... Zostaje tylko emot.
 */

int 
powstrzymaj(string str)
{
	object *oblist,kogo,bity;
	int n;
	if (TP->query_attack()) return notify_fail("Juz walczysz.\n");
	if (!str) return notify_fail("Kogo chcesz powstrzymac?\n");
	oblist=parse_this(str,"%l:"+PL_BIE);
	n=sizeof(oblist);
	if (!n) return notify_fail("Kogo chcesz powstrzymac?\n");
	if (n>1) return notify_fail("Kogo dokladnie chcesz powstrzymac?\n");
	kogo=oblist[0];
	bity=kogo->query_attack();
	if (!(bity->query_npc() && bity->query_alignment()>0)) 
		return notify_fail("Powstrzymywac mozesz tylko mordujacych niewinnych mieszkancow Krain.\n");
	if (!F_DARE_ATTACK(TP,kogo)) return notify_fail("Nie odwazysz sie na to.\n");
	actor("Ruszasz na",oblist,PL_BIE," w nadzieji powstrzymania go od zabicia niewinnej osoby.");
	all2act("rusza na",oblist,PL_BIE,", starajac sie odciagnac go od "+ bity->short(PL_DOP)+".");
	target("rusza na ciebie, starajac sie zaslonic "+bity->short(PL_BIE)+".",oblist);
//	kogo->attack_object(TP); // Mozna zakomentowac... Jesli za dobre bez taxa
	TP->attack_object(kogo);
	return 1;
}


int 
ochron(string str)
{
	object *oblist,kogo,bity;
	int n;
	if (TP->query_attack()) return notify_fail("Juz walczysz.\n");
	if (!str) return notify_fail("Kogo chcesz ochronic?\n");
	oblist=parse_this(str,"%l:"+PL_BIE);
	n=sizeof(oblist);
	if (!n) return notify_fail("Kogo chcesz ochronic?\n");
	if (n>1) return notify_fail("Kogo dokladnie chcesz ochronic?\n");
	bity=oblist[0];
	oblist=bity->query_enemy(-1);
	while (sizeof (oblist) && !present(oblist[0], environment(TP)))
		oblist-=({oblist[0]});
	if (!sizeof(oblist)) 
		return notify_fail(bity->query_Imie(TP, PL_MIA)+" z nikim nie walczy!\n");
	kogo=oblist[0];
	if (!(bity->query_npc() && bity->query_alignment()>0)) 
		return notify_fail("Chronic mozesz tylko niewinnych mieszkancow Krain.\n");
	if (!F_DARE_ATTACK(TP,kogo)) return notify_fail("Nie odwazysz sie na to.\n");
	actor("Ruszasz na",({kogo}),PL_BIE," w nadzieji powstrzymania go od zabicia niewinnej osoby.");
	all2act("rusza na",({kogo}),PL_BIE,", starajac sie odciagnac go od "+ bity->short(PL_DOP)+".");
	target("rusza na ciebie, starajac sie zaslonic "+bity->short(PL_BIE)+".",({kogo}));
//	kogo->attack_object(TP); // Mozna zakomentowac... Jesli za dobre bez taxa
	TP->attack_object(kogo);
	return 1;
}

int 
pozdrow(string str)
{
	object *oblist;
	if (!str) 
	{
		write("Podnosisz dlon do gory w powitalnym gescie Straznikow Krain.\n");
		all("podnosi dlon do gory w powitalnym gescie Straznikow Krain.");
		return 1;
	}
	return notify_fail("Ta komenda nie przyjmuje zadnych argumentow.\n");
}

int
zagroz(string str)
{
	object *oblist, bron;
	string zaimek, krotki, zaimek2;
	int n;
	bron=(TP->query_weapon(W_RIGHT) || TP->query_weapon(W_BOTH) || TP->query_weapon(W_LEFT));
	if (!bron)
	{
		if (!str)
		{
			allbb("patrzy wokol z grozna mina, pilnujac by nikt nie wszczynal rozroby.");
			write("Rozgladasz sie wokol z grozna mina, pilnujac by nikt nie wszczynal rozroby.\n");
			return 1;
		}
		oblist=parse_this(str,"%l:"+PL_CEL);
		n=sizeof(oblist);
		if (!n) return notify_fail("Komu chesz zagrozic?\n");
		if (n-1) zaimek="im";
		else zaimek=oblist[0]->query_zaimek(PL_CEL,0);
		all2actbb("patrzy na",oblist,PL_BIE," z grozna mina, ktora nie wrozy "+zaimek+" nic dobrego w razie wszczecia rozroby.");
		target("patrzy na Ciebie z grozna mina, ktora nie wrozy ci nic dobrego w razie wszczecia rozroby.",oblist);
		actor("Patrzysz na",oblist,PL_BIE," z grozna, nie wrozaca nic dobrego, mina.");
		return 1;
	}
	krotki=bron->short(PL_MIA);
	zaimek = TP->query_zaimek(PL_DOP,1);
	if (!str)
	{
		allbb("rozglada sie wokol z grozna mina, a "+krotki+" w "+zaimek+" reku drzy niespokojnie.");
		write("Rozgladasz sie wokol z grozna mina, jakby od niechcenia ruszajac "+bron->short(PL_NAR)+".\n");
		return 1;
	}
	oblist=parse_this(str,"%l:"+PL_CEL);
	n=sizeof(oblist);
	if (!n) return notify_fail("Komu chesz zagrozic?\n");
	if (n-1) zaimek2="im";
	else zaimek2=oblist[0]->query_zaimek(PL_CEL,1);
	all2actbb("patrzy na",oblist,PL_BIE," z grozna mina, ktora nie wrozy "+zaimek2+" nic dobrego w razie wszczecia rozroby. "+ capitalize(krotki)+" drzy niespokojnie w "+zaimek+" reku.");
	target("patrzy na Ciebie z grozna mina, a "+krotki+" w "+zaimek+" reku drzy niespokojnie. Nie wrozy ci to nic dobrego w razie wszczecia rozroby.",oblist);
	actor("Patrzysz na",oblist,PL_BIE," z grozna, nie wrozaca nic dobrego, mina, a "+krotki+" w twoim reku drzy niespokojnie.");
	return 1;
}	

int
melduj(string str)
{
	object *oblist;
	string intro,tmp;
	int n;
	intro=TP->short(PL_MIA);
	tmp=TP->query_guild_title_race();
	if (tmp) intro+=(" "+tmp);
	tmp=TP->query_guild_title_occ();
	if (tmp) intro+=(", "+tmp);
	tmp=TP->query_guild_title_lay();
	if (tmp) intro+=(", "+tmp);
	intro+=(", " + ADMIN->query_stanowisko(TP->query_real_name()));
	if (!str) return notify_fail("Chyba melduj sie?\n");
	parse_command(str,TP,"'sie' %s",str);
	if (!str || str=="")
	{
		all("wypreza dumnie piers, staje na bacznosc i salutujac melduje sie: "+intro);
		write("Wyprezasz dumnie piers, stajesz na bacznosc i salutujac meldujesz sie: "+intro+"\n");
		return 1;
	}
	oblist=parse_this(str,"%l:"+PL_CEL);
	n=sizeof(oblist);
	if (!n) return notify_fail("Komu chcesz sie przedstawic?\n");
	all2act("wypreza dumnie piers, staje na bacznosc i salutujac",oblist,PL_CEL," melduje sie: "+intro);
	target("wypreza dumnie piers, staje na bacznosc i salutujac Ci melduje sie: "+intro,oblist);
	actor("Wyprezasz dumnie piers, stajesz na bacznosci i salutujac",oblist,PL_CEL," meldujesz sie: "+intro);
	return 1;
}


int 
pomoc(string str)
{
	if (!str)
	{
		write("Szkarlatna szarfa jest symbolem twojej przynaleznosci do"
			+" Strazy Krain. Jesli ja zawiazesz ( 'zawiaz szarfe' ) na sobie, "
			+"wszyscy beda widzieli, ze jestes jednym ze Straznikow. \n"
			+"Jako Straznik masz kilka dodatkowych komend: \n"
			+"\t skkrzyknij [do kogo] \n\t skokrzyk \n\t skpowitaj "
			+"\n\t skzagroz [komu] \n\t skmelduj \n\t skpowstrzymaj 'kogo' \n\t skochron 'kogo'\n"
			+"Mozesz uzyskac pomoc do tych komend: skpomoc 'komenda' \n");
			return 1;
	}
	switch (str)
	{
		case "skkrzyknij" : 
			write("Komenda: skkrzyknij [do kogo] \nZ grozna mina krzyczysz "
				+"[do kogo]: Spokoj! Prosze przestrzegac prawa!\n");
			break;
		case "skokrzyk" :
			write("Komenda: skokrzyk \nZ dumna mina wydajesz z siebie bojowy"
				+" okrzyk Straznikow Krain: Na Strazy Prawa stac"
				+" przysiegl[em/am]!\n");
			break;
		case "skpowitaj" :
			write("Komenda: skpowitaj \nPodnosisz do gory dlon w powitalnym "
				+"gescie Straznikow Krain.\n");
			break;
		case "skzagroz" :
			write("Komenda: skzagroz \nRozgladasz sie wokol z grozna mina, "
				+"pilnujac by nikt nie wszczynal rozroby.\nRozgladasz sie "
				+"wokol z grozna mina, a [bron] w twoim reku drzy "
				+"niespokojnie.\nKomenda: skzagroz [komu]\nPatrzysz na [kogo] "
				+"z grozna, nie wrozaca nic dobrego mina.\nPatrzysz na [kogo] "
				+"z grozna, nie wrozaca nic dobrego mina, a [bron] w twoim"
				+" reku drzy niespokojnie.\n");
			break;
		case "skmelduj" :
			write("Komenda: skmelduj sie [komu]\nWyprezasz dumnie piers, stajesz na "
				+"bacznosc i salutujac [komu] meldujesz sie: (twoje imie, wszystkie "
				+"tytuly i na koncu 'Straznik Krain/Sierzant Strazy Krain/Komendant Strazy Krain')\n");
			break;
		case "skpowstrzymaj" :
			write("Komenda: skpowstrzymaj 'kogo'\nRuszasz na [kogo] w nadziej "
				+"powstrzymania go przed zabiciem [kogo].\nKomenda ta sluzy do "
				+"wlaczenia sie do walki po stronie niewinnego mieszkanca "
				+"bronionych krain.\n");
			break;
		case "skprzedstaw" : 
			write("Komenda: skprzedstaw sie [komu]\nDziala tak samo jak " +
				"przedstaw sie, jednak do wszystkich tytulow gildiowych " +
				"dodaje tez 'Straznik Krain/Sierzant Strazy Krain/Komendant Strazy Krain'.\n");
			break;
		default: write("Nie ma takiego tematu pomocy.\n");
	}
	return 1;
} 

int
przedstaw(string str)
{
   object *komu=0,*inni=0;
   int wszyscy=0,i;
   string pres=TP->query_name() + " " + TP->query_title() + ", " + 
   		(stringp(pres=ADMIN->query_stanowisko(TP->query_real_name()))
   		 ?pres + ", ":"")+TP->query_rasa();
   
   notify_fail(capitalize(query_verb()) + " sie [<komu>]?\n");
   if (!stringp(str)) return 0;
   
   if (str!="sie"&&!sscanf(str,"sie %s",str)) return 0;
   
   if (strlen(str)&&str!="sie")
      {
       komu=parse_this(str,"%l:" + PL_CEL);
      }
   else
      {komu=FILTER_LIVE(all_inventory(environment(TP)))-({TP});
       wszyscy=1;}
   
   if (!sizeof(komu)&&!wszyscy) return 0;
   
   TP->reveal_me();
   
   write("Przedstawiasz sie " + (wszyscy?"wszystkim":COMPOSITE_LIVE(komu,PL_CEL)) +
   	".\n");
   
   if (wszyscy&&!sizeof(komu)) 
      {
         write("Nie widzisz w tym co prawda najmniejszego sensu, gdyz " +
   	  "nie ma nikogo, kto moglby cie uslyszec.\n");
   	 return 1;
      }
   
   for (i=0;i<sizeof(komu);i++)
       {
          tell_object(komu[i],capitalize(TP->short(komu[i],PL_MIA)) + 
          	" przedstawia sie jako:\n" + pres + ".\n");
       }

   inni=FILTER_LIVE(all_inventory(environment(TP)))-komu-({TP});
   
   for (i=0;i<sizeof(inni);i++)
       {
          tell_object(inni[i],capitalize(TP->short(inni[i],PL_MIA)) + 
          	" przedstawia sie " + FO_COMPOSITE_LIVE(komu,inni[i],PL_CEL) +
          	".\n");
       }
   
   komu->add_introduced(TP->query_real_name(PL_MIA),
   			TP->query_real_name(PL_BIE));
   
   return 1;
}
