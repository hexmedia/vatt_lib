inherit "/d/Imperium/std/room";

#include "mith.h"

create_imperium_room()
{
   set_short("Budynek Strazy Krain");
   
   set_long("Znajdujesz sie w niezbyt obszernym pomieszczeniu " +
   	"wewnatrz budynku Strazy Krain. Drzwi prowadzace na ulice " +
   	"znajduja sie w polnocnej scianie. W przeciwleglej scianie " +
   	"widnieja drzwi z napisem 'Komendant', jeszcze jedno " +
   	"przejscie znajduje sie w scianie zachodniej. Wzdluz scian " +
   	"stoi pare dlugich debowych law, widnieje tez srednich rozmiarow " +
   	"stol.\n");
   add_item(({"stol","lawe","lawy"}), "Jest to jedyne umieblowanie " +
   	"tego pomieszczenia. Dlugie lawy znajduja sie przy scianach, " +
   	"stol zas w najdalszym od wejscia rogu.\n");
   
   add_item("drzwi","Jedne drzwi prowadza na ulice, drugie zas do biura " +
   	"komendanta.\n");
   
   add_exit("/d/Imperium/Nuln/koszary/garnizonowa3","polnoc",0,1,0);
   add_exit(NPATH + "biuro","poludnie",0,1,0);
   add_exit(NPATH + "izba","zachod",0,1,0);
}