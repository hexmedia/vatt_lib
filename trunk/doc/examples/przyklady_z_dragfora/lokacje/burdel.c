/*---------Burdelik w Crimmor---------*/
/*-----------Copyright Volo-----------*/
/*----------Normalny burdel-----------*/
#pragma strict_types

inherit "/d/Faerun/std/room";

#include <stdproperties.h>
#include <macros.h>
#include "../definicje.h"

int wszystko(string c);
int wstan (string b);
int usiadz (string a);
int przeczytaj_tabliczke();

void
create_room()
{
    set_short("Dom publiczny");
    set_long("Stoisz wewnatrz solidnego budynku, ktory od strony ulicy zupelnie nie "
	  + "przypomina tego czym jest w srodku. Przejrzyste okna zdobia czerwone zaslony oraz "
	  + "rozowe firany, zas zaraz przy wejsciu na pieterko stoi drewniana lada. Na ladzie "
	  + "lezy pieknie wyprasowany bialy, koronkowy obrus, a na nim polozono kilka pomaranczowych "
	  + "serwetek sluzacych jako ozdoba. W rogu sali wejsciowej dostrzegasz obita bialym pluszem "
	  + "kanape, nad ktora to zawieszono duzy obrazek, rozebranej do bielizny, pieknej kobiety. "
	  + "Na scianie powieszono jakas dziwna tabliczke, ktora to obito w debowa ramke. "
	  + "Najprawdopodobniej umieszczono na niej tutejszy cennik "
	  + "'uslug'. \n");
    add_item( ({ "tabliczke", "cennik" }), "Na scianie zawieszono obity w debowe ramki, starannie "
	  + "napisany cennik. Jak zauwazyles osoba, ktora wszystko nan pisala poslugiwala sie najbardziej "
	  + "wymyslnymi sztuczkami jakie oferuje wdzieczna sztuka kaligrafii. Moze wartalo by ja przeczytac? \n");
    add_item("kanape", "Jest to stojaca w rogu, obita pluszem kanapa, na ktorej w kazdej chwili mozesz "
	  + "spokojnie usiasc nawet w duzym towarzystwie, gdyz jej siedzenie jest wystarczajaco szerokie "
	  + "aby pomiescilo sie na niej wiele, zadnych goracych emocji mezczyzn. Moze chcialbys sobie na niej usiasc? \n");
    add_item( ({ "zaslony", "firany" }), "Sa to zaslaniajace okna czerwone zaslony, a takze rozowe "
	  + "i jaskrawe niczym gwiazdy firanki. Dobrze podkreslaja one klimat tego miejsca i czynia je "
	  + "bardzo magicznym w innym tego slowa znaczeniu. \n");
    add_item("obraz", "Zawieszony nad kanapa obraz prezentuje naga kobiete o dlugich czarnych wlosach "
	  + "ktore zaslaniaja jej kuszaca twarz oraz inne czesci ciala. Sadzac po nieco spiczastych uszach "
	  + "oraz troche innym profilu nosa mozesz stwierdzic, ze w jej zylach plynela zapewne rowniez "
	  + "krew cudownych elfow. \n");
    add_item("lade", "Drewniana lada za, ktora znajduje sie wejscie na pietro gdzie spelniane sa "
	  + "wymagania niecierpliwych klientow. Na ladzie polozono bialy obrus oraz kilka pomaranczowych "
	  + "serwetek, ktore sluza jako mila ozdoba. \n");
    add_cmd_item( ({ "cennik", "tabliczke" }), ({"przeczytaj"}), 
                 przeczytaj_tabliczke);
    set_event_time(75.0,75.0);
    set_events_on(1);
    add_event("Lekki wiaterek podwiewa zaslone. \n");
    add_event("Ze schodow schodzi wlasnie pewien wymeczony, ubrany mezczyzna. \n");
    add_event("Minela cie w tej chwili jakas piekna kobieta o harmonijnych ksztaltach. \n");
    add_event("Czujesz w powietrzu zapach parzonej herbaty. \n");
    add_event("Do lady podchodzi spragniony korzystania z tutejszych uslug mezczyzna. \n");
	add_event("Z gory dobiegaja cie odglosy upojnej zabawy.\n");
    add_exit("ulicasl4.c","poludnie");
}
    int
    przeczytaj_tabliczke()
{
    saybb(QCIMIE(this_player(), PL_MIA) + " z zainteresowaniem czyta tutejszy cennik uslug.\n");
    write("Brak wolnych pokoi. \n");
    return 1;
}
int
usiadz (string a)
{
	string gdzie;

	notify_fail("Usiadz gdzie?\n");
	if(!a)
		return 0;
	if (!sscanf(a, "na %s", gdzie))
		return 0;
	if (!gdzie)
		return 0;
	if (gdzie != "kanapie")
		return 0;
	if(this_player()->query_prop(SIEDZACY))
	{
		write("Alez ty juz siedzisz!\n");
		return 1;
	}
	this_player()->add_prop(SIEDZACY, 1);
	this_player()->add_prop(LIVE_S_EXTRA_SHORT, " siedzi na kanapie.");
	write("Wygodnie siadzasz na kanapie.\n");
	saybb(capitalize(this_player()->query_name()) + " wygodnie siada na kanapie.\n");
	return 1;
}

int
wstan (string b)
{
	string buhu;

	notify_fail("Wstan z czego?\n");
	if(!b)
		return 0;
	if(!sscanf(b, "z %s", buhu))
		return 0;
	if(!buhu)
		return 0;
	if(buhu != "kanapy")
		return 0;
	if (!(this_player()->query_prop(SIEDZACY)))
	{
		notify_fail("Nigdzie nie siedzisz.\n");
		return 0;
	}
	this_player()->add_prop(SIEDZACY, 0);
	this_player()->add_prop(LIVE_S_EXTRA_SHORT, "");
	write("Wstajesz z kanapy.\n");
	saybb(capitalize(this_player()->query_name()) + " wstaje z kanapy.\n");
	return 1;
}

int
wszystko(string c)
{
	string czas;
	string *siedzace, *stojace;
	
	stojace = ({"zeskocz"});
	siedzace = ({"n","polnoc","s","poludnie","w","zachod","e","wschod","nw",
        "polnocny-zachod","sw","poludniowy-zachod","se","poludniowy-wschod",
        "polnocny-wschod","ne","teleport",
        "u","gora","d","dol","goto","home", "wychyl"});

	czas = query_verb();

	if(this_player()->query_prop(SIEDZACY))
	{
		if(member_array(czas, siedzace) == -1) return 0;		
		else 
		{
			write("Moze wstan zanim to zrobisz?\n");
			return 1;
		}
	}
	else
	{
		if(member_array(czas, stojace) == -1) return 0;		
		else 
		{
			write("Jesli chcesz to zrobic usiadz najpierw na kanapie.\n");
			return 1;
		}
	}
	return 0;
}

/*----Podpisano: 'Volothamp Geddarm'----*/
    
