#pragma strict_types

inherit "/d/Faerun/std/room.c";

#include <stdproperties.h>
#include "../definicje.h"
#include <macros.h>
#include <wywolania.h>

void
create_room()
{
	set_short("Alkowa");
	set_long("Jest to wyodrebniona czesc tawerny, ktora ukryto dobrze za podluzna "
	  + "kotara. Tylko tutaj mozesz spokojnie porozmawiac w cztery oczy z wybrana "
	  + "osoba nie bedac zaklocanym przez gwar rozmow w glownej sali. Panuje tu "
	  + "niemala cisza, ktora wydaje sie byc nawet podejrzana. Na scianach znajduja "
	  + "sie liczne swieczniki dobrze oswietlajace alkowe. Spotyka sie tutaj wiele "
	  + "odwiedzajacych tawerne osob lecz w tym momencie alkowa swieci w zasadzie "
	  + "pustkami.\n");

	add_item( "swieczniki", "Przymocowane do scian czteroramienne swieczniki "
	  + "oswietlaja alkowe rzucajac na nia niezbyt nikle swiatlo.\n");

	set_event_time(300.0,300.0);
	set_events_on(1);
	add_event("Z glownej sali dobiega twych uszu podniesiony glos jednego z gosci.\n");
	add_event("Slyszysz plomyk tlacej sie swiecy.\n");

	room_add_object(PATH + "kufer.c", 1);

	add_exit("plonacy_czarodziej1.c", "polnoc");

 add_npcs(NPC_PATH + "orlen1.c", 1);

	/* Propy, obiekty i npce */
	
	add_prop(ROOM_I_INSIDE,1);
}

