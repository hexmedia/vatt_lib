#pragma strict_types

inherit "/d/Faerun/std/karczma.c";

#include <stdproperties.h>
#include "../definicje.h"
#include <macros.h>
#include <pub.h>

void
create_room()
{
	set_short("Tawerna 'Plonacy Czarodziej'");
	set_long("Ta tawerna jest miejscem nieustannie przepelnionym goscmi, zarowno "
	  + "przyjezdnymi jak i miejscowymi. Akolici Lathandera z pobliskiej swiatyni "
	  + "sa szkoleni do podtrzymywania prowadzonych tu zywych rozmow i zabaw. "
	  + "'Plonacy Czarodziej' to niewielki, ale przyjemny przybytek a ty wlasnie "
	  + "stoisz w jego glownej sali. Jest tutaj kilka okraglych stolikow i wiele "
	  + "wolnych na ten czas krzesel. Do polnocnej sciany przymocowano solidnie "
	  + "kilka prostych polek, na ktorych szynkarz eksponuje wiele roznych "
	  + "starych naczyn jak na przyklad dzbany na miod pitny czy mysliwskie "
	  + "kufle z licznymi zdobieniami sluzace do przechowywania piwa. Na scianie "
	  + "przy wejsciu wisi jakas tabliczka.\n");

	add_item( "stoliki", "Zwykle, drewniane stoliki, ktore otoczone sa przez "
	  + "rownie zwykle i drewniane krzesla. Na blacie kazdego stolika postawiono "
	  + "swieczke i polozono kilka czystych serwetek.\n");
	add_item( "krzesla", "Sa to drewniane krzesla z oparciami, ktore ustawiono "
	  + "dookola kazdego stolika. Z radoscia musisz przyznac, ze jest jeszcze "
	  + "kilka wolnych miejsc.\n");
	add_item( "polki", "Przymocowane do polnocnej sciany proste polki, na ktorych "
	  + "karczmarz postawil wiele starych naczyn nie nadajacych sie juz do uzytku.\n");
	add_item( "dzbany", "Sa to wielkie dzbany, w ktorych poprzedni wlasciciele tej "
	  + "starej tawerny podawali gosciom pitny miod.\n");
	add_item( "kufle", "Mysliwskie kufle stojace na polce nie sa zwyklymi szklanicami "
	  + "sluzacymi do picia piwa lecz misternie stworzonymi naczyniami, ktore z "
	  + "wierzchu zaopatrzono w liczne ozdoby bedace scenami z wielu polowan jakie "
	  + "mialy miejsce w okolicach Beregostu.\n");

	add_cmd_item(({"tabliczke","menu"}),({"czytaj","przeczytaj"}), "@@menu");

	add_prop(ROOM_I_INSIDE,1);

	add_exit("ulica7.c", "wschod");
	add_exit("plonacy_czarodziej2.c", "poludnie");

	set_event_time(180.0,180.0);
	set_events_on(1);
	add_event("Wsrod gwaru rozmow slyszysz wymiane zdan paru magow.\n");
	add_event("Gospodarz mile wita kilku nowoprzybylych gosci.\n");
	add_event("Na podloge rozlewa sie cala zawartosc kufla piwa.\n");
	add_event("Stojacy na polce dzban niemalze rozbija sie na ziemi.\n");
	add_event("Ze strony drzwi wpada do pomieszczenia zimne powietrze.\n");
	add_event("W powietrzu roznosi sie won przeroznych olejkow.\n");
	add_event("Kolo ciebie przeszedl mezczyzna niosacy pyszne udko kurczaka.\n");
	add_event("Czujesz przyjemny zapach pitnego miodu, ktory roznosi sie po tawernie.\n");
	add_event("Pewien bywalec stara sie stargowac cene posilku.\n");
	add_event("Ktos wykloca sie glosno o cene noclegu.\n");
	add_event("Surowy akolita lustruje wzrokiem wnetrze tawerny.\n");

	add_food("Ociekajace tluszczem udko z kurczaka", ({"udko","udka z kurczaka", "ociekajace tluszczem udko"}), 
		     "pyszne, ociekajace tluszczem udko z kurczaka", 180, 80, "pyszne, ociekajace tluszczem udko z kurczaka");

	add_food("Pieczona, swinska karkowka", ({"karkowke","swinska karkowke", "pieczona karkowke"}), 
		     "pieczona, swinska karkowke", 250, 40, "pieczona, swinska karkowke");

	add_food("Kawalek sera", ({"ser","kawalek sera"}), 
		     "kawalek sera", 160, 20, "kawalek sera");

	add_food("Talerz pysznych lazanek", ({"lazanki","pyszne lazanki", "talerz lazanek"}), 
		     "talerz pysznych lazanek", 170, 90, "pyszne lazanki");

	add_food("Pyszny mleczny jogurt, zawierajacy liczne kawalki owocow i innych nie znanych Ci przypraw", ({"jogurt","przysmak", "przysmak selune"}), 
		     "pyszny mleczny jogurt", 100, 100, "pyszny mleczny jogurt, zawierajacy liczne kawalki owocow i innych nie znanych Ci przypraw");

	add_drink("miod",({"miodu", "dzbana miodu"}), ({"miod", "dzban miodu"}), "pysznego, zlocistego miodu",
    500,15,180,PL_MESKI_NOS_NZYW, DZBAN, "miodu", "Jest to duzy dzban z porecznym uchwytem. Na jego powierzchni "
	+ "wystrugano niewielkie zdobienia przedstawiajace prawdopodobnie sceny z polowan. Dostrzegasz takze emblemat gospody "
	+ "'Plonacy Czarodziej'.\n", 1000);

	add_drink("piwo",({"piwa", "kufla piwa"}), ({"piwo", "kufel piwa"}), "dlugo wazonego, mysliwskiego piwa",
    500,7,170,PL_NIJAKI_NOS, KUFEL, "piwa", "Jest to drewniany kufel z porecznym uchwytem. Na jego powierzchni "
	+ "wystrugano niewielkie zdobienia przedstawiajace prawdopodobnie sceny z polowan. Dostrzegasz na jego powierzchni emblemat gospody "
	+ "'Plonacy Czarodziej'.\n", 2500);

	add_drink("wodka",({"wodki", "kieliszka wodki"}), ({"wodke", "kieliszek wodki"}), "ciemnej wodki ziolowej",
    100,45,500,PL_ZENSKI, KIELISZEK, "wodki", "Jest to wykonany z gladkiego, krystalicznego szkla maly kieliszek "
	+ "sluzacy do picia niewielkich ilosci mocnych alkoholi. Dostrzegasz na jego powierzchni emblemat gospody "
	+ "'Plonacy Czarodziej'.\n");

	add_drink("gorzalka",({"gorzalki", "kieliszka gorzalki"}), ({"gorzalke", "kieliszek gorzalki"}), "mocnej gorzalki",
    50,55,1000,PL_ZENSKI, KIELISZEK, "gorzalki", "Jest to wykonany z gladkiego, krystalicznego szkla maly kieliszek "
	+ "sluzacy do picia niewielkich ilosci mocnych alkoholi. Dostrzegasz na jego powierzchni emblemat gospody "
	+ "'Plonacy Czarodziej'.\n");

	add_drink("mleko",({"mleka", "szklanki mleka"}), ({"mleko", "szklanke mleka"}), "cieplego mleka od krowy",
    250,0,80,PL_NIJAKI_NOS, SZKLANKA, "mleka", "Oto zwykla wysoka szklanka sluzaca do picia przeroznych napoi. Dostrzegasz "
	+ "na jej powierzchni emblemat gospody 'Plonacy Czarodziej'.\n", 2000);

	set_sit("przy stoliku","przy stoliku","od stolika");

	ustaw_karczmarza(NPC_PATH + "flint.c");

}
string
menu()
{
    say(QCIMIE(this_player(),PL_MIA) + " czyta tabliczke.\n");
    return (
    "o-------------------------------------------------------------o\n"+
	"|                  Tawerna 'Plonacy Czarodziej'               |\n"+
	"o-------------------------------O-----------------------------o\n"+
    "|  Trunki:                      |          Jadlo:             |\n"+
    "|                               |                             |\n"+
	"|- Dzban miodu pitnego     (90) |- Udko kurczaka          (80)|\n"+
    "|- Kufel mysliwskiego piwa (85) |- Pieczona karkowka      (40)|\n"+ 
    "|- Wodka ziolowa           (50) |- Kawalek sera           (20)|\n"+
	"|- Mocna gorzalka          (60) |- Lazanki                (90)|\n"+
	"|- Cieple mleko od krowy   (20) |- Przysmak Selune       (100)|\n"+
	"|                               |                             |\n"+
	"o--------------------------------O----------------------------o\n"+
	"| Wszystkie ceny podawane sa w miedziakach. Zyczymy smacznego!|\n"+
	"o-------------------------------------------------------------o\n");
}



