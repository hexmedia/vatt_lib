#pragma strict_types  //Falandir 2003, opisy Kedrig
inherit "lib/pub";    //Restauracja "Gesie pioro" 
#include <stdproperties.h>
#include <macros.h>
#include "../definicje.h"
inherit STD+"room_crimm";
public int no_command();
public int no_command2();
void
create_crimm_room()
{       
    set_short("Restauracja 'Gesie pioro'");
    set_long("Znajdujesz sie w przestronnym pomieszczeniu: sali glownej "+
             "restauracji 'Gesie pioro'. Jest to prawdziwie wytworna karczma o "+
             "czym swiadcza zarowno podawane tu potrawy jak i styl umeblowania "+
             "czy ogolna dekoracja w sklad, ktorej wchodza cenne obrazy w drogich "+
             "ramach. Jest tutaj szereg slicznych ciemno czerwonych stoliczkow "+
             "oraz takich samych krzeselek, wykonanych prawdopodobnie z machoniu. "+
             "Ciemna debowa lada, a za nia zapracowany barman i szafka wypelniona "+
             "po brzegi najrozniejszymi alkoholami dodaje temu miejscu osobliwego "+
             "uroku. Na scianie wisi drewniana tablica stanowiaca jadlospis. "+
             "Na polnocy znajduje sie przejscie do zatloczonej kuchni przez, "+
             "ktore od czasu do czasu, podaza jakis dystyngowany kelner. Z "+
             "zachodu dociera do twych uszu melodyjny spiew ptakow, ktore "+
             "ulokowaly swe gniazda na licznych drzewach uroczego ogrodka.\n"); 
    add_prop(ROOM_I_INSIDE,1);
    add_cmd_item(({"tabliczke","menu","jadlospis"}),({"czytaj","przeczytaj"}),
                   "@@menu");
    
    add_drink("porter","porter","kufel mocnego, ciemnego portera",
              "",10,500,0,500,PL_MESKI_NZYW);
              
    add_drink("likier","liker","szklaneczke slodkiego, brzoskwiniowego likieru",
              "",({"brzoskwiniowy","brzoskwiniowi"})
              ,17,150,0,450,PL_MESKI_NZYW); 
              
    add_drink("wino","wina","kielich pysznego bialego wina",
              "",({({"pol wytrawny","bialy"}),({"pol wytrawni",
              "biali"})}),12,100,0,50,PL_NIJAKI_NOS); 
              
    add_drink("wino","wina","kieliszek gorzkiego, czerwonego wina",
              "",({({"wytrawny","czerwony"}),({"wytrawni",
              "czerwoni"})}),12,100,0,1500,PL_NIJAKI_NOS);
              
    add_drink("wino","wina","kielich zgaszacego pragnienie wina ognistego",
              "",({"ogniste","ognisci"})
              ,12,100,0,1200,PL_NIJAKI_NOS); 
              
    add_drink("wino","wina","kieliszek wytrawnego, bialego wina",
              "",({({"wytrawny","bialy"}),({"wytrawni",
              "biali"})}),12,100,0,1250,PL_NIJAKI_NOS); 
              
   
    add_food("bigos",({({"mysliwski","mysliwskie"})})
            ,300,35,PL_MESKI_NOS_ZYW,"doskonale doprawiony, bigos mysliwski");

    add_food("ser",//({ ({"damaryjski"}),{("damaryjskie"})}),
             0,300, 35,PL_MESKI_NOS_ZYW,"talerzyk pysznego damaryjskiego sera czerwonego");
             
    add_food("chleb",//({ ({"bialy"}), ({"biali"}) }),
            0,300, 40,PL_MESKI_NZYW, "kilka kromek bialego, pszennego chleba");
            
    add_food("prosiak",//({ ({"nadziewany"}), ({"nadziewani"}) }),
            0,450,70,PL_MESKI_NZYW,"nadziewanego farszem prosiaka");
            
    add_food("jagniecie",//({ ({"duszony"}), ({"duszone"}) }),
            0,500,85,PL_NIJAKI_NOS,"duszone jagniecine w ziolach");
            
    add_food("sernik",//({ ({"krolewski"}), ({"krolewscy"}) }),
            0,250, 30,PL_MESKI_NZYW, "podzielony na kawalki, krolewski sernik");        
            
            
    add_item("stoliczki","Sa to sliczne, ciemno czerwone stoliczki wykonane "+
             "z machoniu, otoczone dookola rownie ladnymi wygodnymi, krzeselkami.\n");
    
    add_item("krzeselka","Otaczajace stoliczki, urocze krzeselka sa tej samej "+
             "ciemno czerwonej barwy co one. Dodaja pomieszczeniu, w ktorym "+
             "stoja niezwyklego i osobliwego uroku.\n");
    add_item("przejscie","Jest to przestronne przejscie do kuchni na polnocy. "+
             "Czesto wychodza przez niego kelnerzy niosacy smakowite i "+
             "apetycznie wygladajace potrawy.\n");
    add_item("lade","Mocna, debowa lada, a za nia zapracowany karczmarz i "+
             "szafka wypelniona po brzegi przeroznymi alkoholami. Jezeli "+
             "zechcesz zamowic jakis trunek mozesz usiasc na wysokim, czerwonym "+
             "krzesle przy ladzie i rozkoszowac sie niezwyklym klimatem miejsca.\n");           
    add_item(({"jadlospis","tabliczke"}),"Jest to drewniana tablica zawierajaca spis wszystkich "+
            "potraw i napoi podawanych w restauracji 'Gesie pioro'. Chyba mozna "+
            "by go przeczytac.\n");
    add_item("obrazy","Drogie obrazy przedstawiajace sceny ze szlacheckich "+
             "pojedynkow, konne poscigi lub po prostu portrety slawnych "+
             "postaci, oprawione w cenne drewniane ramy.\n");
    add_item("ogrodek","Z tego pomieszczenia mozna przez uchylone drzwi, "+
             "dostrzec uroczy ogrodek stworzony na okazje upalnych dni, "+
             "kiedy klientom nie chce sie siedziec wewnatrz restauracji. "+
             "Z tamtego kierunku slychac spiew ptakow i widac wpadajace "+
             "przez drzwi jasne promienie slonca.\n");



    set_event_time(75,this_object());
    add_event("Do restauracji wchodzi obszarpany biedak i widzac oferowany "+
              "komfort natychmiast opuszcza to miejsce.\n");
    add_event("Przez drzwi z ogrodu wchodzi pewien zamozny, przystojny "+
              "szlachcic i grzecznie sie zegnajac opuszcza restauracje.\n");
    add_event("Przez drzwi wejsciowe wchodzi grupka pewnych zagadanych "+
              "jegomosciow po czym zajmuja stoliczek w rogu.\n");
    add_event("Nieznany ci, wstawiony pijak wchodzi do baru z dziarska mina, "+
              "lecz widzac, iz pomylil miejsca czym predzej wychodzi.\n");    
    add_event("Wokol wyczuwa sie osobliwy, nieco tajemniczy klimat tego "+
              "miejsca.\n");
    add_npcs(NPC_PATH+"restaurator",1);
    clone_object(NPC_PATH+"restaurator")->move(this_object());
    
    add_exit("restauracja1",({"ogrodek","do ogrodka"}));
    add_exit("uswiat2","wschod");
    add_exit("restauracja2",({"kuchnia","do kuchni"}));

}
void
init()
{
    ::init();
    init_pub();
    add_action("sit_down","usiadz");
    add_action("stand_up","wstan");  
    add_action("sit_down1","usiadz");
    add_action("stand_up1","wstan"); 
    add_action("do_all_commands", "", 1);    
}
string
menu()
{
    say(QCIMIE(this_player(),PL_MIA)+ " czyta tabliczke.\n");
    return
  "\nO----------------------------------------------------O \n"+
    "|__________MENU__________MENU__________MENU__________|\n"+
    "|                                                    |\n"+
    "|Przystawki:                           Ceny:         |\n"+
    "|                                                    |\n"+
    "|1. Damaryjski ser czerwony.           75  mdz       |\n"+
    "|2. Bialy chleb.                       50  mdz       |\n"+
    "|                                                    |\n"+
    "|Dania glowne:                                       |\n"+
    "|                                                    |\n"+ 
    "|1. Nadziewany prosiak                 175 mdz       |\n"+  
    "|2. Duszona jagniecina                 150 mdz       |\n"+
    "|                                                    |\n"+
    "|Desery:                                             |\n"+
    "|                                                    |\n"+
    "|1. Krolewski sernik                   95  mdz       |\n"+
    "|                                                    |\n"+
    "|Wybor trunkow:                                      |\n"+
    "|                                                    |\n"+
    "|1. Porter                             50  mdz       |\n"+
    "|2. Brzoskiwiowy likier                45  mdz       |\n"+
    "|3. Wytrawne czerwone wino             150 mdz       |\n"+
    "|4. Pol wytrawne biale wino            120 mdz       |\n"+
    "|5. Wino ogniste                       125 mdz       |\n"+
    "|                                                    |\n"+
    "O----------------------------------------------------O\n";
        
}
public int
do_all_commands(string str)
{
    string verb;
    string *nie_dozwolone;
    string *nie_dozwolone2;
    nie_dozwolone=({"n","polnoc","s","poludnie","w","zachod","e","wschod","nw",
        "polnocny-zachod","sw","poludniowy-zachod","se","poludniowy-wschod",
        "polnocny-wschod","ne","teleport",
        "u","gora","d","dol","goto","home"});
    nie_dozwolone2=({"zamow"});   
    verb = query_verb();
    if (this_player()->query_prop(SIEDZACY)==1)
    {
    if (member_array(verb,nie_dozwolone)==-1)
        return 0;
    else 
        return no_command();
    }
    if(this_player()->query_prop(SIEDZACY)==0)
    {
    if(member_array(verb,nie_dozwolone2)==-1)
        return 0;
        else
        return no_command2();
    }
    return 0; 
}
public int
no_command()
{
    write("Moze bys tak najpierw wstal.\n");
    return 1;
}
public int
no_command2()
{
    write("Zeby cos zamowic musisz najpierw siedziec.\n");
    return 1;
}
int
sit_down(string str)
{
    object who,*list;
    string check;
  
    if (!str)
        return 0;

    if (sscanf(str,"przy %s",check)==1)
        str = check;
    
    
    notify_fail("Gdzie chcesz usiasc, moze przy stoliczku?\n");
    if(str!="stoliczku")return 0;
    if(this_player()->query_prop(SIEDZACY)==1)
    {
    write("Ale ty juz siedzisz.\n");
    return 1;
    }
    this_player()->add_prop(SIEDZACY,1);
    this_player()->add_prop(LIVE_S_EXTRA_SHORT," siedzac"+
    this_player()->koncowka("y","a")+" przy stoliczku");
    write("Siadasz wygodnie przy stoliczku.\n");
    say(QCTNAME(this_player())+" siada wygodnie przy stoliczku.\n");
    return 1;
}
int
stand_up1(string str)
{
    object who,*list;
    string check;

    notify_fail("Nie siedzisz nigdzie.\n");
    if (this_player()->query_prop(SIEDZACY)==0)
        return 0;

    this_player()->change_prop(SIEDZACY,0);
    this_player()->change_prop(LIVE_S_EXTRA_SHORT,"");
    write("Wstajesz od stoliczka.\n");
    say(QCTNAME(this_player())+" wstaje od stoliczka.\n");
    return 1;
}
int
sit_down1(string str)
{
    object who,*list;
    string check;
  
    if (!str)
        return 0;

    if (sscanf(str,"przy %s",check)==1)
        str = check;
    
    
    notify_fail("Gdzie chcesz usiasc, moze przy stoliczku?\n");
    if(str!="ladzie")return 0;
    if(this_player()->query_prop(SIEDZACY)==1)
    {
    write("Ale ty juz siedzisz.\n");
    return 1;
    }
    this_player()->add_prop(SIEDZACY,1);
    this_player()->add_prop(LIVE_S_EXTRA_SHORT," siedzac"+
    this_player()->koncowka("y","a")+" przy ladzie");
    write("Siadasz wygodnie przy ladzie.\n");
    say(QCTNAME(this_player())+" siada wygodnie przy ladzie.\n");
    return 1;
}
int
stand_up(string str)
{
    object who,*list;
    string check;

    notify_fail("Nie siedzisz nigdzie.\n");
    if (this_player()->query_prop(SIEDZACY)==0)
        return 0;

    this_player()->change_prop(SIEDZACY,0);
    this_player()->change_prop(LIVE_S_EXTRA_SHORT,"");
    write("Wstajesz od lady.\n");
    say(QCTNAME(this_player())+" wstaje od lady.\n");
    return 1;
}
