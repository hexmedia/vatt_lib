#pragma strict_types

inherit "/d/Faerun/std/karczma.c";

#include <stdproperties.h>
#include "../definicje.h"
#include <macros.h>
#include <pub.h>

void
create_room()
{
set_short("Wesoly Zongler");
	set_long("Znajdujesz sie na obszernej sali bedacej wnetrzem gospody 'Wesoly Zongler'. "
	  + "Plotki mowia, ze nie ma chwili zeby w tym miejscu nie slychac bylo dziekow zabawy, "
	  + "muzyki i powszechnego ucztowania. Wielu mlodych obywateli miasteczka spotyka sie "
	  + "przy napojach i tancach. Pomieszczenie wydaje sie byc w zasadzie przecietne. Liczne "
	  + "stoliki, skonstruowane przez stolarza na planie okregu zapelniaja cala przestrzen "
	  + "biesiadnej izby, znajdujacej sie zaraz przed drewniana scena, na ktorej wesolo "
	  + "gra wynajeta orkiestra badz odbywaja sie mrozace krew w zylach wystepy. Na suficie "
	  + "wisi dyndajac krysztalowy zyrandol, ktory musial byc bardzo kosztowny skoro wlasciciel, "
	  + "zamozny pan zechcial sobie na niego pozwolic. Skoczne melodie faktycznie slyszane sa "
	  + "we wnetrzu budynku bezustannie i jak widac bardzo podoba sie to zebranej tu klienteli. "
	  + "Na scianie zawieszono miejscowy spis podawanego jadla oraz napitku.\n");

	add_item( "stoliki", "Sa to skonstruowane przez stolarza na planie okregu stoliki, przy ktorych "
	  + "ustawiono cztery krzeselka.\n");
	add_item( "zyrandol", "Ten dyndajacy pod sufitem, krysztalowy zyrandol wyglada na taki, ktory "
	  + "duzo kosztuje, totez dlugo moznaby zachwycac sie jego pieknoscia i rzucanym przezen swiatlem.\n");
	add_item( "orkiestre", "Grajaca na scenie orkiestra sklada sie z czterech doroslych mezczyzn: "
	  + "Jednego skrzypka, jednego bebniarza, jednego gitarzysty i jednego czlowieka grajacego na harfie. "
	  + "Chyba faktycznie dobrze graja, gdyz muzyka, ktora cie otacza przyprawic moze o gesia skorke.\n");
	add_item( ({"spis","spis jadla"}), "Jest to w zasadzie gliniana tabliczka, na ktorej niewielkim "
	  + "patyczkiem wypisano dokladnie wszystkie podawane tu potrawy.\n");

	add_exit("ulica2.c", "poludnie");

	room_add_object(PATH + "scena.c", 1);

	/* Propy, obiekty i npce */
	
	add_prop(ROOM_I_INSIDE,1);

	/* Karczma */
	add_cmd_item(({"spis","spis jadla","tabliczke","gliniana tabliczke"}),({"czytaj","przeczytaj"}), "@@menu");

	add_food("Aromatyczna, pieczona wieprzowine", ({"wieprzowine","aromatyczna wueprzowine", "pieczona wieprzowine"}), 
		     "aromatyczna, pieczona wieprzowine", 200, 70, "aromatyczna, pieczona wieprzowine");

	add_food("Gotowana wolowine", ({"wolowine","gotowana wolowine"}), 
		     "gotowana wolowine", 170, 65, "gotowana wolowine");

	add_food("Wysmienitego, pieczonego dzika", ({"dzika","wysmienitego dzika","pieczonego dzika","wysmienitego, pieczonego dzika"}), 
		     "wysmienitego, pieczonego dzika", 250, 100, "wysmienitego, pieczonego dzika");

	add_drink("koniak",({"koniaku", "kieliszka koniaku"}), ({"koniak", "kieliszek koniaku"}), "miejscowego koniaku",
    100,50,500,PL_MESKI_NOS_NZYW, KIELISZEK, "koniaku", "Jest to niewielki kieliszek, na ktorego powierzchni znajduje sie "
	+ "ornament w ksztalcie smiejacego sie zonglera w stroju blazna.\n");

	add_drink("sok",({"soku", "szklanki soku"}), ({"sok", "szklanke soku","wieloowocowy sok"}), "pysznego, wieloowocowego soku",
    250,0,100,PL_MESKI_NOS_NZYW, SZKLANKA, "soku", "Jest to wysoka szklanka, na ktorej powierzchni znajduje sie "
	+ "ornament w ksztalcie smiejacego sie zonglera w stroju blazna.\n");

	add_drink("wino",({"wina", "kieliszka wina"}), ({"wino", "kieliszek wina"}), "bialego wina",
    100,50,650,PL_NIJAKI_NOS, KIELISZEK, "wina", "Jest to niewielki kieliszek, na ktorego powierzchni znajduje sie "
	+ "ornament w ksztalcie smiejacego sie zonglera w stroju blazna.\n", 1500);

	set_sit("przy stoliku","przy stoliku","od stolika");

	ustaw_karczmarza(NPC_PATH + "dirleden.c");
	add_npcs(NPC_PATH + "mlodzieniec.c", 2);

	/* Eventy */
	set_event_time(180.0,180.0);
	set_events_on(1);
	add_event("Twoje ucho wychwytuje plynace ze sceny piekne dzwieki.\n");
	add_event("Orkiestra na chwile przestaje grac.\n");
	add_event("Wynajeta orkiestra zaczyna grac jakas bardzo skoczna melodie.\n");
	add_event("Na scene wychodza trzy wynajete tancerki.\n");
	add_event("W powietrzu roznosi sie won pieczonego dzika przyrzadzanego przez kucharza.\n");
	add_event("Kolo ciebie przeszedl mezczyzna niosacy tace z kiliszkiem koniaku.\n");
	add_event("Po calej sali rozchodzi sie glosna rozmowa kilku mlodych mezczyzn.\n");
}
string
menu()
{
    say(QCIMIE(this_player(),PL_MIA) + " czyta uwaznie spis jadla i napitku.\n");
    return(
    " _____________________________________\n"+
	"|        Spis jadla i napitku:        |\n"+
	"|                                     |\n"+
	"| * Jadlo:                            |\n"+
	"| > Pieczona wieprzowina,        *70* |\n"+
	"| > Gotowana wolowina,           *65* |\n"+        
	"| > Pieczony dzik.              *100* |\n"+
	"|                                     |\n"+
	"| * Napitek:                          |\n"+
	"| > Miejscowy koniak,            *50* |\n"+
	"| > Biale wino,                  *65* |\n"+
	"| > Sok wieloowocowy.            *25* |\n"+
	"|                                     |\n"+
	" -------------------------------------\n"+
	"| Wszystkie ceny podano w miedziakach |\n"+
	" -------------------------------------\n");
}

