#pragma strict_types

inherit "/d/Faerun/std/karczma.c";

#include <stdproperties.h>
#include "../definicje.h"
#include <macros.h>
#include <pub.h>

void
create_room()
{
	set_short("Czerwony Bukiet");
	set_long("Jest to obszerna i przytulna izba, ktora jest glowna sala gospody 'Czerwony "
	  + "Bukiet'. Zgodnie z wszystkimi plotkami stawia sie w tym miejscu na szybkosc i "
	  + "wydajnosc obslugiwanych klientow, a pono jest to rowniez najwieksza karczma w "
	  + "calym Beregoscie. Dostrzegasz tutaj szereg przeroznych law, przy ktorych w kazdej "
	  + "chwili moglbys spoczac, azeby zjesc cos badz zwilzyc sucha gardziel. Sale dobrze "
	  + "oswietlaja zawieszone przy kazdej scianie swieczniki, zas przytulnosci pomieszczeniu "
	  + "dodaja prawdziwie antyczne meble, ktore swiadcza o tym, iz moze byc to najstarsza "
	  + "tawerna jaka widzialo miasto Beregost. Panuje tutaj nieprzenikniona cisza za "
	  + "co wielu kupcow oraz urzednikow wielbi to miejsce jako dogodna pozycja do zawierania "
	  + "umow i dokonywania transakcji. Na scianie dostrzegasz zawieszona tabliczke stanowiaca "
	  + "miejscowy jadlospis.\n");

	add_item( "meble", "Te prawdziwie antyczne meble swiadczyc moga jedynie o wieku tego miejsca, "
	  + "ktore mogloby sie okazac najstarsza i najwczesniej wzniesiona gospoda w Beregoscie.\n");

	add_item( "lawy", "Ot nieco nisko osadzone na czterech nogach lawy, wykonane z ociosanych "
	  + "drewnianych bali. W kazdej chwili mozesz usiasc przy jednej z nich i zamowic to czego "
	  + "tylko dusza zapragnie o ile nie powstrzyma tych pragnien objetosc twej kiesy.\n");

	add_item( "swieczniki", "Te czteroramienne swieczniki znakomicie oswietlaja sale bedac "
	  + "przymocowanymi do scian za pomoca mocnych, stalowych uchwytow.\n");

	add_item( "tabliczke", "Ta drewniana tabliczka wypisany ma na sobie caly tutejszy "
	  + "jadlospis. Moze wartalo by ja przeczytac?\n");

	add_exit("ulica5.c", "poludnie");

	/* Propy, obiekty i npce */
	
	add_prop(ROOM_I_INSIDE,1);

	/* Karczma */
	add_cmd_item(({"tabliczke","jadlospis"}),({"czytaj","przeczytaj"}), "@@menu");
	
	add_food("Pyszna, domowa konfiture", ({"konfiture","pyszna konfiture", "domowa konfiture","konfitury"}), 
		     "pyszna, domowa konfiture", 180, 45, "pyszna, domowa konfiture");

	add_food("Kilka kromek bialego chleba", ({"chleb","kromki chleba", "bialy chleb"}), 
		     "kilka kromek bialego chleba", 170, 30, "kilka kromek bialego chleba");

	add_food("Talerz z pysznym miesem wieprzowym", ({"wieprzowina","talerz wieprzowiny","mieso","mieso wieprzowe"}), 
		     "talerz z pysznym miesem wieprzowym", 200, 55, "talerz z pysznym miesem wieprzowym");

	add_drink("napoj",({"napoju", "kubka napoju"}), ({"napoj", "kubek napoju"}), "cieplego, rozgrzewajacego, mleczno-miodowego napoju",
    300,0,50,PL_MESKI_NOS_NZYW, KUBEK, "napoju", "Jest to szklany kubek, na ktorego powierzchni znajduje sie "
	+ "symbol gospody 'Czerwony Bukiet' w Beregoscie.");

	add_drink("porter",({"porteru", "kufla porteru"}), ({"porter", "kufel porteru"}), "dlugo wazonego, kupieckiego porteru",
    500,7,60,PL_MESKI_NOS_NZYW, KUFEL,"portera", "Jest to drewniany kufel z porecznym uchwytem. Na jego powierzchni "
	+ "wystruganodokladnie symbol gospody 'Czerwony Bukiet' w Beregoscie.", 2500);

	add_drink("kawa",({"kawy", "filizanka kawy"}), ({"kawe", "filizanke kawy"}), "czarnej niczym noc kawy",
    150,0,150,PL_ZENSKI, FILIZANKA, "kawy", "Jest to nieduza porcelanowa filizanka, na ktorej powierzchni znajduje sie "
	+ "symbol gospody 'Czerwony Bukiet' w Beregoscie.");

	set_sit("przy lawie","przy drewnianej lawie","od drewnianej lawy");

	ustaw_karczmarza(NPC_PATH + "jasmine.c");
	add_npcs(NPC_PATH + "kupiec.c", 1);

	/* Eventy */
	set_event_time(180.0,180.0);
	set_events_on(1);
	add_event("W gospodzie panuje nieprzerwana cisza.\n");
	add_event("Ogniki tancza plomiennie na szczytach czteroramiennych swiecznikow.\n");
	add_event("Ktos upuscil na podloge talerz z konfitura.\n");
	add_event("Ze strony drzwi wpada do pomieszczenia zimne powietrze.\n");
	add_event("W powietrzu roznosi sie pyszna won pieczonego chleba.\n");
	add_event("Kolo ciebie przeszedl mezczyzna niosacy talerz z wieprzowina.\n");
	add_event("Czujesz przyjemny zapach miodowego, rozgrzewajacego napoju.\n");
	add_event("Pewien bywalec stara sie stargowac cene posilku.\n");
	add_event("Ktos wykloca sie glosno o cene noclegu.\n");
}
string
menu()
{
    say(QCIMIE(this_player(),PL_MIA) + " czyta tabliczke.\n");
    return (
    " _____________________________________\n"+
	"|             Jadlospis:              |\n"+
	"|                                     |\n"+
	"| Dostepne jadlo:                     |\n"+
	"| 1. Domowe konfitury,           *45* |\n"+
	"| 2. Bochenek Bialego chleba,    *30* |\n"+        
	"| 3. Mieso wieprzowe.            *55* |\n"+
	"|                                     |\n"+
	"| Dostepny napitek:                   |\n"+
	"| 1. Rozgrzewajacy napoj,        *25* |\n"+
	"| 2. Kupiecki porter,            *30* |\n"+
	"| 3. Mala, czarna kawa.          *15* |\n"+
	"|                                     |\n"+
	" -------------------------------------\n"+
	"| Wszystkie ceny podano w miedziakach |\n"+
	" -------------------------------------\n");
}



