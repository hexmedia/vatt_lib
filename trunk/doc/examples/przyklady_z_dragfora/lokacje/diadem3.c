inherit "/d/std/room";
#include <macros.h>
#include <stdproperties.h>
int wyjdz();
void 
create_room()
{
    set_short("Dom uciech Miedzianego Diademu");
    set_long("Stoisz w obszernym i ciemnym pomieszczeniu. Jest tutaj dosc duszno, a "+ 
        "migajace rozowe swiatlo nieznacznie oswietla cala sale. Slyszysz "+
        "spokojna muzyke w rytm ktorej zmyslowo na dozej drewnianej scenie "+
        "tancza na wpol nagie kobiety. Jezeli jestes samotny i potrzebujesz "+
        "towarzystwa na noc to znalezles sie w odpowiednim miejscu.\n");
    add_prop(ROOM_I_INSIDE,1);
    add_item("scene","Jest to duza drewniana scena na ktorje w rytm muzyki tancza "+
            "prawie nagie kobiety.\n");
            
            set_event_time(65,this_object());
    add_event("Przepiekna zakolczykowana elfka mowi do ciebie: Moze chcesz sie "+
              "zabawic?\n");
    add_event("Slyszysz glosne jeki dochodzace z gornej czesci budynku.\n");
    add_event("Bard zaczyna grac inna melodie.\n");
    add_event("Jakas kobieta wraz z mezczyzna podozaja na gore.\n");
    add_event("Przechodzaca obok Ciebie kobieta upuscila chusteczke poczym szybko "+
              "schylila sie po nia wypinajac sie w Twoja strone.\n");
    
    
    add_exit("diadem2","polnoc");
    add_exit("diadem3","gora", wyjdz);
    
}

int
wyjdz()
{
    write("Niemozesz tam wejsc.\n");
    return 0;
}




void
init()
{
    ::init();
}
