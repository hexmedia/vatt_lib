#pragma strict_types
#pragma no_clone
#include "../def.h"
inherit STD+"room_cand";
inherit "/lib/siedzenie";


void
create_cand_room()
{
    set_short("Areszt w koszarach");

    set_long("Rozgladasz sie po nieszczegolnie przyjemnym miejscu jakim jest cela "
           + "polozona w koszarach twierdzy Candlekeep. Nie znajduje sie tu nic "
	   + "ciekawego za wyjatkiem pryczy, malego stoliczka, a takze dziwnych napisow "
	   + "wyrytych na kazdej ze scian. Ciche dzwieki spadajacych z sufitu kropel "
	   + "odbijaja sie echem po calym pomieszczeniu w momencie gdy te rozbijaja sie "
	   + "trafiajac w twarda, kamienna posadzke.\n");

    add_item("prycze", "Zwykla, drewniana prycza.\n");

    add_item("posadzke", "Oto twarda, kamienna posadzka.\n");

    add_item("stoliczek", "Jest to nieduzy stoliczek, ktory zapewne ma juz swoje lata.\n");

    add_item("sciany", "Kazda ze scian surowego aresztu pokryto dziwnymi napisami. Moze "
           + "warto byloby je obejrzec dokladniej?\n");
 
    add_item("napisy", "Wszystkie wydrapane na scianach napisy dotycza zazwyczaj nedznego "
           + "losu osob, ktore trafily do tego aresztu.\n");
    add_cmd_item("napisy",({"przeczytaj"}),"Wszystkie wydrapane na scianach napisy dotycza zazwyczaj nedznego "+
    "losu osob, ktore trafily do tego aresztu.\n");

    add_prop(ROOM_I_INSIDE, 1);

    add_exit("koszary","drzwi");

    set_sit("na pryczy","na drewnianej pryczy","z pryczy",2);

}
void
leave_inv(object ob, object from)
{
    leave_inv_sit(ob);
    ::leave_inv(ob, from);
}
void
init()
{
    ::init();
    init_sit();
}

