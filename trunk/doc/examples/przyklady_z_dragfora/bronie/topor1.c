#pragma strict_types
inherit "/d/std/bron";
#include "../pal.h"
void
create_weapon()
{
     set_autoload();
       ustaw_nazwe(({"topor","topora","toporowi","topor","toporem", "toporze"}), 
    ({"topory","toporow","toporom","topory","toporami","toporach"}), PL_MESKI_NOS_NZYW);

       
       dodaj_przym("blyszczacy","blyszczacy");
       dodaj_przym("solamnijski","solamnijscy");

        set_long("Jest to dwureczny topor o ostrzu do ktorego dodano jakiejs "
        +"domieszki, powodujacej, ze jest ono bardzo blyszczace. Dosc krotkie "
        +"stylisko owiniete jest jakims materialem zapobiegajacym wyslizgnieciu "
        +"sie topora z dloni podczas walki. Na ostrzu widzisz wypuncowany znak "
        +"Solamnii.\n");

        set_wt(W_AXE);
        set_dt(W_SLASH);
        set_hands(W_BOTH);

        add_prop(OBJ_I_VOLUME, 7000);
        add_prop(OBJ_I_WEIGHT, 9500);

        set_hit(21);
        set_pen(32);
}
