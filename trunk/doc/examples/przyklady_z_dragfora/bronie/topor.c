/*
 * Filename: topor.c
 *
 * Zastosowanie: Topor.
 * 
 * Copyright Volothamp Geddarm.
 *
 */
inherit "/d/std/bron";  

#include <filter_funs.h>
#include <ss_types.h>
#include <wa_types.h>
#include <stdproperties.h>
#include <wa_types.h>
#include <cmdparse.h>
#include <macros.h>
#include <pl.h>
#include <options.h>
#include <macros.h>
#include <formulas.h>
#include <const.h>

#define TP this_player()
object wielder;

    void
    create_weapon()
{
    set_autoload();  
    ustaw_nazwe(({ "topor bojowy", "topora bojowego", "toporowi bojowemu", "topor bojowy", "toporem bojowym", "toporze bojowym" }),
                ({ "topory bojowe", "toporow bojowych", "toporom bojowym", "topory bojowe", "toporami bojowymi", "toporach bojowych" }), PL_MESKI_NOS_NZYW); 
    
	dodaj_nazwy(({ "topor","topora","toporowi","topor","toporem","toporze"}),
		        ({ "topory","toporow","toporom","topory","toporami","toporach"}),PL_MESKI_NOS_NZYW);

	dodaj_przym("kolisty", "kolisci");
	dodaj_przym("obureczny", "obureczni");

    set_long("Spogladasz na masywny topor przystosowany do chwytania oburacz. Ta cecha z "
         + "pewnoscia zwieksza jakosc zadawanych obrazen lecz jednoczesnie sprawia, ze topor "
	  + "jest ciezszy i mniej poreczny. Tego typu bronie cenia sobie nawet krasnoludy "
	  + "zamieszkujace glebokie podziemne twierdze, ktore na kowalstwie znaja sie jak "
	  + "nikt. Glownie ozdobiono w kilku miejscach tajemniczymi symbolami i podobnie "
 + "poczyniono z drewnianym trzonem. Mimo to uwazasz, ze zadna z ozdob nie ma zadnego "
	  + "magicznego i blizej nieokreslonego znaczenia oprocz jednego, ktory bez watpienia "
	  + "jest osobistym znakiem slynnej kuzni polozonej w Beregoscie.\n");
    
	set_hit(37);
    set_pen(27);

	set_wt(W_AXE);
    
	set_dt(W_SLASH);
	set_dt(W_IMPALE);
    
	set_hands(W_BOTH);
    
	add_prop(OBJ_I_WEIGHT, 7500);                
	add_prop(OBJ_I_VALUE, 1450);
}

/*----Podpisano: 'Volothamp Geddarm'----*/


  

