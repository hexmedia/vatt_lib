/*
 * Filename: berdysz.c
 *
 * Zastosowanie: Berdysz.
 * 
 * Copyright Volothamp Geddarm.
 *
 */
inherit "/d/std/bron";  

#include <filter_funs.h>
#include <ss_types.h>
#include <wa_types.h>
#include <stdproperties.h>
#include <wa_types.h>
#include <cmdparse.h>
#include <macros.h>
#include <pl.h>
#include <options.h>
#include <macros.h>
#include <formulas.h>
#include <const.h>

#define TP this_player()
object wielder;

    void
    create_weapon()
{
    set_autoload();   
    
	ustaw_nazwe(({ "berdysz","berdysza","berdyszowi","berdysz","berdyszem","berdyszu"}),
		        ({ "berdysze","berdyszow","berdyszom","berdysze","berdyszami","berdyszach"}),PL_MESKI_NOS_NZYW);

	dodaj_przym("krotki", "krotcy");
	dodaj_przym("stalowy", "stalowi");

    set_long("Spogladasz na masywny buzdygan o morderczym ostrzu i krotkim, drewnianym "
	  + "trzonie. Tego typu bron to oczywiscie skrzyzowanie topora bojowego wraz z "
	  + "dluga straznicza halabarda. Wyglada on bardzo niebezpiecznie i jest uzywany "
	  + "jedynie przez wprawnych wojownikow o szerokim doswiadczeniu bojowym.\n");
    
	set_hit(21);
    set_pen(16);
    
	set_wt(W_AXE);
	
	set_dt(W_SLASH);
	set_dt(W_IMPALE);
    
	set_hands(W_BOTH);
    
	add_prop(OBJ_I_WEIGHT, 7500);                
	add_prop(OBJ_I_VALUE, 450);
}

/*----Podpisano: 'Volothamp Geddarm'----*/


  

