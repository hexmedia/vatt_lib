#pragma strict_types
inherit "/d/std/bron";
#include "../pal.h";

void create_weapon()
{
   set_autoload();
   ustaw_nazwe( ({ "sztylet", "sztyletu", "sztyletowi", "sztylet", "sztyletem",
                   "sztylecie" }), ({ "sztylety", "sztyletow", "sztyletom",
                   "sztylety", "sztyletami", "sztyletach" }), PL_MESKI_NOS_NZYW );
                   
    dodaj_przym("falisty","falisci");
    dodaj_przym("polyskliwy","polyskliwi");
   set_long("Jest to dlugi sztylet o falistym ostrzu, ktore zostalo tak stworzone, "
   +"aby zadawalo rozleglejsze rany oraz powodowalo dosc powaznie obrazenia wewnetrzne. "
   +"Czesto takich sztyletow uzywaja skrytobojcy oraz zlodzieje, gdyz po kilku "
   +"ciosach przeciwnik nie ma szans przezycia.\n");

            set_hit(29);
            set_pen(17);
            
            set_wt(W_KNIFE);
            set_dt(W_IMPALE);
            set_hands(W_ANYH);
             
            add_prop(OBJ_I_WEIGHT, 1000);
            add_prop(OBJ_I_VOLUME, 600);
            add_prop(OBJ_I_VALUE, 660);
            
}
                           
