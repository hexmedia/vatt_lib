/*
 * Filename: miecz.c
 *
 * Zastosowanie: Miecz.
 * 
 * Copyright Volothamp Geddarm.
 *
 */
inherit "/d/std/bron";  

#include <filter_funs.h>
#include <ss_types.h>
#include <wa_types.h>
#include <stdproperties.h>
#include <wa_types.h>
#include <cmdparse.h>
#include <macros.h>
#include <pl.h>
#include <options.h>
#include <macros.h>
#include <formulas.h>
#include <const.h>

#define TP this_player()
object wielder;

    void
    create_weapon()
{
    set_autoload();  
    ustaw_nazwe(({ "miecz", "miecza", "mieczowi", "miecz", "mieczem", "mieczu" }),
                ({ "miecze", "mieczy", "mieczom", "miecze", "mieczami", "mieczach" }), PL_MESKI_NOS_NZYW); 
    
	dodaj_przym("prosty", "prosci");
	dodaj_przym("dlugi", "dludzy");

    set_long("Jest to dlugi miecz przystosowany do chwytania w jedna reke. Wyglada on jednak "
	  + "dosyc przecietnie mimo, iz jego klinge formowal, a rekojesc skladal sam Mistrz kowalstwa "
	  + "z Beregostu o czym swiadczy niewielki znak wykonany na powierzchni ostrza. Owa bronia "
	  + "moznaby jak sadzisz przebijac lekkie pancerze lecz w konfrontacji z mocna zbroja plytowa "
	  + "moze zawiesc.\n");
    
	set_hit(20);
    set_pen(20);
    
	set_wt(W_SWORD);
	
	set_dt(W_SLASH);
	set_dt(W_IMPALE);
    
	set_hands(W_ANYH);
    
	add_prop(OBJ_I_WEIGHT, 2500);                
	add_prop(OBJ_I_VALUE, 450);
}

/*----Podpisano: 'Volothamp Geddarm'----*/


  

