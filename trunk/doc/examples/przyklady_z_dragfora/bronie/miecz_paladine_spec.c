#pragma strict_types
inherit "/d/std/bron";
#include <filter_funs.h>
#include <ss_types.h>
#include <wa_types.h>
#include <stdproperties.h>
#include <wa_types.h>
#include <cmdparse.h>
#include <macros.h>
#include <pl.h>
#include <options.h>
#include <macros.h>
#include <formulas.h>
#include <const.h>

#define CO_ILE 8
#define TP this_player()
#define TO this_object()

int ktory_cios;
object wielder;

void create_weapon()
{
    set_autoload();
   ustaw_nazwe(({ "claymore", "claymora", "claymorowi", "claymore", "claymorem", "claymorze" }),
               ({ "claymory", "claymorow", "claymorom", "claymory", "claymorami", "claymorach" }),
               PL_MESKI_NOS_NZYW);
  
   dodaj_nazwy(({ "miecz", "miecza", "mieczowi", "miecz", "mieczem", "mieczu" }),
               ({ "miecze", "mieczy", "mieczom", "miecze", "mieczami", "mieczach" }),
               PL_MESKI_NOS_NZYW);

    dodaj_przym("dwusieczny","dwusieczni");
    dodaj_przym("zlocisty","zlocisci");
   
    set_long("Ten dlugi na prawie trzy stopy dwusieczny miecz o prostym i szerokim ostrzu "+
             "posiada plytkie zbrocze. Ostrze dziwnie, zlociscie poblyskuje, a ramiona jelca "
             +"zostaly odchylone w strone glowni, a rekojesc jest na tyle dluga i wygodna "+
             "by zapewnic pewny chwyt. Polkolista glowica zabezpiecza przed wysuwaniem sie miecza "
             +"z dloni.\n");
   
            set_hit(25);
            set_pen(30);
            add_prop(OBJ_I_IS_MAGIC_WEAPON, 1);
            
            set_wt(W_SWORD);
            set_hands(W_ANYH);
            set_dt(W_SLASH);
                      
            add_prop(OBJ_I_WEIGHT, 8500);
            add_prop(OBJ_I_VOLUME, 6000);
}

int did_hit(int aid, string hdesc, int phurt, object enemy, int dt, int phit, int dam)
{
        object *ja = FILTER_LIVE(all_inventory(environment(this_player()))) - ({ this_player() });
        int i;
        mixed* rezultat;
        string jak;
        ::did_hit();
        
        if (ktory_cios < CO_ILE) ktory_cios++;
        
        if ((this_player()->query_stat(SS_DEX) > 60) && (ktory_cios == CO_ILE))
        {
         rezultat=enemy->hit_me( 2* ( ( this_player()->query_stat(SS_DEX) + this_player()->query_stat(SS_WIS) ) ), 50, W_SLASH, ja, -1);
         ja -= ({enemy});
         
         jak = "prawie niezauwazalnie";
         if (rezultat[0] > 5) jak = "nieznacznie";
         if (rezultat[0] > 8) jak = "bardzo lekko";
         if (rezultat[0] > 10) jak = "lekko";
         if (rezultat[0] > 15) jak = "widocznie";
         if (rezultat[0] > 20) jak = "dosc mocno";
         if (rezultat[0] > 25) jak = "mocno";
         if (rezultat[0] > 30) jak = "dotkliwie";
         if (rezultat[0] > 40) jak = "powaznie";
         if (rezultat[0] > 50) jak = "ciezko";
         if (rezultat[0] > 55) jak = "bardzo ciezko"; 
         if (rezultat[0] > 60) jak = "poteznie";
         
         if (enemy->query_hp() <= 0)
         {
                this_player()->catch_msg("Z ostrza twojego miecza znow daje sie "
        	+"uslyszec dziwne buczenie. Rekojesc jakby przykleja sie do twojej reki, a miecz "
        	+"zaczyna prowadzic twoja dlon.Ze zdziwieniem spogladasz na bron, ktora wiedziona "
        	+"jakas nadludzka sila celnie uderza przeciwnika. Kropelki krwi wraz z okruchami "
        	+"zmiazdzonych kosci wylatuja w powietrze, tworzac makabryczny widok.\n"
        	+"Po chwili twoj przeciwnik pada na ziemie w kaluzy krwi.\n");
        	
                enemy->catch_msg("Nagle z ostrza miecza " + QIMIE(this_player(), PL_DOP) 
        	+" znow daje sie uslyszec dziwne buczenie. Zdziwion" + this_player()->koncowka("y","a")
        	+" jakby tym faktem " + QIMIE(this_player(),PL_MIA) + " spoglada na bron, ktora "
        	+"celnie i perfekcyjnie przeszywa twoje cialo. Slyszysz chrzest lamanych kosci i "
        	+"widzisz tylko zapadajaca ciemnosc i niesamowity, lsniacy zlociscie claymore w "
        	+"rekach twojego przeciwnika.\nPotem zapada ciemnosc...\n");
                
                for(i=0; i<sizeof(ja); i++)
                {
                  	ja[i]->catch_msg("Nagle z ostrza miecza " + QIMIE(this_player(), PL_DOP)
        		+" znow daje sie uslyszec dziwne buczenie. Zdziwion" + this_player()->koncowka("y","a")
        		+" jakby tym faktem " + QIMIE(this_player(),PL_MIA) + " spoglada na bron, ktora "
        		+"celnie i perfekcyjnie przecina cialo przeciwnika. W powietrze wylatuja okruchy"
        		+"zmiazdzonych kosci pomieszane z kropelkami krwi tworzac makabryczny widok.\n"
        		+QIMIE(enemy,PL_MIA)+" pada na kolana, chwieje sie chwile "
        		+"po czym upada na ziemie w kaluzy krwi.\n"); 
        	}
        	
        	enemy->do_die(this_player());
                return 1;
         
         }
         
         enemy->catch_msg("Nagle z ostrza miecza " + QIMIE(this_player(), PL_DOP)
         +" znow daje sie uslyszec dziwne buczenie. Zdziwion" + this_player()->koncowka("y","a")
         +" jakby tym faktem " + QIMIE(this_player(),PL_MIA) + " spoglada na bron, ktora "
         +"przeszywa twoje cialo raniac cie "+jak+".\n");
         
         this_player()->catch_msg("Z twojego miecza znow zaczyna dochodzic dziwne buczenie. "
         +"Rekojesc przykleja sie do twojej reki, a miecz zaczyna prowadzic twoja dlon.\n"
         +"Z niejakim zdziwieniem spogladasz na bron, ktora trafia przeciwnika, raniac go "
         +jak+".\n");
         
         for(i=0; i<sizeof(ja); i++)
         {
                ja[i]->catch_msg("Nagle z ostrza miecza " + QIMIE(this_player(), PL_DOP)
        	+" znow daje sie uslyszec dziwne buczenie. Zdziwion" + this_player()->koncowka("y","a")
        	+" jakby tym faktem " + QIMIE(this_player(),PL_MIA) + " spoglada na bron, ktora "
        	+"przeszywa cialo "+QIMIE(enemy,PL_DOP)+" raniac "
        	+enemy->koncowka("go", "ja")+" " + jak + ".\n");
         }
         
         return 2;
       
       }
       
       return 0;

}
mixed wield(object what)
{
   set_alarm(0.1,0.0,"add_wielder");
   
   this_player()->set_skill_extra(0, 30);
   this_player()->set_skill_extra(23, 30);
   
   write("Gdy dobywasz miecza, ostrze rozblyskuje dziwnym, magicznym blaskiem, ktore na chwile "
   +"cie oslepia i wydaje z siebie dziwne buczenie. Po chwili jednak wszystko wraca do normy.\n");
   say(QCIMIE(this_player(),PL_MIA)+" dobywa claymora i unosi go w gore. Ostrze rozjarza sie dziwnym blaskiem, "
   +"a w oczach " + QCIMIE(this_player(),PL_DOP) + " zauwazasz fanatyczny obled.\nPo chwili jednak wszystko wraca do normy.\n");
   
   return 1;
}

void add_wielder()
{
    wielder = environment(this_object());
}

object query_wielder()
{
    return wielder;
}

mixed unwield(object what)
{
   set_this_player(wielder);
   
   if(environment(this_object())!=wielder) return 1;
   
   TP->set_skill_extra(0, 0);
   TP->set_skill_extra(23, 0);
   
   write("Opuszczasz claymora, i czujesz, jakby cos wyrywalo czesc ciebie. Pragniesz ponownie dobyc miecza, "
   +"aby moc walczyc.\n");
   say(QCIMIE(this_player(),PL_MIA)+" opuszcza claymora. W " + this_player()->koncowka("jego","jej") + "oczach zauwazasz "
   +"nieopisany smutek, a " + this_player()->koncowka("jego","jej") + " dlonie jakby wiedzione jakas nadludzka sila same "
   +"wedruja w kierunku rekojesci.\n. Po chwili jednak " + QCIMIE(this_player(),PL_MIA) + " otrzasa sie i "
   +"uspokaja.\n");     
   
   return 1;
}