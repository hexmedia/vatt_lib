/*
 * Filename: wekiera.c
 *
 * Zastosowanie: Wekiera.
 * 
 * Copyright Volothamp Geddarm.
 *
 */
inherit "/d/std/bron";  

#include <filter_funs.h>
#include <ss_types.h>
#include <wa_types.h>
#include <stdproperties.h>
#include <wa_types.h>
#include <cmdparse.h>
#include <macros.h>
#include <pl.h>
#include <options.h>
#include <macros.h>
#include <formulas.h>
#include <const.h>

#define TP this_player()
object wielder;

    void
    create_weapon()
{
    set_autoload();  
    ustaw_nazwe(({ "wekiera", "wekiery", "wekierze", "wekiere", "wekiera", "wekierze" }),
                ({ "wekiery", "wekier", "wekierom", "wekiery", "wekierami", "wekierach" }), PL_ZENSKI); 

	dodaj_przym("jednoreczny", "jednoreczni");
	dodaj_przym("kaplanski", "kaplanscy");

    set_long("Spogladasz na przystosowana do chwytania w jednej rece bron znana powszechnie "
	  + "jako wekiera. Musisz jednak wiedziec, ze prawdopodobnie wojownik nia wladajacy "
	  + "posiada pewna opatrznosc bostw, gdyz tego typu bronie przeznaczone sa szczegolnie "
      + "dla kaplanow, ktorzy rowniez wojaczka sie paraja. To zas oznacza, ze owa wekiera "
	  + "musiala jeszcze niedawno zostac poswiecona, totez mimo swego lekkiego ciezaru "
	  + "prawdopodobnie zadaje niemale obrazenia. Na jej trzonie dostrzegasz niewielki "
	  + "symbol bedacy osobistym znakiem slynnej kuzni polozonej w Beregoscie.\n");
    
	set_hit(37);
    set_pen(34);
    
	set_wt(W_CLUB);

	set_dt(W_BLUDGEON);
    
	set_hands(W_ANYH);
    
	add_prop(OBJ_I_WEIGHT, 3000);                
	add_prop(OBJ_I_VALUE, 1750);
}

/*----Podpisano: 'Volothamp Geddarm'----*/


  

