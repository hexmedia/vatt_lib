/*
 * Filename: sztylet.c
 *
 * Zastosowanie: Sztylet.
 * 
 * Copyright Volothamp Geddarm.
 *
 */
inherit "/d/std/bron";  

#include <filter_funs.h>
#include <ss_types.h>
#include <wa_types.h>
#include <stdproperties.h>
#include <wa_types.h>
#include <cmdparse.h>
#include <macros.h>
#include <pl.h>
#include <options.h>
#include <macros.h>
#include <formulas.h>
#include <const.h>

#define TP this_player()
object wielder;

    void
    create_weapon()
{
    set_autoload();  
    
	ustaw_nazwe(({ "sztylet","sztyletu","sztyletowi","sztylet","sztyletem","sztylecie"}),
		        ({ "sztylety","sztyletow","sztyletom","sztylety","sztyletami","sztyletach"}),PL_MESKI_NOS_NZYW);

	dodaj_przym("lsniacy", "lsniacy");
	dodaj_przym("jednosieczny", "jednosieczni");

    set_long("Spogladasz na szczegolny rodzaj sztyletu o ostrzu, ktore przystosowane "
	  + "jest do zadawania ciosow jedynie jedna strona. Rekojesc wykonana jest starannie "
	  + "podobnie jak krociutka klinga. Jest jednak oczywiste, ze takie noze sluza "
	  + "do zadawania obrazen bardziej opancerzonym przeciwnikom, ktorzy nie spodziewaja "
	  + "sie rozpatrzenia w ich uzbrojeniu luki stosownej do wyprowadzenia ataku.\n");
    
	set_hit(7);
    set_pen(12);

	set_wt(W_KNIFE);
    
	set_dt(W_SLASH);
	set_dt(W_IMPALE);
    
	set_hands(W_ANYH);
    
	add_prop(OBJ_I_WEIGHT, 400);                
	add_prop(OBJ_I_VALUE, 250);
}

/*----Podpisano: 'Volothamp Geddarm'----*/


  

