/*
 * Filename: mlot_kowalski.c
 *
 * Zastosowanie: Mlot kowalski.
 * 
 * Copyright Volothamp Geddarm.
 *
 */
inherit "/d/std/bron";  

#include <filter_funs.h>
#include <ss_types.h>
#include <wa_types.h>
#include <stdproperties.h>
#include <wa_types.h>
#include <cmdparse.h>
#include <macros.h>
#include <pl.h>
#include <options.h>
#include <macros.h>
#include <formulas.h>
#include <const.h>

#define TP this_player()
object wielder;

    void
    create_weapon()
{
    set_autoload();  
    ustaw_nazwe(({ "mlot kowalski", "mlota kowalskiego", "mlotowi kowalskiemu", "mlot kowalski", "mlotem kowalskim", "mlocie kowalskim" }),
                ({ "mloty kowalskie", "mlotow kowalskich", "mlotom kowalskim", "mloty kowalskie", "mlotami kowalskimi", "mlotach kowalskich" }), PL_MESKI_NOS_NZYW); 
    
	dodaj_nazwy(({ "mlot","mlota","mlotowi","mlot","mlotem","mlocie"}),
		        ({ "mloty","mlotow","mlotom","mloty","mlotami","mlotach"}),PL_MESKI_NOS_NZYW);

	dodaj_przym("masywny", "masywni");

    set_long("Jest to wykonany z zelaza typowy mlot kowalski jaki stosowany jest przez doswiadczonych i silnych kowali, "
	  + "znajacych swoj rzemieslniczy kunszt od podszewki. Wyglada on na dosyc ciezki lecz poreczny. "
	  + "Nawet, iz nie jest on w pewnym sensie wyrobem kuzni posiada na trzonie jej osobisty "
	  + "i jak widac nieodzowny znak.\n");
    
	set_hit(27);
    set_pen(27);
    
	set_wt(W_WARHAMMER);

	set_dt(W_BLUDGEON);
    
	set_hands(W_ANYH);
    
	add_prop(OBJ_I_WEIGHT, 4500);                
	add_prop(OBJ_I_VALUE, 100);
}

/*----Podpisano: 'Volothamp Geddarm'----*/


  

