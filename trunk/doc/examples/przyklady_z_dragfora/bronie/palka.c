/*
 * Filename: palka.c
 *
 * Zastosowanie: Palka.
 * 
 * Copyright Volothamp Geddarm.
 *
 */
inherit "/d/std/bron";  

#include <filter_funs.h>
#include <ss_types.h>
#include <wa_types.h>
#include <stdproperties.h>
#include <wa_types.h>
#include <cmdparse.h>
#include <macros.h>
#include <pl.h>
#include <options.h>
#include <macros.h>
#include <formulas.h>
#include <const.h>

#define TP this_player()
object wielder;

    void
    create_weapon()
{
    set_autoload();   
    
	ustaw_nazwe(({ "palka","palki","palce","palke","palka","palce"}),
		        ({ "palki","palek","palkom","palki","palkami","palkach"}),PL_ZENSKI);

	dodaj_przym("dlugi", "dludzy");
	dodaj_przym("debowy", "debowi");

    set_long("Ot zwykla debowa palka, ktorej zaleta jest to, ze dobrze wytrenowany "
	  + "bojownik moze za jej pomoca ogluszyc przeciwnika podczas walki. Jest ona "
	  + "dlugiego lecz starannego wykonania i jak sadzisz taka bronia posluguja "
	  + "sie zazwyczaj drobni rozbojnicy, wartownicy, nieduze humanoidalne kreatury "
	  + "lub praktykujacy giermkowie doswiadczonych i zaprawionych w boju rycerzy.\n");
    
	set_hit(11);
    set_pen(9);
    
	set_wt(W_CLUB);

	set_dt(W_BLUDGEON);
    
	set_hands(W_ANYH);
    
	add_prop(OBJ_I_WEIGHT, 2000);                
	add_prop(OBJ_I_VALUE, 200);
}

/*----Podpisano: 'Volothamp Geddarm'----*/


  

