/*
 * Filename: wlocznia.c
 *
 * Zastosowanie: Wlocznia.
 * 
 * Copyright Volothamp Geddarm.
 *
 */
inherit "/d/std/bron";  

#include <filter_funs.h>
#include <ss_types.h>
#include <wa_types.h>
#include <stdproperties.h>
#include <wa_types.h>
#include <cmdparse.h>
#include <macros.h>
#include <pl.h>
#include <options.h>
#include <macros.h>
#include <formulas.h>
#include <const.h>

#define TP this_player()
object wielder;

    void
    create_weapon()
{
    set_autoload();   
    
	ustaw_nazwe(({ "wlocznia","wloczni","wloczni","wlocznie","wlocznia","wloczni"}),
		        ({ "wlocznie","wloczni","wloczniom","wlocznie","wloczniami","wloczniach"}),PL_ZENSKI);

	dodaj_przym("prosty", "prosci");
	dodaj_przym("drewniany", "drewniani");

    set_long("Sadzisz, ze ta naprawde prymitywna bronia malo kto moglby naprawde zadac "
	  + "obrazenia o ile nie jest doswiadczonym i zaprawionym w boju zbrojnym. Tymczasem "
	  + "masz przed soba zwykla wlocznie prostego wykonania, o dlugim trzonie i nieco "
	  + "tepym ostrzu. Sluzy ona bardziej do prowadzenia walki w zwarciu anizeli dla "
	  + "dalekich rzutow.\n");
    
	set_hit(9);
    set_pen(12);

	set_wt(W_POLEARM);
    
	set_dt(W_SLASH);
	set_dt(W_IMPALE);
    
	set_hands(W_ANYH);
    
	add_prop(OBJ_I_WEIGHT, 3000);                
	add_prop(OBJ_I_VALUE, 150);
}

/*----Podpisano: 'Volothamp Geddarm'----*/


  

