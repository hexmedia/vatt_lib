#pragma strict_types

#include <pl.h>
#include <stdproperties.h>
#include <ss_types.h>

inherit "/d/Faerun/std/room.c";

#define PL_BANDYTA ({ "bandyta", "bandyty", "bandycie", "bandyte", "bandyta", "bandycie" }), ({ "bandyci", "bandytow", "bandytom", "bandytow", "bandytami", "bandytach"}), PL_MESKI_OS
#define PL_BANDYTKA ({ "bandytka", "bandytki", "bandytce", "bandytke", "bandytka", "bandytce" }), ({ "bandytki", "bandytek", "bandytkom", "bandytki", "bandytkami", "bandytkach"}), PL_ZENSKI

#define BAN_I_DOODDANIA "_ban_i_dooddania"
#define BAN_I_UCIEKAL "_ban_i_uciekal"
#define BAN_I_ALARM_ID "_ban_i_alarm_id"
#define BAN_I_UCIEKL "_ban_s_uciekl_" + file_name(this_object())

int dodaj_bandyte(string *prasa, string *mrasa, int rodz, string *rprzym, string *pprzym, string *dprzym, string dl, int bandit);
int ustaw_szanse(int ile);
int ustaw_proc(int ile);
int stworz_bandytow();
int usun (object co);
int sprawdz ();
int start_bandytow();
int blokuj (string a);
int wez_przedmiot (object co, object czyj);
int wypowiedz (object ob, int tryb);
int odliczanie(object ob, int faza);
int testuj_wyjscie(object czyje);
int atak (string b);
int zyja();
int ustaw_teksty(string *nowe);
mixed *wylosuj_rase();
string *losowe_przym(int ile);
int wyjmij (string c);
int search (string d);
int wypisz (object kto, int txt);
int ustaw_staty(int *nowe);
int ustaw_umy(int *nowe);
object wylosuj();

object schowek;
object *bandyci = ({});
string *dane = ({});
int ilu = 0, prawd = 50, proc = 20;
int *staty = ({30, 30, 30, 25, 25, 35});
int *umy = ({30, 30, 30});
string *teksty = ({"Dawnom sie nie bil. Moze teraz bede mial okazje.",
				   "Odloz wszystko co masz! Masz 5 minut...",
				   "Spadaj stad! I lepiej nie wracaj!",
				   "Skoro nadal dajesz to bedziemy brac.",
				   "Dobra. Mozesz isc. Lepiej nie wracaj",
				   "Dobrze. Dawaj dalej.",
				   "Uwazaj bo bede musial Ci zrobic krzywde!",
				   "Sam tego chicales!",
				   "Nie gadaj tylko dawaj kosztownosci!\n",
				   "Nagle bandyci wyskakuja ze swej kryjowki!"});

void
reset_room()
{
	if (sprawdz()) stworz_bandytow();
	::reset_room();
}

void
init()
{
	::init();
	add_action("blokuj", "", 1);
	add_action("atak","zabij");
	add_action("search", "szukaj");
	add_action("wyjmij", "wyjmij");
}

void
enter_inv(object ob, object from)
{
	int i = 0, sz = 0, count = 0;
	object *przedm;

	::enter_inv(ob, from);

	if(!ilu || !prawd)
		return;
	
	if(!zyja())
		return;

	if(living(ob))
	{
		if(ob->query_prop(BAN_I_UCIEKL) != 1)
		{
			if(!ob->query_wiz_level() && (member_array(ob, bandyci) == -1))
			{
				przedm = deep_inventory(ob);
				for(i = 0, sz = sizeof(przedm) ; i < sz ; i++)
				{
					count += przedm[i]->query_prop(OBJ_I_VALUE);
				}
				count = ftoi(itof(count) * (itof(proc)/itof(100)));
				if((count > 0) && (ob->query_prop(BAN_I_UCIEKL) == 0))
				{
					ob->add_prop(BAN_I_DOODDANIA, count);
					ob->add_prop(BAN_I_UCIEKAL, 0);
					set_alarm(1.0, 0.0, "wypowiedz", ob, 1);
					ob->add_prop(BAN_I_ALARM_ID, set_alarm(60.0, 0.0, "odliczanie", ob, 1));
				}
				else
				{
					set_alarm(1.0, 0.0, "wypowiedz", ob, 2);
				}
			}
		}
		else
		{
			wypowiedz(ob, 7);
			set_alarm(0.5, 0.0, "odliczanie", ob, 5);
		}
	}
	else
		wez_przedmiot(ob, from);
}

int
wez_przedmiot (object co, object czyj)
{
	int count;
	object wykonawca;

	count = czyj->query_prop(BAN_I_DOODDANIA);
	wykonawca = wylosuj();
	
	if(czyj->query_prop(BAN_I_UCIEKL) == 1)
	{
		write(capitalize(wykonawca->short()) + " chowa " + co->short(PL_BIE) + ".\n");
		say(capitalize(wykonawca->short()) + " chowa " + co->short(PL_BIE) + ".\n");
		usun(co);
		return 1;
	}
	else
	{
		if(co->query_prop(OBJ_I_VALUE) > 0)
		{
			if(count <= 0)
				wypowiedz(czyj, 3);
			else
			{
				count -= co->query_prop(OBJ_I_VALUE);
				czyj->add_prop(BAN_I_DOODDANIA, count);
				if(count <= 0)
				{
					wypowiedz(czyj, 4);
					remove_alarm(czyj->query_prop(BAN_I_ALARM_ID));
					czyj->add_prop(BAN_I_UCIEKL, 2);
				}
				else
					wypowiedz(czyj, 5);
			}
			write(capitalize(wykonawca->short()) + " chowa " + co->short(PL_BIE) + ".\n");
			say(capitalize(wykonawca->short()) + " chowa " + co->short(PL_BIE) + ".\n");
			usun(co);
		}
	}
	return 1;
}

int
wypowiedz (object ob, int tryb)
{

	object wykonawca;
	wykonawca = wylosuj();
	
	if(!objectp(ob))
		return 0;
	if(!tryb)
		return 0;

	switch(tryb)
	{
		case 1:
			wykonawca->command("powiedz do " + ob->query_name(PL_BIE) + " " + teksty[tryb]);
			break;
		case 2:
			wykonawca->command("powiedz do " + ob->query_name(PL_BIE) + " " + teksty[tryb]);
			break;
		case 3:
			wykonawca->command("powiedz do " + ob->query_name(PL_BIE) + " " + teksty[tryb]);
			break;
		case 4:
			wykonawca->command("powiedz do " + ob->query_name(PL_BIE) + " " + teksty[tryb]);
			break;
		case 5:
			wykonawca->command("powiedz do " + ob->query_name(PL_BIE) + " " + teksty[tryb]);
			break;
		case 6:
			wykonawca->command("powiedz do " + ob->query_name(PL_BIE) + " " + teksty[tryb]);
			break;
		case 7:
			wykonawca->command("powiedz do " + ob->query_name(PL_BIE) + " " + teksty[tryb]);
			break;
		default:
			wykonawca->command("powiedz do " + ob->query_name(PL_BIE) + " " + teksty[0]);
			wykonawca->command("umiechnij sie pskudnie do " + ob->query_name(PL_BIE));
			break;
	}
	return 1;
}

int
odliczanie(object ob, int faza)
{
	object wykonawca;
	wykonawca = wylosuj();
	
	if(!objectp(ob))
		return 0;
	
	switch(faza)
	{
		case 1:
			wykonawca->command("powiedz do " + ob->query_name(PL_BIE) + " Masz jeszcze 4 minuty. Dawaj!");
			set_alarm(60.0, 0.0, "odliczanie", ob, 2);
			break;
		case 2:
			wykonawca->command("powiedz do " + ob->query_name(PL_BIE) + " Masz jeszcze 3 minuty. Zaczynasz mi dzialac na nerwy!");
			set_alarm(60.0, 0.0, "odliczanie", ob, 3);
			break;
		case 3:
			wykonawca->command("powiedz do " + ob->query_name(PL_BIE) + " Masz jeszcze 2 minuty. Dawaj bo tego nie przezyjesz!");
			set_alarm(60.0, 0.0, "odliczanie", ob, 4);
			break;
		case 4:
			wykonawca->command("powiedz do " + ob->query_name(PL_BIE) + " Masz jeszcze 1 minute. Wrrr!");
			set_alarm(60.0, 0.0, "odliczanie", ob, 5);
			break;
		case 5:
			wykonawca->command("powiedz do " + ob->query_name(PL_BIE) + " Koniec z toba!");
			bandyci->attack_object(ob);
			break;
		default:
			break;
	}
	return 1;
}

int
atak (string b)
{
	object kogo;

	if(!zyja())
		return 0;

	if(this_interactive()->query_wiz_level())
		return 0;

	if(!strlen(b))
		return 0;

	parse_command(b, this_object(), "%l:" + PL_BIE, kogo);

	if(member_array(kogo[kogo[0]], bandyci) == -1)
		return 0;

	remove_alarm(this_interactive()->query_prop(BAN_I_ALARM_ID));
	this_interactive()->add_prop(BAN_I_UCIEKL, 1);
	odliczanie(this_interactive(), 5);
	return 0;
}

int
search (string d)
{
	if(zyja())
	{
		write("Szukanie czegokolwiek gdy zyja bandyci nie jest dobrym pomyslem.\n");
		return 1;
	}
	
	if(!stringp(d))
	{
		set_alarm(3.0, 0.0, "wypisz", this_interactive(), 0);
		return 0;
	}

	if((d == "lupow") || (d == "przedmiotow") || (d == "kosztownosci") || (d == "zrabowanych przedmiotow"))
	{
		saybb(capitalize(this_interactive()->short()) + " zaczyna szukac " + d + ".\n");
		write("Zaczynasz szukac " + d + ".\n");
		set_alarm(3.0, 0.0, "wypisz", this_interactive(), 1);
		return 1;
	}
	return 0;
}

int
wypisz(object kto, int txt)
{
	object *zawartosc;
	int i, sz;
	string temp;

	zawartosc = all_inventory(schowek);

	if(sizeof(zawartosc) == 0)
	{
		if(txt)
			write("Nie udalo ci sie znalezc niczego interesujacego.\n");
		return 1;
	}
	else
	{
		if(txt)
		{
			saybb(capitalize(kto->short()) + " znalazl jakies przedmioty!\n");
			temp = "Udalo Ci sie znalezc:\n";
		}
		else
			temp = "Udalo Ci sie znalezc skrytke bandytow a w niej:\n";

		for(i = 0, sz = sizeof(zawartosc) ; i < sz ; i++)
		{
			if(i == 0)
				temp += capitalize(zawartosc[i]->short(PL_BIE));
			else
				temp += zawartosc[i]->short(PL_BIE);
			if(i < (sz-2))
				temp += ", ";
			if(i == sz-2)
				temp += " i ";
		}
		temp += ".\n";
		write (temp);
		write("Mozesz wyjac te rzeczy ze schowka.\n");
		if(!txt)
			write("Ponadto...\n");
		return 1;
	}
}

int
wyjmij (string c)
{
	object *co;
	int i, sz;
	string temp;

	if(zyja())
		return 0;

	if(!parse_command(c, schowek, "%i 'ze' 'schowka'", co))
	{
		write("Nie ma takiej rzeczy w schowku bandytow.\n");
		return 1;
	}
	
	if(co[0] == 0)
		co -= ({schowek});

	temp = "Wyjmujesz ";
	for(i = 1, sz = sizeof(co) ; i < sz ; i++)
	{
		temp += co[i]->short(PL_BIE);
		if(i < sz-2)
			temp += ", ";
		if(i == sz-2)
			temp += " i ";
	}
	temp += " ze schowka.\n";

	write(temp); 
	(co - ({co[0]}))->move(this_interactive());

	return 1;
}

int
usun (object co)
{
	return co->move(schowek, 1);
}

int
testuj_wyjscie(object czyje)
{
	int i = 0, zlicz = 0;

	for (i = 0 ; i < ilu ; i++)
	{
		zlicz += (bandyci[i]->query_stat(1))/2;
	}

	if((czyje->query_stat(1) + random(30)) > zlicz)
	{
		write("Udaje Ci sie wyrwac bandytom!\n");
		remove_alarm(czyje->query_prop(BAN_I_ALARM_ID));
		return 0;	
	}
	write(capitalize(wylosuj()->short()) + " zagradza Ci droge.\n");
	return 1;
}

int
sprawdz()
{
	if(random(100) < prawd) return 1;
	return 0;
}

int
blokuj (string a)
{
	string *zadne;
	string *test;
	string cmd;

	if(!zyja())
		return 0;

	if(this_interactive()->query_wiz_level()) return 0;

	if(this_interactive()->query_prop(BAN_I_DOODDANIA) <= 0) return 0;

	zadne = ({"zakoncz", "ukryj"});
	
	test = ({"goto","home"}) + query_exit_cmds();
	
	cmd = query_verb();

	if(member_array(cmd, zadne) != -1)
	{
		if(cmd == zadne[0])
			write("Spanie w trakcie napadu to nie jest dobry pomysl.\n");
		if(cmd == zadne[1])
		{
			if(a =="sie")
				write("Ukrywanie sie w trakcie napadu to nie jest dobry pomysl.\n");
			else
				write("Bandyci zauwaza ze cos chowasz.\n");
		}
		return 1;
	}
	if(member_array(cmd, test) != -1)
	{
		if((this_interactive()->query_prop(BAN_I_UCIEKAL) == 0) && (this_interactive()->query_prop(BAN_I_UCIEKL) != 1))
		{
			if(this_interactive()->query_prop(BAN_I_UCIEKL) == 2)
				return 0;
			wypowiedz(this_interactive(), 6);
			this_interactive()->add_prop(BAN_I_UCIEKAL, 1);
			return 1;
		}	
		else
		{
			if(this_interactive()->query_prop(BAN_I_UCIEKL) == 0)
			{
				wypowiedz(this_interactive(), 7);
				set_alarm(1.0, 0.0, "odliczanie", this_interactive(), 5);
			}
			this_interactive()->add_prop(BAN_I_UCIEKL, 1);
			return testuj_wyjscie(this_interactive());
		}
	}
	return 0;
}

int
zyja()
{
int i = 0, lv = 0;

	if(!ilu || !prawd)
		return 0;

	if(!sizeof(bandyci))
		return 0;
	
	for(i = 0 ; i < ilu ; i++)
		if(bandyci[i] == 0)
			lv++;
	
	if(lv == ilu)
		return 0;
	return 1;
}

int
ustaw_szanse(int ile)
{
	if((ile < 0) || (ile > 100))
		return 0;
	prawd = ile;
	return 1;
}

int
ustaw_proc(int ile)
{
	if((ile <= 0) || (ile > 100))
		return 0;
	proc = ile;
	return 1;
}

int
ustaw_teksty(string *nowe)
{
	int i = 0;

	if(sizeof(nowe) != 10)
		return 0;
	teksty = nowe;
	return 1;
}

int
ustaw_staty(int *nowe)
{
	if(nowe == 0)
		return 0;
	if(sizeof(nowe) != 6)
		return 0;

	staty = nowe;
	return 1;
}

int
ustaw_umy(int *nowe)
{
	if(nowe == 0)
		return 0;
	if(sizeof(nowe) != 3)
		return 0;

	umy = nowe;
	return 1;
}

int
start_bandytow()
{
	if(sprawdz())
	{
		stworz_bandytow();
		return 1;
	}
	return 0;
}

object
wylosuj()
{
	object bandyta;
	int done = 1;
	
	if(!zyja())
		return 0;

	while(done)
	{
		bandyta = bandyci[random(sizeof(bandyci))];
		if(objectp(bandyta))
			done = 0;
	}
	return bandyta;
}

varargs int
dodaj_bandyte(string *prasa = 0, string *mrasa = 0, int rodz = 0, string *rprzym = 0, string *pprzym = 0, string *dprzym = 0, string dl = 0, int bandit = 1)
{
	string temp;

	if((prasa == 0) || (mrasa == 0) || (rodz == 0) || (rprzym == 0))
	{
		mixed *rasa;

		rasa = wylosuj_rase();
		prasa = rasa[0];
		mrasa = rasa[1];
		rodz = rasa[2];
		rprzym = rasa[3];
	}
	if(pprzym == 0)
	{
		pprzym = losowe_przym(1);
	}
	if(dprzym == 0)
	{
		dprzym = losowe_przym(2);
	}
	if(dl == 0)
		dl = "Obserwujesz wlasnie typowego bandyte jaki w grupach grasuje na drogach i okrada bogatych."+
			 "Ow osobnik dawno temu wyparl sie zwierzchnictwa swego wladcy i odwrocil sie do niego stajac"+
			 "sie po prostu zwyklym banita. Aktualnie czycha on na twe pieniadze oraz reszty przejezdzajacych"+
		     "tedy ludzi. Dobrze uzbrojony zastawia droge kazdej osobie, ktora zechce opuscic to miejsce "+
		     "zanim rzuci swe kosztownosci na ziemie.\n";


	temp = (dl + "|" + implode(pprzym, ":") + "|" + implode(dprzym, ":") + "|" + implode(rprzym, ":") + "|" + implode(prasa, ":") + "|" + implode(mrasa, ":") + "|" + rodz + "|" + bandit);
	dane += ({temp});
	ilu++;
	return 1;
}

int
stworz_bandytow()
{
	int i;
	string *elem;

	if (!ilu) return 1;

	if(!sizeof(bandyci)) bandyci = allocate(ilu);

	if(!objectp(schowek))
		schowek = clone_object("/std/container.c");

	if(!zyja())
	{
		object *tu;
		int a, b;

		say(teksty[9] + "\n");
		tu = all_inventory(this_object());
		for(a = 0, b = sizeof(tu) ; a < b ; a++)
		{
			if(living(tu[a]))
				set_alarm(1.0, 0.0, "enter_inv", tu[a], this_object());
		}
	}

	for(i = 0 ; i < ilu ; i++)
	{
		if(bandyci[i] != 0) continue;
		
		elem = explode(dane[i], "|");
		bandyci[i] = clone_object("/std/monster.c");

		if(atoi(elem[6]) == 3)
		{
			bandyci[i]->ustaw_odmiane_rasy(explode(elem[4], ":"), explode(elem[5], ":"), atoi(elem[6]));
			bandyci[i]->set_gender(1);
			if(atoi(elem[7]))
			{
				bandyci[i]->ustaw_nazwe(PL_BANDYTKA);
				bandyci[i]->dodaj_przym(explode(elem[1], ":")[0], explode(elem[1], ":")[1]);
				bandyci[i]->dodaj_przym(explode(elem[2], ":")[0], explode(elem[2], ":")[1]);
				bandyci[i]->dodaj_przym(explode(elem[3], ":")[0], explode(elem[3], ":")[1]);
			}
			else
			{
				bandyci[i]->ustaw_nazwe(explode(elem[4], ":"), explode(elem[5], ":"), atoi(elem[6]));
				bandyci[i]->dodaj_przym(explode(elem[1], ":")[0], explode(elem[1], ":")[1]);
				bandyci[i]->dodaj_przym(explode(elem[2], ":")[0], explode(elem[2], ":")[1]);
			}
		}
		else
		{
			bandyci[i]->ustaw_odmiane_rasy(explode(elem[4], ":"), explode(elem[5], ":"), atoi(elem[6]));
			bandyci[i]->set_gender(0);
			if(atoi(elem[7]))
			{
				bandyci[i]->ustaw_nazwe(PL_BANDYTA);
				bandyci[i]->dodaj_przym(explode(elem[1], ":")[0], explode(elem[1], ":")[1]);
				bandyci[i]->dodaj_przym(explode(elem[2], ":")[0], explode(elem[2], ":")[1]);
				bandyci[i]->dodaj_przym(explode(elem[3], ":")[0], explode(elem[3], ":")[1]);
			}
			else
			{
				bandyci[i]->ustaw_nazwe(explode(elem[4], ":"), explode(elem[5], ":"), atoi(elem[6]));
				bandyci[i]->dodaj_przym(explode(elem[1], ":")[0], explode(elem[1], ":")[1]);
				bandyci[i]->dodaj_przym(explode(elem[2], ":")[0], explode(elem[2], ":")[1]);
			}
		}
		bandyci[i]->add_prop(OBJ_I_WEIGHT, 74000 + (random(31)*1000)); 
		bandyci[i]->add_prop(OBJ_I_VOLUME, 35000 + (random(31)*1000));
		bandyci[i]->add_prop(NPC_I_NO_RUN_AWAY, 1);
		bandyci[i]->add_prop(NPC_M_NO_ACCEPT_GIVE, " mowi do Ciebie: Zadnych sztuczek! Odkladaj rzeczy na ziemie!\n");
		bandyci[i]->set_stats(({staty[0] + random(30), staty[1] + random(30), staty[2] + random(30), staty[3] + random(15), staty[4] + random(15), staty[5] + random(20)}));
		bandyci[i]->set_default_answer(teksty[8]);
		bandyci[i]->set_long(elem[0]);
		bandyci[i]->odmien_short();
		bandyci[i]->odmien_plural_short();
		bandyci[i]->set_skill(SS_PARRY, umy[0] + random(20));
		bandyci[i]->set_skill(SS_DEFENCE, umy[1] + random(20));
		bandyci[i]->set_skill(SS_WEP_SWORD, umy[2] + random(20));
		bandyci[i]->add_weapon("/d/Wiz/w/yevaud/bandyci_bron/prosty_masywny");
		bandyci[i]->add_armour("/d/Wiz/w/yevaud/bandyci_bron/kolczuga");
		bandyci[i]->add_armour("/d/Wiz/w/yevaud/bandyci_bron/buty");
		bandyci[i]->add_armour("/d/Wiz/w/yevaud/bandyci_bron/spodnie");
		bandyci[i]->heal_hp(bandyci[i]->query_max_hp());
	}

	bandyci->move(this_object(), 1);

	return 1;
}

mixed *
wylosuj_rase()
{
	int rasa = random(19);

	switch(rasa)
	{
		case 0:
			return({PL_MEZCZYZNA, ({"ludzki", "ludzcy"})});
		case 1:
			return({PL_KOBIETA, ({"ludzki", "ludzcy"})});
		case 2:
			return({PL_ELF, ({"elfi", "elfi"})});
		case 3:
			return({PL_ELFKA, ({"elfi", "elfi"})});
		case 4:
			return({PL_KRASNOLUD, ({"krasnoludzki", "krasnoludzcy"})});
		case 5:
			return({PL_KRASNOLUDKA, ({"krasnoludzki", "krasnoludzcy"})});
		case 6:
			return({PL_HOBBIT, ({"hobbicki", "hobbiccy"})});
		case 7:
			return({PL_HOBBITKA, ({"hobbicki", "hobbiccy"})});
		case 8:
			return({PL_GNOM, ({"gnomi", "gnomi"})});
		case 9:
			return({PL_GNOMKA, ({"gnomi", "gnomi"})});
		case 10:
			return({PL_GOBLIN, ({"goblinski", "goblinscy"})});
		case 11:
			return({PL_GOBLINKA, ({"goblinski", "goblinscy"})});
		case 12:
			return({PL_HALFLING, ({"halflinski", "halflinscy"})});
		case 13:
			return({PL_HALFLINKA, ({"halflinski", "halflinscy"})});
		case 14:
			return({PL_OGR, ({"ogrzy", "ogrzy"})});
		case 15:
			return({PL_OGRZYCA, ({"ogrzy", "ogrzy"})});
		case 16:
			return({PL_ORK, ({"orczy", "orczy"})});
		case 17:
			return({PL_ORCZYCA, ({"orczy", "orczy"})});
		case 18:
			return({PL_POLORK, ({"polorczy", "polorczy"})});
		case 19:
			return({PL_POLORCZYCA, ({"polorczy", "polorczy"})});
	}
}

string *
losowe_przym(int ile)
{
	string *lp, *lm;
	int i;

	if(ile == 1)
	{
		lp = ({"grozny", "brzydki", "szpetny", "jednooki", "paskudny", "wielki"});
		lm = ({"grozni", "brzydcy", "szpetni", "jednoocy", "paskudni", "wielcy"});
		i = random(6);
		return ({lp[i], lm[i]});
	}
	else
	{
		lp = ({"potezny", "muskularny", "umiesniony", "szczurowaty", "zwinny", "atletyczny"});
		lm = ({"potezni", "muskularni", "umiesnieni", "szczurowaci", "zwinni", "atletyczni"});
		i = random(6);
		return ({lp[i], lm[i]});
	}
	return ({" ", " "});
}
