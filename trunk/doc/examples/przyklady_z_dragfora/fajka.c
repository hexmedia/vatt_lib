/*
 * Wlasnie stworzylem cud nad cudami
 * Dnia 7 czerwca roku panskiego 2003
 * Ja, Czarodziej Antrajk
 */

inherit "/std/object";
#include <formulas.h>
#include <macros.h>
#include <ss_types.h>
#include <stdproperties.h>
#include <wa_types.h>
#include <filter_funs.h>
#include <options.h>
#include <pl.h>
#define TP this_player()
#define TO this_object()
int zaczyna_palic();
int zaciagnij();
int dym_nosem();
int dym_ustami();
int milosc();
int zblednij();
int potrzasnij();
int wytrzep();
int wytrzyj();
int usmiech();
int wspominaj();
int kotwica();
int spojrz();
int kaszel();
int smiech();
int zgas();
int pomoc();
string stary_opis;
int wyd;
void dym();
object room;
void
create_object()
{

  ustaw_nazwe(({"fajka","fajki","fajce","fajke","fajka","fajce"}),
       ({"fajki","fajek","fajka","fajki","fajkami","fajkach"}),PL_ZENSKI);

  dodaj_przym("przepiekna", "przepiekne");
  dodaj_przym("rzezbiona", "rzezbione");
  set_no_show_composite(1);
  set_long("Przygladasz sie niezwykle pieknej fajce wykonanej z mahoniu. " +
           "Cala, prawie lokciowa powierzchnie pokrywaja recznie rzezbiona " +
           "elfie runy, zas sama glowka fajki zostala wyprofilowana na " +
           "ksztalt rozwartej paszczy czerwonego smoka gotowego by zionac " +
           "ogniem, badz chociaz tytoniowym dymem. " +
           "Do fajki przymocowano skorzany rzemyk, dzieki ktoremu mozna " +
           "ja bezpiecznie zawiesic na szyi. " +
           "Na ustniku wygrawerowano slowo <fjpomoc>.\n");

  add_prop(OBJ_I_VALUE, 1000);
  add_prop(OBJ_I_WEIGHT, 120);
  add_prop(OBJ_I_VOLUME, 130);
  add_prop(OBJ_M_NO_SELL, "Nie mozesz sprzedac swojej fajki!\n");
  add_prop(OBJ_M_NO_DROP, "Nie. Nie odwazysz sie na to.\n");
  add_prop(OBJ_M_NO_GIVE, "Kiedy to jest wylacznie twoje.\n");

  TP->add_subloc("fajkarz", TO);

}
void
init()
{
    ::init();
    add_action("zaczyna_palic","fjzapal");
    add_action("zaciagnij","fjzaciagnij");
    add_action("dym_nosem","fjnos");
    add_action("dym_ustami","fjusta");
    add_action("milosc","fjmilosc");
    add_action("zblednij","fjzblednij");
    add_action("potrzasnij","fjpotrzasnij");
    add_action("wytrzep","fjwytrzep");
    add_action("wytrzyj","fjwytrzyj");
    add_action("usmiech","fjusmiech");
    add_action("wspominaj","fjwspominaj");
    add_action("kotwica","fjkotwica");
    add_action("spojrz","fjspojrz");
    add_action("kaszel","fjkaszel");
    add_action("smiech","fjsmiech");
    add_action("zgas","fjzgas");
    add_action("pomoc","fjpomoc");
    add_action("destruct","zniszcz");
}
void
dym()
{
tell_room(room,"Dym sie rozwiewa.\n");
room->set_long(stary_opis);
}
int
pomoc()
{
        write("=========================================================================\n"+
        "          EMOCJE PALACZA : \n"+
        "\n"+
        "-fjzapal   	  - zapalenie fajeczki \n"+
        "-fjzaciagnij     - zaciagniecie sie dymem \n"+
        "-fjnos           - wypuszczenie dymu nosem \n"+
        "-fjusta          - wypuszczenie dymu ustami \n"+
        "-fjmilosc        - okazywanie swych uczuc do fajki \n"+
        "-fjzblednij      - gdy ma sie ochote zapalic \n"+
        "-fjpotrzasnij    - potrzasniecie pusta fajka \n"+
        "-fjwytrzep       - wytrzepanie fajki \n"+
        "-fjwytrzyj       - wytarcie fajki \n"+
        "-fjusmiech       - usmiech palacza \n"+
        "-fjwspominaj     - wspominanie dawnych dziejow \n"+
        "-fjkotwica       - drobna sztuczka ku uciesze gawiedzi \n"+
        "-fjspojrz        - poszukiwanie ziela fajkowego \n"+
        "-fjkaszel        - nagly atak kaszlu \n"+
        "-fjsmiech        - okazywanie radosci z stanu blogosci \n"+
        "-fjzgas          - zgaszenie fajeczki \n"+
        "\n"+
        "-fjpomoc         - powyzszy spis \n"+
        "\n"+
        "\n"+
        "\n"+
        " Jesli chcesz rzucic nalog to mozesz zawsze 'zniszczyc' swoja fajke.\n"+
        "\n"+
        "           Wszelkie bledy nalezy zglaszac Arcypalaczowi Antrajkowi\n"+
        "==============================================================================\n");

        return 1;
}
zaczyna_palic() 
{
room = environment(this_player());

saybb(QCIMIE(this_player(), PL_MIA) + " zapala fajke i wklada ja do ust.\n");
write("Zapalasz fajke i wkladasz ja do ust.\n");
stary_opis = room->query_long();
room->set_long(stary_opis + "Unosi sie tu tytoniowy dym.\n");
wyd = set_alarm(10.0,0.0,dym);
return 1;
}
int
zaciagnij() 
{
saybb(QCIMIE(this_player(), PL_MIA) + " zaciaga sie powoli fajkowym dymem.\n");
write("Zaciagasz sie powoli fajkowym dymem.\n");
stary_opis = room->query_long();
room->set_long(stary_opis + "Unosi sie tu tytoniowy dym.\n");
wyd = set_alarm(5.0,0.0,dym);
return 1;
}
int
dym_nosem() 
{
saybb(QCIMIE(this_player(), PL_MIA) + " wypuszcza powoli dym nosem.\n");
write("Wypuszczasz powoli dym nosem.\n");
stary_opis = room->query_long();
room->set_long(stary_opis + "Unosi sie tu tytoniowy dym.\n");
wyd = set_alarm(5.0,0.0,dym);
return 1;
}
int
dym_ustami() 
{
saybb(QCIMIE(this_player(), PL_MIA) + " wypuszcza powoli dym ustami.\n");
write("Wypuszczasz powoli dym ustami.\n");
stary_opis = room->query_long();
room->set_long(stary_opis + "Unosi sie tu tytoniowy dym.\n");
wyd = set_alarm(5.0,0.0,dym);
return 1;
}
int
milosc()
{
saybb(QCIMIE(this_player(), PL_MIA) + " obdarza swoja fajke spojrzeniem pelnym milosci i troski.\n");
write("Spogladasz na swoja fajke z miloscia i troska.\n");
return 1;
}
int
zblednij()
{
saybb(QCIMIE(this_player(), PL_MIA) + " gwaltownie blednie. Jego jakby zagubiony wzrok bladzi dookola, " +
"a reka powoli przesuwa sie w strone fajki, na ktorej po chwili mocno sie zaciska. Palacz spoglada na nia " +
"pelnym glodu wzrokiem, nieswiadomie glosno przelykajac sline. Widac ze ma wielka ochote szybko ja napelnic..\n");
write("Robi Ci sie zimno, a twoj wzrok po chwili bladzenia nieswiadomie zatrzymuje sie na fajce, ktora nie " +
"wiadomo kiedy znalazla sie w twoich trzesacych sie rekach. Trzeba by ja natychmiast napelnic.\n");
return 1;
}
int
potrzasnij()
{
saybb(QCIMIE(this_player(), PL_MIA) + " potrzasa prawie pusta glowka swojej fajki. Na jego twarzy " +
"pojawia sie wyraz szczerego smutku.\n");
write("Potrzasasz prawie pusta glowka swojej fajki. Na twojej twarzy wykwita wyraz szczerego smutku. " +
"Musisz wkrotce znow ja napelnic!\n");
return 1;
}
int
wytrzep()
{
saybb(QCIMIE(this_player(), PL_MIA) + " delikatnie zaczyna uderzac glowka swojej fajki o kant dloni, " + 
"wytrzepujac z niej popiol. Po chwili jest gotowa do ponownego nabicia.\n");
write("Delikatnie zaczynasz uderzac glowka swojej fajki o kant dloni, wytrzepujac z niej popiol. " +
"Po chwili jest gotowa do ponownego nabicia.\n");
return 1;
}
int
wytrzyj()
{
saybb(QCIMIE(this_player(), PL_MIA) + " szybkim ruchem rekawa przeciera powierzchnie swojej ukochanej fajki.\n");
write("Szybkim ruchem rekawa przecierasz powierzchnie swojej ukochanej fajki.\n");
return 1;
}
int
usmiech() 
{
saybb(QCIMIE(this_player(), PL_MIA) + " usmiecha sie ukazujac swoje zolte od nalogu " +
"zebiska.\n");
write("Usmiechasz sie pokazujac wszystkim swoje zolte zeby palacza.\n");
return 1;
}
int
wspominaj()
{
saybb(QCIMIE(this_player(), PL_MIA) + " zaczyna wspominac dawne, zle czasy kiedy to nikt nie wiedzial o czyms " + 
"takim jak fajka, zas zycie bylo szare i ponure. Na jego twarzy pojawia sie wyraz ulgi, plynacej z faktu, ze " + 
"niemusi zyc w tamtych podlych czasach.\n");
write("Zaczynasz wspominac dawne, zle czasy kiedy to nikt nie wiedzial o czyms takim jak fajka, zas zycie " +
"bylo szare i ponure. Jakie to szczescie, ze nie musisz zyc w tamtych podlych czasach.\n");
return 1;
}
int
kotwica()
{
saybb(QCIMIE(this_player(), PL_MIA) + " powoli wydmuchuje dym ustami, wciagajac go w tym samym czasie nosem, " +
" przez co tworzy zabawny zagiel z dymu. Po chwili przestaje, a dym rozplywa sie w powietrzu.\n");
write("Zaczynasz powoli wydmuchiwac dym ustami, wciagajac go w tym samym czasie nosem, przez co tworzy sie " +
"zabawny zagiel z dymu. Po chwili przestajesz, a dym rozplywa sie w potwierzu.\n");
return 1;
}
int
spojrz()
{
saybb(QCIMIE(this_player(), PL_MIA) + " szybko ogarnia szalonym wzrokiem cala okolice w poszukiwaniu ziela fajkowego.\n");
write("Szybko lustrujesz cala okolice w poszukiwaniu ziela fajkowego.\n");
return 1;
}
int
kaszel()
{
saybb(QCIMIE(this_player(), PL_MIA) + " zaczyna spazmatycznie kaszlec, trzymajac sie za klatke piersiowa. "+
"Mozna sadzic, ze jego pluca sa w okropnym stanie.\n");
write("Twoim cialem wstrzasa spazmatyczny kaszel, wskazujacy na znaczace zniszczenie twoich pluc. " +
"Moze powoli trzeba ograniczyc ziele fajkowe?.\n");
return 1;
}
int
smiech()
{
saybb(QCIMIE(this_player(), PL_MIA) + " gwaltownie wyjmuje fajke z ust, a nastepnie wybucha dzikim, " + 
"nieokielznanym wrecz smiechem. Zamykajac oczy z radosci zaczyna caly trzas sie jak galaretka, a jego " +
"usmiech biegnacy od ucha do ucha oraz rytmicznie podskakujaco to w gore to w dol glowa, synchronizujaca " +
"sie z wydawanymi przez niego salwami chichotu, sprawia iz wyglada naprawde komicznie. " +
"\n"+
"Ocierajac splywajace mu po policzku lzy, otwiera powoli oczy i wyraznie stara sie zlapac " +
"gleboki oddech. Po chwili wyglada juz tak jak przedtem i widocznie zadowolony z czegos spowrotem " +
"wklada sobie fajke do ust.\n");
write("Szybkim ruchem reki, chcac ratowac zawartosc fajki, wyjmujesz ja z ust, by nastepnie " +
"parsknac dzikim, nieokielznanym wrecz smiechem, wydobywajacym sie prosto z twojej przepony. " +
"Z radosci przymykasz oczy i trzesac sie caly jak galaretka szczerzysz zeby w usmiechu biegnacym " +
"od ucha do ucha, a twoja glowa rytmicznie podskakuje w gore i w dol, synchronizujac sie z " +
"wydawanymi przez ciebie salwami chichotu. " +
"\n"+
"Powolutku dochodzisz do siebie ocierajac splywajace ci po policzku lzy, i lapiac szybko " +
"glebokie oddechy otwierasz oczy, starajac sie wygladac powaznie. Po chwili wysilku i koncentracji, " +
"znow wydajesz sie byc soba. Z zadowoleniem z osiagnietego stanu, wkladasz fajke do ust i zaczynasz " +
"ja sobie pykac.\n");
return 1;
}
int
zgas() 
{
saybb(QCIMIE(this_player(), PL_MIA) + " gasi fajke.\n");
write("Gasisz fajke.\n");
return 1;
}
int
destruct(string co)
{
saybb(QCIMIE(this_player(), PL_MIA) + " zdecydowanym ruchem dloni niszczy swoja fajke. " + 
"Po "+TP->koncowka("jego", "jej")+" twarzy splywa pojedyncza lza.\n");
write("Zdecydowanym ruchem dloni niszczysz swoja fajke. Czujesz, ze po twej " +
"spynela pojedyncza lezka, ucielesniajaca zal twego serca.\n");
TP->remove_subloc("fajkarz", TO);
TO->remove_object();
return 1;
}
string
show_subloc(string subloc, object on_obj, object for_obj)
{
    if (on_obj == for_obj)
    return "Na szyi nosisz przepieknie rzezbiona fajke w ksztalcie smoczego pyska.\n";
    return "Na szyi nosi przepieknie rzezbiona fajke w ksztalcie smoczego pyska.\n";
}
