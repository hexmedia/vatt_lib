#include <formulas.h>
inherit "/d/Faerun/std/stdbizuteria";
#include <wa_types.h>
#include <stdproperties.h>
#include <pl.h>

void
create_bizuteria()
{
	set_autoload();  //zeby nieznikal
    ustaw_nazwe(({ "naszyjnik", "naszyjnika", "naszyjnikowi", "naszyjnik", 
                   "naszyjnikiem", "naszyjniku" }),
               ({ "naszyjniki", "naszyjnikow", "naszyjnikom", "naszyjniki", 
                  "naszyjnikami", "naszyjnikach" }), PL_MESKI_NOS_NZYW);

    dodaj_przym("kunsztowny","kunsztowni");
    dodaj_przym("zloty","zloci");

    set_long("Jest to naszyjnik, ktory nosza zazwyczaj bardzo bogate "+
		"damy. Jego oczka w ksztalcie listkow sa wykonane ze zlota. Na "+
		"nich zostaly umieszczone kawalki diamentow. Calosc sprawia wrazenie "+
		"bardzo drogiego drobiazgu.\n");

 /* Poniewaz bizuteria niechroni, musimy jej
 * wiec ustawic tylko sloty - na co sie ja zaklada.
 */
    set_slots(A_NECK);
             
/* Wartosc naszej bizuterii w miedziakach
 */
    add_prop(OBJ_I_VALUE, 2500);
	add_prop(OBJ_I_VOLUME, 25);  //obietosc mala
    add_prop(OBJ_I_WEIGHT, 550);  //550 gramow
}