inherit "/std/object.c";

#include <pl.h>
#include <stdproperties.h>
#include <macros.h>
#include <filter_funs.h>

int czytaj_ksiazke();

create_object()
{
    ustaw_nazwe( ({"ksiazka","ksiazki","ksiazce","ksiazke","ksiazkami","ksiazkach"}), 
                 ({"ksiazki","ksiazek","ksiazkom","ksiazki","ksiazkami","ksiazkach"}), PL_ZENSKI);
		 
    dodaj_przym("czerwony","czerwoni");

    set_long("Jest to nieduza czerwona ksiazka, na ktorej okladce znajduje sie "
	  + "wypisany jakims tajemnym jezykiem tytul. Prawdopodobnie proby "
	  + "poznania jej tresci bez uprzedniego przygotowania natychmiastowo "
	  + "spelzna na niczym wiec sam nie wiesz czy wartalo by sprobowac. Mimo "
	  + "to zastanawiasz sie do kogo nalezy ten zadziwiajacy tom.\n");

	add_cmd_item("ksiazke",({"przeczytaj"}), czytaj_ksiazke);
    
	add_prop(CONT_I_WEIGHT, 100);
}
int
czytaj_ksiazke()
{
	saybb(QCIMIE(this_player(), PL_MIA) + " usilnie stara sie poznac tresc czerwonej ksiazki lecz sadzisz, ze "
	  + "owe wysilki spelzna na niczym.\n");
	write("Powoli kartkujesz stronice ksiegi. Mimo calej swej skoncentrowanej wiedzy wszelkie "
	  + "proby odczytania jej tresci pozostawiaja cie na nowo w niepewnosci i poczuciu ciekawosci.\n");
	return 1;
}
    
int query_ksiazka_orlena()
{
return 1;
}
