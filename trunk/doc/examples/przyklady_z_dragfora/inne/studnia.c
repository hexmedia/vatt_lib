#pragma strict_types

inherit "/std/object.c";
inherit "/lib/drink_water.c";

#include <stdproperties.h>
#include "./definicje.h"
#include <macros.h>

void
create_object()
{

    ustaw_nazwe(({"studnia", "studni", "studni", "studnie", "studnia", "studni"}), ({"studnie", "studni", "studniom", "studnie", "studniami", "studniach"}), PL_ZENSKI);
	set_long("Jest to wykonana z rownych kamiennych cegielek, niewielka studnia. Wode "
	  + "wydobywa sie zen za pomoca cebrzyka, ktory przymocowano lina do drewnianego kolowrotu. "
	  + "Ten zas umieszczony zostal zaraz pod slomianym daszkiem znajdujacym sie nad studzienka.\n");
	dodaj_przym("kamienny","kamienni");
	add_prop(OBJ_I_CONTAIN_WATER,1);
	add_prop(OBJ_I_NO_GET,1);
    set_drink_places("ze studni");
}

void
init ()
{
	::init();
	init_drink_water();
}

public void
drink_effect(string str)
{
    write("Za pomoca cebrzyka wydobywasz ze studni nieco wody, ktora natychmiast spozywasz.\n");
    saybb(QIMIE(this_player(), PL_MIA) + " za pomoca drewnianego cebrzyka wydobywa "
	  + "z kamiennej studni nieco wody, ktora natychmiast spozywa.\n");
}

public void
fill_effect(object beczulka, string skad)
{
    if (beczulka->query_ilosc_plynu())
    {
        write("Dopelniasz " + beczulka->short(this_player(), PL_BIE)
            + " woda wydobyta " + skad + " za pomoca cebrzyka.\n");
        saybb(QCIMIE(this_player(), PL_MIA) + " dopelnia "
            + QSHORT(beczulka, PL_BIE) + " woda wydobyta " + skad + " za pomoca cebrzyka.\n");
    }
    else
    {
        write("Napelniasz " + beczulka->short(this_player(), PL_BIE)
            + " woda wydobyta " + skad + " za pomoca cebrzyka.\n");
        saybb(QCIMIE(this_player(), PL_MIA) + " napelnia "
            + QSHORT(beczulka, PL_BIE) + " woda wydobyta " + skad + " za pomoca cebrzyka.\n");
    }
}

