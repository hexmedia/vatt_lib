#pragma strict_types

inherit "/std/object.c";

#include <stdproperties.h>
#include "./definicje.h"
#include <macros.h>

int otworz_szafke();

void
create_object()
{

    ustaw_nazwe(({"szafka", "szafki", "szafce", "szafke", "szafka", "szafce"}), ({"szafki", "szafek", "szafkom", "szafki", "szafkami", "szafkach"}), PL_ZENSKI);
	set_long("Ot zwykla kuchenna szafka.\n");
	
	dodaj_przym("zamkniety","zamknieci");
	dodaj_przym("kuchenny","kuchenni");
	add_prop(OBJ_I_NO_GET,1);

	add_cmd_item("szafke", ({"otworz"}), otworz_szafke);

}
    int
    otworz_szafke()
{
    saybb(QCIMIE(this_player(), PL_MIA) + " otwiera kuchenna szafke po czym przeglada "
	  + "jej zawartosc. Po chwili z lekkim skrzywieniem na twarzy zamyka drzwiczki.\n");
    write("Otwierasz kuchenna szafke lecz ku twemu zdumieniu znajdujesz tam jedynie "
	  + "trzy puste dzbanki po miodzie pitnym i pojemniczek na sol rowniez z widocznym "
+ "wen dnem. Rozaczarowany zamykasz drzwiczki. \n");
	return 1;
}

