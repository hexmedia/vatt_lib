inherit "/std/object.c";

#include <pl.h>
#include <stdproperties.h>
#include <macros.h>
#include <filter_funs.h>

create_object()
{
    ustaw_nazwe( ({"spinki","spinek","spinkom","spinki","spinkami","spinkach"}), 
                 ({"spinki","spinek","spinkom","spinki","spinkami","spinkach"}), PL_NIJAKI_NOS);
		 
    dodaj_przym("elegancki","eleganccy");

    set_long("Sa to dwie spinki wykonane z jakiegos ozdobnego kruszczu, ktore cechuja "
	  + "sie tym, iz zawsze wywoluja szacunek w stosunku do osoby, ktora nosi je wewnatrz "
	  + "mankietow swojej koszuli. Na ich powierzchni dostrzegasz znak w ksztalnie "
	  + "ryby. Nie wiesz co oznacza ow ornament lecz i tak ludzie zamozni i bogaci "
	  + "lubia nosic takie dodatki i z checia prezentuja je budzac zazdrosc wsrod "
	  + "prostego plebsu.\n");
    
	add_prop(CONT_I_WEIGHT, 100);
}

    

