inherit "/std/food.c";

#include <pl.h>
#include <macros.h>
#include <stdproperties.h>

create_food()
{

    ustaw_nazwe(        ({"ser czerwony","sera czerwonego","serowi czerwonemu","ser czerwony","serem czerwonym","serze czerwonym"}),
        ({"sery czerwone","serow czerwonych","serom czerwonym","sery czerwone","serami czerwonymi","serach czerwonych"}),
        PL_MESKI_NOS_NZYW);

    dodaj_nazwy(({"ser","sera","serowi","ser","serem","serze"}),
        ({"sery","serow","serom","sery","serami","serach"}),
        PL_MESKI_NOS_NZYW);

    dodaj_przym("damaryjski","damaryjscy");

    set_long("Jest to typowy damaryjski ser czerwony o niezwykle egzotycznym "
           + "smaku i aromacie. Dobrze nadaje sie na krolewski stol lecz nieco "
	   + "dziwnie bedzie zobaczyc go w ekwipunku poszukiwacza przygod jako "
	   + "jego drugie sniadanie, glownie ze wzgledu na kosztownosc.\n");

    set_amount(80);

}

