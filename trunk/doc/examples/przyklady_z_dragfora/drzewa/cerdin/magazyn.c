#pragma strict_types
#pragma no_clone
inherit "/std/room";
inherit "/lib/store_support"; 
#include <stdproperties.h>
#include <macros.h> 
void
create_room()
{
    set_short("Magazyn tartaku.\n");
    set_long("Jestes w magazynie. W tej lokacji umieszczane "+
        "sa wszystkie\nprzedmioty, ktore gracz sprzedal.\n");    
    add_prop(ROOM_I_INSIDE, 1); 
    reset_room();    
}
public void
reset_room()
{
    reset_store();
}
void                             
enter_inv(object ob, object skad)
{                                
    ::enter_inv(ob, skad);    
    store_update(ob); 
}

