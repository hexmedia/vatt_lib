#pragma strict_types+
#pragma no_clone

inherit "/std/room";
inherit "/d/Wiz/cerdin/tartak/tartak";
#include <stdproperties.h> 

void
create_room()
{
    set_short("Tartak");
    set_long("Jest to tartak, nie bardzo wiesz co on tu robi, lecz "+
	     "chyba nie zamierzasz sie klucic z jego tworca.\n");
        
    config_default_trade(); 
    
    set_store_room("/d/Wiz/cerdin/tartak/magazyn.c"); 
    set_tartak_room("/d/Wiz/cerdin/tartak/tartak_magazyn.c");
    
    add_item( ({ "instrukcje", "tabliczke" }), "Tabliczka ze wskazowkami "+
        "dla klientow.\n");
        
    add_cmd_item( ({ "instrukcje", "tabliczke" }), "przeczytaj",
        "@@standard_shop_sign");
    
    add_prop(ROOM_I_INSIDE, 1);   
    add_exit("droga16","polnocny-wschod");
}
void
init()
{
    ::init(); 
    
    init_tartak(); 
}
