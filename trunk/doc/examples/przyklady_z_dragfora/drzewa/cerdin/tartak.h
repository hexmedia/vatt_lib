/* 
 * /sys/tartak.h
 *
 * Ten plik zawiera wszystkie definicje zwiazane z tartakiem 
 * i drzewami.
 */

#ifndef TARTAK_DEFINED
#define TARTAK_DEFINED

#define TR_M_TYP		"_tr_i_typ"

#define TR_SOSNA		({"sosna","sosnowy","sosnowi"})

/* ------ZADNYCH-DEFINICJI-PONIZEJ-TEJ-LINI--------- */
#endif TARTAK_DEFINED
