/*
 * Filename: koszula.c
 *
 * Zastosowanie: Koszula dla mlodzienca.
 * 
 * Copyright Volothamp Geddarm.
 *
 */
inherit "/d/Faerun/std/stdubrania.c";

#include <stdproperties.h>
#include <wa_types.h>
#include <formulas.h>
#include <macros.h>

void
create_ubranie()
{
    ustaw_nazwe( ({"koszula", "koszuli", "koszuli", "koszule", "koszula", "koszuli"}),
		         ({"koszule", "koszul", "koszulom", "koszule", "koszulami", "koszulach"}), PL_ZENSKI);
    dodaj_przym("elegancki", "eleganccy");
    dodaj_przym("bialy", "biali");
    set_long("Jest to biala, wykonana z lekkiego materialu koszula o eleganckim "
	  + "i gustownym kroju. Na dlugich rekawach ma ona mankiety, a guziki "
	  + "zrobiono z jakiegos lekkiego i miekkiego tworzywa. Mimo to jest to "
	  + "w pewien sposob kosztowny ubior i nie kazdego chlopa stac na takie "
	  + "odzienie.\n");
    set_slots(A_BODY, A_ARMS);
    add_prop(OBJ_I_VOLUME, query_default_weight() / 6);
	add_prop(OBJ_I_VALUE, 10);
}


