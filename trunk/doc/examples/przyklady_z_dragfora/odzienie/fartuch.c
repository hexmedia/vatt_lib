/*
 * Filename: fartuch.c
 *
 * Zastosowanie: Fartuch dla kowala.
 * 
 * Copyright Volothamp Geddarm.
 *
 */
inherit "/d/Faerun/std/stdubrania.c";

#include <stdproperties.h>
#include <wa_types.h>
#include <formulas.h>
#include <macros.h>

void
create_ubranie()
{
    ustaw_nazwe( ({"fartuch", "fartucha", "fartuchowi", "fartuch", "fartuchem", "fartuchu"}),
		         ({"fartuchy", "fartuchow", "fartuchom", "fartuchy", "fartuchami", "fartuchach"}), PL_MESKI_NOS_NZYW);
    
	dodaj_przym("brudny", "brudni");
    dodaj_przym("kowalski", "kowalscy");
    
	set_long("Jest to nieco zabrudzony juz typowo kowalski fartuch. Dostrzegasz na jego "
	  + "szorstkiej powierzchni slady starych, niezmytych plan, a takze widzisz, ze "
	  + "jest on juz niekiedy pozolkly. Zaklada sie go na tulow oraz nogi, tak aby obie "
	  + "te partie ciala kowala chronil przed zarem buchajacym z paleniska w ich kuzniach.\n");
    set_slots(A_BODY, A_ARMS);
    add_prop(OBJ_I_VOLUME, query_default_weight() / 6);
	add_prop(OBJ_I_VALUE, 10);
}


