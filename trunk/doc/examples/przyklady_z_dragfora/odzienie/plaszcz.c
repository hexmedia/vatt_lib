/*
 * Filename: plaszcz.c
 *
 * Zastosowanie: Plaszcza nie widziales?
 * 
 * Copyright Volothamp Geddarm.
 *
 */
inherit "/d/Faerun/std/stdubrania.c";

#include <stdproperties.h>
#include <wa_types.h>
#include <formulas.h>
#include <macros.h>

void
create_ubranie()
{

    ustaw_nazwe( ({"plaszcz", "plaszcza", "plaszczowi", "plaszcz", "plaszczem", "plaszczu"}),
		         ({"plaszcze", "plaszczy", "plaszczom", "plaszcze", "plaszczami", "plaszczach"}), PL_MESKI_NOS_NZYW);
    
	dodaj_przym("skorzany","podrozni");
	dodaj_przym("podrozny", "podrozny");
    
	set_long("Jest to wykonany z grubego, skorzanego materialu plaszcz podrozniczy. Ze wzgledu "
	  + "na wykonanie jest on prawdopodobnie dosyc kosztowny lecz to nie zmienia faktu, iz "
	  + "jest rowniez bardzo pozytecznym odzieniem podczas dluzszych, wyczerpujacych sily wedrowek. "
	  + "Dokladne obszycie, idealny kroj i swietna, zwierzeca skora bedaca miekka pod dotykiem sprawiaja, "
	  + "ze ow ubior moze uchodzic niemalze za arcydzielo krawieckie w tej jednakze dziedzinie.\n");
    
	set_slots(A_ROBE);

    add_prop(OBJ_I_WEIGHT, 50);
	add_prop(OBJ_I_VALUE, 500);
}


