/*
 * Filename: buty.c
 *
 * Zastosowanie: Buty dla kupca w gospodzie.
 * 
 * Copyright Volothamp Geddarm.
 *
 */

inherit "/d/Faerun/std/stdubrania.c";

#include <wa_types.h>
#include <stdproperties.h>
#include <pl.h>

void
create_ubranie() 
{
    dodaj_nazwy(({ "buty", "butow", "butom", "buty", "butami", "butach" }), PL_ZENSKI);

    ustaw_nazwe(({ "para butow", "pary butow", "parze butow", "pare butow", "para butow", "parze butow" }),
                ({ "pary butow", "par butow", "parom butow", "pary butow", "parami butow", "parach butow" }), PL_ZENSKI);

    ustaw_shorty(({ "para skorzanych butow z cholewa", "pary skorzanych butow z cholewa", 
                    "parze skorzanych butow z cholewa", "pare skorzanych butow z cholewa", 
                    "para skorzanych butow z cholewa", "parze skorzanych butow z cholewa" }), 
                 ({ "pary skorzanych butow z cholewa", "par skorzanych butow z cholewa", 
                    "parom skorzanych butow z cholewa", "pary skorzanych butow z cholewa", 
                    "parami skorzanych butow z cholewa", "parach skorzanych butow z cholewa" }), PL_ZENSKI);

    dodaj_przym("lekki", "leccy");

    set_long("Sa to wykonane z mocnej i dobrej skory buty, ktore posiadaja szeroka "+
        "cholewe, w ktorej zwykle moznaby schowac cos w rodzaju prostego sztyletu "+
	"badz zwyklego luczywka. Takie obuwie nosza zazwyczaj urzednicy, kupcy lub "+
	"osoby parajace sie prawdziwie z polityka.\n");

    set_slots(A_FEET);

    add_prop(OBJ_I_VALUE, 300);
}

int
is_auto_sobj()
{
        return 1;
}
