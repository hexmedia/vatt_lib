/*
 * Filename: kapelusz.c
 *
 * Zastosowanie: Kapelusz dla maga Orlena.
 * 
 * Copyright Volothamp Geddarm.
 *
 */
inherit "/d/Faerun/std/stdubrania.c";

#include <stdproperties.h>
#include <wa_types.h>
#include <formulas.h>
#include <macros.h>

void
create_ubranie()
{
    ustaw_nazwe( ({"kapelusz", "kapelusza", "kapeluszowi", "kapelusz", "kapeluszem", "kapeluszu"}),
//
		         ({"kapelusze", "kapeluszy", "kapeluszom", "kapelusze", "kapeluszami", "kapeluszach"}), PL_MESKI_NOS_NZYW);
    dodaj_przym("spiczasty", "spiczasci");
    set_long("Jest to gustowny kapelusz o stylowym, szerokim rondzie i "
	  + "spiczastym czubku. Takie same nakrycia glowy nosza magowie co "
	  + "poznac mozesz po licznych magicznych wzorach wyszytych na materiale. "
	  + "Ow kapelusz ma rowniez szczegolna wlasciwosc, iz trudno zdejmowac "
	  + "go kiedy chcesz sie z kims przywitac co jednoznacznie podkresla wladze "
	  + "i potege noszacej go osoby.\n");
    
	set_slots(A_HEAD);

    add_prop(OBJ_I_WEIGHT, 500);
	add_prop(OBJ_I_VALUE, 10);
}


