/*
 * Filename: zbroja_skorzana.c
 *
 * Zastosowanie: Zbroja skorzana.
 * 
 * Copyright Volothamp Geddarm.
 *
 */
inherit "/d/Faerun/std/zbroja.c";

#include <stdproperties.h>
#include <wa_types.h>
#include <formulas.h>
#include <macros.h>

void
create_armour()
{

	set_autoload();

    ustaw_nazwe( ({"zbroja", "zbroi", "zbroi", "zbroje", "zbroja", "zbroi"}),
		         ({"zbroje", "zbroi", "zbrojom", "zbroje", "zbrojami", "zbrojach"}), PL_ZENSKI);
    dodaj_przym("skorzany", "skorzani");
	dodaj_przym("cwiekowany", "cwiekowani");

    set_long("Jest to wykonana ze skory jakiegos zwierzecia zbroja, w ktora dla zwiekszenia "
 + "wytrzymalosci i komfortu oraz wygody noszenia, wbito zelazne cwieki. Sadzisz, ze moze "
	  + "ona zawiesc w walce kiedy przeciwnik trzymac bedzie zelazny mlot lecz do skradania sie przez puszcze "
	  + "nie znajdziesz lepszego pancerza. Jest on lekki, cichy i nie ogranicza ruchow, a na dodatek "
	  + "dobrze komponuje sie z lesnym otoczeniem. Dlatego wlasnie jest ta zbroja niezwykle ceniona "
	  + "i polecana przez doswiadczonych lowcow o szerokich doswiadczeniach. Dostrzegasz na jej powierzchni "
	  + "osobisty znak slynnej kuzni polozonej w Beregoscie.\n");

    set_ac(A_BODY, 12, 11, 10,
		   A_ARMS, 8, 9, 9);
    add_prop(OBJ_I_VOLUME, query_default_weight() / 6);
	add_prop(OBJ_I_WEIGHT, 3000);
	add_prop(OBJ_I_VALUE, 700);
}


