/*
 * Filename: helm.c
 *
 * Zastosowanie: Helm.
 * 
 * Copyright Volothamp Geddarm.
 *
 */
inherit "/d/Faerun/std/zbroja.c";

#include <stdproperties.h>
#include <wa_types.h>
#include <formulas.h>
#include <macros.h>

void
create_armour()
{
    set_autoload();

    ustaw_nazwe( ({"helm", "helma", "helmowi", "helm", "helmem", "helmie"}),
		         ({"helmy", "helmow", "helmom", "helmy", "helmami", "helmach"}), PL_MESKI_NOS_NZYW);
    
	dodaj_przym("masywny", "masywni");
	dodaj_przym("zamkniety", "zamknieci");

set_long("Jest to zamkniety helm, posiada otwor w ksztalcie litery 'T' dajacy "
	  + "sposobnosc nie wprowadzania otworow na oczy ani przylbicy. Zauwazasz nan liczne zdobienia wygladajace "
	  + "na bardzo misterne i zawile, glownie w ksztalcie smokow zjadajacych wlasne ogony. Ponadto dostrzegasz "
	  + "na jego powierzchni osobisty znak slynnej kuzni polozonej w Beregoscie.\n");

    set_ac(A_HEAD, 20, 20, 20);

    add_prop(OBJ_I_VOLUME, query_default_weight() / 6);
	add_prop(OBJ_I_WEIGHT, 4500);
	add_prop(OBJ_I_VALUE, 750);
}


