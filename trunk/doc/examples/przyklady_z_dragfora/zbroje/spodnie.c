
inherit "/d/std/zbroja";
#include "../pal.h"

void create_armour() 
{
      set_autoload();
      ustaw_nazwe( ({ "spodnie", "spodni", "spodniom", "spodnie",
      		      "spodniami", "spodniach" }), PL_MESKI_NOS_ZYW);

      dodaj_nazwy(({ "para spodni", "parze spodni", "parze spodni",
		     "pare spodni", "para spodni", "parze spodni" }), 
		     ({ "pary spodni", "par spodni", "parom spodni", 
		     "pary spodni", "parami spodni", "parach spodni" }),
		     PL_ZENSKI);

      ustaw_shorty(({ "para dlugich spodni", "parze dlugich spodni", 
      		      "parze dlugich spodni", "pare dlugich spodni", 
      		      "para dlugich spodni", "parze dlugich spodni" }),
      		      ({ "pary dlugich spodni", "par dlugich spodni", 
      		      "parom dlugich spodni", "pary dlugich spodni", 
      		      "parami dlugich spodni", "parach dlugich spodni" }), 
      		      PL_ZENSKI);

     dodaj_przym("dlugi", "dludzy");
      
     set_long("Dlugie, skorzane spodnie. Nie zapewniaja specjalnej ochrony "
     +"nog przed ciosami, jednak sa bardzo wygodne, dlatego nosi je wielu "
     +"mieszczan.");

     set_ac(A_LEGS, 2, 2, 2);
}
 
