/*
 * Filename: kolczuga1.c
 *
 * Zastosowanie: Kolczuga.
 * 
 * Copyright Volothamp Geddarm.
 *
 */
inherit "/d/Faerun/std/zbroja.c";

#include <stdproperties.h>
#include <wa_types.h>
#include <formulas.h>
#include <macros.h>

void
create_armour()
{
	set_autoload();

    ustaw_nazwe( ({"kolczuga", "kolczugi", "kolczudze", "kolczuge", "kolczuga", "kolczudze"}),
		         ({"kolczugi", "kolczug", "kolczugom", "kolczugi", "kolczugami", "kolczugach"}), PL_ZENSKI);
    
	dodaj_przym("blyszczacy", "blyszczacy");
    dodaj_przym("kaplanski", "kaplanscy");
    set_long("Jest to stworzona z pojedynczych koleczek, gruba kolczuga wykonana specjalnie "
	  + "dla kaplanow Lathandera. Wyglada ona na stosunkowo krotka zbroje lecz mimo to "
	  + "stanowi znakomita ochrone ramion oraz tulowia, zas na miejscu klatki piersiowej "
	  + "dostrzegasz pieknie wypolerowany znak Lathandera. Ponadto warto dodac, ze jak "
	  + "na mocna zbroje jest ona pieknie wyczyszczona, zas promienie swiatla odbijaja sie "
	  + "od niej blyszczac wspaniale.\n");
    set_ac(A_BODY, 33, 34, 33,
		   A_ARMS, 28, 28, 28);
    add_prop(OBJ_I_VOLUME, query_default_weight() / 6);
	add_prop(OBJ_I_WEIGHT, 7500);
	add_prop(OBJ_I_VALUE, 1700);
}


