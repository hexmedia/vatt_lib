inherit "/d/std/zbroja";
#include "../pal.h"

#define SUKNIA_SUBLOC "sukienka"

string *losowy_przym()
{

 string *lp;
 string *lm;
 int i;
 lp=({ "niebieski"  ,"czerwony"  ,"zielony"  ,"zolty"  ,"czarny" ,
       "bialy" ,"bordowy", "kremowy"});
 lm=({ "niebiescy"  ,"czerwoni"  ,"zieloni" ,"zolci"  ,"czarni"  ,
       "biali" ,"bordowi", "kremowi"});
 i=random(8);

 return ({ lp[i] , lm[i] });

}

void create_armour()
{

	string *randadj=losowy_przym();
     set_autoload();		
    ustaw_nazwe( ({ "sukienka", "sukienki", "sukience", "sukienke", "sukienka", "sukience" }),
    ({ "sukienki", "sukienek", "sukienkom", "sukienki", "sukienkami", "sukienkach" }),
    PL_ZENSKI);

    dodaj_przym(randadj[0],randadj[1]);

    set_long("Jest to ladna sukienka uszyta z bardzo lekkiego materialu. Jest dosc skromna, jednak "
    +"wyglada modnie. Dzieki temu, ze jest dosc dluga i ma rekawy, jest dosyc ciepla.\n");
             
    set_ac(A_BODY, 2, 3, 2,
    	   A_ARMS, 3, 2, 3,
    	   A_LEGS, 2, 3, 2);
}

int wear(object ob)
{
	if (TP->query_gender()==G_MALE)
	{
		write("Ubierasz na siebie sukienke. Po krotkiej chwili zauwazasz, ze "
		+"mijajace cie osoby dziwnie ci sie przygladaja.\nCzujesz sie jak idiota.\n");
		
		saybb(QCIMIE(this_player(),PL_MIA) + " zaklada na siebie sukienke. Na jego "
		+"twarzy wykwita po chwili dziwny wyraz, po czym zarumieniony zaczyna "
		+"patrzec tepo w ziemie, probujac nie zwracac na siebie uwagi.\n");
		
		TP->remove_subloc(SUKNIA_SUBLOC);
		TP->add_subloc(SUKNIA_SUBLOC,TO);
		TO->move(TP,SUKNIA_SUBLOC);
		
		return 1;
	} else {
		write("Zakladasz na siebie sukienke. Ahhh... Jak to milo czasem odpoczac "
		+"od kolczugi i miecza i ubrac sie w cos normalnego.\n");
		saybb(QCIMIE(this_player(),PL_MIA) + " ubiera na siebie sukienke, a na jej "
		+"twarzy wykwita wyraz blogosci. Widac, ze jest zadowolona z nowego ubioru.\n");	
	}
}

int remove(object ob)
{
	if (TP->query_gender()==G_MALE)
	{
		write("Zdejmujesz z siebie sukienke. Ciekawskie spojrzenia jeszcze "
		+"przed chwila gapiacych sie na ciebie jak na zielonego kota osob "
		+"odwracaja sie.\nCzujesz sie o wiele lepiej.\n");
		
		saybb(QCIMIE(this_player(),PL_MIA) + " zdejmuje z siebie sukienke, a na jego "
		+"twarzy wykwita wyraz blogosci. Chyba znow poczul sie soba.\n");
		
		TP->remove_subloc(SUKNIA_SUBLOC);
		
		return 1;	
		
	} else {
		write("Zdejmujesz z siebie sukienke. Hm... Czyzbys za duzo odslonila? "
		+"Mezczyzni jakos dziwnie sie gapia.\n");
		saybb(QCIMIE(this_player(),PL_MIA) + " powoli, z niejakim ociaganiem "
		+"zdejmuje z siebie sukienke.\n");
	}	
}

string show_subloc(string subloc, object on_obj, object for_obj)
{
	if (on_obj == for_obj)
		return "Masz na sobie sukienke, w ktorej czujesz sie jak skonczony idiota.\n";
	
    	return "W sukience, ktora ma na sobie wyglada conajmniej podejrzanie. Chyba lepiej trzymac sie "
    	+"w bezpiecznej odleglosci.\n";
    	return MASTER+":";
}
