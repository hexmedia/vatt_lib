inherit "/d/std/zbroja";
#include <wa_types.h>
#include <stdproperties.h>
#include <pl.h>

void
create_armour()
{
    set_autoload();
    ustaw_nazwe(({ "tarcza", "tarczy", "tarczy", "tarcze", 
                   "tarcza", "tarczy" }),
               ({ "tarcze", "tarcz", "tarczom", "tarcze", 
                  "tarczami", "tarczach" }), PL_ZENSKI);

    dodaj_przym( "lsniacy", "lsniacy" );
    dodaj_przym("duzy","duzi");

    set_long("Jest to lsniaca tarcza wykonana z jakiegos drogiego metalu "
    +"pokryta dodatkowo tworzywem utwardzajacym. Stanowi ona bardzo dobra ochrone "
    +"przed groznymi ciosami przeciwnika. Na jej powierzchni "
    +"widzisz wymalowanego smoka, znak strazy swiatynnej Paladina.\n");
           
    setup_shield(SH_TARCZA, 25);
}
