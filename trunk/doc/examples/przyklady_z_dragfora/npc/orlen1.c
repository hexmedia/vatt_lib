inherit "/d/Faerun/std/questy";
inherit "/std/monster";

#include <macros.h>
#include <stdproperties.h>
#include <language.h>
#include <composite.h>
#include <money.h>
#include <pl.h>
#include <time.h>
#include <formulas.h>
#include <std.h>
#include "../definicje.h"
#include <filter_funs.h>
#include <wywolania.h>

int wyrzuc (object kogo);
int przenies (object kogo);
int *umy = ({10, 10, 10});
object *przyzwani = ({});
int filter_mamidlo (object a);
int pojaw ();
int zniknij ();
static object *pomocnicy=({});
int zajety;
void ksiazka_orlena (int faza,object pla);
int ksiazka ();

void
create_monster()
{
    ustaw_imie(({ "orlen", "orlena", "orlenowi", "orlena", "orlenem", "orlenie"}), PL_MESKI_OS);
	
	dodaj_nazwy(PL_MEZCZYZNA);
    
	set_title("Der, Czarnoksieznik z Waterdeep");
    
	dodaj_przym("bardzo stary","bardzo starzy");
    dodaj_przym("siwobrody","siwobrodzi");
    
	ustaw_odmiane_rasy(PL_MEZCZYZNA);
    
	set_stats( ({ 42, 43, 41, 100, 100, 99 }) );
    
	set_gender(G_MALE);
    
	add_prop(CONT_I_WEIGHT, 59000); 
    add_prop(CONT_I_VOLUME, 35000); 
    add_prop(CONT_I_HEIGHT, 175);
    add_prop(NPC_M_NO_ACCEPT_GIVE,0);
	set_act_time(180);
    add_act("emote spoglada na ciebie bystro.");
    add_act("zmarszcz brwi");
    add_act("hmm");
    add_act("emote praktykuje rzucanie jakiegos zaklecia.");
    add_act("emote poprawia ulozenie swego spiczastego kapelusza.");
    add_act("emote gladzi swa brode w zamysleniu.");
	add_act("emote chwyta pewniej plonaca laske w obie rece.");
	add_act("emote pochyla sie nieznacznie.");
	add_act("emote krzata sie w zamysleniu.");
	add_act("powiedz Tez bym sie napil.");

    set_chat_time(300);
    add_chat("Gdzie moze byc ten przekleta ksiega?!");
	add_chat("Przyslal cie Kelddath? Nic u mnie nie wskorasz.");
	add_chat("Nie ma bytu, ktory bylby ode mnie potezniejszy w calym Beregoscie, a moze i dalej!");

	set_cchat_time(60);
    add_cchat("Nic nie wiesz o bytach tak poteznych jak ja!");
    add_cchat("Wiesz cozes uczynil glupcze?!");
    add_cchat("Nie. Takiego zuchwalstwa nie puszcze ci plazem!");
	add_cchat("Nedzna, smiertelna istoto! Zal mi ciebie.");

	set_default_answer(VBFC_ME("default_answer"));
  
    set_long("Jest to bardzo stary juz mezczyzna, ktory prawdopoobnie wiele "
	  + "przezyl podczas swego zycia. Na pierwszy rzut oka przypomina on wiekowego "
	  + "maga o dlugiej siwej brodzie i przyodzianego w prawdziwie czarodziejskie "
	  + "odzienie jakim jest spiczasty kapelusz i blekitna szata. Laska, ktora trzyma "
	  + "oburacz posiada plonaca koncowka i z cala pewnoscia nie jest ona zwyklym kijem, "
	  + "podobnie zreszta jak osobnik, ktorego obserwujesz nie jest zwyklym starcem acz "
	  + "zapewne bardzo poteznym i uzdolnionym magicznie bytem o wielkim doswiadczeniu "
	  + "zyciowym i jeszcze wiekszej, lecz moze slusznie, pewnosci siebie.\n");

	set_skill(SS_DEFENCE, umy[0] + random(40));
    set_skill(SS_PARRY, umy[1] + random(30));
    set_skill(SS_WEP_POLEARM, umy[2] + random(20));

    add_armour(ODZIENIE_PATH + "szata");
    add_armour(ODZIENIE_PATH + "sandaly");
    add_armour(ODZIENIE_PATH + "kapelusz");
    add_weapon(WEAPONS_PATH + "laska");

	add_ask(({"pomoc", "prace", "zadanie", "zlecenie"}), VBFC_ME("drugie_pytanie"));

	reg_godzinne(7, "pojaw");
	reg_godzinne(20, "zniknij");
}

void
add_introduced(string imie_mia, string imie_bie)
{
	set_alarm(2.0, 0.0, "return_introduce", find_player(imie_mia));
}
void
return_introduce(object ob)
{
    command("przedstaw sie " + OB_NAME(ob));
    command("uklon sie " + OB_NAME(ob) + " z godnoscia");
}
string
default_answer()
{
	set_alarm(2.0,0.0, "pytanie", this_interactive());
}
void
pytanie(object pla)
{
	command("powiedz do " + pla->query_name(PL_BIE) + " Nie zawracaj mi glowy glupia, smiertelna istoto!");
}
void
emote_hook(string emote, object wykonujacy, string przyslowek)
{
    switch (emote)
    {
        case "kopnij":
        case "spoliczkuj" :
        case "szturchnij":
        case "opluj" : set_alarm(2.0, 0.0, "zly", wykonujacy); break;
        	    
        case "zasmiej": 
        case "pocaluj":
        case "przytul": set_alarm(2.0, 0.0, "dobry", wykonujacy);
    }
}



void
zly(object kto)
{
    switch (random(2)) 
    {
		case 0 : command("krzyknij Precz z moich oczu!"); 
	         set_alarm(3.0,0.0, "wyrzuc", kto); break;
		case 1 : command("krzyknij Odejdz stad glupcze pokim jeszcze dobry!");
             set_alarm(3.0,0.0, "wyrzuc", kto); break;
    default:
    }
}

void
dobry(object kto) 
{
        
    if(kto->koncowka("chlop","baba")=="chlop") zly(kto);
    else
    {   
        switch (random(2)) 
        {
          case 0 : command("powiedz Odczep sie ode mnie!"); break;
          case 1 : command("powiedz Precz z moich oczu!"); break;          
        }
      }
} 
int
wyrzuc (object kogo)
{
    command("emote wykonuje tajemniczy gest wypowiadajac przy tym czarodziejska inkantacje.");
	set_alarm(3.0,0.0, "przenies", kogo);
}

int
przenies (object kogo)
{
	object *obecni;
	obecni = all_inventory(find_object(ROOMS_PATH + "ulica5.c"));
	obecni = FILTER_LIVE(obecni);
	obecni->catch_msg("Nagle obok ciebie pojawia sie jaskrawy portal, z ktorego "
	  + "ku twemu zdumieniu wypada " + kogo->query_name() + ".\n");
	kogo->catch_msg("Czujesz jak magiczna sila gdzies cie przenosi.\n");
	kogo->move(ROOMS_PATH + "ulica5.c", 1);
}

void
do_die (object killer)
{
	command("zasmiej sie zlowieszczo");
	command("powiedz Czy uwazasz, ze tak latwo mnie zabic?");
	command("krzyknij Glupcze!");
	write("Wykrzykuje kilka tajemniczych slow, a nagly blyska otacza go, a rany na jego ciele zasklepiaja sie.\n");
	say("Wykrzykuje kilka tajemniczych slow, a nagly blyska otacza go, a rany na jego ciele zasklepiaja sie.\n");
	this_object()->heal_hp(query_max_hp());
}

public int
special_attack(object victim)
{
int szansa = random(100);
object nowy;
object *temp;
int i;

		if(szansa <= 22)
		{	
			write("Czarnoksieznik kieruje w twoja strone swa reke, a z jego dlonie wylatuje strzala raniac cie bolesnie.\n");
			say("Czarnoksieznik kieruje swa reke w strone " + victim->query_name(PL_BIE) + ", a z jego dlonie wylatuje strzala raniac "+ victim->koncowka("go", "ja") +" bolesnie.\n");
			victim->reduce_hit_point(100);
		}
		if((szansa >= 23) && (szansa <= 46))
		{
			write("Czarnoksieznik sklada rece wykonujac tajemniczy gest, a spomiedzy jego dloni wyplywaja kule czystej energii uderzajac w twe cialo.\n");
			say("Nagly blysk i jeki " + victim->query_name(PL_BIE) + " swiadcza o bolu doznanym przy zderzeniu z kulami magicznej mocy.\n");
			victim->reduce_hit_point(75);
		}
		if((szansa >= 47) && (szansa <= 71))
		{
			write("Spomiedzy rak czarownika wydobywaja sie z naglym sykiem plomienie parzac cie dotkliwie.\n");
			say("Zapach palonego ciala dochodzacy od strony " + victim->query_name(PL_BIE) + " jest najwyrazniej efektem poparzenia przez czar " + this_object()->query_name(PL_BIE) + ".\n");
			victim->reduce_hit_point(120);
		}
		if((szansa >= 72) && (szansa <= 96))
		{
			write("Mag kieruje w twa strone swe rece, wysylajac forumjac dlonmi stozek wyjatkowego zimna.\n");
			say("Lodowaty powiew skupiajcay sie na " + victim->query_name(PL_NAR) + " jest oznaka rzuconego " + this_player()->koncowka("nan", "na nia") + " zaklecia.\n");
			victim->reduce_hit_point(160);
		}
		if((szansa >= 97) && (szansa <= 98))
		{
			write("\nMag wykrzykuje kilka slow. Nagle przed toba materializuje sie poterzna postura ogrzego berserkera.\n\n");
			say("\nMag wykrzykuje kilka slow. Nagle przed toba materializuje sie poterzna postura ogrzego berserkera.\n\n");
			nowy = clone_object(NPC_PATH + "ogr_orlena");
			nowy->move(environment(this_object()),1);
			nowy->command("krzyknij AAAARRRGGGH!");
			nowy->attack_object(victim);
			przyzwani += ({ nowy });
		}
		if(szansa == 99)
		{
			write("\nNagly bylsk swiatla oslepia cie, a gdy odzyskujesz wzrok dostrzegasz, ze czarnoskieznik stal sie... kilkoma?\n\n");
			say("\nNagly bylsk swiatla oslepia cie, a gdy odzyskujesz wzrok dostrzegasz, ze czarnoskieznik stal sie... kilkoma?\n\n");
			for(i = 0; i < 4; i++)
			{
				nowy = clone_object(NPC_PATH + "mamidlo");
				nowy->move(environment(this_object()),1);
				nowy->attack_object(victim);
				przyzwani += ({ nowy });
			}
			temp = filter(przyzwani, filter_mamidlo);
			victim->attack_object(temp[random(sizeof(temp))]);
		}
	return 1;
}

void
attacked_by (object ob)
{
	command("krzyknij Glupcze!");
	przyzwani->attack_object(ob);
}

int
filter_mamidlo (object a)
{
	if(a->query_real_name() == "orlen")
		return 1;
	return 0;
}

int
pojaw ()
{
	this_object()->move(ROOMS_PATH + "plonacy_czarodziej2.c", 1);
	write("W naglym bylsku fioletowego swiatla otwiera sie portal, z ktorego powolnym krokiem wychodzi stary siwobrody mag.\n");
	return 1;
}

int
zniknij ()
{
	write("W powolnym blasku zielonkawej poswiaty rzeczywistosc zakrzywia sie tworzac wyrwe, w ktora dumnym krokiem wkracza siwy mag.\n");
	this_object()->move(ROOMS_PATH + "temp.c", 1);
	return 1;
}

int
drugie_pytanie()
{
	command("hmm");
	set_alarm(1.0,0.0, "ksiazka");
}

int
ksiazka()
{

    object for_who;
    for_who = this_player();

    if(zajety==1)
    {
	    command("powiedz do " + this_player()->short(PL_DOP) + " Nie przerywaj " + this_player()->koncowka("malpoludzie","malpoludko") + "!");
	    return 1;
    }

    if(query_quest_skonczony(for_who, "Ksiazka_Orlena") == 1)
    {
        command("powiedz do " + this_player()->short(PL_DOP) + " Nic na razie dla ciebie sie nie znajdzie.");
	    command("prychnij");
	    return 1;
    }
    else
    {
        if(query_postep_w_zadaniu("Ksiazka_Orlena", for_who, 0))
        {
            command("powiedz do " + this_player()->short(PL_DOP) + " Znalazles te przekleta ksiazke?!");
            return 1;
        }
        else
        {
            ksiazka_orlena(-1, for_who);
        }
    }
    return 1;
}

ksiazka_orlena(int faza,object pla)
{
    switch(++faza)
    {
	     case 0:

         zajety = 1;

		 command("powiedz do " + this_player()->short(PL_DOP) + " Tak ty mozesz sie przydac.");
		 command("usmiechnij sie podstepnie");

		 set_alarm(1.0, 0.0, "ksiazka_orlena", faza,pla);
		 break;

	     case 1:

	     command("powiedz do " + this_player()->short(PL_DOP) + " Kiedym wracal ze swiatyni Lathandera po drobnej "
		       + "sprzeczce z Kelddathem Ormlyrem...");

	     set_alarm(3.0, 0.0, "ksiazka_orlena", faza,pla);
	     break;

	     case 2:

	     command("powiedz do " + this_player()->short(PL_DOP) + " Musialem zgubic gdzies po drodze pewien bardzo wazny przedmiot...");

	     set_alarm(3.0, 0.0, "ksiazka_orlena", faza,pla);
	     break;

	     case 3:

	     command("powiedz do " + this_player()->short(PL_DOP) + " Jest nim pewna ksiazka zawierajaca bardzo...");

	     set_alarm(1.0, 0.0, "ksiazka_orlena", faza,pla);
		 break;

		 case 4:

	     command("powiedz do " + this_player()->short(PL_DOP) + " Zreszta niewazne co zawiera. Nie twoj interes.");
	     command("prychnij");
		 command("powiedz do " + this_player()->short(PL_DOP) + " Znajdz ja i mi przynies. Wtedy dostaniesz odpowiednia nagrode.");

	     set_alarm(2.0, 0.0, "ksiazka_orlena", faza,pla);
		 break;

		 case 5:    

            dodaj_robiony_quest("Ksiazka_Orlena", pla, 1);
            dodaj_postep_w_zadaniu(pla, "Ksiazka_Orlena");
            zajety = 0;
            break;
    }
}

enter_inv(object ob, object from)
{

   if(function_exists("create_object", ob) == PATH+ "ksiazka")
   {

   command("powiedz do " + this_player()->short(PL_DOP) + " Znalazles ksiazke?");

   if(query_quest_skonczony(from,"List_Candlekeep_Sklep_Karczma"))
   {

   command("powiedz do " + this_player()->short(PL_DOP) + " No nic... Skoro mi ja dajesz.");
   command("machnij reka lekko");

   }
   else
   {

   catch_quest(-1,from);
   }
   }

 ::enter_inv(ob,from);

}

catch_quest(int faza, object pla)
{

    switch(++faza)

    {

    case 0:

    command("emote kartkuje ksiazke uwaznie.");

    set_alarm(4.0, 0.0, "catch_quest", faza,pla);
    break;

    case 1:
   
	command("powiedz do " + this_player()->short(PL_DOP) + " To ta. Nareszcie...");

    set_alarm(1.0, 0.0, "catch_quest", faza,pla);

    break;

    case 2:

    command("popatrz na " + this_player()->short(PL_DOP) + " uwaznie");
    command("powiedz do " + this_player()->short(PL_DOP) + " Dlugo to trwalo. Zbyt dlugo.");

    set_alarm(3.0, 0.0, "catch_quest", faza,pla);

    break;

    case 3:

    command("powiedz do " + this_player()->short(PL_DOP) + " Znaj laske swego Mistrza.");
    write("Mag daje ci trzy zlote monety.\n");
	say(this_object()->qeury_Imie(this_object, PL_MIA) + " daje " + this_player()->qeury_Imie(this_player, PL_CEL) + " trzy zlote monety.\n");

    nagrodz_gracza("Ksiazka_Orlena", pla);

    pla->catch_msg("Czujesz sie ze przybywa ci nieco doswiadczenia.\n");
    MONEY_MAKE_GC(5)->move(pla);

    break;
    }
}

