#pragma strict types

inherit "/std/monster";

#include <macros.h>
#include <stdproperties.h>
#include <language.h>
#include <composite.h>
#include <money.h>
#include <pl.h>
#include <time.h>
#include <formulas.h>
#include <std.h>
#include <filter_funs.h>

int *umy = ({10, 10, 10});

void
create_monster()
{
   
    ustaw_odmiane_rasy(PL_GNOM);

    dodaj_nazwy(PL_MEZCZYZNA);

    dodaj_nazwy(({"jubiler", "jubilera", "jubilerowi", "jubilera", "jubilerem", "jubilerze"}), 
    		({"jubilerzy", "jubilerow", "jubilerom", "jubilerow", "jubilerami", "jubilerach"}), PL_MESKI_OS); 
    
    set_gender(G_MALE);

    set_title("Bindersvalt, Jubiler z Athkatli");

    ustaw_imie( ({"angus","angusa","angusowi","angusa","angusem","angusie"}), PL_MESKI_OS);

    set_long("Ten niski gnom jest niezwykle dystyngowany i w przeciwnosci do innych "
      + "przedstawicieli swej rasy do wielu spraw podchodzi nadzwyczaj powaznie. "
      + "W jego prawym oku dostrzegasz szklany monokl, zas sam jubiler jest ubrany "
      + "we frak, ktory dodaje mu powagi i elegancji. Oglada kazdego klienta w "
      + "calkowitym milczeniu, patrzac czy ktorys z nich nie dybie na jego "
      + "arcydziela bizuterii. Co jakis czas podchodzi do jakiegos jegomoscia "
      + "prezentujac mu zadany wyrob bizuterii.\n");

    set_stats( ({ 23, 23, 25, 45, 45, 45 }) );

    set_skill(SS_DEFENCE, umy[0] + random(20));
    set_skill(SS_PARRY, umy[1] + random(20));
    set_skill(SS_UNARM_COMBAT, umy[2] + random(20));
    

    set_act_time(200);
    add_act("emote przyglada ci sie badawczo, uzywajac do tego swego monokla.");
    add_act("emote podaza na zaplecze, a nastepnie przynosi stamtad piekna obraczke.");
    add_act("emote poprawia bizuterie zgromadzona w gablotkach.");
    add_act("emote poprawia ulozenie swego fraka.");
    add_act("emote poleruje grawerowany pierscionek, uzywajac do tego bialej szmatki.");

    add_act("' Slucham?");
    add_act("' Pragnalbym polecic nasze znakomite wyroby.");
    add_act("' Chcialbym zapewnic panstwa, ze moja bizuteria jest najwyzszej jakosci.");
       
    set_cchat_time(160);
    add_cchat("Zaraz zawolam straz.");
    add_cchat("Nie oddam moich skarbow!");

    set_default_answer(VBFC_ME("default_answer"));

    set_stats ( ({ 45, 45, 70, 45, 45, 60 }) );
    
    set_skill(SS_DEFENCE, 15 + random(10));
    set_skill(SS_UNARM_COMBAT, 40 + random(10));
    set_skill(SS_PARRY, 10 + random(10));
      
    dodaj_przym("dostojny","dostojni");
    dodaj_przym("malomowny","malomowni");

    add_prop(CONT_I_WEIGHT, 40); 
    add_prop(CONT_I_HEIGHT, 100); 
    
}
void
add_introduced(string imie)
{
    set_alarm(2.0, 0.0, "return_introduce", imie);
}

void
return_introduce(string imie)
{
    object osoba;
    osoba = present(imie, environment());
    if (osoba)
        command("powiedz Dzien dobry prosze " + this_player()->koncowka("pana","pania") + ".");
        command("przedstaw sie");
}
string
default_answer()
{
     command("powiedz Mogl" + this_player()->koncowka("bys","abys") + " powtorzyc? Kiepsko ostatnio u mnie ze sluchem...");
}
void
emote_hook(string emote, object wykonujacy, string przyslowek)
{
    switch (emote)
    {
        case "kopnij": set_alarm(2.0, 0.0, "zly", wykonujacy);
        	    break;
        case "spoliczkuj" :
        case "szturchnij":
        case "opluj" : set_alarm(2.0, 0.0, "zly", wykonujacy);
        	    break;
        	    
        case "zasmiej": 
        case "pocaluj": set_alarm(2.0, 0.0, "dobry", wykonujacy);
        	      break;
        case "przytul": set_alarm(2.0, 0.0, "dobry", wykonujacy);
    }
}
void
zly(object kto)
{
    switch (random(4)) 
    {
        
    case 0 : command("powiedz Natychmiast zaprzestan."); break;
    case 1 : command("powiedz Zabierzcie go odemnie."); break;
    case 2 : command("opluj " + OB_NAME(kto)); break;
    case 3 : command("powiedz Chyba lubisz kratki wiezienia, gdyz tam skonczysz!");
    }
}

void
dobry(object kto) 
{
        
    if(kto->koncowka("chlop","baba")=="chlop") zly(kto);
    else
      {
        
    
        switch (random(3)) 
        {
          case 0 : command("powiedz Zobaczymy sie jescze pozniej."); break;
          case 1 : command("emote czerwieni sie."); break;
          case 2 : command("powiedz Przepraszam, lecz teraz pracuje."); break;
          
        }
      }
} 

