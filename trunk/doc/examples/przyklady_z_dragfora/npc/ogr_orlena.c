/*----Ogr berserker----*/
/*---Copyright Volo----*/
/*-----Do expowiska----*/
inherit "/std/monster.c";

#include <stdproperties.h>
#include <macros.h>
#include <wa_types.h>
#include <ss_types.h>
#include "../definicje.h"

int *staty = ({20, 15, 30, 0, 0, 10});
int *umy = ({15, 30, 30});

int f_filter(object ktory);

    string *
    losowy_przym()
{
    string *lp;
    string *lm;
    int i;
    lp=({ "grozny", "silny", "niepokonany", "muskularny", "wielki"});
    lm=({ "grozni", "silni", "niepokonani", "muskularni", "wielcy"});
    i=random(5);
    return ({ lp[i] , lm[i] });
}
    void
    create_monster()
{
    string *przym = losowy_przym();
    ustaw_odmiane_rasy( ({ "berserker", "berserkera", "berserkerowi", "berserkera", "berserkerem", "berserkerze" }), 
                        ({ "berserkerzy", "berserkerow", "berserkerom", "berserkerow", "berserkerami", "berserkerach" }), PL_MESKI_OS);
	dodaj_nazwy( ({"ogr", "ogra", "ogrowi", "ogra", "ogrem", "ogrze" }), ({"ogry", "ogrow", "ogrom", "ogry", "ogrami", "ograch" }), PL_MESKI_OS);
    set_gender(G_MALE);
    dodaj_przym( przym[0], przym[1] );
    dodaj_przym("ogrzy","ogrzy");
    set_long("Przygladasz sie wlasnie najgorszej odmianie ogrzego wojownika - berserkerowi. "
      + "Dzierzac dwureczny topor lecz zupelnie bez zadnej zbroi, moglby on pokonac caly "
      + "oddzial amnijskich rycerzy na skutek wpadniecia w bojowy szal. Osobnik, ktorego w "
      + "tej chwili obserwujesz ma dziki, krwawy usmiech, ktory oznacza ze przebywanie z nim "
      + "w jednym pokoju mogloby skonczyc sie dla ciebie tragiczna smiercia z reki szalenca. "
      + "Kly ogra sa nieco wysuniete do przodu, a muskularne rece zdaja sie miec sile potrzebna "
      + "do miazdzenia najtwardszych skal w tym przekletym lesie. \n");
    add_prop(LIVE_I_NEVERKNOWN, 1);
    add_prop(CONT_I_WEIGHT, 92000);
    add_prop(CONT_I_HEIGHT, 209);
	add_prop(LIVE_I_NO_CORPSE, 1);
    set_stats(({staty[0] + random(30), staty[1] + random(25), staty[2] + random(20), staty[3] + random(15), staty[4] + random(15), staty[5] + random(60)}));
    set_skill(SS_DEFENCE, umy[0] + random(26));
    set_skill(SS_PARRY, umy[1] + random(25));
    set_skill(SS_WEP_AXE, umy[2] + random(40));
    set_cchat_time(10 + random(30));
    add_cchat("Ja zmiazdzysz marny robak, ty!");
    add_cchat("Ty glupiec, ze zapuscic sie tutaj!");
    add_cchat("AAARRRGGGH!");
    add_cchat("Ty nie przezyc!");
    add_cchat("Intruz zostac zdeptany!");
	set_attack_chance(100);
	set_aggressive(1);
    add_weapon(WEAPONS_PATH + "topor");
	set_alarm(60.0, 0.0, "remove_object");
}

/*Funkcja wywolywana gdy ktos zaatakuje berserkera*/

void
attacked_by (object ob)
{
	object *obecni;

	obecni = all_inventory(environment(this_object()));  //Tu dostajemy cala zawartosc pomieszczenia;

	obecni = filter(obecni, f_filter); //Tu filtrujemy tylko berserkerow

	obecni -= ({this_object()}); //Ze wszystkich berserkerow usuwamy tego ktory zostal zaatakowany (juz walczy)

	obecni->command("krzyknij ARRGGGGGGHHHHHHH!");   //Tekst jaki krzycza berserkerzy gdy pomagaja koledze

	obecni->attack_object(ob); //Reszta atakuje napastnika
}

/*Funkcja filtrujaca berserkerow*/

int
f_filter(object ktory)
{
	if(ktory->query_real_name() == "orlen") //Jesli obiekt ktory sprawdzamy to berserker
		return 1;                          //<------------ To zwacamy 1
	return 0;                              //<------------ W przeciwnym wypadku zwaracamy 0
}
/*----Podpisano Volothamp Geddarm----*/

void
remove_object()
{
     command("emote rozplywa sie w powietrzu na skutek przelamania zaklecia.");
	all_inventory(this_object())->remove_object();
	::remove_object();
}

