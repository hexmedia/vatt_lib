/*
 * Filename: wartownik.c
 *
 * Zastosowanie: Przybramowi wartownicy. ;)
 * 
 * Copyright Volothamp Geddarm.
 *
 */
inherit "/std/monster.c";

#include <macros.h>
#include <stdproperties.h>
#include <language.h>
#include <composite.h>
#include <money.h>
#include <pl.h>
#include <time.h>
#include <formulas.h>
#include <std.h>
#include <mudtime.h>
#include "../definicje.h"

int *staty = ({35, 40, 45, 10, 10, 35});
int *umy = ({25, 20, 30});

int f_filter(object ktory);

    string *
    losowy_przym()
{
    string *lp;
    string *lm;
	string *lpa;
	string *lma;
    int i, j;
    lp=({ "hardy", "czujny", "barczysty", "uwazny", "masywny"});
    lm=({ "hardzi", "czujni", "barczysci", "uwazni", "masywni"});
	lpa=({ "milczacy", "grozny", "ogorzaly", "czarnowlosy", "wasaty"});
	lma=({ "milczacy", "grozni", "ogorzali", "czarnowlosi", "wasaci"});
	i = random(5);
	j = random(5);
	return ({ lp[i] , lm[i], lpa[j], lma[j] });
}
    void
    create_monster()
{
    string *przym = losowy_przym();
	ustaw_odmiane_rasy( ({"wartownik", "wartownika", "wartownikowi", "wartownika", "wartownikiem", "wartowniku" }),
		                ({"wartownicy", "wartownikow", "wartownikom", "wartownikow", "wartownikami", "wartownikach" }), PL_MESKI_OS);
    set_gender(G_MALE);
	dodaj_nazwy(PL_MEZCZYZNA);
    
	dodaj_przym( przym[0], przym[1] );
    dodaj_przym( przym[2], przym[3] );
    
	set_long("Spogladasz wlasnie na umiesnionego, dojrzalego mezczyzne, ktorego jedynym "
	  + "zadaniem jest strzezenie bram miasta. Wyglada on jednoczesnie na uwaznego i "
	  + "czujnego, a rowniez na hardego i pewnego siebie. Ubrany jest w skorzany pancerz i "
	  + "tak naprawde to nie przypomina on zakutych w stal gwardzistow jacy kreca sie "
	  + "po kupieckich miastach lecz po prostu lekko uzbrojonego mezczyzne o szerokich barach, "
	  + "bacznie oglodajacego kazda osobe jaka wkracza do miasta przez drewniane wrota.\n");
    
	add_prop(LIVE_I_NEVERKNOWN, 1);
    add_prop(CONT_I_WEIGHT, 80000);
    add_prop(CONT_I_HEIGHT, 175);
    
	set_stats(({staty[0] + random(20), staty[1] + random(20), staty[2] + random(10), staty[3] + random(5), staty[4] + random(5), staty[5] + random(40)}));
    
	set_skill(SS_DEFENCE, umy[0] + random(10));
    set_skill(SS_PARRY, umy[1] + random(10));
    set_skill(SS_WEP_SWORD, umy[2] + random(10));
    
	set_cchat_time(20 + random(30));
    add_cchat("Juz ja ci pokaze.");
    add_cchat("Nie dosiegnie mnie ani twa dlon ani twoj jezyk zuchwalcze.");

	set_act_time(40 + random(20));
	add_act("emote maszeruje szybko nieopodal wrot.");
	add_act("emote wykonuje kilka szybkich ruchow dla rozruszania kosci.");
	add_act("emote przyglada ci sie bardzo czujnie.");
	add_act("ziewnij");

	set_chat_time(55 + random(20));
	add_chat("Uwazasz sie za mocnego?");
   add_chat("Nie wszczynaj bojek w miescie. Zaplacilbys za to zyciem.");
	add_chat("Kelddath dobrze mnie wynagradza. Nie mam zamiaru rezygnowac.");

	add_armour("/d/Faerun/Miasta/beregost/zbroje/zbroja_skorzana");
	add_armour("/d/Faerun/Miasta/beregost/odzienie/sandaly");
add_weapon("/d/Faerun/Miasta/beregost/bronie/miecz");

	set_default_answer(VBFC_ME("default_answer"));
}
void
add_introduced(string imie_mia, string imie_bie)
{
	set_alarm(2.0, 0.0, "return_introduce", find_player(imie_mia));
}
void
return_introduce(object ob)
{
    command("pokiwaj glowa powitalnie");
}
void
attacked_by (object ob)
{
	object *obecni;

	obecni = all_inventory(environment(this_object())); 

	obecni = filter(obecni, f_filter); 

	obecni -= ({this_object()}); 

	obecni->command("krzyknij Pomozmy mu! Brac lotra!");  

	obecni->attack_object(ob); 
}
int
f_filter(object ktory)
{
	if((ktory->query_rasa() == "gwardzista") || (ktory->query_rasa() == "straznik")) 
		return 1;                         
	return 0;                          
}
    string
	default_answer()
{
	command("emote przyglada ci sie ze zdziwieniem.");
    set_alarm(3.0, 0.0, "zdziwienie");
}
    void
    emote_hook(string emote, object wykonujacy, string przyslowek)
{
    switch (emote)
    {
        case "kopnij": set_alarm(2.0, 0.0, "zly", wykonujacy);
        	    break;
        case "spoliczkuj" :
        case "szturchnij":
        case "opluj" : set_alarm(2.0, 0.0, "zly", wykonujacy);
        	    break;
        	    
        case "zasmiej": 
        case "pocaluj": set_alarm(2.0, 0.0, "dobry", wykonujacy);
        	      break;
        case "przytul": set_alarm(2.0, 0.0, "dobry", wykonujacy);
    }
}



void
zly(object kto)
{
    switch (random(3)) 
    {
        
    case 0 : command("krzyknij Masz szczescie, ze cie jeszcze ignoruje!!"); break;
    case 1 : command("powiedz do " + kto->short(PL_DOP) + " Zjezdzaj blaznie."); break;
    case 2 : command("powiedz do " + kto->short(PL_DOP) + " Lepiej sie uspokoj zanim strace cierpliwosc."); break;
    default:
    }
}

void
dobry(object kto) 
{
        
    if(kto->koncowka("chlop","baba")=="chlop") zly(kto);
    else
      {
        
    
        switch (random(3)) 
        {
          case 0 : command("powiedz Moglabys to powtorzyc?"); break;
          
        }
      }
} 
void
zdziwienie(object pla)
{
    command("powiedz Niestety. Nie wiem o co ci chodzi.");
}

/*----Podpisano Volothamp Geddarm----*/




