inherit "/std/monster.c";

#include <macros.h>
#include <stdproperties.h>
#include <language.h>
#include <composite.h>
#include <money.h>
#include <pl.h>
#include <time.h>
#include <formulas.h>
#include <std.h>
#include <mudtime.h>
#include "../definicje.h"

int *staty = ({50, 50, 50, 50, 50, 50});
int *umy = ({50, 50, 50, 50, 50, 50});

int f_filter(object ktory);
int sprawdzanie ();

    string *
    losowy_przym()
{
    string *lp;
    string *lm;
	string *lpa;
	string *lma;
    int i, j;
    lp=({ "mlody", "silny", "barczysty", "smukly", "niski"});
    lm=({ "mlodzi", "silni", "barczysci", "smukli", "niscy"});
	lpa=({ "czarnowlosy", "energiczny", "jasnowlosy", "nerwowy", "spiczastouchy"});
	lma=({ "czarnowlosi", "energiczni", "jasnowlosi", "nerwowi", "spiczastousi"});
	i = random(5);
	j = random(5);
	return ({ lp[i] , lm[i], lpa[j], lma[j] });
}
    void
    create_monster()
{
    string *przym = losowy_przym();
	ustaw_odmiane_rasy(            ({"gwardzista","gwardzisty","gwardziscie","gwardziste","gwardzista","gwardziscie"}),
            ({"gwardzisci","gwardzistow","gwardzistom","gwardzistow","gwardzistami","gwardzistach"}),
            PL_MESKI_OS);
    set_gender(G_MALE);
	dodaj_nazwy(PL_MEZCZYZNA);
    
	dodaj_przym( przym[0], przym[1] );
    dodaj_przym( przym[2], przym[3] );
    
	set_long("Masz przed soba wysokiego na okolo trzy i pol stopy, mlodego mezczyzne "
	       + "o twardych rysach, ogorzalej twarzy, a takze atletycznych muskulach. "
		   + "Jego sylwetka swiadczy o tym, iz musi byc on naprawde krzepki w krzyzu "
		   + "i zapewne wlasnie dlatego to on strzeze porzadku w Beregoscie. Oplacany "
		   + "jest ten zoldak przez Kelddatha Ormlyra, arcykaplana ze swiatyni Lathandera "
		   + "i burmistrza miasteczka. Sadzisz jednak, ze walka z jakimkolwiek gwardzista nie bylaby "
		   + "latwa, gdyz aby zapewnic bezpieczenstwo miastu najwyzszy kaplan Lathandera "
		   + "musial wpierw upewnic sie, ze kazdy stroz prawa jest dostatecznie silny i "
		   + "dobrze przeszkolony na wypadek starcia.\n");
    
	add_prop(LIVE_I_NEVERKNOWN, 1);
    add_prop(CONT_I_WEIGHT, 85000);
    add_prop(CONT_I_HEIGHT, 175);
    
	set_stats(({staty[0] + random(10), staty[1] + random(10), staty[2] + random(10), staty[3] + random(10), staty[4] + random(10), staty[5] + random(10)}));
    
	set_skill(SS_DEFENCE, umy[0] + random(10));
    set_skill(SS_PARRY, umy[1] + random(10));
    set_skill(SS_WEP_SWORD, umy[2] + random(10));
	set_skill(SS_WEP_AXE, umy[3] + random(10));
	set_skill(SS_SHIELD_PARRY, umy[4] + random(10));
	set_skill(SS_WEP_CLUB, umy[5] + random(10));
    
	set_cchat_time(30 + random(30));
    add_cchat("Ku chwale Beregostu!");
    add_cchat("Nie uciekniesz zbrodniarzu!");
	add_cchat("Nawet nie mysl o ucieczce!");
	add_cchat("W imie prawa!");
	add_cchat("Poddaj sie!");

	set_cact_time(30 + random(30));
	add_cact("emote chwyta pewnie swa bron.");

	set_act_time(50 + random(20));
	add_act("emote klania sie przechodniom.");
	add_act("emote spoglada do gory.");
	add_act("emote opiera sie na broni.");
	add_act("ziewnij");
	add_act("emote rozglada sie czujnie.");
	add_act("ziewnij");
	add_act("emote rozglada sie czujnie.");

	set_chat_time(55 + random(20));
	add_chat("Nie wszczynaj w miescie rozrob.");
	add_chat("W imie Kelddatha strzege tutaj prawa.");

add_weapon("/d/Faerun/Miasta/beregost/bronie/bekart.c");
	add_armour("/d/Faerun/Miasta/beregost/zbroje/buty.c");
	add_armour("/d/Faerun/Miasta/beregost/zbroje/kolczuga.c");
	add_armour("/d/Faerun/Miasta/beregost/zbroje/helm.c");
	add_armour("/d/Faerun/Miasta/beregost/odzienie/spodnie.c");
	add_armour("/d/Faerun/Inne/pomocna/odzienie/szeroki_pas.c");

	set_default_answer(VBFC_ME("default_answer"));

	set_restrain_path("/d/Miasta/Faerun/beregost/lokacje");

	set_aggressive(sprawdzanie);
}
void
attacked_by (object ob)
{
	object *obecni;

	obecni = all_inventory(environment(this_object())); 

	obecni = filter(obecni, f_filter); 

	obecni -= ({this_object()}); 

	obecni->command("krzyknij Trzymaj sie!");  

	obecni->attack_object(ob); 
}



int
f_filter(object ktory)
{
	if((ktory->query_rasa() == "gwardzista") || (ktory->query_rasa() == "straznik")) 
		return 1;                         
	return 0;                          
}
    string
	default_answer()
{
	command("emote zastanawia sie.");
    set_alarm(3.0, 0.0, "zdziwienie");
}
    void
    emote_hook(string emote, object wykonujacy, string przyslowek)
{
    switch (emote)
    {
        case "kopnij": set_alarm(2.0, 0.0, "zly", wykonujacy);
        	    break;
        case "spoliczkuj" :
        case "szturchnij":
        case "opluj" : set_alarm(2.0, 0.0, "zly", wykonujacy);
        	    break;
        	    
        case "zasmiej": 
        case "pocaluj": set_alarm(2.0, 0.0, "dobry", wykonujacy);
        	      break;
        case "przytul": set_alarm(2.0, 0.0, "dobry", wykonujacy);
    }
}



void
zly(object kto)
{
    switch (random(5)) 
    {
        
    case 0 : command("krzyknij Przestan glupcze bo pozalujesz!!"); break;
    case 1 : command("powiedz do " + kto->short(PL_DOP) + " Nie umiesz sie dobrze zachowywac?"); break;
    case 2 : command("zignoruj " + kto->short(PL_BIE)); break;
    default:
    }
}

void
dobry(object kto) 
{
        
    if(kto->koncowka("chlop","baba")=="chlop") zly(kto);
    else
      {
        
    
        switch (random(3)) 
        {
          case 0 : command("usmiechnij sie promiennie"); break;
          
        }
      }
} 
void
zdziwienie(object pla)
{
    command("powiedz Niestety nie wiem o co ci chodzi.");
}

int
sprawdzanie()
{
	if(GWARDIA->query_czy_zly(this_player()))
	{
		return 1;
	}
	return 0;
}

void
do_die(object killer)
{
    object *nasi;
    int i,j,k;

    nasi = query_team();
    if(k = sizeof(nasi))
    {
        for(j = 0;j < k; j++)
                this_object()->team_leave(nasi[j]);
        for(j = 1; j < k;j++)
        {
                nasi[0]->team_join(nasi[j]);
                nasi[0]->add_following(nasi[j]);
        }
        nasi[0]->set_random_move(50);
    }
    ::do_die(killer);    
}
/*----Podpisano Volothamp Geddarm----*/

