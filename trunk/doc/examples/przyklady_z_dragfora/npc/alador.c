/*----Ribald Kramarz----*/
/*----Copyright Volo----*/
/*----Sklepikarz na TP--*/
/*Simon Quest*/
#pragma strict types

inherit "/std/monster";
inherit "/d/Faerun/std/questy";

#include <macros.h>
#include <stdproperties.h>
#include <language.h>
#include <composite.h>
#include <money.h>
#include <pl.h>
#include <time.h>
#include <formulas.h>
#include <std.h>
#include <mudtime.h>
#include "../definicje.h"

static object *pomocnicy=({});

int zajety;
void
create_monster()
{
    ustaw_imie(({ "alador", "aladora", "aladorowi", "aladora", "aladorem", "aladorze"}), PL_MESKI_OS);
	dodaj_nazwy(PL_MEZCZYZNA);
    dodaj_przym("tegi","tedzy");
    dodaj_przym("brodaty","brodaci");
    ustaw_odmiane_rasy(PL_CZLOWIEK);
    set_stats( ({ 52, 42, 53, 42, 42, 78 }) );
    set_gender(G_MALE);
    add_prop(CONT_I_WEIGHT, 78000); 
    add_prop(CONT_I_VOLUME, 35000); 
    add_prop(CONT_I_HEIGHT, 175);
    set_act_time(90);
    add_act ("chrzaknij");
    add_act ("emote czeka na cos niecierpliwie.");
    add_act ("emote rozglada sie czujnie");
    set_chat_time(120);
    add_chat("Hej przyjacielu. Szukam pomocy.");
    add_chat("Slyszales o kims takim jak Marius?");
    add_chat("Na pewno nie plywales po morzach tyle co ja.");
    add_chat("Moze rozgladasz sie za latwa praca?");
    set_cchat_time(20);
    add_cchat("Kto podniosl reke na Aladora wilka nie zyl dostatecznie dlugo by o tym opowiedziec!");
    add_cchat("Ty glupcze.");
    add_cchat("Poniesiesz sromotna kleske glupcze. Solennie ci to obiecuje");
    add_cchat("Niech bostwa morskie przeklna twa dusze szczurze ladowy!");
	set_default_answer(VBFC_ME("default_answer"));
	add_weapon(BRONIE+"miecz6");

  
    set_long("Obserwujesz wlasnie niezwykle ciekawa osobistosc. Jest to jeden z wielu wilkow "
	  + "morskich jakich zawsze mozna spotkac w porcie, gdy do brzegu przybija jakis okret. Ci "
	  + "najbardziej lubia popic sobie wtedy cieplej gorzalki badz od razu poszukac zalogi na kolejny "
	  + "rejs. regula jest, ze takie osoby jak ten oto mezczyzna czesto sa kapitanami jakichs zruinowanych "
	  + "okretow pirackich badz zwyklych szkut szmuglerskich, totez nie warto sie z nimi zadawac, gdyz czesto "
	  + "jest to rownie niebezpieczne jak walka z czterema wielorybami, ktore w tej chwili tratuja twoj statek. \n");
	add_ask(({"pomoc", "prace", "zadanie"}),VBFC_ME("skrzynka"));
}
    void
    add_introduced(string imie_mia, string imie_bie)
{
	set_alarm(2.0, 0.0, "return_introduce", find_player(imie_mia));
}
    void
    return_introduce(object ob)
{
    command("przedstaw sie " + OB_NAME(ob));
    command("podaj reke " + OB_NAME(ob));
	command("powiedz Witaj przyjacielu. Moze znasz kogos takiego jak Marius?");
}
    string
	default_answer()
{
	command("powiedz Nie rozumiem co do mnie mowisz. Czy zechcial" + this_player()->koncowka("bys", "abys")
    + " wyrazac sie nieco jasniej?");
}
    void
    emote_hook(string emote, object wykonujacy, string przyslowek)
{
    switch (emote)
    {
        case "kopnij":
        case "spoliczkuj" :
        case "szturchnij":
        case "opluj" : set_alarm(2.0, 0.0, "zly", wykonujacy);
        	    break;
        	    
        case "zasmiej": 
        case "pocaluj": set_alarm(2.0, 0.0, "dobry", wykonujacy);
        case "przytul": set_alarm(2.0, 0.0, "dobry", wykonujacy);
				break;
	}
}



void
zly(object kto)
{
    switch (random(5)) 
    {
		case 0 : command("powiedz Sprobuj jeszcze raz, a przetrace ci wszystkie kosci. Obiecuje..."); break;
	    case 1 : attack_object(kto); break;
		case 2 : command("opluj " + OB_NAME(kto)); break;
		case 3 : command("kopnij " + OB_NAME(kto)); break;
		case 4 : command("powiedz do " + kto->short(PL_BIE) + " Niejeden probowal mnie pokonac. Zadnemu sie nie udalo!"); break;
		default:
    }
}

void
dobry(object kto) 
{
        
    if(kto->koncowka("chlop","baba")=="chlop") zly(kto);
    else
      {
        
    
        switch (random(3)) 
        {
          case 0 : command("powiedz Zobaczymy sie jescze pozniej."); break;
          case 1 : command("powiedz No, no slicznotko. Nie przesadzaja z tymi czuloscami."); break;
          case 2 : command("powiedz Dawno nie zaznalem pocalunku kobiety. Moglabys to powtorzyc skarbie?"); break;
        }
      }
}

skrzynka()
{
    object for_who;
    int test;
    for_who = this_player();
    
    if(zajety==1)
    {
	    command("powiedz Poczekaj chwilke daj mi skonczyc.");
	    return 1;
    }
    
    if(query_quest_skonczony(for_who,"Skrzynka_Czarna_Crimmor")==1)//jesli jeden to questbit zostal ustawiony
    {
    	command("powiedz do " + lower_case(for_who->query_name(PL_DOP)) + " Skoro znalazles Mariusa to nie mam dla ciebie juz wiecej zadan.");
        return 1;
    }
    else
    {
              if(query_postep_w_zadaniu("Skrzynka_Czarna_Crimmor",for_who,0))
              {
	              command("powiedz do " + lower_case(for_who->query_name(PL_DOP)) + " I jak? Znalazles tego Mariusa?");
                  return 1;
              }   
              else
              {
                  command("powiedz do " + lower_case(for_who->query_name(PL_DOP)) + " Chyba sami bogowie zeslali mi dzisiaj ciebie!");
 			      command("emote wskazuje czarna skrzynke.");           
 		          set_alarm(10.0, 0.0, "czarna_skrzynka", for_who);
              }
    }    
}

void
czarna_skrzynka(object pla)
{   
    if(!present(pla,environment(this_object()))) return;
    command("powiedz do " + lower_case(pla->query_name(PL_DOP)) + " Widzisz to? Znalazlem te skrzyneczke juz dawno temu.");
    command("powiedz do " + lower_case(pla->query_name(PL_DOP)) + " Na jej wieku znajduje sie wyryty napis: 'Niechaj klatwa spocznie na rodzie Wilfenow'");
    set_alarm(10.0, 0.0, "czarna_skrzynka1", pla);
}
void
czarna_skrzynka1(object pla)
{
    if(!present(pla,environment(this_object()))) return;
    command("powiedz do " + lower_case(pla->query_name(PL_DOP)) + " Jak zapewne zauwazyles skrzynke zamknieto na jakas magiczna klodke.");
    command("namysl sie nad kluczem");
	set_alarm(10.0, 0.0, "czarna_skrzynka2", pla);
}
void
czarna_skrzynka2(object pla)
{
    if(!present(pla,environment(this_object()))) return;
    command("powiedz do " + lower_case(pla->query_name(PL_DOP)) + " Slyszalem ze ktos z rodziny Wilfenow zyje tu w tym miescie.");
    command("emote usmiecha sie smutno.");
    set_alarm(10.0, 0.0, "czarna_skrzynka3", pla);
}
void
czarna_skrzynka3(object pla)
{
    if(!present(pla,environment(this_object()))) return;
    command("powiedz do " + lower_case(pla->query_name(PL_DOP)) + " Po to tu przybylem by oddac ja prawowitemu wlascicielowi. Ale niestety musze wracac do swoich rodzinnych stron na dalekiej polnocy...");
	set_alarm(10.0, 0.0, "czarna_skrzynka4", pla);
}
void
czarna_skrzynka4(object pla)
{
    if(!present(pla,environment(this_object()))) return;
	command("powiedz do " + lower_case(pla->query_name(PL_DOP)) + " Jezeli dostarczysz te skrzynke Wilfenowi bede ci wdzieczny. Jestem tez pewny, iz on rowniez.");
    clone_object(NPC_PATH+"skrzynka.c")->move(this_object());
    command("daj skrzynke "+lower_case(pla->query_name(PL_CEL)));
    dodaj_robiony_quest("Skrzynka_Czarna_Crimmor",pla,1);
    dodaj_postep_w_zadaniu(pla,"Skrzynka_Czarna_Crimmor");
    zajety=0;
}

/*----Podpisano: 'Volothamp Geddarm'----*/


