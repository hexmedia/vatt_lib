#include "../pal.h"

inherit "/std/monster";

void create_monster()
{
	int topor_on;
	
    ustaw_imie(({"gruth","grutha","gruthowi","grutha","gruthem","gruthu"}),
                PL_MESKI_OS);
    set_title("Firestorm, Kupiec z Palanthasu");

    ustaw_odmiane_rasy(PL_KRASNOLUD);

    set_gender(G_MALE);
    
    set_long("Krepy, prawie kwadratowy brodacz stojacy przed toba, z cala pewnoscia jest "
    +"reprezentantem dumnej rasy krasnoludzkiej. To bardzo dobrze swiadczy o poziomie "
    +"sklepu, gdyz nikt nie zna sie na broni tak jak krasnoludy. Na jego twarzy widzisz "
    +"sporych rozmiarow szrame biegnaca od ucha przez policzek i chowajaca sie w gaszczu "
    +"rudej brody. Blizna sugerowac moze, ze byl niegdys wojownikiem, jednak porzucil "
    +"zycie wojownika dla czegos spokojniejszego.\n");

    dodaj_przym("krepy","krepi");
    dodaj_przym("rudy","rudzi");

        set_stats(({60,65,90,30,40,55}));

        set_cact_time(4);
        add_cact("krzyknij Atakujesz mnie?! Zginiesz wiec, przysiegam na moja brode!");
        add_cact("krzyknij Posmakuj ostrza mojego topora!");

        set_act_time(10 + random(20));
        add_act("emote przynosi z magazynu jakis topor.");
        add_act("emote pokazuje bron klientowi.");        

    set_skill(SS_DEFENCE, 40);
    set_skill(SS_PARRY, 60);
    set_skill(SS_WEP_AXE, 70);
    
    add_armour(ZBR+"kaftan.c");
    add_armour(ZBR+"spodnie.c");

    set_alignment(50); 
    topor_on=0; 
}

void add_introduced(string kto)
{
     set_alarm (2.0,0.0,"intro",find_player(kto));
}

void intro(object ob)
{
    if (objectp(ob)) 
        command ("przedstaw sie "+OB_NAME(ob));
}

public void bron_sie()
{
    int topor_on;
    command("emote w blyskawicznym tempie wydobywa spod lady swoj topor!"); 
    if (topor_on==0)
    {
    	(clone_object(BRN+"topor1.c")->move(TO));
    	topor_on=1;
    }
    command("dobadz topora");
    command("krzyknij Urrraaaa!");
}


void attacked_by(object wrog)
{
    command("krzyknij Tak? Walczym wiec!");
    set_alarm(0.1, 0.0, "bron_sie");
    return ::attacked_by(wrog);
}
