/*
 * Przyklad najprostszego sklepu.
 */

inherit "/std/room"; /* ten obiekt jest pokojem */
inherit "/lib/shop"; /* ten obiekt jest sklepem */


#include <stdproperties.h> /* standardowe wlasciwosci */
#include <object_types.h> /* standardowe typy objektow */

private int czy_sprzedawc; // ustalamy czy w tym sklepie mozna sprzedwac

/* prototypy */
int dodaj_sprzedawalne(int war);

void
create_room()
{
    /* Zmiena czy pokazywac ustala indywidualne ustawienia sklepu
     * Okre la czy w danym sklepie mo�na sprzedawa� przedmioty czy nie
     * 1 - mo�na
     * 0 - niemo��a 
     */
    czy_sprzedawc=1; 

    // teraz dodajemy rodzaje obiektow ktore gracz moze sprzedac
    // i jednczesnie tylko te obiekty bede sie pojawiac na liscie
    dodaj_sprzedawalne(O_ZBROJE);
    // dodaj_sprzedawalne(0);

    set_short("Przykladowy sklep.\n");
    set_long("Znajdujesz sie w przykladowym sklepie. Wydaje sie byc "+
        "bardzo... niewykonczony. Na scianie wisi jakas tabliczka. "+
        "Oprocz niej jest jeszcze wyjscie do magazynu, bedacego "+
        "na zachod stad.\n");
        
    config_default_trade(); /* ustawia standardowe wartosci */
    
    set_store_room("/doc/examples/handel/magazyn.c");
    add_item( ({ "instrukcje", "tabliczke" }), "Tabliczka ze wskazowkami "+
        "dla klientow.\n");
        
    add_cmd_item( ({ "instrukcje", "tabliczke" }), "przeczytaj",
        "@@standard_shop_sign");
    
    add_prop(ROOM_I_INSIDE, 1); /* to jest pokoj, a nie lokacja na zewnatrz */
    
    add_exit("magazyn.c", "zachod", "@@wiz_check", 1); /* do magazynu */
    				/*  Sprawdza czy gostek moze tam wejsc */
}



/*
 * Function name:   dodaj_sprzedawalne
 * Description:     Funkcja funkjca ustala jakiego typu elementy maja
 *                  byc dopuszczone do sprzedarzy
 * Arguments:       int argument w postacie O_UBRANIA | O_ZBROJE
 *                  domyslnie ustawiony na zero czyli dopuszczamy wszystkie elementy
 * Autor            Nunas
 */
int
dodaj_sprzedawalne(int war=0)
{
    dostepne_obiekty=war;
    return 1;
}


/*
 * Function name:   niema
 * Description:     Funkcja wywolywana kiedy  czy_pokazywac = 0 
 * Arguments:       string z parametrami
 * Autor            Nunas
 */
int
niema(string str)
{
    notify_fail("W tym sklepie nie mo�na sprzedawa�, ani wycenia�.\n");
    return 0;
}


// ===== NADPISUJEMY FUNKCJE Z SHOP - Nunas

/*
 * Function name:   do_sell
 * Description:     Nadpisana funkcja w zaleznosci od ustawien 
 *                  je�eli czy_pokazywac = 0 to wywolujemy fun niema
                    je�eli czy_pokazywac = 1 to wywo�ujemy rodzicza ::do_sell
 * Arguments:       string z parametrami
 * Autor            Nunas
 */
int
do_sell(string str)
{
    if(czy_sprzedawc==0)
    {
        return niema(str);
    } else 
    {
        return ::do_sell(str);
    }
}

/*
 * Function name:   do_value
 * Description:     Nadpisana funkcja w zaleznosci od ustawien 
 *                  je�eli czy_pokazywac = 0 to wywolujemy fun niema
                    je�eli czy_pokazywac = 1 to wywo�ujemy rodzicza ::do_value
 * Arguments:       string z parametrami
 * Autor            Nunas
 */

int
do_value(string str)
{
    if(czy_sprzedawc==0)
    {
        return niema(str);
    } else 
    {
        return ::do_value(str);
    }
}
/*
 * Function name: standard_shop_sign
 * Description:   Nadpisana funkcja w zaleznosci od ustawien 
 *                  je�eli czy_pokazywac = 0 to zamieniamy string
 * Arguments:     none
 * Returns:       message string
 */

string
standard_shop_sign()
{
    if(czy_sprzedawc==0)
    {
    return
    "Oto przyklady tego, co mozesz tu uczynic:\n" +
    "    kup miecz za zlote monety    (standardowo najmniejsza denominacja)\n" +
    "    kup miecz za zloto i wez miedziaki reszty\n" +
    "    pokaz              - Pozwoli ci obejrzec dokladnie jakis przedmiot\n"+
    "                         z magazynu, zanim podejmiesz decyzje o kupnie go.\n"+
    "    przejrzyj          - Pozwala ci przejrzec zawartosc magazynu. Mozesz\n"+
    "                         tez 'przejrzec zbroje' i 'przejrzec bronie'.\n\n" +
    "    W tym sklepie nie mozesz sprzedawac!!!\n";
    } else 
    {
        return ::standard_shop_sign();
    }
}

// ===== KONIEC NADPISYWANIA FUNKCJI Z SHOP - Nunas



void
init()
{
    ::init(); /* MUSI byc wywolane w kazdym inicie */
    init_shop(); /* dodanie komend takich jak 'kup', 'sprzedaj' itp */
}



