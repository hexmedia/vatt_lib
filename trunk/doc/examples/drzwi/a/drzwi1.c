/**
 * Przyk�adowe drzwi.
 *
 * Przyk�ad z Arkadii dostosowany do nowego standardu drzwi przez Kruna.
 * @date Grudzie� 2007
 */
inherit "/std/door";

#include <pl.h>
#include "definicje.h"

void
create_door()
{
    ustaw_nazwe( ({ "drzwi", "drzwi", "drzwiom", "drzwi", "drzwiami",
        "drzwiach" }), PL_NIJAKI_OS);
        //Nazwa drzwi

    dodaj_przym("stalowy", "stalowi");
        //Przymiotniki drzwi

    set_other_room(SCIEZKA + "pokoj2.c");
        //�ciezka do pokoju gdzie b�dzie druga strona drzwi

    set_door_id(KOD_DRZWI);
        //Unikalne id drzwi

    set_long("Masz przed sob� stalowe drzwi. Ich masywny wygl�d "+
        "stwarza w tobie pewno��, ze to co znajduje sie za nimi " +
        "jest ca�kowicie bezpieczne. Pod uchwytem widac zamek.\n");
        //A to opis drzwi. Mo�emy te� u�y� set_door_desc - tak by�o dwaniej -
        //ale najlepiej chyba standardowy set_long:)

    set_open_desc("Na wchodniej �cianie daj� si� zauwa�y� otwarte stalowe "+
        "drzwi.\n");
    set_closed_desc("Na wschodniej �cianie daj� si� zauwa�y� zamkni�te "+
        "stalowe drzwi.\n");
        //Funkcjami tymi ustawiamy opis drzwi w longu room'a w kt�rym si� znajduj�.
        //Je�li nie chcemy ich w longu... to ustawiamy "" lub nie wywo�ujemy tej funkcji.
        //Bezpieczniejsze jest ustawienie "" bo nigdy nie wiadomo co komu przyjdzie do
        //g�owy i jak zedytuje muda:P

    set_pass_command("wsch�d", "przez stalowe drzwi na wsch�d", "ze zachodu, zza stalowych drzwi");
        //Zamiast wpisywa� to tam, mo�emy r�wnie� ustawi� to tak jak robi�o si� to dawniej:
    set_pass_mess("Przechodzisz przez stalowe drzwi na wsch�d.\n");
        //ale nie jest to polecane, chyba, �e ju� gdzie� w kodzie chcesz zmieni� te komunikaty.

    set_key(KOD_KLUCZA);
        //Tylko kluczem z tym kodem b�dzie mo�na drzwi otworzy�.

    set_lock_name(({"zamek", "zamka", "zamkiem", "zamek", "zamkiem", "zamku"}), 0, PL_MESKI_NOS_NZYW);
        //A tu ustawiamy pe�n� odmian� liczby pojedynczej nazwy zamka, je�li nazwa jest w s�owniku
        //to mo�emy j� doda� tylko za pomoc� mianownika, jednak akurat nazwa zamek odmienia si� r�nie
        //w zale�no�ci od tego czy jest to zamek w drzwiach czy zamek w kt�rym si� mieszka, dlatego
        //t� nazw� zawsze nale�y odmieni�, aby p�niej nie by�o niedom�wie�. Przecie� wystarczy kopiuj
        //wklej:)

    set_lock_desc("Ot co! Zwyk�y zamek! Wk�adasz klucz i otwierasz.\n");
        //Ustawiamy opis zamka.
}