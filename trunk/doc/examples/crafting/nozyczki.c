inherit "/std/object";
inherit "/lib/tool";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <macros.h>
#include <materialy.h>
#include <tools.h>

void
create_object()
{
    ustaw_nazwe("no�yczki");

    set_long("Przyk�adowe no�yczki krawieckie. UWAGA! Same szyj�!\n");

    add_prop(OBJ_I_WEIGHT, 500);
    add_prop(OBJ_I_VOLUME, 200);
    add_prop(OBJ_I_VALUE, 1000);

    ustaw_material(MATERIALY_STAL);
    add_tool_domain(TOOL_DOMAIN_TAILOR);
}

public mixed
notify_use_tool(object ob, int domain, int sslvl)
{
    return ::notify_use_tool();
}
