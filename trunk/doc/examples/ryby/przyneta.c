inherit "/std/object";

void
create_object()
{
    ustaw_nazwe("rybka");
}

/* Zwraca rodzaj przynety. Mozliwe wartosci to:
 * 'chleb', 'robak', 'rybka'
 */
string
query_przyneta_typ()
{
    return "rybka";
}
