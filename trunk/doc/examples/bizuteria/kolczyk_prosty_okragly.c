#include <pl.h>
#include <materialy.h>
#include <object_types.h>
#include "/sys/stdproperties.h"
inherit "/std/kolczyk.c";

void
create_kolczyk()
{
    dodaj_przym("prosty","pro�ci");
    dodaj_przym("okr�g�y","okr�gli");
    set_long("Zdaje si�, �e jest to kolczyk o najprostrzym "+
         "wzorze, jakie kiedykolwiek wymy�olono. "+
         "Sk�ada si� jedynie z ma�ego fragmentu stalowego "+
         "drutu zgi�tego na kszta�t ko�a.\n");

    add_prop(OBJ_I_VALUE, 15);
    add_prop(OBJ_I_VOLUME, 10);
    add_prop(OBJ_I_WEIGHT, 5);
    set_type(O_BIZUTERIA);
    ustaw_material(MATERIALY_STAL);
}