/**
 * \file /doc/examples/bronie/strzeleckie/strzala.c
 *
 * Przyk�adowa o�owiana kulka do procy.
 *
 * @author Krun
 * @date Stycze� 2008
 */

inherit "/std/object.c";

inherit "/lib/pocisk.c";

#include <pl.h>
#include <wa_types.h>

void
create_object()
{
    ustaw_nazwe(({"kulka", "kulki", "kulce", "kulke", "kulk�", "kulce"}),
        ({"kulki", "kulek", "kulkom", "kulki", "kulkami", "kulkach"}),
        PL_ZENSKI);

    dodaj_przym("ma�y", "mali");
    dodaj_przym("o�owiany", "o�owiani");

    set_long("Zwyk�a metalowa kulka do u�ycia w procy.\n");

    ustaw_typ_pocisku(W_L_SLING);
    ustaw_jakosc(10);

    set_dt(W_OBUCHOWE);
}

public void leave_env(object from, object to)
{
    if(!pocisk_leave_env(from, to))
        ::leave_env(from, to);
}