/**
 * \file /doc/examples/bronie/strzeleckie/strzala.c
 *
 * Przyk�adowa strza�a.
 *
 * @author Krun
 * @date Grudzie� 2007
 */

inherit "/std/object.c";

inherit "/lib/pocisk.c";

#include <pl.h>
#include <wa_types.h>

void
create_object()
{
    ustaw_nazwe(({"strza�a", "strza�y", "strzale", "strza�e", "strza��", "strzale"}),
        ({"strza�y", "strza�", "strza�om", "strza�y", "strza�ami", "strza�ach"}),
        PL_ZENSKI);

    dodaj_przym("drewniany", "drewniani");
    dodaj_przym("d�ugi", "d�udzy");

    set_long("Zwyk�a strza�a... ot co.\n");

    ustaw_typ_pocisku(W_L_BOW);
    ustaw_jakosc(10);

    set_dt(W_IMPALE);
}

public void leave_env(object from, object to)
{
    if(!pocisk_leave_env(from, to))
        ::leave_env(from, to);
}