/* 	uliczka2.c - (c) Ilintar 2002
	Inna typowa ciemna uliczka w miescie Athkatla
*/
inherit "/std/room";
#include <macros.h>
#include <stdproperties.h>
#include <ss_types.h>
#include <filter_funs.h>

#define JEST_ZLODZIEJ "_jest_zlodziej_ilintar1"
#define WIDZI_ZLODZIEJA "_jest_widoczny_zlodziej_ilintar1"
#define ZLODZIEJ_AKTYWNY "_jest_zlodziej_aktywny1"

int alarmik;
int czy_widzi_zbira(void);
int czy_widzial_zbira(void);
int pojawia_sie_zlodziej(void);
int sprawdz_czy_walczy(void);
object zlodziej;
object ulica;

int
leave_inv(object ob, object to)
{
    this_player()->remove_prop(WIDZI_ZLODZIEJA);
    ::leave_inv(ob, to);
}

void
reset_room(void)
{
zlodziej->remove_object();
this_object()->remove_prop(ZLODZIEJ_AKTYWNY);
this_object()->add_prop(JEST_ZLODZIEJ, 1);
::reset_room();
}

void 
create_room(void)
{
    ulica = this_object();
    set_short("Podejrzana aleja");
    set_long("Opuszczone kamienice, ktore rozciagaja sie po obu stronach waskiej "+
	"uliczki, bardzo skutecznie odcinaja dostep swiatla, nic wiec nie rozjasnia "+
	"panujacych tutaj ciemnosci. Ulica prowadzi z polnocy na poludnie, przy czym "+
	"polnocne wyjscie zastawione jest dosc pokaznych rozmiarow skrzynia, obok "+
	"ktorej ledwo jest wystarczajaco miejsca na przecisniecie sie.\n");
    add_prop(JEST_ZLODZIEJ, 1);
    add_item("skrzynie", "@@czy_widzi_zbira");
    add_cmd_item("skrzynie", "przesun", "Skrzynia jest zdecydowanie zbyt duza i zbyt "+
	"ciezka, zebys byl w stanie ja przesunac.\n");
    add_cmd_item("zlodzieja", "zabij", "@@czy_widzial_zbira");
    set_event_time(80.0);
    add_event("Za swoimi plecami slyszysz nieznaczny szmer.\n");
    add_event("Z drugiego pietra kamienicy po prawej stronie slyszysz podejrzane " +
	"dzwieki.\n");
    add_event("Nagly podmuch wiatru wzburza pyl zalegly w uliczce.\n");
    add_exit("uliczka3.c", "polnoc", pojawia_sie_zlodziej, 0);
    add_exit("uliczka1.c", "poludnie", 0, 0);
}

int
sprawdz_czy_walczy()
{
    object *obecni;
    if (!present(zlodziej, ulica))
    {
	ulica->remove_prop(ZLODZIEJ_AKTYWNY);
	remove_alarm(alarmik);
	return 0;
    }
    obecni = FILTER_PLAYERS(all_inventory(ulica));
    if (sizeof(obecni) == 0)
    {
	remove_alarm(alarmik);
	this_object()->reset_room();
    }
    return 0;
}	
    
string
czy_widzi_zbira(void)
{
    if ((this_player()->query_skill(SS_AWARENESS) + random(20)) < 40 || 
	(this_object()->query_prop(JEST_ZLODZIEJ) == 0))
    {
	return "Skrzynia wyglada na bardzo ciezka i masywna, ale obok niej jest "+
		"wystarczajaco duzo miejsca, zeby sie przecisnac.\n";
    }
    else
    {
	this_player()->add_prop(WIDZI_ZLODZIEJA, 1); 
	return "Skrzynia wyglada na bardzo ciezka i masywna. Dodatkowo jest "+
	"kryjowka pewnego nieudolnego zlodzieja, ktoremu wydaje sie, ze jest "+
	"doskonale ukryty przed twoimi oczyma.\n";
    }
}

string
czy_widzial_zbira(void)
{
    if (this_player()->query_prop(WIDZI_ZLODZIEJA)) 
    {
	return "Mimo, ze zlodziej jest schowany dosc nieudolnie, to nie jestes w " +
		"stanie dosiegnac go nie przeszedlszy uprzednio kolo skrzyni.\n";
    }
    else return "Nie widzisz zadnej takiej osoby.\n";
}

int
pojawia_sie_zlodziej(void)
{
    if (this_object()->query_prop(ZLODZIEJ_AKTYWNY))
    {
	write("Zlodziej nie pozwala ci przejsc dalej na polnoc.\n");
	return 1;
    }
    if (this_object()->query_prop(JEST_ZLODZIEJ))
    {
	if (!this_player()->query_prop(WIDZI_ZLODZIEJA))
	{
		write("Zza skrzyni nagle wyskakuje zlodziej!\n");
	}
	else
	{
		write("Zlodziej, ktorego dostrzegles wczesniej za skrzynia, "+
			"wyskakuje zzan, gdy probujesz przejsc.\n");
	}
	saybb("Zza skrzyni nagle wyskakuje zlodziej i atakuje "+
		QCIMIE(this_player(),PL_BIE) + "!\n");
	zlodziej = clone_object("/d/Toril/ilintar/athkatla/zlodziej.c");
	zlodziej->move(this_object());
	this_object()->remove_prop(JEST_ZLODZIEJ);
	add_prop(ZLODZIEJ_AKTYWNY, 1);
	alarmik = set_alarm(1.0, 2.0, "sprawdz_czy_walczy");
	return 1;
    }
    else return 0;
}

