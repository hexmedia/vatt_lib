/**
 * \file /doc/examples/zbroje/tarcza.c
 *
 * Przyk�ad tarczy.
 *
 * @author Krun
 * @date 04.09.08
 */

inherit "/std/tarcza.c";

#include <pl.h>
#include <wa_types.h>

void
create_tarcza()
{
    set_long("Liczne �lady i wg��bienia, �wiadczy� mog� o do�� burzliwej "+
        "historii, kt�r� musia� prze�y� ten puklerz. Szeroki na oko�o p� "+
        "�okcia �elazny wypuk�y okr�g, zosta� zwie�czony po �rodku d�ugim "+
        "na palec, gro�nie wygl�daj�cym stalowym szpicem, kt�ry mo�e s�u�y� "+
        "do szybkiego wyprowadzenia ciosu z drugiej r�ki w nieopatrznie "+
        "ods�oni�ty kawa�ek cia�a. Od wewn�trz tarczy, kiedy� zamontowano "+
        "sk�rzane pasy, lecz teraz postrz�pione skrawki zosta�y zast�pione "+
        "konopnym plecionym sznurem.\n");

    dodaj_przym("�elazny");
    dodaj_przym("podniszczony");

    ustaw_typ(SH_PUKLERZ);

    //Nie musimy ustawia� nazwy poniewa� ustawi si� automatycznie ze wzgl�du na typ,
    //chyba, �e chcemy aby nasza tarcza mia�a jak�� szczeg�ln� nazw�, nie tarcza albo
    //puklerz.

    //Je�li kodujemy puklerz, nale�y zauwa�y�, �e warto by mu by�o poustawia� ac.
    set_shield_ac(7, 7, 3); //Warto�ci jak w set_ac, domy�lnie warto�ci ustawione s�
    //  w chwili obecnej na 4.
}
