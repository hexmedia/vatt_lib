inherit "/std/dylizans_in.c";

void create_wnetrze()
{
	set_short("Na wozie");
	set_long("Znajdujesz sie na niezbyt ekskluzywnym, wiejskim wozie."
	+" Na jego przedzie znajduje si� niewielkie siedzonko"
	+" przeznaczone zapewne dla wo�nicy, wi�c chyba niestety musisz"
	+" zadowoli� si� sianem, kt�rego tu pe�no.\n");
	
	add_sit("na sianie", "na sianie", "z siana");
	add_sit("na przedzie", "na przedzie wozu", "z miejsca wo�nicy");
	
	/* Dodajemy komendy dla wysiadania z dyli�ansu
	    "wyskocz" jest zarezerwowane dla wyskakiwania z dyli�ansu 
	    w trakcie jazdy*/
	dodaj_komende("zejd�", "zchodzi", "zej��");
	dodaj_komende("zeskocz", "zeskakuje", "zeskoczy�");
	
	/* Dodajemy eventy jazdy... */
	dodaj_event_jazdy("W�z ko�ysze si� niespokojnie.");
	dodaj_event_jazdy("Konie prychaj� cicho.");
	dodaj_event_jazdy("Wo�nica kr�tkimi cmokni�ciami i uderzeniami bata pop�dza konie.");
	dodaj_event_jazdy("Zaczynasz mie� ju� dosy� tej jazdy...");
	
	/* I czas co jaki si� pojawiaj� */
	set_event_time(15);
	
}
	