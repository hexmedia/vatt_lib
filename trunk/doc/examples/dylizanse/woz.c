inherit "/std/dylizans.c";

#include <pl.h>
#include <stdproperties.h>

/* ACHTUNG: po sklonowaniu dyliansu na lokacje trzeba na nim
 wywoa funkcje start(), zeby zaczal jezdzc */
void create_dylizans()
{
	ustaw_nazwe("woz");
	set_long("Zwyk�y drabiniasty w�z nie wyr�nia si� niczym szczeg�lnym."
	+" Dostrzegasz na nim ca�kiem sporo siana, co powinno"
	+" umili� podr� ewentualnym kilentom. Masz wra�enie, �e zaprz�one do wozu"
	+" dwa gniade konie nie powinny pa�� ze zm�czenia w po�owie drogi.\n");
	
	dodaj_przym("d�ugi", "d�udzy");
	dodaj_przym("drabiniasty", "drabinia�ci");
	
	add_prop(OBJ_I_WEIGHT, 15000);
	add_prop(OBJ_I_VOLUME, 337500);
	
	/* Dodajemy komendy za pomoc� kt�rych mo�na wsi��� do dyli�ansu*/	
	dodaj_komende("wskocz", "wskakuje", "wskoczy�", "na", PL_DOP);
	
	/* Ustawiamy cene w miedziakach */
	ustaw_cene(10);
	
	/* Ustawiamy czas sp�dzany na postoju w sekundach */
	ustaw_czas_postoju(30);
	
	/* Ustawiamy ilosc miejsc w dyli�ansie */
	ustaw_ilosc_miejsc(6);
	
	/* Okre�lamy tras� dyli�ansu
	      ** - post�j */
	ustaw_trase( ({"**", "e", "e", "**", "n", "n", "**", "n", "se", "**"}) );
	
	/* Ustawiamy nazwy kolejnych postoj�w, przypadek gram. dowolny,
	    wa�ne tylko, aby zgadza�o si� z tym co gada wo�nica */
	ustaw_postoje( ({"postoju 1", "postoju 2", "postoju 3", "postoju 4"}) );
	
	/* Okre�lamy szybko�� dyli�ansu - czas jaki sp�dza na lokacji podczas jazdy */
	ustaw_szybkosc(4);
	
	/* Podajemy �cie�ke do wo�nicy... (dyli�ans klonuje npca sam)*/
	ustaw_woznice("/doc/examples/npce/dowodczyni.c");
	
	/* Podajemy �cie�ke do wn�trza dylizansu */
	ustaw_wnetrze("/d/Aretuza/rantaur/woz_in.c");
	
	/* Ustawiamy komunikat na wjechanie dyli�ansu na lokacje... */
	set_enter_msg("Zza horyzontu wy�ania si� d�ugi drabiniasty w�z.");
	
	/* ... i na wyjechanie ( w obu mozna uzywac vbfc)*/
	set_leave_msg("Przy rytmicznym t�t�cie ko�skich kopyt w�z oddala si�@@ret_kierunek_na@@.");
	
	/* Przydatne funkcje: 
	     * ret_postoj() - zwraca nam aktualny postoj
	     * ret_nastepny_postoj() - zwraca nastepny postoj
	     * ret_kierunek_na - zwraca kierunek w ktorym wyjezdza dylizans
	     * ret_kierunek_z - zwraca kierunek z ktorego przyjechal dylizans
	*/
}

/* Wstawiamy dodatkowe warunki, ktore musi spe�ni� gracz, zeby
    wsi��� do dyli�ansu.
      Je�li funkcja zwraca 1, to nie wpuszczapy delikwenta, je�li 0 - wpuszczamy*/
int wsiadz_inne()
{
	if(this_player()->query_gender())
	{
		woznica->command("powiedz do "+this_player()->query_real_name(1)
						+" Uooo ni, moja droga ja jeno ch�op�w bior�!");
		return 1;
	}
}

/* A tu zestawy g�ownie dla wo�nicy... */

/* Akcje wykonywane kiedy wo�nica wyszed� z dyli�ansu i jest juz na zewn�trz*/
void msg_enter_out()
{
	woznica->command("emote krzyczy gromko: Dojechalim do "+ret_postoj()+"! �a za chwilunie" 
					+" w stronem "+ret_nastepny_postoj()+" zmierzamy.");
	woznica->command("pociagnij nosem poteznie");
}

/* Kiedy wchodzi do dyli�ansu i jest w �rodku*/
void msg_enter_in()
{
	woznica->command("powiedz Nu, jadziem! Ino si� nie wychyla�!");
	woznica->command("usiadz na przedzie");
	woznica->command("emote chyta lejce i �ywymi okrzykami pop�dza konie.");
}

/* Kiedy wchodzi do dyli�ansu, ale jest jeszcze na zewn�trz :P */
void msg_leave_out()
{
	woznica->command("krzyknij Oodjazd!");
	woznica->command("emote rozglada si� w nadzieji na znalezienie jeszcze jednego kilenta.");
}

/* No..i kiedy wychodzi z dyli�ansu, ale jest jeszcze w srodku */
void msg_leave_in()
{
	woznica->command("powiedz Dotarlim do "+ret_postoj()+"!");
	woznica->command("wstan");
	woznica->command("podrap sie w zadek");
}

/* Akcje wykonywane kiedy gracza nie sta� na podr� */
void msg_niestac()
{
	woznica->command("powiedz do "+this_player()->query_real_name(1)
					+" Ooo, bratku, na taka podr� to cie chiba nista�.");
}




	
	