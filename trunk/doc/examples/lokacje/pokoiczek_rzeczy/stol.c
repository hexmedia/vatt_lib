/* Ostra przerobka ze stolika na napoje z Dragfor,
     Dziaa ju prawie jak container :)
        Praca zbiorowa :P
        Delvert, Gregh, Lil
	A na koniec Molder wywali�� po�ow� i wklei�� co innego :P */

/* stol jako przyklad dla tworzenia longow */

#pragma strict_types

inherit "/std/container";

#include <pl.h>
#include <macros.h>
#include <filter_funs.h>
#include <stdproperties.h>
#include <composite.h>
#include <cmdparse.h>
#include <object_types.h>

nomask void
create_container()
{
    ustaw_nazwe("st�");

    set_long("Prosty, drewniany st�.@@opis_sublokacji| Na blacie |na|.|||le�y |le�� |le�y @@\n");

    add_item(({ "p�yt�", "blat", "powierzchni�" }), "Drewniana powierzchnia nie jest zbyt r�wna\n");
    add_item(({ "nog�", "nogi" }), "Drewniane nogi s� lekko krzywe, " +
        "a jednak ca�y st� zdaje si� by� stabilny.\n");
    setuid();
    seteuid(getuid());
    add_prop(CONT_I_MAX_VOLUME, 20000);
    add_prop(CONT_I_VOLUME, 16800);

    add_prop(CONT_I_CANT_WLOZ_DO, 1); /* By nie mo�na by�o nic do niego w�o�y� komend� 'wloz do' */

    add_prop(CONT_I_WEIGHT, 10000);
    add_prop(CONT_I_MAX_WEIGHT, 20000);

    add_prop(CONT_I_DONT_SHOW_CONTENTS, 1);

    /* dodajemy sublokacj� 'na', by mo�na by�o co� na st� k�a�� */
    add_subloc("na");
}

public int
query_type()
{
    return O_MEBLE;
}

