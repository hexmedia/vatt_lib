/*
 * Przyklad napisany przez Zbojcerza Elvarona z Krainy Zgromadzenia.
 */

inherit "/std/creature";
inherit "/std/combat/unarmed";

inherit "/std/act/action";

#include <pl.h>
#include <macros.h>
#include <wa_types.h>
#include <ss_types.h>
#include <filter_funs.h>
#include <stdproperties.h>

/*
 * Definiujemy to dla wlasnej wygody
 */
#define A_ROGI      0
#define A_LKOPYT    1
#define A_PKOPYT    2

#define H_GLOWA     0
#define H_TULOW     1

void
create_creature()
{
    string *przm = losowy_przym();

    ustaw_odmiane_rasy( ({"krowa","krowy","krowie","krowe","krowa","krowie"}),
       ({"krowy","krow","krowom","krowy","krowami","krowach"}), PL_ZENSKI);

    add_prop(LIVE_I_NEVERKNOWN, 1);

    set_gender(G_FEMALE);

    random_przym(({"�aciaty", "�aciaci", "czarny", "czarni"}), ({"chudy", "chudzi", "t�usty", "t�u�ci", "cielny", "cielni",
        "ko�cisty", "ko�ci�ci"}), ({"kulej�cy", "kulej�cy"}));

    set_long("Zwykla krowa mleczna.\n");

    set_stats(({ 30, 5, 30, 5, 5, 20}));

    set_act_time(60);
    add_act("emote zuje.");
    add_act("emote ryczy muuuuu....");

    set_skill(SS_DEFENCE, 5);

    set_attack_unarmed(0, 5, 5, W_IMPALE,   50, "rogi");
    set_attack_unarmed(1, 5, 5, W_BLUDGEON, 20, "kopyto", ({"lewy",  "przedni"}));
    set_attack_unarmed(2, 5, 5, W_BLUDGEON, 20, "kopyto", ({"prawy", "przedni"}));
    set_attack_unarmed(3, 5, 5, W_BLUDGEON,  5, "kopyto", ({"lewy",  "tylny"}));
    set_attack_unarmed(4, 5, 5, W_BLUDGEON,  5, "kopyto", ({"prawy", "tylny"}));

    set_hitloc_unarmed(0, ({ 1, 1, 1 }), 10, "g�owa");
    set_hitloc_unarmed(1, ({ 1, 1, 1 }), 30, "tu��w");
    set_hitloc_unarmed(2, ({ 1, 1, 1 }), 20, "noga", ({"lewy", "przedni"}));
    set_hitloc_unarmed(3, ({ 1, 1, 1 }), 20, "noga", ({"prawy", "przedni"}));
    set_hitloc_unarmed(4, ({ 1, 1, 1 }), 10, "noga", ({"tylny", "lewy"}));
    set_hitloc_unarmed(5, ({ 1, 1, 1 }), 10, "noga", ({"tylny", "przedni"}));
}

void
enter_inv(object ob, object from)
{
    if(member_array("zio�o", ob->parse_command_id_list()) >= 0)
        command("przytul sie do " + OB_NAME(from));

    ::enter_inv(ob, from);
}
