
  Nr. Skilla      Skr�t                  Opis
  --------------------------------------------------------------------
  10           SS_WEP_SWORD           Umiej�tno�� walki mieczami
  11           SS_WEP_POLEARM         Umiej�tno�� walki b. drzewcowymi
  12           SS_WEP_AXE             Umiej�tno�� walki toporami
  13           SS_WEP_KNIFE           Umiej�tno�� walki sztyletami
  14           SS_WEP_CLUB            Umiej�tno�� walki maczugami
  15           SS_WEP_WARHAMMER       Umiej�tno�� walki m�otami
  16           SS_WEP_MISSILE         Bronie strzeleckie
  17           SS_WEP_JAVELIN         Bronie rzucane
  20           SS_2H_COMBAT           Walka dwiema bro�mi
  21           SS_UNARM_COMBAT        Walka bez broni
  22           SS_BLIND_COMBAT        Walka w ciemno�ci
  23           SS_PARRY               Parowanie
  24           SS_DEFENCE             Uniki
  25           SS_MOUNTED_COMBAT      Walka konna
  26           SS_SHIELD_PARRY        Tarczownictwo
  30           SS_SPELLCRAFT          Rzucanie czar�w
  34           SS_FORM_ILLUSION       Iluzje
  36           SS_HERBALISM           Zielarstwo
  37           SS_ALCHEMY             Alchemia
  38           SS_FORM_TRANSMUTATION  Transmutacja
  50           SS_OPEN_LOCK           Otwieranie zamk�w
  51           SS_PICK_POCKET         Kieszonkostwo
  52           SS_ACROBAT             Akrobatyka
  53           SS_FR_TRAP             Wykrywanie pu�apek
  54           SS_SNEAK               Skradanie si�
  55           SS_HIDE                Ukrywanie si�
  100          SS_APPR_MON            Ocena przeciwnika
  101          SS_APPR_OBJ            Ocena obiektu
  102          SS_APPR_VAL            Szacowanie
  103          SS_SWIM                P�ywanie
  104          SS_CLIMB               Wspinaczka
  105          SS_ANI_HANDL           Opieka nad zwierz�tami
  106          SS_LOC_SENSE           Wyczucie kierunku
  107          SS_TRACKING            Tropienie
  108          SS_HUNTING             �owiectwo
  109          SS_LANGUAGE            Znajomo�� j�zyk�w
  110          SS_AWARENESS           Spostrzegawczo��
  111          SS_TRADING             Targowanie si�
  112          SS_RIDING              Jazda konna
  113          SS_MUSIC               Muzykalno��
  114          SS_WOODCUTTING         Drwalstwo 
