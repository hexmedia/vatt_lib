*******************************************************************************
    W pliku tym zawarte s� WYMAGANIA, kt�re stawiane s� przed wizami koduj�cymi
w domenach, jak r�wnie� wytyczne i propozycje dotycz�ce kodu obiekt�w. Uprasza 
si� wiz�w o przyswojenie sobie tych przepis�w i aktywne ich stosowanie oraz
w miar� czasu i mo�liwo�ci dostosowanie starego kodu. W razie jakichkolwiek
w�tpliwo�ci prosz� si� kontaktowa� z Lordem.
    Plik ten zosta� napisany przez Galena i zmieniony dla potrzeb Imperium
przez Silvy'ego, nast�pnie przekszta�cony LEGALNIE dla potrzeb Avalonu, i
zmodyfikowany zgodnie z pomys�ami tw�rc�w Vatt'gherna.
*******************************************************************************

1. KODOWANIE LOKACJI
   1.1. D�ugi opis lokacji _musi_ zawiera� przynajmniej cztery linie tekstu.

   1.2. Opis nie powinien zawiera� powt�rze� (chyba, �e jako �rodek
       stylistyczny) oraz niezr�czno�ci, a zdania powinny by� zrozumia�e,
       ale te� w miar� mo�liwo�ci z�o�one (nie jest wskazane prymityzowanie
       opisu poprzez sztuczne skracanie zda�).

   1.3. Opis podlega ocenie Lorda, wyznaczonych przez niego wiz�w oraz AoB
       i moze zosta� niedopuszczony.

   1.4. Kr�tki opis powinien jednoznacznie sugerowa�, w jakiej krainie znajduje
       si� gracz, chyba, �e z innych wzgl�d�w jest to niewskazane
       (po kr�tkim opisie _nie_ma_kropki_).

   1.5. _NIE WOLNO_ tworzy� _JEDNAKOWYCH_ lokacji poprzez kopiowanie plik�w
       i zmian� tylko paru szczeg��w. Nawet gdy jest do opisania 50 lokacji
       lasu, nale�y sprawi�, aby opisy r�ni�y si�, gdy� w rzeczywisto�ci
       tylko og�lnie las jest wsz�dzie taki sam. Gdy si� lepiej przyjrze�,
       ka�de miejsce posiada jakie� indywidualne cechy. Aby kraina by�a
       ciekawa nale�y wsz�dzie wprowadza� jakie� dodatkowe elementy, kt�re
       b�d� si� rzuca�y w oko. W przypadku ch�ci utworzenia krainy z losowymi
       wyj�ciami, gdzie cz�sto uzasadnione jest przedstawianie jednakowych
       lokacji, aby wywo�a� wra�enie zagubienia - nale�y si� skonsultowa� z
       Lordem lub wyznaczonymi przez niego lud�mi.
       Du�a cz�� opisu tworzy opis pory roku i dnia, dlatego wa�ne jest, aby
       plik standardowy z tymi opisami by� dobrze napisany.

   1.6. NIE NALE�Y stosowa� arkadiowego rozr�nienia mi�dzy 'obejrzyj'
	a 'przeczytaj'. Jest jasne, �e gracz wpisuj�c 'obejrzyj napis'
	nie zamierza otrzyma� opisu "istotnie, jest tu napis", a po prostu
	to co tam jest napisane. 'przeczytaj' mo�e by� umieszczone
	tylko w BARDZO szczeg�lnych przypadkach, kt�rych generalnie na
	razie sobie nie wyobra�am. Tak wi�c 'przeczytaj' rezerwujemy
	dla biblioteki, poczty i tablic og�oszeniowych.
	
   1.7. W wywo�aniach obejrzenia item�w stosujemy hierarchi� w
	zale�no�ci od poziomu, na kt�rym opisana jest dana rzecz,
        czyli przyk�adowo - je�li w pokoju mo�na ob 'obraz', to:
	ob obraz - "Jest na nim jaka� posta�"
	ob posta� - "S�ucham?" 
	ob posta� na obrazie - "Jest to kr�l Niedamir, trzyma w r�ku miecz"
	ob miecz - "S�ucham?"
	ob miecz postaci na obrazie - "Tak, to bez w�tpienia Excalibur, jest
					na nim jaki� napis"
	ob napis na mieczu postaci na obrazie - my�l�, �e ju� wiadomo o
					co chodzi :)	
	
    
   1.8. �adna lokacja i kraina nie mo�e by� bezsensown� pu�apk� na gracza,
       kt�ra uniemo�liwia�aby mu wydostanie si� stamt�d. Zawsze powinny
       istnie� mo�liwo�ci opuszczenia lokacji,do kt�rej si� wesz�o, cho�
       mog� by� ukryte lub obwarowane pewnymi warunkami. W przypadku,
       gdyby kto� poczu� nieprzepart� ochot� stworzenia takiego miejsca,
       _musi_ skontaktowa� si� z lordem i uzyska� jego pozwolenie.

   1.9. Komendy (definiowane w add_exit) kt�re gracz ma wyda�
	aby przej�� na s�siedni� lokacj� powinny by�:
	- kierunkiem �wiata - w momencie gdy lokacja jest traktem,
	polem, lasem, jaskini�, b�d� wn�trzem budynku
        (za wyj�tkiem wyj�cia z budynku),
	- nazwa (a nie kierunek!) miejsc do kt�rych wchodzimy -
	przy wszelkich sklepach, domach, ku�niach, fryzjerach etc.
	- 'wyj�cie' (a nie kierunek!) ten kierunek pozwala nam
	wyj�� na zewn�trz sklepu, domu, ku�ni, fryzjera etc.

   1.10. KA�DA lokacja musi includowa� plik, gdzie zdefiniowane s� sta�e
        ze �cie�kami dost�pu, oraz u�ywa� tych sta�ych wsz�dzie gdzie
        zachodzi potrzeba przedstawienia �cie�ki do jakiego� pliku. U�atwi
        to ewentualne zmiany katalog�w.

   1.11. Pliki lokacji danego projektu powinny by� w osobnym katalogu, a nie
        wymieszane ze wszystkimi obiektami.

   1.12. Kod lokacji powinien by� czytelny i napisany zgodnie z zasadami
        estetyki i poprawno�ci zawartymi w lpc.txt.

   1.13. Wszystkie przedmioty i obiekty znajduj�ce si� w opisie lokacji
       musz� mie� sw�j opis w postaci add_item, uwzgl�dniaj�cy wszystkie
       sensowne nazwy danego przedmiotu umieszczone w bierniku.

   1.14. Bez konsultacji z w�adzami domeny i AoB nie wolno umieszcza� w
        lokacji prop�w specjalnych, zmieniaj�cych w zasadniczy spos�b
        mo�liwo�ci gracza w danym miejscu. Chodzi tu g��wnie o nadawanie
        flag no_attack, no_magic i temu podobnych, czy to poprzez propy
        zawarte w systemie czy te� poprzez napisane przez siebie funkcje.

   1.15. Nie wolno w zasadzie tworzy� teren�w o dost�pie tylko dla dobrych
        lub tylko dla z�ych. Je�li nawet np. �li nie powinni wchodzi� gdzie�
        tzw g��wnym wej�ciem, to powinni mie� mo�liwo�� innego dostania si�
        w to miejsce nawet je�li by�aby to droga trudna i niebezpieczna.
        Ca�kowite zamykanie jakiego� terenu dla grupy graczy jest w przypadku
        wi�kszych obszar�w zawieraj�cych og�lnodost�pne teoretycznie "dobra"
        ca�kowicie niedopuszczalne. Jakkolwiek by�bym sk�onny dopu�ci� np
        �wi�tyni� w kt�rej tylko dobry m�g�by si� pomodli�. Wszystko to
        w gruncie rzeczy zale�y od r�wnowagi sygnowanej przez AoB i danego
        Lorda, tak wi�c w razie w�tpliwo�ci prosz� o kontakt z powy�szymi.

   1.16. Wiz kieruj�cy w�asnym projektem otrzymuje pewn� autonomi� w kierowaniu
        pracami, jakkolwiek nie zwalnia go to ze znajomo�ci i przestrzegania
        powy�szych przepis�w. W razie jakich� w�tpliwo�ci powinien si� zg�osi�
        do Lorda, gdy� jego kod mo�e zosta� niedopuszczony przy sprawdzaniu.

2. KODOWANIE NPC

   2.1 Ustalanie stat�w npc musi by� zgodne z plikiem wielkosci.txt

   2.2 D�ugo�� i rodzaj opisu, skille, chat'y itp podlegaj� indywidualnej
       ocenie wiza koduj�cego, jakkolwiek mog� zosta� przedstawione do
       korekty w momencie dopuszczania.
   
   2.3 Chcia�bym tu wyr�ni� dwa rodzaje npc: tzw og�lne i szczeg�lne.
       Te pierwsze to np monstra z ustawionym shortem gwardzista czy te�
       mieszkaniec, czyli npc niekonkretne i wystepuj�ce w liczbie wi�kszej
       ni� jeden. Te drugie to charakterystyczne npc wystepuj�ce pojedynczo.
       Nale�y tu rozr�ni� zasady ich pisania:
       NPC szczeg�lne - powinny si� przedstawia�, powinny mie� niepowtarzaln�
                        bro� i str�j, powinny mie� ograniczon� liczb�
                        element�w losowych i nawet staty powinny by� sta�e.
                        Powinny mie� standardow� odpowied�, reagowa� na r�ne
                        zapytania i emoty oraz mie� jakie� charakterystyczne
                        chaty. Jednym s�owem powinny by� pisane ca�kowicie
                        indywidualnie.
       NPC og�lne - powinny dziedziczyc wspolny plik w ktorym znajdowalyby
                   sie standardowe chaty odpowiedzi itp. Powinny miec jak
                   najwiecej elementow losowych: przymiotniki, statsy, skile
                   bron, zbroje, a czasem nawet rase i inne. Powinny tez
                   uzywac jakichs przedmiotow charakterystycznych dla krainy.
                   Czyli nie nalezy dla kazdego npc w krainie tworzyc innego
                   ekwipunku, choc nalezy tez wziasc pod uwage jego statsy i
                   i skile,  a wiec jak latwo go zabic.
   
   2.4 Nie wolno bez ustalen z lordem nadawac npcom flagi uniemozliwiajacej
       jego zaatakowanie,  jak rowniez przy tworzeniu specjali i innych
       usprawnien typu czary i poisony, nalezy uzyskac zgode lorda lub
       wyznaczonych osob.
3. KODOWANIE EKWIPUNKU I OBIEKTOW NIESTANDARDOWYCH
   3.1 Wartosci hit, pen, ac, cena zbroi i broni podlegaja pod przepisy
       zawarte w manualach domenowych ( bronie.txt, zbroje.txt ).
       Jakiekolwiek przekroczenie w gore wymienionych
       tam granic, oraz dodanie przedmiotowi specjalnych wlasciwosci
       (pomagajacych graczowi) wymaga zgody najpierw lorda a pozniej AoB.
       
   3.2 Nie nalezy tworzyc osobnego ekwipunku dla kazdego npc. Najlepiej
       jest korzystac z kilku roznej jakosci broni i zbroi w danej krainie,
       choc dla npc szczegolnych  zwykle nalezy napisac osobne przedmioty.

4. KODOWANIE QUESTOW 
   
   4.1 Wszystkie questy musza byc konsultowane i akceptowane przez Lorda
       domeny.
   
   4.2 Korzysci dla gracza w postaci expow wyznacza Lord w porozumieniu
       z AoB; inne korzysci musza byc poddane akceptacji Lorda i AoB
   
   4.3 Questy nalezy kodowac w zgodzie z odpowiednimi manualami domenowymi
       i ogolnodomenowymi traktujacymi o tym.
   
   4.4 Nalezy zachowac odnosnie questow scisla tajemnice nie ujawniajac
       ich nikomu bez zgody lorda lub archow.
   
   4.5 Kod questow w znacznej czesci powinien byc ukryty w katalogach
       private wizow, np w postaci pojedynczego pliku includowanego lub
       inheritowanego  w odpowiednich obiektach, gdzie beda tylko 
       wystepowac nieczytelne dla osob postronnych odwolania czy inicjacje
       odpowiednich funkcji. Za zgoda lorda mozna tez ukryc cale obiekty
       questowe.
   
   4.6 Questy powinny byc logowane (kto i kiedy zrobil, ewentualnie
       jak to zrobil) o co musi zadbac wiz tworzacy quest. Przy czym logi te
       musza byc tajne.
   
   4.7 Warunki jakie powinien spelniac quest:
       - podstawowym warunkiem przy akceptacji jest jego sensownosc, czyli
         zgodnosc z zasadami rollplayu i stworzenie calej otoczki questa.
         Po prostu koniecznosc wykonania jakiegos zadania musi miec jakies
         racjonalne wyjasnienie. Nie jestem w tym miejscu zwolennikiem 
         questow w ktorych trzeba np przyniesc duchowi sandal, za co dostaje
         sie expy, a juz dlaczego jakies skeletony jeszcze tego sandala
         mialyby bronic nie rozumiem zupelnie. Mysle ze nie nalezy do tego
         stopnia laczyc rzeczy doczesnych ze swiatem  magii i duchow, 
         jakkolwiek nie mam nic przeciwko questom o magii i duchach, ale za to
         zrobionym sensownie.
      -  drugim warunkiem akceptacji jest rozwiazywalnosc,  czyli mozliwosc
         zrobienia danego questa przez gracza bez uzycia skomplikowanego
         opisu. Nalezy pamietac, ze najmniej jest opisow questow, ktore
         da sie rozwiazac. Nie nalezy wiec pozostawiac wszystkie graczowi
         w domysle i umieszczac rozne rollplayowe hinty, jakkolwiek nie
         przesadzone, ktore dociekliwemu graczowi umozliwia rozwiazanie.
         Quest powinien tez rozgrywac sie na pewnym ograniczonym terenie
         lub w przypadku wiekszych zadan, posiadac np w bibliotece jakis
         ogolny hint, rozwijany w poszczegolnych miejscach domeny. Nie 
         powinno byc sytuacji, ze jakis klucz znaleziony gdzies w najglebszych
         tunelach otwiera drzwi gdzies w Nuln, jesli nie jest gdzies
         w hincie zaznaczona taka mozliwosc. Mowiac ogolnie zawsze powinien
         byc dosc scisle wyznaczony obszar poszukiwan danej rzeczy do questa,
         wzglednie, gdynie jest on wyznaczony, to domyslny jest najblizszy
         teren.
      -  chetnie widziane beda przezemnie questy zawierajace duza liczbe
         elementow losowych. Takie questy beda wyzej oceniane i punktowane
         ze wzgledu na trudnosci ze zrobieniem do nich opisu przez graczy.
         Quest powinien byc tak skonstruowany, ze zawiera pewne latwo
         zmienialne elementy (oprocz losowych), tak, zeby po jakims czasie
         jego funkcjonowania mozna bylo cos przeksztalcic. Chetnie widze
         w questach rowniez roznego rodzaju zagadki slowne i logiczne lub
         wrecz cale zadania logiczne, gdyz rowniez zwiekszaja one 
         bezpieczenstwo questa. Lepsze sa tez questy bardziej skomplikowane
         z duza iloscia elementow i czynnikow bo wymagaja nawet z opisem
         sporego zaangazowania gracza i zmarnowania czasu. Nie znaczy to
         ze nie nalezy robic questow prostych, gdyz jak wykazuje praktyka
         takie polegajace na tym aby gdzies pojsc i cos  przyniesc sa 
         najbezpieczniejsze, bo niewiele daja, a kazdy moze je wykonac bez
         problemu. Pozatym zwiekszaja one wydatnie znajomosc swiata.
      -  niedopuszczalne sa questy nastawione tylko na przedmioty specjalnie
         do tego questa napisane w sytuacji, gdy przedmioty danego rodzaju
         sa bardziej powszechnie dostepne w grze. Wezmy np takie wiaderko.
         Nie moze byc tak, ze mozemy przyniesc wode do danego questa tylko
         w jednym jedynym wiaderku gleboko schowanym np pod kopcem siana.
         Taki quest bylby zrodlem frustracji dla gracza, ktory zapewne 
         znalazlby wczesniej 10 innych wiaderek tylko nie mogl ich wykorzystac.
         Takie przedmioty powinny byc nieco bardziej ogolne (mozna np 
         wykorzystac fakt ze cos jest pojemnikiem), w ostatecznosci mozna
         nadawac przedmiotom propy, ale trzeba sie liczyc z tym, ze inni 
         wizowie nie beda chcieli zmieniac napisanych przez siebie obiektow,
         a wtedy za pozwoleniem lorda bedzie to trzeba zrobic samemu. Jednak
         mozliwosc komplikowania questow nie ogranicza sie do wykorzystywania
         tego typu przedmiotow. Powiedzialbym nawet, ze nalezy unikac tego
         typu problemow tak konstruujac fabule questu, aby osiagnac pozadany
         przez siebie efekt szukania w inny rollplayowy sposob, a wierzcie mi
         ze jest ich nieskonczona liczba.
      -  Niechetnie bede tez akceptowal questy tylko dla zlych, lub tylko dla
         dobrych. Jedyna dopuszczalna dla mnie opcja jest quest w dwoch wersjach
         dla zlych i dla dobrych. 
      -  Ewentualni autorzy zgadywanek slownych w Ishtar, gdziekolwiek bylyby
         one umieszczone, beda obowiazkowo musieli zapewnic losowosc zagadki,
         jak rowniez zmiane puli co jakis czas.
      -  Milo widziane beda tez ciagi questow, a wiec jakies jedno duze zadanie
         podzielone na mniejsze fragmenty, za ktore bylyby expy. Oczywiscie
         takie zadanie musi spelniec wszystkie warunki odnosnie questow,
         ktore wymienilem wczesniej.
      -  Nie nalezy tworzyc questow prostych ale powernych, tzn polegajacych
         mniej wiecej na tym ze sie bierze questa idzie zabija goscia i wraca 
         po expy. Questy maja byc czyms co promuje myslenie i znajomosc swiata
         na Vatt'ghernie, a nie wielkosc gracza. Choc nie mam nic przeciwko, by 
         quest posiadal zarowno rozwiazanie silowe jak i logiczne, ale to
         wszystko musi miec sens. W pewnych wypadkach koniecznosc zabicia 
         jakichs npc pomoze niedopuscic zbyt malych graczy do questa, lub
         bedzie promowala wspolprace graczy, ale zabicie duzych npc nie powinno
         stanowic glownego przedmiotu questu, lub tez nie powinno sie dac
         wykonac zbyt prosto(np tylko przy uzyciu specjalnego miecza itp).
         W przypadku mniejszych questow lub tez questow klimatotworczych
         zabicie niewielkich npc moze byc jednym z przedmiotow questa, ale 
         musi posiadac swoje logiczne uzasadnienie oraz dodatkowe elementy
         utrudniajace. Wszystko do uzgodnienia z lordem.
      -  Bardzo ciekawym rodzajem questow, sa questy wielodostepne, czyli
         opracowane tak, by conajmniej dwoch graczy moglo w nich brac
         udzial, ale w roznych rolach. Oczywiscie musza one takze spelniac
         powyzsze warunki, ale same w sobie juz utrudniaja stworzenie opisu,
         promuja natomiast wspolprace graczy, ktorzy mieliby szanse dostac
         za quest jednakowe wynagrodzenie, w wypadku gdyby rownie dobrze
         wykonali stojace przed nimi zadania. Tego typu questy beda sie
         cieszyly moim nieklamanym entuzjazmem i bede dazyl do ich 
         rozpowszechniania. 

5. KODOWANIE GILDII
   5.1 Kodowanie gildii jest zadaniem trudnym i nie bedzie dostepne dla 
       nowych wizow. Niemniej aby zaczac kodowac gildie trzeba najpierw 
       przedstawic do akceptacji jej pomysl w postaci ustnej lub pisemnej. 
       
  5.3  Aby pomysl i opis gildii byly akceptowalne przez Lorda i AoB musza
       spelnic szereg warunkow:
       - musza byc w zgodzie z manualami ogolnymi o gildiach
       - gildia musi miec swoj typ
       - opis powinien zawierac propozycje korzysci (skile i inne) i
         zaplaty za to(tax i inne) dyskutowalne z AoB.
       - opis powinien zaiwrac etos gildii i zasady funkcjonowania oraz
         dokladny spis elementow rollplayowych (emoty oraz bardziej wymyslne)
       - pomysl akceptuje w zasadzie tylko lord a AoB oglada juz opis zawarty
         w pliku i daje zgode albo tez nie (ewentualnie proponuje kierunek 
         zmian)
       - gildia musi miec sensowne uzasadnienie swego istnienia wraz z 
         zadaniami jakie jej przeznaczono oraz powinna byc oparta na 
         literaturze domenowej, a wlasciwie nie moze popadac w konflikt
         z charakterem domeny(ostateczniejej zgodnosc i tak ocenia lord)
       - kazda gildia musi miec swoj niepowtarzalny charakter
  
  5.4 Az do odwolania nie beda dopuszczane dla graczy gildie typowo magiczne,
      co podyktowane jest koniecznoscia stworzenia najpierw swiata, a pozniej
      systemu magii, ktore bylyby w odpowiedniej rownowadze. Przy czym sama
      obecnosc magii w swiecie nie jest jeszcze przesadzona.
      
  5.5 Gildie OCC powinny byc raczej otwarte dla wszystkich tudziez wiekszosci
      ras. Tylko w razie jawnego konfliktu z zalozeniami gildii, jesli 
      zalozen nie da sie zmienic bo zmieniloby to calkowicie charakter gildii,
      dopuszczalne jest wykluczenie jednej lub wiecej ras z mozliwosci
      czlonkostwa.
      
  5.7 Gildia w swym statucie nie powinna nakazywac wojny z innym zrzeszeniem
      graczy, pozostawiajac tego typu sprawy w gestii wladz gildii, tudziez
      jej ogolu.
      
  5.8 Wizowie opiekujacy sie gildia _nie moga_ wtracac sie w sprawy gildii,
      wywierac jakiegokolwiek nacisku na czlonkow czy wladze, sugerowac
      czy podpowiadac wybory dokonywane przez czlonkow oraz zmieniac
      rollplayu gildii bez konsultacji i na prosbe czlonkow, chyba, ze
      ktoras z wymienionych rzeczy stanowi obowiazek danego wiza.
      Wiz jakkolwiek moze wyrazac swoje prywatne opinie, to jednak nie
      powinny  miec one wplywu na wykonywane przez niego obowiazki i nie
      wolno wyrazac ichw stosunkudo gracza w sposob nakazujacy, grozacy
      czy jakikolwiek, inny ktory moglby byc odczytany jako nacisk.
      Zaleca sie tez ograniczenie kontaktow z czlonkami gildii do consulatu,
      tudziez odpowiednich wladz.
