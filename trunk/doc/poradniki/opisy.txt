
 1. Wprowadzenie
 2. Pokazywanie vs. M�wienie
 3. Og�lne wskaz�wki
 4. Opisy lokacji
 5. NPCe
 6. Wnioski ko�cowe


  1. Wprowadzenie
  ---------------

  Styl - "widok i odczucia" danego miejsca - jest niezwykle wa�ny dla
zbudowania wirtualnego �wiata. Opisy tekstowe s� wszystkim, czym �miertelnicy
nape�niaj� sw� wyobra�ni�, aby stworzy� �wiat w swych umys�ach. Dlatego te�,
wa�ne jest, aby u�ywa� s��w w jak najbardziej tw�rczy spos�b.
Celem jest wykreowanie bogatego w szczeg�y, przekonuj�cego i sp�jnego �wiata.
�miertelnicy powinni zapomnie�, �e siedz� przed monitorem, jedynie wklepuj�c
komendy na swych klawiaturach. Zamiast tego, powinni uwierzy�, �e tak naprawd�
odkrywaj� nieznan� i przera�aj�c� kniej�, albo odpieraj� nag�� napa��
uzbrojonych zb�jnik�w, b�d� niszcz� �wiet� Czar� Feyda Straszliwego (przyp.
red. - licentia poetica).
Gdziekolwiek pope�niamy b��d, kt�ry zachwiewa uwag� �miertelnika, ten
wyobra�ony �wiat w jego umy�le m�tnieje. Ci�g�e slogany, anachronizmy, z�e
opisy, skr�ty, zniewagi ("Przecie� to kamie�, g�upku!") -- wszystkie one s�
negatywnymi do�wiadczeniami, kt�re przykuwaj� uwag� - zamiast do odkrywania
nieznanego �wiata - do mechaniki jego budowy.

  2. Pokazywanie vs. M�wienie
  ---------------------------

  W literaturze autorzy musz� u�y� r�norakich �rodk�w, aby wywo�a� w czytaj�cym
prawdziwe odczucie, jak dane miejsce (lokacja) wygl�da, jakie naprawd� jest,
oraz jak by to by�o znale�� si� w takim miejscu. Najbardziej sztampowi autorzy
kradn� czytaj�cym mo�liwo�� wysnuwania w�asnych wniosk�w na podstawie
obserwacji. D��� do najprostszych i najmniej tw�rczych sposob�w opisania
miejsca.
 Po prostu M�WI� na co patrzysz:

"To jest biblioteka. Na tych p�kach jest z setka ksi��ek. Bibliotekarz siedzi
tu dzie� i noc, ka��c im m�wi� cicho, je�li ju� musz� m�wi� w og�le."

 No c�... dzia�a, ale jest to nieco obra�liwe dla czytelnika, kt�ry wola�by
my�le� sam za siebie. Znacznie bardziej interesuj�c� dla tego� czytelnika,
oraz prawdopodobnie bardziej wymagaj�c� dla autora, jest metoda, w kt�rej dana
lokacja by nie mowi�a, lecz UKAZYWA�A co si� dzieje:

"Rz�d za rz�dem p�ek rozci�ga si� od samego wej�cia do tej jak�e wielkiej i
cichej komnaty, gdzie przy�mione blaski �wiec padaj� na grzbiety ksi�g
wszelkich kszta�t�w i rozmiar�w. Pobliskie biurko zajmowane jest przez
pomarszczonego, acz wygl�daj�cego na nadal pe�nego wigoru, m�czyzn�, kt�ry
uwa�nie rozgl�da si� dooko�a."

 Tutaj, ten sam pok�j jest opisany, lecz �miertelnik sam podejmuje decyzje na
temat tego co widzi, miast �eby kto� wprost powiedzia� mu, ze to jest
biblioteka, za� taki a taki bibliotekarz lubi jak tu jest cicho. Kropka.

  3. Og�lne wskaz�wki
  -------------------

- U�ywaj strony czynnej (j�zyk aktywny) zamiast biernej (j�zyk pasywny). J�zyk
czynny sprawia, �e �wiat �yje. Wykracza poza zadeklarowanie stanu obiektu:
   ("Jest tu �aba na li�ciu" - idiotyczne, prawda?) i pozwala dope�ni� i
uszczeg�owi� impresj� czytelnika. Np.
    "Li�� lilii go�ci zielon� �abk�"
albo:
    "Spoczywaj�c na li�ciu lilii, zielone �absko mruga powoli."
J�zyk pasywny cz�sto u�ywa odwo�an do "Ty", takich jak "Widzisz...",
"Zauwa�asz...", "S�yszysz...". Jest on tak�e wywo�ywany, gdy mamy do czynienia
ze stwierdzeniami typu "Znajduj� si� tu...", "Jest to...". Staraj si� unika�
wymienionych technik, a j�zyk sam z siebie nabierze �ycia.
Opisy typu:
   "Rz�d za rz�dem p�ek rozci�ga si� od samego wej�cia do tej jak�e wielkiej i
     cichej komnaty, gdzie przy�mione blaski �wiec..."
  s� znacznie bardziej efektowne i efektywne ni�:
   "Jest to du�a i cicha komnata. Rz�d za rz�dem p�ek..."
lub te�:
   "Jeste� w du�ej i cichej komnacie. Rz�d za rz�dem..."

Inny przyk�ad: generalnie lepiej jest stworzy� opis ze zdaniem "Ga��� wystaje
z pnia drzewa" zamiast zdania "Widzisz ga��� wystaj�c� z pnia drzewa". R�nica
jest do�� subtelna, lecz warunkuje pomi�dzy mo�liwo�ci� samodzielnego
dociekania i wyboru na co si� patrzy, a zmuszaniem do spojrzenia na pewne
rzeczy.

 - Unikaj t�umaczenia �miertelnikom co czuj� i co my�l�, jakie s� granice ich
percepcji i wiedzy, co powinni a czego nie powinni zrobi�. Dla przyk�adu, nie
u�ywajcie zwrot�w typu "Widok rozk�adaj�cych si� cia� przyprawia ci� o
md�o�ci". A mo�e z�a posta� tak naprawd� LUBI takie widoki!
  Opis powinien by� przejrzysty i barwny na tyle, aby �miertelnik m�g�
wykszta�ci� swe w�asne emocje w stosunku do danego przedmiotu, miejsca,
osoby. Je�li mowa tu o ciemnym i zat�ch�ym lochu, powinien on by� na tyle
dobrze opisany, aby �miertelnik poczu� si� nieco przestraszony (albo jak u
siebie w domu - zale�y od postaci).
 - Unikaj stwierdze� "Ten klucz ma dziwny kszta�t. Ciekawe do czego mo�e
pasowa�?". Mo�e �miertelnika wcale to nie ciekawi. Mo�e uzywa� go ju� milion
razy i doskonale wie do czego pasuje.

 - W podobny spos�b, nigdy nie pisz opis�w, jak np. "Jest to najwi�ksze drzewo
jakie kiedykolwiek widzia�e�." -- a sk�d mo�emy wiedzie� co dany �miertelnik
widzia� w swoim �yciu, hm?!

 - Opisy nie powinny zawiera� wskaz�wek "Mo�e powiniene� si� dok�adniej
przyjrze�.", "Mo�e powiniene� tam wej��.", itp. �miertelnicy powinni by� na
tyle ciekawi i sprytni, aby samemu z siebie to sprawdzi�.

  4. Opisy lokacji
  ----------------

 Opis miejsca, pokoju nie powinien by� okropnie d�ugi; po prostu powinien
pozwoli� da� �miertelnikowi odczu� co takiego widzi dooko�a siebie. Oczywi�cie
wypada napisa� mu wi�cej ni� "Jest to droga, co si� ci�gnie z p�nocy na
po�udnie", ale opis nie powinien tak�e si� ci�gn�� na akapity.

 - Unikaj wciskania wszystkich szczeg��w w og�lny opis miejsca. Daj
�miertelnikowi tyle, ile jest mu potrzebne aby zechcia� spogl�da� na
poszczeg�lne elementy tego co widzi, a tak�e wynagr�d� go za ten trud.

 - Staraj si� dostarczy� opisu (add_item) dla wszystkich rzeczownik�w z
d�ugiego opisu lokacji, a tak�e dla wa�niejszych rzeczownik�w z opis�w tych
wcze�niej wymienionych (tych ww ITEM�w). Na przyk�ad, je�li opis "ska�y"
wspomina du�� jaskini�, zatem lokacja powinna wyszczeg�lnia� itemy "jaskinia"
i "du�a jaskinia". Dostarczaj tak�e opis�w ITEM�w, kt�re musz� si� tam po
prostu znajdowa� (np. "pod�oga", "�ciany" i "sufit" w ka�dym zamkni�tym
pomieszczeniu).

 - Odpowiadaj na rozs�dne pr�by manipulacji przedmiotami (itemami) wymienionymi
w opisie lokacji. Je�li wspomina on o drzewie, wtedy odpowiadaj na pr�by
wspi�cia si� na nie. Je�li na lokacji znajduje si� strumyk, pami�taj o
mo�liwo�ci napicia si� z niego, a tak�e p�ywania/nurkowania/chlapania, itp.
Nie jest oczywi�cie konieczne pozwolenie �miertelnikowi na dan� czynno��, ale
"�adna z ga��zi nie wystaje na tyle nisko, aby m�c po niej si� wspi��." jest
znacznie lepsz� odpowiedzi� ni� "S�ucham?".

 - Nie zak�adaj z g�ry, opisuj�c cokolwiek, �e �miertelnik pod��a w okre�lonym
kierunku. To jest, unikaj stwierdze� "Jeste� ju� niemal na samym szczycie,
jeszcze jedno podci�gni�cie." - a mo�e �miertelnik w�a�nie zacz�� schodzi�?

 - Nagradzaj uwa�nych, wnikliwych i ciekawskich �miertelnik�w. Nagroda nie musi
by� namacalna i materialna. Odnalezienie kontur�w szkieletu Staro�ytnego Smoka
dziesi�� p�ek skalnych pod nami ju� samo w sobie jest frajd�.

 - B�d� dok�adny i szczeg�owy w opisach.
"D�b szypu�kowy" jest bardziej przekonuj�cy, ni� "drzewo";
"Czarno-brunatny ujadaj�cy wilczarz" bardziej dzia�a na wyobra�ni�, ni�
"kundel". U�ywajcie swej w�asnej wyobra�ni, ale te� korzystajcie z setek
�r�de� w sieci, w s�ownikach, leksykonach - a �wiat stanie si� bogaty w
detale.

 - NIGDY nie obra�aj
�miertelnika, szczeg�lnie za ciekawo�� i wnikliwo��. Je�li
kto� zadaje sobie trud przygl�dania si� ka�demu przedmiotowi w pokoju (tj.
rzeczownikowi z opisu lokacji), powinien zosta� wynagrodzony poprzez obrazowe
szczeg�y sk�adaj�ce si� na �ywy �wiat - a nie strzelony w pysk has�em "Czytaj
dok�adnie opis, debilu!".

  5. NPCe (Non-Player Characters - istoty nie sterowane przez graczy)
  -------------------------------------------------------------------

 - Opisy NPC�w powinny by� pisane z tak� sam� pieczo�owito�ci� jak opisy
lokacji. Oznacza to, �e powinni�my unika� form "Ty", "Jest to", kt�re mowi�
�miertelnikom co powinni my�le�, itd.

 - NPCe powinny odpowiada� na rozs�dne i adekwatne pytania. W szczeg�lno�ci,
je�eli NPC wypowiada jak�� kwesti�, powinien odpowiada� na pytania zwi�zane z
jego wcze�niejszymi wypowiedziami.

 - NPCe powinny si� zachowywa� "rozs�dnie". Nie jest, przyk�adowo, zachowaniem
rozs�dnym dla stra�nika, aby beztrosko si� przygl�da�, gdy kto� morduje
mieszczan. Nie jest zachowaniem rozs�dnym dla kasztelana t�pe przygl�danie
si�, gdy kto� przeszukuje jego gabinet. Powinni oni odpowiednio reagowa� na
czyny �miertelnik�w. �ebrak winien jest reagowa� na podarek, tak samo jak
minstrel na pro�b� o pie��.
 U�ywaj propa NPC_I_NO_LOOKS dla NPC�w, kt�re s� wyra�nie opisani. No bo po
c� opisywa� jak pi�kna jest elfka, kt�ra dostojnie przemkn�a niemal �okie�
od nas, gdy w jej opisie widnieje "Wygl�da paskudnie jak na elfk�."?

 - W podobny spos�b, ustawiaj wzrost i wag� NPC�w adekwatnie do ich wygl�du.
Wysoki szczup�y mieszczanin nie powinien mie� opisu "Jest bardzo niski i
niespotykanie gruby jak na cz�owieka."

 - Je�eli w d�ugim opisie lokacji wskazujemy na obecno�� okre�lonego NPCa,
generalnie dobrym pomys�em by�oby mie� ich zawsze obecnych na lokacji jako
"npc object". Umo�liwia to interakcj�, kt�ra winna mie� miejsce gdy tylko NPC
jest obecny.

 - NPCe nie powinny dysponowa� nieadekwatnymi atrybutami. Je�li potrafi� one
widzie� w ciemno�ci, _MUSI_ po temu by� jaki� pow�d, prawda?

  6. Wnioski ko�cowe

  Czarodzieje opisuj� szczeg�owo, a �miertelnicy interpretuj� jak tylko chc�.
 (Notka t�umacza: nie wiem sk�d Thurg to wyszpera�, ale podpisuj� si� pod tym
 wszystkimi 5476 palcami. Swoj� drog�: zauwa�yli�cie mo�e, �e:
   a) Cholera... szkoda, �e tak p�no to przeczyta�em.
   b) Moim skromnym zdaniem, mog� istnie� pewne odst�pstwa od przytoczonych
   zasad.
   c) Ha! Ha! - Chyba jednak nikt z Arko-wizuff tego nigdy nie czyta�?
   d) Strony czynna i bierna w j�zyku polskim prezentuj� si� nieco inaczej ni�
      w angielskim - pozwoli�em sobie na pewn� modyfikacj� notki oryginalnej
     dla potrzeb NAS. Nadawanie s�owu pisanemu "�ycia" jest nieco odmienne.
