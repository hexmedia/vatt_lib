/*
 * Na podstawie opisu Alcyone
 */
inherit "/std/herb";

void
create_herb()
{
    ustaw_nazwe("korze^n");

    dodaj_przym("bia^lor^o^zowy", "bia^lor^o^zowi");
    dodaj_przym("kulisty", "kuli^sci");

    set_id_long("Na pierwszy rzut oka rozpoznajesz w tej ro^slinie czosnek, " +
	"gdy^z unosi si^e od niej charakterystyczny dra^zni^acy nozdrza " +
	"zapach. G^l^owka czosnku i jej z^abki otulone s^a bia^lawymi " +
	"^luskami, kt^ore z ^latwo^sci^a mo^zna oderwa^c. Ro^slina ta " +
	"nie jest u^zywana tylko do przypraw, ale r^ownie^z jest wybitnie " +
	"lecznicza. Mo^ze przynie^s^c ulg^e na niekt^ore dolegliwo^sci " +
	"i z powodzeniem oczyszcza organizm. Ma bardzo ostry, pal^acy smak " +
	"i specyficzny zapach.\n");

    ustaw_nazwe_ziola("czosnek");

    set_amount(20);
    set_decay_time(300);
    set_id_diff(0);
    set_herb_value(10);
    
}
