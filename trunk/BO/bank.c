// molder 12.2005
inherit "/std/room";
inherit "/lib/bank.c";

#include <stdproperties.h>
//#include "../rinde.h"

void
create_room() 
{
    set_short("Ma^le zakurzone pomieszczenie");
    set_long("Niskie pomieszczenie, kt^orego skrzypi�c� pod�og� pokrywa " +
		"spora warstwa kurzu, sprawia wra�enie ubogiej, wiejskiej chatki. " +
		"W powietrzu unosi si^e dusz^acy zapach dymu, dra�ni�cy nozdrza. " +
		"Pod jedn� ze �cian ustawione jest spore biurko, kt�re prze�y�o ju� " +
		"swoje lata co wida� po resztkach ciemnej farby, jak� zosta�o " +
		"pomalowane. Wystaj�cy za nim wielki fotel, kt^ory niegdy^s zosta^l " +
		"obity jasnym pluszem, teraz na wp^o^l zosta^l zjedzony przez mole. " +
		"Fotel s^lu�y zapewne osobie, kt�rej to pomieszczenie nie jest obce " +
		"i bez skr�powania mo�e si� w nim rozsi���. Po lewej stronie " +
		"znajduj� si� spore, d�bowe drzwi, zza kt�rych dobiega cichy, " +
		"rytmiczny brz�k przeliczanych monet. Przy suficie wprawiono " +
		"ma^le okienko wpuszczaj^ace do ^srodka niewiele swiat^la, " +
		"gdy^z przys^loni^ete jest ciemn^a zas^lon^a.\n");
		
    add_item("pod^log^e",
		"Drewniana pod�oga, kt^ora skrzypi cicho pod naporem twoich st^op, " +
		"pokryta jest spor^a warstw^a kurzu.\n");
		
    add_item("biurko",
		"Sporej wielko�ci, drewniane biurko ustawione jest pod jedn^a ze " +
		"^scian. Po resztach ciemnej farby, kt^or^a zosta^lo pomalowane " +
		"wida^c, i^z prze^zy^lo ju^z ono swoje najlepsze lata. Blat " +
		"zawalony jest spor^a ilo^sci^a papier^ow i r^o^znych dokument^ow, " +
		"starannie na nim pouk^ladanych.\n");
		
    add_item("fotel",
		"Zza du^zego biurka, stoj^acego pod jedn^a ze ^scian wystaje " +
		"wielki, drewniany fotel. Niegdy^s zosta^l obity jasnym i mi^ekkim " +
		"pluszem. Jednak teraz w po^lowie zosta^l zjedzony przez mole.\n");
		
    add_item("drzwi",
		"Do jednej ze ^scian wprawione s^a spore, d^ebowe drzwi, zza " +
		"kt^orych dobiega cichy, rytmiczny brz^ek przeliczanych monet.\n");
		
    add_item( ({"okienko","zas^lon^e" }),
		"Ma^le okienko wprawione zosta^lo tu^z przy suficie. Wpuszcza do " +
		"^srodka niewiele swiat^la, gdy^z przys^loni^ete jest star^a, " +
		"ciemn^a zaslona.\n");
		
    add_event("Zza drzwi dochodzi cichy brz^ek przeliczanych monet.\n");
    add_event("Pod^loga skrzypi cicho pod naporem twoich st^op.\n");
    add_event("Drzwi z trzaskiem obi^ly si^e o futryn^e.\n");
    add_event("Zas^lona zako^lysa^la si^e lekko, poruszona przez wiatr.\n");
	
    add_exit("place",({"wyjscie", "do wyj^scia"}));

    // nikt nie lubi tabliczek :P 
    add_item("tabliczke", "@@standard_bank_sign@@");
	
    add_prop(ROOM_I_INSIDE,1);
        
    set_bank_fee(1);
    config_default_trade();
    config_trade_data();
}

void
init()
{
	::init();
	bank_init();
}
