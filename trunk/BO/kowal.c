/* Kowal z Rinde.
      by Lil 19.08.05               */

#pragma strict types
inherit "/std/humanoid.c";
inherit "/lib/craftsman.c";

#include <macros.h>
#include <stdproperties.h>
#include <money.h>
#include <pl.h>
#include <formulas.h>
#include <std.h>
#include <ss_types.h>
#include <wa_types.h>
#include <composite.h>
#include <filter_funs.h>

void
create_humanoid()
{
    ustaw_odmiane_rasy("m�czyzna");
    dodaj_nazwy(({"kowal", "kowala", "kowalowi", "kowala",
                  "kowalem", "kowalu"}),
                ({"kowale", "kowali", "kowalom", "kowali",
                  "kowalami", "kowalach"}), PL_MESKI_OS);
    set_gender(G_MALE);
    set_title(", Kowal");
    set_long("Silne ramiona, pot�na sylwetka i d�onie, kt�rych sk�ra "+
        "jest wyrobiona niemal jak rzemie� wzbudzaj� respekt i "+
        "szacunek nie tylko w�r�d klient�w. Jego ku�nia to jego "+
        "w�asne kr�lestwo. Ponadto �w jasnow�osy olbrzym, o nieodgadnionym "+
        "spojrzeniu niebieskich oczu jest obiektem westchnie� wielu "+
        "okolicznych dziewcz�t i panienek. Nie zwyk� nosi� koszuli, "+
        "jego nagi, doskonale wyrze�biony tors po�yskuje w �wietle "+
        "padaj�cym z paleniska. Na nogach ma lu�ne, p��cienne spodnie "+
        "wsadzone w cholewy wysokich but�w. Jego policzek przecina "+
        "d�uga, szpec�ca blizna, a na tu�owiu i ramionach wida� "+
        "pozosta�o�ci po wielu innych ranach.\n");
    dodaj_przym("pot�ny","pot�ni");
    dodaj_przym("jasnow�osy","jasnow�osi");


    set_stats ( ({ 74, 41, 78, 21, 30, 84 }) );
    set_skill(SS_DEFENCE, 50 + random(4));
    set_skill(SS_WEP_CLUB, 33 + random(10));
    set_skill(SS_PARRY, 10 + random(10));
    set_skill(SS_UNARM_COMBAT, 55 + random(15));

    add_prop(NPC_I_NO_RUN_AWAY, 1);
    add_prop(CONT_I_WEIGHT, 91000);
    add_prop(CONT_I_HEIGHT, 172);

    craftsman_set_sold_item_names( ({"kom�rk�"}) );
}

void
init()
{
    ::init();
    craftsman_init();
}

void
craftsman_configure_order(int order_id, string co)
{
    craftsman_set_item_file(order_id, "/d/Wiz/delvert/komorka");

    craftsman_set_time_to_complete(order_id, 10);

    craftsman_set_item_cost(order_id, 100);

    craftsman_add_attribute(order_id, "kolor", ({"zielona", "��ta"}) );
}
