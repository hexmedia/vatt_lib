/**
 * \file /sys/colors.h
 *
 * Plik zawieraj�cy definicje kolor�w do mieszacza kolor�w, oraz definicje kolor�w wy�wietlanych na ekranie
 *
 * @author Krun - definicje do mieszacza
 * @author Jeremian - definicje do kolor�w wy�wietlanych
 * @date zima 2006/2007
 */

#ifndef COLORS_H
#define COLORS_H

#include <files.h>

/**
 * Definicje do mixera kolor�w:
 */
#define CL_RGB_BEZOWY           ({200, 190, 185})
#define CL_RGB_BIALY            ({0, 0, 0})
#define CL_RGB_BORDOWY          ({80, 40, 70})
#define CL_RGB_BRZOSKWINIOWY    ({220, 150, 90})
#define CL_RGB_BRAZOWY          ({150, 100, 50})
#define CL_RGB_BRUNATNY         ({120, 80, 50})
#define CL_RGB_BURACZKOWY       ({160, 60, 95})
#define CL_RGB_BURSZTYNOWY      ({255, 190, 0})
#define CL_RGB_BLEKITNY         ({0, 205, 255})
#define CL_RGB_CIELISTY         ({190, 155, 130})
#define CL_RGB_CEGLASTY         ({205, 100, 50})
#define CL_RGB_CYTRYNOWY        ({255, 245, 85})
#define CL_RGB_CZARNY           ({255, 255, 255})
#define CL_RGB_CZEKOLADOWY      ({65, 25, 15})
#define CL_RGB_CZERWONY         ({255, 0, 0})
#define CL_RGB_FIOLETOWY        ({120, 95, 175})
#define CL_RGB_GRAFITOWY        ({85, 90, 110})
#define CL_RGB_GRANATOWY        ({0, 50, 155})
#define CL_RGB_KARMAZYNOWY      ({220, 20, 60})
#define CL_RGB_MARCHEWKOWY      ({235, 115, 55})
#define CL_RGB_MIEDZIANY        ({120, 75, 35})
#define CL_RGB_MIODOWY          ({120, 75, 5})
#define CL_RGB_NIEBIESKI        ({0, 0, 255})
#define CL_RGB_OLIWKOWY         ({130, 130, 0})
#define CL_RGB_PLATYNOWY        ({220, 210, 200})
#define CL_RGB_PURPUROWY        ({130, 0, 130})
#define CL_RGB_ROZOWY           ({255, 205, 220})
#define CL_RGB_RUBINOWY         ({120, 5, 45})
#define CL_RGB_SELEDYNOWY       ({100, 155, 155})
#define CL_RGB_SREBRNY          ({200, 210, 210})
#define CL_RGB_SZARY            ({130, 130, 130})
#define CL_RGB_TURKUSOWY        ({99, 210, 230})
#define CL_RGB_ZIELONY          ({0, 255, 0})
#define CL_RGB_ZOLTY            ({255, 255, 0})
#define CL_RGB_ZLOTY            ({220, 205, 75})
#define CL_RGB_ZLOCISTY         ({255, 240, 120})

#define CL_NAZWA_BEZOWY         ({"be^zowy", "be^zowi"})
#define CL_NAZWA_BIALY          ({"bia^ly", "bia^li"})
#define CL_NAZWA_BORDOWY        ({"bordowy", "bordowi"})
#define CL_NAZWA_BRZOSKWINIOWY  ({"brzoskwiniowy", "brzoskwiniowi"})
#define CL_NAZWA_BRAZOWY        ({"br^azowy", "br^azowi"})
#define CL_NAZWA_BRUNATNY       ({"brunatny", "brunatni"})
#define CL_NAZWA_BURACZKOWY     ({"buraczkowy", "buraczkowi"})
#define CL_NAZWA_BURSZTYNOWY    ({"bursztynowy", "bursztynowi"})
#define CL_NAZWA_BLEKITNY       ({"b^lekitny", "b^lekitni"})
#define CL_NAZWA_CIELISTY       ({"cielisty", "cieli^sci"})
#define CL_NAZWA_CEGLASTY       ({"ceglasty", "cegla^sci"})
#define CL_NAZWA_CYTRYNOWY      ({"cytrynowy", "cytrynowi"})
#define CL_NAZWA_CZARNY         ({"czarny", "czarni"})
#define CL_NAZWA_CZEKOLADOWY    ({"czekoladowy", "czekoladowi"})
#define CL_NAZWA_CZERWONY       ({"czerwony", "czerwoni"})
#define CL_NAZWA_FIOLETOWY      ({"filetowy", "filetowi"})
#define CL_NAZWA_GRAFITOWY      ({"grafitowy", "grafitowi"})
#define CL_NAZWA_GRANATOWY      ({"granatowy", "granatowi"})
#define CL_NAZWA_KARMAZYNOWY    ({"karmazynowy", "karmazynowi"})
#define CL_NAZWA_MARCHEWKOWY    ({"marchewkowy", "marchewkowi"})
#define CL_NAZWA_MIEDZIANY      ({"miedziany", "miedziani"})
#define CL_NAZWA_MIODOWY        ({"miodowy", "miodowi"})
#define CL_NAZWA_NIEBIESKI      ({"niebieski", "niebiescy"})
#define CL_NAZWA_OLIWKOWY       ({"oliwkowy", "oliwkowi"})
#define CL_NAZWA_PLATYNOWY      ({"platynowy", "platynowi"})
#define CL_NAZWA_PURPUROWY      ({"purpurowy", "purpurowi"})
#define CL_NAZWA_ROZOWY         ({"r^o6zowy", "r^o^zowi"})
#define CL_NAZWA_RUBINOWY       ({"rubinowy", "rubinowi"})
#define CL_NAZWA_SELEDYNOWY     ({"seledynowy", "seledynowi"})
#define CL_NAZWA_SREBRNY        ({"srebrny", "srebrni"})
#define CL_NAZWA_SZARY          ({"szary", "szarzy"})
#define CL_NAZWA_TURKUSOWY      ({"turkusowy", "turkusowi"})
#define CL_NAZWA_ZIELONY        ({"zielony", "zieloni"})
#define CL_NAZWA_ZOLTY          ({"z^o^lty", "z^o^lci"})
#define CL_NAZWA_ZLOTY          ({"z^loty", "z^loci"})
#define CL_NAZWA_ZLOCISTY       ({"z^locisty", "z^locisci"})

#define CL_NAZWA_CIEMNO         "ciemno"
#define CL_NAZWA_JASNO          "jasno"

#define CL_RGB_KOLOROW ({CL_RGB_BEZOWY, CL_RGB_BIALY, CL_RGB_BORDOWY, CL_RGB_BRZOSKWINIOWY, CL_RGB_BRAZOWY, CL_RGB_BRUNATNY, CL_RGB_BURACZKOWY, CL_RGB_BURSZTYNOWY, CL_RGB_BLEKITNY, CL_RGB_CIELISTY, CL_RGB_CEGLASTY, CL_RGB_CYTRYNOWY, CL_RGB_CZARNY, CL_RGB_CZEKOLADOWY, CL_RGB_CZERWONY, CL_RGB_FIOLETOWY, CL_RGB_GRAFITOWY, CL_RGB_GRANATOWY, CL_RGB_KARMAZYNOWY, CL_RGB_MARCHEWKOWY, CL_RGB_MIEDZIANY, CL_RGB_MIODOWY, CL_RGB_NIEBIESKI, CL_RGB_OLIWKOWY, CL_RGB_PLATYNOWY, CL_RGB_PURPUROWY, CL_RGB_ROZOWY, CL_RGB_RUBINOWY, CL_RGB_SELEDYNOWY, CL_RGB_SREBRNY, CL_RGB_SZARY, CL_RGB_TURKUSOWY, CL_RGB_ZIELONY, CL_RGB_ZOLTY, CL_RGB_ZLOTY, CL_RGB_ZLOCISTY})

#define CL_NAZWY_KOLOROW ({CL_NAZWA_BEZOWY, CL_NAZWA_BIALY, CL_NAZWA_BORDOWY, CL_NAZWA_BRZOSKWINIOWY, CL_NAZWA_BRAZOWY, CL_NAZWA_BRUNATNY, CL_NAZWA_BURACZKOWY, CL_NAZWA_BURSZTYNOWY, CL_NAZWA_BLEKITNY, CL_NAZWA_CIELISTY, CL_NAZWA_CEGLASTY, CL_NAZWA_CYTRYNOWY, CL_NAZWA_CZARNY, CL_NAZWA_CZEKOLADOWY, CL_NAZWA_CZERWONY, CL_NAZWA_FIOLETOWY, CL_NAZWA_GRAFITOWY, CL_NAZWA_GRANATOWY, CL_NAZWA_KARMAZYNOWY, CL_NAZWA_MARCHEWKOWY, CL_NAZWA_MIEDZIANY, CL_NAZWA_MIODOWY, CL_NAZWA_NIEBIESKI, CL_NAZWA_OLIWKOWY, CL_NAZWA_PLATYNOWY, CL_NAZWA_PURPUROWY, CL_NAZWA_ROZOWY, CL_NAZWA_RUBINOWY, CL_NAZWA_SELEDYNOWY, CL_NAZWA_SREBRNY, CL_NAZWA_SZARY, CL_NAZWA_TURKUSOWY, CL_NAZWA_ZIELONY, CL_NAZWA_ZOLTY, CL_NAZWA_ZLOTY, CL_NAZWA_ZLOCISTY})

/* -- Makra do mieszacza kolor�w -- */
#define RGB2HEX(r)              COLOR_MIXER->rgb_to_hex(r)
#define HEX2RGB(h)              COLOR_MIXER->hex_to_rgb(h)
#define COLOR_FIX(r)            COLOR_MIXER->generate_new_color(r)
#define COLOR_FIX_HEX(h)        COLOR_MIXER->generate_new_color_hex(h)
#define COLOR_NAME(r,p,q,l)     COLOR_MIXER->query_color_name(r,p,q,l)
#define COLOR_NAME_HEX(h,p,q,l) COLOR_MIXER->query_color_name(HEX2RGB(h),p,q,l)
#define NEAREST_COLOR(r)        COLOR_MIXER->find_nearest_color(r)
#define NEAREST_COLOR_HEX(h)    RGB2HEX(COLOR_MIXER->find_nearest_color(HEX2RGB(h)))


/**
 * Definicje do kolor�w widocznych na ekranie:
 */
#define COLOR_BOLD_ON            1
#define COLOR_DARK               2
#define COLOR_ITALICS_ON         3
#define COLOR_UNDERLINE_ON       4
#define COLOR_BLINK              5
#define COLOR_INVERSE_ON         7
#define COLOR_STRIKETHROUGH_ON   9
#define COLOR_BOLD_OFF          22
#define COLOR_ITALICS_OFF       23
#define COLOR_UNDERLINE_OFF     24
#define COLOR_INVERSE_OFF       27
#define COLOR_STRIKETHROUGH_OFF 29
#define COLOR_FG_BLACK          30
#define COLOR_FG_RED            31
#define COLOR_FG_GREEN          32
#define COLOR_FG_YELLOW         33
#define COLOR_FG_BLUE           34
#define COLOR_FG_MAGENTA        35
#define COLOR_FG_CYAN           36
#define COLOR_FG_WHITE          37
#define COLOR_FG_DEFAULT        39
#define COLOR_BG_BLACK          40
#define COLOR_BG_RED            41
#define COLOR_BG_GREEN          42
#define COLOR_BG_YELLOW         43
#define COLOR_BG_BLUE           44
#define COLOR_BG_MAGENTA        45
#define COLOR_BG_CYAN           46
#define COLOR_BG_WHITE          47
#define COLOR_BG_DEFAULT        49

#define COLOR_FG_START          30
#define COLOR_FG_END            39

#define COLOR_BG_START          40
#define COLOR_BG_END            49

/* -- Makra do kolor�w widocznych na ekranie gracza FIXME: Do wycofania-- */
#define SET_COLOR(x)            set_color(x)
#define SET_COLOR1(x,y)         set_color(x,y)
#define SET_COLOR2(x,y,z)       set_color(x,y,z)
#define CLEAR_COLOR             clear_color()

/* -- Makra do kolorwania standardowego kolorowania -- */
#define KOLORUJ_POZIOM(x,y,z)           COLORS_FILE->koloruj_poziom((x),(y),(z))
#define KOLORUJ_POZIOM1(x,y,z)          COLORS_FILE->koloruj_poziom((x),(y),(z),0)
#define KOLORUJ_POZIOM2(x,y,z,v,d)      COLORS_FILE->koloruj_poziom((x),(y),(z),(v),(d))
#define KOLORUJ_POZIOM3(x,y,z,v)        COLORS_FILE->koloruj_poziom((x),(y),(z),(v))

#define KOLORUJ_TABLICE(x,y)            COLORS_FILE->koloruj_tablice((x),(y))
#define KOLORUJ_TABLICE1(x,y,z)         COLORS_FILE->koloruj_tablice((x),(y),(z))
#define KOLORUJ_TABLICE2(x,y,z,v)       COLORS_FILE->koloruj_tablice((x),(y),(z),(v))

#endif
