MIECZE:
(*) {N|%k:0 n}ieuwa�nie dopuszcza{sz %p:3| ci�| %p:3} zbyt blisko by pewnie ci��, przez co zadany �rodkiem klingi %b:1 cios nie rani {[go|jej|%p]|ci�|[go|jej|%p]} tak mocno jakby m�g�.

(**) {R|%k:0 r}zuc{asz|a} si� w{| twoj� | }stron�{ %p:1 || %p:1 }ca�ym cia�em jednocze�nie tn�c zza ucha %b:4. Rozp�dzone ostrze ci�gnie za sob� wianuszek krwii po tym rozcina {ci|[mu|jej|mu|im|im]} %h:3. <<<< ACHTUNG

SZABLE:
(*) {P|%k:0 p}rawie przykl�kuj{esz|e} w dalekim wypadzie tn�c {%p:3|ci�|%p:3} po %h:5 {morderczo/niespotykanie/niesamowicie/niezwykle} szybkim ruchem %b:4, kt�r[ego|ej|%b] ostrze wyrzuca w g�r� jasn� smug� krwii.

TOPORY:
(**) {R|%k:0 r}�bi{esz|e} na odlew %p:3 %b:4 trafiaj�c {[go|j�|%p]|ci�|[go|j�|%p]} w %h:3. Cofaj�c si� po ciosie {z trudem/z trudno�ci�/ledwie} wyszarpujesz bro� z krwawi�cej rany.

(**) Prostym uderzeniem z g^ory {t|%k:0 t}rafi{asz|a} {%p:3|ci^e|%p:3} w %h:3, zbli^zaj^ac si^e jednocze^snie bardzo, po tym jak poni^os^l {ci^e|[go|j^a|je|ich|je]} impet %b:1.

(**) {B|%K:0 b}ierz{esz|e} szeroki zamach zza g^lowy ods^laniaj^ac si^e tym samym. Jednak^ze gdy {%p:0 r}ob{isz|i} krok do przodu, chc^ac to wykorzysta^c otrzymuj{esz|e} kopni^ecie w pier^s, za kt^orym opada nieub^lagane ostrze %b:1 trafiaj^ac {ci^e|[go|to|j^a|ich|je]} prosto w %h:3. <<ACHTUNG

SZTYLETY:
Zadaj�c %b:4 kolejne b�yskawiczne, krzy�uj�ce si� ci�cia {s|%k:0 s}prawi{asz|a}, �e zmienia si� w l�ni�c� �semk�. Nie spos�b nawet zauwa�y�, kt�re z nich dosi�ga{ ci� | }w ko�cu{ | %p:3 }w %h:3. <<< ACHTUNG!!! <<<