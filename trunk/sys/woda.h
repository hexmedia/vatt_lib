/* plik z definicjami potrzebnymi do p�ywania w wodzie ;p
 * Vera.
 */

//przynajmniej na ile musimy mie� teorii+praktyki p�ywania
//by od razu nie uton�� :)
#define LIMIT_UM_SWIM   5
//prop definiuj�cy ilo�� powietrza podczas toni�cia (il. powietrza przy nurkowaniu 
//sama si� wylicza na podst. stat�w i innych)
#define IL_POWIETRZA_KRYTYCZNEGO    15
//przynajmniej ile jeszcze sil musimy miec by moc wyplynac na powierzchnie... dosc krytyczna sprawa;]
#define LIMIT_ZMECZENIA_NA_WYPLYWANIE   220
//przynajmniej ile jeszcze sil musimy miec by moc zanurkowac glebiej
#define LIMIT_ZMECZENIA_NA_NURKOWANIE   350
//przynajmniej ile jeszcze sil musimy miec by moc przeplynac na lok. obok
#define LIMIT_ZMECZENIA_NA_PRZEPLYWANIE 400
//o ile redukujemy powietrze w alarmie, podczas nurkowania:
#define REDUKUJEMY_POWIETRZE    (2 + random(3))



//lista zakazanych komend w wodzie, na powierzchni:
#define ZAKAZANE_NA_POWIERZCHNI ({"tupnij","podskocz","dygnij","podrepcz", \
                                  "przebieraj","nadepnij","przest�p","trop",\
                                  "zabij","wesprzyj"})
//lista zakazanych komend pod wod�:
#define ZAKAZANE_POD_WODA       ({"tupnij","podskocz","dygnij","powiedz","szepnij", \
                                  "krzyknij","kla�nij","klaszcz","zaklaszcz","zagwi�d�", \
                                  "westchnij","spoliczkuj","chrz�knij","odetchnij", \
                                  "podrepcz","przebieraj","otrzyj","nadepnij","przest�p", \
                                  "prychnij","trop","zapal","zabij","wesprzyj"})
//prop definiuj�cy ile jeszcze mamy powietrza, je�li jeste�my pod wod�. odejmujemy co chwila.
#define IL_POWIETRZA    "_il_powietrza"
//a to jest maksymalna warto�� powietrza.
#define IL_POWIETRZA_MAX    "_il_powietrza_max"
//ile poziom�w lokacji 'pod wod�' mamy pod t� lokacj�.
//je�li ustawimy np. 3 to b�d� 3 lokacje: 2 'pod wod�' oraz 1 'na dnie'
#define IL_POZIOMOW     "_il_poziomow"
//ktorym poziomem jest dana lokacja:
#define POZIOM          "_poziom"
//a ten prop dajemy chwilowo tym, ktorzy probuja z wody wlezc na lodke ;p
#define WSPINAM_SIE_NA_LODZ "_wspinam_sie_na_lodz"
//ten tez chwilowo, na czas wioslowania:
#define WIOSLUJE "_wiosluje"
//wyliczona �rednia g�sto�ci wszystkich materia��w sk�adaj�cych si� na dany obiekt:
#define SUMA_GESTOSCI   "_suma_gestosci"
//w graczu prop ten oznacza sume_gestosci wszystkich rzeczy z jego ekwipunku
#define SUMA_GESTOSCI_EKW   "_suma_gestosci_ekw"
//tego propa dajemy graczowi, i przypisujemy do niego lokacj� na kt�rej po raz
//pierwszy dali�my mu alarm na p�ywanie. Ta w�a�nie lokacja b�dzie przetrzymywa�
//wszystkie alarmy na p�ywanie,powietrze,toni�cie.
#define PIERWSZA_ALARM_LOK  "_pierwsza_alarm_lok"
//ID alarmu ustawiane jako prop w graczu. alarmy za� s� ustawiane w lokacjach.
#define PLYWAM_ALARM    "_plywam_alarm"
#define POWIETRZE_ALARM "_powietrze_alarm"
//gracz zaczyna ton��
#define TONIE   "_tonie"
//definiuje �e na lokacji dna tej lokacji znajduj� si� raki
#define SA_RAKI "_sa_raki"


#define DBG(x) find_player("Xvera")->catch_msg("woda dbg: "+x+"\n");