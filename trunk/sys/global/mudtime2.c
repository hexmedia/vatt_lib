
/**
 * Kalendarz Wiedźminlandu
 *
 * \file /sys/global/mudtime2.c
 *
 * @author Molder
 * @date   04.2004
 * @version 1.0
 *
 * @author  Krun
 * @date    2008
 * @version 1.1
 */

#pragma unique
#pragma no_clone
#pragma no_reset
#pragma no_inherit
#pragma save_binary
#pragma strict_types

#include <math.h>
#include <time.h>
#include <files.h>
#include <macros.h>
#include <mudtime.h>
#include <language.h>
#include <sun_times.h>
#include <stdproperties.h>

#define DBG(x) find_player("krun")->catch_msg("MUDTIME2: " + (x) + "\n")


//Oba wychodzą w chwili kodowania na 1426. Elfi jest przesuwany z roku gregoriańskiego
//natomiast ludzki z roku żydowskiego.
#define LUDZ_START_YEAR     4522
#define ELF_START_YEAR      762

public int pora_dnia(mixed x = "", mixed y = "");

/*
 * opis godziny (pora, przed-po godzina)
 */
private string
godzina(void)
{
    string uzup, godzina, uzup2;
    int godzinapom = GODZINA;

    switch(pora_dnia())
    {
        case MT_SWIT:
            uzup2 = "^swit";
            break;
        case MT_WCZESNY_RANEK:
            uzup2 = "wczesny ranek";
            break;
        case MT_RANEK:
            uzup2 = "ranek";
            break;
        case MT_POLUDNIE:
            uzup2 = "^srodek dnia";
            break;
        case MT_POPOLUDNIE:
            uzup2 = "popo^ludnie";
            break;
        case MT_WIECZOR:
            uzup2 = "wiecz^or";
            break;
        case MT_POZNY_WIECZOR:
            uzup2 = "p^o^xny wiecz^or";
            break;
        case MT_NOC:
            uzup2 = "noc";
            break;
    }

    /* jesli trzeba doda godzine */
    switch (MINUTA)
    {
        case 0..4:
            uzup = " godzina ";
            break;
        case 5..30:
            uzup = " po ";
            break;
        case 31..55:
            godzinapom += 1;
            uzup = " przed ";
            break;
        case 56..60:
            godzinapom += 1;
            uzup = " godzina ";
            break;
    }

    switch(uzup)
    {
        case " przed ":
            godzina = LANG_SORD(((godzinapom - 1) % 12) + 1, PL_BIE, PL_ZENSKI);
            break;
        case " godzina ":
            godzina = LANG_SORD(((godzinapom - 1) % 12) + 1, PL_MIA, PL_ZENSKI);
            break;
        case " po ":
            godzina = LANG_SORD(((godzinapom - 1) % 12) + 1, PL_DOP, PL_ZENSKI);
            break;
        default : return "b^l^ad w przed_po";
    }

    return uzup2 + "," + uzup + godzina ;
}

/**
 * @return aktualna pora dnia
 */
public int
pora_dnia(mixed x = "", mixed y = "")
{
/*    object pobj = previous_object(0);

    for (int i = 0 ; (i > -50 && (pobj = previous_object(-i))) ; i--)
    {
        find_player("krun")->catch_msg(calling_function(i) + ", " + (calling_object(i) ? OB_NAME(calling_object(i)) + ", " : "" ) + OB_NAME(pobj) + "; ");
    }

    find_player("krun")->catch_msg("\n\n");*/
    //Musiałem troche przerobić, żeby pobierało czasy.
    object r, p;

    return MT_POLUDNIE;

    if(x == "" || y == "")
    {
        r = previous_object_not_me();

        while(r && !r->is_room())
            r = ENV(r);
        
        if (r) 
        {
            x = r->query_prop(ROOM_I_WSP_X);
            y = r->query_prop(ROOM_I_WSP_Y);
        }
    }

    p = POGODA_OBJECT->query_plan(x, y);

    int  teraz      = time() / 60;
    int  wschod     = p->get_wschod() / 60;
    int  zachod     = p->get_zachod() / 60;
    int  poludnie   = mktime(0, 0, 12) / 60;
    int *ltime      = localtime();

    if(!wschod)
    {   //Jeśli nie udało się nowym sposobem to próbujemy starym
        wschod = SUN_TIMES[ltime[7]-1][0] + mktime(0, 0, 0) / 60;
    }

    if(!zachod)
    {   //Tu postępujemy podobnie
        zachod = SUN_TIMES[ltime[7]-1][1] + mktime(0, 0, 0) / 60;
    }

    //Musiałem to całkiem przebudować bo na troche innej zasadzie jest teraz wyliczane.

    //Najpierw te definicje które sztywno określają godziny:)
    if((ltime[2] == 11 && ltime[1] > 30) || (ltime[2] == 12 && ltime[1] < 30))
        return MT_POLUDNIE;

    //A teraz to co jest płynne, zależne od dnia.
    if(wschod - 45 > teraz)
        return MT_NOC;

    if((wschod + PROC(poludnie - wschod, 5)) > teraz)
        return MT_SWIT;

    if((wschod + PROC(poludnie - wschod, 50)) > teraz)
        return MT_WCZESNY_RANEK;

    if(poludnie > teraz)
        return MT_RANEK;

    if(teraz < PROC(zachod, 79))
        return MT_POPOLUDNIE;

    if(zachod + 10 > teraz)
        return MT_WIECZOR;

    if(zachod + 45 > teraz)
        return MT_POZNY_WIECZOR;

    return MT_NOC;
}

/**
* @return Aktualną porę roku
* @see \file mudtime.h
* MT_WIOSNA - wiosna
* MT_LATO - lato
* MT_JESIEN - jesien
* MT_ZIMA - zima
*
*/
nomask public int
pora_roku()
{
    int dr = DZIEN_ROKU - PRZESTEPNY(ROK);

    //21 marca do 21 czerwca
    if (dr > 79 && dr < 171)
        return MT_WIOSNA;
    else if (dr > 172 && dr < 264) //22 czerwca do 22 września
        return MT_LATO;
    else if (dr > 265 && dr < 354) //23 września do 20 grudnia
        return MT_JESIEN;
    else //21 grudnia do 21 marca
        return MT_ZIMA;
}

public int
query_day()
{
    return DZIEN;
}

public int
query_hour()
{
    return GODZINA;
}

public int
query_nhour()
{
    int toReturn = GODZINA;

    if (MINUTA >= 30)
        toReturn++;

    if (toReturn == 25)
        toReturn = 1;

    return toReturn;
}

public int
query_minute()
{
    return MINUTA;
}

public int
query_second()
{
    return SEKUNDA;
}

public string
query_time()
{
    return date("G:i:s");
}

/** *********************************************************************** **
 ** *******************   K A L E N D A R Z   E L F I   ******************* **
 ** *********************************************************************** **/

/**
 * Dzień roku w kalendarzu elfim.
 */
public int elf_dzien_roku(int d = DZIEN_ROKU, int r = ROK)
{
    int dr;
    dr = 365 + PRZESTEPNY(r);

    if(d > dr || d < 0)
        throw("Dzie^n roku w roku " + r + " nie mo^ze wykracza^c poza " + r + ".\n");

    d += 10;
    d -= (d > dr ? dr : 0);

    return d;
}

/**
 * Długość podanego miesiąca elfiego.
 */
int
elf_dlugosc_miesiaca(int m = 0, int r = (ROK - ELF_START_YEAR))
{
    r = r + ELF_START_YEAR;
    switch(m)
    {
        case 0: return 45;
        case 1: return 44 + PRZESTEPNY(r);
        case 2: return 47;
        case 3: return 48;
        case 4: return 45;
        case 5: return 46;
        case 6: return 45;
        case 7: return 45;
    }
}

/**
 * Funkcja sprawdza który elfi miesiąc elfi był podanego dnia roku
 * w podanym roku.
 *
 * @param d     dzień roku
 * @param r     rok
 */
public varargs int
elf_miesiac(int d = DZIEN_ROKU, int r = ROK)
{
    int m, d2;

    d = elf_dzien_roku(d);

    for(m = 0, d2 = 0; d2 < d ; d2 += elf_dlugosc_miesiaca(m), m++);

    return --m;
}

/**
 * @param d  dzień roku
 * @return dzień elfiego miesiąca.
 */
public varargs int
elf_dzien_miesiaca(int d = DZIEN_ROKU, int r = ROK)
{
    int d2, d3, m;

    d = elf_dzien_roku(d);

    for(m = 0, d2 = 0; d2 < d ; d2 += elf_dlugosc_miesiaca(m, r - ELF_START_YEAR), m++);

    d2 -= elf_dlugosc_miesiaca(--m, r - ELF_START_YEAR);

    d3 = d - d2;

    return d3;
}


/**
 * Który elfi rok obecnie mamy.
 */
int elf_rok(int r = ROK)
{
    return -1;
    return r - ELF_START_YEAR;
}

/**
 * Zmienia liczbę dni z początku roku na datę z kalendarza elfów.
 */
public string elf_data(int d = DZIEN_ROKU, int r = ROK)
{
    int rok = elf_rok(r);
    return LANG_SORD(elf_dzien_miesiaca(d, r), PL_MIA, PL_MESKI_NOS_NZYW) +
        " " + MT_MIESIAC_ELFI(elf_miesiac(d, r)) + (rok != -1 ? " roku " + elf_rok(r) : "");
}

/**
 * Elfie święta wypadają 1 każdego elfiego miesiąca.
 */
public varargs string elf_swieto(int d = DZIEN_ROKU, int r = ROK)
{
    int m  = elf_miesiac(d, r);
    int dm = elf_dzien_miesiaca(d, r);
    int km = elf_dlugosc_miesiaca(m, r - ELF_START_YEAR);
    int z  = km - dm;

    m++;
    m = (m > 8 ? 0 : m);

    string za;

    //Musimy obliczyć kiedy zaczyna się następny miesiąc
    if(z <= 7 && z > 1)
        return "Za " + LANG_SNUM(z, PL_MIA, PL_MESKI_NOS_NZYW) + " " + ilosc(z, "dzie^n", "dni", "dni")+ " " + MT_MIESIAC_ELFI(m) + ".\n";
    else if(z == 1)
        return "Ju^z jutro " + MT_MIESIAC_ELFI(m) + "!\n";
    else if(z == 0)
        return "Dzi^s " + MT_MIESIAC_ELFI(m) + ".\n";
}

/** *********************************************************************** **
 ** *****************   K A L E N D A R Z   L U D Z K I   ***************** **
 ** *********************************************************************** **/

int ludz_dlugosc_miesiaca(int m, int r)
{
    return KLUDZKI_FILE->max_days_in_ludz_month(m, r + LUDZ_START_YEAR);
}

/**
 * Funkcja sprawdza który miesiąc ludzki był podanego dnia roku w
 * podanym roku.
 *
 * @param d     dzień roku
 * @param r     rok
 */
public varargs int ludz_miesiac(int d = DZIEN_ROKU, int r = ROK)
{
    return KLUDZKI_FILE->dr2ludz(d, r, LUDZ_START_YEAR)[1]-1;
}

public varargs int ludz_dzien_miesiaca(int d = DZIEN_ROKU, int r = ROK)
{
    return KLUDZKI_FILE->dr2ludz(d, r, LUDZ_START_YEAR)[0];
}

public int ludz_rok(int d = DZIEN_ROKU, int r = ROK)
{
    return KLUDZKI_FILE->dr2ludz(d, r, LUDZ_START_YEAR)[2];
}

public varargs string ludz_data(int d = DZIEN_ROKU, int r = ROK)
{
    int rok = ludz_rok(r);
    return LANG_SORD(ludz_dzien_miesiaca(d, r), PL_MIA, PL_MESKI_NOS_NZYW) +
        " " + MT_MIESIAC_LUDZKI(ludz_miesiac(d, r)) + (rok != -1 ? " roku " + ludz_rok(d, r) : "");
}

/** *********************************************************************** **
 ** ********************   O B S Ł U G A   Ś W I Ą T   ******************** **
 ** *********************************************************************** **/

/**
 * Odliczanie do wazniejszych swiat w wiedzminlandzie.
 */
public varargs string swieto(int d = DZIEN_ROKU, int r = ROK)
{
    //We wszystkich kalendarzach pokazujemy święta elfie, które u ludzi również
    //się przyjeły, co do świąt ludzkich to narazie żadnych nie ma, i nie wiem
    //czy będą pokazywane gdziekolwiek:P

    string elfie = elf_swieto(d, r);

    if(elfie)
        return elfie;
    else
        return "";
}


/** *********************************************************************** **
 ** **********   W Y Ś W I E T L A N I E   K A L E N D A R Z A   ********** **
 ** *********************************************************************** **/

/**
 * Zwraca opis godziny dla komendy czas.
 */
public string czas_na_mudzie(void)
{
    return "Jest " + godzina() + ".\n";
}

/**
 * Zwraca opis dnia roku w odpowiednim kalendarzu, dla komend czas i data.
 */
public string data_na_mudzie(void)
{
    object pl = previous_interactive();

    //Sprawdzamy jakiego kalendarza gracz używa
    string data;

    switch(pl->query_prop(PLAYER_I_KALENDARZ))
    {
        case MT_KALENDARZ_LUDZKI:
            data  = "Wedle rachuby ludzi mamy dzi^s " + ludz_data() + ".\n";
            break;
        case MT_KALENDARZ_LUDZKI|MT_KALENDARZ_ELFI:
            data = "Wedle rachuby elf^ow mamy dzi^s " + elf_data() + ".\n" +
                "Za^s wedle ludzkiej rachuby " + ludz_data() + ".\n";
            break;
        default:
            data = "Wedle rachuby elf^ow mamy dzi^s " + elf_data() + ".\n";
            break;
    }

    return data + swieto();
}