/**
 * \file /sys/global/przymiotniki.c
 *
 * Plik obs�uguj�cy odmian� przymiotnik�w jak i obliczanie ich liczby mnogiej.
 *
 * @author Krun, krun@vattghern.com.pl
 * @date 15.9.2008 18:46:34
 */

#define DBG(x) find_player("krun")->catch_msg(set_color(find_player("krun"), COLOR_FG_RED) + " PRZYMITONIKI DEBUGGER: " +   \
               clear_color(find_player("krun")) + (x) + "\n");

#define WYJATKI ({"umi�niony"})

#include <colors.h>

/**
 * Funkcja ma za zadanie obliczy� liczb� mnog� z przymiotnika.
 *
 * Za napisanie tej funkcji dzi�kujemy Lurkowi z Nightala bardziej popularnie
 * znanemu jako Gruszka:)
 *
 * @param przym     przymiotnik od kt�rego liczymy liczb� mnog�.
 *
 * @return Liczbe mnog� podanego przymiotnika.
 */
string
oblicz_mnoga(string przym)
{
    int wyjatek;
    string *wyjatki, mnoga;

    wyjatki = WYJATKI;

    if(member_array(przym, wyjatki) != -1)
        wyjatek = 1;

    switch(przym[-2..])
    {
        case "�y":
            if(!wyjatek)
            {
                mnoga = przym[0..-3] + "zi";
                break;
            }
            //Intencjonalny brak breaka.
        case "dy":
            mnoga = przym[0..-3] + "dzi";
            break;
        case "ny":
            if(wyjatek || przym[-4..] == "lony")
                mnoga = przym[0..-4] + "eni";
            else if(przym[-3..] == "zny")
                mnoga = przym[0..-4] + "�ni";
            else if(member_array(przym[-3], ({'a', 'i', 'o', 'y', 'u'})) != -1)
                mnoga = przym[0..-4] + "eni";
            else
                mnoga = przym[0..-3] + "ni";
            break;
        case "ki":
            mnoga = przym[0..-3] + "cy";
            break;
        case "ry":
            mnoga = przym[0..-3] + "rzy";
            break;
        case "ty":
            mnoga = przym[0..-3] + "ci";
            break;
        default:
            mnoga = przym;
    }

//     DBG("Pojedyncza = " + przym + ", Mnoga = " + mnoga + ".\n");

    return mnoga;
}
