/**
 * \file /sys/combat.h
 *
 * Plik zawiera podstawowe definicje zwi�zane z walk�.
 */

#ifndef COMBAT___SYS
#define COMBAT___SYS

#define COMBAT_OBJECT ("/std/combat/combat_object")

#define CB_SPEED_BASE           600
#define CB_MAX_SPEED            60
#define CB_DEFAULT_SPEED        20

#define CB_HIT_REWARD           1

#define CB_THIS_LW_SKILL(b)     (SS_WEP_FIRST + (W_MISSILE + 1 + b->query_typ()))
#define CB_LW_SKILL             (SS_WEP_FIRST + W_MISSILE)

/*
 * SPOSOBY OBRONY
 */
#define CB_SPO_UNIKI            0
#define CB_SPO_PAROWANIE        1
#define CB_SPO_TARCZOWNICTWO    2

/*
 * STYLE WALKI:
 */
#define CB_STL_SILNY_NEUTRALNY          0
#define CB_STL_SILNY_OFENSYWNY          1
#define CB_STL_SILNY_DEFENSYWNY         2
#define CB_STL_SZYBKI_NEUTRALNY         3
#define CB_STL_SZYBKI_OFENSYWNY         4
#define CB_STL_SZYBKI_DEFENSYWNY        5
#define CB_STL_GRUPOWY_NEUTRALNY        6
#define CB_STL_GRUPOWY_OFENSYWNY        7
#define CB_STL_GRUPOWY_DEFENSYWNY       8
#define CB_STL_NORMALNY                 9

/* Tablica modyfikator�w zawiera odpowiednio:
 *  ({modyfikator do szybko�ci walki, modyfikator do si�y cios�w,
 *    modyfikator do szansy uniku, modyfikator do szansy sparowania broni�,
 *    modyfikator do szansy sparowania tarcz�,
 *    modyfikator do zm�czenia,
 *    modyfikator do zu�ycia miejsca na lokacji ( narazie nieaktywne) })
 *
 * Modyfikatory podajemy w formie procentowej, najlepiej od 5 do 400%:) ( - no kurwa bez jaj -_- [v] )
 *
 * TODO-AOB: Sprawdzi� warto�ci
 * kolejno�� zjebana? [v]
 */
#define CB_STL_MOD                                                  \
    ({                                                              \
        ({ 100, 100, 100, 100, 100, 100, 100, 100 }), /*normalny*/      \
        ({  25, 300,  50, 100, 200, 120, 120,  75 }), /*silny n*/   \
        ({ 100, 100, 100, 100, 100, 100, 100, 100 }), /*silny o*/   \
        ({ 100, 100, 100, 100, 100, 100, 100, 100 }), /*silny d*/   \
        ({ 100,  75, 200, 200, 200,  75, 130,  50 }), /*szybki n*/  \
        ({ 100, 100, 100, 100, 100, 100, 100, 100 }), /*szybki o*/  \
        ({ 100, 100, 100, 100, 100, 100, 100, 100 }), /*szybki d*/  \
        ({ 300,  40,  70,  70,  70, 140,  50, 500 }), /*grupowy n*/ \
        ({ 100, 100, 100, 100, 100, 100, 100, 100 }), /*grupowy o*/ \
        ({ 100, 100, 100, 100, 100, 100, 100, 100 }), /*grupowy d*/ \
    })

#define CB_STL_MOD_SZYBKOSC         0
#define CB_STL_MOD_OBRAZENIA        1
#define CB_STL_MOD_UNIKI            2
#define CB_STL_MOD_PAROWANIE        3
#define CB_STL_MOD_TARCZOWNICTWO    4
#define CB_STL_MOD_ZMECZENIE        5
#define CB_STL_MOD_ZASLANIANIE      6
#define CB_STL_MOD_POLE             7

#if 0
//Wst�pnie si� z tego wycofujemy.
/*
 * A teraz kilka modyfikator�w od stylu walki do sposobu obrony
 *
 * ({uniki, parowanie broni�, parowanie tarcz�})
 *
 * x to styl walki atakuj�cego, y broni�cego si�
 * TODO-AOB: Poustawia� warto�ci
 */
#define CB_STL_POR_DEF_MOD                          \
    ({                                              \
        ({                                          \
            ({ 100, 100, 100 }),                    \
            ({ 100, 100, 100 }),                    \
            ({ 100, 100, 100 }),                    \
            ({ 100, 100, 100 }),                    \
            ({ 100, 100, 100 }),                    \
            ({ 100, 100, 100 }),                    \
        }),                                         \
        ({                                          \
            ({ 100, 100, 100 }),                    \
            ({ 100, 100, 100 }),                    \
            ({ 100, 100, 100 }),                    \
            ({ 100, 100, 100 }),                    \
            ({ 100, 100, 100 }),                    \
            ({ 100, 100, 100 }),                    \
        }),                                         \
        ({                                          \
            ({ 100, 100, 100 }),                    \
            ({ 100, 100, 100 }),                    \
            ({ 100, 100, 100 }),                    \
            ({ 100, 100, 100 }),                    \
            ({ 100, 100, 100 }),                    \
            ({ 100, 100, 100 }),                    \
        }),                                         \
        ({                                          \
            ({ 100, 100, 100 }),                    \
            ({ 100, 100, 100 }),                    \
            ({ 100, 100, 100 }),                    \
            ({ 100, 100, 100 }),                    \
            ({ 100, 100, 100 }),                    \
            ({ 100, 100, 100 }),                    \
        }),                                         \
        ({                                          \
            ({ 100, 100, 100 }),                    \
            ({ 100, 100, 100 }),                    \
            ({ 100, 100, 100 }),                    \
            ({ 100, 100, 100 }),                    \
            ({ 100, 100, 100 }),                    \
            ({ 100, 100, 100 }),                    \
        }),                                         \
        ({                                          \
            ({ 100, 100, 100 }),                    \
            ({ 100, 100, 100 }),                    \
            ({ 100, 100, 100 }),                    \
            ({ 100, 100, 100 }),                    \
            ({ 100, 100, 100 }),                    \
            ({ 100, 100, 100 }),                    \
        }),                                         \
    })


#define CB_STL_POR_DEF_MOD_UNIKI            0
#define CB_STL_POR_DEF_MOD_PAROWANIE        1
#define CB_STL_POR_DEF_MOD_TARCZOWNICTWO    2

#endif
/*
 * WSZELKIEGO RODZAJU WRA�LIWO�CI:
 */

//Wszelkiego rodzaju wra�liwo�ci mo�na ustawia� podaj�c jako argument
//numer materia�u z pliku /sys/materialy.h te zdefiniowane tutaj
//s� dodatkowe i maj� warto�ci ujemne.

//Wra�liwo�ci na r�ne rzeczy
#define CB_WR_GORACO            1
#define CB_WR_ZIMNO             2

#define FIGHT_COMUNICATE_COLOR 41



#endif COMBAT___SYS
