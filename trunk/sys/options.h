/*
 * options.h
 *
 * This file contains all wizard option defines.
 */

#ifndef _options_h
#define _options_h


// z gena
//#define OPT_DEFAULT_STRING " 20 8036\"  !"
#define OPT_DEFAULT_STRING " 20 80       !"
// The lowest bit number for toggles, first three bytes reserved for
// more length.
#define OPT_BASE             48

///Opcje g��wne - <0, 20)

// More length
#define OPT_MORE_LEN                 0
// Szeroko�� ekranu
#define OPT_SCREEN_WIDTH             1
// Brief display mode
#define OPT_BRIEF                    2
// Echo display mode
#define OPT_ECHO                     3
// Gag all messages of misses // z gena
#define OPT_GAG_MISSES               4
// Auto-wrap long notes       // z gena
#define OPT_AUTOWRAP                 5
// Kompresja MCCP2
#define OPT_MCCP2                    6
// Protok� d�wi�ku MSP
#define OPT_MSP                      7
// Kodowanie polskich znak�w diakrytycznych
#define OPT_KODOWANIE                8
// Kolorki ansi
#define OPT_COLORS                   9
// Kolorowanie mowy
#define OPT_COLORS_SPEECH           10
// Kolorowanie wyj��
#define OPT_COLORS_EXITS            11
// Otrzymywanie przedmiot�w od innych
#define OPT_RECEIVING               12
// Ukrywanie obecno�ci w kto
#define OPT_HIDE_IN_WHO             13
#define OPT_MXP 					14

/// Opcje walki - <20, 30)

// Styl walki
#define OPT_FIGHT_STYLE             20
// Atakowane miejsce
#define OPT_FIGHT_ATAKOWANE         21
// Poziom hp przy jakim uciekamy
#define OPT_FIGHT_WHIMPY            22
// Widzenie walk innych
#define OPT_FIGHT_BLOOD             23
// Honorowa walka, bez uderze� mi�dzy nogi
#define OPT_FIGHT_HONOROWA          24
// Walka do og�uszenia (By� mo�e do wycofania)
#define OPT_FIGHT_MERCIFUL          25
// Wy��czenie walki bez broni (Nieaktywne, By� mo�e do wycofania)
#define OPT_FIGHT_UNARMED_OFF       26

/// Opcje wizard�w - <30, 40) - Nieaktywnne

// Automatic display of current directory on cwd changes
#define OPT_AUTO_PWD                30

#endif /* _options_h */
