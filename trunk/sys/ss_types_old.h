/*
 * ss_types.h
 *
 * This file defines the available stats and skills. Use this file to
 * in conjunction with query_stat() and query_skill() calls.
 *
 */

#ifndef __SSO_TYPES__
#define __SSO_TYPES__

#define SSO_MAX           150

#define SSO_STAT_DESC ({ "str", "dex", "con", "int", "dis" })
#define SSO_NO_STATS      5

#define SSO_STR           0
#define SSO_DEX           1
#define SSO_CON           2
#define SSO_INT           3
#define SSO_WIS           3
#define SSO_DIS           4

/* List of skills as is going to be used */

/* Specialized fighting skills */
#define SSO_2H_COMBAT        20
#define SSO_UNARM_COMBAT     21
#define SSO_BLIND_COMBAT     22
#define SSO_PARRY            23
#define SSO_DEFENCE          24
#define SSO_MOUNTED_COMBAT   25
#define SSO_SHIELD_PARRY     26
#define SSO_THROWING         27

/*
 * Someone said defence can be spelled defense too..
 */
#define SSO_DEFENSE          (SSO_DEFENCE)

/* Magic skills */
#define SSO_SPELLCRAFT       30
#define SSO_HERBALISM        36
#define SSO_ALCHEMY          37

/*
 * These are the forms of magic available.
 */
#define SSO_FORM_TRANSMUTATION   38
#define SSO_FORM_ILLUSION        34
#define SSO_FORM_DIVINATION      39
#define SSO_FORM_ENCHANTMENT     35
#define SSO_FORM_CONJURATION     40
#define SSO_FORM_ABJURATION      32

/*
 * These are the elements available.
 */
#define SSO_ELEMENT_FIRE     41
#define SSO_ELEMENT_AIR      42
#define SSO_ELEMENT_EARTH    43
#define SSO_ELEMENT_WATER    44
#define SSO_ELEMENT_LIFE     33
#define SSO_ELEMENT_DEATH    31

/* Umiejętności złodziejskie */
#define SSO_OPEN_LOCK            50
#define SSO_PICK_LOCK            SSO_OPEN_LOCK
#define SSO_PICK_POCKET          51
#define SSO_ACROBAT              52
#define SSO_FR_TRAP              53
#define SSO_SNEAK                54
#define SSO_HIDE                 55
#define SSO_BACKSTAB             56
#define SSO_BREAKDOWN            57

/* Umiejetność kowalskie */
#define SSO_CRAFTING_TAILOR      60
#define SSO_CRAFTING_BLACKSMITH  61
#define SSO_CRAFTING_CARVING     62

/* Umiejetności językowe */
#define SSO_LANG_COMMON          70
#define SSO_LANGUAGE             109

/* Umiejetności ogólne */
#define SSO_APPR_MON             100
#define SSO_APPR_OBJ             101
#define SSO_APPR_VAL             102
#define SSO_SWIM                 103
#define SSO_CLIMB                104
#define SSO_ANI_HANDL            105
#define SSO_LOC_SENSE            106
#define SSO_TRACKING             107
#define SSO_HUNTING              108
#define SSO_AWARENESS            110
#define SSO_TRADING              111
#define SSO_RIDING               112
#define SSO_MUSIC                113
#define SSO_WOODCUTTING          114
#define SSO_FISHING              115

/* The min level a trained skill decays */
// #define MIN_SKILL_LEVEL         20

/* The time between skill decays in seconds (3h) */
// #define SKILL_DECAY_INTERVAL    10800

// #include "/config/sys/ss_types2.h"

#endif  __SSO_TYPES__
