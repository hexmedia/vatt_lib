/**
 * \file /sys/pattern.h
 *
 * Plik z definicjami dla systemu wzorc�w.
 */

#include <ss_types.h>

#ifndef SYS_PATTERN_H
#define SYS_PATTERN_H

/*
 * Domy�lna trudno�� wykonania
 */

#define PATTERN_DEFAULT_DIFFICULTY 50

/*
 * Rodzaje dziedzin
 */

#define PATTERN_DOMAIN_NONE -1
#define PATTERN_DOMAIN_BLACKSMITH 0
#define PATTERN_DOMAIN_TAILOR 1
#define PATTERN_DOMAIN_CARVING 2

/*
 * Odwzorowanie dziedzin na skille
 */

#define PATTERN_SKILL_MAP ([ PATTERN_DOMAIN_BLACKSMITH : SS_CRAFTING_BLACKSMITH,\
                             PATTERN_DOMAIN_TAILOR : SS_CRAFTING_TAILOR,\
                             PATTERN_DOMAIN_CARVING : SS_CRAFTING_CARVING ])

#endif
