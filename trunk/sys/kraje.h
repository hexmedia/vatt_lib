/**
 * \file /sys/kraje.h
 *
 * Plik z definicjami identyfikatorów poszczególnych krajów.
 *
 * @author Krun
 * @date 3.10.2008
 */

#ifndef D__KRAJE__D
#define D__KRAJE__D

#define KR_REDANIA  0x1
#define KR_AEDIRN   0x2
#define KR_KAEDWEN  0x4
#define KR_TEMERIA  0x8
#define KR_CINTRA   0x10
#define KR_WMN      0x20 /* Wolne Miasto Novigrad */
#define KR_MAHAKAM  0x40
#define KR_VERDER   0x80

#endif /* D__KRAJE__D */