/**
 * \file /sys/tools.h
 *
 * Plik z definicjami dla systemu narz�dzi.
 */

#include <ss_types.h>

#ifndef SYS_TOOLS_H
#define SYS_TOOLS_H

/*
 * Domy�lna trudno�� u�ywania
 */

#define TOOL_DEFAULT_DIFFICULTY 50

/*
 * Rodzaje dziedzin
 */

#define TOOL_DOMAIN_NONE -1
#define TOOL_DOMAIN_BLACKSMITH 0
#define TOOL_DOMAIN_TAILOR 1
#define TOOL_DOMAIN_CARVING 2

/*
 * Odwzorowanie dziedzin na skille
 */

#define TOOL_SKILL_MAP ([ TOOL_DOMAIN_BLACKSMITH : SS_CRAFTING_BLACKSMITH,\
                          TOOL_DOMAIN_TAILOR : SS_CRAFTING_TAILOR,\
                          TOOL_DOMAIN_CARVING : SS_CRAFTING_CARVING ])

#endif
