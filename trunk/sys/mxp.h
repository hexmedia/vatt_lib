/**
 * Plik zawiera definicje przydatne do obs�ugi MXP przez muda
 *
 * Autor: Krun
 * Czerwiec, Lipiec 2007
 */

#define MXP_OPEN                0
#define MXP_SECURE              1
#define MXP_LOCKED              2
#define MXP_RESET               3
#define MXP_TEMP_SECURE_MODE    4
#define MXP_LOCK_OPEN_MODE      5
#define MXP_LOCK_SECURE_MODE    6
#define MXP_LOCK_LOCKED_MODE    7

#define MXP_ROOM_NAME           10
#define MXP_ROOM_DESCRIPTION    11
#define MXP_ROOM_EXITS          12
#define MXP_WELCOME_TEXT        13

#define MXP_NR_TAGS_ALLOWED_BEGIN   20
#define MXP_NR_TAGS_ALLOWED_END     99

#define MXP_LT      ""
#define MXP_GT      ""

#define MXP_AMP     ""

#define MXP_QUOTE   ""

#define MXP_T_LT    "&lt;"
#define MXP_T_GT    "&gt;"
#define MXP_T_AMP   "&amp;"
#define MXP_T_QUOTE "&quote;"

#define MXP_TAG(x)      ("/secure/mxp.c")->mxp_tag(x)
#define MXP_LINE(x)     ("/secure/mxp.c")->mxp_line(x)
#define MXP_PROCESS(x)  ("/secure/mxp.c")->process_mxp(x)