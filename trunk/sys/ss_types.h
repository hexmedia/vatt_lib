/**
 * \file /sys/ss_types.h
 *
 *  Przemapowa�em wszystkie umy, teraz s� pogrupowane w troche ja�niejszy spos�b.
 * Wymaga to przeniesienia um�w u wszystkich starych graczy, ale to za�atwi za
 * nas automat. No i oczywi�cie jak kto� u�ywa� numerk�w zamiast definicji
 * to dostanie odemnie ostr� bure. Bo kto to widzia� jakie� numerki wpiernicza�
 * po to s� definicje �eby si� ich trzyma�!
 *
 * WALKA:
 *  Numery od 0 do 9        przeznaczone s� dla stat�w.
 *  Numery od 10 do 29      przeznaczone s� dla r�nych rodzaj�w broni
 *  Numery od 30 do 49      przeznaczone s� dla styl�w walki.
 *  Numery od 50 do 69      s� zarezerwowane dla walki aczkowliek narazie wolne
 *  Numery od 70 do 100     przeznaczone s� dla wszelkiego rodzaju pozosta�ych
 *                          um�w odpowiedzialnych za walk�.
 *
 * RZEMIOS�O:
 *  Numery od 100 do 199    przeznaczone sa dla wszelkiego rodzaju czynno�ci
 *                          rzemie�lniczych takich jak drwalstwo, rybo��stwo
 *                          czy kowalstwo.
 *
 * Z�ODZIEJSTWO:    (Niby rzemios�o ale jednak troche r�ne)
 *  Numery od 200 do 249    przeznaczone s� dla umiej�tno�ci z�odziejskich.
 *
 * MAGICZNE:
 *  Numery od 250 do 299    s� prawie wolne ale przeznaczone tylko i wy��cznie
 *                          dla magii gdyby�my j� jednak kiedy� mieli wprowadzi�.
 *
 * J�ZYKOWE:
 *  Numery od 300 do 349    przeznaczone s� dla znajomo�ci przer�nych j�zyk�w
 *                          �wiata. Kiedy� mo�e pododajemy.
 *
 * OG�LNE:
 *  Numery od 350 do 500    zarezerwowane s� dla umiej�tno�ci og�lnych, czyli takich
 *                          kt�rych nie da�o si� sklasyfikowa� nigdzie indziej.
 *
 * This file defines the available stats and skills. Use this file to
 * in conjunction with query_stat() and query_skill() calls.
 *
 */

#ifndef __SS_TYPES__
#define __SS_TYPES__

#define SS_MAX                      500
#define SS_MIN                      10

//STATY

#define SS_STAT_DESC                ({ "str", "dex", "con", "int", "dis" })
#define SS_NO_STATS                 5

#define SS_STR                      0
#define SS_DEX                      1
#define SS_CON                      2
#define SS_INT                      3
#define SS_WIS                      3
#define SS_DIS                      4

//KONIEC STAT�W

//UMIEJ�TNO�CI ODPOWIEDZIALNE ZA WALK�
/* List of skills as is going to be used */
#define SS_WEP_FIRST                10   /* Index pierwszej borni */

#define SS_WEP_SWORD                SS_WEP_FIRST + 0  /* W_SWORD */
#define SS_WEP_POLEARM              SS_WEP_FIRST + 1  /* W_POLEARM */
#define SS_WEP_AXE                  SS_WEP_FIRST + 2  /* W_AXE */
#define SS_WEP_KNIFE                SS_WEP_FIRST + 3  /* W_KNIFE */
#define SS_WEP_CLUB                 SS_WEP_FIRST + 4  /* W_CLUB */
#define SS_WEP_WARHAMMER            SS_WEP_FIRST + 5  /* W_WARHAMMER */
#define SS_WEP_MISSILE              SS_WEP_FIRST + 6  /* W_MISSILE */
#define SS_WEP_BOW                  SS_WEP_FIRST + 7  /* W_L_BOW */
#define SS_WEP_CROSSBOW             SS_WEP_FIRST + 8  /* W_L_CROSSBOW and W_L_H_CROSSBOW */
#define SS_WEP_SLING                SS_WEP_FIRST + 9  /* W_L_SLING */

/*
 * Style walki znacznie wp�ywaj� na to jaki jest
 * Na forum dotycz�cym walki wyja�nione jest co i jak.
 */
#define SS_STL_FIRST                30  /* Indeks pierwszego stylu walki */
#define SS_STL_NORMALNY             SS_STL_FIRST + 0    /* FIXME: Wsumie to nie jestem pewien czy */
#define SS_STL_OFENSYWNY            SS_STL_FIRST + 1    /*        styl normalny jest w og�le */
#define SS_STL_DEFENSYWNY           SS_STL_FIRST + 2    /*        potrzebny w umach, ale w sumie */
#define SS_STL_SILNY                SS_STL_FIRST + 3    /*        mo�e on definiowa� nasz� umiej�tno�� */
#define SS_STL_SZYBKI               SS_STL_FIRST + 4    /*        walki w og�le. */
#define SS_STL_GRUPOWY              SS_STL_FIRST + 5

/* Dodatkowe umiej�tno�ci odpowiedzialne za walk�. */
#define SS_2H_COMBAT                70
#define SS_UNARM_COMBAT             71
#define SS_BLIND_COMBAT             72
#define SS_PARRY                    73
#define SS_DEFENCE                  74
#define SS_DEFENSE                  (SS_DEFENCE)
#define SS_MOUNTED_COMBAT           75
#define SS_SHIELD_PARRY             76
#define SS_THROWING                 77
#define SS_VEILING                  78

//KONIEC UMIEJ�TNO�CI ODPOWIEDZIALNYCH ZA WALK�

//UMIEJ�TNO��I RZEMIE�LNICZE:

/* Umiejetno�� kowalskie: */
#define SS_CRAFTING_TAILOR          100
#define SS_CRAFTING_BLACKSMITH      101
#define SS_CRAFTING_CARVING         102

#define SS_WOODCUTTING              103
#define SS_FISHING                  104
#define SS_TRADING                  105
#define SS_HUNTING                  106
#define SS_HERBALISM                107
#define SS_ALCHEMY                  108

//UMIEJ�TNO��I Z�ODZIEJSKIE:

#define SS_OPEN_LOCK                200
#define SS_PICK_LOCK                SS_OPEN_LOCK
#define SS_PICK_POCKET              201
#define SS_ACROBAT                  202
#define SS_FR_TRAP                  203
#define SS_SNEAK                    204
#define SS_HIDE                     205
#define SS_BACKSTAB                 206
#define SS_BREAKDOWN                207

//UMIEJ�TNO��I MAGICZNE:
//Na razie pusto, mo�e jakie� proste znaki?!

//UMIEJ�TNO�CI J�ZYKOWE:
//Mo�e wprowadzi� kilka j�zyk�w:) Oczywi�cie ka�dy zna mow� wsp�ln�, ale je�li
//kto� na wst�pie wybiera pochodzenie z nilfgardu niech m�wi tak�e nilfgardzkim
//Elfy i krasnale niech napieprzaj� starsz� mow�... Mo�e nawet jakie�
//akcenty w normalnej mowie da�:P
#define SS_LANG_COMMON          300
#define SS_LANGUAGE             301
#define SS_STARSZA_MOWA         302
#define SS_NILFGARDZKI          303
#define SS_KRASNOLUDZKI         304

//UMIEJ�TNO�CI OG�LNE
#define SS_APPR_MON             350
#define SS_APPR_OBJ             351
#define SS_APPR_VAL             352
#define SS_SWIM                 353
#define SS_CLIMB                354
#define SS_ANI_HANDL            355
#define SS_LOC_SENSE            356
#define SS_TRACKING             357
#define SS_AWARENESS            358
#define SS_RIDING               359
#define SS_MUSIC                360

/* The time between skill decays in seconds (3h) */
#define SKILL_DECAY_INTERVAL    10800

#include "/config/sys/ss_types2.h"

#endif  __SS_TYPES__

