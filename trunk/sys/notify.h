/**
 * \file /sys/notify.h
 *
 * Plik z definicjami do powiadamiacza.
 *
 * Warto�ci przeniesione z notify.c w masterze.
 *
 * @author Krun
 * @date Stycze� 2008
 */

#define NOTIFY_MESSAGES ({                                                                  \
    ({"zalogowa� si�.",         "zalogowa�a si�.",          "zalogowa�o si�."}),            \
    ({"wylogowa� si�.",         "wylogowa�a si�.",          "wylogowa�o si�."}),            \
    ({"zerwa� po��czenie.",     "zerwa�a po��czenie.",      "zerwa�o po��czenie."}),        \
    ({"odzyska� po��czenie.",   "odzyska�a po��czenie.",    "odzyska�o po��czenie."}),      \
    ({"po��czy� si� ponownie.", "po��czy�a si� ponownie",   "po��czy�o si� ponownie."})})

#define NOTIFY_ALL          (1)
#define NOTIFY_WIZARDS      (2)
#define NOTIFY_LORDS        (4)
#define NOTIFY_DOMAIN       (8)
#define NOTIFY_IP           (16)
#define NOTIFY_NO_LD        (32)
#define NOTIFY_BLOCK        (64)
#define NOTIFY_SELECTED     (128)
#define NOTIFY_NO_MULTI     (256)
#define NOTIFY_NO_REPORT    (512)

#define NOTIFY_LOGIN        (0)
#define NOTIFY_LOGOUT       (1)
#define NOTIFY_LINKDIE      (2)
#define NOTIFY_REVIVE       (3)
#define NOTIFY_SWITCH       (4)
#define NOTIFY_MULTI        (5)

#define GRAPH_PERIODS       (24)
#define GRAPH_QUEUE         (WIZ_KEEPER)
#define GRAPH_WIZARDS       (WIZ_KEEPER + 1)
#define GRAPH_ALL           (WIZ_KEEPER + 2)
#define GRAPH_SIZE          (WIZ_KEEPER + 3)
#define GRAPH_ROWS          (20)