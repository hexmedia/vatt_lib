/**
 * \file /sys/parse.h
 *
 * Plik z definicjami dla parsera XML.
 *
 * @author Krun, krun@vattghern.com.pl
 * @date 28.7.2008 15:10:16
 */

#ifndef XML_PARSER_DEFS
#define XML_PARSER_DEFS

#define XML_ATTR_NAME_ALLOWED               "a�bc�de�fghijkl�mn�o�pqrstuwxyz��1234567890"
// #define XML_ATTR_NAME_ALLOWED_S             "a�bc�de�fghijkl�mn�o�pqrstuwxyz��"

#define XML_PARSE_OK                    0
#define XML_PARSE_ERROR_DATA            1
#define XML_PARSE_ERROR_TAG             2
#define XML_PARSE_ERROR_ATTR_NAME       3
#define XML_PARSE_ERROR_ATTR_VAL        4

// POD TYM PROSZ� NIC NIE DEFINIOWA�, MO�E TO PROWADZI� DO B��D�W - KRUN
#endif