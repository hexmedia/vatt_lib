#ifndef MATERIALY_H
#define MATERIALY_H

#define MATERIALY_FILE          ("/sys/global/materialy")

// szlachetne
#define MATERIALY_SREBRO         "_materialy_sz_srebro"
#define MATERIALY_ZLOTO          "_materialy_sz_zloto"
#define MATERIALY_MITHRIL        "_materialy_sz_mithril"
#define MATERIALY_RUBIN          "_materialy_sz_rubin"
#define MATERIALY_DIAMENT        "_materialy_sz_diament"

#define MATERIALY_SZ_SREBRO      "_materialy_sz_srebro"
#define MATERIALY_SZ_ZLOTO       "_materialy_sz_zloto"
#define MATERIALY_SZ_MITHRIL     "_materialy_sz_mithril"
#define MATERIALY_SZ_RUBIN       "_materialy_sz_rubin"
#define MATERIALY_SZ_DIAMENT     "_materialy_sz_diament"
#define MATERIALY_SZ_TURKUS      "_materialy_sz_turkus"

// drewno
#define MATERIALY_DR_CIS         "_materialy_dr_cis"
#define MATERIALY_DR_BUK         "_materialy_dr_buk"
#define MATERIALY_DR_BRZOZA      "_materialy_dr_brzoza"
#define MATERIALY_DR_DAB         "_materialy_dr_dab"
#define MATERIALY_DR_SWIERK      "_materialy_dr_swierk"
#define MATERIALY_DR_SOSNA       "_materialy_dr_sosna"
#define MATERIALY_DR_TOPOLA      "_materialy_dr_topola"
#define MATERIALY_DR_WIERZBA     "_materialy_dr_wierzba"
#define MATERIALY_DR_WIAZ        "_materialy_dr_wiaz"
#define MATERIALY_DR_BERBERYS    "_materialy_dr_berberys"
#define MATERIALY_DR_LIPA        "_materialy_dr_lipa"
#define MATERIALY_DR_KLON        "_materialy_dr_klon"
#define MATERIALY_DR_JESION      "_materialy_dr_jesion"
#define MATERIALY_DR_JODLA       "_materialy_dr_jodla"
#define MATERIALY_DR_JARZEBINA   "_materialy_dr_jarzebina"
#define MATERIALY_DR_MAHON       "_materialy_dr_mahon"
#define MATERIALY_DR_HEBAN       "_materialy_dr_heban"

// rzemie�lnicze
#define MATERIALY_MARMUR         "_materialy_rz_marmur"
#define MATERIALY_GRANIT         "_materialy_rz_granit"
#define MATERIALY_ZELAZO         "_materialy_rz_zelazo"
#define MATERIALY_STAL           "_materialy_rz_stal"
#define MATERIALY_MIEDZ          "_materialy_rz_miedz"
#define MATERIALY_MOSIADZ        "_materialy_rz_mosiadz"
#define MATERIALY_SZKLO          "_materialy_rz_szklo"
#define MATERIALY_KWARC          "_materialy_rz_kwarc"
#define MATERIALY_WIKLINA        "_materialy_rz_wiklina"
#define MATERIALY_GLINA          "_materialy_rz_glina"
#define MATERIALY_SLOMA		 "_materialy_rz_sloma"

#define MATERIALY_RZ_MARMUR         "_materialy_rz_marmur"
#define MATERIALY_RZ_GRANIT         "_materialy_rz_granit"
#define MATERIALY_RZ_ZELAZO         "_materialy_rz_zelazo"
#define MATERIALY_RZ_STAL           "_materialy_rz_stal"
#define MATERIALY_RZ_MIEDZ          "_materialy_rz_miedz"
#define MATERIALY_RZ_MOSIADZ        "_materialy_rz_mosiadz"
#define MATERIALY_RZ_SZKLO          "_materialy_rz_szklo"
#define MATERIALY_RZ_KWARC          "_materialy_rz_kwarc"
#define MATERIALY_RZ_WIKLINA        "_materialy_rz_wiklina"
#define MATERIALY_RZ_GLINA          "_materialy_rz_glina"
#define MATERIALY_RZ_SLOMA	    "_materialy_rz_sloma"


// tkaniny
#define MATERIALY_JEDWAB         "_materialy_tk_jedwab"
#define MATERIALY_WELNA          "_materialy_tk_welna"
#define MATERIALY_BAWELNA        "_materialy_tk_bawelna"
#define MATERIALY_ATLAS          "_materialy_tk_atlas"
#define MATERIALY_AKSAMIT        "_materialy_tk_aksamit"
#define MATERIALY_WELUR          "_materialy_tk_welur"
#define MATERIALY_TIUL           "_materialy_tk_tiul"
#define MATERIALY_SZYFON         "_materialy_tk_szyfon"
#define MATERIALY_TK_PIKOWA      "_materialy_tk_tk_pikowa"
#define MATERIALY_JUTA           "_materialy_tk_juta"
#define MATERIALY_LEN            "_materialy_tk_len"
#define MATERIALY_SATYNA         "_materialy_tk_satyna"
#define MATERIALY_KREPA          "_materialy_tk_krepa"
#define MATERIALY_MULINA         "_materialy_tk_mulina"
#define MATERIALY_KORDONEK       "_materialy_tk_kordonek"

#define MATERIALY_TK_JEDWAB         "_materialy_tk_jedwab"
#define MATERIALY_TK_WELNA          "_materialy_tk_welna"
#define MATERIALY_TK_BAWELNA        "_materialy_tk_bawelna"
#define MATERIALY_TK_ATLAS          "_materialy_tk_atlas"
#define MATERIALY_TK_AKSAMIT        "_materialy_tk_aksamit"
#define MATERIALY_TK_WELUR          "_materialy_tk_welur"
#define MATERIALY_TK_TIUL           "_materialy_tk_tiul"
#define MATERIALY_TK_SZYFON         "_materialy_tk_szyfon"
#define MATERIALY_TK_TK_PIKOWA      "_materialy_tk_tk_pikowa"
#define MATERIALY_TK_JUTA           "_materialy_tk_juta"
#define MATERIALY_TK_LEN            "_materialy_tk_len"
#define MATERIALY_TK_SATYNA         "_materialy_tk_satyna"
#define MATERIALY_TK_KREPA          "_materialy_tk_krepa"
#define MATERIALY_TK_MULINA         "_materialy_tk_mulina"
#define MATERIALY_TK_KORDONEK       "_materialy_tk_kordonek"

// sk�ry
#define MATERIALY_SK_BYDLE          "_materialy_sk_bydle"
#define MATERIALY_SK_CIELE          "_materialy_sk_ciele"
#define MATERIALY_SK_KROLIK         "_materialy_sk_krolik"
#define MATERIALY_SK_WILK           "_materialy_sk_wilk"
#define MATERIALY_SK_NIEDZWIEDZ     "_materialy_sk_niedzwiedz"
#define MATERIALY_SK_SWINIA         "_materialy_sk_swinia"

// inne
#define MATERIALY_IN_ROSLINA      "_materialy_in_roslina"
#define MATERIALY_ROSLINA         MATERIALY_IN_ROSLINA
#define MATERIALY_IN_PAPIER       "_materialy_in_papier"
#define MATERIALY_PAPIER          MATERIALY_IN_PAPIER

// makra
#define NAZWA_MATERIALU(m, p)       (MATERIALY_FILE->nazwa_materialu(m, p))
#define GESTOSC_MATERIALU(m)        (MATERIALY_FILE->gestosc_materialu(m))
#define WYTRZYMALOSC_MATERIALU(m)   (MATERIALY_FILE->wytrzymalosc_materialu(m))
#define PALNOSC_MATERIALU(m)        (MATERIALY_FILE->latwopalnosc_materialu(m))
#define KRUCHOSC_MATERIALU(m)       (MATERIALY_FILE->kruchosc_materialu(m))

/*** nazwy ***/

#define MATERIALY_NAZWY ([                                          \
                    MATERIALY_SREBRO:       "srebro",               \
                    MATERIALY_ZLOTO:        "z�oto",                \
                    MATERIALY_MITHRIL:      "mithril",              \
                    MATERIALY_RUBIN:        "rubin",                \
                    MATERIALY_DIAMENT:      "diament",              \
                    MATERIALY_SZ_TURKUS:    "turkus",               \
                    MATERIALY_DR_CIS:       "cisowe drewno",        \
                    MATERIALY_DR_BUK:       "bukowe drewno",        \
                    MATERIALY_DR_BRZOZA:    "brzozowe drewno",      \
                    MATERIALY_DR_DAB:       "d�bowe drewno",        \
                    MATERIALY_DR_SWIERK:    "�wierkowe drewno",     \
                    MATERIALY_DR_SOSNA:     "sosnowe drewno",       \
                    MATERIALY_DR_TOPOLA:    "topolowe drewno",      \
                    MATERIALY_DR_WIERZBA:   "wierzbowe drewno",     \
                    MATERIALY_DR_WIAZ:      "wi�zowe drewno",       \
                    MATERIALY_DR_BERBERYS:  "berberysowe drewno",   \
                    MATERIALY_DR_LIPA:      "lipowe drewno",        \
                    MATERIALY_DR_KLON:      "klonowe drewno",       \
                    MATERIALY_DR_JESION:    "jesionowe drewno",     \
                    MATERIALY_DR_JODLA:     "jod�owe drewno",       \
                    MATERIALY_DR_JARZEBINA: "jarz�binowe drewno",   \
                                                                    \
                    MATERIALY_MARMUR:       "marmur",               \
                    MATERIALY_GRANIT:       "granit",               \
                    MATERIALY_ZELAZO:       "�elazo",               \
                    MATERIALY_STAL:         "stal",                 \
                    MATERIALY_MIEDZ:        "mied�",                \
                    MATERIALY_MOSIADZ:      "mosi�dz",              \
                    MATERIALY_SZKLO:        "szk�o",                \
                    MATERIALY_KWARC:        "kwarc",                \
                    MATERIALY_WIKLINA:      "wiklina",              \
                    MATERIALY_GLINA:        "glina",                \
                                                                    \
                    MATERIALY_JEDWAB:       "jedwab",               \
                    MATERIALY_WELNA:        "we�na",                \
                    MATERIALY_BAWELNA:      "bawe�na",              \
                    MATERIALY_ATLAS:        "atlas",                \
                    MATERIALY_AKSAMIT:      "aksamit",              \
                    MATERIALY_WELUR:        "welur",                \
                    MATERIALY_TIUL:         "tiul",                 \
                    MATERIALY_SZYFON:       "szyfon",               \
                    MATERIALY_TK_PIKOWA:    "tkanina pikowa",       \
                    MATERIALY_JUTA:         "juta",                 \
                    MATERIALY_LEN:          "len",                  \
                    MATERIALY_SATYNA:       "satyna",               \
                    MATERIALY_KREPA:        "krepa",                \
                    MATERIALY_MULINA:       "mulina",               \
                    MATERIALY_KORDONEK:     "kordonek",             \
                    MATERIALY_SK_BYDLE:     "bydl�ca sk�ra",        \
                    MATERIALY_SK_CIELE:     "ciel�ca sk�ra",        \
                    MATERIALY_SK_KROLIK:    "kr�licza sk�ra",       \
                    MATERIALY_SK_WILK:      "wilcza sk�ra",         \
                    MATERIALY_SK_NIEDZWIEDZ:"nied�wiedzia sk�ra",   \
                    MATERIALY_SK_SWINIA:    "�wi�ska sk�ra",        \
                    MATERIALY_IN_ROSLINA:   "ro�lina"])

#endif
