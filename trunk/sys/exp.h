/**
 * ======================================================
 * NIE MODYFIKOWA� BEZ KONSULTACJI Z keeperem!!!!!!!!!!!!!!!!
 * ======================================================
 *
 * \file /sys/exp.c
 *
 * plik przechowuje warto�ci expa przyznawane
 * za wszelkie czynno�ci i przy innych okazjach.
 *
 * ostatnie s�owo w nazwie define'a m�wi o tym
 * kt�remu skillowi/statowi przyznajemy expa.
 *
 * Edycje bez konsultacji z keeperem b�d� surowo karane!
 *
 */

#ifndef EXP_VALUES
#define EXP_VALUES

#define EXP_PRAKTYCZNY                              0
#define EXP_TEORETYCZNY                             1

/* Co ile minut odpalamy alarm zmniejszaj�cego rutynk� */
#define EXP_ROUTINE_MOD_FREQ                        (10+random(4))

/* Na jaki okres czasu jest przyznany limit (w minutach)*/
#define EXP_ROUTINE_LIMIT_PERIOD                    120

/* Limit expienia w minutach */
#define EXP_ROUTINE_LIMIT                           80

/* Czas progowy w sekundach, p�ki co identyczny
 * dla wszystkich skilli */
#define EXP_ROUTINE_INTERVAL                        150

/* Ile razy gracz musi przekroczy� limit, �eby dostawa�
 * 1 punkcik za czynno�� */
/* [W�a�ciwie ta w�a�ciwo�� chyba nie jest konieczna i
 * w przysz�o�ci mo�na j� wywali� o_O]
 */
#define EXP_ROUTINE_LIMIT_BREAK                     1.0


/*  ============================
 *                do�wiadczenie og�lne
 *   ============================ */

/* za ka�dy odebrany punkt zm�czenia dostaje si� tyle.
    UWAGA: koniecznie floaty!!! */
#define EXP_ZA_PUNK_ZMECZENIA_STR                   0.1
#define EXP_ZA_PUNK_ZMECZENIA_CON                   0.2


/*  ============================
 *                   przy rzucaniu
 *   ============================ */

/* exp dla rzucajacacego */
#define EXP_RZUCANIE_PRZY_SPUDLOWANIU_DEX           2
#define EXP_RZUCANIE_PRZY_SPUDLOWANIU_THROWING      2

#define EXP_RZUCANIE_PRZY_UNIKU_PRZECIWNIKA_DEX     2
#define EXP_RZUCANIE_PRZY_UNIKU_PRZECIWNIKA_THROWING    2

#define EXP_RZUCANIE_TRAFILISMY_DEX                 7
#define EXP_RZUCANIE_TRAFILISMY_THROWING            8

#define EXP_RZUCANIE_NIEBRONIA_TRAFILISMY_DEX       1
#define EXP_RZUCANIE_NIEBRONIA_TRAFILISMY_THROWING  2

/* exp dla tego w kogo rzucamy*/
#define EXP_RZUCANIE_UNIK_PRZED_POCISKIEM_DEX       9
#define EXP_RZUCANIE_UNIK_PRZED_POCISKIEM_DEFENSE   8
#define EXP_RZUCANIE_UNIK_PRZED_POCISKIEM_AWARENESS 7

#define EXP_RZUCANIE_UNIK_PRZED_NIEBRONIA_DEX       1
#define EXP_RZUCANIE_UNIK_PRZED_NIEBRONIA_DEFENSE   2
#define EXP_RZUCANIE_UNIK_PRZED_NIEBRONIA_AWARENESS 1

#define EXP_RZUCANIE_DOSTALEM_AWARENESS             1


/*  ============================
 *                  przy strzelaniu
 *   ============================ */

/* exp dla strzelaj�cego */
#define EXP_STRZELANIE_KRYT_PUDLO_MISSILE           1
#define EXP_STRZELANIE_PUDLO_MISSILE                2
#define EXP_STRZELANIE_TRAFILEM_MISSILE            10
#define EXP_STRZELANIE_TRAFILEM_KRYT_MISSILE       16


/*  ============================
 *                   w czasie walki
 *   ============================ */

/* przy trafieniu, zale�nie od tego czym walczymy. Za ka�dy cios dostajemy to razy whit */
#define EXP_WALKA_TRAFILISMY_SKILLBRONI             0.07
#define EXP_WALKA_TRAFILISMY_2H_COMBAT              7
#define EXP_WALKA_TRAFILISMY_WALKAWRECZ             0.07
#define EXP_WALKA_TRAFILISMY_DIS                    0.09
#define EXP_WALKA_ZABILISMY_DIS                     150
#define EXP_WALKA_OGLUSZYLISMY_DIS                  100

/* gdy przeciwnik sparuje nasz cios dostaje du�o r�nych */
#define EXP_WALKA_SPAROWALEM_STR                    7
#define EXP_WALKA_SPAROWALEM_DEX                    5
#define EXP_WALKA_SPAROWALEM_CON                    2
#define EXP_WALKA_SPAROWALEM_PARRY                  10
#define EXP_WALKA_SPAROWALEM_AWARENESS              2

/* gdy przeciwnik uniknie naszego ciosu dostaje du�o r�nych */
#define EXP_WALKA_UNIKNALEM_STR                     4
#define EXP_WALKA_UNIKNALEM_DEX                     8
#define EXP_WALKA_UNIKNALEM_CON                     1
#define EXP_WALKA_UNIKNALEM_DEFENSE                 10
#define EXP_WALKA_UNIKNALEM_AWARENESS               5

/* przy ka�dym ciosie wyprowadzanym przez nas czy przez wroga w ciemnosci */
#define EXP_WALKA_W_CIEMNOSCI_ZA_CIOS_BLINDCMBT     7


/*  ============================
 *            przy u�ywaniu r�nych skilli
 *   ============================ */

/* handel - tyle expa dostajemy do skilla za kazde 20 groszy obrotu */
#define EXP_HANDEL_SPRZEDANIE_ZA_20_GROSZY          1
#define EXP_HANDEL_KUPIENIE_ZA_20_GROSZY            1

/* wspinaczka - za wspinanie si� ;) */
#define EXP_WSPINAM_NIEUDANY_CLIMB                  3
#define EXP_WSPINAM_NIEUDANY_CLIMB_STR              3
#define EXP_WSPINAM_NIEUDANY_CLIMB_DEX              2
#define EXP_WSPINAM_NIEUDANY_CLIMB_CON              1

#define EXP_WSPINAM_PRAWIE_UDANY_CLIMB              7
#define EXP_WSPINAM_PRAWIE_UDANY_CLIMB_STR          7
#define EXP_WSPINAM_PRAWIE_UDANY_CLIMB_DEX          6
#define EXP_WSPINAM_PRAWIE_UDANY_CLIMB_CON          8

#define EXP_WSPINAM_UDANY_CLIMB                     4
#define EXP_WSPINAM_UDANY_CLIMB_STR                 5
#define EXP_WSPINAM_UDANY_CLIMB_DEX                 4
#define EXP_WSPINAM_UDANY_CLIMB_CON                 5

/* szukanie */
#define EXP_SZUKANIE_NIEUDANE_AWARENESS             1
#define EXP_SZUKANIE_UDANE_AWARENESS                7

/* szukanie zi� */
#define EXP_SZUKANIE_ZIOLA_UDANE_HERBALISM          4
#define EXP_SZUKANIE_ZIOLA_NIEUDANE_HERBALISM       1
#define EXP_SZUKANIE_ZIOLA_UDANE_WIS                2
#define EXP_SZUKANIE_ZIOLA_NIEUDANE_WIS             1
#define EXP_SZUKANIE_ZIOLA_UDANE_INT                2
#define EXP_SZUKANIE_ZIOLA_NIEUDANE_INT             1
#define EXP_SZUKANIE_ZIOL_UDANE_HERBALISM           3
#define EXP_SZUKANIE_ZIOL_NIEUDANE_HERBALISM        1
#define EXP_SZUKANIE_ZIOL_UDANE_WIS                 3
#define EXP_SZUKANIE_ZIOL_NIEUDANE_WIS              1
#define EXP_SZUKANIE_ZIOL_UDANE_INT                 1
#define EXP_SZUKANIE_ZIOL_NIEUDANE_INT              1

/* tropienie */
#define EXP_TROPIENIE_NIEUDANY_TRACKING             2
#define EXP_TROPIENIE_NIEUDANY_TRACKING_WIS         2
#define EXP_TROPIENIE_PRAWIE_UDANY_TRACKING         6
#define EXP_TROPIENIE_PRAWIE_UDANY_TRACKING_WIS     5
#define EXP_TROPIENIE_UDANY_TRACKING                12
#define EXP_TROPIENIE_UDANY_TRACKING_WIS            10

/* scinanie drzew - odpowiednio za najmniejsze
 * i najwieksze drzewo
 */
#define EXP_SCINANIE_DRZEW_STR_MIN                  4
#define EXP_SCINANIE_DRZEW_WOODCUTTING_MIN          4
#define EXP_SCINANIE_DRZEW_STR_MAX                  18
#define EXP_SCINANIE_DRZEW_WOODCUTTING_MAX          21
#define EXP_SCINANIE_DRZEW_PRZECINANIE_STR          4
#define EXP_SCINANIE_DRZEW_PRZECINANIE_WOODCUTTING  4

/* w�dkarstwo */
#define EXP_WEDKARSTWO_PRZYWIAZYWANIE_DEX           2
#define EXP_WEDKARSTWO_PRZYWIAZYWANIE_FISHING       2
#define EXP_WEDKARSTWO_ZLOWIONA_DEX                 8
#define EXP_WEDKARSTWO_ZLOWIONA_FISHING             10
#define EXP_WEDKARSTWO_ZERWANA_DEX                  3
#define EXP_WEDKARSTWO_ZERWANA_FISHING              5

/* �owiectwo */
#define EXP_LOWIECTWO_UDERZENIE                     6
#define EXP_LOWIECTWO_CELOWANIE                     4

/* �piewanie */
// Odpowiednio wzorem d�ugo�� �piewanego tekstu *
// poni�sza warto��.
#define EXP_SPIEWANIE_MUSIC                         0.3

/* strzelania z �uku */
//Te warto�ci s� modyfikowane przez funkcje F_BS_EXP w formulas.h
#define EXP_STRZELANIE_UDANE_TLWS                   20
#define EXP_STRZELANIE_UDANE_LWS                    20
#define EXP_STRZELANIE_UDANE_DEX                    20
#define EXP_STRZELANIE_UDANE_STR                    20

#define EXP_STRZELANIE_NIEUDANE_TLWS                10
#define EXP_STRZELANIE_NIEUDANE_LWS                 10
#define EXP_STRZELANIE_NIEUDANE_DEX                 10
#define EXP_STRZELANIE_NIEUDANE_STR                 10

#define EXP_STRZELANIE_CELUNIK_TLWS                 15
#define EXP_STRZELANIE_CELUNIK_LWS                  15
#define EXP_STRZELANIE_CELUNIK_DEX                  15
#define EXP_STRZELANIE_CELUNIK_STR                  15

//A te ju� nie
#define EXP_STRZELANIE_UNIKNALEM_DEX                50
#define EXP_STRZELANIE_UNIKNALEM_DEFENSE            70

#define EXP_STRZELANIE_OSLONILEM_STR                2
#define EXP_STRZELANIE_OSLONILEM_SHIELD_PARRY       20

#define EXP_STRZELANIE_SPAROWALEM_DEX               100
#define EXP_STRZELANIE_SPAROWALEM_PARRY             150

/* z�odziejstwo */
//otwieranie zamk�w
#define EXP_WYLAMYWANIE_UDANE_BREAKDOWN             20
#define EXP_WYLAMYWANIE_UDANE_STR                   20
#define EXP_WYLAMYWANIE_UDANE_INT                   5
#define EXP_WYLAMYWANIE_UDANE_DEX                   10

#define EXP_WYLAMYWANIE_NIEUDANE_BREAKDOWN          5
#define EXP_WYLAMYWANIE_NIEUDANE_STR                4
#define EXP_WYLAMYWANIE_NIEUDANE_INT                1
#define EXP_WYLAMYWANIE_NIEUDANE_DEX                4

#define EXP_OTW_WYTRYCHEM_UDANE_PICKLOCK            20
#define EXP_OTW_WYTRYCHEM_UDANE_DEX                 4
#define EXP_OTW_WYTRYCHEM_UDANE_INT                 20

#define EXP_OTW_WYTRYCHEM_NIEUDANE_PICKLOCK         5
#define EXP_OTW_WYTRYCHEM_NIEUDANE_DEX              3
#define EXP_OTW_WYTRYCHEM_NIEUDANE_INT              5

/*  ============================
 *                  przy z�odziejaszkowaniu ;p
 *   ============================ */

/* exp dla podgl�dacza */
#define EXP_PODGLADANIE_UDANE_DEX                   10
#define EXP_PODGLADANIE_UDANE_INT                   7
#define EXP_PODGLADANIE_UDANE_PICKPOCKET            2
#define EXP_PODGLADANIE_NIEUDANE_DEX                3
#define EXP_PODGLADANIE_NIEUDANE_INT                2
/*exp dla podgl�dywanego, przydzielane
    oczywiscie tylko, jak sie zorientujemy*/
#define EXP_PODGLADANY_INT                          5
#define EXP_PODGLADANY_AWARENESS                    6
/* exp dla reszty graczy na lok, ktorzy to zobacz� */
#define EXP_PODGLADANIE_AWARENESS                    2
/*-------------------------------
 exp dla z�odzieja za kradzie�, te wszystkie definy sa jeszcze
 kapke modyfikowane w /cmd/live/thief.c. Definiujemy tu max. wartosc */
#define EXP_KRADNE_UDANE_DEX                        17
#define EXP_KRADNE_UDANE_PICKPOCKET                 20
/* exp dla ofiary z�odzieja, za zorientowanie si� */
#define EXP_KRADZIONY_WIDZE_AWARENESS               9
#define EXP_KRADZIONY_WIDZE_INT                     6
/* exp dla ofiary z�odzieja, za zorientowanie si�,
    niestety, ju� po fakcie... :) */
#define EXP_OKRADZIONY_WIDZE_AWARENESS              5
#define EXP_OKRADZIONY_WIDZE_INT                    3
/* exp dla widz�w */
#define EXP_KRADZIEZ_AWARENESS                      2
#define EXP_KRADZIEZ_INT                            2

/*  ============================
 *                  przy ukrywaniu czego� lub siebie
 *      warto�ci te s� modyfikowane w cmd_sec
 *   ============================ */
#define EXP_UKRYWAM_SIE_UDANE_DEX                   2
#define EXP_UKRYWAM_SIE_UDANE_HIDE                  3
#define EXP_UKRYWAM_SIE_NIEUDANE_HIDE               1

/* skradanie si� */
#define EXP_SKRADAM_SIE_UDANY_SNEAK                 (1 + random(3))
#define EXP_SKRADAM_SIE_UDANY_DEX                   (1 + random(3))
#define EXP_SKRADAM_SIE_NIEUDANY_SNEAK              1



/* p�ywanie wp�aw, nurkowanie oraz wios�owanie
 */
#define EXP_PLYNE_WPLAW_SWIM                        3
#define EXP_PLYNE_WPLAW_STR                         2
#define EXP_PLYNE_WPLAW_DEX                         3

#define EXP_NURKUJE_SWIM                            5
#define EXP_NURKUJE_CON                             3
#define EXP_NURKUJE_DEX                             5

#define EXP_WIOSLUJE_STR                            3

/* ===========================
 *     QUESTY                        OSTRO�NIE!!!
 * ========================== */

/* sygnet dla raghola */
#define EXP_QUEST_SYGNET_DLA_RAGHOLA_WOODCUTING     3000
/*list od krawca z piany, ten defin to podstawa, kt�ra
    jest jeszcze mno�ona przez ilo�� zaliczonych checkpoint�w*/
#define EXP_QUEST_UTOPCE_INT                        1000
/*que�cik u ciumka�y - wo�nicy z dylka rinde-bm-piana*/
#define EXP_QUEST_CIUMKALA_INT                      1400
/*quest od zielarza z bagien piany
    W przypadku wzorowo wykonanego questa nagroda to
    max: 3*EXP_QUEST_ENADEL */
#define EXP_QUEST_ENADEL 2200

#endif EXP_VALUES
