#ifndef PL_DEFS
#define PL_DEFS

/* Przypadki */
#define PL_MIA  0
#define PL_DOP  1
#define PL_CEL  2
#define PL_BIE  3
#define PL_NAR  4
#define PL_MIE  5

/* Rodzaje gramatyczne */

#define PL_MESKI_OS             0
#define PL_MESKI_NOS_ZYW        1
#define PL_MESKI_NOS_NZYW       2
#define PL_MESKI_NZYW           2

#define PL_ZENSKI               3

#define PL_NIJAKI_OS            4
#define PL_NIJAKI_NOS           5

#define QRODZAJ(ob)     (ob->query_rodzaj())

/* Makra ulatwiajace ustawienie odmian */

/* Dla graczy (choc nie tylko) */
#define PL_CZLOWIEK     ({"cz^lowiek", "cz^lowieka", "cz^lowiekowi",           \
                          "cz^lowieka", "cz^lowiekiem", "cz^lowieku"}),        \
                        ({"ludzie", "ludzi", "ludziom", "ludzi", "lud^xmi",  \
                          "ludziach"}), PL_MESKI_OS, 1
#define PL_MEZCZYZNA    ({"m^e^zczyzna", "m^e^zczyzny", "m^e^zczy^xnie",           \
                          "m^e^zczyzn^e", "m^e^zczyzn^a", "m^e^zczy^xnie"}),         \
                        ({"m^e^zczy^xni", "m^e^zczyzn", "m^e^zczyznom",            \
                          "m^e^zczyzn", "m^e^zczyznami", "m^e^zczyznach"}),       \
                        PL_MESKI_OS
#define PL_KOBIETA      ({"kobieta", "kobiety", "kobiecie", "kobiet^e",      \
                          "kobiet^a", "kobiecie"}),                          \
                        ({"kobiety", "kobiet", "kobietom", "kobiety",       \
                          "kobietami", "kobietach"}), PL_ZENSKI
#define PL_ELF          ({"elf", "elfa", "elfowi", "elfa", "elfem",         \
                          "elfie"}),                                        \
                        ({"elfy", "elf^ow", "elfom", "elfy", "elfami",       \
                          "elfach"}), PL_MESKI_NOS_ZYW
#define PL_ELFKA        ({"elfka", "elfki", "elfce", "elfk^e", "elfk^a",      \
                          "elfce"}),                                        \
                        ({"elfki", "elfek", "elfkom", "elfki", "elfkami",   \
                          "elfkach"}), PL_ZENSKI
#define PL_POLELF       ({"p^o^lelf", "p^o^lelfa", "p^o^lelfowi", "p^o^lelfa",\
			"p^o^lelfem", "p^o^lelfie"}), ({"p^o^lelfy", "p^o^lelf^ow",\
			"p^o^lelfom", "p^o^lelfy", "p^o^lelfami", "p^o^lelfach"}),\
			PL_MESKI_NOS_ZYW
#define PL_POLELFKA     ({"p^o^lelfka", "p^o^lelfki", "p^o^lelfce", "p^o^lelfk^e",\
			"p^o^lelfk^a", "p^o^lelfce"}), ({"p^o^lelfki", "p^o^lelfek",\
			"p^o^lelfkom", "p^o^lelfki", "p^o^lelfkami",\
			"p^o^lelfkach"}), PL_ZENSKI
#define PL_KRASNOLUD    ({"krasnolud", "krasnoluda", "krasnoludowi",        \
                          "krasnoluda", "krasnoludem", "krasnoludzie"}),    \
                        ({"krasnoludy", "krasnolud^ow", "krasnoludom",       \
                          "krasnoludy", "krasnoludami", "krasnoludach"}),   \
                        PL_MESKI_NOS_ZYW
#define PL_KRASNOLUDKA  ({"krasnoludka", "krasnoludki", "krasnoludce",      \
                          "krasnoludk^e", "krasnoludk^a", "krasnoludce"}),    \
                        ({"krasnoludki", "krasnoludek", "krasnoludkom",     \
                          "krasnoludki", "krasnoludkami",                   \
                          "krasnoludkach"}), PL_ZENSKI
#define PL_NIZIOLEK       ({"nizio^lek", "nizio^lka", "nizio^lkowi", "nizio^lka", \
                          "nizio^lkiem", "nizio^lku"}),                         \
                        ({"nizio^lki", "nizio^lk^ow", "nizio^lkom", "nizio^lk^ow",    \
                          "nizio^lkami", "nizio^lkach"}), PL_MESKI_OS
#define PL_NIZIOLKA     ({"nizio^lka", "nizio^lkki", "nizio^lce", "nizio^lk^e",   \
                          "nizio^lk^a", "nizio^lce"}),                         \
                        ({"nizio^lki", "nizio^lek", "nizio^lkom", "nizio^lki",  \
                          "nizio^lkami", "nizio^lkach"}), PL_ZENSKI
#define PL_GNOM         ({"gnom", "gnoma", "gnomowi", "gnoma", "gnomem",    \
                          "gnomie"}),                                       \
                        ({"gnomy", "gnom^ow", "gnomom", "gnomy", "gnomami",  \
                          "gnomach"}), PL_MESKI_NOS_ZYW
#define PL_GNOMKA       ({"gnomka", "gnomki", "gnomce", "gnomk^e", "gnomk^a", \
                          "gnomce"}),                                       \
                        ({"gnomki", "gnomek", "gnomkom", "gnomki",          \
                          "gnomkami", "gnomkach"}), PL_ZENSKI
#define PL_GOBLIN       ({"goblin", "goblina", "goblinowi", "goblina",      \
                          "goblinem", "goblinie"}),                         \
                        ({"gobliny", "goblin^ow", "goblinom", "gobliny",     \
                          "goblinami", "goblinach"}), PL_MESKI_NOS_ZYW
#define PL_GOBLINKA     ({"goblinka", "goblinki", "goblince", "goblink^e",   \
                          "goblink^a", "goblince"}),                         \
                        ({"goblinki", "goblinek", "goblinkom", "goblinki",  \
                          "goblinkami", "goblinkach"}), PL_ZENSKI

/* Inne */

#define PL_ORK          ({"ork", "orka", "orkowi", "orka", "orkiem",        \
                          "orku"}),                                         \
                        ({"orki", "ork^ow", "orkom", "orki", "orkami",       \
                          "orkach"}), PL_MESKI_NOS_ZYW
#define PL_ORCZYCA      ({"orczyca", "orczycy", "orczycy", "orczyc^e",       \
                          "orczyc^a", "orczycy"}),                           \
                        ({"orczyce", "orczyc", "orczycom", "orczyce",       \
                          "orczycami", "orczycach"}), PL_ZENSKI
#define PL_POLORK       ({"p^o^lork", "p^o^lorka", "p^o^lorkowi", "p^o^lorka",\
			"p^o^lorkiem", "p^o^lorku"}), ({"p^o^lorki", "p^o^lork^ow",\
			"p^o^lorkom", "p^o^lorki", "p^o^lorkami", "p^o^lorkach"}),\
			PL_MESKI_NOS_ZYW
#define PL_POLORCZYCA   ({"p^o^lorczyca", "p^o^lorczycy", "p^o^lorczycy",\
			"p^o^lorczyc^e", "p^o^lorczyc^a", "p^o^lorczycy"}),\
			({"p^o^lorczyce", "p^o^lorczyc", "p^o^lorczycom",\
			"p^o^lorczyce", "p^o^lorczycami", "p^o^lorczycach"}),\
			PL_ZENSKI

#endif PL_DEFS
