/*
 * /cmd/wiz/possob.c
 *
 * This object is the object that controls the release mechanism during
 * possessions as well as the possessed player in case of player possessions.
 */

#pragma no_inherit
#pragma save_binary
#pragma strict_types

#include <macros.h>
#include <std.h>
#include <stdproperties.h>

inherit "/std/object";

/*
 * Global variables.
 */
static object demon;
static object possessed;
static int    is_player;

/*
 * Prototypes.
 */
static varargs int release(string str);
static int think(string arg);
static void check_env();

/*
 * Function name: create_object
 * Description:	  Initialize the object
 */
public void
create_object()
{
    set_name("possob");
    add_name("player");

    set_long("Jest to obiekt s�u��cy do possessowania. Jednak�e nie powiniene� "+
        "go widzie�. To, �e go widzisz oznacza, i� wyst�pi� b��d, i nale�y o nim "+
        "jak najpr�dzej powiadomi� odpowiednie osoby komend� - zg�o� b��d.\n");
    set_no_show();
    is_player = 0;
}

/*
 * Function name: init
 * Description  : Initialize some commands.
 */
public void
init()
{
    ::init();

    add_action(release, "quit");
    add_action(think, "pomy�l");
}

/*
 * Function name: release
 * Description  : Stop possessing.
 * Arguments    : string str - the command line argument.
 * Returns      : int 1/0 - success/failure.
 */
static int
release(string str)
{
    if (strlen(str))
    {
    	notify_fail("Quit what?\n");
	return 0;
    }

    /* Get rid of the snoop. */
    if (is_player)
    {
	snoop(this_object());
    }

    /* Move the demon back into his own body. */
    exec(demon, possessed);

    /* If the possessed object is a player, move him back. */
    if (is_player)
    {
	exec(possessed, this_object());
    }

    tell_object(possessed, "Czujesz, �e obca wola opuszcza tw�j umys� i " +
	"odzyskujesz kontrol� nad swym cia�em.\n");
    tell_object(demon, "Opuszczasz umys� " + possessed->query_Imie(demon, PL_BIE) + ", " +
	"kt�ry odzyskuje kontrol� nad w�asnym cia�em.\n");
    possessed->set_possessed("");
    remove_object();

    return 1;
}

/*
 * Function name: possess
 * Description  : Set up the possession of a player or living complete with
 *                initialization of the possess object and messages to both
 *                parties.
 * Arguments    : object ob1 - the demon.
 *                object ob2 - the possessed.
 * Returns      : int 1/0 - true if successful.
 */
public int
possess(object ob1, object ob2)
{
    if (obj_no_change)
    {
	return 0;
    }

    /* This function may only be called from the command soul of normal
     * wizards.
     */
    if (previous_object() != find_object(WIZ_CMD_NORMAL))
    {
	return 0;
    }

    demon = ob1;
    possessed = ob2;

    /* If the possessed object is a player, move him here. */
    if (interactive(possessed))
    {
	is_player = 1;
	exec(this_object(), possessed);
    }

    exec(possessed, demon);
    possessed->set_possessed(demon->query_real_name());

    tell_object(this_object(), "Czujesz, �e twoja dusza zostaje " +
    	"obezw�adniona i zepchni�ta w najbardziej mroczne zakamarki "+
    	"twego m�zgu. Jaka� pot�na, obca si�a woli przejmuje kontrol� "+
    	"nad twym cia�em. Teraz juz mo�esz tylko patrze� i czeka�, a� "+
    	"nadejdzie kres tej beznadziejnej tortury. Wydaje Ci si�, �e "+
    	"jeste� w stanie wy�acznie 'pomy�le�' jak�� nieweso�� my�l.\n");
    tell_object(possessed, "Przejmujesz kontrol� nad " + 
        possessed->query_Imie(demon, PL_NAR) + ". Zawsze mo�esz przesta� "+
	"go kontrolowa� wpisuj�c po prostu 'quit'.\n");

    /* Give the possessed the ability to 'think'. It parses all commands
     * executed by the possessed to the function think in order to generate
     * proper fail message when the player wants to do anything else.
     */
    enable_commands();
    add_action(think, "", 1);

    /* Start a periodic environment checker. */
    set_alarm(60.0, 60.0, check_env);

    return 1;
}

/*
 * Function name: check_env
 * Description  : Periodically check that the object that is possessed
 *                hasn't been destructed or the possession object moved.
 *                If this happens, stop possessing.
 */
static void
check_env()
{
    /*
     * In this case the possessed body has been destroyed which means
     * that the demon has been thrown out as well. Better remove his
     * body as well in that case.
     */
    if (!possessed && demon)
	demon->remove_object();

    /*
     * This object has been moved out of the possessed body. 
     * Stop possessing.
     */
    if (environment(this_object()) != possessed)
	release(possessed->query_real_name());
}

/*
 * Function name: think
 * Description  : Allows the sadly possessed to think, the only way to
 *                communicated with the demon in charge of his body.
 * Arguments    : string arg - the thought.
 */
public int
think(string arg)
{
    /* The player can only think, every other command will return in a
     * proper fail message.
     */
    if (query_verb() == "pomysl")
    {
	if (!strlen(arg))
	{
	    tell_object(possessed, "Cogito ergo sum.\n");
	}
	else
	{
	    tell_object(possessed, "My�lisz sobie: " + arg + "\n");
	}

	return 1;
    }

    notify_fail("Nie jeste� w stanie tego zrobi�, gdy� twe cia�o "+
        "jest przej�te przez jakiego� pot�nego czarodzieja. Jeste� "+
        "w stanie tylko 'pomy�le�' sobie jak�� smutn� my�l.\n");
    return 0;
}

/*
 * Function name: catch_tell
 * Description  : This function is called when a message is printed to
 *                the poor soul crammed into this object. It prints the
 *                message to the connected socket.
 * Arguments    : string msg - the message to print.
 */
public void
catch_tell(string msg)
{
    write_socket(msg);
}

/*
 * Function name: query_prevent_shadow
 * Description  : Prevents shadowing of this object.
 * Returns      : int 1 - always.
 */
public nomask int
query_prevent_shadow()
{
    return 1;
}

/*
 * Function name: query_possessed
 * Description:	  Return the possessed object.
 */
public string
query_possessed()
{
    return (string)possessed->query_real_name();
}

/*
 * Function name: query_demon
 * Description  : Return the name of the possessing object.
 * Returns      : string - the name of the demon.
 */
public string
query_demon()
{
    return demon->query_real_name();
}

/*
 * Function name: query_real_name
 * Description  : This function returns the name of the wizard possessing
 *                the poor living whose soul is crammed into this object.
 *                The return value of this function should be identical to
 *                the result of the function query_demon().
 * Returns      : string - the name.
 */
public string
query_real_name()
{
    return geteuid(this_object());
}

/*
 * Function name: modify_command
 * Description  : This function modifies the command the possessed living
 *                enters before it is executed. Normally this would resolve
 *                aliases et cetera, but we just return the command as it
 *                was entered.
 * Arguments    : string cmd - the command the player entered.
 * Returns      : string - the same as the argument 'cmd'.
 */
public string
modify_command(string cmd)
{
    return cmd;
}
