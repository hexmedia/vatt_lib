/**
 * \file /std/belt.c
 *
 * Standard pasow.
 * Stworzony kiedys przez Ghasha, wzglednie dokonczony
 * przez Gjoefa pod patronatem Very.
 * Dzis mamy 6 maja 2007.
 *
 * Ehh. Ludziska kochane, pas == ubranie a u nas na mudzie ubranie == zbroja
 * A zbroje pisze si� na /std/armour.c
 * Po cholere podwaja� kod zak�adania, zdejmowania, wy�wietlania....
 * Pozatym po kij sto sublock�w jak mo�na po prostu na subinventory zrobi�?
 * Poprawi�em co uwa�a�em za b��dne. [Krun]
 *
 * Aha, nie poprawialem komentarzy ;)
 *
 * @author Ghash
 * @author Gjoef
 * @author Vera
 * @author Krun
 */

#include <macros.h>
#include <subloc.h>
#include <cmdparse.h>
#include <wa_types.h>
#include <object_types.h>
#include <stdproperties.h>

inherit "/std/armour";

#define CAP(x)                  capitalize(x)

#define PAS_SLOT(x)             ("_pas_subloc_" + (x))
#define ZATKNIETE_SLOT(x)       ((x) + "_ZATKNIETE")
#define PRZYCZEPIONE_SLOT(x)    ((x) + "_PRZYCZEPIONE")

static int              max_slots_prz;      /* Ile maksymalnie rzeczy mozna przyczepic */
static int              max_slots_zat;
static int              max_weight_zat;
static int              max_weight_prz;     /* Ile moze wazyc najciezszy przedmiot za pasem */
static int              max_volume_zat;     /* Ile wynosi maksymalna objetosc dla pojed. itemu */
static int              max_volume_prz;

string                  subloc_id;

public string query_subloc_id()
{
    return subloc_id;
}

public string set_subloc_id()
{
    subloc_id = "_pas_subloc_" + explode(file_name(TO), "#")[-1];
}

int
query_attachable()
{
    return 1;
}

int
query_attached()
{
    if (!TO->query_worn())
        return 0;

    return 1;
}

/**
 * Ustawia maksymaln� wag�, kt�ra decyduje o tym, jak
 * cie�kie przedmioty mo�emy zatkn�� za pasem.
 * @param w Maksymalna waga
 */
void
set_max_weight_zat(int w)
{
    max_weight_zat = w;
}

/**
 * @return Maksymaln� wag� przedmiotu jaki mo�emy zatkn�� za pasem.
 */
int
query_max_weight_zat()
{
    return max_weight_zat;
}

/**
 * Ustawia maksymaln� wag� przedmiotu jaki mo�emy do pasa przyczepi�.
 * @param w Maksymalna waga
 */
void
set_max_weight_prz(int w)
{
    max_weight_prz = w;
}
/**
 * @return Maksymaln� wag� przedmiotu jaki mo�emy do pasa przyczepi�.
 */
int
query_max_weight_prz()
{
    return max_weight_prz;
}

/**
 * Ustawia maksymaln� obj�to�� przedmiotu, kt�ry mo�emy zatkn�� za pasem.
 * @param v Maksymalna obj�to��
 */
void
set_max_volume_zat(int v)
{
    max_volume_zat = v;
}

/**
 * @return Zwraca maksymaln� obj�to�� przedmiotu, kt�ry mo�emy zatkn�� za pasem
 */
int
query_max_volume_zat()
{
    return max_volume_zat;
}

/**
 * Ustawia maksymaln� obj�to�� przedmiotu, kt�ry mo�e by� do pasa przyczepiony.
 * @param v Maksymalna obj�to��.
 */
void
set_max_volume_prz(int v)
{
    max_volume_prz = v;
}

/**
 * @return Maksymaln� obj�to�� przedmiotu, kt�ry mo�e by� do pasa przyczepiony.
 */
int
query_max_volume_prz()
{
    return max_volume_prz;
}

/**
 * Ustawia maksymaln� ilo�� slot�w na przedmioty przyczepiane do pasa.
 * @param max Maksymalna ilo�� slot�w.
 */
void
set_max_slots_prz(int max)
{
    max_slots_prz = max;
}

/**
 * @return Maksymaln� ilo�� slot�w na przedmioty przyczepiane do pasa..
 */
int
query_max_slots_prz()
{
    return max_slots_prz;
}

/**
 * Ustawia maksymaln� ilo�� slot�w na przedmioty zatykane za pasem.
 * @param max Maksymalna ilo�� slot�w.
 */
void
set_max_slots_zat(int max)
{
    max_slots_zat = max;
}

/**
 * @return Maksymaln� ilo�� slot�w na przedmioty zatykane za pasem.
 */
int
query_max_slots_zat()
{
    return max_slots_zat;
}

void                                                                        //
att_add_prz(string sloc, object ob)
{
    ob->add_prop("_att_id", subloc_id);
    ob->add_prop(OBJ_I_DONT_INV, 1);
}

void                                                                        //
att_add_zat(string sloc, object ob)
{
    ob->add_prop("_att_id", subloc_id);
    ob->add_prop(OBJ_I_DONT_INV, 1);
}

void
att_remove(object ob)
{
    string slot;
    if (objectp(TO->query_worn()))
    {
        slot = ZATKNIETE_SLOT(TO->query_subloc_id());
        if(!sizeof(TO->query_worn()->subinventory(slot)))
            TO->query_worn()->remove_subloc(slot);

        slot = PRZYCZEPIONE_SLOT(TO->query_subloc_id());
        if(!sizeof(TO->query_worn()->subinventory(slot)))
            TO->query_worn()->remove_subloc(slot);
    }

    ob->remove_prop("_att_id");
    ob->remove_prop(OBJ_I_DONT_INV);
    ob->move(TP, 0);
}

mixed
att_ob_map(int i = 0)
{
    object *ret = ({});

    switch (i)
    {
        case 1:
            ret = TO->query_worn()->subinventory(ZATKNIETE_SLOT(subloc_id));
            break;
        case 2:
            ret = TO->query_worn()->subinventory(PRZYCZEPIONE_SLOT(subloc_id));
            break;

        default:
            ret = att_ob_map(1) + att_ob_map(2);
            break;
    }

    ret = filter(ret, objectp);
    return ret;
}

void
att_hook_zatknij(object liv, object ob, object belt)
{
    liv->catch_msg("Zatykasz " + QSHORT(ob, PL_BIE) + " za "+
        QSHORT(belt, PL_BIE) + ".\n");
    tell_roombb(environment(liv), QCIMIE(liv, PL_MIA) + " zatyka "+
        QSHORT(ob, PL_BIE) + " za " + QSHORT(belt, PL_BIE) + ".\n",
        ({liv}));
}

void
att_hook_wyjmij(object liv, object ob, object belt)
{
    liv->catch_msg("Wyjmujesz " + QSHORT(ob, PL_BIE) + " zza "+
        QSHORT(belt, PL_DOP) + ".\n");
    tell_roombb(environment(liv), QCIMIE(liv, PL_MIA) + " wyjmuje "+
        QSHORT(ob, PL_BIE) + " zza " + QSHORT(belt, PL_DOP) + ".\n",
        ({liv}));
}

void
att_hook_przyczep(object liv, object ob, object belt)
{
    liv->catch_msg("Przyczepiasz " + QSHORT(ob, PL_BIE) + " do "+
        QSHORT(belt, PL_DOP) + ".\n");
    tell_roombb(environment(liv), QCIMIE(liv, PL_MIA) + " przyczepia "+
        QSHORT(ob, PL_BIE) + " do " + QSHORT(belt, PL_DOP) + ".\n",
        ({liv}));
}


void
att_hook_odczep(object liv, object ob, object belt)
{
    liv->catch_msg("Odczepiasz " + QSHORT(ob, PL_BIE) + " od "+
        QSHORT(belt, PL_DOP) + ".\n");
    tell_roombb(environment(liv), QCIMIE(liv, PL_MIA) + " odczepia "+
        QSHORT(ob, PL_BIE) + " od " + QSHORT(belt, PL_DOP) + ".\n",
        ({liv}));
}

int
filter_obs(object ob)
{
    if (!ob->query_subloc() || ob->query_subloc() == SUBLOC_WIELD)
        return 1;
}

int
filter_attached(object ob, mixed m = calling_function(-3))
{
    string sloc;

    sloc = ob->query_subloc();

    if (!sloc)
        return 0;

    switch(m)
    {
        case "wyjmij":
            if (wildmatch(ZATKNIETE_SLOT("*"), sloc))
                return 1;

        case "odczep":
            if (wildmatch(PRZYCZEPIONE_SLOT("*"), sloc))
                return 1;

        default:
            if (wildmatch(subloc_id+"*", sloc))
                return 1;
    }
}


int
is_attachable(object ob, int i)
{
    switch(i)
    {
        case 1:
            if(function_exists("create_weapon", ob))
            {
                if(ob->query_wt() == W_KNIFE ||
                    ob->query_wt() == W_SWORD ||
                    ob->query_wt() == W_AXE ||
                    ob->query_wt() == W_CLUB ||
                    ob->query_wt() == W_WARHAMMER ||
                    ob->query_nazwa(0) == "siekiera" ||
                    ob->query_nazwa(0) == "siekierka" ||
                    ob->query_nazwa(0) == "toporek" ||
                    ob->query_nazwa(0) == "mieczyk" ||
                    ob->query_nazwa(0) == "miecz")
                {
                    return 1;
                }
                return 0;
            }
        case 2:
            if(ob->query_prop(OBJ_I_ATTACHABLE))
                return 1;

            return 0;
        default:
            return 1;
    }
}

int
zatknij(string str)
{
    int i;
    mixed tmp;
    object *arr1, *arr2, ob, att;
    string slot;

    notify_fail("Zatknij co gdzie?\n");

    if (!str)
        return 0;

    if (!parse_command(str, all_inventory(TP), "%i:"+PL_BIE+" 'za' %i:"+PL_BIE, arr1, arr2))
        return 0;

    arr1 = NORMAL_ACCESS(arr1, "filter_obs", TO);
    arr2 = NORMAL_ACCESS(arr2, 0, 0);

    if (!sizeof(arr1) || !sizeof(arr2))
        return 0;

    notify_fail("Nie mo^zesz zatkn^a^c naraz wi^ecej ni^z jednego przedmiotu.\n");
    if (sizeof(arr1) > 1)
        return 0;

    ob = arr1[0];
    att = arr2[0];

    if(att != TO)
        return 0;

    notify_fail("Nie masz na sobie niczego, za co m" + TP->koncowka("^og^l", "og^la") + "by^s to zatkn^a^c.\n");
    if (!ob)
        return 0;

    tmp = att->query_subloc_id();
    slot = ZATKNIETE_SLOT(tmp);

    notify_fail("Ale^z masz ju^z " + ob->query_short(3) + " za " + TO->query_short(4) + ".\n");
    if (ob->query_subloc() == slot)
        return 0;

    notify_fail(CAP(ob->short(TP, PL_MIA))+" jest na to zbyt ci^e^zk"+
        ob->koncowka("i","a","ie")+".\n");
    if (ob->query_prop(OBJ_I_WEIGHT) > max_weight_zat ||
        ob->query_prop(CONT_I_WEIGHT) > max_weight_zat)
        return 0;

    notify_fail(CAP(ob->short(TP, PL_MIA))+" jest na to zbyt du^z"+
        ob->koncowka("y","a","e")+".\n");
    if (ob->query_prop(OBJ_I_VOLUME) > max_volume_zat ||
        ob->query_prop(CONT_I_VOLUME) > max_volume_zat)
        return 0;

    notify_fail("Musisz najpierw za^lo^zy^c " + att->query_short(3) + ".\n");
    if (TO->query_worn() != TP)
        return 0;

    notify_fail("Nie mo^zesz zatkn^a^c " + ob->query_short(1) + " za " + att->query_short(3) + ".\n");
    if (!is_attachable(ob, 1))
        return 0;

    /* Zmieni�em na zatykanie wszystkiego na jeden slot. [Krun]
     */
    notify_fail("Nie uda ci si^e zatkn^a^c ju^z niczego wi^ecej.\n");

    //Je�li slota nie ma to musimy doda�.
    if (member_array(slot, TP->query_sublocs()) == -1)
        TP->add_subloc(slot, ob);
    //A jak jest to sprawdzi� czy zmie�ci si� jeszcze co�.
    else if(sizeof(TO->query_worn()->subinventory(slot)) >= query_max_slots_zat())
        return 0;

    /* Jezeli ktos chce zabronic przyczepiania czegos */
    if (att->att_prevent_attach(TP, ob))
        return 1;

    /* Jesli bron byla wczesniej dobyta */
    if(ob->query_subloc() == SUBLOC_WIELD)
	ob->unwield_me();

    /* Jezeli ktos chce zmienic opis przyczepiania */
    if (!ob->att_hook_zatknij(TP, att))
        att_hook_zatknij(TP, ob, att);


    att->att_add_zat(slot, ob);
    ob->move(TP, slot);
    ob->add_prop("_att_id", att->query_subloc_id());

    return 1;
}

int
wyjmij(string str)
{
    int i;
    mixed tmp;
    object *arr1, *arr2, ob, att;
    string slot = ZATKNIETE_SLOT(query_subloc_id());

    notify_fail("Wyjmij co?\n");
    if (!str)
        return 0;

    notify_fail("Wyjmij co zza czego?\n");
    if (!parse_command(str, all_inventory(TP),
        "%i:"+PL_BIE+" 'zza' %i:"+PL_DOP, arr1, arr2))
        return 0;

    arr1 = NORMAL_ACCESS(arr1, "filter_attached", TO);
    arr2 = NORMAL_ACCESS(arr2, 0, 0);

    if (!sizeof(arr1) || !sizeof(arr2))
        return 0;

    ob = arr1[0];
    att = arr2[0];

    notify_fail("Nie mo^zesz niczego wyj^a^c zza "+
        att->short(TP, PL_DOP)+".\n");
    if (!att->query_attachable())
        return 0;

    notify_fail(CAP(ob->short(TP, PL_MIA))+" nie jest zatkni^et"+ob->koncowka("y","a","e")+" za " + att->query_short(3) + ".\n");
    if (ob->query_subloc() != slot)
        return 0;

    /* Jezeli ktos chce zmienic opis odczepiania */
    if (!ob->att_hook_wyjmij(TP, att))
        att_hook_wyjmij(TP, ob, att);

    att->att_remove(ob);

    return 1;
}

int
przyczep(string str)
{
    int i;
    mixed tmp;
    object *arr1, *arr2, ob, att;
    string slot;

    notify_fail("Przyczep co gdzie?\n");
    if (!str)
        return 0;

    if (!parse_command(str, all_inventory(TP),
        "%i:"+PL_BIE+" 'do' %i:"+PL_DOP, arr1, arr2))
        return 0;

    arr1 = NORMAL_ACCESS(arr1, "filter_obs", TO);
    arr2 = NORMAL_ACCESS(arr2, 0, 0);

    if (!sizeof(arr1) || !sizeof(arr2))
        return 0;

    notify_fail("Nie mo^zesz przyczepi^c naraz wi^ecej ni^z jednego przedmiotu.\n");
    if (sizeof(arr1) > 1)
        return 0;

    ob = arr1[0];
    att = arr2[0];

    if(att != TO)
        return 0;

    notify_fail("Nie masz na sobie niczego, do czego m" + TP->koncowka("^og^lby^s",
    "og^laby^s") + " przyczepi^c " + ob->query_short(3) + ".\n");
    if (!ob)
        return 0;

    tmp = att->query_subloc_id();
    slot = PRZYCZEPIONE_SLOT(tmp);
    notify_fail("Ale^z masz ju^z " + ob->query_short(3) + " przy " + TO->query_short(5) + ".\n");
    if (ob->query_subloc() == slot)
        return 0;

    notify_fail(CAP(ob->short(TP, PL_MIA))+" jest na to zbyt ci^e^zk"+
        ob->koncowka("i","a","ie")+".\n");
    if (ob->query_prop(OBJ_I_WEIGHT) > max_weight_prz ||
        ob->query_prop(CONT_I_WEIGHT) > max_weight_prz)
        return 0;

    notify_fail(CAP(ob->short(TP, PL_MIA))+" jest na to zbyt du^z"+
        ob->koncowka("y","a","e")+".\n");
    if (ob->query_prop(OBJ_I_VOLUME) > max_volume_prz ||
        ob->query_prop(CONT_I_VOLUME) > max_volume_prz)
        return 0;

    notify_fail("Musisz najpierw za^lo^zy^c " + att->query_short(3) + ".\n");
    if (TO->query_worn() != TP)
        return 0;

    notify_fail("Nie mo^zesz przyczepi^c " + ob->query_short(1) + " do " + att->query_short(1) + ".\n");
    if (!is_attachable(ob, 2))
        return 0;


    tmp = att->query_subloc_id();

    /* Tu te� przerobi�em na jeden subloc z subinventory. [Krun] */
    notify_fail("Nie uda ci si^e przyczepi^c ju^z niczego wi^ecej.\n");
    slot = PRZYCZEPIONE_SLOT(tmp);
    if (member_array(slot, TP->query_sublocs()) == -1)
        TP->add_subloc(slot, ob);
    else if(sizeof(TP->subinventory(slot)) >= query_max_slots_prz())
        return 0;

    /* Jezeli ktos chce zabronic przyczepiania czegos */
    if (att->att_prevent_attach(TP, ob))
        return 1;

    /* Jezeli ktos chce zmienic opis przyczepiania */
    if (!ob->att_hook_przyczep(TP, att))
        att_hook_przyczep(TP, ob, att);

    att->att_add_prz(slot, ob);
    ob->move(TP, slot);
    ob->add_prop("_att_id", att->query_subloc_id());

    return 1;
}

int
odczep(string str)
{
    int i;
    mixed tmp;
    object *arr1, *arr2, ob, att;

    notify_fail("Odczep co?\n");
    if (!str)
        return 0;

    if (!parse_command(str, all_inventory(TP),
        "%i:"+PL_BIE+" 'od' %i:"+PL_DOP, arr1, arr2))
        return 0;

    arr1 = NORMAL_ACCESS(arr1, "filter_attached", TO);
    arr2 = NORMAL_ACCESS(arr2, 0, 0);

    if (!sizeof(arr1) || !sizeof(arr2))
        return 0;

    ob = arr1[0];
    att = arr2[0];

    notify_fail("Nie mo^zesz niczego odczepi^c od "+
        att->short(TP, PL_DOP)+".\n");
    if (!att->query_attachable())
        return 0;

    notify_fail(CAP(ob->short(TP, PL_MIA))+" nie jest przyczepion"+ob->koncowka("y","a","e")+" do " + att->query_short(1) + ".\n");
    if (ob->query_subloc() != PRZYCZEPIONE_SLOT(query_subloc_id()))
        return 0;

    /* Jezeli ktos chce zmienic opis odczepiania */
    if (!ob->att_hook_odczep(TP, att))
        att_hook_odczep(TP, ob, att);

    att->att_remove(ob);

    return 1;
}

void
dobadz(string str)
{
    object *was, das;

    if (!str || str=="")
    {
        notify_fail("Dob^ad^x czego?\n");
        return;
    }

    if (parse_command(str, all_inventory(TP),
        "%i:" + PL_DOP, was))
    {
        was = NORMAL_ACCESS(was, 0, 0);
        das = was[0];

        if (member_array(das, TO->query_worn()->subinventory(ZATKNIETE_SLOT(query_subloc_id()))) != -1)
            att_hook_wyjmij(TP, das, TO);

        TO->att_remove(das);
    }
}

void
init_attach_lib()
{
    add_action(zatknij,     "zatknij");
    add_action(wyjmij,      "wyjmij");

    add_action(przyczep,    "przyczep");
    add_action(przyczep,    "przytrocz");
    add_action(odczep,      "odczep");

    add_action(dobadz,      "dob^a^d^x");
    add_action(dobadz,      "chwy^c");
}

/*
*  Function name: att_remove_from_sublocs
*  Description  : Funkcja ta musi byc w kazdym remove_object, dziedziczacym ten
*                 lib.
*/
void
att_remove_from_sublocs()
{
    //Musimy powyjmowa� wsio co jest na sublockach.

    //Najsampierw co zatkni�te
    string tmp = query_subloc_id();
    string slot;

    object *obs;

    slot = ZATKNIETE_SLOT(tmp);
    if(slot && member_array(slot, TO->query_worn()->query_sublocs()) != -1)
    {
        obs = TO->query_worn()->subinventory(slot);
        while(sizeof(obs))
        {
            TO->query_worn()->command("wyjmij " + OB_NAME(obs[0]) + " zza " + OB_NAME(TO));
            obs = exclude_array(obs, 0, 0);
        }
    }

    slot = PRZYCZEPIONE_SLOT(tmp);
    if(slot && member_array(slot, TO->query_worn()->query_sublocs()) != -1)
    {
        obs = TO->query_worn()->subinventory(slot);
        while(sizeof(obs))
        {
            TO->query_worn()->command("odczep " + OB_NAME(obs[0]) + " zza " + OB_NAME(TO));
            obs = exclude_array(obs, 0, 0);
        }
    }
}

string
att_desc(object for_obj)
{
    string str = "", zat, prz;
    object *z, *p;

    z = TO->att_ob_map(1);
    p = TO->att_ob_map(2);

    if(sizeof(z))
        zat = FO_COMPOSITE_DEAD(z, for_obj, PL_NAR);

    if(sizeof(p))
        prz = FO_COMPOSITE_DEAD(p, for_obj, PL_BIE);

    if (strlen(zat))
    {
        str = " z zatkni^et" + ilosc(sizeof(z), z[0]->koncowka("ym","^a", "ym", "ymi", "ymi"), "ymi", "ymi") + " za^n " + zat;

        if (strlen(prz))
            str += ", przy nim za^s nosi" + (for_obj == TO->query_worn() ? "sz" : "") + " " + prz;
    }
    else if (strlen(prz))
        str = ", przy kt^orym nosi" + (for_obj == TO->query_worn() ? "sz" : "") + " " + prz;

    return str;
}

///////////////////////////////////////////////////////////////////////////////

void
create_belt()
{
    ustaw_nazwe("pas");
    dodaj_przym("gotowy", "gotowi");
}

nomask void
create_armour()
{
    set_long("Zwyk�y standardowy pas.\n");

    add_name("belt_id");

    add_prop(OBJ_I_WEIGHT, 1000);
    add_prop(OBJ_I_VOLUME, 500);

    set_type(O_UBRANIA);
    set_slots(A_HIPS);

    set_max_slots_zat(3);
    set_max_slots_prz(3);
    set_max_weight_zat(2500);
    set_max_weight_prz(800);
    set_max_volume_zat(2300);
    set_max_volume_prz(1200);

    set_subloc_id();

    set_wf(TO);

    create_belt();
}

string
qury_pomoc()
{
    return "Maj^ac pas, mo^zesz przyczepi^c do niego sakiewk^e lub zatkn^a^c " +
        "za^n bro^n.\n";
}

public string
short(mixed fob, int przyp)
{
    if(!objectp(fob))
    {
        if(intp(fob))
            przyp = fob;

        fob = TP;
    }

    if((calling_function(-5) == "cb_show_worn" || calling_function(-6) == "cb_show_worn") &&
        !this_player()->query_prop(TEMP_SHOW_ALL_THINGS))
    {
        return ::short(fob, przyp) + att_desc(fob);
    }
    else
        return ::short(fob, przyp);
}

void
init()
{
    ::init();
    init_attach_lib();
}

void
remove_object()
{
    att_remove_from_sublocs();
    ::remove_object();
}

public string long(string str, object fob)
{
    if(!str)
    {
        string *old_long = explode(::long(0, fob), "\n");
        string zat, prz;

        //str = implode(old_long, "\n")[..-3];

        if(sizeof(TO->att_ob_map(1)))
            zat = FO_COMPOSITE_DEAD(TO->att_ob_map(1), fob, PL_BIE);

        if(sizeof(TO->att_ob_map(2)))
            prz = FO_COMPOSITE_DEAD(TO->att_ob_map(2), fob, PL_BIE);

	str = "";
        if (strlen(zat))
        {
            str += "Zatkni�to za niego " + zat;

            if (strlen(prz))
                str += ", za� przypi�te s� " + prz;
        }
        else if (strlen(prz))
            str += "Przypi�to do niego " + (fob == TO->query_worn() ? "sz" : "") + " " + prz;

        return old_long[0] + "\n" + str + ".\n" + implode(old_long[1..-1], "\n") + "\n";
    }

    return ::long(str, fob);
}

public int remove(object ob)
{
    att_remove_from_sublocs();
}

/**
 * Funkcja identyfikuj�ca przedmiot jako pas.
 * @return 1 zawsze
 */
public int is_belt() { return 1; }

/**
 * Funkcja identyfikuj�ca przedmiot jako pas.
 * @return 1 zawsze
 */
public int query_belt() { return 1; }

//Trzeba dopisa� auto�adowanie sie tak, aby rzeczy zostawa�y przypi�te czy te� zatkni�te, ale wymaga troche gimnastyki
//dlatego zostawi�em na p�niej [Krun].
