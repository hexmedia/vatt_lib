/**
 * \file /std/pattern.c
 *
 * Ten plik zawiera wsparcie dla wzorc�w.
 */

#include <pattern.h>
#include <rozmiary.h>
#include <stdproperties.h>
#include "/config/login/login.h"

int pattern_domain = PATTERN_DOMAIN_NONE;
int pattern_difficulty = PATTERN_DEFAULT_DIFFICULTY;
string item_filename;
int estimated_price = 100;
int want_sizes = 0;
int time_to_complete = 300;
mixed attributes = ({});
string* possible_materials = 0;

/**
 * Zwraca dziedzin�, do kt�rej nale�y dany wzorzec.
 *
 * @return dziedzina, do kt�rej nale�y wzorzec
 */
public int
query_pattern_domain()
{
  return pattern_domain;
}

/**
 * Ustawia dziedzin�, do kt�rej nale�y dany wzorzec.
 *
 * @param domain dziedzina, do kt�rej nale�y wzorzec
 */
public void
set_pattern_domain(int domain)
{
  pattern_domain = domain;
}

/**
 * Zwraca szacowany czas wykonania przedmiotu.
 *
 * @return szacowany czas wykonania przedmiotu.
 */
public int
query_time_to_complete()
{
  return time_to_complete;
}

/**
 * Ustawia szacowany czas wykonania przedmiotu.
 *
 * @param time szacowany czas wykonania przedmiotu.
 */
public void
set_time_to_complete(int time)
{
  time_to_complete = time;
}

/**
 * Zwraca trudno�� wykonania danego projektu.
 *
 * @return trudno�� wykonania danego projektu
 */
public int
query_pattern_difficulty()
{
  return pattern_difficulty;
}

/**
 * Ustawia trudno�� wykonania danego prjektu.
 *
 * @param difficulty trudno�� wykonania danego projektu
 */
public void
set_pattern_difficulty(int difficulty)
{
  pattern_difficulty = difficulty;
}

/**
 * Zwraca �cie�k� do pliku, kt�ry jest opisywany przez dany wzorzec.
 *
 * @return �cie�ka do pliku, kt�ry jest opisywany przez dany wzorzec
 */
public string
query_item_filename()
{
  return item_filename;
}

/**
 * Ustawia �cie�k� do pliku, kt�ry jest opisywany przez dany wzorzec.
 *
 * @param filename �cie�ka do pliku, kt�ry jest opisywany przez dany wzorzec
 */
public void
set_item_filename(string filename)
{
  item_filename = filename;
}

/**
 * Konfiguruje obiekt.
 *
 * @param item obiekt do skonfigurowania
 * @param attrs wybrane przez u�ytkownika atrybuty
 */
public void
configure_item(object item, mapping attrs)
{
  float* rozmiary;
  float* wspolczynniki;
  int il;
  if (pointerp(attrs["__sizes"])) {
    if (sizeof(attrs["__sizes"]) == 3) { /* ubranie dla kogo� */
      string race = attrs["__sizes"][0];
      int gender = attrs["__sizes"][1];
      float factor = attrs["__sizes"][2];
      wspolczynniki = RACEROZMOD[race][gender];
      rozmiary = item->pobierz_rozmiary(item->query_slots_setting());
      for (il = 0; il <= ROZ_MAX; il++) {
        if (rozmiary[il] != -1.0) {
          rozmiary[il] = wspolczynniki[il] * factor;
        }
      }
      item->add_prop(ARMOUR_S_DLA_RASY, race);
      item->add_prop(ARMOUR_I_DLA_PLCI, gender);
      item->ustaw_rozmiary(rozmiary);
    }
    else if (sizeof(attrs["__sizes"]) == 21) { /* ubranie dla this_player */
      string race = this_player()->query_race();
      int gender = this_player()->query_gender();
      rozmiary = item->pobierz_rozmiary(item->query_slots_setting());
      for (il = 0; il <= ROZ_MAX; il++) {
        if (rozmiary[il] != -1.0) {
          rozmiary[il] = attrs["__sizes"][il];
        }
      }
      item->add_prop(ARMOUR_S_DLA_RASY, race);
      item->add_prop(ARMOUR_I_DLA_PLCI, gender);
      item->ustaw_rozmiary(rozmiary);
    }
  }
}

/**
 * Zwraca szacowan� cen� przedmiotu.
 *
 * @return szacowana cena przedmiotu
 */
public int
query_estimated_price()
{
  return estimated_price;
}

/**
 * Ustawia szacowan� cen� przedmiotu.
 *
 * @param price szacowana cena przedmiotu
 */
public void
set_estimated_price(int price)
{
  estimated_price = price;
}

/**
 * Zwraca czy dany wzorzec wymaga podania rozmiar�w.
 *
 * @return czy dany wzorzec wymaga podania rozmiar�w.
 */
public int
query_want_sizes()
{
  return want_sizes;
}

/**
 * Ustawia, �e dany wzorzec wymaga podania rozmiar�w.
 */
public void
enable_want_sizes()
{
  want_sizes = 1;
}

/**
 * Dodaje atrybut do wzorca.
 *
 * @param name nazwa atrybutu
 * @param attrs mo�liwe warto�ci atrybutu
 *
 * @return miejsce w tablicy dodanego atrybutu
 */
public int
add_attribute(string name, mixed attrs)
{
  attributes += ({ ({ name, attrs }) });
  return sizeof(attributes) - 1;
}

/**
 * Usuwa atrybut z wzorca.
 *
 * @param name nazwa atrybutu
 */
public void
remove_attribute(string name)
{
  attributes = filter(attributes, &operator(!=)(name) @ &operator([])(, 0));
}

/**
 * Zwraca tablic� atrybut�w.
 *
 * @return tablica atrybut�w
 */
public mixed
query_attributes()
{
  return attributes;
}

/**
 * Zwraca potencjalnie potrzebne materia�y do wykonania przedmiotu.
 *
 * @return tablica potencjalnie potrzebnych materia��w
 */
public string*
query_possible_materials()
{
  if (!pointerp(possible_materials)) {
    setuid();
    seteuid(getuid());
    possible_materials = clone_object(query_item_filename())->query_materialy();
  }
  return possible_materials;
}

/**
 * Ustawia potencjalnie potrzebne materia�y do wykonania przedmiotu.
 *
 * @param materials potencjalnie potrzebne materia�y do wykonania przedmiotu
 */
public void
set_possible_materials(string* materials)
{
  possible_materials = materials;
}
