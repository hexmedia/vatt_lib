/**
 * \file /std/golab.c
 *
 * @author (Nie wiem kto) poprawki Krun
 *
 * Go��b pocztowy.
 */

inherit "/std/creature";
inherit "/std/combat/unarmed";

inherit "/std/act/action";

#include <pl.h>
#include <files.h>
#include <macros.h>
#include <colors.h>
#include <cmdparse.h>
#include <ss_types.h>
#include <wa_types.h>
#include <filter_funs.h>
#include <stdproperties.h>

/*
 * Definiujemy to dla wlasnej wygody
 */
#define A_DZIOB     0
#define A_LLAPA     1
#define A_PLAPA     2

#define H_GLOWA     0
#define H_KORPUS    1

#define DEF_CZAS_LOTU (7.0 + frandom(10, 2))

int odeslany;

object list;

string adresat;
string sender;

string zaatakowany();

void
create_golab()
{
    ustaw_odmiane_rasy( ({"go^l^ab","go^l^ebia","go^l^ebiowi","go^l^ebia",
        "go^l^ebiem","go^l^ebiu"}),({"go^l^ebie","go^l^ebi","go^l^ebiom",
        "go^l^ebie","go^l^ebiami","go^l^ebiach"}), PL_MESKI_NZYW);

    ustaw_shorty( ({"go^l^ab pocztowy","go^l^ebia pocztowego",
        "go^l^ebiowi pocztowemu","go^l^ebia pocztowego",
        "go^l^ebiem pocztowym","go^l^ebiu pocztowym"}),
        ({"go^l^ebie pocztowe","go^l^ebi pocztowych",
        "go^l^ebiom pocztowym","go^l^ebie pocztowe",
        "go^l^ebiami pocztowymi","go^l^ebiach pocztowych"}), PL_MESKI_NZYW);

    set_gender(G_MALE);

    set_long("Zwyk^ly go��b pocztowy.\n");
}

void
create_creature()
{
    set_stats( ({ 5, 40, 5, 5, 15, 5}));

    set_skill(SS_DEFENCE, 35);
    set_skill(SS_UNARM_COMBAT, 30);
    /*od kiedy go��bie s� takie bojowe? ;) V.
    set_attack_unarmed(A_DZIOB, 10, 10, W_IMPALE, 60, "dziobem");
    set_attack_unarmed(A_LLAPA, 5, 5, W_SLASH,  20, "pazurami lewej ^lapki");
    set_attack_unarmed(A_PLAPA, 5, 5, W_SLASH,  20, "pazurami prawej ^lapki");
    */
    set_hitloc_unarmed(H_GLOWA, ({ 10, 20, 15 }), 15, "g^lowa");
    set_hitloc_unarmed(H_KORPUS, ({ 10, 10, 20 }), 85, "cia�o");
  //set_hitloc_unarmed(0, ({1, 1, 1}), 40, "g�owa");
  //set_hitloc_unarmed(1, ({1, 1, 1}), 60, "cia�o");
    remove_prop(OBJ_M_NO_GET);
    add_prop(LIVE_I_NEVERKNOWN, 1);

    add_prop(CONT_I_WEIGHT, 2000);
    add_prop(CONT_I_VOLUME, 3000);
    add_prop(OBJ_I_VALUE,196);

    add_prop(OBJ_M_NO_ATTACK, &zaatakowany());

    create_golab();

    list = 0;
}

void
destructnij()
{
    destruct();
}

string
zaatakowany()
{
    set_alarm(0.1, 0.0, &write("Pr�bujesz zaatakowa� "+
        this_object()->short(PL_BIE)+", lecz ten"
        +" w ostatniej chwili wzbija si� w powietrze"
        +" i odlatuje.\n"));
    saybb(capitalize(this_object()->short(PL_MIA))+" w ostatniej chwili wzbija si� w powietrze i"
        +" odlatuje.\n");
        set_alarm(0.5,0.0,&destructnij());
  return "";
}


string
query_long()
{
    return ::query_long() + (list ? "Zauwa^zasz ma^l^a karteczk^e przy jego n^o^zce.\n" : "");
}

static private object
znajdz_adresata(string dop)
{
    object ob = SECURITY->finger_player(adresat, PL_DOP);
    if(ob)
        object ob2 = find_player(ob->query_real_name());
    else
        return 0;
    ob->remove_object();
    return ob2;
}

public varargs void przylot(object player, int nie_znalazl=0);

static void
golab_wyslany()
{
    //Tu trzeba zrobi� rozr�nienie na miasta, �eby mi�dzy miastami
    //porusza� si� troche wolniej, na razie podzia� tylko na locacje.

    object player = znajdz_adresata(adresat);

    if(player && ENV(player) == ENV(TP))
    {
        set_alarm(0.1, 0.0, &przylot(TP));
    }
    else
    {
        //Tu trzeba b�dzie doda� sprawdzanie nazwy miasta i odleg�o�ci
        //mi�dzy miastami.
        set_alarm(DEF_CZAS_LOTU, 0.0, &przylot(TP));
    }
}

public int napisz(string str);
public int wyslij(string str);
public int odczep(string str);
public int odeslij(string str);

void
init()
{
    ::init();
    add_action(napisz, "napisz");
    add_action(wyslij, "wy^slij");
    add_action(odczep, "odczep");
    add_action(odeslij, "ode�lij");
}

string
query_pomoc()
{
    return "Napisz wiadomo�� i wy�lij, za� je�li si� pomylisz po prostu j� odczep.\n";
}

public void
done_editing(string text, object tp=0)
{
    list = clone_object("/lib/wiadomosc");
    list->move(this_object());
    list->set_message(text);
    tp->catch_msg("Ko�czysz pisanie wiadomo�ci.\n");
}

int
napisz(string str)
{
    if (!(str ~= "wiadomo^s^c"))
    {
        notify_fail("Napisz co? Mo^ze wiadomo^s^c?\n");
        return 0;
    }

    if (list)
    {
        notify_fail("Ten go^l^ab ju^z ma jedn^a wiadomo^s^c.\n");
        return 0;
    }

    clone_object(EDITOR_OBJECT)->edit(&done_editing(,TP));

    return 1;
}

int
wyslij(string str)
{
    object golab;

    if (!str || !parse_command(str, this_player(), "%o:" + PL_BIE + " 'do' %s", golab, adresat) || golab != this_object())
    {
        notify_fail("Wy^slij " + short(PL_BIE) + " do kogo?\n");
        return 0;
    }

    if(ENV(TP)->query_prop(ROOM_I_INSIDE))
    {
        notify_fail("Aby to uczyni� musisz wyj�� na zewn�trz.\n");
        return 0;
    }

    write("Wysy^lasz go^l^ebia pocztowego w sin^a dal.\n");
    saybb(QCIMIE(this_player(), PL_MIA) + " wysy^la go^l^ebia pocztowego w sin^a dal.\n");

    set_no_show();
    set_no_show_composite(1);
    sender = TP->query_real_name();

    golab_wyslany();

    return 1;
}

public varargs void
przylot(object player, int nie_znalazl=0)
{
    string kto;

    object odbiorca = znajdz_adresata(adresat);

    // golab wraca, jesli adresata nie ma w grze, a jesli jest w grze
    // w pomieszczeniu to 1/5 szansy, �e wr�ci.
    // jesli nie znalaz� to wraca po podw�jnie d�ugim czasie..
    if((!odbiorca || (ENV(odbiorca)->query_prop(ROOM_I_INSIDE) &&
       random(5) != 2) && !odeslany) || nie_znalazl)
    {
        //Je�li gracz w tym czasie przeszed� do jakiego� pomieszczenia, to go��b
        //nie wraca!
        if(ENV(player)->query_prop(ROOM_I_INSIDE))
        {
            destructnij();
            return;
        }
        if(!nie_znalazl)
        {
            set_alarm(DEF_CZAS_LOTU, 0.0, &przylot(player, 1));
            return;
        }
        player->catch_msg("Go^l^ab powr^oci^l... widocznie nie znalaz^l adresata.\n");
        saybb("Go^l^ab pocztowy siada na ramieniu "+
            QIMIE(player, PL_DOP) + ".\n", ({player}));
    }
    else if(odbiorca && odeslany)
    {
        TO->move(odbiorca, 1);
        odbiorca->catch_msg("Go^l^ab powr^oci^l... widocznie nie znalaz^l adresata.\n");;
        tell_roombb(ENV(odbiorca), "Go^l^ab pocztowy siada na ramieniu "+
            QIMIE(odbiorca, PL_DOP) + ".\n", ({odbiorca}), TO);
        odeslany = 0;
    }
    else
    {
        this_object()->move(odbiorca, 1);
        odbiorca->catch_msg(set_color(COLOR_FG_CYAN) +
            "Jaki^s go^l^ab pocztowy l^aduje ci na ramieniu.\n" + clear_color());
        tell_roombb(ENV(odbiorca), "Go^l^ab pocztowy siada na ramieniu "+
            QIMIE(odbiorca, PL_DOP) + ".\n", ({odbiorca}), TO);
    }
    unset_no_show();
    set_no_show_composite(0);
}

int
odeslij(string str)
{
    object *ob;

    NF("Co chcesz odes�a�?\n");

    if(!str)
        return 0;

    if(!parse_command(str, all_inventory(TP), "%i:" + PL_BIE, ob))
        return 0;

    if(!sizeof(ob))
        return 0;

    ob = NORMAL_ACCESS(ob, 0, 0);

    if(!sizeof(ob))
        return 0;

    if(sizeof(ob) > 1)
    {
        NF("Nie mo�esz odes�a� wi�cej ni� jednego go��bia na raz.\n");
        return 0;
    }

    if(ENV(TP)->query_prop(ROOM_I_INSIDE))
    {
        notify_fail("Aby to uczyni� musisz wyj�� na zewn�trz.\n");
        return 0;
    }

    adresat = sender;
    odeslany = 1;
    golab_wyslany();

    write("Odsy�asz go��bia do adresata.\n");
    saybb(QCIMIE(this_player(), PL_MIA) + " wysy^la go^l^ebia pocztowego w sin^a dal.\n");

    set_no_show();

    return 1;
}

int
odczep(string str)
{
    NF("Co takiego chcesz odczepi�?\n");

    if(!str)
        return 0;

    if(!wildmatch("wiadomo�� *", str) && !wildmatch("karteczk� *", str))
        return 0;

    NF("Od czego chcesz odczepi� wiadomo��?\n");

    object *golab;
    if(!parse_command(str, all_inventory(TP), "'wiadomo��' / 'karteczk�' 'od' [nogi] / [n�ki] %i:" + PL_DOP, golab))
        return 0;

    if(!golab)
        return 0;

    golab = NORMAL_ACCESS(golab, 0, 0);

    if(!sizeof(golab))
        return 0;

    if(sizeof(golab) > 1)
    {
        NF("Nie mo�esz odczepi� jednej wiadomo�ci od n�g kilku go��bi.\n");
        return 0;
    }

    if(golab[0] != TO)
        return 0;

    if(!list)
    {
        NF("Do n�ki " + short(TP, PL_DOP) + " nie jest przyczepiona �adna "+
            "wiadomo��.\n");
        return 0;
    }

    list->move(TP, 1);
    list = 0;

    write("Odczepiasz wiadomo�� od n�ki " + short(TP, PL_DOP) + ".\n");
    saybb(QCIMIE(TP, PL_MIA) + " odczepia wiadomo�� od n�ki " + short(TP, PL_DOP) + ".\n");
    return 1;
}

/**
 * Miska cu�:
 *
 * Kamie�, p�ytka, na kominek, colorado, 1 paczka(0,67m^2), 52.97brutto + jedna paczka
 * naro�niki kolorado - 61zl
 */