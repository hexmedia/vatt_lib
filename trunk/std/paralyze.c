/**
 *
 * \file /std/paralyze.c
 *
 * Clone and move this object to a player if you want to paralyze him.
 *
 * Gdy minie czas parali�u wywo�ywana jest w obiekcie finish_object
 * funkcja finish_fun. Gdy parali� zostanie przerwany komend�
 * wywo�ana jest stop_fun w stop_object.
 *
 * Eventy wsp�pracuj� z globalnym systemem event�w
 *
 * @author Kto� z Gena albo Arki
 * @author Molder - dodanie finish'u
 * @author Krun - wprowadzenie nowej obs�ugi event�w
 */
#pragma strict_types

#include <files.h>
#include <macros.h>
#include <stdproperties.h>

inherit "/std/object";

inherit "/lib/event.c";

#define WYWOLYWAL_KOMENDE "_wywolywal_komende"

/*
 * Variables
 */
static string   stop_verb,          /* What verb to stop this paralyze ? */
                stop_fun,           /* What function to call when stopped */
                finish_fun,         /* Funkcja wywo�ywana przy automatycznym zako�czeniu */
                fail_message,       /* Message to write when command failed */
                stop_message,       /* Message to write when paralyze stopped */
                finish_message;     /* Wiadomo�� pokazywana przy automatycznym zako�czeniu */

static int      remove_time,        /* Shall it go away automatically? */
                remove_r_time,      /* Czas usuwania si� parali�u */
                remove_alarm,       /* Id alarmu usuwania si� parali�u */
                block_all;          /* Czy blokowa� wsio */

static object   stop_object,        /* Object to call stop_fun in when stopped */
                finish_object;      /* Obiekt na kt�rym wywo�ywana jest funkcja ko�cz�ca */

static mixed    stop_fun_args,      /* Argumenty do wywo�ania przez stop_funa */
                finish_fun_args;    /* Argumenty do wywo�ania przez funish_funa */

static float    remove_t_time;      /* Czas usuwania aktualnego parali�u */

/* Niekt�re komendy s� zawsze dozwolone i nie ma sposobu na ich usuni�cie. */
#define ALLOWED ({"przyzwij", "odpowiedz", "kto", "wezwij", "przedstawieni", "zapami�tani",     \
                    "zapami�taj", "zapomnij", "sp�jrz", "obejrzyj", "zerknij", "spogl�dnij",    \
                    "sp", "odpowiedz", "dru�yna", "do��cz", "porzu�", "przeka�"})
#define SPECIAL_ALLOWED ({"?", "wezwij", "zg�o�"})

static string *allowed = ALLOWED + ({ "zako�cz", "u�miechnij", "za�piewaj",
            "powiedz","krzyknij", "szepnij", "ski�", "pokiwaj", "pokr��",
            "kiwnij", "skrzyw", "u�miech", "i", "inwentarz"});
/**
 * Automatycznie pozwalamy na wszystkie komendy z \file /cmd/live/info.c i \file /cmd/live/state.c.
 */

static string *special_allowed = SPECIAL_ALLOWED + ({"'"});

/*
 * Prototypes
 */
void set_standard_paralyze(string str);
int stop(string str);
varargs void stop_paralyze();

/**
 * Ustawia jako dost�pne tylko standardowe komendy, kt�rych nie mo�na usun��
 * z allowed, chyba, �e za pomoc� block all
 */
void
standard_allowed()
{
    allowed = ALLOWED;
    special_allowed = SPECIAL_ALLOWED;
}

/**
 * Dodaje komend� do komend mo�liwych do wykonania dla gracza maj�cego
 * na sobie parali�.
 *
 * @param str nazwa komendy
 * @param flag czy jest to komenda, po kt�rej nie nast�puje znak spacji
 */
void
add_allowed(string str, int flag=0)
{
    if(flag)
    {
        if(member_array(str, special_allowed) == -1)
            special_allowed += ({str});
    }
    else
        if(member_array(str, allowed) == -1)
            allowed += ({str});
}

/**
 * Usuwa komend�, kt�ra gracz posiadaj�cy parali� moze wykona�
 *
 * @param str nazwa komendy
 * @param flag czy jest to komenda, po kt�rej nie nast�puje znak spacji
 */
void
remove_allowed(string str, int flag=0)
{
    if(flag)
    {
        if(member_array(str, SPECIAL_ALLOWED) != -1)
            special_allowed -= ({str});
    }
    else
        if(member_array(str, ALLOWED) != -1)
            allowed -= ({str});
}

/**
 * Tworzy i konfiguruje paraliz.
 */
void
create_paralyze()
{
    set_standard_paralyze("paralyze");
}

void init_events()
{
}

/**
 * Konstruktor standarowego obiektu.
 */
nomask void
create_object()
{
    add_prop(OBJ_M_NO_DROP, 1);
    add_prop(OBJ_M_NO_STEAL,1);
    add_prop(OBJ_M_NO_SELL, 1);
    add_prop(OBJ_M_NO_GIVE, 1);

    create_paralyze();

    set_no_show();
}

/**
 * Wywo�ywane gdy spotykamy obiekt.
 */
void
init()
{
    if (remove_time)
    {
        remove_t_time = itof(remove_time) + frandom(remove_r_time,2);
        remove_alarm = set_alarm(remove_t_time, 0.0, stop_paralyze);
    }

    add_action(stop, "", 1);
}

/**
 * Funkcja blokuj�ca komendy.
 *
 * @param str argumenty do komendy.
 *
 * @return 1 komenda zostanie sparali�owana
 * @return 0 komenda nie zostanie sparali�owana
 */
int
stop(string str)
{
    if(!block_all)
    {
        /* parali�ujemy tylko tych kt�rzy maj� parali� w inventory */
        if (environment() != this_player())
            return 0;

        /* je�li komenda jest w allowed to nie parali�ujemy jej */
        if(member_array(query_verb(), allowed) >= 0)
            return 0;

        /* je�li komenda jest w special_allowed to nie parali�ujemy �adnej komendy
        zaczynaj�cej si� od stringa identycznego jak string w special_allowed
        (specjalna tablica do fukcji takich jak: "'", "?"*/
        foreach(string x : special_allowed)
        {
            //Musimy wykluczy� znaki, kt�re wildmatch traktuje jako dowolne
            if(x == "?" && query_verb()[0] != '?')
                continue;
            if(x == "*" && query_verb()[0] != '*')
                continue;
            if(wildmatch(x + "*", query_verb()))
                return 0;
        }

        /**
         * Pozwolimy na komendy z \file /cmd/live/info.c i \file /cmd/live/state.c
         */
        object s;
        s = find_object(CMD_LIVE_INFO);
        if(member_array(query_verb(), m_indexes(s->query_cmdlist())) >= 0)
            return 0;

        s = find_object(CMD_LIVE_STATE);
        if(member_array(query_verb(), m_indexes(s->query_cmdlist())) >= 0)
            return 0;
    }

    //A wizowi jednak pozwolimy.. Jak kliknie dwa razy.. Co jak co ale czasem trza:P
    if(TP->query_prop(WYWOLYWAL_KOMENDE) == query_verb() + " " + str)
        return 0;

    if(!block_all)
    {
        /* je�li jest komenda przerywaj�ca parali�, sprawdzamy j� */
        if (stringp(stop_verb) && (query_verb() ~= stop_verb))
        {
            /* Je�li stop_fun jest zdefiniowana MUSI zwr�ci� 1 aby
                parali� nie zadzia�a� */
            if (objectp(stop_object) && call_other(stop_object, stop_fun, ({str}) + stop_fun_args))
                return 1;

            if (stringp(stop_message))
                this_player()->catch_msg(stop_message);

            remove_object();

            return 1;
        }
    }

    if(TP->query_wiz_level())
    {
        write("WIZ: Jeste� pewny, �e chcesz wywo�a� t� komend�? Potwierd� przez ponowne wpisanie.\n");
        TP->add_prop(WYWOLYWAL_KOMENDE, query_verb() + " " + str);
    }

    /* We allow VBFC, so here we may use catch_msg(). */
    if (stringp(fail_message))
        this_player()->catch_msg(fail_message);

    return 1;
}

/**
 * Function name: set_stop_verb
 * Description  : Set the verb to stop paralyze, if possible.
 * Arguments    : string verb - the verb to use.
 */
void
set_stop_verb(string verb)
{
    stop_verb = verb;
}

/**
 * Function name: query_stop_verb
 * Description  : Return the stopping verb.
 * Returns      : string - the verb.
 */
string
query_stop_verb()
{
    return stop_verb;
}

/**
 * Dzi�ki tej funkcji mo�emy ustawi� funkcje, kt�ra b�dzie wywo�ywana kiedy parali� zostanie
 * przerwany/zako�czony.
 * @param fun nazwa funkcji.
 * @param ... argumenty do funkcji
 */
varargs void
set_stop_fun(string fun, ...)
{
    stop_fun = fun;
    stop_fun_args = argv;
}

/*
 * Function name: query_stop_fun
 * Description  : Returns the function to call when paralyze stops.
 * Returns      : string - the function name.
 */
string
query_stop_fun()
{
    return stop_fun;
}

/**
 * Funkcja wywo�ywana gdy parali� ko�czy si� automatycznie.
 * UWAGA: Do funkcji fun jako pierwszy argument zawsze przesylany jest paralizowany!
 * dopiero nastepne argumenty to argumenty ktore sami podalismy.
 * @param fun funkcja
 * @param ... argumenty do tej funkcji.
 */
public varargs void
set_finish_fun(string fun, ...)
{
    finish_fun = fun;
    finish_fun_args = argv;
}

/**
 * @return nazwe funkcji wywo�ywanej gdy parali� ko�czy si� automatycznie.
 */
string
query_finish_fun()
{
    return finish_fun;
}

/**
 * @return argumenty do funkcji wywo�ywanej gdy parali� si� ko�czy.
 */
mixed
query_finish_fun_args()
{
    return finish_fun_args;
}

/**
 * Ustawiamy obiekt na kt�rym wywo�ywana jest finish_fun.
 *
 * @param ob obiekt.
 */
void
set_finish_object(object ob)
{
    finish_object = ob;
}

/**
 * @return obiekt na kt�rym wywo�ywana jest finish_fun
 */
object
query_finish_object()
{
    return finish_object;
}

/*
 * Function name: set_stop_object.
 * Description  : Set which object to call the stop function in.
 * Arguments    : object ob - the object.
 */
void
set_stop_object(object ob)
{
    stop_object = ob;
}

/*
 * Function name: query_stop_object
 * Description  : Returns which object to call the stop function in.
 * Returns      : object - the object.
 */
object
query_stop_object()
{
    return stop_object;
}

/*
 * Function name: set_fail_message
 * Description  : Set the fail message when player tries to do something.
 *                This supports VBFC and uses this_player().
 * Arguments    : string - the fail message.
 */
void
set_fail_message(string message)
{
    fail_message = message;
}

/*
 * Function name: query_fail_message
 * Description  : Returns the fail message when player tries to do something.
 *                This returns the real value, not resolved for VBFC.
 * Returns      : string - the message.
 */
string
query_fail_message()
{
    return fail_message;
}

/**
 * Function name: set_remove_time
 * Description  : Set how long time player should be paralyzed (in seconds).
 * Arguments    : int time - the time to set.
 */
public varargs void
set_remove_time(int time, int r)
{
    remove_time = time;
}

/*
 * Function name: query_remove_time
 * Description  : Returns the paralyze time (in seconds).
 * Returns      : int - the time.
 */
int
query_remove_time()
{
    return remove_time;
}

float
query_this_remove_time()
{
    return remove_t_time;
}

float
query_remove_time_to_left()
{
    if(!remove_alarm)
        return 0.0;

    mixed ga = get_alarm(remove_alarm);

    if(!pointerp(ga) || sizeof(ga) != 5)
        return 0.0;

    return ga[2];
}

/**
 * @return czas dodawany przez randoma do remove_time.
 */
public int query_remove_r_time()
{
    return remove_r_time;
}

/*
 * Function name: set_stop_message
 * Description  : Set the message written when paralyze stops. This may
 *                support VBFC and uses this_player().
 * Arguments    : string - the message.
 */
void
set_stop_message(string message)
{
    stop_message = message;
}

/*
 * Function name: query_stop_message
 * Description  : Returns the message written when paralyze stops. It returns
 *                the real value, not solved for VBFC.
 * Returns      : string - the message.
 */
string
query_stop_message()
{
    return stop_message;
}

void
set_finish_message(string message)
{
    finish_message = message;
}

string
query_finish_message()
{
    return finish_message;
}

/**
 * T� funkcj� ustawiamy, �e parali� b�dzie blokowa� wszystko co do niego wchodzi.
 * Jest to przeznaczone dla parali�y podczas kt�rych tracimy przytomno��,
 * kt�rych nie mo�na przesta�!
 */
public varargs void set_block_all(int a=1) { block_all = a; }

/**
 * @return czy parali� blokuje wszystko
 * @return 1 blokuje wsio
 * @return 0 nie blokuje wszystkiego
 */
public int query_block_all() { return block_all; }

/*
 * Function name: set_standard_paralyze
 * Description  : Set up standard settings for a paralyze.
 * Arguments    : string str - When the player uses the stop-verb, 'stop',
 *                             the message 'You stop <str>.\n' is printed
 *                             to the player.
 */
void
set_standard_paralyze(string str)
{
    set_stop_verb("przesta�");
/*
    set_stop_fun("stop_paralyze");
    set_stop_object(previous_object());

    Wywoluje runtima too deep recursion, bowiem stop_fun jest wywolywane
    ze stop_paralyze(), a tamtem wywoluje samego siebie - o ile
    set_stop_paralyze() jest wywolywane z samego siebie, z funkcji
    create_paralyze().
 */
    set_stop_message("Przestajesz " + str + ".\n");
    set_fail_message("Jeste� teraz zaj�t"+this_player()->koncowka("y","a")+
        " czym innym. Musisz wpierw przesta� by m�c zrobi�, to co chcesz.\n");
}

/**
 * Funkcja wywo�ana gdy minie czas trwania parali�u.
 */
void
stop_paralyze()
{
    if (!objectp(environment()))
    {
        remove_object();
        return;
    }

    set_this_player(environment());

    if (objectp(finish_object) && stringp(finish_fun))
    {
        try
        {
            call_otherv(finish_object, finish_fun, ({environment()}) + finish_fun_args);
        }
        catch (mixed x)
        {
            filter(users(), &->query_wiz_level())->catch_msg(x);
        }
    }
    else if (strlen(finish_message))
        environment()->catch_msg(finish_message);

    remove_object();
}

/*
 * Function name: stat_object
 * Description  : Function called when wiz tries to stat this object.
 * Returns      : string - the extra information to print.
 */
string
stat_object()
{
    string str = ::stat_object();

    if (strlen(stop_verb))
        str += set_color(31)+"Czasownik ko�cz�cy:"+set_color(0)+" " + stop_verb + "\n";

    if (strlen(stop_fun))
        str += set_color(31)+"Funkcja ko�cz�ca:"+set_color(0)+"  " + stop_fun + "\n";

    if (strlen(stop_message))
        str += set_color(31)+"Wiadomo�� pokazywana gdy ko�czymy:"+set_color(0)+" " + stop_message + "\n";

    if (objectp(stop_object))
        str += set_color(31)+"Obiekt na kt�rym wywo�ywana jest funkcja ko�cz�ca:"+set_color(0)+"  " + file_name(stop_object) + "\n";

    if (strlen(finish_fun))
        str += set_color(31)+"Funkcja wywo�ywana przy automatycznym ko�cu:"+set_color(0)+"  " + finish_fun + "\n";

    if (strlen(finish_message))
        str += set_color(31)+"Wiadomo�� pokazywana przy automatycznym ko�cu:"+set_color(0)+" " + finish_message + "\n";

    if (objectp(finish_object))
        str += set_color(31)+"Obiekt na kt�rym wywo�ywana jest funkcja automatycznego ko�ca:"+set_color(0)+"  " + file_name(finish_object) + "\n";

    if (remove_time)
        str += set_color(31)+"Czas przez jaki parali� b�dzie si� utrzymywa� w graczu:"+set_color(0)+"  " + remove_time + "\n";
    
    if (fail_message)
        str += set_color(31)+"Wiadomo�� pokazywana przy nieudanym wywo�aniu komendy:"+set_color(0)+"  " + fail_message + "\n";

    return str;
}

/**
 * Dodaje event do parali�u.
 * Argumenty do tej funkcji(pierwsze dwa) przyjmuj� VBFC
 *
 * @param ev1 event dla sparali�owanego gracza
 * @param ev2 nieobowi�zkowy event dla reszty graczy
 *            na lokacji, domy�lnie niewy�wietlany
 * @param fun funkcja wykonywana r�wnocze�nie z pokazaniem
 *            eventu.
 */
varargs void add_event(string ev1, string ev2=0, mixed fun=0)
{
    ::add_event(({ev1, ev2}), fun);
}

/**
 * Funkcja pokazuj�ca eventy. Poniewa� ma ona nieco inne dzia�anie ni�
 * standardowa taka funkcja, skierowana do room'a dlatego te�
 * jej dzia�anie trzeba by�o, no c�... Ca�kowicie przerobi�.
 *
 */
void show_event(mixed event)
{
    //Npcom nie pokazujemy.
    if(!interactive(environment()))
        return;

    if(!event)
        return;

    if(stringp(event[0]))
        environment()->catch_msg(event[0] + "\n");
    else if(!pointerp(event[0]) || !sizeof(event[0]))
        return;
    else
    {
        environment()->catch_msg(event[0][0] + "\n");

        if(event[0][1] != 0)
        {
            object prev_player = this_player();
            set_this_player(environment());
            tell_roombb(ENV(environment()), (process_tags(event[0][1]) + "\n"), ({environment()}));
            set_this_player(prev_player);
        }
    }
}

/**
 * Funkcja identyfikuj�ca obiekt jako parali�
 *
 * @return 1 - zawsze
 */
public int
query_paralyze()
{
    return 1;
}

/**
 * Funkcja identyfikuj�ca obiekt jako parali�.
 *
 * @return 1 - zawsze
 */
public int
is_paralyze()
{
    return 1;
}
