/**
 * \file /std/npc.c
 *
 * This is the base for all NPC creatures that need to use combat tools.
 *
 */

#pragma save_binary
#pragma strict_types

inherit "/std/creature.c";

#include <pl.h>
#include <stdproperties.h>
#include <filter_funs.h>
#include <macros.h>
#include <object_types.h>

#define DBG(x) find_player("vera")->catch_msg(x);

string Admin;
int wzywanie_strazy = 0;
int reakcja_na_walke = 0;
string spolecznosc = "";
int byl_donos, ile=0;
int jest_signal_steal=0;    //to do kradziezy z lokacji
int jest_signal_kradziez=0; //a to do kradziezy z ekwipunku
public int spierniczylismy_signal_fight(object atakujacy, object atakowany, object gdzie);
public int alarm_na_signal_fight(object dokogo,object ob, object lok);
public int signal_fight(object atakowany, object gdzie);
public void set_admin(string str);
public object query_admin();
public string set_spolecznosc(string s);
public string query_spolecznosc();

void
create_npc()
{

}

nomask void
create_creature()
{
    create_npc();

    if (!random(3))
        add_leftover("/std/leftover", "z�b", 26+random(6) , 0, 1, 0, 0, O_KOSCI);

    if (!random(2))
        add_leftover("/std/leftover", "czaszka", 1, 0, 1, 1, 0, O_KOSCI);

    if (!random(3))
        add_leftover("/std/leftover", "ko��", random(5), 0, 1, 1, 0, O_KOSCI);

    if (!random(4))
        add_leftover("/std/leftover", "rzepka", 2, 0, 1, 1, 0, O_KOSCI);

    if (!random(4))
        add_leftover("/std/leftover", "�opatka", 2, 0, 1, 1, 0, O_KOSCI);

    if(!query_prop(LIVE_I_UNDEAD))
    {
        if (!random(3))
            add_leftover("/std/leftover", "ucho", 2, 0, 0, 0);

        if (!random(5))
            add_leftover("/std/leftover", "skalp", 1, 0, 0, 1);

        if (!random(3))
            add_leftover("/std/leftover", "paznokie�", random(5) + 1, 0, 0, 0);

        if (!random(2))
            add_leftover("/std/leftover", "serce", 1, 0, 0, 1, 0, O_JEDZENIE);

        if (!random(3))
            add_leftover("/std/leftover", "nos", 1, 0, 0, 0);

        if (!random(2))
            add_leftover("/std/leftover", "nerka", 2, 0, 0, 1, 0, O_JEDZENIE);

        add_leftover("/std/leftover", "oko", 2, 0, 0, 0, 0, O_JEDZENIE);
    }
}

void
reset_npc()
{
    ::reset_creature();
}

nomask void
reset_creature()
{
    reset_npc();
}

/*
 * Description:  Use the combat file for generic tools
 */
public string
query_combat_file()
{
    return "/std/combat/ctool";
}

/*
 * Function name:  default_config_npc
 * Description:    Sets all neccessary values for this npc to function
 */
varargs public void
default_config_npc(int lvl)
{
    default_config_creature(lvl);
}

/** *************************************************************************** **
 **   Tutaj zaczyna sie kod zwiazany z wzywaniem strazy, wysylaniem sygna��w    **
 **                o walce i kradziezy oraz reakcji na nie.                     **
 **                                                                             **
 **                             Vera.                                           **
 ** *************************************************************************** **/

public void
set_wzywanie_strazy(int i)
{
    wzywanie_strazy = i;
}

public int
query_wzywanie_strazy()
{
    return wzywanie_strazy;
}

/**
 * To jest funkcja ustawiaj�ca reakcj� NIE w samym bitym npcu, ale
 * w npcach, kt�re to us�ysz�, �e kto� si� bije.
 * Ustawi� mo�na:
 * 0 - tylko krzyczy, �e si� bij� i przesy�a sygna� dalej, je�li
 * ma query_wzywanie_strazy
 * 1 - przychodzi i pomaga bitemu npcowi
 */
public int
set_reakcja_na_walke(int i)
{
    reakcja_na_walke = i;
}

public int
query_reakcja_na_walke()
{
    return reakcja_na_walke;
}

/*
 * Function name: set_spolecznosc
 * Description:   Funkcja ta ustawia spolecznosc do jakiej nalezy dany
 *                npc. Ustawienie tego jest wymagane jedynie w wypadku
 *                ustawienia reakcji na walke na pomaganie sobie wzajemnie
 *                Dzieki temu npce wiedza komu pomagac, a komu nie.
 * Arguments:     String - nazwa spolecznosci.
 */
public string
set_spolecznosc(string s)
{
    spolecznosc = s;
}

public string
query_spolecznosc()
{
    return spolecznosc;
}

/*
 *Funkcja s�u��ca do udania si� do danej lokacji. Przydatna np. podczas wspierania
 *innych oraz wracania si� p�niej na swoj� lokacj�.
 * Zwraca 0 je�li si� nie uda�o i�� (npce nie potrafi� wywa�a� drzwi)
 */
int
idz_na_lok(object dokad)
{
//DBG("idz na lok: "+file_name(dokad)+"\n");
    mixed droga=znajdz_sciezke(ENV(TO),dokad);
    int i;

    for(i=0;i<sizeof(droga);i++)
    {
        object env = ENV(TO);
        TO->command(droga[i]);
        if (ENV(TO) == env) //nie uda�o si� przej��
        {
            //no to otwieramy
            object door = filter(filter(all_inventory(ENV(TO)),
                    &->is_door()), &operator(==)(droga[i]) @
                    &operator([])(,0) @ &->query_pass_command())[0];
            //door2 to drzwi po drugiej strony
            object door2 = door->query_other_door();
            //door->do_unlock_door("");
            //door2->do_unlock_door("");
            //door->open_door(door->short(0));
            TO->command("otworz "+door->short(PL_BIE));
            //door2->open_door(door2->short(0));

            //ups! Nic tu nie wsk�ramy, wracamy si�.
            if(door->query_locked())
            {
                if(TO->query_prop("ostatnio_bylem_na_lok"))
                {
                    idz_na_lok(TO->query_prop("ostatnio_bylem_na_lok"));
                    TO->remove_prop("ostatnio_bylem_na_lok");
                }
                break;
            }

            //nadal zamkni�te? To pewnie mamy tu do czynienia ze specjaln� komend� otwierania
            //np. 'odsu� kotar�'
            if(!door->query_open())
            {
                string komenda = pointerp(door->query_open_command()) ? door->query_open_command()[0] :
                    door->query_open_command();
                    TO->command(komenda+" "+door->short(PL_BIE));
            }
#if 0
            else
            {
                tell_room(ENV(door2),"Nagle z wielkim hukiem "+door2->short(PL_BIE)+" otwieraj� si� "+
                "wywa�one przez "+QIMIE(straznik,PL_DOP)+"!\n");
            }
#endif
            TO->command(droga[i]);
        }
    }
    if(ENV(TO) == dokad)
        return 1;
    else
        return 0;
}

/* jaki� tam emote po zako�czonej walce. Jestem npcem, kt�ry wspar�.
 * Argument to npc kt�rego wspierali�my.
 */
void
jakis_pierdolnik_na_wyjscie_po_walce(object zkim)
{
    switch(random(8))
    {
        case 0:
            command("klepnij "+zkim->short(PL_DOP)+" w ramie");
            break;
        case 1:
            command("skin na "+zkim->short(PL_BIE));
            break;
        case 2:
            command("westchnij ciezko");
            break;
        case 3:
            command("popatrz ze wspolczuciem na "+zkim->short(PL_BIE));
            break;
        case 3:
            command("skin");
            break;
        default:
            break;
    }
}

/*
 * Funkcja pomocnicza, wywo�uje si� w npcu, kt�ry wspiera drugiego npca
 * to to TO :P bo alarmy ustawiamy na t� funkcj�.
 * Ma na celu sprawdzanie, czy jeszcze toczy si� walka, je�li nie to
 * powr�t na poprzedni� lokacj� na kt�rej dany npc by�
 * zkim to ofiara ataku - npc kt�rego wspieramy
 */
void
zacznij_sprawdzac_czy_jeszcze_walka(object to, object zkim)
{
//DBG("\nzacznij_sprawdzac_czy_jeszcze_walka wywo�uj�cy: "+file_name(to)+" zkim: "+file_name(zkim)+"\n");
    if(to->query_attack() || zkim->query_attack())
    {
        /*ale to jeszcze nie wszystko, �ycie jest brutalne, ma�o kto
         po�wi�ci�by �ycie za drugiego bli�niego :> */
        if(to->query_hp() < 20.0)
        {
            to->command("przestan atakowac");
            to->command("spanikuj");

            if(to->query_prop("ostatnio_bylem_na_lok"))
            {
                to->idz_na_lok(to->query_prop("ostatnio_bylem_na_lok"));
                to->remove_prop("ostatnio_bylem_na_lok");
            }
            else
                to->run_away();

        }
        else
            set_alarm(4.0,0.0,"zacznij_sprawdzac_czy_jeszcze_walka",to,zkim);
    }
    else if(to->query_prop("ostatnio_bylem_na_lok"))
    {
        to->jakis_pierdolnik_na_wyjscie_po_walce(zkim);
        to->idz_na_lok(to->query_prop("ostatnio_bylem_na_lok"));
        to->remove_prop("ostatnio_bylem_na_lok");
    }
}

/*
 * Ucieklismy z pola bitwy. Ustawiamy wiec alarm co chwila sprawdzajac,
 * czy nie wrocilismy do walki
 */
public int
spierniczylismy_signal_fight(object atakujacy, object atakowany,object gdzie)
{
    if (!atakowany || !atakujacy) {
        return 1;
    }
    if(ENV(atakowany) == ENV(atakujacy))
        alarm_na_signal_fight(atakujacy,atakowany,gdzie); //??
    else
    {
        set_alarm(5.0,0.0,"spierniczylismy_signal_fight",atakujacy,atakowany,gdzie);
//        ile++;
    }
    //Nie bedziemy przeciez przestawiali alarmu w nieskonczonosc. Po 20 razach dajemy
    //sobie siana, niech sie dzieje wola boza... Ktos ma lepszy pomysl? :)
//TODO
/*    if(ile>20)
    {

    }*/
}

public int
alarm_na_signal_fight(object dokogo,object ob, object lok)
{
    dokogo->signal_fight(ob, lok, ENV(dokogo));
    return 1;
}

public int
zeruj_donos()
{
    byl_donos=0;
    return 1;
}


void
no_to_wspieramy(object atakowany)
{
    //jezus maria, ja pitole ile ja si� naszuka�em, �eby wykry� w tym b��d!
    //zamiast shorta trzeba query_name. G�wno, dupa, ja pierdole! Ugh :P
    //Jakie query_name, OB_NAME zawsze dzia�a! I to trzeba a nie jakie� query_name!
    command("wesprzyj " + OB_NAME(atakowany));

    //nadal nie wspieramy (np. ze wzgl�du na t�ok) cho� jeste�my na miejscu
    //spr�bujmy wi�c zaatakowa� kogo� z wrogiej dru�yny, je�li jest.
    if(!query_attack())
    { //spr�bujmy zaatakowa� kogo� innego, cz�onka timu agresor�w
        object *reszta = (atakowany->query_attack())->query_team_others();
        if(sizeof(reszta)) //agresor nie jest sam
        {
            command("zabij " + OB_NAME(reszta[random(sizeof(reszta))]));

            //wci�� nie atakujemy - WTF. Tego nie przewidzia�em.
            if(!query_attack())
            {
                command("zaklnij");
                if(query_prop("ostatnio_bylem_na_lok"))
                {
                    idz_na_lok(query_prop("ostatnio_bylem_na_lok"));
                    remove_prop("ostatnio_bylem_na_lok");
                }
            }
        }
        else //wr�g nie jest w �adnej dru�ynie
        {
            //TODO. I co teraz?
            command("westchnij ciezko");
            if(query_prop("ostatnio_bylem_na_lok"))
            {
                idz_na_lok(query_prop("ostatnio_bylem_na_lok"));
                remove_prop("ostatnio_bylem_na_lok");
            }
        }
    }

    set_alarm(2.0,0.0,"zacznij_sprawdzac_czy_jeszcze_walka",TO,atakowany);
}

/**
 * funkcja wywo�ywana jest w obiekcie , kt�ry ma zareagowa�.
 * argumenty:
 *   atakowany to ofiara, na kt�rej zaatakowanie mamy zareagowa�
 *   gdzie to miejsce w kt�rym odbywa si� ten atak
 */
void
reaguj_na_walke(object atakowany, object gdzie)
{
    switch(query_reakcja_na_walke())
    {
        case 0: //donos, krzyk, ze bija, przekazywanie sygnalu dalej. Default.
            if(TO->query_straznik() || TO->query_oficer())
                return;
            if(byl_donos==0 && !query_attack() && is_humanoid())
            {
                switch(random(6))
                {
                    case 0:
                        if(query_wzywanie_strazy())
                            command("emote m�wi: Bij� si�! Stra�e!");
                        else
                            command("krzyknij Na pomoc!");
                        break;
                    case 1:
                        if(query_wzywanie_strazy())
                            command("krzyknij Stra�e! Bij� si�!");
                        else
                            command("powiedz Ech, znowu kogo� t�uk�.");
                        break;
                    case 2:
                        if(query_wzywanie_strazy())
                            command("krzyknij Stra�!");
                        break;
                    case 3:
                        command("powiedz Och! S�yszycie to?! Bij� si�!");
                        break;
                    case 4:
                        if(query_wzywanie_strazy())
                            command("krzyknij Stra�e! Stra�e! Bandyci!");
                        break;
                    case 5:
                        break;
                 }
                 byl_donos=1;
                  set_alarm(7.0,0.0,"zeruj_donos");
            }
            if(query_wzywanie_strazy())
                set_alarm(0.5,0.0,"alarm_na_signal_fight",TO,atakowany,gdzie);

            break;
        case 1:
            //wspieramy tylko npca tej samej spolecznosci co my.
            if(atakowany->query_spolecznosc() ~= TO->query_spolecznosc())
            {
                //je�li nie ma nas na lok. npca do wsparcia:
                if(ENV(atakowany) != ENV(TO))
                {   //zapisujemy lokacje na kt�rej byli�my, �eby�my mogli p�niej tam wr�ci�
                    add_prop("ostatnio_bylem_na_lok",ENV(TO));
                    //i idziemy do npca do wsparcia:
                    idz_na_lok(ENV(atakowany));
                }

                //w dup� znowu g�upi b��d! Tu nie mo�e by� alarmu.
                //set_alarm(itof(random(3)+1),0.0,"no_to_wspieramy",atakowany);
                no_to_wspieramy(atakowany);
            }
        case 2: //RESZTY NIE MA...Tj. uciekanie, jak si� us�yszy walk�
                //dla wyj�tkowo p�ochliwych... TODO
            break;
    }
}

public int
signal_fight(object atakowany, object gdzie)
{
    object atakujacy=atakowany->query_attack();

    //je�li to my jeste�my ofiar�, co c� mamy zrobi�? Ano niewiele mo�emy...
    if(atakowany == TO)
        return 0;
    //nie reagujemy na czyj�� (atakowany) walk�, je�li sami walczymy.
    if(TO->query_attack())
        return 0;
    //tylko m�dre npce s� wspierane, z reszt� dajmy sobie spok�j
    if(!atakowany->query_npc() || atakowany->is_zwierze())
        return 0;
    if(!objectp(atakujacy))
        return 0;

//DBG("WYWO�UJ�CY signal_fight: "+file_name(TO)+" atakowany: "+file_name(atakowany)+
//            " gdzie:"+file_name(gdzie)+"\n");

    //zabezpieczaj�ce sprawdzenie, czy npc nie uciek�
    //gdy� signal_fight jest wywo�ywany z alarmu i co� przez
    //ten czas mog�o si� wydarzy� ju�...
    if(ENV(atakowany) == ENV(atakujacy))
        reaguj_na_walke(atakowany, gdzie);
    else //Ojej, ucieklismy z pola bitwy? :)
        spierniczylismy_signal_fight(atakujacy,atakowany,gdzie);

#if 0
    //hmm, olejmy reszt�, je�li ju� walczymy. Ponadto ze wzgl�du na konstrukcj� tej funkcji
    //wystarczy, �e wykonamy j� tylko dla ofiary, ona ju� przesy�a sygna� dalej.
    if(TO->query_attack() && *atakowany != TO)
        return 1;

    object atakujacy=atakowany->query_enemy();
    if (!atakowany || !atakujacy)
        return 1;

    if(objectp(atakujacy) && ENV(atakowany) == ENV(atakujacy)) //mamy atakuj�cego &&
    {                         //zabezpieczaj�ce sprawdzenie, czy npc nie uciek�
        int i,y;
        //liv to wszystkie npce, kt�re b�d� reagowa�. Wpierw s� to npce z tego samego ENV co ofiara
        object *liv = FILTER_OTHER_LIVE(all_inventory(ENV(TO))) - ({TO});
        //a to s� wszystkie lokacje, w kt�rych b�dziemy jeszcze szuka� reszty npc�w.
        string *lokacje= ENV(TO)->query_exit_rooms();
        //je�li mamy jakie� drzwi (niezamkni�te na klucz!) jeszcze, to dodajemy do lokacje wyj�cia drzwi
        object *doors = filter(filter(all_inventory(ENV(TP)),&->is_door()), &operator(==)(0) @ &->query_locked() );

        foreach(object drzwi : doors)
            lokacje+=({drzwi->query_other_room()});

        //przeszukajmy wszystkie lokacje w poszukiwaniu humanoidalnych npc�w
        for(i=0;i<sizeof(lokacje);i++)
            liv+=filter(filter(FILTER_LIVE(all_inventory(find_object(lokacje[i]))), &->query_npc()), &->is_humanoid() );

        //wykonywane dla ka�dego livinga ka�dej lokacji
        for(y=0;y<sizeof(liv);y++)
            liv[y]->reaguj_na_walke(atakowany, gdzie);
    }
    else if(objectp(atakujacy)) //Ojej, ucieklismy z pola bitwy? :)
        spierniczylismy_signal_fight(atakujacy,atakowany,gdzie);
#endif
    return 1;
}

public void
attacked_by(object ob)
{
    if (TO->query_wzywanie_strazy() && !query_prop(LIVE_M_MOUTH_BLOCKED))
    {
        switch(random(9))
        {
            case 0: command("krzyknij Stra�! Na pomoc!"); break;
            case 1: command("krzyknij Stra�! Pomocy!"); break;
            case 2: command("krzyknij Pomocy, atakuj^a!"); break;
            case 3: command("krzyknij Na pomoc! Bij^a!"); break;
            case 4: command("krzyknij Stra�! Stra^z! Stra^z!"); break;
            default: break;
        }

        if(objectp(query_admin()))
            query_admin()->atak_na_npca(this_object(),ob);
        else
        {//No jak to? Ma wzywanie stra�y ustawione, a Admina nie?! Niemo�eby�!
            write("Wyst�pi� b��d! Zg�o� to natychmiast w postaci, kt�r� w�a�nie "+
                "atakujesz!\n");
        }

        //Wysylanie sygnalu o atakowaniu na sasiednie lokacje
        int i=0,y=0;
        object lok=ENV(this_object()), ob=this_object();
        string *lokacje=lok->query_exit_rooms();
#if 0
        object *liv=FILTER_LIVE(all_inventory(find_object(lokacje[i])));
        object *liv;
#endif
        //Wysylamy do livingow na sasiednich lokacjach:

        for(i=0;i<sizeof(lokacje);i++)
        {
             //Sprawdzamy wpierw, czy sasiednia lokacja jest w ogole zaladowana do pamieci
             if(find_object(lokacje[i]))
             {
#if 0
                 liv=FILTER_LIVE(all_inventory(find_object(lokacje[i])));
                 for(y=0;y<sizeof(liv);y++)
                 {
                     set_alarm(0.5,0.0,"alarm_na_signal_fight",liv[y],ob, lok);
                 }
#endif
                 //Dojebie jeszcze taki maly event. ;)
                 string exit_cmd=ENV(this_object())->query_exit_cmds()[i], str;
                 switch(exit_cmd)
                 {
                     case "wsch�d": str="z zachodu"; break;
                     case "zach�d": str="ze wschodu"; break;
                     case "p�noc": str="z po�udnia"; break;
                     case "po�udnie": str="z p�nocy"; break;
                     case "po�udniowy-wsch�d": str="z p�nocnego-zachodu"; break;
                     case "po�udniowy-zach�d": str="z p�nocnego-wschodu"; break;
                     case "p�nocny-wsch�d": str="z po�udniowego-zachodu"; break;
                     case "p�nocny-zach�d": str="z po�udniowego-wschodu"; break;
                     case "g�ra": str="z do�u"; break;
                     case "d�": str="z g�ry"; break;
                     default: str="z oddali"; break;
                 }
                 tell_room(lokacje[i] ,capitalize(str)+
                           " dochodz^a twych uszu odg^losy walki!\n");
             }
        }
        //Wysylamy do livingow na tej samej lokacji:
        object *liv_here=FILTER_OTHER_LIVE(all_inventory(lok));
        for(i=0;i<sizeof(liv_here);i++)
        {
            //liv_here[i]->signal_fight(ob, lok);
            set_alarm(1.0,0.0,"alarm_na_signal_fight",liv_here[i],ob,lok);
        }

    }
    else //Dla tych, ktorzy nie sa chronieni przez straz... TODO
    {
    }

    ::attacked_by(ob);
}



int
sprawdz_miejsce_obiektu(object co)
{

    //p�ki co sublok nie jest sprawdzany... bo jak?
    if(file_name(ENV(co)) == co->query_orig_place()[0])
        return 1;
    else
        return 0;
}

/*
 * zg�aszamy kradzie�, tj: wpisujemy do obiektu 'co'
 * �e go ukrad� 'kto'. Stra� jak natrafi na ten obiekt, przy
 * najbli�szej rewizji (a robi je na ka�dej lokacji, bo jest zajebista:) )
 * zajmie si� ju� reszt� :)
 */
void
zglos_kradziez(object kto, object co)
{
		string *tab=({ }), dodajemy;
		int prior,prz;
		for(prior=0;prior<=10;prior++)
		{
			int ile_przym=sizeof(kto->query_lista_przymiotnikow2(prior));
			if(ile_przym)
			{
				foreach(string *przyms:kto->query_lista_przymiotnikow2(prior))
					tab+=({przyms[0]});
			}
		}

//DBG("przesy�am argumenty: "+file_name(co)+", "+file_name(kto)+" przymiotniki:\n");
//foreach(string x:tab)
//DBG(x+"\n");

		co->set_who_stole(({kto->query_real_name()}) + tab);
		//co->add_prop(OBJ_I_KRADZIONE,1);
}

void
test_co_zrobil_gracz(object kto, object co)
{
    //czy 'co' wyl�dowa�o tam, gdzie przynale�y...(orig_place)
    if(sprawdz_miejsce_obiektu(co))
    {
//DBG("od�o�one!\n");
        //je�li zd��y� wr�ci� na lokacj� z npcem, to jaki� komunikacik:>
		switch(random(7))
		{
			case 0:
        	set_alarm(1.0, 0.0, "command_present", kto, "powiedz do "+
                           OB_NAME(kto)+" Dzi�kuj�. Prosz� tego wi�cej "+
                            "nie robi�.");
			break;
			case 1:
        	set_alarm(1.0, 0.0, "command_present", kto, "powiedz do "+
                           OB_NAME(kto)+" Dzi�kuj�.");
			break;
			case 2: command("skin"); break;
			case 3: command("m�wi do siebie pod nosem: Ech, z�odziejaszki, "+
					"wszystko by wynie�li.");
					break;
			case 4: command("powiedz No, tak lepiej.");
					break;
			case 5:
                    set_alarm(1.0, 0.0, "command_present", kto, "powiedz do "+
                           OB_NAME(kto)+" I nie r�b tak wi�cej.");
                    break;
            case 6:
                    command("odetchnij z ulga");
                    break;
		}

    }
    else if (ENV(kto) != ENV(TO)) //uciek� ?
    {
//DBG("nieod�o�one! wzywam stra�!\n");
		if(query_wzywanie_strazy())
			command("krzyknij Stra�! Z�odziej!");
		else
			command("krzyknij Z�odziej!");
		zglos_kradziez(kto,co);

    }
    else if(ENV(kto) == ENV(TO)) //nie uciek�, stoi jak ko�ek nadal.
    {
//DBG("stoi jak ko�ek nadal\n");
		if(query_wzywanie_strazy())
	        set_alarm(1.0, 0.0, "command_present", kto, "powiedz do "+
                            OB_NAME(kto)+" Do�� tego! Wzywam stra�!");//?
		else
	        set_alarm(1.0, 0.0, "command_present", kto, "powiedz do "+
                            OB_NAME(kto)+" Do�� tego z�odzieju!");//?
        set_alarm(2.0,0.0,"zglos_kradziez",kto,co);
    }
    else {
//DBG("dupa???\n");
    }
}

void
set_admin(mixed str)
{
    if(objectp(str))
        Admin = MASTER_OB(str);
    else if(stringp(str))
        Admin = str;
}

object
query_admin()
{
    if(!stringp(Admin))
        return 0;

    return find_object(Admin);
}

void
zeruj_signal_steal()
{
    jest_signal_steal=0;
}

void
zeruj_signal_kradziez()
{
    jest_signal_kradziez=0;
}

/*
 * Standardowa reakcja na kradzie�. (tj. zabranie czego� np. z lokacji,
 * a nie z ekwipunku. Z ekwipunku jest signal_kradziez ;P
 */
void
signal_steal(object kto, object co)
{
	if(jest_signal_steal)
		return;

	if(member_array(kto,FILTER_CAN_SEE(FILTER_LIVE(all_inventory(ENV(TO))),
		TO)) == -1)
		return;

	if(query_prop(LIVE_M_MOUTH_BLOCKED))
		return;

	jest_signal_steal=1;
	string co_mowie="";
//DBG("SIGNAL_STEAL, kto: "+kto->query_real_name()+", co: "+co->short());
    if(TO->query_wzywanie_strazy())
    {
        switch(random(6))
        {
            case 0: co_mowie="Od�� "+co->koncowka("m�j","moj�","moje")+
                    " "+co->query_name(PL_BIE)+" na swoje miejsce, bo wezw� "+
                    "stra�!"; break;
            case 1: co_mowie="Hej! Od�� "+co->koncowka("m�j","moj�","moje")+
                    " "+co->query_name(PL_BIE)+" na miejsce, bo wezw� "+
                    "stra�e!"; break;
            case 2: co_mowie="Ej�e! "+co->koncowka("Ten","Ta","To")+
                    " "+co->query_name(PL_MIA)+" jest "+
                    co->koncowka("m�j","moja","moje")+", od�� to na "+
                    "miejsce, w przeciwnym razie wezw� stra�e!"; break;
            case 3: co_mowie="Ej�e! "+co->koncowka("Ten","Ta","To")+
                    " "+co->query_name(PL_MIA)+" jest "+
                    co->koncowka("m�j","moja","moje")+", od�� to na "+
                    "miejsce, w przeciwnym razie wezw� stra�e i si� "+
                    "z tob� porachuj�!"; break;
            case 4: co_mowie="Zostaw "+co->koncowka("ten","t�","to")+
                    " "+co->query_name(PL_BIE)+", bo wezw� stra�e!"; break;
            case 5: co_mowie="Od�� to na swoje miejsce!"; break;
            default: co_mowie="Od�� "+co->koncowka("m�j","moj�","moje")+
                    " "+co->query_name(PL_BIE)+" na swoje miejsce, bo wezw� "+
                    "stra�!"; break;
        }
    }
    else
    {
        switch(random(6))
        {
            case 0: co_mowie="Od�� "+co->koncowka("m�j","moj�","moje")+
                    " "+co->query_name(PL_MIA)+" na swoje miejsce!"; break;
            case 1: co_mowie="Hej! Od�� "+co->koncowka("m�j","moj�","moje")+
                    " "+co->query_name(PL_MIA)+" na miejsce, bo porozmawiamy "+
                    "inaczej!"; break;
            case 2: co_mowie="Ej�e! "+co->koncowka("Ten","Ta","To")+
                    " "+co->query_name(PL_MIA)+" jest "+
                    co->koncowka("m�j","moja","moje")+", od�� to na "+
                    "miejsce, w przeciwnym razie wezw� ziomk�w i si� z "+
					"tob� porachuj�!"; break;
            case 3: co_mowie="Ej�e! "+co->koncowka("Ten","Ta","To")+
                    " "+co->query_name(PL_MIA)+" jest "+
                    co->koncowka("m�j","moja","moje")+", od�� to na "+
                    "miejsce, bo...."; break;
            case 4: co_mowie="Zostaw "+co->koncowka("ten","ta","to")+
                    " "+co->query_name(PL_MIA)+"!"; break;
            case 5: co_mowie="Od�� to na swoje miejsce!"; break;
            default: co_mowie="Od�� "+co->koncowka("m�j","moj�","moje")+
                    " "+co->query_name(PL_MIA)+" na swoje miejsce!"; break;
        }
    }


        //Dajemy graczowi ostrze�enie.
        set_alarm(1.0, 0.0, "command_present", kto, "powiedz do "+
                            OB_NAME(kto)+" "+co_mowie);

        //Sprawd�my, czy poskutkowa�o...
        set_alarm(10.0+itof(random(10)),0.0,"test_co_zrobil_gracz",kto,co);

	set_alarm(2.0,0.0,"zeruj_signal_steal");
}

//funkcja wywo�ywana wtedy, kiedy NPC zaatakuje kogo�. Ta funkcja jest te� w playerze
//i to tam jest obs�uga tego ca�ego szajsu, no, ale jakby kto� kiedy� chcia� doda�
//npcom cu�, no to ma:
public int
prepare_signal_fight()
{
}

/*
 * sygnal ten wysylamy ofierze zlodzieja, a takze tym npcom, ktore zauwazyly
 * kradziez na kims innym. Sygnal ten wysylany jest bez trzeciego argumentu
 * wtedy, kiedy system przesyla go ofierze kradziezy, lecz czy kradziez
 * zostala przez ofiare rozpoznana na czas i czy obiekt zostal skradziony,
 * czy nie - to nalezy sprawdzic, przez porownywanie czy 'co' jest w ENV(TO)
 * jesli !objectp(ofiara).
 * testy czy widzimy zlodzieja itepe juz sa za nami.
*/
void
signal_kradziez(object zlodziej,object *co,object ofiara=TO)
{
    string co_mowie="";
    int ukradli=0; //flaga czy ukradli ;)

    if(!objectp(zlodziej) || !sizeof(co))
        return;

    foreach(object x : co)
        if(ENV(x) != TO)
            ukradli=1;

    //to nam co� kradn�!
    if(ofiara == TO)
    {
        if(jest_signal_kradziez)
            return;

        jest_signal_kradziez = 1;

        //tylko pr�bowali ukra��
        if(!ukradli)
        {
            switch(random(6))
            {
                case 0: co_mowie="Ej�e! Co to ma znaczy�?!"; break;
                case 1: co_mowie="Spr�buj jeszcze raz, a po�a�ujesz!"; break;
                case 2: co_mowie="Na twoim miejscu nie pr�bowa�"+
                        TO->koncowka("bym","abym","bym")+" tego."; break;
                case 3: co_mowie="Co to by�o?"; break;
                case 4: co_mowie="Uwa�aj sobie!"; break;
                case 5:
                    if(zlodziej->query_average_stat()-4 >
                           TO->query_average_stat())
                        break;
                    if(!TO->query_attack())//przeciw cwaniaczkom;]
                        command("zabij "+OB_NAME(zlodziej));
                    break;
            }
            if(strlen(co_mowie))
                command_present(zlodziej,"powiedz do "+OB_NAME(zlodziej)+
                " "+co_mowie);
        }
        else //ukradli!
        {
            if(TO->query_wzywanie_strazy())
            {
                switch(random(4))
                {
                    case 0: co_mowie="Stra�e! Z�odziej!!!"; break;
                    case 1: co_mowie="Z�odziej! Na pomoc!"; break;
                    case 2: co_mowie="Pomocy! Z�odziej!"; break;
                    case 3: co_mowie="Stra�!!!"; break;
                }
                command("krzyknij "+co_mowie);

                foreach(object x : co)
                    zglos_kradziez(zlodziej,x);
            }
            else//nie wzywamy stra�y, musimy wi�c radzi� sobie sami
            {

                //sprawdzmy, czy daliby�my mu rad�, jak du�o silniejszy
                //to nie ryzykujmy �ycia dla swojego ekwipunku...przewa�nie ;]
                if(!random(5) && (zlodziej->query_average_stat()-4 >
                                TO->query_average_stat()))
                {
                    switch(random(3))
                    {
                        case 0: co_mowie="Ty chamie!"; break;
                        case 1: co_mowie="Nie zapomn� ci tego!"; break;
                        case 2: co_mowie="Jeszcze ci si� za to odp�ac�!"; break;
                    }
                    command_present(zlodziej,"powiedz do "+OB_NAME(zlodziej)+
                                " "+co_mowie);
                    if(random(2))
                        command("zacisnij piesci");
                    else if(random(2))
                        command("zagryz wargi");
                    else
                        command("zmruz oczy");

                    return;
                }
                switch(random(5))
                {
                    case 0: co_mowie="Ty z�odzieju!!!"; break;
                    case 1: co_mowie="Zaraz ci poka��!"; break;
                    case 2: co_mowie="Ju� jeste� trup!"; break;
                    case 3..4: co_mowie=""; break;
                }
                if(strlen(co_mowie))
                    command_present(zlodziej,"powiedz do "+OB_NAME(zlodziej)+
                                " "+co_mowie);

                if(!TO->query_attack())//przeciw cwaniaczkom;]
                    command("zabij "+OB_NAME(zlodziej));
            }
        }

    }
    else //kradna innemu npcowi, a my to zauwazylismy
    {
        //mamy w dupie graczy
        if(interactive(ofiara))
            return;

        //mamy w dupie npcow nie z naszego podworka
        if(ofiara->query_spolecznosc() != TO->query_spolecznosc())
            return;

        if(!TO->query_attack())//przeciw cwaniaczkom;]
            return;

        //olejmy to, jesli to byla tylko nieudana proba
        if(!ukradli)
            return;

        /*sprawdzmy, czy daliby�my mu rad�, jak du�o silniejszy
        //to nie nastawiajmy si� dla czyjego� ekwipunku..
        if((zlodziej->query_average_stat() - ofiara->query_average_stat()) >
            TO->query_average_stat())
            return;*/

        switch(random(3))
        {
            case 0:
                set_alarm(1.0,0.0,"command","szepnij "+
                OB_NAME(ofiara)+ "A to z�odziej!"); break;
            case 1: set_alarm(1.0,0.0,"command_present",ofiara,"powiedz do "+
                OB_NAME(ofiara)+ " Z�odziej! Widzia�"+
                ofiara->koncowka("e�","a�","e�")+"?"); break;
            case 2:
                set_alarm(1.0,0.0,"command","powiedz "+
                "Z�odziej!!"); break;
        }

        //poinformujmy o tym ofiare, jesli jeszcze o tym nie wie
        if(jest_signal_kradziez)
            return;

        ofiara->signal_kradziez(zlodziej,co);
    }

    set_alarm(2.0,0.0,"zeruj_signal_kradziez");
}
