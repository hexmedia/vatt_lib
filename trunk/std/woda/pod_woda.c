/*
 * Lokacja pod wod�, czyli gdzie� pomi�dzy dnem a powierzchni�
 *
 *
 * Vera.
 */

#include <macros.h>
#include <composite.h>
#include <stdproperties.h>
#include <ss_types.h>
#include <woda.h>
#include <exp.h>

inherit "/std/room";
inherit "/std/woda/lok_wody";
object powierzchnia, pod_woda, gora; //gora mo�e by� powierzchni�, gdy poziom jest tylko 1

void
create_pod_woda()
{
}

void
create_room()
{
    
    set_polmrok_long("Ciemne wody jeziora otaczaj� ci� ze wszech stron. Wszystkie tajemnice tego miejsca"+
    " zosta�y oblepione przez p�mrok, kt�ry pozwala jedynie na dostrze�enie ledwo rysuj�cej si� "+
    "powierzchni tych w�d gdzie� nad tob�.\n");
    
    add_prop(ROOM_I_TYPE, ROOM_UNDER_WATER);
    add_prop(ROOM_I_HIDE,-1);
    add_prop(ROOM_M_NO_STEAL,1);
    add_prop(ROOM_CANT_THROW,1);
    //kto� mo�e pomy�le�, �e wtedy woda b�dzie azylem, ale to nieprawda, bo w wodzie
    //b�dziemy si� szybko m�czy�....
    add_prop(ROOM_M_NO_ATTACK, 1);
    add_prop(ROOM_I_INSIDE, 1);
    
    //gar�� zwyk�ych event�w
    set_event_time(280.0);
    add_event("Ledwie s�yszalne, niezidentyfikowane odg�osy dobiegaj� ci� z g�ry.\n");
    add_event("Co� zimnego i paskudnie ob�lizg�ego otar�o si� o ciebie.\n");
    add_event("D�wi�ki, takie jak szumy i bulgotanie rozlegaj� si� tu co chwil�.\n");
    add_event("Robi si� przyjemniej, gdy wp�ywasz na cieplejszy pr�d.\n");
    add_event("Przez wod�, kt�ra zewsz�d ci� otacza, czujesz si� jak w �onie matki.\n");
    //add_event("
    if(random(2))
        add_event("Co� organicznego pog�aska�o ci� po r�ce, zapewne jakie� wodorosty.\n");
    else
        add_event("Co� organicznego pog�aska�o ci� po nodze, zapewne jakie� wodorosty.\n");
    
    
    create_pod_woda();
}


//ta funkcja jest wywo�ywana w podawaniu longa lokacji podwodnej i wy�wietla czy widzimy na g�rze ��dki i ludzi
string
czy_cos_na_gorze()
{
    if(!objectp(powierzchnia))
        return "";
    else
    {
        object *lil = ({ });
       foreach(object l : AI(powierzchnia))
 //       foreach(object l : AI(TO))
            if(l->query_lodka() || l->is_living())
                lil += ({l});
            
        if(sizeof(lil))
            return " Tu� nad sob� dostrzegasz "+
            /*(sizeof(lil)>1?"j� ":" ") +*/COMPOSITE_DEAD(lil,PL_MIA) +".";
    }
    
    return "";
}

void
set_powierzchnia(object x)
{
    powierzchnia = x;
}
object
query_powierzchnia()
{
    return powierzchnia;
}
void
set_dol(object x)
{
    pod_woda = x;
}
object
query_dol()
{
    return pod_woda;
}

void
set_gora(object x)
{
    gora = x;
}
object
query_gora()
{
    return gora;
}

void
koniec_paralizu_wynurz(object *x, object kto)
{    
    //i zm�cz:
    zmecz(kto,1);
    
    if(query_prop(POZIOM) == 1)
        tell_roombb(ENV(kto),QCIMIE(kto,PL_MIA)+" wyp�ywa na powierzchni�.\n",
               ({kto}),kto);
    else
        tell_roombb(ENV(kto),QCIMIE(kto,PL_MIA)+" wyp�ywa na g�r�.\n",
               ({kto}),kto);
    
    kto->move(query_gora(), 1);
    
    if(!query_gora()->query_prop(POZIOM))
        tell_roombb(ENV(kto),QCIMIE(kto,PL_MIA)+" wyp�ywa na powierzchni�.\n",
               ({kto}),kto);
    else
        tell_roombb(ENV(kto),QCIMIE(kto,PL_MIA)+" wynurza si� na powierzchni�.\n",
               ({kto}),kto);
               
    kto->do_glance(1);
    
    //eexpik tylko na nurkowanie jest. one way only ;p
}


int
wynurz(string str)
{
    notify_fail("Wynurz si�?\n");
    
    if((query_verb() ~= "g�ra" && !strlen(str)) ||
       (query_verb() ~= "wynurz" && str ~= "si�") ||
       (query_verb() ~= "wyp�y�" && str ~= "na powierzchni�"))
    {
        //powa�ny b��d, mo�e kosztowa� �ycie gracza...
        //mimo to, niepowinien on nigdy nast�pi�. :>>
        if(!objectp(query_powierzchnia())) //nigdy nigdy to nie powinno si� przytrafi�!
        {
            write("Wyst�pi� nieoczekiwany b��d nr 239cd. Nie mog� znale�� powierzchni "+
            "jeziora. Prosz� zg�o� to, wraz z okoliczno�ciami w jakich do tego dosz�o.\n");
            notify_wizards(TP, "Ten gracz zaraz si� utopi w "+file_name(ENV(TP))+" z powodu b��du "+
            "nr 239cd! Przenie� go szybko gdzie� i napraw b��d!\n");
            return 1;
        }
        
        //czy mamy jeszcze na to si��?
        if(TP->query_fatigue() <= LIMIT_ZMECZENIA_NA_WYPLYWANIE)
        {
            write("Pr�bujesz wyp�yn�� na g�r�, lecz opadasz z si�!\n");
            return 1;
        }
        
        //czas p�yni�cia: 1/2si�y + um p�ywania
        int czas = query_czas();
    
        object p=clone_object("/std/paralyze.c");
        p->set_finish_object(TO);
        p->set_finish_fun("koniec_paralizu_wynurz", TP);
        p->set_remove_time(czas);
        p->set_stop_verb("przesta�");
        p->set_stop_message("Przestajesz p�yn�� w g�r�.\n");
        p->set_fail_message("Chwil�! W�a�nie pr�bujesz p�yn�� w kierunku powierzchni!\n");
        p->move(TP,1);
    
        if(query_prop(POZIOM) == 1)
        {
            write("Zaczynasz p�yn�� na powierzchni�.\n");
            saybb(QCIMIE(TP,PL_MIA)+" zaczyna p�yn�� na powierzchni�.\n");
        }
        else //jeste�my g��biej!
        {
            write("Zaczynasz p�yn�� do g�ry.\n");
            saybb(QCIMIE(TP,PL_MIA)+" zaczyna p�yn�� do g�ry.\n");
        }
        
        return 1;
    }
    
    return 0;
}


void
enter_inv(object ob, object from)
{
    ::enter_inv(ob,from);
    
    wspolne_enter_inv(ob, from);
}

void
leave_inv(object ob, object to)
{
    ::leave_inv(ob,to);
    
    if(!ob->is_living())
        return; 
    
    //gagatek wychodzi z wody?
    if(is_lok_wody(to))
        return;
    
    
    //wi�c jednak ju� sie nie pluskamy:
    
    ob->remove_prop(TONIE);//????? FIXME <-to usuwamy w alarmie. �eby jeszcze ostatni raz si� wywo�a�a funkcja toniemy i tam pokaza� komunikat
    //�e, 'ufff uda�o ci si� wyj�� z tego ca�o'
    ob->remove_prop(IL_POWIETRZA);
    ob->remove_prop(IL_POWIETRZA_MAX);
    
    //usuwamy alarm i prop na p�ywanie:
    (ob->query_prop(PIERWSZA_ALARM_LOK))->usun_alarm(ob->query_prop(PLYWAM_ALARM));
    ob->remove_prop(PLYWAM_ALARM);
    
    //i na redukcj� powietrza:    <--czemu to zrobi�em? przecie� wtedy nie b�dzie super eventa o nabieraniu powietrza
                                    //przy wynurzaniu na powierzchni�! A poza tym w funkcji redukuj_powietrze samo sie usunie...
    //(ob->query_prop(PIERWSZA_ALARM_LOK))->usun_alarm(ob->query_prop(POWIETRZE_ALARM));
    //ob->remove_prop(POWIETRZE_ALARM);
    
    ob->remove_prop(LIVE_S_EXTRA_SHORT);
    ob->remove_prop(SUMA_GESTOSCI_EKW);
    ob->remove_prop(PIERWSZA_ALARM_LOK);
}

void
koniec_paralizu(object *co,object kto, object cel, string kierunek, int str_mod)
{
    tell_roombb(ENV(kto),QCIMIE(kto,PL_MIA)+" p�ynie na "+kierunek+".\n",
               ({kto}),kto);    
    
    string skad = " nik�d";
    skad = zmien_kierunek(kierunek);
    
    //kto->add_fatigue(-(25*str_mod + random(10)));
    zmecz(kto,0);
    
    kto->move(cel,1);
    
    /*if(!ENV(kto)->query_prop(ROOM_IN_WATER) &&
        !ENV(kto)->query_prop(ROOM_UNDER_WATER))
    {
        kto->write("Wychodzisz z wody.\n");
        tell_room(ENV(kto),QCIMIE(TP,PL_MIA)+" wychodzi z wody.\n", ({kto}));
        object przemoczenie = clone_object("/std/efekty/przemoczenie.c");
        przemoczenie->przemocz(kto);
    }
    else*/
        tell_roombb(cel,QCIMIE(TP,PL_MIA)+" przyp�ywa z"+skad+".\n", ({kto}),kto);
    
    kto->do_glance(1);
    
}

public int
kierunki(string str) //ta funkcja w zasadzie to wy�apuje wszystkie komendy wpisywane pod wod�
{
    string cmd = query_verb();
    int i = -1,
        ob_flag = 0;
        
    //je�li komenda to kt�ra� z tych zakazanych, to nie da rady...
    if(member_array(cmd,ZAKAZANE_POD_WODA) != -1)
    {
        write("Jeste� teraz pod wod�, jak�e zatem m"+TP->koncowka("�g�by�","og�aby�")+
        " to uczyni�?\n");
        return 1;
    }

    if(cmd ~= "p�y�" && strlen(str) > 3 && str[0..1] ~= "na")
        cmd = str[3..];
    //trzeba zmieni� te� 'sp na kierunek':
DBG("str='"+str+"'");
//write("str='"+str+"'");
    if((cmd ~= "sp" || cmd ~= "sp�jrz" || cmd ~="ob" || cmd~="obejrzyj") && (!str ||str == "0"))
    {
        //zwyk�e sp na lokacji na kt�rej jeste�my.
        return 0;
//         if((cmd != "ob" && cmd !="obejrzyj") && strlen(str) > 3)
//             cmd = str[3..];
    }

    
    //trzeba zmienic skr�ty na pe�ne nazwy:
    switch(cmd)
    {
        case "n": case "polnoc": cmd = "p�noc"; break;
        case "ne": case "polnocny-wschod": cmd = "p�nocny-wsch�d"; break;
        case "nw": case "polnocny-zachod": cmd = "p�nocny-zach�d"; break;
        case "e": case "wschod": cmd = "wsch�d"; break;
        case "se": case "poludniowy-wschod": cmd = "po�udniowy-wsch�d"; break;
        case "s": case "poludnie": cmd = "po�udnie"; break;
        case "sw": case "poludniowy-zachod": cmd = "po�udniowy-zach�d"; break;
        case "w": case "zachod": cmd = "zach�d"; break;
        case "p�noc":
        case "p�nocny-wsch�d":
        case "wsch�d":
        case "po�udniowy-wsch�d":
        case "po�udnie":
        case "po�udniowy-zach�d":
        case "zach�d":
        case "p�nocny-zach�d":
            break;
    }

    //r�nica w tym kodzie a kodzie z woda_powierzchnia jest taka, �e
    //tutaj musimy sprawdza� po powierzchni wyj�cia i klonu pod_woda kierunku...
    i = member_array(cmd,query_powierzchnia()->query_exit_cmds());
    
    if(i == -1)
        cmd = "dupa";
    
    if(query_verb() ~= "sp" || query_verb() ~="sp�jrz" || query_verb() ~= "ob" || query_verb() ~= "obejrzyj")
        ob_flag = 1;
    
//  write("cmd="+cmd+"\nstr="+str+"\n");    
     //sp na kierunek  - pod wod� mo�emy patrze� tylko na g�r�.
     if(ob_flag && cmd == "dupa" && (str ~= "na g�r�" || str ~="u" || str~="g�r�" || str~="na u"))
     {   
        write("Spogl�dasz na g�r�:\n");
        saybb(QCIMIE(TP,PL_MIA)+" spogl�da na g�r�.\n");
        TP->daj_paraliz_na_sp("NONE",file_name(query_powierzchnia()));
        return 1;
     }
     
//  write("cmd="+cmd+"\n");    
     //if wpisywana komenda to �adne z wyj�� dost�pnych na powierzchni :
     //lub nie ma takiego poziomu na lokacji obok...
    if(cmd == "dupa"  || query_powierzchnia()->query_prop(IL_POZIOMOW) < query_prop(POZIOM))
        return 0;
    
    
    
    if(TP->query_fatigue() <= LIMIT_ZMECZENIA_NA_PRZEPLYWANIE)
    {
        write("Nie masz ju� si�.\n");
        return 1;
    }
    
    LOAD_ERR(query_powierzchnia()->query_exit_rooms()[i]);
    object lok = find_object(query_powierzchnia()->query_exit_rooms()[i]);
    if(lok->query_prop(ROOM_I_TYPE) != ROOM_IN_WATER &&
        lok->query_prop(ROOM_I_TYPE) != ROOM_BEACH)
    {
        write("Nie mo�esz tam pop�yn��.\n");
        return 1;
    }
    
    
    //no to wyszukajmy teraz interesuj�ca nas lokacj� podwodn�
    object cel = znajdz_cel(lok);
//DBG("liczba poziomow celu: "+lok->query_prop(IL_POZIOMOW)+"\n");
//DBG("celem jest: "+file_name(cel)+"\n");
    
    
    //czas p�yni�cia: 1/2si�y - um p�ywania
    int czas = query_czas();
    
    object p=clone_object("/std/paralyze.c");
    p->set_finish_object(TO);
    p->set_finish_fun("koniec_paralizu", TP, cel, 
                        query_powierzchnia()->query_exit_cmds()[i], czas);
    p->set_remove_time(czas);
    p->set_stop_verb("przesta�");
    p->set_stop_message("Przestajesz p�yn��.\n");
    p->set_fail_message("Chwil�! W�a�nie dok�d� p�yniesz!\n");
    p->move(TP,1);
    
    cmd = "na " + cmd;
// dump_array(query_exit_rooms());
// DBG(i);
    //jeszcze sprawd�my, czy jest specjalny opis wyj�cia lokacji
//     cmd = sprawdz_specjalny_opis_wyjscia(cmd, query_exit_rooms()[i]);
        
    write("Zaczynasz p�yn�� "+cmd+".\n");
    saybb(QCIMIE(TP,PL_MIA)+" zaczyna p�yn�� "+cmd+".\n");
    
    return 1;
}

void
koniec_paralizu_na_nurkowanie(object *x, object kto, int czas)
{
    //i zm�cz:
    zmecz(kto,1);
    
    kto->move(query_dol(),1);
        
    kto->catch_msg("Nurkujesz jeszcze g��biej.\n");    
    tell_room(ENV(kto), QCIMIE(kto,PL_MIA)+" przyp�ywa z g�ry.\n", ({kto}));
    kto->do_glance(1);
    
    if(!kto->query_prop(PIERWSZA_ALARM_LOK))
        kto->add_prop(PIERWSZA_ALARM_LOK, TO);
    
    /*if(!czy_jest_alarm(POWIETRZE_ALARM, kto))
            kto->add_prop(POWIETRZE_ALARM,
                 (kto->query_prop(PIERWSZA_ALARM_LOK))->set_alarm(1.0,0.0,"redukuj_powietrze",kto));*/
                          
    //expik? :)
    kto->increase_ss(SS_SWIM, EXP_NURKUJE_SWIM);
    kto->increase_ss(SS_CON, EXP_NURKUJE_CON);
    kto->increase_ss(SS_DEX, EXP_NURKUJE_DEX);
}

int
zanurkuj(string str)
{
    if(strlen(str))
        return 0;
    
    //czy mamy jeszcze na to si��?
    if(TP->query_fatigue() <= LIMIT_ZMECZENIA_NA_NURKOWANIE)
    {
        write("Pr�bujesz zanurkowa�, lecz okazuje si�, �e nie starczy ci ju� na to si�.\n");
        return 1;
    }
    
    //ustawiamy d� dla TO, a d� od powierzchni to albo jeszcze pod_woda, albo ju� 'dno'.
    //ta funkcja sama to sprawdzi i obliczy.
    ustaw_dol_jesli_nie_ma(TO, query_prop(POZIOM) + 1);
    
    
    int czas = query_czas();
    object p=clone_object("/std/paralyze.c");
    p->set_finish_object(TO);
    p->set_finish_fun("koniec_paralizu_na_nurkowanie", TP, czas);
    p->set_remove_time(czas);
    p->set_stop_verb("przesta�");
    p->set_stop_message("Przestajesz nurkowa�.\n");
    p->set_fail_message("Chwil�! W�a�nie pr�bujesz da� nura!\n");
    p->move(TP,1);
    
        
    write("Zaczynasz nurkowa� jeszcze g��biej.\n");
    saybb(QCIMIE(TP,PL_MIA)+" zaczyna nurkowa�.\n");
    
    return 1;
}

string
query_pomoc()
{
    return "Jeste� obecnie pod wod�, jednak mo�esz porusza� si� (p�ywa�)"+
    "na strony niczym na l�dzie. Mo�esz tak�e spr�bowa� tutaj zanurkowa� "+
    "jeszcze g��biej, lub wyp�yn�� na powierzchni�.\n";
}

init()
{
    ::init();
    add_action(kierunki, "", 1);
    add_action(zanurkuj,"zanurkuj");
    add_action(zanurkuj,"nurkuj");
    add_action(zanurkuj,"d�");
    
    
    add_action(wynurz,"wynurz");
    add_action(wynurz,"g�ra");
    add_action(wynurz,"wyp�y�");
}