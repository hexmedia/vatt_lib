/**
 * samo dno! :O
 * mo�na spr�bowa� je przeszuka�...
 *poza tym, dno o poziomie > 2 jest ciemnym miejscem.
 * Hmmm. no i nie mo�na p�yn�� ni�ej wiadomo! :)
 *
 * kiedy� jak b�dzie trzeba, to mo�na przerobi�, �eby lokacje 'pod wod�' i 'dna' klonowa�y
 * si� z ustalonej w lokacji powierzchni �cie�ki, dzi�ki czemu dno jeziora i dno rzeki
 * mia�yby inne opisy, w�a�ciwo�ci itepe.
 * P�ki mamy jedno tylko jeziorko, to tego nie robi�em.
 *
 * @author Vera.
 * @date 20.07.2009
 */

#include <macros.h>
#include <composite.h>
#include <stdproperties.h>
#include <ss_types.h>
#include <filter_funs.h>
#include <woda.h>


inherit "/std/room";
inherit "/std/woda/lok_wody";
object powierzchnia, pod_woda, gora; //gora mo�e by� powierzchni�, gdy poziom jest tylko 1
string dno();
string rakowy();
int swiatelko(); //dno jak jest poziomem > od 2 to jest ciemne

int tajm_ostatniego_szukania_rakow = 0; // :)

void
create_pod_woda()
{
}

void
create_room()
{   
    //p�mrok, czyli opis gdy na zewn�trz jest noc.
    set_polmrok_long("Z absolutnie �adnej strony "+
    "nie dostrzegasz cho�by najmniejszego �wiate�ka. Jedynym punktem odniesienia jest "+
    "dno, kt�re wyczuwasz po omacku.\n");
    //opis, zamiast 'Ciemne miejsce.':
    add_prop(ROOM_S_DARK_LONG, "Wszelkie promienie �wietlne gin� w mrocznej g��binie. "+
        "Jedynym punktem odniesienia jest dno, kt�re wyczuwasz po omacku.\n");
    add_prop(ROOM_I_TYPE, ROOM_UNDER_WATER);
    add_prop(ROOM_I_HIDE,-1);
    add_prop(ROOM_M_NO_STEAL,1);
    add_prop(ROOM_CANT_THROW,1);
    //kto� mo�e pomy�le�, �e wtedy woda b�dzie azylem, ale to nieprawda, bo w wodzie
    //b�dziemy si� szybko m�czy�....
    add_prop(ROOM_M_NO_ATTACK, 1);
    add_prop(ROOM_I_INSIDE, 1);
    
    //sami opisujemy przy przeszukiwaniu dna rzeczy.
    add_prop(CONT_I_DONT_SHOW_CONTENTS,1);
    
    add_prop(ROOM_I_LIGHT, &swiatelko()); //dno jak jest poziomem > od 2 to jest ciemne
    add_item("dno","@@dno@@");
    
    //gar�� zwyk�ych event�w
    set_event_time(280.0);
    add_event("Od wysokiego ci�nienia zaczyna dokucza� ci b�l w uszach.\n");
    add_event("Co� zimnego i paskudnie ob�lizg�ego otar�o si� o ciebie.\n");
    add_event("D�wi�ki, takie jak szumy i bulgotanie rozlegaj� si� tu co chwil�.\n");
    add_event("Czujesz, �e masz tu do czynienia z jak�� tajemnicz� i nieopisan� "+
                "faun� i flor�.\n");
    add_event("Ogarniaj� ci� dreszcze. Pr�dy s� tu znacznie ch�odniejsze ni� "+
                "raptem par� �okci wy�ej.\n");
                
    add_event("@@rakowy:"+file_name(TO)+"@@");
    add_event("@@rakowy:"+file_name(TO)+"@@");
    
    if(random(2))
        add_event("Co� organicznego pog�aska�o ci� po r�ce, zapewne jakie� wodorosty.\n");
    else
        add_event("Co� organicznego pog�aska�o ci� po nodze, zapewne jakie� wodorosty.\n");
    
    
    
    create_pod_woda();
}

int //dno jak jest poziomem > od 2 to jest ciemne
swiatelko()
{
     if(query_prop(POZIOM) > 2)
         return 0;
     else
         return 1;
}

string
dno()
{
    string str = "";
    
    str += "Ledwie dostrzegalne piaszczyste dno jeziora zarastaj� "+
    "liczne wodorosty, w�r�d kt�rych, gdyby� si� przyjrza�"+
    TP->koncowka("","a")+" na pewno z �atwo�ci� dostrzeg�"+TP->koncowka("by�","aby�")+
    " inne bogactwa fauny i flory. "+
    "Grz�ski jasnoszary mu� stanowi niedu�� cz�� pokrycia dna. Dostrzegasz tu przede"+
    " wszystkim piasek, na kt�rym gdzieniegdzie zalegaj� rozk�adaj�ce si� szcz�tki ro�linne.\n";
    
    return str;
}

string
rakowy() //event
{
    if(!powierzchnia->query_prop(SA_RAKI))
        return "";
    
    string str = "";
    
    if(query_prop(ROOM_I_LIGHT))
        switch(random(3))
        {
            case 0 : str = "Po piaszczystym dnie przechadza si� rak.\n"; break;
            case 1 : str = "Jakie� zwierz� schowa�o si� do norki, czy�by rak?.\n"; break;
            case 2 : str = "W�saty rak przeszed� w�a�nie niedaleko ciebie odgra�aj�c "+
                        "si� swymi szczypcami.\n"; break;
        }
    
    return str;
}

/*
 * Nazwa funkcji : search_fun
 * Opis          : Sprawdza, czy graczowi udalo sie, poza ukrytymi w obiekcie
 *                 obiektami, znalezc cos interesujacego.
 * Argumenty     : string str - argument komendy
 *                 int trail  - uzyta komenda: 0, gdy 'szukaj',
 *                                             1, gdy 'przeszukaj'.
 * Zwraca        : Komunikat, ktory ma byc wyswietlony graczowi, lub 0, jesli
 *                 nie udalo mu sie znalezc niczego interesujacego.
 * Uwaga         : Jesli maskujesz te funkcje, zamiast zwracac 0, wywoluj jej
 *                 oryginalna wersje. (return ::search_fun(str, trail);)
 */
static string
search_fun(string str, int trail)
{
    string rak = powierzchnia->query_prop(SA_RAKI);
    int ile_rakow = 0;
    
    if(rak) //czy tu maj� by� raki.
        ile_rakow = random(3) - 1;
    
    if(str ~= "dno" && trail == 1) //czyli if wpisali�my 'przeszukaj dno'
    {
        object *inv = FILTER_DEAD(AI(ENV(TP)));
        
        if(!sizeof(inv))
        {
            //write("Nie udalo ci sie znalezc niczego interesujacego.\n");
        }
        else   //to co lezy na dnie widoczne tylko tutaj.
        {
            write("Na dnie znajdujesz "+COMPOSITE_DEAD(inv,PL_BIE)+
                    ".\n");
            return "";
        }
    }
    
    if(str ~= "rak�w" && !trail)
    {
        //tj: je�li ostatnio szukali�my w mniejszym odst�pie ni� 5-15 sek.
        if(ile_rakow <= 0 ||
            time() - 120 - random(100) < tajm_ostatniego_szukania_rakow)
        {
            write("Nie znajdujesz tu �adnych rak�w.\n");
            return "";
        }
        else
        {
            tajm_ostatniego_szukania_rakow = time();
            object *raki = ({});
            for(int i = 0 ; i < ile_rakow ; i++)
                raki += ({clone_object(rak)});
            write("Udaje ci si� zebra� "+COMPOSITE_LIVE(raki,PL_BIE)+".\n");
            foreach(object x : raki)
                x->move(TP, 1);
            return "";
        }
        
        
    }
    
    return ::search_fun(str, trail);
}

/*
void
search_now(object searcher, string str, int trail)
{
    string *x, *cp;
    
    if (member_array(lower_case(TP->query_name()), explode(read_file(WZIELI_LIST), "#")) == -1)
    {
    TP->catch_msg("W^sr^od przegni^lych resztek tkaniny odnajdujesz stary przemoczony list.\n");
    clone_object(LIST)->move(this_player());                                                    
    
    cp = explode(read_file(ROBIACY), "#");
    if (member_array(lower_case(TP->query_name()), cp) != -1)
    {
        x = explode(cp[member_array(lower_case(TP->query_name()), cp) + 1], "");
        if (x[6] != "1")                                                        
        {
            x[6] = "1";
            cp[member_array(lower_case(TP->query_name()), cp) + 1] = implode(x, "");
            write_bytes(ROBIACY, 0, implode(cp, "#"));                              
         // TP->catch_msg(set_color(42) + "***CHECKPOINT 7***\n" + clear_color());  
        }
    }
    write_file(WZIELI_LIST, lower_case(TP->query_name()) + "#");
    //TP->catch_msg("panie prezesie, zadanie wykonane\n");      
    }
    else
       TP->catch_msg("Nie uda^lo ci si^e znale^x^c niczego interesuj^acego.\n");
}

void
search_object(string str, int trail)
{
    int time, search_alarm;
    object obj;            

    time = query_prop(OBJ_I_SEARCH_TIME) + 5;
    if (time < 1)
        search_now(this_player(), str, trail);
    else
    {
        remove_alarm(search_alarm);
        search_alarm = set_alarm(itof(time), 0.0,
                                 &search_now(this_player(), str, trail));
        seteuid(getuid(this_object()));
        obj = clone_object("/std/paralyze");
        obj->set_standard_paralyze(trail ? "przeszukiwa^c "
          + (query_prop(ROOM_I_IS) ? str :
                this_object()->short(this_player(), PL_BIE)) :
            "szuka^c" + (str ? " " + str : ""));
        obj->set_stop_fun("stop_search");
                obj->set_stop_object(this_object());
        obj->set_remove_time(time);
        obj->move(this_player(), 1);
    }
}
*/







int
jestem_dnem() // ;(((((((((((((
{
    return 1;
}

void
set_powierzchnia(object x)
{
    powierzchnia = x;
}
object
query_powierzchnia()
{
    return powierzchnia;
}
void
set_pod_woda(object x)
{
    pod_woda = x;
}
object
query_pod_woda()
{
    return pod_woda;
}
void
set_gora(object x)
{
    gora = x;
}
object
query_gora()
{
    return gora;
}

void
koniec_paralizu_wynurz(object *x, object kto)
{    
    //i zm�cz:
    zmecz(kto,1);
    
    if(query_prop(POZIOM) == 1)
        tell_room(ENV(kto),QCIMIE(kto,PL_MIA)+" wyp�ywa na powierzchni�.\n",
               ({kto}));
    else
        tell_room(ENV(kto),QCIMIE(kto,PL_MIA)+" wyp�ywa na g�r�.\n",
               ({kto}));
    
    kto->move(query_gora(), 1);
    
    if(!query_gora()->query_prop(POZIOM))
        tell_room(ENV(kto),QCIMIE(kto,PL_MIA)+" wyp�ywa na powierzchni�.\n",
               ({kto}));
    else
        tell_room(ENV(kto),QCIMIE(kto,PL_MIA)+" przyp�ywa z do�u.\n",
               ({kto}));
               
    kto->do_glance(1);
}


int
wynurz(string str)
{
    notify_fail("Wynurz si�?\n");
    
    if((query_verb() ~= "g�ra" && !strlen(str)) ||
       (query_verb() ~= "wynurz" && str ~= "si�") ||
       (query_verb() ~= "wyp�y�" && str ~= "na powierzchni�"))
    {
        //powa�ny b��d, mo�e kosztowa� �ycie gracza...
        //mimo to, niepowinien on nigdy nast�pi�. :>>
        if(!objectp(query_powierzchnia()))
        {
            write("Wyst�pi� nieoczekiwany b��d nr 239cd. Nie mog� znale�� powierzchni "+
            "jeziora. Prosz� zg�o� to, wraz z okoliczno�ciami w jakich do tego dosz�o.\n");
            return 1;
        }
        
        //czy mamy jeszcze na to si��?
        if(TP->query_fatigue() <= LIMIT_ZMECZENIA_NA_WYPLYWANIE)
        {
            write("Pr�bujesz wyp�yn�� na g�r�, lecz opadasz z si�!\n");
            return 1;
        }
        
        //czas p�yni�cia: 1/2si�y + um p�ywania
        int czas = query_czas();
    
        object p=clone_object("/std/paralyze.c");
        p->set_finish_object(TO);
        p->set_finish_fun("koniec_paralizu_wynurz", TP);
        p->set_remove_time(czas);
        p->set_stop_verb("przesta�");
        p->set_stop_message("Przestajesz p�yn�� w g�r�.\n");
        p->set_fail_message("Chwil�! W�a�nie pr�bujesz p�yn�� w kierunku powierzchni!\n");
        p->move(TP,1);
    
        if(query_prop(POZIOM) == 1)
        {
            write("Zaczynasz p�yn�� na powierzchni�.\n");
            saybb(QCIMIE(TP,PL_MIA)+" zaczyna p�yn�� na powierzchni�.\n");
        }
        else //jeste�my g��biej!
        {
            write("Zaczynasz p�yn�� do g�ry.\n");
            saybb(QCIMIE(TP,PL_MIA)+" zaczyna p�yn�� do g�ry.\n");
        }
        
        return 1;
    }
    
    return 0;
}

void
enter_inv(object ob, object from)
{
    ::enter_inv(ob,from);
    
    wspolne_enter_inv(ob, from);
}

void
leave_inv(object ob, object to)
{
    ::leave_inv(ob,to);
    
    //gagatek wychodzi z wody?
    if(is_lok_wody(to))
        return;
    
    if(!ob->is_living())
        return; 
    
    
    ob->remove_prop(TONIE);
    ob->remove_prop(IL_POWIETRZA);
    ob->remove_prop(IL_POWIETRZA_MAX);
    
    //usuwamy alarm i prop na p�ywanie:
    (ob->query_prop(PIERWSZA_ALARM_LOK))->usun_alarm(ob->query_prop(PLYWAM_ALARM));
    ob->remove_prop(PLYWAM_ALARM);
    //i na redukcj� powietrza:    
    (ob->query_prop(PIERWSZA_ALARM_LOK))->usun_alarm(ob->query_prop(POWIETRZE_ALARM));
    ob->remove_prop(POWIETRZE_ALARM);
    
    ob->catch_msg("Wychodzisz z wody.\n");
    
    ob->remove_prop(LIVE_S_EXTRA_SHORT);
    ob->remove_prop(SUMA_GESTOSCI_EKW);
    ob->remove_prop(PIERWSZA_ALARM_LOK);
}

public int
kierunki(string str)
{
    string cmd = query_verb();
    int i = -1,
        ob_flag = 0;
        
    //je�li komenda to kt�ra� z tych zakazanych, to nie da rady...
    if(member_array(cmd,ZAKAZANE_POD_WODA) != -1)
    {
        write("Jeste� teraz pod wod�, jak�e zatem m"+TP->koncowka("�g�by�","og�aby�")+
        " to uczyni�?\n");
        return 1;
    }

    if(cmd ~= "p�y�" && strlen(str) > 3 && str[0..1] ~= "na")
        cmd = str[3..];

        //trzeba zmieni� te� 'sp na kierunek':
    if((cmd ~= "sp" || cmd ~= "sp�jrz" || cmd ~="ob" || cmd~="obejrzyj") && (!str ||str == "0"))
    {
        //zwyk�e sp na lokacji na kt�rej jeste�my.
        return 0;
    }

    
    //trzeba zmienic skr�ty na pe�ne nazwy:
    switch(cmd)
    {
        case "n": case "polnoc": cmd = "p�noc"; break;
        case "ne": case "polnocny-wschod": cmd = "p�nocny-wsch�d"; break;
        case "nw": case "polnocny-zachod": cmd = "p�nocny-zach�d"; break;
        case "e": case "wschod": cmd = "wsch�d"; break;
        case "se": case "poludniowy-wschod": cmd = "po�udniowy-wsch�d"; break;
        case "s": case "poludnie": cmd = "po�udnie"; break;
        case "sw": case "poludniowy-zachod": cmd = "po�udniowy-zach�d"; break;
        case "w": case "zachod": cmd = "zach�d"; break;
        case "p�noc":
        case "p�nocny-wsch�d":
        case "wsch�d":
        case "po�udniowy-wsch�d":
        case "po�udnie":
        case "po�udniowy-zach�d":
        case "zach�d":
        case "p�nocny-zach�d":
            break;
    }


    //r�nica w tym kodzie a kodzie z woda_powierzchnia jest taka, �e
    //tutaj musimy sprawdza� po powierzchni wyj�cia i klonu pod_woda kierunku...
    i = member_array(cmd,query_powierzchnia()->query_exit_cmds());
//write("i="+i+"\n");
    if(i == -1)
        cmd = "dupa";
    
    if(query_verb() ~= "sp" || query_verb() ~="sp�jrz" || query_verb() ~= "ob" || query_verb() ~= "obejrzyj")
        ob_flag = 1;
    
    
     //sp na kierunek  - pod wod� mo�emy patrze� tylko na g�r�.
     if(ob_flag && cmd == "dupa" && (str ~= "na g�r�" || str ~="u" || str~="g�r�" || str~="na u"))
     {   
        write("Spogl�dasz na g�r�:\n");
        saybb(QCIMIE(TP,PL_MIA)+" spogl�da na g�r�.\n");
        TP->daj_paraliz_na_sp("NONE",file_name(query_powierzchnia()));
        return 1;
     }
     
     //if wpisywana komenda to �adne z wyj�� dost�pnych na powierzchni :
     //lub nie ma takiego poziomu na lokacji obok...
    if(cmd == "dupa" || query_powierzchnia()->query_prop(IL_POZIOMOW) < query_prop(POZIOM))
        return 0;
    
    
    if(TP->query_fatigue() <= LIMIT_ZMECZENIA_NA_PRZEPLYWANIE)
    {
        write("Nie masz ju� si�.\n");
        return 1;
    }
    
    LOAD_ERR(query_powierzchnia()->query_exit_rooms()[i]);
    object lok = find_object(query_powierzchnia()->query_exit_rooms()[i]);
    if(lok->query_prop(ROOM_I_TYPE) != ROOM_IN_WATER &&
        lok->query_prop(ROOM_I_TYPE) != ROOM_BEACH)
    {
        write("Nie mo�esz tam pop�yn��.\n");
        return 1;
    }
    
    //no to wyszukajmy teraz interesuj�ca nas lokacj� podwodn�
    object cel = znajdz_cel(lok);
//DBG("liczba poziomow celu: "+lok->query_prop(IL_POZIOMOW)+"\n");
//DBG("celem jest: "+file_name(cel)+"\n");
    
    
    //czas p�yni�cia: 1/2si�y + um p�ywania
    int czas = query_czas();
    
    object p=clone_object("/std/paralyze.c");
    p->set_finish_object(TO);
    p->set_finish_fun("koniec_paralizu", TP, cel, 
                        query_powierzchnia()->query_exit_cmds()[i], czas);
    p->set_remove_time(czas);
    p->set_stop_verb("przesta�");
    p->set_stop_message("Przestajesz p�yn��.\n");
    p->set_fail_message("Chwil�! W�a�nie dok�d� p�yniesz!\n");
    p->move(TP,1);
    
    cmd = "na " + cmd;

    //jeszcze sprawd�my, czy jest specjalny opis wyj�cia lokacji
//     cmd = sprawdz_specjalny_opis_wyjscia(cmd, query_exit_rooms()[i]);
        
    write("Zaczynasz p�yn�� "+cmd+".\n");
    saybb(QCIMIE(TP,PL_MIA)+" zaczyna p�yn�� "+cmd+".\n");
    
    return 1;
}


string
query_pomoc()
{
    return "Jeste� obecnie pod wod�, na samym dnie, jednak mo�esz porusza� si� "+
    "(p�ywa�) na strony niczym na l�dzie. Mo�esz tak�e spr�bowa� wyp�yn�� na "+
    "g�r�.\n";
}

init()
{
    ::init();
    add_action(kierunki, "", 1);    
    
    add_action(wynurz,"wynurz");
    add_action(wynurz,"g�ra");
    add_action(wynurz,"wyp�y�");
}
