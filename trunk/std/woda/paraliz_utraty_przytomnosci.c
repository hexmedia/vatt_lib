/*
 * jest to przerobiony parali� og�uszenia...
 * g��wn� r�nica jest taka, �e tutaj gracz pozostawiony sam sobie (czyli pod wod�)
 * umiera.
 * 
 */

inherit "/std/paralyze";

#include <macros.h>
#include <stdproperties.h>
#include <files.h>
#include <log.h>

#include <woda.h>

object corpse;

void
create_paralyze()
{
    set_name("utonal_paraliz");

    set_finish_object(this_object());
    set_finish_fun("ocknij_sie");
    set_remove_time(120 + random(60));
    set_block_all();

    setuid();
    seteuid(getuid());
}

public varargs void
umieraj(object player, object env)
{
    //object corpse;
    string *temp;
    int i;

    // Sprawdzamy czy �mier� naprawde si� nam nale�y.
    if((player->query_wiz_level() && player->query_prop(WIZARD_I_IMMORTAL)) || player->query_ghost())
    {
        return;
    }

    /* Stupid wiz didn't give the objectp to the killer. *
    if (!objectp(killer))
        killer = previous_object();
    /* Bad wiz, calling do_die in someone. *
    if ((MASTER_OB(killer) == WIZ_CMD_NORMAL) ||
        (MASTER_OB(killer) == TRACER_TOOL_SOUL))
        killer = this_interactive();
*/
//    CEX; combat_extern->cb_death_occured(killer, oglusz);

//    killer->notify_you_killed_me(this_object());

/*    // ogluszanie rozpatrujemy najpierw
    if((killer->query_option(OPT_FIGHT_MERCIFUL) &&
        query_interactive(this_object()) && !this_object()->query_npc()) ||
        oglusz)
    {
        object paraliz;

        corpse = clone_object("/std/cialo_ogluszone");

        corpse->ustaw_denata();
        corpse->change_prop(CONT_I_WEIGHT, query_prop(CONT_I_WEIGHT));
        corpse->change_prop(CONT_I_VOLUME, query_prop(CONT_I_VOLUME));
        corpse->add_prop(CORPSE_M_RACE, query_rasy());
        corpse->add_prop(CORPSE_M_PRACE,query_prasy());
        corpse->add_prop(CORPSE_I_RRACE, query_rodzaj_rasy() + 1);
        corpse->add_prop(CONT_I_TRANSP, 1);
        corpse->change_prop(CONT_I_MAX_WEIGHT, query_prop(CONT_I_MAX_WEIGHT));
        corpse->change_prop(CONT_I_MAX_VOLUME, query_prop(CONT_I_MAX_VOLUME));
        corpse->add_leftover(query_leftover());
        corpse->add_prop(CORPSE_AS_KILLER,
            ({ killer->query_real_name(), killer->query_nonmet_name() }) );

        this_object()->stop_fight(killer);
        this_object()->stop_fight();
        killer->stop_fight(this_object());

        corpse->move(environment(this_object()), 1);
        move_all_to(corpse);
        this_object()->move(VOID_ROOM);
        //FIXME: Nie wiem czy to przenoszenie ma sens, skoro stam�d od razu gracza wywala..
        // Tymczasowo zakomentowa�em wywalanie z voida, ale nie wiem z jakiego powodu ono
        // tam jest, i wola�bym, �eby si� autor wypowiedzia�. (Krun)

        paraliz = clone_object("/std/paralize/ogluszenie");
        paraliz->set_corpse(corpse);
        paraliz->set_fail_message("Chcesz, chcia�"
            +this_object()->koncowka("e�","a�","e�")
            +" to zrobi�, ale... twe my�li biegn� ju�"
            +" w inn� stron�.\n");

        paraliz->move(this_object(), 1);

        TO->catch_msg("Ogarnia ci� ciemno��.\n");

        //trzeba to logowac tyz! vera.
        log_file(LOG_PLAYEROGLUSZENI,ctime(time()) + " " +
                TP->query_real_name()+" przez "+
        (interactive(killer)?killer->query_real_name():file_name(killer)) +"\n");

        return;
    }

    /* rzeczy zabitego przenosimy na lokacj� */
    //player->move_all_to(environment(this_object()));
    all_inventory(corpse)->move(env, 1);
DBG("dupa2\n");
    /* po graczach nie zostaj� cia�a *
    if (!(player->query_prop(LIVE_I_NO_CORPSE)) && !interactive(player))
    {
        if (!objectp(corpse = (object)this_object()->make_corpse()))
        {
            corpse = clone_object("/std/corpse");
            corpse->ustaw_denata();
            corpse->change_prop(CONT_I_WEIGHT, query_prop(CONT_I_WEIGHT));
            corpse->change_prop(CONT_I_VOLUME, query_prop(CONT_I_VOLUME));
            corpse->add_prop(CORPSE_M_RACE, query_rasy());
            corpse->add_prop(CORPSE_M_PRACE,query_prasy());
            corpse->add_prop(CORPSE_I_RRACE, query_rodzaj_rasy() + 1);
            corpse->add_prop(CONT_I_TRANSP, 1);
            corpse->change_prop(CONT_I_MAX_WEIGHT,
                query_prop(CONT_I_MAX_WEIGHT));
            corpse->change_prop(CONT_I_MAX_VOLUME,
                query_prop(CONT_I_MAX_VOLUME));
            corpse->add_leftover(query_leftover());

            corpse->add_prop(CORPSE_AS_KILLER,
                     ({ killer->query_real_name(),
                        killer->query_nonmet_name() }) );
            corpse->move(environment(this_object()), 1);
        }
    }*/

    player->set_ghost(1);
DBG("dupa2\n");
    if (!player->second_life(0))
    {
        all_inventory(env)->signal_stop_fight(player);
        //player->add_prop(PLAYER_I_LAST_FIGHT, time());
        player->remove_object();
    }
    //else
        corpse->remove_object();
DBG("dupa2\n");
}



void
ocknij_sie()
{
    object env = environment(corpse);
    object player = environment(this_object());

    while (environment(env))
        env = environment(env);
DBG("dupa1\n");
    if(env->query_prop(ROOM_I_TYPE) != ROOM_IN_WATER &&
        env->query_prop(ROOM_I_TYPE) != ROOM_UNDER_WATER)
    {
        player->move(env, 1);
        all_inventory(corpse)->move(player, 1);
        corpse->remove_object();

        player->command("zakrztus sie");
        write("Powoli dochodzisz do siebie.\n");
        saybb(QCIMIE(player, PL_MIA) + " wstaje.\n");
        
        
        log_file(LOG_PLAYEROGLUSZENI,ctime(time()) + " " +
            player->query_real_name()+" jednak sie nie utopil...!\n");
    }
DBG("dupa1\n");        
    //nikt nas nie odratowa�...no to umieranko? ]:->
    umieraj(player, env);
    
DBG("dupa1\n");    
    log_file(LOG_PLAYEROGLUSZENI,ctime(time()) + " " +
            player->query_real_name()+" utopil sie.\n");
}

void
set_corpse(object ob)
{
    corpse = ob;
}
