/*
 * Ten plik zawiera funkcje pomocnicze dla lokacji wody -
 * wody powierzchni, lokacji pod wod� oraz dna
 * Vera
 */

#include <macros.h>
#include <ss_types.h>
#include <stdproperties.h>
#include <materialy.h>
#include <object_types.h>
#include <log.h>
#include <files.h>
#include <woda.h>
#include <exp.h>

string zmien_kierunek(string str);
int is_lok_wody(object to);
object ustaw_dol_jesli_nie_ma(object dla_obiektu, int poziom = 0);
float srednia_gestosci(object ob);
void obiekt_tonie(object ob, object skad);
void usun_alarm(int alarm);


public string
query_auto_load()
{
    return 0;
}

//kolejna funkcja pomocnicza...
string
sprawdz_specjalny_opis_wyjscia(string opis, string cel)
{
    mixed *wyjscia = ENV(TP)->query_exit();

    if(pointerp(wyjscia[member_array(cel, wyjscia) + 1]))
        opis = wyjscia[member_array(cel, wyjscia) + 1][1];

    return opis;
}

/*ta funkcja wywo�uje si� jako koniec parali�u na p�ywanie na <kierunek>
 */
void
koniec_paralizu(object *co,object kto, object cel, string kierunek, int str_mod)
{
    string kierunekX = "na " + kierunek;
    //string kierunek_X = sprawdz_specjalny_opis_wyjscia(kierunek, file_name(cel));
 
    object old_room = ENV(kto);
    tell_room(old_room,QCIMIE(kto,PL_MIA)+" oddala si� "+kierunekX+".\n",
               ({kto}));    
 
    kto->add_fatigue(-(25*str_mod + random(10)));
    kto->add_panic(10);

    kto->move(cel,1);

    //komunikaty!
    if(!is_lok_wody(cel)) //to nie woda
    {
        kto->write("Wychodzisz z wody.\n");
        tell_room(cel,QCIMIE(TP,PL_MIA)+" wychodzi z wody.\n", ({kto}));
        object przemoczenie = clone_object("/std/efekty/przemoczenie.c");
        przemoczenie->przemocz(kto);
    }
    else if(is_lok_wody(old_room)) //to woda, z wody
    {
    
        string odw_kierunek = "";
        odw_kierunek = zmien_kierunek(kierunek);
        if(strlen(odw_kierunek))
            odw_kierunek = " z" + odw_kierunek;
        
        tell_room(cel,QCIMIE(TP,PL_MIA)+" przyp�ywa"+odw_kierunek+".\n",({TP}));
        foreach(object x : filter(AI(cel), &->query_lodka()))
            tell_room(x->query_wnetrze(),QCIMIE(TP,PL_MIA)+" przyp�ywa"+odw_kierunek+".\n",({TP}));
    }
    else //do wody, nie z wody ;p np. z ��dki/pomostu.
    {
        tell_room(cel,QCIMIE(TP,PL_MIA)+" wskakuje do wody.\n",({TP}));
        foreach(object x : filter(AI(cel) - ({TP}), &->query_lodka()))
            tell_room(x->query_wnetrze(),QCIMIE(TP,PL_MIA)+" wskakuje do wody.\n",({TP}));
    }

    kto->do_glance(1);
    
    
    //expik? :)
    kto->increase_ss(SS_SWIM, EXP_PLYNE_WPLAW_SWIM);
    kto->increase_ss(SS_STR,  EXP_PLYNE_WPLAW_STR);
    kto->increase_ss(SS_DEX,  EXP_PLYNE_WPLAW_DEX);
}


//czy lokacja 'to' jest lokacj� wodn� ;]
int
is_lok_wody(object to)
{
    return (to->query_prop(ROOM_I_TYPE) == ROOM_IN_WATER ||
        to->query_prop(ROOM_I_TYPE) == ROOM_UNDER_WATER);
}

/*funkcja pomocnicza, do ustalania czasu parali�u p�ywania gdzie�, 
 nurkowania, wynurzania si�*/
int
query_czas()
{
    int czas = 2;
    //czas p�yni�cia: 1/2 (procentowo) zr�czno�ci + um p�ywania
    switch((TP->query_stat(SS_DEX) * 100 / 136) / 2 + TP->query_skill(SS_SWIM))
    {
        case 0..19:  czas = 7; break;
        case 20..35: czas = 6; break;
        case 36..49: czas = 5; break;
        case 50..69: czas = 4; break;
        case 70..89: czas = 3; break;
    }
    return czas;
}


/*funkcja dodaj�ca zm�czenie
 je�li mod to 0, to p�ywamy, a jak 1 to nurkujemy/wyp�ywamy na powierzchni�
 */
 
void
zmecz(object kogo, int mod)
{
    kogo->add_panic(1);
    int ile = 0;
    
    switch(mod)
    {
        case 0: //p�ywamy
            ile = -((random(80) + 200 + (kogo->query_intoxicated() * 3)) );
            break;
        case 1: //nurkujemy lub wyp�ywamy na powierzchni�:
            ile = -((random(80) + 400 + (kogo->query_intoxicated() * 5)) );
            break;
    }
    
    //modyfikator od przeci��enia gracza. Powinien by� do�� poka�ny:
    ile -= ftoi(kogo->query_prop(SUMA_GESTOSCI_EKW)) * 100;
    
//DBG(kogo->query_real_name()+": zmecz: "+ile);
    kogo->add_fatigue(ile);
}


/* funkcja wywo�ywana, gdy zaczynamy ton�� - sama na siebie wywo�uje alarm
 */
void
toniemy(object kto, int jak_dlugo = 0)
{
    //uda�o si�, uratowani! wyszli�my na powierzchni�.
    if(!is_lok_wody(ENV(kto)))
    {
        usun_alarm(kto->query_prop(TONIE));
        kto->remove_prop(TONIE);
        
        //no chyba �e niekoniecznie :P
        if(kto->query_ghost() || file_name(ENV(kto)) == VOID_ROOM)
        {
            return;
        }
        
        kto->catch_msg("Ufff! Uda�o ci si� wyj�� z tego ca�o!\n");
        kto->command("odetchnij spazmatycznie");
        return;
    }

    if(!kto->query_prop(TONIE))
        kto->add_prop(TONIE,1);

    switch(jak_dlugo)
    {
        case 0:
            kto->catch_msg("Zaczynasz si� topi�!\n");
            tell_roombb(ENV(kto),QCIMIE(kto,PL_MIA)+" zaczyna si� topi�!\n",({kto}),kto);
            kto->add_panic(30);
            break;
            
        case 1:
            kto->command("spanikuj");
            kto->add_panic(10);
            break;
            
        case 2:
            kto->catch_msg("Rozpaczliwie bijesz ko�czynami w wod�, to "+
                "jednak na nic si� zdaje, a ty nabierasz coraz wi�cej wody w p�uca.\n");
            tell_roombb(ENV(kto),QCIMIE(kto,PL_MIA)+" rozpaczliwie bije "+
                "ko�czynami w wod�.\n",({kto}),kto);
            break;
            
        case 4:
            kto->catch_msg("Nerwowo kopiesz nogami.\n");
            break;
            
        case 5:
            kto->catch_msg("Coraz bardziej oddalasz si� "+
            "od powierzchni wody, lecz nie poddajesz si�.\n");
            
            break;
            
            
        case 3:
            kto->catch_msg("Opadasz coraz ni�ej!\n");
            break;
        case 6:
        case 16:
        case 24:
        case 38:
        case 50:
            if(!ENV(kto)->jestem_dnem())
                obiekt_tonie(kto, ENV(kto));
            break;
    }
        
    kto->add_fatigue(-80 - random(50));
    kto->add_panic(20);
    
   
DBG("tonie: "+kto->query_real_name()+" jak dlugo: "+jak_dlugo);
    set_alarm(3.0+itof(random(3)), 0.0, "toniemy", kto, jak_dlugo + 1);
    return;
}

/*
 * funkcja wywo�ywana z alarmu gdy obiekt (albo gracz, ale wtedy 
 * to ju� tylko jego cia�o), kt�ry zostaje wrzucony
 * do wody tonie
 */
void
obiekt_tonie(object ob, object skad)
{
    //tylko obiekty 'godne' naszej uwagi m�wi� nam �e ton� :)
    if(ob->query_prop(OBJ_I_VOLUME) > 500)
    {
        tell_roombb(ENV(ob), QCSHORT(ob,PL_MIA)+" tonie.\n", ({ob}),ob);
        //informujemy te� tych na ��dkach:
        foreach(object lodka : filter(AI(ENV(ob)), &->query_lodka()))
            tell_room(lodka, QCSHORT(ob,PL_MIA)+" tonie.\n", ({ob}));
    }
    
    ustaw_dol_jesli_nie_ma(skad, skad->query_prop(POZIOM) + 1);
    ob->move(skad->query_dol(), 1);
    
    
    if(ob->is_living())
        ob->catch_msg("Opadasz pod wod� coraz ni�ej.\n");;
}

//funkcja pomocnicza...
object //1 arg: obiekt dla jakiego obiektu ustawiamy dol, drugi arg int - ktory to juz bedzie poziom
ustaw_dol_jesli_nie_ma(object dla_obiektu, int poziom = 0)
{
    //powierzchnia celu:
    object powierzchnia = (dla_obiektu->query_prop(POZIOM) ?
                           dla_obiektu->query_powierzchnia() : dla_obiektu);
                           
DBG("ustawiam dol jesli go nie ma, dla obiektu: "+file_name(dla_obiektu));   

    //ustawiamy pierwszy poziom.
    if(!objectp(dla_obiektu->query_dol()))
    {
        if(powierzchnia->query_prop(IL_POZIOMOW) <= poziom)
        {
            dla_obiektu->set_dol(clone_object("/std/woda/dno"));
            //jeszcze ustawmy opisy dla tego do�u
            (dla_obiektu->query_dol())->set_short(powierzchnia->query_short_dna());
            (dla_obiektu->query_dol())->set_long(powierzchnia->query_long_dna());
        }   
        else
        {
            dla_obiektu->set_dol(clone_object("/std/woda/pod_woda"));
            //jeszcze ustawmy opisy dla tego do�u
            (dla_obiektu->query_dol())->set_short(powierzchnia->query_short_pod_woda());
            (dla_obiektu->query_dol())->set_long(powierzchnia->query_long_pod_woda());
        }
        
DBG("int poziom="+poziom+", prop powierzchnia->ilpoziomow:"+powierzchnia->query_prop(IL_POZIOMOW));   
        //ustawiamy lokacj� powierzchni, oraz przesy�amy dalej liczb� poziom�w.
        (dla_obiektu->query_dol())->set_powierzchnia(powierzchnia);
        //ustawiamy lokacj� g�ry. gdy poziom jest 1, g�ra = powierzchnia, naturlich!
        (dla_obiektu->query_dol())->set_gora(dla_obiektu);
        
        (dla_obiektu->query_dol())->add_prop(POZIOM,poziom);
        
    }

    return dla_obiektu->query_dol();
}




/* a ta funkcja znajduje nam lokacj�, na kt�r� chcemy p�yn��
 (p�ywawszy pod wod� :P )
 jako argument podajemy lokacj� powierzchni, czyli sam� g�r� kierunku na kt�ry
 si� udajemy.
 
 Ta funkcja wywo�ywana jest tylko przy p�ywaniu na boki
 (do p�ywania w g�r� i w d� s� inne funkcje, a p�ywanie po skosie jest
 surowo zabronione :P )
 
 UWAGA: jakby kto� si� zastanawia�, to dzia�a to tak, �e: z dna 3 poziomu p�yn��
 na lok o 2 poziomach wp�ywamy na to w�a�nie dno (czyli 2poziom), czyli jest mo�liwe
 wpisuj�c nazw� kierunku (na boki) de facto pop�yn�� o poziom wy�ej.
 Ni�ej za� nie da rady.
 */
object
znajdz_cel(object powierzchnia_celu)
{
    object cel; //cel... to brzmi dumnie!
    object tmp;
    //ilo�� poziom�w w pionie w kt�rym jeste�my.
    //int il_poziomow = (TO->query_powierzchnia())->query_prop(IL_POZIOMOW);
    //aktualny poziom na kt�rym jeste�my:
    int akt_poziom = TO->query_prop(POZIOM);
    int x = 1;
    
    while(x <= akt_poziom) //p�tla b�dzie si� wykonywa�a tyle razy ile wynosi aktualny poziom
    {
        cel = ustaw_dol_jesli_nie_ma(powierzchnia_celu, x);
        powierzchnia_celu = cel;
        
        if(cel->jestem_dnem())
            break;
        
        x++;
    }
       
    if(!objectp(cel)) //teoretycznie niemo�liwe....
    {
        write("Wyst�pi� powa�ny b��d w p�ywaniu nr 2ve-p0. Prosz� zg�o� go, "+
              "podaj�c ten numer oraz okoliczno�ci w jakich do niego dosz�o.\n");
        return 0;
    }
    
    return cel;
}

//pomocnicza funkcja do filtrowania ekwipunku gracza.
//taka sama znajduje si� w lodka_in.c , ale to jedyna powt�rka, wi�c
//dla takiej ma�ej pierd�ki nie ma sensu robi� wsp�lnego std...
int
filtruj_niewidoczne(object ob)
{
    return !(ob->query_no_show() || ob->query_no_show_composite() ||
        /*ob->query_prop(OBJ_I_DONT_INV) || bleble! g�wno. Obiekty w subloku tez maj� by� liczone!*/
        ob->query_prop(OBJ_M_NO_DROP) ||
        ob->query_prop(WIZARD_ONLY) );
}

//zwraca nam zm�czenie obiektu 'kto' w procentach.
int
query_proc_fatigue(object kto)
{
    return kto->query_fatigue() * 100 / kto->query_max_fatigue();
}

/* ta funkcja wywo�uje si� co chwila z alarmu i
 * odpowiada za m.in. zabieranie zm�czenia z powodu przebywania w wodzie,
 * itd.
 */
void
plywam(object kto)
{
    /*if(ENV(kto) != TO)
        return;
    
    if(kto->query_prop(PLYWAM_ALARM))
        return;*/
    
    
    int skill = kto->query_skill(SS_SWIM) + kto->query_tskill(SS_SWIM);
    
    if(skill < LIMIT_UM_SWIM)
    {
        toniemy(kto);
        return;
    }

    //ustalmy propa g�sto�ci wszystkich obiekt�w, kt�re posiadamy
    //b�dzie to nam potrzebne do ustawiania toni�cia,dodawania zm�czenia...
    if(!kto->query_prop(SUMA_GESTOSCI_EKW))
    {
        float g = 0.0;
        foreach(object obj : filter(AI(kto), &filtruj_niewidoczne()))
        {
DBG("szukamy gestosci dla obiektu " +obj->query_name());
            g += srednia_gestosci(obj);
        }        
        kto->add_prop(SUMA_GESTOSCI_EKW, g);
//DBG("skill: "+skill+", a sume gestosci ustawiam :");dump_array(({g}));
        if(g >= itof(skill / 2)) //if SUMA_GESTOSCI_EKW >= um p�ywania (teor + prakt) / 2
        {
            toniemy(kto);
            return;
        }
            
    }

    zmecz(kto,0);
    
    switch(skill)
    {
        case 0..10:  kto->add_panic(5); break;
        case 11..20: kto->add_panic(4); break;
        case 21..26: kto->add_panic(3); break;
        case 27..30: kto->add_panic(2); break;
        case 31..49: kto->add_panic(1); break;
        default:
                if(!random(3))
                    kto->add_panic(1);
                break;
    }
    
    int proc_fatig = query_proc_fatigue(kto); //FIXME
   /*
   Zmeczenie daje sie we znaki.                                                                                                              
Jestes juz powaznie zmeczony.                                                                                                             
Opadasz z sil.                                                                                                                            
Zaczynasz sie topic!                                                                                                                      
woda dbg: tonie: vera jak dlugo: 0                                                                                                        
Wpadasz w panike!                                                                                                                         
woda dbg: tonie: vera jak dlugo: 1    

a mia�em przed: Zmeczenie:  4643( 9900
po eventach Zmeczenie:  4194( 9900)
*/
    //proc_fatig = (random(2) ? proc_fatig : -1); //randomowo co drugi raz event:
    if(kto->query_prop(TONIE))
        proc_fatig = -1;    //darujmy sobie eventy o zm�czeniu skoro toniemy...
        
DBG(kto->query_real_name()+": zm�czenie procentowe: "+proc_fatig+"\n");
    switch(proc_fatig)
    {
        
        case 95..97:
        case 85..87:
            switch(random(3))
            {
                case 0:
                    if(ENV(kto)->query_powierzchnia() == ENV(kto))
                        kto->catch_msg("Swobodnie unosisz si� na powierzchni wody.\n");
                    else
                        kto->catch_msg("Czujesz si� wypocz�t"+kto->koncowka("y","a")+".\n");
                    break;
                case 1:
                    kto->catch_msg("Masz jeszcze sporo si�.\n");
                    break;
            }
            
            break;
            
        case 69..71:
        case 60..62:
            switch(random(4))
            {
                case 0:
                    kto->catch_msg("Zaczynasz odczuwa� zm�czenie.\n");
                    break;
                case 1:
                    kto->catch_msg("Powoli odczuwasz zm�czenie.\n");
                    break;
                case 2:
                    kto->catch_msg("Niewielkie zm�czenie zaczyna dawa� o sobie zna�.\n");
                    break;
                case 3:
                    kto->catch_msg("Zaczynasz odczuwa� pierwsze oznaki zm�czenia.\n");
                    break;
            }
            break;
        case 50..52:
        case 47:
            switch(random(3))
            {
                case 0:
                    kto->catch_msg("M�czysz si�.\n");
                    break;
                case 1:
                    kto->catch_msg("Zm�czenie daje si� we znaki.\n");
                    break;
            }
            break;
        case 37..38:
        case 33..34:
        case 29:
            switch(random(3))
            {
                case 0:
                    kto->catch_msg("Jeste� ju� powa�nie zm�czon"+kto->koncowka("y","a")+".\n");
                    break;
                case 1:
                    kto->catch_msg("Zaczyna brakowa� ci si�.\n");
                    break;
                case 2:
                    kto->catch_msg("Zaczynasz ju� odczuwa� powa�ne zm�czenie.\n");
                    break;
            }
        case 16..17:
        case 13..14:
            switch(random(3))
            {
                case 0:
                    kto->catch_msg("Czujesz si� �miertelnie zm�czon"+kto->koncowka("y","a")+".\n");
                    break;
                case 1:
                    kto->catch_msg("Opadasz z si�.\n");
                    break;
                case 2:
                    kto->catch_msg("Si�y opuszczaj� ci� ca�kowicie.\n");
                    break;
            }
            break;
        case 0..3:
            //kto->catch_msg("Toniesz.\n");
            toniemy(kto);
            return;
            break;       
    }
           
    /*if(!kto->query_prop(PLYWAM_ALARM))
        kto->add_prop(PLYWAM_ALARM, set_alarm(6.0+itof(random(3)),0.0,"plywam",kto));*/
    
    /*if(!czy_jest_alarm(PLYWAM_ALARM, kto)) //je�li kto nie ma alarmu PLYWAM_ALARM, to mu go dajemy z powrotem
        kto->add_prop(PLYWAM_ALARM, (kto->query_prop(PIERWSZA_ALARM_LOK))->daj_alarm_na("plywam", 
                                                                                    kto, 6.0 + itof(random(3))));
      */
    
DBG("p�ywam: "+kto->query_real_name());
    //set_alarm(3.0+itof(random(3)), 0.0, "plywam", kto);
}

/*
 * TA DA DA DAM! zabijamy gracza :O
 * funkcja zer�ni�ta i zrobiona na podst. do_die
 * najpierw tracimy przytomno��....
 */
public varargs void
traci_przytomnosc(object kto)
{
    object corpse;
    string *temp;
    int i;
    
    //te pierdo�y na nic si� ju� nie zdadz�...
    //kto->remove_prop(TONIE);
    kto->remove_prop(IL_POWIETRZA);
    kto->remove_prop(IL_POWIETRZA_MAX);
    kto->remove_prop(PLYWAM_ALARM);
    kto->remove_prop(POWIETRZE_ALARM);
    kto->remove_prop(LIVE_S_EXTRA_SHORT);
    kto->remove_prop(SUMA_GESTOSCI_EKW);
    
    
    // Sprawdzamy czy �mier� naprawde si� nam nale�y.
    if((kto->query_wiz_level() && kto->query_prop(WIZARD_I_IMMORTAL)) || kto->query_ghost())
    {
        if(kto->query_wiz_level())
            kto->catch_msg("Normalnie ju� by� si� utopi�"+kto->koncowka("","a")+"! "+
            "Masz szcz�cie, �e jeste� czarownikiem! :)\n");
        
        return;
    }

    //przenie�my ju� bez ceregieli denata na dno
    while(!ENV(kto)->jestem_dnem())
        obiekt_tonie(kto, ENV(kto));
    
    
    //CEX; combat_extern->cb_death_occured(killer, oglusz);

    //killer->notify_you_killed_me(this_object());
    
    // ogluszanie rozpatrujemy najpierw
    //if((killer->query_option(OPT_FIGHT_MERCIFUL) &&
    //    query_interactive(this_object()) && !this_object()->query_npc()) ||
    //    oglusz)
    //{
    object paraliz;

    corpse = clone_object("/std/cialo_ogluszone");

    corpse->ustaw_denata();
    corpse->change_prop(CONT_I_WEIGHT, kto->query_prop(CONT_I_WEIGHT));
    corpse->change_prop(CONT_I_VOLUME, kto->query_prop(CONT_I_VOLUME));
    corpse->add_prop(CORPSE_M_RACE, kto->query_rasy());
    corpse->add_prop(CORPSE_M_PRACE, kto->query_prasy());
    corpse->add_prop(CORPSE_I_RRACE, kto->query_rodzaj_rasy() + 1);
    corpse->add_prop(CONT_I_TRANSP, 1);
    corpse->change_prop(CONT_I_MAX_WEIGHT, kto->query_prop(CONT_I_MAX_WEIGHT));
    corpse->change_prop(CONT_I_MAX_VOLUME, kto->query_prop(CONT_I_MAX_VOLUME));
    corpse->add_leftover(kto->query_leftover());
    //corpse->add_prop(CORPSE_AS_KILLER,
    //    ({ killer->query_real_name(), killer->query_nonmet_name() }) );

    //kto->->stop_fight(killer);
    kto->stop_fight();
    //killer->stop_fight(this_object());

    corpse->move(ENV(kto), 1);
    kto->move_all_to(corpse);
    this_object()->move(VOID_ROOM);
    //FIXME: Nie wiem czy to przenoszenie ma sens, skoro stam�d od razu gracza wywala..
    // Tymczasowo zakomentowa�em wywalanie z voida, ale nie wiem z jakiego powodu ono
    // tam jest, i wola�bym, �eby si� autor wypowiedzia�. (Krun)

    paraliz = clone_object("/std/woda/paraliz_utraty_przytomnosci");
    paraliz->set_corpse(corpse);
    paraliz->set_fail_message("Chcesz, chcia�"
        +kto->koncowka("e�","a�","e�")
        +" to zrobi�, ale... twe my�li biegn� ju�"
        +" w inn� stron�.\n");

    paraliz->move(kto, 1);

    kto->catch_msg("Ogarnia ci� ciemno��.\n");

    //trzeba to logowac tyz! vera.
    log_file(LOG_PLAYEROGLUSZENI,ctime(time()) + " " +
            kto->query_real_name()+" si� topi na lok: "+file_name(ENV(kto))+"\n");

    return;


    /* rzeczy zabitego przenosimy na lokacj� */
    kto->move_all_to(ENV(kto));

    /* po graczach nie zostaj� cia�a *
    if (!(this_object()->query_prop(LIVE_I_NO_CORPSE)) && !interactive(this_object()))
    {
        if (!objectp(corpse = (object)this_object()->make_corpse()))
        {
            corpse = clone_object("/std/corpse");
            corpse->ustaw_denata();
            corpse->change_prop(CONT_I_WEIGHT, query_prop(CONT_I_WEIGHT));
            corpse->change_prop(CONT_I_VOLUME, query_prop(CONT_I_VOLUME));
            corpse->add_prop(CORPSE_M_RACE, query_rasy());
            corpse->add_prop(CORPSE_M_PRACE,query_prasy());
            corpse->add_prop(CORPSE_I_RRACE, query_rodzaj_rasy() + 1);
            corpse->add_prop(CONT_I_TRANSP, 1);
            corpse->change_prop(CONT_I_MAX_WEIGHT,
                query_prop(CONT_I_MAX_WEIGHT));
            corpse->change_prop(CONT_I_MAX_VOLUME,
                query_prop(CONT_I_MAX_VOLUME));
            corpse->add_leftover(query_leftover());

            corpse->add_prop(CORPSE_AS_KILLER,
                     ({ killer->query_real_name(),
                        killer->query_nonmet_name() }) );
            corpse->move(environment(this_object()), 1);
        }
    }*/

    kto->set_ghost(1);

    //if (!this_object()->second_life(killer))
    if (!kto->second_life(0))
    {
        //all_inventory(ENV(kto))->signal_stop_fight(kto);
        //TP->add_prop(PLAYER_I_LAST_FIGHT, time());
        kto->remove_object();
    }
    else
        corpse->remove_object();
}

//a ta funkcja z kolei ustawia ile graczowi (argument 'komu')
//damy powietrza
int
ustal_ilosc_powietrza(object komu)
{
    //ustawiamy ilosc_powietrza, zaczynamy od stat wytrzyma�o��
    int ilosc_powietrza = komu->query_stat(SS_CON) - random(15);
    //potr�camy za zm�czenie
    ilosc_powietrza -= (100 - query_proc_fatigue(komu)) / 2;
    //za panik� te� potr�camy
    ilosc_powietrza -= komu->query_panic() / 2;
    //a teraz bonus - za wysoki um p�ywania w praktyce
    ilosc_powietrza += komu->query_skill(SS_SWIM) / 3;
    
    //ehh...a� by si� chcia�o za jaranie fajek te� potr�ci� :P
    
    //sprawd�my przypadkiem, czy nie za du�o potr�cili�my:
    if(ilosc_powietrza <= 10)
        ilosc_powietrza = 10;
    
    return ilosc_powietrza;
}


//funkcja odpowiedzialna za zabieranie graczowi powietrza,
//bo jest pod wod�.
void
redukuj_powietrze(object kto)
{   
    int il = kto->query_prop(IL_POWIETRZA),
        il_max = kto->query_prop(IL_POWIETRZA_MAX);
    
    if(il <= 0) //redukcja do 0, bez minusowych liczb
        kto->remove_prop(IL_POWIETRZA);
    else
        kto->add_prop(IL_POWIETRZA, il - REDUKUJEMY_POWIETRZE);
    
DBG(kto->query_real_name()+": redukuj� powietrze. zosta�o: "+il);

    //eventy przy wychodzeniu na powierzchni�
    if(ENV(kto)->query_prop(ROOM_I_TYPE) != ROOM_UNDER_WATER)
    {
        switch(il)
        {
            case 0..10:
                kto->catch_msg("Z wielk� ulg� zn�w nabierasz powietrza do p�uc. Uff!\n");
                tell_room(ENV(kto),QCIMIE(kto,PL_MIA)+" oddycha spazmatycznie.\n",({kto}));
                break;
            case 11..20:
                kto->command("odetchnij gleboko");
                break;
            case 21..35:
                kto->catch_msg("Z ulg� zn�w nabierasz powietrza do p�uc.\n");
                break;
        }
        usun_alarm(kto->query_prop(POWIETRZE_ALARM));
        kto->remove_prop(POWIETRZE_ALARM);
        kto->remove_prop(IL_POWIETRZA);
        kto->remove_prop(IL_POWIETRZA_MAX);
        return; //jak jeste�my na powierzchni, to nic ju� dalej nie robimy...
    }
    
    //eventy informuj�ce o ilo�ci powietrza:
    switch((il * 100) / il_max) //procentowa ilo�� powietrza
    { //obecny z woda.h defin to: potr�canie 2 do 4
        case 7..10:
            kto->catch_msg("Zaczynasz si� dusi�!\n");
            break;
        case 15..18:
            kto->catch_msg("Zaczyna brakowa� ci powietrza!\n");
            break;
        case 21..25:
            kto->catch_msg("Czujesz, �e wytrzymasz na bezdechu jeszcze tylko troch�.\n");
            break;
        case 32..35:
            if(il_max <= 45)
                kto->catch_msg("Masz coraz mniej tlenu w p�ucach.\n");
            break;
        case 40..43:
            if(il_max <= 46)
                kto->catch_msg("Masz jeszcze sporo tlenu w p�ucach.\n");
            else
                kto->catch_msg("Powoli zaczyna brakowa� ci tlenu w p�ucach.\n");
            break;
        case 53..56:
            if(il_max > 50)
                kto->catch_msg("Powietrza wystarczy ci jeszcze na do�� d�ugo.\n");
            
            break;
        case 72..75:
            if(il_max > 50)
                kto->catch_msg("Powietrza wystarczy ci jeszcze na bardzo d�ugo.\n");
            else
                kto->catch_msg("Powietrza wystarczy ci jeszcze na jaki� czas.\n");
            break;
    }
    
    if(il <= 0)
    {
        kto->catch_msg("Dusisz si�.\n");
        kto->remove_prop(IL_POWIETRZA);
        kto->remove_prop(IL_POWIETRZA_MAX);
        usun_alarm(kto->query_prop(PLYWAM_ALARM));
        kto->remove_prop(PLYWAM_ALARM);
        traci_przytomnosc(kto);
        
    }
    else
    {/*
        if(!kto->query_prop(POWIETRZE_ALARM))
            kto->add_prop(POWIETRZE_ALARM,
                set_alarm(3.0+itof(random(3)),0.0,"redukuj_powietrze",kto));*/
    }
            
}

//funkcja tylko do opisywania kto sk�d przyp�ywa itd.
string
zmien_kierunek(string str)
{
    switch(str)
    {
        case "p�noc" : str = " po�udnia"; break;
        case "p�nocny-zach�d" : str = " po�udniowego-wschodu"; break;
        case "zach�d" : str = "e wschodu"; break;
        case "po�udniowy-zach�d" : str = " p�nocnego-wschodu"; break;
        case "po�udnie" : str = " p�nocy"; break;
        case "po�udniowy-wsch�d" : str = " p�nocnego-zachodu"; break;
        case "wsch�d" : str = " zachodu"; break;
        case "p�nocny-wsch�d" : str = " po�udniowego-zachodu"; break;
        case "goto" : str = ""; break;
    }
    return str;
}

//to jest w�a�ciwa funkcja wyliczaj�ca �redni�.
float
wylicz_gestosc(object ob)
{
    string *materialy = ob->query_materialy();
    float srednia, *gestosci=({}),
          sumka = 0.0;

    if(!sizeof(materialy)) //uuu, kto� nie przydzieli� materia��w, nie�adnie.
    {
        if(!ob->is_living() && !ob->is_corpse())
            TP->notify_wizards(ob,"Ten obiekt nie ma przypisanych materia��w z "+
                        "jakich jest zrobiony! :[ Popraw kto�!\n");
        srednia = 2.0; //wi�c b�dzie ton��, za kar� :P
    }
    else
    {
        foreach(string material : materialy)
            gestosci += ({ GESTOSC_MATERIALU(material) });
    
        foreach(float gest : gestosci)
            sumka += gest;
            
        srednia = sumka / itof(sizeof(gestosci));
    }
    
    return srednia;
}

/*
 * ta funkcja wylicza �redni� g�sto�ci wszystkich materia��w w obiekcie 'ob'
 *
 */
float
srednia_gestosci(object ob)
{
    float gestosc = wylicz_gestosc(ob);
    
    //jeszcze na koniec sprawd�my, czy to przypadkiem nie jaki� kontener, plecak czy torba.
    //wtedy g�sto�ci obiekt�w w �rodku tak�e musimy zsumowa�.
    if(ob->is_container())
    {
        foreach(object x : ob->subinventory())
            gestosc += wylicz_gestosc(ob);
    }
    
    return gestosc;
}

//funkcja ukrywa object ob
void
ukryj(object ob)
{
    ob->add_prop(OBJ_I_HIDE, 10 + random(80));
}

/*
 * wywo�ywane z enter_env w lokacjach wody,
 * jak obiekt, kt�ry nie jest livingiem ani ��dk� 'wchodzi' na lok. wody (np. jest wyrzucony przez kogo�)
 */
void
obiekt_wpadl_do_wody(object ob, object from)
{   //wi�c trzeba obliczy�, czy to utonie, czy jak w og�le ma si� zachowa�...

    float srednia;

    //cia�o gracza, co straci� przytomno��:
    /*hmmm...nie to chyba jednak nie b�dzie potrzebne. sprawd�my... WARNING
    if(ob->is_cialo_ogluszone())
    {
        set_alarm(1.0, 0.0, "obiekt_tonie",ob);
        return;
    }*/

    //komunikaty: inne dla lokacji powierzchni i inne dla lokacji pod wod�:
    if(ENV(ob)->jestem_lokacja_powierzchni_wody())
        tell_room(ENV(ob),"Wyrzucon"+ob->koncowka("y","a","e")+" w�a�nie"+
            (from->query_lodka()? "z "+from->short(PL_DOP) : "")+" "+
            ob->short(PL_MIA)+" wpada do wody.\n");
    else if(from == ENV(ob)->query_gora())
    {//pod wod� tylko 'godne' obiekty m�wi� o swym marnym losie ;p
        if(ob->query_prop(OBJ_I_VOLUME) > 800)
            tell_room(ENV(ob),QCSHORT(ob,PL_MIA)+ "przydryfowuje z g�ry.\n");
    }   
        
    //gasn� �wiat�a... ;]
    if(ob->query_lit())
        ob->extinguish_me();
    
    
    
    
    //je�li obiekt jest ju� na dnie, to nie obliczamy �adnych pierd�,
    //tylko ukrywamy go i ju�. 
    if(ENV(ob)->jestem_dnem()) //smutna nazwa funkcji :(
    {
        //, a malutkie przedmioty zwykle gin� bezpowrotnie
        if(ob->query_prop(OBJ_I_VOLUME) <= 4)
        {
            ob->remove_object();
            return;
        }
        
        set_alarm(1.0,0.0, "ukryj", ob);
        return;
    }
    
    
    
    
        
    //no dobra, to gracze ju� wiedz�... teraz obliczmy czy b�dzie ton��..
    //czy p�ywa�..
    //wyliczamy �redni� g�sto�ci wszystkich udzia��w materia��w w 'ob'
    //(�e te� nie ma takiego makra np. w math.h, ehh :P)
    //czyli: obiekt wpada do wody po raz pierwszy, bo nie ma wyliczonej
    // jeszcze SUMY_GESTOSCI 
    if(!ob->query_prop(SUMA_GESTOSCI))
        srednia = srednia_gestosci(ob);
    else //obiekt ma ju� wyliczon� sum� g�sto�ci, wi�c szcz�dzamy na procku :)
        srednia = ob->query_prop(SUMA_GESTOSCI);
            
        
    //eventowy plusk dla du�ych rzeczy :) tylko na lokacji powierzchni wody.
    if(ob->query_prop(OBJ_I_WEIGHT) >= 4000 &&
        ob->query_prop(OBJ_I_VOLUME) >= 4000 &&
            ENV(ob)->jestem_lokacja_powierzchni_wody())
    {
        tell_room(ENV(ob),"Wyrzucon"+ob->koncowka("y","a","e")+" w�a�nie "+
            ob->short(PL_MIA)+" wpada z g�o�nym pluskiem do wody, chlapi�c "+
            "na wszystkie strony.\n");
            
        foreach(object x : filter(AI(ENV(ob)), &->query_lodka()))
            tell_room(x->query_wnetrze(),
                       "Wyrzucon"+ob->koncowka("y","a","e")+" w�a�nie "+
                ob->short(PL_MIA)+" wpada z g�o�nym pluskiem do wody, chlapi�c "+
                "na wszystkie strony.\n");
    }
        
        
        
    if(srednia >= 1.0) //we are sinking, we arre sinking!
    {
        float czas;
        switch(ftoi(srednia)) //ustawmy czas toni�cia, im wi�ksza
        {   //�rednia g�sto�ci, tym szybciej
            case 1: czas = 10.0; break;
            case 2: czas = 7.0; break;
            case 3: czas = 5.0; break;
            case 4: czas = 3.0; break;
            case 5..6: czas = 2.0; break;
            default: czas = 1.0; break;
        }
        set_alarm(czas, 0.0, "obiekt_tonie",ob, ENV(ob));
    }
    else //floating
    {
         //no c�...tutaj obiekt po prostu p�ywa, wi�c chyba nic nie trzeba robi�.
    }
       
}


/*
 * funkcja usuwaj�ca dany alarm
 */
void
usun_alarm(int alarm)
{
DBG("usuwam alarm: "+alarm);

    if(alarm)
        remove_alarm(alarm);
    return;
}


void
wspolne_enter_inv(object ob, object from)
{
  
    //��dki... to inna para kaloszy :)
    if(ob->query_lodka())
        return;
    
    //a rybki? :)
    if(ob->query_type() == O_RYBY)
    {
        ob->remove_object();
        return;
    }
    
    //no to gagatek jest w wodzie!
    if(ob->is_living())
    {   
        //a gdy b�d� uuuumiera�, uuuumiera�, uuuuumiera�...
        if(ob->query_prop(TONIE))
        {
            if(!ob->query_prop(IL_POWIETRZA))
            {   //przy toni�ciu bierzemy z sob� zwykle mniej powietrza ni� przy nurkowaniu
                ob->add_prop(IL_POWIETRZA, IL_POWIETRZA_KRYTYCZNEGO);
                ob->add_prop(IL_POWIETRZA_MAX, ustal_ilosc_powietrza(ob));
            }
            
            ob->add_prop(LIVE_S_EXTRA_SHORT," ton�c"+ob->koncowka("y","a","e")+" w wodzie");
        }
        
        if(TO->query_prop(ROOM_I_TYPE) == ROOM_UNDER_WATER && !ob->query_prop(POWIETRZE_ALARM))
            ob->add_prop(POWIETRZE_ALARM, set_alarm(1.0, 3.0, "redukuj_powietrze", ob));
            
        if(!ob->query_prop(PIERWSZA_ALARM_LOK)) //To znaczy, �e ob wskakuje po raz pierwszy
        {   //i to w�a�nie TA lokacja (this_object()) b�dzie przetrzymywa� jego alarm na p�ywanie
            ob->add_prop(PIERWSZA_ALARM_LOK, TO);
            ob->add_prop(PLYWAM_ALARM, set_alarm(2.0, 4.0, "plywam", ob)); //jako prop, bo musimy zna� ID alarmu do
                                                                            //jego usuni�cia
            ob->add_prop(LIVE_S_EXTRA_SHORT," p�ywaj�c"+ob->koncowka("y","a","e")+" w wodzie");
        }
        
        
        //gasn� jego lampy...
        foreach(object lampa : filter(AI(ob), &->query_lit()))
            lampa->extinguish_me();
        
        //wywalamy zamoczenie, bo b�dziemy dawa� nowe przy wyj�ciu z wody
        foreach(object kropelki : filter(AI(ob), &->query_jestem_efektem_przemoczenia()))
            kropelki->wysusz();
    }
    else //co� wyrzucamy za burt�...
        obiekt_wpadl_do_wody(ob, from);
    
}

