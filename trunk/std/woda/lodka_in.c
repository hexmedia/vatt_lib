/* Jest to lokacja wn�trza ��dki. C�z tu mog� powiedzie�...
 * Tyle nad tym pracowa�em, �e ju� sam nie wiem co napisa�.
 * ��dki maj� mas� ficzer�w, nie spos�b tego wszystkiego tu opisa�.
 * Komendy w �rodku, czyli tutaj, to: zacumuj,odcumuj, wyskocz, wskocz, wyrzu�, wy��w
 *
 * 
  
 TODO: ??
 -wy�awianie
-przeci��anie i od razu ograniczanie pojemno�ci/ci�aru
-eventy zm�czeniowe sprawdzaj�ce jeszcze czy jest og�lny przyrost, czy maleje.... (?? kurde, nie wiem o co mi chodzi�o
                                                         -jak mo�e by� og�lny przyrost? p�ywaj�c m�czymy si� i ju�)



 w lokacji do cumowania dac:
 string
query_short_zew()
{
    return "Przy pomo�cie";
}


    autor: Vera
    data:  kaniku�y 2009
*/

inherit "/std/room";
inherit "/std/ryby/lowisko";

#include <cmdparse.h>
#include <filter_funs.h>
#include <macros.h>
#include <options.h>
#include <pl.h>
#include <stdproperties.h>
#include <speech.h>
#include <sit.h>
#include <ss_types.h>
#include <woda.h>
#include <exp.h>

int wyjdz(string str);
void krzyknij(string str);
void dodaj_komende(string cmd, string os3, string inf);
void set_lodka(object obj);
void rozkolysz(int o_ile = 0);
int filtruj_niewidoczne(object ob);

object lodka;
mixed *komendy = ({ });
string *rooms = ({}), //pomieszczenia na ktore mo�emy si� przemieszcza� �odzi�.
        *cmds = ({});//a to komendy...
        
string short_env = "";//short i long lokacji na ktorej jest ��dka
string long_env = ""; 
string opis_wnetrza_lodki = ""; //zamiast set_long w lodka_in ustawiamy to

string *nazwy = ({ }); //nazwy do add_item ��dka :) ustawiane w enter_inv
int opis_flag = 0; //1 je�li ma poda� long, czyli wpisujemy 'sp' czy co� takiego,
                    //0 je�li przemieszczamy si�, lub wpisujemy tylko 'zerknij'
int kolysze_sie = 0, //poziom rozko�ysania ��dki
    kolysze_sie_alarm,
    przestan_kolysac_alarm;

void create_wnetrze()
{
}

void dodaj_komende(string cmd, string os3, string inf)
{
    komendy += ({ ({cmd})+({os3})+({inf}) });
}

void
sprawdz_czy_jeszcze_walka_i_rozkolysz(float i)
{
    foreach(object kto : FILTER_LIVE(AI(TO)))
        if(kto->query_attack())
        {
            rozkolysz();
            set_alarm(i, 0.0, "sprawdz_czy_jeszcze_walka_i_rozkolysz", i+itof(random(4))+1.0);
            return;
        }
}

int
no_attack()
{
    rozkolysz(3);
    set_alarm(2.0, 0.0, "sprawdz_czy_jeszcze_walka_i_rozkolysz", 2.0);
    return 0;
}

//��dka uspokaja si�:
void
przestan_kolysac()
{
    //je�li jeszcze kto� na ��dce walczy, to nie przestajemy ko�ysa�.
    foreach(object kto : FILTER_LIVE(AI(TO)))
        if(kto->query_attack())
            return;
    
    kolysze_sie = 0;
    kolysze_sie_alarm = 0;
    tell_room(TO,QCSHORT(lodka,PL_MIA)+" przestaje si� ko�ysa�.\n");
}

//a to ju� w�a�ciwie ko�ysanie:
void
kolyszemy()
{
    if(!get_alarm(przestan_kolysac_alarm))
    {
        przestan_kolysac_alarm = set_alarm(2.0+itof(random(4)),0.0,"przestan_kolysac");
        return;
    }
    
    switch(kolysze_sie)
    {
        case 1..4:
            if(random(4)) // 75% szans? :)
            {
                tell_room(TO,QCSHORT(lodka,PL_MIA)+" zaczyna si� niebezpiecznie ko�ysa�!\n");
                tell_room(ENV(lodka),QCSHORT(lodka,PL_MIA)+" zaczyna si� niebezpiecznie ko�ysa�.\n");
            }
                
            break;
            
        default:
            //kto� wypada. randomowo. 
            object *wypada = ({});
            foreach(object kto : FILTER_LIVE(AI(TO)))
                if(random(2))
                    wypada += ({kto});
                
            if(!sizeof(wypada))
                break;
            
            object gdzie;
            if(objectp(ENV(lodka)->query_kapielisko()))
                gdzie = ENV(lodka)->query_kapielisko();
            else
                gdzie = ENV(lodka);
            
            tell_room(TO,COMPOSITE_LIVE(wypada,PL_MIA)+" nagle trac"+
            (sizeof(wypada)>1?"�":"i")+" r�wnowag� i wypada"+
            (sizeof(wypada)>1?"j�":"")+ " za burt�!\n", wypada);
            foreach(object wypad : wypada)
            {
                wypad->catch_msg("Nagle tracisz r�wnowag� i wypadasz za burt�!\n");
                
                //no i jego subinventory(0) te� wypada...
                object *inv = wypad->subinventory(0);
                inv = filter(inv, &filtruj_niewidoczne());
                foreach(object a : inv)
                    a->move(gdzie,1);
                
                wypad->move(gdzie, 1);
                wypad->do_glance(1);
                wypad->stop_fight();
            }
            tell_room(ENV(wypada[0]),COMPOSITE_LIVE(wypada,PL_MIA)+" wpada"+
            (sizeof(wypada)>1?"j�":"")+ " z "+QSHORT(lodka,PL_DOP)+" do wody!\n", wypada);
            
            //zerujemy z powrotem.
            kolysze_sie = 0;
            
            break;
    }
}

//wywo�ywane gdy ��dka ma si� rozko�ysa�, podwy�sza poziom rozko�ysania (kolysze_sie)
//gdy arg == 0 to podwy�sza o 1, gdy nie to podwy�sza o o_ile :P
void
rozkolysz(int o_ile = 0)
{
    if(!o_ile)
        kolysze_sie++;
    else
        kolysze_sie += o_ile;
    
    if(!get_alarm(kolysze_sie_alarm))
        kolysze_sie_alarm = set_alarm(1.0+itof(random(3)),0.0,"kolyszemy");
}


object
*poka_obiekty(object skad)
{
    object *co = FILTER_SHOWN(AI(skad)) - ({lodka, TP});
    if(sizeof(co))
        return co;
    else
        return ({});
}

public string
exits_description()
{
    string s = ENV(lodka)->exits_woda_description();
    
    if(stringp(s) && strlen(s))
        return s;
    else
        return ::exits_description();
}

string
query_co_na_wodzie( int kropka = 0)
{
    object *co = poka_obiekty(ENV(lodka));
    string str = ""; //".\n";
    if(!sizeof(co))
        return "";
    
    str += "Na zewn�trz dostrzegasz "+ COMPOSITE_DEAD(co,PL_BIE);
    
    if(kropka)
        str += ".\n";
    
    return str;
}


string
opisy_zew()
{
    if(!objectp(lodka))
    {
        //return "Wyst�pi� b��d nr 91f3 ! Zg�o� to koniecznie!\n";
    }
    
    string poczatek = TO->show_sit();
    
    if(!strlen(poczatek))
        poczatek = COMPOSITE_LIVE(FILTER_LIVE(AI(TO)),PL_MIA);
        
    
    //lokacje na kt�rych jest zacumowana ��dka s� specyficzne
    if(strlen(lodka->czy_zacumowana()))
        return /*ENV(lodka)->query_short_zew() +".\n"+*/ long_env + query_co_na_wodzie(1);
    else
        return /*short_env + ".\n" +*/ long_env + query_co_na_wodzie(1);
    //return ENV(lodka)->short();
}

string
short()
{    //kolorki jak na innych lokacjach, bajer ;p
    string str = set_color(36)+set_color(1) ;
    string short_zew = ENV(lodka)->query_short_zew();
    
    if(strlen(short_zew))
        str += short_zew;
    else
        str += short_env;
    
    str += clear_color();
    
    if(member_array(query_verb(),({"ob","obejrzyj","sp","sp�jrz"})) != -1) //if to jedno z tych komend
    { //write("longiem ? \n");
        opis_flag = 1;
    }
    else
    {
        opis_flag = 0;
        //str += "\n"+ENV(lodka)->exits_description() ;
        str += (sizeof(poka_obiekty(ENV(lodka))) ? ".\n" : "") + query_co_na_wodzie();
    }
    //write("(str: "+str+")");
    return str;
}


nomask void
create_room()
{
    //dodaj_komende("wyskocz", "wyskakuje", "wyskoczy�");

    //w �odzi nie mo�na si� bi�
    add_prop(ROOM_M_NO_ATTACK, &no_attack());
    //ani si� ukrywa�
    add_prop(ROOM_I_HIDE,-1);
    remove_prop(ROOM_I_INSIDE);
    //tu nie wolno tez rzucac
    add_prop(ROOM_CANT_THROW,"To miejsce jest zbyt ma�e, by "+
        "m�c tutaj rzuca�!\n");       
    add_prop(ROOM_NO_FIRE,1);
    //zawarto�� w 'ob ��dk�' :)
    add_prop(CONT_I_DONT_SHOW_CONTENTS, 1);
        
    create_wnetrze();
    
    set_long("@@opisy_zew@@");
}

//ta funkcja dodaje teraz wiose�ka...
void
dodaj_wiosla_teraz(int ile)
{   
    while(ile)
    {
        object wioslo = clone_object("/std/woda/wioslo");
        wioslo->move(TO, 1);
        --ile;
    }
}
//funkcja �aduj�ca od razu...(no mo�e nie tak od razu) wios�a do ��dek.
void
dodaj_wioslo(int ile)
{
    set_alarm(1.0,0.0,"dodaj_wiosla_teraz",ile);
}

//nadpisujemy funkcje z /std/room/description.c = wy�wietlania rzeczy w longu,
//w ��dce nic nie wy�wietlamy. dopiero w 'ob lodke' widac zawartosc.
public int
czy_wyswietlic_rzecz(object rzecz)
{
    return 0;
}

int
zacumuj(string str)
{
    notify_fail("Zacumuj gdzie?\n");
    
    if(!str) return 0;
    
    if(!objectp(lodka))
    {
       notify_fail("Wyst�pi� b��d w ��dce nr 8894e. Prosz� zg�o� "+
        "go, podaj�c ten numer wraz z okoliczno�ciami w jakich to wystapi�o.\n");
        return 0;
    }
    
    if(strlen(lodka->czy_zacumowana()))
    {
        notify_fail(lodka->koncowka("Ten","Ta","To")+" "+lodka->short(PL_MIA)+
        " jest ju� zacumowan"+lodka->koncowka("y","a","e")+".\n");
        return 0;
    }
    
    if(!ENV(lodka)->is_kapielisko())
    {
        notify_fail("Tu nie ma gdzie zacumowa�.\n");
        return 0;
    }
    
    string gdzie = ENV(lodka)->query_cumowanie();
    
    if(str != gdzie)
        return 0;
    
    lodka->zacumuj(gdzie);
    write("Cumujesz "+lodka->short(PL_MIA)+" "+gdzie+".\n");
    saybb(QCIMIE(TP,PL_MIA)+" cumuje "+lodka->short(PL_MIA)+" "+gdzie+".\n");
    tell_room(ENV(lodka),"Znajduj�c"+TP->koncowka("y","a","e")+" si� na "+
    lodka->short(PL_MIE)+" "+QCIMIE(TP,PL_MIA)+" cumuje "+
    lodka->koncowka("go","j�","je")+".\n");
    
    return 1;
}

int
odcumuj(string str)
{
    notify_fail("Odcumuj co?\n");
    
    if(!str) return 0;
    
    if(!objectp(lodka))
    {
       notify_fail("Wyst�pi� b��d w ��dce nr 7894d. Prosz� zg�o� "+
        "go, podaj�c ten numer wraz z okoliczno�ciami w jakich to wystapi�o.\n");
        return 0;
    }
    
    if(!strlen(lodka->czy_zacumowana()))
    {
        notify_fail(lodka->koncowka("Ten","Ta","To")+" "+lodka->short(PL_MIA)+
        " nie jest nigdzie zacumowan"+lodka->koncowka("y","a","e")+".\n");
        return 0;
    }
    
    object obj;
    
    if(!parse_command(lower_case(str), ({lodka}), "%o:"+PL_BIE, obj))
        return 0;
    
    if(obj != lodka)
        return 0;
    
    lodka->odcumuj();
    write("Odcumowujesz "+lodka->short(PL_BIE)+".\n");
    saybb(QCIMIE(TP,PL_MIA)+" odcumowuje "+lodka->short(PL_BIE)+".\n");
    tell_room(ENV(lodka),"Znajduj�c"+TP->koncowka("y","a","e")+" si� na "+
    lodka->short(PL_MIE)+" "+QCIMIE(TP,PL_MIA)+" odcumowuje "+
    lodka->koncowka("go","j�","je")+".\n");
    
    return 1;
}

int
filtruj_niewidoczne(object ob)
{
    return !(ob->query_no_show() || ob->query_no_show_composite() ||
        ob->query_prop(OBJ_I_DONT_INV) || !ob->query_prop(OBJ_M_NO_DROP));
}

int
wyskocz(string str)
{
    if(!objectp(lodka))
    {
       notify_fail("Wyst�pi� b��d w ��dce nr 44g94e. Prosz� zg�o� "+
        "go, podaj�c ten numer wraz z okoliczno�ciami w jakich to wystapi�o.\n");
        return 0;
    }
   
    object gdzie;
    notify_fail(capitalize(query_verb())+" do wody?\n");
    
    if(str != "do wody")
        return 0;
    
    if(ENV(lodka)->query_kapielisko())
        gdzie = ENV(lodka)->query_kapielisko();
    else
        gdzie = ENV(lodka);
    
    if(gdzie->query_prop(ROOM_I_TYPE) != ROOM_IN_WATER)
    {
       notify_fail("Wyst�pi� b��d w ��dce nr 9x99t94. Prosz� zg�o� "+
        "go, podaj�c ten numer wraz z okoliczno�ciami w jakich to wyst�pi�o.\n");
        return 0;
    }
    
    if(TP->query_prop(SIT_SIEDZACY) || TP->query_prop(SIT_LEZACY))
    {
        write("A mo�e najpierw wstaniesz?\n");
        return 1;
    }
    
    //z inventory nie wejdziemy.
    object *inv = TP->subinventory(0);
    inv = filter(inv, &filtruj_niewidoczne());
    if(sizeof(inv))
    {
        write("Musisz od�o�y� "+ COMPOSITE_DEAD(inv, PL_BIE) +", by m�c to zrobi�.\n");
        return 1;
    }
    
    if(!HAS_FREE_HANDS(TP))
    {
        write("Musisz mie� obie r�ce wolne, by si� wyk�pa�.\n");
        return 1;
    }
    
    //event dla tych pod wod�:
     if(objectp(ENV(lodka)->query_dol()) && !random(2))
         tell_room(ENV(lodka)->query_dol(), "Z g�ry dobiegaj� ci� jakie� odg�osy - "+
         "chyba w�a�nie kto� wskoczy� do wody.\n");
    
    write("Wskakujesz do wody.\n");
    say(QCIMIE(TP,PL_MIA)+" wskakuje do wody.\n");
    
    object kap;
    if(objectp(kap = ENV(lodka)->query_kapielisko()))
        TP->move(kap,1);
    else
        TP->move(ENV(lodka),1);
    
    TP->do_glance(1);
    say(QCIMIE(TP,PL_MIA)+" wyskakuje z "+lodka->short(PL_DOP)+" do wody.\n");
    
    return 1;
}

void
koniec_paralizu_na_wylawianie(object *xxx,object kto,object *co)
{
//    write("todo: testy na wag�, nasz� si�e itepe.\n");
    object *wylowione = ({});
    foreach(object x : co)
    {
        //if(x->query_prop(OBJ_I_WEIGHT) < 10000 && !x->is_living() && )
           wylowione += ({x});
    }
    
    write("powiedzmy, �e wy�owi�e� wszystko. \n");
    
    wylowione->move(TP);
    return;
}

void
koniec_paralizu_(object *co,object kto,int i)
{
    tell_room(ENV(lodka),capitalize(lodka->short(PL_MIA))+" oddala si� na "+cmds[i]+".\n");  
    
    foreach(object x : filter(AI(ENV(lodka)) - ({lodka, kto}), &->query_lodka()))
        tell_room(x->query_wnetrze(),capitalize(lodka->short(PL_MIA))+" oddala si� na "+cmds[i]+".\n");
 
    string skad = " nik�d";
    switch(cmds[i])
    {
        case "p�noc" : skad = " po�udnia"; break;
        case "p�nocny-zach�d" : skad = " po�udniowego-wschodu"; break;
        case "zach�d" : skad = "e wschodu"; break;
        case "po�udniowy-zach�d" : skad = " p�nocnego-wschodu"; break;
        case "po�udnie" : skad = " p�nocy"; break;
        case "po�udniowy-wsch�d" : skad = " p�nocnego-zachodu"; break;
        case "wsch�d" : skad = " zachodu"; break;
        case "p�nocny-wsch�d" : skad = " po�udniowego-zachodu"; break;
        case "pomost" : skad = " jeziora"; break;
    }

    lodka->move(rooms[i], 1);
    tell_room(ENV(lodka),capitalize(lodka->short(PL_MIA))+" nadp�ywa z"+skad+".\n");

    foreach(object x : filter(AI(ENV(lodka)) - ({lodka, kto}), &->query_lodka()))
        tell_room(x->query_wnetrze(),capitalize(lodka->short(PL_MIA))+" nadp�ywa z"+skad+".\n");

    object old_player = TP;
    foreach(object x : FILTER_LIVE(AI(TO)))
    {
        set_this_player(x);
        x->do_glance(1);
    }
    set_this_player(old_player);

    remove_prop(WIOSLUJE);
    
    //m�cz�ce nieco nieco!
    kto->add_fatigue(-(random(180) + 430 ));
    
    //expik? :)
    kto->increase_ss(SS_STR, EXP_WIOSLUJE_STR);
}

object *
query_obsy_za_burta(string str)
{
    object *obsy = ({ });
    if(parse_command(lower_case(str), ENV(lodka)," %i:"+PL_BIE, obsy))
    {
        obsy = CONTAINER_ACCESS(obsy, ENV(lodka));
        obsy = FILTER_SHOWN(obsy);
        obsy -= ({lodka});
    }
    return obsy;
}

//funkcja ta przechwytuje wszystko co wpisujemy, b�d�c na ��dce
public int
kierunki(string str)
{
    string cmd = query_verb();
    int i = -1,
        ob_flag = 0;
 
    if(cmd ~= "p�y�" && strlen(str) > 3 && str[0..1] ~= "na")
        cmd = str[3..];
    if(cmd ~= "wios�uj" && strlen(str) > 3 && str[0..1] ~= "na")
        cmd = str[3..];    

    //trzeba zmieni� te� 'sp na kierunek':
    if(cmd ~= "sp" || cmd ~= "sp�jrz" || cmd ~="ob" || cmd~="obejrzyj")
    {
        if((cmd != "ob" && cmd !="obejrzyj") && strlen(str) > 3)
            cmd = str[3..];
    }

    
    //trzeba zmienic skr�ty na pe�ne nazwy:
    switch(cmd)
    {
        case "n": case "polnoc": cmd = "p�noc"; break;
        case "ne": case "polnocny-wschod": cmd = "p�nocny-wsch�d"; break;
        case "nw": case "polnocny-zachod": cmd = "p�nocny-zach�d"; break;
        case "e": case "wschod": cmd = "wsch�d"; break;
        case "se": case "poludniowy-wschod": cmd = "po�udniowy-wsch�d"; break;
        case "s": case "poludnie": cmd = "po�udnie"; break;
        case "sw": case "poludniowy-zachod": cmd = "po�udniowy-zach�d"; break;
        case "w": case "zachod": cmd = "zach�d"; break;
        case "p�noc":
        case "p�nocny-wsch�d":
        case "wsch�d":
        case "po�udniowy-wsch�d":
        case "po�udnie":
        case "po�udniowy-zach�d":
        case "zach�d":
        case "p�nocny-zach�d":
            break;
    }

    i = member_array(cmd,cmds);
    
    if(i == -1)
        cmd = "dupa";
    
    if(query_verb() ~= "sp" || query_verb() ~="sp�jrz" || query_verb() ~= "ob" || query_verb() ~= "obejrzyj")
        ob_flag = 1;
    
    
     //sp na kierunek 
     if(ob_flag && cmd != "dupa")
     {   
        write("Spogl�dasz na "+cmd+":\n");
        saybb(QCIMIE(TP,PL_MIA)+" spogl�da na "+cmd+".\n");
        TP->daj_paraliz_na_sp((i==-1 ? "NONE" : cmd),rooms[i]);
        return 1;
     }


     //ob co� za burt�...
     if(ob_flag && cmd == "dupa" && strlen(str))
     {
            if(str[0..2] == "na ") //�eby 'str' by�o tylko opisem tego CO chcemy obejrze�.
                str = str[3..];

            object *obsy_za_burta = query_obsy_za_burta(str);
            
            if(sizeof(obsy_za_burta))
            {
                //je�li kt�re� z nich jest ��dk�, to wypisujemy te� livingi na niej
                foreach(object lo : obsy_za_burta)
                    if(lo->query_lodka())
                    {
                        object *livingi = FILTER_LIVE(AI(lo->query_wnetrze()));
                        write("Na zewn�trz dostrzegasz "+
                            (strlen(lo->czy_zacumowana()) ? "zacumowan"+lo->koncowka("ego","�","e")+
                            " "+lo->czy_zacumowana() : "p�ywaj�c"+lo->koncowka("ego","�","e") )+" "+
                            lo->short(PL_BIE)+
                            (sizeof(livingi) ? ", a w "+lo->koncowka("nim","niej")+" "+
                            COMPOSITE_LIVE(livingi,PL_BIE) : "" ) + ".\n");
                            
                        obsy_za_burta -= ({lo});
                    }
                if(sizeof(obsy_za_burta))
                    write("Widzisz "+COMPOSITE_DEAD(obsy_za_burta,PL_BIE)+" "+
                        (ENV(obsy_za_burta[0])->query_kapielisko() ? "na zewn�trz" :"za burt�")+".\n");
                
                return 1;
            }
     }
     
     
     //mo�e komenda to jedna z tych, kt�re ko�ysz� �odzi�:
     if((query_verb() ~= "podskocz" && !str) || (query_verb() ~= "zata�cz" && (!str || str[0..1] == "z ")))
         rozkolysz();


     /*if(member_array(cmd,({"p�noc","p�nocny-wsch�d","wsch�d","po�udniowy-wsch�d",
            "po�udnie","po�udniowy-zach�d","zach�d","p�nocny-zach�d"})) != -1)
         return 0;*/
            
            //write(i+"\n");
    //if wpisywana komenda to �adne z wyj�� dost�pnych na ENV(lodka) :
    if(cmd == "dupa")
        return 0;
    /*if(i == -1 && cmd != "dupa")
    {
        write("Nie mo�esz tam pop�yn��.\n");
        return 1;
    }*/
    
    //sprawd�my czy trzymamy w r�kach wios�o
    object *wiosla = filter(CO_TRZYMA(TP), &->query_wioslo());
    if(!sizeof(wiosla))
    {
        notify_fail("Nie trzymasz �adnego wios�a, jak wi�c masz wios�owa�?\n");
        return 0;
    }
    
    
    if(TP->query_fatigue() <= 240)
    {
        write("Nie masz ju� si�.\n");
        return 1;
    }
    
    LOAD_ERR(rooms[i]);
    object lok = find_object(rooms[i]);
    if(lok->query_prop(ROOM_I_TYPE) != ROOM_IN_WATER &&
        lok->query_prop(ROOM_I_TYPE) != ROOM_BEACH)
    {
        write("Nie mo�esz tam pop�yn��.\n");
        return 1;
    }
    
    //zacumowana nie pop�ynie! :)
    if(strlen(lodka->czy_zacumowana()) && cmd != "dupa")
    {
        write("Wpierw musisz odcumowa� "+lodka->koncowka("tego","t�","to")+" "+
                lodka->short(PL_BIE)+".\n");
        return 1;
    }
    
    //kto� inny wios�uje
    if(query_prop(WIOSLUJE))
    {
        write("Zaczekaj, kto� ju� wios�uje.\n");
        return 1;
    }
    
    //wios�owa� mo�na tylko na siedz�co
    if(!TP->query_prop(SIT_SIEDZACY))
    {
        write("Wpierw usi�d�.\n");
        return 1;
    }

    //a teraz myk z uciekaniem tym, kt�rzy pr�buj� wspi�� si� na nasz� ��dk�! :))
        //sprawd�my wpierw czy kto� si� wspina:
    object *alpinisci = filter(AI(ENV(lodka)), &->query_prop(WSPINAM_SIE_NA_LODZ)),
            *para_,
            *wlasciwi_alpinisci = ({});
    if(sizeof(alpinisci)) //oho! kto� si� wspina :>
    {
        foreach(object alp : alpinisci)
        {
            //ka�dego alpinisty po kolei znajdzmy parali�e kt�re ma:
            para_ = filter(AI(alp),&->is_paralyze());
            //paraliz zawsze powinien by� tylko jeden,
            //ten aktualny
            if(!sizeof(para_)) //just in case
                continue; //przerywamy p�tl�.
            //je�li parali� nie dotyczy tej ��dki, to te� przerywamy p�tl�:
            if(para_[0]->query_finish_object() != lodka)
                continue;

            //ok, teraz ju� wiemy, �e 'alp' wspina si� na nasz� ��dk�!
            //przerywamy wi�c jego niecny plan:
            para_[0]->stop_paralyze();
            alp->catch_msg("Nie udaje ci si� wspi�� na "+lodka->short(PL_BIE)+
            ", poniewa� "+lodka->koncowka("ten","ta","to")+" zaczyna odp�ywa�.\n");
            
            //dodajmy gagatka do naszej listy, zeby pokazac p�niej we write list�:
            wlasciwi_alpinisci += ({alp});
        }
        
        write("Udaremniasz pr�b� wej�cia na pok�ad "+
                COMPOSITE_LIVE(wlasciwi_alpinisci,PL_CEL)+".\n");
    }
    //koniec myku z uciekaniem tym co si� zakradaj� na nasz pok�ad :)

    
    //czas p�yni�cia od si�y
    int czas = 2;
    switch(TP->query_stat(SS_STR))
    {
        case 0..19:  czas = 6; break;
        case 20..35: czas = 5; break;
        case 36..69: czas = 4; break;
        case 70..89: czas = 3; break;
    }
    
    object p=clone_object("/std/paralyze.c");
    p->set_finish_object(TO);
    p->set_finish_fun("koniec_paralizu_",TP,i);
    p->set_remove_time(czas);
    p->set_stop_verb("przesta�");
    p->set_stop_message("Przestajesz wios�owa�.\n");
    p->set_fail_message("Chwil�! W�a�nie wios�ujesz!\n");
    p->move(TP,1);
    
    add_prop(WIOSLUJE,TP->query_real_name());
    
    write("Zaczynasz wios�owa� na "+cmd+".\n");
    saybb(QCIMIE(TP,PL_MIA)+" zaczyna wios�owa� na "+cmd+".\n");
    
    return 1;
}

int
wyrzuc(string str)
{
    notify_fail("Wyrzuc co [za burt�] ?\n");
        
    if(!str) return 0;
    
    if(!objectp(lodka))
    {
       notify_fail("Wyst�pi� b��d w ��dce nr 67d84c. Prosz� zg�o� "+
        "go, podaj�c ten numer wraz z okoliczno�ciami w jakich to wystapi�o.\n");
        return 0;
    }
    
    object *obj;
    if(!parse_command(lower_case(str), AI(TP), "%i:"+PL_BIE+ " [za] [burt�]", obj))
        return 0;
    
    obj = NORMAL_ACCESS(obj,0,0);
    
    if(!sizeof(obj))
        return 0;
    
    
    write("Wyrzucasz "+COMPOSITE_DEAD(obj,PL_BIE)+" za burt�.\n");
    saybb(QCIMIE(TP,PL_MIA)+" wyrzuca "+COMPOSITE_DEAD(obj,PL_BIE)+" za burt�.\n");
    
    obj->move(ENV(lodka),1);
    return 1;
}

int
wylow(string str)
{
    notify_fail("Wy��w co?\n");
    
    if(!str)
        return 0;
    
    object *obsy_za_burta = query_obsy_za_burta(str),
            *obsy2 = ({});
    
    if(!sizeof(obsy_za_burta))
        return 0;
    
    obsy2 += filter(obsy_za_burta, &->query_lodka()) + filter(obsy_za_burta, &->is_living());
    
    if(sizeof(obsy2))
    {
        write("Nie mo�esz wy�owi� "+COMPOSITE_DEAD(obsy2,PL_DOP)+".\n");
        return 1;
    }
    
    if(ENV(obsy_za_burta[0])->is_kapielisko())
    {
        write("Nie mo�esz wy�owi� "+COMPOSITE_DEAD(({obsy_za_burta[0]}),PL_DOP)+".\n");
        return 1;
    }
    
    write("Pr�bujesz wy�owi� "+COMPOSITE_DEAD(obsy_za_burta,PL_BIE)+".\n");
    say(QCIMIE(TP,PL_MIA)+" pr�buje wy�owi� "+COMPOSITE_DEAD(obsy_za_burta,PL_BIE)+".\n");
    
    object p=clone_object("/std/paralyze.c");
    p->set_finish_object(TO);
    p->set_finish_fun("koniec_paralizu_na_wylawianie",TP,obsy_za_burta);
    p->set_remove_time(2);
    p->set_stop_verb("przesta�");
    p->set_stop_message("Zaprzestajesz pr�by wy�owienia "+COMPOSITE_DEAD(obsy_za_burta,PL_BIE)+".\n");
    p->set_fail_message("Chwil�! W�a�nie pr�bujesz co� wy�owi�!\n");
    p->move(TP,1);
        
    return 1;
}

nomask void init_cmds()
{
    int cmdarr_size = sizeof(komendy);
    for(int i =0;i < cmdarr_size; i++)
    {
        mixed cmd = komendy[i][0];
        add_action("wyjdz", cmd);
    }
}

init()
{
    add_action(kierunki, "", 1);
    ::init();
    
    init_cmds();
    add_action(&krzyknij(), "krzyknij");
    add_action(zacumuj,"zacumuj");
    add_action(odcumuj,"odcumuj");
    
    
    add_action(wyskocz,"wyskocz");
    add_action(wyskocz,"wskocz");
    
    add_action(wyrzuc,"wyrzu�");
 
    add_action(wylow,"wy��w");
}

int wyjdz(string str)
{
    int cmdarr_size = sizeof(komendy);
    int cmdindex;

    for(int i = 0; i < cmdarr_size; i++)
    {
        mixed tmp = komendy[i][0];

        if(query_verb() ~= tmp)
        {
            cmdindex = i;
            break;
        }
    }

    if(!str)
    {
        notify_fail("Sk�d chcesz "+komendy[cmdindex][2]+"?\n");
        return 0;
    }

    if(!parse_command(lower_case(str), this_object(), " 'z' "+"'"+lodka->query_nazwa(PL_DOP)+"' "))
    {
        notify_fail("Sk�d chcesz "+komendy[cmdindex][2]+"?\n");
        return 0;
    }

    if(!objectp(lodka))
    {
        notify_fail("Ups. Wyst�pi� b��d nr 9d3k! Prosz� zg�o� to w ��dce "+
        "podaj�c ten numer jak i okoliczno�ci powstania b��du.\n");
        return 0;
    }

    if(TP->query_prop(PLAYER_M_SIT_SIEDZACY) != 0 || TP->query_prop(PLAYER_M_SIT_LEZACY))
    {
        notify_fail("Mo�e najpierw wstaniesz?\n");
        return 0;
    }

	if(!strlen(lodka->czy_zacumowana()))
	{
        notify_fail("Nie mo�esz "+komendy[cmdindex][2]+", poniewa� "+lodka->short(PL_MIA)+
        " nie jest nigdzie zacumowan"+lodka->koncowka("y","a","e")+".\n");
        return 0;
	}

    tell_roombb(ENV(lodka), QCIMIE(TP, PL_MIA)+" "+komendy[cmdindex][1]
			+" z "+lodka->query_nazwa(PL_DOP)+".\n");
	TP->move_living("M", ENV(lodka), 1);
	tell_roombb(this_object(), QCIMIE(TP, PL_MIA)+" "+komendy[cmdindex][1]
			+" z "+lodka->query_nazwa(PL_DOP)+".\n");
	return 1;
}

void set_lodka(object obj)
{
    lodka = obj;
}

void
set_opisy_zew(string ss, string ll)
{
    short_env = ss;
    long_env = ll;
}

void
opis_wnetrza(string s)
{
    opis_wnetrza_lodki = s;
}

string
query_opis_wnetrza()
{
    string toRet = opis_wnetrza_lodki;
    
    object *co = poka_obiekty(TO);
    
    if(sizeof(co))
        toRet += "W "+lodka->short(PL_MIE)+" dostrzegasz "+COMPOSITE_DEAD(co,PL_BIE)+".\n";
    
    return toRet;
}

string
shout_name()
{
    object pobj = previous_object(-1);

    if (ENV(pobj) == ENV(TP) &&
        CAN_SEE_IN_ROOM(pobj) && CAN_SEE(pobj, TP))
        return TP->query_Imie(pobj, PL_MIA);
    if (pobj->query_met(TP))
        return TP->query_name(PL_MIA);
    return TP->query_osobno() ?
           TP->koncowka("Jaki� m�czyzna", "Jaka� kobieta") :
           TP->koncowka("Jaki� ", "Jaka� ", "Jakie� ")
         + TP->query_rasa(PL_MIA);
}

string
shout_string(string shout_what)
{

    object pobj = previous_object(-1);
    int empty_string = shout_what == "0";

    if (ENV(pobj) == ENV(TP))
        return empty_string ? "krzyczy g�o�no." : "krzyczy: " + shout_what;

    return empty_string ? "krzyczy g�o�no z "+lodka->query_nazwa(PL_DOP)+"." :
    "krzyczy z "+lodka->query_nazwa(PL_DOP)+": " + shout_what;
}

int
krzyknij(string str)
{
    object env;
    object *lokacje;
    mixed Prop;
    if (Prop = TP->query_prop(LIVE_M_MOUTH_BLOCKED) != 0)
      {
	notify_fail(stringp(Prop) ? Prop :
		    "Nie jeste� w stanie wydoby� z siebie �adnego d�wi�ku.\n");
	return 0;
      }


    lokacje = ({ENV(lodka)});
    //dodajemy wszystkie ��dki na lokacji. (��cznie z nasz� w�asn�)
    foreach(object x : filter(AI(ENV(lodka)),&->query_lodka()))
        lokacje += ({x->query_wnetrze()});
    
    

    if (TP->query_option(OPT_ECHO))
        write("Krzyczysz" + (str ? ": " + str : " g�o�no.") + "\n");
    else
        write("Ju�.\n");
    int ile = sizeof(lokacje);
    while(ile--)
    tell_room(lokacje[ile], "@@shout_name:" + file_name(this_object())
                + "@@ @@shout_string:" + file_name(this_object()) + "|"
		+ SPEECH_DRUNK(str, TP) + "@@\n", ({TP}));
    return 1;
}

int
query_lodka_in()
{
    return 1;
}

public string
query_auto_load()
{
    return 0;
}

string
query_pomoc()
{
    string str = "";
    
    if(strlen(lodka->czy_zacumowana()))
        str += "Aby wyp�yn�� na g��bok� wod�, musisz wpierw odcumowa� "+
            lodka->koncowka("ten","t�","to")+" "+lodka->short(PL_BIE)+".\n";
    else
        str += "Je�li chcesz dobi� do brzegu, powin"+TP->koncowka("iene�","na�")+
            " wpierw zacumowa�.\n";
    
    str += "Skoro ju� tutaj jeste� mo�esz �mia�o skorzysta� z owej �odzi w celach "+
    "rekreacyjno-w�dkarskich. Pami�taj, �e do p�ywania "+lodka->koncowka("owym","ow�")+" "+
    lodka->short(PL_NAR)+" potrzebujesz tak�e wios�a. Mo�esz te� zawsze co� wyrzuci� za burt�, gdy sytuacja b�dzie "+
    "grozi�a przeci��eniem, bowiem "+lodka->query_name(PL_MIA)+" "+lodka->koncowka("ten",
    "ta")+" przeznaczon"+lodka->koncowka("y","a","e")+" jest dla niewielkiej ilo�ci "+
    "os�b. Analogicznie, balastu nabra� mo�esz tak�e pr�buj�c co� wy�owi� z jeziora, "+
    "za� gdy zechcesz si� orze�wi�, wystarczy wskoczy� do wody!\n";
    return str;
}

void
ustaw_lokacje_i_kierunki(string *rr, string *cc)
{
    if(!pointerp(rr) || !pointerp(cc))
    {
        write("Ups. Wyst�pi� b��d nr 792kd8! Prosz� zg�o� to w ��dce "+
        "podaj�c ten numer jak i okoliczno�ci powstania b��du.\n");
        return;
    }
    
    rooms = rr;
    cmds = cc;
}

public void
enter_inv(object ob, object from)
{
    ::enter_inv(ob, from);
    
    
    if(!objectp(lodka))
    {
        write("Ups. Wyst�pi� b��d nr 661ent3k! Prosz� zg�o� to w ��dce "+
        "podaj�c ten numer jak i okoliczno�ci powstania b��du.\n");
        return;
    }
    
    //dodajemy item ��dki do obejrzenia, je�li jeszcze go nie ma.
    if(!sizeof(nazwy))
    {
        nazwy = lodka->query_nazwy()[PL_BIE];
        add_item(nazwy,"@@query_opis_wnetrza@@");
    }
    
}


