/** 
 * Jest to obiekt ��dki. �ci�le powi�zany z jej wn�trzem lodka_in.c :)
 * Wszystkich ficzer�w wymienia� to ju� nie wymieni�, bo za du�o tego wszystkiego
 * Ot, mia�y by� niewinne ��dki, par� linijek kodu dla relaksu od naprawy b��d�w,
 * A wysz�o co� o wiele wi�kszego ;]
 *
 * @author Vera
 * @date kaniku�y 2009
 */

inherit "/std/container";

#include <macros.h>
#include <pl.h>
#include <stdproperties.h>
#include <object_types.h>
#include <materialy.h>
#include <filter_funs.h>
#include <sit.h>
#include <ss_types.h>
#include <cmdparse.h>
#include <woda.h>
#include <mudtime.h>

mixed *komendy = ({ });
object wnetrze;
string zacumowana = "";

/*
 przyk�ad chyba ilustruje wszystko:
 dodaj_komende(({"wskocz", "wskakuje","wskoczy�","do",PL_DOP}));
 */
void dodaj_komende(string *cmd);
void ustaw_wnetrze(string str);
string czy_zacumowana();
string co_na_lodce();
void zacumuj(string str);

void
create_lodka()
{
}

nomask void
create_container()
{
    
    setuid();
    seteuid(getuid());
    ustaw_nazwe("lodka");
    dodaj_nazwy("lodz");
    dodaj_nazwy("baczek");
    set_long("Zwyk�a ��dka.\n");
    
    add_prop(CONT_I_CLOSED, 1);
    add_prop(CONT_I_CANT_OPENCLOSE, 1);
    add_prop(CONT_I_DONT_SHOW_CONTENTS, 1);
    add_prop(CONT_I_CANT_WLOZ_DO, 1);
    add_prop(CONT_I_TRANSP, 0);
    
    set_type(O_WEDKARSKIE); //no a jakie? 

    add_prop(OBJ_I_NO_GET, "Hmmm... Ciekawe.\n");
    
    create_lodka();
    
    set_long(long()+"@@czy_zacumowana@@"+"@@co_na_lodce@@");
}

string
czy_zacumowana()
{
    if(strlen(zacumowana))
        return "W tej chwili jest zacumowana "+zacumowana+".\n";
    else
        return "";
}

string
co_na_lodce()
{
    if(!objectp(wnetrze))
        return "A na ��dce b��d nr 301k...Zg�o� go prosz�!\n";
    
    object *ekw = /*wnetrze->subinventory(0);*/
                FILTER_CAN_SEE(all_inventory(wnetrze),TP);
                
    if(!sizeof(ekw))
        return "";
    
    //mo�na te� u�y� ilosc(ile, "stoi", "stoja", "stoi")
    return "W "+short(PL_MIE)+" dostrzegasz "+COMPOSITE_DEAD(ekw,PL_BIE)+".\n";
}


//funkcja pomocnicza, zwracaj�ca notify_fail do
//pr�by wej�cia na pok�ad, je�li nie ma tam ju� dla nas miejsca...
string
sprawdz_czy_zmieszcze()
{
    string notif = "";
    int suma_obj=0, suma_wagi=0;
    
    foreach(object x : AI(wnetrze))
    {
        suma_obj += x->query_prop(OBJ_I_VOLUME);
        suma_wagi += x->query_prop(OBJ_I_WEIGHT);
    }

    if(suma_obj >= TO->query_prop(CONT_I_MAX_VOLUME))
        notif = "Wygl�da na to, �e nie zmie�cisz si� ju� do "+TO->query_nazwa(PL_DOP)+".\n";
    if(suma_wagi >= TO->query_prop(CONT_I_MAX_WEIGHT))
        notif = "Wygl�da na to, �e "+TO->query_nazwa(PL_DOP)+" jest ju� przeci��on"+
        koncowka("y","a","e")+".\n";
    
    return notif;
}

void
zacumuj(string str)
{
    zacumowana = str;
}

void
odcumuj()
{
    zacumowana = "";
}

void
dodaj_komende(string *cmd)
{
    komendy += ({cmd});
}

void ustaw_wnetrze(string str)
{
    wnetrze = clone_object(str);
    wnetrze->set_lodka(TO);
}

object
query_wnetrze()
{
    return wnetrze;
}

int
wsiadz(string str)
{    
    object obj;
    int cmdarr_size = sizeof(komendy);
    int index;
    
    notify_fail(capitalize(query_verb())+" gdzie?\n");
    
    if(!strlen(str))
        return 0;
    
    //mo�e pr�bujemy wle�� na ��dk� b�d�c w wodzie...
    //a za to odpowiedzialna jest inna funkcja:
    if(ENV(TP)->query_prop(ROOM_I_TYPE) == ROOM_IN_WATER)
        return 0;
    
    if(ENV(TO)->pora_roku() == MT_ZIMA)
    {
        notify_fail("Pokrywa wody skuta jest grubym lodem, wi�c nie ma jak pop�ywa�.\n");
        return 0;
    }
    
    if(!sizeof(komendy))
    {
        notify_fail("Wyst�pi� b��d w ��dce nr 234ve. Prosz� zg�o� "+
        "go, podaj�c ten numer wraz z okoliczno�ciami w jakich to wystapi�o.\n");
        return 0;
    }
    
    for(int i = 0; i < cmdarr_size; i++)
    {
        mixed tmp = komendy[i][0];
        if(query_verb() ~= tmp)
        {
            index = i;
            break;
        }
    }
    
    if(!parse_command(lower_case(str), AI(ENV(TP)), " '"+komendy[index][3]+
                "' %o:"+komendy[index][4], obj))
        return 0;
        
    if(obj != TO)
        return 0;

    if(ENV(TP)->query_lodka() || ENV(TP) != ENV(TO)) 
        return 0;
    
    //=======OK :) Dobra ��dka wybrana.==========
    
    if(!objectp(wnetrze))
    {
        notify_fail("Wyst�pi� b��d w ��dce nr 654cce. Prosz� zg�o� "+
        "go, podaj�c ten numer wraz z okoliczno�ciami w jakich to wystapi�o.\n");
        return 0;
    }
    
    //na siedz�co/le��co nie wsi�dziemy
    if(TP->query_prop(SIT_SIEDZACY) || TP->query_prop(SIT_LEZACY))
    {
        write("A mo�e najpierw wstaniesz?\n");
        return 1;
    }
    
    //sprawdzamy czy zacumowana
    if(!strlen(zacumowana))
    {
        write("Nie mo�esz tam "+komendy[index][2]+" poniewa� "+koncowka("ten","ta","to")+
        " nie jest zacumowan"+koncowka("y","a","e")+".\n");
        return 1;
    }
    
    //sprawdzamy czy si� zmie�cimy
    int suma_obj=0, suma_wagi=0;
    foreach(object x : AI(wnetrze))
    {
        suma_obj += x->query_prop(OBJ_I_VOLUME);
        suma_wagi += x->query_prop(OBJ_I_WEIGHT);
    }

    if(suma_obj >= TO->query_prop(CONT_I_MAX_VOLUME))
    {
        notify_fail("Wygl�da na to, �e nie zmie�cisz si� ju� do "+TO->query_nazwa(PL_DOP)+".\n");
        return 0;
    }
    if(suma_wagi >= TO->query_prop(CONT_I_MAX_WEIGHT))
    {
        notify_fail("Wygl�da na to, �e "+TO->query_nazwa(PL_DOP)+" jest ju� przeci��on"+
        koncowka("y","a","e")+".\n");
        return 0;
    }
    /*
    int wynik =TP->move(wnetrze);
    
    if(wynik) //1 lub 8, ale uog�lnijmy.
    {
        notify_fail("Chyba si� tam ju� nie zmie�cisz!\n");
        return 0;
    }
    else
    {*/
    saybb(QCIMIE(TP, PL_MIA)+" "+komendy[index][1]+" "+komendy[index][3]+" "+
            TO->short(komendy[index][4])+".\n");
    
    TP->move_living("M", wnetrze, 1);
    
    saybb(QCIMIE(TP,PL_MIA)+" "+komendy[index][1]+" "+komendy[index][3]+" "+
            TO->short(komendy[index][4])+".\n");
    
    
    return 1;
}

/*ta funkcja wywo�uje si�, gdy ko�czy si� parali� na wspinanie 
 si� z wody na ��dk� */
void
koniec_paralizu_na_wspinanie_sie(object *co, object kto)
{
    //to bardzo m�cz�ca czynno��! :) (sprawdzone empirycznie...)
    kto->add_fatigue(-(kto->query_max_fatigue() / 4));    
    kto->remove_prop(WSPINAM_SIE_NA_LODZ);
    //i nawet nie wiadomo czy nam si� uda:
    if(kto->query_stat(SS_STR) < 40 + random(10) || !random(5))
    {
        kto->catch_msg("Nie udaje ci si� wspi��.\n");
        tell_room(ENV(kto),QCIMIE(kto,PL_MIA)+" zrezygnowan"+kto->koncowka("y","a","e")+
        " zaprzestaje pr�by wspi�cia si� na "+short(PL_BIE)+".\n",({kto}));
        return;
    }

    //jeszcze raz sprawdzamy czy si� zmie�cimy, �eby unikn�� g�upich b��d�w
    string notif = sprawdz_czy_zmieszcze();
    if(strlen(notif))
    {
        write(notif);
        return;
    }

    tell_room(ENV(kto),QCIMIE(kto,PL_MIA)+" wspina si� "+
            "na "+short(PL_BIE)+".\n",({kto}));        
    kto->move(wnetrze,1);
    kto->catch_msg("Wchodzisz na "+short(PL_BIE)+".\n");
    kto->do_glance(1);
    tell_room(ENV(kto),QCIMIE(kto,PL_MIA)+" wchodzi na pok�ad.\n",({kto}));
}


/*
 * wchodzenie na ��dk�, jak jeste�my w wodzie i ��dka p�ywa ko�o nas.
 */
int
wespnij(string str)
{
    if(query_verb() ~= "wespnij")
        notify_fail(capitalize(query_verb())+" si� gdzie?\n");
    else
        notify_fail("Wejd� gdzie?\n");
    
    //wchodzimy tylko z wody... wchodzenie na pok�ad z pomostu jest w innej funkcji
    if(ENV(TP)->query_prop(ROOM_I_TYPE) != ROOM_IN_WATER)
        return 0;
    
    object *lodki;

    if(query_verb() ~= "wespnij" &&
        !parse_command(lower_case(str), AI(ENV(TP)), " 'si�' 'na' %i:"+PL_BIE, lodki))
        return 0;
    
    if(query_verb() ~= "wejd�" &&
        !parse_command(lower_case(str), AI(ENV(TP)), " 'na' %i:"+PL_BIE, lodki))
        return 0;
    
    lodki = NORMAL_ACCESS(lodki,0,0);
    
    if(!sizeof(lodki))
        return 0;
    if(lodki[0] != TO)
        return 0;
    
    //taka wspinaczka kosztuje sporo wysi�ku
    if(TP->query_fatigue() < TP->query_max_fatigue() / 4)
    {
        write("Nie masz ju� na to si�y.\n");
        return 1;
    }
    
    //sprawdzamy czy si� zmie�cimy
    string notif = sprawdz_czy_zmieszcze();
    if(strlen(notif))
    {
        write(notif);
        return 1;
    }
    
    //OK --- dajemy parali�.
    
    object p=clone_object("/std/paralyze.c");
    p->set_finish_object(TO);
    p->set_finish_fun("koniec_paralizu_na_wspinanie_sie",TP);
    p->set_remove_time(3+random(3));
    p->set_stop_verb("przesta�");
    p->set_stop_message("Zaprzestajesz pr�by wspi�ci� si� na "+
                    lodki[0]->short(PL_BIE)+".\n");
    p->set_fail_message("Chwil�! W�a�nie pr�bujesz wspi�� si� na "+
                    lodki[0]->short(PL_BIE)+"!\n");
    p->move(TP,1);
    
    TP->add_prop(WSPINAM_SIE_NA_LODZ,1);
    
    write("Pr�bujesz wspi�� si� na "+lodki[0]->short(PL_BIE)+".\n");
    say(QCIMIE(TP,PL_MIA)+" pr�buje wspi�� si� na "+lodki[0]->short(PL_BIE)+".\n");
    //poinformujmy o tym fakcie tak�e graczy w ��dce:
    tell_room(wnetrze,QCIMIE(TP,PL_MIA)+" pr�buje wspi�� si� na pok�ad.\n");
    
    return 1;
}

int
rozkolysz(string str)
{
    notify_fail("Co chcesz rozko�ysa�?\n");
    object *lodki;
        
    if(!parse_command(lower_case(str), AI(ENV(TP)), "%i:"+PL_BIE, lodki))
        return 0;
    
    lodki = NORMAL_ACCESS(lodki,0,0);
    
    if(!sizeof(lodki))
        return 0;
    if(lodki[0] != TO)
        return 0;
    
    if(TP->query_fatigue() <= TP->query_max_fatigue() / 5)
    {
        write("Nie masz na to si�.\n");
        return 1;
    }
    
    if(!objectp(wnetrze))
    {
        notify_fail("Wyst�pi� b��d w ��dce nr 217cex. Prosz� zg�o� "+
        "go, podaj�c ten numer wraz z okoliczno�ciami w jakich to wystapi�o.\n");
        return 0;
    }
    
    //na siedz�co/le��co nie wsi�dziemy
    if(TP->query_prop(SIT_SIEDZACY) || TP->query_prop(SIT_LEZACY))
    {
        write("A mo�e najpierw wstaniesz?\n");
        return 1;
    }
    
    if(ENV(TP)->query_prop(ROOM_I_TYPE) == ROOM_IN_WATER ||
        ENV(TP)->query_prop(ROOM_I_TYPE) == ROOM_UNDER_WATER)
    {
        //ci co sami ledwo p�ywaj� nie maj� tyle pa�era
        if(TP->query_skill(SS_SWIM) <= 15)
        {
            write("To ci si� nie uda, zbyt niepewnie czujesz si� w wodzie.\n");
            return 1;
        }
        write("Pr�bujesz rozko�ysa� "+short(PL_BIE)+ ".\n");
        saybb(QCIMIE(TP,PL_MIA)+" pr�buje rozko�ysa� "+short(PL_BIE)+ ".\n");
        
        if(random(2))
            wnetrze->rozkolysz();
    }
    else
    {
        wnetrze->rozkolysz(2);
        write("Zapierasz si� nogami o grunt i z ca�ych si� ko�yszesz "+short(PL_NAR)+".\n");
        saybb(QCIMIE(TP,PL_MIA)+" zapiera si� nogami o grunt i z ca�ych si� ko�ysze "+short(PL_NAR)+".\n");
    }
    
    TP->add_fatigue(-855);
    
    return 1;
    
}

nomask void
init_cmds()
{
    int cmdarr_size = sizeof(komendy);

    for(int i =0;i < cmdarr_size; i++)
    {
        mixed cmd = komendy[i][0];
        add_action("wsiadz", cmd);
    }

    add_action(wespnij,"wejd�");
    add_action(wespnij,"wespnij");
    
    //szata�ska komenda do wyrzucania ludzi z ��dek. Musi jednak by�, poniewa�
    //nie chcemy z niezacumowanych (ale jeszcze w porcie) oraz przepe�nionych ��dek robi� safeloki, prawda? :)
    add_action(rozkolysz,"rozkolysz");
}

init()
{
    ::init();
    init_cmds();
}

string
query_pomoc()
{
    string str;
    if((ENV(TP)->query_prop(ROOM_I_TYPE) == ROOM_IN_WATER ||
        ENV(TP)->query_prop(ROOM_I_TYPE) == ROOM_UNDER_WATER))
        str = "Na t� ��dk� mo�esz spr�bowa� si� wspi��.\n";
    else
        str = "Na t� ��dk� mo�esz spr�bowa� "+komendy[0][2]+".\n";
    
    
    //help o ko�ysaniu pokazujemy wtedy, jak kto� jest na ��dce.
    if(objectp(wnetrze) && sizeof(FILTER_LIVE(AI(wnetrze))))
        str += "(mo�esz tak�e spr�bowa� j� rozko�ysa�)\n";
    
    return str;
}

int
query_lodka()
{
    return 1;
}

void
enter_env(object dest, object old)
{
    ::enter_env(dest,old);
    
    if(!objectp(wnetrze))
    {
        write("Ups. Wyst�pi� b��d nr ddx5213dk! Prosz� zg�o� to w ��dce "+
        "podaj�c ten numer jak i okoliczno�ci powstania b��du.\n");
        return;
    }
    
    if(interactive(ENV(TO))) //potrzebne mi do testowania lodek...
        return;
    
    //wysy�amy ��dce aktualne kierunki i lokacje
    wnetrze->ustaw_lokacje_i_kierunki(ENV(TO)->query_exit_rooms(), ENV(TO)->query_exit_cmds());
    
    //ustawiamy �owiska dla lokacji wn�trza ��dki
    string *lowiska = ENV(TO)->query_lowiska();
    if(pointerp(lowiska) &&  sizeof(lowiska))
    {
        foreach(string lowisko : lowiska) //zwykle jest jedno lowisko, ale mo�e....
        {
            mapping rybky = ([ ]);
            foreach(string ryba : ENV(TO)->query_nazwy_ryb(lowisko))
                rybky[ryba] = ENV(TO)->query_procent_ryby(lowisko,ryba);
            
            wnetrze->dodaj_lowisko(lowisko,rybky);
        }
    }
    
    //aktualizujemy w ��dce opisy short i long, zgodnie z otoczeniem ��dki
    wnetrze->set_opisy_zew(ENV(TO)->short(), ENV(TO)->long());
    
    //aktualizujemy w ��dce itemy - maj� by� takie jak na zewn�trz
    foreach(mixed x : query_item())
        wnetrze->add_item(x[0],x[1],x[2]);
    
    //obiekt�w p�ywaj�cych na zewn�trz mo�na zas obejrze� tylko short (w longu jest)
    //oraz je ew. 'wy�owi�'
    //wyj�tkiem s� 
    
}

//��dka si� przemieszcza
varargs void
leave_env(object from, object to, string fromSubloc)
{
    ::leave_env(from, to, fromSubloc);
    
    if(!objectp(wnetrze))
    {
        write("Ups. Wyst�pi� b��d nr 415dfg53! Prosz� zg�o� to w ��dce "+
        "podaj�c ten numer jak i okoliczno�ci powstania b��du.\n");
        return;
    }
    
    wnetrze->wyczysc_lowiska();
}
