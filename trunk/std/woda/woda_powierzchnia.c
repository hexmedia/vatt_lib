/*
 * Lokacja wody, na powierzchni. Czyli tam gdzie np. p�ywaj� ��dki :P
 *
 *
 * TODO:
 * -zamarzanie wody. Najlepiej zrobi� to tak:
 *              nowy std lokacji - woda_zamarznieta i nanie�� drobne zmiany w std/woda/kapielisko, by
 *              zale�nie od temperatury i pory roku klonowa�o si� odpowiednie std - to, lub zamarzniete.
 *              -nowy typ lokacji dodac - LOD 
 * Vera.
 */

#include <macros.h>
#include <composite.h>
#include <stdproperties.h>
#include <ss_types.h>
#include <woda.h>
#include <exp.h>
#include <cmdparse.h>

inherit "/std/room";
inherit "/std/woda/lok_wody";

string query_long_pod_woda();
string query_short_pod_woda();
string query_short_dna();
string query_long_dna();
void set_long_pod_woda(string str);
void set_short_pod_woda(string str);
void set_short_dna(string str);
void set_long_dna(string str);

string long_pod_woda,
       short_pod_woda,
       short_dna,
       long_dna;

object pod_woda; //klon lokacji pod wod�

void
create_woda_powierzchnia()
{
}


//�eby nie by�o tak monotonnie..
string
random_short_pod_woda()
{
    string str = "";
    switch(random(6))
    {
        case 0..2: str = "Pod wod�"; break;
        case 3: str = "Wodne g��biny"; break;
        case 4: str = "Pod powierzchni� wody"; break;
        case 5: str = "W wodnej toni"; break;
    }
    return str;
}

//ta funkcja jest wywo�ywana w podawaniu longa lokacji podwodnej i wy�wietla czy widzimy na g�rze ��dki i ludzi
string
czy_cos_na_gorze()
{/*
    if(!objectp(powierzchnia))
        return "";
    else*/
    {
        object *lil = ({ });
        //foreach(object l : AI(powierzchnia))
        foreach(object l : AI(TO))
            if(l->query_lodka() || l->is_living())
                lil += ({l});
            
        if(sizeof(lil))
            return " Tu� nad sob� dostrzegasz "+
            /*(sizeof(lil)>1?"j� ":" ") +*/ COMPOSITE_DEAD(lil,PL_MIA) +".";
    }
    
    return "";
}

void
create_room()
{
    
    add_prop(ROOM_I_TYPE, ROOM_IN_WATER);
    add_prop(ROOM_I_HIDE,-1);
    add_prop(ROOM_M_NO_STEAL,1);
    add_prop(ROOM_CANT_THROW,1);
    //kto� mo�e pomy�le�, �e wtedy woda b�dzie azylem, ale to nieprawda, bo w wodzie
    //b�dziemy si� szybko m�czy�....
    add_prop(ROOM_M_NO_ATTACK, 1);

    
    //USTAWIAMY DOMY�LNE OPISY LOKACJI PODWODNYCH:
    set_short_pod_woda(random_short_pod_woda());
    set_long_pod_woda("Zewsz�d otacza ci� przejrzysta, lecz o nieco zielonkawym zabarwieniu "+
    "woda.@@czy_cos_na_gorze@@ Bogactwo podwodnego �wiata rozpo�ciera si� niemal�e we wszystkie strony.\n");
    
    set_short_dna("Na dnie");
    //normalny opis, jak widzimy dno, czyli jest do�� p�ytko, bo �wiat�o dzienne z zew.
    //dobiega tu:
    set_long_dna("Ledwie roz�wietlone wodne g��biny ukazuj� przed tob� piaszczysto muliste dno.\n");
    
    
    create_woda_powierzchnia();
}

string query_long_pod_woda()
{
    return long_pod_woda;
}
void set_long_pod_woda(string str)
{
    long_pod_woda = str;
}
string query_short_pod_woda()
{
    return short_pod_woda;
}
void set_short_pod_woda(string str)
{
    short_pod_woda = str;
}
string query_short_dna()
{
    return short_dna;
}
void set_short_dna(string str)
{
    short_dna = str;
}
string query_long_dna()
{
    return long_dna;
}
void set_long_dna(string str)
{
    long_dna = str;
}

//zacznijmy od tajnej funkcji. t������� :>
int
jestem_lokacja_powierzchni_wody()
{
    return 1;
}

void
set_dol(object x)
{
    pod_woda = x;
}
object
query_dol()
{
    return pod_woda;
}

void
enter_inv(object ob, object from)
{
    ::enter_inv(ob,from);
    
    wspolne_enter_inv(ob, from);
}

void
leave_inv(object ob, object to)
{
    ::leave_inv(ob,to);
    
    //gagatek wychodzi z wody?
    if(is_lok_wody(to))
        return;
    
    if(!ob->is_living())
        return; 
    
    
    ob->remove_prop(TONIE);
    ob->remove_prop(IL_POWIETRZA);
    ob->remove_prop(IL_POWIETRZA_MAX);
    
    //usuwamy alarm i prop na p�ywanie:
    (ob->query_prop(PIERWSZA_ALARM_LOK))->usun_alarm(ob->query_prop(PLYWAM_ALARM));
    ob->remove_prop(PLYWAM_ALARM);
    
    //i na redukcj� powietrza:
    if((ob->query_prop(PIERWSZA_ALARM_LOK))->get_alarm(ob->query_prop(POWIETRZE_ALARM)))
    {
        (ob->query_prop(PIERWSZA_ALARM_LOK))->usun_alarm(ob->query_prop(POWIETRZE_ALARM));
        ob->remove_prop(POWIETRZE_ALARM);
    }   
    
    ob->remove_prop(LIVE_S_EXTRA_SHORT);
    ob->remove_prop(SUMA_GESTOSCI_EKW);
    
    ob->remove_prop(PIERWSZA_ALARM_LOK);
}

/* funkcja ta przechwytuje wszystko...
 * troch� niefortunna nazwa, ale ju� si� przyzwyczai�em :P
 */
public int
kierunki(string str)
{
    string cmd = query_verb();
    int i = -1,
        ob_flag = 0;
        
    //je�li komenda to kt�ra� z tych zakazanych, to nie da rady...
    if(member_array(cmd,ZAKAZANE_NA_POWIERZCHNI) != -1)
    {
        write("Jeste� teraz w wodzie, jak�e zatem m"+TP->koncowka("�g�by�","og�aby�")+
        " to uczyni�?\n");
        return 1;
    }   

    if(cmd ~= "p�y�" && strlen(str) > 3 && str[0..1] ~= "na")
        cmd = str[3..];
    //trzeba zmieni� te� 'sp na kierunek':
    if(cmd ~= "sp" || cmd ~= "sp�jrz" || cmd ~="ob" || cmd~="obejrzyj")
    {
        if((cmd != "ob" && cmd !="obejrzyj") && strlen(str) > 3)
            cmd = str[3..];
    }

    
    //trzeba zmienic skr�ty na pe�ne nazwy:
    switch(cmd)
    {
        case "n": case "polnoc": cmd = "p�noc"; break;
        case "ne": case "polnocny-wschod": cmd = "p�nocny-wsch�d"; break;
        case "nw": case "polnocny-zachod": cmd = "p�nocny-zach�d"; break;
        case "e": case "wschod": cmd = "wsch�d"; break;
        case "se": case "poludniowy-wschod": cmd = "po�udniowy-wsch�d"; break;
        case "s": case "poludnie": cmd = "po�udnie"; break;
        case "sw": case "poludniowy-zachod": cmd = "po�udniowy-zach�d"; break;
        case "w": case "zachod": cmd = "zach�d"; break;
        case "p�noc":
        case "p�nocny-wsch�d":
        case "wsch�d":
        case "po�udniowy-wsch�d":
        case "po�udnie":
        case "po�udniowy-zach�d":
        case "zach�d":
        case "p�nocny-zach�d":
            break;
    }

    i = member_array(cmd,query_exit_cmds());
    
    if(i == -1)
        cmd = "dupa";
    
    if(query_verb() ~= "sp" || query_verb() ~="sp�jrz" || query_verb() ~= "ob" || query_verb() ~= "obejrzyj")
        ob_flag = 1;
    
    
     //sp na kierunek 
     if(ob_flag && cmd != "dupa")
     {   
        write("Spogl�dasz na "+cmd+":\n");
        saybb(QCIMIE(TP,PL_MIA)+" spogl�da na "+cmd+".\n");
        TP->daj_paraliz_na_sp((i==-1 ? "NONE" : cmd),query_exit_rooms()[i]);
        return 1;
     }
     
     //---w lodka_in tu jest ob cos za burta
     
     //event dla tych pod wod�:
     if(query_verb() ~= "powiedz" && strlen(str) ||
         query_verb() ~= "zasmiej" && strlen(str) ||
         query_verb() ~= "krzyknij")
     {
         if(objectp(TO->query_dol()) && !random(3))
             tell_room(TO->query_dol(), "Z g�ry dobiegaj� ci� jakie� ha�asy.\n");
     }
     
     
     //if wpisywana komenda to �adne z wyj�� dost�pnych tutaj :
    if(cmd == "dupa")
        return 0;
    
    if(TP->query_fatigue() <= LIMIT_ZMECZENIA_NA_PRZEPLYWANIE)
    {
        write("Nie masz ju� si�.\n");
        return 1;
    }
    
    LOAD_ERR(query_exit_rooms()[i]);
    object lok = find_object(query_exit_rooms()[i]);
    if(lok->query_prop(ROOM_I_TYPE) != ROOM_IN_WATER &&
        lok->query_prop(ROOM_I_TYPE) != ROOM_BEACH)
    {
        write("Nie mo�esz tam pop�yn��.\n");
        return 1;
    }
    
    int czas = query_czas();
    object p=clone_object("/std/paralyze.c");
    p->set_finish_object(TO);
    p->set_finish_fun("koniec_paralizu", TP, lok, query_exit_cmds()[i], czas);
    p->set_remove_time(czas);
    p->set_stop_verb("przesta�");
    p->set_stop_message("Przestajesz p�yn��.\n");
    p->set_fail_message("Chwil�! W�a�nie dok�d� p�yniesz!\n");
    p->move(TP,1);
    
    cmd = "na " + cmd;
    
    //jeszcze sprawd�my, czy jest specjalny opis wyj�cia lokacji
    cmd = sprawdz_specjalny_opis_wyjscia(cmd, query_exit_rooms()[i]);
        
    write("Zaczynasz p�yn�� "+cmd+".\n");
    saybb(QCIMIE(TP,PL_MIA)+" zaczyna p�yn�� "+cmd+".\n");
    
    return 1;
}


void
koniec_paralizu_na_nurkowanie(object *x, object kto, int czas)
{
    int ilosc_powietrza = ustal_ilosc_powietrza(kto);
        
    kto->add_prop(IL_POWIETRZA, ilosc_powietrza);
    kto->add_prop(IL_POWIETRZA_MAX, ilosc_powietrza);
    
    
    //i zm�cz:
    zmecz(kto,1);
    
    tell_room(ENV(kto), QCIMIE(kto,PL_MIA)+" nurkuje.\n",({kto}));
    
    kto->move(query_dol(),1);
    
    kto->catch_msg("Nurkujesz pod powierzchni� wody.\n");
    tell_room(ENV(kto), QCIMIE(kto,PL_MIA)+" przyp�ywa z g�ry.\n",({kto}));
    kto->do_glance(1);
    
    if(!kto->query_prop(PIERWSZA_ALARM_LOK))
        kto->add_prop(PIERWSZA_ALARM_LOK, TO);
    (kto->query_prop(PIERWSZA_ALARM_LOK))->set_alarm(2.0,0.0,"redukuj_powietrze",kto);
    
    //expik? :)
    kto->increase_ss(SS_SWIM, EXP_NURKUJE_SWIM);
    kto->increase_ss(SS_CON, EXP_NURKUJE_CON);
    kto->increase_ss(SS_DEX, EXP_NURKUJE_DEX);
}

int
zanurkuj(string str)
{
    if(strlen(str))
        return 0;
    
    //czy mamy jeszcze na to si��?
    if(TP->query_fatigue() <= LIMIT_ZMECZENIA_NA_NURKOWANIE)
    {
        write("Pr�bujesz zanurkowa�, lecz po chwili bezsiln"+TP->koncowka("y","a","e")+
        " zn�w wyp�ywasz na powierzchni�.\n");
        return 1;
    }    
    
    //ustawiamy d� dla obiektu powierzchni, czyli dla TO, a d� od powierzchni to
    //oczywi�cie dopiero pierwszy poziom podwodnego �wiatka :)
    ustaw_dol_jesli_nie_ma(TO, 1);
    
    
    int czas = query_czas();
    object p=clone_object("/std/paralyze.c");
    p->set_finish_object(TO);
    p->set_finish_fun("koniec_paralizu_na_nurkowanie", TP, czas);
    p->set_remove_time(czas);
    p->set_stop_verb("przesta�");
    p->set_stop_message("Przestajesz nurkowa�.\n");
    p->set_fail_message("Chwil�! W�a�nie pr�bujesz da� nura!\n");
    p->move(TP,1);
    
        
    write("Nabierasz powietrza w p�uca i zaczynasz nurkowa�.\n");
    saybb(QCIMIE(TP,PL_MIA)+" zaczyna nurkowa�.\n");
    
    return 1;
}

//zwyk�y emot: (zer�ni�ty z balii z Bia�ego Mostu) ;]
int
ochlap(string str)
{
    object *cele;

    notify_fail(capitalize(query_verb())+" kogo?\n");

    if (!str) return 0;

    if (!parse_command(str, all_inventory(ENV(TP)) +
        all_inventory(TP), "%l:"+PL_BIE, cele))
        return 0;

    cele = NORMAL_ACCESS(cele, 0, 0);

    if (sizeof(cele) >= 2)
    {
        notify_fail("Nie mo�esz chlapa� wszystkich naraz!\n");
        return 0;
    }

    if(sizeof(cele)==0)
    {
        notify_fail("Kogo chcesz ochlapa�?\n");
        return 0;
    }

    if(!sizeof(cele))
        return 0;

    TP->catch_msg("Z psotliwym u^smieszkiem na twarzy chlapiesz wod^a na "+
        QIMIE(cele[0],PL_DOP)+".\n");
    saybb(QCIMIE(TP,PL_MIA)+" z psotliwym u^smieszkiem na twarzy chlapie wod^a "+
        "na "+  QIMIE(cele[0],PL_DOP)+".\n", ({ TP, cele[0] }));
    cele[0]->catch_msg(QCIMIE(TP,PL_DOP)+" z psotliwym u^smieszkiem na twarzy "+
        "chlapie ci^e wod^a.\n");
    return 1;
}

string
query_pomoc()
{
    return "Jeste� obecnie w wodzie, jednak mo�esz porusza� si� "+
    "(p�ywa�) na strony niczym na l�dzie. Mo�esz tu tak�e spr�bowa� zanurkowa�.\n";
}

init()
{
    ::init();
    add_action(kierunki, "", 1);
    add_action(zanurkuj,"zanurkuj");
    add_action(zanurkuj,"nurkuj");
    add_action(zanurkuj,"d�");
    
    //emot:
    add_action(ochlap, "ochlap");
}