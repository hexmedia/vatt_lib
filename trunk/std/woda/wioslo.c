/* Wios�o! 
 * Dla ciebie, dla rodziny. :)
 * Vera. 9.07.2009
 */

inherit "/std/holdable_object.c";

#include <macros.h>
#include <pl.h>
#include <stdproperties.h>
#include <object_types.h>
#include <materialy.h>

void 
create_wioslo()
{
}

void
create_holdable_object()
{
    set_hands(W_BOTH);
    setuid();
    seteuid(getuid());
 
    ustaw_nazwe("wioslo");
    dodaj_nazwy("pagaj");
    
    dodaj_przym("kr�tki", "kr�tcy");
    dodaj_przym("drewniany", "drewniani");
    set_long("To zwyk�y drewniany pagaj. D�ugi na oko�o cztery stopy g�adki kij "+
    "wyposa�ony na jednym ko�cu w niedu�e zaokr�glone pi�ro i nic poza tym.\n");
    
    add_prop(OBJ_I_VOLUME, 7020);
    add_prop(OBJ_I_WEIGHT, 4040);
    add_prop(OBJ_I_VALUE, 5);
    set_type(O_WEDKARSKIE);
    ustaw_material(MATERIALY_DR_DAB,100);
    
    create_wioslo();
}

string
query_pomoc()
{
    return "Wios�o, jak sama nazwa wskazuje - s�u�y do wios�owania.\n";
}

int
query_wioslo()
{
    return 1;
}
