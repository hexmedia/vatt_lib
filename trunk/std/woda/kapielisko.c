/* *99***1#
 * K�pielisko! :) 
 * W�a�ciwie jest to lib, ale porz�dek w plikach wzorowa�em na rybach - wszystko co do ryb jest
 * w jednym katalogu, /std/ryby, wi�c i tak samo zrobi�em z p�ywaniem - /std/woda
 *
 * �eby mo�na by�o si� gdzie� k�pa�, to trzeba w tej lokacji zrobi� tak:
 * inherit kapielisko of kors.
 * p�niej
 *      set_exits_woda_description("Jezioro rozci�ga si� na p�nocy.\n"); 
 * oraz
    init()
    {
        ::init();
        init_kapielisko();
    }
 * Mozna te� doda� inn� komend� ni� 'wskocz do wody'. Je�li chcesz, to zapoznaj si� z funkcj� ustaw_kapielisko
 *
 *
 * autor: Vera.
 * data: kaniku�y 2009
 */

#include <macros.h>
#include <composite.h>
#include <stdproperties.h>
#include <ss_types.h>
#include <object_types.h>
#include <cmdparse.h>

//taki tam niewinny propik ;)
#define DEBIL_PROBUJE_WSKOCZYC  "_debil_probuje_wskoczyc"

string  gdzie = "do wody",
        *jak = ({"wskocz","wskakujesz","wskakuje","wskoczy�"}),
        skad = "z pomostu",
        cumowanie = "",
        exits_woda = ""; //opisy wyj�� jak wchodzimy na ��dk� np. 'jezioro jest na p�nocy'
object kapielisko;

/* prosta komenda, co tu t�umaczyc.
 * jako argument podajemy string lokacji wody do ktorej wskakujemy.
 * opcjonalnie jako drugi arg dajemy to co ma by� do 'wskocz' lub 'wejdz'
 * jesli nic nie podamy, bedzie domyslnie: 'do wody'
 * trzeci arg ma mie� form�:
 * ({"wskocz","wskakujesz","wskakuje","wskoczy�"}) je�li chcemy inn� komend� da�.
 * czwarty arg pokazuje sie tym na lokacji wody skad ktos wskakuje do nas :)
 */
int ustaw_kapielisko(string lok, string str = "do wody", string *arr = ({ }), string sskad = "")
{
    if(!stringp(lok))
	    return 0;

    LOAD_ERR(lok);
    kapielisko = find_object(lok);
    
    gdzie = str;
    
    if(strlen(sskad))
        skad = sskad;
    
    if(sizeof(arr))
        jak = arr;
    
    return 1;
}


void
plusk()
{
    tell_room(TO,"Woda rozbryzguje si� na wszystkie strony, chlapi�c ci� nieznacznie.\n");
}

/* mo�na t� funkcj� nadpisa� w lok z k�pieliskiem. :)
 */
void
hop_effect()
{
    string str = "";
    //na bomb� ;), plusk!
    if(TP->query_skill(SS_SWIM) < 20)
    {
        set_alarm(1.0,0.0,"plusk");
        str = "niezdarnie ";
    }
            
        
        
    write(capitalize(jak[1])+" "+str+gdzie+".\n");
    saybb(QCIMIE(TP,PL_MIA)+" "+jak[2]+" "+str+gdzie+".\n");
}


int
filtruj_niewidoczne(object ob)
{
    return !(ob->query_no_show() || ob->query_no_show_composite() ||
        ob->query_prop(OBJ_I_DONT_INV) || ob->query_prop(OBJ_M_NO_DROP) ||
        ob->query_prop(WIZARD_ONLY) );
}

int
filtruj_ryby(object ob)
{
    if(ob->query_type() == O_RYBY)
    {
        ob->move(kapielisko, 1);
        return 0;
    }
    else
        return 1;
}


int
hop(string str)
{
    object kap, *inv;
    
    if(str != gdzie)
    {
        notify_fail("Gdzie chcesz "+jak[3]+"?\n");
        return 0;
    }
    
    if(TO->pora_roku() == MT_ZIMA)
    {
        notify_fail("Pokrywa wody skuta jest grubym lodem, jak wi�c chcia�"+TP->koncowka("by�","aby�")+
        " tam wskoczy�?\n");
        return 0;
    }
    
    
    //?!?!? lokacja kapieliska si� nie �aduje! :O
    if(!objectp(kapielisko))
    {
        notify_fail("Wyst�pi� powa�ny b��d w k�pielisku, nr 390-xd. Prosz� zg�o� "+
        "go, podaj�c ten numer wraz z okoliczno�ciami w jakich to wystapi�o.\n");
        return 0;
    }
    
    //z inventory nie wejdziemy.
    inv = TP->subinventory(0);
    inv = filter(inv, &filtruj_niewidoczne());
    
    //z rybami chyba mo�na wskoczy�? ;) a dok�adniej to ryby same wskakuj� ;p
    inv = filter(inv, &filtruj_ryby());
    
    if(sizeof(inv))
    {
        write("Musisz od�o�y� "+ COMPOSITE_DEAD(inv, PL_BIE) +", by m�c "+jak[3] +" "+
        gdzie+".\n");
        return 1;
    }
    
    if(!HAS_FREE_HANDS(TP))
    {
        write("Musisz mie� obie r�ce wolne, by si� wyk�pa�.\n");
        return 1;
    }
    
    //a oto test czy gracz jest debilem:
    if(TP->query_skill(SS_SWIM)+TP->query_tskill(SS_SWIM) < 5 && !TP->query_prop(DEBIL_PROBUJE_WSKOCZYC))
    {
        write("Przecie� ty nie potrafisz p�ywa�! Powt�rz komend�, je�li este� pew"+TP->koncowka("ien","na")+
        ", �e chcesz tam "+jak[3]+".\n");
        TP->add_prop(DEBIL_PROBUJE_WSKOCZYC);
        return 1;
    }
    
    
    hop_effect();
    TP->move(kapielisko, 1);
    TP->do_glance();
    saybb(QCIMIE(TP,PL_MIA)+" "+jak[2]+" "+gdzie+" "+(strlen(skad) ? skad : "")+".\n");
    
    //event dla tych pod wod�:
    if(objectp(kapielisko->query_dol()) && !random(2))
        tell_room(kapielisko->query_dol(), "Z g�ry dobiegaj� ci� jakie� odg�osy - "+
         "chyba w�a�nie kto� wskoczy� do wody.\n");
    
    return 1;
}

int
wyrzuc(string str)
{
    notify_fail("Wyrzuc co do wody ?\n");
        
    if(!str) return 0;
    
    if(!objectp(kapielisko))
    {
       notify_fail("Wyst�pi� b��d w k�pielisku nr 53cd3c. Prosz� zg�o� "+
        "go, podaj�c ten numer wraz z okoliczno�ciami w jakich to wystapi�o.\n");
        return 0;
    }
    
    object *obj;
    if(!parse_command(lower_case(str), AI(TP), "%i:"+PL_BIE+ " 'do' 'wody'", obj))
        return 0;
    
    obj = NORMAL_ACCESS(obj,0,0);
    
    if(!sizeof(obj))
        return 0;
    
    
    write("Wyrzucasz "+COMPOSITE_DEAD(obj,PL_BIE)+" do wody.\n");
    saybb(QCIMIE(TP,PL_MIA)+" wyrzuca "+COMPOSITE_DEAD(obj,PL_BIE)+" do wody.\n");
    
    obj->move(kapielisko,1);
    return 1;
}


/* to trzeba wywo�ywa� w init() na lokacji
 */
public void
init_kapielisko()
{
    add_action(hop, jak[0]);
    
    add_action(wyrzuc,"wyrzu�");
}

void
set_cumowanie(string str)
{
    cumowanie = str;
}

string
query_cumowanie()
{
    return cumowanie;
}

void
set_exits_woda_description(string str)
{
    exits_woda = str;
}
string
exits_woda_description()
{
    return exits_woda;
}


object
query_kapielisko()
{
    return kapielisko;
}

int
is_kapielisko()
{
    return 1;
}

string
query_pomoc()
{
    if(TO->pora_roku() != MT_ZIMA)
        return "Z tego miejsca mo�esz "+jak[3]+" "+gdzie+".\n Mo�esz tak�e wyrzuci� co� do wody.\n";
    else
        return "Pokrywa lodu uniemo�liwia p�ywanie.\n";
}
