/**
 * Standard promow - obiekt promu
 *
 * @author Avard
 * @date 22.12.09
 *
 */
inherit "/std/object";
inherit "/lib/trade";

#include <macros.h>
#include <mudtime.h>
#include <cmdparse.h>
#include <pl.h>
#include <stdproperties.h>
#include <filter_funs.h>
#include <mudtime.h>
#include <sit.h>

#define PUSTKA "/std/prom/prom_pusta.c"

//#define DBG(x) find_player("avard")->catch_msg(x+"\n");
//#define PUSTKA "/d/Aretuza/avard/prom_lokacja.c"

/* Funkcje */
void ustaw_czas_postoju(int i);
int query_czas_postoju();

void ustaw_czas_podrozy(int i);
int query_czas_podrozy();

void ustaw_czas_podrozy_random(int i);
int query_czas_podrozy_random();

void ustaw_cene(int i);
int query_cena();

void ustaw_wnetrze(string str);
object query_wnetrze();

void ustaw_przewoznika(string str);
object query_przewoznik();

void ustaw_przystanie(string *str);
void ustaw_przystanie_sciezki(string *str);

void set_enter_msg(string str);
void set_leave_msg(string str);

int wsiadz(string str);
void odplywamy();
void doplywamy();

void odwracamy_przystanie();
void start();

/* Zmienne */
int czas_postoju = 15;
int czas_podrozy = 20;
int czas_podrozy_random = 10;
int ktora_przystan = 1;
int cena = 10;
int alarm_id = 0;

object wnetrze;
string wnetrze_sciezka;
object przewoznik;
string przewoznik_sciezka;

string enter_msg;
string leave_msg;

string *przystanie = ({ });
string *przystanie_sciezki = ({ });

void create_prom()
{
}

nomask void create_object()
{
    ustaw_nazwe("lodka");
    dodaj_nazwy("statek");
    set_long("Jest to standardowy prom.\n");
    add_prop(OBJ_I_WEIGHT, 500000);
    add_prop(OBJ_I_VOLUME, 500000);
    add_prop(OBJ_I_NO_GET, "Nie mo^zesz tego wzi^a^c.\n");
    ustaw_przystanie(({"Workroom Avarda", "Workroom Veli", "Workroom Cairella"}));
    ustaw_przystanie_sciezki(({"/d/Aretuza/avard/workroom", 
        "/d/Aretuza/veli/workroom", "/d/Aretuza/cairell/workroom"}));
    ustaw_wnetrze("/std/prom/prom_in");
    ustaw_przewoznika("/std/prom/prom_przewoznik");
    set_enter_msg("dop^lywa do przystani");
    set_leave_msg("wyp^lywa z przystani");
    create_prom();
}

/**** FUNKCJE SET I QUERY ****/

/**
 * Nazwa funkcji: ustaw_czas_postoju
 * Opis: Ustawia czas postoju promu na przystani, w sekundach.
 *       Dla obu przystani ustawiany jest jednakowy czas.
 */
void ustaw_czas_postoju(int i)
{
    czas_postoju = i;
}

int query_czas_postoju()
{
    return czas_postoju;
}

/**
 * Nazwa funkcji: ustaw_czas_podrozy
 * Opis: Ustawia czas w sekundach potrzebny do doplyniecia do celu.
 *       Czas jest jednakowy niezaleznie od kierunku podrozy.
 */
void ustaw_czas_podrozy(int i)
{
    czas_podrozy = i;
}

int query_czas_podrozy()
{
    return czas_podrozy;
}

/**
 * Nazwa funkcji: ustaw_czas_podrozy_random
 * Opis: Ustawia zakres randomowanego czasu, ktory zostaje dodany do
 *       wlasciwego czasu podrozy.
 */
void ustaw_czas_podrozy_random(int i)
{
    czas_podrozy_random = random(i);
}

int query_czas_podrozy_random()
{
    return czas_podrozy_random;
}

void ustaw_cene(int i)
{
    cena = i;
}
int query_cena()
{
    return cena;
}

/**
 * Nazwa funkcji: ustaw_wnetrze
 * Opis: Sciezka do lokacji, ktora ma byc pokladem promu. 
 */
void ustaw_wnetrze(string str)
{
    LOAD_ERR(str);
    wnetrze = find_object(str);
    wnetrze->ustaw_prom(TO);
}

object query_wnetrze()
{
    return wnetrze;
}

/**
 * Nazwa funkcji: ustaw_przewoznika
 * Opis: Sciezka do przewoznika
 */
void ustaw_przewoznika(string str)
{
    przewoznik_sciezka = str;
    przewoznik = find_object(str);
    wnetrze->ustaw_przewoznika(przewoznik);
}

object query_przewoznik()
{
    return przewoznik;
}

/**
 * Nazwa funkcji: ustaw_przystanie
 * Opis: Ustawia nazwy przystani zgodnie z kolejnoscia odwiedzania.
 *       Nalezy podac je tylko w jedna strone, funkcja odwroc_przystanie
 *       ustawi je poprawnie na droge powrotna.
 *       Przystanie nalezy podawac w mianowniku
 */
void ustaw_przystanie(string *str)
{
    przystanie = str;
}
/**
 * Nazwa funkcji: ustaw_przystanie_sciezki
 * Opis: Ustawia sciezki do lokacji z przystaniami zgodnie z kolejnoscia 
 *       odwiedzania. Nalezy podac je tylko w jedna strone, funkcja 
 *       odwroc_przystanie_sciezki ustawi je poprawnie na droge powrotna.
 */
void ustaw_przystanie_sciezki(string *str)
{
    przystanie_sciezki = str;
}

string set_enter_msg(string str)
{
    enter_msg = str;
}

string set_leave_msg(string str)
{
    leave_msg = str;
}

/**** FUNKCJE Z AKCJAMI ****/ 

init()
{
    ::init();
    add_action("wsiadz", "wsi^ad^z");
    add_action("wsiadz", "wejd^x");   //wsiadanie na poklad promu
}

int
wsiadz(string str)
{    
    object obj;
    if(query_verb() ~= "wsi^ad^x")
        notify_fail("Wsi^ad^x na co?\n");
    else
        notify_fail("Wejd^x na co?\n");
    
    
    if(!strlen(str))
        return 0;
    
    if(!parse_command(str, ENV(TP), "'na' %o:" + PL_BIE, obj))
        return 0;
        
    if(obj != TO)
        return 0;

    //Jezeli z jakiegos powodu prom wyplynal, ale zostal na lokacji przystani.
    if(wnetrze->query_plyniemy() == 1)
    {
        TP->catch_msg("Tego promu nie powinno tutaj by^c w tym momencie. "+
            "Zg^lo^s b^l^ad na lokacji podaj^ac w nim kod: jut561.\n");
        return 1;
    }
    
    //Nie mozna sie wczolgac/przeturlac na prom.
    if(TP->query_prop(SIT_SIEDZACY) || TP->query_prop(SIT_LEZACY))
    {
        write("Musisz najpierw wsta^c.\n");
        return 1;
    }

    if(member_array(TP->query_real_name(),przewoznik->query_wrogowie()) != -1)
    {
        przewoznik->hook_olej_wroga(TP);
        TP->catch_msg("Zdaje si^e, ^ze czym� sobie musia^l"+
            TP->koncowka("e�","a�")+" nagrabi^c i teraz "+
            QIMIE(przewoznik,PL_BIE)+" nie chce ci^e obs^lu^zy^c.\n");
        return 1;
    }

    if(present(przewoznik, ENV(TP)))
    {
        if(cena != 0)
        {
            if(can_pay(cena, TP))
            {
                pay(cena, TP);
                TP->catch_msg("P^lacisz nale^zn� kwot^e przewo^xnikowi "+
                    "i wsiadasz do " + TO->short(PL_DOP) + ".\n");
            }
            else
            {
                TP->catch_msg("Nie sta^c ci^e na to.\n");
                return 1;
            }
        }
        else
        {
            TP->catch_msg("Wsiadasz na "+TO->short(PL_BIE)+".\n");
        }
    }
    else
    {
        TP->catch_msg("Nie ma przewo^xnika, zg^lo^s b^l^ad na lokacji "+
            "podaj^ac kod: jut563.\n");
        return 1;
    }
   
    saybb(QCIMIE(TP, PL_MIA)+" wsiada na "+TO->short(PL_BIE)+".\n");
    tell_roombb(wnetrze, QCIMIE(TP, PL_MIA)+" wchodzi na pok^lad.\n");
    TP->move_living("M", wnetrze, 1);
    return 1;
}

void odplywamy()
{
    ktora_przystan++;
    set_alarm(itof(czas_podrozy+czas_podrozy_random),0.0,"doplywamy");
    przewoznik->command("krzyknij Odp^lywamy!");
    tell_roombb(ENV(TO),QCIMIE(przewoznik,PL_MIA)+" wchodzi na pok^lad.\n");
    tell_roombb(wnetrze,QCIMIE(przewoznik,PL_MIA)+" wchodzi na pok^lad.\n");
    przewoznik->move_living("M", wnetrze, 1);
    tell_roombb(ENV(TO),capitalize(short(TO,PL_MIA))+" "+leave_msg+".\n");
    wnetrze->ustaw_plyniemy(1);
    move_object(PUSTKA);
}

void doplywamy()
{
    wnetrze->ustaw_ostatni_port(przystanie_sciezki[ktora_przystan-1]);
    alarm_id = set_alarm(itof(czas_postoju),0.0,"odplywamy");
    move_object(przystanie_sciezki[ktora_przystan-1]);
    tell_roombb(ENV(TO),capitalize(short(TO,PL_MIA))+" "+enter_msg+".\n");
    wnetrze->ustaw_plyniemy(0);
    przewoznik->command("krzyknij Dop^lyneli^smy! "+
        przystanie[ktora_przystan-1]+".");
    if(sizeof(przystanie) == ktora_przystan)
    {
        odwracamy_przystanie();
        ktora_przystan = 1;
    }
    przewoznik->command("krzyknij Nast^epny przystanek - "+
        przystanie[ktora_przystan]+".");
    przewoznik->command("zejdz z pokladu");
    przewoznik->command("krzyknij Nast^epny przystanek - "+
        przystanie[ktora_przystan]+".");
}

void uciekamy()
{
    remove_alarm(alarm_id);
    ktora_przystan++;
    set_alarm(itof(czas_podrozy+czas_podrozy_random),0.0,"doplywamy");
    tell_roombb(ENV(TO),QCIMIE(przewoznik,PL_MIA)+" wbiega na pok^lad.\n");
    tell_roombb(wnetrze,QCIMIE(przewoznik,PL_MIA)+" wbiega na pok^lad.\n");
    przewoznik->move_living("M", wnetrze, 1);
    tell_roombb(ENV(TO),capitalize(short(TO,PL_MIA))+" "+leave_msg+".\n");
    wnetrze->ustaw_plyniemy(1);
    move_object(PUSTKA); //pustka
}
/**** FUNKCJE POMOCNICZNE ****/

void odwracamy_przystanie()
{
    string tmp;
    int k = sizeof(przystanie)-1;
    for(int i=0;i<=(k/2);i++)
    {
        tmp = przystanie[i];
        przystanie[i]=przystanie[k-i];
        przystanie[k-i] = tmp;
        tmp = przystanie_sciezki[i];
        przystanie_sciezki[i]=przystanie_sciezki[k-i];
        przystanie_sciezki[k-i] = tmp;
    }
}

void start()
{
    przewoznik = clone_object(przewoznik_sciezka);

    if(!objectp(wnetrze))
        wnetrze = find_object(wnetrze_sciezka);

    if(objectp(przewoznik))
        przewoznik->move(wnetrze);

    if(!objectp(wnetrze->query_przewoznik()))
        wnetrze->ustaw_przewoznika_o("przewoznik_std_prom");

    if(!objectp(wnetrze->query_prom()))
        wnetrze->ustaw_prom(TO);

    przewoznik->init_arg();
    przewoznik->ustaw_prom(TO);
    doplywamy();
}

public string
query_auto_load()
{
    return 0;
}
