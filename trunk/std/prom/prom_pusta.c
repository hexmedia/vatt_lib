/**
 * Standard promow - lokacja na ktora wplywa prom, niedostepna dla graczy
 *
 * @author Avard
 * @date 22.12.09
 *
 */
inherit "/std/room";

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <pogoda.h>

void create_room() 
{
    set_short("Woda");
}

string
dlugi_opis()
{
    string str;
    str = "Nie powi"+TP->koncowka("niene^s","nna^s")+" widzie^c tego "+
        "opisu. Zg^lo^s b^l^ad.\n";
    return str;
}