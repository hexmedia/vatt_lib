/**
 * Standard promow - przewoznik na promie
 *
 * @author Avard
 * @date 22.12.09
 *
 */

#include <macros.h>

inherit "/std/humanoid.c";

void wyrzucanie(object prom, object przewoznik, object kto);
void dodaj_wroga(string imie);
string *query_wrogowie();

string *wrogowie = ({ });
object prom;

void
create_przewoznik()
{
}

nomask void
create_humanoid()
{
    set_living_name("przewoznik_std_prom");//NIE ZMIENIAC!
    ustaw_odmiane_rasy(PL_MEZCZYZNA);
    set_long("Standardowy przewoznik.\n");

    create_przewoznik();
}

void
ustaw_prom(object ob)
{
    prom = ob;
}

object
query_prom()
{
    return prom;
}
/**
 * Nazwa funkcji: wyrzucanie
 * Opis: Funkcja wykonuje sie w momencie gdy przewoznik chce wyrzucic
 *       kogos ze swojego promu. Mozna ja nadpisac aby spersonalizowac
 *       przewoznika (zalecane).
 * Argumenty: object prom - obiekt promu
 *            object przewoznik - obiekt przewoznika
 *            object kto - kogo przewoznik ma wyrzucic z promu
 */ 
void wyrzucanie(object prom, object przewoznik, object kto)
{
    command("'To nie by^l najlepszy pomys^l!");
    kto->catch_msg(QCIMIE(przewoznik,PL_MIA) + " wyrzuca ci^e z promu.\n");
    tell_roombb(environment(prom), QCIMIE(kto, PL_MIA)+" zostaje wyrzucony "+
        "z pok^ladu promu.\n");
    kto->move_living("M", environment(prom),1,1);
    tell_roombb(environment(przewoznik), QCIMIE(przewoznik,PL_MIA)+
        " wyrzuca "+QIMIE(kto,PL_DOP)+" z promu.\n");
    
}

void
dodaj_wroga(string imie)
{
    if(member_array(imie,wrogowie) == -1)
        wrogowie += ({imie});
}

string *
query_wrogowie()
{
    return wrogowie;
}

void attacked_by(object wrog)
{
    dodaj_wroga(wrog->query_real_name());
    if(ENV(TO) == ENV(prom))
    {
        command("'Zostaw mnie!");
        prom->uciekamy();
    }
}

void
hook_olej_wroga(object kogo)
{
    command("'A posz"+kogo->koncowka("ed�","�a","�o")+" mi tu st�d!");
    command("popatrz nieprzyjaznie na "+OB_NAME(kogo));
}

public string
query_auto_load()
{
    return 0;
}
