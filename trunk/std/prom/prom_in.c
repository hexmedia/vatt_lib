/**
 * Standard promow - wnetrze promu
 *
 * @author Avard
 * @date 22.12.09
 *
 */

inherit "/std/room.c";

#include <pogoda.h>
#include <stdproperties.h>
#include <mudtime.h>
#include <macros.h>
#include <pl.h>

#define DBG(x) find_player("avard")->catch_msg(x+"\n");

void ustaw_prom(object ob);
void ustaw_przewoznika_o(string str);
void ustaw_ostatni_port(string str);
string query_ostatni_port();
object query_prom();
object query_przewoznik();
void ustaw_plyniemy(int a);
int query_plyniemy();
int no_attack();

int plyniemy = 0;
object prom;
object przewoznik;
object par;
string prom_sciezka;
string ostatni;

void create_poklad()
{
}

nomask void create_room()
{
    set_short("Pok^lad");
    set_long("Bardzo fajny pok^lad, bardzo fajnego, standardowego promu.\n");

    add_prop(ROOM_I_INSIDE,0);
    add_prop(ROOM_M_NO_ATTACK, &no_attack());

    create_poklad();
}

init()
{
    ::init();
    add_action("zejdz", "zejd^x");
}

public string
exits_description()
{
    if(plyniemy == 0)
        return "Mo^zesz zej^s^c z pok^ladu.\n";
    else
        return "";
}
/**** FUNKCJE SET I QUERY ****/

void ustaw_prom(object ob)//Funkcja ta wywoluje sie w kodzie promu w funkcji
{                       //set_wnetrze
    prom = ob;
}

object query_prom()
{
    return prom;
}

void ustaw_przewoznika_o(string str)
{
    przewoznik = find_living(str);
}

object query_przewoznik()
{
    return przewoznik;
}

void ustaw_plyniemy(int a)
{
    plyniemy = a;
}

int query_plyniemy()
{
    return plyniemy;
}

void ustaw_ostatni_port(string str)
{
    ostatni = str;
}

string query_ostatni_port()
{
    return ostatni;
}

/**** FUNKCJE Z AKCJAMI ****/

int zejdz(string str)
{
    notify_fail("Z czego chcesz zej^s^c? Mo^ze z pok^ladu?\n");

    if(plyniemy == 1)
    {
        write("Nie mo^zesz teraz zej^s^c na brzeg. Prom w^la^snie "+
            "p^lynie.\n");
        return 1;
    }

    if(!str)
        return 0;

    if(!parse_command(str, TP, "'z' 'pok^ladu'"))
        return 0;

    write("Schodzisz z pok^ladu.\n");

    saybb(QCIMIE(TP, PL_MIA)+" schodzi z pok^ladu.\n");
    tell_roombb(ENV(prom),QCIMIE(TP, PL_MIA)+" schodzi z pok^ladu na "+
        "brzeg.\n");
    TP->move_living("M", ENV(prom), 1);
    return 1;
}

int
sprawdz(object kto)
{
    if(!kto->query_attack())
        return 0;
    /*------ Debug Mode --------------------
    DBG("PRZED WARUNKIEM");
    if(przewoznik != 0)
        DBG("JEST PRZEWOZNIK");
    if(prom != 0)
        DBG("JEST PROM");
    if(ENV(przewoznik) != 0)
        DBG("JEST ENV PRZEWOZNIK");
    if(przewoznik->query_env() != 0)
        DBG("JEST ENV PRZEWOZNIK");
    if(ENV(prom) != 0)
        DBG("JEST ENV PROM");
    ----------------------------------------*/
    if(ENV(przewoznik) == ENV(prom))
    {
        tell_roombb(ENV(przewoznik), QCIMIE(przewoznik,PL_MIA)+
            " wbiega na prom.\n");
        przewoznik->move_living("M", TO, 1);
        tell_roombb(ENV(przewoznik), QCIMIE(przewoznik,PL_MIA)+
            " wbiega na prom.\n");
        przewoznik->wyrzucanie(prom, przewoznik,kto);
        tell_roombb(ENV(przewoznik), QCIMIE(przewoznik,PL_MIA)+
            " schodzi z pok�adu.\n");
        przewoznik->move_living("M", ENV(prom), 1);
        tell_roombb(ENV(przewoznik), QCIMIE(przewoznik,PL_MIA)+
            " schodzi z pok�adu.\n");  
    }
    else if(ENV(przewoznik) == TO) 
    {
        przewoznik->wyrzucanie(prom,przewoznik,kto);
        par = clone_object("/std/prom/wyrzucenie_paraliz");
        par->ustaw_ostatni_port(ostatni);
        par->move(kto);
        
    }
    else
    {
        DBG("Problem z promem, sprawdz");
    }
    return 1;
}

/**** FUNKCJE POMOCNICZE ****/

int
no_attack()
{
    set_alarm(1.0,0.0,"sprawdz",TP);
    return 0;
}

public string
query_auto_load()
{
    return 0;
}
