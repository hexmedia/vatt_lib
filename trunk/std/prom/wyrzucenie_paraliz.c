/** 
 * Autor: Avard
 * Info : Paraliz nakladany na gracza gdy zostanie wyrzucony za burte promu.
 * Opisy eventow dla Pontaru: Veli
 */

#pragma strict_types
#include <macros.h>
#include <pogoda.h>
#include <mudtime.h>
#include <ss_types.h>

inherit "/std/paralyze";

#define ILE_EVENTOW 7

int i = 1;
string ostatni_port = "";

void evencioszki();
void ustaw_ostatni_port(string str);
string query_ostatni_port();

void
create_paralyze()
{
    set_name("wyrzucony_z_promu_do_wody");
    set_fail_message("Nie mo^zesz tego zrobi^c, walczysz o swoje ^zycie.\n");
    set_finish_object(this_object());
    set_finish_fun("wyrzucony_z_promu_na_brzeg");
    set_remove_time(ILE_EVENTOW*7);
    set_block_all();
    setuid();
    seteuid(getuid());
    evencioszki();
}

void
wyrzucony_z_promu_na_brzeg(object player)
{
    int zmeczenie = player->query_fatigue();
    if(player->query_skill(SS_SWIM) < 40)
    {
        //TP->do_die(0,1);
        //Trzeba tutaj gracza ogluszyc, bo sie podtapia i traci przytomnosc. 
        saybb(QCIMIE(player,PL_MIA)+" zosta^l"+koncowka("","a","o")+
            " wyrzucon"+koncowka("y","a","e")+" na brzeg.\n");
        player->move_living("M",ostatni_port);
        player->set_fatigue(zmeczenie - zmeczenie); //100% zabieramy
    }
    else
    {
        player->catch_msg("Wyp^lywasz na brzeg.\n");
        saybb(QCIMIE(player,PL_MIA)+" wyp^lywa na brzeg.\n");
        player->move_living("M",ostatni_port);
        player->set_fatigue(zmeczenie - ((zmeczenie/10)*9)); //90% zabieramy
    }
    //peace
    remove_object();
    return;
}

void ustaw_ostatni_port(string str)
{
    ostatni_port = str;
}

string query_ostatni_port()
{
    return ostatni_port;
}

void
evencioszki()
{
    if(i < ILE_EVENTOW)
        set_alarm(7.0,0.0,"evencioszki");
       
       // Opisy dla Pontaru
    switch(i)
    {
        //-------------------
        case 1: 
        if(MT_PORA_ROKU == MT_WIOSNA)
            TP->catch_msg("W asy^scie g^lo^snego plusku wpadasz do "+
                "wezbra^lych po zimie, mulistych w^od Pontaru!\n");
        else if(MT_PORA_ROKU == MT_ZIMA)
            TP->catch_msg("W asy^scie g^lo^snego plusku wpadasz do "+
                "lodowatych, zamulonych w^od Pontaru!\n");
        else 
            TP->catch_msg("W asy^scie g^lo^snego plusku wpadasz do "+
                "ciep^lawych, mulistych i pe^lnych zakwit^lych glon^ow "+
                "w^od Pontaru!\n");
        break;

        //-------------------
        case 2: 
            TP->catch_msg("Brunatne strugi wody zamykaj^a si^e za tob^a, a "+
            "krzyk wi^e^xnie ci w gardle, gdy odruchowo starasz si^e "+
            "zlapa^c oddech!\n");
        break;
        
        //-------------------
        case 3: 
            TP->catch_msg("Woda wdziera ci si^e do ust i zalewa oczy!"+
            " Krztusisz si^e!\n");
        break;

        //-------------------
        case 4:
        if(TP->query_skill(SS_SWIM) < 20)
            TP->catch_msg("Rozpaczliwie wymachujesz rekami i nogami "+
            "usiluj^ac z^lapa^c oddech!\n");
        else if(TP->query_skill(SS_SWIM) < 40)
            TP->catch_msg("Z trudem, wyrzucasz ramiona do przodu usiluj^ac "+
            "pokona^c silny nurt, jednak si^la ^zywio^lu przyt^lacza ci^e "+
            "bezlito^snie!\n");
        else
            TP->catch_msg("Odruchowo zamykasz usta, by powstrzyma^c "+
            "wdzieraj^ac^a si^e w ka^zdy zakamarek brudn^a wod^e i "+
            "wymachuj^ac rozplaczliwie ko^nczynami usi^lujesz wyp^lyn^ac "+
            "na powierzchni^e.\n");
        break;

        //-------------------
        case 5:
        if(TP->query_skill(SS_SWIM) < 40)
            TP->catch_msg("Czujesz piek^acy b^ol, gdy po^lykasz "+
            "smierdz^acy, rzeczny mu^l, a pod powiekami ^swiat "+
            "wybucha czerwieni^a!\n");
        else
            TP->catch_msg("Ca^l^a si^l^a woli i niemal bez tchu "+
            "rozcinasz to^n ch^lodnej wody i gwa^ltownie wci^agasz "+
            "powietrze do p^luc.\n");
        break;

        //-------------------
        case 6:
        if(TP->query_skill(SS_SWIM) < 40)
            TP->catch_msg("Zach^lystujesz si^e wod^a!\n");
        else
            TP->catch_msg("Zanosisz si^e ostrym kaszlem, gdy ^lapiesz "+
            "pi^ek^ace powietrze.\n");
        break;

        //-------------------
        case 7:
        if(TP->query_skill(SS_SWIM) < 40)
            TP->catch_msg("Czy tak wygl^ada koniec? Powoli tracisz "+
            "przytomno^s^c...\n");
        else
            TP->catch_msg("Czujesz jak obejmuje ci^e leniwy nurt rzeki "+
            "znosi powoli twoje kompletnie wycie^nczone cialo w strone "+
            "bezpiecznego brzegu...\n");
        break;

        //-------------------
        default: TP->catch_msg("Wystapil blad w promie, zg^lo^s go prosz^e "+
            "podaj^ac jego kod: par53lin.\n");break;
    } 
    i++;
}
