/*
 *   Jest to butelka oleju do lamp. Sama butelka jest beczulka,
 *       zatem mozemy wykorzystac butelke do innych celow, po
 *       spozytkowaniu oleju.
 *                                          Lil. 18.09.2005
 */


inherit "/std/beczulka";
inherit "/lib/keep";

#include <stdproperties.h>
#include <macros.h>
#include <composite.h>
#include <materialy.h>
#include <cmdparse.h>

void
create_olej()
{
}

void
create_beczulka()
{
    ustaw_nazwe("butelka");
    dodaj_nazwy("olej");

    set_keep();


    // Wartosc pelnej butelki, spada w miare zuzycia
//    set_value(100);

    add_prop(OBJ_I_WEIGHT, 1500);
    add_prop(OBJ_I_VOLUME, 1500);
    add_prop(OBJ_I_VALUE, 110);
//    add_prop(OBJ_I_VALUE, actual_value);

    ustaw_material(MATERIALY_RZ_SZKLO, 100);

    create_olej();

    set_pojemnosc(2000);
    set_ilosc_plynu(2000);
    set_opis_plynu("g�stego oleju do lamp");
    set_nazwa_plynu_dop("oleju");


    jak_smakuje = "Smakuje paskudnie.";
    jak_pije_jak_sie_zachowuje = "krzywi si� mimowolnie.";

}


void
zmienia_sie_ilosc_plynu()
{
    int ix;
    if (query_ilosc_plynu() == 0)
    {
        for (ix = 0; ix < 6; ix++)
        {
            remove_name("olej", ix);
            remove_pname("oleje", ix);
        }

        odmien_short();
        odmien_plural_short();

        add_prop(OBJ_I_VALUE, 0);
    }
}

/* Dzialania tej funkcji nie jestem w stanie wytlumaczyc w prosty sposob...
 * To zbyt skomplikowane...
 */
public int
jestem_butelka_oleju()
{
        return 1;
}

/*
 * Function name: fill_other
 * Description  : Funkcja napelniajaca lampy olejem
 * Arguments    : object - co napelniamy
 */
void
fill_other(object co)
{
    int ile, max;

    max = co->query_max_time() - co->query_time(1);

    if (query_ilosc_plynu() == 0)
    {
        write("Z " + short(this_player(), PL_DOP) +
            " trudno ci b�dzie cokolwiek nape�ni�.\n");
        return ;
    }

    ile = min(max, query_ilosc_plynu());

    co->set_time_left(co->query_time(1) + ile);
    set_ilosc_plynu(query_ilosc_plynu() - ile);

    write("Nape�niasz " + co->short(this_player(),PL_BIE) +
        " z " + short(this_player(),PL_DOP) +
	(query_ilosc_plynu() ? "" : " opr�niaj�c j� zupe�nie") + ".\n");
    say(QCIMIE(this_player(),PL_MIA) + " nape�nia " + QSHORT(co,PL_BIE) + ".\n");


        odmien_short();
        odmien_plural_short();
}

public string
query_olej_auto_load()
{
    return " #OLEJ#" + query_ilosc_plynu() + "," + query_pojemnosc() + "#OLEJ# ";
}

public string
init_olej_arg(string arg)
{
    int il_pl, pojem;
    string foobar, toReturn;

    if (arg == 0) {
        return 0;
    }

    if (sscanf(arg, "%s#OLEJ#%d,%d#OLEJ#%s", foobar, il_pl, pojem, toReturn) == 4) {
        foreach (string curstr : explode(foobar, "")) {
            if (curstr != " ")
                return arg;
        }
        set_ilosc_plynu(il_pl);
        set_pojemnosc(pojem);
        return toReturn;
    }
    return arg;
}

public string
query_auto_load()
{
    return ::query_auto_load() + query_olej_auto_load() + query_keep_auto_load();
}

public string
init_arg(string arg)
{
    return init_keep_arg(init_olej_arg(::init_arg(arg)));
}
