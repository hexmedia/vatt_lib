/**
 * \file /std/container.c
 *
 * Contains all routines relating to objects that can hold other objects
 *
 */

#pragma save_binary
#pragma strict_types

inherit "/std/object";

/*
 * If you are going to copy this file, in the purpose of changing
 * it a little to your own need, beware:
 *
 * First try one of the following:
 *
 * 1. Do clone_object(), and then configure it. This object is specially
 *    prepared for configuration.
 *
 * 2. If you still is not pleased with that, create a new empty
 *    object, and make an inheritance of this object on the first line.
 *    This will automatically copy all variables and functions from the
 *    original object. Then, add the functions you want to change. The
 *    original function can still be accessed with '::' prepended on the name.
 *
 * The maintainer of this LPmud might become sad with you if you fail
 * to do any of the above. Ask other wizards if you are doubtful.
 *
 * The reason of this, is that the above saves a lot of memory.

   Defined functions and variables:

   set_room(string/object)        Set the room that is the internal of this
                                  object. If this is set there is no objects
				  in this object.

   update_light()                 This is usually not called except when an
                                  object CHANGES light status inside this
				  container.

   There are query_xx() functions for all of the above set_xx() functions.

   These usual functions are also defined:
   reset(), long(), short(), id()

*/
#include <macros.h>
#include <stdproperties.h>
#include <composite.h>
#include <subloc.h>
#include <ss_types.h>

/*
 * All variables in this file must be declared static. This is to ensure
 * that the player object higher in the inherit chain can use save and
 * restore object without saving variables from this file.
 */
static  object      cont_linkroom;      /* The room connected to this container */

static  int         cont_cur_light,     /* The current light status in cont. */
                    cont_cur_weight,
                    cont_cur_volume,
                    cont_block_prop,
                    cosbylopod,         /* co� by�o pod i zosta�o podczas przesuni�cia */
                    cosbylona;          /* co� by�o na i zosta�o podczas przesuni�cia */

static  mapping cont_sublocs,           /* Map of sublocations and the object res-
                                            ponsible for the subloc, in container */
                            /* jeremian's changes */
                cont_sublocs_props,     /* w�a�ciwo�ci sublokacji */
                            /* --- */
                cont_subloc_ids;        /* Map of sublocation ids to sublocation */

static mixed container_objects = ({ });             // tablica sciezek do npcow
static mixed container_objects_present = ({ });     // tablica npcow
static mixed container_objects_functions = ({ });   // tablica funkcji
static mixed container_objects_sublocs = ({ });     // tablica docelowych sublokacji

static int enable_clone_objects;

/*
 * Prototypes
 */
void create_container();
void reset_container();
void update_internal(int l, int w, int v, int a);
public int light();
public nomask int weight();
public nomask int volume();
nomask void fix_ob_subloc_when_remove(object ob, string sloc);
nomask string *map_subloc_id(string *sloc, string rm_sloc);
nomask int filter_subloc_id(string *sloc, string rm_sloc);
nomask varargs int subloc_filter(object ob, mixed sloc, int przyp);

//funkcja sprawdza czy tab1 == tab2
int
equiv_array(mixed tab1, mixed tab2)
{
    int j;
    if (!(pointerp(tab1) && pointerp(tab2)))
        return 0;
    if (sizeof(tab1) == sizeof(tab2))
    {
        j = 0;
        foreach( string elem : tab1)
        {
            if (elem != tab2[j])
                break;
            ++j;
        }

        if (j == sizeof(tab2))
            return 1;
    }
    return 0;
}

/*
 * Function name: create_object
 * Description:   Create the container (constructor)
 */
public nomask void
create_object()
{
    cont_block_prop = 0;
    add_prop(CONT_I_IN, 1);         /* Can have things inside it */
    add_prop(OBJ_I_LIGHT, light);   /* The total light of container */
    add_prop(OBJ_I_WEIGHT, weight); /* The total weight of container */
    add_prop(OBJ_I_VOLUME, volume); /* The total volume of container */
    add_prop(CONT_I_REDUCE_WEIGHT, 100); /* No reduction */
    add_prop(CONT_I_REDUCE_VOLUME, 100); /* No reduction */
    cont_block_prop = 1;
    cont_sublocs = ([]);
    cont_subloc_ids = ([]);
    /* jeremian's changes */
    cont_sublocs_props = ([]);
    /* --- */

    enable_reset();

    create_container();

    reset();
}

/*
 * Block functions disabling setting of calculated properties
 */
nomask int add_prop_obj_i_light() { return cont_block_prop; }

public void
add_prop(string prop, mixed val)
{
    if (cont_block_prop)
    {
        if(prop ~= OBJ_I_WEIGHT)
            return ::add_prop(CONT_I_WEIGHT, val);
        else if (prop ~= OBJ_I_VOLUME)
            return ::add_prop(CONT_I_VOLUME, val);
    }

    return ::add_prop(prop, val);
}

/*
 * Function name: create_container
 * Description:   Reset the container (standard)
 */
public void
create_container()
{    
    ustaw_nazwe(({"pojemnik", "pojemnika", "pojemnikowi", "pojemnik", "pojemnikiem", "pojemniku"}),
        ({"pojemniki", "pojemnik�w", "pojemnikom", "pojemniki", "pojemnikami", "pojemnikach"}),
        PL_MESKI_NOS_NZYW);
                 
    add_prop(CONT_I_WEIGHT, 1000);	/* 1 Kg is weight of empty container */
    add_prop(CONT_I_VOLUME, 1000);	/* 1 liter is volume of empty cont. */
    add_prop(CONT_I_IL_RZECZY, 0);	/* na pocz�tku nic nie ma w kontenerze */

    add_prop(CONT_I_MAX_WEIGHT, 1000); /* 1 Kg is max total weight */
    add_prop(CONT_I_MAX_VOLUME, 1000); /* 1 liter is max total volume */
}

/**
 * Funkcja kolejkuj�ca obiekty do dodania do kontenera.
 *
 * Obiekty s� dodawane podczas resetu kontenera, ale tylko wtedy, gdy nie
 * ma ich ju� stworzonych na mudzie.
 *
 * @param path �cie�ka do pliku obiektu.
 * @param ile ile kopii ma si� doda� (defaultowo <b>1</b>).
 * @param fun funkcja odpowiedzialna za stwierdzenie, czy obiekt
 *            ma by� dodany, czy nie.
 * @param subloc docelowa sublokacja dodania przedmiotu
 */
void
add_object(string path, int ile = 1, mixed fun = 0, mixed subloc = 0)
{
    if (path[-2..] == ".c")
        path = path[..-3];

    while (ile--)
    {
        container_objects += ({ path });
        container_objects_functions += ({ fun });
        container_objects_present += ({ 0 });
        container_objects_sublocs += ({ subloc });
    }
}

/**
 * Parsuje nazwe sublokacji i zwraca jej typ.
 *
 * @param sloc nazwa sublokacji
 *
 * @return <ul>
 *           <li> <b>0</b> - defaultowa sublokacja 'w'
 *           <li> <b>1</b> - sublokacja typu 'w'
 *           <li> <b>2</b> - sublokacja typu 'na'
 *           <li> <b>3</b> - sublokacja typu 'pod'
 *           <li> <b>4</b> - nieznany typ
 *         </ul>
 */
int
parse_subloc_type(mixed sloc)
{
  if (pointerp(sloc)) {
    if (sizeof(sloc) == 7) {
      if (stringp(sloc[6])) {
        if (sloc[6] == "w") {
          return 1;
        }
        else if (sloc[6] == "do") {
          return 1;
        }
        else if (sloc[6] == "na") {
          return 2;
        }
        else if (sloc[6] == "pod") {
          return 3;
        }
      }
    }
    else if (sizeof(sloc) == 6) {
      return 1;
    }
  }
  else {
    if (sloc == 0) {
      return 1; /* na razie tak b�dzie wygodniej - zreszt� defaultowa lokacja jest typu 'w' */
    }
    else if (sloc ~= "do") {
      return 1;
    }
    else if (sloc ~= "na") {
      return 2;
    }
    else if (sloc ~= "pod") {
      return 3;
    }
  }
  return 4;
}

/**
 * Liberalnie parsuje nazwe sublokacji i zwraca jej typ
 *
 * @param sloc nazwa sublokacji
 *
 * @return <ul>
 *           <li> <b>0</b> - defaultowa sublokacja 'w'
 *           <li> <b>1</b> - sublokacja typu 'w'
 *           <li> <b>2</b> - sublokacja typu 'na'
 *           <li> <b>3</b> - sublokacja typu 'pod'
 *           <li> <b>4</b> - nieznany typ
 *         </ul>
 */
int
liberal_parse_subloc_type(mixed sloc)
{
  if (pointerp(sloc)) {
    if (sizeof(sloc) == 7) {
      if (stringp(sloc[6])) {
        if (sloc[6] == "w") {
          return 1;
        }
        else if (sloc[6] == "do") {
          return 1;
        }
        else if (sloc[6] == "na") {
          return 2;
        }
        else if (sloc[6] == "pod") {
          return 3;
        }
      }
    }
    else if (sizeof(sloc) == 6) {
      return 1;
    }
  }
  else {
    if (sloc == 0) {
      return 1; /* na razie tak b�dzie wygodniej - zreszt� defaultowa lokacja jest typu 'w' */
    }
    else if (sloc[0..1] ~= "do") {
      return 1;
    }
    else if (sloc[0..1] ~= "na") {
      return 2;
    }
    else if (sloc[0..2] ~= "pod") {
      return 3;
    }
  }
  return 4;
}

/*
 * Nazwa: add_subloc_prop
 * Opis: Dzia�anie identyczne jak add_prop,
 *       tylko �e dotyczy sublokacji.
 * Argumenty: sloc - Nazwa sublokacji.
 *            prop - Nazwa w�a�ciwo�ci do dodania.
 *            val - Warto�� w�a�ciwo�ci.
 * Zwraca: None.
 */
public void
add_subloc_prop(mixed sloc, string prop, mixed val)
{
	mixed oval;

  if (pointerp(sloc)) {
    sloc = sloc[0];
  }

	/* je�eli val nie jest ustawione, to usuwamy aktualn� warto�� */
	if (!val) {
		this_object()->remove_subloc_prop(sloc, prop);
		return;
	}

	/* jeste�my zgodni z blokowaniem zmian tak jak w add_prop */
	if (obj_no_change || call_other(this_object(), "add_subloc_prop" + sloc + prop, val)) {
		return;
	}

	if (!mappingp(cont_sublocs_props)) {
		cont_sublocs_props = ([]);
	}

	/* jak mapping dla danej sublokacji nie istnieje - tworzymy go */
	if (!mappingp(cont_sublocs_props[sloc])) {
		cont_sublocs_props[sloc] = ([]);
	}

	oval = this_object()->query_subloc_prop(sloc, prop);

	(cont_sublocs_props[sloc])[prop] = val;

	/* powiadamiamy o zmianie w�a�ciwo�ci kontener */
	this_object()->notify_change_subloc_prop(sloc,
			                         prop,
						 this_object()->query_subloc_prop(sloc, prop),
						 oval);
}

/*
 * Nazwa: change_subloc_prop
 * Opis: Funkcja opakowuj�ca add_subloc_prop.
 */
public void
change_subloc_prop(mixed sloc, string prop, mixed val)
{
  if (pointerp(sloc)) {
    sloc = sloc[0];
  }
	add_subloc_prop(sloc, prop, val);
}

/*
 * Nazwa: remove_subloc_prop
 * Opis: Dzia�anie identyczne jak remove_prop,
 *       tylko �e dotyczy sublokacji.
 * Argumenty: sloc - Nazwa sublokacji.
 *            prop - Nazwa w�a�ciwo�ci do usuni�cia.
 * Zwraca: None.
 */
public void
remove_subloc_prop(mixed sloc, string prop)
{
  if (pointerp(sloc)) {
    sloc = sloc[0];
  }
	/* je�eli wszelkie zmiany zosta�y zablokowane, to nic nie robimy */
	if (obj_no_change ||
	    !mappingp(cont_sublocs_props) ||
	    !prop ||
	    !mappingp(cont_sublocs_props[sloc]) ||
	    call_other(this_object(), "remove_subloc_prop" + sloc + prop)) {
		return;
	}

	/* powiadamiamy kontener o zmianach */
	this_object()->notify_change_subloc_prop(sloc,
			                         prop,
						 0,
						 this_object()->query_subloc_prop(sloc, prop));

	cont_sublocs_props[sloc] = m_delete(cont_sublocs_props[sloc], prop);
}

/*
 * Nazwa: query_subloc_prop
 * Opis: Znajduje warto�� w�a�ciwo�ci danej sublokacji
 * Argumenty: sloc - Nazwa sublokacji.
 *            prop - Nazwa w�a�ciwo�ci do znalezienia.
 * Zwraca: Warto�� albo 0.
 */
public mixed
query_subloc_prop(mixed sloc, string prop)
{
  if (pointerp(sloc)) {
    sloc = sloc[0];
  }
	if (!mappingp(cont_sublocs_props) || !mappingp(cont_sublocs_props[sloc])) {
		return 0;
	}
	return check_call((cont_sublocs_props[sloc])[prop]);
}

/*
 * Nazwa: query_subloc_props
 * Opis: Zwraca wszystkie w�a�ciwo�ci sublokacji
 * Argumenty: sloc - Nazwa sublokacji.
 * Zwraca: Tablica w�a�ciwo�ci albo 0.
 */
public nomask mixed
query_subloc_props(mixed sloc)
{
  if (pointerp(sloc)) {
    sloc = sloc[0];
  }
	if (!mappingp(cont_sublocs_props) || !mappingp(cont_sublocs_props[sloc])) {
		return 0;
	}
	return m_indexes(cont_sublocs_props[sloc]);
}

/**
 * Zale�nie od ustawionego rodzaju sublokacji (za pomoc� propa
 * SUBLOC_I_RODZAJ), zwraca jedn� z ko�c�wek.
 *
 * @param sloc sublokacja, dla kt�rej szukamy ko�c�wki
 * @param meski ko�c�wka zwr�cona w przypadku rodzaju m�skiego
 * @param zenski ko�c�wka zwr�cona w przypadki rodzaju �e�skiego
 * @param nijaki ko�c�wka zwr�cona w przypadku rodzaju nijakiego
 * @param mos ko�c�wka zwr�cona w przypadku rodzaju m�skoosobowego
 * @param mnos ko�c�wka zwr�cona w przypadku rodzaju niem�skoosobowego
 *
 * @return ko�c�wka zale�na od rodzaju sublokacji.
 */
public varargs string
subloc_koncowka(mixed sloc, string meski, string zenski, string nijaki = 0, string mos = 0, string mnos = 0)
{
  int rodzaj = query_subloc_prop(sloc, SUBLOC_I_RODZAJ);

  if (query_subloc_prop(sloc, SUBLOC_I_TYLKO_MN))
  {
    if (rodzaj == -(PL_MESKI_OS+1) )
      return mos;
    else
      return mnos;
  }

  switch (rodzaj)
  {
    case PL_MESKI_OS:
    case PL_MESKI_NOS_ZYW:
    case PL_MESKI_NOS_NZYW:
      return meski;
    case PL_ZENSKI:
      return zenski;
    case PL_NIJAKI_OS:
    case PL_NIJAKI_NOS:
      if (!nijaki)
        return meski;
      else
        return nijaki;
  }
}

/*
 * Nazwa: query_subloc_prop_setting
 * Opis: Zwraca prawdziw� warto�� w�a�ciwo�ci
 * Argumenty: sloc - Nazwa sublokacji.
 *            prop - Nazwa w�a�ciwo�ci.
 * Zwraca: Prawdziw� warto�� (mixed).
 */
public nomask mixed
query_subloc_prop_setting(mixed sloc, string prop)
{
  if (pointerp(sloc)) {
    sloc = sloc[0];
  }
	if (!mappingp(cont_sublocs_props) || !mappingp(cont_sublocs_props[sloc])) {
		return 0;
	}
	return (cont_sublocs_props[sloc])[prop];
}

/*
 * Nazwa: notify_change_subloc_prop
 * Opis: Ta funkcja jest wywo�ywana, gdy zmianiamy
 *       w�a�ciwo�ci sublokacji.
 * Argumenty: sloc - Nazwa sublokacji.
 *            prop - Nazwa w�a�ciwo�ci.
 *            val - Nowa warto��.
 *            oval - Stara warto��.
 */
public void
notify_change_subloc_prop(mixed sloc, string prop, mixed val, mixed oval)
{
}

/*
 * Nazwa: is_subloc_prop_set
 * Opis: Sprawdzenie, czy jaka� w�a�ciwo�� jest ustawiona
 * Argumenty: sloc - Nazwa sublokacji.
 *            prop - Nazwa w�a�ciwo�ci.
 * Zwraca: 1 - dana w�a�ciwo�� jest ustawiona
 *         0 - dana w�a�ciwo�� nie jest ustawiona
 */

public int
is_subloc_prop_set(mixed sloc, string prop)
{
  if (pointerp(sloc)) {
    sloc = sloc[0];
  }
	if (!mappingp(cont_sublocs_props) || !mappingp(cont_sublocs_props[sloc])) {
		return 0;
	}
	if (member_array(prop, m_indexes(cont_sublocs_props[sloc])) != -1) {
		return 1;
	}
	return 0;
}

public int
czy_cosbylopod() {
	return cosbylopod;
}

public int
czy_cosbylona()
{
    return cosbylona;
}

/* --- */

/*
 * Function name: reset_object
 * Description:   Reset the container
 */
public nomask void
reset_object()
{
    if (enable_clone_objects)
    {
        string res;
        mixed fun;
        int i = sizeof(container_objects);

        while (i--)
        {
            if (container_objects_present[i])
                continue;

            fun = container_objects_functions[i];

            if (fun && !((stringp(fun) && call_self(fun)) || (functionp(fun) && fun())))
                continue;

            if ((res = catch(container_objects_present[i] = clone_object(container_objects[i]))))
            {
                write("\nB��d przy tworzeniu obiektu\"" + container_objects[i] + "\".\n"+res+"\n");
                continue;
            }

            container_objects_present[i]->move(this_object(), container_objects_sublocs[i]);
            container_objects_present[i]->init_arg(0);
            container_objects_present[i]->start_me();
        }
    }

    reset_container();
}

/*
 * Function name: reset_container
 * Description:   Reset the container (standard)
 */
public void
reset_container()
{
}

/*
 * Function name: volume_left()
 * Description:   Returns the volume left to fill.
 * Returns:	  non_negative integer
 */
public int
volume_left()
{
    if (query_prop(CONT_I_RIGID))
	return query_prop(CONT_I_MAX_VOLUME) - query_prop(CONT_I_VOLUME) -
	    cont_cur_volume;
    else
	return query_prop(CONT_I_MAX_VOLUME) - query_prop(OBJ_I_VOLUME);
}

/*
 * Function name: query_internal_light
 * Description:   Returns the lightvalue of object internal to this container
 * Returns:	  Lightvalue
 */
public int
query_internal_light() { return cont_cur_light; }

/*
 * Function name: light
 * Description:   Returns the light status in this container
 *                This function is called from query_prop() only.
 * Returns:	  Light value
 */
public int
light()
{
    int li;

    li = query_prop(CONT_I_LIGHT);
    if (query_prop(CONT_I_TRANSP) ||
	query_prop(CONT_I_ATTACH) ||
	!query_prop(CONT_I_CLOSED))
    {
    	if (cont_linkroom)
	    return cont_linkroom->query_prop(OBJ_I_LIGHT) + li;
    	else
	    return cont_cur_light + li;
    }
    else
	return li;
}


/*
 * Function name: weight
 * Description:   Returns the accumulated weight of the container.
 *                This function is called from query_prop() only.
 * Returns:	  Weight value
 */
public nomask int
weight()
{
    int wi;

    wi = query_prop(CONT_I_WEIGHT);
    if (cont_linkroom)
 	return cont_linkroom->query_prop(OBJ_I_WEIGHT) + wi;
    else
	return cont_cur_weight + wi;
}

/*
 * Function name: volume
 * Description:   Returns the volume of the container. How much volume it
 *		  currently takes up, a nonrigid sack accumulates this.
 *                This function is called from query_prop() only.
 * Returns:	  Volume value
 */
public nomask int
volume()
{
    int vo;

    if (query_prop(CONT_I_RIGID))
        return query_prop(CONT_I_MAX_VOLUME);

    vo = query_prop(CONT_I_VOLUME);

    if (cont_linkroom)
        return cont_linkroom->query_prop(OBJ_I_VOLUME) + vo;
    else
        return cont_cur_volume + vo;
}


/*
 * Function name: set_room
 * Description:   Connects a room to the internals of the container.
 * Arguments:	  room: The room object or a filename
 */
public void
set_room(mixed room)
{
    if (query_lock())
        return;   /* All changes has been locked out */

    cont_linkroom = room;
}

/*
 * Function name: query_room
 * Description:   Ask what room is connected to the internal of this object
 * Returns:       The object
 */
public object
query_room() { return cont_linkroom; }

/*
 * Function name: enter_inv
 * Description:   Called when objects enter this container or when an
 *                object has just changed its weight/volume/light status.
 * Arguments:     ob: The object that just entered this inventory
 *		  from: The object from which it came.
 */
public void
enter_inv(object ob, object from)
{
    int l, w, v, a;

    if (objectp(cont_linkroom) )
        ob->move(cont_linkroom, ob->query_subloc());

    l = ob->query_prop(OBJ_I_LIGHT);
    w = ob->query_prop(OBJ_I_WEIGHT);
    v = ob->query_prop(OBJ_I_VOLUME);
    if (ob->query_prop(HEAP_I_IS))
        a = ob->num_heap();
    else
        a = 1;

    update_internal(l, w, v, a);
}

/* jeremian's changes */

public void
subloc_enter_inv(mixed sloc, object ob)
{
    if (query_subloc_prop(sloc, CONT_I_MAX_WEIGHT) > 0)
        change_subloc_prop(sloc, CONT_I_WEIGHT, query_subloc_prop(sloc, CONT_I_WEIGHT) + ob->query_prop(OBJ_I_WEIGHT));
    if (query_subloc_prop(sloc, CONT_I_MAX_VOLUME) > 0)
        change_subloc_prop(sloc, CONT_I_VOLUME, query_subloc_prop(sloc, CONT_I_VOLUME) + ob->query_prop(OBJ_I_VOLUME));
    if (query_subloc_prop(sloc, CONT_I_MAX_RZECZY) > 0)
    {
        if (ob->query_prop(HEAP_I_IS))
            change_subloc_prop(sloc, CONT_I_IL_RZECZY, query_subloc_prop(sloc, CONT_I_IL_RZECZY) + ob->num_heap());
        else
            change_subloc_prop(sloc, CONT_I_IL_RZECZY, query_subloc_prop(sloc, CONT_I_IL_RZECZY) + 1);
    }
}

public void
subloc_leave_inv(mixed sloc, object ob)
{
    if (query_subloc_prop(sloc, CONT_I_MAX_WEIGHT) > 0)
        change_subloc_prop(sloc, CONT_I_WEIGHT, query_subloc_prop(sloc, CONT_I_WEIGHT) - ob->query_prop(OBJ_I_WEIGHT));
    if (query_subloc_prop(sloc, CONT_I_MAX_VOLUME) > 0)
        change_subloc_prop(sloc, CONT_I_VOLUME, query_subloc_prop(sloc, CONT_I_VOLUME) - ob->query_prop(OBJ_I_VOLUME));
    if (query_subloc_prop(sloc, CONT_I_MAX_RZECZY) > 0)
    {
        if (ob->query_prop(HEAP_I_IS))
            change_subloc_prop(sloc, CONT_I_IL_RZECZY, query_subloc_prop(sloc, CONT_I_IL_RZECZY) - ob->num_heap());
        else
            change_subloc_prop(sloc, CONT_I_IL_RZECZY, query_subloc_prop(sloc, CONT_I_IL_RZECZY) - 1);
    }
}

/* --- */


/*
 * Function name: leave_inv
 * Description:   Called when objects leave this container or when an
 *                object is about to change its weight/volume/light status.
 * Arguments:     ob: The object that just leaved this inventory
 *		  to: Where it went.
 */
public void
leave_inv(object ob, object to)
{
    int l, w, v, a;

    if (objectp(cont_linkroom))
        return;

    l = ob->query_prop(OBJ_I_LIGHT);
    w = ob->query_prop(OBJ_I_WEIGHT);
    v = ob->query_prop(OBJ_I_VOLUME);
    if (ob->query_prop(HEAP_I_IS))
        a = ob->num_heap();
    else
        a = 1;

    update_internal(-l, -w, -v, -a);

    TO->event_check();
}

/*
 * Function name: enter_env
 * Description:   The container enters a new environment
 * Arguments:     dest - The destination
 *		  old  - From what object
 */
void
enter_env(object dest, object old)
{
    int weight, vol;

    if (dest)
    {
        weight = -cont_cur_weight + cont_cur_weight * 100 /
                dest->query_prop(CONT_I_REDUCE_WEIGHT);

        if (query_prop(CONT_I_RIGID))
            vol = 0;
        else
            vol = -cont_cur_volume + cont_cur_volume * 100 /
                dest->query_prop(CONT_I_REDUCE_VOLUME);

        dest->update_internal(0, weight, vol, 0);
    }

    TO->event_check();

    ::enter_env(dest, old);
}

/*
 * Function name: leave_env
 * Description:   The container leaves an old environment
 * Arguments:     from - From what object
 *		  to   - To what object
 *		  fromSubloc - w jakiej sublokacji by� kontener
 */
varargs void
leave_env(object from, object to, string fromSubloc)
{
    int weight, vol;

    /* jeremian's changes 14.06.2005 */

    int sublocamount, i, j, itemamount;
    string * subloctable;

    /* --- */

    if (from)
    {
        weight = cont_cur_weight - cont_cur_weight * 100 /
            from->query_prop(CONT_I_REDUCE_WEIGHT);

        if (query_prop(CONT_I_RIGID))
            vol = 0;
        else
            vol = cont_cur_volume - cont_cur_volume * 100 / from->query_prop(CONT_I_REDUCE_VOLUME);

        from->update_internal(0, weight, vol, 0);

        /* jeremian's changes 01.07.2005 */

        subloctable = this_object()->query_sublocs();
        sublocamount = sizeof(subloctable);

        cosbylopod = 0;
        cosbylona = 0;

        for (i = 0; i < sublocamount; ++i)
        {
            if ((liberal_parse_subloc_type(subloctable[i]) == 3) &&
                    (query_subloc_prop(subloctable[i], SUBLOC_I_POD_NIE_ZOSTANA) == 0))
            {
                object * aitems = this_object()->subinventory(subloctable[i]);
                itemamount = sizeof(aitems);
                for (j = 0; j < itemamount; ++j)
                {
                    if (fromSubloc)
                        aitems[j]->move(from, fromSubloc);
                    else
                        aitems[j]->move(from, 0);
                    cosbylopod = 1;
                }
            }
            if ((liberal_parse_subloc_type(subloctable[i]) == 2) &&
                    (query_subloc_prop(subloctable[i], SUBLOC_I_NA_NIE_SPADNA) == 0))
            {
                object * aitems = this_object()->subinventory(subloctable[i]);
                itemamount = sizeof(aitems);
                for (j = 0; j < itemamount; ++j)
                {
                    if (fromSubloc)
                        aitems[j]->move(from, fromSubloc);
                    else
                        aitems[j]->move(from, 0);
                    cosbylona = 1;
                }
            }

        }

        /* --- */
    }
    ::leave_env(from, to, fromSubloc);
}

/*
 * Nazwa funkcji : prevent_leave
 * Opis          : Sprawdza, czy dany obiekt moze opuscic ten pojemnik.
 * Argumenty     : object - sprawdzany obiekt.
 * Funkcja zwraca: 1 - jesli _NIE_ moze. 0 - jesli moze.
 */
public int
prevent_leave(object ob)
{
    return 0;
}

/*
 * Nazwa funkcji : prevent_enter
 * Opis          : Sprawdza, czy dany obiekt moze wejsc do tego pojemnika.
 * Argumenty     : object - sprawdzany obiekt.
 * Funkcja zwraca: 1 - jesli _NIE_ moze wejsc, 0 - jesli moze.
 */
public int
prevent_enter(object ob)
{
    return 0;
}

/*
 * Function name: update_internal
 * Description:   Updates the light, weight and volume of things inside
 *                also updates a possible environment.
 * Arguments:     l: Light diff.
 *		  w: Weight diff.
 *		  v: Volume diff.
 *		  a: ilo�� rzeczy
 */
public void
update_internal(int l, int w, int v, int a)
{
    object ob, env;

    cont_cur_light += l;
    cont_cur_weight += w;
    cont_cur_volume += v;
    add_prop(CONT_I_IL_RZECZY, query_prop(CONT_I_IL_RZECZY) + a);

    if (!(env = environment()))
        return;

    /*
     * There are some containers that does not distribute internal light
     *
     * Transparent containers always do.
     * Containers that has its inventory attached on the outside do.
     * Closed containers dont if none of the above applies.
     */
    if (!query_prop(CONT_I_TRANSP) && !query_prop(CONT_I_ATTACH) && query_prop(CONT_I_CLOSED))
        l = 0;

    /* Rigid containers do not change in size
    */
    if (query_prop(CONT_I_RIGID))
        v = 0;

    if (l || w || v)
        env->update_internal(l, w * 100 / env->query_prop(CONT_I_REDUCE_WEIGHT),
            v * 100 / env->query_prop(CONT_I_REDUCE_VOLUME));
}

/*
 * Function name: update_light
 * Description:   Reevalueate the lightvalue of the container.
 */
public void
update_light(int recursive)
{
    int i;
    object *ob_list;

    ob_list = all_inventory(this_object());
    cont_cur_light = 0;

    if (!sizeof(ob_list))
        return;

    for (i = 0; i < sizeof(ob_list); i++)
    {
        if (recursive)
            ob_list[i]->update_light(recursive);
        cont_cur_light += ob_list[i]->query_prop(OBJ_I_LIGHT);
    }
}

/*
 * Function name: notify_change_prop
 * Description:   This function is called when a property in an object
 * 		  in the inventory has been changed.
 * Arguments:	  prop - The property that has been changed.
 *		  val  - The new value.
 *		  old  - The old value.
 */
public void
notify_change_prop(string prop, mixed val, mixed old)
{
    object pobj;
    int n, o, ld;

    if (old == val)
        return;
    if (member_array(prop, ({ CONT_I_ATTACH, CONT_I_TRANSP, CONT_I_CLOSED,
        CONT_I_LIGHT, OBJ_I_LIGHT, CONT_I_WEIGHT, OBJ_I_WEIGHT, CONT_I_VOLUME, OBJ_I_VOLUME })) < 0)
    {
        return;
    }

    pobj = previous_object();

    switch(prop)
    {
        case CONT_I_LIGHT:
        case OBJ_I_LIGHT:
            update_internal(val - old, 0, 0, 0);
            if (living(this_object()))
                this_object()->reveal_me(1);
            return;

        case CONT_I_WEIGHT:
        case OBJ_I_WEIGHT:
            update_internal(0, val - old, 0, 0);
            return;

        case CONT_I_VOLUME:
        case OBJ_I_VOLUME:
            update_internal(0, 0, val - old, 0);
            return;
    }

    n = pobj->query_internal_light();

    if (!n)
        return;

    ld = -1;  /* No change */

    /*
     * The rest is for light distribution. These are the rules:
     *
     * if ATTACH means always distribute.
     * if TRANSP means always distribute
     * if !CLOSED means distribute
     *
     * If a change in one prop causes a change in light distribution
     * from the container in this container, depends on the others.
     *
     * NOTE
     *   It is not _this_ container that has changed it is one of
     *   the containers within this container.
     */
    if (prop == CONT_I_ATTACH && !pobj->query_prop(CONT_I_TRANSP) && pobj->query_prop(CONT_I_CLOSED))
        ld = (val != 0);		/* 0 -> turn off, 1 -> turn on */

    else if (prop == CONT_I_TRANSP && !pobj->query_prop(CONT_I_ATTACH) && pobj->query_prop(CONT_I_CLOSED))
        ld = (val != 0);  		/* 0 -> turn off, 1 -> turn on */

    else if (prop == CONT_I_CLOSED && !pobj->query_prop(CONT_I_ATTACH) && !pobj->query_prop(CONT_I_TRANSP))
        ld = (val != 0);  		/* 0 -> turn off, 1 -> turn on */

    if (ld < 0)
        return;

    if (ld == 0)
        update_internal(n, 0, 0, 0);
    else
        update_internal(-n, 0, 0, 0);

    return;
}


/************************************************************
 *
 * Sublocation routines. These routines manages sublocations within
 * and around containers. All containers start out with the default
 * sublocation 'inside'. Sublocation are given as second argument
 * to move()
 *
 * Sublocations are given specific names when added to a container
 *
 * Each sublocation has a responsiple object. On this object the following
 * routines can be called:
 *
 *		show_subloc(string subloc, object on_obj, object for_obj)
 *			- Print a description of the sublocation 'subloc'
 *			  on object 'ob_obj' for object 'for_obj'.
 *
 */

/*
 * Function name: add_subloc
 * Description:   Add a named sublocation to this container.
 * Arguments:     sloc: Name of sub location
 *		  resp: Object responsible for the sublocation (or filename)
 *			If == 0, the default sublocation describer will be
 *			used.
 *		  ids:  one or a list of ids that can be used to identify
 *			the sublocation. This is only relevant for special
 *			sublocations that are not standard preposition-
 *			sublocs. These sublocs are most often related
 *			to a specific subitem and an id could be:
 *			'under nose', 'on leg', 'in pocket' etc.
 */
public varargs void
add_subloc(string sloc, mixed resp, mixed ids)
{
    int i;
    mixed old;

    cont_sublocs[sloc] = resp;

    if (stringp(ids))
        ids = ({ ids });

    for (i = 0; i < sizeof(ids); i++)
    {
        old = cont_subloc_ids[ids[i]];
        if (sizeof(old))
            cont_subloc_ids[ids[i]] = old + ({ sloc });
        else
            cont_subloc_ids[ids[i]] = ({ sloc });
    }
}

/*
 * Function name: query_subloc_obj
 * Description:   Get the object corresponding to a subloc string
 */
public object
query_subloc_obj(string sloc) { return cont_sublocs[sloc]; }

/*
 * Function name: query_sublocs
 * Description:   Get the current list of sublocations for this container
 */
public string *
query_sublocs()
{
    mixed ind = m_indexes(cont_sublocs);

    if(!pointerp(ind))
        return ({});
    else
        return m_indexes(cont_sublocs);
}

/*
 * Function name: remove_subloc
 * Description:   Remove a named sublocation of this container.
 * Arguments:     sloc: Name of sub location
 *
 */
public void
remove_subloc(string sloc)
{
    cont_sublocs = m_delete(cont_sublocs, sloc);

    map(all_inventory(this_object()), &fix_ob_subloc_when_remove(, sloc));
    cont_subloc_ids = map(cont_subloc_ids, &map_subloc_id(, sloc));
    cont_subloc_ids = filter(cont_subloc_ids, filter_subloc_id);
}

nomask string *
map_subloc_id(string *sloc, string rm_sloc)
{
    int pos;

    if ((pos = member_array(rm_sloc, sloc)) >= 0)
        sloc = exclude_array(sloc, pos, pos);

    return sloc;
}

nomask int
filter_subloc_id(string *sloc, string rm_sloc)
{
    return (sizeof(sloc) > 0);
}

nomask void
fix_ob_subloc_when_remove(object ob, string sloc)
{
    if ((string)ob->query_subloc() == sloc)
        ob->move(this_object(), 0); /* Default sublocation */

    return 0;
}

/*
 * Function name: subloc_id
 * Description:   Give the sublocation(s) if any for a specific id
 * Arguments:     id: name osublocation
 */
public string *
subloc_id(string id)
{
    return cont_subloc_ids[id];
}

/*
 * Function name: subloc_cont_access
 * Description:   Check if a sublocation can be accessed or not
 * Arguments:     sloc: Name of the sublocation
 *		  acs:	Access type as defined in /sys/subloc.h
 *		  for_obj: Living for which the access is to be checked
 * Returns:	  1 if accessible
 */
public string *
subloc_cont_access(string sloc, string acs, object for_obj)
{
    object slob;

    if (!objectp(for_obj))
        for_obj = previous_object();

    slob = cont_sublocs[sloc];

    if (!objectp(slob))
        return SUBL_CODE->subloc_access(sloc, this_object(), acs, for_obj);
    else
        return slob->subloc_access(sloc, this_object(), acs, for_obj);
}

/*
 * Function name: subinventory
 * Description:   Give the subinventory for a specific sublocation
 * Arguments:     sloc: sublocation
 */
public object *
subinventory(mixed sloc)
{
    if (objectp(cont_linkroom))
        return filter(all_inventory(cont_linkroom), &subloc_filter(, sloc));

    return filter(all_inventory(), &subloc_filter(, sloc));
}

/* jeremian's changes */

public varargs string
subloc_items(mixed loc_op = 0, int przyp = PL_MIA)
{
    string loc_str = "";
    int loc_i;
    object *loc_tab;

    loc_tab = subinventory(loc_op);

    if(sizeof(loc_tab)>0)
        loc_str += COMPOSITE_DEAD(loc_tab, przyp);

    return loc_str;
}

/*
 * Nazwa: policz_rzeczy_nazwa
 * Opis: Funkcja zwraca ilo�� rzeczy na danej lokacji nazywaj�cych si� tak jak podany argument.
 * Argumenty: nazwa - nazwa, kt�rej szukamy
 *            sloc - nazwa sublokacji
 * Zwraca: Opis sublokacji.
 */


public int
policz_rzeczy_nazwa(string nazwa, mixed sloc = 0)
{
    int i, il, ile;
    object *obarr;

    obarr = subinventory(sloc);

    ile = 0;
    il = sizeof(obarr);
    for (i = 0; i < il; ++i)
        if (nazwa ~= obarr[i]->short())
            ++ile;

    return ile;
}


/*
 * Nazwa: opis_sublokacji
 * Opis: Zwraca opis sublokacji z jej rzeczami lub alternatywny tekst.
 * Argumenty: prefiks - pocz�tek opisu
 *            sloc - nazwa sublokacji
 *            postfiks - ko�c�wka opisu
 *            alt - alternatywny tekst
 *            przyp - przypadek, w jakim maj� by� wy�wietlane rzeczy
 *            s1 - wartosc, gdy num = 1,
 *            s2 - wartosc, gdy num = 2,3,4,42... itp.
 *            s3 - wartosc, gdy num = 5,10,14... itp.
 * Zwraca: Opis sublokacji.
 */

public varargs string
opis_sublokacji(string prefiks="", mixed sloc=0, string postfiks="", string alt="", int przyp = PL_MIA,
    mixed s1 = 0, mixed s2 = 0, mixed s3 = 0)
{
    object *obarr;
//write("opis_sublokacji(string prefiks="+prefiks+", mixed sloc="+sloc+", string postfiks="+postfiks+", string alt="+alt+", int przyp = "+przyp+","+
//    "mixed s1 = "+s1+", mixed s2 = "+s2+", mixed s3 = "+s3+") \n\n ");
    if (stringp(sloc) && sloc == "")
        sloc = 0;

    if ((sloc == 0) ? query_prop(CONT_I_CLOSED) : query_subloc_prop(sloc, CONT_I_CLOSED))
        return alt;

    if (sizeof((obarr = this_object()->subinventory(sloc))) > 0)
        return prefiks + ((stringp(s1) && stringp(s2) && stringp(s3)) ?
            ilosc(policz_rzeczy_nazwa(obarr[0]->short(), sloc), s1, s2, s3) : "") +
            this_object()->subloc_items(sloc, przyp) + postfiks;

    return alt;
}

/*
 * Nazwa: jest_rzecz_w_sublokacji
 * Opis: Sprawdza, czy jaka� rzecz jest w danej sublokacji
 * Argumenty: sloc - nazwa sublokacji
 *            rzecz - nazwa rzeczy
 * Zwraca: 1 - przedmiot o danej nazwie jest w sublokacji
 *         0 - nie ma przedmiotu o danej nazwie
 */

public varargs int
jest_rzecz_w_sublokacji(mixed sloc, string rzecz, int ile = 1)
{
    int i;
    int licznik = 0;
    object *inwentarz;

    if (!stringp(rzecz))
        return 0;
//write("jest_rzecz_w_sublokacji(mixed sloc, string rzecz, int ile = 1) : "+sloc+" "+rzecz+" "+ile+"\n");
    inwentarz = this_object()->subinventory(sloc);
    
    for (i=0;i<sizeof(inwentarz);i++)
    {
        if (rzecz ~= inwentarz[i]->short())
        {
            licznik++;

            if (licznik >= ile)
                    return 1;
        }
    }

    return 0;
}

/*
 * Nazwa: opis_sublokacji_od_rzeczy
 * Opis: Zwraca opis, je�eli rzecz znajduje si� w sublokacji sloc, lub alternatywny tekst
 *       w przeciwnym przypadku.
 * Argumenty: rzecz - przedmiot, kt�rego obecno�� jest sprawdzana
 *            sloc - nazwa sublokacji
 *            opis - opis wy�wietlany w przypadku, gdy test si� powi�d�
 *            alt - alternatywny tekst
 * Zwraca: Opis.
 */

public varargs string
opis_sublokacji_od_rzeczy(string rzecz, mixed sloc=0, string opis="", string alt="")
{
    if (stringp(sloc) && sloc == "")
        sloc = 0;

    if ((sloc == 0) ? query_prop(CONT_I_CLOSED) : query_subloc_prop(sloc, CONT_I_CLOSED))
        return alt;

    if (jest_rzecz_w_sublokacji(sloc, rzecz))
        return opis;

    return alt;
}

/**
 * Zwraca opis w zale�no�ci od tego, czy sublokacja jest zamkni�ta.
 *
 * @param sloc testowana sublokacja
 * @param opis opis, kt�ry ma by� zwr�cony
 *
 * @return opis, je�eli sublokacja jest zamkni�ta
 */
public string
if_subloc_closed(mixed sloc, string opis)
{
    if ((sloc == 0) ? query_prop(CONT_I_CLOSED) : query_subloc_prop(sloc, CONT_I_CLOSED))
        return opis;

    return "";
}

/**
 * Zwraca opis w zale�no�ci od tego, czy sublokacja nie jest zamkni�ta.
 *
 * @param sloc testowana sublokacja
 * @param opis opis, kt�ry ma by� zwr�cony
 *
 * @return opis, je�eli sublokacja nie jest zamkni�ta
 */
public string
if_subloc_not_closed(mixed sloc, string opis)
{
    if ((sloc == 0) ? (!query_prop(CONT_I_CLOSED)): (!query_subloc_prop(sloc, CONT_I_CLOSED)))
        return opis;

    return "";
}

/**
 * Funkcja filtruj�ca obiekty wed�ug podanej sublokacji.
 *
 * @param ob    - rozpatrywany obiekt
 * @param sloc  - nazwa sublokacji
 * @param przyp - przypadek nazwy sublokacji
 *
 * @return <ul>
 *           <li> <b>1</b> obiekt jest w podanej sublokacji
 *           <li> <b>0</b> obiekt <b>nie</b> jest w podanej sublokacji
 *         </ul>
 */
nomask varargs int
subloc_filter(object ob, mixed sloc, int przyp = 0)
{
    mixed sublok = ob->query_subloc();

    if (pointerp(sloc))
    {
        if (pointerp(sublok))
            return equiv_array(sublok, sloc);

        return sublok ~= sloc[przyp];
    }
    else
    {
        if (pointerp(sublok))
            return (sublok[przyp] ~= sloc);
            
        //(0 ~= "�ciana") zwraca 1 :O co� jest nie teges, chyba z ~=... Wi�c troch� to przerabiam (vera):
        if((intp(sublok) && stringp(sloc)) || stringp(sublok) && intp(sloc))
            return 0;
        return (sublok ~= sloc);
    }
    return 0;
}

/* --- */

/*
 * Function name: show_sublocs
 * Description:   Give a description of each sublocation. This is a default
 *		  routine merely calling show_cont_subloc in this object for
 *		  each sublocation.
 * Arguments:	  for_obj: The object for which description is given
 *		  slocs:   Identifiers for sublocations
 */
public varargs string
show_sublocs(object for_obj, mixed *slocs)
{
    int il;
    string str;
    mixed data;

//    write("J0\n");

    if (!objectp(for_obj))
        for_obj = previous_object();

    if (!sizeof(slocs))
        slocs = m_indexes(cont_sublocs) + ({ 0 });
    else
        slocs = slocs & ( m_indexes(cont_sublocs) + ({ 0 }) );

    for (str = "", il = 0; il < sizeof(slocs); il++)
    {
        data = this_object()->show_cont_subloc(slocs[il], for_obj);

        if (stringp(data))
            str += data;
        else if (data == 0 && slocs[il] != 0)
            cont_sublocs = m_delete(cont_sublocs, slocs[il]);
    }

    return str;
}

/*
 * Function name: show_cont_subloc
 * Description:   Give a description of one sublocation.
 * Arguments:	  sloc : The name of the sublocation
 *		  for_obj: For whom to show the sublocation
 * Returns:	  string or 0 for invalid or 1 for temporary bad
 */
public varargs mixed
show_cont_subloc(string sloc, object for_obj)
{
    int il;
    string data;
    mixed ob;

    ob = cont_sublocs[sloc];

    /* jeremian's changes */

    if (objectp(ob))
        data = ob->show_subloc(sloc, this_object(), for_obj);
    else if (stringp(ob))
    {
        catch(ob->teleledningsanka());
        ob = find_object(ob);
        if (ob)
            data = ob->show_subloc(sloc, this_object(), for_obj);
        else
            return 1;
    }
    else
    {
        data = SUBL_CODE->show_subloc(sloc, this_object(), for_obj);
        if (!stringp(data) && data == 0)
            return 0;
    }

    /* --- */

    if (!stringp(data))
        return 1;

    return data;
}

/*
 * Function name: stat_object
 * Description:   This function is called when a wizard wants to get more
 *                information about an object.
 * Returns:       str - The string to write..
 */
string
stat_object()
{
    string str;
    int tmp;

    str = ::stat_object();

    if (tmp = query_prop(CONT_I_WEIGHT))
        str += "Waga pustego pojemnika: " + tmp + "\n";
    if (tmp = query_prop(CONT_I_MAX_WEIGHT))
        str += "Maks. waga pojemnika: " + tmp + "\n";
    if (tmp = query_prop(CONT_I_VOLUME))
        str += "Obj^eto^s^c pustego pojemnika: " + tmp + "\n";
    if (tmp = query_prop(CONT_I_MAX_VOLUME))
        str += "Maks. pojemnosc pojemnika: " + tmp + "\n";
    if (tmp = query_prop(CONT_I_IL_RZECZY))
        str += "Ilo�� rzeczy w pojemniku: " + tmp + "\n";
    if (tmp = query_prop(CONT_I_MAX_RZECZY))
        str += "Maks. ilo�� rzeczy w pojemniku: " + tmp + "\n";

    return str;
}

public string
query_container_auto_load()
{
    string toReturn = " " + query_prop(CONT_I_WEIGHT) + "," + query_prop(CONT_I_VOLUME) + "," +
        query_prop(CONT_I_MAX_WEIGHT) + "," + query_prop(CONT_I_MAX_VOLUME) + " @(@ ";
    string subload;
    int first;

    foreach(mixed sloc : ((pointerp(query_sublocs()) ? query_sublocs() + ({0}) : ({0}))))
    {
        toReturn += " @[@ ";
        if (stringp(sloc))
            toReturn += sloc;
        else if (pointerp(sloc))
        {
            toReturn += "[";
            first = 1;
            foreach (string elem : sloc)
            {
                if (!first)
                    toReturn += ",";

                toReturn += elem;
                first = 0;
            }
            toReturn += "]";
        }
        toReturn += " @:@ ";
        first = 1;
        foreach (object ob : subinventory(sloc))
        {
            if (stringp((subload = ob->query_auto_load())))
            {
                if (!first)
                    toReturn += " @,@ ";

                toReturn += subload;
                first = 0;
            }
        }
        toReturn += " @]@ ";
    }

    return toReturn + " @)@ ";
}

private void
init_container_subarg(string arg)
{
    object ob;
    mixed cursloc;
    string curarg, err;
    string file, argument;
    string curstring;
//    write("SUBARG: " + arg + "\n");
    sscanf(arg, "%s@:@%s", cursloc, curarg);
    cursloc = cursloc[1..-2];
    curarg = curarg[1..-2];

    if (cursloc == "")
        cursloc = 0;

    if (stringp(cursloc) && (sscanf(cursloc, "[%s]", curstring) == 1))
    {
        cursloc = ({});
        foreach (string elem : explode(curstring, ","))
            cursloc += ({elem});
    }

    if (curarg == "")
        return;

    curstring = "";
    foreach (string substring : explode(curarg, "@,@"))
    {
        curstring += substring;
        // nie jeste�my w �adnym podkontenerze
        if (sizeof(explode(curstring, "@(@")) == sizeof(explode(curstring, "@)@")))
        {
            if (curstring[0..0] == " ")
                curstring = curstring[1..];

            if (sscanf(curstring, "%s:%s", file, argument) != 2)
            {
                if (curstring[-1..-1] == " ")
                    file = curstring[..-2];
                else
                    file = curstring;
                argument = 0;
            }

            if (!stringp(file))
            {
                write("Auto load string corrupt: " + curstring + "\n");
                curstring = "";
                continue;
            }
            setuid();
            seteuid(getuid());

            if ((err = LOAD_ERR(file)))
            {
                write("Error in loading file: [" + file + "] (" + err + ")\n");
                curstring = "";
                continue;
            }

            if (!objectp(find_object(file)))
            {
                write("Cannot find cloned object: " + file + "\n");
                curstring = "";
                continue;
            }

            catch(ob = clone_object(file));

            if (stringp(argument))
            {
                if ((err = catch(ob->init_arg(argument))))
                {
                    write("CONTAINER: " + file_name(TO) + "\n OB: " + file_name(ob) + "\n  Error during init_arg: " + err + "\n");
                    catch(ob->remove_object());
                    curstring = "";
                    continue;
                }
            }
            catch(ob->move(this_object(), cursloc, 1));
            curstring = "";
        }
        else
            curstring += "@,@";
    }
}

public string
init_container_arg(string arg)
{
    int ocounter, ccounter;
    int i1, i2, i3, i4;

    if (arg == 0)
        return 0;

    if (sscanf(arg, "%d,%d,%d,%d", i1, i2, i3, i4) == 4)
    {
        add_prop(CONT_I_WEIGHT, i1);
        add_prop(CONT_I_VOLUME, i2);
        add_prop(CONT_I_MAX_WEIGHT, i3);
        add_prop(CONT_I_MAX_VOLUME, i4);
    }

    string newarg = implode(explode(implode(explode(arg, "@(@")[1..], "@(@"), "@)@")[..-2], "@)@");
    string curstring = "";
    ocounter = ccounter = 0;
    if(!query_room())
    {
        foreach (string substring : explode(newarg, "@[@")[1..])
        {
            ocounter += 1;
            ccounter += sizeof(explode(substring, "@]@")[1..]);
            if (ocounter == ccounter)
            { // Znale�li�my zamykaj�cy nawias
                curstring += implode(explode(substring, "@]@")[..-2], "@]@");
                init_container_subarg(curstring);
                curstring = "";
                ocounter = ccounter = 0;
            }
            else
                curstring += substring + "@[@";
        }
    }
    return explode(arg, "@)@")[-1];
}

public string
query_auto_load()
{
    return ::query_auto_load() + query_container_auto_load();
}

public string
init_arg(string arg)
{
    if (arg == 0)
    {
        enable_clone_objects = 1;
        reset();
        enable_clone_objects = 0;
    }
    return init_container_arg(::init_arg(arg));
}

/**
 * Funkcja identyfikuj�ca obiekt jako pojemnik
 *
 * @return zawsze 1
 */
public int is_container() { return 1; }