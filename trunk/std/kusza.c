/* Std kuszy poczynione przez Aldeima */

inherit "/d/Aretuza/aldeim/strzeleckie/strzelecka";
#include <stdproperties.h>
#include <macros.h>

int jakosc;

public void
create_kusza()
{
}

int
query_jakosc()
{
  return jakosc;
}

void
ustaw_jakosc(int x)
{
  jakosc = x;
  if (x < 1) ustaw_jakosc(1);
  if (x > 10) ustaw_jakosc(10);

  /* Zakres cech kuszy:
     celnosc: 10 - 100
     szybkosc: 1 - 4
     sila:    50 - 100
   */

  set_min_sila(jakosc*10);
  set_celnosc(jakosc*10);
  set_szybkosc(1 + (jakosc/3));
  set_sila(50 + jakosc*5);
}

nomask void
create_strzelecka()
{
  ustaw_nazwe("kusza");
  set_long("Najzwyczajniejsza w ^swiecie kusza.\n");
  set_zasieg(1);
  set_pocisk("be^lt");
  create_kusza();
}

public void
przygotowanie(object kto, object bron, object strzala)
{
  set_this_player(kto);
  write("Umieszczasz " + strzala->query_short(PL_BIE) + " w "+
    "mechanizmie " + bron->query_short(PL_DOP) + ". Nast^epnie "+
    "k^ladziesz " + bron->query_zaimek(PL_BIE,0) + " jednym "+
    "ko^ncem na ziemi i zaczynasz za pomoc^a masywnej korby "+
    "nakr^eca^c urz^adzenie.\n");
  saybb(QCIMIE(this_player(), PL_MIA) + " umieszcza " +
    strzala->query_short(PL_BIE) + " w "+
    "mechanizmie " + bron->query_short(PL_DOP) + ". Nast^epnie "+
    "k^ladzie " + bron->query_zaimek(PL_BIE,0) + " jednym "+
    "ko^ncem na ziemi i zaczyna za pomoc^a masywnej korby "+
    "nakr^eca^c urz^adzenie.\n");
}
