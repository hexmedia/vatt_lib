/**
 * \file /std/torch.c
 *
 * fajna pochodnia, b�d�ca r�wnocze�nie maczug� kt�r� mo�na walczy�.
 *
 */

#pragma save_binary
#pragma strict_types

inherit "/std/weapon.c";
inherit "/lib/light.c";
inherit "/lib/psuj.c";

#include <object_types.h>
#include <stdproperties.h>
#include <wa_types.h>

/**
 * Funkcja tworz�ca obiekt.
 */
void
create_torch()
{
    //�eby tej broni stra� nam nie zapierdala jak biegamy z ni� po mie�cie.
    add_prop(WEAPON_I_LEGAL_WIELD, 1);

    ustaw_nazwe("pochodnia");
}

/**
 * Wywo�ujemy kreator inheritowanego standardu.
 */
nomask void
create_weapon()
{
    set_long("Kr^otki drewniany palik zosta^l obrobiony i cz^e^sciowo "+
        "oheblowany, w celu pozbawienia go co wi^ekszych drzazg, kt^ore "+
        "mog^lyby porani^c u^zytkownika owej pochodni. Na drugim ko^ncu "+
        "kij zosta^l owini^ety tkanin^a nasi^akni^et^a ^swierkow^a "+
        "^zywic^a, by ^luczywo mog^lo p^lon^a^c d^lugim i jasnym "+
        "^swiat^lem, jak^ze przydatnym w r^o^znych jaskiniach i "+
        "grotach, czy te^z poprostu w mie^scie, w ciemnym zau^lku.\n");

    add_prop(OBJ_I_LIGHT,     0);
    add_prop(OBJ_I_WEIGHT,  700);
    add_prop(OBJ_I_VOLUME, 1000);

    set_pen(1);
    set_hit(2); //kto m�dry ustawi� na 20? eh..

    set_wt(W_MACZUGA);
    set_dt(W_BLUDGEON);

    //koniec expienia z nigdy nie niszczej�cymi pochodniami
    set_likely_dull(30+random(100));
    set_likely_break(20+random(100));

    set_hands(W_ANYH);

    //Tutaj ustawienia z /lib/light.c
    set_time(1700 +random(100));
    set_strength(1);
    set_value(13);

    set_type(O_POCHODNIE);
    
    //pochodnie te� si� psuja, wi�c ustawiamy zmienne do /lib/psuj:
    jak_szybko_sie_niszczy(20);
    niszczeje(); //u�ywamy obiektu, niszczy si� wi�c.

    config_light();

    create_torch();
}

public void
reset_torch()
{
}

public nomask void
reset_weapon()
{
    reset_torch();
}

/**
 * W tej funkcji dodajemy podstawowe komendy dzia�aj�ce w pochodni.
 */
public void
init()
{
    ::init();
    init_light();
}

public string
query_auto_load()
{
    return ::query_auto_load() + query_light_auto_load();
}

public string
init_arg(string arg)
{
    return init_light_arg(::init_arg(arg));
}

public varargs int
unwield_me(int cicho=0)
{
    unwield_light();
    return ::unwield_me(cicho);
}

public varargs int
move(mixed dest, mixed subloc, int force = 0, int test_only = 0)
{
    move_light();
    return ::move(dest, subloc, force, test_only);
}