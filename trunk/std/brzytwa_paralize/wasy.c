#pragma strict_types
#include <macros.h>

inherit "/std/paralyze";

void
create_paralyze()
{
    set_name("strzyze_sobie_wasy");
    set_stop_verb("przesta�");
    set_stop_message("Przestajesz strzyc sobie w�sy.\n");
    set_fail_message("Jeste� teraz zaj�t"+TP->koncowka("y","a")+
                     " strzy�eniem si�. Musisz wpisa� "+
                "'przesta�' by m�c zrobi� to co chcesz.\n");
    set_finish_object(this_object());
    set_finish_fun("koniec_strzyzenia_wasow");
    set_remove_time(10);
    setuid();
    seteuid(getuid());
}

void
koniec_strzyzenia_wasow(object player)
{
    player->catch_msg("Ko^nczysz strzyc sobie w�sy.\n");
    player->add_old_fatigue(-5);
    player->set_dlugosc_wasow(0.0);
    player->przym_od_zarostu();
    saybb(QCIMIE(player,PL_MIA)+" ko^nczy strzyc sobie w�sy.\n");
    remove_object();
    return;
}
