/**
 * \file /std/proca.c
 *
 * Standard procy
 *
 * @author Krun
 * @date Stycze� 2008
 */

inherit "/std/bron_strzelecka.c";

#include <pl.h>
#include <macros.h>
#include <wa_types.h>

/**
 * Redefiniujemy t� funkcj� aby od razu poustawia� wszystkie warto�ci,
 * je�li nie s� jeszcze ustawione kiedy j� definiujemy.
 *
 * @param j jako�� broni
 */
void
ustaw_jakosc(int j)
{
    ustaw_celnosc(j);          //Celno�� broni
    ustaw_sile_razenia(j);     //Si�a ra�enia broni

    ::ustaw_jakosc(j);
}

void
create_proca()
{
}

void
create_bron_strzelecka()
{
    ustaw_nazwe(({"proca", "procy", "procy", "proce", "proc�", "procy"}),
        ({"proce", "proc", "proc�", "proce", "procami", "procach"}), PL_ZENSKI);

    set_long("Najzwyczajniejsza w �wiecie proca.\n");

    ustaw_potrzebna_sile(20);       //Si�a potrzebna do u�ycia broni
    ustaw_potrzebna_zrecznosc(40);  //Zr�czno�� potrzebna do u�ycia broni
    ustaw_zasieg(1);                //Standardowo
    ustaw_typ(W_L_PROCA);           //Typ broni.

    set_hands(W_ANYH);

    dodaj_komende_ladowania("w�� %p:" + PL_BIE + " do kieszeni / ko�yski %b:" + PL_DOP, "w�o�y�", "wk�adasz", "wk�ada");
    dodaj_komende_naciagania("zakr�� %b:" + PL_NAR, "zakr�ci�", "zaczynasz kr�ci�", "zaczyna kr�ci�");
    dodaj_komende_strzelania("strzel", "strzeli�", "strzelasz", "strzela", PL_DOP);
    dodaj_komende_strzelania("zwolnij chwyt / uchwyt / sznur %b:" + PL_DOP, "zwolni�", "zwalniasz", "zwalnia");

    ustaw_opis_napietej_broni("W tej chwili %k:" + PL_MIA + " kr�ci ni� nad g�ow�.\n");
    ustaw_opis_zaladowanego_pocisku("W jej kieszeni znajduje si� %p:" + PL_BIE + ".\n");

    create_proca();
}

/**
 * Niestety w procy jest g�upi opis kr�cenia i trzeba samemu doda�.
 */
public string long(string str, object fob)
{
    if(!str)
    {
        return ::long(0, fob) + (fob == ENV(TO) ? "Kr�cisz " +
            TO->koncowka("nim", "ni�", "nim", "nimi", "nimi") + " wok� g�owy.\n" :
            ENV(TO)->short(fob, PL_MIA) + " kr�ci " + TO->koncowka("nim", "ni�", "nim", "nimi", "nimi") +
            " wok� swojej g�owy.\n");
    }
    ::long(str, fob);
}

static void bs_hook_koniec_naciagania(object pl)
{
    pl->catch_msg("Jeste� tak zm�czony, �e nie jeste� ju� w stanie dalej kr�ci� " + TO->short(pl, PL_DOP) + ".\n");
    tell_roombb(ENV(pl), QCIMIE(pl, PL_MIA) + " przestaje kr�ci� " + QSHORT(TO, PL_BIE) + ".\n", ({pl}));
}

static void bs_hook_zaprzestanie_naciagania(object pl)
{
    pl->catch_msg("Przestajesz kr�ci� " + TO->short(pl, PL_DOP) + ".\n");
    tell_roombb(ENV(pl), QCIMIE(pl, PL_MIA) + " przestaje kr�ci� " +
        QSHORT(TO, PL_BIE) + ".\n", ({pl}));
}