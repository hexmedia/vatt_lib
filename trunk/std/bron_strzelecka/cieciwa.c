/**
 * \file /std/bronie_strzeleckie/cieciwa.c
 *
 * C� wart �uk czy kusza bez ci�ciwy?? Nic!
 *
 * @author Krun
 * @date Stycze� 2008
 */

#pragma strict_types

inherit "/std/object.c";

#include <pl.h>
#include <stdproperties.h>

int         jakosc;         /* jako�� ci�ciwy */

float       stan;           /* Procentowy stan ci�ciwy 0-100 */

string     *opisy_stanu;    /* opisy stanu */

object      bron;           /* Bro� w jakiej jest ci�ciwa */

/**
 * Kreator ci�ciwy.
 */
public void create_cieciwa()
{
}

/**
 * Kreator obiektu.
 */
public nomask void create_object()
{
    ustaw_nazwe(({"ci�ciwa", "ci�ciwy", "ci�ciwie", "ci�ciwe", "ci�ciw�", "ci�ciwe"}),
        ({"ci�ciwy", "ci�ciw", "ci�ciwom", "ci�ciwy", "ci�ciwami", "ci�ciwach"}), PL_ZENSKI);

    set_long("Zwyk�a ci�ciwa.\n");

    create_cieciwa();
}

/**
 * Dzi�ki tej funkcji mo�emy okre�li� jako�� ci�ciwy.
 *
 * @param j jako��
 */
public void ustaw_jakosc(int j) { jakosc = max(1, min(100, j)); }

/**
 * @return jako�� ci�ciwy
 */
public int query_jakosc() { return jakosc; }

/**
 * Ustawiamy stan ci�ciwy.
 *
 * @param s stan ci�ciwy
 */
public void ustaw_stan(float s) { stan = min(100.0, max(0.0, stan)); }

/**
 * @return stan ci�ciwy
 */
public float query_stan() { return stan; }

/**
 * Ustawiamy poziomy stanu.
 * Od idealnego do najgorszego.
 *
 * @param stany
 */
public void ustaw_stany(string *s) { stan = 0; opisy_stanu = s; }

/**
 * @return dopuszczalne stany ci�ciwy
 */
public string * query_opisy_stanu() { return opisy_stanu; }

/**
 * @return aktualny poziom stanu
 */
public int query_act_stan() { return ftoi(stan / (100.0 / itof(sizeof(opisy_stanu)))); }

/**
 * Funkcja wywo�wyana na ci�ciwie kiedy ta jest wk�adana do broni.
 */
public void wkladam_do_broni_strzeleckiej(object b)
{
    bron = b;
    set_no_show_composite(1);
    add_prop(OBJ_M_NO_DROP, "Nie mo�esz teraz tego od�o�y�.\n");
    add_prop(OBJ_M_NO_THROW, "Nie mo�esz tym teraz rzuci�.\n");
    add_prop(OBJ_M_NO_SELL, "Nie mo�esz tego teraz sprzeda�.\n");
    add_prop(OBJ_M_NO_STEAL, "Nie mo�esz tego ukra��.\n");
    add_prop(OBJ_M_NO_BUY, "Nie mo�esz tego kupi�.\n");
    add_prop(OBJ_M_NO_GIVE, "Nie mo�esz tego nikomu da�.\n");
    add_prop(OBJ_M_NO_TELEPORT, "Nie mo�esz tego przenie��.\n");
}

/**
 * Funkcje wywo�ywana na ci�ciw� kiedy ta jest wyjmowana z broni.
 */
public void wyciagamy_z_broni_strzeleckiej()
{
    bron = 0;
    set_no_show_composite(0);
    remove_prop(OBJ_M_NO_DROP);
    remove_prop(OBJ_M_NO_THROW);
    remove_prop(OBJ_M_NO_SELL);
    remove_prop(OBJ_M_NO_STEAL);
    remove_prop(OBJ_M_NO_BUY);
    remove_prop(OBJ_M_NO_GIVE);
    remove_prop(OBJ_M_NO_TELEPORT);
}

/**
 * @return zwraca w jakiej broni jest ci�ciwa
 */
public object query_bron() { return bron; }

/**
 * Funkcja identyfikuj�ca obiekt jako ci�ciwe.
 *
 * @return 1 - zawsze
 */
public int is_cieciwa() { return 1; }

public string query_auto_load()
{
    return ::query_auto_load() + "#CIEC#" + !!query_bron() + "#CIEC#";
}

public string init_arg(string arg)
{
    string pre, post, is_in;

    if(sscanf(arg, "%s#CIEC#%d#CIEC#%s", pre, is_in, post) != 3)
        return arg;

    if(!is_in)
        return arg;
    else
        remove_object(); //Usuwamy coby si� nie powiela�o
}

public string init_cieciwa(string arg)
{
    return ::init_arg(arg);
}