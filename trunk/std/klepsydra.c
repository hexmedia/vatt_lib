/**
 * \file /std/klepsydra.c
 *
 * Standard wszelkiego rodzaju kelpsydr.
 */

inherit "/std/holdable_object.c";

#include <pl.h>
#include <macros.h>
#include <cmdparse.h>
#include <stdproperties.h>

int         czas,
            time_left,
            alarm_left;

public void
create_klepsydra()
{
}

public nomask void
create_holdable_object()
{
    ustaw_nazwe(({"klepsydra", "klepsydry", "klepsydrze", "klepsydre", "klepsydra", "klepsydrze"}),
        ({"klepsydry", "klepsydr", "klepsydrami", "klepsydry", "klepsydrami", "klepsydrami"}),
        PL_ZENSKI);

    time_left = 0;

    create_klepsydra();
}

public void
reset_klepsydra()
{
}

public nomask void
reset_holdable_object()
{
    reset_klepsydra();
}

public void
check_time_left()
{
    int *alarmy;

    alarmy = get_alarm(alarm_left);

    if(!alarmy || !sizeof(alarmy) || sizeof(alarmy) < 3)
    {
        time_left = 0;
        return;
    }

    time_left = time() - ftoi(alarmy[2]);
}

/**
 * Ustawiamy czas jaki klepsydra b�dzie domierza�... Bedzie to robi� z jak��
 * dok�adno�ci�.
 *
 * @param t ile czasu odmierza�.
 */
public void
set_time(int t)
{
    czas = t;
}

/**
 * @return jaki czas bedzie si� przesypywa� piasek.
 */
public int
query_time()
{
    return czas;
}

/**
 * @return czas kt�ry pozosta� do przesypania si� piasku.
 */
public int
query_time_left()
{
    check_time_left();
    return time_left;
}

public void
przesyp_sie()
{
    if(living(ENV(TO)))
        ENV(TO)->catch_msg("Piasek w " + TO->short(ENV(TO), PL_MIE) + " przesypa� si� do dolnej cz�ci.\n");
    else
        tell_room(ENV(TO), "Piasek w " + QSHORT(TO, PL_MIE) + " przesypa� si� do dolnej cz�ci.\n");
}

public void
wlacz_alarm()
{
    float t;
    mixed *cur_alarm;

    if(query_time_left() == 0)
        t = itof(czas);
    else
    {
        cur_alarm = get_alarm(alarm_left);
        t = cur_alarm[2];
        remove_alarm(alarm_left);
    }

    alarm_left = set_alarm(t, 0.0, &przesyp_sie());
}

public int
przewroc(string str)
{
    NF("Co chcesz odwr�ci�?\n");

    if(!str)
        return 0;

    object *klepsydra;

    if(!parse_command(str, AI(TP) + AIE(TP), "%i:" + PL_BIE, klepsydra))
        return 0;

    klepsydra = NORMAL_ACCESS(klepsydra, 0, 0);

    if(!sizeof(klepsydra))
        return 0;
    if(sizeof(klepsydra) > 1)
        return NF("Nie mo�esz odwr�ci� kilku rzeczy na raz.\n");

    if(klepsydra[0] != TO)
        return 0;
    write("Odwracasz " + TO->short(TP, PL_BIE) + ".\n");
    saybb(QCIMIE(TP, PL_MIA) + " odwraca " + QSHORT(TO, PL_BIE) + ".\n");

    if(query_time_left() == 0)
        write("Piasek w " + TO->short(TP, PL_MIE) + " zaczyna si� przesypywa�.\n");
    else
        write("Piasek w " + TO->short(TP, PL_MIE) + " przesypuje si� teraz w drug� stron�.\n");

    wlacz_alarm();

    return 1;
}

void
init()
{
    ::init();

    add_action(przewroc, "przwr��");
    add_action(przewroc, "odwr��");
}