/**
 * Standard Efekt�w
 *
 * @author Krun
 * @date co� oko�o wakacji 2007
 */

inherit "/std/object.c";

#include <macros.h>
#include <stdproperties.h>

int         all_time,
            max_time,
            max_end_time,
            event_time,
            event_diff,
            event_alarm;
mixed       events;
object      efekt_ob;
string      eff_subloc,
            eff_subloc_fo_tp,
            eff_subloc_fo_oth;

#define DBG(x) find_player("krun")->catch_msg("EFEKT DEBUGER: " + x + "\n")

void
create_effect()
{
}

nomask void
create_object()
{
    create_effect();
    ustaw_nazwe(({"efekt", "efektu", "efektowi", "efekt", "efektem", "efekcie"}),
        ({"efekty", "efekt�w", "efektom", "efekty", "efektami", "efektach"}),
        PL_NIJAKI_NOS);
    set_no_show();
}

void
reset_effect()
{
}

nomask void
reset_object()
{
    reset_effect();
}

/**
 * Dodaje event pokazywany co czas okre�lony w set_event_time
 * @param fop komunikat dla gracza kt�ry ma na sobie efekt - mo�liwe u�ycie vbfc i tag�w
 * @param foo komunikat dla graczy widz�cych gracza
 * @param prio priorytet eventu, im wy�szy tym cz�ciej event b�dzie wybierany spo�r�d innych
 * @param fun funkcja wywo�ywana na graczu
 */
varargs void
add_event(string fop, string foo, int prio, string fun, ...)
{
    if(pointerp(argv[0]))
        argv = argv[0];

    if(pointerp(events))
        events += ({ ({fop, foo, prio, fun, argv}) });
    else
        events = ({ ({fop, foo, prio, fun, argv}) });
}

/**
 * @return Zwraca tablice z eventami.
 */
string
query_events()
{
    return events;
}

/**
 * Ustawia co jaki czas pokazywany b�dzie event.
 * @param czas Czas co jaki b�dzie pokazywany event.
 * @param wachanie O tyle mog� r�ni� si� r�nice czasu pomi�dzy kolejnymi efektami.
 */
void
set_event_time(int czas, int wachanie)
{
    event_time = max(1, czas - wachanie/2);
    event_diff = wachanie;
}

int
query_event_time()
{
    return event_time + random(event_diff);
}

//Ju� nie wiem po co ta funkcja:P
int
query_real_event_time()
{
    return event_time + event_diff/2;
}

int
query_time_diff()
{
    return event_diff;
}

void
set_effect_time(int time)
{
    max_end_time = time;
}

int
query_effect_time()
{
    return max_time;
}

void
ustaw_subloc(string s, string fotp, string footh)
{
    eff_subloc = s;
    eff_subloc_fo_tp = fotp;
    eff_subloc_fo_oth = footh;
}

string
query_subloc()
{
    return eff_subloc;
}

int start_effect(mixed ob);
int stop_effect(mixed ob);

void
enter_env(object from, object to)
{
    if(from != to)
    {
        if(living(from))
            if(member_array(TO, from->query_prop(PLAYER_AO_HAS_EFFECTS)) >= 0)
                stop_effect(from);
        if(living(to))
            if(member_array(TO, to->query_prop(PLAYER_AO_HAS_EFFECTS)) == -1)
                start_effect(to);
    }
}

int
losuj_po_priorytetach(int *prio)
{
    int mx;

    foreach(int x : prio)
        if(x > mx)
            mx = x;

    int r = random(mx);
    int ret;
    int *zgadz = ({});

    for(int i=0; i < sizeof(prio); i++)
    {
        if(prio[i] == r)
            zgadz += ({i});
        if(!zgadz && (ABS(prio[i] - r) < ABS(prio[ret] - r)))
            ret = i;
    }

    if(zgadz)
        ret = random(sizeof(zgadz));

    return ret;
}

void
show_event()
{
    int i, czas, *prio;

    prio = filter(events, &operator([])(, 2));
    i = losuj_po_priorytetach(prio);

    efekt_ob->catch_msg(process_tags(events[i][0]));
    if(events[i][1])
        tell_roombb(ENV(efekt_ob), process_tags(process_string(events[i][1])), efekt_ob, efekt_ob);

    if(stringp(events[i][3]))
        call_otherv(efekt_ob, events[i][3], events[i][4]);

    czas = event_time + random(event_diff);
    all_time += czas;

    if(all_time <= max_time)
        event_alarm = set_alarm(itof(czas), 0.0, show_event);
    else
    {
        stop_effect(efekt_ob);
        all_time = 0;
        remove_object();
    }
}

/**
 * Efekt zaczyna dzia�a�
 * @param ob obiekt na kt�rym efekt ma zosta� rozpocz�ty.
 * @return pora�ka(<=0), sukces(1)
 */
int
start_effect(mixed ob)
{
    if(stringp(ob))
    {
        string str = ob;
        if(!(ob = find_player(str)))
            if(!(ob = find_object(str)))
                if(!(ob = find_living(str)))
                    return 0;
    }
    if(!objectp(ob))
        return 0;
    if(efekt_ob == ob)
        return -1;
    if(efekt_ob)
        return -2;
    if(!living(ob))
        return -3;
    efekt_ob = ob;

    event_alarm = set_alarm(itof(event_time + random(event_diff)), 0.0, show_event);

    return 1;
}

/**
 * Efekt przestaje dzia�a�
 * @param ob Obiekt na kt�rym efekt ma zosta� zako�czny.
 * @return pora�ka(<=0), sukces(1), w��czono eventy ko�ca(2)
 */
int
stop_effect(mixed ob)
{
    object *eff_obs;

    if(stringp(ob))
    {
        string str = ob;
        if(!(ob = find_player(str)))
            if(!(ob = find_object(str)))
                if(!(ob = find_living(str)))
                    return 0;
    }

    if(!objectp(ob))
        return 0;
    if(!efekt_ob)
        return -1;
    if(efekt_ob != ob)
        return -2;
    remove_alarm(event_alarm);

    eff_obs = efekt_ob->query_prop(PLAYER_AO_HAS_EFFECTS);

    int i;
    if((i = member_array(TO, eff_obs)) >= 0)
        eff_obs = exclude_array(eff_obs, i, i);

    efekt_ob->change_prop(PLAYER_AO_HAS_EFFECTS, eff_obs);

    efekt_ob = 0;
    return 1;
}

public string
show_subloc(string subloc, object on_obj, object for_obj)
{
     if(subloc != eff_subloc && eff_subloc)
         return "";
     if(on_obj == for_obj && eff_subloc_fo_tp)
         return process_tags(process_string(eff_subloc_fo_tp));
     else if(eff_subloc_fo_oth)
         return process_tags(process_string(eff_subloc_fo_oth));
}
