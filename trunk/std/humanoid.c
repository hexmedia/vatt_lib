/**
 * \file /std/humanoid.c
 *
 * Standard my�l�cych humanoid�w.
 *
 * W chwili obecnej jedyn� r�nic� mi�dzy \file /std/monster.c
 * na kt�rym ten standard jest oparty jest to, �e w tym
 * standardzie nie ma szansy edytowa� atak�w bez broni.
 *
 * @author Krun
 * @date Wrzesie� 2008
 */

#pragma save_binary
#pragma strict_types

//WARNING: Nie przestawia� kolejno�ci bo ma DU�E znaczenie dla dzia�ania.
inherit "/std/monster.c";
inherit "/std/combat/humunarmed.c";
inherit "/std/act/asking.c";
inherit "/std/act/ask_gen.c";
inherit "/std/act/chat.c";

void
create_humanoid()
{
}

nomask void
create_monster()
{
    create_humanoid();
    set_all_attack_unarmed(10, 10);
}

void
reset_humanoid()
{
}

nomask void
reset_monster()
{
    reset_humanoid();
}

/**
 * Funkcja identyfikuj�ca obiekt jako my�l�cego humanoida.
 *
 * @return Zawsze 1
 */
nomask int
is_th_humanoid()
{
    return 1;
}

public string
init_arg(string arg)
{
     //Krun pozdrawiam :) Zakomentowa�e� to p� roku temu, przez co by� b��d z nagimi npcami.
     //Vera.
     //PS. Naucz si� kodowa� w zespole!:)
     if(arg == 0)
         move_and_wearwield();

    ::init_arg(arg);
}
