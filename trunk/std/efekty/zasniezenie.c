/* efekt rzucania si� �nie�kami
    Vera
 */

inherit "/std/object";

#include <stdproperties.h>
#include <macros.h>

#define SUBLOC		"snieg_subloc"
#define ZASNIEZENIE "_zasniezenie"

int alarm_eventowy;
void event_sniegu(float delay);
int prog = random(10) + 4;


void
create_object()
{
    ustaw_nazwe("sniezka");
    add_prop(OBJ_M_NO_GIVE, 1);
    add_prop(OBJ_M_NO_DROP, 1);
    set_no_show();
}

public void                   //3minuty+ random(3min)
start_event_sniegowy(mixed ob, float czas = 180.0 + itof(random(180)))
{
    if (stringp(ob))
	ob = find_object(ob);

    if (member_array(SUBLOC, ob->query_sublocs()) != -1)
	ob->query_subloc_obj(SUBLOC)->remove_object();

    this_object()->move(ob, 1);
    ob->add_subloc(SUBLOC, this_object(), 0);
    alarm_eventowy = set_alarm(45.0+itof(random(10)), 0.0, &event_sniegu(65.0));

    set_alarm(czas, 0.0, "usun_eventy_sniegu");
}


void
event_sniegu(float delay)
{
    set_this_player(environment(this_object()));

    switch(random(18))
    {
        case 0: TP->command("zadrzyj"); break;
        case 1: 
            TP->catch_msg("Czujesz, lodowaty ch��d na karku. Odruchowo wytrz�sasz zza ko�nierza tyle �niedu ile "+
            "zdo�asz, ale wilgo� pozostaje sp�ywaj�c w d� plec�w. Zimne!\n");
            saybb(QCIMIE(TP,PL_MIA)+" nerwowym ruchem wytrz�sa �nieg zza ko�nierza, a je"+TP->koncowka("go","j")+" twarz wyra�nie "+
                                                                           "czerwienieje od zimna.\n");
                break;
        case 2:
                TP->catch_msg("Z twojego ramienia zsuwaj^a si^e ostatnie drobiny ^sniegu.\n");
                saybb("Z ramienia "+QIMIE(TP,PL_DOP)+" zsuwaj^a si^e ostatnie drobiny ^sniegu.\n");
                break;
        case 3:
            TP->catch_msg("�wiat dooko�a zaczyna powoli wirowa�...\n");
            break;
        case 4:
            
            TP->catch_msg("Opadaj^a z ciebie niemal ostatnie drobiny ^snie^znego puchu, lecz czujesz jak jaka^s "+
                        "resztka dosta^la ci si^e do buta powoli przemieniaj^ac w lodowat^a wilgo^c.\n");
            saybb("Z "+QIMIE(TP,PL_DOP)+" opadaj^a ostatnie drobiny ^sniegu, lecz nadal jest on"+TP->koncowka("","a")+" rumian"+
                                                           TP->koncowka("y","a")+" od ch�odu.\n");
            break;
        default: //nic
            break;
    }

    alarm_eventowy = set_alarm(delay+5.0, 0.0, &event_sniegu(delay+2.0));
}

public void
usun_eventy_sniegu()
{
    environment(this_object())->remove_subloc(SUBLOC);
    this_object()->remove_object();
}

public string
show_subloc(string subloc, object on_obj, object for_obj)
{
    if (subloc != SUBLOC)
	return "";

    
    if(on_obj->query_prop(ZASNIEZENIE) > prog)
    {
        if (on_obj == for_obj)
        return "�nieg pokrywa niemal�e ca�e twoje cia�o.\n";
        else
        return "Jest ca�" + on_obj->koncowka("y", "a") +
            " w �niegu.\n";    
    }
    else
    {
        if (on_obj == for_obj)
        return "Gdzieniegdzie masz na sobie zlepione warstwy �niegu.\n";
        else
        return "Gdzieniegdzie ma na sobie zlepione warstwy �niegu.\n";
    }
}

public int
jestem_efektem_zasniezenia()
{
    return 1;
}
