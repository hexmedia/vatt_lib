/* Jest to raczej niegro�na choroba przenoszona
    przez moskity z bagna obok Piany.
    Pr�cz event�w o gor�czce, potach i subloku z b�blami
    zabiera tak�e czasem zm�czenie. That's all.
    Vera
 */

inherit "/std/object";

#include <stdproperties.h>
#include <macros.h>

#define SUBLOC		"choroba_moskitow_subloc"

int alarm_eventowy;

void event_choroby(float delay);

void
create_object()
{
    ustaw_nazwe("choroba");
    add_prop(OBJ_M_NO_GIVE, 1);
    add_prop(OBJ_M_NO_DROP, 1);
    set_no_show();
}

public void                   //30minut+ random(10min)
zachoruj(mixed ob, float czas = 1800.0 + itof(random(600)))
{
    if (stringp(ob))
	ob = find_object(ob);

    if (member_array(SUBLOC, ob->query_sublocs()) != -1)
	ob->query_subloc_obj(SUBLOC)->remove_object();

    this_object()->move(ob, 1);
    ob->add_subloc(SUBLOC, this_object(), 0);
    alarm_eventowy = set_alarm(425.0, 0.0, &event_choroby(65.0));

    set_alarm(czas, 0.0, "wyzdrowiej");
}

void
paw(object kto)
{
    kto->command("zwymiotuj");
}

void
event_choroby(float delay)
{
    set_this_player(environment(this_object()));

    TP->add_fatigue(-random(80));

    switch(random(12))
    {
        case 0: TP->command("zadrzyj ."); break;
        case 1..2: 
          saybb(QCIMIE(TP, PL_MIA) + " drapie si� nerwowo po ca�ym ciele.\n");
          TP->catch_msg("Drapiesz si� nerwowo po ca�ym ciele.\n");
                break;
        case 3:
                TP->catch_msg("Czujesz si� raczej niewyra�nie.\n");
                break;
        case 4:
            TP->catch_msg("�wiat dooko�a zaczyna powoli wirowa�...\n");
            break;
        case 5:
            TP->command("zblednij");
            break;
        case 6:
            TP->catch_msg("B�l g�owy staje si� coraz bardziej uci��liwy.\n");
        case 7:
            TP->catch_msg("Czujesz mrowienie na ciele.\n");
        case 8:
            TP->catch_msg("Chyba zaraz zwymiotujesz!\n");
            set_alarm(1.0+itof(random(3)),0.0,"paw",TP);
            break;
        default: //nic
            break;
    }

    alarm_eventowy = set_alarm(delay+200.0, 0.0, &event_choroby(delay+200.0));
}

public void
wyzdrowiej()
{
    environment(this_object())->remove_subloc(SUBLOC);
    this_object()->remove_object();
}

public string
show_subloc(string subloc, object on_obj, object for_obj)
{
    if (subloc != SUBLOC)
	return "";

    if (on_obj == for_obj)
	return "Ca�e twoje cia�o pokryte jest licznymi "+
                "b�blami.\n";
    else
	return "Ca�e je" + on_obj->koncowka("go", "j", "go") +
	    " cia�o pokryte jest licznymi b�blami.\n";
}

public int
jestem_efektem_choroby_bagiennej()
{
    return 1;
}

void
sprawdz_po_loginie() //sprawdzamy, czy po przeleogowaniu sie mamy chorobe, jak nie to dodajemy
{
    if(!get_alarm(alarm_eventowy))
        zachoruj(ENV(TO));
}

public void
init()
{
    ::init();
    set_alarm(5.0,0.0, "sprawdz_po_loginie");
}
