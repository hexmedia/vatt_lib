/**
 * Silnik obs�ugujacy gildie w graczu
 *
 * Autor: Krun
 * Data : 08.05.2007
 */

mapping silniki_gildii = ([]);

private void sprawdz_gildie();

/**
 * Funkcja przypisuje gildie do gracza.
 * @param path �cie�ka do silnika gidlii
 */
void
dodaj_gildie(string path)
{
    if(!path)
        return;

    sprawdz_gildie();

    if(path[-2..-1] != ".c")
        path += ".c";

    if(file_size(path) == -1)
        return;

    string nazwa_gildii = (path)->query_nazwa_gildii();

    if(is_mapping_index(nazwa_gildii, silniki_gildii))
        silniki_gildii = m_delete(silniki_gildii, nazwa_gildii);

    silniki_gildii[nazwa_gildii] = path;

    if(!(path)->query_czlonek(TO->query_real_name()))
        (path)->dodaj_czlonka(TO->query_real_name());
}

/**
 * Funkcja usuwa przypisanie gildii do gracza.
 * @param arg nazwa gildii lub �cie�ka do silnika
 */
void
usun_gildie(string arg)
{
    if(arg[-2..-1] != ".c")
        arg += ".c";

    string nazwa_gildii;

    if(file_size(arg) == -1)
    {
         if(is_mapping_index(arg[0..-3], silniki_gildii))
             nazwa_gildii = arg[0..-3];
         else
             return;
    }
    nazwa_gildii = nazwa_gildii || (arg)->query_nazwa_gildii();   

    if(((silniki_gildii[nazwa_gildii])->query_czlonek(TO->query_real_name())))
        (silniki_gildii[nazwa_gildii])->usun_czlonka(TO->query_real_name());

    if(is_mapping_index(nazwa_gildii, silniki_gildii))
        silniki_gildii = m_delete(silniki_gildii, nazwa_gildii);
}

string *
query_krotkie_nazwy(string nazwa_gildii)
{
    sprawdz_gildie();
    return (silniki_gildii[nazwa_gildii])->query_krotkie_nazwy();
}

/**
 * @param nazwa_gildii pe�na lub kr�tka nazwa gildii z kt�rej chcemy pobra� tytu� gracza
 * @return Tytu� gracza z gildii podanej w argumencie.
 */
string
query_tytul_gildiowy(string nazwa_gildii)
{
    sprawdz_gildie();

    if(silniki_gildii[nazwa_gildii])
        return (silniki_gildii[nazwa_gildii])->query_tytul_czlonka(TO->query_real_name());

    foreach(string dl : m_indexes(silniki_gildii))
    {
        string *kr;
        kr = query_krotkie_nazwy(dl);
        if(!kr)
            continue;
        if(member_array(nazwa_gildii, kr) != -1)
        {
            return (silniki_gildii[dl])->query_tytul_czlonka(TO->query_real_name());
        }
    }
}

/**
 * @param nazwa_gildii nazwa gildii z kt�rej chcemy pobra� range gracza
 * @return range gracza w gildii podanej jako arg
 */
int
query_ranga_gildiowa(string nazwa_gildii)
{
    sprawdz_gildie();

    return ((silniki_gildii[nazwa_gildii])->query_ranga_czlonka(TO->query_real_name()));  
}

/**
 * @return wszyskie silniki gildii w jakich jest gracz
 */
mapping
query_gildie()
{
    sprawdz_gildie();
    return silniki_gildii;
}

/**
 * @return  plik silniku gildii.
 * @param nazwa_gildii nazwa gildii kt�rej plik chcemy dosta�
 */
string
query_gildia(string nazwa_gildii)
{
    sprawdz_gildie();
    return silniki_gildii[nazwa_gildii];
}

/**
 * Funkcja sprawdza czy gracz jest cz�onkiem wszystkich gildii
 * kt�re ma w li�cie, je�li zosta� z kt�rejkolwiek usuni�ty, zostaje
 * ona usuni�ta z tej listy.
 */
private void
sprawdz_gildie()
{
    int i;
    string *ind = m_indexes(silniki_gildii);

    for(i=0;i<sizeof(ind);i++)
    {
        if((silniki_gildii[ind[i]])->query_czlonek(TO->query_real_name()))
            continue;
        usun_gildie(ind[i]);
    }
}