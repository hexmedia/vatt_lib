/*
 * player/cmd_sec.c
 *
 * This is a subpart of player_sec.c
 *
 * Some standard commands that should always exist are defined here.
 * This is also the place for the quicktyper command hook.
 *
 *
 *  S� tutaj (na samym dole) te� parali�e do:
 *      -sp na kierunek
 *      -ukrywania i skradania (2.11.2008)
 *                                  by Vera.
 */

#include <files.h>
#include <macros.h>
#include <std.h>
#include <stdproperties.h>

//filter funs potrzebne tylko do koniec_paralizu_wiec_spogladamy() V.
#include <filter_funs.h>
//a to do parali��w do ukrywania si�:
#include <ss_types.h>
#include <exp.h>
#include <pogoda.h>
#include <materialy.h>
#include <wa_types.h>
#include <options.h>


//#define DBG(x) wmsg("SNEAK: "+x)
#define DBG(x) 0

/*
 * Prototypes.
 */
public nomask void save_me();
nomask int quit(string str);
public int save_character(string str);
static nomask int change_password(string str);

/*
 * Global variables, they are static and will not be saved.
 */
static int save_alarm;           /* The id of the autosave-alarm */
static private string password2; /* Used when someone changes his password. */

//dla usprawnienia usuwania przymiotnika 'zamy�lony' - po co ci�gle go sprawdza�
//tak b�dzie dzia�a� l�ej i szybciej. V.
int zamyslony = 0;

/*
 * modyfikator, kt�ry dodajemy do wyniku skradania si�
 * zale�ny od event�w, jakie natrafimy podczas tej czynno�ci.
 * Reszta modyfikator�w jest ju� w funkcji daj_paraliz_na_skradanie_se V */
int sneak_event_mod = 0;

/* ostatnia lokacja na kt�rej si� ukrywali�my.
 * je�li nast�pna pr�ba ukrycia si� jest na tej samej lokacji,
 * to nie przypisujemy expa
 */
string last_hide_loc = "";

/*
 * Function name: start_autosave
 * Description  : Call this function to start autosaving. Only works for
 *                wizards.
 */
static nomask void
start_autosave()
{
    /* Start autosaving if the player is not a wizard. */
    if (!query_wiz_level())
    {
        remove_alarm(save_alarm);

        save_alarm = set_alarm(300.0, 0.0, &save_me());
    }
}

/*
 * Function name: stop_autosave
 * Description  : Call this function to stop autosaving.
 */
static nomask void
stop_autosave()
{
    remove_alarm(save_alarm);
    save_alarm = 0;
}

void
dodaj_zamyslony()
{
    zamyslony = 1;
}

public int
usun_przym_zamyslony(string str)
{
    if(zamyslony)
    {
        zamyslony = 0;
        TP->check_if_idle(TP);
    }
    return 0;
}

/*
 * Function name: cmd_sec_reset
 * Description  : When the player logs in, this function is called to link
 *                some essential commands to him.
 */
static nomask void
cmd_sec_reset()
{
    add_action(usun_przym_zamyslony, "", 1);
    add_action(quit,            "zako�cz");
    add_action(save_character,  "nagraj");
    add_action(change_password, "has�o");

    init_cmdmodify();

    /* Start autosaving. */
    start_autosave();
}

static nomask string*
recursive_auto_str(object ob)
{
    object *objects;
    string str;
    string *auto = ({ });

    if (!objectp(ob))
        return auto;

    objects = all_inventory(ob);
    int i = sizeof(objects);

    while(--i >= 0)
    {
        str = objects[i]->query_auto_load();
        // FIXME: uwzgl�dni� rzeczy, kt�rych gracz nie m�g� zabra� ze sob�
        if (strlen(str))
            auto += ({ str });
        else
            auto += recursive_auto_str(objects[i]);
    }

    return auto;
}

/*
 * Function name: compute_auto_str
 * Description  : Walk through the inventory and check all the objects for
 *                the function query_auto_load(). Constructs an array with
 *                all returned strings. query_auto_load() should return
 *                a string of the form "<file>:<argument>".
 */
static nomask void
compute_auto_str()
{
    object *objects = all_inventory(TO);
    object *worn_objects = query_worn();
    string str;
    string *auto = ({ });

    int i = sizeof(worn_objects);
    while(--i >= 0) {
        str = worn_objects[i]->query_auto_load();
        // FIXME: uwzgl�dni� rzeczy, kt�rych gracz nie m�g� zabra� ze sob�
        if (strlen(str))
        {
            auto += ({ str });
        }
        else {
            auto += recursive_auto_str(worn_objects[i]);
        }
        objects -= ({ worn_objects[i] });
    }

    i = sizeof(objects);
    while(--i >= 0)
    {
        str = objects[i]->query_auto_load();
        // FIXME: uwzgl�dni� rzeczy, kt�rych gracz nie m�g� zabra� ze sob�
        if (strlen(str))
            auto += ({ str });
        else
            auto += recursive_auto_str(objects[i]);
    }
    set_auto_load(auto);
}

/*
 * Function name: save_me
 * Description  : Save all internal variables of a character to disk.
 */
public nomask void
save_me()
{
    /* Do some queries to make certain time-dependent
     * vars are updated properly.
     */
    query_mana();
    query_old_fatigue();
    query_hp();
    query_stuffed();
    query_soaked();
    query_intoxicated();
    query_headache();
    query_age();
    compute_auto_str();
    seteuid(0);
    SECURITY->save_player();
    seteuid(getuid(this_object()));

    /* If the player is a mortal, we will restart autosave. */
    start_autosave();
}

/*
 * Function name:   save_character
 * Description:     Saves all internal variables of a character to disk
 * Returns:         Always 1
 */
public int
save_character(string str)
{
    save_me();
    write("Ju�.\n");
    return 1;
}

/*
 * Function name: quit
 * Description:	  The standard routine when quitting. You cannot quit while
 *                you are in direct combat.
 * Returns:	  1 - always.
 */
nomask int
quit(string str)
{
    object *inv, tp;
    int    index, loc, size;

    tp = this_player();
    set_this_player(this_object());

    if (str == "cierpienia")
    {
        write("W takim razie pom�cz si� jeszcze troch�!\n");
        return 1;
    }
    
    tp->set_option(OPT_KODOWANIE, tp->query_option(OPT_KODOWANIE));

    //Ustawiamy lokacje na kt�rej gracz si� wyologowywuje.. TELEPORTY DLA WIZ�W!!!!
    object env = ENV(TO);
    if(!(TO->query_wiz_level()))
    {
        if(env)
        {
            if(SECURITY->legal_login_location(file_name(env)))
                TO->set_last_logout_location(file_name(env));
            else
            {
                string psl = TO->query_prop(OBJ_S_LOGOUT_LOCATION);
                LOAD_ERR(psl);
                if(psl && SECURITY->legal_login_location(psl) && find_object(psl))
                    TO->set_last_logout_location(psl);
            }
        }
    }

    //Przerobi�em troche ten kawa�ek kodu, dopiero je�li apoka jest za 10
    //sekund to mo�na zako�czy� bez przeszk�d.
    //Pozatym na wszelki wypadek przenios�em save_me wy�ej... (Krun)
    save_me();
    int shutt=ARMAGEDDON->shutdown_time();

    if((!shutt || shutt > 2) && calling_function(0) != "cleanup_shutdown")
    {
        if(TO->query_attack() || (fold(map(all_inventory(ENV(TO)),
           &operator(==)(TO, ) @ &->query_attack()),
           &operator(+)(), 0) > 0))
        {
            write(set_color(COLOR_BG_RED) + "Nie mo�esz "+
                "zako�czy� gry w takim po�piechu - jeste� w " +
                "trakcie walki." + clear_color() + "\n");
            return 1;
        }

        if(time() - query_prop(PLAYER_I_LAST_FIGHT) < 180)
        {
            write(set_color(COLOR_BG_RED) + "Dopiero co " +
                "walczy�" + TP->koncowka("e�", "a�") +
                ", odczekaj kilka minut." + clear_color()+ "\n");
            return 1;
        }
    }

    //czemu all_inventory a nie deep? Przecie� rzeczy
    //z plecak�w ty� trzeba sprawdzi�. Vera.
    //inv = all_inventory(this_object());
    inv = deep_inventory(this_object());

    /* Only mortals drop stuff when they quit. */
    //A TO CZEMU?w takim przypadku moglibysmy nie zauwazyc bledow [VERA]
    //A to, �eby kto� jakich� g�upot w �wiecie nie pozostawia� [KRUN]
    if(!query_wiz_level() && SECURITY->query_wiz_rank(query_real_name()) <= WIZ_MAGE)
    {
        index = -1;
        size = sizeof(inv);
        while(++index < size)
        {
            if (!(stringp(inv[index]->query_auto_load())))
            {
            /* However, we only try to drop it if the player is
            * carrying it on the top level.
            */
            if (!(inv[index]->query_prop(OBJ_M_NO_DROP)))
            {
                    //sprawd�my czy nie mamy tej rzeczy np. w plecaku
                    //wtedy przenosimy si�owo na momencik do gracza. Verek
                    if(ENV(inv[index]) != TO)
                        inv[index]->move(TO,1);
                    //wiem...amatorszczyzna...Ale nie jestem pewien, czy
                    //OB_NAME wszystko wy�apie. Powinno!
                    //Jedno jest pewne: ten command z parse_command_id_list
                    //nie dzia�a� na /std/corpse ! (mimo, �e dobrze zwraca�.
                    //V.
                    if(!command("po�� " + OB_NAME(inv[index])))
                        command("po�� " + inv[index]->parse_command_id_list(PL_BIE)[0]);
                    inv[index] = 0;
        }
            // FIXME: uwzgl�dni� rzeczy, kt�rych gracz nie m�g� zabra� ze sob�
                //Vera m�wi: gdybym wiedzia� o co temu komu� chodzi�o,
                //kto napisa� to 'fix_me' to mo�e
                //i bym to teraz zrobi�, ale nie wiem :P
        }
    }

    }

    TP->check_if_idle(TP,1);

    write("Nagrywam posta�.\n");

    inv = deep_inventory(this_object());
    size = sizeof(inv);
    index = -1;
    while(++index < size)
    {
        if (objectp(inv[index]))
        {
            // FIXME: uwzgl�dni� rzeczy, kt�rych gracz nie m�g� zabra� ze sob�
            SECURITY->do_debug("destroy", inv[index]);
        }
    }

    //Poniewa� na wszelki wypadek parali�e sie zapami�tuj�, �eby przy zerwaniu by�o ok.
    filter(deep_inventory(TP), &->is_paralyze())->remove_object();

    set_this_player(tp);

    environment(this_object())->on_player_logout(this_object());
    this_object()->remove_object();
    return 1;
}

/*
 * Function name: change_password_new
 * Description  : This function is called by change_password_old to catch the
 *                new password entered by the player. Calls itself again to
 *                verify the new entered password and makes sure the new
 *                password is somewhat secure.
 * Arguments    : string str - the new password.
 */
static nomask void
change_password_new(string str)
{
    write("\n");
    if (!strlen(str))
    {
        write("Nie poda�" + koncowka("e�", "a�", "o�") + " �adnego has�a, wi�c "+
            "nie zostanie ono zmienione.\n");
        return;
    }

    /* The first time the player types the new password. */
    if (password2 == 0)
    {
        if (strlen(str) < 6)
        {
            write("Nowe has�o musi mie� minimum 6 znak�w.\n");
            return;
        }

        if (!(SECURITY->proper_password(str)))
        {
            write("Nowe has�o musi spe�nia� podstawowe zasady " +
                "bezpiecze�stwa.\n");
            return;
        }

        password2 = str;
        input_to(change_password_new, 1);
        write("Wpisz nowe has�o ponownie, w celu potwierdzenia go.\n");
        write("Nowe has�o (ponownie): ");
        return;
    }

    /* Second password doesn't match the first one. */
    if (password2 != str)
    {
        write("Nowe has�a nie zgadzaj� si�. Has�o nie zosta�o zmienione.\n");
        return;
    }

    set_password(crypt(password2, "$1$"));	/* Generate new seed */
    password2 = 0;
    write("Has�o zmienione.\n");
}

/*
 * Function name: change_password_old
 * Description  : Takes and checks the old password.
 * Arguments    : string str - the given (old) password.
 */
static nomask void
change_password_old(string str)
{
    write("\n");
    if (!strlen(str) || !match_password(str))
    {
        write("Nieprawid�owe stare has�o.\n");
        return;
    }

    password2 = 0;
    input_to(change_password_new, 1);
    write("�eby twoje has�o by�o bezpieczniejsze, uwa�amy, i� powinno\n" +
          "spe�nia� podstawowe kryteria:\n" +
          "- has�o musi mie� minimum 6 znak�w;\n" +
          "- has�o musi zawiera� przynajmniej jeden znak nie b�d�cy " +
            "liter�;\n" +
          "- has�o musi zaczyna� si� i ko�czyc liter�.\n\n" +
          "Nowe has�o: ");
}

/*
 * Function name: change_password
 * Description  : Allow a player to change his old password into a new one.
 * Arguments    : string str - the command line argument.
 * Returns:     : int 1 - always.
 */
static nomask int
change_password(string str)
{
    write("Stare has�o: ");
    input_to(change_password_old, 1);
    return 1;
}

//------------------nizej wywolywane jest z soula przy sp na kierunek---------
string tmp_str2_ob;
void
daj_paraliz_na_sp(string str, string najaka_lok = "")
{
    tmp_str2_ob = str;
    object p=clone_object("/std/paralyze.c");
    p->set_finish_object(TP);
    
    if(strlen(najaka_lok))
        p->set_finish_fun("koniec_paralizu_wiec_spogladamy",najaka_lok);
    else
        p->set_finish_fun("koniec_paralizu_wiec_spogladamy");
    
    p->set_remove_time(1);
    p->set_stop_verb("przesta�");
    p->set_stop_message("Przestajesz spogl�da�.\n");
    p->set_fail_message("Chwil�! W�a�nie spogl�dasz!\n");
    p->remove_allowed("sp",0);
    p->remove_allowed("sp�jrz",0);
    p->remove_allowed("ob",0);
    p->remove_allowed("obejrzyj",0);
    p->remove_allowed("spogl�dnij",0);
    p->move(TP,1);
}
/*
 * Funkcja wywolana gdy paraliz sie skonczy w 'sp na kierunek'
 * Przeniesiona z soula, poniewaz byly zonki, jak dwoch graczy naraz
 * spojrzy gdzies ;)
 * doda�em argumencik, teraz ta funkcja dzia�a tak�e do wygl�dania
 * z dyli�ansu. ;] A po co mam duplikowa� kod. :P
 * Przy dylku object x to dylek. Veru�.
 *
 * A teraz jeszcze korzystam z tego przy sp na kierunek w ��dkach :)
 * Vera.
 */
int
koniec_paralizu_wiec_spogladamy(object x,string cos = "")
{
      object *lv, *dd, room;
      string str;

    if(tmp_str2_ob == "NONE")
    {
        write("Nie dostrzegasz tam niczego ciekawego.\n");
        return 1;
    }
 
    if(strlen(cos))
    {
        if(!objectp(room = find_object(cos)))
        {
            LOAD_ERR(cos);
            room = find_object(cos);
        }
    }
    else if(strlen(tmp_str2_ob))
    {
        if(!objectp(room = find_object(tmp_str2_ob)))
        {
            LOAD_ERR(tmp_str2_ob);
            room = find_object(tmp_str2_ob);
        }
    }
    //room = find_object(strlen(cos) ? cos : tmp_str2_ob);

      if (room->query_prop(OBJ_I_LIGHT) <= -(this_player()->query_prop(LIVE_I_SEE_DARK)))
      {
        if (stringp(room->query_prop(ROOM_S_DARK_LONG))) {
          write(room->query_prop(ROOM_S_DARK_LONG));
          return 1;
        }
        write("Ciemne miejsce.\n");
        return 1;
      }
      str = room->short();
      if (str[-1..-1] != ".") str += ".\n"; else str += "\n";
      write(str);
      lv = FILTER_CAN_SEE(FILTER_LIVE(all_inventory(room)), this_player());
      if (sizeof(lv))
        write(capitalize(COMPOSITE_FILE->desc_live(lv, PL_MIA, 1)) + ".\n");
      dd = FILTER_DEAD(all_inventory(room));

    if(x->query_dylizans()) //dylka nie pokazujemy, je�li 'wygl�damy' z niego
        dd -= ({x});

    //to jak ju� tu zacz��em grzeba� zn�w, to dam co� takiego:
    //przy 'sp na kierunek' nie powinno by� wida� rzeczy kt�re s�
    //jako 'niewyswietlane'
    string *niewyswietlane = room->query_shorty_rzeczy_niewyswietlanych();
    foreach(object dupa : dd)
        if(member_array(dupa->short(),niewyswietlane) != -1)
            dd-=({dupa});

      dd = filter(dd, objects_filter);
      if (sizeof(dd))
        write(capitalize(COMPOSITE_FILE->desc_dead(dd, PL_MIA, 1)) + ".\n");
      write(describe_combat(lv));
      return 1;
}
//------------------wyzej wywolywane jest z soula przy sp na kierunek---------

//------------------nizej wywolywane jest z soula przy ukrywaniu--------------
void
daj_paraliz_na_ukrywanie(object *co = ({ }), int val =0, int sukces = 1,object gdzie = 0,mapping heap_help_map=0)
{
    object p=clone_object("/std/paralyze.c");
    int self = 0;

    if(sizeof(co) && co[0] == TP)
        self = 1;

    if(val <= 0)
        sukces = 0;

    p->set_finish_object(TP);
    p->set_finish_fun("koniec_paralizu_wiec_ukrywamy",co,val,sukces,gdzie,self,heap_help_map);

    /*nigdy nie by�em or�em z matmy, ale ten wz�r na remove_time
    dzia�a tak, �e: skill od 0-9 daje 5, od 10-29 daje 4,
    i tak dalej. Cel osi�gni�ty - im wy�szy um tym mniejszy czas. :P */
    p->set_remove_time(((-TP->query_skill(SS_HIDE)/10 + 10) /2) + sizeof(co));
    p->set_stop_verb("przesta�");

    if(!sukces) //czyli z g�ry pora�ka - dajem inne, uniwersalne komunikaty
    {
        p->set_stop_message("Zaniechujesz pr�by ukrycia "+(self?"si�":"czego�")+".\n");
        p->set_fail_message("Chwil�! W�a�nie pr�bujesz gdzie� "+(self?"si�":"co�")+" ukry�!\n");
    }
    else if(sizeof(co) && co[0] == TP) //czyli ukrywam siebie
    {
        p->set_stop_message("Zaniechujesz pr�by ukrycia si�.\n");
        p->set_fail_message("Chwil�! W�a�nie pr�bujesz gdzie� si� ukry�!\n");
    }
    else //ukrywam *co
    {
        p->set_stop_message("Zaniechujesz pr�by ukrycia "+
            COMPOSITE_DEAD(co,PL_DOP)+".\n");
        p->set_fail_message("Chwil�! W�a�nie pr�bujesz ukry� "+
            COMPOSITE_DEAD(co,PL_BIE)+"!\n");
    }

    p->remove_allowed("sp",0);
    p->remove_allowed("sp�jrz",0);
    p->remove_allowed("ob",0);
    p->remove_allowed("obejrzyj",0);
    p->remove_allowed("spogl�dnij",0);
    p->move(TP,1);
}

int
koniec_paralizu_wiec_ukrywamy(object x,object *co = ({ }), int val =0,
            int sukces=0,object gdzie=0,int self=0, mapping heap_help_map=0)
{
    int exp_dex =  val / 10 + EXP_UKRYWAM_SIE_UDANE_DEX + sizeof(co); //2do12 + sizeof(co)
    int exp_hide = val / 10 + EXP_UKRYWAM_SIE_UDANE_HIDE + sizeof(co); //3do13 + sizeof(co)

    if(!sukces) //z g�ry za�o�ona pora�ka
    {
        //nie dajemy expa za ukrywanie si� wci�� na tej samej lokacji
        if(last_hide_loc != file_name(ENV(TP)))
            TP->increase_ss(SS_HIDE, EXP_UKRYWAM_SIE_NIEUDANE_HIDE);

        write("Nie jeste� w stanie " + (self ? "si� tu dobrze schowa�" :
          "tu czegokolwiek schowa�") + ".\n");
        //przypisujemy lok na kt�rej ostatnio pr�bowali�my si� ukry�.
        last_hide_loc = file_name(ENV(TP));

        return 1;
    }
    if(!sizeof(co)) //to by by�o naprawd� DZIWNE!
    {
        write("Wyst�pi� b��d w ukrywaniu, o numerze 34s69cd. Prosz�, 'zg�o�' "+
        "ten b��d podaj�c co i gdzie pr�bowa�"+TP->koncowka("e�","a�")+" ukry� oraz "+
        "ten numer b��du, a postaramy si� naprawi� to jak najszybciej.\n");
        return 1;
    }

    //ukrywamy siebie
    if(co[0] == TP)
        write("Uda�o ci si� gdzie� ukry�.\n");
    else
    {
        heap_help_map = m_delete(heap_help_map, 0);
        foreach(object q : m_indices(heap_help_map))
            if(q->query_prop(HEAP_I_IS))
                q->split_heap(heap_help_map[q]);

        //A o to i poprawka magicznego b��du, kt�ry wisia� na tablicy ju� ponad 2 lata i
        //przez bardzo d�ugi czas plasowa� si� na 1 miejscu :)
        //chodzi�o o ujawnianie groszy, przy ukrywaniu, kiedy ju� jakie� le�a�y na pod�odze
        //set_no_merge ma jedynie taki minus, �e wi�cej obiekt�w jest tworzonych, bo si�
        //nie ��cz�, ale to i tak nic takiego w por�wnaniu do ca�ego dnia sp�dzonego
        //na poprawie tego b��du :) Vera. 2009-07-04 18:38:00
        //-----------------bugfix-------------------
        foreach(object x : co)
            if(x->query_prop(HEAP_I_IS))
                x->set_no_merge(1);
        //-----+++---------bugfix--------+++--------        
            
            
        if(!objectp(gdzie)) //ukrywamy 'co'
            co = filter(co, "/cmd/live/things"->manip_relocate_to);
        else if(objectp(gdzie)) //ukrywamy 'co' 'gdzie'
            co = filter(co, &("/cmd/live/things")->manip_relocate_to(, gdzie));

        if(!sizeof(co))
        {
            notify_fail("Nic nie schowa�" + TP->koncowka("a�","e�") + ".\n");
            return 0;
        }

        TP->catch_msg("Uda�o ci si� ukry� " + COMPOSITE_DEAD(co, PL_BIE) +
              (objectp(gdzie)?" do "+QSHORT(gdzie, PL_DOP):"")+".\n");
        saybb(QCIMIE(this_player(), PL_MIA) + " ukrywa co�"+
              (objectp(gdzie)?" do "+QSHORT(gdzie, PL_DOP):"")+".\n");
    }


    co->add_prop(OBJ_I_HIDE, val);
    TP->add_mana(-val / 5); //zabieramy troch� si� mentalnych

    //nie dajemy expa za ukrywanie si� wci�� na tej samej lokacji
    if(last_hide_loc != file_name(ENV(TP)))
    {
        //dodajemy expa:
        TP->increase_ss(SS_DEX, exp_dex);
        TP->increase_ss(SS_HIDE, exp_hide);
    }

    //przypisujemy lok na kt�rej ostatnio si� ukrywali�my.
    last_hide_loc = file_name(ENV(TP));

    return 1;
}
//------------------wy�ej wywolywane jest z soula przy ukrywaniu--------------


//------------------nizej wywolywane jest z soula przy przemykaniu------------
void
eventy_skradania()
{
    string str = "";

    //du�e rzeczy na lokacji
    object *tmp_obj = ({ });
    foreach(object x : FILTER_SHOWN(FILTER_DEAD(all_inventory(ENV(TP)))))
        if(x->query_prop(OBJ_I_VOLUME) >= 15000)
            tmp_obj += ({x});

    /* eventy s� uporz�dkowane w kolejno�ci chronologicznej od
      najwa�niejszych */

    switch(TP->query_skill(SS_SNEAK))
    {
        case 0..19:

            if(!random(4))
                break;

            if(random(2) && CZY_JEST_SNIEG(ENV(TP)))
            {
                str = "�nieg trzeszczy pod twymi stopami!\n";
                sneak_event_mod += -3 - random(4);
            }

            if(ENV(TP)->query_prop(ROOM_I_TYPE) == ROOM_FOREST &&
                !strlen(str)) //nie zosta� wylosowany poprzedni jaki�
            {
                if(!random(4))
                {
                    str = "Ga��� pod twymi stopami �amie si� z g�o�nym trzaskiem!\n";
                    sneak_event_mod += -2 - random(3);
                }
                else if(!random(2))
                {
                    str = "Potykasz si� o konar drzewa! Ufff... ma�o brakowa�o.\n";
                    sneak_event_mod += -1 - random(2);
                }
            }
            else if(ENV(TP)->query_prop(ROOM_I_TYPE) == ROOM_TRACT &&
                !strlen(str)) //nie zosta� wylosowany poprzedni jaki�
            {
                if(!random(3))
                {
                    str = "Potykasz si� o jaki� wystaj�cy na drodze kamie�!\n";
                    sneak_event_mod += -2 - random(3);
                }
            }
            else if(ENV(TP)->query_prop(ROOM_I_TYPE) == ROOM_IN_CITY &&
                !strlen(str)) //nie zosta� wylosowany poprzedni jaki�
            {
                if(!random(3))
                {
                    str = "Potykasz si� o co� wystaj�cego z ulicy!\n";
                    sneak_event_mod += -2 - random(3);
                }
            }
            
            if(random(3) && !strlen(str))
                str = "Przystajesz na chwil�, by lepiej oceni� sytuacj�.\n";
            else if(random(2) && !strlen(str))
                str = "Po chwili wahania kontynuujesz pr�b�.\n";
            
            break;
        
        case 20..39:
            if(!random(3))
                break;

            if(!random(3))
            {
                str = "Ufff! Prawie by� si� potkn"+TP->koncowka("��","�a")+"!\n";
                sneak_event_mod += -1;
            }
            else if(!random(3))
                str = "Przystajesz na chwil�, by lepiej oceni� sytuacj�.\n";
            else if(!random(2) && sizeof(tmp_obj))
                str = "Pr�bujesz zas�oni� si� za "+
                QSHORT(tmp_obj[random(sizeof(tmp_obj))], PL_BIE)+".\n";
            else if(!random(2))
                str = "Delikatnie stawiaj�c stopy kroczysz naprz�d.\n";

            break;

        case 40..100:
            if(!random(2))
                break;

            if(random(3))
                str = "Z koci� gracj� posuwasz si� naprz�d.\n";
            else if(random(3))
                str = "Ostro�nie kroczysz naprz�d.\n";
            else if(random(3))
                str = "Przystajesz na moment, by znale�� najdogodniejsz� "+
                      "drog�.\n";
            else if(random(2) && sizeof(tmp_obj))
            {
                str = "Umiej�tnie wykorzystuj�c po�o�enie "+
                QSHORT(tmp_obj[random(sizeof(tmp_obj))], PL_BIE)+" przemykasz "+
                "niepostrze�enie.\n";
            }
            else if(random(2) && sizeof(tmp_obj))
            {
                str = "Dostrzegasz mo�liwo�� skrycia si� za "+
                QSHORT(tmp_obj[random(sizeof(tmp_obj))], PL_NAR)+".\n";
            }

            break;

        default:    //reszta ju� da rad� bez event�w. Czas si� skraca, lepiej
                    //to robimy itd.
                break;
    }

    if(strlen(str))
        TP->catch_msg(str);
}


void
daj_paraliz_na_przemykanie(int i, int val, int sukces = 1)
{
DBG("val pocz�tkowy: "+val+"\n");
    LOAD_ERR(ENV(TP)->query_exit_rooms()[i]);
    object p = clone_object("/std/paralyze.c"),
          cel = find_object(ENV(TP)->query_exit_rooms()[i]);
    int tmp;

    //je�li buty lub zbroja posiada za g��wny sk�adnik jeden z tych
    //materia��w, wtedy ujemne modyfikatory s�.
    string *glosne_materialy =
          ({MATERIALY_SZ_ZLOTO, MATERIALY_SZ_SREBRO,
          MATERIALY_RZ_ZELAZO, MATERIALY_RZ_STAL, MATERIALY_RZ_MIEDZ,
          MATERIALY_RZ_MOSIADZ
          }),
           *tmp_zbroj;

    if(!objectp(cel))
    {
        notify_wizards(ENV(TP), "st�d nie ma wyj�cia na :"+
            ENV(TP)->query_exit_cmds()[i]+"!!!!!!!!! A POWINNO BO TAK M�WI "+
            "query_exit_cmds!\n");
        write("Wyst�pi� b��d o numerze 32nv95. Nie widz� st�d wyj�cia na "+
            ENV(TP)->query_exit_cmds()[i]+"! Prosz� zg�o� to czym pr�dzej!\n");
        return;
    }

    if(val <= 0)
        sukces = 0;


    //zerujemy modyfikator eventowy z poprzedniego skradania si�
    sneak_event_mod = 0;

    //+++++++++++++++++MODYFIKATORY+++++++++++++++++++++++

    //przeci��enie
    tmp = TP->query_encumberance_weight();
    val = val - tmp / 6;
if(tmp/6)
DBG("mod za przeci��enie: "+(tmp/6)+"\n");

    //strach
    tmp = (10 + (int)TP->query_stmpt(SS_DIS) * 3);
    tmp = 100 * TP->query_panic() / (tmp != 0 ? tmp : TP->query_panic());
    //tmp jest teraz procentem od 0-100, wi�c zmniejszmy, dajmy na to na 4,
    //w ko�cu panika powinna mie� du�y wp�yw.
    val = val - tmp / 4;
if(tmp/4)
DBG("mod za strach: "+(tmp/4)+"\n");
    //pija�stwo
    tmp = TP->query_prop(LIVE_I_MAX_INTOX);
    tmp = 100 * TP->query_intoxicated() / (tmp != 0 ? tmp : TP->query_intoxicated());
    if(tmp)
{      val = val - tmp / 3;
if(tmp/3)
DBG("mod za pija�stwo: "+(tmp/3)+"\n");
}
    else //kacyk?
    {
        tmp = TP->query_prop(LIVE_I_MAX_INTOX);
        tmp = 100 * TP->query_headache() / (tmp != 0 ? tmp : TP->query_headache());
        val = val - tmp / 10;
if(tmp/10)
DBG("mod za kacyk: "+(tmp/10)+"\n");
    }
    
    //A teraz te ciekawsze modyfikatorki ;) (ju� nie procentowo!)
    //Ci�kie buty to du�y minus. Podobnie jak i zbroja.
    tmp_zbroj = (TP->query_armour(TS_R_FOOT))->query_materialy();
    foreach(string *s : tmp_zbroj) //im wi�cej warstw tym gorzej
        if(member_array(s[0], glosne_materialy) != -1)
        {
            tmp = -6 - random(6);
DBG("mod za ci�kie buciory: "+tmp+"\n");
            val += tmp;
        }

    //mod za ci�k� zbroj� na tors.
    //Ju� nie taki masakryczny jak za buty, ale zawsze
    tmp_zbroj = (TP->query_armour(TS_CHEST))->query_materialy();
    foreach(string *s : tmp_zbroj) //im wi�cej warstw tym gorzej
        if(member_array(s[0], glosne_materialy) != -1)
        {
            tmp = -4 - random(5);
DBG("mod za ci�k�/brzecz�c� zbroj�: "+tmp+"\n");
            val += tmp;
        }

    if(HAS_FREE_HAND(TP))
    {
        tmp = 1;
DBG("bonus za woln� r�k� "+tmp+"\n");
        val += tmp;
    }

    if(TP->query_prop(LIVE_I_SEE_DARK) < 0)
    {
        tmp = -16 - random(8);
DBG("mod za ciemno jak w dupie "+tmp+"\n");
        val += tmp;
    }

    tmp = 0;
    //modyfikatory od typu lokacji na kt�r� przemykamy
    switch(cel->query_prop(ROOM_I_TYPE))
    {   //zacznijmy od najtrudniejszych:
        case ROOM_IN_WATER:
        case ROOM_UNDER_WATER:   tmp = -60; break;
        case ROOM_DESERT:  tmp = -50; break;
        case ROOM_TREE:    tmp = -40; break;
        case ROOM_FIELD:   tmp = -5; break;
        //bonusy:
        case ROOM_IN_CITY:    tmp = 1 + random(6); break;
        case ROOM_TRACT:    //tutaj zdecydowanie naj�atwiej si� skrada�
        case ROOM_FOREST:   //wi�c dajemy bonus
        case ROOM_SWAMP:   tmp = 8 + random(5); break;
        case ROOM_CAVE:
        case ROOM_MOUNTAIN:   tmp = 1 + random(3); break;
    }

    if(tmp != 0)
{
      val += tmp;
DBG("mod za typ lokacji docelowej: "+tmp+"\n");
}

    //pod os�on� nocy outside dostajemy bonus
    if(cel->jest_noc() && !cel->query_prop(ROOM_I_INSIDE))
    {
        tmp = random(4) + 6;
DBG("bonus za to, �e noc: "+tmp+"\n");
        val += tmp;
    }
    //im ja�niej, tym trudniej si� skrada�
    if(cel->query_prop(ROOM_I_LIGHT) > 1)
    {
        tmp = -2 - (random(5));
DBG("mod za to, �e kurewsko jasno jest tutaj: "+tmp+"\n");
        val += tmp;
    }

    //z drzewa jest trudno! ujemny mod.
    if(ENV(TP)->query_prop(ROOM_I_TYPE) == ROOM_TREE &&
        cel->query_prop(ROOM_I_TYPE) != ROOM_TREE)
    {
        tmp = -6 - (random(5));
DBG("mod za to, �e z drzewa czmychamy: "+tmp+"\n");
        val += tmp;
    }


    //+++++++++++KONIEC MODYFIKATOR�W+++++++++++++++++++++
DBG("val po modyfikatorach: "+val+"\n");

    p->set_finish_object(TP);
    p->set_finish_fun("koniec_paralizu_wiec_przemykamy",i,val,sukces,cel);

    /*nigdy nie by�em or�em z matmy, ale ten wz�r na remove_time
    dzia�a tak, �e: skill od 0-9 daje 5, od 10-29 daje 4,
    i tak dalej. Cel osi�gni�ty - im wy�szy um tym mniejszy czas. :P */
    p->set_remove_time(((-TP->query_skill(SS_SNEAK)/10 + 10) /2) + 1);
    p->set_stop_verb("przesta�");

    p->set_stop_message("Zaniechujesz pr�by przemkni�cia si�.\n");
    p->set_fail_message("Chwil�! W�a�nie pr�bujesz dok�d� przemkn��!\n");
    p->set_event_time(1.0 + itof(random(3)));
    p->set_event_fun("eventy_skradania");
    p->set_event_object(TO);
    p->move(TP,1);
}

int
koniec_paralizu_wiec_przemykamy(object x, int i, int val, int sukces, object cel)
{
    int hiding = ENV(TP)->query_prop(ROOM_I_HIDE);
    int bval = TP->query_skill(SS_HIDE);


    //musimy go destructn�� parali� przed wywo�aniem command() na graczu
    filter(deep_inventory(TP), &->is_paralyze())->remove_object();

DBG("modyfikator eventowy: "+sneak_event_mod+"\n");
    //dodajemy modyfikator eventowy
    val += sneak_event_mod;
DBG("ko�cowy val: "+val+"\n");

    if(!sukces)
    {
DBG("z g�ry za�o�ona pora�ka\n");
        //expimy skradanko
        TP->increase_ss(SS_SNEAK, EXP_SKRADAM_SIE_NIEUDANY_SNEAK);
        //ale zabieramy troch� zm�czenia
        TP->add_fatigue(-5);

        write("Nie jeste� w stanie wymkn�� si� st�d niezauwa�enie.\n");
        return 1;
    }

    if(val <= 0)
    {
DBG("Pora�ka za modyfikatory\n");
        //expimy skradanko
        TP->increase_ss(SS_SNEAK, EXP_SKRADAM_SIE_NIEUDANY_SNEAK);
        //ale zabieramy troch� zm�czenia
        TP->add_fatigue(-5);

        write("Zdaj�c sobie spraw� z ja�owo�ci tej pr�by w ostatniej chwili "+
              "zawracasz z powrotem. Bez w�tpi�nia zosta�"+TP->koncowka("","a")+
              "by� zauwa�on"+TP->koncowka("y","a")+"!\n");
        return 1;
    }

    TP->add_prop(OBJ_I_HIDE, val);
    TP->add_prop(LIVE_I_SNEAK, 1);
    TP->command(ENV(TP)->query_exit_cmds()[i]);

    bval = (bval - hiding) / 2;
    

    if (hiding < 0 || bval <= 0)
    {
        //expimy skradanko
        TP->increase_ss(SS_SNEAK, EXP_SKRADAM_SIE_NIEUDANY_SNEAK);
        //ale zabieramy troch� zm�czenia
        TP->add_fatigue(-7);

        write("Jest tu zbyt ci�ko si� schowa�, wi�c jeste� widoczn" +
            TP->koncowka("a","y") + " z powrotem.\n");
        TP->reveal_me(0);
        return 1;
    }

    if (TP->query_prop(OBJ_I_LIGHT) &&
        (TP->query_prop(OBJ_I_LIGHT) >
        environment(TP)->query_prop(OBJ_I_LIGHT)))
    {
        //expimy skradanko
        TP->increase_ss(SS_SNEAK, EXP_SKRADAM_SIE_NIEUDANY_SNEAK);
        //ale zabieramy troch� zm�czenia
        TP->add_fatigue(-7);
        write("Posiadaj�c �r�d�o �wiat�a nie jeste� w stanie ukry� si� " +
            "skutecznie.\n");
        TP->reveal_me(1);
        return 1;
    }

    val = bval + random(bval);
    TP->add_prop(OBJ_I_HIDE, val);

    //expimy skradanko tylko, je�li skradamy si� na lok, na kt�rej
    //ostatnio nas nie by�o
    if(!TP->query_prop(LIVE_O_LAST_LAST_ROOM) ||
          TP->query_prop(LIVE_O_LAST_LAST_ROOM) != cel)
    {
        TP->increase_ss(SS_SNEAK, EXP_SKRADAM_SIE_UDANY_SNEAK);
        TP->increase_ss(SS_SNEAK, EXP_SKRADAM_SIE_UDANY_DEX);
    }
    //ale zabieramy troch� zm�czenia
    TP->add_fatigue(-7);
    //i mentalnego
    TP->add_mana(-(val / 8 + 2));

    return 1;
}
//------------------wy�ej wywolywane jest z soula przy przemykaniu-----------

