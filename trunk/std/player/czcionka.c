/**
 * Trzeba ten plik jako� przezwa� i u�ywa� go nie tylko do czcionki.
 */

/*
 * Taki ma�y pliczek pomocniczy, do obs�ugi r�nych standard�w
 * kodowania polskich znak�w.
 */
#define FONT_STANDARD   0
#define FONT_LATIN2     1
#define FONT_WIN1250    2
#define FONT_UTF8       4

int kodowanie,
    ansi,
    mxp;

int
query_kodowanie()
{
    return kodowanie;
}

void 
ustaw_kodowanie(int k)
{
    kodowanie = k;
}

void
ustaw_czcionke(int i)
{
    kodowanie = i;
}

int
query_czcionka()
{
    return kodowanie;
}

void
ustaw_ansi(int a)
{
    ansi = a;
}

int
query_ansi() 
{
    return ansi;
}

void
ustaw_mxp(int m)
{
    mxp = m;
}

int
query_mxp()
{
    return mxp;
}
