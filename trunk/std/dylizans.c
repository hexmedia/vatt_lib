/**
 *  Dylizanse by Rantaur [31.07.06]
 *  Wersja 2.0 beta ;)
 *  do u�ytku doprowadzi�, b��dy poprawi� i doda� nowe ficzery,
 *  takie jak wo�nica, reakcja na walk� i reszta: vera 11.12.07
 *
 * @author Rantaur
 * @date 31.07.06
 *
 * @author Vera
 * @date 11.12.07
 */

inherit "/std/object";
inherit "/lib/trade";

#include <macros.h>
#include <mudtime.h>
#include <cmdparse.h>
#include <pl.h>
#include <stdproperties.h>
#include <filter_funs.h>

#define DBG(x) find_player("vera")->catch_msg(x+"\n");

void set_enter_msg(string str);

void set_leave_msg(string str);

void msg_enter_in();

void msg_enter_out();

void msg_leave_in();

void msg_leave_out();

void msg_niestac();

void start();

void stop();

/*
 przyk�ad chyba ilustruje wszystko:
 dodaj_komende("wskocz", "wskakuje","wskoczy�", "na", PL_DOP);
 */
void dodaj_komende(string cmd, string os3, string inf, string prep, int gcase);

void ustaw_cene(int i);

void ustaw_czas_postoju(int i);             /* Ustawia czas postoju w sekundach */

void ustaw_ilosc_miejsc(int i);

void ustaw_postoje(string *str);            /* Ustawia nazwy kolejnych postojow
                                               Podajemy w mianowniku
                                               Np. ({"Karczma Baszta", "Przedbramie rinde"})*/

void ustaw_trase(string *str);              /* Ustawia trase:
                                               ** - oznacza postoj
                                               Kazda trasa powinna zaczynac sie i konczyc postojami ;)
                                               Przyklad: ({"**","e","e,"n","e","**"}) */

void ustaw_trase_powrotu(string *str);      /* Ustawia trase powrotna w przypadku, gdy
                                               nie mozna uzyc inwersji. Uzycie analogiczne do
                                               ustaw_trase */

void ustaw_szybkosc(int i);                 /* Ustawia czas pobytu pojazdu na lokacji
                                               w czasie jazdy */

void ustaw_wnetrze(string str);

void ustaw_woznice(string str);

int wsiadz_inne();

int wsiadz(string str);

int query_ruszam_sie();

mixed *komendy = ({ });
string *postoje = ({ });
string *trasa_curr = ({ });
string *trasa_forw = ({ });
string *trasa_back = ({ });

nomask void postoj();
//int parse_this(string str);
string ret_postoj();
string ret_nastepny_postoj();
string ret_kierunek_z();
string ret_kierunek_na();
string sub_dir_alias(string str);
static string zmien_kierunek_na_przeciwny(string str);
static string zmien_kierunek(string str);
string *odwroc_trase(string *tmp_trasa);
string *odwroc_postoje(string *arr);
void po_postoju();

int alarm_id;

int cena = 10;
int czas_postoju = 20;
int dir = 1;
int ruszam_sie = 0;
int szybkosc = 5;
int ilosc_miejsc = 6;
object woznica;
string woznica_path;
object wnetrze;
string wnetrze_path;

string enter_msg;
string leave_msg;

int ktory_postoj = 0;
int progress = 0;

void create_dylizans()
{
}

nomask void create_object()
{
    setuid();
    seteuid(getuid());
    ustaw_nazwe("dylizans");
    set_long("Jest to standardowy dyli�ans.\n");
    add_prop(OBJ_I_WEIGHT, 25042);
    add_prop(OBJ_I_VOLUME, 337500);
    add_prop(OBJ_I_NO_GET, "Jasne! Mo�e jeszcze spr�bujesz go schowa� do kieszonki?\n");
    set_enter_msg("przyje�d�a");
    set_leave_msg("wyje�d�a");
    create_dylizans();
    config_default_trade();
    trasa_curr = trasa_forw;
}

init()
{
    ::init();
    init_cmds();
}

string
query_pomoc()
{
    if(query_ruszam_sie())
        return UC(query_name())+" w�a�nie gdzie� p�dzi. By z "+
            koncowka("niego","niej","tego")+" skorzysta� musisz "+
            "odnale�� post�j na jakim si� zatrzymuje i tam na "+
            koncowka("niego","ni�","to")+" zaczeka�.\n";
    else
        return "Za pewn� op�at� "+query_name()+" "+koncowka("ten","ta","to")+
                " przewiezie ci� w okre�lone miejsce (na postoju zwykle "+
                "znajduj� si� tabliczki informacyjne o tym, gdzie dan"+
                koncowka("y","a","e")+ " si� zatrzymuje. Je�li chcesz "+
                "skorzysta� z us�ugi musisz wpierw zap�aci� wo�nicy.\n";
}

int
zaplac()
{
    write("Wystarczy, �e wsi�dziesz, a wo�nica ju� sam zatroszczy si� "+
        "o twoje monety.\n");
    return 1;
}

int
sprawdz_woznice()
{
    if(!objectp(woznica))
        woznica=find_living(woznica_path);

    if(!objectp(wnetrze))
        ustaw_wnetrze(wnetrze_path);

    if(!objectp(woznica))
        return 0;

    if(!objectp(wnetrze->query_woznica()))
        wnetrze->set_woznica(woznica);

    return 1;
}
nomask void init_cmds()
{
    int cmdarr_size = sizeof(komendy);

    for(int i =0;i < cmdarr_size; i++)
    {
        mixed cmd = komendy[i][0];
        add_action("wsiadz", cmd);
    }

    add_action("zaplac", "zap�a�");
}

nomask int move_me()
{
    if(trasa_curr[progress] == 0)
        return 0;

    if(trasa_curr[progress] ~= "**")
    {
        postoj();
        return 1;
    }

    if(trasa_curr[progress] ~= "brama")
    {
        //TODO: otwieranie bram zrobic (p�ki co niepotrzebne)
        return 1;
    }

    if(!sprawdz_woznice())
        return 0;

    int index = member_array(sub_dir_alias(trasa_curr[progress]), environment()->query_exit_cmds());
    if(index == -1)
    {
        tell_roombb(wnetrze, ">> Wyst�pi� b��d podczas przemieszczania" +
            "obiektu, b�d� tak dobry/a i zg�o� go <<\n");
        return 0;
    }

    ruszam_sie = 1;
    string path = environment(TO)->query_exit_rooms()[index];
    tell_roombb(environment(), check_call(leave_msg)+".\n");

    TO->move(path);
    progress++;

    tell_roombb(environment(), check_call(enter_msg)+".\n");

    if(trasa_curr[progress] ~= "**")
    {
        postoj();
        return 1;
    }

    alarm_id = set_alarm(itof((szybkosc)+random(3)), 0.0, &move_me());
    return 1;
}

nomask void postoj()
{
    wnetrze->stop_events();
    remove_alarm(alarm_id);
    ruszam_sie = 0;

    msg_leave_in();

    sprawdz_woznice();

    if(objectp(woznica))
        woznica->command(wnetrze->query_exitcmd());

    msg_enter_out();

    if(progress == sizeof(trasa_curr)-1)
    {
        if(sizeof(trasa_back) == 0)
            trasa_curr = odwroc_trase(trasa_curr);
        else
        {
            if(dir)
            {
                trasa_curr = trasa_back;
                dir = 0;
            }
            else
            {
                trasa_curr = trasa_forw;
                dir = 1;
            }
        }
        postoje = odwroc_postoje(postoje);
        progress = 1;
        ktory_postoj = 1;
    }
    else
    {
        progress++;
        ktory_postoj++;
    }


    alarm_id = set_alarm(itof(czas_postoju), 0.0, &po_postoju());
}

void dodaj_komende(string cmd, string os3, string inf, string prep, int gcase)
{
    komendy = komendy + ({ ({cmd})+({os3})+({inf})+({prep})+({gcase}) });
}

void set_enter_msg(string str)
{
    enter_msg = str;
}

void set_leave_msg(string str)
{
    leave_msg = str;
}


void msg_enter_in()
{
}

void msg_enter_out()
{
}

void msg_leave_in()
{
}

void msg_leave_out()
{
}

void msg_niestac()
{
}

void start()
{
    woznica = clone_object(woznica_path);

    if(!objectp(wnetrze))
        wnetrze = find_object(wnetrze_path);

    if(objectp(woznica))
        woznica->move(wnetrze);

    woznica->init_arg();
    woznica->set_dylek(TO);
    wnetrze->set_woznica(find_living(file_name(woznica)));
    move_me();
}

void stop()
{
    remove_alarm(alarm_id);
}

void ustaw_cene(int i)
{
    cena = i;
}

void ustaw_czas_postoju(int i)
{
    czas_postoju = i;
}

void ustaw_ilosc_miejsc(int i)
{
    ilosc_miejsc = i;
}

void ustaw_postoje(string *str)
{
    postoje = str;
}

void ustaw_szybkosc(int i)
{
    szybkosc = i;
}

void ustaw_trase(string *str)
{
    trasa_forw = str;
}

void ustaw_trase_powrotu(string *str)
{
    trasa_back = str;
}

void ustaw_wnetrze(string str)
{
    LOAD_ERR(str);
    wnetrze_path = str;
    wnetrze = find_object(str);
    wnetrze->set_dylizans(TO);
}

void ustaw_woznice(string str)
{
    LOAD_ERR(str);
    woznica_path = str;
    woznica = find_object(str);
}

int wsiadz(string str)
{
    int cmdarr_size = sizeof(komendy);
    int cmdindex;

    for(int i = 0; i < cmdarr_size; i++)
    {
        mixed tmp = komendy[i][0];
        if(query_verb() ~= tmp)
        {
            cmdindex = i;
            break;
        }
    }

    if(!str)
    {
        notify_fail("Gdzie chcesz "+komendy[cmdindex][2]+"?\n");
        return 0;
    }

    object obj;

    if(!parse_command(lower_case(str), environment(), " '"+komendy[cmdindex][3]+"' %o:"+komendy[cmdindex][4], obj))
    {
        notify_fail("Gdzie chcesz "+komendy[cmdindex][2]+"?\n");
        return 0;
    }

    if(obj != TO)
        return 0;

    if(ruszam_sie)
    {
        notify_fail("Nie mo�esz wsi��� do jad�ce"+
            koncowka("go","j", "ych", "ych", "ych")+" "+
            TO->query_nazwa(PL_DOP)+"!\n");
        return 0;
    }

    sprawdz_woznice();

    if(member_array(TP->query_real_name(),woznica->query_wrogowie()) != -1)
    {
        woznica->hook_olej_wroga(TP);
        TP->catch_msg("Zdaje si�, �e czym� sobie musia�"+TP->koncowka("e�","a�")+
        " nagrabi� i teraz "+QIMIE(woznica,PL_BIE)+" nie chce ci� obs�u�y�.\n");
        return 1;
    }

    int ile_ludzi = sizeof(FILTER_LIVE(all_inventory(wnetrze)));

    if(ile_ludzi >= ilosc_miejsc && TP != woznica)
    {
        notify_fail("Wygl�da na to, �e nie zmie�cisz si� ju� do "+TO->query_nazwa(PL_DOP)+".\n");
        return 0;
    }

    if(wsiadz_inne()) return 1;

    if(present(woznica, environment(TP)) && cena != 0)
    {
        if(!can_pay(cena, TP))
        {
            msg_niestac();
            return 1;
        }
        else
        {
            pay(cena, TP);
            TP->catch_msg("P�acisz nale�n� kwot� wo�nicy i wsiadasz do " + TO->short(PL_DOP) + ".\n");
        }
    }
    else
    {
        TP->catch_msg("W okolicy nie dostrzegasz wo�nicy - zdaje si�, �e " + TO->query_nazwa(PL_BIE) +
            " czeka nieco d�u�szy post�j...\n");
    }

    saybb(QCIMIE(TP, PL_MIA)+" p�aci nale�n� kwot� wo�nicy, po czym "+
        komendy[cmdindex][1]+" "+komendy[cmdindex][3]+" "+
        TO->short(komendy[cmdindex][4])+".\n");
    TP->move_living("M", wnetrze, 1);
    return 1;
}

int wsiadz_inne()
{
}

int query_ruszam_sie()
{
  return ruszam_sie;
}

string sub_dir_alias(string str)
{
    switch (str)
    {
        case "n":       return "polnoc";
        case "ne":      return "polnocny-wschod";
        case "e":       return "wschod";
        case "se":      return "poludniowy-wschod";
        case "s":       return "poludnie";
        case "sw":      return "poludniowy-zachod";
        case "w":       return "zachod";
        case "nw":      return "polnocny-zachod";
        case "d":       return "dol";
        case "u":
        case "g�r?":    return "gora";
        default: return str;
    }
}

string* odwroc_trase(string *tmp_trasa)
{
    string *trasa_inv;
    int size = sizeof(tmp_trasa);
    trasa_inv = allocate(size);
    size--;
    for(int i = 0; i <= size; i++)
    {
        switch (tmp_trasa[size-i])
        {
            case "n":     trasa_inv[i] = "s";     break;
            case "ne":    trasa_inv[i] = "sw";    break;
            case "e":     trasa_inv[i] = "w";     break;
            case "se":    trasa_inv[i] = "nw";    break;
            case "s":     trasa_inv[i] = "n";     break;
            case "sw":    trasa_inv[i] = "ne";    break;
            case "w":     trasa_inv[i] = "e";     break;
            case "nw":    trasa_inv[i] = "se";    break;
            default:      trasa_inv[i] = tmp_trasa[size-i];
        }
    }
    return trasa_inv;
}

string *odwroc_postoje(string *arr)
{
    int size = sizeof(arr);
    string *arr_inv = allocate(size);
    size--;

    for(int i = 0; i <= size; i++)
        arr_inv[i] = arr[size-i];
    return arr_inv;
}

string ret_postoj()
{
    return postoje[ktory_postoj];
}

string ret_nastepny_postoj()
{
    if(ktory_postoj != sizeof(postoje)-1)
        return postoje[ktory_postoj+1];

    string *tmp_postoje = allocate(sizeof(postoje));
    tmp_postoje = odwroc_postoje(postoje);
    return tmp_postoje[1];
}

string ret_kierunek_z()
{
    return zmien_kierunek_na_przeciwny(trasa_curr[progress-1]);
}

string ret_Kierunek_z()
{
    return capitalize(ret_kierunek_z());
}

string ret_kierunek_na()
{
    return zmien_kierunek(trasa_curr[progress]);
}

string ret_Kierunek_na()
{
    capitalize(ret_kierunek_na());
}

void po_postoju()
{
    msg_leave_out();

    sprawdz_woznice();

    if(objectp(woznica))
    {
        tell_roombb(environment(), QCIMIE(woznica, PL_MIA) + " " + komendy[0][1] + " " +
        komendy[0][3] + " " + TO->query_nazwa(komendy[0][4])+".\n");
        woznica->move_living("M", wnetrze);
        tell_roombb(wnetrze, QCIMIE(woznica, PL_MIA)+" przybywa.\n");
    }

    msg_enter_in();
    wnetrze->show_events();
    move_me();
}

static string
zmien_kierunek(string str)
{
    switch (str)
    {
        case "n":   return " na p�noc";
        case "s":   return " na po�udnie";
        case "e":   return " na wsch�d";
        case "w":   return " na zach�d";
        case "ne":  return " na p�nocny-wsch�d";
        case "nw":  return " na p�nocny-zach�d";
        case "se":  return " na po�udniowy-wsch�d";
        case "sw":  return " na po�udniowy-zach�d";
        case "d":   return " w d�";
        case "g":   return " pod g�r�";
        default:    return " " + str;
    }
}

static string
zmien_kierunek_na_przeciwny(string str)
{
    switch (str)
    {
        case "n":   return "z po�udnia";
        case "s":   return "z p�nocy";
        case "e":   return "z zachodu";
        case "w":   return "ze wschodu";
        case "ne":  return "z po�udniowego-zachodu";
        case "nw":  return "z po�udniowego-wschodu";
        case "se":  return "z p�nocnego-zachodu";
        case "sw":  return "z p�nocnego-wschodu";
        case "d":   return "z g�ry";
        case "g":   return "z do�u";
        default:    return "";
    }
}

/**
 * Funkcja identyfikuj�ca obiekt jako dyli�ans.
 *
 * @return 1 zawsze
 */
int query_dylizans() { return 1; }

/**
 * Funkcja identyfikuj�ca obiekt jako dyli�ans.
 *
 * @return 1 zawsze
 */
int is_dylizans() { return 1; }

/**
 * Wy��czamy auto�adowanie si�.
 */
public string
query_auto_load()
{
    return 0;
}

