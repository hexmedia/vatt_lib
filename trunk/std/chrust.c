/**
 * Jest to obiekt chrustu z kt�rego mo�na u�o�y� ognisko! :P
 *
 * Dzielenie wi�zki chrustu zer�ni�te z /std/food, z poprawkami
 * u�atwiaj�cymi implementacj� ;)
 *
 * @author Vera
 */

#pragma strict_types

inherit "/std/object";
//#include <object_types.h>

#include <sit.h>
#include <macros.h>
#include <cmdparse.h>
#include <ss_types.h>
#include <stdproperties.h>

#define DBG(x) find_player("vera")->catch_msg(x+"\n");

#define CHRUST_OBJ "/std/chrust"

static  object  *gFail;
object           paraliz;

void
create_object()
{
    ustaw_nazwe_glowna("wi�zka");
    ustaw_nazwe("chrust");

    set_long("Suche patyki, drobne ga��zki, kora i igliwie tworzy "+
             "t� wi�zk� chrustu.\n");
    add_prop(OBJ_I_WEIGHT, 800 + random(1200));
    add_prop(OBJ_I_VOLUME, query_prop(OBJ_I_WEIGHT) * 7);
}

void
complete()
{
    object ogn = clone_object("/std/ognisko.c");

    /*
     * switch(TP->query_acc_exp(4)) CO TO KURWA ZA query_acc_exp(4) ludzie kurwa kiedy wy si� nauczycie korzysta� z makr
     * I co, i kto� zmieni numerki(jak to si� sta�o po wprowadzeniu intelektu) i jest do bani bo p� muda przestaje
     * dzia�a� przez takie duperele... Nosz kurwa przecie� przy ka�dej, zmianie nie mo�na ca�ego muda przegl�da�...
     * Normalnie mam ochote kogo� powiesi� za jaja.
     * VERA NAUCZ SI� KORZYSTA� Z DEFIN�W. Kto� je do kurwy po co� zrobi�, mo�e po to, �eby by�o si� p�niej �atwiej
     * po�apa� o co chodzi�o autorowi? Pewnie tak.
     * Wiesz co tu si� sprawdza�o od wprowadzenia intelektu??? Pewnie, �e nie wiesz! Odwaga!!! Odwaga!
     * A teraz mi wyt�umacz co odwaga ma wsp�lnego z uk�adaniem ogniska?:)
     * Mam nadzieje, �e tym przyd�ugawym tekstem u�wiadomi�em Ci, �e definy s� dobre i nale�y
     * ich u�ywa�!!!!!!!!!!!! [KRUN]
     */
    switch(TP->query_acc_exp(SS_INT))
    {
        case 0..1000: write("Nie, przecie� ty kompletnie nie wiesz jak to zrobi�! "+
                      "Twoja misterna konstrukcja rozpada si� natychmiast po "+
                      "tym, jak odsuwasz od niej swe d�onie.\n");
                      saybb(QIMIE(this_player(), PL_CEL)+" uda�o si� u�o�y� jak�� "+
                      "konstrukcj� przypominaj�c� ognisko, lecz ta prawie natychmiast "+
                      "po tym rozpada si�.\n");
                      break;
        case 1001..2000: write("Uda�o ci si� u�o�y� co�, co swoim wygl�dem przypomina "+
                         "troch� ognisko.\n");
                      saybb(QIMIE(this_player(), PL_CEL)+" uda�o si� u�o�y� co�, "+
                         "co swoim wygl�dem przypomina troch� ognisko.\n");
                      //ogn->add_fuel( ({ this_object() }) );
                      ogn->move(environment(this_player()), 1);
                      break;
        default:      write("Uda�o ci si� u�o�y� niewielkie ognisko.\n");
                      saybb(QIMIE(this_player(), PL_CEL)+" uda�o si� u�o�y� "+
                      "niewielkie ognisko.\n");
                      //ogn->add_fuel( ({ this_object() }) );
                      ogn->move(environment(this_player()), 1);
                      break;
    }

    paraliz->remove_object();
    remove_object();
}

/**
 * Funkcja przerywaj�ca uk�adanie ogniska.
 */
public void przerwij_ukladanie()
{
    paraliz->remove_object();
    write("Przestajesz uk�ada� ognisko.\n");
    saybb(QCIMIE(TP, PL_MIA) + " przestaje uk�ada� ognisko.\n");
}

int
sprawdz_typ_lokacji()
{
    if(ENV(TP)->query_prop(ROOM_I_TYPE) & ROOM_SWAMP == ROOM_SWAMP ||
        ENV(TP)->query_prop(ROOM_I_TYPE) & ROOM_IN_CITY == ROOM_IN_CITY ||
        ENV(TP)->query_prop(ROOM_I_TYPE) & ROOM_TREE == ROOM_IN_AIR ||
        ENV(TP)->query_prop(ROOM_I_TYPE) & ROOM_UNDER_WATER == ROOM_UNDER_WATER ||
        ENV(TP)->query_prop(ROOM_I_TYPE) & ROOM_IN_WATER == ROOM_IN_WATER ||
        ENV(TP)->query_prop(ROOM_I_TYPE) & ROOM_TREE == ROOM_TREE ||
        ENV(TP)->query_prop(ROOM_I_TYPE) & ROOM_NORMAL == ROOM_NORMAL ||
        ENV(TP)->query_prop(ROOM_NO_FIRE))
    {
        return 1;
    }
    else
        return 0;
}

int
uloz(string str)
{
    if(str != "ognisko")
    {
        notify_fail(capitalize(query_verb())+" co?\n");
        return 0;
    }

    if(!HAS_FREE_HANDS(TP))
    {
        notify_fail("Musisz mie� obie d�onie wolne, aby m�c to zrobi�.\n");
        return 0;
    }

    if(TP->query_prop(SIT_LEZACY))
    {
        notify_fail("Najpierw wsta�!\n");
        return 0;
    }

    if(sprawdz_typ_lokacji())
    {
        notify_fail("To chyba nie jest odpowiednie miejsce do "+
            "uk�adania ogniska.\n");
        return 0;
    }


    setuid();
    seteuid(getuid());

    paraliz = clone_object("/std/paralyze.c");
    paraliz->set_standard_paralyze("uk�ada� ugnisko");
    paraliz->set_fail_go_message("Jeste� zaj�t"+TP->koncowka("y","a","e")+
             " uk�adaniem ogniska, musisz wpierw przesta� to robi�, by m�c "+
              "si� tam uda�.\n");
    paraliz->set_fail_message("Jeste� zaj�t"+TP->koncowka("y","a","e")+
             " uk�adaniem ogniska, musisz wpierw przesta� to robi�, by m�c "+
              "to uczyni�.\n");
    paraliz->set_stop_verb("przesta�");
    paraliz->set_stop_object(TO);
    paraliz->set_stop_fun("przerwij_ukladanie");

    paraliz->set_finish_object(this_object());
    paraliz->set_finish_fun("complete");

    paraliz->set_remove_time(10 + random(15));

    paraliz->move(this_player());

    write("Kl�kasz na ziemi i zaczynasz uk�ada� ognisko.\n");
    saybb(QCIMIE(this_player(), PL_MIA) + " kl�ka na ziemi i zaczyna " +
        "uk�ada� ognisko.\n");

    return 1;
}

int
divide_one_thing(object ob)
{
    if (ob->query_prop(OBJ_I_WEIGHT) < 1000)
        return 0;

    return 1;
}

int
divide_access(object ob)
{
    if ((environment(ob) == TP) && (function_exists("uloz", ob)) && (ob->query_short()))
        return 1;
    else
        return 0;
}

/*
 * Function name: divide_text
 * Description:   Here the divide message is written. You may redefine it if
 *		  you wish.
 * Arguments:     arr - Objects being divided
 *		  vb  - The verb player used to divide them
 */
void
divide_text(object *arr, string vb)
{
    string str;

    write("Dzielisz " + (str = COMPOSITE_DEAD(arr, PL_BIE)) + " na dwie cz^e^sci.\n");
    saybb(QCIMIE(TP, PL_MIA) + " dzieli " + str + " na dwie cz^e^sci.\n");
}

void
destruct_object()
{
    add_prop(TEMP_OBJ_ABOUT_TO_DESTRUCT, 1);
    set_alarm(0.5, 0.0, remove_object);
}

public int
podziel(string str)
{
    object 	*a, *foods;
    int		il;
    string str2, vb;

    notify_fail("Co chcesz podzieli^c?\n");

    /* This food has already been divided or already been tried, so we won't
     * have to test again.
     */
    if (query_prop(TEMP_OBJ_ABOUT_TO_DESTRUCT) || TP->query_prop(TEMP_STDFOOD_CHECKED))
        return 0;

    vb = query_verb();

    a = CMDPARSE_ONE_ITEM(str, "divide_one_thing", "divide_access");

    if (sizeof(a) > 0)
    {
        divide_text(a, vb);
        for (il = 0; il < sizeof(a); il++)
        {
            object ob1 = clone_object(CHRUST_OBJ), ob2 = clone_object(CHRUST_OBJ);
            string dop = a[il]->query_nazwa(PL_DOP), sdop = a[il]->singular_short(PL_DOP);
            int j;

            ob1->add_prop(OBJ_I_WEIGHT,a[il]->query_prop(OBJ_I_WEIGHT)/2);
            ob1->add_prop(OBJ_I_VOLUME,a[il]->query_prop(OBJ_I_VOLUME)/2);

            ob1->set_long(a[il]->long());

            ob2->add_prop(OBJ_I_WEIGHT,a[il]->query_prop(OBJ_I_WEIGHT)/2);
            ob2->add_prop(OBJ_I_VOLUME,a[il]->query_prop(OBJ_I_VOLUME)/2);

            ob2->set_long(a[il]->long());
            a[il]->destruct_object();
            ob1->move(TP, 1);
            ob2->move(TP, 1);
        }
        return 1;
    }
    else
    {
        notify_fail(capitalize(TO->short(PL_MIA)) + " " +
            koncowka("jest", "jest", "jest", "s^a", "s^a") + " zbyt ma" +
            koncowka("^ly", "^la", "^le", "li", "^le") + ", ^zeby " +
            koncowka("go", "j^a", "je", "ich", "je") + " podzieli^c.\n");

        return 0;
    }
}

void
init()
{
    ::init();
    add_action(uloz, "u��");
    add_action(uloz, "zbuduj");
    add_action(podziel, "podziel");
}

/**
 * Funkcja identyfikuj�ca obiekt jako chrust.
 *
 * @return 1 zawsze
 */
public int query_chrust() { return 1; }

/**
 * Funkcja identyfikuj�ca obiekt jako chrust.
 *
 * @return 1 zawsze
 */
public int is_chrust() { return 1; }