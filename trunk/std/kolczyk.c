/* Troche przerobek, i kolczyki gotowe! :) Lil 23.08.2005 
 *
 * 
 * Plik:	kolczyk_std.c
 * Autor:	Cerdin
 * Data:	27.12.03 (bardzo raaaano!)
 * Opis:	Standard kolczyka.
 */
inherit "/std/object";

#include <pl.h>
#include <macros.h>


void
create_kolczyk()
{
    set_long("Jest to matowy, niewielki kolczyk.\n");
}

nomask void
create_object()
{
    ustaw_nazwe("kolczyk");
    create_kolczyk();
}

int
query_kolczyk()
{
    return 1;
}

string
query_pomoc()
{   
    return koncowka("�w ", "Ow� ", "Owo ") + this_object()->short() +
	" mo�esz umie�ci� w miejscu, kt�re zosta�o przek�ute. " +
        "Je�li chcesz, zawsze mo�esz sprawdzi� te przek�ute miejsca. " +
	"Masz do dyspozycji tak�e kilka innych zabieg�w. Wypoleruj lub wyczy��, je�li " +
	"chcesz, by l�ni�" + this_object()->koncowka("", "a", "o") +
	" jak now" + this_object()->koncowka("y", "a", "e") + ". Je�li nie jeste� pew" +
	this_player()->koncowka("ien","na") + " umocowania - spr�buj go poci�gn��, " +
	"by to sprawdzi�, lub poprawi� jego po�o�enie. Dodatkowo mo�esz si� nim pobawi� " +
	"lub pochwali�.\n";
}
