/**
 * \file /std/room.c
 *
 * Oto obiekt lokacji. Powinien by� inheritowany przez wszystkie lokacje.
 */

#pragma save_binary
#pragma strict_types

inherit "/std/container";

inherit "/lib/sit.c";
inherit "/lib/event.c";

//Te dwa inherity w przysz�o�ci mo�e uda si� jako� usun��.
inherit "/lib/room/herbs.c";
inherit "/lib/room/drzewa.c";

#include <std.h>
#include <kraje.h>
#include <files.h>
#include <macros.h>
#include <ss_types.h>
#include <stdproperties.h>
//oba inkludy do snie�ek:
#include <mudtime.h>
#include <pogoda.h>

#include "/std/room/exits.c"
#include "/std/room/search.c"
#include "/std/room/description.c"
#include "/std/room/link.c"
#include "/std/room/move.c"
#include "/std/room/chrust.c"
//ekonomiczniej by�oby �eby w dyrektywie zamie�ci� chocia� jaki� if miesi�c zimowy, czy cokolwiek...no ale chuj
#include "/std/room/sniezka.c"

/* na potrzeby odczytywania lokacji */
int lokacja_jest_odczytana = 0;
string init_args = 0;

static string   obj_short; /* Przeniesione z /std/object.c */

static object   room_link_cont;	/* Linked container */
static object  *accept_here = ({ }); /* Items created here on roomcreation */

static mixed room_npcs = ({ }); // tablica sciezek do npcow
static mixed room_npcs_present = ({ }); // tablica npcow
static mixed room_npcs_functions = ({ }); // tablica funkcji

static mixed room_doors = ({});             // tablica �cie�ek do drzwi
static mixed room_doors_present = ({});     // tablica drzwi
static mixed room_doors_functions = ({});   // tablica funkcjii

static mixed room_windows = ({});           // tablica �cie�ek do okien
static mixed room_windows_present = ({});   // tablica okien
static mixed room_windows_functions = ({}); // tablica funkcji

static int enable_clone_npcs;

string start_place;

void enable_cloning_npcs();

void
add_npc(string path, int ile = 1, mixed fun = 0)
{
    if (path[-2..] == ".c")
        path = path[..-3];

    while (ile--)
    {
        NPCE_OBJECT->new_npc(path);
        room_npcs += ({ path });
        room_npcs_functions += ({ fun });
        room_npcs_present += ({ 0 });
    }
}

/**
 * Funkcja prosi SECURITY o zapisanie informacji o danym pomieszczeniu w pliku.
 */
public nomask void
zapisz_mnie()
{
    seteuid(0);
    SECURITY->save_room();
    seteuid(getuid(this_object()));
}

/**
 * Funkcja fizycznie zapisuje informacje o danym pomieszczeniu do pliku.
 */
public nomask int
zapisz_lokacje()
{
    seteuid(getuid(this_object()));
    save_object("/saves/rooms/" + implode(explode(MASTER + "/", "/")[1..], "_"));
    seteuid(getuid(this_object()));

    return 1;
}

/**
 * Funkcja odczytuje informacje o danym pomieszczeniu w pliku.
 *
 * @return 0 sukces
 * @return 1 pora�ka
 */
public int
odczytaj_lokacje()
{
    int ret;

    if (!((MASTER != ROOM_OBJECT) && (sizeof(explode(file_name(this_object()), "#")) == 1)))
        return 1;

    seteuid(getuid(this_object()));

    if (catch(ret = restore_object(file_name(this_object()))))
        return 1;

    return ret;
}

/**
 * Funkcja odpowiedzialna za zapis lokacji do pliku.
 */
public void
remove_room()
{
    int i = sizeof(room_npcs);
    if ((MASTER != ROOM_OBJECT) && (sizeof(explode(file_name(this_object()), "#")) == 1))
    {
        if (query_prop(ROOM_I_NIE_ZAPISUJ) == 0)
        {
            init_args = query_auto_load();
            if (stringp(init_args))
                init_args = implode(explode(init_args, ":")[1..], ":");

            zapisz_mnie();
        }
    }

    while (i--)
        NPCE_OBJECT->delete_npc(room_npcs[i]);
}

/*
 * Nazwa funkcji: remove_object
 * Opis         : Funkcja ta najpierw zapisuje lokacj�, a nast�pnie wywo�uje remove_object()
 *                z nadklasy.
 * Zwraca       : int - wynik remove_object() z nadklasy
 */

public int
remove_object()
{
    remove_room();
    return ::remove_object();
}

/*
 * Nazwa funkcji: czy_odczytana_lokacja
 * Opis         : Funkcja podaje czy lokacja zosta�a odczytana z pliku czy nie.
 *                Wizard nie ma �adnego interesu w nadpisywaniu tej funkcji, tak wi�c
 *                jest ona no mask.
 * Zwraca       : int 1/0 - nie odczytana/odczytana
 */
nomask int
czy_odczytana_lokacja()
{
    return lokacja_jest_odczytana;
}

/*
 * Function name: create_room
 * Description  : Constructor. You should redefine this function to create
 *                your own room.
 */
public void
create_room()
{
    remove_prop(ROOM_I_INSIDE);          /* Default room has heaven above */
    add_prop(ROOM_I_TYPE, ROOM_NORMAL);  /* Default is a room */

    set_polmrok_long("Panuje tu p�mrok, wi�c nie widzisz wszystkiego dok�adnie.\n");

    enable_cloning_npcs();
    
}

/*
 * Function name: create_container
 * Description  : Constructor. Since you may not redefine this function,
 *                you must define the function create_room() to create your
 *                room.
 */
nomask void
create_container()
{
    add_prop(ROOM_I_IS,    1);
    add_prop(ROOM_I_LIGHT, 1);
    add_prop(ROOM_I_HIDE, 15);

    room_link_cont = 0;

    seteuid(creator(this_object()));

    /* As service to the folks, we automatically call the function
     * enable_reset() to start resetting if the function reset_room() has
     * been redefined.
     */
    if (function_exists("reset_room", this_object()) != ROOM_OBJECT)
        enable_cloning_npcs();

    lokacja_jest_odczytana = !!(odczytaj_lokacje());

    set_event_time(300.0);
    set_long("@@dlugi_opis@@");

    create_room();

    //Resetujemy zio�a coby sobie ros�y �adnie.
    if (TO->query_ziola())
        TO->odnow_ziola();

    //A teraz drzewka, te� niech rosn�
    if (TO->query_lista_drzew())
        TO->odnow_drzewa();

    if (query_prop(ROOM_I_TYPE) & ROOM_FOREST == ROOM_FOREST && !query_prop(ROOM_I_ILOSC_CHRUSTU))
        add_prop(ROOM_I_ILOSC_CHRUSTU, random(10));

    POGODA_OBJECT->register_room(this_object());

    accept_here = all_inventory(this_object());

    if (!sizeof(accept_here))
        accept_here = ({ });

    if (!lokacja_jest_odczytana)
        init_arg(0);
    else if (SECURITY->query_auto_load_rooms() || query_prop(ROOM_I_ALWAYS_LOAD))
        init_arg(init_args);
    else
        init_arg(0);
}

/*
 * Function name: reset_room
 * Description  : This function should be redefined to make the room reset
 *                every half our or so. If you redefine it, you do not have
 *                to call enable_reset() since we call it as part of our
 *                service ;-) Note that this service is only valid for rooms.
 */
void
reset_room()
{
}

/*
 * Function name: reset_container
 * Description  : This function will reset the container. Since you may not
 *                redefine it, you must define the function reset_room() to
 *                make the room reset.
 */
nomask void
reset_container()
{
    //Odnawiamy chrust na lokacji
    if (query_prop(ROOM_I_TYPE) & ROOM_FOREST == ROOM_FOREST && !query_prop(ROOM_I_ILOSC_CHRUSTU))
        add_prop(ROOM_I_ILOSC_CHRUSTU, random(5));

    //Odnawiamy zio�a kt�re b�d� znajdowa� si� na lokacji
    if (TO->query_ziola())
        TO->odnow_ziola();

    //Odnawiamy drzewa na lokacji
    if (TO->query_lista_drzew())
        TO->odnow_drzewa();

    //Usuwamy info o ognisku
    TO->remove_prop(ROOM_WAS_FIRE);

    if (enable_clone_objects)
    {
        string res;
        mixed fun;

        //dla drzwi
        for (int i = 0; i < sizeof(room_doors); i++)
        {
            if (room_doors_present[i])
                continue;

            fun = room_doors_functions[i];

            if (fun && !((stringp(fun) && call_self(fun)) || (functionp(fun) && fun())))
                continue;

            if ((res = catch(room_doors_present[i] = clone_object(room_doors[i]))))
            {
                write("\nB��d przy tworzeniu drzwi\"" + room_doors[i] + "\".\n"+res+"\n");
                continue;
            }

            room_doors_present[i]->move(this_object(), 0);
            room_doors_present[i]->init_arg(0);
            room_doors_present[i]->start_me();
        }

        //dla okien
        for (int i = 0; i < sizeof(room_windows); i++)
        {
            if (room_windows_present[i])
                continue;

            fun = room_windows_functions[i];

            if (fun && !((stringp(fun) && call_self(fun)) || (functionp(fun) && fun())))
                continue;

            if ((res = catch(room_windows_present[i] = clone_object(room_windows[i]))))
            {
                write("\nB��d przy tworzeniu drzwi\"" + room_windows[i] + "\".\n"+res+"\n");
                continue;
            }

            room_windows_present[i]->move(this_object(), 0);
            room_windows_present[i]->init_arg(0);
            room_windows_present[i]->start_me();
        }
    }

    if (enable_clone_npcs)
    {
        object ob;
        string res;
        mixed fun;
        int i = sizeof(room_npcs);

        while (i--)
        {
            if (!NPCE_OBJECT->get_free_slots(room_npcs[i]))
                continue;

            if (objectp(room_npcs_present[i]))
                continue;

            fun = room_npcs_functions[i];

            if (fun && !((stringp(fun) && call_self(fun)) || (functionp(fun) && fun())))
                continue;

            if ((res = catch(room_npcs_present[i] = clone_object(room_npcs[i]))))
            {
                write("\nB��d przy tworzeniu npca\"" + room_npcs[i] + "\".\n"+res+"\n");
                continue;
            }

            //Rejestrujemy npc'a
            NPCE_OBJECT->register_npc(room_npcs_present[i]);

            //przeszukajmy okolic� w poszukiwaniu swego cia�a i
            //zabierzmy je, je�li si� respawnujemy! V.

            filter(filter(all_inventory(TO), &->query_corpse()), &operator(==)(room_npcs_present[i]->query_nonmet_name(),) @
                &->query_nonmet_name())->remove_object();

            room_npcs_present[i]->move(this_object(), 1);
            room_npcs_present[i]->init_arg(0);
            room_npcs_present[i]->start_me();
        }
    }

    reset_room();

    if (!sizeof(accept_here))
        accept_here = ({ });
    else
        accept_here = accept_here - ({ 0 });
}


/**
 * @return Zwraca kr�tki opis lokacji.
 * @see query_short()
 *
 * @note
 *      Dobrze by�oby wycofa� si� z jednej z tych funkcji albo short albo query_short, nie ma sensu
 *      trzyma� dw�ch funkcji robi�cych to samo, zw�aszcza, �e powoli ca�kowicie wycofujemy si� z funkcji
 *      query_short w obiektach standardowych - proponuje zachowanie tej funkcji [Krun]
 */
public string
short()
{
//     if (functionp(obj_short)) {
//         return &obj_short();
//     }
    
    return obj_short;
}

/**
 * @return Zwraca kr�tki opis lokacji.
 * @see short()
 *
 * @note
 *      Dobrze by�oby wycofa� si� z jednej z tych funkcji albo short albo query_short, nie ma sensu
 *      trzyma� dw�ch funkcji robi�cych to samo, zw�aszcza, �e powoli ca�kowicie wycofujemy si� z funkcji
 *      query_short w obiektach standardowych - proponuje zachowanie funkcji short() [Krun]
 */
public string
query_short()
{
    return obj_short;
}

/**
 * Funkcj� t� ustawamy kr�tki opis lokacji.
 *
 * @param str   Kr�tki opis lokacji (string lub funkcja)
 */
public void
set_short(mixed str)
{
    if (stringp(str)) {
        
        if (strlen(str) && str[-1..-1] == "\n")
            str = str[..-2];
        if (strlen(str) && str[-1..-1] == ".")
            str = str[..-2];

        obj_short = capitalize(str);
    } else if (functionp(str)) {
        obj_short = str;
    }
}

/*
 * Function name: clone_here
 * Description  : The behaviour of this function is exactly the same as the
 *                efun clone_object(). It clones the 'file' and returns the
 *                objectpointer to that object. However, it will also add
 *                the object to a list of items that 'belongs' in this room.
 *                This means that the presence of this object in the room will
 *                not prevent the room from being cleaned with clean_up().
 * Arguments    : string file - the path to the item to clone.
 * Returns      : object - the objectpointer to the clone.
 */
public object
clone_here(string file)
{
    object ob;

    ob = clone_object(file);
    accept_here += ({ ob });
    return ob;
}

/*
 * Function name: query_cloned_here
 * Description  : Returns all objects that have been cloned in this room with
 *                clone_here() or registered with add_accepted_here().
 * Returns      : object * - the list of objects.
 */
public object *
query_cloned_here()
{
    return secure_var(accept_here);
}

/**
 * Funkcja ta dodaje przedmiot jako akceptowalny przy wykonywaniu clean_up.
 * Je�li podany tu przedmiot b�dzie na lokacji i wywo�ana zostanie komenda clean_up to zostanie ona wykonana
 * a gdyby przedmiot nie zosta� tak dodany clean_up zosta�o by wsztrzymane.
 *
 * @param ob    Dodawany obiekt.
 */
void
add_accepted_here(object ob)
{
    accept_here += ({ ob });
}

/**
 * @return Zwraca poziom o�wietlenia pomieszczenia.
 */
nomask int
light()
{
    int li;

    li = query_prop(ROOM_I_LIGHT);
    if (objectp(room_link_cont))
    {
        if ((environment(room_link_cont)) &&
            (room_link_cont->query_prop(CONT_I_TRANSP) ||
            room_link_cont->query_prop(CONT_I_ATTACH) ||
            !room_link_cont->query_prop(CONT_I_CLOSED)))
        {
            li += (environment(room_link_cont))->query_prop(OBJ_I_LIGHT);
        }
    }
    return query_internal_light() + li;
}

/*
 * Function name: set_container
 * Description:   Sets the container for which the room represents the inside
 * Arguments:	  ob: The container object
 */
public void
set_container(object ob)
{
    room_link_cont = ob;
}

/**
 * Funkcja maskuj�ca funkcje set_room z \file /std/container.c kt�ra nie jest prawid�owa dla room�w.
 */
public nomask void
set_room(mixed room)
{
}

/*
 * Function name: update_internal
 * Description:   Updates the light, weight and volume of things inside
 *                also updates a possible connected container.
 * Arguments:     l: Light diff.
 *		  w: Weight diff. (Ignored)
 *		  v: Volume diff. (Ignored)
 */
public void
update_internal(int l, int w, int v, int a)
{
    ::update_internal(l, w, v, a);

    if (l)
        all_inventory()->notify_light_change(l);

    if (room_link_cont)
        room_link_cont->update_internal(l, w, v, a);
}

/**
 * Ta funkcja usuwa pomieszczenie je�li nic w niem nie ma.
 *
 * @note
 *      Ta funkcja u nas jest nieu�ywana, ale prawdopodbnie w nied�ugiej
 *      przys�o�ci powr�cimy do jej stosowania - w przypadku kiedy uda mi si�
 *      przerobi� funkcje znajdz_sciezke, ale to mo�e chwile zaj�� z powodu
 *      nat�oku innych obowi�zk�w.
 *
 * @note
 *      Funkcja pozwala zachowa� wiele pami�ci, a przytym je�li j� dobrze
 *      napisa� da�oby si� odpowiednio zapisywa� i odczytywa� lokacje, tak, a�eby po wej�ciu i wyj�ciu gracza
 *      mimo i� lokacja si� odnowi�a wci�� wygl�da�a tak samo.
 *
 * @return <ul><li> 1 - m�wi o tym, i� chcieliby�my aby funkcj� wywo�a� ponownie </li>
 *             <li> 0 - �e funkcja nie powinna by� ponownie wywo�ywana </li></ul>
 */
public int
clean_up()
{
    /* Do not destroy the room object. */
    if (MASTER == ROOM_OBJECT)
        return 0;

    if (!query_prop(ROOM_I_NO_CLEANUP) && !sizeof(all_inventory(this_object()) - accept_here))
        remove_object();

    return 1;
}

/**
 * Funkcja klonuje obiekt w podanej ilo�ci i wy�wietla komunikat.
 *
 * @note
 *      Je�li jako ilo�� podamy liczb� wi�ksz� ni� 1 to zwr�cona zostanie tablica, w przeciwnym przypadku
 *      zwr�cony zostanie pojedynczy obiekt.
 *
 * @param file  Plik klonowanego obiektu
 * @param num   Ilo�� obiekt�w jaka zostanie sklonowana
 * @param mess  Wiadomo�� do pokazania podczas klonowania obiektu.
 *
 * @return <ul><li>Sklonowany obiekt</li>
 *             <li>Tablice ze sklonowanymi obiektami</li></ul>
 */
varargs mixed
room_add_object(string file, int num = 1, string mess = 0)
{
    num = MAX(1, num);

    seteuid(getuid());

    //Dla jednego obiektu zwracamy inn� warto�� wi�c nie ma sensu definiowa� wszystkich zmiennych
    if (num == 1)
    {
        object ob;

        ob = clone_object(file);

        if (stringp(mess))
        {
            ob->move(TO, 1);
            tell_roombb(TO, mess, ({}), ob);
        }
        else if (living(ob))
            ob->move_living("xx", TO);
        else
            ob->move(TO, 1);

        return ob;
    }
    else
    {
        object *ob, *lv;

// NOTE: To b�dzie mo�na zastosowa� jak naprawie dwuelementowego allocata [Krun]
//         ob = allocate(num, file);
//         ob = map(ob, &clone_object());

        for (int i = 0 ; i < num ; i++)
            ob += ({clone_object(file)});

        if (stringp(mess))
        {
            tell_roombb(TO, mess, ({}), ob);
            ob->move_object(TO, 1);
        }

        lv = FILTER_LIVE(ob);

        (ob - lv)->move(TO, 1);
        lv->move_living("xx", TO);

        return ob;
    }
}

/**
 * Dzi�ki tej funkcji mo�emy doda� drzwi na lokacje.
 * @param path - �cie�ka do drzwi.
 * @param fun  - funkcja odpowiadaj�ca za stwierdzenie czy drzwi maj� zosta� dodane czy te� nie.
 */
public void
add_door(string path, mixed fun = 0)
{
    if (!pointerp(room_doors) || !pointerp(room_doors_functions))
    {
        room_doors              = ({});
        room_doors_functions    = ({});
    }

    room_doors              += ({ path });
    room_doors_functions    += ({ fun });
    room_doors_present      += ({ 0 });
}

/**
 * Dzi�ki tej funkcji mo�emy doda� drzwi na lokacje.
 * @param path  - �cie�ka do okna
 * @param ilosc - ilo�� okien.
 * @param fun   - funkcja odpowiadaj�ca za stwierdzenie czy drzwi mog� zosta� dodane czy te� nie.
 */
public void
add_window(string path, int ilosc = 1, mixed fun = 0)
{
    if (!pointerp(room_windows) || !pointerp(room_windows_functions))
    {
        room_windows            = ({});
        room_windows_functions  = ({});
    }

    while (ilosc-- > 0)
    {
        room_windows            += ({ path });
        room_windows_functions  += ({ fun });
        room_windows_present    += ({ 0 });
    }
}

/**
 * @return Nazw� domeny w kt�rej le�y dana lokacja.
 */
nomask string
query_domain()
{
    /* Normal room. */
    if (wildmatch("/d/*", file_name(this_object())))
        return explode(file_name(this_object()), "/")[2];

    /* Link-room. */
    /* Dodalem test query_link_master(), bo jak 0 wchodzilo do wildmatch
     * to byl runtime...
     */
    if (query_link_master() && wildmatch("/d/*", query_link_master()))
        return explode(query_link_master(), "/")[2];

    /* This shouldn't happen. */
    return BACKBONE_UID;
}

/**
 * @return Nazwe kraju w jakim le�y dana lokacja.
 */
nomask string
query_kraj()
{
    return "Redania";
}

/**
 * @return Identyfikator kraju w jakim le�y dana lokacja.
 */
nomask int
query_kraj_id()
{
    return KR_REDANIA;
}

/**
 * Funkcja odpowiedzialna za wy�wietlanie si� w roomie event�w.
 */
public void show_event(mixed event)
{
    if (event[0])
        tell_roombb(this_object(), event[0], 0, 0, 1);
}

/**
 *  W��czamy klonowanie npc�w.
 */
void
enable_cloning_npcs()
{
    enable_clone_npcs = 1;
    reset();
}

/**
 * Ustawiamy jakie miejsce startowe b�dzie ustawia� ta lokacja.
 * 0 je�li ma nie ustawia�.
 */
public void
set_start_place(string s)
{
    start_place = s;
}

/**
 * @return Lokacja startowa ustawiana przez t� lokacje.
 */
string
query_start_place()
{
    return start_place;
}

/**
 * Sprawdza, czy dany obiekt mo�e wej�� do tej lokacji.
 *
 * @param ob    Sprawdzany obiekt
 *
 * @return <ul><li> 1 - Nie mo�e wej��</li>
 *             <li> 0 - Mo�e wej�� </li></ul>
 */
public int
prevent_enter(object ob)
{
    if (start_place)
        ob->set_temp_start_location(start_place);

    return ob->prevent_enter_env(TO);
}

/**
 * Wywo�ywane kiedy jaki� obiekt opuszcza lokacje.
 *
 * @param ob    Opuszczaj�cy obiekt
 * @param dest  Docelowy obiekt.
 */
void
leave_inv(object ob, object dest)
{
    //Zmieni�em kolejno�� a pozatym doda�em funkcje czyszcz�c� found_herbs'
    //z zi� ju� znalezionych.

    if (ob->query_herb())
        TO->clean_found_herbs_from_ob(ob);

    leave_inv_sit(ob);
    ::leave_inv(ob,dest);
}

/**
 * Sprawdza, czy dany obiekt mo�e wyj�� z lokacji.
 *
 * @param ob    Sprawdzany obiekt
 *
 * @return <ul><li> 1 - Nie mo�e wej��</li>
 *             <li> 0 - Mo�e wej�� </li></ul>
 */
public int
prevent_leave(object ob)
{
    //zwalniamy slot w rejestratorze.
    //i to powinno kruca pom�c na niepojawianie si� npc�w na lokacjach... hiphip hurrra
    if (ob->query_npc())
        NPCE_OBJECT->remove_npc(ob);

    return ob->prevent_leave_env(TO);
}

/**
 * Wczytujemy zapisane dane.
 */
public string
init_arg(string arg)
{
    set_alarm(0.1, 0.0, &enable_cloning_npcs());
    return ::init_arg(arg);
}

/**
 * Funkcja inicjalizuj�ca dzia�ania jakie mo�na wykona� w standardowym roomie.
 */
void
init()
{
    init_dirs();
    init_chrust();
    
    if(pora_roku() == MT_ZIMA)//�nie�ki dodajemy tylko w zim�.
        init_sniezki();
    
    init_sit();
    ::init();
}

/**
 * Wywo�ywane kiedy wiz sprawdza statystyki obiektu.
 */
string
stat_object()
{
    string str;
    int type;

    str = ::stat_object() + "Pomieszczenie: ";

    if (query_prop(ROOM_I_INSIDE))
            str += "wewn�trz, ";
    else
        str += "na zewn�trz, ";

    type = query_prop(ROOM_I_TYPE);
    str += " ";
    string *typy = ({});

    if (type & ROOM_NORMAL == ROOM_NORMAL)
        typy += ({"zwyk�y"});
    if (type & ROOM_IN_WATER == ROOM_IN_WATER)
        typy += ({"w wodzie"});
    if (type & ROOM_UNDER_WATER == ROOM_UNDER_WATER)
        typy += ({"pod wod�"});
    if (type & ROOM_IN_AIR == ROOM_IN_AIR)
        typy += ({"w powietrzu"});
    if (type & ROOM_BEACH == ROOM_BEACH)
        typy += ({"pla�a"});
    if (type & ROOM_DESERT == ROOM_DESERT)
        typy += ({"pustynia"});
    if (type & ROOM_FOREST == ROOM_FOREST)
        typy += ({"las"});
    if (type & ROOM_FIELD == ROOM_FIELD)
        typy += ({"pole"});
    if (type & ROOM_CAVE == ROOM_CAVE)
        typy += ({"jaskinia"});
    if (type & ROOM_SWAMP == ROOM_SWAMP)
        typy += ({"bagno"});
    if (type & ROOM_MOUNTAIN == ROOM_MOUNTAIN)
        typy += ({"g�ry"});
    if (type & ROOM_TRACT == ROOM_TRACT)
        typy += ({"trakt"});
    if (type & ROOM_IN_CITY == ROOM_IN_CITY)
        typy += ({"miasto"});
    if (type & ROOM_TREE == ROOM_TREE)
        typy += ({"drzewo"});

    str += COMPOSITE_WORDS(typy) + "\n";
    str += "Kraj: " + query_kraj() + ", Domena: " + query_domain() + "\n";

    return str;
}

/**
 * Funkcja identyfikuj�ca obiekt jako pomieszczenie
 *
 * @return zawsze 1
 */
public int
is_room()
{
    return 1;
}
