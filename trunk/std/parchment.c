/**
 * Pergaminy wszelkiego rodzaju
 * Autor: Avard
 * Data : 16 lipca 2010
 */

inherit "/std/object";

#include <object_types.h>
#include <config.h>
#include <cmdparse.h>
#include <files.h>
#include <macros.h>
#include <stdproperties.h>
#include <pl.h>

//--- Variables ---//

string textOnParchment = "";
int writeable = 1;
int rolled = 0;

//--- Prototypes ---//

int read_parchment(string str);
int write_parchment(string str);
int eat_parchment(string str);
int tear_parchment(string str);
int roll_parchment(string str);
int unroll_parchment(string str);
string long_desc();

//------------------//

void
create_parchment()
{
    ustaw_nazwe("pergamin");
    dodaj_przym("zwyk^ly", "zwykli");
    set_long(&long_desc());
}

nomask void
create_object()
{
    add_prop(OBJ_I_WEIGHT, 10);
    add_prop(OBJ_I_VOLUME, 10);

    set_type(O_ZWOJE);

    create_parchment();
}

string long_desc()
{
    if(rolled) return "Zwini�ty pergamin.\n";
    else return writeable ? "Czysty pergamin.\n" : 
        "Na tym pergaminie jest co^s zapisane.\n";
}

public void
init()
{
    ::init();

    add_action(read_parchment, "przeczytaj");
    add_action(write_parchment, "zapisz");
    add_action(write_parchment, "napisz");
    add_action(eat_parchment, "zjedz");
    add_action(tear_parchment, "podrzyj");
    add_action(tear_parchment, "zniszcz");
    add_action(roll_parchment, "zwin");
    add_action(unroll_parchment, "rozwi�");
}

int
read_parchment(string str)
{
    object parchment;

    notify_fail("Przeczytaj co?\n");

    if (!str) return 0;
    if (!strlen(str)) return 0;

    if(!parse_command(str,environment(TP),"%o:" + PL_BIE, parchment))
        return 0;

    if(writeable)
    {
        write("Pr^obujesz czyta^c "+TO->short(TO, PL_BIE)+
            ", ale okazuje si^e, ^ze jest pust"+
            TO->koncowka("y","a","e")+"!\n");
        saybb(QCIMIE(TP, PL_MIA) + " czyta " + TO->short(TO, PL_MIA)+".\n");
        return 1;
    }
    
    if(rolled)
    {
        write("Nie mo�esz tego zrobi�, najpierw rozwi� "+TO->short(TO, PL_BIE)+".\n");
        return 1;
    }

    write(textOnParchment);
    saybb(QCIMIE(TP, PL_MIA) + " czyta " + TO->short(TO, PL_MIA)+".\n");
    return 1;
}

int
write_parchment(string str)
{
    object parchment;
    if(query_verb() ~= "napisz")
        notify_fail("Napisz na czym?\n");
    else
        notify_fail("Zapisz co?\n");

    if (!str) return 0;
    if (!strlen(str)) return 0;


    if(query_verb() ~= "napisz")
    {
        if(!parse_command(str,environment(TP),"'na' %o:" + PL_MIE, parchment))
            return 0;
    }
    else
    {
        if(!parse_command(str,environment(TP),"%o:" + PL_BIE, parchment))
            return 0;
    }
    
    if(rolled)
    {
        write("Nie mo�esz tego zrobi�, najpierw rozwi� "+TO->short(TO, PL_BIE)+".\n");
        return 1;
    }
    
    if(!writeable)
    {
        write("Co^s ju^z jest zapisane na "+ TO->short(TO, PL_MIE)+".\n");
        return 1;
    }

    write("Zaczynasz pisa^c na "+ TO->short(TO, PL_MIE)+".\n");
    saybb(QCIMIE(TP, PL_MIA) + " zaczyna pisa^c na "+
        TO->short(TO, PL_MIE)+".\n");
    TP->add_prop(LIVE_S_EXTRA_SHORT, " pisz^ac" +
        this_player()->koncowka("y", "a", "e") + " na " +
        TO->short(TO, PL_DOP));

    seteuid(getuid());

    clone_object(EDITOR_OBJECT)->edit("done_editing", "", 0, 0);
    return 1;
}

int
eat_parchment(string str)
{
    object parchment;
    notify_fail("Zjedz co?\n");

    if (!str) return 0;
    if (!strlen(str)) return 0;


    if(!parse_command(str,environment(TP),"%o:" + PL_BIE, parchment))
        return 0;

    write("Zjadasz "+ TO->short(TO, PL_BIE)+".\n");
    saybb(QCIMIE(TP, PL_MIA) + " zjada "+ TO->short(TO, PL_BIE)+".\n");

    TO->remove_object();
    return 1;
}

int
tear_parchment(string str)
{
    object parchment;
    if(query_verb() ~= "podrzyj")
        notify_fail("Podrzyj co?\n");
    else
        notify_fail("Zniszcz co?\n");

    if (!str) return 0;
    if (!strlen(str)) return 0;


    if(!parse_command(str,environment(TP),"%o:" + PL_BIE, parchment))
        return 0;

    write("Drzesz "+ TO->short(TO, PL_BIE)+" na ma^le kawa^leczki.\n");
    saybb(QCIMIE(TP, PL_MIA) + " rozdziera "+ TO->short(TO, PL_BIE)+
        " na ma^le kawa^leczki.\n");

    TO->remove_object();
    return 1;
}

int
roll_parchment(string str)
{
    object parchment;
    notify_fail("Zwi� co?");

    if (!str) return 0;
    if (!strlen(str)) return 0;

    if(rolled)
    {
        write("Ale "+TO->short(TO,PL_BIE)+" jest ju� zwini�ty.\n");
        return 1;
    }

    if(!parse_command(str,environment(TP),"%o:" + PL_BIE, parchment))
        return 0;

    write("Zwijasz "+ TO->short(TO, PL_BIE)+" w rulonik i zwi�zujesz sznureczkiem.\n");
    saybb(QCIMIE(TP, PL_MIA) + " zwija "+ TO->short(TO, PL_BIE)+
        " w rulonik i zwi�zuje sznureczkiem.\n");
        
    rolled = 1;
    return 1;
}

int
unroll_parchment(string str)
{
    object parchment;
    notify_fail("Rozwi� co?");

    if(!rolled)
    {
        write("Ale "+TO->short(TO,PL_BIE)+" jest ju� rozwini�ty.\n");
        return 1;
    }

    if(!parse_command(str,environment(TP),"%o:" + PL_BIE, parchment))
        return 0;

    write("Rozwi�zujesz sznureczek i rozwijasz "+ TO->short(TO, PL_BIE)+".\n");
    saybb(QCIMIE(TP, PL_MIA) + " rozwi�zuje sznureczek i rozwija "
        + TO->short(TO, PL_BIE)+".\n");

    rolled = 0;
    return 1;
}

/*
 * Function name: done_editing
 * Description  : When the player is done editing the text, this function
 *                will be called with the message as parameter.
 * Arguments    : string message - text typed by the player.
 * Returns      : int - 1/0 - true if text was added.
 */

public nomask int
done_editing(string message)
{
    this_player()->remove_prop(LIVE_S_EXTRA_SHORT);

    textOnParchment = message;
	if(textOnParchment == "") 
	{
		write("Przerywasz pisanie na "+ TO->short(TO, PL_MIE)+".\n");
		saybb(QCIMIE(TP, PL_MIA) + " przerywa pisanie na "+
			TO->short(TO, PL_MIE)+".\n");
	}
	else
	{
		write("Ko^nczysz pisa^c na "+ TO->short(TO, PL_MIE)+".\n");
		saybb(QCIMIE(TP, PL_MIA) + " ko^nczy pisanie na "+
			TO->short(TO, PL_MIE)+".\n");
		writeable = 0;
	}
    return 1;
}

/* Pomoc przydaloby sie napisa� troch� �adniej. ;) TODO FIXME*/
string query_pomoc()
{
    return 
    "Mo�esz:\n"+
    "- zapisa� pergamin / pisa� na pergaminie \n"+
    "- przeczyta� pergamin \n"+
    "- zwin�� pergamin oraz go rozwin�� \n"+
    "- podrze� / zniszczy� lub zje��� pergamin \n";
}
