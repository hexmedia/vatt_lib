/**
 * Obiekt rz�dz�cy walk�.
 *
 * W obiekcie tym znajduj� sie wszystkie funkcje odpowiedzialne za interakcje mi�dzy
 * walcz�cymi.
 * Jego g��wn� odpowiedzialno�ci� jest wywo�anie zwarcia, powiadomienie obiekt�w o tym,
 * �e i komu maj� zacz�� zadawa� ciosy.
 *
 * Obiekt ustawia tak�e w graczach modyfikatory odpowiadaj�ce za plusy i minusy dotycz�ce
 * konfiguracji walki.
 * 
 * @FIXME:
 *  - W�a�nie zauwa�y�em, �e obiekt mo�e gubi� wrog�w w momencie gdy gracz si� przelogouje
 *    @SOLUTION: zamiast OB_NAME stosowa� query_real_name
 */

/**
 * Kr�tkie info dla mnie: Na pewno trzeba to zrobi� tak, �eby gracz m�g� okre�li� z kim walczy,
 * przy czym pojawia si� problem, �e mamy 4 graczy na polu walki, wszyscy walcz�, i �aden nie
 * walczy ze sob� na wzajem, nie wiem czy to dobry pomys�, ale aby unikn�� takiej sytuacji
 * proponuje aby atakuj�c jakiego� gracza od razu i on nam zaczyna� oddawa�, ewentualnie,
 * mo�na zrobi� tak, �eby by�y du�e minusy do ataku na osoby nie atakuj�ce nas, je�li kto�
 * akurat nas atakuje. Kwestia jeszcze do przedyskutowania, narazie wybieram rozwi�zanie
 * pierwsze, ale jest ono �atwe do zmiany.
 * 
 */

inherit "/std/room.c";

#define FT_CLASH_TIME       0
#define FT_FIGHTERS         1
#define FT_CLASH_PARALYZE   2

#define CBO_LOCATION(x)     file_name(ENV(x))

#define DBG(x) find_player("avard")->catch_msg(x + "\n"); find_player("cairell")->catch_msg(x + "\n");

#include <combat.h>
#include <macros.h>
#include <colors.h>
#include <composite.h>
#include <filter_funs.h>

static mapping fights = ([]);
static mapping enemies = ([]);
static object *fighters = ({});
static mapping player_enemy = ([]);

string walczacy;


// void do_clash(mixed index, int i);
void check_clash();

//@todo: t� funckj� trzeba sprawdzi� czy jest wywo�ywana z takimi parametrami jak potrzeba
//       bo co� mi si� tak wydaje, �e nie jest
static void stroke_in_back(object player, string m_index, int index);

static void stop_user_fights(object player, int fight);

public void create_room() 
{
    set_long("Pok�j zarz�dzania walkami.\n");
    set_short("Pomieszczenie zarz�dzania walk�. Mo�esz tu spojrze� na walki lub na samych walcz�cych.\n");
    //wyj�cie na korytarz garstangu
    add_exit("/d/Standard/wiz/garstang/korytarz2", ({"u","do g�ry na korytarz"}));
    
    add_item("walcz�cych", "@@walczacy@@");
    add_item("walki", "@@walki@@");

    check_clash();
}

string
walczacy() {
    if (sizeof(fighters)) {
        int i;
        string str = "Aktualnie na mudzie walcz�: \n\t";
        
        for (i = 0 ; i < sizeof(fighters) ; i++) {
            
            if (i != 0) {
                if (i % 5 == 0) {
                    str += "\n\t";
                } else {
                    str += ", ";
                }
            }
            
            str += fighters[i]->query_name();
        }
        
        if (i % 5 != 1) {
            str += "\n";
        }
        
        return str;
    } else {
        return "Nikt aktualnie nie walczy.\n";
    }
}

string
walki() {
    if (m_sizeof(fights)) {    
        string str = "Aktualnie prowadzone s� takie walki: \n";
        
        string *indexes = m_indexes(fights);
        for (int i = 0 ; i < sizeof(indexes) ; i++) {
            str += "\tNa lokacji: " + indexes[i] + "\n";
            for (int j = 0 ; j < sizeof(fights[indexes[i]]) ; j++) {
                str += "\t\tpomi�dzy: " + COMPOSITE_WORDS2(map(fights[indexes[i]][j][FT_FIGHTERS], &->query_name()), " i ") + "\n";
            }
        }
        
        return str;
    } else {
        return "Nikt aktualnie nie walczy.\n";
    }
}

/**
 * Funkcja dodaj�ca walk� i sprawdzaj�ca warunki tej walki.
 *
 * @WARNING Funkcja przyjmuje, �e wszystkie walki s� poprawne,
 *          nie sprawdza czy wszystko jest ok,
 *          sprawdzenie musi odby� si� w innym miejscu
 * 
 * @param object attacker - atakuj�cy
 * @param object victim - ofiara ataku :)
 * @param object attacker_location - lokacja gdzie walcz�
 * 
 * @return <ul>
 *  <li>0 - uda�o si� doda�</li>
 *  <li>1 - gracze nie s� na tej samej lokacji</li>
 *  <li>2 - nie mo�e bo walczy, a pr�buje z osob� z kt�r� nie walczy</li>
 *  <li>3 - walka nie zosta�a rozpocz�ta, poniewa� gracze pr�buj� po��czy� si� z tego samego adresu ip</li>
 * </ul>
 * 
 * @todo <ul>
 * <li> Trzeba doda� raportowanie je�li gracze s� z tego samego adresu ip (jaki� system raportowania si� przyda)</li>
 * </ul>
 */
public int 
add_fight(object attacker, object victim, object attacker_location = 0)
{
DBG("add_fight attacker-"+file_name(attacker)+" victim:"+file_name(victim)+(attacker_location ? "attacker_loc:"+file_name(attacker_location) : ""));
    string location;
    
    //Usuwamy graczy, kt�rzy si� wylogowali, etc.
    fighters -= ({ 0 });
    
     if (query_ip_number(attacker) == query_ip_number(victim) && !attacker->query_wiz_level()) {
         //@todo tu raporty
         attacker->catch_msg("Pr�ba oszustwa zosta�a zg�oszona.\n"); //nieprawda ;p TODO
         return 3;
     }
    
    if (attacker_location == 0) {
        attacker_location = ENV(attacker);
    }
    
    location = file_name(attacker_location);
    
    if (file_name(ENV(victim)) != location) {
        return 1;
    }

    //Obaj gracze walcz� lub atakuj�cy walczy a ofiara nie
    if (member_array(victim, fighters) != -1 && member_array(attacker, fighters) != -1 || member_array(attacker, fighters) != -1) {
DBG("add_fight: dziwnym jest1. victim jest w tablicy fighters && attacker jest w tab. fighters lub attacker jest...");
        return 2;
    //Walczy tylko ofiara
    } else if (member_array(victim, fighters) != -1) {
DBG("add_fight: dziwnym jest2 - victim jest w tablicy fighters");
        for (int i = 0 ; i < sizeof(fights[location][FT_FIGHTERS]) ; i++) {
            if(member_array(victim, fights[location][i][FT_FIGHTERS]) != -1) {
                fights[location][i][FT_FIGHTERS] += ({attacker});
                fighters += ({attacker});
            }
        }
    //�aden z graczy nie walczy
    } else {
        fighters += ({victim, attacker});
        
        if (!is_mapping_index(location, fights)) {
            fights[location] = ({ });
        }
        
        fights[location] += ({ ({ time() , ({ attacker, victim }), 0 }) });
    }

    player_enemy[attacker->query_real_name()] = victim;
    
    if (!player_enemy[victim->query_real_name()]) {
DBG("!!!!DODAJE DO player_enemy["+victim->query_real_name()+"] = obj "+file_name(attacker));
        player_enemy[victim->query_real_name()] = attacker;
    }
    
    return 0;
}

/**
 * Sygna� wywo�ywany przez opuszczenie przez gracza lokacji.
 * 
 * 
 * @param object player - gracz kt�ry wychodzi
 * @param object from   - sk�d gracz wychodzi
 * @param object to     - dok�d gracz idzie
 * 
 */
public int
exit_location(object player, object from, object to)
{
    string from_str, to_str;
    object *inv_to;
    
    from_str = file_name(from);
    to_str   = file_name(to);
    
    //Najpierw musimy wyprowadzi� gracza z walk, w kt�rych bierze udzia�
    for (int i = 0 ; i < sizeof(fights[from_str]) ; i++) {
        if (member_array(player, fights[from_str][i][FT_FIGHTERS]) != -1) {
            stroke_in_back( player, from_str, i );
            fights[from_str][i][FT_FIGHTERS] -= ({ player });
        }
    }
    
    fighters -= ({ player });
    
    inv_to = all_inventory(to);
#if 0
    //Nie wiem czy nie fajnie by�oby doda� tutaj opcje czy gracz chce automatycznie wznawia�
    //walki, kt�re z jakiego� powodu zosta�y przerwane.
    for (int i = 0 ; i < sizeof(enemies[OB_NAME(player)]) ; i++) {
        int j = member_array(enemies[OB_NAME(player)][i], inv_to);
        
        if (j != -1 && inv_to[j]->is_living()) {
            add_fight(player, inv_to[i], to);
            break;
        }
    }
#endif
}

/**
 * Funkcja odpowiedzialna za zako�czenie walki przez gracza
 * 
 * @param object player - gracz kt�ry ko�czy walk�
 * @param object enemy  - gracz z kt�rym ko�czy walk�.
 * @param int    force  - czy walka ma si� sko�czy� z obu stron (peace)
 */
void
stop_fight(object player, object enemy = 0, int force = 0) {
DBG("CO: stop_fight dla:"+file_name(player)+" wr�g:"+file_name(enemy)+" force="+force);
    
    string location = CBO_LOCATION(player);
// DBG("CO: stop_fight string location = "+location);
// DBG("CO: stop_fight dumpuje fights["+location);
// dump_array(fights[location]);
// dump_mapping(fights);
    //Musimy usun�� gracza z wszystkich walk na lokacji i sprawdzi�, czy te walki
    //maj� si� jeszcze mi�dzy kim toczy�.
    for (int i = 0 ; i < sizeof(fights[location]) ; i++) {
// DBG("CO: stop_fight for i = "+i+" ; i < "+sizeof(fights[location])+"; i++");
        if (member_array(player,  fights[location][i][FT_FIGHTERS]) != -1) {
// DBG("CO: stop_fight jest memberem - player obj "+file_name(player));
            foreach (object fighter : fights[location][i][FT_FIGHTERS]) {
// DBG("CO: stop_fight foreach - obj "+file_name(fighter));
                if (player_enemy[fighter->query_real_name()] == player) {
                    fighter->stop_clash();
                    player->stop_clash();
                    player_enemy[fighter->query_real_name()] = 0;
                    fighter->select_next_enemy(fighter, i, fighter->query_enemies());

                    if(force) //peace
                    {
                        stop_user_fights(player, i);
                        stop_user_fights(enemy, i);
                    }
                }
            }
        }
// DBG("CO: stop_fight - usuwam gracza "+file_name(player)+" z fights["+location+"]["+i+"]["+FT_FIGHTERS+"]");
        fights[location][i][FT_FIGHTERS] -= ({player});
        
        if (sizeof(fights[location][i][FT_FIGHTERS]) < 2) {
            fights = m_delete(fights, location);
        }
    }
    
    player_enemy = m_delete(player_enemy, player->query_real_name());
    
}

/**
 * Usuwa gracza z wszystkich mo�liwych miejsc gdzie zosta� zapisany w zwi�zku z prowadzeniem walki
 */
static void
stop_user_fights(object player, int fight) {
    string location = CBO_LOCATION(player);
DBG("stop_user_fights. wyrzucam player z:");
// dump_array(fights[location][fight][FT_FIGHTERS]);
    player_enemy = m_delete(player_enemy, player->query_real_name());
    fights[location][fight][FT_FIGHTERS] -= ({ player });
    fighters -= ({ player });
    
    for (int i = 0 ; i < sizeof(fights[location][fight][FT_FIGHTERS]) ; i++) {
        if (player_enemy[OB_NAME(fights[location][fight][FT_FIGHTERS][i])] == player) {
            player_enemy[OB_NAME(fights[location][fight][FT_FIGHTERS][i])] = 0;
        }
    }
    
    enemies = m_delete(enemies, OB_NAME(player));
    
    string *ind = m_indexes(enemies);
    
    for (int i = 0 ; i < sizeof(ind) ; i++) {
        enemies[ind[i]] = filter(enemies[ind[i]], &operator(!=)(,player));
    }
}

static void
stroke_in_back(object player, string m_index, int index) {
    
}

void 
check_clash_players(mixed index, int i) {
    foreach (object fighter : fights[index][i][FT_FIGHTERS]) {
        if (CBO_LOCATION(fighter) != index) {
			DBG("CHECK_CLASH_PLAYERS1. Usuwamy "+fighter+"z fighters.");
            fights[index][i][FT_FIGHTERS] -= ({ fighter });
            fighters -= ({ fighter });
            player_enemy = m_delete(player_enemy, fighter->query_real_name());
        }
    }
    
    if (sizeof(fights[index][i][FT_FIGHTERS]) < 2) {
        if (sizeof(fights[index][i][FT_FIGHTERS])) {
			DBG("CHECK_CLASH_PLAYERS2. Usuwamy "+fights[index][i][FT_FIGHTERS][0]+"z fighters.");
            fighters -= ({ fights[index][i][FT_FIGHTERS][0] });
        }
        
        if (sizeof(fights[index]) == 1) {
			DBG("CHECK_CLASH_PLAYERS3");
            fights = m_delete(fights, index);
        } else {
			DBG("CHECK_CLASH_PLAYERS3");
            fights[index] = a_delete(fights[index], i);
        }
    }
}    

void
start_clash(mixed index, int i) {
    fights[index][i][FT_CLASH_TIME] = time();
    
    //Sprawdzamy czy rzeczywi�cie jest mi�dzy kim zrobi� zwarcie, je�li nie to usuwamy walk�.
    check_clash_players(index, i);
    
    if (!is_mapping_index(index, fights) || sizeof(fights[index]) <= i)  {
        DBG("CO: start_clash: Nie wiem czemu ale return " + i + " " + is_mapping_index(index, fights) + " " + member_array(i, fights[index]));
        DBG("CO: start_clash wywo�any z argumentami: mixed index:"+index+", int i:"+i);
        DBG("CO: Dump_mapping fights:");dump_mapping(fights);
        return;
    }
    
    fights[index][i][FT_FIGHTERS]->start_clash();

    DBG("ZWARCIE pomi�dzy " + COMPOSITE_WORDS2(map(fights[index][i][FT_FIGHTERS], &->query_name()), " i ") + "!");
}

/**
 * Funkcja podejmuj�ca decyzje kiedy i kto ma ze sob� starcie
 */
void
check_clash() {
    mixed indexes = m_indexes(fights);
// DBG("check_clash");   
    foreach (string index : indexes) {
        fights[index] -= ({ 0 });
// DBG("check_clash: string index:"+index);
        for (int i = 0 ; i < sizeof(fights[index]) ; i++) {
            if (time() - fights[index][i][FT_CLASH_TIME] > 3) { //@todo tu trzeba wstawi� jaki� czas ze wzoru
                //wyliczany na podstawie tego co ustalimy
                start_clash(index, i);
            }
        }
    }
    
    set_alarm(1.0, 0.0, &check_clash());
}

public object
query_attack_ob(object player) 
{
    if (!player) {
        return 0;
    }
    
    return (object)player_enemy[player->query_real_name()];
}

public mixed *
query_enemies(object player) {
    return enemies[player->query_enemies()];
}
    
public mixed *
query_attacked_by(object player, int fight) {
    object *attacked_by = ({});
    string location = CBO_LOCATION(player);
    
    
    if (!is_mapping_index(location, fights)) {
        return ({ });
    }
    
    fights[location][fight][FT_FIGHTERS] = FILTER_LIVE(fights[location][fight][FT_FIGHTERS]);
    
    for (int i = 0 ; i < sizeof(fights[location][fight][FT_FIGHTERS]) ; i++) {
        if (player_enemy[fights[location][fight][FT_FIGHTERS][i]->query_real_name()] == player) {
            attacked_by += ({ fights[location][fight][FT_FIGHTERS][i] });
        }
    }
    
    return attacked_by;
}

public void
update_enemies(object player, int fight) {
    string location = file_name(ENV(player));
    
    for (int i = 0 ; i < sizeof(fights[location][fight][FT_FIGHTERS]); i++) {
        if (!fights[location][fight][FT_FIGHTERS][i]->is_living() ||
            ENV(player) != ENV(fights[location][fight][FT_FIGHTERS][i])) 
        {
            fights[location][fight][FT_FIGHTERS] = a_delete(fights[location][fight][FT_FIGHTERS], i);
        }
    }
}
 
/** 
 * Funkcja odpowiadaj�ca za wybranie nowego wroga.
 * 
 * @param player gracz kt�ry ma zmieni� wroga
 * @param fight index walki w kt�rej gracz walczy
 */
public object
select_next_enemy(object player, int fight) {
    object attack_ob;
    object *attacked_by = ({}), *enemies = ({}), *first = ({});
    
    attacked_by = query_attacked_by(player, fight);

    first = attacked_by & enemies;
    
    if (!sizeof(first)) {
        if (!sizeof(attacked_by)) {
            enemies = player->query_enemies();
            if (sizeof(enemies)) {
                attack_ob = enemies[0];
            }
        } else {
            attack_ob = attacked_by[0];
        }
    } else {
        attack_ob = first[0];
    }
    
    if (!attack_ob) {
        player->remove_prop(LIVE_I_ATTACK_DELAY);
        player->remove_prop(LIVE_I_STUNNED);
    }
    
    return attack_ob;
}

/**
 * Funkcja odpowiadaj�ca za zmian� wroga w�r�d wrog�w z kt�rymi aktualnie walczymy.
 */
public int
change_enemy(object player, object enemy) {
    
    object *attacked_by;
    
    if (member_array(enemy, attacked_by) == -1) {
        return 0;
    } else {
        attacked_by[player] = enemy;
        
        return 1;
    }
    
}
    
    
    

/*
update 
clone /d/Standard/Redania/Rinde/npc/sekretarz
zabij rindesekretarz
zabij rindesekretarz
Dump /std/combat/combat_object fights
Call /std/combat/combat_object check_clash
Dump /std/combat/combat_object fights
*/
    