/**
 *
 * \file /std/combat/paralyze_clash.c
 *
 * @author: krun, vera
 *
 * Parali� u�ywany gdy gracz jest w zwarciu
 */

inherit "/std/paralyze";

#include <macros.h>
#include <stdproperties.h>

void
create_paralyze()
{
    set_name("paralyze_clash");
    set_block_all();

    setuid();
    seteuid(getuid());
    
    add_allowed("opcje");
    add_allowed("walka");
	add_allowed("powiedz");
	add_allowed("wezwij"); //Just in case. ;) [Avard]
	add_allowed("zglos"); 
	add_allowed("peace"); //Do testow meczace jest dwukrotne wpisywanie peace zeby przerwac walke. ;) [Avard]
    
    set_fail_message("W�a�nie walczysz! Nie mo�esz tego wykona�.\n");
}
