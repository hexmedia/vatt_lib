/**
 * \file /std/combat/cplain.c
 *
 *      LVL 1
 *
 *  Znajduj� si� tu proste zasady walki dla istot, kt�re potrafi� walczy� tylko
 * za pomoc� w�asnych cz�ci cia�a i nie u�ywaj� do tego �adnych przedmiot�w.
 *
 *  Podobnie jak pozosta�e obiekty walki ten r�wnie� jest klonowany i dolinkowywany
 * do gracza.
 *
 * Obecnie plikiem opiekuje si� @author Krun
 * Data ostatniej modyfikacji @date Sierpie� 2008
 */

#pragma save_binary
#pragma strict_types
#pragma no_reset

#include <pl.h>

inherit "/std/combat/cbase.c";

public void
create_cplain()
{
}

/*
* Function name: create_cbase
* Description:   Reset the combat functions
*/
public nomask void
create_cbase()
{
    if (qme())
        return;

    create_cplain();
}

/*
 * Function name: cb_configure
 * Description:   Configure nonhumanoid attacks and hitlocations.
 */
public void
cb_configure()
{
    cb_set_speed(15);
    qme()->cr_configure();
}

/******************************************************************
 *
 * These functions should be accessible from 'me'
 *
 */

/**
 * Dodaje atak do tablicy atak�w.
 *
 * @param wchit     Klasa broni do zadawania obra�e�
 * @param wcpen     Klasa broni do przenikliwo�ci(?)
 * @param damtype   Rodzaj obra�e�
 * @param prcuse    Procentowa szansa na u�ycie
 * @param id        Specyficzne id ataku, dla istot humanoidalnych
 *                  zalecane jest u�ycie, W_NONE, W_RIGHT, W_LEFT, W_FOOTR,
 *                  W_FOOTL, W_BOTH, W_FEET.
 * @param skill     Umiej�tno�� u�ycia
 * @param weight    Waga broni(opcjonalnie)
 * @param fcost     Dodatkowe zm�czenie przy przeprowadzaniu ataku
 *                  za pomoc� tego ataku:)
 * @param volume    Obj�to�� broni(opcjonalnie)
 *
 * @return 1/0  Atak dodany/niedodany,
 */
public int
cb_add_attack(int wchit, mixed wcpen, int damtype, int prcuse,
    int id, int skill = 0, int weight = 0, int fcost = 0, int volume = 0)
{
    return ::cb_add_attack(wchit, wcpen, damtype, prcuse, id, skill, weight, fcost, volume);
}

/**
 * Za pomoc� tej funkcji usuwamy atak z livinga.
 *
 * @param id usuwanego ataku.
 *
 * @return 1/0  Atak usuni�ty/nieusuni�ty,
 */
public int
cb_remove_attack(int id)
{
    return ::remove_attack(id);
}

/**
 * Dodajemy do livinga hiltokacje.
 *
 * @param ac        Ac hitlokacji
 * @param prchit    Procentowa szansa na trafienie w hitlokacje
 * @param nazwa      opis hitlokacji
 * @param id        id hitlokacji
 * @param przym     przymiotniki
 * @param hp        czy doda� hp info
 *
 * @return 0 nie dodany
 * @return !0 dodany
 */
public int
cb_add_hitloc(mixed ac, int prchit, mixed nazwa, int id, mixed przym = 0, int hp = 1)
{
    return ::cb_add_hitloc(ac, prchit, nazwa, id, przym, hp);
}

public void
cb_add_hitloc_area(int area, string desc, string cel, string bie, mixed hitlocs)
{
    return ::cb_add_hitloc_area(area, desc, cel, bie, hitlocs);
}

/******************************************************************
 *
 * These functions are called from the central combat routines in
 * xcombat. Here they are linked to corresponding cr_ routines in the
 * mobile object.
 */

/*
 * Function name: cb_try_hit
 * Description:   Decide if a certain attack fails because of something
 *                related to the attack itself, ie specific weapon that only
 *		  works some of the time.
 * Arguments:     aid:   The attack id
 * Returns:       True if hit, otherwise 0.
 */
public int
cb_try_hit(int aid)
{
    return (int)qme()->cr_try_hit(aid);
}

/*
 * Function name: cb_got_hit
 * Description:   Tells us that we got hit. It can be used to reduce the ac
 *                for a given hitlocation for each hit. This is supposed to be
 *                replaced by a more intelligent routine in creature and
 *                humanoid combat. (called from cb_hit_me)
 * Arguments:     hid:   The hitloc id
 *                ph:    The %hurt
 *                att:   Attacker
 *		  aid:   The attack id
 *                dt:    The damagetype
 *	 	  dam:	 The damage in hit points
 */
public varargs object
cb_got_hit(int hid, int ph, object att, int aid, int dt, int dam)
{
    return qme()->cr_got_hit(hid, ph, att, aid, dt, dam);

    return 0;
}

/**
 * Dzi�ki tej funkcji sprawdzamy opis danego ataku.
 *
 * @param aid   id ataku
 * @param przyp przypadek w jakim ma zosta� podany opis ataku,
 *              nieaktywne w przypadku zwyk�ych cios�w.
 *
 * @return opis ataku.
 */
public string
cb_attack_desc(int aid, int przyp = PL_DOP)
{
    string desc;

    if(desc = qme()->cr_attack_desc(aid))
        return desc;
    else
        return ::cb_attack_desc(aid, przyp);
}

/**
 * @param aid   Identyfikator ataku
 *
 * @return Rodzaj nazwy ataku.
 */
public int
cb_query_attack_rodzaj(int aid)
{
    int rodz;

    if(rodz = qme()->cr_query_attack_rodzaj(aid))
        return rodz;
    else
        return ::cb_query_attack_rodzaj(aid);
}

/*
 * Function name: cb_did_hit
 * Description:   Tells us that we hit something. Should produce combat
 *                messages to all relevant parties. This is supposed to be
 *                replaced by a more intelligent routine in creature and
 *                humanoid combat. (called from heart_beat)
 * Arguments:     aid:    The attack id
 *                hdesc:  The hitlocation description.
 *                phurt:  The %hurt made on the enemy
 *                enemy:  The enemy who got hit
 *		  dt:	  The current damagetype
 *		  phit:   The %success that we made with our weapon
 *		  dam:	  The damamge made in hitpoints
 *		  tohit:  How well did we hit
 *		  def_ob: Obj that defended or how we defended (if miss)
 *		  armour: Armour on the hit hitlocation
 */
public void
cb_did_hit(int aid, string hdesc, int phurt, object enemy, int dt,
       int phit, int dam, int tohit, mixed def_ob, object armour,
       int woundType, int woundLoc, int handicapLoc)
{
    if (qme()->cr_did_hit(aid, hdesc, phurt, enemy, dt, phit, dam,
            tohit, def_ob, armour, woundType, woundLoc, handicapLoc))
    {
        return;
    }

    ::cb_did_hit(aid, hdesc, phurt, enemy, dt, phit, dam, tohit, def_ob,
        armour, woundType, woundLoc, handicapLoc);
}
