/**
 * \file /std/combat/chumlock.c
 *
 *          LVL 4
 *
 *  Plik ten jest zablokowan� wersj� pliku \file /std/combat/chumanoid.c.
 * Nie dozwolone s� �adne edycje, dodawanie nowych hitlokacji jak i atak�w.
 * Dozwolona jest tylko edycja istniej�cych atak�w(np. przy dobyciu broni).
 *
 * Obecnie plikiem opiekuje si� @author krun
 * Data ostatniej edycji @date Sierpie� 2008
 */

#pragma save_binary
#pragma strict_types

inherit "/std/combat/chumanoid";

#include <pl.h>
#include <wa_types.h>

public void
create_chumlock()
{
}

/**
 * Funkcja kreator.
 */
public nomask void
create_chumanoid()
{
    ::create_chumanoid();
  
    create_chumlock();
}

/**
 * Dodaje atak do tablicy atak�w.
 *
 * @param wchit     Klasa broni do zadawania obra�e�
 * @param wcpen     Klasa broni do przenikliwo�ci(?)
 * @param damtype   Rodzaj obra�e�
 * @param prcuse    Procentowa szansa na u�ycie
 * @param id        Specyficzne id ataku, dla istot humanoidalnych
 *                  zalecane jest u�ycie, W_NONE, W_RIGHT, W_LEFT, W_FOOTR,
 *                  W_FOOTL, W_BOTH, W_FEET.
 * @param skill     Umiej�tno�� u�ycia
 * @param weight    Waga broni(opcjonalnie)
 * @param fcost     Dodatkowe zm�czenie przy przeprowadzaniu ataku
 *                  za pomoc� tego ataku:) (opcjonalnie)
 * @param volume    Obj�to�� broni(opcjonalnie)
 *
 * @return 1/0  Atak dodany/niedodany,
 */
public nomask int
cb_add_attack(int wchit, mixed wcpen, int damtype, int prcuse,
    int id, int skill, int weight = 0, int fcost = 0, int volume = 0)
{
    //Zezwalamy na modyfikacje tylko tych atak�w kt�re ju� znamy.
    if(member_array(id, query_attack_id()) >= 0 || calling_function() == "cb_configure" || calling_function() == "cb_reset_hiloc")
        return ::cb_add_attack(wchit, wcpen, damtype, prcuse, id, skill, weight, fcost);
    else
        return 0;
}

/**
 * Ca�kowicie blokujemy usuwanie atak�w.
 */
public nomask int cb_remove_attack(int id)
{
    return 0;
}

/**
 * Dodajemy do livinga hiltokacje.
 *
 * @param ac        Ac hitlokacji
 * @param prchit    Procentowa szansa na trafienie w hitlokacje
 * @param desc      opis hitlokacji
 * @param id        id hitlokacji
 * @param przym     przymiotniki
 * @param hp        czy doda� hp info
 *
 * @return 0 nie dodany
 * @return !0 dodany
 */
public nomask int
cb_add_hitloc(mixed ac, int prchit, mixed desc, int id, mixed przym = 0, int hp = 1)
{
    //Zezwalamy na modyfikacje tylko na znanych hitlokacjiach, a i to nie wiem jak d�ugo i w jakim stopniu
    if(member_array(id, query_hitloc_id()) >= 0 || calling_function() == "cb_configure" || calling_function() == "cb_reset_hiloc")
        return ::cb_add_hitloc(ac, prchit, desc, id, przym, hp);
    else
        return 0;
}

/**
 * Ca�kowicie blokujemy usuwanie hitlokacji.
 */
public nomask int
cb_remove_hitloc(int id)
{
    return 0;
}

/**
 * Jako, �e gracze mog� zosta� r�wnie� og�uszeni w przypadku
 * gdy maj� na korpusie poni�ej 10% i na g�owie poni�ej 20%.
 * Ale pojawia si� r�wnie� pewna komplikacja przy zabijaniu.
 * Ot� je�li gracz nawet nie ma nic hp na kt�rej� z hitlokacji
 * to mo�e prze�y�, je�li atakuj�cy chcia� go og�uszy�(Mo�e
 * wcale nie znaczy, �e musi:P, ch�� og�uszenia nie jest jedno
 * znaczna z og�uszeniem, 1/2 na prze�ycie).
 * @warning Je�li jako stun podamy -2 to nie b�dzie mo�liwo�ci og�uszenia.
 *          albo gracz umrze albo prze�yje(bo co jak umiera w ognisku?:P)
 *
 * @param kto   kto zada� ostatnie obra�enia.
 * @param stun  czy ma zosta� og�uszony
 *
 * @return <ul>
 *  <li> 0 - nic nie zrobione </li>
 *  <li> 1 - gracz zabity </li>
 *  <li> 2 - gracz og�uszony </li>
 *  <li> 3 - gracz usuni�ty z atak�w </li> </ul>
 */
public nomask int
cb_check_killed(object kto, int stun = 0)
{
    if(stun == -1)
        return ::cb_check_killed(0, -1);

    if(stun == -2)
    {
        if(me->query_hp(-HA_GLOWA) <= 0 || me->query_hp(-HA_KORPUS) <= 0)
        {
            me->do_die(kto);

            cb_check_killed(0, -1);

            return 1;
        }
    }
    else
    {
        if(me->query_hp(-HA_GLOWA) <= 0 || me->query_hp(-HA_KORPUS) <= 0)
        {
            if(stun && random(2))
            {
                me->do_die(kto, 1);
                cb_check_killed(0, -1);
                return 2;
            }
            else
            {
                me->do_die(kto);
                cb_check_killed(0, -1);
                return 1;
            }
        }
        else if(me->query_hp(-HA_GLOWA) <= (20 * me->query_max_hp(-HA_GLOWA) / 100) ||
            me->query_hp(-HA_KORPUS) <= (10 * me->query_max_hp(-HA_KORPUS) / 100) && random(4))
        {
            me->do_die(kto, 1);

            cb_check_killed(0, -1);

            return 2;
        }
    }
}
