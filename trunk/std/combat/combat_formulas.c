#include "combath.h"
#include <math.h>
#include <combat.h>

nomask int
szansa_obrony(int x)
{
    float z = itof(x);

    return ftoi(
        1.34414467371461E-07    * (z * z * z * z) -
        5.80659522691049E-08    * (z * z * z) -
        0.00271879472283097     * (z * z) +
        0.248967254604981       * (z) +
        88.8521598528623) + 1;
}

public int
cp_combat_speed(int s, int stl, int skill) {
    return PROC(s, STYLEMOD(CB_STL_MOD[stl][CB_STL_MOD_SZYBKOSC], skill));
}

public int
cp_combat_clash_speed(int i) {
    return 3;
}