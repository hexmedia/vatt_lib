/**
 * \file /std/combat/chumanoid.c
 *
 *          LVL 3
 *
 *  W pliku tym znajduj� si� wszelkie zasady walki dla istot humanoidalnych.
 * Odpowiada on wi�c za walk� mi�dzy graczami, npcami
 *
 *  Obiekt ten predefiniuje wszystkie ataki, hitlocki a tak�e obszary
 * hitlokacji humanoid�w(czyli obiekt�w opartych o conajmniej
 * \file /std/monster.c oraz \file /std/humanoid.c
 *
 * Obecnie plikiem opiekuje si� @author krun
 * Data ostatniej edycji @date Wrzesie� 2008
 */

inherit "/std/combat/ctool.c";

#include "/std/combat/combath.h"
#include <formulas.h>
// #include <ss_types.h>
#include <std.h>
#include <wa_types.h>
#include <options.h>
#include <pl.h>
#include <math.h>

/*#define IS_KRUN (qme()->query_real_name() == "krun")
#define DBG(x) find_player("krun")->catch_msg("Combat_debugger:" + (x) + "\n")
#define DBK(x) if(IS_KRUN){DBG(x);}*/

#define IS_KRUN ;
#define DBG(x)  ;
#define DBK(x)  ;

static  int             attuse;      /* Total %use, 100% is 1 attack */
static  int             al_mod_id;   /* Alarm na wywolanie cb_modify_procuse() */

public void
create_chumanoid()
{
}

/*
 * Function name: create_ctool
 * Description:   Reset the combat functions
 */
public nomask void
create_ctool()
{
    if (me)
        return;

    create_chumanoid();
}

/**
 * @param wep Je�li chcemy uzyska� info o konkretnej nie o wszystkich
 *            broniach.
 * @return Zwraca uma pos�ugiwania si� aktualnie dzier�on� broni�,
 *         je�li aktualnie nie jest dzier�ona �adna bro� zwraca
 *         uma walka bez broni.
 */
public varargs int
cb_weapon_skill(object wep=0)
{
    int skill, *wielded, i;

    if(wep)
        return (int)me->query_skill(SS_WEP_FIRST + ((int)wep->query_wt() - W_FIRST));

    wielded = me->subinventory(SUBLOC_WIELD);

    if(!sizeof(wielded))
        return ::cb_weapon_skill();

    for(i = 0; i < sizeof(wielded); i++)
    {
        skill += max(0, (int)me->query_skill(SS_WEP_FIRST +
           ((int)wielded[i]->query_wt() - W_FIRST)));
    }

    //Walczy dwiema bro�mi
    if(i > 1)
    {
        skill += me->query_skill(SS_2H_COMBAT);
        i++;
    }

    //Nie widzi w pomieszczeniu - lub nie widzi z kim walczy
    if(!CAN_SEE_IN_ROOM(ENV(me)) || (attack_ob && !me->can_see(attack_ob)))
    {
        skill += me->query_skill(SS_BLIND_COMBAT);
        i++;
    }
    skill /= i;

    return skill;
}

/*
 * Function name: cb_configure
 * Description:   Configure humanoid attacks and hitlocations.
 * Returns:       True if hit, otherwise 0.
 */
public void
cb_configure()
{
    object *obs;
    int il, size;

    ::cb_configure();

    me->add_subloc(SUBLOC_WIELD, this_object());
    me->add_subloc(SUBLOC_WORNA, this_object());

    obs = me->subinventory(SUBLOC_WORNA);
    il = -1;
    size = sizeof(obs);

    while(++il < size)
        obs[il]->move(me, 0);

    if(sizeof(obs))
        tell_object(me,"Oops! Chyba musisz jeszcze raz za�o�y� wszystkie " +
            "zbroje.\n");

    obs = me->subinventory(SUBLOC_WIELD);
    il = -1;
    size = sizeof(obs);

    while(++il < size)
        obs[il]->move(me, 0);

    if(sizeof(obs))
        tell_object(me, "Oops! Chyba musisz doby� broni jeszcze raz.\n");

    cb_add_hitloc(A_UARM_AC,  4, "g�owa",              A_HEAD);
    cb_add_hitloc(A_UARM_AC, 14, "klatka piersiowa",   A_CHEST);
    cb_add_hitloc(A_UARM_AC, 13, "brzuch",             A_STOMACH);
    cb_add_hitloc(A_UARM_AC,  3, "plecy",              A_BACK);
    cb_add_hitloc(A_UARM_AC,  5, "bark",               A_L_SHOULDER,   "lewy");
    cb_add_hitloc(A_UARM_AC,  5, "bark",               A_R_SHOULDER,   "prawy");
    cb_add_hitloc(A_UARM_AC,  6, "rami�",              A_L_ARM,        "lewy");
    cb_add_hitloc(A_UARM_AC,  6, "rami�",              A_R_ARM,        "prawy");
    cb_add_hitloc(A_UARM_AC,  5, "przedrami�",         A_L_FOREARM,    "lewy");
    cb_add_hitloc(A_UARM_AC,  5, "przedrami�",         A_R_FOREARM,    "prawy");
    cb_add_hitloc(A_UARM_AC,  3, "d�o�",               A_L_HAND,       "lewy");
    cb_add_hitloc(A_UARM_AC,  3, "d�o�",               A_R_HAND,       "prawy");
    cb_add_hitloc(A_UARM_AC,  6, "udo",                A_L_THIGH,      "lewy");
    cb_add_hitloc(A_UARM_AC,  6, "udo",                A_R_THIGH,      "prawy");
    cb_add_hitloc(A_UARM_AC,  5, "�ydka",              A_L_SHIN,       "lewy");
    cb_add_hitloc(A_UARM_AC,  5, "�ydka",              A_R_SHIN,       "prawy");
    cb_add_hitloc(A_UARM_AC,  3, "stopa",              A_L_FOOT,       "lewy");
    cb_add_hitloc(A_UARM_AC,  3, "stopa",              A_R_FOOT,       "prawy");

    me->cr_reset_hitloc(A_HEAD);
    me->cr_reset_hitloc(A_CHEST);
    me->cr_reset_hitloc(A_STOMACH);
    me->cr_reset_hitloc(A_L_SHOULDER);
    me->cr_reset_hitloc(A_R_SHOULDER);
    me->cr_reset_hitloc(A_L_ARM);
    me->cr_reset_hitloc(A_R_ARM);
    me->cr_reset_hitloc(A_L_FOREARM);
    me->cr_reset_hitloc(A_R_FOREARM);
    me->cr_reset_hitloc(A_L_HAND);
    me->cr_reset_hitloc(A_R_HAND);
    me->cr_reset_hitloc(A_L_THIGH);
    me->cr_reset_hitloc(A_R_THIGH);
    me->cr_reset_hitloc(A_L_SHIN);
    me->cr_reset_hitloc(A_R_SHIN);
    me->cr_reset_hitloc(A_L_FOOT);
    me->cr_reset_hitloc(A_R_FOOT);
    me->cr_reset_hitloc(A_BACK);

    cb_add_hitloc_area(HA_KORPUS, "korpus", "korpusowi", "korpus",
        ({A_CHEST, A_STOMACH, A_L_SHOULDER, A_R_SHOULDER, A_BACK}));
    cb_add_hitloc_area(HA_L_REKA, "lewa r�ka", "lewej r�ce", "lew� r�k�",
        ({A_L_ARM, A_L_FOREARM, A_L_HAND}));
    cb_add_hitloc_area(HA_P_REKA, "prawa r�ka", "prawej r�ce", "praw� r�k�",
        ({A_R_ARM, A_R_FOREARM, A_R_HAND}));
    cb_add_hitloc_area(HA_L_NOGA, "lewa noga", "lewej nodze", "lew� nog�",
        ({A_L_THIGH, A_L_SHIN, A_L_FOOT}));
    cb_add_hitloc_area(HA_P_NOGA, "prawa noga", "prawej nodze", "praw� nog�",
        ({A_R_THIGH, A_R_SHIN, A_R_FOOT}));
    cb_add_hitloc_area(HA_GLOWA, "g�owa", "g�owie", "g�ow�", ({A_HEAD}));

    cb_add_attack(0, 0, 0, 0, W_RIGHT);
    cb_add_attack(0, 0, 0, 0, W_LEFT);
    cb_add_attack(0, 0, 0, 0, W_BOTH);
    cb_add_attack(0, 0, 0, 0, W_FOOTR);
    cb_add_attack(0, 0, 0, 0, W_FOOTL);

    me->cr_reset_attack(W_RIGHT);
    me->cr_reset_attack(W_LEFT);
    me->cr_reset_attack(W_BOTH);
    me->cr_reset_attack(W_FOOTR);
    me->cr_reset_attack(W_FOOTL);
}

/*
 * Description:
 *		show_subloc(string subloc, object on_obj, object for_obj)
 *			- Print a description of the sublocation 'subloc'
 *			  on object 'ob_obj' for object 'for_obj'.
 */
public string
show_subloc(string subloc, object on, object for_obj)
{
    if (subloc == SUBLOC_WIELD)
        return cb_show_wielded(for_obj);

    else if (subloc == SUBLOC_WORNA)
        return cb_show_worn(for_obj);

    else
        return "";
}

/*
 * Description: Humanoids might reallocate what attacks they use when the
 *              attacks are modified. (If maxuse is set)
 *              The distribution formula is:
 *
 *                       %use = %maxuse * (wchit*wcpen) / ( sum(wchit*wcpen) )
 */
public void
cb_modify_procuse()
{
    int il, *attid, *enabled_attacks, swc, puse, weapon_no; //, unarmed_off;
    mixed *att;

    if(!attuse)
        return;
    
    attid = query_attack_id();
    att = allocate(sizeof(attid));
    enabled_attacks = allocate(sizeof(attid));
    weapon_no = sizeof(filter(cb_query_weapon(-1), &->is_weapon()));
    //unarmed_off = me->query_option(OPT_UNARMED_OFF);

    for (swc = 0, il = 0; il < sizeof(attid); il++)
    {   
        att[il] = query_attacks(attid[il]);

        
        /* test to see if this attack is enabled */
        if (/*!unarmed_off || */(weapon_no < 1) || cb_query_weapon(attid[il]))
        {
            enabled_attacks[il] = 1;
            swc += att[il][ATT_WCHIT] * att[il][ATT_M_HIT];
        }
    }

    for (il = 0; il < sizeof(attid); il++)
    {
        if (swc && enabled_attacks[il])
            puse = (attuse * att[il][ATT_WCHIT] * att[il][ATT_M_HIT]) / swc;
        else
            puse = 0;

/*
    object asdf = this_player();
    set_this_player(find_player("vera"));
    this_player()->catch_msg("\nATAK:\n\n");
    dump_array(att[il]);
    set_this_player(asdf);*/
        
        ::cb_add_attack(att[il][ATT_WCHIT], att[il][ATT_WCPEN],
                     att[il][ATT_DAMT], puse, attid[il],
                     (att[il][ATT_SKILL] ? att[il][ATT_SKILL] : -1),
                     att[il][ATT_WEIGHT], att[il][ATT_FCOST] );
    }
    
}

/*
 * Description: Set the %attacks used each turn. 100% is one attack / turn
 * Arguments:   sumproc: %attack used
 */
public void
cb_set_attackuse(int sumproc)
{
    attuse = sumproc;
    cb_modify_procuse();
}

/*
 * Description: Query the total %attacks used each turn. 100% is one attack / turn
 * Returns:     The attackuse
 */
public int
cb_query_attackuse() { return attuse; }

/*
 * Description: Add an attack, see /std/combat/cbase.c
 */
static varargs int
cb_add_attack(int wchit, mixed wcpen, int damtype, int prcuse, int id, int skill = 0, int weight = 0,
    int fcost = 0, int volume = 0)
{
    int ret;

    ret = ::cb_add_attack(wchit, wcpen, damtype, prcuse, id, skill, weight, fcost);

    if (!get_alarm(al_mod_id))
        al_mod_id = set_alarm(1.0, 0.0, &cb_modify_procuse());
// find_player("vera")->catch_msg("chumanoid -> cb_add_attack :"+wchit+" "+damtype+" "+prcuse+" "+id+"\n");
    return ret;
}

/*
 * Function name: cb_wield_weapon
 * Description:   Wield a weapon.
 * Arguments:	  wep - The weapon to wield.
 * Returns:       True if wielded.
 */
public mixed
cb_wield_weapon(object wep)
{
    int aid, wcskill, owchit, owcpen;
    mixed *att;
    string str;


    if (stringp(str = ::cb_wield_weapon(wep)))
    {
        return str;
    }

    aid = (int) wep->query_attack_id();
    if (cb_query_weapon(aid) == wep && wep->is_weapon())
    {
        att = query_attacks(aid);
        /*
         * We get no more use of the weapon than our skill with it allows.
         */
        wcskill = (int)me->query_skill(SS_WEP_FIRST +
                ((int)wep->query_wt() - W_FIRST));
        if (wcskill < 1)
            wcskill = -1;
        cb_add_attack(att[ATT_WCHIT], att[ATT_WCPEN], att[ATT_DAMT],
                att[ATT_PROC], aid, wcskill, att[ATT_WEIGHT], att[ATT_FCOST]);

        if (aid == W_BOTH) //FIXME. CO� NIE TEGES!!! bo p�niej w cbase -> query_hitloc_area wywo�ywane z 6144 jest g�wno!
        {
            cb_add_attack(0, 0, W_BLUDGEON, 0, W_LEFT, -1);
            cb_add_attack(0, 0, W_BLUDGEON, 0, W_RIGHT, -1);
        }
    }

    cb_modify_procuse();

    return 1;
}

public void
cb_unwield(object wep)
{
    int aid;

    ::cb_unwield(wep);

    aid = wep->query_attack_id();

    if (aid == W_BOTH)
    {
        me->cr_reset_attack(W_LEFT);
        me->cr_reset_attack(W_RIGHT);
    }

    cb_modify_procuse();

    return ;
}

/**
 * Dzi�ki tej funkcji npce nie b�da si� ju� la� bez broni:P
 */
public int
cb_dobadz_broni()
{
    //Jeszcze raz sprawdzimy czy na pewno npc nie gracz
    if(interactive(qme()))
        return 1;

    //A teraz zn�w sprawdzimy czy ju� czego� dobytego nie ma
    if(sizeof(qme()->query_weapon(-1)) != 0)
        return 1;

    //A teraz ju� zaczniemy poszukiwania najlepszej broni.
    object *inv = deep_inventory(qme());

    inv = filter(inv, &->is_weapon());

    int max = 0;
    object ret;
    foreach(object o : inv)
    {
        int hit = o->query_hit();
        int pen = o->query_pen();
        int um = cb_weapon_skill(o);

        int cur = pen+(hit/2)*um;

        if(cur > max)
            max = cur;

        ret = o;
    }

    if(ret)
    {
        while(ENV(ENV(ret)) != qme() && ENV(ret) != qme())
            ret->move(ENV(ENV(ret)), 1);

        if(ENV(ret) != qme())
        {
            ret->move(ret, qme());

            tell_roombb(ENV(qme()), QCIMIE(qme(), PL_MIA) + " wyci�ga " + QSHORT(ret, PL_BIE) +
                " z " + QSHORT(ENV(ret), PL_DOP) + ".\n", ({qme()}));
        }

        qme()->command("dob�d� " + OB_NAME(ret));
    }
}

/**
 * Zak�adamy ciuch b�d� zbroje na dan� cze�� cia�a.
 *
 * @param arm       zak�ada zbroja b�d� ubranie
 * @param na_co     okre�lenie hitlokacji w postaci stringa.
 *
 * @return <ul>
 *   <li> prawda je�li zbroja za�o�ona </li>
 *   <li> komunikat b�edu je�li nieza�o�ona </li>
 * </ul>
 */
public mixed
cb_wear_arm(object arm, string na_co = 0)
{
    if(!arm->is_wearable_item())
        return capitalize(arm->short(PL_MIA)) + " nie da si� za�o�y�.\n";

    return ::cb_wear_arm(arm, na_co);
}

/**
 * Dzi�ki tej funkcji sprawdzamy czy jeszcze �yjemy.
 * W wypadku humanoid�w sprawdzane s� dwa kluczowe
 * obszary hitlokacji - g�owa i korpus.
 * Je�li na kt�rym� z tych obszar�w mamy <= 0 punkt�w �ycia
 * to umieramy, w przeciwnym wypadku �yjemy.
 *
 * @param kto   kto zada� ostatnie obra�enia.
 * @param stun  czy ma zosta� og�uszony
 *
 * @return <ul>
 *  <li> 0 - nic nie zrobione </li>
 *  <li> 1 - gracz zabity </li>
 *  <li> 2 - gracz og�uszony </li>
 *  <li> 3 - gracz usuni�ty z atak�w </li> </ul>
 */
public int
cb_check_killed(object kto, int stun = 0)
{
    if(stun == -1)
        ::cb_check_killed(0, -1);

    if(me->query_hp(-HA_KORPUS) < 0 || me->query_hp(-HA_GLOWA))
    {
        me->do_die(kto);

        ::cb_check_killed(0, -1);

        return 1;
    }
}