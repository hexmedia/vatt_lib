/**
 *
 * \file /std/combat/paralyze_close.c
 *
 * @author: krun
 *
 * Parali� u�ywany gdy gracz zostaje og�uszony.
 *
 * TODO:
 * - <i>gracz og�uszony przewraca si� na ziemi�, przyda�o by si�, �eby tam le�a�.</i>
 */

inherit "/std/paralyze";

#include <macros.h>
#include <stdproperties.h>

object corpse;

void
create_paralyze()
{
    set_name("paralyze_close");
    set_block_all();

    setuid();
    seteuid(getuid());
    
    add_allowed("opcje");
    add_allowed("walka");
	
    set_fail_message("Jeste� teraz zaj�t"+TP->koncowka("y","a")+" walk�!\n");
}

void
ocknij_sie()
{
}
