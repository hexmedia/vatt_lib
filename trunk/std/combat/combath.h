/**
 * \file /std/combat/combat.h
 *
 * Definicje wykorzystywane w walce.
 */


//-------------------------
// WZORY
//



#define IS_ME(m,o)      ((TP == qme()) ? (m) : (o))

#define         HA_DESC     0
#define         HA_BIE      1
#define         HA_CEL      2
#define         HA_HITLOCS  3

#define MAX_ATTACK                  10
#define MAX_HITLOC                  20
#define MAX_ENEMIES                 10
#define MAX_ATTACKING_ENEMIES       4

//Dwa pomocnicze makra licz�ce sobie skuteczno�� stylu
// a - atakuj�cy
// b - broni�cy si�
// m - modyfikator
// s - styl

//Mo�na uzyska� od 70 do 110% efektywno�ci
#define STYLEMOD(m, u)          PROC(m, ((2 * (u)) / 5 + 70))
//Tutaj modikator zale�y od por�wnania i te� mo�e wyno�i� od 50 do 100%
// #define PSTYLEMOD(m, au, bu)    PROC(m, ((bu) - (au)) / 2 + 100)

#define SZANSA1_LOSOWOSC        0
//Ile jest procent losowo�ci przy liczeniu szansy uniku itp
#define SZANSA2_LOSOWOSC        5

/* Veru� tu masz �adnie zebrane wzorki na procentow� szanse na wykonanie
 * uniku, sparowania czy sparowania tarcz�.
 * Oznaczenia:
 *  b - oznacza broni�cego si�
 *  a - oznacza atakuj�cego
 *  u - uniki
 *  i - upicie
 *  o - obci��enie
 *  d - zr�czno��
 *  t - um tarczownictwo
 *  w - um broni
 *  p - p�d broni lub si�a je�li walczymy wr�cz
 *  h - hit broni,
 *  r - parowanie,
 *  m - modyfikator od stylu walki,
 *  l - modyfikator od ustawionej kolejno�ci obrony,
 *  z - zas�ony
 */

#define OBRAZENIA(ap, aw, ah, m)                                                        \
    PROC(m, ((2 * (ah + ap) + aw) / 4))

#define SZANSA_UNIKU1(bu, bd, bi, bo, m)                                                \
    1

#define SZANSA_SPAROWANIA1(br, bw, bd, bi, bo, m, l)                                    \
    0

#define SZANSA_TARCZOWNICTWA1(bt, bd, bs, bi, bo, m, l)                                 \
    0

#define SZANSA_UNIKU2(bu, bd, ai, ao, ad, aw, ap, ah, bi, bo, m)                        \
    szansa_obrony(PROC((m) - SZANSA2_LOSOWOSC + random(SZANSA2_LOSOWOSC),               \
        (4 * (bu) + 3 * (bd) + (ai) + (ao)) / 10 ) -                                    \
    PROC((m) - SZANSA2_LOSOWOSC + random(SZANSA2_LOSOWOSC),                             \
        (4 * (ad) + 6 * (aw) + 4 * ap + (ah) + 3 * (bi) +                               \
        (bo)) / 19))

#define SZANSA_SPAROWANIA2(br, bd, bs, bw, ai, ao, ad, aw, ap, ah, bi, bo, m)           \
    szansa_obrony(PROC((m) - SZANSA2_LOSOWOSC + random(SZANSA2_LOSOWOSC),               \
        (6 * (br) + 4 * (bd) + 4 * (bw) + 2 * (ai) + (ao)) / 17) -                      \
    PROC((m) - SZANSA2_LOSOWOSC + random(SZANSA2_LOSOWOSC),                             \
        (4 * (ad) + 3 * (aw) + 4 * ap + (ah) + 2 * (bi) +                               \
        (bo)) / 15))

#define SZANSA_TARCZOWNICTWA2(bt, bd, bs, ai, ao, aw, ad, ap, bi, m)                    \
    szansa_obrony(PROC((m) - SZANSA2_LOSOWOSC + random(SZANSA2_LOSOWOSC),               \
        (5 * (bt) + 3 * (bs) + (ai) + 2 * (ao)) / 11) -                                 \
    PROC((m) - SZANSA2_LOSOWOSC + random(SZANSA2_LOSOWOSC),                             \
        (5 * ap + 3 * aw + bi) / 9))


#define SZANSA_ZASLONIENIA(az, ad, as, bz, bd, bs, m)                                   \
        1
#define SZANSA_PRZELAMANIA_ZASLONY(ad, as, bz, bd, bs, m)                               \
        1

#define PODLICZ_EXP(x, y, z)    (((ftoi(itof(x)*y))*z)/40)
#define PODLICZ_EXP2(x, y)      (ftoi(itof(x)*y))

#define ATT_WCHIT           0
#define ATT_WCPEN           1
#define ATT_DAMT            2
#define ATT_PROC            3
#define ATT_SKILL           4 /* The skill 0-100 of this attack */
#define ATT_M_HIT           5
#define ATT_M_PEN           6
#define ATT_WEIGHT          7
#define ATT_FCOST           8
#define ATT_VOLUME          9

#define HIT_AC              0
#define HIT_PROC            1
#define HIT_DESC            2
#define HIT_M_AC            3
#define HIT_RODZ            4
#define HIT_PRZYM           5

#define NW_OPISY_CIOSOW ({                                      \
    "staraj�c si� przewidzie� kolejny ruch przeciwnika,",       \
    "korzystaj�c z nieuwagi przeciwnika,",                      \
    "wykorzystuj�c dekoncentracj� przeciwnika,",                \
    "planuj�c kolejny cios,",                                   \
    "dostrzegaj�c luk� w obronie przeciwnika,",                 \
    "uprzedzaj�c cios przeciwnika,",                            \
    "zauwa�aj�c ods�oni�cie przeciwnika,",                      \
    "wychylaj�c si� nieznacznie do przodu,",                    \
    "czuj�c przyspieszony oddech przeciwnika,",                 \
    "widz�c dekoncentracj� przeciwnika,",                       \
    "pr�buj�c znale�� luk� w obronie przeciwnika,",             \
    "staraj�c si� zdekoncentrowa� przeciwnika,"})

#define NW_PRZYMIOTNIK ({                                       \
    "niepewnym",                                                \
    "szcz�liwym",                                              \
    "spokojnym",                                                \
    "nieznacznym",                                              \
    "powolnym",                                                 \
    "prostym",                                                  \
    "finezyjnym",                                               \
    "nietypowym",                                               \
    "zaskakuj�cym",                                             \
    "przemy�lanym",                                             \
    "pewnym",                                                   \
    "szybkim",                                                  \
    "celnym",                                                   \
    "zdecydowanym"})

#define NW_RODZAJ_CIOSU ([                                                                  \
    W_IMPALE:   ({"wypadem", "pchni�ciem", "wbiciem", "uderzeniem", "ciosem", "atakiem"}),  \
    W_SLASH:    ({"zamachem", "wymachem", "ci�ciem", "uderzeniem", "ciosem", "atakiem"}),   \
    W_BLUDGEON: ({"zamachem", "wymachem", "uderzeniem", "ciosem", "atakiem"})])

#define NW_O_LEDWO ([                                           \
    W_IMPALE:   ({                                              \
            ({"nak�uwa",    ({"nieznacznie", "delikatnie"}) }), \
            ({"k�uje",      ({"nieznacznie", "delikatnie"}) }), \
        }),                                                     \
    W_SLASH:    ({                                              \
            ({"zadrapuje",  ({"nieznacznie", "delikatnie"}) }), \
            ({"nacina",     ({"nieznacznie", "delikatnie"}) }), \
        }),                                                     \
    W_BLUDGEON: ({                                              \
            ({"muska",      ({"nieznacznie"})}),                \
            ({"obija",      ({"delikatnie"})}),                 \
            ({"ociera",     ({"nieznacznie"})}),                \
            ({"siniaczy",   ({"delikatnie"})}),                 \
        }),                                                     \
    ])

#define NW_O_LEKKO ([                                           \
    W_IMPALE:   ({                                              \
            ({"k�uje",      ({"p�ytko", "lekko"})}),            \
            ({"nak�uwa",    ({"p�ytko", "lekko"})}),            \
        }),                                                     \
    W_SLASH:    ({                                              \
            ({"tnie",       ({"p�ytko", "lekko"})}),            \
            ({"nacina",     ({"p�ytko", "lekko"})}),            \
        }),                                                     \
    W_BLUDGEON: ({                                              \
            ({"obija",      ({"lekko"})}),                      \
            ({"uderza",     ({"lekko"})}),                      \
        })                                                      \
    ])

#define NW_O_RANISZ ([                                          \
    W_IMPALE:   ({                                              \
            ({"rani",       ({"bole�nie"})})                    \
        }),                                                     \
    W_SLASH:    ({                                              \
            ({"tnie",       ({"bole�nie"})}),                   \
            ({"tnie",       ({"do�� g��boko"})}),               \
        }),                                                     \
    W_BLUDGEON: ({                                              \
            ({"t�ucze",     ({"bole�nie"})}),                   \
            ({"t�ucze",     ({"powa�nie"})}),                   \
            ({"�omocze",    ({""})})                            \
        })                                                      \
    ])

#define NW_O_POWAZNIE ([                                        \
    W_IMPALE:   ({                                              \
            ({"trafia",     ({"zadaj�c powa�ne obra�enia"})}),  \
        }),                                                     \
    W_SLASH:    ({                                              \
            ({"siecze",     ({"g��boko"})}),                    \
        }),                                                     \
    W_BLUDGEON: ({                                              \
            ({"gruchocze",  ({""})}),                           \
            ({"obija",      ({"porz�dnie"})}),                  \
        })                                                      \
    ])

#define NW_O_BCIEZKO ([                                         \
    W_IMPALE:   ({                                              \
            ({"rani",       ({"bardzo g��boko"})}),             \
            ({"trafia",     ({"rani�c powa�nie"})}),            \
        }),                                                     \
    W_SLASH:    ({                                              \
            ({"r�bie",      ({"bardzo ci�ko rani�c", ""})}),   \
        }),                                                     \
    W_BLUDGEON: ({                                              \
            ({"mia�d�y",    ({"pot�nie"})}),                   \
            ({"grzmoci",    ({"z ca�ej si�y", "pot�nie"})}),   \
            ({"druzgocze",  ({"pot�nie"})}),                   \
        })                                                      \
    ])

#define NW_O_MASAKRUJESZ ([                                     \
    W_IMPALE:   ({                                              \
            ({"masakruje",  ({""})}),                           \
        }),                                                     \
    W_SLASH:    ({                                              \
            ({"masakruje",  ({""})}),                           \
        }),                                                     \
    W_BLUDGEON: ({                                              \
            ({"masakruje",  ({""})}),                           \
        })                                                      \
    ])

// Sta�e zwi�zane z ranami
#define HITLOC_WOUND_THRESHOLD  2
#define AREA_WOUND_THRESHOLD    2
