/**
 * \file /std/combat/ctool.c
 *
 *          LVL 2
 *
 *  Zewn�trzny plik dziedzicz�cy z \file /std/comabt/cplain.c
 * u�ywany przez wszystko powy�ej monster.c(lub mobile.c nie wiem co ni�ej:P)
 *
 *  Ten plik jest w tej chwili zawsze sklonowany poniewa� odpowiada za wszelkiego
 * pokazywanie wszelkiego rodzaju zbroi, jak i za bronie.
 *
 * Obecnie plikiem opiekuje si� @author vera
 * Data ostatniej edycji @date po �wi�tach
 *
 * TODO:
 *  * Przu pokazywaniu zbroi grupowanie slot�w, tak, a�eby nogi opisywane by�y osobno
 *    r�ce osobno, a tu��w osobno
 *  * Przy update_armour i update_weapon powinny by� zachowywane miejsca na kt�re dana
 *    rzecz jest za�o�ona, czy te� r�ce przez kt�re jest chwycona.
 */

#pragma save_binary
#pragma strict_types
#pragma no_reset

#include "combath.h"

inherit "/std/combat/cplain";

#include <math.h>
#include <debug.h>
#include <files.h>
#include <macros.h>
#include <formulas.h>
#include <ss_types.h>
#include <wa_types.h>
#include <composite.h>
#include <filter_funs.h>
#include <stdproperties.h>

#define CQME   if(!qme()) destruct();

/* Prototypes */
public void cb_remove_arm(object wep);
static void adjust_ac(int hid, object arm, int rm);
public mixed cb_query_armour(int which);

static mapping aid_attack = ([]),  /* The 'weapon' aiding the attacks */
               aid_hitloc = ([]),  /* The 'armours' protecting the hitlocs */
               tool_slots = ([]);  /* The object occupying a certain slot' */
static object* zalozone = ({});    /* Tablica za�o�onych rzeczy */

static mixed   *aid_defense = ({}); /* Tarcze i bronie do parowania - dane */
static object  *aid_defense_id = ({}); /* jw, wskazniki do obiektow */

/*
   NOTE

     There is a limited number of tool slots and one slot can only be
     occupied by one object at a time. One object may occupy many slots though.

     Armours can protect one or more hit locations. What hit locations
     a given armour protects is given by the function 'query_protects' in
     the armour.

     Weapons can only aid one attack. The attack id is given by the
     function 'query_attack_id' in the weapon.

     The tool slots are used to ensure that the use of a weapon and an armour
     do not conflict. The slots that a weapon or armour occupies are given
     by the function 'query_slots' in weapons and armours.

     Observe that what attacks and hitlocation a weapon and armour is defined
     to aid is independant of what slots it allocates.

     The combat system makes no checks on that relevant tool slots aids
     relevant attacks and hit locations. This is taken care of in
     /std/armour.c and /std/weapon.c

     Tool slots are made as defined by the objects. The only thing that the
     combat system does is to ensure that two tools do not use the same
     slot.

   MAGICAL armours

     Magical armours can protect one or more hitlocations without allocating
     a tool slot. If the property OBJ_I_IS_MAGIC_ARMOUR is defined the armour
     is considered to be a magic one. The magic armour can of course allocate
     any number of tool slots, just like a normal armour.

   MAGICAL weapons

     Magical weapons work just like normal weapons. A magical 'weapon' that
     allocates no combat slot is not a 'weapon' it is an independant magic
     object and must cause damage onto the enemy on its own. Such magic
     attacks are not supported in the combat system.

*/

public void
create_ctool()
{
}

/*
 * Function name: create_cplain
 * Description:   Reset the combat functions
 */
public nomask void
create_cplain()
{
    CQME;

    create_ctool();
}

/*
 * Function name: cb_configure
 * Description:   Configure humanoid attacks and hitlocations.
 * Returns:       True if hit, otherwise 0.
 */
public void
cb_configure()
{
    CQME;

    ::cb_configure();

    qme()->add_subloc(SUBLOC_WIELD);
    qme()->add_subloc(SUBLOC_WORNA);
}

/*
 * Description: Give status information about the combat values
 * 	More info.
 */
public string
cb_status()
{
    CQME;

    string str;
    int size;

    size = sizeof(aid_defense_id);

    if (!size)
        return ::cb_status();

    str = sprintf("\n%-25s %17s %5s\n", "  Kr^otki opis",
        "Bonus do parowania", "%use");

    while (--size >= 0)
    {
        str += sprintf("%-30s    %-5d        %-4d\n",
            aid_defense_id[size]->short(),
            aid_defense[size][0], aid_defense[size][1]);
    }

    return ::cb_status() + str;
}

public void
cb_modify_def_procuse()
{
    CQME;

    int sum, size, x;
    size = sizeof(aid_defense);

    x = -1;

    while(++x < size)
        sum += aid_defense[x][0];

    x = -1;

    while(++x < size)
        aid_defense[x][1] = (aid_defense[x][0] * 100 / sum);

    return ;
}

/**
 * Funkcja pozwalaj�ca na dobycie broni.
 *
 * @param wep obiekt dobywanej broni
 * @param slot na jaki slot chcemy doby� broni
 *
 * WARNING: Tu znajduje si� b��d powoduj�cy, �e przy chwytaniu wi�kszej ilo�ci zak�adalnych
 *          tarczy, tarcza pojawia si� jako za�o�ona. Ale narazie nie mam pomys�u jak go
 *          poprawnie politycznie naprawi�.
 */
public varargs mixed
cb_wield_weapon(object wep, int slot = 0, int chwyc = 0)
{
    CQME;

    int il, aid, *slots, extra, size, weight, max_weight, str, fcost;
    object *obs;

    aid = (int) wep->query_attack_id();
    slots = (int*) wep->query_slots();

    /* Can we use this weapon ?
     */
    if(!query_attacks(aid) && wep->query_weapon())
    {
        return "Hmm, ta bro^n chyba zosta�a stworzona dla jakiej� innej " +
            "rasy - nie wyobra�asz sobie jak " + qme()->koncowka("m^og�by�",
            "mog�aby�") + " z niej korzysta�.\n";
    }

    /**
     * Je�li podali�my slot to sprawdzamy czy jest on wolny.
     */
    if(slot)
    {
        if(tool_slots[slot])
        {
            if(pointerp(tool_slots[slot]))
            {
                if(pointerp(cb_query_armour(slot)) && sizeof(tool_slots[slot] - cb_query_armour(slot)))
                {   //Sprawdzamy co to za slot coby wys�a� adekwatny komunikat.
                    if(slot == TS_R_HAND)
                        return "Praw� r�k� masz ju� zaj�t�.\n";
                    else if(slot == TS_L_HAND)
                        return "Lew� r�k� masz ju� zaj�t�.\n";
                    else if(slot == TS_R_LEG)
                        return "Praw� nog� masz ju� zaj�t�.\n";
                    else if(slot == TS_L_LEG)
                        return "Lew� nog� masz ju� zaj�t�.\n";
                    else
                        return "Nie mo�esz tego doby� poniewa� ju� co� dzier�ysz.\n";
                }
            }
        }
    }
    else
    {   //W przeciwnym wypadku musimy sprawdzi� na jaki slot powinna wyw�drowa� bro�.
        foreach (int slot : slots)
        {
            string co;
            if(slot == TS_R_HAND || slot == TS_L_HAND)
                co = "r�ce";
            else if(slot == TS_R_FOOT || slot == TS_L_FOOT)
                co = "nodze";

            if (tool_slots[slot])
            {
                if (pointerp(tool_slots[slot]))
                {
                    if (pointerp(cb_query_armour(slot)) && sizeof(tool_slots[slot] - cb_query_armour(slot)) ||
                        !pointerp(cb_query_armour(slot)) && sizeof(tool_slots[slot]))
                    {
                        return "Dzier�ysz ju� co� w " + (slot == TS_R_HAND ? "prawej" : "lewej") + " " + co + ".\n";
                    }
                }
            }
        }
    }

    int mov_err;
    if(mov_err = (wep->move(qme(), SUBLOC_WIELD)))
    {
        CMD_LIVE_THINGS->move_err_short(mov_err, wep, qme());
        return "";
    }

    //NOTE: Narazie zdecydowa�em si� zrezygnowa� z broni zak�adanych,
    //      mo�e kiedy� w razie potrzeby obmy�li si� jaki� inny spos�b
    //      na ich u�ycie, zw�aszcza, �e w sumie zbroje te� powinny
    //      zwi�ksza� obra�enia, bo jest r�nica jak kto� ci przypieprzy
    //      nog� bez buta i nog� w blaszanej puszce.
#if 0

    foreach(int slot : slots)
    {
        if(pointerp(tool_slots[slot]))
            tool_slots[slot] += ({ wep });
        else
            tool_slots[slot] = ({ wep });
    }

    zalozone = ({ wep }) + zalozone;
#endif

    aid_attack[aid] = wep;
    weight = wep->query_prop(OBJ_I_WEIGHT);
    str = qme()->query_stat(SS_STR);

    if((max_weight = F_MAX_WEP_WEIGHT(str)) < weight)
    {
        /* Nie oplaca sie walczyc za ciezka dla nas bronia. Za kazdy
        * nadmiarowy kilogram wagi broni placimy 1 punkt zmeczenia.
        */
        fcost = (weight - max_weight) / 1000 + 1;

        if(!interactive(qme()))
        {
            log_file("TOO_HEAVY_WEP",
                sprintf("%-12s fcost %d, str %d, maxw=%d <-> w=%d %s\n",
                (interactive(qme()) ? capitalize(qme()->query_real_name()) :
                file_name(qme())), fcost, str, max_weight, weight, file_name(wep)));
        }
    }

    if(wep->is_weapon())
    {
        cb_add_attack(wep->query_hit(), wep->query_modified_pen(), wep->query_dt(),
            wep->query_procuse(), aid, 0, weight, fcost);
    }

    /*
     * If more than two weapons wielded check the 2H combat, only with 2H
     * skill > 20 will it be profitable to wield 2 weapons.
     * Attackuse range from 80 to 150
     */
    if(sizeof(cb_query_weapon(-1)) > 1)
    {
        extra = qme()->query_skill(SS_2H_COMBAT);
        extra = extra > 20 ? extra / 2 : extra - 20;
        this_object()->cb_set_attackuse(100 + extra);
    }

    extra = F_PARRYMOD(wep->query_hit(),
        qme()->query_skill(SS_PARRY) +
        (qme()->query_skill(wep->query_wt() + SS_WEP_FIRST) / 2));

    il = qme()->query_stat(SS_STR) * 150;
    if(wep->query_hands() != W_BOTH)
        il = il * 6 / 10;

    if((size = wep->query_prop(OBJ_I_WEIGHT)) > il)
        extra = (100 - (165 * (size - il) / il)) * extra / 100;

    if(extra > 0)
    {
        aid_defense_id += ({ wep });
        aid_defense += ({ ({ extra, 0, weight * str }) });
        cb_modify_def_procuse();
    }

    return 1;
}

/**
 * Funkcja generuje i zwraca stringa z opisem dobywanych i trzymanych
 * przez livinga przedmiot�w.
 *
 * @param ob    obiekt dla kt�rego przeznaczony jest opis
 *
 * @return opis dobywanych i trzymanych(chwyconych) przedmiot�w.
 */
public string
cb_show_wielded(object ob)
{
    CQME;

    mixed *b, *c, *d;
    int il, size;
    string str;

    //Zrobi�em podzia� na bronie i obiekty chwytane. [Krun]
    if(sizeof(d=m_values(aid_attack)))
    {
        b = filter(d, &->is_weapon());
        c = d - b;
    }
    else
        return "";

    b -= ({0});
    c -= ({0});

    if (!sizeof(c) && !sizeof(b))
        return "";

    str = "";

    if(sizeof(b))
    {
        if (ob != qme())
            str += "Dzier�y ";
        else
            str += "Dzier�ysz ";

        //Przerobi�em, �eby nie by�o miecz i miecz tylko dwa miecze:)
        b = filter(unique_array(b, "query_wield_desc"), pointerp);
        il = -1;
        size = sizeof(b);
        int asize;
        while(++il < size)
        {
            b[il] = filter(b[il], objectp);
            asize = sizeof(b[il]);
            if(asize == 1)
                b[il] = b[il][0]->query_wield_desc(0);
            else if(asize > 1)
                b[il] = b[il][0]->query_wield_desc(asize);
            else
                b[il] = 0;
        }

        b = b - ({ 0 });
        str += COMPOSITE_WORDS2(b, " oraz ") + ".\n";
    }

    if(sizeof(c))
    {
        if (ob != qme())
            str += "Trzyma ";
        else
            str += "Trzymasz ";

        //Przerobi�em, �eby nie by�o miecz i miecz tylko dwa miecze:)
        c = filter(unique_array(c, "query_wield_desc"), pointerp);
        il = -1;
        size = sizeof(c);
        int asize;
        while(++il < size)
        {
            c[il] = filter(c[il], objectp);
            asize = sizeof(c[il]);
            if(asize == 1)
                c[il] = c[il][0]->query_wield_desc(0);
            else if(asize > 1)
                c[il] = c[il][0]->query_wield_desc(asize);
            else
                c[il] = 0;
        }

        c = c - ({ 0 });

        str += COMPOSITE_WORDS2(c, " a tak�e ") + ".\n";
    }

    return str;
}

/**
 * Dzi�ki tej funkcji mo�emy nakaza� livingowi opuszczenie broni.
 *
 * @param wep - opuszczana bro�.
 */
public void
cb_unwield(object wep)
{
    CQME;

    int aid;

    aid = (int)wep->query_attack_id();

    /* Are we using it?
     */
    if (aid_attack[aid] != wep)
        return;

    /* Take it away from the wield subloc
    */
    if (environment(wep) == qme())
        wep->move(qme());

    aid_attack[aid] = 0;
    cb_remove_arm(wep);

    qme()->cr_reset_attack(aid);

   /* If we wield no more than 1 weapon our % use should go back to 100 */

    if (sizeof(cb_query_weapon(-1)) < 2)
        this_object()->cb_set_attackuse(100);

    if ((aid = member_array(wep, aid_defense_id)) >= 0)
    {
        aid_defense_id = exclude_array(aid_defense_id, aid, aid);
        aid_defense = exclude_array(aid_defense, aid, aid);
    }
}

/**
 * FIXME: Do usuni�cia.
 */
static int
empty_slot(object ob, object curw)
{
    CQME;

    return (ob != curw);
}


static int
weapons_out(object ob)
{
    CQME;

    return (ob && ob->check_armour());
}

/**
 * Filtrowanie przedmiot�w naszyjnych:P
 */
static int
necklaces_filter(object ob)
{
    CQME;

    return (ob && (ob->query_prop(ARMOUR_I_NECKLACE) || ob->query_prop(ARMOUR_I_NECKLACE_FULL)));
}

/**
 * Funkcja zwraca wszystkie dobyte bronie - i tylko bronie. Nie zwraca
 * �adnych innych przedmiot�w trzymanych przez gracza. Na przyk�ad trzymanych lamp.
 *
 * @param which z jakiego slota bro� wy�wietli� (-1 = z wszystkich w formie tablicy)
 *
 * @return bro� ze slota lub tablice z wszystkimi bro�mi.
 */
public mixed
cb_query_weapon(int which)
{
    CQME;

    if(sizeof(m_values(aid_attack)))
    {
        if (which >= 0)
            return (aid_attack[which]->is_weapon() ? aid_attack[which] : 0);
        else
            return filter(m_values(aid_attack), &->is_weapon()) - ({ 0 });
    }
}

/**
 * Funkcja zwraca wszystkie przedmioty chwycone, nie zwraca broni.
 *
 * @param which z jakiego slota przedmioty wy�wietli�(-1 = z wszystkich w formie tablicy)
 *
 * @return przedmioty ze slota lub tablice z wszystkimi przedmiotami.
 */
public mixed
cb_query_chwycone(int which)
{
    CQME;

    if(sizeof(m_values(aid_attack)))
    {
        if (which >= 0) // hmm, krun , a to co to ma by� ? :P [v]
            return (aid_attack->which->is_weapon() ? 0 : aid_attack[which]);
        else
            return filter(m_values(aid_attack), &not() @ &->is_weapon()) - ({ 0 });
    }
}

/**
 * Funkcja sprawdza, czy slot o numerze <b>indice</b> nie jest pusty.
 *
 * @param indice numer slotu do sprawdzenia.
 *
 * @return
 * <ul>
 * <li> <b>1</b>, je�eli slot jest niepusty
 * <li> <b>0</b>, je�eli slot jest pusty
 * </ul>
 */
private int
slot_not_empty(int indice)
{
    CQME;

    if ((pointerp(tool_slots[indice])) && (sizeof(tool_slots[indice])))
        return 1;

    return 0;
}

/**
 * Zwraca informacje o zaj�tych slotach. Je�li jako argument poda si� konkretny
 * identyfikator slota, funkcja zwr�ci obiekty go okupuj�ce (lub 0, gdy slot
 * jest niezaj�ty). Je�li za� jako argument poda si� -1, funkcja zwr�ci tablic�
 * z identyfikatorami wszystkich zaj�tych slot�w.
 *
 * @param slot_num identyfikator slota lub -1.
 *
 * @return
 * <ul>
 * <li> object* - tablica obiekt�w okupuj�cych podany slot
 * <li> int* - tablica z identyfikatorami okupowanych slot�w
 * </ul>
 */
public mixed
cb_query_slot(int slot_num)
{
    CQME;

    if (slot_num >= 0)
        return tool_slots[slot_num];
    else
        return filter(m_indices(tool_slots), &slot_not_empty());
}

/**
 * Funkcja zwraca tablic� ze wszystkimi za�o�onymi/dobytymi przedmiotami
 *
 * @return Tablica ze wszystkimi za�o�onymi/dobytymi przedmiotami.
 */
public object*
cb_query_worn()
{
    CQME;

    return secure_var(zalozone);
}

/**
 * Funkcja zwraca tablic� ze wszystkimi widocznymi za�o�onymi/dobytymi
 * przedmiotami.
 *
 * @return Tablica ze wszystkimi widocznymi za�o�onymi/dobytymi
 * przedmiotami.
 */
public object *
cb_query_worn_visible()
{
    CQME;

    object* a = ({});
    int tmpslots = 0, sloty = 0;

    foreach(object curItem : FILTER_SHOWN(zalozone))
    {
        tmpslots = 0;

        foreach(int slot : curItem->query_wear_at_slots())
        {
            tmpslots |= slot;

            if(slot == TS_R_HAND)
                tmpslots |= TS_R_FINGER;
            if(slot == TS_L_HAND)
                tmpslots |= TS_L_FINGER;
        }
        if(tmpslots & (~sloty))
        {
            a += ({ curItem });
            if((tmpslots != TS_R_FINGER) && (tmpslots != TS_L_FINGER) && ((tmpslots != TS_NECK) ||
                ((tmpslots == TS_NECK) && (!curItem->query_prop(ARMOUR_I_NECKLACE)))))
            {
                sloty |= tmpslots;
            }
        }
    }

    return a;
}

/*
 * Function name: adjust_ac
 * Description:   Adjust relevant hitlocations for a given armour slot
 *                when we wear an armour or remove an armour.
 * Arguments:     hid:   The hitlocation id as given in /sys/wa_types.h
 *                arm:   The armour.
 *                rm:    True if we remove armour
 */
static void
adjust_ac(int hid, object arm, int rarm)
{
    CQME;

    int il, *ac;
    mixed *oldloc;

    ac = (int *)arm->query_ac(hid);
    oldloc = query_hitloc(hid);

    for(il = 0; il < W_NO_DT; il++)
    {
        if(rarm)
            oldloc[0][il] -= ac[il];
        else
            oldloc[0][il] += ac[il];
    }

    cb_add_hitloc(oldloc[HIT_AC], oldloc[HIT_PROC], (oldloc[HIT_DESC] + ({oldloc[HIT_RODZ]})), hid, oldloc[HIT_PRZYM]);
}

/**
 * Funkcja zajmuje sloty potrzebne dla podanej, zak�adanej rzeczy.
 *
 * @param ob    zak�adana rzecz
 */
void
cb_zajmij_sloty(object ob, int *slots = 0)
{
    CQME;

    if(!slots)
        slots = ob->query_slots();

    foreach(int slot : slots)
    {
        if(pointerp(tool_slots[slot]))
            tool_slots[slot] += ({ ob });
        else
            tool_slots[slot] = ({ ob });
    }

    ob->set_wear_at_slots(slots);

    zalozone = ({ ob }) + zalozone;
}

/**
 * Funkcja zwalnia sloty, kt�re by�y u�ywane przez zdejmowan� w�a�nie rzecz.
 *
 * @param ob    zdejmowana rzecz.
 */
void
cb_zwolnij_sloty(object ob)
{
    CQME;

    object* tmp;
    int il, size;

    tmp = m_indices(tool_slots);
    size = sizeof(tmp);

    for(il = 0; il < size; ++il)
    {
        tool_slots[tmp[il]] -= ({ ob });

        if(sizeof(tool_slots[tmp[il]]) == 0)
            m_delete(tool_slots, tmp[il]);
    }

    zalozone -= ({ ob });
}


/**
 * Funkcja ma za zadanie sprawdzi� na jaki slot/jakie sloty
 * za�o�ona zostanie zbroja.
 *
 * @param arm zbroja, kt�r� zak�adamy
 * @param na_co na co chcemy za�o�y�.
 *
 * @return sloty na kt�re zbroja zostanie za�o�ona.
 */
public mixed
cb_wear_arm_check_slots(object arm, string na_co = 0)
{
    int *slots;
    object old_tp = TP;

    //Je�li wiemy na co chcemy za�o�y� to musimy tego poszuka�.
    if(na_co)
    {
        if(sizeof(arm->query_slots()) > 1)
            return "Nie mo�esz za�o�y� " + arm->short(qme(), PL_DOP) + " na " + na_co + ".\n";

        int k = member_array(na_co, map(m_values(hitloc_ac), &operator([])(,2)));
        int id = member_array(k, m_indexes(hitloc_ac));

        if(k == -1 || id == -1 ||
            (id != arm->query_slot_side_bits() && id != (arm->query_slot_side_bits() << 1)))
        {
            return "Nie mo�esz za�o�y� " + arm->short(qme(), PL_DOP) + " na " + na_co + ".\n";
        }

        slots = ({id});
    }
    else
    {
        slots = arm->query_slots();

        // Zwr�cimy pierwszy slot z slot�w udost�pniaj�cych za�o�enie na dwie strony.
        if(arm->query_slot_side_bits())
        {
            for(int i = 0 ; i < sizeof(slots) ; i++)
            {
                for(int j = TS_HEAD; j <= MAX_TS; j <<= 1)
                {
                    if((slots[i] & j) == j)
                    {
                        slots[i] = j;
                        break;
                    }
                }
            }
        }
    }

    if(sizeof(slots) == 1)
    {
        if(is_mapping_index(slots[0], tool_slots) && pointerp(tool_slots[slots[0]]) &&
            sizeof(tool_slots[slots[0]]) && tool_slots[slots[0]][-1]->query_last_layer())
        {
            int side_bts;

            side_bts = arm->query_slot_side_bits() << 1;

            if(!(na_co || !side_bts || is_mapping_index(side_bts, tool_slots) && pointerp(tool_slots[side_bts]) &&
                sizeof(tool_slots[side_bts]) && tool_slots[side_bts][-1]->query_last_layer()))
            {
                slots = ({side_bts});
            }
        }
    }

    return slots;
}

/**
 * Zak�adamy ciuch b�d� zbroje na dan� cze�� cia�a.
 *
 * @param arm       zak�ada zbroja b�d� ubranie
 * @param na_co     okre�lenie hitlokacji w postaci stringa.
 *
 * @return <ul>
 *   <li> sloty na kt�re zbroja zosta�a za�o�ona, je�li zosta�a za�o�ona </li>
 *   <li> komunikat b�edu je�li nieza�o�ona </li>
 * </ul>
 */
public mixed
cb_wear_arm(object arm, string na_co = 0)
{
    CQME;

    int *hid, *tmp1, *tmp2, il, *slots, size, extra, bonus;
    object *obs, tmp;
    string str;
    int wie_tmp1, wie_tmp2;
    int i, j, dodac;

    //Je�li wiemy na co chcemy za�o�y� to musimy tego poszuka�.
    if(na_co)
    {
        if(sizeof(arm->query_slots()) > 1)
            return "Nie mo�esz za�o�y� " + arm->short(qme(), PL_DOP) + " na " + na_co + ".\n";

        int k = member_array(na_co, map(m_values(hitloc_ac), &operator([])(,2)));
        int id = member_array(k, m_indexes(hitloc_ac));

        if(k == -1 || id == -1 ||
            (id != arm->query_slot_side_bits() && id != (arm->query_slot_side_bits() << 1)))
        {
            return "Nie mo�esz za�o�y� " + arm->short(qme(), PL_DOP) + " na " + na_co + ".\n";
        }

        slots = ({id});
    }
    else
        slots = arm->query_slots();         /* The needed tool slot */

    tmp1 = query_hitloc_id();               /* hitlokacje */
    wie_tmp1 = sizeof(tmp1);
    tmp2 = arm->query_protects();           /* sloty zajmowane przez zbroj� */
    wie_tmp2 = sizeof(tmp2);
    hid = ({ });

    for(i = 0; i < wie_tmp1; ++i)
    {
        dodac = 0;
        for(j = 0; j < wie_tmp2; ++j)
        {
            if(tmp1[i] & tmp2[j])
            {
                dodac = 1;
                break;
            }
        }
        if(dodac)
            hid += ({ tmp1[i] });
    }

    /* Warunek wykasowany. Od teraz zbroje moga deklarowac ac na toolsloty
     * nie nalezace do hitlokacji humanoidow. Po prostu nie beda uwzgledniane.
     * Potrzebna jest dlatego operacja powyzej. /Alvin
     * ----------------------------------------------------------------------
     * A w�a�nie, �e nie mog�, mo�na b�dzie potem jakie� zbroje dla konik�w
     * czy zwierz�tek hodowlanych porobi�, i jak by taki cz�owiek w zbroji
     * dla konia wygl�da�, przecie� w 90% tego si� nawet nie da za�o�y�!
     * [Krun]
     */

    //Sprawdzamy czy zbroja si� na nas nadaje.
    if(sizeof(hid) && (sizeof(query_hitloc_id() & hid) < sizeof(hid)))
        return capitalize(arm->short(me, PL_MIA)) + " zupe�nie na ciebie" +
            " nie pasuj" + (arm->query_tylko_mn() ? "�" : "e") + ".\n";

    //Sprawdzamy czy mo�liwe jest za�o�enie czego�, na slot na kt�ry zak�adamy
    //je�li jest na nim rzecz kt�ra ma ustawion� opcj�, �e jest ostani�
    //warstw�, to blokujemy, w przeciwnym wypadku dopuszczamy.
    il = -1;
    size = sizeof(slots);

    if(size == 1)
    {
        if(is_mapping_index(slots[0], tool_slots) && pointerp(tool_slots[slots[0]]) &&
            sizeof(tool_slots[slots[0]]) && tool_slots[slots[0]][-1]->query_last_layer())
        {
            int side_bts;

            side_bts = arm->query_slot_side_bits() << 1;

            if(na_co || !side_bts || is_mapping_index(side_bts, tool_slots) && pointerp(tool_slots[side_bts]) &&
                sizeof(tool_slots[side_bts]) && tool_slots[side_bts][-1]->query_last_layer())
            {
                return "W za�o�eniu " + arm->short(qme(), PL_DOP) + " przeszkadza ci " +
                    tool_slots[slots[il]][-1]->short(qme(), PL_BIE) + "!\n";
            }
            else
                slots = ({side_bts});
        }
    }
    else
    {
        while(++il < size)
        {
            if(is_mapping_index(slots[il], tool_slots) && pointerp(tool_slots[slots[il]]) &&
                sizeof(tool_slots[slots[il]]) && tool_slots[slots[il]][-1]->query_last_layer())
            {
                return "W za�o�eniu " + arm->short(qme(), PL_DOP) + " przeszkadza ci " +
                    tool_slots[slots[il]][-1]->short(qme(), PL_BIE) + "!\n";
            }
        }
    }

    if(arm->move(qme(), SUBLOC_WORNA))
    {
        return "Z jakiego� dziwnego powodu nie mo�esz za�o�y� " +
            arm->short(me, PL_DOP) + ". Najlepiej zg�o� w " +
            arm->koncowka("nim", "niej") + " b��d.\n";
    }

    cb_zajmij_sloty(arm, slots);

    foreach(int x : hid)
    {
        /* Add this armour to the ones protecting this hitlocation
         */
        obs = aid_hitloc[x];
        if (pointerp(obs))
            obs = obs + ({ arm });
        else
            obs = ({ arm });

        aid_hitloc[x] = obs;
        adjust_ac(x, arm, 0);  /* Fix one hitlocs acc ac */
    }

    return slots;
}

/**
 * Funkcja ta opisuje co dany living ma na sobie.
 *
 * @param ob    obiekt dla kt�rego przeznaczony jest opis.
 *
 * @return Opis dla podanego obiektu
 *
 * TODO:
 *  - Trzeba jako� �adniej z�o�y� ten opis, jak kto� ma jakie�
 *    pomys�y to niech albo napisze tu pod spodem, albo
 *    wy�le mi poczt� mudow� b�d� mailem. [Krun] - wzorem
 *    mo�e by� Warlock(nie wiem jak jest na Barsie).
 */
public string
cb_show_worn(object ob)
{
    CQME;

    mixed *a;
    string str = "", *ret = ({});
    int i, il, size, pierwszy = 1;

    if(!(this_player()->query_prop(TEMP_SHOW_ALL_THINGS)))
    {
        int pn;

        pn = cb_poziom_nagosci();

        if(pn == 4)
            ret += ({ (ob == qme() ? "Jeste�" : "Jest") + " zupe�nie nag" + qme()->koncowka("i", "a", "ie") });

        //Troche tu zamiesza�em. Troszke inaczej sprawdzane to wsio jest bo dziwne to by�o.
        //Teraz sprawdzamy czy to kobita czy facet. Dla faceta sprawdzimy tylko czy nie ma
        //jajek na wierzchu, dla kobitek trza jeszcze brzuszek:)

        //I jeszcze jeden problem. na hipsach mo�emy mie� pas, a on z pewno�ci� nie zas�ania tego
        //co trza mie� zas�onione.
        else
        {
            if(pn)
            {
                string *nagie = ({});

                if(pn & 1 == 1)
                    nagie += ({"biodra"});

                if(pn & 2 == 2)
                    nagie += ({"piersi"});

                ret += ({ (ob == qme() ? "Masz" : "Ma") + " nagie " + COMPOSITE_WORDS2(nagie, " oraz ") });
            }

            a = filter(cb_query_worn_visible(), weapons_out);

            // Posortowa�em nieznacznie.
            object *glowa = ({}), *pas = ({}), *szyja = ({}), *rpalec = ({}), *lpalec = ({}), *reszta = ({});

            size = sizeof(a);
            for(il = size-1; il >= 0; --il)
            {
                if (sizeof(a[il]->query_slots()) == 1)
                {
                    switch (a[il]->query_slots()[0])
                    {
                        case TS_HEAD:
                            glowa += ({a[il]});
                            a -= ({ a[il] });
                            break;
                        case TS_NECK:
                            szyja += ({a[il]});
                            a -= ({ a[il] });
                            break;
                        case TS_HIPS:
                            pas += ({ a[il] });
                            a -= ({ a[il] });
                            pierwszy = 0;
                            break;
                        case TS_R_FINGER:
                            rpalec += ({ a[il] });
                            a -= ({ a[il] });
                            break;
                        case TS_L_FINGER:
                            lpalec += ({ a[il] });
                            a -= ({ a[il] });
                            break;
                    }
                }
            }

            if(sizeof(glowa))
            {
                ret += ({"Na g�ow� ma" + (ob == qme() ? "sz" : "") + " za�o�on" +
                    glowa[0]->koncowka("y", "�", "e", "ych", "e") + " " +
                    glowa[0]->short(ob, PL_BIE)});
            }

            if(sizeof(szyja))
            {
                object* necklaces = filter(szyja, necklaces_filter);
                object* szale = szyja - necklaces;

                if(sizeof(necklaces))
                {
                    object* firstnecklaces = unique_array(necklaces, &->short(ob, PL_MIA))[0];

                    ret += ({"Na szyi " + (ob == qme() ? "masz" : "ma") + " " +
                        ilosc(sizeof(firstnecklaces), firstnecklaces[0]->koncowka("zawieszony",
                            "zawiszon�", "zawieszone", "zawieszonych", "zawszone"), "zawiszone", "zawieszone") + " " +
                        FO_COMPOSITE_DEAD(necklaces, ob, PL_BIE) +
                        (sizeof(szale) ? ", pod " + ((sizeof(necklaces) > 1) ?
                        "kt�rymi" : necklaces[0]->koncowka("kt�rym", "kt�r�", "kt�rym", "kt�rymi", "kt�rymi")) +
                        " wida� " + szale[0]->short(ob, PL_BIE) : "") });
                }
                else if(sizeof(szale))
                {
                    ret += ({(ob == qme() ? "Twoja" : qme()->koncowka("Jego", "Jej")) +
                        " szyja jest owini�ta " + szale[0]->short(ob, PL_NAR)});
                }
            }

            if(sizeof(rpalec))
            {
                ret += ({"Na serdecznym palcu prawej r�ki " +
                    (ob == qme() ? "nosisz" : "nosi") + " " + FO_COMPOSITE_DEAD(rpalec, ob, PL_BIE)});
            }

            if(sizeof(lpalec))
            {
                ret += ({"Na serdecznym palcu lewej r�ki " +
                    (ob == qme() ? "nosisz" : "nosi") + " " + FO_COMPOSITE_DEAD(lpalec, ob, PL_BIE)});
            }

            if(sizeof(pas))
            {
                ret += ({"W pasie spina" + (sizeof(pas) > 1 ? "j�" : "") + " " +
                    (ob == qme() ? "ci�" : qme()->koncowka("go", "j�", "je", "ich", "je")) + " " +
                    FO_COMPOSITE_DEAD(pas, ob, PL_MIA) });
            }
        }
    }
    else
    {
        a = zalozone;
        a = filter(a, weapons_out);
        a = FILTER_SHOWN(a);
    }

    str = "";
    if(sizeof(ret))
        str += implode(ret, ".\n") + ".\n";

    if(sizeof(a))
        str += (ob == qme() ? "Masz na sobie" : "Ma na sobie") + " " + FO_COMPOSITE_DEAD(a, ob, PL_BIE) + ".\n";

    return str;
}

/**
 * Dzi�ki tej funkcji mo�emy zdj�� livingowi zbroj�.
 *
 * @param arm   zdejmowana zbroja.
 */
public void
cb_remove_arm(object arm)
{
    CQME;

    int il, pos, *hids, size;
    mixed *a, *b;
    object *tmp;

    //Sprawdzamy czy �ci�gamy rzecz zak�adan�:P
    if(member_array(arm, zalozone) < 0)
    {
        a = m_values(aid_hitloc); /* All protecting objects */
        b = ({ });
        il = -1;
        size = sizeof(a);
        while(++il < size)
            b += pointerp(a[il]) ? a[il] : ({});
        if (member_array(arm, b) < 0)
            return 0;
    }

    if (environment(arm) == qme())
        arm->move(qme());

    tmp = m_indices(tool_slots);
    size = sizeof(tmp);
    for (il = 0; il < size; ++il)
    {
        tool_slots[tmp[il]] -= ({ arm });

        if (sizeof(tool_slots[tmp[il]]) == 0)
            m_delete(tool_slots, tmp[il]);
    }

    zalozone -= ({ arm });

    hids = query_hitloc_id();

    //Usuwamy wszelkie bonusy jak i minusy dawane przez zbroj�.
    il = -1;
    size = sizeof(hids);
    while(++il < size)
    {
        if(pointerp(aid_hitloc[hids[il]]) &&
            (pos = member_array(arm, aid_hitloc[hids[il]])) >= 0)
        {
            adjust_ac(hids[il], arm, 1);
            if(!sizeof(aid_hitloc[hids[il]] =
                exclude_array(aid_hitloc[hids[il]], pos, pos)))
            {
                qme()->cr_reset_hitloc(hids[il]);
            }
        }
    }
    if ((pos = member_array(arm, aid_defense_id)) >= 0)
    {
        aid_defense_id = exclude_array(aid_defense_id, pos, pos);
        aid_defense = exclude_array(aid_defense, pos, pos);
    }

}

/**
 * Dzi�ki tej funkcji mo�emy dowiedzie� si� jakie zbroje na jakim slocie
 * ma na sobie living.
 *
 * @param which     slot kt�ry chcemy obejrze�
 *
 * @return
 *  <ul>
 *     <li> tablic� ze zbrojami na danym slocie </li>
 *     <li> je�li @param which jest -1 to tablic� ze wszystkimi zbrojami </li>
 *     <li> je�li @param which jest -2 to mapping indexowany slotami, ze zbrojami na poszczeg�lnych slotach </li>
 *  </ul>
 */
public mixed
cb_query_armour(int which)
{
    CQME;

    mixed *a, *b;
    int il, size;

    if(which >= 0)
        return filter(tool_slots[which], &->is_armour());

    if(which == -1)
        return filter(zalozone, &->is_armour());
    else if(which == -2)
    {
        int *ind;
        mapping mp;

        mp = tool_slots;

        if(!m_sizeof(mp))
            return 0;

        for(int i = 0 ; i < sizeof(ind = m_indexes(mp)) ; i++)
            mp[ind[i]] = filter(mp[ind[i]], &->is_armour());

        return mp;
    }
}

/**
 * Dzi�ki tej funkcji sprawdzamy opis danego ataku.
 *
 * @param aid   id ataku
 * @param przyp przypadek w jakim ma zosta� podany opis ataku,
 *              nieaktywne w przypadku zwyk�ych cios�w.
 *
 * @return opis ataku.
 */
public string
cb_attack_desc(int aid, int przyp = PL_DOP)
{
    CQME;

    if(objectp(aid_attack[aid]))
        return QSHORT(aid_attack[aid], przyp);
    else
        return ::cb_attack_desc(aid, przyp);
}

/**
 * @param aid   Identyfikator ataku
 *
 * @return Rodzaj nazwy ataku.
 */
public int
cb_query_attack_rodzaj(int aid)
{
    CQME;

    if(objectp(aid_attack[aid]))
        return (aid_attack[aid]->query_rodzaj() - 1);
    else
        return ::cb_query_attack_rodzaj(aid);
}

/**
 * @param id ataku
 *
 * @return Obiekt przyporz�dkowany do ataku o podanym id lub 0 je�li takiego
 *         obiektu nie ma.
 */
public object
cb_query_attack_object(int id)
{
    if(objectp(aid_attack[id]))
        return aid_attack[id];

    return 0;
}

/*
 * Function name: cb_try_hit
 * Description:   Decide if a certain attack fails because of something
 *                related to the attack itself, ie specific weapon that only
 *                works some of the time.
 * Arguments:     aid:   The attack id
 * Returns:       True if hit, otherwise 0.
 */
public int
cb_try_hit(int aid)
{
    CQME;

    if(objectp(aid_attack[aid]))
        return aid_attack[aid]->try_hit(cb_query_attack());
    else
        return (int)qme()->cr_try_hit(aid);
}

/*
 * Function name: cb_did_hit
 * Description:   Tells us that we hit something. Should produce combat
 *                messages to all relevant parties.
 * Arguments:     aid:    The attack id
 *                hdesc:  The hitlocation description.
 *                phurt:  The %hurt made on the enemy
 *                enemy:  The enemy who got hit
 *                dt:     The current damagetype
 *                phit:   The %success that we made with our weapon
 *                dam:    The damamge made in hit points
 *		  tohit:  How well did we hit
 *		  def_ob: Obj that defended or how we defended (if miss)
 *		  armour: Armour on the hit hitlocation
 */
public varargs void
cb_did_hit(int aid, string hdesc, int phurt, object enemy, int dt,
           int phit, int dam, int tohit, mixed def_ob, object armour,
           int woundType, int woundLoc, int handicapLoc)
{
    CQME;

    if((!enemy) || (!qme()))
        return;

    if(objectp(aid_attack[aid]) &&
        aid_attack[aid]->did_hit(aid, hdesc, phurt, enemy, dt, phit, dam,
                tohit, def_ob, armour, woundType, woundLoc, handicapLoc))
    {
        /*
         * Adjust our panic level
         */
        if(phurt >= 0)
            cb_add_panic(-3 - phurt / 5);
        else
            cb_add_panic(1);
        return;
    }
    else
        ::cb_did_hit(aid, hdesc, phurt, enemy, dt, phit, dam, tohit, def_ob,
            armour, woundType, woundLoc, handicapLoc);
}

/*
 * Function name: cb_got_hit
 * Description:   Tells us that we got hit. It can be used to reduce the ac
 *                for a given hitlocation for each hit.
 * Arguments:     hid:   The hitloc id
 *                ph:    The %hurt
 *                att:   Attacker
 *                aid:   The attack id
 *                dt:    The damagetype
 *                dam:   The damage done in hitpoints
 * Returns: The armour that parried the attack.
 */
public varargs object
cb_got_hit(int hid, int ph, object att, int aid, int dt, int dam, int bs = 0)
{
    CQME;

    int il, size, sum, ran, tmp;
    object *arms, arm;

    /*
     * Many armours may help to cover the specific bodypart: hid
     */
    arms = aid_hitloc[hid];

    if(pointerp(arms))
    {
        sum = query_hitloc(hid)[0][QUICK_FIND_EXP(dt)] - 1;
        ran = random(sum);
        il = -1;
        size = sizeof(arms);
        while(++il < size)
        {
            if(!bs)
                arms[il]->got_hit(hid, ph, att, aid, dt, dam);
            else
                arms[il]->got_hit(hid, ph, att, aid, dt, dam); //FIXME: zmieni� aid na jaki� aid broni strzeleckiej:P

            if(!(dam || arm))
            {
                sum -= arms[il]->query_ac(hid, dt);

                if(bs)
                    arm = arms[il];

                if ((sum <= ran) && (!arms[il]->query_prop(ARMOUR_I_MAGIC_PARRY)))
                    arm = arms[il];
            }
        }
    }

    if(!bs)
        qme()->cr_got_hit(hid, ph, att, aid, dt, dam);

    return arm /*tool_slots[hid]*/;
}

/**
 * Funkcja aktualizuje wszystkie dane zwi�zane ze zbroj�
 * (zak�ada i zdejmuje zbroje).
 *
 * @param obj   zbroja do uaktualnienia.
 *
 * TODO:
 *  - Zbroja nie powinna by� zdejmowana i zak�adana, a tylko na nowo
 *    liczone powinny by� wszystlie warto�ci, funkcja ta powinna by�
 *    u�ywana ju� przy zak�adaniu zbroji aby nie podwaja� kodu.
 */
public void
cb_update_armour(object obj)
{
    CQME;

    if(!obj)
        return ;

    cb_remove_arm(obj);
    cb_wear_arm(obj);

#if 0
    int *index, i, j, size, size2;
    object *arms;
    mixed *oldloc;

    if (!obj)
        return;

    index = m_indexes(aid_hitloc);
    i = -1;
    size = sizeof(index);
    while(++i < size)
    {
        arms = aid_hitloc[index[i]];
        if (pointerp(arms) && obj && (member_array(obj, arms) > - 1))
        {
            me->cr_reset_hitloc(index[i]);
            j = -1;
            size2 = sizeof(arms);
            while(++j < size2)
                adjust_ac(index[i], arms[j], 0);
        }
    }
#endif
}

/**
 * Funkcja uaktualnia wszelkie warto�ci modyfikiwane przez bro�
 *
 * @param wep   bro� kt�rej warto�ci maj� zosta� uaktualnione
 *
 * TODO:
 *  - podobnie jak w cb_update_armour
 */
public void
cb_update_weapon(object wep)
{
    CQME;

    if (!wep)
        return;

    cb_unwield(wep);
    cb_wield_weapon(wep);
}

/**
 * Dzi�ki tej funkcji dowiadujemy si� jakie tarcze, kt�rymi mo�na w jaki�
 * spos�b operowa�(os�ania� si�) ma przy sobie gracz.
 *
 * @return wszystkie tarcze, kt�rymi mo�na operowa�.
 */
public object *
cb_query_tarcze()
{
    //Tarcze, kt�rymi mo�emy operowa� mog� by� przez nas chwycone.
    //lub te� za�o�one na kt�re� z przedramion.
    object  tmp,
           *tarcze;

//     dump_array(aid_attack);
    tarcze = (filter(m_values(aid_attack), &->is_shield()) ?: ({}));
//     dump_array(tarcze);

    if((tmp = cb_query_armour(A_L_FOREARM))->is_shield())
        tarcze += ({tmp});
    if((tmp = cb_query_armour(A_R_FOREARM))->is_shield())
        tarcze += ({tmp});

    return ({});
}

/**
 * Funkcja ta sprawdza czym gracz mo�e si� broni�, pozwala to na wykluczenie
 * sposob�w obrony, kt�re s� dla gracza niedost�pne.
 *
 * WARNING: Funkcja nie sprawdza czy mamy w danej dziedzinie jak�� umiej�tno��.
 *          Bada tylko czy w og�le jest mo�liwo�� wykonania danego sposobu
 *          obrony.
 *
 * @return tablica
 *  <ul>
 *    <li> 1/0 - uniki </li>
 *    <li> 1/0 - parowanie broni� </li>
 *    <li> 1/0 - os�ona tarcz� </li>
 *  </ul>
 */
public mixed
cb_query_sposoby_obrony()
{
    int     uniki;
    object *parowanie,
           *tarczownictwo;

    if(MAX(0.0, TP->query_hitloc_area_hp(HA_L_NOGA)) + MAX(0.0, TP->query_hitloc_area_hp(HA_P_NOGA)) > 0.0)
        uniki = 1;

    //Parowa� mo�emy je�li mamy jak�� bro�:
    parowanie = aid_defense_id;

    //Os�oni� si� tarcz� mo�emy w momencie kiedy mamy chwycon� jak�� tarcz�.
    tarczownictwo = cb_query_tarcze();

    return ({uniki, parowanie, tarczownictwo});
}

/**
 * FIXME: FUNKCJA DO WYWALENIA!
 */
public varargs mixed
cb_query_defense(int ped, int bs=0)
{
    CQME;

    int size, x, rnd, bonus;

    size = sizeof(aid_defense_id);
    rnd = random(100); x = 0;
    while (--size >= 0)
    {
        x += aid_defense[size][1];

        if (rnd < x)
        {
            bonus = aid_defense[size][0];

            if (ped > aid_defense[size][2])
            {
                /* Jesli 'ped' (sila * masa) broni atakujacej jest
                 * wiekszy od broniacej, broniacy ma minusy do parowania.
                 */
                bonus -= (((2 * bonus * ped + 30000) /
                    (ped + aid_defense[size][2] + 30000)) - bonus);

                //Bronimy przed broni� strzeleck� wi�c:
                // szansa na pr�b� obrony broni� wynosi 1 - 100
                if(bs)
                {
                    if(aid_defense_id[size]->is_weapon())
                    {
                        if(!random(100))
                            bonus -= 2 * random(bonus) + 50;
                        else
                            return 0;
                    }
                    else
                        bonus -= random(bonus/2);
                }

                if (bonus <= 0)
                    return 0;
            }
            /* Przeciwnik atakuje unarmed, my parujemy bronia. Odwracamy
             * wartosc, zeby dac znac, zeby nie pisalo ze parowalismy
             * bronia - bo to glupiutko wyglada.
             */
            else if (!ped && !aid_defense_id[size]->check_armour())
                bonus = -bonus;

            int typ = (aid_defense_id[size]->is_weapon ? 1 : (aid_defense_id[size]->is_shield() ? 2 : 0));
            return ({ typ, aid_defense_id[size], bonus });
        }
    }

    return 0;
}

/**
 * FIXME: Funkcja do wywalenia po przepisaniu poni�szej.
 */
public int
ord_by_size(object ob1, object ob2)
{
    CQME;

    float s1 = ob1->query_f_size();
    float s2 = ob2->query_f_size();

    return (s1 > s2 ?: (-(s1 < s2) ?: 0));
}

/**
 * FIXME: Funkcja do ca�kowitego przepisania
 */
public void
cb_zaloz_zbroje()
{
    CQME;

    //Player mo�e sam nie musimy za niego
    if(qme()->is_player())
        return;

    object *zbroje = qme()->query_armours();

    //Jak nie mamy zbroi to za�o�ymy wsio, przecie z go�� dup� lata� nie b�dziemy
    if(!sizeof(zbroje))
        TO->command("za�� wszystko");

    zbroje = sort_array(zbroje, ord_by_size);

    foreach(object x : zbroje)
    {
        x->wylicz_rozmiar(0, TO);
        x->wear_me(1, TO);
    }
}

/**
 * Funkcja sprawdza i zwraca poziom nago�ci osobnika.
 *
 * @return Poziom nago�ci
 * <li>
 *  <ul> 1 - biodra </ul>
 *  <ul> 2 - piersi </ul>
 *  <ul> 4 - ca�kiem </ul>
 * </li>
 *
 * @WARNING Mo�na mie� nagie biodra i piersi a nie by� ca�kiem nagim:)
 *
 * Je�li mamy nagie i biodra i piersi to warto�ci� b�dzie 3 poniewa� jest to suma
 * bitowa 1 i 2:)
 */
public int
cb_poziom_nagosci()
{
    mixed tmp_tab;

    tmp_tab = qme()->query_armour(-1);

    if(!tmp_tab || (pointerp(tmp_tab) && (!sizeof(tmp_tab))))
        return 4;

    int ret = 0;

    if(!pointerp(tmp_tab = qme()->query_armour(TS_HIPS)) || !sizeof(filter(tmp_tab, not @ &->is_belt()) - ({0})))
        ret |= 1;

    if(qme()->query_gender() == G_FEMALE && !pointerp(tmp_tab = qme()->query_armour(TS_STOMACH)) || !sizeof(tmp_tab))
        ret |= 2;

    return ret;
}