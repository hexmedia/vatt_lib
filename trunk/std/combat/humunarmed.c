/**
 * \file /std/combat/humunarmed.c
 *
 * W tym pliku znajduj� si� funkcje odpowiedzialne za
 * walk� nieuzbrojonych domy�lnych humanoid�w,
 * czyli takich kt�re walcz�, je�li s� nieuzbrojone
 * tylko r�kami, �okciami, nogami i kolanami.
 */
#pragma save_binary
#pragma strict_types

inherit "/std/combat/monunarmed.c";

#include <pl.h>
#include <macros.h>
#include <wa_types.h>
#include <ss_types.h>
#include <formulas.h>

/**
 * @param aid   Identyfikator ataku.
 * @param przyp Przypadek.
 *
 * @return Opis ataku o podanym @param aid w podanym @param przyp
 */
public string
cr_attack_desc(int aid, int przyp = PL_NAR)
{
    switch(aid)
    {
        case W_RIGHT:
            if(random(100) < 80)
            {
                switch(przyp)
                {
                    case PL_MIA:    return "prawa pi��";
                    case PL_DOP:    return "prawej pi�ci";
                    case PL_CEL:    return "prawej pi�ci";
                    case PL_BIE:    return "prawa pi��";
                    case PL_NAR:    return "praw� pi�ci�";
                    case PL_MIE:    return "prawej pi�ci";
                    default:        return "\n\nB��d hun11. Zg�o� go opisuj�c okoliczno�ci w jakich do niego dosz�o.\n\n";
                }
            }
            else
            {
                switch(przyp)
                {
                    case PL_MIA:    return "prawy �okie�";
                    case PL_DOP:    return "prawego �okcia";
                    case PL_CEL:    return "prawemu �okciowi";
                    case PL_BIE:    return "prawy �okie�";
                    case PL_NAR:    return "prawym �okciem";
                    case PL_MIE:    return "prawym �okciu";
                    default:        return "\n\nB��d hun12. Zg�o� go opisuj�c okoliczno�ci w jakich do niego dosz�o.\n\n";
                }
            }
            break;

        case W_LEFT:
            if(random(100) < 80)
            {
                switch(przyp)
                {
                    case PL_MIA:    return "lewa pi��";
                    case PL_DOP:    return "lewej pi�ci";
                    case PL_CEL:    return "lewej pi�ci";
                    case PL_BIE:    return "lewa pi��";
                    case PL_NAR:    return "lew� pi�ci�";
                    case PL_MIE:    return "lewej pi�ci";
                    default:        return "\n\nB��d hun13. Zg�o� go opisuj�c okoliczno�ci w jakich do niego dosz�o.\n\n";
                }
            }
            else
            {
                switch(przyp)
                {
                    case PL_MIA:    return "lewy �okie�";
                    case PL_DOP:    return "lewego �okcia";
                    case PL_CEL:    return "lewemu �okciowi";
                    case PL_BIE:    return "lewy �okie�";
                    case PL_NAR:    return "lewym �okciem";
                    case PL_MIE:    return "lewym �okciu";
                    default:        return "\n\nB��d hun14. Zg�o� go opisuj�c okoliczno�ci w jakich do niego dosz�o.\n\n";
                }
            }
            break;

        case W_BOTH:
            return "obur�cz";

        case W_FOOTR:
            if(random(100) < 80)
            {
                switch(przyp)
                {
                    case PL_MIA:    return "prawa stopa";
                    case PL_DOP:    return "prawej stopy";
                    case PL_CEL:    return "prawej stopie";
                    case PL_BIE:    return "prawa stop�";
                    case PL_NAR:    return "praw� stop�";
                    case PL_MIE:    return "prawej stopie";
                    default:        return "\n\nB��d hun15. Zg�o� go opisuj�c okoliczno�ci w jakich do niego dosz�o.\n\n";
                }
            }
            else
            {
                switch(przyp)
                {
                    case PL_MIA:    return "prawe kolano";
                    case PL_DOP:    return "prawego kolana";
                    case PL_CEL:    return "prawemu kolanu";
                    case PL_BIE:    return "prawe kolano";
                    case PL_NAR:    return "prawym kolano";
                    case PL_MIE:    return "prawym kolanem";
                    default:        return "\n\nB��d hun16. Zg�o� go opisuj�c okoliczno�ci w jakich do niego dosz�o.\n\n";
                }
            }
            break;

        case W_FOOTL:
            if(random(100) < 80)
            {
                switch(przyp)
                {
                    case PL_MIA:    return "lewa stopa";
                    case PL_DOP:    return "lewej stopy";
                    case PL_CEL:    return "lewej stopie";
                    case PL_BIE:    return "lew� stop�";
                    case PL_NAR:    return "lew� stop�";
                    case PL_MIE:    return "lewej stopie";
                    default:        return "\n\nB��d hun17. Zg�o� go opisuj�c okoliczno�ci w jakich do niego dosz�o.\n\n";
                }
            }
            else
            {
                switch(przyp)
                {
                    case PL_MIA:    return "lewe kolano";
                    case PL_DOP:    return "lewego kolana";
                    case PL_CEL:    return "lewemu kolanu";
                    case PL_BIE:    return "lewe kolano";
                    case PL_NAR:    return "lewym kolanem";
                    case PL_MIE:    return "lewym kolanie";
                    default:        return "\n\nB��d hun18. Zg�o� go opisuj�c okoliczno�ci w jakich do niego dosz�o.\n\n";
                }
            }
            break;
    }

    return "\n\nB��d hun19. Zg�o� go opisuj�c okoliczno�ci w jakich do niego dosz�o.\n\n"; /* should never occur */
}

/**
 * @param aid   Identyfikator ataku
 *
 * @return Rodzaj nazwy ataku.
 */
public int
cr_query_attack_rodzaj(int aid)
{
    switch(aid)
    {
        case W_LEFT:
        case W_RIGHT:
        case W_BOTH:
        case W_FOOTL:
        case W_FOOTR:
            return PL_ZENSKI;
    }
}

/**
 * Ustawia wszystkie ataki dla normalnych humanoid�w.
 *
 * @param hit
 * @param pen
 */
public void
set_all_attack_unarmed(int hit, int pen)
{
    TO->set_attack_unarmed(W_RIGHT, hit, pen,   W_BLUDGEON, 25, 0);
    TO->set_attack_unarmed(W_LEFT,  hit, pen,   W_BLUDGEON, 25, 0);
    TO->set_attack_unarmed(W_FOOTR, hit, pen,   W_BLUDGEON, 25, 0);
    TO->set_attack_unarmed(W_FOOTL, hit, pen,   W_BLUDGEON, 25, 0);
}
