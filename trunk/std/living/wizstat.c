/*
 * /std/living/wizstat.c
 *
 * Contains the code to implement the various wizardly information commands.
 */

#include <filepath.h>

/*
 * Function name: stat_living
 * Description  : Give status information on this living.
 * Returns      : string - the description.
 */
public string
stat_living()
{
    string str, tmp;
    object to;

    to = this_object();

    str = sprintf("Imi�: %-11s Ranga: %-10s (%-2d) " +
            "Rasa: %-10s P�e�: %-10s\n" +
            "Plik: %-31s Uid: %-11s  Euid: %-11s\n",
            capitalize(query_real_name()),
            WIZ_RANK_NAME(SECURITY->query_wiz_rank(query_real_name())),
            SECURITY->query_wiz_level(query_real_name()),
            to->query_rasa(),
            to->query_gender_string(),
            extract(RPATH(file_name(this_object())), 0, 30),
            getuid(this_object()),
            geteuid(this_object()));

    if ((function_exists("enter_game", this_object()) != PLAYER_SEC_OBJECT) ||
            (this_interactive() == this_object()) ||
            wildmatch("*jr*", this_object()->query_real_name()) ||
            (SECURITY->query_wiz_level(this_interactive()->query_real_name()) > 16))
        str += sprintf(
                "------------------------------------------------------" +
                "--------------------\n" +
                "Hp(x100): %4d(%4d)         Mana(x10): %4d(%4d)\n" +
                "Zm�czenie: %5d(%5d)  Waga: %7d(%7d)   Obj�to��: %7d(%7d)\n" +
                "------------------------------------------------------" +
                "--------------------\n" +
                "  Stats:   %|@12s\n"  +
                "    Val:   %|@12d\n" +
                "    Acc:   %|@12d\n" +
                "------------------------------------------------------" +
                "--------------------\n" +
                "Intox: %4d  Headache: %3d  Stuffed: %3d Soaked: %3d  " +
                "Align : %d\n" +
                "Ghost   : %3d  Invis  : %3d Npc   : %3d  " +
                "Whimpy: %3d\n",
                to->query_hp(),
                to->query_max_hp(),
                to->query_mana(),
                to->query_max_mana(),
                to->query_fatigue(),
                to->query_max_fatigue(),
                to->query_prop(OBJ_I_WEIGHT),
                to->query_prop(CONT_I_MAX_WEIGHT),
                to->query_prop(OBJ_I_VOLUME),
                to->query_prop(CONT_I_MAX_VOLUME),
                map(SS_STAT_DESC, upper_case),
                ({ to->query_stat(SS_STR),
                 to->query_stat(SS_DEX),
                 to->query_stat(SS_CON),
                 to->query_stat(SS_INT),
                 to->query_stat(SS_DIS) }),
                ({ to->query_acc_exp(SS_STR),
                 to->query_acc_exp(SS_DEX),
                 to->query_acc_exp(SS_CON),
                 to->query_acc_exp(SS_INT),
                 to->query_acc_exp(SS_DIS) }),
                to->query_intoxicated(),
                to->query_headache(),
                to->query_stuffed(),
                to->query_soaked(),
                to->query_alignment(),
                to->query_ghost(),
                to->query_invis(),
                to->query_npc(),
                to->query_whimpy());

    if (strlen(tmp = to->query_prop(OBJ_S_WIZINFO)))
        str += "Wizinfo:\n" + tmp;

    return str;
}

/*
 * Function name: fix_skill_desc
 * Description  : This function will compose the string describing the
 *                individual skills the player has.
 * Arguments    : int sk_type     - the skill number.
 *                mapping sk_desc - the mapping describing the skills.
 * Returns      : string - the description for this skill.
 */
nomask static string
fix_skill_desc(int sk_type, mapping sk_desc)
{
    string desc;

    if (pointerp(sk_desc[sk_type]))
    {
        desc = sk_desc[sk_type][0];
    }
    else
    {
        if (!(desc = this_object()->query_skill_name(sk_type)))
        {
            desc = "Specjalny";
        }
    }

    return sprintf("%-35s: %3d %10s, %3d %10s, %10f",
        sprintf("%s %5s", extract(desc, 0, 23), "(" + sk_type + ")"),
        this_object()->query_skill(sk_type),
        "[" + this_object()->query_acc_exp(sk_type) + "]",
        this_object()->query_tskill(sk_type),
        "[" + this_object()->query_acc_exp(sk_type, EXP_TEORETYCZNY) + "]",
        TP->query_rutyna(sk_type));
}

/*
 * Function name: skill_living
 * Description  : This function returns a proper string describing the
 *                skills of this living.
 * Returns      : string - the description.
 */
public string
skill_living()
{
    string *skills;
    string sk;
    int *sk_types;
    int index;
    int size;
    mapping sk_desc;

    sk_types = sort_array(query_all_skill_types());

    if (!sizeof(sk_types))
        return capitalize(query_real_name()) + " nie ma �adnych umiej�tno�ci.\n";

    sk_desc = SS_SKILL_DESC;
    sk = "";
    skills = map(sk_types, &fix_skill_desc(, sk_desc));
    size = (sizeof(skills));
    index = -1;

    while(++index < size)
        sk += sprintf("%79s\n", skills[index] + "");

    return "Umiej�tno�ci postaci o imieniu - " + capitalize(query_real_name()) + ":\n" +
        sprintf("%|35s %|14s %|14s %|10s\n", "SKILL:", "praktyka:", "teoria:", "rutyna:") + sk + "\n";
}
