/**
 * \file /std/living/move.c
 *
 * This is a subpart of living.c
 *
 * All movement related routines are coded here.
 */

#include <stdproperties.h>
#include <macros.h>
#include <filter_funs.h>
#include <std.h>
#include <options.h>
#include <sit.h>
#include <colors.h>

static object	*following_me;        /* List of objects following me */

public void follow_leader(string com);

static string
zmien_kierunek(string str)
{
    switch (str)
    {
        case "p�noc":              case "n":   return " na p�noc";
        case "po�udnie":            case "s":   return " na po�udnie";
        case "wsch�d":              case "e":   return " na wsch�d";
        case "zach�d":              case "w":   return " na zach�d";
        case "p�nocny-wsch�d":     case "ne":  return " na p�nocny-wsch�d";
        case "p�nocny-zach�d":     case "nw":  return " na p�nocny-zach�d";
        case "po�udniowy-wsch�d":   case "se":  return " na po�udniowy-wsch�d";
        case "po�udniowy-zach�d":   case "sw":  return " na po�udniowy-zach�d";
        case "d�":                 case "d":   return " na d�";
        case "g�ra":                case "u":   return " na g�r�";
        default: return " " + str;
    }
}

static string
zmien_kierunek_na_przeciwny(string str)
{
    switch (str)
    {
        case "p�noc":              case "n":   return " z po�udnia";
        case "po�udnie":            case "s":   return " z p�nocy";
        case "wsch�d":              case "e":   return " z zachodu";
        case "zach�d":              case "w":   return " ze wschodu";
        case "p�nocny-wsch�d":     case "ne":  return " z po�udniowego-zachodu";
        case "p�nocny-zach�d":     case "nw":  return " z po�udniowego-wschodu";
        case "po�udniowy-wsch�d":   case "se":  return " z p�nocnego-zachodu";
        case "po�udniowy-zach�d":   case "sw":  return " z po�udniowego-wschodu";
        case "d�":                 case "d":   return " z g�ry";
        case "g�ra":                case "u":   return " z do�u";
        default: return "";
    }
}

/*
 * Function name: move_reset
 * Description  : Reset the move module of the living object.
 */
static nomask void
move_reset()
{
    set_m_in(LD_ALIVE_MSGIN);
    set_m_out(LD_ALIVE_MSGOUT);

    set_mm_in(LD_ALIVE_TELEIN);
    set_mm_out(LD_ALIVE_TELEOUT);

    following_me = ({});
}

int
wyfiltruj_obecnych(object ob)
{
    if (environment(this_object()) != environment(ob))
        return 0;

    if (this_object()->check_seen(ob))
    {
        if(sizeof(filter(all_inventory(TP), &->is_paralyze())))
            return 0;

        return 1;
    }

    return 0;
}

private static int
wyfiltruj_mogacych_przejsc(object tm, string vb, string com, int przemykanie)
{
    int ret = 0;
    object oldtp = TP;

    set_this_player(tm);
    ret = ENV(tm)->unq_move(com, przemykanie, vb, 1, 1);
    set_this_player(oldtp);

    return ret;
}

static private void
do_glance_for_teams(object player)
{
    object oldtp = TP;
    set_this_player(player);
    player->do_glance(player->query_option(OPT_BRIEF));
    set_this_player(oldtp);
}

static private void
send_msginoout(object ob, object *team, string msg, string msgp, int mn, string jak, int color=0)
{
    if(!CAN_SEE_IN_ROOM(ob))
        return ;

    team = filter(team, &->check_seen(ob));

    int s = sizeof(team);

    if(!s)
        return;
    else if(s == 1 && mn)
        msg = " " + msgp + jak + ".\n";

    if(!msg)
        return;

    msg = set_color(color) + capitalize(FO_COMPOSITE_LIVE(team, ob, PL_MIA)) + msg +
        clear_color();

    ob->catch_msg(msg);
}

public void
pokaz_ze_przechodze(object *team, mixed *jak, object *zostajacy=({}))
{
    //npc� nie b�dziemy pokazywa�.
    if(!interactive(TO))
        return;

    team -= ({TO});
    zostajacy -= ({TO});

    int st, sz;
    st = sizeof(team);
    sz = sizeof(zostajacy);

    if(!st && !sz)
        return;

    set_this_player(TO);

    TO->catch_msg(SET_COLOR(COLOR_BOLD_ON) +
        (st ? "Wraz z " + FO_COMPOSITE_LIVE(team, TO, PL_NAR) : "") +
        (sz ? (st ? ", ale bez" : "Bez") + " " + FO_COMPOSITE_LIVE(zostajacy, TO, PL_DOP) + (st ? "," : "") : "") +
        ((pointerp(jak[1]) && (sizeof(jak[1]) == 4)) ? jak[1][3] : " pod��asz" + jak[1]) + ".\n" + clear_color());
}

/**
 * Funkcja pozwalaj�ca na przemieszczenie livinga.
 *
 * Pozwala na przeniesienie living'a w inne miejsce. W pierwszym
 * argumencie okre�lamy spos�b przeniesienia w drugim miejsce
 * do kt�rego obiekt ma zosta� przeniesiony. Trzeci argument okre�la
 * czy cz�onkowie dru�yny maj� pod�rza� za wychodz�cym je�li jest
 * on jej przyw�dc�. Trzeci argument okre�la nam czy mamy spojrze�
 * po przej�ciu, za� za pomoc� czwartego argumentu mo�emy okre�li�,
 * czy mamy rzeczywi�cie przenie�� czy tylko przetestowa� czy damy
 * rade.
 *
 * @param jak           - jakim sposobem przeniesiony ma zosta� living
 * @param gdzie         - gdzie przeniesiony ma zosta� living.
 * @param nie_sledz     - czy living ma by� �ledzony przez dru�yne
 * @param nie_patrz     - czy po przej�ciu na lokacje mamy zmusi� living
 *                        do spojrzenia(wa�ne tylko dla graczy).
 * @param przemykanie   - czy mamy si� przemyka�.
 * @param test          - je�li to jest prawd� to move_exit_locationliving
 *                        zwr�ci odpowiedni� warto�� ale nie przeniesie gracza.
 *
 * @return Kod okre�laj�cy wynik przeniesienia:
 *         <ul>
 *           <li> <b>0</b> Sukces</li>
 *           <li> <b>1</b> Zbyt ci�ki dla miejsca docelowego</li>
 *           <li> <b>2</b> Nie mo�e by� opuszczony</li>
 *           <li> <b>3</b> Nie mo�na przenie�� z obecnego pomieszczania </li>
 *           <li> <b>4</b> Obiekt nie mo�e by� umieszczany w torbach, etc.</li>
 *           <li> <b>5</b> Miejsce docelowe nie przyjmuje obiekt�w</li>
 *           <li> <b>6</b> Obiekt nie mo�e by� podniesiony</li>
 *           <li> <b>7</b> Inne (Tekst zosta� wypisany w funkcji <i>move()</i>)</li>
 *           <li> <b>8</b> Zbyt du�y dla miejsca docelowego</li>
 *           <li> <b>9</b> Dotychczasowy kontener jest zamkni�ty</li>
 *           <li> <b>10</b> Docelowy kontener jest zamkni�ty</li>
 *           <li> <b>11</b> Jest za du�o rzeczy w docelowym kontenerze</li>
 *         </ul>
 */
public varargs int
move_living(mixed jak, mixed gdzie, int nie_sledz = 0, int nie_patrz = 0, int przemykanie = 0, int test = 0)
{
    object oldtp;
    mixed msg;
    object from = ENV(TO);

    oldtp = this_player();

    //Je�li gdzie nie jest obiektem to pr�bujemy je odzyka�.
    if(!objectp(gdzie))
    {
        msg = LOAD_ERR(gdzie);

        if (stringp(msg))
        {
            if (!environment(this_object()))
            {
                tell_object(this_object(), "PANIC Move error: " + msg);
                gdzie = this_object()->query_default_start_location();
                msg = LOAD_ERR(gdzie);
                gdzie = find_object(gdzie);
            }
            else
            {
                tell_object(this_object(), msg);
                SECURITY->log_loaderr(gdzie, environment(this_object()), jak,
                    previous_object(), msg);
                return 7;
            }
        }
        else if(stringp(gdzie))
            gdzie = find_object(gdzie);
    }

    //Je�li gdzie nie jest pomieszczeniem to olewamy
    if(!gdzie->query_prop(ROOM_I_IS) && !gdzie->is_room())
        return 7;

    //Je�li testujemy star� metod�.
    if (!jak)
        return move(gdzie, 0, 1, test);

    //Je�li nie mo�na przenie�� do tego obiektu to olewamy
    if(this_object()->query_prop(LIVE_M_NO_MOVE))
    {
        write(this_object()->query_prop(LIVE_M_NO_MOVE));
        return 7;
    }

    if(pointerp(jak))
    {
        jak = secure_var(jak);

        if(pointerp(jak[0]))
            jak[0] = jak[0][0];

        if(stringp(jak[1]))
            jak[1] = " " + jak[1];

        if (sizeof(jak) > 2)
        {
            if (stringp(jak[2]))
                jak[2] = " " + jak[2];
        }
        else
            jak += ({ "" });
    }

    if(stringp(jak))
        jak = ({ jak, zmien_kierunek(jak), zmien_kierunek_na_przeciwny(jak) });

    int index;
    // Je�li nie uda si� przenie�� za pomoc� move to teraz zwracamy b��d.
    if (index = move(gdzie, 0, 0, 1))
        return index;

    object env = environment(this_object()), *team = query_team();;
    string com, vb = query_verb(), cmd;
    int size = sizeof(team);
    if(size && !nie_sledz && !przemykanie && jak[0] != "X" && jak[0] != "M" && jak)
    {
        // A teraz troche przerobione pod��anie za przyw�dc�.
        if (!strlen(vb))
        {
            if (sizeof(explode(jak[0], " ")) == 1)
                vb = jak[0];
            else
                vb = "";
        }
        else
            com = env->query_dircmd();

        cmd = vb + (com ? " " + com : "");
    }

    team = filter(team, &wyfiltruj_obecnych());
    //Wyfiltrowywujemy z tych, kt�rzy nie dadz� rady p�j��..
    object *zostajacy = team;
    team = filter(team, &wyfiltruj_mogacych_przejsc(, vb, com, przemykanie));

    size = sizeof(team);
    string msgin, msgout;
    if(jak[0] == "M")
    {
        msgin = 0;
        msgout = 0;
    }
    else if(jak[0] == "X")
    {
        msgin = " " + this_object()->query_mm_in() + "\n";
        msgout = " " + this_object()->query_mm_out() + "\n";
    }
    else if(przemykanie)
    {
        if(size)
        {
            if (pointerp(jak[2]) && (sizeof(jak[2]) == 3))
                msgin = " " + jak[2][2] + ".\n";
            else
                msgin = " " + this_object()->query_m_in() + " skradaj^ac si�" + jak[2] + ".\n";

            if (pointerp(jak[1]) && (sizeof(jak[1]) == 4))
                msgout = " " + jak[1][2] + ".\n";
            else
                msgout = " przemyka si�" + jak[1] + ".\n";
        }
        else
        {
            if (pointerp(jak[2]) && (sizeof(jak[2]) == 3))
                msgin = " " + jak[2][2] + ".\n";
            else
                msgin = " pod��aj� skradaj^ac si�" + jak[2] + ".\n";

            if (pointerp(jak[1]) && (sizeof(jak[1]) == 4))
                msgout = " " + jak[1][2] + ".\n";
            else
                msgout = " przemykaj� si�" + jak[1] + ".\n";
        }
    }
    else
    {
        if (pointerp(jak[2]) && (sizeof(jak[2]) == 3))
            msgin = " " + jak[2][0] + ".\n";
        else
            msgin = " " + this_object()->query_m_in() + jak[2] + ".\n";

        if (pointerp(jak[1]) && (sizeof(jak[1]) == 4))
            msgout = " " + jak[1][0] + ".\n";
        else
            msgout = " " + this_object()->query_m_out() + jak[1] + ".\n";
    }

    if(size)
    {
        if (jak && jak[0] != "X" && jak[0] != "M")
        {
            if (pointerp(jak[2]) && (sizeof(jak[2]) == 3))
                msgin = " " + jak[2][1] + ".\n";
            else
                msgin = " przybywaj�" + jak[2] + ".\n";

            if (pointerp(jak[1]) && (sizeof(jak[1]) == 4))
                msgout = " " + jak[1][1] + ".\n";
            else
                msgout = " pod��aj�" + jak[1] + ".\n";
        }
    }

    /* Je�li nie mamy ustawionego this_playera to teraz go ustawiamy. */
    if (this_object() != this_player())
        set_this_player(this_object());

    //Je�li testujemy to w tym momencie mo�emy ju� sko�czy� bo dalej nie wykonywane s�
    //ju� �adne testy.
    if(test)
        return 0;

    
    object *team_all = team + ({TO});

    object *other_present;
    if(env)
    {
        /* Update the last room settings. */
        add_prop(LIVE_O_LAST_LAST_ROOM, query_prop(LIVE_O_LAST_ROOM));
        add_prop(LIVE_O_LAST_ROOM, env);
        add_prop(LIVE_S_LAST_MOVE, (vb = query_verb()));

        /* Update the hunting status */
        this_object()->adjust_combat_on_move(1);

        /* Leave footprints. */
        if (!env->query_prop(ROOM_I_INSIDE) && !query_prop(LIVE_I_NO_FOOTPRINTS))
        {
            /* Sprawdzamy czy sposob wyjscia jest godny zapisania */
            if(jak[1] ~= " M" || jak[1] ~= " X")
                env->add_prop(ROOM_AS_DIR, ({ "...do nik�d - urywaj� si� nagle w pewnym miejscu", query_rasa(PL_BIE) }));
            else
                env->add_prop(ROOM_AS_DIR, ({ jak[1], query_rasa(PL_BIE) }));
        }

        /* Report the departure. */
        if (msgout && !query_prop(LIVE_I_SILENT_MOVE) && !query_prop(LIVE_I_TEAM_MOVE))
        {
            if (jak && jak[0] != "X" && jak[0] != "M")
            {
                other_present = FILTER_LIVE(all_inventory(environment())) - team - ({ this_object() });
                for_each(other_present, &send_msginoout(, team_all, msgout,
                    TO->query_m_out(), !!size, jak[1], COLOR_FG_CYAN));
            }
            else
                saybb(SET_COLOR(COLOR_FG_CYAN) + QCIMIE(this_player(), PL_MIA) + msgout + CLEAR_COLOR);
        }
    }

    if (index = move(gdzie))
        return index;
    
    //Inforumujemy obiekt walki, �e gracz si� przaemieszcza, i gdzie si� przemieszcza
    //ale tylko wtedy, je�li jest 'from' - przy klonowaniu npca nie ma lokacji sk�d on pochodzi, gdy� pochodzi on z ciemnej dupy.
    if(objectp(from))
        COMBAT_OBJECT->exit_location(TO, from, gdzie);

    if(!przemykanie)
        remove_prop(OBJ_I_HIDE);

    //Wy�wietlamy komunikat o wyj�ciu.
    if(msgin && !query_prop(LIVE_I_SILENT_MOVE) && !query_prop(LIVE_I_TEAM_MOVE))
    {
        if(jak && jak[0] != "X" && jak[0] != "M")
        {
            other_present = FILTER_LIVE(all_inventory(environment())) - team - ({ this_object() });
            for_each(other_present, &send_msginoout(, team_all, msgin,
                TP->query_m_in(), !!size, jak[2], COLOR_FG_GREEN));
        }
        else
           saybb(SET_COLOR(COLOR_FG_GREEN) + QCIMIE(this_player(), PL_MIA) + msgin + CLEAR_COLOR);
    }

    /* See is people were hunting us or if we were hunting people. */
    this_object()->adjust_combat_on_move(0);

    if((size || sizeof(zostajacy)) && !nie_sledz && !przemykanie && jak[0] != "X" && jak[0] != "M" && jak)
    {
        team->add_prop(LIVE_I_TEAM_MOVE, 1);

        // Wy�wietlamy przechodz�cym odpowiedni komunikat.
        filter(team, &->follow_leader(cmd));
        set_this_player(TO);

        // Wyfiltrowywujemy tych, kt�rzy jednak nie poszli
        team = filter(team, &operator(==)(ENV(TO), ) @ &environment());
        zostajacy -= team;
        team_all = team + ({TO});
        filter(team_all, &->pokaz_ze_przechodze(team_all, jak, zostajacy));

        set_this_player(TO);

        team->remove_prop(LIVE_I_TEAM_MOVE);
    }

    /* Ka�emy graczowi spojrze� po wej�ciu do pomieszczenia.
     */
    team = filter(team_all, &interactive());

    if(!nie_patrz && !TO->query_prop(LIVE_I_TEAM_MOVE))
        for_each(team, &do_glance_for_teams());

    /* Only reset this_player() if we weren't this_player already. */
    if (oldtp != this_object())
        set_this_player(oldtp);

    return 0;
}

/**
 * Funkcja odpowiedzialna za pod��anie za przyw�dc� dru�yny.
 *
 * @param com jakich argument�w do komendy u�y� aby pod�rzy� za przyw�dc�.
 * Function name: follow_leader
 * Description  : If the leader of the team moved, follow him/her.
 * Arguments    : string com - the command to use to follow the leader.
 *
 * TODO:         Zabezpieczenie przeciw wykonywaniu dziwnych komend przez
 *               lidera.
 */
public void
follow_leader(string com)
{
    /* Only accept this call if we are called from our team-leader. */
    if (previous_object() != query_leader())
        return;

    // Lider stoi na tej samej lokacji co my wi�c za nim nie idziemy
    // w ten spos�b nikt nie mo�e wywy�a� jakich� dzikich komend
    // kt�re maj� dziury:P
    if(ENV(TO) == ENV(query_leader()))
        return;

    if(TO->query_prop(SIT_LEZACY) || TO->query_prop(SIT_SIEDZACY))
        TO->command("$wsta�");

    set_this_player(TO);

    /* We use a call_other since you are always allowed to force yourself.
     * That way, we will always be able to follow our leader.
     */
    TO->add_prop(PLAYER_I_IN_TEAM_GLANCE_LATER, 1);

    TO->command("$" + com);
}

/*
 * Function name: reveal_me
 * Description  : Reveal me unintentionally.
 * Arguments    : int tellme - true if we should tell the player.
 * Returns      : int - 1 : He was hidden, 0: He was already visible.
 */
public nomask int
reveal_me(int tellme)
{
    object *list, *list2;
    int index, size;

    if (!query_prop(OBJ_I_HIDE))
        return 0;

    if (tellme)
    {
        this_object()->catch_msg("Nie jeste� ju� schowan" +
            koncowka("y", "a", "e") + ".\n");
    }

    list = FILTER_LIVE(all_inventory(environment()) - ({ this_object() }) );
    list2 = FILTER_IS_SEEN(this_object(), list);
    list -= list2;

    remove_prop(OBJ_I_HIDE);

    list = FILTER_IS_SEEN(this_object(), list);

    index = -1;
    size = sizeof(list);
    while(++index < size)
    {
        tell_object(list[index], "Ku twojemu zdumieniu, " +
            this_object()->query_imie(list[index], PL_MIA) + " pojawi�" +
            koncowka("", "a", "o") + " si� nagle tu� obok ciebie!\n");
    }

    index = -1;
    size = sizeof(list2);
    while(++index < size)
    {
        tell_object(list2[index], this_object()->query_Imie(list2[index],
            PL_MIA) + " wychodzi z ukrycia.\n");
    }

    return 1;
}

public void
add_following(object ob)
{
    if (ob)
        following_me = (following_me - ({ ob })) + ({ ob });
}

public void
remove_following(object ob)
{
    following_me -= ({ ob });
}

public object *
query_following()
{
    return following_me + ({});
}

// -------------- bardziej zaawansowane funkcje do poruszania sie -----

private mixed go_to_where;
private mixed go_to_finally;
private mixed go_to_path;
private int go_to_path_index;
private int go_to_alarm;

/*
 * go_to - rusza livinga do okreslonego miejsca
 * where - object/string - miejsce docelowe
 * finally - string/function - funkcja wywolana po dotarciu do celu
 */
varargs public void
go_to(mixed where, mixed finally, float go_to_time=10.0)
{
    mixed path;

    if (!environment(this_object()))
        return;

    path = znajdz_sciezke(environment(this_object()), where);

    if (!pointerp(path))
        return;

    go_to_where = where;
    go_to_finally = finally;

    if (sizeof(path) == 0)
    {
        this_object()->go_to_finally();
        return;
    }

    go_to_path = path;
    go_to_path_index = 0;
    go_to_alarm = set_alarm(go_to_time, 0.0, "go_to_one_step(go_to_time)");
}

private void
go_to_one_step(float go_to_time)
{
    this_object()->command(go_to_path[go_to_path_index++]);

    if (go_to_path_index >= sizeof(go_to_path))
        this_object()->go_to_finally();
    else
        go_to_alarm = set_alarm(go_to_time, 0.0, "go_to_one_step(go_to_time)");
}

private void
go_to_finally()
{
    if (stringp(go_to_finally))
        call_self(go_to_finally);
    else if (functionp(go_to_finally))
        applyv(go_to_finally, 0);
}
