/*
  kawalek wyciety zywcem z /std/trigaction.c
  reakcje na emoty i mowienie
*/
#pragma save_binary
#pragma strict_types

#if 0
/*
 * Function name: emote_hook
 * Description  : Whenever an emotion is performed on this NPC, this function
 *                is called to let the NPC know about the emotion. This way
 *                we can avoid the usage of all those costly triggers.
 * Arguments    : string emote - the name of the emotion performed. This
 *                    always is the command the player typed, query_verb().
 *                object actor - the actor of the emotion.
 *                string adverb - the adverb used with the emotion, if there
 *                    was one. When an adverb is possible with the emotion,
 *                    this argument is either "" or it will contain the used
 *                    adverb, preceded by a " " (space). This way you can
 *                    use the adverb in your reaction if you please without
 *                    having to parse it further.
 *                string info - in case of some emotions, above information
 *                    is not enough to fully recognize performed subemotion.
 *                    If so, additional info, depending on particular
 *                    emotion, such as used proverb preceded by a space, is
 *                    provided.
 */
public void
emote_hook(string emote, object actor, string adverb = 0, string info = 0)
{
}

/*
 * Function name: emote_hook_onlookers
 * Description  : Whenever this NPC sees an emotion being performed on
 *                someone else, or when it is performed on in the room in
 *                general, this function is called to let the NPC know about
 *                the emotion. This way we can avoid the usage of all those
 *                costly triggers.
 * Arguments    : string emote - the name of the emotion performed. This
 *                    always is the command the player typed, query_verb().
 *                object actor - the actor of the emotion.
 *                object *oblist - the targets of the emotion, if there were
 *                    any.
 *                string adverb - the adverb used with the emotion, if there
 *                    was one. When an adverb is possible with the emotion,
 *                    this argument is either "" or it will contain the used
 *                    adverb, preceded by a " " (space). This way you can
 *                    use the adverb in your reaction if you please without
 *                    having to parse it further.
 *                string info - in case of some emotions, above information
 *                    is not enough to fully recognize performed subemotion.
 *                    If so, additional info, depending on particular
 *                    emotion, such as used proverb preceded by a space, is
 *                    provided.
 */
public void
emote_hook_onlookers(string emote, object actor, object *oblist = 0,
                     string adverb = 0, string info = 0)
{
}

/*
 * Nazwa funkcji : say_to_hook
 * Opis          : Funkcja wywolywana w NPCu ilekroc ktos cos do nas
 *		   mowi, przy uzyciu 'powiedz do'.
 * Argumenty     : string say_string - tekst mowiony do nas.
 */
public void
say_to_hook(string say_string)
{
}
#endif
