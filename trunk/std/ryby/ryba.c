inherit "/std/object";

#include <macros.h>
#include <math.h>
#include <mudtime.h>
#include <ss_types.h>
#include <stdproperties.h>
#include <object_types.h>

/* Zmienne */
float	*mod_pory = ({ });	    	// Liczby okreslajace kiedy ile ryb zeruje

int	 	min_weight,		    		// Minimalna waga ryby
	 	avg_weight,		    		// Srednia waga ryby
	 	max_weight,		    		// Maksymalna waga ryby
	 	*walecznosc,		    	// Walczenosc ryby ({mniejsza, wieksza})
		rozpoznawalnosc,	    	// Jak trudno rozpoznac rybe
	 	max_cena;			    	// Maksymalna cena za rybe

string	*przynety_mniejszej;		// Przynety na ktore bierze mniejsza ryba
string	*przynety_wiekszej;	    	// Przynety na ktore bierze wieksza ryba
string  *rodzaj_brania;		    	// Sposob w jaki bierze ryba ({mniejsza, wieksza})

string	 id_long;		    		// Long zidentyfikowanej ryby
string	 unid_long;		    		// Long niezidentyfikowanej

string	*id_nazwa = allocate(2);    // Odmiana rozpoznanej ryby - szczupak, szczupaka...itd
int		id_rodzaj;		    		// Rodzaj gramatyczny powy�szego


/* Funkcyje */
public string ryba_long();
void set_min_weight(int i);
void set_max_weight(int i);
void set_max_cena(int i);
int query_cena();

void create_ryba()
{
    ustaw_nazwe("ryba");
    dodaj_przym("zwyczajny", "zwyczajni");
    dodaj_przym("p�aski", "p�ascy");

    set_min_weight(10);
    set_max_weight(1500);

    set_max_cena(10);
    add_prop(OBJ_I_WEIGHT, 5000);
}

void
create_object()
{
    create_ryba();

    set_long(&ryba_long());

	set_type(O_RYBY);
}

/* Automagiczna objetosc 
 * ... i automagiczna cena 
 */
public void add_prop(string prop, mixed val)
{
    if(prop ~= OBJ_I_WEIGHT)
	{
        ::add_prop(prop, val);
		::add_prop(OBJ_I_VOLUME, ((random(5)+8)*val)/10);
		::add_prop(OBJ_I_VALUE, query_cena());
	}
    else
        ::add_prop(prop, val);
}
/**
 * Okre�la jaki procent ryb �eruje o dane porze.
 * @param kiedy Tablica z kolejnymi wartosciami,
 *		ustawiamy w kolejnosci: rano, poludnie,
 *		popoludnie, wieczor, noc. Wartosci zapisujemy
 *		jako ulamek(50% jako 0.5). Maksymalna warto��
 *		to 1.
 */
void set_mod_pory(float *mod)
{
    mod_pory = mod;
}

/* Zwraca modyfikator pory dnia. Jako argument podajemy
 * pore dnia z mudtime.h. Bez argumentow dostaniemy
 * tablice zawierajaca wszystkie wspolczynniki. 
 */
varargs mixed query_mod_pory(int kiedy = -1)
{
    if(kiedy == -1)
		return mod_pory;

    if((kiedy == MT_WCZESNY_RANEK) || (kiedy == MT_RANEK))
		return mod_pory[0];

    if(kiedy == MT_POLUDNIE)
		return mod_pory[1];

    if(kiedy == MT_POPOLUDNIE)
		return mod_pory[2];

    if((kiedy == MT_WIECZOR) || (kiedy == MT_POZNY_WIECZOR))
		return mod_pory[3];

    if((kiedy == MT_NOC) || (kiedy == MT_SWIT))
		return mod_pory[4];
}

void set_przynety(string *mniejszej, string *wiekszej)
{
    przynety_mniejszej = mniejszej;
    przynety_wiekszej = wiekszej;
}

string *query_przynety_mniejszej()
{
    return przynety_mniejszej;
}

string *query_przynety_wiekszej()
{
    return przynety_wiekszej;
}

string *query_przynety()
{
    return przynety_mniejszej+przynety_wiekszej;
}

void set_min_weight(int i)
{
    min_weight = i;
}

int query_min_weight()
{
    return min_weight;
}

void set_avg_weight(int i)
{
    avg_weight = i;
}

int query_avg_weight()
{
    return avg_weight;
}

void set_max_weight(int i)
{
    max_weight = i;
}

int query_max_weight()
{
    return max_weight;
}

void set_rodzaj_brania(string* s)
{
    rodzaj_brania = s;
}

/* 
 * Zwraca rodzaj brania 
 * @param jaka Czy chodzi o rybe mloda(0) czy stara(1)
 * @return Rodzaj brania
 */
string query_rodzaj_brania(int jaka)
{
    return rodzaj_brania[jaka];
}

void set_walecznosc(int *i)
{
    walecznosc = i;
}

int *query_walecznosc()
{
    return walecznosc;
}

void set_rozpoznawalnosc(int i)
{
    rozpoznawalnosc = i;
}

int query_rozpoznawalnosc()
{
    return rozpoznawalnosc;
}

void set_id_long(string str)
{
    id_long = str;
}

void set_unid_long(string str)
{
    unid_long = str;
}

string query_id_long()
{
    return id_long;
}

string query_unid_long()
{
    return unid_long;
}

string ryba_long()
{
    if(TP->query_skill(SS_FISHING) < rozpoznawalnosc)
		return unid_long;
    else
		return id_long;
}

void set_max_cena(int i)
{
    max_cena = i;
}

int query_max_cena()
{
    return max_cena;
}

int query_cena()
{
    float ret = LINEAR_FUNC(itof(min_weight), 1.0, itof(max_weight),
							itof(max_cena), itof(query_prop(OBJ_I_WEIGHT)));

    return ftoi(ret);
}

/*
 * Okresla czy ryba jest mala czy duza
 * @param waga Jesli podamy otrzymamy wynik dla
 *	       ryby o podanej wadze, jesli nie
 *	       - wynik dla egzamplarza na ktorym
 *	       wywolujemy funkcje.
 * @return Gdy ryba jest mala - 0, gdy duza - 1
 */
varargs int duza_mala(int waga = -1)
{
    if(waga == -1)
		waga = query_prop(OBJ_I_WEIGHT);
    
    if(waga <= avg_weight)
		return 0;
    else
		return 1;
}

void ustaw_id_nazwe(string *lp, string *lm, int rodzaj)
{
    if(sizeof(lp) != 6 || sizeof(lm) != 6)
		return;

    id_nazwa[0] = ({}) + lp;
    id_nazwa[1] = ({}) + lm;
    id_rodzaj = rodzaj;

}

// Shorty - TODO

/*public varargs string 
short(mixed for_obj, mixed przyp)
{
    int c;
    
    if (!objectp(for_obj))
    {
        if (intp(for_obj))
            przyp = for_obj;
        else if (stringp(for_obj))
            przyp = atoi(for_obj);
        
        for_obj = previous_object();
    }
    else
        if (stringp(przyp))
            przyp = atoi(przyp);

    if (!pointerp(obj_shorts))
        odmien_short();

    if (query_prop(OBJ_I_BROKEN)) {
        if (query_prop(OBJ_I_BROKEN) == 2)
            return oblicz_przym("z�amany", "z�amani", przyp,
                    this_object()->query_rodzaj(), 0) + " " +
                check_call(obj_shorts[przyp], for_obj);
        return oblicz_przym("zniszczony", "zniszczeni", przyp,
                this_object()->query_rodzaj(), 0) + " " +
            check_call(obj_shorts[przyp], for_obj);
    }
	
    return check_call(obj_shorts[przyp], for_obj);
}*/




