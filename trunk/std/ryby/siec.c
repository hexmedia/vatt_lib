inherit "/std/object";

#include <cmdparse.h>
#include <composite.h>
#include <macros.h>
#include <pl.h>
#include <stdproperties.h>

#define RYBKA "/std/ryby/rybka.c"

/* Zmienne */
string	gdzie_zarzucona = 0;		// Gdzie zarzucona jest w tej chwili siec
object	*zlowione = ({ });		// Zlowione obiekty
int	pojemnosc = 15;			// Ile ryb moze pomiescic siec
int	alarm_id;			// Id alarmu odpowiadajacego za napelnianie sieci
int	min_time = 16;			// Minimalny czas po ktorym w siec zlapie sie 1 rybka
int	max_time = 28;			// Maksymalny  ----------------||------------------


/* Funkcje */
int zarzuc(string str);
int wyciagnij(string str);

void napelnij();


void
create_siec()
{
    ustaw_nazwe(({"sie� rybacka", "sieci rybackiej", "sieci rybackiej",
		"sie� ryback�", "sieci� ryback�", "sieci rybackiej"}),
		({"sieci rybackie", "sieci rybackich", "sieciom rybackim",
		 "sieci rybackie", "sieciami rybackimi", "sieciach rybackich"}),
		PL_ZENSKI);

	dodaj_nazwy("sie�");

    set_long("Bardzo �adna sie�.\n");
}

void
create_object()
{
    create_siec();

	setuid();
    seteuid(getuid());

    set_long(long()+"@@zarzucona_desc@@");
}

init()
{
    ::init();
    add_action(&zarzuc(), "zarzuc");
    add_action(&wyciagnij(), "wyciagnij");
}


string zarzucona_desc()
{
    if(!gdzie_zarzucona)
	    return "";

    return "W tej chwili jest zarzucona "+gdzie_zarzucona+".\n";
}

int
zarzuc(string str)
{
    notify_fail("Chyba raczej 'zarzu� sie� [gdzie]'?\n");

    if(!str)
		return 0;

    object *siec;
    string gdzie;
    if(!parse_command(lower_case(str), TP, " %i:"+PL_BIE+" / %i:"+PL_BIE+" %s ", siec, gdzie))
		return 0;

    siec = NORMAL_ACCESS(siec, 0, 0);

    if(!sizeof(siec) || !siec)
		return 0;

    if(sizeof(siec) > 1)
    {
		notify_fail("Mo�esz zarzuci� tylko jedn� sie� na raz!\n");
		return 0;
    }

    if(siec[0] != TO)
		return 0;

    if(gdzie_zarzucona)
    {
		notify_fail("Ale� sie� jest ju� zarzucona!\n");
		return 0;
    }

    string *lowiska = ENV(TP)->query_lowiska();

    if(!sizeof(lowiska) || !lowiska)
    {
		notify_fail("Niestety, w pobli�u nie dostrzegasz �adnego miejsca nadaj�cego"
			+" si� do po�owu ryb.\n");
		return 0;
    }

    if(!strlen(gdzie))
    {
		if(sizeof(lowiska) == 1)
	    	gdzie = lowiska[0];

		if(sizeof(lowiska) > 1)
		{
	    	notify_fail("Jest tu kilka miejsc w kt�rych mo�na �owi�. Sprecyzuj w kt�rym"
		    	+" z nich chcesz zarzuci� sie�.\n");
	    	return 0;
		}
    }

    if(member_array(gdzie, lowiska) == -1)
    {
		notify_fail("Nie mo�esz zarzuci� tam sieci!\n");
		return 0;
    }
    
    /* Robimy ograniczenie - 2 sieci na lowisko */
    int ile_sieci = 0;
    object *ob = all_inventory(ENV(TP));
    ob = filter(ob, &->query_is_siec_rybacka());
    int size = sizeof(ob);

    for(int i = 0; i < size; i++)
	if(ob[i]->query_gdzie_zarzucona() ~= gdzie)
	    ile_sieci++;

    if(ile_sieci >= 2)
    {
		write("W tym miejscu nie mo�esz ju� zarzuci� kolejnej sieci!\n");
		return 1;
    }

    gdzie_zarzucona = gdzie;
    add_prop(OBJ_M_NO_GET, "W tej chwili sie� jest zarzucona, mo�esz j� wyci�gn��.\n");
    alarm_id = set_alarm(itof(min_time+random(max_time-min_time+1)), 0.0, "napelnij");
    TO->move(ENV(TP));

    write("Robisz du�y zamach i zarzucasz "+short(PL_BIE)+" "+gdzie+".\n");
    saybb(QCIMIE(TP, PL_MIA)+" robi du�y zamach i zarzuca "
	    +short(PL_BIE)+" "+gdzie+".\n");

    /* Dodajemy przymiotnik */
    obj_przym[0] = ({"zarzucony"})+obj_przym[0];
    obj_przym[1] = ({"zarzuceni"})+obj_przym[1];
    odmien_short();
    odmien_plural_short();

    return 1;
}

int wyciagnij(string str)
{
    notify_fail("Co chcesz wyci�gn�� z wody?\n");

    if(!str)
	return 0;

    object *siec;
    if(!parse_command(lower_case(str), ENV(TP), " %i:"+PL_BIE, siec))
	return 0;

    siec = NORMAL_ACCESS(siec, 0, 0);

    if(!sizeof(siec) || !siec)
	return 0;

    if(sizeof(siec) > 1)
    {
	notify_fail("Mo�esz wyci�gn�� tylko jedn� sie� na raz!\n");
	return 0;
    }

    if(siec[0] != TO)
	return 0;

    if(!gdzie_zarzucona)
    {
	notify_fail("Ale� ta sie� nie jest nigdzie zarzucona!\n");
	return 0;
    }

    remove_adj("zarzucony");
    odmien_short();
    odmien_plural_short();

    if(sizeof(zlowione))
	TP->catch_msg("Wyci�gasz "+short(PL_MIA)+" z wody, a wraz z ni� "
		    +COMPOSITE_DEAD(zlowione, PL_BIE)+".\n");
    else
	TP->catch_msg("Wyci�gasz "+short(PL_MIA)+" z wody, lecz niestety,"
		    +" jest pusta.\n");

    saybb(QCIMIE(TP, PL_MIA)+" wyci�ga "+short(PL_MIA)+" z wody.\n");

    remove_prop(OBJ_M_NO_GET);

    zlowione->move(TP);
    TO->move(TP, 0);

    gdzie_zarzucona = 0;
    zlowione = ({ });
    remove_alarm(alarm_id);

    return 1;
}


void napelnij()
{
    if(sizeof(zlowione) >= pojemnosc)
	return;

    zlowione += ({ clone_object(RYBKA) });

    alarm_id = set_alarm(itof(min_time+random(max_time-min_time+1)), 0.0, "napelnij");
}


void set_pojemnosc(int i)
{
    pojemnosc = i;
}

int query_pojemnosc()
{
    return pojemnosc;
}

void set_min_time(int i)
{
    min_time = i;
}

int query_min_time()
{
    return min_time;
}

void set_max_time(int i)
{
    max_time = i;
}

int query_max_time()
{
    return max_time;
}

string query_gdzie_zarzucona()
{
    return gdzie_zarzucona;
}

int query_is_siec_rybacka()
{
    return 1;
}



