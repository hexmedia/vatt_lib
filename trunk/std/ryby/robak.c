//to� to d�d�ownica!

inherit "/std/object";
#include <macros.h>
#include <stdproperties.h>

int alarm_id;

void pelzamy();
int rozdepcz(string str);

void
create_object()
{
	ustaw_nazwe("robak");

    dodaj_nazwy("przyn�ta");

	dodaj_przym("r�owawy", "r�owawi");
	dodaj_przym("�liski", "�liscy");

	set_long("Zar�wo kolor jak i charakterystyczny kszta�t"
			+" tego robaka wskazuj�, i� jest to pospolita"
			+" d�d�ownica. Robak nieustannie wije si� wyginaj�c"
			+" swoje cia�o na wszelkie mo�liwe sposoby -"
			+" wida�, �e najch�tniej znalaz�by si� g��boko"
			+" pod ziemi�. Wiadomo, i� d�d�ownice s� przysmakiem"
			+" mniejszych ryb, wobec czego stawoni� doskona��"
			+" na nie przyn�t�.\n");

	add_prop(OBJ_I_WEIGHT, 3);
	add_prop(OBJ_I_VOLUME, 1);
	add_prop(OBJ_I_VALUE, 0);
}

string
query_przyneta_typ()
{
    return "robak";
}

init()
{
    ::init();
    add_action(&rozdepcz(), "rozdepcz");
    add_action(&rozdepcz(), "zdepcz");
    add_action(&rozdepcz(), "rozgnie�");
}


void enter_env(object from, object old)
{
	if(alarm_id)
		remove_alarm(alarm_id);

	alarm_id = set_alarm(50.0+itof(random(60)), 0.0, "pelzamy");
}

void pelzamy()
{
	if(!objectp(ENV(TO)))
		return;

	if(function_exists("create_container", ENV(TO)) == "/std/room")
	{
		tell_roombb(ENV(TO), capitalize(short())+" wpe�za w jaki�"
				+" zakamarek zupe�nie znikaj�c ci z oczu.\n", 0, TO);
		remove_object();
        return;
	}

	if(interactive(ENV(TO)))
	{
		if(!objectp(ENV(ENV(TO))))
			return;

		ENV(TO)->catch_msg(capitalize(short())+" pe�za przez chwil�"
				+" po twoim ciele, po czym spada na ziemi�.\n");
		TO->move(ENV(ENV(TO)));
	}
}

int rozdepcz(string str)
{
    notify_fail("Co chcesz rozdepta�?\n");

    if(!str)
        return 0;

    object robak;
    if(!parse_command(lower_case(str), ENV(TP), " %o:"+PL_BIE, robak))
        return 0;

    if(!robak)
        return 0;

    if(robak != TO)
        return 0;

    if(function_exists("create_container", ENV(TO)) != "/std/room")
    {
        notify_fail("Aby robak m�g� zosta� rozdeptany musi znajdowa� si� na ziemi.\n");
        return 0;
    }

    TP->catch_msg("Z nijak� satysfakcj� rozdeptujesz "+short(PL_BIE)
            +" i rozmazujesz jego pozosta�o�ci na ziemi.\n");
    saybb(QCIMIE(TP, PL_MIA)+" z nijak� satysfakcj� rozdeptuje "+short(PL_BIE)
            +" i rozmazuje jego pozosta�o�ci na ziemi.\n");

    set_alarm(0.1, 0.0, "remove_object");

    return 1;
}

string
query_pomoc()
{
    return "Zdaje si�, i� tego oto �licznego robaczka mo�na rozdepta� lub te�"
            +" u�y� go jako przyn�ty na ryb�.\n";
}

public int
query_dzdzownica()
{
    return 1;
}
