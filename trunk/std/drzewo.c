/**
 *
 * \file /std/drzewo.c
 *
 * Drzewo.
 *  wersja 2, lipiec 2007
 *
 * Info:
 *
 * Wartosci graniczne:
 *  - waga drzewa - od 50000 do 1500000 (gram)
 *  - czas scinania - od 15 do 120 (sekund)
 *
 *  Punkty na podstawie ktorych wyliczano funkcje
 *  obliczajaca czas scinania:
 *
 *  Kondycja    Skill   Czas teoretyczny    Czas zwracany przez funkcje
 *                                                  (rzeczywisty)
 *
 *  50000         0         50                        48
 *  1500000       0         120                       120
 *  50000       100         15                        19
 *  1500000     100         50                        48
 *
 * Postac funkcji:
 *
 * t(kondycja, skill) = [a*(kondycja)^b]*c^(skill)
 *
 * a = 2.61707549101924
 * b = 0.269402398091003
 * c = 0.990878944206786
 *
 * Wyprowadzona przy pomocy programu DataFit(polecam) :P
 * Czas ociosywania jest rowny 1.1*(czas scianania)
 *
 *
 * Jak to dzia�a?
 *
 *      Drzewo zostaje �ci�te/ociosane, gdy jego kondycja b�dzie r�wna 0
 * lub spadnie poni�ej zera. Co kilka sekund wywo�ywany jest alarm,
 * kt�ry zmniejsza ow� kondycj� o warto��:
 *   pocz�tkowy_czas_�cinania/cz�sto��_wywo�ywania_alarmu
 * Gdzie pocz�tkowy czas to czas zwr�cony przez wy�ej zaprezentowan�
 * funkcj� gdy gracz zaczyna� �cina� drzewo.
 *
 * @author Rantaur
 * @date 2.07.2007
 */

inherit "/std/object";

#include <cmdparse.h>
#include <composite.h>
#include <exp.h>
#include <macros.h>
#include <math.h>
#include <materialy.h>
#include <object_types.h>
#include <pl.h>
#include <stdproperties.h>
#include <filter_funs.h>
#include <ss_types.h>
#include <sit.h>

#define DBG(x) find_player("root")->catch_msg(x+"\n")

/* Nasz 'skill' czyli 80% drwalstwa + 20% si�y */
#define SKILL(x) ftoi(0.8*itof(x->query_skill(SS_WOODCUTTING)) + \
                   0.2*itof(x->query_stat(SS_STR)))

/* A tutaj rzeczywisty skill ;) */
#define WOODCUTTING_SKILL this_player()->query_skill(SS_WOODCUTTING) /* KRUCAFIX RANT NIE DAWAJ SKLLI NUMERKAMI */

/* Tyle zmeczenia bedzie odejmowane za ka�dym wywo�aniem alarmu */
#define ZMECZENIE -5

/* Wylicza czas scinania */
#define F_CZAS_SCINANIA(con, skill) \
        2.61707549101924 * \
        pow(itof(con), 0.269402398091003) * \
        pow(0.990878944206786, itof(skill))


#define ZETNIJ_EVENTY(p, t)                                                                                    \
({                                                                                                      \
    ({                                                                                                  \
        "Bierzesz lekki zamach i z impetem uderzasz " + t->query_nazwa(PL_NAR) + " w drzewo.",          \
        QCIMIE(p, PL_MIA) + " bierze lekki zamach i z impetem uderza " + t->query_nazwa(PL_NAR) +       \
            " w drzewo."                                                                                \
    }),                                                                                                 \
    ({                                                                                                  \
        "Przez kr�tk� chwil� masz wra�enie, �e drzewo przechyla si� w jedn� stron�."                    \
    }),                                                                                                 \
    ({                                                                                                  \
        "Z wn�trza drzewa dochodzi przeci�g�y trzask.",                                                 \
        "Z wn�trza drzewa �cinanego przez " + QIMIE(p, PL_BIE) + " dochodzi przeci�g�y trzask."         \
    }),                                                                                                 \
    ({                                                                                                  \
        "Uderzasz " + t->query_nazwa(PL_NAR) + " w drzewo, a z jego korony sypi� si� ma�e, "+           \
            "pousychane ga��zki.",                                                                      \
        QCIMIE(p, PL_MIA) + " uderza " + t->query_nazwa(PL_NAR) + " w drzewo, a z jego korony sypi� "+  \
            "si� ma�e, pousychane ga��zki."                                                             \
    }),                                                                                                 \
    ({                                                                                                  \
        "Dochodzi ci� wyra�ny zapach �wie�ego drewna.",                                                 \
        "Podmuch wiatru przyni�s� ze sob� wyra�ny zapach �wie�ego drewna."                              \
    }),                                                                                                 \
    ({                                                                                                  \
        "Miarowo uderzasz " + t->query_nazwa(PL_NAR) + " w " + query_nazwa(PL_BIE) + ".",               \
        QCIMIE(p, PL_MIA) + " miarowo uderza " + t->query_nazwa(PL_NAR) + " w " + query_nazwa(PL_BIE) + \
            "."                                                                                         \
    }),                                                                                                 \
    ({                                                                                                  \
        "Bierzesz szeroki zamach i krzepkim uderzeniem od�upujesz od " + query_nazwa(PL_DOP) +          \
            " kawa�ek kory.",                                                                           \
        QCIMIE(p, PL_MIA)+" bierze szeroki zamach i krzepkim uderzeniem od�upuje od " +                 \
            QSHORT(TO, PL_DOP) + " kawa�ek kory."                                                       \
    }),                                                                                                 \
    ({                                                                                                  \
        "Cofasz si� o krok by po chwili kopn�� z impetem w " + short(p, PL_BIE) + " " +                 \
            "i oceni� jak d�ugo przyjdzie ci " + TO->koncowka("go", "j�", "je") + " jeszcze �cina�." ,  \
        QCIMIE(p, PL_MIA)+" cofa si� o krok od " + QSHORT(TO, PL_DOP) + " by po chwili, za " +          \
            "pomoc� krzepkiego kopni�cia, oceni� jak d�ugo przyjdzie " + p->koncowka("mu", "jej") +     \
            " jeszcze " + TO->koncowka("go", "j�", "je") + " �cina�."                                   \
    }),                                                                                                 \
    ({                                                                                                  \
        "Nag�y podmuch wiatru rozwia� po okolicy nieco trocin.",                                        \
        "Nag�y podmuch wiatru rozwia� po okolicy nieco trocin."                                         \
    })                                                                                                  \
});

#define OCIOSAJ_EVENTY(p, t)                                                                            \
({                                                                                                      \
    ({                                                                                                  \
        "Szybkim ruchem �amiesz niewielk� ga��zk�, kt�ra zahaczy�a o tw� r�k�.",                        \
        QCIMIE(p, PL_MIA)+" szybkim ruchem od�amuje od " + short(TP, PL_DOP) + " niewielk� " +          \
            "ga��zk�."                                                                                  \
    }),                                                                                                 \
    ({                                                                                                  \
        "Potykasz si� o jedn� z ga��zi.",                                                               \
        QCIMIE(p, PL_MIA) + " potyka si� o jak�� ga���."                                                \
    }),                                                                                                 \
    ({                                                                                                  \
        "Lekko podcinasz ga���, by za chwil� z�ama� j� krzepkim kopni�ciem.",                           \
        QCIMIE(p, PL_MIA) + " lekko podcina jedn� z ga��zi " + short(TP, PL_DOP) + ", by za chwil� " +  \
            "z�ama� j� krzepkim kopni�ciem."                                                            \
    }),                                                                                                 \
    ({                                                                                                  \
        "Z uporem rozsuwasz ga��zie, by odci�� je tu� przy nasadzie.",                                  \
        QCIMIE(p, PL_MIA)+" z uporem rozsuwa ga��zie " + short(PL_DOP) + ", by odci�� je tu� przy " +   \
            "nasadzie."                                                                                 \
    })                                                                                                  \
})

#define ZETNIJ_EVENTY_NA_DRZEWIE(p, t)                                                                  \
    ({                                                                                                  \
        "Drzewo zatrz�s�o si� od uderzenia siekier�.\n",                                                \
        "Z wn�trza drzewa dochodzi przeci�g�y trzask.\n",                                               \
        "Masz wra�enie, �e drzewo lekko przechyla si� w jedn� stron�.\n",                               \
        "Drzewo przechodz� wibracje od rytmicznych uderze� siekier�.\n"                                 \
    });


#define TREE_I_LAST_EVENT       "_tree_i_last_event"

/* ------------------------------------------------------------------- */
/* ----------------------------- ZMIENNE ----------------------------- */
/* ------------------------------------------------------------------- */

int         galezie;                /* Ilosc galezi jakie mozna urwac z drzewa*/

int         alarm_wyryj;            /* Id alarmu przy ryciu napisu */
int         start_con;              /* Kondycja jaka mialo drzewo gdy gracz
                                       zaczal je scinac */

float       cena;                   /* Cena za 100kg drewna */
float       gestosc;                /* Gestosc drewna */
int         con_zetnij;             /* Kondycja do scinania */
int         con_ociosaj;            /* Kondycja do ociosywania */

string      sciezka_klody,          /* Sciezka do obiektu klody drzewa */
            sciezka_galezi;         /* Sciezka do obiektu galezi drzewa */

string      napis;                  /* Napis wyryty na pniu */
string      napis_autor;            /* Autor powyzszego napisu */

object      nadrzewie; 				/* Odpowiednia lokacja na drzewie */


/* ------------------------------------------------------------------- */
/* ----------------------------- FUNKCJE ----------------------------- */
/* ------------------------------------------------------------------- */

void dodaj_przym_wagi();
void set_sciezka_galezi(string str);
void set_sciezka_klody(string str);

void
create_tree()
{
    ustaw_nazwe("sosna");
    dodaj_przym("zielony", "zieleni");

    set_long("Jest to najzwyczajniejsze w �wiecie drzewo.\n");

    set_sciezka_galezi("/std/drzewo/galaz.c");
    set_sciezka_klody("/std/drzewo/kloda.c");

    add_prop(OBJ_I_WEIGHT, 500000);
}

nomask void
create_object()
{
    add_prop(OBJ_M_NO_GET, "Chyba sobie �artujesz...\n");
    add_prop(OBJ_I_DONT_GLANCE, 1);
    add_prop(OBJ_I_DONT_SHOW_IN_LONG, 1);

    galezie = random(4)+2;
    gestosc = 0.6;

    create_tree();

    dodaj_nazwy("drzewo");
    dodaj_przym_wagi();
    set_long(query_long()+"@@desc_stan@@");
}

void add_prop(string prop, mixed val)
{
    ::add_prop(prop, val);

    if(prop ~= OBJ_I_WEIGHT)
    {
        if(floatp(gestosc) && gestosc > 0.0)
            ::add_prop(OBJ_I_VOLUME, ftoi(itof(val)/gestosc) + ftoi(pow(-1.0, itof(random(2))))*random(6000));

        con_zetnij = val;
        con_ociosaj = val;
    }
}


/* ------------------------------------------------------------------- */
/* ---------------------- FUNKCJE QUERY & SET ------------------------ */
/* ------------------------------------------------------------------- */


/**
 * Ustawia cene danego gatunku drzewa.
 * @param ile Cena za 100 kg drewna danego gatunku
 */
void
set_cena(float ile)
{
    cena = ile;
}

/**
 * Zwraca cene za dany gatunek drzewa.
 * @return Cena za 100 kg drewna danego gatunku
 */
float
query_cena()
{
    return cena;
}

/**
 * Ustawia gestosc danego gatunku drzewa
 * @param f Gestosc
 */
void
set_gestosc(float f)
{
    gestosc = f;
}


/**
 * Zwraca kondycje scinanego drzewa.
 * @return Kondycja drzewa
 */
int
query_con_zetnij()
{
    return con_zetnij;
}

/**
 * Modyfikuje kondycje scinanego drzewa
 * @param ile Jaka liczbe chcemy dodac do kondycji
 */
void
modify_con_zetnij(int ile)
{
    con_zetnij += ile;
}

/**
 * Ustawia sciezke do obiektu galezi danego drzewa
 * @param str Sciezka do galezi
 */
void
set_sciezka_galezi(string str)
{
    sciezka_galezi = str;
}

/**
 * Zwraca sciezke do obiektu galezi danego drzewa
 * @return Sciezka do obiektu galezi
 */
string
query_sciezka_galezi()
{
    return sciezka_galezi;
}

/**
 * Ustawia sciezke do obiektu klody danego drzewa
 * @param str Sciezka do klody
 */
void
set_sciezka_klody(string str)
{
    sciezka_klody = str;
}

/**
 * Zwraca sciezke do obiektu klody danego drzewa
 * @return Sciezka do obiektu klody
 */
string
query_sciezka_klody()
{
    return sciezka_klody;
}

/**
 * Zwraca czas przez jaki byloby scinane drzewo
 * @param c Poczatkowa kondycja drzewa
 * @param s Skill gracza
 * @return Czas w sekundach
 */
int
query_czas_scinania(int c, int s)
{
    return ftoi(F_CZAS_SCINANIA(c, s));
}

/**
 * Zwraca czas przez jaki byloby ociosywane drzewo
 * @param c Poczatkowa kondycja drzewa
 * @param s Skill gracza
 * @return Czas w sekundach
 */
int
query_czas_ociosywania(int c, int s)
{
    return ftoi(1.1*(F_CZAS_SCINANIA(c, s)));
}

/**
 * Sprawdza czy drzewo jest sciete.
 * @return 1 - gdy jest, 0 - gdy nie jest
 */
int
query_sciete()
{
    if(con_zetnij <= 0)
        return 1;
    else
        return 0;
}

/**
 * Jesli drzewo jest mlode, tudziez male
 * to zwraca 1
 */
int query_mlode()
{
    if(query_prop(OBJ_I_WEIGHT) < 400000)
        return 1;
    else
        return 0;
}

/**
 * Zwraca odpowiadajaca drzewu lokacje
 * @return Lokacja
 */
object
query_na_drzewie()
{
    return nadrzewie;
}

/**
 * Ustawia odpowiadajaca drzewu lokacje
 * @param ob Lokacja
 */
void
set_na_drzewie(object ob)
{
    if(nadrzewie)
        if (ENV(TO))
            ENV(TO)->remove_exit(""+OB_NUM(nadrzewie));

    nadrzewie = ob;
    if (ENV(TO))
    {
        ENV(TO)->remove_exit(""+OB_NUM(nadrzewie));
        ENV(TO)->add_exit(file_name(nadrzewie), ""+OB_NUM(nadrzewie), 1, 1, 1);
    }
}


/* ------------------------------------------------------------------- */
/* -------------------- FUNKCJE ROZNE, POMOCNICZE -------------------- */
/* ------------------------------------------------------------------- */

/**
 * Ustawia przymiotnik zalezny od wagi drzewa.
 */
void
dodaj_przym_wagi()
{
    int w = query_prop(OBJ_I_WEIGHT);

    if(w >= 50000 && w < 110000)
        dodaj_przym("m�ody", "m�odzi");
    if(w >= 110000 && w < 320000)
        dodaj_przym("dojrza�y", "dojrzali");
    if(w >= 320000 && w < 600000)
        dodaj_przym("leciwy", "leciwi");
    if(w >= 600000 && w < 900000)
        dodaj_przym("stary", "starzy");
    if(w >= 900000 && w < 1200000)
        dodaj_przym("wiekowy", "wiekowi");
    if(w >= 1200000)
        dodaj_przym("pot�ny", "pot�ni");

    odmien_short();
    odmien_plural_short();
}

/**
 * Sprawdza czy gracz ma dobyt� bron okreslonego typu
 * @param kto U kogo sprawdzamy
 * @param jakie Tablica dozwolonych typow obiekt�w
 * @return Tablica pasujacych broni lub zero w przypadku braku takowych
 */
object *check_wielded_weapons(object kto, int *jakie)
{
    object *wielded = kto->subinventory("wielded");
    object *ret = ({ });

    if(!sizeof(wielded))
        return 0;

    if(member_array(wielded[0]->query_type(), jakie) != -1)
        ret += ({wielded[0]});

    if(sizeof(wielded) == 2)
        if(member_array(wielded[1]->query_type(), jakie) != -1)
            ret += ({wielded[1]});

    if(!sizeof(ret))
        return 0;

    return ret;
}

/**
 * Zwraca lokacj� w kt�rej znajduje si� obiekt
 * @return Obiekt lokacji
 */
object find_room()
{
    object tmp = this_object();

    while(function_exists("create_container", tmp) != "/std/room")
        tmp = environment(tmp);

    return tmp;
}

/**
 * Opisuje 'stan' drzewa
 */
string
desc_stan()
{
    string ret = "";

    /* Kondycja drzewa */
    if(!query_sciete())
    {
        float proc = itof(con_zetnij)/itof(query_prop(OBJ_I_WEIGHT));

        if(proc >= 0.6 && proc < 0.9)
            ret += koncowka("Jego", "Jej", "Jego")+" pie� jest lekko nadci�ty.\n";

        if(proc > 0.3 && proc < 0.6)
        {
            ret += "Na pniu zauwa�asz g��bokie naci�cie, najwyra�niej"
                    +" kto� pr�bowa� ju� �ci�� to drzewo.\n";
        }

        if(proc <= 0.3)
        {
            ret += "Pie� drzewa jest bardzo mocno nadci�ty, masz wra�enie, �e"
                +" wystarczy�oby kilka krzepkich uderze� toporem, aby zwali�"
                +" je na ziemie.\n";
        }
    }
    else
        ret += "Jest �ci�t"+koncowka("y","a","e")+".\n";


    /* Napis na drzewie */
    if(strlen(napis) > 0)
        ret += "Na pniu wyryty jest napis: "+napis+".\n";


    /* Kto siedzi na drzewie */
    if (objectp(nadrzewie))
    {
        object* kto;

        kto = FILTER_LIVE(all_inventory(nadrzewie));
        kto = FILTER_CAN_SEE(kto, TP);

        if (sizeof(kto))
            ret += "Na drzewie widzisz " + COMPOSITE_LIVE(kto, PL_BIE) + ".\n";
    }

    return ret;
}

int
wyryj_stop()
{
    remove_alarm(alarm_wyryj);

    return 0;
}

void
wyryj_finish(string tekst, object autor)
{

    autor->catch_msg("Uda�o ci si� wyry� napis.\n");

    napis = tekst;
    napis_autor = autor->query_real_name();
}


/* ------------------------------------------------------------------- */
/* ------------------------ MECHANIKA SCINANIA ----------------------- */
/* ------------------------------------------------------------------- */

/*
 * Eventy do scinania drzewa.
 */
void
zetnij_event()
{
    int last_event = query_prop(TREE_I_LAST_EVENT);
    object player = query_prop(TREE_O_CUTTER);
    object tool = query_prop(TREE_O_CUTTER_TOOL);

    if(!player || !tool)
        return;

    mixed *eventy = ZETNIJ_EVENTY(player, tool);

    int index;

    if(sizeof(eventy) > 1)
        while(last_event == (index = random(sizeof(eventy))));

    change_prop(TREE_I_LAST_EVENT, index);

    player->catch_msg(eventy[index][0]+"\n");

    if(sizeof(eventy[index]) == 2)
        saybb(eventy[index][1]+"\n", ({ player }));

    /* I eventy dla osob na drzewie */
    if(!objectp(nadrzewie))
        return;

    string *eventy_na_drzewie = ZETNIJ_EVENTY_NA_DRZEWIE(player, tool);

    tell_room(query_na_drzewie(), eventy_na_drzewie[random(sizeof(eventy_na_drzewie))]);
}

void
ociosaj_event()
{
    int last_event = query_prop(TREE_I_LAST_EVENT);
    object player = query_prop(TREE_O_CUTTER);
    object tool = query_prop(TREE_O_CUTTER_TOOL);

    if(!player || !tool)
        return;

    mixed *eventy = OCIOSAJ_EVENTY(player, tool);

    int index;

    if(sizeof(eventy) > 1)
        while(last_event == (index = random(sizeof(eventy))));

    change_prop(TREE_I_LAST_EVENT, index);

    player->catch_msg(eventy[index][0]+"\n");

    if(sizeof(eventy[index]) == 2)
        saybb(eventy[index][1]+"\n", ({ player }));
}


/**
 * Wywo�ywana gdy gracz przestanie �cina�/obrabia� drzewo
 */
public varargs int
stop_work(mixed silent = 0)
{
    object paraliz;
    object player = query_prop(TREE_O_CUTTER);

    remove_prop(TREE_O_CUTTER);
    remove_prop(TREE_O_CUTTER_TOOL);

    // Bo paraliz przekazuje do funkcji tablice
    if(pointerp(silent))
        if(sizeof(silent) > 1)
            silent = silent[1];
        else
            silent = 0;

    if(objectp(paraliz = present("scinka_paraliz_zetnij", player)))
    {
        if(!silent)
        {
            player->catch_msg("Przestajesz �cina� "+short(PL_BIE)+".\n");
            saybb(QCIMIE(player, PL_MIA)+" przestaje �cina� "+short(PL_BIE)+".\n");
        }
    }
    else if(objectp(paraliz = present("scinka_paraliz_ociosaj", player)))
    {
        if(!silent)
        {
            player->catch_msg("Przestajesz obrabia� "+short(PL_BIE)+".\n");
            saybb(QCIMIE(player, PL_MIA)+" przestaje obrabia� "+short(PL_BIE)+".\n");
        }

    }
    else
        return 0;

    paraliz->remove_object();

    return 1;
}

void
zetnij_finish()
{
    con_zetnij = 0;

    object player = query_prop(TREE_O_CUTTER);
    object tool = query_prop(TREE_O_CUTTER_TOOL);

    /* Opisujemy */
    player->catch_msg("Zauwa�asz jak "+short(PL_MIA)+" zaczyna ko�ysa� si� na boki."
            +" Zbieraj�c w sobie wszystkie si�y zaciskasz d�onie na r�koje�ci "
            +tool->query_nazwa(PL_DOP)+" i szybkim, pewnym ruchem trafiasz w sam"
            +" �rodek wy��obionej dziury. Z wnetrza drzewa daje si� s�yszec g�o�ny"
            +" trzask, a ty z zadowoleniem przygl�dasz si� jak przechyla si� ono na"
            +" jedn� stron� i z hukiem uderza o ziemi�, wyrzucaj�c wszystkie trociny"
            +" w powietrze.\n");

    saybb("�cinane przez "+QIMIE(player, PL_BIE)+" drzewo zaczyna ko�ysa� si� na boki."
        +" Nie czekaj�c ani sekundy "+QIMIE(player, PL_MIA)+" szybkim i pewnym ruchem uderza"
        +" w sam �rodek wy��obionej w pniu dziury. Po chwili drzewo leniwie"
        +" zwala si� na ziemi� wyrzucaj�c w g�r� nieco trocin.\n");

    /* Wywalamy rzeczy z lokacji na drzewie */
    if(objectp(nadrzewie))
    {
        object *na_inv = all_inventory(nadrzewie);
        object *livings;

        tell_roombb(nadrzewie, capitalize(short(PL_MIA))+" nagle zaczyna si� chwia� i"
            +" po chwili z �oskotem zwala si� na ziemi�, a ty wraz z "
            +koncowka("nim", "ni�", "nim")+"!\n\n");

        livings = FILTER_LIVE(na_inv);

        na_inv->move(ENV(TO));

        if(sizeof(livings))
        {
            tell_roombb(ENV(TO), "Spod ga��zi �wie�o �ci�tego drzewa wygrzebuje si� "
                +COMPOSITE_LIVE(FILTER_LIVE(na_inv), PL_MIA)+".\n", livings);

            livings->reveal_me();

            foreach(object pl : livings)
            {
                set_this_player(pl);
                pl->do_glance();
            }

            livings->reduce_hp(0, 2000, 10);

            set_this_player(player);
        }
    }

    /* Dodajemy expa */
    int exp_str = ftoi(LINEAR_FUNC(0.0, itof(EXP_SCINANIE_DRZEW_STR_MIN),
        1500000.0, itof(EXP_SCINANIE_DRZEW_STR_MAX),
        itof(query_prop(OBJ_I_WEIGHT))));

    int exp_skill =  ftoi(LINEAR_FUNC(0.0, itof(EXP_SCINANIE_DRZEW_WOODCUTTING_MIN),
        1500000.0, itof(EXP_SCINANIE_DRZEW_WOODCUTTING_MAX),
        itof(query_prop(OBJ_I_WEIGHT))));

    player->increase_ss(0, exp_str);
    player->increase_ss(114, exp_skill);

    /* Dodajemy przymiotnik */
    obj_przym[0] = ({"�ci�ty"})+obj_przym[0];
    obj_przym[1] = ({"�ci�ci"})+obj_przym[1];
    odmien_short();
    odmien_plural_short();

    stop_work(1);

    /* Zeby sciete drzewo bylo pierwsze w kolejce */
    object env = ENV(TO);
    TO->move("/d/Standard/Redania/Rinde_okolice/Las/lokacje/tartak_magazyn");
    TO->move(env);
}

/*
 * Funkcja wywolywana pod koniec ociosywania
 */
void
ociosaj_finish()
{
    con_ociosaj = 0;

    object player = query_prop(TREE_O_CUTTER);
    object tool = query_prop(TREE_O_CUTTER_TOOL);

    /* Opisujemy */
    saybb(QCIMIE(player, PL_MIA)+" odcina jeszcze kilka mniejszych ga��zi,"
       +" po czym ko�czy obrabia� "+short(PL_BIE)+".\n");
    player->catch_msg("Odcinasz jeszcze kilka mniejszych ga��zi, po czym"
       +" ko�czysz obrabia� "+short(PL_BIE)+".\n");

    /* Dodajemy expa */
    int exp_str = ftoi(LINEAR_FUNC(0.0, itof(EXP_SCINANIE_DRZEW_STR_MIN),
        1500000.0, itof(EXP_SCINANIE_DRZEW_STR_MAX),
        itof(query_prop(OBJ_I_WEIGHT))));

    int exp_skill =  ftoi(LINEAR_FUNC(0.0, itof(EXP_SCINANIE_DRZEW_WOODCUTTING_MIN),
        1500000.0, itof(EXP_SCINANIE_DRZEW_WOODCUTTING_MAX),
        itof(query_prop(OBJ_I_WEIGHT))));

    player->increase_ss(0, exp_str);
    player->increase_ss(114, exp_skill);

    stop_work(1);

    /* Klonujemy galezie */
    int masa_galezi = ftoi(0.056*itof(query_prop(OBJ_I_WEIGHT))+7207.0)+random(1500);
    int ile_galezi = ftoi(0.00000483*itof(query_prop(OBJ_I_WEIGHT))+2.76)+random(4);
    int srednia = masa_galezi/ile_galezi;
    int rand_masa;

    object galaz;

    for(int i=0; i < ile_galezi; i++)
    {
        if(i%2 != 1)
        {
            rand_masa=random((3*srednia)/20);
            galaz = clone_object(sciezka_galezi);
            galaz->add_prop(OBJ_I_WEIGHT, srednia+rand_masa);
        }
        else
        {
            galaz = clone_object(sciezka_galezi);
            galaz->add_prop(OBJ_I_WEIGHT, srednia-rand_masa);
        }

        galaz->move(ENV(TO));
    }

    object kloda = clone_object(sciezka_klody);

    kloda->set_gestosc(gestosc);
    kloda->set_cena(cena);
    kloda->set_kloda_owner(player);
    kloda->add_prop(OBJ_I_WEIGHT, (query_prop(OBJ_I_WEIGHT)-masa_galezi));
    kloda->remove_owner_alarm();

    kloda->move(ENV(TO), 1);

    remove_object();
}

/**
 */
void
beat()
{
    int     zetnij;
    object  bron = query_prop(TREE_O_CUTTER_TOOL);
    object  player = query_prop(TREE_O_CUTTER);
    object  paraliz = previous_object();

    zetnij = (paraliz->query_name() == "scinka_paraliz_zetnij");

    if(!objectp(player))
        return;

    // Czy gracz zada kolejne uderzenie
    if(player->query_old_fatigue() + ZMECZENIE < 0)
    {   //Nie zada
        if(zetnij)
        {
            player->catch_msg("Zm�czenie sprawia, i� nie mo�esz dalej " +
                "�cina� "+short(PL_DOP)+".\n");
        }
        else
        {
            player->catch_msg("Zm�czenie sprawia, i� nie mo�esz dalej " +
                "obrabia� "+short(PL_DOP)+".\n");
        }

        //Usuwamy parali�.
        paraliz->remove_object();
        stop_work();

        return;
    }

    //zada
    player->add_old_fatigue(ZMECZENIE);

    // T�pimy bro�
    if(objectp(bron))
        bron->did_parry();

    // Je�li bro� p�k�a, przestajemy �cina�
    if(bron->query_prop(OBJ_I_BROKEN))
    {
        stop_work();
        paraliz->remove_object();
        return;
    }

    //Zmniejszamy kondycje drzewa
    if(start_con <= 0)
    {
        player->catch_msg("Wyst�pi� bardzo mhroczny b��d. Zg�o� go i nie zapomnij " +
            "napisa�, �e by� mhroczny!\n");
        return;
    }

    int dmg = ftoi(paraliz->query_last_event_time());

    // Jesli scinamy
    if(zetnij)
    {
        dmg *= start_con / query_czas_scinania(start_con, SKILL(player));
        con_zetnij -= dmg;
    }
    // Jesli ciosamy
    else
    {
        dmg *= start_con / query_czas_ociosywania(start_con, SKILL(player));
        con_ociosaj -= dmg;
    }

    // Konczymy scinac/ciosac jesli kondycja spadnie do 0
    if(query_sciete() && zetnij)
    {
        zetnij_finish();
        paraliz->remove_object();
        return;
    }
    else if((con_ociosaj <= 0.0))
    {
        //Ko�czymy ociosywanie
        ociosaj_finish();
        paraliz->remove_object();
        return;
    }
    else if(zetnij)
        zetnij_event();
    else
        ociosaj_event();
}

public mixed
try_zetnij()
{
    if(con_zetnij <= 0)
        return "Ale� to drzewo jest ju� �ci�te!\n";

    if(ENV(TO) == TP)
    {
        return "Drzewo znajduje si� w twoim ekwipunku...lepiej zg�o� b��d"
            +" i opisz jak do tego dosz�o.\n";
    }

    if(TP->query_prop(SIT_SIEDZACY) || TP->query_prop(SIT_LEZACY))
        return "Mo�e najpierw wstaniesz?\n";

    /* Sprawdzamy czy gracz ma czym sciac drzewo */
    object *tools = check_wielded_weapons(TP, ({O_BRON_TOPORY, O_BRON_TOPORY_2H}));

    if(!sizeof(tools) || !tools)
        return "Potrzebujesz topora aby �ci�� drzewo.\n";

    /* Drzewo moze scinac jedna osoba */
    if(objectp(query_prop(TREE_O_CUTTER)))
        return "To drzewo jest ju� �cinane przez kogo� innego.\n";

    if(TP->query_old_fatigue() - ZMECZENIE < 0)
        return "Odpocznij chwil� zanim podejmiesz si� tej czynno�ci.\n";

    /* ------ Opisy ------ */

    if (query_na_drzewie())
    {
        object* all = FILTER_LIVE(all_inventory(query_na_drzewie()));
        tell_room(query_na_drzewie(), "Drzewo zatrz�s�o si� od uderzenia"
            +" siekier�.\n");
    }

    string opis;

    int drwalstwo = WOODCUTTING_SKILL;

    if(drwalstwo < 20)
    {
        opis = "Niezbyt pewnie zaciskasz d�onie na r�koje�ci "
            +tools[0]->query_nazwa(PL_DOP)+" i uderzasz w drzewo"
            +" najmocniej jak potrafisz. Niestety twoje starania nie"
            +" przynios�y zamierzonego efektu - "+tools[0]->query_nazwa(PL_MIA)
            +" wbi�"+tools[0]->koncowka("", "a", "o")+ " si� w drewno na bardzo"
            +" ma�� g��boko��.\n";

        saybb(QCIMIE(TP, PL_MIA)+" niepewnie zaciska d�onie na r�koje�ci "
            +tools[0]->query_nazwa(PL_DOP)+", po czym hukiem uderza w pobliskie"
            +" drzewo. Wygl�da jednak na to, i� "+TP->koncowka("jego", "jej")
            +" starania nie przynios�y zamierzonego efektu - drzewo prawie w"
            +" og�le nie podda�o si� ostrzu "+tools[0]->query_nazwa(PL_DOP)+".\n");
    }
    else if(drwalstwo < 40)
    {
        opis = "Wiedzion"+TP->koncowka("y", "a")+" instynktem pochylasz si� lekko"
            +" opuszczaj�c "+tools[0]->query_nazwa(PL_BIE)+", by niemal w tej"
            +" samej chwili zerwa� "+tools[0]->koncowka("go", "j�", "go")
            +" ku g�rze i po skosie uderzy� w "+TO->short(PL_BIE)+" jak najmocniej."
            +" Drzewo zadr�a�o lekko, jednak b�dzie potrzeba jeszcze wielu takich"
            +" uderze�, by zwali� je na ziemi�.\n";

        saybb(QCIMIE(TP, PL_MIA)+" jak gdyby wiedzion"+TP->koncowka("y", "a")
            +" instynktem pochyla si� lekko opuszczaj�c "+tools[0]->query_nazwa(PL_BIE)
            +", by niemal w tej samej chwili zerwa� "+tools[0]->koncowka("go", "j�", "go")
            +" ku g�rze i po skosie uderzy� w "+TO->short(PL_BIE)+" jak najmocniej."
            +" Przez moment masz wra�enie, �e ziemia zadr�a�a lekko, jednak z pewno�ci� "
            +QIMIE(TP, PL_MIA)+" b�dzie musia�"+TP->koncowka("", "a")
            +" si� jeszcze wiele natrudzi�, by zwali� drzewo na ziemi�.\n");
    }
    else if(drwalstwo < 60)
    {
        opis = "Spogl�dasz oceniaj�co na pie� i wypatrujesz miejsca, w kt�re najlepiej"
            +" by�oby uderzy�. Po chwili odrzucasz "+tools[0]->query_nazwa(PL_BIE)
            +" za siebie i z p�obrotu, staraj�c si� skupi� ca�� si�� w jednym miejscu,"
            +" uderzasz w pie� "+TO->short(PL_DOP)+".\n";

        saybb(QCIMIE(this_player(), PL_MIA)+" spogl�da oceniaj�co na pie� "+TO->short(PL_DOP)
            +" wypatruj�c miejsca, w kt�re najlepiej by�oby uderzy�. Po chwili odrzuca "
            +tools[0]->query_nazwa(PL_BIE)+" za siebie i z p�obrotu, jakby staraj�c si�"
            +" skupi� ca�� si�� w jednym miejscu, uderza w pie� drzewa.\n");
    }
    else if(drwalstwo < 80)
    {
        opis = "Nie zwlekaj�c ani chwili, odruchowo poprawiasz chwyt na "
            +tools[0]->query_nazwa(PL_MIE)+" i spokojnym, wyuczonym ruchem,"
            +" z ca�ej si�y r�biesz w pie� "+TO->short(PL_DOP)+". Ostrze "
            +tools[0]->query_nazwa(PL_DOP)+" bezproblemowo zag��bia si� w drewno,"
            +" a towarzyszy temu g�o�ny huk, kt�ry jednak pr�dko cichnie.\n";

        saybb(QCIMIE(TP, PL_MIA)+" nie zwlekaj�c ani chwili, odruchowo poprawia chwyt na "
            +tools[0]->query_nazwa(PL_MIE)+" i spokojnym, wyuczonym ruchem, z ca�ej si�y"
            +" r�bie w pie� "+TO->short(PL_DOP)+". Ostrze dzier�one"
            +tools[0]->koncowka("go", "j", "go")+" przeze� "+tools[0]->query_nazwa(PL_DOP)
            +" bezproblemowo zag��bia si� w drewno, a towarzyszy temu g�o�ny huk, kt�ry"
            +" jednak pr�dko cichnie.\n");
    }
    else
    {
        opis = "Przez kr�tk� chwil� wodzisz fachowym wzrokiem po pniu drzewa."
            +" Oceniwszy sytuacj� stajesz w lekkim rozkroku, odrzucasz "
            +tools[0]->query_nazwa(PL_BIE)+" za rami� i pewnymi, poci�g�ymi"
            +" ruchami zamachujesz si� na drzewo. Z chwil�, gdy ostrze "
            +tools[0]->query_nazwa(PL_DOP)+" dotyka pnia, po okolicy rozchodzi"
            +" si� pot�ny huk.\n";

        saybb(QCIMIE(this_player(), PL_MIA)+" przez kr�tk� chwil� wodzi fachowym"
            +" wzrokiem po pniu drzewa. Oceniwszy sytuacj� staje w lekkim"
            +" rozkroku, odrzuca "+tools[0]->query_nazwa(PL_BIE)+" za rami�"
            +" i pewnymi, poci�g�ymi ruchami zamachuje si� na drzewo. Z"
            +" chwil�, gdy ostrze dzier�one"+tools[0]->koncowka("go", "j", "go")
            +" przeze� "+tools[0]->query_nazwa(PL_DOP)+" dotyka pnia, po"
            +" okolicy rozchodzi si� pot�ny huk.\n");
    }

    object paraliz = clone_object("/std/paralyze");
    paraliz->set_name("scinka_paraliz_zetnij");
    paraliz->set_fail_message("Teraz �cinasz drzewo, je�li chcesz zrobi�"+
        " co� innego po prostu 'przesta�'.\n");
    paraliz->set_stop_message("");
    paraliz->set_stop_fun("stop_work");
    paraliz->set_stop_verb("przesta�");
    paraliz->set_stop_object(TO);

    paraliz->set_event_time(6, 4); // 6 + random(4)
    paraliz->set_event_fun("beat", TO);

    paraliz->move(TP);

    add_prop(TREE_O_CUTTER, TP);
    add_prop(TREE_O_CUTTER_TOOL, tools[0]);

    start_con = con_zetnij;

    return opis;
}

public mixed
try_ociosaj()
{
    if(con_zetnij > 0)
        return "Ale� najpierw musisz �ci�� to drzewo!\n";

    /* Sprawdzamy czy gracz ma czym sciac drzewo */
    object *tools = check_wielded_weapons(TP, ({O_BRON_TOPORY, O_BRON_TOPORY_2H}));

    if(!sizeof(tools) || !tools)
        return "Do obr�bki drzew potrzebujesz odpowiedniego narz�dzia.\n";

    if(ENV(TO) == TP)
        return "Lepiej by�oby najpierw od�o�y� drzewo.\n";

    if(objectp(query_prop(TREE_O_CUTTER)))
        return "To drzewo jest ju� obrabiane przez kogo� innego.\n";

    if(TP->query_prop(SIT_SIEDZACY) > 0 || TP->query_prop(SIT_LEZACY) > 0)
       return "Mo�e najpierw wstaniesz?\n";

    if(TP->query_old_fatigue()-ZMECZENIE < 0)
    {
        return "Jeste� zbyt zm�czon"+TP->koncowka("y", "a")+" by rozpocz��"
              +" obr�bk�.\n";
    }

    string opis = "Podchodzisz do "+TO->short(PL_DOP)+" i z mozo�em"
        +" zabierasz si� do "+koncowka("jego", "jej", "jego")
        +" obr�bki.\n";

    saybb(QCIMIE(TP, PL_MIA)+" podchodzi do "+TO->short(PL_DOP)+" i z mozo�em"
        +" zabiera si� do "+koncowka("jego", "jej", "jego")+" obr�bki.\n");

    object paraliz = clone_object("/std/paralyze");
    paraliz->set_name("scinka_paraliz_ociosaj");
    paraliz->set_fail_message("Teraz obrabiasz drzewo, je�li chcesz zrobi�"
        +" co� innego po prostu 'przesta�'.\n");

    paraliz->set_stop_message("");

    paraliz->set_stop_fun("stop_work");
    paraliz->set_stop_verb("przesta�");
    paraliz->set_stop_object(TO);

    paraliz->set_event_time(6, 4);
    paraliz->set_event_fun("beat", TO);

    paraliz->move(TP);

    add_prop(TREE_O_CUTTER, TP);
    add_prop(TREE_O_CUTTER_TOOL, tools[0]);
    add_prop(OBJ_M_NO_GET, "Kto� w�a�nie obrabia to drzewo, nie mo�esz"
        +" go teraz zabra�!\n");

    start_con = con_ociosaj;

    return opis;
}


public mixed try_wyryj(string tekst)
{
    if(napis)
        return "Niestety, na tym drzewie kto� ju� wyry� napis.\n";

    if(strlen(tekst) > 20)
        return "Napis mo�e sk�ada� si� z co najwy�ej 20 liter.\n";

    /* Aby wyryc napis gracz musi miec cos sztyletopodobnego */
    object *tools = check_wielded_weapons(TP, ({O_BRON_SZTYLETY}));

    if(!tools)
        return "Aby wyry� napis potrzebujesz no�a lub sztyletu.\n";

    object paraliz = clone_object("/std/paralyze");
    paraliz->set_name("scinka_paraliz_wyryj");
    paraliz->set_fail_message("Teraz ryjesz napis, aby zrobi�"
        +"co� innego wystarczy przesta�.\n");

    int rm_time = 9 + random(12);
    paraliz->set_remove_time(rm_time);

    paraliz->set_stop_message("Przestajesz ry� napis.\n");
    paraliz->set_stop_fun("wyryj_stop");
    paraliz->set_stop_verb("przesta�");
    paraliz->set_stop_object(TO);

    paraliz->move(TP);

    alarm_wyryj = set_alarm(itof(rm_time), 0.0, "wyryj_finish", tekst, TP);

    saybb(QCIMIE(TP, PL_MIA)+" z mozo�em zaczyna ry� na pniu "+TO->short(PL_DOP)
        +" jaki� napis.\n");

    return "Z moz�em zaczynasz ry� napis na pniu "+TO->short(PL_DOP)+".\n";
}

int
wespnij(string str)
{
    object *drzef;
    object paraliz;

    if(query_verb() ~= "wespnij")
       notify_fail("Wespnij si� gdzie?\n");
    else
       notify_fail("Wejd� gdzie?\n");

    if(!strlen(str))
        return 0;

    if(!HAS_FREE_HANDS(this_player()))
    {
        write("Musisz mie� obie d�onie wolne, aby m�c to zrobi�.\n");
        return 1;
    }

    if(TP->query_prop(SIT_SIEDZACY))
    {
        write("Czujesz, �e wspinaczka na siedz�co nie jest twoj� domen�.\n");
        return 1;
    }
    if(TP->query_prop(SIT_LEZACY))
    {
        write("Czujesz, �e wspinaczka na le��co nie jest twoj� domen�.\n");
        return 1;
    }

    if(objectp(nadrzewie))
    {
        //drobna zmiana. Zliczmy lepiej obj�to�ci living�w, ni�
        //ich ilo�� XD Verek.
        object *livingi = FILTER_LIVE(all_inventory(nadrzewie));
        if(sizeof(livingi))
        {
            int objetosc_livingow;

            objetosc_livingow = fold(livingi->query_prop(OBJ_I_VOLUME), &operator(+)(,), 0);

            if(objetosc_livingow >= 200)
            {
                write("Chyba ju� si� tam nie zmie�cisz!\n");
                return 1;
            }
        }
    }


    if(!parse_command(str, environment(TP), "'si^e' 'na' %i:" +PL_BIE,drzef) &&
       !parse_command(str, environment(TP), "'na' %i:" +PL_BIE,drzef))
    {
         return 0;
    }

    drzef = NORMAL_ACCESS(drzef, 0, 0);

    if (!sizeof(drzef))
        return 0;

    if (drzef[0] != TO)
        return 0;

    if (drzef[0]->query_sciete())
        return 0;

    if(TP->query_old_fatigue()-40 < 0)
    {
        write("Jeste� zbyt zm�czon"+TP->koncowka("y", "a")+", by rozpocz�� wspinaczk�.\n");
        return 1;
    }

    if(TP->query_prop(OBJ_I_HIDE))
        TP->command("ujawnij sie");

    write("Zaczynasz wspina� si� na " + drzef[0]->short(PL_BIE) + ".\n");
    saybb(QCIMIE(this_player(), PL_MIA) + " zaczyna wspina� si� na "
        + drzef[0]->short(PL_BIE) + ".\n");

    TP->add_prop("_live_s_extra_short_old", TP->query_prop(LIVE_S_EXTRA_SHORT));
    TP->add_prop(LIVE_S_EXTRA_SHORT, " wspinaj�c" + TP->koncowka("y", "a", "e")
        + " si� na " + drzef[0]->short(PL_BIE));

    if((TP->query_skill(SS_CLIMB) + (TP->query_stat(SS_DEX)/5)) <= 14 + random(10))
    {
        clone_object("/std/drzewo/paraliz_wspina_sie_niedarady")->move(TP);
        return 1;
    }
    if((TP->query_skill(SS_CLIMB) + (TP->query_stat(SS_DEX)/5)) <= 24 + random(10))
    {
        clone_object("/std/drzewo/paraliz_wspina_sie_prawie")->move(TP);
        return 1;
    }

    paraliz = clone_object("/std/drzewo/paraliz_wspina_sie");
    paraliz->set_drzewo(TO);
    paraliz->move(this_player());

    return 1;
}

public mixed try_ulam()
{
    if(galezie == 0)
    {
        return "Na drzewie nie dostrzegasz ga��zi, kt�re by�"+TP->koncowka("", "a")
            +"by� w stanie u�ama�.\n";
    }

    galezie -= 1;

    int waga = random(2000)+500;

    object galaz = clone_object(sciezka_galezi);
    galaz->add_prop(OBJ_I_WEIGHT, waga);
    galaz->move(this_player());

    saybb(QCIMIE(TP, PL_MIA)+" z �atwo�ci� u�amuje ga��� z "+TO->short(PL_DOP)+".\n");

    return "U�amujesz ga��� z "+TO->short(PL_DOP)+".\n";
}

string
query_pomoc()
{
    return "Drzewo mo�esz spr�bowa� �ci�� lub si� na nie wspi��. A je�li "+
        "odczuwasz pal�c� potrzeb� uzewn�trznienia swych emocji "+
        "spr�buj wyry� na nim jaki� napis. Mo�esz tak�e ulama� "+
        "z niego jak�� ga���, oczywi�cie o ile b�dzie to mo�liwe.\n";
}

init()
{
    ::init();
    add_action(&wespnij(),"wespnij");
    add_action(&wespnij(),"wejd�");
    add_action(&wespnij(),"wyjd�");
}

/**
 * Zwraca rzeczywista cene danej klody
 * @return Cena
 */
int query_real_cena()
{
    if(intp(cena))
        cena = 1.0;

    int ret = ftoi(LINEAR_FUNC(0.0, 0.0, 130000.0, cena,
        itof(query_prop(OBJ_I_WEIGHT))));

    if(!ret)
        ret = 1;

    return ret;
}

/**
 * Kopia funkcji appraise_value.
 * Function name: appraise_value
 * Description:   This function is called when someon tries to appraise value
 *                of this object.
 * Arguments:     num - use this number instead of skill if given.
 */
public string
appraise_tree_value(int num)
{
    int value, skill, seed;

    if (!num)
        skill = this_player()->query_skill(SS_APPR_VAL);
    else
        skill = num;

/*
 * To nie mialo wiekszego sensu... /Alvin.
 *    skill = 1000 / (skill + 1);
 */
    skill = ((100 - skill) * 6 / 10) + 1;
    value = query_real_cena();
    sscanf(OB_NUM(this_object()), "%d", seed);
    skill = random(skill, seed);
    value = cut_sig_fig(value + ((skill % 2 ?: -1) * skill * value / 100), 2);

    switch (value)
    {
        case 0..19:
            return value + " grosz" + ilosc(value, "", "e", "y");
            break;
        case 20..239:
             if (value % 20)
                 return value / 20 + " denar" + ilosc(value / 20, "a",
                 "y", "^ow") + " i " + value % 20 + " grosz" + ilosc(value,
                 "", "e", "y");
             else
                 return value / 20 + " denar" + ilosc(value/20, "a", "y", "^ow");
             break;
        case 240..92233720368547758:
             if (value % 240 >= 20)
                 return value / 240 + " koron" + ilosc(value / 240, "^e",
                 "y", "") + " i " + value % 240 / 20 + " denar" +
                 ilosc(value % 240 / 20, "a", "y", "^ow");
             else
                 return value / 240 + " koron" + ilosc(value/240, "^e", "y", "");
             break;
    }
}

/**
 * Funkcja identyfikuj�ca obiekt jako drzewo.
 *
 * @return 1 zawsze
 */
public int is_drzewo() { return 1; }