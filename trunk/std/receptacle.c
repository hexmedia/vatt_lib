/**
 * \file /std/receptacle.c
 *
 * Plik ten zawiera kod obs�uguj�cy otwieranie/zamykanie kontener�w. Ponadto
 * znajduje si� w nim wsparcie dla zamykania/otwierania za pomoc� klucza.
 * Dodatkowo mo�na spr�bowa� wy�ama� zamek, je�eli istnieje.
 *
 * Je�eli chcesz doda� jakie� dodatkowe sprawdzenia przed pr�b� manipulowania
 * kontenerem, lub co� innego, mo�esz dopisa� w�asny kod. Nale�y go umie�ci�
 * w nast�puj�cych funkcjach:
 *
 *   varargs int open(object ob)
 *   varargs int close(object ob)
 *   varargs int lock(object ob)   Tylko wtedy gdy kontener ma zamek
 *   varargs int unlock(object ob) Tylko wtedy gdy kontener ma zamek
 *   varargs int pick(object ob)   Tylko je�eli zamek mo�na wy�ama�
 *
 * Funkcje te s� wywo�ywane poprzez wska�nik do aktualnie rozwa�anego kontenera
 * i s� opcjonalne. �eby okre�li�, w kt�rych plikach funkcje te mog� by�
 * znalezione, musisz u�y�:
 *
 *   void set_cf(object o)   Ustawia obiekt z definicj� open(), close() etc.
 *
 * Funkcje te mog� zwraca� nast�puj�ce warto�ci:
 * <ul>
 *   <li> <b>0</b> je�eli nie ma zmiany w manipulacji kontenerami
 *   <li> <b>1</b> je�eli mo�na manipulowa�, lecz tekst wypisywany jest w funkcji
 *   <li> <b>2</b> je�eli <b>nie</b> mo�na manipulowa�
 *   <li> <b>3</b> je�eli <b>nie</b> mo�na manipulowa� i tekst wypisywany jest
 *                 w funkcji.
 * </ul>
 *
 * Do obs�ugi zamka w kontenerze s� nast�puj�ce funkcje:
 *
 *   void set_key(mixed keyvalue)  Ustawia warto�� klucza
 *   void set_pick(int pick_level) Ustawia stopie� trudno�ci wy�amania zamka
 *   void set_no_pick(1/0)         Ustawia niemo�no�� wy�amania zamka
 *
 * Standardowy poziom trudno�ci wy�amania zamka ustawiony jest na 40. Mo�esz
 * ustawi� poziom trudno�ci wy�amania zamka w zakresie [1..99]. Poziom ten
 * sprawdzany jest z liczb� wyliczon� na podstawie umiej�tno�ci gracza. Warto��
 * spoza przedzia�u spowoduje wy�amanie zamka niemo�liwym do wykonania. Dodatkowo
 * mo�esz ustawi� <i>set_no_pick</i> by zamka na pewno nie mo�na by�o wy�ama�.
 *
 * Dla ka�dej funkcji <i>set_</i> jest odpowiadaj�ca funkcja <i>query_</i>
 *
 * <b>UWAGA</b>
 *
 * Je�eli nie zdefiniujesz w�asnych kr�tkich opis�w dla liczby mnogiej,
 * mog� zosta� wygenerowane bardzo brzydkie napisy.
 *
 * <b>PORADA</b>
 *
 * Nie przedefiniowuj funkcji <i>short</i>, <i>pshort</i> i <i>long</i>
 * w swoim w�asnym kodzie. Lepiej u�ywaj <i>set_short</i>, <i>set_pshort</i>
 * i <i>set_long</i> z VBFC by uzyska� lepsze wyniki.
 */
#pragma save_binary
#pragma strict_types

inherit "/std/container";
inherit "/lib/open_close.c";

#include <cmdparse.h>
#include <files.h>
#include <macros.h>
#include <ss_types.h>
#include <stdproperties.h>

#define MANA_TO_PICK_LOCK      10
#define PICK_SKILL_DIFFERENCE  30
#define HANDLE_KEY_FAIL_STRING " co czym?"

/*
 * Zmienne globalne
 */
static object cont_func;      /* obiekt definiuj�cy open(), close(), etc.        */
static mapping keys = ([]);   /* mapping kluczy otwieraj�cych sublokacje tego kontenera */
static mapping picks = ([]);  /* mapping trudno�ci wy�amania zamk�w sublokacji */

/**
 * Konstruktor obiektu.
 *
 * Funkcj� t� nale�y przedefiniowa�, by m�c ustawi� odpowiednie rzeczy w nowo
 * konstruowanym obiekcie. Jednak�e skoro jest to tylko kontener z kilkoma
 * dodatkami, mo�na w tym celu u�y� tak�e funkcji <i>create_container()</i>
 */
void
create_receptacle()
{
}

/**
 * Konstruktor obiektu.
 *
 * Skoro plik ten nazywa si� <b>/std/receptacle</b>, mo�esz u�y� funkcji
 * <i>create_receptacle</i> dla stworzenia obiektu. Dodatkowo, jak zwykle,
 * mo�esz u�y� <i>create_container</i>, gdy� jest to tylko kontener z kilkoma
 * dodatkami.
 */
void
create_container()
{
    config_open_close();
    create_receptacle();
}

/**
 * Funkcja wywo�ywana przy resecie obiektu.
 *
 * Je�eli chcesz, by ten obiekt resetowa� si� w regularnych odst�pach czasu,
 * musisz przedefiniowa� t� w�a�nie funkcj�. Poniewa� obiekt ten jest tylko
 * kontenerem z kilkoma dodatkami, mo�esz tak�e u�y� funkcji
 * <i>reset_container()</i>. Nie zapomnij wywo�a� <i>enable_reset()</i>
 * w konstruktorze, by obiekt rzeczywi�cie zacz�� si� resetowa�.
 */
void
reset_receptacle()
{
}

/**
 * Funkcja wywo�ywana przy resecie obiektu.
 *
 * Je�eli stworzy�e� kontener za pomoc� <i>create_receptacle</i> i chcesz,
 * by si� resetowa� za pomoc� <i>reset_receptacle</i>, to ta funkcja w�a�nie
 * to umo�liwia. Nie zapomnij wywo�a� <i>enable_reset()</i> w konstruktorze,
 * by obiekt rzeczywi�cie zacz�� si� resetowa�.
 */
void
reset_container()
{
  reset_receptacle();
}

/**
 * Funkcja wywo�ywana przy inicjalizacji.
 *
 * Dowi�zuje ona komendy do manipulowania kontenerem do odpowiednich funkcji.
 */
public void
init()
{
    ::init();
}

/**
 * Zwraca prawdziwy kr�tki opis.
 *
 * Prawdziwy kr�tki opis jest bez dodanych przymiotnik�w 'otwarty'/'zamkni�ty'.
 *
 * @param for_obj dla kogo jest generowany opis.
 * @param przyp przypadek, w kt�rym ma by� opis.
 *
 * @return Kr�tki opis.
 */
public varargs string
real_short(mixed for_obj, int przyp)
{
      return ::short(for_obj, przyp);
}

/**
 * Zwraca kr�tki opis.
 *
 * Zwracany opis jest z dodanymi przymiotnikami 'otwarty'/'zamkni�ty'.
 *
 * @param for_obj dla kogo jest generowany opis.
 * @param przyp przypadek, w kt�rym ma by� opis.
 *
 * @return Kr�tki opis.
 */
public varargs string
short(mixed for_obj, mixed przyp)
{
    if (!objectp(for_obj))
    {
        if (intp(for_obj))
            przyp = for_obj;
        else if (stringp(for_obj))
            przyp = atoi(for_obj);

        for_obj = previous_object();
    }
    else
    {
        if (stringp(przyp))
            przyp = atoi(przyp);
    }

    if (query_prop(CONT_I_NO_OPEN_DESC))
    {
        return ::short(for_obj, przyp);
    }

    return oblicz_przym(!query_open() ? "zamkni�ty" : "otwarty", !query_open() ?
        "zamkni�ci" : "otwarci", przyp, query_rodzaj(), query_tylko_mn()) +
        " " + ::short(for_obj, przyp);
}

/**
 * Zwraca kr�tki opis dla wielu przedmiot�w.
 *
 * Zwracany opis jest z dodanymi przymiotnikami 'otwarty'/'zamkni�ty'
 *
 * @param for_obj dla kogo jest generowany opis.
 * @param przyp przypadek, w kt�rym ma by� opis.
 *
 * @return Kr�tki opis dla wielu przedmiot�w.
 */
public varargs string
plural_short(mixed for_obj, int przyp)
{
    string str;
    int closed;

    if (intp(for_obj))
    {
        przyp = for_obj;
        for_obj = this_player();
    }
    /* nie sprawdzamy czy przyp to int, bo nie ma makr do tej funkcji */

    str = ::plural_short(for_obj, przyp);

    if (query_prop(CONT_I_NO_OPEN_DESC))
    {
        return str;
    }

    if (stringp(str))
    {
        return oblicz_przym(!query_open() ? "zamkni�ty" : "otwarty", !query_open() ?
            "zamkni�ci" : "otwarci", przyp, query_rodzaj(), 1) + " " + str;
    }
    else
        return str;
}

/**
 * Zwraca d�ugi opis.
 *
 * Do d�ugiego opisu jest doklejana informacja, czy kontener jest 'otwarty',
 * czy 'zamkni�ty'.
 *
 * @param for_obj dla kogo jest generowany opis.
 *
 * @return D�ugi opis.
 */
public varargs string
long(object for_obj)
{/*
    if (query_prop(CONT_I_NO_OPEN_DESC))
        return ::long(for_obj);*/

    return ::long(for_obj) + 
    (TO->query_psuj() ? TO->stan_popsucia() : "") + //to obiekt�w z /lib/psuj.c
    ((query_prop(CONT_I_NO_OPEN_DESC)) ? "" :
    (query_tylko_mn() ? "S� " : "Jest ") +
        (!query_open() ? "zamkni�" : "otwar") +
        koncowka("ty", "ta", "te", "ci", "te") + ".\n");
}

private int
owner_pred(object ob, string* owners)
{
    return (member_array(MASTER_OB(ob), owners) != -1);
}

/**
 * Funkcja sprawdza, czy do danej sublokacji przypisany jest zamek.
 *
 * @param sublok sprawdzany sublok
 *
 * @return <ul>
 *           <li> <b>1</b> je�eli do subloka przypisany jest zamek,
 *           <li> <b>0</b> w przeciwnym przypadku.
 *         </ul>
 */
public nomask int
filter_subloc_keys(mixed sublok)
{
    if (!pointerp(sublok))
        return 0;

    if (keys[sublok[0]] == 0)
        return 0;

    return 1;
}

/**
 * Funkcja obs�uguj�ca otwieranie.
 *
 * Pr�buje otworzy� jedn� lub wi�cej kontener�w.
 *
 * @param str tre�� komendy gracza.
 *
 * @return <ul>
 *            <li> <b>1</b> sukces,
 *            <li> <b>0</b> pora�ka.
 *         </ul>
 */
public varargs mixed
open_me(int cicho=0)
{
    if(objectp(TO->query_cf()) && (TO->query_cf())->open(TO))
    {
        return capitalize(TO->real_short(PL_MIA)) + " nie chc" +
            (TO->query_tylko_mn() ? "�" : "e") +
            " si� " + (open_commandb ? open_commandb : "otworzy�") + ".\n";
    }
    return ::open_me(cicho);
}

public varargs mixed
open_subloc(string sublok, int cicho=0)
{
    int cres;

    if (!CAN_SEE_IN_ROOM(TP))
        return ("Jest zbyt ciemno, �eby to zrobi�.\n");

    if (sublok == "")
        return 0;

    string what = (string)TO->real_short(TP, PL_DOP);

    string *sublocs = filter(TO->query_sublocs(), &operator(~=)(sublok, ) @ &(TO)->query_subloc_prop(, CONT_M_OPENCLOSE));

    if (sizeof(sublocs) > 1)
        return ("Zdecyduj si�, co chcesz otworzy�.\n");

    if ((!sizeof(sublocs)) || (!pointerp(sublocs[0])))
        return 0;

    if (!(TO->query_subloc_prop(sublocs[0], CONT_I_CLOSED)))
    {
        return capitalize(sublocs[0][PL_MIA]) + " " + what + " ju� " +
            (TO->query_subloc_prop(sublocs[0], SUBLOC_I_TYLKO_MN) ? "s�" : "jest") + " otwar" +
            TO->subloc_koncowka(sublocs[0], "ty", "ta", "te", "ci", "te") + ".\n";
    }
    else if (TO->query_subloc_prop(sublocs[0], CONT_I_LOCK))
    {
        return capitalize(sublocs[0][PL_MIA]) + " " + what + " " +
            (TO->query_subloc_prop(sublocs[0], SUBLOC_I_TYLKO_MN) ? "s�" : "jest") +
            " zamkni�" + TO->subloc_koncowka(sublocs[0], "ty", "ta", "te", "ci", "te") +
            " na klucz.\n";
    }
    else
    {
        if (!objectp(TO->query_cf()))
            cres = 0;
        else
        {
            cres = (int)((TO->query_cf())->open(TO, sublocs[0]));

            if (cres)
            {
                return capitalize(sublocs[0][PL_MIA]) + " " + what + " nie chc" +
                    (TO->query_subloc_prop(sublocs[0], SUBLOC_I_TYLKO_MN) ? "�" : "e") +
                    " si� otworzy�.\n";
            }
        }

        if (!cres)
        {  
            TO->niszczeje(); //u�ywamy obiektu, niszczy si� wi�c.
            
            this_player()->set_obiekty_zaimkow(({ TO }));
            write("Otwierasz " + sublocs[0][PL_BIE] + " " + what + ".\n");
            saybb(QCIMIE(TP, PL_MIA) + " otwiera " + sublocs[0][PL_BIE] + " " + what + ".\n");
            TO->remove_subloc_prop(sublocs[0], CONT_I_CLOSED);
            if(objectp(ENV(TP)))
            {
                object *owners = filter(all_inventory(ENV(TP)), &owner_pred(, TO->query_owners()));
                owners->signal_rusza_moje_rzeczy(this_player(), TO);
            }
        }
    }
}

/**
 * Funkcja obs�uguj�ca zamykanie.
 *
 * @return <ul>
 *            <li> <b>1</b> sukces,
 *            <li> <b>0</b> pora�ka.
 *         </ul>
 */
public varargs mixed
close_me(int cicho=0)
{
    if(objectp(TO->query_cf()) && (TO->query_cf())->close(TO))
    {
        return capitalize(TO->real_short(PL_MIA) + " nie chc" +
            TO->query_tylko_mn() ? "�" : "e") + "si� " +
            (close_commandb ? close_commandb : "zamkn��") + ".\n";
    }

    return ::close_me();
}

public varargs mixed
close_subloc(string sublok, int cicho=0)
{
    int cres;
    if (!CAN_SEE_IN_ROOM(TP))
        return ("Jest zbyt ciemno, �eby to zrobi�.\n");

    string what = (string)TO->real_short(TP, PL_DOP);

    string *sublocs = filter(TO->query_sublocs(), &operator(~=)(sublok, ) @ &(TO)->query_subloc_prop(, CONT_M_OPENCLOSE));

    if (sizeof(sublocs) > 1)
        return ("Zdecyduj si�, co chcesz zamkn��.\n");

    if ((!sizeof(sublocs)) || (!pointerp(sublocs[0])))
        return 0;

    if (TO->query_subloc_prop(sublocs[0], CONT_I_CLOSED))
    {
        return capitalize(sublocs[0][PL_MIA]) + " " + what + " ju� " +
            (TO->query_subloc_prop(sublocs[0], SUBLOC_I_TYLKO_MN) ? "s�" : "jest") + " zamkni�" +
            TO->subloc_koncowka(sublocs[0], "ty", "ta", "te", "ci", "te") + ".\n";
    }
    else if (TO->query_subloc_prop(sublocs[0], CONT_I_LOCK))
    {
        return capitalize(sublocs[0][PL_MIA]) + " " + what + " nie chc" +
            (TO->query_subloc_prop(sublocs[0], SUBLOC_I_TYLKO_MN) ? "�" : "e") +
            " si� zamkn��.\n";
    }
    else
    {
        if (!objectp(TO->query_cf()))
            cres = 0;
        else
        {
            cres = (int)((TO->query_cf())->close(TO, sublocs[0]));

            if (cres)
            {
                return capitalize(sublocs[0][PL_MIA]) + " " + what + " nie chc" +
                    (TO->query_subloc_prop(sublocs[0], SUBLOC_I_TYLKO_MN) ? "�" : "e") +
                    " si� zamkn��.\n";
            }
        }

        if (!cres)
        {
            //to dla obiekt�w z /lib/psuj.c
            if(TO->query_psuj())
            {
                TO->niszczeje(); //u�ywamy obiektu, niszczy si� wi�c.
    
                if(TO->zniszczona())
                {
                    NF(TO->koncowka("Ten", "Ta", "To")+" "+TO->query_nazwa(PL_MIA)+" "+
                    "jest ju� kompletnie zniszczon"+TO->koncowka("y","a","e")+
                        " i nie nadaje si� do niczego.\n");
                    return 0;
                }
            }
            
            this_player()->set_obiekty_zaimkow(({ TO }));
            write("Zamykasz " + sublocs[0][PL_BIE] + " " + what + ".\n");
            saybb(QCIMIE(TP, PL_MIA) + " zamyka " + sublocs[0][PL_BIE] + " " + what + ".\n");
            TO->add_subloc_prop(sublocs[0], CONT_I_CLOSED, 1);
            if (objectp(ENV(TO)))
            {
                object *owners = filter(all_inventory(ENV(TO)), &owner_pred(, TO->query_owners()));
                owners->signal_rusza_moje_rzeczy(this_player(), TO);
            }
        }
    }
}

/**
 * Funkcja obs�uguj�ca zamykanie zamka.
 *
 * @param str tre�� komendy gracza.
 *
 * @return <ul>
 *            <li> <b>1</b> sukces,
 *            <li> <b>0</b> pora�ka.
 *         </ul>
 */
public varargs mixed
lock_me(object key, int cicho=0)
{
    if ((objectp(TO->query_cf())) &&
        ((TO->query_cf())->lock(TO)))
    {
        return ("Nie mo�esz " + (lock_commandb ? lock_commandb : "zamkn��") + " " + TO->real_short(TP, PL_DOP)
            + ".\n");
    }
    return ::lock_me(key, cicho);
}

public varargs mixed
lock_subloc(string sublok, object key, int cicho=0)
{
    if(TO->query_subloc_item(sublok, CONT_I_LOCK))
    {
        mixed sublocs = filter(TO->query_sublocs(), &operator(~=)(sublok, ) @ &(TO)->query_subloc_prop(, CONT_M_OPENCLOSE));
        sublocs = filter(sublocs, &filter_subloc_keys());
        return (capitalize(sublocs[0][PL_MIA]) + " " + TO->real_short(TP, PL_DOP) +
            " ju� " + (TO->query_subloc_prop(sublok, SUBLOC_I_TYLKO_MN) ? "s�" : "jest") +
            " zamkni�" + TO->subloc_koncowka(sublok, "ty", "ta", "te", "ci", "te") + ".\n");
    }

    // TODO: !!!

    return 1;
}

/**
 * Funkcja obs�uguj�ca otwieranie zamka.
 *
 * Pr�buje otworzy� zamek u jednego lub wi�kszej ilo�ci kontener�w.
 *
 * @param str tre�� komendy gracza.
 *
 * @return <ul>
 *            <li> <b>1</b> sukces,
 *            <li> <b>0</b> pora�ka.
 *         </ul>
 */
public varargs mixed
unlock_me(object key, int cicho = 0)
{
    if ((objectp(TO->query_cf())) && ((TO->query_cf())->lock(TO)))
    {
        return ("Nie mo�esz " + (unlock_commandb  ? unlock_commandb : "otworzy�") + " " + TO->real_short(TP, PL_DOP)
            + ".\n");
    }

    return ::unlock_me(key, cicho);
}

public varargs mixed
unlock_subloc(string sublok)
{
    if(!TO->query_subloc_item(sublok, CONT_I_LOCK))
    {
        mixed sublocs = filter(TO->query_sublocs(), &operator(~=)(sublok, ) @ &(TO)->query_subloc_prop(, CONT_M_OPENCLOSE));
        sublocs = filter(sublocs, &filter_subloc_keys());
        return (capitalize(sublocs[0][PL_MIA]) + " " + TO->real_short(TP, PL_DOP) +
            " nie " + (TO->query_subloc_prop(sublok, SUBLOC_I_TYLKO_MN) ? "s�" : "jest") +
            " zamkni�" + TO->subloc_koncowka(sublok, "ty", "ta", "te", "ci", "te") + ".\n");
    }
    //TODO!!
    return 0;
}

/**
 * Funkcja obs�uguj�ca wy�amywanie zamka.
 *
 * @return <ul>
 *            <li> <b>1</b> sukces,
 *            <li> <b>0</b> pora�ka.
 *         </ul>
 */
public varargs mixed
pick_me(object wytrych, int cicho=0)
{
    if ((objectp(TO->query_cf())) && ((TO->query_cf())->pick(TO)))
    {
        return ("Nie mo�esz " + (pick_commandb ? pick_commandb : "otworzy�") + " " + TO->real_short(TP, PL_DOP)
          + ".\n");
    }
    return ::pick_me(wytrych, cicho);
}

public varargs mixed
pick_subloc(string sublok, object wytrych, int cicho=0)
{
    if(!TO->query_subloc_item(sublok, CONT_I_LOCK))
    {
        mixed sublocs = filter(TO->query_sublocs(), &operator(~=)(sublok, ) @ &(TO)->query_subloc_prop(, CONT_M_OPENCLOSE));
        sublocs = filter(sublocs, &filter_subloc_keys());
        return (capitalize(sublocs[0][PL_MIA]) + " " + TO->real_short(TP, PL_DOP) +
            " nie " + (TO->query_subloc_prop(sublok, SUBLOC_I_TYLKO_MN) ? "s�" : "jest") +
            " zamkni�" + TO->subloc_koncowka(sublok, "ty", "ta", "te", "ci", "te") + ".\n");
    }
    //TODO!
}

/**
 * Funkcja obs�uguj�ca wy�amywanie zamka.
 *
 * @return <ul>
 *            <li> <b>1</b> sukces,
 *            <li> <b>0</b> pora�ka.
 *         </ul>
 */
public varargs mixed
breakdown_me(object wytrych, int cicho=0)
{
    if ((objectp(TO->query_cf())) && ((TO->query_cf())->breakdown(TO)))
    {
        return ("Nie mo�esz " + (breakdown_commandb ? breakdown_commandb : "otworzy�") + " " + TO->real_short(TP, PL_DOP)
          + ".\n");
    }
    return ::breakdown_me(wytrych, cicho);
}

public varargs mixed
breakdown_subloc(string sublok, object wytrych, int cicho=0)
{
    if(!TO->query_subloc_item(sublok, CONT_I_CLOSED))
    {
        mixed sublocs = filter(TO->query_sublocs(), &operator(~=)(sublok, ) @ &(TO)->query_subloc_prop(, CONT_M_OPENCLOSE));
        sublocs = filter(sublocs, &filter_subloc_keys());
        return (capitalize(sublocs[0][PL_MIA]) + " " + TO->real_short(TP, PL_DOP) +
            " nie " + (TO->query_subloc_prop(sublok, SUBLOC_I_TYLKO_MN) ? "s�" : "jest") +
            " zamkni�" + TO->subloc_koncowka(sublok, "ty", "ta", "te", "ci", "te") + ".\n");
    }
    //TODO!
}

/**
 * Ustawia obiekt odpowiedzialny za dodatkowy kod przy otwieraniu/zamykaniu.
 *
 * Pozwala ustawi� obiekt, kt�ry przechowuje kod wywo�ywany przed pr�b�
 * otwarcia konkretnego obiektu. Kod ten b�dzie wywo�any w funkcjach
 * <i>open()</i> i <i>close()</i>. Funkcje te powinny zwraca� <b>1</b>, je�eli
 * nie jest mo�liwe otwarcie/zamkni�cie kontenera lub <b>0</b>, gdy nie ma
 * �adnych przeciwwskaza� ku temu.
 *
 * @param obj obiekt przechowuj�cy dodatkowy kod.
 */
public void
set_cf(object obj)
{
    if (query_lock())
    {
        return;
    }

    cont_func = obj;
}

/**
 * Zwraca obiekt przechowuj�cy dodatkowy kod.
 *
 * @return obiekt przechowuj�cy dodatkowy kod.
 */
public object
query_cf()
{
    return cont_func;
}

/**
 * Funkcja ta zwraca opis przedmiotu dla czarodzieja.
 *
 * @return opis przedmiotu dla czarodzieja.
 */
public string
stat_object()
{
    string str = ::stat_object();

    if (TO->query_key())
    {
        str += "Kod klucza: \"" + TO->query_key() + "\"\n";

        if (TO->query_pick() == 0)
            str += "Zamek nie do otworzenia wytrychem.\n";
        else
            str += "Poziom trudno�ci otworzenia wytrychem: " + TO->query_pick() + "\n";

        if(TO->query_breakdown() == 0)
            str += "Zamek nie do wy�amania.\n";
        else
            str += "Poziom trudno�ci wy�amania: " + TO->query_breakdown() + "\n";
    }
    else
        str += "Nie ma zamka.\n";

    return str;
}

/**
 * Zwraca opis trudno�ci otworzenia zamka.
 *
 * Opis ten jest budowany na podstawie r�nicy mi�dzy poziomem umiej�tno�ci
 * gracza a poziomem trudno�ci wy�amania zamka.
 *
 * @param pick_val wspomniana r�nica
 *
 * @return opis trudno�ci otworzenia zamka.
 */
public nomask string
get_pick_chance(int pick_val)
{
  if (pick_val >= 40)
    return "zamek da si� otworzy� przez samo spojrzenie na niego"; //??! WTF, magia? :) [vera]
  if (pick_val >= 30)
    return "zamek jest bardzo �atwo otworzy�";
  if (pick_val >= 20)
    return "zamek jest ca�kiem �atwo otworzy�";
  if (pick_val >= 10)
    return "zamek jest �atwo otworzy�";
  if (pick_val >= 0)
    return "zamek da si� otworzy�";
  if (pick_val >= -10)
    return "zamek da si� otworzy� z pewnymi trudno�ciami";
  if (pick_val >= -20)
    return "otwarcie zamka mo�e nastr�czy� troch� trudno�ci";
  if (pick_val >= -30)
    return "zamek jest bardzo ci�ko otworzy�";
  if (pick_val >= -40)
    return "zamka prawie na pewno nie da si� otworzy�";

  /* pick_val < -40 */
  return "zamka nie da si� w �aden spos�b otworzy�";
}

/**
 * Funkcja oceniaj�ca dany obiekt.
 *
 * @param num liczba u�ywana zamiast poziomu umiej�tno�ci oceniania gracza
 */
public varargs void
appraise_object(int num)
{
    int pick_level = query_pick();
    int seed;
    int skill;

    ::appraise_object(num);

    if (!query_key())
    {
        return;
    }

    if (!num)
    {
        skill = (int)this_player()->query_skill(SS_APPR_OBJ);
    }
    else
    {
        skill = num;
    }

    sscanf(OB_NUM(this_object()), "%d", seed);
    skill = random((1000 / (skill + 1)), seed);
    pick_level = (int)this_player()->query_skill(SS_OPEN_LOCK) -
        cut_sig_fig(pick_level + (skill % 2 ? -skill % 70 : skill) *
        pick_level / 100, 2);

    write ("Masz wra�enie, �e " + get_pick_chance(pick_level) + ".\n");
    //TODO: doda� breakdowna
}

/**
 * Funkcja zwracaj�ca napis dla odtworzenia.
 *
 * @return napis dla odtworzenia.
 */
public nomask string
query_receptacle_auto_load()
{
    return ("#RECEPTALCE#" + query_open() + "#" + query_locked() + "#RECEPTALCE#");
}

/**
 * Funkcja ustawiaj�ca parametry obiektu na podstawie napisu.
 *
 * @param arg napis odtwarzania.
 */
public nomask string
init_receptacle_arg(string arg)
{
    string foobar, rest;
    if (arg == 0)
        return 0;

    do_open();
    do_unlock();

    int open, locked;
    sscanf(arg, "%s#RECEPTALCE#%d#%d#RECEPTALCE#%s", foobar, open, locked, rest);

    if(locked)
        do_lock();
    else if(!open)
        do_close();
    else
        do_open();

    return foobar + rest;
}

/**
 * Funkcja zwracaj�ca napis dla auto_�adowania.
 *
 * @return Napis dla auto_�adowania
 */
public string
query_auto_load()
{
    return ::query_auto_load() + query_receptacle_auto_load();
}

/**
 * Funkcja inicjalizuj�ca obiekt przy auto_�adowaniu.
 *
 * @param arg napis odtwarzania.
 */
public string
init_arg(string arg)
{
    return init_receptacle_arg(::init_arg(arg));
}

//Heh, na razie to musi tak dzia�a�, �eby by�o i tak jak teraz i tak jak dawniej
public void
do_open()
{
    ::do_open();
    remove_prop(CONT_I_CLOSED);
    remove_prop(CONT_I_LOCK);
}

public void
do_close()
{
    ::do_close();
    add_prop(CONT_I_CLOSED, 1);
    remove_prop(CONT_I_LOCK);
}

public void
do_unlock()
{
    ::do_unlock();
    add_prop(CONT_I_CLOSED, 1);
    remove_prop(CONT_I_LOCK);
}

public void
do_lock()
{
    ::do_lock();
    add_prop(CONT_I_CLOSED, 1);
    add_prop(CONT_I_LOCK, 1);
}

public void
do_pick()
{
    ::do_pick();
    remove_prop(CONT_I_LOCK);
    add_prop(CONT_I_CLOSED, 1);
}

public void
do_breakdown()
{
    ::do_breakdown();
    remove_prop(CONT_I_CLOSED);
    remove_prop(CONT_I_LOCK);
}

/**
 * Funkcja identyfikuj�ca obiekt jako zamykany pojemnik
 *
 * @return 1 zawsze
 */
public int is_receptacle() { return 1; }