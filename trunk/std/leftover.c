/*
 * Drobne zmiany: Skoro czas rzeczywisty - czemu rozk�adamy si� tak szybko? ^_^
 *                    Lil. 23.08.2005
 * - A bo tak ty g�upia fl�dro - rzek� Vera, po czym przyspieszy� nieco czas
 * rozk�adania si� leftover�w (cia�a jednak rozk�adaj� si� znacznie wolniej, ni�
 * pojedyncze szcz�tki). 17.12.07
 * 
 * leftover.c 
 *
 * Podstawowy kod leftover'ow
 *
 * Obiekt dziala na bardzo podobnej zasadzie, jak /std/food.c. Podstawowa
 * roznica, jest set_decay_time(), determinujacy jak dlugo obiekt
 * moze zostac w pomieszczeniu, zanim sie nie rozlozy - 
 * jest to czas znacznie kr�tszy (bo tylko 60min).
 * Ponadto leftovery nie s� auto�adowane i tak ma pozosta�!
 */

#pragma save_binary
#pragma strict_types

inherit "/std/food";

#include <cmdparse.h>
#include <composite.h>
#include <files.h>
#include <language.h>
#include <macros.h>
#include <stdproperties.h>

int decay_time;		/* Czas jaki uplynie zanim sie nie rozlozy */
int simple_names;	/* Setup of simple names or not */
static private int decay_alarm;
string l_rasa;		/* Rasa poprzedniego wlasciciela organu */
int l_hard;		/* organ twardy czy mietki ;) */
string* _rasa;
int _rodzaj_rasy;
string _organ;
int corpse_waga;
public void decay_fun();


/*
 * Pomocnicza funkcja losuj�ca ile wa�y� (tym samym: ile syci�) ma dany leftover
 * Vera.
 */
int
wylosuj_amount()
{
	switch(_organ)
	{
		case "mi�so":
			return (corpse_waga / 25 + random(5)) + 2;
		case "z�b":
			return corpse_waga>2000?random(20) + 7:random(3) + 2;
		case "czaszka":
			return corpse_waga>2000?random(950) + 7:random(110) + 2;
		case "ko��":
			return corpse_waga>2000?random(750) + 7:random(80) + 2;
		case "serce":
			return corpse_waga>1000?random(20) + 4:random(7) + 2;
		case "oko":
			return random(5) + 2;
		default:
			return corpse_waga>1000?random(10) + 4:random(5) + 2;
	}

}


public void
create_leftover()
{
    simple_names = 1;
    dodaj_nazwy("szcz�tek");
}

public void
leftover_init(string organ, string *rasa,
	      int rodzaj_rasy = PL_MESKI_NOS_ZYW, int hard = 0, int waga = 1, int amount = -1)
{
	corpse_waga = waga;

    l_rasa = rasa[PL_MIA];
    l_hard = hard;
    _rasa = rasa;
    _rodzaj_rasy = rodzaj_rasy;
    _organ = organ;

    if (amount > 0) {
        set_amount(amount);
    }
    else {
        set_amount(wylosuj_amount());
    }

    if (simple_names)
    {
	ustaw_nazwe(organ);
	set_long("Krwaw" + koncowka("y", "a", "e") + " " + organ + 
	   ", najprawdopodobniej jakie" + (rodzaj_rasy == PL_ZENSKI ? "j^s"
	   : "go^s") + " " + rasa[PL_DOP] + ".\n");
    }
}

public nomask void
create_food() 
{ 
    simple_names = 0;
    decay_time = 60; /*86400*/;
    create_leftover();
}

public void
set_decay_time(int time)
{
    decay_time = time;
}

public int
query_decay_time()
{
    return decay_time;
}

public string
query_rasa()
{
    return l_rasa;
}

public int
query_hard()
{
   return l_hard;
}

public void 
enter_env(object dest, object old) 
{
    ::enter_env(dest, old);
if(!get_alarm(decay_alarm))
    decay_alarm = set_alarm(1.0, 0.0, decay_fun);
    //remove_alarm(decay_alarm);
    //if (function_exists("create_container", dest) == ROOM_OBJECT)
//	decay_alarm = set_alarm(1.0, 0.0, decay_fun);
}

public void
decay_fun()
{
    int num;
    
    if (--decay_time)
	decay_alarm = set_alarm(60.0, 0.0, decay_fun);
    else
    {
        if (query_rodzaj() == PL_NIJAKI_OS)
            num = 0;
        else
        {
            num = num_heap();
//            if ((num < 10 || num > 20) && (num = num % 10) < 5 && num > 1)
//                num = 1;
//            else num = 0;
	    num = ilosc(num, 0, 1, 0);
        }
        
	tell_roombb(environment(this_object()),
	            QCSHORT(this_object(), PL_MIA) + " obraca"
	          + (num ? "j^a" : "") + " si^e w proch.\n", ({}),
	            this_object());
	remove_object();
    }
}

/*
 * Function name:	consume_them 
 * Description:		Consumes a number of food item. This function
 *			shadows the ordinary /std/food function.
 *			All this to detect cannibals...
 * Arguments:		arr: Array of food objects.
 */
public void
consume_them(object *arr)
{
    int il;
    
    for (il = 0; il < sizeof(arr); il++)
    {
        if (arr[il]->query_rasa() == this_player()->query_rasa())
	{
	    write("Czyni^ac to, czujesz si^e troch^e nieswojo, ale...\n");
	    this_player()->add_prop("cannibal", 1);
	}
	arr[il]->delay_destruct();
    }
}

int
eat_one_thing(object ob)
{
    if (ob->query_hard())
    {
//        write("Chyba nie chcesz zjesc " + ob->short(PL_DOP) + "?\n");
        return 0;
    }
    return ::eat_one_thing(ob);
}

void
config_split(int new_num, mixed orig)
{
    ::config_split(new_num, orig);
    l_hard = orig->query_hard();
    l_rasa = orig->query_rasa();
}

int
query_leftover()
{
    return 1;
}

/*
zakomentowa� verek. poniewa� leftovery maj� si� nie auto�adowa� i ju�.

public string
query_leftover_auto_load()
{
    if (pointerp(_rasa))
        return " #leftover#" + _rodzaj_rasy + "," + _organ + "," + l_hard + "," + query_amount() + "," + _rasa[0] + "," +
            _rasa[1] + "," + _rasa[2] + "," + _rasa[3] + "," + _rasa[4] + "," + _rasa[5] + "#leftover# ";
    return "";
}

public string
init_leftover_arg(string arg)
{
    string foobar, toReturn;
    int tmp1, tmp2, tmp3;
    string a1, a2, a3, a4, a5, a6, organ;

    if (arg == 0) {
        return 0;
    }

    if (sscanf(arg, "%s#leftover#%d,%s,%d,%d,%s,%s,%s,%s,%s,%s#leftover#%s",
                foobar, tmp1, organ, tmp2, tmp3, a1, a2, a3, a4, a5, a6, toReturn) == 12) {
        leftover_init(organ, ({ a1, a2, a3, a4, a5, a6 }), tmp1, tmp2, 1, tmp3);
        return toReturn;
    }
    return arg;
}

public string
query_auto_load()
{
    return ::query_auto_load() + query_leftover_auto_load();
}

public string
init_arg(string arg)
{
    return init_leftover_arg(::init_arg(arg));
}*/
