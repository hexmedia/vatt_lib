/*
 *    Nieco teorii - bo sam sie potem nie polapie :P
 *
 *    Kazde drzewo posiada 'kondycje' ktora wyliczana jest na podstawie
 *    jego masy.
 *    Wzor na czas scinania drzewa ma postac: t=ax^b*c^y, gdzie x to masa drzewa
 *    y - potencjal gracza . Drzewo uznajemy za sciete kiedy jego kondycja jest
 *    rowna, badz mniejsza od zera. Kondycja zmniejsza sie z kazdym beat'em o
 *    okreslona wartosc, zatem warunek na sciecie wyglada tak:
 *               C - N*d = 0           ||   C - kondycja, N - ilosc beat'ow, d - 'obrazenia' na 1 beat'a
 *
 *    A czas scinania wyraza sie wzorem t=3N (poniewaz beat jest co 3 sekundy).
 *
 *    Przedstawiamy wiec wzor na czas (t=ax^b*c^y) tak aby pasowal do powyzszej
 *    postaci i dostajemy:
 *
 *            C = ax^b/3             ||  Chodzi o to, ze po podstawieniu tego do t=3N dostajemy
 *            d = 1/c^y                ||  t=ax^b*c^y ;)
 *            N = C/d
 *
 *    I generalnie to tyle, analogiczny mechanizm zastosowany przy obrabianiu drzewa.
 *
 */

inherit "/std/paralyze";

#include <exp.h>
#include <macros.h>
#include <math.h>
#include <filter_funs.h>
#include <pl.h>
#include <stdproperties.h>
#include <ss_types.h>
#include <composite.h>

/**********************
 *          ZMIENNE           *
 **********************/
object 	drzewo;
object 	tool;
object 	player;

float 	k_dmg;
int		f_cost;

void sciete(object player);
int przestal();
void beat();

void create_paralyze()
{
	set_name("scinka_paraliz_zetnij");
	set_stop_verb("przesta�");
	set_stop_message("Przestajesz �cina� drzewo.\n");
	set_stop_object(this_object());
	set_stop_fun("przestal");
	set_fail_message("Teraz �cinasz drzewo, je�li chcesz zrobi�"
				+" co� innego po prostu 'przesta�'.\n");
	set_finish_object(this_object());
	set_finish_fun("sciete");
	set_event_time(9);

	add_allowed("'");
	add_allowed("powiedz");

	setuid();
        seteuid(getuid());
}

void init_events()
{
	add_event("Bierzesz lekki zamach i z impetem uderzasz "
			+tool->query_nazwa(4)+" w drzewo.@@event1:"+file_name(this_object())+"@@",
			QCIMIE(player, PL_MIA)+" bierze lekki zamach i"
			+" z impetem uderza "+tool->query_nazwa(4)+" w drzewo.");
	add_event("Przez kr�tk� chwil� masz wra�enie, �e drzewo przechyla si� w jedn� stron�.");
	add_event("Z wn�trza drzewa dochodzi przeci�g�y trzask.@@event2:"+file_name(this_object())+"@@",
			"Z drzewa �cinanego przez "+QIMIE(player, PL_BIE)+" dochodzi"
			+" przeci�g�y trzask.");
	add_event("Uderzasz "+tool->query_nazwa(4)+" w drzewo, a z jego korony"
			+" sypi� si� ma�e, pousychane ga��zki.@@event3:"+file_name(this_object())+"@@",
			QCIMIE(player, PL_MIA)+" uderza "+tool->query_nazwa(4)
			+" w drzewo, a z jego korony sypi� si� ma�e, pousychane ga��zki.");
	add_event("Dochodzi ci� wyra�ny zapach �wie�ego drewna.",
			"Podmuch wiatru przyni�s� ze sob� wyra�ny zapach �wie�ego drewna.");
	add_event("Miarowo uderzasz "+tool->query_nazwa(4)+" w drzewo.@@event4:"+file_name(this_object())+"@@",
			QCIMIE(player, PL_MIA)+" miarowo uderza "
			+tool->query_nazwa(4)+" w drzewo.");
	add_event("Bierzesz szeroki zamach i krzepkim uderzeniem od�upujesz od drzewa kawa�ek"
			+" kory.", QCIMIE(player, PL_MIA)
			+" bierze szeroki zamach i krzepkim uderzeniem od�upuje od "
			+drzewo->short(PL_DOP)+" kawa�ek kory.");
	add_event("Cofasz si� o krok by po chwili kopn�� z impetem w "+drzewo->short(PL_BIE)
			+" i oceni� jak d�ugo przyjdzie ci "+drzewo->koncowka("go", "j�", "je")
            +" jeszcze �cina�." ,
            QCIMIE(player, PL_MIA)+" cofa si� o krok od "+drzewo->short(PL_DOP)
            +" by po chwili, za pomoc� krzepkiego"+" kopni�cia, oceni� jak d�ugo przyjdzie "
            +player->koncowka("mu", "jej")+" jeszcze "+drzewo->koncowka("go", "j�", "je")
            +" �cina�.");
	add_event("Nag�y podmuch wiatru rozwia� po okolicy nieco trocin.",
			"Nag�y podmuch wiatru rozwia� po okolicy nieco trocin.");
}

string
event1()
{
    if (drzewo->query_na_drzewie()) {
        object* all = FILTER_LIVE(all_inventory(drzewo->query_na_drzewie()));
        tell_room(drzewo->query_na_drzewie(), "Drzewo zatrz�s�o si� od uderzenia siekier�.\n");
    }
    return "";
}

string
event2()
{
    if (drzewo->query_na_drzewie()) {
        object* all = FILTER_LIVE(all_inventory(drzewo->query_na_drzewie()));
        tell_room(drzewo->query_na_drzewie(), "Z wn�trza drzewa dochodzi przeci�g�y trzask.\n");
    }
    return "";
}

string
event3()
{
    if (drzewo->query_na_drzewie()) {
        object* all = FILTER_LIVE(all_inventory(drzewo->query_na_drzewie()));
        tell_room(drzewo->query_na_drzewie(), "Masz wra�enie, �e drzewo lekko przechyla si� w jedn� stron�.\n");
    }
    return "";
}

string
event4()
{
    if (drzewo->query_na_drzewie()) {
        object* all = FILTER_LIVE(all_inventory(drzewo->query_na_drzewie()));
        tell_room(drzewo->query_na_drzewie(), "Drzewo przechodz� wibracje od rytmicznych uderze� siekier�.\n");
    }
    return "";
}

void start()
{
	set_alarm(3.0, 0.0, &beat());
}

void beat()
{
	/* Sprawdzamy czy gracz ma na tyle sily, zeby walnac w drzewo */
	if(player->query_old_fatigue()-f_cost < 0)
	{
		player->catch_msg("Ze zm�czenia przerywasz �cinanie "+drzewo->query_short(1)+".\n");
		przestal();
		remove_object();
	}

	/* Jesli tak to zmniejszamy kondycje drzewa i meczymy gracza */
	player->add_old_fatigue(-f_cost);
	drzewo->mod_k_zetnij(-k_dmg);

	/* Niszczymy troszke narzedzie */
	/*tool->set_weapon_hits(tool->query_weapon_hits()+1);
	tool->did_hit();
	if(!tool->query_wielded())
	{
		player->catch_msg("Przestajesz �cina� drzewo.\n");
		przestal();
		remove_object();
	}*/

	/* I sprawdzamy czy drzewo nie zostalo juz przypadkiem sciete */
	if((intp(drzewo->query_k_zetnij()) ? itof(drzewo->query_k_zetnij()) : drzewo->query_k_zetnij()) <= 0.0)
		sciete(player);
	else
		set_alarm(3.0, 0.0, &beat());
}

public int
policz_rzeczy_nazwa(string nazwa, object* obarr)
{
    int i, il, ile;

    ile = 0;
    il = sizeof(obarr);
    for (i = 0; i < il; ++i) {
        if (nazwa ~= obarr[i]->short()) {
            ++ile;
        }
    }
    return ile;
}

void sciete(object player)
{
    object *all;

    drzewo->set_scinajacy(0);

    /* Opisujemy */
    player->catch_msg("Zauwa�asz jak "+drzewo->short(PL_MIA)+" zaczyna ko�ysa�"
		+" si� na boki. Zbieraj�c w sobie wszystkie si�y zaciskasz d�onie na"
		+" r�koje�ci "+tool->query_nazwa(PL_DOP)+" i szybkim, pewnym"
		+" ruchem trafiasz w sam �rodek wy��obionej dziury. Z wnetrza drzewa"
		+" daje si� s�yszec g�o�ny trzask, a ty z zadowoleniem przygl�dasz si� jak"
		+" owoc twej pracy przechyla si� na jedn� stron� i z hukiem uderza o ziemi�,"
		+" wyrzucaj�c wszystkie trociny w powietrze.\n");

    saybb("�cinane przez "+QIMIE(player, PL_BIE)+" drzewo zaczyna ko�ysa� si� na boki."
	+" Nie czekaj�c ani sekundy "+QIMIE(player, PL_MIA)+" szybkim i pewnym ruchem uderza"
	+" w sam �rodek wy��obionej w pniu dziury. Po chwili drzewo leniwie"
	+" zwala si� na ziemi� wyrzucaj�c w g�r� nieco trocin.\n");

    if(drzewo->query_na_drzewie() && sizeof(all = all_inventory(drzewo->query_na_drzewie())))
    {
	object* liv = FILTER_LIVE(all);
        tell_room(ENV(drzewo), "Z przewracaj�cego si� drzewa co� spad�o.\n", 0);

        foreach (object pl : liv)
	{
            pl->catch_msg("Drzewo przewraca si� z �oskotem na ziemi�, a ty razem z nim!\n");
            pl->reduce_hp(0, 5000 + random(15000));
            pl->check_killed(TO);
            pl->add_old_fatigue(-(50 + random(150)));
        }

        all->move(ENV(drzewo), 1);
    }

    /* Dodajemy expa */
    int exp_str = ftoi(LINEAR_FUNC(0.0, itof(EXP_SCINANIE_DRZEW_STR_MIN),
				   1500000.0, itof(EXP_SCINANIE_DRZEW_STR_MAX),
		        	   itof(drzewo->query_prop(OBJ_I_WEIGHT))));

    int exp_skill =  ftoi(LINEAR_FUNC(0.0, itof(EXP_SCINANIE_DRZEW_WOODCUTTING_MIN),
				     1500000.0, itof(EXP_SCINANIE_DRZEW_WOODCUTTING_MAX),
				     itof(drzewo->query_prop(OBJ_I_WEIGHT))));

    player->increase_ss(SS_STR, exp_str);
    player->increase_ss(SS_WOODCUTTING, exp_skill);

    /* Dodajemy przymiotnik */
    string *przym = drzewo->query_przym(1, 0,1);
    string *pprzym = drzewo->query_pprzym(1,0,1);
    drzewo->remove_adj(przym);
    drzewo->dodaj_przym("�ci�ty", "�ci�ci");
    int psize = sizeof(przym);
    for(int i=0; i < psize; i++)
	drzewo->dodaj_przym(przym[i], pprzym[i]);
    drzewo->odmien_short();
    drzewo->odmien_plural_short();

    drzewo->remove_prop(OBJ_M_NO_GET);
    drzewo->remove_prop(OBJ_I_DONT_GLANCE);
    drzewo->remove_prop(OBJ_I_DONT_SHOW_IN_LONG);

    /* Zeby sciete drzewo bylo pierwsze w kolejce */
    object env = environment(drzewo);
    drzewo->move("/d/Standard/Redania/Rinde_okolice/Las/lokacje/tartak_magazyn");
    drzewo->move(env);

    remove_object();
}

int przestal()
{
	drzewo->set_scinajacy(0);
	saybb(QCIMIE(TP, PL_MIA)+" przestaje �cina� "+drzewo->short(PL_BIE)+".\n");
	return 0;
}

void set_tool(object obj)
{
	tool = obj;
}

void set_drzewo(object ob)
{
	drzewo = ob;
}

void set_player(object ob)
{
	player = ob;
}

void set_k_dmg(float f)
{
	k_dmg = f;
}

void set_f_cost(int i)
{
	f_cost = i;
}
