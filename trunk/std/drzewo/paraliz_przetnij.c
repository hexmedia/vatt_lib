inherit "/std/paralyze";

#include <exp.h>
#include <macros.h>
#include <stdproperties.h>
#include <ss_types.h>

object kloda;

void przecieta();

void create_paralyze()
{
	set_name("scinka_paraliz_przetnij");
	set_stop_verb("przesta�");
	set_stop_message("Przestajesz przecina� k�od�.\n");
	set_fail_message("Teraz przecinasz k�od�, aby zrobi� co� innego wystarczy przesta�.\n");
	set_finish_object(this_object());
	set_finish_fun("przecieta");
	set_remove_time(random(12)+10);
	
	add_allowed("'");
	add_allowed("powiedz");

	setuid();
	seteuid(getuid());
}

void przecieta()
{
	object *klody = allocate(2);
	int masa= kloda->query_prop(OBJ_I_WEIGHT);
	int random_waga = random(masa/10);

	klody[0] = clone_object(MASTER_OB(kloda));
	klody[1] = clone_object(MASTER_OB(kloda));
	klody[0]->add_prop(OBJ_I_WEIGHT, (masa/2)-random_waga);
	klody[1]->add_prop(OBJ_I_WEIGHT, (masa/2)+random_waga);
	klody->ustaw_cene(kloda->query_cena());
	klody->set_kloda_owner(kloda->query_kloda_owner());
	klody->remove_owner_alarm();
	klody->move(environment(kloda));
	
	TP->increase_ss(SS_STR, EXP_SCINANIE_DRZEW_PRZECINANIE_STR);
	TP->increase_ss(SS_WOODCUTTING, EXP_SCINANIE_DRZEW_PRZECINANIE_WOODCUTTING);
	
	ENV(TO)->catch_msg("Uda�o ci si� przeci�� "+kloda->short(PL_BIE)
						+" na dwie cz�ci.\n");
	kloda->remove_object();
}

void ustaw_klode(object ob)
{
	kloda = ob;
}

