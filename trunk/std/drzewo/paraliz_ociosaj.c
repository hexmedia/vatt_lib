inherit "/std/paralyze";
 
#include <exp.h>
#include <macros.h>
#include <math.h>
#include <filter_funs.h>
#include <pl.h>
#include <stdproperties.h>
#include <ss_types.h>

#define DEBUG(x) find_player("rantaur")->catch_msg(x+"\n")

#define KLODA "/std/drzewo/kloda.c"
#define GALAZ "/std/drzewo/galaz.c"
#define STERTA "/std/drzewo/sterta.c"
 
 /**********************
 *          ZMIENNE           *
 **********************/
 
object 	drzewo;
object 	tool;
object 	player;

float 	k_dmg;
int		f_cost;

void beat();
void ociosane(object player);
int przestal();

void create_paralyze()
{
	set_name("scinka_paraliz_ociosaj");
	set_stop_verb("przesta�");
	set_stop_message("Przestajesz obrabia� drzewo.\n");
	set_stop_object(this_object());
	set_stop_fun("przestal");
	set_fail_message("Teraz obrabiasz drzewo, je�li chcesz zrobi�"
				+" co� innego po prostu 'przesta�'.\n");
	set_finish_object(this_object());
	set_finish_fun("ociosane");	
	set_event_time(8);
	
	add_allowed("'");
	add_allowed("powiedz");

	setuid();
        seteuid(getuid());
}

void init_events()
{
	add_event("Szybkim ruchem �amiesz niewielk� ga��zk�, kt�ra zahaczy�a o tw� r�k�.",
			QCIMIE(player, PL_MIA)+" szybkim ruchem od�amuje od "+drzewo->short(PL_DOP)
			+" niewielk� ga��zk�.");
	add_event("Potykasz si� o jedn� z ga��zi.");
	add_event("Lekko podcinasz ga���, by za chwil� z�ama� j� krzepkim kopni�ciem.",
			QCIMIE(player, PL_MIA)+" lekko podcina jedn� z ga��zi "+drzewo->short(PL_DOP)
			+", by za chwil� z�ama� j� krzepkim kopni�ciem.");
	add_event("Z uporem rozsuwasz ga��zie, by odci�� je tu� przy nasadzie.", 
			QCIMIE(player, PL_MIA)+" z uporem rozsuwa ga��zie "+drzewo->short(PL_DOP)+", by odci�� je tu� przy nasadzie.");
}

/* Funkcja inicjujaca, wylicza potrzebne wartosci */
void start()
{
	set_alarm(3.0, 0.0, &beat());
}

/* Funkcja wywolywana przy kazdym uderzeniu w drzewo */
void beat()
{
	/* Sprawdzamy czy gracz ma na tyle sily, zeby walnac w drzewo */
	if(player->query_old_fatigue()-f_cost < 0)
	{
		player->catch_msg("Ze zm�czenia przerywasz obrabianie "+drzewo->query_short(1)+".\n");
		przestal();
		remove_object();
	}
	
	/* Jesli tak to zmniejszamy kondycje drzewa i meczymy gracza */
	player->add_old_fatigue(-f_cost);
	drzewo->mod_k_ociosaj(-k_dmg);
	
	/*tool->set_weapon_hits(tool->query_weapon_hits()+1);
	tool->did_hit();
	if(!tool->query_wielded())
	{
		player->catch_msg("Przestajesz �cina� drzewo.\n");
		przestal();
		remove_object();
	}*/
	
	/* I sprawdzamy czy drzewo nie zostalo juz przypadkiem sciete */
	if((intp(drzewo->query_k_ociosaj()) ? itof(drzewo->query_k_ociosaj()) : drzewo->query_k_ociosaj()) <= 0.0)
		ociosane(player);
	else
		set_alarm(3.0, 0.0, &beat());
	
}

/* Funkcja wywolywana po ociosaniu drzewa */
void ociosane(object player)
{
    /* Opisujemy */
    saybb(QCIMIE(player, PL_MIA)+" odcina jeszcze kilka mniejszych ga��zi,"
       +" po czym ko�czy obrabia� "+drzewo->short(PL_BIE)+".\n");
    player->catch_msg("Odcinasz jeszcze kilka mniejszych ga��zi, po czym"
       +" ko�czysz obrabia� "+drzewo->short(PL_BIE)+".\n");
	
    int exp_str = ftoi(LINEAR_FUNC(0.0, itof(EXP_SCINANIE_DRZEW_STR_MIN),
				   1500000.0, itof(EXP_SCINANIE_DRZEW_STR_MAX),
		        	   itof(drzewo->query_prop(OBJ_I_WEIGHT)))); 

    int exp_skill =  ftoi(LINEAR_FUNC(0.0, itof(EXP_SCINANIE_DRZEW_WOODCUTTING_MIN),
				     1500000.0, itof(EXP_SCINANIE_DRZEW_WOODCUTTING_MAX),
				     itof(drzewo->query_prop(OBJ_I_WEIGHT))));

    player->increase_ss(SS_STR, exp_str);
    player->increase_ss(SS_WOODCUTTING, exp_skill);

	/* Klonujemy galezie */
	int masa_galezi = ftoi(0.056*itof(drzewo->query_prop(OBJ_I_WEIGHT))+7207.0)+random(1500);
	int ile_galezi = ftoi(0.00000483*itof(drzewo->query_prop(OBJ_I_WEIGHT))+2.76)+random(4);
	int srednia = masa_galezi/ile_galezi;
	object galaz;
	int rand_masa;
	
	/* Okreslamy gdzie przenies galezie - na lokacje czy do sterty */
	object move_obj = ENV(drzewo);
	
	/*if(ile_galezi > 2)
	{
		move_obj = present("sterta galezi", ENV(drzewo));
		
		if(!move_obj)
		{
			move_obj = clone_object(STERTA);
			move_obj->move(ENV(drzewo));
		}
	}*/
	
	/* Galezie klonujemy parami, gdzie na przemian do wagi jednej dodajemy losowa wartosc,
		zas od drugiej odejmujemy */
    string sciezka_galezi = drzewo->query_sciezka_galezi();

	for(int i=0; i < ile_galezi; i++)
	{
		if(i%2 != 1)
		{
			rand_masa=random((3*srednia)/20);
			galaz = clone_object(sciezka_galezi);	
			galaz->add_prop(OBJ_I_WEIGHT, srednia+rand_masa);
		}
		else
		{
			galaz = clone_object(sciezka_galezi);
			galaz->add_prop(OBJ_I_WEIGHT, srednia-rand_masa);
		}

		galaz->move(move_obj);
	}
	
	object kloda = clone_object(drzewo->query_sciezka_klody());
	kloda->ustaw_gestosc(drzewo->query_gestosc());
	kloda->ustaw_cene(drzewo->query_cena());
	kloda->set_kloda_owner(drzewo->query_scinajacy());
	kloda->add_prop(OBJ_I_WEIGHT, (drzewo->query_prop(OBJ_I_WEIGHT)-masa_galezi));
	kloda->remove_owner_alarm();
	
	kloda->move(ENV(drzewo));
	
	drzewo->remove_object();
	remove_object();
}

int przestal()
{
	drzewo->set_scinajacy(0);
	saybb(QCIMIE(TP, PL_MIA)+" przestaje obrabia� "+drzewo->short(PL_BIE)+".\n");
	drzewo->remove_prop(OBJ_M_NO_GET);
	return 0;
}

void set_tool(object ob)
{
	tool = ob;
}

void set_drzewo(object ob)
{
	drzewo = ob;
}

void set_player(object ob)
{
	player = ob;
}

void set_k_dmg(float f)
{
	k_dmg = f;
}

void set_f_cost(int i)
{
	f_cost = i;
}


 
 
