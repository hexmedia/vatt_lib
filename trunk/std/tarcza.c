/**
 * \file /std/tarcza.c
 *
 * Standard tarcz i puklerzy.
 *
 * @author  Krun
 * @date    11.04.2008
 * @version 1.0
 */

#pragma no_reset
#pragma strict_types

inherit "/std/armour.c";
inherit "/lib/holdable_item.c";

#include <pl.h>
#include <wa_types.h>
#include <object_types.h>
#include <stdproperties.h>

static  int
        typ,        // typ tarczy
       *shield_ac;  // ac tarczy

public void ustaw_typ(int t);
public int  query_typ();

/**
 * Funkcja tworz�ca obiekt tarczy.
 */
void
create_tarcza()
{
    ustaw_nazwe(({"tarcza", "tarczy", "tarczy", "tarcze", "tarcz�", "tarczy"}),
        ({"tarcze", "tarczy", "tarczom", "tarczami", "tarczach", "tarczami"}),
        PL_ZENSKI);

    dodaj_przym("ma�y", "mali");
    dodaj_przym("tr�jk�tny", "tr�jk�tni");

    set_long("Najzwyklejsza w �wiecie tarcza.\n");

    ustaw_typ(SH_TARCZA);
}

/**
 * Funkcja tworz�ca chwytany obiekt.
 */
nomask void
create_armour()
{
    set_type(O_ZBROJE);

    ustaw_nazwe(({"tarcza", "tarczy", "tarczy", "tarcze", "tarcz�", "tarczy"}),
        ({"tarcze", "tarczy", "tarczom", "tarczami", "tarczach", "tarczami"}),
        PL_ZENSKI);
    ustaw_nazwe(({"puklerz", "puklerza", "puklerzowi", "puklerz", "puklerzem", "puklerzu"}),
        ({"puklerze", "puklerzy", "puklerzom", "puklerze", "puklerzami", "puklerzach"}),
        PL_MESKI_NOS_NZYW);

    //Tarcze maj� zawsze na wszystkich pasowa�.
    add_prop(ARMOUR_I_D_ROZCIAGLIWOSC, 100);
    add_prop(ARMOUR_I_U_ROZCIAGLIWOSC, 100);

    set_hands(W_ANYH);

    create_tarcza();

    set_last_layer(1);

    if(query_typ() != SH_PUKLERZ && member_array("puklerz", query_nazwy()[0]) != -1)
        remove_name("puklerz", "puklerze");

    if(query_typ() >= SH_TARCZA)
    {
        if(!pointerp(query_slots()) || !sizeof(query_slots()) || query_slots()[0] == 0)
        {
            if(!pointerp(shield_ac))
                shield_ac = ({10, 10, 10});

            set_slots(A_BACK);
            set_ac(A_BACK, shield_ac[0], shield_ac[1], shield_ac[2]);
        }
    }
}

/**
 * Ustawiamy typ tarczy.
 * @param t typ tarczy
 */
public void
ustaw_typ(int t)
{
    typ = t;
}

/**
 * @return Typ tarczy
 */
public int
query_typ()
{
    return typ;
}

/**
 * Ustawiamy ac naszej tarczy.
 *
 * @param klu - ac na rany k�ute.
 * @param cie - ac na rany ci�te.
 * @param obu - ac na rany obuchowe,
 */
public void
set_shield_ac(int klu, int cie, int obu)
{
    shield_ac = ({klu, cie, obu});
}

void
leave_env(object from, object dest, string fsubloc)
{
    holdable_item_leave_env(from, dest, fsubloc);

    ::leave_env(from, dest, fsubloc);
}

void
enter_env(object dest, object from)
{
    holdable_item_enter_env(dest, from);

    ::leave_env(dest, from);
}

/*
 * Musimy redefiniowa� i wear_me i wield_me poniewa� jest to za r�wno zbroja
 * jak i obiekt chwytany, i da�o by si� go w przeciwnym wypadku r�wnocze�nie
 * chwyci� i za�o�y�, a to nie efekt porz�dany.
 */

public varargs mixed
wear_me(int cicho, mixed na_miare = 0, string na_co = 0)
{
    if(query_wielded())
        return "Nie mo�esz za�o�y� " + query_nazwa(PL_CEL) + " kiedy " + koncowka("go", "j�") + " trzymasz.\n";

    return ::wear_me(cicho, na_miare, na_co);
}

public varargs mixed
hold_me(int cicho, string czym = 0)
{
    if(query_worn())
        return "Nie mo�esz chwyci� " + query_nazwa(PL_CEL) + " kiedy masz " + koncowka("go", "j�") + " na sobie.\n";

    return ::hold_me(cicho, czym);
}

/**
 * Identyfikuje obiekt jako tarcze.
 *
 * @return 1 zawsze
 */
public int
is_shield()
{
    return 1;
}

/**
 * Identyfikuje obiekt jako tarcze.
 *
 * @return 1 zawsze
 */
public int
is_tarcza()
{
    return 1;
}

/**
 * @return Opis jaki jest pokazywany graczowi podczas sparowania dan� tarcz�.
 */
string *
query_parry_desc()
{
    switch(query_typ())
    {
        case SH_PUKLERZ:
            return ({"zbijasz", "zbija"});
        case SH_TARCZA:
        case SH_PAWEZ:
            return ({"parujesz", "paruje"});
    }
}

