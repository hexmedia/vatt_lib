/* Std do woznic. Verek*/

#include <macros.h>

inherit "/std/humanoid.c";

object dylek;
//wrogowie to imiona tych, ktorzy wszczynali jakies burdy na dylku.
//nasz woznica nie bedzie ich obslugiwal
//(przynajmniej do apoki ;p )
string *wrogowie = ({ });

void
create_woznica()
{
}

nomask void
create_humanoid()
{
    //ustaw_nazwe("mezczyzna");
    dodaj_nazwy("woznica");

    create_woznica();
}

void
set_dylek(object d)
{
    dylek = d;
}

object
query_dylek()
{
    return dylek;
}

int
is_woznica()
{
    return 1;
}

void
dodaj_wroga(string imie)
{
    if(member_array(imie,wrogowie) == -1)
        wrogowie += ({imie});
}

string *
query_wrogowie()
{
    return wrogowie;
}

void
attacked_by(object wrog)
{
    //bo jesli jest w dylku, to wie co ma robic - wywala z niego.
    //kod w dylku_in.c
    if(!ENV(TO)->query_dylizans_in())
    {
        //dodajmy delikwenta do listy wrogow
        dodaj_wroga(wrog->query_real_name());
        //...i spitalamy, hhyhy :P
        dylek->stop();
        dylek->po_postoju();
    }
}

public string
query_auto_load()
{
    return 0;
}

void
hook_reaguj_na_walke()
{
    command("krzyknij I �ebym ci� tu wi�cej nie widzia�!");
}

void
hook_olej_wroga(object kogo)
{
    command("'A posz"+kogo->koncowka("ed�","�a","�o")+" mi tu st�d!");
    command("popatrz nieprzyjaznie na "+OB_NAME(kogo));
}


