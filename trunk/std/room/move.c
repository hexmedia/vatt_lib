/**
 * \file /std/room/move.c
 *
 * This is a sub-part of /std/room.c
 *
 * It handles loading rooms and moving between rooms.
 */

static string room_dircmd;   /* Command given after the triggering verb */

/**
 * Dzi�ki tej funkcji mo�emy sprawdzi� jak bardzo zm�czy nas przej�cie z jednej lokacji na drug�.
 *
 * @return zm�czenie
 */
public varargs int query_zmeczenie(int index, object meczony = this_player(), int przemykanie = 0)
{
    int tired;

    /* Compute the fatigue bonus. Sneaking gives double fatigue and
     * so does talking with 80% encumberance.
     */
    tired = query_tired_exit(index / 3) * FATIGUE_MULTIPLIER;
    tired = (przemykanie ? (tired * 2) : tired);

    switch(meczony->query_encumberance_weight())
    {
        case 20..39:
            tired += tired / 4 > 0 ? tired / 4 : 1; break;
        case 40..59:
            tired += tired / 3 > 3 ? tired / 3 : 3; break;
        case 60..79:
            tired += tired / 2 > 5 ? tired / 2 : 5; break;
        case 80..100:
            tired += tired     > 8 ? tired     : 8; break;
    }

    //Rebalansowalem to troche �eby dostosowa� do nowych liczb
    //==============================================================
    //5% zmeczenia od nienoszenia kt�rego� z but�w
    if(!sizeof(TP->query_slot(TS_R_FOOT)))
        tired += 5 * tired / 100;
    if(!sizeof(TP->query_slot(TS_L_FOOT)))
        tired += 5 * tired / 100;

    //I jeszcze troche podniesiemy, je�li mamy kaca, jeste�my pijani,
    //jeste�my g�odni czy spragnieni.

    int glod = TP->query_stuffed() * 100 / TP->query_prop(LIVE_I_MAX_EAT);
    int pragnienie = TP->query_soaked() * 100 / TP->query_prop(LIVE_I_MAX_DRINK);
    int kac = TP->query_headache() * 100 / TP->query_prop(LIVE_I_MAX_INTOX);
    int pijanstwo = TP->query_intoxicated() * 100 / TP->query_prop(LIVE_I_MAX_INTOX);

    switch(pragnienie)
    {
        case 0..30:     tired += 10 * tired / 100;  break;  // 10%
        case 31..50:    tired += 7  * tired / 100;  break;  // 7%
        case 51..60:    tired += 4  * tired / 100;  break;  // 4%
        case 61..90:                                break;  // pozostaje po staremu
        default:        tired -= 4  * tired / 100;  break;  // odejmujemy od zm�czenia 4%
    }

    switch(glod)
    {
        case 0..30:
            tired += 10 * tired / 100;  break;  // 10%
        case 31..50:
            tired += 5  * tired / 100;  break;  // 5%
    }

    switch(kac)
    {
        case 30..40:    tired += 4  * tired / 100;  break;  // 4%
        case 41..60:    tired += 8  * tired / 100;  break;  // 8%
        case 61..80:    tired += 12 * tired / 100;  break;  // 12%
        case 81..100:   tired += 16 * tired / 100;  break;  // 16%
    }

    switch(pijanstwo)
    {
        case 20..40:    tired += 5  * tired / 100;  break;  // 5%
        case 41..60:    tired += 10 * tired / 100;  break;  // 10%
        case 61..80:    tired += 15 * tired / 100;  break;  // 15%
        case 81..100:   tired += 20 * tired / 100;  break;  // 20%
    }

    return tired;
}

/*
 * Function name: load_room
 * Description  : Finds a room object for a given array index.
 * Arguments    : int index - of the file name entry in the room_exits array.
 * Returns      : object - pointer to the room corresponding to the argument
 *                         or 0 if not found.
 */
object
load_room(int index)
{
    mixed droom;
    string err;
    object ob;

    mixed room_exits = query_exit();

    droom = check_call(room_exits[index]);
    if (objectp(droom))
    {
        return droom;
    }

    /* Handle linkrooms that get destructed, bad wizard... baa-aad wizard. */
    if (!stringp(droom))
    {
        remove_exit(room_exits[index + 1]);
        this_player()->move_living("X", query_link_master());
        return 0;
    }

    ob = find_object(droom);
    if (objectp(ob))
    {
        return ob;
    }

    if (err = LOAD_ERR(droom))
    {
        SECURITY->log_loaderr(droom, environment(this_object()),
                              room_exits[index + 1], this_object(), err);
        write("B^l^ad we wczytywaniu:\n" + err + " <" + droom +
            ">\nKoniecznie 'zg^lo^s b^l^ad' o tym.\n");
        return 0;
    }
    return find_object(droom);
}

/*
 * Function name: query_dircmd
 * Description:   Gives the rest of the command given after move verb.
 *                This can be used in blocking functions (third arg add_exit)
 * Returns:       The movecommand as given.
 */
public string
query_dircmd()
{
    return room_dircmd;
}

/*
 * Function name: set_dircmd
 * Description:   Set the rest of the command given after move verb
 *                  This is mainly to be used from own move_living() commands
 *                  where you want a team to be able to follow their leader.
 * Arguments:     rest - The rest of the move command
 */
public void
set_dircmd(string rest)
{
    room_dircmd = rest;
}

/**
 * Funkcja wywo�ywana kiedy gracz chce przej�� z lokacji na lokacj� wykorzystuj�c
 * standardowe wyj�cie. Funkcja jest te� wykorzystywana do testowania czy
 * przej�cie jest mo�liwe.
 *
 * @param str           argument podany do komendy
 * @param przemykanie   czy gracz wpr�buje si� przemkn��
 * @param move_cmd      komenda jak� gracz pr�buje przej�� na inn� lokacje
 * @param test          czy gracz ma zosta� przeniesiony czy wykonujemy tylko test
 * @param team          czy przej�cie jest dru�ynowe.
 *
 * @return 1 lub 0
 */
public varargs int
unq_move(string str, int przemykanie = 0, string move_cmd = query_verb(), int test = 0, int team = 0)
{
    int index;
    int size;
    int wd;
    int tired;
    int tmp;
    int flag = TP->query_prop(PLAYER_I_IN_TEAM_GLANCE_LATER);
    object room;
    string wyjscie;

    if (stringp(str))
        move_cmd += " " + str;

    if(!TP)
        return 0;

    TP->remove_prop(PLAYER_I_IN_TEAM_GLANCE_LATER);

    room_dircmd = str;
    index = -3;
    mixed exits = query_exit();
    size = sizeof(exits);

    while((index += 3) < size)
    {
        if(!pointerp(exits[index + 1]))
            wyjscie = exits[index+1];
        else if (!pointerp(exits[index + 1][0]))
            wyjscie = exits[index + 1][0];
        else
        {
            for(int i = 0; i < sizeof(exits[index + 1][0]); i++)
            {
                wyjscie = exits[index + 1][0][i];
                if(!(move_cmd ~= wyjscie))
                    continue;
                else
                    break;
            }
        }

        /* This is not the verb we were looking for. */
        if (!(move_cmd ~= wyjscie))
            continue;

        /* Check whether the player is allowed to use the exit. */
        if ((wd = check_call(exits[index + 2])) > 0)
        {
            if (wd > 1)
                continue;
            else
                return !test;
        }

        /* Room could not be loaded, error message is printed. */
        if (!objectp(room = load_room(index)))
        {
            return !test;
        }

#if DO_NOT_TIRE_YOUNGER_THAN > 0
        if(TP->query_age() > DO_NOT_TIRE_YOUNGER_THAN)
        {
#endif
            tired = query_zmeczenie(index, TP, przemykanie);
            /* Gracz jest zbyt zm�czny na to aby przej�� z tej lokacji na nast�pn�. */
            if(TP->query_fatigue() < tired)
            {
                if(team)
                {
                    write("Nie mo�esz pod��y� za przyw�dc�, poniewa� jeste� na to zbyt "+
                        "zm�czony.\n");
                }
                else
                {
                    write("Jeste� tak zm�czon" + this_player()->koncowka("y", "a") + ", �e nie mo�esz " +
                        "i�� w tym kierunku.\n");
                }
                return !test;
            }
            if(!test)
                this_player()->add_fatigue(-tired);
#if DO_NOT_TIRE_YOUNGER_THAN > 0
        }
#endif

        if ((wd == 0) || (!transport_to(exits[index + 1], room, -wd)))
        {
            if(!test)
            {
                this_player()->im_moving(wyjscie);
                switch (this_player()->move_living(exits[index + 1], room, 0, flag))
                {
                    case 11:
                        notify_fail("Ju� si� tam nie zmie�cisz.\n");
                        return 0;
                }
            }
        }
        return 1;
    }

    /* We get here if a 'block' function stopped a move. The block function
     * should have printed a fail message.
     */
    return 0;
}

/*
 * Function name: unq_no_move
 * Description  : This function here so that people who try to walk into a
 *                'normal', but nonexistant direction get a proper fail
 *                message rather than the obnoxious "What?". Here, 'normal'
 *                exits are north, southeast, down, excetera.
 * Arguments    : string str - the command line argument.
 * Returns      : int 0 - always.
 */
public int
unq_no_move(string str)
{
    if (member_array(query_verb(), DEF_DIRS) != -1)
    {
        notify_fail("Nie widzisz ^zadnego wyj^scia prowadz^acego na " +
            (query_verb() ~= "g^ora" ? "g^or^e" : query_verb()) + ".\n");
    }
    return 0;
}
