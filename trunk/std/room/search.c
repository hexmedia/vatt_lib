/**
 * \file /std/room/search.c
 *
 * Plik wydzielony z /std/room/description.c bo funkcje szukania nie wiem z jakiej paki by�y tam.
 * W ka�dym razie dzi�ki temu b�dzie troche wi�kszy porz�dek i �atwiej si� w tym wszystki po�apa�.
 *
 * @author przeniesienia Krun
 * @author funkcji w description nieznany.
 * @date 2007
 */

#pragma strict_type

#include <exp.h>
#include <login.h>

static string
chrust_search(string str)
{
    if(str ~= "chrustu" || str ~= "chrust")
    {
        if(query_prop(ROOM_I_ILOSC_CHRUSTU) > 0)
        {
            return "Znajdujesz tu troch� chrustu kt�ry " +
                TP->koncowka("m�g�by�", "mog�aby�") + " zebra�.\n";
        }
    }

    return 0;
}

/**
 * Sprawdza czy gracz szuka na lokacji jakiego� standardowego obiektu,
 * takiego jak zio�a czy chrust.
 */
static string
search_fun(string str, int trail)
{
    string search_result;

    if (!trail && (search_result = chrust_search(str)))
        return search_result;

    if(!trail && (search_result = TO->herb_search(str)))
        return search_result;

    return ::search_fun(str, trail);
}

/**
 * Funkcja wywo�ywana kiedy gracz tropi.
 * @param player tropi�cy
 * @param track_skill u�yta umiej�tno�� tropienia.
 */
void
track_now(object player, int track_skill)
{
    string *track_arr,
            result = "Nie znajdujesz ^zadnych ^slad^ow.\n",
            dir,
           *dir_arr,
            race,
           *races = RACES + ({ "jakie^s zwierz^e" });
    int     i;
    mixed  *exits;

    if (!player)
        return ;

    track_arr = query_prop(ROOM_AS_DIR);

    // just in case, but presently, ROOM_I_INSIDE prevents setting of ROOM_AS_DIR
    if (query_prop(ROOM_I_INSIDE))
        result += "Wyst�pi� b��d RII w tropieniu, koniecznie 'zg�o�' go czarodziejom!\n";

    track_skill /= 2;
    track_skill += random(track_skill);

    if (CAN_SEE_IN_ROOM(player) && pointerp(track_arr) && track_skill > 0)
    {
        dir = track_arr[0];

        race = track_arr[1];
        switch (race)
        {
            case "elfk^e":          race = "elfa";          break;
            case "p^olelfk^e":      race = "elfa";          break;
            case "p^olelfa":        race = "elfa";          break;
            case "krasnoludk^e":    race = "krasnoluda";    break;
            case "nizio^lk^e":      race = "nizio^lka";     break;
            case "m^e^zczyzn^e":    race = "cz^lowieka";    break;
            case "kobiet^e":        race = "cz^lowieka";    break;
            case "gnomk^e":         race = "gnoma";         break;
        }

        result = "Jeste^s w stanie wyr^o^zni^c kilka ^slad^ow na ziemi.\n";

        TP->add_mana(-4);
        TP->add_old_fatigue(-random(8)+1);

        switch(track_skill)
        {
            case  1..10:
                player->increase_ss(SS_TRACKING, EXP_TROPIENIE_NIEUDANY_TRACKING);
                break;

            case 11..20:
                    /* Mamy szanse na pomylenie kierunku */
                if(random(3) > 1)
                {
                    exits = query_exit();

                    if((sizeof(exits) > 0) && (!wildmatch("w stron�*", dir)))
                    {
                            /* Oczyszczamy tablice z fake-wyjsc (Rantaur) */
                        int exits_size = sizeof(exits);
                        mixed *to_del = ({ });

                        for(int j = 0; j < exits_size; j++)
                        {
                            if(!stringp(exits[j]))
                                continue;

                            if(explode(exits[j], "#")[0] ~= "/std/drzewo/nadrzewie")
                            {
                                to_del += ({ exits[j] });
                                to_del += ({ exits[j+1] });
                                to_del += ({ exits[j+2] });

                                j += 2;
                            }
                        }

                        exits -= to_del;

                        if(i = sizeof(exits) - 1)
                        {
                            i = random(i / 3) * 3 + 1;

                            if (pointerp(exits[i]))
                                dir = exits[i][1];
                            else
                                dir = exits[i];

                            dir = " "+dir;
                        }
                    }
                }

                /* Z niskim umem nie zobaczymy prowadzacego na drzewo - Rantaur */
                if(!wildmatch("w stron�*", dir))
                    result += "Naj^swie^zsze prowadz^a prawdopodobnie"
                    + TO->modify_dir(dir) + ".\n";

                player->increase_ss(SS_TRACKING, EXP_TROPIENIE_PRAWIE_UDANY_TRACKING);
                break;

            case 21..50:
                result += "Naj^swie^zsze prowadz^a" + TO->modify_dir(dir) + ".\n";
                player->increase_ss(SS_TRACKING, EXP_TROPIENIE_UDANY_TRACKING);
                break;

            case 51..75:
                if(random(2))
                    race = ODMIANA_RASY[races[random(sizeof(races))]][0][PL_BIE];

                result += "Naj^swie^zsze zosta^ly pozostawione " +
                    "prawdopodobnie przez " + race + " i prowadz^a" + TO->modify_dir(dir) +
                    ".\n";
                player->increase_ss(SS_TRACKING, EXP_TROPIENIE_UDANY_TRACKING);
                break;

            case 76..150:
                result += "Najswie^zsze zosta^ly pozostawione przez " + race +
                    " i prowadz^a" + TO->modify_dir(dir) + ".\n";
                player->increase_ss(SS_TRACKING, EXP_TROPIENIE_UDANY_TRACKING);
                break;
        }
    }
    else
    {
        player->increase_ss(SS_TRACKING, EXP_TROPIENIE_NIEUDANY_TRACKING);
    }

    if (CAN_SEE_IN_ROOM(player) && track_skill > 0)
    {
        /*Info o ognisku. Vera */
        switch(query_prop(ROOM_WAS_FIRE))
        {
        case 0: break;
        case 1:
            if(track_skill > 90)
                result+="Zauwa�asz, �e jeszcze niedawno by�o tu "+
                "urz�dzone malutkie ognisko.\n";
            break;
        case 2:
            if(track_skill > 80)
                result+="Zauwa�asz, �e jeszcze niedawno by�o tu "+
                "urz�dzone ma�e ognisko.\n";
            break;
        case 3..4:
            if(track_skill > 62)
                result+="Zauwa�asz, �e jeszcze niedawno by�o tu "+
                "urz�dzone niewielkie ognisko.\n";
            break;
        case 5:
            if(track_skill > 42)
                result+="Zauwa�asz, �e jeszcze niedawno by�o tu "+
                "urz�dzone do�� spore ognisko.\n";
            break;
        case 6:
            if(track_skill > 22)
                result+="Zauwa�asz, �e jeszcze niedawno by�o tu "+
                "urz�dzone spore ognisko.\n";
            break;
        case 7..10:
            if(track_skill > 0)
                result+="Zauwa�asz, �e jeszcze niedawno by�o tu "+
                "urz�dzone ogromne ognisko.\n";
            break;
        default:
            if(query_prop(ROOM_WAS_FIRE)>10)
                result+="Zauwa�asz, �e jeszcze niedawno by�o tu "+
                "urz�dzone ogromne ognisko.\n";
            break;
        }
    }

    player->catch_msg(result);
    player->remove_prop(LIVE_S_EXTRA_SHORT);
    tell_roombb(environment(player), QCIMIE(player, PL_MIA) + " wstaje.\n",
                ({player}), player);
    return;
}

/**
 * Jaki� gracz rozpoczyna tropienie w tym pomieszczeniu.
 */
void
track_room()
{
    int     time,
        track_skill;
    object  paralyze;

    time = query_prop(OBJ_I_SEARCH_TIME);
    if (time < 1)
        time = 10;
    else
        time += 5;

    track_skill = this_player()->query_skill(SS_TRACKING);
    time -= track_skill/10;

    if (time < 1)
        track_now(this_player(), track_skill);
    else
    {
        set_alarm(itof(time), 0.0, &track_now(this_player(), track_skill));

        seteuid(getuid());
        paralyze = clone_object("/std/paralyze");
        paralyze->set_standard_paralyze("tropi^c");
        paralyze->set_stop_fun("stop_track");
        paralyze->set_stop_object(this_object());
        paralyze->set_stop_verb("przesta^n");
        paralyze->set_stop_message("Przestajesz szuka^c ^slad^ow.\n");
        paralyze->set_remove_time(time);
        paralyze->set_fail_message("Jeste^s zaj^et" +
                                   (this_player()->query_gender() == G_FEMALE ? "a" : "y") +
                                   " szukaniem ^slad^ow. Wpisz 'przesta^n', je^sli chcesz zrobi^c co^s " +
                                   "innego.\n");
        paralyze->move(this_player(),1);
    }
}

/**
 * Zako�czenie, lub przerwanie poszukiwania �lad�w.
 * @return Zawsze 0.
 */
varargs int
stop_track(mixed arg)
{
    if (!objectp(arg))
    {
        mixed *calls = get_all_alarms();
        mixed *args;
        int i;

        for (i = 0; i < sizeof(calls); i++)
        {
            if (calls[i][1] == "track_now")
            {
                args = calls[i][4];
                if (args[0] == this_player())
                    remove_alarm(calls[i][0]);
            }
        }
    }

    saybb(QCIMIE(this_player(), PL_MIA) + " przestaje szuka^c slad^ow.\n");
    this_player()->remove_prop(LIVE_S_EXTRA_SHORT);

    return 0;
}