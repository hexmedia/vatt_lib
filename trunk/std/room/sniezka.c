/**
 * Wydzielony z rooma plik odpowiedzialny za lepienie �nie�ek
 *
 * @author Vera
 */

void
ulepiona_sniezka(object player)
{
    int mverr;
    object ob;

    set_this_player(player);

    ob = clone_object("/d/Standard/obj/sniezka.c");

    mverr = ob->move(TP);

    if(mverr)
    {
        if(mverr == 1)
            write(UC(ob->short(PL_MIA)) + " jest za ci�k" + ob->koncowka("i", "a", "ie") + ".\n");
        else if(mverr == 8)
            write(UC(ob->short(PL_MIA)) + " jest za du�" + ob->koncowka("y", "a", "e") + ", "+
                "odk�adasz j� na ziemi^e.\n");
        else
            write("Nie jeste� w stanie podnie�� " + ob->short(PL_MIA) + ".\n");

        saybb(QCIMIE(TP, PL_MIA) + " odk�ada " + QSHORT(ob, PL_BIE) + ".\n");

        ob->move(environment(TP));
    }
    else
    {
        write("Sprawnie i szybko ugniatasz ze ^sniegu niezawodny pocisk.\n");
        saybb(QCIMIE(TP, PL_MIA) + " ugniata ze ^sniegu neizawodny pocisk.\n");
    }

}

int
przestan_lepic()
{
    write("Przestajesz lepi^c ^snie^zk^e.\n");
    return 0;
}

int
ulep_sniezke(string str)
{
    int time;
    object par;
    
    if(query_prop(ROOM_I_INSIDE))
        return 0;
    
    notify_fail("Co takiego chcesz ulepi�?\n");

    if(str != "�nie�k�" && str != "sniezke" && str != "�niezke") //itd... blebleble
        return 0;
    
    if(!CZY_JEST_SNIEG(ENV(TP)))
    {
        notify_fail("Ale tu nie ma �niegu!\n");
        return 0;
    }
    
    write("Przykl^ekasz nabieraj^ac w d^lonie troch^e ^sniegu i zaczynasz lepi^c ^snie^zk^e.\n");
    saybb(QCIMIE(TP,PL_MIA)+" nabiera w d^lonie troch^e ^sniegu i zaczyna lepi^c ^snie^zk^e.c\n");

    time = 1 + random(3);

    par = clone_object("/std/paralyze.c");
    par -> set_finish_object(TO);
    par -> set_finish_fun("ulepiona_sniezka");
    par -> set_stop_verb("przesta�");
    par -> set_standard_paralyze("lepi� �nie�k�");
    par -> set_stop_fun("przestan_lepic");
    par -> set_stop_object(TO);
    par -> set_remove_time(time);
    par -> set_fail_message("Teraz lepisz ^snie^zk^e, nie mo^zesz tego zrobi^c!\n");
    par -> move(TP, 1);

    return 1;
}

void
init_sniezki()
{
    add_action(ulep_sniezke, "ulep");   
}
