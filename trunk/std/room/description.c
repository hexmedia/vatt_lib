/**
 * \file /std/room/description.c
 *
 * To jest cz�� pliku /std/room.c
 *
 * In this module you will find the things relevant to the description of the
 * room.
 *
 */

#include <exp.h>
#include <math.h>
#include <login.h>
#include <colors.h>
#include <mudtime.h>
#include <language.h>
#include <ss_types.h>
#include <composite.h>
#include <stdproperties.h>

#define EXITS_COLOR COLOR_FG_YELLOW

static  mixed   room_descs;                         // Extra longs added to the rooms own
static  int gDefaultExitDesc = 0;
static mapping rzeczy_niewyswietlane = ([ ]);       // rzeczy, kt�rych nie wy�wietlamy w defaultowej sublokacji
static mapping rzeczy_niewyswietlane_plik = ([]);   // rzeczy, kt�rych nie wy�wietlamy w defaultowej sublokacji
static int niewyswietlane_mask = 0;                 // maska typ�w rzeczy niewy�wietlanych
string polmrok_long;                                // d�ugi opis p�mroku.

#ifdef DBG
#undef DBG
#define DBG(x) find_player("krun")->catch_msg("B��D W ROOMIE(descr)" + x + "\n")
#endif DBG

/**
 * Funkcja ustawiaj�ca mask� rzeczy niewy�wietlanych.
 *
 * @param maska - nowa maska rzeczy niewy�wietlanych.
 *
 * Maska rzeczy niewy�wietlanych odpowiada za wyrzucenie z opis�w lokacji
 * pewnych typ�w przedmiot�w.
 */
public void
set_niewyswietlane_mask(int maska)
{
    niewyswietlane_mask = maska;
}

/**
 * Funkcja zwracaj�ca mask� rzeczy niewy�wietlanych
 *
 * @return maska rzeczy niewy�wietlanych.
 *
 * Maska rzeczy niewy�wietlanych odpowiada za wyrzucenie z opis�w lokacji
 * pewnych typ�w przedmiot�w.
 */
public int
get_niewyswietlane_mask()
{
    return niewyswietlane_mask;
}

/**
 * Dodaje rzecz/rzeczy do listy rzeczy niewy�wietlanej na domy�lnej sublokacji przez podanie shorta.
 *
 * @param rzecz     Nazwa/Short rzeczy, kt�ra ma by� niewy�wietlana
 * @param ilosc     Ile rzeczy ma by� niewy�wietlanych (domy�lnie - 1)
 */
public void
dodaj_rzecz_niewyswietlana(string rzecz, int ilosc = 1)
{
    int tmp, n;
    string *pomtab;

    pomtab = m_indices(rzeczy_niewyswietlane);

    if ((n = member_array(rzecz, pomtab)) == -1)
        rzeczy_niewyswietlane[rzecz] = ({0, ilosc});
    else
    {
        tmp = rzeczy_niewyswietlane[pomtab[n]][1];
        rzeczy_niewyswietlane[pomtab[n]] = ({0, ilosc + tmp});
    }
}

/**
 * Dodaje rzecz/rzeczy do listy rzeczy niewy�wietlanych na domy�lnej sublokacji przez podanie pliku.
 *
 * @param rzecz     Nazwa pliku - pe�na �cie�ka - rzeczy kt�ra ma by� niewy�wietlana.
 * @param ilosc     Ile rzeczy ma by� niewy�wietlanych (domy�lnie - 1)
 */
public void
dodaj_rzecz_niewyswietlana_plik(string rzecz, int ilosc = 1)
{
    int tmp, n;
    string *pomtab;

    pomtab = m_indices(rzeczy_niewyswietlane_plik);

    if ((n = member_array(rzecz, pomtab)) == -1)
        rzeczy_niewyswietlane_plik[rzecz] = ({0, ilosc});
    else
    {
        tmp = rzeczy_niewyswietlane_plik[pomtab[n]][1];
        rzeczy_niewyswietlane_plik[pomtab[n]] = ({0, ilosc + tmp});
    }
}

/**
 * Usuwa rzecz/rzeczy z listy rzeczy niewy�wietlanych na domy�lnej sublokacji dodan� poprzez podanie shorta.
 *
 * @param rzecz     Nazwa/Short rzeczy, kt�ra ma zosta� usuni�ta
 * @param ilosc     Ile rzeczy ma zosta� usuni�tych z listy (domy�lnie - 1).
 */
public void
usun_rzecz_niewyswietlana(string rzecz, int ilosc = 1)
{
    int tmp, n;
    string *pomtab;

    pomtab = m_indices(rzeczy_niewyswietlane);

    if ((n = member_array(rzecz, pomtab)) != -1)
    {
        tmp = rzeczy_niewyswietlane[pomtab[n]][1] - ilosc;

        if (tmp <= 0)
        {
            tmp = 0;
            m_delete(rzeczy_niewyswietlane, pomtab[n]);
            return;
        }

        rzeczy_niewyswietlane[pomtab[n]] = ({0, tmp});
    }
}

/**
 * Usuwa rzecz/rzeczy z listy rzeczy niewy�wietlanych na domy�lnej sublokacji dodan� poprzez podanie pliku.
 *
 * @param rzecz     Nazwa pliku - pe�na �cie�ka - rzeczy kt�ra ma by� niewy�wietlana.
 * @param ilosc     Ile rzeczy ma zosta� usuni�tych z listy (domy�lnie - 1).
 */
public void
usun_rzecz_niewyswietlana_plik(string rzecz, int ilosc = 1)
{
    int tmp, n;
    string *pomtab;

    pomtab = m_indices(rzeczy_niewyswietlane_plik);

    if ((n = member_array(rzecz, pomtab)) != -1)
    {
        tmp = rzeczy_niewyswietlane_plik[pomtab[n]][1] - ilosc;

        if (tmp <= 0)
        {
            tmp = 0;
            m_delete(rzeczy_niewyswietlane_plik, pomtab[n]);
            return;
        }
        rzeczy_niewyswietlane_plik[pomtab[n]] = ({0, tmp});
    }
}

/**
 * Resetuje licznik wy�wietle� w rzeczach niewy�wietlanych dodanych przez podanie shorta.
 */
public void
oczysc_rzeczy_niewyswietlane()
{
    int i, il, tmp;
    string* pomtab;

    pomtab = m_indices(rzeczy_niewyswietlane);
    il = sizeof(pomtab);

    for (i = 0; i < il; ++i)
    {
        tmp = rzeczy_niewyswietlane[pomtab[i]][1];
        rzeczy_niewyswietlane[pomtab[i]] = ({ 0, tmp });
    }
}

/**
 * Resetuje licznik wy�wietle� w rzeczach niewy�wietlanych dodanych przez podanie �cie�ki.
 */
public void
oczysc_rzeczy_niewyswietlane_plik()
{
    int i, il, tmp;
    string* pomtab;

    pomtab = m_indices(rzeczy_niewyswietlane_plik);
    il = sizeof(pomtab);

    for (i = 0; i < il; ++i)
    {
        tmp = rzeczy_niewyswietlane_plik[pomtab[i]][1];
        rzeczy_niewyswietlane_plik[pomtab[i]] = ({ 0, tmp });
    }
}

/**
 * Sprawdza, czy dana rzecz ma by� wy�wietlona. Aktualizuje licznik wy�wietle�.
 *
 * @param rzecz     Obiekt kt�ry ma zosta� sprawdzony
 *
 * @return <ul><li>1 - rzecz mo�e by� wy�wietlona</li>
 *             <li>0 - rzecz nie mo�e by� wy�wietlona</li></ul>
 */
public int
czy_wyswietlic_rzecz(object rzecz)
{
    int n;
    int* tmp;
    string* pomtab;

    if (!objectp(rzecz))
       return 0;

    // sprawdzamy mask� rzeczy niewy�wietlanych
    if (rzecz->query_type() & niewyswietlane_mask)
        return 0;

    // sprawdzamy odpowiedniego propa
    if (rzecz->query_prop(OBJ_I_DONT_SHOW_IN_LONG))
        return 0;

    // sprawdzamy w�r�d rzeczy po pliku;
    pomtab = m_indices(rzeczy_niewyswietlane_plik);

    if ((n=member_array(MASTER_OB(rzecz), m_indices(rzeczy_niewyswietlane_plik))) != -1)
    {
        tmp = rzeczy_niewyswietlane_plik[pomtab[n]];
        tmp[0] = tmp[0] + 1;
        rzeczy_niewyswietlane_plik[pomtab[n]] = tmp;

        if (rzeczy_niewyswietlane_plik[pomtab[n]][1] == 0)
            return 0;
        if (rzeczy_niewyswietlane_plik[pomtab[n]][0] <= rzeczy_niewyswietlane_plik[pomtab[n]][1])
            return 0;
    }

    // sprawdzamy w�r�d rzeczy po nazwie
    pomtab = m_indices(rzeczy_niewyswietlane);
    if ((n = member_array(rzecz->short(), m_indices(rzeczy_niewyswietlane))) != -1)
    {
        tmp = rzeczy_niewyswietlane[pomtab[n]];
        tmp[0] = tmp[0] + 1;
        rzeczy_niewyswietlane[pomtab[n]] = tmp;

        if (rzeczy_niewyswietlane[pomtab[n]][1] == 0)
            return 0;

        if (rzeczy_niewyswietlane[pomtab[n]][0] <= rzeczy_niewyswietlane[pomtab[n]][1])
            return 0;
    }

    return 1;
}

/**
 * A to taka pierdolnikowa funkcja, potrzeba mi do 'sp na kierunek',
 * mo�e jeszcze gdzie indziej b�dzie przydatna. Vera
 */
public string *
query_shorty_rzeczy_niewyswietlanych()
{
    return m_indexes(rzeczy_niewyswietlane);
}

/**
 * Funkcja dzia�a podbnie jak set_long.
 * Je�li polmrok_long b�dzie zdefiniowany to opis ten b�dzie si� pokazywa� podczas p�mroku, je�li
 * nie zobaczymy domy�lny opis p�mroku - w czasie gdy na lokacji panuje p�mrok oczywi�cie.
 */
public void
set_polmrok_long(string str)
{
    polmrok_long = str;
}

/**
 * @return opis p�mroku
 */
public string
polmrok_long()
{
    return check_call(polmrok_long);
}

/*
 * Function name: 	add_my_desc
 * Description:   	Add a description printed after the normal
 *                      longdescription.
 * Arguments:	  	str: Description as a string
 *			cobj: Object responsible for the description
 *			      Default: previous_object()
 */
public void
add_my_desc(string str, object cobj = previous_object())
{
    if (query_prop(ROOM_I_NO_EXTRA_DESC))
        return;

    if (!room_descs)
        room_descs = ({ cobj, str });
    else
        room_descs = room_descs + ({ cobj, str });
}

/*
 * Function name:       change_my_desc
 * Description:         Change a description printed after the normal
 *                      longdescription. NOTE: if an object has more than
 *                      one extra description only one will change.
 * Arguments:           str: New description as a string
 *                      cobj: Object responsible for the description
 *                            Default: previous_object()
 */
public void
change_my_desc(string str, object cobj = previous_object())
{
    int i;
    mixed tmp_descs;

    if (query_prop(ROOM_I_NO_EXTRA_DESC))
        return;

    if (!cobj)
        return;

    i = member_array(cobj, room_descs);

    if (i < 0)
        add_my_desc(str, cobj);
    else
        room_descs[i + 1] = str;
}

/*
 * Function name: 	remove_my_desc
 * Description:   	Removes an earlier added  description printed after
 *                      the normal longdescription.
 * Arguments:	  	cobj: Object responsible for the description
 *			      Default: previous_object()
 */
public void
remove_my_desc(object cobj = previous_object())
{
    int i, sf;

    if (query_prop(ROOM_I_NO_EXTRA_DESC))
        return;

    sf = objectp(cobj);

    i = member_array(cobj, room_descs);
    while (i >= 0)
    {
        if (sf)
            room_descs = exclude_array(room_descs, i, i + 1);
        else
            room_descs = exclude_array(room_descs, i - 1, i);
        i = member_array(cobj, room_descs);
    }
}

/*
 * Function name: 	query_desc
 * Description:   	Gives a list of all added descriptions to this room
 * Returns:       	Array on the form:
 *			    desc1, obj1, desc2, obj2, ..... descN, objN
 */
public mixed
query_desc()
{
    return slice_array(room_descs, 0, sizeof(room_descs));
}

private string
add_colors(string exit)
{
    return set_color(EXITS_COLOR) + exit + clear_color();
}

private static int
filter_open_door(object ob)
{
    if(ob->is_door() && ob->query_open())
        return 1;
}

private static string
map_open_door(string *str)
{
     return explode(str[0] + " ", " ")[0];
}

/**
 * Funkcja wy�wietla wszystkie widoczne wyj�cia z lokacji w standardowy spos�b.
 * Funkcja ta w miare mo�liwo�ci powinna by� edytowana i napisana w spos�b opisowy.
 *
 * @return opis wyj��.
 */
public string
exits_description()
{
    string *exits;
    int size;

    exits = query_obvious_exits();
    exits += map(filter(all_inventory(TO), &filter_open_door())->query_pass_command(), &map_open_door());

    size = sizeof(exits);

    if(size == 0)
        return "";

    exits = map(exits, &add_colors());

    gDefaultExitDesc = 1;

    switch(ilosc(size, 1, 2, 3))
    {
        case 1:
            return "Jest tutaj jedno widoczne wyj^scie: " + exits[0] + ".\n";
        case 2:
            return "S^a tutaj " + LANG_SNUM(size, PL_MIA, PL_NIJAKI_NOS)
                + " widoczne wyj^scia: " + COMPOSITE_WORDS(exits) + ".\n";
        case 3:
            return "Jest tutaj " + LANG_SNUM(size, PL_MIA, PL_NIJAKI_NOS)
                 + " widocznych wyj^s^c: " + COMPOSITE_WORDS(exits) + ".\n";
    }
}

/**
 * Funkcja opisuj�ca pomieszczenie i jego wyj�cia lub item dodany przez \see add_item() o nazwie podanej w argumencie
 *
 * @param str   Nazwa itemu kt�ry chcemy obejrze� (opt)
 *
 * @return D�ugi opis pomieszczenia wraz z wyj�ciami, lub je�li podali�my argument, opis itemu.
 */
varargs public mixed
long(string str)
{
    int index;
    int size;
    mixed lg;
    mixed herbs_obs;
    string ex, ziola;

    lg = ::long(str);
    if (stringp(str))
        return lg;
    if (!stringp(lg))
        lg = "";

    /* This check is to remove extra descriptions that have been added by
     * an object that is now destructed.
     */
    while ((index = member_array(0, room_descs)) >= 0)
        room_descs = exclude_array(room_descs, index, index + 1);

    if (pointerp(room_descs))
    {
        index = -1;
        size = sizeof(room_descs);
        while((index += 2) < size)
        {
            lg = lg + process_tags(check_call(room_descs[index]));
        }
    }

    if (room_no_obvious)
        return lg;

    herbs_obs = TP->query_prop(LIVE_I_FOUND_HERBS);
    if(mappingp(herbs_obs))
        if(member_array(MASTER, m_indexes(herbs_obs)))
            herbs_obs = herbs_obs[MASTER];
        else
            herbs_obs = 0;
    else
        herbs_obs = 0;

    //FIXME: Dopisa� zapominanie znalezionych zi�.
    TO->zapomnij_ziola(TP->query_real_name());

    ex = exits_description();
    ziola = TO->opisz_ziola(TO->query_znalezione(TP->query_real_name()), 1);

    return lg + (ziola ? ziola : "") + (gDefaultExitDesc ? ex :
        set_color(EXITS_COLOR) + ex + clear_color());
}

/**
 * Dopisuje z przedu na je�li kierunek jes standardowy.
 */
string
modify_dir(string dir)
{
    switch(dir)
    {
        case "p^o^lnoc":
        case "p^o^lnocny-wsch^od":
        case "wsch^od":
        case "po^ludniowy-wsch^od":
        case "po^ludnie":
        case "po^ludniowy-zach^od":
        case "zach^od":
        case "p^o^lnocny-zach^od":
        case "g^ore":
        case "d^o^l":
            return "na " + dir;
        case "g^ora":
            return "na g^or^e";
        default:
            return dir;
    }
}

/**
 * Definiuje komunikat dotycz�cy godziny dla komendy czas.
 * Funkcja ta mo�e, a nawet powinna by� maskowana stosownie
 * do kalendarza lokalnego.(Mo�na by co� pomy�le� nad opcj�
 * wyboru kalendarza kt�rym chcemy si� pos�ugiwa�, jako cz�owiek
 * ma�o prawdopodobne abym wog�le zna� elfi kalendarz..).
 *
 * @return Pe�en komunikat b�d� 0 - je�li 0 gracz otrzyma komunikat standardowy
 */
public string check_time()
{
    return MT_CZAS_MUDOWY;
}

/**
 * @return Data zmiany pory dnia dla lokacji.
 */
public string check_date()
{
    return MT_DATA_MUDOWA;
}

/**
 * @return Aktualna pora dnia zgodna z definicjami z <mudtime.h>
 */
public int
pora_dnia()
{
    return MT_PORA_DNIAXY(TO->query_prop(ROOM_I_WSP_X), TO->query_prop(ROOM_I_WSP_Y));
}

/**
 * @return Aktualna pora roku z <mudtime.h>.
 */
public int
pora_roku()
{
    return MT_PORA_ROKU;
}

/**
 * Sprawdza czy na lokacji jest dzie� czy noc.
 * @warning
 *     Lepiej u�ywa� funkcji o bardziej intuicyjnej nazwie 'jest_dzien' lub 'jest_noc'.
 *
 * @return 1 je�li jest obecnie noc
 * @return 0 je�li dzie�.
 */
public int
dzien_noc()
{
    switch (MT_PORA_DNIAXY(TO->query_prop(ROOM_I_WSP_X), TO->query_prop(ROOM_I_WSP_Y)))
    {
        case MT_WCZESNY_RANEK:
        case MT_RANEK:
        case MT_POLUDNIE:
        case MT_POPOLUDNIE:
        case MT_WIECZOR:
            return 0;
        case MT_POZNY_WIECZOR:
        case MT_NOC:
        case MT_SWIT:
            return 1;
        default:
            throw("Illegal value returned: pora_dnia() = " + pora_dnia()
                + ".\n");
    }
}

/**
 * Sprawdza czy na lokacji jest dzie� czy noc.
 *
 * @return 1 je�li dzie�.
 * @return 0 je�li noc.
 */
public int
jest_dzien()
{
    return !dzien_noc();
}

/**
 * Sprawdza czy na lokacji jest dzie� czy noc.
 *
 * @return 1 je�li noc.
 * @return 0 je�li dzie�.
 */
public int
jest_noc()
{
    return dzien_noc();
}

/**
 * @return Zwraca kolor jaki ustawione maj� wyj�cia.
 */
int
query_exits_color()
{
    return EXITS_COLOR;
}

/**
 * @return Zwraca czy opis wyj�� pokazywany graczowi na lokacji jest standardowy czy te� nie.
 */
int
query_default_exits_description()
{
    return gDefaultExitDesc;
}
