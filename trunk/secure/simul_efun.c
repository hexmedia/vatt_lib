/**
 * \file /secure/simul_efun.c
 *
 * W tym pliku znajduj� si� wszystkie funkcje symuluj�ce dzia�aniem
 * funkcje z drivera. S� one wywo�ywane tak, jakgdyby by�y inheritowane
 * w ka�dym obiekcie, jednak dzieje si� to automatycznie.
 *
 * It is managed with a hidden call_other() though so the functions will
 * not appear as true internal functions in objects.
 */

/* We must use the absolute path here because SECURITY is not loaded yet
 * when we load this module.
 */
#include "/sys/filter_funs.h"
#include "/sys/std.h"
#include "/sys/macros.h"
#include "/sys/colors.h"
#include "/sys/options.h"
#include "/sys/stdproperties.h"
#include "/sys/flags.h"
#include "/sys/filepath.h"
#include "/sys/mxp.h"
#include "/sys/cmdparse.h"
#include "/sys/przymiotniki.h"

#pragma no_clone
#pragma no_inherit
#pragma resident
#pragma strict_types

/* Define this if you have CFUN's turned on. If not, then LPC functions will
 * be used rather than CFUN implementations in the gamedriver.
 */
#define CFUN
#define SLVTHRC_LOG(str) \
    ("/d/Wiz/silvathraec/private/say_logger"->log_say(str))

/* Prototypes */
varargs string getuid(object ob);
int atoi(string num);
int seteuid(string str);
// varargs string set_color(mixed who, int color, int tlo=0, ...);
// varargs string clear_color(object who=TP);

int is_link(string path);
nomask varargs void dump_array(mixed a, string tab);
nomask varargs void dump_mapping(mapping m, string tab);
static void dump_elem(mixed sak, string tab);

/*
 * Called by master on startup to get correct uids
 */
void
fixeuid()
{
    set_auth(this_object(), "root:root");
}

/*
 * No one is allowed to shadow the simulated efuns
 */
int
query_prevent_shadow()
{
    return 1;
}

/************************************************************************
 *
 * EFUN SHELLS
 *
 * These are here for reasons of compatibility and comfort
 *
 */

void
write(mixed data) = "write";

void
tell_object(object liveob, string message)
{
    liveob->catch_tell(message);
}

static string
getnames(object ob)
{
    return ob->query_real_name();
}

/*
 * Nazwa funkcji: m_indices
 * Opis         : Here it is Dworkin, for all language purists...
 *                Obviously the proper plural for "index" is "indices" and
 *                not "indexes" as the author of this efun thought.
 * Argumenty    : mixed arg - the argument for the efun m_indexes()
 * Zwraca       : mixed - the return value from the efun m_indexes().
 */
mixed
m_indices(mixed arg)
{
    return m_indexes(arg);
}


/*
 * Nazwa funkcji: slice_array
 * Opis         : Return a portion of a given array
 * Argumenty    : mixed a - an array
 *                int f   - The index at which to start.  If less
 *                          than zero, it is set to 0.
 *                int t   - the index at which to end.  If greater
 *                          or equal to the size of 'a', it is
 *                          set to sizeof(a) - 1.
 * Zwraca       : mixed   - The portion of array 'a', starting at
 *                          'f' and ending at 't'.
 */
mixed
slice_array(mixed a, int f, int t)
{
    if (f < 0)
        f = 0;
    if (t >= sizeof(a))
        t = sizeof(a) - 1;
    if (f > t)
        return ({});
    return (pointerp(a)) ? a[f..t] : 0;
}


static string
slice_cmds(mixed *ar)
{
    return ar[0];
}

/*
 * Nazwa funkcji: get_localcmd
 * Opis         : Returns an array of commands (excluding soul commands)
 *                available to an object.
 * Argumenty    : mixed ob - the object for which you want a command list
 * Zwraca       : string * - an array of command names
 */
varargs string *
get_localcmd(mixed ob = previous_object())
{
    if (!objectp(ob))
        return ({});

    return map(commands(ob), slice_cmds);
}

void
localcmd()
{
    this_player()->catch_tell(implode(get_localcmd(previous_object()), " ") + "\n");
}

#ifdef CFUN
varargs int
cat_file(string file, int start, int len) = "cat_file";

varargs int
cat(string file, int start, int len)
{
    string euid, slask;
    int ret;

    sscanf(query_auth(previous_object()), "%s:%s", slask, euid);

    set_auth(this_object(), "#:" + euid);
    ret = cat_file(file, start, len);
    set_auth(this_object(), "#:root");
    return ret;
}

#else
varargs int
cat(string file, ...)
{
    string *lines;
    int i;
    string euid, slask;

    sscanf(query_auth(previous_object()), "%s:%s", slask, euid);

    set_auth(this_object(), "#:" + euid);

    switch(sizeof(argv))
    {
    case 0:
        slask = "" + read_file(file);
        break;
    case 1:
        slask = "" + read_file(file, argv[0]);
        break;
    case 2:
        slask = "" + read_file(file, argv[0], argv[1]);
        break;
    default:
        set_auth(this_object(), "#:" + "root");
        throw("Too many arguments to cat.");
        break;
    }

    set_auth(this_object(), "#:" + "root");
    lines = explode(slask, "\n");
    this_player()->catch_tell(slask);
    // Kludge warning!!!
    return (sizeof(lines) == 1 && strlen(lines[0]) <= 3) ? 0 : sizeof(lines);
}
#endif CFUN

/*
 * Nazwa funkcji: exclude_array
 * Opis         : Deletes a section of an array
 * Argumenty    : arr: The array
 *		  from: Index from which to delete elements
 *		  to: Last index to be deleted.
 * Zwraca       :
 */
public mixed
exclude_array(mixed arr, int from, int to)
{
    mixed a,b;
    if (!pointerp(arr))
        return 0;

    if (from > sizeof(arr))
        from = sizeof(arr);
    a = (from <= 0 ? ({}) : arr[0 .. from - 1]);

    if (to < 0)
        to = 0;
    b = (to >= sizeof(arr) - 1 ? ({}) : arr[to + 1 .. sizeof(arr) - 1]);

    return a + b;
}

/*
 * Nazwa funkcji: inventory
 * Opis         : Zwraca wskaznik do obiektu o podanym numerze w ekwipunku
 *		  podanego obiektu. Funkcja korzysta z efunkcji
 *		  all_inventory().
 * Argumenty: mixed ob  - obiekt o ktorego zawartosc chodzi. Domyslnie
 * 		   	  previous_object()
 *	      mixed num - numer obiektu do pobrania. Powinien to byc integer,
 *			  wskazujacy na index obiektu w tablicy, jaka zwraca
 *			  all_inventory(ob).
 * Funkcja zwraca:
 *	      object    - Element o numerze num z ekwipunku obiektu.
 */
varargs nomask object
inventory(mixed ob, mixed num)
{
    object         *inv;

    if (!objectp(ob))
    {
        if (intp(ob))
            num = ob;
        ob = previous_object();
    }

    inv = all_inventory(ob);

    if ((num <= sizeof(inv)) && (num >= 0))
        return inv[num];
    else
        return 0;
}

/*
 * Nazwa funkcji: all_environment
 * Opis         : Gives an array of all containers which an object is in, i.e.
 *		  match in matchbox in bigbox in chest in room, would for the
 *		  match give: matchbox, bigbox, chest, room
 * Argumenty    : ob: The object
 * Zwraca       : The array of containers.
 */
nomask object *
all_environment(object ob)
{
    object *r;

    if (!ob || !environment(ob))
        return 0;
    if (!environment(environment(ob)))
        return ({ environment(ob)  });

    r = ({ ob = environment(ob) });

    while (environment(ob))
        r += ({ ob = environment(ob)  });

    return r;
}

/*
 * query_xverb should return the part of the verb that had to be filled in
 * when an add_action(xxx, "yyyyy", 1) was executed.
 * Until we get a GD implementation it will simply return query_verb().
 */
nomask          string
query_xverb()
{
    return query_verb();
}

#ifdef CFUN
static nomask mixed
sort(mixed arr, function lfunc) = "sort";

varargs nomask mixed
sort_array(mixed arr, mixed lfunc, object obj)
{
    if (stringp(lfunc))
    {
        if (!obj)
            obj = previous_object();
        lfunc = mkfunction(lfunc, obj);
    }
    return sort(arr, lfunc);
}
#else
/*
 * Macros and forward declarations for the sort function(s)
 */

/*
 * MTHRESH is the smallest partition for which we compare for a median
 * value instead of using the middle value.
 */
#define	MTHRESH	6

/*
 * THRESH is the minimum number of entries in a partition for continued
 * partitioning.
 */
#define	THRESH	4

/*
 * Make comparison of elements a bit easier to read
 */
#define	compar(a, b)	call_other(obj, lfunc, arr[a], arr[b])

/*
 * Swap two array elements
 */
#define	SWAP(a, b) { \
	elem = arr[a]; \
	arr[a] = arr[b]; \
	arr[b] = elem; \
    }

/*
 * Knuth, Vol. 3, page 116, Algorithm Q, step b, argues that a single pass
 * of straight insertion sort after partitioning is complete is better than
 * sorting each small partition as it is created.  This isn't correct in this
 * implementation because comparisons require at least one (and often two)
 * function calls and are likely to be the dominating expense of the sort.
 * Doing a final insertion sort does more comparisons than are necessary
 * because it compares the "edges" and medians of the partitions which are
 * known to be already sorted.
 *
 * This is also the reasoning behind selecting a small THRESH value (see
 * Knuth, page 122, equation 26), since the quicksort algorithm does less
 * comparisons than the insertion sort.
 */
#define	SORT(bot, n) { \
	if (n > 1) \
	    if (n == 2) \
	    { \
		  t1 = bot + 1; \
		  if (compar(t1, bot) < 0) \
		      SWAP(t1, bot); \
	    } \
	    else \
		arr = insertion_sort(arr, bot, bot + n - 1, lfunc, obj); \
    }

static mixed
quick_sort(mixed arr, int orig_bot, int orig_top, string lfunc, object obj);
static mixed
insertion_sort(mixed arr, int orig_bot, int orig_top, string lfunc, object obj);

public varargs void log_file(string file, string text, int csize);

/*
 * Nazwa funkcji:  	sort_array
 * Opis         :	Sorts the elements of an array
 * Argumenty    :	arr: The array to be sorted
 *			lfunc: (optional) Function taking two arguments
 *			       and returns negative value if arg1 < arg2,
 *			       zero if arg1 == arg2 and positive if
 *			       arg1 > arg2.
 *			       Default: Compare arguments directly
 *			obj: (optional) Object defining above function.
 *			     Default: this_object()
 *
 */
varargs nomask mixed
sort_array(mixed arr, string lfunc, object obj)
{

    if (sizeof(arr) < 2)
        return arr;

    if (!lfunc)
    {
        lfunc = "sort_compare";
        obj = this_object();
    }
    else if (!obj)
        obj = previous_object();

    if (sizeof(arr) >= THRESH)
        return quick_sort(arr, 0, sizeof(arr) - 1, lfunc, obj);
    else
        return insertion_sort(arr, 0, sizeof(arr) - 1, lfunc, obj);
}

static mixed
quick_sort(mixed arr, int orig_bot, int orig_top, string lfunc, object obj)
{
    mixed elem;
    int   top, bot, mid, nmemb, bsv, t1, t2, n1, n2, skipit;

    nmemb = orig_top - orig_bot + 1;
    bot = orig_bot;

    /*
     * bot and nmemb must be set
     */
    for (;;) {
        /*
         * Find middle and top elements
         */
        mid = bot + (nmemb >> 1);
        top = bot + nmemb - 1;

        /*
         * Find the median of the first, last and middle element (see Knuth,
         * Vol. 3, page 123, Eq. 28).  This test order gets the equalities
         * right.
        */
        if (nmemb >= MTHRESH)
        {
            n1 = compar(bot, mid);
            n2 = compar(mid, top);
            if (n1 < 0 && n2 > 0)
                t1 = compar(bot, top) < 0 ? top : bot;
            else if (n1 > 0 && n2 < 0)
                t1 = compar(bot, top) > 0 ? top : bot;
            else
                t1 = mid;
            if (t1 != mid)
            {
                SWAP(t1, mid);
                mid--;
            }
        }

        /*
         * Standard quicksort, Knuth, Vol. 3, page 116, Algorithm Q.
         */

#define didswap n1
#define newbot  t1
#define replace t2

        didswap = 0;
        for (bsv = bot ;;)
        {
            for (; bot < mid && compar(bot, mid) <= 0 ; bot++);
            skipit = 0;
            while (top > mid)
            {
                if (compar(mid, top) <= 0)
                {
                    top--;
                    continue;
                }
                newbot = bot + 1;		/* value of bot after swap */
                if (bot == mid)			/* top <-> mid, mid == top */
                    replace = mid = top;
                else                    /* bot <-> top */
                {
                    replace = top;
                    top--;
                }
                skipit = 1;
                break;
            }
            if (!skipit)
            {
                if (bot == mid)
                    break;

                /*
                 * bot <-> mid, mid == bot
                 */
                replace = mid;
                newbot = mid = bot;		/* value of bot after swap */
                top--;
            }
            SWAP(bot, replace);
            bot = newbot;
            didswap = 1;
        }

        /*
         * Quicksort behaves badly in the presence of data which is already
         * sorted (see Knuth, Vol. 3, page 119) going from O N lg N to O N^2.
         * To avoid this worst case behavior, if a re-partitioning occurs
         * without swapping any elements, it is not further partitioned and
         * is insert sorted.  This wins big with almost sorted data sets and
         * only loses if the data set is very strangely partitioned.  A fix
         * for those data sets would be to return prematurely if the insertion
         * sort routine is forced to make an excessive number of swaps, and
         * continue the partitioning.
         */
        if (!didswap)
            return insertion_sort(arr, bsv, bsv + nmemb - 1, lfunc, obj);
#undef didswap
#undef newbot
#undef replace

#define nlower n1
#define nupper n2

        bot = bsv;
        nlower = mid - bot;			/* size of lower partition */
        mid++;
        nupper = nmemb - nlower - 1;		/* size of upper partition */

        if (nlower > nupper)
        {
            if (nupper >= THRESH)
                arr = quick_sort(arr, mid, mid + nupper - 1, lfunc, obj);
            else
            {
                SORT(mid, nupper);
                if (nlower < THRESH)
                {
                    SORT(bot, nlower);
                    return arr;
                }
            }
            nmemb = nlower;
        }
        else
        {
            if (nlower >= THRESH)
                arr = quick_sort(arr, bot, bot + nlower - 1, lfunc, obj);
            else
            {
                SORT(bot, nlower);
                if (nupper < THRESH)
                {
                    SORT(mid, nupper);
                    return arr;
                }
            }
            bot = mid;
            nmemb = nupper;
#undef nupper
#undef nlower
        }
    }
}

static mixed
insertion_sort(mixed arr, int bot, int orig_top, string lfunc, object obj)
{
    mixed elem;
    int   top, t1, t2, nmemb;

    nmemb = orig_top - bot + 1;

    /*
     * A simple insertion sort (see Knuth, Vol. 3, page 81, Algorithm
     * S).  Insertion sort has the same worst case as most simple sorts
     * (O N^2).  It gets used here because it is (O N) in the case of
     * sorted data.
     */
    top = bot + nmemb;
    for (t1 = bot + 1 ; t1 < top ;)
    {
        for (t2 = t1 ; --t2 >= bot && compar(t1, t2) < 0 ;);
        if (t1 != ++t2)
        {
            SWAP(t1, t2);
        }
        else
            t1++;
    }
    return arr;
}

int
sort_compare(mixed elem1, mixed elem2)
{
    if (intp(elem1) && intp(elem2))
        return elem1 - elem2;
    if (stringp(elem1) && intp(elem2))
        elem2 = "" + elem2;
    if (stringp(elem2) && intp(elem1))
        elem1 = "" + elem1;
    if (elem1 < elem2)
        return -1;
    else if (elem1 > elem2)
        return 1;
    return 0;
}
#endif CFUN

/*
 *
 * These are the LPC backwardscompatible simulated efuns.
 *
 */

/*
 * Nazwa funkcji: getuid
 * Opis         : Returns the uid of an object.
 * Argumenty    : object ob - the object to get the uid for.  If not
 *                            specified, previous_object() is used.
 * Zwraca       : string - the object's uid.
 */
varargs string
getuid(object ob = previous_object())
{
    string uid = explode(query_auth(ob),":")[0];
    return (uid == "0" ? 0 : uid);
}

/*
 * Nazwa funkcji: geteuid
 * Opis         : Returns the euid of an object
 * Argumenty    : object ob - the object to get the euid for.  If not
 *                            specified, previous_object() is used.
 * Zwraca       : string - the object's euid.
 */
varargs mixed
geteuid(object ob = previous_object())
{
    string euid = explode(query_auth(ob),":")[1];
    return (euid == "0" ? 0 : euid);
}

/*
 * Nazwa funkcji: export_uid
 * Opis         : Sets the euid of one object to the uid of previous_object().
 *                This can only be done if the euid of the object is 0.
 * Argumenty    : object ob - the object to set the euid for
 */
void
export_uid(object ob)
{
    string euid, euid2;
    euid = explode(query_auth(ob),":")[1];
    euid2 = explode(query_auth(previous_object()), ":")[1];
    if (euid2 == "0")
        throw("Illegal to export euid 0");
    if (euid == "0")
        set_auth(ob, euid2 + ":#");
}

/*
 * Nazwa funkcji: seteuid
 * Opis         : set the euid of previous_object() to a given userid
 * Argumenty    : string str - the userid
 * Zwraca       : int 1 / 0 - euid could be set / euid could not be set
 */
int
seteuid(string str)
{
    string uid = explode(query_auth(previous_object()),":")[1];

    if (str && str != uid)
        if (!SECURITY->valid_seteuid(previous_object(), str))
            return 0;
    set_auth(previous_object(), "#:" + (str ? str : "0"));
    return 1;
}

/*
 * Nazwa funkcji: setuid
 * Opis         : set the uid of previous_object() to its creator
 */
void
setuid()
{
    set_auth(previous_object(), SECURITY->creator_object(previous_object()) + ":#");
}

/*
 * Nazwa funkcji: interactive
 * Opis         : Find out whether an object is an interactive player or not.
 * Argumenty    : object ob - the object to check, defaults to this_object()
 *                            when omitted.
 * Zwraca       : int - 1/0 - interactive or not.
 */
varargs int
interactive(object ob = previous_object())
{
    return (objectp(ob) && stringp(query_ip_number(ob)));
}

/*
 * Nazwa funkcji: atoi
 * Opis         : This converts a string into a number.
 * Argumenty    : string num - the string to convert.
 * Zwraca       : int - the corresponding number, or 0.
 */
int
atoi(mixed num)
{
    int inum;

    if(intp(num))
        return num;
    else if(!stringp(num))
        return 0;
    else if(sscanf(num, "%d", inum) == 1)
        return inum;
    else
        return 0;
}

/**
 * @param num liczba ca�kowita
 * @return stringa z inta
 */
string
itoa(int num)
{
    return num + "";
}

/**
 * @param str string
 * @return Floata ze stringa
 */
float
atof(string str)
{
    float f;

    if(sscanf(str, "%f", f) == 1)
        return f;
}

/**
 * Funkcja prywatna, do kolorowania w ls
 */
private static string
colorate_it(string file, string path)
{
    int color;
    if(is_link(path + "/" + file))
        color = COLOR_FG_CYAN;
    else if(file_size(path + "/" + file) == -2)
        color = COLOR_FG_MAGENTA;
    else if(file_size(path + "/" + file) >= 0)
    {
        string ext = explode(file, ".")[-1];
        if(ext == "h")
            color = COLOR_FG_YELLOW;
        else if(ext == "c")
            color = COLOR_FG_GREEN;
        else if(ext == "o")
            color = COLOR_FG_BLUE;
    }
    else
    {
        //Tu przyda�o by si� to zamieni�, kiedy ju� strlen bedzie zwraca� d�ugo�� stringa
        //bez uwzgl�dniania koloru.
        //return file;
        color = COLOR_FG_DEFAULT;
    }

    return set_color(color) + file + clear_color();
}

/**
 * Wy�wietla zawarto�� podanego katalogu.
 * @param path �cie�ka do katalogu
 * @param mode spos�b wy�wietlania ls'a
 */
void
ls(string path, string mode)
{
    string	*files, *slices, a, b, *items, *foobar, tme;
    int		i, j, ml, size;

    this_object()->seteuid(geteuid(previous_object()));

    if (!strlen(path))
        path = "/*";
    else if (sscanf(path + "dummy", "%s*%s", a, b) != 2)
    {
        if (strlen(path))
        {
            if (path[strlen(path) - 1] == '/')
                path += "*";
            else
                path += "/*";
        }
        else
            path = "/*";
    }

    if (!sizeof(files = get_dir(path)))
    {
        if(is_link(path))
        {
            this_player()->catch_msg("Katalog '" + path + "' jest dowi�zaniem symbolicznym do "+
                "katalogu: '" + real_path(path) + "', w kt�rym nie ma �adnych plik�w.\n");
        }
        else
            this_player()->catch_msg("Brak plik�w w: '" + path + "'.\n");

        return;
    }

    slices = explode(path + "/", "/");
    path = implode(slices[0..sizeof(slices) - 2], "/");

    if (files[0] == ".")
    {
        if (files[1] == "..")
            files = files[2..sizeof(files) - 1];
        else
            files = files[1..sizeof(files) - 1];
    }
    else if (files[0] == "..")
        files = files[1..sizeof(files) - 1];

    //Usuwamy pliki z ~ na ko�cu rozszerzenia i z samym rozszerzeniem.
    //Chyba �e podano argument a
    if(!wildmatch("*a*", mode))
    {
        foreach(string x : files)
        {
            if(x[-1] == '~' || x[0] == '.')
                files -= ({x});
        }
    }

    //Sortujemy tak, �eby katalogi by�y na pocz�tku a p�niej pliki
    //Nie sortujemy jesli t, bo nizej i tak to inaczej przesortuje..
    //po co robi� to dwa razy?
    if(!wildmatch("*t*", mode))
    {
        string *x, y;
        x = filter(files, &operator(==)(-2,) @ &file_size() @ &operator(+)((path + "/"),));
        files -= x;
        files = x + files;
    }

    if(wildmatch("*n*", mode))
        items = files + ({});
    else
        items = map(files, &colorate_it(, path));

    /*
     * First do information retreival.
     */
    //Jaki� g�upi ten kod.. zamiast prostego wildmatcha zrobi� taka zabawa...:)
    for (j = 0 ; j < strlen(mode) ; j++)
    {
        switch (mode[j])
        {
            case 'l':
                items = ({});
                int k;
                for (i = 0, k=0 ; i < sizeof(files) ; i++)
                {
                    if(!wildmatch("*a*", mode))
                    {
                        if(file_size(path + "/" + files[i]) != -2)
                        {
                            if(files[i][-1] == '~' || files[i][0] == '.')
                                continue;
                        }
                    }
                    if ((size = file_size(path + "/" + files[i])) == -2)
                    {
                        items += ({ set_color(COLOR_FG_MAGENTA) + "d " });
                        size = 512;
                    }
                    else if (find_object(path + "/" + files[i]))
                        items += ({ set_color(COLOR_BOLD_ON) + "* " });
                    else
                        items += ({ "- " });

                    int color = COLOR_FG_DEFAULT;
                    if(!wildmatch("*n*", mode))
                    {
                        string *exp = explode(files[i], ".");
                        if(sizeof(exp))
                        {
                            if(exp[-1] == "c")
                                color = COLOR_FG_GREEN;
                            else if(exp[-1] == "o")
                                color = COLOR_FG_BLUE;
                            else if(exp[-1] == "h")
                                color = COLOR_FG_YELLOW;
                        }
                    }

                    if(is_link(path + "/" + files[i]))
                        items[k] = set_color(COLOR_FG_CYAN) + items[k];
                    else
                        items[k] = set_color(color) + items[k];

                    tme = efun::ctime(file_time(path + "/" + files[i]));
                    tme = tme[4..9] + tme[19..23] + tme[10..15];

                    items[k] += sprintf("%-20s%10d  %s", files[i], size, tme);
                    items[k] += clear_color();

                    k++;
                }
                ml = 1;
                break;

            case 't':
            case 'r':
                /* Do sorting/rearranging later */
            case 'a':
            case 'n':
                //Sprawdzane w inny spos�b
                break;

            case 'F':
                for (i = 0; i < sizeof(items); i++)
                {
                if (file_size(path + "/" + files[i]) == -2)
                    items[i] += "/";
                else if (find_object(path + "/" + files[i]))
                    items[i] += "*";
                }

                ml = 0;
                break;

            default:
                this_player()->catch_tell("Nieznana flaga: \"" + mode[j..j+1] + "\".\n");
        }
    }

    /*
     * Now do sorting.
     */
    for (j = 0 ; j < strlen(mode) ; j++)
    {
        switch (mode[j])
        {
            case 't':
                slices = ({});
                for (i = 0 ; i < sizeof(files) ; i++)
                slices += ({ "" + file_time(path + "/" + files[i]) + ":" + i });
                sort_array(slices, "ls_sort_t", this_object());
                foobar = items + ({});
                items = ({ });
                for (i = 0 ; i < sizeof(files) ; i++)
                {
                items += ({ foobar[atoi(explode(slices[i], ":")[1])] });
                }
                break;

            default:
                break;
        }
    }


    /*
     * Check for reverse ordering.
     */
    for (j = 0 ; j < strlen(mode) ; j++)
    {
        switch (mode[j])
        {
            case 'r':
                slices = items;
                items = ({});
                for (i = sizeof(slices) - 1 ; i >= 0 ; i--)
                items += ({ slices[i] });
                break;

            default:
                break;
        }
    }

    if (ml)
    {
        foreach (string an_item : items)
            this_player()->catch_tell(an_item + "\n");
    }
    else
        this_player()->catch_tell(sprintf("%-*#s\n", 76, implode(items, "\n")));
}

int
ls_sort_t(string item1, string item2)
{
    return atoi(explode(item2, ":")[0]) - atoi(explode(item1, ":")[0]);
}

//#ifdef CFUN
#ifdef 0		/* Zamienic pozniej z poprzednim wierszem... */
public varargs void
tell_room(mixed room, mixed str, mixed oblist = 0,
	  object from_player = this_player()) = "tell_room";
#else
/*
 * Nazwa funkcji : tell_room
 * Opis          : Wyswietla wiadomosc istotom w danej lokacji.
 * Argumenty     : room: Lokacja.
 *                 str: Tekst do wyswietlenia, przekazywany adresatom za
 *                      posrednictwem funkcji catch_msg(). Moze zawierac VBFC.
 *                 oblist: Jej elementom tekst nie bedzie przekazany.
 *                 object from_player: ?
 *                 int changeTP - je�li 1 - to b�dzie zmienia� TP
 *                 dla ka�dego playera (przydatne np. do kolor�w)
 */
public varargs void
tell_room(mixed room, mixed str, mixed oblist = 0, object from_player = this_player(), int changeTP = 0)
{
    mixed *plist;

    if (!room)
        return;

    if (stringp(room) && !(room = find_object(room)))
        return;

    if (objectp(oblist))
        oblist = ({ oblist });

//        FILTER_LIVE(all_inventory(room) + ({room}) - oblist)->catch_msg(str);
    plist = all_inventory(room) + ({ room });

    foreach (mixed item : plist)
        if (living(item) && (member_array(item, oblist) < 0))
            item->catch_msg(str, from_player, changeTP);

}
//#endif CFUN
#endif 0		/* Zamienic pozniej z poprzednim wierszem... */

/*
 * Nazwa funkcji : tell_roombb
 * Opis          : Wyswietla wiadomosc istotom, ktore widza w danej lokacji,
 *                 a takze, opcjonalnie, widza podana osobe.
 * Argumenty     : room: Lokacja.
 *                 str: Tekst do wyswietlenia, przekazywany adresatom za
 *                      posrednictwem funkcji catch_msg(). Moze zawierac VBFC.
 *                 oblist: Jej elementom tekst nie bedzie przekazany.
 *                 seen: Opcjonalnie, obiekt/y ktore musza widziec widzowie.
 */
public varargs void
tell_roombb(mixed room, mixed str, mixed oblist = 0, mixed seen = 0, int changeTP = 0)
{
    if (stringp(room))
        room = find_object(room);

    if (objectp(oblist))
        oblist = ({ oblist });
    else if (!pointerp(oblist))
        oblist = ({ });

    if (room)
    {
        object *plist = all_inventory(room) + ({room});

        /* can_see_in_room jest prawdziwe tylko dla livingow */
        // Niewielkie zmiany na szybko.. Da si� ustawi� kilka obiekt�w kt�re trzeba widzie�.
        if (seen)
        {
            if(!pointerp(seen))
                if(objectp(seen))
                    seen = ({seen});
                else
                    seen = 0;
        }

        if(seen)
        {
            object *obs = FILTER_CAN_SEE_IN_ROOM(plist - (object *)oblist);
            foreach(object ob : (object *)seen)
                obs = FILTER_IS_SEEN(ob, obs);
            obs->catch_msg(str, 0, changeTP);
        }
 //           FILTER_IS_SEEN(seen, FILTER_CAN_SEE_IN_ROOM(plist
 //           - (object *)oblist))->catch_msg(str);
        else
            FILTER_CAN_SEE_IN_ROOM(plist - (object *)oblist)->catch_msg(str, 0, changeTP);
     }
}

//#ifndef CFUN
#ifndef 0		/* Zamienic pozniej z poprzednim wierszem... */
/*
 * Nazwa funkcji : say
 * Opis          : Wyswietla wiadomosc istotom w tej samej lokacji co
 *                 this_player().
 * Argumenty     : str: Tekst do wyswietlenia, przekazywany adresatom za
 *                      posrednictwem funkcji catch_msg(). Moze zawierac VBFC.
 *                 oblist: Jej elementom tekst nie bedzie przekazany -
 *                         standardowo this_playerowi().
 */
public void
say(mixed str, mixed oblist = ({this_player()}))
{
    object tp = this_player();
    object env = environment(tp);

    if (env)
        tell_room(env, str, oblist);

    tell_room(tp, str, oblist);
}
#else
public varargs void
say(mixed str, mixed oblist) = "say";
//#endif CFUN
#endif 0		/* Zamienic pozniej z poprzednim wierszem... */

/*
 * Nazwa funkcji : saybb
 * Opis          : Wyswietla wiadomosc istotom, ktore widza this_playera().
 * Argumenty     : str: Tekst do wyswietlenia, przekazywany adresatom za
 *                      posrednictwem funkcji catch_msg(). Moze zawierac VBFC.
 *                 oblist: Jej elementom tekst nie bedzie przekazany -
 *                         standardowo this_playerowi().
 *                 seen: Obiekty kt�re musz� by� widziane, domy�lnie TP
 */
public void
saybb(mixed str, mixed oblist = ({TP}), mixed seen = ({TP}))
{
    object tp = this_player();
    object env = environment(tp);

    if (objectp(oblist))
        oblist = ({ oblist });
    else if (!pointerp(oblist))
        oblist = ({ tp });

    if(!objectp(seen) || !pointerp(seen))
        seen = TP;

    if (env)
        tell_roombb(env, str, oblist, seen);

    tell_roombb(tp, str, oblist, seen);
}

/*
 * Nazwa funkcji: log_file
 * Opis         : Logs a message in the creators ~/log/ subdir in a given
 *		  file.
 *
 * 		  The optional argument 'csize' controls the cycling size
 * 		  of the log. If the argument is == 0 the default cycle
 * 		  size is used, if the argument is == -1 the maximum cycle
 * 		  size is used, if a positive integer is given, that cycle
 * 		  size is used provided that it is less or equal to the
 * 		  maximum cycle size.
 *
 * Argumenty    : file: The filename.
 *		  text: The text to add to the file
 * 		  csize: The cycle size, if any (optional)
 */
public varargs void
log_file(string file, string text, int csize)
{
    mixed           path,
                    oldeuid,
                    cr;
    string 	    *split, fnam;
    int 	    il, fsize, msize, dsize;

    cr = SECURITY->creator_object(previous_object());

    /* Let backbone objects log things in /log */
    if (cr == SECURITY->get_bb_uid())
    {
        path = "/log";
        cr = SECURITY->get_root_uid();
    }
    else
        path = SECURITY->query_wiz_path(cr) + "/log";

    /* We swap to the userid of the user trying to do log_file */
    oldeuid = geteuid(this_object());
    this_object()->seteuid(cr);

    if (file_size(path) != -2)
    {
	/* We must create the path
        */
	split = explode(path + "/", "/");

	for (fnam = "", il = 0; il < sizeof(split); il++)
	{
	    fnam += "/" + split[il];
	    if (file_size(fnam) == -1)
		mkdir(fnam);
	    else if (file_size(fnam) > 0)
	    {
		this_object()->seteuid(oldeuid);
		return;
	    }
	}
    }

    file = path + "/" + file;

#ifdef CYCLIC_LOG_SIZE

    fsize = file_size(file);
    msize = CYCLIC_LOG_SIZE[cr];
    dsize = CYCLIC_LOG_SIZE[0];

    if (csize > 0)
	msize = ((csize > msize && msize > 0)
	    ? (msize ?: csize)
	    : csize);

    if (csize == 0)
	msize = (msize ?: dsize);

    if (csize < 0)
	msize = (msize == 0 ? dsize : msize);

    if (msize > 0 && fsize > msize)
	rename(file, file + ".old");

#endif /* CYCLIC_LOG_SIZE */

    write_file(file, ctime(time()) + "\n" + text);
    this_object()->seteuid(oldeuid);
}

/************************************************************************
 *
 * PSEUDO EFUNS
 *
 * Functions below are useful but should probably not be made into efuns.
 * They are here because they are very practical and often used.
 */

/*
 * Nazwa funkcji: creator
 * Opis         : Get the name of the creator of an object or file
 *                (see creator_object() and creator_file())
 * Argumenty    : mixed ob - The object or filename for which to
 *                           get the creator
 * Zwraca       : string - The creator name
 */
public string
creator(mixed ob)
{
    if (objectp(ob))
        return (string) SECURITY->creator_object(ob);
    else
        return (string) SECURITY->creator_file(ob);
}

/*
 * Nazwa funkcji: domain
 * Opis         : Get the name of an object's domain (see domain_object())
 * Argumenty    : object ob - the object for which to get the domain
 * Zwraca       : string - the domain name
 */
public string
domain(object ob)
{
    return (string) SECURITY->domain_object(ob);
}

/*
 * Nazwa funkcji: strchar
 * Opis         : Convert an integer from 0 to 255 to its corresponding
 *                letter.
 * Argumenty    : int x - the integer to convert
 * Zwraca       : string - the corresponding letter.
 */
public string
strchar(int x)
{
    if (x >=0 && x < 256)
	return
	    ("?        \t\n\r  " +
	     "                " +
	     " !\"#$%&'()*+,-./0123456789:;<=>?" +
	     "@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^^_" +
	     "`abcdefghijklmnopqrstuvwxyz{|}~^?")[x..x];
    else
	return " ";
}

/*
 * Nazwa funkcji: type_name
 * Opis         : This function will return the type of a variable in string
 *                form.
 * Argumenty    : mixed etwas - the variable to type.
 * Zwraca       : string - the name of the type.
 */
string
type_name(mixed etwas)
{
    if (intp(etwas))
        return "int";
    else if (floatp(etwas))
        return "float";
    else if (stringp(etwas))
        return "string";
    else if (objectp(etwas))
        return "object";
    else if (pointerp(etwas))
        return "array";
    else if (mappingp(etwas))
        return "mapping";
#ifdef _FUNCTION
    else if (functionp(etwas))
        return "function";
#endif _FUNCTION
    return "!UNKNOWN!";
}

/*
 * Nazwa funkcji: dump_array
 * Opis         : Dumps a variable with write() for debugging purposes.
 * Argumenty    : a: Anything including an array
 */
nomask varargs void
dump_array(mixed a, string tab)
{
    int             n,
                    m;
    mixed 	    ix, val;

    if (!tab)
        tab = "";
    if (!pointerp(a) && !mappingp(a))
    {
        dump_elem(a, tab);
        return;
    }
    else if (pointerp(a))
    {
        this_player()->catch_tell("(Array)\n");
        m = sizeof(a);
        n = 0;
        while (n < m)
        {
            this_player()->catch_tell(tab + "[" + n + "] = ");
            dump_elem(a[n], tab);
            n += 1;
        }
    }
    else /* Mappingp */
        dump_mapping(a, tab);
}

/*
 * Nazwa funkcji: dump_mapping
 * Opis         : Dumps a variable with write() for debugging purposes.
 * Argumenty    : a: Anything including an array
 */
nomask varargs void
dump_mapping(mapping m, string tab)
{
    mixed *d;
    int i, s;
    string dval, val;

    if (!tab)
        tab = "";

    d = m_indexes(m);
    s = sizeof(d);
    this_player()->catch_tell("(Mapping) ([\n");
    for(i = 0; i < s; i++)
    {
        if (intp(d[i]))
            dval = "(int)" + d[i];

        if (floatp(d[i]))
            dval = "(float)" + ftoa(d[i]);

        if (stringp(d[i]))
            dval = "\"" + d[i] + "\"";

        if (objectp(d[i]))
            dval = file_name(d[i]);

        if (pointerp(d[i]))
            dval = "(array:" + sizeof(d[i]) + ")";

        if (mappingp(d[i]))
            dval = "(mapping:" + m_sizeof(d[i]) + ")";
#ifdef _FUNCTION
        if (functionp(d[i]))
            dval = sprintf("%O", d[i]);

        if (functionp(m[d[i]]))
            val = sprintf("%O", m[d[i]]);
#endif _FUNCTION

        if (intp(m[d[i]]))
            val = "(int)" + m[d[i]];

        if (floatp(m[d[i]]))
            val = "(float)" + ftoa(m[d[i]]);

        if (stringp(m[d[i]]))
            val = "\"" + m[d[i]] + "\"";

        if (objectp(m[d[i]]))
            val = file_name(m[d[i]]);

        if (pointerp(m[d[i]]))
            val = "(array:" + sizeof(m[d[i]]) + ")";

        if (mappingp(m[d[i]]))
            val = "(mapping:" + m_sizeof(m[d[i]]) + ")";

        this_player()->catch_tell(tab + dval + ":" + val + "\n");

        if (pointerp(d[i]))
            dump_array(d[i]);

        if (pointerp(m[d[i]]))
            dump_array(m[d[i]]);

        if (mappingp(d[i]))
            dump_mapping(d[i], tab + "   ");

        if (mappingp(m[d[i]]))
            dump_mapping(m[d[i]], tab + "   ");
    }

    this_player()->catch_tell("])\n");
}

/*
 * Nazwa funkcji: inherit_list
 * Opis         : Returns the inherit list of an object.
 * Argumenty    : ob - the object to list.
 */
nomask string *
inherit_list(object ob)
{
    return SECURITY->do_debug("inherit_list", ob);
}

static nomask void
dump_elem(mixed sak, string tab)
{
    if (pointerp(sak))
        dump_array(sak, tab + "   ");
    else if (mappingp(sak))
        dump_mapping(sak, tab + "   ");
    else
    {
        this_player()->catch_tell("(" + type_name(sak) + ") ");

        if (objectp(sak))
            this_player()->catch_tell(file_name(sak));
        else if (floatp(sak))
            this_player()->catch_tell(ftoa(sak));
        else
            this_player()->catch_tell(sprintf("%O",sak));
    }

    this_player()->catch_tell("\n");
}

/*
 * Function:    secure_var
 * Opis         : Return a secure copy of the given variable
 * Argumenty    :   var - the variable
 * Zwraca       :     the secured copy
 */
mixed
secure_var(mixed var)
{
    if (pointerp(var) || mappingp(var))
        return map(var, secure_var);

    return var;
}

#ifdef _FUNCTION
/*
 * Nazwa funkcji: composite_util
 * Opis         : Returns the composition of function f applied to the result
 *                of function g applied on variable x.
 * Argumenty    : function f - the outer function in the composition.
 *                function g - the inner function in the composition.
 *                mixed x    - the argument to perform the functions on.
 * Zwraca       : mixed - the result of the composition.
 */
static mixed
compose_util(function f, function g, mixed x)
{
    return f(g(x));
}

/*
 * Nazwa funkcji: compose
 * Opis         : The returns the composition of two functions
 *                (in the mathematical sense).  So if
 *                h = compose(f,g)  then  h(x) == f(g(x))
 * Argumenty    : f, g: the functions to compose.
 * Zwraca       : a function which is the composition of f and g.
 */
function
compose(function f, function g)
{
    return &compose_util(f,g);
}

/*
 * Nazwa funkcji: apply_array
 * Opis         : apply a function to an array of arguments
 * Argumenty    : f: the function, v: the array
 * Zwraca       : f(v1,...vn) if v=({v1,...vn})
 */
mixed
applyv(function f, mixed *v)
{
    function g = papplyv(f, v);
    return g();
}

/*
 * Nazwa funkcji: for_each
 * Opis         : For each of the elements in the array 'elements', for_each
 *                calls func with it as as parameter.
 *                This is the same functionality as the efun map, but without
 *                the return value.
 * Argumenty    : mixed elements      (the array/mapping of elements to use)
 *                function func       (the function to recieve the elements)
 */
void
for_each(mixed elements, function func)
{
    int i;
    int sz;
    mixed arr;

    arr = elements;
    if (mappingp(elements))
        arr = m_values(elements);
    sz = sizeof(arr);
    for(i = 0; i < sz; i++)
        func(arr[i]);
}

/*
 * Nazwa funkcji: constant
 * Opis         : returns the first argument and ignores the second
 * Argumenty    : x, y
 * Zwraca       : x
 */
mixed
constant(mixed x, mixed y)
{
    return x;
}

/*
 * Nazwa funkcji: identity
 * Opis         : returns its argument unmodified
 * Argumenty    : x
 * Zwraca       : x
 */
mixed
identity(mixed x)
{
    return x;
}


/*
 * Nazwa funkcji: not
 * Opis         : returns the locigal inverse of the argument
 * Argumenty    : x
 * Zwraca       : !x
 */
int
not(mixed x)
{
    return !x;
}

/*
 * Nazwa funkcji: mkcompare_util
 * Opis         : Compare two items and return a value meaningful for
 *                sort_array() (that is, 1, 0, or -1).
 * Argumenty    : function f            - If specified, the comparison values
 *                                        will be derived by calling this
 *                                        function rather than simply comparing
 *                                        items directly.
 *                function compare_func - A function that determines how the
 *                                        items will be compared.  This is
 *                                        typically &operator(<)().
 *                mixed x               - The first item to compare.
 *                mixed y               - The second item to compare.
 * Zwraca       : 1, 0, or -1, as determined by compare_func
 */
int
mkcompare_util(function f, function compare_fun, mixed x, mixed y)
{
    if (f)
    {
    	x = f(x);
    	y = f(y);
    }

    if (compare_fun(x, y))
    {
        return -1;
    }

    if (compare_fun(y, x))
    {
        return 1;
    }

    return 0;
}

/*
 * Nazwa funkcji: mkcompare
 * Opis         : Takes a normal comparison function, like < and returns a
 *                pointer to the mkcompare_util() function, which is suitable
 *                for use with sort_array().
 *                See mkcompare_util() above.
 * Argumenty    : function f - an optional function pointer argument.  If
 *                             specified, the comparison will be done on the
 *                             values derived from this function.  If not
 *                             specified, the objects to be sorted will be
 *                             compared directly.
 *                function compare_fun - This function determines how the
 *                                       sorted items are to be compared.  It
 *                                       defaults to &operator(<)().
 * Zwraca       : A function pointer useful with sort_array().
 */
varargs function
mkcompare(function f, function compare_fun = &operator(<)())
{
    return &mkcompare_util(f, compare_fun);
}

/**
 * Funkcja ilosc zwraca s1, s2 lub s3 w zaleznosci od wartosci num
 * s1 - wartosc, gdy num = 1,
 * s2 - wartosc, gdy num = 2,3,4,42... itp.
 * s3 - wartosc, gdy num = 5,10,14... itp.
 * Przyklad:
 *  ilosc(122, "sztuka", "sztuki", "sztuk") = "sztuki"
 */
public mixed
ilosc(int num, mixed s1, mixed s2, mixed s3)
{
    return (num == 1 ? s1 : (num % 10 <= 4 && num % 10 >= 2 && num % 100 != 1 && (num % 100) / 10 != 1 ? s2 : s3));
}

public string
oblicz_koncowki(string str, mixed obj)
{
    int which = 0;
    int size;
    int i, j, k;
    int in;
    string ret;
    mixed tmp;


    if (objectp(obj))
        which = obj->query_gender() + 1;
    else if (pointerp(obj) && sizeof(obj) == 1)
        which = obj[0]->query_gender() + 1;
    else if (pointerp(obj) && sizeof(obj) > 1)
    {
        tmp = filter(obj, &operator(==)(,0) @ &->query_gender()); // 0 -> G_MALE

        if (sizeof(tmp) == 0)
            which = 5;
        else
            which = 4;
    }
    else
        return "";

    size = strlen(str);
    ret = "";
    in = 0;
    i = 0;
    for (j = 0; j < size; j++)
    {
        if (str[j] == '{' && in == 0)
        {
            if (j > 0) ret += str[i..j-1];
            in = 1;
            k = 1;
            i = j + 1;
        }

        if (str[j] == '/' && in == 1)
        {
            if (which == k) ret += str[i..j-1];
            k++;
            i = j + 1;
        }

        if (str[j] == '}' && in == 1)
        {
            if (which == k) ret += str[i..j-1];
            in = 0;
            i = j + 1;
        }
    }

    if (j > 0)
        ret += str[i..j-1];

    return ret;
}

public string
z_ze(string str)
{
    int ix;

    switch(lower_case(str[0]))
    {
        case 's':
        case 'S':
            if (str[1] == 'z')
            ix = 2;
            else
            ix = 1;
            break;
        case 'z':
        case 'Z':
            ix = 1;
            break;
        default: return "z " + str;
    }

    if (member_array(str[ix], ({ 'a', 'e', 'i', 'o', 'u', 'y' }) ) == -1)
        return "ze " + str;
    else
        return "z " + str;
}

varargs mixed
fold(mixed arr, function fun, mixed acc)
{
    acc = secure_var(acc);
    foreach (mixed x : arr) acc = fun(x, acc);
    return acc;
}

/**
 * Funkcja przydatna do u�ywania w filtrach, mapach i foldach:)
 */
public string ob_name(object ob)
{
    return OB_NAME(ob);
}

// to jest wersja z kolejka jako tablica
/*
mixed
znajdz_sciezke(mixed start, mixed end)
{
    object przetwarzany;
    object *kolejka;
    mixed poprzednik = ([ ]);
    mixed odleglosc = ([ ]);
    mixed odwiedzone = ([ ]);
    string *wyjscia;
    mixed sciezka = ({ });

    if (stringp(start))
    {
	LOAD_ERR(start);
	start = find_object(start);
    }

    if (stringp(end))
    {
	LOAD_ERR(end);
	end = find_object(end);
    }

    if (!start || !end)
	return 0;

    odleglosc[start] = 0;
    poprzednik[start] = 0;
    kolejka = ({ start });
    odwiedzone[start] = 1;

    while (sizeof(kolejka))
    {
	przetwarzany = kolejka[0];
	wyjscia = przetwarzany->query_exit_rooms();
	wyjscia += (mixed *)(filter(all_inventory(przetwarzany), &->is_door())->query_other_room()) - ({ 0 });
	foreach (string x : wyjscia)
	{
	    object room;

	    if (!x)
		continue;

	    LOAD_ERR(x);
	    room = find_object(x);

	    if (!room)
		continue;

	    if (!odwiedzone[room])
	    {
		odleglosc[room] = odleglosc[przetwarzany] + 1;
		poprzednik[room] = przetwarzany;
		kolejka += ({ room });
		odwiedzone[room] = 1;
	    }
	}
	kolejka = kolejka[1..];
    }

    przetwarzany = end;
    while (przetwarzany != start)
    {
	object poprz;

	if (!przetwarzany)
	    return 0;

	poprz = poprzednik[przetwarzany];
	sciezka = ({ poprz->exit_room_to_cmd(file_name(przetwarzany)) }) + sciezka;
	przetwarzany = poprz;
    }

    return sciezka;
}
*/

mixed
znajdz_sciezke(mixed start, mixed end)
{
    object przetwarzany;
    object kolejka;
    mixed poprzednik = ([ ]);
    mixed odleglosc = ([ ]);
    mixed odwiedzone = ([ ]);
    string *wyjscia;
    mixed sciezka = ({ });

    if (stringp(start))
    {
        LOAD_ERR(start);
        start = find_object(start);
    }

    if (stringp(end))
    {
        LOAD_ERR(end);
        end = find_object(end);
    }

    if (!start || !end)
        return 0;

    kolejka = clone_object(QUEUE_OBJECT);

    odleglosc[start] = 0;
    poprzednik[start] = 0;
    kolejka->push_back(start);
    odwiedzone[start] = 1;

    while (kolejka->size())
    {
        przetwarzany = kolejka->pop_front();
        wyjscia = przetwarzany->query_exit_rooms();
        wyjscia += (mixed *)(filter(all_inventory(przetwarzany), &->is_door())->query_other_room()) - ({ 0 });
        wyjscia += (mixed *)(filter(all_inventory(przetwarzany), &->is_gate())->query_other_room()) - ({ 0 });

        foreach (string x : wyjscia)
        {
            object room;

            if (!x)
                continue;

            LOAD_ERR(x);
            room = find_object(x);

            if (!room)
                continue;

            if (!odwiedzone[room])
            {
                odleglosc[room] = odleglosc[przetwarzany] + 1;
                poprzednik[room] = przetwarzany;
                kolejka->push_back(room);
                odwiedzone[room] = 1;
            }
        }
    }

    kolejka->remove_object();

    przetwarzany = end;

    while (przetwarzany != start)
    {
        object poprz;

        if (!przetwarzany)
            return 0;

        poprz = poprzednik[przetwarzany];
        sciezka = ({ poprz->exit_room_to_cmd(file_name(przetwarzany)) }) + sciezka;
        przetwarzany = poprz;
    }

    return sciezka;
}

mixed
reverse_array(mixed arr)
{
    mixed ret;
    int i, size;

    if (!pointerp(arr))
        return arr;

    ret = allocate(size = sizeof(arr));

    for (i = 0; i < size; ++i)
        ret[size - i - 1] = arr[i];

    return ret;
}

mixed
topological_sort_mapping(mixed vertices, mapping edges)
{
    mixed ret = ({ });
    mixed tmp = ({ });

    if (!pointerp(vertices))
        return ({ });

    vertices = secure_var(vertices);

    if (!mappingp(edges))
        return vertices;

    edges = secure_var(edges);

    while (sizeof(vertices))
    {
        foreach (mixed i : m_indexes(edges))
        {
            edges[i] -= tmp;
            if (!sizeof(edges[i]))
            m_delkey(edges, i);
        }

        tmp = (mixed *)vertices - m_indexes(edges);

        if (!sizeof(tmp))
            throw("Topological sort on non-DAG.\n");

        ret += tmp;
        vertices -= tmp;
    }

    return reverse_array(ret);
}

int
equal_array(mixed a1, mixed a2)
{
    int i;

    if (a1 == a2)
        return 1;

    if (!pointerp(a1) || !pointerp(a2))
        return 0;

    if (sizeof(a1) != sizeof(a2))
        return 0;

    for (i = 0; i < sizeof(a1); ++i)
        if (!equal_array(a1[i], a2[i]))
            return 0;

    return 1;
}

mixed *
a_delete(mixed *array, int i) {
    if (i == 0) {
        array = array[1..-1];
    } else {
        array = array[0..i-1] + array[i+1..-1];
    }
    return array;
}


/**
 * !!UWAGA!!
 * Funkcja zosta�a przeniesiona do DRIVERA, kt�ry od nowej wersji wspiera kolorki niemal�e w pe�ni.
 * Przez to nie jest konieczne, wymagane, ani nawet po��dane podawanie playera, dla kt�rego ma
 * zosta� pokazany kolor, nale�y po prostu poda� kolor.
 * Kolor t�a nale�y ustawi� osobn� funkcj�, poniewa� b�d� d��y� do usuni�cia tej za�lepki
 *
 * KRUN
 *
 * Funkcja zmienia kolor wy�wietlania dla gracza.
 *
 * @param who   - osoba kt�rej ma zosta� zmieniony kolor(opcjonalny, niepo��dany)
 * @param color - kolor
 * @param tlo   - je�li nie podamy pozostanie to kt�re jest(opcjonalny, niepo��dany)
 * @param ... W pozosta�ych argumentach mo�emy poda� format tekstu(podkre�lony, migaj�cy, pogrubiony, itp., niepo��dane).
 *
 * @return string zmieniajacy kolorek lub pusty je�li gracz ma wy��czon� opcje
 */
// varargs string
// set_color(mixed who, int color, int tlo = 0, ...)
// {
//     if(!objectp(who))
//     {
//         if(intp(who))
//         {
//             argv = ({tlo}) + argv;
//             tlo = color;
//             color = who;
//         }
//     }
//     
//     foreach (int arg : argv)
//     {
//         switch (arg)
//         {
//             case COLOR_BOLD_ON: efun::set_color(COLOR_BOLD_ON); break;
//             case COLOR_BOLD_OFF: efun::set_color(COLOR_BOLD_OFF); break;
//             case COLOR_ITALICS_ON: efun::set_color(COLOR_ITALICS_ON; break;
//             case COLOR_ITALICS_OFF: efun::set_color(COLOR_ITALICS_OFF; break;
//             case COLOR_UNDERLINE_ON: efun::set_color(COLOR_UNDERLINE_ON; break;
//             case COLOR_UNDERLINE_OFF: efun::set_color(COLOR_UNDERLINE_OFF; break;
//             case COLOR_INVERSE_ON: efun::set_color(COLOR_INVERSE_ON; break;
//             case COLOR_INVERSE_OFF: efun::set_color(COLOR_INVERSE_OFF; break;
//             case COLOR_STRIKETHROUGH_ON: strikethrough = COLOR_STRIKETHROUGH_ON; break;
//             case COLOR_STRIKETHROUGH_OFF: strikethrough = COLOR_STRIKETHROUGH_OFF; break;
//             case COLOR_DARK: dark = COLOR_DARK; break;
//             case COLOR_BLINK: blink = COLOR_BLINK; break;
//         }
//     }
// 
//     if (bold)
//         toReturn += "[" + bold + "m";
// 
//     if (italics)
//         toReturn += "[" + italics + "m";
// 
//     if (underline)
//         toReturn += "[" + underline + "m";
// 
//     if (inverse)
//         toReturn += "[" + inverse + "m";
// 
//     if (strikethrough)
//         toReturn += "[" + strikethrough + "m";
// 
//     if (dark)
//         toReturn += "[" + dark + "m";
// 
//     if (blink)
//         toReturn += "[" + blink + "m";
// 
//     if(tlo)
//         toReturn += "[" + tlo + "m";
// 
//     return toReturn + "[" + color + "m";
// }

/**
 * Funkcja czy�ci kolor wy�wietlania dla gracza.
 */
// varargs string
// clear_color(object who = TP)
// {
//     if(!who || !interactive(who))
//         return "";
// 
//     if (!who->query_option(OPT_COLORS))
//         return "";
// 
//     return "[0m";
// }

/**
 * Funkcja wycina z tekstu kolory
 */
// string
// clear_color_format(string str)
// {
//     string *exp = explode(str + "[", "[");
// 
//     if(sizeof(exp) == 1)
//         return str;
// 
//     string ret = "";
//     foreach(string x : exp)
//         ret += x[(strchr(x, "m")+1)..];
// 
//     return ret;
// }

/**
 * Elementy z tablicy wielopoziomowej przepisuje do tablicy jednopoziomowej.
 *
 * @param x Tablica wielopoziomowa.
 *
 * @return Jednopoziomow� tablice z wszystkimi elementami tablic kt�re zawiera�a tablica podana jako argument.
 */
mixed
flatten(mixed x)
{
    mixed ret = ({});

    foreach(mixed y: x)
    {
        if(pointerp(y))
            ret += flatten(y);
        else
            ret += ({y});
    }

    return ret;
}

/**
 * Funkcja pozwala nam sprawdzi� czy zmienna i jest indexem mappingu m
 * @param i zmienna kt�ra ma by� indexem mappingu
 * @param m mapping
 * @return 0/1
 */
int
is_mapping_index(mixed i, mapping m)
{
    if(!mappingp(m))
        return 0;

    if(member_array(i, m_indexes(m)) >= 0)
        return 1;
}

/**
 * Funkcja pozwala nam sprawdzi� czy zmienna znajduje si� w warto�ciach mappingu m
 * @param i zmienna kt�ra ma by� indexem mappingu
 * @param m mapping
 * @return 0/1
 */
int
is_mapping_value(mixed i, mapping m)
{
    if(!mappingp(m))
        return 0;

    if(member_array(i, m_values(m)) >= 0)
        return 1;
}

/**
 * Dzi�ki tej funkcji mo�emy u�ywa� tag�w xml w naszym kodzie.
 * Wszystkie dost�pne tagi znale�� mo�ecie pod komend�
 * 'xmltags'.
 * Przed wywo�aniem tej funkcji, aby unikn�� runtime'a wywo�a�
 * nale�y setid(); seteuid(getuid());
 * @param str tekst do przetworzenia
 * @param vp  obiekt dla kt�rego tekst ma zosta� przetworzony
 *            lub 1 je�li przy przetwarzaniu ma zosta� u�yte
 *            VBFC.
 * @return Przetworzony tekst.
 */
string
process_tags(string str, mixed vp=0)
{
    return TAGI->process_tags(str, vp);
}

/**
 * Funkcja t�umaczy kod xml do ansi.
 * Dozwolone tagi html: b, i, u, strikethrough, inverse, dark, #color, #background
 * Tagi oznaczone # do dzia�ania potrzebuj� argumentu - oto dopuszczalne
 * argumenty: black, red, green, yellow, blue, magenta, cyan, white, default
 * Przed wywo�aniem tej funkcji, aby unikn�� runtime'a wywo�a�
 * nale�y setid(); seteuid(getuid());
 * @param xml kod xml
 * @param vp je�li podamy 1 zostanie u�yte vbfc, je�li podamy obiekt gracza, to
 *           ansi zostanie ustawione dla gracza.
 * @return Zwraca przerobiony na ansi kod xml
 */
string
xml_to_ansi(string xml, mixed vp=0)
{
    return TAGI->xml_to_ansi(xml, vp);
}

/**
 * Funkcja sprawdza czy tablice s� identyczne.
 * @param arr1 pierwsza tablica
 * @param arr2 druga tablica
 * @param flag 1 je�li tablice maj� mie� argumenty w tej samej kolejno�ci.
 * return 1/0 identyczne/r�ne
 */
int
compare_arr(mixed arr1, mixed arr2, int flag = 0)
{
    arr1 = secure_var(arr1);
    arr2 = secure_var(arr2);

    if((!pointerp(arr1) && !pointerp(arr2)) || !flag)
        return (arr1 == arr2);

    if(!pointerp(arr1) || !pointerp(arr2))
        return 0;

    if(sizeof(arr1) != sizeof(arr2))
        return 0;

    if(!flag)
    {
        arr1 = sort_array(arr1);
        arr2 = sort_array(arr2);
    }

    int i;
    for(i=0;i<sizeof(arr1);i++)
    {
       if(pointerp(arr1[i]) && pointerp(arr2[i]))
       {
           if(!compare_arr(arr1[i], arr2[i]))
           {
               return 0;
           }
       }
       else if(arr1[i] != arr2[i])
            return 0;
    }

    return 1;
}

/**
 *Jest to funkcja pomocnicza, wysy�aj�ca info wizardom przebywaj�cym
 *aktualnie na mudzie - dzi�ki temu w razie awarii - czarodzieje s�
 *szybko poinformowani.
 *Uwaga: U�ywa� TYLKO w przypadkach KRYTYCZNYCH!!!
 *Argumenty: object kto - w kim, lub czym wyst�pi� b��d
 *           string co - co to jest za b��d
 *Vera
 * Przeniesienie do simul_efun - Krun
 *
 */
int
notify_wizards(object kto, string co)
{
    object *us = filter(users(), &->query_wiz_level());

    foreach(mixed u : us)
    {
        if (query_ip_number(u) &&
            !(u->query_prop(WIZARD_I_BUSY_LEVEL) & BUSY_C))
        {
            tell_object(u,"\nB��D KRYTYCZNY!\nPow�d: "+
                kto->query_name(PL_MIA)+" ("+RPATH(file_name(kto))+"). " +
                (strlen(co) ? "\nTre��: " + co : "") + "\n\n");
        }

    }
    return 1;
}

/* Funkcja pomocnicza przy debuggowaniu.
 * Zamiast define DBG(str) mo�emy u�ywa� tego
 * i mie� pewno��, �e TP dostanie j� tylko,
 * je�li jest wizem
 */
void
wmsg(string msg)
{
    if (TP->query_wiz_level())
        TP->catch_msg(msg);
}


/**
 * Funkcja sprawdza czy plik o podanej �cie�ce istnieje.
 * @param file_name nazwa pliku
 * @return 0/1 nie istnieje/istnieje
 */
int
file_exists(string file_name)
{
    return (file_size(file_name) > -1);
}

/**
 * Dodaje rozsze�enie do nazwy pliku, je�li jeszcze go nie ma.
 * @param file_name nazwa pliku
 * @param ext rozsze�enie (domy�lnie <i>.c</i>)
 * @return nazwe pliku z rozsze�eniem.
 */
string
add_extension(string file_name, string ext=".c")
{
    if(!file_name)
        return "";

    if(ext[0..0] != ".")
        ext = "." + ext;

    int ex_size = strlen(ext);

    if(file_name[-1..-1] == ".")
        return file_name + ext[1..-1];
    if(file_name[-ex_size..-1] != ext)
        return file_name + ext;

    return file_name;
}

/**
 * Usuwa rozsze�enie z nazwy pliku.
 * @param file_name Nazwa pliku, z kt�rej b�dziemy usuwa� rozszerzenie.
 * @param ext Rozsze�enie kt�re ma zosta� usuni�te<i>(opcjonalny)</i>
 * @return Nazwe pliku z obci�tym rozszerzeniem.
 */
string
remove_extension(string file_name, string ext = "")
{
    if(!file_name)
        return "";

    if(ext != "")
    {
        if(ext[0..0] != ".")
           ext = "." + ext;

        return explode(file_name, ext)[0];
    }
    return implode(explode(file_name, ".")[0..-2], ".");
}

/**
 * Funkcja zwraca randomow� liczb� w postaci floata.
 * @param max maksymalna liczba jaka mo�e zosta� wylosowana
 * @param pp ilo�� miejsc po przecinku(defaultowo: 2)
 * @return wylosowanego floata
 */
float
frandom(mixed max, int pp=2)
{
    if(intp(max))
        max = itof(max);

    float real_max = max;
    int pw = ftoi(pow(10.0, itof(pp)));

    max = ftoi(max + 0.99);

    max = max * pw;

    int rand = random(max+1);
    string str = itoa(rand);

    float ret = atof(rand + ".0e-" + pp);

    if(ret > real_max)
        return real_max;

    return ret;
}

/**
 * Czy�ci tablic� z jednakowych element�w.
 * @param arr tablica do wyczyszczenia
 * @return Wyczyszczon� tablic� lub 0 je�li argumentem nie by�a tablica.
 */
mixed *
clean_array(mixed *arr)
{
    if(!pointerp(arr))
        return 0;
    if(!sizeof(arr))
        return ({});

    int i, j;
    for(i=0;i<sizeof(arr);i++)
    {
        for(j=0;j<sizeof(arr);j++)
        {
            if(i==j)
                continue;
            if(arr[i] == arr[j])
                arr = exclude_array(arr, j, j);
        }
    }

    return arr;
}

//Funkcje koniecznie do przeniesienia do drivera, tylko chwilowo tu.
#if 0
/**
 * To by si� przyda�o jako� przenie�� do normalnego member_array'a...
 * A nie maskowa� efuna simulem:P
 * Co do zmian w sk�adni mi�dzy starym a nowym:
 * Mamy nowy argument, dotyczy on tylko sytuacji kiedy poszukujemy elementu
 * b�d�cego tablic�. Argument ten m�wi nam czy tablice musz� mie� identyczn�
 * kolejno��, czy te� mog� by� w r�nej kolejno�ci
 */
int
member_array(mixed elem, mixed tab, int flag=0)
{
    elem = secure_var(elem);
    tab  = secure_var(tab);

    if(pointerp(elem))
    {
        int i;
        for(i=0;i<sizeof(tab);i++)
        {
            if(!pointerp(tab[i]))
                break;
            if(compare_arr(elem, tab[i], flag))
                return i;
        }
        return -1;
    }
    else
        return efun::member_array(elem, tab);
}
#endif

/**
 * Funkcja ta sprawdza za pomoc� efuna real_path czy podana w
 * argumencie �cie�ka jest dowi�zaniem (symbolicznym lub twardym)
 * Z muda dowi�za� nie da si� robi�(mo�e kiedy�:P) ale
 * da si� je robi� z poziomu linuxa, a to by�a powa�na dziura
 * w zabezpieczeniach. Teraz jest to zablokowane, a czy co� jest
 * dowi�zaniem mo�na sprawdzi� w�a�nie za pomoc� tej funkcji.
 *
 * @param path �cie�ka do sprawdzenia.
 *
 * @return 1 - podana �cie�ka jest linkiem
 * @return 2 - podana �cie�ka nie jest linkiem
 */
int is_link(string path)
{
    if(path[-1] == '*')
        path = path[..-2];

    if(path[-1] == '/')
        path = path[..-2];

    string rp = real_path(path);

    if(path == rp || !rp)
        return 0;

    return 1;
}

string
capitalize(mixed str)
{
    if(!str)
        return efun::capitalize(str);

    if(!stringp(str))
        return efun::capitalize(str);

    //nie powi�kszamy kolork�w
    if(str[0] == 27)
    {
        int charnr = strchr(str, "m");
        if(charnr < 0)
            return efun::capitalize(str);
        return str[0..(charnr)] + capitalize(str[(charnr+1)..]);
    }
    //Powiekszamy pierwsz� liter�.
    //if(member_array(str[0..0], explode(ALPHABET_PL, "")) == -1 && sizeof(str) != 1)
    //    return str[0..0] + capitalize(str[1..]);

    return efun::capitalize(str);
}

/**
 * @param i kt�rego livinga w kolejno�ci zwr�ci�
 * @return Zwraca poprzedniego livinga
 */
object
previous_living(int i=0)
{
    if(i > 0)
        return 0;

    int j = i;

    object pl;
    while((pl = previous_object(i--)) && (!living(pl) || interactive(pl) && (j++) < 0));

    return pl;
}

/**
 * @param i kt�rego interactiva w kolejno�ci zwr�ci�
 * @return Zwraca poprzedniego interactiva
 */
object
previous_interactive(int i=0)
{
    if(i > 0)
        return 0;

    int j = i;

    object pi;
    while((pi = previous_object(i--)) && (!interactive(pi) || interactive(pi) && (j++) < 0));

    return pi;
}

/**
 * Ta funkcja powinna by� u�ywana we wszystkich funkcjach wywo�ywanych przez vbfc.
 * Dzi�ki niej zdobywamy vbfc_object, nawet je�li vbfc by�o zagnie�d�ane(co
 * narazie niestety wi��e si� z komplikacjami i wymaga edycji drivera).
 * @return VBFC obiekt.
 */
object
vbfc_object()
{
    object pin = previous_interactive();

    if(pin)
        return pin;

    pin = 0;
    //je�li nie znajdziemy interactiva, to szukamy livinga..
    if(pin = previous_living())
        return pin;

    //je�li nie znajdziemy interactiva ani livinga, to musimy przynajmniej usun��
    // /secure/vbfc_object.

    int i = -2;
    while(remove_extension(MASTER_OB(previous_object(i)), "c") == "/secure/vbfc_object")
        i--;

    return previous_object(i);
}

/**
 * A ta funkcja chyba na dobre zago�ci w simulach.
 * Musia�em j� zdefiniowa�, �eby wszystkie obiekty zacze�y dzia�a� poprawnie.
 * Nie wiem kto wymy�li� wyszukiwanie zwyk�ym presentem, w opise kt�rego
 * wyra�nie pisze, �e jest do LIVING�W.
 *
 * @param nz nazwa lub short szukanego obiektu
 * @param gdzie gdzie szuka� tego obiektu
 *
 * @return 0 obiektu nie uda�o si� znale��
 * @return pasuj�cy obiekt lub pasuj�ce obiekty
 */
public mixed
present(mixed str, mixed gdzie=({previous_object()}), int co=0)
{
    if(!objectp(str) && !stringp(str))
        throw("Z�y argument 1 do funkcji present(). Wymagany typ to string lub object za� otrzymant to " + type_name(str) + ".\n");

    if(!pointerp(gdzie))
        gdzie = ({gdzie});

    object *a;
    string *b;
    a = filter(gdzie, objectp);
    b = filter(gdzie, stringp);

    gdzie = a + b;

    if(!sizeof(gdzie))
    {
        throw("Z�y argument 2 do funkcji present(). Wymagany typ to tablica string�w lub obiekt�w za� otrzymany to " +
            type_name(gdzie) + ".\n");
    }

    object ob;

    if(objectp(ob = efun::present(str, gdzie)))
        return ob;

    object *ret;

    if(objectp(str))
    {
        foreach(object *inv : map(gdzie, &all_inventory()))
            if(member_array(str, inv) != -1)
                return str;
    }
    else
    {
        if(!parse_command(str, flatten(map(gdzie, &all_inventory())), "%i:" + PL_MIA, ret))
            return 0;

        ret = NORMAL_ACCESS(ret, 0, 0);

        if(!sizeof(ret))
            return 0;

        if(co == 0)
            return ret[0];

        return ret;
   }
}

/**
 * Funkcja prawdza czy podana zmienna jest ci�giem vbfc
 *
 * @param ciag ci�g do sprawdzenia
 *
 * @return 1 jest ci�giem
 * @return 0 nie jest ci�giem
 */
public int
vbfcp(mixed ciag)
{
    if(!stringp(ciag))
        return 0;

    if(sscanf("@@%s@@", ciag) == 1)
    {
        //Trzebaby tu sprawdzi� czy s� tylko dozwolone znaki:)
        return 1;
    }
}

/**
 * Funkcja podmienia warto�ci w stringu.
 *
 * @param zmieniane Mapping ze zmienianymi warto�ciami gdzie kluczami s�
 *                  obecne ci�gi znak�w, a warto�ciami oczekiwane ci�gi.
 * @param str       Zamieniane stringi.
 *
 * @return Podmieniony string.
 */
public string
str_replace(mapping zmieniane, string str)
{
    string *mkeys = m_indexes(zmieniane);

    for(int i = 0 ; i < sizeof(mkeys) ; i++)
        str = implode(explode(str, mkeys[i]), zmieniane[mkeys[i]]);

    return str;
}

/**
 * FUNCKJA WYCOFANA u�yj dictionary_search()
 */
public mixed
slownik_pobierz(string str)
{
    return dictionary_search(str);
}

/**
 * FUNKCJA WYCOFANA u�yj dictionary_check()
 */
public int
slownik_sprawdz(string str)
{
    return dictionary_check(str);
}

public int
is_pragma_no_reset(mixed x=0, mixed y=0)
{
    return 0;
}
//Includujemy plik z obs�ug� mxp (krun)
#ifdef MXP_IS_ACTIVE
#include "/secure/mxp.c"
#endif MXP_IS_ACTIVE

/*public varargs string
set_color(int i, ...)
{
return "";
}

public string
clear_color_format(string a)
{
return a;;
}

public string 
clear_color_string(string a)
{
return a;
}

public string
clear_color()
{
return "";
}*/

#endif _FUNCTION
