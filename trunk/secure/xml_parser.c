/**
 * \file /secure/xml_parser
 *
 * Lpc'owy parser j�zyka XML.
 *
 * @author Krun, krun@vattghern.com.pl
 * @date 28.7.2008 14:23:42
 */

#pragma no_reset
#pragma resident
#pragma save_binary
#pragma strict_types

#include <macros.h>
#include <xml_parser.h>

#define XML_OPEN_TAG_FUN                    0
#define XML_CLOSE_TAG_FUN                   1
#define XML_HANDLER_FUN                     2

#define DBG(x)  find_player("krun")->catch_msg((x) + "\n");

/**
 * Zmienne
 */
object parse_object = 0;
string open_tag_fun,
       close_tag_fun,
       handler_fun;

mixed tagi = ({});
mixed wartosci = ([]);

/** ********************************************************************** **
 ** ************** F U N K C J E   P R Y W A T N E *********************** **
 ** ********************************************************************** **/

/**
 * Sprawdza czy nazwa tagu, atrybutu jest w�a�ciwa.
 *
 * @return <ul>
 *  <li> !0 - poprawna </li>
 *  <li> 0  - niepoprawna </li>
 * </ul>
 */
private status
check_name(string name)
{
    int i;
    string *allowed;

    allowed = explode(XML_ATTR_NAME_ALLOWED, "");


    for(i = 0 ; i < strlen(name) ; i++)
        if(member_array(name[i..i], allowed) == -1)
            return 0;

    return 1;
}

/**
 * Wywo�uje funkcje na @param parse_object
 *
 * @param funkcja do wywo�ania
 * @param ... argumenty do wywo�ywanej funkcji
 */
private varargs int
call_parse(string funkcja, ...)
{
    if(!parse_object)
        return 0;

    return call_otherv(parse_object, funkcja, argv);
}

/**
 * Wywo�uje funkcje @param open_tag_fun na obiekcie @param parse_object
 *
 * @param tag - otwierany tag
 * @param attr - atrybuty otwieranego tagu
 */
private int
call_open_fun(string tag, mapping attr = ([]))
{
    return call_parse(open_tag_fun, tag, attr);
}

/**
 * Wywo�uje funkcje @param close_tag_fun na obiekcie @param parse_object
 *
 * @param tag - zamykany tag
 */
private int
call_close_fun(string tag)
{
    return call_parse(close_tag_fun, tag);
}

/**
 * Wywo�uje funkcje @Param handler_fun na obiekcie @param parse_object
 *
 * @param tresc - pobrana tre��
 */
private int
call_handler_fun(string tresc)
{
    return call_parse(handler_fun, tresc);
}

/**
 * Ustawiamy obiekt na kt�rym b�dziemy wywo�ywa� funkcje parsuj�ce.
 *
 * @param ob obiekt na kt�rym wywo�ywane b�d� funkcje parsuj�ce.
 */
void
set_parse_object(object ob)
{
    parse_object = ob;
}

/**
 * @return obiekt na kt�rym wywo�ywane b�d� funkcje parsuj�ce.
 */
object
query_parse_object()
{
    return parse_object;
}

/**
 * Ustawiamy funkcje wywo�ywane do parsowania.
 * Je�li ustawiamy jako zmienn� typu function to wszystkie podane
 * argumenty zostan� zignorowane.
 *
 * @param open_tag  - wywo�ywana podczas otwierania tag�w.
 * @param close_tag - wywo�ywana podczas zamykania tag�w.
 * @param handler   - wywo�ywana podczas zbierania danych.
 */
void
set_parse_functions(mixed open_tag, mixed close_tag, mixed handler)
{
    if(!stringp(open_tag))
    {
        if(functionp(open_tag))
            open_tag = explode(function_name(open_tag), "->")[-1];
        else
            return;
    }

    if(!stringp(close_tag))
    {
        if(functionp(close_tag))
            close_tag = explode(function_name(close_tag), "->")[-1];
        else
            return;
    }

    if(!stringp(handler))
    {
        if(functionp(handler))
            handler = explode(function_name(handler), "->")[-1];
        else
            return;
    }

    open_tag_fun    = open_tag;
    close_tag_fun   = close_tag;
    handler_fun     = handler;
}

/**
 * @return Zwraca funkcje wywo�ywane przy parsowaniu dokumentu.
 */
string *
query_parse_functions()
{
    return ({open_tag_fun, close_tag_fun, handler_fun});
}

/**
 * Funkcja parsuj�ca dane.
 */
int
parse_xml(string dane)
{
    int sl, sg, sz;
    string tag, value, *e;
    mapping attr = ([]);

    dane = str_replace((["  " : "", "\n" : ""]), dane);

    if(dane[0] != '<' || dane[-1] != '>')
        return XML_PARSE_ERROR_DATA;

    sl = sizeof(explode(dane, "<"));
    sg = sizeof(explode(dane, ">")) + (dane[-1] == '>' ? 1 : 0);
    sz = sizeof(explode(dane, "</")) + sizeof(explode(dane, "/>")) - 2;

    if(sl != sg || sl != sz * 2)
        return XML_PARSE_ERROR_DATA;

    if(dane[0..1] == "<?")
    {
        int i = 1;
        while(dane[++i] != '>' && i < strlen(dane));

        if(i == strlen(dane) - 1)
        {
            DBG("KRUCYFUX");
            return XML_PARSE_ERROR_DATA;
        }

        dane = dane[++i..];
    }

    e = explode(dane, "<")[1..];

    foreach(string a : e)
    {
        int mb_r = 0, closed;
        string *b = explode(a, ">");

//         dump_array(b);

        if(!sizeof(b))
            continue;

        tag = b[0];

        if(tag[0] == '/')
        {   //Mamy tag ko�cz�cy.
            if(!check_name(tag[1..]))
                return XML_PARSE_ERROR_TAG;

            call_close_fun(tag[1..]);
            closed = 1;
        }
        else
        {
            int i, zn;
            string pattr, attr_name, attr_val, *c;

            i = 0;
            //Szukamy tagu.
            while(tag[++i] != ' ' && i < strlen(tag))
            {
                if(tag[i] == '>')
                {
                    zn = 1;
                    break;
                }
            }

            if(!zn)
            {
                int slash = 0, strl;

                pattr = tag[i..];

                tag = tag[0..(i - 1)];

                //A teraz bierzemy si� za atrybuty

                //Atrybut�w mo�e by� dowolna ilo��
                //Zawsze zaczynaj� si� " i ko�cz� "

                attr_name = "";
                attr_val = "";
                strl = strlen(pattr);
                for(int j = 0 ; j < strlen(pattr) ; j++)
                {
                    int br = 0;

                    while(pattr[++j] != '=' && j < strl)
                    {
                        if(pattr[j] == '/')
                        {
                            br = 1;
                            break;
                        }

                        if(pattr[j] != ' ')
                            attr_name += pattr[j..j];
                    }

                    if(br)
                        break;

                    if(!check_name(attr_name))
                        return XML_PARSE_ERROR_ATTR_NAME;

                    while(pattr[++j] != '\"' && j < strl);

                    while(1)
                    {
                        if(j > strl)
                            break;

                        if(pattr[++j] == '\"' && pattr[j - 1] != '\\')
                        {
                            attr[attr_name] = attr_val;
                            attr_name = "";
                            attr_val = "";
                            break;
                        }

                        attr_val += pattr[j..j];
                    }
                }
            }

            if(!check_name(tag))
                return XML_PARSE_ERROR_TAG;

            call_open_fun(tag, attr);
            attr = ([]);

            if(pattr[-1] == '/')
            {
                closed = 1;
                call_close_fun(tag);
            }

            if(sizeof(b) > 1)
            {
                if(strlen(b[1]))
                    call_handler_fun(b[1]);
            }
        }
    }
}