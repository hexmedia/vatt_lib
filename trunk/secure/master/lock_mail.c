/**
 * Plik do blokowania adres�w email.
 *
 * Autor: Krun
 * Czerwiec roku pa�skiego 2007
 */

string *blocked_mails;

static private int
add_to_email_list(string email)
{
    if(!pointerp(blocked_mails))
        blocked_mails = ({email});
    else
        blocked_mails += ({email});

    save_master();
}

static private int
remove_from_email_list(string email)
{
    NF("Adresu nie ma na li�cie zablokowanych, nie mo�e wi�c zosta� usuni�ty.\n");
    if(!pointerp(blocked_mails))
        return 0;
    if(!sizeof(blocked_mails))
        return 0;
    if(!member_array(email, blocked_mails) == -1)
        return 0;

    blocked_mails -= ({email});

    save_master();
    return 1;
}

public int
block_mail(string str)
{
    if(!CALL_BY(WIZ_CMD_ARCH))
        return 0;

    if(!str)
        return 0;

    string *tmp;
    if(sizeof(tmp=explode(str, "@")) != 2 || sizeof(explode(tmp[1], ".")) != 2)
    {
        NF(str + " nie wydaje si� by� poprawnym adresem email.\n");
        return 0;
    }

    if(member_array(str, blocked_mails) >= 0)
    {
        write("Zosta� ju� wcze�niej dodany do listy.\n");
        return 1;
    }

    tmp = explode(str, " ");

    if(sizeof(tmp) > 2)
    {
        NF("Co chcesz zrobi� z tym adresem?\n");
        return 0;
    }

    str = tmp[1];
    string add_or_remove;
    if(tmp[0] == "a" || tmp[0] == "add" || tmp[0] == "dodaj" || tmp[0] == "d")
    {
        add_or_remove = "dodany do";
        add_to_email_list(str);
    }
    else if(tmp[0] == "r" || tmp[0] == "remove" || tmp[0] == "usun" || tmp[0] == "u")
    {
        add_or_remove = "usuni�ty z";
        if(!remove_from_email_list(str))
            return 0;
    }

    write("Adres " + str + " zosta� " + add_or_remove + " listy blokowanych adres�w.\n");
    return 1;
}

public int
check_blocked_mail(string str)
{
    return (member_array(str, blocked_mails) >= 0);
}