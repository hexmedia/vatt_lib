/*
 * /secure/admin/faircombat.c
 *
 * This module checks weapons and armours
 *
 * Rather unneccesary, time-consuming code that we could do without if people
 * only could stop cheating.
 *
 */

#include "/sys/wa_types.h"
#include "/sys/log.h"
#include "/sys/math.h"

static void bad_object(object obj, string logfile, string logmess);

/*
 * Nazwa funkcji:  check_wep
 * Opis         :  Check that a weapon is all right.
 * Argumenty    :  wep - The weapon in question and takes neccesary actions to
                   modify what is wrong.
 * Zwraca       :  Correct weapon class.
 */
nomask int
check_wep(object wep)
{
    if (function_exists("check_weapon", wep) != "/std/weapon")
        return 0;

    return 1;
}

/*
 * Nazwa funkcji:  check_arm
 * Opis         :  Check that a armour is all right.
 * Argumenty    :  arm - The armour in question and takes neccesary actions to
                   modify what is wrong.
 * Zwraca       :  Correct armour class.
 */
nomask int
check_arm(object arm)
{
    if (!arm->is_armour())
        return 0;

    return 1;
}

/*
 * Nazwa funkcji: bad_object
 * Opis         : Dispose of bad objects
 * Argumenty    : obj - The object.
 *                logfile - Where to log the event.
 *                logmess - What to log.
 */
static void
bad_object(object obj, string logfile, string logmess)
{
    write("The " + obj->query_name() + " dissolves in a puff of black smoke!\n");
    log_file(logfile, logmess);
    obj->remove_object();
}
