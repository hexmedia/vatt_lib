/**
 * \file /secure/master/location_checker.c
 *
 * Dzi�ki temu plikowi mo�emy sprawdzi� czy wiz mo�e czy nie mo�e wej��
 * na jak�� lokacje.
 */

#define PLAYER_ALLOWED              ({"/d/Standard/login/wioska/*", "/d/Standard/Redania/*",    \
                                      "/d/Standard/wiz/dywanik", "/std/drzewo/nadrzewie",       \
                                      "/d/Standard/room/void", "/d/Standard/obj/statue"})

#define WIZ_LVL                     15
#define WIZARD_FORBIDDEN            PLAYER_ALLOWED

string *lc_allowed_wizards = ({});

public int
check_if_location_is_forbidden(mixed wiz, mixed location)
{
    if(stringp(wiz))
        wiz = find_player(wiz);

    if(!objectp(wiz))
        return 0;

    if(stringp(location))
        location = find_object(location);

    if(!objectp(location))
        return 0;

    if(!interactive(wiz))
        return 0;

    //Je�li gracz to sprawdzamy czy celem jest jedna z lokacji dozwolonych gracz�.
    if(!wiz->query_wiz_level() && !wildmatch("*jr", wiz->query_real_name()))
    {
        foreach(string allow : PLAYER_ALLOWED)
        {
            if(wildmatch(allow, MASTER_OB(location)))
                return 0;
        }

        return 1;
    }
    //Je�li nie to czy kt�ra� z zabronionych wizom.
    else if(SECURITY->query_wiz_level(wiz->query_real_name()) < WIZ_LVL)
    {
        if(member_array(wiz->query_real_name(), lc_allowed_wizards) >= 0)
            return 0;

        foreach(string forbidden : WIZARD_FORBIDDEN)
        {
            if(wildmatch(forbidden, MASTER_OB(location)))
            {
                mixed *at = SECURITY->query_admin_team_members("aow");
                at = at || ({});
                (map(at, &find_player())-({0}))->catch_msg(UC(wiz->query_real_name()) +
                    " pr�buje si� dosta� na teren dla niego zakazany: "+ MASTER_OB(location) + "!\n");
                return 2;
            }
        }
    }
}

public void
lc_add_allowed(mixed wiz)
{
    if (MASTER_OB(previous_object()) != WIZ_CMD_LORD)
        return 0;

    if(objectp(wiz))
        wiz = wiz->query_real_name();

    if(!stringp(wiz))
        return;

    if(!pointerp(lc_allowed_wizards))
        lc_allowed_wizards = ({});

    lc_allowed_wizards += ({wiz});
}

public void
lc_remove_allowed(mixed wiz)
{
    if (MASTER_OB(previous_object()) != WIZ_CMD_LORD)
        return 0;

    if(objectp(wiz))
        wiz = wiz->query_real_name();

    if(!stringp(wiz))
        return;

    if(!pointerp(lc_allowed_wizards))
        lc_allowed_wizards = ({});

    lc_allowed_wizards -= ({wiz});
}

public int
lc_query_allowed(mixed wiz)
{
    if(objectp(wiz))
        wiz = wiz->query_real_name();

    if(!stringp(wiz))
        return 0;

    return (member_array(wiz, lc_allowed_wizards) >= 0);
}

public string *
lc_query_all_allowed()
{
    return lc_allowed_wizards;
}

public int
legal_login_location(string loc)
{
    if(!loc)
        return 0;

    if(sizeof(explode(loc, "#")) == 2)
        return 0;

    if(wildmatch("/d/Standard/Redania/*", loc))
        return 1;
}