/**
 * \file /secure/master/slownik.c
 *
 * Plik obs�uguuj�cy administracje s�ownika z poziomu mastera.
 *
 * @author  Krun (krun@vattghern.pl)
 * @date    25.4.2008
 * @version 1.0
 */

public int
slownik_zatwierdz(string mianownik)
{
    if(query_wiz_rank(TP->query_real_name()) < WIZ_MAGE)
        return 0;

//     return slownik_admin_zatwierdz(mianownik);
}

public int
slownik_odrzuc(string mianownik)
{
    if(query_wiz_rank(TP->query_real_name()) < WIZ_MAGE)
        return 0;

//     return slownik_admin_odrzuc(mianownik);
}

public int
slownik_usun(string mianownik)
{
    if(query_wiz_rank(TP->query_real_name()) < WIZ_MAGE)
        return 0;

//     return slownik_admin_usun(mianownik);
}