/*
 * /secure/master/fob.c
 *
 * Subpart of /secure/master.c
 *
 * Handles all domain, wizard and application administration in the game.
 */

#include "/sys/composite.h"
#include "/sys/const.h"

/*
 * These global variables are stored in the KEEPERSAVE.
 */
private int	dom_count;	/* The next domain number to be added */
private mapping m_domains;	/* The domain mapping */
private mapping m_wizards;	/* The wizard mapping */
private mapping m_applications;	/* The applications mapping */
private mapping m_trainees;	/* The list of trainees */
private mapping m_global_read;	/* The global read mapping */

/**************************************************************************
 *
 * The 'm_domains' mapping holds the domain name as index and an array with
 * the following items as value:
 *
 * [ 0] - The domain number. It is stored as tag in the player along with
 *        the quest bit set by the domain objects.
 *
 * [ 1] - The short name of the domain. This should be a unique abbreviation
 *        of exactly three characters.
 *
 * [ 2] - The name of the domain lord.
 *
 * [ 3] - The name of the steward of the domain, if any.
 *
 * [ 4] - An array holding the names of all members, including the lord and
 *        the steward.
 *
 * [ 5] - The name of the domain madwand, if any.
 *
 * [ 6] - The max number of members the domain wants to accept.
 *
 * [ 7] - The number of quest xp given to mortals
 *
 * [ 8] - The number of combat xp given to mortals
 *
 * [ 9] - The number of commands executed in the domain.
 *
 **************************************************************************
 *
 * The 'm_wizards' mapping holds the wizard name as index and an array with
 * the following items as value:
 *
 * [ 0] - The rank of the wizard.
 *
 * [ 1]	- The level of the wizard.
 *
 * [ 2]	- The name of the wizard who last changed the level/rank.
 *
 * [ 3] - The time the level/rank was last changed.
 *
 * [ 4]	- The domain the wizard is a member of.
 *
 * [ 5]	- The name of the wizard who last changed the domain.
 *
 * [ 6] - The time the domain was last changed.
 *
 **************************************************************************
 *
 * The 'm_applications' mapping has the domain-names as index and the values
 * are arrays with the names of the wizards applying for membership.
 *
 **************************************************************************
 *
 * The 'm_global_read' mapping contains all wizards who have global read
 * access. Their names are the incides. The values is an array with the
 * following information:
 *
 * [ 0] - The wizards who granted the access.
 *
 * [ 1] - A short string describing the reason the wizard has global read.
 *
 **************************************************************************
 *
 * The 'm_trainees' mapping contains all wizards who have been marked as
 * trainee. Their names are the indices. The value is '1'. This mapping is
 * a mapping since that is quicker to test than membership of an array.
 *
 */

/* The maximum number of members a Lord may allow to his domain. */
#define DOMAIN_MAX	 	(9)

/* These are the indices to the arrays in the domain-mapping. */
#define FOB_DOM_NUM	 	(0)
#define FOB_DOM_SHORT    	(1)
#define FOB_DOM_LORD	 	(2)
#define FOB_DOM_STEWARD  	(3)
#define FOB_DOM_MEMBERS	 	(4)
#define FOB_DOM_MADWAND  	(5)
#define FOB_DOM_MAXSIZE	 	(6)
#define FOB_DOM_QXP	 	(7)
#define FOB_DOM_CXP	 	(8)
#define FOB_DOM_CMNDS    	(9)

/* These are the indices to the arrays in the wizard-mapping. */
#define FOB_WIZ_RANK	 	(0)
#define FOB_WIZ_LEVEL	 	(1)
#define FOB_WIZ_CHLEVEL	 	(2)
#ifdef FOB_KEEP_CHANGE_TIME
#define FOB_WIZ_CHLTIME  	(3)
#define FOB_WIZ_DOM	 	(4)
#define FOB_WIZ_CHDOM	 	(5)
#define FOB_WIZ_CHDTIME  	(6)
#else
#define FOB_WIZ_DOM	 	(3)
#define FOB_WIZ_CHDOM	 	(4)
#endif FOB_KEEP_CHANGE_TIME

// Takie male spolszczenie - Delvert
#define FOB_WIZ_CHLEVEL_MIA	(0)
#define FOB_WIZ_CHLEVEL_BIE	(1)
#define FOB_WIZ_CHDOM_MIA	(0)
#define FOB_WIZ_CHDOM_BIE	(1)

/*
 * Nazwa funkcji: load_fob_defaults
 * Opis         : This function is called from master.c when the KEEPERAVE
 *                file cannot be found. The defined values can be found in
 *                config.h.
 */
static void
load_fob_defaults()
{
    m_wizards = DEFAULT_WIZARDS;
    m_domains = DEFAULT_DOMAINS;
    dom_count = m_sizeof(m_domains);
    m_applications = ([ ]);
    m_global_read = ([ ]);
    m_trainees = ([ ]);
}

/*
 * Nazwa funkcji: getwho
 * Opis         : This function gets the name of the interactive command
 *                giver. The euid of the interactive player and the euid
 *                of the previous object must be equal in order to function.
 * Zwraca       : string - the name or "" in case of inconsistencies.
 */
static string
getwho()
{
    string euid;

    euid = geteuid(this_interactive());
    return ((euid == geteuid(previous_object())) ? euid : "");
}

/************************************************************************
 *
 * The domain administration code.
 *
 */

/*
 * Nazwa funkcji: query_domain_name
 * Opis         : Find the domain name from the number.
 * Argumenty    : int number - the domain number.
 * Zwraca       : string - the domain name if found, else 0.
 */
string
query_domain_name(int number)
{
    string *domains;
    int index;

    domains = m_indices(m_domains);
    index = sizeof(domains);

    while(--index >= 0)
    {
        if (m_domains[domains[index]][FOB_DOM_NUM] == number)
        {
            return domains[index];
        }
    }

    return 0;
}

/*
 * Nazwa funkcji: query_domain_number
 * Opis         : Find the number of a domain.
 * Argumenty    : string dname - the name of the domain.
 * Zwraca       : int - the number if found, -1 otherwise.
 */
int
query_domain_number(string dname)
{
    dname = capitalize(dname);

    if (!sizeof(m_domains[dname]))
    {
	return -1;
    }

    return m_domains[dname][FOB_DOM_NUM];
}

/*
 * Nazwa funkcji: query_domain_short
 * Opis         : Find the short name of the domain.
 * Argumenty    : string dname - the name of the domain.
 * Zwraca       : string - the short name if found, "" otherwise.
 */
string
query_domain_short(string dname)
{
    dname = capitalize(dname);

    if (!sizeof(m_domains[dname]))
    {
	return "";
    }

    return m_domains[dname][FOB_DOM_SHORT];
}

/*
 * Nazwa funkcji: set_domain_short
 * Opis         : Set the short name of a domain. The short name is an
 *                appropriate abbreviation of exactly three characters of
 *                the domain-name. This function may only be called from
 *                the Lord soul, so we do not have to make additional
 *                checks.
 * Argumenty    : string dname - the name of the domain (capitalized).
 *                string sname - the short name of the domain (lower case).
 * Zwraca       : int 1/0 - success/failure.
 */
int
set_domain_short(string dname, string sname)
{
    /* This function may only be called from the Lord soul. */
    if (!CALL_BY(WIZ_CMD_LORD))
        return 0;

    m_domains[dname][FOB_DOM_SHORT] = sname;
    save_master();
    return 1;
}

/*
 * Nazwa funkcji: query_domain_lord
 * Opis         : Find the lord of a domain.
 * Argumenty    : string dname - the name of the domain.
 * Zwraca       : string the domain lord if found, "" otherwise.
 */
string
query_domain_lord(string dname)
{
    dname = capitalize(dname);

    if (!sizeof(m_domains[dname]))
    {
        return "";
    }

    return m_domains[dname][FOB_DOM_LORD];
}

/*
 * Nazwa funkcji: query_domain_steward
 * Opis         : Find the steward of a domain.
 * Argumenty    : string dname - the name of the domain.
 * Zwraca       : string the steward if the domain exists, "" otherwise.
 */
string
query_domain_steward(string dname)
{
    dname = capitalize(dname);

    if (!sizeof(m_domains[dname]))
        return "";

    return m_domains[dname][FOB_DOM_STEWARD];
}

/*
 * Nazwa funkcji: query_domain_members
 * Opis         : Find and return the member array of a given domain.
 * Argumenty    : string dname - the domain.
 * Zwraca       : string * - the array if found, ({ }) otherwise.
 */
string *
query_domain_members(string dname)
{
    dname = capitalize(dname);

    if (!sizeof(m_domains[dname]))
    {
	return ({ });
    }

    return secure_var(m_domains[dname][FOB_DOM_MEMBERS]);
}

/*
 * Nazwa funkcji: query_domain_madwand
 * Opis         : Find and return the name of the madwand of the domain.
 *                When there is no madwand support, always returns "".
 * Argumenty    : string dname - the domain.
 * Zwraca       : string - the name of the madwand if found, "" otherwise.
 */
string
query_domain_madwand(string dname)
{
#ifdef FOB_MADWAND_SUPPORT
    dname = capitalize(dname);

    if (!sizeof(m_domains[dname]))
    {
	return "";
    }

    return m_domains[dname][FOB_DOM_MADWAND];
#else
    return "";
#endif FOB_MADWAND_SUPPORT
}

/*
 * Nazwa funkcji: query_domain_list
 * Opis         : Return a list of all domains.
 * Zwraca       : string * - the list.
 */
string *
query_domain_list()
{
    return m_indices(m_domains);
}

/*
 * Nazwa funkcji: set_domain_max
 * Opis         : Set maximum number of wizards wanted. This function may
 *                only be called from the Lord soul so we do not have to
 *                make any additional checks.
 * Argumenty    : string dname - the capitalized domain name.
 *                int    max   - the new maximum.
 * Zwraca       : int 1/0 - success/failure.
 */
int
set_domain_max(string dname, int max)
{
    /* May only be called from the Lord soul. */
    if (!CALL_BY(WIZ_CMD_LORD))
    {
	return 0;
    }

    m_domains[dname][FOB_DOM_MAXSIZE] = max;
    save_master();
    return 1;
}

/*
 * Nazwa funkcji: query_domain_max
 * Opis         : Find and return the maximum number of wizards wanted.
 * Argumenty    : string dname - the name of the domain.
 * Zwraca       : int - the max number if found, -1 otherwise.
 */
int
query_domain_max(string dname)
{
    dname = capitalize(dname);

    if (!sizeof(m_domains[dname]))
        return -1;

    return m_domains[dname][FOB_DOM_MAXSIZE];
}

/*
 * Nazwa funkcji: query_default_domain_max
 * Opis         : Get the default maximum number of members in a domain.
 * Zwraca       : int - the value defined in DOMAIN_MAX.
 */
int
query_default_domain_max()
{
    return DOMAIN_MAX;
}

/*
 * Nazwa funkcji: make_domain
 * Opis         : Create a new domain.
 * Argumenty    : string dname - the domain name.
 *                string sname - the short domain name.
 *                string wname - the name of the domain lord.
 * Zwraca       : int 1/0 - success/failure.
 */
int
make_domain(string dname, string sname, string wname)
{
    string cmder;

    /* Only accept calls from the arch command soul. */
    if (!CALL_BY(WIZ_CMD_ARCH))
    {
	return 0;
    }

    cmder = getwho();
    if (sizeof(m_domains[dname]))
    {
	notify_fail("Domena " + dname + " ju^z istnieje.\n");
	return 0;
    }

    if ((strlen(dname) > 11) ||
	(strlen(sname) != 3))
    {
	notify_fail("Nazwa domeny mo^ze mie^c co najwy^zej 11 znak^ow, a " +
		    "skr^ot nazwy - dok^ladnie 3 znaki.\n");
	return 0;
    }

    if (sizeof(filter(m_indices(m_domains),
		      &operator(==)(sname) @ query_domain_short)))
    {
	notify_fail("Skr^ot '" + sname + "' jest ju^z w u^zyciu.\n");
    }

    if ((!pointerp(m_wizards[wname])) ||
	(m_wizards[wname][FOB_WIZ_RANK] != WIZ_APPRENTICE))
    {
	notify_fail("Gracz " + capitalize(wname) +
		    " nie jest rekrutem.\n");
	return 0;
    }

    set_auth(this_object(), "root:root");

    switch(file_size("/d/" + dname))
    {
    case -1:
	/* Check for old discarded instances of the domain. If there is one,
	 * we must use it or else you shall have to rename or remove it
	 * first.
	 */
	if (file_size(DISCARD_DOMAIN_DIR + "/" + dname) == -2)
	{
	    if (rename((DISCARD_DOMAIN_DIR + "/" + dname), ("/d/" + dname)))
	    {
		write("Przywr^ocono domen^e '" + dname + "'.\n");
	    }
	    else
	    {
		write("Nie uda^lo si^e przywr^oci^c domeny '" +
		    dname + "'. Zr^ob to r^ecznie.\n");
		return 1;
	    }
	}
	else
	{
	    /* Create the domain subdirectory. */
	    if (!mkdir("/d/" + dname))
	    {
		write("Nie uda^lo si^e utworzy^c katalogu domeny!\n");
		return 1;
	    }

	    write("Utworzono katalog dla nowej domeny '" + dname + "'.\n");
	}
	break;

    case -2:
	write("Katalog domeny '" + dname + "' ju^z istnieje.\n");
	break;

    default:
	write("Ju^z istnieje plik /d/" + dname +
	      ", wi^ec utworzenie domeny o tej nazwie jest niemo^zliwe.\n");
	return 1;
    }

    /* Add the domain entry to the domain mepping. */
    m_domains[dname] =
	({ dom_count++, sname, wname, "", ({ }), "", DOMAIN_MAX, 0, 0, 0 });

    /* Make the apprentice a lord. This will also save the domain mapping. */
    add_wizard_to_domain(dname, wname, cmder);
    do_change_rank(wname, WIZ_LORD, cmder);

    return 1;
}

/*
 * Nazwa funkcji: remove_domain
 * Opis         : Remove a domain, demote the wizards, move the code.
 * Argumenty    : string dname - the domain to remove.
 * Zwraca       : int 1/0 - success/failure.
 */
int
remove_domain(string dname)
{
    int    index;
    int    size;
    string cmder;
    string *members;

    /* May only be called from the arch soul. */
    if (!CALL_BY(WIZ_CMD_ARCH))
    {
	return 0;
    }

    cmder = getwho();
    dname = capitalize(dname);
    if (!sizeof(m_domains[dname]))
    {
	notify_fail("Domena '" + dname + "' nie istnieje.\n");
	return 0;
    }

    if (dname == WIZARD_DOMAIN)
    {
	write("Domena " + WIZARD_DOMAIN + " nie mo^ze by^c usuni^eta.\n");
	return 1;
    }

    if (file_size(DISCARD_DOMAIN_DIR + "/" + dname) != -1)
    {
	notify_fail("Ju^z istnieje " + DISCARD_DOMAIN_DIR +
		    "/" + domain + ". Usu^n albo zmie^n nazw^e.\n");
	return 0;
    }

    members = m_domains[dname][FOB_DOM_MEMBERS];
    index = -1;
    size = sizeof(members);
    while(++index < size)
    {
	/* Don't demote mages, arches and keepers. */
	switch(m_wizards[members[index]][FOB_WIZ_RANK])
	{
	case WIZ_MAGE:
	case WIZ_ARCH:
	case WIZ_KEEPER:
	    break;

	default:
	    do_change_rank(members[index], WIZ_APPRENTICE, cmder);
	    write("Zdegradowano: " + capitalize(members[index]) +
		  //" do rangi apprentice.\n");
                  " do rangi nowicjusz.\n");
	}

	/* Though expel everyone. */
	add_wizard_to_domain("", members[index], cmder);
	write("Wyrzucono: " + capitalize(members[index]) + ".\n");
    }

    /* Remove all sanctions given out to or received by the domain. */
    remove_all_sanctions(dname);

    /* Move the domain directory to the discarded domain directory. */
    mkdir(DISCARD_DOMAIN_DIR);
    if (!rename("/d/" + dname, DISCARD_DOMAIN_DIR + "/" + dname))
    {
	write("Nie uda^lo si^e przenie^s^c katalogu domeny do " +
	    DISCARD_DOMAIN_DIR + ".\n");
	return 1;
    }

    /* Delete the domain from the domain mapping. */
    m_domains = m_delete(m_domains, dname);
    save_master();

    write("Domena " + dname + " zosta^la usuni^eta.\n");
    return 1;
}

/*
 * Nazwa funkcji: tell_domain
 * Opis         : Tell something to all in the domain save one, who gets a
 *                special message.
 * Argumenty    : string dname - the domain.
 *		  string wname - special message to this wiz.
 *		  string wmess - wizard message.
 *		  string dmess - domain message.
 */
static void
tell_domain(string dname, string wname, string wmess, string dmess)
{
    object wiz;
    string *wlist;
    int    size;

    if (objectp(wiz = find_player(wname)))
    {
	tell_object(wiz, wmess);
    }

    wlist = (string *)m_domains[dname][FOB_DOM_MEMBERS] - ({ wname });
    size = sizeof(wlist);
    while(--size >= 0)
    {
	if (objectp(wiz = find_player(wlist[size])))
	{
	    tell_object(wiz, dmess);
	}
    }
}

/************************************************************************
 *
 * The madwand administration code.
 *
 */

/*
 * Nazwa funkcji: transform_mortal_into_wizard
 * Opis         : This function gives the mortal the apprentice scroll and
 *                alters the start location of the player, both if they
 *                player is logged in or now.
 * Argumenty    : string wname - the name of the mortal.
 *                string cmder - the name of the wizard doing the honour.
 */
static void
transform_mortal_into_wizard(string wname, string cmder)
{
    object  wizard;
    object  scroll, scroll2, scroll3;
    object *players;
    mapping playerfile;
    int     fingered;

    /* Update the wizard-mapping. This just adds an empty slot for the
     * player. He isn't apprentice yet.
     */
    m_wizards[wname] = ({ WIZ_MORTAL, WIZ_RANK_START_LEVEL(WIZ_MORTAL),
        cmder, "", cmder });

    save_master();

    if (objectp(wizard = find_player(wname)))
    {
        wizard->set_notify(19); // AWI, del

        if (catch(scroll = clone_object(APPRENTICE_SCROLL_FILE)))
        {
            write("B^l^ad przy klonowaniu powitania ("+
                    APPRENTICE_SCROLL_FILE+").\n");
            tell_object(wizard, "B^l^ad przy klonowaniu apprentice scroll'a.\n");
        }
        else if(catch(scroll2 = clone_object(APPRENTICE_SCROLL2_FILE)))
        {
            write("B^l^ad przy klonowaniu zwoju dla pocz^atkuj^acych "+
                    "czarodziej^ow. ("+APPRENTICE_SCROLL2_FILE+")\n");
            tell_object(wizard, "B^l^ad przy klonowaniu zwoju dla "+
                        "pocz^atkuj^acych czarodziej^ow\n");
        }
        else if(catch(scroll3 = clone_object(APPRENTICE_SCROLL3_FILE)))
        {
            write("B^l^ad przy klonowaniu zwoju z zasadami dla "+
                    "czarodziej^ow. ("+APPRENTICE_SCROLL3_FILE+")\n");
            tell_object(wizard, "B^l^ad przy klonowaniu zwoju z zasadami dla "+
                        "czarodziej^ow\n");
        }
        else
        {
            write(capitalize(wname) + " otrzyma^l" + wizard->koncowka("", "a", "o") +
                "powitanie oraz zwoje ze wskaz^owkami.\n");
            tell_object(wizard, "\n\nDosta^l" + wizard->koncowka("e^s", "a^s", "o^s") +
                " zw^oje zawieraj^ace bardzo cenne informacje. "+
                    "Koniecznie je przeczytaj!\n");
            scroll->move(wizard, 1);
            scroll2->move(wizard, 1);
            scroll3->move(wizard, 1);
            wizard->command("przeczytaj ma^ly srebrzysty zw^oj");
        }
        wizard->set_wiz_level();
        wizard->set_default_start_location(WIZ_ROOM);
        wizard->save_me();
    }
    else
    {
        playerfile = restore_map(PLAYER_FILE(wname));

        if (!pointerp(playerfile["auto_load"]))
            playerfile["auto_load"] = ({ });

        playerfile["notify"] = 19; // AWI, del
        playerfile["auto_load"] += ({ APPRENTICE_SCROLL_FILE,
                                      APPRENTICE_SCROLL2_FILE,
                                      APPRENTICE_SCROLL3_FILE });
        playerfile["default_start_location"] = WIZ_ROOM;

        save_map(playerfile, PLAYER_FILE(wname));

        fingered = 1;
        wizard = SECURITY->finger_player(wname);
    }

    players = users() - ({ this_interactive(), wizard });
    players -= QUEUE->queue_list(0);

           //jakiej krainy niesmiertelnych? :P zmieniam. Mam nadzieje,
           //ze nowy komunikat sie spodoba ;) V.

    /*players->catch_tell("Niebo roz^swietla szkar^latny blask rozdartej " +
        "osnowy magii. Zdajesz sobie spraw^e, ^ze " + capitalize(wname) +
        " wst^api^l" + (wizard->query_gender() == G_MALE ? "" : "a") +
        " do Krainy Nie^smiertelnych.\n");*/
      players->catch_tell("Z oddali s�ycha� dono�ny g�os tr�bki i go�ca "+
         "wydzieraj�cego si� wniebog�osy: Pamparampam! "+capitalize(wname)+
	 " w�a�nie wst�pi�"+(wizard->query_gender()==G_MALE?"":"a")+
	 " do Akademii Czarodziej�w z Aretuzy!\n");

    SECURITY->log_public("NEW_WIZ",
        sprintf("%s   %-11s   Avg = %3d; Age = %3d\n", ctime(time()),
        capitalize(wname), wizard->query_average_stat(),
        (wizard->query_age() / 43200)), -1);

    /* Clean up after ourselves. */
    if (fingered)
    {
	do_debug("destroy", wizard);
    }
}

#ifdef FOB_MADWAND_SUPPORT
/*
 * Nazwa funkcji: draft_madwand
 * Opis         : This function is used to draft a madwand to a domain.
 *                A madwand is a mortal player who is changed into a
 *                wizard, linked to the domain (s)he is added to. When
 *                expelled from the domain, the madwand reverts to mortal
 *                again with the previous status.
 * Argumenty    : string dname - the domain to draft the madwand to.
 *                string wname - the mortal to make wizard.
 *                string cmder - who does it.
 * Zwraca       : int 1/0 - success/failure.
 */
static int
draft_madwand(string dname, string wname, string cmder)
{
    object player;

    /* Only mortals can be made into madwand. */
    if (m_wizards[wname])
    {
	notify_fail("Tylko �miertelnika mo^zna zmieni^c w madwanda. " +
		    capitalize(wname) + " jest czarodziejem.\n");
	return 0;
    }

    /* No such domain. */
    if (!m_domains[dname])
    {
	notify_fail("Nie istnieje domena " + dname + ".\n");
	return 0;
    }

    /* Domain already has a madwand. */
    if (strlen(m_domains[dname][FOB_DOM_MADWAND]))
    {
	notify_fail("Domena " + dname + " ma ju^z madwanda, kt^orym jest " +
		    capitalize(m_domains[dname][FOB_DOM_MADWAND]) + ".\n");
	return 0;
    }

    /* Domain is full. */
    if (sizeof(m_domains[dname][FOB_DOM_MEMBERS]) >=
	m_domains[dname][FOB_DOM_MAXSIZE])
    {
	notify_fail("Domena " + dname + " jest ju^z pe^lna.\n");
	return 0;
    }

    if (!objectp(player = find_player(wname)))
    {
	write("Gracz musi by^c zalogowany, ^zeby da^lo si^e go uczyni^c madwandem.\n");
	return 1;
    }

    /* First save the mortals playerfile in a name.madwand */
    set_auth(this_object(), "root:root");
    rename(PLAYER_FILE(wname) + ".o", MADWAND_FILE(wname));

    /* Then make the mortal into a wizard. Note that this alone does not
     * make the mortal into apprentice, but that isn't necessary. We can
     * put him into the domain just like that.
     */
    transform_mortal_into_wizard(wname, cmder);

    /* Mark the wizard as madwand and make him a trainee. */
    m_domains[dname][FOB_DOM_MADWAND] = wname;

    /* Then draft the mortal wizard into the domain. First we make him/her
     * into a normal wizard though ;-)
     */
    do_change_rank(wname, WIZ_NORMAL, cmder);
    add_wizard_to_domain(dname, wname, cmder);

    log_file("MADWAND", ctime(time()) + " " + capitalize(wname) +
	     " drafted to " + dname + " by " + capitalize(cmder) + ".\n", -1);
    return 1;
}

/*
 * Nazwa funkcji: expel_madwand
 * Opis         : Expel a madwand from the domain (s)he is a member of.
 *                This will turn the wizard back into the mortal (s)he
 *                was before joining a domain as wizard.
 * Argumenty    : string wname - the madwand to turn into mortal again.
 *                string cmder - the command giver.
 * Zwraca       : int 1/0 - success/failure.
 */
static int
expel_madwand(string wname, string cmder)
{
    string dname;
    object wiz;

    /* Remove the wizard from the domain. */
    dname = m_wizards[wname][FOB_WIZ_DOM];
    add_wizard_to_domain("", wname, cmder);

    /* Unmark the madwand as being the madwand of the domain. Then
     * remove him/her from the wizard-list.
     */
    m_domains[dname][FOB_DOM_MADWAND] = "";
    m_wizards = m_delete(m_wizards, wname);
    save_master();

    /* Remove all possible sanctions by the soon to be former madwand. */
    remove_all_sanctions(wname);

    /* Give the player the notice and then boot him/her. */
    if (objectp(wiz = find_player(wname)))
    {
	tell_object(wiz, "W momencie opuszczenia domeny " + dname +
		    " zostajesz z powrotem ^smiertelni" +
		    wiz->koncowka("kiem", "czk^a") + ".\n");
	wiz->quit();

	if (objectp(wiz))
	{
	    do_debug("destroy", wiz);
	}
    }

    /* Try to restore the mortal playerfile. */
    if (file_size(MADWAND_FILE(wname)) > 0)
    {
	rename(MADWAND_FILE(wname), PLAYER_FILE(wname) + ".o");
    }

    log_file("MADWAND", ctime(time()) + " " + capitalize(wname) +
	     " expelled from " + dname + " by " + capitalize(cmder) + "\n",
	     -1);
    return 1;
}

/*
 * Nazwa funkcji: madwand_to_wizard
 * Opis         : With this function a madwand can be changed into a normal
 *                wizard when he or she has proven worthy. This way the
 *                madwand flag can be removed without having to demote,
 *                expel, and squeeze the wizard.
 * Argumenty    : string wname - the name of the wizard.
 * Zwraca       : int 1/0 - success/failure.
 */
int
madwand_to_wizard(string wname)
{
    string cmder;
    string dname;

    /* May only be called from the arch soul. */
    if (!CALL_BY(WIZ_CMD_ARCH))
    {
	return 0;
    }

    cmder = getwho();
    wname = lower_case(wname);
    if (!m_wizards[wname])
    {
	notify_fail("Nie ma czarodzieja o takim imieniu.\n");
	return 0;
    }

    dname = m_wizards[wname][FOB_WIZ_DOM];
    if (m_domains[dname][FOB_DOM_MADWAND] != wname)
    {
	write(capitalize(wname) + " nie jest madwandem domeny " +
	      dname + ".\n");
	return 1;
    }

    /* Remove the old madwand save-file if there is one. */
    if (file_size(MADWAND_FILE(wname)) >= 0)
    {
	rm(MADWAND_FILE(wname));
    }

    /* Unmark the wizard as madwand. */
    m_domains[dname][FOB_DOM_MADWAND] = "";
    save_master();
    log_file("MADWAND", ctime(time()) + " " + FORMAT_NAME(capitalize(wname)) +
	     " made into a real wizard by " + capitalize(cmder) + ".\n", -1);
    return 1;
}
#endif FOB_MADWAND_SUPPORT

/************************************************************************
 *
 * The wizard administration code.
 *
 */

/*
 * Nazwa funkcji: add_wizard_to_domain
 * Opis         : Add a wizards to a domain and removes the wizard from his/
 *                her pervious domain. If you add a wizard to the domain "",
 *                it removes the wizard from his current domain.
 *                This is an interal function only that just adds or removes
 *                the people without additional checks. Those must have been
 *                made earlier. Note that this function does not alter the
 *                rank of the wizard. This must have been altered before.
 * Argumenty    : string dname - the domain to add the wizard to. If "",
 *                               the wizard is removed from his/her domain.
 *		  string wname - the wizard to add/remove.
 *		  string cmder - who does it.
 * Zwraca       : int 1/0 - success/failure.
 */
static int
add_wizard_to_domain(string dname, string wname, string cmder)
{
    string old_domain;
    string old_dir;
    string new_dir;

    dname = capitalize(dname);
    wname = lower_case(wname);
    old_domain = m_wizards[wname][FOB_WIZ_DOM];

    /* Mages, arches and keepers cannot be without a domain. They will be
     * moved to WIZARD_DOMAIN if you try to remove them from their domain.
     */
    if ((!strlen(dname)) &&
	((m_wizards[wname][FOB_WIZ_RANK] == WIZ_MAGE) ||
	 (m_wizards[wname][FOB_WIZ_RANK] >= WIZ_ARCH)))
    {
	dname = WIZARD_DOMAIN;
    }

    m_wizards[wname][FOB_WIZ_DOM] = dname;
    m_wizards[wname][FOB_WIZ_CHDOM] = allocate(2);
    m_wizards[wname][FOB_WIZ_CHDOM][FOB_WIZ_CHDOM_MIA] = cmder;
    m_wizards[wname][FOB_WIZ_CHDOM][FOB_WIZ_CHDOM_BIE] =
	    find_player(cmder)->query_real_name(3); // W bierniku
#ifdef FOB_KEEP_CHANGE_TIME
    m_wizards[wname][FOB_WIZ_CHDTIME] = time();
#endif FOB_KEEP_CHANGE_TIME

    /* If the person leaves an old domain, update the membership and tell
     * the people.
     */
    if (strlen(old_domain))
    {
	m_domains[old_domain][FOB_DOM_MEMBERS] -= ({ wname });
	m_trainees = m_delete(m_trainees, wname);

        tell_domain(old_domain, wname, ("Nie jeste^s ju^z cz^lonkiem domeny " +
            old_domain + ".\n"), (capitalize(wname) +
            " nie jest ju^z cz^lonkem domeny " + old_domain + ".\n"));
    }
    else
    {
	old_domain = WIZARD_DOMAIN;
    }

    /* Leaving the domain and not joining another domain. */
    if (!strlen(dname))
    {
	save_master();

	return 1;
    }

    /* Joining a new domain means no more applications. */
    remove_all_applications(wname);

    /* Add him/her to the domain-list. */
    m_domains[dname][FOB_DOM_MEMBERS] += ({ wname });

    /* Mark him as trainee if necessary. */
    if ((dname != WIZARD_DOMAIN) &&
	(m_wizards[wname][FOB_WIZ_RANK] == WIZ_NORMAL))
    {
	m_trainees[wname] = 1;
    }

    tell_domain(dname, wname, ("Jeste^s teraz cz^lonkiem domeny " + dname +".\n"),
		capitalize(wname) + " jest teraz cz^lonkiem domeny " +
		dname + ".\n");

    new_dir = "/d/" + dname + "/" + wname;
    old_dir = "/d/" + old_domain + "/" + wname;

    set_auth(this_object(), "root:root");

    /* If there isn't a directory yet, create one. */
    if (file_size(new_dir) != -2)
    {
	/* This isn't nice, but the player still is a new member of the
	 * domain. That hasn't changed.
	 */
	if (file_size(new_dir) != -1)
	{
	    write("Nie uda^lo si^e utworzy^c katalogu " + new_dir +
		  ", bo istnieje ju^z plik o tej nazwie.\n");
	    return 1;
	}

	/* If there is a directory in the old domain, move it if it is in
	 * WIZARD_DOMAIN. We don't want to move domain-directories from
	 * normal domains.
	 */
	if ((file_size(old_dir) == -2) &&
	    (old_domain == WIZARD_DOMAIN))
	{
	    rename(old_dir, new_dir);
	}
	else
	{
	    mkdir(new_dir);
	}
    }
    /* This one is also joining a domain, but apperently he was a member
     * before because there is already a directory with his/her name. If
     * possible, we move the private directory.
     */
    else if ((file_size(old_dir + "/private") == -2) &&
	     (file_size(new_dir + "/private") == -1))
    {
	rename(old_dir + "/private", new_dir);
    }

    return 1;
}

/*
 * Nazwa funkcji: draft_wizard_to_domain
 * Opis         : With this function an archwizard or keeper can draft a
 *                wizard to a domain. This command can also be used to draft
 *                madwands.
 * Argumenty    : string dname - the domain to draft the wizard to.
 *                string wname - the wizard to draft.
 * Zwraca       : int 1/0 - success/failure.
 */
int
draft_wizard_to_domain(string dname, string wname)
{
    string cmder;

    /* May only be called from the arch soul. */
    if (!CALL_BY(WIZ_CMD_ARCH))
    {
	return 0;
    }

    cmder = getwho();
    dname = capitalize(dname);
    wname = lower_case(wname);

    if (!m_domains[dname])
    {
        notify_fail("Nie ma domeny o nazwie '" + dname + "'.\n");
        return 0;
    }

    /* No vacancies in the domain. */
    if (sizeof(m_domains[dname][FOB_DOM_MEMBERS]) >=
	m_domains[dname][FOB_DOM_MAXSIZE])
    {
	notify_fail("Nie ma wolnych miejsc w domenie " + dname + ".\n");
	return 0;
    }

    /* Drafting a madwand */
    if (!sizeof(m_wizards[wname]))
    {
#ifdef FOB_MADWAND_SUPPORT
	return draft_madwand(dname, wname, cmder);
#else
	notify_fail("Na tym mudzie nie da si^e tworzy^c madwand^ow.\n");
	return 0;
#endif FOB_MADWAND_SUPPORT
    }

    /* Don't draft people who are in another domain. */
    if (strlen(m_wizards[wname][FOB_WIZ_DOM]) &&
	(m_wizards[wname][FOB_WIZ_DOM] != WIZARD_DOMAIN))
    {
	notify_fail(capitalize(wname) + " ju^z jest cz^lonkiem domeny " +
		    m_wizards[wname][FOB_WIZ_DOM] + ".\n");
	return 0;
    }

    /* Only draft apprentices. */
    if ((m_wizards[wname][FOB_WIZ_RANK] != WIZ_APPRENTICE) &&
	(m_wizards[wname][FOB_WIZ_RANK] < WIZ_NORMAL))
    {
	notify_fail(capitalize(wname) + " nie jest apprentice'em.\n");
	return 0;
    }

    /* Apprentices should be made full wizard. */
    if (m_wizards[wname][FOB_WIZ_RANK] == WIZ_APPRENTICE)
    {
	do_change_rank(wname, WIZ_NORMAL, cmder);
    }

    write("W^l^aczono czarodzieja " + capitalize(wname) + " do domeny " +
	dname + ".\n");
    return add_wizard_to_domain(dname, wname, cmder);
}

/*
 * Nazwa funkcji: expel_wizard_from_domain
 * Opis         : Expel a wizard from a domain.
 * Argumenty    : string wname - the wizard to expel.
 * Zwraca       : int 1/0 - success/failure.
 */
int
expel_wizard_from_domain(string wname)
{
    string cmder;

    /* May only be called from the Lord soul. */
    if (!CALL_BY(WIZ_CMD_LORD))
    {
	return 0;
    }

    wname = lower_case(wname);
    if (!strlen(wname))
    {
	notify_fail("Wyrzuci^c kogo?\n");
	return 0;
    }

    if (!sizeof(m_wizards[wname]))
    {
	notify_fail(capitalize(wname) + " jest ^smiertelnikiem!\n");
	return 0;
    }

    if (!strlen(m_wizards[wname][FOB_WIZ_DOM]))
    {
	notify_fail(capitalize(wname) + " nie jest cz^lonkiem ^zadnej domeny.\n");
	return 0;
    }

    /* Lords may only boot their own domain members. Use <= WIZ_LORD to
     * also include expelling stewards.
     */
    cmder = getwho();
    if ((m_wizards[cmder][FOB_WIZ_RANK] <= WIZ_LORD) &&
	(m_wizards[wname][FOB_WIZ_DOM] != m_wizards[cmder][FOB_WIZ_DOM]))
    {
	write(capitalize(wname) + " nie jest cz^lonkiem twojej domeny " +
	      m_wizards[wname][FOB_WIZ_DOM] + ".\n");
	return 1;
    }

    /* Try to add mages, arches and keepers to WIZARD_DOMAIN. */
    if ((m_wizards[wname][FOB_WIZ_RANK] == WIZ_MAGE) ||
	(m_wizards[wname][FOB_WIZ_RANK] >= WIZ_ARCH))
    {
	if (m_wizards[wname][FOB_WIZ_DOM] == WIZARD_DOMAIN)
	{
	    write(capitalize(wname) + " nie mo^ze opu^sci^c domeny " +
		  WIZARD_DOMAIN + ".\n" );
	    return 1;
	}

	return add_wizard_to_domain(WIZARD_DOMAIN, wname, cmder);
    }

    /* If the player was a madwand, kick him out via another funciton.
     * When madwand support is turned off, just unmark him as madwand
     * and expel him the normal way.
     */
    if (m_domains[m_wizards[wname][FOB_WIZ_DOM]][FOB_DOM_MADWAND] == wname)
    {
#ifdef FOB_MADWAND_SUPPORT
	return expel_madwand(wname, cmder);
#else
	m_domains[m_wizards[wname][FOB_WIZ_DOM]][FOB_DOM_MADWAND] = 0;
#endif FOB_MADWAND_SUPPORT
    }

    /* Else, demote them and kick them out of the domain. */
    do_change_rank(wname, WIZ_APPRENTICE, cmder);
    return add_wizard_to_domain("", wname, cmder);
}

/*
 * Nazwa funkcji: leave_domain
 * Opis         : Someone wants to leave his/her domain.
 * Zwraca       : int 1/0 - success/failure.
 */
int
leave_domain()
{
    string cmder;

    /* May only be called from the normal wizards soul. */
    if (!CALL_BY(WIZ_CMD_NORMAL))
    {
	return 0;
    }

    /* Mages, arches and keepers cannot leave WIZARD_DOMAIN. */
    cmder = getwho();
    if ((m_wizards[cmder][FOB_WIZ_DOM] == WIZARD_DOMAIN) &&
	((m_wizards[cmder][FOB_WIZ_RANK] >= WIZ_ARCH) ||
	 (m_wizards[cmder][FOB_WIZ_RANK] == WIZ_MAGE)))
    {
	write("Nie mo^zesz opu^sci^c domeny " + WIZARD_DOMAIN + ".\n");
	return 1;
    }

    /* Domain-member or not? */
    if (!strlen(m_wizards[cmder][FOB_WIZ_DOM]))
    {
	write("Nie jeste^s cz^lonkiem ^zadnej domeny.\n");
	return 1;
    }

    /* Madwands have their own expelling function. When madwand support is
     * not turned on, just unmark the madwand and expel him the normal way.
     * This turns the madwand into a normal wizard.
     */
    if (m_domains[m_wizards[cmder][FOB_WIZ_DOM]][FOB_DOM_MADWAND] == cmder)
    {
#ifdef FOB_MADWAND_SUPPORT
	return expel_madwand(cmder, cmder);
#else
	m_domains[m_wizards[cmder][FOB_WIZ_DOM]][FOB_DOM_MADWAND] = "";
#endif FOB_MADWAND_SUPPORT
    }

    /* Demote the wizard if he isn't a mage, arch or keeper. */
    if ((m_wizards[cmder][FOB_WIZ_RANK] != WIZ_MAGE) &&
	(m_wizards[cmder][FOB_WIZ_RANK] < WIZ_ARCH))
    {
	do_change_rank(cmder, WIZ_APPRENTICE, cmder);
    }

    /* Add the wizard to the empty domain, i.e leave the domain. */
    return add_wizard_to_domain("", cmder, cmder);
}

/*
 * Nazwa funkcji: rename_wizard
 * Opis         : A service function to update the name of the wizard in
 *                case a wizard changes his/her name. It makes no validity
 *                checks.
 * Argumenty    : string oldname - the old name of the wizard.
 *                string newname - the new name of the wizard.
 */
static int
rename_wizard(string oldname, string newname)
{
    string dname = m_wizards[oldname][FOB_WIZ_DOM];

    /* Rename the wizard in the wizard mapping. */
    write("Przeniesiono status czarodzieja na " + capitalize(newname) + ".\n");
    m_wizards[newname] = secure_var(m_wizards[oldname]);
    m_wizards = m_delete(m_wizards, oldname);

    /* Update trainee status. */
    if (m_trainees[oldname])
    {
        write("Przeniesiono status praktykanta na " + capitalize(newname) +
	    ".\n");
        m_trainees[newname] = 1;
        m_trainees = m_delete(m_trainees, oldname);
    }

    /* Update global read. */
    if (m_global_read[oldname])
    {
        write("Przeniesiono prawa globalnego odczytu na " +
	    capitalize(newname) + ".\n");
        m_global_read[newname] = secure_var(m_global_read[oldname]);
        m_global_read = m_delete(m_global_read, oldname);
    }

    /* Update domain information. */
    if (strlen(dname))
    {
        write("Uaktualniono przynale^zno^s^c do domeny czarodzieja " +
	    capitalize(newname) + ".\n");
        m_domains[dname][FOB_DOM_MEMBERS] -= ({ oldname });
        m_domains[dname][FOB_DOM_MEMBERS] += ({ newname });

        switch(m_wizards[newname][FOB_WIZ_RANK])
        {
        case WIZ_STEWARD:
            m_domains[dname][FOB_DOM_STEWARD] = newname;
            break;

        case WIZ_LORD:
            m_domains[dname][FOB_DOM_LORD] = newname;
            break;
        }

        if (rename(("/d/" + dname + "/" + oldname),
               ("/d/" + dname + "/" + newname)))
        {
            write("Katalog domowy przeniesiony.\n");
        }
        else
        {
            write("Nie uda^lo si^e przenie^s^c katalogu domowego.\n");
        }
    }

    save_master();
    log_file("LEVEL",
        sprintf("%s %-11s: renamed to %-11s by %s.\n",
        ctime(time()),
        capitalize(oldname),
        capitalize(newname),
        capitalize(this_interactive()->query_real_name())));
}

/************************************************************************
 *
 * The applications administration code.
 *
 */

/*
 * Nazwa funkcji: apply_to_domain
 * Opis         : Apply to a domain.
 * Argumenty    : string dname - domain the application goes to.
 * Zwraca       : int 1/0 - success/failure.
 */
int
apply_to_domain(string dname)
{
    string wname;
    string *appl;
    object wiz;

    /* May only be called from the apprentice soul. */
    if (!CALL_BY(WIZ_CMD_APPRENTICE))
    {
	return 0;
    }

    if (!strlen(dname))
    {
	notify_fail("Do kt^orej domeny chcesz si^e ubiega^c?\n");
	return 0;
    }

    dname = capitalize(dname);
    wname = getwho();

    switch(m_wizards[wname][FOB_WIZ_RANK])
    {
    case WIZ_MAGE:
    case WIZ_ARCH:
    case WIZ_KEEPER:
	if (m_wizards[wname][FOB_WIZ_DOM] != WIZARD_DOMAIN)
	{
	    notify_fail("Jeste^s ju^z cz^lonkiem domeny " +
			m_wizards[wname][FOB_WIZ_DOM] + ".");
	    return 0;
	}
	break;

    case WIZ_PILGRIM:
	notify_fail("Misj^a pielgrzyma jest w^edr^owka po tym ^swiecie; " +
		    "nie mo^zesz nigdzie osi^a^s^c na sta^le....\n");
	return 0;
	break;

    default:
	if (m_wizards[wname][FOB_WIZ_DOM] != "")
	{
	    notify_fail("Jeste^s ju^z cz^lonkiem domeny " +
			m_wizards[wname][FOB_WIZ_DOM] + ".\n");
	    return 0;
	}
	break;
    }

    if (!sizeof(m_domains[dname]))
    {
	notify_fail("Nie istnieje domena '" + dname + "'.\n");
	return 0;
    }

    /* See if there is an array of people for that domain already. */
    if (!pointerp(m_applications[dname]))
    {
	m_applications[dname] = ({ });
    }
    /* else see if the player already applied. */
    else if (member_array(wname, m_applications[dname]) != -1)
    {
	write("Ju^z z^lo^zy^l" + find_player(wname)->koncowka("e^s", "a^s") +
	    " podanie do domeny " + dname + "!\n");
	return 1;
    }

    /* See whether there is room in the domain. This does not terminate
     * the application process if it fails.
     */
    if (sizeof(m_domains[dname][FOB_DOM_MEMBERS]) >=
	m_domains[dname][FOB_DOM_MAXSIZE])
    {
	write("Domena " + dname + " jest ju^z pe^lna, ale mimo to twoje " +
	      "podanie zosta^lo zarejestrowane.\n");
    }

    /* Add the person to the list of people who applied. */
    m_applications[dname] += ({ wname });
    save_master();

    /* Tell the wizard and the members in the domain. */
    tell_domain(dname, wname, ("Z^lo^zy^l" + find_player(wname)->koncowka("e^s","a^s") +
		" podanie do domeny " + dname + ".\n"),
		(capitalize(wname) + " z^lo^zy^l" +
		find_player(wname)->koncowka("", "a") + " podanie do domeny " +
		 dname + ".\n"));

    return 1;
}

/*
 * Nazwa funkcji: accept_application
 * Opis         : A lord accepts an application. This function can also
 *                be used by the Liege to draft a madwand into the domain.
 * Argumenty    : string wname - the wizard to accept.
 * Zwraca       : int 1/0 - success/failure.
 */
int
accept_application(string wname)
{
    string dname;
    string cmder;

    /* May only be called from the Lord soul. */
    if (!CALL_BY(WIZ_CMD_LORD))
    {
	return 0;
    }

    if (!strlen(wname))
    {
	notify_fail("Przyjmij kogo?\n");
	return 0;
    }

    cmder = getwho();
    if (m_wizards[cmder][FOB_WIZ_RANK] > WIZ_LORD)
    {
	write("Nie jeste^s lordem ^zadnej domeny!\n");
	return 1;
    }

    wname = lower_case(wname);
    dname = m_wizards[cmder][FOB_WIZ_DOM];

    if (sizeof(m_domains[dname][FOB_DOM_MEMBERS]) >=
	m_domains[dname][FOB_DOM_MAXSIZE])
    {
	notify_fail("Domena " + dname + " nie ma ju^z wolnych miejsc.\n");
	return 0;
    }

    /* When the draftee isn't a wizard, this means the Lord wants to draft
     * the mortal as a madwand.
     */
    if (!m_wizards[wname])
    {
#ifdef FOB_MADWAND_SUPPORT
	return draft_madwand(dname, wname, cmder);
#else
	notify_fail("Na tym mudzie nie da si^e tworzy^c madwand^ow.\n");
	return 0;
#endif FOB_MADWAND_SUPPORT
    }

    if (!pointerp(m_applications[dname]))
    {
	write("Nikt nie z^lo^zy^l podania do twojej domeny.\n");
	return 1;
    }
    else if (member_array(wname, m_applications[dname]) == -1)
    {
	write(capitalize(wname) + " nie z^lo^zy^l(a) podania do twojej domeny.\n");
	return 1;
    }

    if ((m_wizards[wname][FOB_WIZ_RANK] != WIZ_APPRENTICE) &&
	 (m_wizards[wname][FOB_WIZ_DOM] != WIZARD_DOMAIN))
    {
	notify_fail(capitalize(wname) +
		    " nie jest Rekrutem, ani cz^lonkiem domeny " +
		    WIZARD_DOMAIN + ".\n");
	return 0;
    }

    /* Joining means no more applications. */
    remove_all_applications(wname);

    /* People who aren't a wizard already should become full wizard. */
    if (m_wizards[wname][FOB_WIZ_RANK] == WIZ_APPRENTICE)
    {
	do_change_rank(wname, WIZ_NORMAL, cmder);
    }

    return add_wizard_to_domain(dname, wname, cmder);
}

/*
 * Nazwa funkcji: deny_application
 * Opis         : The lord denies an application of a wizard.
 * Argumenty    : string wname - the wizard name.
 * Zwraca       : int 1/0 - success/failure.
 */
int
deny_application(string wname)
{
    string dname;
    string cmder;
    object wiz;

    /* May only be called from the Lord soul. */
    if (!CALL_BY(WIZ_CMD_LORD))
    {
	return 0;
    }

    cmder = getwho();
    if (m_wizards[cmder][FOB_WIZ_RANK] > WIZ_LORD)
    {
	notify_fail("Nie jeste^s Lordem ani Seniorem ^zadnej domeny!\n");
	return 0;
    }

    wname = lower_case(wname);
    dname = m_wizards[cmder][FOB_WIZ_DOM];

    if (!pointerp(m_applications[dname]))
    {
	write("Nikt nie z^lo^zy^l podania do twojej domeny.\n");
	return 1;
    }
    else if (member_array(wname, m_applications[dname]) == -1)
    {
	write(capitalize(wname) + " nie z^lo^zy^l(a) podania do domeny " +
	    dname + ".\n");
	return 1;
    }

    /* Remove the application. */
    m_applications[dname] -= ({ wname });

    /* Remove the domain-entry if this was the last application. */
    if (!sizeof(m_applications[dname]))
    {
        m_applications = m_delete(m_applications, dname);
    }

    if (objectp(wiz = find_player(wname)))
    {
	tell_object(wiz, "Twoje podanie do domeny " + dname +
		    " zosta^lo odrzucone.\n");
    }

    write("Wycofa^l" + this_player()->koncowka("e^s", "a^s") +
	" podanie czarodzieja " + capitalize(wname) + " do domeny " +
	dname + ".\n");
    return 1;
}

/*
 * Nazwa funkcji: regret_application
 * Opis         : A wizard regrets an application to a domain.
 * Argumenty    : string dname - the domain name.
 * Zwraca       : int 1/0 - success/failure.
 */
int
regret_application(string dname)
{
    string wname;

    /* May only be called from the apprentice soul. */
    if (!CALL_BY(WIZ_CMD_APPRENTICE))
    {
	return 0;
    }

    if (!strlen(dname))
    {
	notify_fail("Podanie do kt^orej domeny chcesz wycofa^c?");
	return 0;
    }

    wname = getwho();
    dname = capitalize(dname);

    /* See if the wizard applied to the domain. */
    if (member_array(wname, m_applications[dname]) == -1)
    {
	notify_fail("Nie sk^lada^l" + this_player()->koncowka("e^s", "a^s") +
	    " podania do domeny " + dname + ".\n");
	return 0;
    }

    /* Remove the application from the list. */
    m_applications[dname] -= ({ wname });

    /* If there are no other applications, remove the empty array. */
    if (!sizeof(m_applications[dname]))
    {
	m_applications = m_delete(m_applications, dname);
    }

    save_master();

    tell_domain(dname, wname, ("Wycofujesz podanie do domeny " + dname +
        ".\n"), (capitalize(wname) + " wycofuje podanie do domeny " +
        dname + ".\n"));
    return 1;
}

/*
 * Nazwa funkcji: remove_all_applications
 * Opis         : Remove all application by a wizard from the application list.
 * Argumenty    : string wname - the name of the wizard in lower case.
 */
static void
remove_all_applications(string wname)
{
    string *domains;
    int    index;
    int    size;

    domains = m_indices(m_applications);

    index = -1;
    size  = sizeof(domains);
    while(++index < size)
    {
	/* Remove the application from the wizard if it exists. */
        if (pointerp(m_applications[domains[index]]))
        {
            write("Usuwam podania czarodzieja " + capitalize(wname) + " do " +
                domains[index] + ".\n");
  	    m_applications[domains[index]] -= ({ wname });

  	    /* If this was the last wizard, remove the domain from the list. */
	    if (!sizeof(m_applications[domains[index]]))
 	    {
	        m_applications = m_delete(m_applications, domains[index]);
 	    }
        }
    }

    /* Save the master object. */
    save_master();
}

/*
 * Nazwa funkcji: filter_applications
 * Opis         : This function checks whether a particular wizard is in the
 *                list of people who have applied to a domain.
 * Argumenty    : string *wizards - the people who have applied to a domain.
 *                string wname    - the wizard to check.
 * Zwraca       : int 1/0 - true if the wizard applied to the domain.
 */
static int
filter_applications(string *wizards, string wname)
{
    return (member_array(wname, wizards) != -1);
}

/*
 * Nazwa funkcji: list_applications_by_wizard
 * Opis         : This function lists all applications made by a wizard.
 * Argumenty    : string wname  - the name of the wizard to list.
 *                int list_self - true if someone inquires about himself.
 * Zwraca       : int 1/0 - success/failure.
 */
static int
list_applications_by_wizard(string wname, int list_self)
{
    mapping domains;

    wname = lower_case(wname);
    domains = filter(m_applications, &filter_applications(, wname));

    wname = (list_self ? "Masz" : (capitalize(wname) + " ma"));

    if (!m_sizeof(domains))
    {
	write((list_self ? "Nie masz" : (capitalize(wname)) + " nie ma") +
	    " z^lo^zonych poda^n do ^zadnej domeny.\n");
	return 1;
    }

    write(wname + " z^lo^zone podania do " +
	  COMPOSITE_WORDS(m_indices(domains)) + ".\n");
    return 1;
}

/*
 * Nazwa funkcji: list_applications
 * Opis         : List applications. Domain-members may see the people that
 *                have applied to the domain. Non-domain members may see
 *                only their own applications. Arches and keepers may see
 *                all applications.
 * Argumenty    : string str - the command line argument.
 * Zwraca       : int 1/0 - success/failure.
 */
int
list_applications(string str)
{
    string cmder;
    string dname;
    string *words;
    int    index;
    int    size;
    int    rank;

    cmder = getwho();
    if (!strlen(cmder))
    {
	notify_fail("Nie masz uprawnie^n, by to zrobi^c.\n");
	return 0;
    }

    rank = m_wizards[cmder][FOB_WIZ_RANK];

    /* Arches or keepers. */
    if (rank >= WIZ_ARCH)
    {
	/* Basic operation: list all applications. */
	if (!strlen(str))
	{
	    if (!m_sizeof(m_applications))
	    {
		write("Nie ma ^zadnych poda^n.\n");
		return 1;
	    }

	    words = m_indices(m_applications);
	    index = -1;
	    size = sizeof(words);
	    write("Domena      Podania\n----------- ------------\n");

	    while(++index < size)
	    {
                write(FORMAT_NAME(words[index]) + " " +
                    COMPOSITE_WORDS(sort_array(map(m_applications[words[index]],
                    capitalize))) + "\n");
	    }
	    return 1;
	}

	words = explode(str, " ");
        if ((sizeof(words) != 2) || (words[0] != "clear"))
        {
            notify_fail("Sk^ladnia: applications clear <domena> / <imi^e>\n");
            return 0;
        }

        /* Remove all applications to a certain domain. */
            if (sizeof(m_domains[capitalize(words[1])]))
            {
                dname = capitalize(words[1]);
            if (!sizeof(m_applications[dname]))
            {
                    write("Nikt nie sk^lada^l podania do " + dname + ".\n");
                return 1;
            }

            m_applications = m_delete(m_applications, dname);
            save_master();
            write("Usuni^ete wszystkie podania do " + dname + ".\n");
            return 1;
        }

        /* Remove all applications by a certain player. */
        write("Sprawdzam podania czarodzieja " + capitalize(words[1]) + ".\n");
        remove_all_applications(lower_case(words[1]));
        return 1;
    }

    /* People in a domain may list the applications to their domain. */
    if ((rank >= WIZ_NORMAL) &&
        (m_wizards[cmder][FOB_WIZ_DOM] != WIZARD_DOMAIN))
    {
        if (strlen(str) && (rank == WIZ_LORD))
            return list_applications_by_wizard(str, 0);

        if (!sizeof(m_applications[m_wizards[cmder][FOB_WIZ_DOM]]))
        {
            write("Nikt nie z^lo^zy^l podania do twojej domeny.\n");
            return 1;
        }

        write("Czarodzieje, kt^orzy z^lo^zyli podanie do twojej domeny: " +
            COMPOSITE_WORDS(sort_array(map(m_applications[m_wizards[cmder][FOB_WIZ_DOM]],
            capitalize))) + ".\n");
        return 1;
    }

    /* List your own applications. */
    return list_applications_by_wizard(cmder, 1);
}

/************************************************************************
 *
 * The experience administration code.
 *
 */

/*
 * Nazwa funkcji: bookkeep_exp
 * Opis         : Note the xp domains give to the mortals and what kind.
 *                This is not saved each time to KEEPERSAVE, we trust it
 *		  to be saved at one time or other. Exact bookkeeping is not
 *		  absolutely crucial and it would take a lot of time.
 * Argumenty    : int q_xp: Amount of quest xp. Can only be 0 or positive.
 *		  int c_xp: Amount of combat xp. Can be both pos. and neg.
 */
public void
bookkeep_exp(int q_xp, int c_xp)
{
    int    cobj = 0;
    string dname = "";
    object pobj = previous_object();
    object giver = previous_object(-1);

    /* It should be a mortal player, not an NPC and it should not be fixup
     * of accumulated stats, not should it be a 'jr' wizhelper character.
     */
    if (!interactive(pobj) || (geteuid(pobj) != BACKBONE_UID) ||
        ((pobj == giver) && (q_xp == 0) && (ABS(c_xp) < 2)) ||
        wildmatch("*jr", pobj->query_real_name()))
    {
        return;
    }

    /* If it is combat XP, we want to get the living, not the combat
     * object, otherwise tracing is real hard.
     */
    if (c_xp && objectp(giver->qme()))
    {
        giver = giver->qme();
        cobj = 1;
    }

#ifdef LOG_BOOKKEEP_ERR
    /* BAD wiz! Won't be wiz much longer. */
    if (objectp(this_interactive()))
    {
        if (SECURITY->query_wiz_level(dname = this_interactive()->query_real_name()))
        {
            set_auth(this_object(), "root:root");
            log_file(LOG_BOOKKEEP_ERR,
                sprintf("%s %-12s %-1s(%8d) %-12s\n",
                    ctime(time()),
                    capitalize(pobj->query_real_name()),
                    ((q_xp == 0) ? "C" : "Q"),
                    ((q_xp == 0) ? c_xp : q_xp),
                    capitalize(dname)), 10000);
        }
    }
    /* This indicates that it wasn't a combat object giving the combat
     * experience.
     */
    else if ((c_xp > 0) && (!living(giver) || !cobj))
    {
        set_auth(this_object(), "root:root");
        log_file(LOG_BOOKKEEP_ERR,
            sprintf("%s %-12s C(%8d) %s\n",
                ctime(time()),
                capitalize(pobj->query_real_name()),
                c_xp,
                file_name(giver)), 10000);
    }
#endif LOG_BOOKKEEP_ERR

    /* Get the euid of the experience giving object. */
    dname = geteuid(giver);
    if (sizeof(m_wizards[dname]))
        dname = m_wizards[dname][FOB_WIZ_DOM];

    /* Experience can only be given by a domain. This object had a bad
     * euid. Nonexistant or apprentice.
     */
    if (!sizeof(m_domains[dname]))
    {
#ifdef LOG_BOOKKEEP_ERR
        /* If this is the case, it is a playerkill and we do not want to
         * log that.
         */
        if (dname != BACKBONE_UID)
        {
            set_auth(this_object(), "root:root");
            log_file(LOG_BOOKKEEP_ERR,
                sprintf("%s %-12s %-1s(%8d) %-1s\n",
                    ctime(time()),
                    capitalize(pobj->query_real_name()),
                    ((q_xp == 0) ? "C" : "Q"),
                    ((q_xp == 0) ? c_xp : q_xp),
                    file_name(giver)), 10000);
        }
#endif LOG_BOOKKEEP_ERR
        return;
    }

#ifdef LOG_BOOKKEEP
    /* This is the log all normal combat experience and quest experience
     * ends up. It is only logged if it exceeds a certain level.
     */
    if (( q_xp > LOG_BOOKKEEP_LIMIT_Q) ||
        ( c_xp > LOG_BOOKKEEP_LIMIT_C) ||
        (-q_xp > LOG_BOOKKEEP_LIMIT_Q) ||
        (-c_xp > LOG_BOOKKEEP_LIMIT_C))
    {
        set_auth(this_object(), "root:root");
        log_file(LOG_BOOKKEEP, sprintf("%s %-12s %-1s(%8d) %-1s\n",
            ctime(time()),
            capitalize(pobj->query_real_name()),
            ((q_xp == 0) ? "C" : "Q"),
            ((q_xp == 0) ? c_xp : q_xp),
            file_name(giver)), 1000000);
    }
#endif LOG_BOOKKEEP

    /* Give the experience to the domain. Not necessary to save it, that
     * will happen next time something needs to be saved.
     */
    m_domains[dname][FOB_DOM_QXP] += q_xp;

    if (c_xp > 0)
        m_domains[dname][FOB_DOM_CXP] += c_xp;
}

/*
 * Nazwa funkcji: do_decay
 * Opis         : The mapping m_domains is mapped over this function to
 *                do the actual decay.
 * Argumenty    : mixed *darr - the array of the individual domains.
 * Zwraca       : mixed * - the modified array of the individual domains.
 */
static mixed *
do_decay(mixed *darr)
{
    int decay;
#ifdef DECAY_XP
    decay = DECAY_XP;
#else
    decay = 100;
#endif
    if (!decay)
        return darr;

    if (darr[FOB_DOM_QXP] >= decay || (-darr[FOB_DOM_QXP]) >= decay)
        darr[FOB_DOM_QXP] -= darr[FOB_DOM_QXP] / decay;
    else if (darr[FOB_DOM_QXP] < 0)
        darr[FOB_DOM_QXP] += 1;
    else if (darr[FOB_DOM_QXP] > 0)
        darr[FOB_DOM_QXP] -= 1;

    if (darr[FOB_DOM_CXP] >= decay || (-darr[FOB_DOM_CXP]) >= decay)
        darr[FOB_DOM_CXP] -= darr[FOB_DOM_CXP] / decay;
    else if (darr[FOB_DOM_CXP] < 0)
        darr[FOB_DOM_CXP] += 1;
    else if (darr[FOB_DOM_CXP] > 0)
        darr[FOB_DOM_CXP] -= 1;
/*
 * Troche za duzo komend zabierane. /Alvin
    darr[FOB_DOM_CMNDS] = darr[FOB_DOM_CMNDS] / DECAY_XP;
 */

    darr[FOB_DOM_CMNDS] = darr[FOB_DOM_CMNDS] / 2;

    return darr;
}

/*
 * Nazwa funkcji: do_decay_cmd
 * Opis         : This decay function will take 1% off the argument value.
 * Argumenty    : int count - the value to decay.
 * Zwraca       : int - the same value minus one per cent.
 */
int
do_decay_cmd(int count)
{
    return count - count / 100;
}

/*
 * Nazwa funkcji: decay_exp
 * Opis         : Let the accumulated xp / domain decay over time. This
 *                is called from check_memory which is called at regular
 *		  intervalls. check_memory also saves to KEEPERSAVE.
 */
static void
decay_exp()
{
    m_domains = map(m_domains, do_decay);
}

/*
 * Nazwa funkcji: query_domain_commands
 * Opis         : Gives the total number of commands executed by mortal
 *                players in a domain.
 * Argumenty    : string dname - the domain name.
 * Zwraca       : int - the number of commands.
 */
int
query_domain_commands(string dname)
{
    dname = capitalize(dname);

    if (!sizeof(m_domains[dname]))
        return 0;

    return m_domains[dname][FOB_DOM_CMNDS];
}

/*
 * Nazwa funkcji: query_domain_qexp
 * Opis         : Gives the quest experience gathered by a domain.
 * Argumenty    : string dname - the domain name.
 * Zwraca       : int - the accumulated experience.
 */
int
query_domain_qexp(string dname)
{
    dname = capitalize(dname);

    if (!sizeof(m_domains[dname]))
        return 0;

    return m_domains[dname][FOB_DOM_QXP];
}

/*
 * Nazwa funkcji: query_domain_cexp
 * Opis         : Gives the combat experience gathered by a domain.
 * Argumenty    : string dname - the domain name.
 * Zwraca       : int - the accumulated experience.
 */
int
query_domain_cexp(string dname)
{
    dname = capitalize(dname);

    if (!sizeof(m_domains[dname]))
        return 0;

    return m_domains[dname][FOB_DOM_CXP];
}

/*
 * Nazwa funkcji: domain_clear_xp
 * Opis         : With this function archwizards and keepers may remove the
 *                experience gathered by a domain. This is for debugging
 *                purposes and when errors have been made and the experience
 *                is not correct any more
 * Argumenty    : string dname - the domain-name.
 * Zwraca       : int 1/0 - success/failure.
 */
int
domain_clear_xp(string dname)
{
    string wname;

    /* May only be called from the arch soul. */
    if (!CALL_BY(WIZ_CMD_ARCH))
        return 0;

    /* Check whether such a domain indeed exists. */
    dname = capitalize(dname);
    if (!sizeof(m_domains[dname]))
    {
        notify_fail("Nie istnieje domena '" + capitalize(dname) + "'.\n");
        return 0;
    }

    /* Wipe the experience, save the master, log possible and tell the
     * wizard.
     */
    m_domains[dname][FOB_DOM_CXP] = 0;
    m_domains[dname][FOB_DOM_QXP] = 0;

    save_master();

#ifdef LOG_BOOKKEEP
    wname = getwho();
    log_file(LOG_BOOKKEEP, sprintf("%s (%s) Cleared by: %s\n",
        ctime(time()), dname, capitalize(wname)), -1);
#endif LOG_BOOKKEEP

    write("Wyczyszczono licznik do^swiadczenia domeny " + capitalize(dname) + ".\n");
    return 1;
}

/************************************************************************
 *
 * The wizard administration code.
 *
 */

/*
 * Nazwa funkcji: query_wiz_level
 * Opis         : Return the level of a wizard.
 * Argumenty    : string wname - the wizard.
 * Zwraca       : int - the level.
 */
int
query_wiz_level(string wname)
{
    wname = lower_case(wname);

    if (!sizeof(m_wizards[wname]))
        return 0;

    return m_wizards[wname][FOB_WIZ_LEVEL];
}

/*
 * Nazwa funkcji: query_wiz_rank
 * Desciption   : Return the rank of the wizard.
 * Argumenty    : string wname - the name of the wizard.
 * Zwraca       : int - the rank.
 */
int
query_wiz_rank(string wname)
{
    wname = lower_case(wname);

    if (!sizeof(m_wizards[wname]))
        return WIZ_MORTAL;

    return m_wizards[wname][FOB_WIZ_RANK];
}

/*
 * Nazwa funkcji: query_wiz_chl
 * Opis         : Return the name of the level changer.
 * Argumenty    : string wname - the wizard.
 *		  int prz - przypadek: 1 - mianownik, 2 - biernik
 * Zwraca       : string - the changer.
 */
string
query_wiz_chl(string wname, int prz = 0)
{
    wname = lower_case(wname);

    if (!sizeof(m_wizards[wname]))
        return "";

    return m_wizards[wname][FOB_WIZ_CHLEVEL][prz];
}

/*
 * Nazwa funkcji: query_wiz_chl_time
 * Opis         : Return the time the level was last changed.
 * Argumenty    : string wname - the wizard.
 * Zwraca       : int - the time.
 */
#ifdef FOB_KEEP_CHANGE_TIME
int
query_wiz_chl_time(string wname)
{
    wname = lower_case(wname);

    if (!sizeof(m_wizards[wname]))
        return 0;

    return m_wizards[wname][FOB_WIZ_CHLTIME];
}
#endif FOB_KEEP_CHANGE_TIME

/*
 * Nazwa funkcji: query_wiz_dom
 * Opis         : Return the domain of a wizard.
 * Argumenty    : string wname - the wizard.
 * Zwraca       : string - the domain, else "".
 */
string
query_wiz_dom(string wname)
{
    wname = lower_case(wname);

    if (!sizeof(m_wizards[wname]))
        return "";

    return m_wizards[wname][FOB_WIZ_DOM];
}

/*
 * Nazwa funkcji: query_wiz_chd
 * Opis         : Return the name of the domain changer.
 * Argumenty    : string wname - the wizard.
 *		  int prz - przypadek: 0 - mianownik, 1 - biernik
 * Zwraca       : string - the changer.
 */
string
query_wiz_chd(string wname, int prz = 0)
{
    wname = lower_case(wname);

    if (!sizeof(m_wizards[wname]))
        return "";

    return m_wizards[wname][FOB_WIZ_CHDOM][prz];
}

/*
 * Nazwa funkcji: query_wiz_chd_time
 * Opis         : Return the time the domain was last changed.
 * Argumenty    : string wname - the wizard.
 * Zwraca       : string - the time.
 */
#ifdef FOB_KEEP_CHANGE_TIME
int
query_wiz_chd_time(string wname)
{
    wname = lower_case(wname);

    if (!sizeof(m_wizards[wname]))
        return 0;

    return m_wizards[wname][FOB_WIZ_CHDTIME];
}
#endif FOB_KEEP_CHANGE_TIME

/*
 * Nazwa funkcji: query_wiz_list
 * Opis         : Return a list of all wizards of a given rank.
 * Argumenty    : int - the rank to get; -1 = all.
 * Zwraca       : string * - the names of the wizards with that name.
 */
public string *
query_wiz_list(int rank)
{
    if (rank == -1)
        return m_indices(m_wizards);

    return filter(m_indices(m_wizards), &operator(==)(rank) @ query_wiz_rank);
}

/*
 * Nazwa funkcji: reset_wiz_uid
 * Opis         : Set up a wizard's uid.
 * Argumenty    : object wiz - the wizard.
 */
public nomask void
reset_wiz_uid(object wiz)
{
    if (!query_wiz_level(wiz->query_real_name()))
        return;

    set_auth(wiz, wiz->query_real_name() + ":#");
}

/*
 * Nazwa funkcji: do_change_rank
 * Opis         : This function actually changes the rank of the person.
 *                It is an internal function that does not make any
 *                additional checks. They should have been made earlier.
 *                When someone is made liege, the previous liege is made
 *                into normal wizard.
 * Argumenty    : string wname - the wizard who rank is changed.
 *                int rank     - the new rank of the wizard.
 *                string cmder - who forces the change.
 */
static void
do_change_rank(string wname, int rank, string cmder)
{
    string dname;
    object wizard;

    /* Log the change of the rank. */
    log_file("LEVEL",
	     sprintf("%s %-11s: %-4s (%2d) -> %-4s (%2d) by %s.\n",
		     ctime(time()),
		     capitalize(wname),
		     WIZ_RANK_SHORT_NAME(m_wizards[wname][FOB_WIZ_RANK]),
		     m_wizards[wname][FOB_WIZ_LEVEL],
		     WIZ_RANK_SHORT_NAME(rank),
		     WIZ_RANK_START_LEVEL(rank),
		     capitalize(cmder)));

    /* If the person was Liege, inform the domain. */
    if (m_wizards[wname][FOB_WIZ_RANK] == WIZ_LORD)
    {
	tell_domain(m_wizards[wname][FOB_WIZ_DOM],
		    m_domains[m_wizards[wname][FOB_WIZ_DOM]][FOB_DOM_LORD],
		    ("Nie jeste^s ju^z Lordem domeny " +
		     m_wizards[wname][FOB_WIZ_DOM] + ".\n"),
		    (capitalize(wname) + " nie jest ju^z Lordem domeny " +
		     m_wizards[wname][FOB_WIZ_DOM] + ".\n"));

	m_domains[m_wizards[wname][FOB_WIZ_DOM]][FOB_DOM_LORD] = "";
    }

    /* If the person was steward, inform the domain. */
    if (m_wizards[wname][FOB_WIZ_RANK] == WIZ_STEWARD)
    {
	tell_domain(m_wizards[wname][FOB_WIZ_DOM],
		    m_domains[m_wizards[wname][FOB_WIZ_DOM]][FOB_DOM_STEWARD],
		    ("Nie jeste^s ju^z Seniorem domeny " +
		     m_wizards[wname][FOB_WIZ_DOM] + ".\n"),
		    (capitalize(wname) + " nie jest ju^z Seniorem domeny " +
		     m_wizards[wname][FOB_WIZ_DOM] + ".\n"));

	m_domains[m_wizards[wname][FOB_WIZ_DOM]][FOB_DOM_STEWARD] = "";
    }

    /* If the person becomes Liege, inform the domain and demote the old
     * Liege, if there was one. Similar for the steward.
     */
    if (rank == WIZ_LORD)
    {
        /* If there already is a Lord, demote the old one. This must be
         * done before the new one is marked as such.
         */
        dname = m_wizards[wname][FOB_WIZ_DOM];

        if (strlen(m_domains[dname][FOB_DOM_LORD]))
            do_change_rank(m_domains[dname][FOB_DOM_LORD], WIZ_NORMAL, cmder);

        m_domains[dname][FOB_DOM_LORD] = wname;

        tell_domain(dname, wname,
            ("Jeste^s teraz Lordem domeny " + capitalize(dname) + ".\n"),
            (capitalize(wname) + " jest teraz Lordem domeny " +
            capitalize(dname) + ".\n"));
    }

    if (rank == WIZ_STEWARD)
    {
        dname = m_wizards[wname][FOB_WIZ_DOM];
        if (strlen(m_domains[dname][FOB_DOM_STEWARD]))
        {
            do_change_rank(m_domains[dname][FOB_DOM_STEWARD], WIZ_NORMAL,
            cmder);
        }

        m_domains[dname][FOB_DOM_STEWARD] = wname;

        tell_domain(dname, wname,
            ("Jeste^s teraz Seniorem domeny " + capitalize(dname) + ".\n"),
            (capitalize(wname) + " jest teraz Seniorem domeny " +
        capitalize(dname) + ".\n"));
    }

    if (rank == WIZ_RETIRED)
        m_trainees[wname] = 1;
    else
    /* Unmark the person as trainee if no longer 'normal wizard'. */
    if ((rank != WIZ_NORMAL) && m_trainees[wname])
        m_trainees = m_delete(m_trainees, wname);

    /* Tell the player about the change in rank. */
    if (objectp(wizard = find_player(wname)))
    {
        tell_object(wizard, "Zosta^l" + wizard->koncowka("e^s", "a^s") +
        ((rank > m_wizards[wname][FOB_WIZ_RANK]) ? " awansowan" :
             " zdegradowan") + wizard->koncowka("y", "a") +
             " do rangi " + WIZ_RANK_NAME(rank) + " (" +
        WIZ_RANK_START_LEVEL(rank) + ") przez " +
        capitalize(find_player(cmder)->query_name(PL_BIE)) + ".\n");
    }

    /* Make the actual change. */
    m_wizards[wname][FOB_WIZ_RANK] = rank;
    m_wizards[wname][FOB_WIZ_LEVEL] = WIZ_RANK_START_LEVEL(rank);
    m_wizards[wname][FOB_WIZ_CHLEVEL] = allocate(2);
    m_wizards[wname][FOB_WIZ_CHLEVEL][FOB_WIZ_CHLEVEL_MIA] = cmder;
    m_wizards[wname][FOB_WIZ_CHLEVEL][FOB_WIZ_CHLEVEL_BIE] =
        find_player(cmder)->query_real_name(3); // W bierniku
#ifdef FOB_KEEP_CHANGE_TIME
    m_wizards[wname][FOB_WIZ_CHLTIME] = time();
#endif FOB_KEEP_CHANGE_TIME

    save_master();

    if (objectp(wizard))
    {
        wizard->reset_userids();
        /* Update the command hooks. */
        wizard->update_hooks();
    }
}

/*
 * Nazwa funkcji: wizard_change_rank
 * Opis         : Using this function, a liege, arch or keeper can try to
 *                alter the rank of a player.
 * Argumenty    : string wname - the wizard whose rank is to be changed.
 *                int rank     - the new rank.
 * Zwraca       : int 1/0 - success/failure.
 */
int
wizard_change_rank(string wname, int rank)
{
    string cmder;
    int    old_rank;
    string dname;
    object wizard;
    string konto;
    mapping map;

    /* May only be called from the Lord soul. */
    if (!CALL_BY(WIZ_CMD_LORD))
        return 0;

    cmder = getwho();
    wname = lower_case(wname);
    if (sizeof(m_wizards[wname]))
    {
        old_rank = m_wizards[wname][FOB_WIZ_RANK];
        dname = m_wizards[wname][FOB_WIZ_DOM];
    }
    else
    {
        old_rank = WIZ_MORTAL;
        dname = "";
    }

    /* See whether the wizard is allowed to alter the rank. Lords may only
     * promote or demote 'normal' wizards to and from steward and vice versa.
     * Stewards may not meddle in ranks.
     */
    if (m_wizards[cmder][FOB_WIZ_RANK] == WIZ_STEWARD && !SECURITY->query_admin_team_member(cmder, "aow"))
    {
        if (m_wizards[cmder][FOB_WIZ_DOM] != m_wizards[wname][FOB_WIZ_DOM])
        {
            write("Mo^zesz to zrobi^c tylko czarodziejowi z twojej w^lasnej " +
                "domeny.\n");
            return 1;
        }

        if ((old_rank == WIZ_RETIRED) && (rank != WIZ_NORMAL) ||
            (old_rank == WIZ_NORMAL) && (rank != WIZ_RETIRED))
        {
            write("Mo^zesz wy^l^acznie retired'a awansowa^c na wizard'a, "+
                "albo wizard'a zdegradowa^c do retired'a.\n");

            return 1;
        }
    }
    else if (m_wizards[cmder][FOB_WIZ_RANK] == WIZ_LORD && !SECURITY->query_admin_team_member(cmder, "aow"))
    {
        if (m_wizards[cmder][FOB_WIZ_DOM] != m_wizards[wname][FOB_WIZ_DOM])
        {
            write("Mo^zesz to zrobi^c tylko czarodziejowi z twojej w^lasnej " +
            "domeny.\n");
            return 1;
        }

        if ( (old_rank == WIZ_PILGRIM) && (rank != WIZ_NORMAL) )
        {
            write("Pilgrim'a mo^zesz tylko przywr^oci^c do rangi wizard'a, " +
                "albo wyrzuci^c z domeny komend^a 'expel'.\n");

            return 1;
        }

        if ((old_rank == WIZ_NORMAL) && (rank != WIZ_RETIRED))
        {
            write("Wizard'a mo^zesz uczyni^c jedynie retired'em.\n");

            return 1;
        }

        if ((old_rank == WIZ_RETIRED) && (rank != WIZ_NORMAL))
        {
            write("Retired'a mo^zesz tylko przywr^oci^c do rangi wizard'a, "+
                "lub wyrzuci^c go z domeny przy pomocy 'expel'.\n");

            return 1;
        }

        if ((old_rank != WIZ_NORMAL) && (old_rank != WIZ_RETIRED) &&
            ((old_rank != WIZ_STEWARD) || (rank != WIZ_NORMAL)))
        {
            write("Seniora mo^zesz uczyni^c tylko Wizardem.\n");
            return 1;
        }
    }
    /* Arches may not meddle with other arches or keepers. However, they may
     * demote themselves to mage.
     */
    else if (old_rank >= m_wizards[cmder][FOB_WIZ_RANK])
    {
        if (cmder != wname)
        {
            write("Nie masz uprawnie^n, ^zeby zmienia^c rang^e " +
                WIZ_RANK_NAME(old_rank) + "a.\n");
            return 1;
        }

        if (rank != WIZ_MAGE)
        {
            write("Mo^zesz si^e zdegradowa^c tylko do rangi mage'a.\n");
            return 1;
        }
    }

    /* See whether the transition is legal. */
    if (member_array(rank, WIZ_RANK_POSSIBLE_CHANGE(old_rank)) == -1)
    {
        write("Nie jest mo^zliwa " +
            ((rank > old_rank) ? "promocja" : "degradacja") + " kogo^s z " +
            WIZ_RANK_NAME(old_rank) + "'a na " + WIZ_RANK_NAME(rank) + "'a.\n");
        return 1;
    }

    /* Promote a mortal into wizardhood. */
    if (old_rank == WIZ_MORTAL)
        transform_mortal_into_wizard(wname, cmder);

    /* Do it. */
    do_change_rank(wname, rank, cmder);

    /* For some ranks, we need to do something after the actual change. */
    switch(rank)
    {
        case WIZ_MORTAL:
            /* If the wizard is in a domain, kick him out. */
            if (strlen(dname))
            {
                add_wizard_to_domain("", wname, cmder);
            }

            /* Burry all evidence of his/her existing. */
            m_wizards = m_delete(m_wizards, wname);
            remove_all_sanctions(wname);

            /* Tell him/her the bad news and boot him. */
            if (objectp(wizard = find_player(wname)))
            {
                tell_object(wizard, capitalize(cmder) + " przywraca ci^e do " +
                    "^swiata ^smiertelnik^ow. Aby gra^c dalej musisz " +
                    "stworzy^c now^a posta^c.\n");

                /* ... and ... POOF! ;-) */
                wizard->quit();
                if (objectp(wizard))
                {
                    do_debug("destroy", wizard);
                }
            }

            konto = finger_player(wname)->query_mailaddr();
            if (file_size(KONTO_FILE(konto)) >= 0)
            {
                map = restore_map(KONTO_FILE(konto));
                map["postacie"] -= ({ wname });
                map["postacie_skasowane"] += ({ wname });
                save_map(map, KONTO_FILE(konto));
            }

            /* Rename the file after the booting. We might change our mind ;-) */
            if (file_size(PLAYER_FILE(wname) + ".o") >= 0)
            {
                int numerek;
                string pliczek;
                while(file_size(pliczek=(PLAYER_FILE(wname) + ".o.wizard"+(numerek++))) >= 0);

                rename(PLAYER_FILE(wname) + ".o", PLAYER_FILE(wname) + ".o.wizard" + (numerek ? numerek+"" : ""));
            }
            break;

        case WIZ_APPRENTICE:
            if ((old_rank == WIZ_MORTAL) && (objectp(wizard = find_player(wname))))
            {
                wizard->set_wiz_level();
                wizard->set_default_start_location(WIZ_ROOM);
                wizard->save_me();
                wizard->update_hooks();
            }
        case WIZ_PILGRIM:
#ifdef 0
        case WIZ_RETIRED:
#endif
        if (strlen(dname))
        {
            add_wizard_to_domain("", wname, cmder);
        }
        break;
    }

    if(old_rank == WIZ_MORTAL)
    {
        write("Hurrrraaaaaaaa mamy nowego czarodzieja. Awansujesz " + capitalize(wname) + " do rangi " +
            WIZ_RANK_NAME(rank) + "'a (" + WIZ_RANK_START_LEVEL(rank) + ").\n");
    }
    else if(rank == WIZ_MORTAL)
    {
	//Krun - daj spok�j z takimi tekstami, jakby� to ty mia� kasowa�
	//wiz�w, a nie ja to chyba te� by ci si� nie u�miecha�o ci�gle tego czyta� :P V.
        write(/*"Buuuuuuuuuuuu jeden wiz mniej, teraz b�dziesz za niego tyra� draniu! " +*/
            "Degradujesz " + capitalize(wname) + " do rangi " + WIZ_RANK_NAME(rank) + "'a (" +
            WIZ_RANK_START_LEVEL(rank) + ").\n");
    }
    else
    {
        write(((rank > old_rank) ? "Awansujesz" : "Degradujesz") + " czarodzieja " +
            capitalize(wname) + " do rangi " + WIZ_RANK_NAME(rank) + "'a (" +
            WIZ_RANK_START_LEVEL(rank) + ").\n");
    }
    return 1;
}

/*
 * Nazwa funkcji: wizard_change_level
 * Opis         : Using this function, a liege, arch or keeper can try to
 *                alter the level of a player.
 * Argumenty    : string wname - the wizard whose level is to be changed.
 *                int rank     - the new level.
 * Zwraca       : int 1/0 - success/failure.
 */
int
wizard_change_level(string wname, int level)
{
    string cmder;
    object wizard;

    /* May only be called from the Lord soul. */
    if (!CALL_BY(WIZ_CMD_LORD))
        return 0;

    cmder = getwho();
    wname = lower_case(wname);
    if (!sizeof(m_wizards[wname]))
    {
        write(capitalize(wname) + " nie jest czarodziejem. Aby go/j^a " +
            "awansowa^c, u^zyj 'promote " + wname + " <rank>'.\n");
        return 1;
    }

    if ((m_wizards[cmder][FOB_WIZ_RANK] <= m_wizards[wname][FOB_WIZ_RANK]) ||
        (((m_wizards[cmder][FOB_WIZ_RANK] == WIZ_LORD) ||
        (m_wizards[cmder][FOB_WIZ_RANK] == WIZ_STEWARD)) &&
        (m_wizards[cmder][FOB_WIZ_DOM] != m_wizards[wname][FOB_WIZ_DOM])))
    {
        write("Nie masz wp^lywu na poziom tego czarodzieja.\n");
        return 1;
    }

    if (m_wizards[wname][FOB_WIZ_LEVEL] == level)
    {
        write(capitalize(wname) + " ju^z ma poziom " + level + ".\n");
        return 1;
    }

    if (level < WIZ_RANK_MIN_LEVEL(m_wizards[wname][FOB_WIZ_RANK]))
    {
        write("Minimalny poziom dla " +
            WIZ_RANK_NAME(m_wizards[wname][FOB_WIZ_RANK]) +
            "'a to " + WIZ_RANK_MIN_LEVEL(m_wizards[wname][FOB_WIZ_RANK]) +
            ".\n");
        return 1;
    }

    if (level > WIZ_RANK_MAX_LEVEL(m_wizards[wname][FOB_WIZ_RANK]))
    {
        write("Maksymalny poziom dla " +
            WIZ_RANK_NAME(m_wizards[wname][FOB_WIZ_RANK]) +
            "'a to " + WIZ_RANK_MAX_LEVEL(m_wizards[wname][FOB_WIZ_RANK]) +
            ".\n");
        return 1;
    }

    log_file("LEVEL",
        sprintf("%s %-11s: %-4s (%2d) -> %-4s (%2d) by %s.\n",
            ctime(time()),
            capitalize(wname),
            WIZ_RANK_SHORT_NAME(m_wizards[wname][FOB_WIZ_RANK]),
            m_wizards[wname][FOB_WIZ_LEVEL],
            WIZ_RANK_SHORT_NAME(m_wizards[wname][FOB_WIZ_RANK]),
            level,
            capitalize(cmder)));

    if (objectp(wizard = find_player(wname)))
    {
        tell_object(wizard, wizard->koncowka("Zosta^le^s ", "Zosta^la^s ") +
            ((level > m_wizards[wname][FOB_WIZ_LEVEL]) ?
            "awansowan" + wizard->koncowka("y", "a") :
            "zdagradowan" + wizard->koncowka("y", "a")) +
            " do poziomu " + level + " przez " +
            capitalize(cmder) + ".\n");
    }

    write(((level > m_wizards[wname][FOB_WIZ_LEVEL]) ? "Awansujesz " :
        "Degradujesz ") + capitalize(wname) + " do poziomu " + level + ".\n");

    m_wizards[wname][FOB_WIZ_LEVEL] = level;
    m_wizards[wname][FOB_WIZ_CHLEVEL][FOB_WIZ_CHLEVEL_MIA] = cmder;
    m_wizards[wname][FOB_WIZ_CHLEVEL][FOB_WIZ_CHLEVEL_BIE] =
        find_player(cmder)->query_real_name(3); // W bierniku
#ifdef FOB_KEEP_CHANGE_TIME
    m_wizards[wname][FOB_WIZ_CHLTIME] = time();
#endif FOB_KEEP_CHANGE_TIME

    save_master();

    return 1;
}

/*
 * Nazwa funkcji: set_keeper
 * Opis         : We have a special command to make keepers. Only keepers
 *                may use it to stress its special importance, but hacking
 *                the master save file should not be necessary.
 * Argumenty    : string wname - the arch to make keeper (or vice versa)
 *                int promote  - 1 = make keeper, 0 = make arch.
 * Zwraca       : int 1/0 - success/ failure.
 */
int
set_keeper(string wname, int promote)
{
    /* May only be called from the keeper soul. */
    if (!CALL_BY(WIZ_CMD_KEEPER))
        return 0;

    do_change_rank(wname, (promote ? WIZ_KEEPER : WIZ_ARCH), getwho());
    return 1;
}

/*
 * Nazwa funkcji: create_wizard
 * Opis         : Certain objects in the game are allowed to promote a mortal
 *                into wizardhood. This function should be called to do the
 *                transformation. The mortal is then made into apprentice.
 * Argumenty    : string name - the name of the mortal to be promoted.
 */
void
create_wizard(string name)
{
    if (member_array(function_exists("make_wiz", previous_object()),
        WIZ_MAKER) == -1)
    {
        return;
    }

    transform_mortal_into_wizard(name, ROOT_UID);
    do_change_rank(name, WIZ_APPRENTICE, ROOT_UID);
}

/*
 * Nazwa funkcji: query_wiz_pretitle
 * Opis         : Gives a pretitle for a specific wizard type.
 * Argumenty    : mixed wiz - the wizard object or name.
 * Zwraca       : string - the pretitle for the wizard.
 */
string
query_wiz_pretitle(mixed wiz)
{
    string name;
    int gender;

    /* Knowing the name, find the objectpointer for the gender. */
    if (stringp(wiz))
    {
        name = wiz;
        wiz = find_player(wiz);
    }

    /* If there is no such object, clone the finger-player and clean up
     * after ourselves. Else, pick up the info from the wizard object.
     */
    if (!objectp(wiz))
    {
        set_auth(this_object(), "root:root");
        wiz = finger_player(name);

        if (!objectp(wiz))
            return "";

        gender = wiz->query_gender();
        wiz->remove_object();
    }
    else
    {
        gender = wiz->query_gender();
        name = wiz->query_real_name();
    }

    //Dla roota wsio inaczej, bo mam tak� ochote:P
    if(name == "root")
        return 0;

    switch(query_wiz_rank(name))
    {
        case WIZ_MORTAL:
            return "";
        /*
         * Powr�t do wersji z Ban Ard, narazie tylko opisowo, potem jak b�d� mia� czas zrobie
         * dwie domeny i odpowiednio je po��cze, a jak kto� chce to wy��czy� to niech odkomentuje definicje
         * poni�ej.
         */
// #define TYLKO_ARTETUZA
        case WIZ_APPRENTICE:
#ifdef TYLKO_ARETUZA
            return ({"Nowicjusz Akademii w Aretuzie", "Nowicjuszka Akademii w Aretuzie",
                "Nowicjusz^atko"})[gender];
#else
            return ({"Nowicjusz Akademii w Ban Ard", "Nowicjuszka Akademii w Artetuzie",
                "Nowicjusz�tko Akademii w Ban Ard"})[gender];
#endif
        case WIZ_PILGRIM:
            return "Konsul";

        case WIZ_RETIRED:
#ifdef TYLKO_ARTETUZA
            return ({"Absolwent Akademii w Aretuzie", "Absolwentka Akademii w Aretuzie",
                "Absolwenci�tko Akademii w Aretuzie"})[gender];
#else
            return ({"Absolwent Akademii w Ban Ard", "Absolwentka Akademii w Aretuzie",
                "Absolweci�tko Akademii w Ban Ard"})[gender];
#endif
        case WIZ_NORMAL:
            if (query_wiz_level(name) == WIZ_RANK_START_LEVEL(WIZ_NORMAL))
            {
#ifdef TYLKO_ARETUZA
                return ({ "Adept Akademii w Aretuzie", "Adeptka Akademii w Aretuzie", "Adepci^atko" })[gender];
#else
                return ({"Adept Akademii w Ban Ard", "Adeptka Akademii w Aretuzie", "Adepci�tko Akademii w Ban Ard"})[gender];
#endif
            }
            else if (query_wiz_level(name) < 15)
            {
#ifdef TYLKO_ARETUZA
                return ({"M^lodszy ucze^n Akademii w Aretuzie", "M^lodsza uczennica Akademii w Aretuzie",
                    "M^lodszy uczniak" })[gender];
#else
                return ({"M�odszy ucze� Akademii w Ban Ard", "M�odsza uczennica Akademii w Aretuzie",
                    "M�odszy uczniak"})[gender];
#endif
            }
            else if (query_wiz_level(name) < 21)
            {
#ifdef TYLKO_ARETUZA
                return ({"Ucze^n Akademii w Aretuzie", "Uczennica Akademii w Aretuzie", "Uczniak"})[gender];
#else
                return ({"Ucze� Akademii w Ban Ard", "Uczennica Akademii w Aretuzie", "Uczniak"})[gender];
#endif
            }
            else
            {
#ifdef TYLKO_ARETUZA
                return ({"Starszy ucze^n Akademii w Aretuzie", "Starsza uczennica Akademii w Aretuzie",
                    "Starszy uczniak"})[gender];
#else
                return ({"Starszy ucze� Akademii w Ban Ard", "Starsza uczennica Akademii w Aretuzie",
                    "Starszy uczniak"})[gender];
#endif
            }
        case WIZ_MAGE:
            if(query_wiz_level(name) < 21)
            {
#ifdef TYLKO_ARETUZA
                return ({"Ucze^n Akademii w Aretuzie", "Uczennica Akademii w Aretuzie", "Uczniak"})[gender];
#else
                return ({"Ucze� Akademii w Ban Ard", "Uczennica Akademii w Aretuzie", "Uczniak"})[gender];
#endif
            }
            else if(query_wiz_level(name) <= WIZ_RANK_MAX_LEVEL(WIZ_NORMAL))
            {
#ifdef TYLKO_ARETUZA
                return ({"Starszy ucze^n Akademii w Aretuzie", "Starsza uczennica Akademii w Aretuzie",
                    "Starszy uczniak"})[gender];
#else
                return ({"Starszy ucze� Akademii w Ban Ard", "Starsza uczennica Akademii w Aretuzie",
                    "Starszy uczniak"})[gender];
#endif
            }
            else
            {
                return ({"Cz^lonek Najwy^zszej Rady Czarodziej^ow", "Cz^lonkini Najwy^zszej Rady Czarodziej^ow",
                    "Cz^lonek Najwy^zszej Rady Czarodziej^ow"})[gender];
            }

        case WIZ_STEWARD:
#ifdef TYLKO_ARETUZA
            return ({"Prorektor Akademii w Aretuzie, Cz�onek Najwy�szej Rady Czarodziej�w", "Prorektorka "+
                "Akademii w Aretuzie, Cz�onkini Najwy�szej Rady Czarodziej�w", "Prorektor"})[gender];
#else
            return ({"Prorektor Akademii w Ban Ard, Cz�onek Najwy�szej Rady Czarodziej�w", "Prorektorka "+
                "Akademii w Aretuzie, Cz�onkini Najwy�szej Rady Czarodziej�w", "Prorektor"})[gender];
#endif

        case WIZ_LORD:
#ifdef TYLKO_ARETUZA
            return ({"Rektor Akademii w Aretuzie, Cz�onek Kapitu�y Czarodziej�w", "Rektorka Akademii "+
                "w Aretuzie, Cz�onkini Kapitu�y Czarodziej�w", "Rektor"})[gender];
#else
            return ({"Rektor Akademii w Ban Ard, Cz�onek Kapitu�y Czarodziej�w", "Rektorka Akademii "+
                "w Aretuzie, Cz�onkini Kapitu�y Czarodziej�w", "Rektor"})[gender];
#endif
        case WIZ_ARCH:
            if(query_wiz_level(name) < 48)
            {
                return ({"Mistrz Magii, Cz�onek Kapitu�y Czarodziej�w", "Mistrzyni Magii, Cz�onkini Kapitu�y Czarodziej�w",
                    "Mistrzunio Maguni" })[gender];
            }
            else
            {
                return ({ "Arcymistrz Magii, Cz�onek Kapitu�y Czarodziej�w",
                    "Arcymistrzyni Magii, Cz�onkini Kapitu�y Czarodziej�w", "Arcymistrzunio Magunieczki" })[gender];
            }
        case WIZ_KEEPER:
            return ({ "Arcymistrz Magii, Przewodnicz�cy Kapitu�y Czarodziej�w",
                "Arcymistrzyni Magii, Przewodnicz�ca Kapitu�y Czarodziej�w", "Arcymistrzunio Magunieczki" })[gender];
    }

    /* Should never end up here. */
    return "";
}

/*
 * Nazwa funkcji: query_wiz_path
 * Opis         : Gives a default path for a wizard
 * Argumenty    : string wname - the wizard name.
 * Zwraca       : string - the default path for the wizard.
 */
string
query_wiz_path(string wname)
{
    string dname;

    /* A domains path. */
    dname = capitalize(wname);

    if (sizeof(m_domains[dname]))
        return "/d/" + dname;

    /* Root. */
    wname = lower_case(wname);
    if (wname == ROOT_UID)
        return "/syslog";

    /* Not a wizard, ie no path. */
    if (!sizeof(m_wizards[wname]))
        return "";

    /* A wizard who is a domain member. */
    dname = m_wizards[wname][FOB_WIZ_DOM];
    if (strlen(dname))
        return "/d/" + dname + "/" + wname;

    /* Non-domain members, ie apprentices, pilgrims and retired people. */
    return "/doc";
}

/*
 * Nazwa funkcji: query_mage_links
 * Opis         : Finds all WIZARD_DOMAIN mages and makes a list of all links.
 * Zwraca       : string * - the list.
 */
public string *
query_mage_links()
{
    string *links;
    int index;
    int size;

    index = -1;

    links = ({});//sort_array( ({ }) + m_domains[WIZARD_DOMAIN][FOB_DOM_MEMBERS]);
    size  = sizeof(links);
    while(++index < size)
    {
        links[index] = "/d/" + WIZARD_DOMAIN + "/" + links[index] + "/" +
            WIZARD_LINK;
    }

    return links;
}

/*
 * Nazwa funkcji: query_domain_links
 * Opis         : Make a list of domainfiles to link.
 * Zwraca       : string * - the list.
 */
public string *
query_domain_links()
{
    int index;
    int size;
    string *links;

    index = -1;
    links = sort_array(m_indices(m_domains));
    size = sizeof(links);

    while(++index < size)
        links[index] = "/d/" + links[index] + "/" + DOMAIN_LINK;

    return links;
}

/*
 * Nazwa funkcji: retire_wizard
 * Opis         : Wizards may retire from active coding as they please.
 *                They loose their coding rights and are placed in the
 *                special retired wizards rank.
 * Zwraca       : int 1/0 - success/failure.
 */
int
retire_wizard()
{
    string cmder;
    int    rank;

    /* May only be called from the 'normal' wizard soul. */
    if (!CALL_BY(WIZ_CMD_NORMAL))
        return 0;

    cmder = getwho();
    rank  = m_wizards[cmder][FOB_WIZ_RANK];

    /* People who aren't full wizards cannot retire. */
    if (rank < WIZ_NORMAL)
    {
        notify_fail("Nie mo^zesz tego zrobi^c. Je^sli chcesz zrezygnowa^c, " +
            "popro^s cz^lonka administracji, aby ci^e zdegradowa^l.\n");
        return 0;
    }

    if (rank >= WIZ_ARCH)
    {
        notify_fail("Nie ma mowy. Arch ani Keeper nie mog^a zrezygnowa^c.\n");
        return 0;
    }

    /* Do it. */
    do_change_rank(cmder, WIZ_RETIRED, cmder);
//    add_wizard_to_domain("", cmder, cmder);

    write("Zrezygnowa^l" + find_player(cmder)->koncowka("e^s", "a^s") +
        " z kodowania.\n");
    return 1;
}

/************************************************************************
 *
 * The global read administration code.
 *
 */

/*
 * Nazwa funkcji: add_global_read
 * Opis         : Add a wizard to the global read list.
 * Argumenty    : string wname   - the name of the wiz to be added.
 *		  string comment - a suitable comment to store along with
 *                                 the name telling why the player has it.
 * Zwraca       : int 1/0 - success/failure.
 */
int
add_global_read(string wname, string comment)
{
    string cmder;
    object wiz;

    /* May only be called from the arch soul. */
    if (!CALL_BY(WIZ_CMD_ARCH))
        return 0;

    cmder = getwho();

    /* Only full wizards can have global read access. */
    wname = lower_case(wname);

    if (m_wizards[wname][FOB_WIZ_RANK] < WIZ_NORMAL)
    {
        notify_fail(capitalize(wname) + " nie ma rangi czarodzieja.\n");
        return 0;
    }

    if (!strlen(comment))
    {
        notify_fail("Musisz poda^c komentarz.\n");
        return 0;
    }

    /* Only add global access if the player does not have it already. */
    if (m_global_read[wname])
    {
        notify_fail(capitalize(wname) + " ju^z ma globalne prawo odczytu.\n");
        return 0;
    }

    /* Add the stuff, save the master and tell the caller. */
    m_global_read[wname] = ({ cmder, comment });
    save_master();

    if (objectp(wiz = find_player(wname)))
    {
        tell_object(wiz, "Zosta^lo ci nadane globalne prawo odczytu przez " +
            capitalize(find_player(cmder)->query_name(PL_BIE)) + ".\n");
    }

    write("Dodano czarodziejowi " + capitalize(wname) + " globalne prawo " +
        "odczytu.\n");
    return 1;
}

/*
 * Nazwa funkcji: remove_global_read
 * Opis         : Remove a wizard from the global access list.
 * Argumenty    : string wname - the wizard to remove.
 * Zwraca       : int 1/0 - true if successful.
 */
int
remove_global_read(string wname)
{
    string cmder;
    object wiz;

    /* May only be called from the arch soul. */
    if (!CALL_BY(WIZ_CMD_ARCH))
        return 0;

    cmder = getwho();

    /* See whether the person indeed has global read access. */
    wname = lower_case(wname);
    if (!m_global_read[wname])
    {
        notify_fail("Nie istnieje czarodziej o imieniu '" +
            capitalize(wname) + "'.\n");
        return 0;
    }

    /* Remove the entry, save the master and notify the caller. */
    m_global_read = m_delete(m_global_read, wname);
    save_master();

    if (objectp(wiz = find_player(wname)))
    {
        tell_object(wiz, "Twoje globalne prawo odczytu zosta^lo cofni^ete " +
            "przez " + capitalize(find_player(cmder)->query_name(PL_BIE)) + ".\n");
    }

    write("Cofni^ete globalne prawo odczytu czarodziejowi " +
        capitalize(wname) + ".\n");
    return 1;
}

/*
 * Nazwa funkcji: query_global_read
 * Opis         : This function returns a mapping with the names of the
 *                people that have global read access. For the format of
 *                the file, see the header of this file.
 * Zwraca       : mapping - the global read list.
 */
mapping
query_global_read()
{
    return secure_var(m_global_read);
}

/************************************************************************
 *
 * The trainee administration code.
 *
 */

/*
 * Nazwa funkcji: add_trainee
 * Opis         : Add a wizard to the list of trainees. This may only be
 *                called from the Lord soul.
 * Argumenty    : string wname - the name of the wizard to mark as trainee.
 * Zwraca       : int 1/0 - success/failure.
 */
int
add_trainee(string wname)
{
    string cmder;

    /* May only be called from the Lord soul. */
    if (!CALL_BY(WIZ_CMD_LORD))
        return 0;

    cmder = getwho();

    /* Only full wizards can be trainees. */
    wname = lower_case(wname);

    if (!m_wizards[wname])
    {
        notify_fail("Nie ma czarodzieja o imieniu '" + capitalize(wname) +
            "'.\n");
        return 0;
    }

    if (m_wizards[wname][FOB_WIZ_RANK] != WIZ_NORMAL)
    {
        notify_fail(capitalize(wname) + " nie jest jeszcze prawdziwym " +
            "czarodziejem.\n");
        return 0;
    }

    /* Lords and stewards may only meddle in their own domain. */
    if ((m_wizards[cmder][FOB_WIZ_RANK] <= WIZ_LORD) &&
        (m_wizards[cmder][FOB_WIZ_DOM] != m_wizards[wname][FOB_WIZ_DOM]))
    {
        notify_fail(capitalize(wname) + " nie jest cz^lonkiem domeny " +
            m_wizards[cmder][FOB_WIZ_DOM] + ".\n");
        return 0;
    }

    /* Only add traineeship if the wizard is not one already. */
    if (m_trainees[wname])
    {
        notify_fail("Czarodziej " + capitalize(wname) +
            " jest ju^z praktykantem.\n");
        return 0;
    }

    /* Add the stuff, save the master and tell the caller. */
    m_trainees[wname] = 1;
    save_master();

    object wiz = find_player(wname);
    wiz->catch_msg("Zosta�e� mianowan" + wiz->koncowka("y", "a", "e") + " "+
        "praktykantem przez " + capitalize(TP->query_name(PL_BIE)) + ".\n");

    write("Czarodziej " + capitalize(wname) + " zosta^l praktykantem.\n");
    return 1;
}

/*
 * Nazwa funkcji: remove_trainee
 * Opis         : Remove a wizard from the list of trainees. This may only
 *                be called from the Lord soul.
 * Argumenty    : string wname - the wizard to remove.
 * Zwraca       : int 1/0 - true if successful.
 */
int
remove_trainee(string wname)
{
    string cmder;

    /* May only be called from the Lord soul. */
    if (!CALL_BY(WIZ_CMD_LORD))
        return 0;

    cmder = getwho();
    wname = lower_case(wname);

    /* Lords and stewards may only meddle in their own domain. */
    if ((m_wizards[cmder][FOB_WIZ_RANK] <= WIZ_LORD) &&
        (m_wizards[cmder][FOB_WIZ_DOM] != m_wizards[wname][FOB_WIZ_DOM]))
    {
        notify_fail(capitalize(wname) + " nie jest cz^lonkiem domeny " +
            m_wizards[cmder][FOB_WIZ_DOM] + ".\n");
        return 0;
    }

    /* Only remove traineeship if the wizard is a trainee. */
    if (!m_trainees[wname])
    {
        notify_fail(capitalize(wname) + " nie jest praktykantem.\n");
        return 0;
    }

    /* Remove the stuff, save the master and tell the caller. */
    m_trainees = m_delete(m_trainees, wname);
    save_master();

    object wiz = find_player(wname);
    wiz->catch_msg(capitalize(TP->query_name(PL_MIA)) + " usun" + wiz->koncowka("��", "�a", "�o") + " ci� z grona "+
        "praktykant�w.\n");

    write("Usuni^eto czarodzieja " + capitalize(wname) +
        " z grona praktykant^ow.\n");
    return 1;
}

/*
 * Nazwa funkcji: query_trainee
 * Opis         : This function returns true if the person is a trainee.
 * Argumenty    : string wname - the wizard to test.
 * Zwraca       : int 1/0 - trainee/not a trainee.
 */
int
query_trainee(string wname)
{
    return m_trainees[lower_case(wname)];
}

/*
 * Nazwa funkcji: query_trainees
 * Opis         : This function returns an array of the people who are
 *                marked trainee in their domain.
 * Zwraca       : string * - the list of trainees.
 */
mapping
query_trainees()
{
    return m_indices(m_trainees);
}

/************************************************************************
 *
 * The code securing the channels.
 *
 */

/*
 * Nazwa funkcji: set_channels
 * Opis         : This function is used by the apprentice commandsoul to
 *                store the mapping with the multi-wizline channels in a
 *                safe place.
 * Argumenty    : mapping channels - the mapping with the channels.
 */
int
set_channels(mapping channels)
{
    /* May only be called from the apprentice soul. */
    if (!CALL_BY(WIZ_CMD_APPRENTICE))
        return 0;

    set_auth(this_object(), "root:root");
    save_map(channels, CHANNELS_SAVE);
    return 1;
}

/*
 * Nazwa funkcji: query_channels
 * Opis         : This function is used by the apprentice commandsoul to
 *                retrieve the channels from disk. This is done since they
 *                are stored in a safe place.
 * Zwraca       : mapping - the mapping with the channels.
 */
mapping
query_channels()
{
    if (!CALL_BY(WIZ_CMD_APPRENTICE))
        return 0;

    set_auth(this_object(), "root:root");
    return restore_map(CHANNELS_SAVE);
}
