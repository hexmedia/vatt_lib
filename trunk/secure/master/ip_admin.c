/**
 * Administrator adres�w ip.
 * @author Krun
 * @date 20.04.2007
 *
 * TODO:
 * - dopisa� obs�uge imion, kt�re mog� mie� multiloga ze sob�
 * - dopisa� obs�ug� imion, kt�re mog� mie� multiloga ze wszystkimi... ASTER rzomdzi
 */

#include "/sys/ip_admin.h"
#include "/sys/composite.h"

mapping ip_list = ([]);
    // lista adres�w ip ([ adres : ({dlaczego dozwolone multi, osoba zezwalajaca, czas}) ])
mixed *names_list = ({});
    // lista imion, kt�re mog� mie� ze sob� multiloga np.
    // ({ ({ "pow�d", "zezwalajacy", czas, ({"konto1" "konto2"})}), .. })
mapping snames_list = ([]);
    // lista imion, kt�re mog� mie� multiloga ze wszystkimi
    // (["konto" :  ({"powod", "zezwalajacy", czas }), .. ])

#define CHECK_SO_CAN_ADD      if(SECURITY->query_wiz_rank(TP->query_real_name()) < IP_RANK_DODAWANIE)  {return 0;}
#define CHECK_SO_CAN_REMOVE   if(SECURITY->query_wiz_rank(TP->query_real_name()) < IP_RANK_USUWANIE)   {return 0;}
#define CHECK_SO_CAN_SEE      if(SECURITY->query_wiz_rank(TP->query_real_name()) < IP_RANK_ZOBACZENIE) {return 0;}

#define POWOD           0
#define ZEZWALAJACY     1
#define CZAS            2
#define OSOBY           3

public varargs int
log_multi(string str, string *players, string c2=" i ")
{
    SECURITY->log_public("MULTILOGI",  ctime(time()) + ": " + str + ": " + COMPOSITE_WORDS2(players, c2) + "\n ");
}

nomask int
add_ip_to_list(string ip, string powod)
{
    CHECK_SO_CAN_ADD;

    mixed tmp;

    if(sizeof(tmp = explode(ip, ".")) != 4)
        return 0;

    if(!atoi(tmp[0]) || !atoi(tmp[1]) || !atoi(tmp[2]) || !atoi(tmp[3]))
        return 0;

    if(!powod)
        return 0;

    if(strlen(powod) < IP_MIN_DLUGOSC_POWODU)
        return 0;

    if(ip_list[ip])
        return -1;

    ip_list[ip] = ({powod, TP->query_real_name(), time()});

    save_master();

    return 1;
}

nomask int
add_names_to_list(string name1, string name2, string powod)
{
    CHECK_SO_CAN_ADD;

    mixed tmp;

    if(!powod)
        return 0;

    if(strlen(powod) < IP_MIN_DLUGOSC_POWODU)
        return 0;

    object p;
    if(!(SECURITY->finger_konto(name1)))
        if(p = finger_player(name1))
            name1 = p->query_mailaddr();

    if(!(SECURITY->finger_konto(name2)))
        if(p = finger_player(name2))
            name2 = p->query_mailaddr();

    if(!SECURITY->finger_konto(name1) || !SECURITY->finger_konto(name2))
        return 0;

    if(!pointerp(names_list))
        names_list = ({});
    else
    {
        names_list = filter(names_list, &pointerp());
        names_list = filter(names_list, &operator(==)(4,) @ &sizeof());
    }

    for(int i = 0; i < sizeof(names_list); i++)
    {
        if(!pointerp(names_list[i][OSOBY]) || sizeof(names_list[i][OSOBY]) != 2)
        {
            names_list = exclude_array(names_list, i, i);
            continue;
        }

        if(member_array(name1, names_list[i][OSOBY]) >= 0 && member_array(name2, names_list[i][OSOBY]) >= 0)
            return 0;
    }

    names_list += ({ ({powod, TP->query_real_name(), time(), ({name1, name2}) }) });

    save_master();

    return 1;
}

nomask int
add_names_to_slist(string name, string powod)
{
    CHECK_SO_CAN_ADD;

    mixed tmp;

    if(!powod)
        return 0;

    if(strlen(powod) < IP_MIN_DLUGOSC_POWODU)
        return 0;

    if(!mappingp(snames_list))
        snames_list = ([]);

    if(!(SECURITY->finger_konto(name)))
    {
        object p;
        if(p = finger_player(name))
            name = p->query_mailaddr();
        else
            return 0;
    }

    if(!mappingp(snames_list))
        snames_list = ([]);

    if(is_mapping_index(name, snames_list))
        return -1;

    snames_list[name] = ({ powod, TP->query_real_name(), time() });

    save_master();

    return 1;
}

nomask int
remove_ip_from_list(string ip)
{
    CHECK_SO_CAN_REMOVE;

    mixed tmp;

    if(sizeof(tmp = explode(ip, ".")) != 4)
        return 0;
    if(!atoi(tmp[0]) || !atoi(tmp[1]) || !atoi(tmp[2]) || !atoi(tmp[3]))
        return 0;

    if(!ip_list[ip])
        return -1;

    ip_list = m_delete(ip_list, ip);

    save_master();

    return 1;
}

nomask int
remove_name_from_list(string name1, string name2)
{
    CHECK_SO_CAN_REMOVE;

    if(!name1 || !name2)
        return 0;

    object p;
    if(!(SECURITY->finger_konto(name1)))
        if(p = finger_player(name1))
            name1 = p->query_mailaddr();

    if(!(SECURITY->finger_konto(name2)))
        if(p = finger_player(name2))
            name2 = p->query_mailaddr();

    if(!pointerp(names_list))
        names_list = ({});

    for(int i = 0; i < sizeof(names_list); i++)
    {
        if(!pointerp(names_list[i][OSOBY]))
        {
            names_list = exclude_array(names_list, i, i);
            continue;
        }

        if(member_array(name1, names_list[i][OSOBY]) >= 0 && member_array(name2, names_list[i][OSOBY]) >= 0)
        {
            names_list = exclude_array(names_list, i, i);
            save_master();
            return 1;
        }
    }
}

nomask int
remove_name_from_slist(string name)
{
    CHECK_SO_CAN_REMOVE;

    if(!name)
        return 0;

    object p;
    if(!(SECURITY->finger_konto(name)))
        if(p = finger_player(name))
            name = p->query_mailaddr();

    if(!mappingp(snames_list))
    {
        snames_list = ([]);
        return 0;
    }

    int i;
    if(!(i = member_array(name, m_indexes(snames_list))))
        return 0;

    snames_list = m_delete(snames_list, m_indexes(snames_list)[i]);

    save_master();

    return 1;
}

nomask mapping query_ip_list()
{
    CHECK_SO_CAN_SEE;

    return ip_list;
}

nomask mixed query_name_list()
{
    CHECK_SO_CAN_SEE;

    return names_list;
}

nomask mapping query_sname_list()
{
    CHECK_SO_CAN_SEE;

    return snames_list;
}

nomask public mixed query_ip_from_list(string ip)
{
    CHECK_SO_CAN_SEE;

    mixed tmp;

    if(sizeof(tmp = explode(ip, ".")) != 4)
        return 0;
    if(!atoi(tmp[0]) || !atoi(tmp[1]) || !atoi(tmp[2]) || !atoi(tmp[3]))
        return 0;

    if(!ip_list[ip])
        return -1;

    return ip_list[ip];
}

nomask public varargs mixed query_name_from_list(string name, string name2)
{
    CHECK_SO_CAN_SEE;

    if(!name)
        return 0;

    if(!pointerp(names_list))
    {
        names_list = ({});
        return 0;
    }

    for(int i = 0; i < sizeof(names_list); i++)
    {
        if(!pointerp(names_list[i][OSOBY]))
        {
            names_list = exclude_array(names_list, i, i);
            continue;
        }

        if(member_array(name, names_list[i][OSOBY]) >= 0 && (!name2 || member_array(name2, names_list[i][OSOBY]) >= 0))
            return names_list[i];
    }
}

public mixed query_name_from_slist(string name)
{
    if(!stringp(name))
        return 0;

    if(!mappingp(snames_list))
    {
        snames_list = ([]);
        return 0;
    }

    if(is_mapping_index(name, snames_list))
        return 0;

    if(!snames_list[name])
        return 0;

    return snames_list[name];
}

int query_login_multi_log(string name)
{
    mapping all_ips = ([]);

    object p = find_player(name);

    if(!p)
        return 0;

    if(p->query_wiz_level())
        return 0;

    string ip_number = query_ip_number(p);
    object *us = users() - ({p});
    us = filter(us, &operator(==)(ip_number,) @ &query_ip_number());
    us = filter(us, &not() @ &->query_wiz_level());

    if(!sizeof(us))
        return 0;

    dump_array(us);

    string konto = p->query_mailaddr();

    //Nie ma konta, to takiego delikwenta nie wy�wietlamy bo on jaki� specjalny
    if(!konto || !SECURITY->finger_konto(konto))
        return 0;

    //Logon�w, poda� nie pokazujemy
    if(lower_case(name) == "podanie" || lower_case(name) == "logon")
        return 0;

    if(is_mapping_index(ip_number, ip_list))
        return 0;

    if(is_mapping_index(name, snames_list))
        return 0;

    //Sprawdzamy czy jest multi i czy nie jest w dozwolonych imionach.
    foreach(object u : us)
    {
        string ip_number2 = query_ip_number(u);
        string konto2 = u->query_mailaddr();

        //W�a�ciwie to 2 poni�szych if'�w raczej nie powinien sprawdza�, ale na wszelki przypadek.
        if(u->query_wiz_level())
            continue;

        if(ip_number2 != ip_number)
            continue;

        int ns;
        for(int i = 0; i < sizeof(names_list); i++)
        {
            if(member_array(konto, names_list[i][3]) >= 0 && member_array(konto2, names_list[i][3]) >= 0)
            {
                ns = 1;
                break;
            }
        }

        if(!ns)
        {
            SECURITY->notify(p, 5);
            log_multi("Posta�", ({ name + " z konta: " + konto, "postaci� " +
                u->query_real_name() + " z konta:" +  konto2}), " z ");
            return 1;
        }
    }
}

mixed
query_multi_log()
{
    mapping all_ips = ([]);

    foreach(mixed u : users())
    {
        if(u->query_wiz_level())
            continue;

        string name = u->query_real_name();
        string konto = u->query_mailaddr();
        string ip_number = query_ip_number(u);

        if(lower_case(name) == "podanie" || lower_case(name) == "logon")
            continue;

        //Sprawdzamy czy ip dozwolony.
        if(!is_mapping_index(ip_number, ip_list))
        {
            if(is_mapping_index(ip_number, all_ips))
                all_ips[ip_number] += ({name});
            else
                all_ips[ip_number] = ({name});
        }
    }

    mixed *ret_ips = ({});

    foreach(mixed i : m_indexes(all_ips))
    {
        if(sizeof(all_ips[i]) < 2)
            continue;

        foreach(mixed n : all_ips[i])
        {
            string konto;
            object p;

            if(!(p = find_player(n)))
                continue;

            konto = p->query_mailaddr();

            if(!SECURITY->finger_konto(konto))
                continue;

            //Sprawdzamy czy dozwolone z ka�dym kontem
            if(is_mapping_index(konto, snames_list))
                all_ips[i] -= ({n});

            //Sprawdzamy czy dozwolone z konkretnym kontem
            foreach(string konto2 : map(all_ips[i], &->query_mailaddr() @ &find_player()))
                for(int j = 0; j < sizeof(names_list); j++)
                    if(member_array(konto, names_list[j][3]) >= 0 && member_array(konto2, names_list[j][3]) >= 0)
                        all_ips[i] -= ({n});
        }
        if(sizeof(all_ips[i]) > 2)
            ret_ips += ({ ({i, all_ips[i]}) });
    }

    return ret_ips;
}
