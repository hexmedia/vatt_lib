/**
 * \file /secure/pogoda.c
 *
 * Plik odpowiedzialny za sterowanie niebem i zjawiskami atmosferycznymi dla
 * poszczeg�lnych room�w.
 *
 * @author Jeremian
 * @author Krun, Vera
 */

inherit "/std/room.c";

#pragma no_clone
#pragma no_inherit
#pragma save_binary
#pragma strict_types

#define LOC_SET_SIZE 10

#include <mudtime.h>
#include <files.h>
#include <stdproperties.h>

#include "/sys/pogoda.h"
#include "/secure/pogoda.h"

#include "/secure/pogoda/slonce.c"
#include "/secure/pogoda/niebo.c"
#include "/secure/pogoda/chmury.c"
#include "/secure/pogoda/gwiazdy.c"
#include "/secure/pogoda/ksiezyc.c"
#include "/secure/pogoda/eventy.c"

private static int main_alarm;           /* g��wny alarm pilnuj�cy mijaj�cego czasu */
private static int last_time;            /* zmienna trzymaj�ca informacje o ostatnim czasie */
private static mapping plany    = ([ ]); /* mapping mapping�w trzymaj�cy plany pogodowe */
private static mapping rplany   = ([ ]); /* mapping trzymaj�cy alarm i obszary dla danego planu */
private static mapping rooms    = ([ ]); /* mapping mapping�w trzymaj�cy obiekty room�w */

#define DBG(x) find_player("vera")->catch_msg(x+"\n");

/** ********************************************************************** **
 ** ****************  F U N K C J E  P O M O C N I C Z E  **************** **
 ** ********************************************************************** **/

public void
create_room()
{
    set_short("g��wny synoptyk");
    add_prop(ROOM_I_LIGHT, 1);

    add_exit("/d/Standard/pogoda/pogoda",
        ({"e", "z powrotem do pogodynki", "z pokoju pogodowego"}) );
    setuid();
    seteuid(getuid());
}

public int stara_pora_dnia()
{
}

/*
 * Nazwa funkcji: short
 * Opis         : Funkcja zwraca opis obiektu.
 * Zwraca       : string - opis obiektu
 */
string
short()
{
    return "g��wny synoptyk";
}

/*
 * Nazwa funkcji: get_plan
 * Opis         : Funkcja zwraca plan pogodowy dla danych wsp�rz�dnych.
 * Argumenty    : int x - wsp�rz�dna x obszaru
 *                int y - wsp�rz�dna y obszaru
 * Zwraca       : object - plan pogodowy
 */
object
get_plan(int x, int y)
{
    if (!mappingp(plany[x]))
        return 0;

    if(!is_mapping_index(y, plany[x]))
        return 0;

    plany[x][y]->ustaw_wspolrzedne(x, y);

    return plany[x][y];
}

/*
 * Nazwa funkcji: get_status
 * Opis         : Funkcja wypisuje informacje o ilo�ci zarejestrowanych room�w.
 */
void
get_status()
{
    int i, j, xallo, yallo,ilosc = 0, ilpl = 0;
    int *xindices, *yindices;
    mapping tmpmap;

    xindices = m_indices(rooms);
    xallo = sizeof(xindices);
    for (i = 0; i < xallo; ++i)
    {
        tmpmap = rooms[xindices[i]];
        yindices = m_indices(tmpmap);
        yallo = sizeof(yindices);
        for (j = 0; j < yallo; ++j)
            ilosc += sizeof(tmpmap[yindices[j]]);
    }

    xindices = m_indices(plany);
    xallo = sizeof(xindices);
    for (i = 0; i < xallo; ++i)
        ilpl += m_sizeof(plany[xindices[i]]);

    write("*** " + capitalize(short()) + " ***\n");
    write("Ilo�� zarejestrowanych pomieszcze�: " + ilosc + "\n");
    write("Ilo�� przygotowanych plan�w: " + ilpl + "\n");
    write("* * *\n");
}

/*
 * Nazwa funkcji: pisz_do_roomu_zew
 * Opis         : Funkcja wypisuje do pomieszczenia komunikat przes�any jako trzeci argument.
 * Argumenty    : object room - pomieszczenie, do kt�rego piszemy
 *                int* wsp - wsp�rz�dne geograficzne pomieszczenia
 *                mixed msg - wiadomo�� do napisania do pomieszczenia
 */
void
pisz_do_roomu_zew(object room, int* wsp, mixed msg)
{
    if (!objectp(room) || !stringp(msg))
        return;

    if (!room->query_prop(ROOM_I_INSIDE))
        tell_roombb(room, msg, 0, TP, 1);
}

/**
 * Funkcja wypisuje w danym pomieszczeniu komunikat o zmianie
 * pory dnia dla danego pomieszczenia.
 * @param room      lokacja w kt�rej wypisujemy komunikat
 * @param wsp       wsp�rz�dne geograficzne lokacji
 */
void
zmiana_pory(object room, int* wsp)
{
    string str = "";

    switch (room->pora_dnia())
    {
        case MT_SWIT:
            if(TEMPERATURA(room) < 10)
                str = random(2) ? "Wstaje rze�ki, ch�odny �wit." : "�wit jest jasny i ch�odny.";
            else if(random(2) && ZACHMURZENIE(room) <= 2)
            {
                str = "Ksi�yc niknie na zachodzie, a na wschodzie wida� "+
                    "ju� porann� szaro�� nieba.";
            }
            else
                str = "Noc si� ko�czy, niebo zaczyna szarze�.";

            if(str)
            {
                str += "\n";
                tell_room(room, str);
            }

            ustaw_slonce(room);
            ustaw_niebo(room);
            ustaw_chmury(room);
            ustaw_gwiazdy(room);
            ustaw_ksiezyc(room);
            break;
        case MT_WCZESNY_RANEK:
            switch(random(4))
            {
                case 0:
                    str = "S�o�ce wschodzi.";
                    break;
                case 1:
                    str = "Pierwsze promienie s�o�ca wychylaj� si� zza horyzontu.";
                    break;
                case 2:
                    str = "Noc powoli przechodzi w szarawy, rze�ki ranek.";
                    break;
                case 3:
                    str = "S�o�ce wychyla si� zza horyzontu.";
                    break;
            }

            if(str)
            {
                str += "\n";
                tell_room(room, str);
            }

            ustaw_slonce(room);
            ustaw_niebo(room);
            ustaw_chmury(room);
            ustaw_gwiazdy(room);
            ustaw_ksiezyc(room);
            break;
        case MT_RANEK:
            //tell_room(room, "Nasta� ranek.\n");
            if(TEMPERATURA(room) <= 5 && random(2))
            {
                str = "Ch��d staje si� bardzo dokuczliwy, gdy noc odchodzi, "+
                    "a nastaje ranek.";
            }

            if(str)
            {
                str+="\n";
                tell_room(room, str);
            }

            ustaw_slonce(room);
            ustaw_niebo(room);
            ustaw_chmury(room);
            ustaw_gwiazdy(room);
            ustaw_ksiezyc(room);
            break;
        case MT_POLUDNIE:
            if(random(2))
                tell_room(room, "Nasta�o po�udnie.\n");

            ustaw_slonce(room);
            ustaw_niebo(room);
            ustaw_chmury(room);
            ustaw_gwiazdy(room);
            ustaw_ksiezyc(room);
            break;
        case MT_POPOLUDNIE:
            //tell_room(room, "Nasta�o popo�udnie.\n");
            ustaw_slonce(room);
            ustaw_niebo(room);
            ustaw_chmury(room);
            ustaw_gwiazdy(room);
            ustaw_ksiezyc(room);
            break;
        case MT_WIECZOR:
            //tell_room(room, "Nasta� wiecz�r.\n");
            ustaw_slonce(room);
            ustaw_niebo(room);
            ustaw_chmury(room);
            ustaw_gwiazdy(room);
            ustaw_ksiezyc(room);
            break;
        case MT_POZNY_WIECZOR:
            if(ZACHMURZENIE(room) >=3) //du�e i wi�ksze
            {
                switch(random(2))
                {
                    case 0:
                        str = "Ostatni promyk s�o�ca niknie pod "+
                            "warstw� ciemnych chmur.";
                        break;
                    case 1:
                        str = "Mimo i� niebo przes�aniaj� chmury, mo�na "+
                            "dostrzec, �e s�o�ce przechyla si� na "+
                            "zachodni� stron� nieba.";
                        break;
                }
            }
            else
            {
                switch(random(4))
                {
                    case 0: str="Ostatnie promienie s�o�ca znikaj� za "+
                                "horyzontem."; break;
                    case 1: str="�wiat powoli ogarnia zmrok."; break;
                    case 2: str="Czerwieniej�ca kula s�o�ca opada ku zachodowi.";
                            break;
                    case 3: str="S�o�ce zachodzi."; break;
                }
            }

            if(str)
            {
                str += "\n";
                tell_room(room, str);
            }

            ustaw_slonce(room);
            ustaw_niebo(room);
            ustaw_chmury(room);
            ustaw_gwiazdy(room);
            ustaw_ksiezyc(room);
            break;
        case MT_NOC:
        if(random(2))
            str="Nasta�a noc.";

        if(TEMPERATURA(room) > 21)
            str="Nasta�a gor�ca, duszna noc.";
        else
        {
            if(room->query_prop(ROOM_I_TYPE) & ROOM_TRACT == ROOM_TRACT)
                str="Ciemna noc zapada nad go�ci�cem.";
            else if(room->query_prop(ROOM_I_TYPE) & ROOM_IN_CITY == ROOM_IN_CITY)
                str="Nad miastem zapada ciemna noc.";
            else if(room->query_prop(ROOM_I_TYPE) & ROOM_FOREST == ROOM_FOREST)
                str="Ciemna noc zapada nad lasem.";
            else if(room->query_prop(ROOM_I_TYPE) & ROOM_MOUNTAIN == ROOM_MOUNTAIN)
                str="Ciemna noc zapada nad g�rami.";
        }

        if(str)
        {
            str+="\n";
            tell_room(room, str);
        }

        ustaw_slonce(room);
        ustaw_niebo(room);
        ustaw_chmury(room);
        ustaw_gwiazdy(room);
        ustaw_ksiezyc(room);
        break;
    }
}

/**
 * Funkcja operuj�ca na zbiorze lokacji o okre�lonej wielko�ci.
 *
 * @param x     - wsp�rz�dna geograficzna x obszaru
 * @param y     - wsp�rz�dna geograficzna y obszaru
 * @param locs  - zbi�r wszystkich lokacji
 * @param size  - maksymalny numer ostatniego elementu
 * @param func  - funkcja, kt�r� chcemy wywo�a� na ka�dej lokacji
 * @param args  - argumenty dla funkcji func
 */
varargs void
do_for_set(int x, int y, object* locs, int size, function func, mixed args = 0)
{

    int k = size;
    while ((k >= 0) && ((size - k) < LOC_SET_SIZE))
    {
        if (objectp(locs[k]))
           func(locs[k], ({ x, y }), args);

        --k;
    }
}

/**
 * Funkcja wykonuje dok�adnie to samo co do_for_set ale tylko wtedy
 * gdy spe�niony jest @param warunek
 *
 * @param warunek   - jaki warunek musi spe�ni� funkcja (dopuszczalne typy to vbfc string,
 *                    function w przeciwny wypadku sprawdzana jest prawda-fa�sz
 * @param x         - wsp�rz�dna geograficzna x obszaru
 * @param y         - wsp�rz�dna geograficzna y obszaru
 * @param locs      - zbi�r wszystkich lokacji
 * @param size      - maksymalny numer ostatniego elementu
 * @param func      - funkcja, kt�r� chcemy wywo�a� na ka�dej lokacji
 * @param args      - argumenty dla funkcji func
 */
varargs void
do_for_set_if(mixed warunek, int x, int y, object* locs, int size, function func, mixed args = 0)
{
    int k = size;
    int w;
    function a;

    if(!vbfcp(warunek) && !functionp(warunek))
        w = !warunek;
    else
        w = 2;

    while ((k >= 0) && ((size - k) < LOC_SET_SIZE))
    {
        if (objectp(locs[k]) && !w || (w == 2 && ((vbfcp(warunek) && call_other(locs[k], warunek)) ||
            (functionp(warunek) && (a = warunek)))))
        {
            if(a && a() || !a)
                func(locs[k], ({ x, y }), args);
        }

        --k;
    }
}

/**
 * Funkcja wywo�uje na ka�dej zarejestrowanej lokacji z danego obszaru
 * funkcj� przes�an� jako argument. Na planie pogodowym wywo�ywana jest
 * druga funkcja.
 *
 * @param x     - wsp�rz�dna geograficzna x obszaru
 * @param y     - wsp�rz�dna geograficzna y obszaru
 * @param func  - funkcja, kt�r� chcemy wywo�a� na ka�dej lokacji
 * @param args  - argumenty dla funkcji func
 * @param pfunc - funkcja, kt�r� chcemy wywo�a� dla ka�ego planu
 * @param pargs - argumenty dla funkcji pfunc
 */
varargs void
do_for_xy(int x, int y, function func, mixed args = 0, function pfunc = 0, mixed pargs = 0)
{
    int k, tabsize;
    object* tmptab;

    if (functionp(pfunc))
        if (mappingp(plany) && mappingp(plany[x]) && objectp(plany[x][x]))
            pfunc(plany[x][y], ({ x, y }), pargs);

    if (functionp(func))
    {
        if (!mappingp(rooms[x]))
            return;

        if (!pointerp(rooms[x][y]))
            return;

        tmptab = rooms[x][y];
        tabsize = sizeof(tmptab);

        for (k = tabsize-1; k >= 0; k -= LOC_SET_SIZE)
        {
            //Alarmik, �eby nam nie wyskoczy� eval - Krun
            set_alarm(0.1, 0.0, &do_for_set(x, y, tmptab, k, func, args));
        }
    }
}

/**
 * Funkcja robi dok�adnie to samo co funkcja do_for_xy z tym, �e wykonuje to
 * tylko je�li spe�niony jest warunek.
 *
 * @param warunek   - jaki warunek musi spe�ni� funkcja (dopuszczalne typy to vbfc string,
 *                    function w przeciwny wypadku sprawdzana jest prawda-fa�sz
 * @param x         - wsp�rz�dna geograficzna x obszaru
 * @param y         - wsp�rz�dna geograficzna y obszaru
 * @param func      - funkcja, kt�r� chcemy wywo�a� na ka�dej lokacji
 * @param args      - argumenty dla funkcji func
 * @param pfunc     - funkcja, kt�r� chcemy wywo�a� dla ka�ego planu
 * @param pargs     - argumenty dla funkcji pfunc
 */
varargs void
do_for_xy_if(mixed warunek, int x, int y, function func, mixed args = 0, function pfunc = 0, mixed pargs = 0)
{
    int k, tabsize;
    object* tmptab;

    if (functionp(pfunc))
        if (mappingp(plany) && mappingp(plany[x]) && objectp(plany[x][x]))
            pfunc(plany[x][y], ({ x, y }), pargs);

    if (functionp(func))
    {
        if (!mappingp(rooms[x]))
            return;

        if (!pointerp(rooms[x][y]))
            return;

        tmptab = rooms[x][y];
        tabsize = sizeof(tmptab);

        for (k = tabsize-1; k >= 0; k -= LOC_SET_SIZE)
        {
            //Alarmik, �eby nam nie wyskoczy� eval - Krun
            set_alarm(0.1, 0.0, &do_for_set_if(warunek, x, y, tmptab, k, func, args));
        }
    }
}

/**
 * Funkcja wywo�uje na ka�dej zarejestrowanej lokacji funkcj�
 * przes�an� jako argument. Na planach pogodowych wywo�ywana jest
 * druga funkcja.
 *
 * @param func  - funkcja, kt�r� chcemy wywo�a� na ka�dej lokacji
 * @param args  - argumenty dla funkcji func
 * @param pfunc - funkcja, kt�r� chcemy wywo�a� dla ka�ego planu
 * @param pargs - argumenty dla funkcji pfunc
 */
varargs void
do_for_each(function func, mixed args = 0, function pfunc = 0, mixed pargs = 0)
{
    int i, j, k, xallo, yallo, tabsize;
    int *xindices, *yindices;
    mapping tmpmap;
    object* tmptab;

    if (!functionp(func) && !functionp(pfunc))
        return;

    xindices = m_indices(rooms);
    xallo = sizeof(xindices);

    for (i = 0; i < xallo; ++i)
    {
        tmpmap = rooms[xindices[i]];
        yindices = m_indices(tmpmap);
        yallo = sizeof(yindices);

	//niez�y bug tu by�, do do_for_xy jako argumenty by�o dawany i,j zamiast
	//xindices i yindices.... Vera.
        for (j = 0; j < yallo; ++j)
            set_alarm(0.1, 0.0, &do_for_xy(xindices[i], yindices[j], func, args, pfunc, pargs));
    }
}

/**
 * Funkcja robi dok�adnie to samo co funkcja do_fer_each z tym wyj�tkiem, �e dzia�a tylko wtedy
 * gdy spe�niony jest podany warunek.
 *
 * @param warunek   - jaki warunek musi spe�ni� funkcja (dopuszczalne typy to vbfc string,
 *                    function w przeciwny wypadku sprawdzana jest prawda-fa�sz
 * @param func      - funkcja, kt�r� chcemy wywo�a� na ka�dej lokacji
 * @param args      - argumenty dla funkcji func
 * @param pfunc     - funkcja, kt�r� chcemy wywo�a� dla ka�ego planu
 * @param pargs     - argumenty dla funkcji pfunc
 */
varargs void do_for_each_if(mixed warunek, function func, mixed args = 0, function pfunc = 0, mixed pargs = 0)
{
    int i, j, k, xallo, yallo, tabsize;
    int *xindices, *yindices;
    mapping tmpmap;
    object* tmptab;

    if (!functionp(func) && !functionp(pfunc))
        return;

    xindices = m_indices(rooms);
    xallo = sizeof(xindices);

    for (i = 0; i < xallo; ++i)
    {
        tmpmap = rooms[xindices[i]];
        yindices = m_indices(tmpmap);
        yallo = sizeof(yindices);

        for (j = 0; j < yallo; ++j)
        {
            //Ten alarmik, �eby nie prze�adowalo evala - Krun
            set_alarm(0.1, 0.0, &do_for_xy_if(warunek, i, j, func, args, pfunc, pargs));
        }
    }
}

/*
 * Nazwa funkcji: main_func
 * Opis         : Funkcja jest odpowiedzialna za eventy zwi�zane z czasem.
 */
void
main_func()
{
    int curr_time = MT_PORA_DNIA;

    if (curr_time != last_time)
        do_for_each(zmiana_pory);

    last_time = curr_time;
}

/*
 * Nazwa funkcji: check_alarm
 * Opis         : Funkcja sprawdza, czy g��wny alarm jest ustawiony. Je�eli nie,
 *                to ustawia odpowiednie zmienne i go inicjalizuje.
 */
void
check_alarm()
{
    if(!main_alarm)
    {
        last_time = MT_PORA_DNIA;
        //Pierwszy po minucie, potem co 5 minut
        main_alarm = set_alarm(60.0, 300.0, main_func);
    }
}

/*
 * Nazwa funkcji: check_pogoda
 * Opis         : Funkcja sprawdza, czy dla danego obszaru jest przygotowany plan pogodowy.
 *                Je�eli nie ma, to przygotowywany jest standardowy.
 * Argumenty    : int x - wsp�rz�dna geograficzna x obszaru
 *                int y - wsp�rz�dna geograficzna y obszaru
 */
void
check_pogoda(int x, int y)
{
    if (!mappingp(plany))
        plany = ([ ]);

    if (!mappingp(plany[x]))
        plany[x] = ([ ]);

    if (!objectp(plany[x][y]))
    {
        object new_plan;
        new_plan = SECURITY->load_planpogody();
        new_plan->init_pogoda();
        register_plan(x, y, new_plan);
    }
}

/*
 * Nazwa funkcji: register_room
 * Opis         : Funkcja rejestruje pomieszczenie. Dodaje w nim rzeczy zwi�zane
 *                z otoczeniem i pogod�.
 * Argumenty    : object room - pomieszczenie do zarejestrowania
 */
void
register_room(object room)
{
    int x, y;

    if (!objectp(room))
        return;

    if (!mappingp(rooms))
        rooms = ([ ]);

    x = room->query_prop(ROOM_I_WSP_X);
    y = room->query_prop(ROOM_I_WSP_Y);

    if (!mappingp(rooms[x]))
        rooms[x] = ([ ]);

    if (!pointerp(rooms[x][y]))
        rooms[x][y] = ({ });

    rooms[x][y] -= ({ 0 });
    rooms[x][y] += ({ room });
    check_pogoda(x, y);
    check_alarm();

    ustaw_slonce(room);
    ustaw_niebo(room);
    ustaw_chmury(room);
    ustaw_gwiazdy(room);
    ustaw_ksiezyc(room);
}

/*
 * Nazwa funkcji: plan_func
 * Opis         : Funkcja jest odpowiedzialna za eventy pogodowe dla danego planu.
 * Argumenty    : object plan - rozwa�any plan pogodowy
 */
void
plan_func(object plan)
{
    int res, *args, i, size;
    string toWrite;
    if (!objectp(plan))
        return;

    if (!mappingp(rplany) || (!sizeof(rplany[file_name(plan)])))
        return;

    args = ({plan->get_zachmurzenie(), plan->get_opady(), plan->get_wiatry(), plan->get_temperatura()});
    res = plan->next_step();
    args += ({plan->get_zachmurzenie(), plan->get_opady(), plan->get_wiatry(), plan->get_temperatura()});

    toWrite = opis_eventow(plan, args);
    if (stringp(toWrite) && (strlen(toWrite)))
    {
        size = sizeof(rplany[file_name(plan)]);

        for (i = 1; i < size; i++)
            do_for_xy(rplany[file_name(plan)][i][0], rplany[file_name(plan)][i][1], pisz_do_roomu_zew, toWrite);
    }

    if (res)
        rplany[file_name(plan)][0] = set_alarmv(itof(res), 0.0, "plan_func", ({ plan }) );
    else
        rplany[file_name(plan)][0] = 0;
}

/*
 * Nazwa funkcji: register_plan
 * Opis         : Funkcja rejestruje plan pogodowy.
 * Argumenty    : int x - wsp�rz�dna geograficzna x obszaru
 *                int y - wsp�rz�dna geograficzna y obszaru
 *                object plan - plan do zarejestrowania
 */
void
register_plan(int x, int y, object plan)
{
    if (!mappingp(plany))
        plany = ([ ]);

    if (!mappingp(plany[x]))
        plany[x] = ([ ]);

    if (!mappingp(rplany))
        rplany = ([ ]);

    if (objectp(plany[x][y]))
    {
        if (pointerp(rplany[file_name(plany[x][y])]))
            rplany[file_name(plany[x][y])] -= ({ ({ x, y }) });

        if (sizeof(rplany[file_name(plany[x][y])]) == 1)
        {
            if (rplany[file_name(plany[x][y])][0])
            {
                remove_alarm(rplany[file_name(plany[x][y])][0]);
                rplany[file_name(plany[x][y])][0] = 0;
            }
            m_delete(rplany, file_name(plany[x][y]));
        }
    }
    plany[x][y] = plan;
//DBG("plan_func odpalamy dla : x="+x+", y="+y+"\n");
    if (!pointerp(rplany[file_name(plan)]))
        rplany[file_name(plan)] = ({ set_alarmv(1.0, 0.0, "plan_func", ({ plan }) ) });

    rplany[file_name(plan)] += ({ ({ x, y }) });

    plany[x][y]->ustaw_wspolrzedne(x, y);
}

/*
 * Nazwa funkcji: print_plan_info_short
 * Opis         : Funkcja wypisuje kr�tkie info o planie pogodowym.
 * Argumenty    : object plan - plan pogodowy
 *                int* wsp - wsp�rz�dne geograficzne planu
 *                mixed parg - dodatkowy argument (nieu�ywany)
 */
void
print_plan_info_short(object plan, int* wsp, mixed parg)
{
    string pre;

    if (!objectp(plan))
        return;

    if (sizeof(wsp) == 2)
        pre = "*** (" + sprintf("%3|i", wsp[0]) + ", " + sprintf("%3|i", wsp[1]) + ") ";
    else
        pre = "*** (---) ";

    write(pre + plan->short() + " [" + file_name(plan) + "]\n");
}

/*
 * Nazwa funkcji: print_room_info_short
 * Opis         : Funkcja wypisuje kr�tkie info o lokacji.
 * Argumenty    : object room - lokacja
 *                int* wsp - wsp�rz�dne geograficzne lokacji
 *                mixed arg - dodatkowy argument (nieu�ywany)
 */

void
print_room_info_short(object room, int* wsp, mixed arg)
{
	string pre;

	if (!objectp(room))
	{
		return;
	}
	if (sizeof(wsp) == 2)
	{
		pre = "(" + sprintf("%3|i", wsp[0]) + ", " + sprintf("%3|i", wsp[1]) + ") ";
	}
	else
	{
		pre = "(---) ";
	}
	write(pre + room->short() + " [" + file_name(room) + "]\n");
}

/*
 * Nazwa funkcji: print_rooms
 * Opis         : Funkcja wypisuje informacje o zarejestrowanych pomieszczeniach.
 */

void
print_rooms()
{
    do_for_each(print_room_info_short, 0, print_plan_info_short);
}

/*
 * Nazwa funkcji: remove_all_rooms
 * Opis         : Funkcja zapisuje wszystkie zarejestrowane obiekty pomieszcze�.
 */

void
remove_all_rooms()
{
    int i, j, k, xallo, yallo, tabsize;
    int *xindices, *yindices;
    mapping tmpmap;
    object* tmptab;

    xindices = m_indices(rooms);
    xallo = sizeof(xindices);
    for (i = 0; i < xallo; ++i)
    {
        tmpmap = rooms[xindices[i]];
        yindices = m_indices(tmpmap);
        yallo = sizeof(yindices);

        for (j = 0; j < yallo; ++j)
        {
            tmptab = tmpmap[yindices[j]];
            tabsize = sizeof(tmptab);
            for (k = tabsize-1; k >= 0; k -= 1)
            {
                if (objectp(tmptab[k]))
                tmptab[k]->remove_room();
            }
        }
    }
    rooms = ([ ]);
}

object
get_warunki()
{
    return clone_object("/d/Standard/pogoda/warunki.c");
}

mixed
check_pogoda_alarm(string plan_name)
{
    write("Plan: " + plan_name + " alarm_id: " + rplany[plan_name][0] + "\n");
    return get_alarm(rplany[plan_name][0]);
}
