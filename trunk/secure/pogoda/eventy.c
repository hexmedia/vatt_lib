/**
 * \file /secure/pogoda/eventy.c
 *
 * Plik odpowiedzialny za funkcje zwi�zane z eventami pogodowymi.
 *
 * TODO:
 * - <i> Przyda�oby si� dopisa� jakie� eventy dla zachmurzenia ca�kowitego </i>
 */

#include <macros.h>
#include <mudtime.h>
#include <pogoda.h>
#include <stdproperties.h>

#define CAN_SEE_EV(x)      (!(x)->query_prop(EYES_CLOSED) || CAN_SEE_IN_ROOM((x)))
    //Musimy sprawdzi� czy gracz mo�e zobaczy� pewne eventy:)

/*�ci�ga:
 *  OLD:         ZMIANA NA:     CO:
 *  gArgs[0]  i  gArgs[4]   -   zachmurzenie.
 *  gArgs[1]  i  gArgs[5]   -   opady
 *  gArgs[2]  i  gArgs[6]   -   wiatr
 *  gArgs[3]  i  gArgs[7]   -   temperatura
 */

static int* gArgs;

string
ev_zachmurzenie_zerowe()
{
    string str="";

    if((ENV(TP)->pora_dnia() > MT_SWIT && ENV(TP)->pora_dnia() <= MT_POPOLUDNIE) &&  //dzie�
        (gArgs[7] >= 29 && gArgs[4] <= POG_ZACH_LEKKIE)) //ciep�o i bez zachmurzenia
    {
        str = "S�o�ce pra�y niemi�osiernie...";
    }

    if(strlen(str))
        str += "\n";

    return str;
}

string
ev_zachmurzenie_lekkie()
{
    string str = "";
    int i = 5;

    switch(random(i+2))
    {
        case 0:
            if(CAN_SEE_EV(TP))
                str = "Nieliczne chmurki przesuwaj� si� powoli po niebie.";
            break;
        case 1:
            if(CAN_SEE_EV(TP))
                str = "Ma�e ob�oczki sun� szybko po niebie.";
            break;
        case 2:
            if(CAN_SEE_EV(TP))
                str = "Chmura szybko przep�ywa po niebie.";
                break;
        case 3:
            if(CAN_SEE_EV(TP))
                str = "Ma�a samotna chmurka sunie po niebie.";
            break;
        case 4:
            if(CAN_SEE_EV(TP))
            {
                if(ENV(TP)->pora_dnia() == MT_WIECZOR && ENV(TP)->pora_roku() == MT_LATO)
                    str = "Zza chmur wy�ania si� s�o�ce roz�wietlaj�c "+
                        "�wiat na czerwono.";
            }
            break;
        case 5:
            if(CAN_SEE_EV(TP))
            {
                if(ENV(TP)->pora_dnia() >= MT_RANEK && ENV(TP)->pora_dnia() <= MT_WIECZOR &&
                    (ENV(TP)->pora_roku() == MT_LATO || ENV(TP)->pora_roku() == MT_WIOSNA))
                {
                    if(TEMPERATURA(ENV(TP)) > 25 && random(2))
                        str = "Chmura, kt�ra na chwil� zas�oni�a s�o�ce sprawi�a, �e zrobi�o si� nieco ch�odniej.";
                    else if(TEMPERATURA(ENV(TP)) > 25)
                        str = "Z nieba leje si� �ar, kiedy s�o�ce wychyla si� zza chmur.";
                    else
                        str = "S�o�ce wynurzy�o si� zza chmur, na chwil� ogrzewaj�c swym ciep�em ca�� krain�.";
                }
            }
            break;
    }

    if(strlen(str))
        str += "\n";

    return str;
}

string
ev_zachmurzenie_srednie()
{
    string str="";
    int i = 3;

    switch(random(i))
    {
        case 0:
            return ev_zachmurzenie_lekkie();
        case 1:
            if(CAN_SEE_EV(TP))
            {
                if(ENV(TP)->pora_dnia() > MT_SWIT &&
                    ENV(TP)->pora_dnia() < MT_POZNY_WIECZOR)
                    str="Ciemne chmury przes�oni�y na chwil� s�o�ce.";
                else
                    str = "Ciemne chmury przes�oni�y na chwil� ksi�yc.";
            }
            break;
        case 2:
            if(CAN_SEE_EV(TP))
            {
                if(gArgs[6] <= POG_WI_SREDNIE)
                {
                    if(ENV(TP)->pora_roku() == MT_JESIEN || ENV(TP)->pora_roku() == MT_ZIMA)
                        str = "Liczne szare chmury leniwie sun� po niebie.";
                    else
                        str = "Liczne chmury leniwie sun� po niebie.";
                }
                else
                    str = "Liczne chmury szybko sun� po niebie.";
                break;
            }
    }

    if(strlen(str))
        str += "\n";

    return str;
}

string
ev_zachmurzenie_duze()
{
    string str="";

    if(ENV(TP)->pora_dnia() == MT_NOC)
    {
        switch(random(2))
        {
            case 0:
                if(CAN_SEE_EV(TP))
                    str="Jaka� chmura przes�oni�a ksi�yc.";
                break;
            case 1:
                if(CAN_SEE_EV(TP))
                {
                    str="Czarne chmury przes�oni�y ksi�yc i gwiazdy. "+
                        "Nie wida� niemal nic.";
                }
                break;
        }
    }

    if(strlen(str))
        str+="\n";

    return str;
}

string
ev_zachmurzenie_calkowite()
{
    string str="";

    if(strlen(str))
        str+="\n";

    return str;
}

string
ev_zmiana_zachmurzenia()
{
    string str="";

    if(gArgs[0] > gArgs[4] && gArgs[4] < POG_ZACH_SREDNIE) //wychodzi s�o�ce
    {
        if(ENV(TP)->pora_dnia()>=MT_RANEK &&
            ENV(TP)->pora_dnia()<=MT_WIECZOR)
        {
            switch(random(9))
            {
                case 0:
                    if(CAN_SEE_EV(TP))
                        str ="Pierwsze promienie s�o�ca omywaj� Ci twarz.";
                    break;
                case 1:
                    if(CAN_SEE_EV(TP))
                        str="Zza chmur wy�ania si� s�o�ce.";
                    break;
                case 2:
                    if(CAN_SEE_EV(TP))
                    {
                        if(ENV(TP)->pora_roku()==MT_WIOSNA)
                            str="Ciep�e wiosenne s�o�ce wy�ania si� zza chmur.";
                        else if(ENV(TP)->pora_roku()==MT_ZIMA)
                            str="Nie�mia�e, zimowe s�o�ce przebija si� przez "+
                                "warstwy chmur.";
                    }
                    break;
                case 3:
                    if(CAN_SEE_EV(TP))
                        str="S�o�ce wy�ania si� zza chmur.";
                    break;
                case 4:
                    if(CAN_SEE_EV(TP))
                        str="S�o�ce wy�ania si� zaa chmur o�lepiaj�c ci� na chwil�.";
                    break;
                case 5:
                    if(CAN_SEE_EV(TP))
                    {
                        str="S�o�ce leniwie wynurza si� zza chmur, zalewaj�c swymi "+
                            "promieniami ca�� okolic�.";
                    }
                    break;
                case 6:
                    if(CAN_SEE_EV(TP))
                        str="Chmury rozsuwaj� si� na boki ukazuj�c s�o�ce.";
                    break;
                case 7:
                    if(CAN_SEE_EV(TP))
                    {
                        str="Wszystko dooko�a si� rozja�ni�o, gdy� s�o�ce wysz�o "+
                            "zza chmur.";
                    }
                    break;
                case 8:
                    if(CAN_SEE_EV(TP))
                    {
                        str="Pojedyncze promienie s�o�ca staraj� si� przebi� "+
                            "przez ciemne chmury";
                    }
                    break;
                //potrzebne sprawdzanie zasloniecia slotu twarzy..
                //Slonce wylania sie zza chmur ogrzewajac ci twarz.
            }
        }
        else
        {
            switch(random(2))
            {
                case 0:
                    if(CAN_SEE_EV(TP))
                        str="Blask ksi�yca stara si� przebi� przez ciemne chmury";
                    break;
                case 1:
                    if(CAN_SEE_EV(TP))
                        str="Ksi�yc wychyla si� zza chmur.";
                    break;
            }
        }
    }
    else if(gArgs[0] < gArgs[4] && gArgs[4] > POG_ZACH_SREDNIE) //chowa si� s�o�ce
    {                                        //& zbieraj� si� chmury
        switch(random(3))
        {
            case 0:
                if(CAN_SEE_EV(TP))
                {
                    if(ENV(TP)->dzien_noc())
                        str="Gruba warstwa chmur zas�oni�a "+ random(2)?"ksi�yc":"gwiazdy"+".";
                    else if(random(2))
                        str="Gruba warstwa chmur zas�oni�a s�o�ce.";
                    else
                        str="S�o�ce chowa si� za chmury.";
                }
                break;
            case 1:
                if(CAN_SEE_EV(TP))
                {
                    if(!ENV(TP)->dzien_noc())
                    {
                        str="S�o�ce chowa si� za chmury powoduj�c �e "+
                            "robi ci si� ch�odniej.";
                    }
                }
                break;
            case 2:
                if(CAN_SEE_EV(TP))
                    str="K�tem oka dostrzegasz zbieraj�ce si� ciemne chmury.";
                break;
        }

        if(gArgs[4] == POG_ZACH_CALKOWITE)
        {
            switch(random(5)) //by� mo�e b�dzie pada�
            {
                case 0:
                    if(CAN_SEE_EV(TP))
                    {
                        if(ENV(TP)->pora_roku() == MT_ZIMA)
                        {
                            str="Ciemne chmury zasnuwaj� ca�e niebo, zwiastuj�c "+
                                "nadej�cie zamieci �nie�nej.";
                        }
                        else
                        {
                            str="Czarne chmury zaczynaj� zbiera� si� na niebie "+
                                "zwiastuj�c nadchodz�cy deszcz.";
                        }
                    }
                    break;
                case 1:
                    if(CAN_SEE_EV(TP))
                    {
                        if(ENV(TP)->pora_dnia() == MT_NOC)
                            str="Ciemne chmury zas�aniaj� ksi�yc czyni�c "+
                                "mrok jeszcze bardziej ciemnym.";
                        else
                            str="Ci�kie, warstwowe chmury przys�oni�y niebo.";
                    }
                    break;
                case 2:
                    if(CAN_SEE_EV(TP))
                        str="Na niebie zbieraj� si� deszczowe chmury.";
                    break;
                case 3:
                    if(CAN_SEE_EV(TP))
                        str="Nad g�owami zbieraj� si� deszczowe chmury.";
                    break;
                case 4:
                    if(CAN_SEE_EV(TP))
                    {
                        str="Czarne chmury zaczynaj� zas�ania� niebo... "+
                            "chyba b�dzie pada�.";
                    }
                    break;
            }
        }
    }

    if(strlen(str))
        str+="\n";

    return str;
}

/*�ci�ga:
 *  OLD:         ZMIANA NA:     CO:
 *  gArgs[0]  i  gArgs[4]   -   zachmurzenie.
 *  gArgs[1]  i  gArgs[5]   -   opady
 *  gArgs[2]  i  gArgs[6]   -   wiatr
 *  gArgs[3]  i  gArgs[7]   -   temperatura
 */
string
ev_zmiana_temperatury()
{
    string str;
    int roznica = gArgs[3] - gArgs[7];

    if(gArgs[3] > gArgs[7] && roznica >= 7) //och�odzenie, o 7 stopni
    {
        switch(random(3))
        {
            case 0: str = "Zrobi�o si� znacznie ch�odniej.";                                break;
            case 1: str = "Znacznie si� och�odzi�o.";                                       break;
            case 2: str = "Masz wra�enie, �e jest ju� znacznie ch�odniej, ni� wcze�niej.";  break;
        }
    }
    else if(gArgs[3] > gArgs[7]) //och�odzenie, o mniej stopni
    {
        switch(random(3))
        {
            case 0: str = "Zrobi�o si� nieco ch�odniej.";                   break;
            case 1: str = "Powietrze zrobi�o si� nieco ch�odniejsze.";      break;
            case 2: str = "Masz wra�enie, �e robi si� odrobin� ch�odniej."; break;
        }
    }
    else if(gArgs[3] < gArgs[7] && roznica >= 7) //ocieplenie, o du�o
    {
        switch(random(3))
        {
            case 0: str = "Zrobi�o si� znacznie cieplej.";  break;
            case 1: str = "Robi si� zdecydowanie cieplej."; break;
            case 2: str = "Jest ju� znacznie cieplej.";     break;
        }
    }
    else if(gArgs[3] < gArgs[7]) //ocieplenie, o ma�o
    {
        switch(random(3))
        {
            case 0: str = "Zrobi�o si� nieco cieplej.";     break;
            case 1: str = "Zaczyna si� ociepla�.";          break;
            case 2: str = "Robi si� coraz cieplej.";        break;
        }
    }
    else
    {
        str = "Temperatura si� zmienia, lecz w nieprzewidziany spos�b, "+
            "by to opisa�. Prosz� zg�o� to jako b��d globalny!";
    }

    str+="\n";
    return str;
}

string
ev_opady_deszcz_lekkie()
{
    string str="";

    switch(random(4+1))
    {
        case 0:
            if(CAN_SEE_EV(TP))
                str = "Malutkie kropelki deszczu spadaj� na ziemi�.";
            break;
        //UNIWERSALNE, dla innych opad�w te�:
        case 1:
            if(CAN_SEE_EV(TP))
                str = "Du�e krople deszczu rozbryzguj� si� na ziemi.";
            else
                str = "Czujesz jak du�e krople deszczu rozbryzguj� si� na twoim ciele.";
            break;
        case 2:
            if(CAN_SEE_EV(TP))
                str = "Krople deszczu spadaj� z nieba.";
            break;
        case 3:
            str = "Krople deszczu uderzaj� miarowo o pod�o�e.";
            break;
        default: str=""; break;
    }

    if(strlen(str))
        str+="\n";

    return str;
}
string
ev_opady_deszcz_srednie()
{
    string str="";

    switch(random(3))
    {
        //UNIWERSALNE, dla innych opad�w te�:
        case 0:
            if(CAN_SEE_EV(TP))
                str = "Du�e krople deszczu rozbryzguj� si� na ziemi.";
            else
                str = "Czujesz jak du�e krople deszczu rozbryzguj� si� na twoim ciele.";
            break;
        case 1:
            if(CAN_SEE_EV(TP))
                str = "Krople deszczu spadaj� z nieba.";
            break;
        case 2:
            str = "Krople deszczu uderzaj� miarowo o pod�o�e.";
            break;
    }

    if(strlen(str))
        str+="\n";

    return str;
}
string
ev_opady_deszcz_ciezkie()
{
    string str = "";

    switch(random(4))
    {
        case 0:
            if(ENV(TP)->pora_roku() == MT_JESIEN || ENV(TP)->pora_roku() ==MT_ZIMA)
                str = "Lodowaty deszcz sp�ywa g�stymi strugami.";
            break;
        //UNIWERSALNE, dla innych opad�w te�:
        case 1:
            str = "Du�e krople deszczu rozbryzguj� si� na ziemi.";
            break;
        case 2:
            str = "Krople deszczu spadaj� z nieba.";
            break;
        case 3:
            str = "Krople deszczu uderzaj� miarowo o pod�o�e.";
            break;
    }


    if(strlen(str))
        str += "\n";

    return str;
}

string
ev_opady_deszcz_ogromne()
{
    string str="";

    switch(random(4))
    {
        case 0:
            str = "Deszcz leje niemi�osiernie, a burzowe grzmoty szalej� woko�o.";
            break;
        case 1:
            if(ENV(TP)->pora_roku() == MT_JESIEN || ENV(TP)->pora_roku() ==MT_ZIMA)
                str = "Lodowaty deszcz sp�ywa g�stymi strugami.";
            break;
        //UNIWERSALNE, dla innych opad�w te�:
        case 2:
            str = "Du�e krople deszczu rozbryzguj� si� na ziemi.";
            break;
        case 3:
            str = "Zdaje si�, �e ulewa rozp�ta�a si� na dobre.";
            break;
    }

    if(strlen(str))
        str += "\n";

    return str;
}
string
ev_opady_snieg_lekkie()
{
    string str="";

    switch(random(2+1))
    {
        case 0:
            str = "Drobne p�atki �niegu ta�cz� weso�o na wietrze.";
            break;
        case 1:
            if(CAN_SEE_EV(TP))
            {
                str = "Delikatne p�atki �niegu wiruj� wok� ciebie w "+
                    "sobie tylko znanym ta�cu.";
            }
            break;
        default:
            str = "";
            break;
    }


    if(strlen(str))
        str += "\n";

    return str;
}
string
ev_opady_snieg_srednie()
{
    string str = "";

    switch(random(3+1))
    {
        case 0:
            if(CAN_SEE_EV(TP))
                str = "�nieg z deszczem zamienia pod�o�e w zimn� brej�.";
            break;
        case 1:
            if(CAN_SEE_EV(TP))
                str = "�nieg sypie jednostajnie.";
            break;
        case 2:
            if(CAN_SEE_EV(TP))
            {
                str = "�nieg osiada na ziemi okrywaj�c ca�� "+
                    "krain� bia�ym puchem.";
            }
            break;
        default:
            str=""; break;
    }

    if(strlen(str))
        str+="\n";

    return str;
}
string
ev_opady_snieg_ciezkie()
{
    string str="";

    switch(random(3))
    {
        case 0:
            if(CAN_SEE_EV(TP))
                str ="Nie wida� niemal nic zza bia�ej kurtyny �niegu.";
            break;
        case 1:
            str = "�nie�na zamie� ciska ci� w zasp�.";
            break;
        case 2:
            if(CAN_SEE_EV(TP))
            {
                str = "�nieg osiada na ziemi okrywaj�c ca�� krain� "+
                    "bia�ym puchem.";
            }
            break;
    }

    if(strlen(str))
        str += "\n";

    return str;
}
string
ev_opady_grad_lekkie()
{
    string str = "";

    switch(random(2+1))
    {
        case 0:
            if(CAN_SEE_EV(TP))
                str = "Z nieba spada ma�y grad stukaj�c o pod�o�e.";
            break;
        case 1:
            if(CAN_SEE_EV(TP))
            {
                str = "Kuleczki drobnego gradu spadaja z nieba w "+
                    "towarzystwie ulewnego deszczu";
            }
            break;
    }

    if(strlen(str))
        str += "\n";

    return str;
}

string
ev_opady_grad_srednie()
{
    string str = "";

    switch(random(1+1))
    {
        case 0:
            str = "Z nieba spadaj� wielkie kule gradu rani�c Ci� lekko!";
            TP->reduce_hp(10, random(1000), 10);
            break;
        default:
            str = "";
            break;
    }

    if(strlen(str))
        str += "\n";

    return str;
}

string
ev_zmiana_opadow()
{
    string str = "";

    if(CO_PADA(ENV(TP)) == PADA_DESZCZ &&
       MOC_OPADOW(ENV(TP)) >= PADA_LEKKO && gArgs[1] == NIC_NIE_PADA)
    {
        if(ENV(TP)->pora_roku()==MT_LATO)
            str = "Nagle z nieba spada ciep�y letni deszczyk.";
        else
        {
            if(MOC_OPADOW(ENV(TP)) > PADA_SREDNIO)
                str = "Ca�y �wiat nagle przys�ania "+
                    "pot�na nawa�nica deszczu.";
            else
                str = "Nagle z nieba spada zimny deszcz.";
        }
    }
    else if(CO_PADA(ENV(TP)) == PADA_DESZCZ && gArgs[1] == NIC_NIE_PADA)
    {
        switch(random(2))
        {
        case 0:
            if(TEMPERATURA(ENV(TP)) >= 18)
            {
                if(MOC_OPADOW(ENV(TP)) < PADA_SREDNIO)
                    str="Zaczyna pada� drobny, ciep�y deszcz.";
                else
                    str="Zaczyna pada� ciep�y deszcz.";
            }
            else
            {
                if(random(2))
                    str="Zaczyna pada�.";
                else
                {
                    if(TEMPERATURA(ENV(TP)) < 10)
                        str = "Ch�odne krople deszczu rozbijaj� si� o twoje cia�o";
                    else
                        str = "Krople deszczu rozbijaj� si� o twoje cia�o";
                }
            }
            break;
        case 1:
            if(MOC_OPADOW(ENV(TP)) > PADA_SREDNIO)
            {
                if(random(2))
                    str = "Zaczyna la�.";
                else
                    str = "Z nieba zrywa si� gwa�towna ulewa.";
            }
            else
                str = "Zaczyna lekko pada�.";
            break;
        }

        if(random(4) == 2)
            str = "Nagle zacz�o pada�.";
    }
    else if(CO_PADA(ENV(TP)) == PADA_SNIEG && gArgs[1] == NIC_NIE_PADA)
    {
        if(random(2))
            str = "Zaczyna pruszy� �nieg.";
        else
            str = "Co� zimnego spad�o na tw�j nos. Zaraz zacz�� pada� �nieg.";
    }
    else if(gArgs[1] > NIC_NIE_PADA && gArgs[5] == NIC_NIE_PADA)
    {
        if(CAN_SEE_EV(TP))
        {
            if((ENV(TP)->pora_dnia() != MT_NOC || ENV(TP)->pora_dnia() != MT_POZNY_WIECZOR ||
                ENV(TP)->pora_dnia() != MT_SWIT))
            {
                if(random(2) && ENV(TP)->pora_roku() != MT_JESIEN &&
                ENV(TP)->pora_roku() != MT_ZIMA)
                    str = "W oddali pojawia si� kolorowa t�cza.";
                else
                    str = "Ostatnie krople deszczu opadaj� na ziemi�, a "+
                        "zza chmur wy�aniaj� si� nie�mia�e promienie s�o�ca.";
            }
        }
    }


    if(strlen(str))
        str += "\n";

    return str;
}


string
ev_wiatry_zerowe()
{
    string str = "";

    if(ENV(TP)->pora_roku() == MT_LATO && gArgs[6] == POG_WI_ZEROWE && random(2))
    {
        str = "Powietrze jest ci�kie i duszne i zdaje si� by� "+
            "zupe�nie nieruchome.";
    }

    if(strlen(str))
        str += "\n";

    return str;
}
string
ev_wiatry_lekkie()
{
    string str = "";

    switch(random(5))
    {
        case 0:
            if(ENV(TP)->pora_roku() == MT_WIOSNA || ENV(TP)->pora_roku() == MT_LATO)
                str = "Ciep�y wietrzyk owiewa ci� przyjemnie.";
            else
                str = "Zimny wiatr wieje ci w twarz.";
            break;
        case 1:
            if(ENV(TP)->query_prop(ROOM_I_TYPE) & ROOM_TRACT == ROOM_TRACT)
            {
                if(TP->query_prop(EYES_CLOSED))
                    str = "Kurz, poderwany z go�ci�ca, drapie ci� w gardle i wyciska �zy z oczu.";
                else
                    str = "Kurz, poderwany z go�ci�ca, drapie ci� w gardle.";
            }
        case 2:
            if(TP->query_prop(EYES_CLOSED))
            {
                str = "Kilka ziarenek piasku niesionych przez wiatr "+
                    "sypie ci w twarz. Uf, ca�e szcz�cie, �e masz "+
                    "zamkni�te oczy.";
            }
            else
            {
                str = "Kilka ziarenek piasku niesionych przez wiatr "+
                    "wpada ci do oczu.";
            }
            break;
        case 3:
            if(TEMPERATURA(ENV(TP)) >= 10)
                   str="Lekki wiatr muska ci� po twarzy.";
            else if(TEMPERATURA(ENV(TP)) < 6)
            {
                switch(random(4))
                {
                    case 0:
                        str = "Ch�odny wiatr muska twoj� twarz.";
                        break;
                    case 1:
                        str = "Przejmuj�cy ch��d ogarn�� twoje cia�o.";
                        break;
                    case 2:
                        str = "Przez twoje cia�o przebiega dreszcz zimna.";
                        break;
                    case 3:
                        str = "Ch�odny wiatr ogarn�� twe cia�o.";
                        break;
                }
            }
            else
                str = "Ch�odny powiew wiatru musn�� twoje cia�o.";
            break;
    }

    if(strlen(str))
        str += "\n";

    return str;
}
string
ev_wiatry_srednie()
{
    string str="";

    switch(random(3+1))
    {
        case 0:
            if(TEMPERATURA(ENV(TP)) < 18)
                str="Zimny wiatr wieje ci w twarz.";
            else
                str="Lekka bryza przyjemnie ch�odzi twoj� twarz.";
            break;
        case 1:
            if(CO_PADA(ENV(TP)) == PADA_SNIEG)
                str="Silniejszy podmuch wiatru rozsypuje �nieg dooko�a.";
            else if(TEMPERATURA(ENV(TP)) < 10)
                str="Przejmuj�cy ch��d ogarn�� twoje cia�o.";
            break;
        case 2:
            if(random(2) && TP->query_dlugosc_wlosow() >= 10.0)
                str="Wiatr rozwiewa nieco twoje w�osy.";
            else if(sizeof(TP->query_worn() >= 2))
                str="Nag�y wiatr szarpie twoje ubranie.";
            break;
    }

    if(strlen(str))
        str+="\n";

    return str;
}
string
ev_wiatry_duze()
{
    string str="";

    switch(random(2+1))
    {
        case 0: str="Czujesz nag�y podmuch mro�nego wiatru.";
                break;
        case 1:
            if(!TP->query_prop(EYES_CLOSED))
                str="Silny wiatr wyciska ci �zy z oczu.";
            break;
    }

    if(strlen(str))
        str+="\n";

    return str;
}
string
ev_wiatry_bduze()
{
    string str="";

    if(random(2))
        str="Gwa�towny podmuch wiatru zmusza ci� do post�pienia kilku "+
            "krok�w w ty�.";

    if(strlen(str))
        str+="\n";

    return str;
}
string
ev_zmiana_wiatru()
{
    string str="";

    if(gArgs[2] < gArgs[3])
    {
        if(random(2))
            str="Wiatr ustaje.";
    }
    else
    {
        switch(random(3))
        {
            case 0:
                if(TEMPERATURA(ENV(TP)) < 11)
                    str = "Zerwa� si� zimny wiatr.";
                else
                    str = "Wiatr narasta.";
                break;
            case 1:
                str = "Wiatr nasila si�.";
                break;
            default:
                str = "";
                break;
        }
    }

    if(strlen(str))
        str+="\n";

    return str;
}

//TODO !!
string
ev_temperatury()
{
    return "";
}

/*�ci�ga:
 *  OLD:         ZMIANA NA:     CO:
 *  gArgs[0]  i  gArgs[4]   -   zachmurzenie.
 *  gArgs[1]  i  gArgs[5]   -   opady
 *  gArgs[2]  i  gArgs[6]   -   wiatr
 *  gArgs[3]  i  gArgs[7]   -   temperatura
 */
string
event_ogolny()
{
    string str="";

    if(!TP)
        return str;

    if((ENV(TP)->pora_dnia() == MT_NOC || ENV(TP)->pora_dnia() == MT_POZNY_WIECZOR) &&
        POG_ZACH_LEKKIE >= gArgs[4])
    {
        switch(random(9))
        {
            case 0:
                if(CAN_SEE_EV(TP))
                {
                    if(gArgs[7] < 4)
                        str="Z twoich ust unosz� si� k��by pary.";
                    else
                        str="Gwiazdy mrugaj� z wysokiego nieba.";
                }
                break;
            case 1:
                if(CAN_SEE_EV(TP))
                str="Ksi�yc migocze bladym blaskiem, o�wietlaj�c "+
                        "drog� po�r�d ciemno�ci."; break;
            case 2:
                if(CAN_SEE_EV(TP))
                    str="Gwiazdy wiruj� wok� ksi�yca w magicznym ta�cu nocnym.";
                break;
            case 3:
                if(CAN_SEE_EV(TP))
                    str="Z odleg�ego zak�tka nieba spada gwiazda.";
                break;
            case 4:
                if(CAN_SEE_EV(TP))
                    str="Nocne niebo usiane jest miliardami roziskrzonych gwiazd.";
                break;
            case 5:
                if(CAN_SEE_EV(TP))
                {
                    if(POG_ZACH_ZEROWE == gArgs[4])
                        str="Gwiazdy migoc� pi�knie na bezchmurnym niebie.";
                }
                break;
            case 6:
                if(CAN_SEE_EV(TP))
                {
                    if(ENV(TP)->query_prop(ROOM_I_TYPE) & ROOM_TRACT == ROOM_TRACT)
                        str = "Promienie ksi�yca o�wietlaj� drog�.";
                    if(ENV(TP)->query_prop(ROOM_I_TYPE) & ROOM_IN_CITY == ROOM_IN_CITY)
                        str = "Promienie ksi�yca o�wietlaj� uliczk�.";
                }
                break;
            case 7:
                if(CAN_SEE_EV(TP))
                    str="Noc jest jasna, a ksi�yc b�yszczy na niebie "+
                        "niczym ogromna srebrna moneta.";
                break;
            case 8:
                if(CAN_SEE_EV(TP))
                {
                    if(gArgs[7] <= 18) //temperatura
                        str="Nocne powietrze jest ch�odne i rze�kie.";
                }
                break;
        }
    }
    else if(random(2) && gArgs[4] > POG_ZACH_ZEROWE)
    {
        if(gArgs[7]>=20/*temperatura*/&&ENV(TP)->pora_dnia()>MT_RANEK
            &&ENV(TP)->pora_dnia()<=MT_WIECZOR)
        {
            if((ENV(TP)->pora_roku() == MT_LATO) && random(2))
            {
                if(CAN_SEE_EV(TP))
                    str="�wiat tonie w promieniach gor�cego, letniego s�o�ca.";
            }
            else if(ENV(TP)->pora_roku() == MT_LATO && gArgs[6] == POG_WI_ZEROWE)
                str="Powietrze jest ci�kie i duszne i zdaje si� by� "+
                    "zupe�nie nieruchome.";
            else if(CZY_JEST_SNIEG(ENV(TP)))
            {
                if(CAN_SEE_EV(TP))
                {
                    if(random(3))
                    {
                        if(ENV(TP)->pora_dnia() == MT_SWIT || ENV(TP)->pora_dnia() == MT_WCZESNY_RANEK)
                            str="�nieg b�yszczy w promieniach wschodz�cego s�o�ca.";
                        else if(ENV(TP)->pora_dnia()>MT_WCZESNY_RANEK && ENV(TP)->pora_dnia()<MT_WIECZOR)
                            str="�nieg b�yszczy w promieniach s�o�ca.";
                        else if(ENV(TP)->pora_dnia() == MT_WIECZOR)
                            str="�nieg b�yszczy w promieniach zachodz�cego s�o�ca.";
                    }
                    else
                        str="P�atki �niegu delikatnie wiruj� w powietrzu.";
                }
            }
            else if(random(2))
                str="S�o�ce grzeje twoj� twarz.";
            else
                str="Ostre s�o�ce razi ci� w oczy.";
        }
        else if((gArgs[7] <= 1) && random(2))
            str="Mr�z szczypie ci� w policzki.";
        else if((gArgs[7] <= 1) && random(2))
        {
            if(CAN_SEE_EV(TP))
                str="Z twoich ust unosz� si� k��by pary.";
        }
        else if(gArgs[6] > POG_WI_LEKKIE)
        {
            if(CAN_SEE_EV(TP))
                str="Silny wiatr gna chmury po niebosk�onie.";
        }
    }

    if(strlen(str))
        str+="\n";

    return str;
}

/*
 * Nazwa funkcji: opis_eventow
 * Opis         : Funkcja zwraca opis eventu pogodowego w zale�no�ci od warunk�w atmosferycznych.
 * Argumenty    : string args - tablica ze zmianami w warunkach atmosferycznych
 * Zwraca       : string - opis eventu pogodowego
 */
string
opis_eventow(object plan, int* args)
{
    int i, amount;
    int *tab;
    string toReturn;

    if (sizeof(args) != 8)
        return 0;

    tab = ({ });
    gArgs = args;

    if (args[0] != args[4])
        for (i = 0; i < ABS(args[4] - args[0]); ++i)
            tab += ({ 5 });

    if (args[1] != args[5])
        for (i = 0; i < ABS(args[5] - args[1]); ++i)
            tab += ({ 6 });

    if (args[2] != args[6])
        for (i = 0; i < ABS(args[6] - args[2]); ++i)
            tab += ({ 7 });

    if (args[3] != args[7])
        for (i = 0; i < ABS(args[7] - args[3]); ++i)
            tab += ({ 8 });

    for (i = 0; i < (args[4] + 2); ++i)
        tab += ({ 1 });

    for (i = 0; i < args[5]; ++i)
        tab += ({ 2 });

    for (i = 0; i < (args[6] + 2); ++i)
        tab += ({ 3 });

    /*
    * Prawdopodobie�stwo eventa zwi�zanego z temperatur�:
    *  temp < -5  -->  (-4 - temp) / 2
    *  -5 <= temp <= 25  -->  0
    *  temp > 25  -->  (temp - 24) / 2
    */
    if (args[7] < -5)
        amount = (-4 - args[7]) / 2;
    else if (args[7] > 25)
        amount = (args[7] - 24) / 2;
    else
        amount = 0;

    for (i = 0; i < amount; ++i)
        tab += ({ 4 });

#if 0
    if(TP->query_prop(EYES_CLOSED)) //bez ceregieli ;)
        return "";    //A w�a�nie, �e z:P
#endif

    /*
     * Wylosowanie rodzaju eventa.
     */
    switch(random(3)) //Wpierw wylosujmy, czy b�dzie to event og�lny,
    {                  //czy pogodowy
        case 0:
            return "@@event_ogolny:"+file_name(TO)+"@@";
            break;
        case 1..2:
            switch(tab[random(sizeof(tab), time())])
            {
                case 1:
                    toReturn = plan->event_zachmurzenie(args[4]);
                    if (stringp(toReturn))
                        return toReturn;
                    switch(args[4])
                    {
                        case POG_ZACH_ZEROWE:       return "@@ev_zachmurzenie_zerowe:"+file_name(TO)+"@@";      break;
                        case POG_ZACH_LEKKIE:       return "@@ev_zachmurzenie_lekkie:"+file_name(TO)+"@@";      break;
                        case POG_ZACH_SREDNIE:      return "@@ev_zachmurzenie_srednie:"+file_name(TO)+"@@";     break;
                        case POG_ZACH_DUZE:         return "@@ev_zachmurzenie_duze:"+file_name(TO)+"@@";        break;
                        case POG_ZACH_CALKOWITE:    return "@@ev_zachmurzenie_calkowite:"+file_name(TO)+"@@";   break;
                    }
                    break;
            case 2:
                toReturn = plan->event_opady(args[5]);
                if (stringp(toReturn))
                    return toReturn;

                switch(args[5])
                {
                    case POG_OP_D_L: return "@@ev_opady_deszcz_lekkie:"+file_name(TO)+"@@";     break;
                    case POG_OP_D_S: return "@@ev_opady_deszcz_srednie:"+file_name(TO)+"@@";    break;
                    case POG_OP_D_C: return "@@ev_opady_deszcz_ciezkie:"+file_name(TO)+"@@";    break;
                    case POG_OP_D_O: return "@@ev_opady_deszcz_ogromne:"+file_name(TO)+"@@";    break;
                    case POG_OP_S_L: return "@@ev_opady_snieg_lekkie:"+file_name(TO)+"@@";      break;
                    case POG_OP_S_S: return "@@ev_opady_snieg_srednie:"+file_name(TO)+"@@";     break;
                    case POG_OP_S_C: return "@@ev_opady_snieg_ciezkie:"+file_name(TO)+"@@";     break;
                    case POG_OP_G_L: return "@@ev_opady_grad_lekkie:"+file_name(TO)+"@@";       break;
                    case POG_OP_G_S: return "@@ev_opady_grad_srednie:"+file_name(TO)+"@@";      break;
                }
                break;
            case 3:
                toReturn = plan->event_wiatr(args[6]);
                if (stringp(toReturn))
                    return toReturn;

                switch(args[6])
                {
                    case POG_WI_ZEROWE:     return "@@ev_wiatry_zerowe:"+file_name(TO)+"@@";    break;
                    case POG_WI_LEKKIE:     return "@@ev_wiatry_lekkie:"+file_name(TO)+"@@";    break;
                    case POG_WI_SREDNIE:    return "@@ev_wiatry_srednie:"+file_name(TO)+"@@";   break;
                    case POG_WI_DUZE:       return "@@ev_wiatry_duze:"+file_name(TO)+"@@";      break;
                    case POG_WI_BDUZE:      return "@@ev_wiatry_bduze:"+file_name(TO)+"@@";     break;
                }
                break;
            case 4:
                toReturn = plan->event_temperatura(args[7]);
                if (stringp(toReturn))
                    return toReturn;
                return "@@ev_temperatury:"+file_name(TO)+"@@";
                break;
                /*�ci�ga:
                 *  OLD:         ZMIANA NA:     CO:
                 *  gArgs[0]  i  gArgs[4]   -   zachmurzenie.
                 *  gArgs[1]  i  gArgs[5]   -   opady
                 *  gArgs[2]  i  gArgs[6]   -   wiatr
                 *  gArgs[3]  i  gArgs[7]   -   temperatura
                 */
            case 5:
                return "@@ev_zmiana_zachmurzenia:"+file_name(TO)+"@@";
                break;
            case 6:
                return "@@ev_zmiana_opadow:"+file_name(TO)+"@@";
                break;
            case 7:
                return "@@ev_zmiana_wiatru:"+file_name(TO)+"@@";
                break;
            case 8:
                return "@@ev_zmiana_temperatury:"+file_name(TO)+"@@";
                break;
        }
        break;
    }
    return 0;
}
