/**
 * /secure/npce.c
 *
 * Plik odpowiedzialny za sterowanie wszelkimi npc'ami w �wiecie.
 *
 * @author Jeremian
 */

inherit "/std/room.c";

#pragma no_clone
#pragma no_reset
#pragma no_inherit
#pragma save_binary
#pragma strict_types

#include <std.h>
#include <files.h>
#include <macros.h>
#include <mudtime.h>
#include <stdproperties.h>

mapping npce = ([]);

#define ILOSC                   0
#define ILOSC_W_SWIECIE         1
#define LOKACJA                 2
#define ZAREJESTROWANE          3

/**
 * Funkcja zwraca opis obiektu.
 * @return opis obiektu
 */
string
short()
{
    return "G��wny zarz�dca npcami";
}

/*
 * Nazwa funkcji: short_npce
 * Opis         : Funkcja wypisuje informacje o zarejestrowanych npcach
 */
void
short_npce()
{
    int i = 0;
    write("*** " + capitalize(short()) + " ***\n");
    foreach (string name : m_indices(npce))
    {
        i++;
        write(i + ") " + name + ": " + npce[name][0] + " (" + npce[name][1] + ")\n");
    }
    write("* * *\n");
}

/**
 * Wej�� mo�na tylko powy�ej mage'a.
 *
 * @param ob wchodz�cy
 *
 * @return 1 wej�cie zabronione
 * @return 0 wej�cie dozwolone
 */
public int prevent_enter(object ob)
{
    setuid();
    string imie = getuid();

    if(SECURITY->query_wiz_rank(imie) < WIZ_MAGE)
    {
        write("Jaka� magiczna si�a nie pozwala Ci tam wej��.\n");
        return 1;
    }

    return 0;
}

void
info_npc(int num)
{
    object ob;
    object* clones;
    int i = 0;
    int j = 0;

    foreach (string name : m_indices(npce))
    {
        i++;
        if (i == num)
        {
            write("*** " + i + ") " + name + " ***\n");
            ob = find_object(name);
            if (ob)
            {
                clones = object_clones(ob);
                foreach (object cur : clones)
                {
                    j++;
                    write("\n" + j + ") " + file_name(cur) + "\n  " + (objectp(ENV(cur)) ? file_name(ENV(cur)) : "0") + "\n");
                }
            }
            return;
        }
    }
    write("Z�y numer npca!\n");
}

public void
register_npc(object ob)
{
    if(!ob)
        return;

    string nazwa = MASTER_OB(ob);

    if (pointerp(npce[nazwa]))
    {
        if(!pointerp(npce[nazwa][ZAREJESTROWANE]))
            npce[nazwa][ZAREJESTROWANE] = ({});

        npce[nazwa][ZAREJESTROWANE]    += ({ob});
        npce[nazwa][ILOSC_W_SWIECIE]   += 1;
    }
    else
        npce[nazwa] = ({ 1, 1, MASTER_OB(previous_object()), ({ob}) });
}

public void
remove_npc(object ob)
{
    string nazwa = MASTER_OB(ob);
    if (pointerp(npce[nazwa]))
    {
        npce[nazwa][ILOSC_W_SWIECIE]    = max(0, npce[nazwa][ILOSC_W_SWIECIE]-1);
        npce[nazwa][ZAREJESTROWANE]    -= ({ob});
    }
}

public void
new_npc(string nazwa)
{
    if (pointerp(npce[nazwa]))
        npce[nazwa][ILOSC] += 1;
    else
        npce[nazwa] = ({ 1, 0, previous_object(), ({}) });
}

public void
delete_npc(string nazwa)
{
    if (pointerp(npce[nazwa]))
        npce[nazwa][ILOSC] = max(0, npce[nazwa][ILOSC]-1);
}

public int
get_free_slots(string nazwa)
{
    if (pointerp(npce[nazwa]))
    {
        npce[nazwa][ZAREJESTROWANE] = filter(npce[nazwa][ZAREJESTROWANE], &operator(!=)(0,));
        npce[nazwa][ILOSC_W_SWIECIE] = sizeof(npce[nazwa][ZAREJESTROWANE]);

        int res = npce[nazwa][ILOSC] - npce[nazwa][ILOSC_W_SWIECIE];
        if (res < 0)
            res = 0;

        return res;
    }
    return 0;
}

public int pomoc(string str)
{
    if(str)
        return 0;

    write("Pomieszczenie do zarz�dzania npc'ami, chwilowo bez �atwiejszego interfejsu ale nied�ugo co� si� na to "+
        "poradzi:).\n");
    return 1;
}

public int info(string str)
{
    NF("O jakim npc'u chcesz uzyska� info?\n");

    if(!str)
        return 0;

    int i;
    if(!(i=atoi(str)))
        if(!(i=LANG_NUMS(str)))
            return 0;

    info_npc(i);
    return 1;
}

public int rozkaz(string str)
{
    NF("Jakiemu npcowi, co chcesz rozkaza� zrobi�?\n");

    if(!str)
        return 0;

    int i;
    if(!(i=atoi(str)))
        if(!(i=LANG_NUMS(str)))
            return 0;

    write("Funkcja jeszcze nie dzia�a.\n");
    return 1;
}


public void init()
{
    add_action(pomoc, "pomoc");
    add_action(pomoc, "?");
    add_action(rozkaz, "rozkaz");

    add_action(info, "info");

    ::init();
}

string snpc()
{
    short_npce();
    return "";
}

void create_room()
{
    if(previous_object(-1) && MASTER_OB(previous_object(-1)) && !previous_object(-2))
        npce = ([]);

    add_item(({"npce", "npca", "npc", "npc'e", "npc'a", "ksi�ge", "ksi��k�"}), &snpc());

    set_long("Ogromna, zbudowana na planie okr�gu sala jest niemal zupe�nie pusta. "+
        "Jedyn� rzecz� kt�r� tu widzisz jest gruba ksi�ga w sk�rzanej oprawie.\n");

    add_exit("/d/Standard/wiz/garstang/korytarz2.c", ({"se", "na korytarz", "z korytarza"}));
}