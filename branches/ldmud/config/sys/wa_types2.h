/*
 * wa_types2.h
 *
 * This file defines the available weapons are locally configured.
 * The only use of this file is that it should be included
 * by /sys/wa_types.h.
 */

/* Weapon types */

#define W_FIRST         0           /* The first weapon index */

#define W_SWORD         W_FIRST + 0
#define W_MIECZ         W_SWORD
#define W_POLEARM       W_FIRST + 1
#define W_DRZEWCOWKA    W_POLEARM
#define W_AXE           W_FIRST + 2
#define W_TOPOR         W_AXE
#define W_KNIFE         W_FIRST + 3
#define W_SZTYLET       W_KNIFE
#define W_NOZ           W_KNIFE
#define W_CLUB          W_FIRST + 4
#define W_MACZUGA       W_CLUB
#define W_WARHAMMER     W_FIRST + 5
#define W_MLOT          W_WARHAMMER
#define W_MISSILE       W_FIRST + 6
#define W_STRZELECKA    W_MISSILE
#define W_JAVELIN       W_FIRST + 7
#define W_MIOTANA       W_JAVELIN
#define W_NO_T          8               /* The number of weapons defined */

/*
 * Definicje broni strzeleckich
 */

#define W_L_FIRST       0
#define W_L_BOW         W_L_FIRST + 0
#define W_L_LUK         W_L_BOW
#define W_L_SLING       W_L_FIRST + 1
#define W_L_PROCA       W_L_SLING
#define W_L_CROSSBOW    W_L_FIRST + 2
#define W_L_KUSZA       W_L_CROSSBOW
#define W_L_H_CROSSBOW  W_L_FIRST + 3
#define W_L_C_KUSZA     W_L_H_CROSSBOW
#define W_L_NO_T        4

/*
 * Drawbacks are arrange for each weapon type ({ dull, corr, break })
 * and types are ({ sword, polearm, axe, knife, club, warhammer, missile,
 *                  javelin })
 */
#define W_DRAWBACKS ({ ({ 4, 10, 4 }), ({ 7, 10, 10 }), ({ 5, 8, 6 }), \
                        ({ 6, 6, 8 }), ({ 5, 0, 6 }), ({ 5, 6, 6 }), \
                        ({ 3, 1, 12 }), ({ 8, 3, 10 }) })
/* Uwagi dla Alvina: Te wartosci sa dosc dziwne, mloty wsadzilem tak,
   zeby pasowaly do reszty. Potem to przeanlizuj i zmien. Slvthrc */

#define W_NAMES ({ "miecz", "bro� drzewcowa", "topor", "sztylet", \
                   "maczuga", "m�ot", "bro� strzelecka", "bro� miotana" })

#define W_L_NAMES ({ "proca", "kusza", "�uk" })

#define W_L_DRAWBACKS ({ ({1,1,1}), ({1,1,1}), ({1,1,1}) })
/* FIXME: Trzeba te warto�ci jako� ustawi�, ja si� nie znam na balansie. Krun. */
