/*
   sys/state_desc2.h

   Holds all textual descriptions of state descriptions of livings.
   
   Here local changes are made to the arrays defined in
   /sys/state_desc.h
*/

/*
   If you insert changes you are recommended to copy /sys/state_desc.h
   to here and make changes. It is important that the below define is
   defined afterwards:
  
#ifndef SD_DEFINED
#define SD_DEFINED
#endif

*/

/*
 * SD_AV_TITLES
 * 
 * The mortal 'level' titles.
 */
#define SD_AV_TITLES ({ "kompletny ��todzi�b",			\
			"kto� kto jeszcze niewiele widzia�",	\
			"kto� kto niewiele wie o �wiecie",	\
			"kto� kto widzia� ju� to i owo",	\
			"kto� kto niejedno widzia�",		\
			"do�wiadczona osoba",			\
			"kto� kto wiele przeszed�",		\
			"osoba kt�ra zwiedzi�a ca�y �wiat",	\
			"wielce do�wiadczona osoba" })

#define SD_AVG_TITLES	({ "kompletnego ��todzioba",			\
			   "kogo�, kto jeszcze niewiele widzia�",	\
			   "kogo�, kto niewiele wie o �wiecie",		\
			   "kogo�, kto widzia� ju� to i owo",		\
			   "kogo�, kto niejedno widzia�",		\
			   "do�wiadczon� osob�",			\
			   "kogo�, kto wiele przeszed�",		\
			   "osob�, kt�ra zwiedzi�a ca�y �wiat",		\
			   "wielce do�wiadczon� osob�"	})
