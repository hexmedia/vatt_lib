/**
 * \file /secure/dis_informator.c
 *
 * Informator o wydarzeniach na mudzie.
 *
 * Aby zosta� dis_informowanym o jakim� wydarzeniu nale�y odwo�a� sie
 * do tego obiektu wywo�uj�c na nim funkcje:
 *
 *    @example dis_inform_event(nazwa_funkcji_do_wywo�ania, wydarzenie, argumenty)
 *    @example dis_inform_hour(nazwa_funkcji_do_wywolania, godzina, argumenty)
 *
 * Dost�pne wydarzenia mo�na znale�� \file /sys/wydarzenia.h
 *
 * Plik nie przetrzymuje zmiennych w pami�ci, poniewa� zajmuj� one sporo miejsca,
 * a tylko odczytuje je wtedy kiedy jest to potrzebne.
 *
 * @author      Krun {krunasek@gmail.com}
 * @date        15.4.2008
 * @version     1.2
 *
 * TODO:
 *  - blokada usuwania, je�li do apoki jest mniej ni� 1 minuta.
 */

#pragma unique
#pragma no_reset
#pragma no_clone
#pragma no_inherit
#pragma save_binary
#pragma strict_types

inherit "/std/room";

#include <time.h>
#include <colors.h>
#include <macros.h>
#include <wydarzenia.h>
#include <stdproperties.h>

#define SAVE_FILE       ("/saves/dis_informator")
#define ZAPISZ_DANE     save_map(dane, SAVE_FILE)
#define ROZLADUJ_DANE   dane = ([]); wczytane = 0
#define SPRAWDZM(x)     x = mappingp(x) ? x : ({})
#define SPRAWDZP(x)     x = pointerp(x) ? x : ({})

#define OBIEKT          0
#define FUNKCJA         1
#define ARGUMENTY       2
#define OD_GODZINY      3
#define DO_GODZINY      4

#define WYDARZENIA      dane["wydarzenia"]
#define GODZINNE        dane["godzinne"]
#define CO_GODZINNE     dane["co_godzinne"]
#define PRZEDZIALOWE    dane["przedzialowe"]

#define DBG(x)  find_player("krun")->catch_msg(set_color(find_player("krun"),           \
                COLOR_FG_CYAN) + "INFORMATOR: " + clear_color(find_player("krun")) +    \
                (x) + "\n");

        int         alarm,
                    alarm_zapisu,
                    wczytane;
static  mapping     dane = ([]);

private static void check_alarm(int force = 0);

void create_room()
{
    set_short("Informator");
    set_long("Informator o wydarzeniach i zmianach godzin w �wiecie Vatt'gherna.\n");
    add_prop(ROOM_I_INSIDE, 1);

    check_alarm(1);
}

public string query_pomoc()
{
    return "Narazie brak interfejsu dla tego pomieszczenia, nie mia�em czasu napisa� - Krun.\n";
}

/** *********************************************************************** **
 ** ***************** F U N K C J E  P O M O C N I C Z E  ***************** **
 ** *********************************************************************** **/

/**
 * Funkcja zapisuj�ca dane i czyszcz�ca zmienn�.
 */
void zapisz_teraz()
{
    DBG("Zapisuje");
    ZAPISZ_DANE;
    ROZLADUJ_DANE;

    check_alarm(1);
}

/**
 * Funkcja startuj�ca alarm do zapisywania, �eby nie zapisywa�o po 10 razy
 */
private static void zapisz_za_chwile()
{
    DBG("Prosz� o zapis");
    if(!get_alarm(alarm_zapisu))
        alarm_zapisu = set_alarm(20.0, 0.0, &zapisz_teraz());
}

/**
 * Funkcja wczytuj�ca dane
 */
void wczytaj_mnie(int force = 0)
{
    if(force || !wczytane)
        dane = restore_map(SAVE_FILE);

    wczytane = 1;
}

/**
 * Funkcja wczytaj_mnieyszczaj�ca dane z danych nieaktualnych.
 */
private static void wyczysc_z_nieaktualnych(int force = 0)
{
    int nw;

    if(!m_sizeof(dane) && !force)
    {
        nw = 1;
        wczytaj_mnie();
    }

    //Musimy przeczy�ci� wszystkie tablice.
    WYDARZENIA      = filter(WYDARZENIA,    &operator([])(,OBIEKT) @ &objectp());
    GODZINNE        = filter(GODZINNE,      &operator([])(,OBIEKT) @ &objectp());
    CO_GODZINNE     = filter(CO_GODZINNE,   &operator([])(,OBIEKT) @ &objectp());
    PRZEDZIALOWE    = filter(PRZEDZIALOWE,  &operator([])(,OBIEKT) @ &objectp());

    if(nw)
        ROZLADUJ_DANE;
}

/**
 * Funkcja sprawdzaj�ca czy alarm jest wystartowany i czy czas
 * najbli�szego wywo�ania jest odpowiedni. I startuj�ca jeszcze raz
 * je�li nie.
 * Alarm wywo�uje si� co pe�n� godzin�, o ka�dej godzinie.
 */
private static void check_alarm(int force = 0)
{
    if(force)
        remove_alarm(alarm);
    else if(get_alarm(alarm))
        return;

    int tm = 3600 - time() % 3600;

    alarm = set_alarm(itof(tm), 3600.0, "wywolaj_alarm");
}

/**
 * Funkcja wywo�uje podan� w argumencie funkcje wraz z argumentami do niej
 */
private void wywolaj_funkcje(mixed arg)
{
    //Je�li wywo�ujemy funkcje podan� jako function to nie bierzemy pod uwag�
    //argument�w(bierzemy pod uwag� tylko te podane razem z funkcj�).

    if(functionp(arg[FUNKCJA]))
    {
        function f;
        f = arg[FUNKCJA];
        f();
    }
    else
        call_otherv(arg[OBIEKT], arg[FUNKCJA], arg[ARGUMENTY]);
}

/**
 * @return Odpowiednio sformatowana nazwa funkcji.
 */
private static string get_function_name(function funkcja)
{
    string fn = function_name(funkcja);

    if(!fn || !strlen(fn) || strlen(fn) < 5)
        return 0;

    if(!strlen(fn))
        return 0;

    fn = explode(fn, "->")[1];

    if(find_char(fn, "?") != -1)
        fn = fn[..-4];

    return fn;
}

/**
 * Funkcja por�wnujaca dwie funkcje.
 *
 * @param tab       tablica z funkcj�, obiektem i argumentami
 * @param fun       funkcja do kt�rej przyr�wnujemy
 *
 * @return 1 je�li funkcje s� identyczne, 0 je�li nie.
 */
public int compare_functions(mixed x, string fun, object ob)
{
    if(fun == x[FUNKCJA] && file_name(ob) == x[OBIEKT])
        return 1;
}

/**
 * Funkcja sprawdzaj�ca czy przypadkiem nie dodajemy tego samego po raz drugi.
 * Je�li tak to po prostu nie dodaje, bo do tej pory robi�o spore klony.
 */
public int porownaj_wydarzenia(int wydarzenie, string funkcja, object pnm)
{
    if(sizeof(filter(WYDARZENIA[wydarzenie], &compare_functions(, funkcja, pnm))))
        return 1;
}

/**
 * Funkcja sprawdzaj�ca czy przypadkiem nie dodajemy tego samego po raz drugi.
 * Je�li tak to po prostu nie dodaje, bo do tej pory robi�o spore klony.
 */
public int porownaj_godzinne(int godzina, mixed funkcja, object pnm)
{
    if(sizeof(filter(GODZINNE[godzina], &compare_functions(, funkcja, pnm))))
        return 1;
}

/**
 * Funkcja sprawdzaj�ca czy przypadkiem nie dodajemy tego samego po raz drugi.
 * Je�li tak to po prostu nie dodaje, bo do tej pory robi�o spore klony.
 */
public int porownaj_cogodzinne(mixed funkcja, object pnm)
{
    if(sizeof(filter(CO_GODZINNE, &compare_functions(, funkcja, pnm))))
        return 1;
}

private static int porownaj_przedzialy(mixed x, string funkcja, object ob, int ho, int hd)
{
    if(x[OD_GODZINY] >= ho && x[DO_GODZINY] <= hd && compare_functions(x, funkcja, ob))
        return 1;
}

/**
 * Funkcja sprawdzaj�ca czy przypadkiem nie dodajemy tego samego po raz drugi.
 * Je�li tak to po prostu nie dodaje, bo do tej pory robi�o spore klony.
 */
public int porownaj_przedzialowe(mixed funkcja, object pnm, int ho, int hd)
{
    if(sizeof(filter(PRZEDZIALOWE, &porownaj_przedzialy(, funkcja, pnm, ho, hd))))
        return 1;
}

/** *********************************************************************** **
 ** ******** F U N K C J E  W Y W O � Y W A N E  Z  Z E W N � T R Z ******* **
 ** *********************************************************************** **/

/**
 * Dzi�ki tej funkcji mo�emy podis_informowa� dis_informator, i� chcemy aby obiekt
 * by� dis_informowany o jakim� wydarzeniu.
 * Dost�pne wydarzenia znajdziecie w <i>man wydarzenia</i>.
 *
 * @param funkcja       funkcja jaka ma by� wywo�ana w obiekcie wywo�uj�cym
 *                      w momencie kiedy wydarzy si� @param wydarzenie.
 * @param wydarzenie    wydarzenie o jakim obiekt wywo�uj�cy ma by� dis_informowany
 * @param force         czy ustawiamy si�owo, je�li tak to nie b�dziemy sprawdza�
 *                      czy dana funkcja nie jest ju� dodana.
 * @param argv          argumenty do wywo�ywanej funkcji.
 */
public varargs void dis_inform_eventv(mixed funkcja, int wydarzenie, int force = 0, mixed argv = ({}))
{
    wczytaj_mnie();

    if(!previous_object_not_me() || !funkcja)
        return;

    if(functionp(funkcja))
        funkcja = get_function_name(funkcja);

    WYDARZENIA = mappingp(WYDARZENIA) ? WYDARZENIA : ([]);

    if(!is_mapping_index(wydarzenie, WYDARZENIA))
        WYDARZENIA[wydarzenie] = ({});
    else if(!force && porownaj_wydarzenia(wydarzenie, funkcja, previous_object_not_me()))
        return;

    WYDARZENIA[wydarzenie] += ({ ({previous_object_not_me(), funkcja, argv}) });

    zapisz_za_chwile();
}

/**
 * Dzi�ki tej funkcji mo�emy podis_informowa� dis_informator, i� chcemy aby obiekt
 * by� dis_informowany o jakim� wydarzeniu.
 * Dost�pne wydarzenia znajdziecie w <i>man wydarzenia</i>.
 *
 * @param funkcja       funkcja jaka ma by� wywo�ana w obiekcie wywo�uj�cym
 *                      w momencie kiedy wydarzy si� @param wydarzenie.
 * @param wydarzenie    wydarzenie o jakim obiekt wywo�uj�cy ma by� dis_informowany
 * @param force         czy ustawiamy si�owo, je�li tak to nie b�dziemy sprawdza�
 *                      czy dana funkcja nie jest ju� dodana.
 * @param ...           argumenty do wywo�ywanej funkcji.
 */
public varargs void dis_inform_event(mixed funkcja, int wydarzenie, int force = 0,  ...)
{
    if(!pointerp(argv))
        argv = ({});

    dis_inform_eventv(funkcja, wydarzenie, force, argv);
}

/**
 * Dzi�ki tej funkcji mo�emy podis_informowa� dis_informator, �e chcemy by�
 * dis_informowani o tym, �e wybi�a dana godzina.
 * Godziny podajemy w formacie od 1 do 24.
 *
 * @param funkcja   funkcja jaka ma by� wywo�ana w obiekcie wywo�uj�cym
 *                  w momencie kiedy na zegarze wybije @param godzina
 * @param godzina   godzina o kt�rej dis_informator ma podis_informowa�.
 * @param force     czy ustawiamy si�owo, je�li tak to nie b�dziemy sprawdza�
 *                  czy dana funkcja nie jest ju� dodana.
 * @param argv      argumenty do wywo�ywanej funkcji.
 */
public varargs void dis_inform_hourv(mixed funkcja, int godzina, int force, mixed argv = ({}))
{
    wczytaj_mnie();

    if(!previous_object_not_me() || !funkcja)
        return;

    if(functionp(funkcja))
        funkcja = get_function_name(funkcja);

    GODZINNE = mappingp(GODZINNE) ? GODZINNE : ([]);

    if(!is_mapping_index(godzina, GODZINNE))
        GODZINNE[godzina] = ({});
    else if(!force && porownaj_godzinne(godzina, funkcja, previous_object_not_me()))
        return;

    GODZINNE[godzina] += ({ ({previous_object_not_me(), funkcja, argv}) });

    zapisz_za_chwile();
}

/**
 * Dzi�ki tej funkcji mo�emy podis_informowa� dis_informator, �e chcemy by�
 * dis_informowani o tym, �e wybi�a dana godzina.
 * Godziny podajemy w formacie od 1 do 24.
 *
 * @param funkcja   funkcja jaka ma by� wywo�ana w obiekcie wywo�uj�cym
 *                  w momencie kiedy na zegarze wybije @param godzina
 * @param godzina   godzina o kt�rej dis_informator ma podis_informowa�.
 * @param force     czy ustawiamy si�owo, je�li tak to nie b�dziemy sprawdza�
 *                  czy dana funkcja nie jest ju� dodana.
 * @param ...       argumenty do wywo�ywanej funkcji.
 */
public varargs void dis_inform_hour(mixed funkcja, int godzina, int force, ...)
{
    if(!pointerp(argv))
        argv = ({});

    dis_inform_hourv(funkcja, godzina, force, argv);
}
/**
 * Dzi�ki tej funkcji dis_informujemy dis_informator, �e chcemy by� dis_informowani
 * o ka�dej godzinie, wywo�anie tej funkcji r�wna si� 24 krotnemu wywo�aniu
 * funkcji dis_inform_hour z podawaniem kolejno godzin.
 *
 * @param funkcja   funkcja jaka ma by� wywo�ana w obiekcie wywo�uj�cym
 *                  w momencie kiedy na zegarze wybije nowa godzina.
 * @param force     czy ustawiamy si�owo, je�li tak to nie b�dziemy sprawdza�
 *                  czy dana funkcja nie jest ju� dodana.
 * @param argv      argumenty do wywo�ywanej funkcji.
 */
public varargs void dis_inform_every_hourv(mixed funkcja, int force = 0, mixed argv = ({}))
{
    DBG("KRUCA");

    wczytaj_mnie();

    if(!previous_object_not_me() || !funkcja)
    {
        DBG("Raz");
        return;
    }

    if(functionp(funkcja))
        funkcja = get_function_name(funkcja);

    CO_GODZINNE = pointerp(CO_GODZINNE) ? CO_GODZINNE : ({});

    if(!force && porownaj_cogodzinne(funkcja, previous_object_not_me()))
    {
        DBG("Dwa");
        return;
    }

    CO_GODZINNE += ({ ({file_name(previous_object_not_me()), funkcja, argv}) });

    zapisz_za_chwile();
}

/**
 * Dzi�ki tej funkcji dis_informujemy dis_informator, �e chcemy by� dis_informowani
 * o ka�dej godzinie, wywo�anie tej funkcji r�wna si� 24 krotnemu wywo�aniu
 * funkcji dis_inform_hour z podawaniem kolejno godzin.
 *
 * @param funkcja   funkcja jaka ma by� wywo�ana w obiekcie wywo�uj�cym
 *                  w momencie kiedy na zegarze wybije nowa godzina.
 * @param force     czy ustawiamy si�owo, je�li tak to nie b�dziemy sprawdza�
 *                  czy dana funkcja nie jest ju� dodana.
 * @param ...       argumenty do wywo�ywanej funkcji.
 */
public varargs void dis_inform_every_hour(mixed funkcja, int force = 0, ...)
{
    if(!pointerp(argv))
        argv = ({});

    dis_inform_every_hourv(funkcja, force, argv);
}

/**
 * Dzi�ki tej funkcji mo�emy by� dis_informowani o danym przedziale godzinowym
 *
 * @param funkcja   funkcja kt�ra ma zosta� wywo�ana
 * @param ho        od kt�rej godziny mamy by� dis_informowani
 * @param hd        do kt�rej godziny
 * @param force     czy ustawiamy si�owo, je�li tak to nie b�dziemy sprawdza�
 *                  czy dana funkcja nie jest ju� dodana.
 * @param argv      argumenty do funkcji
 */
public varargs void dis_inform_about_hoursv(mixed funkcja, int ho, int hd, int force = 0, mixed argv = ({}))
{
    wczytaj_mnie();

    if(!previous_object_not_me() || !funkcja)
        return;

    if(functionp(funkcja))
        funkcja = get_function_name(funkcja);

    PRZEDZIALOWE = pointerp(PRZEDZIALOWE) ? PRZEDZIALOWE : ({});

    if(!force && porownaj_przedzialowe(funkcja, previous_object_not_me(), ho, hd))
        return;

    PRZEDZIALOWE += ({ ({file_name(previous_object_not_me()), funkcja, argv, ho, hd}) });

    zapisz_za_chwile();
}

/**
 * Dzi�ki tej funkcji mo�emy by� dis_informowani o danym przedziale godzinowym
 *
 * @param funkcja   funkcja kt�ra ma zosta� wywo�ana
 * @param ho        od kt�rej godziny mamy by� dis_informowani
 * @param hd        do kt�rej godziny
 * @param force     czy ustawiamy si�owo, je�li tak to nie b�dziemy sprawdza�
 *                  czy dana funkcja nie jest ju� dodana.
 * @param ...       argumenty do funkcji
 */
public varargs void dis_inform_about_hours(mixed funkcja, int ho, int hd, int force = 0, ...)
{
    if(!pointerp(argv))
        argv = ({});

    dis_inform_about_hoursv(funkcja, ho, hd, force, argv);
}

/** *********************************************************************** **
 ** ****************  F U N K C J E  W Y W O � U J � C E  ***************** **
 ** *********************************************************************** **/

/**
 * Funkcja wywo�ywana przez alarm w momencie kiedy up�ywa kolejna godzina,
 * na razie nie ma innych opcji:P
 */
public void wywolaj_alarm()
{
    wczytaj_mnie(1);

    int h = GODZINA;

    //Co 4 h czy�cimy z odwo�a� do obiekt�w, kt�re ju� nie istniej�.
    if(!(h % 4))
        wyczysc_z_nieaktualnych();

    if(is_mapping_index(h, GODZINNE) && mappingp(GODZINNE[h]) && m_sizeof(GODZINNE))
        filter(GODZINNE[h], &wywolaj_funkcje());

    if(pointerp(CO_GODZINNE) && sizeof(CO_GODZINNE))
        filter(CO_GODZINNE, &wywolaj_funkcje());

    if(pointerp(PRZEDZIALOWE) && sizeof(PRZEDZIALOWE))
    {
        foreach(mixed p : PRZEDZIALOWE)
        {
            if(p[0] >= h && p[1] <= h)
                wywolaj_funkcje(p[..-3]);
        }
    }

    //Na wszelki wypadek sprawdzamy czy alarm jest w��czony a je�li nie
    //to go w��czamy
    check_alarm();

    ROZLADUJ_DANE;
}

/**
 * Funkcja przyjmuj�ca z zewn�trz dis_informacje o jakim� wydarzeniu.
 *
 * @param wydarzenie
 */
public void uruchom_wydarzenie(int wydarzenie)
{
    wczytaj_mnie();

    if(!is_mapping_index(wydarzenie, WYDARZENIA))
        return;

    filter(WYDARZENIA[wydarzenie], &wywolaj_funkcje());

    ROZLADUJ_DANE;
}

/**
 * Redefinujemy funkcje �eby si� przypadkiem nie savewowa�o przy
 * usuwaniu obiektu, i �eby usun�� stare dane.
 */
void remove_object()
{
    rm(add_extension(SAVE_FILE, "o"));

    ::remove_object();
}
