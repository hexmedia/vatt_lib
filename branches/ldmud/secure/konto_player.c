/**
 *
 * \file /secure/konto_player.c
 *
 * Plik obs�uguj�cy konta postaci.
 *
 * @author Delvert
 * @date gdzie� oko�o 2003 roku
 */

#pragma strict_types

inherit "/std/object";

#include <std.h>
#include <files.h>
#include <config.h>
#include <composite.h>

#define TIME_OUT_TIME   300
#define KONTO_INFO      "/d/Standard/doc/login/KONTO_INFO"

static mixed   *sect_array;    /* The array holding section information */
static string  *text_array,    /* Array holding all sections text */
                cur_sect,      /* Current section to display */
                cur_menu;      /* Current menu */

static void write_info();
static void do_menu(string s_num);
string fix_menu(string str);
void enter_game();
void nagraj_konto();
void lista_postaci();
void nowa_postac();
void haslo_konta();
#ifdef POSTACIE_NA_PODANIE
void postac_na_podanie();
#endif POSTACIE_NA_PODANIE

//void haslo_postaci();
void zasady();

private string  name,                   // Nazwa konta
               *postacie,               // �yj�ce postaci
               *postacie_krzyzyk,       // Postacie, kt�re ju� nie �yj�
               *postacie_skasowane,     // Postacie, kt�re po�egna�y si� ze �wiatem Vatt'gherna szybko i bole�nie:)
                password;               // Has�o konta
private int     czas_utworzenia,        // Kiedy konto zosta�o za�o�one
                ilosc_podan,            // Ilo�� poda� z�o�onych z konta
                password_time;          // Czas ostatniej zmiany has�a

static int time_out_alarm;
static string old_password;
static int password_set = 0;
static int login_flag;

/**
 * Funkcja wywo�uj�ca si� przy pierwszym tworzeniu konta.
 */
public void
account_create_now()
{
    //Musimy sprawdzi� czy konto rzeczywi�cie nie istnieje, je�li istnieje to nie wywo�amy
    //nic
    object k;
    if((k=SECURITY->finger_konto(name)) && (k->remove_object() || 1)) //Drugi warunek jest zawsze prawd�
        return;                                                       //co wi�cej to celowe[Krun]

    czas_utworzenia = time();
    postacie = ({});
    postacie_krzyzyk = ({});
    postacie_skasowane = ({});
    ilosc_podan = 0;
}

/*
 * Nazwa funkcji: new_password
 * Opis         : This function is used to let a new character set his
 *                password.
 * Argumenty    : string p - the intended password.
 */
static void
new_password(string p)
{
    write_socket("\n");

    if (p == "zakoncz")
    {
        write_socket("Trudno sie mowi - do nastepnego razu.\n");
        remove_object();
        return;
    }

    if (!password)
    {
        if (strlen(p) < 6)
        {
            write_socket("Haslo musi miec przynajmniej 6 znakow.\n");
            input_to(new_password, 1);
            write_socket("Haslo: ");
            return;
        }

        if (!(SECURITY->proper_password(p)))
        {
            write_socket("Podane haslo nie spelnia ustalonych przez nas " +
                "podstawowych kryteriow\nbezpieczenstwa.\n");
            input_to(new_password, 1);
            write_socket("Haslo: ");
            return;
        }

        if (strlen(old_password) &&
            (crypt(p, old_password) == old_password))
        {
            write_socket("Haslo musi sie roznic od poprzedniego.\n");
            write_socket("Haslo: ");
            input_to(new_password, 1);
            return;
        }

        password = p;
        input_to(new_password, 1);
        write_socket("Dobrze. Ponownie wpisz to samo haslo, zeby je " +
            "zweryfikowac.\n");
        write_socket("Haslo (to samo): ");
        return;
    }

    if (password != p)
    {
        password = 0;
        write_socket("Hasla sie nie zgadzaja. Wiecej konsekwencji "+
            "nastepnym razem!\n");
        input_to(new_password, 1);
        write_socket("Haslo (nowe haslo, pierwsze podejscie): ");
        return;
    }

    /* Crypt the password. Use a new seed. */
    password = crypt(password, "$1$");
    nagraj_konto();

    enter_game();
}

/*
 * Nazwa funkcji: tell_password
 * Opis         : This function tells the player what we expect from his
 *                new password and then prompt him for it.
 */
static void
tell_password()
{
    write_socket(
        "Aby ustrzec twe haslo przed zlamaniem, uwazamy ze powinno ono spelniac\n" +
        " podstawowe kryteria:\n" +
        " - musi miec przynajmniej 6 znakow;\n" +
        " - musi miec przynajmniej 1 znak nie bedacy litera;\n" +
        " - musi zaczynac sie i konczyc litera.\n\n" +

        "Nowe haslo: ");

    input_to(new_password, 1);
}

/*
 * Nazwa funkcji: check_password
 * Opis         : If an existing player tries to login, this function checks
 *                for the password. If you fail, you are a granted a second
 *                try.
 * Argumenty    : string p - the intended password.
 */
static void
check_password(string p)
{
    int wiz;

    write_socket("\n");

    /* Player has no password, force him/her to set a new one. */
    if (password == 0)
    {
        write_socket("Nie masz zadnego hasla!\nMusisz ustawic nowe " +
           "zanim dopuscimy cie dalej.\n\n");
        password_set = 1;
        old_password = password;
        password = 0;
        tell_password();
        return;
    }

    /* Password matches. Go! */
    if (crypt(p, password) == password)
    {
#if 0
#ifdef FORCE_PASSWORD_CHANGE
        wiz = SECURITY->query_wiz_rank(name);

        if ((password_time + (wiz ? FOUR_MONTHS : SIX_MONTHS)) < time())
        {
            write_socket("Minel" + (wiz ? "y ponad 4 miesiace"
                : "o ponad 6 miesiecy") + " od momentu ostatniej " +
            "zmiany twego hasla.\nW zwiazku z tym musisz je zmienic, " +
            "zanim dopuscimy cie dalej.\n\n");
            password_set = 1;
            old_password = password;
            password = 0;
            tell_password();
            return;
        }
#endif FORCE_PASSWORD_CHANGE
#endif

        SECURITY->notify(this_object(), 0);
        enter_game();
        return;
    }

    write_socket("Niewlasciwe haslo!\n");

    /* Player already had a second chance. Kick him/her out. */
    if (login_flag)
    {
        remove_object();
        return;
    }

    login_flag = 1;
    write_socket("Haslo (druga i ostatnia proba): ");
    input_to(check_password, 1);
}

void
start_player(string str)
{
    mapping ma;

    set_auth(this_object(), "root:root");
    ma = restore_map("/players/konta/" + extract(str, 0, 0) + "/" + str);

    name = ma["name"];
    password = ma["password"];
    password_time = ma["password_time"];
    postacie = ma["postacie"];
    postacie_krzyzyk = ma["postacie_krzyzyk"];
    postacie_skasowane = ma["postacie_skasowane"];

    if(!pointerp(postacie))
        postacie = ({});

    if(!pointerp(postacie_krzyzyk))
        postacie_krzyzyk = ({});

    if(!pointerp(postacie_skasowane))
        postacie_skasowane = ({});

    //Przerzucamy nie�ywe postaci do postacie_krzyzyk, a skasowane do postacie_skasowane
    if(sizeof(postacie))
    {
        object *all_p = map(postacie, &(SECURITY)->finger_player());
        object *dedniete = filter(all_p, &->query_final_death());
        object *skasowane = filter(all_p, not);

        postacie -= ((map(filter(dedniete->query_real_name(), &stringp()), &lower_case()) ?: ({})) +
            (map(filter(skasowane->query_real_name(), &stringp()), &lower_case()) ?: ({})));

        postacie_krzyzyk += (map(filter(dedniete->query_real_name(), &stringp()), &lower_case()) ?: ({}));
        postacie_skasowane += (map(filter(skasowane->query_real_name(), &stringp()), &lower_case()) ?: ({}));

        all_p->remove_object();
    }
    // I przerzucone

    write_socket("Haslo: ");
    input_to(check_password, 1);
}

void
enter_game()
{
    string data, *split, *split2, str, fnam;
    int il, il2;

    data = read_file(KONTO_INFO);
    if (!data)
        remove_object();

    split = explode(data + "##", "##");

    /* discard first empty element */
    split = split[1..sizeof(split)];

    sect_array = ({});
    text_array = ({});

    for (il = 0; il < sizeof(split); il += 2)
    {
        sect_array += ({ split[il] });
        split2 = explode(split[il + 1], "#include");
        if (sizeof(split2) < 2)
            text_array += ({ split[il + 1] });
        else
        {
            for (il2 = 1; il2 < sizeof(split2); il2++)
            {
                if (sscanf(split2[il2],"\"%s\"%s", fnam, str) == 2)
                    split2[il2] = read_file(fnam) + str;
            }

            text_array += ({ implode(split2, " ") });
        }
    }

    enable_commands();
    cur_sect = sect_array[0];
    cur_menu = fix_menu(cur_sect);
    write_info();
    input_to(do_menu);
}

static void
do_menu(string s_num)
{
    int max, num;

    if ((s_num == "zakoncz") || (s_num == "0"))
    {
        nagraj_konto();
        remove_object();
        return;
    }

    num = member_array(s_num, sect_array);
    if (num >= 0)
        cur_sect = s_num;

    switch(num)
    {
        case 1: lista_postaci();        return;
        case 2: nowa_postac();          return;
        case 3: haslo_konta();          return;
        case 4: zasady();               return;
#ifdef POSTACIE_NA_PODANIE
        case 5: postac_na_podanie();    return;
#endif POSTACIE_NA_PODANIE
    }

    write_info();
    input_to(do_menu);
}

void
time_out()
{
    write_socket("Twoj czas uplynal.\n");
    nagraj_konto();
    remove_object();
}

void
write_info()
{
#if 0
    mixed *calls = get_all_alarms();
    int pos;

    for (pos=0 ; pos<sizeof(calls) ; pos++)
        if (calls[pos][1] == "time_out")
            remove_alarm(calls[pos][0]);

    set_alarm(600.0, 0.0, time_out);*/
#endif

    write_socket("\n\n\t   VATT'GHERN - konto " + name + "\n\n" +
        "\t[1] - Lista stworzonych postaci.\n" +
        "\t[2] - Stworzenie nowej postaci.\n" +
        "\t[3] - Zmiana hasla konta.\n" +
        "\t[4] - Zasady.\n" +
#ifdef POSTACIE_NA_PODANIE
        "\t[5] - Zloz podanie o postac specjalna.\n" +
#endif POSTACIE_NA_PODANIE
        "\n\t[0] - Wyjscie.\n\n" +
        "Wybierz jedna z opcji: ");
}

static string
fix_menu(string str)
{
    int pos, il, clen;

    return "Wybierz opcje: ";
}

string
query_real_name()
{
    return "<" + name + ">";
}

void
nagraj_konto()
{
    mapping map;

    set_auth(this_object(), "root:root");
    map = (["name": name,
            "password": password,
            "password_time": password_time,
            "postacie": postacie,
            "postacie_krzyzyk": postacie_krzyzyk,
#ifdef POSTACIE_NA_PODANIE
            "ilosc_postaci" : ilosc_postaci,
#endif POSTACIE_NA_PODANIE
          ]);
    save_map(map, "/players/konta/" + extract(name, 0, 0) + "/" + name);
}

void
wcisnij_enter(string str)
{
    write_info();
    input_to(do_menu);
}

void
lista_postaci()
{
    string str = "";

    int ilosc;
    if((ilosc = sizeof(postacie)))
    {
        str += "\nPosiadasz nastepujace postacie: ";
        str += COMPOSITE_WORDS2(map(postacie, capitalize), " oraz ") + "\n";

        if(ilosc > MAX_POSTACI_NA_KONCIE)
        {
            if(SECURITY->query_wiz_level(name))
            {
                str += "Posiadasz wiecej postaci niz jest to dopuszczone, ale " +
                    "jako konto ze szczegolnymi przywilejami mozesz je posiadac.\n";
            }
            else
            {
                str+="UWAGA! Posiadasz wiecej postaci niz to jest "+
                    "dopuszczalne. Zglos to czym predzej ze wskazaniem "+
                    "na postacie, ktore powinny zostac usuniete. "+
                    "Mozesz posiadac maksymalnie dwie postacie.\n";
            }
        }
    }
    else
        str += "\nNie masz na koncie zadnej zywej postaci.\n";

    if(sizeof(postacie_krzyzyk))
        str += "\nNiegdys posrod zywych: " + COMPOSITE_WORDS2(map(postacie_krzyzyk, capitalize), " a tak�e ") + "\n";
    else
        str += "\nNie masz zadnej postaci, ktora zakonczylaby juz swoj zywot.\n";

    if(sizeof(postacie_skasowane))
    {
        str += "\nNastepujace twoje postacie zostaly skasowane: " +
            COMPOSITE_WORDS(map(postacie_skasowane, capitalize)) + "\n";
    }

    if(str == "")
        str = "Nie masz na swoim koncie zadnych zywych, niezywych ani skasowanych postaci.\n\n";

    write_socket(str);
    write_socket("[Wcisnij ENTER, aby przejsc do glownego menu]");
    input_to(wcisnij_enter);
}

void
nowa_postac()
{
    object *all_p;
    if(sizeof(postacie))
    {
        all_p = map(postacie, &(SECURITY)->finger_player());
        object *to_rem = all_p;
        all_p = all_p - filter(all_p, &->query_final_death());
        to_rem->remove_object();
    }
    else
        all_p = ({});

    if(sizeof(all_p) >= MAX_POSTACI_NA_KONCIE && !SECURITY->query_wiz_level(name))
    {
        write_socket("\nMasz juz wykorzystany limit postaci. Musisz "+
            "wpierw usmiercic ktoras, by moc zalozyc nowa. Mozesz posiadac "+
            "maksymalnie dwie postacie.\n"+
            "By usmiercic postac, wystarczy ze napiszesz w tej sprawie do mg.\n");
        return;
    }

    object nowa;
    write_socket("\nNowa postac...\n\n");
    nowa = clone_object("/secure/login");
    exec(nowa, this_object());
    nowa->stworz_postac(name); //'name' to adres email
    nagraj_konto();
    remove_object();
}

void
haslo_konta()
{
    write_socket("\nHaslo konta...\n\n");
    old_password = password;
    password = 0;
    tell_password();
}

void
zasady()
{
    write_socket(read_file("/doc/help/general/zasady"));
    write_socket("\n[Wcisnij ENTER, aby przejsc do glownego menu]");
    input_to(wcisnij_enter);
}

#ifdef POSTACIE_NA_PODANIE
void podanie_postaci(object editor, object pl, string podanie)
{
    editor->remove_object();

    string lpodanie = "\n" + ctime(time()) + "     " + TP->query_real_name()\n\n" +
        sprintf("%79|s\n", "-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-") +
        podanie +
        sprintf("%79|s\n", "-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-");

    //Zapisujemy podanie do log�w
    SECURITY->log_syslog("PODANIA", lpodanie);

    SECURITY->note_something(podanie, LOG_PODANIE_ID, pl);
}

void postac_na_podanie()
{
    write_socket("A wi�c chcesz za�o�y� unikatow� posta�? Dobrze. Opisz "+
        "tu zar�wno historie swojej postaci jak i plany jej kreacji. "+
        "Przemy�l dobrze plan kreacji postaci, poniewa� p�niej b�dziemy "+
        "ci� z tego rozliczali. Pozatym zaraz po za�o�eniu postaci musisz "+
        "porozmawia� z jakim� czarodziejem na temat tej�e postaci aby "+
        "ustali� zasady j� obowi�zuj�ce. Pami�taj tak�e, i� ka�de naruszenie "+
        "zasad przez t� lub kt�r�kolwiek inn� posta� z twojego konta b�dzie "+
        "traktowane ostrzej, poniewa� przyznaj�c posta� oka�emy Ci zaufanie "+
        "a z�amanie zasad b�dzie traktowane jako jego nadu�ycie.\n\n"+
        "Dobrze, wi�c teraz napisz dlaczego mamy Ci przyzna� posta�, jaka "+
        "jest jej historia i opisz sw�j plan na jej kreacje: \n");

    object editor = clone_object(EDITOR_OBJECT);
    editor->edit(&podanie_postaci(editor, TP));
}

#endif POSTACIE_NA_PODANIE

public int
remove_object()
{
    destruct();
    return 1;
}
