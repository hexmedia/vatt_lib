/**
 * Plik do obs�ugi mxp przez muda
 *
 * Autor: Krun
 * Czerwiec, Lipiec 2007
 */

#include "/sys/mxp.h"
#include "/sys/macros.h"

//Gdy ju� napisze si� opcje trzeba odkomentowa� poni�sze linijki
/*
#define CHECK_IF_OPTION_ENABLED(x)              \
    object player = find_last_interactive();    \
    if(!player->query_option(OPT_MXP))          \
        return (x);
*/

#define CHECK_IF_OPTION_ENABLED(x) ;

string process_mxp(string str);

object
find_last_interactive()
{
    if(TP)
       return TP;
    int i=0;

    while(previous_object(i--))
        if(interactive(previous_object(i)))
            return previous_object(i);
}

string
mxp_line_tag(int type)
{
    CHECK_IF_OPTION_ENABLED("");
    return "[" + type + "z";
}

string
mxp_tag(string tag)
{
    CHECK_IF_OPTION_ENABLED("");
    return MXP_LT + tag + MXP_GT;
}

string
mxp_ctag(string tag)
{
    CHECK_IF_OPTION_ENABLED("");
    return mxp_tag("/" + tag);
}

string
mxp_var(string var)
{
    CHECK_IF_OPTION_ENABLED("");
    return MXP_AMP + var;
}

string
mxp_exit(string kierunek, string opis="")
{
    CHECK_IF_OPTION_ENABLED("");
    return process_mxp(mxp_tag("send " + MXP_QUOTE + "kierunek" + MXP_QUOTE) + (opis ? opis : kierunek) + mxp_ctag("send"));
}


string
mxp_add_element(string name, string value, mixed att = 0, int empty = 0)
{
    if(att == 1)
    {
        empty = 1;
        att = 0;
    }

    return MXP_LT + "!ELEMENT " + name + " '" + MXP_LT +  value + MXP_GT + "'" +
        (att ? " ATT='" + att + "'" : "") + (empty ? "EMPTY" : "") + MXP_GT;
}

string
clear_mxp_format(string str)
{
    //Trzeba jako� napisa� sprawdzanie czy dany tag zosta� zdefiniowany...
    //�eby nie czy�ci�o mudowego xml'a:)
    return str;
}

string
process_mxp(string str)
{
    CHECK_IF_OPTION_ENABLED(str);
    str = implode(explode(str+"&", "&"), MXP_T_AMP);
    str = implode(explode(str+">", ">"), MXP_T_GT);
    str = implode(explode(str+"<", "<"), MXP_T_LT);
    str = implode(explode(str+"\"", "\""), MXP_T_QUOTE);
    str = implode(explode(str+MXP_GT, MXP_GT), ">");
    str = implode(explode(str+MXP_LT, MXP_LT), "<");
    str = implode(explode(str+MXP_QUOTE, MXP_QUOTE), "\"");
    str = implode(explode(str+MXP_AMP, MXP_AMP), "&");

    return mxp_line_tag(MXP_SECURE) + str + mxp_line_tag(MXP_RESET);
}

/**
 * Funkcja musi zosta� wywo�ana zanim u�yjemy mxp.
 * Definiowane s� w niej wszystkie elementy
 * Oczywi�cie mo�na doda� swoje elementy
 * ale o tym poczytacie w manualu do mxp
 * jak tylko kto� go u nas na mudzie napisze:P
 */
string
mxp_initialize()
{
}