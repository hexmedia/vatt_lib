/*
 * /secure/pogoda.h
 *
 * Plik z deklaracjami funkcji dla obiektu pogody
 */

/*
 * /secure/pogoda.c
 */
string short();
void get_status();
void pisz_do_roomu_zew(object room, int* wsp, mixed msg);
void zmiana_pory(object room, int* wsp);
varargs void do_for_each(function func, mixed args, function pfunc, mixed pargs);
varargs void do_for_xy(int x, int y, function func, mixed args, function pfunc, mixed pargs);
void main_func();
void check_alarm();
void check_pogoda(int x, int y);
void register_room(object room);
void register_plan(int x, int y, object plan);
void plan_func(object plan);
void print_rooms();
void remove_all_rooms();
object get_plan(int x, int y);
void print_plan_info_short(object plan, int* wsp, mixed parg);
void print_room_info_short(object room, int* wsp, mixed arg);

/*
 * /secure/pogoda/slonce.c
 */
string opis_slonca(string roomstr);
int ustaw_slonce(object room);

/*
 * /secure/pogoda/niebo.c
 */
string opis_nieba(string roomstr);
int ustaw_niebo(object room);

/*
 * /secure/pogoda/chmury.c
 */
string opis_chmur(string roomstr);
int ustaw_chmury(object room);

/*
 * /secure/pogoda/gwiazdy.c
 */
string opis_gwiazd(string roomstr);
int ustaw_gwiazdy(object room);

/*
 * /secure/pogoda/ksiezyc.c
 */
string opis_ksiezyca(string roomstr);
int ustaw_ksiezyc(object room);

/*
 * /secure/pogoda/eventy.c
 */
string opis_eventow(object plan, int* args);
