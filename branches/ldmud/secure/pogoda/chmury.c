/*
 * /secure/pogoda/chmury.c
 *
 * Plik odpowiedzialny za funkcje zwi�zane z chmurami.
 *
 */

/*
 * Nazwa funkcji: opis_chmur
 * Opis         : Funkcja zwraca opis chmur w zale�no�ci od czasu i czynnik�w pogodowych.
 * Argumenty    : string roomstr - file_name pomieszczenia, nad kt�rym chmury opisujemy
 * Zwraca       : string - opis chmur
 */

string
opis_chmur(string roomstr)
{
	int x, y;
	string toReturn = "";
	object room = find_object(roomstr);
	object plan_pogody;

	if (!objectp(room))
	{
		return "Nie zauwa�asz niczego takiego.";
	}

	if (ustaw_chmury(room))
	{
		return "Nie zauwa�asz niczego takiego.";
	}
	
	x = room->query_prop(ROOM_I_WSP_X);
	y = room->query_prop(ROOM_I_WSP_Y);
	
	check_pogoda(x, y);

	plan_pogody = get_plan(x, y);
	
	switch (plan_pogody->get_zachmurzenie())
	{
		case POG_ZACH_LEKKIE:
			toReturn += "Nieliczne chmurki przesuwaj� si� powoli po niebie.";
			break;
		case POG_ZACH_SREDNIE:
			toReturn += "�redniej wielko�ci skupiska chmur zdobi� niebosk�on.";
			break;
		case POG_ZACH_DUZE:
			toReturn += "Grube chmury prawie ca�kowicie zakrywaj� niebo.";
			break;
		case POG_ZACH_CALKOWITE:
			toReturn += "Gruba warstwa chmur ca�kowicie zakrywa niebo.";
			break;
	}
	
	return toReturn;
}

/*
 * Nazwa funkcji: ustaw_chmury
 * Opis         : Funkcja sprawdza, czy w danym pomieszczeniu chmury maj� by� widoczne
 *                czy nie.
 * Argumenty    : object room - lokacja, w kt�rej obecno�� chmur jest sprawdzana 
 */

int
ustaw_chmury(object room)
{       
	object plan_pogody;
	if (!objectp(room))
	{
		return 0;
	}
	room->remove_item("chmury");
	plan_pogody = get_plan(room->query_prop(ROOM_I_WSP_X), room->query_prop(ROOM_I_WSP_Y));
        if ((room->query_prop(ROOM_I_INSIDE) && !room->widac_chmury()) ||
			(plan_pogody->get_zachmurzenie() == POG_ZACH_ZEROWE))
	{
		return 1;
	}
	room->add_item(({"chmury", "chmurki"}), "@@opis_chmur:"+POGODA_OBJECT+"|"+
			file_name(room)+"@@\n");
	return 0;
}
