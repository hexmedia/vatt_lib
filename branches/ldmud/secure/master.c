/**
 * \file /secure/master.c
 *
 * This is the LPmud master object, used from version 3.0.
 *
 * It is the first object loaded, save the simul_efun object, but that
 * isn't a real object rather set of simulated efuns that have to be
 * somewhere.
 *
 * Everything written with 'write()' at startup will be printed on
 * stdout.
 *
 * 1. create() will be called first.
 * 2. flag() will be called once for every argument to the flag -f
 * 	supplied to the driver.
 * 3. start_boot() will be called.
 * 4. preload_boot() will be called for each file to preload.
 * 5. final_boot() will be called.
 * 6. The game will enter multiuser mode, and enable log in.
 */

#pragma no_clone
#pragma no_inherit
#pragma save_binary
#pragma strict_types

/*
 * This is the only object in the game in which we have to use absolute path'
 * to the inclusion files. The reason for this is that the function that
 * contains the search-path for inclusion files is defined in this object.
 */
#include "/sys/debug.h"
#include "/sys/filepath.h"
#include "/sys/files.h"
#include "/sys/language.h"
#include "/sys/living_desc.h"
#include "/sys/log.h"
#include "/sys/macros.h"
#include "/sys/mail.h"
#include "/sys/std.h"
#include "/sys/stdproperties.h"
#include "/sys/time.h"
#include "/sys/udp.h"
#include "/sys/wydarzenia.h"

#define SAVEFILE   ("/syslog/KEEPERSAVE")
#define GAME_START ("/GAME_START")
#define DEBUG_RESTRICTED ({ \
            "dump_objects",     /* informacje o rozmieszczeniu obiektow */ \
         /* "get_variables", */ /* restrykcje wbudowane w do_debug() */	\
            "inhibitcallouts",  /* wylaczenie wykonywania alarmow */	\
            "mudstatus",        /* jeszcze nie wiem dlaczego :) */	\
            "send_udp",         /* podszywanie sie pod intermuda */	\
            "set_swap",         /* grzebanie w pamieci */		\
            "shared_strings",   /* _zdecydowanie_ zbyt duzo informacji */ \
            "shutdown",         /* Apokalipsa bez ostrzezenia */	\
            "swap",             /* grzebanie w pamieci (wylaczone) */	\
            "warnobsolete",     /* za duzo spamu w lplogu */		\
                         })
#define RESET_TIME (900.0) /* 15 minutes */

/* All prototypes have been placed in /secure/master.h */
#include "/secure/master.h"

#include "/secure/master/fob.c"
#include "/secure/master/siteban.c"
#include "/secure/master/spells.c"
#include "/secure/master/language.c"
#include "/secure/master/notify.c"
#include "/secure/master/sanction.c"
#include "/secure/master/guild.c"
#include "/secure/master/mail_admin.c"
#include "/secure/master/ip_admin.c"
#include "/secure/master/lock_mail.c"
#include "/secure/master/location_checker.c"
#include "/secure/master/slownik.c"

/*
 * The global variables that are saved in the SAVEFILE.
 */
private int     game_started;
private string  *def_locations;
private string  *temp_locations;
private mapping known_muds;
private int     runlevel;
private int     auto_load_rooms;

/*
 * The global variables that are not saved.
 */
private static int     mem_fail_flag;
private static int     memory_limit;
private static mapping command_substitute;
private static string  udp_manager;
private static mapping admin_teams;
private static int     irregular_uptime;

/*
 * Nazwa funkcji: create
 * Opis         : This is the first function called in this object.
 */
void
create()
{
    /* Using a global variable for this is exactly TWICE as fast as using a
     * defined mapping. At this point we only have the default direction
     * commands here because they are all over the game in add_action()s.
     * The other abbreviations can easily be added to the respective souls.
     */
    command_substitute = ([
        "n"  : "p^o^lnoc",
        "ne" : "p^o^lnocny-wsch^od",
        "e"  : "wsch^od",
        "se" : "po^ludniowy-wsch^od",
        "s"  : "po^ludnie",
        "sw" : "po^ludniowy-zach^od",
        "w"  : "zach^od",
        "nw" : "p^o^lnocny-zach^od",
        "u"  : "g^ora",
        "d"  : "d^o^l",
    ]);

    mem_fail_flag = 0;
#ifdef LARGE_MEMORY_LIMIT
    memory_limit = LARGE_MEMORY_LIMIT;
#else
    memory_limit = 28000000;
#endif
    set_auth(this_object(), "root:root");

    /* We reset the master every RESET time seconds, initially synchronizing
     * it at exactly 1 second after that occurance. I.e. if RESET_TIME is
     * 1 hour, it will be started exactly one second after the top of the
     * hour.
     */
    set_alarm(((RESET_TIME + 1.0) - (itof(time() % ftoi(RESET_TIME)))),
        RESET_TIME, reset_master);

    /* Compute the uptime for this reboot. */
#ifdef REGULAR_UPTIME
    irregular_uptime = (REGULAR_UPTIME * 3600);
#ifdef UPTIME_VARIATION
    irregular_uptime +=
        (random(UPTIME_VARIATION * 3600) - (UPTIME_VARIATION * 1800));
#endif UPTIME_VARIATION
#endif REGULAR_UPTIME

}

/*
 * Nazwa funkcji: reset_master
 * Opis         : This function will be called regularly, each RESET_TIME
 *                seconds. Since we want only one alarm running, from this
 *                function we can make calls to other modules that need it.
 */
static void
reset_master()
{
    /* Check whether there is still enough memory to run the game. The
     * argument 1 means decay the domain experience.
     */
    check_memory(1);

    /* Gather information for the graph command. */
    probe_for_graph();

    /* Save the master. */
    save_master();
}

/*
 * Nazwa funkcji: short
 * Opis         : This function returns the short description of this object.
 * Zwraca       : string - the short description.
 */
string
short()
{
    return "the hole of the donut";
}

/*
 * Nazwa funkcji: save_master
 * Opis         : This function saves the master object to a file.
 */
static void
save_master()
{
    set_auth(this_object(), "root:root");

    save_object(SAVEFILE);
}

/*********************************************************************
 *
 * GD - INTERFACE LFUNS
 *
 * The below lfuns are called from the Gamedriver for various reasons.
 */

/*
 * Called on startup of game if '-f' is given on the commandline.
 *
 * To test a new function xx in object yy, do
 * driver "-fcall yy xx arg" "-fshutdown"
 */
static void
flag(string str)
{
    string file, arg;

    if (game_started)
        return;

    if (sscanf(str, "for %d", arg) == 1)
        return;

    if (str == "shutdown")
    {
        do_debug("shutdown");
        return;
    }

    if (sscanf(str, "echo %s", arg) == 1)
    {
        write(arg + "\n");
        return;
    }

    if (sscanf(str, "call %s %s", file, arg) == 2)
    {
        arg = (string)call_other(file, arg);
        write("Got " + arg + " back.\n");
        return;
    }
    write("master: Unknown flag " + str + "\n");
}

/*
 * Nazwa funkcji:   get_mud_name
 * Opis         :   Gives the name of the mud. The name will be #defined in
 *		    all files compiled in the mud. It can not contain spaces.
 * Zwraca       :   Name of the mud.
 *		    We always return a string but we must declare it mixed
 *		    otherwise the type checker gets allergic reactions.
 */
mixed
get_mud_name()
{
#ifdef MUD_NAME
    mixed n;

    n = MUD_NAME;
    if (mappingp(n))
    {
        if (stringp(n[debug("mud_port")]))
            return n[debug("mud_port")];
        else
            return n[0];
    }
    else if (stringp(MUD_NAME))
        return MUD_NAME;
#endif MUD_NAME
    return "LPmud(" + debug("version") + ":" + MUDLIB_VERSION + ")";
}

/*
 * Nazwa funkcji: get_root_uid
 * Opis         : Gives the uid of the root user.
 * Zwraca       : string - the name of the 'root' user.
 */
string
get_root_uid()
{
    return ROOT_UID;
}

/*
 * Nazwa funkcji: get_bb_uid
 * Opis         : Gives the uid of the backbone user. That is, each user
 *                that does not have an uid of its own. Apart from backbone
 *                all wizard names and domain names are valid uids. And
 *                root naturally.
 * Zwraca       : string - the name of the 'backbone' user.
 */
string
get_bb_uid()
{
    return BACKBONE_UID;
}

/*
 * Nazwa funkcji: get_vbfc_object
 * Opis         : This function returns the objectpointer to the VBFC
 *                object.
 * Zwraca       : object - the objectpointer to the VBFC object.
 */
object
get_vbfc_object()
{
    return VBFC_OBJECT->ob_pointer();
}

/*
 * Nazwa funkcji: connect
 * Opis         : This function is called every time a player connects. We
 *                return a clone of the login object through which the socket
 *                of the new player is connected.
 *                The efun input_to() cannot be called from here.
 * Zwraca       : object - the login object.
 */
static object
connect()
{
    write("\n");
    set_auth(this_object(), "root:root");

    return clone_object(LOGIN_OBJECT);
}

/*
 * Nazwa funkcji: valid_set_auth
 * Opis         : Whenever the hidden authorization information of an object,
 *                i.e. the uid or the euid, is altered this function is being
 *                called. It checks the format of the new autorization
 *                information and makes sure that the change is valid.
 * Argumenty    : object setter      - the object forcing the change.
 *                object getting_set - the object being changed.
 *                string value       - the new value.
 * Zwraca       : string - the new value.
 */
string
valid_set_auth(object setter, object getting_set, string value)
{
    string *oldauth;
    string *newauth;
    string auth = query_auth(getting_set);

    if (!stringp(value) ||
        ((setter != this_object()) &&
        (setter != find_object(SIMUL_EFUN)) &&
        (MASTER_OB(setter)[0..11] != "/sys/global/") &&
        (MASTER_OB(setter) != "/secure/login")) &&
        (MASTER_OB(setter) != "/cmd/live/things") &&
        ((calling_function() != "wyjrzyj") || (function_exists("wyjrzyj", setter) != "/lib/peek")) &&
        ((calling_function() != "zajrzyj") || (function_exists("zajrzyj", setter) != "/lib/peek")))
    {
        return auth;
    }

    newauth = explode(value, ":");

    if (sizeof(newauth) != 2)
        return auth;

    oldauth = (stringp(auth) ? explode(auth, ":") : ({ "0", "0"}) );

    if (newauth[0] == "#")
        newauth[0] = oldauth[0];

    if (newauth[1] == "#")
        newauth[1] = oldauth[1];

    return implode(newauth, ":");
}

/*
 * Nazwa funkcji:   valid_seteuid
 * Opis         :   Checks if a certain user has the right to set a certain
 *                  objects effective 'userid'. All objects has two 'uid'
 *                  - Owner userid: Wizard who caused the creation.
 *                  - Effective userid: Wizard responsible for the objects
 *                    actions.
 *                  When an object creates a new object, the new objects
 *                  'Owner userid' is set to creating objects 'Effective
 *                  userid'.
 * Argumenty    :   ob:   Object to set 'effective' user id in.
 *                  str:  The effuserid to be set.
 * Zwraca       :   True if set is allowed.
 * Note:	    Setting of effuserid to userid is allowed in the GD as
 *		    well as setting effuserid to 0.
 */
int
valid_seteuid(object ob, string str)
{
    string uid = getuid(ob);

    /* Root can be anyone it pleases. */
    if (uid == ROOT_UID)
        return 1;

    /* We can be ourselves. That is... set the euid to our uid. */
    if (uid == str)
        return 1;

    /* Arches and keepers can be anyone they please, except for root and
     * other arches or keepers.
     */
    if (query_wiz_rank(uid) >= WIZ_ARCH)
    {
        return ((query_wiz_rank(str) < WIZ_ARCH) &&
            (str != ROOT_UID));
    }

    /* When arches and keepers are in a domain, the lord can't change
     * euid in his objects to the arch.
     */
    if (query_wiz_rank(str) >= WIZ_ARCH)
        return 0;

    /* A lord can be anyone of his subject wizards. */
    if ((uid == query_domain_lord(str)) ||
        (uid == query_domain_lord(query_wiz_dom(str))))
    {
        return 1;
    }

    /* No one else can be anything. */
    return 0;
}

/*
 * Nazwa funkcji: valid_write
 * Opis         : Checks whether a certain user has the right to write a
 *                particular file.
 * Argumenty    : string path  - the path name of the file to be write.
 *                mixed writer - the name or object of the writer.
 *                string func  - the calling function.
 * Zwraca       : int 1/0 - allowed/disallowed.
 */
int
valid_write(string file, mixed writer, string func)
{
    string *dirs;
    string dname;
    string wname;
    string dir;

    /* Nikt nie ma praw do .svn */
    if (member_array(".svn", explode(file, "/") - ({ "" })) != -1)
        return 0;

    if (objectp(writer))
        writer = geteuid(writer);

    /* Root may do as he please. */
    if (writer == ROOT_UID)
        return 1;

    /* Keepers and arches may do as they please. */
    if (query_wiz_rank(writer) >= WIZ_ARCH)
        return 1;

    /* Anonymous objects cannot do anything. */
    if (!stringp(writer) || !strlen(writer))
        return 0;

    //Ma�y waruneczek, ot� musimy sprawdzi� czy nasz pliczek jest rzeczywi�cie
    //tam gdzie go mud widzi. Czy to nie jakie� dowi�zanie symboliczne. W tym
    //celu wykorzystujemy real_path();
    string rp = real_path(file);
    if(rp && rp != file)
        file = rp;

    dirs = explode(file, "/") - ({ "" });

    /* Everyone may write in /open */
    if ((sizeof(dirs) > 0) && (dirs[0] == "open"))
        return 1;

    if (m_global_read[writer])
        return 1;

    /* Must be /d/Domain/something at least. */
    if (sizeof(dirs) < 3)
        return 0;

    dname = dirs[1];

    /* W /d/common moga zapisywac wylacznie lordowie i administracja.
     * Stewardzi moga zapisywac w /d/common/Domena.
     */
    if (dname == "common")
    {
        if (query_wiz_rank(writer) == WIZ_LORD)
            return 1;

        if ((query_wiz_rank(writer) == WIZ_STEWARD) &&
            (dirs[2] == query_wiz_dom(writer)))
            return 1;

        return 0;
    }

    /* The domain must be an existing domain. */
    if ((dirs[0] != "d") || (query_domain_number(dname) == -1))
        return 0;

    /* A Lord can write anywhere in the domain, and wizards can naturally
     * write their own directory.
     */
    wname = dirs[2];
    if ((query_domain_lord(dname) == writer) ||
        ((wname == writer) &&
        (query_wiz_dom(wname) == dname)))
    {
        return 1;
    }

    /* Steward can write anywhere, except in the Lord's directory. */
    if ((query_domain_steward(dname) == writer) &&
        (wname != query_domain_lord(dname)))
    {
        return 1;
    }

    /* We have to check for the directory sanctions here because they might
     * disclose the private directories.
     */
    if (recursive_valid_write_path_sanction(writer, dname, dirs[2..]))
        return 1;

    /* Only the Lord of a domain can write in the 'domain' directory. */
    if (wname == "domain")
        return 0;

    /* The private directory of a wizard is closed to all others. */
    dir = ((sizeof(dirs) > 3) ? dirs[3] : "");
    if (dir == "private")
        return 0;

    if (m_global_read[writer])
        return 1;
    /* To write something now you need a sanction. If it is a wizard directory
     * it must be a personal sanction or a domain 'write all' sanction. */
    if (query_wiz_dom(wname) == dname)
    {
        return (valid_write_sanction(writer, wname) ||
            valid_write_all_sanction(writer, dname));
    }

    /* Praktykanci i poni�ej 14 lvl nie maj� mo�liwo�ci edycji
     * w ca�ej domenie.
     */
    if ((SECURITY->query_wiz_level(wname) < 14 || query_trainee(wname)) && find_player(wname))
        return 0;

    /* Wizards can write everywhere in the domain and so can the domain itself
     * unless this is the domain for 'lonely' wizards or unless the directory
     * belongs to another wizard.
     */
    if ((query_wiz_dom(wname) != dname) &&
        ((query_wiz_dom(writer) == dname) ||
        (dname == writer)))
    {
        return (dname != WIZARD_DOMAIN);
    }

    /* If the directory is in another place, just a domain write sanction
     * suffices.
     */
    {
        return (valid_write_sanction(writer, dname) ||
            valid_write_all_sanction(writer, dname));
    }

    return 0;
}

/*
 * Nazwa funkcji: valid_read
 * Opis         : Checks if a certain user has the right to read a file.
 * Argumenty    : string path  - path name of the file to be read.
 *                mixed reader - the object or name of the reader.
 *                string func  - the calling function.
 * Zwraca       : int 1/0 - allowed/disallowed.
 */
int
valid_read(string file, mixed reader, string func)
{
    string *dirs;
    string dname;
    string wname;
    string dir;

    if(file[-1] == '*')
        file = file[..-2];

    /* Nikt nie ma praw do .svn */
    if (member_array(".svn", explode(file, "/") - ({ "" })) != -1)
        return 0;

    /* Everyone is allowed to see the time or size of a file. */
    if ((func == "file_time") || (func == "file_size"))
        return 1;

    if (objectp(reader))
        reader = geteuid(reader);

    /* Allow read in / */
    if (file == "/")
        return 1;

    /* Root and archwizards and keepers may do as they please. */
    if ((reader == ROOT_UID) || (query_wiz_rank(reader) >= WIZ_ARCH))
        return 1;

    /* Anonymous objects cannot do anything. */
    if (!stringp(reader) || !strlen(reader))
        return 0;

    if (file_name(previous_object()) == PLAYER_ADMIN)
        return 1;

    //Ma�y waruneczek, ot� musimy sprawdzi� czy nasz pliczek jest rzeczywi�cie
    //tam gdzie go mud widzi. Czy to nie jakie� dowi�zanie (symboliczne lub trwa�e). W tym
    //celu wykorzystujemy real_path();
    string rp = real_path(file);
    if(rp && rp != file)
        file = rp;

    /* These directories are closed to all save the admin. */
    dirs = explode(file, "/") - ({ "" });
    if ((dirs[0] == "players") || (dirs[0] == "binaries"))
        return 0;

    if(dirs[0] == "d" && sizeof(dirs) > 1)
        dname = dirs[1];

    /* W /d/common moga czytac wylacznie lordowie i administracja.
     * Stewardzi moga czytac w /d/common/Domena.
     */
    if (dname == "common")
    {
        if (query_wiz_rank(reader) == WIZ_LORD)
            return 1;

        if ((query_wiz_rank(reader) == WIZ_STEWARD) &&
            (dirs[2] == query_wiz_dom(reader)))
            return 1;

        return 0;
    }

    /* The domain must be an existing domain. */
    if (dname && query_domain_number(dname) == -1)
        return 0;

    /* Lord i senior mog� grzeba� w ca�ej domenie.*/
    if ((query_domain_lord(dname) == reader) || query_domain_steward(dname) == reader)
        return 1;

    wname = ((sizeof(dirs) > 2) ? dirs[2] : "");

    //Mia� katalog w innej domenie to te� mo�e go odczyta�, a czemu nie?
    if(wname == reader)
        return 1;

    /* The open directory of a domain or wizard is free to read for all. */
    dir = ((sizeof(dirs) > 3) ? dirs[3] : "");
    if ((wname == "open") || (dir == "open"))
        return 1;

    /* The private directory of a wizard is closed to all others. */
    if (dir == "private")
        return 0;

    /* Some people have been granted global read rights. Global read includes
     * the private directory of a domain.
     */
    if (m_global_read[reader])
        return 1;

    /* Zmiany by lil. Bo mlodzi maja za duzy dostep! */
    if((dirs[0]=="cmd") || (dirs[0]=="lib"))
    {
        if(query_wiz_level(reader) < 14)
           return 0;
        else
           return 1;
    }

    if(dirs[0] == "secure")
    {
        if(query_wiz_rank(reader) < WIZ_MAGE)
           return 0;
        else
           return 1;
    }

    if(dirs[0] == "std")
    {
        if(query_wiz_rank(reader) < WIZ_MAGE)
           return 0;
        else
           return 1;
    }

    if(sizeof(dirs) > 1 && dirs[0]=="sys" && dirs[1]=="exp.h")
    {
        //Ten plik jest w pizdu tajny. Nie da rady
        if(query_wiz_rank(reader) < WIZ_MAGE || query_wiz_level(reader) < 25)
           return 0;
        else
           return 1;
    }
    /*Koniec zmian Lil.*/

    /* Praktykanci maj� dost�p tylko do siebie i do /doc i do /open */
    if(query_wiz_rank(reader) < WIZ_NORMAL && find_player(reader))
    {
        if(dirs[0] == "doc" || dirs[0] == "open")
            return 1;

        return 0;
    }
    else if(query_trainee(reader) && find_player(reader))
    {
        //do doc i open
        if(dirs[0] == "doc" || dirs[0] == "open")
            return 1;

        //do swojego
        if(sizeof(dirs) > 3 && (dirs[0] == "d" && dirs[1] == query_wiz_dom(reader) && dirs[2] == reader))
            return 1;

        //do open domeny
        if(sizeof(dirs) > 3 && (dirs[0] == "d" && dirs[1] == query_wiz_dom(reader) && dirs[2] == "open"))
            return 1;

        //Widzi zawarto�� katalogu domenowego
        if(sizeof(dirs) > 2 && (dirs[0] == "d" && dirs[1] == query_wiz_dom(reader)))
            return 1;

        if(sizeof(dirs) == 1)
            return 1;

        return 0;
    }

    /* The AoP team and Lords can see various logs. Others cannot see them. */
    if ((dirs[0] == "syslog") && (sizeof(dirs) > 2) && (dirs[1] == "log"))
    {
        if (member_array(dirs[2], AOP_TEAM_LOGS) > -1 &&
            (query_wiz_rank(reader) >= WIZ_LORD ||
            query_admin_team_member(reader, "mg")))
        {
            return 1;
        }

        return 0;
    }

    /* Allow all directories that aren't in a domain. */
    if(sizeof(dirs) > 1 && (dirs[0] != "d") || (sizeof(dirs) == 1))
        return 1;

    /* We have to check for the directory sanctions here because they might
     * disclose the private directories.
     */
    if (strlen(wname) &&
        recursive_valid_read_path_sanction(reader, dname, dirs[2..]))
    {
        return 1;
    }

    /* Wizards can read everywhere in the domain and so can the domain itself
     * unless this is the domain for 'lonely' wizards.
     */
    if ((query_wiz_dom(reader) == dname) || (dname == reader))
        return (dname != WIZARD_DOMAIN);

    /* The private directory of a domain is close to all others. */
    if (wname == "private")
        return 0;

    /* To read something now you need a sanction. If it is a wizard directory
     * it must be a personal sanction or a domain 'read all' sanction. If the
     * directory is in another place, just a domain read sanction suffices.
     */
    if (query_wiz_dom(wname) == dname)
    {
        return (valid_read_sanction(reader, wname) ||
            valid_read_all_sanction(reader, dname));
    }
    else
    {
        return (valid_read_sanction(reader, dname) ||
            valid_read_all_sanction(reader, dname));
    }

    /* No show. */
    return 0;
}

#if 0
/*
 * Nazwa funkcji: valid_move
 * Opis         : This function is called by the gamedriver to see whether
 *                it is possible to move an object to a destination.
 * Argumenty    : object ob   - the object to move.
 *                object dest - the intended destination.
 * Argumenty    : int 1/0 - allowed/disallowed.
 */
int
valid_move(object ob, object dest)
{
    return 1;
}

/*
 * Nazwa funkcji: valid_resident
 * Opis         : This function is called to see whether the object is allowed
 *                to set the pragma 'resident'. At this point no object is
 *                allowed to do so. That is safest.
 * Argumenty    : object ob - the object to test.
 * Zwraca       : int 1/0 - allowed/disallowed.
 */
int
valid_resident(object ob)
{
    return 0;
}
#endif

/*
 * Nazwa funkcji: valid_debug
 * Opis         : This function is called to see whether the object is allowed
 *                to call the efun debug() if the object is anything but this
 *                object SECURITY. Since we don't want other objects to call
 *                debug other than via SECURITY->do_debug(), we disallow it
 *                for all.
 * Argumenty    : object ob - the object calling valid_debug
 *                string cmd - the debug command.
 *                mixed arg1 - the argument 1 to debug.
 *                mixed arg2 - the argument 2 to debug.
 *                mixed arg3 - the argument 3 to debug.
 * Zwraca       : int 1/0 - allowed/ disallowed.
 */
varargs int
valid_debug(object ob, string cmd, mixed arg1, mixed arg2, mixed arg3)
{
    return 0;
}

/*
 * Nazwa funkcji: valid_query_ip_ident
 * Opis         : This function is called to check whether the object is
 *                allowed to call the efun query_ip_ident() on a particular
 *                target.
 * Argumenty    : object actor  - the object that wants to call the efun.
 *                object target - the object the actor wants to know about.
 * Zwraca       : int 1/0 - allowed/ disallowed.
 */
varargs int
valid_query_ip_ident(object actor, object target)
{
    string euid = geteuid(actor);

    /* Only archwizard and keepers may call this efun. */
    return ((query_wiz_rank(euid) >= WIZ_ARCH) || (euid == "root"));
}

/*
 * Nazwa funkcji: valid_query_ip
 * Opis         : This function is called to check whether the actor is
 *                allowed to call the efun query_ip_number() or _name() on
 *                a particular target.
 * Argumenty    : mixed actor   - the actor that wants to call the efun.
 *                object target - the object the actor wants to know about.
 * Zwraca       : int 1/0 - allowed/ disallowed.
 */
varargs int
valid_query_ip(mixed actor, object target)
{
    return 1;
#if 0
    if (objectp(actor))
    {
        actor = geteuid(actor);
    }

    /* Lords and arches can see ip-number. */
    if (query_wiz_rank(actor) >= WIZ_LORD)
    {
        return 1;
    }

    /* Members of the domain arch and player arch team, too. */
    if (query_admin_team_member(actor, "aod") ||
        query_admin_team_member(actor, "mg"))
    {
        return 1;
    }

    return 0;
#endif
}

/*
 * Nazwa funkcji: check_snoop_validity
 * Opis         : Do the actual validity checking.
 * Argumenty    : object snooper - the prospective snooper.
 *		  object snopee  - the prospective snoopee.
 *		  int sanction - consider sanctioning or not.
 * Zwraca       : int 1/0 - allowed/disallowed.
 */
int
check_snoop_validity(object snooper, object snoopee, int sanction)
{
    int by_type;
    int on_type;
    string by_name;
    string on_name;

    by_name = geteuid(snooper);
    by_type = query_wiz_rank(by_name);
    on_name = geteuid(snoopee);
    on_type = query_wiz_rank(on_name);

    /* Only wizards can snoop. */
    if (by_type <= WIZ_MAGE)
        return 0;

    /* Lords and stewards can snoop members everywhere, if the snoopee has a
     * level lower than their own and they can snoop apprentices too.
     */
    if (((by_type == WIZ_LORD) ||
        (by_type == WIZ_STEWARD)) &&
        (((query_wiz_dom(on_name) == query_wiz_dom(by_name)) &&
        (on_type < by_type)) ||
        (on_type == WIZ_APPRENTICE)))
    {
        return 1;
    }

    /* Arch snoops all but arch. */
    if ((by_type >= WIZ_ARCH) &&
        (on_type < WIZ_ARCH))
    {
        return 1;
    }

    /* Mortals are safe in sanctuary for all but arch++. */
    if (environment(snoopee) &&
        (environment(snoopee)->query_prevent_snoop()))
    {
        return 0;
    }

    /* Ordinary wizzes snoops all mortals. */
    if (on_type == WIZ_MORTAL && by_type >= WIZ_MAGE)
    {
        return 1;
    }

    /* A wizard can sanction another wizard to snoop him. */
    if (sanction)
    {
        return valid_snoop_sanction(by_name, on_name);
    }

    return 0;
}

/* This macro will log the snoop-action when the actor is not a member of
 * the administration.
 */
#ifdef LOG_SNOOP
#define ACTION_SNOOP(str) \
if (query_wiz_rank(caller_name) < WIZ_ARCH) \
{ this_object()->log_syslog(LOG_SNOOP, ctime(time()) + " " + (str)); }
#endif LOG_SNOOP

/*
 * Nazwa funkcji: valid_snoop
 * Opis         : Checks if a user has the right to snoop another user.
 * Argumenty    : object initiator - the actor for the command.
 *                object snooper   - the prospective snooper.
 *		  object snopee    - the prospective snoopee.
 * Zwraca       : int 1/0 - allowed/disallowed.
 */
public int
valid_snoop(object initiator, object snooper, object snoopee)
{
    string caller_name;
    string actor_name;
    string target_name;
    int result;

    caller_name = geteuid(initiator);
    actor_name  = geteuid(snooper);

    /* Break snoop case. Valid if the breaker has the right to snoop the
     * person currently doing the snooping and naturally people can break
     * their own snoop as well. Do not consider sanctioning in this case.
     */
    if (!snoopee)
    {
        if ((caller_name != actor_name) &&
            !check_snoop_validity(initiator, snooper, 0))
        {
            return 0;
        }

#ifdef LOG_SNOOP
        ACTION_SNOOP(sprintf(" %-11s snoop broken by %s\n",
                    capitalize(snooper->query_real_name()),
                    capitalize(caller_name)));
#endif LOG_SNOOP
        return 1;
    }

    /* Prevent accidental breaking of snoop. If the target is already snooped
     * it is not possible to set a new snoop on that target.
     */
    if (efun::query_snoop(snoopee))
        return 0;

    /* Set up snoop case. This way player A forces B to snoop C. This is
     * only valid for archwizards++. We do not consider sanctioning in
     * this case.
     */
    target_name = geteuid(snoopee);
    if (caller_name != actor_name)
    {
        if ((query_wiz_rank(caller_name) < WIZ_ARCH) ||
            !check_snoop_validity(initiator, snoopee, 0))
        {
            return 0;
        }

#ifdef LOG_SNOOP
        ACTION_SNOOP(sprintf(" %-11s snoops %-11s forced by %s\n",
            capitalize(snooper->query_real_name()),
            capitalize(snoopee->query_real_name()),
            capitalize(caller_name)));
#endif LOG_SNOOP
        return 1;
    }

    /* Ordinary snoop case. A wants to snoop B. Valid if snooper can snoop
     * snoopee. Consider sanctioning in this case.
     */
    if (check_snoop_validity(snooper, snoopee, 1))
    {
#ifdef LOG_SNOOP
        ACTION_SNOOP(sprintf(" %-11s snoops %s\n",
            capitalize(snooper->query_real_name()),
            capitalize(snoopee->query_real_name())));
        if (query_wiz_rank(caller_name) < WIZ_ARCH)
        {
            this_object()->log_syslog(LOG_SNOOP, "    " +
                RPATH(file_name(environment(snooper))) + " snoops " +
                RPATH(file_name(environment(snoopee))) + "\n");
        }

#endif LOG_SNOOP
        return 1;
    }

    return 0;
}

/*
 * Nazwa funkcji: creator_file
 * Opis         : Gives the name of the creator of a file. This is a
 *                direct function of the file_name(). The creator must be
 *                a domain or wizard name, the root or backbone euid or
 *                it can be 0 if it is invalid.
 * Argumenty    : string str - the path to process.
 * Zwraca       : string - the name of the creator.
 */
string
creator_file(string str)
{
    string *parts;

    /* Get the parts of the name. */
    parts = explode(str, "/") - ({ "" });

    /* The file is probably in a domain directory. */
    if (parts[0] == "d")
    {
        /* The file is owned by an active wizard. */
        if ((sizeof(parts) > 2) &&
            (query_wiz_dom(parts[2]) == parts[1]))
        {
            return parts[2];
        }

        /* The file is owned by an active domain. */
        if (query_domain_number(parts[1]) > -1)
        {
            return parts[1];
        }
    }

    /* Everything in /secure gets return root uid. */
    if (parts[0] == "secure")
        return ROOT_UID;

    /* No cloning or loading from or /open. */
    if (parts[0] == "open")
        return 0;

    /* All else: return backbone uid. */
    return BACKBONE_UID;
}

string
modify_path(string path, object ob)
{
    return path;
}

#if 0
string
valid_compile_path(string path, string filename, string fun)
{
    return path;
}
#endif

/*
 * Convert a possibly relative path to an absolute path. We can assume
 * that there is a this_player(). This is called from within the editor.
 */
string
make_path_absolute(string path)
{
    return FTPATH(this_player()->query_path(), path);
}

/*
 * Nazwa funkcji: load_domain_link
 * Opis         : Try to load a domain_link file
 * Argumenty    : file - The domain_link to load
 * Zwraca       : True if everything went ok, false otherwise
 */
static int
load_domain_link(string file)
{
    string err, creator;

    if (extract(file, -2) == ".c")
        file = extract(file, 0, -3);

    if (file_size(file + ".c") == -1)
        return 0;

    creator = creator_file(file);
    set_auth(this_object(), "root:" + creator);

    if (err = (string)LOAD_ERR(file))
    {
        write("\tCan not load: " + file + ":\n     " + err + "\n");
        return 0;
    }

    write("\t" + file + ".c (" + creator + ")\n");

    catch(file->preload_link());

    return 1;
}

/*
 * Nazwa funkcji: 	start_boot()
 * Opis         :	Loads master data, including list of all domains and
 *			wizards. Then make a list of preload stuff
 * Argumenty    :	load_empty: If true start_boot() does no preloading
 * Return:		List of files to preload
 */
static string *
start_boot(int load_empty)
{
    string *prefiles, *links;
    object simf;
    mixed test;
    int size, ix;

    if (game_started)
        return 0;

    set_auth(this_object(), "root:root");

    /* Fix the userids of the simul_efun object */
    if (objectp(simf = find_object(SIMUL_EFUN)))
    {
        set_auth(simf, "root:root");
    }

    write("Retrieving master data.\n");
    if (!restore_object(SAVEFILE))
    {
        write(SAVEFILE + " nonexistant. Using defaults.\n");
        load_fob_defaults();
        load_guild_defaults();
        reset_graph();
        runlevel = WIZ_MORTAL;
        auto_load_rooms = 1;
    }

    if (load_empty)
    {
        write("Not preloading.\n");
        return 0;
    }

    /* Make list of preload stuff: Domain and wizard */
#ifdef PRELOAD_FIRST
    if (stringp(PRELOAD_FIRST))
        prefiles = explode(read_file(PRELOAD_FIRST), "\n");
    else if (pointerp(PRELOAD_FIRST))
        prefiles = PRELOAD_FIRST + ({});
#endif PRELOAD_FIRST

    write("Loading and setting up domain links:\n");
    links = filter(query_domain_links() + query_mage_links(),
        load_domain_link);

    size = sizeof(links);
    ix = -1;

    while (++ix < size)
        prefiles = prefiles + links[ix]->query_preload();

    return prefiles;
}

static void
preload_boot(string file)
{
    string err, creator;

    if (file_size(file + ".c") == -1)
    {
        return;
    }

    creator = creator_file(file);
    set_auth(this_object(), "root:" + creator);

    if (err = (string)LOAD_ERR(file))
    {
        write("\tCan not load: " + file + ":\n     " + err + "\n");
    }
    else
    {
        write("\tPreloading: " + file + ".c  (" + creator + ")\n");
        if (strlen(file = catch(file->teleledningsanka())))
        {
            write("\tError: " + file + ".c\n");
        }
    }
}

/*
 * Nazwa funkcji: final_boot
 * Opis         : This function will be called from the gamedriver when the
 *                game is started, after start_boot() and preload_boot() are
 *                called. Note that this function is not called when the
 *                master is updated.
 */
static void
final_boot()
{
    int theport;

    game_started = 1;
    theport = debug("mud_port");
    if (theport)
    {
        set_auth(this_object(), "root:root");
        write_file((GAME_START + "." + theport), ctime(time()) + "\n");
    }

    debug("set_swap",
        ({
            SWAP_MEM_MIN,
            SWAP_MEM_MAX,
            SWAP_TIME_MIN,
            SWAP_TIME_MAX
            }) );

    /* Tell the graph we rebooted. */
    mark_graph_reboot();

#ifdef UDP_MANAGER
    udp_manager = UDP_MANAGER;
    if (catch(udp_manager->teleledningsanka()))
        udp_manager = 0;

    if (stringp(udp_manager))
        udp_manager->send_startup_udp(MUDLIST_SERVER[0], MUDLIST_SERVER[1]);
    else
#endif UDP_MANAGER
        debug("send_udp", MUDLIST_SERVER[0], MUDLIST_SERVER[1],
            "@@@" + UDP_STARTUP + this_object()->startup_udp() + "@@@\n");
}

/*
 * Nazwa funkcji: start_shutdown
 * Opis         : This function is called by the gamedriver to get a list
 *                of all interactive objects that need to be disconnected
 *                from the game when it is shutting down.
 * Zwraca       : object * - the interactive users in the game.
 */
object *
start_shutdown()
{
    return users();
}

/*
 * Nazwa funkcji: cleanup_shutdown
 * Opis         : This function is called for each interactive object when
 *                the game is shutting down. It makes all those players
 *                quit the game.
 * Argumenty    : object ob - the object to force to quit.
 */
static void
cleanup_shutdown(object ob)
{
    set_this_player(ob);
    ob->quit();
}

/*
 * Nazwa funkcji: final_shutdown
 * Opis         : When all mortals are kicked out of the game, this function
 *                is called last by the gamedriver before the game closes.
 */
static void
final_shutdown()
{
    string *links;
    int index;
    int size;

    if (stringp(udp_manager))
    {
	udp_manager->send_shutdown_udp(MUDLIST_SERVER[0], MUDLIST_SERVER[1]);
	udp_manager->update_masters_list();
    }

    /* Process the graph data even if this isn't the top of the hour. */
    graph_process_data();

    /* Save the master. */
    save_master();

    /* Call the domain links. */
    links = query_domain_links();
    size = sizeof(links);
    index = -1;
    while(++index < size)
    {
	if (file_size(links[index] + ".c") != -1)
	{
	    catch(links[index]->armageddon());
	}
    }

    /* Na ko�cu niszczymy wszystkie pomieszczenia */
    POGODA_OBJECT->remove_all_rooms();
}

/*
 * Nazwa funkcji: log_error
 * Opis         : This function is called from the game driver if there is
 *                an error while compiling an object.
 * Argumenty    : string path  - the path of the object having the error.
 *                string error - the error message.
 */
static void
log_error(string path, string error)
{
    set_auth(this_object(), "root:root");

    path = query_wiz_path(creator_file(path)) + "/log";
    if (file_size(path) != -2)
    {
	mkdir(path);
    }

    path += "/errors";
    if (file_size(path) == -1)
    {
	write_file(path, "*\n* To jest twoj log bledow\n*\n\n");
    }
    write_file(path, ctime(time()) + "\n" + error);
}

/*
 * This function is called from GD when rooms are destructed so that master
 * can move players to safety.
 */
void
destruct_environment_of(object ob)
{
    if (environment(ob))
	catch(ob->move(environment(ob)));

    if (!query_ip_number(ob))
	return;
    ob->move_living("X", ob->query_default_start_location());
}

/*
 * Nazwa funkcji: define_include_dirs
 * Opis         : Define  where the '#include' statement is supposed to
 *                search for files. "." will automatically be searched
 *                first, followed in order as given below. The path should
 *                contain a '%s', which will be replaced by the file
 *                searched for.
 * Zwraca       : string * - the array of path to search.
 */
string *
define_include_dirs()
{
    return ({ "/sys/%s" });
}

/*
 * Nazwa funkcji: get_ed_buffer_save_file_name
 * Opis         : When a wizard is in ed and goes linkdead, this function
 *                is called to get the name of the save-file. If the
 *                wizard does not have the necessary rights, the function
 *                will disallow save.
 * Argumenty    : string - the name of the file to save
 * Zwraca       : string - the name of the secured save-file
 *                0      - the player has no write-rights.
 */
string
get_ed_buffer_save_file_name(string file)
{
    string *path;

    if (!objectp(this_player()))
	return 0;

    if (!valid_write(file, this_player(), "ED"))
	return 0;

    path = explode(file, "/");
    path[sizeof(path) - 1] = "dead_ed_" + path[sizeof(path) - 1];

    return implode(path, "/");
}

/* save_ed_setup and restore_ed_setup are called by the ed to maintain
   individual options settings. These functions are located in the master
   object so that the local gods can decide what strategy they want to use.

/*
 * The wizard object 'who' wants to save his ed setup. It is saved in the
 * file ~wiz_name/.edrc . A test should be added to make sure it is
 * a call from a wizard.
 *
 * Don't care to prevent unauthorized access of this file. Only make sure
 * that a number is given as argument.
 */
int
save_ed_setup(object who, int code)
{
    string file;

    if (!intp(code))
	return 0;
    file = query_wiz_path((string)who->query_real_name()) + "/.edrc";
    rm(file);
    return write_file(file, code + "");
}

/*
 * Retrieve the ed setup. No meaning to defend this file read from
 * unauthorized access.
 */
int
retrieve_ed_setup(object who)
{
    string file;
    int code;

    file = query_wiz_path(who->query_real_name()) + "/.edrc";
    if (file_size(file) <= 0)
	return 0;
    sscanf(read_file(file), "%d", code);
    return code;
}

/*
 * Nazwa funkcji: query_allow_shadow
 * Opis         : This function is called from the game driver to find out
 *                whether it is allowed to shadow a particular object. The
 *                object that wants to shadow is previous_object(). To
 *                prevent shadowing, the target object will have to define
 *                the function query_prevent_shadow() to return 1.
 * Argumenty    : object target - the object targeted for shadowing.
 * Zwraca       : int 1/0 - allowed/disallowed.
 */
int
query_allow_shadow(object target)
{
    return !(target->query_prevent_shadow(previous_object()));
}

/*
 * Nazwa funkcji:   valid_exec
 * Opis         :   Checks if a certain 'program' has the right to use exec()
 * Argumenty    :   name: Name of the 'program' that attempts to use exec()
 *                        Note that this is different from file_name(),
 *                        The program name is what calling_program returns.
 *                  to:   destination of socket
 *                  from: target of the socket
 * Zwraca       :   True if exec() is allowed.
 */
int
valid_exec(string name, object to, object from)
{
    name = "/" + name;
    if ((name == (LOGIN_OBJECT + ".c")) ||
	(name == "/secure/konto_player.c") ||
	(name == (POSSESSION_OBJECT + ".c")) ||
	(name == (LOGIN_NEW_PLAYER + ".c")) ||
	(name == (WIZ_CMD_ARCH + ".c")))
    {
	return 1;
    }

    return 0;
}

/*
 * Nazwa funkcji: simul_efun_reload
 * Opis         : This function sets the authorisation variables for the
 *                simul_efun object.
 */
void
simul_efun_reload()
{
    set_auth(find_object(SIMUL_EFUN), "root:root");
}

/*
 * Nazwa funkcji: loaded_object
 * Opis         : This function is called when an object is loaded into
 *                memory by another object. It tests whether it was valid
 *                to load the object and sets the authorisation variables
 *                in the loaded object. If the load was not valid, throw()
 *                will terminate the execution.
 * Argumenty    : object lob - the loading object.
 *                object ob  - the loaded object.
 */
void
loaded_object(object lob, object ob)
{
    string creator = creator_object(ob);
    string *auth = explode(query_auth(lob), ":");

    if (!strlen(creator))
    {
        do_debug("destroy", ob);
        throw("Loading a bad object from: " + file_name(lob) + ".\n");
        return;
    }

    if (auth[1] == "0")
    {
        creator = file_name(ob);
        do_debug("destroy", ob);
        throw("Unauthorized load: " + creator + " by: " +
            file_name(lob) + ".\n");
        return;
    }

    if ((creator == BACKBONE_UID) || (creator == auth[0]))
    {
        set_auth(ob, (auth[1] + ":" + auth[1]));
        return;
    }

    set_auth(ob, (creator + ":0"));
}

/*
 * Nazwa funkcji: cloned_object
 * Opis         : This function is called when an object is cloned. It
 *                tests whether the clone was valid. It also sets the
 *                authorisation variable in the cloned object. If the
 *                clone was not valid, throw() will terminate the
 *                execution.
 * Argumenty    : object cob - the cloning object.
 *                object ob  - the cloned object.
 */
void
cloned_object(object cob, object ob)
{
    string creator = creator_object(ob);
    string *auth = explode(query_auth(cob), ":");

    if (!strlen(creator))
    {
        creator = file_name(ob);
        do_debug("destroy", ob);
        throw("Unauthorized clone: " + creator + " by: " +
            file_name(cob) + ".\n");
        return;
    }

    if (auth[1] == "0")
    {
        creator = file_name(ob);
        do_debug("destroy", ob);
        throw("Cloning without privilege: " + creator + " by: " +
            file_name(cob) + ".\n");
        return;
    }

    if ((creator == BACKBONE_UID) || (creator == auth[0]))
    {
        set_auth(ob, (auth[1] + ":" + auth[1]));
        return;
    }

    set_auth(ob, (creator + ":0"));
}

/*
 * Nazwa funkcji: modify_command
 * Opis         : Modify a command given by a certain living object. This can
 *                be used for many quicktyper-like functions. There are also
 *                some master.c defined substitutions. Commands that start
 *                with a dollar ($) are not substututed.
 * Argumenty    : string cmd - the command to modify.
 *                object ob - the object for which to modify the command.
 * Zwraca       : string - the modified command to execute.
 */
string
modify_command(string cmd, object ob)
{
    string str;
    string domain;
    int no_subst;

    while(wildmatch("$*", cmd))
    {
        cmd = extract(cmd, 1);
        no_subst = 1;
    }

    while(wildmatch(" *", cmd))
    {
        cmd = extract(cmd, 1);
    }

    if (strlen(str = command_substitute[cmd]))
    {
        if (query_ip_number(ob) &&
            !ob->query_wiz_level() &&
            pointerp(m_domains[domain = environment(ob)->query_domain()]))
        {
            m_domains[domain][FOB_DOM_CMNDS]++;
        }
        return str;
    }

    /* We can not allow any handwritten VBFC */

    /* No modification for NPC's */
    if (!query_ip_number(ob))
    {
	if (wildmatch("*@@*", cmd))
	{
	    cmd = implode(explode(cmd, "@@"), "##");
	}
        return cmd;
    }

    /* Count commands for ranking list */
    if (environment(ob) &&
        !ob->query_wiz_level() &&
        pointerp(m_domains[domain = environment(ob)->query_domain()]))
    {
        m_domains[domain][FOB_DOM_CMNDS]++;
    }

    /* No modification if it starts with a "$". */
    if (no_subst)
    {
	if (wildmatch("*@@*", cmd))
	{
	    cmd = implode(explode(cmd, "@@"), "##");
	}
        return cmd;
    }

    cmd = ob->modify_command(readable_string(cmd));
    if (wildmatch("*@@*", cmd))
    {
	cmd = implode(explode(cmd, "@@"), "##");
    }
    return cmd;
}

/*
 * Nazwa funkcji: query_memory_percentage
 * Opis         : This function will return the percentage of memory usage
 *                of the game so far. When the counter reaches 100, it is
 *                time to reboot.
 * Zwraca       : int - the relative memory usage.
 */
nomask public int
query_memory_percentage()
{
    int    f;
    int    cval;
    string mc;
    string foobar;

    mc = debug("malloc");
    sscanf(mc, "%ssbrk requests: %d %d (a) %s", foobar, f, cval, foobar);

    return (cval / (memory_limit / 100));
}

/*
 * Nazwa funkcji:   memory_failure
 * Opis         :   This function is called when the gamedriver considers
 *                  itself in trouble and need the game shut down in a
 *		    graceful manner. This function _must_ be called
 *                  via a call_other. It may only be called by root itself
 *                  or by a member of the administration.
 */
static void
memory_failure()
{
    if (!mem_fail_flag)
    {
        mem_fail_flag = 1;

        set_auth(this_object(), "root:root");
        ARMAGEDDON->start_shutdown("Ca^ly ^swiat zosta^l opanowany przez "
                                + "Ciemno^s^c.", 10, ROOT_UID);
    }
}

/*
 * Nazwa funkcji:   memory_reconfigure
 * Opis         :    This function is called when the gamedriver receives
 *		    an external signal, denoting that the memory status
 *		    has changed.
 * Argumenty    :   mem: Memory size, 0 small, 1 large.
 */
static void
memory_reconfigure(int mem)
{
    string mess = "a different";
    object *list;
    int    index;
    int    size;

#ifdef LARGE_MEMORY_LIMIT
    if (((mem == 0) && (memory_limit == SMALL_MEMORY_LIMIT)) ||
        ((mem == 1) && (memory_limit == LARGE_MEMORY_LIMIT)))
    {
        return;
    }

    if (mem == 0)
    {
        memory_limit = SMALL_MEMORY_LIMIT;
        mess = "ma�y";
    }
    else
    {
        memory_limit = LARGE_MEMORY_LIMIT;
        mess = "du�y";
    }
    check_memory(0);
#endif LARGE_MEMORY_LIMIT

    list = filter(users(), &->query_wiz_level());
    size = sizeof(list);
    index = -1;
    while(++index < size)
        tell_object(list[index], "@ " + date("G:i") + " Armageddon: Zmieni�em limit pami�ci na " + mess + ".\n");
}

/*
 * This function is called if the driver gets sent a signal that it catches.
 */
static void
external_signal(string sig_name)
{
    write("Received " + sig_name + " signal.\n");
    switch (sig_name)
    {
    case "INT":
	ARMAGEDDON->start_shutdown("Kto^s wstrzyma^l gr^e!", 0, ROOT_UID);
	break;
    case "HUP":
    case "KILL":
    case "QUIT":
    case "TERM":
        debug("shutdown");
        break;
    case "USR1":
	memory_reconfigure(0);
	break;
    case "USR2":
	memory_reconfigure(1);
	break;
    case "TSTP":
    case "CONT":
	break;
    case "UNKNOWN":
    default:
	write("Unknown signal received!\n");
    }
}

/*
 * Nazwa funkcji: query_memory_limit
 * Opis         : This function returns the current memory limit.
 */
public int
query_memory_limit()
{
    return memory_limit;
}

/*
 * Nazwa funkcji: query_memory_failure
 * Opis         : This function returns 1 if memory failure is detected.
 */
public int
query_memory_failure()
{
    return mem_fail_flag;
}

/*
 * Nazwa funkcji: log_incoming_service
 * Opis         : This function will make a log of the incomming service.
 * Argumenty    : string request - the request to log.
 *                string wname   - the wizard name.
 *                string path    - the path to log.
 */
#ifdef LOG_FTP
static string
log_incoming_service(string request, string wname, string path)
{
    string *parts;
    string fname;

    parts = explode(path, "/") - ({ "" });
    wname = lower_case(wname);

    if ((sizeof(parts) > 1) && (query_domain_number(parts[1]) >= 0))
	fname = parts[1];

    this_object()->log_syslog(("ftplog/" + (stringp(fname) ? fname : "Lib")),
        sprintf("%s %-5s %-11s %s\n", ctime(time()), request, wname, path),
			      5000000);
    this_object()->log_syslog(("ftplog/wizards/" + extract(wname, 0, 0) +
        "/" + wname), sprintf("%s %-5s %s\n", ctime(time()), request, path),
        1000000);
}
#endif LOG_FTP

/*
 * Nazwa funkcji: incoming_service
 * Opis         : Handle incoming request from other programs. This function
 *                may only be called from the gamedriver.
 * Argumenty    : string request - the request.
 * Zwraca       : string - the answer to the request.
 */
static string
incoming_service(string request)
{
    string *tmp;
    string str;
    string path;
    object ob;

    /* There must be a request, or we cannot answer it ;-) */
    if (!strlen(request))
        return "ERROR Bad request\n";


    /* The request may be separated by three different characters, \n,
     * \r or the space.
     */
    tmp = explode(request, "\n");
    if (sizeof(tmp))
    {
	request = tmp[0];
    }
    tmp = explode(request, "\r");
    if (sizeof(tmp))
    {
	request = tmp[0];
    }
    tmp = explode(request, " ");

    /* Switch on the request command. */
    switch (lower_case(tmp[0]))
    {
    case "user":
	if (sizeof(tmp) != 2)
	{
	    return "ERROR Wrong number of parameters\n";
	}

	tmp[1] = lower_case(tmp[1]);
	ob = find_player(tmp[1]);
	if (objectp(ob))
	{
	    if (query_wiz_rank(tmp[1]) >= WIZ_APPRENTICE)
	    {
		path = query_wiz_path(tmp[1]);
#ifdef LOG_FTP
		log_incoming_service("AUTH", tmp[1], path);
#endif LOG_FTP
		return ob->query_password() + ":" +
		    query_wiz_level(tmp[1]) + ":" +
		    path + "\n";
	    }
	}
	else
	{
	    ob = finger_player(tmp[1]);
	    if (objectp(ob))
	    {
		path = query_wiz_path(tmp[1]);
		str = ob->query_password() + ":" +
		    query_wiz_level(tmp[1]) + ":" +
		    path + "\n";
		ob->remove_object();
		if (query_wiz_rank(tmp[1]) >= WIZ_APPRENTICE)
		{
#ifdef LOG_FTP
		    log_incoming_service("AUTH", tmp[1], path);
#endif LOG_FTP
		    return str;
		}
	    }

	    /* This is done to ensure that no one will ever log in with the
	     * name of a domain. The "*" as password can never be matched,
	     * though it allows to type "cd ~Domain".
	     */
	    if (query_domain_number(capitalize(tmp[1])) >= 0)
	    {
		return "*:0:/d/" + capitalize(tmp[1]) + "\n";
	    }
	}
	return "ERROR No such user\n";

    case "read":
	if (sizeof(tmp) != 3)
	{
	    return "ERROR Wrong number of parameters\n";
	}
	if (valid_read(tmp[2], lower_case(tmp[1]), "FTP"))
	{
#ifdef LOG_FTP
	    log_incoming_service("READ", tmp[1], tmp[2]);
#endif LOG_FTP
	    return "READ access\n";
	}
	return "ERROR No access\n";

    case "write":
	if (sizeof(tmp) != 3)
	{
	    return "ERROR Wrong number of parameters\n";
	}
	if (valid_write(tmp[2], lower_case(tmp[1]), "FTP"))
	{
#ifdef LOG_FTP
	    log_incoming_service("WRITE", tmp[1], tmp[2]);
#endif LOG_FTP
	    return "WRITE access\n";
	}
	return "ERROR No access\n";

    default:
	return "ERROR Unknown request\n";
    }
}

#if 0
/*
 * Nazwa funkcji:   valid_save_binary
 * Opis         :   This function is called when a file has ordered the GD
 *                  to save a binary image of the program. This might not
 *		    be allowed by any and every file so master is asked.
 * Argumenty    :   file: Filename of the object.
 */
int
valid_save_binary(string filename)
{
    return 1;
}

/* ob wishing to inherit inherit_file */
int
valid_inherit(object ob, string inherit_file)
{
    return 1;
}

/* ob trying to load file */
int
valid_load(object ob, string file)
{
    return 1;
}
#endif

/*
 * Nazwa funkcji:   master_reload
 * Opis         :   Called from GD after a reload of the master object
 */
void
master_reload()
{
}

void
recreate(object old_master)
{
    create();
    game_started = 0;
    start_boot(1); /* This does what we want */
    game_started = 1;
#ifdef UDP_MANAGER
    udp_manager = UDP_MANAGER;
#endif
}

/*
 * Nazwa funkcji:   incoming_udp
 * Opis         :   Called from GD if a udp message has been received. This
 *		    can only happen if CATCH_UDP_PORT has been defined in
 *		    the GD's config.h file.
 * Argumenty    :    from_host: The IP number of the sending host
 *		    message:   The message sent.
 */
void
incoming_udp(string from_host, string message)
{
//    log_syslog("UDP_IO", sprintf("%15s < %s\n", from_host,
//               implode(explode(message, "\n"), "\\n")));

    if (stringp(udp_manager))
	udp_manager->incoming_udp(from_host, message);
    else
    {
	set_auth(this_object(), "#:root");
	log_file("LOST_UDP", "(" + from_host + ") " + message + "\n", -1);
    }
}

void
mark_quit(object player)
{
    string text;
    int index = 0;
    object prev;

    if ((query_idle(player) > 1200) ||
        (player->query_linkdead()) ||
        ((player == this_interactive()) &&
         (query_verb() == "zakoncz")))
    {
        return;
    }

    text = "Current time: " + ctime(time()) + "\nDestructed  : " +
        capitalize(player->query_real_name()) + ".\n";
    if (objectp(this_interactive()))
    {
        text += "Interactive : " +
            capitalize(this_interactive()->query_real_name()) + ".\n";
    }
    if (strlen(query_verb()))
    {
        text += "Queried verb: " + query_verb() + "\n";
    }

    while(objectp(prev = calling_object(index)))
    {
        text += "    call: " + file_name(prev) + "  " +
            calling_function(index) + "()";
        if (interactive(prev))
        {
            text += "  [" + capitalize(prev->query_real_name()) + "]";
        }
        text += "\n";

        if ((MASTER_OB(prev) + ".c") != ("/" + calling_program(index)))
        {
            text += "    file: /" + calling_program(index) + "\n";
        }

        index--;
    }

    set_auth(this_object(), "root:root");
    write_file("/syslog/log/DESTRUCTED", text + "\n");
}

/*
 * Nazwa funkcji: remove_interactive
 * Opis         : Called from GD if a player logs out or goes linkdead. If
 *                the player quit the game, we don't do anything.
 * Argumenty    : object ob    - the player that leaves the game.
 *                int linkdead - true if the player linkdied.
 */
static void
remove_interactive(object ob, int linkdied)
{
    string master_ob;

    QUEUE->dequeue(ob);

    /* If someone who is logging in linkdies, we just dispose of it. Also,
     * people who are trying to create a character, will have to start
     * over again.
     */
    master_ob = MASTER_OB(ob);
    if ((master_ob == LOGIN_OBJECT) ||
	(master_ob == LOGIN_NEW_PLAYER) ||
	(master_ob == GAMEINFO_OBJECT) ||
	(master_ob == KONTO_OBJECT))
    {
	ob->remove_object();
	return;
    }

    /* Player left the game. */
    if (!linkdied)
    {
	/* Notify the wizards of the fact that the player quit. */
	notify(ob, 1);
   // mark_quit(ob);
        return;
    }

    /* Notify the wizards of the linkdeath. */
    notify(ob, 2);
    /* -----------------------------------------------*/

#ifdef STATUE_WHEN_LINKDEAD
    ob->linkdie();
#endif STATUE_WHEN_LINKDEAD
}

/*
 * Nazwa funkcji: gamedriver_message
 * Opis         : This function may (only) be called by the gamedriver to
 *                give a message to all players if that is necessary.
 * Argumenty    : string str - the message to tell the people
 */
static void
gamedriver_message(string str)
{
    users()->catch_tell(str);
}

/*
 * Nazwa funkcji: runtime_error
 * Opis         : In case a runtime error occurs, we tell the message to
 *                the people who need to hear it.
 * Argumenty    : string error   - the error message.
 *                object ob      - the object that has the error.
 *                string program - the program name of the error.
 *                string file    - the filename of the error.
 */
static void
runtime_error(string error, object ob, string prog, string file)
{
    string fmt_error, cr, path;

    
    fmt_error =
        "\n\nRuntime error: " + error +
        "       Obiekt: " + (ob ? file_name(ob) : "<???>") +
        "\n      Program: " + prog +
        "\n         Plik: " + file + "\n\n";
//test2
    if (this_interactive())
    {
        if (this_interactive()->query_wiz_level() ||
                this_interactive()->query_prop(PLAYER_I_SEE_ERRORS))
        {
            this_interactive()->catch_tell(fmt_error);
        }
        else
        {
            notify_wizards(this_interactive(),fmt_error);
            this_interactive()->catch_tell("Wyst^api^l powa^zny b^l^ad. " +
                    "Zg^lo^s go, opisuj^ac okoliczno^sci w jakich do niego dosz^lo.\n");
        }
    }
    path = "";
    if (objectp(ob))
    {
        cr = creator_object(ob);
        path = query_wiz_path(cr);
    }
    path += "/log/runtime";
    fmt_error = ctime(time()) +
        "\nRuntime error: " + error +
        "       Obiekt: " + (ob ? file_name(ob) : "<???>") +
        "\n      Program: " + prog +
        "\n         Plik: " + file + "\n\n";

    write_file(path, fmt_error);
}

/*
 *    ----------------------------------------------------------------
 *    The code below this divisor is never called from the gamedriver.
 *    ----------------------------------------------------------------
 */

/**
 * Funkcja przenosi plik gracza z /players/konta/ do
 * /players/konta/deleted.
 * Dodatkow dodaje teksd do logo DELETED
 * @param account - konto do usuni�cia
 * @param reason  - pow�d usuni�cia
 * @return 1/0    ok/b��d
 */
public int
remove_accountfile(string account, string reason)
{
    string file, deleted;
    int number = 1;
    string konto;
    mapping map;

    // Mo�e by� wywo�ane tylko z soula helpera
    if (!CALL_BY(WIZ_CMD_HELPER))
        return 0;

    file = KONTO_FILE(account) + ".o";
    deleted = DELETED_KONTO_FILE(account) + ".o";

    /* If there is a file, move it to the deleted dir. */
    if (file_size(file) < 0)
    {
        notify_fail("Brak pliku konta: " + file + ".\n", 2);
        return 0;
    }

    if (file_size(deleted) != -1)
    {
        while (file_size(deleted + "." + number) != -1)
            number++;

        deleted += "." + number;
    }
    else
        number = 0;

    /* Move the file */
    if (!rename(file, deleted))
        return 0;

    /* Log the action */
    log_file("DELETED_ACCOUNTS", ctime(time()) + " " + capitalize(account) +
        " przez " + capitalize(this_interactive()->query_real_name()) +
        " (" + reason + ")\n", -1);

    return 1;
}

/*
 * Nazwa funkcji: remove_playerfile
 * Opis         : This function moves a playerfile from /players/<?>/
 *                to /players/deleted/<?>/
 *                It also adds some text to the log DELETED.
 * Argumenty    : player - The player to remove
 *                reason - The reason to why the file is removed
 * Zwraca       : True if everything went ok, false other wise
 */
public int
remove_playerfile(string player, string reason)
{
    string file, deleted;
    int number = 1;
    string konto;
    mapping m;

    /* Mo�e by� wywo�ane tylko z soula helpera i
        w wiosce przy kasowaniu gracza kt�ry zrezygnowa� z tworzenia postaci */
    if (!CALL_BY(WIZ_CMD_HELPER) && stringp(finger_player(player)->query_race_name()))
        return 0;

    file = PLAYER_FILE(player) + ".o";
    deleted = DELETED_FILE(player) + ".o";

    /* If there is a file, move it to the deleted dir. */
    if (file_size(file) < 0)
    {
        notify_fail("Brak pliku postaci: " + file + ".\n");
        return 0;
    }

    konto = finger_player(player)->query_mailaddr();
    if (file_size(KONTO_FILE(konto + ".o")) >= 0)
    {
        m = restore_map(KONTO_FILE(konto));

        if(!pointerp(m["postacie"]))
            m["postacie"] = ({});

        m["postacie"] -= ({ player });

        if(!pointerp(m["postacie_skasowane"]))
            m["postacie_skasowane"] = ({});

        m["postacie_skasowane"] += ({ player });
        save_map(m, KONTO_FILE(konto));
    }

    if (file_size(deleted) != -1)
    {
        while (file_size(deleted + "." + number) != -1)
            number++;

        deleted += "." + number;
    }
    else
        number = 0;

    /* Move the file */
    if (!rename(file, deleted))
        return 0;

    /* Usuniecie poczty delikwenta */
    file = sprintf("%s%s/%s.o", MAIL_DIR, player[0..0], player);
    if (file_size(file) >= 0)
        rm(file);

    /* Log the action */
    log_file("DELETED", ctime(time()) + " " + capitalize(player) +
        " przez " + capitalize(this_interactive()->query_real_name()) +
        " (" + reason + ")\n", -1);

    return 1;
}

/**
 * Ta funkcja odpowiada za ustawienie graczowi final_death na 1
 */
public void set_player_final_death(object player)
{
    find_player("krun")->catch_msg("Death = " + MASTER_OB(previous_object()) + " std = " + DEATH_ROOM + "\n");

    //Mo�emy wywo�a� tylko z death_rooma, tylko na graczu
    if(MASTER_OB(previous_object()) != DEATH_ROOM)
        return;

    player->set_final_death();

    mapping m;
    m = restore_map(KONTO_FILE(player->query_mailaddr()));

    if(!pointerp(m["postacie"]))
        m["postacie"] = ({});

    m["postacie"] -= ({player->query_real_name()});

    if(!pointerp(m["postacie_krzyzyk"]))
        m["postacie_krzyzyk"] = ({});

    m["postacie_krzyzyk"] += ({player->query_real_name()});

    save_map(m, KONTO_FILE(player->query_mailaddr()));
}

/*
 * Nazwa funkcji: rename_playerfile
 * Opis         : This renames a player.
 * Argumenty    : string oldname - the old name of the player.
 *                string newname - the new name of the player.
 * Zwraca       : int 1/0 - success/failure.
 */
public int
rename_playerfile(string oldname, string newname)
{
    mapping playerfile;
    mapping map;
    string konto;

    /* May only be called from the Arch soul. */
    if (!CALL_BY(WIZ_CMD_ARCH))
    {
        return 0;
    }

    /* Rename the playerfile itself. */
    if (!rename(PLAYER_FILE(oldname) + ".o",
        PLAYER_FILE(newname) + ".o"))
    {
        notify_fail("Renaming playerfile failed!\n");
        return 0;
    }

    /* The name should also be updated in the playerfile. */
    playerfile = restore_map(PLAYER_FILE(newname));
    playerfile["name"] = newname;
    save_map(playerfile, PLAYER_FILE(newname));
    write("Player " + capitalize(oldname) + " succesfully renamed to " +
        capitalize(newname) + ".\n");

    konto = finger_player(newname)->query_mailaddr();
    if (file_size(KONTO_FILE(konto)) >= 0)
    {
        map = restore_map(KONTO_FILE(konto));
        map["postacie"] -= ({ oldname });
        map["postacie"] += ({ newname });
        save_map(map, KONTO_FILE(konto));
    }

    /* Rename the mail folder if there is one. */
    if (file_size(FILE_NAME_MAIL(oldname) + ".o") > 0)
    {
        if (rename(FILE_NAME_MAIL(oldname) + ".o",
            FILE_NAME_MAIL(newname) + ".o"))
        {
            write("Mail folder found and renamed.\n");
        }
        else
        {
            write("Mail folder found, but renaming failed.\n");
        }
    }

    /* Update a wizard. */
    if (query_wiz_rank(oldname))
    {
        rename_wizard(oldname, newname);
    }

    log_file("DELETED", ctime(time()) + " " + capitalize(oldname) +
        " -> " + capitalize(newname) + ", renamed by " +
        capitalize(this_interactive()->query_real_name()) + ".\n", -1);
    return 1;
}

/*
 * Nazwa funkcji: proper_password
 * Opis         : This function can be used to check whether a certain
 *                password is less likely to be broken using a general
 *                cracker. Therefore the following is checked:
 *                - the password must at least be 6 characters long;
 *                - the password must contain at least one 'non-letter';
 *                - this letter may not be the first or the last letter;
 * Argumenty    : string str - the password to check.
 * Zwraca       : int 1/0 - proper/bad.
 */
int
proper_password(string str)
{
    int index = -1;
    int size;
    int normal;
    int stage = 0;

    /* Length of the password must be at least 6 characters. */
    size = strlen(str);
    if (size < 6)
    {
	return 0;
    }

    /* This may seem a little strange, but it actually is quite simple. As
     * stated before, there should at least be one non-letter and that
     * should be between normal letters. Therefore, starting at stage 0, we
     * wait until we find a letter. Have we found that, more to
     * stage 1 and wait for a non-letter. Then, at stage 2 we again wait for
     * a normal letter. If all checks out, we should be at stage 3 at the
     * end.
     */
    str = lower_case(str);
    while(++index < size)
    {
	normal = ((str[index] >= 'a') && (str[index] <= 'z'));

	switch(stage)
	{
	case 0:
	    stage = (normal ? 1 : 0);
	    break;

	case 1:
	    stage = (normal ? 1 : 2);
	    break;

	case 2:
	    stage = (normal ? 3 : 2);
	    break;
	}
    }

    /* If the stage isn't 3, the password isn't good. */
    return (stage == 3);
}

/*
 * Nazwa funkcji: generate_password
 * Opis         : This function will generate a six character password that
 *                consists of letters, numbers and or special characters.
 *                Passwords generated with this function are considered to
 *                be safe enough against cracking programs.
 * Zwraca       : string - the password.
 */
public string
generate_password()
{
    string tmp = "";
    int    size = 8;
    int    index;

    while(--size >= 0)
    {
	switch(random(5))
	{
	case 0:
	    /* With 20% change, add a single digit */
	    tmp += ("" + random(10));
	    break;

	case 1:
	    /* With 20% chance, add a "special" character */
	    index = random(24);
	    tmp += "@?%^^&*()[]{};:<>,.-_=+"[index..index];
	    break;

	case 2..4:
	default:
	     /* With 60% chance, add a letter, capitalized with 50% chance */
	    index = random(26);
	    tmp += (random(2) ? ALPHABET[index..index] :
		capitalize(ALPHABET[index..index]));
	    break;
	}
    }

    return tmp;
}

/*
 * Nazwa funkcji: remote_setuid
 * Opis         : With this function the COMMAND_DRIVER can request that
 *                its authorisation information is reset in order to allow
 *                for a new uid/euid when another player uses the same soul.
 */
void
remote_setuid()
{
    if (function_exists("open_soul", previous_object()) ==
	COMMAND_DRIVER)
    {
	set_auth(previous_object(), "0:0");
    }
}

/*
 * Nazwa funkcji: set_helper_soul_euid
 * Opis         : The helper soul is intended for wizards to get some commands
 *                for abilities they would not normally enjoy. Thus, the soul
 *                requires a higher access level than normally. Make sure that
 *                only the right object qualifies, though.
 */
public void
set_helper_soul_euid()
{
    if (file_name(previous_object()) == WIZ_CMD_HELPER)
    {
        set_auth(previous_object(), "root:root");
    }
}

/*
 * Nazwa funkcji: creator_object
 * Opis         : Gives the name of the creator of an object. This is a
 *                direct function of the file_name() of the object.
 * Argumenty    : object obj - the object to get the creator from.
 * Zwraca       : string - the name of the domain or wizard who created
 *                         the object.
 */
string
creator_object(object obj)
{
    if (!objectp(obj))
    {
	return 0;
    }

    return creator_file(file_name(obj));
}

/*
 * Nazwa funkcji:   domain_object
 * Opis         :   Gives the name of the domain of an object. This is a
 *                  direct function of the file_name() of the object.
 * Zwraca       :   Name of the domain
 */
string
domain_object(object obj)
{
    string str,dom,wiz,name;

    if (!obj) return 0;

    str = file_name(obj);
    if (str[1] == 'd')
	sscanf(str,"/d/%s/%s/%s",dom,wiz,name);
    else
	dom = 0;

    return dom;
}

/*
 * Nazwa funkcji: load_player
 * Descripton:    This function is called from /std/player_sec
 *		  when the player object is loaded initially.
 *                It sets the euid of the player to root for
 *                the duration of the load.
 */
int
load_player()
{
    int res;
    object pobj;

    pobj = previous_object();

    if (function_exists("load_player", pobj) != PLAYER_SEC_OBJECT ||
	!LOGIN_NEW_PLAYER->legal_player(pobj))
	return 0;
    else
    {
	set_auth(this_object(), "#:root");
	export_uid(pobj);
	res = (int)pobj->load_player(pobj->query_real_name());
	set_auth(this_object(), "#:" + (pobj->query_wiz_level() ?
			  pobj->query_real_name() : BACKBONE_UID));
	export_uid(pobj);
	set_auth(this_object(), "#:root");
	return res;
    }
}

/*
 * Nazwa funkcji:   save_player
 * Opis         :   Saves a player object.
 */
int
save_player()
{
    int res;
    object pobj;

    pobj = previous_object();

    if ((function_exists("save_player", pobj) != PLAYER_SEC_OBJECT) ||
	!LOGIN_NEW_PLAYER->legal_player(pobj))
    {
	return 0;
    }
    else
    {
	set_auth(this_object(), "#:backbone");
        pobj->fix_saveprop_list();
	set_auth(this_object(), "#:root");
	export_uid(pobj);
	res = (int)pobj->save_player(pobj->query_real_name());
	pobj->open_player();
	set_auth(this_object(), "#:" + (pobj->query_wiz_level() ?
					pobj->query_real_name() :
					BACKBONE_UID));
	export_uid(pobj);
	set_auth(this_object(), "#:root");
	return res;
    }
}

/*
 * Nazwa funkcji: save_room
 * Opis         : Zapisuje obiekt lokacji
 * Zwraca       : int - wynik funkcji zapisz_lokacje
 */
int
save_room()
{
    int res;
    object pobj;

    pobj = previous_object();

    if ((function_exists("zapisz_lokacje", pobj) != "/std/room"))
        return 0;

    else
    {
        set_auth(this_object(), "#:root");
        export_uid(pobj);
        res = (int)pobj->zapisz_lokacje(file_name(pobj));
        set_auth(this_object(), "#:" + BACKBONE_UID);
        export_uid(pobj);
        set_auth(this_object(), "#:root");
        return res;
    }
}

int
save_craftsman()
{
    int res;
    object pobj;

    pobj = previous_object();

    if ((function_exists("save_craftsman", pobj) != "/lib/craftsman"))
    {
        return 0;
    }
    else
    {
        set_auth(this_object(), "#:root");
        export_uid(pobj);
        res = (int)pobj->save_craftsman(MASTER_OB(pobj));
        set_auth(this_object(), "#:" + BACKBONE_UID);
        export_uid(pobj);
        set_auth(this_object(), "#:root");
        return res;
    }
}

/*
 * Nazwa funkcji: load_planpogody
 * Opis         : Klonuje standardowy plan pogody i zwraca gotowy obiekt.
 *
 * Zwraca       : object - standardowy plan pogody
 */

object
load_planpogody()
{
    return clone_object("/std/plan_pogodowy");
}

int
rem_def_start_loc(string str)
{
    if (!def_locations)
        def_locations = STARTING_PLACES;

    if (query_wiz_rank(this_interactive()->query_real_name()) < WIZ_ARCH)
    {
        write("Tylko archowie i keeperzy maj� prawo usun�� lokacje startow�.\n");
        return 1;
    }

    set_auth(this_object(), "#:root");
    while (member_array(str, def_locations) >= 0) /* Delete  */
        def_locations = def_locations - ({ str });

    save_master();
}

int
add_def_start_loc(string str)
{
    if (!def_locations)
        def_locations = STARTING_PLACES;

    if (query_wiz_rank(this_interactive()->query_real_name()) < WIZ_ARCH)
    {
        write("Tylko arch lub keeper ma prawo doda^c lokacj^e startow^a.\n");
        return 1;
    }

    if (file_size(str + ".c") < 0)
    {
        write("Brak pliku: " + str + "\n");
        return 1;
    }
    while (member_array(str, def_locations) >= 0) /* Delete copies */
        def_locations = def_locations - ({ str });

    def_locations = def_locations + ({ str });
    save_master();
}

int
rem_temp_start_loc(string str)
{
    if (!temp_locations)
        temp_locations = TEMP_STARTING_PLACES;

    if (query_wiz_rank(this_interactive()->query_real_name()) < WIZ_ARCH)
    {
        write("Tylko arch lub keeper ma prawo usunac lokacje startowa.\n");
        return 1;
    }
    while (member_array(str, temp_locations) >= 0) /* Delete copies */
        temp_locations = temp_locations - ({ str });

    save_master();
}

int
add_temp_start_loc(string str)
{
    if (!CALL_BY(WIZ_CMD_ARCH))
    {
        write("Tylko arch lub keeper ma prawo doda^c lokacj^e startow^a.\n");
        return 1;
    }
    if (file_size(str + ".c") <= 0)
    {
        write("Brak pliku: " + str + "\n");
        return 1;
    }
    if (!sizeof(temp_locations))
        temp_locations = TEMP_STARTING_PLACES;

    temp_locations = (temp_locations - ({ str }) ) + ({ str });
    save_master();
}

public string *
query_list_def_start()
{
    return secure_var(def_locations);
}

public string *
query_list_temp_start()
{
    return secure_var(temp_locations);
}

int
check_temp_start_loc(string str)
{
    if(temp_locations)
        temp_locations = TEMP_STARTING_PLACES;

    return member_array(str, temp_locations);
}

int
check_def_start_loc(string str)
{
    if (!def_locations)
        def_locations = STARTING_PLACES;
    return member_array(str, def_locations);
}

public varargs void
log_syslog(string file, string text, int length = 0)
{
    string fname;

    fname = calling_program();

    if ((fname[0..5] != "secure") &&
        (fname[0..3] != "cmd/") &&
        (fname[0..3] != "std/") &&
        (fname[0..15] != "d/Standard/login/") &&
        (fname != "d/Standard/obj/bank_central.c") &&
        (fname != "d/Standard/obj/odmieniacz.c"))
    {
        return;
    }

    set_auth(this_object(), "#:root");
    log_file(file, text, length);
}

void
log_public(string file, string text)
{
    int fsize, msize;
    string fname;

    fname = calling_program();

    file = OPEN_LOG_DIR + "/" + file;

//    if(fname[0..5] != "secure" &&
//       fname[0..9] != "std/living")
//	return;

#ifdef CYCLIC_LOG_SIZE
    fsize = file_size(file);
    msize = CYCLIC_LOG_SIZE["root"];

    if (msize > 0 && fsize > msize)
        rename(file, file + ".old");
#endif /* CYCLIC_LOG_SIZE */

    set_auth(this_object(), "#:root");
//    write_file(file, ctime(time()) + "\n" + text);
    write_file(file, text);
}

/*
 * Nazwa funkcji: query_player_file_time()
 * Opis         : This function will return the file-time the playerfile
 *                of a player was last saved.
 * Argumenty    : string pl_name - the name of the player.
 * Zwraca       : int - the file-time of the players save-file.
 */
int
query_player_file_time(string pl_name)
{
    if (!strlen(pl_name))
        return 0;

    pl_name = lower_case(pl_name);
    set_auth(this_object(), "root:root");
    return file_time(PLAYER_FILE(pl_name) + ".o");
}

/*
 * Nazwa funkcji: exist_player
 * Opis         : Checks whether a player exist or not.
 * Argumenty    : string pl_name: the name of the player to check.
 * Zwraca       : int 1/0 - true if the player exists.
 */
int
exist_player(string pl_name)
{
    if (!strlen(pl_name))
        return 0;

    pl_name = lower_case(pl_name);
    return (file_size(PLAYER_FILE(pl_name) + ".o") > 0);
}

/**
 * Funkcja ma za zadanie zwr�ci� obiekt gracza, kt�ry mo�e nie by� zalogowany w grze.
 * Na obiekcie takim wykona� mo�na wi�kszo�� funkcji wykonywalnych na graczu.
 * @param pl_name Nazwa gracza
 * @param file    Nazwa pliku gracza (opcjonalnie, mo�e zosta� podany jako int, b�dzie wtedy uwa�any za przyp)
 * @param przyp   Przypadek w jakim nazwa zosta�a podana (opcjonalnie)
 *
 * @return  Obiekt gracza.
 */
varargs object
finger_player(string pl_name, mixed file, int przyp)
{
    object ob;
    int ret, lev;
    string f, pobj;

    if (strlen(pl_name) == 0)
        return 0;

    if(intp(file))
    {
        przyp = file;
        file = 0;
    }

    if (!file)
        file = FINGER_PLAYER;

    set_auth(this_object(), "#:backbone");
    ob = clone_object(file);

    f = function_exists("load_player", ob);
    if (f != PLAYER_SEC_OBJECT && f != FINGER_PLAYER)
    {
        do_debug("destroy", ob);
        return 0;
    }

    ob->master_set_name(pl_name);
    ob->open_player();               /* sets euid == 0 */
    set_auth(this_object(), "#:root");
    export_uid(ob);
    ret = ob->load_player(pl_name, przyp);

    set_auth(this_object(), "#:backbone");
    export_uid(ob);			/* Make the object powerless */
    ob->set_trusted(1);
    set_auth(this_object(), "#:root");
    ob->dodaj_przym(0);			/* Set the adjectives correctly */

    pobj = (previous_object() ? MASTER_OB(previous_object()) : 0);
    if (pobj &&
        (pobj != LOGIN_OBJECT) &&
        (pobj != CMD_LIVE_SOCIAL) &&
        (pobj != WIZ_CMD_APPRENTICE) &&
        (pobj != WIZ_CMD_NORMAL) &&
        (pobj != PLAYER_ADMIN))
    {
        log_syslog("FINGER_OB", sprintf("%s: %s%s\n%s\n", ctime(time()),
            (this_interactive() ? "(" + this_interactive()->query_real_name() + ") " : ""),
            pl_name, DEBUG_FMT_CALLS(0)));
    }

    if (ret)
        return ob;
    else
    {
        ob->remove_object();
        return 0;
    }
}

/*
 * finger_konto
 */
varargs object
finger_konto(string pl_name, string file)
{
    object ob;
    int ret, lev;
    string f, pobj;

    if (strlen(pl_name) == 0)
        return 0;

    if (!file)
        file = FINGER_KONTO;

    set_auth(this_object(), "#:backbone");
    ob = clone_object(file);

    ob->master_set_name(pl_name);
    ob->open_konto();               /* sets euid == 0 */
    set_auth(this_object(), "#:root");
    export_uid(ob);
    ret = ob->load_konto(pl_name);

    set_auth(this_object(), "#:backbone");
    export_uid(ob);	                /* Make the object powerless */
    ob->set_trusted(1);
    set_auth(this_object(), "#:root");

    pobj = (previous_object() ? MASTER_OB(previous_object()) : 0);
    if (pobj && (pobj != LOGIN_OBJECT) && (pobj != CMD_LIVE_SOCIAL) &&
        (pobj != WIZ_CMD_APPRENTICE) && (pobj != WIZ_CMD_NORMAL))
    {
        log_syslog("FINGER_OB", sprintf("%s: %s%s\n%s\n", ctime(time()),
            (this_interactive() ? "(" + this_interactive()->query_real_name() + ") "
            : ""), pl_name, DEBUG_FMT_CALLS(0)));
    }

    if (ret)
        return ob;
    else
    {
        ob->remove_object();
        return 0;
    }
}

/*
 * Nazwa funkcji: note_something
 * Opis         : This function is called from the info.c commandsoul when
 *                someone made a report. It distuishes between sys-reports
 *                and room-related reports and writes them to the correct
 *                directory.
 * Argumenty    : string str - the message to log.
 *                int id     - the id (type) of the log.
 *                object env - the environment of this_player().
 */
void
note_something(string str, int id, object env)
{
    string file;
    string text = extract(ctime(time(), 1), 3, -4);

    if (extract(text, 0, 0) == " ")
        text = extract(text, 1);

    /* If there is a SYS-related log, write it to the proper log file in the
     * OPEN_LOG_DIR.
     */
    if(id == LOG_SYSBUG_ID || id == LOG_SYSIDEA_ID || id == LOG_SYSPRAISE_ID)
    {
        set_auth(this_object(), "#:root");

        if(id == LOG_SYSPRAISE_ID)
            PRAISE_OBJECT->post_msg(text, id, "GLOBALNA", capitalize(TP->query_real_name()), str);
        else
            LOG_OBJECT->post_msg(text, id, "GLOBALNY", capitalize(this_player()->query_real_name()), str);

        write_file((OPEN_LOG_DIR + "/SYS" + LOG_PATH(id)), (text + " " +
            file_name(env) + " (" + capitalize(this_player()->query_real_name()) + ")\n" + str + "\n"));
        return;
    }

    file = creator_object(env);
    string of = file;

    file = query_wiz_path(file) + "/log";
    if (file_size(file) != -2)
        mkdir(file);

    file += LOG_PATH(id);

    string adres;
    if(!interactive(env) && env)
    {
        adres=file_name(env);
        if(explode(adres, "/")[1] == "d" && explode(adres, "/")[2] == "Standard")
        {
            adres = implode(explode(adres, "/")[3..], "/");
            adres = "../" + adres;
        }
    }
    else
        adres = "W postaci o imieniu " + env->query_name(PL_MIA);

    //Lokalne b��dy wiza nie s� wrzucane na tablice. Mo�e kiedy� zrobi si� tablice wizowskie
    //a narazie wsio l�duje tylko w plikach.
    if(LC(of) == "standard" || LC(of) == "root" || LC(of) == "backbone")
    {
        if(id == LOG_PRAISE_ID)
            PRAISE_OBJECT->post_msg(text, id, adres, capitalize(this_player()->query_real_name()), str);
        else if(id == LOG_BREAKDOWN_ID)
        {
            BREAKDOWN_OBJECT->post_msg(text, id, "Z�amane przez " + env->query_name(PL_BIE),
                capitalize(this_player()->query_real_name()), str);
        }
        else
            LOG_OBJECT->post_msg(text, id, adres, capitalize(this_player()->query_real_name()), str);
    }

    write_file(file, text + " " + (env ? file_name(env) : "") + " (" +
        capitalize(this_player()->query_real_name()) + ")\n" + str + "\n");
}

/*
 * Nazwa funkcji:   query_snoop
 * Opis         :   Tells caller if a user is snooped.
 * Argumenty    :    snoopee: pointer to a user object
 * Zwraca       :   0 if not snooped, 1 if snooped and caller is lord or lower
 *                  object pointer to snooper if caller is arch or higher
 */
mixed
query_snoop(object snoopee)
{
    object snooper;
    int type = query_wiz_rank(geteuid(previous_object()));

    if (type >= WIZ_ARCH)
        return efun::query_snoop(snoopee);
    else if (check_snoop_validity(previous_object(), snoopee, 1) &&
        (snooper = (efun::query_snoop(snoopee))) &&
        (type >= query_wiz_rank(snooper->query_real_name())))
    {
        return 1;
    }
    else
        return 0;
}

/*
 * Nazwa funkcji: query_start_time
 * Opis         : Return the time when the game started.
 * Zwraca       : int - the time.
 */
public int
query_start_time()
{
    int theport;
    string game_start;

    theport = debug("mud_port");
    if (theport != 0)
    {
        game_start = GAME_START + "." + theport;

        if (file_size(game_start) > 0)
            return min(file_time(game_start), object_time(this_object()));
    }

    /* This value will be wrong if the master has been updated. */
    return object_time(this_object());
}

/*
 * Nazwa funkcji: query_irregular_uptime
 * Opis         : The (irregular) uptime after which this game is being
 *                rebooted. This uptime is counted from the start of the
 *                game.
 * Zwraca       : int - the (irregular) uptime, or 0.
 */
public int
query_irregular_uptime()
{
    return irregular_uptime;
}

/*
 * Nazwa funkcji: commune_log
 * Opis         : Logs a commune from a mortal.
 * Argumenty    : string str  - the message.
 */
public void
commune_log(string str)
{
    log_public("COMMUNE", sprintf("%s: %-11s: %s", ctime(time()), this_interactive()->query_real_name(), str));
}

public void
set_auto_load_rooms(int on)
{
    auto_load_rooms = on;
}

public int
query_auto_load_rooms()
{
    return auto_load_rooms;
}

/*
 * Nazwa funkcji: set_runlevel
 * Opis         : Set the runlevel. That is the lowest rank of player that
 *                is allowed into the game. Set to WIZ_MORTAL (0) when the
 *                game is open to all players and wizards. This function
 *                may only be called from the normal wizard soul, i.e. from
 *                the 'shutdown' command.
 * Argumenty    : int - the runlevel. The argument is not checked for
 *                    validity. That must have been done in the command.
 */
public void
set_runlevel(int level)
{
    if (previous_object() != find_object(WIZ_CMD_MAGE))
    {
        write("Pr�ba nielegalnego wywo�ania set_runlevel().");
        log_file(LOG_SHUTDOWN, ctime(time()) + " Pr�ba nielegalnego wywo�ania " +
            "funkcji set_runlevel.\n");
        return;
    }

#ifdef LOG_SHUTDOWN
    log_file(LOG_SHUTDOWN, ctime(time()) + " Runlevel " +
        WIZ_RANK_NAME(level) + " set by " + this_interactive()->query_name() +
        ".\n", -1);
#endif LOG_SHUTDOWN

    runlevel = level;

    save_master();
}

/*
 * Nazwa funkcji: query_runlevel
 * Opis         : Returns the runlevel. This is the lowest rank of player
 *                that is allowed into the game. It returns WIZ_MORTAL (0)
 *                when the game is open to all players and wizards.
 * Zwraca       : int - the runlevel.
 */
public int
query_runlevel()
{
    return runlevel;
}
/*
 * Nazwa funkcji: master_shutdown
 * Opis         : Perform the final shutdown. This function may only be
 *                called from the armageddon object.
 * Zwraca       : 1 - Ok, 0 - No shutdown performed.
 */
public int
master_shutdown(string reason)
{
    if (MASTER_OB(previous_object()) != ARMAGEDDON)
        return 0;

#ifdef LOG_SHUTDOWN
    log_file(LOG_SHUTDOWN, ctime(time()) + " " + reason, -1);
#endif LOG_SHUTDOWN

    INFORMATOR->uruchom_wydarzenie(WD_APOKALIPSA_WYWOLANIE);

    /* This MUST be a this_object()->
     * If it is removed the game wont go down, so hands off!
     */
    this_object()->do_debug("shutdown");
    return 1;
}

/*
 * Nazwa funkcji: request_shutdown
 * Opis         : When a wizard wants to shut down the game, this
 *                function is called to invoke Armageddon. The function
 *                should be called from the shutdown command in
 *                WIZ_CMD_NORMAL.
 * Argumenty    : string reason - the reason to shut down the game.
 *                int    delay  - the delay in minutes.
 */
public void
request_shutdown(string reason, int delay)
{
    string euid    = getwho();
    string shutter = ARMAGEDDON->query_shutter();

    if (strlen(shutter))
    {
        write("Aktualnie trwa ju� apokalipsa kt�r� zapocz�tkowa�: " + shutter + ".\n");
        return;
    }

    if (query_wiz_rank(euid) < WIZ_MAGE)
    {
        write("Ty nie mo�esz wywo�a� apokalispy. Anuluje j�.\n");
        return;
    }

    if (reason == "memory_failure")
    {
        /* Arches and keepers can force a memory failure every time. Other
        * wizards can only do so if the memory usage is >= 90%.
        */
        if ((query_wiz_rank(euid) < WIZ_ARCH) && (query_memory_percentage() < 90))
        {
            write("Jeszcze nie ma powodu do wywo�ywania memory_failure().\n");
            return;
        }

        memory_failure();
        return;
    }

    INFORMATOR->uruchom_wydarzenie(WD_APOKALIPSA);

    ARMAGEDDON->start_shutdown(reason, delay, euid);
}

/*
 * Nazwa funkcji: calcel_shutdown
 * Opis         : When the wizard has second thoughts and does not want
 *                to shut the game down after all, this function is
 *                called. The function should be called from
 *                WIZ_CMD_NORMAL.
 */
public void
cancel_shutdown()
{
    string euid    = getwho();
    string shutter = ARMAGEDDON->query_shutter();
    int    rank    = query_wiz_rank(euid);

    if (mem_fail_flag)
    {
        write("Vatt'ghern jest restartowany z powodu zbyt du�ego zu�ycia pami�ci RAM. Nie ma "+
            "mo�liwo�ci zatrzymania apokalipsy.\n");
        return;
    }

    if (rank <= WIZ_MAGE)
    {
        write("Ty nie mo�esz zatrzymywa� apokalips.\n");
        return;
    }

    if ((euid != shutter) && (rank <= WIZ_LORD))
    {
        write("Nie mo�esz zatrzyma� apokalipsy wywo�anej przez kogokolwiek innego niz ty.\n");
        return;
    }

    ARMAGEDDON->cancel_shutdown(euid);
}
/*
 * Nazwa funkcji:  wiz_home
 * Opis         :  Gives a default 'home' for a wizard, domain or a player
 * Argumenty    :  wiz: The wizard name.
 * Zwraca       :  A filename for the 'home' room.
 */
string
wiz_home(string wiz)
{
    string path;

    if (query_wiz_rank(wiz) == WIZ_MORTAL)
        if (query_domain_number(wiz) < 0)	/* Not even a domain */
            return "";

    path = query_wiz_path(wiz) + "/workroom.c";
    set_auth(this_object(), "#:root");
    if (file_size(path) <= 0)
        write_file(path, "inherit \"/std/workroom\";\n\n" +
            "void\ncreate_workroom()\n{\n  ::create_workroom();\n}\n");

    return path;
}

/*
 * Nazwa funkcji:  wiz_force_check
 * Opis         :  Checks if one wizard is allowed to force another
 * Argumenty    :  forcer: Name of wizard trying to force
 *		   forced: Name of wizard being forced
 * Zwraca       :  True if ok
 */
int
wiz_force_check(string forcer, string forced)
{
    int             rlev,
                    dlev;

    if (forcer == forced)
        return 1;

    rlev = query_wiz_rank(forcer);
    if ((rlev == WIZ_KEEPER) || (forcer == ROOT_UID))
        return 1;

    dlev = query_wiz_rank(forced);

    if ((rlev >= WIZ_ARCH) && (dlev <= WIZ_LORD))
        return 1;

    return 0;
}

/*
 * Nazwa funkcji: set_sanctioned
 * Opis         : Set the 'sanctioned' field in the player.
 * Argumenty    : wizname - The wizard to set in
 *		  map - The 'sanctioned' map to set.
 */
public int
set_sanctioned(string wizname, mapping map)
{
    string wname;
    object wiz;

    wname = geteuid(previous_object());

    if ((wname != wizname) && (query_wiz_rank(wname) < WIZ_ARCH))
        return 0;

    wiz = find_player(wizname);

    if (!wiz)
        return 0;

    wiz->set_sanctioned(map);
    return 1;
}

/*
 * Nazwa funkcji: query_banished
 * Opis         : Find out whether a name has been banished for use by
 *                new players.
 * Argumenty    : string name - the name to check for banishment.
 * Zwraca       : int 1/0     - true when the name has been banished.
 */
public int
query_banished(string name)
{
    if (!strlen(name))
        return 0;

    /* No need to set the euid. Everyone can see file sizes. */
    name = lower_case(name);
    return (file_size(BANISH_FILE(name)) > 0);
}

/*
 * Nazwa funkcji: banish
 * Opis         : Banish a name, info about name, remove a banishment.
 * Argumenty    : name - the name to perform banish operation on.
 *		  what - what to do.
 * Zwraca       : A list with information.
 */
mixed *
banish(string name, int what)
{
    string file;
    mixed *info;

    info = allocate(2);

    if ((previous_object() != find_object(WIZ_CMD_NORMAL)) &&
        (previous_object() != find_object(WIZ_CMD_APPRENTICE)) &&
        (previous_object() != find_object(WIZ_CMD_ARCH)))
    {
        return ({});
    }

    name = lower_case(name);
    file = BANISH_FILE(name);

    if (file_size(file) > -1)
    {
        info[0] = read_file(file);
        info[1] = file_time(file);
    }

    switch (what)
    {
        case 0: /* Information */
            if (file_size(file) > -1)
                return info;
            break;

        case 1: /* Remove */
            if (file_size(file) > -1)
            {
                rm(file);

                    /* Log the action */
                    log_file("BANISHED", ctime(time()) + " " + capitalize(name)
                        + ": removed by " + capitalize(geteuid(previous_object()))
                        + ".\n", -1);

                return info;
            }
            break;

        case 2: /* Add */
            if (file_size(file) > -1)
                return info;
            write_file(file, geteuid(previous_object()));

            /* Log the action */
            log_file("BANISHED", ctime(time()) + " " + capitalize(name)
                + ": added by " + capitalize(geteuid(previous_object()))
                + ".\n", -1);
            break;

        default: /* Strange */
            break;
    }

    return ({});
}

/*
 * Nazwa funkcji: do_debug
 * Opis         : This function is a front for the efun debug(). You are
 *                only allowed to call debug() through this object because we
 *                need to make some security checks.
 * Argumenty    : string icmd - the debug command.
 *                mixed a1    - a possible argument to debug().
 *                mixed a2    - a possible argument to debug().
 *                mixed a3    - a possible argument to debug().
 * Zwraca       : mixed - the relevant return value for the particular
 *                        debug command.
 */
varargs mixed
do_debug(string icmd, mixed a1, mixed a2, mixed a3)
{
    string euid = geteuid(previous_object());

    /* Some debug() commands are not meant to be called by just anybody. Only
     * 'root' and the administration may call them.
     */
    if (member_array(icmd, DEBUG_RESTRICTED) != -1)
    {
        if (euid != ROOT_UID &&
            previous_object() != this_object() &&
            previous_object() != find_object(SIMUL_EFUN) &&
            query_wiz_rank(euid) < WIZ_ARCH &&
            !query_admin_team_member(euid, "aog") &&
            !query_admin_team_member(euid, "aom"))
        {
            return 0;
        }
    }

    /* In order to get the variables of a certain object, you need to have
     * the proper euid. If you are allowed to write to the file, you are
     * also allowed to view its variables.
     */
    if ((icmd == "get_variables") &&
        (!valid_write(file_name(a1), euid, "get_variables")))
    {
        return 0;
    }

    /* Since debug() returns arrays and mappings by reference, we need to
     * process the value to make it secure, so people cannot alter it.
     */
    return secure_var(debug(icmd, a1, a2, a3));
}

/*
 * Nazwa funkcji:   send_udp_message
 * Opis         :   Sends a udp message to arbitrary host, port
 * Argumenty    :    to_host: Hostname or IP-number
 *		    to_port: Portnumber
 *		    msg:     Message to send
 * Zwraca       :    True if the message is sent. There is of course no
 *		    guarantee it will be received.
 */
int
send_udp_message(string to_host, int to_port, string msg)
{
//    log_syslog("UDP_IO", sprintf("%15s > %s\n", to_host,
//               implode(explode(msg, "\n"), "\\n")));

    if (stringp(udp_manager) &&
        previous_object() == find_object(udp_manager))
    {
        return debug("send_udp", to_host, to_port, msg);
    }
    else
        return 0;
}

/*
 * Nazwa funkcji:  check_memory
 * Opis         :  Checks with 'debug malloc' if it is time to reboot
 * Argumenty    :  dodecay - decay xp or nto.
 *		   The limit can be defined in config.h as MEMORY_LIMIT
 */
public void
check_memory(int dodecay)
{
    int uptime;

    /* Is the game too big? */
    if (query_memory_percentage() >= 100)
        memory_failure();

    uptime = time() - query_start_time();
#ifdef REGULAR_REBOOT
    /* See whether it is time to reboot the game. The game must have been
     * up at least an hour to reboot, though.
     */
    if (uptime > 3600)
    {
        /* We have to add this 3600 because time() starts counting at 01:00. */
        if ((((time() + 3600) % 86400) / 3600) == REGULAR_REBOOT)
        {
            set_auth(this_object(), "root:root");
            ARMAGEDDON->start_shutdown("Ju^z po " + REGULAR_REBOOT +
                ":00, co oznacza, ^ze nadesz^la pora na codzienn^a Apokalips^e!",
                10, ROOT_UID);
        }
    }
#endif REGULAR_REBOOT

#ifdef REGULAR_UPTIME
    if (uptime > irregular_uptime)
    {
        set_auth(this_object(), "root:root");
/*
        ARMAGEDDON->start_shutdown("The game has been up " + CONVTIME(uptime) +
            ", time for a reboot!", 10, ROOT_UID);
*/
        ARMAGEDDON->start_shutdown(CONVTIME(uptime) + " od ostatniej " +
            "Apokalipsy - najwy^zsza pora na nastepn^a!", 10, ROOT_UID);
    }
#endif REGULAR UPTIME

    /* We should add a decay here for the xp stored for each domain. */
    if (dodecay == 1)
        decay_exp();
}

/*
 * Nazwa funkcji:  startup_udp
 * Opis         :  Give the contents of the package to send as startup
 *		   message to the mudlist server. This is default if we
 *		   have no UDP_MANAGER.
 * Zwraca       :  The message.
 */
string
startup_udp()
{
    return
        "||NAME:" + get_mud_name() +
        "||VERSION:" + do_debug("version") +
        "||MUDLIB:" + MUDLIB_VERSION +
        "||HOST:" + query_host_name() +
        "||PORT:" + debug("mud_port") +
        "||PORTUDP:" + debug("udp_port") +
        "||TIME:" + efun::ctime(time());
}

/*
 * Nazwa funkcji:  set_known_muds
 * Opis         :  The UDP manager can set what muds are known.
 * Zwraca       :   True if set.
 */
int
set_known_muds(mapping m)
{
    if (stringp(udp_manager) &&
        previous_object() == find_object(udp_manager))
    {
        known_muds = m;
        save_master();
        return 1;
    }
    return 0;
}

/*
 * Nazwa funkcji:  query_known_muds
 * Opis         :  Give the currently known muds
 * Zwraca       :  A mapping of information indexed with mudnames
 */
mapping
query_known_muds()
{
    if (mappingp(known_muds))
        return secure_var(known_muds);

    return 0;
}

/*
 * Nazwa funkcji: load_admin_teams
 * Opis         : At boot-time, this function loads all admin teams.
 */
void
load_admin_teams()
{
    string *files;
    int    index;

    admin_teams = ([ ]);
    files = get_dir(ALIAS_DIR + "admin") + get_dir(ALIAS_DIR + "mg") + get_dir(ALIAS_DIR + "ao?");
    index = sizeof(files);

    while(--index >= 0)
    {
        admin_teams[files[index]] =
            explode(read_file(ALIAS_DIR + files[index]), "\n");
    }
}

/*
 * Nazwa funkcji: query_admin_team_members
 * Opis         : Returns which wizards are member of an admin team.
 * Argumenty    : string team - the team to check.
 * Zwraca       : string * - array of team members, or 0.
 */
string *
query_admin_team_members(string team)
{
    load_admin_teams();
    if(!mappingp(admin_teams))
        return 0;
    if (!pointerp(admin_teams[team]))
        return 0;

    return ({ }) + admin_teams[team];
}

/*
 * Nazwa funkcji: query_admin_team_member
 * Opis         : Returns which a certain wizard is a member of a certain
 *                admin team.
 * Argumenty    : string name - the member to check.
 *                string team - the team to check.
 * Zwraca       : int 1/0 - member or not.
 */
int
query_admin_team_member(string name, string team)
{
    load_admin_teams();
    return (pointerp(admin_teams[team]) &&
        (member_array(name, admin_teams[team]) > -1));
}

/*
 * Nazwa funkcji: query_prevent_shadow
 * Opis         : This function will prevent shadowing of this object.
 * Zwraca       : int 1 - always.
 */
nomask int
query_prevent_shadow()
{
    return 1;
}