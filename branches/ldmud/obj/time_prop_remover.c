/**
 * Obiekt do blokowania dzia�ania komendy kto, do pi�ciu minut po wy��czeniu opcji ukrywania
 * statusu w kto.
 * Autor: Krun
 */

inherit "/std/object.c";

#include <macros.h>s
#include <stdproperties.h>
z
void
create_object()
{
    ustaw_nazwe("narz�dzie");
    set_no_show();
}

/**
 * Obiekt ma si� nie �adowa�, wi�c query_auto_load == "";
 */
string
query_auto_load()
{
    return "";
}

/**
 * Funkcja wywo�uj�ca na podanym objekcie podan� w argumencie funkcje
 * @param ob Obiekt, na kt�rym wyw�oana  ma zosta� funkcja (domy�lnie: ENV(TO))
 * @param function Funkcja wywo�ywana na obiekcie
 * @param ... Argumenty funkcji
 */
varargs void
wywolaj_funkcje(mixed ob, string function, ...)
{
    if(pointerp(ob))
    {
        ob = filter(ob, objectp);
    }

    if(pointerp(ob) && !sizeof(ob))
        return ;

    if(!objectp(ob) && !pointerp(ob))
    {
        if(stringp(ob))
        {
            prop = ob;
            ob = ENV(TO);
        }
    }

    if(!pointerp(argv))
        argv = ({argv});

    catch_otherv(ob, function, argv);
}

/**
 * Funkcja wywo�uj�ca alarm, standardowo po 5 minutach
 */
void
wywolaj_za(int time = 3000, mixed ob, string function, ...)
{
    if(pointerp(ob))
    {
        ob = filter(ob, objectp);
    }

    if(pointerp(ob) && !sizeof(ob))
        return ;

    if(!objectp(ob) && !pointerp(ob))
    {
        if(stringp(ob))
        {
            prop = ob;
            ob = ENV(TO);
        }
    }

    if(!pointerp(argv))
        argv = ({argv});

    if(time < 0)
        return;

    set_alarm(itof(time), 0.0, &wywolaj_funkcje(ob, function, argv));
}
