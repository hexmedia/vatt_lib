/**
 * Obiekt do blokowania dzia�ania komendy kto, do pi�ciu minut po wy��czeniu opcji ukrywania
 * statusu w kto.
 * Autor: Krun
 */

inherit "/std/object.c";

#include <macros.h>s
#include <stdproperties.h>

void
create_object()
{
    set_name("function changer");
    set_no_show();
}

/**
 * Obiekt ma si� nie �adowa�, wi�c query_auto_load == "";
 */
string
query_auto_load()
{
    return "";
}

/**
 * Funkcja wywo�uj�ca na podanym objekcie podan� w argumencie funkcje
 * @param ob Obiekt, na kt�rym wyw�oana  ma zosta� funkcja (domy�lnie: ENV(TO)) (opt)
 * @param funkcja Funkcja wywo�ywana na obiekcie
 * @param argv Tablica z argumentami funkcji (opt)
 */
varargs void
wywolaj_funkcje(mixed ob, mixed funkcja, mixed argv)
{
    if(pointerp(ob))
    {
        ob = filter(ob, objectp);
    }

    if(pointerp(ob) && !sizeof(ob))
        return ;

    if(!objectp(ob) && !pointerp(ob))
    {
        if(stringp(ob))
        {
            argv = ({funkcja}) + argv; 
            funkcja = ob;
            ob = ENV(TO);
        }
    }
    else if(!stringp(funkcja))
        return ;

    if(!pointerp(argv))
        argv = ({argv});

    call_otherv(ob, funkcja, argv);

    if(!sizeof(get_all_alarms()))
        remove_object();
}

/**
 * Funkcja wywo�uj�ca alarm, standardowo po 5 minutach
 * @param time czas za jaki ma zosta� wywo�ana funkcja (domy�lnie 5 min) (opt)
 * @param ob objekt lub objekty na kt�rych funkcja ma zosta� wywo�ana (domy�lnie ENV(TO)) (opt)
 * @param funkcja Funkcja kt�ra ma zosta� wywo�ana
 * @param ... Argumetny funkcji (opt)
 */
varargs void
wywolaj_za(mixed time, mixed ob, mixed funkcja, ...)
{
    if(!intp(time))
    {
        if(pointerp(argv))
            argv = ({funkcja}) + argv;
        else
            argv = ({funkcja});

        funkcja = ob;
        ob = time;
        time = 3000;
    }

    if(pointerp(ob))
    {
        ob = filter(ob, objectp);
    }

    if(pointerp(ob) && !sizeof(ob))
        return ;

    if(!objectp(ob) && !pointerp(ob))
    {
        if(stringp(ob))
        {
            argv = ({funkcja}) + argv; 
            funkcja = ob;
            ob = ENV(TO);
        }
    }
    else if(!stringp(funkcja))
        return ;

    if(!pointerp(argv))
        argv = ({argv});

    if(!pointerp(argv))
        argv = ({argv});

    if(time < 0)
        return;

    set_alarm(itof(time), 0.0, &wywolaj_funkcje(ob, funkcja, argv));
}
