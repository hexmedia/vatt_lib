/*

short: karbowane owalne liscie. // pisze li�cie, a nie korzen czy cos, bo w tym ziolku
                                 // sie zbiera wlasnie liscie - reszta jest nieprzydatna,
                                // a calosc osadzona na klaczu (trzy liscie)


//opis dla laikow (short rzecz jasna taki sam)

long: Dlugie i podluzne liscie, ktorych brzegi sa karbowane, nie wygladaja na pospolita 
rosline i trudno dostrzec w niej jakies znane ziolo.

//dla znawcow

long: Dlugie i owalne liscie, ktorych brzegi sa karbowane, nie wygladaja na pospolita 
rosline, ale po dluzszym przyjrzeniu sie rozpoznajesz w niej bobrka. Ziolo to najczesciej
mozna spotkac na bagnach i podmoklych lakach badz w okolicach stawow gdzie rosnie w malych
skupiskach. Liscie znane sa ze swoich leczniczych wlasciwosci lecz zazwyczaj stosuje
sie je w mieszankach ziolowych. 

>zjedz lisc
Powoli przezuwasz karbowane owalne liscie i czujesz w ustach gorzki smak.


//po (nie wiem)  10-20 sekundach
Czujesz sie bardziej glodny/a.

Jak bedzie juz cala alchemia to z tego ziola w polaczeniu z szalwia, jalowcem, liscmi
poziomki i centuria mozna zrobic napar/masc czy co tam jesZcze - ktore usmierzaja bole,
pobudzaja apetyt i inne. Moze tez poprawiac samopoczucie.

*/

inherit "/std/herb";

#include <herb.h>

void
create_herb()
{
    ustaw_nazwe("li��");

    dodaj_przym("karbowany", "karbowani");
    dodaj_przym("owalny", "owalni");

    ustaw_nazwe_ziola("bobrek");

    set_id_long("D�ugie i owalne li�cie, kt�rych brzegi s� karbowane, " +
	"nie wygl�daj� na pospolit� ro�lin�, ale po d�u�szym przyjrzeniu " +
	"si� rozpoznajesz w niej bobrka. Zio�o to najcz�ciej mo�na " +
	"spotka� na bagnach i podmok�ych ��kach b�d� w okolicach staw�w, " +
	"gdzie ro�nie w ma�ych skupiskach. Li�cie znane s� ze swoich " +
	"leczniczych w�a�ciwo�ci, lecz zazwyczaj stosuje si� je w " +
	"mieszankach zio�owych.\n");
    set_unid_long("D�ugie i owalne li�cie, kt�rych brzegi s� karbowane, " +
	"nie wygl�daj� na pospolit� ro�lin� i trudno dostrzec w niej " +
	"jakie� znane zio�o.\n");

    set_effect(HERB_SPECIAL, 0, 0);

    set_amount(20);
    set_decay_time(700);
    set_id_diff(30);
    set_herb_value(100);
}

void
special_effect()
{
    set_alarm(15.0, 0.0, "glodnienie", this_player());
//    set_alarm(10.0, 0.0, &(this_player())->catch_msg("Czujesz si� " +
//	"bardziej g�odn" + this_player()->koncowka("y", "a", "e") + ".\n"));
//    set_alarm(10.0, 0.0, &(this_player())->add_stuffed(-50));
}

void
glodnienie(object kto)
{
    kto->catch_msg("Czujesz si� " +
	"bardziej g�odn" + kto->koncowka("y", "a", "e") + ".\n");
    kto->add_stuffed(-50);
}
