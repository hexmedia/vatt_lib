/**
 * \file /std/coins.c
 *
 * This is the heap object for coins.
 */

#pragma save_binary
#pragma strict_types
#pragma no_inherit

inherit "/std/heap";

#include <object_types.h>
#include <macros.h>
#include <money.h>
#include <language.h>
#include <stdproperties.h>
#include <ss_types.h>
#include <std.h>
#include <debug.h>
#include <colors.h> /* FIXME: DELETE */

/*
 * Global variable. It holds the coin type.
 */
static string coin_type;

/*
 * Prototype.
 */
void set_coin_type(string str);

/*
 * Function name: create_coins
 * Description  : Called at creation of the coins. To create your own coins
 *                you must define this function.
 */
public void
create_coins()
{
    ustaw_nazwe("moneta");

    set_heap_size(1);
}

/*
 * Function name: create_heap
 * Description  : Constructor. This will create the heap and set some stuff
 *                that we want. You may not mask this function. You have to
 *                use create_coins() to create your own coins.
 */
public nomask void
create_heap()
{
    add_prop(OBJ_M_NO_SELL, 1);
    set_type(O_MONETY);

    create_coins();
}

/*
 * Function name: reset_coins
 * Description  : In order to have some code executed when this heap of
 *                coins resets, mask this function. Notice that in order
 *                to make them reset, call enable_reset() from the function
 *                create_coins().
 */
public void
reset_coins()
{
}

/*
 * Function name: reset_heap
 * Description  : Called to make this heap reset. You may not mask this
 *                function, so use reset_coins() instead.
 */
public nomask void
reset_heap()
{
    reset_coins();
}

/**
 * Zwraca stringa dla odzyskania rodzaju monet.
 *
 * @return cz�� stringa do zapisu
 */
string
query_coins_auto_load()
{
    return "#coins#" + coin_type + "#";
}

/*
 * Function name: query_auto_load
 * Description  : Coins are autoloading. This function is called to find
 *                out whether they are. It returns the coin type and the
 *                number of coins in this heap.
 * Returns      : string - the auto-load string.
 */
public string
query_auto_load()
{
    return ::query_auto_load() + query_coins_auto_load();
}

/**
 * Inicjalizuje typ monet.
 *
 * @param arg - odczytany string pasuj�cy do tego zwracanego przez
 * <b>query_coins_auto_load()</b>
 *
 * @return pozosta�a cz�� napisu.
 */
string
init_coins_arg(string arg)
{
    string foobar, ct, rest;

    if (arg == 0)
        return 0;

    if (sscanf(arg, "%s#coins#%s#%s", foobar, ct, rest) == 3)
    {
        set_coin_type(ct);
        return rest;
    }

    return arg;
}

/*
 * Function name: init_arg
 * Description  : Called when autoloading. It will set the type of coins
 *                and the number of coins in the heap.
 * Arguments    : string arg - the auto-load argument.
 */
string
init_arg(string arg)
{
    return init_coins_arg(::init_arg(arg));
}

/*
 * Function name: short
 * Description  : This function is called to get the short description of
 *                these coins. We make it dependant on the intelligence of
 *                the onlooker and have special cases for different numbers
 *                of coins.
 * Arguments    : object for_object - the object that wants to know.
 * Returns      : string - the short string.
 */
public varargs string
short(mixed for_object, mixed przyp)
{
    int typ = member_array(coin_type, MONEY_TYPES), num = num_heap();

    string str;

    /* No elements in the heap == no show. */
    if (num < 1)
        return 0;

    /* No identifier: BAD coins. Remove them. */
    if (!strlen(query_prop(HEAP_S_UNIQUE_ID)))
    {
        set_alarm(0.1, 0.0, remove_object);
        find_player("krun")->catch_msg(SET_COLOR(COLOR_FG_RED) + "MONEY_REPORT: WYWALAM SI� NA HEAP_S_UNIQUE_ID!\n" + CLEAR_COLOR);
        return "ghost coins";
    }

    /* No onlooker, default to this_player(). */
    if (!objectp(for_object))
    {
        if (intp(for_object))
            przyp = for_object;
        else if (stringp(for_object))
            przyp = atoi(for_object);

        for_object = this_player();
    }
    else
        if (stringp(przyp))
            przyp = atoi(przyp);

    /* One coin, singular, not really a heap. */
    if (num < 2)
        return query_nazwa(przyp);

    /* Less than a dozen, we see the number as a word. */
    if (num < 12)
    {
        return LANG_SNUM(num, przyp, MONEY_NAMES[typ][2]) + " " +
            ilosc(num, "", MONEY_NAMES[typ][1][PL_MIA], MONEY_NAMES[typ][1][PL_DOP]);
    }

    /* If we are smart enough, we can see the number of coins. */
    if (for_object->query_stat(SS_INT) / 2 > num)
    {
        return num + " " + ilosc(num, "", MONEY_NAMES[typ][1][PL_MIA], MONEY_NAMES[typ][1][PL_DOP]);
    }

    /* Else, default to 'many' or to a 'huge heap'. */
    if (num < 1000)
    {
        switch(przyp)
        {
            case PL_MIA: str = "wiele "; break;
            case PL_DOP: str = "wielu "; break;
            case PL_CEL: str = "wielu "; break;
            case PL_BIE: str = "wiele "; break;
            case PL_NAR: str = "wieloma "; break;
            case PL_MIE: str = "wielu "; break;
        }

        return str + query_pnazwa(PL_DOP);
    }
    else
    {
        switch(przyp)
        {
            case PL_MIA: str = "ogromny stos "; break;
            case PL_DOP: str = "ogromnego stosu "; break;
            case PL_CEL: str = "ogromnemu stosowi "; break;
            case PL_BIE: str = "ogromny stos "; break;
            case PL_NAR: str = "ogromnym stosem "; break;
            case PL_MIE: str = "ogromnym stosie "; break;
        }

        return str + query_pnazwa(PL_DOP);
    }

    return 0;
}

/*
 * Function name: long
 * Description  : This function will slip the short description into the
 *                long description. Money will always look like good
 *                money, but don't try to fool the shopkeepers with wooden
 *                coins ;-)
 * Returns      : string - the long description.
 */
varargs public mixed
long()
{
    int typ;
    string material="",material2="";

    switch(query_name())
    {
        case "grosz": material="miedziana"; material2="miedziane"; break;
        case "denar": material="srebrna"; material2="srebrne"; break;
        case "korona": material="z�ota"; material2="z�ote"; break;
        default: material="dziwna"; material2="dziwne"; break;
    }

    if (num_heap() < 2)
    {
        return "Ta niewielka "+material+" moneta to "+short(PL_MIA)+". "+
               "Na jej rewersie wybito reda�skiego or�a z rozpostartymi "+
               "ku niebiosom skrzyd�ami. Wygl�da na prawdziw"+
               koncowka("y", "^a", "e")+".\n";
    }
    else
    {
        typ = member_array(coin_type, MONEY_TYPES);

        return "Te niewielkie " +material2+" monety to "+query_pnazwa(PL_MIA)+". Na ich rewersach "+
            "wybito reda�skiego or�a z rozpostartymi ku niebiosom skrzyd�ami"+
            ". Wygl^adaj^a na prawdziwe.\n";
    }
}

/*
 * Function name: set_coin_type
 * Description  : Set the type of coins we have here. Update all necessary
 *                properties with respect to the coins.
 * Arguments    : string str - the coin type to set.
 */
public void
set_coin_type(string str)
{
    int x, ix, old_ix;
    string log, nome, czas, *wywolania;
    object env = environment();

    /* If this is one of the default coin types, set the weight, volume
     * and value correctly.
     */
    ix = member_array(str, MONEY_TYPES);

    if (ix < 0)
        str = MONEY_TYPES[ix = 0];

    mark_state();
    add_prop(HEAP_I_UNIT_VALUE, MONEY_VALUES[ix]);
    add_prop(HEAP_I_UNIT_WEIGHT, MONEY_WEIGHT[ix]);
    add_prop(HEAP_I_UNIT_VOLUME, MONEY_VOLUME[ix]);
    update_state();

    /* If there is a coin-type, remove that coin type as adjective. */
    if (coin_type)
    {
        old_ix = member_array(coin_type, MONEY_TYPES);
        remove_name("_" + coin_type + " moneta_");
    }

    if (env && (!interactive(env) || !SECURITY->query_wiz_rank(env->query_real_name())))
    {
        czas = ctime(time())[4..];
        log = sprintf("%s   sct %4d %s->%s w ", czas[14..], num_heap(),
            coin_type, str);

        if (interactive(env))
            log += capitalize(env->query_real_name());
        else
            log += file_name(env);

        wywolania = DEBUG_POBJS(0);
        x = -1;
        while (++x < sizeof(wywolania))
            wywolania = wywolania[0..x] + (wywolania[(x+1)..] - wywolania[x..x]);

        log += " (";
        if (this_interactive())
            log += getuid(this_interactive()) + "; ";
        log += implode(wywolania, "->") + "); ";
        czas = czas[0..11];
        x = -1;
        while (czas[++x] == ' ')
            ;
        czas = czas[x..];
        log += czas;

        SECURITY->log_syslog("MONEY_LOG", (log + "\n"), MONEY_LOG_SIZE);
    }

    /* Set the new coin type and set it as an adjective. Also, we update
     * our identifier.
     */
    coin_type = str;
    add_prop(HEAP_S_UNIQUE_ID, MONEY_UNIQUE_NAME(coin_type));
    add_name("_" + str + " moneta_");
    ustaw_nazwe(MONEY_NAMES[ix][0], MONEY_NAMES[ix][1], MONEY_NAMES[ix][2]);
//    dodaj_przym(MONEY_ODM[ix][0], MONEY_ODM[ix][1]);
}

public void
set_heap_size(int num)
{
    string str, typ, czas, *wywolania;
    object env = environment(this_object());
    int x, old_size = num_heap();

    ::set_heap_size(num);

    if (!env || ((num - old_size) < MONEY_LOG_LIMIT[coin_type]) || (MASTER_OB(previous_object()) == COINS_OBJECT) ||
        (interactive(env) && SECURITY->query_wiz_rank(env->query_real_name())))
    {
        return;
    }

    czas = ctime(time())[4..];
    switch(coin_type)
    {
        case "srebro":  typ = "sr"; break;
        case "z^loto":   typ = "zl"; break;
        case "mithryl": typ = "MT"; break;
        default:	    typ = "md";
    }

    str = sprintf("%s   shs %4d->%-4d %2s w ", czas[14..], old_size, num, typ);

    if (interactive(env))
        str += capitalize(env->query_real_name());
    else
        str += file_name(env);

    wywolania = DEBUG_POBJS(0);
    x = -1;

    while (++x < sizeof(wywolania))
        wywolania = wywolania[0..x] + (wywolania[(x+1)..] - wywolania[x..x]);

    str += " (";
    if (this_interactive())
        str += getuid(this_interactive()) + "; ";

    str += implode(wywolania, "->") + "); ";

    czas = czas[0..11];
    x = -1;

    while (czas[++x] == ' ');

    czas = czas[x..];
    str += czas;

    SECURITY->log_syslog("MONEY_LOG", (str + "\n"), MONEY_LOG_SIZE);
}

/*
 * Function name: query_coin_type
 * Description  : Return what type of coins we have.
 * Returns      : string - the coin type.
 */
public string
query_coin_type()
{
    return coin_type;
}

/*
 * Function name: config_split
 * Description  : When a part of this heap is split, we make sure the new
 *                heap is made into the correct type of coins as well by
 *                setting the coin type to the coin type of the heap we are
 *                being split from.
 * Arguments    : int new_num - the number of coins in this new heap.
 *                object orig - the heap we are split from.
 */
public void
config_split(int new_num, object orig)
{
    ::config_split(new_num, orig);

    set_coin_type(orig->query_coin_type());
}

public string
appraise_value(int num)
{
    int value, skill, seed;
    value = cut_sig_fig(appraise_number(num) * query_prop(HEAP_I_UNIT_VALUE), 2);

    return value + " grosz" + ilosc(value, "", "e", "y");
}

public int
appraise_number(int num)
{
    int value = num_heap();

    if (value < max(12, this_player()->query_stat(SS_INT) / 2))
        return value;
    else
        return ::appraise_number(num);
}

public void
appraise_object(int num)
{
    string str;
    int ile = appraise_number(num);

    write(this_object()->long(0, this_player()) + "\n");
    write("Oceniasz, ^ze " + short(PL_MIA) + " wa^z" +
        ilosc(ile, "y", "�", "y") + " " + appraise_weight(num) +
        ", za^s " + query_zaimek(PL_DOP, 0, (num_heap() > 1)) +
        " obj^eto^s^c wynosi " + appraise_volume(num) + ".\n");

    write("Wydaje ci si^e, ^ze " + (ile == 1 ? "jest 1 sztuka warta " :
        (ile%10 <= 4 && ile%10 >= 2 && ile%100 != 1 && (ile%100)/10 != 1 ?
        "s^a " + ile + " sztuki warte " : "jest " + ile + " sztuk wartych ")) +
        appraise_value(num) + ".\n\n");
}


/*
 * Function name: stat_object
 * Description  : When a wizard stats this heap of coins, we add the coin
 *                type to the information.
 * Returns      : string - the stat-description.
 */
public string
stat_object()
{
    return ::stat_object() + "Typ monety: " + coin_type + "\n";
}

/*
 * Function name: move
 * Description  : Make sure moving of money is logged if the amount is
 *                larger than a certain amount.
 * Arguments    : see move in /std/object.c
 * Returns      : see move in /std/object.c
 */
varargs nomask public int
move(mixed dest, mixed subloc, int force = 0, int test_only = 0)
{
    string str, str2, czas, importance, typ, *wywolania;
    object env = environment(), prev = previous_object();
    int rval = ::move(dest, subloc, force, test_only);
    int x;

    /* If there was an error moving or if the limit is not high enough, do
     * not log.
     */
    if (rval || ((num_heap() < MONEY_LOG_LIMIT[coin_type] && !SECURITY->query_wiz_rank(env->query_real_name()))))
        return rval;

    if (MASTER_OB(prev) == COINS_OBJECT) // split_heap(), itp.
        return 0;

    if (stringp(dest))
        dest = find_object(dest);

    /* No destination means coins are destructed. That is not interesting.
     * If the coins enter a wizard that is not interesting either.
     */
    if (!objectp(dest) || (interactive(dest) && SECURITY->query_wiz_rank(dest->query_real_name())))
        return 0;

    czas = ctime(time())[4..];
    switch(coin_type)
    {
        case "srebro":  x = 12; typ = "sr"; break;
        case "z^loto":   x = 240; typ = "zl"; break;
        case "mithryl": x = 24000; typ = "MT"; break;
        default: x = 1; typ = "md";
    }
    x *= num_heap();

    if (x >= 24000)
        importance = "****";
    else if (x >= 3600)
        importance = " ++ ";
    else
        importance = "    ";

    str = czas[14..];
    str2 = sprintf("%4d %2s %4s ", num_heap(), typ, importance);

    if ((interactive(dest)) &&
        (calling_function() == "load_auto_obj") &&
        (previous_object() == dest))
    {
        str += " login " + str2 + capitalize(dest->query_real_name());
        str2 = "";
    }
    else
    {
        str += "  move " + str2;
        if (objectp(env))
        {
            if (interactive(env))
            {
                if (SECURITY->query_wiz_rank(env->query_real_name()))
                    str += upper_case(env->query_real_name());
                else
                    str += capitalize(env->query_real_name());
            }
            else
                str += file_name(env);

            str2 = "";
        }
        else
        {
            str += "void";
            wywolania = DEBUG_POBJS(0);
            x = -1;
            while (++x < sizeof(wywolania))
            {
                wywolania = wywolania[0..x] +
                    (wywolania[(x+1)..] - wywolania[x..x]);
            }

            str2 = " (" + implode(wywolania, "->") + ")";
        }

        str += " -> " + (interactive(dest) ?
            capitalize(dest->query_real_name()) : file_name(dest));
    }

    czas = czas[0..11];
    x = -1;
    while (czas[++x] == ' ')
        ;

    czas = czas[x..];

    /* Log the transation. */
    SECURITY->log_syslog("MONEY_LOG", (str + str2 + "; " + czas + "\n"), MONEY_LOG_SIZE);

    return 0;
}

/*============================================================================
 *Emoty - podrzucanie i gryzienie. Vera
 */

int
podrzuc(string str)
{
    object moneta;
    string co_wypadlo;

    notify_fail("Co chcesz podrzuci�?\n");

    if (!str)
        return 0;

    if(str=="monety" || str=="grosze" || str=="korony" || str=="denary")
        return 0; //Co by si� nie bawi� zbytnio w jakie� split_heapy etc.

    if (!parse_command(str, TP, "%o:" + PL_BIE, moneta))
        return 0;

    if (!moneta) return 0;

    if (moneta != TO)
        return 0;

    if(!HAS_FREE_HAND(TP))
    {
        write("Musisz mie� jedn� d�o� woln�, aby to zrobi�.\n");
        return 1;
    }

    string kon, kon2,kon3="";
    switch(moneta->query_name())
    {
        case "grosz": kon="go"; kon2="go"; break;
        case "denar": kon="go"; kon2="go"; break;
        case "korona": kon="j�"; kon2="jej"; kon3="a"; break;
        default: kon="j�"; kon2="jej"; kon3="a"; break;
    }

    switch(random(2))
    {
        case 0: co_wypadlo="Wypad�a reszka."; break;
        case 1: co_wypadlo="Wypad� orze�."; break;
    }

    //ma�y te�cik na zr�czno��
    if(TP->query_stat(SS_DEX) <= (25+random(25)))
    {
        moneta->split_heap(1);
        moneta->move(ENV(TP));
        write("Podrzucasz wysoko "+moneta->short(PL_BIE)+", lecz "+
              "nie uda�o ci si� "+kon2+" z�apa� gdy spada�"+
              kon3+". "+co_wypadlo+"\n");
        saybb(QCIMIE(TP,PL_MIA)+" podrzuca wysoko "+moneta->short(PL_BIE)+
              ", lecz nie uda�o "+TP->koncowka("mu","jej","temu")+
              " si� "+kon2+" z�apa� gdy spada�"+kon3+". "+co_wypadlo+"\n");
        return 1;
    }

    moneta->split_heap(1);
    write("Podrzucasz wysoko "+moneta->short(PL_BIE)+" i �apiesz "+
          kon+" w locie, po czym szybko przek�adasz na d�o�. "+co_wypadlo+"\n");
    saybb(QCIMIE(TP,PL_MIA)+" podrzuca wysoko "+moneta->short(PL_BIE)+
              " i �apie "+kon+
              " w locie, po czym szybko przek�ada na d�o�. "+co_wypadlo+"\n");
    return 1;
}

int
ugryz(string str)
{
    object moneta;
    notify_fail("Co chcesz ugry��?\n");

    if (!str) return 0;

    if(str=="monety" || str=="grosze" || str=="korony" || str=="denary")
        return 0; //Co by si� nie bawi� zbytnio w jakie� split_heapy etc.

    if (!parse_command(str, TP, "%o:" + PL_BIE, moneta))
        return 0;

    if (!moneta) return 0;

    if (moneta != TO)
        return 0;

    if(!HAS_FREE_HAND(TP))
    {
        write("Musisz mie� jedn� d�o� woln�, aby to zrobi�.\n");
        return 1;
    }

    string kon2;
    switch(moneta->query_name())
    {
        case "grosz": kon2="jego"; break;
        case "denar": kon2="jego"; break;
        case "korona": kon2="jej"; break;
        default: kon2="jej"; break;
    }

    moneta->split_heap(1);
    write("Gryziesz mocno "+moneta->short(PL_BIE)+", staraj�c si� "+
          "sprawdzi� "+kon2+" autentyczno��.\n"+
          "Po b�lu w z�bach wnioskujesz, �e moneta ta jest prawdziwa.\n");
    saybb(QCIMIE(TP,PL_MIA)+" gryzie mocno "+moneta->short(PL_BIE)+
              ", staraj�c si� sprawdzi� "+kon2+" autentyczno��.\n");
    return 1;

}

public int
pomoc(string str)
{
    object moneta;

    notify_fail("Nie ma pomocy na ten temat.\n");

    if (!str) return 0;

    if (!parse_command(str, TP, "%o:" + PL_MIA, moneta))
        return 0;

    if (!moneta) return 0;

    if (moneta != TO)
        return 0;

    write("By sprawdzi� ich autentyczno�� spr�buj je ugry��. Mo�esz je tak�e "+
          "popodrzuca� z osobna, by roztrzygn�� jakie� spory.\n");
    return 1;
}

nomask void
init()
{
    ::init();
    add_action(podrzuc,"podrzu�");
    add_action(ugryz,"ugry�");
    add_action("pomoc", "?", 2);
}

/**
 * Funckcja identyfikuj�ca obiekt jako monete.
 *
 * @return 1 zawsze
 */
public int is_coin() { return 1; }

/**funkcja nadpisuj�ca query_orig_place.
 * monety s� w�asno�ci� pa�stwa, a nie prywatn�. ;)
 */
public string *
query_orig_place()
{
    return ({ });
}
