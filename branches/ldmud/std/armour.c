/**
 * \file /std/armour.c
 *
 * Plik ten zawiera funkcje zwi�zane ze zbrojami r�nego typu.
 */

#pragma save_binary
#pragma strict_types

inherit "/std/container.c";
inherit "/lib/wearable_item.c";
inherit "/lib/condition.c";

#include <pl.h>
#include <macros.h>
#include <wa_types.h>
#include <rozmiary.h>
#include <formulas.h>
#include <cmdparse.h>
#include <composite.h>
#include <object_types.h>
#include <stdproperties.h>
#include "/config/login/login.h"

string arm_condition_desc();
void update_prop_settings();
varargs void remove_broken(int silent = 0);
public nomask int *query_protects();

static int      old,
                max_value,          /* Warto�� pancerza, kiedy nie jest zu�yty jeszcze */
                hits,               /* Ilo�� uderze�, jakie mo�e przyj�� pancerz bez pogorszenia stanu */
                repair;             /* Jak du�o zosta�o naprawione w pancerzu */

static string   gFail;              /* Informacja o b��dzie kiedy pancerz jest noszony */
static mapping  ac_table;           /* Roz�o�enie ac na ro�nych hitlokacjach */

public string *
parse_command_adjectiv1_id_list()
{
    if (this_object()->query_worn())
        return ::parse_command_adjectiv1_id_list() + ({ "za�o�ony" });
    else
        return ::parse_command_adjectiv1_id_list() + ({ "nieza�o�ony" });
}

public string *
parse_command_adjectiv2_id_list()
{
    if (this_object()->query_worn())
        return ::parse_command_adjectiv2_id_list() + ({ "za�o�eni" });
    else
        return ::parse_command_adjectiv2_id_list() + ({ "nieza�o�eni" });
}


/**
 * W celu stworzenia pancerza powinno si� przedefiniowa� t� funkcj�. Zazwyczaj
 * korzysta si� z tej funkcji by ustawi� nazw� i opis, typ pancerza, klas�
 * pancerza, etc.
 */
void
create_armour()
{
}

/**
 * Funkcja ta jest wywo�ywana by stworzy� pancerz. Ustawia ona cz��
 * standardowych ustawie� i wywo�uje <b>create_armour</b>. Musisz
 * przedefiniowa� <b>create_armour</b>, poniewa� <b>create_object</b>
 * ma atrybut <i>nomask</i>, czyli nie mo�esz jej przedefiniowa�.
 */
public nomask void
create_container()
{
    ::create_container();
    add_prop(CONT_I_DONT_SHOW_CONTENTS, 1);
    add_prop(CONT_I_CANT_WLOZ_POD, 1);
    add_prop(CONT_I_CANT_WLOZ_DO, 1);
    add_prop(CONT_I_CANT_ODLOZ, 1);
    add_prop(CONT_I_CANT_POLOZ, 1);

    ac_table = ([]);

    worn = 0;
    add_prop(OBJ_I_VALUE, "@@query_value");
    add_prop(OBJ_I_VOLUME, 1000);
    add_prop(OBJ_I_WEIGHT, 500);

    set_type(O_ZBROJE);

    create_armour();

    if(query_type() == O_ZBROJE)
        dodaj_nazwy("zbroja");

    if(query_type() == O_UBRANIA)
        dodaj_nazwy("ubranie");

    update_prop_settings();

    //tzn. ze mamy do czynienia z ubraniem, skoro nic nie chroni.
    //A ubranie powinno sie latwiej niszczyc. V.

    //A g�wno prawda. Ubrania te� chroni�.  [Krun]
    if(!sizeof(query_protects()))
    {
        set_likely_break(9);
        set_likely_cond(20);
    }
    else
    {
        //balansik by verek. Ponadto - spojrz kilka linijek nizej w tej funkcji.
        //to jest dla zbroi, do ubran - spojrz kilka linijek nizej ;p
        set_likely_break(4);
        set_likely_cond(10);
    }
}

/**
 * D�ugi opis. Dodajemy informacj� o stanie pancerza do niego. Funkcja ta tylko
 * zwraca opis, nie wypisuje go na ekranie.
 *
 * @param str opis pseudo-rzeczy albo obiektu.
 * @param for_obj obiekt, dla kt�rego jest opis.
 *
 * @return d�ugi opis.
 */
public varargs string
long(string str, object for_obj)
{
    return ::long(str, for_obj) + (str ? "" : arm_condition_desc());
}

/**
 * Funkcja wywo�ywana kiedy pancerz opuszcza jakie� �rodowisko.
 *
 * @param from Opuszczane �rodowisko.
 * @param to Nowe �rodowisko.
 */
void
leave_env(object from, object to, string fsubloc)
{
    wearable_item_leave_env(from, to);

    ::leave_env(from, to, fsubloc);
}

/**
 * Funkcja wywo�ywana kiedy pancerz wchodzi do nowego �rodowiska.
 *
 * @param dest Nowe �rodowisko
 * @param old Stare �rodowisko.
 */
void
enter_env(object dest, object old)
{
    wearable_item_enter_env(dest, old);

    ::wearable_item_enter_env(dest, old);
}

/**
 * Funkcja sprawdzaj�ca czy gracz dojrzy ten obiekt.
 *
 * @param for_obj Obiekt dla kt�rego jest sprawdzane.
 */
int
check_seen(object for_obj)
{
    if(!(::check_seen(for_obj)))
        return 0;

    return wearable_item_check_seen(for_obj);
}
/**
 * Ustawia klas� pancerza zbroi. Przy jej podawaniu precyzuje si�
 * klas� pancerza osobno dla ka�dej hitlokacji, kt�r� zbroja chroni.
 * Klas� pancerza charakteryzuj� trzy liczby - klasy pancerza na
 * trzy rodzaje obra�e�.
 *
 * <b>set_ac()</b> przyjmuje dowoln� ilo�� argument�w, kt�ra jest krotno�ci�
 * 4. Ka�da czw�rka opisuje klas� pancerza na jednej hitlokacji (zdefiniowane
 * w <b>/sys/wa_types.h</b>).
 *
 * @param hid identyfikator hitlokacji (A_HEAD, itp)
 * @param ac_klute wyparowania na rany k�ute
 * @param ac_ciete wyparowania na rany ciete
 * @param ac_obuch wyparowania na rany obuchowe
 */
void
set_ac(int hid, int ac_klute = -1, int ac_ciete = 0, int ac_obuch = 0, ...)
{
    int size, ix, il, ok;

    if(query_lock())
        return;

    if(ac_klute == -1)
    {
        old = hid;
        return;
    }

    size = sizeof(argv);
    if((size % 4) != 0)
    {
        throw("/std/armour.c - z�a liczba argument�w do set_ac.\n");
        return;
    }

    argv = ({ hid, ac_klute, ac_ciete, ac_obuch }) + argv;
    ok = (!wear_at && !slot_side_bits);

    ix = -4;
    while((ix += 4) <= size)
    {
        if(argv[ix] < 0)
        {
            argv[ix] *= -1;
            ac_side_bits |= argv[ix];

            if(ok)
                slot_side_bits |= argv[ix];

            argv[ix] = (argv[ix] | (argv[ix] << 1));
        }
        else
        {
            ac_slots |= argv[ix];

            if(ok)
                wear_at |= argv[ix];
        }

        for(il = TS_HEAD; il <= argv[ix]; il <<= 1)
            if(il & argv[ix])
                ac_table[il] = ({ argv[ix + 1], argv[ix + 2], argv[ix + 3] });
    }
}

/**
 * Zwraca liczb� slot�w sk�adaj�cych si� na dan� hitlokacj�.
 *
 * @param hid hitlokacja, o kt�rej wielko�� pytamy.
 *
 * @return liczba slot�w sk�adaj�cych si� na dan� hitlokacj�.
 */
int
wielkosc_hitlokacji(int hid)
{
    int i, tmp;

    tmp = 0;
    for (i = 1; i <= hid; i <<= 1)
        if (hid & i)
            ++tmp;

    return tmp;
}


/**
 * Podlicza warto�� klasy pancerza chroni�cego dan� sublokacj�.
 *
 * @param hid - hitlokacja, o kt�rej pancerz pytamy
 * @param dtype - typ obra�e�
 *
 * @return warto�� klasy pancerza chroni�cego dan� sublokacj�.
 */

int
podlicz_ac(int hid = 0, int dtype = 0)
{
    int pancerz = 0;
    int i, il, size;
    int* klucze = (int*)m_indices(ac_table);

    il = sizeof(klucze);
    size = wielkosc_hitlokacji(hid);

    for (i = 0; i < il; ++i)
        if (hid & klucze[i])
            pancerz += ac_table[klucze[i]][dtype];

    if (size > 0)
        pancerz /= size;

    return pancerz;
}

/**
 * Zwraca warto�� klasy pancerza chroni�cego dan� hitlokacj�.
 * W zale�no�ci od obecno�ci drugiego argumentu zwraca tablic�
 * z warto�ciami pancerza na r�ne typy obra�e� lub klas� pancerza
 * na podany typ obra�e�.
 *
 * @param hid hitlokacja, o kt�rej pancerz pytamy
 * @param dtype typ obra�e� (W_IMPALE, W_SLASH, W_BLUDGEON - zdefiniowane
 * w <b>/sys/wa_types.h</b>) lub -1, je�eli chcemy uzyska� klas� pancerza
 * dla wszystkich typ�w.
 *
 * @return
 * <ul>
 *   <li> warto�� klasy pancerza dla danego typu, je�eli typ zosta� podany
 *   <li> tablica postaci ({ac na k�ute, ac na ci�te, ac na obuchowe})
 * </ul>
 */
public mixed
query_ac(int hid = 0, int dtype = -1)
{
    int prot;

    prot = (ac_worn ?: (ac_slots | ac_side_bits | (ac_side_bits << 1)));

    if (dtype == -1)
    {
        return ((hid & prot)
            ? ({ podlicz_ac(hid, 0) - condition + repair,
            podlicz_ac(hid, 1) - condition + repair,
            podlicz_ac(hid, 2) - condition + repair })
            : ({ 0, 0, 0 }));
    }

    switch(dtype)
    {
        case W_IMPALE:      dtype = 0;  break;
        case W_SLASH:       dtype = 1;  break;
        case W_BLUDGEON:    dtype = 2;  break;
        default: dtype = 0;
    }

    return ((hid & prot) ? podlicz_ac(hid, dtype) - condition + repair : 0);
}

/**
 * Pancerz si� zepsu�, tak wi�c zdejmujemy go z gracza.
 *
 * @param silent - <b>prawda</b> gdy chcemy, by funkcja nie wypisywa�a nic
 * na ekran.
 */
varargs void
remove_broken(int silent = 0)
{
    if(this_object()->query_prop(OBJ_I_BROKEN))
        return;

    if (!worn || !wearer)
    {
        add_prop(OBJ_I_BROKEN, 1);
        return;
    }

    if (objectp(wear_func))
        wear_func->remove(this_object());

    if (!silent)
    {
        tell_object(wearer, capitalize(short(wearer, PL_MIA)) +
            " rozpada" + (query_tylko_mn() ? "j^a" : "") + " si^e!\n");
        tell_room(environment(wearer), QCSHORT(this_object(), PL_MIA) + " " +
            QIMIE(wearer, PL_DOP) + " rozpada" +
            (query_tylko_mn() ? "j^a" : "") + " si^e!\n", ({ wearer }));
    }

    TO->remove_me(1);
    wearer->remove_arm(this_object());
    add_prop(OBJ_I_BROKEN, 1);
    worn = 0;
}

/**
 * Ustawia licznik sparowa� przez ten pancerz. Im wi�cej pancerz sparowa�
 * uderze�, tym bardziej podatny jest na pogorszenie stanu.
 *
 * @param new_hits - nowa warto�� licznika sparowa�
 */
public void
set_armour_hits(int new_hits)
{
    hits = new_hits;
}

/**
 * Zwraca licznik sparowa� pancerza. Im mniejsza to liczba, tym mniejsza szansa,
 * �e pancerzowi pogorszy si� stan.
 *
 * @return licznik sparowa� pancerza
 */
public int
query_armour_hits()
{
    return hits;
}

/**
 * Funkcja wywo�ywana, gdy kto� ustawia w�asno�� <b>value</b> temu obiektowi.
 *
 * @param val - nowa warto�� w�asno�ci
 *
 * @return <b>1</b> je�eli nie chcemy pozwoli� na ustawienie nowej warto�ci
 */
int
add_prop_obj_i_value(mixed val)
{
    if (!max_value)
    {
        max_value = -1;
        return 0;
    }

    if (intp(val) && val)
        max_value = val;

    return 1;
}

/**
 * Zwraca warto�� pancerza.
 *
 * @return warto�� pancerza
 */
int
query_value()
{
    if (query_prop(OBJ_I_BROKEN))
        return 0;

    return (max_value * F_ARMOUR_VALUE_REDUCE(condition - repair) / 100);
}

/**
 * Powiadamia zbroj�, �e w�a�nie zosta�a u�yta do parowania.
 */
public void
did_parry()
{
    hits++;
    if (F_ARMOUR_CONDITION_WORSE(hits, arm_ac, likely_cond))
    {
        hits = 0;
        set_condition(query_condition() + 1);
    }
}

/**
 * Powiadamia zboj�, �e zosta�a trafiona. Funkcja ta mo�e by� u�ywana
 * do redukowania ac dla hitlokacji przy ka�dym trafieniu.
 *
 * @param hid - hitlokacja
 * @param ph - procent obra�e�
 * @param att - obiekt atakuj�cy
 * @param dt - rodzaj obra�e�
 * @param dam - zadane obra�enia
 */
varargs int
got_hit(int hid, int ph, object att, int dt, int dam)
{
    if (dam <= 0)
        return 0;

    did_parry();

    return 0;
}

/**
 * Sprawdza, czy obiekt jest pancerzem.
 *
 * @return <b>1<b>
 */
nomask int
check_armour()
{
    return 1;
}

/**
 * Zwraca tablic� ze slotami chronionymi przez pancerz.
 *
 * @return tablica ze slotami chronionymi przez pancerz
 */
public nomask int *
query_protects()
{
    int prot, abit, *hids;

    prot = (ac_worn ?: (ac_slots | ac_side_bits | (ac_side_bits << 1) ));

    for (hids = ({}), abit = A_HEAD; abit <= prot; abit <<= 1)
        if (prot & abit)
            hids = hids + ({ abit });

    return hids;
}

/**
 * Zwraca standardow� warto�� zbroi, wyliczon� na podstawie jej klas
 * pancerza i hitlokacji (cz�ci cia�a), kt�re chroni.
 *
 * @return �rednia warto�� dla danego typu zbroi
 */
public int
query_default_value()
{
    int val, *slots, ix, ac, double;

    val = (ac_slots | ac_side_bits);
    for (ix = TS_HEAD, slots = ({ }); ix <= val; ix <<= 1)
        if (ix & val)
        slots += ({ ix });

    val = 0;
    ix = sizeof(slots);
    while (--ix >= 0)
    {
        ac = (ac_table[slots[ix]][0] + ac_table[slots[ix]][1] +
            ac_table[slots[ix]][2]) / 3;

        ac = (20 + ac * (ac + 21) / 3);
        switch(slots[ix])
        {
            case A_BODY:	val += 4 * ac / 5; break;
            case A_LEGS:	val += 3 * ac / 5; break;
            case A_HEAD:	val += ac; break;
            case A_R_ARM:	val += ac / 3; break;
            case A_L_ARM:	val += ac / 3; break;
            case A_R_FOOT:	val += ac / 4; break;
            case A_L_FOOT:	val += ac / 4; break;
            default:		val += 4 * ac / 5; break;
        }
    }

    return val;
}

/**
 * Zwraca standardow� wag� zbroi, wyliczon� na podstawie jej klas
 * pancerza i hitlokacji (cz�ci cia�a), kt�re chroni.
 *
 * @return �rednia waga dla danego typu zbroi
 */
public int
query_default_weight()
{
    int val, *slots, ix, ac;

    val = (ac_slots | ac_side_bits);
    for (ix = TS_HEAD, slots = ({ }); ix <= val; ix <<= 1)
        if (ix & val)
            slots += ({ ix });

    val = 0;
    ix = sizeof(slots);
    while (--ix >= 0)
    {
        ac = (ac_table[slots[ix]][0] + ac_table[slots[ix]][1] +
            ac_table[slots[ix]][2]) / 3;
        ac = ((ac > 19) ? 875 : 500) * ac + 150;

        switch(slots[ix])
        {
            case A_BODY:	val += 5 * ac / 10; break;
            case A_LEGS:	val += 2 * ac / 10; break;
            case A_HEAD:	val += ac / 10; break;
            case A_R_ARM:	val += ac / 10; break;
            case A_L_ARM:	val += ac / 10; break;
            case A_R_FOOT:	val += ac / 6; break;
            case A_L_FOOT:	val += ac / 6; break;
            default:		val += ac / 20; break;
        }
    }

    return val;
}

/**
 * Uaktualnia wag� i warto�� obiektu do prawid�owych warto�ci.
 */
nomask void
update_prop_settings()
{
    if (max_value == -1)
        max_value = query_default_value();

    if (F_WEIGHT_FAULT_ARMOUR(query_prop(OBJ_I_WEIGHT), query_default_weight()) &&
        !query_prop(OBJ_I_IS_MAGIC_ARMOUR))
    {
        add_prop(OBJ_I_WEIGHT, query_default_weight());
    }
}

/**
 * Funkcja zwraca obiekt nosz�cy dany pancerz, je�eli jest on w og�le noszony.
 *
 * @return obiekt nosz�cy dany pancerz, je�eli jest on w og�le noszony
 */
object
query_worn()
{
    if (worn)
        return wearer;
    return 0;
}

/**
 * Funkcja ta jest wywo�ywana przy komendzie 'stat'.
 *
 * @return dok�adny opis obiektu
 */
string
stat_object()
{
    string str, tmp;
    int typ, *slots, *side_bits, ix;

    str = ::stat_object();

    str += wearable_item_usage_desc();

    for (side_bits = ({}), ix = TS_HEAD; ix <= ac_side_bits; ix <<= 1)
        if (ix & ac_side_bits)
        side_bits += ({ ix });
    slots = (int *)m_indices(ac_table) - side_bits;
    ix = sizeof(slots);
    if (ix)
    {
        typ = ac_side_bits << 1;
        str += "Chroni:               k^lute  ci^ete  obuchowe\n";
        while (--ix >= 0)
        {
#if 0
            switch(slots[ix])
            {
                case A_HEAD: tmp = "g^low^e"; break;
                case A_NECK: tmp = "szyj�"; break;
                case A_CHEST: tmp = "klatk� piersiow�"; break;
                case A_STOMACH: tmp = "brzuch"; break;
                case A_HIPS: tmp = "biodra"; break;
                case A_R_THIGH: tmp = "prawe udo"; break;
                case A_L_THIGH: tmp = "lewe udo"; break;
                case A_R_SHIN: tmp = "praw� gole�"; break;
                case A_L_SHIN: tmp = "lew� gole�"; break;
                case A_R_LEG: tmp = "lew� nog�"; break;
                case A_L_LEG: tmp = "praw� nog�"; break;
                case A_R_SHOULDER: tmp = "prawy bark"; break;
                case A_L_SHOULDER: tmp = "lewy bark"; break;
                case A_R_ARM: tmp = "prawe rami^e"; break;
                case A_L_ARM: tmp = "lewe rami^e"; break;
                case A_R_FOREARM: tmp = "prawe przedrami�"; break;
                case A_L_FOREARM: tmp = "lewe przedrami�"; break;
                case A_R_HAND: tmp = "praw� d�o�"; break;
                case A_L_HAND: tmp = "lew� d�o�"; break;
                case A_R_FINGER: tmp = "serdeczny palec prawej r�ki"; break;
                case A_L_FINGER: tmp = "serdeczny palec lewej r�ki"; break;
                case A_R_FOOT: tmp = "praw� stop�"; break;
                case A_L_FOOT: tmp = "lew� stop�"; break;
                default: tmp = "'" + slots[ix] + "'"; break;
            }
#endif
            tmp = SLOT_NAME(slots[ix], PL_BIE);

            str += sprintf("%20s:%5d, %5d, %5d;\n",
                capitalize((slots[ix] & typ ? "prawe lub " : "") + tmp),
                ac_table[slots[ix]][0], ac_table[slots[ix]][1],
                ac_table[slots[ix]][2] );
        }
    }
    else
        str += "Nie chroni ^zadnej hitlokacji.\n";

    if (!query_worn())
        wearer = this_player();

    str += "Trafienia: " + hits + "  Stan: " + condition + "  Naprawy: " + repair + "\n";

    return str + "\n";
}

/**
 * Funkcja zwraca opis stanu pancerza.
 *
 * @return opis stanu pancerza
 */
string
arm_condition_desc()
{
  string str;

  if (query_prop(OBJ_I_BROKEN))
    return (query_tylko_mn() ? "S^a" : "Jest") + " zupe^lnie zniszcz" +
      koncowka("ony", "ona", "one", "eni", "one") + ".\n";

  switch(condition - repair)
  {
    case 0:
      str = "w znakomitym stanie";
      break;
    case 1:
    case 2:
      str = "lekko podniszcz" + koncowka("ony", "ona", "one", "eni",
          "one");
      break;
    case 3:
    case 4:
      str = "w kiepskim stanie";
      break;
    case 5:
    case 6:
    case 7:
      str = "w op^lakanym stanie";
      break;
    default:
      str = "gotow" + koncowka("y", "a", "e", "i", "e") +
        " si^e rozpa^s^c w ka^zdej chwili";
      break;
  }

  return "Wygl^ada na to, ^ze " + (query_tylko_mn() ? "s^a" : "jest") +
    " " + str + ".\n";
}

/**
 * Kto� chce oceni� obiekt. Dodajemy informacj� jak si� powinno nosi�
 * dan� zbroj�.
 *
 * @param num poziom umiej�tno�ci, z jakim przeprowadzana jest ocena.
 */
void
appraise_object(int num)
{
    ::appraise_object(num);

    appraise_wearable_item();
}

/**
 * Zwraca stringa dla odzyskania podstawowych zmiennych pancerza.
 *
 * @return cz�� stringa do zapisu
 */
string
query_arm_auto_load()
{
    return "#ARM#" + hits + "#" + query_prop(OBJ_I_BROKEN) + "#";
}

/**
 * Inicjalizuje odzyskane zmienne pancerza.
 *
 * @param arg - odczytany string pasuj�cy do tego zwracanego przez
 * <b>query_arm_auto_load()</b>
 *
 * @return pozosta�a cz�� napisu.
 */
string
init_arm_arg(string arg)
{
    string foobar, rest;
    int    br;

    if (arg == 0)
        return 0;

    if(sscanf(arg, "%s#ARM#%d#%d#%s", foobar, hits, br, rest) == 4)
    {
        if(br)
            add_prop(OBJ_I_BROKEN, 1);

        return foobar + rest;
    }

    int cond, rep;
    //Stary odczyt
    if(sscanf(arg, "%s#ARM#%d#%d#%d#%d#%s", foobar, hits, cond, rep, br, rest) == 6)
    {
        if(br)
            add_prop(OBJ_I_BROKEN, 1);

        set_condition(cond);
        set_repair(rep);

        return foobar + rest;
    }

    return arg;
}

/**
 * Wywo�ywana w celu uzyskania stringa do p�niejszego odzyskania. Je�eli
 * s� jakie� zmienne, kt�re dodatkowo chcia�oby si� odzyskiwa�, to trzeba
 * przedefiniowa� t� funkcj�, zachowuj�� minimalnie posta�, jaka aktualnie
 * w tej funkcji.
 *
 * Je�eli z jakich� wzgl�d�w nie chcesz, by pancerz by� odzyskiwalny,
 * to musisz przedefiniowa� t� funkcj�, by zwraca�a <b>0</b>.
 *
 * @return string do p�niejszego odzyskania
 */
public string
query_auto_load()
{
    return ::query_auto_load() + query_rozmiary_auto_load() + query_wearable_auto_load() +
        query_arm_auto_load() + query_condition_auto_load();
}

/**
 * Kiedy pancerz jest odzyskiwany, funkcja ta jest wywo�ywana w celu ustawienia
 * niezb�dnych zmiennych. Je�eli przedefiniujesz t� funkcj�, to musisz wywo�ywa�
 * <b>::init_arg</b> z argumentem, kt�ry zwr�ci�a funkcja
 * <b>query_arm_auto_load</b>.
 *
 * @param arg - argument do sparsowania
 *
 * @return pozosta�a cz�� napisu
 */
public string
init_arg(string arg)
{
    return init_condition_arg(init_arm_arg(init_wearable_arg(init_rozmiary_arg(::init_arg(arg)))));
}

public int legal_repair(int rep)
{
    return F_LEGAL_ARMOUR_REPAIR(rep, query_condition());
}

public void repair_hook(int rep)
{
    if(worn && wearer)
        wearer->update_armour(this_object());
}

public void contidion_hook(int cond)
{
    if (F_ARMOUR_BREAK(query_condition() - query_repair(), query_likely_break()))
        set_alarm(0.1, 0.0, remove_broken);

    if (worn && wearer)
        wearer->update_armour(this_object());
}

/**
 * Funkcja identyfikuj�ca obiekt jako zbroje.
 *
 * @return 1
 */
public int
am_i_an_armour() { return 1; }

/**
 * Funkcja identyfikuj�ca obiekt jako zbroje.
 *
 * @return 1
 */
public int
is_armour() { return 1; }
