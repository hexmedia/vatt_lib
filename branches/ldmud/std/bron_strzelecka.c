/**
 * \file /std/bron_strzelecka.c
 *
 * Standardzik broni strzeleckiej, u�ywany we wszystkich broniach strzeleckich takich jak �uk,
 * kusza czy proca a kiedy� by� mo�e co� jeszcze.
 *
 * @author Krun
 * @date Grudzie� 2007
 *
 * TODO:
 * - mo�liw�� �adowania kilku pocisk�w na bro�
 *
 */

//W ko�cu to bro� wi�c na standardzie broni.
inherit "/std/weapon.c";

//A �e dodatkowo mo�emy to za�o�y�, to i wearable_item.
inherit "/lib/wearable_item.c";

#include <pl.h>
#include <colors.h>
#include <macros.h>
#include <cmdparse.h>
#include <formulas.h>
#include <ss_types.h>
#include <wa_types.h>
#include <strzelecka.h>
#include <object_types.h>
#include <stdproperties.h>

/*
 * Definicje:
 */

#define DBG(x)      find_player("krun")->catch_msg(set_color(find_player("krun"),   \
    COLOR_FG_RED, COLOR_BOLD_ON) + "Bro� strzelecka: " + x + "\n" + clear_color(find_player("krun")));

//Przypadki poszczeg�lnych obiekt�w
#define PARSE_BRON              0
#define PARSE_CIECIWA           1
#define PARSE_POCISK            2
#define PARSE_NACIAGACZ         3

//Ustawienie w tablicy komend
#define KOMENDY_WZOR            0
#define KOMENDY_BEZOKOL         1
#define KOMENDY_1OS             2
#define KOMENDY_2OS             3
#define KOMENDY_PRZYPADKI       4

//Przydatne makra
#define THIS_LW_SKILL           (SS_WEP_FIRST + (W_MISSILE + 1 + TO->query_typ()))
#define LW_SKILL                (SS_WEP_FIRST + W_MISSILE)

/**********************************************************************/

/*
 * Zmienne konfiguracyjne:
 */

int         zasieg,                 /* zasi�g broni */
            sila_razenia,           /* si�a ra�enia broni */
            potrzebna_sila,         /* si�a potrzebna do korzystania */
            potrzebna_zrecznosc,    /* zr�czno�� potrzebna do korzystania */
            potrzebny_um,           /* poziom uma potrzebny do korzystania */
            celnosc,                /* celnosc broni */
            jakosc,                 /* jako�� broni(automatycznie ustawia reszte) */
            typ,                    /* typ broni */
            is_wearable,            /* czy jest to bro� zak�adana */
            sila_naciagu,           /* si�a naci�gu broni */
            alarm_naciagania,       /* Alarm przy naci�ganiu broni */
            mod_celnosci;           /* Modyfikator do skuteczno�ci celowania zale�ny od momentu celowania */

mapping     komendy_naciagania,     /* komenda naci�gania broni + bezokolicznik + przypadki obiekt�w */
            komendy_ladowania,      /* komenda nak�adania strza�y na bro� + bezokolicznik + przypadki obiekt�w */
            komendy_strzelania,     /* komenda strzelania z broni + bezokolicznik + przypadki obiekt�w */
            komendy_zakladania;

//A tu troche zmiennych u�ywanych przy u�ywaniu broni

object      cel,            /* do czego celujemy ( nie wiadomo czy trafimy:P ) */
            env_cel,        /* na jakiej lokacji by� cel, jak celowali�my */
            lokacja,        /* je�li na �lepo celujemy na jak�� lokacj� */
            pocisk,         /* pocisk za�adowany do broni */
            korba,          /* obiekt korby */
            paraliz_naciagania,
                            /* obiekt parali�u naciagania */
            cieciwa;        /* obiekt cieciwy powoli wprowadzany do u�ycia */

string      kierunek,       /* kierunek w jakim celujemy */
            opis_zaladowanego_pocisku,
                            /* pattern dla opisu pokazywanego w longu gdy pocisk jest za�adowany */
            opis_napietej_broni;
                            /* opis pokazywany w longu gdy bro� jest napi�ta */

/*
 * Prototypy funkcji:
 */

private static int *find_przypadki_in_patt(string patt);
private static string zamien_na_patt(string patt);
private static varargs string koloruj(string arg, mixed fob, int color, int ext=0);
varargs string zamien_kierunek(string k, int i=0);
private static string zamien_kierunek_na_odwrotny(string k);
private static object find_other_room(string kierunek);
private static void dodaj_shadow_celu(object who, object celujacy=TP);
private static void dodaj_shadow_celujacego(object who, object celujacy=TP);
private static varargs mixed sparsuj_komende(string arg, string patt, mixed env, string verb);
private static varargs string bsprintf(string patt, string ob, string oc, string op, string on, string os="");
private static void usun_cel();
private static void wyladuj_pocisk_z_broni();
private static void zaladuj_pocisk_do_broni(object p);
private static object wylosuj_cel_z_lokacji(object l, object p);
private static void bron_naciagnieta(int sn, string verb, object pl = ENV(TO));
private static string naciaganie_kom(string verb, int ident);
public void wloz_cieciwe_do_broni();

/**********************************************************************/
/**
 * Kreator broni strzeleckiej.
 */
public void
create_bron_strzelecka()
{
}

/**
 * Kreator zwyk�ej broni.
 */
public nomask void
create_weapon()
{
    ustaw_nazwe(({"bro� strzelecka", "broni strzeleckiej", "broni strzeleckiej",
        "bro� strzeleck�", "broni� strzeleck�", "broni strzeleckiej"}),
        ({"bronie strzeleckie", "broni strzeleckich", "broniom strzeleckim",
        "bronie strzeleckie", "bro�mi strzeleckimi", "broniach strzeleckich"}),
        PL_ZENSKI);

    set_long("Ot co... Zwyk�a bro� strzelecka.\n");

    set_hands(W_BOTH);
    create_bron_strzelecka();
}

/**
 * Funkcja wywo�uj�ca si� podczas resetu broni strzeleckiej.
 */
public void
reset_bron_strzelecka()
{
}

/**
 * Funkcja wywo�ywana podczas resetu broni.
 */
public nomask void
reset_weapon()
{
    reset_bron_strzelecka();
}

/** *********************************************************************** **
 ** *****************       U S T A W I E N I A        ******************** **
 ** *********************************************************************** **/

/**
 * @return aktualnie za�adowany pocisk.
 */
public object query_pocisk() { return pocisk; }

/**
 * @return aktualny cel
 */
public object query_cel() { return cel; }

/**
 * @return aktualne otoczenie celu.
 */
public object query_env_cel() { return env_cel; }

/**
 * @return kierunek w jakim aktualnie celujemy.
 */
public string query_kierunek() { return kierunek; }

/**
 * @return jak mocno naci�gni�ta jest bro�.
 */
public int query_sila_naciagu() { return sila_naciagu; }

/**
 * @return czas przez jaki bro� b�dzie napinana
 */
public float query_czas_naciagu()
{
    if(!objectp(paraliz_naciagania))
        return 0; //Nienapi�ty/a

    return paraliz_naciagania->query_this_remove_time();
}

/**
 * @return ile czasu bro� jest napianna
 */
public float query_a_czas_naciagu()
{
    if(!objectp(paraliz_naciagania))
        return 0; //Nienapi�ty/a

    return paraliz_naciagania->query_remove_time_to_left();
}

/**
 * @return kto naci�ga bro�.
 */
public object query_naciagajacy() { return ENV(TO); }

/**
 * @return ci�ciw� za�o�on� w broni
 */
public object query_cieciwa() { return cieciwa; }

/**
 * Ustawiamy plik standardowej ci�ciwy.
 *
 * @param fn plik ci�ciwy
 */
public void ustaw_cieciwe(string fn) { cieciwa = clone_object(fn); wloz_cieciwe_do_broni(); }

/**
 * Ustawiamy typ broni strzeleckiej.
 */
public void ustaw_typ(int t) { typ = t; }

/**
 * @return typ broni strzeleckiej.
 */
public int query_typ() { return typ; }

/**
 * Dzi�ki tej funkcji mo�emy ustawi� zasi�g broni.
 * @param z (1-mo�liwo�� strzelania na s�siedni� lokacje, 0-brak mo�liwo�ci)
 */
public void ustaw_zasieg(int z) { zasieg = !!z; }

/**
 * @return 1 mo�a strzela� na lokacje na kt�re prowadz� przej�cia
 * @return 0 mo�na strzela� tylko na lokacje na kt�rej jeste�my.
 */
public int query_zasieg() { return zasieg; }

/**
 * Funkcja ustawiaj�ca jako�� broni.
 *
 * @param j jako�� broni
 */
public void ustaw_jakosc(int j) { jakosc = j; }

/**
 * @return jako�� broni.
 */
public int query_jakosc() { return jakosc; }

/**
 * Dzi�ki tej funkcji ustawimy minimaln� si�� potrzebn� do u�ycia tej broni.
 * @param s potrzebna si�a
 */
public void ustaw_potrzebna_sile(int s) { potrzebna_sila = s; }

/**
 * @return si�e potrzebn� do u�ycia broni.
 */
public int query_potrzebna_sila() { return potrzebna_sila; }

/**
 * Funkcja ta ustawia minimaln� zr�czno�� potrzebn� do u�ycia obiektu.
 * @param z potrzebna zr�czno��
 */
public void ustaw_potrzebna_zrecznosc(int z) { potrzebna_zrecznosc = z; }

/**
 * @return zr�czno�� potrzebn� do pos�ugiwania si� broni�.
 */
public int query_potrzebna_zrecznosc() { return potrzebna_zrecznosc; }

/**
 * Ustawiamy umiej�tno�� potrzebn� do pos�ugiwania si� broni�.
 * @param u potrzebna umiej�tno��
 */
public void ustaw_potrzebna_umiejetnosc(int u) { potrzebny_um = u; }

/**
 * @return wysoko�� umiej�tno�ci potrzebnej do u�ycia tek broni - umiej�tno�� rozpoznawana jest przez typ obiektu.
 */
public int query_potrzebna_umiejetnosc() { return potrzebny_um; }

/**
 * Ustawiamy jak celna jest ta bro�.
 * @param p celno�� broni
 */
public void ustaw_celnosc(int c) { celnosc = c; }

/**
 * @return celno�� tej broni.
 */
public int query_celnosc() { return celnosc; }

/**
 * Ustawiamy jak silne obra�enia bro� b�dzie zadawa�.
 * @param m modyfikator do obra�e� (1-100)
 */
public void ustaw_sile_razenia(int m) { sila_razenia = min(100, max(1, m)); }

/**
 * @return zwraca modyfikator si�y ra�enia broni.
 */
public int query_sila_razenia() { return sila_razenia; }

/**
 * Ustawiamy opis za�adowanego pocisku.
 *
 * @param str Opis, jesli chcemy u�y� jakiej� ko�c�wki to podaj wszystkie 5 argument�w
 *            do funkcji koncowka w [] oddzielone za pomoc� |.
 *            Je�li chcesz wywo�a� to na innym obiekcie ni� strza�a po ostatnim argumencie
 *            przed ] dodaj # i file_name obiektu.
 */
public void ustaw_opis_zaladowanego_pocisku(string str)
{
    opis_zaladowanego_pocisku = str;
}

/**
 * @return opis za�adowanego pocisku pokazywany w �uku.
 */
public string query_opis_zaladowanego_pocisku() { return opis_zaladowanego_pocisku; }

/**
 * Ustawiamy opis pokazywany w longu gdy bro� jest napi�ta.
 *
 * @param o opis pokazywany w longu.
 */
public void ustaw_opis_napietej_broni(string o)
{
    opis_napietej_broni = o;
}

/**
 * @return opis napietej broni.
 */
public string query_opis_napietej_broni()
{
    return opis_napietej_broni;
}

/** *********************************************************************** **
 ** *****************          H  O  O  K  I          ********************* **
 ** *********************************************************************** **/

static void bs_hook_koniec_naciagania(object pl)
{
    pl->catch_msg("Jeste� tak zm�czony, �e nie jeste� ju� w stanie dalej naci�ga� " +
        query_cieciwa()->short(pl, PL_BIE) + " " + TO->short(pl, PL_DOP) + ".\n");
    tell_roombb(ENV(pl), QCIMIE(pl, PL_MIA) + " przestaje naci�ga� " + QSHORT(query_cieciwa(), PL_BIE) + " " +
        QSHORT(TO, PL_BIE) + ".\n", ({pl}));
}

static void bs_hook_zaprzestanie_naciagania(object pl)
{
    pl->catch_msg("Przestajesz naci�ga� " + query_cieciwa()->short(pl, PL_BIE) + " " + TO->short(pl, PL_DOP) + ".\n");
    tell_roombb(ENV(pl), QCIMIE(pl, PL_MIA) + " przestaje naci�ga� " + QSHORT(query_cieciwa(), PL_BIE) + " " +
        QSHORT(TO, PL_BIE) + ".\n", ({pl}));
}

/** *********************************************************************** **
 ** *****************      K  O  M  E  N  D  Y        ********************* **
 ** *********************************************************************** **/

/**
 * Dodajemy komend� naci�gania broni.
 * @param patt komenda + wz�r
 * @param bez  bezokolicznik komendy
 * @param os1  komenda w pierwszej osobie
 * @param os2  komenda w drugiej osobie
 * @param przyp przypadek, argument opcjonalny u�ywany zwykle w sytuacjach kiedy komenda nie przyjmuje �adnego argumentu
 *              w celu poprawnego formatowania komunikat�w.
 */
public varargs void dodaj_komende_naciagania(string patt, string bez, string os1, string os2, int przyp = -1)
{
    int *przyps = find_przypadki_in_patt(patt);
    string *tmp = explode(patt, " ");
    string komenda = tmp[0];
    patt = implode(tmp[1..], " ");

    if(przyp != -1)
        przyps = ({przyp});

    if(!mappingp(komendy_naciagania))
        komendy_naciagania = ([]);

    komendy_naciagania[komenda] = ({patt, bez, os1, os2, przyps});
}

/**
 * @return komendy naci�gania broni
 */
public mapping query_komendy_naciagania() { return komendy_naciagania; }

/**
 * @return czy podany string jest komend� naci�gania.
 */
public int is_komenda_naciagania(string komenda)
{
    return is_mapping_index(komenda, komendy_naciagania);
}

/**
 * @return tablice z podan� komend� naci�gania.
 */
public mixed query_komenda_naciagania(string komenda)
{
    if(is_komenda_naciagania(komenda))
        return komendy_naciagania[komenda];
}

/**
 * Usuwamy komende naci�gania broni.
 * @param com komenda
 */
public void usun_komende_naciagania(string komenda)
{
    if(is_komenda_naciagania(komenda))
        komendy_strzelania = m_delete(komendy_naciagania, komenda);
}

/**
 * Dzi�ki tej funkcji mo�emy wyzerowa� tablice z komendami naci�gania.
 */
public void zresetuj_komendy_naciagania() { komendy_naciagania = 0; }

/**
 * Dodajemy komend� �adowania broni.
 * @param patt komenda + wz�r
 * @param bez  bezokolicznik komedny
 * @param os1  komenda w pierwszej osobie
 * @param os2  komenda w drugiej osobie
 * @param przyp przypadek, argument opcjonalny u�ywany zwykle w sytuacjach kiedy komenda nie przyjmuje �adnego argumentu
 *              w celu poprawnego formatowania komunikat�w.
 */
public varargs void dodaj_komende_ladowania(string patt, string bez, string os1, string os2, int przyp = -1)
{
    int *przyps = find_przypadki_in_patt(patt);
    string *tmp = explode(patt, " ");
    string komenda = tmp[0];
    patt = implode(tmp[1..], " ");

    if(przyp != -1)
        przyps = ({przyp});

    if(!mappingp(komendy_ladowania))
        komendy_ladowania = ([]);

    komendy_ladowania[komenda] = ({patt, bez, os1, os2, przyps});
}

/**
 * @return komendy �adowania pocisku do bro�.
 */
public mapping query_komendy_ladowania() { return komendy_ladowania; }

/**
 * @return 1 podany string jest komend� �adowania.
 * @return 0 podany string nie jest komend� �adowania.
 */
public int is_komenda_ladowania(string komenda)
{
    return (is_mapping_index(komenda, komendy_ladowania));
}

/**
 * @return tablice z podan� komend� �adowania.
 */
public mixed query_komenda_nakladania(string komenda)
{
    if(is_mapping_index(komenda, komendy_ladowania))
        return komendy_ladowania[komenda];
}

/**
 * Usuwamy komende �adowania pocisku do bro�.
 * @param com komenda
 */
public void usun_komende_nakladania(string komenda)
{
    if(is_mapping_index(komenda, komendy_ladowania))
        komendy_ladowania = m_delete(komendy_ladowania, komenda);
}

/**
 * Dzi�ki tej funkcji mo�emy wyzerowa� tablice z komendami �adowania.
 */
public void zresetuj_komendy_ladowania() { komendy_ladowania = 0; }

/**
 * Dodajemy komend� strzelania broni.
 * @param patt komenda + wz�r
 * @param bez  bezokolicznik komedny
 * @param os1  komenda w pierwszej osobie
 * @param os2  komenda w drugiej osobie
 * @param przyp przypadek, argument opcjonalny u�ywany zwykle w sytuacjach kiedy komenda nie przyjmuje �adnego argumentu
 *              w celu poprawnego formatowania komunikat�w.
 */
public varargs void dodaj_komende_strzelania(string patt, string bez, string os1, string os2, int przyp=-1)
{
    int *przyps = find_przypadki_in_patt(patt);
    string *tmp = explode(patt, " ");
    string komenda = tmp[0];
    patt = implode(tmp[1..], " ");

    if(przyp != -1)
        przyps = ({przyp});

    if(!mappingp(komendy_strzelania))
        komendy_strzelania = ([]);

    komendy_strzelania[komenda] = ({patt, bez, os1, os2, przyps});
}

/**
 * @return komendy strzelania z broni
 */
public mapping query_komendy_strzelania() { return komendy_strzelania; }

/**
 * @return czy podany string jest komend� strzelania.
 */
public int is_komenda_strzelania(string komenda)
{
    return (is_mapping_index(komenda, komendy_strzelania));
}

/**
 * @return tablice z podan� komend� strzelania z tej broni.
 */
public mixed query_komenda_strzelania(string komenda)
{
    if(is_mapping_index(komenda, komendy_strzelania))
        return komendy_strzelania[komenda];
}

/**
 * Usuwamy komende strzelania z broni.
 * @param com komenda
 */
public void usun_komende_strzelania(string komenda)
{
    if(is_mapping_index(komenda, komendy_strzelania))
        komendy_strzelania = m_delete(komendy_strzelania, komenda);
}

/**
 * Dzi�ki tej funkcji mo�emy wyzerowa� tablice z komendami strzelania.
 */
public void zresetuj_komendy_strzelania() { komendy_strzelania = 0; }

public int naciagnij(string arg, int spc);

//Bierzemy si� do roboty.
public int wyceluj(string arg)
{
    NF("Gdzie chcesz wycelowa�?\n");

    if(!arg)
        return 0;

    //Najpierw sprawdzimy czy gracz mo�e si� wystarczaj�co skupi�.
    int mzc = F_BS_MANA_ZA_CELOWANIE(TO->query_jakosc());
    if((TP->query_mana() - mzc) < 0)
        return NF("Nie jeste� w stanie skupi� si� wystarczaj�co.\n");

    string sdo_kogo, na_kierunek;
    object *do_kogo = 0;
    object other_room;

    if(parse_command(arg, AIE(TP), "'do' %i:"+PL_DOP, do_kogo) ||
        parse_command(arg, AIE(TP), "'w' %i:" + PL_BIE, do_kogo))
    {   //Strzelamy na nasz� lokacje
        do_kogo = NORMAL_ACCESS(do_kogo, 0, 0);
    }
    else if((sscanf(arg, "do %s na %s", sdo_kogo, na_kierunek) == 2 ||
        sscanf(arg, "w %s na %s", sdo_kogo, na_kierunek) == 2) && query_zasieg())
    {   //Strzelamy na kt�r�� z okolicznych lokacji
        other_room = find_other_room(na_kierunek);

        if(!other_room)
            return 0;

        if(!parse_command(sdo_kogo, AI(other_room), "%i:" + PL_DOP, do_kogo))
            return 0;

        do_kogo = ({do_kogo[0][1]});
    }
    else if(sscanf(arg, "na %s", na_kierunek) == 1 && query_zasieg())
    {
        other_room = find_other_room(na_kierunek);
    }
    else
        return 0;

    //No to rozpoznali�my co gdzie jest, teraz poustawiamy i wy�wietlimy komunikaty.

    if(do_kogo && !sizeof(do_kogo))
        return 0;

    if(sizeof(do_kogo) > 1)
    {
        return NF("Jak chcesz wycelowa� do kilku " +
            (sizeof(filter(do_kogo, &living())) > 0 ? "os�b " +
            (sizeof(filter(do_kogo, &not() @ &living())) > 0 ? "lub rzeczy" : "") :
            "rzeczy") + " na raz?\n");
    }

    if(cel && do_kogo[0] == cel)
        return NF("Ju� celujesz do " + cel->short(TP, PL_DOP) + ".\n");

    if(!TO->query_wielded())
        return NF("Najpierw musisz doby� " + TO->short(TP, PL_DOP) + ".\n");

    if(!query_pocisk() && query_typ() == W_L_BOW)
    {
        string verb = m_indexes(komendy_ladowania)[0];
        return NF("Nie mo�esz teraz wycelowa� w nic ani nikogo. Najpierw " +
            "musisz " + komendy_ladowania[verb][KOMENDY_BEZOKOL] + " " +
            bsprintf(komendy_ladowania[verb][KOMENDY_WZOR], "jego",
            query_cieciwa()->query_nazwa(komendy_ladowania[verb][KOMENDY_PRZYPADKI][PARSE_CIECIWA]),
            "jaki� pocisk", "") + ".\n");
    }

    if(query_typ() == W_L_SLING)
        naciagnij(m_indexes(komendy_naciagania)[0], 1);

    TP->add_mana(-mzc);

    string na_kierunek1 = zamien_kierunek(na_kierunek, 1);
    string na_kierunek2 = zamien_kierunek(na_kierunek, 2);
    string na_kierunek3 = zamien_kierunek(na_kierunek, 3);
    string na_kierunek4 = zamien_kierunek(na_kierunek, 4);

    if(query_typ() != W_L_SLING)
    {
        if(!cel && !env_cel)
        {
            write("Unosisz " + TO->short(TP, PL_BIE) + " i celujesz" +
                ((pointerp(do_kogo) && objectp(do_kogo[0])) ? " do " + do_kogo[0]->short(TP, PL_DOP) +
                (objectp(other_room) ? " znajduj�cego si� " : "") : "") +
                (objectp(other_room) ? " " +  ((pointerp(do_kogo) && objectp(do_kogo[0])) ? na_kierunek1 : na_kierunek4) : "") +
                ".\n");
            saybb(QCIMIE(TP, PL_MIA) + " unosi " + QSHORT(TO, PL_BIE) + " i celuje" +
                ((pointerp(do_kogo) && objectp(do_kogo[0]) && !objectp(other_room)) ? " do " + QSHORT(do_kogo[0], PL_DOP) : "") +
                (objectp(other_room) ? " kogo� lub czego� " + na_kierunek2 : "") +
                ".\n", ({TP}) + (pointerp(do_kogo) && objectp(do_kogo[0]) ? ({do_kogo[0]}) : ({})));

            //Komunikaty do gracza nie na tej lokacji
            if(objectp(other_room) && pointerp(do_kogo) && objectp(do_kogo[0]) && interactive(do_kogo[0]))
            {
                int z = F_BS_ZOBACZY_ZE_CELUJA_DO_NIEGO(do_kogo[0]->query_skill(SS_AWARENESS));

                if(z == 1)
                    do_kogo[0]->catch_msg(koloruj("Zdaje ci si�, �e kto� do ciebie celuje.\n", do_kogo[0], COLOR_FG_YELLOW));
                else if(z == 2)
                {
                    do_kogo[0]->catch_msg(koloruj("Zauwa�asz, �e kto� " + na_kierunek2 +
                        " do ciebie celuje.\n", do_kogo[0], COLOR_FG_RED));
                }
                else if(z == 3)
                {
                    do_kogo[0]->catch_msg(koloruj("Zauwa�asz, �e " + na_kierunek2 + " celuje do ciebie " +
                        TP->short(do_kogo[0], PL_BIE) + ".\n", do_kogo[0], COLOR_FG_RED, COLOR_BOLD_ON));
                }
            }
            //A teraz do celu na tej lokacji.
            else if(pointerp(do_kogo) && objectp(do_kogo[0]) && interactive(do_kogo[0]))
            {
                do_kogo[0]->catch_msg(koloruj(UC(TP->short(do_kogo[0], PL_MIA)) + " celuje do CIEBIE!!!\n", do_kogo[0], COLOR_FG_RED,
                    COLOR_BOLD_ON));
            }
        }
        //A teraz je�li ju� by� jaki� cel to inny komunikacik
        else
        {
            write("Przestajesz celowa� " + (objectp(env_cel) && objectp(cel) ? "do znajduj�cego si� " +
                zamien_kierunek(kierunek, 1) : "do") + " " + (objectp(cel) ? cel->short(TP, PL_DOP) : "") +
                (objectp(env_cel) ? zamien_kierunek(kierunek, 1) : "") +
                " i zaczynasz celowa� " + (pointerp(do_kogo) && objectp(do_kogo[0]) ?
                do_kogo[0]->short(TP, PL_BIE) : "do") + " " + (objectp(other_room) ? "znajduj�cego si� " +
                na_kierunek1 : "") + (objectp(other_room) ? na_kierunek1 : "") + ".\n");

            if(env_cel != other_room || other_room == ENV(TP))
            {
                saybb(QCIMIE(TP, PL_MIA) + " przestaje celowa� do " + (objectp(env_cel) ? " kogo� lub czego� " +
                    zamien_kierunek(kierunek, 1) : QSHORT(cel, PL_DOP)) +
                    " i zaczyna celowa� do " + (objectp(other_room) ? "kogo� lub czego�" +
                    na_kierunek1 : QSHORT(do_kogo[0], PL_DOP)) + ".\n");
            }

            if(pointerp(do_kogo) && objectp(do_kogo[0]) && interactive(do_kogo[0]) && other_room)
            {
                int z = F_BS_ZOBACZY_ZE_CELUJA_DO_NIEGO(do_kogo[0]->query_skill(SS_AWARENESS));
                //Damy szanse celowi zobaczy�, �e si� do niego celuje... Uzale�nimy to od
                //spostrzegawczo�ci.
                if(z == 1)
                    do_kogo[0]->catch_msg("Zdaje ci si�, �e kto� do ciebie celuje.\n");
                else if(z == 2)
                {
                    do_kogo[0]->catch_msg(koloruj("Zauwa�asz, �e kto� " + na_kierunek2 + " do ciebie celuje.\n",
                        do_kogo[0], COLOR_FG_YELLOW));
                }
                else if(z == 3)
                {
                    do_kogo[0]->catch_msg(koloruj("Zauwa�asz, �e " + na_kierunek2 + " celuje do ciebie " +
                        TP->short(do_kogo[0], PL_BIE) + ".\n", do_kogo[0], COLOR_FG_RED, COLOR_BOLD_ON));
                }
            }

            if(interactive(cel) && env_cel)
            {
                int z = F_BS_ZOBACZY_ZE_CELUJA_DO_NIEGO(cel->query_skill(SS_AWARENESS));

                if(z == 1)
                    cel->catch_msg("Wra�enie, �e kto� do ciebie celuje znikne�o.\n");
                else if(z == 2)
                    cel->catch_msg(koloruj("Wydaje ci si�, �e ju� nikt do ciebie nie celuje.\n", cel, COLOR_FG_GREEN));
                else if(z == 3)
                {
                    cel->catch_msg(koloruj(UC(TP->short(cel, PL_MIA)) + " przesta� do ciebie celowa�.\n",
                        cel, COLOR_FG_GREEN, COLOR_BOLD_ON));
                }
            }
        }
    }
    else
    {
        if(!env_cel && !cel)
        {
            write("Celujesz" + ((pointerp(do_kogo) && objectp(do_kogo[0])) ? " do " + do_kogo[0]->short(TP, PL_DOP) +
                (objectp(other_room) ? " znajduj�cego si� " : "") : "") + (objectp(other_room) ? " " +
                ((pointerp(do_kogo) && objectp(do_kogo[0])) ? na_kierunek1 : na_kierunek4) : "") + ".\n");
        }
        else
        {
            write("Przestajesz celowa� " + (objectp(env_cel) && objectp(cel) ? "do znajduj�cego si� " +
                zamien_kierunek(kierunek, 1) : "do") + " " + (objectp(cel) ? cel->short(TP, PL_DOP) : "") +
                (objectp(env_cel) ? zamien_kierunek(kierunek, 1) : "") +
                " i zaczynasz celowa� " + (pointerp(do_kogo) && objectp(do_kogo[0]) ?
                do_kogo[0]->short(TP, PL_BIE) : "do") + " " + (objectp(other_room) ? "znajduj�cego si� " +
                na_kierunek1 : "") + (objectp(other_room) ? na_kierunek1 : "") + ".\n");
        }
    }

    if(cel)
        cel->remove_shadow_celu();

    if(other_room)
    {
        env_cel = other_room;
        kierunek = na_kierunek;
    }

    if(sizeof(do_kogo))
        cel = do_kogo[0];

    if(!sila_naciagu)
        mod_celnosci = 80;
    else
        mod_celnosci = 100;

    dodaj_shadow_celu(cel);
    dodaj_shadow_celujacego(cel, TP);

    return 1;
}

public int
sprawdz_cel(string arg)
{
    if(!cel)
    {
        write("Aktualnie do nikogo nie celujesz.\n");
        return 1;
    }

    write("Aktualnie celujesz " + (objectp(cel) ? "do " + cel->short(TP, PL_DOP) : "") +
        (objectp(env_cel) ? zamien_kierunek(kierunek, 1) : "") + ".\n");

    return 1;
}

private static string dodaj_wzor(string verb, int t, object fob)
{
    mapping komendy;

    switch(t)
    {
        case 0:     komendy = komendy_strzelania;       break;
        case 1:     komendy = komendy_ladowania;        break;
        case 2:     komendy = komendy_naciagania;       break;
        default:    return "'" + set_color(fob, COLOR_FG_MAGENTA) + verb + clear_color(fob) + "'";
    }

    if(!komendy[verb][KOMENDY_WZOR] || komendy[verb][KOMENDY_WZOR] == "")
        return "'" + set_color(fob, COLOR_FG_MAGENTA) + verb + clear_color(fob) + "'";

    return "'" + set_color(fob, COLOR_FG_MAGENTA) + (t != 0 ? komendy[verb][KOMENDY_BEZOKOL] : verb) + " " +
        bsprintf(komendy[verb][KOMENDY_WZOR], "", "", (t==1 ? "co�" : ""), "")[..-2] + clear_color(fob) + "'";
}

public int help(string arg)
{
    if(!arg)
        return 0;

    object *x;
    if(!parse_command(arg, AIE(TP) + AI(TP), "%i:" + PL_MIA, x))
        return 0;

    x = NORMAL_ACCESS(x, 0, 0);

    if(!sizeof(x))
        return 0;

    if(x[0] != TO)
        return 0;

    string *kl = m_indexes(komendy_ladowania);
    string *ks = m_indexes(komendy_strzelania);
    string *kn = m_indexes(komendy_naciagania);

    ks = map(ks, &dodaj_wzor(,0,TP));
    kl = map(kl, &dodaj_wzor(,1,TP));
    kn = map(kn, &dodaj_wzor(,2,TP));

    write(capitalize(short(TP, PL_MIA)) + " nie jest skomplikowan" + koncowka("y", "a", "e", "ni", "ne") + " w u�yciu. "+
        "Mo�esz " + koncowka("go", "jej", "go", "ich", "ich") + " " + dodaj_wzor("doby�", 4, TP) + " "+
        "a tak�e " + dodaj_wzor("opu�ci�", 4, TP) + ". Przed " + COMPOSITE_WORDS2(ks, " czy te� ") + " musisz "+
        koncowka("nim", "ni�", "nim", "nimi", "nimi") + " w kogo� " + dodaj_wzor("wycelowa�", 4, TP) + " "+
        "a nast�pnie " + COMPOSITE_WORDS2(kl, " lub ") + " i w ko�cu musisz " +
        COMPOSITE_WORDS2(kn, " albo ") + ".\n");
    //FIXME: �ukiem mo�na te� walczy� wr�cz jak broni� drzewcow�... Ale, �e to wymaga sporych zmian w systemie
    //       wi�c jest do dodania.
    return 1;
}

public int zaladuj(string arg)
{
    string verb = query_verb();

    if(!is_komenda_ladowania(verb))
    {
        DBG("1");
        return 0;
    }

    NF("Co takiego chcesz " + komendy_ladowania[verb][KOMENDY_BEZOKOL] + "?\n");

    if(!arg)
    {
        DBG("2");
        return 0;
    }

    string patt = zamien_na_patt(komendy_ladowania[verb][KOMENDY_WZOR]) || "%p:" + PL_BIE + " 'do' " + "%b:" + PL_DOP;

    mixed obiekty = sparsuj_komende(arg, patt, all_inventory(TP), verb);

    if(!pointerp(obiekty))
    {
        if(stringp(obiekty))
            return NF2(obiekty, 5);
        else if(intp(obiekty) && obiekty < 0)
        {
            switch(-(--obiekty))
            {
                case PARSE_BRON:
                    NF2("Nie mo�esz " + komendy_ladowania[verb][KOMENDY_BEZOKOL] + " " +
                        bsprintf(komendy_ladowania[verb][KOMENDY_WZOR],
                        ((komendy_ladowania[verb][KOMENDY_PRZYPADKI][PARSE_BRON] == PL_DOP) ?
                        "kilka broni" : "kilku broni" ), "", "", "") + "r�wnocze�nie.\n", 5);
                    break;
                case PARSE_CIECIWA:
                    NF2("Jedna bro� mo�e mie� tylko jedn� ci�ciw�.\n", 5);
                    break;
                case PARSE_POCISK:
                    NF2("Nie mo�esz " + komendy_ladowania[verb][KOMENDY_BEZOKOL] + " " +
                        bsprintf(komendy_ladowania[verb][KOMENDY_WZOR], "", "",
                        ((komendy_ladowania[verb][KOMENDY_PRZYPADKI][PARSE_POCISK] == PL_DOP) ?
                        "kilka pocisk�w" : "kilku pocisk�w"), "") + "r�wnocze�nie.\n", 5);
                    break;
                default:
                    int n = -(--obiekty);
                    notify_wizards(TO, "Bro� strzelecka informuje, �e nie przewidziano " +
                        "u�ycia typu" + n + " do komendy strzelania. Prosz� o sprawdzenie pliku " + MASTER_OB(TO) +
                        ", je�li jest to zamierzone u�ycie prosz� o kontakt - KRUN");
                    return NF2("Wyst�pi� b��d numer bs1. Zg�o� go opisuj�c okoliczno�ci " +
                        "w jakich do niego dosz�o. Szczeg�ln� uwag� zwr�� na komend�, " +
                        "kt�ra go wywo�a�a.\n", 5);
            }
        }
        DBG("3");
        return 0;
    }

    int *prp = komendy_ladowania[verb][KOMENDY_PRZYPADKI];

    NF2(capitalize(komendy_ladowania[verb][KOMENDY_BEZOKOL]) + " " + (bsprintf(patt, "czego", "", "co", "")) + "?\n", 2);

    if(!obiekty[PARSE_POCISK] || !objectp(obiekty[PARSE_POCISK]))
        return 0;

    if(!obiekty[PARSE_POCISK]->is_bullet())
    {
        return NF2(capitalize(obiekty[PARSE_POCISK]->short(TP, PL_MIA)) + " nie nadaje si� do u�ycia w " +
            TO->short(TP, PL_DOP) + ".\n", 5);
    }

    if(cieciwa)
    {
        if(!obiekty[PARSE_CIECIWA])
            return NF2("Na co chcesz to " + komendy_ladowania[verb][KOMENDY_BEZOKOL] + "?\n", 5);

        if(!obiekty[PARSE_CIECIWA]->is_cieciwa())
            return NF2(capitalize(obiekty[PARSE_CIECIWA]->short(TP, PL_MIA)) + " nie jest ci�ciw�.\n", 5);

    }

    if(!obiekty[PARSE_BRON] || !objectp(obiekty[PARSE_BRON]))
        return NF2("Na " + bsprintf(komendy_ladowania[verb][KOMENDY_WZOR], "czego",
            cieciwa->query_nazwa(TP, PL_MIA), "", "") + "\n", 5);

    if(obiekty[PARSE_BRON] != TO)
        return 0;

    if(cieciwa)
        if(obiekty[PARSE_CIECIWA] != cieciwa)
            return NF2("Na co chcesz to " + komendy_ladowania[verb][KOMENDY_BEZOKOL] + "?\n",5);

    if(pocisk)
        return NF2("Nie mo�esz tego " + komendy_ladowania[verb][KOMENDY_BEZOKOL] + ". Przeszkadza " +
            "Ci w tym " + pocisk->short(TP, PL_BIE) + ".\n", 5);

    if(query_wielded() != TP && (query_typ() == W_L_BOW || query_typ() == W_L_SLING))
        return NF2("Najpierw musisz doby� " + short(TP, PL_DOP) + ".\n", 5);

    zaladuj_pocisk_do_broni(obiekty[PARSE_POCISK]);

    write(capitalize(komendy_ladowania[verb][KOMENDY_1OS]) + " " +
        bsprintf(patt, TO->short(TP, komendy_ladowania[verb][KOMENDY_PRZYPADKI][PARSE_BRON]),
        (query_cieciwa() ? query_cieciwa()->short(TP, komendy_ladowania[verb][KOMENDY_PRZYPADKI][PARSE_CIECIWA]) : ""),
        pocisk->short(TP, komendy_ladowania[verb][KOMENDY_PRZYPADKI][PARSE_POCISK]), "") + ".\n");

    saybb(QCIMIE(TP, PL_MIA) + " " + komendy_ladowania[verb][KOMENDY_2OS] + " " +
        bsprintf(patt, QSHORT(TO, komendy_ladowania[verb][KOMENDY_PRZYPADKI][PARSE_BRON]),
        (query_cieciwa() ? QSHORT(query_cieciwa(), komendy_ladowania[verb][KOMENDY_PRZYPADKI][PARSE_CIECIWA]) : ""),
        QSHORT(pocisk, komendy_ladowania[verb][KOMENDY_PRZYPADKI][PARSE_POCISK]), "") + ".\n");

    return 1;
}

public int naciagnij(string arg, int spc)
{
    string verb;

    if(!spc)
        verb = query_verb();
    else
        verb = arg;

    DBG("verb = " + verb + "\n");
    if(!spc)
    {
        //Sprawdzamy czy komenda istnieje.
        if(!is_komenda_naciagania(verb))
            return 0;

        NF("Co takiego chcesz " + komendy_naciagania[verb][KOMENDY_BEZOKOL] + "?\n");

        if(!arg)
            return 0;

        string patt = zamien_na_patt(komendy_naciagania[verb][KOMENDY_WZOR]) || "%b:" + PL_DOP;

        mixed obiekty = sparsuj_komende(arg, patt, all_inventory(TP), verb);

        if(!pointerp(obiekty))
        {
            return NF(capitalize(verb) + " " +
                bsprintf(komendy_naciagania[verb][KOMENDY_WZOR], "czego", "", "", "") + "?\n");
        }

        if(intp(obiekty) && obiekty == -1)
        {
            return NF(capitalize(verb) + " " +
                bsprintf(komendy_naciagania[verb][KOMENDY_WZOR], "czego", "", "", "") + "?\n");
        }

        if(!objectp(obiekty[PARSE_BRON]))
        {
            return NF(capitalize(verb) + " " +
                bsprintf(komendy_naciagania[verb][KOMENDY_WZOR], "czego", "", "", "") + "?\n");
        }

        if(obiekty[PARSE_BRON] != TO)
        {
            return NF(capitalize(verb) + " " +
                bsprintf(komendy_naciagania[verb][KOMENDY_WZOR], "czego", "", "", "") + "?\n");
        }

        if(obiekty[PARSE_BRON]->query_wielded() != TP)
            return NF("Najpierw musisz doby� " + TO->short(TP, PL_DOP) + ".\n");
    }

    int sn = F_BS_SILA_NACIAGU(TP->query_stat(SS_STR),
        TP->query_stat(SS_DEX), TP->query_skill(THIS_LW_SKILL),
        TP->query_skill(LW_SKILL), potrzebna_sila, potrzebna_zrecznosc,
        potrzebny_um);

    int czas_n = F_BS_CZAS_NACIAGANIA(sn);
    int max_czasn = F_BS_MAX_CZAS_NACIAGANIA(sn, TP->query_old_fatigue());

    DBG("si�a naci�gu = " + sn + ", czas naci�gu = " + czas_n + ", maksymalny czas naci�gania wynosi = " + max_czasn);
    DBG("zm�czenie w intervale = " + F_BS_ZMECZENIE_W_INTERVALE(sn) + ", fat = " + TP->query_old_fatigue());

    if(!F_BS_DA_RADE_NACIAGNAC(sn))
    {
        return NF("Nie dasz rady " + komendy_naciagania[verb][KOMENDY_BEZOKOL] + " " +
            bsprintf(komendy_naciagania[verb][KOMENDY_WZOR],
            TO->short(TP, komendy_naciagania[verb][KOMENDY_PRZYPADKI][PARSE_BRON]), "", "", "") + ".\n");
    }

    write(capitalize(naciaganie_kom(verb, 0)) + " " +
        bsprintf(komendy_naciagania[verb][KOMENDY_WZOR],
        TO->short(TP, komendy_naciagania[verb][KOMENDY_PRZYPADKI][PARSE_BRON]),
        (query_cieciwa() ? query_cieciwa()->short(TP, komendy_naciagania[verb][KOMENDY_PRZYPADKI][PARSE_CIECIWA]) : ""),
        "", "") + ".\n");

    saybb(QCIMIE(TP, PL_MIA) + " " + naciaganie_kom(verb, 1) + " " +
        bsprintf(komendy_naciagania[verb][KOMENDY_WZOR],
        QSHORT(TO, komendy_naciagania[verb][KOMENDY_PRZYPADKI][PARSE_BRON]),
        (query_cieciwa() ? QSHORT(query_cieciwa(), komendy_naciagania[verb][KOMENDY_PRZYPADKI][PARSE_CIECIWA]) : ""),
        "", "") + ".\n");

    object paraliz = clone_object("/std/paralyze.c");
    paraliz->set_name("paraliz_naciagania");
    paraliz->set_fail_message("W tej chwili naci�gasz " + TO->short(TP, PL_BIE) + ". Przesta� "+
        "aby zrobi� co� innego.\n");
    paraliz->set_stop_message("");
    paraliz->set_stop_fun("stop_naciagania");
    paraliz->set_stop_verb("przesta�");
    paraliz->set_stop_object(TO);

    alarm_naciagania = set_alarm(itof(czas_n), 0.0, &bron_naciagnieta(sn, verb, TP));

    if(query_typ() < W_L_CROSSBOW)
    {
        paraliz->set_remove_time(max_czasn);
        paraliz->set_finish_fun("koniec_naciagania");
        paraliz->set_finish_object(TO);
        paraliz->set_finish_message("");
    }

    //Zezwalamy na wszystkie komendy strza�u i celowania
    map(({"wyceluj", "celuj"}) + (m_sizeof(komendy_strzelania) ? m_indexes(komendy_strzelania) : ({})), &(paraliz)->add_allowed());

    paraliz->move(TP, 1);

    paraliz_naciagania = paraliz;

    return 1;
}

public int
stop_naciagania()
{
    object player = ENV(TO);

    //Je�li nie ma �adnej komedny to zako�czymy bez komuniaktu
    if(!m_sizeof(komendy_naciagania))
        return 1;

    string verb = m_indexes(komendy_naciagania)[0];

    bs_hook_zaprzestanie_naciagania(player);

    DBG("zm�czenie naci�ganiem = " + F_BS_ZMECZENIE_NACIAGANIEM(sila_naciagu, (query_czas_naciagu() - query_a_czas_naciagu())));

    player->add_old_fatigue(-F_BS_ZMECZENIE_NACIAGANIEM(sila_naciagu, (query_czas_naciagu() - query_a_czas_naciagu())));

    sila_naciagu = 0;
    paraliz_naciagania->remove_object();
    remove_alarm(alarm_naciagania);

    return 1;
}

public int
koniec_naciagania()
{
    object player = ENV(TO);
    bs_hook_koniec_naciagania(player);

    sila_naciagu = 0;
    usun_cel();

    //Troche mu zostawimy, ale nie du�o
    player->set_old_fatigue(5);
    dump_array(player);
    DBG("Zm�czyli�my go ca�kiem, a co?\n");

    paraliz_naciagania->remove_object();

    return 1;
}

public int strzel(string arg)
{
    int kwp;
    string verb = query_verb();

    //Sprawdzamy czy komenda istnieje.
    if(!is_komenda_strzelania(verb))
        return 0;

    NF("Co takiego chcesz " + komendy_strzelania[verb][KOMENDY_BEZOKOL] + "?\n");

    if(!arg)
    {
        if(komendy_strzelania[verb][KOMENDY_WZOR] && komendy_strzelania[verb][KOMENDY_WZOR] != "")
            return 0;
        else
            kwp = 1;
    }

    if(!query_sila_naciagu())
    {
        string nv = m_indexes(komendy_naciagania)[0];
        return NF("Najpierw musisz " + komendy_naciagania[nv][KOMENDY_BEZOKOL] + " " +
            bsprintf(komendy_naciagania[nv][KOMENDY_WZOR],
            short(TP, komendy_naciagania[nv][KOMENDY_PRZYPADKI][PARSE_BRON]),
            (query_cieciwa()->short(TP, komendy_naciagania[nv][KOMENDY_PRZYPADKI][PARSE_CIECIWA]) ?: ""),
            "", "") + "!\n");
    }

    if(!query_cel() && !query_env_cel())
        return NF("Najpierw w kogo� lub co� wyceluj.\n");

    if(!kwp)
    {
        string patt = zamien_na_patt(komendy_strzelania[verb][KOMENDY_WZOR]) || "%b:" + PL_DOP;

        mixed obiekty = sparsuj_komende(arg, patt, all_inventory(TP), verb);

        if(!pointerp(obiekty))
        {
            switch(-(--obiekty))
            {
                case PARSE_CIECIWA:
                    return NF("Nie mo�esz " + komendy_strzelania[verb][KOMENDY_BEZOKOL] + " " +
                        bsprintf(komendy_strzelania[verb][KOMENDY_WZOR],
                        " ten " + TO->query_nazwa(komendy_strzelania[verb][KOMENDY_PRZYPADKI][PARSE_BRON]) +
                        "ma tylko jedn�",
                        query_cieciwa()->query_nazwa(komendy_strzelania[verb][KOMENDY_PRZYPADKI][PARSE_CIECIWA]), "", "") +
                        "!\n");
                case PARSE_BRON:
                    return NF("Nie mo�esz " + komendy_strzelania[verb][KOMENDY_BEZOKOL] + " " +
                        bsprintf(komendy_strzelania[verb][KOMENDY_WZOR],
                        "kilku " + TO->query_nazwa(komendy_strzelania[verb][KOMENDY_PRZYPADKI][PARSE_BRON]) +
                        "r�wnocze�nie",
                        query_cieciwa()->query_pnazwa(komendy_strzelania[verb][KOMENDY_PRZYPADKI][PARSE_CIECIWA]), "", "") +
                        ".\n");
                default:
                    int n = -(--obiekty);
                    notify_wizards(TO, "Bro� strzelecka informuje, �e nie przewidziano " +
                        "u�ycia typu" + n + " do komendy strzelania. Prosz� o sprawdzenie pliku " + MASTER_OB(TO) +
                        ", je�li jest to zamierzone u�ycie prosz� o kontakt - KRUN");
                    return NF("Wyst�pi� b��d numer bs2. Zg�o� go opisuj�c okoliczno�ci " +
                        "w jakich do niego dosz�o. Szczeg�ln� uwag� zwr�� na komend�, " +
                        "kt�ra go wywo�a�a.\n");
            }
        }

        if(!objectp(obiekty[PARSE_BRON]))
            return 0;

        if(TO != obiekty[PARSE_BRON])
            return 0;

        if(!objectp(obiekty[PARSE_CIECIWA]))
            return 0;

        if(obiekty[PARSE_CIECIWA] != query_cieciwa())
            return 0;
    }

    if(!query_pocisk() && !kwp)
    {
        write(capitalize(komendy_strzelania[verb][KOMENDY_1OS]) + " " +
            bsprintf(komendy_strzelania[verb][KOMENDY_WZOR], TO->short(TP,
            komendy_strzelania[verb][KOMENDY_PRZYPADKI][PARSE_BRON]), "", "", "") + ".\n");
        tell_roombb(ENV(TO), QCIMIE(TP, PL_MIA) + " " + komendy_strzelania[verb][KOMENDY_2OS] + " " +
            bsprintf(komendy_strzelania[verb][KOMENDY_WZOR], QSHORT(TO,
            komendy_strzelania[verb][KOMENDY_PRZYPADKI][PARSE_BRON]), "", "", "") + ".\n", ({TP}));

        sila_naciagu = 0;
        paraliz_naciagania->remove_object();

        return 1;
    }
    else if(!query_pocisk())
        return NF("Nie mo�esz strzeli� bez odpowiedniego pocisku.\n");

    //A teraz posprawdzamy sobie umy, polosujemy.. Bo przecie zawsze mo�na trafi� kogo� obok naszej ofiary. A czemu nie?
    int tmp = F_BS_CZY_TRAFI(TP->query_stat(SS_DEX), TP->query_skill(THIS_LW_SKILL), TP->query_skill(LW_SKILL),
        TO->query_celnosc(), (env_cel == ENV(TP)), mod_celnosci);

    DBG("czy trafi = " + tmp + "\n");

    int ntc; //nie ten cel

    object mis_cel;
    if(!tmp)
    {   //Mo�e nawet trafimy! Ale na pewno nie w to co chcieli�my.
        switch(random(100))
        {
            case 0..49: //50% szansy na to, �e nie trafimy nikogo.
                if(tmp == -2)
                    mis_cel = cel;
                else
                    mis_cel = (random(3) ? 0 :
                        (random(4) ? wylosuj_cel_z_lokacji(ENV(cel), TP) : wylosuj_cel_z_lokacji(ENV(TP), TP)));
                cel = 0;
                env_cel = 0;
                break;

            case 50..80: //30% na to, �e trafimy w kogo� na lokacji na kt�r� celujemy
                mis_cel = cel;
                cel = wylosuj_cel_z_lokacji(env_cel, TP);
                break;

            default:    //20% sznasy na to, �e trafimy w cel na naszej lokacji
                mis_cel = cel;
                env_cel = ENV(TP);
                cel = wylosuj_cel_z_lokacji(env_cel, TP);
                break;
        }

        tmp = -tmp;
        ntc = 1;
    }

    TP->add_old_fatigue(-F_BS_ZMECZENIE_NACIAGANIEM(sila_naciagu, (query_czas_naciagu() - query_a_czas_naciagu())));

    if(!cel)
    { // Nie trafimy w nic... Ale mo�emy komu� �adny komunikat wy�wietli�.
        if(mis_cel)
        {   //Wy�wietlimy komu� komunikat, �e strza�a przelecia�a mu tu� obok nosa:)
            mis_cel->catch_msg("Tu� obok ciebie przelecia�" +
                pocisk->koncowka("", "a", "o") + " " + pocisk->query_nazwa(PL_MIA) +
                (ENV(mis_cel) == ENV(TP) ? " wystrzelona przez " + TP->short(mis_cel, PL_BIE) : "") +
                ".\n");
            write("Wystrzelon" + pocisk->koncowka("y", "a", "e") + " przez ciebie " + pocisk->query_nazwa(PL_MIA) +
                (ENV(mis_cel) == ENV(TP) ? " przelatuje tu� obok " + mis_cel->short(TP, PL_DOP) + " i" : "") +
                " odlatuje w sin� dal.\n");
            if(ENV(mis_cel) != ENV(TP))
            {
                tell_roombb(ENV(mis_cel), "Jak" + pocisk->koncowka("i�", "a�", "e�") + " " +
                    pocisk->query_nazwa(PL_MIA) + " przelatuje tu� obok " + QIORS(mis_cel, PL_DOP) + " i odlatuje w sin� dal.\n",
                    mis_cel);
                tell_roombb(ENV(TP), "Wystrzelon" + pocisk->koncowka("y", "a", "e") + " " +
                    "przez " + QIMIE(TP, PL_BIE) + " " + pocisk->query_nazwa(PL_MIA) + " odlatuje w sin� dal.\n", TP);
            }
            else
                saybb("Wystrzelon" + pocisk->koncowka("y", "a", "e") + " przez " + QIMIE(TP, PL_BIE) + " " +
                    pocisk->query_nazwa(PL_MIA) + " przelatuje tu� obok " + QIORS(mis_cel, PL_DOP) + " i odlatuje "+
                    "w sin� dal.\n", ({mis_cel, TP}));
        }
        else
        {
            write("Wystrzelon" + pocisk->koncowka("y", "a", "e") + " przez ciebie " + pocisk->query_nazwa(PL_MIA) +
                " nie trafia nikogo i odlatuje w sin� dal.\n");
            saybb("Wystrzelon" + pocisk->koncowka("y", "a", "e") + " przez " + QCIMIE(TP, PL_BIE) + " " +
                pocisk->query_nazwa(PL_MIA) + " nie trafia nikogo i odlatuje w sin� dal.\n");
        }
    }
    else
    {   //Huurrrrrrrrrrrrrrraaaaaaaaaaaaaaaa trafili�my! ... tylko czy tam gdzie chcieli�my?:P

        int s = F_BS_SILA_STRZALU(TP->query_skill(THIS_LW_SKILL), TP->query_skill(LW_SKILL),
            TP->query_stat(SS_DEX), query_sila_naciagu(), pocisk->query_jakosc(), query_sila_razenia());
        int sz = F_BS_SZYBKOSC_STRZALU(TP->query_skill(THIS_LW_SKILL), pocisk->query_jakosc(),
            (pocisk->query_prop(OBJ_I_WEIGHT)/max(1,pocisk->query_prop(OBJ_I_VOLUME))),
            TO->query_sila_razenia(), query_sila_naciagu());

        DBG("Si�a strza�u = " + s + ", szybko�� pocisku = " + sz + "\n");

            //Argumenty: strzelaj�cy, pocisk, bron, si�a, szybko��
        if(!cel->bs_hit_me(TP, pocisk, TO, s, sz, query_sila_naciagu()))
            pocisk->remove_object();

        sila_naciagu = 0;
        usun_cel();
        paraliz_naciagania->remove_object();
    }

    return 1;
}

public int przestan(string arg)
{
   NF("Czego chcesz zaprzesta�?\n");

    if(!(arg ~= "celowa�"))
        return 0;

    if(!cel)
        return NF2("Nie celujesz do nikogo.\n", 3);

    write("Przestajesz celowa� do " + cel->short(TP, PL_DOP) + ".\n");
    saybb(QCIMIE(TP, PL_MIA) + " przestaje celowa� "+
        (objectp(env_cel) ? "do " + QIMIE(cel, PL_DOP) : zamien_kierunek(kierunek, 1)) + ".\n", ({TP, cel}));

    if(interactive(cel) && env_cel)
    {
        int z = F_BS_ZOBACZY_ZE_CELUJA_DO_NIEGO(cel->query_skill(SS_AWARENESS));

        if(z == 1)
            cel->catch_msg("Wra�enie, �e kto� do ciebie celuje znikne�o.\n");
        else if(z == 2)
            cel->catch_msg(koloruj("Wydaje ci si�, �e ju� nikt do ciebie nie celuje.\n", cel, COLOR_FG_GREEN));
        else if(z == 3)
        {
            cel->catch_msg(koloruj(UC(TP->short(cel, PL_MIA)) + " przesta� do ciebie celowa�.\n",
                cel, COLOR_FG_GREEN, COLOR_BOLD_ON));
        }
    }
    else if(interactive(cel))
    {
        cel->catch_msg(koloruj(UC(TP->short(cel, PL_MIA)) + " przesta� do ciebie celowa�.\n",
            cel, COLOR_FG_GREEN, COLOR_BOLD_ON));
    }
    usun_cel();
    return 1;
}

void
init()
{
    ::init();

    //Jako, �e celowanie ma zawsze takie same komendy, dodajemy na sztywno.
    add_action(&wyceluj(),      "celuj");
    add_action(&wyceluj(),      "wyceluj");
    //Komenda pozwalaj�ca sprawdzi� aktualny cel.
    add_action(&sprawdz_cel(),  "cel");
    add_action(&przestan(),     "przesta�");

    add_action(&help(),         "?", 2);
    add_action(&help(),         "pomoc");

    //A teraz komendy dowawane dynamicznie..

    foreach(string cmd : m_indexes(komendy_ladowania))
        add_action(&zaladuj(), cmd);

    foreach(string cmd : m_indexes(komendy_naciagania))
        add_action(&naciagnij(), cmd);

    foreach(string cmd : m_indexes(komendy_strzelania))
        add_action(&strzel(), cmd);
}

/** *********************************************************************** **
 ** *****************       O  B  S  �  U  G  A        ******************** **
 ** *********************************************************************** **/

/**
 * Funkcja w patternie wyszukuje przypadki dla poszczeg�lnych obiekt�w,
 * i zwraca je w kolejno�ci: bro�, cel, pocisk, naci�garka
 * @param patt pattern do przeszukania.
 *zamien_kierunek
 * @return wszystkie przypadki wyst�puj�ce w obiekcie lub -1 je�li przypadek
 * @return do danego obiektu nie istnieje.
 */
//private static
int *find_przypadki_in_patt(string patt)
{
//     int *przyp = allocate(4, -1); FIXME: jak zmienie w driverze to tu te� musze (KRUN)
    int *przyp = ({-1, -1, -1, -1});

    string *tmp = explode(patt, ":");

    for(int i=1;i<sizeof(tmp);i++)
    {
        if(intp(tmp[i][0]) && tmp[i-1][-2] == '%')
        {
            switch(tmp[i-1][-1])
            {
                case 'b': przyp[PARSE_BRON] =       atoi(tmp[i][0..0]); break;
                case 'c': przyp[PARSE_CIECIWA] =    atoi(tmp[i][0..0]); break;
                case 'p': przyp[PARSE_POCISK] =     atoi(tmp[i][0..0]); break;
                case 'n': przyp[PARSE_NACIAGACZ] =  atoi(tmp[i][0..0]); break;
            }
        }
    }

    return przyp;
}

/**
 * A ta funkcja pomaga w zamianie patterna bez "'" na pattern z "'".
 *
 * @param patt pattern do przerobienia.
 *
 * @return odpowiednio przeformatowanego stringa.
 */
private static string zamien_na_patt(string patt)
{
    string ret = "";
    string *tmp = explode(implode(explode(patt + ",", ","), " , ") + " ", " ");

    for(int i=0;i<sizeof(tmp);i++)
    {
        if(tmp[i] == "" || !tmp[i])
            continue;

        if(i!=0 && tmp[i][0] != ',')
            ret += " ";

        if(tmp[i][0] != '%' && tmp[i][0] != '/' && tmp[i][0] != '[' && tmp[i][0] != '\'' && tmp[i][0] != ',')
            ret += "'" + tmp[i] + "'";
        else
            ret += tmp[i];
    }
    return ret;
}

/**
 * Zamieniamy pattern na taki odpowiedni do sprintfa.
 *
 * @param patt pattern do zamiany
 *
 * @return zamieniony pattern.
 */
private static string zamien_na_bpatt(string patt)
{
    string *exp;

    exp = explode(patt, " / ");

    if(sizeof(exp) > 1)
    {
        patt = "";
        for(int i=0;i<sizeof(exp);i+=2)
            patt = exp[i] + " " + implode(explode(exp[(i+1)], " ")[1..], " ");
    }

    exp = explode(implode(explode(implode(explode(implode(explode(patt, "'"), ""), "]"), ""), "["), ""), "%");

    for(int i=1; i < sizeof(exp); i++)
        if(exp[i])
            exp[i] = "s" + (exp[i][1] == ':' ? exp[i][3..] : exp[i][1..]);

     return implode(exp, "%");
}

/**
 * Zamieniamy pattern na taki odpowiedni do parse_command.
 *
 * @param patt pattern do zamiany
 *
 * @return zamieniony pattern
 */
private static string zamien_na_ppatt(string patt)
{
    string *exp = explode(patt, "%");

    for(int i=1; i < sizeof(exp); i++)
        if(exp[i])
            exp[i] = "i" + exp[i][1..];

    return implode(exp, "%");
}

/**
 * Dzi�ki tej funkcji �adnie kolorujemy komunikaty wy�wietlane graczwi.
 *
 * @param arg tekst do pokolorowania
 * @param fob obiekt dla kt�rego kolorujemy(opcjonalnie)
 * @param color kolor na jaki kolorujemy
 * @param ext extra dodatki(opcjonalnie)
 * @return pokolorowany string
 */
private static varargs string koloruj(string arg, mixed fob, int color, int ext=0)
{
    if(fob == -1)
        return SET_COLOR1(color, ext) + arg + CLEAR_COLOR;
    else if(!color)
        return set_color(fob, color) + arg + clear_color(fob);

    return set_color(fob, color, ext) + arg + clear_color(fob);
}

/**
 * Zamieniamy kierunek.
 */
varargs string zamien_kierunek(string k, int i=0)
{
    if(!k)
        return "";

    if(i == 0)
    {
        switch(plain_string(k))
        {
            case "u":
            case "gora":
                return "g�ra";

            case "d":
            case "dol":
                return "d�";

            case "n":
            case "polnoc":
                return "p�noc";

            case "s":
            case "poludnie":
                return "po�udnie";

            case "e":
            case "wschod":
                return "wsch�d";

            case "w":
            case "zachod":
                return "zach�d";

            case "se":
            case "poludniowy-wschod":
            case "poludniowy wschod":
                return "po�udniowy-wsch�d";

            case "sw":
            case "poludniowy-zachod":
            case "poludniowy zachod":
                return "po�udniowy-zach�d";

            case "ne":
            case "polnocny-wschod":
            case "polnocny wschod":
                return "p�nocny-wsch�d";

            case "nw":
            case "polnocny-zachod":
            case "polnocny zachod":
                return "p�nocny-zach�d";

            case "przod":   return "prz�d";
            case "tyl":     return "ty�";
            case "wyjscie": return "wyj�cie";

            default: return k;
        }
    }
    else if(i == 1)
    {
        switch(plain_string(k))
        {
            case "u":
            case "gora":
                return "na g�rze";

            case "d":
            case "dol":
                return "na dole";

            case "n":
            case "polnoc":
                return "na p�nocy";

            case "s":
            case "poludnie":
                return "na po�udniu";

            case "e":
            case "wschod":
                return "na wschodzie";

            case "w":
            case "zachod":
                return "na zachodzie";

            case "se":
            case "poludniowy-wschod":
            case "poludniowy wschod":
                return "na po�udniowym-wschodzie";

            case "sw":
            case "poludniowy-zachod":
            case "poludniowy zachod":
                return "na po�udniowym-zachodzie";

            case "ne":
            case "polnocny-wschod":
            case "polnocny wschod":
                return "na p�nocnym-wschodzie";

            case "nw":
            case "polnocny-zachod":
            case "polnocny zachod":
                return "na p�nocnym-zachodzie";

            case "przod":   return "z przodu";
            case "tyl":     return "z ty�u";
            case "lewo":    return "z lewej";
            case "prawo":   return "z prawej";
            case "wyj�cie": return "za wyj�ciem";
            default:        return k;
        }
    }
    else if(i == 2)
    {
        switch(plain_string(k))
        {
            case "u":
            case "gora":
                return "z g�ry";

            case "d":
            case "dol":
                return "z do�u";

            case "n":
            case "polnoc":
                return "z p�nocy";

            case "s":
            case "poludnie":
                return "z po�udnia";

            case "e":
            case "wschod":
                return "ze wschodu";

            case "w":
            case "zachod":
                return "z zachodu";

            case "se":
            case "poludniowy-wschod":
            case "poludniowy wschod":
                return "z po�udniowego-wschodu";

            case "sw":
            case "poludniowy-zachod":
            case "poludniowy zachod":
                return "z po�udniowego-zachodu";

            case "ne":
            case "polnocny-wschod":
            case "polnocny wschod":
                return "z p�nocnego-wschodu";

            case "nw":
            case "polnocny-zachod":
            case "polnocny zachod":
                return "z p�nocnego-zachodu";

            case "przod":   return "z przodu";
            case "tyl":     return "z ty�u";
            case "lewo":    return "z lewej";
            case "prawo":   return "z prawej";
            case "wyj�cie": return "od strony wyj�cia";
            default:        return "zza " + k;
        }
    }
    else if(i == 4)
    {
        switch(plain_string(k))
        {
            case "u":
            case "gora":
                return "na g�r�";

            case "d":
            case "dol":
                return "na d�";

            case "n":
            case "polnocy":
                return "na p�noc";

            case "s":
            case "poludniu":
                return "na po�udnie";

            case "e":
            case "wschodzie":
                return "na wsch�d";

            case "w":
            case "zachodzie":
                return "na zach�d";

            case "se":
            case "poludniowym-wschodzie":
            case "poludniowym wschodzie":
                return "na po�udniowy-wsch�d";

            case "sw":
            case "poludniowym-zachodzie":
            case "poludniowym zachodzie":
                return "na po�udniowy-zach�d";

            case "ne":
            case "polnocnym-wschodzie":
            case "polnocnym wschodzie":
                return "na p�nocny-wsch�d";

            case "nw":
            case "polnocnym-zachodzie":
            case "polnocnym zachodzie":
                return "na p�nocny-zach�d";

            case "przod":   return "z przodu";
            case "tyl":     return "z ty�u";
            case "prawa":   return "z prawej";
            case "lewa":    return "z lewej";
            default:        return "za " + k;
        }
    }
    else if(i == 5)
    {switch(plain_string(k))
        {
            case "u":
            case "gore":
                return "g�ra";

            case "d":
            case "dol":
                return "d�";

            case "n":
            case "polnocy":
                return "p�noc";

            case "s":
            case "poludniu":
                return "po�udnie";

            case "e":
            case "wschodzie":
                return "wsch�d";

            case "w":
            case "zachodzie":
                return "zach�d";

            case "se":
            case "poludniowym-wschodzie":
            case "poludniowym wschodzie":
                return "po�udniowy-wsch�d";

            case "sw":
            case "poludniowym-zachodzie":
            case "poludniowym zachodzie":
                return "po�udniowy-zach�d";

            case "ne":
            case "polnocnym-wschodzie":
            case "polnocnym wschodzie":
                return "p�nocny-wsch�d";

            case "nw":
            case "polnocnym-zachodzie":
            case "polnocnym zachodzie":
                return "p�nocny-zach�d";

            case "przodu":      return "prz�d";
            case "tylu":        return "ty�";
            case "prawa":       return "prawo";
            case "lewa":        return "lewo";
            default:            return k;
        }
    }
}

/**
 * Funkcja prywatna zamieniaj�ca kierunek na kierunek odwrotny.
 * @param k zamieniany kierunek
 * @return odwrotny kierunek.
 */
private static string zamien_kierunek_na_odwrotny(string k)
{
    switch(plain_string(k))
    {
        case "polnoc":      case "n":       return "z po�udnia";
        case "poludnie":    case "s":       return "z p�nocy";
        case "wschod":      case "e":       return "z zachodu";
        case "zachod":      case "w":       return "ze wschodu";
        case "dol":         case "d":       return "z g�ry";
        case "gora":        case "u":       return "z do�u";
        case "przod":                       return "z ty�u";
        case "tyl":                         return "z przodu";
        case "lewo":                        return "z prawej";
        case "prawo":                       return "z lewej";
        case "wyj�cie":                     return "z fixme"; //FIXME!

        case "ne":
        case "polnocny-wschod":
        case "polnocny wschod":
            return "z po�udniowego-zachodu";

        case "se":
        case "poludniowy-wschod":
        case "poludniowy wschod":
            return "z p�nocnego-zachodu";

        case "nw":
        case "polnocny-zachod":
        case "polnocny zachod":
            return "z po�udniowego-wschodu";

        case "sw":
        case "poludniowy-zachod":
        case "poludniowy zachod":
            return "z poludniowego-zachodu";

        default:    return "z FIXME"; //FIXME
    }
}

private static object find_other_room(string na_kierunek)
{
    string *cmds = ENV(TP)->query_exit_cmds();

    int i = member_array(zamien_kierunek(na_kierunek, 5), cmds);

    if(i == -1)
        i = member_array(implode(explode(zamien_kierunek(na_kierunek, 5), " "), "-"), cmds);

    if(i == -1)
        i = member_array(zamien_kierunek(na_kierunek, 1), cmds);

    if(i == -1)
        i = member_array(implode(explode(zamien_kierunek(na_kierunek, 1), " "), "-"), cmds);

    return find_object(ENV(TP) -> query_exit_rooms()[i]);
}

/**
 * Funkcja wy�wietlaj�ca komunikaty.
 *
 * @param patt - wz�r na kt�ry nak�adamy tekst.
 * @param ob - opis broni.
 * @param oc - opis celu.
 * @param op - opis pocisku.
 * @param on - opis naci�garki.
 *
 * @return przeformatowany tekst.
 */
private static string bsprintf(string patt, string ob, string oc, string op, string on, string ok="")
{
    //Najpierw musimy zamieni� patt na normalny wz�r.
    string bpatt = zamien_na_bpatt(patt);

    string *miejsca = map(explode(patt, "%")[1..], &operator([])(,0));

    string *arr = allocate(5);

    int i;
    if((i = member_array('b', miejsca)) >= 0)
        arr[i] = ob;

    if((i = member_array('c', miejsca)) >= 0)
        arr[i] = oc;

    if((i = member_array('p', miejsca)) >= 0)
        arr[i] = op;

    if((i = member_array('n', miejsca)) >= 0)
        arr[i] = on;

    if((i = member_array('k', miejsca)) >= 0)
        arr[i] = ok;

    string ret = sprintf(bpatt, arr[0], arr[1], arr[2], arr[3], arr[4]);

    return ret;
}

public varargs string sprawdz_koncowki(string str, object ob)
{
    string mes, zen, nij, mm, mn, przed, po;

    if(sscanf(str, "%s[%s|%s|%s|%s|%s]%s", przed, mes, zen, nij, mm, mn, po) != 7)
        return str;

    string *exp = explode(mn, "#");
    mn = exp[0];

    if(sizeof(exp) > 1 && find_object(exp[1]))
        ob = find_object(exp[1]);

    if(!ob || !objectp(ob))
        ob = TO;

    str = przed + ob->koncowka(mes, zen, nij, mm, mn) + po;

    str = sprawdz_koncowki(str);

    return str;
}

/**
 * Funkcja lokalna parsuj�ca obiektu wed�ug podanego wzoru.
 *
 * @param arg string pobrany od gracza
 * @param patt wz�r w formie opisanej w <i>man bronie strzeleckie</i>
 * @param env �rodowisko w jakim szukamy obiektu, lub obiekty w�r�d kt�rych szukamy.
 * @param verb komenda przez kt�r� szukamy.
 *
 * @return <i>0</i> je�li niepowodzenie
 * @return <i>({})</i> argument�w je�li powodzenie przy czym
 * @return <i>[0]</i> - u�ywana bro�
 * @return <i>[1]</i> - cel
 * @return <i>[2]</i> - pocisk
 * @return <i>[3]</i> - naci�garka(np. korba do ci�kiej kuszy)
 */
private static varargs mixed sparsuj_komende(string arg, string patt, mixed env, string verb)
{
    if(!arg)
        return 0;

    if(!patt)
        return ({0, 0, 0, 0});

    string *miejsca = map(explode(patt, "%")[1..], &operator([])(,0));

    string ppatt = zamien_na_ppatt(patt);

    mixed ob = allocate(4);

    //Sortujemy coby ci�ciwa z tej broni by�a najpierwsza
    object *sr;
    object *sorted_items = all_inventory(TP);
    sr = filter(sorted_items, &operator(==)(,cieciwa));

    sorted_items = sr + (sorted_items - sr);
    if(!parse_command(arg, sorted_items, ppatt, ob[0], ob[1], ob[2], ob[3]))
        return 0;

    if(ob[0])
    {
        ob[0] = NORMAL_ACCESS(ob[0], 0, 0);

        if(!sizeof(ob[0]))
            return 0;

        if(sizeof(ob[0]) > 1)
            return -1;
    }

    if(ob[1])
    {
        ob[1] = NORMAL_ACCESS(ob[1], 0, 0);

        if(!sizeof(ob[1]))
            return 0;

        if(sizeof(ob[1]) > 1)
            return -2;
    }

    if(ob[2])
    {
        ob[2] = NORMAL_ACCESS(ob[2], 0, 0);

        if(!sizeof(ob[2]))
            return 0;

        if(sizeof(ob[2]) > 1)
            return -3;
    }

    if(ob[3])
    {
        ob[3] = NORMAL_ACCESS(ob[3], 0, 0);

        if(!sizeof(ob[3]))
            return 0;

        if(sizeof(ob[3]) > 1)
            return -4;
    }

    object *ret = allocate(4);

    int i;
    if((i = member_array('b', miejsca)) >= 0)
        ret[0] = ob[i][0];

    if((i = member_array('c', miejsca)) >= 0)
        ret[1] = ob[i][0];

    if((i = member_array('p', miejsca)) >= 0)
        ret[2] = ob[i][0];

    if((i = member_array('n', miejsca)) >= 0)
        ret[3] = ob[i][0];

    return ret;
}

//A to funkcje wywo�ywane z zewn�trz.

#define INFO_COLOR  COLOR_FG_YELLOW

/**
 * Ta funkcja wywo�ywana jest gdy gracz zmienii miejsce przebywania.
 * Dzi�ki niej mo�emy sprawdzi� czy cel jest dalej w naszym zasi�gu.
 *
 * @param old stare �rodowisko
 * @param dest nowe �rodowisko
 * @param celujacy osoba celuj�ca
 *
 * @return <i>0</i> nie poruszy� si�.
 * @return <i>1</i> zosta� w polu widzenia
 * @return <i>2</i> cel znikno� z pola widzenia.
 */
public int cel_zmienil_pozycje(object old, object dest)
{
    int i;
    object celujacy = ENV(TO);
    //Najpierw musimy sprawdzi� czy objekt jest na kt�rej� z lokacji w zasi�gu.
    if(old == dest)
    {   //cel si� nie poruszy�.
        return 0;
    }
    else if(objectp(dest) && (i=member_array(MASTER_OB(dest), ENV(celujacy)->query_exit_rooms())) >= 0 && query_zasieg())
    {   //przechodzi ale zostaje w zasi�gu.
        env_cel = dest;

        //zmieniamy kierunek
        kierunek = ENV(celujacy)->query_exit_cmds()[i];

        celujacy->catch_msg(koloruj(capitalize(cel->short(celujacy, PL_MIA)) + " poruszy� si�, "+
            "ale pozostaje w zasi�gu two" + TO->koncowka("jego", "jej", "jego", "ich", "ich") + " " +
            TO->short(celujacy, PL_DOP) + ".\n", celujacy, INFO_COLOR));
    }
    else if(dest == ENV(celujacy))
    {   //przechodzi na lokacje na kt�rej stoi celuj�cy
        env_cel = 0;
        kierunek = 0;

        celujacy->catch_msg(koloruj(capitalize(cel->short(celujacy, PL_MIA)) + " poruszy� si�, "+
            "teraz stoi obok ciebie.\n", celujacy, INFO_COLOR));
    }
    else
    {   //poza zasi�giem.
        celujacy->catch_msg(koloruj(capitalize(cel->short(celujacy, PL_MIA)) + " poruszy� si� i znajduje si� "+
            "teraz poza zasi�giem two" + TO->koncowka("jego", "jej", "jego", "ich", "ich") + " " +
            TO->short(celujacy, PL_DOP) + ".\n", celujacy, INFO_COLOR));

        usun_cel();

        return 2;
    }

    return 1;
}

public int celujacy_zmienil_pozycje(object old, object dest, object cel)
{
    int i;
    object celujacy = ENV(TO);

    //Najpierw musimy sprawdzi� czy obiekt jest dalej w naszym zasi�gu.
    if(old == dest) //cel si� nie poruszy�.
        return 0;
    else if(objectp(dest) && dest == ENV(cel))
    {   //przyszli�my na lokacje z celem.
        env_cel = 0;
        kierunek = 0;

        celujacy->catch_msg(koloruj(capitalize(cel->short(celujacy, PL_MIA)) + " stoi teraz "+
            "obok ciebie.\n", celujacy, INFO_COLOR));
    }
    else if(objectp(dest) && (i = member_array(MASTER_OB(ENV(cel)), dest->query_exit_rooms())) >= 0)
    {   //przechodzimy ale cel pozostaje w zasi�gu.
        //zmieniamy kierunek
        kierunek = dest->query_exit_cmds()[i];

        celujacy->catch_msg(koloruj(capitalize(cel->short(celujacy, PL_MIA)) + " pozostaje w zasi�gu " +
            "two" + TO->koncowka("jego", "jej", "jego", "ich", "ich") + " " + TO->short(celujacy, PL_DOP) +
            ".\n", celujacy, INFO_COLOR));
    }
    else
    {   //poza zasi�giem.
        celujacy->catch_msg(koloruj(capitalize(cel->short(celujacy, PL_MIA)) + " znajduje si� "+
            "teraz poza zasi�giem two" + TO->koncowka("jego", "jej", "jego", "ich", "ich") + " " +
            TO->short(celujacy, PL_DOP) + ".\n", celujacy, INFO_COLOR));

        usun_cel();

        return 2;
    }

    return 1;
}

/**
 * Funkcja lokalna dodaj�ca shadow celowi.
 *
 * @param cel cel jak sama nazwa wskazuje
 * @param celujacy obiekt celujacy z tej broni
 */
private static void dodaj_shadow_celu(object cel, object celujacy=ENV(TO))
{
    object shd = clone_object(SHADOW_CELU);
    shd -> shadow_me(cel);
    cel -> ustaw_scl_celujacego(celujacy);
    cel -> ustaw_scl_bron(TO);
}

/**
 * Funkcja lokalna dodaj�ca shadow celuj�cemu z broni.
 *
 * @param cel obiekt b�d�cy celem
 * @param celujacy obiekt celuj�cy do <i>celu</i>
 */
private static void dodaj_shadow_celujacego(object cel, object celujacy=ENV(TO))
{
    object shd = clone_object(SHADOW_CELUJACEGO);
    shd -> shadow_me(celujacy);
    celujacy -> ustaw_sc_cel(cel);
    celujacy -> ustaw_sc_bron(TO);
}

private static void usun_cel()
{
    object c = ENV(TO);
    c->remove_shadow_celujacego();
    cel->remove_shadow_celu();

    cel = 0;
    env_cel = 0;
    kierunek = 0;
}

private static void wyladuj_pocisk_z_broni()
{
    pocisk->wyladuj_mnie_z_broni();
    pocisk = 0;
}

private static void zaladuj_pocisk_do_broni(object p)
{
    pocisk = p;

    pocisk->zaladuj_mnie_do_broni();
}

private static int policz_szanse(object ob)
{
    return ob->query_prop(OBJ_I_WEIGHT) / max(1, ob->query_prop(OBJ_I_VOLUME));
}

/**
 * Funkcja losuj�ca cel.
 */
private static object wylosuj_cel_z_lokacji(object l, object p)
{
    if(!l)
        return 0;

    object *lai = all_inventory(l) - ({p});

    int *laiv = map(lai, &policz_szanse());

    int rnd = random(fold(laiv, &operator(+)(,), 0));

    int i, wal;
    for(i = 0; (i < sizeof(laiv) && wal < rnd) ; i++)
        wal += laiv[i];

    return lai[sizeof(laiv) - i - 1];
}

/**
 * Funkcja odpowiadaj�ca za naci�gni�cie broni,
 * wywo�ywana z alarmu.
 *
 * @param sn si�a naci�gu
 * @param verb czasownik uzyty do wywo�ania
 * @param pl naci�gaj�cy
 */
private static void bron_naciagnieta(int sn, string verb, object pl = ENV(TO))
{
    if(query_typ() != W_L_SLING)
    {
        pl->catch_msg(capitalize(naciaganie_kom(verb, 2) + " " +
            bsprintf(komendy_naciagania[verb][KOMENDY_WZOR],
            TO->short(TP, komendy_naciagania[verb][KOMENDY_PRZYPADKI][PARSE_BRON]),
            (query_cieciwa()->short(TP, komendy_naciagania[verb][KOMENDY_PRZYPADKI][PARSE_CIECIWA]) ?: ""),
            "", "") + ".\n"));
        tell_roombb(ENV(pl), QCIMIE(pl, PL_MIA) + " " + naciaganie_kom(verb, 3) + " " +
            bsprintf(komendy_naciagania[verb][KOMENDY_WZOR],
            QSHORT(TO, komendy_naciagania[verb][KOMENDY_PRZYPADKI][PARSE_BRON]),
            (query_cieciwa() ? QSHORT(query_cieciwa(), komendy_naciagania[verb][KOMENDY_PRZYPADKI][PARSE_CIECIWA]) : ""),
            "", "") + ".\n", ({pl}));
    }
    else
        pl->catch_msg("Obr�ci�e� " + TO->short(pl, PL_NAR) + " wok� siebie i jeste� got�w do oddania strza�u.\n");

    sila_naciagu = sn;
}

/**
 * Funkcja zwracaj�ca odpowiedni komunikat w odpowiednim momencie.
 *
 * @param verb
 * @param ident
 */
private static string naciaganie_kom(string verb, int ident)
{
    DBG("verb = " + verb);
    int forwho = (ident % 2);
    string com = komendy_naciagania[verb][(forwho ? KOMENDY_2OS : KOMENDY_1OS)];

    string *exp = explode(com, "|");

    if(!sizeof(exp) || sizeof(exp) == 1)
        return com;

    return exp[ident / 2];
}

/**
 * Funkcja wywo�ywana kiedy obiekt opuszcza otoczenie.
 *
 * @param from opuszczane otoczenie
 * @param to �rodowisko do kt�rego wchodzimy.
 */
void
leave_env(object from, object to)
{
     ::leave_env(from, to);

     wyladuj_pocisk_z_broni();
     usun_cel();

     wearable_item_leave_env(from, to);
}

/**
 * Funkcja wywo�ywana jest kiedy obiekt wchodzi do nowego otoczenia.
 *
 * @param dest otoczenie do kt�rego przenosimy
 * @param old �rodowisko z kt�rego przenosimy.
 */
void
enter_env(object dest, object old)
{
    ::enter_env(dest, old);

    wearable_item_enter_env(dest, old);
}

/**
 * Funkcja sprawdzaj�ca czy obiekt jest widoczny.
 *
 * @param for_obj obiekt dla kt�rego sprawdzamy.
 */
public int
check_seen(object for_obj)
{
    if(!(::check_seen(for_obj)))
        return 0;

    return wearable_item_check_seen(for_obj);
}

/**
 * Funkcja pozwalaj�ca oceni� obiekt.
 *
 * @param num FIXME
 */
void
appraise_object(int num)
{
    ::appraise_object(num);

    appraise_wearable_item();
}

/**
 * Zwraca string z warto�ciami zapami�tywanymi przez bro�.
 */
public string
query_strzelecka_auto_load()
{
    return "#B<>S#" + cieciwa->query_auto_load() + "#B<>S#"; //Na razie nic nie zapami�tujemy.
}

/**
 * Funkcja odczytuj�ca zapisane dane i przywracaj�ca bro� do stanu z poprzeniego
 * zalogowania.
 *
 * @param arg string z zapami�tanymi danymi
 */
public string
init_strzelecka_arg(string arg)
{
    string pre, post, cieciwa, fl, args;

    if(sscanf(arg, "%s#B<>S#%s#B<>S#%s", pre, cieciwa, post) != 3)
        return arg;

    if(sscanf(cieciwa, "%s:%s", fl, args) == 2)
    {
        if(file_size(fl) == -1)
            return pre + post; //Plik nie znaleziony

        object ob;

        ob = clone_object(fl);

        ob->move(ENV(TO), 1);

        ob->wkladam_do_broni_strzeleckiej(TO);

        ob->init_cieciwa(args);
    }

    return pre + post;
}

/**
 * Dzi�ki tej funkcji zapami�tamy kilka wa�nych rzeczy.
 */
public string
query_auto_load()
{
    return ::query_auto_load() + query_rozmiary_auto_load() + query_strzelecka_auto_load() + query_wearable_auto_load();
}

/**
 * Funkcja odczytuj�ca ustawienia obiektu.
 *
 * @param arg ustawienia do oczytania.
 */
public string
init_arg(string arg)
{
    return init_wearable_arg(init_strzelecka_arg(init_rozmiary_arg(::init_arg(arg))));
}

/**
 * Musimy pokaza� czy za�adowana jest strza�a i czy jest napi�ty.
 */
public string long(string str, object fob)
{
    if(!str)
    {
        string old_long = ::long(0, fob);
        string *exp = explode(old_long, "\n");
        string ret = implode(exp[..-2], "\n");

        ret += "\n";
        if(query_sila_naciagu() && opis_napietej_broni)
        {
            string s1, s2;
            int przyp_cieciwy;
            int przyp_naciagajacego;

            sscanf(opis_napietej_broni, "%sc:%d %s", s1, przyp_cieciwy, s2);
            sscanf(opis_napietej_broni, "%sk:%d %s", s1, przyp_naciagajacego, s2);

            if(fob)
            {
                ret += bsprintf(sprawdz_koncowki(opis_napietej_broni, TO),
                    query_cieciwa()->short(fob, przyp_cieciwy), "", "", "",
                    query_naciagajacy()->short(fob, przyp_naciagajacego));
            }
            else
            {
                ret += bsprintf(sprawdz_koncowki(opis_napietej_broni, TO),
                    query_cieciwa()->short(przyp_cieciwy), "", "", "",
                    query_naciagajacy()->short(przyp_naciagajacego));
            }
        }

        if(query_pocisk() && opis_zaladowanego_pocisku)
        {
            string s1, s2;
            int przyp_pocisku;
            int przyp_cieciwy;

            sscanf(opis_zaladowanego_pocisku, "%sp:%d %s", s1, przyp_pocisku, s2);
            sscanf(opis_zaladowanego_pocisku, "%sc:%d %s", s1, przyp_cieciwy, s2);

            if(fob)
            {
                ret += bsprintf(sprawdz_koncowki(opis_zaladowanego_pocisku, TO),
                    "" , query_cieciwa()->short(fob, przyp_cieciwy),
                    query_pocisk()->short(fob, przyp_cieciwy), "");
            }
            else
            {
                ret += bsprintf(sprawdz_koncowki(opis_zaladowanego_pocisku, TO),
                    "" , query_cieciwa()->short(przyp_cieciwy),
                    query_pocisk()->short(przyp_cieciwy), "");
            }
        }
        ret += exp[-1] + "\n";

        return ret;
    }
    return ::long(str, fob);
}

public void zmien_cieciwe()
{
}

/**
 * Funkcja pomocnicza do przeniesiena ci�ciwy do otoczenia broni
 */
public void przenies_w_env(object c) { c->move(ENV(TO), 1); }

/**
 * Funkcja wywo�ywana kiedy wk�adamy ci�ciwe do broni
 */
public void wloz_cieciwe_do_broni()
{
    if(!cieciwa)
        return;

    set_alarm(0.1, 0.0, &przenies_w_env(cieciwa));
    cieciwa->wkladam_do_broni_strzeleckiej(TO);
}

/**
 * Funkcja wywo�ywana kiedy wyjmujemy ci�ciwe z broni.
 */
public void
wyjmij_cieciwe_z_broni()
{
    cieciwa->wyciagam_z_broni_strzeleckiej();
}

/**
 * Funkcja wywo�ywana kiedy bro� jest usuwana.
 */
public void remove_object()
{
    //Musimy wyrzuci� pocisk, zabra� shadowy, etc.
    wyladuj_pocisk_z_broni();
    usun_cel();
    paraliz_naciagania->remove_object();
    cieciwa->remove_object();
    ::remove_object();;
}

/**
 * Funkcja identyfikuj�ca obiekt jako bro� strzeleck�.
 *
 * @return 1 zawsze
 */
public int is_bron_strzelecka() { return 1; }

/**
 * Funkcja identyfikuj�ca obiekt jako bro� strzeleck�.
 *
 * @return 1 zawsze
 */
public int is_strzelecka() { return 1; }