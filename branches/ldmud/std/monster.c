/**
 * \file /std/monster.c
 *
 * Standard istot humanoidalnych ale nie b�d�cych standardowymi takimi istotami.
 *
 */

#pragma save_binary
#pragma strict_types

#include <pl.h>
#include <formulas.h>
#include <rozmiary.h>
#include <wa_types.h>
#include <stdproperties.h>

#include "/config/login/login.h"

//WARNING: Nie przestawia� kolejno�ci bo ma DU�E znaczenie dla dzia�ania.
inherit "/std/combat/monunarmed.c";
inherit "/std/npc.c";
inherit "/std/act/action.c";
inherit "/std/act/add_things.c";
inherit "/std/act/attack.c";
inherit "/std/act/domove.c";
inherit "/std/act/ranpick.c"; //FIXME: Sprawdzi� czy potrzebne

inherit "/lib/cloth_wearer.c";

/**
 * Konstruktor naszego monsterka.
 */
void
create_monster()
{
    ::create_npc();

    ustaw_nazwe(({"humanoid", "humanoida", "humanoidowi", "humanoida", "humanoidem", "humanoidzie"}),
        ({"humanoidy", "humanoid�w", "humanoid�", "humanoidami", "humanoidach"}), PL_MESKI_OS);
}

/**
 * Konstruktor npcka.
 */
nomask void
create_npc()
{
    set_all_hitloc_unarmed(({1, 1, 1}));
    create_monster();
}

/**
 * Resetor monsterka.
 */
void
reset_monster()
{
    ::reset_npc();
}

/**
 * Resetor npc'a.
 */
nomask void
reset_npc()
{
    reset_monster();
}

/**
 * @return plik odpowiedzialny za walk� dla tego standardu.
 */
public string
query_combat_file()
{
    return "/std/combat/chumanoid";
}

/**
 * Funkcja m�wi�ca o tym, i� jest to humanoid.
 *
 * @return Zawsze 1.
 */
public int
is_humanoid()
{
    return 1;
}

public string
query_auto_load()
{
    return ::query_auto_load() + query_rozmiary_auto_load();
}

public string
init_arg(string arg)
{
    arg = init_rozmiary_arg(arg);

    return ::init_arg(arg);
}
