inherit "/std/room";

#include <cmdparse.h>
#include <filter_funs.h>
#include <macros.h>
#include <options.h>
#include <pl.h>
#include <stdproperties.h>
#include <speech.h>
#define DBG(x) find_player("vera")->catch_msg(x+"\n");


int wyjdz(string str);
void krzyknij(string str);
void dodaj_event_jazdy(string str);
void dodaj_komende(string cmd, string os3, string inf);
void set_dylizans(object obj);
string query_exitcmd();

int event_alarmid;
int event_time = 10;

object dylizans;
object woznica;
object *do_wyskoczenia = ({ });
string *eventy = ({ });
mixed *komendy = ({ });

void create_wnetrze()
{
}

void dodaj_komende(string cmd, string os3, string inf)
{
    komendy += ({ ({cmd})+({os3})+({inf}) });
}

/**
 * przy wyskakiwaniu z wozu, oraz przy wyrzucaniu przez wo�nic�
 * np. za atak
 */
varargs void
no_to_siup(object kogo, int s)
{
    //najpierw dodajmy delikwenta do listy wrog�w
    if(!objectp(woznica))
        dylizans->sprawdz_woznice();
    if(objectp(woznica))
        woznica->dodaj_wroga(kogo->query_real_name());

    kogo->move_living("M", ENV(dylizans), 1);
    kogo->catch_msg("Z niema�� si�� uderzasz o ziemi�. Au�...\n");
    int maxhp = ftoi(kogo->query_max_hp());
    int dmg = random((3*maxhp)/20)+(maxhp/20);
    kogo->reduce_hp(0, dmg * 100, 10);
}

int
sprawdz(object kto)
{
    if(!kto->query_attack())
        return 0;

    if(!dylizans->sprawdz_woznice())
        return 0;

    //musimy wpierw sprawdzi�, czy wo�nica jest tutaj czy
    //outside, na postoju, �eby lipy nie by�o.
    if(ENV(woznica) == ENV(dylizans)) //wo�nica jest na postoju
    {
        kto->catch_msg(QCIMIE(woznica,PL_MIA)+" przegania ci� "+
        " ze swoj"+dylizans->koncowka("ego","ej","ego")+" "+
        dylizans->query_nazwa(PL_DOP)+" i wyrzuca na zewn�trz.\n");
    }
    else if(ENV(woznica) == TO) //wo�nica jest w dylku
    {
        kto->catch_msg("Zdecydowanym ruchem "+ QIMIE(woznica,PL_MIA)
            +" pozbawia ci� r�wnowagi i wyrzuca z "+
        dylizans->query_nazwa(PL_DOP)+".\n");
    }
    else //o szit...nie ma wo�nicy... ale jest ustawiony!
    {   //tak si� nigdy nie powinno zdarzy�!
        notify_wizards(woznica,"Co on robi na lokacji: "+
            file_name(ENV(woznica))+"??? TO B��D!!!\n");

        woznica->move(ENV(dylizans));
        sprawdz(kto);
    }

    tell_roombb(ENV(dylizans), QCIMIE(kto, PL_MIA)+" wypada z "
        +dylizans->query_nazwa(PL_DOP)+" obijaj�c si� przy tym co bole�nie.\n");
    no_to_siup(kto, 1);
    tell_roombb(this_object(), QCIMIE(kto, PL_MIA)+" wypada z "
        +dylizans->query_nazwa(PL_DOP)+"!\n");
    woznica->hook_reaguj_na_walke();

    return 1;
}

int
no_attack()
{
    set_alarm(1.0,0.0,"sprawdz",TP);
    return 0;
}

nomask void create_room()
{
    set_short("Wn�trze dyli�ansu");
    set_long("Standardowe wn�trze dyli�ansu.\n");
    eventy = ({ });
    komendy = ({ });
    dodaj_komende("wyskocz", "wyskakuje", "wyskoczy�");

    create_wnetrze();

    //w dylku nie mo�na si� bi�, wo�nica wyrzuca takich.
    add_prop(ROOM_M_NO_ATTACK, &no_attack());
    //ani si� ukrywa�
    add_prop(ROOM_I_HIDE,-1);
    add_prop(ROOM_I_INSIDE, 1);
    //tu nie wolno tez rzucac
    add_prop(ROOM_CANT_THROW,"To miejsce jest zbyt ma�e, by "+
        "m�c rzuca�!\n");
}

int
wyjrzyj(string str)
{
    if(strlen(str) && (str != "na zewn�trz" || str!="z wozu"))
    {
        notify_fail("Gdzie chcesz wyjrze�?\n");
        return 0;
    }

    saybb(QCIMIE(TP, PL_MIA)+" wygl�da na zewn�trz.\n");
    //ta funkcja jest g�upia. Przekazywaniu argument�w jest G�UPIE!
    TP->koniec_paralizu_wiec_spogladamy(dylizans,file_name(ENV(dylizans)));

    return 1;
}

init()
{
    ::init();
    init_cmds();
    add_action(&krzyknij(), "krzyknij");
    add_action("wyjrzyj","wyjrzyj");
}

nomask void init_cmds()
{
    int cmdarr_size = sizeof(komendy);
    for(int i =0;i < cmdarr_size; i++)
    {
        mixed cmd = komendy[i][0];
        add_action("wyjdz", cmd);
    }
}


int wyjdz(string str)
{
    int cmdarr_size = sizeof(komendy);
    int cmdindex;

    for(int i = 0; i < cmdarr_size; i++)
    {
        mixed tmp = komendy[i][0];

        if(query_verb() ~= tmp)
        {
            cmdindex = i;
            break;
        }
    }

    if(!str)
    {
        notify_fail("Sk�d chcesz "+komendy[cmdindex][2]+"?\n");
        return 0;
    }

    if(!parse_command(lower_case(str), this_object(), " 'z' "+"'"+dylizans->query_nazwa(PL_DOP)+"' "))
    {
        notify_fail("Sk�d chcesz "+komendy[cmdindex][2]+"?\n");
        return 0;
    }

    if(!objectp(dylizans))
    {
        notify_fail("Ups. Wyst�pi� b��d!\n");
        return 0;
    }

    if(TP->query_prop(PLAYER_M_SIT_SIEDZACY) != 0 || TP->query_prop(PLAYER_M_SIT_LEZACY))
    {
        notify_fail("Mo�e najpierw wstaniesz?\n");
        return 0;
    }

	if(dylizans->query_ruszam_sie())
	{
		if(query_verb() != "wyskocz")
		{
			notify_fail("Nie mo�esz w tej chwili "+komendy[cmdindex][2]+" z "
					+dylizans->query_nazwa(PL_DOP)+".\n");
			return 0;
		}

		if(member_array(TP, do_wyskoczenia) != -1)
		{
			//TODO: wyskakiwanie z �odzi
			//TODO: Opisy w zaleznosci od obrazen
	               do_wyskoczenia -= ({TP});

	               tell_roombb(ENV(dylizans), QCIMIE(TP, PL_MIA)+" wyskakuje z "
		      +dylizans->query_nazwa(PL_DOP)+" obijaj�c si� przy tym co bole�nie.\n");
	               tell_roombb(this_object(), QCIMIE(TP, PL_MIA)+" wyskakuje z "
			   +dylizans->query_nazwa(PL_DOP)+"!\n");
                        no_to_siup(TP);
			return 1;
		}
		else
		{
			do_wyskoczenia += ({TP});
			notify_fail("Napewno chcesz wyskoczy�"
			+" z p�dz�c"+koncowka("ego", "ej", "ego", "ych", "ych")
			+" "+dylizans->query_nazwa(1)+"?\n");
			return 0;
		}
	}


        tell_roombb(ENV(dylizans), QCIMIE(TP, PL_MIA)+" "+komendy[cmdindex][1]
			+" z "+dylizans->query_nazwa(PL_DOP)+".\n");
	TP->move_living("M", ENV(dylizans), 1);
	tell_roombb(this_object(), QCIMIE(TP, PL_MIA)+" "+komendy[cmdindex][1]
			+" z "+dylizans->query_nazwa(PL_DOP)+".\n");
	return 1;
}

void set_dylizans(object obj)
{
    dylizans = obj;
}

string
shout_name()
{
    object pobj = previous_object(-1);

    if (ENV(pobj) == ENV(TP) &&
        CAN_SEE_IN_ROOM(pobj) && CAN_SEE(pobj, TP))
        return TP->query_Imie(pobj, PL_MIA);
    if (pobj->query_met(TP))
        return TP->query_name(PL_MIA);
    return TP->query_osobno() ?
           TP->koncowka("Jaki� m�czyzna", "Jaka� kobieta") :
           TP->koncowka("Jaki� ", "Jaka� ", "Jakie� ")
         + TP->query_rasa(PL_MIA);
}

string
shout_string(string shout_what)
{

    object pobj = previous_object(-1);
    int empty_string = shout_what == "0";

    if (ENV(pobj) == ENV(TP))
        return empty_string ? "krzyczy g�o�no." : "krzyczy: " + shout_what;

    return empty_string ? "krzyczy g�o�no z "+dylizans->query_nazwa(PL_DOP)+"." :
    "krzyczy z "+dylizans->query_nazwa(PL_DOP)+": " + shout_what;
}

int
krzyknij(string str)
{
    object env;
    object *lokacje;
    mixed Prop;
    if (Prop = TP->query_prop(LIVE_M_MOUTH_BLOCKED) != 0)
      {
	notify_fail(stringp(Prop) ? Prop :
		    "Nie jeste� w stanie wydoby� z siebie �adnego d�wi�ku.\n");
	return 0;
      }

    if (env = ENV(TP))
      lokacje = ({ENV(dylizans)}) + ({env});

    if (TP->query_option(OPT_ECHO))
        write("Krzyczysz" + (str ? ": " + str : " g�o�no.") + "\n");
    else
        write("Ju�.\n");
    int ile = sizeof(lokacje);
    while(ile--)
    tell_room(lokacje[ile], "@@shout_name:" + file_name(this_object())
                + "@@ @@shout_string:" + file_name(this_object()) + "|"
		+ SPEECH_DRUNK(str, TP) + "@@\n", ({TP}));
    return 1;
}

void dodaj_event_jazdy(string str)
{
    add_event(str);
}

void show_events(mixed event)
{
    tell_roombb(this_object(), event);
}

string query_exitcmd()
{
    string str = komendy[1][0]+" z "+dylizans->query_nazwa(PL_DOP);
    return str;
}

int
query_dylizans_in()
{
    return 1;
}

void
set_woznica(object x)
{
    woznica = x;
}

object
query_woznica()
{
    return woznica;
}

public string
query_auto_load()
{
    return 0;
}
