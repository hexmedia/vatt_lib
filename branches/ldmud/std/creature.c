/**
 *
 * \file /std/creature.c
 *
 * This is the base for all nonhumanoid livings.
 *
 */

#pragma save_binary
#pragma strict_types

#include <pl.h>
#include <stdproperties.h>

//WARNING: Nie przestawia� kolejno�ci bo ma DU�E znaczenie dla dzia�ania.
inherit "/std/combat/unarmed.c";
inherit "/std/mobile.c";

#define QEXC query_combat_object()

void
create_creature()
{
}

nomask void
create_mobile()
{
    if(!this_object()->is_humanoid())
        add_prop(NPC_M_NO_ACCEPT_GIVE," nie potrafi niczego przyjmowa�.\n");

    create_creature();
}

void
reset_creature()
{
}

nomask void
reset_mobile()
{
    reset_creature();
}

/*
 * Description:  Use the plain (no tools) combat file
 */
public string
query_combat_file() { return "/std/combat/cplain"; }

/*
 * Function name:  default_config_npc
 * Description:    Sets some necessary values for this creature to function
 *                 This is basically stats and skill defaults.
 */
varargs public void
default_config_creature(int lvl)
{
    default_config_mobile(lvl);
}

/*
 * Function name: add_attack
 * Description:   Add an attack to the attack array.
 * Arguments:
 *             wchit: Weapon class to hit
 *             wcpen: Weapon class penetration
 *	       dt:    Damage type
 *             %use:  Chance of use each turn
 *	       id:    Specific id, for humanoids W_NONE, W_RIGHT etc
 *
 * Returns:       True if added.
 */
public int
add_attack(int wchit, int wcpen, int damtype, int prcuse, int id)
{
    if (!QEXC)
        return 0;

    return (int)QEXC->cb_add_attack(wchit, wcpen, damtype, prcuse, id);
}

/*
 * Function name: remove_attack
 * Description:   Removes a specific attack
 * Arguments:     id: The attack id
 * Returns:       True if removed
 */
public int
remove_attack(int id)
{
    if (!QEXC)
        return 0;

    return (int)QEXC->cb_remove_attack(id);
}

/**
 * Dodajemy do livinga hiltokacje.
 *
 * @param ac        Ac hitlokacji
 * @param prchit    Procentowa szansa na trafienie w hitlokacje
 * @param desc      opis hitlokacji
 * @param id        id hitlokacji
 * @param przym     przymiotniki
 * @param hp        czy doda� hp info
 *
 * @return 0 nie dodany
 * @return !0 dodany
 */
public varargs int
add_hitloc(int *ac, int prchit, string desc, int id, mixed przym, int hp = 1)
{
    if (!QEXC)
        return 0;

    return (int)QEXC->cb_add_hitloc(ac, prchit, desc, id, przym, hp);
}

/*
 * Function name: remove_hitloc
 * Description:   Removes a specific hit location
 * Arguments:     id: The hitloc id
 * Returns:       True if removed
 */
public int
remove_hitloc(int id)
{
    if (!QEXC)
        return 0;

    return (int)QEXC->cb_remove_hitloc(id);
}

/**
 * Funkcja m�wi�ca o tym, i� nie jest to humanoid.
 *
 * @return Zawsze 0.
 */
public int
is_humanoid()
{
    return 0;
}

/**
 * Funkcja identyfikuj�ca obiekt jako creature.
 *
 * @return Zawsze 1.
 */
public int
is_creature()
{
    return 1;
}

