//inherit "/std/object";

#include <object_types.h>

inherit "/std/armour.c";
inherit "/lib/wearable_item";

public nomask void
create_armour()
{
//     set_looseness(3);
//     set_layers(1);
    this_object()->create_ubranie();
}

public nomask void
create_ubranie()
{
//     set_looseness(3);
//     set_layers(1);
}

void
leave_env(object from, object to)
{
     ::leave_env(from, to);
     wearable_item_leave_env(from, to);
}
void
appraise_object(int num)
{
    ::appraise_object(num);
    appraise_wearable_item();
}

int query_default_weight() { return 850; }

public int
query_type()
{
    return O_UBRANIA;
}

