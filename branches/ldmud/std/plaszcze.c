/*  Autor: Avard
    Data : 4.11.07
    Info : Std do plaszczy, wbudowany kaptur 
    TODO : Ukrywanie przymiotnika zwiazanego z twarza pod "zakapturzony" */

inherit "/std/armour";

#include <object_types.h>
#include <wa_types.h>
#include <rozmiary.h>
#include <formulas.h>
#include <stdproperties.h>
#include <macros.h>
#include <cmdparse.h>
#include <composite.h>
#include "/config/login/login.h"

int hood = 0;
int naciagniety = 0;
int naciagnij(string str);
int sciagnij(string str);

void
create_plaszcz()
{
    ustaw_nazwe("p^laszcz");
    dodaj_przym("d^lugi","d^ludzy");
    dodaj_przym("czarny","czarni");
    set_long("Zawalisty p^laszcz\n");
}

nomask void
create_armour()
{
    set_slots(A_CLOAK);
    set_type(O_UBRANIA);       
    create_plaszcz();
}

int
set_hood(int i)
{
    hood = i;
    return hood;
}
int
query_hood()
{
    return hood;
}

init()
{
   ::init();
   add_action(naciagnij, "naciagnij");
   add_action(sciagnij, "sciagnij");
}

int
naciagnij(string str)
{
    object plaszcz;
    if (!str) 
    return 0;
    if(parse_command(str,environment(this_object()),
		"'kaptur' [%o:]" + PL_DOP,plaszcz))
    {
        if(hood == 1)
        {
            if(naciagniety == 1)
            {
                write("Ju^z masz kaptur naci^agni^ety na g^low^e.\n");
                return 1;
            }
            else
            {
                if(objectp(query_worn()))
                {
                    write("Naci^agasz kaptur "+ short(TP, PL_DOP) +" na "+
                        "g^low^e zas^laniaj^ac twarz.\n");
                    saybb(QCIMIE(this_player(), PL_MIA) +" naci^aga kaptur na "+
                        "g^low^e zas^laniaj^ac swoj^a twarz.\n");
                    naciagniety = 1;
                    return 1;
                }
                else
                {
                    write("Musisz najpierw za^lo^zy^c p^laszcz.\n");
                    return 1;
                }
            }
        }
        else
        {
            write("Ten p^laszcz nie ma kaptura.\n");
            return 1;
        }
    }
    notify_fail("Naci^agnij co?\n");
    return 0;
}

int
sciagnij(string str)
{
    object plaszcz;
    if (!str) 
    return 0;
    if(parse_command(str,environment(this_object()),
		"'kaptur' [%o:]" + PL_DOP,plaszcz))
    {
        if(hood == 1)
        {
            if(naciagniety == 0)
            {
                write("Nie masz ^zadnego kaptura na g^lowie.\n");
                return 1;
            }
            else
            {
                if(objectp(query_worn()))
                {
                    write("^Sci^agasz kaptur "+ short(TP, PL_DOP) +" z "+
                        "g^lowy ods^laniaj^ac twarz.\n");
                    saybb(QCIMIE(this_player(), PL_MIA) +" ^sci^aga kaptur "+
                        short(TP, PL_DOP) +" z g^lowy ods^laniaj^ac twarz.\n");
                    naciagniety = 0;
                    return 1;
                }
                else
                {
                    write("Nie masz ^zadnego kaptura na g^lowie.\n");
                    return 1;
                }
            }
        }
        else
        {
            write("Ten p^laszcz nie ma kaptura.\n");
            return 1;
        }
    }
    notify_fail("^Sci^agnij co?\n");
    return 0;
}

public mixed
remove_me(int cicho = 0)
{
    if(living(query_worn())) // Sprawdzic tez czy kaptur zalozony
        query_worn()->command("sciagnij kaptur plaszcza"); // Tutaj wywolac funkcje zdejmujaca kaptur

    return ::remove_me(cicho);
}

public string
query_pomoc()
{
    string toWrite;

    notify_fail("Nie ma pomocy na ten temat.\n");

    if(hood == 1)
    {
        toWrite = koncowka("Ten","Ta","To") + " " + short(TP, PL_MIA)+
            " posiada kaptur, kt^ory mo^zesz naci^agn^a^c lub "+
            "^sci^agn^a^c z g^lowy.\n";
    }
    return toWrite;
}