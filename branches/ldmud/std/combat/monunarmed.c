/**
 * \file /std/combat/monunarmed.c
 *
 * To w�a�ciwie kopia dawnego pliku \file /std/combat/hununarmed.c
 * w kt�rym teraz s� definicje dla domy�lnych humanoid�w,
 * czyli dla humanoid�w kt�re nieuzbrojone walcz� pi�ciami, �okciami
 * nogami i kolanami.
 *
 * Plik ten jest inheritowany przez \file /std/monster.c
 */
#pragma save_binary
#pragma strict_types

#include <pl.h>
#include <macros.h>
#include <wa_types.h>
#include <ss_types.h>
#include <formulas.h>

inherit "/std/combat/unarmed.c";

#define QEXC (this_object()->query_combat_object())
#define WW_BONUS 3

/*
 * Function name: cr_configure
 * Description:   Configures basic values for this humanoid.
 */
public void
cr_configure()
{
    ::cr_configure();

    if (query_attackuse())
        QEXC->cb_set_attackuse(query_attackuse());
    else
        QEXC->cb_set_attackuse(100);
}

/*
 * Function name: cr_reset_attack
 * Description:   Set the values for a specific attack. These are called from
 *                the external combat object.
 * Arguments:     aid: The attack id
 */
public void
cr_reset_attack(int aid)
{
    int wchit, wcpen, uskill;

    ::cr_reset_attack(aid);

    if(!sizeof(query_ua_attack(aid)))
    {
        wchit = W_HAND_HIT;
        wcpen = W_HAND_PEN;

        if(uskill = this_object()->query_skill(SS_UNARM_COMBAT))
        {
            wchit += F_UNARMED_HIT(uskill, this_object()->query_stat(SS_DEX));
            wcpen += F_UNARMED_PEN(uskill, this_object()->query_stat(SS_STR));
        }

        uskill = max(-1, uskill);

        switch(aid)
        {
            case W_RIGHT:
                if((!QEXC->is_configured(HA_P_REKA)) ||
                    (QEXC->query_hitloc_area_percentage(HA_P_REKA) <= 0.0))
                {
                    QEXC->cb_add_attack(0, 0, W_BLUDGEON, 0, aid, uskill + WW_BONUS);
                }
                else
                    QEXC->cb_add_attack(wchit, wcpen, W_BLUDGEON, 25, aid, uskill + WW_BONUS);

                break;

            case W_LEFT:
                if ((!QEXC->is_configured(HA_L_REKA)) ||
                    (QEXC->query_hitloc_area_percentage(HA_L_REKA) <= 0.0))
                {
                    QEXC->cb_add_attack(0, 0, W_BLUDGEON, 0, aid, uskill + WW_BONUS);
                }
                else
                    QEXC->cb_add_attack(wchit, wcpen, W_BLUDGEON, 25, aid, uskill + WW_BONUS);

                break;

            case W_BOTH:
                /*
                 * We use the hands separately in unarmed combat
                 */
                QEXC->cb_add_attack(0, 0, W_BLUDGEON, 0, aid, uskill + WW_BONUS);
                break;

            case W_FOOTR:
                if((!QEXC->is_configured(HA_P_NOGA)) ||
                    (QEXC->query_hitloc_area_percentage(HA_P_NOGA) <= 0.0))
                {
                    QEXC->cb_add_attack(0, 0, W_BLUDGEON, 0, aid, uskill);
                }
                else
                    QEXC->cb_add_attack(wchit, wcpen, W_BLUDGEON, 25, aid, uskill);

                break;

            case W_FOOTL:
                if((!QEXC->is_configured(HA_L_NOGA)) ||
                    (QEXC->query_hitloc_area_percentage(HA_L_NOGA) <= 0.0))
                {
                    QEXC->cb_add_attack(0, 0, W_BLUDGEON, 0, aid, uskill);
                }
                else
                    QEXC->cb_add_attack(wchit, wcpen, W_BLUDGEON, 25, aid, uskill);
                break;
        }
    }
}

/*
 * Function name: cr_reset_hitloc
 * Description:   Set the values for a specific hitlocation
 * Arguments:     hid: The hitlocation (bodypart) id
 */
public void
cr_reset_hitloc(int hid)
{
    ::cr_reset_hitloc(hid);

    if(!sizeof(query_ua_hitloc(hid)))
    {
        switch(hid)
        {
            case A_HEAD:
                QEXC->cb_add_hitloc(A_UARM_AC,  4, "g�owa",             hid);
                break;
            case A_CHEST:
                QEXC->cb_add_hitloc(A_UARM_AC, 14, "klatka piersiowa",  hid);
                break;
            case A_STOMACH:
                QEXC->cb_add_hitloc(A_UARM_AC, 13, "brzuch",            hid);
                break;
            case A_BACK:
                QEXC->cb_add_hitloc(A_UARM_AC,  3, "plecy",             hid);
                break;
            case A_L_SHOULDER:
                QEXC->cb_add_hitloc(A_UARM_AC,  5, "bark",              hid, "lewy");
                break;
            case A_R_SHOULDER:
                QEXC->cb_add_hitloc(A_UARM_AC, 5, "bark",               hid, "prawy");
                break;
            case A_L_ARM:
                QEXC->cb_add_hitloc(A_UARM_AC, 6, "rami�",              hid, "lewy");
                break;
            case A_R_ARM:
                QEXC->cb_add_hitloc(A_UARM_AC, 6, "rami�",              hid, "prawy");
                break;
            case A_L_FOREARM:
                QEXC->cb_add_hitloc(A_UARM_AC, 5, "przedrami�",         hid, "lewy");
                break;
            case A_R_FOREARM:
                QEXC->cb_add_hitloc(A_UARM_AC, 5, "przedrami�",         hid, "prawy");
                break;
            case A_L_HAND:
                QEXC->cb_add_hitloc(A_UARM_AC, 3, "d�o�",               hid, "lewy");
                break;
            case A_R_HAND:
                QEXC->cb_add_hitloc(A_UARM_AC, 3, "d�o�",               hid, "prawy");
                break;
            case A_L_THIGH:
                QEXC->cb_add_hitloc(A_UARM_AC, 6, "udo",                hid, "lewy");
                break;
            case A_R_THIGH:
                QEXC->cb_add_hitloc(A_UARM_AC, 6, "udo",                hid, "prawy");
                break;
            case A_L_SHIN:
                QEXC->cb_add_hitloc(A_UARM_AC, 5, "�ydka",              hid, "lewy");
                break;
            case A_R_SHIN:
                QEXC->cb_add_hitloc(A_UARM_AC, 5, "�ydka",              hid, "prawy");
                break;
            case A_L_FOOT:
                QEXC->cb_add_hitloc(A_UARM_AC, 3, "stopa",              hid, "lewy");
                break;
            case A_R_FOOT:
                QEXC->cb_add_hitloc(A_UARM_AC, 3, "stopa",              hid, "prawy");
                break;
        }
    }
}

/*
 * Function name: cr_try_hit
 * Description:   Decide if a certain attack fails because of something
 *                related to the attack itself.
 * Arguments:     aid:   The attack id
 * Returns:       True if hit, otherwise 0.
 */
public int
cr_try_hit(int aid) { return 1; }

/*
 * Function name: cr_got_hit
 * Description:   Tells us that we got hit. It can be used to reduce the ac
 *                for a given hitlocation for each hit.
 * Arguments:     hid:   The hitloc id
 *                ph:    The %hurt
 *                att:   Attacker
 *        aid:   The attack id
 *                dt:    The damagetype
 *        dam:   The damage in hit points
 */
public void
cr_got_hit(int hid, int ph, object att, int aid, int dt, int dam)
{
    /* We do not tell if we get hit in any special way
    */
}

/*
 * Function name: set_all_hitloc_unarmed
 * Description:   Set up all unarmed hit location basics
 * Arguments:     ac - an int or an array to modify the ac....
 */
public void
set_all_hitloc_unarmed(mixed ac)
{
    if(intp(ac))
        ac = ({ac, ac, ac});

    set_hitloc_unarmed(A_HEAD,          ac,  4, "g�owa");
    set_hitloc_unarmed(A_CHEST,         ac, 14, "klatka piersiowa");
    set_hitloc_unarmed(A_STOMACH,       ac, 13, "brzuch");
    set_hitloc_unarmed(A_BACK,          ac,  3, "plecy");
    set_hitloc_unarmed(A_L_SHOULDER,    ac,  5, "bark",         "lewy");
    set_hitloc_unarmed(A_R_SHOULDER,    ac,  5, "bark",         "prawy");
    set_hitloc_unarmed(A_L_ARM,         ac,  6, "rami�",        "lewy");
    set_hitloc_unarmed(A_R_ARM,         ac,  6, "rami�",        "prawy");
    set_hitloc_unarmed(A_L_FOREARM,     ac,  5, "przedrami�",   "lewy");
    set_hitloc_unarmed(A_R_FOREARM,     ac,  5, "przedrami�",   "prawy");
    set_hitloc_unarmed(A_L_HAND,        ac,  3, "d�o�",         "lewy");
    set_hitloc_unarmed(A_R_HAND,        ac,  3, "d�o�",         "prawy");
    set_hitloc_unarmed(A_L_THIGH,       ac,  6, "udo",          "lewy");
    set_hitloc_unarmed(A_R_THIGH,       ac,  6, "udo",          "prawy");
    set_hitloc_unarmed(A_L_SHIN,        ac,  5, "�ydka",        "lewy");
    set_hitloc_unarmed(A_R_SHIN,        ac,  5, "�ydka",        "prawy");
    set_hitloc_unarmed(A_L_FOOT,        ac,  3, "stopa",        "lewy");
    set_hitloc_unarmed(A_R_FOOT,        ac,  3, "stopa",        "prawy");
}

/*
 * Function name: update_procuse()
 * Description:   use this to update the percent usage of attacks
 */
public void
update_procuse()
{
    QEXC->cb_modify_procuse();
}
