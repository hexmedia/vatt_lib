/**
 * \file /std/combat/cbase.c
 *
 *    LVL 0
 *
 *      W tym obiekcie znajduj� si� wy��czone z livinga procedury
 * odpowiedzialne za walk� mi�dzy dwoma obiektami �yj�cymi.
 *
 *      Obiekt ten jest klonowany a wska�nik do niego jest
 * przypisywany w obiekcie dla kt�rego ten obiekt przejmuje
 * funkcje walki.
 *
 *      Obecna wersja to: 3.0.0 beta oparta o wersj� wcze�niejsz�, ze
 *  sporymi zmianami.
 *
 *      Ten plik jest implementacj� walki dla najprostszych obiekt�w
 * ale opieraj� si� o niego wszystkie p�niejsze implementacje
 * dla living�w itd. Je�li chcesz dokona� jakich� wi�kszych zmian
 * w sposobie walki danego livinga to proponuje utworzy� osobny
 * plik i to w�a�nie jego nie cbase linkowa� do swojego livinga.
 *
 * Obecnie plikiem opiekuje si� @author Krun
 * Data ostatniej modyfikacji @date Sierpie� 2008
 *
 */

#pragma save_binary
#pragma strict_types
#pragma no_reset

#include "combath.h"

#include <exp.h>
#include <std.h>
#include <math.h>
#include <debug.h>
#include <combat.h>
#include <colors.h>
#include <macros.h>
#include <options.h>
#include <comb_mag.h>
#include <formulas.h>
#include <ss_types.h>
#include <wa_types.h>
#include <composite.h>
#include <filter_funs.h>
#include <opisy_walki.h>
#include <przymiotniki.h>
#include <stdproperties.h>

#define DBG(x)              find_player("krun")->catch_msg(set_color(find_player("krun"),                   \
                                COLOR_FG_RED) + "FIGHT_DEBUG: " + clear_color(find_player("krun")) + (x) + "\n")

#define PROC_WEIGHT(x)      TO_PROC(x->query_prop(CONT_I_MAX_WEIGHT) - x->query_prop(CONT_I_WEIGHT),        \
                                x->query_prop(OBJ_I_WEIGHT) - x->query_prop(CONT_I_WEIGHT))
#define PROC_INTOX(x)       TO_PROC(x->query_prop(LIVE_I_MAX_INTOX), x->query_intoxicated())
#define ST2HUN(x,y)         ftoi(F_STAT_TO_100(x->query_stat(y)))

#define LOG_COMBAT(f,t,m)   log_file((f), (m)->query_real_name() + " (" + file_name(m) + "): " + (t))

#define CQME                if(!check_qme())    \
                            {                   \
                                destruct();     \
                                return 0;       \
                            }

/*
 * Prototypy
 */
public          nomask  int     cb_query_panic();
public varargs  nomask  void    cb_attack(object ob, int og);
public varargs          mixed   cb_wield_weapon(object wep, int slot);
public                  void    cb_unwield(object wep);
static          nomask  int     fixnorm(int offence, int defence);
static varargs  nomask  void    heart_beat();
static                  void    stop_heart();
public                  object  qme();
public          nomask  object  check_qme();
public                  int     cb_query_real_speed();
public          nomask  void    cb_update_enemies();
public          nomask  mixed   cb_query_attack();
public          nomask  mixed   cb_update_attack();
                nomask  int     szansa_obrony(int x);
public                  int    *query_hitloc_area_members(int area);
public                  string  cb_hitloc_desc(int hid, int przyp);
public varargs  nomask  void    cb_stop_fight(mixed elist);
public          nomask  int     query_hitloc_area(int hid);

/**
 * Format element�w w tablicy atak�w:
 * ({ wchit, wcpen, dt, use, skill, weight })
 *  wchit:  Klasa hita broni
 *  wcpen:  Klasa pena broni
 *  dt:     Typ zadawanych obra�e�

 *
 *  To wsio jest prawdopodobnie nieaktualne, ale nie jestem do ko�ca pewien czy ju�
 *  wszystko zmieni�em co mia�em zmieni�, a je�li nie to ten tekst mo�e si� jeszcze
 *  do czego� przyda�.
 *
    Format of each element in the attacks array:
	 ({ wchit, wcpen, dt, %use, skill, weight })
	      wchit: Weapon class to hit
	      wcpen: Weapon class penetration
	      dt:    Damage type
	      %use:  Chance of use each turn
   	      skill: The skill of this attack (defaults to wcpen)
   	      m_hit: The modified tohit used in combat
	      m_pen: The modified pen used in combat
	      weight:The weight of the tool

	 att_id:    Specific id, for humanoids W_NONE, W_RIGHT etc

    Format of each element in the hitloc_ac array:

	 ({ *ac, %hit, desc })
	      ac:    The ac's for each damagetype for a given hitlocation
	      %hit:  The chance that a hit will hit this location
	      desc:  String describing this hitlocation, ie "head", "tail"
	      m_ac:  Modified ac to use in combat
		       .......
	 Note that the sum of all %hit must be 100.

*/

static int
        panic,                  /* Dont panic... */
        panic_time,             /* Time panic last checked. */
        tohit_val,              /* A precalculated tohit value for someone */
        hit_heart,              /* Controls the quickness when fighting. */
        i_am_real,              /* True if the living object is interactive */
        alarm_id,               /* The id of the heart_beat alarm */
        set_speed,              /* The base speed set externally */
        real_speed,             /* Really how often do we hit */
        msg_counter,            /* Licznik do walki dru�ynowej, chyba trzeba to jako� zmieni�;P [Krun] */
        t_w_plecy;              /* czas ostatniego oberwania po plecach */

static mapping
        hitloc_ac = ([]),       /* Hitlokacje obiektu */
        attacks = ([]),         /* Wszystkie ataki livinga */
        hitloc_areas = ([]);    /* Obszary hitlokacji */

static object
        me,                     /* The living object concerned */
       *enemies = ({}),         /* Array holding all living I hunt */
       *to_stun = ({}),         /* Gracze do og�uszenia */
       *attacked_by = ({}),     /* w czasie tury mo�na walczy� z kilkoma przeciwnikami jednocze�nie*/
        tohit_ob,               /* Last object we calculated tohit values */
        attack_ob,              /* Aktualny wr�g. */
        l_w_plecy,              /* Lokacja na kt�rej ostatnio oberwali�my po plecach*/
        zaslaniany,             /* Living, kt�rego zas�aniamy. */
       *zaslaniajacy;           /* Livingi, kt�re zas�aniaj� nas. */

/**
 * Funkcja redobywa bro� podan� w argumencie.
 * Tym samym uaktualnia wszystkie warto�ci przyznawane
 * graczowi przez bro�.
 *
 * @param bron bro� kt�r� mamy redoby�.
 *
 * TODO: Funkcja powinna redobywa� na hitlokacje na kt�rej bro� poprzednio
 *       by�a.
 */
public void
cb_update_weapon(object wep)
{
    if(!wep)
        return;

    cb_unwield(wep);
    cb_wield_weapon(wep);
}

/**
 * Podstawowe statystyki dla komendy 'combatstat'
 */
public string
cb_status()
{
    CQME;

    string str, str2;
    int tmp;
    mixed ac;
    object *valid_enemies;

    enemies = enemies - ({ 0 }); /* Maybe some old enemy is dead?? */
    to_stun = to_stun - ({ 0 });

    str = "Obiekt livinga: " + file_name(me) +
        " (Uid: " + getuid(me) + ", Euid: " + geteuid(me) + ")\n";

    str += "Obiekt walki: " + file_name(this_object()) +
        " (Uid: " + getuid(this_object()) +
        ", Euid: " + geteuid(this_object()) + ")\n";

    if(attack_ob)
    {
        str += IS_ME("Walczysz", "Walczy") + " z: " +
            attack_ob->query_name() + " (" + file_name(attack_ob) +")\n";
    }

    /* if the enemies have been destroyed then it can cause problems
     * so remove the non live ones.  Left
     */
    cb_update_enemies();
    valid_enemies = FILTER_LIVE(enemies);

    if(sizeof(valid_enemies))
        str += "Wrogowie:\n" + break_string(COMPOSITE_LIVE(valid_enemies, 0), 76, 3);
    else
        str += "�adnych wrog�w.";

    valid_enemies = FILTER_LIVE(attacked_by);

    if(sizeof(attacked_by))
        str += "\nAtakuj� " + IS_ME("ci�", "go") + ":\n" + break_string(COMPOSITE_LIVE(valid_enemies, 0), 76, 3);
    else
        str += "\nNikt " + IS_ME("ci�", "go") + " nie atakuje.";


/*
    str += sprintf("\nPanic: %3d, Attacks: %3d, Hitlocations: %3d\n",
        cb_query_panic(), m_sizeof(attacks), m_sizeof(hitloc_ac));
*/

    if(m_sizeof(attacks))
        str += sprintf("\n\n%-20s %@|9s\n","  Ataki ",({"wchit", "k�ute  ci�te  obuch  ", "wcskill", "%u�ycia" }));

    foreach(int aid : m_indexes(attacks))
    {
        ac = attacks[aid][ATT_DAMT];

        str2 = this_player()->check_call(this_object()->cb_attack_desc(aid));

        if(strlen(str2) > 19)
            str2 = str2[0..18];

        str += sprintf("%-20s %|8s %-6s %-6s %-6s %|9d %|9d\n", str2 + ":",
            attacks[aid][ATT_WCHIT] + "/" + attacks[aid][ATT_M_HIT], (ac & W_IMPALE ? attacks[aid][ATT_WCPEN][0] + "/" +
            attacks[aid][ATT_M_PEN][0] : " ---"), (ac & W_SLASH ? attacks[aid][ATT_WCPEN][1] + "/" +
            attacks[aid][ATT_M_PEN][1] : " ---"), (ac & W_BLUDGEON ? attacks[aid][ATT_WCPEN][2] + "/" +
            attacks[aid][ATT_M_PEN][2] : " ---"), attacks[aid][ATT_SKILL], attacks[aid][ATT_PROC]);
    }

    if(m_sizeof(hitloc_ac))
    {
        str += sprintf("\n%-18s %-10s %@|6s %|16s %|5s\n","  Hitlokacja", "Ident",
            ({"k�u", "ci�", "obu", " %u"}), "hp/max", "rana");
    }

    foreach(int hid : sort_array(m_indexes(hitloc_ac)))
    {
        str += sprintf("%-18s %-10s", cb_hitloc_desc(hid, PL_MIA), "(" + hid + "):") + " ";
        ac = hitloc_ac[hid][HIT_AC];

        if(!pointerp(ac))
            ac = ({ ac, ac, ac });
        else
            ac = ac[0..2];

        str += sprintf("%@|6d %|6d %|16s ", ac, hitloc_ac[hid][HIT_PROC],
            sprintf("%5d/%5d", me->query_hp(hid), me->query_max_hp(hid)));

        switch(me->query_hitloc_wound(hid))
        {
            case W_IMPALE:      str += " k�u";   break;
            case W_SLASH:       str += " ci�";   break;
            case W_BLUDGEON:    str += " obu";   break;
        }

        str += "\n";
    }

    if(m_sizeof(hitloc_areas))
        str += sprintf("\n%-18s %|20s %|10s\n","  Obszar", "hp/max", "%hp");

    foreach(int a : m_indexes(hitloc_areas))
    {
        int max_area_hp, area_hp, percentage;
        string area;

        area = hitloc_areas[a][HA_DESC];

        area_hp     = me->query_hp(-a);
        max_area_hp = me->query_max_hp(-a);
        percentage  = me->query_proc_hp(-a);

        str += sprintf("%-18s %|20s %|10s\n", area,
            sprintf("%5d/%5d", area_hp, max_area_hp), sprintf("%3d", percentage));
    }

    str += "\n" + sprintf("%14s: %10d %15s: %10d %14s: %10d\n", "Si�a",
            me->query_stat(SS_STR), "Zr�czno��", me->query_stat(SS_DEX),
            "Kondycja", me->query_stat(SS_CON)) +
        sprintf("%14s: %10d %15s: %10d %14s: %10d\n", "Parowanie",
            me->query_skill(SS_PARRY), "Uniki", me->query_skill(SS_DEFENSE),
            "Tarczownictwo", me->query_skill(SS_SHIELD_PARRY)) +
        sprintf("%14s: %10d %15s: %10d %14s: %10d\n", "Styl grupowy",
            me->query_skill(SS_STL_GRUPOWY), "Styl szybki",
            me->query_skill(SS_STL_SZYBKI), "Styl silny",
            me->query_skill(SS_STL_SILNY)) +
        sprintf("%14s: %10d %15s: %10d %14s: %10d\n", "Um-prawa r�ka",
            me->query_skill(me->query_weapon(TS_R_HAND) + SS_WEP_FIRST),
            "Walka w ciemno�", me->query_skill(SS_BLIND_COMBAT),
            "Walka wr�cz", me->query_skill(SS_UNARM_COMBAT)) +
        sprintf("%14s: %10d %15s: %10d %14s: %10d\n", "Um-lewa r�ka",
            me->query_skill(me->query_weapon(TS_L_HAND) + SS_WEP_FIRST),
            "Walka 2 bro�mi", me->query_skill(SS_2H_COMBAT),
            "Walka konna", me->query_skill(SS_MOUNTED_COMBAT)) +
        sprintf("%14s: %10d %15s: %10d %14s: %10d\n", "Combat stat av", (tmp = me->query_cmb_average_stat()),
            "Exp at kill", (F_KILL_GIVE_EXP(tmp) * (!i_am_real ? me->query_exp_factor() / 100 : 1)),
            "Enc", (me->query_encumberance_weight() + me->query_encumberance_volume() / 2)) +
        sprintf("%14s: %4d(%4d) %15s: %4d(%4d) %14s: %4d(%4d)\n", "Szybko��",
            cb_query_real_speed(), (CB_SPEED_BASE / set_speed), "Zm�czenie(x10)",
            me->query_fatigue() / 10, me->query_max_fatigue() / 10, "Hp(x100)", me->query_hp() / 100,
            me->query_max_hp() / 100);

    return str;
}

/*
 * Function name: cb_data
 * Description:   More data about combat stats.
 * Returns:	  A string to write
 */
string
cb_data()
{
    string str;
    int i, size, val, tmp, t2, ac;
    object *arr;

    CQME;

    str = "Living object: " + file_name(me) +
        " (Uid: " + getuid(me) + ", Euid: " + geteuid(me) + ")\n";

    str += "Combat object: " + file_name(this_object()) +
        " (Uid: " + getuid(this_object()) +
        ", Euid: " + geteuid(this_object()) + ")\n";

    val = 2 * fixnorm(me->query_stat(SS_DEX), 50) -
        fixnorm(me->query_prop(CONT_I_VOLUME), 60000) -
        fixnorm(me->query_encumberance_weight() +
        me->query_encumberance_volume() + 5, 65);


    foreach(int aid : m_indexes(attacks))
        tmp += attacks[aid][ATT_WCHIT] * attacks[aid][ATT_PROC];

    tmp /= 100;

    val += 4 * fixnorm(2 * tmp, 50);

    str += sprintf("\n%-20s %5d\n", "Offensive tohit:", val);

    foreach(int aid : m_indexes(attacks))
    {
        ac = attacks[aid][ATT_DAMT];

        if(ac & W_IMPALE)
            tmp = attacks[aid][ATT_M_PEN][0];
        else if(ac & W_SLASH)
            tmp = attacks[aid][ATT_M_PEN][1];
        else if(ac & W_BLUDGEON)
            tmp = attacks[aid][ATT_M_PEN][2];

        val += tmp * attacks[aid][ATT_PROC];
    }

    val /= 100;

    str += sprintf("%-20s %5d\n", "Offensive pen:", val);

    val = 2 * fixnorm(50, me->query_stat(SS_DEX)) - fixnorm(60000, me->query_prop(CONT_I_VOLUME)) - fixnorm(65,
        me->query_encumberance_weight() + me->query_encumberance_volume() + 5);

    if (sizeof((object *)me->query_weapon(-1) - ({ 0 })))
        tmp = me->query_skill(SS_PARRY);
    else
        tmp = me->query_skill(SS_UNARM_COMBAT) / 2;

    tmp += me->query_skill(SS_DEFENSE);

    val += 4 * fixnorm(70, tmp);

    str += sprintf("%-20s %5d\n", "Defensive tohit:", val);

    val = 0;

    foreach(int hid : m_indexes(hitloc_ac))
        val += hitloc_ac[hid][HIT_M_AC][0] * hitloc_ac[hid][HIT_PROC];

    val /= 100;

    str += sprintf("%-20s %5d\n", "Defensive ac:", val);

    str += "\nExp at kill: " + (F_KILL_GIVE_EXP(me->query_average_stat()) *
        (!i_am_real ? me->query_exp_factor() : 100) / 100) +
        "  Speed: " + me->query_prop(LIVE_I_QUICKNESS);

    arr = all_inventory(me);
    i = -1;
    size = sizeof(arr);

    while(++i < size)
        tmp += arr[i]->query_prop(OBJ_I_VALUE);

    str += "  Carried value: " + tmp + " (" + sizeof(arr) + ") objects.\n";

    return str;
}

/**
 * Resetujemy funkcje walki.
 */
public void
create_cbase()
{
}

/**
 * Wywo�ujemy funkcje create_cbase().
 */
public nomask void
create_object()
{
    CQME;

    panic_time  = time();
    set_speed   = CB_DEFAULT_SPEED;

    create_cbase();
}

/**
 * Ta funkcja jest wywo�ywana w momencie kiedy chcemy usun�� ten obiekt je�li
 * nie istnieje obiekt do kt�rego dolinkowany jest ten combat_object.
 */
public void
clean_up()
{
    if(!objectp(me))
        destruct();
}

/**
 * Funkcja redefiniowana po to, a�eby nie usun�� obiektu walki dla
 * obiektu, kt�ry istnieje.
 */
public void
remove_object()
{
    set_alarm(1.0, 0.0, "clean_up");
}

/**
 * Funkcja pr�buj�ca dolinkowa� objekt combata, je�li nie jest ju� do czego� dolinkowany,
 * do poprzedniego livinga, je�li nie uda si� dolinkowa� to usuwa ten obiekt.
 */
public void
cb_link()
{
    if(objectp(me))
        return;

    me = previous_living();

    if(me)
        i_am_real = !(me->query_npc());
    else
        destruct();
}

/**
 * @return Living do kt�rego dolinkowany jest obiekt walki.
 */
public object qme()
{
    return me;
}

/**
 * Sprawdza czy ten obiekt dolinkowany jest do jakiego� obiektu.
 * Je�li tak to go zwraca, je�li nie to usuwa obiekt.
 */
public nomask object check_qme()
{
    //Je�li nie ma obiektu to pr�bujemy go zlinkowa�
    //je�li si� nie da to usuwamy ten obiekt.
    if(!me)
    {
        cb_link();

        if(!me)
        {
            remove_object();
            return 0;
        }
    }
    else
        return me;
}

/**
 * Konfiguruje ataki i hitlokacje.
 */
public void
cb_configure()
{
    hitloc_ac   = ([]);
    attacks     = ([]);
}

/**
 * @return Rzeczywist� szybko�� walki w danym momencie.
 */
public int cb_query_real_speed()
{
    CQME;

    int s       = set_speed;
    int stl     = qme()->query_fight_style();
    int maxfat;

    s = PROC(s, STYLEMOD(CB_STL_MOD[stl][CB_STL_MOD_SZYBKOSC],
        qme()->query_skill(SS_STL_FIRST + stl))) / (!TP->query_atakowany_obszar() ? 1 : 2);

    return s + PROC((((maxfat = me->query_max_fatigue()) - me->query_fatigue()) / maxfat), 60);
}

/**
 * Zmienianie poziomu spanikowania.
 *
 * @param dpan o ile zmieniany jest poziom.
 */
public void
cb_add_panic(int dpan)
{
    CQME;

    int oldpan;

    oldpan = cb_query_panic();

    panic += dpan;

    if(panic < 0)
        panic = 0;

    if(!panic && oldpan)
        tell_object(me, "Uspokajasz si�.\n");
}

/**
 * @return poziom paniki.
 */
public int
cb_query_panic()
{
    CQME;

    int n, chk;

    if(me->query_enemy(0))
        panic_time = time(); /* So we don't heal panic while fighting */
    else
    {
        n = (time() - panic_time) / F_INTERVAL_BETWEEN_PANIC_HEALING;
        if(n > 0 && (chk = (1 << n)) > 0)
        {
            if(panic > 0)
                panic = panic / chk;

            panic_time += n * F_INTERVAL_BETWEEN_PANIC_HEALING;
        }
    }

    return panic;
}

/**
 * Sprawdza poziom paniki, i je�li jest on wystarczaj�co wysoki to powoduje
 * �e living ucieka.
 */
public void
cb_may_panic()
{
    CQME;

    if(random(cb_query_panic()) > (10 + (int)me->query_stat(SS_DIS) * 3))
    {
        tell_object(me, "Wpadasz w panik�!\n");
        tell_room(environment(me), QCIMIE(me, PL_MIA) + " wpada w panik�!\n", ({me}));

        me->query_team_others()->add_panic(25);
        me->run_away();
    }
}

/**
 * Funkcja sprawdza i updatuje warunki walki.
 */
public void
cb_update_combat(int extra_fatigue)
{
    CQME;

    int spd;
    int fatig;
    float f_spd;

    // Walka oczywi�cie troch� m�czy.
    fatig = me->query_encumberance_weight() / 15 + 1;

    if(me->query_fatigue())
        me->add_fatigue(-(fatig + extra_fatigue) * 100);

    if(me->query_fatigue() < 10)
    {
        tell_object(me, "Czujesz si� ekstremalnie zm�czon" +
            me->koncowka("y", "a", "e") + ".\n");
        me->reduce_hit_point(1);
    }

    //Walka powoduje, �e mo�emy spanikowa�.
    cb_may_panic();

    //A mo�e uda�o nam si� pokona� przeciwnika.
    cb_update_attack();

    if(real_speed != (spd = cb_query_real_speed()))
    {
       real_speed = spd;
       remove_alarm(alarm_id);

       f_spd = itof(spd) / 10.0;

       alarm_id = set_alarm(f_spd, f_spd, heart_beat);
    }
}

/**
 * Dzi�ki tej funkcji mo�emy ustawi� szybko�� walki czyli ilo�� atak�w
 * na minut�. Na minut� maksymalnie mo�na przeprowadzi� 60 a minimalnie
 * jeden atak.
 *
 * Standardow� warto�ci� jest 15 atak�w na minute.
 *
 * @param speed     ilo�� mo�liwych do przeprowadzenia atak�w na minute.
 */
public void
cb_set_speed(int speed)
{
    CQME;

    speed = max(min(speed, CB_MAX_SPEED), 1);

    set_speed = CB_SPEED_BASE / speed;
    real_speed = cb_query_real_speed();

    if(alarm_id)
        alarm_id = set_alarm(get_alarm(alarm_id)[2], itof(real_speed) / 10.0, heart_beat);
}

/**
 * @return szybkoo�� walki - ile razy w ci�gu minuty posta� atakuje.
 */
public int
cb_query_speed()
{
    CQME;

    int s   = CB_SPEED_BASE / set_speed;
    int stl = qme()->query_fight_style();

    s = PROC(s, STYLEMOD(CB_STL_MOD[stl][CB_STL_MOD_SZYBKOSC], qme()->query_skill(SS_STL_FIRST + stl)));

    return s;
}

/**
 * TODO: Ta funkcja musi zacz�� co� robi�:)
 *
 * Funkcja odpowiada za ustawienie obiektu walki odpowiednio podczas upicia
 * si� gracza.
 *
 * @param pintox    poziom upicia
 */
public void
cb_adjust_combat_on_intox(int pintox)
{
    CQME;

#if 0

    object *p;

    if (pintox < 90)
        return;

    p = all_inventory(environment(me));

    if (!sizeof(p))
        ;/* Here we check for neat things to do */
#endif
}

/**
 * Normalizuje warto�ci ofenywne i defensywne.
 */
static nomask int
fixnorm(int offence, int defence)
{
    if (offence + defence == 0)
        return 0;

    return ((100 * offence) / (offence + defence)) - 50;
}

/*
 * Function name: cb_update_tohit_val
 * Description:   Update the tohit value for some object. Changing ones
 *		  encumberance while fighting will not have any effect
 *		  unless this function is called, but I think it's worth
 *		  it since we save cpu.
 * Arguments:	  ob - The object we shall try to hit
 *		  weight - If the formula should be weighted any way.
 */
varargs void
cb_update_tohit_val(object ob, int weight)
{
    CQME;

    tohit_ob = ob;
    tohit_val = 2 * fixnorm(me->query_stat(SS_DEX), ob->query_stat(SS_DEX)) -
        fixnorm(me->query_prop(CONT_I_VOLUME), ob->query_prop(CONT_I_VOLUME)) -
        fixnorm(me->query_encumberance_weight() + me->query_encumberance_volume() + 5,
        ob->query_encumberance_weight() + ob->query_encumberance_volume() + 5);

    tohit_val += weight;
}

/**
 * Sprawdza, czy trafili�my nasz� ofiar�, czy te� nie.
 *
 * Ca�kowite nietrafienie jest bardzo ma�o prawdopodobne,
 * ale pozostaje, jednak mo�e si� to przyda�y� tylko osob�
 * o bardzo niskim poziomie wszystkiego.
 *
 * Czynniki wp�ywaj�ce na to czy trafimy:
 *  a) ze strony zadaj�cego cios:
 *    = hit broni,
 *    = umiej�tno�� w�adania broni�,
 *    = g�sto�� broni(waga * obj�to��),
 *    = umiej�tno�� w walce obranym stylem,
 *    = zr�czno��,
 *    = si��,
 *    = poziom upojenia alkoholowego lub te� narkotykowego,
 *    = dodatkowe warto�ci podytkowane warunkami walki:
 *      - podczas walki konnej umiej�tno�� takiej walki,
 *      - podczas walki w ciemno�ci umiej�tno�� za ni� odpowiadaj�ca,
 *
 *  b) ze strony broni�cego si� zale�ne jest to od tego
 *     jaki typ obrony b�dziemy pr�bowa� wykorzysta�:
 *    = dla unik�w:
 *     - umiej�tno�� unik�w,
 *     - zr�czno��,
 *     - umiej�tno�� w walce obranym stylem,
 *     - modyfikator od stylu walki,
 *     - poziom upojenia alkoholowego b�d� narkotykowego,
 *     - dodatkowe warunki opisane powy�ej,
 *    = dla parowania tarcz�:
 *     - umiej�tno�� tarczownictwa,
 *     - zr�czno��,
 *     - umiej�tno�� w walce obranym stylem,
 *     - modyfikator od stylu walki,
 *     - poziom upojenia alkoholowego b�d� narkotykowego,
 *     - dodatkowe warunki opisane powy�ej,
 *    = dla parowania broni�
 *     - umiej�tno�� parowania,
 *     - umiej�tno�� pos�ugiwania si� broni�,
 *     - zr�czno��,
 *     - umiej�tno�� w walce obranym stylem,
 *     - modyfikator od stylu walki,
 *     - poziom upojenia alkoholowego b�d� narkotykowego,
 *     - dodatkowe warunki opisane powy�ej.
 *
 * O tym jaki spos�b obrony spr�bujemy zastosowa� decyduje nasz styl walki,
 * styl walki przeciwnika, posiadane umiej�tno�ci oraz do�� spory random.
 * Podczas reakcji na atak pr�bujemy tylko 2 z tych 3 sposob�w, przy
 * czym przy drugim szansa jest ju� zmniejszona o po�ow�,
 * nale�y jednak pami�ta�, �e modyfikatory s� teraz tak ustawione,
 * a�by walka rozgrywa�a si� w kilku celnych i wielu niecelnych ciosach.
 *
 *  Funkcja zosta�a ca�kowicie przepisana w oparciu o dane z forum walki
 * i star� funkcj�.
 *
 * TODO:
 * 1) Po wykonaniu uniku, parowania, silnego niecelnego ciosu, czy te� otrzymaniu
 *    silnego ciosu w r�k� w�adaj�c� broni�(je�li to jedyna bro�), g�ow� lub korpus,
 *    b�d� os�ony po silnym ciosie mo�na odebra� graczowi tur�.
 *    (Przez cios silny rozumiem cios zdejmuj�cy graczowi powy�ej 50% hp z hitlokacji)
 * 2) Przy bardzo silnych ciosach, nawet tych wyparowanych przez zbroj� gracz mo�e
 *    si� przewr�ci�. Tutaj por�wna�bym szybko�� broni z si�� i wytrzyma�o�ci�.
 *
 * @param aid   identyfikator ataku
 * @param mod   tablic�:
 *              <ul>
 *                <li> zmodyfikowany hit broni </li>
 *                <li> waga broni </li>
 *                <li> obj�to�� broni </li>
 *                <li> umiej�tno�� pos�ugiwania si� broni� </li>
 *                <li> objekt broni kt�r� atakujemy </li>
 *              </ul>
 * @param vic   obiekt postaci kt�r� chcemy trafi�.
 *
 * @return Tablice w postaci
 *  <ul>
 *    <li><i>0</i> int > 0 - oznacza jak dobrze trafili�my
 *                           w przeciwnym wypadku informuje
 *                           nas o tym jak bardzo sp�d�owali�my.</li>
 *    <li><i>1</i> mixed   - W przypadku nietrafienia, mo�e przyjmowa�
 *                           nast�puj�ce warto�ci:
 *                           <ul>
 *                             <li><i>0</i>  - przeciwnik zrobi� unik</li>
 *                             <li><i>1</i>  - spud�owali�my</li>
 *                             <li><i>ob</i> - obiekt tarczy b�d� broni</li>
 *                           </ul></li>
 *  </ul>
 *
 * TODO: Funkcje mo�naby cz�ciowo rozbi� i przenie�� do ctool.c, teraz niepotrzebnie
 *       dla istot nienosz�cych broni i tarcz sprawdzana jest taka mo�liwo��,
 *       ale nie jest to zadanie priorytetowe, kod dzia�a w takiej formie jak jest
 *       a obci��enie dla systemu jest niewielkie, wi�c nie trzeba tego rusza�.
 */
public mixed
cb_tohit(int aid, mixed mod, object vic)
{
    CQME;

    if(!pointerp(mod) || sizeof(mod) != 5)
    {
        write("Wyst�pi� b��d nr. wal342, zg�o� go opisuj�c okoliczno�ci w jakich do niego dosz�o.\n");
        vic->catch_msg("Wyst�pi� b��d nr. wal342, zg�o� go opisuj�c okoliczno�ci w jakich do niego dosz�o.\n");
        return ({ 0, 1 });
    }

    //Najpierw troche danych potrzebnych nam do sprawdzenia czy trafimy w przeciwnika,
    //nie ma sensu pobiera� wi�cej wi�c zrobimy to p�niej.
    int  mstl   = qme()->query_fight_style();
    int  mssk   = ST2HUN(qme(), SS_STL_FIRST + mstl);
    int  mdex   = ST2HUN(qme(), SS_DEX);
    int  mupc   = PROC_INTOX(qme());
    int  mwsk   = mod[3];

    //Najpierw sprawdzimy czy w og�le trafimy. Wi�kszo�� nietrafie� to jednak uniki
    //przeciwnika - tych tu nie uwzgl�dniamy, wi�c trzeba by� prawdziw� lebiod�
    //i totalnie nie umie� pos�ugiwa� si� dan� broni�, nie mie� zr�czno�ci i
    //nie zna� obranego stylu walki

    if((mssk + 2 * mdex + 3 * mwsk) / 6 < random(20 + PROC(80, mupc)))
    {   //A jednak komu� si� uda�o nietrafi�:)
        DBG("Nietrafiony!");
        return ({ 0, 1 });
    }

    //Zbieramy reszte danych o zadaj�cym cios:
    int  mwges  = mod[1] / max(1, mod[2]);
    int  mhit   = mod[0];
    int  mstr   = ST2HUN(qme(), SS_STR);
    int  mobc   = PROC_WEIGHT(qme());
    int  mwped  = (mwges ? (mwges * mstr) / 100 : mstr);

    //Oraz dane o przyjmuj�cym cios:
    int  pstl   = vic->query_fight_style();
    int  pssk   = ST2HUN(vic, SS_STL_FIRST + pstl);
    int  pdex   = ST2HUN(vic, SS_DEX);
    int  pstr   = ST2HUN(vic, SS_STR);
    int  pobc   = PROC_WEIGHT(vic);
    int  pupc   = PROC_INTOX(vic);
    int  punik  = vic->query_skill(SS_DEFENCE);
    int  ptarcz = vic->query_skill(SS_SHIELD_PARRY);
    int  pparow = vic->query_skill(SS_PARRY);

    //Modyfikatory
    int *mstlm  = CB_STL_MOD[mstl];
    int *pstlm  = CB_STL_MOD[pstl];

#if 0
    DBG("mssk = " + mssk + ", mdex = " + mdex + ", mwsk " + mwsk + ", mwges = " + mwges + ", mhit = " + mhit + ", mstr = " + mstr +
        ", mobc = " + mobc + ", mwped = " + mwped + "\n         " +
        "pstl = " + pstl + ", pssk = " + pssk + ", pdex = " + pdex + ", pstr = " + pstr + ", pobc = " + pobc + ", pupc = " + pupc +
        ", punik = " + punik + ", ptarcz = " + ptarcz + ", pparow = " + pparow + "\n        " +
        "pstlm = ({" + pstlm[0] + ", " + pstlm[1] + ", " + pstlm[2] + ", " + pstlm[3] + ", " + pstlm[4] + ", " + pstlm[5] + ", " +
        pstlm[6] + "})\n            "+
        "mstlm = ({" + mstlm[0] + ", " + mstlm[1] + ", " + mstlm[2] + ", " + mstlm[3] + ", " + mstlm[4] + ", " + mstlm[5] + ", " +
        mstlm[6] + "})\n");

    DBG("STYLEMOD(100,  0)  = " + STYLEMOD(100,   0) + "\n          " +
        "STYLEMOD(100, 10)  = " + STYLEMOD(100,  10) + "\n          " +
        "STYLEMOD(100, 50)  = " + STYLEMOD(100,  50) + "\n          " +
        "STYLEMOD(100, 70)  = " + STYLEMOD(100,  70) + "\n          " +
        "STYLEMOD(100, 100) = " + STYLEMOD(100, 100) + "\n");
#endif

    //Uaktalniamy obiekt kt�ry od nas dostanie wpierdasz.
    if(vic != tohit_ob)
        cb_update_tohit_val(vic);

    mixed sposoby = vic->query_sposoby_obrony();    //dost�pne sposoby obrony

    int ilosc_sposobow, obrona;

    foreach(mixed sp : sposoby)
        if(pointerp(sp) && sizeof(sp) || sp && !pointerp(sp))
            ilosc_sposobow++;

    //Je�li �aden ze sposob�w nie jest dost�pny no to c�. Zostaje liczy� na zbroje.
    if(pointerp(sposoby) && ilosc_sposobow)
    {
        int sposob = -1, pwsk, s;
        object parry_ob;

        s = ilosc_sposobow;

        //Pr�bujemy max 2 razy wi�c ma�a pentelka
        while(s--)
        {
            //Najpierw sprawdzamy, kt�re sposoby s� w og�le dost�pne.
            if(ilosc_sposobow == 1)
            {   //Je�li dost�pny jest tylko jeden spos�b to nie ma �adnego problemu z wyborem.
                sposob = member_array(1, sposoby);
            }
            else
            {   //W przeciwnym wypadku trzeba troche policzy�.
                int *szanse = allocate(3);

                if(sposoby[CB_SPO_UNIKI] && sposob != CB_SPO_UNIKI)
                {   //Liczymy jaka jest szansa na unikni�cie. Najwi�kszy na to wp�yw
                    //ma um, obci��enie, upicie i styl walki, troche mniejsze zr�czno��,
                    //i styl walki przeciwnika.
                    szanse[CB_SPO_UNIKI] = SZANSA_UNIKU1(punik, pdex, pupc, pobc, STYLEMOD(pstlm[CB_STL_MOD_UNIKI], pssk));

//                         PROC(PROC((((3 * punik + pdex) / 4) / ((pupc + pobc) / 2)), ;
                }

                if(sposoby[CB_SPO_PAROWANIE] && sposob != CB_SPO_PAROWANIE)
                {   //Szans� na sparowanie opieramy w g��wnej mierze o umiej�tno�� parowania, pos�ugiwania
                    //si� broni� i styl walki, troche mniejszy wp�yw ma por�wnanie styl�w walki,
                    //zr�czno�� oraz upicie

                    //Je�li parujemy i mamy wi�cej ni� jedn� bro�, to musimy wylosowa�,
                    //kt�r� broni� sparujemy:

                    /* TODO: przyda�o by si� jako� liczy� szanse na sparowanie dan� broni�,
                     *       ale narazie nie chce mi si� tego robi�. [Krun]
                     */
                    parry_ob = sposoby[CB_SPO_PAROWANIE][random(sizeof(sposoby[CB_SPO_PAROWANIE]))];

                    pwsk = vic->query_skill(SS_WEP_FIRST + parry_ob->query_wt());

                    szanse[CB_SPO_PAROWANIE] = SZANSA_SPAROWANIA1(pparow, pwsk, pdex, pupc, pobc,
                        STYLEMOD(pstlm[CB_STL_MOD_PAROWANIE], pssk), parry_ob);

//                         PROC(PROC((((3 * pparow + 3 * pwsk + pdex) / 7) / (2 * pupc + pobc) / 3),
                }

                if(sposoby[CB_SPO_TARCZOWNICTWO] && sposob != CB_SPO_TARCZOWNICTWO)
                {   //Szansa na tarczownictwo opiera si� g��wnie na: umiej�tno�ci tarczownictwa,
                    //zr�czno�ci i stylu walki, dodatkow pod uwag� brane s�: por�wnanie styl�w
                    //walki, si�a oraz poziom upicia.
                    parry_ob = sposoby[CB_SPO_TARCZOWNICTWO][random(sizeof(sposoby[CB_SPO_TARCZOWNICTWO]))];

                    szanse[CB_SPO_TARCZOWNICTWO] = SZANSA_TARCZOWNICTWA1(ptarcz, pdex, pstr, pupc, pobc,
                        STYLEMOD(pstlm[CB_STL_MOD_TARCZOWNICTWO], pssk), parry_ob);

//                         PROC(PROC((((3 * ptarcz + 3 * pdex + pstr + pobc) / 8) / pupc),
                }

//                 DBG("Szanse = ({ " + szanse[CB_SPO_UNIKI] + ", " + szanse[CB_SPO_PAROWANIE] + ", " + szanse[CB_SPO_TARCZOWNICTWO] + "});");

                //Przeliczamy na szanse procentowe
                int sum = fold(szanse, &operator(+)(,), 0);

                if(!sum)
                    break;

                szanse[CB_SPO_UNIKI]           = TO_PROC(sum, szanse[CB_SPO_UNIKI]);
                szanse[CB_SPO_PAROWANIE]       = TO_PROC(sum, szanse[CB_SPO_PAROWANIE]);
                szanse[CB_SPO_TARCZOWNICTWO]   = TO_PROC(sum, szanse[CB_SPO_TARCZOWNICTWO]);

                //TODO: Modyfikacja szans w przypadku walki konnej od tego uma, + do tarczownictwa i - do parowania.

//                 DBG("Szanse = ({ " + szanse[CB_SPO_UNIKI] + ", " + szanse[CB_SPO_PAROWANIE] + ", " + szanse[CB_SPO_TARCZOWNICTWO] + "});");

                int r = random(100);

                sum = 0;

                while(r < (sum += szanse[sposob++]));
            }

            DBG("Spos�b = " + sposob);

            //Ufff wiemy ju� jakim sposobem b�dziemy pr�bowali si� broni�:)
            //to teraz jeszcze musimy zobaczy� czy nam si� uda.

            int szansa;
            //I teraz zn�w liczymy zale�nie od sposobu.

            //WARNING: Szansa na niewykonanie czegokolwiek powinna
            //         wynosi� dla postaci o mniej wi�cej r�wnych statystykach
            //         oko�o 5%, czyli co 20 cios jest trafiony. Wiem, �e to ma�o
            //         ale w ten spos�b b�dziemy mieli bardziej rzeczywist� walk�
            //         a si�e cios�w podkr�ci si� tak, �eby przy walce r�wnych
            //         postaci wystarczy�o oko�o 3 w jeden obszar hitlock�w.
            switch(sposob)
            {
                //No to teraz kwiatuszek dla balansatora, ale w tym musimy uwzgl�dni� warunki z obu stron:)
                case CB_SPO_UNIKI:
                    /* Modyfikatory dodatnie:
                     *  - uniki,
                     *  - zr�czno��,
                     *  - modyfikator ze stylu walki,
                     *  - upicie przeciwnika
                     *  - obci��enie przeciwnika,
                     * Modyfikatory ujemne:
                     *  - zr�czno�� przeciwnika,
                     *  - um broni przeciwnika,
                     *  - p�d broni przeciwnika,
                     *  - hit broni przeciwnika,
                     *  - upicie,
                     *  - obci��enie,
                     * Oraz Modikatory ze stylu.
                     */
                    szansa = SZANSA_UNIKU2(punik, pdex, mupc, mobc, mdex, mwsk, mwped, mhit, pupc, pobc,
                        STYLEMOD(pstlm[CB_STL_MOD_TARCZOWNICTWO], pssk));

                    //TODO: Modyfikacja szansy od walki konnej - minus od 50 do 75% zale�nie od umiej�tno�ci
                    //      walka konna.
                    break;
                case CB_SPO_PAROWANIE:
                    /* Modyfikatory dodatnie:
                     *  - um parowania,
                     *  - zr�czno��,
                     *  - si�a,
                     *  - um broni,
                     *  - upicie przeciwnika,
                     *  - obci��enie przeciwnika,
                     * Modyfikatory ujemne:
                     *  - um broni przeciwnika,
                     *  - zr�czno�� przeciwnika,
                     *  - p�d broni przeciwnika,
                     *  - hit broni przeciwnika,
                     *  - upicie,
                     *  - obci��enie,
                     * Oraz Modikatory ze stylu.
                     */
                    szansa = SZANSA_SPAROWANIA2(pparow, pdex, pstr, pwsk,
                        mupc, mobc, mdex, mwsk, mwped, mhit, pupc, pobc,
                        STYLEMOD(pstlm[CB_STL_MOD_TARCZOWNICTWO], pssk));

                    //TODO: Modyfikacja parowania do walki konnej - minus oko�o 15%-40%
                    //      w zale�no�ci od umiej�tno�ci walki konnej.

                    break;
                case CB_SPO_TARCZOWNICTWO:
                    /* Modfikatory dodatnie:
                     *  - um tarczownictwa
                     *  - zr�czno��
                     *  - si�a
                     *  - poziom upicia przeciwnika
                     *  - poziom obci��enia przeciwnika
                     * Modyfikatory ujemne:
                     *  - um broni przeciwnika
                     *  - zr�czno�� przeciwnika
                     *  - p�d broni przeciwnika
                     *  - poziom upojenia alkoholowego
                     * Oraz Modikatory ze stylu
                     */
                    szansa = SZANSA_TARCZOWNICTWA2(ptarcz, pdex, pstr, mupc, mobc, mwsk, mdex, mwped, pupc,
                        STYLEMOD(pstlm[CB_STL_MOD_TARCZOWNICTWO], pssk));

                    //TODO: Modyfikacja szans od walki konnej - minus oko�o 5%-20% w zale�no�ci od umiej�tno�ci
                    //      walka konna
                    break;
            }

            //Modyfikujemy szanse na obrone w zale�no�ci od pozosta�ych warunk�w walki:

            if(!CAN_SEE_IN_ROOM(qme()) || !CAN_SEE(qme(), vic))
            {
                szansa += PROC(szansa, max(100, (qme()->query_skill(SS_BLIND_COMBAT) / 2) + 50));

                //TODO: Do przeniesienia i uzale�nienia od obra�e�. [Krun]
                if(!qme()->query_prop(EYES_CLOSED)) //nie ma tak letko! V.
                    qme()->increase_ss(SS_BLIND_COMBAT, EXP_WALKA_W_CIEMNOSCI_ZA_CIOS_BLINDCMBT);
            }

            if(!CAN_SEE_IN_ROOM(vic) || !CAN_SEE(vic, qme()))
            {
                szansa = PROC(szansa, max(100, (qme()->query_skill(SS_BLIND_COMBAT) / 2) + 50));

                //TODO: J.w. [Krun]
                if(!qme()->query_prop(EYES_CLOSED)) //nie ma tak letko! V.
                    qme()->increase_ss(SS_BLIND_COMBAT, EXP_WALKA_W_CIEMNOSCI_ZA_CIOS_BLINDCMBT);
            }

            /*
             * Sprytny haczyk autorstwa Silvathraeca na walke z wieloma
             * przeciwnikami. Gdy uderzamy osobe, ktora nie uderza nas,
             * ma ona dwa razy mniejsza obrone.
             * Edytowa�em troch� ten hahahaczyk. szansa obrony wynosi 75-90% [Krun]
             */
            if(vic->query_attack() != qme())
                szansa = PROC(75 + random(15), szansa);

            //Je�li to druga pr�ba to mamy 40% szansy mniej
            int rnd = random(100);

            int mszansa = szansa + (MAX(2, ilosc_sposobow) == (s + 1) ? 0 : 40);
//             DBG("Szansa: " + szansa + ", rnd = " + rnd + " > 100 - " + mszansa + " = " + (100 - mszansa));

            if(random(100) > 100 - mszansa)
            {
                obrona = -szansa / 2;
//                 DBG("Obrona: " + obrona);
                break;
            }
            else if(!s)
                obrona = szansa / 2;
        }

        if(obrona < 0)
        {
            if(objectp(parry_ob))
                parry_ob->did_parry();

            /* TODO: Doda� exp za obrone:
             */
            switch(sposob)
            {
                case CB_SPO_UNIKI:
                    break;
                case CB_SPO_PAROWANIE:
                    break;
                case CB_SPO_TARCZOWNICTWO:
                    break;
            }

            return ({ -obrona, 0 });
        }
    }

    //Huh, je�li tu jeste�my, to znaczy, �e przeciwnikowi nie uda�o si� w �aden spos�b wybroni� i
    //pozosta�o mu jeszcze tylko ac na zbroi, ale to ju� jest sprawdzane w innej funkcji wi�c musimy
    //policzy� obra�enia.

    //No to, �eby nie by�o, �e tu posz�o szybko, to te� trzeba by�o ciekawy wzorek zastosowa�.
    int obrazenia = OBRAZENIA(mwped, mwsk, mhit, mstlm[CB_STL_MOD_OBRAZENIA]);

    int d1, d2;

    int obr = 4 * fixnorm(d1 = (random(obrazenia) + random(obrazenia) + random(obrazenia)) / 3, (d2 = random(obrona)));

    //TODO: Doda� do obra�e� modyfikator od wra�liwo�ci je�li spe�nione s� odpowiednie warunki.

    DBG("Obra�enia = " + obr + "");

//     DBG(sprintf(">>> hit: %d(%d), def: %d(%d) => fn: %d + %d = %d.\n",
//         d1, obr, d2, obrona, obrazenia, tohit_val, obrazenia + tohit_val));

    return ({ obr, 0 });
}

#if 0
    int d1, d2;
    int whit = 4 * fixnorm(d1 = (random(wchit) + random(wchit) + random(wchit)) / 3, (d2 = random(defense)));

    me->catch_msg(sprintf(">>> hit: %d(%d), def: %d(%d) => fn: %d + %d = %d.\n",
        d1, wchit, d2, defense, whit, tohit_val, whit + tohit_val));

    whit += tohit_val;

    if (whit > 0)
    {
        int exp;
        /* trafili�my i wesz�o! */
        mixed tmp = 0, tmp1 = 0, tmp2 = 0;
        if (tmp = me->query_weapon(TS_HANDS))
        {
            me->increase_ss(tmp->query_wt() + SS_WEP_FIRST,
                (exp=PODLICZ_EXP(whit,EXP_WALKA_TRAFILISMY_SKILLBRONI,tmp->query_pen())));
            cb_update_weapon(tmp);
        }

        if (tmp1 = me->query_weapon(TS_R_HAND))
        {
            me->increase_ss(tmp1->query_wt() + SS_WEP_FIRST,
                (exp=PODLICZ_EXP(whit,EXP_WALKA_TRAFILISMY_SKILLBRONI,tmp1->query_pen())));
            cb_update_weapon(tmp1);
        }

        if (tmp2 = me->query_weapon(TS_L_HAND))
        {
            me->increase_ss(tmp2->query_wt() + SS_WEP_FIRST,
                (exp=PODLICZ_EXP(whit,EXP_WALKA_TRAFILISMY_SKILLBRONI,tmp2->query_pen())));
            cb_update_weapon(tmp2);
        }

        if (!tmp && !tmp1 && !tmp2)
        {
            me->increase_ss(SS_UNARM_COMBAT, (exp=PODLICZ_EXP2(whit,EXP_WALKA_TRAFILISMY_WALKAWRECZ)));
        }

        me->increase_ss(SS_DIS, (exp = PODLICZ_EXP2(whit,EXP_WALKA_TRAFILISMY_DIS)));

        if (sizeof(this_object()->cb_query_weapon(-1)) > 1)
            me->increase_ss(SS_2H_COMBAT, EXP_WALKA_TRAFILISMY_2H_COMBAT);

        return ({ whit, 0 });
    }

    /* Zmienna tohit_val wpisuje sie na konto uniku.
     */
    if (tohit_val < 0)
        what_defended[1] -= tohit_val;

    /* Generalnie rzecz biorac, parowanie jest duzo bardziej naturalne
     * i duzo bardziej czeste, niz inne formy unikania ciosow. Nieraz
     * nawet mogac uniknac ciosu, wolimy go sparowac. Powinnismy
     * uwzglednic to w opisach.
     */
    if (what_defended[0] > 0)
    {
        defense = what_defended[0];
        what_defended[1] -= defense;
        what_defended[0] += defense;
    }

    vic->catch_msg(sprintf("DEF: %d - %d - %d.\n",
        what_defended[0], what_defended[1], what_defended[2]));

    defense = random(what_defended[0] + what_defended[1] + what_defended[2]);

    if (defense < what_defended[0])
    {
        if(co_paruje == 2)
        {
            if(def[1]->is_weapon()) // sparowa�
                vic->increase_ss(SS_PARRY, EXP_WALKA_SPAROWALEM_PARRY);
            else //Odbi� tarcz�
                vic->increase_ss(SS_SHIELD_PARRY, EXP_WALKA_SPAROWALEM_PARRY);
        }

        /* przeciwnik sparowa� lub odbi� tarcz�.*/
        vic->increase_ss(SS_STR, EXP_WALKA_SPAROWALEM_STR);
        vic->increase_ss(SS_DEX, EXP_WALKA_SPAROWALEM_DEX);
        vic->increase_ss(SS_CON, EXP_WALKA_SPAROWALEM_CON);
        vic->increase_ss(SS_AWARENESS, EXP_WALKA_SPAROWALEM_AWARENESS);

        def[1]->did_parry(); // Dajemy znac parujacej broni/tarczy.
        return ({ (whit - 1), def[1] });
    }
    else if (defense < (what_defended[0] + what_defended[1]))
    {
        /* przeciwnik zrobi� unik */
        vic->increase_ss(SS_STR, EXP_WALKA_UNIKNALEM_STR);
        vic->increase_ss(SS_DEX, EXP_WALKA_UNIKNALEM_DEX);
        vic->increase_ss(SS_CON, EXP_WALKA_UNIKNALEM_CON);
        vic->increase_ss(SS_DEFENSE, EXP_WALKA_UNIKNALEM_DEFENSE);
        vic->increase_ss(SS_AWARENESS, EXP_WALKA_UNIKNALEM_AWARENESS);

        return ({ (whit - 1), 0 });
    }
    else
    {
        /* nie trafili�my */
        return ({ (whit - 1), 1 });
    }

    return 0;
}
#endif

/*
 * Function name: cb_try_hit
 * Description:   Decide if we a certain attack fails because of something
 *                related to the attack itself, ie specific weapon that only
 *                works some of the time. This is supposed to be
 *                replaced by a more intelligent routine in creature and
 *                humanoid combat. (called from heart_beat)
 * Arguments:     aid:   The attack id
 * Returns:       True if hit, otherwise 0.
 */
public int
cb_try_hit(int aid)
{
    CQME;

    return 1;
}

/*
 * Function name: cb_got_hit
 * Description:   Tells us that we got hit. It can be used to reduce the ac
 *                for a given hitlocation for each hit. This is supposed to be
 *                replaced by a more intelligent routine in creature and
 *                humanoid combat. (called from cb_hit_me)
 * Arguments:     hid:   The hitloc id
 *                ph:    The %hurt
 *                att:   Attacker
 *		  aid:   The attack id of the attacker
 *                dt:    The damagetype
 *		  dam:	 The number of hitpoints taken
 */
public varargs object
cb_got_hit(int hid, int ph, object att, int aid, int dt, int dam)
{
    CQME;

    return 0;
}

/**
 * Dzi�ki tej funkcji sprawdzamy opis danego ataku.
 *
 * @param aid   id ataku
 * @param przyp przypadek w jakim ma zosta� podany opis ataku,
 *              nieaktywne w przypadku zwyk�ych cios�w.
 *
 * @return opis ataku.
 */
public string
cb_attack_desc(int aid, int przyp = PL_NAR)
{
    CQME;

    switch(przyp)
    {
        case PL_MIA: return "cia�o";
        case PL_DOP: return "cia�a";
        case PL_CEL: return "cia�u";
        case PL_BIE: return "cia�o";
        case PL_NAR: return "cia�em";
        case PL_MIE: return "ciele";
    }
}

/**
 * @param aid   Identyfikator ataku
 *
 * @return Rodzaj nazwy ataku.
 */
public int
cb_query_attack_rodzaj(int aid)
{
    CQME;

    return PL_NIJAKI_NOS;
}

/**
 * @param id ataku
 *
 * @return Obiekt przyporz�dkowany do ataku o podanym id lub 0 je�li takiego
 *         obiektu nie ma.
 */
public object
cb_query_attack_object(int id)
{
    return 0;
}


/**
 * @param hid   Identyfikator hitlokacji.
 * @param przyp Przypadek(opt.)
 *
 * @return opis hitlokacji o podanym id w podanym przypadku
 */
public string
cb_hitloc_desc(int hid, int przyp = 0)
{
    CQME;

    string przym;

    if(!is_mapping_index(hid, hitloc_ac))
        return ({"korpus", "korpusu", "korpusowi", "korpus", "korpusem", "korpusie"})[przyp];

    przym = "";

    foreach(string *p : hitloc_ac[hid][HIT_PRZYM])
        przym += oblicz_przym(p[0], p[1], przyp, hitloc_ac[hid][HIT_RODZ], (hitloc_ac[hid][HIT_RODZ] < 0 ? 1 : 0)) + " ";

    return przym + hitloc_ac[hid][HIT_DESC][0][przyp];
}

/**
 * Wysy�a opis walki do ludzi kt�rzy chc� j� zobaczy�
 *
 * @param str   String do wy�wietlenia na ekranie gracza
 * @param enemy Wr�g z kt�rym walczymy
 * @param arr   Pojedynczy obiekt lub tablica obiekt�w, kt�rym wiadomo�� ma nie zosta�
 *              przekazana
 */
varargs void
tell_watcher(string str, object enemy, mixed arr)
{
    CQME;

    object *obs;

    obs = all_inventory(environment(me)) - ({ me, enemy });

    if(arr)
    {
        if(pointerp(arr))
            obs -= arr;
        else
            obs -= ({ arr });
    }

    foreach(object x : obs)
    {
        if(!x->query_option(OPT_FIGHT_BLOOD) && CAN_SEE_IN_ROOM(x))
        {
            if(CAN_SEE(x, me))
                x->catch_msg(str);
            else
            {
                tell_object(x, enemy->query_Imie(x, PL_MIA) +
                    " zostaje przez kogo� trafion" +
                    enemy->koncowka("y", "a", "e") + ".\n");
            }
        }
    }
}

/**
 * G��wnym zadaniem tej funkcji jest opisanie obronionego, czy te� przyj�tego ciosu
 * jak te� i opisie doznanych obra�e�.
 *
 * @param aid       id ataku
 * @param hid       id przyjmuj�cej hitlokacji
 * @param phurt     % zadanych obra�e�
 * @param enemy     wr�g kt�ry dosta� cios.
 * @param dt        typ obra�e�
 * @param phit      procentowy sukces
 * @param dam       uszkodzenia w hit pointach
 * @param tohit     jak dobre by�o uderzenie
 * @param def_ob    objekt kt�rym si� bronili�my
 * @param armours   zbroje na hitlokakacji kt�r� trafiali�my - s� tu obiekt
 *                  tylko tych zbroi, kt�re zosta�y przebite i tej na kt�rej
 *                  bro� si� zatrzyma�a(narazie tylko pierwszej:P)
 * @param wt        typ rany
 * @param wl        hitlokacja na kt�rej jest rana
 * @param ho        obszar na kt�rym mamy handicapa.
 */
public void
cb_did_hit(int aid, int hid, int phurt, object enemy, int dt,
    int phit, int dam, int tohit, mixed def_ob, object *armours,
    int wt = 0, int wl = 0, int ho = 0)
{
    CQME;

    if((!objectp(enemy)) || (!objectp(me)))
        return;

    string *opisy = ({});

    int is_enemy_real = interactive(enemy);

    if(phurt < 0)
    {
        cb_add_panic(1);

        string zaimek = enemy->koncowka("ten", "ta", "ono");

        // TODO: To wszystko mo�e z powodzeniem zosta� przeniesione do ctoola.c
        //       bo zwyk�ym livingom, kt�re nie umiej� pos�ugiwa� si� broni�
        //       nie jest zupe�nie potrzebne
        if(objectp(def_ob))
        {
            if(def_ob->check_weapon()) // paruje bron
                opisy = OW_OPISZ_PAROWANIE(qme(), enemy, aid, hid, dt, def_ob);
            else //Os�aniamy si� tarcz�
                opisy = OW_OPISZ_TARCZOWNICTWO(qme(), enemy, aid, hid, dt, def_ob);
        }
        else if (!def_ob) /* unik broniacego */
            opisy = OW_OPISZ_UNIK(qme(), enemy, aid, hid, dt);
        else /* nietrafienie (nieumiejetnosc atakujacego)*/
        {
            //Ten spos�b rozwi�zywania walki powinien by� tak rzadki,
            //�e raczej nie ma sensu jako� specjalnie formatowa� opis�w
            //walki dla niego.

            string with = cb_attack_desc(aid);

            if(i_am_real)
            {
                me->catch_msg("Nie udaje ci si� trafi� " +
                    enemy->query_imie(me, PL_DOP) + " " + with + ".\n");
            }

            if(is_enemy_real)
            {
                enemy->catch_msg(me->query_Imie(enemy, PL_CEL) + " nie udaje " +
                    "si� trafi� ciebie " + with + ".\n");
            }

            tell_watcher(QCIMIE(me, PL_CEL) + " nie udaje si� trafi� " +
                QIMIE(enemy, PL_DOP) + " " + with + ".\n", enemy);

            return;
        }
    }

    if((phurt == 0) && sizeof(armours)) // zbroja wyparowala
        opisy = OW_OPISZ_WYPAROWANIE(qme(), enemy, aid, hid, dt, armours[0]);

    if(sizeof(opisy) == 3)
    {
        if(i_am_real)
            me->catch_msg(opisy[0]);

        if(is_enemy_real)
            me->catch_msg(opisy[1]);

        tell_watcher(opisy[2], enemy);

        return;
    }

    //Sprawdzili�my czy cios doszed�, je�li doszli�my do tego momentu to znaczy, �e tak:)

    object  wep;
    mixed  *obrazenia;
    string  opis_ciosu,
            przymiotnik,
            rodzaj_ciosu,
           *opis_rany,
           *opis_handicapa,
            zaimek;

    cb_add_panic(-3 - (phurt / 5));

    zaimek = enemy->query_zaimek(PL_BIE, 0);

    opisy = OW_OPISZ_CIOS(qme(), enemy, aid, hid, dt, phurt);

    if(i_am_real)
        me->catch_msg(opisy[0]);

    if(is_enemy_real)
        me->catch_msg(opisy[1]);

    tell_watcher(opisy[2], enemy);

    // TODO: tymczasowe rozwi�zanie - nale�y przenie�� do obiektu livinga
    if(ho < 0)
    {
        enemy->catch_msg("HANDICAP!!!\n");
        int hitloc = (-1) * ho;

        if(hitloc > 1000)
            hitloc /= 1000;

        enemy->catch_msg("hitloc = " + hitloc + "\n");

        if(member_array(hitloc, enemy->query_combat_object()->query_hitloc_area_members(HA_P_REKA)) > -1)
        {
            enemy->catch_msg("PRAWA R�KA!!!\n");
            mixed tmp;

            if(tmp = enemy->query_weapon(TS_HANDS))
                tmp->unwield_me();

            if(tmp = enemy->query_weapon(TS_R_HAND))
                tmp->unwield_me();

            enemy->disable_attack(W_RIGHT);
        }
        else if(member_array(hitloc, enemy->query_combat_object()->query_hitloc_area_members(HA_L_REKA)) > -1)
        {
            enemy->catch_msg("LEWA R�KA!!!\n");
            mixed tmp;

            if (tmp = enemy->query_weapon(TS_HANDS))
                tmp->unwield_me();

            if (tmp = enemy->query_weapon(TS_L_HAND))
                tmp->unwield_me();

            enemy->disable_attack(W_LEFT);
        }
        else if(member_array(hitloc, enemy->query_combat_object()->query_hitloc_area_members(HA_P_NOGA)) != -1)
        {
            //TODO: Zmiana opisu chodzenia, modyfikator do zm�czenia

            enemy->disable_attack(W_FOOTR);
        }
        else if(member_array(hitloc, enemy->query_combat_object()->query_hitloc_area_members(HA_L_NOGA)) != -1)
        {
            //TODO: Zmiana opisu chodzenia, modyfikator do zm�czenia

            enemy->disable_attack(W_FOOTL);
        }
        else if(member_array(hitloc, enemy->query_combat_object()->query_hitloc_area_members(HA_KORPUS)) != -1)
        {
            //TODO: Og�uszenie gracza
        }
        else if(member_array(hitloc, enemy->query_combat_object()->query_hitloc_area_member(HA_GLOWA)) != -1)
        {
            //TODO: Og�uszenie gracza
        }
    }
//     DBG("A tu nie");
}

public varargs mixed
cb_query_defense(int weight, int bs=0)
{
    CQME;

    return 0;
}

public int
filter_to_reward(object ob)
{
    CQME;

    return (living(ob) && ((member_array(ob, enemies) != -1) ||
        (member_array(me, ob->query_enemy(-1)) != -1)));
}

/**
 * Funkcja maj�ca na celu nagrodzenie gracza zar�wno za przeprowadzenie celnego ciosu
 * jak i za zabicie przeciwnika,
 *
 * @param attacker  Atakuj�cy
 * @param dam       Uszkodzenia
 * @param kill      Czy uda�o si� zabi�.
 *
 * @TODO Do pe�nego przepatrzenia i przepisania, jedyne co jest dobre to increase_ss.
 */
public void
cb_reward(object attacker, int dam, int kill)
{
    CQME;

    int tmp, j, align, size;
    object *share, *others;

    if(!kill)
    {
        attacker->add_exp(dam, 1);
        return;
    }

    tmp = F_KILL_GIVE_EXP(me->query_cmb_average_stat());
    if (!i_am_real)
        tmp = me->query_exp_factor() * tmp / 100;

    share = filter(all_inventory(environment(me)), filter_to_reward);

    if(size = sizeof(share))
    {
        tmp /= size;

        while(--size >= 0)
            share[size]->add_exp(tmp, 1);
    }

    /*
     * Change the alignment of the killer
     */
    attacker->set_alignment((align = attacker->query_alignment()) +
        F_KILL_ADJUST_ALIGN(align, me->query_alignment()));

    //Je�li zabili�my to robimy nagradzamy w nast�puj�cy spos�b.
    attacker->increase_ss(SS_DIS, EXP_WALKA_ZABILISMY_DIS);
}

/*
 * Function name: cb_death_occured
 * Description:   Called when 'me' dies
 * Arguments:     killer: The enemy that caused our death.
 */
public varargs void
cb_death_occured(object killer, int oglusz=0)
{
    CQME;

    int il, size;
    object *tm;

    if(killer->query_option(OPT_FIGHT_MERCIFUL) && query_interactive(me) &&
        !me->query_npc() || oglusz)
    {
        if(living(killer))
            tell_roombb(environment(me), QCIMIE(me, PL_MIA) + " osuwa si^e na ziemi^e.\n", ({ me }), killer);
    }

    /*
     * Tell everyone the bad (good) news.
     */
    if(!oglusz)
    {
        mixed die_text;
        //Dorobi�em motywik z mo�liwo�ci� zmienienia tego tekstu przez propa:P
        if((die_text = me->query_prop(LIVE_I_DIE_TEXT)))
            if(pointerp(die_text))
                die_text = die_text[random(sizeof(die_text))];

        if(!die_text)
            die_text = "poleg�" + me->koncowka("", "a", "o");

        tell_roombb(ENV(me), set_color(COLOR_FG_CYAN) + QCIMIE(me, PL_MIA) + " " +
            die_text + "." + clear_color() + "\n", ({me}), ({me}));

        tell_object(killer, set_color(killer, COLOR_FG_GREEN)+"Zabi�" + killer->koncowka("e�", "a�", "o�") + " " +
            me->query_imie(killer, PL_BIE) + ".\n" + clear_color(killer));

        if (living(killer))
            tell_roombb(environment(me), QCIMIE(killer, PL_MIA) + " zabi�" +
                killer->koncowka("", "a", "o") + " " +
                QIMIE(me, PL_BIE) + ".\n", ({me, killer}), killer);
    }
    stop_heart();

    /*
     * Reward for kill (den enes d|d, den andres br|d)
     */
    cb_reward(killer, 0, 1);

    /*
     * We forget our enemies when we die.
     */
    enemies = ({});
    to_stun = ({});
    attacked_by = ({});

    attack_ob = 0;

    /*
     * Adjust panic values
     */
    killer->add_panic(-25); /* Killing the enemy reduces panic */
    tm = (object*)killer->query_team_others();
    il = -1;
    size = sizeof(tm);

    while(++il < size)
        if(environment(killer) == environment(tm[il]))
            tm[il]->add_panic(-15);

    tm = (object*)me->query_team_others();
    il = -1;
    size = sizeof(tm);

    while(++il < size)
        if(environment(me) == environment(tm[il]))
            tm[il]->add_panic(25);
}

/**
 * Dodaje \param wrog do tablicy wrog�w lub tablicy
 * do og�uszenia dla 'me'.
 * Wywo�ywana w przypadku gdy my zaatakujemy kogo�
 * lub kto� zaatakuje nas, ale nie tylko.
 *
 * @param wrog      Dodawany wr�g.
 * @param force     Je�li jest prawdziwe i nie jest -1 lub -2 to
 *                  ustawiamy \param wrog jako pierwszego na li�cie wrog�w.
 *                  Je�li -1 to na li�cie do og�uszenia a je�li -2 to na
 *                  pierwszym miejscu tej listy.
 */
public varargs void
cb_add_enemy(object wrog, int force)
{
    CQME;

    int pose, poss, es, ss, su, tmp;

    cb_update_enemies();
    /* Make sure panic value is updated before we add enemies */
    cb_query_panic();

    poss = member_array(wrog, to_stun);
    pose = member_array(wrog, enemies);

    if(force == -2 || force == -1)
    {
        if(pose >= 0)
            enemies = exclude_array(enemies, pose, pose);

        force = -(1 + force);

        if(poss >= 0)
        {
            if(force)
                to_stun = ({wrog}) + exclude_array(to_stun, poss, poss);
        }
        else if(force)
            to_stun = ({wrog}) + to_stun;
        else
            to_stun += ({wrog});
    }
    else
    {
        if(poss >= 0)
            to_stun = exclude_array(to_stun, poss, poss);

        if(force && pose >= 0)
            enemies = ({wrog}) + exclude_array(enemies, pose, pose);
        else if(force)
            enemies = ({wrog}) + enemies;
        else if(pose < 0)
            enemies += ({wrog});
    }

    es = sizeof(enemies);
    ss = sizeof(to_stun);
    su = es + ss;

    if(su > MAX_ENEMIES)
    {
        tmp = random(su);

        if(tmp < es)
        {
            //TODO: Nie wiem czy tu nie powinno by� jakiego� komunikatu,
            //      Sprawdzenia czy nie s� na tej samej lokacji.
            enemies = slice_array(enemies, 0, MAX_ENEMIES - 1 - ss);
        }
        else
        {
            //TODO: To samo co wy�ej.
            to_stun = slice_array(to_stun, 0, MAX_ENEMIES - 1 - es);
        }
    }
    else if(member_array(wrog, attacked_by) == -1)
    {
        //Zawsze znajdzie si� miejsce dla wroga, kt�ry jest
        //w�r�d atakuj�cych a z kt�rym chcemy si� bi�.
        if(wrog->query_attack() == me && attack_ob == wrog)
        {
            if(sizeof(attacked_by) == MAX_ATTACKING_ENEMIES)
            {
                object wypadniety;
                wypadniety = attacked_by[random(MAX_ATTACKING_ENEMIES - 1)];

                wypadniety->catch_msg("Wypadasz z kr�gu walcz�cych z " +
                    QIMIE(me, PL_NAR) + ".\n");

                wypadniety->no_more_attacked_by(me);
                attacked_by = attacked_by - ({wypadniety});
            }

            attacked_by = ({wrog}) + attacked_by;

            wrog->catch_msg("Zaczynasz wymienia� ciosy z " + QCIMIE(me, PL_NAR) + ".\n");
        }
        else if(wrog->query_attack() == me) // Je�li to kto� nas zaatakowa�.
        {
            if(sizeof(attacked_by) < MAX_ATTACKING_ENEMIES)
                attacked_by += ({wrog});
        }
    }
}

/**
 * @return Zwraca uma pos�ugiwania si� aktualnie dzier�on� broni�,
 *         je�li aktualnie nie jest dzier�ona �adna bro� zwraca
 *         uma walka bez broni.
 */
public varargs int
cb_weapon_skill()
{
    CQME;

    return me->query_skill(SS_UNARM_COMBAT);
}

/**
 * Funkcja zadaje cios w plecy przeciwnikowi.
 */
public void
cb_zadaj_cios_w_plecy()
{
    CQME;

    if((time() + 3 > t_w_plecy) && (l_w_plecy = ENV(me)))
    {
        t_w_plecy = time();
        l_w_plecy = ENV(me);

//         heart_beat(1);
    }
}

/**
 * Funkcja s�u�y do sprawdzenia czy mo�na i zadania ciosu w plecy
 * walcz�cemu kt�ry ucieka.
 */
public void
cb_cios_w_plecy()
{
    CQME;

    //Sprawdzamy czy s� oba potrzebne obiekty
    if(!attack_ob || !me)
        return;

    //Sprawdzamy czy obiekty s� na tej samej lokacji
    if(ENV(attack_ob) != ENV(me))
        return;

    //Sprawdzamy czu obiekty ze sob� walcz�.
    if(attack_ob->query_enemy() != me)
        return;

    int tdex, tspo, twep, edex, euni;

    tdex = attack_ob->query_stat(SS_DEX);
    tspo = attack_ob->query_skill(SS_AWARENESS);
    twep = attack_ob->query_combat_object()->cb_weapon_skill();

    edex = me->query_stat(SS_DEX);
    euni = me->query_skill(SS_DEFENCE);

    //Korzystamy z wzoru umieszczonego w formulas.h �eby by�o �atwiej
    //balansowa�.
    if(F_ZADA_CIOS_W_PLECY(tdex, tspo, twep, edex, euni))
    {
        attack_ob->query_combat_object()->cb_zadaj_cios_w_plecy();
        //wywo�ujemy funkcje zadaj�c� cios.
    }
}

/**
 * TODO: T� funkcje trzeba przejrze� i prawdopodobnie cz�ciowo przepisa�.
 *
 * Function name: cb_adjust_combat_on_move
 * Description:   Called to let movement affect the ongoing fight. This
 *                is used to print hunting messages or drag enemies along.
 * Arguments:	  True if leaving else arriving
 */
public void
cb_adjust_combat_on_move(int leave)
{
    CQME;

    /*
     * Odes�anie do funkcji sprawdzaj�cej czy zada� Cios w plecy.
     */
    if(leave)
        cb_cios_w_plecy();

    int i, pos, size;
    object *inv, enemy, *all, *rest, *drag;

    if (sizeof(enemies) && environment(me))
    {
        all = all_inventory(environment(me));
        inv = all & enemies;
        size = sizeof(inv);

        if (leave)
        {
            /*
             * If the aggressors are around.
             */
            if (size)
            {
                drag = ({ });
                rest = ({ });
                i = -1;
                while(++i < size)
                {
                    if (inv[i]->query_prop(LIVE_O_ENEMY_CLING) == me)
                    {
                        drag += ({ inv[i] });
                        tell_object(inv[i], me->query_Imie(inv[i], PL_MIA) + " wybywa, ci�gn�c ci� za sob�.\n");
                    }
                    else
                    {
                        rest += ({ inv[i] });

                        inv[i]->catch_msg(set_color(inv[i], COLOR_FG_YELLOW, COLOR_BOLD_ON) +
                            me->query_Imie(inv[i], PL_MIA) +
                            " uciek�" + me->koncowka("", "a", "o") +
                            " ci.\n" + clear_color(inv[i]));
                    }
                }

                if (sizeof(drag))
                {
                    if (i_am_real)
                        me->catch_msg("Wybywasz, ci�gn�c za sob� " + COMPOSITE_LIVE(drag, PL_BIE) + ".\n");

                    /* Stop fighting all the enemies that don't follow us.
                    We must still fight the enemies that do, since otherwise
                    we can move so quickly that we don't update our enemies
                    to include them when they attack again, although they
                    will autofollow and attack again on entry.
                    */
                    this_object()->cb_stop_fight(rest);
                    attacked_by = attacked_by - rest;
                    rest->notify_i_escaped(me);
                }
            }
            else
            {
                if (size)
                {
                    i = -1;
                    while(++i < size)
                    {
                        if (CAN_SEE(me, inv[i]) && CAN_SEE_IN_ROOM(me) &&
                        !NPATTACK(inv[i]))
                        {
                            me->attack_object(inv[i]);
                            cb_update_tohit_val(inv[i], 30); /* Give hunter bonus */
                            tell_room(environment(me), QCIMIE(me, PL_MIA) +
                                " atakuje " + QIMIE(inv[i], PL_BIE) + ".\n",
                                ({ inv[i], me }));
                            tell_object(inv[i], me->query_Imie(inv[i], PL_MIA) +
                                " atakuje ciebie!\n");
                            tell_object(me, "Atakujesz " +
                                inv[i]->query_imie(me, PL_BIE) + ".\n");
                        }
                    }
                }
            }
        }
    }
}

/**
 * Funkcja ta odpowiada za uciekanie npc'a lub gracza o x lokacji.
 * Opisy ucieczki zwierzak�w s� troche urozmaicone - mo�na by to
 * przerobi� na urozmaicenie w std zwierzak�w, ale to mo�e kiedy�
 *
 * @param dir zm�czenie pierwszego wyj�cia
 * @param na_ile na ile lokacji mamy uciec.
 *
 * TODO:
 *  - funkcja do przebudowania.
 */
public void
cb_run_away(string dir, int na_ile = 1)
{
    CQME;

    object          here = environment(me);
    int             size, pos, i, j;
    mixed          *exits = ({});
    string         *std_exits, exit;

    //Jak nie ma gdzie ucieka� to nie ma sobie co g�owy zawraca�.
    if(!sizeof(here->query_exit()))
        return;

    //Je�li npc ma nie ucieka� lub jest duchem to te� nie ma sensu nic wykonywa�.
    if (me->query_ghost() || (!i_am_real && me->query_prop(NPC_I_NO_RUN_AWAY)))
        return;

    //dir == hurray je�li to kolejne wykonanie funkcji
    //Je�li jest okre�lony wyj�cie to znaczy, �e biegniemy tylko na jedn� lokacj�
    if (!stringp(dir) || dir == "hurray")
    {
        here = environment(me);
        std_exits = ({ "p�noc", "po�udnie", "zach�d", "wsch�d", "g�ra", "d�" });

        exits = here->query_exit();

        object last_lok = me->query_prop(LIVE_O_LAST_ROOM);

        int q;

        //Sprawdzamy czy w obiekcie nie ma zdefiniowanych jakich� szczeg�lnych warunk�w
        //np. w zwierz�tach s�.
        mixed ne = me->query_run_away_exits(exits, last_lok);
        if(pointerp(ne) && sizeof(ne) > 3)
            exits = ne;

        //Nie b�dziemy wraca� tam gdzie dopiero co byli�my. Chyba, �e nie ma innej
        //mo�liwo�ci a nasz wr�g stoi teraz obok nas, to b�dziemy biega� dopuki nie
        //uciekniemy od kt�rego� z nich.
        if(dir != "hurray")
        {
            if(last_lok)
            {
                q = member_array(MASTER_OB(last_lok), exits);

                if(q != -1 && sizeof(exits) > 3)
                {
                    if(q != 0)
                    {
                        mixed *ne;
                        ne = exits[0..q-1] + exits[q+3..];
                        if(sizeof(ne))
                            exits = ne;
                    }
                    else
                    {
                        if(sizeof(exits[q+3..]))
                            exits = exits[q+3..];
                    }
                }
            }
        }

        size = sizeof(exits);
        j = random(size / 3);

        if(size < 3)
            return;
    }

    string old_mout, old_min, new_mout, new_min;
    old_mout    = me->query_m_out();
    old_min     = me->query_m_in();

    new_mout    = me->query_away_m_out();
    new_min     = me->query_away_m_in();

    new_mout    = new_mout || "w panice wybiega";
    new_min     = new_min  || "wbiega, dr��c ze strachu";

    if(stringp(dir) && dir != "hurray")
        catch(me->command(dir));
    else
    {
        //A tu zupe�nie przebudowa�em bo wydawa�o mi si� g�upie, pozatym prawdopodobnie to
        //w�a�nie tu si� sypa�o. [Krun]
        i = 0;
        while(i++ < size && here == environment(me))
        {
            exit = (pointerp(exits[j * 3 + 1]) ?
                (pointerp(exits[j * 3 + 1][0]) ? exits[j * 3 + 1][0][0] : exits[j * 3 + 1][0])
                : exits[j * 3 + 1]);

            me->set_m_out(new_mout);
            me->set_m_in(new_min);

            catch(me->command(exit));

            j++;

            if (j * 3 >= size)
                j = 0;
        }
    }

    me->set_m_out(old_mout);
    me->set_m_in(old_min);

    if (here == environment(me))
    {
        tell_room(environment(me), QCIMIE(me, PL_MIA) + " pr�bowa�" +
           me->koncowka("", "a", "o") + " uciec, ale " +
           me->koncowka("mu", "jej", "mu") + " si� nie uda�o.\n", ({me}));

        me->catch_msg(set_color(me, COLOR_FG_YELLOW, COLOR_BOLD_ON) + "Pr�bowa�" + me->koncowka("e�", "a�", "o�") + " uciec, " +
            "ale nie uda�o ci si�.\n" + clear_color(me));
        //Sprawdzili�my wszystie mo�liwo�ci je�li si� nie uda�o to trudno. Ni� b�dziemy pr�bowali dalej bo to nie bardzo ma sens.
        //Tak ma�e prawdopodobie�stwo, �e si� uda, a du�e obci��enie dla systemu.
        return;
    }
    else
        me->catch_msg(set_color(me, COLOR_FG_GREEN) + "Uda�o ci si� uciec!\n" + clear_color(me));

    if(--na_ile > 0)    //Mog�o przeszkoczy� i by�yby jaja.
        cb_run_away("hurray", na_ile);
}

/**
 * TODO: Funkcja do przejrzenia.
 *
 * Funkcja wywo�ywana kiedy wr�g ucieka.
 */
public void
cb_enemy_escaped()
{
    CQME;

    object *new;

    /*
     * To cling to an enemy we must fight it.
     */
    me->remove_prop(LIVE_O_ENEMY_CLING);

    /*
     * Switch enemy if we have an alternate
     */
    enemies = enemies - ({ 0 });
    to_stun = to_stun - ({0});
    attacked_by = attacked_by - ({attack_ob});

    new = (all_inventory(environment(me)) & attacked_by) - ({ attack_ob });
    if(!sizeof(new))
        new = (all_inventory(environment(me)) & enemies) - ({ attack_ob });

    if (sizeof(new))
        attack_ob = new[0];
    else
    {
        if (attack_ob && attack_ob->query_ghost())
        {
            me->remove_prop(LIVE_I_ATTACK_DELAY);
            me->remove_prop(LIVE_I_STUNNED);
        }
        attack_ob = 0;
    }

   /*
    * We attack another enemy when old enemy left.
    */
    if (attack_ob)
    {
        tell_object(me, "Koncentrujesz si� na walce z " +
        attack_ob->query_imie(me, PL_NAR) +".\n");

        attack_ob->attacked_by(me);
        heart_beat();

        return;
    }
    else
    {
        stop_heart();
        return;
    }
}

/*
 * Function name: cb_wield_weapon
 * Description:   Wield a weapon. 'Weapon' is here a general term for any tool
 *                used as a weapon. Only players are limited to /std/weapon
 *		  weapons.
 * Arguments:	  wep - The weapon to wield.
 * Returns:       True if wielded.
 */
public varargs mixed
cb_wield_weapon(object wep, int slot = 0)
{
    CQME;

    return "";
}

/**
 * A ta funkcja odpowiada za dobywanie broni, przez npc'e,
 * zeby nie by�o, �e npce ci�gle bez broni biegaj�.
 */
public int
cb_dobadz_broni()
{
    CQME;

    return 0;
}

/*
 * Function name: cb_show_wielded
 * Description:   Describe the currently wielded weapons.
 * Argumensts:    ob: The object to give the description
 * Returns:       Description string.
 */
public string
cb_show_wielded(object ob)
{
    CQME;

    return "";
}

/*
 * Function name: unwield
 * Description:   Unwield a weapon.
 * Arguments:	  wep - The weapon to unwield.
 * Returns:       None.
 */
public void
cb_unwield(object wep)
{
}

/**
 * Funkcja zwraca wszystkie dobyte bronie - i tylko bronie. Nie zwraca
 * �adnych innych przedmiot�w trzymanych przez gracza. Na przyk�ad lamp.
 *
 * @param which z jakiego slota bro� wy�wietli� (-1 = z wszystkich w formie tablicy)
 *
 * @return bro� ze slota lub tablice z wszystkimi bro�mi.
 */
public mixed
cb_query_weapon(int which)
{
    if(which == -1)
        return ({});
    else
        return 0;
}

/**
 * Funkcja zwraca wszystkie przedmioty chwycone, nie zwraca broni.
 *
 * @param which z jakiego slota przedmioty wy�wietli�(-1 = z wszystkich w formie tablicy)
 *
 * @return przedmioty ze slota lub tablice z wszystkimi przedmiotami.
 */
public mixed
cb_query_chwycone(int which)
{
    if(which == -1)
        return ({});
    else
        return 0;
}

/**
 * Funkcja ta sprawdza czym gracz mo�e si� broni�, pozwala to na wykluczenie
 * sposob�w obrony, kt�re s� dla gracza niedost�pne - gdy gracz nie ma broni
 * nie mo�e parowa� broni�, gdy nie ma tarczy tarcz�.
 *
 * WARNING: Funkcja nie sprawdza czy mamy w danej dziedzinie jak�� umiej�tno��.
 *          Bada tylko czy w og�le jest mo�liwo�� wykonania danego sposobu
 *          obrony.
 *
 * @return tablica
 *  <ul>
 *    <li> 1/0 - uniki </li>
 *    <li> 1/0 - parowanie broni� </li>
 *    <li> 1/0 - os�ona tarcz� </li>
 *  </ul>
 */
public mixed
cb_query_sposoby_obrony()
{
    int     uniki;

    //W zwierzaczkach trzeba sprawdzi� wszystkie dolne - g��wne obszary hitlokacji.
    uniki = 1;

    return ({uniki, 0, 0});
}

/**
 * @return wszystkie tarcze, kt�rymi mo�na operowa�.
 */
public object *
cb_query_tarcze()
{
    return ({});
}

/**
 * Zak�adamy ciuch b�d� zbroje na dan� cze�� cia�a.
 *
 * @param arm       zak�ada zbroja b�d� ubranie
 * @param na_co     okre�lenie hitlokacji w postaci stringa.
 *
 * @return zawsze 0, bo podstawowy combat nie musi obs�ugiwa� zbroji ani ubra�.
 */
public int
cb_wear_arm(object arm, string na_co = 0)
{
    return 0;
}

/*
 * Function name: cb_show_worn
 * Description:   Describe the currently worn armours
 * Argumensts:    ob: The object to give the description
 * Returns:       Description string.
 */
public string
cb_show_worn(object ob)
{
    return "";
}

/*
 * Function name: cb_remove_arm
 * Description:   Remove an armour
 * Arguments:	  arm - The armour.
 */
public void
cb_remove_arm(object arm)
{
}

/*
 * Function name: cb_query_armour
 * Description:   Returns the armour of a given position.
 *		  A list of all if argument -1 is given.
 * Arguments:	  which: A numeric label describing an armour
 *                       location. On humanoids this is TS_HEAD etc.
 * Returns:       The corresponding armour
 */
public mixed
cb_query_armour(int which)
{
    if(which == -1)
        return ({});
    else
        return 0;
}

/***********************************************************
 * The non redefinable functions follows below
 */

/**
 * Funkcja sprawdza i w razie czego uruchamia bicie serca gracza.
 */
static void
restart_heart()
{
    CQME;

    if(!alarm_id || !get_alarm(alarm_id))
    {
        float spd;
        real_speed = cb_query_speed();
        spd = itof(real_speed) / 10.0;
        alarm_id = set_alarm(spd, spd, heart_beat);
    }
}

/**
 * Wy��cza bicie serca gracza je�li jest ju� niepotrzebne.
 */
static void
stop_heart()
{
    CQME;

    me->remove_prop(LIVE_I_ATTACK_DELAY);
    remove_alarm(alarm_id);
    alarm_id = 0;
}

/**
 * Funkcja sprawdza czy nasz obiekt zosta� zabity.
 * Je�li co� nie jest graczem, a je�li co� u�ywa
 * tej wersji niezamaskowanej przez chumanoida, to
 * znaczy, �e humanoidem nie jest, a co za tym
 * idzie nie mo�e zosta� og�uszone, tylko odrazu
 * przechodzi w form� nie�yw�.
 *
 * @param kto   kto zada� ostatnie obra�enia.
 * @param stun  czy ma zosta� og�uszony
 *
 * @return <ul>
 *  <li> 0 - nic nie zrobione </li>
 *  <li> 1 - gracz zabity </li>
 *  <li> 2 - gracz og�uszony </li>
 *  <li> 3 - gracz usuni�ty z atak�w </li> </ul>
 */
public int
cb_check_killed(object kto, int stun = 0)
{
    if(stun == -1)
    {
        enemies     -= ({ attack_ob });
        to_stun     -= ({ attack_ob });
        attacked_by -= ({ attack_ob });

        return 3;
    }

    if(me->query_hp() <= 0.0)
    {
        me->do_die(kto);

        cb_check_killed(0, -1);

        return 1;
    }
}

/**
 * Funkcja ma za zadanie wybra� hitlokacje, poprzez odpowiednie modyfikacje
 * szans procentowych ze wzgl�du na ilo�� hp na hitlokacji oraz atakowany
 * przez \param attacker obszar.
 *
 * @param attacker  Atakuj�cy.
 */
public int
cb_choose_hitlocation(object attacker)
{
    int i, proc, rand, ao, ha;
    mapping hp;

    ao = attacker->query_atakowany_obszar();
    hp = 0;

    foreach(int hid : hitloc_ac)
    {
        proc = hitloc_ac[hid][HIT_PROC];

        if(proc <= 0)
            break;

        ha = query_hitloc_area(hid);

        if(ao == ha)  //Kawa�ek odpowiedzialny za atakowany obszar.
        {
            if(ao != HA_GLOWA)
                proc += 20;
            else
                proc += 5;
        }

        proc = PROC(proc, ((attacker->query_proc_hp(-ha) / 2) + 50));

        //Opcja raczej nie mo�liwa, ale na wszelki wypadek:P
        if(proc <= 0)
        {
            DBG("KRUN SCHRZANI�E�:P");
            break;
        }


        hp[hid] = proc;
        rand += proc;
    }

    rand = random(rand);

    if(!m_sizeof(hp))
        return -1;

    foreach(int hid : hp)
    {
        if(i >= rand)
            return hid;

        i += hp[hid];
    }

    return -1;
}

/**
 * Funkcja wybiera atak kt�rym przeciwnik zostanie zaatakowany.
 * Funkcja uwzgl�dnia ilo�� punkt�w �ycia na poszczegolnych
 * ko�czynach odpowiadaj�cych za ataki.
 *
 * @param victim    Osoba kt�r� atakujemy.
 */
public int
cb_choose_attack(object victim)
{
    int i, proc, rand;
    mapping ap;

    ap   = ([]);
    rand = 0;

    foreach(int aid : attacks)
    {
        proc = attacks[aid][ATT_PROC];

        if(proc <= 0)
            continue;

        proc = PROC(proc, me->query_proc_hp(-ATT2HA(aid)));

        if(proc <= 0)
            continue;

        ap[aid] = proc;
        rand += proc;
    }

    if(!m_sizeof(ap))
        return -1;

    rand = random(rand);
    i = 0;

    foreach(int aid : ap)
    {
        if(i >= rand)
            return aid;

        i += ap[aid];
    }

    return -1;
}

/**
 * Ka�de wywo�anie tej funkcji to jedna runka walki z wybranym wrogiem.
 * Ta funkcja jest wywo�ywana tak d�ugo jak walcz�cy s� na tej samej
 * lokacji i �yj�.
 */
varargs static nomask void
heart_beat()
{
    int     dt, hitsuc, tmp, phit, extra_fatigue, aid, tr;
    string  logtext;
    mixed   hitresult, *dbits, pen, fail;
    object *new, ob, defend_how;

    if(!objectp(me) || me->query_ghost())
    {
        attack_ob = 0;
        stop_heart();
        DBG("1");
        return;
    }

    //Sprawdzamy czy wrogowie s� aktualni i czy ataki s� aktualne.
    cb_update_enemies();
    cb_update_attack();

    //Je�li z nikim nie walczymy to stopujemy bicie serca.
    if(!cb_query_attack())
    {
        stop_heart();
        DBG("2");
        return;
    }

    //Ustawiamy kiedy gracz ostatnio walczy�.
    me->add_prop(PLAYER_I_LAST_FIGHT, time());

    /* First do some check if we actually attack    */
    if(pointerp(fail = me->query_prop(LIVE_AS_ATTACK_FUMBLE)) &&
        sizeof(fail))
    {
        if(i_am_real)
            me->catch_msg(fail[0]);

        DBG("3");
        return;
    }

    if((tmp = me->query_prop(LIVE_I_ATTACK_DELAY)))
    {
        if((tmp -= (real_speed / 10)) > 0)
        {
            me->add_prop(LIVE_I_ATTACK_DELAY, tmp);
            DBG("4");
            return;
        }
        else
            me->remove_prop(LIVE_I_ATTACK_DELAY);
    }

#if 0
    if (((hit_heart * 100) + me->query_prop(LIVE_I_QUICKNESS)) < random(100))
        return 1; /* Wait one round more then. */
    hit_heart = -1;
#endif

    //Je�li jeste�my og�uszeni to nie walczymy.
    if(me->query_prop(LIVE_I_STUNNED))
    {
        DBG("5");
        return;
    }

    /* jeste�my i atakujemy. */
    attack_ob->attacked_by(me);

    /* sprawdzamy czy na pewno walczymy czy mo�e tylko jeste�my
        w atakuj�cym t�umie */
    if((member_array(me, attack_ob->query_attacked_by()) == -1) &&
        (attack_ob->query_attack() != me))
    {
        /* je�li kto� nas atakuje to raczej odpowiadamy na atak ni� czekamy a� zwolni si�
           miejsce w kolejce do naszego upragnionego celu */
        if(sizeof(attacked_by))
        {
            me->catch_msg("Koncentrujesz si� na walce z " +
                QIMIE(attacked_by[0],PL_NAR) + ".\n");
            cb_add_enemy(attacked_by[0]);
        }
        else
        {
            /* dostajemy wiadomo�� tylko co jaki� czas */
            if(msg_counter % 10 == 0)
            {
                me->catch_msg("Pr�bujesz w��czy� si� do walki z " +
                    QIMIE(attack_ob, PL_NAR) + ", ale tam jest ju� spory t�ok.\n");
            }
            msg_counter++;

            DBG("6");

            return;
        }
    }

    //Npc'e mog� robi� specjalne ataki podczas gdy gracze raczej nie.
    if(!i_am_real && me->special_attack(attack_ob))
    {
        DBG("7");
        return;
    }

    //To pozosta�o�� systemu magii z gena, narazie tylko zakomentuje bo nie jestem pewien czy nie jest
    //to gdzie� wykorzystywane, ale raczej nie jest.
    //TODO: Do wyrzucenia.
#if 0
    if (objectp(ob = me->query_prop(LIVE_O_SPELL_ATTACK)))
    {
        me->remove_prop(LIVE_O_SPELL_ATTACK);
        ob->spell_attack(me, attack_ob);
        return;
    }
#endif

    //Je�li gracz pr�buje si� skoncentrowa� to przepada mu tura.
    if(me->query_prop(LIVE_I_CONCENTRATE))
    {
        DBG("8");
        return;
    }

    //FIXME: dopisa� przepadanie tur przy przewr�ceniu, zadaniu silniejszego ciosu, itp.

//     DBG(me->query_real_name() + " ATAKUJE");

    //Maksymalnie na runde mo�emy przeprowadzi� dwie pr�by zadania ataku, przy czym przy
    //drugiej szansa powodzenia jest 3x mniejsza.
    for(int tr = 0 ; tr < 2 ; tr++)
    {
        aid = cb_choose_attack(attack_ob);

        DBG("Aid = " + aid + "\n");

        if(aid == -1)
        {
            qme()->catch_msg("Jeste� tak pokieraszowany, �e nie dasz rady zada� ju� �adnego ciosu.\n");
            cb_stop_fight();
            return;
        }

        hitsuc = cb_try_hit(aid);

        if(hitsuc <= 0 || random(0 * 3))
            continue;

        /*
         * The intended victim can also force a fail. like in the weapon
         * case, if fail, the cause must produce explanatory text himself.
         */
        hitsuc = attack_ob->query_not_attack_me(me, aid);

        if(hitsuc > 0)
        {
            DBG("12");
            continue;
        }

        hitresult = cb_tohit(aid,
            ({attacks[aid][ATT_M_HIT], attacks[aid][ATT_WEIGHT], attacks[aid][ATT_VOLUME],
            attacks[aid][ATT_SKILL], 0}), attack_ob);

        defend_how = hitresult[1];
        hitsuc = hitresult[0];

        /* Istnieje zawsze szansa zadania kilku ciosow na raz. Wybieramy
         * watrosc zmeczenia najbardziej meczacego z nich.
         * EDIT: Taka szansa ju� nie istnieje, ale mo�e jeszcze wr�ci
         * TODO: Przemy�le�
         * WARNING: Tak czy tak zrobi�em sum� zm�czenia, w ko�cu nawet je�li
         *          uda si� przeprowadzi� kilka atak�w to na pewno b�dzie nas
         *          to kosztowa� wi�cej zm�czenia wi�c dlaczego mamy wybiera�
         *          tylko ten najbardziej m�cz�cy a nie liczy� z sumy?
         */
        extra_fatigue += attacks[aid][ATT_FCOST];

        /* Choose one damage type.
         */
        dt = attacks[aid][ATT_DAMT];
        dbits = ({ dt & W_IMPALE, dt & W_SLASH, dt & W_BLUDGEON }) - ({ 0 });
        dt = dbits[random(sizeof(dbits))];

        DBG("hitsuc = " + hitsuc);

        if(hitsuc > 0)
        {
            pen = attacks[aid][ATT_M_PEN];

            if(sizeof(pen))
            {
                tmp = MATH_FILE->quick_find_exp(dt);

                if((tmp < sizeof(pen)))
                    pen = pen[tmp];
                else
                    pen = pen[0];
            }

            if(!random(30000))
            {
                pen *= 6;

                SECURITY->log_syslog("CRITICAL",
                    sprintf("%s: %-11s on %-11s (pen = %4d)\n  %s on %s\n",
                    ctime(time()), me->query_real_name(),
                    attack_ob->query_real_name(), pen, file_name(me),
                    file_name(attack_ob)));
            }

            hitresult = attack_ob->hit_me(pen, dt, hitsuc, me, aid);
        }
        else
            hitresult = attack_ob->hit_me(-1, dt, hitsuc, me, aid);

        /*
         * Generate combat message, arguments Attack id, hitloc description
         * proc_hurt, Defender
         */
        if(hitsuc > 0)
        {
            phit = attacks[aid][ATT_M_PEN][tmp];

            if(phit > 0)
                phit = 100 * hitresult[2] / phit;
            else
                phit = 0;
        }

        if(hitresult[1])
        {
            cb_did_hit(aid, hitresult[1], hitresult[0], attack_ob,
                dt, phit, hitresult[3], hitsuc, defend_how,
                ({hitresult[4]}), hitresult[5], hitresult[6], hitresult[7]);

            DBG("13");
            break;
        }
        else
        {
            DBG("14");
            break; /* Ghost, linkdeath, immortals etc */
        }

        /*
         * Sprawdzamy czy zabili�my, je�li nie to sprawdzimy czy co� nie wysiad�o.
         */
        if(!attack_ob->check_killed(me, member_array(attack_ob, to_stun) >= 0))
            attack_ob->check_areas();
    }

    cb_update_combat(extra_fatigue);

    if(attack_ob && !attack_ob->query_ghost())
        return;
    else
    {
        new = (all_inventory(environment(me)) & attacked_by) - ({ attack_ob });

        if(!sizeof(new))
            new = (all_inventory(environment(me)) & enemies) - ({ attack_ob });

        if(sizeof(new))
        {
            attack_ob = new[0];

            if(attack_ob)
                tell_object(me, "Koncentrujesz si� na walce z " + attack_ob->query_imie(me, PL_NAR) +".\n");

            attack_ob->attacked_by(me);
        }
        else
        {
            attack_ob = 0;
            stop_heart();
            return;
        }
    }

    return;
}

/**
 *
 * @param wcpen     Zmodyfikowana klasa penetracji broni
 * @param dt        Typ obra�e� zadawanych przez bro�.
 * @param hitsuc    Jak dobrze posz�o uderzenie
 * @param attakcer  Obiekt atakuj�cego.
 * @param attack_id Identyfikator ataku.
 * @param thid      Hitlokacja kt�ra ma zosta� uszkodzona,
 *                  warto�� nie koniecznie musi zosta� ustawiona.
 *
 * @return  Rezultat uderzenia ({ procentowe poranienie, opis hitlokacji, phit, obra�enia, zbroje })
 *
 * Description:   Called to decide damage for a certain hit on 'me'.
 */
varargs public nomask mixed
cb_hit_me(int wcpen, int dt, int hitsuc, object attacker, int attack_id,
    int thid = -1)
{
    CQME;

    object     *list, armour;
    int         proc_hurt, hp, tmp, dam, phit, size, j,
                woundType = 0, woundLoc = 0, handicapLoc = 0;
    string      msg;
    mixed       ac;

    //Nie mo�na nic zrobi� nie�ywemu.
    if(me->query_ghost())
    {
        tell_object(attacker, me->query_Imie(attacker, PL_MIA) +
            " ju� nie �yje, naprawd�.\n");
        tell_roombb(environment(me), QCIMIE(attacker, PL_MIA) +
            " pr�buje zabi� trupa.\n", ({attacker}));
        tell_object(me, attacker->query_Imie(me, PL_MIA) +
            " bezskutecznie pr�buje ci� zaatakowa�.\n");
        me->stop_fight(attacker);
        attacker->stop_fight(me);

        LOG_COMBAT("ATTACK_DEAD", ctime(time()), me);

        return ({ 0, 0, 0, 0, 0, 0, 0, 0 });
    }

    /*
     * Update the list of aggressors. If we hit ourselves: no update.
     */

    cb_add_enemy(attacker);

    if(!attack_ob && attacker != me)
        attack_ob = attacker;

    restart_heart();

    /*
     * Choose a hit location, and compute damage if wcpen > 0
     */
    if(thid == -1 || !is_mapping_index(thid, hitloc_ac))
    {
        size = m_sizeof(hitloc_ac);

        if(!size)
        {
            attacker->catch_msg("Zg�o� b��d w " + me->query_imie(attacker,
                PL_MIE) + "! Nie ma zdefiniowanych hitlokacji.\n");
            log_file("BAD_HITLOC", me->query_real_name() + " (" + file_name(me) +
                "): " + file_name(this_object()) + ", brak hitlokacji.\n");

            me->stop_fight(attacker);
            attacker->stop_fight(me);
            return ({ 0, 0, 0, 0, 0, woundType, woundLoc, handicapLoc });
        }

        thid = cb_choose_hitlocation(attacker);

        if(thid == -1)
        {
            log_file("BAD_HITLOC", me->query_real_name() + " (" + file_name(me) +
                "): " + file_name(this_object()) + ", suma szans trafienia "+
                "hitlokacji < 100\n");
            ({me, attacker})->catch_msg("Wyst�pi� w tobie b��d o numerze wal11. "+
                "Zg�o� go opisuj�c okoliczno�ci w jakich do niego dosz�o.\n");
            return ({ 0, 0, 0, 0, 0, 0, 0, 0 });
        }
    }

    ac = hitloc_ac[thid][HIT_M_AC];

    if(wcpen > 0)
    {
        if(dt == MAGIC_DT)  //WARNING: Balans: nie wiem czy na magiczne ataki nie powinno by� ac?:) [Krun]
            ac = 0;
        else
        {
            tmp = (int)MATH_FILE->quick_find_exp(dt);

            if(sizeof(ac) && (tmp < sizeof(ac)))
                ac = ac[tmp];
            else if(sizeof(ac))
                ac = ac[0];
            else if(!intp(ac))
                ac = 0;

            // Za kazde 100 hitsuc odejmujemy okolo 7% ac.
            if(ac > 0)
            {
                ac -= (ac * hitsuc / 670);
                ac = random(ac) + random(ac);
            }
        }

        phit = wcpen / 2;
        phit = random(phit) + random(phit) / 2;

        dam = MAX(0, F_DAMAGE(phit, ac));
    }
    else
    {
        dam = 0;
        phit = (wcpen < 0 ? wcpen : -1);
    }

    hp = ftoi(me->query_hp());

    /*
     * Wizowie s� nie�miertelni je�li chc�!
     */
    if((int)me->query_wiz_level() && dam >= hp && me->query_prop(WIZARD_I_IMMORTAL))
    {
        tell_object(me, "W�a�ciwie to powien" + me->koncowka("iene�", "nna�", "nno�", "ni�cie", "nny�cie") +
            " ju� nie�y� jednak twoja magia pozwala ci odzyska� cz�� zdrowia.\n");
        tell_room(environment(me),
        QCIMIE(me, PL_MIA) + " zaczyna dziwnie wymachiwa� r�kami, wykrzykiwa� jakie� "+
            "niezrozumia�e dla ciebie s�owa, po czym zauwa�asz, �e rany w " +
             me->koncowka("jego", "jej", "jego", "ich", "ich") + " ciele zasklepiaj� si�.\n",
            ({me}));

        me->set_hp(0, -1);

        return ({ 0, 0, 0, 0, 0, woundType, woundLoc, handicapLoc });
    }

    if(me->is_zwierze())
    {
        /* zwierzetom zadajemy od 50% do 250% standardowych obrazen
           zaleznie od skilla hunting */
        dam = ftoi((itof(50 + 2 * (attacker->query_skill(SS_HUNTING)))/100.0) * itof(dam));
            attacker->increase_ss(SS_HUNTING, EXP_LOWIECTWO_UDERZENIE);
    }

    /*
     * Ju�, hurt me.
     */
    if(dam > 0 && hp)
    {
        proc_hurt = (100 * dam) / hp;

        if(dam && !proc_hurt)
            proc_hurt = 1;     /* Less than 1% damage */
    }
    else if(dam > 0)
        proc_hurt = 100;
    else if(wcpen >= 0)
        proc_hurt = 0;
    else
        proc_hurt = -1;     //Wr�g spud�owa�.

    if(dam > 0)
    {
        int mahp, oahp, nahp, mhhp, ohhp, nhhp;

        mahp = 0;
        oahp = 0;
        nahp = 0;
        mhhp = me->query_max_hp(thid);
        ohhp = me->query_hp(thid);

        me->reduce_hp(thid, dam * 100);

        nhhp = me->query_hp(thid);

        if(ohhp > (mhhp * HITLOC_WOUND_THRESHOLD) / 10 &&
            nhhp <= (mhhp * HITLOC_WOUND_THRESHOLD) / 10)
        {   //Pojawia si� nowa rana.
            me->set_hitloc_wound(thid, dt);
            woundType = dt;
            woundLoc = thid;
        }

        foreach(int id : m_indexes(hitloc_areas))
        {
            if(member_array(id, hitloc_areas[id][HA_HITLOCS]))
            {
                mahp = me->query_max_hp(-id);
                nahp = me->query_hp(-id);
            }
        }

        oahp = nahp + ohhp - nhhp;

        if(oahp > 0 && nahp <= 0)
        {   //Cz�� cia�a odmawia pos�usze�stwa
            handicapLoc = -thid;

            if(oahp > (mahp * AREA_WOUND_THRESHOLD) / 10)
                handicapLoc *= 1000;
        }
        else if(oahp > (mahp * AREA_WOUND_THRESHOLD) / 10 &&
            nahp <= (mahp * AREA_WOUND_THRESHOLD) / 10)
        {   //Cz�� cia�a zaczyna odmawia� pos�usze�stwa.
            handicapLoc = thid;
        }
    }

    /*
     * Adjust our panic level
     */
    if(proc_hurt >= 0)
        cb_add_panic(2 + proc_hurt / 5);

    /*
     * Tell us where we were attacked and by which damagetype
     * proc_hurt == -1 -> przeciwnik spudlowal.
     */
    if(proc_hurt >= 0)
        armour = cb_got_hit(thid, proc_hurt, attacker, attack_id, dt, dam);

    /*
     * Reward attacker for hurting me
     */
    if(dam)
    {
#ifdef CB_HIT_REWARD
        //	cb_reward(attacker, dam, 0);
#endif
        if(random(dam) > random(me->query_stat(SS_DIS)))
            me->cmdhooks_break_spell();
    }

    return ({ proc_hurt, thid, phit, dam, armour, woundType, woundLoc, handicapLoc });
}

/**
 * Funkcja rozpoczynaj�ca walk�.
 *
 * @param victim Atakowany obiekt.
 * @param og     Czy chcemy og�uszy�.
 */
public varargs nomask void
cb_attack(object victim, int og)
{
    CQME;

    restart_heart();

    if (victim == me || victim == attack_ob || victim->query_ghost())
        return;

    /*
     * Swap attack
     */
    if((sizeof(attacked_by) ==  MAX_ATTACKING_ENEMIES) && (member_array(victim, attacked_by) == -1))
    {
        me->catch_msg("Jeste� zbyt zaj�t" +me->koncowka("y", "a") +
            " walk� aby m�c zaatakowa� " + QIMIE(victim, PL_BIE) + ".\n");
        return;
    }

    //Gdyby przypadkiem kto� nie wyszed� z ukrycia to go wychodzimy.
    me->reveal_me(1);
    victim->reveal_me(1);

    //Npc'e przed walk� dobywaj� broni.
    if(!interactive(victim) && sizeof(victim->query_weapons(-1)) == 0)
        victim->dobadz_broni();

    if(!interactive(me) && sizeof(me->query_weapons(-1)) == 0)
        me->dobadz_broni();

    if(attack_ob)
        attack_ob->no_more_attacked_by(me);

    cb_add_enemy(victim, (og ? -2 : 1));
    attack_ob = victim;

    victim->attacked_by(me);
    msg_counter = 0;

    me->catch_msg("Ju�.\n");
}

/*
 * Function name:  cb_attacked_by
 * Description:    This routine is called when we are attacked or when
 *                 someone we are hunting appears in our location.
 * Arguments:	   ob: The attacker
 */
public nomask void
cb_attacked_by(object ob)
{
    CQME;

    cb_add_enemy(ob);

    if(!attack_ob || (!query_ip_number(attack_ob) && query_ip_number(ob)))
        attack_ob = ob;

    restart_heart();

    if(me)
        me->cr_attacked_by(ob);
}

public nomask void
cb_no_more_attacked_by(object ob)
{
    CQME;

    attacked_by = attacked_by - ({ ob}) ;
}

/**
 * Przerywamy walk� z wszystkimi lub tylko wybranymi wrogami.
 *
 * @param elist lista wrog�w z kt�rymi chcemy przerwa� walk�
 *              je�li 0 to przerywamy z wszystkimi, je�li obiekt to tylko z tym obiektem.
 */
public varargs nomask void
cb_stop_fight(mixed elist = 0)
{
    CQME;

    //Wysy�amy sygna�, �e walka sko�czona... I nie robi� mi ju�
    //alarm�w czy_walka bo zabije! (krun)

    all_inventory(ENV(me))->signal_stop_fight(me, elist);
    me->add_prop(PLAYER_I_LAST_FIGHT, time());

    if(objectp(elist))
        elist = ({ elist });
    else if(!pointerp(elist))
        elist = ({});

    if(member_array(me, attack_ob->query_attacked_by()) != -1)
        attack_ob->no_more_attacked_by(me);

    if(member_array(attack_ob, elist) >=  0 || !elist)
        attack_ob = 0;

    if(pointerp(elist))
    {
        if(pointerp(to_stun))
            to_stun = to_stun - (object *)elist;
        if(pointerp(enemies))
            enemies = enemies - (object *)elist;
    }
    else
    {
        to_stun = ({});
        enemies = ({});

        return;
    }

    if(sizeof(elist = (enemies & all_inventory(environment(me)))))
        attack_ob = elist[0];
}

/*
 * Function name: cb_query_enemy
 * Description:   Gives our current enemy
 * Arguments:     arg: Enemy number, (-1 == all enemies)
 * Returns:       Object pointer to the enemy
 */
public nomask mixed
cb_query_enemy(int arg)
{
    CQME;

    cb_update_enemies();

    if (arg == -1)
        return enemies + ({});
    else if (arg < sizeof(enemies))
        return enemies[arg];
    else
        return 0;
}

public nomask object *
cb_query_attacked_by()
{
    CQME;

    return attacked_by + ({});
}

/*
 * Function name: cb_query_attack_ob
 * Description:   Gives the enemy we are fighting, if we are fighting...
 * Returns:       Object pointer to the enemy
 */
public nomask mixed
cb_query_attack()
{
    CQME;

    if (attack_ob && !attack_ob->query_ghost() &&
        environment(attack_ob) == environment(me))
    {
        return attack_ob;
    }

    return 0;
}

/*
 * Function name: cb_update_enemies
 * Description  : Makes sure our enemy list is valid.
 */
public nomask void
cb_update_enemies()
{
    CQME;

    enemies = filter(enemies, objectp);
}

/*
 * Function Name: cb_update_attack
 * Description  : Update the current attack_ob by looking for new
 *                enemies to attack. Only call this when attack_ob used
 *                to be correct.
 * Returns      : object - the target to attack
 */
public nomask mixed
cb_update_attack()
{
    CQME;

    object *targets, old_enemy;

    /* Old enemy valid? */
    old_enemy = attack_ob;
    if (cb_query_attack())
    {
        return attack_ob;
    }

    me->notify_enemy_gone(attack_ob);
    /* To cling to an enemy we must fight it. */
    if (me->query_prop(LIVE_O_ENEMY_CLING) == attack_ob)
        me->remove_prop(LIVE_O_ENEMY_CLING);

    old_enemy = attack_ob;
    attack_ob = 0;

    /* Look for any new enemies to attack. */
    targets = (all_inventory(environment(me)) & enemies) - ({ attack_ob });

    if (sizeof(targets))
        attack_ob = targets[0];

    /* We attack another enemy when old enemy left. */
    if (attack_ob)
    {
        tell_object(me, "Koncentrujesz si� na walce z  " +
            attack_ob->query_imie(me, PL_NAR) + ".\n");
    }
    else
    {
        /* If we killed our previous enemy and have no more we're nice
         * enough to remove some stuns. */
        me->remove_prop(LIVE_I_ATTACK_DELAY);
        me->remove_prop(LIVE_I_STUNNED);
    }

    return attack_ob;
}

/*
 * Function name:  cb_heal
 * Description:    Heals the living object. Adds hp, mana and fatigue, panic
 * Arguments:	   delay: Number of heart_beats since last heal
 * Returns:        0 if we healed 'me'
 */
public nomask int
cb_heal(int delay)
{
    CQME;

    return 0;
}

/**********************************************************
 *
 * Below is internal functions, only used by the inheritor of
 * this standard combat object.
 */

/**
 * Dodaje atak do tablicy atak�w.
 *
 * @param wchit     Klasa broni do zadawania obra�e�
 * @param wcpen     Klasa broni do przenikliwo�ci(?)
 * @param damtype   Rodzaj obra�e�
 * @param prcuse    Procentowa szansa na u�ycie
 * @param id        Specyficzne id ataku, dla istot humanoidalnych
 *                  zalecane jest u�ycie, W_NONE, W_RIGHT, W_LEFT, W_FOOTR,
 *                  W_FOOTL, W_BOTH, W_FEET, W_ANYH.
 * @param skill     Umiej�tno�� u�ycia
 * @param weight    Waga broni(opcjonalnie)
 * @param fcost     Dodatkowe zm�czenie przy przeprowadzaniu ataku
 *                  za pomoc� tego ataku:) (opcjonalnie)
 * @param volume    Obj�to�� broni(opcjonalnie)
 *
 * @return 1/0  Atak dodany/niedodany,
 */
static varargs int
add_attack(int wchit, mixed wcpen, int damtype, int prcuse, int id, int skill, int weight = 0,
    int fcost = 0, int volume = 0)
{
    CQME;

    int pos, *pen, *m_pen, m_hit, strength_mod;

    if(m_sizeof(attacks) >= MAX_ATTACK)
        return 0;

    pen = allocate(W_NO_DT);
    m_pen = allocate(W_NO_DT);

    if(skill == 0)
        skill = wchit;
    else if (skill < 1)
        skill = 0;

    m_hit = F_TOHITMOD(wchit, skill);
    strength_mod = F_STRENGTH_DAMAGE_MOD(me->query_stat(SS_STR));

    pos = -1;
    while(++pos < W_NO_DT)
    {
        if(!pointerp(wcpen))
        {
            m_pen[pos] = F_PENMOD(wcpen, skill);
            pen[pos] = wcpen;
        }
        else if(pos >= sizeof(wcpen))
        {
            m_pen[pos] = (pos ? m_pen[0] : 0);
            pen[pos] = (pos ? pen[0] : 0);
        }
        else
        {
            m_pen[pos] = F_PENMOD(wcpen[pos], skill);
            pen[pos] = wcpen[pos];
        }

        m_pen[pos] += strength_mod;
    }

    attacks[id] = ({ wchit, pen, damtype, prcuse, skill, m_hit, m_pen, weight, fcost, volume });

    return 1;
}

/**
 * Za pomoc� tej funkcji usuwamy atak z livinga.
 *
 * @param id usuwanego ataku.
 *
 * @return 1/0  Atak usuni�ty/nieusuni�ty,
 */
static int
remove_attack(int id)
{
    CQME;

    int pos;

    if(!is_mapping_index(id, attacks))
        return 0;

    attacks = m_delete(attacks, id);

    return 1;
}

/**
 * @return Identyfikatory wszystkich atak�w.
 */
public int *
query_attack_id()
{
    CQME;

    return m_indexes(attacks);
}

/**
 * @param id Identyfikator ataku.
 *
 * @return
 */
public mixed *
query_attack(int id)
{
    CQME;

    if(!is_mapping_index(id, attacks))
        return 0;

    return attacks[id];
}

/**
 * Dodajemy do livinga hiltokacje.
 *
 * @param ac        Ac hitlokacji
 * @param prchit    Procentowa szansa na trafienie w hitlokacje
 * @param nazwa      opis hitlokacji
 * @param id        id hitlokacji
 * @param przym     przymiotniki
 * @param hp        czy doda� hp info
 *
 * @return 0 nie dodany
 * @return !0 dodany
 */
static varargs int
add_hitloc(mixed ac, int prchit, mixed nazwa, int id, mixed przym = 0, int hp = 1)
{
    CQME;

    int pos, *act, *m_act, ns, rodz;

    if(pointerp(nazwa))
    {
        ns = sizeof(nazwa);

        if(!ns || !pointerp(nazwa[0]) || sizeof(nazwa[0]) != 6 ||
            (ns == 3 && (!pointerp(nazwa[1]) || sizeof(nazwa[1]) != 6 || !intp(nazwa[2]))) ||
            (ns == 2 && !intp(nazwa[1])))
        {
            object oldtp = TP;
            set_this_player(find_player("krun"));
            dump_array(({nazwa, ns, pointerp(nazwa[0]), sizeof(nazwa[0]), intp(nazwa[1])}));
            set_this_player(oldtp);
            throw("B��dna odmiana nazwy w funkcji add_hitloc().\n");
            return 0;
        }
    }
    else if(stringp(nazwa))
        nazwa = slownik_pobierz(nazwa);
    else
    {
        throw("B��dna odmiana nazwy w funkcji add_hitloc()\n.");
        return 0;
    }

    rodz = nazwa[-1];
    nazwa = nazwa[0..-2];

    if(stringp(przym))
        przym = ({ PRZYM_Z_MNOGA(przym) });
    else if(pointerp(przym))
    {
        for(int i = 0 ; i < sizeof(przym) ; i++)
        {
            if(stringp(przym[i]))
                przym[i] = PRZYM_Z_MNOGA(przym[i]);
            else if(!pointerp(przym[i]) || sizeof(przym[i]) != 2 || !stringp(przym[i][0]) || !stringp(przym[i][1]))
            {
                throw("B��dnie podana tablica przymiotnik�w do funkcji add_hitloc().\n");
                return 0;
            }
        }
    }
    else if(przym)
    {
        throw("B��dnie podana tablica przymiotnik�w do funkcji add_hitloc().\n");
        return 0;
    }

    if(m_sizeof(hitloc_ac) >= MAX_HITLOC)
        return 0;

    act = allocate(W_NO_DT);
    m_act = allocate(W_NO_DT);

    pos = -1;

    while(++pos < W_NO_DT)
    {
        if(!pointerp(ac))
        {
            m_act[pos] = F_AC_MOD(ac);
            act[pos] = ac;
        }
        else if(pos >= sizeof(ac))
        {
            act[pos] = (pos ? act[0] : 0);
            m_act[pos] = (pos ? F_AC_MOD(act[0]) : 0);
        }
        else
        {
            m_act[pos] = F_AC_MOD(ac[pos]);
            act[pos] = ac[pos];
        }
    }

    hitloc_ac[id] = ({ act, prchit, nazwa, m_act, rodz, przym });

    if(hp)
        qme()->add_hitloc_hp_info(id, prchit);

    return 1;
}

/**
 * Dodajemy grupe hitlokacji.
 *
 * @param area      Identyfikator obszaru.
 * @param desc      Opis obszaru.
 * @param cel       Celownik obszaru
 * @param bie       Biernik obszaru
 * @param hitlocks  Hitlokacje sk�adaj�ce si� na obszar.
 */
static void
add_hitloc_area(int area, string desc, string cel, string bie, mixed hitlocs)
{
    CQME;

    hitloc_areas[area] = ({desc, bie, cel, hitlocs});
}

/*
 * Function name: remove_hitloc
 * Description:   Removes a specific hit location
 * Arguments:     id: The hitloc id
 * Returns:       True if removed
 */
static int
remove_hitloc(int id)
{
    CQME;

    if(is_mapping_index(id, hitloc_ac))
        hitloc_ac = m_delete(hitloc_ac, id);
    return 0;
}

/**
 * Funkcja usuwa obszar hitlokacji.
 *
 * @param id Identyfikator obszaru
 */
static void
remove_hitloc_area(int id)
{
    CQME;

    hitloc_areas = m_delete(hitloc_areas, id);
}

/**
 * @return Zwraca tablice z identyfikatorami hitlokacji.
 */
public int *
query_hitloc_id()
{
    CQME;

    return m_indexes(hitloc_ac) + ({});
}

/**
 * @return Zwraca wszystkie obszary hitlokacji.
 */
public int *
query_hitloc_area_id()
{
    CQME;

    return m_indexes(hitloc_areas) + ({});
}

/**
 * @return Zwraca nazwy wszystkich obszar�w hitlokacji.
 */
public string *
query_hitloc_areas_names()
{
    CQME;

    return map(hitloc_areas, &operator([])(,HA_DESC));
}

/**
 * @param area sprawdzany obszar hitlokacji
 *
 * @return Zwraca nazw� podanego obszaru hitlokacji.
 */
public string
query_hitloc_area_name(int area)
{
    CQME;

    return hitloc_areas[area][HA_DESC];
}

/**
 * @param area sprawdzany obszar hitlokacji
 *
 * @return Zwraca celownik nazwy podanego obszaru hitlokacji.
 */
public string
query_hitloc_area_cel(int area)
{
    CQME;

    if(!is_mapping_index(area, hitloc_areas))
        return 0;

    return hitloc_areas[area][HA_CEL];
}

/**
 * @param area  sprawdzany obszar hitlokacji
 *
 * @return Zwraca biernik nazwy podanego obszaru hitlokacji.
 */
public string
query_hitloc_area_bie(int area)
{
    CQME;

    if(!is_mapping_index(area, hitloc_areas))
        return 0;

    return hitloc_areas[area][HA_BIE];
}

/**
 * @param hid   Identyfikator hitlokacji
 *
 * @return Obszar do kt�rego przynale�y hiotlokacja.
 */
public int
query_hitloc_area(int hid)
{
    mapping m;
    m = filter(hitloc_areas, &operator(!=)(-1,) @ &member_array(hid, ) @ &operator([])(, HA_HITLOCS));

    return m_values(m)[0];
}

/**
 * @param area  sprawdzany obszar hitlokacji
 *
 * @return Zwraca hitlokacje znajduj�ce si� w danym obszarze hitlokacji.
 */
public int *
query_hitloc_area_members(int area)
{
    CQME;

    if(!is_mapping_index(area, hitloc_areas))
        return 0;

    return hitloc_areas[area][HA_HITLOCS];
}

/**
 * @return czy obszar hitlokacji jest skonfigurowany.
 */
public int
is_configured(int area)
{
    CQME;

    return (is_mapping_index(area, hitloc_areas) && sizeof(hitloc_areas[area]));
}

/**
 * @param id identyfikator hitlokacji
 *
 * @return Informacje o konkretnej hitlokacji - tablica:
 *  <ul>
 *    <li> 0 - mixed    ac      - ac danej lokacji, mo�e byc 3 elem. tablica (na kazdy typ ataku) </li>
 *    <li> 1 - int      proc    - % szansa trafienia w dan� hitlokacj� </li>
 *    <li> 2 - string   desc    - opis danej hitlokacji </li>
 *    <li> 3 - mixed    m_ac    - to samo co ac, tyle ze zawiera modified ac </li>
 *    <li> 4 - int      rodz    - rodzaj opisu danej hitlokacji </li>
 *  </ul>
 */
public nomask mixed *
query_hitloc(int id)
{
    CQME;

    if(is_mapping_index(id, hitloc_ac))
        return hitloc_ac[id];

    return 0;
}
/**
 * @param id    Identyfikator hitlokacji
 *
 * @return procentowy udzia� hitlokacji (-1 brak hitlokacji).
 */
public nomask int
cb_query_hitloc_proc(int id)
{
    CQME;

    if(!is_mapping_index(id, hitloc_ac))
        return -1;

    return hitloc_ac[id][HIT_PROC];
}

/**
 * @param hid   sprawdzana hitlokacja
 *
 * @return Rodzaj nazwy hitlokacji.
 */
public nomask int
cb_query_hitloc_rodzaj(int hid)
{
    CQME;

    mixed hl = query_hitloc(hid);

    if(!sizeof(hl))
        return 0;

    hl = hl[HIT_RODZ];
    return (hl < 0 ? -(hl-1) : hl);
}

/**
 * @param hid   sprawdzana hitlokacja.
 *
 * @return czy nazwa hitlokacji jest w liczbie mnogiej.
 */
public nomask int
cb_query_htiloc_mn(int hid)
{
    CQME;

    return (query_hitloc(hid)[HIT_RODZ] < 0);
}

mixed
cb_query_to_stun()
{
    CQME;

    return to_stun;
}

/** *********************************************************************** **
 ** *****************  B R O N I E  S T R Z E L E C K I E  **************** **
 ** *********************************************************************** **/

public nomask void
cb_bs_did_hit(object strzelajacy, object pocisk, object bron, int sila, int szybkosc)
{
}

/**
 * Funkcja wywo�ywana kiedy dostajemy z broni strzeleckiej.
 *
 * @param strzelajacy strzelajacy z broni.
 * @param pocisk pocisk kt�rym dostaniemy
 * @param bron bro� z kt�rej by�o strzelane
 * @param sila si�a strza�u
 * @param szybkosc szybko�� strza�u
 * @param sila_naciagu si�a naci�gu.
 */
public nomask int
cb_bs_hit_me(object strzelajacy, object pocisk, object bron, int sila, int szybkosc, int sila_naciagu)
{
    CQME;

    string z_kierunku, na_kierunek, na_kierunek2;

    if(ENV(me) != ENV(strzelajacy))
    {
        //Wyszukujemy �adnie kierunku z kt�rego przylecia�a strza�a.
        int i;
        if((i = member_array(MASTER_OB(ENV(strzelajacy)), ENV(me)->query_exit_rooms())) >= 0)
            z_kierunku = bron->zamien_kierunek(ENV(me)->query_exit_cmds()[i], 2);
        if((i = member_array(MASTER_OB(ENV(me)), ENV(strzelajacy)->query_exit_rooms())) >= 0)
        {
            na_kierunek = bron->zamien_kierunek(ENV(strzelajacy)->query_exit_cmds()[i], 1);
            na_kierunek2 = bron->zamien_kierunek(ENV(strzelajacy)->query_exit_cmds()[i], 3);
        }
    }

    int hid, j, tmp = random(100), size = m_sizeof(hitloc_ac);

    //Musimy sprawdzi� w co gracz trafi. FIXME: doda� do opcji wybranego obszaru
    foreach(int hloc : m_indexes(hitloc_ac))
    {
        j += hitloc_ac[hloc][HIT_PROC];

        if(j > tmp)
        {
            hid = hloc;
            break;
        }
    }

    if(!is_mapping_index(hid, hitloc_ac))
    {
        hid = m_indexes(hitloc_ac)[-1];
        log_file("BAD_HITLOC", me->query_real_name() + " (" + file_name(me) +
            "): " + file_name(this_object()) + ", suma szans trafienia "+
            "hitlokacji < 100\n");
    }

    string hitlokacja = cb_hitloc_desc(hid);

    mixed def = me->query_defense(pocisk->query_prop(OBJ_I_WEIGHT) * me->query_stat(SS_STR), 1);

    int *d = ({0, 0, 0}), dt;

    int damt = pocisk->query_dt();

    if(damt & W_IMPALE)
        d[0]++;
    if(damt & W_SLASH)
        d[1]++;
    if(damt & W_BLUDGEON)
        d[2]++;

    //Jak nieustawione to k�ute:)
    if(!fold(d, &operator(+)(), 0))
        dt = 0;

    while(d[(dt=random(sizeof(d)))] != 1);

    /**
     * Tablica what_defended ma postac :
     * [0] - bonus parowania bronia
     * [1] - bonus z unikow
     */
    int *what_defended = ({0, 0});
    object def_ob;

    if(sizeof(def) == 2 && def[0] > 0 && objectp(def[1]))
    {   //Sprawdzamy czy czym� nie wyparuje.
        def_ob = def[1];

        if(def_ob->is_weapon())
            what_defended[0] += -20 + def_ob->query_prop(OBJ_I_VOLUME) / 20;
        else
            what_defended[0] += def_ob->query_prop(OBJ_I_VOLUME);
    }

    what_defended[1] = F_BS_UNIK(szybkosc, me->query_skill(SS_DEFENSE));

    int rnd = random((3 * (what_defended[0] + what_defended[1])) / 2);

    int obr = F_BS_OBRAZENIA(sila, szybkosc);
    int max_obr = F_BS_MAX_OBRAZENIA(sila, szybkosc);

    if(rnd < what_defended[0])
    {   //sparujemy broni� lub tarcz�!
        me->catch_msg("W ostatnim momencie udaje Ci si� " + (def_ob->is_weapon() ? "sparowa�" : "os�oni� si� przed") +
            " lec�c� w twoim kierunku" +
            ((ENV(me) == ENV(strzelajacy)) ? ", wystrzelon" +
            pocisk->koncowka("y", "�", "e", "ych", "e") + " z " + bron->short(me, PL_DOP) + " przez " +
            strzelajacy->short(me, PL_BIE) : " " + z_kierunku) + " " +
            pocisk->query_nazwa((def_ob->is_weapon() ? PL_BIE : PL_NAR)) + ".\n");
        tell_roombb(ENV(me), QCIMIE(me, PL_MIA) + " w ostatnim momencie " +
            (def_ob->is_weapon() ? "paruje" : "os�ania si� przed") + " lec�c� w niego" +
            ((ENV(me) == ENV(strzelajacy)) ? ", wystrzelon" +
            pocisk->koncowka("y", "�", "e", "ych", "e") + " z " + QSHORT(bron, PL_DOP) + " przez " +
            QSHORT(strzelajacy, PL_BIE) : " " + z_kierunku) + " " +
            pocisk->query_nazwa((def_ob->is_weapon() ? PL_BIE : PL_NAR)) + ".\n", ({strzelajacy, me}));
        strzelajacy->catch_msg(QCIMIE(me, PL_MIA) + " w ostanim momencie udaje si� " +
            (def_ob->is_weapon() ? "sparowa�" : "os�oni� si� przed") + " wystrzelon" +
            pocisk->koncowka("y", "�", "e", "ych", "e") + " przez Ciebie " +
            "z " + bron->short(strzelajacy, PL_DOP) + " " +
            pocisk->query_nazwa((def_ob->is_weapon() ? PL_BIE : PL_NAR)) +".\n");

        strzelajacy->increase_ss(CB_THIS_LW_SKILL(bron),
            F_BS_EXP(bron->query_potrzebna_umiejetnosc(), EXP_STRZELANIE_CELUNIK_TLWS));
        strzelajacy->increase_ss(CB_LW_SKILL,   F_BS_EXP(bron->query_potrzebna_umiejetnosc(), EXP_STRZELANIE_CELUNIK_LWS));
        strzelajacy->increase_ss(SS_DEX,        F_BS_EXP(bron->query_potrzebna_zrecznosc(), EXP_STRZELANIE_CELUNIK_DEX));
        strzelajacy->increase_ss(SS_STR,        F_BS_EXP(bron->query_potrzebna_sila(), EXP_STRZELANIE_CELUNIK_STR));

        if(def_ob->is_weapon())
        {
            strzelajacy->increase_ss(SS_DEX,        EXP_STRZELANIE_SPAROWALEM_DEX);
            strzelajacy->increase_ss(SS_PARRY,      EXP_STRZELANIE_SPAROWALEM_PARRY);
        }
        else
        {
            strzelajacy->increase_ss(SS_STR, EXP_STRZELANIE_OSLONILEM_STR);
            strzelajacy->increase_ss(SS_SHIELD_PARRY, EXP_STRZELANIE_OSLONILEM_SHIELD_PARRY);
        }

        if(ENV(me) != ENV(strzelajacy))
        {
            tell_roombb(ENV(strzelajacy), QCIMIE(TP, PL_MIA) + " wystrzeliwuje " + pocisk->query_nazwa(PL_MIA)+
                " z " + QSHORT(bron, PL_DOP) + " " + na_kierunek2 + ".\n", ({me, strzelajacy}));
        }
    }
    else if(rnd < what_defended[1] || !obr)
    {   //unikniemy
        me->catch_msg("W ostatnim momencie unikasz " + pocisk->query_nazwa(PL_DOP) + " " +
            ((ENV(me) == ENV(strzelajacy)) ? "wystrzelonej z " + bron->short(me, PL_DOP) + " " +
            "przez " + strzelajacy->short(me, PL_BIE) : "lec�cej w Ciebie " + z_kierunku) + ".\n");
        tell_roombb(ENV(me), QCIMIE(TP, PL_MIA) + " w ostatnim momencie unika " + pocisk->query_nazwa(PL_DOP) + " " +
            (ENV(me) == ENV(strzelajacy) ? " wystrzelonej z " + QSHORT(bron, PL_DOP) + " " +
            "przez " + QSHORT(strzelajacy, PL_BIE) : " lec�cej w niego z " + z_kierunku) + ".\n", ({me, strzelajacy}));
        strzelajacy->catch_msg(capitalize(me->short(strzelajacy, PL_MIA)) +
            " w ostatnim momencie uchyla si� przed " + pocisk->query_nazwa(PL_NAR) + " wystrzelon" +
            pocisk->koncowka("ym", "�", "ym", "nymi", "nymi") + " przez Ciebie z " + bron->short(strzelajacy, PL_DOP) + ".\n");

        if(ENV(me) != ENV(strzelajacy))
        {
            tell_roombb(ENV(strzelajacy), QCIMIE(TP, PL_MIA) + " wystrzeliwuje " + pocisk->query_nazwa(PL_MIA)+
                " z " + QSHORT(bron, PL_DOP) + " " + na_kierunek2 + ".\n", ({me, strzelajacy}));
        }

        strzelajacy->increase_ss(CB_THIS_LW_SKILL(bron),
            F_BS_EXP(bron->query_potrzebna_umiejetnosc(), EXP_STRZELANIE_CELUNIK_TLWS));
        strzelajacy->increase_ss(CB_LW_SKILL,   F_BS_EXP(bron->query_potrzebna_umiejetnosc(), EXP_STRZELANIE_CELUNIK_LWS));
        strzelajacy->increase_ss(SS_DEX,        F_BS_EXP(bron->query_potrzebna_zrecznosc(), EXP_STRZELANIE_CELUNIK_DEX));
        strzelajacy->increase_ss(SS_STR,        F_BS_EXP(bron->query_potrzebna_sila(), EXP_STRZELANIE_CELUNIK_STR));

        me->increase_ss(SS_DEX,          EXP_STRZELANIE_UNIKNOLEM_DEX);
        me->increase_ss(SS_DEFENSE,      EXP_STRZELANIE_UNIKNOLEM_DEFENSE);

        return 0;
    }

//     write("sila = ");dump_array(sila);
//     write("szybkosc = ");dump_array(szybkosc);
//     write("sila_naciagu = ");dump_array(sila_naciagu);
//     write("obr = ");dump_array(obr);
//     write("max_obr = ");dump_array(max_obr);

    object armour = TO->cb_got_hit(0, obr, strzelajacy, 0, TP->query_dt(), 1);

    if(armour->is_armour() && !F_BS_PRZEBIJE_ZBROJE(armour->query_ac(hid, dt), szybkosc))
    {
        strzelajacy->catch_msg("Wystrzel" + pocisk->koncowka("ony", "ona", "one", "eni", "one") +
            " przez ciebie z " + bron->short(strzelajacy, PL_DOP) + " " + pocisk->query_nazwa(PL_MIA) + " " +
            " odbija si� od " + armour->short(strzelajacy, PL_DOP) + " " + me->short(strzelajacy, PL_DOP) + ".\n");
        tell_roombb(ENV(me), (ENV(me) == ENV(strzelajacy) ? "Wystrzel" +
            pocisk->koncowka("ony", "ona", "one", "eni", "one") + " przez " + QIMIE(strzelajacy, PL_DOP) +
            " z " + QSHORT(bron, PL_DOP) : "Nadlatuj�cy " + z_kierunku) + " " +
            pocisk->query_nazwa(PL_MIA) + " odbija si� od " + QSHORT(armour, PL_DOP) + " " +
            QCIMIE(me, PL_DOP), ({me, strzelajacy}));
        me->catch_msg((ENV(me) == ENV(strzelajacy) ? "Wystrzel" +
            pocisk->koncowka("ony", "ona", "one", "eni", "one") + " przez " + strzelajacy->short(me, PL_BIE) + " z " +
            bron->short(me, PL_DOP) : "Lec�c " + pocisk->koncowka("y", "a", "e", "y", "e") + " " + z_kierunku) +
            " " + pocisk->query_nazwa(PL_MIA) + " odbija si� od twojej " + armour->short(me, PL_DOP) + ".\n");

        if(ENV(me) != ENV(strzelajacy))
        {
            tell_roombb(ENV(strzelajacy), QCIMIE(TP, PL_MIA) + " wystrzeliwuje " + pocisk->query_nazwa(PL_BIE) +
                " z " + QSHORT(bron, PL_DOP) + " " + na_kierunek2 + ".\n", ({me, strzelajacy}));
        }
    }

    strzelajacy->catch_msg("Wystrzelon" + pocisk->koncowka("y", "a", "e", "i", "e") +
        " przez ciebie z " + bron->short(strzelajacy, PL_DOP) + " " + pocisk->query_nazwa(PL_MIA) + " " +
        pocisk->opis_obrazen((100*obr/max_obr), pocisk->query_dt(), hid, hitlokacja, strzelajacy, me, armour, strzelajacy) +
        ".\n");
    me->catch_msg((ENV(me) == ENV(strzelajacy) ? "Wystrzelon" + pocisk->koncowka("y", "a", "e", "i", "e") +
        " przez " + strzelajacy->short(me, PL_BIE) + " z " + bron->short(me, PL_DOP) :
        "Lec�c" + pocisk->koncowka("y", "a", "e", "e", "e") + " " + z_kierunku) + " " + pocisk->query_nazwa(PL_MIA) + " " +
        pocisk->opis_obrazen((100*obr/max_obr), pocisk->query_dt(), hid,
        hitlokacja, strzelajacy, me, armour, me) + ".\n");

    strzelajacy->increase_ss(CB_THIS_LW_SKILL(bron),
        F_BS_EXP(bron->query_potrzebna_umiejetnosc(), EXP_STRZELANIE_UDANE_TLWS));
    strzelajacy->increase_ss(CB_LW_SKILL,   F_BS_EXP(bron->query_potrzebna_umiejetnosc(), EXP_STRZELANIE_UDANE_LWS));
    strzelajacy->increase_ss(SS_DEX,        F_BS_EXP(bron->query_potrzebna_zrecznosc(), EXP_STRZELANIE_UDANE_DEX));
    strzelajacy->increase_ss(SS_STR,        F_BS_EXP(bron->query_potrzebna_sila(), EXP_STRZELANIE_UDANE_STR));

    if(ENV(strzelajacy) != ENV(me))
    {
        tell_roombb(ENV(me), capitalize(z_kierunku) + " przylatuje ja" +
            pocisk->koncowka("ki�", "ka�", "kie�", "cy�", "kie�") +
            " " + pocisk->query_nazwa(PL_MIA) + ", kt�r" + pocisk->koncowka("y", "a", "e", "zy", "e") +
            " " + pocisk->opis_obrazen((100*obr/max_obr), pocisk->query_dt(), hid,
            hitlokacja, strzelajacy, me, armour, 0) + ".\n", ({me, strzelajacy}));

        tell_roombb(ENV(strzelajacy), QCIMIE(TP, PL_MIA) + " wystrzeliwuje " + pocisk->query_nazwa(PL_BIE) +
            " z " + QSHORT(bron, PL_DOP) + " " + na_kierunek2 + ".\n", ({me, strzelajacy}));
    }
    else
    {
        tell_roombb(ENV(strzelajacy), "Wystrzelon" + pocisk->koncowka("y", "a", "e", "i", "e") +
            " z " + QSHORT(bron, PL_DOP) + " przez " + QIMIE(strzelajacy, PL_DOP) + " " +
            pocisk->query_nazwa(PL_MIA) + " " +
            pocisk->opis_obrazen((100*obr/max_obr), pocisk->query_dt(), hid, hitlokacja,
            strzelajacy, me, armour, 0) + ".\n", ({me, strzelajacy}));
    }

    me->reduce_hp(0, obr * 100);

    //Zabijamy je�li hp poni�ej 0. FIXME: Do zmiany!
    if(me->query_hp() <= 0)
        me->do_die(strzelajacy);

    return 1; //FIXME: change to 0
}

/** ********************************************************************** **
 ** *************************** Z A S � O N Y  *************************** **
 ** ********************************************************************** **/

/**
 * Dzi�ki tej funkcji mo�emy kaza� livingowi kogo� zas�oni�.
 *
 * @param kogo    living kt�rego zas�aniamy.
 *
 * @return 1/0  sukces/pora�ka
 */
public int
cb_zaslon(object kogo)
{
    CQME;

    if(1)   //TODO: przepisa� na sprawdzenie czy 'kogo' walczy.
    {
        int szansa = SZANSA_ZASLONIENIA(me->query_skill(SS_VEILING), me->query_stat(SS_DEX),
            me->query_stat(SS_STR), kogo->query_skill(SS_VEILING), kogo->query_stat(SS_DEX),
            kogo->query_stat(SS_STR), STYLEMOD(CB_STL_MOD[stl][CB_STL_MOD_SZYBKOSC],
            qme()->query_skill(SS_STL_FIRST + qme()->query_fight_style())));

        if(random(100) > 100 - szansa)
        {   //Udana zas�ona
            zaslaniany = kogo;
            zaslaniany->dodaj_zaslaniajacego(me);
            return 1;
        }
        else
        {   //Nieudana zas�ona
            return 0;
        }
    }
    else
    {   //Nie walczy wi�c zas�ona zawsze udana.
        return 1;
    }
}

/**
 * Dzi�ki tej funkcji mo�emy kaza� livingowi spr�bowa� prze�ama� zas�on�
 * kogo� na kim�, je�li nie podamy kogo zas�on� chcemy prze�ama� przeciwnik
 * zostanie wybrany losowo.
 *
 * @param na_kim
 * @param kogo
 *
 * @return 1/0 sukces/pora�ka
 */
public int
cb_przelam_zaslone(object na_kim, object kogo = 0)
{
    CQME;

    if(!kogo)
    {
        object *tmp = na_kim->query_zaslaniajacy();
        kogo = tmp[random(sizeof(tmp))];
    }

    int szansa = SZANSA_PRZELAMANIA_ZASLONY(me->query_stat(SS_DEX), me->query_stat(SS_STR),
        kogo->query_skill(SS_VEILING), kogo->query_stat(SS_DEX), kogo->query_stat(SS_STR),
        STYLEMOD(CB_STL_MOD[stl][CB_STL_MOD_SZYBKOSC], qme()->query_skill(SS_STL_FIRST +
        qme()->query_fight_style())));

    if(random(100) > 100 - szansa)
    {   //Udane prze�amanie zas�ony
        kogo->przestan_zaslaniac();

        return 1;
    }
    else    //Nieudane prze�amanie
        return 0;
}

/**
 * Dzi�ki tej funkcji mo�emy kaza� livingowi przesta� zas�ania� innego livinga
 *
 * @return 1/0 sukces/pora�ka
 */
public int
cb_przestan_zaslaniac()
{
    CQME;

    zaslaniany->usun_zaslaniajacego(me);
    zaslaniany = 0;
}

/**
 * Dodajemy zas�aniaj�cego nas.
 *
 * @param zas
 */
public void
cb_dodaj_zaslaniajacego(object zas)
{
    CQME;

    if(!pointerp(zaslaniajacy))
        zaslaniajacy = ({});

    zaslaniajacy += ({zas});
}

/**
 * Usuwamy zas�aniaj�cego nas.
 *
 * @param zas
 */
public void
cb_usun_zaslaniajacego(object zas)
{
    CQME;

    if(!pointerp(zaslaniajacy))
    {
        zaslaniajacy = ({});
        return;
    }

    zaslaniajacy -= ({zas});
}

/**
 * @return Poziom nago�ci, zawsze 0 poniewa� nie humanoidy z natury s� nagie,
 *         nikogo to nie dziwi i nie rzuca si� w oczy.
 */
public int
cb_poziom_nagosci()
{
    CQME;

    return 0;
}

/**
 * @return Liste zas�aniaj�cych.
 */
public object *
cb_query_zaslaniajacy()
{
    CQME;

    return zaslaniajacy;
}

/** *********************************************************************** **
 ** ****************  F U N K C J E  P O M O C N I C Z E  ***************** **
 ** *********************************************************************** **/

nomask int
szansa_obrony(int x)
{
    float z = itof(x);

    return ftoi(
        1.34414467371461E-07    * (z * z * z * z) -
        5.80659522691049E-08    * (z * z * z) -
        0.00271879472283097     * (z * z) +
        0.248967254604981       * (z) +
        88.8521598528623) + 1;
}
