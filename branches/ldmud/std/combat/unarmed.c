/**
 * \file /std/combat/unarmed.c
 *
 *  Unarmed initialization routines.
 *
 * These routines can inherited by all living objects that need
 * unarmed combat values of a standardized type.
 *
 * Plik ten jest inheritowany przez @param /std/creature.c
 */
#pragma save_binary
#pragma strict_types

#include "combath.h"

#include <macros.h>
#include <wa_types.h>
#include <przymiotniki.h>

static  mapping     ua_attdata,     /* wchit, wcpen, damtype and %use */
                    ua_hitdata;     /* acarray, %hit */
static  int         ua_attuse;      /* %attacked used each turn */

/*
 * Prototypes
 */
public void cr_reset_attack(int aid);
public void cr_reset_hitloc(int hid);

#define QEXC (this_object()->query_combat_object())

/**
 * Funkcja pozwala na wy��czenie podanego ataku
 *
 * @param aid Identyfikator ataku.
 */
public varargs void
disable_attack(int aid)
{
    if(!is_mapping_index(aid, ua_attdata))
        return;

    ua_attdata[aid] += ({ua_attdata[aid][3]});
    ua_attdata[aid][3] = 0;

    if(QEXC && !objectp(QEXC->cb_query_weapon(aid)))
        this_object()->cr_reset_attack(aid);
}

/**
 * Funkcja pozwala na w��czenie podanego ataku
 *
 * @param aid Identyfikator ataku.
 */
public varargs void
enable_attack(int aid)
{
    if(!is_mapping_index(aid, ua_attdata))
        return;

    if(sizeof(ua_attdata[aid]) >= 9)
        ua_attdata[aid][3] = ua_attdata[aid][-1];

    ua_attdata[aid] = ua_attdata[aid][0..-1];

    if(QEXC && !objectp(QEXC->cb_query_weapon(aid)))
        this_object()->cr_reset_attack(aid);
}

/**
 * Ustawia informacje o ataku.
 *
 * @param aid   Identyfikator ataku.
 * @param hit   Klasa uderzenia.
 * @param pen   Klasa penetracji.
 * @param dt    Typ zadawanych obra�e�
 * @param puse  Szansa u�ycia w ka�dej turze.
 * @param nazwa Nazwa w mianowniku lub pe�na odmiana z rodzajem.
 * @param przym Przymiotnik lub przymitniki w formie tablicy (otp).
 * @param ng    Nazwa g��wna w mianowniku lub pe�na odmiana nazwy g��wnej z rodzajem (opt).
 */
public varargs void
set_attack_unarmed(int aid, int hit, int pen, int dt, int puse, mixed nazwa, mixed przym = 0, mixed ng = 0)
{
    int rodz, ns, gs;

    if(!mappingp(ua_attdata))
        ua_attdata = ([]);

    if(nazwa)
    {
        if(pointerp(nazwa))
        {
            ns = sizeof(nazwa);

            if(!ns || !pointerp(nazwa[0]) || sizeof(nazwa[0]) != 6 ||
                (ns == 3 && !pointerp(nazwa[1]) || sizeof(nazwa[1]) != 6 || !intp(nazwa[2])) ||
                (ns == 2 && !intp(nazwa[1])))
            {
                throw("B��dna odmiana nazwy w funkcji set_attack_unarmed().");
            }
        }
        else if(stringp(nazwa))
            nazwa = slownik_pobierz(nazwa);
        else
            throw("B��dna odmiana nazwy w funkcji set_attack_unarmed().");

        rodz  = nazwa[-1];
        nazwa = nazwa[0..-2];
    }

    if(ng)
    {
        if(pointerp(ng))
        {
            gs = sizeof(ng);

            if(!gs || !pointerp(ng[0]) || sizeof(ng[0]) != 6 ||
                (gs == 3 && !pointerp(ng[1]) || sizeof(ng[1]) != 6 || !intp(ng[2])) ||
                (gs == 2 && !intp(ng[1])))
            {
                throw("B��dna odmiana nazwy w funkcji set_attack_unarmed().");
            }
        }
        else if(stringp(ng))
            ng = slownik_pobierz(ng);
        else
            throw("B��dna odmiana nazwy w funkcji set_attack_unarmed().");

        rodz = ng[-1];
        ng = ng[0..-2];
    }

    if(stringp(przym))
        przym = ({ PRZYM_Z_MNOGA(przym) });
    else if(pointerp(przym))
    {
        for(int i = 0 ; i < sizeof(przym) ; i++)
        {
            if(stringp(przym[i]))
                przym[i] = PRZYM_Z_MNOGA(przym[i]);
            else if(!pointerp(przym[i]) || sizeof(przym[i]) != 2 || !stringp(przym[i][0]) || !stringp(przym[i][1]))
                throw("B��dnie podana tablica przymiotnik�w do funkcji set_attack_unarmed().\n");
        }
    }
    else if(przym)
        throw("B��dnie podana tablica przymiotnik�w do funkcji set_attack_unarmed().\n");

    ua_attdata[aid] = ({ hit, pen, dt, puse, rodz, nazwa, przym, ng });

    /*
     * If we have a combat object and no weapon for this attack then
     * modify direct
     */
    if(QEXC && !objectp(QEXC->cb_query_weapon(aid)))
        this_object()->cr_reset_attack(aid);
}

/**
 * @param aid Identyfikator ataku
 *
 * @return Wszystki informacje o sprawdzanym ataku.
 */
public mixed *
query_ua_attack(int aid)
{
    return ua_attdata[aid];
}

/**
 * Ustawia informacje o hitlokacji.
 *
 * @param hid   Identyfikator hitlokacji
 * @param ac    Ac hitlokacji.
 * @param phit  Procentowa szansa na trafienie w hitlokacje.
 * @param nazwa Tablica z nazwami hitlokacji, lub nazwa w mianowniku.
 * @param przym Przymiotniki nazwy hitlokacji.
 * @param hp    czy doda� info o hp.
 *
 * @return 0 - zawsze
 */
public varargs void
set_hitloc_unarmed(int hid, int *ac, int phit, mixed nazwa, mixed przym = 0, int hp = 1)
{
    int ns, rodz;

    if(!mappingp(ua_hitdata))
        ua_hitdata = ([]);

    if(pointerp(nazwa))
    {
        ns = sizeof(nazwa);

        if(!ns || !pointerp(nazwa[0]) || sizeof(nazwa[0]) != 6 ||
            (ns == 3 && !pointerp(nazwa[1]) || sizeof(nazwa[1]) != 6 || !intp(nazwa[2])) ||
            (ns == 2 && !intp(nazwa[1])))
        {
            throw("B��dna odmiana nazwy w funkcji set_hitloc_unarmed().");
        }
    }
    else if(stringp(nazwa))
        nazwa = slownik_pobierz(nazwa);
    else
        throw("B��dna odmiana nazwy w funkcji set_hitloc_unarmed().");

    rodz  = nazwa[-1];
    nazwa = nazwa[0..-2];

    if(stringp(przym))
        przym = ({ PRZYM_Z_MNOGA(przym) });
    else if(pointerp(przym))
    {
        for(int i = 0 ; i < sizeof(przym) ; i++)
        {
            if(stringp(przym[i]))
                przym[i] = PRZYM_Z_MNOGA(przym[i]);
            else if(!pointerp(przym[i]) || sizeof(przym[i]) != 2 || !stringp(przym[i][0]) || !stringp(przym[i][1]))
                throw("B��dnie podana tablica przymiotnik�w do funkcji set_hitloc_unarmed().\n");
        }
    }
    else if(przym)
        throw("B��dnie podana tablica przymiotnik�w do funkcji set_hitloc_unarmed().\n");

    ua_hitdata[hid] = ({ ac, phit, nazwa, rodz, przym });

    /*
     * If we have a combat object and no armour for this hitlocation then
     * modify direct
     */
    if(QEXC && !objectp(QEXC->cb_query_armour(hid)))
        this_object()->cr_reset_hitloc(hid);
}

/**
 * @param hid Identyfikator hitlokacji
 *
 * @return Wszystki informacje o sprawdzanej hitlokacji.
 */
public mixed *
query_ua_hitloc(int hid)
{
    return ua_hitdata[hid];
}

/*
 * Description: Set the %attacks used each turn. 100% is one attack / turn
 * Arguments:   sumproc: %attack used
 */
public void
set_attackuse(int sumproc)
{
    ua_attuse = sumproc;

    if (QEXC)
        QEXC->cb_set_attackuse(sumproc);
}

/*
 * Description: Give the %attacks used each turn. 100% is one attack / turn
 * Returns:     %attack used
 */
public int
query_attackuse() { return ua_attuse; }

/*
 * Function name: cr_configure
 * Description:   Configures basic values for this creature.
 */
public void
cr_configure()
{
    if(mappingp(ua_hitdata))
        map(m_indexes(ua_hitdata), cr_reset_hitloc);

    if(mappingp(ua_attdata))
        map(m_indexes(ua_attdata), cr_reset_attack);
#if 0
    //FIXME: Nie jestem pewien czy to powinno by� wyrzucone
    if(mappingp(ua_hitdata))
        map(m_indexes(ua_hitdata), cr_reset_hitloc);
#endif

    if(ua_attuse)
        QEXC->cb_set_attackuse(ua_attuse);
}

/*
 * Function name: cr_reset_attack
 * Description:   Set the values for a specific attack. These are called from
 *		  the external combat object.
 * Arguments:     aid: The attack id
 */
public void
cr_reset_attack(int aid)
{
    mixed att;

    if(!mappingp(ua_attdata))
    {
        ua_attdata = ([]);
        return;
    }

    att = ua_attdata[aid];

    if(sizeof(att) >= 4)
        QEXC->cb_add_attack(att[0], att[1], att[2], att[3], aid);
}

/*
 * Function name: cr_reset_hitloc
 * Description:   Set the values for a specific hitlocation
 * Arguments:     hid: The hitlocation (bodypart) id
 */
public void
cr_reset_hitloc(int hid)
{
    mixed hloc;

    if(!mappingp(ua_hitdata))
        ua_hitdata = ([]);

    if(is_mapping_index(hid, ua_hitdata))
    {
        hloc = ua_hitdata[hid];

        if(sizeof(hloc) >= 5)
        {
            QEXC->cb_add_hitloc(hloc[0], hloc[1], hloc[2] + ({hloc[3]}),
                hid, hloc[4]);
        }
    }
}

/*
 * Function name: cr_try_hit
 * Description:   Decide if a certain attack fails because of something
 *                related to the attack itself.
 * Arguments:     aid:   The attack id
 * Returns:       True if hit, otherwise 0.
 */
public int
cr_try_hit(int aid) { return 1; }

/**
 * @param aid   Identyfikator ataku.
 * @param przyp Przypadek.
 *
 * @return Opis ataku o podanym @param aid w podanym @param przyp
 */
public string
cr_attack_desc(int aid, int przyp = PL_NAR)
{
    if(is_mapping_index(aid, ua_attdata) && sizeof(ua_attdata[aid]) >= 8)
    {
        string nazwa = "";

        if(ua_attdata[aid][7])
        {
            nazwa += ua_attdata[aid][7][przyp];
            przyp = PL_DOP;
        }

        if(pointerp(ua_attdata[aid][6]))
            foreach(string *przym : ua_attdata[aid][6])
                nazwa += oblicz_przym(przym[0], przym[1], przyp, ua_attdata[aid][4], 0);

        nazwa += ua_attdata[aid][5][przyp];

        return nazwa;
    }
    else
        return "\n\nB��d un11 (" + aid + "). Zg�o� go opisuj�c okoliczno�ci w jakich do niego dosz�o.\n\n";
}

/**
 * @param aid   Identyfikator ataku
 *
 * @return Rodzaj nazwy ataku.
 */
public int
cr_query_attack_rodzaj(int aid)
{
    if(is_mapping_index(aid, ua_attdata) && sizeof(ua_attdata[aid]) >= 8)
        return ua_attdata[aid][4];

    return -1;
}

/*
 * Function name: cr_got_hit
 * Description:   Tells us that we got hit. It can be used to reduce the ac
 *                for a given hitlocation for each hit.
 * Arguments:     hid:   The hitloc id
 *                ph:    The %hurt
 *                att:   Attacker
 *		  aid:   The attack id
 *                dt:    The damagetype
 *		  dam:   The damage in hitpoints
 */
public varargs void
cr_got_hit(int hid, int ph, object att, int aid, int dt, int dam)
{
}

/*
 * Function name:  cr_attacked_by
 * Description:    This routine is called when we are attacked or when
 *                 someone we are hunting appears in our location. This
 *		   routine is simply a notification of the fact. The combat
 *		   system will start a fight without us doing anything here.
 * Arguments:	   ob: The attacker
 */
public void
cr_attacked_by(object ob)
{
}
