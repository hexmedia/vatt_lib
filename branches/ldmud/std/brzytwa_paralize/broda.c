#pragma strict_types
#include <macros.h>

inherit "/std/paralyze";

void
create_paralyze()
{
    set_name("strzyze_sobie_brode");
    set_stop_verb("przesta�");
    set_stop_message("Przestajesz strzyc sobie brod�.\n");
    set_fail_message("Jeste� teraz zaj�t"+TP->koncowka("y","a")+
                     " strzy�eniem si�. Musisz wpisa� "+
                "'przesta�' by m�c zrobi� to co chcesz.\n");
    set_finish_object(this_object());
    set_finish_fun("koniec_strzyzenia_brody");
    set_remove_time(15);
    setuid();
    seteuid(getuid());
}

void
koniec_strzyzenia_brody(object player)
{
    player->catch_msg("Ko^nczysz strzyc sobie brod�.\n");
    player->add_old_fatigue(-7);
    player->set_dlugosc_brody(0.0);
    player->przym_od_zarostu();
    saybb(QCIMIE(player,PL_MIA)+" ko^nczy strzyc sobie brod�.\n");
    remove_object();
    return;
}
