
/**
 * \file /std/object.c
 *
 * Contains all basic routines for configurable objects.
 *
 * TODO:
 * - Rodzaje gramatyczne mo�na troche przerobi�, bo nie konieczne jest u�ycie
 *   zapisywanie rodzaju do ka�dego przypadku i do liczby pojedynczej i mnogiej.(Krun)
 */

#pragma save_binary
#pragma strict_types

#include <exp.h>
#include <std.h>
#include <time.h>
#include <debug.h>
#include <flags.h>
#include <macros.h>
#include <cmdparse.h>
#include <filepath.h>
#include <ss_types.h>
#include <composite.h>
#include <materialy.h>
#include <object_types.h>
#include <przymiotniki.h>
#include <stdproperties.h>

string jak_pachnie, jak_wacha_jak_sie_zachowuje; /*dwie takie male pierdolki do
                                      wachania przedmiotow. Lil  */

static string  *obj_shorts;        /* Odmiana short'a przez przypadki w lp. */
static string  *obj_pshorts;       /* Odmiana short'a przez przypadki w lmn. */
static mixed    obj_long;          /* Long description */
static string  *obj_commands;      /* The commands for each command item */
static mixed    obj_subloc;        /* nazwa sublokacji, w kt�rej znajduje si� obiekt */
static mixed   *nazwa_glowna = 0;
static mixed   *obj_names = ({ ({ }), ({ }), ({ }), ({ }), ({ }), ({ }) });
                /* Nazwy obiektu w liczbie pojedynczej */
static mixed   *obj_pnames = ({ ({ }), ({ }), ({ }), ({ }), ({ }), ({ }) });
                /* Nazwy obiektu w liczbie mnogiej */
static mixed   *obj_rodzaje = ({ ({ }), ({ }), ({ }), ({ }), ({ }), ({ }) });
static mixed   *obj_prodzaje = ({ ({ }), ({ }), ({ }), ({ }), ({ }), ({ }) });
                /* Rodzaje gramatyczne nazw obiektu */
static mixed    obj_przym = ({ ({ }), ({ }) });
                /* Lista przymiotnikow - mian lp i lmn */
static mixed    obj_items;          /* The items (pseudo look) of this object */
static mixed    obj_cmd_items;      /* The command items of this object */
static mixed    obj_state;          /* The internal state, used for light etc */
static mixed    magic_effects;      /* Magic items effecting this object. */
static int      obj_no_show;        /* Don't display this object. */
static int      obj_no_show_c;      /* Don't show this object in composite desc */
static int      obj_no_change;      /* Lock value for configuration */
static int      obj_rodzaj_shorta;  /* Jak sama nazwa wskazuje */
static int      obj_sum_sklad;      /* Sumaryczna wielko�� udzia��w materia��w */
static object   obj_previous;       /* Caller of function resulting in VBFC */
static mapping  obj_props;          /* Object properties */
static mapping  obj_sklad;          /* Materia�y, z jakich sk�ada si� obiekt */
private static int hb_index;        /* Identification of hearbeat callout */
private static int reset_interval;  /* Constant used to set reset interval */
private static int reset_alarm;     /* Alarm wywo�ywany do resetowania lokacji */

/* zmienne do obiektow na ktorych mozna siadac */
static string   sit_cmd;
static string   sit_long;
static string   sit_up;
static string   sit_przyimek;
static int      sit_space;
static int      sit_przyp;

static string  *object_owners = ({ });
static int      obj_type = O_INNE;
static string  *orig_place = ({ });
static string  *who_stole = ({ });

static int      last_reset_time;

static int      reset_ilosc = 0;

static string   obj_inited;         /* czy zosta�o ju� za�adowane i z jakim argumentem.
                                       nie za�aduje drugi raz tego samego, coby si�
                                       przedmioty nie podwaja�y. */
/*
 * Prototypes
 */
public          void    add_prop(string prop, mixed val);
public          void    remove_prop(string prop);
public          void    set_no_show_composite(int i);
public  varargs void    add_name(mixed name, int przyp, mixed rodzaje);

public          int     search_hidden(object obj, object who);
public          int     is_live_dead(object obj, int what);
public  varargs int     dodaj_nazwy(mixed nazwy, mixed pnazwy, int rodzaj);

public  varargs mixed   check_call(mixed retval, object for_obj);
public          mixed   query_prop(string prop);
public  varargs mixed   query_adj(int arg, int przyp);
public  varargs mixed   query_przym(int arg, int przyp, int rodzaj);
public  varargs mixed   query_pprzym(int arg, int przyp, int rodzaj);
public  string         *query_owners();

/*
 * PARSE_COMMAND
 *
 * These lfuns are called from within the efun parse_command() to get the
 * three different sets of ids. If no plural ids are returned then the
 * efun will try to make pluralforms from the singular ids.
 *
 * If no normal ids are returned then parse_command will never find the object.
 */
// Nazwa glowna
public string *
parse_command_ng_id_list(int przyp)
{
    if (pointerp(nazwa_glowna))
        return ({ nazwa_glowna[0][przyp] });
    else
        return ({ });
}

public int *
parse_command_ng_rodzaj_id_list(int przyp)
{
    if (pointerp(nazwa_glowna))
        return ({ nazwa_glowna[2] });
    else
        return ({ });
}

public string *
parse_command_ng_plural_id_list(int przyp)
{
    if (pointerp(nazwa_glowna))
        return ({ nazwa_glowna[1][przyp] });
    else
        return ({ });
}

public int *
parse_command_ng_plural_rodzaj_id_list(int przyp)
{
    if (pointerp(nazwa_glowna))
        return ({ nazwa_glowna[2] });
    else
        return ({ });
}
// Koniec nazwy glownej

public string *
parse_command_id_list(int przyp)
{
    return obj_names[przyp];
}

public int *
parse_command_rodzaj_id_list(int przyp)
{
    return obj_rodzaje[przyp];
}

public string *
parse_command_plural_id_list(int przyp)
{
    return obj_pnames[przyp];
}

public int *
parse_command_plural_rodzaj_id_list(int przyp)
{
    return obj_prodzaje[przyp];
}

public string *
parse_command_adjectiv1_id_list()
{
    string *lista;

    lista = obj_przym[0];

    //Dodajemy zniszczony lub z�amany je�li obiekt taki jest.
    if(query_prop(OBJ_I_BROKEN) == 1)
        lista += ({"zniszczony"});
    else if(query_prop(OBJ_I_BROKEN) == 2)
        lista += ({"z�amany"});

    return lista;
}

public string *
parse_command_adjectiv2_id_list()
{
    string *lista;

    lista = obj_przym[1];

    //Dodajemy zniszczony lub z�amany je�li obiekt taki jest.
    if(TO->query_prop(OBJ_I_BROKEN) == 1)
        lista += ({"zniszczeni"});
    else if(TO->query_prop(OBJ_I_BROKEN) == 2)
        lista += ({"z�amani"});

    return lista;
}

/*
 * Pokazuje, jak dany przedmiot pachnie.
 *                                    Lil.
 */
int
smell_effect()
{
    if (jak_pachnie) write(check_call(jak_pachnie)+"\n");
    if (jak_wacha_jak_sie_zachowuje) saybb(QCIMIE(this_player(), PL_MIA)+
        " "+check_call(jak_wacha_jak_sie_zachowuje)+"\n");
    return 1;
}

/*
 * Function name: set_heart_beat
 * Description:   Emulate old heartbeat code
 * Arguments:     repeat - 1 to enable, 0 to disable
 * Returns:       Return value from set_alarm()
 */
nomask int
set_heart_beat(mixed repeat, string func = "heart_beat")
{
    float delay;
    int ret;
    object tp;

    if (intp(repeat))
        delay = itof(repeat * 2);
    else if (floatp(repeat))
        delay = repeat;
    else
        throw("B��dny argument 1 do set_heart_beat.\n");

    remove_alarm(hb_index);

    if (delay > 0.0)
    {
        tp = this_player();
        set_this_player(0);
        ret = set_alarm(delay, delay, mkfunction(func));
        set_this_player(tp);
    }
    return hb_index = ret;
}

/*
 * Function name: create_object
 * Description:   Create the object (Default for clones)
 */
public void
create_object()
{
    add_prop(OBJ_I_WEIGHT, 1000);       /* 1 Kg is default */
    add_prop(OBJ_I_VOLUME, 1000);       /* 1 l is default */
    obj_no_change = 0;
    obj_no_show = 0;

    dodaj_nazwy(({"przedmiot", "przedmiotu", "przedmiotowi",
        "przedmiot", "przedmiotem", "przedmiocie"}),
        ({"przedmioty", "przedmiot�w", "przedmiotom",
        "przedmioty", "przedmiotami", "przedmiotach"}),
        PL_MESKI_NOS_NZYW);
}

public nomask varargs void enable_reset(int on);
public nomask void reset();

/*
 * Function name: create
 * Description  : Object constructor, called directly after load / clone.
 *                It calls the public create function and sets the only
 *                default variable.
 */
public nomask void
create()
{
    string ob_name;

    ob_name = OB_NAME(this_object());

    obj_names = ({ ({ ob_name }), ({ ob_name }), ({ ob_name }),
        ({ ob_name }), ({ ob_name }), ({ ob_name }) });
    obj_rodzaje = ({ ({ 4 }), ({ 4 }), ({ 4 }),
        ({ 4 }), ({ 4 }), ({ 4 }) });

    this_object()->create_object();

#if 0
   if (pointerp(obj_names) && sizeof(obj_names) == 6)
        obj_names = map(obj_names, &operator(+)( , ({ ob_name }) ));
    if (pointerp(obj_rodzaje) && sizeof(obj_rodzaje) == 6)
        obj_rodzaje = map(obj_rodzaje, &operator(+)( , ({ 4 }) ));
#endif
}

/*
 * Function name: reset_object
 * Description:   Reset the object (Default for clones)
 */
#if 0
public void
reset_object()
{
}
#endif

/**
 * @return wraca czas co jaki resetowany b�dzie obiekt. Czas w granicach od 30 minut do 150 minut
 */
public float rnd_reset_time()
{
    return 1800.0 + frandom(7200.0, 3);
}

/*
 * Function name: reset
 * Description:   Reset the object (always called, used as constructor)
 * Arguments:     arg: The reset argument.
 */
public nomask void
reset()
{
    enable_reset(reset_interval);

    TO->reset_object();
}

/*
 * Function name: enable_reset
 * Description  : Used to enable or disable resets in an object. The reset
 *                interval is based on the factor given as argument to this
 *                function. By default, the reset time will averagely be
 *                slightly larger than one hour. By using a factor, this period
 *                can be increased or decreased, using the following formula:
 *                    Reset interval = 60 minutes * (100 / factor)
 * Arguments    : (optional) int factor - when omitted, the factor will default
 *                to 100 (60 minutes). Use 0 to disable resets in this object.
 *                Valid values for the factor are in the range 10 to 200, which
 *                make for a reset interval of approximately 600 to 30 minutes
 *                on average.
 */
public nomask varargs void
enable_reset(int on = 100)
{
    //je�li ma pragme no reset to nie resetujemy: Resety chce przenie�� do drivera dlatego
    //na pragmie.
    if(is_pragma_no_reset() || !function_exists("reset_object", this_object()))
        return;

    float reset_time;

    if (obj_no_change || !on)
        return;

    on = min(200, max(1, on));

    reset_interval = on;

    if(sizeof(get_alarm(reset_alarm)))
        remove_alarm(reset_alarm);

    if (reset_interval)
    {
        reset_time = rnd_reset_time() * itof(reset_interval) / 100.0;

        if (reset_time < 0.0)
            reset_time = 0.0;

            reset_alarm = set_alarm(reset_time, 0.0, reset);
    }
}

/*
 * Function name: get_this_object()
 * Description  : Always returns the objectpointer to this object.
 * Returns      : object - this_object()
 */
object
get_this_object()
{
    return this_object();
}

/*
 * Function name: update_actions
 * Description:   Updates our defined actions in all relevant objects.
 */
public void
update_actions()
{
    if (environment(this_object()))
        move_object(environment(this_object()));
}

/*
 * Nazwa funkcji : id
 * Opis          : Ta funkcja jest uzywana do sprawdzenia, czy dany obiekt
 *		   ma podana nazwe, w liczbie pojedynczuej, w podanym
 *		   przypadku.
 * Argumenty     : string  - sprawdzana nazwa,
 *		   int     - [opcjonalnie, domyslnie mianownik] sprawdzany
 *		             przypadek.
 * Funkcja zwraca: int 1/0 - prawda, jesli rzeczywiscie obiekt ma taka nazwe
 *			     w podanym przypadku w liczbie pojedynczej.
 */
public varargs int
id(string str, int przyp = PL_MIA)
{
     return (member_array(str, obj_names[przyp]) >= 0);
}

/*
 * Nazwa funkcji : plural_id
 * Opis          : Ta funkcja jest uzywana do sprawdzenia, czy dany obiekt
 *		   ma podana nazwe, w liczbie mnogiej, w podanym przypadku.
 * Argumenty     : string  - sprawdzana nazwa,
 *		   int     - [opcjonalnie, domyslnie mianownik] sprawdzany
 *		             przypadek.
 * Funkcja zwraca: int 1/0 - prawda, jesli rzeczywiscie obiekt ma taka nazwe
 *			     w podanym przypadku w liczbie mnogiej.
 */
public varargs int
plural_id(string str, int przyp)
{
     return (member_array(str, obj_pnames[przyp]) >= 0);
}

/*
 * Nazwa funkcji : long
 * Opis          : Opisuje obiekt, lub jeden z pseudo-itemow zdefiniowanych
 *		   w nim. Rozpoznaje ewentualne VBFC.
 * Argumenty     : string str - pseudo-item do opisania. Jest to rzecz
 *				dodana przy uzyciu add_item. Jesli ta
 *				zmienna jest ustawiona na 0, zwrocony
 *				zostanie caly opis obiektu.
 *		   object for_obj - obiekt, dla ktorego przeznaczony jest
 *				    ten dlugi opis.
 * Funkcja zwraca: string - opis calego obiektu, albo pseudo-itemu.
 */
varargs public mixed
long(string str, object for_obj)
{
    int index;

    if (!str)
        return process_tags(check_call(obj_long, for_obj) ?:
               "Jest to nieopisany obiekt.\n");

    if (!pointerp(obj_items))
        return 1;

    index = sizeof(obj_items);
    while(--index >= 0)
        if (member_array(str, obj_items[index][0]) >= 0)
            return process_tags(check_call(obj_items[index][1]) ?:
                   "Nie zauwa^zasz niczego specjalnego.\n");

    /* If we end up here there were no such item. Why 1? /Mercade */
    return 1;
}

/*
 * Function name: query_long
 * Description  : Gives the set long description. This does not evaluate
 *                possible VBFC but returns exactly what was set as long
 *                description with set_long().
 * Returns      : mixed - exactly what was given to set_long(). This can
 *                        either be the long description of the VBFC in
 *                        string of functionpointer form.
 */
public mixed
query_long()
{
    return process_tags(obj_long);
}

/*
 * Function name: check_seen
 * Description:   True if this object can be seen by a given object
 * Arguments:     for_obj: The object for which visibilty should be checked
 * Returns:       1 if this object can be seen.
 */
public int
check_seen(object for_obj)
{
//    int aw, seed;

    if (!objectp(for_obj) || obj_no_show || for_obj->query_prop(EYES_CLOSED) ||
        (!for_obj->query_wiz_level() &&
        (for_obj->query_prop(LIVE_I_SEE_INVIS) < this_object()->query_prop(OBJ_I_INVIS) ||
        for_obj->query_skill(SS_AWARENESS) < this_object()->query_prop(OBJ_I_HIDE))))
    {
        return 0;
    }
#if 0
    if (!objectp(for_obj) ||
	obj_no_show ||
	(!for_obj->query_wiz_level() &&
	 (for_obj->query_prop(LIVE_I_SEE_INVIS) <
	  this_object()->query_prop(OBJ_I_INVIS))))
	return 0;

//    aw = for_obj->query_skill(SS_AWARENESS) / 2;
//    sscanf(OB_NUM(for_obj), "%d", seed);
    //Nie wiem po co to seed tu jest. Nie mam zielonego poj�cia
    //co numer obiektu ma wsp�lnego z mo�no�ci� jego znalezienia....
    //Mo�e kto� wyt�umaczy?:P (Krun)

//    if (aw + random(aw, (seed + query_prop(OBJ_I_HIDE))) <
//        query_prop(OBJ_I_HIDE))
//    return 0;
// Nie wiem co za debil to pisa�... Ale NIE MO�NA w takich miejsach
// u�ywa� losowych formu�ek!!! Nie zawsze przecie� wylosuje to samo!!!
// Przywracam wersje staroarkowa! (Krun)

    if (for_obj->query_skill(SS_AWARENESS) < this_object()->query_prop(OBJ_I_HIDE))
        return 0;
#endif //Taki kurwa bolek.. Przez to si� wszystko jeba�o.. Autor tego g�wna niech si� wstydzi...
       //Je�li to kto� od nas to wisi mi flache:P
    return 1;
}

/*
 * Nazwa funkcji : odmien_short
 * Opis          : Wymusza odmiane shorta w liczbie pojedynczej przez
 *		   przypadki, korzystajac z przymiotnikow i glownej
 *		   nazwy obiektu.
 */
public void
odmien_short()
{
    int c;

    obj_shorts = allocate(6);

    if (pointerp(nazwa_glowna))
    {
        for (c = 0; c < 6; c++)
        {
            if (sizeof(obj_przym[0]))
            {
                obj_shorts[c] = nazwa_glowna[0][c] + " " +
                    implode(query_przym(1, PL_DOP), " ") + " " +
                    obj_names[PL_DOP][0];
            }
            else
                obj_shorts[c] = nazwa_glowna[0][c] + " " + obj_names[PL_DOP][0];
        }
        obj_rodzaj_shorta = (int)nazwa_glowna[2];
    }
    else
    {
        for (c = 0; c < 6; c++)
        {
            if (sizeof(obj_przym[0]))
            {
                obj_shorts[c] = implode(query_przym(1, c), " ") + " " +
                    obj_names[c][0];
            }
            else
                obj_shorts[c] = obj_names[c][0];
        }
        obj_rodzaj_shorta = (int)obj_rodzaje[0][0];
    }
}

/*
 * Nazwa funkcji : short
 * Opis          : Zwraca krotki opis tego obiektu. Tablica odmiany shorta
 *		   przez przypadki zostanie utworzona, jesli obiekt jeszcze
 *		   nie ma jej ustawionej (albo w kodzie, albo poprzez
 *		   poprzednie wywolanie short() ). Tworzenie tablicy odmiany
 *		   shorta polega na laczeniu dostepnych przymiotnikow
 *		   z nazwami. Shorty moga byc wartosciami typu VBFC.
 * Argumenty     : object for_obj - Obiekt ktory chce shorta.
 *                 int    przyp   - Przypadek w jakim short ma byc zwrocony.
 * Funkcja zwraca: string - krotki opis obiektu w liczbie pojedynczej.
 */
public varargs string
short(mixed for_obj, mixed przyp)
{
    int c;

    if (!objectp(for_obj))
    {
        if (intp(for_obj))
            przyp = for_obj;
        else if (stringp(for_obj))
            przyp = atoi(for_obj);

        for_obj = previous_object();
    }
    else
        if (stringp(przyp))
            przyp = atoi(przyp);

    if (!pointerp(obj_shorts))
        odmien_short();

    if (query_prop(OBJ_I_BROKEN))
    {
        if (query_prop(OBJ_I_BROKEN) == 2)
        {
            return oblicz_przym("z�amany", "z�amani", przyp,
                this_object()->query_rodzaj(), 0) + " " +
                check_call(obj_shorts[przyp], for_obj);
        }

        return oblicz_przym("zniszczony", "zniszczeni", przyp,
            this_object()->query_rodzaj(), 0) + " " +
            check_call(obj_shorts[przyp], for_obj);
    }

    return check_call(obj_shorts[przyp], for_obj);
}

/*
 * Function name:   vbfc_short
 * Description:     Gives short as seen by previous_object
 * Returns:         string holding short()
 * Arguments:       pobj: Object which to make the relation for
 *                  if not defined we assume that we are doing a vbfc
 *                  through the vbfc_object
 */
varargs public string
vbfc_short(mixed pobj, mixed przyp)
{
    object tp;

    if (!objectp(pobj))
    {
        if (intp(pobj))
            przyp = pobj;
        else if (stringp(pobj))
            przyp = atoi(pobj);

        pobj = previous_object(-1);
    }
    else
        if (stringp(przyp))
            przyp = atoi(przyp);

    if (!this_object()->check_seen(pobj) ||
	!CAN_SEE_IN_ROOM(pobj))
    {
	return ({ "co^s", "czego^s", "czemu^s", "co^s", "czym^s", "czym^s" })[przyp];
    }

    return short(pobj, przyp);
}

/*
 * Nazwa funkcji :
 * Opis          :
 * Argumenty     :
 * Funkcja zwraca:
 */
varargs public string
vbfc_cshort(mixed pobj, mixed przyp)
{
    /*if (!objectp(pobj))
    {
        if (intp(pobj))
            przyp = pobj;
        else if (stringp(pobj))
            przyp = atoi(pobj);

        pobj = previous_object(-1);
    }
    else
        if (stringp(przyp))
            przyp = atoi(przyp);
    */

    string ret = vbfc_short(pobj, przyp);
    ret = capitalize(ret);

    return ret;
}

/*
 * Nazwa funkcji : query_short
 * Opis          : Funkcja zwraca krotki opis w liczbie pojedycznej w podanym
 *		   przypadku, bez odwolywania sie do VBFC. Zwraca dokladnie
 *		   to samo, co zostalo ustawione przez ustaw_shorty().
 *		   *UWAGA* Mozliwe, ze ta funkcja w przyszlosci zostanie
 *		   usunieta!
 * Argumenty     : int przyp - short w ktorym przypadku ma zostac zwrocony.
 * Funkcja zwraca: string - krotki opis obiektu, w zadanym przypadku,
 *			    w liczbie pojedynczej.
 */
public varargs string
query_short(int przyp)
{
    if (!pointerp(obj_shorts)) {
//        odmien_short();
//        if (!pointerp(obj_shorts)) {
            return 0;
//        }
    }


    return obj_shorts[przyp];
}

/*
 * Nazwa funkcji : odmien_plural_short
 * Opis          : Wymusza odmiane shorta w liczbie mnogiej przez
 *		   przypadki, korzystajac z przymiotnikow i glownej
 *		   nazwy obiektu.
 */
public void
odmien_plural_short()
{
    int c;

    if (!sizeof(obj_pnames[0]))
        return 0;

    obj_pshorts = allocate(6);

    if (pointerp(nazwa_glowna))
    {
	for (c = 0; c < 6; c++)
    	    if (sizeof(obj_przym[0]))
        	obj_pshorts[c] = nazwa_glowna[1][c] + " " +
		    implode(query_pprzym(1, PL_DOP), " ") + " " +
        	    obj_names[PL_DOP][0];
    	    else
        	obj_pshorts[c] = nazwa_glowna[1][c] + " " + obj_names[PL_DOP][0];
    }
    else
    {
	for (c = 0; c < 6; c++)
    	    if (sizeof(obj_przym[0]))
        	obj_pshorts[c] = implode(query_pprzym(1, c), " ") + " " +
        	    obj_pnames[c][0];
    	    else
        	obj_pshorts[c] = obj_pnames[c][0];
    }
}

/*
 * Nazwa funkcji : plural_short
 * Opis          : Zwraca krotki opis tego obiektu w liczbie mnogiej.
 *		   Tablica odmiany mnogiego shorta przez przypadki zostanie
 *		   utworzona, jesli obiekt jeszcze nie ma jej ustawionej
 *		   (albo w kodzie poprzez ustaw_shorty(), albo poprzez
 *		   poprzednie wywolanie short() ). Tworzenie tablicy odmiany
 *		   shorta polega na laczeniu dostepnych przymiotnikow
 *		   z nazwami w liczbie mnogiej. Shorty moga byc wartosciami
 *		   typu VBFC.
 * Argumenty     : object for_obj - Obiekt ktory chce shorta.
 *                 int    przyp   - Przypadek w jakim short ma byc zwrocony.
 * Funkcja zwraca: string - krotki opis obiektu w liczbie mnogiej.
 */
public varargs string
plural_short(mixed for_obj, int przyp)
{
    int c;

    if (intp(for_obj))
    {
        przyp = for_obj;
        for_obj = this_player();
    }
    /* Nie sprawdzamy czy przyp to int, bo nie ma makr do tej funkcji */

    if (!pointerp(obj_pshorts))
        odmien_plural_short();

    if (objectp(for_obj) && !check_seen(for_obj))
        return 0;

    if (query_prop(OBJ_I_BROKEN)) {
        if (query_prop(OBJ_I_BROKEN) == 2)
            return oblicz_przym("z�amany", "z�amani", przyp,
                    this_object()->query_rodzaj(), 1) + " " +
                check_call(obj_pshorts[przyp], for_obj);
        return oblicz_przym("zniszczony", "zniszczeni", przyp,
                this_object()->query_rodzaj(), 1) + " " +
            check_call(obj_pshorts[przyp], for_obj);
    }

    return check_call(obj_pshorts[przyp], for_obj);
}

/*
 * Nazwa funkcji : query_plural_short
 * Opis          : Funkcja zwraca krotki opis w liczbie mnogiej w podanym
 *		   przypadku, bez odwolywania sie do VBFC. Zwraca dokladnie
 *		   to samo, co zostalo ustawione przez ustaw_shorty().
 *		   *UWAGA* Mozliwe, ze ta funkcja w przyszlosci zostanie
 *		   usunieta!
 * Argumenty     : int przyp - short w ktorym przypadku ma zostac zwrocony.
 * Funkcja zwraca: string - krotki opis obiektu, w zadanym przypadku,
 *			    w liczbie mnogiej.
 */
public varargs string
query_plural_short(int przyp)
{
    if (!pointerp(obj_pshorts))
/*        odmien_plural_short();*/
          return 0;

    return obj_pshorts[przyp];
}

/*
 * Nazwa funkcji : query_tylko_mn
 * Opis          : Zwraca 1, gdy short (jesli zdefiniowany), lub nazwa
 *		   glowna obiektu jest nazwa nie majaca liczby pojedynczej.
 *		   (np. slowo 'drzwi').
 * Funkcja zwraca: int - patrz wyzej.
 */
public varargs int
query_tylko_mn()
{
    if (obj_rodzaj_shorta != 0)
        return (obj_rodzaj_shorta < 0);

    return (obj_rodzaje[0][0] < 0);
}

/**
 * Zale�nie od rodzaju nazwy g��wnej obiektu zwraca jedn� z ko�c�wek.
 * Oczywi�cie mo�e mie� szersze zastosowanie do jakichkolwiek
 * string�w maj�cych by� uzale�nionymi od rodzaju. Ostatnie dwie
 * ko�c�wki b�d� u�yte wy��cznie w przypadku gdy obiekt ma nazwy
 * wy��cznie w liczbie mnoegiej (np. drzwi), lub aktualnie w formie
 * mnogiej wyst�puje (np. monety, ludzie).
 *
 * @param konc Ta wersja funkcji za argument przyjmuje r�no-elementow�
 *             tablic� ( maksymalnie 5 element�w ) przy czym
 *             dwa pierwsze elementy s� obowi�zkowe.
 *
 * @return Wybran� w zale�no�ci od aktualnego rodzaju shorta
 *         i nazwy obiektu ko�c�wk�.
 *
 * @see koncowka
 */
public string koncowkav(string *konc)
{
    int rodzaj = this_object()->query_rodzaj();

    if(query_tylko_mn())
    {
        if(rodzaj == -(PL_MESKI_OS+1))
            return konc[3];
        else
            return konc[4];
    }

    switch(rodzaj)
    {
        case PL_MESKI_OS:
        case PL_MESKI_NOS_ZYW:
        case PL_MESKI_NOS_NZYW:
            return konc[0];
        case PL_ZENSKI:
            return konc[1];
        case PL_NIJAKI_OS:
        case PL_NIJAKI_NOS:
            if(!konc[2])
                return konc[0];
            else
                return konc[2];
    }
}

/**
 * Zale�nie od rodzaju nazwy g��wnej obiektu zwraca jedn� z ko�c�wek.
 * Oczywi�cie mo�e mie� szersze zastosowanie do jakichkolwiek
 * string�w maj�cych by� uzale�nionymi od rodzaju. Ostatnie dwie
 * ko�c�wki b�d� u�yte wy��cznie w przypadku gdy obiekt ma nazwy
 * wy��cznie w liczbie mnoegiej (np. drzwi), lub aktualnie w formie
 * mnogiej wyst�puje (np. monety, ludzie).
 *
 * @param meski         rodzaj m�ski, lp
 * @param zenski        rodzaj �e�ki, lp
 * @param nijaki        rodzaj nijaki, lp
 * @param mos           m�skoosobowa, lm
 * @param mnos          niem�skoosobowa, lm
 *
 * Obowi�zkowe s� tylko dwa pierwsze argumenty. W przypadku gdy nie
 * podana zosanie ko�c�wka dla rodzaju nijakiego algorytm u�yje
 * ko�c�wki dla rodzaju m�skiego.
 *
 * @return Wybran� w zale�no�ci od aktualnego rodzaju shorta
 *         i nazwy obiektu ko�c�wk�.
 *
 * @see koncowkav
 */
public string
koncowka(string meski, string zenski, string nijaki = 0,
    string mos = 0, string mnos = 0)
{
    return koncowkav(({meski, zenski, nijaki, mos, mnos}));
}

/*
 * Nazwa funkcji : query_zaimek
 * Opis          : Funkcja zwraca zaimek przedmiotu, na ktorej wywolujemy
 *		   te funkcje, w podanym przypadku i liczbie.
 * Argumenty     : int przypadek - przypadek szukanego zaimka.
 *		   int dluga	 - czy forma zaimka ma byc w dlugiej formie.
 *				   (np. zaimek mezczyzny, w celowniku ma
 *				   dluga forme 'jemu', zas krotka 'mu').
 *				   (opcjonalny)
 *		   int liczba	 - 0 pojedyczna, 1 mnoga (opcjonalny).
 * Funkcja zwraca: string - zaimek - patrz wyzej.
 */
public string
query_zaimek(int przypadek, int dluga = 0, int liczba = 0)
{
    int rodzaj;

    rodzaj = this_object()->query_rodzaj();

    if (!liczba)
    {
	switch (przypadek)
	{
        case PL_MIA:
            switch (rodzaj)
            {
		case PL_MESKI_OS:
		case PL_MESKI_NOS_ZYW:
		case PL_MESKI_NOS_NZYW: return "on";
		case PL_ZENSKI: return "ona";
		case PL_NIJAKI_OS:
		case PL_NIJAKI_NOS: return "ono";
            }
        case PL_DOP:
            switch (rodzaj)
            {
		case PL_ZENSKI: return "jej";
		default: return (dluga ? "jego" : "go");
            }
        case PL_CEL:
            switch(rodzaj)
            {
		case PL_ZENSKI: return "jej";
		default: return (dluga ? "jemu" : "mu");
            }
        case PL_BIE:
            switch(rodzaj)
            {
		case PL_MESKI_OS:
		case PL_MESKI_NOS_ZYW:
		case PL_MESKI_NOS_NZYW: return (dluga ? "jego" : "go");
		case PL_ZENSKI: return "j^a";
		case PL_NIJAKI_OS:
		case PL_NIJAKI_NOS: return (dluga ? "jego" : "je");
            }
        case PL_NAR:
            switch(rodzaj)
            {
		case PL_ZENSKI: return "ni^a";
		default: return "nim";
            }
        case PL_MIE:
            switch(rodzaj)
            {
		case PL_ZENSKI: return "niej";
		default: return "nim";
            }
	}
    }
    else
    {
	switch (przypadek)
	{
	    case PL_MIA:
		switch(rodzaj)
		{
		    case PL_MESKI_OS: return "oni";
		    default: return "one";
		}
	    case PL_DOP:
		return "ich";
	    case PL_CEL:
		return "im";
	    case PL_BIE:
		switch(rodzaj)
		{
		    case PL_MESKI_OS: return "ich";
		    default: return "je";
		}
	    case PL_NAR:
		return "nimi";
	    case PL_MIE:
		return "nich";
	}
    }
    return 0;
}

/*
 * Function name: add_prop
 * Description:   Add a property to the property list
 *                If the property already exist, the value is replaced
 *                If a function "add_prop" + propname is declared or
 *                is shadowing this_object then that function is called
 *                prior to the setting of the property.
 *                NOTE
 *                  If the optional function above returns something other
 *                  than 0. The property will NOT be set.
 *
 * Arguments:     prop - The property string to be added.
 *                val: The value of the property
 * Returns:       None.
 */
public void
add_prop(string prop, mixed val)
{
    mixed oval;

    /* If there isn't a value, remove the current value. */
    if (!val)
    {
        remove_prop(prop);
        return;
    }

    /* All changes might have been locked out. */
    if (obj_no_change)
        return;

    if (call_other(this_object(), "add_prop" + prop, val))
        return;

    if (!mappingp(obj_props))
        obj_props = ([ ]);

    oval = query_prop(prop);
    obj_props[prop] = val;

    if (environment())
        environment()->notify_change_prop(prop, query_prop(prop), oval);
}

/*
 * Function name: change_prop
 * Description  : This function is a mask of add_prop. For details, see
 *                the header of that function.
 */
public void
change_prop(string prop, mixed val)
{
    add_prop(prop, val);
}

/*
 * Function name: remove_prop
 * Description:   Removes a property string from the property list.
 * Arguments:     prop - The property string to be removed.
 */
public void
remove_prop(string prop)
{
    /* All changes may have been locked out. */
    if (obj_no_change || !mappingp(obj_props) || !prop)
        return;

    if (call_other(this_object(), "remove_prop" + prop))
        return;

    if (environment())
        environment()->notify_change_prop(prop, 0, query_prop(prop));

    obj_props = m_delete(obj_props, prop);
}

#define CFUN
#ifndef CFUN
/*
 * Function name: query_prop
 * Description:   Find the value of a property.
 * Arguments:     prop - The property searched for.
 * Returns:       The value or 0.
 */
public mixed
query_prop(string prop)
{
    if (!mappingp(obj_props))
        return 0;

    return check_call(obj_props[prop]);
}
#else
public mixed
query_prop(string prop) = "query_prop";
#endif

/*
 * Function name: query_props
 * Description:   Give all the existing properties
 * Returns:       An array of property names or 0.
 */
public nomask mixed
query_props()
{
    if (mappingp(obj_props))
        return m_indexes(obj_props);
    else
        return 0;
}

public nomask mixed
query_props_val()
{
    mixed toReturn = ({});
    mixed props = query_props();

    if (!props)
        return 0;

    foreach (string prop : props)
        toReturn += ({({prop, query_prop(prop)})});

    return toReturn;
}

/*
 * Nazwa: is_prop_set
 * Opis: Sprawdzenie, czy jaka� w�a�ciwo�� jest ustawiona
 * Argumenty: prop - Nazwa w�a�ciwo�ci.
 * Zwraca: 1 - dana w�a�ciwo�� jest ustawiona
 *         0 - dana w�a�ciwo�� nie jest ustawiona
 */

public int
is_prop_set(string prop)
{
        if (!mappingp(obj_props)) {
                return 0;
        }
        if (member_array(prop, m_indexes(obj_props)) != -1) {
                return 1;
        }
        return 0;
}

/*
 * Function name: query_prop_setting
 * Description:   Returns the true setting of the prop
 * Arguments:     prop - The property searched for
 * Returns:       The true setting (mixed)
 */
public nomask mixed
query_prop_setting(string prop)
{
    if (!mappingp(obj_props))
	return 0;
    return obj_props[prop];
}

/*
 * Function name: notify_change_prop
 * Description:   This function is called when a property in an object
 *                in the inventory has been changed.
 * Arguments:     prop - The property that has been changed.
 *                val  - The new value.
 *                oval - The old value
 */
public void
notify_change_prop(string prop, mixed val, mixed oval)
{
}

/*
 * materia�y
 */

/*
 * Nazwa funkcji : ustaw_material
 * Opis          : Ustawia, �e obiekt sk�ada si� mi�dzy innymi z danego materia�u.
 * Argumenty     : string material - nazwa materia�u
 *                 int udzial - wielko�� udzia�u
 * Funkcja zwraca: ---
 */

public void
ustaw_material(string material, int udzial = 100)
{
	if ((!stringp(material)) || (udzial < 0)) {
		return;
	}

	if (!mappingp(obj_sklad)) {
		obj_sklad = ([ ]);
	}

    if (obj_sklad[material] > 0) {
        obj_sum_sklad -= obj_sklad[material];
    }

	obj_sklad[material] = udzial;
	obj_sum_sklad += udzial;

    if (udzial == 0) {
        obj_sklad = m_delete(obj_sklad, material);
    }
}

/*
 * Nazwa funkcji : udzial_materialu
 * Opis          : Zwraca, ile dany materia� ma udzia�u w obiekcie.
 * Argumenty     : string material - nazwa materia�u
 * Funkcja zwraca: int - udzia� materia�u w obiekcie.
 */

public int
udzial_materialu(string material)
{
	if ((!stringp(material)) || (!mappingp(obj_sklad))) {
		return 0;
	}
	return obj_sklad[material];
}

/*
 * Nazwa funkcji : procent_materialu
 * Opis          : Zwraca, ile dany materia� procentowo sk�ada si� na obiekt.
 * Argumenty     : string material - nazwa materia�u
 * Funkcja zwraca: float - procentowy udzia� materia�u w obiekcie.
 */

public float
procent_materialu(string material)
{
	if ((!stringp(material)) || (!mappingp(obj_sklad))) {
		return 0.0;
	}
	if (member_array(material, m_indices(obj_sklad)) == -1) {
		return 0.0;
	}
	return ((itof(obj_sklad[material])/itof(obj_sum_sklad)) * 100.0);
}

/*
 * Nazwa funkcji: query_materialy
 * Opis:          Zwraca tablice nazw materialow (jako stringi)
 *               posortowan� od najwi�kszego udzia�u materia�u do najmniejszego
 */
public string* //Vera
query_materialy()
{
    return sort_array(m_indices(obj_sklad));
}

/*
 * ---
 */

/*
 * Function name: mark_state
 * Description:   Mark the internal state so that update is later possible
 */
public void
mark_state()
{
    /* More properties can be added here if need be
     */
    obj_state = ({ query_prop(OBJ_I_LIGHT), query_prop(OBJ_I_WEIGHT),
		   query_prop(OBJ_I_VOLUME) });
}

/*
 * Function name: update_state
 * Description:   Update the environment according to the changes in our
 *                state;
 */
public void
update_state()
{
    int l, w, v;

    l = query_prop(OBJ_I_LIGHT);
    w = query_prop(OBJ_I_WEIGHT);
    v = query_prop(OBJ_I_VOLUME);

    /* More properties can be added here if need be
     */
    if (environment(this_object()))
	environment(this_object())->update_internal(l - obj_state[0],
						    w - obj_state[1],
						    v - obj_state[2]);
}

/**
 * Funkcja przemieszczaj�ca obiekt.
 *
 * Obiekt przemieszczany jest do miejsca okre�lonego za pomoc� napisu/obiektu.
 * Je�eli drugi argument jest r�wny <b>1</b>, nie s� wykonywane �adne testy.
 * W innym przypadku drugi argument okre�la sublokacj�, do kt�rej funkcja
 * ma przemie�ci� obiekt.
 *
 * @param dest obiekt lub nazwa pliku, gdzie ma zosta� przemieszczony obiekt
 * @param subloc
 *        <ul>
 *          <li> <b>1</b> zawsze przemie�ci�
 *          <li> nazwa sublokacji
 *        </ul>
 *
 * @return Kod okre�laj�cy wynik przemieszczenia:
 *         <ul>
 *           <li> <b>0</b> Sukces</li>
 *           <li> <b>1</b> Zbyt ci�ki dla miejsca docelowego</li>
 *           <li> <b>2</b> Nie mo�e by� opuszczony</li>
 *           <li> <b>3</b> Nie mo�e by� wyj�ty z dotychczasowego kontenera</li>
 *           <li> <b>4</b> Obiekt nie mo�e by� umieszczany w torbach, etc.</li>
 *           <li> <b>5</b> Miejsce docelowe nie przyjmuje obiekt�w</li>
 *           <li> <b>6</b> Obiekt nie mo�e by� podniesiony</li>
 *           <li> <b>7</b> Inne (Tekst zosta� wypisany w funkcji <i>move()</i>)</li>
 *           <li> <b>8</b> Zbyt du�y dla miejsca docelowego</li>
 *           <li> <b>9</b> Dotychczasowy kontener jest zamkni�ty</li>
 *           <li> <b>10</b> Docelowy kontener jest zamkni�ty</li>
 *           <li> <b>11</b> Jest za du�o rzeczy w docelowym kontenerze</li>
 *         </ul>
 */
varargs public int
move(mixed dest, mixed subloc, int force = 0, int test_only = 0)
{
    object          old;
    int             is_room, rw, rv, is_live_dest, is_live_old,
                    srw, srv, ra, sra, ogrRze, ogrWag, ogrObj, sa,
                    uw,uv,
                    sw,sv;
    mixed           tmp;
    mixed          tmpsubloc;

    ogrRze = 0;
    ogrWag = 0;
    ogrObj = 0;

    if (!dest)
        return 5;

    old = environment(this_object());
    if (stringp(dest))
    {
        call_other(dest, "??");
        dest = find_object(dest);
    }
    if (!objectp(dest))
        dest = old;

    int illegal = SECURITY->check_if_location_is_forbidden(TO, dest);
    if(illegal == 2 && ENV(TO))
    {
        if(TO->query_wiz_level())
            TO->catch_msg("Ta lokacja jest dla Ciebie terenem zakazanym. Nie mo�esz si� tam przedosta�.\n");
        else
            TO->catch_msg("Jaka� magiczna si�a unieruchamia Ci� i przez chwile nie pozwala Ci si� rusza�.\n");
        return 7;
    }

    if (subloc == 1)
    {
        if (test_only)
            return 0;

        move_object(dest);

        obj_subloc = 0;
        subloc = 0;

    }
    else if (old != dest)
    {
        if (!force)
        {
            if (!dest || !dest->query_prop(CONT_I_IN) || dest->query_prop(CONT_M_NO_INS))
                return 5;
            if ((old) && (old->query_prop(CONT_M_NO_REM)))
                return 3;
            if (old && old->query_prop(CONT_I_CLOSED) &&
                    intp(this_object()->query_subloc()) &&
                    (this_object()->query_subloc() == 0))
            {
                return 9;
            }

            if (old && !(intp(this_object()->query_subloc()) && (this_object()->query_subloc() == 0)) &&
                    old->query_subloc_prop(this_object()->query_subloc(), CONT_I_CLOSED))
            {
                return 9;
            }

            if(illegal && ENV(TO))
            {
                if(TO->query_wiz_level())
                    TO->catch_msg("Ta lokacja jest dla Ciebie terenem zakazanym. Nie mo�esz si� tam przedosta�.\n");
                else
                    TO->catch_msg("Jaka� magiczna si�a unieruchamia Ci� i przez chwile nie pozwala Ci si� rusza�.\n");
                return 7;
            }
        }

        if (old)
            is_live_old = (function_exists("create_container",
                old) == "/std/living");

        is_live_dest = (function_exists("create_container",
            dest) == "/std/living");

        if (old && is_live_old && this_object()->query_prop(OBJ_M_NO_DROP) && !force)
            return 2;

        is_room = (int) dest->query_prop(ROOM_I_IS);

        if (!force)
        {
            if (!is_live_dest)
            {
                if ((!is_room) && (this_object()->query_prop(OBJ_M_NO_INS)))
                    return 4;
                if (dest && dest->query_prop(CONT_I_CLOSED) && intp(subloc) && (subloc == 0))
                    return 10;
                if (dest && !(intp(subloc) && (subloc == 0)) && dest->query_subloc_prop(subloc, CONT_I_CLOSED))
                    return 10;
            }
            else
            {
                if ((!is_live_old) && (this_object()->query_prop(OBJ_M_NO_GET)))
                    return 6;
                else if (is_live_old && this_object()->query_prop(OBJ_M_NO_GIVE))
                    return 3;
            }
        }

        rw = dest->query_prop(CONT_I_MAX_WEIGHT) -
        dest->query_prop(OBJ_I_WEIGHT);
        if (dest->is_prop_set(CONT_I_MAX_WEIGHT))
            ogrWag = 1;

        rv = dest->volume_left();

        if (dest->is_prop_set(CONT_I_MAX_VOLUME))
            ogrObj = 1;

        ra = dest->query_prop(CONT_I_MAX_RZECZY) -
        dest->query_prop(CONT_I_IL_RZECZY);

        if (dest->is_prop_set(CONT_I_MAX_RZECZY))
            ogrRze = 1;

        if  (subloc != 1)
        {
            srw = dest->query_subloc_prop(subloc, CONT_I_MAX_WEIGHT) -
                dest->query_subloc_prop(subloc, CONT_I_WEIGHT);
            srv = dest->query_subloc_prop(subloc, CONT_I_MAX_VOLUME) -
                dest->query_subloc_prop(subloc, CONT_I_VOLUME);
            sra = dest->query_subloc_prop(subloc, CONT_I_MAX_RZECZY) -
                dest->query_subloc_prop(subloc, CONT_I_IL_RZECZY);

            if (srw < 0)
                srw = 0;
            if (srv < 0)
                srv = 0;
            if (sra < 0)
                sra = 0;

            if (dest->is_subloc_prop_set(subloc, CONT_I_MAX_WEIGHT))
            {
                if (ogrWag)
                    rw = (srw > rw ? rw : srw);
                else
                {
                    rw = srw;
                    ogrWag = 1;
                }
            }
            if (dest->is_subloc_prop_set(subloc, CONT_I_MAX_VOLUME))
            {
                if (ogrObj)
                    rv = (srv > rv ? rv : srv);
                else
                {
                    rv = srv;
                    ogrObj = 1;
                }
            }
            if (dest->is_subloc_prop_set(subloc, CONT_I_MAX_RZECZY))
            {
                if (ogrRze)
                    ra = (sra > ra ? ra : sra);
                else
                {
                    ra = sra;
                    ogrRze = 1;
                }
            }
        }

        if (rw < 0)
            rw = 0;
        if (rv < 0)
            rv = 0;
        if (ra < 0)
            ra = 0;

        if (!query_prop(HEAP_I_IS))
        {
            if (!force)
            {
                if (ogrWag)
                    if (rw < query_prop(OBJ_I_WEIGHT))
                        return 1;

                if (ogrObj)
                    if (rv < query_prop(OBJ_I_VOLUME))
                        return 8;

                if (ogrRze)
                    if (ra == 0)
                        return 11;
            }
        }
        else
        {
            sw = 0;
            sv = 0;
            sa = 0;
            if (ogrWag)
            {
                if (rw < query_prop(OBJ_I_WEIGHT))
                {
                    uw = query_prop(HEAP_I_UNIT_WEIGHT);
                    if ((uw > rw) && !force)
                        return 1;

                    sw = rw / uw; /* Taka ilo�� stosu mo�e by� uniesiona */
                    sv = sw;
                }
            }
            if (ogrObj)
            {
                if (rv < query_prop(OBJ_I_VOLUME))
                {
                    uv = query_prop(HEAP_I_UNIT_VOLUME);
                    if ((uv > rv) && !force)
                        return 8;

                    sv = rv / uv; /* Taka ilo�� stosu mo�e by� uniesiona */
                    if (!sw)
                        sw = sv;
                }
            }
        if (!test_only) {
            if (ogrRze) {
                sa = ra;
                if (sw || sv || sa) {
                    this_object()->split_heap((sw < sv) ?
                            ((sw < sa) ? sw : sa) : ((sv < sa) ? sv : sa));
                }
            }
            else {
                if (sw || sv)
                    this_object()->split_heap((sw < sv) ? sw : sv);
            }
        }
        }

        if (old && old->prevent_leave(this_object()) && !force)
            return 7;

        if (dest && dest->prevent_enter(this_object()) && !force)
            return 7;

        if (!test_only)
            move_object(dest);
    }

    if (!test_only)
    {
        tmpsubloc = obj_subloc;
        obj_subloc = subloc;
    }

    if (old != dest)
    {
        if (old)
        {
            if (!test_only) {
                if (sizeof(filter(all_inventory(old)-({this_object()}), &->signal_leave(this_object(), dest)))) {
                    if (!force) {
                        return 7;
                    }
                }
            }

            if (test_only)
                return 0;

            this_object()->leave_env(old, dest, tmpsubloc);
            old->leave_inv(this_object(),dest);
            old->subloc_leave_inv(tmpsubloc, this_object());
        }

        if (test_only)
            return 0;

        if (dest)
        {
            this_object()->enter_env(dest, old);
            dest->enter_inv(this_object(),old);
            dest->subloc_enter_inv(subloc, this_object());
            (all_inventory(dest) - ({ this_object() }))->signal_enter(this_object(), old);
        }
    }

    if (test_only)
        return 0;

    mark_state();
    return 0;
}

/* Ustawia wartosc zmiennej obj_subloc
 * (Rantaur)
 */
void set_obj_subloc(string subloc)
{
    obj_subloc = subloc;
}
/*
 * Function name: query_subloc
 * Description:   Get the current sub location's name
 */
mixed
query_subloc()
{
    return obj_subloc;
}

/*
 * Nazwa funkcji: is_in_subloc
 * Opis: Sprawdza, czy rzecz znajduje si� w podanej sublokacji
 * Argumenty: mixed sloc - nazwa sublokacji
 * Zwraca: 1 - przedmiot znajduje si� w takiej sublokacji
 *         0 - przedmiot nie znajduje si� w takiej sublokacji
 */

int
is_in_subloc(mixed sloc)
{
    return query_subloc() ~= sloc;
}

/*
 * Function name: enter_inv
 * Description  : This function is called each time an object enters the
 *                inventory of this object. If you mask it, be sure that
 *                you _always_ call the ::enter_inv(ob, old) function.
 * Arguments    : object ob  - the object entering our inventory.
 *                object old - wherever 'ob' came from. This can be 0.
 */
void
enter_inv(object ob, object old)
{
}

/*
 * Function name: enter_env
 * Description  : This function is called each time this object enters a
 *                new environment. If you mask it, be sure that you
 *                _always_ call the ::enter_env(dest, old) function.
 * Arguments    : object dest - the destination we are entering.
 *                object old  - the location we came from. This can be 0.
 */
void
enter_env(object dest, object old)
{
	if((this_object()->query_prop("_siedzonko")) == 1)
		if((dest->query_prop(ROOM_I_IS)) == 1)
			dest->add_sit(sit_cmd, sit_long, sit_up, sit_space, ({ this_object() }), sit_przyp, sit_przyimek);
}

/*
 * Function name: leave_inv
 * Description  : This function is called each time an object leaves the
 *                inventory of this object. If you mask it, be sure that
 *                you _always_ call the ::leave_inv(ob, old) function.
 * Arguments    : object ob   - the object leaving our inventory.
 *                object dest - wherever 'ob' goes to. This can be 0.
 */
void
leave_inv(object ob, object dest)
{
}

private int
owner_pred(object ob, string* owners)
{
    return (member_array(MASTER_OB(ob), owners) != -1);
}

/*
 * Function name: leave_env
 * Description  : This function is called each time this object leaves an
 *                old environment. If you mask it, be sure that you
 *                _always_ call the ::leave_env(dest, old) function.
 * Arguments    : object old  - the location we are leaving.
 *                object dest - the destination we are going to. Can be 0.
 *                string fromSubloc - z jakiej sublokacji przenosimy obiekt
 */
varargs void
leave_env(object old, object dest, string fromSubloc)
{
    if((this_object()->query_prop("_siedzonko")) == 1)
        if((old->query_prop(ROOM_I_IS)) == 1)
            old->remove_sit(sit_cmd, sit_space, ({ this_object() }));
    if (stringp(query_owners()))
        TO->set_owners(query_owners());
    if (objectp(old) && sizeof(query_owners()) && interactive(dest)) {
        object *present = all_inventory(old) - ({ this_object() });
        object *owners = filter(present, &owner_pred(, query_owners()));
        foreach (object owner : owners) {
            owner->signal_steal(dest,TO);
        }
    }
}

/*
 * Function name: recursive_rm
 * Description  : When an object is removed, its complete inventory is
 *                mapped through this function. If it is an interactive
 *                object, it will be moved to its default starting
 *                location else it will be descructed.
 * Arguments    : object ob - the object to remove.
 */
void
recursive_rm(object ob)
{
    if (query_ip_number(ob))
        ob->move(ob->query_default_start_location());
    else
        ob->remove_object();
}

/*
 * Function name: remove_object
 * Description:   Removes this object from the game.
 * Returns:       True if the object was removed.
 */
public int
remove_object()
{
    if (!TO)
        return 1;
    map(all_inventory(), recursive_rm);
    if (environment(this_object()))
    {
        if(strlen(sit_long))
        {
            environment(this_object())->kolesie_wstaja(this_object());
        }
        environment(this_object())->leave_inv(this_object(),0);
        this_object()->leave_env(environment(this_object()),0);
    }
    if (interactive())
        saybb(QCIMIE(this_object(), PL_MIA) + " opuszcza �wiat Vatt'gherna.\n");
    destruct();
    return 1;
}

/*
 * Function name: vbfc_caller
 * Description:   This function will hopfully return correct object which this
 *                vbfc is for.
 * Returns:       The object who wants a vbfc
 */
public object
vbfc_caller()
{
    return obj_previous;
}

#ifndef CFUN
/*
 * Function name: check_call
 * Description  : This is the function that resolves VBFC. Call it with a
 *                an argument in either function-VBFC or string-VBFC and this
 *                function will parse the argument, make the necessary call
 *                and return the queried value. If a non-VBFC argument is
 *                passed along, it will be returned unchanged. For more
 *                information on VBFC, see 'man VBFC'.
 * Arguments    : mixed retval   - an argument containing VBFC.
 *                object for_obj - the object that wants to know, if omitted,
 *                                 use previous_object().
 * Returns      : mixed - the resolved VBFC.
 */
public nomask varargs mixed
check_call(mixed retval, object for_obj = previous_object())
{
    int             more;
    string          a, b;
    mixed           proc_ret;

    if (functionp(retval))
    {
        obj_previous = for_obj;

        proc_ret = retval();

        obj_previous = 0;
        return proc_ret;
    }

    if (!stringp(retval))
    {
        return retval;
    }

    obj_previous = for_obj;

    more = sscanf(retval, "@@%s@@%s", a, b);

    if (more == 0 && wildmatch("@@*", retval))
        proc_ret = process_value(extract(retval, 2), 1);
    else if (more == 1 || (more == 2 && !strlen(b)))
        proc_ret = process_value(a, 1);
    else
        proc_ret = process_string(retval, 1);

    obj_previous = 0;
    return proc_ret;
}
#else
public nomask varargs mixed
check_call(mixed retval, object for_obj = previous_object()) = "check_call";
#endif

/*
 * Function name: reset_euid
 * Description  : This function can be called externally to make sure that
 *                the euid of this object is set exactly to the uid of the
 *                object. All it does is: seteuid(getuid(this_object()));
 */
public void
reset_euid()
{
    seteuid(getuid());
}

/*
 * Function name: add_list
 * Description:   Common routine for set_name, set_pname, set_adj etc
 * Arguments:     list: The list of elements
 *                elem: string holding one new element.
 *                first: True if it is the main name, pname adj
 * Returns:       The new list.
 */
private string *
add_list(string *list, mixed elem, int first)
{
    string *e;

    if (obj_no_change)
        return list;

    if (pointerp(elem))
        e = elem;
    else
        e = ({ elem });

    if (!pointerp(list))
        list = ({ }) + e;
    else if (first)
        list = e + (list - e);
    else
        list = list + (e - list);

    return list;
}

/*
 * Function name: del_list
 * Description:   Removes one or many elements from a list.
 * Arguments:     list_old: The list as it looks.
 *                list_del: What should be deleted
 * Returns:       The new list.
 */
private string *
del_list(string *list_old, mixed list_del)
{
#if 0
    int il, pos;

    if (obj_no_change)
	return list_old;                /* All changes has been locked out */

    if (!pointerp(list_del))
	list_del = ({ list_del });

    for (il = 0; il < sizeof(list_del); il++)
    {
	pos = member_array(list_del[il], list_old);
	if (pos >= 0)
	    list_old = exclude_array(list_old, pos, pos);
    }
    return list_old;
#endif
    if (obj_no_change)
	return list_old;       		/* All changes has been locked out */

    if (!list_old)
        return list_old;

    if (!pointerp(list_del))
	list_del = ({ list_del });

    return (list_old - (string *)list_del);

}

/*
 * Function name: query_list
 * Description:   Gives the return of a query on a list.
 * Arguments:     list: The list in question
 *                arg: If true then the entire list is returned.
 * Returns:       A string or an array as described above.
 */
private mixed
query_list(mixed list, int arg)
{
    if (!pointerp(list))
	return 0;

    if (!arg && sizeof(list))
	return list[0];
    else
	return list + ({});
}

/*
 * Nazwa funkcji  : query_rodzaj
 * Opis           : Zwraca rodzaj gramatyczny shorta (jesli jest
 *		    zdefiniowany) lub glownej nazwy obiektu.
 * Funkcja zwraca : int - patrz opis
 */
//TO VARARGS jest tu dlatego, �e w zi�kach query_rodzaj ma dodatkowy argument!
//Jak mo�ecie to nie usuwajcie, a jak ju� usuwacie to przynajmniej poinformujcie
//bo chwile musia�em dochodzi� co si� sta�o, �e zio�o nie dzia�a.(Krun) PS. Zmiany nie ma w logach albo ja �lepy jestem.
varargs public int
query_rodzaj()
{
    int rodzaj;

    if (obj_rodzaj_shorta)
        rodzaj = obj_rodzaj_shorta;
    else
        rodzaj = obj_rodzaje[0][0];

    if (rodzaj < 0)
        return (-rodzaj - 1);
    else
        return (rodzaj - 1);
}

public int
query_rodzaj_shorta()
{
    return obj_rodzaj_shorta;
}

/*
 * Nazwa funkcji : query_real_rodzaj
 * Opis          : Zwraca prawdziwy rodzaj pierwszej nazwy, lub wszystkich
 *		   nazw w podanym przypadku, w liczbie pojedynczej.
 *		   Prawdziwy, gdyz rodzaje sa zapamietywane w obiekcie w
 *		   specjalny sposob, rozniacy sie nieco od ogolnie znanego.
 *		   (wartosci PL_ZENSKI, itd). Przy zapamietywaniu rodzaju,
 *		   numer mu przyporzadkowany jest zwiekszany o 1, a jesli
 *		   dana nazwa nie posiada liczby pojedynczej, zmienia sie
 *		   mu jeszcze znak na ujemny.
 * Argumenty     : int wszystkie - czy zwrocic rodzaj wszystkich nazw w danym
 *				   przypadku, czy tylko pierwszej.
 *		   int przyp - przypadek, w ktorym rodzaje maja byc zwracane.
 * Funkcja zwraca: int - prawdziwy rodzaj lub rodzaje obiektu, liczbie
 *			 pojedynczej, w podanym przypadku.
 */
public mixed
query_real_rodzaj(int wszystkie, int przyp)
{
    return query_list(obj_rodzaje[przyp], wszystkie);
}

/*
 * Nazwa funkcji : query_real_prodzaj
 * Opis          : Zwraca prawdziwy rodzaj pierwszej nazwy, lub wszystkich
 *		   nazw w podanym przypadku, w liczbie mnogiej.
 *		   Prawdziwy, gdyz rodzaje sa zapamietywane w obiekcie w
 *		   specjalny sposob, rozniacy sie nieco od ogolnie znanego.
 *		   (wartosci PL_ZENSKI, itd). Przy zapamietywaniu rodzaju,
 *		   numer mu przyporzadkowany jest zwiekszany o 1, a jesli
 *		   dana nazwa nie posiada liczby pojedynczej, zmienia sie
 *		   mu jeszcze znak.
 * Argumenty     : int wszystkie - czy zwrocic rodzaj wszystkich nazw w danym
 *				   przypadku, czy tylko pierwszej.
 *		   int przyp - przypadek, w ktorym rodzaje maja byc zwracane.
 * Funkcja zwraca: int - prawdziwy rodzaj lub rodzaje obiektu, liczbie
 *			 mnogiej, w podanym przypadku.
 */
public varargs mixed
query_real_prodzaj(int arg, int przyp)
{
    return query_list(obj_prodzaje[przyp], arg);
}

/*
 * Nazwa funkcji : query_rodzaj_nazwy
 * Opis          : Funkcja zwraca jaki rodzaj ma podana nazwa, o ile
 *		   wogole zostala ona dodana w obiekcie. Mozna rowniez
 *		   wyszczegolnic, w jakiej liczbe szukac nazwy
 *		   (pojedynczej czy mnogiej), oraz w jakim przypadku.
 *		   Wartosc -1 podana w argumencie liczba albo przypadek,
 *		   spowoduje, ze przeszukiwane beda odpowiednio obie
 *		   liczby, albo wszystkie rodzaje.
 * Argumenty     : string nazwa  - nazwa, ktorej rodzaju szukamy.
 *		   int	  liczba - wyszczegolnienie, w jakiej liczbie
 *				   szukac danej nazwy.
 *					-1 - w obu (domyslnie)
 *					 0 - w pojedynczej
 *					 1 - w mnogiej.
 *		   int 	  przyp  - wyszczegolnienie, w jakim przypadku
 *				   szukac danej nazwy. -1 oznacza, ze we
 *				   wszystkich.
 * Funkcja zwraca: Rodzaj, albo -1, jesli obiekt nie posiadal podanej nazwy.
 */
public int
query_rodzaj_nazwy(string nazwa, int liczba = -1, int przyp = -1)
{
    int x, y;
    int rodzaj = 13; // Nierealna wartosc rodzaju.

    if (przyp == -1)
    {
        if (liczba != 1) // 0 lub -1, czyli pojedyncza
        {
            x = -1;
            while (++x < 6)
                if ((y = member_array(nazwa, obj_names[x])) != -1)
                {
                    rodzaj = obj_rodzaje[x][y];
                    break;
                }
        }

        if (liczba != 0) // 1 lub -1, czyli mnoga
        {
            x = -1;
            while (++x < 6)
                if ((y = member_array(nazwa, obj_pnames[x])) != -1)
                {
                    rodzaj = obj_prodzaje[x][y];
                    break;
                }
        }
    }
    else
    {
        if (liczba != 1) // 0 lub -1, czyli pojedyncza
            if ((y = member_array(nazwa, obj_names[przyp])) != -1)
               rodzaj = obj_rodzaje[przyp][y];

        if ((rodzaj == 13) && (liczba != 0)) // 1 lub -1, czyli mnoga
            if ((y = member_array(nazwa, obj_pnames[przyp])) != -1)
		rodzaj = obj_prodzaje[przyp][y];
    }

    if (rodzaj == 13)
        return -1;

    if (rodzaj < 0)
        return (-rodzaj - 1);
    else
        return (rodzaj - 1);
}


#ifndef CFUN
static int
usun_stare_nazwy(mixed nazwy, int przyp, int lmn, int czy_dodaj = 0)
{
    int c, d, *tmp2, size;
    string *tmp;

    if (!pointerp(nazwy))
        nazwy = ({ nazwy });

    if (!(c = sizeof(nazwy)))
        return 1;

    if (lmn)
        size = sizeof(obj_pnames[przyp]);
    else
        size = sizeof(obj_names[przyp]);

    if (czy_dodaj && (c == 1) && size)
    {
        if (lmn)
        {
            if (obj_pnames[przyp][0] == nazwy[0])
                return 0;
        }
        else
            if (obj_names[przyp][0] == nazwy[0])
                return 0;
    }

    size--;

    if (!lmn)
    {
        while (--c >= 0)
        {
            tmp = ({}); tmp2 = ({});
            d = size;
            while (d >= 0)
            {
                if (nazwy[c] == obj_names[przyp][d])
                {
                    // Usuwamy element d.
                    if (d) // nie jest na poczatku
                    {
                         tmp = obj_names[przyp][0..d-1];
                         tmp2 = obj_rodzaje[przyp][0..d-1];
                    }
                    if (d != size) // ani na koncu
                    {
                         tmp += obj_names[przyp][d+1..size];
                         tmp2 += obj_rodzaje[przyp][d+1..size];
                    }
                    size--;
                    obj_names[przyp] = tmp; obj_rodzaje[przyp] = tmp2;
                }
                d--;
            }
        }
    }
    else //lmn
    {
        while (--c >= 0)
        {
            tmp = ({}); tmp2 = ({});
            d = size;
            while (d >= 0)
            {
                if (nazwy[c] == obj_pnames[przyp][d])
                {
                    // usuwamy element d
                    if (d) // nie jest na poczatku
                    {
                         tmp = obj_pnames[przyp][0..d-1];
                         tmp2 = obj_prodzaje[przyp][0..d-1];
                    }
                    if (d != size) // ani na koncu
                    {
                         tmp += obj_pnames[przyp][d+1..size];
                         tmp2 += obj_prodzaje[przyp][d+1..size];
                    }
                    size--;
                    obj_pnames[przyp] = tmp; obj_prodzaje[przyp] = tmp2;
                }
                d--;
            }
        }
    }
    return 1;
}
#else
public int
usun_stare_nazwy(mixed nazwy, int przyp, int lmn, int czy_dodaj) = "usun_stare_nazwy";
#endif


public int
ustaw_nazwe_glowna(string str)
{
    mixed odm = slownik_pobierz(str);

    if(sizeof(odm) == 2)
        nazwa_glowna = ({ odm[0], odm[0], -odm[1] - 1 });
    else
        nazwa_glowna = ({ odm[0], odm[1], odm[2] });

    return 1;
}

public varargs string
query_nazwa_glowna(int przyp)
{
    if(nazwa_glowna)
        return nazwa_glowna[0][przyp];
}

public varargs string
query_pnazwa_glowna(int przyp)
{
    if(nazwa_glowna)
        return nazwa_glowna[1][przyp];
}

/*
 * Nazwa funkcji  : ustaw_nazwe
 * Opis           : Jest to pomocnicza funkcja, majaca zapobiec ewentualnym
 *                  bledom w czasie wpisywania odmiany nazwy przez przypadki.
 *                  Sluzy do definiowania odmiany glownej nazwy obiektu.
 *                  Podaje sie dwie tablice i warunkiem ich wpisania do nazw
 *                  jest prawidlowa ilosc elementow(6, bo jest 6 przypadkow).
 *		    Mozna tez, dla nazw nie posiadajacych lp, podac tylko
 *		    jedna tablice, z odmiana w lmn. Mudlib uwzgledni to we
 *		    wszystkich odmianach.
 * Argumenty      : string *nazwy - tablica zawierajaca odmiane nazwy obiektu
 *                                  przez przypadki w liczbie pojedynczej.
 *                  string *pnazwy - tablica zawierajaca odmiane nazwy
 *                                   obiektu przez przypadki w liczbie
 *                                   mnogiej.
 *		    int rodzaj - rodzaj gramatyczny, w jakim ta nazwa
 *				 jest.
 * Funkcja zwraca : 1, gdy nazwy zostaly wprowadzone do zmiennych, 0
 *                  w przeciwnym wypadku.
 */
public varargs int
ustaw_nazwe(mixed nazwy, mixed pnazwy,
            int rodzaj = this_object()->query_rodzaj())
{
    int c, d, size;
    string *tmp;
    int *tmp2;

    if(!intp(rodzaj))
        rodzaj = atoi(rodzaj);

    if (stringp(nazwy))
    {
        mixed odm = slownik_pobierz(nazwy);

        nazwy = odm[0];

        if(sizeof(odm) == 2)
        {
            pnazwy = nazwy;
            rodzaj = -odm[1] - 1;
        }
        else
        {
            pnazwy = odm[1];
            rodzaj = odm[2];
        }
    }

    if(sizeof(nazwy) != 6)
    {
        throw(sizeof(nazwy) + " 0 = " + nazwy[0] + ", 1 = " + nazwy[1] + "\n");
        throw("Z^la ilo^s^c przypadk^ow w odmianie liczby pojedynczej.\n");
        return 0;
    }

    if (pointerp(pnazwy))
    {
        if (sizeof(pnazwy) != 6)
        {
            throw("Z^la ilo^s^c przypadk^ow w odmianie liczby mnogiej.\n");
            return 0;
        }

        rodzaj += 1;
    }
    else
    {
        if (!intp(pnazwy))
            return 0;

        rodzaj = -(pnazwy + 1);
        pnazwy = nazwy;
    }

    for (c = 0; c < 6; c++)
    {
        usun_stare_nazwy(nazwy[c], c, 0, 0);
        obj_names[c] = ({ nazwy[c] }) + obj_names[c];

        obj_rodzaje[c] = ({ rodzaj }) + obj_rodzaje[c];

        usun_stare_nazwy(pnazwy[c], c, 1, 0);
        obj_pnames[c] = ({ pnazwy[c] }) + obj_pnames[c];
        obj_prodzaje[c] = ({ rodzaj }) + obj_prodzaje[c];
    }

    return 1;
}

/*
 * Nazwa funkcji  : dodaj_nazwy
 * Opis           : Jest to pomocnicza funkcja, majaca zapobiec ewentualnym
 *                  bledom w czasie wpisywania odmiany nazwy przez przypadki.
 *                  Sluzy do dodawania nazw, ktorymi bedziemy mogli
 *		    'zahaczyc' nasz obiekt (czyli np. wykonac na nim jakies
 *		    komendy). Podaje sie dwie tablice i warunkiem ich wpisania
 *		    do nazw jest prawidlowa ilosc elementow (6, bo jest
 *		    6 przypadkow). Mozna tez, dla nazw nie posiadajacych,
 *		    liczby pojedynczej, podac tylko jedna tablice, z odmiana
 *		    w liczbie mnogiej. Mudlib uwzgledni to we wszystkich
 *		    odmianach.
 * Argumenty      : string *nazwy - tablica zawierajaca odmiane nazwy obiektu
 *                                  przez przypadki w liczbie pojedynczej.
 *                  string *pnazwy - tablica zawierajaca odmiane nazwy
 *                                   obiektu przez przypadki w liczbie
 *                                   mnogiej.
 *		    int rodzaj - rodzaj gramatyczny, w jakim ta nazwa
 *				 jest.
 * Funkcja zwraca : 1, gdy nazwy zostaly wprowadzone do zmiennych, 0
 *                  w przeciwnym wypadku.
 */
public varargs int
dodaj_nazwy(mixed nazwy, mixed pnazwy,
    int rodzaj = this_object()->query_rodzaj())
{
    int c, d, size;
    string *tmp;
    int *tmp2;

    if (stringp(nazwy))
    {
        mixed odm = slownik_pobierz(nazwy);

        nazwy = odm[0];

        if(sizeof(odm) == 2)
        {
            pnazwy = nazwy;
            rodzaj = -odm[1] - 1;
        }
        else
        {
            pnazwy = odm[1];
            rodzaj = odm[2];
        }
    }

    if (sizeof(nazwy) != 6)
    {
        throw("Z^la ilo^s^c przypadk^ow w odmianie liczby pojedynczej.\n");
        return 0;
    }

    if (pointerp(pnazwy))
    {
        if (sizeof(pnazwy) != 6)
        {
            throw("Z^la ilo^s^c przypadk^ow w odmianie liczby mnogiej.\n");
            return 0;
        }

        rodzaj += 1;
    }
    else
    {
        if (!intp(pnazwy))
            return 0;

        rodzaj = -(pnazwy + 1);
        pnazwy = nazwy;
    }

    for (c = 0; c < 6; c++)
    {
        if (usun_stare_nazwy(nazwy[c], c, 0, 1))
        {
            obj_names[c] =  obj_names[c] + ({ nazwy[c] });
            obj_rodzaje[c] = obj_rodzaje[c] + ({ rodzaj });
        }

        if (usun_stare_nazwy(pnazwy[c], c, 1, 1))
        {
            obj_pnames[c] += ({ pnazwy[c] });
            obj_prodzaje[c] += ({ rodzaj });
        }
    }


    return 1;
}

varargs public int
dodaj_przym(string lp_mian, string lmn_mian = "", int first = 0)
{
    mixed tmp;

    if(!lp_mian)
        return 0;

    if(!lmn_mian || lmn_mian == "")
        lmn_mian = PRZYM_OBLICZ_MNOGA(lp_mian);

    obj_przym[0] = add_list(obj_przym[0], lp_mian, first);
    obj_przym[1] = add_list(obj_przym[1], lmn_mian, first);

    return 1;
}

/*
 * Nazwa funkcji : query_przym
 * Opis          : Zwraca przymiotnik(i) obiektu w liczbie pojedynczej.
 * Argumenty     : int arg   - jesli prawdziwy, zostana zwrocone
 *			       wszystkie przymiotniki danego przypadku.
 *		   int przyp - numer przypadku.
 *		   int rodzaj- opcjonalnie, wymuszenie zmiany rodzaju.
 *			       nowy rodzaj powinien byc w formie, w jakiej
 *			       rodzaje sa zapamietywane w mudlibie.
 *			       (patrz: naglowek funkcji query_rodzaj)
 * Funkcja zwraca: mixed     - int 0    - nie ma zadnych przymiotnikow.
 *			       string   - jeden przymiotnik, jesli arg
 *					  jest falszywy (rowny 0).
 *			       string * - tablica z wszystkimi
 *					  przymiotnikami w danym przypadku.
 */
public varargs mixed
query_przym(int arg, int przyp, int rodzaj = this_object()->query_real_rodzaj())
{
    int ix, size, liczba;
    string *wynik = ({});

    if (!sizeof(obj_przym[0]))
        return ({ });

    if (rodzaj < 0)
    {
        liczba = 1;
        rodzaj = -rodzaj;
    }
    else liczba = 0;

    rodzaj -= 1;

    if (arg)
    {
        size = sizeof(obj_przym[0]);
        ix = -1;
        while(++ix < size)
        {
            wynik = wynik + ({ oblicz_przym(obj_przym[0][ix],
                obj_przym[1][ix], przyp, rodzaj, liczba) });
        }

        return wynik;
    }

    return oblicz_przym(obj_przym[0][0], obj_przym[1][0], przyp,
        rodzaj, liczba);
}

/*
 * Nazwa funkcji : query_pprzym
 * Opis          : Zwraca przymiotnik(i) obiektu w liczbie mnogiej.
 * Argumenty     : int arg   - jesli prawdziwy, zostana zwrocone
 *			       wszystkie przymiotniki danego przypadku.
 *		   int przyp - numer przypadku.
 *		   int rodzaj- opcjonalnie, wymuszenie zmiany rodzaju.
 *			       nowy rodzaj powinien byc w formie, w jakiej
 *			       rodzaje sa zapamietywane w mudlibie.
 *			       (patrz: naglowek funkcji query_rodzaj)
 * Funkcja zwraca: mixed     - int 0    - nie ma zadnych przymiotnikow.
 *			       string   - jeden przymiotnik, jesli arg
 *					  jest falszywy (rowny 0).
 *			       string * - tablica z wszystkimi
 *					  przymiotnikami w danym przypadku.
 */
public varargs mixed
query_pprzym(int arg, int przyp, int rodzaj = this_object()->query_real_rodzaj())
{
    int ix, size;
    string *wynik = ({});

    if (!sizeof(obj_przym[0]))
        return ({});

    if (rodzaj < 0)
        rodzaj = -rodzaj;

    rodzaj -= 1;

    if (arg)
    {
        size = sizeof(obj_przym[0]);
	ix = -1;
        while(++ix < size)
        {
            wynik = wynik + ({ oblicz_przym(obj_przym[0][ix], obj_przym[1][ix],
                przyp, rodzaj, 1) });
        }
        return wynik;
    }

    return oblicz_przym(obj_przym[0][0], obj_przym[1][0], przyp, rodzaj, 1);
}

/*
 * Nazwa funkcji  : ustaw_shorty
 * Opis           : Ustawia pelna odmiane krotkich opisow obiektu przez
 *                  przypadki, zarowno w liczbie pojedynczej jak i mnogiej.
 *                  Obie tablice z shortami musza zawierac po 6 elementow.
 * Argumenty      : string  *shorty - tablica z odmiana krotkich opisow
 *                                    obiektu przez przypadki w liczbie p.
 *                  string *pshorty - tablica z odmiana shortow obiektu
 *                                    przez przypadki w liczbie mnogiej.
 *		    int rodzaj - rodzaj gramatyczny, w jakim ta nazwa
 *				 jest.
 * Funkcja zwraca : 1, gdy podana zostala wlasciwa ilosc elementow i
 *                  shorty zostaly ustawione. 0 w przeciwnym razie.
 */
public varargs int
ustaw_shorty(mixed shorty, mixed pshorty,
    int rodzaj = this_object()->query_rodzaj())
{
    if (stringp(shorty))
    {
        mixed odm = slownik_pobierz(shorty);

        shorty = odm[0];

        if(sizeof(odm) == 2)
        {
            pshorty = shorty;
            rodzaj  = -odm[1] - 1;
        }
        else
        {
            pshorty = odm[1];
            rodzaj = odm[2];
        }
    }

    if (sizeof(shorty) != 6)
    {
        throw("Z^la ilo^s^c przypadkow w odmianie short^ow liczby pojedynczej.\n");
        return 0;
    }

    rodzaj += 1;

    if (pointerp(pshorty))
    {
        if (sizeof(pshorty) != 6)
        {
            throw("Z^la ilo^s^c przypadk^ow w odmianie liczby mnogiej.\n");
            return 0;
        }

    }
    else
    {
        if (!intp(pshorty))
            return 0;

        rodzaj = -pshorty;
        pshorty = shorty;
    }

    obj_shorts = shorty + ({});

    // mozna nie dodawac, gdy jest rodzaj tylko_mnoga.
    // nie wiem po co dodaje. plural_short() moze sie zwracac do short(),
    // gdy napotka na ujemny rodzaj.
    obj_pshorts = pshorty + ({});

    obj_rodzaj_shorta = rodzaj;

    return 1;
}

/*
 * Nazwa funkcji : usun_shorty
 * Opis          : Usuwa cala obecna odmiane shortow obiektu.
 */
public void
usun_shorty()
{
    if (obj_no_change)
        return 0;

    obj_shorts = 0;
    obj_pshorts = 0;

    obj_rodzaj_shorta = 0;
}

/*
 * Nazwa funkcji : set_name
 * Opis          : Ustawia na poczatku tablicy nazw jedna lub wiecej nazwe w
 *		   podanym przypadku, w liczbie pojedynczej. (nazwa glowna).
 *		   Mozna rowniez podac w jakim rodzaju gramatycznym bedzie
 *		   kazda z nazw. W przypadku, gdy sie poda za malo
 *		   rodzajow (albo nie poda sie ich wcale), funkcja sama
 *		   dopelni tablice rodzajow tak, by kazda dodawana nazwa miala
 *		   swoj rodzaj. Jesli zajdzie potrzeba dopelnienia, a bedzie
 *		   podany chociaz jeden rodzaj, funkcja dopelni pierwszym
 *		   podanym rodzajem. Gdy sie nie poda zadnego rodzaju,
 *		   zostanie uzyty domyslny rodzaj - zenski. Rodzaje powinny
 *		   miec specjalna, przetworzona wartosc - patrz naglowek
 *		   funkcji query_rodzaj().
 * Argumenty     : name - nazwa, lub tablica nazw do dodania, w lp.
 *		   przyp - przypadek
 *		   rodzaje - rodzaj, lub tablica z prawdziwymi rodzajami
 *			     (w formie, w jakiej mudlib je zapamietuje).
 */
public varargs void
set_name(mixed name, int przyp = 0, mixed rodzaje = ({}))
{
    int x, wypelniany_rodzaj, size_rodzaje;

    if (!pointerp(name))
        name = ({ name });

    if (!pointerp(rodzaje))
       rodzaje = ({ rodzaje });

    size_rodzaje = sizeof(rodzaje);
    if (size_rodzaje)
        wypelniany_rodzaj = rodzaje[0];
    else
        wypelniany_rodzaj = PL_ZENSKI + 1;

    if ((x = (sizeof(name) - size_rodzaje)) > 0)
        while (x--)
            rodzaje += ({ wypelniany_rodzaj });

    usun_stare_nazwy(name, przyp, 0, 0);
    obj_names[przyp] = name + obj_names[przyp];
    obj_rodzaje[przyp] = rodzaje + obj_rodzaje[przyp];
}

/*
 * Nazwa funkcji : add_name
 * Opis          : Dodaje na koniec tablicy nazw jedna lub wiecej nazwe w
 *		   podanym przypadku, w liczbie pojedynczej.
 *		   Mozna rowniez podac w jakim rodzaju gramatycznym
 *		   bedzie kazda z nazw. W przypadku, gdy sie poda za malo
 *		   rodzajow (albo nie poda sie ich wcale), funkcja sama
 *		   dopelni tablice rodzajow tak, by kazda dodawana nazwa
 *		   miala swoj rodzaj. Jesli zajdzie potrzeba
 *		   dopelnienia, a bedzie podany chociaz jeden rodzaj,
 *		   funkcja dopelni pierwszym podanym rodzajem. Gdy sie nie
 *		   poda zadnego rodzaju, zostanie uzyty domyslny rodzaj -
 *		   zenski. Rodzaje powinny miec specjalna, przetworzona
 *		   wartosc - patrz naglowek funkcji query_rodzaj().
 * Argumenty     : name - nazwa, lub tablica nazw do dodania, w lp.
 *		   przyp - przypadek
 *		   rodzaje - rodzaj, lub tablica z prawdziwymi rodzajami
 *			     (w formie, w jakiej mudlib je zapamietuje).
 */
public varargs void
add_name(mixed name, int przyp = 0, mixed rodzaje = ({}))
{
    int x, wypelniany_rodzaj, size_rodzaje;

    if (!pointerp(name))
        name = ({ name });

    if (!pointerp(rodzaje))
       rodzaje = ({ rodzaje });

    size_rodzaje = sizeof(rodzaje);
    if (size_rodzaje)
        wypelniany_rodzaj = rodzaje[0];
    else
        wypelniany_rodzaj = PL_ZENSKI + 1;

    if ((x = (sizeof(name) - size_rodzaje)) > 0)
        while (x--)
            rodzaje += ({ wypelniany_rodzaj });

    if (usun_stare_nazwy(name, przyp, 0, 1))
    {
        obj_names[przyp] += name;
        obj_rodzaje[przyp] += rodzaje;
    }
}

public varargs void remove_pname(mixed pname, int przyp);

/**
 * Dzi�ki tej funckcji mo�emy usun�� z tablicy nazw� lub nazwy obiektu.
 *
 * @param nazwa nazwa kt�r� chcemy usun��
 * @param przyp przypadek w kt�rym chcemy usun�� nazw�(-1 je�li chcemy usun�� wszystkie, mianownik mnogiej je�li usuwamy
 *              tak�e liczbe mnog�..).
 */
public varargs void
remove_name(mixed name, mixed przyp)
{
    if(stringp(przyp))
    {
        remove_pname(przyp, -1);
        przyp = -1;
    }

    if(!intp(przyp))
        return;

    if(przyp == -1)
    {
        int i;

        if((i = member_array(name, obj_names[PL_MIA])) == -1)
            return;

        for(int p = 0;p<sizeof(obj_names); p++)
            usun_stare_nazwy(name, p, 0, 0);
    }
    else
        usun_stare_nazwy(name, przyp, 0, 0);
}

/*
 * Nazwa funkcji : query_name
 * Opis          : Zwraca pierwsza albo wszystkie nazwy z danego przypadku,
 *		   w liczbie pojedynczej.
 * Argumenty     : int arg - czy ma zwrocic tylko nazwe glowna, czy wszystki
 *			     w danym przypadku.
 *		   int przyp - przypadek, w ktorym maja byc nazwy
 * Funkcja zwraca: Nazwe lub tablice nazw w zadanym przypadku.
 */
varargs public mixed
query_name(int arg, int przyp)
{
    return query_list(obj_names[przyp], arg);
}

/*
 * Nazwa funkcji  : query_nazwa
 * Opis           : Zwraca nazwe obiektu w okreslonym przypadku, o ile obiekt
 *                  zostal odmieniony za pomoca dodaj_nazwy() lub
 *		    ustaw_nazwe(). W przeciwnym wypadku zwraca pierwsza
 *		    nazwe z mianownika.
 * Argumenty      : int przyp - przypadek.
 * Funkcja zwraca : string - nazwa obiektu.
 */
varargs public string
query_nazwa(int przyp)
{
    return obj_names[przyp][0];
}

/**
 * @return wszystkie nazwy obiektu.
 */
public mixed
query_nazwy()
{
    return obj_names;
}

/*
 * Nazwa funkcji : set_pname
 * Opis          : Ustawia na poczatku tablicy nazw jedna lub wiecej nazwe w
 *		   podanym przypadku, w liczbie mnogiej. (nazwa glowna).
 *		   Mozna rowniez podac w jakim rodzaju gramatycznym bedzie
 *		   kazda z nazw. W przypadku, gdy sie poda za malo
 *		   rodzajow (albo nie poda sie ich wcale), funkcja sama
 *		   dopelni tablice rodzajow tak, by kazda dodawana nazwa miala
 *		   swoj rodzaj. Jesli zajdzie potrzeba dopelnienia, a bedzie
 *		   podany chociaz jeden rodzaj, funkcja dopelni pierwszym
 *		   podanym rodzajem. Gdy sie nie poda zadnego rodzaju,
 *		   zostanie uzyty domyslny rodzaj - zenski. Rodzaje powinny
 *		   miec specjalna, przetworzona wartosc - patrz naglowek
 *		   funkcji query_rodzaj().
 * Argumenty     : name - nazwa, lub tablica nazw do dodania, w lmn.
 *		   przyp - przypadek
 *		   rodzaje - rodzaj, lub tablica z prawdziwymi rodzajami
 *			     (w formie, w jakiej mudlib je zapamietuje).
 */
public varargs void
set_pname(mixed pname, int przyp, mixed rodzaje = ({}))
{
    int x, wypelniany_rodzaj, size_rodzaje;

    if (!pointerp(pname))
        pname = ({ pname });

    if (!pointerp(rodzaje))
       rodzaje = ({ rodzaje });

    size_rodzaje = sizeof(rodzaje);
    if (size_rodzaje)
        wypelniany_rodzaj = rodzaje[0];
    else
        wypelniany_rodzaj = PL_ZENSKI + 1;

    if ((x = (sizeof(pname) - size_rodzaje)) > 0)
        while (x--)
            rodzaje += ({ wypelniany_rodzaj });

    usun_stare_nazwy(pname, przyp, 1, 0);
    obj_pnames[przyp] = pname + obj_pnames[przyp];
    obj_prodzaje[przyp] = rodzaje + obj_prodzaje[przyp];
}

/*
 * Nazwa funkcji : add_pname
 * Opis          : Dodaje na koniec tablicy nazw jedna lub wiecej nazwe w
 *		   podanym przypadku, w liczbie mnogiej.
 *		   Mozna rowniez podac w jakim rodzaju gramatycznym
 *		   bedzie kazda z nazw. W przypadku, gdy sie poda za malo
 *		   rodzajow (albo nie poda sie ich wcale), funkcja sama
 *		   dopelni tablice rodzajow tak, by kazda dodawana nazwa
 *		   miala swoj rodzaj. Jesli zajdzie potrzeba
 *		   dopelnienia, a bedzie podany chociaz jeden rodzaj,
 *		   funkcja dopelni pierwszym podanym rodzajem. Gdy sie nie
 *		   poda zadnego rodzaju, zostanie uzyty domyslny rodzaj -
 *		   zenski. Rodzaje powinny miec specjalna, przetworzona
 *		   wartosc - patrz naglowek funkcji query_rodzaj().
 * Argumenty     : name - nazwa, lub tablica nazw do dodania, w lmn.
 *		   przyp - przypadek
 *		   rodzaje - rodzaj, lub tablica z prawdziwymi rodzajami
 *			     (w formie, w jakiej mudlib je zapamietuje).
 */
public varargs void
add_pname(mixed pname, int przyp, mixed rodzaje = ({}))
{
    int x, wypelniany_rodzaj, size_rodzaje;

    if (!pointerp(pname))
        pname = ({ pname });

    if (!pointerp(rodzaje))
       rodzaje = ({ rodzaje });

    size_rodzaje = sizeof(rodzaje);
    if (size_rodzaje)
        wypelniany_rodzaj = rodzaje[0];
    else
        wypelniany_rodzaj = PL_ZENSKI + 1;

    if ((x = (sizeof(pname) - size_rodzaje)) > 0)
        while (x--)
            rodzaje += ({ wypelniany_rodzaj });

    if (usun_stare_nazwy(pname, przyp, 1, 1))
    {
	obj_pnames[przyp] += pname;
	obj_prodzaje[przyp] += rodzaje;
    }
}

/**
 * Dzi�ki tej funkcji mo�emy usun�� nazw� w liczbie mnogiej.
 * @param pname nazwa w liczbie mnogiej
 * @param przyp przypadek w jakim usuwamy nazw�(-1 we wszystkich przypadkach).
 */
public varargs void
remove_pname(mixed pname, int przyp)
{
    if(przyp == -1)
    {
        int i;

        if((i = member_array(pname, obj_pnames[PL_MIA])) == -1)
            return;

        for(int p = 0;p<sizeof(obj_pnames); p++)
            usun_stare_nazwy(pname, p, 1, 0);
    }
    else
        usun_stare_nazwy(pname, przyp, 1, 0);
}

/*
 * Nazwa funkcji  : query_pname
 * Opis           : W zaleznosci od argumentow, zwraca liste wszystkich
 *                  nazw w lmn w podanym przypadku, albo tylko glowna.
 * Argumenty      : int arg - 1, gdy ma zwrocic liste wszystkich nazw
 *			      w danym przypadku; 0 gdy tylko glowna.
 *                  int przyp - dany przypadek.
 * Funkcja zwraca : mixed - tablice z nazwami w podanym przypadku, albo
 *                          tylko nazwe glowna.
 */
varargs public mixed
query_pname(int arg, int przyp)
{
    return query_list(obj_pnames[przyp], arg);
}

/*
 * Nazwa funkcji  : query_pnazwa
 * Opis           : Zwraca nazwe obiektu w liczbie mnogiej, w okreslonym
 * 		    przypadku, o ile obiekt zostal odmieniony za pomoca
 *		    dodaj_nazwy() lub ustaw_nazwe(). W przeciwnym wypadku
 *		    zwraca pierwsza nazwe z tablicy mianownika.
 * Argumenty      : int przyp - przypadek.
 * Funkcja zwraca : string - nazwa obiektu w liczbie mnogiej.
 */
varargs public string
query_pnazwa(int przyp)
{
    return obj_pnames[przyp][0];
}

/**
 * @return wszystkie nazwy obiektu w liczbie mnoegiej.
 */
public mixed
query_pnazwy()
{
    return obj_pnames;
}

#if 0
/*
 * Nazwa funkcji : set_adj
 * Opis          : Funkcja NIE istnieje. Uzyj dodaj_przym().
 */
public varargs void
set_adj(mixed adj, int przyp)
{
    obj_adjs[przyp] = add_list(obj_adjs[przyp], adj, 1);
}
#endif

#if 0
/*
 * Nazwa funkcji : add_adj
 * Opis          : Funkcja NIE istnieje. Uzyj dodaj_przym().
 */
public varargs void
add_adj(mixed adj, int przyp)
{
    obj_adjs[przyp] = add_list(obj_adjs[przyp], adj, 0);
}
#endif

/*
 * Nazwa funkcji : remove_adj
 * Opis          : Usuwa podane przymiotniki z tablicy z przymiotnikami
 * Argumenty     : mixed adj - string albo tablica z przymiotnikami
 *			       do usuniecia. Powinny one byc podane
 *			       w rodzaju meskim w liczbie pojedynczej.
 */
public void
remove_adj(mixed adj)
{
    int i, pos;

    if (obj_no_change)
	return ;       		/* All changes has been locked out */

    if (!adj)
        return ;

    if (!pointerp(adj))
        adj = ({ adj });

    i = sizeof(adj);
    while (--i >= 0)
    {
        pos = member_array(adj[i], obj_przym[0]);
        if (pos >= 0)
        {
            obj_przym[0] = exclude_array(obj_przym[0], pos, pos);
            obj_przym[1] = exclude_array(obj_przym[1], pos, pos);
        }
    }
}

/*
 * Nazwa funkcji : query_adj
 * Opis          : Zwraca przymiotnik(i) obiektu w liczbie pojedynczej.
 * Argumenty     : int arg   - jesli prawdziwy, zostana zwrocone
 *			       wszystkie przymiotniki danego przypadku.
 *		   int przyp - numer przypadku.
 * Funkcja zwraca: mixed     - int 0    - nie ma zadnych przymiotnikow.
 *			       string   - jeden przymiotnik, jesli arg
 *					  jest falszywy (rowny 0).
 *			       string * - tablica z wszystkimi
 *					  przymiotnikami w danym przypadku.
 */
varargs public mixed
query_adj(int arg, int przyp)
{
     return query_przym(arg, przyp);
}

/*
 * Do uzytku przez /std/player/savevars_sec.c i /std/heap.c
 */
public varargs mixed
query_przymiotniki()
{
    return obj_przym + ({ });
}

/*
 * Nazwa funkcji  : set_padj
 * Opis          : Funkcja istnieje tylko dla kompatybilnosci. Nie uzywac.
 */
public void
set_padj(mixed elem)
{
#if 0
    add_list(obj_padjs, elem, 1);
#endif
}

/*
 * Nazwa funkcji  : add_padj
 * Opis          : Funkcja istnieje tylko dla kompatybilnosci. Nie uzywac.
 */
public varargs void
add_padj(mixed elem, int przyp)
{
#if 0
    obj_padjs[przyp] = add_list(obj_padjs[przyp], elem, 0);
#endif
}

/*
 * Nazwa funkcji : query_padj
 * Opis          : Zwraca przymiotnik(i) obiektu w liczbie mnogiej.
 * Argumenty     : int arg   - jesli prawdziwy, zostana zwrocone
 *			       wszystkie przymiotniki danego przypadku.
 *		   int przyp - numer przypadku.
 * Funkcja zwraca: mixed     - int 0    - nie ma zadnych przymiotnikow.
 *			       string   - jeden przymiotnik, jesli arg
 *					  jest falszywy (rowny 0).
 *			       string * - tablica z wszystkimi
 *					  przymiotnikami w danym przypadku.
 */
public varargs mixed
query_padj(int arg, int przyp)
{
    return query_pprzym(arg, przyp);
}

#if 0
/*
 * Function name: set_short
 * Description:   Sets the string to return for short description.
 *                If not defined, the first name is used instead.
 * Arguments:     short: The short description
 * *UWAGA* Ta funkcja ZOSTALA USUNIETA. W zamian uzywaj
 *         funkcji ustaw_shorty().
 */
public void
set_short(string short)
{
    if (!obj_no_change)
	obj_short = short;
}
#endif

#if 0
/*
 * Function name: set_pshort
 * Description:   Sets the string to return for plural short description.
 *                If not defined, 0 is returned.
 * Arguments:     pshort: The plural short description
 * *UWAGA* Ta funkcja ZOSTALA USUNIETA. Wzamian uzywaj
 *         funkcji ustaw_shorty().
 */
public void
set_pshort(string pshort)
{
    if (!obj_no_change)
	obj_pshort = pshort;
}
#endif

/*
 * Function name: set_long
 * Description  : This function sets the long description of this object.
 *                It can be a string or VBFC in string or functionpointer
 *                form.
 * Arguments    : mixed long - the long description.
 */
public void
set_long(mixed long)
{
    if (!obj_no_change)
        obj_long = long;
}

/*
 * Function name: set_lock
 * Description:   Locks out all changes to this object through set_ functions.
 */
public void
set_lock()
{
    obj_no_change = 1;
}

/*
 * Function name: query_lock
 * Description:   Gives the lock status of this object
 * Returns:       True if changes to this object is locked out
 */
public int
query_lock()
{
    return obj_no_change;
}

/*
 * Function name: set_no_show
 * Description:   Don't show these objects.
 */
public void
set_no_show()
{
    obj_no_show = 1;
    set_no_show_composite(1);
}

/*
 * Function name: unset_no_show
 * Description:   Show it again. Note that if you want the object to appear
 *                in composite descriptions you have to set_no_show_composite(0)
 *                to since it is automatically set when setting no_show.
 */
public void
unset_no_show()
{
    obj_no_show = 0;
}

/*
 * Function name: query_no_show
 * Description:   Return no show status.
 */
public int
query_no_show()
{
    return obj_no_show;
}

/*
 * Function name: set_no_show_composite
 * Description:   Don't show this object in composite descriptions, otherwise
 *                this object is part of the game like any other.
 * Arguments:     1 - don't show, 0 - show it again
 */
void
set_no_show_composite(int i)
{
    obj_no_show_c = i;
}

/*
 * Function name: unset_no_show_composite
 * Description:   Show an object in compisite descriptions again.
 */
void
unset_no_show_composite()
{
    obj_no_show_c = 0;
}

/*
 * Function name: query_no_show_composite
 * Description:   Return status if to be shown in composite descriptions
 */
int
query_no_show_composite()
{
    return obj_no_show_c;
}

/*
 * Function name:  add_magic_effect
 * Description:    Notifies this object that it has been placed
 *                 a magical effect upon it.
 * Arguments:      The effect object, or the filename of
 *                 the code which handles the magical
 *                 effect. (Filename for a shadow.)
 */
varargs void
add_magic_effect(mixed what)
{
    if (!what)
	what = previous_object();

    if (!pointerp(magic_effects))
	magic_effects = ({ what });
    else
	magic_effects += ({ what });
}

/*
 * Function name:  remove_magic_effect
 * Description:    Removes the magical effect from the
 *                 list of effects affecting this object.
 * Arguments:      What effect.
 * Returns:        If the effect was found.
 */
varargs int
remove_magic_effect(mixed what)
{
    int il;

    if (!what)
	what = previous_object();

    il = member_array(what, magic_effects);

    if (il == -1)
	return 0;

    magic_effects = exclude_array(magic_effects, il, il);
    return 1;
}

/*
 * Function name:  query_magic_effects
 * Description:    Returns the magical effects upon this
 *                 object.
 */
object *
query_magic_effects()
{
    if (!pointerp(magic_effects))
	magic_effects = ({});
    magic_effects -= ({ 0 });
    return magic_effects;
}

#if 0
/*
 * Function name: query_magic_res
 * Description:   Return the total resistance of worn objects
 * Arguments:     prop - The searched for property.
 * Returns:       How resistant this object is to that property
 */
public int
query_magic_res(string prop)
{
    int res;

    res = this_object()->query_prop(PRE_OBJ_MAGIC_RES + prop);
    return res > 100 ? 100 : res;
}
#endif

/*
 * Function name: query_magic_res
 * Description:   Return the total resistance for this object
 * Arguments:     prop - The searched for property.
 */
int
query_magic_res(string prop)
{
    int no_objs, max, max_add, max_stat, i;
    mixed value;
    object *list;

    list = this_object()->query_magic_effects();

    if (!sizeof(list))
	return (int)this_object()->query_prop(PRE_OBJ_MAGIC_RES + prop);

    max_add = 100;

    for (i = 0; i < sizeof(list); i++)
    {
	value = list[i]->query_magic_protection(prop, this_object());
	if (intp(value))
	    value = ({ value, 0 });
	if (pointerp(value))
	{
	    if ((sizeof(value) > 0) && !value[1])
		max_stat = max_stat > value[0] ? max_stat : value[0];
	    else
		max_add = max_add * (100 - value[0]) / 100;
	}
    }

    if (max_add > 0)
	max_add = 100 - max_add;

    max = max_stat > max_add ? max_stat : max_add;
    max += (int)this_object()->query_prop(PRE_OBJ_MAGIC_RES + prop);

    return max < 100 ? max : 100;
}

/*
 * Function name:  query_magic_protection
 * Description:    This function should return the
 *                 amount of protection versus an
 *                 attack of 'prop' on 'obj'.
 * Arguments:      prop - The element property to defend.
 *                 protectee  - Magic protection for who or what?
 */
varargs mixed
query_magic_protection(string prop, object protectee = previous_object())
{
    if (protectee == this_object())
	return query_prop(prop);
    else
	return 0;
}

/*

   Items (pseudo look)
   -------------------

*/


/*
 * Function name: item_id
 * Description  : Identify items in the object. This means that the function
 *                will return true if the argument has been added to this
 *                object using add_item().
 * Arguments    : string str - the name to test.
 * Returns      : int 1/0 - is added with add_item() or not.
 */
public varargs int
item_id(string str, int przyp = -1)
{
  int size;

  if (!obj_items)
  {
    return 0;
  }

  size = sizeof(obj_items);
  while(--size >= 0)
  {
    if (member_array(str, obj_items[size][0]) >= 0)
    {
      if ((przyp == -1) || (przyp == obj_items[size][2])) {
        return 1;
      }
    }
  }

  return 0;
}

/**
 * Zwraca nazw� obiektu, jaka znajduj� si� w tablicy.
 *
 * @param str nazwa obiektu, kt�r� sprawdzamy
 *
 * @return nazwa obiektu
 */
public varargs string
item_name(string str, int przyp = -1)
{
  int size;
  int k;

  if (!obj_items)
  {
    return 0;
  }

  size = sizeof(obj_items);
  while(--size >= 0)
  {
    if ((k = member_array(str, obj_items[size][0])) >= 0)
    {
      if ((przyp == -1) || (przyp == obj_items[size][2])) {
        return obj_items[size][0][k];
      }
    }
  }

  return 0;
}

/*
 * Function name: add_item
 * Description:   Adds an additional item to the object. The first
 *		  argument is a single string or an array of
 *      	  strings holding the possible name(s) of the item.
 *		  The second argument is the long description of
 *		  the item. add_item can be repeatedly called with
 *                new items. The second argument can be VBFC.
 * Arguments:	  names: Alternate names for the item,
 *                mixed desc: desc of the item (string or VBFC)
 * Returns:	  True or false.
 */
public varargs int
add_item(mixed names, mixed desc, int przyp = PL_DOP)
{
    if (query_prop(ROOM_I_NO_EXTRA_ITEM))
	return 0;

    if (!pointerp(names))
	names = ({ names });

    if (obj_items)
	obj_items = obj_items + ({ ({ names, desc, przyp }) });
    else
	obj_items = ({ ({ names, desc, przyp }) });
}

/*
 * Function name: set_add_item
 * Description:   Sets the 'pseudo items' of an object.
 * Arguments:     pseudo_items - a mixed array of pseudo items to be added.
 */
public void
set_add_item(mixed pseudo_items)
{
    obj_items = pseudo_items;
}

/*
 * Function name: query_item
 * Description:   Get the additional items array.
 * Returns:       Item array, see below:

  [0] = array
     [0] ({ "name1 of item1", "name2 of item1",... })
     [1] "This is the description of the item1."
     [2] Przypadek
  [1] = array
     [0] ({ "name1 of item2", "name2 of item2", ... })
     [1] "This is the description of the item2."
     [2] Przypadek
*/
public mixed
query_item()
{
    return obj_items;
}

/*
 * Function name: remove_item
 * Description:   Removes one additional item from the additional item list
 * Arguments:     name: name of item to remove.
 * Returns:       True or false. (True if removed successfully)
 */
public int
remove_item(string name)
{
    int i;

    if (!pointerp(obj_items))
	return 0;

    if (query_prop(ROOM_I_NO_EXTRA_ITEM))
	return 0;
    for (i = 0; i < sizeof(obj_items); i++)
	if (member_array(name, obj_items[i][0]) >= 0 )
	{
	    obj_items = exclude_array(obj_items, i, i);
	    return 1;
	}
    return 0;
}

/*
 * Function name: cmditem_action
 * Description:   Find and execute a command for a specific command item
 * Arguments:     str: The rest of the command
 */
public int
cmditem_action(string str)
{
    string verb = query_verb();
    int n = sizeof(obj_cmd_items);
    int cmd, arg;
    mixed ex;
    string fail;

    fail = capitalize(verb) + " co?\n";

//    if (!str)
//        return 0;

    while (n--)
        if ((cmd = member_array(verb, obj_cmd_items[n][1])) != -1 &&
	    ((obj_cmd_items[n][3] == "") ? : (fail = obj_cmd_items[n][3])) &&
            (arg = member_array(str, obj_cmd_items[n][0])) != -1 &&
            (ex = check_call(obj_cmd_items[n][2][cmd <
                      sizeof(obj_cmd_items[n][2]) ? cmd : 0])))
        {
            if (stringp(ex))
                write(ex);

            return 1;
        }

    notify_fail(fail);
    return 0;
}

/*
 * Function name: add_cmd_item
 * Description:   Adds a specific item with associated commands to the
 *                object. These are similar to the normal items but
 *                they add commands which can then executed by players.
 *                The first argument is a single string or an array of
 *                strings holding the possible name(s) of the item.
 *                The second argument is the command / description array of
 *                the item. add_cmd_item can be repeatedly called with
 *                new items.
 * Arguments:     names: Alternate names for the item,
 *                cmd_arr:  Commands to give to get the desc
 *                desc_arr: descs of the item for each command
 * Returns:       True or false.
*/
public int
add_cmd_item(mixed names, string *cmd_arr, mixed desc_arr, string fail = "")
{
    if (query_prop(ROOM_I_NO_EXTRA_ITEM))
	return 0;

    if (!pointerp(names))
	names = ({ names });

    if (!pointerp(cmd_arr))
	cmd_arr = ({ cmd_arr });

    if (!pointerp(desc_arr))
	desc_arr = ({ desc_arr });

    if (!pointerp(obj_commands))
	obj_commands = ({ });

    if (obj_cmd_items)
	obj_cmd_items = obj_cmd_items + ({ ({ names, cmd_arr, desc_arr, fail }) });
    else
	obj_cmd_items = ({ ({ names, cmd_arr, desc_arr, fail }) });

    if (sizeof(cmd_arr))
	obj_commands = obj_commands + (cmd_arr - obj_commands);
}

/*
 * Function name: query_cmd_item
 * Description:   Get the command items array.
 * Returns:       Item array, see below:

  [0] = array
     [0] ({ "name1 of item1", "name2 of item1",... })
     [1] ({ "command1", "command2", .... "commandN" })
     [2] ({
	    "string to print if command1 given",
	    "string to print if command2 given",
		   ......
	    "string to print if commandN given",
	 })

  Example:
	({
	    ({ "flower", "viola" }),
	    ({ "smell", "taste", "get" }),
	    ({ "It smells nice", "It tastes awful!", "@@tryget" }),
	})

*/
public mixed
query_cmd_item()
{
    return obj_cmd_items;
}

/*
 * Function name: remove_cmd_item
 * Description:   Removes one command item from the command item list
 * Arguments:     name: name of item to remove.
 * Returns:       True or false. (True if removed successfully)
 */
public int
remove_cmd_item(string name)
{
    int i, il;
    string *cmd_arr;

    if ( !pointerp(obj_cmd_items) ) return 0;

    if (query_prop(ROOM_I_NO_EXTRA_ITEM)) return 0;
    for ( i = 0; i<sizeof(obj_cmd_items); i++)
	if ( member_array(name, obj_cmd_items[i][0])>=0 )
	{
	    obj_cmd_items = exclude_array(obj_cmd_items,i,i);
	    obj_commands = ({});
	    for (il = 0; il < sizeof(obj_cmd_items); il++)
	    {
		cmd_arr = obj_cmd_items[il][1];
		if (sizeof(cmd_arr))
		    obj_commands = obj_commands + (cmd_arr - obj_commands);
	    }
	    return 1;
	}
    return 0;
}

/*
 * Function name: set_trusted
 * Description:   Sets the effuserid to the userid of this object. This is
 *                used by the 'trust' command mainly on wiztools.
 * Arguments:     arg - 1 = set the euid of this object.
 *                      0 = remove the euid.
 */
public void
set_trusted(int arg)
{
    object cobj;

    if (!objectp(cobj = previous_object()))
	return;

    if ((geteuid(cobj) != getuid()) &&
	(geteuid(cobj) != SECURITY->query_domain_lord(getuid())) &&
	(SECURITY->query_wiz_rank(geteuid(cobj)) < WIZ_ARCH) &&
	(geteuid(cobj) != ROOT_UID))
    {
	return;
    }

    if (arg)
	seteuid(getuid(this_object()));
    else
	seteuid(0);
}

/*
 * Function name: cut_sig_fig
 * Description:   Will reduce the number given to a new number with specified
 *                number of significant numbers.
 * Arguments:     fig - the number to correct.
 *		  num - number of significant numbers.
 * Returns:       the number with two significant numbers
 */
public int
cut_sig_fig(int fig, int num)
{
    int fac, lim;
    fac = 1;

    lim = ftoi(pow(10.0, itof(num)));
    while(fig > lim)
    {
	fac = fac * 10;
	fig = fig / 10;
    }

    return fig * fac;
}

/*
 * Function name: appraise_value
 * Description:   This function is called when someon tries to appraise value
 *                of this object.
 * Arguments:     num - use this number instead of skill if given.
 */
public string
appraise_value(int num)
{
    int value, skill, seed;

    if (!num)
        skill = this_player()->query_skill(SS_APPR_VAL);
    else
        skill = num;

/*
 * To nie mialo wiekszego sensu... /Alvin.
 *    skill = 1000 / (skill + 1);
 */
    skill = ((100 - skill) * 6 / 10) + 1;
    value = query_prop(OBJ_I_VALUE);
    sscanf(OB_NUM(this_object()), "%d", seed);
    skill = random(skill, seed);
    value = cut_sig_fig(value + ((skill % 2 ?: -1) * skill * value / 100), 2);

    switch (value)
    {
        case 0..19:
            return value + " grosz" + ilosc(value, "", "e", "y");
            break;
        case 20..239:
             if (value % 20)
                 return value / 20 + " denar" + ilosc(value / 20, "a",
                 "y", "^ow") + " i " + value % 20 + " grosz" + ilosc(value,
                 "", "e", "y");
             else
                 return value / 20 + " denar" + ilosc(value/20, "a", "y", "^ow");
             break;
        case 240..92233720368547758:
             if (value % 240 >= 20)
                 return value / 240 + " koron" + ilosc(value / 240, "^e",
                 "y", "") + " i " + value % 240 / 20 + " denar" +
                 ilosc(value % 240 / 20, "a", "y", "^ow");
             else
                 return value / 240 + " koron" + ilosc(value/240, "^e", "y", "");
             break;
    }
}

/*
 * Function name: appraise_weight
 * Description:   This function is called when someon tries to appraise weight
 *                of this object.
 * Arguments:     num - use this number instead of skill if given.
 */
public string
appraise_weight(int num)
{
    int value, skill, seed;
    float valuedr, valueoz, valuelb; //kolejno: drachmy, uncje i funty.

    if (!num)
        skill = this_player()->query_skill(SS_APPR_OBJ);
    else
        skill = num;

/*
 * To nie mialo wiekszego sensu... /Alvin.
 *    skill = 1000 / (skill + 1);
 */
    skill = ((100 - skill) * 6 / 10) + 1;
    value = query_prop(OBJ_I_WEIGHT);
    sscanf(OB_NUM(this_object()), "%d", seed);
    skill = random(skill, seed);
    value = cut_sig_fig(value + ((skill % 2 ?: -1) * skill * value / 100), 2);

    valuedr = itof(value) / 0.866;
    valueoz = itof(value) / 28.35;
    valuelb = valueoz / 16.0;

    if (itof(value) < 28.35)
        return ftoi(valuedr) + " " + ilosc(ftoi(valuedr), "drachm^e", "drachmy", "drachm");
    else
    {
        if (itof(value) < 453.6)
            return ftoi(valueoz) + " " + ilosc(ftoi(valueoz), "uncj^e", "uncje", "uncji");
        else
            return ftoi(valuelb) + " " + ilosc(ftoi(valuelb), "funt", "funty", "funt^ow");
    }
}

/*
 * Function name: appraise_volume
 * Description:   This function is called when someon tries to appraise volume
 *                of this object.
 * Arguments:     num - use this number instead of skill if given.
 */
public string
appraise_volume(int num)
{
    int value, skill, seed;
    float valueoz, valuept, valueqt, valuegal; /* kolejno: uncje (plynne),
                                                  pinty, kwarty, galony. */
    if (!num)
	skill = this_player()->query_skill(SS_APPR_OBJ);
    else
	skill = num;

/*
 * To nie mialo wiekszego sensu... /Alvin.
 *    skill = 1000 / (skill + 1);
 */
    skill = ((100 - skill) * 6 / 10) + 1;
    value = query_prop(OBJ_I_VOLUME);
    sscanf(OB_NUM(this_object()), "%d", seed);
    skill = random(skill, seed);
    value = cut_sig_fig(value + ((skill % 2 ?: -1) * skill * value / 100), 2);

    valueoz = itof(value) / 28.41;
    valuept = valueoz / 20.0;
    valueqt = valuept / 2.0;
    valuegal = valueqt / 4.0;

    if (itof(value) < 28.41)
    {
        if (itof(value) < 2.841)
            return ftoi(itof(value) / 0.2841) + " " + ilosc(ftoi(itof(value) /
            0.2841), "setn^a uncji", "setne uncji", "setnych uncji");
        else
            return ftoi(itof(value) / 2.841) + " " + ilosc(ftoi(itof(value) /
            2.841), "dziesi^at^a uncji", "dziesi^ate uncji", "dziesi^atych " +
            "uncji");
    }
    else
    {
        if (itof(value) > 4545.6)
            return ftoi(valuegal) + " " + ilosc(ftoi(valuegal), "galon",
            "galony", "galon^ow");
        if (itof(value) > 1136.4)
            return ftoi(valueqt) + " " + ilosc(ftoi(valueqt), "kwart^e",
            "kwarty", "kwart");
        if (itof(value) > 568.2)
            return ftoi(valuept) + " " + ilosc(ftoi(valuept), "pint^e",
            "pinty", "pint");
        else
            return ftoi(valueoz) + " " + ilosc(ftoi(valueoz), "uncj^e",
            "uncje", "uncji");
    }
}

/*
 * Function name: appraise_object
 * Description:   This function is called when someon tries to appraise this
 *                object.
 * Arguments:    num - use this number instead of skill if given.
 */
public void
appraise_object(int num)
{
    write(this_object()->long(0, this_player()) + "\n");

    if (this_player() == this_object())
    {
        write("Oceniasz, �e wa�ysz " + appraise_weight(num) +
                ", za� twoja obj�to�� wynosi " + appraise_volume(num) + ".\n");
        write("Wydaje ci si�, �e jeste� war" + koncowka("t", "ta", "te") + " " +
                appraise_value(num) + ".\n");
    }
    else
    {
	string wei = appraise_weight(num);
	string vol = appraise_volume(num);
	string val = appraise_value(num);

	if (wei[0..1] == "0 " && vol[0..1] == "0 ")
	    write("Nie jeste^s w stanie ocenic " +
		koncowka("jego", "jej", "jego", "ich", "ich") +
		" wagi ani obj^eto^sci.\n");
	else if (vol[0..1] == "0 ")
            write("Oceniasz, �e " + short(PL_MIA) + " wa�" +
                koncowka("y", "y", "y", "�", "�") + " " + wei + ".\n");
	else if (wei[0..1] == "0 ")
            write("Oceniasz, �e obj^eto^s^c " + short(PL_DOP) +
                " wynosi " + vol + ".\n");
	else
            write("Oceniasz, �e " + short(PL_MIA) + " wa�" +
                koncowka("y", "y", "y", "�", "�") + " " + wei +
                ", za� " + koncowka("jego", "jej", "jego", "ich", "ich") +
                " obj�to�� wynosi " + vol + ".\n");

	if (val[0..1] != "0 ")
    	    write("Wydaje ci si�, �e " + (query_tylko_mn() ? "s�" : "jest") +
                " war" + koncowka("t", "ta", "te", "ci", "te") + " " +
                val + ".\n");
	else
    	    write("Wydaje ci si�, �e nie " + (query_tylko_mn() ? "maj�" : "ma") +
                " �adnej warto�ci.\n");
    }
}

/*
 * Function namn: query_value
 * Description:   Does the same thing as query_prop(OBJ_I_VALUE)
 *                but is needed since unique_array() doesn't send an
 *                argument.
 * Returns:       The value
 */
int
query_value()
{
    return query_prop(OBJ_I_VALUE);
}

/*
 * Nazwa funkcji : search_fun
 * Opis          : Sprawdza, czy graczowi udalo sie, poza ukrytymi w obiekcie
 *                 obiektami, znalezc cos interesujacego.
 * Argumenty     : string str - argument komendy
 *                 int trail  - uzyta komenda: 0, gdy 'szukaj',
 *                                             1, gdy 'przeszukaj'.
 * Zwraca        : Komunikat, ktory ma byc wyswietlony graczowi, lub 0, jesli
 *                 nie udalo mu sie znalezc niczego interesujacego.
 * Uwaga         : Jesli maskujesz te funkcje, zamiast zwracac 0, wywoluj jej
 *                 oryginalna wersje. (return ::search_fun(str, trail);)
 */
static string
search_fun(string str, int trail)
{
    return 0;
}

/*
 * Nazwa funkcji : search_now
 * Opis          : Odpala wynik przeszukania.
 * Argumenty     : object searcher - przeszukujacy
 *                 string str      - argument komendy
 *                 int trail       - uzyta komenda: 0, gdy 'szukaj',
 *                                                  1, gdy 'przeszukaj'.
 */
public void
search_now(object searcher, string str, int trail)
{
    string hidden, fun;
    object *live, *dead, *found;

    if (!searcher)
        return;

    set_this_player(searcher);
    if (query_prop(ROOM_I_IS))
    {
        found = all_inventory(this_object()) - ({searcher});
        found = filter(found, &search_hidden(, searcher));

        if(sizeof(found))
        {
            hidden = COMPOSITE_STH(found, PL_BIE);

            TP->increase_ss(SS_AWARENESS, EXP_SZUKANIE_UDANE_AWARENESS);

            write("Znajdujesz " + hidden + ".\n");
            say(QCIMIE(searcher, PL_MIA) + " znajduje " + QCOMPSTH(PL_BIE)
              + ".\n", ({searcher}) + live);
            live->catch_msg(QCIMIE(searcher, PL_MIA) + " znajduje ci^e.\n");
        }
    }

    if (fun = search_fun(str, trail))
    {
        write(fun);
    }
    else if (!sizeof(found))
    {
        TP->increase_ss(SS_AWARENESS,EXP_SZUKANIE_NIEUDANE_AWARENESS);

        write("Nie udalo ci si^e znale^x^c niczego interesuj^acego.\n");
    }
}

/*
 * Function name: is_live_dead
 * Description  : This function can be used to check whether an object
 *                is a living object or whether it is non-living. Basically
 *                it checks whether the outcome for living(obj) is equal to
 *                the second argument "what".
 * Arguments    : object obj - the object to check.
 *                int what   - when 1, return whether obj is living, when 0
 *                             return whether obj is dead. Note: other "true"
 *                             values for "what" will not work.
 * Returns      : int 1/0 - living/dead if "what" is 1, dead/living if
 *                          "what" is 0.
 */
int
is_live_dead(object obj, int what)
{
    return (living(obj) == what);
}

int
search_hidden(object obj, object who)
{
    if (!obj->query_prop(OBJ_I_HIDE))
	return 0;

    if (who && !who->query_wiz_level() &&
	    who->query_skill(SS_AWARENESS) <
		obj->query_prop(OBJ_I_HIDE))
	return 0;
    obj->remove_prop(OBJ_I_HIDE);
    return 1;
}


/*
 * Nazwa funkcji : search_object
 * Opis          : Ktos usiluje przeszukac obiekt.
 * Argumenty     : string str - argument komendy
 *                 int trail  - uzyta komenda: 0, gdy 'szukaj',
 *                                             1, gdy 'przeszukaj'.
 */
public void
search_object(string str, int trail)
{
    int time;
    object obj;

    time = query_prop(OBJ_I_SEARCH_TIME) + 5;

    if (time < 1)
        search_now(this_player(), str, trail);
    else
    {
        seteuid(getuid(this_object()));
        obj = clone_object("/std/paralyze");
        obj->set_standard_paralyze(trail ? "przeszukiwa^c " +
            (query_prop(ROOM_I_IS) ? str :
            this_object()->short(this_player(), PL_BIE)) :
            "szuka^c" + (str ? " " + str : ""));
        obj->set_name("paraliz_szukania");
        obj->set_stop_fun("stop_search");
        obj->set_stop_object(this_object());
        obj->set_remove_time(time);
        obj->set_finish_object(TO);
        obj->set_finish_fun("search_now", str, trail);
        obj->move(this_player(), 1);
    }
}

/**
 * Function name: stop_search
 * Description:   This function is called if the player decides to stop his
 *                search.
 * Arguments:     arg - If string extra string when player made a command
 *                if object, the time ran out on the paralyze.
 * Returns:       If this function should not allow the player to stop search
 *                it shall return 1.
 */
varargs int
stop_search(mixed arg)
{
    return 0;
}

/*
 * Function name: stat_object
 * Description:   This function is called when a wizard wants to get more
 *                information about an object.
 * Returns:       str - The string to write..
 */
string
stat_object()
{
    string str, tstr;
    mixed tmp;
    string* tmptab;
    int il, size;
    float proc;

    str = "Plik: " + file_name(this_object()) + ", Tw^orca: " +
        creator(this_object()) + ", Uid: " + getuid(this_object()) +
        ", Euid: " + geteuid(this_object()) + "\n";
    if (tstr = query_prop(OBJ_S_WIZINFO))
        str += "Dodatkowe informacje:\n\t" + tstr;

    str += "Nazwa: " + query_name() + " \t";
    str += "Kr^otki opis: " + short() + "\n";
    str += "D^lugi opis: " + long();

    tstr = "";
    if (tmp = query_prop(OBJ_I_WEIGHT))
        tstr += "Waga obiektu: " + tmp + " \t";
    if (tmp = query_prop(OBJ_I_VOLUME))
        tstr += "Obj^eto^s^c obiektu: " + tmp + " \t";
    if (tmp = query_prop(OBJ_I_VALUE))
        tstr += "Warto^s^c obiektu: " + tmp + "";
    if (strlen(tstr))
        str += tstr + "\n";

    tstr = "";
    if ((mappingp(obj_sklad)) && (size = m_sizeof(obj_sklad)))
    {
        tmptab = m_indices(obj_sklad);
        for (il = 0; il < size; ++il)
            if ((proc = procent_materialu(tmptab[il])) > 0.0)
                tstr += " \t" + MATERIALY_NAZWY[tmptab[il]] + " = " + sprintf("%.2f", proc) + "%\n";
    }
    if (strlen(tstr))
        str += "Sk�ad obiektu:\n" + tstr;

    tstr = "";
    if (tmp = query_prop(OBJ_I_HIDE))
        tstr += "ukryty\t";
    if (tmp = query_prop(OBJ_I_INVIS))
        tstr += "niewidzialny\t";
    if (tmp = query_prop(MAGIC_AM_MAGIC))
        tstr += "magiczny\t";
    if (query_no_show())
        tstr += "no_show\t";
    if (!query_no_show() && query_no_show_composite())
        tstr += "no_show_composite\t";
    if (strlen(tstr))
        str += tstr + "\n";

    return str;
}

/*
 * Function name: query_alarms
 * Description:   This function gives all alarms set in this object.
 * Returns:       The list as given by get_all_alarms.
 */
mixed
query_alarms()
{
    return get_all_alarms();
}

nomask public int
object_help(string str)
{
    string pomoc;
    object *co;
    if((TO->is_room && str) || (!TO->is_room() && (!str || !parse_command(str, AIE(TP) + DI(TP), "%i:" + PL_MIA, co) ||
        !(co = NORMAL_ACCESS(co, 0, 0)) || sizeof(co) > 1 || co[0] != TO) || (!(pomoc = TO->query_pomoc()) && !(pomoc == TO->query_help()))))
    {
        return 0;
    }

    write(pomoc);

    if(TO->is_room())
        write("\nAby uzyska� pomoc og�ln� wpisz 'pomoc og�lna'.\n");

    return 1;
}

/*
 * Function name: init
 * Description  : Add the 'command items' of this object. Note that if
 *		  you redefine this function, the command items will not
 *		  work unless you do ::init(); in your code!
 */
public void
init()
{
    int index = sizeof(obj_commands);

    while(--index >= 0)
        add_action(cmditem_action, obj_commands[index]);

    add_action(&object_help(), "?", 1);
    add_action(&object_help(), "pomoc");
}

public string
extra_desc_unique_fun()
{
//    mixed ret = obj_props[OBJ_S_EXTRA_DESC];
//    if (!stringp(ret)) return "";
//    return ret;
    return "";
}

/* Nazwa funkcji: make_me_sitable
 * Opis		: do pomieszczenia w ktorym przebywa obiekt
 *                dodawany jest add_sit     (c)Molder
 * Argumenty:     jak w add_sit
 */

public void
make_me_sitable(string sit_cm, string sit_lon, string sit_u, int sit_spac, int przyp = PL_MIE, string przyimek = "na")
{
    this_object()->add_prop("_siedzonko", 1);
    sit_cmd         = sit_cm;
    sit_long        = sit_lon;
    sit_up          = sit_u;
    sit_space       = sit_spac;
    sit_przyp       = przyp;
    sit_przyimek    = przyimek;
}

public void
set_owners(mixed o)
{
    if(!pointerp(o))
    {
        if (stringp(o))
            o = ({ o });
        else if(objectp(o))
            o = ({ o->query_real_name() });
        else
            return;
    }

    o = filter(o, stringp);

    if(!sizeof(o))
        return;

    object_owners = o;
}

public string *
query_owners()
{
    return object_owners;
}

/**
 * Dzi�ki tej funkcji mo�emy obiektom �adowanym na lokacji usun��
 * orig_place:)
 */
public void
unset_orig_place()
{
    orig_place = 0;
}

/**
 * @return miejce gdzie orginalnie powinien znajdowa� si� przedmiot.
 */
public string*
query_orig_place()
{
    return orig_place;
}

/**
 * Ustawia osobe(osoby) kt�re ukrad�y dany przedmiot.
 * @param who osoby kt�re ukrad�y przedmiot.
 */
public void
set_who_stole(mixed who)
{
    if(!pointerp(who))
    {
        if(stringp(who))
            who = ({ who });
        else if(objectp(who))
            who = ({ who->query_real_name() });
        else
            return;
    }

    who = filter(who, stringp);

    if(!sizeof(who))
        return;

    who_stole = who;
}

/**
 * @return Osobe(by) kt�ra ukrad�a przedmiot.
 */
public string*
query_who_stole()
{
    return who_stole;
}

public void
set_type(int type)
{
    obj_type = type;
}

public int
query_type()
{
    return obj_type;
}

string
show_subloc(string subloc, object on_obj, object for_obj)
{
    return 0;
}

/**
 * Funkcja ta wywo�uje znajduj�c� si� w swoim argumencie funkcje co godzine.
 * @param funkcja funkcja w postaci string'a lub function'a.
 * @param ... argumenty do funkcji fun.
 */
public varargs void set_alarm_every_hour(mixed fun, ...)
{
    //Troch� t� funkcje przebudowa�em, teraz korzysta z infromatora,
    //dzi�ki czemu jej dzia�anie jest o tyle lepsze, �e nie powoduje
    //dodania alarmu w ka�dym obiekcie, a w zamian za to jest jeden
    //alarm, kt�ry wywo�uje si� w momencie rozpocz�cia ka�dej kolejnej
    //godziny.

    if(!argv || !pointerp(argv))
        argv = ({});

    INFORMATOR->inform_every_hourv(fun, 0, argv);

#if 0
    if(!stringp(fun))
    {
        if(functionp(fun))
        {
            fun = function_name(fun);

            if(wildmatch("*->*", fun))
                fun = explode(fun, "->")[1];
        }
        else
            return;
    }

    set_alarmv((59.0 - itof(MT_MINUTA)) * 60.0 + (65.0 - itof(MT_SEKUNDA)),
        3600.0, fun, (pointerp(argv) && sizeof(argv) ? argv : ({})));
#endif
}

/**
 * Funkcja zwracaj�ca napis dla auto_�adowania. [naprawianie]
 */
public nomask string
query_repairable_auto_load()
{
    if (query_prop(OBJ_I_IN_REPAIR_BY_CRAFTSMAN))
        return " ~REP" + query_prop(OBJ_I_IN_REPAIR_BY_CRAFTSMAN) + "REP~ ";

    return "";
}

/**
 * Funkcja inicjuj�ca obiekt na podstawie podanego napisu. [naprawianie]
 */
public nomask string
init_repairable_arg(string arg)
{
    int id;
    string toReturn, foobar;

    if (arg == 0) {
        return 0;
    }

    if (wildmatch("*~REP*REP~*", arg)) {
        sscanf(arg, "%s~REP%dREP~ %s", foobar, id, toReturn);
        foreach (string curstr : explode(foobar, "")) {
            if (curstr != " ")
                return arg;
        }
        add_prop(OBJ_I_IN_REPAIR_BY_CRAFTSMAN, id);
        return toReturn;
    }
    return arg;
}

/**
 * Funkcja zwracaj�ca napis dla auto_�adowania. [ukrywanie]
 */
public nomask string
query_hide_auto_load()
{
    if (query_prop(OBJ_I_HIDE)) {
        return " ~HIDE" + query_prop(OBJ_I_HIDE) + "HIDE~ ";
    }
    return "";
}

/**
 * Funkcja inicjuj�ca obiekt na podstawie podanego napisu. [ukrywanie]
 */
public nomask string
init_hide_arg(string arg)
{
    int id;
    string toReturn, foobar;

    if (arg == 0) {
        return 0;
    }

    if (wildmatch("*~HIDE*HIDE~*", arg))
    {
        sscanf(arg, "%s~HIDE%dHIDE~ %s", foobar, id, toReturn);
        foreach (string curstr : explode(foobar, ""))
        {
            if (curstr != " ")
                return arg;
        }
        add_prop(OBJ_I_HIDE, id);
        return toReturn;
    }
    return arg;
}


public string
query_auto_load()
{
    int first = 1;
    string toReturn = MASTER + ":" + query_prop(OBJ_I_WEIGHT) + "," +
        query_prop(OBJ_I_VOLUME) + "," + query_prop(OBJ_I_VALUE) + " #";

    toReturn += implode(query_orig_place(), ",") + "#";
    toReturn += implode(query_who_stole(), ",") + "#";

    foreach (string material : query_materialy())
    {
        if (!first)
            toReturn += ",";

        toReturn += material + "=" + udzial_materialu(material);
    }

    return toReturn + "# " + query_repairable_auto_load() + query_hide_auto_load();
}

public varargs string
init_arg(string arg)
{
    obj_inited = arg;

    string materials, rest, orig, steal;

    int i1, i2, i3;
    if (arg == 0)
    {
        string subloc;
        object ob;
        if (pointerp(query_subloc()))
        {
            if (sizeof(query_subloc()))
                subloc = query_subloc()[0];
            else
                subloc = "0";
        }
        else
        {
            subloc = query_subloc();

            if (subloc == 0)
                subloc = "0";
        }

        orig_place = ({ subloc });
        ob = TO;

        while ((ob = ENV(ob)))
        {
            if(ob->is_player())
            {
                orig_place = ({});
                break;
            }

            if (IS_ROOM_OBJECT(ob))
                orig_place = ({ MASTER_OB(ob) }) + orig_place;
            else
            {
                if (pointerp(ob->query_subloc()))
                {
                    if (sizeof(ob->query_subloc()))
                        subloc = ob->query_subloc()[0];
                    else
                        subloc = 0;
                }
                else
                    subloc = ob->query_subloc();

                orig_place = ({ MASTER_OB(ob) + "|" + subloc }) + orig_place;
            }

            if(ob->is_living())
                break;
        }
        return 0;
    }
    else if (sscanf(arg, "%d,%d,%d #%s#%s#%s#%s", i1, i2, i3, orig, steal, materials, rest) == 7)
    {
        add_prop(OBJ_I_WEIGHT, i1);
        add_prop(OBJ_I_VOLUME, i2);
        add_prop(OBJ_I_VALUE, i3);

        foreach (string material : query_materialy())
            ustaw_material(material, 0);

        foreach (string matnum : explode(materials, ","))
        {
            string matnam;
            int num;
            sscanf(matnum, "%s=%d", matnam, num);
            ustaw_material(matnam, num);
        }

        foreach (string cur_orig : explode(orig, ","))
            orig_place += ({ cur_orig });

        foreach (string cur_elem : explode(steal, ","))
            who_stole += ({ cur_elem });

    }

    return init_hide_arg(init_repairable_arg(arg));
}

/**
 * Funkcja losuj�ca przymiotniki.
 * Stara wersja(nie zalecana do u�ycia) wygl�da tak:
 * @param przymiotniki lista z przymiotnikami - przym1poj:przym1mn przym2poj:przym2mn||...
 * @param n ilo�� losowanych przymiotnik�w.
 * Nowa wersja wygl�da tak i t� prosz� stosowa�.
 * @param przym1poj,przym1mn,przym2poj....przymNpoj,przymNmn
 * @param n ilo�� losowanych przymiotnik�w
 * @example
 * Stary zapis:
 * random_przym("du�y:duzi ma�y:mali �redni:�redni||bia�y:biali, 2)
 * b�dzie wygl�da� tak:
 * random_przym(({"du�y", "duzi", "ma�y", "mali", "�redni", "�redni"}), ({"bia�y", "biali"}), 2);
 */
varargs void random_przym(...)
{
    //Stary spos�b:
    if(sizeof(argv) == 2)
    {
        //Sprawdzamy czy odpowiednie warto�ci, kiedy� tego nie trzeba by�o robi�,
        //ale teraz mamy inne argumenty i musimy.

        if(!stringp(argv[0]))
        {
            throw("Podane dwa argumenty do random_przym i pierwszy nie string.");
            return;
        }

        if(!intp(argv[1]))
        {
            throw("Podane dwa argumenty do random_przym i drugi nie integer.\n");
            return;
        }

        string str = argv[0];
        int n = argv[1];

        mixed* groups = ({ });

        groups = explode(str, "||");

        int groups_size = sizeof(groups);

        n = min(n, groups_size);

        for(int i =0; i < groups_size; i++)
            groups[i] = explode(groups[i], " ");

        for(int a = 0; a < n; a++)
        {
            int group_index = random(sizeof(groups));
            int adj_index = random(sizeof(groups[group_index]));
            string* tmp = explode(groups[group_index][adj_index], ":");
            if(!sizeof(tmp))
            {
                a--;
                continue;
            }
            dodaj_przym(tmp[0], tmp[1]);
            groups = groups-({groups[group_index]});
        }
    }
    //nowy spos�b
    else
    {
        //Sprawdzamy czy ostatni argument jest integerem
        if(!intp(argv[sizeof(argv)-1]))
        {
            throw("Ostatni argument funkcji random_przym() nie jest integerem.\n");
            return;
        }

        //A to, �eby nie wylosowa�
        int n = min(argv[sizeof(argv)-1], sizeof(argv)-1);

        for(int i=0; i < sizeof(argv)-1; i++)
        {
            if(!pointerp(argv[i]))
            {
                throw("Argument " + (i+1) + " w random_przym() musi by� tablic� o liczbie element�w " +
                    "r�wnej 2 lub jakiejkolwiek wielokrotno�ci liczby 2.\n");
                return;
            }

            if(sizeof(argv[i]) < 2)
            {
                throw("Argument " + (i+1) + " w random_przym jest tablica o zbyt ma�ej ilo�ci element�w."+
                    "Minimalna ilo�� element�w to 2.\n");
                return;
            }

            if(sizeof(argv[i]) % 2 != 0)
            {
                throw("Wielko�� tablicy w argumencie " + (i+1) + " musi by� wielokrotno�ci� liczby 2.\n");
                return;
            }
        }

        for(int i=0; i < n; i++)
        {
            int j = random(sizeof(argv)-1);

            if(sizeof(argv[j]) > 2)
            {
                int k = random(sizeof(argv[j])/2)*2;
                dodaj_przym(argv[j][k], argv[j][k+1]);
            }
            else
                dodaj_przym(argv[j][0], argv[j][1]);

            argv = argv-({argv[j]});
        }
    }
}

/**
 * Funkcja wywo�ywana kiedy dostajemy z broni strzeleckiej.
 *
 * @param strzelajacy strzelajacy z broni.
 * @param pocisk pocisk kt�rym dostaniemy
 * @param bron bro� z kt�rej by�o strzelane
 * @param sila si�a strza�u
 * @param szybkosc szybko�� strza�u
 * @param sila_naciagu si�a naci�gu.
 *
 * @return > 1 je�li pocisk ma nie zosta� zniszczony w funkcji strzel.
 */
public int
bs_hit_me(object strzelajacy, object pocisk, object bron, int sila, int szybkosc, int sila_naciagu)
{
    string z_kierunku, na_kierunek;

    if(ENV(TO) != ENV(strzelajacy))
    {
        //Wyszukujemy �adnie kierunku z kt�rego przylecia�a strza�a.
        int i;
        if((i = member_array(MASTER_OB(ENV(strzelajacy)), ENV(TO)->query_exit_rooms())) >= 0)
            z_kierunku = bron->zamien_kierunek(ENV(TO)->query_exit_cmds()[i], 2);
        if((i = member_array(MASTER_OB(ENV(TO)), ENV(strzelajacy)->query_exit_rooms())) >= 0)
            na_kierunek = bron->zamien_kierunek(ENV(strzelajacy)->query_exit_cmds()[i], 3);

        tell_roombb(ENV(TO), capitalize(z_kierunku) + " przylatuje " + pocisk->query_nazwa(PL_MIA) + ", "+
            "kt�ry trafia w " + QSHORT(TO, PL_BIE) + " i odbija si� od " +
            koncowka("niego", "niej", "niego", "nich", "nich") + ".\n");
    }

    strzelajacy->catch_msg("Trafiasz w " + short(strzelajacy, PL_BIE) + " " +
        (ENV(strzelajacy) != ENV(TO) ? na_kierunek + " " : "") + "jednak wystrzel" +
        pocisk->koncowka("ony", "ona", "one", "eni", "one") + " z " + bron->short(strzelajacy, PL_DOP) + " " +
        pocisk->query_nazwa(PL_MIA) + " odbija si� od " + koncowka("niego", "niej", "niego", "nich", "nich") + ".\n");

    if(ENV(strzelajacy) == ENV(TO))
    {
        tell_roombb(ENV(strzelajacy), QCIMIE(strzelajacy, PL_MIA) + " trafia w " + QSHORT(TO, PL_BIE) + " " +
            "jednak wystrzel" + pocisk->koncowka("ony", "ona", "one", "eni", "one") + " z " + QSHORT(bron, PL_DOP) + " " +
            pocisk->query_nazwa(PL_MIA) + " odbija si� od " + koncowka("niego", "niej", "niego", "nich", "nic") + ".\n",
            ({strzelajacy}));
    }
    tell_roombb(ENV(strzelajacy), QCIMIE(TP, PL_MIA) + " wystrzeliwuje " + pocisk->query_nazwa(PL_MIA) +
        " z " + QSHORT(bron, PL_DOP) + " " + na_kierunek + ".\n", ({TO,strzelajacy}));

    //Przenosimy pocisk do otoczenia, kulki i kamienie nie znikn� co najwy�ej zgin�.

    if(pocisk->query_typ() == W_L_SLING)
    {
        pocisk->move(ENV(TO));
        pocisk->hide_me();
    }

    return 1;
}

/**
 * Funkcja zapami�tuje odmian�, opis i przymiotniki.
 */
public string query_opis_auto_load()
{
    string przymiotniki_p, przymiotniki_m;
    if(sizeof(obj_przym) >= 2)
    {
        przymiotniki_p = implode(obj_przym[0], "$") + "";
        przymiotniki_m = implode(obj_przym[1], "$") + "";
    }

    string dlugi;

    if(functionp(obj_long))
    {
        string fn = function_name(obj_long);
        dlugi = "FUN" + fn;
    }
    else
        dlugi = obj_long;

    string short_p = implode(obj_shorts, "$") + "";
    string short_m = implode(obj_pshorts, "$") + "";
    string short_r = obj_rodzaj_shorta + "";

    string nazwy_p = implode(map(obj_names, &implode(,"~!~")), "$") + "";
    string nazwy_m = implode(map(obj_pnames, &implode(,"~!~")), "$") + "";

    string nazwy_rp = implode(map(obj_rodzaje, &implode(,"~!~") @ &map(,&operator(+)("",))), "$") + "";
    string nazwy_rm = implode(map(obj_prodzaje,&implode(,"~!~") @ &map(,&operator(+)("",))), "$") + "";

    return "#@<OO<@#" + przymiotniki_p + "#" + przymiotniki_m + "|#|" + dlugi + "|#|" + short_p + "#" + short_m + "#"+
        short_r + "#" + nazwy_p + "#" + nazwy_m + "#" + nazwy_rp + "#" + nazwy_rm + "#@>OO>@#";
}

/**
 * Funkcja odczytuje odmiane, przymiotniki i opis.
 */
public string init_opis_arg(string arg)
{
    string przymiotniki_p, przymiotniki_m, opis, opis2, short_p, short_m, short_r, nazwa_p, nazwa_m, nazwa_rp, nazwa_rm, pre, post;

    if(!arg)
        return arg;

    if(sscanf(arg, "%s#@<OO<@#%s#%s|#|%s|#|%s#%s#%s#%s#%s#%s#%s#@>OO>@#%s", pre, przymiotniki_p, przymiotniki_m,
        opis, short_p, short_m, short_r, nazwa_p, nazwa_m, nazwa_rp, nazwa_rm, post) != 12)
    {
        return arg;
    }

    obj_przym[0] = explode(przymiotniki_p, "$");
    obj_przym[1] = explode(przymiotniki_m, "$");

    if(stringp(opis))
    {
        if(opis[0..3] == "FUN")
        {
            opis = opis[4..];

            if(wildmatch("*->*", opis))
                opis = explode(opis, "->")[-1];

            function f = mkfunction(opis, TO);
        }
        else
            obj_long = opis;
    }

    obj_shorts          = explode(short_p, "$");
    obj_pshorts         = explode(short_m, "$");

    obj_rodzaj_shorta   = atoi(short_r);

    obj_names           = map(explode(nazwa_p, "$"), &explode(,"~!~"));
    obj_pnames          = map(explode(nazwa_m, "$"), &explode(, "~!~"));

    obj_rodzaje         = map(explode(nazwa_rp, "$"), &explode(, "~!~"));
    obj_prodzaje        = map(explode(nazwa_rm, "$"), &explode(, "~!~"));

    return pre + post;
}