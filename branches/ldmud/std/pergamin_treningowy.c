/**
 * \file /std/pergamin_treningowy.c
 *
 * Plik pomocny w zdobywaniu do�wiadczenia teoretycznego.
 *
 * @author Krun
 * @date Grudzie� 2007
 */

inherit "/std/object.c";
inherit "/lib/training.c";

#include <files.h>
#include <macros.h>
#include <cmdparse.h>
#include <ss_types.h>
#include <materialy.h>
#include <object_types.h>
#include <stdproperties.h>

//Kilka zmiennych
private static int      cena,
                        jakosc,
                        mn,
                        mx,
                        um,
                        czas_czytania,
                        p_start_time,
                        b_id,
                        przeczytany,
                        last_ev;
object                  czytajacy;

void
create_pergamin()
{
    set_long("Jaki� g�upi pergamin treningowy.\n");
}

void
create_object()
{
    ustaw_nazwe("pergamin");

    add_prop(OBJ_M_NO_DROP, 1);
    add_prop(OBJ_M_NO_GIVE, 1);
    add_prop(OBJ_M_NO_SELL, 1);
    add_prop(OBJ_M_NO_STEAL, 1);
    add_prop(OBJ_M_NO_TELEPORT, 1);
    add_prop(OBJ_M_NO_THROW, 1);
    add_prop(OBJ_I_WEIGHT, 10);
    add_prop(OBJ_I_VOLUME, 10);

    ustaw_material(MATERIALY_PAPIER);

    set_type(O_ZWOJE);

    create_pergamin();
}

public void set_long(string long)
{
    ::set_long(long + (um ? "Dzi�ki temu " + short(TP, PL_CEL) + " mo�esz podnie�� swoj� wiedz� "+
        "w " + SS_SKILL_DESC[um][2] : "") + ".\n");
}

public void ustaw_b_id(int i) { b_id = i; }
public int query_b_id() { return b_id; }

public void ustaw_skilla(int u) { um = u; }
public int query_skill() { return um; }

public void ustaw_max_uma(int u) { mx = u; }
public int query_max_uma() { return mx; }

public void ustaw_min_uma(int u) { mn = u; }
public int query_min_uma() { return mn; }

public void ustaw_jakosc(int j) { jakosc = j; }
public int query_jakosc() { return jakosc; }

public void ustaw_cene(int c) { cena = c; }
public int query_cena() { return cena; }

public void ustaw_czas_czytania(int c) { czas_czytania = c; }
public int query_czas_czytania() { return czas_czytania; }

public void eventy_czytania()
{
    int ne;

    while((ne = random(8)) == last_ev);

    last_ev = ne;

    switch(ne)
    {
        case 0:
            czytajacy->catch_msg("Natrafiasz na ma�y kleks, kilka sekund "+
                "zastanawiasz si� co za s�owo powinno znajdowa� si� w tym miejscu, "+
                "po chwili rezygnujesz i czytasz dalej.\n");
            jakosc = (99 * jakosc) / 100;
            break;
        case 1:
            if(ENV(czytajacy)->pora_roku() != MT_ZIMA)
            {
                czytajacy->catch_msg("Na " + TO->short(czytajacy, PL_MIE) +
                    " siada mucha. Zdecydowanym ruchem r�ki zganiasz j� z niego i "+
                    "kontynuujesz czytanie.\n");
            }
            break;
        case 2:
            czytajacy->catch_msg("Natrafiasz na niewyra�nie napisane s�owo. Przez " +
                "chwile skupiasz na nim wzrok i udaje ci si� je rozszyfrowa�.\n");
            break;
        case 3:
            czytajacy->catch_msg("Tw�j wzrok natrafia na wyjedzon� przez mola dziurk�. Przez moment "+
                "zastanawiasz si� jakie s�owo sta�o si� po�ywk� dla insekta, poczym wracasz do czytania.\n");
            break;
        case 4:
            czytajacy->catch_msg("Nag�y trzask gdzie� za rega�ami zmusi� ci� do odruchowego "+
                "podniesienia g�owy znad tekstu. Po kr�tkim poszukiwaniu wzrokiem miejsca "+
                "gdzie przerwa�" + czytajacy->koncowka("e�", "a�") + " czytanie wracasz do "+
                "dalszej lektury.\n");
            break;
        case 5:
            czytajacy->catch_msg("Natrafiasz na linijk�, kt�ra wygl�da tak jakby tu� po jej "+
                "napisaniu, kto� zatar� niechc�cy �wie�o napisany tekst. Po chwili "+
                "skupienia doczytujesz si� jej tre�ci.\n");
            break;
        case 6:
            czytajacy->catch_msg("�lad po jakim� wylanym napoju odcisn�� swoje pi�tno na "+
                "czytanym przez ciebie tek�cie. Po d�u�szej pr�bie rozszyfrowania "+
                "tekstu poddajesz si� i czytasz dalej.\n");
            jakosc = (95 * jakosc / 100);
            break;
        case 7:
            czytajacy->catch_msg("Przerywasz czytanie by ostro�nie przytrzyma� naderwany w "+
                "tym miejscu pergamin. Po po��czeniu kartek s�owa na nowo staj� si� wyra�ne, "+
                "dzi�ki czemu spokojnie wracasz do lektury.\n");
            break;
    }
}

static private void dodaj_paraliz(object ob)
{
    object paraliz = clone_object(PARALYZE_OBJECT);

    p_start_time = time();

    paraliz->set_name("paraliz_uczenia_sie");
    paraliz->set_fail_message("W tym momencie zajmujesz si� czytaniem.\n"+
        "Nie mo�esz robi� nic innego.\n");

    paraliz->set_stop_message("");
    paraliz->set_stop_fun("koniec_czytania");
    paraliz->set_stop_verb("przesta�");
    paraliz->set_stop_object(TO);

    paraliz->set_event_time(10.0 + frandom(15, 2));
    paraliz->set_event_fun("eventy_czytania");
    paraliz->set_event_object(TO);

    paraliz->set_finish_fun("koniec_czytania");
    paraliz->set_finish_object(TO);
    paraliz->set_remove_time(czas_czytania);

    paraliz->move(TP, 1);
}

public int przeczytaj(string arg)
{
    object *perg;

    NF("Co takiego chcesz przeczyta�?\n");

    if(!arg)
        return 0;

    if(!parse_command(arg, ENV(TP), "%i:" + PL_BIE, perg))
        return 0;

    if(!perg)
        return 0;

    perg = NORMAL_ACCESS(perg, 0, 0);

    if(!perg)
        return 0;

    if(sizeof(perg) > 1)
        return NF("Nie mo�esz przeczyta� wi�cej ni� jedn� rzecz na raz.\n");

    if(perg[0] != TO)
        return 0;

    if(przeczytany)
        return NF("Ale� ju� posiad�e� t� wiedz�.\n");

    czytajacy = TP;

    write("Zaczynasz czyta� " + short(TP, PL_BIE) + ".\n");
    saybb(QCIMIE(TP, PL_MIA) + " zaczyna czyta� " + QSHORT(TO, PL_BIE) + ".\n");

    TP->add_prop(LIVE_S_EXTRA_SHORT, " czytaj�cy " + QSHORT(TO, PL_BIE) + "");

    // dodajemy parali�
    dodaj_paraliz(TP);

    return 1;
}

public void koniec_czytania()
{
    int ile_expa, roznica;
    //Jesli przestalismy to dostaniemy tylko czesc expa.

    if((roznica = time() - p_start_time) < czas_czytania)
    {
        ile_expa = ftoi(itof(jakosc) * (itof(roznica)/itof(czas_czytania)));
        czas_czytania -= roznica;
        jakosc = jakosc - ile_expa;
    }
    else
    {
        ile_expa = jakosc;
        przeczytany = 1;
    }

    int dodano = dodaj_expa(um, TP, ile_expa, mn, mx);
    write("Ko�czysz czyta� " + short(TP, PL_BIE) + (dodano ? ". Posiad�e� troch� wiedzy o " +
        SS_SKILL_DESC[um][2] : "") + ".\n");

    saybb(QCIMIE(TP, PL_MIA) + " ko�czy czyta� " + QSHORT(TO, PL_BIE) + ".\n");
    TP->remove_prop(LIVE_S_EXTRA_SHORT);
}

void init()
{
    add_action(przeczytaj, "przeczytaj");
}

public int
query_pergamin_treningowy()
{
    return 1;
}

public int
is_pergamin_treningowy()
{
    return 1;
}

public void
leave_env(object from, object to)
{
    if(calling_function() != "remove_object")
        remove_object();
}

public int
query_auto_load()
{
    return 0;
}