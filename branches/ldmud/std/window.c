/**
 * \file /std/door.c
 *
 * Standardowe drzwi oparte o standaryzowany plik /lib/open_close.c
 *
 * Plik zbudowany na podstawie starych drzwi.
 * @author Krun
 * @date Grudzie� 2007
 *
 */
#pragma save_binary
#pragma strict_types

inherit "/std/door.c";

#include <pl.h>
#include <macros.h>
#include <formulas.h>
#include <cmdparse.h>
#include <stdproperties.h>

int         is_outer,       /* czy jest to okno zewn�trzne czy wewn�trzne */
            pietro,         /* na kt�rym pi�trze jest okno */
            modifier=100;   /* modyfikator hp odejmowanego przy upadku */


/**
 * Kreator okna.
 */
public void
create_window()
{
}

/**
 * Kreator drzwi na podstawie kt�rych zbudowane jest okno.
 */
public nomask void
create_door()
{
    set_no_lock(1);   // standardowo okna nie da si� zablokowa�
    set_no_unlock(1); // nie da si� zablokowa� to i odblokowa� si� nie da
    set_no_pick(1);   // nie da si� zablokowa� to i nie ma przy czym wytrychem
                      // grzeba�.

    set_oneside(1);   // Zwykle okna s� jednostronne!
    create_window();
}

/**
 * Dzi�ki tej funkcji mo�emy ustawi� czy okno jest wewn�trzne czy zewn�trzne.
 *
 * @param out - 1 je�li jest to okno zewn�trzne, 0 je�li nie
 */
public void
set_outer(int out)
{
    is_outer = !!out;
}

/**
 * @return 1 - okno zewn�trzne
 * @return 0 - okno wewn�trzne
 */
public int
query_outer()
{
    return is_outer;
}

/**
 * Ustawiamy na kt�rym pi�trze jest okno.
 *
 * @param p pi�tro
 */
public void ustaw_pietro(int p)
{
    pietro = p;
}

/**
 * @return na kt�rym pi�trze jest to okno.
 */
public int query_pietro()
{
    return pietro;
}

/**
 * Ustawiamy modyfikator hp odebranego za spadanie
 * domy�lnie 100%. 0-500%
 *
 * @param mod modyfikator
 */
public void ustaw_modyfikator(int mod)
{
    modifier = min(500, max(0, mod));
}

/**
 * Pass_door ma nie dzia�a� wi�c blokujemy!
 */
public int
pass_door(string arg)
{
    return 0;
}

public void pass_it(object tp)
{
    tp->reduce_hp(0, F_HP_ZA_UPADEK(pietro) * modifier / 100, 10);
}

/**
 * W zamian za pass_door mamy pass_window, kt�re obs�uguje tylko z g�ry
 * okre�lone komendy.
 */
public int
pass_window(string arg, mixed okno)
{
    int tmpm, ret;
    string bezokol, os1, os2;

    switch(query_verb())
    {
        case "wyjd�":
            if(is_outer)
                return NF2("Nie mo�na wyj�� przez okno umieszczone na zewn�trz budynku.\n", 3);

            bezokol = "wyj��";
            os1 = "wychodzisz";
            os2 = "wychodzi";
            break;

        case "wejd�":
            if(!is_outer)
                return NF2("Nie mo�na wej�c przez okno umieszczone wewn�trz.\n", 3);

            bezokol = "wej��";
            os1 = "wchodzisz";
            os2 = "wchodzi";
            break;

        case "wyskocz":
            bezokol = "wyskoczy�";
            os1 = "wyskakujesz";
            os2 = "wyskakuje";
            break;

        default:
            if(is_outer)
            {
                bezokol = "wej��";
                os1 = "wchodzisz";
                os2 = "wychodzisz";
            }
            else
            {
                bezokol = "wyj��";
                os1 = "wychodzisz";
                os2 = "wychodzi";
            }
    }

    NF("Przez co chcesz " + bezokol + "?\n");

    if(!okno)
    {
        if(!arg)
            return 0;

        if(!parse_command(arg, ENV(TP), "'przez' %i:" + PL_BIE, okno))
            return 0;

        okno = NORMAL_ACCESS(okno, 0, 0);

        if(!sizeof(okno))
            return 0;

        if(sizeof(okno) > 1)
            return NF("Jak chcesz " + bezokol + " przez kilka okien na raz?\n");

        okno = okno[0];
    }

    if(!my_pass_mess)
    {
        tmpm = 1;
        if(is_outer)
            my_pass_mess = "Wchodzisz przez " + okno->short(TP, PL_BIE) + ".\n";
        else
            my_pass_mess = "Wyskakujesz przez " + okno->short(TP, PL_BIE) + ".\n";
    }

    ret = (::pass_door());

    if(tmpm)
        my_pass_mess = 0;

    return ret;
}

/**
 * A to taka pomocnicza funkcyjka.
 */
public int
pass_window2(string arg)
{
    object *okno;

    if(!arg)
        return 0;

    if(!parse_command(query_verb(), "%i:" + PL_MIA, okno))
        return 0;

    return pass_window(0, okno);
}

/**
 * A t� funkcje blokujemy bo w oknie nie ma ona funkcjonalno�ci.
 */
public varargs void
set_pass_command(mixed s1, mixed s2, mixed s3)
{
    ;
}

/**
 * A t� zn�w musimy zwracamy wszystkie komendy kt�re dopisali�my,
 * bez w zgl�du na to co jest w zmiennej pass_command, pass_mess1
 * i pass_mess2, kt�ra dla tego obiektu po prostu nie istnieje.
 */
public string *
query_pass_command()
{
    return ({TO->query_name(PL_MIA)}) + (query_outer() ? ({"wejd�"}) : ({"wyjd�", "wyskocz"}));
}


/**
 * Troche musimy namiesza� w inicie coby by�o dobrze.
 */
public void
init()
{
    ::init();

    if(query_outer())
    {
        add_action(pass_window, "wejd�");
    }
    else
    {
        add_action(pass_window, "wyjd�");
        add_action(pass_window, "wyskocz");
    }

    add_action(pass_window2, "");
}

/**
 * @return Obiekt drugiego okna.
 */
public object
query_other_window()
{
    return query_other_door();
}

// A teraz kilka funkcji dla sp�jno�ci ze star� wersj�, bo mi sie za chuj wszystkiego poprawia� nie chce.
public void
set_window_id(string id)
{
    set_door_id(id);
}

public string
query_widnow_id()
{
    return query_door_id();
}

public void
set_window_desc(mixed desc)
{
    set_long(desc);
}

public string
query_window_desc()
{
    return query_long();
}