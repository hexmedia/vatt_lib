/**
 * TODO:
 *  - przerobi� na random_przym.
 */

inherit "/std/holdable_object.c";

#include <pl.h>
#include <stdproperties.h>
#include <macros.h>
#include <filter_funs.h>

string *losowy_przym()
{
   string *lp;
   string *lm;
   int i;
   lp=({ "niepozorny", "ma^ly", "czarny", "szary", "brunatny",
"niewielki"});
   lm=({"niepozorni", "mali", "czarni", "szarzy",  "brunatni",
"niewielcy"});
   i=random(6);
   return ({ lp[i] , lm[i] });
}

void
create_holdable_object()
{
    string *przm = losowy_przym();

    ustaw_nazwe("krzemie�");

    dodaj_przym( przm[0], przm[1] );

    set_long("Ma^ly niepozorny krzemie^n. Kto by pomy^sla^l, ^ze to " +
        "jedno z najwi^ekszych odkry^c w historii...\n");

    add_prop(CONT_I_WEIGHT, 200);
    add_prop(CONT_I_VOLUME, 50);
    add_prop(OBJ_I_VALUE,1);
}

int
query_krzemien()
{
    return 1;
}

public string
query_krzemien_auto_load()
{
    return "##std/krzemien#s#" + query_przym() + "#" + query_pprzym() + "##std/krzemien#e#";
}

public string
init_krzemien_arg(string arg)
{
    string poj, mn, foobar, rest;
    if (arg == 0) {
        return 0;
    }
    sscanf(arg, "%s##std/krzemien#s#%s#%s##std/krzemien#e#%s", foobar, poj, mn, rest);
    remove_adj(query_przym());
    dodaj_przym( poj, mn );
    return rest;
}

public string
query_auto_load()
{
    return ::query_auto_load() + query_krzemien_auto_load();
}

public string
init_arg(string arg)
{
    return init_krzemien_arg(::init_arg(arg));
}
