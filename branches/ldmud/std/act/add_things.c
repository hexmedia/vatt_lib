/**
 * \file /std/act/add_things.c
 *
 * This is a file inherited into monster.c that enables the player to
 * add weapons and armour to the monster in a very convinient way. The
 * monster will automaticly wield/wear the things.
 *
 * just do add_armour(string filename);        or
 *         add_weapon(string filename);
 *
 * and the monster will clone, move and wield/wear the things.
 *      (The functions return the objects)
 *
 *     Dimitri, the mighty Avatar !
 *
 * We thank PaderMUD for this File
 */

#pragma save_binary
#pragma strict_types

#include <macros.h>
#include <stdproperties.h>

object *wep = ({});
object *arm = ({});

void move_and_wearwield()
{
    object old_tp = TP;
    set_this_player(TO); // dla wield/wear_me().

    if(sizeof(arm))
    {
        foreach (object x : wep)
        {
            if (x->move(TO))
                x->remove_object();
            else
            {
                x->init_arg(0);
                x->wield_me(1);
            }
        }
    }

    if(sizeof(arm))
    {
        foreach (object x : arm)
        {
            if (x->move(TO))
                x->remove_object();
            else
            {
                x->init_arg(0);
                x->wear_me(1, TO);
            }
        }
    }

    arm = filter(arm, objectp);
    wep = filter(wep, objectp);
    set_this_player(old_tp);
}

/**
 * Dodajemy bro� do npc'a
 *
 * @param file plik broni
 * @param ilosc ile egzemplarzy dodac
 *
 * UWAGA: Wywo�anie dwa razy add_weapon z tym samym plikiem doda nam tylko jeden egzemplarz! Musimy dodawa�
 *        z argumentem ilo�� a�eby uzyska� wi�cej egzemplarzy(By� mo�e uda si� rozwik�ac ten problem).
 * @param ob zwraca obiekt broni.
 */
public varargs object
add_weapon(string file, int ilosc=1)
{
    object *weapons = ({});

    if (!strlen(file) || !ilosc)
        return 0;

    seteuid(getuid(this_object()));

    while(ilosc-- > 0)
        weapons += ({ clone_object(file) });

    weapons = filter(weapons, objectp);

    if (!sizeof(weapons))
        return 0;

    wep += weapons;

    return weapons[0];
}

/**
 * Dodajemy zbroje do npc'a
 *
 * @param file plik zbroi
 * @param ilosc ile egzemplarzy dodac
 *
 * UWAGA: Wywo�anie dwa razy add_weapon z tym samym plikiem doda nam tylko jeden egzemplarz! Musimy dodawa�
 *        z argumentem ilo�� a�eby uzyska� wi�cej egzemplarzy(By� mo�e uda si� rozwik�ac ten problem).
 * @param ob zwraca obiekt broni.
 */
public varargs object
add_armour(string file, int ilosc=1)
{
    object *armours = ({});

    if (!strlen(file) || !ilosc)
        return 0;

    seteuid(getuid(this_object()));

    while(ilosc-- > 0)
        armours += ({ clone_object(file) });

    armours = filter(armours, objectp);

    if (!sizeof(armours))
        return 0;

    arm += armours;

    return armours[0];
}

/**
 * Zwraca wszystkie zbroje, kt�re zosta�y do gracza dodane
 * za pomoc� funkcji add_armour().
 *
 * @return Zbroje.
 */
public object *query_armours()
{
    return arm;
}

/**
 * Zwraca wszystkie bronie, kt�re zosta�y do gracza dodane
 * funkcji add_weapon().
 *
 * @return Bronie.
 */
public object *query_weapons()
{
    return wep;
}

#if 0
public string query_add_things_auto_load(string arg)
{
    string *armours = map(arm, &operator([])(,0) @ &explode(,"#") @ &file_name());
    string *weapons = map(wep, &operator([])(,0) @ &explode(,"#") @ &file_name());

    return "#<AAT#" + implode(armours, "$") + "#" + implode(weapons, "$") + "#TAA>#";
}

public string init_add_things_auto_load(string arg)
{
    string *armours, *weapons, pre, post, arms, weps;

    if(sscanf(arg, "%s#<AAT#%s#%s#TAA>#%s", pre, arms, weps, post) != 4)
        return arg;

    //Je�li npc ma zbroje to j� linkujemy
    int i;
    object *ai = all_inventory(TO);
    foreach(string a : arms)
    {
        if((i = member_array(a, map(ai, &operator([])(,0) @ &explode(,"#") @ &file())) != -1))
            arm += ({ai[i]});
        else
            arm += add_armour(a);
    }

    foreach(string w : weps)
    {
        if((i = member_array(a, map(ai, &operator([])(,0) @ &explode(,"#") @ &file())) != -1))
            weps += ({ai[i]});
        else
            wesp += add_weapon(a);
    }

    armours = explode(arms, "$");
    weapons = explode(weps, "$");

    map(armours, add_armour());
    map(weapons, add_weapon());

    return pre + post;
}
#endif
