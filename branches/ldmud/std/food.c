/**
 * \file /std/food.c
 *
 *  This is the standard object used for any form of eatable things
 *
 *  Standardowe u�ycie /std/food.c
 * @example
 *       inherit "/std/food";
 *
 *       void
 *       create_food()
 *       {
 *           set_amount(amount_in_gram);
 *           set_name("name of food");
 *           set_short(...);
 *           set_long(....);
 *       }
 *
 *   If you want any special effect in your food you can define
 *   special_effect() which is called when eating it.
 *   The number of items you ate is passed as argument to special_effect();
 *
 *   Food may auto_load and auto_loading is added by default, but only if the
 *   the food has been coded in a particular object and not cloned from
 *   this file directly and externally set up.
 */

#pragma save_binary
#pragma strict_types

inherit "/std/heap";

#include <object_types.h>
#include <cmdparse.h>
#include <composite.h>
#include <files.h>
#include <macros.h>
#include <stdproperties.h>

/*
 * Global variables. They are not saved.
 */
static  int     food_amount;
static  object *gFail;
static  int     decay_time, decayed, decay_alarm;
static  string *decay_adj;
string          loaded_by;

/*
 * Prototypes.
 */
public  int     eat_it(string str);
public  int     divide_it(string str);
public  void    decay();
public  void    total_decay();

/*
 * Function name: create_food
 * Description  : This function is the constructor. You should redefine
 *                it to create your own food object.
 */
public void
create_food()
{
    ustaw_nazwe("posi�ek");
}

/*
 * Function name: create_heap
 * Description  : The heap constructor. You should not redefine this
 *                function, but use create_food() instead. It makes the
 *                food object have a default size of 1 item.
 */
public nomask void
create_heap()
{
    set_heap_size(1);
    add_prop(OBJ_M_NO_SELL, "Jedzenie nie mo^ze by^c ponownie sprzedawane.\n");

    decay_adj = ({ "zepsuty", "zepsuci" });

    set_type(O_JEDZENIE);

    loaded_by = calling_function(-2);
    create_food();
}

/*
 * Function name: set_long
 * Description  : Set the long description. If no unique identifier is
 *                set, the long description is used.
 * Arguments    : string long - the long description.
 */
public void
set_long(string long)
{
    ::set_long(long);

    if (!query_prop(HEAP_S_UNIQUE_ID))
        add_prop(HEAP_S_UNIQUE_ID, food_amount + "|" + long);
}

/*
 * Function name:	set_amount
 * Description:		sets the amount of food in this food (in grams)
 * Arguments:		a: The amount of food
 */
public void
set_amount(int a)
{
    food_amount = a;

    if(!query_prop(OBJ_I_VOLUME))
        add_prop(OBJ_I_VOLUME, a / 10 + random(a / 5) + 1);

    add_prop(OBJ_I_WEIGHT, a);
    if (query_prop(HEAP_S_UNIQUE_ID))
        add_prop(HEAP_S_UNIQUE_ID, a + "|" + explode(query_prop(HEAP_S_UNIQUE_ID), "|")[1]);
}

/*
 * Function name:	query_amount
 * Description:		Gives the amount of food in this food
 * Returns:		Amount as int (in grams)
 */
public int
query_amount() { return food_amount; }

public void
set_decay_time(int time)
{
    if (time < 0)
        time = 0;

    decay_time = time;
}

public int
query_decay_time()
{
     return decay_time;
}

public void
set_decay_adj(string *adjs)
{
    if (sizeof(adjs) != 2)
        return;

    decay_adj = adjs + ({});
}

public string *
query_decay_adj()
{
    return decay_adj + ({});
}

public void
stop_decay()
{
    if (!decay_alarm || !decay_time || decayed)
        return ;

    decay_time = ftoi(get_alarm(decay_alarm)[2] / 60.0);
    remove_alarm(decay_alarm);
    decay_alarm = 0;
}

public void
start_decay()
{
    if (decay_alarm || !decay_time || decayed)
        return ;

    decay_alarm = set_alarm(itof(decay_time * 60), 0.0, &decay());

}

public void
decay()
{
    remove_alarm(decay_alarm);
    decay_alarm = 0;
    decayed = 1;
    decay_time = 10;

    dodaj_przym(decay_adj[0], decay_adj[1]);

    usun_shorty();
}

public int
query_decayed()
{
    return decayed;
}


public void
init()
{
    ::init();

    add_action(eat_it, "zjedz");
    add_action(divide_it, "podziel");
}

/*
 * Function name: consume_text
 * Description:   Here the eat message is written. You may redefine it if
 *		  you wish.
 * Arguments:     arr - Objects being consumed
 *		  vb  - The verb player used to consume them
 */
void
consume_text(object *arr, string vb)
{
    string str;

    write("Zjadasz " + (str = COMPOSITE_DEAD(arr, PL_BIE)) + ".\n");
    saybb(QCIMIE(TP, PL_MIA) + " zjada " + str + ".\n");
}

/*
 * Function name:	eat_it
 * Description:		Eats the objects described as parameter to 'eat'. It
 *			uses command parsing to find which objects to eat.
 * Arguments:		str: The trailing command after 'eat ...'
 * Returns:		True if command successfull
 */
public int
eat_it(string str)
{
    object 	*a, *foods;
    int		il;
    string str2, vb;

    notify_fail("Co chcesz zje^s^c?\n");

    /* This food has already been eaten or already been tried, so we won't
     * have to test again.
     */
    if (query_prop(TEMP_OBJ_ABOUT_TO_DESTRUCT) ||
	TP->query_prop(TEMP_STDFOOD_CHECKED))
    {
	return 0;
    }

    gFail = ({ });
    vb = query_verb();

    a = CMDPARSE_ONE_ITEM(str, "eat_one_thing", "eat_access");
    if (sizeof(a) > 0)
    {
	consume_text(a, vb);
	for (il = 0; il < sizeof(a); il++)
	{
	    a[il]->special_effect(a[il]->num_heap());
	    a[il]->destruct_object();
	}
	return 1;
    }
    else
    {
	TP->add_prop(TEMP_STDFOOD_CHECKED, 1);
	set_alarm(1.0, 0.0,
	    &(TP)->remove_prop(TEMP_STDFOOD_CHECKED));
	if (sizeof(gFail))
	    notify_fail("@@eat_fail:" + file_name(this_object()));
	return 0;
    }
}

string
eat_fail()
{
    string str = "";

/*
    str = "Probujesz zjesc " + COMPOSITE_DEAD(gFail, PL_BIE) + ", ale "+
        "juz nie mozesz.\n";
    saybb(QCIMIE(TP, PL_MIA) + " probuje zjesc " + QCOMPDEAD(PL_BIE) +
	    ", ale juz nie moze.\n");
*/
    TP->remove_prop(TEMP_STDFOOD_CHECKED);
    return str;
}

int
eat_access(object ob)
{
    if ((environment(ob) == TP) &&
	(function_exists("create_heap", ob) == FOOD_OBJECT) &&
	(ob->query_short()))
	return 1;
    else
	return 0;
}

int
eat_one_thing(object ob)
{
    int am, num, i;

    am = (int) ob->query_amount();
    num = (int) ob->num_heap();

    for (i = 1; i <= num; i++)
    {
        if (ob->query_decayed())
        {
	    if (i == 1)
	    {
		ob->split_heap(1);
		notify_fail("Nie mo^zesz zje^s^c " + ob->short(PL_DOP) +
		    ", gdy^z jest " + oblicz_przym(decay_adj[0],
		    decay_adj[1], PL_MIA, ob->query_rodzaj(), 0) + ".\n");
		gFail += ({ ob });
	    	return 0;
	    }
	    ob->split_heap(i - 1);
	    return 1;
        }

        if (!TP->eat_food(am))
        {
	    if (i == 1)
	    {
		ob->split_heap(1);
/*
		write(capitalize(ob->short()) + " to troche za duzo dla "+
		    "twojego zoladka.\n");
*/
		write("Chyba nie dasz rady zje^s^c " + ob->short(PL_DOP) +
		    ".\n");
		gFail += ({ ob });
	    	return 0;
	    }
	    ob->split_heap(i - 1);
	    return 1;
	}
    }

    return 1;
}

/*
 * Function name: divide_text
 * Description:   Here the divide message is written. You may redefine it if
 *		  you wish.
 * Arguments:     arr - Objects being divided
 *		  vb  - The verb player used to divide them
 */
void
divide_text(object *arr, string vb)
{
    string str;

    write("Dzielisz " + (str = COMPOSITE_DEAD(arr, PL_BIE)) + " na dwie cz^e^sci.\n");
    saybb(QCIMIE(TP, PL_MIA) + " dzieli " + str + " na dwie cz^e^sci.\n");
}

/*
 * Function name:	divide_it
 * Description:		Divides the objects described as parameter to 'divide'. It
 *			uses command parsing to find which objects to divide.
 * Arguments:		str: The trailing command after 'divide ...'
 * Returns:		True if command successfull
 */
public int
divide_it(string str)
{
    object 	*a, *foods;
    int		il, num_heap;
    string str2, vb;

    notify_fail("Co chcesz podzieli^c?\n");

    /* This food has already been divided or already been tried, so we won't
     * have to test again.
     */
    if (query_prop(TEMP_OBJ_ABOUT_TO_DESTRUCT) ||
	TP->query_prop(TEMP_STDFOOD_CHECKED))
    {
	return 0;
    }

    gFail = ({ });
    vb = query_verb();

    a = CMDPARSE_ONE_ITEM(str, "divide_one_thing", "divide_access");
//    a = CMDPARSE_ONE_ITEM(str, "", "divide_access");
//    if (sizeof(a) > 1)
//    {
//	write("Nie mo^esz dzieli^c wi^ecej ni^z jednej rzeczy naraz.\n");
//	return 1;
//    }

    if (sizeof(a) > 0)
    {
	divide_text(a, vb);
	for (il = 0; il < sizeof(a); il++)
	{
            int num_heap = a[il]->num_heap(); //to po to jest, �eby dzieli�
            //wszystko, jak podajemy '2 chleby', to musi zrobi� to dwukrotnie
            //i po to to w�a�nie jest. :P Verk.
            while(num_heap-- >= 1)
            {
	    object ob1 = clone_object(FOOD_OBJECT), ob2 = clone_object(FOOD_OBJECT);
	    string dop = a[il]->query_nazwa(PL_DOP), sdop = a[il]->singular_short(PL_DOP);
	    mixed przym = a[il]->query_przymiotniki();
	    int j;

	    if (dop[0..6] == "kawa^lka")
	    {
		ob1->ustaw_nazwe(({a[il]->query_nazwa(PL_MIA),
		    a[il]->query_nazwa(PL_DOP), a[il]->query_nazwa(PL_CEL),
		    a[il]->query_nazwa(PL_BIE), a[il]->query_nazwa(PL_NAR),
		    a[il]->query_nazwa(PL_NAR)}), ({a[il]->query_pnazwa(PL_MIA),
		    a[il]->query_pnazwa(PL_DOP), a[il]->query_pnazwa(PL_CEL),
		    a[il]->query_pnazwa(PL_BIE), a[il]->query_pnazwa(PL_NAR),
		    a[il]->query_pnazwa(PL_MIE)}), a[il]->query_rodzaj());
		ob1->ustaw_shorty(({a[il]->singular_short(PL_MIA),
		    a[il]->singular_short(PL_DOP), a[il]->singular_short(PL_CEL),
		    a[il]->singular_short(PL_BIE), a[il]->singular_short(PL_NAR),
		    a[il]->singular_short(PL_NAR)}), ({a[il]->plural_short(PL_MIA),
		    a[il]->plural_short(PL_DOP), a[il]->plural_short(PL_CEL),
		    a[il]->plural_short(PL_BIE), a[il]->plural_short(PL_NAR),
		    a[il]->plural_short(PL_MIE)}), PL_MESKI_NOS_NZYW);
	    }
	    else
	    {
		ob1->ustaw_nazwe(({"kawa^lek " + dop, "kawa^lka " + dop,
		    "kawa^lkowi " + dop, "kawa^lek " + dop, "kawa^lkiem " + dop,
		    "kawa^lku " + dop}), ({"kawa^lki " + dop, "kawa^lk^ow " + dop,
		    "kawa^lkom " + dop, "kawa^lki " + dop, "kawa^lkami " + dop,
		    "kawa^lkach " + dop}), PL_MESKI_NOS_NZYW);
//		ob1->dodaj_nazwy(({a[il]->query_nazwa(PL_MIA),
//		    a[il]->query_nazwa(PL_DOP), a[il]->query_nazwa(PL_CEL),
//		    a[il]->query_nazwa(PL_BIE), a[il]->query_nazwa(PL_NAR),
//		    a[il]->query_nazwa(PL_NAR)}), ({a[il]->query_pnazwa(PL_MIA),
//		    a[il]->query_pnazwa(PL_DOP), a[il]->query_pnazwa(PL_CEL),
//		    a[il]->query_pnazwa(PL_BIE), a[il]->query_pnazwa(PL_NAR),
//		    a[il]->query_pnazwa(PL_MIE)}), a[il]->query_rodzaj());
		ob1->ustaw_shorty(({"kawa^lek " + sdop, "kawa^lka " + sdop,
		    "kawa^lkowi " + sdop, "kawa^lek " + sdop, "kawa^lkiem " + sdop,
		    "kawa^lku " + sdop}), ({"kawa^lki " + sdop, "kawa^lk^ow " + sdop,
		    "kawa^lkom " + sdop, "kawa^lki " + sdop, "kawa^lkami " + sdop,
		    "kawa^lkach " + sdop}), PL_MESKI_NOS_NZYW);
	    }
	    for (j = 0; j < sizeof(przym[0]); ++j)
		ob1->dodaj_przym(przym[0][j], przym[1][j]);
	    ob1->set_amount(a[il]->query_amount()/2);
	    ob1->set_long(a[il]->long());
	    ob1->add_prop(HEAP_I_UNIT_VOLUME, a[il]->query_prop(HEAP_I_UNIT_VOLUME)/2);
	    ob1->add_prop(HEAP_I_UNIT_WEIGHT, a[il]->query_prop(HEAP_I_UNIT_WEIGHT)/2);
	    ob1->add_prop(HEAP_I_UNIT_VALUE, a[il]->query_prop(HEAP_I_UNIT_VALUE)/2);
	    ob1->set_decay_time(a[il]->query_decay_time());
	    ob1->set_decay_adj(a[il]->query_decay_adj());
	    if (dop[0..6] == "kawa^lka")
	    {
		ob2->ustaw_nazwe(({a[il]->query_nazwa(PL_MIA),
		    a[il]->query_nazwa(PL_DOP), a[il]->query_nazwa(PL_CEL),
		    a[il]->query_nazwa(PL_BIE), a[il]->query_nazwa(PL_NAR),
		    a[il]->query_nazwa(PL_NAR)}), ({a[il]->query_pnazwa(PL_MIA),
		    a[il]->query_pnazwa(PL_DOP), a[il]->query_pnazwa(PL_CEL),
		    a[il]->query_pnazwa(PL_BIE), a[il]->query_pnazwa(PL_NAR),
		    a[il]->query_pnazwa(PL_MIE)}), a[il]->query_rodzaj());
		ob2->ustaw_shorty(({a[il]->singular_short(PL_MIA),
		    a[il]->singular_short(PL_DOP), a[il]->singular_short(PL_CEL),
		    a[il]->singular_short(PL_BIE), a[il]->singular_short(PL_NAR),
		    a[il]->singular_short(PL_NAR)}), ({a[il]->plural_short(PL_MIA),
		    a[il]->plural_short(PL_DOP), a[il]->plural_short(PL_CEL),
		    a[il]->plural_short(PL_BIE), a[il]->plural_short(PL_NAR),
		    a[il]->plural_short(PL_MIE)}), PL_MESKI_NOS_NZYW);
	    }
	    else
	    {
		ob2->ustaw_nazwe(({"kawa^lek " + dop, "kawa^lka " + dop,
		    "kawa^lkowi " + dop, "kawa^lek " + dop, "kawa^lkiem " + dop,
		    "kawa^lku " + dop}), ({"kawa^lki " + dop, "kawa^lk^ow " + dop,
		    "kawa^lkom " + dop, "kawa^lki " + dop, "kawa^lkami " + dop,
		    "kawa^lkach " + dop}), PL_MESKI_NOS_NZYW);
//		ob2->dodaj_nazwy(({a[il]->query_nazwa(PL_MIA),
//		    a[il]->query_nazwa(PL_DOP), a[il]->query_nazwa(PL_CEL),
//		    a[il]->query_nazwa(PL_BIE), a[il]->query_nazwa(PL_NAR),
//		    a[il]->query_nazwa(PL_NAR)}), ({a[il]->query_pnazwa(PL_MIA),
//		    a[il]->query_pnazwa(PL_DOP), a[il]->query_pnazwa(PL_CEL),
//		    a[il]->query_pnazwa(PL_BIE), a[il]->query_pnazwa(PL_NAR),
//		    a[il]->query_pnazwa(PL_MIE)}), a[il]->query_rodzaj());
		ob2->ustaw_shorty(({"kawa^lek " + sdop, "kawa^lka " + sdop,
		    "kawa^lkowi " + sdop, "kawa^lek " + sdop, "kawa^lkiem " + sdop,
		    "kawa^lku " + sdop}), ({"kawa^lki " + sdop, "kawa^lk^ow " + sdop,
		    "kawa^lkom " + sdop, "kawa^lki " + sdop, "kawa^lkami " + sdop,
		    "kawa^lkach " + sdop}), PL_MESKI_NOS_NZYW);
	    }
	    for (j = 0; j < sizeof(przym[0]); ++j)
		ob2->dodaj_przym(przym[0][j], przym[1][j]);
	    ob2->set_amount(a[il]->query_amount()/2);
	    ob2->set_long(a[il]->long());
	    ob2->add_prop(HEAP_I_UNIT_VOLUME, a[il]->query_prop(HEAP_I_UNIT_VOLUME)/2);
	    ob2->add_prop(HEAP_I_UNIT_WEIGHT, a[il]->query_prop(HEAP_I_UNIT_WEIGHT)/2);
	    ob2->add_prop(HEAP_I_UNIT_VALUE, a[il]->query_prop(HEAP_I_UNIT_VALUE)/2);
	    ob2->set_decay_time(a[il]->query_decay_time());
	    ob2->set_decay_adj(a[il]->query_decay_adj());


            /*Dziwne nie..?*/
            a[il]->move(VOID_ROOM,1);
            /*Te� tak my�l�, ale jest to taki trik, zapobiegaj�cy
            bugom typu "przyci�nij enter to ci si� rozmno�� kawa�ki chleba"
            Bug polega� na tym, �e heap chleba dzielonego stawa� si� ju� heapem
            tych dw�ch nowych kawa�k�w, a powstawa� nowy obiekt na u�amek sekundy
            kt�ry by� tym starym chlebem. (nie mog�em znale��
            gdzie i w jaki spos�b on jest tworzony - na pewno to sprawka durnego
            kodu z /std/heap.c) No i ten nowy obiekt oczywi�cie nie dziedziczy�
            propa ABOUT_TO_DESTRUCT, a �e istnia� przez 0.1 sekundy to da�o rad�
            go dzieli� jeszcze przez ten czas.
            C�, zdaje si�, �e musia�bym stworzy� nowy std heap�w, �eby �adniej
            to rozwi�za�, tymczasem borym lasem nie widz� sensu w przepisywaniu
            dzia�aj�cego kodu (dobra, wiem, dzia�a ledwo ledwo, ale jednak!)
            priorytety, priorytety...ech. No. A w tym VOID_ROOM to dzielone jedzonko
            i tak istnieje tylko 0.1 sekundy ;p
            Mam nadziej�, �e pozby�em si� tych paskudnych bug�w z jedzeniem raz
            na zawsze.
                                                Vera. Wednesday, May 28 2008 */


	    a[il]->destruct_object();
	    ob1->set_heap_num(1);
	    ob1->move(TP, 1);
	    ob1->start_decay();
	    ob2->set_heap_num(1);
	    ob2->move(TP, 1);
	    ob2->start_decay();

            }
	}

	return 1;
    }
    else
    {
	TP->add_prop(TEMP_STDFOOD_CHECKED, 1);
	set_alarm(1.0, 0.0,
	    &(TP)->remove_prop(TEMP_STDFOOD_CHECKED));
	if (sizeof(gFail))
	    notify_fail("@@divide_fail:" + file_name(this_object()));
	return 0;
    }
}

string
divide_fail()
{
    string str = "";

    TP->remove_prop(TEMP_STDFOOD_CHECKED);
    return str;
}

int
divide_access(object ob)
{
    if ((environment(ob) == TP) &&
	(function_exists("create_heap", ob) == FOOD_OBJECT) &&
	(ob->query_short()))
	return 1;
    else
	return 0;
}

int
divide_one_thing(object ob)
{
    int am, num, i;

    am = (int) ob->query_amount();
    num = (int) ob->num_heap();

    for (i = 1; i <= num; i++)
    {
        if (ob->query_prop(OBJ_I_WEIGHT) < 50)
        {
	    if (i == 1)
	    {
		ob->split_heap(1);
		notify_fail(capitalize(ob->short(PL_MIA)) + " " +
		    koncowka("jest", "jest", "jest", "s^a", "s^a") + " zbyt " +
		    koncowka("ma^ly", "ma^la", "ma^le", "mali", "ma^le") + ", ^zeby " +
		    koncowka("go", "j^a", "je", "ich", "je") + " podzieli^c.\n");
		gFail += ({ ob });
	    	return 0;
	    }
	    ob->split_heap(i - 1);
	    return 1;
        }
    }

    return 1;
}

void
config_split(int new_num,mixed orig)
{
    ::config_split(new_num, orig);
    set_amount(orig->query_amount());
}

public void
enter_env(object dest, object from)
{
    ::enter_env(dest, from);

    if (!decayed)
        return ;

    if (function_exists("create_container", dest) == ROOM_OBJECT)
    {
        if (decay_alarm)
            return ;

        decay_alarm = set_alarm(itof(decay_time), 0.0, &total_decay());
    }
    else if (decay_alarm)
    {
        decay_time = ftoi(get_alarm(decay_alarm)[2]);
        remove_alarm(decay_alarm);
        decay_alarm = 0;
    }
}

public void
total_decay()
{
    tell_roombb(environment(this_object()), capitalize(short()) + " " +
        "rozk^lada" + (num_heap() > 1 ? "j^a" : "") + " si^e zupe^lnie.\n",
        ({}), this_object());

    remove_object();
}

void
destruct_object()
{
    if (leave_behind > 0)
    {
        set_heap_size(leave_behind);
    }
    else
    {
        add_prop(TEMP_OBJ_ABOUT_TO_DESTRUCT, 1);
        set_alarm(1.0, 0.0, remove_object);
    }
}

/*
 * Function name: stat_object
 * Description:   This function is called when a wizard wants to get more
 *                information about an object.
 * Returns:       str - The string to write..
 */
string
stat_object()
{
    return ::stat_object() + "Jedzenie: " + food_amount + " (gram^ow).\n";
}

public string
init_food_arg(string str)
{
#if 0
    int hiuv, hiuw, hiuvl, am, hs, dt;
    string s1, dl, names, przyms, dec, s2, patt;
    if(sscanf(str, "%s#FOOD#%s#FOOD#%s", s1, patt, s2) != 3)
        return str;

    if(sscanf(patt, "%d | %d | %d | %d | %d | %d | %d | %s | %s | %s | %s | %s",
        hs, am, hiuw, hiuv, hiuvl, dt, decayed, dl, loaded_by, names, przyms, dec) != 12)
    {
        return str;
    }

    set_long(dl);
    set_amount(am);
    set_heap_size(hs);
    set_decay_time(dt);
    add_prop(HEAP_I_UNIT_WEIGHT, hiuw);
    add_prop(HEAP_I_UNIT_VOLUME, hiuv);
    add_prop(HEAP_I_UNIT_VALUE, hiuvl);

    if(stringp(names))
    {
        int r;
        string *p, *m;
        mixed stmp;

        stmp = explode(names, "<>");
        p = explode((string)stmp[0], "&");
        if(!intp(stmp[1]))
        {
            m = explode((string)stmp[1], "&");
            r = (int)stmp[2];
            ustaw_nazwe(p, m, r);
        }
        else
        {
            r = (int)stmp[1];
            ustaw_nazwe(p, r);
        }
    }

    if(stringp(dec))
    {
        string *stmp;
        stmp = explode(dec, "&");
        decay_adj = ({stmp[0], stmp[1]});

        if(decayed)
            dodaj_przym(stmp[0], stmp[1]);
    }

    if(stringp(przyms))
    {
        string *p, *m, *stmp;
        stmp = explode(przyms, "<>");

        p = explode(stmp[0], "&");
        m = explode(stmp[1], "&");

        if(sizeof(p) == sizeof(m))
            for(int i=0;i<sizeof(p);i++)
                if(p[i] != decay_adj[0])
                    dodaj_przym(p[i], m[i]);
    }
    odmien_short();
    odmien_plural_short();

    return s1 + s2;
#else
    return str;
#endif
}

public string
query_auto_load()
{
#if 0
    //�adujemy tylko te kt�re wysz�y z karczmy.
    if(loaded_by == "food_na_wynos" || MASTER != FOOD_OBJECT)
    {
        return ::query_auto_load() +
            "#FOOD#" + num_heap() + " | " + query_amount() + " | " + query_prop(HEAP_I_UNIT_WEIGHT) + " | " +
            query_prop(HEAP_I_UNIT_VOLUME) + " | " + query_prop(HEAP_I_UNIT_VALUE) + " | " +
            query_decay_time() + " | " + query_decayed() + " | " + query_long() + " | " + loaded_by + " | " +
            query_nazwa(0) + "&" + query_nazwa(1) + "&" + query_nazwa(2) + "&" +
            query_nazwa(3) + "&" + query_nazwa(4) + "&" + query_nazwa(5) + "<>" +
            (query_tylko_mn() ? "" :
            query_pnazwa(0) + "&" + query_pnazwa(1) + "&" + query_pnazwa(2) + "&" +
            query_pnazwa(3) + "&" + query_pnazwa(4) + "&" + query_pnazwa(5) + "<>") +
            query_rodzaj() + " | " + implode(query_przym(1), "&") + "<>" + implode(query_pprzym(1), "&") +
            (sizeof(decay_adj) == 2 ? " | " + decay_adj[0] + "&" + decay_adj[1] : "") + "#FOOD#";
    }
    else
        return 0;
#else
    return 0; //KEEPER chce coby si� nie �adowa�o.
#endif
}

public string
init_arg(string arg)
{
#if 0
    return init_food_arg(::init_arg(arg));
#else
    return arg;
#endif

}
