/* Mala lokacja na drzewie :)
      Bedzie mozna stad przeskoczyc na mur.
                                            Lil */

inherit "/std/room";

#include <stdproperties.h>
#include <macros.h>
#include <pl.h>

object drzewo;

void
create_room() 
{
	set_short("Na ga��zi drzewa");
	set_long("Ga��� jest gruba, lecz i tak jest to do�� niestabilne miejsce. "+
	         "Ga��ziane ramiona si�gaj� dosy� daleko, lecz mo�na przebywa� " +
             "tylko przy samym konarze, gdzie nie zmie�ci si� zbyt wiele os�b.\n");
	add_sit("na ga��zi","na ga��zi","z ga��zi",2);
        add_prop(CONT_I_MAX_VOLUME, 200000);
	add_prop(ROOM_I_INSIDE,0);
	add_prop(ROOM_I_TYPE,ROOM_TREE);

}

void
set_drzewo(object ob)
{
    int ind;
    mixed exits;

    room_exits = 0;
    add_exit(MASTER_OB(ENV(ob)), ({ "d", "na d^o^l",
            ({"zsuwa si� z pnia "+ob->short(PL_DOP), "zsuwa si� z pnia "+ob->short(PL_DOP), 
             "ostro�nie zsuwa si� z pnia "+ob->short(PL_DOP)}) })
            , 0, 20);
    set_short("Na ga��zi " + ob->query_nazwa(PL_DOP));
    drzewo = ob;

    exits = ENV(drzewo)->query_exit();

    if (!sizeof(exits)) 
        return;
    
    for (ind = 0; ind < sizeof(exits); ind += 3) {
        if ((exits[ind+1] ~= "p�noc") ||
                (exits[ind+1] ~= "p�nocny-wsch�d") ||
                (exits[ind+1] ~= "wsch�d") ||
                (exits[ind+1] ~= "po�udniowy-wsch�d") ||
                (exits[ind+1] ~= "po�udnie") ||
                (exits[ind+1] ~= "po�udniowy-zach�d") ||
                (exits[ind+1] ~= "zach�d") ||
                (exits[ind+1] ~= "p�nocny-zach�d")) {
            add_exit(exits[ind], exits[ind+1], 1, 1, 1);
                    }
    }
}

object
query_drzewo()
{
    return drzewo;
}
