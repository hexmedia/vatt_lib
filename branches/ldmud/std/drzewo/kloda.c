inherit "/std/object.c";

#include <cmdparse.h>
#include <exp.h>
#include <macros.h>
#include <math.h>
#include <stdproperties.h>
#include <ss_types.h>
#include <object_types.h>

/* Kto przecina klode */
#define LOG_O_PRZETNIJ "_log_o_przetnij"

#define DBG(x) find_player("root")->catch_msg(x+"\n")

float   cena = 2.0;
float   gestosc=0.6;
object  kloda_owner;

void dodaj_przym_wagi();
int przetnij(string str);
object *check_wielded_weapons(object kto, int *jakie);

void create_kloda()
{
	ustaw_nazwe("kloda");
	set_long("Jest to zwyk�a, niczym nie wyr�niaj�ca si� k�oda.\n");
}

void create_object()
{
	create_kloda();
	make_me_sitable("na", "na k�odzie", "z k�ody", 3);
}

/* -------------------------- FUNKCJE QUERY & SET -------------------------- */

/**
 * Ustawia gestosc danego gatunku drzewa
 * @param f Gestosc
 */
void set_gestosc(float f)
{
	gestosc = f;
}

/**
 * Zwraca gestosc danego gatunku drzewa
 * @return Gestosc
 */
float query_gestosc()
{
	return gestosc;
}

/**
 * Ustawia cene danego gatunku drzewa.
 * @param ile Cena za 100 kg drewna danego gatunku
 */
void
set_cena(float f)
{
	cena = f;
}

/**
 * Zwraca cene za dany gatunek drzewa.
 * @return Cena za 100 kg drewna danego gatunku
 */
float query_cena()
{
	return cena;
}

/**
 * Zwraca rzeczywista cene danej klody
 * @return Cena
 */
int query_real_cena()
{
    if(intp(cena))
        cena = 1.0;

    int ret = ftoi(LINEAR_FUNC(0.0, 0.0, 130000.0, cena,
                itof(query_prop(OBJ_I_WEIGHT))));

    if(!ret)
        ret = 1;

    return ret;
}

/**
 * Ustawia wlasciciela klody, to jest osobe, ktora
 * wczesniej ociosala drzewo
 * @param ob Wlasciciel
 */
void set_kloda_owner(object ob)
{
    kloda_owner = ob;
}

/**
 * Zwraca wlasciciela klody
 * @return Wlasciciel
 */
object query_kloda_owner()
{
    return kloda_owner;
}

/* ---------------------- FUNKCJE ROZNE, POMOCNICZE ---------------------- */

void add_prop(string prop, mixed val)
{
    ::add_prop(prop, val);

    if(prop == OBJ_I_WEIGHT)
    {
        if(floatp(gestosc) && gestosc > 0.0)
            ::add_prop(OBJ_I_VOLUME, ftoi(itof(val)/gestosc));

        if(floatp(cena) && cena > 0.0)
            ::add_prop(OBJ_I_VALUE, query_real_cena());

        dodaj_przym_wagi();
    }
}

void dodaj_przym_wagi()
{
    int w = query_prop(OBJ_I_WEIGHT);

    if(w < 60000)
        dodaj_przym("lekki", "letcy");
    if(w >= 60000 && w < 320000)
        dodaj_przym("niegruby", "niegrubi");
    if(w >= 320000 && w < 600000)
        dodaj_przym("zwyczajny", "zwyczajni");
    if(w >= 600000 && w < 900000)
        dodaj_przym("masywny", "masywni");
    if(w >= 900000 && w < 1200000)
        dodaj_przym("ci�ki", "ci�cy");
    if(w >= 1200000)
        dodaj_przym("ogromny", "ogromni");

    odmien_short();
    odmien_plural_short();
}

object *check_wielded_weapons(object kto, int *jakie)
{
    object *wielded = kto->subinventory("wielded");
    object *ret = ({ });

    if(!sizeof(wielded))
        return 0;

    if(member_array(wielded[0]->query_type(), jakie) != -1)
        ret += ({wielded[0]});

    if(sizeof(wielded) == 2)
    {
        if(member_array(wielded[1]->query_type(), jakie) != -1)
            ret += ({wielded[1]});
    }

    if(!sizeof(ret))
        return 0;

    return ret;
}

void remove_owner_alarm()
{
    set_alarm(900.0, 0.0, "set_kloda_owner", 0);
}

int
przetnij_stop()
{
    remove_prop(LOG_O_PRZETNIJ);

    return 0;
}

void
przetnij_finish()
{
    query_prop(LOG_O_PRZETNIJ)->catch_msg("Uda�o ci si� przeci�� " + short(PL_BIE)+".\n");

    object *klody = allocate(2);
    int masa= query_prop(OBJ_I_WEIGHT);
    int random_waga = random(masa/10);

    klody[0] = clone_object(MASTER_OB(TO));
    klody[1] = clone_object(MASTER_OB(TO));

    klody->set_gestosc(gestosc);
    klody->set_cena(query_cena());
    klody->set_kloda_owner(query_kloda_owner());
    klody->remove_owner_alarm();

    klody[0]->add_prop(OBJ_I_WEIGHT, (masa/2)-random_waga);
    klody[1]->add_prop(OBJ_I_WEIGHT, (masa/2)+random_waga);

    klody->move(ENV(TO));

    TP->increase_ss(0, EXP_SCINANIE_DRZEW_PRZECINANIE_STR);
    TP->increase_ss(SS_WOODCUTTING, EXP_SCINANIE_DRZEW_PRZECINANIE_WOODCUTTING);

    remove_prop(LOG_O_PRZETNIJ);

    remove_object();
}

/* ---------------------- FUNKCJE OBSLUGUJACE KOMENDY ---------------------- */

int przetnij(string str)
{
    notify_fail("Co chcesz przeci��?\n");

    if(!str)
        return 0;

    object obj;
    if(!parse_command(lower_case(str), environment(TP) , " %o:"+PL_BIE, obj))
        return 0;

    if(obj != TO)
        return 0;

    if(ENV(TO) == TP)
    {
        notify_fail("�atwiej by�oby najpierw j� od�o�y�.\n");
        return 0;
    }

    if(query_prop(OBJ_I_WEIGHT) < 10000)
    {
        notify_fail("Nie mo�esz jej przeci�� na jeszcze mniejsze cz�ci.\n");
        return 0;
    }

    object *tools = check_wielded_weapons(TP, ({2097152}));

    if(!tools)
    {
        notify_fail("Aby przeci�� k�od� potrzebujesz pi�y.\n");
        return 0;
    }

    if(!tools[0]->query_pila())
    {
        if(sizeof(tools) == 2)
        {
            if(!tools[1]->query_pila())
            {
                notify_fail("Aby przeci�� k�od� potrzebujesz pi�y.\n");
                return 0;
            }
        }
        else
        {
            notify_fail("Aby przeci�� k�od� potrzebujesz pi�y.\n");
            return 0;
        }
    }

    if(objectp(query_prop(LOG_O_PRZETNIJ)))
    {
        notify_fail("Kto� ju� j� przecina!\n");
        return 0;
    }

    TP->catch_msg("Zabierasz si� za przecinanie "+TO->short(PL_DOP)+".\n");
    saybb(QCIMIE(TP, PL_MIA)+" zabiera si� za przecinanie "+TO->short(PL_DOP)+".\n");

    object paraliz = clone_object("/std/paralyze");
    paraliz->set_name("scinka_paraliz_przetnij");
    paraliz->set_remove_time(random(12)+10);

    paraliz->set_stop_verb("przesta�");
    paraliz->set_stop_message("Przestajesz przecina� "+short(PL_BIE)+".\n");
    paraliz->set_fail_message("Teraz przecinasz k�od�, aby zrobi� co� innego wystarczy przesta�.\n");
    paraliz->set_stop_fun("przetnij_stop");
    paraliz->set_stop_object(TO);

    paraliz->set_finish_fun("przetnij_finish");
    paraliz->set_finish_object(TO);

    paraliz->move(TP);

    add_prop(LOG_O_PRZETNIJ, TP);

    return 1;
}

init()
{
    ::init();
    add_action(&przetnij(), "przetnij");
}

void stat()
{
    DBG("Cena="+ftoa(cena));
    DBG("Real cena="+query_real_cena());
    DBG("Value="+query_prop(OBJ_I_VALUE));
    DBG("Waga="+query_prop(OBJ_I_WEIGHT));
}


