/*
 * player/cmd_sec.c
 *
 * This is a subpart of player_sec.c
 *
 * Some standard commands that should always exist are defined here.
 * This is also the place for the quicktyper command hook.
 */

#include <files.h>
#include <macros.h>
#include <std.h>
#include <stdproperties.h>


//filter funs potrzebne tylko do koniec_paralizu_wiec_spogladamy() V.
#include <filter_funs.h>

/*
 * Prototypes.
 */
public nomask void save_me();
nomask int quit(string str);
public int save_character(string str);
static nomask int change_password(string str);

/*
 * Global variables, they are static and will not be saved.
 */
static int save_alarm;           /* The id of the autosave-alarm */
static private string password2; /* Used when someone changes his password. */

//dla usprawnienia usuwania przymiotnika 'zamy�lony' - po co ci�gle go sprawdza�
//tak b�dzie dzia�a� l�ej i szybciej. V.
int zamyslony = 0;

/*
 * Function name: start_autosave
 * Description  : Call this function to start autosaving. Only works for
 *                wizards.
 */
static nomask void
start_autosave()
{
    /* Start autosaving if the player is not a wizard. */
    if (!query_wiz_level())
    {
        remove_alarm(save_alarm);

        save_alarm = set_alarm(300.0, 0.0, &save_me());
    }
}

/*
 * Function name: stop_autosave
 * Description  : Call this function to stop autosaving.
 */
static nomask void
stop_autosave()
{
    remove_alarm(save_alarm);
    save_alarm = 0;
}

void
dodaj_zamyslony()
{
    zamyslony = 1;
}

public int
usun_przym_zamyslony(string str)
{
    if(zamyslony)
    {
        zamyslony = 0;
        TP->check_if_idle();
    }
    return 0;
}

/*
 * Function name: cmd_sec_reset
 * Description  : When the player logs in, this function is called to link
 *                some essential commands to him.
 */
static nomask void
cmd_sec_reset()
{
    add_action(usun_przym_zamyslony, "", 1);
    add_action(quit,            "zako�cz");
    add_action(save_character,  "nagraj");
    add_action(change_password, "has�o");

    init_cmdmodify();

    /* Start autosaving. */
    start_autosave();
}

static nomask string*
recursive_auto_str(object ob)
{
    object *objects;
    string str;
    string *auto = ({ });

    if (!objectp(ob))
        return auto;

    objects = all_inventory(ob);
    int i = sizeof(objects);

    while(--i >= 0)
    {
        str = objects[i]->query_auto_load();
        // FIXME: uwzgl�dni� rzeczy, kt�rych gracz nie m�g� zabra� ze sob�
        if (strlen(str))
            auto += ({ str });
        else
            auto += recursive_auto_str(objects[i]);
    }

    return auto;
}

/*
 * Function name: compute_auto_str
 * Description  : Walk through the inventory and check all the objects for
 *                the function query_auto_load(). Constructs an array with
 *                all returned strings. query_auto_load() should return
 *                a string of the form "<file>:<argument>".
 */
static nomask void
compute_auto_str()
{
    object *objects = all_inventory(TO);
    object *worn_objects = query_worn();
    string str;
    string *auto = ({ });

    int i = sizeof(worn_objects);
    while(--i >= 0) {
        str = worn_objects[i]->query_auto_load();
        // FIXME: uwzgl�dni� rzeczy, kt�rych gracz nie m�g� zabra� ze sob�
        if (strlen(str))
        {
            auto += ({ str });
        }
        else {
            auto += recursive_auto_str(worn_objects[i]);
        }
        objects -= ({ worn_objects[i] });
    }

    i = sizeof(objects);
    while(--i >= 0)
    {
        str = objects[i]->query_auto_load();
        // FIXME: uwzgl�dni� rzeczy, kt�rych gracz nie m�g� zabra� ze sob�
        if (strlen(str))
            auto += ({ str });
        else
            auto += recursive_auto_str(objects[i]);
    }
    set_auto_load(auto);
}

/*
 * Function name: save_me
 * Description  : Save all internal variables of a character to disk.
 */
public nomask void
save_me()
{
    /* Do some queries to make certain time-dependent
     * vars are updated properly.
     */
    query_mana();
    query_old_fatigue();
    query_hp();
    query_stuffed();
    query_soaked();
    query_intoxicated();
    query_headache();
    query_age();
    compute_auto_str();
    seteuid(0);
    SECURITY->save_player();
    seteuid(getuid(this_object()));

    /* If the player is a mortal, we will restart autosave. */
    start_autosave();
}

/*
 * Function name:   save_character
 * Description:     Saves all internal variables of a character to disk
 * Returns:         Always 1
 */
public int
save_character(string str)
{
    save_me();
    write("Ju�.\n");
    return 1;
}

/*
 * Function name: quit
 * Description:	  The standard routine when quitting. You cannot quit while
 *                you are in direct combat.
 * Returns:	  1 - always.
 */
nomask int
quit(string str)
{
    object *inv, tp;
    int    index, loc, size;

    tp = this_player();
    set_this_player(this_object());

    if (str == "cierpienia")
    {
        write("W takim razie pom�cz si� jeszcze troch�!\n");
        return 1;
    }

    //Ustawiamy lokacje na kt�rej gracz si� wyologowywuje.. TELEPORTY DLA WIZ�W!!!!
    object env = ENV(TO);
    if(!(TO->query_wiz_level()))
    {
        if(env)
        {
            if(SECURITY->legal_login_location(file_name(env)))
                TO->set_last_logout_location(file_name(env));
            else
            {
                string psl = TO->query_prop(OBJ_S_LOGOUT_LOCATION);
                LOAD_ERR(psl);
                if(psl && SECURITY->legal_login_location(psl) && find_object(psl))
                    TO->set_last_logout_location(psl);
            }
        }
    }

    //Przerobi�em troche ten kawa�ek kodu, dopiero je�li apoka jest za 10
    //sekund to mo�na zako�czy� bez przeszk�d.
    //Pozatym na wszelki wypadek przenios�em save_me wy�ej... (Krun)
    save_me();
    int shutt=ARMAGEDDON->shutdown_time();

    if((!shutt || shutt > 2) && calling_function(0) != "cleanup_shutdown")
    {
        if(TO->query_attack() || (fold(map(all_inventory(ENV(TO)),
           &operator(==)(TO, ) @ &->query_attack()),
           &operator(+)(), 0) > 0))
        {
            write(set_color(TP, COLOR_BG_RED) + "Nie mo�esz "+
                "zako�czy� gry w takim po�piechu - jeste� w " +
                "trakcie walki." + clear_color() + "\n");
            return 1;
        }

        if(time() - query_prop(PLAYER_I_LAST_FIGHT) < 180)
        {
            write(set_color(TP, COLOR_BG_RED) + "Dopiero co " +
                "walczy�" + TP->koncowka("e�", "a�") +
                ", odczekaj kilka minut." + clear_color()+ "\n");
            return 1;
        }
    }

    //czemu all_inventory a nie deep? Przecie� rzeczy
    //z plecak�w ty� trzeba sprawdzi�. Vera.
    //inv = all_inventory(this_object());
    inv = deep_inventory(this_object());

    /* Only mortals drop stuff when they quit. */
    //A TO CZEMU?w takim przypadku moglibysmy nie zauwazyc bledow [VERA]
    //A to, �eby kto� jakich� g�upot w �wiecie nie pozostawia� [KRUN]
    if(!query_wiz_level() && SECURITY->query_wiz_rank(query_real_name()) <= WIZ_MAGE)
    {
        index = -1;
        size = sizeof(inv);
        while(++index < size)
        {
            if (!(stringp(inv[index]->query_auto_load())))
            {
            /* However, we only try to drop it if the player is
            * carrying it on the top level.
            */
            if (!(inv[index]->query_prop(OBJ_M_NO_DROP)))
            {
                    //sprawd�my czy nie mamy tej rzeczy np. w plecaku
                    //wtedy przenosimy si�owo na momencik do gracza. Verek
                    if(ENV(inv[index]) != TO)
                        inv[index]->move(TO,1);
                    //wiem...amatorszczyzna...Ale nie jestem pewien, czy
                    //OB_NAME wszystko wy�apie. Powinno!
                    //Jedno jest pewne: ten command z parse_command_id_list
                    //nie dzia�a� na /std/corpse ! (mimo, �e dobrze zwraca�.
                    //V.
                    if(!command("po�� " + OB_NAME(inv[index])))
                        command("po�� " + inv[index]->parse_command_id_list(PL_BIE)[0]);
                    inv[index] = 0;
        }
            // FIXME: uwzgl�dni� rzeczy, kt�rych gracz nie m�g� zabra� ze sob�
                //Vera m�wi: gdybym wiedzia� o co temu komu� chodzi�o,
                //kto napisa� to 'fix_me' to mo�e
                //i bym to teraz zrobi�, ale nie wiem :P
        }
    }

    }

    TP->check_if_idle(TP,1);

    write("Nagrywam posta�.\n");

    inv = deep_inventory(this_object());
    size = sizeof(inv);
    index = -1;
    while(++index < size)
    {
        if (objectp(inv[index]))
        {
            // FIXME: uwzgl�dni� rzeczy, kt�rych gracz nie m�g� zabra� ze sob�
            SECURITY->do_debug("destroy", inv[index]);
        }
    }

    //Poniewa� na wszelki wypadek parali�e sie zapami�tuj�, �eby przy zerwaniu by�o ok.
    filter(deep_inventory(TP), &->is_paralyze())->remove_object();

    set_this_player(tp);

    environment(this_object())->on_player_logout(this_object());
    this_object()->remove_object();
    return 1;
}

/*
 * Function name: change_password_new
 * Description  : This function is called by change_password_old to catch the
 *                new password entered by the player. Calls itself again to
 *                verify the new entered password and makes sure the new
 *                password is somewhat secure.
 * Arguments    : string str - the new password.
 */
static nomask void
change_password_new(string str)
{
    write("\n");
    if (!strlen(str))
    {
        write("Nie poda�" + koncowka("e�", "a�", "o�") + " �adnego has�a, wi�c "+
            "nie zostanie ono zmienione.\n");
        return;
    }

    /* The first time the player types the new password. */
    if (password2 == 0)
    {
        if (strlen(str) < 6)
        {
            write("Nowe has�o musi mie� minimum 6 znak�w.\n");
            return;
        }

        if (!(SECURITY->proper_password(str)))
        {
            write("Nowe has�o musi spe�nia� podstawowe zasady " +
                "bezpiecze�stwa.\n");
            return;
        }

        password2 = str;
        input_to(change_password_new, 1);
        write("Wpisz nowe has�o ponownie, w celu potwierdzenia go.\n");
        write("Nowe has�o (ponownie): ");
        return;
    }

    /* Second password doesn't match the first one. */
    if (password2 != str)
    {
        write("Nowe has�a nie zgadzaj� si�. Has�o nie zosta�o zmienione.\n");
        return;
    }

    set_password(crypt(password2, "$1$"));	/* Generate new seed */
    password2 = 0;
    write("Has�o zmienione.\n");
}

/*
 * Function name: change_password_old
 * Description  : Takes and checks the old password.
 * Arguments    : string str - the given (old) password.
 */
static nomask void
change_password_old(string str)
{
    write("\n");
    if (!strlen(str) || !match_password(str))
    {
        write("Nieprawid�owe stare has�o.\n");
        return;
    }

    password2 = 0;
    input_to(change_password_new, 1);
    write("�eby twoje has�o by�o bezpieczniejsze, uwa�amy, i� powinno\n" +
          "spe�nia� podstawowe kryteria:\n" +
          "- has�o musi mie� minimum 6 znak�w;\n" +
          "- has�o musi zawiera� przynajmniej jeden znak nie b�d�cy " +
            "liter�;\n" +
          "- has�o musi zaczyna� si� i ko�czyc liter�.\n\n" +
          "Nowe has�o: ");
}

/*
 * Function name: change_password
 * Description  : Allow a player to change his old password into a new one.
 * Arguments    : string str - the command line argument.
 * Returns:     : int 1 - always.
 */
static nomask int
change_password(string str)
{
    write("Stare has�o: ");
    input_to(change_password_old, 1);
    return 1;
}

//------------------nizej wywolywane jest z soula przy sp na kierunek---------
string tmp_str2_ob;
void
daj_paraliz_na_sp(string str)
{
    tmp_str2_ob = str;
    object p=clone_object("/std/paralyze.c");

    p->set_finish_object(TP);
    p->set_finish_fun("koniec_paralizu_wiec_spogladamy");
    p->set_remove_time(1);
    p->set_stop_verb("przesta�");
    p->set_stop_message("Przestajesz spogl�da�.\n");
    p->set_fail_message("Chwil�! W�a�nie spogl�dasz!\n");
    p->remove_allowed("sp",0);
    p->remove_allowed("sp�jrz",0);
    p->remove_allowed("ob",0);
    p->remove_allowed("obejrzyj",0);
    p->remove_allowed("spogl�dnij",0);
    p->move(TP,1);
}
/*
 * Funkcja wywolana gdy paraliz sie skonczy w 'sp na kierunek'
 * Przeniesiona z soula, poniewaz byly zonki, jak dwoch graczy naraz
 * spojrzy gdzies ;)
 * doda�em argumencik, teraz ta funkcja dzia�a tak�e do wygl�dania
 * z dyli�ansu. ;] A po co mam duplikowa� kod. :P
 * Przy dylku object x to dylek. Veru�.
 */
int
koniec_paralizu_wiec_spogladamy(object x,string cos = "")
{
      object *lv, *dd, room;
      string str;

    room = find_object(strlen(cos)?cos:tmp_str2_ob);

      if (room->query_prop(OBJ_I_LIGHT) <= -(this_player()->query_prop(LIVE_I_SEE_DARK)))
      {
        if (stringp(room->query_prop(ROOM_S_DARK_LONG))) {
          write(room->query_prop(ROOM_S_DARK_LONG));
          return 1;
        }
        write("Ciemne miejsce.\n");
        return 1;
      }
      str = room->short();
      if (str[-1..-1] != ".") str += ".\n"; else str += "\n";
      write(str);
      lv = FILTER_CAN_SEE(FILTER_LIVE(all_inventory(room)), this_player());
      if (sizeof(lv))
        write(capitalize(COMPOSITE_FILE->desc_live(lv, PL_MIA, 1)) + ".\n");
      dd = FILTER_DEAD(all_inventory(room));

    if(x->query_dylizans()) //dylka nie pokazujemy, je�li 'wygl�damy' z niego
        dd -= ({x});

    //to jak ju� tu zacz��em grzeba� zn�w, to dam co� takiego:
    //przy 'sp na kierunek' nie powinno by� wida� rzeczy kt�re s�
    //jako 'niewyswietlane'
    string *niewyswietlane = room->query_shorty_rzeczy_niewyswietlanych();
    foreach(object dupa : dd)
        if(member_array(dupa->short(),niewyswietlane) != -1)
            dd-=({dupa});

      dd = filter(dd, objects_filter);
      if (sizeof(dd))
        write(capitalize(COMPOSITE_FILE->desc_dead(dd, PL_MIA, 1)) + ".\n");
      write(describe_combat(lv));
      return 1;
}
//------------------wyzej wywolywane jest z soula przy sp na kierunek---------

