/*
 * W tym pliku znajduj� si� rzeczy zwi�zane z expem,
 * jednak dotycz� one TYLKO i WY��CZNIE graczy.
 *
 * P�ki co w pliku znajduj� si� rzeczy do obs�ugi rutyny.
 *
 * Sta�e pozwalaj�ce balansowanie rutyny znajduj� si� w pliku
 * sys/exp.h.
 *
 * Rutyna obejmuje tylko czynno�ci, kt�re wykonuje si� przez pewien
 * czas, jak na przyk�ad tropienie, �cinanie drzew, szukanie zi�.
 * Aby obj�a czynno�ci wykonywane natychmiast, takie jak sprzedawanie
 * rzeczy, ocenianie obiektu, nale�y wprowadzi� podobny mechanizm redukuj�cy
 * expa w zale�no�ci od ilo�ci wywo�a� funkcji increase_ss w danym odcinku
 * czasu.
 *
 */

#include <exp.h>

#define SKILL_USAGE_SIZE 2

#define DEBG(x) ;

/* ---------------- ZMIENNE  ----------------- */

/*
 * Poni�ej mapping opisuj�cy cz�stotliwo�� u�ywania skilli
 * w postaci:
 * <numer_skilla>: [0] czas ostatniego uzycia
 *                 [1] ca�kowity czas u�ywania skilla
 */

private mapping skill_usage;


/* ---------------- FUNKCJE  ----------------- */

/**
 * Ustawia czas ostatniego u�ycia skilla.
 * @param skill Numer skilla
 * @param t Czas ostatniego u�ycia.
 */
public void
set_skill_last_use_time(int skill, int t)
{
    if(skill < SS_NO_STATS)
        return;

    if(!mappingp(skill_usage))
        skill_usage = ([ ]);

    if(!sizeof(skill_usage[skill]))
        skill_usage[skill] = allocate(SKILL_USAGE_SIZE);

    skill_usage[skill][0] = t;
}

/**
 * Zwraca czas ostatniego u�ycia skilla.
 * @param skill Skill, kt�rego chcemy sprawdzi�.
 * @return Czas ostatniego u�ycia skilla.
 */
public int
query_skill_last_use_time(int skill)
{
    if(skill < SS_NO_STATS)
        return 0;

    if(!mappingp(skill_usage))
        return 0;

    if(!sizeof(skill_usage[skill]))
        return 0;

    return skill_usage[skill][0];
}

/**
 * Modyfikuje ca�kowity czas expienia danego
 * skilla.
 * @param skill Numer skilla. Dla -1 modyfikuj� si� wszystkie skille.
 * @param t Czas o jaki zmieniamy.
 */
public void
modify_exp_time(int skill, int t)
{
    if(skill < SS_NO_STATS && skill != -1)
        return;

    if(!mappingp(skill_usage))
        return;

    int old;

    if(skill == -1)
    {
        int *keys = m_indexes(skill_usage);

        // Modyfikujemy wszystkie skille
        foreach(int key : keys)
        {
            if(!sizeof(skill_usage[key]))
                continue;

            old = skill_usage[key][1];

            if(old + t <= 0)
                skill_usage[key][1] = 0;
            else
                skill_usage[key][1] = old + t;
        }
    }
    else
    {
        old = skill_usage[skill][1];

        if(old + t <= 0)
            skill_usage[skill][1] = 0;
        else
            skill_usage[skill][1] = old + t;
    }
}

/**
 * Zwraca ca�kowity czas expienia danego skilla.
 * @param skill Numer skilla
 * @return Czas expienia owego skilla.
 */
public int
query_exp_time(int skill)
{
    if(skill < SS_NO_STATS)
        return 0;

    if(!mappingp(skill_usage))
        return 0;

    if(!sizeof(skill_usage[skill]))
        return 0;

    return skill_usage[skill][1];
}

/**
 * Zeruje wszystkie dane zwi�zane z u�yciami skilla.
 * @param skill Numer skilla, kt�rego dane maj� zosta� skasowane.
 *              Dla -1 kasowane s� dane wszystkich skilli.
 */
public void
clear_skill_usage_data(int skill)
{
    if(skill < SS_NO_STATS && skill >= 0)
        return;

    if(!mappingp(skill_usage))
        return;

    if(skill == -1)
        skill_usage = ([ ]);
    else
        skill_usage[skill] = ({ });
}

/*
 * Funkcja oblicza o ile zmniejsza sie rutyna przy
 * pojedy�czym wywo�aniu funkcji zmniejszaj�cej.
 */
public int
query_routine_mod_value()
{
    // To +1 to w zastepstwie za zaokraglanie wartosci w g�r�
    return (EXP_ROUTINE_LIMIT*EXP_ROUTINE_MOD_FREQ)/(EXP_ROUTINE_LIMIT_PERIOD)+1;
}

/*
 * Ta funkcja jest wywo�ywana co parena�cie minut. Konkretnie
 * jest to okre�lone w EXP_ROUTINE_MOD_FREQ. Ma ona na celu
 * zmniejszanie czasu expienia, tak by po pewnym czasie zn�w
 * mo�na by�o expi� bez efekt�w ubocznych.
 */
void
routine_mod()
{
    DEBG("Rutynowy alarm! Bum!");
    TO->modify_exp_time(-1, (-1)*(query_routine_mod_value()));

    set_alarm(itof(60*EXP_ROUTINE_MOD_FREQ), 0.0, &routine_mod());
}

/*
 * Inicjalizuje rutyn�.
 * Innymi s�owy zmniejsza czasy expienia, kt�re zmniejszy�by normalnie
 * alarm. Jednak biedny alarm nie m�g� tego zrobi�, bo gracza nie by�o
 * w grze. Wobec tego robimy to r�cznie.
 */
public void
init_routine()
{
    DEBG("****** Init rutyny ******");
    if(!mappingp(skill_usage))
        return;

    int *keys = m_indexes(skill_usage);
    int t = time();
    int mod, last_use;

    foreach(int key : keys)
    {
        mod = t - query_skill_last_use_time(key);

        if(mod <= 0)
            continue;

        mod *= query_routine_mod_value();
        mod /= EXP_ROUTINE_MOD_FREQ*60;
        mod *= -1;

        DEBG("[Mod czasu expienia] "+mod);

        modify_exp_time(key, mod);
    }

    // Odpalamy alarma, kt�ry b�dzie stopniowo zmniejsza� rutynke.
    set_alarm(itof(60*EXP_ROUTINE_MOD_FREQ), 0.0, &routine_mod());
}

/**
 * Zwraca o ile razy gracz przekroczy� rutynowy limit. Je�li
 * go nie przekroczy� to oczywi�cie zwraca warto�� z przedzia�u
 * [0;1)
 * @param ss Numer skilla, kt�ry chcemy sprawdzi�
 * @return Liczba opisuj�ca ila razy gracz przekroczy� limit.
 */
public float
query_rutyna(int ss)
{
     return (itof(TO->query_exp_time(ss))/60.0)/itof(EXP_ROUTINE_LIMIT);
}
