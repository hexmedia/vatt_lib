/**
 * Oto plik, w kt�rym mie�ci si� ca�y kod zarz�dzaj�cy sygna�ami nawi�zywania
 * walki mi�dzy dwoma graczami, lecz nie tylko, tak�e mi�dzy graczem a npcem.
 * W takim przypadku signal_fight npca jest w /std/npc.c
 * Vera
 */

#include <macros.h>
#include <filter_funs.h>
object atakujacy;

public int
signal_fight(object atakowany, object gdzie)
{
//find_player("vera")->catch_msg("signal_fight w PLAYERZE!\n");
//NIC NIE ROBIMY TU JU�.
    return 1;
}

void
walka_event(string gdzie, string skad)
{
//find_player("vera")->catch_msg("walka event: gdzie: "+gdzie+" skad: "+skad+"\n");
    if(skad != "")
    {
        tell_room(gdzie ,capitalize(skad)+
             " dochodz^a twych uszu odg^losy walki!\n");
    }
    else
        tell_room(gdzie ,"Z oddali dochodz^a twych uszu odg^losy walki!\n");
}

private static int sprawdz_drzwi(object d)
{
    if(d->is_door() && d->query_opened())
        return 1;
}

public int
prepare_signal_fight(object ofiara)
{
    //liv to wszystkie livingi. Wpierw s� to livingi z tego samego ENV co ofiara
    object *liv = FILTER_OTHER_LIVE(all_inventory(ENV(TP)));
    //a to s� lokacje, w kt�rych b�dziemy jeszcze szuka� npc�w.
    string *lokacje = filter(ENV(TP)->query_exit_rooms(), &stringp());

    //Dwa filtry s� wolniejsze od jednego, lepiej zrobi� dodatkow� funckj�
    //je�li mamy jakie� drzwi (niezamkni�te na klucz!) to odnotujmy je te�.
    object *doors = filter(all_inventory(ENV(TP)), &sprawdz_drzwi());
//     object *doors = filter(filter(all_inventory(ENV(TP)),&->is_door()),
//         &operator(==)(0) @ &->query_locked() );


    string skad="", str="";
    //dla wszystkich lokacji standardowych:
    for(int i=0;i<sizeof(lokacje);i++)
    {
        if(!find_object(lokacje[i]))
            continue;

        liv+=FILTER_LIVE(all_inventory(find_object(lokacje[i])));

        //+info o walce:
        skad=ENV(TP)->query_exit_cmds()[i];
        switch(skad)
        {
            case "wsch�d": str="z zachodu"; break;
            case "zach�d": str="ze wschodu"; break;
            case "p�noc": str="z po�udnia"; break;
            case "po�udnie": str="z p�nocy"; break;
            case "po�udniowy-wsch�d": str="z p�nocnego-zachodu"; break;
            case "po�udniowy-zach�d": str="z p�nocnego-wschodu"; break;
            case "p�nocny-wsch�d": str="z po�udniowego-zachodu"; break;
            case "p�nocny-zach�d": str="z po�udniowego-wschodu"; break;
            case "g�ra": str="z do�u"; break;
            case "d�": str="z g�ry"; break;
            default: str="z oddali"; break;
        }

        walka_event(lokacje[i], str);
    }

    //dla wszystkich lokacji z drzwiami:
    int y; object druga_lok; string nasza_lok = file_name(ENV(TP)), wyjscie="";
    foreach(object drzwi : doors)
    {
        druga_lok = ENV(drzwi->query_other_door());

        liv+=FILTER_LIVE(all_inventory(druga_lok));

        //+info o walce
        //'y' to numer wpisu nazwy lokacji naszego TP
        //w tablicy query_exit lokacji po drugiej stronie drzwi
        y = member_array(nasza_lok, druga_lok->query_exit() );
        if(y != -1 ) //powinno != -1 :P
        {
            //w query_exit mamy tablic� string�w z nazwami przej��. powinna mie� 3 elementy
            if(pointerp(druga_lok->query_exit()[y+1]) && sizeof(druga_lok->query_exit()[y+1]) >= 3 )
                wyjscie=(druga_lok->query_exit()[y+1])[2];
        }

        walka_event(file_name(druga_lok), wyjscie);
        wyjscie = "";
    }


    set_alarm(itof(random(3)+1),0.0,"alarm_na_signal_fight",liv,ofiara, ENV(ofiara));

    signal_fight(ofiara, ENV(ofiara));

    return 1;
}

/* argumenty:
 * dokogo to tablica living�w do kt�rych przesy�amy sygna�
 * ofiara to oczywi�cie ofiara ataku :)
 * lok to lokacja na kt�rej ten haniebny czyn jest pope�niany
 */
public void
alarm_na_signal_fight(object *dokogo,object ofiara, object lok)
{
    foreach(object living : dokogo)
        living->signal_fight(ofiara,lok);
}
