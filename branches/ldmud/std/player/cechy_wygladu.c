/*
 * Sa tutaj zdefiniowane cechy takie jak: kolor_oczu, 
 * wiek oraz budowa_ciala i cecha_szczegolna(opcjonalnie)
 * +czy ktos jest lysy na stale.
 *
 * Plus dodatkowo spec_opis - zdanie kt�re mo�emy graczowi
 * zdefiniowa� za pomoc� set_spec_opis. Co� takiego pojawi mu
 * si� w longu.
 *
 *          Vera
 *
 *
 */


int     wiek,
        lysy_na_stale;
string  *kolor_oczu = ({}),
        *budowa_ciala = ({}),
        *cecha_szczegolna=({}),
        *spec_opis = ({ });



/* Tu ustawiamy specjalny opis, jaki chcemy da� graczowi
 * Prosz� nie rozdawa� tego na prawo i lewo, tylko
 * na podanie i to w SZCZEG�LNYCH warunkach.
 *
 *
 * ta komenda przyjmuje 2 elementow� tablic� string�w
 * s� to opisy
 * op[0] - dla siebie (ob siebie)
 * op[1] - dla innych (ob kogos)
 * Vera.
 */ 
public int
set_spec_opis(string *op)
{
    //�le podane dane
    if(!pointerp(op) || sizeof(op) != 2 || !stringp(op[0]) || !stringp(op[1]))
        return 0;

     spec_opis = op;

    return 1;
}

public string
*query_spec_opis()
{
    return spec_opis;
}


public string
*query_kolor_oczu()
{
    return kolor_oczu;
}

public void
set_kolor_oczu(string *str)
{
    kolor_oczu = str;
}

public int
query_wiek()
{
    return wiek;
}

public int
set_wiek(int liczba)
{
    wiek = liczba;
}

public int
query_lysy_na_stale()
{
    return lysy_na_stale;
}
public int //jesli chcemy kogos lysego na stale, to dajemy mu 1.
set_lysy_na_stale(int liczba)
{
    if(liczba==1)
       this_object()->set_dlugosc_wlosow(0.0);

    lysy_na_stale = liczba;
}

public string
*query_budowa_ciala()
{
   return budowa_ciala;
}

public string
set_budowa_ciala(string *str)
{
   budowa_ciala = str;
}

public string
*query_cecha_szczegolna()
{
   return cecha_szczegolna;
}

public string
set_cecha_szczegolna(string *str)
{
   cecha_szczegolna = str;
}
