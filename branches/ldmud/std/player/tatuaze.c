#include <wa_types.h>

mixed tatuaze;

void
init_tatuaze()
{
    add_subloc("subloc_tatuaze", this_object());
}

public void
dodaj_tatuaz(string gdzie, string typ, string opis)
{
    if (!tatuaze) tatuaze = ([ ]);
    tatuaze[gdzie] = ({ typ, opis });
}

public void
usun_tatuaz(string gdzie)
{
    m_delkey(tatuaze, gdzie);
}

public mixed
query_tatuaz(string gdzie)
{
    if (!tatuaze) return 0;
    return secure_var(tatuaze[gdzie]);
}

public string
opis_tatuazy(object for_obj = 0)
{
    string str;
    mixed ret = ({ });

    if (!m_sizeof(tatuaze))
	return "";

    if (for_obj == this_object())
	str = " masz ";
    else
	str = " ma ";

    foreach(string gdzie, mixed tatuaz : tatuaze)
    {
	switch (gdzie)
	{
	    // query_armour zwraca 0 lub pusta tablice jesli miejsce jest wolne
	    // sizeof i dla 0, i dla pustej tablicy zwraca 0, wiec jest ok
	    // ale generalnie lepiej uwazac na takie trikowe zagrania ;)
	    case "na karku": // ?
		if (sizeof(this_object()->query_armour(TS_NECK))) continue;
		break;
	    case "na szyi":
		if (sizeof(this_object()->query_armour(TS_NECK))) continue;
		break;
	    case "na plecach":
		if (sizeof(this_object()->query_armour(TS_TORSO))) continue;
		break;
//	    case "na ":
//		if (sizeof(this_object()->query_armour(TS_))) continue;
//		break;
//	    case "na ":
//		if (sizeof(this_object()->query_armour(TS_))) continue;
//		break;
	    case "na prawym ramieniu":
		if (sizeof(this_object()->query_armour(TS_R_ARM))) continue;
		break;
	    case "na lewym ramieniu":
		if (sizeof(this_object()->query_armour(TS_L_ARM))) continue;
		break;
	    case "na prawym przedramieniu":
		if (sizeof(this_object()->query_armour(TS_R_FOREARM))) continue;
		break;
	    case "na lewym przedramieniu":
		if (sizeof(this_object()->query_armour(TS_L_FOREARM))) continue;
		break;
        // FIXME: Mozna zrobic bardziej dokladny slot
	    case "na lewej piersi":
		if (sizeof(this_object()->query_armour(TS_TORSO))) continue;
		break;
	    case "na prawej piersi":
		if (sizeof(this_object()->query_armour(TS_TORSO))) continue;
		break;
	    case "na brzuchu":
		if (sizeof(this_object()->query_armour(TS_STOMACH))) continue;
		break;
        // FIXME: To na dole to raczej tymczasowe
	    case "na lewym po^sladku":
		if (sizeof(this_object()->query_armour(TS_L_THIGH))) continue;
		break;
	    case "na prawym po^sladku":
		if (sizeof(this_object()->query_armour(TS_R_THIGH))) continue;
		break;
	    case "na lewym udzie":
		if (sizeof(this_object()->query_armour(TS_L_THIGH))) continue;
		break;
	    case "na prawym udzie":
		if (sizeof(this_object()->query_armour(TS_R_THIGH))) continue;
		break;
	    case "na lewej �ydce":
		if (sizeof(this_object()->query_armour(TS_L_SHIN))) continue;
		break;
	    case "na prawej �ydce":
		if (sizeof(this_object()->query_armour(TS_R_SHIN))) continue;
		break;
	    case "na lewej stopie":
		if (sizeof(this_object()->query_armour(TS_L_FOOT))) continue;
		break;
	    case "na prawej stopie":
		if (sizeof(this_object()->query_armour(TS_R_FOOT))) continue;
		break;
	    // query_weapon daje obiekt lub 0, wiec tu juz bez uzycia sizeof
	    case "na prawej d�oni":
		if (this_object()->query_weapon(TS_R_HAND) ||
		    sizeof(this_object()->query_armour(TS_R_HAND))) continue;
		break;
	    case "na lewej d�oni":
		if (this_object()->query_weapon(TS_L_HAND) ||
		    sizeof(this_object()->query_armour(TS_L_HAND))) continue;
		break;
	    default:
		break;
	}
	ret += ({ gdzie + str + tatuaz[0] + " tatua� " + tatuaz[1] });
    }

    if (!sizeof(ret))
	return "";

    return capitalize(COMPOSITE_WORDS2(ret, ", a ")) + ".\n";
}
