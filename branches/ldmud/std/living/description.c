/**
 * \file /std/living/description.c
 *
 * This is a subpart of /std/living.c
 *
 * All description relevant routines are defined here.
 *
 * NOTE
 * There is some calls of the type: this_object()->function()
 * in a number of places. The reason for this is to allow those
 * functions to be shadowed as internal function calls are not
 * possible to shadow.
 *
 *
 * Dodalam w ocenianiu przeciwnika wiek. Poki co tylko dla ludzi. Lil.
 */

#include <colors.h>
#include <macros.h>
#include <livings.h>
#include <wa_types.h>
#include <formulas.h>
#include <language.h>
#include <composite.h>
#include <state_desc.h>
#include <filter_funs.h>
#include <stdproperties.h>

#ifdef DAY_AND_NIGHT
#include <mudtime.h>
#endif

private static int appearance_offset;
private static int is_linkdead;

/*
 * Nazwa funkcji: set_linkdead
 * Opis         : Set the linkdeath-status of the player.
 * Argumenty    : int i - true if the player is linkdead.
 */
public void
set_linkdead(int i)
{
    if (i)
        is_linkdead = time();
    else
        is_linkdead = 0;
}

/*
 * Nazwa funkcji:   ustaw_imie
 * Opis		  : Dzieki tej funkcji mozna zdefiniowac imie kazdemu
 *		    livingowi.
 * Argumenty	  : *imie - Tablica z odmiana imienia.
 *		    rodzaj - Rodzaj gramatyczny imienia.
 * Funkcja zwraca : int - 1 jesli imie zostalo nadane, 0 w przeciwnym wypadku.
 */
public int
ustaw_imie(string *imie, int rodzaj = PL_MESKI_NOS_ZYW)
{
    int x = 6;

    if (sizeof(imie) != 6)
        return 0;

    while (--x >= 0)
        ::set_name(imie[x], x, rodzaj);

    add_prop(LIVE_I_INTRODUCING, 1);
    remove_prop(LIVE_I_NEVERKNOWN);

    set_living_name(imie[0]);

    return 1;
}

/*
 * Function name:   add_introduced
 * Description:     Add the name of a living who has introduced herself to us
 * Argumenty:       imie_mia: Imie przedstawionej osoby w mianowniku
 *                  imie_bie: Imie przedstawionej osoby w bierniku
 */
public void
add_introduced(string imie_mia, string imie_bie)
{
    if (query_prop(LIVE_I_INTRODUCING))
        set_alarm(0.5 + itof(random(20)) / 10.0, 0.0, "return_introduce", imie_mia);
}

public void
return_introduce(string imie)
{
    object osoba = present(imie, environment());

    if (osoba)
        command("przedstaw sie " + OB_NAME(osoba));
}

/*
 * Nazwa funkcji: query_linkdead
 * Opis         : Return the linkdeath-status of the player.
 * Zwraca       : int - if false, the player is not linkdead. Else it
 *                      returns the time the player linkdied.
 */
public int
query_linkdead()
{
    return is_linkdead;
}

/*
 * Nazwa funkcji: is_humanoid
 * Opis         : Tells whether we are humanoid or not. By default, all
 *                livings are marked as humanoid and then in /std/creature
 *                we unmark them as such by masking this function.
 * Zwraca       : int - 1.
 */
public int
is_humanoid()
{
    return 1;
}

/*
 * Nazwa funkcji: query_met
 * Opis         : Tells if we know a certain living's name. As NPC's always
 *                know everyone, this function is masked in the player object
 *                for true met/nonmet behaviour for players (if defined).
 * Argumenty    : mixed name - the name or objectpointer to the living we
 *                             are supposed to have met.
 * Zwraca       : int 1 - always as NPC's always know everyone.
 */
public int
query_met(mixed name, int dummy = 0)
{
    return 1;
}

/*
 * Nazwa funkcji:   notmet_me
 * Opis         :   Finds out if obj is considered to have met me. Players
 *                  must have been introduced to me. All others don't have to
 *                  be introduced to know me.
 * Argumenty    :   obj: Object in question, has it met me?
 * Zwraca       :   True if object has not met me.
 */
public int
notmet_me(object obj)
{
#ifdef MET_ACTIVE
    if (obj && query_ip_number(obj))
        return !obj->query_met(this_object());
#else
    return !this_player()->query_met(this_object());
#endif MET_ACTIVE
}

/*
 * Nazwa funkcji:   query_real_name
 * Opis         :   Returns the lowercase name of this living.
 *                  E.g.: "fatty".
 * Argumenty:	    przyp - Przypadek. Mozna podac, nie trzeba.
 * Zwraca       :   The name
 */
public varargs string
query_real_name(int przyp)
{
    return lower_case(::query_name(0, przyp));
}

/*
 * Nazwa funkcji:   query_name
 * Opis         :   Returns the capitalized name of this living.
 *                  E.g.: "Fatty".
 * Zwraca       :   The name
 */
public varargs string
query_name(int przyp)
{
    return capitalize(::query_nazwa(przyp));
}

/*
 * Nazwa funkcji:   query_met_name
 * Opis         :   Returns the name of this living, or "ghost of name" if
 *                  this living is not quite so living. E.g.: "Fatty".
 * Zwraca       :   The name
 */
public string
query_met_name(int przyp = 0)
{
    if(this_object()->query_ghost() && this_object()->query_race() != "byt astralny")
    {
        switch(przyp)
        {
            case PL_MIA: return "duch "    + query_name(PL_DOP);
            case PL_DOP: return "ducha "   + query_name(PL_DOP);
            case PL_CEL: return "duchowi " + query_name(PL_DOP);
            case PL_BIE: return "ducha "   + query_name(PL_DOP);
            case PL_NAR: return "duchem "  + query_name(PL_DOP);
            case PL_MIE: return "duchu "   + query_name(PL_DOP);
            default:     return "duch "    + query_name(PL_DOP);
        }
    }
    return query_name(przyp);
}


/*
 * Nazwa funkcji : query_nonmet_name
 * Opis          : Zwraca opis tego livinga w liczbie pojedynczej,
 *		   w formie przeznaczonej dla osob, ktore go nie znaja,
 *		   np. "niebieskooka zgrabna elfka". Jesli living nie ma
 *		   ustawionych shortow, zostana one utworzone na podstawie
 *		   dwoch pierwszych przymiotnikow oraz rasy.
 * Argumenty     : int przyp - przypadek, w jakim ma zostac podany opis.
 * Funkcja zwraca: string - opis livinga przeznaczony dla osob nie znajacych
 *			    go, w liczbie pojedynczej.
 */
public varargs string
query_nonmet_name(int przyp)
{
    string *adj, str;
    string gender;

    /* Espcially true for NPC's. If a short has been set, use it. */
    if (strlen(query_short(przyp)))
        return ::short(przyp);

    if (sizeof((adj = this_object()->query_przym(1, przyp, this_object()->query_rodzaj_rasy() + 1 ))) > 0)
        str = implode(adj, " ") + " ";
    else
        str = "";

    if (query_ghost() && this_object()->query_race() != "byt astralny")
    {
        switch(przyp)
        {
            case PL_MIA: str += LV_DUCH[PL_MIA]; break;
            case PL_DOP: str += LV_DUCH[PL_DOP]; break;
            case PL_CEL: str += LV_DUCH[PL_CEL]; break;
            case PL_BIE: str += LV_DUCH[PL_BIE]; break;
            case PL_NAR: str += LV_DUCH[PL_NAR]; break;
            case PL_MIE: str += LV_DUCH[PL_MIE]; break;
        }

        if (!this_object()->query_rasa())
            return str;

        str += " ";
        przyp = PL_DOP;
    }

    if (this_object()->query_osobno())
    {
        switch(this_object()->query_gender())
        {
            case G_MALE:
                 switch(przyp)
                 {
                     case PL_MIA: str += "m�czyzna"; break;
                     case PL_DOP: str += "m�czyzny"; break;
                     case PL_CEL: str += "m�czy�nie"; break;
                     case PL_BIE: str += "m�czyzn�"; break;
                     case PL_NAR: str += "m�czyzn�"; break;
                     case PL_MIE: str += "m�czy�nie"; break;
                 }
                 break;
            case G_FEMALE:
                 switch(przyp)
                 {
                     case PL_MIA: str += "kobieta"; break;
                     case PL_DOP: str += "kobiety"; break;
                     case PL_CEL: str += "kobiecie"; break;
                     case PL_BIE: str += "kobiet�"; break;
                     case PL_NAR: str += "kobiet�"; break;
                     case PL_MIE: str += "kobiecie"; break;
                 }
                 break;
            case G_NEUTER:
                 switch(przyp)
                 {
                     case PL_MIA: str += "obojnak"; break;
                     case PL_DOP: str += "obojnaka"; break;
                     case PL_CEL: str += "obojnakowi"; break;
                     case PL_BIE: str += "obojnaka"; break;
                     case PL_NAR: str += "obojnakiem"; break;
                     case PL_MIE: str += "obojnaku"; break;
                 }
                 break;
        }
    }
    else
        str += this_object()->query_rasa(przyp);

    return str;
}

/*
 * Nazwa funkcji : query_nonmet_pname
 * Opis          : Zwraca opis tego livinga w liczbie mnogiej,
 *		   w formie przeznaczonej dla osob, ktore go nie znaja,
 *		   np. "niebieskookie zgrabne elfki". Jesli living nie ma
 *		   ustawionych shortow, zostana one utworzone na podstawie
 *		   dwoch pierwszych przymiotnikow oraz rasy. U livingow nie
 *		   humanoidalnych nie pojawi sie plec. U humanoidalnych moze
 *		   to zostac osiagniete poprzez dodanie propa
 *		   LIVE_I_NO_GENDER_DESC.
 * Argumenty     : int przyp - przypadek, w jakim ma zostac podany opis.
 * Funkcja zwraca: string - opis livinga przeznaczony dla osob nie znajacych
 *			    go, w liczbie mnogiej.
 */
public varargs string
query_nonmet_pname(int przyp)
{
    string *adj, str;
    string gender;

    /* Espcially true for NPC's. If a short has been set, use it. */
    if (strlen(query_plural_short(przyp)))
    {
        return ::plural_short(przyp);
    }

    if (sizeof((adj = this_object()->query_pprzym(1, przyp,
            this_object()->query_rodzaj_rasy() + 1 ))) > 0)
    {
        str = implode(adj[..1], " ") + " ";
    }
    else str = "";

    if (query_ghost() && this_object()->query_race() != "byt astralny")
    {
        switch(przyp)
        {
            case PL_MIA: str += LV_DUCHY[PL_MIA];   break;
            case PL_DOP: str += LV_DUCHY[PL_DOP];   break;
            case PL_CEL: str += LV_DUCHY[PL_CEL];   break;
            case PL_BIE: str += LV_DUCHY[PL_BIE];   break;
            case PL_NAR: str += LV_DUCHY[PL_NAR];   break;
            case PL_MIE: str += LV_DUCHY[PL_MIE];   break;
        }

        if (!this_object()->query_prasa())
            return str;

        str += " ";
        przyp = PL_DOP;
    }

    if (this_object()->query_osobno())
    {
        switch(this_object()->query_gender())
        {
            case G_MALE:
                switch(przyp)
                {
                    case PL_MIA: str += "m�czy�ni"; break;
                    case PL_DOP: str += "m�czyzn"; break;
                    case PL_CEL: str += "m�czyznom"; break;
                    case PL_BIE: str += "m�czyzn"; break;
                    case PL_NAR: str += "m�czyznami"; break;
                    case PL_MIE: str += "m�czyznach"; break;
                }
                break;
            case G_FEMALE:
                switch(przyp)
                {
                    case PL_MIA: str += "kobiety"; break;
                    case PL_DOP: str += "kobiet"; break;
                    case PL_CEL: str += "kobietom"; break;
                    case PL_BIE: str += "kobiety"; break;
                    case PL_NAR: str += "kobietami"; break;
                    case PL_MIE: str += "kobietach"; break;
                }
                break;
            case G_NEUTER:
                switch(przyp)
                {
                    case PL_MIA: str += "obojnaki"; break;
                    case PL_DOP: str += "obojnakow"; break;
                    case PL_CEL: str += "obojnakom"; break;
                    case PL_BIE: str += "obojnakow"; break;
                    case PL_NAR: str += "obojnakami"; break;
                    case PL_MIE: str += "obojnakach"; break;
                }
                break;
        }
    }
    else
        str += this_object()->query_prasa(przyp);

    return str;
}

/*
 * Nazwa funkcji:   query_Met_name
 * Opis         :   Returns the capitalized name of this living, prepended
 *                  with "Ghost of" if the living is not that living at all.
 *                  E.g.: "Ghost of Fatty".
 * Zwraca       :   The capitalized name of the living when met.
 */
public varargs string
query_Met_name(int przyp)
{
    return capitalize(query_met_name(przyp));
}

/**
 *  Ta funkcja dodaje 'krytyczne'
 *  przymiotniki, takie jak: pijany, nagi i zamy�lony.
 *
 *  Ustali�em, �e dodawa� si� b�dzie max. 1 przymiotnik
 *  wi�c chronologicznie od tych najbardziej krytycznych:
 *  nagi, zamy�lony i na ko�cu pijany.
 *
 *  @param przyp przypadek w jakim dodajemy przymiotnik.
 *
 *  @return przymiotnik specjalny
 *
 *  @author Vera
 */
public string
dodaj_spec_przym(int przyp = 0)
{
    if(TO->is_humanoid())
    {
        if(query_poziom_nagosci() == 4)
            return oblicz_przym("nagi", "nadzy", przyp, TO->query_rodzaj(), 0);
        else if(TO->has_idle_now() == -1)
            return oblicz_przym("zamy�lony", "zamy�leni", przyp, TO->query_rodzaj(), 0);
        else
        {   //Zmieni�em tu w ten spos�b zajmowanie pami�ci na a i b odbywa si� dopiero
            //w momencie kiedy inne ify zawiod�, co oszcz�dza setne sekundy:P

            int a, b;

            a = TO->query_prop(LIVE_I_MAX_INTOX);
            b = TO->query_intoxicated();
            a = 100 * b / (a != 0 ? a : b);

            if(a >= 50) //od 'pijanego' w g�r� dodajemy spec_przym 'pijany'
                return oblicz_przym("pijany", "pijani", przyp, TO->query_rodzaj(), 0);
        }
    }

    return 0;
}

/**
 * Zwraca opis osobnika widzianego przez kogo� innego,
 * domy�lnie @see vbfc_object().
 * Funkcja sprawdza czy osoba dla kt�rej sprawdzamy zna
 * @see this_object() oraz czy go widzi.
 * Edit by Vera: do przymiotnik�w specjalnych @see dodaj_spec_przym()
 *
 * @param pobj  Obiekt kt�ry ma widzie� @see this_object()
 * @param przyp Przypadek w jakim chcemy imi� otrzyma�.
 * @param on    Czy pokaza� imi� bez specjalnych przymiotnik�w
 *
 * @return Kr�tki opis b�d� imi� @see this_object(). (Patrz wy�ej)
 */
public string
query_imie(mixed pobj = 0, mixed przyp = 0, int on = 0)
{
    string pre, aft, spec_przym;

    pre = ""; aft = "";

    if(!objectp(pobj))
    {
        if(przyp)
            on = 1;

        przyp = pobj;
        pobj = vbfc_object();
    }

    if(!intp(przyp))
    {
        if(stringp(przyp))
            przyp = atoi(przyp);
        else
            przyp = 0;
    }

    if(!CAN_SEE(pobj, this_object()) || !CAN_SEE_IN_ROOM(pobj))
    {
        switch(przyp)
        {
            case PL_MIA: return "kto�";
            case PL_DOP: return "kogo�";
            case PL_CEL: return "komu�";
            case PL_BIE: return "kogo�";
            case PL_NAR: return "kim�";
            case PL_MIE: return "kim�";
        }
    }

    if(query_prop(OBJ_I_INVIS))
    {
        pre = "(";
        aft = ")";
    }
    else if(query_prop(OBJ_I_HIDE))
    {
        pre = "[";
        aft = "]";
    }

#ifdef MET_ACTIVE
    if(notmet_me(pobj))
        return pre + this_object()->query_nonmet_name(przyp) + aft;
    else
#endif MET_ACTIVE
    {
        if(on || !(spec_przym = dodaj_spec_przym(przyp)))
            return pre + TO->query_met_name(przyp) + aft;
        else
            return pre + spec_przym + " " + TO->query_met_name(przyp) + aft;
    }
}
/**
 * @see query_imie()
 *
 * Jedyna r�nica, to to, �e tu zawsze b�dzie du�a litera.
 */
public string
query_Imie(mixed pobj = 0, mixed przyp = 0, int on = 0)
{
    return capitalize(query_imie(pobj, przyp, on));
}

/**
 * Funkcja ma za zadanie zwr�ci� imi� b�d� opis gracza
 * widzianego przez kogo� innego, domy�lnie przez @see vbfc_object().
 *
 * Zale�nie od tego czy @param pobj zna czy nie zna pokazywane jest albo imi�
 * albo rasa @see this_object().
 *
 * @param pobj  Obiekt kt�remu ma zosta� pokazane (domy�lnie @see vbfc_object()).
 * @param przyp Przypadek w jakim ma zosta� pokazane imi� lub rasa.
 * @param on    Czy do imienia ma zosta� dodany przymiotnik specjalny @see dodaj_spec_przym()
 *
 * @return Imi� lub ras� zale�nie do tego jak widziany jest @see this_object(). (Patrz wy�ej)
 */
public string
query_imie_lub_rasa(mixed pobj = 0, mixed przyp = 0, int on = 0)
{
    if(!objectp(pobj))
    {
        if(przyp)
            on = 1;

        przyp = pobj;
        pobj = vbfc_object();
    }

    if(!intp(przyp))
    {
        if(stringp(przyp))
            przyp = atoi(przyp);
        else
            przyp = 0;
    }

#ifdef MET_ACTIVE
    if(pobj->query_met(TP))
        return query_imie(pobj, przyp, on);
    else
#endif MET_ACTIVE
        return "jak" + TP->koncowka("i�", "a�", "ie�") + " " + query_rasa(przyp);
}

/**
 * @see query_imie_lub_rasa()
 *
 * Jedyna r�nica, to to, �e tu zawsze b�dzie du�a litera.
 */
public string
query_Imie_lub_Rasa(mixed pobj = 0, int przyp = 0, int on = 0)
{
    return capitalize(query_imie_lub_rasa(pobj, przyp, on));
}

/**
 * @see query_imie()
 *
 * @warning Prosz� o nie u�ywanie tej funkcji.
 *          Jest ona wycofywana. [KRUN]
 */
public varargs string
query_art_name(mixed pobj, int przyp, int on = 0)
{
    return query_imie(pobj, przyp, on);
}

/**
 * @see query_Imie()
 *
 * @warning Prosz� o nie u�ywanie tej funkcji.
 *          Jest ona wycofywana. [KRUN]
 */
public varargs string
query_Art_name(mixed pobj, int przyp, int on = 0)
{
    return capitalize(query_art_name(pobj, przyp, on));
}

/**
 * @see query_imie()
 *
 * @warning Prosz� o nie u�ywanie tej funkcji.
 *          Jest ona wycofywana. [KRUN]
 */
public varargs string
query_the_name(mixed pobj, int przyp, int on = 0)
{
    return query_art_name(pobj, przyp, on);
}

/**
 * @see query_Imie()
 *
 * @warning Prosz� o nie u�ywanie tej funkcji.
 *          Jest ona wycofywana. [KRUN]
 */
public varargs string
query_The_name(object pobj, int przyp, int on = 0)
{
    return capitalize(query_the_name(pobj, przyp, on));
}

/*
 * Nazwa funkcji : query_wolacz
 * Opis          : Znajduje wolacz liczby pojedynczej imienia istoty.
 * Funkcja zwraca: string - szukany wolacz.
 */
public string
query_wolacz()
{
    return LANG_FILE->wolacz(this_object()->query_name(PL_MIA),
                             this_object()->query_name(PL_MIE));
}

/*
 * Nazwa funkcji: query_exp_title
 * Opis         : Returns "wizard" if this living is a wizard, or else
 *                tries to calculate a title from the stats
 * Zwraca       : string - the title.
 */
public string
query_exp_title()
{
    int a, s;

    s = sizeof(SD_AV_TITLES);
    a = ((this_object()->query_average_stat() * s) / 100);
    a = ((a >= s) ? (s - 1) : a);

    return SD_AV_TITLES[a];
}

/*
 * Function:    query_presentation
 * Opis         : Gives a presentation of the living in one line. Including
 *              Name, Race, Guild titles, Alignment and Experience level
 *              This should only be displayed to met players.
 *              E.g.: "Fatty the donut-fan, wizard, male gnome (eating)"
 * Zwraca       :     The presentation string
 */
public string
query_presentation()
{
    string sur, orig, tit, altit;

    sur = query_surname();
    orig = query_origin();
    tit = query_title();
#ifndef NO_ALIGN_TITLE
    altit = this_object()->query_al_title();
#endif

    return query_name() +
        (strlen(sur) ? (" " + sur) : "") +
        (strlen(orig) ? (" " + orig) : "") +
        (strlen(tit) ? ((tit[0] == ',' ? "" : " ") + tit + ", ") : ", ") +
        this_object()->query_rasa(PL_MIA)
#ifndef NO_ALIGN_TITLE
        + (strlen(c) ? (" (" + c + ")") : "")
#endif
        ;
}

/**
 * Funkcja ma za zadanie znale�ienie i zwr�cenie kr�tkiego opisu
 * livinga. R�wnocze�nie sprawdzaj�c czy pokaza� go jako osob�
 * znajom� czy te� nie, a tak�e dodaj�c niewidzialno�ci.
 *
 * @param for_obj   dla kogo ma by� wy�wietlony short (domy�lnie @see vbfc_object())
 * @param przyp     W jakim przypadku short ma zosta� wy�wietlony.
 *
 * @return Kr�tki opis livinga.
 */
public varargs string
short(mixed for_obj, mixed przyp)
{
    string desc;
    string extra;

    if(!objectp(for_obj))
    {
        if(intp(for_obj))
            przyp = for_obj;
        else if (stringp(for_obj))
            przyp = atoi(for_obj);

        for_obj = this_player();
    }
    else
        if(stringp(przyp))
            przyp = atoi(przyp);

     if(przyp || !strlen(extra = query_prop(LIVE_S_EXTRA_SHORT)))
         extra = "";

    if(for_obj == this_object())
    {
        switch(przyp)
        {
            case PL_MIA:    return "ty";
            case PL_DOP:    return "ciebie";
            case PL_CEL:    return "tobie";
            case PL_BIE:    return "ciebie";
            case PL_NAR:    return "tob�";
            case PL_MIE:    return "tobie";
        }
    }

    /*
     * If a specific short is set with set_short, use that description
     */
    if(strlen(query_short(przyp)))
        return ::short(for_obj, przyp) + extra;

#ifdef STATUE_WHEN_LINKDEAD
    if(is_linkdead && !TO->query_linkdead_in_combat())
    {
        desc = LV_STATUA[przyp];
        desc += " ";
        przyp = PL_DOP;
    }
    else
#endif
        desc = "";

    //WARNING: To tylko test narazie.
    desc += query_imie(for_obj, przyp);

#if 0
#ifdef MET_ACTIVE
    if (notmet_me(for_obj))
        desc += this_object()->query_nonmet_name(przyp);
    else
#endif
        desc += this_object()->query_met_name(przyp);
#endif

    return desc + extra;
}

public varargs string
plural_short(mixed for_obj, int przyp)
{
    string desc;

    if (!objectp(for_obj))
    {
        if (intp(for_obj))
            przyp = for_obj;
        else if (stringp(for_obj))
            przyp = atoi(for_obj);

        for_obj = this_player();
    }
    else
        if (stringp(przyp))
            przyp = atoi(przyp);

    if (strlen(query_plural_short(przyp)))
        return ::plural_short(for_obj, przyp);

#ifdef STATUE_WHEN_LINKDEAD
    if (is_linkdead && !TO->query_linkdead_in_combat())
    {
        switch(przyp)
        {
            case PL_MIA: desc = LV_STATUY[PL_MIA];  break;
            case PL_DOP: desc = LV_STATUY[PL_DOP];  break;
            case PL_CEL: desc = LV_STATUY[PL_CEL];  break;
            case PL_BIE: desc = LV_STATUY[PL_BIE];  break;
            case PL_NAR: desc = LV_STATUY[PL_NAR];  break;
            case PL_MIE: desc = LV_STATUY[PL_MIE];  break;
        }
        przyp = PL_DOP;
    }
    else
#endif
        desc = "";

#ifdef MET_ACTIVE
    if (notmet_me(for_obj))
        desc += this_object()->query_nonmet_pname(przyp);
    else
#endif
        desc += this_object()->query_met_name(przyp);

    return desc;
}

/*
 * Nazwa funkcji: vbfc_short
 * Opis         : Gives short as seen by previous object
 * Zwraca       :  string holding short()
 */
public varargs string
vbfc_short(int przyp)
{
    object for_obj;

    for_obj = previous_object(-1);
    if (!this_object()->check_seen(for_obj))
    {
        switch(przyp)
        {
            case PL_MIA: return "kto�";
            case PL_DOP: return "kogo�";
            case PL_CEL: return "komu�";
            case PL_BIE: return "kogo�";
            case PL_NAR: return "kim�";
            case PL_MIE: return "kim�";
        }
    }

    return this_object()->short(for_obj, przyp);
}

/*
 * Function name: show_sublocs
 * Description:   Give a description of each sublocation. This is a default
 *        routine merely calling show_cont_subloc in this object for
 *        each sublocation.
 * Arguments:     for_obj: The object for which description is given
 *        slocs:   Identifiers for sublocations
 */
public varargs string
show_sublocs(object for_obj, mixed *slocs)
{
    int il;
    string str;
    mixed data;

    if (!objectp(for_obj))
        for_obj = previous_object();

    if (!sizeof(slocs))
        slocs = m_indexes(cont_sublocs) + ({ 0 });
    else
        slocs = slocs & ( m_indexes(cont_sublocs) + ({ 0 }) );

    //Lekko posortowane sublocki.
    string *standard_slocs = LV_STANDARD_SUBLOCS;

    str = "";
    for(il = 0; il < sizeof(standard_slocs); il++)
    {
        data = this_object()->show_cont_subloc(standard_slocs[il], for_obj);

        if (stringp(data))
            str += data;
        else if (data == 0 && standard_slocs[il] != 0)
            cont_sublocs = m_delete(cont_sublocs, standard_slocs[il]);
    }

    slocs -= standard_slocs;

    for(il = 0; il < sizeof(slocs); il++)
    {
        data = this_object()->show_cont_subloc(slocs[il], for_obj);

        if (stringp(data))
            str += data;
        else if (data == 0 && slocs[il] != 0)
            cont_sublocs = m_delete(cont_sublocs, slocs[il]);
    }

    return str;
}


/*
 * Nazwa funkcji:   long
 * Opis         :   Returns the long-description of this living, and shows also
 *                  the inventory. Handles invisibility and met/nonmet. Note
 *                  that the function does not do any writes! It only returns
 *                  the correct string.
 * Argumenty    :   string for_obj: return an item-description
 *                  object for_obj: return the living-description
 * Zwraca       :   The description string
 */
public varargs string
long(mixed for_obj)
{
    string          cap_pronoun, res;
    object          eob;
    int             a, s;

    if (stringp(for_obj))   /* Items */
        return ::long(for_obj);

    if (!objectp(for_obj))
        for_obj = this_player();

    if (for_obj == this_object())
    {
        res = "Jeste� " + this_object()->query_nonmet_name(PL_NAR) +
            ", znan" + koncowka("ym", "�", "ym") + " jako:\n" +
            this_object()->query_presentation() + ".\n";
    }
    else
    {
        if (!strlen(res = query_long()))
        {
            if (!notmet_me(for_obj))
            {
                res = "Jest " + this_object()->query_nonmet_name(PL_NAR) +
                    ", znan" + koncowka("ym", "�", "ym") + " jako:\n" +
                    this_object()->query_presentation() + ".\n";
            }
            else if (!(this_object()->short(for_obj)))
                return "";
            else
                res = "Jest to " + this_object()->short(for_obj, PL_MIA) + ".\n";

            if (this_object()->query_ghost())
            {
                res = "Jest to ";
#ifdef MET_ACTIVE
                if (notmet_me(for_obj))
                    return res + this_object()->query_nonmet_name(PL_MIA) +
                        ".\n";
                else
#endif
                return res + this_object()->query_name() + ".\n";
            }
        }
        else
        {
            res = check_call(res);
        }
    }

#if 0
    if (this_object()->is_humanoid()
        !this_object()->query_npc() && !this_object()->query_wiz_level() &&
        !this_object()->notmet_me(for_obj))
    {
        if (for_obj == this_object())
            res += "Wygl�dasz na ";
        else
            res += "Wygl�da na ";

        s = sizeof(SD_AVG_TITLES);
        a = ((this_object()->query_average_stat() * s) / 100);
        a = ((a >= s) ? (s - 1) : a);

        res += SD_AVG_TITLES[a] + ".\n";
    }
#endif

    return res + this_object()->show_sublocs(for_obj);
}

private string
z_ze(string str)
{
    int ix;

    switch(lower_case(str[0]))
    {
        case 's':
        case 'S':
            if (str[1] == 'z')
            ix = 2;
            else
            ix = 1;
            break;
        case 'z':
        case 'Z':
            ix = 1;
            break;
        default: return "z " + str;
    }

    if (member_array(str[ix], ({ 'a', 'e', 'i', 'o', 'u', 'y' }) ) == -1)
        return "ze " + str;
    else
        return "z " + str;
}

/*
 * Nazwa funkcji: describe_combat
 * Opis         : This function describes the combat that is going on in
 *                the room this_object() is in. It is an internal function
 *                and should only be called internally by do_glance.
 * Argumenty    : object *livings - the livings in the room.
 */
static string
describe_combat(object *livings)
{
    int     index;
    int     size;
    string  text = "";
    object  victim;
    mapping fights = ([ ]);
    mixed tmp_alv;

    /* Sanity check. No need to print anything if there aren't enough
     * people to actually fight. Note that if there is only one living, it
     * is possible that we fight that living.
     */
    if ((size = sizeof(livings)) < 1)
        return "";

    /* First compile a mapping of all combats going on in the room.
     * The format is the victim as index and an array of all those
     * beating on the poor soul as value. Add this_object() to the
     * list since it isn't in there yet.
     */
    livings += ({ this_object() });
    size++;
    index = -1;
    while(++index < size)
    {
        /* Only if the living is actually fighting. */
        if (objectp(victim = livings[index]->query_attack()))
        {
            if (pointerp(fights[victim]))
                fights[victim] += ({ livings[index] });
            else
                fights[victim] = ({ livings[index] });
        }
    }

    /* No combat going on. */
    if (!m_sizeof(fights))
        return "";

    /* First we describe the combat of the player him/herself. This will
     * be a nice compound message. Start with 'outgoing' combat.
     */
    if (objectp(victim = this_object()->query_attack()))
    {
        fights[victim] -= ({ this_object() });

        /* Victim is fighting back. */
        if (victim->query_attack() == this_object())
        {
            text = "walczysz " + z_ze(victim->query_imie(this_object(),
                PL_NAR));

            if (fights[this_object()])
            fights[this_object()] -= ({ victim });
        }
        else
        {
            text = "koncentrujesz si� na walce " +
                z_ze(victim->query_imie(this_object(), PL_NAR));
        }

        /* Other people helping us attacking the same target. */
        if (sizeof(fights[victim]))
        {
            text = "Wraz " + z_ze(FO_COMPOSITE_LIVE(fights[victim],
                this_object(), PL_NAR)) + ", " + text;
        }
        else
            text = capitalize(text);

        fights = m_delete(fights, victim);

        /* Other people hitting on me. */
        if (index = sizeof(fights[this_object()]))
        {
            text += ", wspart" + victim->koncowka("ym", "�") + " przez " +
                FO_COMPOSITE_LIVE(fights[this_object()], this_object(),
                PL_BIE);
        }

        text += ".\n";
    }
    /* If we aren't fighting, someone or something may be fighting us. */
    else if (index = sizeof(fights[this_object()]))
    {
        text = capitalize(FO_COMPOSITE_LIVE(fights[this_object()],
            this_object(), PL_MIA) + " koncentruj" +
            ((index == 1) ? "e" : "�") + " si� na walce z tob�.\n");
    }

    /* Now generate messages about the other combat going on. This will
     * not be as sophisticated as the personal combat, but it will try to
     * to circumvent printing two lines of 'a fights b' and 'b fights a'
     * since I think that is a silly way of putting things.
     */
    fights = m_delete(fights, this_object());
    livings = m_indices(fights);
    size = sizeof(livings);
    index = -1;
    while(++index < size)
    {
        if (!fights[livings[index]])
            continue;

        /* Victim is fighting (one of his) attackers. */
        if (objectp(victim = livings[index]->query_attack()) &&
            (member_array(victim, fights[livings[index]]) >= 0))
        {
            fights[livings[index]] -= ({ victim });

            /* Walka z victim zostala opisana juz w poprzednim ustepie,
            * teraz tylko wyszczegolniamy kogo victim atakuje */
            if (!fights[victim])
            {
                text += victim->query_Imie(this_object(), PL_MIA) +
                    " koncentruje si� na walce " +
                    z_ze(livings[index]->query_imie(this_object(), PL_NAR)) +
                    ".\n";
            }
            else
            {
                fights[victim] -= ({ livings[index] });

                /* Start with the the name of one of the fighters. */
                text += livings[index]->query_Imie(this_object(), PL_MIA);

                /* Then the people helping the first combatant. */
                if (sizeof(fights[victim]))
                {
                    text += ", wraz " + z_ze(FO_COMPOSITE_LIVE(fights[victim],
                    this_object(), PL_NAR));
                }

                /* Then the second living in the fight. */
                text += " walczy " + z_ze(victim->query_imie(this_object(),
                    PL_NAR));

                /* And the helpers on the other side. */
                if (sizeof(fights[livings[index]]))
                {
                    text += ", wspart" + victim->koncowka("ym", "�")
                    + " przez " + FO_COMPOSITE_LIVE(fights[livings[index]],
                    this_object(), PL_BIE);
                }
                text += ".\n";
            }
        }
        else
        {
            text += capitalize(FO_COMPOSITE_LIVE(fights[livings[index]],
            this_object(), PL_MIA)) + " koncentruj" +
            ((sizeof(fights[livings[index]]) == 1) ? "e" : "�") +
            " si� na walce " +
            z_ze(livings[index]->query_imie(this_object(), PL_NAR)) +
            ".\n";
        }

        fights = m_delete(fights, livings[index]);
        fights = m_delete(fights, victim);
    }

    return text;
}

int objects_filter(object ob)
{
    if (ob->query_no_show() || ob->query_no_show_composite())
        return 0;

    if (ob->query_prop(OBJ_I_DONT_GLANCE))
        return 0;

    if (!ob->query_prop(OBJ_I_VOLUME))
        return 1;

    return ob->query_prop(OBJ_I_VOLUME) > 700 - 10*this_player()->query_skill(SS_AWARENESS);
}

/*
 * Nazwa funkcji: do_glance
 * Opis         : This is the routine describing rooms to livings. It will
 *                print the long or short description of the room, the
 *                possible exits, all visible non-living and living objects
 *                in the room (but not this_object() itself) and then it
 *                will print possible attacks going on. Note that this
 *                function performs a write(), so this_player() will get the
 *                descriptions printed to him/her.
 * Argumenty    : int brief - if true, write the short-description,
 *                            else write the long-description.
 * Zwraca       : int 1 - always.
 */
public int
do_glance(int brief)
{
    int i;
    object env;
    object *ob_list;
    object *lv;
    object *dd, *tmpdd;
    string item;

    /* Po cholere npc'e maj� co� widzie�?:). */
    if (!interactive(this_object()))
        return 0;

    /* Wizard gets to see the filename of the room we enter and a flag if
     * there is WIZINFO in the room.
     */
    string longd, shortd = "", exitsd, fightd = "", restd = "";
    env = environment();
    if (query_wiz_level())
    {
        write(set_color(TO, COLOR_FG_CYAN));
        if (stringp(env->query_prop(OBJ_S_WIZINFO)))
            write("Wizinfo ");

        write(file_name(env) + clear_color(TO) + "\n");
    }

    int dark;
    /* It is dark. */
    if (!CAN_SEE_IN_ROOM(TO))
    {
        shortd = LD_DARK_SHORT;

        if (!stringp(item = env->query_prop(ROOM_S_DARK_LONG)) || !brief)
        {
            dark = 1;
            longd = LD_DARK_LONG;
        }
        else
            longd = item;
    }
    else
    {
        shortd = env->short();
        /* Describe the room and its contents. */
#ifdef DAY_AND_NIGHT
        if((!env->query_prop(ROOM_I_INSIDE) &&
            !env->query_prop(ROOM_I_ALWAYS_BRIGHT) &&
            ((env->pora_dnia() >= MT_POZNY_WIECZOR) ||
            (env->pora_dnia() <= MT_SWIT)) &&
            ((env->query_prop(OBJ_I_LIGHT) +
            query_prop(LIVE_I_SEE_DARK)) < 2)) &&
            this_player()->query_skill(SS_AWARENESS) < 70)
        {
            if (!brief)
            {
                string toWrite = env->polmrok_long();
                if(stringp(toWrite))
                    longd = toWrite;
                else
                    longd = "Panuje tu p�mrok, wi�c nie widzisz wszystkiego dok�adnie.\n";
            }
#if 0
                if (!env->query_noshow_obvious())
                    write(env->exits_description());
                //Czemu wykomentowane?? Gdyby kto� to kiedy� odkomentowa� to niech nie
                // zapomni, �e pasowa�o by doda� kolorek - (Krun)
#endif
            ob_list = all_inventory(env) - ({ this_object() });
            ob_list = filter(ob_list, &not() @ &->query_prop(OBJ_I_DONT_GLANCE));
            ob_list = filter(ob_list, objects_filter);
            lv = FILTER_LIVE(ob_list);

            tmpdd = FILTER_SHOWN(ob_list - lv);
            tmpdd = filter(tmpdd, &->is_in_subloc(0));
            env->oczysc_rzeczy_niewyswietlane();
            env->oczysc_rzeczy_niewyswietlane_plik();

            dd = filter(tmpdd, &env->czy_wyswietlic_rzecz());
        }
        else
#endif DAY_AND_NIGHT
        {
            if (brief)
            {
                if (!env->query_noshow_obvious())
                {
                    item = env->exits_description();

                    if(env->query_default_exits_description())
                        exitsd = item;
                    else
                        exitsd = set_color(TO, env->query_exits_color()) + item + clear_color(TO);
                }
            }
            else
                longd = env->long();

            ob_list = all_inventory(env) - ({ this_object() });
            ob_list = filter(ob_list, &not() @ &->query_prop(OBJ_I_DONT_GLANCE));
            lv = FILTER_LIVE(ob_list);

            tmpdd = FILTER_SHOWN(ob_list - lv);
            tmpdd = filter(tmpdd, &->is_in_subloc(0));

            env->oczysc_rzeczy_niewyswietlane();
            env->oczysc_rzeczy_niewyswietlane_plik();

            dd = filter(tmpdd, &env->czy_wyswietlic_rzecz());
        }

        item = COMPOSITE_FILE->desc_dead(dd, 0, 1);
        if (stringp(item))
            restd += capitalize(item) + ".\n";

        item = environment(this_object())->show_sit();
        if (stringp(item))
            restd += capitalize(item);

        item = COMPOSITE_FILE->desc_live(lv, PL_MIA, 1);
        if (stringp(item))
            restd += capitalize(item) + ".\n";

        /* Give a nice description of the combat that is going on. */
        fightd = describe_combat(lv);
    }

    if(!strlen(longd) || (strlen(shortd) && shortd != longd[0..-3]))
    {
        if(!brief && !dark)
            write(set_color(TO, COLOR_FG_CYAN, COLOR_BOLD_ON) + shortd + "." + clear_color(TO) + "\n");
        else
            write(shortd + ".\n");
    }

    write(longd ?: "");
    write(exitsd ?: "");
    write(restd ?: "");
    write(fightd ?: "");

    return 1;
}

/*
 * Function name: appraise_wiek
 * Description:   Funkcja sciagnieta prawie zywcem z appraise_weight z /std/object.c
 *                Sluzy do oceniania wieku postaci. Lil.
 * Arguments:     num - use this number instead of skill if given.
 */
public string
appraise_wiek(int num)
{
    int value, skill, seed;

    if (!num)
        skill = this_player()->query_skill(SS_APPR_OBJ);
    else
        skill = num;

    skill = ((100 - skill) * 6 / 10) + 1;
    value = this_object()->query_wiek();
    sscanf(OB_NUM(this_object()), "%d", seed);
    skill = random(skill, seed);
    value = cut_sig_fig(value + ((skill % 2 ?: -1) * skill * value / 100), 2);

    //TYM RASOM TROCHE TRUDNIEJ OKRESLIC WIEK, WIEC DODAJEMY TROCHE RANDOMA
    switch(this_player()->query_race())
    {
        case "nizio�ek":
        case "gnom":
        case "krasnolud":
            value= value + random(10, atoi(OB_NUM(this_player()))) - random(10,atoi(OB_NUM(this_player())));
            break;
        case "elf":
            switch(value)
            {
                case 1..2:
                    return "elfie niemowle";
                case 3..10:
                    return "elfie dziecko";
                case 11..30:
                    return "bardzo m�od" + koncowka("ego elfa", "� elfk�", "e elfi�tko");
                case 31..50:
                    return "m�od" + koncowka("ego elfaa", "� elfk�", "e elfi�tko");
                case 51..150:
                    return "dojrza�" + koncowka("ego elfa", "� elfk�", "e elfi�tko");
                default:
                    return "wiekow" + koncowka("ego elfa", "� elfk�", "e elfi�tko");
            }
            break;
        default:
            value = value;
            break;
    }

    return LANG_SNUM(value, PL_MIA, PL_ZENSKI) + " " + ilosc(value, "rok", "lata", "lat");
}

/**
 * Ta funkcja jest wywo�ywana kiedy oceniamy innego gracza.
 *
 * @param num ???
 */
public void
appraise_object(int num)
{
    // �eby bylo �atwiej
    int wiek = this_object()->query_wiek();
    string rasa = this_object()->query_race_name();
    string info_o_wieku;

    if(wiek)
    {
        if (this_object() == this_player())
            info_o_wieku = " Masz " + wiek + " " + ilosc(wiek, "rok", "lata", "lat") + ".";
        else
            info_o_wieku = " Wygl�da " + (rasa != "elf" ? "na jakie� " : "") + appraise_wiek(num) + ".";
    }
    else
        info_o_wieku = " ";

    write("\n" + this_object()->long(this_player()) + "\n");

    if (this_player() == this_object())
    {
        write("Oceniasz, �e wa�ysz " + appraise_weight(num) +
            ", za� twoja obj�to�� wynosi " + appraise_volume(num) +
            "." + info_o_wieku + "\n");
    }
    else
    {
        write("Oceniasz, �e " + short(this_player(), PL_MIA) + " wa�y " +
            appraise_weight(num) + ", za� " + koncowka("jego", "jej", "jego") +
            " obj�to�� wynosi " + appraise_volume(num) + "." +
            info_o_wieku + "\n");
    }
}

#if 0
//Nieu�ywane
/*
 * Nazwa funkcji: query_align_text
 * Opis         :  Returns the alignment text for this object.
 * Zwraca       :  The alignment text.
 */
string
query_align_text()
{
    int a, prc;
    string t, *names;

    a = this_object()->query_alignment();

    if (a < 0)
    {
        prc = (100 * (-a)) / (F_KILL_NEUTRAL_ALIGNMENT * 100);
        names = SD_EVIL_ALIGN;
    }
    else
    {
        prc = (100 * a) / (F_KILL_NEUTRAL_ALIGNMENT * 100);
        names = SD_GOOD_ALIGN;
    }

    a = (sizeof(names) * prc) / 100;

    if (a >= sizeof(names))
        a = sizeof(names) - 1;

    return names[a];
}
#endif
