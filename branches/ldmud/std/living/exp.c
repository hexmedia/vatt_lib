/**
 * \file /std/living/exp.c
 *
 * Ten plik zawiera funkcje do obsługi nowego systemu expa.
 */


/* co ile czasu spada ss */
#define EXP_INTERVAL 1800
/* defaultowa wielkość, o jaką spada ss */
#define EXP_DEFAULT_STEP 4
/* przez ile czasu od ostatniego użycia spada ss */
#define EXP_USAGE_INTERVAL 86400

/* mapping ze szczegółowymi wielkościami, o ile spadają ss */
#define EXP_STEPS_MAP ([\
    SS_STR:10,\
    SS_DEX:8,\
    SS_CON:7,\
    SS_INT:2,\
    SS_DIS:1\
    ])

#define DBG(x) TO->catch_msg(x+"\n")

/* --- */

#include <exp.h>
#include <macros.h>

/* Mapping w postaci:
 *  <numer_skilla> : ({ <czas_ostatniego_uzycia_skilla>,
 *                      <wielkość_o_jaką_spada_skill_z_czasem>
 *                      <ile_czasu_expimy_juz_danego_skilla>
 *                   })
 */
mapping exp_ss_map;

int exp_last_decay;

static int exp_alrm_id = 0;
static mapping exp_delta = ([]);    /* Zmiany w doświadczeniu poszczególnych ss */
static int exp_stat_delta = 0;
static mixed *exp_skill_delta = ({0, 0});

/**
 * Funkcja zmieniająca wartość doświadczenia związanego z ss.
 *
 * @param ss numer cechy/umiejętności
 * @param amount wielkość doświadczenia
 * @param type
 */
public void
increase_ss(int ss, int amount, int type = EXP_PRAKTYCZNY)
{
    if(!mappingp(exp_ss_map))
        exp_ss_map = ([]);

    if (!pointerp(exp_ss_map[ss]))
    {
        int step = EXP_DEFAULT_STEP;

        if (EXP_STEPS_MAP[ss] > 0)
            step = EXP_STEPS_MAP[ss];
        //dla cech tylko praktyka

        if(ss < SS_NO_STATS)
        {
            exp_ss_map[ss] = allocate(3);

            exp_ss_map[ss][0] = time();
            exp_ss_map[ss][1] = step;
        }
        else
        {
            exp_ss_map[ss] = allocate(2);
            exp_ss_map[ss][EXP_TEORETYCZNY] = allocate(3);
            exp_ss_map[ss][EXP_PRAKTYCZNY] = allocate(3);

            exp_ss_map[ss][type][0] = time();
            exp_ss_map[ss][type][1] = step;
        }
    }

    // Dodajemy expa do statów
    if (ss < SS_NO_STATS)
    {
        set_acc_exp(ss, query_acc_exp(ss) + amount);
        exp_delta[ss] += amount;
        exp_stat_delta += amount;
        update_stat(ss);
    }
    // Albo do skilli...
    else
    {
        if(amount > 0)
        {
            int last_use = TO->query_skill_last_use_time(ss);
            int curr_time = time();
            int time_delta = curr_time - last_use;

            if(time_delta < EXP_ROUTINE_INTERVAL && time_delta > 0)
                TO->modify_exp_time(ss, time_delta);

            /* Ustawiamy czas ostatniego uzycia skilla */
            TO->set_skill_last_use_time(ss, time());

#ifdef FAJNY_DEBUGGER
            // DO WYWALENIA!!!
            int old_amount = amount;
#endif FAJNY_DEBUGGER

            /* Modyfikujemy ilośc przyznawanego expa */
            if(amount > 1 && type == EXP_PRAKTYCZNY)
            {
                float d = TO->query_rutyna(ss) || 0.0;
#ifdef FAJNY_DEBUGGER
                DBG("tyle razy przekroczony limit = "+ftoa(d));
#endif
                if(d > 1.0)
                    amount = ftoi(LINEAR_FUNC(1.0, itof(amount), EXP_ROUTINE_LIMIT_BREAK, 1.0, d));

                //Praktyka
                amount = F_EXP_MOD_PRAKTYKA(amount, TO->query_acc_exp(ss, EXP_PRAKTYCZNY),
                    TO->query_acc_exp(ss, EXP_TEORETYCZNY));

                amount = max(1, amount);
            }
#ifdef FAJNY_DEBUGGER
            DBG("********************************************");
            DBG("Exp " + (type ? "teoretczny" : "praktyczny"));
            DBG("Exp bazowy = "+old_amount);
            DBG("Exp po rutynie = "+amount);
            DBG("Bieżący czas = "+time());
            DBG("Ostatnie użycie skilla = "+ last_use);
            DBG("Różnica = "+(time_delta));
            DBG("Expisz już "+TO->query_exp_time(ss)+" sekund.");
            DBG("********************************************");
#endif FAJNY_DEBUGGER
        }

        set_acc_exp(ss, query_acc_exp(ss, type) + amount, type);

        if(sizeof(exp_delta[ss]) != 2)
            exp_delta[ss] = allocate(2);

        exp_delta[ss][type] += amount;
        exp_skill_delta[type] += amount;
        update_skill(ss);
    }
}

/**
 * Funkcja odpowiedzialna za zmniejszanie cech/umiejętności.
 */
public void
decrease_ss()
{
    int size, cur_time;
    int *itab, *curtab;

    if (!mappingp(exp_ss_map))
        exp_ss_map = ([]);

    itab = m_indices(exp_ss_map);
    size = sizeof(itab);
    cur_time = time();

    int j = 0;
    for (int i = 0; i < size; ++i)
    {
        if(itab[i] < SS_NO_STATS)
            curtab = exp_ss_map[itab[i]];
        else if(j != EXP_TEORETYCZNY)
        {
            curtab = exp_ss_map[itab[i]][EXP_TEORETYCZNY];
            i--;
            j = EXP_TEORETYCZNY;
        }
        else
        {
            curtab = exp_ss_map[itab[i]][EXP_PRAKTYCZNY];
            j = EXP_PRAKTYCZNY;
        }

        if((cur_time - curtab[0]) < EXP_USAGE_INTERVAL)
            increase_ss(itab[i], -curtab[1], j);
    }

    if (!exp_alrm_id)
        exp_alrm_id = set_alarm(itof(EXP_INTERVAL), itof(EXP_INTERVAL), decrease_ss);

    exp_last_decay = cur_time;
}

/**
 * Uruchamia proces zmniejszania się cech/umiejętności.
 */
public void
start_ss_decay()
{
    int t_in, t_out, i, size, t_delta, to_decr;
    int* itab, *curtab;

    t_in = time();
    t_out = SECURITY->query_player_file_time(query_real_name());
    t_delta = t_in - t_out;
    //  write("DEBUG: start_ss_decay(): logout time=" + t_delta + ", last decay=" + exp_last_decay + "\n");

    if (!mappingp(exp_ss_map))
        exp_ss_map = ([]);

    itab = m_indices(exp_ss_map);
    size = sizeof(itab);
    int j = 0;
    for (int i = 0; i < size; ++i)
    {
        if(itab[i] < SS_NO_STATS)
            exp_ss_map[itab[i]][0] = exp_ss_map[itab[i]][0] + t_delta;
        else
        {
            exp_ss_map[itab[i]][EXP_TEORETYCZNY][0] = exp_ss_map[itab[i]][EXP_TEORETYCZNY][0] + t_delta;
            exp_ss_map[itab[i]][EXP_PRAKTYCZNY][0] = exp_ss_map[itab[i]][EXP_PRAKTYCZNY][0] + t_delta;
        }
    }

    exp_last_decay += t_delta;

    if (!exp_alrm_id)
    {
        to_decr = 0;
        if ((t_in - exp_last_decay) < EXP_INTERVAL)
            to_decr = EXP_INTERVAL - t_in + exp_last_decay;

        exp_alrm_id = set_alarm(itof(to_decr), itof(EXP_INTERVAL), decrease_ss);
    }
}

/**
 * Funkcja zwracająca wielkość zmian doświadczenia związanego z cechami.
 *
 * @return Wielkość zmian doświadczenia związanego z cechami.
 */
public int
query_exp_stat_delta()
{
    return exp_stat_delta;
}

/**
 * Funkcja zwracająca wielkość zmian doświadczenia związanego z umiejętnościami.
 *
 * @param type dla jakiego typu doświadczenia zwrócić
 *
 * @return Wielkość zmian doświadczenia związanego z umiejętnościami.
 */
public varargs int
query_exp_skill_delta(int type=EXP_PRAKTYCZNY)
{
    return exp_skill_delta[type];
}

#if 0
/**
 * Funkcja tymczasowa służąca do zamiany starych wartosci na nowe.
 */
public void
wprowadz_nowy_system_expa_w_expc()
{
    if(calling_function() == "wprowadz_nowy_system_expa")
    {
        int *m_ind = m_indexes(exp_ss_map);
        for(int i=0;i<sizeof(m_ind);i++)
        {
            //Zmieniamy tylko w przypadku skilli, cechy zostawiamy w spokoju
            if(m_ind[i] >= SS_NO_STATS)
            {
                mixed old_tab = exp_ss_map[m_ind[i]];
                exp_ss_map[m_ind[i]] = allocate(2);
                exp_ss_map[m_ind[i]][EXP_TEORETYCZNY] = old_tab;
                exp_ss_map[m_ind[i]][EXP_PRAKTYCZNY]  = old_tab;
            }
        }
    }
}
#endif
