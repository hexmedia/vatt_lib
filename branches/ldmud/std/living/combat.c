/**
 *  \file /std/living/combat.c
 *
 *  This is a subpart of living.c
 *  All internal combat routines are coded here.
 *
 *  This file is included into living.c
 *
 *  Most of the functionality is moved to the external combat object,
 *  see /std/combat/cbase
 */

#include <log.h>
#include <std.h>
#include <combat.h>
#include <options.h>
#include <wa_types.h>
#include <object_types.h>
#include <stdproperties.h>

#define CRET return combat_extern
#define CEX if(!combat_extern) combat_reload()
#define CREX CEX; CRET

static int      time_to_heal,   /* Healing counter */
                quickness,      /* how quick are we? */
                run_alarm;      /* Alarm used for panic code */

static string   whimpy_dir,     /* Direction to wimpy in if needed */
                whimpy_loc;     /* Location we whimpy from */

static object   my_leader,      /* Pointer to team leader if exist */
               *my_team,        /* Array of team members if leader */
                combat_extern;  /* The external combat object */

static mixed    leftover_list;  /* The list of leftovers */

// Prototypy
varargs public mixed    query_leftover(string organ);
public int              remove_leftover(string organ);
public void             run_away(int na_ile = 1);
public object           query_attack();

/*
 * Function name:   query_combat_file
 * Description:     Gives the name of the file to use for combat.
 * Returns:         The name
 */
public string
query_combat_file()
{
    return COMBAT_FILE;
}

/*
 * Function name:   query_combat_object
 * Description:     Gives the object that is currently acting as external
 *                  combat object
 * Returns:         the combat object
 */
public object
query_combat_object()
{
    CRET;
}

static void
combat_reload()
{
    if(combat_extern)
        return;

    combat_extern = clone_object(query_combat_file());

    /*
     * We can define our own combat object, but it must inherit
     * the original.
     */
    if (function_exists("create_object", combat_extern) != COMBAT_FILE)
    {
        write("ILLEGAL COMBAT OBJECT: " +
              function_exists("create_object", combat_extern) +
              " should be: " + COMBAT_FILE + "\n");
        destruct();
    }

    combat_extern->cb_link(); /* Link me to the combat object */

    /*
     * Configure this living object. For humanoids this includes adding
     * the hand attacks and head, torso etc hitlocations.
     */
    combat_extern->cb_configure();
}

/*
 * Function name:   combat_reset
 * Description:     Reset the combat functions of the living object.
 * Arguments:       arg: Reset argument.
 */
static nomask void
combat_reset()
{
    my_team = 0;
    add_subloc(SUBLOC_WORNA, this_object());
    add_subloc(SUBLOC_WIELD, this_object());
    combat_reload();
}

/**
 * Called from enemy combat object when it thinks we died.
 * @param killer The enemy that caused our death.
 * @param oglusz Czy przeciwnik ma zosta� zabity czy og�uszony.
 */
public varargs void
do_die(object killer, int oglusz=0)
{
    object corpse;
    string *temp;
    int i;

    // Sprawdzamy czy �mier� naprawde si� nam nale�y.
    if((!TO->is_humanoid() && query_hp() > 0) ||
        (TO->is_humanoid() && query_hp(-HA_GLOWA) > 0 && query_hp(-HA_KORPUS) > 0) ||
        (TO->is_player() && oglusz && query_proc_hp(-HA_GLOWA) > 20 && query_proc_hp(-HA_KORPUS) > 10) ||
        query_wiz_level() && query_prop(WIZARD_I_IMMORTAL) || query_ghost())
    {
        return;
    }

    /*To jest sygna� wysy�any do stra�nik�w,
      Ostatnia szansa na uratowanie przed �mierci� go�cia, kt�ry
      by� mo�e przypadkiem nie zas�u�y� na �mier� */
    if(killer->czy_napewno_zasluzyl(this_object()))
    {
        killer->command("przestan atakowac " + OB_NAME(TO));
        return;
    }

    /* Stupid wiz didn't give the objectp to the killer. */
    if (!objectp(killer))
        killer = previous_object();
    /* Bad wiz, calling do_die in someone. */
    if ((MASTER_OB(killer) == WIZ_CMD_NORMAL) ||
        (MASTER_OB(killer) == TRACER_TOOL_SOUL))
        killer = this_interactive();

    CEX; combat_extern->cb_death_occured(killer, oglusz);

    killer->notify_you_killed_me(this_object());

    // ogluszanie rozpatrujemy najpierw
    if((killer->query_option(OPT_FIGHT_MERCIFUL) &&
        query_interactive(this_object()) && !this_object()->query_npc()) ||
        oglusz)
    {
        object paraliz;

        corpse = clone_object("/std/cialo_ogluszone");

        corpse->ustaw_denata();
        corpse->change_prop(CONT_I_WEIGHT, query_prop(CONT_I_WEIGHT));
        corpse->change_prop(CONT_I_VOLUME, query_prop(CONT_I_VOLUME));
        corpse->add_prop(CORPSE_M_RACE, query_rasy());
        corpse->add_prop(CORPSE_M_PRACE,query_prasy());
        corpse->add_prop(CORPSE_I_RRACE, query_rodzaj_rasy() + 1);
        corpse->add_prop(CONT_I_TRANSP, 1);
        corpse->change_prop(CONT_I_MAX_WEIGHT, query_prop(CONT_I_MAX_WEIGHT));
        corpse->change_prop(CONT_I_MAX_VOLUME, query_prop(CONT_I_MAX_VOLUME));
        corpse->add_leftover(query_leftover());
        corpse->add_prop(CORPSE_AS_KILLER,
            ({ killer->query_real_name(), killer->query_nonmet_name() }) );

        this_object()->stop_fight(killer);
        this_object()->stop_fight();
        killer->stop_fight(this_object());

        corpse->move(environment(this_object()), 1);
        move_all_to(corpse);
        this_object()->move(VOID_ROOM);
        //FIXME: Nie wiem czy to przenoszenie ma sens, skoro stam�d od razu gracza wywala..
        // Tymczasowo zakomentowa�em wywalanie z voida, ale nie wiem z jakiego powodu ono
        // tam jest, i wola�bym, �eby si� autor wypowiedzia�. (Krun)

        paraliz = clone_object("/std/paralize/ogluszenie");
        paraliz->set_corpse(corpse);
        paraliz->set_fail_message("Chcesz, chcia�"
            +this_object()->koncowka("e�","a�","e�")
            +" to zrobi�, ale... twe my�li biegn� ju�"
            +" w inn� stron�.\n");

        paraliz->move(this_object(), 1);

        TO->catch_msg("Ogarnia ci� ciemno��.\n");

        //trzeba to logowac tyz! vera.
        log_file(LOG_PLAYEROGLUSZENI,ctime(time()) + " " +
                TP->query_real_name()+" przez "+
        (interactive(killer)?killer->query_real_name():file_name(killer)) +"\n");

        return;
    }

    /* rzeczy zabitego przenosimy na lokacj� */
    move_all_to(environment(this_object()));

    /* po graczach nie zostaj� cia�a */
    if (!(this_object()->query_prop(LIVE_I_NO_CORPSE)) && !interactive(this_object()))
    {
        if (!objectp(corpse = (object)this_object()->make_corpse()))
        {
            corpse = clone_object("/std/corpse");
            corpse->ustaw_denata();
            corpse->change_prop(CONT_I_WEIGHT, query_prop(CONT_I_WEIGHT));
            corpse->change_prop(CONT_I_VOLUME, query_prop(CONT_I_VOLUME));
            corpse->add_prop(CORPSE_M_RACE, query_rasy());
            corpse->add_prop(CORPSE_M_PRACE,query_prasy());
            corpse->add_prop(CORPSE_I_RRACE, query_rodzaj_rasy() + 1);
            corpse->add_prop(CONT_I_TRANSP, 1);
            corpse->change_prop(CONT_I_MAX_WEIGHT,
                query_prop(CONT_I_MAX_WEIGHT));
            corpse->change_prop(CONT_I_MAX_VOLUME,
                query_prop(CONT_I_MAX_VOLUME));
            corpse->add_leftover(query_leftover());

            corpse->add_prop(CORPSE_AS_KILLER,
                     ({ killer->query_real_name(),
                        killer->query_nonmet_name() }) );
            corpse->move(environment(this_object()), 1);
        }
    }

    set_ghost(1);

    if (!this_object()->second_life(killer))
    {
        all_inventory(ENV(TO))->signal_stop_fight(TO);
        TP->add_prop(PLAYER_I_LAST_FIGHT, time());
        this_object()->remove_object();
    }
    else
        corpse->remove_object();
}

//Funkcja u�ywana przez move_all_to.
private static int
filter_to_move_all(object ob)
{
    if(!ob->query_worn() && !ob->query_junior_tool())
        return 1;
}

/*
 * Function name:   move_all_to
 * Description:     Move the entire inventory of this_object to dest
 * Arguments:       dest: destination of the inventory
 */
static nomask void
move_all_to(object dest)
{
    object *oblist;
    int i, ret;

    /* zabieramy graczowi wszystko opr�cz ubra� kt�re ma na sobie
       i junior_tool'a je�li to junior:P */
    if(interactive(this_object()))
        oblist = filter(all_inventory(this_object()), &filter_to_move_all());
    else
        oblist = all_inventory(this_object());

    if (oblist && sizeof(oblist) > 0)
    {
        for (i = sizeof(oblist) - 1; i >= 0; i--)
        {
            /* remove all poisons.. you are dead, so they wo not bother you
             * in your new body.
             */
            if (function_exists("create_object", oblist[i]) == "/std/poison_effect")
            {
                oblist[i]->remove_object();
                continue;
            }
            if (catch(ret = oblist[i]->move(dest)))
                log_file("DIE_ERR", ctime(time()) + " " +
                    this_object()->query_name() + " (" +
                    file_name(oblist[i]) + ")\n");
            else if (ret)
                oblist[i]->move(environment(this_object()));
        }
    }
}

/*************************************************
 *
 * Whimpy routines
 *
 */

/*
 * Function name:   set_whimpy_dir
 * Description:     Sets the favourite direction of the whimpy escape routine
 * Arguments:       str: the direction string
 */
public void
set_whimpy_dir(string str)
{
    whimpy_dir = str;
}

/*
 * Function name:   query_whimpy_dir
 * Description:     Gives the current favourite whimpy escape direction
 * Returns:         The direction string
 */
public string
query_whimpy_dir()
{
    return whimpy_dir;
}


/*************************************************
 *
 * Team routines
 *
 */

/*
 * Function name:    set_leader
 * Description:      Sets this living as a member in a team
 *                   It will fail if this living is a leader itself
 * Arguments:             leader: The objectpointer to the leader of the team
 * Returns:             True if successfull
 */
public int
set_leader(object leader)
{
    if (sizeof(my_team))
        return 0;                        /* We can't be both leader and led */

    my_leader = leader;

    return 1;
}

/*
 * Function name:   query_leader
 * Description:     Gives the object of the living who is the leader of the
 *                  team that we are in.
 * Returns:         The object with the leader, or 0 if we're not in a team
 */
public object
query_leader()
{
    return my_leader;
}

/*
 * Function name:   team_join
 * Description:     Sets this living as the leader of another
 *                  Fails if we have a leader, then we can't lead others.
 * Arguments:            member: The objectpointer to the new member of my team
 * Returns:         True if member accepted in the team
 */
public int
team_join(object member)
{
    if (my_leader)
        return 0;

    if (!member->set_leader(this_object()))
        return 0;

    if (member_array(member, query_team()) >= 0)
        return 1;                /* Already member */

    if (!my_team)
        my_team = ({ member });
    else
        my_team = my_team + ({ member });
    return 1;
}

/*
 * Function name:   query_team
 * Description:     Gives an array with all objects of team members in it
 * Returns:         The array with team members
 */
public object *
query_team()
{
    int i;

    if (!my_team)
        return ({});

    my_team -= ({ 0 });

    return my_team + ({ });
}

/*
 * Function name:   team_leave
 * Description:     Removes this living as the leader of another
 * Arguments:            member: The objectpointer to the member leaving my team
 */
public void
team_leave(object member)
{
    int a;

    a = member_array(member, my_team);
    if (a >= 0)
    {
        my_team[a]->set_leader(0);
        my_team = exclude_array(my_team, a, a);
        if (!sizeof(my_team))
            my_team = 0;
    }
}

/*
 * Function name:   query_team_others
 * Description:     Gives all members/leader that we are joined up with
 * Returns:         The array with all other members
 */
public object *
query_team_others()
{
    object* team;
    if (my_leader) {
        team = my_leader->query_team() + ({ my_leader });
        team -= ({ TO });
        return team;
    }
    else {
        return query_team();
    }
}


/************************************************************
 *
 * Redirected functions to the external combat object
 *
 */

/*
 * Function name:   hit_me
 * Description:     Called to make damage on this object. The actually
 *                    made damage is returned and will be used to change
 *                    the score of the aggressor.
 * Arguments:            wcpen           - ModifiedWeapon class penetration (mpen)
 *                    dt              - damagetype, use MAGIC_DT if ac will not
 *                                    help against this attack.
 *                  hitsuc        - how well did we hit.
 *                    attacker             - Object hurting us
 *                    attack_id           - Special id saying what attack hit us. If
 *                                    you have made a special attack, let the
 *                                  id be -1
 *                  target_hitloc - Optional argument specifying a hitloc
 *                                  to damage.  If not specified or an
 *                                  invalid hitloc is given, a random
 *                                  one will be used.
 * Returns:         The hitresult as given by the external combat object.
 *                        ({ proc_hurt, hitloc description, phit, dam, armour })
 */
varargs public mixed
hit_me(int wcpen, int dt, int hitsuc, object attacker, int attack_id,
       int target_hitloc = -1)
{
    mixed hres;
    int wi;

    /*
     * Start nonplayers when attacked
     */
    start_heart();

    CEX;
    hres = (mixed)combat_extern->cb_hit_me(wcpen, dt, hitsuc, attacker,
        attack_id, target_hitloc);

    if(wi = query_whimpy())
    {   //Uciekamy
        if(((100 * query_hp()) / query_max_hp()) < wi)
        {
            if(run_alarm != 0)
                remove_alarm(run_alarm);

            whimpy_loc = file_name(environment(this_object()));
            run_alarm = set_alarm(1.0, 0.0, run_away);
        }
    }

    return hres;
}

/**
 * Funkcja sprawdza czy uda nam si� czym� zas�oni�.
 *
 * @param ped - p�d broni
 * @param bs  - czy jest to atak z broni strzeleckiej
 *
 * @return Tablica
 *  <ul>
 *    <li> int      - typ obrony </li>
 *    <li> object   - je�li podano typ 1 lub 2(tarcza lub parowanie) to tu znajduje si� obiekt </li>
 *    <li> int      - modyfikator do si�y obrony </li>
 *  </ul>
 *
 * WARNING: Je�li modyfikator do si�y obrony jest ujemny, oznacza to, �e pr�bowali�my
 *          sparowa� bro� go�ymi r�kami, a to zwykle nie ko�czy si� dobrze.
 */
public varargs mixed
query_defense(int ped, int bs = 0)
{
    CREX->cb_query_defense(ped);
}

 /*To ma wywolywac funkcje riot we wszystkich obiektach na lokacji */
public void
call_riot()
{
    object *oblist;
    int i, size;

    oblist = all_inventory(environment(this_object()));
    oblist += ({ environment(this_object()) });

    for (i=0, size = sizeof(oblist); i < size; i++)
    {
        oblist[i]->riot(this_object());
    }
}

/*
 * Function name:   attack_object
 * Description:     Start attacking, the actual attack is done in heart_beat
 * Arguments:            The object to attack
 */
public varargs void
attack_object(object ob, int og = 0)
{
    all_inventory(environment(this_object()))->signal_attack(this_object(), ob);

    //Startujemy bicie serca.
    start_heart();

    CEX; combat_extern->cb_attack(ob, og);
}

/*
 * Function name:   attacked_by
 * Description:     This routine is called when we are attacked.
 * Arguments:            ob: The attacker
 */
public void
attacked_by(object ob)
{
    //npcom kazujemy za�o�y� zbroje i doby� broni je�li jeszcze tego nie zrobi�y.
    if(!interactive(TO) && TO->query_npc() && !TO->is_player())
    {
        //dobywamy broni
//         dobadz_najlepszej_broni();

        //zak�adamy ubrania
//         zaloz_najlepsza_zbroje();
    }

    if (this_object()->query_prop(PLAYER_M_SIT_SIEDZACY) || TO->query_prop(PLAYER_M_SIT_LEZACY))
        command("wstan");

    /* Przestajemy scinac drzewo jesli ktos nas zaatakuje - Rantaur*/
    if(present("scinka_paraliz_zetnij", this_object()) ||
       present("scinka_paraliz_ociosaj", this_object()))
    {
        command("przestan");
    }

    /* Przestajemy szuka� [Krun]*/
    if(present("paraliz_szukania", TO))
        command("przesta� szuka�");

    // Je�li walczymy w spos�b bezpo�redni to nie mo�emy naci�ga� broni.
    if(present("paraliz_naciagania", TO))
        command("przesta� naci�ga�");

    call_riot();
    CEX; combat_extern->cb_attacked_by(ob);
}

/*
 * Function name:   query_not_attack_me
 * Description:     The intended victim may force a fail when attacked.
 *                  If fail, the cause must produce explanatory text himself.
 * Arguments:       who: The attacker
 *                  aid: The attack id
 * Returns:         True if the attacker fails hitting us, false otherwise.
 */
public int
query_not_attack_me(object who, int aid)
{
    return 0;
}

/*
 * Function name:   combat_init
 * Description:     Notes when players are introduced into our environment
 *                  Used to attack known enemies on sight.
 */
nomask void
combat_init()
{
    /*
     * Is this_player() in list of known enemies ?
     * Use attacked_by() so that not forced to swap current enemy
     */
    CEX;

    /* Can't attack people you can't see */
    if(!CAN_SEE(this_object(), this_player()))
        return;

    if(member_array(this_player(), this_object()->query_enemy(-1)) >= 0)
    {
        this_object()->reveal_me(1);
        this_player()->reveal_me(1);
        this_object()->attacked_by(this_player());
    }
}

/*
 * Function name:   heal_living
 * Description:     Heals the living object
 * Arguments:            num: Should be 0, otherwise >0 for lost heart_beats
 */
static nomask void
heal_living(int num)
{
    time_to_heal += (num + 1);
    CEX;
    time_to_heal = (int)combat_extern->cb_heal(time_to_heal);
}

/*
 * Function name: run_away
 * Description:   Runs away from the fight
 * Arguments:     int na_ile - na ile lokacji mamy
 *                spieprzac. Domyslnie 1.
 */
public void
run_away(int na_ile = 1)
{
    if(TO->query_ghost() || !living(TO)) //mo�e umarli�my! Verva.
        return;

    int dont_whimpy;

    if(run_alarm)
    {
        remove_alarm(run_alarm);
        run_alarm = 0;

        if(whimpy_loc && whimpy_loc != file_name(environment(this_object())))
            dont_whimpy = 1;

        whimpy_loc = 0;
    }

    if(dont_whimpy)
        return;

    CEX; combat_extern->cb_run_away(whimpy_dir,na_ile);
}

/*
 * Nazwa funkcji : notify_i_escaped
 * Opis          : Wywolywana przez combat object osoby, ktora uciekla
 *                   z walki we wszystkich swoich przeciwnikach.
 * Argumenty     : ob - obiekt osoby, ktora uciekla
 */
public void
notify_i_escaped(object ob)
{
    if(query_attack() == ob)
    {
        CEX;
        combat_extern->cb_enemy_escaped();
    }
}

/*
 * Function name:   stop_fight
 * Description:     Stops the current fight
 */
public void
stop_fight(mixed elist = 0)
{
    CEX; combat_extern->cb_stop_fight(elist);
}

/*
 * Function name:   query_enemy
 * Description:     Gives information of recorded enemies. If you want to
 *                    know currently fought enemy (if any) call query_attack()
 * Arguments:       arg: Enemy number (-1 == all enemies)
 * Returns:         Object pointer to the enemy
 */
public mixed
query_enemy(int arg)
{
    CREX->cb_query_enemy(arg);
}

/*
 * Function name:   query_attacked_by
 * Description:     Zwraca tablic� przeciwnik�w kt�rzy nas w danej chwili atakuj�.
  * Returns:         Object pointer to the enemy
 */
public object *
query_attacked_by()
{
    CREX->cb_query_attacked_by();
}

/*
 * Function name:   query_attack
 * Description:     Return the attacked object.
 * Returns:         The attacked object.
 */
public object
query_attack()
{
    CEX; return (object)combat_extern->cb_query_attack();
}

/*
 * Function name:   add_enemy
 * Description:     dodaje wroga w czasie walki.
 * Returns:         Obiekt kt�ry chce z nami walczy�.
 */
public void
add_enemy(object ob)
{
    CEX; combat_extern->cb_add_enemy(ob);
}

public void
no_more_attacked_by(object ob)
{
    CEX; combat_extern->cb_no_more_attacked_by(ob);
}

/*
 * Nazwa funkcji : set_combat_speed
 * Opis          : Ustawia szybkosc walki danej postaci.
 * Argumenty     : int speed - ilosc atakow na minute. Przyjmowane
 *                        sa wartosci od 1 do 60. Standardowo, kazdy walczy
 *                        z szybkoscia 15 atakow na minute.
 */
public void
set_combat_speed(int speed)
{
    CEX; combat_extern->cb_set_speed(speed);
}

/*
 * Nazwa funkcji : query_combat_speed
 * Opis          : Zwraca szybkosc walki danej postaci.
 * Funkcja zwraca: int - ile razy w ciagu minuty dana postac atakuje.
 */
public int
query_combat_speed()
{
    CREX->cb_query_speed();
}

/*******************************************
 *
 * Weapon and Armour routines.
 *
 * These are merely registration routines for objects used in combat.
 * The actual management of their function is done in the external combat
 * object. The terminology of weapons, armours, wield and wear remain only
 * for backwards compatibility and confusion.
 */

/*
 * Function name:   wield
 * Description:     Wield a weapon.
 * Arguments:            wep - The weapon to wield.
 * Returns:         True if wielded.
 *                  String with failure message if not wielded.
 */
public mixed
wield(object wep, int slot = 0)
{
     CREX->cb_wield_weapon(wep, slot);
}

/*
 * Function name:   unwield
 * Description:     Unwield a weapon.
 * Arguments:            wep: The weapon to unwield.
 * Returns:         None.
 */
public void
unwield(object wep)
{
    CEX; combat_extern->cb_unwield(wep);
}

/*
 * Function name: query_weapon
 * Description  : Returns the weapon held in a specified location or all the
 *                weapons the living wields when -1 is given as argument.
 * Arguments    : int which - a numeric label describing a weapon location.
 *                            On humanoids this is W_RIGHT etc. Give -1 to
 *                            list all weapons.
 * Returns      : object   - the corresponding weapon or 0.
 *                object * - all weapons in an array for which == -1.
 */
varargs public mixed
query_weapon(int which)
{
    CREX->cb_query_weapon(which);
}

varargs public mixed
query_chwycone(int which)
{
    CREX->cb_query_chwycone(which);
}

/*
 * Function name: update_weapon
 * Description:   Call this function if the stats or skills of a weapon has
 *                  changed.
 * Arguments:     wep - the weapon
 */
public void
update_weapon(object wep)
{
    CEX; combat_extern->cb_update_weapon(wep);
}

/**
 * Zaj�cie slot�w przez dany obiekt.
 *
 * @param ob Obiekt zajmuj�cy sloty.
 */
void
zajmij_sloty(object ob)
{
    CREX->cb_zajmij_sloty(ob);
}

/**
 * Zwraca tablic� widocznych za�o�onych/dobytych rzeczy.
 *
 * @return Tablica widocznych za�o�onych/dobytych rzeczy.
 */
public object*
query_worn_visible()
{
    CREX->cb_query_worn_visible();
}


/**
 * Zwolnienie slot�w zajmowanych przez dany obiekt.
 *
 * @param ob  Obiekt do zwolnienia slot�w.
 */
void
zwolnij_sloty(object ob)
{
    CREX->cb_zwolnij_sloty(ob);
}

/**
 * Funkcja ma za zadanie sprawdzi� na jaki slot/jakie sloty
 * za�o�ona zostanie zbroja.
 *
 * @param arm zbroja, kt�r� zak�adamy
 * @param na_co na co chcemy za�o�y�.
 *
 * @return sloty na kt�re zbroja zostanie za�o�ona.
 */
public mixed
wear_arm_check_slots(object arm, string na_co = 0)
{
    CREX->cb_wear_arm_check_slots(arm, na_co);
}

/**
 * Zak�adamy ciuch b�d� zbroje na dan� cze�� cia�a.
 *
 * @param arm       zak�ada zbroja b�d� ubranie
 * @param na_co     okre�lenie hitlokacji w postaci stringa.
 *
 * @return <ul>
 *   <li> prawda je�li zbroja za�o�ona </li>
 *   <li> komunikat b�edu je�li nieza�o�ona </li>
 * </ul>
 */
public mixed
wear_arm(object arm, string na_co = 0)
{
    CREX->cb_wear_arm(arm, na_co);
}

/*
 * Function name:   remove_arm
 * Description:     Remove an armour
 * Arguments:            arm - The armour.
 */
public void
remove_arm(object arm)
{
    CEX; combat_extern->cb_remove_arm(arm);
}

/*
 * Function name: query_armour
 * Description  : Returns the armour of a given position or lists all armours
 *                worn when -1 is given as argument.
 * Arguments    : int which - a numeric label describing an armour location.
 *                            On humanoids this is TS_HEAD etc. Give -1 to
 *                            list all.
 * Returns      : object * - the corresponding armours or 0 (or ({})).
 *                object * - all armours when -1 is given.
 */
varargs public mixed
query_armour(int which)
{
    CEX; return (int)combat_extern->cb_query_armour(which);
}

/*
 * Function name: update_armour
 * Description:   Call this function when the ac of an armour has changed
 * Arguments:     arm - the armour
 */
public void
update_armour(object arm)
{
    CEX; combat_extern->cb_update_armour(arm);
}

/*
 * Nazwa funkcji : query_slot
 * Opis          : Zwraca informacje o zajetych slotach. Jesli jako argument
 *                   poda sie konkretny identyfikator slota, funkcja zwroci
 *                   obiekt go okupujacy (lub 0, gdy slot jest niezajety).
 *                   Jesli zas jako argument poda sie -1, funkcja zwroci
 *                   tablice z identyfikatorami wszystkich zajetych slotow.
 * Argumenty     : int - identyfikator slota lub -1.
 * Funkcja zwraca: object - obiekt okupujacy podany slot, lub
 *                   int * - tablica z identyfikatorami okupowanych slotow.
 */
public mixed
query_slot(int slot_num)
{
    CREX->cb_query_slot(slot_num);
}

/**
 * Funkcja zwraca tablic� ze wszystkimi za�o�onymi/dobytymi przedmiotami
 *
 * <b>WARNING</b>
 *
 * Funkcja ta zwraca przedmioty od najbardziej zewn�trznego do najbardziej
 * wewn�trznego. Tzn. na 0 miejscu jest rzecz ostatnio za�o�ona/dobyta,
 * a na ostatnim miejscu tablicy jest miejsc najwcze�niej za�o�ona/dobyta.
 *
 * @return tablica ze wszystkimi za�o�onymi/dobytymi przedmiotami.
 */
public object*
query_worn()
{
    CREX->cb_query_worn();
}


/*
 * Function name:   adjust_combat_on_move
 * Description:     Called to let movement affect the ongoing fight. This
 *                  is used to print hunting messages.
 * Arguments:            True if leaving else arriving
 */
public void
adjust_combat_on_move(int leave)
{
    CEX; combat_extern->cb_adjust_combat_on_move(leave);
}

/*
 * Function name:   adjust_combat_on_intox
 * Description:     Called to let intoxication affect combat. This
 *                  is used to do nasty drunk type things *laugh*
 * Arguments:       pintox: %intoxicated
 */
public void
adjust_combat_on_intox(int pintox)
{
    CEX; combat_extern->cb_adjust_combat_on_intox(pintox);
}

/**
 * Konfiguruje poziom spanikowania gracza (dodaje lub odejmuje do/od obecnego poziomu).
 *
 * @param dpan warto�� dodawana
 */
public void
add_panic(int dpan)
{
    CEX; combat_extern->cb_add_panic(dpan);
}

/**
 * @return Poziom spanikowania gracza
 */
public int
query_panic()
{
    CEX; return (int)combat_extern->cb_query_panic();
}

/*
 * Function name:   combat_status
 * Description:     Let the combat object describe the combat status
 * Returns:         Description as string
 */
public string
combat_status()
{
    CREX->cb_status();
}

/*
 * Function name:   combat_data
 * Description:            Let the combat object describe some combat data
 * Returns:            Description as string
 */
public string
combat_data()
{
    CREX->cb_data();
}

/**
 * Wysy�a opis walki do ludzi kt�rzy chc� j� zobaczy�
 *
 * @param str   String do wy�wietlenia na ekranie gracza
 * @param enemy Wr�g z kt�rym walczymy
 * @param arr   Pojedynczy obiekt lub tablica obiekt�w, kt�rym wiadomo�� ma nie zosta�
 *              przekazana
 */
varargs void
tell_watcher(string str, object enemy, mixed arr)
{
    CEX; combat_extern->tell_watcher(str, enemy, arr);
}

/**
 * Dzi�ki tej funkcji dodajemy do cia�a organ.
 *
 * @param obj       �cie�ka do obiektu organu.
 * @param organ     Pe�na odmiana lub mianownik nazwy organu.
 * @param nitems    Liczba organ�w tego typu (-1 = niesko�czenie wiele)
 * @param vbfc      VBFC do sprawdzenia
 * @param hard      Czy to twardy organ, kt�ry zostaje po roz�o�eniu si� cia�a.
 * @param cut       Czy organ da si� wyrwa� czy te� musi by� wyci�ty.
 * @param value     Warto�� organu
 * @param type      Rodzaj przedmiotu
 */
varargs public void
add_leftover(string obj, mixed organ, int nitems, string vbfc,
             int hard, int cut, int value = 0, int type = O_INNE)
{
    if(!sizeof(leftover_list))
        leftover_list = ({ });

    if(!stringp(organ) || !pointerp(organ))
        return;

    remove_leftover(organ);

    leftover_list += ({ ({ obj, organ, nitems, vbfc, hard, cut, value, type }) });
}

/**
 * Funkcja zwraca list� organ�w.
 * Je�li podamy argument to w pierwszej kolejno�ci sprawdzone zostanie czy
 * taki organ istnieje, a p�niej zostanie wy�wietlona tablica z konfiguracj�
 * organu.
 *
 * @param organ organ kt�ry chcemy sprawdzi�, w mianowniku(opt).
 *
 * @return Zale�nie od tego czy podamy organ albo pe�n� list� organ�w
 *         albo tablice z konfiguracj� poszukiwanego organu.
 *         W przypadku kiedy podamy organ ale nie zostanie on znaleziony 0.
 */
varargs public mixed
query_leftover(string organ)
{
    int i;

    if(!sizeof(leftover_list))
        return ({ });

    if(!strlen(organ))
        return leftover_list;

    for(i = 0; i < sizeof(leftover_list); i++)
    {
        if(stringp(leftover_list[i][1]))
        {
            if(organ ~= leftover_list[i][1])
                return leftover_list[i];
        }
        else if(pointerp(leftover_list[i][1]))
        {
            if(organ ~= leftover_list[i][1][0][0])
                return leftover_list[i];
        }
    }
}

/**
 * Funkcja pomocnicza.
 */
private int
leftover_filter(mixed nazwa)
{
    if(pointerp(nazwa))
        nazwa = nazwa[0][0];

    return nazwa;
}

/**
 * Usuwa organ z cia�a.
 *
 * @param organ Organ kt�ry chcemy usun��.
 *
 * @return <li>
 *  <ul> 1 - organ usuni�ty </ul>
 *  <ul> 0 - organu nie uda�o si� usun�� </ul>
 */
public int
remove_leftover(string organ)
{
    int i;

    if(!sizeof(leftover_list))
        return 0;

    leftover_list = filter(leftover_list, not @ &operator(~=)(organ,) @ leftover_filter);
}

/*
 * Function name: add_attack_delay
 * Description:   Set the LIVE_I_ATTACK_DELAY prop properly.
 *                Use this function if possible instead of altering the prop.
 * Arguments:          secs - How many seconds
 *                type - How it should be added.
 *                       0 - Just add it.
 *                       1 - Minimum value, i.e newtime=MAX(oldtime,secs)
 */
public void
add_attack_delay(int secs, int type)
{
    int old, new;
    old = new = query_prop(LIVE_I_ATTACK_DELAY);
    if (type)
    {
        if (secs > old)
            new = secs;
    }
    else
        new += secs;
    if (new != old) add_prop(LIVE_I_ATTACK_DELAY, new);
}

/*
 * Function name: add_stun
 * Description:   Stun the living with the LIVE_I_STUNNED prop.
 *                Use this function if possible instead of altering the prop.
 */
public void
add_stun()
{
    add_prop(LIVE_I_STUNNED, query_prop(LIVE_I_STUNNED) + 1);
}

/*
 * Function name: remove_stun
 * Description:   Remove a stun made by add_stun.
 *                Use this function if possible instead of altering the prop.
 */
public void
remove_stun()
{
    int tmp = query_prop(LIVE_I_STUNNED) - 1;
    if (tmp <=  0)
        remove_prop(LIVE_I_STUNNED);
    else
        add_prop(LIVE_I_STUNNED, tmp);
}

public mixed
query_to_stun()
{
    CEX; combat_extern->cb_query_to_stun();
}

/**
 * Funkcja tylko dla livinga, dobywaj�ca najlepszej broni je�li living
 * jest atakowany i nie ma nic dobytego.
 */
public nomask int
dobadz_broni()
{
    CREX->cb_dobadz_broni();
}

/**
 * Funkcja zak�adaj�ca najlepsze zbroje przy �adowaniu si� livinga,
 * �eby nago nie gania�.
 */
public nomask int
zaloz_zbroje()
{
    CREX->cb_zaloz_zbroje();
}

/**
 * Funkcja wywo�ywana kiedy dostajemy z broni strzeleckiej.
 *
 * @param strzelajacy   strzelajacy z broni.
 * @param pocisk        pocisk kt�rym dostaniemy
 * @param bron          bro� z kt�rej by�o strzelane
 * @param sila          si�a strza�u
 * @param szybkosc      szybko�� strza�u
 * @param sila_naciagu  si�a naci�gu.
 */
public int
bs_hit_me(object strzelajacy, object pocisk, object bron, int sila, int szybkosc, int sila_naciagu)
{
    CREX->cb_bs_hit_me(strzelajacy, pocisk, bron, sila, szybkosc, sila_naciagu);
}

/**
 * @param id    Identyfikator ataku.
 *
 * @return Obiekt przyporz�dkowany do ataku o podanym id lub 0 je�li takiego
 *         obiektu nie ma.
 */
public object
query_attack_object(int id)
{
    CREX->cb_query_attack_object(id);
}

/**
 * @param id    Identyfikator ataku.
 *
 * @return Rodzaj nazwy ataku.
 */
public nomask int
query_attack_rodzaj(int id)
{
    CREX->cb_query_attack_rodzaj(id);
}

/**
 * @return Identyfikatory wszystkich atak�w.
 */
public nomask int *
query_attack_id()
{
    CREX->query_attack_id();
}

/**
 * @return Identyfikatory wszystkich hitlokacji.
 */
public nomask int *
query_hitloc_id()
{
    CREX->query_hitloc_id();
}

/**
 * @return Identyfikatory wszystkich obszar�w hitlokacji.
 */
public nomask int *
query_hitloc_area_id()
{
    CREX->query_hitloc_area_id();
}

/**
 * @param id hitlokacji
 *
 * @return Rodzaj nazwy hitlocka.
 */
public nomask int
query_hitloc_rodzaj(int id)
{
    CREX->cb_query_hitloc_rodzaj(id);
}

/**
 * @param id hitlokacji
 *
 * @return Czy nazwa hitlocka jest w liczbie mnogiej.
 */
public nomask int
query_hitloc_mn(int id)
{
    CREX->cb_query_hitloc_mn(id);
}

/**
 * Dzi�ki tej funkcji sprawdzamy opis danego ataku.
 *
 * @param id    Identyfikator hitlokacji
 * @param przyp przypadek w jakim ma zosta� podany opis ataku,
 *              nieaktywne w przypadku zwyk�ych cios�w.
 *
 * @return opis hitlokacji.
 */
public nomask string
query_hitloc_desc(int id, int przyp = 0)
{
    CREX->cb_hitloc_desc(id);
}

/**
 * @param id    Identyfikator hitlokacji
 *
 * @return procentowy udzia� hitlokacji (-1 brak hitlokacji).
 */
public nomask int
query_hitloc_proc(int id)
{
    CREX->cb_query_hitloc_proc(id);
}

/**
 * @param id Identyfikator obszaru
 *
 * @return List� hitloc�w w danym obszarze.
 */
public int *
query_hitloc_area_members(int id)
{
    CREX->query_hitloc_area_members(id);
}

public string
query_attack_desc(int id, int przyp = PL_DOP)
{
    CREX->cb_attack_desc(id, przyp);
}

/**
 * Funkcja ta sprawdza czym gracz mo�e si� broni�, pozwala to na wykluczenie
 * sposob�w obrony, kt�re s� dla gracza niedost�pne - gdy gracz nie ma broni
 * nie mo�e parowa� broni�, gdy nie ma tarczy tarcz�.
 *
 * @return tablica
 *  <ul>
 *    <li> 1/0 - uniki </li>
 *    <li> 1/0 - parowanie broni� </li>
 *    <li> 1/0 - os�ona tarcz� </li>
 *  </ul>
 */
public int *
query_sposoby_obrony()
{
    CREX->cb_query_sposoby_obrony();
}

/** ********************************************************************** **
 ** *************************** Z A S � O N Y  *************************** **
 ** ********************************************************************** **/

/**
 * Dzi�ki tej funkcji mo�emy kaza� livingowi kogo� zas�oni�.
 *
 * @param kogo    living kt�rego zas�aniamy.
 *
 * @return 1/0  sukces/pora�ka
 */
public int
zaslon(object kogo)
{
    CREX->cb_zaslon(kogo);
}

/**
 * Dzi�ki tej funkcji mo�emy kaza� livingowi spr�bowa� prze�ama� zas�on�
 * kogo� na kim�, je�li nie podamy kogo zas�on� chcemy prze�ama� przeciwnik
 * zostanie wybrany losowo.
 *
 * @param na_kim
 * @param kogo
 *
 * @return 1/0 sukces/pora�ka
 */
public int
przelam_zaslone(object na_kim, object kogo = 0)
{
    CREX->cb_przelam_zaslone(na_kim, kogo);
}

/**
 * Dzi�ki tej funkcji mo�emy kaza� livingowi przesta� zas�ania� innego livinga
 *
 * @return 1/0 sukces/pora�ka
 */
public int
przestan_zaslaniac()
{
    CREX->cb_przestan_zaslaniac();
}

/**
 * Dodajemy zas�aniaj�cego nas.
 *
 * @param zaslaniajacy
 */
public void
dodaj_zaslaniajacego(object zaslaniajacy)
{
    CREX->cb_dodaj_zaslaniajacego(zaslaniajacy);
}

/**
 * Usuwamy zas�aniaj�cego nas.
 *
 * @param zaslaniajacy
 */
public void
usun_zaslaniajacego(object zaslanajacy)
{
    CREX->cb_usun_zaslaniajacego(zaslanajacy);
}

/**
 * @return Liste zas�aniaj�cych.
 */
public object *
query_zaslaniajacy()
{
    CREX->cb_query_zaslaniajacy();
}

/**
 * @return poziom nago�ci
 */
public int
query_poziom_nagosci()
{
    CREX->cb_poziom_nagosci();
}

/**
 * Funkcja sprawdza czy zostali�my zabici czy te� nie,
 * czyli sprawdza czy mamy na odpowiednich obszarach
 * odpowiedni� ilo�� punkt�w hp aby �y�.
 *
 * @param kto   Obiekt zabijaj�cy w razie czego:)
 * @param stun  Czy obiekt mia� zosta� og�uszony, je�li tak to szansa na
 *              zabicie go jest troche mniejsza.
 */
public int
check_killed(object kto, int stun)
{
    CREX->cb_check_killed(kto);
}

/**
 * Funkcja sprawdza czy kt�ry� z naszych obszar�w nie zosta� wy��czony z akcji.
 *
 * @param kto   �ci�gaj�cy hp z obszaru.
 */
public int
check_areas(object kto)
{
    //FIXME: Komunikaty do wymiany:P
    int lhp, php;
    mixed tmp;

    lhp = query_proc_hp(-HA_L_REKA);
    php = query_proc_hp(-HA_P_REKA);

    if(lhp < 30 && php < 30)
    {
        if((tmp = query_weapon(W_BOTH))  || (tmp = query_chwycone(W_BOTH)))
            tmp->unwield_me();

        if((tmp = query_weapon(W_LEFT))  || (tmp = query_chwycone(W_LEFT)))
            tmp->unwield_me();

        if((tmp = query_weapon(W_RIGHT)) || (tmp = query_chwycone(W_RIGHT)))
            tmp->unwield_me();
    }

    if(lhp < 30)
    {
        if((tmp = query_weapon(W_RIGHT)) || (tmp = query_chwycone(W_RIGHT)))
            tmp->unwield_me();

        if((tmp = query_weapon(W_BOTH))  || (tmp = query_chwycone(W_BOTH)))
            tmp->unwield_me();
    }

    if(php < 30)
    {
        if((tmp = query_weapon(W_BOTH))  || (tmp = query_chwycone(W_BOTH)))
            tmp->unwield_me();

        if((tmp = query_weapon(W_RIGHT)) || (tmp = query_chwycone(W_RIGHT)))
            tmp->unwield_me();
    }

    if(query_proc_hp(-HA_L_NOGA) < 30)
    {
        if((tmp = query_weapon(W_FOOTL)) || (tmp = query_chwycone(W_FOOTL)))
            tmp->unwield_me();
    }

    if(query_proc_hp(-HA_P_NOGA) < 30)
    {
        if((tmp = query_weapon(W_FOOTR)) || (tmp = query_chwycone(W_FOOTR)))
            tmp->unwield_me();
    }
}
