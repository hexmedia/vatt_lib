/**
 * \file /std/living/wounds.c
 *
 * Plik ten jest cz�ci� living.c
 * Znajduj� si� tutaj funkcje do obs�ugi ran.
 *
 * Plik ten includowany jest do living.c
 */

#include <macros.h>

#define QEXC (TO->query_combat_object())

/**
 * Opisuje moment powstania rany.
 */
public mixed
describe_new_wound(int hitloc, int wound, object fo = 0)
{
    if(!wound)
        return ({"", "", ""});

    return ({
        " " + (fo ? TO->query_Imie(fo, PL_MIA) : QCIMIE(TO, PL_MIA)) +
        ": NOWA RANA: lokacja=" + hitloc + ", rana=" + wound + ".",
        " NOWA RANA: lokacja=" + hitloc + ", rana=" + wound + ".",
        " " + QCIMIE(TO, PL_MIA) + ": NOWA RANA: lokacja=" + hitloc +
        ", rana=" + wound + "."
        });
}

/**
 * Opisuje moment uszkodzenia ko�czyny.
 */
public mixed
describe_new_handicap(int hitloc, object fo = 0)
{
    if (hitloc < -1000)
        hitloc /= -1000;

    if (hitloc <= 0)
        return ({"", "", ""});

    foreach (string area : QEXC->query_hitloc_areas())
    {
        if (member_array(hitloc, QEXC->query_hitloc_area_members(area)) > -1)
        {
            return ({
                " " + (fo ? TO->query_Imie(fo, PL_MIA) : QCIMIE(TO, PL_MIA)) + ": NOWY HANDICAP: " + area + ".",
                " NOWY HANDICAP: " + area + ".",
                " " + QCIMIE(TO, PL_MIA) + ": NOWY HANDICAP: " + area + "."
                });
        }
    }

    return ({"", "", ""});
}

