/*
 * /std/plan_pogodowy.c
 *
 * Plik zawieraj�cy prototyp planu pogodowego
 */

#pragma save_binary
#pragma strict_types

#include <pogoda.h>

private static  int     zachmurzenie,   /* stopie� zachmurzenia */
                        opady,          /* stopie� i rodzaj opad�w */
                        wiatry,         /* stopie� wietrzno�ci */
                        temperatura,    /* temperatura */
                        ilosc_sniegu,   /* ile na lokacji jest �niegu */
                        ilosc_wody,     /* ile na lokacji jest wody (tej napadanej) */
                        wschod,         /* godzina dzisiejszego wschodu s�o�ca(timestamp) */
                        zachod,         /* godzina dzisiejszego zachodu s�o�ca(timestamp) */
                        wsp_x, wsp_y,   /* wsp�rz�dne planu pogodowego */
                        last_update;    /* czas ostatniego update'a danych w bazie */
private static  object  warunki;        /* obiekt warunk�w pogodowych, z kt�rych s� pobierane,
                                           je�li nie s� ustawione */
varargs int co_pada(int opady);
void update_pogoda();

void ustaw_wspolrzedne(int x, int y)
{
    wsp_x = x;
    wsp_y = y;

    warunki = POGODA_OBJECT->get_warunki();

    update_pogoda();
}

/*
 * Nazwa funkcji: short
 * Opis         : Funkcja zwraca opis obiektu.
 * Zwraca       : string - opis obiektu
 */
string
short()
{
    return "standardowy plan pogodowy";
}

void
update_pogoda()
{
    if(!warunki || (last_update && last_update == warunki->get_last_update(wsp_x, wsp_y)))
        return;

    ilosc_wody = ilosc_sniegu = 0;

    zachmurzenie    = warunki->get_zachmurzenie(wsp_x, wsp_y);
    opady           = warunki->get_opady(wsp_x, wsp_y);
    wiatry          = warunki->get_wiatry(wsp_x, wsp_y);
    temperatura     = warunki->get_temperatura(wsp_x, wsp_y);
    last_update     = warunki->get_last_update(wsp_x, wsp_y);

    switch (co_pada(opady))
    {
        case PADA_DESZCZ:
            ilosc_wody = 1;
            break;

        case PADA_SNIEG:
        case PADA_GRAD:
            ilosc_sniegu = 1;
            break;
    }
}

/*
 * Nazwa funkcji: set_zachmurzenie
 * Opis         : Funkcja ustawia zachmurzenie w danym obiekcie.
 * Argumenty    : int zach - nowe zachmurzenie
 */
void
set_zachmurzenie(int zach)
{
    zachmurzenie = zach;
}

/*
 * Nazwa funkcji: set_opady
 * is         : Funkcja ustawia opady w danym obiekcie.
 * Argumenty    : int opad - nowe opady
 */
void
set_opady(int opad)
{
    opady = opad;
}

/*
 * Nazwa funkcji: set_wiatry
 * Opis         : Funkcja ustawia wiatry w danym obiekcie.
 * Argumenty    : int wiatr - nowe wiatry
 */
void
set_wiatry(int wiatr)
{
    wiatry = wiatr;
}

/*
 * Nazwa funkcji: set_temperatura
 * Opis         : Funkcja ustawia temperatur� w danym obiekcie.
 * Argumenty    : int temp - nowa temperatura
 */
void
set_temperatura(int temp)
{
    temperatura = temp;
}

/**
 * Ustawia w minutach dat� najbli�szego wschodu s�o�ca.
 *
 * @param t czas nabli�szego wschodu s�o�ca
 */
void set_wschod(int t)
{
    wschod = t;
}

/**
 * Ustawia w minutach dat� najbli�szego zachodu s�o�ca.
 *
 * @param t czas nabli�szego zachodu s�o�ca
 */
void set_zachod(int t)
{
    zachod = t;
}

/*
 * Nazwa funkcji: get_zachmurzenie
 * Opis         : Funkcja zwraca zachmurzenie w danym obiekcie.
 * Zwraca       : int - zachmurzenie
 */
int
get_zachmurzenie()
{
    update_pogoda();
    return zachmurzenie;
}

/*
 * Nazwa funkcji: get_opady
 * Opis         : Funkcja zwraca opady w danym obiekcie.
 * Zwraca       : int - opady
 */
int
get_opady()
{
    update_pogoda();
    return opady;
}

/*
 * Nazwa funkcji: get_wiatry
 * Opis         : Funkcja zwraca wiatry w danym obiekcie.
 * Zwraca       : int - wiatry
 */
int
get_wiatry()
{
    update_pogoda();
    return wiatry;
}

/**
 * @return Aktualnie panuj�c� temperature
 */
int
get_temperatura()
{
    update_pogoda();
    return temperatura;
}

/**
 * @return Timestamp'a z dzisiejszym wschodem s�o�ca.
 */
int get_wschod()
{
    update_pogoda();
    return wschod;
}

/**
 * @return Timestamp'a z dzisiejszym zachodem s�o�ca.
 */
int get_zachod()
{
    update_pogoda();
    return zachod;
}

/**
 * @return Opis eventa zwi�zanego z zachmurzeniem.
 *
 * @param zachmurzenie wielko�� zachmurzenia
 */
string
event_zachmurzenie(int zachmurzenie)
{
    return 0;
}

/**
 * Zwraca opis eventa zwi�zanego z opadami.
 *
 * @param opady rodzaj i wielko�� opad�w
 */
string
event_opady(int opady)
{
    return 0;
}

/**
 * Zwraca opis eventa zwi�zanego z wiatrem.
 *
 * @param wiatr si�a wiatru
 */
string
event_wiatr(int wiatr)
{
    return 0;
}

/**
 * Zwraca opis eventa zwi�zanego z temperatur�.
 *
 * @param temperatura wielko�� temperatury
 */
string
event_temperatura(int temperatura)
{
    return 0;
}

/**
 * Zwraca informacj� o tym, jaki jest typ opad�w.
 *
 * @return typ opad�w
 */
varargs int
co_pada(int opady = get_opady())
{
    switch(opady)
    {
        case POG_OP_D_L:
        case POG_OP_D_S:
        case POG_OP_D_C:
        case POG_OP_D_O:
            return PADA_DESZCZ;
        case POG_OP_S_L:
        case POG_OP_S_S:
        case POG_OP_S_C:
            return PADA_SNIEG;
        case POG_OP_G_L:
        case POG_OP_G_S:
            return PADA_GRAD;
        default:
            return NIC_NIE_PADA;
    }
}

/**
 * Zwraca informacj� o tym, jak mocno pada.
 *
 * @return moc opad�w
 */
int
moc_opadow()
{
    switch (get_opady())
    {
        case POG_OP_D_L:
        case POG_OP_S_L:
        case POG_OP_G_L:
            return PADA_LEKKO;
        case POG_OP_D_S:
        case POG_OP_S_S:
        case POG_OP_G_S:
            return PADA_SREDNIO;
        case POG_OP_D_C:
        case POG_OP_S_C:
            return PADA_MOCNO;
        case POG_OP_D_O:
            return PADA_BMOCNO;
        default:
            return NIC_NIE_PADA;
    }
}

int
get_ilosc_wody()
{
    return ilosc_wody;
}

int
get_ilosc_sniegu()
{
    return ilosc_sniegu;
}