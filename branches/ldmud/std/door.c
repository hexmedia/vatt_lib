/**
 * \file /std/door.c
 *
 * Standardowe drzwi oparte o standaryzowany plik /lib/open_close.c
 *
 * Plik zbudowany na podstawie starych drzwi.
 * @author Krun
 * @date Grudzie� 2007
 *
 */
#pragma save_binary
#pragma strict_types

inherit "/std/object";
inherit "/lib/open_close.c";

#include <pl.h>
#include <macros.h>
#include <ss_types.h>
#include <cmdparse.h>
#include <language.h>
#include <stdproperties.h>

string  other_room,             /* id drzwi z drugiej strony */
        door_id,                /* id tych drzwi, je�li nie ustawimy to automatycznie ustawiony zostanie key */
        open_desc,              /* Opis otwartych drzwi */
        closed_desc;            /* Opis zamkni�tych drzwi */

mixed   sposob,                 /* spos�b przenoszenia graczy */
        other_door,             /* Drzwi po drugiej stronie */
        pass_command = ({}),    /* Komenda u�ywana do przej�cia przez drzwi */
        fail_pass,              /* Komunikat przy nieudanym przechodzniu */
        pass_mess1,             /* Komunikat przy przechodzeniu dla tych u nas w roomie */
        pass_mess2,             /* Komunikat przy przechodzeniu dla w docelowym roomie */
        open_mess3,             /* ew komunikat wysy�any na lokacje po drugiej stronie drzwi */
        close_mess3,            /* jw dla zamykania */
        unlock_mess3,           /* jw dla odblokowywania/otwierania kluczem */
        lock_mess3,             /* jw dla blokowania/zamykania kluczem */
        pick_mess3,             /* jw dla otwierania wytrychem */
        breakdown_mess3,        /* jw dla wy�amywania */
        my_pass_mess;           /* Komunikat wy�wietlany */

int     is_oneside;             /* Uk�on w stron� okien, bo mog� by� jednostronne. */

/*
 * Prototypy:
 */
void load_other_door();
static void remove_door_info(object dest);
public void set_pass_mess(string s1);
public varargs void set_fail_pass(...);
void remove_object();
string *query_pass_command();

/**
 * Inicjalizujemy drzwii.
 */
void
create_door()
{
}

/**
 * Inicjalizujemy obiekt.
 */
void
create_object()
{
    config_open_close();

    add_prop(OBJ_I_WEIGHT, 60000);
    add_prop(OBJ_I_VOLUME, 80000);
    add_prop(DOOR_I_HEIGHT, 220); /* Standardowa wysokosc 2,2 metry. */
    add_prop(OBJ_I_NO_GET, 1);
    set_no_show_composite(1);

    set_pick(50);
    set_breakdown(50);

    set_open(1);
    set_locked(0);

    create_door();

    if(query_key() && query_lock_name())
        add_item(query_lock_name(PL_BIE), (query_lock_desc() ? query_lock_desc() : "Zwyk�a dziurka na klucz.\n"));
}

/**
 * Resetujemy drzwi.
 */
void
reset_door()
{
}

/**
 * Resetujemy obiekt.
 */
nomask void
reset_object()
{
    reset_door();
}

int pass_door(string str);
/*
 * Function name: init
 * Description:   Initalize the door actions
 */
void
init()
{
    ::init();

    if(pointerp(query_pass_command()))
    {
        for(int i = 0 ; i < sizeof(query_pass_command()) ; i++)
        {
            if(stringp(query_pass_command()[i]))
                add_action(pass_door, explode(query_pass_command()[i], " ")[0]);
        }
    }

    init_open_close();
}

/**
 * Dzi�ki tej funkcji mo�emy zapuka� w drzwi. Autorem orgina�u jest
 * Lil.
 *
 * @param cicho 1 - nie wy�wietlamy komunikat�w, 0 wy�wietlamy
 * @return 1        - sukces
 * @return 0        - pora�ka
 * @return string   - pora�ka przy czym wy�wietlany jest zwr�cony string
 */
public varargs int
knock_me(int cicho = 0)
{
    notify_fail("Zapukaj gdzie ?\n");

    write("Pukasz do " + TO->short(PL_DOP) + ".\n");
    // czy tu powinno byc saybb? bo to wyswietla tylko tym, co widza zdarzenie /d
    saybb(QCIMIE(this_player(),PL_MIA) + " puka do " +
        QSHORT(TO, PL_DOP) + ".\n");
    tell_room(TO->query_other_room(), "Kto� puka do "+
              QSHORT(TO, PL_DOP) + ".\n");

    return 1;
}
/**
 * Dzi�ki tej funkcji mo�emy uderzy� w drzwi.
 * Funkcja napisana na podstawie zapukaj by Lil.
 *
 * @param cicho 1 - nie wy�wietlamy komunikat�w, 0 wy�wietlamy
 * @return 1        - sukces
 * @return 0        - pora�ka
 * @return string   - pora�ka przy czym wy�wietlany jest zwr�cony string
 */
int
bang_me(int cicho = 0)
{
    write("Walisz w " + short(PL_BIE) + ".\n");
    // czy tu powinno byc saybb? bo to wyswietla tylko tym, co widza zdarzenie /d
    say(QCIMIE(this_player(),PL_MIA) + " wali w " +
        QSHORT(TO, PL_BIE) + ".\n");
    tell_room(TO->query_other_room(), "Kto� wali w"+
              QSHORT(TO, PL_BIE) + ".\n");

    return 1;
}

/**
 * Funkcja wywo�ywana przy przechodzeniu przez drzwi.
 *
 * @param tp przechodz�cy
 */
public int pass_it(object tp)
{
}

/**
 * Funkcja wywo�ywana przy przechodzeniu przez drzwi.
 */
int
pass_door(string arg)
{
    int dexh;

    if (!other_door)
        load_other_door();

    string curCmd = query_verb() + (stringp(arg) ? " " + arg : "");

    //Sprawdzamy czy komenda pasuje;
    if(sizeof(query_pass_command()))
    {
        if(member_array(curCmd, query_pass_command()) < 0)
            return 0;
    }

    /*
     * Sprawdzamy czy gracz zmie�ci sie w tych drzwiach.
     */
    dexh = 2 + (this_player()->query_stat(SS_DEX) / 25);

    if (TO->query_open())
    {
        /*
         * Dajmu mu szanse je�li ma wystarczaj�co du�� zr�czno�c to da rade,
         * je�li nie to... no c�:)
         */
        if ((int)this_player()->query_prop(CONT_I_HEIGHT) >
            query_prop(DOOR_I_HEIGHT) * dexh)
        {
            write("Jeste^s zbyt du^z" +
                this_player()->koncowka("y", "a", "e") +
                " i za ma^lo zr^eczn" +
                this_player()->koncowka("y", "a", "e") +
                ", ^zeby si^e przecisn^a^c przez " + short(PL_BIE));
            return 1;
        }
        else if ((int)this_player()->query_prop(CONT_I_HEIGHT) >
            query_prop(DOOR_I_HEIGHT))
        {
            write("Z wielkim trudem przeciskasz si^e przez " + short(PL_BIE) + ".\n");
            saybb(QCIMIE(this_player(), PL_MIA) + " z wielkim trudem " +
                "przeciska si^e przez " + this_object()->short(PL_BIE) + ".\n");
        }
        else if (my_pass_mess)
            tell_object(this_player(), capitalize(check_call(my_pass_mess)) + "\n");

        this_player()->move_living(sposob, other_room);
    }
    else
    {
        if(sizeof(fail_pass))
        {
            if(functionp(fail_pass[0]))
            {
                function s = fail_pass[0];
                write(s());
            }
            else if(stringp(fail_pass[0]))
                write(check_call(fail_pass[0]));
        }
        else
            write("Nie mo�esz przej�� przez zamkni�t" +
                TO->koncowka("y", "a", "e", "ci", "e") + " " + TO->short(PL_BIE) + ".\n");
    }

    pass_it(TP);

    return 1;
}

/**
 * Ustawiamy opis drzwi
 * @param desc opis drzwi
 */
void
set_door_desc(mixed desc)
{
    set_long(desc);
}

/**
 * @return opis drzwi
 */
string
query_door_desc()
{
    return query_long();
}

/**
 * Ustawiamy opis drzwi kiedy s� otwarte.
 * @param desc opis otwartych drzwi
 */
void
set_open_desc(string desc)
{
    open_desc = desc;
}

/**
 * @return Opis otwartych drzwii.
 */
string
query_open_desc()
{
    return open_desc;
}

/**
 * Ustawiamy opis zamkni�tych drzwi.
 * @param desc opis zamkni�tych drzwi
 */
void
set_closed_desc(string desc)
{
    closed_desc = desc;
}

/**
 * @return opis zamkni�tych drzwi.
 */
string
query_closed_desc()
{
    return closed_desc;
}

/**
 * Konwertujemy nazwy wyj�� do j�zyka poskiego.
 */
public string
convert_exit_desc_to_polish(string x)
{
    switch (plain_string(x))
    {
        case "n":
        case "polnoc": return "p^o^lnoc";
        case "ne":
        case "polnocny-wschod": return "p^o^lnocny-wsch^od";
        case "e":
        case "wschod": return "wsch^od";
        case "se":
        case "poludniowy-wschod": return "po^ludniowy-wsch^od";
        case "s":
        case "poludnie": return "po^ludnie";
        case "sw":
        case "poludniowy-zachod": return "po^ludniowy-zach^od";
        case "w":
        case "zachod": return "zach^od";
        case "nw":
        case "polnocny-zachod": return "p^o^lnocny-zach^od";
        case "d":
        case "dol": return "d^o^l";
        case "u":
        case "gora": return "g^ora";
        default: return x;
    }
}

/**
 * Ustawiamy komendy dzi�ki kt�rym b�dziemy mogli przechodzi� przez drzwi.
 * @param s1 komenda lub tablica z komendami
 * @param s2 komunikat dla tych na lokacji z graczem
 * @param s3 komunikat dla tych na drugiej lokacji
 */
public varargs void
set_pass_command(mixed s1, mixed s2, mixed s3)
{
    //A ja przepisa�em od nowa z pe�n� star� funcjonalno�ci�:) (Krun)
    if(!pointerp(s1))
    {
        if(stringp(s1))
            s1 = ({s1});
        else
            return;
    }

    if(!stringp(s1[0]))
        return;

    pass_command = map(s1, &convert_exit_desc_to_polish());

    if(stringp(s1))
        pass_mess1 = s2;
    if(stringp(s2))
        pass_mess2 = s3;

    if(sizeof(sposob) == 3)
    {
        sposob[0] = s1;
        if(stringp(s2))
            sposob[1] = s2;
        if(stringp(s3))
            sposob[2] = s3;
    }
    else
        sposob = ({s1, s2, s3});
}


/**
 * @return Komendy u�ywane do wychodzenia przez drzwi.
 */
string *
query_pass_command()
{
    return pass_command;
}

/**
 * Dzi�ki tej funkcji mo�emy ustawi� komunikat wy�wietlany graczowi gdy przechodzi przez drzwi.
 * @param k komunikat pokazywany graczowi
 */
public void
set_pass_mess(string k)
{
    my_pass_mess = k;
}

/**
 * Funkcja zwracaj�ca to co ustawili�my w set_pass_command
 */
mixed
query_sposob()
{
    return sposob;
}

/**
 * Ustawiamy komunikat wy�wietany graczowi przy nieuadnym przechodzeniu przez drzwi.
 * @param mess komunikat
 */
void
set_fail_pass(...)
{
    fail_pass = argv;
}

/**
 * @return komunikat wy�wietlany graczowi przy nie udanym otwieraniu drzwi.
 */
mixed
query_fail_pass()
{
    return fail_pass;
}

/**
 * Dzi�ki tej funkcji mo�emy ustawi� pomieszczenie po drugiej stronie drzwi.
 * @param name s�eizka do pomieszczenia po drugiej stronie drzwi.
 */
void
set_other_room(string name)
{
    other_room = name;
}

/**
 * @return Pomieszczenie po drugiej stornie drzwi
 */
string
query_other_room()
{
    return other_room;
}

/**
 * Ustawia unikalne id drzwi
 * @param id id drzwi
 */
void
set_door_id(string id)
{
    id = "DOOR_ID" + id + "DOOR_ID";
    door_id = id;
//    ustaw_shorty(({ id, id, id, id, id, id }) , PL_ZENSKI);
}

/**
 * @return zwraca unikalne id drzwi
 */
string
query_door_id()
{
    return door_id;
}

/**
 * Je�li ustawimy to na 1 to b�d� to drzwi jednostronne.
 * @param i 1-jednostronne, 0 - dwustronne
 */
public void
set_oneside(int i)
{
    is_oneside = !!i;
}

/**
 * @return 1 je�li drzwi s� jednostronne(nie potrzebuj� drugich drzwi.
 * @return 0 je�li drzwi s� dwustronne.
 */
public int
query_oneside()
{
    return is_oneside;
}

/**
 * @return wyszukuje i zwraca drug� stron� drzwi.
 */
object
query_other_door()
{
    if (is_oneside)
        return 0;

    if (!other_door)
        load_other_door();

    return other_door;
}

/**
 * Funkcja filtruj�ca, lokalna.
 */
private static varargs int
other_door_filter(object ob, int i=0)
{
    if(ob->query_door_id() == TO->query_door_id() && ob->is_door())
        return 1;
}

/**
 * Wyszukuje i ustawia drug� stron� drzwi.
 */
void
load_other_door()
{
    object *doors;

    seteuid(getuid());

    //Nie ma drugiej strony.
    if(!strlen(other_room))
    {
        notify_wizards(TO, "Druga strona drzwi: " + MASTER + " nie zosta�a ustawiona!\n");
        write("Wyst�pi� powa�ny b�ad. Zg�o� go opisuj�c okoliczno�ci w jakich do niego dosz�o.\n");
        if(!other_door)
            return 0;
    }

    //Pr�bujemy za�adowa� drug� stron�.
    if (!find_object(other_room))
    {
        other_room->teleledningsanka();
        if (!find_object(other_room))
        {
            notify_wizards(TO, "B^l^ad w ^ladowaniu drugiej strony drzwi: " + other_room + ".\n");

            remove_door_info(environment(this_object()));
            string tmp = TO->short(PL_MIE);
            set_alarm(0.1, 0.0, "remove_object");
            return "Wyst�pi� powa�ny b��d w " + tmp + ". Zg�o� go opisuj�c okoliczno�ci w jakich do niego dosz�o.\n";
        }
    }

    //Wyszukujemy drzwi z drugiej strony na podstawie id a p�niej keya
    object *tmp; //Zmienna pomocnicza
    tmp = doors;
    doors = filter(all_inventory(find_object(other_room)), &other_door_filter());

    if(!sizeof(doors))
       doors = filter(all_inventory(find_object(other_room)), &other_door_filter(,1));

    if(sizeof(doors) > 1)
    {
        notify_wizards(TO, "Hmmm... Dziwne, wydaje si�, �e po drugiej stronie jest dwoje drzwi, " +
            "o tym samym kluczu. Napraw TO!\n");
        //Pomijamy komunikat do gracza, bo mo�na normalnie dzia�a�.. Tylko usuniemy jedne drzwi.

        //Usuwamy reszte drzwi.
        doors[1..]->remove_object();
    }

    if (!doors || !sizeof(doors) && !is_oneside)
    {
        notify_wizards(TO, "Po drugiej stronie, po za^ladowaniu pokoju nie ma drzwi: " +
            other_room + ".\n");
        remove_door_info(environment(this_object()));
        string tmp = TO->short(PL_MIE);
        set_alarm(0.1, 0.0, "remove_object");
        return "Wyst�pi� powa�ny b��d w " + tmp + ". Zg�o� go opisuj�c okoliczno�ci w jakich do niego dosz�o.\n";
    }

    other_door = doors[0];
}

/*
 * Function name: add_door_info
 * Description:   Add information about this door to the room it
 *                stands in. If this door already exists, autodestruct.
 * Arguments:     dest - The room that contains the door.
 */
static void
add_door_info(object dest)
{
    string *door_ids;
    object *doors;

    door_ids = (string *)dest->query_prop(ROOM_AS_DOORID);
    doors = (object *)dest->query_prop(ROOM_AO_DOOROB);
    if (!pointerp(door_ids))
    {
        door_ids = ({});
        doors = ({});
    }

    /*
     * One door of the same type is enough.
     */
    if (member_array(door_id, door_ids) >= 0)
    {
        write("Jedne drzwi wystarcz^a. [id=" + door_id + "] [dest=" + file_name(dest) + "]\n");
        remove_object();
        return;
    }

    door_ids += ({ door_id });
    doors += ({ this_object() });

    dest->add_prop(ROOM_AS_DOORID, door_ids);
    dest->add_prop(ROOM_AO_DOOROB, doors);
}

/*
 * Function name: remove_door_info
 * Description:   Remove information about this door from the room it
 *		  stands in.
 * Arguments:	  dest - The room that contains the door.
 */
static void
remove_door_info(object dest)
{
    string *door_ids;
    object *doors;
    int pos;

    door_ids = (string *)dest->query_prop(ROOM_AS_DOORID);
    doors = (object *)dest->query_prop(ROOM_AO_DOOROB);
    if (!sizeof(door_ids))
        return;

    pos = member_array(door_id, door_ids);
    if (pos < 0)
        return;

    door_ids = exclude_array(door_ids, pos, pos);
    doors = exclude_array(doors, pos, pos);

    dest->add_prop(ROOM_AS_DOORID, door_ids);
    dest->add_prop(ROOM_AO_DOOROB, doors);
}

/*
 * Function name: enter_env
 * Description:   The door enters a room, activate it.
 * Arguments:	  dest - The destination room,
 * 		  old - Where it came from
 */
void
enter_env(object dest, object old)
{
    string *itemy;
    int x;

    add_door_info(dest);
    if (TO->query_open())
        dest->change_my_desc(check_call(open_desc), this_object());
    else
        dest->change_my_desc(check_call(closed_desc), this_object());
}

/**
 * Drzwi nie mog� opuszcza� swojego otoczenia. Je�li wi�c je
 * opuszczaj� to je po prostu usuwamy...
 */
void
leave_env(object old, object dest)
{
    if (!(old=ENV(TO)))
        return;

    old->remove_my_desc(this_object());
    remove_door_info(old);

    set_alarm(0.1, 0.0, "remove_object");
}

public varargs void
set_open_mess(mixed s1, mixed s2, mixed s3)
{
    open_mess3 = s3;
    ::set_open_mess(s1, s2);
}

public mixed
query_open_mess()
{
    return ::query_open_mess() + ({open_mess3});
}

public varargs void
set_close_mess(mixed s1, mixed s2, mixed s3)
{
    close_mess3 = s3;
    ::set_close_mess(s1, s2);
}

public mixed
query_close_mess()
{
    return ::query_close_mess() + ({close_mess3});
}

public varargs void
set_unlock_mess(mixed s1, mixed s2, mixed s3)
{
    unlock_mess3 = s3;
    ::set_unlock_mess(s1, s2);
}

public mixed
query_unlock_mess()
{
    return ::query_unlock_mess() + ({unlock_mess3});
}

public varargs void
set_lock_mess(mixed s1, mixed s2, mixed s3)
{
    lock_mess3 = s3;
    ::set_lock_mess(s1, s2);
}

public mixed
query_lock_mess()
{
    return ::query_lock_mess() + ({lock_mess3});
}

public varargs void
set_pick_mess(mixed s1, mixed s2, mixed s3)
{
    ::set_pick_mess(s1, s2);
    pick_mess3 = s3;
}

public mixed
query_pick_mess()
{
    return ::query_pick_mess() + ({pick_mess3});
}

public varargs void
set_breakdown_mess(mixed s1, mixed s2, mixed s3)
{
    ::set_breakdown_mess(s1, s2);
    breakdown_mess3 = s3;
}

public mixed
query_breakdown_mess()
{
    return ::query_breakdown_mess() + ({breakdown_mess3});
}

public void
open_now()
{
    if(!is_oneside)
        query_other_door()->open_from_other_side();
}

public void
close_now()
{
    if(!is_oneside)
        query_other_door()->close_from_other_side();
}

public void
unlock_now()
{
    if(!is_oneside)
        query_other_door()->unlock_from_other_side();
}

public void
lock_now()
{
    if(!is_oneside)
        query_other_door()->lock_from_other_side();
}

public void
breakdown_now()
{
    if(!is_oneside)
        query_other_door()->breakdown_from_other_side();
}

public void
pick_now()
{
    if(!is_oneside)
        query_other_door()->pick_from_other_side();
}

public void
do_open()
{
    if(calling_function() != "do_open" && !is_oneside)
        query_other_door()->do_open();

    ::do_open();
    ENV(TO)->change_my_desc(check_call(open_desc), this_object());
}

public void
do_close()
{
    if(calling_function() != "do_close" && !is_oneside)
        query_other_door()->do_close();

    ::do_close();
    ENV(TO)->change_my_desc(check_call(closed_desc), this_object());
}

public void
do_lock()
{
    if(calling_function() != "do_lock" && !is_oneside)
        query_other_door()->do_lock();

    ::do_lock();
}

public void
do_unlock()
{
    if(calling_function() != "do_unlock" && !is_oneside)
        query_other_door()->do_unlock();

    ::do_unlock();
}

public void
do_breakdown()
{
    if(calling_function() != "do_breakdown" && !is_oneside)
        query_other_door()->do_breakdown();

    ::do_breakdown();
    ENV(TO)->change_my_desc(check_call(open_desc), this_object());
}

public void
do_pick()
{
    if(calling_function() != "do_pick" && !is_oneside)
        query_other_door()->do_pick();

    ::do_pick();
}

public void
open_from_other_side()
{
    if(open_mess3)
    {
        if(functionp(open_mess3))
        {
            function f = open_mess3;
            tell_room(ENV(TO), f());
        }
        else
            tell_room(ENV(TO), open_mess3);
    }
    else
        tell_room(ENV(TO), UC(TO->short(PL_MIA)) + " otwieraj� si�.\n");

    do_open();
}

public void
close_from_other_side()
{
    if(close_mess3)
    {
        if(functionp(close_mess3))
        {
            function f = close_mess3;
            tell_room(ENV(TO), f());
        }
        else
            tell_room(ENV(TO), close_mess3);
    }
    else
        tell_room(ENV(TO), UC(TO->short(PL_MIA)) + " zamyka" + (query_tylko_mn() ? "j�" : "") + " si�.\n");

    do_close();
}

public void
unlock_from_other_side()
{
    if(unlock_mess3)
    {
        if(functionp(unlock_mess3))
        {
            function f = unlock_mess3;
            tell_room(ENV(TO), f());
        }
        else
            tell_room(ENV(TO), unlock_mess3);
    }
    else
        tell_room(ENV(TO), "Od " + TO->short(PL_DOP) + " dochodzi Ci� odg�os 'klik'.\n");

    do_unlock();
}

public void
lock_from_other_side()
{
    if(lock_mess3)
    {
        if(functionp(lock_mess3))
        {
            function f = lock_mess3;
            tell_room(ENV(TO), f());
        }
        else
            tell_room(ENV(TO), lock_mess3);
    }
    else
        tell_room(ENV(TO), "Od " + TO->short(PL_DOP) + " dochodzi Ci� odg�os 'klik'.\n");

    do_lock();
}

public void
breakdown_from_other_side()
{
    if(breakdown_mess3)
    {
        if(functionp(breakdown_mess3))
        {
            function f = breakdown_mess3;
            tell_room(ENV(TO), f());
        }
        else
            tell_room(ENV(TO), breakdown_mess3);
    }
    else
        tell_room(ENV(TO), UC(TO->short(PL_MIA)) + " otwieraj� si� nagle.\n");

    do_breakdown();
}

public void
pick_from_other_side()
{
    if(breakdown_mess3)
    {
        if(functionp(breakdown_mess3))
        {
            function f = breakdown_mess3;
            tell_room(ENV(TO), f());
        }
        else
            tell_room(ENV(TO), breakdown_mess3);
    }
    else
        tell_room(ENV(TO), "Od " + TO->short(PL_DOP) + " dochodzi Ci� odg�os 'klik'.\n");

    do_unlock();
}

/**
 * Je�li usuwamy jedne drzwi.. To drugie te�!
 */
void
remove_object()
{
    object old;

    if (!(old=ENV(TO)))
        return;

    old->remove_my_desc(this_object());

    remove_door_info(old);

    //Usuwamy te drugie drzwi.
    //To jednak nie by� dobry pomys�!
    //Od teraz nie usuwamy.
    //Trzeba zrobi� funkcje add_door i wtedy b�dziemy usuwa�.
#if 0
    if(other_door)
    {
        if(calling_function() != "remove_object" && previous_object() != other_door)
            other_door->remove_object();
    }
    else if(calling_function() != "load_other_door")
        load_other_door();
#endif

    ::remove_object();
}

public varargs int
lock_me(object key, int cicho=0)
{
    if(query_no_lock() && !query_locked())
        return ::close_me(cicho);
    else if(query_no_lock())
        return "Wygl�da na to, �e " + TO->short(PL_BIE) + " s� zablokowane z drugiej strony.\n";

    return ::lock_me(key, cicho);
}

public varargs int
unlock_me(object key, int cicho=0)
{
    if(query_no_unlock() && TO->query_locked())
        return ::open_me(cicho);
    else if(query_no_unlock())
        return "Wygl�da na to, �e " + TO->short(PL_BIE) + " s� zablokowane z drugiej strony.\n";

    return ::unlock_me(key, cicho);
}

/**
 * Funkcja identyfikuj�ca obiekt jako drzwi.
 *
 * @return 1 zawsze
 */
public int is_door() { return 1; }