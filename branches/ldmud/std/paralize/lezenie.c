/*
 * Plik oparty na paralyze.c, istotnie uproszczony. 
 * Sklonuj i przenie� ten obiekt do inwentarza gracza 
 * kt�ry ma si� przewr�ci�/po�o�y�.
 *
 * Gdy parali� ma by� przerwany wywo�ana jest stop_fun.  
 * 
 * Mi�ego przewracania, 
 * Molder.
 */
#pragma strict_types
#include <stdproperties.h>

inherit "/std/object";

int stop(string str);

/*
 * Function name: create_object
 * Description:   The standard create routine.
 */
nomask void
create_object()
{
    set_no_show();
    
    add_prop(OBJ_M_NO_DROP, 1);
    add_prop(OBJ_M_NO_STEAL,1);
    add_prop(OBJ_M_NO_SELL, 1);
    add_prop(OBJ_M_NO_GIVE, 1);

}

/*
 * Function name: init
 * Description:   Called when meeting an object
 */
void
init()
{
    add_action(stop, "", 1);
}

string
query_fail_message()
{
    return "Najpierw musisz wsta�.\n";
}

string
query_stop_message()
{
    return "Wstajesz.\n";
}

/* wywo�ywana gdy gracz pr�buje wsta�. je�li zwr�ci 0 pr�ba si� nie uda */
int stop_fun()
{
  
    int waga = (this_player()->query_prop(OBJ_I_WEIGHT) - this_player()->query_prop(CONT_I_WEIGHT))/1000;
    int zmeczenie = this_player()->query_old_fatigue();

    /* je�li dorzucili�my graczowi co� ci�kiego do inwentarza */
    if(this_player()->query_prop(OBJ_I_WEIGHT) > this_player()->query_prop(CONT_I_MAX_WEIGHT))
    {
        this_player()->catch_msg("Tw�j ekwipunek przygniata ci� z powrotem do ziemi.\n");
        return 0;
    }

    /* b�dziemy dawa� tyle zm�czenia ile wa�y 1/2 inwentarza + bonusowo 5 punkt�w*/
    waga = waga/2 + 5;

    if(waga > zmeczenie)
    {
        this_player()->catch_msg("Nie masz si�y si� podnie��.\n");
        return 0;
    }
    
    this_player()->add_old_fatigue(- waga);
   
    if(zmeczenie / waga < 3)
    {
        this_player()->catch_msg("Wstajesz, cho� kosztuje ci� to sporo wysi�ku.\n");
        return 1;
    }
    
    this_player()->catch_msg(query_stop_message());

    return 1;
}

/*
 * Nazwa        : stop
 * Description : Here all commands the player gives comes.
 * Argument   : string str - The command line argument.
 * Returns      : int 1/0    - komenda gracza zostanie/nie zostanie 
 *                                      sparali�owana.
 */
int stop(string str)
{
    string verb = query_verb();
    string stop_verb; 
    string *nie_dozwolone;

    /* parali�ujemy tylko tych kt�rzy maj� parali� w inventory */
    if (environment() != this_player())
        return 0;
     
    stop_verb = "wsta�";

    nie_dozwolone = environment(environment(this_object()))->query_exit_cmds();
    nie_dozwolone += ({"zata�cz","podskocz","nadepnij","kopnij","uk�o�","pok�o�","dygnij","podrepcz"});
    nie_dozwolone += ({"poklep","przebieraj","przest�p","tupnij", "wyp�acz"});
    nie_dozwolone +=({"usi�d�","zabij"});
    nie_dozwolone += ({"po��","rzu�"});

    /* tylko niekt�re polecenia s� zabronione */
    if ((member_array(verb,nie_dozwolone) != -1))
    {
        this_player()->catch_msg(query_fail_message());
        return 1;
    }        

    /* je�li jest komenda przerywaj�ca parali�, sprawdzamy j� */
    if (verb ~= stop_verb || (verb ~= "podnie�" && str ~= "si�"))
    {
	    /* Je�li stop_fun() zwr�ci 0 parali� b�dzie trwa� nadal */
        if (stop_fun() == 0)
	        return 1;
	   
        set_alarm(0.3, 0.0, remove_object);
        this_player()->remove_prop("_przewrocony");

	    return 1;
    }

     return 0;
}

/**
 * Wy��czamy auto_�adowanie parali�u le�enia.
 */
string
query_auto_load()
{
       return 0;
}
