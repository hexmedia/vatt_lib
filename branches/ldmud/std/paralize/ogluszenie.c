/**
 *
 * \file /std/paralize/ogluszenie.c
 *
 * Autor: Prawdopodobnie Delvert:P
 *
 * Parali� u�ywany gdy gracz zostaje og�uszony.
 *
 * TODO:
 * - <i>gracz og�uszony przewraca si� na ziemi�, przyda�o by si�, �eby tam le�a�.</i>
 */

inherit "/std/paralyze";

#include <macros.h>
#include <stdproperties.h>

object corpse;

void
create_paralyze()
{
    set_name("ogluszenie_paraliz");

    set_finish_object(this_object());
    set_finish_fun("ocknij_sie");
    set_remove_time(120 + random(60));
    set_block_all();

    setuid();
    seteuid(getuid());
}

void
ocknij_sie()
{
    object env = environment(corpse);
    object player = environment(this_object());

    while (environment(env))
        env = environment(env);

    player->move(env, 1);
    all_inventory(corpse)->move(player, 1);
    corpse->remove_object();

    write("Powoli dochodzisz do siebie.\n");
    saybb(QCIMIE(player, PL_MIA) + " wstaje.\n");
}

void
set_corpse(object ob)
{
    corpse = ob;
}
