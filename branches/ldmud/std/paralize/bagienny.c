inherit "/std/paralyze.c";

#include <pl.h>
#include <macros.h>
#include <object_types.h>
#include <stdproperties.h>

void
create_paralyze()
{
    set_no_show();
    set_long("Tego nie widzisz!\n");
    add_allowed("krzyknij");
    add_allowed("pierdnij");
    add_allowed("wrza^snij");
    add_allowed("sp");
    add_allowed("sp^ojrz");
    add_allowed("i");
    add_allowed("inwentarz");
    add_allowed("stan");
    add_allowed("kondycja");
    add_allowed("k");
    add_allowed("cechy");
    add_allowed("ob");
    add_allowed("obejrzyj");
    add_allowed("oce^n");
    add_allowed("kto");
    add_allowed("spanikuj");
    add_allowed("pierdnij");
    add_allowed("beknij");
    add_allowed("za^lam");
    add_allowed("westchnij");
    add_allowed("odetchnij");
    add_allowed("otrzyj");
    add_allowed("namysl");
    add_allowed("':");
    add_allowed("wzdrygnij");
    add_allowed("za^smiej");
    add_allowed("zarechocz");
    add_allowed("zachichocz");
    add_allowed("podrap");
    remove_allowed("podskocz");
    
  
    set_fail_message("B^loto uniemo^zliwia ci wi^ekszo^s^c ruch^ow, wi^ec nie mo^zesz "
            +        "tego uczyni^c.\n");
    set_alarm(15.0, 0.0, "remove_object");
}