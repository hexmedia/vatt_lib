/*
 * gate.c
 * To jest standard bram miejskich.
 *                                     Vera, ;)
 *                                     dnia ok. 01.06.06.
 *
 *
 *
 * Generalnie standard ten z za�o�enia mia� by� podobny do standardu drzwi, by
 * nie miesza� za du�o, ale niestety, w kilku kwestiach musia�y zaistnie�
 * pewne ca�kiem spore r�nice.
 *
 * Standardowa brama wazy 240 kilo, i ma objetosc
 * 320 litrow. Standardowa wysokosc bramy wynosi 3 metry.
 * Poziomy bezpieczenstwa standardowej bramy to poziomy domyslny, czyli: 1.
 *
 * A zatem, spis podstawowych opcji, kt�re MUSIMY umie�ci� w naszej bramie:
 *  1. D�ugi opis.
 *      - Funkcja:    set_gate_desc()
 *      - Przyjmuje:  stringa z longiem ;]
 *      - Uwagi:      gdzie� w tej funkcji musimy te� da� wywo�ywanie funkcji
 *                    opened_or_closed_desc w taki spos�b:
 *                    set_gate_desc("Jest to brama. "+
 *                    check_call(opened_or_closed_desc));
 *                    Dzi�ki temu dostaniemy opis zale�ny od tego, czy brama
 *                    jest otwarta czy nie. Domy�lny string leci wg. schematu
 *                    "W tej chwili jest otwarta/zamkni�ta.\n", ale oczywi�cie
 *                    mo�emy nadpisac t� funkcj�, by zwraca�a innego stringa.
 *
 *  2. Jaki spos�b brama ma by� otwierana?
 *       - Funkcja:   set_open_command() (oraz opcjonalnie: string czym)
 *       - Przyjmuje: string komendy
 *       - Uwagi:     Niestety, ale nie mo�emy sobie wymy�li� komendy jakiej
 *                    chcemy. :P Jest �ci�le okre�lona pula dost�pnych komend,
 *                    a od wybranej komendy zale�e� b�dzie te� to, czy b�dziemy
 *                    musieli nadpisa� stringa 'czym'. A zatem, do wyboru mamy
 *                    komendy: zapukaj, zastukaj, szarpnij, poci�gnij i zadzwo�
 *                    W przypadku: zastukaj oraz szarpnij, poci�gnij i zadzwo�
 *                    musimy poda� stringa czym. I tak, np. do zastukaj string
 *                    czym to b�dzie "ko�atk�", w przypadku szarpnij np.
 *                    czym="sznur", a zadzwo� np: czym="dzwonkiem".
 *
 *  3. Ustalanie godzin otwarcia i zamkni�cia.
 *       - Funkcja:   godzina_zamkniecia() oraz godzina_otwarcia()
 *       - Przyjmuje: obie przyjmuj� inta godzin�.
 *       - Uwagi:     Musimy tu ustali� o kt�rej godzinie brama ma by�
 *                    otwierana, a o kt�rej zamykana. Domy�lna brama zamykana
 *                    jest o godzinie 23, a otwierana o 6.
 *                    Nale�y pami�ta� o tym, �e doba na mudzie
 *                    (przynajmniej z tego co mi wiadomo) jest od 1 do 24.
 *                    Godzina zamkni�cia NIE MO�E by� p�niejsza od
 *                    p�nocy(24)!!!
 *
 *  4. Ustawianie poziomu bezpiecze�stwa.
 *       - Funkcja:   set_security_lvl()
 *       - Przyjmuje: inta poziom.
 *       - Uwagi:     Poziom�w mamy cztery. od 0 do 3.
 *                    Co nieco o security_lvl's:
 *                    0 - ka�dy mo�e wej��/wyj��. Nie trzeba si� przedstawia�.
 *                        W godzinach zamkniecia wystarczy zapuka� czy co�.. ;)
 *                    1 - w godzinach zamkni�cia mog� wej��/wyj�� tylko
 *                        mieszka�cy danego miasta oraz dru�yna, w kt�rej
 *                        liderem jest mieszkaniec. Je�li gracz jest sam, lub
 *                        je�li jest liderem dru�yny to musi si� przedstawi�
 *                        (poziom domy�lny).
 *                    2 - w godzinach zamkni�cia mog� wej��/wyj�� tylko
 *                        mieszka�cy danego miasta. Ka�dy si� musi przedstawi�.
 *                    3 - w godzinach zamkni�cia nikt nie mo�e wej��/wyj��. W
 *                        godzinach "otwarcia" ka�dy si� musi przedstawi�.
 *                        W godzinach "otwarcia" bycie mieszka�cem nie jest
 *                        wymagane (jak zreszt� na ka�dym innym poziomie).
 *                        Cho� praktycznie w "godzinach otwarcia" brama i tak
 *                        jest zamkni�ta, lecz mimo tego S� r�nice. ;-)
 *
 *
 *
 *  5. Inne.
 * Ponadto by brama dobrze funkcjonowa�a musimy nadpisa� stringa 'skad'
 * za pomoca funkcji set_skad().
 * Je�li jest to brama z miejsca, z kt�rego mo�na pochodzi� to wype�niamy
 * go tym w�a�nie pochodzeniem, czyli np w funkcji create_gate() robimy:
 * set_skad("z Rinde") je�li natomiast z tego miejsca nie mo�na pochodzi�
 * (dziwna sprawa, niby czemu nie, skoro to miejsce jest na tyle zamo�ne,
 * �e maj� ju� swoj� bram�?!) to przypisujemy pustego stringa: set_skad("").
 * A tak�e dodajemy 3 unikalne funkcje dla ka�dej bramy: set_other_room(),
 * set_gate_id() oraz set_pass_command().
 * Dodatkowo KONIECZNIE trzeba bramie doda� odpowiedniego propa. Zale�nie
 * od tego, czy brama jest od strony wewn�trznej miasta czy zewn�trznej -
 * dodajemy propa GATE_IS_INSIDE lub GATE_IS_OUTSIDE.
 *
 * Je�li kto� chce mie� (bardzo wskazane^_^) w swojej bramie indywidualne
 * teksty stra�nik�w nale�y nadpisa� funkcje:
 * string info_security_lvl0(), string info_security_lvl1(),
 * string info_security_lvl2(), string info_security_lvl3(),
 * string info_security_lvl4(), string info_security_lvl5(),
 * string info_security_lvl6(), string info_security_lvl7(),
 * string info_security_lvl8() oraz string info_security_lvl9().
 * By dowiedzie� si� jaka funkcja odpowiada za jaki teksty - odsy�am do
 * smana funkcji query_info_security_lvl.
 *
 * TODO:
 * Plik do ca�kowitego przerobienia:/ (KRUN)
 */
#pragma save_binary
#pragma strict_types

inherit "/std/object";
#include <stdproperties.h>
#include <macros.h>
#include <ss_types.h>
#include <cmdparse.h>
#include <language.h>
#include <mudtime.h>
#include <filter_funs.h>
#include <composite.h>

string	other_room,	/* The name of the other side of the gate */
	gate_id,	/* A unique (?) id of the gate */
    	gate_desc,	/* A description of the gate */
   	open_desc,	/* The description of the open gate */
	closed_desc,	/* The description of the closed gate */
	pass_mess,	/* tekst przy wychodzeniu */
	my_pass_mess,
	fail_pass,	/* The fail pass message */
	fail_close,	/* The fail close message */
	komenda;        /* Komenda u�yta do otwierania bramy */

string	*open_mess,	/* The open messages */
        *info_security_lvls, /* Informacje wyswietlane przy wchodzeniu... */
	fail_open,	/* The fail open messages */
	*close_mess;	/* The close messages */

object	other_gate,
        leader;
object *czlonkowie;     /* czlonkowie dru�yny leadera */
        	/* The gate at the other side */

string	*pass_commands,		/* The commands used to enter the gate */
	*open_commands,		/* The commands used to open the gate */
	*close_commands,	/* The commands used to close the gate */
	czym,           /* np. w stukaniu: ko�atk�, a szarpaniu: sznurem. */
	skad;           /* pochodzenie graczy z miasta, w ktorym sa bramy.
	                   np. "z Rinde" */

int	open_status,		/* If the gate is open or not */
	lock_status,		/* If the gate is locked or not */
	open_str,		/* Strength needed to open gate */
	zamykamy,               /* Godzina o ktorej brama jest zamykana */
	otwieramy,              /* Godzina o ktorej brama jest otwierana */
	leader_introduced,      /* Czy leader dru�yny si� przedstawi�? */
    open_alrm,          /* id alarmu otwierania - �eby nie otwiera�o si� wielokrotnie */
	close_alrm,          /* --||-- */
	security;           /* Czy w nocy wpuszczamy tylko mieszkancow? */

/*
 * Some prototypes
 */
void create_gate();
void set_other_room(string name);
void set_gate_id(string id);
void set_pass_command(mixed command);
void set_pass_mess(mixed mess);
void set_fail_pass(string mess);
void set_open_command(mixed command);
void set_open_mess(string *mess);
void set_fail_open(string mess);
void set_close_command(mixed command);
void query_close_command();
void query_open_command();
void set_close_mess(string *mess);
void set_fail_close(string mess);
void load_other_gate();
static void remove_gate_info(object dest);
void do_open_gate(string mess);
void do_close_gate(string mess);
void set_open(int i);
void set_locked(int i);
void godzina_otwarcia(int i);
void godzina_zamkniecia(int i);
int sprawdzanie_otwarcia();
int set_security_lvl(int i);
int query_security_lvl();
int lock_gate();
int set_skad(string str);
string query_skad();
int unlock_gate();
void is_leader_introduced(int i);
int query_leader_introduced();
object query_leader_ob();
void query_info_security_lvl(int i);
int check_security_lvls(object komu, int cicho);
void try_to_open_gate(object komu);
int query_widoczni_czlonkowie(object x, object czyjego);
string info_security_lvl0();
string info_security_lvl1();
string info_security_lvl2();
string info_security_lvl3();
string info_security_lvl4();
string info_security_lvl5();
string info_security_lvl6();
string info_security_lvl7();
string info_security_lvl8();
string info_security_lvl9();
string opened_or_closed_desc();

/*
 * Function name: create_object
 * Description:   Initialize object.
 */
void
create_object()
{
    pass_commands = ({});
    open_commands = ({});
    close_commands = ({});
    add_prop(OBJ_I_WEIGHT, 240000);
    add_prop(OBJ_I_VOLUME, 320000);
    add_prop(DOOR_I_HEIGHT, 300); /* Standardowa wysokosc 3 metry. */
    add_prop(OBJ_I_NO_GET, 1);
    add_prop(OBJ_I_DONT_GLANCE,1);
    set_fail_pass("@@standard_fail_pass");
    set_open_mess(({"@@standard_open_mess1", "@@standard_open_mess2",
        "@@standard_open_mess3" }));
    set_fail_open("@@standard_fail_open");
    set_close_mess(({"@@standard_close_mess1", "@@standard_close_mess2",
        "@@standard_close_mess3" }));
    set_fail_close("@@standard_fail_close");

    set_long("Jest to du�a dwuskrzyd�owa brama miejska. "+
	    "@@opened_or_closed_desc@@");
    set_pass_mess("@@standard_pass_mess");
    set_open(1); /*Brama zwykle jest otwarta w godzinach otwarcia, nie? ;) */
    set_locked(0);
    ustaw_nazwe("brama");
    set_open_command(({"zapukaj", "uderz"})); /* W standardow� bram� pukamy,
                                                 chc�c j� otworzy�.
                                                 R�wnie� mo�emy w ni� uderza�. */
    czym=""; /* Je�li jak_otwieramy to szarpnij lub poci�gnij to tu powinno
                by�: sznur. Je�li jak_otwieramy to zadzwo� to tu powinno by�:
                dzwonkiem                                                   */
    godzina_zamkniecia(24); /* Standardowa brama zamykana jest o p�nocy */
    godzina_otwarcia(6); /* Standardowa brama otwierana jest o sz�stej */
    set_alarm_every_hour("sprawdzanie_otwarcia");
    set_security_lvl(0); /* Tylko mieszkancy danego miasta moga wejsc w nocy
                             do miasta. Poza tym, kazdy gracz w nocy musi sie
                             przedstawiac. Domyslnie jest to wlaczone,
                             jak widac.                                     */
    set_pass_mess("przez bram� na zewn�trz"); /* Taki tam default... */
    set_skad(""); /* Tylko ludzi z takim pochodzeniem wpuszczamy. */

    //TO MUSI BY� W BRAMIE:
    /*set_other_room("/d/Standard/miasta/Rinde/poludnie/ulica4");
    add_prop(GATE_IS_OUTSIDE,1);
    set_gate_id("asdf");
    set_pass_command(({"po�udnie","s"}));*/

    create_gate();
    set_alarm(2.0, 0.0, "sprawdzanie_otwarcia");
}

/*
 * Function name: sprawdzanie_otwarcia()
 * Description:   Sprawdzamy, czy brama nie jest zamknieta (ze wzgl�du na
 *                godzin�).
 */
int
sprawdzanie_otwarcia()
{
   int godzina=MT_NHOUR;
                              //&& jesli godzina miesci sie w zakresie otwarcia
   if((query_security_lvl()==3) && ((godzina>otwieramy) && (godzina<zamykamy)))
   {
     //  lock_gate();
    //   set_locked(1);
       set_alarm(3.5, 0.0, "do_close_gate","");
	 other_gate->set_alarm(5.0, 0.0,"do_close_gate",
	                       check_call(close_mess[1]));
       return 1;
   }
   else if(godzina==zamykamy)
   {
      lock_gate();
      set_locked(1);
      tell_room(environment(TO),
                capitalize(TO->short(PL_MIA)) + " zostaje zaryglowan"+
                TO->koncowka("y","a","e")+".\n");
      set_alarm(5.0, 0.0, "do_close_gate","");
	    other_gate->set_alarm(5.0, 0.0,"do_close_gate",
	                          check_call(close_mess[1]));

      return 1;
   }
   else if(godzina==otwieramy)
   {
      unlock_gate();
      set_locked(0);
      tell_room(environment(TO),
                capitalize(TO->short(PL_MIA)) + " zostaje otwart"+
                TO->koncowka("y","a","e")+".\n");
      if (get_alarm(open_alrm) == 0) {
          open_alrm = set_alarm(2.5, 0.0, "do_open_gate","");
          other_gate->set_alarm(5.0, 0.0,"do_open_gate", check_call(open_mess[1]));
      }
      return 1;
   }
   else if((godzina<otwieramy) || (godzina>zamykamy))
   {
      lock_gate();
      set_locked(1);
      set_alarm(4.0, 0.0, "do_close_gate","");
	    other_gate->set_alarm(5.0, 0.0,"do_close_gate",
	                          check_call(close_mess[1]));

      return 1;
   }
   else
      return 1;
}


/*
 * Function name: create_gate
 * Description:   Sets default names and id
 */
void
create_gate()
{
}

/*
 * Function name: reset_gate
 * Description:   Reset the gate
 */
void
reset_gate()
{
}

/*
 * Function name: reset_object
 * Description:   Reset the object
 */
nomask void
reset_object()
{
    reset_gate();
}








int pass_gate(string str);
int open_gate(string co);
int close_gate(string str);

/*
 * Function name: init
 * Description:   Initalize the gate actions
 */
void
init()
{
    int i;

    ::init();
    add_action("all_commandss","",1);
    for (i = 0 ; i < sizeof(pass_commands) ; i++)
	add_action(pass_gate, check_call(pass_commands[i]));

    for (i = 0 ; i < sizeof(open_commands) ; i++)
	add_action(open_gate, check_call(open_commands[i]));
}

/*
 * Function name: usuwamy_no_pass
 * Description:   usuwamy graczowi propa NO_PASS_THRU_GATE
 */
void
usuwamy_no_pass(object komu)
{
    komu->remove_prop(NO_PASS_THRU_GATE);
}

/*
 * Function name: dodajemy_no_pass
 * Description: dodajemy graczowi propa NO_PASS_THRU_GATE i ustawiamy
 *              alarm na funkjc� usuwamy_no_pass()
 */
void
dodajemy_no_pass(object komu)
{
    komu->add_prop(NO_PASS_THRU_GATE,1);
    set_alarm(3.5, 0.0, "usuwamy_no_pass",komu);
}


/*
 * Function name: check_security_lvls
 * Description:   sprawdzamy poziomy bezpiecze�stwa.
 * Arguments:	  object komu, int cicho = 0
 */
int
check_security_lvls(object komu, int cicho = 0)
{
    int il,ilx,obca_druzyna=0,przedst_team=0;
    object *arr=komu->query_team_others();


    if(sizeof(arr))
        czlonkowie=filter(all_inventory(ENV(TO)),
	             &query_widoczni_czlonkowie(,komu));

    for(ilx=0;ilx<sizeof(czlonkowie);ilx++)
    {
        if(czlonkowie[ilx]->query_prop(INTRODUCED_AT_GATE))
	    przedst_team++;
    }

    if(query_security_lvl()>0 && komu->query_leader() &&
       TO->query_prop(GATE_IS_OUTSIDE))
    {
	tell_room(ENV(TO),query_info_security_lvl(7));
        return 1;
    }

    if(query_security_lvl()==3)
    {
	    if(lock_status)
	    {
	        tell_room(ENV(TO),query_info_security_lvl(6));
	        return 1;
	    }
	    else
	    {
	        if(!komu->query_leader())
	        {

	            if(!komu->query_prop(INTRODUCED_AT_GATE))
	            {
	                dodajemy_no_pass(komu);
	                if(TO->query_prop(GATE_IS_OUTSIDE))
                        {
                           tell_room(ENV(TO),QCIMIE(komu,PL_MIA)+" bezskutecznie pr�buje"+
                                           " dosta� si� do �rodka.\n",({komu}));
                        }
                        else if(TO->query_prop(GATE_IS_INSIDE))
                        {
                           tell_room(ENV(TO),QCIMIE(komu,PL_MIA)+" bezskutecznie "+
                                 "pr�buje wydosta� si� na zewn�trz.\n",({komu}));
                        }
	                tell_room(ENV(TO),query_info_security_lvl(8));
	                return 1;
	            }

	            if(przedst_team>0)
	            {
	                tell_room(ENV(TO),query_info_security_lvl(9));
	                return 1;
	            }
	        }
	    }
    }
    if(query_security_lvl()==2)
    {
	    if(lock_status)
	    { object gracz=komu;// gracz=TO;
	        if(!gracz->query_leader())
	        {
	            if(!komu->query_prop(INTRODUCED_AT_GATE))
	            {
	               dodajemy_no_pass(komu);
	               if(TO->query_prop(GATE_IS_OUTSIDE))
                       {
                           tell_room(ENV(TO),QCIMIE(komu,PL_MIA)+" bezskutecznie pr�buje"+
                                           " dosta� si� do �rodka.\n",({komu}));
                       }
                       else if(TO->query_prop(GATE_IS_INSIDE))
                       {
                           tell_room(ENV(TO),QCIMIE(komu,PL_MIA)+" bezskutecznie "+
                                 "pr�buje wydosta� si� na zewn�trz.\n",({komu}));
                       }
	               tell_room(ENV(TO),query_info_security_lvl(8));
	               return 1;
	            }
	            if(sizeof(czlonkowie))
	            {
	                if(przedst_team>0)
	                {
	                    tell_room(ENV(TO),query_info_security_lvl(9));
	                    return 1;
	                }
	            }
	            if(query_skad()!="")
	            {
    	                if(komu->query_origin()~=query_skad())
	                {
                            for(il=0;il<sizeof(czlonkowie);il++)
	                    {
	                       if(czlonkowie[il]->query_origin()!=query_skad())
	                            obca_druzyna++;
	                    }
	                    if(obca_druzyna==1)
	                    {
   	                        tell_room(ENV(TO),
   	                                  query_info_security_lvl(4));
	                        return 1;
	                    }
	                    else if(obca_druzyna>1)
	                    {
    	                        tell_room(ENV(TO),
    	                                  query_info_security_lvl(5));
	                        return 1;
	                    }
	                    tell_room(ENV(TO),
	                              query_info_security_lvl(0));
	                }
	                else// if(query_skad())
	                {
	                    tell_room(ENV(TO),
	                              query_info_security_lvl(3));
	                    return 1;
	                }
	            }
	            //else
	               //write("nima!");
	        }
	    }
    }
    if(query_security_lvl()==1)
    {
	    if(lock_status)
	    {
	        int il,obca_druzyna=0;

                if(!komu->query_prop(INTRODUCED_AT_GATE))
	        {
	               dodajemy_no_pass(komu);
	               if(TO->query_prop(GATE_IS_OUTSIDE))
                       {
                           tell_room(ENV(TO),QCIMIE(komu,PL_MIA)+" bezskutecznie pr�buje"+
                                           " dosta� si� do �rodka.\n",({komu}));
                       }
                       else if(TO->query_prop(GATE_IS_INSIDE))
                       {
                           tell_room(ENV(TO),QCIMIE(komu,PL_MIA)+" bezskutecznie "+
                                 "pr�buje wydosta� si� na zewn�trz.\n",({komu}));
                       }
	               tell_room(ENV(TO),query_info_security_lvl(8));
	               return 1;
	        }
                if(!komu->query_leader())//0 zwraca, ze tak...dziwne nie?;)
	        {
	            if(query_skad()!="")
	            {
    	                if(komu->query_origin()==query_skad())
	                {
    	                    tell_room(ENV(TO),
    	                              query_info_security_lvl(0));
	                    for(il=0;il<sizeof(arr);il++)
	                    {
    	                        if(arr[il]->query_origin() != query_skad())
	                            obca_druzyna++;
	                    }
	                    if(obca_druzyna==1&&TO->query_prop(GATE_IS_OUTSIDE))
	                    tell_room(ENV(TO),query_info_security_lvl(1));
	                    else if(obca_druzyna>1&&TO->query_prop(GATE_IS_OUTSIDE))
	                    tell_room(ENV(TO),query_info_security_lvl(2));
	                }
	                else if(TO->query_prop(GATE_IS_OUTSIDE))
	                {
	                    tell_room(ENV(TO),
	                              query_info_security_lvl(3));
	                    return 1;
	                }
	            }
	        }
	     }
    }
    if((query_security_lvl()==0) && !cicho)
        tell_room(ENV(TO),query_info_security_lvl(0));
}

/*
 * Function name: pass_gate
 * Description:   Pass the gate.
 * Arguments:	  arg - arguments given
 */
int
pass_gate(string arg)
{
    int dexh;

    if (!other_gate)
        load_other_gate();

    /*
       The times higher a player can be and still get through
       */
    dexh = 2 + (TP->query_stat(SS_DEX) / 25);

    if (open_status)
    {
        /* Lets say we arbitrarily can bend as dexh indicates.
           For something else, inherit and redefine.
           */
        if ((int)TP->query_prop(CONT_I_HEIGHT) >
                query_prop(DOOR_I_HEIGHT) * dexh)
        {
            write("Jeste^s zbyt du^z" +
                    TP->koncowka("y", "a", "e") +
                    " i za ma^lo zr^eczn" +
                    TP->koncowka("y", "a", "e") +
                    ", ^zeby si^e przecisn^a^c przez " + short(PL_BIE));
            return 1;
        }
        else if ((int)TP->query_prop(CONT_I_HEIGHT) >
                query_prop(DOOR_I_HEIGHT))
        {
            write("Z wielkim trudem przeciskasz si^e przez " + short(PL_BIE) +
                    ".\n");
            saybb(QCIMIE(TP, PL_MIA) + " z wielkim trudem "
                    + "przeciska si^e przez " + TO->short(PL_BIE)
                    + ".\n");
        }

        check_security_lvls(TP,1);

        if(TP->query_prop(NO_PASS_THRU_GATE))
            return 1;


        if (my_pass_mess) tell_object(TP, capitalize(check_call(my_pass_mess)));
        {
            string cmd = check_call();
            TP->move_living(({check_call(pass_commands[0]), check_call(pass_mess)}), other_room);
            TP->add_old_fatigue(-(random(4)));
        }
    }
    else
        write(check_call(fail_pass));

    return 1;
}

/*
 * Function name: open_gate
 * Description:   Open the gate.
 * Arguments:	  arg - arguments given
 */
int
open_gate(string co)
{
    komenda=query_verb();
    mixed brama_ob;

    if(komenda=="zapukaj" || komenda=="zastukaj")
        notify_fail(capitalize(komenda)+" gdzie?\n");
    else if(komenda=="szarpnij" || komenda~="poci�gnij")
        notify_fail(capitalize(komenda)+" co?\n");
    else if(komenda~="zadzwo�")
        notify_fail(capitalize(komenda)+" czym?\n");
    else if (komenda == "uderz")
        notify_fail(capitalize(komenda)+" w co?\n");

    if(czym!="")
    {
        if(co!=czym)
          return 0;
    }

    if (!stringp(komenda))
        return 0;

    if(((komenda=="zapukaj") || (komenda=="uderz")) && (!co))
        return 0;

    if(komenda=="zapukaj")
    {
       if (!parse_command(co, environment(TO), " 'do' %i:" + PL_DOP, brama_ob) &&
               !parse_command(co, environment(TO), " 'w' %i:" + PL_BIE, brama_ob))
        return 0;
    }

    if(komenda=="uderz")
    {
       if (!parse_command(co, environment(TO), " 'w' %i:" + PL_BIE, brama_ob))
        return 0;
    }


    if(komenda=="zastukaj" && co!=czym)
        return 0;

    //if(TP->query_prop(WLASNIE_PUKA) && (query_security_lvl()!=3))
	if (get_alarm(open_alrm))
    {
        notify_fail("Cierpliwo�ci...\n");
        return 0;
    }

    brama_ob = CMDPARSE_STD->normal_access(brama_ob, 0, TO, 1);

    if (!brama_ob || !sizeof(brama_ob) || brama_ob[0] != TO)
        return 0;

    if (!other_gate)
	load_other_gate();

    if (!open_status)
    {
        if (lock_status)
	{   //dluzej otwieramy...
	    write(check_call(open_mess[2]));
	    saybb(QCIMIE(TP, PL_MIA) + " "
	        + check_call(open_mess[0]));
	    if(!(environment(TO)->dzien_noc()))
	    {
	        tell_room(TO->query_other_gate(), "Kto� dobija si� do "+
	            TO->query_other_gate()->short(PL_DOP) + " od drugiej "+
	            "strony.\n");
	    }
        if (get_alarm(open_alrm) == 0) {
            open_alrm = set_alarm(4.0, 0.0, "try_to_open_gate",TP);
        }
	}
	else
	{
	    write(check_call(open_mess[2]));
	    saybb(QCIMIE(TP, PL_MIA) + " "
	        + check_call(open_mess[0]));
        if (get_alarm(open_alrm) == 0) {
            open_alrm = set_alarm(2.0, 0.0, "try_to_open_gate",TP);
        }
	}
    }
    else
    {
    	write(check_call(fail_open));
    }
    return 1;
}

/*
 * Function name: try_to_open_gate
 * Description:   Je�li poziomy bezpiecze�stwa zosta�y sprawdzone to
 *                ustawiamy alarm na otwieranie bram.
 * Arguments:	  Object komu
 */
void
try_to_open_gate(object komu)
{

    if(!check_security_lvls(komu))
    {
        if (get_alarm(open_alrm) == 0) {
            open_alrm = set_alarm(2.5, 0.0, "do_open_gate",check_call(open_mess[1]));
            set_alarm(2.5, 0.0,&other_gate->do_open_gate(check_call(open_mess[1])));
        }
    }
}

/*
 * Function name: do_open_gate
 * Description:   Otwieramy bram� i ustawiamy alarm na zamykanie.
 * Arguments:     Mo�emy poda� stringa mess, kt�ry ustawia jak brama ma si�
 *                otwiera�. Je�li go nie podajemy, to nie b�dzie w og�le.
 */
void
do_open_gate(string mess)
{
    object env;
    env = environment(TO);
    TO->remove_prop(GATE_IS_CLOSED);
    if (strlen(mess))
	tell_roombb(env, mess);
    open_status = 1;
	if(get_alarm(close_alrm) == 0)
    	close_alrm = set_alarm(3.5, 0.0, "do_close_gate",check_call(close_mess[1]));
}

/*
 * Function name: close_gate
 * Description:   Close the gate.
 * Arguments:	  arg - arguments given
 */
int
close_gate(string arg)
{
    mixed brama_ob;

    notify_fail("Co chcesz zamkn^a^c?\n");

    if (!stringp(arg))
        return 0;

    if (!parse_command(arg, environment(TO), "%i:3", brama_ob))
        return 0;

    brama_ob = CMDPARSE_STD->normal_access(brama_ob, 0, TO, 1);

    if (!brama_ob || !sizeof(brama_ob) || brama_ob[0] != TO)
        return 0;

    if (!other_gate)
	load_other_gate();

    if (open_status)
    {
	if (TP->query_stat(SS_STR) < open_str)
	{
            write(capitalize(short(PL_MIA)) + " nie chc" +
                (query_tylko_mn() ? "^a" : "e") + " si^e ruszy^c.\n");
	}
	else
	{
	    write(check_call(close_mess[2]));
	    saybb(QCIMIE(TP, PL_MIA) + " " +
		check_call(close_mess[0]));
	    do_close_gate(check_call(close_mess[1]));
	    other_gate->do_close_gate(check_call(close_mess[1]));
	}
    }
    else
    {
        notify_fail(check_call(fail_close));
        return 0;
    }

    return 1;
}


/*
 * Function name: do_close_gate
 * Description:   Zamykamy bram�.
 * Arguments:     Mo�emy poda� stringa mess, kt�ry ustawi jak brama ma si�
 *                otworzy�. Je�li nie podamy, to go nie b�dzie w og�le.
 */
void
do_close_gate(string mess)
{
    object env;
    env = environment(TO);
    TO->add_prop(GATE_IS_CLOSED,1);
    if (strlen(mess))
	tell_roombb(env, mess);

    open_status = 0;
}

/*
 * Function name: lock_gate
 * Description:   Lock the gate.
 */
int
lock_gate()
{
    lock_status=1;
    return 1;

}

/*
 * Function name: unlock_gate
 * Description:   Unlock the gate.
 */
int
unlock_gate()
{
    lock_status=0;
    return 1;
}

/*
 * Function name: do_unlock_gate
 * Description:   Odblokowujemy bram�.
 * Arguments:     string messs, kt�ry wy�wietlamy na lokacji.
 */
void
do_unlock_gate(string mess)
{
    if (strlen(mess))
	tell_roombb(environment(TO), mess);
    lock_status = 0;
}

/*
 * Function name: godzina_otwarcia
 * Description:   Przyjmuje inta - godzine o ktorej brama ma byc otwierana.
 */
void
godzina_otwarcia(int i)	{ otwieramy = i; }
/*
 * Function name: godzina_zamkniecia
 * Description:   Przyjmuje inta - godzine o ktorej brama ma byc zamykana.
 */
void
godzina_zamkniecia(int i)	{ zamykamy = i; }
/*
 * Function name: query_godzina_otwarcia
 * Description:   Sprawdzamy godzinke, o ktorej otwieramy brame.
 */
int
query_godzina_otwarcia() { return otwieramy; }
/*
 * Function name: query_godzina_zamkniecia
 * Description:   Sprawdzamy godzinke, o ktorej zamykamy brame.
 */
int
query_godzina_zamkniecia() { return zamykamy; }
/*
 * Function name: is_leader_introduced
 * Description:   pierwszy argument to int, 1 jesli jest przedstawiony,
 *                drugi to obiekt wlasnie lider..
 */
void
is_leader_introduced(int i)
{
    leader_introduced = i;
}
/*
 * Function name: query_leader_introduced
 * Description:   int 1 je�li leader dru�yny si� przedstawi�.
 */
int
query_leader_introduced()	{ return leader_introduced; }
/*
 * Function name: query_leader_ob
 * Description:   zwraca obiekt lidera druzyny
 */
object
query_leader_ob()	{ return leader; }

/*
 * Function name: set_security_lvl
 * Description:   Ustawiamy czy wpuszczamy po nocach tylko mieszka�c�w
 *                danego miasta oraz czy w nocy trzeba si� przedstawia�,
 *                by m�c wej��.
 */
int
set_security_lvl(int i)
{
    if(i>3 || i<0)
    {
	write("Z�y poziom bezpiecze�stwa. Ustawiam domy�lny.\n");
        security = 1;
    }
    else
	security = i;
}
/*
 * Function name: query_security_lvl
 * Description:   Sprawdzamy czy wpuszczamy po nocach tylko mieszka�c�w
 *                danego miasta oraz czy w nocy trzeba si� przedstawia�,
 *                by m�c wej��.
 */
int
query_security_lvl() { return security; }
/*
 * Function name: set_skad
 * Description:   Przyjmuje stringa - pochodzenie z miasta, w ktorym
 *                sa bramy. Np. "z Rinde".
 */
void
set_skad(string str)	{ skad = str; }
/*
 * Function name: query_skad
 * Description:   Zwraca stringa - pochodzenie z miasta, w ktorym
 *                sa bramy. Np. "z Rinde".
 */
string
query_skad()	{ return skad; }

/*
 * Function name: set_open
 * Description:   Set the open staus of the gate
 */
void
set_open(int i)	{ open_status = i; }

/*
 * Function name: query_open
 * Description:   Query the open status of the gate.
 */
int
query_open() { return open_status; }

#if 0
/*
 * Function name: set_gate_name
 * Description:	  Set the name of the gate
 */
void
set_gate_name(mixed name)
{
  if (pointerp(name))
      gate_name = name;
  else
      gate_name = ({ name });
}
#endif

/*
 * Function name: set_gate_desc
 * Description:   Set the long description of the gate
 *
void
set_gate_desc(string desc) { gate_desc = desc; }

/*
 * Function name: query_gate_desc
 * Description:   Query the long description of the gate
 *
string
query_gate_desc() { return gate_desc; }

/*
 * Function name: set_open_desc
 * Description:   Set the description of the gate when open
 */
void
set_open_desc(string desc) { open_desc = desc; }

/*
 * Function name: query_open_desc
 * Description:   Query the open description of the gate
 */
string
query_open_desc() { return open_desc; }

/*
 * Function name: set_closed_desc
 * Description:   Set the description of the gate when closed
 */
void
set_closed_desc(string desc) { closed_desc = desc; }

/*
 * Function name: query_closed_desc
 * Description:   Query the description of the gate when closed
 */
string
query_closed_desc() { return closed_desc; }


public string
convert_exit_desc_to_polish(string x)
{
    switch (plain_string(x)) {
	case "n":
	case "polnoc": return "p^o^lnoc";
	case "ne":
	case "polnocny-wschod": return "p^o^lnocny-wsch^od";
	case "e":
	case "wschod": return "wsch^od";
	case "se":
	case "poludniowy-wschod": return "po^ludniowy-wsch^od";
	case "s":
	case "poludnie": return "po^ludnie";
	case "sw":
	case "poludniowy-zachod": return "po^ludniowy-zach^od";
	case "w":
	case "zachod": return "zach^od";
	case "nw":
	case "polnocny-zachod": return "p^o^lnocny-zach^od";
	case "d":
	case "dol": return "d^o^l";
	case "u":
	case "gora": return "g^ora";
	default: return x;
    }
}

/*
 * Function name: set_pass_command
 * Description:   Set which command is needed to pass the gate
 */
void
set_pass_command(mixed command)
{
    if (pointerp(command))
	pass_commands = command;
    else
	pass_commands = ({ command });
    // przeksztalcamy standardowe nazwy wyjsc na POLSKIE /d
    pass_commands = map(pass_commands, convert_exit_desc_to_polish);
}

/*
 * Function name: set_pass_mess
 */
void
set_pass_mess(mixed mess)
{
    if (stringp(mess))
	pass_mess = mess;
    else if (pointerp(mess) && sizeof(mess) == 1)
	pass_mess = mess[0];
    else if (pointerp(mess) && sizeof(mess) > 1)
	pass_mess = mess[0], my_pass_mess = mess[1];
}

/*
 * Function name: query_pass_command
 * Description:   Query what command lets you pass the gate
 */
string *
query_pass_command() { return pass_commands; }

/*
 * Function name: set_fail_pass
 * Description:   Set messaged when failing to pass the gate.
 */
void
set_fail_pass(string mess) { fail_pass = mess; }

/*
 * Function name: query_fail_pass
 * Description:   Query message when failing to pass the gate
 */
string
query_fail_pass()		{ return fail_pass; }

/*
 * Function name: set_open_command
 * Description:   Set command to open the gate
 */
void
set_open_command(mixed command)
{
    if (pointerp(command))
	open_commands = command;
    else
	open_commands = ({ command });
}

/*
 * Function name: query_open_command
 * Description:   Query what command opens the gate
 */
string *
query_open_command() { return open_commands; }

/*
 * Function name: set_open_mess
 * Description:   Set the message to appear when gate opens
 */
void
set_open_mess(string *mess) { open_mess = mess; }

/*
 * Function name: query_open_mess
 * Description:   Query what messages we get when gate is opened
 */
string *query_open_mess() { return open_mess; }

/*
 * Function name: set_fail_open
 * Description:   Set the message when we fail to open gate
 */
void
set_fail_open(string mess)
{
	fail_open = mess;
}

/*
 * Function name: query_fail_open
 * Description:   Query message when open fails
 */
string
query_fail_open() { return fail_open; }

/*
 * Function name: set_close_command
 * Description:   Set what command closes the gate
 */
void
set_close_command(mixed command)
{
    if (pointerp(command))
	close_commands = command;
    else
	close_commands = ({ command });
}

/*
 * Function name: query_close_command
 * Description:   Query what command closes the gate
 */
string *
query_close_command() { return close_commands; }

/*
 * Function name: set_close_mess
 * Description:   Set the message to appear when we close the gate
 */
void
set_close_mess(string *mess) { close_mess = mess; }

/*
 * Function name: query_info_security_lvl
 * Description:   Wy�wietla odpowiedni string odpowiedniego poziomu
 *                bezpiecze�stwa.
 * Arguments:     0 - Wchodzi, bo jest mieszka�cem, wszystko ok.
 *                1 - W dru�ynie ma kogo�, kto nie nale�y do mieszka�c�w,
 *                    ale i tak wchodz�.
 *                2 - W dru�ynie ma kilku, kt�rzy nie s� mieszka�cami,
 *                    ale i tak wchodz�.
 *                3 - Gracz nie jest mieszka�cem i nie wejdzie z tego powodu.
 *                4 - Gracz nie wejdzie, bo w dru�ynie ma kogo�, kto nie nale�y
 *                    do mieszka�c�w.
 *                5 - Gracz nie wejdzie, bo w dru�ynie ma kilku, kt�rzy nie
 *                    nale�� do mieszka�c�w.
 *                6 - Gracz nie wejdzie, bo nie. Po prostu. ;)
 *                7 - Gracz jest cz�onkiem dru�yny prowadzonej przez lidera
 *                    i pr�buje przej�� przez bram�. Wi�c dostaje komunikat
 *                    o tym, �eby sta� grzecznie w szeregu, bo to dow�dca
 *                    zajmuje si� dyplomacj� ;)
 *                8 - Gracz ma si� przedstawi�.
 *                9 - Dru�yna dow�dcy jest nieprzedstawiona. Dostaje komunikat
 *                    o tym, by j� przedstawi�.
 *
 */
string
query_info_security_lvl(int i)
{
    switch(i)
    {
        case 0: return info_security_lvl0();
        case 1: return info_security_lvl1();
        case 2: return info_security_lvl2();
        case 3: return info_security_lvl3();
        case 4: return info_security_lvl4();
        case 5: return info_security_lvl5();
        case 6: return info_security_lvl6();
        case 7: return info_security_lvl7();
        case 8: return info_security_lvl8();
        case 9: return info_security_lvl9();
    }
}
/*
 * Function name: query_close_mess
 * Description:   Query message when we close the gate
 */
string *query_close_mess()		{ return close_mess; }

/*
 * Function name: set_fail_close
 * Description:   Set message when we fail to close the gate
 */
void
set_fail_close(string mess) { fail_close = mess; }

/*
 * Function name: query_fail_close
 * Description:   Query message when we fail to close the gate
 */
string
query_fail_close() { return fail_close; }

/*
 * Function name: set_locked
 * Description:   Set lock status
 */
void
set_locked(int i) { lock_status = i; }

/*
 * Function name: query_locked
 * Description:   Query lock status
 */
int
query_locked() { return lock_status; }

/*
 * Function name: set_other_room
 * Description:   Set which rooms is on the other side
 */
void
set_other_room(string name) { other_room = name; }

/*
 * Function name: query_other_room
 * Description:   Query what room is on the other side
 */
string
query_other_room() { return other_room; }

/*
 * Function name: set_gate_id
 * Description:   Set the id of the gate
 */
void
set_gate_id(string id)
{
    id = "-g-" + id + "-g-";
    gate_id = id;
}

/*
 * Function name: query_gate_id
 * Description:   Query the id of the gate
 */
string
query_gate_id()	{ return gate_id; }

/*
 * Function name: query_other_gate
 * Description:   Get the other gate object pointer. The other
 *		  gate will be loaded if neccesary. If that proovs
 *		  impossible, this gate will autodestruct.
 */
object
query_other_gate()
{
    if (!other_gate)
	load_other_gate();

    return other_gate;
}

/*
 * Function name: load_other_gate
 * Description:   Try to load the gate in the other room. If this
 *		  fails, autodestruct.
 */
void
load_other_gate()
{
    string *gate_ids;
    object *gates;
    int pos;

    seteuid(getuid());

    /*
     * No other side or already loaded.
     */
    if (!strlen(other_room) || other_gate)
	return;

    /*
     * Try to load the other side.
     */
    if (!find_object(other_room))
    {
	other_room->teleledningsanka();
	if (!find_object(other_room))
	{
	    write("B^l^ad w ^ladowaniu drugiej strony bramy: " + other_room + ".\n");
	    remove_gate_info(environment(TO));
	    remove_object();
	    return;
	}
    }

    gate_ids = (string *)other_room->query_prop(ROOM_AS_GATEID);
    gates = (object *)other_room->query_prop(ROOM_AO_GATEOB);
    pos = member_array(gate_id, gate_ids);
    if (pos < 0)
    {
	write("Po drugiej stronie, po za^ladowaniu pokoju nie ma bramy: " +
		other_room + ".\n");
	remove_gate_info(environment(TO));
	remove_object();
	return;
    }

    other_gate = gates[pos];
}

/*
 * Function name: add_gate_info
 * Description:   Add information about this gate to the room it
 *		  stands in. If this gate already exists, autodestruct.
 * Arguments:	  dest - The room that contains the gate.
 */
static void
add_gate_info(object dest)
{
    string *gate_ids;
    object *gates;

    gate_ids = (string *)dest->query_prop(ROOM_AS_GATEID);
    gates = (object *)dest->query_prop(ROOM_AO_GATEOB);
    if (!pointerp(gate_ids))
    {
	gate_ids = ({});
	gates = ({});
    }

    /*
     * One gate of the same type is enough.
     */
    if (member_array(gate_id, gate_ids) >= 0)
    {
	write("Jedna brama wystarczy.\n");
	remove_object();
	return;
    }

    gate_ids += ({ gate_id });
    gates += ({ TO });

    dest->add_prop(ROOM_AS_GATEID, gate_ids);
    dest->add_prop(ROOM_AO_GATEOB, gates);
}

/*
 * Function name: remove_gate_info
 * Description:   Remove information about this gate from the room it
 *		  stands in.
 * Arguments:	  dest - The room that contains the gate.
 */
static void
remove_gate_info(object dest)
{
    string *gate_ids;
    object *gates;
    int pos;

    gate_ids = (string *)dest->query_prop(ROOM_AS_GATEID);
    gates = (object *)dest->query_prop(ROOM_AO_GATEOB);
    if (!sizeof(gate_ids))
	return;

    pos = member_array(gate_id, gate_ids);
    if (pos < 0)
	return;

    gate_ids = exclude_array(gate_ids, pos, pos);
    gates = exclude_array(gates, pos, pos);

    dest->add_prop(ROOM_AS_GATEID, gate_ids);
    dest->add_prop(ROOM_AO_GATEOB, gates);
}

/*
 * Function name: enter_env
 * Description:   The gate enters a room, activate it.
 * Arguments:	  dest - The destination room,
 * 		  old - Where it came from
 */
void
enter_env(object dest, object old)
{
    TP->remove_prop(LIVE_I_INTRODUCING);
    add_gate_info(dest);
}

/*
 * Function name: leave_env
 * Description:   The gate leaves a room, remove it.
 * Arguments:     old - Where it came from,
 * 		  dest - The destination room
 */
void
leave_env(object old, object dest)
{
    if (!old)
	return;
    remove_gate_info(old);
}

/*
 * Function name: opened_or_closed_desc
 * Description:   Jest to ma�y string wrzucany do d�ugiego opisu bramy.
 *                Ma�y zmienny opisik zale��cy od tego, czy brama jest
 *                otwarta, czy zamkni�ta.
 */
string
opened_or_closed_desc()
{
    return "W tej chwili jest " +
        (query_open()==1 ? "otwarta" : "zamkni�ta") + ".";
}

/*
 * Function name: standard_open_mess1
 */
string
standard_open_mess1()
{
    if(komenda=="zapukaj")
        return "puka do "+ short(TP, PL_DOP) + ".\n";
    else if(komenda=="uderz")
        return "uderza w "+ short(TP, PL_BIE) + ".\n";
    else if(komenda=="zastukaj")
        return "stuka "+czym+" "+short(TP, PL_DOP) + ".\n";
    else if(komenda~="zadzwo�")
        return "dzwoni "+czym+" do "+short(TP, PL_DOP) + ".\n";
    else if(komenda~="poci�gnij")
        return "ci�gnie za "+czym+" "+short(TP, PL_DOP) + ".\n";
    else if(komenda=="szarpnij")
        return "szarpie za "+czym+" "+short(TP, PL_DOP) + ".\n";
}

/*
 * Function name: standard_open_mess2
 */
string
standard_open_mess2()
{
    return "Wrota "+short(PL_DOP) + " otwieraj�" +
        /*(query_tylko_mn() ? "j^a" : "") +*/ " si^e.\n";
}

/*
 * Function name: standard_open_mess3
 */
string
standard_open_mess3()
{
    if(komenda=="zapukaj")
        return "Pukasz do "+ short(TP, PL_DOP) + ".\n";
    else if(komenda=="uderz")
        return "Uderzasz w "+ short(TP, PL_BIE) + ".\n";
    else if(komenda=="zastukaj")
        return "Stukasz "+czym+" "+short(TP, PL_DOP) + ".\n";
    else if(komenda~="zadzwo�")
        return "Dzwonisz "+czym+" do "+short(TP, PL_DOP) + ".\n";
    else if(komenda~="poci�gnij")
        return "Ci�gniesz za "+czym+" "+short(TP, PL_DOP) + ".\n";
    else if(komenda=="szarpnij")
        return "Szarpiesz za "+czym+" "+short(TP, PL_DOP) + ".\n";
}

/*
 * Function name: standard_fail_open
 */
string
standard_fail_open()
{
    return capitalize(short(PL_MIA)) +
        (query_tylko_mn() ? " s^a" : " jest") + " ju^z otwar" +
        koncowka("ty", "ta", "te", "ci", "te") + ".\n";
}

/*
 * Function name: standard_close_mess1
 */
string
standard_close_mess1()
{
    return "zamyka " + short(PL_BIE) + ".\n";
}

/*
 * Function name: standard_close_mess2
 */
string
standard_close_mess2()
{
    return "Wrota "+short(PL_DOP) + " zamykaj� si�.\n";
}

/*
 * Function name: standard_close_mess3
 */
string
standard_close_mess3()
{
    return "Zamykasz " + short(TP, PL_BIE) + ".\n";
}

/*
 * Function name: info_security_lvl0
 * Description:   Zwraca stringa z komunikatem nr. 0.
 */
string
info_security_lvl0()
{
    string str;
    switch(random(7))
    {
        case 0: str="Stra�nik wo�a: Dobra, przej�cie!"; break;
        case 1: str="Stra�nik krzyczy: Dooobraaa!"; break;
        case 2: str="Stra�nik wo�a: No juu�!"; break;
        case 3: str="Stra�nik wo�a: Dobraa, otwieram!"; break;
        case 4: str="Stra�nik wo�a: Ju� otwieram!"; break;
        default: str="Stra�nik wo�a: Ju�!"; break;
    }
    str+="\n";
    return str;
}

/*
 * Function name: info_security_lvl1
 * Description:   Zwraca stringa z komunikatem nr. 1
 */
string
info_security_lvl1()
{
    string str;
    switch(random(5))
    {
        case 0: str="Stra�nik wo�a: Wpuszczasz niemieszka�ca na w�asn� "+
                    "odpowiedzialno��!\n"; break;
        case 1: str="Stra�nik wo�a: Obcy wchodz� na twoj� odpowiedzialno��!\n";
                break;
        case 2: str="Stra�nik krzyczy: Obcy wchodzi na twoj� odpowiedzialno��!\n";
                break;
        case 3..4: str=""; break;
        default: str=""; break;
    }
    return str;
}

/*
 * Function name: info_security_lvl2
 * Description:   Zwraca stringa z komunikatem nr. 2
 */
string
info_security_lvl2()
{
    string str;
    switch(random(5))
    {
        case 0: str="Stra�nik wo�a: Wpuszczasz niemieszka�c�w na w�asn� "+
                    "odpowiedzialno��!\n"; break;
        case 1: str="Stra�nik wo�a: Ci obcy wchodz� na twoj� odpowiedzialno��!\n";
                break;
        case 2: str="Stra�nik wo�a: Je�li ci obcy co� tu zmajstruj�, to ty "+
                    "za to b�dziesz odpowiada�!\n"; break;
        case 3..4: str=""; break;
        default: str=""; break;
    }
    return str;
}


/*
 * Function name: info_security_lvl3
 * Description:   Zwraca stringa z komunikatem nr. 3
 */
string
info_security_lvl3()
{
    string str;
    switch(random(5))
    {
        case 0: str="Stra�nik wo�a: Poszed� mi tu st�d! Znikaj!"; break;
        case 1: str="Stra�nik wo�a: S�ysza�e�? Ju� ci� tu nie ma!"; break;
        case 2: str="Stra�nik wo�a: Obcych nie wpuszczamy!"; break;
        case 3: str="Stra�nik wo�a: Obcym wstep wzbroniony! Zmykaj!"; break;
        case 4: str="Stra�nik wo�a: Nie jeste� "+query_skad()+", wi�c "+
                    "zmykaj!"; break;
        default: str="Stra�nik wo�a: Zmykaj!"; break;
    }
    str+="\n";
    return str;
}

/*
 * Function name: info_security_lvl4
 * Description:   Zwraca stringa z komunikatem nr. 4
 */
string
info_security_lvl4()
{
    string str;
    switch(random(4))
    {
        case 0: str="Stra�nik krzyczy: Hola! Nie wejdziesz z obcym!"; break;
        case 1: str="Stra�nik wo�a: Hej! Mam zakaz wpuszczania obcych! "+
                    "Nie wejdziecie!"; break;
        case 2: str="Stra�nik wo�a: Nie wpuszczam obcych!"; break;
        case 3: str="Stra�nik wo�a: Z obcym ci� nie wpuszcz�!"; break;
        default: str="Stra�nik wo�a: Nie ma wej�cia z obcym!"; break;
    }
    str+="\n";
    return str;
}

/*
 * Function name: info_security_lvl5
 * Description:   Zwraca stringa z komunikatem nr. 5
 */
string
info_security_lvl5()
{
    string str;
    switch(random(5))
    {
        case 0: str="Stra�nik krzyczy: Hola! Nie wejdziesz z obcymi!"; break;
        case 1: str="Stra�nik wo�a: Hej! Mam zakaz wpuszczania obcych! "+
                    "Nie wejdziecie!"; break;
        case 2: str="Stra�nik wo�a: Nie wpuszczam obcych!"; break;
        case 3: str="Stra�nik wo�a: Z obcymi ci� nie wpuszcz�!"; break;
        case 4: str="Stra�nik wo�a: Nie wejdziesz z niemieszka�cami!"; break;
        default: str="Stra�nik wo�a: Nie ma wej�cia z obcymi!"; break;
    }
    str+="\n";
    return str;
}

/*
 * Function name: info_security_lvl6
 * Description:   Zwraca stringa z komunikatem nr. 6
 */
string
info_security_lvl6()
{
    string str;
    switch(random(8))
    {
        case 0: if(TO->query_prop(GATE_IS_OUTSIDE))
                   { str="Stra�nik krzyczy: Nie wejdziesz, sio!\n"; }
                else if(TO->query_prop(GATE_IS_INSIDE))
                   { str="Stra�nik krzyczy: Nie wyjdziesz, sio!\n"; }
                break;
        case 1: if(TO->query_prop(GATE_IS_OUTSIDE))
                   {str="Stra�nik wo�a: Posz"+TP->koncowka("ed�","�a")+
                    " mi tu st�d! Nie wejdziesz!\n";}
                else if(TO->query_prop(GATE_IS_INSIDE))
                   {str="Stra�nik wo�a: Posz"+TP->koncowka("ed�","�a")+
                    " mi tu st�d! Nie wyjdziesz!\n";}
                break;
        case 2..3: if(TO->query_prop(GATE_IS_OUTSIDE))
                   { str="Stra�nik wo�a: Mam zakaz wpuszczania!\n"; }
                else if(TO->query_prop(GATE_IS_INSIDE))
                   { str="Stra�nik wo�a: Mam zakaz wypuszczania!\n"; }
                break;
        case 4: str="Stra�nik krzyczy: Nie otwieram!\n"; break;
        case 5: str="Stra�nik wo�a: Id� st�d!\n"; break;
        case 6..7: str=""; break;

        default: str="Stra�nik wo�a: Nie otwieram!\n"; break;
    }
    return str;
}

/*
 * Function name: info_security_lvl7
 * Description:   Zwraca stringa z komunikatem nr. 7
 */
string
info_security_lvl7()
{
    string str;
    switch(random(8))
    {
        case 0 :str="Stra�nik wo�a"+
	        ": Hej, ty tam! Ustaw si� w szeregu psia "+
	        "ma�, bo nie przejdziecie!\n"; break;
	case 1: if(TO->query_prop(GATE_IS_OUTSIDE))
	           str="Stra�nik wo�a: Sta� w szeregu kurwa! Bo nie wpuszcz�!\n";
	        else if(TO->query_prop(GATE_IS_INSIDE))
	           str="Stra�nik wo�a: Sta� w szeregu kurwa! Bo nie wypuszcz�!\n";
	        break;
	case 2: if(TO->query_prop(GATE_IS_OUTSIDE))
	          str="Stra�nik wo�a"+
	          ": St�j w szeregu u licha, bo nigdzie nie wejdziecie!\n";
	        else if(TO->query_prop(GATE_IS_INSIDE))
	          str="Stra�nik wo�a"+
	          ": St�j w szeregu u licha, bo nigdzie nie wyjdziecie!\n";
	          break;
	case 3..4: str="Stra�nik klnie siarczy�cie.\n"; break;
	case 5: str="Stra�nik puka si� w czo�o.\n"; break;
	case 6..7: str=""; break;
	default: str="Stra�nik puka si� w czo�o.\n"; break;
    }
    return str;
}

/*
 * Function name: info_security_lvl8
 * Description:   Zwraca stringa z komunikatem nr. 8
 */
string
info_security_lvl8()
{
    string str;
    switch(random(6))
    {
        case 0: str="Stra�nik wo�a: Przedstaw si�!"; break;
        case 1: str="Stra�nik wo�a: Ej�e! Przedstawi� mi si� tu, "+
                    "bo nie przejdziecie!"; break;
        case 2: str="Stra�nik wo�a: A imi� jakie� masz? Hehe."; break;
        case 3: str="Stra�nik wo�a: O szlag, przedstaw si�!"; break;
        case 4: if(TO->query_prop(GATE_IS_OUTSIDE))
                  {str="Stra�nik wo�a: Hej ty! Nie wpuszcz� ci� ot tak! "+
                  "Przedstaw si�!";}
                else if(TO->query_prop(GATE_IS_INSIDE))
                  {str="Stra�nik wo�a: Hej ty! Nie wypuszcz� ci� ot tak! "+
                  "Przedstaw si�!";}
                break;
        case 5: str="Stra�nik wo�a: Imi�?"; break;
        default: str="Stra�nik wo�a: Imi�?"; break;
    }
    str+="\n";
    return str;
}

/*
 * Function name: info_security_lvl9
 * Description:   Zwraca stringa z komunikatem nr. 9
 */
string
info_security_lvl9()
{
    string str;
    switch(random(6))
    {
        case 0: str="Stra�nik ryczy: Przedstaw swoj� comitiv�! Ale to ju�!";
                break;
        case 1: str="Stra�nik wo�a: A twoja dru�yna? Przedstaw j�!"; break;
        case 2: str="Stra�nik wo�a: A co to za dru�yna, psia ma�? Przedstawi� "+
                "si�!"; break;
        case 3: str="Stra�nik wo�a: O�esz ty, ale dru�yna! Hehe, jak "+
                "si� nazywacie? Panienki z wojenki? Hehe."; break;
        case 4: str="Stra�nik ryczy: Imiona! Wszystkich! Ju�!"; break;
        case 5: str="Stra�nik krzyczy: Ej�e! Imiona podawa�!"; break;
        default: str="Stra�nik krzyczy: Ej�e! Imiona podawa�!"; break;
    }
    str+="\n";
    return str;
}


/*
 * Function name: standard_gate_desc
 */
string
standard_gate_desc()
{
    return "Mocno wygladaj^ac" + koncowka("y", "a", "e", "y", "e") +
        short(PL_MIA) + ".\n";
}

/*
 * Function name: standard_fail_pass
 */
string
standard_fail_pass()
{
    return capitalize(short(PL_MIA)) + " " +
        (query_tylko_mn() ? "s^a" : "jest") + " zamkni^e" +
        koncowka("ty", "ta", "te", "ci", "te") + ".\n";
}

/*
 * Function name: standard_fail_close
 */
string
standard_fail_close()
{
     return capitalize(short(PL_MIA)) + " ju^z " +
         (query_tylko_mn() ? "s^a" : "jest") + " zamkni^e" +
         koncowka("ty", "ta", "te", "ci", "te") + ".\n";
}

/*
 * Function name: standard_fail_unlock
 */
string
standard_fail_unlock()
{
    return capitalize(short(PL_MIA)) + " ju^z " +
        (query_tylko_mn() ? "s^a" : "jest") + " otwar" +
        koncowka("ty", "ta", "te", "ci", "te") + ".\n";
}

string
standard_pass_mess()
{
    return query_verb();
}

/*
 * Function name: is_gate
 * Description:   1 je�li ten obiekt jest bram� :P
 */
public int
is_gate()
{
    return 1;
}

/*
 * Function name: query_widoczni_czlonkowie
 * Description:   Prosta funkcja zwracaj�ca nieukrytych cz�onk�w
 *                dru�yny.
 * Arguments:      1 object x - cz�onek,
 *                 2 object czyjego - leader dru�yny.
 * Returns:       1 lub 0.
 *
 */
int
query_widoczni_czlonkowie(object x, object czyjego)
{
    if(czyjego != x->query_leader())
       return 0;
    if(x->query_no_show())
       return 0;
    return living(x);
}

/*
 * Function name: wyjeb_propa
 * Description:   Usuwamy propa INTRODUCED_AT_GATE
 */
void
wyjeb_propa(object leader)
{
    leader->remove_prop(INTRODUCED_AT_GATE);
}

/*
 * Function name: all_commandss
 * Description:   Parsuje wszystkie komendy podawane na lokacji z bram� i
 *                wyci�ga z nich komend� "przedstaw" i odpowiednio na ni�
 *                reaguje.
 */
public int
all_commandss(string str)
{
    object ob=TP;
    int i;
    object *ludzie=FILTER_LIVE(all_inventory(environment(TO)));
    switch(query_verb())
    {
        case "przedstaw":
          if(!ob->query_leader() && !TO->query_open())
          {
            if(str~="si�")
            {
                is_leader_introduced(1);
                leader=ob;
                leader->add_prop(INTRODUCED_AT_GATE,1);
                set_alarm(25.0,0.0,"wyjeb_propa",leader);
                czlonkowie=leader->query_team();
                if(lock_status) {//dluzej
                    if (get_alarm(open_alrm) == 0) {
                        open_alrm = set_alarm(4.0, 0.0, "try_to_open_gate",TP);
                    }
                }
                else {
                    if (get_alarm(open_alrm) == 0) {
                        open_alrm = set_alarm(2.0, 0.0, "try_to_open_gate",TP);
                    }
                }
            }

            for(i=0;i<sizeof(czlonkowie);i++)
                if(str~=czlonkowie[i]->query_real_name(PL_BIE))
                    czlonkowie[i]->add_prop(INTRODUCED_AT_GATE,1);
          }
            break;

        default: break;
    }
}
