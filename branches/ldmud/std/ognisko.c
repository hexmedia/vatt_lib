/**
 * \file /std/ognisko.c
 *
 * @author Vera
 * @date 13 Sept 2006
 *
 * Jest to po prostu ognisko, zbudowane z chrustu i/lub ga��zi i innych.
 * Mo�na do niego wrzuca� co popadnie, sika� na nie, przeskakiwa� i kilka
 * innych pierd�, ale przede wszystkim jest to arcy KLIMATOWE ognisko -
 * niezb�dne do dobrego rpga ;)
 *
 * TODO:
 * - Sprawdzi� dlaczego rozwala lighta.
 */

inherit "/std/container";
#include <macros.h>
#include <stdproperties.h>
#include <object_types.h>
#include <cmdparse.h>
#include <ss_types.h>
#include <filter_funs.h>
#include <sit.h>

#define DBG(x) find_player("vera")->catch_msg(x+"\n");
#define EVENT(x) tell_room(environment(TO),x);
#define ZAPALONE "_zapalone"

//Prototypy
int objetosc();
int waga();
int set_fuel(int x);
int add_fuel(int x);
int query_lit();
int wypalanie();
int events();
int burning_progress(object x);
int switch_przym();

int sizeof_da_fire=1;
int fuel=0, wypalone=0;
int prev_light_room; /*Pierwotny wska�nik OBJ_I_LIGHT tego pomieszczenia*/
string *przyms=({"ma�y","mali"}); /*ma�e, ogromne etc. */
object *has_added_fuel=({}); /*Obiekty wrzucone do ogniska*/


void
create_ognisko()
{
}

void
create_container()
{
    ustaw_nazwe("ognisko");
    dodaj_przym("ma�y","mali");
    dodaj_przym("zgaszony","zgaszeni");
    set_long("@@opisik@@"+
             //query_lit() != 0 ?
                //"@@opis_sublokacji| P�onie w nim |w|.||" + PL_BIE+ "@@":
                "@@opis_sublokacji| Dostrzegasz w nim ||.||" + PL_BIE+ "@@"+"\n");

    add_prop(CONT_I_VOLUME, &objetosc());
    add_prop(CONT_I_MAX_VOLUME, 500000);
    add_prop(CONT_I_WEIGHT, &waga());
    add_prop(CONT_I_MAX_WEIGHT, 350000);
    add_prop(OBJ_I_LIGHT, 0);
    add_prop(OBJ_M_NO_GET, 1);

    make_me_sitable("przy", "przy ognisku", "od ogniska", 10, PL_MIE, "przy");

    set_type(O_INNE);
    set_fuel(100);

    setuid();
    seteuid(getuid());

    add_prop(CONT_I_DONT_SHOW_CONTENTS, 1);

    //add_subloc("w");
    //add_subloc_prop("w", SUBLOC_S_OB_GDZIE, "wewn�trz ogniska");


    create_ognisko();
}

int
set_fuel(int x)
{
    fuel=x;
    return 1;
}

int
query_fuel()
{
    return fuel;
}

int
objetosc()
{
    int ret=333;

    if(sizeof(all_inventory(TO)))
        foreach(object x : all_inventory(TO))
            ret+=x->query_prop(OBJ_I_VOLUME);

    return ret;
}

int
waga()
{
    int ret=640;

    if(sizeof(all_inventory(TO)))
        foreach(object x : all_inventory(TO))
            ret+=x->query_prop(OBJ_I_WEIGHT);

    return ret;
}

int
query_ognisko()
{
    return 1;
}

int
query_lit()
{
    if(query_prop(ZAPALONE))
        return -1;
    else
        return 0;
}

string opisik()
{
    string str;
    string dodatek_do_opisu="";
    string oswietlenie="";

    str="Podstaw� tego ";

    switch(sizeof_da_fire)
    {
        case 1:     dodatek_do_opisu="malutkiego";
                    oswietlenie="rzucaj�c nik�e �wiat�o na okolic�"; break;
        case 2:     dodatek_do_opisu="ma�ego";
                    oswietlenie="rzucaj�c nik�e �wiat�o na okolic�"; break;
        case 3..4:  dodatek_do_opisu="niewielkiego";
                    oswietlenie="rzucaj�c �wiat�o na okolic�"; break;
        case 5:  dodatek_do_opisu="do�� sporego";
                 oswietlenie="o�wietlaj�c okolic�"; break;
        case 6:  dodatek_do_opisu="sporego";
                 oswietlenie="o�wietlaj�c okolic�"; break;
        case 7..10: dodatek_do_opisu="ogromnego";
                 oswietlenie="rzucaj�c wielkie cienie na okolic�"; break;
        default:
            if(sizeof_da_fire>10)
            {
                dodatek_do_opisu="ogromnego";
                oswietlenie="rzucaj�c nik�e �wiat�o na okolic�";
            }
            else if(sizeof_da_fire<=0)
            {
                dodatek_do_opisu="malutkiego";
                oswietlenie="rzucaj�c wielkie cienie na okolic�";
            }
            break;
    }

    if(dodatek_do_opisu != "")
        str+=dodatek_do_opisu;

    str+=" ogniska s� kawa�ki kory, patyczki i inne "+
         "niewielkie drewienka";

    if(query_lit() != 0)
    {
        str+=". Z ogniska bije ciep�a po�wiata, a p�omienne j�zyki "+
              "igraj� wok� ";
        if(environment(TO)->dzien_noc())
            str+=oswietlenie;
        else
            str+="w swym dzikim ta�cu";

    str+=".";
    }
    else
        str+=". W tej chwili jest zgaszone.";

    return str;
}

int
burn_burn_burn()
{
    TO->add_prop(ZAPALONE,1);
    remove_adj("zgaszony");
    dodaj_przym("p�on�cy","p�on�cy");
    odmien_short();
    odmien_plural_short();
    //prev_light_room=environment(TO)->query_prop(OBJ_I_LIGHT);
    //environment(TO)->remove_prop(OBJ_I_LIGHT);
    //int sett=prev_light_room+1;
    //environment(TO)->add_prop(OBJ_I_LIGHT,sett);

    /*Pierdolony kurwa cont_block_prop !!! Nienawidze tego, kto to wymy�li�!
      Straci�em nerwy... :P */
    cont_block_prop = 0;
    add_prop(OBJ_I_HAS_FIRE, 1);
    add_prop(OBJ_I_LIGHT, 1);
    cont_block_prop = 1;

    if(sizeof(all_inventory(TO)))
    {
        foreach(object x : all_inventory(TO))
        {
             x->add_prop(OBJ_I_HAS_FIRE,1);
             burning_progress(x);
        }

    }

    set_alarm(2.0, 40.0, &wypalanie());
    set_alarm(10.0,60.0, &events());

    return 1;
}

int
extinguish_me_dude()
{
    TO->remove_prop(ZAPALONE);
    remove_adj("p�on�cy");
    dodaj_przym("zgaszony","zgaszeni");
    odmien_short();
    odmien_plural_short();

    cont_block_prop = 0;
    remove_prop(OBJ_I_HAS_FIRE);
    remove_prop(OBJ_I_LIGHT);
    cont_block_prop = 1;

    if(sizeof(all_inventory(TO)))
        foreach(object x : all_inventory(TO))
             x->remove_prop(OBJ_I_HAS_FIRE);

    return 1;
}

int
wypalanie()
{
    if(query_lit() == 0)
        return 1;

    switch(random(4))
    {
        case 0: fuel-=4; /*sizeof_da_fire-=1;*/ break;
        case 1: fuel-=5; /*sizeof_da_fire-=1;*/ break;
        case 2: fuel-=6; break;
        case 3: fuel-=8; break;
    }

    if(random(8) == 1 && fuel < 900)
    	sizeof_da_fire-=1;

    switch_przym();

    if(fuel>12 && fuel<18)
         EVENT(capitalize(short(0))+" powoli przygasa.\n");

    if(fuel>8 && fuel<12)
         EVENT("Ostatnie p�omyki z "+short(PL_DOP)+" powoli "+
                         "przygasaj�.\n");

    if(fuel<=0)
    {
        fuel=0;

        EVENT(capitalize(short(0))+" wypala si�.\n");

        extinguish_me_dude();

        TO->remove_adj("zgaszony");
        TO->dodaj_przym("wypalony","wypaleni");
        odmien_short();
        odmien_plural_short();

        wypalone=1;
        has_added_fuel=({});
    }

    return 1;
}

int
events()
{
    if(query_lit() == 0)
        return 1;

    object ran_obj;

    switch(random(10))
    {
        case 0:
              if(sizeof(all_inventory(TO)))
              {
                  ran_obj=all_inventory(TO)[random(sizeof(all_inventory(TO)))];
                  if(ran_obj->query_prop(OBJ_I_VOLUME) > 100)
                       EVENT("J�zyki p�omieni w ognisku ra�no skacz� po "+
                             ran_obj->short(5)+".\n");
              }
              break;
        case 1:
              switch(random(2))
              {
                  case 0: EVENT("Co� trzasn�o w ognisku.\n"); break;
                  case 1: EVENT("Nagle co� strzeli�o w ognisku.\n"); break;
              }
              break;
        case 2:
              EVENT("W ognisku co� zaskwiercza�o.\n");
              break;
        case 3:
              if(sizeof(all_inventory(TO)))
              {
                  ran_obj=all_inventory(TO)[random(sizeof(all_inventory(TO)))];
                  if(ran_obj->query_prop(OBJ_I_VOLUME) > 100)
                      EVENT("P�omienie wij� si� wok� "+ran_obj->short(PL_DOP)+".\n");
              }
              break;
        case 4:
              EVENT("Ogie� wzbija si� raptownie w g�r�, by po chwili wr�ci� do "+
                    "swego ta�ca.\n");
              break;
        case 5:
              foreach(object playa : FILTER_PLAYERS(all_inventory(environment(TO))))
              {
                  if ((objectp(playa->query_prop(SIT_SIEDZACY)) &&
                      (playa->query_prop(SIT_SIEDZACY) == this_object())) ||
                      (objectp(playa->query_prop(SIT_LEZACY)) &&
                      (playa->query_prop(SIT_LEZACY) == this_object())))
                  {
                      switch(sizeof_da_fire)
                      {
                          case 0..3: playa->catch_msg("Ciep�o bij�ce z ogniska "+
                                           "grzeje przyjemnie.\n");
                                     break;
                          case 4..7: playa->catch_msg("Ciep�o bij�ce z ogniska zaczyna "+
                                           "ci� lekko parzy�.\n"); break;
                          case 8..11: playa->catch_msg("Ciep�o bij�ce z ogniska parzy ci� "+
                                            "niemi�osiernie.\n"); break;
                      }
                  }
              }
              break;
        case 6:
              object *d_obj=FILTER_DEAD(all_inventory(environment(TO)));

              if(!sizeof(d_obj))
                  break;

              if(!environment(TO)->dzien_noc())
                  break;

              object chosen=d_obj[random(sizeof(d_obj))];
              if(!chosen->query_prop(OBJ_I_VOLUME)>500)
                  chosen=d_obj[random(sizeof(d_obj))];
              /*spr�bujmy jeszcze raz,coby nie traci� eventa :P */
              if(!chosen->query_prop(OBJ_I_VOLUME)>500)
                  chosen=d_obj[random(sizeof(d_obj))];
              else
                  break;

              EVENT("Cienie rzucane przez j�zyki p�omieni igraj� weso�o po "+
                    chosen->short(PL_CEL)+".\n");
              break;
        case 7:
              if(environment(TO)->query_prop(ROOM_I_INSIDE))
                  break;

              EVENT("P�omienie nieznacznie uginaj� si� pod wiatrem.\n");
              break;
        case 8:
              if(environment(TO)->query_prop(ROOM_I_INSIDE))
                  break;

              EVENT("Przez chwile czujesz przygnany nag�ym podmuchem "+
                    "wiatru zapach ogniska.\n");
              break;
        case 9:
              EVENT("Z ogniska dobiega weso�y trzask "+
                    "drewienek.\n");
              break;
    }

    return 1;
}

int
spalamy_doszczetnie(object co)
{
    /*mo�e ju� zgasili�my ognisko?*/
    if(query_lit() == 0)
        return 1;

    /*mo�e kto� go jako� wyci�gn��?*/
    if(environment(co) != TO)
        return 1;

    /*Nie pokazujemy tego eventa dla malutkich przedmiocik�w*/
    switch(co->query_prop(OBJ_I_VOLUME))
    {
        case 0..10: break;
        case 11..30:
             foreach(object playa : FILTER_PLAYERS(all_inventory(environment(TO))))
             {
                 if(playa->query_skill(SS_AWARENESS) > 80)
                     playa->catch_msg(capitalize(co->short(0))+" w "+
                            TO->short(PL_MIE)+" spala si� doszcz�tnie.\n");
             }
             break;
        case 31..100:
             foreach(object playa : FILTER_PLAYERS(all_inventory(environment(TO))))
             {
                 if(playa->query_skill(SS_AWARENESS) > 50)
                     playa->catch_msg(capitalize(co->short(0))+" w "+
                            TO->short(PL_MIE)+" spala si� doszcz�tnie.\n");
             }
             break;
        case 101..200:
             foreach(object playa : FILTER_PLAYERS(all_inventory(environment(TO))))
             {
                 if(playa->query_skill(SS_AWARENESS) > 30)
                     playa->catch_msg(capitalize(co->short(0))+" w "+
                            TO->short(PL_MIE)+" spala si� doszcz�tnie.\n");
             }
             break;
        default:
             switch(random(4))
             {
                 case 0..2:
                     EVENT(capitalize(co->short(0))+" w "+
                            TO->short(PL_MIE)+" spala si� doszcz�tnie.\n");
                     break;
                 case 3: break;
             }
             break;
    }

    /*Usu�my z tablicy, co by takie ognisko za du�o miejsca w pami�ci nie
      zajmowa�o;) */
    has_added_fuel-=({co});

    co->remove_object();
    return 1;
}

int
zapalamy_obiekt(object co,float szybkopalnosc)
{
    /*mo�e ju� zgasili�my ognisko?*/
    if(query_lit() == 0)
        return 1;

    /*mo�e kto� go jako� wyci�gn��?*/
    if(environment(co) == TO)
    {
        /*Nie pokazujemy tego eventa dla JU� p�on�cych obiekt�w (np. pochodnia)*/
        if(co->query_prop(OBJ_I_HAS_FIRE))
        {
            set_alarm(szybkopalnosc+10.0,0.0,"spalamy_doszczetnie",co);
            return 1;
        }

        /*Nie pokazujemy tego eventa dla malutkich przedmiocik�w*/
        switch(co->query_prop(OBJ_I_VOLUME))
        {
            case 0..10: break;
            case 11..30:
                 foreach(object playa : FILTER_PLAYERS(all_inventory(environment(TO))))
                 {
                     if(playa->query_skill(SS_AWARENESS) > 180)
                         playa->catch_msg(capitalize(co->short(0))+" w "+
                         TO->short(PL_MIE)+" zajmuje si� ogniem.\n");
                 }
                 break;
            case 31..100:
                 foreach(object playa : FILTER_PLAYERS(all_inventory(environment(TO))))
                 {
                     if(playa->query_skill(SS_AWARENESS) > 80)
                         playa->catch_msg(capitalize(co->short(0))+" w "+
                         TO->short(PL_MIE)+" zajmuje si� ogniem.\n");
                 }
                 break;
            case 101..200:
                 foreach(object playa : FILTER_PLAYERS(all_inventory(environment(TO))))
                 {
                     if(playa->query_skill(SS_AWARENESS) > 40)
                         playa->catch_msg(capitalize(co->short(0))+" w "+
                         TO->short(PL_MIE)+" zajmuje si� ogniem.\n");
                 }
                 break;
            default:
                         EVENT(capitalize(co->short(0))+" w "+
                         TO->short(PL_MIE)+" zajmuje si� ogniem.\n");
                 break;
        }

        set_alarm(szybkopalnosc,0.0,"spalamy_doszczetnie",co);

        co->set_condition(15); /*Dzia�a tylko na ubrania i zbroje*/
    }

    return 1;
}

int
switch_przym()
{
    switch(sizeof_da_fire)
    {
        case 1..2:     przyms=({"ma�y","mali"});        break;
        case 3..4:  przyms=({"niewielki","niewielcy"}) ;break;
        case 5..6:  przyms=({"spory","sporzy"});        break;
        case 7..10: przyms=({"ogromny","ogromni"});     break;
        default:
            if(sizeof_da_fire>10)
                przyms=({"ogromny","ogromni"});
            else if(sizeof_da_fire<=0)
                przyms=({"ma�y","mali"});
            break;
    }

    /*if(!sizeof(TO->query_przymiotniki()))
        return 1;*/

    /*Mo�e ju� takie s�?*/
    /*if(!query_przymiotniki()[0][0]==przyms[0] ||
       !query_przymiotniki()[0][1]==przyms[0])
    {*/
        string last;
        if(TO->query_przymiotniki()[0][0] == "p�on�cy" ||
           TO->query_przymiotniki()[0][0] == "zgaszony")
            last=TO->query_przymiotniki()[0][1];
        else
            last=TO->query_przymiotniki()[0][0];

        if(last==przyms[0])
            return 1;

        remove_adj(last);
        if(query_lit() != 0)
            remove_adj("p�on�cy");
        else
            remove_adj("zgaszony");

        dodaj_przym(przyms[0],przyms[1]);
        if(query_lit() != 0)
            dodaj_przym("p�on�cy","p�on�cy");
        else
            dodaj_przym("zgaszony","zgaszeni");

        odmien_short();
        odmien_plural_short();
//    }

    return 1;
}

int
query_sizeof_da_fire()
{
    return sizeof_da_fire;
}

int
increase_fire(int obj, float o_ile)
{
    /*super tajny wz�r XD*/
    sizeof_da_fire = sizeof_da_fire + ftoi( itof(obj) * o_ile / 2620.0 );

    if(sizeof_da_fire>10)
        sizeof_da_fire=10;
    else if(sizeof_da_fire<0)
        sizeof_da_fire=1;

    add_fuel((obj+ftoi(o_ile))/29);

    switch_przym();

    /*No i dojebmy jakiego� eventa:P */
    if(obj>=1000)
        switch(random(5))
        {
            case 0: EVENT("Ogie� w ognisku wzbija si� dziko w g�r�!\n");
                    break;
            case 1: EVENT("P�omienie wzbijaj� si� na chwil� wysoko w g�r�.\n");
                    break;
            case 2: EVENT("Ogie� ro�nie niebiezpiecznie.\n");
                    break;
            case 3..4: break;
        }


    return 1;
}

int
burning_progress(object co)
{
    /*Sprawdzmy sobie wpierw, czy wrzucany obiekt nie jest zapalony */
    if(co->query_lit() != 0 && query_lit() == 0)
    {
        if(wypalone)
            return 1;

        EVENT(capitalize(short(0))+" zapala si�.\n");
        burn_burn_burn();
    }
    else if(query_lit()==0) /*Je�li ognisko si� nie pali, to nie ma progressu,*/
    {                       /*ale te� nie dodajemy do go do tablicy, �eby�my
                              mogli p�niej zabra� z niego paliwo etc.*/
        //has_added_fuel-=({co});
        return 1;
    }

    /*No to teraz obczajmy materia�y z jakich jest zrobiony 'co'*/
    string *materialy=({}), *ex;
    if(sizeof(co->query_materialy()))
        materialy=co->query_materialy();

    /*We�my materia�, kt�rego jest najwi�cej w danym obiekcie, co by si� nie
      pierdoli� za du�o... */
    float proc_mat=0.0;
    /*Jak szybko wrzucony obiekt si� spala(ca�kowicie) oraz
      ilekro� razy zwi�ksza na ten czas ogie�*/
    float szybkopalnosc=0.0, zwieksza_ogien=0.0;

    foreach(string x : materialy)
    {
//FIXME : przerobi� �eby korzysta�o z funkcji materia��w

        if(procent_materialu(x) > proc_mat)
        {
            proc_mat=procent_materialu(x);
            ex=explode(x,"_");
            /*ex[2] nas interesuje,bo okre�la rodzaj materia�u*/
            switch(ex[2])
            {
                case "tk": /*tkaniny*/
                           szybkopalnosc=5.0+itof(random(5));
                           zwieksza_ogien=1.4;
                           break;
                case "dr": /*drewno*/
                           szybkopalnosc=230.0+itof(random(80));
                           zwieksza_ogien=2.0;
                           break;
                case "sz": /*szlachetne*/
                           szybkopalnosc=900.0;
                           zwieksza_ogien=0.0;
                           break;
                case "rz": /*rzemie�lnicze*/
							switch(ex[3])
							{
								case "marmur":
								case "granit":
								case "zelazo":
								case "stal":
								case "szklo":
								case "kwarc":
                           		szybkopalnosc=1250.0+itof(random(190));
                           		zwieksza_ogien=0.1;
								break;
								default:
								szybkopalnosc=250.0+itof(random(90));
                           		zwieksza_ogien=0.1;
								break;
							}
                           break;
                case "sk": /*sk�ry*/
                           szybkopalnosc=15.0+itof(random(15));
                           zwieksza_ogien=1.6;
                           break;
                default:
                           if(co->query_type() == O_MONETY)
                           {
                               szybkopalnosc=600.0;
                               zwieksza_ogien=1.0;
                           }
                           else
                           {
                               szybkopalnosc=2.0+itof(random(3));
                               zwieksza_ogien=1.0;
                           }
                           break;
            }
        }
    }

    /*Just in case...wg. typu obiektu*/
    if(szybkopalnosc==0.0 && zwieksza_ogien==0.0)
    {
        if(co->query_type() == O_MONETY)
        {
            szybkopalnosc=600.0;
            zwieksza_ogien=1.0;
        }
        else if(co->query_type() ==O_ZWOJE || co->query_type()==O_KSIAZKI ||
                co->query_type()==O_ZIOLA || co->query_type()==O_LINY)
        {
            szybkopalnosc=4.0;
            zwieksza_ogien=1.0;
        }
        else
        {
            szybkopalnosc=2.0+itof(random(3));
            zwieksza_ogien=1.0;
        }
    }

    /*Ustawiamy alarmy*/
    set_alarm(szybkopalnosc,0.0,"zapalamy_obiekt",co,szybkopalnosc);

    /*Je�li nie pobrali�my jeszcze paliwa z tego obiektu...*/
    if(member_array(co,has_added_fuel) == -1)
    {
//DBG("bla! dodajemy do tablicy: "+file_name(co)+".\n");
            set_alarm(2.0,0.0,"increase_fire",co->query_prop(OBJ_I_VOLUME),
                      zwieksza_ogien);
            has_added_fuel+=({co});
        /*z kt�rych tak�e nie pobrali�my paliwka, ale ju� by�y w ognisku*/
          /*Dajmy increase_fire tym obiektom, kt�re by�y w ognisku
          ale z kt�rych nie pobrali�my characzu w postaci paliwa ;)
            if(sizeof(all_inventory(TO)))
            {
                object *aktualne=all_inventory(TO);
                object *sprawdzane=aktualne-has_added_fuel;
                foreach(object s : sprawdzane)
                {
                    set_alarm(2.0,0.0,"increase_fire",
                        co->query_prop(OBJ_I_VOLUME),zwieksza_ogien);
                  /*Dodajemy obiekt do tablicy wrzuconych obiekt�w, tak,
               by�my np. nie podbijali ilo�ci paliwa wrzucaj�c ci�gle
              ten  sam przedmiot*/

    }

    return 1;
}

int
dokladanie(string str)
{

    object *co, *ognisko;
    string *nazwy=({});

    notify_fail(capitalize(query_verb()) + " co do czego?\n");

    if(!strlen(str))
        return 0;

    if(!parse_command(str, environment(TP),
        "%i:"+PL_BIE+" 'do' %i:"+PL_DOP, co, ognisko))
        return 0;

    co = INV_ACCESS(co); // oto jak sie uzywa INV_ACCESSa ! tadam!
    ognisko = NORMAL_ACCESS(ognisko, 0, 0);

    /*if (sizeof(co) != 1)
        return 0;*/
    if (sizeof(ognisko) != 1)
        return 0;

    if(!ognisko[0]->query_ognisko())
        return 0;

    if(ognisko[0] != TO)
        return 0;

    if(!sizeof(co))
        return NF("Co chcesz wrzuci� do " + TO->query_short(PL_DOP) + "?\n");

    /*Czy obiekt jest jaki� specjalny?*/
    foreach(object x : co)
        if(x->query_prop(OBJ_M_NO_DROP))
            write("Nie mo�esz wrzuci� " + x->short(TP, PL_DOP) + " do " + TO->short(TP, PL_DOP) + ".\n");

    write("Wrzucasz " + COMPOSITE_DEAD(co, PL_BIE) + " do " + TO->short(TP, PL_DOP) + ".\n");
    saybb(QCIMIE(TP, PL_MIA) + " wrzuca " + QCOMPDEAD(PL_BIE) +
        " do " + QSHORT(TO, PL_DOP) + ".\n");

    /* przenosimy obiekty do ogniska i wywolujemy na nich burning_progress*/
    foreach(object x : co)
    {
         x->move(ognisko[0]);

         if(query_lit()!=0)
             x->add_prop(OBJ_I_HAS_FIRE,1);

         burning_progress(x);
    }

    return 1;
}

int
wez(string str)
{
    object *co=({}), *ognisko=({});

    if(!strlen(str))
        return 0;

    if(!query_lit())
        return 0;

    if(parse_command(str, environment(TP),
        "%i:"+PL_BIE+" 'z' %i:"+PL_DOP, co, ognisko))
    {
        if(!ognisko[0][1]->query_ognisko()) //Glupio...
            return 0;

        write("Chyba si� na to nie zdob�dziesz.\n");
        return 1;
    }

    foreach(object x : co)
    {
        x->remove_prop(OBJ_I_HAS_FIRE);
        x->remove_prop(OBJ_I_LIGHT);
    }

    return 0;
}

public int
pomoc(string str)
{
    object ogn;

    notify_fail("Nie ma pomocy na ten temat.\n");

    if (!str) return 0;

    if (!parse_command(str, environment(TP), "%o:" + PL_MIA, ogn))
        return 0;

    if (!ogn) return 0;

    if (ogn != this_object())
        return 0;

    if(TP->query_gender())
        write("Oczywi�cie, takie ognisko mo�na zapali� i zgasi�. Mo�esz tak�e "+
          "dorzuci� co� do niego (najlepiej �atwopalne przedmioty, kt�re "+
          "utrzymuj� ogie� przy �yciu), lub zniszczy� je. Ponadto mo�esz "+
          "tak�e wpatrze� si� w nie, przeskoczy�, lub spr�bowa� ogrza� swe "+
          "d�onie nad tym�e ogniskiem.\n");
    else
        write("Oczywi�cie, takie ognisko mo�na zapali� i zgasi�. Mo�esz tak�e "+
          "dorzuci� co� do niego (najlepiej �atwopalne przedmioty, kt�re "+
          "utrzymuj� ogie� przy �yciu), lub zniszczy� je. Ponadto mo�esz "+
          "tak�e wpatrze� si� w nie, przeskoczy�, lub spr�bowa� ogrza� swe "+
          "d�onie nad tym�e ogniskiem. Aczkolwiek i tak zapewne najbardziej "+
          "interesuj�c� ci� opcj� jest sikanie na to. Czy� nie?\n");

    return 1;
}

int
zniszcz(string str)
{
    object *ogn;

    notify_fail("Zniszcz co ?\n");

    if (!str) return 0;

    if (!parse_command(str, environment(TP), " %i:" + PL_BIE, ogn))
        return 0;

    if (!ogn) return 0;

    if (ogn[0][1] != TO)
        return 0;

    if(query_lit() != 0)
    {
        write("P�on�cego ogniska nie spos�b zniszczy�.\n");
        return 1;
    }

    write("Kilkoma szybkimi kopni�ciami rozgarniasz resztki "+TO->short(PL_DOP)+", "+
          "pozostawiaj�c po nim tylko ma�y popi�.\n");
    saybb(QCIMIE(TP,PL_MIA)+" kilkoma szybkimi kopni�ciami rozgarnia resztki "+TO->short(PL_DOP)+", "+
          "pozostawiaj�c po nim tylko ma�y popi�.\n");

    /*Dodajemy do rooma info, �e by�o tu ognisko, by mo�na by�o to wytropi� */
    environment(TO)->add_prop(ROOM_WAS_FIRE,sizeof_da_fire);
    environment(TO)->run_fire_alarm();

    /*Najpierw wyrzu�my rzeczy, kt�re by�y w ognisku */
    if(sizeof(all_inventory(TO)))
        foreach(object x : all_inventory(TO))
            x->move(environment(TO));

    /*Usu�my siedzonko*/
    environment(TO)->remove_sit("przy ognisku");

    /*I usu�my obiekt ogniska*/
    remove_object();

    return 1;
}

int
przeskocz(string str)
{
    object *ogn;

    if(TP->query_prop(SIT_SIEDZACY) || TP->query_prop(SIT_LEZACY))
    {
        write("A mo�e by� najpierw wsta�"+TP->koncowka("","a","o")+"?\n");
        return 1;
    }

    notify_fail("Przeskocz co ?\n");

    if (!str) return 0;

    if (!parse_command(str, environment(TP), " %i:" + PL_BIE, ogn))
        return 0;

    if (!ogn) return 0;

    if (ogn[0][1] != TO)
        return 0;

    if(query_lit() != 0)
    {
        switch(sizeof_da_fire)
        {
            case 0..2:
                write("Przeskakujesz zwinnie nad "+short(PL_NAR)+".\n");
                saybb(QCIMIE(TP,PL_MIA)+" zwinnie przeskakuje nad "+
                      short(PL_NAR)+".\n");
                break;
            case 3..5:
                if(TP->query_acc_exp(5) <= 50000)
                    write("Chyba si� na to nie odwa�ysz.\n");
                else
                {
                    write("Przeskakujesz zwinnie nad "+short(PL_NAR)+".\n");
                    saybb(QCIMIE(TP,PL_MIA)+" zwinnie przeskakuje nad "+
                      short(PL_NAR)+".\n");
                }
                break;
            case 7..8:
                if(TP->query_acc_exp(5) <= 100000)
                    write("Chyba si� na to nie odwa�ysz.\n");
                else
                {
                    write("Przeskakujesz zwinnie nad "+short(PL_NAR)+".\n"+
                          "Auu! Troch� si� poparzy�"+TP->koncowka("e�","a�")+
                          ".\n");
                    saybb(QCIMIE(TP,PL_MIA)+" zwinnie przeskakuje nad "+
                      short(PL_NAR)+".\n");

                    TP->reduce_hp(0, 5000, 10);
                }
                break;
            case 9..10:
                if(TP->query_acc_exp(SS_INT) >= 200000)
                {
                    write("Tylko g�upiec by�by w stanie zrobi� co� takiego.\n");
                    break;
                }

                if(TP->query_acc_exp(SS_DIS) <= 200000)
                    write("Chyba si� na to nie odwa�ysz.\n");
                else
                {
                    write("Pr�bujesz przeskoczy� nad "+short(PL_NAR)+".\n"+
                          "C� za g�upi pomys�...\n");
                    saybb(QCIMIE(TP,PL_MIA) + " wskakuje do " + short(PL_DOP)+".\n");

                    TP->reduce_hp(0, 100000);
                    TP->check_killed(TO, -2);
                }
                break;
            default:
                if(sizeof_da_fire<1)
                {
                    write("Przeskakujesz zwinnie nad "+short(PL_NAR)+".\n");
                    saybb(QCIMIE(TP,PL_MIA)+" zwinnie przeskakuje nad "+
                          short(PL_NAR)+".\n");
                }
                else if(sizeof_da_fire>10)
                {
                    if(TP->query_acc_exp(4) >=200000)
                    {
                        write("Tylko g�upiec by�by w stanie zrobi� co� takiego.\n");
                        break;
                    }

                    if(TP->query_acc_exp(5) <= 200000)
                        write("Chyba si� na to nie odwa�ysz.\n");
                    else
                    {
                        write("Pr�bujesz przeskoczy� nad " + short(PL_NAR) + ".\n" +
                              "C� za g�upi pomys�...\n");
                        saybb(QCIMIE(TP,PL_MIA) + " wskakuje do " + short(PL_DOP)+".\n");

                        TP->reduce_hp(0, 100000);
                        TP->check_killed(TO, 2);
                    }
                }
                break;
        }
    }
    else /*Trzeba nie lada odwagi, �eby przeskoczy� zgaszone ognisko :P */
    {
        write("Przeskakujesz nad "+short(PL_NAR)+".\n");
        saybb(QCIMIE(TP,PL_MIA)+" przeskakuje nad "+ short(PL_NAR)+".\n");
    }

    return 1;
}

int
wpatrz(string str)
{
    object *ogn;
    notify_fail("Wpatrz si� w co ?\n");

    if (!str) return 0;

    //if(str~="si� w ognisko" || str~="si� w ogie�")
    if(!parse_command(str, environment(TP), "'si�' 'w' %i:" + PL_BIE, ogn))
        return 0;


    if (!ogn) return 0;

    if (ogn[0][1] != TO)
        return 0;

    if(query_lit() == 0)
    {
         write("Zamglonym wzrokiem wpatrujesz si� w "+short(PL_BIE)+".\n");
         saybb(QCIMIE(TP,PL_MIA)+" zamglonym wzrokiem wpatruje si� w "+
               short(PL_BIE)+".\n");
         return 1;
    }

    write("Zamglonym wzrokiem wpatrujesz si� w igraj�ce w ognisku "+
          "j�zyki p�omieni.\n");
    saybb(QCIMIE(TP,PL_MIA)+" zamglonym wzrokiem wpatruje si� w igraj�ce "+
          "w ognisku j�zyki p�omieni.\n");

    return 1;
}

int
nasikaj(string str)
{
    object *ogn;

    if(TP->query_prop(SIT_SIEDZACY) || TP->query_prop(SIT_LEZACY))
    {
        write("A mo�e by� najpierw wsta�"+TP->koncowka("","a","o")+"?\n");
        return 1;
    }

    notify_fail("Nasikaj na co ?\n");

    if (!str) return 0;

    if (!parse_command(str, environment(TP), " 'na' %i:" + PL_BIE, ogn))
        return 0;

    if (!ogn) return 0;

    if (ogn[0][1] != TO)
        return 0;

    if(TP->query_gender() !=0)
    {
        write("Przykro mi, ale nie obdarzono ci� odpowiednim narz�dziem do tego.\n");
        return 1;
    }

    if(query_lit() == 0)
    {
        write("Sikasz na "+short(PL_BIE)+".\n");
        saybb(QCIMIE(TP,PL_MIA)+" sika na "+short(PL_BIE)+".\n");
        return 1;
    }

    switch(sizeof_da_fire)/*Jak zwykle..Emote jest zale�ny od rozmiaru ognia*/
    {
        case 0..3:
            write("Sikasz na "+short(PL_BIE)+".\n"+
                  "Ognisko ga�nie z sykiem.\n");
            saybb(QCIMIE(TP,PL_MIA)+" sika na "+short(PL_BIE)+".\n"+
                  "Ognisko ga�nie z sykiem.\n");
            extinguish_me_dude();
            break;
        case 4..8:
            write("Sikasz na "+short(PL_BIE)+".\n");
            saybb(QCIMIE(TP,PL_MIA)+" sika na "+short(PL_BIE)+".\n");
            fuel=fuel - fuel / 2;
            sizeof_da_fire-=3;
            if(query_lit()!=0)
                EVENT("Ognisko przygasa z g�o�nym sykiem.\n");
            break;
        case 9..10:
            write("Sikasz na "+short(PL_BIE)+".\n");
            saybb(QCIMIE(TP,PL_MIA)+" sika na "+short(PL_BIE)+".\n");
            fuel=fuel - fuel / 8;

            EVENT("Z ogniska dobiega g�o�ny syk.\n");
            break;
        default:
            if(sizeof_da_fire<0)
            {
                write("Sikasz na "+short(PL_BIE)+".\n"+
                      "Ognisko ga�nie z sykiem.\n");
                saybb(QCIMIE(TP,PL_MIA)+" sika na "+short(PL_BIE)+".\n"+
                      "Ognisko ga�nie z sykiem.\n");
                extinguish_me_dude();
            }
            else if(sizeof_da_fire>10)
            {
                write("Sikasz na "+short(PL_BIE)+".\n");
                saybb(QCIMIE(TP,PL_MIA)+" sika na "+short(PL_BIE)+".\n");
                fuel=fuel - fuel / 12;

                EVENT("Z ogniska dobiega g�o�ny syk.\n");
            }
            break;
    }

    return 1;
}

int
ogrzej(string str)
{
    object *ogn;
    notify_fail("Ogrzej co nad czym ?\n");

    if (!str) return 0;

    if(!parse_command(str, environment(TP),
        " 'd�onie' / 'r�ce' 'nad' %i:" + PL_NAR, ogn))
        return 0;


    if (!ogn) return 0;

    if (ogn[0][1] != TO)
        return 0;


    if(query_lit() == 0)
    {
        write("Zdaje si�, �e nici z ogrzewania. Ognisko nie p�onie!\n");
        return 1;
    }

    write("Wystawiasz d�onie w kierunku "+short(PL_DOP)+", by je nieco "+
          "ogrza�.\n");
    saybb(QCIMIE(TP,PL_MIA)+" wystawia swe d�onie w kierunku "+
          short(PL_DOP)+", by je nieco ogrza�.\n");


    return 1;
}

//TODO...
/*int
zalej(string str)
{
    object *ogn, *poj;

    if (!str) return 0;

    if(query_verb() ~= "wylej")
    {
        notify_fail("Wylej zawarto�� czego na co ?\n");

        if (!parse_command(str, environment(TP),
            " 'zawarto��' %i:"+PL_DOP+" 'na' %i:", poj, ogn))
           return 0;

        poj = INV_ACCESS(poj); // oto jak sie uzywa INV_ACCESSa ! tadam!
        ogn = NORMAL_ACCESS(ogn, 0, 0);

        if (!ogn) return 0;
        if (!poj) return 0;

        if (ogn[0][1] != TO)
            return 0;

dump_array(poj);dump_array(ogn);



    }
    else if(query_verb() ~= "zalej")
    {
        notify_fail("Zalej co czym sk�d ?\n");




    }


    return 1;
}*/

void
init()
{
    ::init();
    add_action("zapalanie", "zapal");
    add_action("zgaszanie", "zga�");
    add_action("dokladanie", "do��");
    add_action("dokladanie", "dorzu�");
    add_action("dokladanie", "wrzu�");
    add_action("dokladanie", "w��");
    add_action("wez","we�");
    add_action("zniszcz","zniszcz");
    /*add_action("zalej","zalej");
    add_action("zalej","wylej");*/
    add_action("nasikaj","nasikaj");
    add_action("nasikaj","wysikaj");
    add_action("wpatrz","wpatrz");
    add_action("przeskocz","przeskocz");
    add_action("ogrzej","ogrzej");

    add_action("pomoc", "?", 2);
}



int
zapalanie(string str)
{
    mixed *co;

    if(wypalone)
    {
        notify_fail("To ognisko jest ju� kompletnie wypalone, nie zapali si� ju�.\n");
        return 0;
    }

    if(!str || !parse_command(str, all_inventory(environment(this_player())),
        "%i:"+PL_BIE, co))
    {
        notify_fail(capitalize(query_verb()) + " co?\n");
        return 0;
    }

    if (!sizeof(filter(all_inventory(this_player()), &->query_krzemien())))
    {
        notify_fail("Nie masz czym wykrzesa^c ognia.\n");
        return 0;
    }

    if(TO->query_prop(ZAPALONE))
    {
        notify_fail("Ale� to ognisko ju� p�onie.\n");
        return 0;
    }

    TO->remove_adj("zgaszony");

    write("Zapalasz "+TO->short(PL_BIE)+".\n");
    say(capitalize(QCIMIE(TP,PL_MIA))+" zapala "+TO->short(PL_BIE)+".\n");

    fuel-=10;

    burn_burn_burn();

    /*TO JEST W BURN_BURN_BURN!!
    if(sizeof(all_inventory(TO)))
        foreach(object x : all_inventory(TO))
        {
            burning_progress(x);
            x->add_prop(OBJ_I_HAS_FIRE,1);
        }
     */

    return 1;
}

int
zgaszanie(string str)
{
    mixed *co;

    if(!str || !parse_command(str, all_inventory(environment(this_player())),
        "%i:"+PL_BIE, co))
    {
        notify_fail(capitalize(query_verb()) + " co?\n");
        return 0;
    }

    if(!TO->query_prop(ZAPALONE))
    {
        notify_fail("Ale� to ognisko wcale nie p�onie.\n");
        return 0;
    }

    write("Gasisz "+TO->short(PL_BIE)+".\n");
    say(capitalize(QCIMIE(TP,PL_MIA))+" gasi "+TO->short(PL_BIE)+".\n");

    extinguish_me_dude();

    return 1;
}

int
add_fuel(int x)
{
    fuel+=x;
    return 1;
}
