 /* Std pocisku do broni strzeleckiej,
    stworzone przez Aldeima, grudzien 2006.
    Poprawki: Vera/04/07*/

inherit "/std/object";

#include <wa_types.h>
#include <stdproperties.h>
#include <macros.h>

int jakosc, typ;

public void
create_pocisk()
{
}

/* Ustawia jakosc pocisku w zakresie 1-10. */
void
set_jakosc(int x)
{
  if (x < 0 || x > 10) return;
  jakosc = x;
}

int
query_jakosc()
{
  return jakosc;
}

/* Ustawia typ obrazen zadawanych przez pocisk.
   Dla strzal i beltow powinno byc to W_IMPALE,
   natomiast dla kamieni W_BLUDGEON. */
void
set_typ(int x)
{
  typ = x;
}

int
query_typ()
{
  return typ;
}

/* Funkcja wywolywana przy trafieniu pociskiem. Mozna tu
   dodawac wszelkiej masci efekty ogniowe, trujace itp.
   w przypadku pociskow specjalnego rodzaju. */
void
efekt_specjalny(object strzelec, object ofiara, object bron)
{
}

nomask void
create_object()
{
    add_prop(OBJ_I_WEIGHT, 100);
  add_prop(OBJ_I_VOLUME, 400);
  set_jakosc(1);
  add_prop(OBJ_I_VALUE, 100 * jakosc);
  set_typ(W_IMPALE);
  ustaw_nazwe("pocisk");
  create_pocisk();
}

/*
 *wywo�ywane podczas przenies_pocisk()
 */
void
niszczymy()
{
	if(random(2))
		jakosc-=1;
	else if(random(2))
		jakosc-=2;
	else
		jakosc-=3;
		

	if(jakosc <= 0)
	{
		if(random(2))
			destruct();
		else
		{
			if(typ==1)
				dodaj_przym("z�amany","z�amani");
			else
				dodaj_przym("p�kni�ty","p�kni�ci");
				
			jakosc=0;
		}
	}
}
