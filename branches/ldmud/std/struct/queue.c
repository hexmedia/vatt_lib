#pragma strict_types

#define MAX_QUEUE_SIZE		5000

mixed queue;
int pos_front, pos_back, queue_size;

void
create()
{
    queue = allocate(MAX_QUEUE_SIZE);
    pos_front = 1;
    pos_back = 0;
    queue_size = 0;
}

public int
remove_object()
{
    destruct();
    return 1;
}

int
size()
{
    return queue_size;
//    return ((pos_back - pos_front + MAX_QUEUE_SIZE) % MAX_QUEUE_SIZE) + 1;
}

void
push_front(mixed x)
{
    if (queue_size == MAX_QUEUE_SIZE)
	throw("The queue is full.\n");

    pos_front = (pos_front + MAX_QUEUE_SIZE - 1) % MAX_QUEUE_SIZE;
    queue[pos_front] = x;
    queue_size++;
}

mixed
pop_front()
{
    mixed ret;

    if (queue_size == 0)
	throw("The queue is empty.\n");

    ret = queue[pos_front];
    queue[pos_front] = 0;
    pos_front = (pos_front + 1) % MAX_QUEUE_SIZE;
    queue_size--;
    return ret;
}

void
push_back(mixed x)
{
    if (queue_size == MAX_QUEUE_SIZE)
	throw("The queue is full.\n");

    pos_back = (pos_back + 1) % MAX_QUEUE_SIZE;
    queue[pos_back] = x;
    queue_size++;
}

mixed
pop_back()
{
    mixed ret;

    if (queue_size == 0)
	throw("The queue is empty.\n");

    ret = queue[pos_back];
    queue[pos_back] = 0;
    pos_back = (pos_back + MAX_QUEUE_SIZE - 1) % MAX_QUEUE_SIZE;
    queue_size--;
    return ret;
}
