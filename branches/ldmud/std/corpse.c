/**
 * \file /std/corpse.c
 *
 * Drobne zmiany: skoro czas rzeczywisty - czemu rozk�adamy si� tak szybko? ^_^
 *
 *                   Lil. 23.08.2005
 *
 * Troch� zmian tu i tam (cia�a ju� short 'cia�o wiewi�rki')
 * no i eventy, opisy longi, shorty, wilki i ghoule by Vera.
 *
 * Przebudowa�em na u�ycie nazyw g��wnej, doda�em rozr�nienia na znaj�cych i nie znaj�cych. [Krun]
 *
 * TODO:
 *  - WILK_PATH i GHOUL_PATH powinny zosta� przeniesione do jakiego� zewn�trznego pliku.
 */

#pragma save_binary
#pragma strict_types

inherit "/std/container";

#include <files.h>
#include <macros.h>
#include <cmdparse.h>
#include <language.h>
#include <ss_types.h>
#include <wa_types.h>
#include <composite.h>
#include <filter_funs.h>
#include <object_types.h>
#include <stdproperties.h>

/* rozk�ad trwa 2 doby */
#define DECAY_TIME          2880

#define DBG(x)              find_player("vera")->catch_msg(x);
#define BYL_EVENT_JEDZONKO  "_byl_event_jedzonko"
#define WILK_PATH           "/d/Standard/obj/wilk_event_trupy.c"
#define GHOUL_PATH          "/d/Standard/obj/ghoul_event_trupy.c"

static  int
            decay,
            decay_id,
            known_flag,     // 1 - neverknown, 2 - alwaysknown
            kiedy_event,    // w jakim procentowym statium rozk�adu maj� pojawi� si� ghoule/wilki
            ktory_event;    // co ma si� pojawi� 1 - wilk, 0 - ghoul (ghoul r�b bardziej roz�o�one trupki:))
        string
           *met_name,
           *nonmet_name,
           *nonmet_pname,
           *state_desc,
           *pstate_desc,
            file,
           *tmp = ({}),
           *tmp2 = ({});
        mixed
            leftover_list;

/*
 * Prototypy
 */
public  int     get_leftover(string arg);
public  int     get_leftover(string arg);
static  int     move_out(object ob);
        void    decay_fun();
        void    decay_fun();
        string  dlugi_opis();

// Musimy redefiniowa� kilka parse_command�w, �eby uzyska� odpowiedni opis.

public string *
parse_command_id_list(int przyp)
{
    string *ret;

    ret = ::parse_command_id_list(przyp);

    ret += ({
        slownik_pobierz("cia�o")[0][przyp],
        slownik_pobierz("szcz�tki")[0][przyp],
        slownik_pobierz("sterta")[0][przyp] + " szcz�tk�w" });

    if(!sizeof(nonmet_name) || !sizeof(met_name))
        return ret;

    if(known_flag != 2)
        ret += ({nonmet_name[przyp]});

    if(known_flag != 1 || previous_living()->query_met(met_name[0]))
        ret += ({met_name[przyp]});

    return ret;
}

public string *
parse_command_plural_id_list(int przyp)
{
    string *ret;

    ret = ::parse_command_id_list(przyp);

    ret += ({
        slownik_pobierz("cia�o")[0][przyp],
        slownik_pobierz("szcz�tki")[0][przyp],
        slownik_pobierz("sterta")[0][przyp] + " szcz�tk�w" });

    if(!sizeof(nonmet_name) || !sizeof(met_name))
        return ret;

    if(known_flag != 2)
        ret += ({nonmet_pname[przyp]});

    return ret;
}

/*
 * Pomocnicza funkcyjka, pierdolka. Procentowo podaje decay
 */
int
get_proc_decay()
{
    return (decay * 100) / DECAY_TIME;
}

void
init()
{
    ::init();

    add_action(get_leftover,    "wyrwij");
    add_action(get_leftover,    "wytnij");
}

void
create_corpse()
{
}

nomask void
create_container()
{
    add_prop(OBJ_I_SEARCH_TIME, 2);
    add_prop(CONT_I_DONT_SHOW_CONTENTS, 1); /* Sami wypiszemy zawartosc */

    create_corpse();

    decay = DECAY_TIME;

    kiedy_event = 20 + random(62);
    ktory_event = (random(82 - kiedy_event) > 41);
}

void
reset_corpse()
{
}

nomask void
reset_container()
{
    reset_corpse();
}

void set_file(string str)
{
    file = str;
}

void
ustaw_denata()
{
    int i;

    string *temp, *temp2;

    object pob;

    pob = previous_object();

    if(pob->query_prop(LIVE_I_NEVERKNOWN))
        known_flag = 1;
    else if(pob->query_prop(LIVE_I_ALWAYSKNOWN))
        known_flag = 2;
    else
        known_flag = 0;


    if(known_flag != 2)
    {
        nonmet_name     = allocate(6);
        nonmet_pname    = allocate(6);

        for (i = 0; i < 6; i++)
        {
            nonmet_name[i]  = pob->query_rasa(i);
            nonmet_pname[i] = pob->query_prasa(i);
        }
    }

    if(known_flag != 1)
    {
        met_name        = allocate(6);

        for (i = 0; i < 6; i++)
            met_name[i] = lower_case(pob->query_name(i));
    }

    mixed przymiotniki = ({pob->query_przym(-1, i), pob->query_pprzym(-1, i)});
    for(int i = 0 ; i < sizeof(przymiotniki) ; i++)
        dodaj_przym(przymiotniki[i][0], przymiotniki[i][1]);

    //Ehh, ludziska, nie tak, po kij ustaw shorty, trzeba troche poredefiniowa� funkcji i b�dzie jak znalaz�:P
    ustaw_nazwe_glowna("cia�o");

    ustaw_nazwe(pob->query_rasa());

    set_long(&dlugi_opis());

    change_prop(CONT_I_WEIGHT, pob->query_prop(CONT_I_WEIGHT));
    change_prop(CONT_I_VOLUME, pob->query_prop(CONT_I_VOLUME));
    decay_id = set_alarm(60.0, 60.0, decay_fun);
}

public string
short(mixed fob, mixed przyp = -1)
{
    if(przyp = -1 && intp(fob))
    {
        przyp   = fob;
        fob     = previous_living();
    }
    else if(!fob)
        fob = previous_living();

    if(!sizeof(met_name))
        return ::short(fob, przyp);

    if(fob->query_name(0) == met_name[0])
        return oblicz_przym("tw�j", "twoi", przyp, PL_MESKI_NOS_NZYW, 0) + " " + slownik_pobierz("cia�o")[0][przyp];

    return slownik_pobierz("cia�o")[0][przyp] + " " + (fob->query_met(met_name[0]) ? capitalize(met_name[przyp]) : ::short(fob, PL_DOP));
}

public string
plural_short(mixed fob, int przyp = -1)
{
    if(przyp = -1 && intp(fob))
    {
        przyp   = fob;
        fob     = previous_living();
    }
    else if(!fob)
        fob = previous_living();

    if(!sizeof(met_name))
        return ::plural_short(fob, przyp);

    if(fob->query_name(0) == met_name[0])
        return oblicz_przym("tw�j", "twoi", przyp, PL_MESKI_NOS_NZYW, 1) + " " + slownik_pobierz("cia�o")[1][przyp];

    return slownik_pobierz("cia�o")[1][przyp] + " " + (fob->query_met(met_name[0]) ? capitalize(met_name[przyp]) : ::plural_short(fob, PL_DOP));
}

/**
 * Ustawia poziom rozk�adu cia�a. Cia�o zaczyna rozk�ada�
 * si� na poziomie 10 (nieprawda, bo 2880.Lil), zupe�nie rozk�ada si� przy zej�ciu do 0.
 * @param d  stopie� rozk�adu.
 */
void
set_decay(int d)
{
    decay = d;
}

/**
 * @return Zwraca stopie� rozk�adu cia�a. Cia�o zaczyna rozk�ada� si�
 *         na poziomie 10(nieprawda, bo 2880.Lil), za� zupe�nie rozk�ada si� przy zej�ciu
 *         do 0.
 */
int
query_decay()
{
    return decay;
}

string
dlugi_opis()
{
    object pob, *inv;
    string ret;

    pob = vbfc_caller();

    if(!pob || !query_ip_number(pob) || pob == this_object())
        pob = this_player();

    if(pob->query_real_name() == lower_case(met_name[0]))
        ret = "Jest to twoje w�asne cia�o.\n";
    else if (pob->query_met(met_name[0]))
        ret = "Jest to martwe cia�o " + capitalize(met_name[PL_DOP]) + ".\n";
    else
        ret = "Jest to martwe cia�o " + nonmet_name[PL_DOP] + ".\n";

    inv = all_inventory(this_object());

    if (!sizeof(inv))
        return ret;

    return ret + "Zauwa�asz przy " + query_zaimek(PL_MIE) + " " + FO_COMPOSITE_DEAD(inv, pob, PL_BIE) + ".\n";
}

int
query_corpse()
{
    return 1;
}

string
query_nonmet_name()
{
    if(sizeof(nonmet_name))
        return nonmet_name[0];
    else
        return "";
}

void
zarzuc_temat(int nr_eventu)
{
    if(ENV(TO)->jest_dzien())
        nr_eventu = 1;

    object *trupki = filter(all_inventory(ENV(TO)),&->query_corpse());

    //losujemy
    int ile_wilkow = random(5) + 2;
    int ile_ghouli = random(2) + 1;

    //w og�le to musz� by� przynajmniej 2 �eby cokolwiek si� dzia�o
    if(sizeof(trupki) < 2)
        return;

    if(query_prop(BYL_EVENT_JEDZONKO))
        return;

    //tylko wi�ksze cia�ka
    if(query_prop(CONT_I_VOLUME) < 900)
        return;

    //tylko na specjalnego typu lokacjach event ten sie pojawi
    int type = ENV(TO)->query_prop(ROOM_I_TYPE);
    if(type & ROOM_BEACH != ROOM_BEACH          &&
        type & ROOM_FOREST != ROOM_FOREST       &&
        type & ROOM_FIELD != ROOM_FIELD         &&
        type & ROOM_MOUNTAIN != ROOM_MOUNTAIN   &&
        type & ROOM_TRACT != ROOM_TRACT)
    {
        return;
    }

    object *npce = ({ });
    switch(nr_eventu)
    {
        case 1: //wilki
            for(int z = 0 ; z < ile_wilkow ; z++)
                npce += ({clone_object(WILK_PATH)});
            break;
        case 0: //ghoule
            for(int z = 0 ; z < ile_ghouli ; z++)
               npce += ({clone_object(GHOUL_PATH)});
            break;

        default: break;
    }

    foreach(object x : npce[1..])
        npce[0]->team_join(x);

    //sprytny myk: npce przybywaja, jesli sie uda;)
    string *lokacje = ENV(TO)->query_exit_rooms();
    object skad;
    //no chyba, �e na tej lokacji nikogo ni ma, to po co? ;)
    if(!sizeof(FILTER_PLAYERS(all_inventory(ENV(TO)))))
        skad = ENV(TO);
    int tt = 0;
    while(sizeof(lokacje) && !objectp(skad))
    {
        if(!sizeof(FILTER_PLAYERS(all_inventory(find_object(lokacje[tt])))))
            skad = find_object(lokacje[tt]);
        else
            lokacje -= ({ lokacje[tt] });

        tt++;
    }

    //je�li si� nie uda�o (tj. na wszystkich s�siednich lokacjach s� gracze)
    if(!objectp(skad))
        skad=ENV(TO);

    string *scz = znajdz_sciezke(skad, ENV(TO));

    if(sizeof(scz) > 0)
        string kierunek = znajdz_sciezke(skad,ENV(TO))[0];
    else
        skad = ENV(TO);

    if(!kierunek)
        skad = ENV(TO);

    foreach(object x : npce)
        x->move(skad);

    if(skad == ENV(TO))
    {
        tell_room(ENV(TO),UC(COMPOSITE_LIVE(npce,PL_MIA)+" przybywa "+ (sizeof(npce) > 1 ? "j�" : "") +
            "wiedzion" + (sizeof(npce) > 1 ? "e" : "y") + " zapachem padliny.\n"));
    }
    else
        npce[0]->command(kierunek);

    foreach(object x : trupki)
        x->add_prop(BYL_EVENT_JEDZONKO,1);
}

/*
 * Nazwa funkcji : decay_fun
 * Opis          : Funkcja rozkladajaca cialo o jeden stopien. Przy zejsciu
 *		   na poziom 3(nieprawda, bo 60.Lil) cialo rozklada sie na 'resztki'. Przy zejsciu
 *		   na 0 i te sie rozkladaja.
 */
void
decay_fun()
{
    object *ob, obj;
    string desc;
    int i, j, flag;

    decay--;

    //jaki� tam evencik, zwyk�y napisik..
    if(!random(5))
    if(get_proc_decay() < 95)
    {
        switch(random(20))
        {
            case 0:
                tell_room(ENV(TO), "Czujesz smr�d padliny.\n");
                break;
            case 1:
                tell_room(ENV(TO), "Powietrze wype�nia fetor zgni�ego mi�sa.\n");
                break;
            case 2:
                if(!sizeof(FILTER_IS_SEEN(TO,({TP}))))
                    tell_room(ENV(TO), "Uderza w ciebie smr�d zgnilizny pochodz�cy z "+ QSHORT(TO,PL_DOP)+".\n");
                else
                    tell_room(ENV(TO), "Uderza w ciebie smr�d zgnilizny.\n");
                break;
            case 3:
                if(get_proc_decay() > 20)
                {
                    if(!sizeof(FILTER_IS_SEEN(TO,({TP}))))
                        tell_room(ENV(TO), "�cierwo "+query_prop(CORPSE_M_RACE)[1]+ " cuchnie okropnie.\n");
                    else
                        tell_room(ENV(TO),"Co� tu okropnie cuchnie.\n");
                }
                else
                    tell_room(ENV(TO), "�mierdzi tu paskudnie.\n");

                break;
            case 4:
                tell_room(ENV(TO), "Po okolicy roztacza si� nieprzyjemny zapach padliny.\n");
                break;
            case 5:
                tell_room(ENV(TO), "Smr�d dobiegaj�cy z padliny staje si� coraz bardziej "+
                    "uporczywy. Krzywisz si� mimowolnie.\n");
                break;
            default: break;
        }
    }

    //fiufiu, no to jedziemy z koksem
    if(get_proc_decay() == kiedy_event)
        zarzuc_temat(ktory_event);

    int case1, case2, case3, case4, case5;
#define WYK_CASE "wykonane_casey_decay_funa"
    int *wyk_case;
    if(pointerp(wyk_case = TO->query_prop(WYK_CASE)))
    {
        case1 = wyk_case[0];
        case2 = wyk_case[1];
        case3 = wyk_case[2];
        case4 = wyk_case[3];
        case5 = wyk_case[4];
    }

    switch(get_proc_decay())
    {
        case 81..100:
            if(!case1)
                ustaw_shorty( tmp, tmp2, PL_NIJAKI_NOS);
            else
                case1 = 1;
            break;

        case 62..80:
            if(!case2)
            {
                if(!random(5)) //evencik
                {
                    if(!sizeof(FILTER_IS_SEEN(TO,({TP}))))
                    {
                        tell_room(ENV(TO),"Cia�o onegdaj �ywe z ka�d� chwil� "+
                            "coraz bardziej przypomina zwyk�� kup� lekko nadgni�e"+
                            "go ju� mi�sa.\n");
                    }
                }
            }
            else
                case2 = 1;

            break;

        case 50..61:
            if(!case3)
            {
                if(!random(5)) //evencik
                    tell_room(ENV(TO),"");
            }
            else
                case3 = 1;

            break;

        case 15..20:
            if(!case4)
            {
//                 if(!random(5)) //evencik
                {
                    if(!sizeof(FILTER_IS_SEEN(TO,({TP}))))
                    {
                        tell_room(ENV(TO),"Zw�oki "+query_prop(CORPSE_M_RACE)[1]+
                            " z biegiem czasu zamieniaj� si� w trudne "+
                            "do zidentyfikowania, cuchn�ce �cierwo.\n");
                        ustaw_shorty("�cierwo");
                        dodaj_nazwy("�cierwo");
                    }
                }
            }
            else
                case4 = 1;
            break;

        case 4..6:
            if(!case5)
            {
                ob = filter(all_inventory(this_object()), move_out);
                    /* fix this to get singular/plural of 'appear' */
                i = ((sizeof(ob) != 1) ? sizeof(ob) : ((ob[0]->query_prop(HEAP_I_IS)) ? (int)ob[0]->num_heap() : 1));

                if(!sizeof(FILTER_IS_SEEN(TO,({TP}))))
                {
                    tell_roombb(environment(this_object()), QCSHORT(this_object(), PL_MIA) +
                            " rozk�ada si�, pozostawiaj�c po sobie " +
                            (i ? "sm�tne resztki, w postaci " + COMPOSITE_DEAD(ob, PL_DOP)
                            : "tylko sm�tne resztki") + ".\n", ({}), this_object());
                }

                state_desc = ({ "sterta", "sterty", "stercie", "sterte", "stert�",
                    "stercie" });

                pstate_desc = ({ "sterty", "stert", "stertom", "sterty",
                    "stertami", "stertach" });

                ustaw_nazwe(state_desc, pstate_desc, PL_ZENSKI);

                ustaw_shorty( ({ "@@short_func|0", "@@short_func|1", "@@short_func|2",
                    "@@short_func|3", "@@short_func|4", "@@short_func|5" }),
                    ({ "@@pshort_func|0", "@@pshort_func|1", "@@pshort_func|2",
                    "@@pshort_func|3", "@@pshort_func|4", "@@pshort_func|5" }),
                    PL_ZENSKI);

                for (i = 0; i < 6; i++)
                {
                    state_desc[i] = state_desc[i] + " rozk�adaj�cych si� szcz�tk�w";
                    pstate_desc[i] = pstate_desc[i] + " rozk�adaj�cych si� szcz�tk�w";
                }
            }
            else
                case5 = 1;
            break;
    }
    TO->add_prop(WYK_CASE, ({case1, case2, case3, case4, case5}));

    if (decay > 0)
        return;

    for (i = 0; i < sizeof(leftover_list); i++)
    {
        if (leftover_list[i][4])
        {
            for (j = 0; j < leftover_list[i][2]; j++)
            {
                obj = clone_object(leftover_list[i][0]);
                obj->leftover_init(leftover_list[i][1], query_prop(CORPSE_M_RACE), query_prop(CORPSE_I_RRACE) - 1,
                    leftover_list[i][4],query_prop(CONT_I_WEIGHT));
                obj->move(environment(this_object()), 0);
                flag = 1;
            }
        }
    }

    if (flag)
    {
        if(!sizeof(FILTER_IS_SEEN(TO,({TP}))))
        {
            tell_roombb(environment(this_object()), QCSHORT(this_object(), PL_MIA) + " rozpada si�.\n",
                ({}), this_object());
        }
    }

    map(all_inventory(this_object()), move_out);
    remove_object();
}

static int
move_out(object ob)
{
    if (ob->move(environment(this_object())))
        return 0;
    else
        return 1;
}

void
remove_object()
{
    map(all_inventory(this_object()), move_out);

    ::remove_object();
}

varargs mixed
query_race(int przyp)
{
    return query_prop(CORPSE_M_RACE)[przyp];
}

varargs mixed
query_prace(int przyp)
{
    return query_prop(CORPSE_M_PRACE)[przyp];
}

/*
 * Function name: add_leftover
 * Description:   Add leftovers (at death) to the body.
 * Arguments:	  leftover - The leftover list to use.
 */
varargs public void
add_leftover(mixed list)
{
    leftover_list = list;
}

/*
 * Function name: query_leftover
 * Description:   Return the leftover list. If an organ is specified, that
 *		  actual entry is looked for, otherwise, return the entire
 *		  list.
 *		  The returned list contains the following entries:
 *		  ({ objpath, organ, nitems, vbfc, hard, cut})
 * Arguments:	  organ - The organ to search for W MIANOWNIKU.
 */
varargs public mixed
query_leftover(string organ, int przyp = PL_MIA)
{
    int i, j;
    mixed ret = ({ });

    if(!sizeof(leftover_list))
        return ({ });

    if(!strlen(organ))
        return leftover_list;

    for (i = 0; i < sizeof(leftover_list); i++)
    {
        if (przyp == PL_MIA)
        {
            if (organ ~= leftover_list[i][1])
                return ({ leftover_list[i] });
        }
        else
        {
            mixed odm = slownik_pobierz(leftover_list[i][1]);

            if (organ ~= odm[0][przyp])
                return ({ leftover_list[i] });

            if (organ ~= odm[1][przyp])
            {
                for (j = 0; j < leftover_list[i][2]; ++j)
                   ret += ({ leftover_list[i] });
            }
        }
    }

    return ret;
}

/*
 * Function name: remove_leftover
 * Description:   Remove a leftover entry from a body.
 * Arguments:	  organ - Which entry to remove W MIANOWNIKU.
 * Returns:       1 - Ju�, removed, 0 - Not found.
 */
public int
remove_leftover(string organ)
{
    int i;

    if (!sizeof(leftover_list))
        return 0;

    leftover_list = filter(leftover_list, operator(!) @ &operator(~=)(organ,) @ &operator([])(,1));
#if 0
    for (i = 0; i < sizeof(leftover_list); i++)
        if (leftover_list[i][1][PL_BIE] == organ)
            leftover_list = leftover_list[0..(i - 1)] + leftover_list[(i + 1)..(sizeof(leftover_list))];
#endif
}

/**
 * Funkcja odpowiadaj�ca za wyrywanie, wycinanie kawa�k�w cia�a z cia�a.
 */
public int
get_leftover(string arg)
{
    mixed   corpses, leftovers;
    object  *found, *weapons, theorgan;
    int     i, slash;
    string  organ, vb, fail;

    if (this_player()->query_prop(TEMP_STDCORPSE_CHECKED))
        return 0;

    vb = query_verb();

    notify_fail(capitalize(vb) + " co z czego?\n");  /* access failure */

    if (!arg)
        return 0;

    if(!CAN_SEE_IN_ROOM(TP))
    {
        write("Nie dasz rady tego zrobi� po ciemku.\n");
        return 0;
    }

    if (this_player()->query_attack())
    {
        write("Jeste� w trakcie walki - lepiej skoncentruj si� na niej, " +
            "bo inaczej bedziesz wygl�da� jak " + short(PL_MIA) + "!\n");
        return 1;
    }

    if (!parse_command(arg, environment(this_player()), "%s 'z' %i:" + PL_DOP, organ, corpses))
        return 0;

    if (!strlen(organ))
        return 0;

    found = VISIBLE_ACCESS(corpses, "find_corpse", this_object());

    if (sizeof(found) == 1)
    {
        switch(vb)
        {
            case "wytnij":
                slash = 0;
                if (!sizeof(weapons = this_player()->query_weapon(-1)))
                {
                    notify_fail("@@get_fail:" + file_name(this_object()) +
                        "|Warto by trzyma� w r�ce co�, czym mo�na by wyci��, " +
                        "nie uwa�asz?\n@@");
                    return 0;

                }

                for (i = 0 ; i < sizeof(weapons) ; i++)
                    if (weapons[i]->query_dt() & W_SLASH)
                        slash = 1;

                if (slash == 0)
                {
                    notify_fail("@@get_fail:" + file_name(this_object()) +
                        "|Je�li chcesz co� wyci��, poszukaj lepiej czego� " +
                        "ostrzejszego.\n@@");
                    return 0;
                }

                break;

            case "wyrwij":
            default:
                slash = 0;
        }

        if (vb == "wytnij" && slash == 0)
        {
            notify_fail("@@get_fail:" + file_name(this_object()) +
            "|Je�li chcesz co� wyci��, poszukaj lepiej czego� " +
            "ostrzejszego.\n@@");
            return 0;
        }

        leftovers = found[0]->query_leftover(organ, PL_BIE);
        if (!sizeof(leftovers))
        {
            notify_fail("@@get_fail:" + file_name(this_object()) +
            "|W " + found[0]->short(PL_MIE) +
            " nie ma niczego takiego.\n@@");
            return 0;
        }

        foreach (mixed leftover : leftovers)
        {
            if (!sizeof(leftover) || leftover[2] == 0)
            {
                notify_fail("@@get_fail:" + file_name(this_object()) +
                    "|W " + found[0]->short(PL_MIE) +
                    " nie ma niczego takiego.\n@@");
                return 0;
            }

            if (leftover[5] && slash == 0)
            {
                notify_fail("Nie mo�esz tego tak po prostu wyrwa�. U�yj no�a, " +
                        "albo czego� w tym stylu...\n");
                return 0;
            }

            if (strlen(leftover[3]))
                fail = check_call(leftover[3]);

            if (strlen(fail))
            {
                notify_fail("@@get_fail:" + file_name(this_object()) + "|" +
                    fail + ".\n@@");
                return 0;
            }

            if(leftover[2]-- == 0)
                remove_leftover(leftover[1]);

            theorgan = clone_object(leftover[0]);
            theorgan->set_type(leftover[7]);
            if (leftover[6])
            {
                theorgan->remove_prop(OBJ_M_NO_SELL);
                theorgan->add_prop(OBJ_I_VALUE, leftover[6]);
            }
            theorgan->leftover_init(leftover[1],
            found[0]->query_prop(CORPSE_M_RACE), found[0]->query_prop(CORPSE_I_RRACE) - 1,
            leftover[4],query_prop(CONT_I_WEIGHT));

            if (theorgan->move(this_player(), 0))
                theorgan->move(environment(this_player()));

            this_player()->set_obiekty_zaimkow(({ theorgan }), found);

            vb = (vb == "wytnij" ? "wycina" : "wyrywa");

            organ = slownik_pobierz(leftover[1])[0][PL_BIE];
            saybb(QCIMIE(this_player(), PL_MIA) + " " + vb + " " + organ +
                " z " + QSHORT(found[0], PL_DOP) + ".\n");
            write(capitalize(vb) + "sz " + organ + " z " + found[0]->short(PL_DOP)
                + ".\n");

        }

        return 1;
    }

    set_alarm(0.5, 0.0, &(this_player())->remove_prop(TEMP_STDCORPSE_CHECKED));
    this_player()->add_prop(TEMP_STDCORPSE_CHECKED, 1);

    if (sizeof(found))
        notify_fail("@@get_fail:" + file_name(this_object()) +
            "|Spr�buj bardziej sprecyzowa�, o kt�re cia�o ci chodzi.\n@@");
    else
        notify_fail("@@get_fail:" + file_name(this_object()) +
            "|Nie widzisz nigdzie w okolicy takiego cia�a.\n@@");
    return 0;
}

string
get_fail(string fault)
{
    return fault;
}

public int
find_corpse(object ob)
{
    if ((function_exists("create_container") == CORPSE_OBJECT) &&
        ((environment(ob) == this_player()) ||
        (environment(ob) == environment(this_player()))))
    {
        return 1;
    }

    return 0;
}


/*
 * Function name: search_fun
 * Description:   This function is called when someone searches the corpse
 *		  as set up in create_container()
 * Arguments:	  player - The player searching
 *		  str    - If the player specifically said what to search for
 * Returns:       A string describing what, if anything the player found.
 */
string
search_fun(string str, int trail)
{
    mixed left;
    string *found;
    int i;

    left = query_leftover();
    found = ({});

    for (i = 0; i < sizeof(left); i++)
    {
        mixed odm = slownik_pobierz(left[i][1]);

        if (left[i][2] == 1)
            found += ({ odm[0][PL_BIE] });
        else if (left[i][2] > 1)
        {
            found += ({ LANG_SNUM(left[i][2], PL_BIE, odm[2]-1) + " " +
                odm[1][LANG_PRZYP(left[i][2], PL_BIE, odm[2]-1)] });
        }
    }

    if (sizeof(found) > 0)
    {
        return "Po dok�adnym przeszukaniu cia�a stwierdzasz, �e " +
            this_player()->koncowka("m�g�by�", "mog�aby�") + " wyrwa� lub wyci�� " +
            COMPOSITE_WORDS(found) + ".\n";
    }

    return ::search_fun(str, trail);
}

public string
query_corpse_auto_load()
{
    string toReturn = " @C@ ";
    return toReturn;
}

public string
init_corpse_arg(string arg)
{
    if (arg == 0)
        return 0;
}

public string
query_auto_load()
{
    return 0;
//    return ::query_auto_load() + query_corpse_auto_load();
}

public string
init_arg(string arg)
{
    return ::init_arg(arg);
//    return init_corpse_arg(::init_arg(arg));
}

/**
 * Funkcja identyfikuj�ca obiekt jako cia�o.
 *
 * @return 1 zawsze
 */
public int is_corpse() { return 1; }

