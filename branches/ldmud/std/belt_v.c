/* FIXME, niedokonczone jeszcze.
 * Standard do pas�w.
 * Umo�liwia on przytraczanie woreczk�w do pasa, a tak�e przypianie pochew 
 * na bronie oraz sakiewek na monety.
 *
 *                                        Vera, 9.08.2006
 */

inherit "/std/armour";

#include <object_types.h>
#include <wa_types.h>
#include <materialy.h>
#include <macros.h>
#include <stdproperties.h>
#include <pl.h>
#include <cmdparse.h>

int przytrocz(string str);
public int pomoc(string str);

create_armour()
{
    ustaw_nazwe("pas");
    dodaj_przym("zwyk^ly","zwykli");
    dodaj_przym("sk^orzany","sk^orzani");
    set_long("Jest to przeci^etny pas ze ^swi^nskiej, grubiej sk^ory.\n");
    add_prop(OBJ_I_WEIGHT, 600);
    add_prop(OBJ_I_VOLUME, 1000);
    add_prop(ARMOUR_I_D_ROZCIAGLIWOSC, 90);
    add_prop(OBJ_I_VALUE, 11);
    ustaw_material(MATERIALY_SK_SWINIA,100);
    set_type(O_UBRANIA);
    set_slots(A_WAIST);
}

void
init()
{
    ::init();
    add_action(przytrocz,"przytrocz");
    //przypinanie pochew jest w std pochew.
    add_action("pomoc", "?", 2);
}

int
filter_belt(object ob)
{
    if (function_exists("filter_belt",ob) != 0 &&
        CAN_SEE(TP, ob))
        return 1;
    return 0;
}

int przytrocz(string str)
{
    object *pas, *co;
    notify_fail("Przytrocz co gdzie ?\n");
    if(!str) return 0;
    if(!parse_command(str, TP->subinventory(), "%i"+PL_BIE+ " 'do' %i:"+PL_DOP, co, pas))
        return 0;
    if(!pas || !co) return 0;
//    if(pas != TO)
//        return 0;

//    pas = NORMAL_ACCESS(pas, 0, 0);
    pas = CMDPARSE_STD->normal_access(pas, "filter_lamp", TO, 1);
    if(!sizeof(pas)||!sizeof(co))
        return 0;

    if(sizeof(pas) > 1)
    {
        notify_fail("Zdecyduj si�, do czego chcesz to przytroczy�.\n");
        return 0;
    }

    /*ret = obs[0]->attach_now(this_player(),gdzie);

    if (ret)
        this_player()->set_obiekty_zaimkow(obs);*/

    if(co[0]->query_name() != "woreczek" || co[0]->query_name() != "sakiewka")
    {
        notify_fail("Nie mo�esz tego przytroczy�.\n");
        return 0;
    }

    

    return 1;
}

public int
pomoc(string str)
{
    object ob;
    notify_fail("Nie ma pomocy na ten temat.\n");
    if(!str) return 0;
    if(!parse_command(str, TP, "%o:" + PL_MIA, ob))
        return 0;
    if(!ob) return 0;
    if(ob != TO)
        return 0;
    write("Mo�esz przytroczy� jaki� woreczek lub sakiewk� do pasa.\n");
    return 1;
}
