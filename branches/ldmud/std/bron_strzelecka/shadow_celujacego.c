/**
 * \file /std/bronie_strzeleckie/shadow_celujacego.c
 *
 * To shadow nak�adany na celuj�ceg podczas celowania do jakiego� obiektu.
 *
 * @author Krun
 * @date Grudzie� 2007/Stycze� 2008
 */

inherit "/std/shadow.c";

object scelujacego_cel;
object scelujacego_bron;

public void
ustaw_sc_cel(object c)
{
    scelujacego_cel = c;
}

public object
query_sc_cel()
{
    return scelujacego_cel;
}

public void
ustaw_sc_bron(object b)
{
    scelujacego_bron = b;
}

public object
query_sc_bron()
{
    return scelujacego_bron;
}

public int
remove_shadow_celujacego()
{
    remove_shadow();
    return 1;
}

public void
enter_env(object dest, object old)
{
    shadow_who->enter_env(dest, old);
    scelujacego_bron->celujacy_zmienil_pozycje(old, dest, scelujacego_cel);
}