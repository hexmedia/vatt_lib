/**
 * \file /std/luk.c
 *
 * Standard �uku oparty o standard broni strzeleckiej.
 *
 * @author Krun
 * @date Grudzie� 2007/Stycze� 2008
 */

inherit "/std/bron_strzelecka";

#include <pl.h>
#include <macros.h>
#include <stdproperties.h>

public void
create_luk()
{
}

/**
 * Redefiniujemy t� funkcj� aby od razu poustawia� wszystkie warto�ci,
 * je�li nie s� jeszcze ustawione kiedy j� definiujemy.
 *
 * @param j jako�� broni
 * @author Krun
 */
void
ustaw_jakosc(int j)
{
    ustaw_celnosc(j);          //Celno�� broni
    ustaw_sile_razenia(j);     //Si�a ra�enia broni

    ::ustaw_jakosc(j);
}

/**
 * Kreator broni strzeleckiej.
 *
 * @author Adleim, Krun
 */
nomask void
create_bron_strzelecka()
{
    ustaw_nazwe(({ "^luk", "^luku", "^lukowi", "^luk", "^lukiem", "^luku" }),
        ({ "^luki", "^luk^ow", "^lukom", "^luki", "^lukami",
        "^lukach" }), PL_MESKI_NOS_NZYW);

    set_long("Najzwyczajniejszy w ^swiecie ^luk.\n");

    ustaw_potrzebna_sile(40);   //Si�a potrzebna do u�ycia broni
    ustaw_zasieg(1);            //Standardowo
    ustaw_typ(W_L_LUK);         //Typ broni.

    dodaj_komende_ladowania("na�� %p:" + PL_BIE + " na %c:" + PL_BIE + " %b:" + PL_DOP, "na�o�y�", "nak�adasz", "nak�ada");
    dodaj_komende_naciagania("naci�gnij %c:" + PL_BIE + " %b:" + PL_DOP, "naci�gn��",
        "zaczynasz naci�ga�|naci�gn��e�", "zaczyna_naci�ga�|naci�gn��");
    dodaj_komende_naciagania("napnij %c:" + PL_BIE + " %b:" + PL_DOP, "napi��",
        "zaczynasz napina�|napi��e�", "zaczyna napina�|napi��");
    dodaj_komende_strzelania("zwolnij %c:" + PL_BIE + " %b:" + PL_DOP, "zwolni�", "zwalniasz", "zwalnia");
    dodaj_komende_strzelania("strzel", "strzeli�", "strzelasz", "strzela", PL_DOP);
    dodaj_komende_strzelania("pu�� %c:" + PL_BIE + " %b:" + PL_DOP, "pu�ci�", "puszczacz", "puszcza");

    ustaw_opis_zaladowanego_pocisku("Na jego %c:"+ PL_BIE + " na�o�ono %p:" + PL_DOP + ".\n");
    ustaw_opis_napietej_broni("Jego %c:" + PL_BIE + " jest napi�ta przez %k:" + PL_BIE + ".\n");

    if(!cieciwa)
        ustaw_cieciwe("/std/bron_strzelecka/cieciwa.c");
    else
        wloz_cieciwe_do_broni();

    create_luk();
}

public void
zmien_cieciwe()
{
    ::zmien_cieciwe();
}
