/**
 * \file /std/bronie_strzeleckie/shadow_celu.c
 *
 * Shadow nak�adany na cel w chwili kiedy kto� do niego celuje.
 *
 * @author Krun
 * @date Grudzie� 2007/Stycze� 2008
 */
inherit "/std/shadow.c";

#include <macros.h>

object scl_celujacy;
object scl_bron;

public void
enter_env(object dest, object old)
{
    shadow_who->enter_env(dest, old);
    scl_bron->cel_zmienil_pozycje(old, dest, scl_celujacy);
}

public void
ustaw_scl_celujacego(object c)
{
    scl_celujacy = c;
}

public object
query_sc_celujacy()
{
    return scl_celujacy;
}

public void
ustaw_scl_bron(object b)
{
    scl_bron = b;
}

public object
query_scl_bron()
{
    return scl_bron;
}

public int
remove_shadow_celu()
{
    remove_shadow();
    return 1;
}

public void
remove_object()
{
    find_player("krun")->catch_msg("cf = " + calling_function());
//     if(calling_function(-1) != "cel_zmienil_pozycje")
//         scl_bron->cel_zmienil_pozycje(ENV(shadow_who), 0, scl_celujacy);
    shadow_who->remove_object();
}