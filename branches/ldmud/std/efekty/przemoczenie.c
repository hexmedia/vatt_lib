inherit "/std/object";

#include <stdproperties.h>
#include <macros.h>

#define SUBLOC		"przemoczenie_subloc"

int alarm_eventowy;

void event_przemoczenia(float delay);

void
create_object()
{
    ustaw_nazwe("przemoczenie");
    add_prop(OBJ_M_NO_GIVE, 1);
    add_prop(OBJ_M_NO_DROP, 1);
    set_no_show();
}

public void
przemocz(mixed ob)
{
    if (stringp(ob))
	ob = find_object(ob);

    if (member_array(SUBLOC, ob->query_sublocs()) != -1)
	ob->query_subloc_obj(SUBLOC)->remove_object();

    this_object()->move(ob, 1);
    ob->add_subloc(SUBLOC, this_object(), 0);
    alarm_eventowy = set_alarm(5.0, 0.0, &event_przemoczenia(5.0));
    set_alarm(300.0, 0.0, "wysusz");
}

void
event_przemoczenia(float delay)
{
    set_this_player(environment(this_object()));
    saybb("Z " + QIMIE(this_player(), PL_DOP) + " kapi^a krople wody.\n");
    this_player()->catch_msg("Kapi^a z ciebie krople wody.\n");
    alarm_eventowy = set_alarm(delay+2.0, 0.0, &event_przemoczenia(delay+2.0));
}

public void
wysusz()
{
    environment(this_object())->remove_subloc(SUBLOC);
    this_object()->remove_object();
}

public string
show_subloc(string subloc, object on_obj, object for_obj)
{
    if (subloc != SUBLOC)
	return "";

    if (on_obj == for_obj)
	return "Jeste^s ca^lkowicie mokr" + on_obj->koncowka("y", "a", "e") +
	    ".\n";
    else
	return "Jest ca^lkowicie mokr" + on_obj->koncowka("y", "a", "e") +
	    ".\n";
}

public int
query_jestem_efektem_przemoczenia()
{
	return 1;
}
