/**
 * \file /std/zwierze.c
 *
 * @author Vera
 * @date 25 Aug 2006
 *
 * U�yj tego obiektu, aby stworzy� dowolne DZIKIE zwierze.
 * np. kot, pies, borsuk, �o�, nied�wied� etc.
 *
 * STD nieco przeze mnie przerobiony. By unikn�� bezsensownej walki
 * z kotem czy innym zwierzakiem - oraz by umo�liwi� polowania z
 * prawdziwego zdarzenia, a nie takie pierdy, �e szczur mnie zabija :P
 * I takie tam.... ^_^
 *
 */
#pragma save_binary
#pragma strict_types

#include <pl.h>
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <object_types.h>
#include <filter_funs.h>

inherit "/std/creature";

inherit "/std/act/seqaction";
inherit "/std/act/trigaction";
inherit "/std/act/domove";
inherit "/std/act/action";
inherit "/std/act/attack";

void ploszymy_sie(object przez_kogo);

int run_away_alarm,
    ploszenie_alarm,
    oswojone;           //Czy zwierze jest oswojone.
//object ostatnia_lok;

#define QEXC query_combat_object()

void
create_zwierze()
{
    ustaw_odmiane_rasy(({"zwierz�", "zwierz�cia", "zwierz�ciu", "zwierz�", "zwierz�ciem",
        "zwierz�ciu"}), ({"zwierz�ta", "zwierz�t", "zwierz�tom", "zwierz�ta",
        "zwierz�tami", "zwierz�tach"}), PL_NIJAKI_NOS);

    dodaj_przym("standardowy", "standardowi");
    dodaj_przym("zwyk�y", "zwykli");

    set_long("Masz przed sob� standardowe zwierze.\n");
}

nomask void
create_creature()
{
    add_leftover("/std/skora", "sk�ra", 1, 0, 1, 1, 0, O_SKORY);
    add_leftover("/std/leftover", "mi�so", 1, 0, 0, 1, 0, O_JEDZENIE);
    add_leftover("/std/leftover", "z�b", random(5) + 2, 0, 1, 0, 0, O_KOSCI);
    add_leftover("/std/leftover", "czaszka", 1, 0, 1, 1, 0, O_KOSCI);
    add_leftover("/std/leftover", "ko��", random(3) + 2, 0, 1, 1, 0, O_KOSCI);
    add_leftover("/std/leftover", "serce", 1, 0, 0, 1, 0, O_JEDZENIE);
    add_leftover("/std/leftover", "oko", 2, 0, 0, 0, 0, O_JEDZENIE);

    create_zwierze();

    dodaj_nazwy(({"zwierz�", "zwierz�cia", "zwierz�ciu", "zwierz�", "zwierz�ciem",
        "zwierz�ciu"}), ({"zwierz�ta", "zwierz�t", "zwierz�tom", "zwierz�ta",
        "zwierz�tami", "zwierz�tach"}), PL_NIJAKI_NOS);
}

void
reset_zwierze()
{
}

nomask void
reset_creature()
{
    reset_zwierze();
}

/*
 * Function name:  default_config_zwierze
 * Description:    Sets some necessary values for this creature to function
 *                 This is basically stats and skill defaults.
 */
varargs public void
default_config_zwierze(int lvl)
{
    default_config_creature(lvl);
}

/*
 * Function name: add_attack
 * Description:   Add an attack to the attack array.
 * Arguments:
 *             wchit: Weapon class to hit
 *             wcpen: Weapon class penetration
 *	       dt:    Damage type
 *             %use:  Chance of use each turn
 *	       id:    Specific id, for humanoids W_NONE, W_RIGHT etc
 *
 * Returns:       True if added.
 */
public int
add_attack(int wchit, int wcpen, int damtype, int prcuse, int id)
{
    if (!QEXC)
        return 0;
    return (int)QEXC->cb_add_attack(wchit, wcpen, damtype, prcuse, id);
}

/*
 * Function name: remove_attack
 * Description:   Removes a specific attack
 * Arguments:     id: The attack id
 * Returns:       True if removed
 */
public int
remove_attack(int id)
{
    if (!QEXC)
        return 0;
    return (int)QEXC->cb_remove_attack(id);
}

/**
 * Dodajemy do livinga hiltokacje.
 *
 * @param ac        Ac hitlokacji
 * @param prchit    Procentowa szansa na trafienie w hitlokacje
 * @param nazwa      opis hitlokacji
 * @param id        id hitlokacji
 * @param przym     przymiotniki
 * @param hp        czy doda� hp info
 *
 * @return 0 nie dodany
 * @return !0 dodany
 */
public int
add_hitloc(mixed ac, int prchit, mixed nazwa, int id, mixed przym = 0, int hp = 1)
{
    if(!QEXC)
        return 0;

    return (int)QEXC->cb_add_hitloc(ac, prchit, nazwa, id, przym, hp);
}

/*
 * Function name: remove_hitloc
 * Description:   Removes a specific hit location
 * Arguments:     id: The hitloc id
 * Returns:       True if removed
 */
public int
remove_hitloc(int id)
{
    if (!QEXC)
        return 0;
    return (int)QEXC->cb_remove_hitloc(id);
}

/**
 * Funkcja m�wi�ca o tym, i� nie jest to humanoid.
 *
 * @return Zawsze 0.
 */
public int
is_humanoid()
{
    return 0;
}

/**
 * Funkcja m�wi�ca o tym, i� jest to zwierze.
 *
 * @return Zawsze 1.
 */
public int
is_zwierze()
{
    return 1;
}

/*
 * No, a tu wiadomo - reakcje na emotki
 */
void
emote_hook(string emote, object wykonujacy, string przyslowek)
{
    switch (emote)
    {
        case "kopnij":
            ploszymy_sie(wykonujacy);
           //set_alarm(0.5, 0.0, "command_gdy_jest", , wykonujacy);
            break;
        case "poca�uj":
        case "przytul":
        case "pog�aszcz":
        case "pog�askaj":

            if(wykonujacy->query_skill(SS_ANI_HANDL) <
              (40+random(15)+(wykonujacy->query_race()~="elf"?0:15)-oswojone*10))
            ploszymy_sie(wykonujacy);

            break;
    }
}








/*
 * Function name: attacked_by
 * Description:   Dzikie zwierz�ta generalnie nie walcz�, z
 *                pewnymi wyj�tkami - dlatego potrzebne s� tu du�e zmiany
 */
void
attacked_by(object wrog)
{
    ::attacked_by(wrog);

    //Wpierw sprawd�my, czy jaki� wyexpiony zwierz ma jakie� szanse
    // np. nied�wied�, w�� :P
    if(TO->query_average_stat() >= wrog->query_average_stat())
        return;

    if(!get_alarm(run_away_alarm) && !get_alarm(ploszenie_alarm))
    {
        //procentowy wykaz ilo�ci hp
        int hp_p = 100 * query_hp() / query_max_hp();

        switch(hp_p)
        {
            case 60..79:
                run_away_alarm = set_alarm(0.3,0.0,"run_away", 2);
                break;
            case 40..59:
                run_away_alarm = set_alarm(0.3,0.0,"run_away", random(3)+1);
                break;
            case 20..39:
                run_away_alarm = set_alarm(0.3,0.0,"run_away", random(3)+2);
                break;
            case 1..19:
                run_away_alarm = set_alarm(0.3,0.0,"run_away", random(4)+3);
                break;
            default:
                run_away_alarm = set_alarm(0.3,0.0,"run_away");
                break;
        }
    }
}

#if 0
//TA funkcja rozpieprza�a zwierzaki, przez to si� zap�tla�y
/**
 * Dzi�ki tej funkcji sp�oszony zwierzak nie powinien
 * nas zafludowa� uciekaniem, co si� zdarza�o, je�li
 * wywo�a�y si� np. 2 run_away z rz�du i drugi by� de facto
 * powrotem na lokacje gracza (i tak kilka razy...)
 */
public int
prevent_enter_env(object dest)
{
    object *gracze = FILTER_PLAYERS(all_inventory(dest));

    //Przy klonowaniu nie blokujemy... w koncu po to si� klonuje,
    //�eby si� obiekt tam znalaz�.
    if(calling_function(-2) == "clone" || calling_function(-3) == "odnow")
        return 0;

    //Je�li nie ma odpowiedniej opieki nad zwierz�tami to si�
    //zwierzak przestraszy i spieprzy

    if(sizeof(gracze))
    {
        foreach(object g : gracze)
        {
            if(g->query_prop(SS_ANI_HANDL) <
                (25 + TO->query_prop(SS_AWARENESS) / 2 + random(15)))
            {
                return 1;
            }
        }
    }

    return 0;
}
#endif

void
ploszymy_sie(object przez_kogo)
{
    if(TO->query_aggressive() || TO->query_attack())
        return;

    if(get_alarm(run_away_alarm) != 0)
        return;

    if(!objectp(przez_kogo))
        return;

    if(ENV(przez_kogo) != ENV(TO))
        return;

    if(random(2))
        command("emote p�oszy si�!");


    if(ENV(TO)->query_prop(ROOM_I_TYPE) & ROOM_NORMAL != ROOM_NORMAL &&
        ENV(TO)->query_prop(ROOM_I_TYPE) & ROOM_IN_CITY != ROOM_IN_CITY)
    {
        run_away(random(3)+1);
    }
    else
        run_away();
}

nomask void
signal_enter(object ob, object skad)
{
    //Je�li zwierze jest possesni�te to nie reaguje nijak
    if(interactive(TO))
        return;

    if(!sizeof(FILTER_IS_SEEN(ob,({TO}))) ||
        !sizeof(FILTER_CAN_SEE_IN_ROOM(({TO}))))
    {
        return;
    }

    //Je�li zwierzak oswojony to nie ucieknie
    if(oswojone > 10)
        return;

    //sprawd�my, czy jest to agresywne zwierz�. W takim przypadku mo�e
    //nawet zaatakowa�. <lol>
    if(TO->query_aggressive())
        return;

    //podczas walki uciekamy, gdy nas goni�.
    if(TO->query_enemy(0) && TO->query_attack() &&
        ENV(TO->query_enemy(0)) == ENV(TO) && !get_alarm(run_away_alarm) &&
        !get_alarm(run_away_alarm) && !get_alarm(ploszenie_alarm))
    {
        run_away_alarm = set_alarm(0.4,0.0, "run_away", random(5)+1);
    }

    //W przeciwnym wypadku...napi�tnowany biedny zwierzak umyka ;)
    //Osoba o niskiej charyzmie dla zwierzat - czyli o niskim umie
    //opieki nad zwierzakami - bedzie je ploszyc.
    if(ob->query_prop(SS_ANI_HANDL) <
        (25 + TO->query_prop(SS_AWARENESS) / 2 + random(15)) &&
        !get_alarm(run_away_alarm) && !get_alarm(ploszenie_alarm))
    {
        if(!TO->query_attack() && !ploszenie_alarm)
            ploszenie_alarm = set_alarm(itof(random(2))+0.5,0.0,"ploszymy_sie",ob);
    }

    return;
}

/**
 * Tutaj mo�emy ustawi� jak zwierze reaguje na krzyk.
 */
void
sygnal_krzyku(object ob, string str, object env=0)
{
    //Je�li wiz possessnie zwierze, to ono nie zareaguje nijak
    if(interactive(TO))
        return;

    if(!get_alarm(ploszenie_alarm) && !get_alarm(run_away_alarm))
        ploszenie_alarm = set_alarm(0.3, 0.0, &ploszymy_sie(ob));
}

/**
 * Ustawiamy czy zwierze jest osowojone a je�li tak to w jakim stopniu.
 * Od poziomu 10 zwierze przestaje ucieka� przed wszystkimi w innym
 * wypadku w przysz�o�ci b�dzie si� liczy� opieke nad zwierz�tami i
 * pewnie dodam uma tresura.
 *
 * @param o poziom oswojenia
 */
public void ustaw_stan_oswojenia(int o)
{
    oswojone = o;
}

/**
 * @return Zwraca czy zwierze jest oswojone czy nie.
 */
public int query_stan_oswojenia()
{
    return oswojone;
}

/**
 * Funkcja zwraca kierunki w jakich mo�e uciec zwierze, zale�nie od stanu oswojenia
 * itp, itd
 *
 * @param exits wszystkie dozwolone kierunki
 * @param last_lok ostatnia lokacja, na kt�rej zwierze si� znajdowa�o.
 *
 * @return zmodyfikowane exits
 */
public mixed * query_run_away_exits(mixed exits, object here)
{
    mixed new_exits = exits;

    int i;
    //Usuwamy pomieszczenia na kt�rych s� ludzie, chyba, �e zwierzak oswojony
    //przynajmniej na 5:)
    if(oswojone <= 5)
    {
        foreach(mixed x : new_exits)
        {
            i = member_array(x, new_exits);
            if(stringp(x) && ((i % 3) == 0))
            {
                if(sizeof(filter(FILTER_LIVE(all_inventory(find_object(x))), not @ &->is_zwierze())))
                {
                    if(i != 0)
                        new_exits = new_exits[0..i-1] + new_exits[i+3..];
                    else
                        new_exits = new_exits[i+3..];
                }
            }
        }
    }

    if(!sizeof(new_exits))
        new_exits = exits;

    //Usuwamy pomieszczenia innych typ�w, by np. nie ucieka� z lasu na trakt
    //No chyba, �e mamy oswojenie powy�ej 7 to wtedy zwierzak nie b�dzie si�
    //ba� uciec.
    if(oswojone <= 7)
    {
        foreach(mixed x : new_exits)
        {
            i = member_array(x, new_exits);
            if(stringp(x) && ((i % 3) == 0))
            {
                object next_loc = find_object(x);
                if(next_loc->query_prop(ROOM_I_TYPE) & here->query_prop(ROOM_I_TYPE) != here->query_prop(ROOM_I_TYPE) &&
                    next_loc->query_prop(ROOM_I_TYPE) & ROOM_FOREST != ROOM_FOREST &&
                    next_loc->query_prop(ROOM_I_TYPE) & ROOM_CAVE != ROOM_CAVE)
                { // Do lasu i jaskini zawsze uciekaj�
                    if(i != 0)
                        new_exits = new_exits[0..i-1] + new_exits[i+3..];
                    else
                        new_exits = new_exits[i+3..];
                }
            }
        }
    }

    //No chyba �e nic nam ju� innego nie zosta�o to uciekamy gdziekolwiek
    if(!sizeof(new_exits))
        new_exits = exits;

    return new_exits;
}

/**
 * @return M_out pokazywane przy uciekaniu.
 */
public string query_away_m_in()
{
    switch(random(6))
    {
        case 0:
            return "wbiega, dr�ac z przestrachu";
        case 1:
            return "przybywa, dr��c ze strachu";
        case 2:
            return "przybywa";
        case 3:
            return "wbiega";
        case 4:
            return "przybywa, dr��c z przera�enia";
        case 5:
            return "wbiega, dr��c z przera�enia";
    }
}

/**
 * @return M_out pokazywane przy ucieczce.
 */
public string query_away_m_out()
{
    switch(random(6))
    {
        case 0:
            return "ucieka w pop�ochu";
        case 1:
            return "ucieka w przera�eniu";
        case 2:
            return "umyka w pop�ochu";
        case 3:
            return "umyka w przera�eniu";
        case 4:
            return "wybiega w pop�ochu";
        case 5:
            return "wybiega w przera�eniu";
    }
}
