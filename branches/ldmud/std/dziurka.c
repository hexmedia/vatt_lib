/*
 * Przerobiony i udoskonalony plik dziurek na kolczyki.
 * 					 Lil. 21.08.2005
 *
 * Zarastanie dziurek doda� Molder dnia 22.08.2005.
 * 
 * Plik:	dziurka.c
 * Autor:	Cerdin
 * Data:	27.12.03 (srodek nocy... cos kolo 12 w poludnie... ale mnie zdarlo)
 * Opis:	Obiekt obslugujacy kolczyki, dodajacy sloty na nie,
 *		i obsluge emocji do kolczykow.
 */

#pragma strict_types

inherit "/std/object";
inherit "/cmd/std/command_driver";

#include <cmdparse.h>
#include <stdproperties.h>
#include <macros.h>
#include <pl.h>
#include <tasks.h>
#include <ss_types.h>
#include <wa_types.h>
#include <composite.h>
#include <filter_funs.h>
#include <language.h>

#define SUB	"_subloc_kolczyka"
#define KOLCZYK	"_kolczyk_i_umieszczony_gdzie"
#define SPRAWDZ_KOLCZYK	if (!cele[0]->query_kolczyk())\
			{\
			notify_fail("Ale� "+cele[0]->short(this_player(),PL_MIA)+\
			" nie jest kolczykiem!\n");\
			}\
			if (!(nr = cele[0]->query_prop(KOLCZYK)))\
			{\
			notify_fail("Ale� "+cele[0]->short(this_player(),PL_MIA)+\
			" nie jest nigdzie umieszczony.\n");\
			return 0;\
			}\

// czas po jakim nieuzywana dziurka zanika. w sekundach
#define CZAS_ZROSNIECIA_UCHA 3456000
#define CZAS_ZROSNIECIA_NOSA 129600
#define CZAS_ZROSNIECIA_BRWI 259200
#define CZAS_ZROSNIECIA_PODBRODKA 86400
#define CZAS_ZROSNIECIA_JEZYKA 300

static private mixed *kolczyki = ({0,0,0,0,0,0});

static private int *dziurka_max = ({10, // Lewe ucho
				    10, // Prawe ucho
        			    3, // Nos
				    2, // Lewa brew
				    2, // Prawa brew
				    1, // Broda
				  });

// mozna zrobic tablice tablic ... jak sie komus chce to niech robi :P (Molder)
static private int *czasy_l_ucho = ({0,0,0,0,0,0,0,0,0,0});
static private int *czasy_p_ucho = ({0,0,0,0,0,0,0,0,0,0});
static private int *czasy_nos = ({0,0,0});
static private int *czasy_l_brew = ({0,0});
static private int *czasy_p_brew = ({0,0});
static private int *czasy_podbrodek = ({0});

static private int *dziurka = ({0,0,0,0,0,0});

static private string *opis_mie = ({"lewym uchu",
				"prawym uchu",
				"nosie",
				"lewej brwi",
				"prawej brwi",
				"brodzie"				
				});
static private string *opis_bie = ({"lewe ucho",
				"prawe ucho",
				"nos",
				"lew� brew",
				"praw� brew",
				"brod�"
				});

static private int Size = sizeof(dziurka);
static private int kolczyk = 0;

void zarastanie();
void resetuj_czasy();

void
create_object()
{
    ustaw_nazwe("dziurka");

    remove_prop(OBJ_I_VALUE);
    remove_prop(OBJ_I_VOLUME);
    remove_prop(OBJ_I_WEIGHT);

    add_name("_dziurka_od_kolczykow");
    set_no_show();

    add_prop(OBJ_M_NO_DROP, 1);
    add_prop(OBJ_M_NO_STEAL,1);
    add_prop(OBJ_M_NO_SELL, 1);
    add_prop(OBJ_M_NO_GIVE, 1);
    
    set_alarm(5.0, 0.0, "zarastanie");
}

void
zarastanie()
{
// jak ktos wie to niech powie czemu "int time = time()" podaje blad "Call of non-function: "int " - Molder. 
    int czas = time();
    int time = czas;
    int i, pom;
    int j = 0;
    int *efekty = ({0,0,0,0,0,0});
    int p = 0;
    string *str = ({"","","","",""});

    for (i = 0, pom = dziurka[0]; i < pom; i++)
    {
	    if (czasy_l_ucho[i] != 0)
	        if (time - czasy_l_ucho[i] > CZAS_ZROSNIECIA_UCHA)
	        {
	            dziurka[0]--;
	            efekty[0]++;
	            czasy_l_ucho[i]=0;
	        }
    }

    for (i = 0, pom = dziurka[1]; i < pom; i++)
    {
	    if (czasy_p_ucho[i] != 0)
	        if(time - czasy_p_ucho[i] > CZAS_ZROSNIECIA_UCHA)
	        {
	            dziurka[1]--;
	            efekty[1]++;
	            czasy_p_ucho[i]=0;
	        }
    }

    for(i=0, pom=dziurka[2]; i < pom; i++)
    {
	    if(czasy_nos[i] != 0)
	        if(time - czasy_nos[i] > CZAS_ZROSNIECIA_NOSA)
	        {
	            dziurka[2]--;
	            efekty[2]++;
	            czasy_nos[i]=0;
	        }
    }

    for(i=0, pom=dziurka[3]; i < pom; i++)
    {
	    if(czasy_l_brew[i] != 0)
	        if(time - czasy_l_brew[i] > CZAS_ZROSNIECIA_BRWI)
	        {
	            dziurka[3]--;
	            efekty[3]++;
	            czasy_l_brew[i]=0;
	        }
    }

    for(i=0, pom=dziurka[4]; i < pom; i++)
    {
	    if(czasy_p_brew[i]!= 0)
	        if(time - czasy_p_brew[i] > CZAS_ZROSNIECIA_BRWI)
	        {
	            dziurka[4]--;
    	        efekty[4]++;
    	        czasy_p_brew[i]=0;
	        }
    }

    if(dziurka[5] == 1 && czasy_podbrodek[0] != 0)
	    if(time - czasy_podbrodek[0] > CZAS_ZROSNIECIA_PODBRODKA)	
        {
	        dziurka[5]--;
	        efekty[5]++;
	        czasy_podbrodek[0]=0;
        }

    for(i=0;i<Size;i++)
	if(efekty[i]!=0)
	{
  	    if(p==0)
	    {	    
    		write("Masz wra�enie, ze znikn�");
		p=1;
	    
	    	if(efekty[i]==1)
	        {
	        if(i==3 || i==4)
	    	    write("a dziurka w twojej " + opis_mie[i]);
		else
		    write("a dziurka w twoim " + opis_mie[i]);
	        }
  	        else	
  	        {
		if(i==3 || i==4)
	    	    write("y dziurki w twojej " + opis_mie[i]);
		else
		    write("y dziurki w twoim " + opis_mie[i]);
	        }		
	    }
	    else
 	    {
		if(efekty[i]==1)
	        {
	        if(i==3 || i==4)
	    	    str[j++] = " dziurka w twojej " + opis_mie[i];
		else
		    str[j++] = " dziurka w twoim " + opis_mie[i];
	        }
  	        else	
  	        {
		if(i==3 || i==4)
	    	    str[j++] = " dziurki w twojej " + opis_mie[i];
		else
		    str[j++] = " dziurki w twoim " + opis_mie[i];
	        }		
	    }
	    
	}

    if(p==1)
    {
	for(i=0;i<j-1;i++)
	    write("," + str[i]);
	if(i==j-1)
	    write(" oraz" + str[i]);
	
	write(".\n");

	for(i=0; i<Size && dziurka[i]==0;i++) ;
	if(i==Size)
	    remove_object();
    }

    while(++i < dziurka[0]) if(czasy_l_ucho[i]==0) czasy_l_ucho[i] = time;
    i=-1;

    while(++i < dziurka[1]) if(czasy_p_ucho[i]==0) czasy_p_ucho[i] = time;

    i=-1;
    while(++i < dziurka[2]) if(czasy_nos[i]==0) czasy_nos[i] = time;

    i=-1;
    while(++i < dziurka[3]) if(czasy_l_brew[i]==0) czasy_l_brew[i] = time;

    i=-1;
    while(++i < dziurka[4]) if(czasy_p_brew[i]==0) czasy_p_brew[i] = time;

    if(dziurka[5]==1) if(czasy_podbrodek[0]==0)	czasy_podbrodek[0] = time;
}


int
query_dziurka()
{
return 1;
}

/* Funkcja:	Sprawdz dziurke
 * Opis:	Podaje czy mozliwe jest stworzenie kolejnej 
 *		dziurki na podanej lokacji
 * Argumenty:   string lokacja - nazwa w bierniku na ktorej chcemy 
 *		stworzyc dziurke.
 * Zwraca:	int, 1 jesli mozna stworzyc tam kolejna dziurke
 *		0 - jesli nie.
 *		-1 - Jesli nie ma takiej lokacji do przebicia
 */
int
sprawdz_dziurke(string lokacja)
{
int nr;

if ((nr = member_array(lokacja, opis_bie)) == -1) return -1;

if (dziurka_max[nr] == dziurka[nr]) return 0;

return 1;

}

/* Funkcja:	dodaj_dziurke
 * Opis:	Tworzy dziurke kolejna na podanej lokacji
 * Argumenty:   string lokacja - nazwa w bierniku na ktorej chcemy 
 *		stworzyc dziurke.
 * Zwraca:	int, 1 jesli prawidlowo stworzona
 *		0 - jesli nie.
 *		-1 - Jesli nie ma takiej lokacji do przebicia
 */

int
dodaj_dziurke(string lokacja)
{
int nr;

if ((nr = member_array(lokacja, opis_bie)) == -1) return -1;

if (dziurka_max[nr] == dziurka[nr]) return 0;

switch(nr)
{
    case 0:
	czasy_l_ucho[dziurka[nr]]= time();
	break;
    case 1:
	czasy_p_ucho[dziurka[nr]]= time();	
	break;
    case 2:
	czasy_nos[dziurka[nr]]= time();	
	break;
    case 3:
	czasy_l_brew[dziurka[nr]]= time();	
	break;
    case 4:
	czasy_p_brew[dziurka[nr]]= time();	
	break;
    case 5:
	czasy_podbrodek[dziurka[nr]]= time();	
}

dziurka[nr]++;
return 1;
}

void
dump_czasy_dziurek()
{
    dump_array(czasy_l_ucho);
    dump_array(czasy_p_ucho);
    dump_array(czasy_nos);
    dump_array(czasy_l_brew);
    dump_array(czasy_p_brew);
    dump_array(czasy_podbrodek);
}

int *
query_dziurki()
{
return dziurka + ({});
}

int
koumiesc(string str)
{
string *pomoc;
object *cele;
int nr;
int nr2;

notify_fail("Umie�� co gdzie?\n");

if (!str || sizeof(pomoc = explode(str," w ")) != 2) return 0;

cele = parse_this(pomoc[0], "%i:" + PL_MIA);
if (!sizeof(cele)) return 0;
if (sizeof(cele) != 1)
    {
    notify_fail("Mo�e lepiej spr�buj umieszcza� je pojedynczo.\n");
    return 0;
    }
if ((nr = member_array(pomoc[1], opis_mie)) == -1) return 0;
if (dziurka[nr] == 0) return 0;
if (!cele[0]->query_kolczyk()) return 0;
if (nr2 = cele[0]->query_prop(KOLCZYK))
    {
    notify_fail("Ale� "+cele[0]->short(this_player(),PL_MIA)+" jest ju� "+
	"umieszczony w "+opis_mie[nr2-1]+"!\n");
    return 0;
    }

if (dziurka[nr] == sizeof(kolczyki[nr]))
    {
    notify_fail("W "+pomoc[1]+" nie ma ju� wolnego miejsca, by� m"+
		    this_player()->koncowka("�g�","og�a")+
	" umie�ci� tam "+cele[0]->short(this_player(),PL_BIE)+".\n");
    return 0;
    }

switch(nr)
{
    case 0:
	czasy_l_ucho[sizeof(kolczyki[nr])]= 0;
	break;
    case 1:
	czasy_p_ucho[sizeof(kolczyki[nr])]= 0;	
	break;
    case 2:
	czasy_nos[sizeof(kolczyki[nr])]= 0;	
	break;
    case 3:
	czasy_l_brew[sizeof(kolczyki[nr])]= 0;	
	break;
    case 4:
	czasy_p_brew[sizeof(kolczyki[nr])]= 0;	
	break;
    case 5:
	czasy_podbrodek[sizeof(kolczyki[nr])]= 0;	
}


write("Umieszczasz "+cele[0]->short(this_player(),PL_BIE)+" w "+pomoc[1]+".\n");
saybb(QCIMIE(this_player(),PL_MIA)+" umieszcza "+cele[0]->short(this_player(),PL_BIE)
	+" w "+pomoc[1]+".\n");
cele[0]->add_prop(OBJ_I_DONT_INV,1);


cele[0]->add_prop(KOLCZYK,nr+1);
cele[0]->add_prop(OBJ_I_NO_DROP,"Najpierw musisz zdj�� "+
			cele[0]->short(this_player(),PL_BIE)+"!\n");
cele[0]->add_prop(OBJ_I_NO_GIVE,"Najpierw musisz zdj�� "+
			cele[0]->short(this_player(),PL_BIE)+"!\n");
cele[0]->add_prop(OBJ_M_NO_SELL,"Najpierw musisz zdj�� "+
			cele[0]->short(this_player(),PL_BIE)+"!\n");

kolczyk++;

if (!sizeof(kolczyki[nr]))
   kolczyki[nr] = ({ cele[0] });
else
    kolczyki[nr] += ({ cele[0] });

return 1;

}

void 
resetuj_czasy()
{

int i=-1;

while(++i < dziurka[0])
    czasy_l_ucho[i] = time();
i=-1;
while(++i < dziurka[1])
    czasy_p_ucho[i] = time();
i=-1;
while(++i < dziurka[2])
    czasy_nos[0] = time();
i=-1;
while(++i < dziurka[3])
    czasy_l_brew[i] = time();
i=-1;
while(++i < dziurka[4])
    czasy_p_brew[i] = time();
if(dziurka[5]==1)
czasy_podbrodek[0] = time();
 
}

int
zdejmij_wszystkie()
{
object *kol=({});
int i = -1;

if (!kolczyk)
    {
    notify_fail("Nie masz na sobie �adnych kolczyk�w.\n");
    return 0;
    }

    while (++i < Size)
	{
	if (!sizeof(kolczyki[i]))
	    continue;
	kol += kolczyki[i];
	kolczyki[i] = 0;
	}

kolczyk = 0;

write("Zdejmujesz z siebie "+COMPOSITE_DEAD(kol,PL_BIE)+".\n");
saybb(QCIMIE(this_player(),PL_MIA)+" zdejmuje "+COMPOSITE_DEAD(kol,PL_BIE)+".\n");

kol->remove_prop(OBJ_I_DONT_INV);
kol->remove_prop(KOLCZYK);
kol->remove_prop(OBJ_I_NO_DROP);
kol->remove_prop(OBJ_I_NO_GIVE);
kol->remove_prop(OBJ_M_NO_SELL);

resetuj_czasy();

return 1;
}


int
kozdejmij(string str)
{
object *cele;
int nr;

notify_fail("Zdejmij co?\n");
if (!str) return 0;

if (str == "wszystkie kolczyki" || str == "kolczyki") return zdejmij_wszystkie();

if (!kolczyk) 
    {
    notify_fail("Nie masz na sobie �adnych kolczyk�w.\n");
    return 0;
    }

cele = parse_this(str, "%i:" + PL_MIA);

if (!sizeof(cele))
    return 0;

if (sizeof(cele) != 1)
    {
    notify_fail("Lepiej zdejmij pojedy�czo, lub zdejmij kolczyki.\n");
    return 0;
    }

SPRAWDZ_KOLCZYK

write("Zdejmujesz "+cele[0]->short(this_player(),PL_BIE)+".\n");
saybb(QCIMIE(this_player(),PL_MIA)+" zdejmuje "+cele[0]->short(this_player(),PL_BIE)+".\n");
cele[0]->remove_prop(OBJ_I_DONT_INV);
cele[0]->remove_prop(KOLCZYK);
cele[0]->remove_prop(OBJ_I_NO_DROP);
cele[0]->remove_prop(OBJ_I_NO_GIVE);
cele[0]->remove_prop(OBJ_M_NO_SELL);


switch(nr-1)
{
    case 0:
	czasy_l_ucho[sizeof(kolczyki[nr-1])-1]= time();
	break;
    case 1:
	czasy_p_ucho[sizeof(kolczyki[nr-1])-1]= time();	
	break;
    case 2:
	czasy_nos[sizeof(kolczyki[nr-1])-1]= time();	
	break;
    case 3:
	czasy_l_brew[sizeof(kolczyki[nr-1])-1]= time();	
	break;
    case 4:
	czasy_p_brew[sizeof(kolczyki[nr-1])-1]= time();	
	break;
    case 5:
	czasy_podbrodek[sizeof(kolczyki[nr-1])-1]= time();	
}

kolczyk--;

kolczyki[nr-1] -= ({cele[0]});

return 1;

}

int
kobaw(string str)
{
object *cele;
int nr;

notify_fail("Baw si� czym?\n");
if (!str) return 0;

if (!kolczyk) 
    {
    notify_fail("Nie masz na sobie �adnych kolczyk�w.\n");
    return 0;
    }

cele = parse_this(str, "[si�] %i:" + PL_NAR);

if (!sizeof(cele))
    return 0;

if (sizeof(cele) != 1) 
    {
    notify_fail("Mo�e lepiej spr�buj bawi� si� kolczykami pojedy�czo.\n");
    return 0;
    }

SPRAWDZ_KOLCZYK

write("Bawisz si� swoim "+cele[0]->short(this_player(),PL_NAR)+" umieszczonym"+
	" w "+opis_mie[nr-1]+".\n");
saybb(QCIMIE(this_player(),PL_MIA)+" bawi si� swoim "
+cele[0]->short(this_player(),PL_NAR)+" umieszczonym"+
" w "+opis_mie[nr-1]+".\n");
return 1;
}

int
kopociagnij(string str)
{
object *cele;
int nr;

notify_fail("Poci�gnij za co?\n");
if (!str) return 0;

if (!kolczyk) 
    {
    notify_fail("Nie masz na sobie �adnych kolczyk�w.\n");
    return 0;
    }

cele = parse_this(str, "[za] %i:" + PL_BIE);

if (!sizeof(cele))
    return 0;

if (sizeof(cele) != 1) 
    {
    notify_fail("Niebardzo wiesz jak si� do tego zabra�.\n");
    return 0;
    }

SPRAWDZ_KOLCZYK

write("Ci�gniesz delikatnie za umieszczony w "+opis_mie[nr-1]+
" "+cele[0]->short(this_player(),PL_MIA)+", sprawdzaj�c czy na pewno "+
"nie wypadnie.\n");
saybb(QCIMIE(this_player(),PL_MIA)+" ci�gnie delikatnie za umieszczony w "
+opis_mie[nr-1]+" "+cele[0]->short(this_player(),PL_MIA)+", jakby chcia�"+
this_player()->koncowka("","a")+" sprawdzi� "+cele[0]->koncowka("jego","jej","jego")+
" umocowanie i zyska� pewno��, �e nie wypadnie przy jakim� gwa�townym ruchu.\n");
return 1;

}

int
kopoleruj(string str)
{
    object *cele;
    int nr;

    notify_fail("Poleruj co?\n");

    if(!str)
        return 0;

    if (!kolczyk) 
    {
        notify_fail("Nie masz na sobie �adnych kolczyk�w.\n");
        return 0;
    }

    cele = parse_this(str, "%i:" + PL_BIE);

    if (!sizeof(cele))
        return 0;

    if (sizeof(cele) != 1) 
    {
        notify_fail("Niebardzo wiesz jak si� do tego zabra�.\n");
        return 0;
    }

    SPRAWDZ_KOLCZYK

    write("Kilkoma szybkimi ruchami polerujesz "+
    cele[0]->short(this_player(),PL_BIE)+".\n");

    saybb(QCIMIE(this_player(),PL_MIA)+" kilkoma szybkimi ruchami poleruje "
    +cele[0]->short(this_player(),PL_MIA)+", sprawiaj�c, �e bi�uteria "+
    "nabiera nowego po�ysku.\n");

    return 1;
}

int
kowyczysc(string str)
{
object *cele;
int nr;

notify_fail("Wyczy�� co?\n");
if (!str) return 0;

if (!kolczyk) 
    {
    notify_fail("Nie masz na sobie �adnych kolczyk�w.\n");
    return 0;
    }

cele = parse_this(str, "%i:" + PL_BIE);

if (!sizeof(cele))
    return 0;

if (sizeof(cele) != 1) 
    {
    notify_fail("Niebardzo wiesz jak si� do tego zabra�.\n");
    return 0;
    }

SPRAWDZ_KOLCZYK

write("Zdejmujesz na chwil� umieszczony w "+opis_mie[nr-1]+" "+
cele[0]->short(this_player(),PL_BIE)+", by oczy�ci� go dok�adnie"+
".\n");
saybb(QCIMIE(this_player(),PL_MIA)+" zdejmuje na chwil� umieszczony w "+opis_mie[nr-1]+" "
+cele[0]->short(this_player(),PL_MIA)+", by oczy�ci� go dok�adnie.\n");
return 1;

}


/* Glupie to :P
int
kozaslon(string str)
{
object *cele;
int nr;

notify_fail("Kozas�o� co?\n");
if (!str) return 0;

if (!kolczyk) 
    {
    notify_fail("Nie masz na sobie �adnych kolczyk�w.\n");
    return 0;
    }

cele = parse_this(str, "%i:" + PL_BIE);

if (!sizeof(cele))
    return 0;

if (sizeof(cele) != 1) 
    {
    notify_fail("Niebardzo wiesz jak si� do tego zabra�.\n");
    return 0;
    }

SPRAWDZ_KOLCZYK

write("Naglym gestem przykladasz dlon do umieszczonego w "+opis_mie[nr-1]+" "+
cele[0]->short(this_player(),PL_BIE)+", probojac go zaslonic przed czyimkolwiek "+
"spojrzeniem.\n");
saybb(QCIMIE(this_player(),PL_DOP)+" naglym gestem przyklada dlon do "+
"umieszczonego w "+opis_mie[nr-1]+" "
+cele[0]->short(this_player(),PL_DOP)+", bezkutecznie probojac go ukryc przed "+
"twym wzrokiem.\n");
return 1;

}*/
/*
int
kopodrap(string str)
{
int nr;

if (!str || sscanf(str,"si� w %s",str) != 1 || (nr = member_array(str,opis_bie)) == -1 )
    {
    notify_fail("Podrap si� gdzie?\n");
    return 0;
    }

if (sizeof(kolczyki[nr]))
    str += ", mimowolnie poprawiaj�c umieszczony tam "+
    kolczyki[nr][0]->short(this_player(),PL_BIE);

write("Drapiesz si� w "+str+".\n");
saybb(QCIMIE(this_player(),PL_MIA)+" drapie si� w "+str+".\n");
return 1;
}*/

int
kopopraw(string str)
{
object *cele;
int nr;

notify_fail("Popraw co?\n");
if (!str) return 0;

if (!kolczyk) 
    {
    notify_fail("Nie masz na sobie �adnych kolczyk�w.\n");
    return 0;
    }

cele = parse_this(str, "%i:" + PL_BIE);

if (!sizeof(cele))
    return 0;

if (sizeof(cele) != 1) 
    {
    notify_fail("Niebardzo wiesz jak si� do tego zabra�.\n");
    return 0;
    }

SPRAWDZ_KOLCZYK

write("Poprawiasz "+
cele[0]->short(this_player(),PL_BIE)+", delikatnie zmieniaj�c jego po�o�enie.\n");
saybb(QCIMIE(this_player(),PL_DOP)+" poprawia "
+cele[0]->short(this_player(),PL_BIE)+", delikatnie zmieniaj�c jego po�o�enie.\n");
return 1;

}


int
kopochwal(string str)
{
object *cele;
int nr;

notify_fail("Pochwal si� czym?\n");
if (!str) return 0;

if (!kolczyk) 
    {
    notify_fail("Nie masz na sobie �adnych kolczyk�w.\n");
    return 0;
    }

cele = parse_this(str, "[si�] %i:" + PL_NAR);

if (!sizeof(cele))
    return 0;

if (sizeof(cele) != 1) 
    {
    notify_fail("Niebardzo wiesz jak si� do tego zabra�.\n");
    return 0;
    }

SPRAWDZ_KOLCZYK

write("Z nie�mia�ym u�miechem na twarzy pocierasz umieszczony w "+opis_mie[nr-1]+" "+
cele[0]->short(this_player(),PL_MIA)+", pr�buj�c zwr�ci� na niego uwag� otoczenia.\n");
saybb(QCIMIE(this_player(),PL_DOP)+" pociera "+
"umieszczony w "+opis_mie[nr-1]+" "
+cele[0]->short(this_player(),PL_MIA)+", staraj�c si� zwr�ci� na niego "+
      "uwag� otoczenia.\n");
return 1;

}

string *
query_opis_dziur(int i)
{
string str;
if (!dziurka[i]) return ({});
str = LANG_SNUM(dziurka[i], PL_BIE, PL_ZENSKI)+" ";
switch(dziurka[i])
    {
    case 1:     str += "dziurk�"; break;
    case 2..4:  str += "dziurki"; break;
    case 5..20: str += "dziurek"; break;
    }
str += " w "+opis_mie[i];

return ({ str });
}

int
kosprawdz(string str)
{
    int i = -1;
    string *tab = ({});

    if (!str || !(str == "dziurki" || str ~= "dziurk�" || str ~= "przek�ute miejsca"))
    {
	notify_fail("Sprawd� co?\n");
	return 0;
    }

    while (++i < Size) tab += query_opis_dziur(i);

    if (sizeof(tab)>0)
	write("Masz "+COMPOSITE_WORDS(tab)+".\n");
    else
	write("Nie masz �adnych przek�utych miejsc.\n");
    return 1;
}


void
init()
{
    ::init();
    add_action("koumiesc","umie��");
    add_action("kozdejmij","zdejmij");
    add_action("kobaw","baw");
    add_action("kobaw","pobaw");
    add_action("kopociagnij","poci�gnij");
    add_action("kopoleruj","poleruj");
    add_action("kopoleruj","wypoleruj");
    add_action("kowyczysc","wyczy��");
    add_action("kopopraw","popraw");
    add_action("kopochwal","pochwal");
    add_action("kosprawdz","sprawd�");
//    add_action("pomoc", "?", 2); <-- To jest w /std/kolczyk.

}

void
enter_env(object dest, object old)
{
    ::enter_env(dest,old);
    if (!living(dest)) return;
    dest->add_subloc(SUB,this_object());
    //test
    obj_subloc = SUB;
}

void
leave_env(object from, object to)
{
    ::leave_env(from, to);
    if (!living(from)) return;
    from->remove_subloc(SUB);
    //test
    obj_subloc = 0;
}

string
show_subloc(string subloc, object on_obj, object for_obj)
{
string str;
string allsub="";
int i = -1;

   if ((subloc != SUB) || on_obj->query_prop(TEMP_SUBLOC_SHOW_ONLY_THINGS)
	|| !kolczyk)
      return "";

if (on_obj == for_obj) 
    str = " masz ";
else
    str = " ma ";

set_this_player(for_obj);

while (++i < Size)
    {
       
	    
    
	    
    if (!sizeof(kolczyki[i]))
	continue;
    allsub += "W "+opis_mie[i]+str+COMPOSITE_DEAD(kolczyki[i],PL_BIE)+". ";
    }
allsub +="\n";
return allsub;

}

/**
 * Inicjalizuje odzyskane zmienne dziurki.
 *
 * @param arg - odczytany string pasuj�cy do tego zwracanego przer
 * <b>query_dziurka_auto_load()</b>
 *
 * @return pozosta�a cz�� napisu.
 */
string
init_dziurka_arg(string arg)
{
    string foobar, dziurka_arg, rest;

    if (arg == 0) {
        return 0;
    }

    sscanf(arg, "%s#DZIURKA#%s#DZIURKA#%s", foobar, dziurka_arg, rest);
    string *pomoc = explode(dziurka_arg,"##");
    int i = -1;

    if (sizeof(pomoc) != Size +28) {
        return arg;
    }

    while (++i < Size)
        dziurka[i] = atoi(pomoc[i]);
    i--;
    while (++i < Size + 10)
        czasy_l_ucho[i-Size] = atoi(pomoc[i]);
    i--;
    while (++i < Size + 20)
        czasy_p_ucho[i-Size-10] = atoi(pomoc[i]);
    i--;
    while (++i < Size + 23)
        czasy_nos[i-Size-20] = atoi(pomoc[i]);
    i--;
    while (++i < Size + 25)
        czasy_l_brew[i-Size-23] = atoi(pomoc[i]);
    i--;
    while (++i < Size + 27)
        czasy_p_brew[i-Size-25] = atoi(pomoc[i]);
    czasy_podbrodek[0] = atoi(pomoc[Size + 27]);

    return rest;
}

public string
init_arg(string arg)
{
    return init_dziurka_arg(::init_arg(arg));
}

/**
 * Zwraca stringa dla odzyskania podstawowych zmiennych dziurki.
 *
 * @return cz�� stringa do zapisu.
 */
string
query_dziurka_auto_load()
{
    string str = "#DZIURKA#";
    int i = -1;

    while (++i < Size)
        str += dziurka[i]+"##";
    i = -1;
    while (++i < 10)
        str += czasy_l_ucho[i]+"##";
    i = -1;
    while (++i < 10)
        str += czasy_p_ucho[i]+"##";
    i = -1;
    while (++i < 3)
        str += czasy_nos[i]+"##";
    i = -1;
    while (++i < 2)
        str += czasy_l_brew[i]+"##";
    i = -1;
    while (++i < 2)
        str += czasy_p_brew[i]+"##";

    str += czasy_podbrodek[0]+"##";

    return str + "#DZIURKA#";
}

public string
query_auto_load()
{
    return ::query_auto_load() + query_dziurka_auto_load();
}
