/**
 * \file /std/room/description.c
 *
 * To jest cz�� pliku /std/room.c
 *
 * In this module you will find the things relevant to the description of the
 * room.
 *
 */

#include <exp.h>
#include <math.h>
#include <login.h>
#include <colors.h>
#include <mudtime.h>
#include <language.h>
#include <ss_types.h>
#include <composite.h>
#include <stdproperties.h>

#define EXITS_COLOR COLOR_FG_YELLOW

static  mixed   room_descs;         /* Extra longs added to the rooms own */
static  int gDefaultExitDesc = 0;

#ifdef DBG
#undef DBG
#endif DBG
#define DBG(x) find_player("krun")->catch_msg("B��D W ROOMIE(descr)" + x + "\n")

/*
 * Function name: 	add_my_desc
 * Description:   	Add a description printed after the normal
 *                      longdescription.
 * Arguments:	  	str: Description as a string
 *			cobj: Object responsible for the description
 *			      Default: previous_object()
 */
public void
add_my_desc(string str, object cobj = previous_object())
{
    if (query_prop(ROOM_I_NO_EXTRA_DESC))
        return;

    if (!room_descs)
        room_descs = ({ cobj, str });
    else
        room_descs = room_descs + ({ cobj, str });
}

/*
 * Function name:       change_my_desc
 * Description:         Change a description printed after the normal
 *                      longdescription. NOTE: if an object has more than
 *                      one extra description only one will change.
 * Arguments:           str: New description as a string
 *                      cobj: Object responsible for the description
 *                            Default: previous_object()
 */
public void
change_my_desc(string str, object cobj = previous_object())
{
    int i;
    mixed tmp_descs;

    if (query_prop(ROOM_I_NO_EXTRA_DESC))
        return;

    if (!cobj)
        return;

    i = member_array(cobj, room_descs);

    if (i < 0)
        add_my_desc(str, cobj);
    else
        room_descs[i + 1] = str;
}

/*
 * Function name: 	remove_my_desc
 * Description:   	Removes an earlier added  description printed after
 *                      the normal longdescription.
 * Arguments:	  	cobj: Object responsible for the description
 *			      Default: previous_object()
 */
public void
remove_my_desc(object cobj = previous_object())
{
    int i, sf;

    if (query_prop(ROOM_I_NO_EXTRA_DESC))
        return;

    sf = objectp(cobj);

    i = member_array(cobj, room_descs);
    while (i >= 0)
    {
        if (sf)
            room_descs = exclude_array(room_descs, i, i + 1);
        else
            room_descs = exclude_array(room_descs, i - 1, i);
        i = member_array(cobj, room_descs);
    }
}

/*
 * Function name: 	query_desc
 * Description:   	Gives a list of all added descriptions to this room
 * Returns:       	Array on the form:
 *			    desc1, obj1, desc2, obj2, ..... descN, objN
 */
public mixed
query_desc()
{
    return slice_array(room_descs, 0, sizeof(room_descs));
}

private string
add_colors(string exit)
{
    return set_color(EXITS_COLOR) + exit + clear_color();
}

private static int
filter_open_door(object ob)
{
    if(ob->is_door() && ob->query_open())
        return 1;
}

private static string
map_open_door(string *str)
{
     return explode(str[0] + " ", " ")[0];
}

/*
 * Function name: exits_description
 * Description:   This function will return the obvious exits described
 *		  in a njice way.
 * Returns:	  The obvious exits
 */
public string
exits_description()
{
    string *exits;
    int size;

    exits = query_obvious_exits();
    exits += map(filter(all_inventory(TO), &filter_open_door())->query_pass_command(), &map_open_door());

    size = sizeof(exits);

    if(size == 0)
        return "";

    exits = map(exits, &add_colors());

    gDefaultExitDesc = 1;

    switch(ilosc(size, 1, 2, 3))
    {
        case 1:
            return "Jest tutaj jedno widoczne wyj^scie: " + exits[0] + ".\n";
        case 2:
            return "S^a tutaj " + LANG_SNUM(size, PL_MIA, PL_NIJAKI_NOS)
                + " widoczne wyj^scia: " + COMPOSITE_WORDS(exits) + ".\n";
        case 3:
            return "Jest tutaj " + LANG_SNUM(size, PL_MIA, PL_NIJAKI_NOS)
                 + " widocznych wyj^s^c: " + COMPOSITE_WORDS(exits) + ".\n";
    }
}

/*
 * Function name: long
 * Description:   Describe the room and possibly the exits
 * Arguments:	  str: name of item or 0
 * Returns:       A string holding the long description of the room.
 */
varargs public mixed
long(string str)
{
    int index;
    int size;
    mixed lg;
    mixed herbs_obs;
    string ex, ziola;

    lg = ::long(str);
    if (stringp(str))
        return lg;
    if (!stringp(lg))
        lg = "";

    /* This check is to remove extra descriptions that have been added by
     * an object that is now destructed.
     */
    while ((index = member_array(0, room_descs)) >= 0)
        room_descs = exclude_array(room_descs, index, index + 1);

    if (pointerp(room_descs))
    {
        index = -1;
        size = sizeof(room_descs);
        while((index += 2) < size)
        {
            lg = lg + process_tags(check_call(room_descs[index]));
        }
    }

    if (room_no_obvious)
        return lg;

    herbs_obs = TP->query_prop(LIVE_I_FOUND_HERBS);
    if(mappingp(herbs_obs))
        if(member_array(MASTER, m_indexes(herbs_obs)))
            herbs_obs = herbs_obs[MASTER];
        else
            herbs_obs = 0;
    else
        herbs_obs = 0;

    //FIXME: Dopisa� zapominanie znalezionych zi�.
    TO->zapomnij_ziola(TP->query_real_name());

    ex = exits_description();
    ziola = TO->opisz_ziola(TO->query_znalezione(TP->query_real_name()), 1);

    return lg + (ziola ? ziola : "") + (gDefaultExitDesc ? ex :
        set_color(EXITS_COLOR) + ex + clear_color());
}

// dopisuje z przodu "na" jesli kierunek jest standardowy, typu "polnoc"
string
modify_dir(string dir)
{
    switch(dir)
    {
        case "p^o^lnoc":
        case "p^o^lnocny-wsch^od":
        case "wsch^od":
        case "po^ludniowy-wsch^od":
        case "po^ludnie":
        case "po^ludniowy-zach^od":
        case "zach^od":
        case "p^o^lnocny-zach^od":
        case "g^ore":
        case "d^o^l":
            return "na " + dir;
        case "g^ora":
            return "na g^or^e";
        default:
            return dir;
    }
}

/**
 * Definiuje komunikat dotycz�cy godziny dla komendy czas.
 * Funkcja ta mo�e, a nawet powinna by� maskowana stosownie
 * do kalendarza lokalnego.(Mo�na by co� pomy�le� nad opcj�
 * wyboru kalendarza kt�rym chcemy si� pos�ugiwa�, jako cz�owiek
 * ma�o prawdopodobne abym wog�le zna� elfi kalendarz..).
 *
 * @return Pe�en komunikat b�d� 0 - je�li 0 gracz otrzyma komunikat standardowy
 */
public string check_time()
{
    return MT_CZAS_MUDOWY;
}

/**
 * @return Data zmiany pory dnia dla lokacji.
 */
public string check_date()
{
    return MT_DATA_MUDOWA;
}

/**
 * @return Aktualna pora dnia zgodna z definicjami z <mudtime.h>
 */
public int
pora_dnia()
{
    return MT_PORA_DNIAXY(TO->query_prop(ROOM_I_WSP_X), TO->query_prop(ROOM_I_WSP_Y));
}

/**
 * @return Aktualna pora roku z <mudtime.h>.
 */
public int
pora_roku()
{
    return MT_PORA_ROKU;
}

/**
 * Sprawdza czy na lokacji jest dzie� czy noc.
 * UWAGA: Lepiej u�ywa� funkcji o bardziej intuicyjnej nazwie 'jest_dzien' lub 'jest_noc'.
 *
 * @return 1 je�li jest obecnie noc
 * @return 0 je�li dzie�.
 */
public int
dzien_noc()
{
    switch (MT_PORA_DNIAXY(TO->query_prop(ROOM_I_WSP_X), TO->query_prop(ROOM_I_WSP_Y)))
    {
        case MT_WCZESNY_RANEK:
        case MT_RANEK:
        case MT_POLUDNIE:
        case MT_POPOLUDNIE:
        case MT_WIECZOR:
            return 0;
        case MT_POZNY_WIECZOR:
        case MT_NOC:
        case MT_SWIT:
            return 1;
        default:
            throw("Illegal value returned: pora_dnia() = " + pora_dnia()
                + ".\n");
    }
}

/**
 * Sprawdza czy na lokacji jest dzie� czy noc.
 *
 * @return 1 je�li dzie�.
 * @return 0 je�li noc.
 */
public int
jest_dzien()
{
    return !dzien_noc();
}
/**
 * Sprawdza czy na lokacji jest dzie� czy noc.
 *
 * @return 1 je�li noc.
 * @return 0 je�li dzie�.
 */
public int
jest_noc()
{
    return dzien_noc();
}

// --- Zmiany Kruna ---
//Funkcje do obs�ugiwania kolorowania wyj��.

/**
 * @return Zwraca kolor jaki ustawione maj� wyj�cia.
 */
int
query_exits_color()
{
    return EXITS_COLOR;
}

/**
 * @return Zwraca czy opis wyj�� pokazywany graczowi na lokacji jest standardowy czy te� nie.
 */
int
query_default_exits_description()
{
    return gDefaultExitDesc;
}
//----------------------
