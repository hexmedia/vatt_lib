/**
 * Lib wspomagający dodawanie Eventów.
 *
 * @author Rantaur
 * @author Krun - przeróbka na lib
 *
 */

#include <files.h>

/*x Zmienne */
static int event_al = 0; // alarm event'a
static mixed eventy = ({ });
static int last_event = -1; //indeks ostatniego eventa

/* Prototypy */
void show_event();

void
set_event_time(float czas)
{
    if (event_al)
        remove_alarm(event_al);

    EVENT_MASTER->add_event_alarm(czas, czas, show_event)
}

void add_event(string jaki, string func = "")
{
    if(jaki)
        eventy += ({ ({ jaki, func }) });
}

void show_event()
{
    int size = sizeof(eventy);

    if(i > 0)
    {
        int i = random(size);

        if(sizeof(eventy) > 1)
            while(i == last_event)
                i = random(size);

        last_event = i;

        tell_roombb(this_object(), eventy[i][0], 0, 0, 1);
        if (eventy[i][1] != "" && stringp(eventy[i][1]))
            call_other(this_object(), eventy[i][1]);
    }
}

