/**
 * Wydzielony z rooma plik odpowiedzialny za chrust.
 *
 * @author Krun
 */

void
pozbierany_chrust(object player)
{
    int mverr;
    object ob;

    set_this_player(player);

    ob = clone_object("/std/chrust.c");

    mverr = ob->move(TP);

    if(mverr)
    {
        if(mverr == 1)
            write(UC(ob->short(PL_MIA)) + " jest za ci�k" + ob->koncowka("i", "a", "ie") + ".\n");
        else if(mverr == 8)
            write(UC(ob->short(PL_MIA)) + " jest za du�" + ob->koncowka("y", "a", "e") + ", "+
                "odk�adasz j� na ziemie.\n");
        else
            write("Nie jeste� w stanie podnie�� " + ob->short(PL_MIA) + ".\n");

        saybb(QCIMIE(TP, PL_MIA) + " odk�ada " + QSHORT(ob, PL_BIE) + ".\n");

        ob->move(environment(TP));
    }
    else
    {
        write("Zebra�"+TP->koncowka("e�", "a�")+" troch� chrustu.\n");
        saybb(QCIMIE(TP, PL_MIA) + " uzbiera�"+TP->koncowka("", "a") +
            " troch� chrustu.\n");
    }

    change_prop(ROOM_I_ILOSC_CHRUSTU, (query_prop(ROOM_I_ILOSC_CHRUSTU)-1));
    TP->remove_prop(LIVE_S_EXTRA_SHORT);
}

varargs int
przestan_zbierac(mixed arg)
{
    if(!objectp(arg))
    {
        mixed *alarmy = get_all_alarms();
        mixed *args;
        int i;

        for(i=0;i<sizeof(alarmy);i++)
        {
            if(alarmy[i][1] == "pozbierany_chrust")
            {
                args = alarmy[i][4];
                if(args[0] == TP)
                remove_alarm(alarmy[i][0]);
            }
        }
    }
    saybb(QCIMIE(TP, PL_MIA) + " ko�czy zbiera� chrust.\n");
    TP->remove_prop(LIVE_S_EXTRA_SHORT);

}

int
zbieranie_chrustu(string str)
{
    int time;
    object par;

    notify_fail("Co takiego chcesz zebra�?\n");

    if(str != "chrust")
        return 0;

    if(!query_prop(ROOM_I_ILOSC_CHRUSTU))
    {
        write("Nie widzisz tu nigdzie ani jednej ga��zki, kt�r� " +
            TP->koncowka("m�g�by�", "mog�aby�")+" zebra�.\n");
        return 1;
    }


    if(TP->query_prop(PLAYER_M_SIT_SIEDZACY) || TP->query_prop(PLAYER_M_SIT_LEZACY))
    {
        notify_fail("A mo�e najpierw staniesz na nogi?\n");
        return 0;
    }

    write("Schylasz si� i zaczynasz zbiera� chrust.\n");
    saybb(QCIMIE(TP, PL_MIA) + " schyla si� i zaczyna zbiera� chrust.\n");

    if(time = query_prop(OBJ_I_SEARCH_TIME))
    {
        if(time < 0)
            time = 5;
        else
            time += 5;
    }

    time = max(2, (time - query_prop(ROOM_I_ILOSC_CHRUSTU)));

    par = clone_object("/std/paralyze.c");
    par -> set_stop_verb("przesta�");
    par -> set_standard_paralyze("zbiera� chrust");
    par -> set_stop_fun("przestan_zbierac");
    par -> set_stop_object(TO);
    par -> set_remove_time(time);
    par -> set_fail_message("Teraz zbierasz chrust, nie mo�esz tego zrobi�!\n");
    par -> move(TP, 1);

    TP->add_prop(LIVE_S_EXTRA_SHORT, " zbieraj�c"+TP->koncowka("y", "a")+" chrust");
    set_alarm(itof(time) + 0.01, 0.0, &pozbierany_chrust(TP));

    return 1;
}

void
init_chrust()
{
    add_action(zbieranie_chrustu, "zbierz");
}
