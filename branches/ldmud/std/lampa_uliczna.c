/*
 * Standard dla lamp ulicznych. Po burzliwych przemyśleniach wzielam
 *   sie do roboty i zrobilam to w stylu najprostszym i jednoczesnie
 *   zabezpieczajacym z gory przed jakimikolwiek niedorzecznymi bledami.
 *   Nie ma npca, nie ma pala, tyczki, nie ma tez oleju nawet!
 *   Jest za to piekna lampa, automatycznie zapalajaca sie i gaszaca
 *   (pseudo event ze strozem) o odpowiednich porach.
 *                                                            Lil.
 */

#pragma save_binary
#pragma strict_types

inherit "/std/object";
 
#include <object_types.h>
#include <cmdparse.h>
#include <composite.h>
#include <macros.h>
#include <stdproperties.h>
#include <materialy.h>
#include <mudtime.h>
 
/*
 * Prototypes:
 */
public void set_strength(int strength);
public int do_light(string str);
int sprawdzam_pore();
int zapalamy(int silent);
int gasimy(int silent);
//int noc;

/*
 * Global variables:
 */
private int Torch_Value,	/* The max value of the torch. */
	    Light_Strength,	/* How strongly the 'torch' will shine */
	    Time_Left;		/* How much time is left? */
string light_mess, extinguish_mess;


nomask void
configure_light()
{
}

/*
 * Function name: create_torch
 * Description:   The standard create. This has some standard settings on
 *		  long and short descriptions, but don't be afraid to change
 *		  them.
 *                The pshort has to be set to some value, otherwise the short
 *                description of several objects will look ugly.
 */
void
create_street_lamp()
{
}

/*
 * Function name: create_object
 * Description:   The standard create routine.
 */
nomask void
create_object()
{
    ustaw_nazwe("lampa uliczna");
    dodaj_nazwy("lampa");

    set_long("Jest to zwykła lampa uliczna.\n");

    add_prop(OBJ_I_LIGHT, 0);
    add_prop(OBJ_I_WEIGHT, 36211);
    add_prop(OBJ_I_VOLUME, 5419);
    add_prop(OBJ_I_VALUE, 601);
    add_prop(OBJ_I_NO_GET, "Ani drgnie. Jest zbyt mocno umocowana.\n");
    add_prop(OBJ_I_DONT_GLANCE, 1);
    ustaw_material(MATERIALY_STAL, 95);
    ustaw_material(MATERIALY_SZKLO, 5);

    set_strength(1);
    create_street_lamp();

    if (ROOM_OBJECT->dzien_noc())
	zapalamy(1); // silent
    else
	gasimy(1); // silent

    set_alarm(1200.0 + itof(random(600)), 0.0, "sprawdzam_pore");
}

int
sprawdzam_pore()
{
    if (environment(this_object())->dzien_noc() != query_prop(OBJ_I_HAS_FIRE))
    {
	if (environment(this_object())->dzien_noc())
	    zapalamy(0);
	else
	    gasimy(0);
    }

    set_alarm(1200.0 + itof(random(600)), 0.0, "sprawdzam_pore");
    return 1;
}

int 
zapalamy(int silent)
{
    add_prop(OBJ_I_HAS_FIRE, 1);
    add_prop(OBJ_I_LIGHT, Light_Strength);
    if (!silent)
	tell_room(environment(this_object()), light_mess);
    return 1;
}

int 
gasimy(int silent = 0)
{
    remove_prop(OBJ_I_HAS_FIRE);
    remove_prop(OBJ_I_LIGHT);
    if (!silent)
	tell_room(environment(this_object()), extinguish_mess);
    return 1;
}

/*
 * Function name: reset_torch
 * Description  : Since you may not mask reset_object() in torches, you have
 *                to redefine this function.
 */
public void
reset_torch()
{
}
 
/*
 * Function name: set_strength
 * Description:   Set the light strength of this 'torch'
 */
public void
set_strength(int strength)
{
    Light_Strength = strength;
}

/*
 * Function name: query_strength
 * Description:   Query how strongly the torch will shine
 */
public int
query_strength()
{
    return Light_Strength;
}

/*
 * Function name: do_light
 * Description:   The player tries to light something.
 * Arguments:	  str - the string describing what he  wants to light.
 * Returns:	  1/0
 */
public int
do_light(string str)
{
    object *a;
    string str2, vb;


    
    seteuid(getuid(this_object()));

	str2 = COMPOSITE_DEAD(a, PL_BIE);
	write("Zapalasz " + str2 + ".\n");
	a->dodaj_przym("zapalony", "zapaleni");
	a->odmien_short();
	a->odmien_plural_short();
	add_prop(OBJ_I_HAS_FIRE, 1);
        add_prop(OBJ_I_LIGHT, 1);

	return 1;
}

void
set_light_message(string str)
{
    light_mess = str;
}

string
query_light_message()
{
    return light_mess;
}

void
set_extinguish_message(string str)
{
    extinguish_mess = str;
}

string
query_extinguish_message()
{
    return extinguish_mess;
}

int
query_lampa_uliczna()
{
   return 1;
}
