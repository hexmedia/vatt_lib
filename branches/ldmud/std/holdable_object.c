/**
 * Standard do obiektow ktore mozemy 'chwycic'. Dziala na
 * podstawie std broni. Bo przeciez wszystko moze byc potencjalna
 * bronia :P
 *
 * Zmiany opieraja sie glownie na zmienieniu komendy 'dobadz'
 * na 'chwyc' oraz na zakomentowaniu kilku funkcji...
 * Sa jeszcze zmiany w komunikatach.
 *                                            Delvert & Lil 03.07.2005
 *
 *
 *  A ja pozwoli�em sobie wprowadzi� holdable_item i oprze� ten obiekt o to
 * w sumie mo�na by wycofa� ale mi si� po prostu nie chce:P
 *                                              Krun 12.04.2008
 *
 * \file /std/holdable_object.c
 *
 * Contains all routines relating to weapons of any kind.
 * Defined functions and variables:
 *
 * set_hit(int)		Sets the weapon "to hit" value.
 *
 * set_pen(int)		Sets the weapon penetration value.
 *
 * set_pm(int *)	Sets some modifiers for the penetration on the weapon.
 *			only useful if weapon is of multiple damage type.
 *
 * set_wt(type)		Set the weapon type.
 *			If not defined, set to default.
 *
 * set_dt(type)		Set the damage type.
 *			If not defined, set to default.
 *
 * set_hands(which)	Set the hand(s) used to wield the weapon.
 *			If not defined, set to default.
 *
 * set_wf(obj)		Sets the name of the object that contains the function
 *			to call for extra defined wield() and unwield()
 *			functions.
 *
 * query_wielded()      Returns the wielder if this object is wielded
 */

#pragma save_binary
#pragma strict_types

inherit "/std/object";
inherit "/lib/holdable_item.c";

#include <files.h>
#include <macros.h>
#include <cmdparse.h>
#include <formulas.h>
#include <language.h>
#include <ss_types.h>
#include <wa_types.h>
#include <object_types.h>
#include <stdproperties.h>

string jak_pachnie, jak_wacha_jak_sie_zachowuje; /*dwie takie male pierdolki do
                                      wachania przedmiotow. Lil  */

static int      hits,		/* No of hits the weapon have made */
                dull,		/* How dull the weapon has become */
                corroded,	/* Corrotion on the weapon */
                repair_dull,	/* How much dullness we have repaired */
                repair_corr,	/* How much corrosion we have repaired */
                likely_corr,	/* How likely will this weapon corrode */
                likely_break,	/* How likely will this weapon break? */
                likely_dull,	/* How likely will it be dulled by battle? */
                max_value;	/* Value of weapon at prime condition */
static string   gFail;          /* Acc error messages when wielding */

/*
 * Prototypes
 */
varargs void    remove_broken(int silent = 0);

/*
 * Function name: create_weapon
 * Description  : Create the weapon. You must define this function to
 *                construct the weapon.
 */
public void
create_holdable_object()
{
}

/*
 * Function name: create
 * Description  : Create the weapon. As this function is declared nomask
 *                you must use the function create_weapon to actually
 *                construct it. This function does some basic initializing.
 */
public nomask void
create_object()
{
    likely_dull = 10;
    likely_corr = 10;
    likely_break = 10;
    wep_hands = F_WEAPON_DEFAULT_HANDS;
    wielded = 0;
    add_prop(OBJ_I_VALUE, "@@query_value");
    add_prop(OBJ_I_VOLUME, 500);
    add_prop(OBJ_I_WEIGHT, 200);

    create_holdable_object();
    configure_holdable_item();
}

/*
 * Pokazuje, jak dany przedmiot pachnie.
 *                                    Lil.
 */
int
smell_effect()
{
    if (jak_pachnie) write(check_call(jak_pachnie)+"\n");
    if (jak_wacha_jak_sie_zachowuje) saybb(QCIMIE(this_player(), PL_MIA)+
        " "+check_call(jak_wacha_jak_sie_zachowuje)+"\n");
    return 1;
}

/*
 * Function name: reset_weapon
 * Description  : This function can be used to reset the weapon at a regular
 *                interval. However, if you do so, you must also call the
 *                function enable_reset() from your create_weapon() function
 *                in order to start up the reset proces.
 */
public void
reset_holdable_object()
{
}

/*
 * Function name: reset_object
 * Description  : Reset the weapon. If you want to make the weapon reset
 *                at a certain interval you must use reset_weapon as this
 *                function is nomasked.
 */
public nomask void
reset_object()
{
    reset_holdable_object();
}

/*
 * Function name: short
 * Description  : The short description. We modify it when the weapon is
 *                broken. There is a little caveat if the wizard has not
 *                set a short description since it will double the
 *                adjective 'broken'.
 * Arguments    : object for_obj - the object that wants to know.
 * Returns      : string - the description.
 */
public varargs string
short(mixed for_obj, mixed przyp)
{
    if (!objectp(for_obj))
    {
        if (intp(for_obj))
            przyp = for_obj;
        else if (stringp(for_obj))
            przyp = atoi(for_obj);

        for_obj = previous_object();
    }
    else
        if (stringp(przyp))
            przyp = atoi(przyp);

    return ::short(for_obj, przyp);
}

/*
 * Function name: plural_short
 * Description  : The plural short description. When the weapon is broken,
 *                we alter it. See 'short' for details.
 * Arguments    : object for_obj - the object that wants to know.
 * Returns      : string - the description.
 */
public varargs string
plural_short(mixed for_obj, int przyp)
{
    string str;

    if (intp(for_obj))
    {
        przyp = for_obj;
        for_obj = this_player();
    }
    /* nie sprawdzamy czy przyp to int, bo nie ma makr do tej funkcji */

    str = ::plural_short(for_obj, przyp);

    return str;
}


/*
 * Function name: leave_env
 * Description  : When the weapon leaves a certain inventory this function
 *                is called. If the weapon is wielded, we unwield. If you
 *                mask this function you _must_ make a call to ::leave_env
 * Arguments    : object from - the object the weapon leaves.
 *                object desc - the destination of the weapon.
 */
varargs void
leave_env(object from, object dest, string fromSubloc)
{
    holdable_item_leave_env(from, dest, fromSubloc);

    ::leave_env(from, dest, fromSubloc);
}

/*
 * Description: This is the tool slot that the weapon occupies now
 */
int *
query_slots()
{
    int abit, *hids;

    for (hids = ({}), abit = 2; abit <= wielded_in_hand; abit <<= 1)
    {
        if (wielded_in_hand & abit)
        {
            hids = hids + ({ abit });
        }
    }
    return hids;
}


/*
 * Function name: set_corroded
 * Description:   Use this to increases the corroded status on weapons.
 * Arguments:     cond - The new condition we want (can only be raised)
 * Returns:       1 if new condition accepted, 0 if no corrosion
 */
int
set_corroded(int corr)
{
    if (corr > corroded)
    {
        corroded = corr;
        if (F_WEAPON_BREAK(dull - repair_dull, corroded - repair_corr,
			likely_break))
            set_alarm(0.1, 0.0, &remove_broken(0));
        if (wielded && wielder)
            wielder->update_weapon(this_object());
        return 1;
    }
    return 0;
}

/*
 * Function name: query_corroded
 * Description:   Returns how many times this weapon has become corroded
 * Returns:	  The number of times
 */
int query_corroded()
{
    return corroded;
}

/*
 * Function name: set_likely_corr
 * Description:   Set how likely it is this weapon will corrode when in acid
 *		  or something like that. 0 means it won|t corrode at all.
 * Arguments:	  i - how likely it will corrode, probably corrode if random(i)
 *		      [0, 20] recommended
 */
void set_likely_corr(int i) { likely_corr = i; }

/*
 * Function name: query_likely_corr
 * Description:   Query how likely the weapon will corrode, use it to test
 * 		  if weapon survived your rustmonster or acid pool :)
 */
int query_likely_corr() { return likely_corr; }

/*
 * Function name: set_dull
 * Description:   Use this to increases the dull status on weapons.
 * Arguments:     cond - The new condition we want (can only be raised)
 * Returns:       1 if new condition accepted
 */
int
set_dull(int du)
{
    if (du > dull)
    {
        dull = du;
        if (F_WEAPON_BREAK(dull - repair_dull, corroded - repair_corr,
			likely_break))
            set_alarm(0.1, 0.0, &remove_broken(0));
        if (wielded && wielder)
            wielder->update_weapon(this_object());
        return 1;
    }
    return 0;
}

/*
 * Function name: query_dull
 * Description:   Returns how many times this weapon has become duller
 * Returns:	  The number of times
 */
int query_dull() { return dull; }

/*
 * Function name: set_likely_dull
 * Description:   Set how likely the weapon will get dull or in case of a club
 *		  or mace, wear down in combat Mainly used from did_hit().
 * Arguments:     i - how likely [0, 30] recommended
 */
void set_likely_dull(int i) { likely_dull = i; }

/*
 * Function name: query_likely_dull
 * Description:   How likely it is this weapon will become duller when used
 * Returns:       How likely it is
 */
int query_likely_dull() { return likely_dull; }

/*
 * Function name: set_likely_break
 * Description:   Set how likely the weapon is to break if you use it.
 * Argument:	  i - How likely, [0, 20] recommended
 */
void set_likely_break(int i) { likely_break = i; }

/*
 * Function name: query_likely_break
 * Description:   How likely is it this weapon will break with use
 * Returns:	  How likely it is
 */
int query_likely_break() { return likely_break; }

/*
 * Function name: remove_broken
 * Description  : The weapon got broken so the player has to stop
 *                wielding it.
 * Arguments    : int silent - true if no message is to be genereated
 *                             about the fact that the weapon broke.
 */
varargs void
remove_broken(int silent = 0)
{
    if(this_object()->query_prop(OBJ_I_BROKEN))
        return;

    if (!wielded || !wielder)
    {
        add_prop(OBJ_I_BROKEN, 1);
        return;
    }

    if (objectp(wield_func))
        wield_func->unwield(this_object());

    /* If the wizard so chooses, these messages may be suppressed. */
    if (!silent)
    {
        tell_object(wielder, capitalize(short(wielder, PL_MIA)) + " ulega zniszczeniu!!!\n");
        tell_room(environment(wielder), QCSHORT(this_object(), PL_MIA) +
            " trzym" + koncowka("any", "ana", "ane", "ani", "ane") +
            " przez " + QCIMIE(wielder, PL_DOP) + " ulega zniszczeniu!!!\n", ({wielder}));
    }

    /* Force the player to unwield the weapon and then adjust the broken
     * information by adding the property and the adjective.
     */
    wielder->unwield(this_object());
    add_prop(OBJ_I_BROKEN, 1);
    wielded = 0;
}

/*
 * Function name: set_repair_dull
 * Description:   When trying to repair the weapon, call this function. Repairs
 *                can only increase the repair factor. (This is sharpening)
 * Arguments:     rep - The new repair number
 * Returns:       1 if new repair status accepted
 */
int
set_repair_dull(int rep)
{
    if (rep > repair_dull && F_LEGAL_WEAPON_REPAIR_DULL(rep, dull))
    {
        repair_dull = rep;
        if (wielded && wielder)
            wielder->update_weapon(this_object());
        return 1;
    }
    return 0;
}

/*
 * Function name: query_repair_dull
 * Description:   How many times has this weapon been sharpened
 * Returns:	  The number of times
 */
int query_repair_dull() { return repair_dull; }

/*
 * Function name: set_repair_corr
 * Description:   When trying to repair the weapon, call this function. Repairs
 *                can only increase the repair factor. This repairs corroded.
 * Arguments:     rep - The new repair number
 * Returns:       1 if new repair status accepted
 */
int
set_repair_corr(int rep)
{
    if (rep > repair_corr && F_LEGAL_WEAPON_REPAIR_CORR(rep, corroded))
    {
        repair_corr = rep;
        if (wielded && wielder)
            wielder->update_weapon(this_object());
        return 1;
    }
    return 0;
}

/*
 * Function name: query_repair_corr
 * Description:	  How many times this weapon has been repaired from corrosion
 * Returns:	  How many times
 */
int query_repair_corr() { return repair_corr; }

/*
 * Function name: set_weapon_hits
 * Description:   By setting the hits counter you will have influence over how
 *                likely the weapon is to get in a worse condition. The hits
 *                variable keeps track of how many times this piece of weapon
 *                has hit something.
 * Argument:      new_hits - integer
 */
public void
set_weapon_hits(int new_hits) { hits = new_hits; }

/*
 * Function name: query_weapon_hits
 * Description:   hits variable keeps track of how many times this weapon has
 *		  hit something.
 * Returns:	  The number of times
 */
public int
query_weapon_hits() { return hits; }

/*
 * Function name: add_prop_obj_i_value
 * Description:   Someone is adding the value prop to this object.
 * Arguments:     val - The new value (mixed)
 * Returns:       1 if not to let the val variable through to the prop
 */
int
add_prop_obj_i_value(mixed val)
{
    if (!max_value)
    {
        max_value = -1;
        return 0;
    }

    if (intp(val) && val)
        max_value = val;

    return 1;
}

/*
 * Function name: query_value
 * Description:   Qhat's the value of this armour
 */
int
query_value()
{
    if (query_prop(OBJ_I_BROKEN))
        return 0;

    return max_value *
	F_WEAPON_VALUE_REDUCE(dull - repair_dull, corroded - repair_corr) / 100;
}


/*
 * Function name: try_hit
 * Description:   Called from living when weapon used.
 * Arguments:     target - Who I intend to hit.
 * Returns:       False if weapon miss. If true it might hit.
 */
int try_hit(object target) { return 1; }

/*
 * Function name: query_is_holdable_object
 * Description:   Sprawdza, czy item jest holdable_object
 * Returns:       Zawsze 1.
 */
nomask int
query_is_holdable_object()
{
    return 1;
}

/*
 * Function name: query_am
 * Description:   Called when wielding the weapon, to check for the parry.
 * Returns:       The armour modifier
 */
public nomask int *
query_am() { return ({ -3, 2, 1}); }
