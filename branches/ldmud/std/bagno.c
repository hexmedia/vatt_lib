/* bagno pope�ni� Avard,
    przejrza� i poprawi� co nieco i gdzieniegdzie Vera ;p */

inherit "/std/room";

#include <stdproperties.h>
#include <macros.h>
#include <filter_funs.h>
#include <object_types.h>
#include <mudtime.h>
#include <ss_types.h>
#include <cmdparse.h>
#include <composite.h>

#define PREDKOSC_ZAPADANIA_SIE (7.0 + itof(random(10)))

int trudnosc_bagna = 1;
int i,sz;
int j = 0;

nomask void start_zapadania(object ob);
nomask void zapadanie(object ob);
varargs string opis_zapadania(int ktory, object ob, object gracz);
void zniszcz_przedmioty();
int wyciaganie(object ratujacy, object ratowany);
private int wyciagnij(string str);

//lista obiekt�w do zniszczenia, wywo�ywana po kilku sek. od od�o�enia
//pierwszego itemu
object *do_zniszczenia = ({ });
int zniszcz_przedmioty_alrm = 0;

void
create_room()
{
    set_short("Bagno");
    set_long("Bagniste bagno\n");
}

void
init()
{
    add_action(wyciagnij, "wyci^agnij");
    ::init();
}

/*
 *   Ustawia jak latwo jest sie utopic w tym bagnie:
 *   1 - bardzo latwe bagno, trudno sie utopic
 *   10 - bardzo trudne bagno, latwo sie utopic
 */

void ustaw_trudnosc_bagna(int trudnosc)
{
    trudnosc_bagna = trudnosc;
}

int query_trudnosc_bagna()
{
    return trudnosc_bagna;
}

public void
enter_inv(object ob, object from)
{
    ::enter_inv(ob, from);

    if(!ob->query_prop(OBJ_I_NIE_WCIAGANY) || !ob->query_prop(LIVE_I_NIE_WCIAGANY))
    {
        if(ob->query_player() != 1 && ob->query_npc() != 1)
        {
            if(ob->query_prop(HEAP_I_IS))
                ob->split_heap(1);

            do_zniszczenia+=({ob});
            if(!get_alarm(zniszcz_przedmioty_alrm))
                zniszcz_przedmioty_alrm =
                    set_alarm(3.0+itof(random(8)),0.0,&zniszcz_przedmioty());
        }
        else
        {
            if(ob->query_prop(OBJ_I_WEIGHT) > 110000 - trudnosc_bagna*6000 )
            {
                set_alarm(1.0+itof(random(3)),0.0,&start_zapadania(ob));
            }
        }
    }
}

public void
leave_inv(object ob, object to)
{
    ::leave_inv(ob, to);

    //je�li zd��yli�my zabra�, to wywalmy z tablicy do usuni�cia.
    if(member_array(ob,do_zniszczenia) != -1)
        do_zniszczenia-=({ob});
}

void
zniszcz_przedmioty()
{
    do_zniszczenia-=({0});

    if(!sizeof(do_zniszczenia)) //...to si� niepowinno zdarzy� nigdy
        return;

    /*if(ob->query_corpse() == 0) // <---Avard wyja�nij!
    {
        tell_roombb(TO,capitalize(ob->query_short()) + " zapada si� w bagnie.\n");
    }*/

    tell_room(TO,capitalize(COMPOSITE_DEAD(do_zniszczenia,PL_MIA))+
            " zapada"+ (sizeof(do_zniszczenia)>1?"j�":"") +
            " si� w bagnistej toni.\n");

    foreach(object ob : do_zniszczenia)
    {
        do_zniszczenia-=({ob});
        ob->remove_object();
    }

}

nomask void
start_zapadania(object ob)
{
    if(ENV(ob) != TO)   //tzn. �e szybko przebieg� przez lokacj�
        return;

    object paraliz = clone_object("/std/paralize/bagienny");
    ob->add_prop(LIVE_S_EXTRA_SHORT, " topi^ac" + ob->koncowka("y", "a", "e")
        +" si^e w bagnie");
    ob->add_prop("paraliz_bagienny", paraliz);
    ob->add_prop("zapadanie_sie_w_bagnie", 1);
    paraliz->move(ob);
    ob->catch_msg(opis_zapadania(1, ob));

/*  Nie trzeba si� tak m�czy�... Zapoznaj si� lepiej z smanem do tell_room :)

    object *obs = FILTER_LIVE(all_inventory(environment(ob)));
    for(i=0, sz=sizeof(obs); i<sz; i++)
    {
        if(obs[i] != ob)
            obs[i]->catch_msg(opis_zapadania(2, obs[i], ob));
    }

    zamiast takich wygibas�w, wystarczy jedna linijka: */
    tell_room(TO,opis_zapadania(2,TO,ob),({ob}));


    if(ob->query_prop("zapadanie_sie_w_bagnie"))
    {
        set_alarm(PREDKOSC_ZAPADANIA_SIE,0.0,&zapadanie(ob));
    }
}

nomask void
zapadanie(object ob)
{
    if(ob->query_prop("zapadanie_sie_w_bagnie"))
    {
        //object *obs = FILTER_LIVE(all_inventory(environment(ob)));
        if(ob->query_fatigue() > 5 && (ob->query_stat(SS_STR) +
            ob->query_stat(SS_DEX))/2 + random(10) >= trudnosc_bagna*10)
        {
           ob->catch_msg(opis_zapadania(3, ob));
            /*
            for(i=0, sz=sizeof(obs); i<sz; i++)
            {
                if(obs[i] != ob)
                    obs[i]->catch_msg(opis_zapadania(4, obs[i], ob));
            }*/
            tell_room(TO,opis_zapadania(4,TO,ob),({ob}));

            ob->query_prop("paraliz_bagienny")->remove_object();
            ob->remove_prop("zapadanie_sie_w_bagnie");
            ob->remove_prop("paraliz_bagienny");
            ob->add_prop(LIVE_S_EXTRA_SHORT, "");
            j=0;
        }
        else if((ob->query_fatigue() > 5) && (ob->query_stat(SS_STR) +
            ob->query_stat(SS_DEX))/2 + random(10) < trudnosc_bagna*10)
        {
            switch(j)
            {
                case 0..1:
                    ob->catch_msg(opis_zapadania(5, ob));
                    /*for(i=0, sz=sizeof(obs); i<sz; i++)
                    {
                        if(obs[i] != ob)
                            obs[i]->catch_msg(opis_zapadania(6, obs[i], ob));
                    }*/
                    tell_room(TO,opis_zapadania(6,TO,ob),({ob}));
                    j++;
                    set_alarm(PREDKOSC_ZAPADANIA_SIE,0.0,&zapadanie(ob));
                break;

                case 2:
                    ob->catch_msg(opis_zapadania(7, ob));
                    /*for(i=0, sz=sizeof(obs); i<sz; i++)
                    {
                        if(obs[i] != ob)
                            obs[i]->catch_msg(opis_zapadania(8, obs[i], ob));
                    }*/
                    tell_room(TO,opis_zapadania(8,TO,ob),({ob}));
                    j++;
                    set_alarm(PREDKOSC_ZAPADANIA_SIE,0.0,&zapadanie(ob));
                break;

                case 3:
                    ob->catch_msg(opis_zapadania(9, ob));
                    /*for(i=0, sz=sizeof(obs); i<sz; i++)
                    {
                        if(obs[i] != ob)
                            obs[i]->catch_msg(opis_zapadania(10, obs[i], ob));
                    }*/
                    tell_room(TO,opis_zapadania(10,TO,ob),({ob}));
                    j = 0;
                    ob->set_hp(0);
                    ob->do_die();
                break;

                default:
                    ob->catch_msg("Wyst^api^l powa^zny b^l^ad w zapadaniu, "+
                    "zg^lo^s go w tej lokacji.\n");
                break;
            }
        }
        else if(ob->query_fatigue() <= 5)
        {
            ob->catch_msg(opis_zapadania(9, ob));
            /*for(i=0, sz=sizeof(obs); i<sz; i++)
            {
                if(obs[i] != ob)
                obs[i]->catch_msg(opis_zapadania(10, obs[i], ob));
            }*/
            tell_room(TO,opis_zapadania(10,TO,ob),({ob}));
            ob->set_hp(0);
            ob->do_die();
        }
    }
}

string
opis_zapadania(int ktory, object ob, object gracz)
{
    if(gracz == 0)
    {
        gracz = ob;
    }
    switch(ktory)
    {
        case 1:
            return "Nagle czujesz, ^ze ziemia pod tob^a zaczyna si^e "+
            "zapada^c, a ty razem z ni^a!\n";
        break;
        case 2:
            return "Nagle ziemia pod " + QIMIE(gracz, PL_NAR) + " zaczyna "+
            "si^e zapada^c. A " + gracz->koncowka("on", "ona", "ono")+
            " razem z ni^a!\n";
        break;
        case 3:
            return "Z trudem wyrywasz najpierw jedn^a, potem drug^a nog^e z "+
            "kleistego pod^lo^za. Po chwili roztrzesion"+
            gracz->koncowka("y","a","e")+ " wychodzisz z bagna.\n";
        break;
        case 4:
            return QCIMIE(gracz, PL_MIA)+" z trudem wyrywa najpierw jedn^a, "+
            "a nast^epnie drug^a nog^e z kleistego pod^lo^za. Po chwili "+
            "roztrz^esion" + gracz->koncowka("y", "a", "o") + " wychodzi z "+
            "bagna.\n";
        break;
        case 5:
            return "Pr^obujesz wydosta^c si^e z grz^askiego pod^lo^za, ale "+
            "nie dajesz rady.\n";
        break;
        case 6:
            return QCIMIE(gracz, PL_MIA) + " stara si^e wydosta^c z bagna, "+
            "ale nie udaje "+gracz->koncowka("mu", "jej")+" si^e to.\n";
        break;
        case 7:
            return "Robi ci si^e coraz zimniej...";
        break;
        case 8:
            return QCIMIE(gracz, PL_MIA) + " rozgl^ada si^e przera^zon"+
            gracz->koncowka("y", "a", "e") + ", poszukuj^ac pomocy.\n";
        break;
        case 9:
            return "Zach^lysn"+ gracz->koncowka("��e�", "�a�")+
            " si^e b^lotem i zupe^lnie trac^ac si^ly "+
            "opadasz w bagnist� to�...\n";
        break;
        case 10:
            return QCIMIE(gracz, PL_MIA) + " nagle si^e zach^lysn"+
            gracz->koncowka("^a^l", "^e^la", "^e^lo") + " i znik"+
            gracz->koncowka("n^a^l", "^la", "^lo") + " w b^locie.\n";
        break;

        default:
            return "Wyst^api^l powa^zny b^l^ad o indeksie 1.1, koniecznie "+
            "zg^lo^s go w tej lokacji.\n";
    }
}

private int
wyciagnij(string str)
{
    object *obs = FILTER_LIVE(all_inventory(environment(TP)));
    int ret;

    notify_fail("Kogo chcesz wyci^agn^a^c z bagna?\n");

    if (!strlen(str))
        return 0;

    if (!parse_command(str, all_inventory(environment(TP)),
        " %l:" + PL_MIE + " 'z' 'bagna'", obs))
        return 0;

    obs = NORMAL_ACCESS(obs, 0, 0);

    if (!sizeof(obs))
        return 0;

    if (sizeof(obs) > 1)
    {
        notify_fail("Nie dasz rady wyci^agn^a^c wi^ecej ni^z jedn^a "+
            "osob^e naraz!\n");
        return 0;
    }

    if(!obs[0]->query_prop("zapadanie_sie_w_bagnie"))
    {
        write("Ale^z " + obs[0]->koncowka("on", "ona") + " nie zapada "+
            "si^e w bagnie!\n");
        return 0;
    }
    wyciaganie(TP, obs[0]);
    return 1;
}

int
wyciaganie(object ratujacy, object ratowany)
{
    if(ratujacy->query_prop("zapadanie_sie_w_bagnie"))
    {
        ratujacy->catch_msg("Nie mo^zesz wyci^aga^c innych samemu si^e "+
            "topi^ac.\n");
        return 1;
    }
    else if((ratujacy->query_stat(SS_STR)*8 +
        ratujacy->query_stat(SS_DEX)*2)/10 < 50 ||
        ratujacy->query_fatigue() < 45)
    {
        ratujacy->catch_msg("Nie masz wystarczaj^aco si^l, ^zeby to "+
            "uczyni^c.\n");
        return 1;
    }
    else if(ratujacy->query_stat(SS_STR) + ratujacy->query_stat(SS_DEX) <
            ratowany->query_prop(OBJ_I_WEIGHT)/2000 + trudnosc_bagna*10)
    {
        ratujacy->catch_msg(QCIMIE(ratowany, PL_MIA) + " jest zbyt ci^e^zki!\n");
        return 1;
    }
    else
    {
        ratujacy->catch_msg("^Lapiesz "+QCIMIE(ratowany, PL_BIE) +
            " za r^ek^e i szybkim szarpni^eciem wyci^agasz " +
            ratowany->koncowka("go", "j^a")+" z b^lota.\n");
        ratowany->catch_msg(QCIMIE(ratujacy, PL_MIA) + " ^lapie ci^e "+
            "gwa^ltowanie za r^ek^e i szybkim szarpni^eciem wyci^aga "+
            "ci^e na bezpieczne pod^lo^ze.\n");
        object *obs = FILTER_LIVE(all_inventory(environment(ratowany)));
        int i, sz;
        for(i=0, sz=sizeof(obs); i<sz; i++)
        {
            if(obs[i] != ratowany && obs[i] != ratujacy)
            {
                obs[i]->catch_msg(QCIMIE(ratujacy, PL_MIA) + " szybkim "+
                    "szarpni^eciem r^eki wyci^aga " +
                    QCIMIE(ratowany, PL_BIE) + " z b^lota.\n");
            }
        }
        ratowany->remove_prop("zapadanie_sie_w_bagnie");
        ratowany->query_prop("paraliz_bagienny")->remove_object();
        ratowany->add_prop(LIVE_S_EXTRA_SHORT, "");
        ratujacy->set_fatigue(ratujacy->query_fatigue() - 45);
        return 1;
    }
}

public string
query_auto_load()
{
    return 0;
}
