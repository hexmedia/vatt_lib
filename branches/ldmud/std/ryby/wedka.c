/* Wedki, 
 * popelnione przez Rantaura
 * roku 2007 w okolicach miesiaca czerwca
 */

inherit "/std/holdable_container";

#include <cmdparse.h>
#include <exp.h>
#include <macros.h>
#include <math.h>
#include <pl.h>
#include <stdproperties.h>
#include <ss_types.h>
#include <wa_types.h>

#define DBG(x) find_player("root")->catch_msg(x+"\n");
#define CZAS_MIN 10.0
#define CZAS_RAND 30

float	gietkosc=0.95;		// Gietkosc wedki
string	lowisko;			// �owisko w kt�rym zarzucili�my w�dk�
mixed	*ryba;				// Ryba �owiona w danej chwili

int	alarm_haczyk;			// Alarm podczas wiazania haczyka
int	alarm_zarzucona;		// Alarm na branie
int	alarm_niezacieta;		// Alarm na zerwanie sie ryby, jesli gracz jej nie zatnie

int	zarzucona;				// Czy wedka jest teraz zarzucona
int	fake_branie;			// Czy mamy fake-branie

int przywiaz(string str);
int odwiaz(string str);
int naloz(string str);
int zarzuc(string str);
int wyciagnij(string str);
int zatnij(string str);
int zdejmij(string str);
public int pomoc(string str);

string query_przyneta();
void throw_me(string gdzie);
void take_me_out(object kto);
void lowienie_zacieta();
/*void hook_zarzuc();
void hook_zatnij_pusta();
void hook_zatnij();*/



void 
create_wedka()
{
    ustaw_nazwe("wedka");
    dodaj_przym("d�ugi", "d�udzy");
    dodaj_przym("leszczynowy", "leszczynowi");

    set_long("Jest to wedka.\n");
}

void
create_holdable_container()
{
    add_subloc("_haczyk");
    add_subloc_prop("_haczyk", CONT_I_CLOSED, 1);
    /*add_subloc_prop("_haczyk",  CONT_I_MAX_WEIGHT, 50000);
    add_subloc_prop("_haczyk",  CONT_I_MAX_VOLUME, 50000);*/


    add_subloc("_przyneta");
    add_subloc_prop("_przyneta", CONT_I_CLOSED, 1);
    /*add_subloc_prop("_przyneta",  CONT_I_MAX_WEIGHT, 50000);
    add_subloc_prop("_przyneta",  CONT_I_MAX_VOLUME, 50000);*/


    add_subloc("_ryba");
    add_subloc_prop("_ryba", CONT_I_CLOSED, 1);
    /*add_subloc_prop("_ryba",  CONT_I_MAX_WEIGHT, 50000);
    add_subloc_prop("_ryba",  CONT_I_MAX_VOLUME, 100000);*/


    /* Do wedki nie mozemy nic wk�ada� ;) */
    add_prop(CONT_I_CLOSED, 1);
    add_prop(CONT_I_CANT_OPENCLOSE, 1);
    add_prop(CONT_I_DONT_SHOW_CONTENTS, 1);
    add_prop(CONT_I_CANT_WLOZ_DO, 1);
    add_prop(CONT_I_TRANSP, 0);

    set_hands(A_HANDS);

    create_wedka();
    
    remove_prop(CONT_I_MAX_VOLUME);
    remove_prop(CONT_I_MAX_WEIGHT);

    set_long(long()+"@@subloc_desc@@");

    setuid();
    seteuid(getuid());
}

public void
przed_opuszczeniem()
{
    take_me_out(TP);
}


init()
{
    ::init();
    add_action(&przywiaz(), "przywi��");
    add_action(&odwiaz(), "odwi��");
    add_action(&naloz(), "na��");
    add_action(&naloz(), "za��");
    add_action(&zarzuc(), "zarzu�");
    add_action(&wyciagnij(), "wyci�gnij");
    add_action(&zatnij(), "zatnij");
    add_action(&zdejmij(), "zdejmij");
    add_action(&pomoc(), "?", 2);

}

string
subloc_desc()
{
    string ret = "";

    if(zarzucona)
	return "W tej chwili jest zarzucona "+lowisko+".\n";

    if(sizeof(subinventory("_ryba")) > 0)
	return "Na ko�cu sznurka dynda "+subinventory("_ryba")[0]->short()+".\n";

    object *haczyk = subinventory("_haczyk");

    if(!sizeof(haczyk) || !haczyk)
	return ret;
    ret += "Na ko�cu sznurka dostrzegasz zawi�zany "
	    +haczyk[0]->short(PL_BIE); 

    object *przyneta = subinventory("_przyneta");

    if(!sizeof(przyneta) || !przyneta)
	ret += ".\n";
    else
	ret += ", za� na "+haczyk[0]->koncowka("nim", "niej")
	    +" zawieszon"+przyneta[0]->koncowka("y", "a", "e")+" jest "
	    +przyneta[0]->short(PL_MIA)+".\n";

    return ret;
}


int
przywiaz_stop()
{
    remove_alarm(alarm_haczyk);
    alarm_haczyk = 0;

    saybb(QCIMIE(this_player(), PL_MIA) + " przestaje przywi�zywa� sznurek w�dki"
            +" do haczyka.\n");

    return 0;
}

int 
przywiaz_finish(int czy_udalo, object haczyk)
{
    alarm_haczyk = 0;

    if(czy_udalo)
    {
        add_subloc_prop("_haczyk", CONT_I_CLOSED, 0);
	    haczyk->move(this_object(), "_haczyk");
        add_subloc_prop("_haczyk", CONT_I_CLOSED, 1);

	    if(TP->query_skill(SS_FISHING) < 40)
	    {
	        TP->increase_ss(SS_DEX, EXP_WEDKARSTWO_PRZYWIAZYWANIE_DEX);
	        TP->increase_ss(SS_FISHING, EXP_WEDKARSTWO_PRZYWIAZYWANIE_FISHING);
	    }

	    TP->catch_msg("Uda�o ci si� przywi�za� haczyk.\n");
	    saybb(QCIMIE(TP, PL_MIA)+" przywi�zuje "+haczyk->short(PL_BIE)
	        +" do sznurka "+short(PL_DOP)+".\n");
    }
    else
	    TP->catch_msg("Mimo dobrych ch�ci nie uda�o ci si� przywi�za� haczyka."
		            +" W�ze� rozwi�za� si� w najmniej odpowiednim momencie.\n");

    return 1;
}

int 
przywiaz(string str)
{
    notify_fail("Przywi�� co do czego?\n");

    if(!str)
	return 0;

    object *wedka, *haczyk;
    if(!parse_command(lower_case(str), TP, " %i:"+PL_BIE+" 'do' %i:"+PL_DOP, haczyk, wedka))
	return 0;

    haczyk = NORMAL_ACCESS(haczyk, 0, 0);
    wedka = NORMAL_ACCESS(wedka, 0, 0);

    if(!sizeof(wedka) || !wedka)
	return 0;

    if(sizeof(wedka) > 1)
    {
	notify_fail("Mo�esz przywi�za� haczyk tylko do jednej w�dki!\n");
	return 0;
    }

    if(wedka[0] != TO)
	return 0;

    if(zarzucona)
    {
	notify_fail("Najpierw lepiej by�oby wyci�gn�� j� z wody.\n");
	return 0;
    }

    if(!sizeof(haczyk) || !haczyk)
	return 0;

    if(sizeof(haczyk) > 1)
    {
	notify_fail("Do w�dki mo�esz przywi�za� tylko jeden haczyk.\n");
	return 0;
    }

    if(!(haczyk[0]->query_haczyk()))
	return 0;

    if(sizeof(subinventory("_haczyk")))
    {
	notify_fail("Na w�dce znajduje si� ju� haczyk!\n");
	return 0;
    }

    TP->catch_msg("W skupieniu zaczynasz przywi�zywa� sznurek "
            +wedka[0]->short(TP, PL_DOP)+" do "+haczyk[0]->short(TP, PL_DOP)+".\n");

    /* Liczymy szanse i czas przywiazywania */
    int skill = TP->query_skill(SS_FISHING);
    float szansa, czas;

    // FIXME: Mozna by to zamienic na jaka tru-matematyczna funkcje
    // Ledwo
    if(skill >= 0 && skill < 10)
    {
	czas = 11.0;
	szansa = 0.5;
    }
    // Troche
    if(skill >= 10 && skill < 20)
    {
	czas = 10.0;
	szansa = 0.5;
    }
    // Pobieznie
    if(skill >= 20 && skill < 30)
    {
	czas = 9.0;
	szansa = 0.5;
    }
    // Zadowalajaco
    if(skill >= 30 && skill < 40)
    {
	czas = 8.0;
	szansa = 0.75;
    }
    // Niezle
    if(skill >= 40 && skill < 50)
    {
	czas = 7.0;
	szansa = 0.75;
    }
    // Dobrze
    if(skill >= 50 && skill < 60)
    {
	czas = 6.0;
	szansa = 0.75;
    }
    // Znakomicie
    if(skill >= 60 && skill < 70)
    {
	czas = 5.0;
	szansa = 0.9;
    }
    // Doskonale
    if(skill >= 70 && skill < 80)
    {
	czas = 4.0;
	szansa = 0.9;
    }
    // Perfekcyjnie
    if(skill >= 80 && skill < 90)
    {
	czas = 3.0;
	szansa = 0.95;
    }
    // Mistrzowsko
    if(skill >= 90)
    {
	czas = 2.0;
	szansa = 1.0;
    }
    
    int udalo_sie = PROB_RANDOM(([1:szansa, 0:(1.0-szansa)]));

    alarm_haczyk = set_alarm(czas, 0.0, "przywiaz_finish", udalo_sie, haczyk[0]);

    object paraliz = clone_object("/std/paralyze");
    paraliz->set_standard_paralyze("przywi�zywa� haczyk");
    paraliz->set_stop_fun("przywiaz_stop");
    paraliz->set_stop_verb("przesta�");
    paraliz->set_stop_object(TO);
    paraliz->set_stop_message("Przestajesz przywi�zywa� haczyk.\n");
    paraliz->set_remove_time(ftoi(czas));
    paraliz->set_fail_message("Skupiasz si� teraz na przywi�zaniu haczyka,"
	    +" je�li chcesz zrobi� co� innego 'przesta�'.\n");
    paraliz->move(this_player());

    return 1;
}

int
odwiaz(string str)
{
    notify_fail("Co od czego chcesz odwi�za�?\n");

    if(!str)
	    return 0;

    object *wedka;
    string str_haczyk;

    /* Zeby bylo(latwiej?) stosujemy 2 parse_command'y ;) */
    if(!parse_command(lower_case(str), TP, " %s 'od' %i:"+PL_DOP,
       str_haczyk, wedka))
	    return 0;

    wedka = NORMAL_ACCESS(wedka, 0, 0);

    if(!sizeof(wedka) || !wedka)
	    return 0;

    if(sizeof(wedka) > 1)
    {
	    notify_fail("Na raz mo�esz odwi�za� haczyk tylko od jednej w�dki!\n");
	    return 0;
    }

    if(wedka[0] != TO)
	    return 0;

    if(zarzucona)
    {
	    notify_fail("Najpierw lepiej by�oby wyci�gn�� j� z wody.\n");
	    return 0;
    }

    if(!str_haczyk)
        return 0;

    object haczyk;
    if(!parse_command(str_haczyk, all_inventory(wedka[0]), " %o:"+PL_BIE,
        haczyk))
	    return 0;

    if(!objectp(haczyk))
        return 0;

    if(sizeof(subinventory("_ryba")))
    {
	    notify_fail("Najpierw zdejmij z niego ryb�!\n");
	    return 0;
    }

    if(sizeof(subinventory("_przyneta")))
    {
	    notify_fail("Najpierw zdejmij z niego przyn�t�!\n");
	    return 0;
    }

    add_subloc_prop("_haczyk", CONT_I_CLOSED, 0);
    haczyk->move(TP);
    add_subloc_prop("_haczyk", CONT_I_CLOSED, 1);

    TP->catch_msg("Odwi�zujesz "+haczyk->short(PL_BIE)+" od sznurka "
	    +wedka[0]->short(PL_DOP)+".\n");

    saybb(QCIMIE(TP, PL_MIA)+" odwi�zuje "+haczyk->short(PL_BIE)
          +" od sznurka "+wedka[0]->short(PL_DOP)+".\n");

    return 1;
}

int
naloz(string str)
{
    if(query_verb() ~= "naloz")
	    notify_fail("Co na co chcesz na�o�y�?\n");
    else
	    notify_fail("Co na co chcesz za�o�y�?\n");

    if(!str)
	    return 0;

    object *przyneta, *wedka;
    if(!parse_command(lower_case(str), TP, " %i:"+PL_BIE+" 'na' %i:"+PL_BIE,
        przyneta, wedka))
	    return 0;

    przyneta = NORMAL_ACCESS(przyneta, 0, 0);
    wedka = NORMAL_ACCESS(wedka, 0, 0);

    if(!sizeof(wedka) || !wedka)
	    return 0;

    if(sizeof(wedka) > 1)
    {
	    notify_fail("Przyn�t� nak�ada si� tylko na jedn� w�dk� na raz!\n");
	    return 0;
    }

    if(wedka[0] != TO)
	    return 0;

    if(zarzucona)
    {
	    notify_fail("Najpierw wyci�gnij w�dk� z wody.\n");
	    return 0;
    }

    if(!sizeof(przyneta) || !przyneta)
	    return 0;

    if(sizeof(przyneta) > 1)
    {
	    notify_fail("Wystarczy je�li u�yjesz pojedy�czej przyn�ty.\n");
	    return 0;
    }

    if(!przyneta[0]->query_przyneta_typ())
    {
	    notify_fail("Nie mo�esz u�y� tego jako przyn�ty!\n");
	    return 0;
    }

    object *haczyk = wedka[0]->subinventory("_haczyk");
    if(!sizeof(haczyk))
    {
	    notify_fail("Najpierw postaraj si� o jaki� haczyk!\n");
	    return 0;
    }

    if(sizeof(subinventory("_przyneta")) > 0)
    {
	    write("Ale� na "+short(PL_MIE)+" jest ju� za�o�one przyn�ta!\n");
	    return 1;
    }

    if(member_array(przyneta[0]->query_przyneta_typ(),
       haczyk[0]->query_przynety()) == -1)
    {
	    write("Niestety na ten haczyk nie uda ci si� za�o�y� "
               +przyneta[0]->short(PL_DOP)+".\n");
	    return 1;
    }

    if(sizeof(subinventory("_ryba")))
    {
        write("Najpierw zdejmij zawieszon� na haczyku ryb�!\n");
        return 1;
    }

    add_subloc_prop("_przyneta", CONT_I_CLOSED, 0);
    przyneta[0]->move(TO, "_przyneta");
    add_subloc_prop("_przyneta", CONT_I_CLOSED, 1);


    TP->catch_msg("Starannie nak�adasz "+przyneta[0]->short(PL_BIE)+" na "
	    +haczyk[0]->short(PL_BIE)+" przywi�zany do sznurka "
	    +wedka[0]->short(PL_DOP)+".\n");

    saybb(QCIMIE(TP, PL_MIA)+" starannym ruchem nak�ada "
	    +przyneta[0]->short(PL_BIE)+" na "+haczyk[0]->short(PL_BIE)
	    +" przywi�zany do sznurka "+wedka[0]->short(PL_DOP)+".\n");

    return 1;
}

int
zarzuc(string str)
{
    notify_fail("Chyba raczej 'zarzu� w�dk� [gdzie]'?\n");
    if(!str)
	return 0;

    object *wedka;
    string gdzie;
    if(!parse_command(lower_case(str), TP, " %i:"+PL_BIE+" / %i:"+PL_BIE+" %s ", wedka, gdzie))
	return 0;

    wedka = NORMAL_ACCESS(wedka, 0, 0);

    if(!sizeof(wedka) || !wedka)
	return 0;

    if(sizeof(wedka) > 1)
    {
	notify_fail("Mo�esz zarzuci� tylko jedn� w�dk� na raz!\n");
	return 0;
    }

    if(wedka[0] != TO)
	return 0;

    if(zarzucona)
    {
	notify_fail("Ale� w�dka jest ju� zarzucona!\n");
	return 0;
    }

    if(query_subloc() != "wielded")
    {
	write("Najpierw j� chwy�!\n");
	return 1;
    }

    string *lowiska = ENV(TP)->query_lowiska();

    if(!sizeof(lowiska) || !lowiska)
    {
	notify_fail("Niestety, w pobli�u nie dostrzegasz �adnego miejsca nadaj�cego"
		+" si� do po�owu ryb.\n");
	return 0;
    }

    if(!gdzie || !strlen(gdzie))
    {
	if(sizeof(lowiska) == 1)
	    gdzie = lowiska[0];

	if(sizeof(lowiska) > 1)
	{
	    notify_fail("Jest tu kilka miejsc w kt�rych mo�na �owi�. Sprecyzuj w kt�rym"
		    +" z nich chcesz zarzuci� w�dke.\n");
	    return 0;
	}
    }

    if(member_array(gdzie, lowiska) == -1)
    {
	notify_fail("Nie mo�esz zarzuci� tam w�dki!\n");
	return 0;
    }
    
    throw_me(gdzie);

    return 1;
}

int wyciagnij(string str)
{
    notify_fail("Co takiego chcesz wyci�gn�� z wody?\n");

    if(!str)
	return 0;

    object *wedka;
    if(!parse_command(lower_case(str), TP, " %i:"+PL_BIE, wedka))
	return 0;

    wedka = NORMAL_ACCESS(wedka, 0, 0);

    if(!sizeof(wedka) || !wedka)
	return 0;

    if(sizeof(wedka) > 1)
    {
	notify_fail("Mo�esz wyci�gn�� tylko jedn� w�dk� na raz!\n");
	return 0;
    }

    if(wedka[0] != TO)
	return 0;

    if(!zarzucona)
    {
	notify_fail("Ale� w�dka nie jest nawet zarzucona!\n");
	return 0;
    }

    if(query_subloc() != "wielded")
    {
	write("Najpierw j� chwy�!\n");
	return 1;
    }

    take_me_out(TP);

    return 1;
}

int zatnij(string str)
{
    notify_fail("Co chcesz zaci��?\n");

    if(!str)
	return 0;

    object *wedka;
    if(!parse_command(lower_case(str), TP, " %i:"+PL_BIE, wedka))
	return 0;

    wedka = NORMAL_ACCESS(wedka, 0, 0);

    if(!sizeof(wedka) || !wedka)
	return 0;

    if(sizeof(wedka) > 1)
	return 0;

    if(wedka[0] != TO)
	return 0;

    if(!zarzucona)
    {
	notify_fail("Mo�e najpierw j� gdzie� zarzu�?\n");
	return 0;
    }

    if(query_subloc() != "wielded")
    {
	write("Najpierw j� chwy�!\n");
	return 1;
    }


    if(fake_branie)
    {
	TP->catch_msg("Gwa�townym ruchem zacinasz "+short(PL_BIE)
		+", a pusty haczyk wylatuje wysoko spod tafli wody i po chwili,"
		+" z cichym pluskiem, z powrotem si� w niej zanurza. Wygl�da na"
		+" to, �e by� to fa�szywy alarm.\n"); 
	saybb(QCIMIE(TP, PL_MIA)+" gwa�townym ruchem zacina "+short(PL_BIE)
		+", a pusty haczyk wylatuje wysoko spod tafli wody i po"
		+" chwili, z cichym pluskiem, z powrotem si� w niej zanurza.\n"); 

	fake_branie = 0;
	return 1;
    }

    if(alarm_niezacieta)
    {
	lowienie_zacieta();
	return 1;
    }

    TP->catch_msg("Gwa�townym ruchem zacinasz "+short(PL_BIE)
		+", a pusty haczyk wylatuje wysoko spod tafli wody i po"
		+" chwili, z cichym pluskiem, z powrotem si� w niej zanurza.\n"); 
    saybb(QCIMIE(TP, PL_MIA)+" gwa�townym ruchem zacina "+short(PL_BIE)
		+", a pusty haczyk wylatuje wysoko spod tafli wody i po"
		+" chwili, z cichym pluskiem, z powrotem si� w niej zanurza.\n"); 
    
    return 1;
}

int zdejmij(string str)
{
    notify_fail("Co z czego chcesz zdj��?\n");

    if(!str)
	return 0;

    string co;
    object *wedka;
    if(!parse_command(lower_case(str), TP, " %s 'z' %i:"+PL_DOP, co, wedka))
	return 0;

    wedka = NORMAL_ACCESS(wedka, 0, 0);

    if(!sizeof(wedka) || !wedka)
	    return 0;

    if(sizeof(wedka) > 1)
	    return 0;

    if(wedka[0] != TO)
	    return 0;

    if(zarzucona)
    {
	    notify_fail("Najpierw lepiej by�oby wyci�gn�� j� z wody.\n");
	    return 0;
    }

    object oco;
    if(!parse_command(co, all_inventory(TO), " %o:"+PL_BIE, oco))
	    return 0;

    if(!oco)
	    return 0;

    /* Zdejmujemy przynete */
    if(sizeof(subinventory("_przyneta")) > 0)
    {	
	    if(oco == subinventory("_przyneta")[0])
	    {
	        if(sizeof(subinventory("_ryba")) > 0)
	        {
		        write("Aby zdj�� "+oco->short(PL_BIE)+" musisz najpierw"
			    +" zdj�� "+subinventory("_ryba")[0]->short(PL_BIE)+".\n");
		        return 1;
	        }

	        write("Zdejmujesz "+oco->short(PL_BIE)+" z "+short(PL_DOP)+".\n");
	        saybb(QCIMIE(TP, PL_MIA)+" zdejmuje "+oco->short(PL_BIE)+" z "
		          +short(PL_DOP)+".\n");

            add_subloc_prop("_przyneta", CONT_I_CLOSED, 0);
	        oco->move(TP);
            add_subloc_prop("_przyneta", CONT_I_CLOSED, 1);

	        return 1;
	    }
    }

    /* Zdejmujemy rybe */
    if(sizeof(subinventory("_ryba")) > 0)
    {
	    if(oco == subinventory("_ryba")[0])
	    {
	        write("Zdejmujesz "+oco->short(PL_BIE)+" z "+short(PL_DOP)+".\n");
            add_subloc_prop("_ryba", CONT_I_CLOSED, 0);
	        oco->move(TP);
            add_subloc_prop("_ryba", CONT_I_CLOSED, 1);
	        return 1;
	    }
    }

    return 0;
}

void lowienie_zerwana()
{
    TP->increase_ss(SS_DEX, EXP_WEDKARSTWO_ZERWANA_DEX);
    TP->increase_ss(SS_FISHING, EXP_WEDKARSTWO_ZERWANA_FISHING);

    subinventory("_przyneta")->remove_object();

    if(ryba[3] == 1)
        subinventory("_haczyk")->remove_object();

    TP->catch_msg("Walcz�ca ryba nagle zrywa si� prostuj�c gwa�townie "
	   +short(PL_BIE)+".\n");
    saybb("Trzymana przez "+QIMIE(TP, PL_BIE)+" "+short(PL_MIA)
	    +" prostuje si� nagle. Najwyra�niej walcz�cej rybie uda�o"
	    +" si� zerwa�.\n");
}

void lowienie_zlowiona()
{
    TP->increase_ss(SS_DEX, EXP_WEDKARSTWO_ZLOWIONA_DEX);
    TP->increase_ss(SS_FISHING, EXP_WEDKARSTWO_ZLOWIONA_FISHING);

    object zlowiona = clone_object(ryba[0]);

    if(ryba[1])
	    zlowiona->add_prop(OBJ_I_WEIGHT, ryba[1]);

    add_subloc_prop("_ryba", CONT_I_CLOSED, 0);
    int ret = zlowiona->move(TO, "_ryba");
    DBG(ret);
    add_subloc_prop("_ryba", CONT_I_CLOSED, 1);

    take_me_out(TP);

    TP->catch_msg("Nagle sznurek napina si� mocno. Zaciskasz d�onie"
            +" na w�dce i powoli wyci�gasz sw� zdobycz na powierzchni�."
            +" Po chwili twoim oczom ukazuje si� dyndaj�ca na ko�cu sznurka "
	        +zlowiona->short(PL_MIA)+".\n");

    saybb("Nagle sznurek trzymanej przez "+QIMIE(TP, PL_BIE)+" w�dki"
	        +" napina si� mocno, "+TP->koncowka("ten", "ta")+" jednak"
            +" nie daje za wygran�. Po chwili z wody wynurza si� dyndaj�ca"
	        +" na ko�cu sznurka "+zlowiona->short(PL_MIA)+".\n");
}

int lowienie_stop()
{
    ryba = 0;
    saybb(QCIMIE(this_player(), PL_MIA) + " zaprzestaje walki z ryb�.\n");

    return 0;
}

void lowienie_zacieta()
{
    remove_alarm(alarm_niezacieta);
    alarm_niezacieta = 0;
	
    TP->catch_msg("Gwa�townym ruchem zacinasz "+short(PL_BIE)
		+" wyczuwaj�c mocny op�r zaci�tej ryby.\n");
    saybb(QCIMIE(TP, PL_MIA)+" gwa�townym ruchem zacina "+short(PL_BIE)+".\n");
	
    subinventory("_przyneta")->remove_object();

    object paraliz = clone_object("/std/paralyze");
    paraliz->set_standard_paralyze("�owi�");

    paraliz->set_stop_fun("lowienie_stop");
    paraliz->set_stop_verb("przesta�");
    paraliz->set_stop_object(TO);
    paraliz->set_stop_message("Poddajesz walk� z ryb�, po kr�tkim czasie"
			    +" tej udaje si� najwyra�niej uwolni�.\n");
    paraliz->set_fail_message("W tej chwili walczysz w ryb�! Je�li chcesz"
			    +" zrobi� co� innego mo�esz 'przesta�'. Jednak"
			    +" wtedy po�egnasz si� r�wnie� ze swoj� zdobycz�.\n");

    paraliz->set_event_time(15);

    paraliz->add_event("Sznurek "+short(PL_DOP)+" na przemian, regularnie napina si�"
			+" i opada.", 
		       "Sznurek "+short(PL_DOP)+" trzymanej przez "+QIMIE(TP, PL_BIE)
			+" na przemian, regularnie napina si� i opada.");
    paraliz->add_event("Ko�c�wka "+short(PL_DOP)+" nieustannie dr�y, przekrzywiaj�c"
			+" si� raz w lewo, a raz w prawo.",
			"Ko�c�wka "+short(PL_DOP)+" trzymanej przez "+QIMIE(TP, PL_BIE)
			+" nieustannie dr�y, przekrzywiaj�c si� raz w lewo, a raz"
			+" w prawo.");
    paraliz->add_event("Sznurek "+short(PL_DOP)+" napina si� mocno, zmuszaj�c ci� do"
			+" zaci�tej walki.");

    /* Czy ryba sie zerwie */
    if(ryba[3])
    {
	paraliz->set_remove_time(ryba[2]/(random(2)+2)+1);
	paraliz->set_finish_fun("lowienie_zerwana");
	paraliz->set_finish_object(TO);
    }
    else
    {
	paraliz->set_remove_time(ryba[2]+1);
	paraliz->set_finish_fun("lowienie_zlowiona");
	paraliz->set_finish_object(TO);

    }

    paraliz->move(TP);
}

void branie_niezacieta()
{
	TP->catch_msg("Sznurek "+short(PL_DOP)+" jeszcze przez chwil� porusza si� niespokojnie,"
		+" po czym powoli opada na wod�.\n");
	saybb("Sznurek trzymanej przez "+QIMIE(TP, PL_BIE)+" "+short(PL_DOP)+" przez jeszcze"
		+" przez chwil� porusza si� niespokojnie, po czym powoli opada na wod�.\n");

	subinventory("_przyneta")->remove_object();
	alarm_niezacieta = 0;
}

void branie()
{
    string rodzaj_brania;

    /* Fake-branie */
    if(random(101) > 93)
    {
		fake_branie = 1;
		rodzaj_brania = "uderzenie";

		alarm_zarzucona = set_alarm(itof(random(CZAS_RAND))+CZAS_MIN, 0.0, "branie");
    }
    /* Tru-branie :P */
    else
    {
	/* Losujemy rybe */
		ryba = ENV(TP)->fish(lowisko, TP->query_skill(115),
				    		query_przyneta(), gietkosc);

		if(!ryba)
		{
	    	write("Wyst�pi� nieziemsko powa�ny b��d! Dobrze by�oby go zg�osi�.\n");
            return;
		}

		rodzaj_brania = ryba[4];
		alarm_niezacieta = set_alarm(4.0+itof(random(4)), 0.0, "branie_niezacieta");	
    }

    switch(rodzaj_brania)
    {
    	case "trykanie":
			TP->catch_msg("Ko�c�wka "+short(PL_DOP)+" zaczyna drga� szybko"
						+" naci�gana przez sznurek.\n");
			saybb("Ko�c�wka "+short(PL_DOP)+" trzymanej przez "+QIMIE(TP, PL_DOP)
						+" zaczyna drga� szybko naci�gana przez sznurek.\n");
			break;

    	case "zgiecie":
			TP->catch_msg("Ko�c�wka "+short(PL_DOP)+" nagle zgina sie mocno"
						+" naci�gni�ta przez sznurek.\n");
			saybb("Ko�c�wka "+short(PL_DOP)+" trzymanej przez "+QIMIE(TP, PL_DOP)
						+" nagle zgina sie mocno naci�gni�ta przez sznurek.\n");
			break;

    	case "uderzenie":
			TP->catch_msg("Ko�c�wka "+short(PL_DOP)+" momentalnie zgina si� w �uk,"
						+" by po chwili gwa�townie wyprostowa� si�.\n");
			saybb("Ko�c�wka "+short(PL_DOP)+" trzymanej przez "+QIMIE(TP, PL_DOP)
						+" momentalnie zgina si� w �uk, by po chwili gwa�townie"
						+" wyprostowa� si�.\n");
			break;

    	default:
			TP->catch_msg("Wyst�pi� niebotyczny b��d! Zg�o� go natychmiast!\n");
			take_me_out(TP);
			break;
    } 
}

void throw_me(string gdzie)
{
    zarzucona = 1;
    lowisko = gdzie;
    obj_przym[0] = ({"zarzucony"})+obj_przym[0];
    obj_przym[1] = ({"zarzuceni"})+obj_przym[1];
    odmien_short();
    odmien_plural_short();
	
    if(query_przyneta())
    	alarm_zarzucona = set_alarm(itof(random(CZAS_RAND))+CZAS_MIN, 0.0, "branie");
	
    TP->catch_msg("Wykonujesz niewielki zamach i zarzucasz w�dk� "+gdzie+"."
            +" Sznurek zanurza si� w wodzie zakre�laj�c na niej kilka kr�g�w.\n");

    saybb(QCIMIE(TP, PL_MIA)+" wykonuje niewielki zamach i zarzuca w�dk� "+gdzie+".\n");
}

void take_me_out(object kto)
{
    
    if(!zarzucona)
		return;

    remove_alarm(alarm_zarzucona);
    remove_alarm(alarm_niezacieta);
    alarm_zarzucona = 0;
    alarm_niezacieta = 0;
    zarzucona = 0;
    lowisko = 0;   
    remove_adj("zarzucony");
    odmien_short();
    odmien_plural_short();

    kto->catch_msg("Wyci�gasz "+short(PL_BIE)+" z wody.\n");
    saybb(QCIMIE(kto, PL_MIA)+" wyci�ga "+short(PL_BIE)+" z wody.\n");
}


void set_gietkosc(float f)
{
    gietkosc = f;
}

float query_gietkosc()
{
    return gietkosc;
}

string query_przyneta()
{
    object *przyneta = subinventory("_przyneta");

    if(!sizeof(przyneta))
		return 0;

    return przyneta[0]->query_przyneta_typ();
}

public int
pomoc(string str)
{                                                                              
    object pohf;

    notify_fail("Nie ma pomocy na ten temat.\n");

    if (!str) 
		return 0;

    if (!parse_command(str, this_player(), "%o:" + PL_MIA, pohf))
        return 0;

    if (pohf != this_object())
        return 0;

    write("Z w�dk� mo�na zrobi� wiele ciekawych rzeczy. Pomijaj�c"
	+" te bardziej nowatorskie zastosowania i skupiaj�c si� na"
	+" tych codziennych mamy:\n\n"
	+"\t * przywi�� <co�> do w�dki (np. haczyk)\n"
	+"\t * odwi�� <co�> od w�dki (np. haczyk)\n"
	+"\t * za�� <co�> na w�dk� (np. przyn�t�)\n"
	+"\t * zdejmij <co�> z w�dki (np. przyn�t� lub ryb�)\n"
	+"\t * zarzuc w�dk� (np. w rzece)\n"
	+"\t * wyci�gnij w�dk�\n"
	+"\t * zatnij w�dk�\n\n"
	+"Je�li masz jaki� ciekawy pomys� odno�nie w�dki - nie wahaj si�!"
	+" Zg�o� go ju� dzi�, setki w�dek mog� by� Tobie wdzi�czne!\n");

    return 1;
}

int query_is_wedka()
{
    return 1;
}



