inherit "/std/object";

#include <stdproperties.h>

void
create_object()
{
	ustaw_nazwe("rybka");

	dodaj_przym("ma�y", "mali");
	dodaj_przym("b�yszcz�cy", "b�yszcz�cy");

    set_long("Malutkich rozmiar^ow b^lyszcz^aca rybka napewno bedzie "+
		 "stanowi^c smakowit^a przyn^et^e dla wi^ekszych okaz^ow. Spogl^ada "+
		 "na ciebie smutnymi martwymi oczami, rozwieraj^ac maly pyszczek w "+
		 "wyrazie bezwzgl^ednego zdziwienia. \n");

	add_prop(OBJ_I_WEIGHT, 12);
	add_prop(OBJ_I_VOLUME, 7);
	add_prop(OBJ_I_VALUE, 0);
}

string
query_przyneta_typ()
{
    return "rybka";
}

