/* Std procy poczynione przez Aldeima */

inherit "/d/Aretuza/aldeim/strzeleckie/strzelecka";
#include <stdproperties.h>
#include <macros.h>

int jakosc;

public void
create_proca()
{
}

int
query_jakosc()
{
  return jakosc;
}

void
ustaw_jakosc(int x)
{
  jakosc = x;
  if (x < 1) ustaw_jakosc(1);
  if (x > 10) ustaw_jakosc(10);

  /* Zakres cech procy:
     celnosc: 10 - 100
     szybkosc: 10 - 15
     sila:     3 - 30
   */

  set_celnosc(jakosc*10);
  set_sila(jakosc*3);
  set_szybkosc(10 + jakosc/2);
}

nomask void
create_strzelecka()
{
  ustaw_nazwe( ({"proca", "procy", "procy", "proc^e", "proc^a", "procy"}),
    ({"proce", "proc", "procom", "proce", "procami", "procach"}), PL_ZENSKI);
  set_long("Najzwyczajniejsza w ^swiecie proca.\n");
  set_zasieg(0);
  set_min_sila(1);
  set_pocisk("kamie^n");
  create_proca();
}

public void
przygotowanie(object kto, object bron, object strzala)
{
  set_this_player(kto);
  write("Wk^ladasz " + strzala->query_short(PL_BIE) + " do "+
    "miseczki " + bron->query_short(PL_DOP) + ", po czym "+
    "zaczynasz energicznie kr^eci^c ni^a nad g^lowa.\n");
  saybb(QCIMIE(this_player(), PL_MIA) + " wk^lada " +
    strzala->query_short(PL_BIE) + " do "+
    "miseczki " + bron->query_short(PL_DOP) + ", po czym "+
    "zaczyna energicznie kr^eci^c ni^a nad g^lowa.\n");
}