/**
 * Plik z funkcjami u�ywanymi do opisu zio�a
 *
 * Autor: Krun
 * Anno Domini 2007
 */

#include <pl.h>
#include <files.h>
#include <macros.h>

int     *id_rodz_cal = ({}),          //Rodzaj nazwy ca�ego zidentyfikowanego zio�a
        *unid_rodz_cal = ({}),        //Rodzaj nazwy ca�ego niezidentyfikowanego zio�a
        *rodz_zident = ({}),          //Rdzaj nazwy zidentyfikowanego zio�a
         id_rodz_shorta_cal,          //Rodzaj shorta calego ziola zidentyfikowanego
         unid_rodz_shorta_cal,        //Rodzaj shorta rosn�cego zio�� niezidentyfikowanego
         rodz_shorta_zident,          //Rodzaj shorta zio�a zidentyfikowanego
         id_rodz_ng_cal,              //Rodzaj nazwy g��wnej ca�ego, zidentyfikowanego zio�a
         unid_rodz_ng_cal,            //Rodzaj nazwy g��wnej ca�ego, niezidentyfikowanego zio�a
         rodz_ng_zident,              //Rodzaj nazwy g��wnej zidentyfikowanego zio�a
         id_ng_cal_tylko_mn,          //Czy nazwa g��wna ca�ego, zidentyfikowanego zio�a jest tylko w liczbie mnogiej
         unid_ng_cal_tylko_mn,        //Czy nazwa g��wna ca�ego, niezidentyfikowanego zio�a jest tylko w liczbie mnogiej
         ng_zident_tylko_mn,          //Czy nazwa g��wna zidentfikowanego zio�a jest tylko w liczbie mnogiej.
        *id_nazwa_cal_tylko_mn = ({}),//Czy nazwa zio�a ca�ego, zidentyfikowanego jest tylko w liczbie mnogiej.
        *unid_nazwa_cal_tylko_mn = ({}),//Czy nazwa zio�a ca�ego, niezidentyfikowanego jest tylko w liczbie mnogiej.
        *nazwa_zident_tylko_mn = ({}),//Czy nazwa zio�a zidentyfikowanego jest tylko w liczbie mnogiej.
         id_short_cal_tylko_mn,       //Czy short zio�a ca�ego, zidentyfikowanego jest tylko w liczbie mnogiej.
         unid_short_cal_tylko_mn,     //Czy short zio�a ca�ego, niezidentyfikowanego jest tylko w liczbie mnogiej.
         short_zident_tylko_mn;       //Czy short zio�a zidentyfikowanego jest tylko w liczbie mnogiej.

string   id_long,                     //D�ugi opis zidentyfikowanego zio�a
         unid_long,                   //D�ugi opis niezidentyfikowanego zio�a
         decay_mess,                  //Wiadomo�� wy�wietlana graczowi w momencie kiedy zio�o usycha
         total_decay_mess,            //Wiadomo�� pokazywana graczowi w momencie kiedy zio�o rozsypuje si�
        *decay_adj,                   //Przymiotnik wyschni�tego zio�a.
        *pshort_zident,               //Shorty zidentyfikowanego zio�a w liczbie pojedynczej
        *mshort_zident,               //Shorty zidentyfikowanego zio�a w liczbie mnogiej
        *id_pshort_cal,               //Shorty ca�ego, zidentyfikowanego zio�a w liczbie pojedynczej
        *id_mshort_cal,               //Shorty ca�ego, zidentyfikowanego zio�a w liczbie mnogiej
        *unid_pshort_cal,             //Shorty ca�ego, niezidentyfikowanego zio�a w liczbie pojedynczej
        *unid_mshort_cal,             //Shorty ca�ego, niezidentyfikowanego zio�a w liczbie mnogiej
        *png_zident,                  //Nazwa g��wna zidentyfikowanego zio�a w liczbie pojedynczej
        *mng_zident,                  //Nazwa g��wna zidentyfikowanego zio�a w liczbie mnogiej
        *id_png_cal,                  //Nazwa g��wna ca�ego zidentyfikowanego zio�a w liczbie pojedynczej
        *id_mng_cal,                  // --||-- w liczbie mnogiej
        *unid_png_cal,                // --||-- niezidentyfikowanego zio�a w liczbie pojedynczej
        *unid_mng_cal;                // --||-- w liczbie mnogiej

mixed   *pnazwa_zident = ({}),        //Liczba pojedyncza nazwy zidentyfikowanego zio�a
        *mnazwa_zident = ({}),        //Liczba mnoga nazwy zidentyfikowanego zio�a
        *id_przym_cal=allocate(2),    //Przymiotniki ca�ego, zidentyfikowanego zio�a
        *unid_przym_cal=allocate(2),  //Przymiotniki ca�ego, niezidentyfikowanego zio�a
        *przym_zident=allocate(2),    //Przymiotniki zidentyfikowanego zio�a
         slowo_grupy,                 //S�owo u�ywane zamiast wiele w rosn�cych zio�ach.
        *id_pnazwa_cal = ({}),        //Nazwa ca�ego, zidentyfikowanego zio�a w liczbie pojedynczej
        *id_mnazwa_cal = ({}),        //Nazwa ca�ego, zidentyfikowanego zio�a w liczbie mnogiej
        *unid_pnazwa_cal = ({}),      //Nazwa ca�ego, niezidentyfikowanego zio�a w liczbie pojedynczej.
        *unid_mnazwa_cal = ({});      //Nazwa ca�ego, niezidentyfikowanego zio�a w liczbie mnogiej


#define SLOWNIK_ERR(x) {throw("Nazwy '" + x + "' nie ma w s�owniku.\n"); return 0;}
#define ZLA_ILOSC_ODMIAN(x)  {throw("Z�a odmiana w liczbie pojedynczej w funkcji " + x + ".\n"); return 0;}
#define ZLA_ILOSC_ODMIAN1(x) {throw("Z�a odmiana w liczbie mnogiej w funkcji " + x + ".\n"); return 0;}
#define ZLA_ILOSC_ODMIAN2(x) ; /*{throw("Nie podano rodzaju w funkcji " + x + ".\n"); return 0;}*/

public varargs string short(mixed fo, mixed przyp);
public varargs string plural_short(mixed fo, int przyp);
public varargs int query_rodzaj(object fo, int flag);
public varargs int query_tylko_mn(object fo, int flag);
public varargs int dodaj_nazwy_ziola(mixed pname, mixed mname, int rodz);
public varargs int dodaj_id_nazwy_calosci(mixed pname, mixed mname, int rodz);
public varargs int dodaj_unid_nazwy_calosci(mixed pname, mixed mname, int rodz);

/**
 * Funkcja pomocnicza.
 */
private static int
nazwa_juz_jest(string nazwa, mixed nazwy)
{
    int i;
    for(i=0;i<sizeof(nazwy);i++)
    {
        if(nazwy[i][0] == nazwa)
            return i;
    }
    return -1;
}

//NAZWY-----------------------------------------------------------------------

//ustaw_*
//dodaj_*
public varargs void
ustaw_nazwe_glowna_ziola(mixed pname, mixed mname, int rodz)
{
    int tylko_mn;

    if(stringp(pname))
    {
        mixed odmiana = slownik_pobierz(pname);

        pname = odmiana[0];

        if(sizeof(odmiana) == 2)
        {
            rodz = odmiana[1];
            tylko_mn = 1;
        }
        else
        {
            rodz = odmiana[2];
            mname = odmiana[1];
        }
    }
    else if(!pointerp(pname))
        ZLA_ILOSC_ODMIAN("ustaw_nazwe_glowna_ziola");

    if(intp(mname))
    {
        tylko_mn = 1;
        rodz = mname;
    }

    if(sizeof(pname) != 6)
        ZLA_ILOSC_ODMIAN("ustaw_nazwe_glowna_ziola")
    if(pointerp(mname))
        if(sizeof(mname) != 6)
            ZLA_ILOSC_ODMIAN1("ustaw_nazwe_glowna_ziola")
        else if(tylko_mn)
           ZLA_ILOSC_ODMIAN2("ustaw_nazwe_glowna_ziola")

    dodaj_nazwy_ziola(pname, mname, rodz);

    png_zident = pname;
    mng_zident = mname;
    ng_zident_tylko_mn = tylko_mn;
    rodz_ng_zident = rodz;
}

public varargs  void
ustaw_id_nazwe_glowna_calosci(mixed pname, mixed mname, int rodz)
{
    int tylko_mn;

    if(stringp(pname))
    {
        mixed odmiana = slownik_pobierz(pname);

        pname = odmiana[0];

        if(sizeof(odmiana) == 2)
        {
            rodz = odmiana[1];
            tylko_mn = 1;
        }
        else
        {
            rodz = odmiana[2];
            mname = odmiana[1];
        }
    }
    else if(!pointerp(pname))
        ZLA_ILOSC_ODMIAN("ustaw_id_nazwe_glowna_calosci");

    if(intp(mname))
    {
        rodz = mname;
        tylko_mn = 1;
    }

    if(sizeof(pname) != 6)
        ZLA_ILOSC_ODMIAN("ustaw_id_nazwe_glowna_calosci");
    if(pointerp(mname))
        if(sizeof(mname) != 6)
            ZLA_ILOSC_ODMIAN1("ustaw_id_nazwe_glowna_calosci")
        else if(!tylko_mn)
            ZLA_ILOSC_ODMIAN2("ustaw_id_nazwe_glowna_calosci")

    dodaj_id_nazwy_calosci(pname, mname, rodz);

    id_png_cal = pname;
    id_mng_cal = mname;
    id_ng_cal_tylko_mn = tylko_mn;
    id_rodz_ng_cal = rodz;
}

public varargs void
ustaw_unid_nazwe_glowna_calosci(mixed pname, mixed mname, int rodz)
{
    int tylko_mn;

    if(stringp(pname))
    {
        mixed odmiana = slownik_pobierz(pname);

        pname = odmiana[0];

        if(sizeof(odmiana) == 2)
        {
            rodz = odmiana[1];
            tylko_mn = 1;
        }
        else
        {
            rodz = odmiana[2];
            mname = odmiana[1];
        }
    }
    else if(!pointerp(pname))
        ZLA_ILOSC_ODMIAN("ustaw_unid_nazwe_glowna_calosci");

    if(intp(mname))
    {
        tylko_mn = 1;
        rodz = mname;
    }

    if(sizeof(pname) != 6)
        ZLA_ILOSC_ODMIAN("ustaw_unid_nazwe_glowna_calosci");
    if(pointerp(mname))
        if(sizeof(mname) != 6)
            ZLA_ILOSC_ODMIAN1("ustaw_unid_nazwe_glowna_calosci")
        else if(!tylko_mn)
            ZLA_ILOSC_ODMIAN2("ustaw_unid_nazwe_glowna_calosci")


    dodaj_unid_nazwy_calosci(pname, mname, rodz);

    unid_png_cal  = pname;
    unid_mng_cal  = mname;
    unid_ng_cal_tylko_mn = tylko_mn;
    unid_rodz_ng_cal = rodz;
}

/**
 * Funkcja ustawia prawdziwa nazwe ziola widoczna tylko dla graczy ktorzy
 * potrafia ziolo zidentyfikowa�
 */
public varargs int
ustaw_nazwe_ziola(mixed pname, mixed mname, int rodz)
{
    int tylko_mn;

    if(stringp(pname))
    {
        mixed odmiana = slownik_pobierz(pname);

        pname = odmiana[0];

        if(sizeof(odmiana) == 2)
        {
            rodz = odmiana[1];
            tylko_mn = 1;
        }
        else
        {
            rodz = odmiana[2];
            mname = odmiana[1];
        }

    }
    else if(!pointerp(pname))
        ZLA_ILOSC_ODMIAN("ustaw_nazwe_ziola");

    if(intp(mname))
    {
        rodz = mname;
        tylko_mn = 1;
    }

    if(sizeof(pname) != 6)
            ZLA_ILOSC_ODMIAN("ustaw_nazwe_ziola");
    if(pointerp(mname))
        if(sizeof(mname) != 6)
            ZLA_ILOSC_ODMIAN1("ustaw_nazwe_ziola")
        else if(tylko_mn)
            ZLA_ILOSC_ODMIAN2("ustaw_nazwe_ziola")

    int i;
    if((i=nazwa_juz_jest(pname, pnazwa_zident)) >= 0)
    {
        pnazwa_zident = exclude_array(pnazwa_zident, i, i);
        mnazwa_zident = exclude_array(mnazwa_zident, i, i);
        nazwa_zident_tylko_mn = exclude_array(nazwa_zident_tylko_mn, i, i);
        rodz_zident   = exclude_array(rodz_zident, i, i);
    }

    pnazwa_zident = ({ pname }) + pnazwa_zident;
    mnazwa_zident = ({ mname }) + mnazwa_zident;
    nazwa_zident_tylko_mn = ({tylko_mn}) + nazwa_zident_tylko_mn;
    rodz_zident   = ({ rodz }) + rodz_zident;

    return 1;
}

/**
 * Alias do ustaw_nazwe_ziola
 */
public varargs int
ustaw_nazwe_zident(mixed pname, string *mname, int rodz)
{
    return ustaw_nazwe_ziola(pname, mname, rodz);
}

/**
 * Ustawia nazwe pod jaki� zio�o b�dzie pokazywane graczowi kt�ry potrafi
 * to zio�o zidentyfikowa�. Opis widoczny jedynie w momencie kiedy zio�o
 * w ca�o�ci znajduje sie na lokacji. Wystarczy poda� mianownik, reszta
 * mo�e by� pobrana ze s�ownika.
 * @param pname Pe�na odmiana anzwy w liczbie pojedynczej.
 * @param mname Pe�na odmiana nazwy w liczbie mnogiej.
 * @param rodz Rodzaj nazwy ca�ego, zidentyfikowanego zio�a.
 * @return 1 w przypadku sukcesu.
 */
public varargs  int
ustaw_id_nazwe_calosci(mixed pname, mixed mname, int rodz)
{
    int tylko_mn;

    if(stringp(pname))
    {
        mixed odmiana = slownik_pobierz(pname);

        pname = odmiana[0];

        if(sizeof(odmiana) == 2)
        {
            rodz = odmiana[1];
            tylko_mn = 1;
        }
        else
        {
            rodz = odmiana[2];
            mname = odmiana[1];
        }
    }
    else if(!pointerp(pname))
        ZLA_ILOSC_ODMIAN("ustwa_id_nazwe_calosci");

    if(intp(mname))
    {
        rodz = mname;
        tylko_mn = 1;
    }

    if(sizeof(pname) != 6)
        ZLA_ILOSC_ODMIAN("ustwa_id_nazwe_calosci")
    if(pointerp(mname))
        if(sizeof(mname) != 6)
            ZLA_ILOSC_ODMIAN1("ustwa_id_nazwe_calosci")
        else if(!tylko_mn)
            ZLA_ILOSC_ODMIAN2("ustwa_id_nazwe_calosci")

    int i;
    mixed old_pname = pname;
    if((i = nazwa_juz_jest(pname, unid_pnazwa_cal)) >= 0)
    {
        id_pnazwa_cal = exclude_array(id_pnazwa_cal, i, i);
        id_mnazwa_cal = exclude_array(id_mnazwa_cal, i, i);
        id_rodz_cal = exclude_array(id_rodz_cal, i, i);
    }
    pname = old_pname;
    id_pnazwa_cal = ({ pname }) + id_pnazwa_cal;
    id_mnazwa_cal = ({ mname }) + id_mnazwa_cal;
    id_nazwa_cal_tylko_mn = ({ tylko_mn }) + id_nazwa_cal_tylko_mn;
    id_rodz_cal = ({ rodz }) + id_rodz_cal;

    return 1;
}

/**
 * Ustawia opis pod jakim zio�o b�dzie pokazywane, graczowi kt�ry zio�a nie
 * potrafi zidentyfikowac. Opis ten wyswietlan jets tylko w momencie kiedy
 * zio�o jest w ca�o�ci na lokacji. Wystarczy poda� tylko pierwszt argument,
 * w takim przypadku pozosta�e zostan� pobrane ze s�ownika.
 * @param pname Pe�na odmiana nazwy w liczbie pojedynczej
 * @param mname Pe�na odmiana nazwy w liczbie mnogiej.
 * @param rodz Rodzaj nazwy ca�ego, niezidentyfikowanego zio�a.
 * @return 1 w przypadku sukcesu.
 */
public varargs int
ustaw_unid_nazwe_calosci(mixed pname, mixed mname, int rodz)
{
    int tylko_mn;

    if(stringp(pname))
    {
        mixed odmiana = slownik_pobierz(pname);

        pname = odmiana[0];

        if(sizeof(odmiana) == 2)
        {
            rodz = odmiana[1];
            tylko_mn = 1;
        }
        else
        {
            rodz = odmiana[2];
            mname = odmiana[1];
        }
    }
    else if(!pointerp(pname))
        ZLA_ILOSC_ODMIAN("ustaw_unid_nazwe_calosci");
    if(intp(mname))
    {
        rodz = mname;
        tylko_mn = 1;
    }

    if(sizeof(pname) != 6)
        ZLA_ILOSC_ODMIAN("ustaw_unid_nazwe_calosci");
    if(pointerp(mname))
        if(sizeof(mname) != 6)
            ZLA_ILOSC_ODMIAN1("ustaw_unid_nazwe_calosci")
        else if(!tylko_mn)
            ZLA_ILOSC_ODMIAN2("ustaw_unid_nazwe_calosci")

    int i;
    if((i = nazwa_juz_jest(pname, unid_pnazwa_cal)) >= 0)
    {
        unid_pnazwa_cal = exclude_array(unid_pnazwa_cal, i, i);
        unid_mnazwa_cal = exclude_array(unid_mnazwa_cal, i, i);
        unid_rodz_cal = exclude_array(unid_rodz_cal, i, i);
    }

    unid_pnazwa_cal = ({ pname }) + unid_pnazwa_cal;
    unid_mnazwa_cal = ({ mname }) + unid_mnazwa_cal;
    unid_nazwa_cal_tylko_mn = ({ tylko_mn }) + unid_nazwa_cal_tylko_mn;
    unid_rodz_cal = unid_rodz_cal + ({ rodz });
    return 1;
}

varargs public int
dodaj_nazwy_ziola(mixed pname, mixed mname, int rodz)
{
    int tylko_mn;

    if(stringp(pname))
    {
        mixed odmiana = slownik_pobierz(pname);

        pname = odmiana[0];

        if(sizeof(odmiana) == 2)
        {
            rodz = odmiana[1];
            tylko_mn = 1;
        }
        else
        {
            rodz = odmiana[2];
            mname = odmiana[1];
        }
    }
    else if(!pointerp(pname))
        ZLA_ILOSC_ODMIAN("dodaj_nazwy_ziola");

    if(intp(mname))
    {
        tylko_mn = 1;
        rodz = mname;
    }

    if(sizeof(pname) != 6)
        ZLA_ILOSC_ODMIAN("dodaj_nazwy_ziola");
    if(pointerp(mname))
        if(sizeof(mname) != 6)
            ZLA_ILOSC_ODMIAN1("dodaj_nazwy_ziola")
        else if(!tylko_mn)
            ZLA_ILOSC_ODMIAN2("dodaj_nazwy_ziola")

    if(nazwa_juz_jest(pname[0], pnazwa_zident) >= 0)
        return 2;

    pnazwa_zident += ({pname});
    mnazwa_zident += ({mname});
    nazwa_zident_tylko_mn += ({tylko_mn});
    rodz_zident += ({rodz});

    return 1;
}

/**
 * Dodaje nazwe ca�ego, zidentyfikowanego zio�a.
 * @param pname Pe�na odmiana w liczbie pojedynczej
 * @param mname Pe�na odmiana w liczbie mnogiej.
 * @param rodz Rodzaj nazwy.
 * @return 1 w przypadku sukcesu, 2 gdy nazwa ju� jest
 */
public varargs int
dodaj_id_nazwy_calosci(mixed pname, mixed mname, int rodz)
{
    int tylko_mn;

    if(stringp(pname))
    {
        mixed odmiana = slownik_pobierz(pname);

        pname = odmiana[0];

        if(sizeof(odmiana) == 2)
        {
            rodz = odmiana[1];
            tylko_mn = 1;
        }
        else
        {
            rodz = odmiana[2];
            mname = odmiana[1];
        }
    }
    else if(!pointerp(pname))
        ZLA_ILOSC_ODMIAN("dodaj_id_nazwy_calosci");

    if(intp(mname))
    {
        rodz = mname;
        tylko_mn = 1;
    }
    if(sizeof(pname) != 6)
        ZLA_ILOSC_ODMIAN("dodaj_id_nazwy_calosci");
    if(pointerp(mname))
        if(sizeof(mname) != 6)
            ZLA_ILOSC_ODMIAN1("dodaj_id_nazwy_calosci")
        else if(!tylko_mn)
            ZLA_ILOSC_ODMIAN2("dodaj_id_nazwy_calosci")

    if((nazwa_juz_jest(pname, unid_pnazwa_cal)) >= 0)
        return 2;

    id_pnazwa_cal += ({ pname });
    id_mnazwa_cal += ({ mname });
    id_nazwa_cal_tylko_mn += ({ tylko_mn });
    id_rodz_cal += ({ rodz });

    return 1;
}

/**
 * Dodaje nazwe ca�ego, niezidentyfikowanego zio�a
 * @param pname Pe�na odmiana w liczbie pojedynczej
 * @param mname Pe�na odmiana w liczbie mnogiej.
 * @param rodz Rodzaj nazwy.
 * @return 1 w przypadku powodzenia, 2 je�li nazwa ju� jest.
 */
public varargs int
dodaj_unid_nazwy_calosci(mixed pname, mixed mname, int rodz)
{
    int tylko_mn;

    if(stringp(pname))
    {
        mixed odmiana = slownik_pobierz(pname);

        pname = odmiana[0];

        if(sizeof(odmiana) == 2)
        {
            rodz = odmiana[1];
            tylko_mn = 1;
        }
        else
        {
            rodz = odmiana[2];
            mname = odmiana[1];
        }
    }
    else if(!pointerp(pname))
        ZLA_ILOSC_ODMIAN("dodaj_unid_nazwy_calosci");

    if(intp(mname))
    {
        rodz = mname;
        tylko_mn = 1;
    }

    if(sizeof(pname) != 6)
        ZLA_ILOSC_ODMIAN("dodaj_unid_nazwy_calosci");
    if(pointerp(mname))
        if(sizeof(mname) != 6)
            ZLA_ILOSC_ODMIAN1("dodaj_unid_nazwy_calosci")
        else if(!tylko_mn)
            ZLA_ILOSC_ODMIAN2("dodaj_unid_nazwy_calosci")

    if(nazwa_juz_jest(pname, unid_pnazwa_cal) >= 0)
        return 2;

    unid_pnazwa_cal += ({ pname });
    unid_mnazwa_cal += ({ mname });
    unid_nazwa_cal_tylko_mn += ({ tylko_mn });
    unid_rodz_cal += ({ rodz });

    return 1;
}
public varargs int
ustaw_short_ziola(mixed pshort, mixed mshort, int rodz)
{
    int tylko_mn;

    if(stringp(pshort))
    {
        mixed odmiana = slownik_pobierz(pshort);

        pshort = odmiana[0];

        if(sizeof(odmiana) == 2)
        {
            rodz = odmiana[1];
            mshort = pshort;
            tylko_mn = 1;
        }
        else
        {
            rodz = odmiana[2];
            mshort = odmiana[1];
        }
    }
    else if(!pointerp(pshort))
        ZLA_ILOSC_ODMIAN("ustwa_short_ziola");

    if(intp(mshort))
    {
        tylko_mn = 1;
        rodz = mshort;
    }

    if(sizeof(pshort) != 6)
        ZLA_ILOSC_ODMIAN("ustwa_short_ziola");
    if(!pointerp(mshort))
        if(sizeof(mshort) != 6)
            ZLA_ILOSC_ODMIAN1("ustwa_short_ziola")
        else if(!tylko_mn)
            ZLA_ILOSC_ODMIAN2("ustwa_short_ziola")

    pshort_zident = pshort;
    mshort_zident = mshort;
    short_zident_tylko_mn = tylko_mn;
    rodz_shorta_zident = rodz;
}

/**
 * Alias do ustaw_short_ziola
 */
public varargs int
ustaw_short_zident(mixed pshort, string *mshort, int rodz = TO->query_rodzaj_zident())
{
    return ustaw_short_ziola(pshort, mshort, rodz);
}

/**
 * Ustawia short zidentyfilowanego zio�a
 * @param pname Pe�na odmiana w liczbie pojedycznej
 * @param mname Pe�na odmiana w liczbie mnogiej
 * @param rodz Rodzaj.
 * @return 1 w przypadku powodzenia
 */
public varargs int
ustaw_id_short_calosci(mixed pshort, mixed mshort, int rodz)
{
    int tylko_mn;

    if(stringp(pshort))
    {
        mixed odmiana = slownik_pobierz(pshort);

        pshort = odmiana[0];

        if(sizeof(odmiana) == 2)
        {
            rodz = odmiana[1];
            mshort = pshort;
            tylko_mn = 1;
        }
        else
        {
            rodz = odmiana[2];
            mshort = odmiana[1];
        }
    }
    else if(!pointerp(pshort))
        ZLA_ILOSC_ODMIAN("ustwa_id_short_calosci");

    if(intp(mshort))
    {
        rodz = mshort;
        tylko_mn = 1;
    }

    if(sizeof(pshort) != 6)
        ZLA_ILOSC_ODMIAN("ustwa_id_short_calosci");
    if(pointerp(mshort))
        if(sizeof(mshort) != 6)
            ZLA_ILOSC_ODMIAN1("ustwa_id_short_calosci")
        else if(!tylko_mn)
            ZLA_ILOSC_ODMIAN2("ustwa_id_short_calosci")

    id_pshort_cal = pshort;
    id_mshort_cal = mshort;
    id_short_cal_tylko_mn = tylko_mn;
    id_rodz_shorta_cal = rodz;

    return 1;
}
/**
 * Ustawia short niezidentyfikowanego, ca�ego zio�a.
 * @param pname Pe�na odmiana w liczbie pojedynczej.
 * @param mname Pe�na odmiana w liczbie mnogiej.
 * @param rodz Rodzaj nazwy
 * @return 1 je�li sukces
 */
public varargs int
ustaw_unid_short_calosci(mixed pshort, mixed mshort, int rodz)
{
    int tylko_mn;

    if(stringp(pshort))
    {
        mixed odmiana = slownik_pobierz(pshort);

        pshort = odmiana[0];

        if(sizeof(odmiana) == 2)
        {
            rodz = odmiana[1];
            mshort = pshort;
            tylko_mn = 1;
        }
        else
        {
            rodz = odmiana[2];
            mshort = odmiana[1];
        }
    }
    else if(!pointerp(pshort))
        ZLA_ILOSC_ODMIAN("ustaw_unid_short_calosci");

    if(intp(mshort))
    {
        tylko_mn = 1;
        rodz = mshort;
    }

    if(sizeof(pshort) != 6)
        ZLA_ILOSC_ODMIAN("ustwa_unid_short_calosci");
    if(pointerp(mshort))
        if(sizeof(mshort) != 6)
            ZLA_ILOSC_ODMIAN1("ustwa_unid_short_calosci")
        else if(!tylko_mn)
            ZLA_ILOSC_ODMIAN2("ustwa_unid_short_calosci")

    unid_pshort_cal = pshort;
    unid_mshort_cal = mshort;
    unid_short_cal_tylko_mn = tylko_mn;
    unid_rodz_shorta_cal  = rodz;

    return 1;
}

//query_*

/**
 * @param przyp Przypadek w jakim ma by� zwracana nazwa g��wna
 * @param fo Obiekt dla kt�rego ma by� zwr�cona nazwa.
 * @return Nazwe g��wn� zio�a zidentyfikowanego w liczbie pojedynczej w podanym przypadku.
 */
public varargs string
query_nazwa_glowna(int przyp, object fo)
{
    int i = 1;
    status mz;

    if(!fo)
        fo = find_previous_living();

    mz = moze_zidentyfikowac(fo);

    if(sizeof(id_pnazwa_cal) && mz && query_rosnie())
    {
        if(sizeof(id_png_cal) == 6)
            return id_png_cal[przyp];
    }
    else if(sizeof(unid_pnazwa_cal) && query_rosnie() && !mz)
    {
        if(sizeof(unid_png_cal) == 6)
            return unid_png_cal[przyp];
    }
    else if(sizeof(pnazwa_zident) && mz)
    {
        if(sizeof(png_zident) == 6)
            return png_zident[przyp];
    }
    else
        return ::query_nazwa_glowna(przyp);
}

/**
 * @param przyp Przypadek w jakim ma by� zwracana nazwa g��wna
 * @param fo Obiekt dla kt�rego ma by� zwr�cona nazwa.
 * @return Nazwe g��wn� zio�a zidentyfikowanego w liczbie pojedynczej w podanym przypadku.
 */
public varargs string
query_pnazwa_glowna(int przyp, object fo)
{
    int i = 1;
    status mz;

    if(!fo)
    {
        fo = TP;
        while(!moze_zidentyfikowac(fo) && fo)
            fo = previous_object(i--);
    }

    mz = moze_zidentyfikowac(fo);

    if(query_tylko_mn(fo))
        return query_nazwa_glowna(przyp, fo);
    else if(sizeof(id_pnazwa_cal) && mz && query_rosnie())
    {
        if(sizeof(id_mng_cal) == 6)
            return id_mng_cal[przyp];
    }
    else if(sizeof(unid_pnazwa_cal) && query_rosnie() && !mz)
    {
        if(sizeof(unid_mng_cal) == 6)
            return unid_mng_cal[przyp];
    }
    else if(sizeof(pnazwa_zident) && mz)
    {
        if(sizeof(mng_zident) == 6)
            return mng_zident[przyp];
    }
    else
        return ::query_pnazwa_glowna(przyp);
}

public varargs string
query_nazwa(int przyp, object fo)
{
    int i = 1;
    status mz;

    przyp = przyp || PL_MIA;

    if(!fo)
    {
        fo = TP;
        while(!moze_zidentyfikowac(fo) && fo)
            fo = previous_object(i--);
    }
    mz = moze_zidentyfikowac(fo);

    if(mz && query_rosnie() && id_pnazwa_cal)
        return id_pnazwa_cal[0][przyp];
    else if(query_rosnie() && unid_pnazwa_cal)
        return unid_pnazwa_cal[0][przyp];
    else if(mz && pnazwa_zident)
        return pnazwa_zident[0][przyp];
    else
        return ::query_nazwa(przyp);
}

public varargs string
query_pnazwa(int przyp, object fo)
{
    int i = 1;
    status mz;

    if(!fo)
    {
        fo = TP;
        while(!moze_zidentyfikowac(fo) && fo)
            fo = previous_object(i--);
    }
    mz = moze_zidentyfikowac(fo);

    if(query_tylko_mn(fo))
        return query_nazwa(przyp, fo);
    else if(mz && query_rosnie() && id_pnazwa_cal)
        return id_mnazwa_cal[0][przyp];
    else if(query_rosnie() && unid_pnazwa_cal)
        return unid_mnazwa_cal[0][przyp];
    else if(mz && pnazwa_zident)
        return mnazwa_zident[0][przyp];
    else
        return ::query_nazwa(przyp);
}

public varargs string
query_name(int przyp, object fo)
{
    return query_nazwa(przyp, fo);
}

public varargs string
query_pname(int przyp, object fo)
{
    return query_pname(przyp, fo);
}

/**
 * @return Zwraca liczbe pojedyncz� prawdziwej nazwy zio�a(nazwy dla graczy kt�rzy potrafi� rozpozna� zio�o).
 * @param przyp Przypadek
 */
public string
query_nazwa_ziola(int przyp)
{
    return pnazwa_zident[0][przyp];
}

/*
 * @return Zwraca liczb� mnog� prawdziwej nazwy zio�a(nazwy dla gracza kt�ry potrafi rozpozna� zio�o).
 * @param przyp Przypadek
 */
public string
query_pnazwa_ziola(int przyp)
{
    return mnazwa_zident[0][przyp];
}

/**
 * @param przyp Przypadek w jakim nazwa ma zosta� zwr�cona(TYLKO PL_MIA i PL_BIE)
 * @return Opis zidentyikowanego zio�a: Patrz ustaw_id_nazwe_calosci.
 */
public varargs string
query_id_nazwa_calosci(int przyp)
{
    if(sizeof(id_pnazwa_cal))
        return id_pnazwa_cal[0][przyp];
}

/**
 *
 * @param przyp przypadek w jakim ma by� zwr�cona nazwa(TYLKO PL_MIA i PL_BIE)
 * @return Nazwa zidentyfikowanego zio�a w liczbie mnogiej: Parz ustaw_id_nazwe_calosci
 */
public varargs string
query_id_pnazwa_calosci(int przyp)
{
    if(sizeof(id_pnazwa_cal))
        return id_mnazwa_cal[0][przyp];
}

public varargs string
query_short(int przyp, object fo)
{
    return (fo ? short(fo, przyp) : short(przyp));
}

public varargs string
query_pshort(int przyp, object fo)
{
    return (fo ? plural_short(fo, przyp) : plural_short(przyp));
}

//PRZYMIOTNIKI-----------------------------------------------------------------

//ustaw_*

public void
ustaw_przym_zepsutego(string lp, string lm)
{
    decay_adj = ({lp, lm});
}

//dodaj_*

private mixed
add_list(mixed list, mixed elem, int first)
{
    mixed e;

    if(obj_no_change)
        return list;

    if(pointerp(elem))
        e = elem;
    else
        e = ({elem});

    if(!pointerp(list))
        list = ({}) + e;
    else if(first)
        list = e + (list - e);
    else
        list += e;

    return list;
}

/**
 * Dodaje przymiotnik zidentyfikowanego zio�a.
 * @param poj Przymiotnik w liczbie pojedynczej, rodzaj m�ski
 * @param mn Przymiotnik w liczbie mnogiej, rodzaj m�ski
 * @param first Czy przymiotnik ma zaj�� pierwsze miejsce w tablicy
 */
public varargs int
dodaj_przym_ziola(string poj, string mn, int first)
{
    if(!poj || !mn)
        return 0;

    if(sizeof(przym_zident) != 2)
        przym_zident = allocate(2);

    przym_zident[0] = add_list(przym_zident[0], poj, first);
    przym_zident[1] = add_list(przym_zident[1], mn, first);

    return 1;
}

/**
 * Alias do dodaj_przym_ziola
 */
public varargs int
dodaj_przym_zident(string poj, string mn, int first)
{
    dodaj_przym_ziola(poj, mn, first);
}

/**
 * Dodaje przymiotniki ca�ego zio�a.
 * @param poj Przymiotnik w liczbie pojedynczej, rodzaj m�ski.
 * @param mn  Przymiotnik w liczbie mnogiej, rodzaj m�ski.
 * @param first Czy przymiotnik ma zaj�� pierwsze miejsce w tablicy.
 */
public varargs int
dodaj_id_przym_calosci(string poj, string mn, int first)
{
    if(!poj || !mn)
        return 0;

    if(sizeof(id_przym_cal) != 2)
       id_przym_cal = allocate(2);

    id_przym_cal[0] = add_list(id_przym_cal[0], poj, first);
    id_przym_cal[1] = add_list(id_przym_cal[1], mn, first);

    return 1;
}

public varargs int
dodaj_unid_przym_calosci(string poj, string mn, int first)
{
    if(!poj || !mn)
        return 0;

    if(sizeof(unid_przym_cal) != 2)
        unid_przym_cal = allocate(2);

    unid_przym_cal[0] = add_list(unid_przym_cal[0], poj, first);
    unid_przym_cal[1] = add_list(unid_przym_cal[1], mn, first);

    return 1;
}

//query_*

varargs public mixed query_pprzym(int arg, int przyp, mixed rodz, object fo);

//Funkcje zmieniaj�ce przymiotniki zale�nie od tego jakie maj� by� pokazand
varargs public mixed
query_przym(int arg, int przyp, mixed rodz, object fo)
{
    int liczba, rodz2, i = 1;
    status mz;
    string *wynik = ({});

    if(!intp(rodz))
    {
        if(objectp(rodz))
           fo = rodz;
        rodz = 0;
    }

    if(!fo)
        fo = find_previous_living();

    rodz2 = rodz;

    mz = moze_zidentyfikowac(fo);

    if(!rodz)
        rodz = TO->query_rodzaj(fo);

    if(query_tylko_mn(fo))
        liczba = 1;

    liczba = liczba || 0;

    if(query_rosnie() && mz && (sizeof(id_pnazwa_cal) || id_pshort_cal))
    {
        int size = (sizeof(id_przym_cal) ?  sizeof(id_przym_cal[0]) : 0);
        if(arg)
        {
            int i = -1;
            while(++i < size)
                if(stringp(id_przym_cal[0][i]) && stringp(id_przym_cal[0][i]))
                    wynik += ({oblicz_przym(id_przym_cal[0][i], id_przym_cal[1][i], przyp, rodz, liczba)});

            return wynik;
        }
        else if(size)
            if(stringp(id_przym_cal[0][0]) && stringp(id_przym_cal[0][0]))
                return oblicz_przym(id_przym_cal[0][0], id_przym_cal[1][0], przyp, rodz, liczba);
    }
    else if(query_rosnie() && (sizeof(unid_pnazwa_cal) || unid_pshort_cal))
    {
        int size = (sizeof(unid_przym_cal) ? sizeof(unid_przym_cal[0]) : 0);
        if(arg)
        {
            int i = -1;
            while(++i < size)
                if(stringp(unid_przym_cal[0][i]) && stringp(unid_przym_cal[0][i]))
                    wynik += ({oblicz_przym(unid_przym_cal[0][i], unid_przym_cal[1][i], przyp, rodz, liczba)});

            return wynik;
        }
        else if(size)
            if(stringp(unid_przym_cal[0][0]) && stringp(unid_przym_cal[0][0]))
                return oblicz_przym(unid_przym_cal[0][0], unid_przym_cal[1][0], przyp, rodz, liczba);
    }
    else if(mz && (sizeof(pnazwa_zident) || pshort_zident))
    {
        int size = (sizeof(przym_zident) ? sizeof(przym_zident[0]) : 0);

        if(arg)
        {
            int i = -1;

            while(++i <  size)
                if(stringp(przym_zident[0][i]) && stringp(przym_zident[0][i]))
                    wynik += ({oblicz_przym(przym_zident[0][i], przym_zident[1][i], przyp, rodz, liczba)});

            return wynik;
        }
        else if(size)
            if(stringp(przym_zident[0][0]) && stringp(przym_zident[0][0]))
                return oblicz_przym(przym_zident[0][0], przym_zident[1][0], przyp, rodz, liczba);
    }

    if(rodz2 != rodz)
        return ::query_przym(arg, przyp);

    return ::query_przym(arg, przyp, rodz);
}

varargs public mixed
query_pprzym(int arg, int przyp, mixed rodz, object fo)
{
    int i = 1, rodz2;
    status mz;
    string *wynik = ({});

    if(!intp(rodz))
    {
        if(objectp(rodz))
            fo = rodz;
        rodz = 0;
    }

    rodz2 = rodz;

    if(!fo)
        fo = find_previous_living();

    mz = moze_zidentyfikowac(fo);

    if(!rodz)
        rodz = query_rodzaj(fo);

    if(query_rosnie() && mz && (sizeof(id_pnazwa_cal) || id_pshort_cal))
    {
        int size = (sizeof(id_przym_cal) ? sizeof(id_przym_cal[0]) : 0);
        if(arg)
        {
            int i = -1;

            while(++i < size)
                if(stringp(id_przym_cal[0][i]) && stringp(id_przym_cal[1][i]))
                    wynik += ({oblicz_przym(id_przym_cal[0][i], id_przym_cal[1][i], przyp, rodz, 1)});

            return wynik;
        }
        else if(size)
            if(stringp(id_przym_cal[0][0]) && stringp(id_przym_cal[1][0]))
                return oblicz_przym(id_przym_cal[0][0], id_przym_cal[1][0], przyp, rodz, 1);
    }
    else if(query_rosnie() && (sizeof(unid_pnazwa_cal) || unid_pshort_cal))
    {
        int size = (sizeof(unid_przym_cal) ? sizeof(unid_przym_cal[0]) : 0);

        if(arg)
        {
            int i = -1;

            while(++i < size)
                if(stringp(unid_przym_cal[0][i]) && stringp(unid_przym_cal[1][i]))
                     wynik += ({oblicz_przym(unid_przym_cal[0][i], unid_przym_cal[1][i], przyp, rodz, 1)});

            return wynik;
        }
        else if(size)
            if(stringp(unid_przym_cal[0][0]) && stringp(unid_przym_cal[1][0]))
                return oblicz_przym(unid_przym_cal[0][0], unid_przym_cal[1][0], przyp, rodz, 1);
    }
    else if(mz && (sizeof(pnazwa_zident) || pshort_zident))
    {
        int size = (sizeof(przym_zident) ? sizeof(przym_zident[0]) : 0);

        if(arg)
        {
            int i = -1;

            while(++i <  size)
                if(stringp(przym_zident[0][i]) && stringp(przym_zident[1][i]))
                    wynik += ({oblicz_przym(przym_zident[0][i], przym_zident[1][i], przyp, rodz, 1)});

            return wynik;
        }
        else if(size)
            if(stringp(przym_zident[0][0]) && stringp(przym_zident[1][0]))
                return oblicz_przym(przym_zident[0][0], przym_zident[1][0], przyp, rodz, 1);
    }

    if(rodz2 != rodz)
        return ::query_pprzym(arg, przyp);

    return ::query_pprzym(arg, przyp, rodz);
}

public varargs mixed
query_adj(int arg, int przyp, int rodz, object fo)
{
    return query_przym(arg, przyp, rodz, fo);
}

public varargs mixed
query_padj(int arg, int przyp, int rodz, object fo)
{
    return query_pprzym(arg, przyp, rodz, fo);
}

public string
query_przym_zepsutego(int przyp, int rodz=TO->query_rodzaj())
{
    return oblicz_przym(decay_adj[0], decay_adj[1], przyp, rodz, 0);
}

public string
query_pprzym_zepsutego(int przyp, int rodz=TO->query_rodzaj())
{
    return oblicz_przym(decay_adj[0], decay_adj[1], przyp, rodz, 1);
}

varargs public mixed
query_przymiotniki(object fob)
{
    if(!fob)
        fob = find_previous_living();

    int mz = moze_zidentyfikowac(fob);

    if(mz && query_rosnie() && sizeof(id_pnazwa_cal))
        return id_przym_cal;
    else if(query_rosnie() && sizeof(unid_pnazwa_cal))
        return unid_przym_cal;
    else if(mz && sizeof(pnazwa_zident))
        return przym_zident;
    else
        return ::query_przymiotniki();
}

//SHORTY_______________________________________________________________

//Zwraca short w liczbie pojedynczej

public varargs string
short(mixed fo, mixed przyp)
{
    int mz, rodz;
    string ret = "", ng, ret2 = "", przyms;
    string *tmp;

    if(!objectp(fo))
    {
        if(intp(fo))
            przyp = fo;
        else if(stringp(fo))
            przyp = atoi(fo);

        fo = find_previous_living();
    }

    if(stringp(przyp))
        przyp = atoi(przyp);

    rodz = query_rodzaj(fo);
    int rodzbng = query_rodzaj(fo, 1);

    if(query_prop(OBJ_I_BROKEN) == 2)
    {
        //Warunek na to, �eby nie by�o zepsuty, zepsuty ciemny li��
        if(decay_adj[0] != "z�amany")
            ret = oblicz_przym("z�amany", "z�amani", przyp, rodz, 1);
    }
    else if(query_prop(OBJ_I_BROKEN) == 1)
    {
        //Warunek na to, �eby nie by�o zepsuty, zepsuty ciemny li��
        if(decay_adj[0] != "zepsuty")
            ret = oblicz_przym("zepsuty", "zepsuci", przyp, rodz, 0);
    }

    if(decayed && sizeof(decay_adj) == 2)
        ret = (ret != "" ? ret + " " : "") + (ret2 = oblicz_przym(decay_adj[0], decay_adj[1], przyp, rodz, 0));

    ng = query_nazwa_glowna(przyp, fo);
    mz = moze_zidentyfikowac(fo);

    if(ng != 0 && ng != "")
        przyp = PL_DOP;

    przyms = (sizeof(query_przym(1, przyp, rodzbng, fo)) ? implode(query_przym(1, przyp, rodzbng, fo), " ") : 0);

    if(query_rosnie() && mz && id_pshort_cal)
        return (ret != "" ? ret + " " : "") + id_pshort_cal[przyp];
    else if(query_rosnie() && mz && sizeof(id_pnazwa_cal))
        return (ret != "" ? ret + " " : "") + (ng ? ng + " " : "") + (przyms ? przyms + " " : "") + id_pnazwa_cal[0][przyp];
    else if(query_rosnie() && unid_pshort_cal)
        return (ret != "" ? ret + " " : "") + unid_pshort_cal[przyp];
    else if(query_rosnie() && sizeof(unid_pnazwa_cal))
        return (ret != "" ? ret + " " : "") + (ng ? ng + " " : "") + (przyms ? przyms + " " : "") + unid_pnazwa_cal[0][przyp];
    else if(mz && pshort_zident)
        return (ret != "" ? ret + " " : "") + pshort_zident[przyp];
    else if(mz && sizeof(pnazwa_zident))
        return (ret != "" ? ret + " " : "") + (ng ? ng + " " : "") + (przyms ? przyms + " " : "") + pnazwa_zident[0][przyp];
    else if(fo)
        return (ret2 != "" ? ret2 + " " : "") + ::short(fo, przyp);
    else
        return (ret2 != "" ? ret2 + " " : "") + ::short(przyp);
}

//Zwraca short w liczbie mnogiej.
public varargs string
plural_short(mixed fo, int przyp)
{
    int mz, rodz, tylko_mn;
    string ret = "", ret2 = "", ng, przyms;
    string *tmp;

    if(!objectp(fo))
    {
        if(intp(fo))
            przyp = fo;
        else if(stringp(fo))
            przyp = atoi(fo);

        fo = find_previous_living();
    }

    rodz = query_rodzaj(fo);
    int rodzbng = query_rodzaj(fo, 1);
    if(query_prop(OBJ_I_BROKEN) == 2)
    {
        //Warunek na to, �eby nie by�o zepsuty, zepsuty ciemny li��
        if(decay_adj[0] != "z�amany")
            ret = oblicz_przym("z�amany", "z�amani", przyp, rodz, 1);
    }
    else if(query_prop(OBJ_I_BROKEN))
    {
        //Warunek na to, �eby nie by�o zepsuty, zepsuty ciemny li��
        if(decay_adj[0] != "zepsuty")
            ret = oblicz_przym("zepsuty", "zepsuci", przyp, rodz, 0);
    }

    if(decayed && sizeof(decay_adj) == 2)
        ret = (ret != "" ? ret + " " : "") + (ret2 = oblicz_przym(decay_adj[0], decay_adj[1], przyp, rodz, 1));

    ng = query_pnazwa_glowna(przyp, fo);

    if(ng != 0 && ng != "")
    {
        przyp = PL_DOP;
        przyms = (query_przym(1, przyp, rodzbng, fo) != ({}) ?  implode(query_przym(1, przyp, rodzbng, fo), " ") : 0);
    }
    else
        przyms = (query_pprzym(1, przyp, rodzbng, fo) != ({}) ? implode(query_pprzym(1, przyp, rodzbng, fo), " ") : 0);

    mz = moze_zidentyfikowac(fo);

    if(query_tylko_mn(fo))
        return short(fo, przyp);
    else
    {
        if(query_rosnie() && mz && id_mshort_cal)
            return (ret != "" ? ret + " " : "") + id_mshort_cal[przyp];
        else if(query_rosnie() && mz && sizeof(id_mnazwa_cal))
            return (ret != "" ? ret + " " : "") + (ng ? ng + " " : "") + (przyms ? przyms + " " : "") +
                (ng ? id_pnazwa_cal[0][przyp] : id_mnazwa_cal[0][przyp]);
        else if(query_rosnie() && unid_mshort_cal)
            return (ret != "" ? ret + " " : "") + unid_mshort_cal[przyp];
        else if(query_rosnie() && sizeof(unid_mnazwa_cal))
            return (ret != "" ? ret + " " : "") + (ng ? ng + " " : "") + (przyms ? przyms + " " : "") +
                (ng ? unid_pnazwa_cal[0][przyp] : unid_mnazwa_cal[0][przyp]);
        else if(mz && mshort_zident)
            return (ret != "" ? ret + " " : "") + mshort_zident;
        else if(mz && sizeof(mnazwa_zident))
            return (ret != "" ? ret + " " : "") + (ng ? ng + " " : "") + (przyms ? przyms + " " : "") +
                (ng ? pnazwa_zident[0][przyp] : mnazwa_zident[0][przyp]);
        else if(fo)
            return (ret2 != "" ? ret2 + " " : "") + ::plural_short(fo, przyp);
        else
            return (ret2 != "" ? ret2 + " " : "") + ::plural_short(przyp);
    }
}

//RODZAJE____________________________________________________

/**
 * @param fo objekt dla  kt�rego ma zosta� wy�wietlony rodzaj
 * @param flag 1 je�li rodzaj ma nie uwzgl�dna� nazwy g��wnej
 * @return rodzaj gramatyczny shorta obiektu
 */
varargs public int
query_rodzaj(mixed fo, int flag)
{
    int i = 1;
    status mz;

    if(!objectp(fo))
    {
        if(intp(fo))
            flag = fo;

        fo = find_previous_living();
    }

    mz = moze_zidentyfikowac(fo);

    if(rosnie && mz && sizeof(id_pshort_cal) == 6)
        return id_rodz_shorta_cal;
    else if(rosnie && mz && sizeof(id_png_cal) == 6 && !flag)
        return id_rodz_ng_cal;
    else if(rosnie && mz && sizeof(id_pnazwa_cal))
        return id_rodz_cal[0];
    else if(rosnie && sizeof(unid_pshort_cal) == 6)
        return unid_rodz_shorta_cal;
    else if(rosnie && sizeof(unid_png_cal) == 6 && !flag)
        return unid_rodz_ng_cal;
    else if(rosnie && sizeof(unid_pnazwa_cal))
        return unid_rodz_cal[0];
    else if(mz && sizeof(pshort_zident) == 6)
        return rodz_shorta_zident;
    else if(mz && sizeof(png_zident) == 6 && !flag)
        return rodz_ng_zident;
    else if(mz && sizeof(pnazwa_zident))
        return rodz_zident[0];
    else
        return ::query_rodzaj();
}

public int
query_real_rodzaj(int wszystkie, int przyp)
{
    object fo = find_previous_living();

    int mz = moze_zidentyfikowac(fo);

    mixed rodz, tlmn;

    if(rosnie && mz && sizeof(id_pnazwa_cal))
    {
        tlmn = id_nazwa_cal_tylko_mn;
        rodz = id_rodz_cal;
    }
    else if(rosnie && sizeof(unid_pnazwa_cal))
    {
        tlmn = unid_nazwa_cal_tylko_mn;
        rodz = unid_rodz_cal;
    }
    else if(mz && sizeof(pnazwa_zident))
    {
        tlmn = nazwa_zident_tylko_mn;
        rodz = rodz_zident[0];
    }
    else
        return ::query_real_rodzaj();

    if(!wszystkie)
    {
        if(pointerp(rodz))
            rodz = rodz[0];

        if(query_tylko_mn(fo))
            return -rodz - 1;
        else
            return rodz + 1;
    }
    else
    {
        if(!pointerp(rodz))
            rodz = ({rodz});

        int i;
        for(i=0;i<sizeof(rodz);i++)
            if(tlmn[i])
                rodz[i] = -rodz[i] - 1;
            else
                rodz[i] = rodz[i] + 1;

        return rodz;
    }
}

/**
 * Funkcja sprawdza czy obiekt wyst�puje tylko w liczbie mnogiej.
 * @param fo obiekt dla kt�rego ma zosta� sprawdzone
 * @param flag je�li 1 nie uwzgl�dniamy nazwy g��wnej
 * @return 1/0 tylko w liczbie mnogiej/pe�na odmiana
 */
public varargs int
query_tylko_mn(mixed fo, int flag)
{
    int i = 1;
    status mz;

    if(!objectp(fo))
    {
        if(intp(fo))
            flag = fo;

        fo = find_previous_living();
    }

    mz = moze_zidentyfikowac(fo);

    if(query_rosnie() && mz && sizeof(id_pshort_cal))
        return id_short_cal_tylko_mn;
    else if(query_rosnie() && mz && sizeof(id_png_cal) && !flag)
        return id_ng_cal_tylko_mn;
    else if(query_rosnie() && mz && sizeof(id_pnazwa_cal))
        return id_nazwa_cal_tylko_mn[0];
    else if(query_rosnie() && sizeof(unid_pshort_cal))
        return unid_short_cal_tylko_mn;
    else if(query_rosnie() && sizeof(unid_png_cal) && !flag)
        return unid_ng_cal_tylko_mn;
    else if(query_rosnie() && sizeof(unid_pnazwa_cal))
       return unid_nazwa_cal_tylko_mn[0];
    else if(mz && sizeof(pshort_zident))
        return short_zident_tylko_mn;
    else if(mz && sizeof(png_zident) && !flag)
        return ng_zident_tylko_mn;
    else if(mz && sizeof(pnazwa_zident))
        return nazwa_zident_tylko_mn[0];
    else
        return ::query_tylko_mn();
}

private string
herb_long_desc()
{
    if(moze_zidentyfikowac(TP))
        return id_long;
    return unid_long;
}

public void
ustaw_komunikat_schniecia(string dm)
{
    decay_mess = dm;
}

public void
ustaw_komunikat_niszczenia(string tdm)
{
    total_decay_mess = tdm;
}

public string
query_komunikat_schniecia()
{
    return decay_mess;
}

public string
query_komunikat_niszczenia()
{
    return total_decay_mess;
}

/**
 * Ustawia opis pokazywany gracza ktorzy potrafia ziolo zidentyfikowac.
 */
void
set_id_long(string l)
{
    id_long = l;
}

/**
 * Zwraca opis dla graczy katorzy potrafia ziolo zidentyfikowac
 */
string
query_id_long()
{
    return id_long;
}

/**
 * Ustawia opis dla graczy ktorzy ziola nie sa w stanie zidentyfikowac
 */
void
set_unid_long(string l)
{
    unid_long = l;
}

/**
 * Zwraca opis ziola dla graczy ktorzy nie panimaja zielarstwa
 */
string
query_unid_long()
{
    return unid_long;
}

