/**
 * \file /lib/holdable_item
 *
 * W pliku tym znajduj� si� wszystkie definicje przeniesione z holdabje_objecta
 * dotycz�ce mo�liwo�ci chwycenia danej rzeczy.
 *
 * @example
 *
 *  create_xxx()
 *  {
 *      ...
 *      configure_holadble_item();
 *      ...
 *  }
 *
 *  leave_env(object from, object dest, string fsubloc)
 *  {
 *      ...
 *      holdable_item_leave_env(from, dest, fsubloc)
 *      ...
 *  }
 *
 *  enter_env(object dest, object from)
 *  {
 *      ...
 *      holdable_item_enter_env(dest, from)
 *      ...
 *  }
 *
 * @author  Krun
 * @date    12.04.2008
 * @version 1.0
 */

#pragma no_clone
#pragma no_reset
#pragma strict_types

#include <pl.h>
#include <macros.h>
#include <formulas.h>
#include <wa_types.h>
#include <stdproperties.h>

static  int
        wep_hands,          /* How many hands the weapon takes */
        wielded,            /* Wielded or not */
        wielded_in_hand;    /* Wielded in which hand */
static object
        wielder,            /* Who is holding it */
        wield_func;         /* Object that defines extra wield/unwield */

/**
 * Funkcja kt�ra powinna by� wywo�ywana w create_.
 */
public nomask void configure_holdable_item()
{
}

/**
 * Funkcja wywo�ywana przed chwyceniem rzeczy.
 * @param cicho - gdy 1, funkcja ma nie wy�wietla� �adnych komunikat�w!
 */
public void
przed_chwyceniem(int cicho)
{
}

/**
 * Funkcja wywo�ywana przed opuszczeniem rzeczy.
 */
public void
przed_opuszczeniem()
{
}

/**
 * Funkcja odpowiadaj�ca za dob�d� mnie.
 * W przypadku obiekt�w chwytanych wy�wietla
 * komunikat, �e mo�na to tylko chwyci�.
 */
nomask varargs mixed
wield_me(int cicho)
{
    if(wielded)
        return 0;
    else
    {
        return "Nie mo�esz " + TO->short(this_player(), PL_DOP) + " doby�, " +
            "ale mo�esz spr�bowa� chwyci�.\n";
    }
}

/**
 * Funkcja odpowiedzialna za chwycenie obiektu.
 *
 * @param cicho     gdy 1 niie wy�wietla �adnych komunikat�w.
 * @param czym      je�li gracz chce chwyci� kt�r�� z r�k.
 *
 * @return <ul>
 *   <li> 1 je�li sukces </li>
 *   <li> komunikat b��du </li>
 * </ul>
 */
public varargs mixed
hold_me(int cicho, string czym = 0)
{
    int wret, skill, pen;
    string penuse, wfail;

    if(czym == "")
        czym = 0;

    if(wielded)
        return "Ju^z trzymasz " + TO->short(this_player(), PL_BIE) + ".\n";
    else if(this_player() != environment(this_object()))
    {
        return "Musisz mie^c przy sobie " + TO->short(this_player(), PL_BIE) +
        ", ^zeby m^oc " + TO->koncowka("go", "j^a", "je", "ich", "je") +
        " chwyci^c...\n";
    }
    else if(TO->query_prop(OBJ_I_BROKEN))
    {
        return capitalize(TO->short(this_player(), PL_MIA)) + " " +
            (TO->query_tylko_mn() ? "s^a" : "jest") + " zniszczon" +
            TO->koncowka("y", "a", "e", "i", "e") +
            " i nie nadaje si^e ju^z do niczego.\n";
    }

    wielder = this_player();

    //Sprawd�my, czym chwycimy.
    wielded_in_hand = wep_hands;
    if(wep_hands != W_ANYH)
    {
        /*
         * Anything in both hands
         */
        if(wielder->query_weapon(W_BOTH) && wep_hands < W_FOOTR)
            return "Twoje r^ece s^a ju^z zaj^ete czym innym.\n";

        /*
         * Anything in the specified hand
         */
        if(wielder->query_weapon(wep_hands))
            return "Ju^z trzymasz " + wielder->query_weapon(wep_hands)->short(wielder, PL_BIE) + ".\n";

        if(wep_hands == W_BOTH && ( (wielder->query_slot(W_RIGHT)                                   &&
            pointerp(wielder->query_slot(W_RIGHT)) && ((pointerp(wielder->query_armour(W_RIGHT))    &&
            sizeof(wielder->query_slot(W_RIGHT) - wielder->query_armour(W_RIGHT)))                  ||
            (!pointerp(wielder->query_armour(W_RIGHT)) && sizeof(wielder->query_slot(W_RIGHT)))))   ||
            (wielder->query_slot(W_LEFT) && pointerp(wielder->query_slot(W_LEFT))                   &&
            ((pointerp(wielder->query_armour(W_LEFT))                                               &&
            sizeof(wielder->query_slot(W_LEFT) - wielder->query_armour(W_LEFT)))                    ||
            (!pointerp(wielder->query_armour(W_LEFT)) && sizeof(wielder->query_slot(W_LEFT)))))))
        {
            return "Potrzebujesz obu r^ak, ^zeby m^oc " +
                TO->koncowka("go", "j^a", "go", "ich", "je") +
                " chwyci�.\n";
        }
        else if(wep_hands == W_BOTH && (wielder->query_proc_hp(-HA_L_REKA) <= 30 ||
            wielder->query_proc_hp(-HA_P_REKA) <= 30))
        {
            return "Masz zbyt pokieraszowane r�ce aby " +
                TO->koncowka("go", "j^a", "go", "ich", "je") +
                "chwyci�.\n";
        }
        else if(wep_hands == W_LEFT && wielder->query_proc_hp(-HA_L_REKA) <= 30)
        {
            return "Lew� r�k� masz zbyt pokieraszowan� aby chwyci� " +
                TO->koncowka("go", "j�", "go", "ich", "je") + " ni�.\n";
        }
        else if(wep_hands == W_RIGHT && wielder->query_proc_hp(-HA_P_REKA) <= 30)
        {
            return "Praw� r�k� masz zbyt pokieraszowan� aby chwyci� " +
               TO->koncowka("go", "j�", "go", "ich", "je") + " ni�.\n";
        }
    }
    else if(!(wielder->query_slot(W_RIGHT) && pointerp(wielder->query_slot(W_RIGHT))        &&
        ((pointerp(wielder->query_armour(W_RIGHT))                                          &&
        sizeof(wielder->query_slot(W_RIGHT) - wielder->query_armour(W_RIGHT)))              ||
        (!pointerp(wielder->query_armour(W_RIGHT))                                          &&
        sizeof(wielder->query_slot(W_RIGHT))))) && (!czym || czym ~= "praw� r�k�"))
    {
        wielded_in_hand = W_RIGHT;
    }

    else if(czym && (wielder->query_proc_hp(-HA_L_REKA) <= 30 || wielder->query_proc_hp(-HA_P_REKA) <= 30))
    {
        return (czym ~= "praw� r�k�" ? "Praw�" : "Lew�") + " r�k� masz zbyt "+
            "pokieraszowan� aby chywci� " + TO->koncowka("go", "j�", "go", "ich", "je")+
            " ni�.\n";
    }
    else if(czym ~= "praw� r�k�" || czym ~= "lew� r�k�")
        return (czym ~= "praw� r�k�" ? "Praw�" : "Lew�" ) + " r�k� masz ju� zaj�t�.\n";
    else if(czym)
        return "Czym chcesz chwyci�?\n";
    else if(wielder->query_proc_hp(-HA_L_REKA) <= 30 || wielder->query_proc_hp(-HA_P_REKA) <= 30)
    {
        return "Masz zbyt pokieraszowane r�ce aby " +
            TO->koncowka("go", "j^a", "go", "ich", "je") +
            "chwyci�.\n";
    }
    else if(wielder->query_proc_hp(-HA_L_REKA) <= 30)
    {
        return "Lew� r�k� masz zbyt pokieraszowan� aby chwyci� " +
            TO->koncowka("go", "j�", "go", "ich", "je") + " ni�.\n";
    }
    else if(wielder->query_proc_hp(-HA_P_REKA) <= 30)
    {
        return "Praw� r�k� masz zbyt pokieraszowan� aby chwyci� " +
            TO->koncowka("go", "j�", "go", "ich", "je") + " ni�.\n";
    }
    else
    {
        return "Nie masz wolnej reki, w kt^ora m" + wielder->koncowka("�g�",
            "og�a") + "by� chwyci� " + TO->short(wielder, PL_BIE) + ".\n";
    }

    przed_chwyceniem(cicho);

    if(stringp(wfail = wielder->wield(this_object())))
        return wfail;

    wret = 0;

    /*
     * A wield function in another object.
     */
    if((!cicho) && ((!wield_func) || (!(wret=wield_func->wield(this_object())))))
    {
        if(wielded_in_hand == W_BOTH)
            write("Chwytasz obur^acz " + TO->short(wielder, PL_BIE) + ".\n");
        else if(wielded_in_hand < W_FOOTR)
        {
            write("Chwytasz " + (wielded_in_hand == W_RIGHT ? "praw^a" :
                "lew^a") + " r^ek^a " + TO->short(wielder, PL_BIE) + ".\n");
        }
        else
        {
            write("Chwytasz " + TO->short(wielder, PL_BIE) + " " +
                (wielded_in_hand == W_FOOTR ? "praw^a" : "lew^a") +
                " stop^a.\n");
        }

        saybb(QCIMIE(this_player(), PL_MIA) + " chwyta " + QSHORT(this_object(), PL_BIE) + ".\n");
    }

    if(intp(wret) && wret >= 0)
    {
        // zeby nie dawac chwyconych rzeczy
        TO->add_prop(OBJ_M_NO_GIVE, "Najpierw musisz opu�ci� " + TO->short(wielder, PL_BIE) + ".\n");
        TO->set_obj_subloc(SUBLOC_WIELD);
        wielded = 1;
        TP->set_obiekty_zaimkow(({TO}));
        return 1;
    }

    if(stringp(wfail = wielder->unwield(TO)))
    {
        /* This is serious and almost fatal, please panic! */
        wielded = 1;
        TP->set_obiekty_zaimkow(({TO}));
        return 1;
    }

    /*
     * If the wieldfunc returned a value <0 the we can not wield
     * likewise if it returned a string, but then we use that string
     * as error message.
     */
    if(stringp(wret))
        return wret;
    else
        return "Nie mo^zesz chwyci^c " + TO->short(wielder, PL_DOP) + "\n";
}

/**
 * Funkcja odpowiedzialna za opuszczenie rzeczy.
 *
 * @return <ul>
 *   <li> 1 je�li sukces </li>
 *   <li> komunikat b��du </li>
 * </ul>
 */
int
unwield_me()
{
    mixed wret;

    if(!wielded || !wielder)
        return 0;

    wret = 0;

    /*
     * A unwield function in another object.
     */
    if((!wield_func) || (!(wret = wield_func->unwield(this_object()))))
    {
        if(TO->check_seen(this_player()))
            write("Opuszczasz " + TO->short(this_player(), PL_BIE) + ".\n");
        else
            write("Opuszczasz co^s.\n");

        saybb(QCIMIE(this_player(), PL_MIA) + " opuszcza " +
            QSHORT(this_object(), PL_BIE) + ".\n");
    }

    /*
     * If the wieldfunc returned a value < 0 then we can not unwield
     */
    if(intp(wret) && (wret >= 0))
    {
        TO->remove_prop(OBJ_M_NO_GIVE);
        przed_opuszczeniem();
        TO->set_obj_subloc(0);
        wielder->unwield(this_object());
        wielded = 0;
        wielded_in_hand = wep_hands;
        TP->set_obiekty_zaimkow(({TO}));
        return 1;
    }
    else if(stringp(wret))
        write(wret);

    return 0;
}

/*
 * Function name: find_wep
 * Description  : This filter function is used to find any weapon wielded
 *                in a particular location.
 * Arguments    : object wep  - the weapon to test.
 *                string ploc - the location to test for.
 * Returns      : int 1/0 - true if the weapon is wielded on that location.
 */
static int
find_wep(object wep, string ploc)
{
    int where;
    string a, b;

    ploc = "d " + ploc + " d";
    where = wep->query_attack_id();

    if(sscanf(ploc, "%slew%s", a, b) && ((where == W_RIGHT) || (where == W_FOOTR)))
        return 0;
    if(sscanf(ploc, "%spraw%s", a, b) && ((where == W_LEFT) || (where == W_FOOTL)))
        return 0;
    if (sscanf(ploc, "%sr^ek%s", a, b) && ((where == W_FOOTL) || (where == W_FOOTR)))
        return 0;
    if (sscanf(ploc, "%sstop%s", a, b) && ((where == W_ANYH) || (where == W_LEFT) ||
        (where == W_RIGHT) || (where == W_BOTH)))
    {
        return 0;
    }

    return 1;
}

/**
 * Funkcja kt�ra MUSI by� wywo�ana kiedy obiekt opuszcza �rodowisko w kt�rym jest
 * czyli w funkcji leave_env()
 *
 * @param from          opuszczane �rodowisko
 * @param dest          nowe �rodowisko
 * @param fromSubloc    opuszczana sublokacja
 */
public void holdable_item_leave_env(object from, object dest, string fromSubloc)
{
    // Zmieni�em tu kolejno�� coby nie by�o najperw odk�ada potem opuszcza:P
    if (wielded)
    {
        if ((!wield_func || !wield_func->unwield(this_object())) && wielder)
        {
            tell_object(wielder, "Opuszczasz " + TO->short(wielder, PL_BIE) + ".\n");
        }

        wielder->unwield(this_object());
        wielded = 0;
    }
}

/**
 * Funkcja kt�ra MUSI zosta� wywo�ana kiedy obiekt wchodzi w nowe �rodowisko
 * czyli w funkcji enter_env()
 *
 * @param dest      nowe �rodowisko
 * @param from      opuszczane �rodowisko
 */
public void holdable_item_enter_env(object dest, object from)
{
}

/**
 * Funkcja ustawia w kt�rych r�kach obiekt mo�e by� trzymany.
 *
 * @param which     r�ce w kt�rych obiekt mo�e by� trzymany.
 *                  Dopuszczalne warto�ci to: <ul>
 *                      <li> W_RIGHT - prawa r�ka </li>
 *                      <li> W_LEFT  - lewa r�ka </li>
 *                      <li> W_BOTH  - obie r�ce </li>
 *                      <li> W_ANYH  - kt�rakolwiek r�ka </li>
 *                      <li> W_LFOOT - lewa noga </li>
 *                      <li> W_RFOOT - prawa noga </li>
 *                  </ul>
 */
void
set_hands(int which)
{
    if(TO->query_lock())
        return;

    if(F_LEGAL_HANDS(which))
        wep_hands = which;
}

/**
 * @return zwraca w kt�rej r�ce obiekt mo�e by� trzymany.
 */
int query_hands() { return wep_hands; }

/*
 * Sets the object to call wield/unwield in when this occurs.
 * Those functions can return:
 *      0 - No affect the weapon can be wielded / unwielded
 *      1 - It can be wielded / unwielded but no text should be printed
 *          (it was done in the function)
 *      -1  It can not be wielded / unwielded default failmsg will be
 *          written
 *             string  It can not be wielded / unwielded 'string' is the
 *             fail message to print
 */
void
set_wf(object obj)
{
    if(TO->query_lock())
        return;         /* All changes has been locked out */

    wield_func = obj;
}

/*
 * Function name: query_wf
 * Description:   Query if/what object defines wield/unwield functions
 */
object query_wf() { return wield_func; }

/*
 * Function name: query_wield_desc
 * Description:   Describe this weapon as wielded by a something.
 * Argumensts:    p: Possessive description of wielder
 * Returns:       Description string.
 */
public nomask string
query_wield_desc()
{
    string str;

    str = TO->short(this_player(), PL_BIE);

    switch (wielded_in_hand)
    {
        case W_RIGHT:return str + " w prawej r^ece";
        case W_LEFT: return str + " w lewej r^ece";
        case W_BOTH: return "obur^acz " + str;
        case W_FOOTR:return str + " w prawej stopie";
        case W_FOOTL:return str + " w lewej stopie";
    }
    return str;
}

/*
 * Function name: query_wielded
 * Description:   If this object is wielded or not
 * Returns:       The object who wields this object if this object is wielded
 */
object
query_wielded()
{
    if (wielded) return wielder;
}