/*
 * Plik: /lib/sit.c
 * Ostatnia modyfikacja: Fain  w Maju 2006
 * Autor: Gregh
 * Data: Nieznana
 *
 */
#include <composite.h>
#include <macros.h>
#include <stdproperties.h>
#include <filter_funs.h>
#include <cmdparse.h>
#include <language.h>
#include <sit.h>
#include <horses.h>

static mixed room_sits;
static mapping room_sits_obj=([]);
static string* this_sit;
static object sit_obj;

int
equiv_array(mixed tab1, mixed tab2)
{
  int j;
  if (!(pointerp(tab1) && pointerp(tab2))) {
    return 0;
  }
  if (sizeof(tab1) == sizeof(tab2)) {
    j = 0;
    foreach( string elem : tab1) {
      if (elem != tab2[j]) {
        break;
      }
      ++j;
    }
    if (j == sizeof(tab2)) {
      return 1;
    }
  }
  return 0;
}

/*
 * Nazwa funkcji : add_sit
 * Opis          : Dodaje siedzenia na lokacji.
 * Argumenty     : sit_cmd:   Komenda do siadania
 *                 sit_long:  Opis siadania np. wygodnie przy stole
 *                 sit_up:    Opis wstawania np. od stolu
 *                 sit_space: Ilosc miejsc, 0 - nieskonczonosc
 */
public varargs int
add_sit(mixed sit_cmd, string sit_long, string sit_up, int sit_space, object *
sit_ob=({}), int przyp = PL_MIE, string przyimek = "na")
{
    int i;
    int juz_taki_jest = 0;
    if (stringp(sit_cmd)) {
      sit_cmd = ({ sit_cmd });
    }

    if(sizeof(sit_ob))
    {
        if(m_sizeof(room_sits_obj))
        {
            room_sits_obj += ([sit_ob[0]:({sit_long,sit_up,sit_space,sit_cmd,przyp,przyimek})]);
        }
        else
        {
            room_sits_obj =([sit_ob[0]:({sit_long,sit_up,sit_space,sit_cmd,przyp,przyimek})]);
        }
    }
    else
    {
    if (pointerp(room_sits))
    {
        for (i = 0; i < sizeof(room_sits); i += 4) {
          if (equiv_array(sit_cmd, room_sits[i])) {
            juz_taki_jest = 1;
            break;
          }
        }

        if (juz_taki_jest)
        {
            if (room_sits[i + 3]) room_sits[i + 3] += sit_space;

        }

        else
            room_sits = room_sits + ({ sit_cmd, sit_long, sit_up, sit_space});
    }
    else
        room_sits = ({ sit_cmd, sit_long, sit_up, sit_space});
    }
    return 1;
}

/*
 * Function name: remove_sit
 * Opis          : Usuwa siedzenia z lokacji.
 * Argumenty     : cmd: Nazwa siedzenia np. przy stole
 */
public int
remove_sit(mixed cmd, int sit_space = 0, object *sit_ob = ({ }))
{
    int i;
    string* real_cmd;
    if(sizeof(sit_ob))
    {
        if(sizeof(room_sits_obj[sit_ob[0]]))
        {
            room_sits_obj=m_delete(room_sits_obj,sit_ob[0]);
        }
    }

    if (stringp(cmd)) {
      cmd = ({ cmd });
    }

    for (i = 0; i < sizeof(room_sits); i += 4)
    {
        real_cmd = room_sits[i];

        if (equiv_array(cmd, real_cmd))
        {
            if (sit_space)
            {
                room_sits[i + 3] -= sit_space;
                if (!room_sits[i + 3])
                {
                    room_sits = exclude_array(room_sits, i, i + 4);
                    return 1;
                }
                room_sits[i + 4] -= sit_ob;
            }
            else
            {
                room_sits = exclude_array(room_sits, i, i + 4);
                return 1;

            }
        }
    }
    return 0;
}

/*
 * Function name: query_sit
 * Opis          : Zwraca tablice siedzen na lokacji w postaci:
 *
 *                 [0] = (string) "przy stole"
 *                 [1] = (string) "z hukiem przy stole"
 *                 [2] = (string) "od stolu"
 *                 [3] = (int) 2
 *                 [4] = (string) "na ziemi"
 *                 [5] = (string) "po turecku na ziemi"
 *                 [6] = (string) "z oci ganiem z ziemi"
 *                 [7] = (int) 0
 *
 */

public mixed
query_sit()
{
    if (!pointerp(room_sits))
    {
        return ({ });
    }

    return ({ }) + room_sits;
}
/*
 * Function name: query_sit_obj
 * Opis          : Zwraca mapping w postaci
 *
 *                ([ object:({"z hukiem na wiadrze",
 *                            "z wiadra",
 *                             0}),
 *                   object:({"wygodnie na stolku",
 *                            "z ociaganiem ze stolka",
 *                              2}) ])
 *
 */
public mapping
query_sit_obj()
{
    if(!m_sizeof(room_sits_obj))
    {
        return ([ ]);
    }

    return ([ ]) +room_sits_obj;
}
/*
 * Function name: query_sit_cmds
 * Opis          : Zwraca tablice nazw siedzen.
 */
public string *
query_sit_cmds()
{
    int index;
    int size;
    mapping tmpItems = ([]);

    string *cmds, sit;
    object *obj;

    size = sizeof(room_sits);

    cmds = ({ });
    index = -4;

    while((index += 4) < size)
    {
      sit = (room_sits[index]);
      if (pointerp(sit)) {
        cmds += ({ sit[0] });
      }
      else {
        cmds += ({ sit });
      }
    }
    //teraz jedziemy dla przedmiotow... pobieramy shorta i dodajemy zaimek
    obj=m_indexes(room_sits_obj);
    for(index=0;index<sizeof(obj);index++)
    {
      tmpItems[obj[index]->short(PL_MIE)] += 1;
      if(tmpItems[obj[index]->short(PL_MIE)]==1) {
        cmds +=({ room_sits_obj[obj[index]][3][0] +" "+obj[index]->short(room_sits_obj[obj[index]][4]) });
      }
      else {
        cmds +=({ room_sits_obj[obj[index]][3][0]+" "+LANG_SORD(tmpItems[obj[index]->short(PL_MIE)],
              room_sits_obj[obj[index]][4], obj[index]->query_rodzaj())+ " "+
            obj[index]->short(room_sits_obj[obj[index]][4]) });
      }
    }
    return cmds;
}
/*
 * Function name: query_sit_longs
 * Opis          : Zwraca tablice opisow siadania.
 */

public string *
query_sit_longs()
{
    int index;
    int size;
    string *cmds, sit;
    object *obj;
    if ((size = sizeof(room_sits)) < 4)
    {
        return ({ });
    }

    cmds = ({ });
    index = -3;
    while((index += 4) < size)
    {
        sit = (room_sits[index]);
        cmds += ({ sit });
    }
    obj=m_indexes(room_sits_obj);
    for(index=0;index<sizeof(obj);index++)
    {
        sit=room_sits_obj[obj[index]][0];
        cmds += ({ sit });
    }
    return cmds;

}

/*
 * Function name: query_sit_up
 * Opis          : Zwraca tablice opisow wstawania.
 */
public string *
query_sit_cmds_up()
{
    int index;
    int size;
    string *cmds, sit;
    object *obj;
    if ((size = sizeof(room_sits)) < 4)
    {
        return ({ });
    }

    cmds = ({ });
    index = -2;

    while((index += 4) < size)
    {
        sit = (room_sits[index]);
        cmds += ({ sit });
    }
    obj=m_indexes(room_sits_obj);
    for(index=0;index<sizeof(obj);index++)
    {
        sit=room_sits_obj[obj[index]][1];
        cmds += ({ sit });
    }
    return cmds;
}

/*
 * Function name: query_sit_space
 * Opis          : Zwraca tablice ilosci miejsc.
 */
public string *
query_sit_space()
{
    int index;
    int size;
    string *cmds, sit;
    object *obj;
    if ((size = sizeof(room_sits)) < 4)

    {
        return ({ });
    }

    cmds = ({ });
    index = -1;
    while((index += 4) < size)
    {
        sit = (room_sits[index]);
        cmds += ({ sit });
    }
    obj=m_indexes(room_sits_obj);
    for(index=0;index<sizeof(obj);index++)
    {
        sit=room_sits_obj[obj[index]][2];
        cmds += ({ sit });
    }
    return cmds;
}

int
check_sit(object kto)
{
  if (equiv_array((kto->query_prop(SIT_SIEDZACY)), this_sit))
    return 1;

  return 0;
}

int
check_sit2(object kto)
{
  if ((kto->query_prop(PLAYER_O_SIEDZE)) == sit_obj)
    return 1;

  return 0;
}

int
check_lie(object kto)
{
  if (equiv_array((kto->query_prop(SIT_LEZACY)), this_sit))
    return 1;

  return 0;
}

int
check_lie2(object kto)
{
    if ((kto->query_prop(PLAYER_O_LEZE)) == sit_obj)
    return 1;


    return 0;
}

public int
usiadz(string str)
{
  string *sits = query_sit_cmds();
  string zaimek;
  object *obs;
  mixed oblist;
  mixed siedzenie;
  int i,b, kladzenie = 0, zmiana = 0;
  int final_notify_fail = 0;

  if (query_verb() ~= "po��") {
    if (str == 0) {
      return 0;
    }
    kladzenie = 1;
    if (explode(str, " ")[0] ~= "si�") {
      str = implode(explode(str, " ")[1..], " ");
      if (str == "") {
        str = 0;
      }
    }
    else {
      return 0;
    }
  }

  if (this_player()->query_prop(LIVE_O_ON_HORSE))
  {
    notify_fail("Dosiadasz przecie� wierzchowca!\n");
    return 0;
  }

  if ((!pointerp(room_sits) || room_sits == ({}))&&(!m_sizeof(room_sits_obj)))
  {
    if (kladzenie) {
      notify_fail("Nie ma tu gdzie si� po�o�y�.\n");
    }
    else {
      notify_fail("Nie ma tu gdzie usi���.\n");
    }
  }
  else
  {
    if (kladzenie) {
      notify_fail("Mo�na tu si� po�o�y� tylko " + COMPOSITE_WORDS(sits) + ".\n");
    }
    else {
      if(!sizeof(sits))
          notify_fail("Nie ma tu gdzie usi���.\n");
      else
          notify_fail("Mo�na tu usi��� tylko " + COMPOSITE_WORDS(sits) + ".\n");
    }
  }

  //prosty sposob na poprawienie bledu "Na bruku siedzi 0." :P
  //Vera.
  if((TP->query_wiz_level() && TP->query_prop(OBJ_I_INVIS)) ||
      TP->query_prop(OBJ_I_HIDE))
  {
    notify_fail("Musisz si� wpierw ujawni�!\n");
    return 0;
  }

  if (kladzenie) {
    if(this_player()->query_prop(SIT_LEZACY))
    {
      notify_fail("Przecie� ju� gdzie� le�ysz.\n");
      return 0;
    }
    if(this_player()->query_prop(SIT_SIEDZACY))
    {
      if (stringp(str)) {
        notify_fail("Przecie� ju� gdzie� siedzisz.\n");
        return 0;
      }
      else {
        siedzenie = this_player()->query_prop(SIT_SIEDZACY);
        for (i = 0; i < sizeof(room_sits); i += 4)
        {
          if (equiv_array(siedzenie, room_sits[i])) {
            if ((room_sits[i + 3] != 0) && (room_sits[i + 3] <= (
                    sizeof(filter(FILTER_LIVE(all_inventory(this_object())), check_sit)) +
                    2*sizeof(filter(FILTER_LIVE(all_inventory(this_object())), check_lie)))))
            {
              notify_fail("Ju� si� nie zmie�cisz.\n");
              return 0;
            }
            this_player()->remove_prop(SIT_SIEDZACY);
            this_player()->add_prop(SIT_LEZACY, room_sits[i]);
            write("K�adziesz si� " + room_sits[i + 1] + ".\n");
            saybb(QCTNAME(this_player()) + " k�adzie si� " + room_sits[i + 1] + ".\n");
            return 1;
          }
        }
        if(sizeof(room_sits_obj[siedzenie]))
        {
          if((room_sits_obj[siedzenie][2]!=0)&&
              (room_sits_obj[siedzenie][2]
               <= (sizeof(filter(FILTER_LIVE(all_inventory(this_object())),
                     check_sit2)) +
                 2*sizeof(filter(FILTER_LIVE(all_inventory(this_object())),
                     check_lie2)))))
          {
            notify_fail("Ju� si� nie zmie�cisz.\n");
            return 0;
          }
          this_player()->remove_prop(SIT_SIEDZACY);
          this_player()->remove_prop(PLAYER_O_SIEDZE);
          this_player()->add_prop(SIT_LEZACY, siedzenie);
          this_player()->add_prop(PLAYER_O_LEZE,siedzenie);
          write("K�adziesz si� " + room_sits_obj[siedzenie][0] + ".\n");
          saybb(QCTNAME(this_player()) + " k�adzie si� " + room_sits_obj[siedzenie][0]+".\n");
          siedzenie->add_prop(SIT_I_NUM_SIE, siedzenie->query_prop(SIT_I_NUM_SIE) - 1);
          siedzenie->add_prop(SIT_I_NUM_LEZ, siedzenie->query_prop(SIT_I_NUM_LEZ) + 1);
          siedzenie->add_prop(OBJ_M_NO_GET,"Kto� tam le�y.\n");
          return 1;
        }
      }
    }
  }
  else {
    if(this_player()->query_prop(SIT_SIEDZACY))
    {
      notify_fail("Przecie� ju� gdzie� siedzisz.\n");
      return 0;
    }
    if(this_player()->query_prop(SIT_LEZACY))
    {
      if (stringp(str)) {
        notify_fail("Przecie� ju� gdzie� le�ysz.\n");
        return 0;
      }
      else {
        siedzenie = this_player()->query_prop(SIT_LEZACY);
        for (i = 0; i < sizeof(room_sits); i += 4)
        {
          if (equiv_array(siedzenie, room_sits[i])) {
            this_player()->remove_prop(SIT_LEZACY);
            this_player()->add_prop(SIT_SIEDZACY, room_sits[i]);
            write("Siadasz " + room_sits[i + 1] + ".\n");
            saybb(QCTNAME(this_player()) + " siada " + room_sits[i + 1] + ".\n");
            return 1;
          }
        }
        if(sizeof(room_sits_obj[siedzenie]))
        {
          this_player()->remove_prop(SIT_LEZACY);
          this_player()->remove_prop(PLAYER_O_LEZE);
          this_player()->add_prop(SIT_SIEDZACY, siedzenie);
          this_player()->add_prop(PLAYER_O_SIEDZE,siedzenie);
          write("Siadasz " + room_sits_obj[siedzenie][0] + ".\n");
          saybb(QCTNAME(this_player()) + " siada " + room_sits_obj[siedzenie][0]+".\n");
          siedzenie->add_prop(SIT_I_NUM_LEZ, siedzenie->query_prop(SIT_I_NUM_LEZ) - 1);
          siedzenie->add_prop(SIT_I_NUM_SIE, siedzenie->query_prop(SIT_I_NUM_SIE) + 1);
          siedzenie->add_prop(OBJ_M_NO_GET,"Kto� tam siedzi.\n");
          return 1;
        }
      }
    }
  }

  if (!str && !zmiana)
  {
    if ((sizeof(room_sits) == 4) && (m_sizeof(room_sits_obj) == 0)) {
      str = room_sits[0][0];
    }
    else if ((sizeof(room_sits) == 0) && (m_sizeof(room_sits_obj) == 1)) {
      str = room_sits_obj[m_indexes(room_sits_obj)[0]][3][0] + " " + m_indexes(room_sits_obj)[0]->short(this_player(), room_sits_obj[m_indexes(room_sits_obj)[0]][4]);
    }
    else {
      return 0;
    }
  }

  for (i = 0; i < sizeof(room_sits); i += 4)
  {
    this_sit = room_sits[i];
    if (member_array(str, this_sit) != -1)
    {
      if ((room_sits[i + 3] != 0) && (room_sits[i + 3] <= (
              sizeof(filter(FILTER_LIVE(all_inventory(this_object())), check_sit)) +
              2*sizeof(filter(FILTER_LIVE(all_inventory(this_object())), check_lie)) +
              kladzenie)))
      {
        notify_fail("Ju� si� nie zmie�cisz.\n");
        return 0;
      }

      this_player()->add_prop(OBJ_I_DONT_GLANCE, 1);
      if (kladzenie) {
        this_player()->add_prop(SIT_LEZACY, room_sits[i]);
        write("K�adziesz si� " + room_sits[i + 1] + ".\n");
        saybb(QCTNAME(this_player()) + " k�adzie si� " + room_sits[i + 1] + ".\n");
      }
      else {
        this_player()->add_prop(SIT_SIEDZACY, room_sits[i]);
        write("Siadasz " + room_sits[i + 1] + ".\n");
        saybb(QCTNAME(this_player()) + " siada " + room_sits[i + 1] + ".\n");
      }

      return 1;
    }
  }

  // sprawdzamy teraz dla obiektow...
  for (int cur_przyp = PL_MIA; cur_przyp <= PL_MIE; cur_przyp++) {
    if(!parse_command(lower_case(str),
          environment(this_player()),"%s %i:" + cur_przyp, zaimek, oblist))
    {
      if (kladzenie) {
        if (!final_notify_fail)
          notify_fail("Mo�na tu si� po�o�y� tylko " + COMPOSITE_WORDS(sits) + ".\n");
      }
      else {
        if(!sizeof(sits))
          notify_fail("Nie ma tu gdzie usi���.\n");
        else if (!final_notify_fail)
          notify_fail("Mo�na tu usi��� tylko " + COMPOSITE_WORDS(sits) + ".\n");
      }
      continue;
    }
    oblist = CMDPARSE_STD->normal_access(oblist, 0, this_object());
    if(!sizeof(oblist))
    {
      if (kladzenie) {
        if (!final_notify_fail)
          notify_fail("Mo�na tu si� po�o�y� tylko " + COMPOSITE_WORDS(sits) + ".\n");
      }
      else {
        if(!sizeof(sits))
          notify_fail("Nie ma tu gdzie usi���.\n");
        else if (!final_notify_fail)
          notify_fail("Mo�na tu usi��� tylko " + COMPOSITE_WORDS(sits) + ".\n");
      }
      continue;
    }
    if(sizeof(oblist) > 1)
    {
      if (kladzenie) {
        notify_fail("Naraz mo�esz po�o�y� si� tylko na jednej rzeczy.\n");
        final_notify_fail = 1;
      }
      else {
        notify_fail("Naraz mo�esz usi��� tylko na jednej rzeczy.\n");
        final_notify_fail = 1;
      }
      continue;
    }
    siedzenie=oblist[0];
    if(!siedzenie->query_prop("_siedzonko"))
    {
      if (kladzenie) {
        notify_fail("Nie mo�esz na tym si� po�o�y�.\n");
        final_notify_fail = 1;
      }
      else {
        notify_fail("Nie mo�esz na tym usi���.\n");
        final_notify_fail = 1;
      }
      continue;
    }
    sit_obj=siedzenie;
    if(sizeof(room_sits_obj[siedzenie]))
    {
      if (kladzenie) {
        if (!final_notify_fail)
          notify_fail("Mo�na tu si� po�o�y� tylko " + COMPOSITE_WORDS(sits) + ".\n");
      }
      else {
        if(!sizeof(sits))
          notify_fail("Nie ma tu gdzie usi���.\n");
        else if (!final_notify_fail)
          notify_fail("Mo�na tu usi��� tylko " + COMPOSITE_WORDS(sits) + ".\n");
      }
      if (room_sits_obj[siedzenie][4] != cur_przyp) {
        continue;
      }
      if (member_array(zaimek, room_sits_obj[siedzenie][3]) == -1) {
        continue;
      }
      if((room_sits_obj[siedzenie][2]!=0)&&
          (room_sits_obj[siedzenie][2]
           <= (sizeof(filter(FILTER_LIVE(all_inventory(this_object())),
               check_sit2)) +
             2*sizeof(filter(FILTER_LIVE(all_inventory(this_object())),
                 check_lie2)) +
             kladzenie)))
      {
        notify_fail("Ju� si� nie zmie�cisz.\n");
        final_notify_fail = 1;
        continue;
      }

      this_player()->add_prop(OBJ_I_DONT_GLANCE, 1);
      siedzenie->add_prop(OBJ_I_DONT_GLANCE,1);
      if (kladzenie) {
        this_player()->add_prop(SIT_LEZACY, siedzenie);
        this_player()->add_prop(PLAYER_O_LEZE,siedzenie);
        write("K�adziesz si� " + room_sits_obj[siedzenie][0] + ".\n");
        saybb(QCTNAME(this_player()) + " k�adzie si� " + room_sits_obj[siedzenie][0]+".\n");
        if ((!siedzenie->query_prop(SIT_I_NUM_SIE)) &&
                (!siedzenie->query_prop(SIT_I_NUM_LEZ))) {
            siedzenie->add_prop("_sit_old_obj_m_no_get",siedzenie->query_prop(OBJ_M_NO_GET));
        }
        siedzenie->add_prop(SIT_I_NUM_LEZ, siedzenie->query_prop(SIT_I_NUM_LEZ) + 1);
        siedzenie->add_prop(OBJ_M_NO_GET,"Kto� tam le�y.\n");
      }
      else {
        this_player()->add_prop(SIT_SIEDZACY, siedzenie);
        this_player()->add_prop(PLAYER_O_SIEDZE,siedzenie);
        write("Siadasz " + room_sits_obj[siedzenie][0] + ".\n");
        saybb(QCTNAME(this_player()) + " siada " + room_sits_obj[siedzenie][0]+".\n");
        if ((!siedzenie->query_prop(SIT_I_NUM_SIE)) &&
                (!siedzenie->query_prop(SIT_I_NUM_LEZ))) {
            siedzenie->add_prop("_sit_old_obj_m_no_get",siedzenie->query_prop(OBJ_M_NO_GET));
        }
        siedzenie->add_prop(SIT_I_NUM_SIE, siedzenie->query_prop(SIT_I_NUM_SIE) + 1);
        siedzenie->add_prop(OBJ_M_NO_GET,"Kto� tam siedzi.\n");
      }
      return 1;
    }
  }
  return 0;
}

/*
 * Function name: do_not_stand_up
 * Opis         : Funkcja wywolywana jezeli gracz siedzi i  ma propa:
 *                SIT_NIE_WSTAN

 */
public void
do_not_stand_up()
{
    write("Nie mo�esz teraz wsta�.\n");
}

public int
wstan()
{
    mixed sit = this_player()->query_prop(SIT_SIEDZACY);
    string real_cmd;
    object *obs;
    int i, lezal = 0;

    if (!sit) {
      sit = this_player()->query_prop(SIT_LEZACY);
      lezal = 1;
    }

    if(!sit)
    {
        notify_fail("Przecie� nigdzie nie siedzisz.\n");
        return 0;
    }


    if (this_player()->query_prop(SIT_NIE_WSTAN))
    {
        do_not_stand_up();
        return 1;
    }
    if(!objectp(sit))
    for (i = 0; i < sizeof(room_sits); i += 4)
    {
        this_sit = room_sits[i];
        real_cmd = room_sits[i];
        if (equiv_array(sit, real_cmd))
        {
            this_player()->remove_prop(SIT_SIEDZACY);
            this_player()->remove_prop(SIT_LEZACY);
            this_player()->remove_prop(OBJ_I_DONT_GLANCE);
            this_player()->remove_prop(PLAYER_O_SIEDZE);
            this_player()->remove_prop(PLAYER_O_LEZE);
            write("Wstajesz " + room_sits[i + 2] + ".\n");
            saybb(QCTNAME(this_player()) + " wstaje " + room_sits[i + 2] +".\n");
           return 1;
        }
    }
    if(objectp(sit))
    {
        if(sizeof(room_sits_obj[sit]))
        {
            sit_obj=sit;
            this_player()->remove_prop(SIT_SIEDZACY);
            this_player()->remove_prop(SIT_LEZACY);
            this_player()->remove_prop(OBJ_I_DONT_GLANCE);
            this_player()->remove_prop(PLAYER_O_SIEDZE);
            this_player()->remove_prop(PLAYER_O_LEZE);

            if (!sizeof(filter(FILTER_LIVE(
            all_inventory(this_object()))-({this_player()}),
            check_sit2)))
            {
                sit->remove_prop(OBJ_I_DONT_GLANCE);
            }
                if (lezal) {
        sit->add_prop(SIT_I_NUM_LEZ, sit->query_prop(SIT_I_NUM_LEZ) - 1);
        if (!sit->query_prop(SIT_I_NUM_LEZ)) {
            sit->add_prop(OBJ_M_NO_GET,"Kto� tam siedzi.\n");
        }
                }
                else {
        sit->add_prop(SIT_I_NUM_SIE, sit->query_prop(SIT_I_NUM_SIE) - 1);
        if (!sit->query_prop(SIT_I_NUM_SIE)) {
            sit->add_prop(OBJ_M_NO_GET,"Kto� tam le�y.\n");
        }
                }

        if ((!sit->query_prop(SIT_I_NUM_SIE)) &&
                (!sit->query_prop(SIT_I_NUM_LEZ))) {
		    sit->add_prop(OBJ_M_NO_GET, sit->query_prop("_sit_old_obj_m_no_get"));
		    sit->remove_prop("_sit_old_obj_m_no_get");
        }
            write("Wstajesz " + room_sits_obj[sit][1] + ".\n");
            saybb(QCTNAME(this_player()) + " wstaje " + room_sits_obj[sit][1] +".\n");
            return 1;
        }
    }
    if(this_player()->query_prop(SIT_SIEDZACY))
    {
        this_player()->remove_prop(SIT_SIEDZACY);
        this_player()->remove_prop(OBJ_I_DONT_GLANCE);
        this_player()->remove_prop(PLAYER_O_SIEDZE);
        write("Wstajesz cho� w zasadzie to nigdzie nie siedzia�" +
              this_player()->koncowka("e�", "a�") + "...\n");
        return 1;
    }
    else if (this_player()->query_prop(SIT_LEZACY)) {
        this_player()->remove_prop(SIT_LEZACY);
        this_player()->remove_prop(OBJ_I_DONT_GLANCE);
        this_player()->remove_prop(PLAYER_O_LEZE);
        write("Wstajesz cho� w zasadzie to nigdzie nie le�a�" +
              this_player()->koncowka("e�", "a�") + "...\n");
        return 1;
    }

    return 0;
}

public int
dosiadz(string str)
{
    object *arr;
    mixed sit;
    int kladzenie = 0;

    if (query_verb() ~= "po��") {
      if (str == 0) {
        return 0;
      }
      kladzenie = 1;
      if (!(explode(str, " ")[0] ~= "si�")) {
        return 0;
      }
      notify_fail("Po�� si� przy kim?\n");
    }
    else {
      notify_fail("Dosi�d� si� do kogo?\n");
    }

    if (!str) return 0;

    if (kladzenie) {
      if (!parse_command(str, environment(this_player()), "'si�' 'przy' %l:" +
            PL_MIE, arr))
        return 0;
    }
    else {
      if (!parse_command(str, environment(this_player()), "'si�' 'do' %l:" +
            PL_DOP, arr))
        return 0;
    }

    arr = CMDPARSE_STD->normal_access(arr, 0, this_object());
    if (!arr || sizeof(arr) == 0)
        return 0;


    if (sizeof(arr) > 1)
    {
      if (kladzenie) {
        notify_fail("Zdecyduj si�, przy kim chcesz si� po�o�y�.\n");
      }
      else {
        notify_fail("Zdecyduj si�, do kogo chcesz si� dosi���.\n");
      }
        return 0;
    }

    if ((!arr[0]->query_prop(SIT_SIEDZACY)) && (!arr[0]->query_prop(SIT_LEZACY)))
    {
      if (kladzenie) {
        notify_fail("Ta osoba nigdzie przecie� nie le�y...\n");
      }
      else {
        notify_fail("Ta osoba nigdzie przecie� nie siedzi...\n");
      }
      return 0;
    }

    if (!arr[0]->query_prop(SIT_SIEDZACY)) {
      sit=arr[0]->query_prop(SIT_LEZACY);
    }
    else {
      sit=arr[0]->query_prop(SIT_SIEDZACY);
    }

    if(objectp(sit))
    {
        sit_obj=sit;
        if(sizeof(room_sits_obj[sit_obj]))
        {
            if((room_sits_obj[sit_obj][2]!=0)&&
            (room_sits_obj[sit_obj][2]
             <= (sizeof(filter(FILTER_LIVE(all_inventory(this_object())),
                 check_sit2)) +
             2*sizeof(filter(FILTER_LIVE(all_inventory(this_object())),
                 check_lie2)) +
             kladzenie)))
            {
              notify_fail("Ju� si� nie zmie�cisz.\n");
              return 0;
            }

            this_player()->add_prop(OBJ_I_DONT_GLANCE, 1);
            if (kladzenie) {
              this_player()->add_prop(SIT_LEZACY, sit_obj);
              this_player()->add_prop(PLAYER_O_LEZE,sit_obj);
              write("K�adziesz si� " + room_sits_obj[sit_obj][0] + ".\n");
              saybb(QCTNAME(this_player()) + " k�adzie si� " + room_sits_obj[sit_obj][0]+".\n");
              sit_obj->add_prop(SIT_I_NUM_LEZ, sit_obj->query_prop(SIT_I_NUM_LEZ) + 1);
              sit_obj->add_prop(OBJ_M_NO_GET,"Kto� tam le�y.\n");
            }
            else {
              this_player()->add_prop(SIT_SIEDZACY, sit_obj);
              this_player()->add_prop(PLAYER_O_SIEDZE,sit_obj);
              write("Siadasz " + room_sits_obj[sit_obj][0] + ".\n");
              saybb(QCTNAME(this_player()) + " siada " + room_sits_obj[sit_obj][0]+".\n");
              sit_obj->add_prop(SIT_I_NUM_SIE, sit_obj->query_prop(SIT_I_NUM_SIE) + 1);
              sit_obj->add_prop(OBJ_M_NO_GET,"Kto� tam siedzi.\n");
            }
            return 1;
        }
    }
    else
    {
      if (kladzenie) {
        return usiadz("si� " + sit[0]);
      }
      else {
        return usiadz(sit[0]);
      }
    }
    return 0;
}

public int
no_command()
{
    write("Mo�e najpierw wsta�.\n");

    return 1;
}

public int
blok_sit(string str)
{
    string verb;
    string *nie_dozwolone;
    string *nie_dozwolone2;

    nie_dozwolone = this_object()->query_exit_cmds();
    nie_dozwolone2 = ({"zata�cz","podskocz",
    "nadepnij","zabij","kopnij","uk�o�",
    "pok�o�","dygnij","podrepcz",
    "przebieraj","przest�p","tupnij"});

    verb = query_verb();

    if(this_player()->query_prop(SIT_SIEDZACY) || this_player()->query_prop(SIT_LEZACY))
    {
        if ((member_array(verb,nie_dozwolone)==-1)&&(member_array(verb,nie_dozwolone2)==-
1))
            return 0;
        else

            return no_command();
    }

    return 0;
}

string
show_sit()
{
  int index;
  int size;
  int i;
  object *sit;
  object *lie;
  object *tmp_sit;
  object *objs;
  string *msg_rest, msg_this_player, msg;
  string dodatek="";
  size = sizeof(room_sits);
  mapping tmpItems = ([]);

  index = -4;
  while((index += 4) < size)

  {
    this_sit = (room_sits[index]);
    sit = filter(FILTER_LIVE(all_inventory(this_object())), check_sit);
    lie = filter(FILTER_LIVE(all_inventory(this_object())), check_lie);
    if (equiv_array(this_player()->query_prop(SIT_SIEDZACY), this_sit))
    {
      if (sizeof(sit) > 1)
      {
        sit -= ({this_player()});
        msg_this_player = "Siedzisz " + room_sits[index+1] + " razem z " +
          COMPOSITE_FILE->desc_live(sit, PL_NAR, 1);
      }
      else
      {
        if (!sizeof(lie)) {
          msg_this_player = "Siedzisz samotnie " + room_sits[index+1];
        }
        else {
          msg_this_player = "Siedzisz " + room_sits[index+1];
        }
      }
      if (sizeof(lie)) {
        msg_this_player += ", a obok le�" + ((sizeof(lie) == 1) ? "y": "�") + " " +
          COMPOSITE_FILE->desc_live(lie, PL_MIA, 1);
      }
    }
    else if (equiv_array(this_player()->query_prop(SIT_LEZACY),  this_sit))
    {
      if (sizeof(lie) > 1)
      {
        lie -= ({this_player()});
        msg_this_player = "Le�ysz " + room_sits[index+1] + " razem z " +
          COMPOSITE_FILE->desc_live(lie, PL_NAR, 1);
      }
      else
      {
        if (!sizeof(sit)) {
          msg_this_player = "Le�ysz samotnie " + room_sits[index+1];
        }
        else {
          msg_this_player = "Le�ysz " + room_sits[index+1];
        }
      }
      if (sizeof(sit)) {
        msg_this_player += ", a obok siedz" + ((sizeof(sit) == 1) ? "i": "�") + " " +
          COMPOSITE_FILE->desc_live(sit, PL_MIA, 1);
      }
    }
    else
    {
      if(sizeof(sit) > 1)
      {
        msg = room_sits[index+1] + " siedz� " +
          COMPOSITE_FILE->desc_live(sit, PL_MIA, 1);
        if (sizeof(lie) > 1) {
          msg += " i le�� " +
            COMPOSITE_FILE->desc_live(lie, PL_MIA, 1);
        }
        else if (sizeof(lie) == 1) {
          msg += " i le�y " +
            COMPOSITE_FILE->desc_live(lie, PL_MIA, 1);
        }
        if (pointerp(msg_rest))
          msg_rest = msg_rest + ({ msg });
        else
          msg_rest = ({ msg });
      }
      else if (sizeof(sit) == 1)
      {
        msg = room_sits[index+1] + " siedzi " +
          COMPOSITE_FILE->desc_live(sit, PL_MIA, 1);
        if (sizeof(lie) > 1) {
          msg += " i le�� " +
            COMPOSITE_FILE->desc_live(lie, PL_MIA, 1);
        }
        else if (sizeof(lie) == 1) {
          msg += " i le�y " +
            COMPOSITE_FILE->desc_live(lie, PL_MIA, 1);
        }
        if (pointerp(msg_rest))
          msg_rest = msg_rest + ({ msg });
        else
          msg_rest = ({ msg });
      }
      else {
        if (sizeof(lie) > 1) {
          msg = room_sits[index+1] + " le�� " +
            COMPOSITE_FILE->desc_live(lie, PL_MIA, 1);
        }
        else if (sizeof(lie) == 1) {
          msg = room_sits[index+1] + " le�y " +
            COMPOSITE_FILE->desc_live(lie, PL_MIA, 1);
        }
        else {
          msg = 0;
        }
        if (stringp(msg)) {
          if (pointerp(msg_rest))
            msg_rest = msg_rest + ({ msg });
          else
            msg_rest = ({ msg });
        }
      }

    }

  }
  //obiekty...
  objs=m_indexes(room_sits_obj);
  if(!sizeof(objs) && size==0) return "";
  if(sizeof(objs))
  {
    for(i=0;i<sizeof(objs);i++)
    {
      sit_obj=objs[i];
      tmpItems[sit_obj->short(PL_MIE)] += 1;
      sit = filter(FILTER_LIVE(all_inventory(this_object())), check_sit2);
      lie = filter(FILTER_LIVE(all_inventory(this_object())), check_lie2);
      if(this_player()->query_prop(SIT_SIEDZACY)==sit_obj)
      {
        if(sizeof(sit)>1)
        {
          sit -= ({this_player()});
          msg_this_player = "Siedzisz " + room_sits_obj[sit_obj][5] + " " +
            sit_obj->short(room_sits_obj[sit_obj][4]) + " razem z " +
            COMPOSITE_FILE->desc_live(sit, PL_NAR, 1);
        }
        else
        {
          if (!sizeof(lie)) {
            msg_this_player = "Siedzisz samotnie " + room_sits_obj[sit_obj][5] + " " +
              sit_obj->short(room_sits_obj[sit_obj][4]);
          }
          else {
            msg_this_player = "Siedzisz " + room_sits_obj[sit_obj][5] + " " +
              sit_obj->short(room_sits_obj[sit_obj][4]);
          }
        }
        if (sizeof(lie)) {
          msg_this_player += ", a obok le�" + ((sizeof(lie) == 1) ? "y": "�") + " " +
            COMPOSITE_FILE->desc_live(lie, PL_MIA, 1);
        }
      }
      else if(this_player()->query_prop(SIT_LEZACY)==sit_obj)
      {
        if(sizeof(lie)>1)
        {
          lie -= ({this_player()});
          msg_this_player = "Le�ysz " + room_sits_obj[sit_obj][5] + " " +
            sit_obj->short(room_sits_obj[sit_obj][4]) + " razem z " +
            COMPOSITE_FILE->desc_live(lie, PL_NAR, 1);
        }
        else
        {
          if (!sizeof(sit)) {
            msg_this_player = "Le�ysz samotnie " + room_sits_obj[sit_obj][5] + " " +
              sit_obj->short(room_sits_obj[sit_obj][4]);
          }
          else {
            msg_this_player = "Le�ysz " + room_sits_obj[sit_obj][5] + " " +
              sit_obj->short(room_sits_obj[sit_obj][4]);
          }
        }
        if (sizeof(sit)) {
          msg_this_player += ", a obok siedz" + ((sizeof(sit) == 1) ? "i": "�") + " " +
            COMPOSITE_FILE->desc_live(sit, PL_MIA, 1);
        }
      }
      else
      {
        if (tmpItems[sit_obj->short(PL_MIE)]==1)
          dodatek = "";
        else
          dodatek = LANG_SORD(tmpItems[sit_obj->short(PL_MIE)],
              room_sits_obj[sit_obj][4], sit_obj->query_rodzaj())+" ";
        if(sizeof(sit) > 1)
        {
          msg = room_sits_obj[sit_obj][5]+" "+dodatek+sit_obj->short(room_sits_obj[sit_obj][4]) +
            " siedz� " +
            COMPOSITE_FILE->desc_live(sit, PL_MIA, 1);
          if (sizeof(lie) > 1) {
            msg += " i le�� " +
              COMPOSITE_FILE->desc_live(lie, PL_MIA, 1);
          }
          else if (sizeof(lie) == 1) {
            msg += " i le�y " +
              COMPOSITE_FILE->desc_live(lie, PL_MIA, 1);
          }
          if (pointerp(msg_rest))
            msg_rest = msg_rest + ({ msg });
          else
            msg_rest = ({ msg });

        }
        else if (sizeof(sit) == 1)
        {
          msg = room_sits_obj[sit_obj][5]+" "+dodatek+sit_obj->short(room_sits_obj[sit_obj][4]) +
            " siedzi " +
            COMPOSITE_FILE->desc_live(sit, PL_MIA, 1);
          if (sizeof(lie) > 1) {
            msg += " i le�� " +
              COMPOSITE_FILE->desc_live(lie, PL_MIA, 1);
          }
          else if (sizeof(lie) == 1) {
            msg += " i le�y " +
              COMPOSITE_FILE->desc_live(lie, PL_MIA, 1);
          }
          if (pointerp(msg_rest))
            msg_rest = msg_rest + ({ msg });
          else
            msg_rest = ({ msg });

        }
        else {
          if (sizeof(lie) > 1) {
            msg = room_sits_obj[sit_obj][5]+" "+dodatek+sit_obj->short(room_sits_obj[sit_obj][4]) +
              " le�� " +
              COMPOSITE_FILE->desc_live(lie, PL_MIA, 1);
          }
          else if (sizeof(lie) == 1) {
            msg = room_sits_obj[sit_obj][5]+" "+dodatek+sit_obj->short(room_sits_obj[sit_obj][4]) +
              " le�y " +
              COMPOSITE_FILE->desc_live(lie, PL_MIA, 1);
          }
          else {
            msg = 0;
          }
          if (stringp(msg)) {
            if (pointerp(msg_rest))
              msg_rest = msg_rest + ({ msg });
            else
              msg_rest = ({ msg });
          }
        }
      }
    }
  }
  if (!msg_this_player)
  {
    if (sizeof(msg_rest) == 0)
      return "";

    return capitalize(COMPOSITE_WORDS2(msg_rest, ", a ")) + ".\n";
  }
  else
  {
    if (sizeof(msg_rest) != 0)
    {
      if (sizeof(msg_rest) > 1)
        msg_this_player += ", ";
      else
        msg_this_player += ", a ";
    }
    return msg_this_player + COMPOSITE_WORDS2(msg_rest, ", a ") + ".\n";

  }
}


public int
pomoc_siadania(string str)
{
    string *sits = query_sit_cmds();

    if (sizeof(sits))
        write("Mo�na tu usi��� " + COMPOSITE_WORDS(sits) + ".\n");
    else
        write("Nie mo�na tu nigdzie usi���.\n");

    return 1;

}

public int
pomoc_kladzenia(string str)
{
  if (!(stringp(str) && (str ~= "si�"))) {
    return 0;
  }
  string *sits = query_sit_cmds();


  if (sizeof(sits))
    write("Mo�na tu si� po�o�y� " + COMPOSITE_WORDS(sits) + ".\n");
  else
    write("Nie mo�na tu nigdzie si� po�o�y�.\n");

  return 1;

}

public void
init_sit()
{
     add_action(usiadz, "usi�d�");
     add_action(usiadz, "po��");
     add_action(dosiadz, "po��");
     add_action(wstan, "wsta�");
     add_action(dosiadz, "dosi�d�");
     add_action(blok_sit, "", 1);
     add_action(pomoc_siadania, "?usi�d�");
     add_action(pomoc_kladzenia, "?po��");
}



void
leave_inv_sit(object o)
{
	object oldtp;
        if (o->query_prop(SIT_SIEDZACY) || o->query_prop(SIT_LEZACY))
	{
		oldtp = this_player();
		set_this_player(o);
		wstan();
		set_this_player(oldtp);
	}
}
void
kolesie_wstaja(object ob)
{
    int i;
    object *sit;

    if(room_sits_obj[ob])
    {
        sit_obj=ob;
        sit = filter(FILTER_LIVE(all_inventory(this_object())), check_sit2);
        for(i=0;i<sizeof(sit);i++)
        {
            leave_inv_sit(sit[i]);
        }
    }
}
