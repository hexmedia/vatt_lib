/**
 * Lib wspomagaj�cy dodawanie Event�w.
 *
 * @author Rantaur
 * @author Krun - przer�bka na lib
 *
 */

#pragma no_clone
#pragma strict_types

#include <files.h>
#include <macros.h>

/* Zmienne */
static int      last_event = -1,    //indeks ostatniego eventu
                event_alarm,        //event w tablicy event�w w globalnym systemie
                stop_on_next_eval;          //
static float    event_time,         //czas co jaki wykonywany jest event
                event_rand,         //dodatkowy czas losowy
                last_event_time;    //czas jaki trwa�o wykonanie ostatniego alarmu
static mixed    eventy = ({}),
                event_fun;
static object   event_obj;


public nomask   void    prepare_show_event();
public          mixed   query_event(int i);

public varargs void wystartuj_alarm(int n = 0)
{
    if(!sizeof(eventy) && !event_fun)
        return;

    remove_alarm(event_alarm);

    if(!n)
        last_event_time = event_time + frandom(event_rand, 2);
    else
        last_event_time = frandom(event_time + event_rand, 2);

    event_alarm = set_alarm(last_event_time, 0.0, &prepare_show_event());
}

/**
 * Ustawia czas co jaki pokazywane b�d� eventy
 * Funkcja wsp�pracuje z globalnym systemem
 * event�w umieszczonym w /secure/event_master.c
 *
 * @param czas - czas co jaki pokazywane s� eventy
 * @param rand - warto�� losowa dodawana do czasu
 */
public nomask varargs void
set_event_time(mixed czas, mixed rand=0.0)
{
    if(!floatp(czas))
    {
        if(intp(czas))
            czas = itof(czas);
        else
            return;
    }

    if(!floatp(rand))
    {
        if(intp(rand))
            rand = itof(rand);
        else
            return;
    }

    if(!czas || czas <= 0.0)
        return;

    event_time = czas;
    event_rand = rand;

    //Tu niestety, ale, �eby mo�na by�o w dowolnej kolejno�ci dodawa� musi
    //by� alarm.
    set_alarm(0.1, 0.0, &wystartuj_alarm());
}

/**
 * Dodajemy event jaki b�dzie pokazywany na lokacji
 *
 * @param jaki tekst wysy�any w evencie (przyjmuje r�wnie� tablic�:
 *             ({dla_tp, dla_reszty})
 * @param func funkcja wywo�ywana wraz z wywo�aniem
 *             eventu
 */
public varargs void add_event(mixed jaki, mixed func = 0)
{
    if(!jaki || !stringp(jaki) || pointerp(jaki) && fold(map(jaki, &stringp()), &operator(-)(,), sizeof(jaki)))
        return;

    eventy += ({ ({ jaki, func }) });

    if(!event_alarm)
        wystartuj_alarm();
}


/**
 * Funckja wywo�ywana do sprawdzenia czy eventy s� rzeczywi�cie potrzebne,
 * czy na lokacji jest jaki� gracz, je�li nikogo nie ma to poczeka do
 * nast�pnego wywo�ania alarmu i je�li w tym czasie nic si� nie zmieni
 * to zaprzestanie wywo�a�, za� je�li kto� jest a alaramry s� wy��czone
 * to je w�aczy na losowy czas z przedzia�u wykonywania si� alarm�w.
 */
public void event_check()
{
    if(sizeof(filter(AI(TO) + (ENV(TO) ? AIE(TO) : ({})), &interactive())))
    {
        if(!event_alarm)
            wystartuj_alarm(1);
    }
    else if(event_alarm)
        stop_on_next_eval;
}

/**
 * Funkcja pozwala na zatrzymanie wszystkich dodanych przez ten obiekt event�w.
 */
public void stop_events()
{
    remove_alarm(event_alarm);
}

/**
 * Funkcja pozwala na wznowienie event�w
 */
public void start_events()
{
    event_check();
}

/**
 * Funkcja ta standardowo nie robi nic i powinna by� edytowana w ka�dym
 * obiekcie na kt�rym jest wywo�ywana. Standardowe wykonanie to np.
 *
 * @example
 *  public void show_event(mixed event)
 *  {
 *      if(event[0])
 *          tell_roombb(this_object(), event[0], 0, 0, 1);
 *  }
 *
 */
public void show_event(int index_eventu)
{
    //Ta funcyjka jest tylko po to coby sie nie rzuca�, �e jej nie ma
    //tre�� pozostaawimy pust�, do edycji w odpowiednich plikach.

    ;
}

public nomask void prepare_show_event()
{
    if(event_fun)
    {
        if(stringp(event_fun))
        {
            call_other(event_obj, event_fun);
        }
        else if(functionp(event_fun))
        {
            function a = event_fun;
            a();
        }
    }
    else if(sizeof(eventy))
    {
        int index;

        if(sizeof(eventy) > 1)
            while(last_event == (index = random(sizeof(eventy))));


        if(!sizeof(eventy[index]))
            return;

        show_event(eventy[index]);

        if(eventy[index][1])
        {
            if(stringp(eventy[index][1]))
                call_other(eventy[index][1], TO);
            else if(functionp(eventy[index][1]))
            {
                function a = eventy[index][1];
                a();
            }
        }
    }

    if(!stop_on_next_eval)
        wystartuj_alarm();
}

/**
 * Je�li w naszym obiekcie ustawimy t� funkcje to wszystkie eventy dodane przez add_event
 * zostan� przez nasz system olane a w zamian za nie pokazane zostan� eventy z funkcji
 * i obiektu tu ustawionego. Nie jest to zalecane dzia�anie, jednak je�li jest taka potrzeba
 * mo�na tej funkcji u�y�.
 *
 * @param func funkcja wy�wietlaj�ca w�asne eventy
 * @param ob   obiekt na kt�rym powy�sza funkcja b�dzie wywo�ywana (domy�lnie: this_object())
 *
 */
public varargs void set_event_fun(mixed func, object ob = this_player())
{
    event_fun = func;
    event_obj = ob;
}

/**
 * !!!UWAGA!!! FUNKCJA WYCOFYWANA
 * Prosz� zobaczy� na man set_event_fun, tam da si� to samo ustawi�.
 * Ta funkcja pozostawiona zaosta�a dla zgodno�ci ze star� wersj� event�w.
 *
 */
public void set_event_ob(object ob)
{
    event_obj = ob;
}

/**
 * !!!UWAGA!!! FUNKCJA WYCOFYWANA
 * Prosz� zobaczy� na man set_event_fun, tam da si� to samo ustawi�.
 * Ta funkcja pozostawiona zaosta�a dla zgodno�ci ze star� wersj� event�w.
 *
 */
public void set_event_object(object ob)
{
    event_obj = ob;
}

/**
 * @return Zwraca za ile sekund wykonany zostanie nast�pny event.
 * @return 0 nie wykonujemy �adnych alarm�w.
 */
public float query_next_event_time()
{
    if(!event_alarm)
        return 0;

    return get_alarm(event_alarm)[2];
}

/**
 * @return czas co jaki pokazywane s� eventy(do tego czasu nie dodany jest randomowy modyfikator)
 */
public float query_event_time()
{
    return event_time;
}

/**
 * @return czas jaki dodawany jest do czasu pokazywania si� eventa w spos�b randomowy.
 */
public float query_event_rand()
{
    return event_rand;
}

/**
 * @return wszystkie eventy jakie mog� zosta� wykonane z danego obiektu
 */
public mixed * query_eventy()
{
    return eventy;
}

/**
 * @return zwraca konktretny event o indexie podanym w argumencie.
 */
public mixed query_event(int i)
{
    return eventy[i];
}

/**
 * @return czas jaki zaje�o wykonanie ostatniego alarmu
 */
public float query_last_event_time()
{
    return last_event_time;
}