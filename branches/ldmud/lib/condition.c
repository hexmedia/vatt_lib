/**
 * \file /lib/condition.c
 *
 * Plik zawiera funkcje odpowiedzialne za niszczenie si� przedmiot�w.
 *
 * @author      Krun
 * @date        11.04.2008
 * @version     1.0
 *
 * WARNING:
 *  Wi�ksza cz�c tego pliku zosta�a skopiowana.
 */

#pragma no_clone
#pragma no_reset
#pragma save_binary
#pragma strict_types

#include <macros.h>
#include <stdproperties.h>

static  int
        repair,             /* Modyfikator naprawiania pancerza */
        condition,          /* Obecna kondycja */
        likely_cond,        /* Jak �atwo zniszczy� */
        likely_break;       /* Jak �atwo z�ama� */


/**
 * Zwraca modyfikator stanu pancerza. Okre�la on, ile razy stan pancerza
 * zosta� pogorszony. Prawdziwy stan pancerza wylicza si� ze wzoru:
 *
 * <code>base_ac - condition + repairs</code>
 *
 * @return modyfikator stanu pancerza
 */
int
query_condition()
{
    return condition;
}

/**
 * Ustawia sk�onno�� pancerza do pogorszenia stanu podczas parowania.
 *
 * @param i sk�onno�� pancerza do pogorszenia stanu. Rekomendowana
 * warto�� z zakresu [0, 30].
 */
void
set_likely_cond(int i)
{
    likely_cond = i;
}

/**
 * Zwraca jak bardzo pancerz jest sk�onny do pogorszenia stanu podczas
 * parowania.
 *
 * @return jak bardzo pancerz jest sk�onny do pogorszenia stanu
 */
int
query_likely_cond()
{
    return likely_cond;
}

/**
 * Ustawia sk�onno�� pancerza do zniszczenia podczas parowania.
 *
 * @param i sk�onno�� pancerza do zniszczenia. Rekomendowana warto��
 * z zakresu [0, 20].
 */
void
set_likely_break(int i)
{
    likely_break = i;
}

/**
 * Zwraca jak bardzo pancerz jest sk�onny do zniszczenia podczas parowania.
 *
 * @return jak bardzo pancerz jest sk�onny do zniszczenia
 */
int
query_likely_break()
{
    return likely_break;
}

/**
 * Kiedy naprawia si� pancerz, trzeba wywo�a� t� funkcj�. Naprawianie mo�e
 * tylko podnie�� wsp�czynnik naprawienia.
 *
 * @param rep - nowy wsp�czynnik naprawienia.
 *
 * @return <b>1</b> je�eli operacja si� powiod�a
 */
int
set_repair(int rep)
{
    if (rep > repair && TO->legal_repair(rep, condition))
    {
        repair = rep;

        TO->repair_hook();

        return 1;
    }
    return 0;
}

/**
 * Zwraca jak wiele razy by� ju� naprawiany ten pancerz. Bie��ce ac wylicza si�
 * ze wzoru:
 *
 * <code>base_ac - condition + repairs</code>
 *
 * @return jak wiele razy pancerz by� ju� naprawiany
 */
int
query_repair()
{
    return repair;
}

/**
 * Funkcja ta ustawia stan skorodowania pancerza. Je�eli pancerz zostanie
 * wgnieciony lub co� innego pogorszy jego stan, wywo�aj t� funkcj�.
 *
 * @param cond nowy stan skorodowania (mo�emy go tylko zwi�kszy�)
 *
 * @return <b>1</b> je�eli stan zosta� zaakceptowany
 */
int
set_condition(int cond)
{
    if (cond > condition)
    {
        condition = cond;

        TO->condition_hook();

        return 1;
    }
    return 0;
}

/**
 * Funkcja do zapami�tywania kondycji.
 */
public string query_condition_auto_load()
{
    return "#<<<COND#" + condition + "," + repair + "," + TO->query_prop(OBJ_I_BROKEN) + "#COND>>>";
}

public string init_condition_arg(string arg)
{
    if(!arg)
        return arg;

    int brok;
    string pre, post;
    if(sscanf(arg, "%s#<<<COND#%d,%d,%d#COND>>>#%s", pre, condition, repair, brok, post) == 5)
    {
        if(brok)
            TO->add_prop(OBJ_I_BROKEN, brok);

        return pre + post;
    }
    else
        return arg;
}