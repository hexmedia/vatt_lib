/**
 *   \file /lib/trade.c
 *
 *   This is a standard trade object with some nice funktions.
 *   Made by Nick, the ever smiling.
 *   Translated into polish by Molder.
 *
 *   Je�eli piszesz sklep, albo pub, albo cokolwiek gdzie obracasz
 *   pieni�dzmi, mo�esz dziedziczy� ten plik i u�atwi� sobie zadanie.
 *
 *   Keep on smiling, /Nick
 *
 *   Typowe u�ycie tego pliku:
 *
 *   inherit "trade.c";
 *
 *   create_room() {
 *       ...
 *       config_default_trade();
 *       [ inne funkcje dotycz�ce trade ]
 *       ...
 *   }
 *
 *   nast�pnie mo�esz wywo�a�:
 *
 *   pay(int amount, object who_pays, string str, int test,
 *       object who_gets, string str2, int silent)
 *   give(int amount, object who_gets, string str, int test,
 *       object who_give, int silent)
 *
 *   W obu funkcjach wymagany jest tylko pierwszy argument - 'amount'.
 *
 *   W za�o�eniu gracz p�aci nam u�ywaj�c 'pay', my p�acimy graczowi u�ywaj�c 'give',
 *   czyli pierwszy obiekt to zawsze this_player(), drugi to this_object(). <br>
 *   Wychodz�c z takich za�o�e�, je�eli nie podasz pierwszego obietku,
 *   wzi�ty zostanie this_player(). Je�li nie podasz drugiego, to pieni�dze zostan�
 *   stworzone przy give, a zniszczone przy pay.
 *
 *   Je�li test == 1 zostanie zwr�cona tablica, ale bez przemieszczenia pieni�dzy.
 *
 *   Je�li jest podany string 'str' lub 'str2' typy monet kt�rymi gracz p�aci i w jakich
 *  chce dosta� reszte zostan� odpowiednio ustawione.
 *
 *   Je�eli silent == 1 nie zostan� wy�wietlone informacje ob��dach przy przenoszeniu
 *   pieni�dzy, na przyk�ad gdy gracz nie mo�e ud�wign�� wszystkich otrzymanych monet
 *   i cz�� z nich upu�ci.
 *
 *                      ----------------------------
 *   Inne uwagi:
 *
 *   Wszystkie funkcje "set_" maj� odpowiadaj�ce im funkcje "query_" .
 *
 *   Wszystkie tablice monet maj� mie� posta�: ({ grosze, denary, korony }),
 *   cho� mo�emy zmniejszy� ilo�� monet kt�r� handlujemy przez set_money_types,
 *   wtedy nale�y u�ywa� odpowiednio mniejszych tablic. W takim przypadku lepiej
 *   jednak u�y� set_money_accept.
 *
 *   Niekt�re funkcje wydaj� si� robi� to samo co odpowiadaj�ce im makra z money.h
 *   (np. what_coins() -> MONEY_COINS(), money_merge() -> MONEY_MERGE itd.).
 *   Nale�y jednak�e u�ywa� funkcji a nie makr, poniewa� makra korzystaj� z globalnych
 *   ustawie� typ�w i w�asno�ci monet, natomiast nasze funkcje z tych kt�re
 *   danemu sprzedawcy ustawili�my.
 *
 *   Fa�szywe monety to te, kt�re nie dziedzicz� po /std/coins.
 *
 *  ***Plik przet�umaczony i przerobiony przez Moldera,***
 *
 *   Kluczowe dla pliku funkcje pay() i give() zakodowane s� przy
 *   u�yciu nast�puj�cej logiki:
 *   Transakcja rozliczana jest w du�ej tablicy, gdzie pierwsza jej cz�� to to, co
 *   zap�aci gracz, a druga to to, co zap�acimy my. Przy pay() oznacza to ilo�� monet
 *   wp�aconych przez gracza i wyp�acan� przez nas reszt�, przy give() pierwsza
 *   cz�� tablicy jest wyzerowana, a my p�acimy graczowi reszt� w wysoko�ci
 *   warto�ci przedmiotu.
 *
 * @author Nick, the every smiling,
 * @author Wersja polska: Molder
 * @author Poprawki i aktualna opieka: Krun
 */

#pragma strict_types
#pragma save_binary

#include <stdproperties.h>
#include <macros.h>
#include <money.h>
#include <ss_types.h>

/* ustawienia dla config_default_trade */
#define MONEY_GIVE_MAX     10000
#define MONEY_GIVE_OUT     ({ 10000,    400,   20 })
#define MONEY_GIVE_REDUCE  ({ 0, 2, 8 })
#define MONEY_ACCEPT       ({ 1, 1, 1 })

#define MONEY_GREED_BUY     100
#define MONEY_GREED_SELL    100
#define MONEY_GREED_CHANGE  100

static  mixed   money_give_max;
static  int    *money_give_out;
static  int    *money_give_reduce;
static  int    *money_accept;
static  mixed   money_greed_buy;
static  mixed   money_greed_sell;
static  mixed   money_greed_change;
static  int    *money_values;
static  string *money_types;
static  string *money_tematy;

/* ilo�� typ�w monet */
static int num_of_types;

/**
 * Ustawia maksymaln� kwot� jak� damy graczowi.
 * @param   m - kwota (int albo mixed)
 */
void set_money_give_max(mixed m) { money_give_max = m; }

/**
 * Ustawia maksymaln� ilo�� ka�dego z typ�w monet jak� mo�emy da�.
 * @param     a - The array of integers
 */
void set_money_give_out(int *a) { money_give_out = a; }

/**
 * Ustawia redukcje dla ka�dego typu monet.
 * Chodzi chyba o to, �eby gracz nie dostawa� zbyt du�o warto�ciowych
 * monet jako reszt� (albo zap�at� za towar). Gracz dostanie odpowiednio
 * wi�cej monet mniej warto�ciowych (chyba, �e ustawisz grosze > 0, wtedy
 * gracz dostanie og�lnie mniej).
 *
 * Np. przy defaultowym ustawieniu ({ 0, 2, 8}) gracz dostanie
 * 107 denar�w i 59 groszy zanim zaczniemy p�aci� dukatami.
 *
 * One reason to reduce is that you will only pay out platinum coins
 * if the seller hands you a very nice object with value > than your
 * reduce factor on platinum, not just as quickly
 * you get a value > the value of platinum.
 *
 * @param     a - tablica redukcji poszczeg�lnych typ�w monet
 */
void set_money_give_reduce(int *a) { money_give_reduce = a; }

/**
 * Ustawia kt�re ze zdefiniowanych typ�w monet przyjmujemy jako zap�at�.
 * @param     a - tablica flag dla typ�w monet (1 - akceptujemy, 0 - nie)
 */
void set_money_accept(int *a) { money_accept = a; }

/**
 * Ustawia chciwo�� gdy gracz co� od nas kupuje.
 * Ustawienie na 100 oznacza, �e cena przy pay()
 * zostanie niezmieniona. Przy >100 gracz zap�aci wi�cej.
 * @param     m - chciwo�� w %, wspiera VBFC
 */
void set_money_greed_buy(mixed m) { money_greed_buy = m; }

/**
 * Ustawia chciwo�� gdy gracz co� nam sprzedaje
 * Ustawienie na 100 oznacza, �e cena przy give()
 * zostanie niezmieniona. Przy >100 gracz dostanie mniej.
 *
 * @param     m - chciwo�� w %, wspiera VBFC
 */
void set_money_greed_sell(mixed m) { money_greed_sell = m; }

/**
 * Ustawia chciwo�� na reszt� kt�r� mamy graczowi wyda�.
 * Ustawienie na 100 oznacza, �e reszta kt�r� gracz dostanie
 * zostanie niezmieniona. Przy >100 gracz dostanie mniejsz� reszt�.
 *
 * @param     m - chciwo�� w %, wspiera VBFC
 */
void set_money_greed_change(mixed m) { money_greed_change = m; }

/**
 * Ustawia warto�� poszczeg�lnych typ�w monet.
 * Nie zmieniaj bez istotnego powodu.
 * @param     a - tablica warto�ci typ�w monet (w groszach)
 */
void set_money_values(int *a) { money_values = a; }

/**
 * Ustawia jakimi typami monet handlujemy.
 * Nie u�ywaj tej funkcji o ile mo�esz u�y� set_money_accept().
 * @param     a - tablica nazw typ�w monet
 */
void set_money_types(string *a)
{
    money_types = a;
    num_of_types = sizeof(a);
}

/**
 * Ustawia tematy odmian nazw rodzajow monet
 * @param     a - tablica temat�w nazw monet
 */
void set_money_tematy(string *a)
{
    money_tematy = a;
}

/**
 * Sprawdza ile co najwy�ej zap�acimy graczowi.
 * @return     warto�� (w groszach)
 */
int query_money_give_max()
{
    return this_object()->check_call(money_give_max);
}

/**
 * Sprawdza nasz� chciwo�� gdy gracz co� od nas kupuje.
 * @return    chciwo�� w %
 */
int query_money_greed_buy()
{
    return this_object()->check_call(money_greed_buy);
}

/**
 * Sprawdza nasz� chciwo�� gdy gracz co� nam sprzedaje.
 * @return    chciwo�� w %
 */
int query_money_greed_sell()
{
    return this_object()->check_call(money_greed_sell);
}

/**
 * Sprawdza nasz� chciwo�� na reszt� kt�r� mamy graczowi wyda�.
 * @return    chciwo�� w %
 */
int query_money_greed_change()
{
    return this_object()->check_call(money_greed_change);
}

/**
 * Sprawdza jakich typ�w monet u�ywamy.
 * @return     tablica nazw typ�w monet
*/
string *query_money_types() { return money_types; }

/**
 * Sprawdza tematy odmian nazw monet kt�rych u�ywamy.
 * @return     tablica temat�w odmian monet
*/
string *query_money_tematy() { return money_tematy; }

/**
 * Sprawdza ile co najwy�ej wyp�acimy graczowi r�nych typ�w monet.
 * @return     tablica ogranicze� wyp�acanych monet danego typu
 */
int *query_money_give_out() { return money_give_out; }

/**
 * Sprawdza o ile redukujemy wyp�aty r�nych typ�w monet.
 * @return     tablica redukcji monet
 */
int *query_money_give_reduce() { return money_give_reduce; }

/**
 * Sprawdza kt�re typy monet akceptujemy przy zap�acie.
 * @return     tablica flag dla typ�w monet
 */
int *query_money_accept() { return money_accept; }

/**
 * Sprawdza jak wyceniamy poszczeg�lne typy monet.
 * @return     tablica warto�ci typ�w monet
 */
int *query_money_values() { return money_values; }

/**
 * Zwraca troch� statystyk dotycz�cych ustawie� handlowych.
 * @return string z ustawieniami
 */
string
stat_trade()
{
    string str;
    int i;

    str = sprintf("\n%-10s %6s %8s %|7s %9s\n", "Typ", "Warto��", "Akceptujemy",
                "Dajemy", "Redukcja");
    str += sprintf("%-10s %6s %8s %|7s %9s\n", "====", "=====", "======",
                "====", "======");
    for (i = 0; i < num_of_types; i++)
        str += sprintf("%-10s %5d  %|8d %7d %|9d\n", money_types[i],
                money_values[i], money_accept[i], money_give_out[i],
                money_give_reduce[i]);

    str += "\ngive max: " + query_money_give_max() + "   greed sprzeda�y: " +
        query_money_greed_buy() + "   greed skupu: " +
        query_money_greed_sell() + "   greed na reszt�: " +
        query_money_greed_change() + "\n";

    return str;
}

/**
 * Ta funkcja zwraca obiekt, do kt�rego przesuni�te (move)
 * zostan� pieni�dze od gracza, je�eli nie mog�y by� przesuni�te
 * do obiektu do kt�rego mia�y by� przesuni�te.
 *
 * Przedefiniuj t� funkcj� je�li chcesz mie� sprzedawc�, kt�ry
 * m�g�by upu�ci� jakie� monety, albo gdy chcesz mie�
 * kontrol� nad wszystkim.
 *
 * @param      type - numer typu przesuwanych monet
 * @param      sum - przemieszczana kwota
 * @param      error - kod b��du (najpewniej z money_move)
 * @return      standardowo funkcja nic nie zwraca,
 *                    zatem pieni�dze s� niszczone.
 */
varargs mixed
query_extra_money_holder(int type, int sum, int error) { return ; }

/**
 * Oblicza warto�� tablicy monet. Bierze pod uwag� warto�ci monet
 * zdefiniowane funkcj� set_money_values().
 *
 * @param     arr - tablica do policzenia
 * @return     warto�� tablicy (w groszach)
 */
int
money_merge(int *arr)
{
    int v, i;

    if (sizeof(arr) != num_of_types)
        return 0;

    for (v = 0, i = 0; i < num_of_types; i++)
        v += arr[i] * money_values[i];

    return v;
}

/**
 * Sprawdza ile jakich monet posiada obiekt.
 * @param      ob - obiekt w kt�rym szukamy monet.
 * @return      tablica postaci: ({ grosze, denary, korony })
*/
int *
what_coins(object ob)
{
    object cn;
    int i, *nums;
    string *ctypes;

    ctypes = money_types;
    nums = allocate(num_of_types);

    for (i = 0; i < num_of_types; i++)
    {
        cn = present("_" + ctypes[i] + " moneta_", ob);
        if (!cn)
        {
            nums[i] = 0;
            continue;
        }
        else
            nums[i] = cn->num_heap();
    }
    return nums;
}

/**
 * Dzieli warto�� (w groszach) na r�ne rodzaje monet jakich u�ywasz.
 * Bierze pod uwag� warto�ci monet zdefiniowane funkcj� set_money_values().
 * @param      value - warto�� w groszach
 * @return      tablica monet
 */
int *
split_values(int value)
{
    int *arr, i;

    arr = allocate(num_of_types);

    if (value > 0)
    {
        for (i = num_of_types - 1; i>= 0; i--)
        {
            arr[i] = value / money_values[i];
            value %= money_values[i];
        }
    }

    return arr;
}

/**
 * Sprawdzamy, czy obiekt sta� na zap�acenie danej ceny
 * (czy ma w inwentarzu wystarczaj�co du�o pieni�dzy).
 * @param     price - cena (w groszach)
 * @param     ob - obiekt kt�ry sprawdzamy
 * @return     1 - sta�
 * @return     0 - nie sta�
 */
int
can_pay(int price, object ob)
{
    if (!(ob = this_player()))
        return 0;
    return price <= money_merge(what_coins(ob));
}

/**
 * Sprawdzamy czy tablica postaci ({ grosze, denary, korony })
 * wystarczy do op�acenia danej ceny.
 * @param     price - cena (w groszach)
 * @param     arr - tablica postaci ({ grosze, denary, korony })
 * @return     1 - wystarczy
 * @return     0 - nie wystarczy
 */
int
can_pay_arr(int price, int *arr)
{
    return price <= money_merge(arr);
}

/**
 * Sprawdza string i redukuje tablice do tych monet kt�rymi chcemy
 * p�aci� (kt�re chcemy dosta�).
 * @param     str - string opisuj�cy monety z kt�rymi obiekt chce pracowa�
 * @param     arr - tablica przechowuj�ca monety
 * @return     funkcja nic nie zwraca, zmienia tylko tablice podan� jako argument
 *                   zeruj�c w niej te monety kt�rymi nie chcemy obraca�.
 */
void
want_to_pay(int *arr, string str)
{
    string *m_names;
    int i, *tmp_arr, j;

    if (!str || (str == ""))
    	return;

    tmp_arr = allocate(num_of_types);
    m_names = explode(str, " ");

    for (i = 0; i < sizeof(m_names); i++)
	   for (j = 0; j < num_of_types; j++)
	        if (wildmatch(money_tematy[j] + "*", m_names[i]))
		          tmp_arr[j] = 1;

    for (i = 0; i < num_of_types; i++)
	arr[i] *= tmp_arr[i];
}

/**
 * Przepuszczamy tablice postaci ({ grosze, denary, korony})
 * przez to jakie typy monet akceptujemy. <br>
 * Je�li kt�rego� elementu tablicy nie akceptujemy, dostaniemy
 * notify_fail, a warto�� w tablicy zostanie wyzerowana.
 * @param     arr - tablica postaci ({ grosze, denary, korony})
 */
void
we_accept(int *arr)
{
    int i;
    for (i = 0; i < num_of_types; i++)
	if (!money_accept[i] && arr[i])
	{
	    notify_fail("Nie przyjmujemy " + money_types[i] + ".\n");
	    arr[i] = 0;
	}
}

/**
 * Sprawdza w jakich monetach obiekt chce mie� wydan� reszt�.
 * @param     arr - the array holding the coins
 * @param     str - the string describing what types the object wants
 * @return     tablica 'arr' z wyzerowanymi typami kt�rymi nie p�acimy
 */
int *
calc_change_str(int *arr, string str)
{
    int i, j, k, *tmp_arr;
    string *m_names;

    m_names = explode(str, " ");
    tmp_arr = allocate(num_of_types);

    for (i = 0; i < sizeof(m_names); i++)
        for (j = 0; j < num_of_types; j++)
            if (wildmatch(money_tematy[j] + "*", m_names[i]))
                for (k = j; k >= 0; k--)
                    tmp_arr[k] = 1;

    for (i = 0; i < num_of_types; i++)
        arr[i] = arr[i] * tmp_arr[i];

    return arr;
}

/**
 *  Oblicza jak� wyda� obiektowi reszt� uwzgl�dniaj�c cen� i to
 *  co zosta�o zap�acone. Je�eli nie podano tablicy 'arr', to jest to
 *  sprzeda�, i ca�a cena zostanie wyp�acona
 *  (bez pobrania procent�w z chciwo�ci money_greed_change).
 *
 * @param     arr - tablica opisuj�ca ile jakich monet dajemy
 * @param     str - string opisuj�cy jak� chcemy dosta� reszt�
 * @param     price - cena (w groszach)
 * @return     tablica najmniejszej ilo�ci monet op�acaj�cych reszt�
 */
int *
calc_change(int price, int *arr, string str)
{
    int *c_arr, new_price, i, sum, *tmp_arr, max;

    c_arr = allocate(num_of_types);
    if (price < 0)
        return c_arr;

    if (arr) /* No greed on change if this isnt change */
        new_price = (money_merge(arr) - price) * 100 / this_object()->check_call(money_greed_change);
    else
       new_price = price;

    /* od teraz new_price przechowuje reszt� */

    if (str && (str != "")) /* If specified how to get change */
       tmp_arr = calc_change_str(money_give_out + ({ }), str);
    else
       tmp_arr = money_give_out;

    /* this is done once to reduce effect if random give max. */
    max = this_object()->check_call(money_give_max);

    /* W przypadku transakcji w ktorych udzial biora gracze,
     * im wyzszy trading, tym wyzszy max_give_out  */
    if (this_player())
        max += (max * this_player()->query_skill(SS_TRADING) / 200);

    if (max <= new_price)
    {
        max = ((9 + random(3)) * max) / 10;
        new_price = min(new_price, max);
    }

    if (new_price <= 0)
        return c_arr;

    /* jedziemy od najbardziej warto�ciowych monet */
    for (i = num_of_types - 1; i >= 0; i--)
    {
        /* je�eli reszta jest wi�ksza od warto�ci monety
            (kt�r� mo�emy wyp�aci�) */
        if (new_price >= money_values[i] && tmp_arr[i])
        {
            sum = new_price / money_values[i];

            /* redukcja */
            sum -= money_give_reduce[i];

            /* ograniczenie do ilo�ci z money_give_out */
            if (sum > tmp_arr[i])
                sum = tmp_arr[i];

            if (sum > 0)
            {
                c_arr[i] = sum;
                new_price -= c_arr[i] * money_values[i];
            }
	    }
    }

    /* zwraca tablic� najmniejszej ilo�ci monet op�acaj�cych reszt� */
    return c_arr;
}

/**
 * Redukuje liczby poszczeg�lnych monet w tablicy do takich ilo�ci,
 * kt�re spe�niaj� cen�. Zaczyna od groszy.
 * @param     arr - tablica monet
 * @param     price - cena do zap�acenia
 * @param     str - string opisuj�cy w jakich monetach obiekt chce dosta� reszt�
 * @return     zsumowan� tablic� przechowuj�c� na pocz�tku monety kt�rymi
 *                      op�acamy cen�, a na ko�cu monety reszty.
 */
int *
calc_coins(int price, int *arr, string str)
{
    int *new_arr, i, new_price, j, tmp;

    new_arr = allocate(num_of_types);
    new_price = price;

    for (i = 0; i < num_of_types; i++)
    {
        new_arr[i] = arr[i];

        /* jesli cena jest <= wartosci ktoregos typu monet */
        if (new_price <= (tmp = new_arr[i] * money_values[i]))
        {
            new_arr[i] = new_price / money_values[i];
            if (new_price > new_arr[i] * money_values[i])
                new_arr[i] += 1;
            break;
       }
       else
            new_price -= tmp;
    }

    /* i tutaj mam mie� tablic� z monetami, kt�rej ka�dy element albo wystarcza sam do
        op�acenia ceny, albo o ca�� jego warto�� jest obni�ona nast�pna warto�� w tablicy . */

    new_price = price;

    /* teraz idziemy od najbardziej warto�ciowych monet */
    for (i = num_of_types - 1; i >= 0; i--)
    {
        arr[i] = new_arr[i];

        /* je�li aktualna cena mo�e by� op�acona przez ten typ monet */
        if (arr[i] * money_values[i] >= new_price)
        {
            /* zerujemy ni�sze */
            for (j = i - 1; j >= 0; j--)
                arr[j] = 0;

            arr[i] = new_price / money_values[i];
            if (new_price > arr[i] * money_values[i])
                arr[i] += 1;
            break;
         }
         /* i odejmujemy od ceny warto�� przyznanych monet */
         new_price -= arr[i] * money_values[i];
    }

    /* jaki� dziwny ten kod, dlaczego nie mo�na po prostu policzy� tego w pierwszej p�tli? */

    /* kalkulowana jest tablica o najwyzszych mozliwych nomina�ach,
        spe�niaj�ca cen�. przekazujemy j� dalej do calc_change() */

    return arr + calc_change(price, arr, str);
}

/**
 * Przemieszcza pieni�dze z obiektu 'from' do obiektu 'to'.
 * @param     str - string okre�laj�cy typ monety ("grosze", "denary" albo "korony")
 * @param     num - ilo�� monet do przeniesienia
 * @param     from - od kogo wzi�� pieni�dze, je�li 0 - pieni�dze s� tworzone
 * @param     to - komu da� pieni�dze, je�li 0 - pieni�dze s� niszczone
 * @return      0 - je�li sukces
 * @return      je�li nie zero to b��d z move()
 * @return      albo -1, je�li 'from' ma za ma�o monet danego typu
*/
int
money_move(string str, int num, object from, object to)
{
    object cn, cf;
    int max;

    if (!str || (num <= 0 )) /* Only positive and existing money */
        return -1;

    if (from)
    {
        cf = present("_" + str + " moneta_", from);
//         find_player("krun")->catch_msg("autoload = " + cf->query_auto_load() + "\n");
        if (cf && function_exists("create_heap", cf) != "/std/coins")
            cf = 0;
    }
    else
        cf = MONEY_MAKE(num, str);

#if DEBUG_TRADE
    string str = "";

    object a;
    string b,c;
    int i=0;
    while(stringp(b=calling_function(-i)) || objectp(a = previous_object(-i)))
    {
        i++;

        if(objectp(a))
            c = MASTER_OB(a);
        else
            c = "";

        str += sprintf("%d$%s$%s, ", -i, b, c);
    }

//     find_player("krun")->catch_msg("wywo�ywanie\n " + str + "\n");
#endif DEBUG_TRADE

    if (!cf || !(max = cf->num_heap()) || num > max )
        return -1;   /* Not enough money to move */
#if DEBUG_TRADE
    if(objectp(from))
        dump_array(({from}) + all_inventory(from)->query_auto_load());
#endif DEBUG_TRADE

    if (to)
    {
        if (num < max)
            cf->split_heap(num);

        int r;
        mixed prop = to->query_prop(NPC_M_NO_ACCEPT_GIVE);
        to->remove_prop(NPC_M_NO_ACCEPT_GIVE);
        r = cf->move(to, 1); //FIXME: Tu trzeba zmieni� jak znajdziemy b��d odpowiadaj�cy za znikanie monet.
        to->add_prop(NPC_M_NO_ACCEPT_GIVE, prop);
        return r;
    }

    if (num < max)
        cf->set_heap_size(max-num);
    else
        cf->remove_object();

    return 0;
}

/**
 * Wykonuje opisane w tablicy przemieszczenie monet okre�lonego typu
 * pomi�dzy nami a obiektem (graczem).
 *
 * @param     arr - tablica przechowuj�ca monety, dwa razy wi�ksza od ilo�ci
 *		        typ�w monet, pierwsza cz�� to monety od 'from', druga cz��
 *             to  zap�ata (reszta) od 'to'.
 * @param     from - ten kto p�aci, a potem ewentualnie dostaje reszte.
 * @param     to - dok�d przesun�� pieni�dze i sk�d zabra� reszte, lub 0.
 * @param     silent - je�eli 1 to nie wy�wietla ewentualnych b��d�w
 * @return     je�li > 0 to jest to b��d z move()
 * 	@return	  je�li -1 obiekt nie mia� pieni�dzy, lub mia� fa�szywki.
 *
 *                   These codes are multiplied with 10 if the error took place
 *                   when we tried to take money from the player. Also we break
 *                   the transaction if there was an error here.
 *
 * @return     -10 je�li b��dny rozmiar tablicy.
 */
varargs int
change_money(int *arr, object from, object to, int silent)
{
    int i, error1, error2, move_error;

    move_error = 0;

    if (sizeof(arr) != num_of_types * 2)
        return -10;

    /* bierzemy monety od kupuj�cego */
    for (i = 0; i < num_of_types; i++)
    {
        if (arr[i] && (error1 = money_move(money_types[i],
            arr[i], from, to)))
        {
#ifdef DEBUG_TRADE
            find_player("krun")->catch_msg("error = " + error1 + "\n");
#endif DEBUG_TRADE
            error2 = money_move(money_types[i], arr[i],
                from, query_extra_money_holder(i, arr[i], error1));
            if (error2 != 0)
            {
                if (!silent && (error2 < 0))
                {
                    write("Wystapi� b��d przy pobieraniu pieni�dzy od " +
                        "gracza. A mo�e w obiegu s� jakie� fa�szywki?\n");
                }
                /*   There shouldn't be any errors from move since the environment should be a
                 *   room, if however a move error has arrived we don't print any text.
                 *   Nor is there a warning if we drop the money on the floor.
                 */
                return error2 * 10;
            }
        }
    }

    /* oddajemy kupuj�cemu reszt� */
    for (i = num_of_types; i < 2 * num_of_types; i++)
    {
        if (arr[i] && (error1 = money_move(money_types[i - num_of_types], arr[i], to, from)) && from)
        {
            error2 = money_move(money_types[i - num_of_types],
            arr[i], to, environment(from));

            if ((move_error == 0) && (error1 != 0))
                move_error = error1;

            /* move error detected */
            if (!silent && error1)
            {
                if(error2 < 0)
                {
                    /* handlarzowi nigdy nie zabraknie kasy ;) */
                    if(this_object()->query_handlarz())
                         money_move(money_types[i - num_of_types], arr[i], 0, from);
                }
                else
                {
                    write("Odk�adasz cz�� reszty.\n");
                    saybb(QCIMIE(from, PL_MIA) + " odk�ada troch� pieni�dzy.\n");
                    silent = 1;
                 }
             }
             else  if (!silent && (error2 < 0))
             {
                 write("Error: No money found in the from object (us), "+
                      "no money given.\n");
                 silent = 1;
             }

             if (error2 < 0)
                  move_error = error2;
        }
    }

    /* There shouldn't be an error if noone has messed around with the coins */
    return move_error;
}

/**
 * Funkcja generuje string opisujacy podana tablice z monetami.
 *
 * @param arr - tablica postaci ({ grosze, denary, korony })
 * @param przyp - w ktorym przypadku ma byc zadany opis
  * @return string z opisem monet typu: "2 grosze, 3 dukaty i korona".
 *
 */
mixed
text(int *arr, int przyp)
{
    string *t_arr;
    int i, j, prz, x;

    if (sizeof(arr) < num_of_types)  /* Not a valid array. */
        return ;

    t_arr = ({ });

    for (i = num_of_types - 1; i >= 0; i--)
    {
        if (arr[i] > 0)
        {
            x = i;

            /* troche inna jest trzecia odmiana dla funkcji 'ilosc',
            * jesli mamy mianownik lub biernik, uzywamy dopelniacza. */
            if (przyp  == 0 || przyp == 3)
                prz = 1;
            else
                prz = przyp;

            /* jeremian's changes */
            // ... i przerobki moje /d
            /* po moich poprawkach dzia�a ... - Molder */
            t_arr += ({ arr[i] + " " +
            ilosc(arr[i], MONEY_NAMES[i][0][przyp],
                MONEY_NAMES[i][1][przyp],
                MONEY_NAMES[i][1][prz]) });
        }
    }

    j = sizeof(t_arr);

    if (j < 1)
        return;

    if (j == 1)
        return t_arr[0] ;
    else
        return implode(t_arr[0 .. j - 2], ", ") + " i " + t_arr[j - 1] ;
}

/**
 * Ustawia standardowe opcje handlowania, musi by� wywo�ane,
 * chyba, �e r�cznie ustawimy KA�D� z opcji <br>
 * (czyli: set_money_give_max(), set_money_give_out(), set_money_give_reduce(),
 * set_money_accept(),set_money_greed_buy(),set_money_greed_sell(),
 * set_money_greed_change(), set_money_values(), set_money_types(), set_money_tematy())
 *
 * Default_trade ustala standardowo warto�ci:
 * - maksymalna kwota jak� dajemy, to 10000 groszy.
 * - wszystkie chciwo�ci s� ustawione na 100 (czyli nie zmieniaj� ceny).
 * - typy i warto�ci monet s� wzi�te z /sys/money.h (/config/sys/money2.h)
 * - akceptowane s� wszystkie typy monet.
 * - wydajemy (jako reszt� b�d� zap�at�) co najwy�ej:  20 koron, 400 dukat�w
 * i 10000 groszy. Zauwa�, �e je�li ilo�� wydawanych groszy != max give out,
 * to mo�liwe jest �e oddamy zbyt ma�o pieni�dzy.
 * - redukujemy wyp�at� monet o 8 koron, 2 dukaty i 0 groszy
*/
void
config_default_trade()
{
    set_money_give_max(MONEY_GIVE_MAX);
    set_money_give_out(MONEY_GIVE_OUT);
    set_money_give_reduce(MONEY_GIVE_REDUCE);
    set_money_accept(MONEY_ACCEPT);
    set_money_greed_buy(MONEY_GREED_BUY);
    set_money_greed_sell(MONEY_GREED_SELL);
    set_money_greed_change(MONEY_GREED_CHANGE);
    set_money_values(MONEY_VALUES);
    set_money_types(MONEY_TYPES);
    set_money_tematy(MONEY_TEMATY);
}

void
default_config_trade() { config_default_trade(); }

/**
 * Pierwszy obiekt p�aci podan� kwot� drugiemu obiektowi (o ile to nie 'test').
 * W za�o�eniu: gracz p�aci nam.
 *
 * @param     price - kwota do zap�acenia (bez uwzgl�dnienia chciwo�ci)
 * @param     ob - obiekt kt�ry ma zap�aci�
 * @param     str - string opisuj�cy jakimi monetami 'ob' chce zap�aci�
 * @param     test - je�li 1 pieni�dze nie b�d� przemieszczone
 * @param     ob2 - obiekt kt�ry powinien otrzyma� pieni�dze, 0 je�li �aden
 * @param     str2 - string opisuj�cy jakimi monetami 'ob2' ma wyda� reszt�
 * @param     silent - je�li 1 nie b�dzie informacji o b��dach
 *                      przy przenoszeniu pieni�dzy.
 * @return      Tablica opisuj�ca ile 'ob' zap�aci� (pierwsza cz��)
 *                    i ile dosta� reszty (druga cz��), albo b��dy:
 * @return      0 - brak osoby p�ac�cej
 * @return      1 - opisane przez 'str' pieni�dze nie wystarczaj�
 * @return      2 - 'ob' nie ma pieni�dzy opisanych przez 'str'
 * @return      3 - 'ob' ma zbyt ma�o pieni�dzy
 * @return      4 - 'ob' ma zbyt ma�o akceptowanych przez nas
 *                   pieni�dzy.
 *
 * @return     Dodatkowo, je�l nie wyst�pi�y powy�sze b��dy, to ostatni
 *                   element zwr�conej tablicy zawiera kod b��du z przemieszczania
 *                   monet. Je�li jest > 0 to znaczy, �e do gracza nie mog�o by�
 *                   przemieszczone tyle monet, i zosta�y one upuszczone.
 *                   Je�li jest to -1 wtedy nie mo�na by�o zabra� pieni�dzy od obiektu.
 */
varargs int *
pay(int price, object ob, string str, int test, object ob2, string str2,
    int silent)
{
    int *arr, i;

    if (!ob)
       ob = this_player();

    if (!ob)
    {
       notify_fail("Brak nabywcy.\n");
       return 0;
    }

    arr = what_coins(ob);
    if (str) /* redukuje tablice do monet kt�rymi chcemy zap�aci� */
       want_to_pay(arr, str);

    if (!can_pay_arr(price, arr))
    {
        if (str && (str != "") && money_merge(arr))
        {
            notify_fail("Udost�pnij wi�ksz� pul� pieni�dzy, " +
                text(arr, PL_MIA) + " to za ma�o, �eby " +
                "ui�ci� ��dan� przez sprzedawc� kwot�.\n");
            return ({ 1 });
        }
        else  if (str && (str != ""))
        {
            notify_fail("Chyba zdecydowa�" +
                this_player()->koncowka("e�", "a�") + " si� zap�aci� " +
                "pieni�dzmi, kt�rych przy sobie nie masz.\n");
            return ({ 2 });
        }

        notify_fail("Nie starczy ci pieni�dzy �eby zap�aci�.\n");
        return ({ 3 });
    }

    we_accept(arr);
    /* Je�li po por�wnaniu z akceptowanymi monetami nie sta� nas na zap�at�,
        to we_accept() ustawi� odpowiedni notify_faill */
    if (!can_pay_arr(price, arr))
        return ({ 4 });

    if ((price == 0) && test)
        arr = arr + calc_change(price, arr, str2);
    else
        arr = calc_coins(price, arr, str2);

    if (!test)
        arr += ({ change_money(arr, ob, ob2, silent) });

    return arr;
}

/**
 * Pierwszy obiekt daje podan� kwot� drugiemu (o ile to nie 'test').
 * W za�o�eniu: my dajemy pieni�dze graczowi.
 *
 * @param     price - kwota do przekazania
 * @param     ob - obiekt kt�ry dostaje
 * @param     ob2 - obiekt kt�ry daje
 * @param     test - je�li 1 pieni�dze nie b�d� przemieszczone
 * @param     str - string opisuj�cy jakie monety gracz chce dosta�
 * @param	  silent - je�li 1 to nie zostan� wy�wietlone komunikaty b��du
 *                      przy przemieszczaniu monet.
 * @return      tablica opisuj�ca ile ob2 zap�aci� i ile dosta� zwrotu
 *                    (lub b��dy: opisane przy pay())
*/
varargs int *
give(int price, object ob, string str, int test=0, object ob2=TO, int silent=0)
{
    int *arr, *tmp_arr, i;

    if (!ob && !test)
        ob = this_player();
    if (!ob && !test)
    {
        notify_fail("Nie ma komu da� pieni�dzy.\n");
        return 0;
    }

    tmp_arr = allocate(num_of_types);
    /* skomentowane z powod�w podanych w pay */
    //price = price * 100 / this_object()->check_call(money_greed_sell);

    arr = calc_change (price, 0, str);
    arr = tmp_arr + arr;

    if (!test)
        arr += ({ change_money(arr, ob, ob2, silent) });

    return arr;
}
