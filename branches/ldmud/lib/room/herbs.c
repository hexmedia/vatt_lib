/**
 * \file /std/room/herbs.c
 *
 * Plik wyodr�bniony z /std/room/description bo z opisywaniem to to wiele wsp�lnego nie mia�o.
 * Opr�cz niego wyodr�bni�em tak�e /std/room/search.c w kt�rym znajduj� si� podstawy dzia�ania
 * ca�ego tego systemu.
 *
 * @author Krun
 * @date 2007-2008
 *
 * TODO:
 * - powolne zapominanie znalezionych zi�(automatyczne)
 */

#pragma strict_types

#include <pl.h>
#include <exp.h>
#include <macros.h>
#include <cmdparse.h>
#include <ss_types.h>
#include <composite.h>
#include <stdproperties.h>

#define HERBS_DIR       "/d/Standard/ziola/"

#define CAN_SEARCH_MORE_THAN_1_HERB 1

#ifdef DBG
#undef DBG
#endif DBG
#define DBG(x)          find_player("krun")->catch_msg("Info z /std/room/herb.c: " + x + "\n")

static  mixed   herbs;              /* Pliki wszystkich zi�, kt�re mog� by� w roomie. */
static  mixed   rosnace;            /* Jakie zio�a w chwili obecnej rosn� na lokacji */
static  mapping found_herbs = ([]); /* Jakie zio�a przez jakiego gracza zosta�y znalezione */

private mixed
only_first_arg(mixed arg)
{
    if(pointerp(arg))
        return arg[0];
    else
        return arg;
}

/**
 * Funkcja czyszcz�ca tablic� znalezionych zi�. Dzi�ki niej, w zio�ach znalezionych przez
 * gracza nie znajd� si� zio�a, kt�re znikne�y ju� z lokacji.
 *
 * @param mixed ziola Zio�a znalezione przez gracza.
 */
private static mixed
przeczysc_z_zebranych(mixed ziola)
{
    if(!pointerp(ziola) || !sizeof(ziola))
        return ({});

    object *ros = flatten(rosnace);

    for(int i = 0; i < sizeof(ziola); i++)
    {
        if(pointerp(ziola[i]))
        {
            if(!sizeof(ziola[i]))
                ziola = exclude_array(ziola, i, i);
            else
            {
                for(int j = 0; j < sizeof(ziola[i]); j++)
                {
                    if(!objectp(ziola[i][j]) || member_array(ziola[i][j], ros) == -1)
                    {
                        ziola = exclude_array(ziola, i, i);
                        break;
                    }
                }
            }
        }
        else if(!objectp(ziola[i]) || member_array(ziola[i], ros) == -1)
            ziola = exclude_array(ziola, i, i);
    }

    return ziola;
}

/**
 * Zwraca opis zi� rosn�cych na lokacji.
 *
 * @param herbs_obs obiekty zi�, kt�re nale�y opisa�.
 * @param zn czy s� to zio�a znalezione wcze�niej, lub czy jest to opis
 * @param cap czy zaczyna� od du�ej litery.
 *
 * @return opis zi�
 */
public varargs string
opisz_ziola(mixed herbs_obs, mixed zn, int cap=1)
{
    mixed tmp;
    string ret;

    object *fho;
    fho = flatten(herbs_obs);
    herbs_obs = map(herbs_obs, only_first_arg);
    fho->unset_no_show();
    fho->set_no_show_composite(0);

    //Je�li nie ma zwyk�ych to sprawdzamy tylko znalezione
    if(!pointerp(herbs_obs) || !pointerp(tmp = unique_array(herbs_obs, &->short(TP, PL_MIA))) ||
        !sizeof(tmp))
    {
        return "";
    }

    if(pointerp(zn) && sizeof(zn))
        ret = "znajdujesz ";
    else
    {
        if(pointerp(tmp[0]))
            ret = LANG_CZAS(sizeof(tmp[0]), "ro�nie", "rosn�") + " tu ";
        else
            ret = "ro�nie tu ";
    }

    ret += COMPOSITE_DEAD(herbs_obs, (pointerp(zn) ? PL_BIE : PL_MIA)) + ".";

    if(pointerp(zn) && sizeof(zn))
        ret += " Opr�cz tego " + opisz_ziola(zn, 0, 0) + "";

    fho->set_no_show();

    if(cap)
        return capitalize(ret) + "\n";
    else
        return ret;
}

/**
 * @param str Je�li zmienna ta jest podana komunikat bedzie j� zawiera�.
 * @return Komunikat gdy gracz nie znajduje zi�.
 */
static string
no_find(mixed str = 0)
{
    return (stringp(str) ?
        "Nie znajdujesz ^zadnych zi^o^l. " + str + "\n" :
        ((intp(str) && str) > 0 ? "Nie uda�o ci si� znale�� " + (str > 1 ? "zi�, kt�rych" : "zio�a, kt�rego") +
        "szuka�e�.\n" : "Szukasz wsz^edzie, ale nie znajdujesz ^zadnych zi^o^l.\n"));
}

static private string
dodaj_sciezke(string plik)
{
    if(plik[0..0] != "/")
        plik = HERBS_DIR + plik;
    if(plik[-2..-1] != ".c")
        plik += ".c";
    return plik;
}

varargs private mixed
clone_herbs(mixed f, int r, int w)
{
    mixed herbs_obs;

    if(pointerp(f))
    {
        if(!w)
            f = map(f, dodaj_sciezke);

        herbs_obs = map(f, clone_object);

        if(r)
            map(herbs_obs, &->ustaw_ze_rosnie(1));

        return herbs_obs;
    }

    if(!w)
        f = dodaj_sciezke(f);

    herbs_obs = clone_object(f);

    if(r)
        herbs_obs -> ustaw_ze_rosnie(1);

    return herbs_obs;
}

int
mz_filter(mixed ob, object pl=TP)
{
    string imie = pl->query_real_name();

    if(!is_mapping_index(imie, found_herbs))
        return (ob[0]->moze_znalezc(pl));
    return (ob[0]->moze_znalezc(pl) && (member_array(ob[0], found_herbs[imie]) == -1));
}

private static mixed
master_ob_sp(mixed ob)
{
    if(pointerp(ob))
        return map(ob, master_ob_sp);
    if(!objectp(ob))
        return;
    return MASTER_OB(ob);
}

//Pozwala znale�� pierwsze zio�o w grupie.
object *
znajdz_pierwsze_z_grupy(object *ziola)
{
    object *pierwsze = ({});

    foreach(object x : ziola)
    {
        foreach(object *y : rosnace)
        {
            if(x == y[0])
            {
                pierwsze += ({x});
                break;
            }
        }
    }
    return pierwsze;
}

void
clean_found_herbs()
{
    if(m_sizeof(found_herbs))
    {
        foreach(string imie : m_indexes(found_herbs))
        {
            int i;
            if(!found_herbs[imie] || !(found_herbs[imie] = przeczysc_z_zebranych(found_herbs[imie])) ||
               !pointerp(found_herbs[imie]) || !sizeof(found_herbs[imie]))
            {
                found_herbs = m_delete(found_herbs, imie);
                continue;
            }
        }
    }
}

void
clean_found_herbs_from_ob(object ob)
{
    if(m_sizeof(found_herbs))
    {
        foreach(string imie : m_indexes(found_herbs))
        {

            int i;
            if((i = member_array(ob, found_herbs[imie])) >= 0)
                found_herbs[imie] = exclude_array(found_herbs[imie], i, i);
            else
            {
                for(i=0;i<sizeof(found_herbs[imie]);i++)
                {
                    if(pointerp(found_herbs[imie][i]))
                        if(member_array(ob, found_herbs[imie][i]) >= 0)
                            found_herbs[imie] = exclude_array(found_herbs[imie], i, i);
                }
            }
            if(!found_herbs[imie])
                found_herbs = m_delete(found_herbs, imie);
        }
    }
}

static private nomask void
dodaj_exp_za_nieudane()
{
    TP->increase_ss(SS_HERBALISM, EXP_SZUKANIE_ZIOL_NIEUDANE_HERBALISM);
    TP->increase_ss(SS_AWARENESS, EXP_SZUKANIE_NIEUDANE_AWARENESS);
    TP->increase_ss(SS_INT, EXP_SZUKANIE_ZIOL_NIEUDANE_INT);
}

static private nomask void
dodaj_exp_za_znalezione(int size, int flaga=0)
{
    int i;
    if(!flaga)
    {
        for(i=0;i<size;i++)
        {
            // Za szukanie zi� og�lnie
            TP->increase_ss(SS_HERBALISM,   EXP_SZUKANIE_ZIOL_UDANE_HERBALISM);
            TP->increase_ss(SS_INT,         EXP_SZUKANIE_ZIOL_UDANE_INT);
            TP->increase_ss(SS_AWARENESS,   EXP_SZUKANIE_UDANE_AWARENESS);
        }
    }
    else
    {
        for(i=0;i<size;i++)
        {
            //Za szukanie konkretnego zio�a
            TP->increase_ss(SS_HERBALISM,   EXP_SZUKANIE_ZIOLA_UDANE_HERBALISM);
            TP->increase_ss(SS_INT,         EXP_SZUKANIE_ZIOLA_UDANE_INT);
            TP->increase_ss(SS_AWARENESS,   EXP_SZUKANIE_UDANE_AWARENESS);
        }
    }
}

/**
 * Funkcja wywo�ywana gdy gracz szuka na lokacji zi�.
 * @param searched_herbs szukane zio�a
 * @param searched_herbs_names nazwy szukanych zio�
 *
 * @return Wiadomo�� do wypisania na ekranie graczowi.
 */
static varargs string
search_for_herbs(object *searched_herbs)
{
    string ret, *searched_herbs_names = searched_herbs->short(TP, PL_DOP);
    object *znalezione;

    if (!sizeof(herbs))
    {
        dodaj_exp_za_nieudane();

        int rt = TO->query_prop(ROOM_I_TYPE);
        if(rt & ROOM_IN_WATER == ROOM_IN_WATER ||
           rt & ROOM_UNDER_WATER == ROOM_UNDER_WATER)
        {
            return no_find("Mo^ze posz^loby ci lepiej, gdyby^s szuka^l" +
                this_player()->koncowka("", "a") + " ich na suchym l^adzie.");
        }
        if(rt & ROOM_IN_AIR == ROOM_IN_AIR)
        {
            return no_find("Mo^ze posz^loby ci lepiej, gdyby^s szuka^l" +
                this_player()->koncowka("", "a") + " ich na bardziej twardym gruncie.");
        }

        if (TO->query_prop(ROOM_I_INSIDE))
        {
            return no_find("Mo^ze posz^loby ci lepiej, gdyby^s szuka^l" +
                   this_player()->koncowka("", "a") + " ich na otwartym " +
                  "terenie.");
        }

        return no_find();
    }

    string imie = TP->query_real_name();

    int i;
    object *znalezione_wczesniej;

    if(is_mapping_index(imie, found_herbs))
    {
        if(pointerp(found_herbs[imie]))
            znalezione_wczesniej = found_herbs[imie];
    }

    znalezione = ({});
    /* --- Zmiany Kruna --- */
    if(!searched_herbs)
    {
        if(pointerp(rosnace))
        {
            mixed tmp = rosnace;

            if(pointerp(znalezione_wczesniej))
                if(sizeof(znalezione_wczesniej))
                    tmp -= znalezione_wczesniej;

            znalezione = filter(tmp, &mz_filter(, TP));
        }
    }
    else
    {
        searched_herbs = filter(searched_herbs, &objectp());

        if(sizeof(searched_herbs))
        {
            foreach(object sh : (searched_herbs -  (znalezione_wczesniej ?: ({}))))
            {
                if(member_array(MASTER_OB(sh), map(flatten(rosnace), master_ob_sp)) != -1 &&
                    (sh->moze_znalezc(TP) && sh->moze_zidentyfikowac(TP) || random(5) > 3))
                {
                    znalezione += ({sh});
                }
            }
        }
        else
            return "Czego chcesz poszuka�?\n";
    }

    mixed wszystkie_znalezione;

    wszystkie_znalezione = (pointerp(znalezione) ? znalezione : ({})) +
        (pointerp(znalezione_wczesniej) ? znalezione_wczesniej : ({}));

    if(!sizeof(wszystkie_znalezione) && !pointerp(searched_herbs))
    {
        dodaj_exp_za_nieudane();
        return no_find();
    }
    else if(!sizeof(wszystkie_znalezione) && pointerp(searched_herbs))
    {
        dodaj_exp_za_nieudane();
        return no_find(sizeof(searched_herbs));
    }

    mixed herbs_obs = flatten(wszystkie_znalezione);
    herbs_obs->unset_no_show();
    herbs_obs->set_no_show_composite(0);

    int zn = (pointerp(searched_herbs) && sizeof(searched_herbs));

    dodaj_exp_za_znalezione(sizeof(znalezione), zn);
    ret = "";

    if(TP->query_prop(LIVE_I_KLECZY))
    {
        saybb(QCIMIE(TP, PL_MIA) + " wstaje z kolan.\n");
        ret += "Wstajesz z kolan.\n";
    }

    if(!sizeof(znalezione))
        ret += "Nie znajdujesz �adnych zi� ale wiesz, �e " + opisz_ziola(znalezione_wczesniej, 0, 0) + "\n";
    else
        ret += opisz_ziola(znalezione, znalezione_wczesniej);

    found_herbs[imie] = wszystkie_znalezione;

    herbs_obs->set_no_show();

    return ret;
    /*----------------------*/
}

/**
 * Dodaje zio�o do lokacji.
 * @param files Plik lub pliki zio�a. Kilka plik�w mo�emu ustawi� zio�u, kt�re ma kilka cz�ci, np. pietruszka(na� i korze�).
 * @return 1 je�li oddane
 */
int
dodaj_ziolo(mixed files)
{
    if (!files)
        return 0;
    if(stringp(files))
        files = ({files});

    if(!pointerp(files))
        return 0;

    // --- Zmiany Kruna ---
    if (!herbs)
        herbs = ({ files });
    else
        herbs += ({ files });

    return 1;
    //---------------------------
}

// --- Zmiany Kruna ---

/**
 * Funkcja odpowida za maksymaln� ilo�� zi� dost�pn� na lokacji.
 */
public int
ilosc_ziol()
{
    int dzielnik = 0;
    int ilosc = 0;
    int rt = TO->query_prop(ROOM_I_TYPE);

    if((rt & ROOM_UNDER_WATER) == ROOM_UNDER_WATER)
        dzielnik++;
    if((rt & ROOM_BEACH) == ROOM_BEACH)
        dzielnik++;
    if((rt & ROOM_DESERT) == ROOM_DESERT)
        dzielnik++;
    if((rt & ROOM_IN_CITY) == ROOM_IN_CITY)
        dzielnik++;
    if((rt & ROOM_IN_AIR) == ROOM_IN_AIR)
        dzielnik++;
    if((rt & ROOM_CAVE) == ROOM_CAVE)
        dzielnik++;
    if((rt & ROOM_NORMAL) == ROOM_NORMAL)
    {
        dzielnik++;
        ilosc += 3;
    }
    if((rt & ROOM_FOREST) == ROOM_FOREST)
    {
        dzielnik++;
        ilosc += 5;
    }
    if((rt & ROOM_SWAMP) == ROOM_SWAMP)
    {
        dzielnik++;
        ilosc += 5;
    }
    if((rt & ROOM_FIELD) == ROOM_FIELD)
    {
        dzielnik++;
        ilosc += 5;
    }
    if((rt & ROOM_MOUNTAIN) == ROOM_MOUNTAIN)
    {
        dzielnik++;
        ilosc += 5;
    }
    if((rt & ROOM_TRACT) == ROOM_TRACT)
    {
        dzielnik++;
        ilosc += 2;
    }
    if((rt & ROOM_IN_WATER) == ROOM_IN_WATER)
    {
        dzielnik++;
        ilosc++;
    }
    if((rt & ROOM_TREE) == ROOM_TREE)
    {
        dzielnik++;
        ilosc++;
    }

    return ilosc / max(1, dzielnik);
}

//Zwraca czy zio�o wyst�pi czy te� nie
private int
filter_wystepowania(mixed x)
{
    if(pointerp(x))
    {
        if(x[0]->query_pora_roku() && x[0]->query_pora_roku() != TO->pora_roku())
            return 0;

        return (x[0]->query_czestosc_wystepowania() > random(100));
    }
    else
    {
        if(x->query_pora_roku() && x->query_pora_roku() != TO->pora_roku())
            return 0;

        return (x->query_czestosc_wystepowania() > random(100));
    }
}

/**
 * Funkcja resetuj�ca zio�a na lokacji.
 */
public void
odnow_ziola()
{
    int ilosc;
    mixed pliki_rosnacych = ({});

    if(!herbs)
        return;

    ilosc = random(ilosc_ziol());

    if(!ilosc)
        return;

    //Losujemy jakie zi�ka b�d� prawdopodobnie ros�y, potem sprawdzimy jaka
    //jest cz�sto�� wyst�powania.
    while(ilosc-- > 0)
        pliki_rosnacych += ({herbs[random(sizeof(herbs))]});

    flatten(rosnace)->remove_object();

    rosnace = map(pliki_rosnacych, &clone_herbs(, 1));

    rosnace = filter(rosnace, &filter_wystepowania());

    int i, size;
    size = sizeof(rosnace);
    for(i=0;i<sizeof(rosnace);i++)
    {
        if(pointerp(rosnace[i]))
        {
            if(i <  size)
            {
                ilosc = 1 + random((rosnace[i][0]->query_sposob_wystepowania()-1));

                //Je�li mamy ilo�� 0 to musimy zi�ko usun��
                if(ilosc <= 0)
                    rosnace[i]->remove_object();
                else while(ilosc-- > 0)
                    rosnace += ({clone_herbs(map(rosnace[i], master_ob_sp), 1)});
            }

            int j, sizew;
            for(j=0;j<sizew;j++)
            {
                if(j < sizew)
                {
                    ilosc = 1 + random((rosnace[i]->query_ilosc_w_calosci()-1));

                    if(ilosc <= 0)
                        rosnace[i][j]->remove_object();
                    else while(ilosc-- > 0)
                        rosnace[i] += ({clone_herbs(rosnace[i][j], 1)});
                }
            }

            rosnace[i] -= ({0});
        }
        else if(objectp(rosnace[i]) && i < size)
        {
            ilosc = 1 + random((rosnace[i]->query_sposob_wystepowania()-1));
            if(ilosc <= 0)
                rosnace[i]->remove_object();
            else while(ilosc-- > 0)
                rosnace += ({ clone_herbs(rosnace[i],1) });
        }
    }
    rosnace -= ({0});

    flatten(rosnace)->move(TO, 1);
    found_herbs = ([]); //Resetujemy znalezione, bo skoro nowe zio�a s�, to i nikt ich
                        //jeszcze nie znalaz�.
}

/**
 * @return Pliki zi�ek jakie mog� znajdowa� si� na lokacji.
 */
string *
query_ziola()
{
    return herbs;
}

/**
 * Ustawia jakie zio�a rosn� na lokacji
 * @param ziola Zio�a rosn�ce na lokacji
 */
public void
ustaw_rosnace(object *ziola)
{
    rosnace = ziola;
}

/**
 * Usuwamy rosn�ce zi�ko
 * @param ob obiekt zi�ka
 */
public void
usun_rosnace(object *ob)
{
    int i;

    for(i=0;i < sizeof(rosnace);i++)
       if(rosnace[i][0] == ob[0])
           rosnace = exclude_array(rosnace, i, i);

    if(!is_mapping_index(TP->query_real_name(), found_herbs))
       DBG(UC(TP->query_real_name()) + " prawdopodbnie zebra� zio�o nie "+
            "znajduj�c go wcze�niej.\n");

    found_herbs[TP->query_real_name()] -= ob;
}

/**
 * @param ziolo zio�o kt�re sprawdzamy
 *
 * @return zwraca czy zio�o ro�nie czy te� nie.
 */
public int
jest_rosnace(object ziolo)
{
    if(!pointerp(rosnace) || !sizeof(rosnace))
        return 0;

    if(member_array(ziolo, rosnace) != -1)
        return 1;

    for(int i = 0; i < sizeof(rosnace); i++)
    {
        if(pointerp(rosnace[i]))
        {
            if(!sizeof(rosnace[i]))
            {
                rosnace = exclude_array(rosnace, i, i);
                continue;
            }
            else
            {
                rosnace[i] = filter(rosnace[i], objectp);

                if(member_array(ziolo, rosnace[i]) != -1)
                    return 1;
            }
        }
        else if(!objectp(rosnace[i]))
            rosnace -= ({rosnace[i]});
    }
}

/**
 * @return Mapping indeksowany po plikach(tablicach plik�w) zi�ek, o warto�ciach ilo�ci zi�ek.
 */
mapping
query_rosnace()
{
    return rosnace;
}
/**
 * @param ziolo zio�o kt�re sprawdzamy
 * @param imie imie gracza przez kt�rego zio�o mia�o by� znalezione
 *
 * @return zwraca czy zio�o zosta�o przez gracza znalezione czy te� nie.
 */
public int
jest_znalezione(object ziolo, int imie)
{
    mixed fh;
    found_herbs[imie] = przeczysc_z_zebranych(found_herbs[imie]);
    fh = found_herbs[imie];

    if(!pointerp(fh) || !sizeof(fh))
        return 0;

    if(member_array(ziolo, fh) != -1)
        return 1;

    for(int i = 0; i < sizeof(fh); i++)
    {
        if(pointerp(fh[i]))
        {
            if(!sizeof(fh[i]))
            {
                rosnace = exclude_array(fh, i, i);
                continue;
            }
            else
            {
                rosnace[i] = filter(fh[i], objectp);

                if(member_array(ziolo, fh[i]) != -1)
                    return 1;
            }
        }
    }
}
/**
 * @param imie Imi� gracza
 * @return Zio�a znalezione przez gracza.
 */
public mixed
query_znalezione(string imie)
{
    if(!is_mapping_index(imie, found_herbs))
        return 0;

    found_herbs[imie] = przeczysc_z_zebranych(found_herbs[imie]);

    return found_herbs[imie];
}

/**
 * Sprawdza czy gracz szuka zi�, nie koniecznie rosn�cych na
 * lokacji tak jak by�o to w poprzedniej wersji.
 */
string
herb_search(string str)
{
    int i;
    mixed herbs_obs, tmp;

    if(!str)
        return 0;

    if(str ~= "zi�")
        return search_for_herbs();

    herbs_obs = map(rosnace, only_first_arg);

    if(!sizeof(herbs_obs))
        return 0;

    object fherbs_obs = flatten(herbs_obs);
    fherbs_obs->unset_no_show();
    fherbs_obs->set_no_show_composite(0);

    //Ma�e sortowanko, nieznalezione na pocz�tek
    mixed tos = filter(herbs_obs, not @ &jest_znalezione(, TP->query_real_name()));
    herbs_obs = tos + (herbs_obs - tos);

    object *found;
    if(!parse_command(str, herbs_obs, "%i:" + PL_DOP, found))
    {
        fherbs_obs->set_no_show();
        return 0;
    }

    found = DIRECT_ACCESS(found);

#ifndef CAN_SEARCH_MORE_THAN_1_HERB
    if(sizeof(found) > 1)
    {
        NF("Nie mo�esz szuka� wi�cej ni� jednego okre�lonego gatunku zio�a jednocze�nie.\n");
        fherbs_obs->set_no_show();
        return 0;
    }
#endif
    if(sizeof(!found))
    {
        fherbs_obs->set_no_show();
        return 0;
    }

    found = filter(found, not @ &jest_znalezione(, TP->query_real_name()));

    //Je�li wpisujemy drugi raz to chyba jasne, �e nie szukamy tego samego co znale�li�my za pierwszym
    //razem dlatego przeczy�cimy troch� tablic�.
    if(!sizeof(found))
    {
        return "Nie uda�o Ci si� znale�� zio�a kt�rego szuka�e�" +
            (sizeof(query_znalezione(TP->query_real_name())) ? ", ale wiesz �e " +
            opisz_ziola(query_znalezione(TP->query_real_name()), 0, 0) : ".\n");
    }

    fherbs_obs->set_no_show();

    if(sizeof(herbs_obs))
        return search_for_herbs(found);

    return 0;
}

public void
herb_leave_env()
{
    foreach(string imie : found_herbs)
        found_herbs[imie] = przeczysc_z_zebranych(found_herbs[imie]);

}