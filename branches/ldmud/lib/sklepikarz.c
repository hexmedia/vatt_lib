/**
 *   \file /lib/sklepikarz.c
 *
 *   Support for shops, based on the example shop I once written
 *   /Nick
 *
 *   Sklepy zostaly przerobione przez Nunasa 27.09.2005
 *   Od 10.02.2006 nad sklepem pracuje Molder.
 *
 *   Ten plik maj� dziedziczy� wszelcy handlarze.
 *   Typowe u�ycie tego pliku:
 *
 *   inherit "/std/humanoid.c";
 *   inherit "/lib/sklepikarz.c";
 *
 *   create_humanoid() {
 *       ...
 *       config_default_sklepikarz();
 *       [ inne funkcje z trade.c i shop.c ]
 *        ...
 *   }
 *   init() {
 *       ::init();
 *       init_sklepikarz();
 *       ...
 *   }
 *
 *   Po wywo�aniu config_default_shop() powinno si� te�:
 *   - ustali� gdzie jest magazyn funkcj� set_store_room()
 *     (chyba, �e magazynem ma by� this_object())
 *   - u�y� funkcji set_co_skupujemy()
 *     (chyba, �e nie chcemy nic skupowa�)
 *   - u�y� funkcji set_co_sprzedajemy()
 *     (chyba, �e chcemy sprzedawa� wszystko)
 *
 *   Nast�pnie mo�na nadpisa� funkcje o nazwach shop_hook_*,
 *   aby uzyska� unikalnie gadaj�cego sklepikarza.
 *
 *   (Hook to taka funkcja, kt�ra jest wywo�ywana przy okazji
 *   zaj�cia r�nych okoliczno�ci. Jedynym jej zadaniem jest ustawienie
 *   stosownego notify_fail() b�d� wy�wietlenie stosownych tekst�w
 *   dla this_player() b�d� innych graczy.)
 *
 * Poprawki (m.in. zrobienie ramki: Vera.
 */

#pragma strict_types
#pragma save_binary

inherit "/lib/trade";

#include <stdproperties.h>
#include <macros.h>
#include <math.h>
#include <language.h>
#include <cmdparse.h>
#include <composite.h>

#include <filter_funs.h>
#include <ss_types.h>
#include <exp.h>

//do definicji slownika:
#include <files.h>

/* okre�la ile co najwy�ej obiekt�w zostanie wy�wietlonych przy 'przejrzyj' */
#define MAXLIST 840

/* okre�la ile przedmiot�w maksymalnie mo�na sprzeda� za pomoc� jednej komendy sprzedaj */
#define MAX_SELL 200

/* okre�la jaki jest minimalny dost�p mi�dzy operacjami zakupu i sprzedarzy aby dosta� za te operacje
   jakiego� expa, takie zabezpieczonko na kup, sprzedaj */
#define MINIMALNA_PRZERWA_MIEDZY_OPERACJAMI_W_SKLEPIE   600 /* 10 minut */

#define SPRZEDAWCA "_sprzedawca"

/* magazyn musi by� pokojem albo this_object() (b�d� w inwentarzu this_object)
 */
static mixed store_room;
static mixed skarbonka;

 /* bitowa informacja na temat tego jakimi elementami handlujemy,
    zgodna z object_types.h  */
static int obiekty_sprzedawane;
static int obiekty_skupowane;

int do_buy(string str);
int do_sell(string str);
int do_value(string str);
int do_show(string str);
int do_list(string str, string title = 0);
int do_store(string str);

int try_buy(string str);
int try_sell(string str);
int try_list(string str);
int try_value(string str);
int try_show(string str);
int help_list();
int help_buy();

int pomoc(string str);
public int pomoc2(string str);

int armour_filter(object ob);
int weapon_filter(object ob);
int query_magazyn_jest_pusty();

int czy_handlujemy();
int czy_handlujemy = 1;

object* unique_array_object(object* tab);


int co_godzine_sprzatanko(); //sprzatanie zakladu

/* ---------------------------------- Funkcje hacz�ce  ---------------------------------------------------- */

/**
 * Wywo�ana je�li gracz pr�buje sprzeda� co� czego nie skupujemy
 * (typ obiektu nieustawiony przez set_obiekty_skupowane())
 * @param  ob - obiekt kt�ry gracz chce sprzeda�
 */
void
shop_hook_tego_nie_skupujemy(object ob)
{
    notify_fail("Nie skupuj� takich rzeczy jak " + ob->short(PL_MIA) + ".\n");
}

/**
 * Wywo�ana je�li gracz pr�buje kupi� co� czego nie sprzedajemy
 * (typ obiektu nieustawiony przez set_obiekty_sprzedawane())
 * @param  ob - obiekt kt�ry gracz chce sprzeda�
 */
void
shop_hook_tego_nie_sprzedajemy(object ob)
{
    notify_fail("Nie sprzedaj� takich rzeczy jak " + ob->short(PL_MIA) + ".\n");
}

/**
 * Wywo�ana je�li gracz chce co� sprzeda� b�d� wyceni�,
 * a my nie skupujemy �adnego typu obiekt�w.
 */
void
shop_hook_niczego_nie_skupujemy()
{
    notify_fail("Przykro mi, ale ja niczego nie skupuj�.\n");
}

/**
 * Wywo�ana je�li gracz chce co� kupi�,
 * a my nie sprzedajemy �adnego typu obiekt�w.
 */
void
shop_hook_niczego_nie_sprzedajemy()
{
    notify_fail("Przepraszam, ale ja niczego nie sprzedaj�.\n");
}

/**
 * Wywo�ana je�li gracz chce co sprzeda� albo wyceni�,
 * ale nie znale�li�my nieczego takiego w jego inwentarzu.
 * @param     str - The string player tried to sell
 */
void
shop_hook_sell_no_match(string str)
{
    notify_fail("Chyba nie masz niczego takiego.\n");
}

/**
 * Wywo�ana je�li nie mamy tego co gracz chce kupi�,
 * przejrze� albo objerze�.
 * @param     str - string kt�ry gracz wpisa�
 */
void
shop_hook_buy_no_match(string str)
{
    notify_fail("Nie mam niczego takiego.\n");
}

/**
 * Wywo�ana gdy gracz pyta o spis rzeczy, a magazyn jest pusty.
 * @param     str - If player specified string
 */
void
shop_hook_list_empty_store(string str)
{
    notify_fail("Chwilowo nie ma nic na sprzeda�.\n");
}

/**
 * Wywo�ana gdy gracz co� kupi�.  Wy�wietla komunikat graczowi
 * oraz innym osobom obecnym w pomieszczeniu.
 * @param   arr - tablica kupionych przedmiot�w
 */
void
shop_hook_bought_items(object *arr)
{
    write("Kupujesz " + COMPOSITE_DEAD(arr, PL_BIE) + ".\n");
    saybb(QCIMIE(this_player(), PL_MIA) + " kupuje " + QCOMPDEAD(PL_BIE) +
".\n");
}

/**
 * Wywo�ana gdy gracz co� sprzeda�. Wy�wietla komunikat graczowi
 * oraz innym osobom obecnym w pomieszczeniu.
 * @param     item - tablica sprzedanych przedmiot�w
 */
void
shop_hook_sold_items(object *item)
{
    write("Sprzedajesz " + COMPOSITE_DEAD(item, PL_BIE) + ".\n");
    saybb(QCIMIE(this_player(), PL_MIA) + " sprzedaje " + QCOMPDEAD(PL_BIE) +
          ".\n");
}

/**
 * Wywo�ana gdy gracz nic nie sprzeda� wpisuj�c 'sprzedaj wszystko'.
 */
void
shop_hook_sold_nothing()
{
    notify_fail("Nic nie sprzeda�" + this_player()->koncowka("e�", "a�") +
        ".\n");
}

/**
 * Wywo�ana gdy obiekt ma zerow� warto�� (OBJ_I_VALUE == 0).
 * @param    ob - obiekt
 */
void
shop_hook_sell_no_value(object ob)
{
    notify_fail(capitalize(ob->short(PL_MIA)) + " nie ma" +
        (ob->query_tylko_mn() ? "j�" : "") + " �adnej warto�ci.\n");
}

/**
 * Wywo�ana je�li obiekt si� pali.
 * @param    ob - obiekt
 */
void
shop_hook_sell_has_fire(object ob)
{
    notify_fail("Ale� to p�onie!\n");
}

/**
 * Wywo�ana je�li obiekt jest za�o�ony albo dobyty,
 * a gracz nie powiedzia�, �e takie obiekty te� chce sprzedawa�.
 * @param    ob - obiekt
 */
void
shop_hook_sell_worn_or_wielded(object ob)
{
    notify_fail("Chyba nie chcesz sprzeda� rzeczy kt�r� dobywasz, lub "+
        "kt�r� masz na sobie.\n");
}

/**
 * Wywo�ana gdy obiekt ma ustawiony prop OBJ_M_NO_SELL (jest niesprzedawalny).
 * @param     ob  - obiekt
 * @param     str - komentarz do OBJ_M_NO_SELL ustawiony przez obiekt
 */
void
shop_hook_sell_no_sell(object ob, string str)
{
    if (stringp(str))
        notify_fail(str);
    else
        notify_fail("Nikt nie kupi od ciebie " + ob->short(PL_DOP) + ".\n");
}

/**
 * Wywo�ana gdy obiekt ma ustawiony prop OBJ_M_NO_BUY (nie mo�na go kupi�).
 * @param     ob  - obiekt
 * @param     str - komentarz do OBJ_M_NO_BUY ustawiony przez obiekt
 */
void
shop_hook_buy_no_buy(object ob, string str)
{
    if (stringp(str))
        notify_fail(str);
    else
        notify_fail(capitalize(ob->short(PL_MIA)) +
            " nie " + (ob->query_tylko_mn() ? "s�" : "jest") + " na " +
            "sprzeda�.\n");
}

/**
 * Wywo�ana gdy obiekt nie chce by� przeniesiony do magazynu.
 * @param     ob  - obiekt
 * @param     err - kod b��du z move()
 */
void
shop_hook_sell_object_stuck(object ob, int err)
{
    notify_fail("Zdaje si�, �e jeste� skazan" + this_player()->koncowka("y",
         "a") + " na " + ob->short(PL_BIE) + ".\n");
}

/**
 * Wywo�ana gdy sprzedawca nie ma czym zap�aci�(Takich sytuacji raczej nie powinno by�) -
 * /lib/trade.c ustawia standardowe odpowiedzi, tutaj mo�na ustawi�
 * inne.
 *
 * @param    ob - obiekt
 * @param    arr - Kod b��du z trade.c
 */
void
shop_hook_sell_cant_give(object ob, int *arr)
{
}

/**
 * Wywo�ana gdy gracz dostaje pieni�dze za sprzedane rzeczy.
 * @param     str - string opisuj�cy otrzymane pieni�dze
 */
void
shop_hook_sell_get_money(string str)
{
    write("Dostajesz " + str + ".\n");
}

/**
 * Wywo�ana gdy gracz nie mo�e unie�� kupionego przedmiotu.
 * @param     ob  - The object
 *                err - Error code from move()
 */
void
shop_hook_buy_cant_carry(object ob, int err)
{
    notify_fail("Nie mo�esz unie�� " + ob->short(PL_DOP) + ".\n");
}

/**
 * Wywo�ana gdy gracz nie mo�e zap�aci� za to co chce kupi�.
 * /lib/trade.c ustawia standardowe odpowiedzi, tutaj mo�na ustawi�
 * inne.
 * @param    ob - obiekt
 * @param    arr - The error code as it comes from trade.c
 */
void
shop_hook_buy_cant_pay(object ob, int *arr)
{
}

/**
 * Wywo�ana gdy nie mo�emy pobra� pieni�dze od gracza
 * z jakiego� dziwnego powodu (fa�szywki?)
 * @param    ob - obiekt kt�ry gracz chcia� kupi�
 */
void
shop_hook_buy_magic_money(object ob)
{
    write("Hmm, masz bardzo dziwne pieni�dze! Nie ma mowy!\n");
}

/**
 * Wywo�ana gdy gracz p�aci za co�
 * @param     str    - string opisuj�cy ile gracz p�aci
 *                change - string opisuj�cy ile dostaje reszty (je�li w og�le)
 */
void
shop_hook_buy_pay_money(string str, string change)
{
    write("P�acisz " + str);
    if (change)
        write(" i dostajesz " + change + " reszty");
    write(".\n");
}

/**
 * Wywo�ana gdy gracz wycenia obiekt kt�ry trzyma w r�ku.
 * @param     ob   - The object
 *                text - The price in text form
 */
void
shop_hook_value_held(object ob, string text)
{
    write("Za " + ob->short(PL_BIE) + " dosta�" +
        this_player()->koncowka("by", "aby") + "� " + text + ".\n");
}

/**
 * Wywo�ana gdy gracz wycenia obiekt kt�ry chce kupi�
 * @param     ob   - The object
 *                text - The value in text form
 */
void
shop_hook_value_store(object ob, string text)
{
    write(capitalize(ob->short(PL_MIA)) + " kosztowa�" +
        ob->koncowka("by", "aby", "oby", "iby", "yby") + " ci�" +
        text + ".\n");
}

/**
 * Funkcja okre�la co widz� inni gracze gdy nasz gracz co� wycenia.
 * @param     str - The text form what the player is asking about
 */
void
shop_hook_value_asking(string str)
{
    saybb(QCIMIE(this_player(), PL_MIA) + " pyta si� o jakie� ceny.\n");
}

/**
 * Ten hook m�wi nam, �e nie skupujemy kradzionych rzeczy
 * tj. z query_orig_place innym, ni� dana lokacja sklepu, oraz
 * wszystkich innych magazyn�w w �wiecie
 */
void
shop_hook_stole_items(object *co)
{
    write("Nie skupuj� kradzionych rzeczy!\n");
}

/**
 * Funkcja okre�la co widz� inni gracze gdy nasz gracz prosi o pokazanie czego�.
 */
void
shop_hook_appraise_object(object ob)
{
    saybb(QCIMIE(this_player(), PL_MIA) + " prosi o pokazanie " +
          ob->short(PL_DOP) + ".\n");
}

/**
 * List an object
 * @param    ob - obiekt
 */
void
shop_hook_list_object(object ob, int price)
{
    string str, mess;

    str = sprintf("%-25s", capitalize(ob->short(PL_MIA)));
    if (mess = text(split_values(price), 0))
        write(str + mess + ".\n");
    else
        write(str + "Za ten przedmiot nic by� nie zap�aci�" +
            this_player()->koncowka("", "a") + ".\n");
}


/* ------------------------- ustawienia handlarza ---------------------- */

/**
 * Obiekt b�dzie rozpoznawany jako handlarz.
 */
int
query_handlarz()
{
    return 1;
}

/**
 * Okre�la gdzie jest magazyn.
 * @param     str - �cie�ka do obiektu magazynu
 */
void
set_store_room(mixed store)
{
    store_room = store;
    store_room->load_me();
    store_room->add_prop(SPRZEDAWCA,TO);
}

/**
 * Sprawdza gdzie jest magazyn z kt�rego korzystamy.
 * @return   plik b�d� obiekt magazynu
 */
mixed
query_store_room()
{
    return store_room;
}

/**
 * Okre�la gdzie obiekt przechowuje swoje pieni�dze.
 * @param     str - �cie�ka do portfela
 */
void
set_skarbonka(mixed portfel)
{
   skarbonka = portfel;
}

/**
 * Sprawdza gdzie jest porfel z kt�rego korzystamy.
 * @return   plik b�d� obiekt portfela
 */
mixed
query_skarbonka()
{
    return skarbonka;
}


/**
 * Ustala jakiego typu przedmioty mog� by� przez sklep skupowane.
 * Domy�lnie config_default_shop() ustala na zero,
 * czyli sklep niczego nie skupuje.
 * @param     int argument w postacie O_UBRANIA | O_ZBROJE
*/
void
set_co_skupujemy(int war)
{
    obiekty_skupowane = war;
}

/**
 * Sprawdza, czy dany przedmiot mo�e by� skupiony z pomini�ciem typu.
 *
 * @param ob sprawdzany obiekt
 */
int
check_skupujemy(object ob)
{
  return -1;
}

/**
 * Ustala jakiego typu przedmioty mog� by� przez sklep sprzedawane.
 * Domy�lnie config_default_shop() ustala na MAXINT,
 * czyli sklep sprzedaje wszystko.
 * @param     int argument w postacie O_UBRANIA | O_ZBROJE
*/
void
set_co_sprzedajemy(int war)
{
    obiekty_sprzedawane = war;
}

/**
 * Sprawdza, czy dany przedmiot mo�e by� sprzedany z pomini�ciem typu.
 *
 * @param ob sprawdzany obiekt
 */
int
check_sprzedajemy(object ob)
{
  return -1;
}

/**
 * Ustawia standardowe opcje handlowania, oraz 3 opcje sklepowe:
 * - magazynem jest this_object()
 * - pieni�dze s� w inwentarzu this_object()
 * - �aden typ przedmiot�w nie jest skupowany
 * - ka�dy typ przedmiot�w jest sprzedawany
 *
 * Je�li nie chcesz u�y� tej funkcji, to pami�taj, �e musisz co najmniej wywo�a�
 * config_default_trade() i ustawi� jaki� magazyn wywo�aniem set_store_room()
 * oraz portfel sklepikarza wywo�aniem set_skarbonka().
*/
void
config_default_sklepikarz()
{
    config_default_trade();

    set_store_room(this_object());
    set_skarbonka(this_object());
    set_co_skupujemy(0);
    set_co_sprzedajemy(MAXINT);

    //sklepikarz domy�lnie sprz�ta sw�j zak�ad co jaki� czas
    //(niekoniecznie co godzine)
    TO->set_alarm_every_hour("co_godzine_sprzatanko");
}


/**
 * Zwraca troch� statystyk dotycz�cych ustawie� sklepikarza.
 * @return string z ustawieniami
 */
string
stat_sklepikarz()
{
    string str;
    int i;

    if (stringp(query_store_room()))
        str = sprintf("Magazyn: %s\n",query_store_room());
    else if (objectp(query_store_room()))
        str = sprintf("Magazyn: %s\n", file_name(query_store_room()));
    else
        str = "Magazyn: ---\n";

    str += sprintf("Skupuj�: %d\n",obiekty_skupowane);
    if (check_skupujemy(0) != -1)
      str += "Ponadto skupuj� tak�e inne rzeczy.\n";

    str += sprintf("Sprzedaj�: %d\n",obiekty_sprzedawane);
    if (check_sprzedajemy(0) != -1)
        str += "Ponadto sprzedaj� tak�e inne rzeczy.\n";

    return str;
}

/*sklepikarz sprz�ta sw�j sklep co jaki� czas. Je�li ma kosz na �mieci.
 Vera*/
int
co_godzine_sprzatanko()
{
    //takie tam, �eby nie by�o tak machinalnie! Niech �yje spontan ;)
    if(!random(3))
        return 0;

    object *kosze = ({ }),
           *zgarniamy= ({ }),
           *dla_siebie = ({ });


    kosze = filter(all_inventory(ENV(TP)),&operator(!=)(0,) @
                    &->query_kosz_na_smieci());

    //bez kosza jak mamy sprz�ta�?
    if(!sizeof(kosze))
        return 0;

    zgarniamy = FILTER_CAN_SEE(FILTER_DEAD(all_inventory(ENV(TP))),TO);

    //nie ma co sprz�ta�
    if(!sizeof(zgarniamy))
        return 0;


    //najpierw otwieramy, nawet jak nie b�dzie co wrzuci�.
    if(!kosze[0]->query_open())
            TO->command("otworz "+OB_NAME(kosze[0]));
    if(!kosze[0]->query_open())
            return 0;

    foreach(object x : zgarniamy)
    {
        if(x->is_coin()) //jak kasa to bierzemy dla siebie. Najpierw zapytajmy
        {
            dla_siebie+=({x});
            zgarniamy-=({x});
        }
        else if(x->query_kosz_na_smieci() || x->query_prop(OBJ_M_NO_GET) ||
                 x->move(kosze[0]) != 0 )
            zgarniamy-=({x});
    }

    if(sizeof(dla_siebie))
    {
        //dajmy do zrozumienia graczom, �eby zabrali kas�, je�li s� na lok.
        if(!sizeof(FILTER_OTHER_LIVE(all_inventory(ENV(TO)))))
            TO->command("wez "+COMPOSITE_DEAD(dla_siebie,PL_BIE));
        else
        {
            TO->command("wskaz na "+COMPOSITE_DEAD(dla_siebie,PL_BIE));
            set_alarm(1.0,0.0,"command",
                        "rozejrzyj sie pytajaco");
            set_alarm(5.0+itof(random(6)),0.0,"command",
                        "wez "+COMPOSITE_DEAD(dla_siebie,PL_BIE));
        }
    }


    if(sizeof(zgarniamy))
        tell_roombb(ENV(TO),QCIMIE(TO,PL_MIA)+" wrzuca "+COMPOSITE_DEAD(zgarniamy,PL_MIA)+
                " do "+kosze[0]->short(PL_DOP)+".");

    TO->command("zamknij "+OB_NAME(kosze[0]));

    return 1;
}

/**
 * Funkcja do wywo�ania w init(), dodaje sklepowe komendy:
 * kup, sprzedaj, wyce�, poka�, przejrzyj, store.
 */
void
init_sklepikarz()
{
    add_action(try_buy,   "kup");
    add_action(help_buy,   "?kup");
    add_action(try_sell,  "sprzedaj");
    add_action(try_value, "wyce�");
    add_action(try_show,  "poka�");
    add_action(help_list, "?przejrzyj");
    add_action(try_list,  "przejrzyj");
    add_action(do_store,  "store");
    add_action(pomoc,"?");
    add_action(pomoc2,"?",2);

}

int
pomoc(string str)
{
    if(strlen(str))
        return 0;

    write("W tym miejscu jest handlarz. Je�li interesuje ci� pomoc\n"+
        "do niego - u�yj sk�adni pomocy podaj�c po znaku zapytania\n"+
        "osob�, kt�ra jest handlarzem. Np. \"?stary mezczyzna\".\n");
    return 1;
}

public int
pomoc2(string str)
{
    object ob;
    notify_fail("Nie ma pomocy na ten temat.\n");

    if (!str)
        return 0;

    if (!parse_command(str, ENV(TP), "%o:" + PL_MIA, ob))
        return 0;

    if (!ob)
        return 0;

    if (ob != this_object())
        return 0;

   write("U kupca mo�esz:\n"+
        " - kupi�     -  kupujesz wybrany przedmiot\n"+
        " - sprzeda�  -  analogicznie\n"+
        " - wyceni�   -  pozwala ci wyceni� za ile posiadany przez ciebie\n "+
        "                przedmiot uda�oby ci si� sprzeda�\n"+
        " - pokaza�   -  sprzedawca pokazuje ci przedmiot, dzi�ki czemu mo�esz\n"+
        "                przyjrze� si� czemu� bli�ej przed dokonaniem zakupu\n"+
        " - przejrze� -  pozwala ci mie� wgl�d na ca�� list� produkt�w, kt�re\n"+
        "                dany kupiec aktualnie posiada na magazynie.\n\n" +
        "\nAcha i pami�taj, �e mieszka�cy p�ac� mniej za wystawione towary.\n\n");
   return 1;
}



/* ----------------------------------------------------------------------- */
/* funkcje 'try' - obs�uguj� wielu kupc�w na lokacji */
/* ----------------------------------------------------------------------- */
/**
 * Funkcja wypisuje notify fail, je�li argument jest 0.
 * @param a to co zwraca do_buy, do_sell, do_value, itd.
 * @return zawsze 1
*/
int
wypisz_nf(int a)
{
    if(a == 0)
        write(stringp(query_notify_fail()) ? query_notify_fail() : "");
    return 1;
}

int
try_buy(string str)
{
    object *kupcy;
    mixed *a;
    string str1, str2,str3;
    int size;

    kupcy = filter(all_inventory(environment(this_player())), &->query_handlarz());
    size = sizeof(kupcy);

    if (!str || str =="")
    {
        if(size == 1)
            write("Kup co?\n");
        else
            write("Kup co od kogo?\n");
        return 1;
    }

    if(sscanf(str,"%s od %s",str1,str2) == 2)
    {
        /* <co�> od <kogo�> za <monety> i wez <reszte>" */
        if (sscanf(str, "%s od %s za %s", str1, str2, str3) == 3)
              str = str1 + " za " + str3;
        else
              str = str1;

        a = FIND_STR_IN_OBJECT(str2, environment(), PL_DOP);
        size = sizeof(a);

        a -= ({TP});
        a = filter(a, &->query_handlarz());

        if(sizeof(a) == 1)
        {
            if(!a[0]->czy_handlujemy())
                write("Ta osoba aktualnie nie chce niczym handlowa�.\n");
            else
                return wypisz_nf(a[0]->do_buy(str));

            return 1;
        }
        else if(sizeof(a) > 1)
            write("Kup, ale od kogo dok�adnie?\n");
        else if(size > sizeof(a))
            write("Ta osoba nie jest handlarzem.\n");
        else
            write("Nie ma tu nikogo takiego.\n");

        return 1;
    }
    else if(size == 1)
    {
        if(!kupcy[0]->czy_handlujemy())
            write("Ta osoba aktualnie nie chce niczym handlowa�.\n");
        else
            return wypisz_nf(do_buy(str));
    }
    else
    {
        write("Od kogo kupi�?\n");
        return 1;
    }

    return 1;
}

int
try_sell(string str)
{
    object *kupcy;
    mixed *a;
    string str1, str2,str3;
    int size, b;

    kupcy = filter(all_inventory(environment(this_player())), &->query_handlarz());
    size = sizeof(kupcy);

    if (!str || str =="")
    {
        if(size == 1)
            write("Sprzedaj co?\n");
        else
            write("Sprzedaj co komu?\n");
        return 1;
    }

    if(parse_command(str,environment(),"%s %l:" + PL_CEL + " %s",str1,a,str2))
    {
        if(str2 == "")
              str = str1;
        else if (sscanf(str2, "za %s", str3) == 1)
              str = str1 + " " + str2;
        else
        {
            write("Sprzedaj co komu za jaki typ monet?\n");
            return 1;
        }

        a = ENV_ACCESS(a);
        size = sizeof(a);
        a = filter(a, &->query_handlarz());

        if(sizeof(a) == 1)
        {
            if(!a[0]->czy_handlujemy())
                write("Ta osoba aktualnie nie chce niczym handlowa�.\n");
            else
                return wypisz_nf(a[0]->do_sell(str));

            return 1;
        }
        else if(sizeof(a) > 1)
            write("Sprzedaj, ale komu dok�adnie?\n");
        else if(size > sizeof(a))
            write("Ta osoba nie jest handlarzem.\n");
        else
            write("Nie ma tu nikogo takiego.\n");

        return 1;

    }
    else if(size == 1)
    {
        if(!kupcy[0]->czy_handlujemy())
            write("Ta osoba aktualnie nie chce niczym handlowa�.\n");
        else
            return wypisz_nf(do_sell(str));
    }
    else
    {
        write("Komu sprzeda�?\n");
        return 1;
    }

    return 1;
}

int
help_list()
{
    write("U�yj tej komendy do przegl�dania ofert sklepikarzy, kupc�w itd.\n"+
          "Je�li w twoim pobli�u znajduje si� kilka istot wystawiaj�cych "+
          "przedmioty na sprzeda�, musisz wtedy skonkretyzowa� czyj� ofert� "+
          "chcesz przejrze�, a nast�pnie od kogo (je�li si� ju� zdecydujesz) "+
          "chcesz kupi�.\n");
    return 1;
}

int
help_buy()
{
    write("U�yj tej komendy, aby kupi� interesuj�cy si� przedmiot. "+
          "Je�li w twoim pobli�u znajduje si� kilka istot wystawiaj�cych "+
          "przedmioty na sprzeda�, musisz wtedy skonkretyzowa� od kogo chcesz "+
          "�w dany przedmiot kupi�.\n");
    return 1;
}

int
try_list(string str)
{
    object *kupcy;
    mixed *a;
    string str1;
    int size;

    kupcy = filter(all_inventory(environment(this_player())), &->query_handlarz());
    size = sizeof(kupcy);

    if (!str || str == "" || str ~= "ofert�")
    {
        if(size > 1)
        {
            write("Czyj� ofert� chcesz przejrze�?\n");
            return 1;
        }
        if(!kupcy[0]->czy_handlujemy())
        {
            write("Ta osoba aktualnie nie chce niczym handlowa�.\n");
            return 1;
        }
        return wypisz_nf(do_list(0));
    }
    else if(sscanf(str,"ofert� %s",str1) == 1)
    {
        a = FIND_STR_IN_ARR(str1, all_inventory(environment()) - ({ this_player() }), PL_DOP);
        size = sizeof(a);
        a = filter(a, &->query_handlarz());

        if(sizeof(a) == 1)
        {
            if(!a[0]->czy_handlujemy())
                write("Ta osoba aktualnie nie chce niczym handlowa�.\n");
            else
                return wypisz_nf(a[0]->do_list(0));

            return 1;
        }
        else if(sizeof(a) > 1)
            write("Przejrzyj, ale czyj� dok�adnie ofert�?\n");
        else if(size > sizeof(a))
            write("Ta osoba nie jest handlarzem.\n");
        else
            write("Nie ma tu nikogo takiego.\n");

        return 1;
    }
    else if(size == 1)
    {
        if(!kupcy[0]->czy_handlujemy())
            write("Ta osoba aktualnie nie chce niczym handlowa�.\n");
        else
            return wypisz_nf(do_list(str));
    }
    else
    {
        write("Czyj� ofert� przejrze�?\n");
        return 1;
    }

    return 1;
}

int
try_value(string str)
{
    object *kupcy;
    mixed *a;
    string str1, str2;
    int size;

    kupcy = filter(all_inventory(environment(this_player())), &->query_handlarz());
    size = sizeof(kupcy);

    if (!str || str =="")
    {
        if(size == 1)
            write("Co chcesz wyceni�?\n");
        else
            write("Co i u kogo chcesz wyceni�?\n");
        return 1;
    }

    if(sscanf(str,"%s u %s",str1,str2) == 2)
    {
        a = FIND_STR_IN_OBJECT(str2, environment(), PL_DOP);
        size = sizeof(a);
        a-=({TP});
        a = filter(a, &->query_handlarz());

        if(sizeof(a) == 1)
        {
            if(!a[0]->czy_handlujemy())
                write("Ta osoba aktualnie nie chce niczym handlowa�.\n");
            else
                return wypisz_nf(a[0]->do_value(str1));

            return 1;
        }
        else if(sizeof(a) > 1)
            write("Wyce�, ale u kogo dok�adnie?\n");
        else if(size > sizeof(a))
            write("Ta osoba nie jest handlarzem.\n");
        else
            write("Nie ma tu nikogo takiego.\n");

        return 1;
    }
    else if(size == 1)
    {
        if(!kupcy[0]->czy_handlujemy())
            write("Ta osoba aktualnie nie chce niczym handlowa�.\n");
        else
            return wypisz_nf(do_value(str));
    }
    else
    {
        write("U kogo wyceni�?\n");
        return 1;
    }

    return 1;
}

int
try_show(string str)
{
    object *kupcy;
    mixed *a;
    string str1, str2,str3;
    int size;

    kupcy = filter(all_inventory(environment(this_player())), &->query_handlarz());
    size = sizeof(kupcy);

    if (!str || str =="")
    {
        if(size == 1)
            write("Poka� co?\n");
        else
            write("Poka� co od kogo?\n");
        return 1;
    }

    if(sscanf(str,"%s od %s",str1,str2) == 2)
    {
        a = FIND_STR_IN_OBJECT(str2, environment(), PL_DOP);
        size = sizeof(a);
		a-=({TP});
        a = filter(a, &->query_handlarz());

        if(sizeof(a) == 1)
        {
            if(!a[0]->czy_handlujemy())
                write("Ta osoba aktualnie nie chce niczym handlowa�.\n");
            else
                return wypisz_nf(a[0]->do_show(str1));

            return 1;
        }
        else if(sizeof(a) > 1)
            write("Poka� przedmiot, ale od kogo dok�adnie?\n");
        else if(size > sizeof(a))
            write("Ta osoba nie jest handlarzem.\n");
        else
            write("Nie ma tu nikogo takiego.\n");

        return 1;
    }
    else if(size == 1)
    {
        if(!kupcy[0]->czy_handlujemy())
            write("Ta osoba aktualnie nie chce niczym handlowa�.\n");
        else
            return wypisz_nf(do_show(str));
    }
    else
    {
        write("Pokaza� przedmiot, ale od kogo?\n");
        return 1;
    }

    return 1;
}

/* ----------------------------------------------------------------------- */
/* ------------------------ pozosta�e funkcje ------------------- */
/* ----------------------------------------------------------------------- */

/**
 * Liczy cen� kt�r� gracz zap�aci za kupno obiektu.
 * @param    ob - obiekt
 * @return     cena
 */
int
query_buy_price(object ob)
{
    /*
    int seed;
    seed = crypt(MASTER_OB(this_player()), "ab")[2];
    return 3 * ob->query_prop(OBJ_I_VALUE) * (query_money_greed_buy()
    +15 - this_player()->query_skill(SS_TRADING) / 4 + random(15, seed))/ 200;  */

    /* zmieni�em formu�� na prostsz� - molder */
    /* a ja doda�em ni�sze ceny dla mieszka�c�w - vera */
   // string *odmiana = SLOWNIK->query_odmiana(TO->query_spolecznosc());

    //Je�li jest prop nie uwzgl�dniamy uma targowanie.
    /*if(TO->query_prop(SELLER_I_NO_BARGAIN))
        return ftoi(max(1.0, itof(ob->query_prop(OBJ_I_VALUE) + query_money_greed_buy())));

    if(pointerp(odmiana) && "z " + odmiana[0][1] ~= TP->query_origin())
    {
        if(!ob->query_prop(OBJ_I_VALUE))
            return 0;
        return ftoi(max(1.0, itof(ob->query_prop(OBJ_I_VALUE))  *
            (1.0 + (itof(query_money_greed_buy() - this_player()->query_skill(SS_TRADING)) / 200.0))));
    }
    else
    {
        if(!ob->query_prop(OBJ_I_VALUE))
            return 0;
        return ftoi(max(1.0, itof(ob->query_prop(OBJ_I_VALUE))  *
            (1.0 + (itof(query_money_greed_buy() - this_player()->query_skill(SS_TRADING)) / 100.0))));
    }*/


    mixed odmiana = slownik_pobierz(TO->query_spolecznosc());
    int greed = query_money_greed_buy();
    int val = ob->query_prop(OBJ_I_VALUE);
    int price;
    float real_greed = itof(greed) - 100.0;

    /* Jesli kupiec sprzedaje po zmniejszonej lub rzeczywistej cenie
     * to nie targujemy sie z znim
     */
    if(real_greed <= 0.0)
        price = ftoi(((real_greed + 100.0)/100.0)*itof(val));
    else
    {

        /* Znizka dla mieszkancow */
        if(pointerp(odmiana) && "z " + odmiana[0][1] ~= TP->query_origin())
            real_greed *= 0.85;

        real_greed *= LINEAR_FUNC(0.0, 1.0, 100.0, 0.2,
                                 itof(TP->query_skill(SS_TRADING)));

        price = ftoi(( 1.0 + (real_greed)/100.0 )*itof(val));
    }

    if(price <= 0)
       return 1;

    return price;
}

/**
 * Liczy cen� kt�r� gracz dostanie za sprzeda� obiektu.
 * @param    ob - obiekt
 * @return   cena
 */
int
query_sell_price(object ob)
{
    //int price;
    /* chcemy aby niezale�nie od skila gracz dosta� zawsze
        min. 20% warto�ci przedmiotu */
    //if(this_player()->query_skill(SS_TRADING) < query_money_greed_sell() - 100)
    //        price = ftoi(0.2 * itof(ob->query_prop(OBJ_I_VALUE)));
    //else
    //    price = ftoi(itof(ob->query_prop(OBJ_I_VALUE))  *
    // (0.2 + (itof(200 - query_money_greed_sell() - 20) / 100.0) *
    //        (itof(this_player()->query_skill(SS_TRADING)) / 100.0) ));
    /* Jak co� ma jak�� warto�� to minimum za 1 grosz sprzedajemy */
   // if ((ob->query_prop(OBJ_I_VALUE) > 0) && (price <= 0))
	 //   return 1;

  //  return price;


     int greed = query_money_greed_sell();
     int val = ob->query_prop(OBJ_I_VALUE);
     int price;

     if(greed <= 100)
        price = ftoi((100.0/itof(greed))*itof(val));
     else
     {

        float mnoznik = (100.0/itof(greed));
        mnoznik *= LINEAR_FUNC(0.0, 1.0, 100.0, 4.6, itof(TP->query_skill(SS_TRADING)));

        if(mnoznik > 1.0)
            mnoznik = 1.0;

        if(mnoznik < 0.2)
            mnoznik = 0.2;

        price = ftoi(mnoznik*itof(val));
     }

    /* Jak co� ma jak�� warto�� to minimum za 1 grosz sprzedajemy */
    if ((ob->query_prop(OBJ_I_VALUE) > 0) && (price <= 0))
        return 1;

    return price;
}

/**
 * Wywo�ana gdy gracz u�ywa komendy 'store'.
 * T� komend� czarodzieje mog� przenie�� si� do pomieszczenia magazynu.
 * Argumenty    : string str - the command line argument (should be void)
 * Zwraca      : int 1/0 - success/failure.
 */
int
do_store(string str)
{
    /* Komenda tylko dla wizard�w. */
    if (!this_player()->query_wiz_level())
    {
        return 0;
    }

    store_room->load_me();
    if (!objectp(store_room) && !objectp(find_object(store_room)))
    {
        notify_fail("Nie mog� znale�� magazynu. Dobrze by by�o to zg�osi� czarodziejom.\n");
        return 0;
    }

    this_player()->move_living("X", store_room);
    return 1;
}



/**
 * Wywo�ana gdy gracz pr�buje sprzeda� tablic� obiekt�w.
 * Obiekty kt�re uda si� sprzeda� s� przenoszone do magazynu.
 * @param  ob - tablica obiekt�w kt�re gracz chce sprzeda�
 * @param  check - je�li 0, to sprzedajemy rzeczy dobyte i za�o�one
 * @param  str - string opisuj�cy jaki typ pieni�dzy gracz chce dosta�
 * @return   tablic� sprzedanych obiekt�w (lub 0 je�li nic nie sprzedano)
 */
object *
sell_it(object *ob, string str, int check)
{
    int price, i, j, k, num, *tmp_arr, *null, *value_arr, *null_arr, *arr, err, exp_price;
    object *sold;
    mixed tmp;

    call_other(store_room, "load_me"); /* Make sure store room is loaded */

    /* magazyn musi by� pokojem, albo by� sprzedawc� (b�d� w inwentarzu sprzedawcy) */
    if (!store_room->is_room() && store_room != this_object() && environment(store_room) != this_object())
        return 0;

    num = sizeof(query_money_types());
    value_arr = allocate(num*2);
    null_arr = value_arr + ({});
    sold = allocate(sizeof(ob));

    for (i = 0; i < sizeof(ob); i++)
    {
        if (!(ob[i]->query_type() & obiekty_skupowane) && (check_skupujemy(ob[i]) < 1))
        {
            shop_hook_tego_nie_skupujemy(ob[i]);
            continue;
        }

        if (ob[i]->query_prop(OBJ_I_VALUE) == 0)
        {
            shop_hook_sell_no_value(ob[i]);
            continue;
        }

        if (check && (ob[i]->query_worn() || ob[i]->query_wielded()))
        {
            shop_hook_sell_worn_or_wielded(ob[i]);
            continue;
        }

        if (tmp = ob[i]->query_prop(OBJ_M_NO_SELL))
        {
            shop_hook_sell_no_sell(ob[i], tmp);
            continue;
        }

        if (ob[i]->query_prop(OBJ_I_HAS_FIRE))
        {
            shop_hook_sell_has_fire(ob[i]);
            continue;
        }

        /* przechowujemy cen� na wypadek gdyby obiekt zosta�
         * zniszczony przez move() */
        price = query_sell_price(ob[i]);

        if (price <= 0)
        {
            shop_hook_sell_no_value(ob[i]);
            continue;
        }

        if (err = ob[i]->move(store_room))
        {
            shop_hook_sell_object_stuck(ob[i], err);
            continue;
        }

        if (price > 0)
        {
            tmp_arr = calc_change(price, null, str);

            if(time() - ob[i]->query_prop(OBJ_I_LAST_TRADE) > MINIMALNA_PRZERWA_MIEDZY_OPERACJAMI_W_SKLEPIE)
            {
                exp_price += price;
                ob[i]->remove_prop(OBJ_I_LAST_TRADE);
            }

            for (k = 0; k < sizeof(tmp_arr); k++)
                value_arr[k] += tmp_arr[k];

            sold[j] = ob[i];
            j++;
            if (j >= MAX_SELL)
                break;
            /* pozwalamy na sprzeda� tylko MAX_SELL rzeczy na raz
             * �eby nie przeci��a� muda */
        }

        if (sizeof(arr = give(price, this_player(), str, 0, skarbonka)) == 1)
        {
            ob[i]->move(TP, 1);
            shop_hook_sell_cant_give(ob[i], arr);
            continue;  /* pay() can handle notify_fail() call */
        }
    }

    sold = sold - ({ 0 });

 /*                Observe there is no message written when sold item
 *                has a value higher than the shop gives out. */

    if (sizeof(sold) > 0)
    {

        shop_hook_sell_get_money(text(value_arr, 0));

        //za towar <1dn 0 expa, a max 100punkt�w. co by nie by�o tak �atwo expi�. Vera
        this_player()->increase_ss(SS_TRADING, min(100,(exp_price/20) *
            EXP_HANDEL_KUPIENIE_ZA_20_GROSZY));

        ob->add_prop(OBJ_I_LAST_TRADE, time());

        return sold;
    }
    else
         return 0;
 }

/**
 * Wywo�ana gdy gracz u�yje komendy 'sprzedaj'.
 *
 * @param     str - to co gracz wpisa� po 'sprzedaj'
 * @return     1 je�li uda�o si� co� sprzeda�
 */
int
do_sell(string str)
{
    object *item;
    int value, check;
    string str1, str2;

    if (!str || str =="")
    {
        notify_fail("Sprzedaj co?\n");
        return 0;
    }

    if((obiekty_skupowane == 0) && (check_skupujemy(0) == -1))
    {
        shop_hook_niczego_nie_skupujemy();
        return 0;
    }

    /*  Did player specify how to get the money? */
    if (sscanf(str, "%s za %s", str1, str2) != 2)
    {
        str1 = str;
        str2 = "";
    }

    if (str1 == "wszystko!")
    {
        str1 = "wszystko";
        check = 0;
    }
    else
        check = 1;

    item = FIND_STR_IN_OBJECT(str1, this_player(), PL_BIE);
    if (!sizeof(item))
    {
        shop_hook_sell_no_match(str1);
        return 0;
    }

    //Vera. Nie skupujemy kradzionych, tj.
    //o query_orig_place() takim jak kt�ry� z magazyn�w w �wiecie
    object *skradzione = filter(item, &operator(!=)(0) @ sizeof @ &->query_orig_place());

    foreach(object x : skradzione)
    {
        object skad = find_object(x->query_orig_place()[0]);

        if(skad->query_prop(SPRZEDAWCA) || x->query_prop(OBJ_I_NIE_SKRADZIONY))
            skradzione -= ({x});
    }

    if(sizeof(skradzione))
    {
        shop_hook_stole_items(skradzione);
        return 0;
    }

    item = sell_it(item, str2, check);
    if (sizeof(item))
    {
        shop_hook_sold_items(item);
        return 1;
    }

    if (str1 == "wszystko")
        shop_hook_sold_nothing();

    return 0; /* nic nie sprzedano */
}

/**
 * Wywo�ana gdy gracz pr�buje kupi� tablic� obiekt�w.
 * Najwy�ej jeden obiekt b�dzie mu sprzedany (pierwszy kt�ry da rad� kupi�).
 *
 * @param     ob - tablica obiekt�w kt�re chcemy kupi�
 *                  str2 - typ monet kt�rymi chcemy zap�aci�
 *                  str3 - typ monet jakie chcemy dosta� jako reszt�
 * @return     tablica kupionych obiekt�w (lub 0 je�li nic nie kupiono)
 */
object *
buy_it(object *ob, string str2, string str3)
{
    int price, exp_price, i, j, k, *value_arr, *arr, error, num, err;
    object *bought;
    mixed tmp;

    num = sizeof(query_money_types());
    value_arr = allocate(2 * num);
    bought = allocate(sizeof(ob));

    for (i = 0; i < sizeof(ob); i++)
    {
        /* przypadkiem kto� si� mo�e w magazynie zapl�ta� */
        if(ob[i]->is_humanoid())
        {
            notify_fail("Eee ... ten kto� nie jest na sprzeda�.\n");
            continue;
        }

        if (!(ob[i]->query_type() & obiekty_sprzedawane) && (check_sprzedajemy(ob[i]) < 1))
        {
            shop_hook_tego_nie_sprzedajemy(ob[i]);
            continue;
        }

        if (tmp = ob[i]->query_prop(OBJ_M_NO_BUY))
        {
            shop_hook_buy_no_buy(ob[i], tmp);
            continue;
        }

        if (ob[i]->query_worn())
        {
            notify_fail("Nie sprzedam ci swojego ubrania!\n");
            continue;
        }

        if (ob[i]->query_wielded())
        {
            notify_fail("Nie sprzedam ci swojej broni!\n");
            continue;
        }

        price = query_buy_price(ob[i]);

        /* If you don't feel greedy you can shorten the calculation above. */
        if (err = ob[i]->move(this_player()))
        {
            shop_hook_buy_cant_carry(ob[i], err);
            continue;
        }

        /* tutaj przekazujemy pieni�dze sklepikarzowi */
        if (sizeof(arr = pay(price, this_player(), str2, 0, skarbonka, str3)) == 1)
        {
            ob[i]->move(store_room, 1);
            shop_hook_buy_cant_pay(ob[i], arr);
            continue;  /* pay() can handle notify_fail() call */
        }

        /* Detect if there was a move error. */
        if (error = arr[sizeof(arr) - 1])
        {
            if (error < -1)
            {
            /* Couldn't take the money from player, the coins were stuck */
                shop_hook_buy_magic_money(ob[i]);
                ob[i]->move(store_room, 1);
                continue;
            }
            /* We don't want the money so no move error to us, if there was one
               it was because the player couldn't hold all coins, and if so the
               drop text is already written, but the deal is still on :) */

            /* chyba jednak b�dziemy chcieli pieni�dze ... - molder */
        }

        if(time() - ob[i]->query_prop(OBJ_I_LAST_TRADE) > MINIMALNA_PRZERWA_MIEDZY_OPERACJAMI_W_SKLEPIE)
        {
            exp_price += price;
            ob[i]->remove_prop(OBJ_I_LAST_TRADE);
        }

        for(k = 0; k < 2 * num; k++)
            value_arr[k] += arr[k];

        bought[j] = ob[i];
        j++;
        if (j >= 1)
            break;
        /* Well, we don't want to let a player accidentily buy too much :) */
    }

    bought = bought - ({ 0 });

    if (sizeof(bought) > 0)
    {
        shop_hook_buy_pay_money(text(value_arr[0 .. num - 1], PL_BIE),
            text(value_arr[num .. 2 * num - 1], PL_BIE));

        //za towar <1dn 0 expa, a max 100punkt�w. co by nie by�o tak �atwo expi�. Vera

        //A ja zrobi�em jeszcze ma�e zabezpieczonko coby kto� nie wpisywa� kup, sprzedaj
        //je�li kupujemy co� i sprzedajemy przed up�ywem 10 minut to expa nie ma:)
        //Zabezpieczenie powy�ej odpowiednio skomentowane.
        this_player()->increase_ss(SS_TRADING, min(100,(exp_price/20) *
            EXP_HANDEL_KUPIENIE_ZA_20_GROSZY));

        ob->add_prop(OBJ_I_LAST_TRADE, time());

        return bought;
    }
    else
        return 0;
}

/**
 * Wywo�ana gdy gracz u�yje komendy 'kup'.
 * @param     string - to co gracz wpisa� po 'kup'
 *                   (co kupi�, za co i jak� reszte wzi��)
 * @return     1 je�li uda�o si� co� kupi�
 */
int
do_buy(string str)
{
    object *item, *item_arr;
    string str1, str2, str3;

    if (!str || str == "")
    {
        notify_fail("Kup co?\n");
        return 0;
    }

   if((obiekty_sprzedawane == 0) && (check_sprzedajemy(0) == -1))
    {
        shop_hook_niczego_nie_sprzedajemy();
        return 0;
    }

    if (query_magazyn_jest_pusty())
    {
        shop_hook_list_empty_store(str);
        return 0;
    }

    if (!store_room->is_room() && store_room != this_object() && environment(store_room) != this_object())
        return 0;

    if (sscanf(str, "%s za %s i we� %s", str1, str2, str3) != 3)
    {
        str3 = "";
        if (sscanf(str, "%s za %s", str1, str2) != 2)
        {
            str2 = "";
            str1 = str;
        }
    }

    if (stringp(store_room))
        item_arr = all_inventory(find_object(store_room));
    else
        item_arr = all_inventory(store_room);

    item_arr = unique_array_object(item_arr);
    item = FIND_STR_IN_ARR(str1, item_arr, PL_BIE);

    if (!sizeof(item))
    {
        shop_hook_buy_no_match(str1);
        return 0;
    }

    item = buy_it(item, str2, str3);

    if (sizeof(item))
    {
        shop_hook_bought_items(item);
        return 1;
    }

    return 0;
}

/**
 * Wywo�ywana gdy gracz u�yje komendy 'wyce�'.
 * @param  str - to co gracz wpisa� po 'wyce�'
 * @return   1 je�li uda�o si� wyceni�
 */
int
do_value(string str)
{
    object *item, store;
    int *arr, price, i, j, num, no_inv;
    mixed tmp;

    if (!str || str =="")
    {
        notify_fail("Wyce� co?\n");
        return 0;
    }

    if((obiekty_skupowane == 0) && (check_skupujemy(0) == -1))
    {
        shop_hook_niczego_nie_skupujemy();
        return 0;
    }

    num = sizeof(query_money_types());
    item = FIND_STR_IN_OBJECT(str, this_player(), PL_BIE);
    if (!sizeof(item))
        no_inv = 1;

    for (i = 0; i < sizeof(item); i++)
    {
        if (!(item[i]->query_type() & obiekty_skupowane) && (check_skupujemy(item[i]) < 1))
        {
            shop_hook_tego_nie_skupujemy(item[i]);
            continue;
        }
        else if(!item[i]->query_prop(OBJ_I_VALUE))
        {
            shop_hook_sell_no_value(item[i]);
            continue;
        }
        else if(tmp = item[i]->query_prop(OBJ_M_NO_SELL))
        {
            shop_hook_sell_no_sell(item[i], tmp);
            continue;
        }

        price = query_sell_price(item[i]);
        arr = give(price, this_player(), "", 1);
        shop_hook_value_held(item[i], text(arr[num .. 2 * num - 1], 0));
        j++;
    }

#if 0
// * Nie sprawdzamy do wyceniania tego co w magazynie. Alvin.
/* by�em tu - molder. */

    call_other(store_room, "load_me");
    store = find_object(store_room);
    item = FIND_STR_IN_OBJECT(str, store, PL_BIE);

    if (!sizeof(item) && no_inv)
        return shop_hook_sell_no_match(str);

    for (i = 0; i < sizeof(item); i++)
    {
        price = query_buy_price(item[i]);
        arr = split_values(price);
        shop_hook_value_store(item[i], text(arr, 0));
        j++;
    }
#endif

    if (no_inv)
    {
        shop_hook_sell_no_match(str);
        return 0;
    }

    shop_hook_value_asking(str);

    if (j > 0)
        return 1;

    return 0;
}

/*
 * Wy�wietla pogrupowan� list� obiekt�w.
 * @param     ile       - ilosc danego towaru
 * @param    str_ob    - nazwa_obiektu
 * @param    cena      - cena obiekty
*/
varargs void
wyswietl_liste_pogrupowana(int ile, string str_ob, int cena, int show_sizes = 1)
{
    string str, mess;
    object ob;
    int i;

    if(wildmatch("*coins", str_ob)==0)
    {
        ob=find_object(str_ob);

        if(catch(ob->short(PL_MIA)))
        {
        }

        else
        {
            int kr,dn,gr;
            kr=split_values(cena)[2];
            dn=split_values(cena)[1];
            gr=split_values(cena)[0];

            string item_size = "";
            if (show_sizes && stringp((item_size = ob->query_size())))
                item_size = " [" + item_size + "]";
            else
                item_size = "";
            if (ile == 0)
            {
                str = sprintf("| %-57s | %-3d | %-3d | %-3d ",
                    capitalize(ob->short(PL_MIA)) + item_size, kr, dn, gr);
            }
            else
            {
                str = sprintf("|%-3d| %-53s | %-3d | %-3d | %-3d ", ile,
                    capitalize(ob->short(PL_MIA)) + item_size, kr, dn, gr);
            }
            write(str+"|\n");
        }
    }
}

/*
 * funkcja w tablicy stringowej wycina takie same elementy
 * Zwraca      :  string * nowa_tab - wyczyszczona tablica
 * Argumenty    : string * tab - tablica
 */
string *
unique_array_string(string *tab)
{
    string *nowa_tab = ({});
    string *check_tab = ({});
    int i, wiel_tab;
    string new_name;
    for(i=0, wiel_tab=sizeof(tab); i<wiel_tab; i++)
    {
        new_name = explode(tab[i], "#")[0] + implode(explode(tab[i], "##")[1..], "##");
        if(member_array(new_name, check_tab) == -1) {
            nowa_tab += ({tab[i]});
            check_tab += ({new_name});
        }
    }
    return nowa_tab;
}

/*
 * funkcja w tablicy obiektowej wycina takie same elementy
 * Zwraca      :  object * nowa_tab - wyczyszczona tablica
 * Argumenty    : object * tab - tablica
 */
object *
unique_array_object(object *tab)
{
    int price;
    object *nowa_tab = ({});
    string *check_tab = ({});
    string item_name;
    foreach (object ob : tab) {
        price = query_buy_price(ob);
        if((ob->query_type() & obiekty_sprzedawane) || (check_sprzedajemy(ob) == 1)) {
            if(catch(ob->short(PL_MIA)))
            {
            }
            else
            {
                string item_size = "";
                if (stringp((item_size = ob->query_size()))) {
                    item_size = " [" + item_size + "]";
                }
                else {
                    item_size = "";
                }
                item_name = MASTER_OB(ob)+"##"+ob->short(PL_MIA)+item_size+"##"+price;
                if(member_array(item_name, check_tab) == -1) {
                    nowa_tab += ({ ob });
                    check_tab += ({ item_name });
                }
            }
        }
    }
    return nowa_tab;
}

/*
 *  Zlicza ile bylo wystapien obiektow takich samych a dokladniej
 *                to stringow z sciezka do obiektow hashem i cena
 * Zwraca      :  int - ile wystapien
 * Argumenty    : string *tab - tablica ktora przeszukujemy
 *                szukana - wartosc ktorej szukamy
 */
int
ile_wystapien(string *tab, string szukana)
{
    int ilosc=0;
    string new_name;
    string new_szukana = explode(szukana, "#")[0] + implode(explode(szukana, "##")[1..], "##");
    foreach (string name : tab) {
        new_name = explode(name, "#")[0] + implode(explode(name, "##")[1..], "##");
        if(new_name == new_szukana)
            ilosc++;
    }
    return ilosc;

}

/**
 * Wywo�ana gdy gracz u�yje komendy 'przejrzyj'.
 *
 * @param str nazwa obiekt�w jakich szukamy (bronie/zbroje albo cokolwiek)
 * @param title tytu� wy�wietlany nad tabelk� z wynikami
 *
 * @return     0 je�li nie mamy niczego takiego na stanie
 * @return     1 je�li co� znale�li�my
*/
int
do_list(string str, string title = 0)
{
    object *item_arr;
    string *pog_arr=({});
    string *str_arr=({});
    object item;
    int i, max, price, *arr;

    if ((obiekty_sprzedawane == 0) && (check_sprzedajemy(0) == -1))
    {
        shop_hook_niczego_nie_sprzedajemy();
        return 0;
    }

    if (query_magazyn_jest_pusty())
    {
        shop_hook_list_empty_store(str);
        return 0;
    }

    if (!store_room->is_room() && store_room != this_object() && environment(store_room) != this_object())
         return 0;

    if (stringp(store_room))
        item_arr = all_inventory(find_object(store_room));
    else
        item_arr = all_inventory(store_room);

    //Wyfiltrowujemy wszystko co jest npcem, lub graczem

    item_arr = filter(item_arr, &not() @ &living());

    if (str == "bronie")
        item_arr = filter(item_arr, weapon_filter);
    else if (str == "zbroje")
        item_arr = filter(item_arr, armour_filter);
    else if (str)
        item_arr = FIND_STR_IN_ARR(str, item_arr, PL_BIE);

    if (sizeof(item_arr) < 1)
    {
        shop_hook_buy_no_match(str);
        return 0;
    }

    max = MIN(MAXLIST, sizeof(item_arr));

    for (i = 0; i < max; i++)
    {
        price = query_buy_price(item_arr[i]);
        if((item_arr[i]->query_type() & obiekty_sprzedawane) || (check_sprzedajemy(item_arr[i]) == 1))
        {
            if(catch(item_arr[i]->short(PL_MIA)))
            {
            }
            else
            {
                string item_size = "";
                if (stringp((item_size = item_arr[i]->query_size())))
                    item_size = " [" + item_size + "]";
                else
                    item_size = "";

                str_arr += ({file_name(item_arr[i])+"##"+item_arr[i]->short(PL_MIA)+item_size+"##"+price});
            }
        }
     }

    // tworzymy tab bez podwojnych wystapien
    pog_arr=unique_array_string(str_arr);

    if(sizeof(pog_arr) == 0)
    {
        notify_fail("Aktualnie nie ma �adnych rzeczy na sprzeda�.\n");
        return 0;
    }

    if (stringp(title))
    {
        int j;
        int len = strlen(title);
        int pre = (73 - len) / 2;
        int post = 73 - pre - len;
        string tmp_str = "  ";

        for (j = 0; j < pre; ++j)
            tmp_str += " ";

        for (j = 0; j < len; ++j)
            tmp_str += "_";

        write(tmp_str + "__\n");
            tmp_str = " ";

        for (j = 0; j < pre; ++j)
            tmp_str += "_";

        tmp_str += "/ " + title + " \\";

        for (j = 0; j < post; ++j)
            tmp_str += "_";

        write(tmp_str + "\n");
    }
    else
    {
        write("\n _________________________________________"+
            "____________________________________\n");
    }
    write("/                                                                  cena       \\\n"+
          "|ile| przedmiot                                             | kr  | dn  | gr  |\n"+
          "|========================================="+
            "====================================|\n");
    // przegladamy nowa pogrupowana tab
    for ( i = 0; i< sizeof(pog_arr); i++)
    {
        // ciapiemy stringa na tab
        // [0] nazwa sciezka do obiektu
        // [1] cena
        string * tstr_arr=explode(pog_arr[i], "##");
        wyswietl_liste_pogrupowana(
            ile_wystapien(str_arr, pog_arr[i]),
            tstr_arr[0],
            atoi(tstr_arr[2]));
    }
    write("\\________________________________________"+
          "_____________________________________/\n");

    if (max < sizeof(item_arr))
    {
        write("Lista zosta�a skr�cona.\n");
    }

    return 1;

}

/**
 * Wywo�ana gdy gracz u�yje komendy 'poka�'.
 * @param   str - string opisuj�cy co gracz chce obejrze�
 * @returns   1 - je�li uda�o si� obejrze�.

 */
int
do_show(string str)
{
    object *item_arr;
    int i, *arr;

    if ((obiekty_sprzedawane == 0) && (check_sprzedajemy(0) == -1))
    {
        shop_hook_niczego_nie_sprzedajemy();
        return 0;
    }

   if (query_magazyn_jest_pusty())
    {
        shop_hook_list_empty_store(str);
        return 0;
    }

    if (!store_room->is_room() && store_room != this_object() && environment(store_room) != this_object())
	    return 0;

    if (stringp(store_room))
	    item_arr = all_inventory(find_object(store_room));
    else
	    item_arr = all_inventory(store_room);

    item_arr = unique_array_object(item_arr);
    item_arr = FIND_STR_IN_ARR(str, item_arr, PL_BIE);

    if (sizeof(item_arr) < 1)
    {
        shop_hook_buy_no_match(str);
        return 0;
    }

    shop_hook_appraise_object(item_arr[0]);
    item_arr[0]->appraise_object();

    return 1;
}

/**
 * Filtr na bro� (obiekty dziedzicz�ce po /std/weapon)
 * @param ob - obiekt do sprawdzenia
 * @return 1 - je�li ob jest broni�
 */
int
weapon_filter(object ob)
{
    return (function_exists("create_object", ob) == "/std/weapon");
}

/**
 * Filtr na zbroje (obiekty dziedzicz�ce po /std/armour)
 * @param ob - obiekt do sprawdzenia
 * @return  1 - je�li ob jest zbroj�
*/
int
armour_filter(object ob)
{
    return (ob->is_armour());
}

/**
* Sprawdza, czy magazyn w magazynie s� jakie� obiekty
* kt�re mo�emy sprzeda�. Sprawdza tylko typ obiektu.
* @return 1 je�li magazyn jest pusty
* @return 0 je�li co� w nim jest
*/
int
query_magazyn_jest_pusty()
{
    object *item_arr;
    int i,size;

    call_other(store_room, "load_me");

    if (!store_room->is_room() && store_room != this_object()
            && environment(store_room) != this_object())
         return 1;

    if (stringp(store_room))
        item_arr = all_inventory(find_object(store_room));
    else
        item_arr = all_inventory(store_room);

    size = sizeof (item_arr);

    for (i = 0; i < size ; i++)
        if ((item_arr[i]->query_type() & obiekty_sprzedawane) || (check_sprzedajemy(item_arr[i]) == 1))
            return 0;

    return 1;
}

/*zwraca nam 1 je�li handlujemy, a na 0 ustawiamy wtedy, kiedy
    nie chcemy �eby handlarz aktualnie handlowa�. Np. bo ju� idzie do domu.
    Verk.*/
int
czy_handlujemy()
{
    return czy_handlujemy;
}
int
set_czy_handlujemy(int x)
{
    czy_handlujemy = x;
}

int
set_cena_towaru(string str, int *wartosc)
{
   object store, *item;

   if (stringp(store_room))
        store = find_object(store_room);
    else
        store = store_room;

    item = FIND_STR_IN_OBJECT(str, store, PL_BIE);

    return 0;
}

mixed simul_buy(int greed, int skill)
{
    float real_greed = itof(greed) - 100.0;

    /* Jesli kupiec sprzedaje po zmniejszonej lub rzeczywistej cenie
     * to nie targujemy sie z znim
     */
    if(real_greed <= 0.0)
        return itof((greed/100));

    real_greed *= LINEAR_FUNC(0.0, 1.0, 100.0, 0.2,
                             itof(skill));

    float price = ( 1.0 + (real_greed)/100.0 );

    if(price <= 0.0)
       return "Sprzedane za 1 grosz.";

    return price;
}

mixed simul_sell(int greed, int skill)
{
     if(greed <= 100)
        return (100/greed);

     float mnoznik = (100.0/itof(greed));
     mnoznik *= LINEAR_FUNC(0.0, 1.0, 100.0, 4.6, itof(skill));

     if(mnoznik > 1.0)
         mnoznik = 1.0;

     if(mnoznik < 0.2)
         mnoznik = 0.2;

    /* Jak co� ma jak�� warto�� to minimum za 1 grosz sprzedajemy */
     if(mnoznik <= 0.0)
        return "Sprzedane za 1 grosz.";

    return mnoznik;
}
