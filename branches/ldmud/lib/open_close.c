/**
 * \file /lib/open_close.c
 *
 * Plik standaryzuj�cy wszelkiego rodzaju otwierane przedmioty, otwierane itp.
 * Od teraz na to wszystko jest jeden standard. Je�li kto� gdzie� chce
 * u�ywa� otwierania to prosz� stosowa� ten standard.
 *
 * Co dzi�ki niemu zyskujemy.. Ano mo�liwo�� wywa�ania otwierane, u�ywania �om�w,
 * wytrych�w, etc. Oczywi�cie standardowe otwierane da sie wywarzy� tylko
 * za pomoc� �oma. A z wytrychem trzeba by� przynajmniej teoretycznie nie�le
 * obeznanym.
 *
 * Komendy typu otw�rz zosta�y przeniesione do items.c
 *
 * @author Krun
 * @date Grudzie� 2007
 *
 * TODO:
 * 1) - Dopisa� wykorzystanie otwierane
 * 2) - Dopisa� mo�liow�� wymiany zamk�w(b�dzie mo�na w��czy� niszczenie si�)
 * 3) - Dopisa� sygna�y wysy�ane do otoczenia przy nielegalnym otwieraniu
 * 4) - Przepisa� wy�amywanie na komunikaty.
 * 5) - Doda� to:
              owners = filter(all_inventory(env), &owner_pred(, items[i]->query_owners()));
              owners->signal_rusza_moje_rzeczy(this_player(), items[i]);
 */

#pragma no_clone
#pragma strict_types

#include <pl.h>
#include <exp.h>
#include <macros.h>
#include <cmdparse.h>
#include <formulas.h>
#include <ss_types.h>
#include <stdproperties.h>


/** 8*8*8*8*8*8*8*8*8*8*8*8*8*8*8*8*8*8*8*8*8*8*8*8*8*
 * Tutaj definicje pozwalaj�ce na wy��czenie pewnych dzia�a�
 */
// #define LOCK_CAN_BE_BROKEN      /* czy zamki mog� si� zepsu� */
#define CAN_BE_PICK             /* czy mo�na otwiera� wytrychem */
#define CAN_BE_BROKENDOWN       /* czy mo�na zamek wy�ama� */
#define PICKLOCK_CAN_BE_BROKEN  /* czy wytrych mo�e si� zepsu� */

int         no_pick,            /* ustawiamy je�li nie da si� otworzy� wytrychem */
            no_breakdown,       /* ustawiamy je�li nie da si� wy�ama� */
            pick,               /* Ustawiamy jak ci�ko otworzy� wytrychem */
            breakdown,          /* Ustawiamy jak ci�ko wy�ama� */
            is_open,            /* czy otwarte */
            is_locked,          /* czy zamkni�te kluczem */
            need_str,           /* si�a potrzebna do otwarcia (NIEAKTYWNE) */
            lock_name_tm,       /* czy nazwa zamka wyst�puje tylko w liczbie mnogiej? */
            lock_name_rodz,     /* rodzaj nazwy zamka */
            no_unlock,          /* je�li drzwi nie mo�na odblokowa� */
            no_lock;            /* je�li drzwi nie mo�na zablokowa� */

string      unique_key,         /* klucz zamka */
            lock_desc,          /* Opis zamka */
           *lock_name = ({}),   /* Tablica z nazwami zamka */
            open_command,       /* komenda s�u��ca do otwarcia */
            open_commandb,      /* bezokolicznik komendy otwarcia */
            close_command,      /* komenda zamkni�cia */
            close_commandb,     /* bezokolicznik komendy zamkni�cia */
            lock_command,       /* komenda zablokowania */
            lock_commandb,      /* bezokolicznik komendy zablokowania */
            unlock_command,     /* komenda odblokowania */
            unlock_commandb,    /* bezokolicznik komendy odblokowania */
            pick_command,       /* komenda otwierania wytrychem */
            pick_commandb,      /* bezokolicznik komendy otwierania wytrychem */
            breakdown_command,  /* komenda wywa�ania */
            breakdown_commandb, /* bezokolicznik komendy */
            open_desc,          /* opis otwartego */
            close_desc;         /* opis zamkni�tego */

mixed       open_mess1,         /* komunikat 1 osoby otwierania objektu */
            open_mess2,         /* komunikat 2 osoby otwierania obiektu */
            close_mess1,        /* jw dla zamykania */
            close_mess2,        /* jw dla zamykania */
            lock_mess1,         /* jw dla zamykania kluczem / blokowania */
            lock_mess2,         /* jw dla zamykania kluczem / blokowania*/
            unlock_mess1,       /* jw dla otwierania kluczem / odblokowywania */
            unlock_mess2,       /* jw dla otwierania kluczem / odblokowywania */
            pick_mess1,         /* jw dla otwierania wytrychem */
            pick_mess2,         /* jw dla otwierania wytrychem */
            breakdown_mess1,    /* jw dla wywa�ania */
            breakdown_mess2,    /* jw dla wywa�ania */
            fail_open = ({}),   /* komendy b��du przy otwieraniu */
            fail_close = ({}),  /* jw przy zamykaniu */
            fail_lock = ({}),   /* jw przy zamykaniu kluczem / blokowaniu */
            fail_unlock = ({}), /* jw przy otwieraniu kluczem / odblokowywaniu */
            fail_pick = ({}),   /* jw przy otwieraniu wytrychem */
            fail_breakdown = ({}); /* jw przy wywa�aniu */

object      klucz;              /* obiektu klucza potrzebny do komunikat�w */


/**
 * Ta funkcja MUSI by� wywo�ana w ka�dym otwieralnym obiekcie.
 */
public void
config_open_close()
{
    no_pick = 1;
    no_breakdown = 1;
}

/**
 * Dzi�ki tej funkcji mo�emy ustawi�, �e nie da si� tego zablokowa�!
 */
public void
set_no_lock(int l)
{
    no_lock = !!l;
}

/**
 * @return sprawdzamy czy mo�e by� zablokowane
 */
public int
query_no_lock()
{
    return no_lock;
}

/**
 * Dzi�ki tej funkcji mo�emy ustawi�, �e nie da si� tego odblokowa�!
 */
public void
set_no_unlock(int l)
{
    no_unlock = !!l;
}

/**
 * @return sprawdzamy czy mo�na odblokowa�
 */
public int
query_no_unlock()
{
    return no_unlock;
}

/**
 * Dzi�ki tej funkcji ustawiamy nazwe zamka.
 * Ustawiamy wszystkie przypadki lub korzystamy ze s�ownika.
 *
 * @param nazwy tablica z nazwami
 * @param tylko_mn czy nazwa tylko w liczbie mnogiej
 * @param rodz rodzaj nazwy
 */
public varargs void
set_lock_name(mixed ln, int tylko_mn=0, int rodz=PL_MESKI_NOS_NZYW)
{
    if(!pointerp(ln))
    {
        if(stringp(ln))
        {
            ln = slownik_pobierz(ln);

            if(sizeof(ln) == 2)
            {
                tylko_mn = 1;
                rodz = ln[1];
            }
            else
                rodz = ln[2];

            ln = ln[0];
        }
    }
    else if(sizeof(ln) != 6)
        return;

    lock_name = ln;
    lock_name_tm = tylko_mn;
    lock_name_rodz = rodz;
}

/**
 * @return Nazw� zamku w odpowiednim przypadku.
 */
public varargs string
query_lock_name(int przyp=0)
{
    if(pointerp(lock_name) && sizeof(lock_name) == 6)
        return lock_name[przyp];
}

/**
 * Ustawiamy opis zamka.
 */
public void
set_lock_desc(string str)
{
    lock_desc = str;
}

/**
 * @return Zwracamy opis zamka.
 */
public string
query_lock_desc()
{
    return lock_desc;
}

/**
 * Dzi�ki tej funkcji mo�emy ustawi� czy obiekt jest w tym momencie zamkni�ty czy otwarty
 * @param op 1 je�li otwarte 0 jesli zamnki�te
 */
public void
set_open(int op)
{
    is_open = op;
}

/**
 * Dzi�ki tej funkcji mo�emy sprawdzi� czy objekt jest zamkni�te czy otwarte
 * @return 1 otwarte
 * @return 0 zamkni�te
 */
public int
query_open()
{
    return (!is_locked && is_open);
}

/**
 * Dzi�ki tej funkcji mo�emy ustawi� czy obiekt jest zamkni�ty na klucz czy nie
 * @param lo 1 zamkni�ty, 0 nie
 */
public void
set_locked(int lo)
{
    is_locked = lo;
}

/**
 * Funkcja ta pozwala nam sprawdzi� czy otwierane s� zamkni�te na klucz czy nie.
 * @return 1 zamkni�te na klucz
 * @return 0 nie zamkniete na klucz
 */
public int
query_locked()
{
    return is_locked;
}

/**
 * Ta funkcja pozwala ustawi� unikalny klucz obiektu, dzi�ki czemu tylko jednym
 * rodzajem klucza b�dzie mo�na je otworzy�.
 * @param k klucz
 */
public void
set_key(string k)
{
    unique_key = k;
}

/**
 * @return Kod unikalnego klucza otwierane
 */
public string
query_key()
{
    return unique_key;
}

/**
 * Je�li to ustawimy to nie b�dzie mo�liwo�ci
 * otwarcia tych otwierane wytrychem.
 */
public void
set_no_pick(int i)
{
    no_pick = !!i;
}

/**
 * @return czy mo�na otworzy� wytrychem
 */
public int
query_no_pick()
{
    return no_pick;
}

/**
 * Ustawiamy jak trudno b�dzie zamek otworzy� wytrychem.
 * @param t truno�� otwarcia zamka wytrychem (1-100)
 */
public void
set_pick(int t)
{
    pick = max(1, min(100, t));
}

/**
 * @return jak trudno otworzy� wytrychem
 */
public int
query_pick()
{
    return pick;
}

/**
 * Ustawia czy da si� wy�ama�.
 */
public void
set_no_breakdown(int i)
{
    no_breakdown = !!i;
}

/**
 * @return czy da si� wy�ama�.
 */
public int
query_no_breakdown()
{
    return no_breakdown;
}

/**
 * Ustawiamy jak ci�ko b�dzie nam wy�ama� otwierane.
 * @param t trundo�� wy�amywania otwierane (1-100).
 */
public void
set_breakdown(int t)
{
    breakdown = max(1, min(100, t));
}

/**
 * Ustawiamy si�� potrzebn� do otwarcia/zamkni�cia.
 * @param s potrzebna si�a (1-150)
 */
public void
set_str(int s)
{
    need_str = max(1, min(150, s));
}

/**
 * @return Si�a potrzebna do otwarcia/zamkni�cia.
 */
public int
query_str()
{
    return need_str;
}

/* Zwykle u�ywamy standardowego soula items, ale nie zawsze stosujemy komendy standardowe
 * dlatego zostawi�em tak� furtk�, dla komend niestandardowych.
 * W takich przypadkach standardowe s� wy��czane.
 */

/**
 * Ustawiamy komende otwierania.
 * UWAGA:
 * Nie wywo�ujcie tej funkcji je�li ma to by� standardowe otw�rz.
 * To jest zdefiniowane w items.c
 * @param c komenda
 * @param b bezokolicznik komendy
 */
public void
set_open_command(string c, string b)
{
    open_command = c;
    open_commandb = b;
}

/**
 * @return komende otwierania.
 */
public string
query_open_command(string c)
{
    return open_command;
}

/**
 * @return bezokolicznik komendy otwierania.
 */
public string
query_open_command_bezokolicznik()
{
    return open_commandb;
}

/**
 * Ustawiamy komende zamykania
 * UWAGA:
 * Nie wywo�ujcie tej funkcji je�li ma to by� standardowe zamknij.
 * To jest zdefiniowane w items.c
 * @param c komenda
 * @param b bezokolicznik komendy
 */
public void
set_close_command(string c, string b)
{
    close_command = c;
    close_commandb = b;
}

/**
 * @return komende zamykania.
 */
public string
query_close_command()
{
    return close_command;
}

/**
 * @return bezokolicznik komendy zamykania
 */
public string
query_close_command_bezokolicznik()
{
    return close_commandb;
}

/**
 * Ustawiamy komende otwierania kluczem.
 * UWAGA:
 * Nie wywo�ujcie tej funkcji je�li ma to by� standardowe otw�rz 'co' 'czym'.
 * To jest zdefiniowane w items.c
 * @param c komenda
 * @param b bezokolicznik komendy
 */
public void
set_unlock_command(string c, string b)
{
    unlock_command = c;
    unlock_commandb = b;
}

/**
 * @return komende otwierania kluczem / odblokowywania.
 */
public string
query_unlock_command()
{
    return unlock_command;
}

/**
 * @return bezokolicznik komendy otwierania kluczem / odblokowywania
 */
public string
query_unlock_command_bezokolicznik()
{
    return unlock_commandb;
}

/**
 * Ustawiamy komende zamykania kluczem
 * UWAGA:
 * Nie wywo�ujcie tej funkcji je�li ma to by� standardowe zamknij 'co' kluczem.
 * To jest zdefiniowane w items.c
 * @param c komenda
 * @param b bezokolicznik komendy
 */
public void
set_lock_command(string c, string b)
{
    lock_command = c;
    lock_commandb = b;
}

/**
 * @return komende zamykania kluczem / blokowania.
 */
public string
query_lock_command()
{
    return lock_command;
}

/**
 * @return bezokolicznik komendy zamykania kluczem / blokowania
 */
public string
query_lock_command_bezokolicznik()
{
    return lock_commandb;
}

/**
 * Ustawiamy komende otwierania wytrychem
 * UWAGA:
 * Nie wywo�ujcie tej funkcji je�li ma to by� standardowe otw�rz 'co' wytrychem.
 * To jest zdefiniowane w items.c
 * @param c komenda
 * @param b bezokolicznik komendy
 */
public void
set_pick_command(string c, string b)
{
    pick_command = c;
    pick_commandb = b;
}

/**
 * @return komende otwierania wytrychem
 */
public string
query_pick_command()
{
    return pick_command;
}

/**
 * @return bezokolicznik komendy otwierania wytrychem.
 */
public string
query_pick_command_bezokolicznik()
{
    return pick_commandb;
}

/**
 * Ustawiamy komende wywa�ania.
 * UWAGA:
 * Nie wywo�ujcie tej funkcji je�li ma to by� standardowe wywa�ania.
 * To jest zdefiniowane w items.c
 * @param c komenda
 * @param b bezokolicznik komendy
 */
public void
set_breakdown_command(string c, string b)
{
    breakdown_command = c;
    breakdown_commandb = b;
}

/**
 * @return komende wy�amywania
 */
public string
query_breakdown_command()
{
    return breakdown_command;
}

/**
 * @return bezokolicznik komnedy otwierania wytrychem
 */
public string
query_breakdown_command_bezokolicznik()
{
    return breakdown_commandb;
}

/**
 * Ustawiamy komunikaty podczas otwierania otwierane
 * @param s1 - komunikat dla innych
 * @param s2 - komunikat dla gracza
 */
public varargs void
set_open_mess(mixed s1, mixed s2)
{
    open_mess1 = s1;
    open_mess2 = s2;
}

/**
 * @return ({komunikat dla innych, komunikat dla gracza})
 */
public mixed
query_open_mess()
{
    return ({open_mess1, open_mess2});
}

/**
 * Ustawiamy komunikaty zamykania otwierane.
 * @param s1 - komunikat dla innych
 * @param s2 - komunikat dla zamykaj�cego
 */
public varargs void
set_close_mess(mixed s1, mixed s2)
{
    close_mess1 = s1;
    close_mess2 = s2;
}

/**
 * @return komunikaty przy zamykaniu otwierane
 * @return ({komunikat dla innych, komunikat dla zamykaj�cego})
 */
public mixed
query_close_mess()
{
    return ({close_mess1, close_mess2});
}

/**
 * Ustawiamy komunikat otwierania otwierane kluczem/odblokowywania
 * @param s1 - komunikat dla innych
 * @param s2 - komunikat dla otwieraj�cego/odblokowuj�cego
 */
public varargs void
set_unlock_mess(mixed s1, mixed s2)
{
    unlock_mess1 = s1;
    unlock_mess2 = s2;
}

/**
 * @return komunikaty otwierania otwierane kluczem/odblokowywania
 * @return ({komunikat dla innych, komynikat dla otwieraj�cego/odblokowuj�cego})
 */
public mixed
query_unlock_mess()
{
    return ({unlock_mess1, unlock_mess2});
}

/**
 * Ustawiamy komunikat zamykania kluczem / blokowania.
 * @return s1 - komunikat dla innych
 * @return s2 - komunikat dla blokuj�cego/zamykaj�cego kluczem.
 */
public varargs void
set_lock_mess(mixed s1, mixed s2)
{
    lock_mess1 = s1;
    lock_mess2 = s2;
}

/**
 * @return komunikaty zamykania kluczem/blokowania
 * @return ({komunikat dla innych, komunikat dla blokuj�cego/zamykajacego kluczem})
 */
public mixed
query_lock_mess()
{
    return ({lock_mess1, lock_mess2});
}

/**
 * Ustawiamy komunikat otwierania wytrychem.
 * @param s1 - komunikat dla innych
 * @param s2 - komunikat otwierania wytrychem
 */
public varargs void
set_pick_mess(mixed s1, mixed s2)
{
    pick_mess1 = s1;
    pick_mess2 = s2;
}

/**
 * @return Komunikaty otwierania wytrychem
 * @return ({komunikat dla innych, komunikat dla otwieraj�cego})
 */
public mixed
query_pick_mess()
{
    return ({pick_mess1, pick_mess2});
}

/**
 * Ustawiamy komunikaty przy udanym wy�amaniu.
 * @param s1 - komunikat dla inny
 * @param s2 - komunikat dla wy�amuj�cego.
 */
public varargs void
set_breakdown_mess(mixed s1, mixed s2)
{
    breakdown_mess1 = s1;
    breakdown_mess2 = s2;
}

/**
 * @return Komunikaty przy udanym wy�amaniu
 * @return ({komunikat dla innych, komunikat dla wy�amuj�cego})
 */
public mixed
query_breakdown_mess()
{
    return ({breakdown_mess1, breakdown_mess2});
}

public varargs void
set_fail_open(...)
{
    fail_open = argv;
}

public mixed
query_fail_open()
{
    return fail_open;
}

public varargs void
set_fail_close(...)
{
    fail_close = argv;
}

public mixed
query_fail_close()
{
    return fail_close;
}

public varargs void
set_fail_unlock(...)
{
    fail_unlock = argv;
}

public mixed
query_fail_unlock()
{
    return fail_unlock;
}

public varargs void
set_fail_lock(...)
{
    fail_lock = argv;
}

public mixed
query_fail_lock()
{
    return fail_lock;
}

public varargs void
set_fail_pick(...)
{
    fail_pick = argv;
}

public mixed
query_fail_pick()
{
    return fail_pick;
}

public varargs void
set_fail_breakdown(...)
{
    fail_breakdown = argv;
}

public void
query_fail_breakdown()
{
    return fail_breakdown;
}

/**
 * Ustawiamy opis otwartych otwierane.
 */
public void
set_open_desc(mixed s)
{
    open_desc = s;
}

/**
 * @return zwracamy opis otwartych otwierane.
 */
public string
query_open_desc()
{
    return open_desc;
}

/**
 * Ustawiamy opis zamkni�tych otwierane.
 */
public void
set_closed_desc(mixed s)
{
    close_desc = s;
}

/**
 * @return zwracamy opis zamkni�tych otwierane.
 */
public string
query_close_desc()
{
    return close_desc;
}

/* A teraz opiszemy te akcje co�my je wy�ej zakodowali, ale na pierwszy ogie� p�jdzie pomoc:) */

/**
 * Wy�wietla pomoc.
 */
public int
pomoc(string str)
{
    if(!str)
        return 0;

    object *ob;
    if(!parse_command(str, all_inventory(ENV(TP)) + all_inventory(TP), "%i:" + PL_MIA, ob))
        return 0;

    ob = NORMAL_ACCESS(ob, 0, 0);

    if(ob[0] != TO)
        return 0;

    write("T" + TO->koncowka("ego", "�", "o", "ych", "e") + " " +
        TO->short(TP, PL_MIA) + " mo�esz otworzy�, zamkn�� oraz " +
        "zamkn�� i otworzy� kluczem"+ (!no_breakdown ? ", spr�bowa� wy�ama� " : "") +
        (!no_pick ? "czy te� spr�bowa� otworzy� je wytrychem" : "") + ".\n");

    return 1;
}

public string
przerob_na_patt(string *str)
{
    string ret = "";

    for(int i=1;i<sizeof(str);i++)
    {
        if(i!=1)
            ret += " ";

        if(str[i][0] != '%' && str[i][0] != '/' && str[i][0] != '[' && str[i][0] != '\'')
            ret += "'"+str[i]+"'";
        else
            ret += str[i];
    }
    return ret;
}

/**
 * Otwieramy.
 */
public int
open_it(string str)
{
    NF("Co chcesz " + open_commandb + "?\n");

    if(!str)
        return 0;

    object *otwierane;
    mixed tmp = explode(open_command + " ", " ");
    int size = sizeof(tmp);
    tmp = przerob_na_patt(tmp);

    if(!parse_command(str, all_inventory(TP) + all_inventory(ENV(TP)),
        (size>1 ? tmp : "%i:" + PL_BIE), otwierane))
    {
        return 0;
    }

    otwierane = NORMAL_ACCESS(otwierane, 0, 0);

    if(!otwierane)
        return 0;
    if(sizeof(otwierane) > 1)
        return NF("Jak chcesz to zrobi� z kilkoma przedmiotami jednocze�nie?\n");

    if(otwierane[0] != TO)
        return 0;

    mixed ret = TO->open_me();

    if(stringp(ret))
        return NF(ret);
    else
        return ret;
}

/**
 * Zamykamy.
 */
public int
close_it(string str)
{
    NF("Co chcesz " + close_commandb + "?\n");

    if(!str)
        return 0;

    object *otwierane, *klucz;
    mixed tmp = explode(close_command + " ", " ");
    int size = sizeof(tmp);
    tmp = przerob_na_patt(tmp);

    if(!parse_command(str, all_inventory(TP) + all_inventory(ENV(TP)),
        (size > 1 ? tmp : "%i:" + PL_BIE), otwierane))
        return 0;

    otwierane = NORMAL_ACCESS(otwierane, 0, 0);

    if(!otwierane)
        return 0;
    if(sizeof(otwierane) > 1)
        return NF("Jak chcesz to zrobi� z kilkoma przedmiotami jednocze�nie?\n");

    if(sizeof(klucz) > 1)
        return NF("Nie dasz rady " + close_commandb + " tego klikoma kluczami jednocze�nie.\n");

    if(otwierane[0] != TO)
        return 0;

    mixed ret = TO->close_me();

    if(stringp(ret))
        return NF(ret);
    else
        return ret;
}

/**
 * Blokujemy
 */
public int
lock_it(string str)
{
    NF2("Co chcesz " + lock_commandb + "?\n", 3);

    if(!str)
        return 0;

    int is_key = 0;
    mixed tmp = explode(lock_command + " ", " ");
    int size = sizeof(tmp);
    tmp = przerob_na_patt(tmp);

    object *otwierane, *key;
    if(!parse_command(str, all_inventory(ENV(TP)), (size > 1 ? tmp : "%i:" + PL_BIE), otwierane))
    {
        if(!parse_command(str, all_inventory(ENV(TP)), (size > 1 ? tmp : "%i:" + PL_BIE + " %i:" + PL_NAR), otwierane, key))
            return 0;

        key = NORMAL_ACCESS(key, 0, 0);
        is_key = 1;
    }

    otwierane = NORMAL_ACCESS(otwierane, 0, 0);

    if(!otwierane)
        return 0;
    if(sizeof(otwierane) > 1)
        return NF2("Jak chcesz to zrobi� z kilkoma przedmiotami jednocze�nie?\n", 3);

    if(otwierane[0] != TO)
        return 0;

    if(TO->query_locked())
    {
        return NF2("Ale� " + TO->short(TP, PL_MIA) + " " +
            TO->koncowka("jest ju� zablokowany", "jest ju� zablokowana", "jest ju� zablokowane",
            "s� ju� zablokowani", "s� ju� zablokowane") + ".\n", 3);
    }

    if(is_key && (!key || !sizeof(key)))
        return NF2("Czym chcesz to " + lock_commandb + "?\n", 3);

    if(is_key && sizeof(key) > 1)
        return NF2("Nie mo�esz " + lock_commandb + " " + otwierane[0]->short(TP, PL_DOP) + " kilkoma kluczami jednocze�nie.\n", 3);

    mixed ret = TO->lock_me((is_key ? key : 0));

    if(stringp(ret))
        return NF(ret);
    else
        return ret;
}

/**
 * Odblokowywujemy
 */
public int
unlock_it(string str)
{
    NF2("Co chcesz " + unlock_commandb + "?\n", 3);

    if(!str)
        return 0;

    int is_key=0;
    mixed tmp = explode(unlock_command + " ", " ");
    int size = sizeof(tmp);
    tmp = przerob_na_patt(tmp);

    object *otwierane, *key;
    if(!parse_command(str, all_inventory(ENV(TP)),
        (size > 1 ? tmp : "%i:" + PL_BIE), otwierane))
    {
        if(!parse_command(str, all_inventory(ENV(TP)),
            (size > 1 ? tmp : "%i:" + PL_BIE + " %i:" + PL_NAR), otwierane, key))
        {
            return 0;
        }
        key = NORMAL_ACCESS(key, 0, 0);
        is_key = 1;
    }

    otwierane = NORMAL_ACCESS(otwierane, 0, 0);

    if(!otwierane || !sizeof(otwierane))
        return 0;

    if(sizeof(otwierane) > 1)
        return NF2("Jak chcesz to zrobi� z kilkoma przedmiotami jednocze�nie?\n", 3);

    if(otwierane[0] != TO)
        return 0;

    if(!TO->query_locked())
    {
        return NF2("Ale� " + TO->short(TP, PL_MIA) + " nie " +
            TO->koncowka("jest ju� zablokowany", "jest ju� zablokowana",
            "jest ju� zablokowane", "s� ju� zablokowani", "s� ju� zablokowane") + ".\n", 3);
    }

    if(is_key && (!key || !sizeof(key)))
        return NF2("Czym chcesz to " + unlock_commandb + "?\n", 3);

    if(is_key && sizeof(key) > 1)
        return NF2("Nie mo�esz " + unlock_commandb + " " + otwierane[0]->short(TP, PL_DOP) + " kilkoma kluczami jednocze�nie.\n", 3);

    mixed ret = TO->unlock_me((is_key) ? key[0] : 0);

    if(stringp(ret))
        return NF(ret);
    else
        return ret;
}

/**
 * Wy�amujemy.
 */
public int
breakdown_it(string str)
{
    NF("Co chcesz " + breakdown_commandb + "?\n");

    if(!str)
        return 0;

    mixed tmp = explode(close_command + " ", " ");
    int size = sizeof(tmp);
    int is_lom = 0;
    tmp = przerob_na_patt(tmp);

    object *otwierane, *lom;
    if(!parse_command(str, all_inventory(ENV(TP)),
        (size > 1 ? tmp : "%i:" + PL_BIE), otwierane))
    {
        if(!parse_command(str, all_inventory(ENV(TP)),
            (size > 1 ? tmp : "%i:" + PL_BIE + " %i:" + PL_NAR), otwierane, lom))
        {
            return 0;
        }
        lom = NORMAL_ACCESS(lom, 0, 0);
        is_lom = 1;
    }

    otwierane = NORMAL_ACCESS(otwierane, 0, 0);

    if(!sizeof(otwierane))
        return 0;

    if(sizeof(otwierane) > 1)
        return NF("Nie mo�esz  "+ breakdown_commandb +  " kilku rzeczy jednocze�nie.\n");

    if(otwierane[0] != TO)
        return 0;

    if(is_lom && !sizeof(lom))
        return NF("Czym chcesz to " + breakdown_commandb + "?\n");

    if(is_lom && sizeof(lom) > 1)
        return NF("Jak chcesz to zrobi� " + COMPOSITE_DEAD(lom, PL_NAR) + "?\n");

    if(is_lom && lom[0]->query_wielded())
        return NF("Najpierw musisz chwyci� " + lom[0]->short(TP, PL_BIE) + ".\n");

    mixed ret = TO->breakdown_me(lom[0]);

    if(stringp(ret))
        return NF(ret);
    else
        return ret;
}

/**
 * Otwieramy wytrychem
 */
public int
pick_it(string str)
{
    NF("Co chcesz " + pick_commandb + "?\n");

    if(!str)
        return 0;

    mixed tmp = explode(pick_command + " ", " ");
    int size = sizeof(tmp);
    object *otwierane, *wytrych;
    tmp = przerob_na_patt(tmp);

    if(!parse_command(str, all_inventory(ENV(TP)),
        (size > 1 ? tmp : "%i:" + PL_BIE + " %i:" + PL_NAR), otwierane, wytrych))
    {
        return 0;
    }

    otwierane = NORMAL_ACCESS(otwierane, 0, 0);
    wytrych = NORMAL_ACCESS(wytrych, 0, 0);

    if(!sizeof(otwierane))
        return 0;

    if(sizeof(otwierane) > 1)
        return NF("Nie mo�esz  "+ pick_commandb +  " kilku rzeczy jednocze�nie.\n");

    if(!sizeof(wytrych))
        return NF("Czym chcesz to " +  pick_commandb + "?\n");

    if(sizeof(wytrych) > 1)
        return NF("Jak chcesz to zrobi� " + COMPOSITE_DEAD(wytrych, PL_NAR) + "?\n");

    if(wytrych[0]->query_wielded())
        return NF("Najpierw musisz doby� " + wytrych[0]->short(TP, PL_BIE) + ".\n");

    mixed ret = TO->pick_me(wytrych[0]);

    if(stringp(ret))
        return NF(ret);
    else
        return ret;
}

void
init_open_close()
{
    if(open_command)
        add_action(open_it, explode(open_command + " ", " ")[0]);

    if(close_command)
        add_action(close_it, explode(close_command + " ", " ")[0]);

    if(close_command != lock_command && lock_command)
        add_action(lock_it, explode(lock_command + " ", " ")[0]);

    if(open_command != unlock_command && unlock_command)
        add_action(unlock_it, explode(unlock_command + " ", " ")[0]);

    if(pick_command != open_command && pick_command != unlock_command && pick_command)
        add_action(pick_it, explode(pick_command + " ", " ")[0]);

    if(breakdown_command != open_command && breakdown_command != unlock_command && breakdown_command)
        add_action(breakdown_it, explode(breakdown_command + " ", " ")[0]);

    add_action(pomoc, "?", 2);
    add_action(pomoc, "pomoc");
}

/**
 * Funkcja otwieraj�ca obiekt.
 */
public int
do_open()
{
    set_open(1);
    set_locked(0);
}

/**
 * Funkcja zamykaj�ca obiekt
 */
public int
do_close()
{
    set_open(0);
    set_locked(0);
}

/**
 * Funkcja blokuj�ca otwieranie obiektu.
 */
public int
do_lock()
{
    set_open(0);
    set_locked(1);
}

/**
 * Funkcja odblokowuj�ca otwieranie obiektu.
 */
public int
do_unlock()
{
    set_open(0);
    set_locked(0);
}


/**
 * Funkcja odpowiadaj�ca za wy�amanie otwierane
 */
public int
do_breakdown()
{
#ifdef CAN_BE_BROKENDOWN
    set_open(1);
    set_locked(0);
#endif CAN_BE_BROKENDOWN
}

/**
 * A t� funkcj� otwieramy wytrychem.
 */
public int
do_pick()
{
#ifdef CAN_BE_PICK
    set_open(0);
    set_locked(0);
#endif CAN_BE_PICK
}

public varargs mixed open_me(int cicho = 0);
public varargs mixed close_me(int cicho = 0);
public varargs mixed lock_me(object key, int cicho = 0);
public varargs mixed unlock_me(object key, int cicho = 0);
public varargs mixed pick_me(object key, int cicho = 0);
public varargs mixed breakdown_me();

//A teraz robimy podstawowe akcje
/**
 * Funkcja obs�uguj�ca otwarcie.
 */
public varargs mixed
open_me(int cicho = 0)
{
    //Je�li zamkni�te na klucz i nie ma set_keya to wywo�ujemy unlock_me
    if(TO->query_locked() && !TO->query_key() && !unlock_command && !TO->query_no_unlock())
        return unlock_me(0, cicho);

    if(query_open())
    {
        if(sizeof(fail_open) && fail_open[0])
        {
            if(functionp(fail_open[0]))
            {
                function f = fail_open[0];
                return f();
            }
            else
                return fail_open[0];
        }
        else
        {
            return "Przecie� " + TO->short(PL_MIA) + " " +
                (TO->query_tylko_mn() ? "s�" : "jest") + " ju� otwar" +
                TO->koncowka("ty", "ta", "te", "ci", "te") + ".\n";
        }
    }

    if(query_locked())
    {
        if(sizeof(fail_open) > 1 && fail_open[1])
        {
            if(functionp(fail_open[1]))
            {
                function f = fail_open[1];
                return f();
            }
            else
                return fail_open[1];
        }
    }

    if((query_locked() && sizeof(fail_open) < 2) || query_str() > TP->query_stat(SS_STR))
    {
        return UC(TO->short(PL_MIA)) + " nie chc" +
            (TO->query_tylko_mn() ? "�" : "e") + " si� ruszy�.\n";
    }

    do_open();

    if(!cicho)
    {
        if(open_mess1)
        {
            if(functionp(open_mess1))
            {
                function f = open_mess1;
                saybb(QCIMIE(TP, PL_MIA) + " " + f());
            }
            else
                saybb(QCIMIE(TP, PL_MIA) + " " + open_mess1);
        }
        else
            saybb(QCIMIE(TP, PL_MIA) + " otwiera " + QSHORT(TO,PL_BIE) + ".\n");

        if(open_mess2)
        {
            if(functionp(open_mess2))
            {
                function f = open_mess2;
                write(f());
            }
            else
                write(open_mess2);
        }
        else
            write("Otwierasz " + TO->query_short(PL_BIE) + ".\n");
    }

    TO->open_now();

    return 1;
}

/**
 * Funkcja ob�uguj�ca zamkni�cie.
 */
public varargs mixed
close_me(int cicho=0)
{
    //Je�li zamkni�ty i nie ma set_key'a to wywo�ujemy lock_me
    if(TO->query_locked() && !TO->query_key() && !lock_command && !TO->query_no_lock())
        return lock_me(0, cicho);

    if(!query_open())
    {
        if(sizeof(fail_close) && fail_close[0])
        {
            if(functionp(fail_close[0]))
            {
                function f = fail_close[0];
                return f();
            }
            else
                return fail_close[0];
        }
        else
        {
            return "Przecie� " + TO->short(PL_MIA) + " " +
                (TO->query_tylko_mn() ? "s�" : "jest") + " " +
                "ju� zamkni�" + TO->koncowka("ty", "ta", "te", "ci", "te") + ".\n";
        }
    }

    if(query_str() > TP->query_stat(SS_STR))
    {
        return UC(TO->short(PL_MIA)) + " nie chc" +
                (TO->query_tylko_mn() ? "�" : "e") + " si� ruszy�.\n";
    }

    do_close();

    if(!cicho)
    {
        if(close_mess2)
        {
            if(functionp(close_mess2))
            {
                function f = close_mess2;
                write(f());
            }
            else
                write(close_mess2);
        }
        else
            write("Zamykasz " + TO->query_short(PL_BIE) + ".\n");

        if(close_mess1)
        {
            if(functionp(close_mess1))
            {
                function f = close_mess1;
                saybb(QCIMIE(TP, PL_MIA) + " " + f());
            }
            else
                saybb(QCIMIE(TP, PL_MIA) + " " + close_mess1);
        }
        else
            saybb(QCIMIE(TP, PL_MIA) + " zamyka " + QSHORT(TO,PL_BIE) + ".\n");
    }

    TO->close_now();

    return 1;
}

/**
 * Funkcja obs�uguj�ca zamykanie kluczem / blokowanie.
 */
public varargs mixed
lock_me(object key, int cicho=0)
{
    if(query_no_lock())
        return 0;

    if(key && key->query_key() != TO->query_key())
        return UC(key->short(PL_MIA)) + " nie pasuje.\n";

    if(TO->query_key() && !key)
        return "Nie masz czym zamkn�� " + query_lock_name(PL_DOP) + " " + TO->short(PL_DOP);

#ifdef LOCK_CAN_BE_BROKEN
    if(TO->query_prop(OC_I_BROKEN_LOCK))
    {
        return UC(TO->query_lock_name(PL_MIA)) + " " + TO->short(PL_NAR) +
                " jest zepsu" + koncowka_zamka("ty", "ta", "te", "ci", "te") + ".\n";
    }
#endif

    if(query_locked())
    {
        if(sizeof(fail_lock) > 1 && fail_lock[1])
        {
            if(functionp(fail_lock[1]))
            {
                function f = fail_lock[1];
                return f();
            }
            else
                return fail_lock[1];
        }
        else
        {
            if(!cicho && key)
            {
                write("Wk�adasz " + key->short(PL_BIE) + " do " + TO->short(PL_DOP) + " i pr�bujesz przekr�ci� "+
                    "jednak ten nie chce ani dgrn��.\n");
                saybb(QCIMIE(TP, PL_MIA) + " wk�ada " + QSHORT(key, PL_BIE) + " do " + QSHORT(TO, PL_DOP) + " " +
                    "i pr�bujesz przekr�ci� jednak ten nie chce ani drgn��.\n");
            }
            else if(!cicho)
            {
                write(UC(TO->short(PL_MIA)) + " wygl�d" + TO->koncowka("a", "a", "a", "j�", "j�") + " na ju� " +
                    "zamkni�te.\n");
            }
            return 2;
        }
    }

    if(query_open())
    {
        if(sizeof(fail_lock) && fail_lock[0])
        {
            if(functionp(fail_lock[0]))
            {
                function f = fail_lock[0];
                return f();
            }
            else
                return fail_lock[0];
        }
        else
            return "Najpierw zamknij "+ TO->short(PL_BIE) + ".\n";
    }

    klucz = key;
    do_lock();

    if(!cicho)
    {
        if(lock_mess2)
        {
            if(functionp(lock_mess2))
            {
                function f = lock_mess2;
                write(f());
            }
            else
                write(lock_mess2);
        }
        else if(key)
            write("Zamykasz " + TO->short(PL_BIE) + " " + key->short(PL_NAR) + ".\n");
        else
            write("Zamykasz " + TO->short(PL_BIE) + ".\n");

        if(lock_mess1)
        {
            if(functionp(open_mess1))
            {
                function f = lock_mess1;
                saybb(QCIMIE(TP, PL_MIA) + " " + f());
            }
            else
                saybb(QCIMIE(TP, PL_MIA) + " " + lock_mess1);
        }
        else if(key)
            saybb(QCIMIE(TP, PL_MIA) + " zamyka " + QSHORT(TO, PL_BIE) + " " + QSHORT(key, PL_NAR) + ".\n");
        else
            saybb(QCIMIE(TP, PL_MIA) + " zamyka " + QSHORT(TO, PL_BIE) + ".\n");
    }

    TO->lock_now();

    return 1;
}

/**
 * Funkcja obs�uguj�ca otwieranie kluczem / odblokowywanie.
 */
public varargs mixed
unlock_me(object key, int cicho = 0)
{
    if(query_no_unlock())
        return 0;

    //Otwieranie wytruchem:)
    if(!key->query_key() && key->query_wytrych())
        return pick_me(key, cicho);

    if(key && key->query_key() != TO->query_key())
        return UC(key->short(PL_MIA)) + " nie pasuje.\n";

    if(TO->query_key() && !key)
        return "Nie masz czym otworzy� " + query_lock_name(PL_DOP) + " " + TO->short(PL_DOP);

#ifdef LOCK_CAN_BE_BROKEN
    if(TO->query_prop(OC_I_BROKEN_LOCK))
    {
        return UC(TO->query_lock_name(PL_MIA)) + " " + TO->short(PL_NAR) +
            " jest zepsu" + koncowka_zamka("ty", "ta", "te", "ci", "te") + ".\n";
    }
#endif LOCK_CAN_BE_BROKEN

    if(!query_locked())
    {
        if(sizeof(fail_unlock) && fail_unlock[0])
        {
            if(functionp(fail_unlock[0]))
            {
                function f = fail_unlock[0];
                return f();
            }
            else
                return fail_unlock[0];
        }
        else
        {
            if(!cicho && key)
            {
                write("Wk�adasz " + key->short(PL_BIE) + " do " + TO->short(PL_DOP) + " i pr�bujesz przekr�ci� "+
                        "jednak " + key->short(PL_MIA) + " nie chce ani dgrn��.\n");
                saybb(QCIMIE(TP, PL_MIA) + " wk�ada " + QSHORT(key, PL_BIE) + " do " + QSHORT(TO, PL_DOP) + " " +
                        "i pr�bujesz przekr�ci� jednak " + QSHORT(key, PL_MIA) + " nie chce ani drgn��.\n");
            }
            else if(cicho)
            {
                write(UC(TO->short(PL_MIA)) + " wygl�d" + TO->koncowka("a", "a", "a", "j�", "j�") + " na ju� " +
                        "zamkni�" + TO->koncowka("ty", "ta", "te", "ci", "te") + ".\n");
            }
            return 2;
        }
    }

    do_unlock();

    if(!cicho)
    {
        if(unlock_mess2)
        {
            if(functionp(unlock_mess2))
            {
                function f = unlock_mess2;
                write(f());
            }
            else
                write(unlock_mess2);
        }
        else if(key)
            write("Otwierasz " + TO->short(PL_BIE) + " " + key->short(PL_NAR) + ".\n");
        else
            write("Otwierasz " + TO->short(PL_BIE) + ".\n");

        if(unlock_mess1)
        {
            if(functionp(unlock_mess1))
            {
                function f = unlock_mess1;
                saybb(QCIMIE(TP, PL_MIA) + " " + f());
            }
            else
                saybb(QCIMIE(TP, PL_MIA) + " " + unlock_mess1);
        }
        else if(key)
            saybb(QCIMIE(TP, PL_MIA) + " otwiera " + QSHORT(TO, PL_BIE) + " " + QSHORT(key, PL_NAR) + ".\n");
        else
            saybb(QCIMIE(TP, PL_MIA) + " otwiera " + QSHORT(TO, PL_BIE) + ".\n");
    }

    TO->unlock_now();
    return 1;
}

/*
 * Funkcje pomocnicze
 */

#ifdef CAN_BE_BROKENDOWN
/**
 * @return czy gracz da rade, czy nie da rady wy�ama�.
 */
public int
player_can_breakdown_this(object p, object l)
{
    if(query_no_breakdown())
        return 0;

    int fat;

    fat = F_OC_BREAKDOWN_FATIGUE;
    if(p->query_old_fatigue() - (2 * fat) <= 0)
    {
        NF("Jeste� na to zbyt zm�czony.\n");
        return 0;
    }
    p->add_old_fatigue(-fat);

    fat = F_OC_BREAKDOWN_MANA;
    if(p->query_mana() - (2 * fat) <= 0)
    {
        NF("Jeste� na to zbyt wyczerpany mentalnie.\n");
        return 0;
    }
    p->add_mana(-fat);

    if(F_OC_PLAYER_CAN_BREAKDOWN(p->query_stat(SS_STR), p->query_stat(SS_DEX),
        p->query_skill(SS_BREAKDOWN), l->query_jakosc(), TO->query_pick()))
    {
        p->increase_ss(SS_BREAKDOWN,    EXP_WYLAMYWANIE_UDANE_BREAKDOWN);
        p->incresse_ss(SS_STR,          EXP_WYLAMYWANIE_UDANE_STR);
        p->increase_ss(SS_INT,          EXP_WYLAMYWANIE_UDANE_INT);
        p->increase_ss(SS_DEX,          EXP_WYLAMYWANIE_UDANE_DEX);
        return 1;
    }

    p->increase_ss(SS_BREAKDOWN,    EXP_WYLAMYWANIE_NIEUDANE_BREAKDOWN);
    p->incresse_ss(SS_STR,          EXP_WYLAMYWANIE_NIEUDANE_STR);
    p->increase_ss(SS_INT,          EXP_WYLAMYWANIE_NIEUDANE_INT);
    p->increase_ss(SS_DEX,          EXP_WYLAMYWANIE_NIEUDANE_DEX);
}
#endif CAN_BE_BROKENDOWN

#ifdef CAN_BE_PICK
/**
 * @return czy gracz da rade czy nie da rady otworzy� wytrychem.
 */
public int
player_can_pick_this(object p, object w)
{
    if(query_no_pick() || query_no_unlock())
        return 0;

    int fat;

    fat = F_OC_PICK_LOCK_FATIGUE;
    if(p->query_old_fatigue() - (2 * fat) <= 0)
    {
        NF("Jeste� na to zbyt zm�czony.\n");
        return 0;
    }
    p->add_old_fatigue(-fat);

    fat = F_OC_PICK_LOCK_MANA;
    if(p->query_mana() - (2 * fat) <= 0)
    {
        NF("Jeste� na to zbyt wyczerpany mentalnie.\n");
        return 0;
    }
    p->add_mana(-fat);

    if(F_OC_PLAYER_CAN_PICK(p->query_stat(SS_DEX), p->query_skill(SS_OPEN_LOCK),
        w->query_wytrych(), TO->query_pick()))
    {
        p->increase_ss(SS_PICK_LOCK,    EXP_OTW_WYTRYCHEM_UDANE_PICKLOCK);
        p->increase_ss(SS_DEX,          EXP_OTW_WYTRYCHEM_UDANE_DEX);
        p->increase_ss(SS_INT,          EXP_OTW_WYTRYCHEM_UDANE_INT);
        return 1;
    }
    p->increase_ss(SS_PICK_LOCK,    EXP_OTW_WYTRYCHEM_NIEUDANE_PICKLOCK);
    p->increase_ss(SS_DEX,          EXP_OTW_WYTRYCHEM_NIEUDANE_DEX);
    p->increase_ss(SS_INT,          EXP_OTW_WYTRYCHEM_NIEUDANE_INT);
}
#endif CAN_BE_PICK

#ifdef PICKLOCK_CAN_BE_BROKEN
/**
 * @return czy wytrych si� zepsuje (je�li tak to go usuwamy).
 */
public int
do_break_picklock(object p, object l)
{
    l->remove_object();
}
#endif PICKLOCK_CAN_BE_BROKEN

#ifdef CAN_BROKE_LOCK
/**
 * @return czy zamek w otwieraneach sie zepsuje
 */
public int
do_break_lock(object p, object l)
{
    add_prop(OC_I_BROKEN_LOCK, 1);
}
#endif CAN_BROKE_LOCK
/**
 * A ta funkcja wywo�ywana jest podczas wy�amywania otwierane.
 */
public varargs mixed
breakdown_me(object lom, int cicho = 0)
{
#ifdef CAN_BE_BROKENDOWN
    //Dla �omu troche inaczej to wygl�da.

    if(query_open())
    {
        return "Przecie� " + TO->koncowka("ten", "ta", "to", "ci", "te") + " " +
            TO->short(PL_MIA) + " " + (TO->query_tylko_mn() ? "s�" : "jest") + " otwarte!\n";
    }

    if(query_no_breakdown())
    {
        return TO->koncowka("Ten", "Ta", "To", "Ci", "Te") + " " +
            TO->short(PL_MIA) + " nie " + (TO->query_tylko_mn() ? "mog�" : "mo�e") +
            "zosta� wy�aman" + TO->koncowka("y", "a", "e", "i", "e") + ".\n";
    }

    if(lom->is_crowbar())
        return NF("Nie mo�esz tym tego wy�ama�.\n");

    if(lom)
    {
        if(!player_can_breakdown_this(TP, lom))
        {
            write("Napierasz z ca�ej si�y na " + TO->short(PL_BIE) + " " +
                lom->short(PL_NAR) + " jednak " + TO->koncowka("ten", "ta", "te", "ci", "te") + " ani drgn�.");
            saybb(QCIMIE(TP, PL_MIA) + " napiera na " + QSHORT(TO, PL_BIE) + " " +
                QSHORT(lom, PL_NAR) + " jednak " + TO->koncowka("ten", "ta", "te", "ci", "te") + " ani drgn�.\n");
            return 1;
        }
        else
        {
            write("Napierasz z ca�ej si�y na " + TO->short(PL_BIE) +
                lom->short(PL_NAR) + ", po chwili zmagania udaje ci si� " +
                TO->koncowka("ten", "ta", "te", "ci", "te") + " otworzy�.\n");
            saybb(QCIMIE(TP, PL_MIA) + " napierasz z ca�ej si�y na " + QSHORT(TO, PL_BIE) + " " +
                QSHORT(lom, PL_NAR) + ", po chwili zmagania udaje mu si� " +
                TO->koncowka("ten", "ta", "te", "ci", "te") + " otworzy�.\n");
        }
    }
    else
    {
        if(!player_can_breakdown_this(TP, lom))
        {
            write("Napierasz z ca�ej si�y na " + TO->short(PL_BIE) +
                "jednak " + TO->koncowka("ten", "ta", "te", "ci", "te") + " ani drgn" +
                (TO->query_tylko_mn() ? "�" : "ie") + ".\n");
            saybb(QCIMIE(TP, PL_MIA) + " napiera na " + QSHORT(TO, PL_BIE) + " " +
                "jednak " + TO->koncowka("ten", "ta", "te", "ci", "te") + " ani drgn" +
                (TO->query_tylko_mn() ? "�" : "ie") + ".\n");
            return 1;
        }
        else
        {
            write("Napierasz z ca�ej si�y na " + TO->short(PL_BIE) +
                ", po chwili zmagania udaje ci si� " + TO->koncowka("go", "je", "go", "ich", "je") + " otworzy�.\n");
            saybb(QCIMIE(TP, PL_MIA) + " napierasz z ca�ej si�y na " + QSHORT(TO, PL_BIE) +
                ", po chwili zmagania udaje mu si� " + TO->koncowka("go", "je", "go", "ich", "je") + " otworzy�.\n");
        }
    }
    //Wy�amujemy otwierane!

#ifdef BROKE_LOCK

    //Czy zamek si� zniszczy
    if(F_OC_LOCK_WILL_BREAK(TP->query_stat(SS_STR), TP->query_stat(SS_DEX), TP->query_skill(SS_BREAKDOWN), lom->query_jakosc(), TO->query_breakdown()))
    {
        do_break_lock(TP, lom);
    }
#endif BROKE_LOCK

    do_breakdown();
    TO->breakdown_now();
#endif CAN_BE_BROKENDOWN
}

/**
 * Funkcja obs�uguj�ca otwieranie otwierane wytrychem.
 */
public varargs mixed
pick_me(object wytrych, int cicho=0)
{
#ifdef CAN_BE_PICK
    if(!query_locked())
    {
        if(sizeof(fail_pick) && fail_pick[0])
        {
            if(functionp(fail_pick[0]))
            {
                function f;
                return f();
            }
            else
                return fail_pick[0];
        }
        else if(!cicho)
        {
            write("Wk�adasz " + wytrych->short(PL_BIE) + " do " + TO->query_lock_name(PL_DOP) + " " +
                TO->short(PL_DOP) + ", jednak wydaje si�, �e " + TO->koncowka("ten", "ta", "to", "ci", "te") +
                "jest ju� otwarte.\n");
            saybb(QCIMIE(TP, PL_MIA) + " wk�ada " + QSHORT(wytrych, PL_BIE) + " do " + TO->query_lock_name(PL_MIE) + " " +
                QSHORT(TO, PL_DOP) + " i zaczyna przy nim majstrowa� majstrowa�.\n");
            return 2;
        }
    }

    klucz = wytrych;

    if(player_can_pick_this(TP, wytrych))
    {
        if(pick_mess1)
        {
            if(functionp(pick_mess1))
            {
                function f = pick_mess1;
                saybb(QCIMIE(TP, PL_MIA) + " " + f());
            }
            else
                saybb(QCIMIE(TP, PL_MIA) + " " + pick_mess1);
        }
        else
            saybb(QCIMIE(TP, PL_MIA) + " wk�ada " + QSHORT(wytrych, PL_BIE) + " do " +
                TO->query_lock_name(PL_MIE) + " " + QSHORT(TO, PL_DOP) + "... " +
                "Chwile majstruje... Po chwili z zamka s�ycha� wyra�ne 'klik'.\n");

        if(pick_mess2)
        {
            if(functionp(pick_mess2))
            {
                function f = pick_mess2;
                write(f());
            }
            else
                write(pick_mess2);
        }
        else
            write("Wk�adasz " + wytrych->short(PL_BIE) + " do " + TO->query_lock_name(PL_MIE) + " " +
                QSHORT(TO, PL_DOP) + ". Chwile majstrujesz przy " + TO->query_lock_name(PL_MIE) + " " +
                " i udaje ci si� go otworzy�.\n");
    }
    else
    {
        if(sizeof(fail_pick) > 1 && fail_pick[1])
        {
            if(functionp(fail_pick[1]))
            {
                function f = fail_pick[1];
                saybb(QCIMIE(TP, PL_MIA) + " " + f());
            }
            else
                saybb(QCIMIE(TP, PL_MIA) + " " + fail_pick[1]);
        }
        else
            saybb(QCIMIE(TP, PL_MIA) + " wk�ada " + QSHORT(wytrych, PL_BIE) + " do " +
                TO->query_lock_name(PL_MIE) + " " + QSHORT(TO, PL_DOP) + " i zaczyna przy nim majstrowa�.\n");

        if(sizeof(fail_pick) > 2 && fail_pick[2])
        {
            if(functionp(fail_pick[2]))
            {
                function f = fail_pick[2];
                write(f());
            }
            else
                write(fail_pick[2]);
        }
        else
        {
            write("Wk�adasz " + wytrych->short(PL_BIE) + " do " + TO->query_lock_name(PL_MIE) + " " +
                TO->short(PL_DOP) + " i pr�bujesz go otworzy�, jednak nie udaje ci si� to.\n");
        }
    }

#ifdef PICKLOCK_CAN_BE_BROKEN
    //Czy zamek si� zniszczy
    if(F_OC_PICKLOCK_WILL_BREAK(TP->query_stat(SS_DEX), TP->query_skill(SS_OPEN_LOCK), wytrych->query_jakosc(), TO->query_pick()))
        do_break_picklock(TP, wytrych);
#endif PICKLOCK_CAN_BE_BROKEN

    do_pick();
    TO->pick_now();
#endif CAN_BE_PICK
}

public int
is_open_close()
{
    return 1;
}
