/**
 * \file /lib/tool.c
 *
 * Ten plik zawiera wsparcie dla narz�dzi.
 */

#include <tools.h>

/*
 * Dziedziny, w kt�rych mo�e by� u�ywane narz�dzie.
 */
private int* tool_domains;

int tool_difficulty = TOOL_DEFAULT_DIFFICULTY;

/**
 * Dodaje dziedzin�, w kt�rej mo�e by� u�ywane narz�dzie.
 *
 * @param domain dziedzina, w kt�rej mo�e by� u�ywane narz�dzie.
 */

public void
add_tool_domain(int domain)
{
	if (!pointerp(tool_domains)) {
		tool_domains = ({});
	}
	if (member_array(domain, tool_domains) == -1) {
		tool_domains += ({domain});
	}
}

/**
 * Zwraca dziedziny, w kt�rych mo�e by� u�ywane dane narz�dzie.
 *
 * @return dziedziny, w kt�rych mo�e by� u�ywane dane narz�dzie.
 */

public mixed
query_tool_domains()
{
	return secure_var(tool_domains);
}

/**
 * Zwraca trudno�� u�ywania danego narz�dzia.
 *
 * @return trudno�� u�ywania danego narz�dzia.
 */
public int
query_tool_difficulty()
{
	return tool_difficulty;
}

/**
 * Funkcja wywo�ywana przy u�yciu narz�dzia.
 *
 * @param ob obiekt, do kt�rego stworzenia u�ywamy narz�dzia
 * @param domain do jakiej dziedziny narz�dzie zosta�o u�yte
 * @param sslvl poziom umiej�tno�ci craftsmana
 *
 * @return 1 pomy�lne u�ycie narz�dzia
 * @return 0 nie uda�o si� narz�dzia u�y�
 * @return string inny rodzaj b��du/powiadomienia
 */

public mixed
notify_use_tool(object ob, int domain, int sslvl)
{
    if (sslvl < query_tool_difficulty()) {
        return 0;
    }
	return 1;
}

/**
 * Ustawia trudno�� u�ywania danego narz�dzia.
 *
 * @param difficulty trudno�� u�ywania danego narz�dzia
 */
public void
set_tool_diffculty(int difficulty)
{
	tool_difficulty = difficulty;
}
