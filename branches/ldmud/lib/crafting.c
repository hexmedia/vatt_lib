/**
 * \file /lib/crafting.c
 *
 * Plik zawieraj�cy funkcje odpowiedzialne za crafting przedmiot�w.
 */

#include <stdproperties.h>
#include <pattern.h>
#include <files.h>
#include <object_types.h>
#include <formulas.h>
#include <rozmiary.h>

/**
 * Funkcja sprawdzaj�ca, czy podane materia�y starcz� do wytworzenia
 * przedmiotu.
 *
 * @param ob sprawdzany obiekt
 * @param items lista obiekt�w, kt�re mog� pos�u�y� jako materia�y
 * @param damages jak bardzo przedmiot jest zniszczony (20 - ca�kowicie)
 *
 * @return 1 wystarczy materia��w do wytworzenia przedmiotu
 * @return 0 nie wystarczy materia��w do wytworzenia przedmiotu
 */
varargs int
check_materials(object ob, object* items, int damaged = 0)
{
    mixed needed = ([]);
    float weight = itof(ob->query_prop(OBJ_I_WEIGHT));
    foreach(string material : ob->query_materialy()) {
        needed[material] = (ob->procent_materialu(material) / 100.0) * weight;
        if ((damaged > 0) && (damaged <= 20)) {
            needed[material] = needed[material] * (itof(damaged) / 20.0);
        }
    }
    foreach(object item : items) {
        foreach(string material : item->query_materials()) {
            if (((floatp(needed[material])) && (needed[material] > 0.0)) ||
                    ((intp(needed[material])) && (needed[material] > 0))) {
                needed[material] -= itof(item->query_material_amount(material));
            }
        }
    }
    foreach(string material : m_indices(needed)) {
        if (needed[material] > 0.0) {
            return 0;
        }
    }
    return 1;
}

/**
 * Funkcja sprawdzaj�ca czy materia�y wystarcz� do naprawy
 * itemu.
 *
 * @param ob obiekt kt�ry ma by� zmieniany
 * @param items obiekty kt�re mog� by� materia�ami
 * @param nr nazwa rozmiaru
 */
public int
check_materials_repair(object ob, object *items, string nr)
{
    //Na razie nie sprawdzamy bo nie zmienia si� waga obiektu
    //Trzeba to b�dzie dopisa�.
    return 1;
}

/**
 * Funckja sprawdzaj�ca czy materia�y wystarcz� do powi�kszenia
 * obiektu - narazie nieu�ywana - FIXME: Doda� w craftsmanie
 *
 * @param ob obiekt kt�ry ma by� powi�kszony
 * @param items obiekty kt�re s�u�� za materia�y
 * @param os stary rozmiar ubrania
 * @param ns rozmiar na kt�ry ubranie ma by� przerobione
 */
public int
check_materials_resize(object ob, object *items, string os, string ns)
{
    return 1;
}

/**
 * Funkcja sprawdzaj�ca, czy podane narz�dzia wystarcz� dla
 * rzemie�lnika do obr�bki przedmiotu wed�ug podanego wzorca.
 *
 * @param craftsman rzemie�lnik
 * @param pattern wz�r
 * @param items lista obiekt�w, kt�re mog� s�u�y� jako narz�dzia
 *
 * @return 1 potrzebne narz�dzia s� dost�pne
 * @return 0 brak potrzebnych narz�dzi
 */
int
check_tools(object craftsman, object pattern, object* items)
{
    int domain = pattern->query_pattern_domain();
    foreach(object item : items) {
        if ((member_array(domain, item->query_tool_domains()) != -1) &&
                (item->query_tool_difficulty() >= pattern->query_needed_tool_difficulty()) &&
                (craftsman->query_skill(PATTERN_SKILL_MAP[domain]) >=
                 item->query_tool_difficulty())) {
            return 1;
        }
    }
    return 0;
}

/**
 * Funkcja sprawdzaj�ca, czy podane narz�dzia wystarcz� dla
 * rzemie�lnika do naprawy lub zmiany rozmiaru danego przedmiotu.
 *
 * @param craftsman rzemie�lnik
 * @param ob dany przedmiot
 * @param items lista obiekt�w, kt�re mog� s�u�y� jako narz�dzia
 *
 * @return 1 potrzebne narz�dzia s� dost�pne
 * @return 0 brak potrzebnych narz�dzi
 */
int
check_tools_r(object craftsman, object ob, object* items)
{
    int domain;
    if (IS_WEAPON_OBJECT(ob) || IS_ARMOUR_OBJECT(ob)) {
        domain = PATTERN_DOMAIN_BLACKSMITH;
    }
    else if (ob->query_type() == O_UBRANIA) {
        domain = PATTERN_DOMAIN_TAILOR;
    }
    else {
        return 0;
    }
    foreach(object item : items) {
        if ((member_array(domain, item->query_tool_domains()) != -1) &&
             (item->query_tool_difficulty() >= 50) &&
             (craftsman->query_skill(PATTERN_SKILL_MAP[domain]) >=
             item->query_tool_difficulty())) {
            return 1;
             }
    }
    return 0;
}

/**
 * Funkcja sprawdzaj�ca, czy rzemie�lnik ma wymagane umiej�tno�ci
 * do wykonania rzeczy wed�ug danego wzorca.
 *
 * @param craftsman rzemie�lnik
 * @param pattern wz�r
 *
 * @return 1 potrzebne umiej�tno�ci s� dost�pne
 * @return 0 brak potrzebnych umiej�tno�ci
 */
int
check_skills(object craftsman, object pattern)
{
    if (craftsman->query_skill(PATTERN_SKILL_MAP[pattern->query_pattern_domain()]) >=
            pattern->query_pattern_difficulty()) {
        return 1;
    }
    return 0;
}

/**
 * Funkcja sprawdzaj�ca, czy rzemie�lnik ma wymagane umiej�tno�ci
 * do naprawy lub zmiany rozmiaru danego przedmiotu.
 *
 * @param craftsman rzemie�lnik
 * @param ob dany przedmiot
 *
 * @return 1 potrzebne umiej�tno�ci s� dost�pne
 * @return 0 brak potrzebnych umiej�tno�ci
 */
int
check_skills_r(object craftsman, object ob)
{
    int domain;
    if (IS_WEAPON_OBJECT(ob) || IS_ARMOUR_OBJECT(ob)) {
        domain = PATTERN_DOMAIN_BLACKSMITH;
    }
    else if (ob->query_type() == O_UBRANIA) {
        domain = PATTERN_DOMAIN_TAILOR;
    }
    else {
        return 0;
    }
    if (craftsman->query_skill(PATTERN_SKILL_MAP[domain]) >= 50) {
        return 1;
    }
    return 0;
}


/**
 * Funkcja zu�ywaj�ca materia�y dla wytworzenia danego przedmiotu.
 *
 * @param ob sprawdzany obiekt
 * @param items lista obiekt�w, kt�re mog� pos�u�y� jako materia�y
 */
void
use_materials(object ob, object* items)
{
    int tmpAmount;
    mixed needed = ([]);
    float weight = itof(ob->query_prop(OBJ_I_WEIGHT));
    foreach(string material : ob->query_materialy()) {
        needed[material] = (ob->procent_materialu(material) / 100.0) * weight;
    }
    foreach(string material : m_indices(needed)) {
        foreach(object item : items) {
            if ((tmpAmount = item->query_material_amount(material))) {
                if (needed[material] > itof(tmpAmount)) {
                    needed[material] -= itof(tmpAmount);
                    item->set_material_amount(material, 0);
                    item->notify_change_material_amount(material, tmpAmount, 0);
                }
                else {
                    item->set_material_amount(material,
                            ftoi(itof(tmpAmount) - needed[material]));
                    needed[material] = 0.0;
                    item->notify_change_material_amount(material, tmpAmount,
                            item->query_material_amount(material));
                    break;
                }
            }
        }
    }
}

/**
 * Funkcja u�ywaj�ca narz�dzi do wytworzenia danego przedmiotu przez
 * podanego rzemie�lnika.
 *
 * @param craftsman rzemie�lnik
 * @param ob sprawdzany obiekt
 * @param pattern wz�r
 * @param items lista obiekt�w, kt�re mog� pos�u�y� jako materia�y
 *
 * @return 1 uda�o si� u�ycie narz�dzia
 * @return 0 nie powiod�o si� u�ycie narz�dzia
 * @return string inny komunikat o b��dzie
 */
mixed
use_tool(object craftsman, object ob, object pattern, object* items)
{
    int domain = pattern->query_pattern_domain();
    mixed toReturn;
    foreach(object item : items) {
        if ((member_array(domain, item->query_tool_domains()) != -1) &&
                (item->query_tool_difficulty() >= pattern->query_needed_tool_difficulty()) &&
                (craftsman->query_skill(PATTERN_SKILL_MAP[domain]) >=
                 item->query_tool_difficulty())) {
            toReturn = item->notify_use_tool(ob, domain, craftsman->query_skill(PATTERN_SKILL_MAP[domain]));
            if (stringp(toReturn)) {
                return toReturn;
            }
            else if (intp(toReturn) && (toReturn == 1)) {
                return 1;
            }
        }
    }
    return 0;
}

/**
 * Funkcja wyliczaj�ca ilo�� punkt�w do naprawienia w danej rzeczy.
 *
 * @param ob sprawdzany obiekt
 *
 * @return int ilo�� punkt�w do naprawienia
 */
int
estimate_repair_effort(object ob)
{
    int cond_rep = ob->query_condition();
    int dull_rep = ob->query_dull();
    int corr_rep = ob->query_corroded();

    if (cond_rep > F_ARMOUR_MAX_REPAIR(ob->query_condition()))
        cond_rep = F_ARMOUR_MAX_REPAIR(ob->query_condition());
    if (dull_rep > F_WEAPON_MAX_REPAIR_DULL(ob->query_dull()))
        dull_rep = F_WEAPON_MAX_REPAIR_DULL(ob->query_dull());
    if (corr_rep > F_WEAPON_MAX_REPAIR_CORR(ob->query_corroded()))
        corr_rep = F_WEAPON_MAX_REPAIR_CORR(ob->query_corroded());

    cond_rep -= ob->query_repair();
    if (cond_rep < 0)
        cond_rep = 0;
    dull_rep -= ob->query_repair_dull();
    if (dull_rep < 0)
        dull_rep = 0;
    corr_rep -= ob->query_repair_corr();
    if (corr_rep < 0)
        corr_rep = 0;

    int toRepair = cond_rep + dull_rep + corr_rep;

    return toRepair;
}

/**
 * Funkcja naprawiaj�ca obiekt.
 *
 * @param ob obiekt do naprawienia.
 */
void
repair_item(object ob)
{
    int cond_rep = ob->query_condition();
    int dull_rep = ob->query_dull();
    int corr_rep = ob->query_corroded();

    if (cond_rep > F_ARMOUR_MAX_REPAIR(ob->query_condition()))
        cond_rep = F_ARMOUR_MAX_REPAIR(ob->query_condition());
    if (dull_rep > F_WEAPON_MAX_REPAIR_DULL(ob->query_dull()))
        dull_rep = F_WEAPON_MAX_REPAIR_DULL(ob->query_dull());
    if (corr_rep > F_WEAPON_MAX_REPAIR_CORR(ob->query_corroded()))
        corr_rep = F_WEAPON_MAX_REPAIR_CORR(ob->query_corroded());

    ob->set_repair(cond_rep);
    ob->set_repair_dull(dull_rep);
    ob->set_repair_corr(corr_rep);
}

/**
 * Funkcja sprawdzaj�ca czy obiekt mo�e by� tak bardzo zmieniony.
 * Maksymalna zmiana to narazie 2 poziomy.
 */
int
can_be_resized(object ob, string os, string ns)
{
    string *rozmiary = ROZMIARY;

    int ios, ins;

    if(ns == "D") //do siebie
        ns = TP->query_size(ob->pobierz_rozmiary());
    else if(ns == "Q") // do siebie w ciuchach
        ns = TP->query_size(ob->oblicz_rozmiar_osoby());

    if((ios=member_array(os, rozmiary)) == -1 || (ins=member_array(ns, rozmiary)) == -1)
        return 0;

    if(ABS(ios-ins) > MAX_RESIZE(ob))
        return 0;

    return 1;
}

/**
 * Dzi�ki tej funkcji pobieramy rozmiar dopasowany do gracza
 * uwzgl�dniwszy to, czy mierzymy z ciuchami czy bez...
 *
 * @param ob obiekt kt�rego rozmiar mieczymy
 * @param player gracz dla kt�rego mierzymy
 * @param ciuch czy uwzgl�dniamy ciuchy czy nie
 *
 * @return tablice z nowymi rozmiarami ciucha.
 */
float *
pobierz_rozmiar(object ob, object player, int ciuch)
{
    float *rozmiarowka, *rozm_pl, *rozm_ob;

    if(ciuch)
        rozm_pl = player->oblicz_rozmiary_osoby();
    else
        rozm_pl = player->pobierz_rozmiary();

    rozm_ob = ob->pobierz_rozmiary();

    rozmiarowka = allocate(sizeof(rozm_pl));

    for(int i=0;i<sizeof(rozm_pl);i++)
    {
        if(rozm_ob[i] != -1.0)
        {
            rozmiarowka[i] = rozm_pl[i] + frandom(1.5, 1) - 0.75;
                //Dodajemy malutk� losowo��. W ko�cu krawiec
                //nie maszyna mo�e si� pomyli�.
                //FIXME: Mo�na to uzale�ni� od um�w krawca.
        }
        else
            rozmiarowka[i] = -1.0;
    }

    return rozmiarowka;
}


/**
 * Funkcja zmieniaj�ca rozmiar obiektu
 *
 * @param ob obiekt kt�rego rozmiar zmieniamy
 * @param player obiekt dla kt�rego zmieniamy
 * @param r nowy rozmiar obiektu - je�li podamy obiekt to b�dzie to
 *          dopasowanie
 */
void
resize_item(object ob, object player, string r)
{
    if(!ob || !player)
        return;

    if(member_array(r, ROZMIARY+({"D", "Q"})) == -1)
        return;

    if(r == "D")
        ob->ustaw_rozmiary(pobierz_rozmiar(ob, player, 0));
    else if(r == "Q")
        ob->ustaw_rozmiary(pobierz_rozmiar(ob, player, 1));
    else
        ob->set_size(r);
}

/**
 * Dzi�ki tej funkcji mo�emy sprawdzi� czy ciuch jest dopasowany
 * do gracza.
 *
 * @param ob ciuch kt�ry chcemy sprawdzi�
 * @param pl gracz do kt�rego ciuch ma by� dopasowany
 * @param ciuch czy maj� by� uwzgl�dniane ciuchy
 */
public int
size_is_good(object ob, object pl, int ciuch)
{
    float *rozm, *rozm2;

    if(ciuch)
        rozm = pl->oblicz_rozmiary_osoby();
    else
        rozm = pl->pobierz_rozmiary();

    rozm2 = ob->pobierz_rozmiary();

    for(int i = 0; i < sizeof(rozm); i++)
    {
        if(rozm2[i] == -1.0)
            continue;

        if(abs(rozm[i] - rozm2[i]) > 1.0)
            return 0;//Je�li wi�ksze od 1.0 to nie takie samo:)
    }

    //Je�li do tej pory nie przerwa�o to znaczy, �e jest takie samo
    return 1;
}
