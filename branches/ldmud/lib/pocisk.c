/**
 * \file /lib/pocisk.c
 *
 * Lib wszelkiego rodzaju pocisk�w.
 *
 * @author Krun
 * @date Grudzie� 2007 / Stycze� 2008
 *
 *      W ka�dym pocisku opartym o tego liba zdefiniowana musi zosta�
 * funkcja prevent_leave w spos�b pokazany poni�ej.
 * @example
 *      public int prevent_leave(object ob)
 *      {
 *          if(pocisk_prevent_leave(ob))
 *              return 1;
 *
 *          return ::prevent_leave(ob);
 *      }
 */

#pragma no_clone
#pragma strict_types

#include <macros.h>
#include <stdproperties.h>

int     typ_pocisku, jakosc, dt;
object  bron;

/**
 * Funkcja identyfikuj�ca obiekt jako pocisk.
 *
 * @return 1 zawsze
 */
public int is_bullet() { return 1; }

/**
 * Funkcja identyfikuj�ca obiekt jako pocisk.
 *
 * @return 1 zawsze
 */
public int query_bullet() { return 1; }

/**
 * Dzi�ki tej funkcji ustawiamy jako�� pocisku.
 *
 * @param j jako�� pocisku
 */
public void ustaw_jakosc(int j) { jakosc = max(1, min(100, j)); }

/**
 * @return Jako�� pocisku.
 */
public int query_jakosc() { return jakosc; }

/**
 * Ustawiamy typ broni w kt�rej ten pocisk b�dzie m�g� dzia�a�.
 *
 * @param tp typ broni
 */
public void ustaw_typ_pocisku(int tp) { typ_pocisku = tp; }

/**
 * @return typ pocisku
 */
public int query_typ_pocisku() { return typ_pocisku; }

/**
 * Ustawia bro� do kt�rej pocisk jest za�adowany.
 *
 * @param wep bro� do kt�rej pocisk jest zaladowany.
 */
public void ustaw_bron(object wep) { bron = wep; }

/**
 * @return bro� do kt�rej za�adowany jest pocisk.
 */
public object query_bron() { return bron; }

/**
 * Funkcja wywo�ywana gdy pocisk opuszcza otoczenie.
 *
 * @param from opuszczane �rodowisko
 * @param to nowe �rodowisko
 */
public int pocisk_leave_env(object from, object to)
{
    if(query_bron())
        return 1;
}

public void zaladuj_mnie_do_broni()
{
    ustaw_bron(previous_object());
    TO->set_no_show_composite(1);
    TO->add_prop(OBJ_M_NO_DROP, "Nie mo�esz tego od�o�y�.\n");
    TO->add_prop(OBJ_M_NO_GIVE, "Nie mo�esz tego nikomu da�.\n");
    TO->add_prop(OBJ_M_NO_SELL, "Nie mo�esz tego sprzeda�.\n");
    TO->add_prop(OBJ_M_NO_STEAL, "Nie mo�esz tego ukra��.\n");
    TO->add_prop(OBJ_M_NO_BUY, "Nie mo�esz tego kupi�.\n");
    TO->add_prop(OBJ_M_NO_TELEPORT, "Nie mo�esz nic z tym zrobi�.\n");
    TO->add_prop(OBJ_M_NO_THROW, "Nie mo�esz tym rzuca�.\n");
}

public void wyladuj_mnie_z_broni()
{
    ustaw_bron(0);
    TO->set_no_show_composite(0);
    TO->remove_prop(OBJ_M_NO_DROP);
    TO->remove_prop(OBJ_M_NO_GIVE);
    TO->remove_prop(OBJ_M_NO_SELL);
    TO->remove_prop(OBJ_M_NO_STEAL);
    TO->remove_prop(OBJ_M_NO_BUY);
    TO->remove_prop(OBJ_M_NO_TELEPORT);
    TO->remove_prop(OBJ_M_NO_THROW);
}

/**
 * Ustawiamy typ obra�e� zadawanych przez pocisk.
 */
public void set_dt(int t)
{
    dt = t;
}

/**
 * @return typ obra�e� zadawanych przez bro�.
 */
public int query_dt()
{
    return dt;
}

private static string
hit_d(int hid, object trafiony, string mes, string zen, string nij, string mnm, string mnn)
{
    int r = trafiony->query_hitloc_rodzaj(hid);
    if(trafiony->query_hitloc_mn(hid))
    {
        if(r >= PL_ZENSKI)
            return mnn;
        else
            return mnm;
    }
    else if(r < PL_ZENSKI)
        return mes;
    else if(r == PL_ZENSKI)
        return zen;
    else if(r > PL_ZENSKI)
        return nij;
}

/**
 * Funkcja ta zwraca opis obra�e� pokazywany przy strza�ach w livingi.
 *
 * @param obrp obra�enia procentowo
 * @param damt typ obra�e�
 * @param hid identyfikator hitlokacji
 * @param hdesc opis sublokacji
 * @param strzelajacy obiekt strzelajacy
 * @param trafiony obiekt trafiony
 * @param zbroja obiekt przebijanej zbroi
 * @param forob obiekt dla kt�rego podajemy, je�li nie podamy wy�wietlimy VBFC.
 *
 * @return opis obra�e�
 */
public string opis_obrazen(int obrp, int damt, int hid, string hdesc,
    object strzelajacy, object trafiony, object zbroja, object forob)
{
    string ret = "";

    int *d = ({0, 0, 0}), dt;

    if(damt & W_IMPALE)
        d[0]++;
    if(damt & W_SLASH)
        d[1]++;
    if(damt & W_BLUDGEON)
        d[2]++;

    //Jak nieustawione to k�ute:)
    if(!fold(d, &operator(+)(), 0))
        dt = 0;

    while(d[(dt=random(sizeof(d)))] != 1);

    int f;
    if(!objectp(forob))
        f = 0;
    else if(forob == strzelajacy)
        f = 1;
    else if(forob == trafiony)
        f = 2;
    else
        f = 0;

    if(1 == 1) //(!zbroja)
    {
        switch(obrp)
        {
            case 0..10:
                ret += "ledwo muska " + ({QIMIE(trafiony, PL_BIE), trafiony->short(strzelajacy, PL_BIE),
                    "Ci�"})[f] + " w " + hdesc;
                break;
            case 11..20:
                ret += "muska " + ({QIMIE(trafiony, PL_BIE), trafiony->short(strzelajacy, PL_BIE),
                    "Ci�"})[f] + " w " + hdesc;
                break;
            case 21..30:
                ret += ({"ledwo wbija si�", "ledwo zadrapuje", "obija"})[dt] + " w " +
                    (f == 2 ? hit_d(hid, trafiony, "tw�j", "twoj�", "twoje", "twoje", "twoje") + " " : "") + hdesc+
                    ({" " + QIMIE(trafiony, PL_BIE), " " + trafiony->short(strzelajacy, PL_BIE), ""})[f];
                break;
            case 31..50:
                ret += ({"wbija si�", "zadrapuje", "lekko uderza"})[dt] + " w " +
                    (f == 2 ? hit_d(hid, trafiony, "tw�j", "twoj�", "twoje", "twoje", "twoje") + " " : "") + hdesc+
                    ({" " + QIMIE(trafiony, PL_BIE), " " + trafiony->short(strzelajacy, PL_BIE), ""})[f];
                break;
            case 51..70:
                ret += ({"wbija si� g��boko", "lekko nacina", "uderza"})[dt] + " w " +
                    (f == 2 ? hit_d(hid, trafiony, "tw�j", "twoj�", "twoje", "twoje", "twoje") + " " : "") + hdesc+
                    ({" " + QIMIE(trafiony, PL_BIE), " " + trafiony->short(strzelajacy, PL_BIE), ""})[f];
                break;
            case 71..90:
                ret += ({"przebija si� przez", "nacina", "t�ucze"})[dt] + " " +
                    (f == 2 ? hit_d(hid, trafiony, "tw�j", "twoj�", "twoje", "twoje", "twoje") + " " : "") + hdesc+
                    ({" " + QIMIE(trafiony, PL_BIE), " " + trafiony->short(strzelajacy, PL_BIE), ""})[f];
                break;
            default:
                ret += ({"przeszywa", "siecze g��boko", "grzmoci"})[dt] + " " +
                    (f == 2 ? hit_d(hid, trafiony, "tw�j", "twoj�", "twoje", "twoje", "twoje") + " " : "") + hdesc+
                    ({" " + QIMIE(trafiony, PL_DOP), " " + trafiony->short(strzelajacy, PL_DOP), ""})[f];
        }
    }
    else
    {   //Tu jest w kurwe zabawy bo trzeba dziadowstwo tak pouk�ada�, coby
        //teksty by�y fajne.
        ret += "przebija si� przez " + (objectp(forob) ? zbroja->short(forob, PL_DOP) : QSHORT(zbroja, PL_DOP));
    }

    return ret;
}
