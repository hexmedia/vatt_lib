/* Troche przerobilam...Wlasciwie calkiem sporo :)
     m.in. komunikaty, super wypasna funkcja is_naczynie, stuknij etc.
     jak i zmienilam komende 'kesnij' na 'napij sie'
     i kilka innych mniejszych lub ciut wiekszych zmian
     typu: trzeba chwycic, aby uzywac ^_^
     Te naczynia z zalozenia sluzyc maja jedynie jako
     naczynia karczemne.
                                                 Lil  03.07.2005 */

/* Obiekt naczynia specjalnie for Dragfor by Simon wiekszosc kodu tutaj
 * pochodzi z /std/beczulka ja dodalem pare funkcji i to wszystko :D. Ale
 * generalnie opiera sie to na /std/beczulka i zamiast napij sie jest kesnij
 * oraz mozliwosc     wychylenia do dna.
 * Simon pazdziernik 2003
 */
#pragma strict_types

inherit "/std/holdable_object";
inherit "/cmd/std/command_driver.c";

#include <pl.h>
#include <macros.h>
#include <stdproperties.h>
#include <wa_types.h>
#include <cmdparse.h>

int niszczenie = 0;

private int     pojemnosc_naczynia,
                ilosc_jedzenia,
                procent_alco;
private string  opis_jedzenia,
                dlugi_opis,
                nazwa_jedzenia;
private mixed   nazwaj;

void
create_naczynie()
{
}

private int     sprobuj(string str);
private int     zjedz(string str);
private int     rozbij (string str);
private int     pomoc (string str);
void            oblicz_sh_po_wypiciu();
int             zle (string str);

/**
 * Konstruktor obiektu. Ustawiona jako nomask, wywoluje
 * create_beczulka().
 */
public nomask void
create_holdable_object()
{
    ustaw_nazwe("naczynie");
    pojemnosc_naczynia = 300;
    ilosc_jedzenia = 0;
    opis_jedzenia = "";
    nazwaj = ({});
    add_prop(OBJ_I_WEIGHT, "@@query_waga");
    add_prop(OBJ_I_VOLUME, "@@query_objetosc");
    add_prop(OBJ_I_VALUE, 1);
    add_prop(OBJ_M_NO_SELL, "@@query_mozna_sprzedac");
    set_long("@@long");
    set_hit(1);
    set_pen(1);
    set_wt(W_CLUB);
    set_dt(W_BLUDGEON);
    set_likely_dull(MAXINT);
    set_likely_break(MAXINT);
    set_weapon_hits(9991000);
    create_naczynie();
}

/**
 * @return Zwraca 1 jesli obiekt jest naczyniem.
 */
public int
is_naczynie()
{
    return 1;
}

/**
 * @return Zwraca wage beczulki, w zaleznosci od jej pojemnosci i ilosci plynu w srodku.
 */
public int
query_waga()
{
    return ilosc_jedzenia + (pojemnosc_naczynia / 4);
}

/**
 * @return Zwraca objetosc beczulki, w zaleznosci od jej pojemnosci.
 */
public int
query_objetosc()
{
    return 6 * pojemnosc_naczynia / 5;
}

/**
 * @return Zwraca czy mozna sprzedac - w zaleznosci od tego, czy jest pusta czy nie.
 * @return 0 - mozna sprzedac,
 * @return string - nie mozna sprzedac, wyjasnienie dlaczego.
 */
public mixed
query_mozna_sprzedac()
{
    return "Nie mo^zesz tego sprzeda^c.";
}

public void
init()
{
     add_action(sprobuj,    "spr^obuj");
     add_action(sprobuj,    "posmakuj");
     add_action(sprobuj,    "skosztuj");
     add_action(zjedz,      "zjedz");
     add_action(rozbij,     "rozbij");
     add_action(pomoc,      "?", 2);

    ::init();
}

private static mixed
tylko_jeden_przypadek(mixed x, int p)
{
    return x[0][p];
}

/**
 * Wywolywana, gdy gracz wpisze komende 'spr�buj' wy�wietla mu jak smakuje
 * dana potrawa.
 *
 * @param string - argument do komendy.
 *
 * @return 1 - komenda przyjeta, lub
 * @return 0 - nie przyjeta, szukaj w innym obiekcie.
 */
private int
sprobuj(string str)
{
    object *obs;
    string co;
    string dd;
    int ret;
    string nazwa;

    notify_fail(capitalize(query_verb()) + " co?\n");
    if (!strlen(str))
        return 0;

    if (member_array(str, map(nazwaj, &tylko_jeden_przypadek(,PL_DOP))) >= 0)
    {
        write("Pr^obujesz "+nazwaj[0][0][PL_DOP]+". ");
        saybb(QCIMIE(TP, PL_MIA)+" pr^obuje "+nazwaj[0][0][PL_DOP]+".\n");

        switch(random(6, atoi(OB_NUM(TP)) + atoi(OB_NUM(this_object()))))
        {
            case 0 : write("Smakuje ca^lkiem nie^xle.\n");  break;
            case 1 : write("Smakuje nienajgorzej.\n");      break;
            case 2 : write("Smakuje wy^smienicie.\n");      break;
            case 3 : write("Smakuje przepysznie.\n");       break;
            case 4 : write("Nie smakuje najlepiej...\n");   break;
            case 5 : write("^Srednio ci smakuje.\n");       break;
        }

        return 1;
    }
    return 0;

// ponizsze na razie jest niewazne
#if 0
    if (!parse_command(str, all_inventory(TP),
        " %i:" + PL_DOP, obs))
        return 0;

    obs = NORMAL_ACCESS(obs, 0, 0);

    if (!sizeof(obs))
        return 0;

    if (sizeof(obs) > 1)
    {
        notify_fail("Mo^zesz si^e napi^c tylko z jednego naczynia naraz.\n");
        return 0;
    }

    if(!obs[0]->query_wielded())
    {
        notify_fail("Musisz wpierw chwyci^c " + koncowka("ten", "ta", "to") +
        " " + query_nazwa(PL_BIE) + ".\n");
        return 0;
    }

    if(obs[0]->query_nazwa_jedzenia_dop() == nazwaj[0][PL_DOP])
    {
        write("probujesz\n");
        return 1;
    }


    if(co!=nazwa_jedzenia) return 0;
    nazwa=obs[0]->query_nazwa(PL_DOP);

    if(wildmatch("s[bcdfghjklmnprstwxz]*",nazwa))
    {
        if(dd!="ze") return 0;
    }
    else
    {
        if(dd!="z") return 0;
    }

    ret = obs[0]->drink_from_it(TP);

    if (ret)
        TP->set_obiekty_zaimkow(obs);

    return ret;
#endif
}

/**
 * @return d�ugi opis naczynia.
 */
public string
long()
{
    string str;

    str = ((::long()) ? (::long()) : "Jest to " + query_nazwa(PL_MIA) + " slu^z^ac" +
        koncowka("y", "a", "e") + " do przechowywania r^o^znych potraw. ") +
        " W tej chwili ";

    switch(100 * ilosc_jedzenia / pojemnosc_naczynia)
    {
        case 0: str += "jest zupe^lnie pust" + koncowka("y", "a", "e"); break;
        case 1..20: str += "zawiera ma^l^a ilo^s^c " + opis_jedzenia; break;
        case 21..50: str += "zawiera troch^e " + opis_jedzenia; break;
        case 51..70: str += "zawiera sporo " + opis_jedzenia; break;
        case 71..90: str += "jest prawie pe^ln" + koncowka("y", "a", "e") +
            " " + opis_jedzenia; break;
        default: str += "jest pe^ln" + koncowka("y", "a", "e") + " " +
            opis_jedzenia; break;
    }

    str += ".\n";

    return str;
}

/**
 * Alias do set_long.
 */
public void
set_dlugi_opis(string opis)
{
    set_long(opis);
}

/**
 * Alias do query_long().
 */
public string
query_dlugi_opis()
{
    return ::query_long();
}

/**
 * Slu�y do ustawiania pojemno�ci beczu�ki w mililitrach.
 * @param pojemnosc - pojemno�� w mililitrach.
 */
public void
set_pojemnosc(int pojemnosc)
{
    pojemnosc_naczynia = pojemnosc;
}

/**
 * @return Zwraca pojemno�� beczulki w mililitrach.
 */
public int
query_pojemnosc()
{
    return pojemnosc_naczynia;
}

/**
 * S�u�y do ustawienia ile gram�w jedzenia znajduje si� na talerzu.
 *
 * @param ilosc gram�w jedzenia na talerzu
 */
public void
set_ilosc_jedzenia(int ilosc)
{
    if (ilosc > pojemnosc_naczynia)
        ilosc = pojemnosc_naczynia;

    ilosc_jedzenia = ilosc;
}

/**
 * @return Zwraca aktualn� ilo�� jedzenia na talerzu.
 */
public int
query_ilosc_jedzenia()
{
    return ilosc_jedzenia;
}

/**
 * Ustawiamy nazwe jedzenia we wszystkich przypadkach i kombinacjach.
 *
 * @param nazwy nazwy jedziena
 */
public void ustaw_nazwy_jedzenia(mixed nazwy)
{
    nazwaj = nazwy;
}

/**
 * @return Zwraca wszystkie nazwy jedzenia we wszystkich przypadkach.
 */
public mixed query_nazwy_jedzenia()
{
    return nazwaj;
}

/**
 * @return Zwraca nazwe jedzenia, kt�re znajduje sie w naczyniu w dopelniaczu.
 */
public string
query_nazwa_jedzenia_dop()
{
    return nazwaj[0][PL_DOP];
}

/**
 * @return Zwraca nazwe jedzenia, kt�re znajduje si� w naczyniu w bierniku.
 */
public string
query_nazwa_jedzenia_bie()
{
    return nazwaj[0][PL_BIE];
}

/*
 * Nazwa funkcji : set_opis_jedzenia
 * Opis          : Sluzy do ustawienia dlugiego opisu plynu znajdujacego
 *		   sie akt. w beczulce. Powinien on byc w dopelniaczu.
 *		   Wykorzystywany jest np. w dlugim opisie beczulki.
 * Argumenty     : string - dlugi opis w dopelaniczu.
 */
public void
set_opis_jedzenia(string opis)
{
    opis_jedzenia = opis;
}

/*
 * Nazwa funkcji : query_opis_jedzenia
 * Opis          : Zwraca dlugi opis plynu znajdujacego sie akt. w beczulce,
 *		   w dopelniaczu.
 * Funkcja zwraca: string - patrz wyzej.
 */
public string
query_opis_jedzenia()
{
    return opis_jedzenia;
}

/*
 * Nazwa funkcji : set_vol
 * Opis          : Ustawia ile procent alkoholu jest w cieczy, ktora
 *		   jest w beczulce.
 * Argumenty     : int - procent alkoholu
 */
public void
set_vol(int vol)
{
    procent_alco = vol;
}

/*
 * Nazwa funkcji : query_vol
 * Opis          : Zwraca jak bardzo procentowy jak napoj w beczulce.
 * Funkcja zwraca: int - ile procent alkoholu jest w plynie w beczulce.
 */
public int
query_vol()
{
    return procent_alco;
}

private int
zjedz(string str)
{
    object *obs;
    int ret;

    notify_fail("Zjedz co?\n");

    if (!strlen(str))
        return 0;

    if ((member_array(str, map(nazwaj, &tylko_jeden_przypadek(,PL_BIE))) >= 0) ||
        str ~= short(PL_BIE))
        return this_object()->zjedz_to(TP);

    return 0;
}

public int
zjedz_to()
{

    string str;
    int kes;
    if (!ilosc_jedzenia)
    {
        write(capitalize(query_nazwa(PL_MIA)) + " jest zupe^lnie pust" +
            koncowka("y", "a", "e") + ".\n");
        return 1;
    }

    kes = MIN(TP->drink_max() / 20, TP->drink_max() - TP->query_soaked());
    if (!kes)
    {
        write("Nie wmusisz w siebie ani odrobiny wi^ecej.\n");
        return 1;
    }

    kes=ilosc_jedzenia;

    if (!TP->eat_food(kes, 1))//jesli nie daje rady w calosci zjesc
    {   //Vera
        if(TP->query_stuffed()>=(TP->query_prop(LIVE_I_MAX_EAT)-100))
        {
            write("Nie wmusisz w siebie ani odrobiny wi^ecej.\n");
            return 1;
        }
        else
        {
            string str_ile;
            int roznica;
            roznica=(TP->query_prop(LIVE_I_MAX_EAT))-(TP->query_stuffed());
            ilosc_jedzenia-=roznica;

            switch(100 * ilosc_jedzenia / pojemnosc_naczynia)
            {
                case 0:        str_ile="wi�kszo��";    break;
                case 1..20:    str_ile="wi�kszo��";    break;
                case 21..50:   str_ile="sporo";        break;
                case 51..70:   str_ile="troch�";       break;
                case 71..90:   str_ile="ma�� ilo��";   break;
                default:       str_ile="pewn� ilo��";  break;
            }
            TP->set_stuffed(TP->query_stuffed()+roznica);
            write("Zjadasz "+str_ile+" "+nazwaj[0][0][PL_DOP]+ ", pozostawiaj�c "+
                "reszt�.\n");
            saybb(QCIMIE(TP, PL_MIA) + " zjada "+str_ile+" "+
            short(PL_DOP) + ", pozostawiaj�c reszt�.\n");

            if(ilosc_jedzenia<=0) // just in case ;)
            {
                ilosc_jedzenia=0;
                opis_jedzenia = "";
                nazwaj = ({});
                oblicz_sh_po_wypiciu();
            }

            return 1;
        }
        write("Nie wmusisz w siebie ani odrobiny wi^ecej.\n");
        return 1;
    }

    ilosc_jedzenia -= kes;

    if (ilosc_jedzenia<=0)
    {
        str = ", opr^o^zniaj^ac " + koncowka("go", "j^a", "je") + " zupe^lnie.\n";
        ilosc_jedzenia=0;
    }

    TP->eat_food(kes, 0);

    write("Zjadasz " +short(PL_BIE)+ str);
    saybb(QCIMIE(TP, PL_MIA) + " zjada " + short(PL_BIE) + str);

    if (ilosc_jedzenia<=0)
    {
        ilosc_jedzenia = 0;
        opis_jedzenia = "";
        nazwaj = ({});
        oblicz_sh_po_wypiciu();
    }

    return 1;
}

void
oblicz_sh_po_wypiciu()
{
    string *kon=({});
    string *kon2=({});
    string *lp=({});
    string *lm=({});
    int i;
    kon+=({koncowka("y","a","e")});
    kon+=({koncowka("ego","ej","ego")});
    kon+=({koncowka("emu","ej","emu")});
    kon+=({koncowka("y","a","e")});
    kon+=({koncowka("ym","a","ym")});
    kon+=({koncowka("ym","ej","ym")});

    kon2+=({koncowka("e","e","e")});
    kon2+=({koncowka("ych","ych","ych")});
    kon2+=({koncowka("ym","ym","ym")});
    kon2+=({koncowka("e","e","e")});
    kon2+=({koncowka("ymi","ymi","ymi")});
    kon2+=({koncowka("ymi","ymi","ymi")});

    for(i=0;i<6;i++)
    {
        lp+=({"pust"+kon[i]+" "+query_nazwa(i)});
        dodaj_przym("pusty","pu�ci"); //chyba dobrze, co?
    }
    for(i=0;i<6;i++)
    {
        lm+=({"pust"+kon2[i]+" "+query_pnazwa(i)});
        dodaj_przym("pusty","pu�ci"); //chyba dobrze, co?
    }
    ustaw_shorty(lp,lm);
}

int
rozbij(string str)
{
    notify_fail("Rozbij co?\n");

    object nacz;
    if (!parse_command(str, TP, "%o:" + PL_BIE, nacz))
        return 0;
    if (nacz != TO)
        return 0;

    unwield_me();
    if (!ilosc_jedzenia)
    {
        write("Ciskasz " + query_nazwa(PL_NAR) +
            " o ziemi^e rozbijaj^ac " + koncowka("go", "j^a", "je") + " na kawa^lki.\n");
        say(QCIMIE(TP, PL_MIA) + " ciska " +
            query_nazwa(PL_NAR) + " o ziemi^e rozbijaj^ac " + koncowka("go", "j^a", "je") +
            " na kawa^lki.\n");
    }
    else
    {
        if(ENV(TP)->query_prop(ROOM_I_INSIDE))
        {
            write("Ciskasz " + query_nazwa(PL_NAR) + " o ziemi^e rozbijaj^ac " +
                koncowka("go", "j^a", "je") + " na kawa^lki, a przy okazji widowiskowo ^swini^ac "+
                "pod^log^e resztkami "+nazwaj[0][0][PL_DOP]+".\n");
            say(QCIMIE(TP, PL_MIA) + " ciska " + query_nazwa(PL_NAR) +
                " o ziemi^e rozbijaj^ac " + koncowka("go", "j^a", "je") + " na kawa^lki, a przy "+
                "okazji widowiskowo ^swini^ac pod^log^e resztkami "+nazwaj[0][0][PL_DOP]+".\n");
        }
        else
        {
            write("Ciskasz "+query_nazwa(PL_NAR)+" o ziemi� rozbijaj^ac " +
                koncowka("go", "j^a", "je") + " na kawa^lki, przy okazji rozrzucaj�c"
                +" resztki "+nazwaj[0][0][PL_DOP]+" po ca�ej okolicy.\n");

            say(QCIMIE(TP, PL_MIA) + " ciska " + query_nazwa(PL_NAR) +
                " o ziemi^e rozbijaj^ac " + koncowka("go", "j^a", "je") + " na kawa^lki, przy "+
                "okazji rozrzucaj�c resztki "+nazwaj[0][0][PL_DOP]+" po ca�ej okolicy.\n");
        }
    }
    remove_object();

    return 1;
}

int
zle (string str)
{
    write("Najpierw " + koncowka("go", "j^a", "je") + " chwy^c!\n");
    return 1;
}


public int
pomoc(string str)
{
    object nacz;

    notify_fail("Nie ma pomocy na ten temat.\n");

    if (!str)
        return 0;

    if (!parse_command(str, TP, "%o:" + PL_MIA, nacz))
        return 0;

    if (nacz != this_object())
        return 0;

    write("Dla "+query_nazwa(PL_DOP)+" "+nazwaj[0][0][PL_DOP]+" dost�pne s� komendy:\n"+
        "rozbij, spr^obuj, zjedz.\n");

    return 1;
}

public int
is_naczynie_jedzenie()
{
    return 1;
}

public string
init_naczynie_j_arg(string str)
{
#if 0
    string s1, names, s2, patt;
    if(sscanf(str, "%s#PUB_NACZYNIE_P#%s#PUB_NACZYNIE_P#%s", s1, patt, s2) != 3)
        return str;

    string nj;
    if(sscanf(patt, "%d | %d | %d | %s | %s | %s | %s | %s | %s",
        pojemnosc_naczynia, ilosc_jedzenia, procent_alco, opis_jedzenia, nazwaj[0][PL_DOP],
        nazwa_bie, dlugi_opis, nj, names) != 9)
    {
        return str;
    }

    if(stringp(names))
    {
        int r;
        string *p, *m;
        mixed stmp;

        stmp = explode(names, "<>");
        p = explode((string)stmp[0], "&");
        if(!intp(stmp[1]))
        {
            m = explode((string)stmp[1], "&");
            r = (int)stmp[2];
            ustaw_nazwe(p, m, r);
        }
        else
        {
            r = (int)stmp[1];
            ustaw_nazwe(p, r);
        }
    }
    odmien_short();
    odmien_plural_short();

    return s1 + s2;
#else
    return 0;
#endif
}

public string
query_auto_load()
{
#if 0
    return ::query_auto_load() +
        "#PUB_NACZYNIE_P#" + pojemnosc_naczynia + " | " + ilosc_jedzenia + " | " + procent_alco + " | " +
        opis_jedzenia + " | " + nazwaj[0][PL_DOP] + " | " + nazwa_bie + " | " + dlugi_opis + " | " + nazwa_jedzenia + " | " +
        query_nazwa(0) + "&" + query_nazwa(1) + "&" + query_nazwa(2) + "&" +
        query_nazwa(3) + "&" + query_nazwa(4) + "&" + query_nazwa(5) + "<>" +
        (query_tylko_mn() ? "" :
        query_pnazwa(0) + "&" + query_pnazwa(1) + "&" + query_pnazwa(2) + "&" +
        query_pnazwa(3) + "&" + query_pnazwa(4) + "&" + query_pnazwa(5) + "<>") +
        query_rodzaj() + "#PUB_NACZYNIE_P#";
#else
    return 0;
#endif
}

public string
init_arg(string arg)
{
#if 0
    return init_naczynie_j_arg(::init_arg(arg));
#else
    return ::init_arg(arg);
#endif
}
