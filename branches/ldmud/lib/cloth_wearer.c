
#include <macros.h>
#include <wa_types.h>
#include <rozmiary.h>
#include <stdproperties.h>
#include "/config/login/login.h"

/*
 * tablica trzymaj�ca wszystkie rozmiary osoby
 */

float* rozmiary;

/*
 * Nazwa funkcji: zrandomizuj_liczbe
 * Opis: Zwraca pierwszy argument poprawiony z pewnym prawdopodobie�stwem.
 * Argumenty: liczba - liczba do poprawienia
 *            procent - procent o jaki nale�y poprawi� liczb�
 * Zwraca: Pierwszy argument poprawiony z pewnym prawdopodobie�stwem.
 */

static float
zrandomizuj_liczbe(float liczba, int procent = 10)
{
    if (procent <= 0)
        return liczba;

    return liczba * (itof(100 + random(2*procent+1) - procent) / 100.0);
}

/*
 * Nazwa funkcji: wylicz_rozmiary
 * Opis: Wylicza rozmiary na podstawie wysokosci i wagi osoby.
 */

void
wylicz_rozmiary()
{
    string race;
    int gender;
    float *wspolczynniki, wzrost, waga, wspOtyl, mnoznikH, mnoznikO;

    /* rasa */
    race = this_object()->query_race();

    if(!race || !is_mapping_index(race, RACEROZMOD))
        race = TO->query_race_name();

    if(!is_mapping_index(race, RACEROZMOD))
        race = "cz�owiek";

    /* p�e� */
    gender = this_object()->query_gender();

    /* wzrost */
    wzrost = itof(this_object()->query_prop(CONT_I_HEIGHT));
    /* waga */
    waga = itof(this_object()->query_prop(CONT_I_WEIGHT));
    /* wsp�czynnik oty�o�ci to waga (g) przez wzrost (cm)*/
    wspOtyl = waga / wzrost;

    /* wsp�czynniki rozmiar�w w zale�no�ci od rasy i p�ci*/
    wspolczynniki = RACEROZMOD[race][gender];
    if (wspolczynniki == 0)
        return;

    /* wyliczenie mno�nik�w */
    mnoznikH = wzrost / RACEROZMOD[race][2][0];
    mnoznikO = wspOtyl / (RACEROZMOD[race][2][1] / RACEROZMOD[race][2][0]);

    /* wyliczamy po kolei kolejne pola tablicy z rozmiarami */
    rozmiary = ({
        /* 1. obw�d g�owy: (r * H) +/- 10% */
        zrandomizuj_liczbe(wspolczynniki[ROZ_OB_GLOWY] * mnoznikH),
        /* 2. obw�d szyi: (r*(1/H)*O) +/- 10% */
        zrandomizuj_liczbe(wspolczynniki[ROZ_OB_SZYI] * (1.0/mnoznikH) * mnoznikO),
        /* 3. obw�d obr�czy ko�czyny g�rnej: (r * H * O) +/- 10% */
        zrandomizuj_liczbe(wspolczynniki[ROZ_OB_OKG] * mnoznikH * mnoznikO),
        /* 4. d�ugo�� ramienia: (r * H) +/- 10% */
        zrandomizuj_liczbe(wspolczynniki[ROZ_DL_RAMIENIA] * mnoznikH),
        /* 5. obw�d ramienia: (r*(1/H)*O) +/- 10% */
        zrandomizuj_liczbe(wspolczynniki[ROZ_OB_RAMIENIA] * (1.0/mnoznikH) * mnoznikO),
        /* 6. d�ugo�� przedramienia: (r * H) +/- 10% */
        zrandomizuj_liczbe(wspolczynniki[ROZ_DL_PRZEDRAMIENIA] * mnoznikH),
        /* 7. obw�d przedramienia: (r*(1/H)*O) +/- 10% */
        zrandomizuj_liczbe(wspolczynniki[ROZ_OB_PRZEDRAMIENIA] * (1.0/mnoznikH) * mnoznikO),
        /* 8. obw�d nadgarstka: (r*(1/H)*O) +/- 10% */
        zrandomizuj_liczbe(wspolczynniki[ROZ_OB_NADGARSTKA] * (1.0/mnoznikH) * mnoznikO),
        /* 9. d�ugo�� d�oni: (r * H) +/- 10% */
        zrandomizuj_liczbe(wspolczynniki[ROZ_DL_DLONI] * mnoznikH),
        /* 10. szeroko�� d�oni: (r*(1/H)*O) +/- 10% */
        zrandomizuj_liczbe(wspolczynniki[ROZ_SZ_DLONI] * (1.0/mnoznikH) * mnoznikO),
        /* 11. d�ugo�� tu�owia: (r * H) +/- 10% */
        zrandomizuj_liczbe(wspolczynniki[ROZ_DL_TULOWIA] * mnoznikH),
        /* 12. obw�d w klatce piersiowej: (r*(1/H)*O) +/- 10% */
        zrandomizuj_liczbe(wspolczynniki[ROZ_OB_W_KLATCE] * (1.0/mnoznikH) * mnoznikO),
        /* 13. obw�d w pasie: (r*(1/H)*O) +/- 10% */
        zrandomizuj_liczbe(wspolczynniki[ROZ_OB_W_PASIE] * (1.0/mnoznikH) * mnoznikO),
        /* 14. obw�d w biodrach: (r*(1/H)*O) +/- 10% */
        zrandomizuj_liczbe(wspolczynniki[ROZ_OB_W_BIODRACH] * (1.0/mnoznikH) * mnoznikO),
        /* 15. d�ugo�� uda: (r * H) +/- 10% */
        zrandomizuj_liczbe(wspolczynniki[ROZ_DL_UDA] * mnoznikH),
        /* 16. obw�d uda: (r*(1/H)*O) +/- 10% */
        zrandomizuj_liczbe(wspolczynniki[ROZ_OB_UDA] * (1.0/mnoznikH) * mnoznikO),
        /* 17. d�ugo�� golenia: (r * H) +/- 10% */
        zrandomizuj_liczbe(wspolczynniki[ROZ_DL_GOLENI] * mnoznikH),
        /* 18. obw�d golenia: (r*(1/H)*O) +/- 10% */
        zrandomizuj_liczbe(wspolczynniki[ROZ_OB_GOLENI] * (1.0/mnoznikH) * mnoznikO),
        /* 19. obw�d kostki: (r*(1/H)*O) +/- 10% */
        zrandomizuj_liczbe(wspolczynniki[ROZ_OB_KOSTKI] * (1.0/mnoznikH) * mnoznikO),
        /* 20. d�ugo�� stopy: (r * H) +/- 10% */
        zrandomizuj_liczbe(wspolczynniki[ROZ_DL_STOPY] * mnoznikH),
        /* 21. szeroko�� stopy: (r*(1/H)*O) +/- 10% */
        zrandomizuj_liczbe(wspolczynniki[ROZ_SZ_STOPY] * (1.0/mnoznikH) * mnoznikO),
    });
}

/**
 * @return Zwraca tablic� rozmiar�w. Je�eli jej nie ma, to najpierw j� wylicza.
 */
float*
pobierz_rozmiary()
{
    if (!pointerp(rozmiary))
        wylicz_rozmiary();

    return rozmiary;
}

/**
 * Wylicza aktualne rozmiary osoby potrzebne do sprawdzenia, czy da si� za�o�y�
 * kolejne ubranie.
 *
 * @return tablica z rozmiarami.
 */
float*
oblicz_rozmiary_osoby()
{
    float* tmp;
    int i, il, size;
    mixed tmptab;
    mixed tmptooltab;

    tmp = ({});
    tmp += this_player()->pobierz_rozmiary();

    if (!pointerp(tmp))
        return 0;

    for (il = TS_HEAD; il <= MAX_TS; il <<= 1)
    {
        tmptab = this_player()->query_armour(il);
        tmptooltab = this_player()->query_slot(il);
        if (pointerp(tmptab)) {
            if (pointerp(tmptooltab))
            {
                tmptooltab -= tmptab;
            }
            size = sizeof(tmptab);
            switch (il)
            {
                case TS_HEAD:
                    if (tmp[ROZ_OB_GLOWY] == -1.0)
                        break;
                    if (sizeof(tmptooltab))
                    {
                        tmp[ROZ_OB_GLOWY] = -1.0;
                        break;
                    }
                    tmp[ROZ_OB_GLOWY] += itof(sizeof(tmptab) * 4);
                    foreach (object ob : tmptab)
                        tmp[ROZ_OB_GLOWY] += ob->query_prop(ARMOUR_F_POW_GLOWA) ?: 0.0;
                    break;
                case TS_NECK:
                    if (tmp[ROZ_OB_SZYI] == -1.0)
                        break;
                    if (sizeof(tmptooltab))
                    {
                        tmp[ROZ_OB_SZYI] = -1.0;
                        break;
                    }
                    tmp[ROZ_OB_SZYI] += itof(sizeof(tmptab) * 4);
                    foreach (object ob : tmptab)
                        tmp[ROZ_OB_SZYI] += ob->query_prop(ARMOUR_F_POW_SZYJA) ?: 0.0;
                    break;
                case TS_R_SHOULDER:
                case TS_L_SHOULDER:
                    if (tmp[ROZ_OB_OKG] == -1.0)
                        break;
                    if (sizeof(tmptooltab))
                    {
                        tmp[ROZ_OB_OKG] = -1.0;
                        break;
                    }
                    tmp[ROZ_OB_OKG] += itof(sizeof(tmptab) * 4);
                    foreach (object ob : tmptab)
                            tmp[ROZ_OB_OKG] += ob->query_prop(ARMOUR_F_POW_BARKI) ?: 0.0;
                    break;
                case TS_CHEST:
                    if (tmp[ROZ_OB_W_KLATCE] == -1.0)
                        break;
                    if (sizeof(tmptooltab))
                    {
                        tmp[ROZ_OB_W_KLATCE] = -1.0;
                        break;
                    }
                    tmp[ROZ_OB_W_KLATCE] += itof(sizeof(tmptab) * 4);
                    foreach (object ob : tmptab)
                        tmp[ROZ_OB_W_KLATCE] += ob->query_prop(ARMOUR_F_POW_PIERSI) ?: 0.0;
                    break;
                case TS_STOMACH:
                    if (tmp[ROZ_OB_W_PASIE] == -1.0)
                        break;
                    if (sizeof(tmptooltab))
                    {
                        tmp[ROZ_OB_W_PASIE] = -1.0;
                        break;
                    }
                    tmp[ROZ_OB_W_PASIE] += itof(sizeof(tmptab) * 4);
                    foreach (object ob : tmptab)
                        tmp[ROZ_OB_W_PASIE] += ob->query_prop(ARMOUR_F_POW_PAS) ?: 0.0;
                    break;
                case TS_R_ARM:
                case TS_L_ARM:
                    if (tmp[ROZ_OB_RAMIENIA] == -1.0)
                        break;
                    if (sizeof(tmptooltab))
                    {
                        tmp[ROZ_OB_RAMIENIA] = -1.0;
                        break;
                    }
                    tmp[ROZ_OB_RAMIENIA] += itof(sizeof(tmptab) * 4);
                    foreach (object ob : tmptab)
                        tmp[ROZ_OB_RAMIENIA] += ob->query_prop(ARMOUR_F_POW_OB_RAMIE) ?: 0.0;
                    break;
                case TS_R_FOREARM:
                case TS_L_FOREARM:
                    if (tmp[ROZ_OB_PRZEDRAMIENIA] == -1.0)
                        break;
                    if (sizeof(tmptooltab))
                    {
                        tmp[ROZ_OB_PRZEDRAMIENIA] = -1.0;
                        break;
                    }
                    tmp[ROZ_OB_PRZEDRAMIENIA] += itof(sizeof(tmptab) * 4);
                    foreach (object ob : tmptab)
                        tmp[ROZ_OB_PRZEDRAMIENIA] += ob->query_prop(ARMOUR_F_POW_OB_PRAMIE) ?: 0.0;
                    break;
                case TS_R_HAND:
                case TS_L_HAND:
                    if ((tmp[ROZ_DL_DLONI] == -1.0) && (tmp[ROZ_SZ_DLONI] == -1.0))
                        break;
                    if (sizeof(tmptooltab))
                    {
                        tmp[ROZ_DL_DLONI] = -1.0;
                        tmp[ROZ_SZ_DLONI] = -1.0;
                        break;
                    }
                    tmp[ROZ_DL_DLONI] += itof(sizeof(tmptab));
                    tmp[ROZ_SZ_DLONI] += itof(sizeof(tmptab));
                    foreach (object ob : tmptab)
                    {
                        tmp[ROZ_DL_DLONI] += ob->query_prop(ARMOUR_F_POW_DL_DLON) ?: 0.0;
                        tmp[ROZ_SZ_DLONI] += ob->query_prop(ARMOUR_F_POW_SZ_DLON) ?: 0.0;
                    }
                    break;
                case TS_HIPS:
                    if (tmp[ROZ_OB_W_BIODRACH] == -1.0)
                        break;
                    if (sizeof(tmptooltab))
                    {
                        tmp[ROZ_OB_W_BIODRACH] = -1.0;
                        break;
                    }
                    tmp[ROZ_OB_W_BIODRACH] += itof(sizeof(tmptab) * 4);
                    foreach (object ob : tmptab)
                        tmp[ROZ_OB_W_BIODRACH] += ob->query_prop(ARMOUR_F_POW_BIODRA) ?: 0.0;
                    break;
                case TS_R_THIGH:
                case TS_L_THIGH:
                    if (tmp[ROZ_OB_UDA] == -1.0)
                        break;
                    if (sizeof(tmptooltab))
                    {
                        tmp[ROZ_OB_UDA] = -1.0;
                        break;
                    }
                    tmp[ROZ_OB_UDA] += itof(sizeof(tmptab) * 4);
                    foreach (object ob : tmptab)
                        tmp[ROZ_OB_UDA] += ob->query_prop(ARMOUR_F_POW_OB_UDO) ?: 0.0;
                    break;
                case TS_R_SHIN:
                case TS_L_SHIN:
                    if (tmp[ROZ_OB_GOLENI] == -1.0)
                        break;
                    if (sizeof(tmptooltab))
                    {
                        tmp[ROZ_OB_GOLENI] = -1.0;
                        break;
                    }
                    tmp[ROZ_OB_GOLENI] += itof(sizeof(tmptab) * 4);
                    foreach (object ob : tmptab)
                        tmp[ROZ_OB_GOLENI] += ob->query_prop(ARMOUR_F_POW_OB_GOLEN) ?: 0.0;
                    break;
                case TS_R_FOOT:
                case TS_L_FOOT:
                    if ((tmp[ROZ_DL_STOPY] == -1.0) && (tmp[ROZ_SZ_STOPY] == -1.0))
                        break;
                    if (sizeof(tmptooltab))
                    {
                        tmp[ROZ_DL_STOPY] = -1.0;
                        tmp[ROZ_SZ_STOPY] = -1.0;
                        break;
                    }
                    tmp[ROZ_DL_STOPY] += itof(sizeof(tmptab) * 2);
                    tmp[ROZ_SZ_STOPY] += itof(sizeof(tmptab) * 2);
                    foreach (object ob : tmptab)
                    {
                        tmp[ROZ_DL_STOPY] += ob->query_prop(ARMOUR_F_POW_DL_STOPA) ?: 0.0;
                        tmp[ROZ_SZ_STOPY] += ob->query_prop(ARMOUR_F_POW_SZ_STOPA) ?: 0.0;
                    }
                    break;
            }
        }
    }
    return tmp;
}

public nomask string
query_rozmiary_auto_load()
{
    int first = 1;
    string toReturn = " ~CWR[";

    foreach (float roz : pobierz_rozmiary())
    {
        if (!first)
            toReturn += ",";

        toReturn += sprintf("%f", roz);
        first = 0;
    }

    return toReturn + "]CWR~ ";
}

public nomask string
init_rozmiary_arg(string arg)
{
    string rozTab, toReturn, foobar;
    float* newRoz = ({});
    float oneRoz;

    if (arg == 0)
        return 0;

    if (sscanf(arg, "%s~CWR[%s]CWR~ %s", foobar, rozTab, toReturn) != 3)
        return arg;

    foreach (string elem : explode(rozTab, ","))
    {
        sscanf(elem, "%f", oneRoz);
        newRoz += ({ oneRoz });
    }
    rozmiary = newRoz;

    return foobar + toReturn;
}

public float query_f_size(float *roz, int ciuch)
{
    float *rozn;

    if(!sizeof(roz))
        roz = rozmiary;

    if(ciuch)
        rozn = TO->oblicz_rozmiary_osoby();
    else
        rozn = TO->pobierz_rozmiary();

    int i, ile;
    float factor = 1.0, rozm = 0.0;
    int gender = TO->query_prop(ARMOUR_I_DLA_PLCI);
    string race = TO->query_prop(ARMOUR_S_DLA_RASY);

    if (!stringp(race))
        race = "cz^lowiek";

    int size = (sizeof(roz) > sizeof(rozn) ? sizeof(roz) : sizeof(rozn));
    for (i = 0; i < size; i++)
    {
        if (roz[i] > 0.0)
        {
            ile++;
            rozm += rozn[i] / RACEROZMOD[race][gender][i];
        }
    }

    if(itof(ile) > 0)
        rozm = rozm / itof(ile);

    if (rozm <= 0)
        factor = 1.0;
    else
        factor = rozm;

    return factor;
}

/**
 * Zwraca rozmiar - je�li nie podamy rozmiaru rzeczy z kt�r�
 * chcemy por�wna� zwr�cony zostanie rozmiar ca�ego gracza,
 * je�li podamy to tylko tych sublc�w, kt�re zajmuje dane
 * ubranie.
 *
 * @param roz tablica z rozmiarami obiektu z kt�rym por�wnujemy
 * @param ciuch czy uwzgl�dnia� ciuchy
 *
 * @return rozmiar
 */
public varargs string
query_size(float *roz, int ciuch)
{
    float factor = query_f_size(roz, ciuch);

    if(factor <= 0.5)
        return "XXXXS";
    else if(factor > 0.5 && factor <= 0.6)
        return "XXXS";
    else if(factor > 0.6 && factor <= 0.7)
        return "XXS";
    else if(factor > 0.7 && factor <= 0.8)
        return "XS";
    else if(factor > 0.8 && factor <= 0.9)
        return "S";
    else if(factor > 0.9 && factor <= 1.0)
        return "M";
    else if(factor > 1.0 && factor <= 1.1)
        return "L";
    else if(factor > 1.1 && factor <= 1.2)
        return "XL";
    else if(factor > 1.2 && factor <= 1.3)
        return "XXL";
    else if(factor > 1.3 && factor <= 1.4)
        return "XXXL";
    else
        return "XXXXL";
}