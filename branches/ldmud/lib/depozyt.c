/**
 * \file /lib/depozyt.c
 *
 * Wszelkiego rodzaju depozyty zostawiane w bankach i nie tylko.
 *
 * @author Krun
 * @date 10 Stycze� 2008
 */

inherit "/lib/trade.c";

#include <files.h>
#include <colors.h>
#include <macros.h>
#include <cmdparse.h>

#define DEP_SIZE                10
#define MAX_DEP_SIZE            200

#define DEPOZYT_SKRZYNKA        "skrzynka"
#define DEPOZYT_POBRANO         "pobrano"
#define DEPOZYT_KOD             "kod"

#define RODZ_PRZYMS             0
#define RODZ_OPIS               1
#define RODZ_NAZWA              2
#define RODZ_WIELKOSC           3

#define SKRZYNKA_DEPOZYTOWA     "/d/Standard/obj/skrzynka_depozytowa.c"

#define DEPOZYT_SAVE(i)          "/d/Standard/saves/depozyty/" + implode(explode(MASTER_OB(TO)+"/", "/")[1..], "_") +  \
    "/" + i

#define RESETOWAL_DEPOZYT       "_resetowal_depozyt"

int         dep_koszt_zalozenia,            /* Koszt za�o�enia skrzynki depozytowej (0 je�li za free) */
            dep_koszt_skorzystania,         /* Koszt skorzystania ze skrzynki depozytowej (0 je�li za free) */
            dep_koszt_powiekszenia,         /* Koszt powi�kszenia skrzynki depozytowej (0 je�li za free) */
            dep_koszt_zlikwidowania;        /* Koszt zlikwidowania skrzynki depozytowej (0 je�li za free) */

mapping     rodzaje_skrzynek = ([]);        /* Rodzaje skrzynek depozytowych */

/** ****************************************************************************** **
 ** *******************      U  S  T  A  W  I  E  N  I  A     ******************** **
 ** ****************************************************************************** **/

/**
 * Ustawiamy koszt zalo�enia skrzynki depozytowej w groszach.
 * Je�li nie jest ustawiony koszt powi�kszenia to funkcja ustawia
 * go na pow�ow� warto�ci kosztu za�o�enia.
 *
 * @param k ustawiany koszt
 */
public void ustaw_koszt_zalozenia_skrzynki_depozytowej(int k)
{
    dep_koszt_zalozenia = k;
    dep_koszt_powiekszenia = k/2 || dep_koszt_powiekszenia;
}

/**
 * @return Koszt za�o�enia skrzynki depozytowej w groszach.
 */
public int query_koszt_zalozenia_skrzynki_depozytowej() { return dep_koszt_zalozenia; }

/**
 * Ustawiamy koszt skorzystania ze skrzynki depozytowej w groszach.
 *
 * @param k ustawiany koszt
 */
public void ustaw_koszt_skorzystania_ze_skrzynki_depozytowej(int k) { dep_koszt_skorzystania = k; }

/**
 * @return Koszt skorzystania ze skrzynki depozytowej w groszach.
 */
public int query_koszt_skorzystania_ze_skrzynki_depozytowej() { return dep_koszt_skorzystania; }

/**
 * Ustawiamy koszt powi�kszenia skrzynki depozytowej w groszach.
 *
 * @param k ustawiany koszt
 */
public void ustaw_koszt_powiekszenia_skrzynki_depozytowej(int k) { dep_koszt_powiekszenia = k; }

/**
 * @return Koszt powiekszenia skrzynki depozytowej w groszach.
 */
public int query_koszt_powiekszenia_skrzynki_depozytowej() { return dep_koszt_powiekszenia; }

/**
 * Ustawiamy koszt zlikwidowania skrzynki depozytowej w groszach.
 *
 * @param k ustawiany koszt
 */
public void ustaw_koszt_zlikwidowania_skrzynki_depozytowej(int k) { dep_koszt_zlikwidowania = k; }

/**
 * @return Koszt zlikwidowania skrzynki depozytowej w groszach.
 */
public int query_koszt_zlikwidowania_skrzynki_depozytowej() { return dep_koszt_zlikwidowania; }

/**
 * Dodajemy nowy rodzaj skrzynki depozytowej w formacie:
 * dodaj_rodzaj_skrzynki(({przymiotniki}), opisy, ({nazwa}));
 *
 * Gdzie
 * @param przymiotniki to tablica z tablicami przymiotnik�w
 * @example
 *  ({"ma�y", "mali", "zielony", "zieloni"})
 * @param opisy opis skrzynki depozytowej
 * @param nazwa ma dok�adnie tak� sam� sk�adnie jak ustaw_nazwe z tym, �e jest w ({ }).
 *              chyba �e podajemy jedynie nazw� w mianowniku i ko�ystamy ze s�ownika.
 * @example
 *  "skrzynka" || ({ ({"skrzynka", "skrzynki", ...}), ({"skrzynki", "skrzynek", ...}), PL_ZENSKI })
 * @param do_wieklosci do jakiej wielko�ci mo�e by� stosowany ten opis
 *
 */
public varargs void dodaj_rodzaj_skrzynki(mixed przymiotniki, string opisy, mixed nazwa, int do_wielkosci = MAX_DEP_SIZE)
{
    int przyp;
    if(stringp(nazwa))
        nazwa = slownik_pobierz(nazwa);

    if(!pointerp(nazwa))
        return;

    int s = m_sizeof(rodzaje_skrzynek);

    while(is_mapping_index(s, rodzaje_skrzynek))
        s++;

    rodzaje_skrzynek[s] = ({ przymiotniki, opisy, nazwa, do_wielkosci });
}

/** ****************************************************************************** **
 ** *********      F  U  N  K  C  J  E   P  O  M  O  C  N  I  C  Z  E   ********** **
 ** ****************************************************************************** **/

public string wylosuj_kod()
{
    string kod = "";

    int max = 10 + random(20);
    for(int i = 0; i < max ; i++)
        kod += itoa(ALPHABET_PL[random(strlen(ALPHABET_PL))]);

    return kod;
}

public int przefiltruj_rodzaje(int r, int s)
{
    if(rodzaje_skrzynek[r][RODZ_WIELKOSC] > s)
        return 1;
}

public int sortuj_rodzaje_skrzynek(int r1, int r2)
{
    return (rodzaje_skrzynek[r1][RODZ_WIELKOSC] > rodzaje_skrzynek[r2][RODZ_WIELKOSC] ?:
        (-(rodzaje_skrzynek[r1][RODZ_WIELKOSC] < rodzaje_skrzynek[r2][RODZ_WIELKOSC]) ?: 0));
}

public object stworz_nowa_skrzynke_depozytowa(string imie)
{
    object skrzyneczka = clone_object(SKRZYNKA_DEPOZYTOWA);

    skrzyneczka->ustaw_rozmiar(DEP_SIZE);
    skrzyneczka->ustaw_wlasciciela(imie);

    int *rs = m_indexes(rodzaje_skrzynek);

    rs = filter(rs, &przefiltruj_rodzaje(,DEP_SIZE));

    if(!rs)
        rs = m_indexes(rodzaje_skrzynek);


    //FIXME: Gdy min zacznie obs�ugiwa� tablice odkomentowa�!
#if 0
    rs = filter(rs, &przefiltruj_rodzaje(,min(map(m_indexes(rodzaje_skrzynek),
        &operator([])(,RODZ_WIELKOSC) ))) );
#else
    rs = sort_array(rs, &sortuj_rodzaje_skrzynek());
#endif

    skrzyneczka->ustaw_rodzaj_skrzynki(rs[random(sizeof(rs))]);

    return skrzyneczka;
}

public object odczytaj_skrzynke_depozytowa(string imie, mapping mp)
{
    object skrzyneczka = clone_object(SKRZYNKA_DEPOZYTOWA);

    string arg = mp[DEPOZYT_SKRZYNKA];
    skrzyneczka->init_arg(arg);

    int r = skrzyneczka->query_rodzaj_skrzynki();

    for(int i = 0; i < sizeof(rodzaje_skrzynek[r][RODZ_PRZYMS]); i+=2)
        skrzyneczka->dodaj_przym(rodzaje_skrzynek[r][RODZ_PRZYMS][i], rodzaje_skrzynek[r][RODZ_PRZYMS][(i+1)]);

    skrzyneczka->set_long(rodzaje_skrzynek[r][RODZ_OPIS]);

    skrzyneczka->ustaw_nazwe(rodzaje_skrzynek[r][RODZ_NAZWA][0], rodzaje_skrzynek[r][RODZ_NAZWA][1],
        rodzaje_skrzynek[r][RODZ_NAZWA][2]);

    skrzyneczka->ustaw_depozyt(MASTER);
    skrzyneczka->ustaw_unikalny_kod_skrzyni(mp[DEPOZYT_KOD]);
    skrzyneczka->odmien_short();

    return skrzyneczka;
}

public void save_depozyt(string imie, mapping mp)
{
    setuid();
    seteuid(getuid());
    set_auth(this_object(), "root:root");

    int fs;
    string dir;
    if((fs=file_size((dir = implode(explode(DEPOZYT_SAVE(imie), "/")[..-2], "/")))) <= 0)
    {
         if(fs != 0)
            rm(dir);

         mkdir(dir);
    }

    if(mappingp(mp))
        save_map(mp, DEPOZYT_SAVE(imie));

    set_auth(TO, getuid() + ":" + getuid());
}

public mapping restore_depozyt(string imie)
{
    if(file_size(add_extension(DEPOZYT_SAVE(imie), ".o")) < 1)
        return 0;

    set_auth(this_object(), "root:root");
    mapping mp = restore_map(DEPOZYT_SAVE(imie));
    set_auth(TO, getuid() + ":" + getuid());

    return mp;
}

public void config_depozyty()
{
    rodzaje_skrzynek = ([]);

    int fs;
    string dir;
    if((fs=file_size((dir = implode(explode(DEPOZYT_SAVE("aa"), "/")[..-2], "/")))) <= 0)
    {
         if(fs != 0)
            rm(dir);

         mkdir(dir);
    }
}

/** ****************************************************************************** **
 ** ***********************      K  O  M  E  N  D  Y     ************************* **
 ** ****************************************************************************** **/

/**
 * Wy�wietla pomoc.
 */
public int depozyt_pomoc(string arg)
{
    if(arg && arg != "depozyt")
        return 0;

    write("Je�li nie masz skrzynki depozytowej mo�esz j� za�o�y�. Gdy oka�e si�, �e ta jest ju� dla ciebie za ma�a " +
        "zawsze mo�esz j�, " + (dep_koszt_powiekszenia ? "za dodatkow� op�at�, " : "") + "powi�kszy�.\n\n" +
        "Aby pobra� swoj� skrzynk� popro� o ni�, za� gdy sko�czysz z niej korzysta� po prostu j� oddaj." +
        (dep_koszt_skorzystania ? " Pami�taj, �e ka�de pobranie uszczupli nieznacznie tw�j bud�et." : "") + "\n\n"+
        "Aby zobaczy� og�ln� pomoc u�yj 'pomoc og�lna'.\n");
    return 1;
}

/**
 * Dzi�ki tej funckji zak�adamy skrzynk� depozytow�.
 */
public int depozyt_zaloz(string arg)
{
    NF("Co chcesz za�o�y�?\n");

    if(wildmatch("skrzynk� *", arg))
        NF2("Jak� skrzynk� chcesz za�o�y�?\n",3);

    int nowa;
    if(arg ~= "now� skrzynk� depozytow�")
    {
        arg = "skrzynk� depozytow�";
        nowa = 1;
    }

    if(!(arg ~= "skrzynk� depozytow�"))
        return 0;

    string imie = TP->query_real_name();

    mapping mp = restore_depozyt(imie);

    if(!mp || !mappingp(mp))
        nowa = 0;

    if(mp && mappingp(mp) && !nowa)
    {
        NF2("Ty ju� masz tu skrzynk� depozytow�.\n",5);
        return 0;
    }
    else if(nowa && !TP->query_prop(RESETOWAL_DEPOZYT))
    {
        NF("Czy jeste� pewien, �e chcesz zlikwidowa� stare i za�o�y� nowe konto?\n"+
            "Wywo�aj komend� jeszcze raz aby potwierdzi�.\n");
        TP->add_prop(RESETOWAL_DEPOZYT, 1);
        return 0;
    }
    TP->remove_prop(RESETOWAL_DEPOZYT);

    if(dep_koszt_zalozenia)
    {
        dep_koszt_zalozenia *= (nowa ? 2 : 1);
        if(!can_pay(dep_koszt_zalozenia, TP))
        {
            NF2("Nie sta� ci� na za�o�enie skrzynki depozytowej.\n", 5);
            return 0;
        }

        mixed r = pay(dep_koszt_zalozenia, TP);

        write("P�acisz " + text(split_values(dep_koszt_zalozenia), PL_BIE) +
            (nowa ? ", likwidujesz star� i zak�adasz now�" : " i zak�adasz") + " " +
            "skrzynk� depozytow�.\n");
        saybb(QCIMIE(TP, PL_MIA) + " p�aci za za�o�enie skrzynki depozytowej.\n");
    }
    else
    {
        write("Zak�adasz skrzynk� depozytow�.\n");
        saybb(QCIMIE(TP, PL_MIA) + " zak�ada skrzynk� depozytow�.\n");
    }

    if(!nowa)
        rm(add_extension(DEPOZYT_SAVE(imie), ".o"));

    object skrzyneczka = stworz_nowa_skrzynke_depozytowa(imie);
    skrzyneczka->ustaw_rozmiar(DEP_SIZE);
    mp = ([DEPOZYT_SKRZYNKA : skrzyneczka->query_auto_load(), DEPOZYT_KOD : wylosuj_kod()]);
    skrzyneczka->remove_object();

    save_depozyt(imie, mp);

    return 1;
}

/**
 * Funkcja pozwalajaca na powi�kszenie skrzyneczki depozytowej.
 */
public int depozyt_powieksz(string arg)
{
    NF("Co chcesz powi�kszy�?\n");

    if(wildmatch("*skrzynk� *", arg))
        NF("Jak� skrzynk� chcesz powi�kszy�?\n");

    if(!(arg ~= "skrzynk� depozytow�") && !(arg ~= "swoj� skrzynk� depozytow�"))
        return 0;

    string imie = TP->query_real_name();

    mapping mp = restore_depozyt(imie);

    if(!mp || !mappingp(mp))
    {
        NF("Nie masz jeszcze skrzynki depozytowej. Wpierw j� za��.\n");
        return 0;
    }

    object skrzyneczka = odczytaj_skrzynke_depozytowa(imie, mp);

    if(skrzyneczka->query_rozmiar() >= MAX_DEP_SIZE)
    {
        NF("Przykro mi, ale ty masz ju� skrzynk� maksymalnych rozmiar�w.\n");
        return 0;
    }

    if(dep_koszt_powiekszenia)
    {
        if(!can_pay(dep_koszt_powiekszenia, TP))
        {
            NF2("Nie sta� ci� na powi�kszenie skrzynki depozytowej.\n", 5);
            return 0;
        }

        mixed r = pay(dep_koszt_powiekszenia, TP);

        write("P�acisz " + text(split_values(dep_koszt_powiekszenia), PL_BIE) + " i powi�kszasz " +
            "swoja skrzynk� depozytow�.\n");
        saybb(QCIMIE(TP, PL_MIA) + " p�aci za powi�kszenie swojej skrzynki depozytowej.\n");
    }
    else
    {
        write("Powi�kszasz swoj� skrzynk� depozytow�.\n");
        saybb(QCIMIE(TP, PL_MIA) + " powi�ksza swoj� skrzynk� depozytow�.\n");
    }

    skrzyneczka->ustaw_rozmiar(min(MAX_DEP_SIZE, skrzyneczka->query_rozmiar() + DEP_SIZE));
    mp = ([DEPOZYT_SKRZYNKA : skrzyneczka->query_auto_load()]);
    skrzyneczka->remove_object();

    save_depozyt(imie, mp);

    return 1;
}

/**
 * Dzi�ki tej funckji likwidujemy skrzynk� depozytow�.
 */
public int depozyt_zlikwiduj(string arg)
{
    NF("Co chcesz zlikwidowa�?\n");

    if(wildmatch("*skrzynk� *", arg))
        NF("Jak� skrzynk� chcesz zlikwidowa�?\n");

    if(!(arg ~= "skrzynk� depozytow�") && !(arg ~= "swoj� skrzynk� depozytow�"))
        return 0;

    string imie = TP->query_real_name();

    mapping mp = restore_depozyt(imie);

    if(!mp || !mappingp(mp))
    {
        NF("Nie masz tu skrzynki depozytowej.\n");
        return 0;
    }

    if(mp[DEPOZYT_POBRANO])
    {
        NF2("Nie mo�esz zlikwidowa� skrzynki depozytowej je�li masz j� pobran�.\n", 5);
        return 0;
    }

    if(dep_koszt_zlikwidowania)
    {
        if(!can_pay(dep_koszt_zlikwidowania, TP))
        {
            NF2("Nie sta� ci� na zlikwidowanie skrzynki depozytowej.\n", 5);
            return 0;
        }

        mixed r = pay(dep_koszt_zlikwidowania, TP);

        write("P�acisz " + text(split_values(dep_koszt_zlikwidowania), PL_BIE) + " za likwidacj� swojej " +
            "skrzynki depozytowej.\n");
        saybb(QCIMIE(TP, PL_MIA) + " p�aci za likwidacj� swojej skrzynki depozytowej.\n");
    }
    else
    {
        write("Likwidujesz swoj� skrzynk� depozytow�.\n");
        saybb(QCIMIE(TP, PL_MIA) + " likwiduje swoj� skrzynk� depozytow�.\n");
    }

    rm(add_extension(DEPOZYT_SAVE(imie), ".o"));

    return 1;
}

/**
 * Dzi�ki tej funckji prosimy o swoj� skrzynk� depozytow�.
 */
public int depozyt_pobierz(string arg)
{
    if(query_verb() ~= "popro�")
    {
        NF("O co chcesz poprosi�?\n");

        if(wildmatch("o* skrzynk� *", arg))
            NF("O jak� skrzynk� chcesz poprosi�?\n");

        if(!(arg ~= "o skrzynk� depozytow�") && !(arg ~= "o swoj� skrzynk� depozytow�"))
            return 0;
    }
    else if(query_verb() ~= "pobierz")
    {
        NF("Co chcesz pobra�?\n");

        if(wildmatch("*skrzynk� *", arg))
            NF("Jak� skrzynk� chcesz pobra�?\n");

        if(!(arg ~= "skrzynk� depozytow�") && !(arg ~= "swoj� skrzynk� depozytow�"))
            return 0;
    }

    string imie = TP->query_real_name();

    mapping mp = restore_depozyt(imie);

    if(!mp || !mappingp(mp))
    {
        NF("Nie masz tu jeszcze skrzynki depozytowej. Aby o ni� poprosi� musisz j� " +
            "wcze�niej za�o�y�.\n");
        return 0;
    }

    if(mp[DEPOZYT_POBRANO])
    {
        NF("Ju� wcze�niej pobra�e� swoj� skrzynk�. Je�li Ci gdzie� zgine�a za�� now�.\n");
        return 0;
    }

    if(dep_koszt_skorzystania)
    {
        if(!can_pay(dep_koszt_skorzystania, TP))
        {
            NF2("Nie masz czym zap�aci� za pobranie swojej skrzynki depozytowej.\n", 5);
            return 0;
        }

        mixed r = pay(dep_koszt_skorzystania, TP);

        write("P�acisz " + text(split_values(dep_koszt_skorzystania), PL_BIE) + " i pobierasz swoj� " +
            "skrzynk� depozytow�.\n");

        saybb(QCIMIE(TP, PL_MIA) + " p�aci i pobiera swoj� skrzynk� depozytow�.\n");
    }
    else
    {
        write("Pobierasz swoj� skrzynk� depozytow�.\n");
        saybb(QCIMIE(TP, PL_MIA) + " pobiera swoj� skrzynk� depozytow�.\n");
    }

    object skrzyneczka = odczytaj_skrzynke_depozytowa(imie, mp);
    skrzyneczka->move(TP, 1);
    mp[DEPOZYT_POBRANO] = 1;

    save_depozyt(imie, mp);

    return 1;
}

/**
 * Dzi�ki tej funckji prosimy o swoj� skrzynk� depozytow�.
 */
public int depozyt_oddaj(string arg)
{
    if(query_verb() ~= "zwr��")
        NF("Co chcesz zwr�ci�?\n");
    else if(query_verb() ~= "oddaj")
        NF("Co chcesz odda�?\n");

    if(!arg)
        return 0;

    object *s;
    if(!parse_command(arg, AI(TP), "%i:" + PL_BIE, s))
        return 0;

    s = NORMAL_ACCESS(s, 0, 0);

    if(!sizeof(s))
        return 0;

    if(sizeof(s) > 1)
    {
        NF("Oddawaj pojedy�czo.\n");
        return 0;
    }

    object skrzyneczka = s[0];

    string imie = TP->query_real_name();

    mapping mp = restore_depozyt(imie);

    if(!mp || !mappingp(mp))
    {
        //W jaki� spos�b gracz dosta� skrzynk� skoro j� ma!
        //I skoro jest st�d, znaczy, �e straci� w skutek b�edu.
        write(set_color(TP, COLOR_FG_RED, COLOR_BOLD_ON) + "Wyst�pi� b��d. Tw�j depozyt "+
            "w bazie zosta� skasowany. Nie martw si�, zosta� w�a�nie odczytany z tego co "+
            "mia�e� w skrzynce, jednak�e prosz� 'zg�o� globalny b��d' opisuj�c okoliczno�ci w " +
            "jakich pojawi� Ci si� ten komunikat." + clear_color(TP) + "\n");
    }

    write("Oddajesz swoj� skrzynk� depozytow�.\n");
    saybb(QCIMIE(TP, PL_MIA) + " oddaje swoj� skrzynk� depozytow�.\n");

    mp[DEPOZYT_SKRZYNKA] = skrzyneczka->query_auto_load();
    mp[DEPOZYT_POBRANO] = 0;

    skrzyneczka->remove_object();

    save_depozyt(imie, mp);

    return 1;
}

public void init_depozyt()
{
    add_action(depozyt_pobierz,     "pobierz");
    add_action(depozyt_pobierz,     "popro�");
    add_action(depozyt_oddaj,       "zwr��");
    add_action(depozyt_oddaj,       "oddaj");
    add_action(depozyt_zaloz,       "za��");
    add_action(depozyt_powieksz,    "powi�ksz");
    add_action(depozyt_zlikwiduj,   "zlikwiduj");
    add_action(depozyt_pomoc,       "pomoc");
    add_action(depozyt_pomoc,       "?", 1);
}