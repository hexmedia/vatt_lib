/**
 *   \file /lib/sklep.c
 *
 *   Plik napisa� Molder.
 *
 *   Ten plik mog� dziedziczy� pomieszczenia sklepowe, 
 *   dzi�ki niemu graczowi wy�wietli si� �adny komunikat 
 *   gdy z jakiego� powodu w okolicy nie b�dzie sklepikarza.
 *   Udost�pnia te� sklepow� tabliczk�.
 *
 *   Typowe u�ycie tego pliku:
 *
 *   inherit "/lib/sklep.c";
 *
 *   init() {
 *       ::init();
 *       init_sklep();
 *       ...
 *   }
 *
 *   Nast�pnie mo�na nadpisa� funkcj�:
 *   shop_hook_brak_sklepikarza()
 */

#pragma strict_types
#pragma save_binary

#include <stdproperties.h>
#include <macros.h>
#include <language.h>
#include <cmdparse.h>
#include <composite.h>

/**
 * Wywo�ana gdy w pobli�u nie ma sklepikarza, a gracz
 * u�y� jakiej� sklepowej komendy. Ustawia notify_fail()
 * kt�ry si� graczowi wy�wietli.
*/
void
shop_hook_brak_sklepikarza()
{
    notify_fail("Niestety nie ma tu nikogo z obs�ugi.\n");
}

/**
 * Sprawdza czy na lokacji jest handlarz (kto� z query_handlarz),
 * je�li nie ma ustawia notify_fail().
 * @return zawsze 0  
 */
int
sprawdz()
{
    object *all;
    int i,size;

    if(sizeof(filter(all_inventory(environment(this_player())), &->query_handlarz())) != 0)
        return 0;

    shop_hook_brak_sklepikarza();
    return 0;
}

/**
 * Funkcja do wywo�ania w init(), dodaje sklepowe komendy:
 * kup, sprzedaj, wyce�, poka�, przejrzyj, store. 
 * Je�li nie ma w pobli�u �adnego sklepikarza wywo�ywana
 * jest shop_hook_brak_sklepikarza().
 */
void
init_sklep()
{
    add_action(sprawdz, "kup");
    add_action(sprawdz, "sprzedaj");
    add_action(sprawdz, "wyce�");
    add_action(sprawdz, "poka�");
    add_action(sprawdz, "przejrzyj");
}

/**
 * Funkcja zwraca opis do tabliczki sklepowej.
 * @return  message string
 */
string
standard_shop_sign()
{
    return
"Oto przyk�ady tego, co mo�esz tu uczyni�\n" +
"    kup miecz za korony    (standardowo najmniejsza denominacja)\n" +
"    kup miecz za korony i we� grosze reszty\n" +
"    sprzedaj miecz za grosze  (standardowo najwi�ksza denominacja)\n" +
"    sprzedaj wszystko  - dzi�ki czemu sprzedasz wszystkie przedmioty, poza\n"+
"                         tymi, kt�re dzier�ysz lub masz na sobie. Pami�taj,\n"+
"                         �e gdy masz du�o przedmiot�w do sprzedania, by�\n"+
"                         mo�e zajdzie konieczno�� ponownego wykonania tej\n"+
"                         komendy.\n" +
"    sprzedaj wszystko! - pozwoli ci sprzedac WSZYSTKIE przedmioty, kt�re\n"+
"                         masz przy sobie - za wyj�tkiem tych, kt�rych nie\n" +
"                         da si� od�o�y� i z wyj�tkiem monet.\n"+
"                         (patrz ostrze�enie dla 'sprzedaj wszystko')\n"+
"    sprzedaj miecze, sprzedaj drugi miecz, sprzedaj dwa miecze - te komendy\n"+
"                         tak�e dzia�aj�. Pami�taj jednak, �e nigdy\n" +
"                         nie b�dziesz w stanie kupi� wi�cej, ni� jednego\n"+
"                         przedmiotu.\n" +
"    wyce�              - Sprzedawca wyceni ile wart jest dany przedmiot,\n"+
"                         zanim zdecydujesz na jego sprzeda�.\n" +
"    poka�              - Pozwoli ci obejrze� dok�adnie przedmiot b�d�cy\n"+
"                         na sprzeda�.\n"+
"    przejrzyj          - Pozwala ci przejrze� zawarto�� magazynu. Mo�esz\n"+
"                         te� 'przejrze� zbroje' i 'przejrze� bronie'.\n";
}