/*
 * /cmd/std/tracer_tool_base.c
 * 
 * The basic tracer tool functions used in the commons soul for
 * parsing arguments to certain functions.
 *
 * TODO:
 * - Trzeba doda� mo�liwo�� zagnie�d�ania tablic i mapping�w w sobie, narazie to nie dzia�a.
 */

#pragma no_clone
#pragma save_binary
#pragma strict_types

inherit "/cmd/std/command_driver";

#include <pl.h>
#include <macros.h>
#include <cmdparse.h>
#include <filepath.h>

#define TRACER_STORES	"_tracer_stores"
#define TRACER_VARS	"_tracer_vars"

/*
 * Function name: find_item
 * Description  : This function will try to find the object the wizard
 *                points at with the string argument.
 * Arguments    : object prev - the previous object found in the chain.
 *                string str  - the string to parse.
 * Returns      : object - the object described with 'str', if any.
 */
object
find_item(object prev, string str)
{
    object ob;
    object *ob_list;
    string tmp;
    int    i;

    if ((str == "here") || (str == "!") || (str == "tu"))
    {
	return environment(this_interactive());
    }

    if (str == "me" || str == "ja") 
    {
	return this_interactive();
    }

    if (str == "^")
    {
	return environment(prev);
    }

    if (sscanf(str, "@%s", tmp) == 1)
    {
	return find_living(tmp);
    }

    if (sscanf(str, "*%s", tmp) == 1)
    {
	return find_player(tmp);
    }

    if (sscanf(str, "$%d", i) == 1)
    {
	ob_list = users();
	write("size: " + sizeof(ob_list) + "\n");
	if ((i >= sizeof(ob_list)) ||
	    (i < 0))
	{
	    return 0;
	}
	return ob_list[i - 1];
    }

    if (prev == 0)
    {
	prev = environment(this_interactive());
    }

    if (sscanf(str, "\"%s\"", tmp) == 1)
    {
	ob_list = all_inventory(prev);
	for (i = 0 ; i < sizeof(ob_list) ; i++)
	{
	    if ((string)ob_list[i]->short() == tmp)
	    {
		return ob_list[i];
	    }
	}
    }

    if (sscanf(str, "#%d", i) == 1)
    {
	if (prev == 0)
	{
	    return 0;
	}
	ob_list = all_inventory(prev);
	if (i > sizeof(ob_list))
	{
	    return 0;
	}

	return ob_list[i - 1];
    }

    if (ob = present(str, prev))
    {
	return ob;
    }

    if (ob = present(str, this_interactive()))
    {
	return ob;
    }

    tmp = FTPATH(this_interactive()->query_path() + "/", str);
    if (strlen(tmp))
    {
	catch(call_other(tmp, "??"));	/* Force load */
	return find_object(tmp);
    }

    return 0;
}

/*
 * Function name: assign
 * Description  : This function will assign tracer values to special tracer
 *                variables stored in a player as properties.
 * Arguments    : string var - the name of the variable.
 *                mixed val  - the tracer value to store.
 */
void
assign(string var, mixed val)
{
    int i;
    mixed *vars;
    mixed *stores;

    vars = (string *)this_interactive()->query_prop(TRACER_VARS);
    stores = (mixed *)this_interactive()->query_prop(TRACER_STORES);

    while (i < sizeof(vars))
    {
	if (vars[i] == var)
	{
	    stores[i] = val;
	    break;
	}
	if (vars[i] == 0)
	{
	    vars[i] = var;
	    stores[i] = val;
	    break;
	}
	i += 1;
    }

    this_interactive()->add_prop(TRACER_VARS, vars);
    this_interactive()->add_prop(TRACER_STORES, stores);
}

/**
 * Funkcja ta u�ywana jest przez tracer_tool, do znalezienie obiekt�w przy wykonywaniu
 * komend takich jak Call, Clone, Cd, itp..
 * @param str     String do parsowania.
 * @param pattern Wz�r po jakim b�dziemy parsowa�
 * @param flag    Je�li 1 to filtrujemy przez deep_inv otoczenia, je�li nie to inv otoczenia i wiza.
 * @return Tablic� ({objekt, pozosta�y tekst})
 */
mixed
parse_list(string str, string pattern = "", int flag = 0)
{
    int p, i;
    string tmp;
    string rest, *args;
    mixed  koncowka;
    object prev;
    object *dest;
    object *obs;

    // krun - wyjebalem pattern argument, bo ZJEBALES :)
    // delv

    prev = environment(this_interactive());

    // ---- Zmiany Kruna -----
    // Niewielka przer�bka, coby najpierw dzia�a�o na parse_command, b�dzie mo�na wreszcie 
    // normalnie wpisywa� rzeczy do Calla, a dopiero jak parse_command nie znajdzie
    // przepuszczamy to przez stary algorytm parse_list, coby niewidzialne rzeczy te�,
    // mo�na by�o zobaczy�.

    if(flag)
        dest = deep_inventory(prev);
    else
        dest = all_inventory(ENV(TI)) + all_inventory(TI);

    if(!str)
        return 0;

    i = sizeof(explode(pattern, "%"));

    //Jak kto� wie jak to niech tu troche edytuje, bo ja nie mam zielonego poj�cia jak do parsa doda� 
    //kilka argument�w, nazie robi ifa do 2 bo wi�cej chyba nie b�dze trzeba.
    if(i == 1 && strlen(pattern) > 0)
        p = parse_command(str, dest, pattern, obs);
    else if(strlen(pattern) > 0)
        p = parse_command(str, dest, pattern, obs, koncowka);

    if(p && sizeof(obs))
    {
        obs = DIRECT_ACCESS(obs);

        if(sizeof(obs) > 1)
        {
            notify_fail("Nie mo�esz zrobi� tego na wi�cej ni� jednym obiekcie jednocze�nie.\n", 10);
            return 0;
        }
    }

    if(pointerp(koncowka))
    {
        koncowka = DIRECT_ACCESS(koncowka);

        if(!sizeof(koncowka))
            return 0;
        if(sizeof(koncowka) > 1)
        {
            notify_fail("Nie mo�esz zrobi� tego na wi�cej ni� jednym obiekcie jednocze�nie.\n", 10);
            return 0;
        }
    }

    if(p && sizeof(obs) == 1)
    {
        if(!koncowka)
            return ({obs[0], ""});
        else if(pointerp(koncowka) && sizeof(koncowka) == 1)
            return ({obs[0], koncowka[0]});
        else 
            return ({obs[0], koncowka});
    }

    args = explode(str, " ");
    str = args[0];

    // ---- Koniec Zmian ---- 

    while (objectp(prev) && stringp(str))
    {
        if (sscanf(str, "%s:%s", tmp, rest) == 2)
        {
            prev = find_item(prev, tmp);
            str = rest;
            continue;
        }
        prev = find_item(prev, str);
        break;
    }

    assign("$", prev);
    if(objectp(prev))
        write(file_name(prev) + "\n");

    // --- Zmiany Kruna ---
    // Coby wsp�gra�o ze starym parse_this
    if(strlen(pattern) > 0)
        return ({prev, implode(args[1..], " ")});
    // --- Koniec zmian --

    return prev;
}

/*
 * Function name: print_value
 * Description  : print out a value in a format appropriate for its type.
 * Arguments    : mixed ret - the value to print.
 */
void
print_value(mixed ret)
{
    if (intp(ret))
    {
	write("(int) " + ret + "\n");
    }
    else if (pointerp(ret))
    {
	write("Array of size " + sizeof(ret) + "\n");
	dump_array(ret);
    }
    else if (mappingp(ret))
    {
	write("Mapping of size " + m_sizeof(ret) + "\n");
	dump_mapping(ret);
    }
    else if (stringp(ret))
    {
	write("String: '" + ret + "'\n");
    }
    else if (objectp(ret))
    {
	write("Object: " + file_name(ret) + "\n");
    }
    else if (floatp(ret))
    {
	write(ftoa(ret) + "\n");
    }
    else if (functionp(ret))
    {
	write(sprintf("%O\n", ret));
    }
    else
    {
	write("Unknown!\n");
    }
}

/*
 * Function name: fix_one_arg
 * Description  : The individual arguments found in parse_arg are checked
 *                with this function to catch all arrays and to distinct
 *                between string or integer
 * Arguments    : what  - the argument to parse
 * Returns      : mixed - either an integer or a string
 */
mixed
fix_one_arg(string what)
{
    int narg;
    float fl;
    string apa;
    mapping mpng = ([]);

    //Troche zmian - dodanie obs�ugi mapping�w... Trzeba doda� obs�ug� zagnie�d�ania!
    //(Krun)
    if(sscanf(what, "({%s})", apa) == 1)
        return map(explode(apa + ",", ","), fix_one_arg);
    if(sscanf(what, "([%s])", apa) == 1)
    {
        string *exp = explode(apa + ",", ",");
        string *mex;
        foreach(string x : exp)
        {
            mex = map(explode(x + ":", ":"), fix_one_arg);
            if(sizeof(mex) >= 2)
                mpng[mex[0]] = mex[1];
        }
        return mpng;
    }

    if((wildmatch("*.*", what) || wildmatch("*e*", what)) && sscanf(what, "%f", fl) == 1)
        return fl;

    if(sscanf(what, "%d", narg) == 1)
        return narg;

    return what;
}

/*
 * Function name: parse_arg
 * Description  : This function is used to split the argument part of the
 *                paramtere to Call. Arguments, either string or integer
 *                may be separated by %% from eachother. An element may
 *                also be an array of mixed string and integer, though
 *                within the array, the elements should be devided by
 *                commas.
 *                Example: Mercade%%({Nick,Mrpr})%%45
 * Arguments    : what  - the string to parse
 * Returns      : mixed - an array with arguments
 */
mixed
parse_arg(string what)
{
    return map(explode(what + "%%", "%%"), fix_one_arg);
}
