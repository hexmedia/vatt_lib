/**
 * \file /cmd/wiz/helper.c
 *
 * This special soul contains some commands useful for wizards with a
 * helper function. The following commands are supported:
 *
 * - delchar
 * - delaccount
 * - lastlogin
 * - login
 * - pinfo
 */

#pragma no_clone
#pragma no_inherit
#pragma strict_types
#pragma save_binary

inherit "/cmd/std/command_driver";

#include <std.h>
#include <mail.h>
#include <colors.h>
#include <macros.h>
#include <ip_admin.h>
#include <stdproperties.h>

#define ALLOWED_WIZARD_FILE         "/cmd/wiz/helpers"
#define ALLOWED_LIEGE_COMMANDS      ({ "pinfo", "lastlogin" })
#define PINFO_EDIT_DONE             "pinfo_edit_done"
#define PINFO_WRITE_DONE            "pinfo_write_done"

#define CHECK_MG_ALLOWED                                                    \
    if(SECURITY->query_wiz_rank(TP->query_real_name()) < WIZ_ARCH &&        \
        !SECURITY->query_admin_team_member(TP->query_real_name(), "mg"))    \
    {                                                                       \
        return 0;                                                           \
    }

#define CHECK_ALLOWED       \
    if (!may_use_command()) \
    {                       \
        return 0;           \
    }

/*
 * Global variables.
 */
private static mapping allowed_wizards = ([ ]);
private static mapping pinfo_edit = ([ ]);

/*
 * Prototype.
 */
string strip_comments(string text);

/*
 * Nazwa funkcji: create
 * Opis         : Constructor. Called at creation.
 */
nomask void
create()
{
    string *lines;
    int    index;
    string *args;

    SECURITY->set_helper_soul_euid();

    if (file_size(ALLOWED_WIZARD_FILE) != -1)
    {
        lines = explode(strip_comments(read_file(ALLOWED_WIZARD_FILE)), "\n") -
            ({ "" });
        index = sizeof(lines);

        while(--index >= 0)
        {
            args = explode(lower_case(lines[index]), " ") - ({ "" });
            allowed_wizards[args[0]] = args[1..];
        }
    }
}

/*
 * Nazwa funkcji: get_soul_id
 * Opis         : Return a proper name of the soul in order to get a nice
 *                printout. People can also use this name with the "allcmd"
 *                command.
 * Zwraca       : string - the name.
 */
nomask string
get_soul_id()
{
    return "helper";
}

/*
 * Nazwa funkcji: query_cmd_soul
 * Opis         : Identify this as a command soul.
 * Zwraca       : int 1 - always.
 */
nomask int
query_cmd_soul()
{
    return 1;
}

/*
 * Nazwa funkcji: using_soul
 * Opis         : Called when a wizard hooks onto the soul.
 * Argumenty    : object wizard - the wizard hooking onto the soul.
 */
public void
using_soul(object wizard)
{
    SECURITY->set_helper_soul_euid();
}

/*
 * Nazwa funkcji: query_cmdlist
 * Opis         : The list of verbs and functions. Please add new in
 *                alphabetical order.
 * Zwraca       : mapping - ([ "verb" : "function name" ])
 */
nomask mapping
query_cmdlist()
{
    return ([
            "delaccount"    : "delaccount",
            "delchar"       : "delchar",

            "lastlogin"     : "lastlogin",
            "login"         : "login",

            "pinfo"         : "pinfo",
            ]);
}

/*
 * Nazwa funkcji: may_use_command
 * Opis         : Tests whether a particular wizard may use a the command.
 *                Arches++ can use all. The function operates on
 *                this_interactive() and query_verb().
 * Zwraca       : int 1/0 - allowed/disallowed.
 */
nomask int
may_use_command(string verb = query_verb())
{
    string name = this_interactive()->query_real_name();

    /* No name, or no verb, no show. */
    if (!strlen(verb) || !strlen(name))
        return 0;

    switch(SECURITY->query_wiz_rank(name))
    {
        case WIZ_ARCH:
        case WIZ_KEEPER:
        case WIZ_MAGE: // tymczasowo dodalem. d
            /* Arches and keepers do all. */
            return 1;

        case WIZ_STEWARD:
        case WIZ_LORD:
            /* Lieges have some special commands. */
            if (member_array(verb, ALLOWED_LIEGE_COMMANDS) >= 0)
                return 1;
                /* Set the euid of the object right. */

        /* Intentionally no break; */

        default:
            /* Wizard may have been allowed on the commands. */
            if (member_array(verb, allowed_wizards[name]) >= 0)
                return 1;
                /* Set the euid of the object right. */
    }

    /* All others, no show. */
    return 0;
}

/*
 * Nazwa funkcji: strip_comments
 * Opis         : Take a text and strip all comments from that file in the
 *                form "slash asterisk ... asterisk slash". It will assume
 *                that the comment-markers are always paired.
 * Argumenty    : string text - the text to strip comments from.
 * Zwraca       : string - the text without comments.
 */
public string
strip_comments(string text)
{
    string left;
    string middle;
    string right;

    /*
     * See whether there is a begin marker /*. If so, then we parse out the
     * start-end combination for comments, and everything in between.
     */
    while(wildmatch("*/\\**", text))
    {
        sscanf(("$" + text + "$"), "%s/*%s*/%s", left, middle, right);
        text = extract((left + right), 1, -2);
    }

    return text;
}

/* ***************************************************************************
 * PINFO: Edit/view the information file on a player.
 */

/*
 * Nazwa funkcji: pinfo_write_done
 * Opis         : Called from the editor when the wizard is done writing
 *                the text for the file on the player.
 * Argumenty    : string text - the text to add to the file.
 */
public void
pinfo_write_done(string text)
{
    string wname = this_player()->query_real_name();

    if (MASTER_OB(previous_object()) != EDITOR_OBJECT)
    {
        write("Nielegalne wywo^lanie pinfo_write_done().\n");
        return;
    }

    if (!stringp(pinfo_edit[wname]))
    {
        write("No pinfo_edit information. Impossible! Please report!\n");
        return;
    }

    if (!strlen(text))
    {
        write("Pinfo zaniechane.\n");
        return;
    }

    /* Make sure we have the proper euid. */
    SECURITY->set_helper_soul_euid();

    write_file(pinfo_edit[wname], ctime(time()) + " " + capitalize(wname) +
        " (" + capitalize(WIZ_RANK_NAME(SECURITY->query_wiz_rank(wname))) +
        "):\n" + text + "\n");
    pinfo_edit = m_delete(pinfo_edit, wname);
    write("Zachowano informacj�.\n");
}

/*
 * Nazwa funkcji: pinfo_edit_done
 * Opis         : Called from the editor when the wizard is done editing
 *                the text for the file on the player.
 * Argumenty    : string text - the text to add to the file.
 */
public void
pinfo_edit_done(string text)
{
    string wname = this_player()->query_real_name();

    if (MASTER_OB(previous_object()) != EDITOR_OBJECT)
    {
        write("Nielegalne wywo^lanie pinfo_edit_done().\n");
        return;
    }

    if (!stringp(pinfo_edit[wname]))
    {
        write("No pinfo_edit information. Impossible! Please report!\n");
        return;
    }

    if (!strlen(text))
    {
        write("Pinfo zaniechane.\n");
        return;
    }

    /* Make sure we have the proper euid. */
    SECURITY->set_helper_soul_euid();

    rm(pinfo_edit[wname]);
    write_file(pinfo_edit[wname], text + "\n" + ctime(time()) + " " +
        capitalize(wname) + " (" +
        capitalize(WIZ_RANK_NAME(SECURITY->query_wiz_rank(wname))) +
        "):\nEdycja poprzedniego wpisu.\n\n");
    pinfo_edit = m_delete(pinfo_edit, wname);
    write("Zachowano informacj^e.\n");
}

/*
 * Nazwa funkcji: pinfo_info
 * Opis         : Na potrzeby fingera, sprawdza, czy istnieje pinfo o
 *                fingerowanym graczu, ktore this_player() moze przeczytac.
 * Argumenty:   : name - imie fingerowanego
 */
public nomask int
pinfo_info(string name)
{
    string wname = this_player()->query_real_name();

    SECURITY->set_helper_soul_euid();

    return file_name(previous_object()) == WIZ_CMD_APPRENTICE &&
           may_use_command("pinfo") &&
           (!(SECURITY->query_wiz_rank(name)) ||
            (SECURITY->query_wiz_rank(wname) == WIZ_LORD &&
             SECURITY->query_wiz_dom(wname) ==
                 SECURITY->query_wiz_dom(name)) ||
            SECURITY->query_wiz_rank(wname) >= WIZ_MAGE) && // bylo ARCH. d
           file_size(PINFO_FILE(name)) > 0;
}

public void wyswietl_szatanskie_info_very(object pl)
{
    if(ENV(pl)->query_prop(ROOM_I_INSIDE))
    {
        tell_room(ENV(pl),"Nagle nad waszymi g�owami zjawia si� "+
            "dymna kula. Otwiera si�, a z jej �rodka uderza g�o�ny grzmot tak "+
            "jasny, pot�ny i o�lepiaj�cy, �e dociera do ciebie, i� "+
            "sama natura, nawet w najgorszych warunkach nie jest w "+
            "stanie wykrzesa� z siebie takiej mocy. "+
            "Chwila ta zdaje si� trwa� wieczno��.\n",({TP}));
        tell_room(ENV(pl),"Ziemia spod twych "+
            "st�p zaczyna "+
            "dr�e�, a w g�owie ko�ataj� rozgniewane wizerunki czarodziej�w "+
            "z Thanedd rzucaj�cych gromkie, przepe�nione nienawi�ci� "+
            "spojrzenia skierowane w stron� "+
            QIMIE(pl,PL_BIE)+".",({TP}));
        tell_room(ENV(pl)," Zdajesz sobie spraw�, �e jest to osoba "+
            "niegodziwa i w�a�nie jej poczynania zosta�y ukarane. "+
            "Piorun uderza w sam czubek "+pl->koncowka("jego","jej","tego")+
            " g�owy. Po chwili wszystko znika wraz z denat"+pl->koncowka("em","k�")+", pozostawiaj�c "+
            "po sobie jedynie smr�d...jakby palonego kurczaka.\n",({TP}));
    }
    else
    {
        tell_room(ENV(pl),"Nagle niebo przecina g�o�ny grzmot tak "+
            "jasny, pot�ny i o�lepiaj�cy, �e dociera do ciebie, i� "+
            "sama natura, nawet w najgorszych warunkach nie jest w "+
            "stanie wykrzesa� z siebie takiej mocy. "+
            "Chwila ta zdaje si� trwa� wieczno��.\n",({TP}));
        tell_room(ENV(pl),"Ziemia spod twych "+
            "st�p zaczyna "+
            "dr�e�, a w g�owie ko�ataj� rozgniewane wizerunki czarodziej�w "+
            "z Thanedd rzucaj�cych gromkie, przepe�nione nienawi�ci� "+
            "spojrzenia skierowane w stron� "+
            QIMIE(pl,PL_BIE)+".",({TP}));
        tell_room(ENV(pl)," Zdajesz sobie spraw�, �e jest to osoba "+
            "niegodziwa i w�a�nie jej poczynania zosta�y ukarane. "+
            "Piorun uderza w sam czubek "+pl->koncowka("jego","jej","tego")+
            " g�owy. Po chwili wszystko znika wraz z denat"+pl->koncowka("em","k�")+", pozostawiaj�c "+
            "po sobie jedynie smr�d...jakby palonego kurczaka.\n",({TP}));
    }
}

/**
 * Pozwala na usuni�cie konta gracza
 */
nomask int
delaccount(string str)
{
    string who, reason, opts, *kom = ({});

    CHECK_MG_ALLOWED;

    NF("Sk�adnia: delaccount [-fbli] <nazwa> <pow�d>\n");

    if(!str)
        return 0;

    string *args = explode(str, " ");

    if(sizeof(args) >= 2 && args[0][0] == '-')
    {
        opts = args[0];
        args = args[1..];
    }

    if(sizeof(args) < 1)
        return 0;

    reason = implode(args[1..], " ");
    who = LC(args[0]);

    object pl, konto;
    if(wildmatch("-*i*", opts))
    {
        pl = find_player(who);
        if(!pl)
            pl = SECURITY->finger_player(who);
        who = pl->query_mailaddr();
    }

    konto = SECURITY->finger_konto(who);

    if(!konto)
        return NF("Takie konto nie istnieje.\n");

    if(!reason || strlen(reason) < 5)
        return NF("Musisz poda� pow�d!\n");

    if(!wildmatch("-*n*", opts))
    {
        string *imiona;

        int ban = wildmatch("-*b*", opts);
        int force = wildmatch("-*f*", opts);

        imiona = konto->query_postacie();
        imiona += konto->query_postacie_krzyzyk();
        foreach(string i : imiona)
        {
            if(pl = find_player(i))
            {
                if(force)
                {
                    wyswietl_szatanskie_info_very(pl);
                    pl->catch_msg("Twoja posta� oraz konto zosta�y skasowane przez czarodziej�w Vatt'gherna.\n");
                    pl->quit();
                }
                else
                    return NF("Nie mo�esz usun�� konta kiedy zalogowane s� jakie� postaci b�d�ce na nim.\n");
            }

            if (SECURITY->query_wiz_rank(i))
                return notify_fail("Najpierw musisz usun�� postaci czarodziej�w z tego konta.\n");

            if (!SECURITY->remove_playerfile(i, "Usuwanie konta spowodowane: " + reason))
                return notify_fail("Nie uda^lo si^e usun^a^c gracza, nie da si� wi�c usun�� konta.\n");

            if(ban)
                SECURITY->banish(i, 2);
        }

        if(!SECURITY->remove_accountfile(who, reason))
            return notify_fail("Nie uda�o si� usun�� konta.\n");

        if(force)
            kom = ({"Si�owo usuni�to konto: " + who});
        else
            kom = ({"Usuni�to konto: " + who});

        if(ban)
            kom += ({"zbanowano imiona postaci"});
    }

    if(wildmatch("-*l*", opts))
    {
        kom += ({"zablokowano je"});
        SECURITY->block_email(who);
    }

    write(COMPOSITE_WORDS(kom) + ".\n");
    return 1;
}


/* **************************************************************************
 * delchar - remove a playerfile
 */
nomask int
delchar(string str)
{
    string who, reason, opts, kom;

    CHECK_MG_ALLOWED;

    notify_fail("Sk�adnia: delchar [-fb] <gracz> <pow^od>\n");

    if(!str)
        return 0;

    string *args = explode(str, " ");

    if(sizeof(args) >= 3 && args[0][0] == '-')
    {
        opts = args[0];
        args = args[1..];
    }

    if(sizeof(args) < 2)
        return 0;

    reason = implode(args[1..], " ");
    who = LC(args[0]);

    object pl;
    if(objectp(pl=find_player(who)))
    {
        if(wildmatch("-*f*", opts))
        {
            kom = "Si�owo usuni�to gracza: " + UC(who);

            wyswietl_szatanskie_info_very(pl);
            pl->catch_msg("\nTwoja posta� zostaje skasowana przez czarodziej�w.\n\n");
            pl->quit();
        }
        else
        {
            notify_fail(capitalize(who) + " przebywa obecnie w grze. Usuni^ecie " +
                find_player(who)->koncowka("go", "jej") + " jest teraz mo�liwe tylko z u�yciem flagi -f. \n");
            return 0;
        }
    }
    else
        kom = "Usuni�to gracza: " + UC(who);

    if (SECURITY->query_wiz_rank(who))
    {
        notify_fail("Czarodzieje powinni by^c najpierw zdegradowani.\n");
        return 0;
    }

    if (!SECURITY->remove_playerfile(who, reason))
    {
        notify_fail("Nie uda^lo si^e usun^a^c gracza.\n");
        return 0;
    }

    if(wildmatch("-*b*", opts))
    {
        kom += " i zbanowano jego imi�";
        SECURITY->banish(who, 2);
    }

    write(kom + ".\n");
    return 1;
}

nomask int
lastlogin(string str)
{
    CHECK_ALLOWED;

    NF("Sk�adnia: lastlogin [-<ilo��>] <imi�>\n");

    if(!str)
        return 0;

    int ilosc;
    string imie;
    if(str[0] == '-')
    {
        string *tmp = explode(str, " ");
        ilosc = atoi(tmp[0][1..-1]);
        imie = tmp[1];
    }
    else
    {
        ilosc = 5;
        imie = str;
    }

    string ent = read_bytes("/d/Standard/log/ENTER",
        file_size("/d/Standard/log/ENTER") - 30000, 30000);
    if(strlen(ent))
        string *enters = filter(explode(ent, "\n"), &wildmatch("* " + imie + " *",));

    if(!sizeof(enters))
    {
        write("Nie zosta�y zapisane �adne ostatnie logowania tego gracza.\n");
        return 1;
    }

    if(sizeof(enters) > ilosc)
        enters = enters[-ilosc..];

    write("Ostatnie logowania gracza o imieniu: " + UC(imie) + ":\n");
    foreach(string x : enters)
        write(x + "\n");

    return 1;
}

nomask int
pinfo(string str)
{
    string *args;
    string name;
    string wname = this_player()->query_real_name();
    int    rank = SECURITY->query_wiz_rank(wname);
    int    do_konta = 0;
    string cmd;
    string text;
    string file;
    object editor;
    object player;

    CHECK_ALLOWED;

    if (!strlen(str))
    {
        notify_fail("Sk�adnia: pinfo [k] [r / t / w / d / e] <imi�> [<tekst>]\n");
        return 0;
    }

    args = explode(str, " ");

    if(sizeof(args) == 1)
        name = lower_case(args[0]);
    else if(args[0] == "k" && sizeof(args) == 3)
        name = lower_case(args[2]);
    else
        name = lower_case(args[1]);

    player = SECURITY->finger_player(name);

    if(sizeof(args) && args[0] == "k")
    {
        args = args[1..-1];
        if(sizeof(explode(str, "@")) != 2)
            name = player->query_mailaddr();
        player = SECURITY->finger_konto(name);
        do_konta = 2;
    }

    if(sizeof(explode(str, "@")) == 2)
    {
        if(!do_konta)
            player = SECURITY->finger_konto(name);
        do_konta += 1;
    }

    args = ((sizeof(args) == 1) ? ( ({ "r" }) + args) : args);

    cmd = args[0];
    if (sizeof(args) > 2)
    {
        text = implode(args[2..], " ");
    }

    if(!player)
    {
        write(set_color(COLOR_FG_GREEN) + (do_konta ? "Konto" : "Posta�") +
            " o tej nazwie nie istnieje.\n" + clear_color());
    }
    /*
     * Access check. The following applies:
     *
     * - arches/keepers can do all.
     * - lieges can only access information on their subject wizards.
     * - people allowed for the command can handle mortal players.
     */
    switch(rank)
    {
        case WIZ_ARCH:
        case WIZ_KEEPER:
        case WIZ_MAGE:
            /* Te poziomy mog� zrobi� wsio.  */
            break;
        case WIZ_LORD:
        case WIZ_STEWARD:
            /* Can only handle their subject wizards. */
            if ((SECURITY->query_wiz_dom(wname) ==
                SECURITY->query_wiz_dom(name)) &&
                (SECURITY->query_wiz_rank(name) <= rank))
            {
                break;
            }
            /* Intentionally no break. Could be an allowed user. */

        default:
            /* May not handle wizards here. */
            if (SECURITY->query_wiz_rank(name))
            {
                write("Nie mo�esz ogl�da� pinfa o innych czarodziejach.\n");
                player->remove_object();
                return 1;
            }
    }

    /* Make sure we have the proper euid. */
    SECURITY->set_helper_soul_euid();
    file = PINFO_FILE(name);

    if(!do_konta)
        name = UC(name);

    switch(cmd)
    {
        case "d":
            if (rank < WIZ_ARCH && member_array(wname, EXPAND_MAIL_ALIAS("mg")) == -1)
            {
                notify_fail("Tylko arch, keeper b�d� MG mo�e usuwa� pinfo.\n");
                player->remove_object();
                return 0;
            }

            if (file_size(file) == -1)
            {
                write("Nie ma �adnego pinfo o " + (do_konta ? "koncie:" : "graczu o imieniu:") + " " + name + ".\n");
                player->remove_object();
                return 1;
            }

            rm(file);
            write("Usuni�to pinfo o " + (do_konta ? "koncie:" : "graczu o imieniu:") + " " + name + ".\n");
            player->remove_object();
            return 1;

        case "e":
            if (rank < WIZ_ARCH && member_array(wname, EXPAND_MAIL_ALIAS("mg")) == -1)
            {
                notify_fail("Tylko arch, keeper b�d� MG mo�e edytowa� pinfo.\n");
                player->remove_object();
                return 0;
            }

            if (file_size(file) == -1)
            {
                write("Nie ma �adnego pinfo o " + (do_konta ? "koncie:" : "graczu o imieniu:") + " " + name + ".\n");
                player->remove_object();
                return 1;
            }
            pinfo_edit[wname] = file;
            text = read_file(file);
            clone_object(EDITOR_OBJECT)->edit(PINFO_EDIT_DONE, text,
                sizeof(explode(text, "\n")));
            player->remove_object();
            return 1;

        case "m":
        case "r":
            if (file_size(file) == -1)
            {
                write("Nie ma �adnego pinfo o " + (do_konta ? "koncie:" : "graczu o imieniu:") + " " + name + ".\n");
                player->remove_object();
                return 1;
            }

            text = read_file(file);

            /* Automatically invoke more, or wizards request. */
            if ((cmd == "m") || (sizeof(explode(text, "\n")) > 100))
                TP->more(text);
            else
                write(text);

            setuid();seteuid(getuid());
            if(do_konta == 3)
            {
                object *postacie, *postacie_krzyzyk;

                postacie = player->query_postacie();
                postacie_krzyzyk = player->query_postacie_krzyzyk();

                string *ch_names, *ch_pinfo;
                ch_names = (sizeof(postacie) ? postacie : ({})) + (sizeof(postacie_krzyzyk) ? postacie_krzyzyk : ({}));

                if(sizeof(ch_names))
                {
                    write("O postaciach znajduj�cych si� na tym koncie s� nast�puj�ce pinfa:\n");
                    write("=======================================\n");

                    foreach(string sname : ch_names)
                    {
                        if(WIZ_CMD_HELPER->pinfo_info(sname))
                        {
                            write("----------------\n" +
                                "O postaci o imieniu " + UC(sname) + ":\n");
                            TP->command("pinfo t " + sname);
                            write("----------------\n");
                        }
                    }
                    write("=======================================\n");
                }
            }
            player->remove_object();
            return 1;

        case "t":
            if(file_size(file) == -1)
            {
                write("Nie ma �adnego pinfo o " + (do_konta ? "koncie:" : "graczu o imieniu:") + " " + name + ".\n");
                player->remove_object();
                return 1;
            }
            player->remove_object();
            tail(file);

            setuid();seteuid(getuid());
            if(do_konta == 3)
            {
                object *postacie, *postacie_krzyzyk;

                postacie = player->query_postacie();
                postacie_krzyzyk = player->query_postacie_krzyzyk();

                string *ch_names, *ch_pinfo;
                ch_names = (sizeof(postacie) ? postacie : ({})) + (sizeof(postacie_krzyzyk) ? postacie_krzyzyk : ({}));

                if(sizeof(ch_names))
                {
                    write("O postaciach znajduj�cych si� na tym koncie s� nast�puj�ce pinfa:\n");
                    write("=======================================\n");

                    foreach(string sname : ch_names)
                    {
                        if(WIZ_CMD_HELPER->pinfo_info(sname))
                        {
                            write("----------------\n" +
                                "O postaci o imieniu " + UC(sname) + ":\n");
                            TP->command("pinfo t " + sname);
                            write("----------------\n");
                        }
                    }
                    write("=======================================\n");
                }
            }
            return 1;

        case "w":
            if (file_size(file) == -1)
                write("Tworzenie pliku pinfa dla " + (do_konta ? "konta:" :  "gracza o imieniu:") + " " + name + ".\n");
            else
            {
                write("Plik pinfo " + (do_konta ? "konta:" : "gracza o imieniu:") + " " + name + " zosta� " +
                    "uzupe�niony o podane informacje.\n");
            }

            if(strlen(text))
            {
                if (strlen(text) > 75)
                    text = break_string(text, 75);

                write_file(file, ctime(time()) + " " + capitalize(wname) + " (" +
                capitalize(WIZ_RANK_NAME(SECURITY->query_wiz_rank(wname))) +
                    "):\n" + text + "\n\n");
            }
            else
            {
                pinfo_edit[wname] = file;
                clone_object(EDITOR_OBJECT)->edit(PINFO_WRITE_DONE);
            }

            player->remove_object();
            return 1;

        default:
            notify_fail("Sk�adnia: pinfo [k] [r / t / w / d / e] <imi�> [<tekst>]\n");
            player->remove_object();
            return 0;
    }

    player->remove_object();
    write("Nieoczekiwane zako�czenie funkcji 'pinfo()'. Prosz� zg�o� o tym.\n");
    return 1;
}

nomask int
ip(string str)
{
    if(calling_function() != "login" && previous_object() != TO)
        return 0;

    if(SECURITY->query_wiz_rank(TP->query_real_name()) < IP_RANK_ZOBACZENIE)
        return 0;

    notify_fail("Sk�adnia: login ip <subkomenda> <adres>\n");

    if(!str)
        return 0;

    string arg;
    if(sscanf(str, "%s %s", str, arg) != 2)
    {
        mapping ips = SECURITY->query_ip_list();

        if(!m_sizeof(ips))
        {
            write("W chwili obecnej nie ma �adnych adres�w ip na li�cie.\n");
            return 1;
        }

        write(sprintf("%15|s %20|s %44|s\n", "Adres Ip", "Dodajacy", "Data dodania"));

        foreach(string name : m_indexes(ips))
            write(sprintf("%15|s %20|s %44|s\n", name, ips[name][1], ctime(ips[name][2])));

        return 1;
    }

    switch(str)
    {
        case "add":
        case "a":
        case "dodaj":
        case "d":
            mixed tmp;

            if(SECURITY->query_wiz_rank(TP->query_real_name()) < IP_RANK_DODAWANIE)
                return 0;

            notify_fail("Sk�adnia: ip <add/dodaj/a/d/> <adres> <pow�d>.\n");
            if(sscanf(arg, "%s %s", arg, str) != 2)
                return 0;

            if(tmp = find_player(arg))
                arg = query_ip_number(tmp);

            notify_fail("Podaj poprawny adres ip lub imi� gracza obecnego w grze.\n");
            if(sizeof(tmp=explode(arg, ".")) != 4)
                return 0;

            if(!atoi(tmp[0]) || !atoi(tmp[1]) || !atoi(tmp[2]) || !atoi(tmp[3]))
                return 0;

            if(!str)
                return notify_fail("Musisz poda� pow�d.\n");

            if(strlen(str) < IP_MIN_DLUGOSC_POWODU)
            {
                notify_fail("Opis powodu za kr�tki. Minimalna dlugo�� wynosi " +
                    IP_MIN_DLUGOSC_POWODU + " znak�w.\n");
                return 0;
            }
            tmp = SECURITY->add_ip_to_list(arg, str);
            if(!tmp)
                return notify_fail("Nie uda�o si� doda� adresu do listy.\n");

            if(tmp == -1)
                return notify_fail("Taki adres zosta� ju� dodany.\n");

            write("Ju�.\n");
            return 1;
        case "remove":
        case "delete":
        case "r":
        case "usu�":
        case "u":
            //mixed tmp;
            notify_fail("Podaj poprawny adres ip.\n");
            if(sizeof(tmp=explode(arg, ".")) != 4)
                return 0;
            if(!atoi(tmp[0]) || !atoi(tmp[1]) || !atoi(tmp[2]) || !atoi(tmp[3]))
                return 0;
            if(SECURITY->query_wiz_rank(TP->query_real_name()) < IP_RANK_USUWANIE)
            {
                notify_fail("Nie mo�esz usuwa� adres�w ip z listy. " +
                    "Jest to dozwolone dla " + WIZ_S[IP_RANK_USUWANIE] + "'a i wy�ej.\n");
                return 0;
            }
            tmp = SECURITY->remove_ip_from_list(arg);
            if(!tmp)
            {
                notify_fail("Nie uda�o si� usun�� adresu z listy.\n");
                return 0;
            }
            if(tmp == -1)
            {
                notify_fail("Takiego adresu nie ma w li�cie dozwolonych multi ip�w.\n");
                return 0;
            }
            write("Ju�.\n");
            return 1;
        case "show":
        case "s":
        case "poka�":
        case "p":
            notify_fail("Podaj poprawny adres ip.\n");
            if(sizeof(tmp=explode(arg, ".")) != 4)
                return 0;
            if(!atoi(tmp[0]) || !atoi(tmp[1]) || !atoi(tmp[2]) || !atoi(tmp[3]))
                return 0;
            tmp = SECURITY->query_ip_from_list(arg);
            if(!tmp)
                return notify_fail("Nie uda�o si� odczyta� adresu.\n");

            if(tmp == -1)
                return notify_fail("Adres '" + arg + "' nie istnieje.\n");

            write("Adres: " + arg + "\nPow�d: " + tmp[0] + "\nOdblokowuj�cy: " +
                tmp[1] + "\nData: " + ctime(time()) + "\n");
            return 1;
        default:
            notify_fail("Nieznana subkomenda.\n");
    }

    return 0;
}

public int
name(string str)
{
    if(calling_function() != "login" && previous_object() != TO)
        return 0;

    if(SECURITY->query_wiz_rank(TP->query_real_name()) < IP_RANK_ZOBACZENIE)
        return 0;

    notify_fail("Sk�adnia: login name <subkomenda> <adres>\n");

    string arg;
    if(!str || sscanf(str, "%s %s", str, arg) != 2)
    {
        mixed *names = SECURITY->query_name_list();

        if(!sizeof(names))
        {
            write("W chwili obecnej nie ma �adnych kont na li�cie.\n");
            return 1;
        }

        write(sprintf("%15|s %15|s %15|s %30|s\n", "Konto1", "Konto2", "Dodajacy", "Data dodania"));

        foreach(mixed name : names)
            write(sprintf("%15|s %15|s %15|s %30|s\n", name[3][0], name[3][1], name[1], ctime(name[2])));

        return 1;
    }

    object p;
    mixed tmp;
    switch(str)
    {
        case "add":
        case "a":
        case "dodaj":
        case "d":
            if(SECURITY->query_wiz_rank(TP->query_real_name()) < IP_RANK_DODAWANIE)
                return 0;

            notify_fail("Sk�adnia: login name <add/dodaj/a/d/> <imi�/konto> <imi�/konto> <pow�d>.\n");

            string arg2;
            if(sscanf(arg, "%s %s %s", arg, arg2, str) != 3)
                return 0;

            if(p = SECURITY->finger_player(arg))
                arg = p->query_mailaddr();

            if(p = SECURITY->finger_player(arg2))
                arg2 = p->query_mailaddr();

            notify_fail("Podaj poprawne imi� gracza lub nazw� konta.\n");

            if(!str)
                return notify_fail("Musisz poda� pow�d.\n");

            if(strlen(str) < IP_MIN_DLUGOSC_POWODU)
            {
                notify_fail("Opis powodu za kr�tki. Minimalna dlugo�� wynosi " +
                    IP_MIN_DLUGOSC_POWODU + " znak�w.\n");
                return 0;
            }

            int res;
            res = SECURITY->add_names_to_list(arg, arg2, str);

            if(!res)
                return notify_fail("Nie uda�o si� doda� kont do listy.\n");

            if(res == -1)
                return notify_fail("Take konto zosta�o ju� dodane.\n");

            write("Ju�.\n");
            return 1;
        case "remove":
        case "delete":
        case "r":
        case "usu�":
        case "u":
            notify_fail("Sk�adnia: login name <remove/usun/r/u/> <imi�/konto> <imi�/konto>.\n");

            if(sscanf(arg, "%s %s", arg, arg2) != 2)
                return 0;

            if(!SECURITY->finger_konto(arg))
            {
                if(p = SECURITY->finger_player(arg))
                    arg = p->query_mailaddr();
                else
                    return notify_fail("Konto o tej nazwie nie istnieje.\n");
            }

            if(!SECURITY->finger_konto(arg2))
            {
                if(p = SECURITY->finger_player(arg2))
                    arg2 = p->query_mailaddr();
                else
                    return notify_fail("Konto o tej nazwie nie istnieje.\n");
            }

            if(SECURITY->query_wiz_rank(TP->query_real_name()) < IP_RANK_USUWANIE)
            {
                notify_fail("Nie mo�esz usuwa� nazw kont listy. " +
                    "Jest to dozwolone dla " + WIZ_S[IP_RANK_USUWANIE] + "'a i wy�ej.\n");
                return 0;
            }

            tmp = SECURITY->remove_name_from_list(arg, arg2);

            if(!tmp)
                return notify_fail("Nie uda�o si� kontna z listy.\n");

            if(tmp == -1)
                return notify_fail("Takiego konta nie ma w li�cie dozwolonych multi.\n");

            write("Ju�.\n");
            return 1;
        case "show":
        case "s":
        case "poka�":
        case "p":
            notify_fail("Podaj poprawne imi� gracza lub nazw� konta.\n");

            object p;

            if(p = SECURITY->finger_player(arg))
                arg = p->query_mailaddr();

            if(!SECURITY->finger_konto(arg))
                return 0;

            tmp = SECURITY->query_name_from_list(arg);

            if(!tmp || sizeof(tmp) != 4)
                return notify_fail("Nie uda�o si� odczyta� nazwy.\n");

            if(tmp == -1)
                return notify_fail("Nazwa '" + arg + "' nie istnieje na li�cie.\n");

            string dnazwa;
            if(lower_case(tmp[3][1]) == lower_case(arg))
                dnazwa = tmp[3][0];
            else
                dnazwa = tmp[3][1];

            write(sprintf("%10s: %20s %10s %10s: %20s\n", "Nazwa 1", arg, "", "Nazwa2", dnazwa) +
                sprintf("%10s: %20s %10s %10s: %20s", "Odblokowuj�cy", tmp[1], "", "Data", ctime(tmp[2])) +
                "Pow�d: " + tmp[0] + "\n");

            return 1;
        default:
            notify_fail("Nieznana subkomenda.\n");
    }

    return 0;
}

public int
aname(string str)
{
    if(calling_function() != "login" && previous_object() != TO)
        return 0;

    if(SECURITY->query_wiz_rank(TP->query_real_name()) < IP_RANK_ZOBACZENIE)
        return 0;

    notify_fail("Sk�adnia: login aname <subkomenda> <adres>\n");

    string arg;
    if(!str || sscanf(str, "%s %s", str, arg) != 2)
    {
        mixed *names = SECURITY->query_name_slist();

        if(!sizeof(names))
        {
            write("W chwili obecnej nie ma �adnego konta na li�cie.\n");
            return 1;
        }

        write(sprintf("%30|s %15|s %30|s\n", "Konto", "Dodajacy", "Data dodania"));

        foreach(mixed name : names)
            write(sprintf("%30|s %15|s %30|s\n", name[3], name[1], ctime(name[2])));

        return 1;
    }

    object p;
    mixed tmp;
    switch(str)
    {
        case "add":
        case "a":
        case "dodaj":
        case "d":
            if(SECURITY->query_wiz_rank(TP->query_real_name()) < IP_RANK_DODAWANIE)
                return 0;

            notify_fail("Sk�adnia: login name <add/dodaj/a/d/> <imi�/konto> <pow�d>.\n");

            if(sscanf(arg, "%s %s %s", arg,  str) != 3)
                return 0;

            if(p = SECURITY->finger_player(arg))
                arg = p->query_mailaddr();

            notify_fail("Podaj poprawne imi� gracza lub nazw� konta.\n");

            if(!str)
                return notify_fail("Musisz poda� pow�d.\n");

            if(strlen(str) < IP_MIN_DLUGOSC_POWODU)
            {
                notify_fail("Opis powodu za kr�tki. Minimalna dlugo�� wynosi " +
                    IP_MIN_DLUGOSC_POWODU + " znak�w.\n");
                return 0;
            }

            int res;
            res = SECURITY->add_names_to_slist(arg, str);

            if(!res)
                return notify_fail("Nie uda�o si� doda� kont do listy.\n");

            if(res == -1)
                return notify_fail("Take konto zosta�o ju� dodane.\n");

            write("Ju�.\n");
            return 1;
        case "remove":
        case "delete":
        case "r":
        case "usu�":
        case "u":
            notify_fail("Podaj poprawne imi� gracza lub nazw� konta.\n");

            if(p = SECURITY->finger_player(arg))
                arg = p->query_mailaddr();

            if(!SECURITY->finger_konto(arg))
                return notify_fail("Konto o tej nazwie nie istnieje.\n");

            if(SECURITY->query_wiz_rank(TP->query_real_name()) < IP_RANK_USUWANIE)
            {
                notify_fail("Nie mo�esz usuwa� nazw kont listy. " +
                    "Jest to dozwolone dla " + WIZ_S[IP_RANK_USUWANIE] + "'a i wy�ej.\n");
                return 0;
            }
            tmp = SECURITY->remove_ip_from_list(arg);
            if(!tmp)
                return notify_fail("Nie uda�o si� usun�� adresu z listy.\n");
            if(tmp == -1)
                return notify_fail("Takiego adresu nie ma w li�cie dozwolonych multi ip�w.\n");

            write("Ju�.\n");
            return 1;
        case "show":
        case "s":
        case "poka�":
        case "p":
            notify_fail("Podaj poprawne imi� gracza lub nazw� konta.\n");

            object p;

            if(p = SECURITY->finger_player(arg))
                arg = p->query_mailaddr();

            if(!SECURITY->finger_konto(arg))
                return 0;

            tmp = SECURITY->query_name_from_slist(arg);

            if(!tmp || sizeof(tmp) != 4)
                return notify_fail("Nie uda�o si� odczyta� nazwy.\n");

            if(tmp == -1)
                return notify_fail("Nazwa '" + arg + "' nie istnieje na li�cie.\n");

            write(sprintf("%10s: %20s %10s %10s: %20s\n", "Nazwa", arg, "", "Odbl.", tmp[1]) +
                "Data" + ctime(tmp[2]) + "\n" + "Pow�d: " + tmp[0] + "\n");

            return 1;
        default:
            notify_fail("Nieznana subkomenda.\n");
    }

    return 0;
}


public int
login(string arg)
{
    CHECK_ALLOWED;

    NF("Sk�adnia: login ip/name/aname\n");

    if(!arg)
        return 0;

    string str;

    if(sscanf(arg, "ip %s", str) == 1 || arg == "ip")
        return ip(str);
    else if(sscanf(arg, "name %s", str) == 1 || arg == "name")
        return name(str);
    else if(sscanf(arg, "aname %s", str) == 1 || arg == "aname")
        return aname(str);
}
