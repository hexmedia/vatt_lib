/**
 * \file /cmd/wiz/normal/slownik.c
 *
 * Plik obs�ugi s�wonika.
 *
 * @author Nieznany
 * @version 1.0
 *
 * Przej�cie na driverowy s�ownik.
 *
 * @author  Krun (krun@vattghern.pl)
 * @date    27.04.2008
 * @version 2.0
 */

#include <pl.h>

private static string
zestringuj_rodzaj(int rodzaj)
{
    switch(rodzaj)
    {
        case PL_MESKI_OS:       return "M�ski osobowy (PL_MESKI_OS)";
        case PL_MESKI_NOS_ZYW:  return "M�ski nieosobowy �ywotny (PL_MESKI_NOS_ZYW)";
        case PL_MESKI_NZYW:     return "M�ski nieosobowy nie�ywotny (PL_MESKI_NOS_NZYW)";
        case PL_ZENSKI:         return "�e�ski (PL_ZENSKI)"; break;
        case PL_NIJAKI_OS:      return "Nijaki osobowy (PL_NIJAKI_OS)"; break;
        case PL_NIJAKI_NOS:     return "Nijaki nieosobowy (PL_NIJAKI_NOS)"; break;
        default:                return "Nieznany rodzaj (" + rodzaj + ")"; break;
    }
}

int
slownik(string str)
{
    string *args;
    string mianownik;

    CHECK_SO_WIZ;

    if (!str)
    {
        notify_fail("Niew^la^sciwa ilo^s^c argument^ow.\n");
        return 0;
    }

    args = explode(str, " ");
    mianownik = implode(args[1..], " ");

    switch(args[0])
    {
        case "d":
        case "dodaj":
            write("Liczba pojedyncza\n\nMianownik\t[kto? co?]:");
            input_to("q_lp_mia");
            return 1;
        case "u":
        case "usu^n":
            if(!mianownik)
            {
                NF("Jakie s�owo chcesz usun��?\n");
                return 0;
            }

            if(slownik_usun(mianownik))
                write("Uda�o si� usun��.\n");
            else
                write("Nie uda�o si� usun��.\n");

            return 1;
        case "s":
        case "sprawd^x":
        case "p":
        case "pobierz":
            int rodzaj;

            if(!mianownik)
            {
                NF("Jakie s�owo chcesz sprawdzi�?\n");
                return 0;
            }

            if(!slownik_sprawdz(mianownik))
            {
                write("S�owo '" + mianownik + "' nie znajduje si� w s�owniku.\n");
                return 1;
            }

            mixed odmiana;
            odmiana = slownik_pobierz(mianownik);

            write("Liczba Pojedyncza:\n" +
                  "  Mianownik:          " + odmiana[0][PL_MIA] + ",\n" +
                  "  Dope�niacz:         " + odmiana[0][PL_DOP] + ",\n" +
                  "  Celownik:           " + odmiana[0][PL_CEL] + ",\n" +
                  "  Biernik:            " + odmiana[0][PL_BIE] + ",\n" +
                  "  Narz�dnik:          " + odmiana[0][PL_NAR] + ",\n" +
                  "  Miejscownik:        " + odmiana[0][PL_MIE] + ",\n");

            if(sizeof(odmiana) == 3)
            {
                write("Liczba Mnoga:\n" +
                      "  Mianownik:          " + odmiana[1][PL_MIA] + ",\n" +
                      "  Dope�niacz:         " + odmiana[1][PL_DOP] + ",\n" +
                      "  Celownik:           " + odmiana[1][PL_CEL] + ",\n" +
                      "  Biernik:            " + odmiana[1][PL_BIE] + ",\n" +
                      "  Narz�dnik:          " + odmiana[1][PL_NAR] + ",\n" +
                      "  Miejscownik:        " + odmiana[1][PL_MIE] + ",\n");
                rodzaj = odmiana[2];
            }
            else
                rodzaj = odmiana[1];

            write("Rodzaj:               " + zestringuj_rodzaj(rodzaj) + ".\n");
            return 1;
    }

    notify_fail("Nieprawid^lowy argument.\n");
    return 0;
}

void
q_lp_mia(string str)
{
    if (!strlen(str))
    {
        TP->add_prop("_odm", ({ }));
        write("Brak liczby pojedynczej.\n\nLiczba mnoga\n\nMianownik      [kto? co?]:");
        input_to("q_lm_mia");
    }
    else if(slownik_sprawdz(str))
        write("To slowo jest juz w slowniku!\n");
    else
    {
        TP->add_prop("_odm", ({ str }));
        write("Dope^lniacz     [kogo? czego?]:");
        input_to("q_lp_dop");
    }
}

void q_lp_dop(string str)
{
    TP->add_prop("_odm", TP->query_prop("_odm") + ({ str }));
    write("Celownik       [komu? czemu?]:"); input_to("q_lp_cel");
}

void q_lp_cel(string str)
{
    TP->add_prop("_odm", TP->query_prop("_odm") + ({ str }));
    write("Biernik        [kogo? co?]:"); input_to("q_lp_bie");
}

void q_lp_bie(string str)
{
    TP->add_prop("_odm", TP->query_prop("_odm") + ({ str }));
    write("Narz^ednik      [z kim? z czym?]:"); input_to("q_lp_nar");
}

void q_lp_nar(string str)
{
    TP->add_prop("_odm", TP->query_prop("_odm") + ({ str }));
    write("Miejscownik    [o kim? o czym?]:"); input_to("q_lp_mie");
}

void q_lp_mie(string str)
{
    TP->add_prop("_odm", TP->query_prop("_odm") + ({ str }));
    write("\nLiczba mnoga\n\nMianownik      [kto? co?]:"); input_to("q_lm_mia");
}

void q_lm_mia(string str)
{
    TP->add_prop("_odm", TP->query_prop("_odm") + ({ str }));
    write("Dope^lniacz     [kogo? czego?]:"); input_to("q_lm_dop");
}

void q_lm_dop(string str)
{
    TP->add_prop("_odm", TP->query_prop("_odm") + ({ str }));
    write("Celownik       [komu? czemu?]:"); input_to("q_lm_cel");
}

void q_lm_cel(string str)
{
    TP->add_prop("_odm", TP->query_prop("_odm") + ({ str }));
    write("Biernik        [kogo? co?]:"); input_to("q_lm_bie");
}

void q_lm_bie(string str)
{
    TP->add_prop("_odm", TP->query_prop("_odm") + ({ str }));
    write("Narz^ednik      [z kim? z czym?]:"); input_to("q_lm_nar");
}

void q_lm_nar(string str)
{
    TP->add_prop("_odm", TP->query_prop("_odm") + ({ str }));
    write("Miejscownik    [o kim? o czym?]:"); input_to("q_lm_mie");
}

void q_lm_mie(string str)
{
    TP->add_prop("_odm", TP->query_prop("_odm") + ({ str }));
    write("\nRodzaj\n" +
          "1. Meski os (np. dwaj m^e^zczy^xni)\n" +
          "2. M^eski nos ^zyw (np. dwa elfy)\n" +
          "3. M^eski nos n^zyw (np. dwa kamienie)\n" +
          "4. ^Ze^nski\n" +
          "5. Nijaki os (np. dwoje dzieci)\n" +
          "6. Nijaki nos (np. dwa ^xd^xb^la)\n>>> ");
    input_to("q_rodzaj");
}

void q_rodzaj(string str)
{
    int rodz;
    string *odm;

    switch (str)
    {
        case "1": rodz = PL_MESKI_OS;       break;
        case "2": rodz = PL_MESKI_NOS_ZYW;  break;
        case "3": rodz = PL_MESKI_NOS_NZYW; break;
        case "4": rodz = PL_ZENSKI;         break;
        case "5": rodz = PL_NIJAKI_OS;      break;
        case "6": rodz = PL_NIJAKI_NOS;     break;
        default:
            write("\nRodzaj\n" +
                "1. Meski os (np. dwaj m^e^zczy^xni)\n" +
                "2. M^eski nos ^zyw (np. dwa elfy)\n" +
                "3. M^eski nos n^zyw (np. dwa kamienie)\n" +
                "4. ^Ze^nski\n" +
                "5. Nijaki os (np. dwoje dzieci)\n" +
                "6. Nijaki nos (np. dwa ^xd^xb^la)\n>>> ");
            input_to("q_rodzaj");
            return;
    }

    odm = TP->query_prop("_odm");

    if (sizeof(odm) == 12)
        slownik_dodaj(odm[0..5], odm[6..11], rodz);
    else
        slownik_dodaj(odm, rodz);
}
