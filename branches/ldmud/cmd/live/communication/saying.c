/**
 * \file /cmd/live/communication/saying.c
 *
 * Plik zawiera funkcje potrzebne do m�wienia.
 *
 * Plik jest przer�bk� z \file /std/player/cmdhooks.c
 * Przerobi� - Krun
 *
 * W pliku istnieje opcja wy��czenia obs�ugi innych j�zyk�w,
 * jak te� opcja wy��czenia obs�ugi free emota. Mo�na te�
 * wy��czy� parsowanie free emota przez parse_command.
 *
 * Aktualnie free emot jest w��czony, za� inne j�zyki wy��czone.
 *
 * <i>Dla w��czenia j�zyk�w podmie� warto�� defina OTHER_LANGUAGES na jak�kolwiek warto�� prawdziw�</i>
 * <i>Dla wy��czenia free emota podmie� warto�� defina FREE_EMOTE na warto�� fa�sz(0)</i>
 * <i>Dla wy��czenia prasowania free emota przez parse_command zmie� wartos� defina PARSE_FREE_�EMOTE na warto�� fa�sz(0)</i>
 *
 * TODO:
 * - <i> Trzeba poprawi� obs�ug� innych j�zyk�w. </i>
 * - <i> Trzeba zintegrowa� m�wienie do kogo� z u�yciem emota z emote_communicate().</i>
 * - <i> Prasowania nie mo�na na razie w��cza�, poniewa� jest problem z okre�leniem
 *       przypadku w bierniku oraz gdy wszystkie przypadki s� takie same.</i>
 */

//W tej chwili plik jest w trakcie przerabiania w celu lepszej obs�ugi j�zyk�w.
//Wszelkie funkcje z flaga FIXME NIE s� u�ywane i nie nale�y zwraca� uwagi na ich
//sk�adnie. Nie wiem kiedy b�d� mia� czas na wi�ksze mieszanie w tym pliku
//narazie jest kilka wa�niejszych rzeczy.
// Krun

#include <std.h>
#include <login.h>
#include <macros.h>
#include <speech.h>
#include <options.h>
#include <cmdparse.h>
#include <filter_funs.h>

/*
 * Prototypes
 */
public varargs int	communicate(string str = "", int lang = 0);

#define REOPEN_SOUL_ALLOWED ([ "exec_done_editing" : WIZ_CMD_NORMAL, \
                               "pad_done_editing"  : WIZ_CMD_NORMAL, \
                               "do_many_delayed"   : WIZ_CMD_NORMAL, \
                               "tail_input_player" : WIZ_CMD_APPRENTICE ])
#define REOPEN_SOUL_RELOAD  "_reloaded"

#define VBFC_SAY(x)             "@@vbfc_say:"+file_name(this_object())+"|"+(x)+"@@"
#define VBFC_SAY_TO(x)          "@@vbfc_say_to:"+file_name(this_object())+"|"+(x)+"|@@"
#define VBFC_SAY_TO1(x,y)       "@@vbfc_say_to:"+file_name(this_object())+"|"+(x)+"||"+ \
                                 implode(map((y), file_name), "|")+"@@"

//J�zyki na razie nie dzia�aj� i s� wy��czone, aby w��czyc nale�y zmieni�
//poni�eszego defina na 1, jednak w chwili obecnej jest to niezalecane,
//i mo�e spowodowa� b��dy podczas kompilacji pliku. (Krun)
#define OTHER_LANGUAGES         0

//Dzi�ki tej definicji bez problemu mo�emy wy��czy� free emota w mowie
#define FREE_EMOTE              1

#if FREE_EMOTE
//Dzi�ki tej definicji mo�emy w��czy� lub wy��czy� parsowanie free emota
#define PARSE_FREE_EMOTE        0
#endif FREE_EMOTE

#if OTHER_LANGUAGES
// FIXME.
// To trzeba przerobi�, bo tego w soulu by� nie mo�e, ale, �e to j�zyki narazie
// zostaje.
static string       *translate = ({ });	/* zdanie przet�umaczone */
static string       *orginal = ({ });	/* oryginalne zdanie */
static int          *codes = ({ });

static mapping slownik = ([
    "krasnolud" : ({
    "gy", "farl", "gor", "huln", "hol", "arn", "jor", "den", "rd",
    "def", "ky", "ra", "nath", "lah", "rjor", "lum", "zha", "rif",
    "um", "ra'h", "mur", "mull", "rak", "sab", "uul", "vo", "mot",
    "jor", "h", "r", "a", "z", "u", "o", "iv'", "za", "n", "v",
    "gh", "da", "nin", "far", "khil", "tal", "wro", "ka", "oks",
    "gar", "naal", "tarn", "raan", "riin", "kaan", "gre", "th",
    "ha", "ka", "nag", "hok", "k", "kad", "bu", "ro", "tu", "kul",
    "ha", "ka", "sh", "et", "hik", "ar", "ug", "wa", "nu", "un",
    "da", "li", "gim", "bia", "ul", "ah", "zha", "dha", "rha",
    "l", "khuz", "za", "zha", "sha", "dum", "kha" }) ]);


/**
 * Funkcja do przepisania, musi szuka� po znajomo�ci j�zyk�w, i konkretnego j�zyka.
 */
public varargs int
language(string str = "")
{
    if (this_object()->query_race_name() != "krasnolud")
        return 0;

    communicate(str, 1);
    return 1;
}

/**
 * Funkcja do przerobienia.
 */
string
skill_msg(object obj)
{
    int skill = obj->query_skill(SS_LANGUAGE);
    int size = sizeof(translate);
    string *msg = ({ });
    int i, x;


    for (i = 0; i < size; ++i)
    {
        if (skill > (random(70, codes[i]) + 30) )
            msg += ({ orginal[i] });
        else
            msg += ({ translate[i] });
    }
    return implode(msg, " ");
}

//FIXME: Do j�zyk�w, prawdopodobnie ca�a do przerobienia:)
void
powiedz()
{
    int i;
    object *present = ({ });
    int size;
    string ss_msg;

    present = FILTER_LIVE(all_inventory(environment(this_object())));
    present -=  ({ this_player() });
    size = sizeof(present);

    for (i = 0; i < size; ++i)
    {
        ss_msg = capitalize(skill_msg(present[i]));
        if (CAN_SEE_IN_ROOM(present[i]) && CAN_SEE(present[i], this_object()))
            present[i]->catch_msg(sprintf("%s %s po krasnoludzku: %s\n",
                query_Imie(present[i], PL_MIA),
                (com_sounds[present[i]->query_race()] ?: "m�wi"),
                ss_msg));
        else
            present[i]->catch_msg(sprintf("G�os %s %s po krasnoludzku: %s\n",
                (present[i]->query_met(this_object())
                    ? query_met_name(PL_DOP)
                    : query_rasa(PL_DOP)),
                (com_sounds[present[i]->query_race()] ?: "m�wi"), ss_msg));
        present[i]->catch_say_to(ss_msg);
    }
    translate = ({ });
    orginal = ({ });
    codes = ({ });
}

//FIXME: Zn�w j�zyki, narazie nawet nie czytam.
string
translator(string str)
{
    int len = (strlen(str) / 3);
    int x, y, seed, code;
    string trans = "";

    if (strlen(str) % 3 !=0)
        len += 1;

    for (x = 0 ; x < len  ; x++)
    {
        for (y = 0 ; y < 3  ; y++)
            seed += str[x * 3 + y];
            code += seed;
        trans = trans + slownik[random(sizeof(slownik), seed)];
        seed = 0;
    }
    codes += ({ code });

    return trans;
}

//FIXME: �ans egejn
int
tumacz(string str)
{
    string str1;
    int i, size;

    orginal = explode(str, " ");

    size = sizeof(orginal);
    for (i = 0; i < size; ++i)
        translate += ({ translator(orginal[i]) });

    powiedz();

    return 1;
}
#endif OTHER_LANGUAGES

#if PARSE_FREE_EMOTE
/**
 * Jak wiadomo vbfc nie mo�na dziedziczy�, funkcja
 * pozwala na obej�cie tego problemu:)
 * @param arg otrzymywany argumen
 * @param flag czy zamienic z vbfc(1) czy na vbfc(2)
 * @return zamieniony string
 */
private static string
change_vbfc(string str, int flag=0)
{
    if(flag)
        return implode(explode(implode(explode(str, "|"), "!~"), "@@"), "$&$&");
    else
        return implode(explode(implode(explode(str, "!~"), "|"), "$&$&"), "@@");
}

/**
 * Funkcja ta filtruje emota i znajduje wszystkie obiekty,
 * kt�re by�y u�yte przez gracza.
 * @param arg string do przerobienia
 * @return tablice z przerobionymi stringami <i>({dla TP, dla reszty})</i>.
 */
private static string *
format_emote_str(string arg)
{
    //Sprawdzamy do czasu, a� znajdziemy wszystkie obiekty.
    //I sprawdzamy przypadki, mo�e by� troche problem z
    //dok�adnym okre�leniem mianownika i biernika, w przypadku
    //kiedy s� one identyczne, ale nie robi nam to �adnego problemu
    //bo bez r�nicy kt�rego u�yjemy.
    int przyp;
    object *emote_ob;
    mixed emote_obs = ({});
    string prefix, postfix, to_write="";

    for(przyp=0;przyp<6;przyp++)
    {
        while(parse_command(arg, ENV(TP), "%s %i:" + przyp + " %s", prefix, emote_ob, postfix))
        {
            emote_ob = NORMAL_ACCESS(emote_ob, 0, 0);

            if(!sizeof(emote_ob))
                continue;

            emote_obs += ({ ({emote_ob, przyp}) });

            arg = prefix + " # " + postfix;
        }
    }

    string *args = explode(arg, "#");

    if(!sizeof(args))
        return ({arg, arg});

    arg = "";

    int i,j,z;
    for(i=0,j=0;i<sizeof(args);i++)
    {
        arg += args[i];
        to_write += args[i];
        if(j<sizeof(emote_obs))
        {
            to_write += COMPOSITE_STH(emote_obs[j][0], emote_obs[j][1]);
            arg += QCOMPSTH(emote_obs[j++][1]);
        }
    }

    arg = change_vbfc(arg, 1);
    return ({to_write, arg});
}
#endif PARSE_FREE_EMOTE

/**
 * Funkcja ta u�ywana jest gdy gracz chce co� do kogo� powiedzie�/
 * Wszyscy inni tak�e b�d� s�ysze� wypowiedziane s�owa.
 * @param str argument do 'powiedz'
 * @param lang u�ywany j�zyk wypowiedzi(Argument niekatywny(zawsze przestawiany na 0))
 * @param emte czy gracz chce wykorzysta� freeemota.
 * @return 1/0 - sukces/pora�ka
 */
public int
communicate_to(string str, int lang = 0, int emote = 0)
{
    object *oblist;
    string r_sound;
    string qcomp;
    string str1;
    string ss_msg;
    string formated_str;
    int i, size;
    object *present = ({ });

    if(!TP)
        return 0;

    /* We must parse the lower case of the string 'str' since parse_command
     * does not find capitalized player names, so it would not trigger on
     * "say to Mercade Greetings!" However, since we want to keep the
     * capitals in the said text, we store the original text in the variable
     * 'say_string' and use that later.
     */
    if (parse_command(str, ENV(TP),
        "'do' %i:" + PL_DOP + " %s", oblist, str))
    {
        oblist = NORMAL_ACCESS(oblist, 0, 0) - ({ this_player(), 0 });
        if (!sizeof(oblist) || !strlen(str))
            return 0;

        qcomp = COMPOSITE_LIVE(oblist, PL_DOP);
    }
    else if (parse_command(str, ({}), "'do' 'siebie' %s", str))
    {
        if (!strlen(str))
            return 0;

        formated_str = FORMAT_SPEECH(SPEECH_DRUNK(implode(explode(str, " ,"), ","), TP));

        if (this_player()->query_option(OPT_ECHO))
        {
#if OTHER_LANGUAGES
            //FIXME: Jak w communicate:)
            if(lang)
                write(koloruj_mowe(capitalize(this_object()->actor_race_sound()) + " do siebie" +
                    " po krasnoludzku: ", formated_str + "\n", TP));
            else
#endif OTHER_LANGUAGES
                write(koloruj_mowe(capitalize(TP->actor_race_sound()) + " do siebie: ",
                    formated_str + "\n", TP));
        }
        else
            write("Ju�.\n");

        say(koloruj_mowe(VBFC_SAY_TO("") + ": ", formated_str, 1) + "\n");

        return 1;
    }
    else
        return 0;

    //Je�li nie m�wimy do siebie to lecimy dalej.

    /* In order to be able to use QCOMPLIVE later, we have to query the
     * COMPOSITE_LIVE macro now, even though the player may not be using
     * echo.
     */
//    write("oblist: "+dump_array(oblist)+"\n");
//    write("qcomp: "+qcomp+"\n");


/**
    Kto� kto pisa� poni�szy kod jest przechujem..
    Skomentowa� by o co chodzi mu chodzi�o, z 5 min patrzy�em
    i doszed�em do wniosku, �e usuwa� spacje sprzed przecink�w..
    Bardziej skomplikowanych rowi�zan stosowac si� nie da?
    Chyba, �e to zabezpieczenie przeciw kradzie�� kodu.. Jak b�dziemy
    tak pisa� to nam nikt nie zakosi, bo wi�cej czasu zajmie
    doj�cie co do czego, ni� napisanie od 0:)

    Kod zostawiam dla potomnych. Niech popatrz� jak pisa� nie nale�y (Krun)
    {
	    int str_length, string_lendiff;
	    int str_i, str_pop;
	    str_length = strlen(str);
	    string_lendiff = strlen(say_string) - str_length;
	    str_pop = 0;
//	    write("length="+str_length+"\n");
	    for (str_i = str_length-1; str_i > 0; --str_i) {
//		    write(">i="+str_i+", str[i]=["+str[str_i]+"], str[i-1]=["+str[str_i-1]+"], pop="+str_pop+", string[i-1+pop]=["+say_string[string_lendiff+str_i-1+str_pop]+"]\n");
		    if ((str[str_i] == ',') && (str[str_i-1] == ' ') && (say_string[string_lendiff+str_i-1+str_pop] != ' ')) {
			    ++str_pop;
		    }
	    }
//    write("say_string:["+say_string+"]("+strlen(say_string)+")\n");
//    write("str:["+str+"]("+strlen(str)+")\n");
//    write("str_pop="+str_pop+"\n");
	    say_string = extract(say_string, str_pop - (strlen(str)));
    }
*/
    formated_str = str;
    string emote_str_ftp = "", emote_str_fot = "";

#if FREE_EMOTE
    if(emote)
    {
        string *exp = explode(formated_str, ":");
        formated_str = implode(exp[1..], ":");
#if PARSE_FREE_EMOTE
        string *formated_emote = format_emote_str(implode(explode(exp[0], "."), " ."));
        emote_str_ftp = implode(explode(implode(explode(formated_emote[0], " ,"), ",") + " .", " ."), ".");
        emote_str_fot = implode(explode(implode(explode(formated_emote[1], " ,"), ",") + " .", " ."), ".");
#else
        emote_str_ftp = exp[0];
        emote_str_fot = exp[0];
#endif PARSE_FREE_EMOTE
        formated_str = FORMAT_SPEECH(SPEECH_DRUNK(formated_str, TP));
    }
    else
#endif FREE_EMOTE
        formated_str = FORMAT_SPEECH(SPEECH_DRUNK(formated_str, TP));

    formated_str = implode(explode(formated_str, " ,"), ",");
    emote_str_ftp = implode(explode(emote_str_ftp, " ,"), ",");
    emote_str_fot = implode(explode(emote_str_fot, " ,"), ",");

    if (this_player()->query_option(OPT_ECHO))
    {
#if OTHER_LANGUAGES
        //FIXME: Jak wy�ej
        if(lang)
            write(koloruj_mowe(capitalize(TP->actor_race_sound()) + " do " +
                qcomp + " po krasnoludzku" + (emote ? emote_str_ftp : "") + ":", formated_str + "\n", TP));
        else
#endif OTHER_LANGUAGES
            write(koloruj_mowe(capitalize(TP->actor_race_sound()) + " " + "do " + qcomp +
#if FREE_EMOTE
                (emote ? " " + emote_str_ftp : "") +
#endif FREE_EMOTE
                ": ", formated_str + "\n", TP));
    }
    else
        write("Ju�.\n");


#if OTHER_LANGUAGES
    //FIXME: Jezyki do przerobienia, nawet nie sprawdzam.
    if(lang)
    {
        orginal = explode(say_string, " ");

        size = sizeof(orginal);
        for (i = 0; i < size; ++i)
            translate += ({ translator(orginal[i]) });

        present = FILTER_LIVE(all_inventory(environment(this_object())));
        present -= ({ this_player() });
        present -= oblist;
	foreach (object x : present)
        {
            ss_msg = capitalize(skill_msg(x));
			set_this_player(x);
			if (emote)
			ss_msg = JZIE+explode(say_string,":")[0] +":"+CL+ZIE+
				explode(say_string,":")[1]+CL;
    		else
			ss_msg = ZIE+ say_string+CL;

			x->catch_msg(sprintf("@@vbfc_say_to:%s@@: %s\n",
		file_name(this_object()), FORMAT_SPEECH(SPEECH_DRUNK(ss_msg,last_pl))),
		(oblist + ({ this_player() }) ));

			set_this_player(last_pl);
        }

	foreach (object x : oblist)
        {
            ss_msg = capitalize(skill_msg(x));
			set_this_player(x);
			if (emote)
			ss_msg = JZIE+explode(say_string,":")[0] +":"+CL+ZIE+
				explode(say_string,":")[1];
    		else
			ss_msg = ZIE+ say_string;


            if (CAN_SEE_IN_ROOM(x) && CAN_SEE(x, this_object()))
					x->catch_msg(U+sprintf("%s %s do ciebie: %s\n",
                    query_Imie(x, PL_MIA),
                    (com_sounds[x->query_race()] ?: "m�wi"),
                    FORMAT_SPEECH(SPEECH_DRUNK(ss_msg,last_pl))+CL));
            else
                x->catch_msg("S�yszysz dochodz�cy z ciemno�ci g�os " +
                        (x->query_met(TO) ? query_met_name(PL_DOP)  :
                        "jakie" + koncowka("go�", "j�") + " " + query_rasa(PL_DOP)) +
                        FORMAT_SPEECH(SPEECH_DRUNK(ss_msg,last_pl))+CL);

            x->catch_say_to(FORMAT_SPEECH(SPEECH_DRUNK(ss_msg,last_pl)));

            set_this_player(last_pl);
        }

        set_obiekty_zaimkow(oblist);
        translate = ({ });
        orginal = ({ });
        codes = ({ });
        return 1;
    }
#endif OTHER_LANGUAGES

    object *rest = all_inventory(ENV(TP));
    rest -= (oblist);
    say(koloruj_mowe(VBFC_SAY_TO1(emote_str_fot, oblist) + ": ", formated_str + "\n", 1), oblist + ({TP}));
    say(koloruj_mowe("<u>" + VBFC_SAY_TO1(emote_str_fot, oblist) + ":</u> ", formated_str + "\n", 1), rest);

    //Wysy�amy sygna� do obiekt�w do kt�rych m�wimy.
    oblist->catch_say_to(formated_str);

    TP->set_obiekty_zaimkow(oblist);

    return 1;
}

#if FREE_EMOTE
/*
 * Nazwa funkcji: emote_communicate
 * Opis         : Funkcja ta jest wywo�ywana gdy gracz zaczyna wypowied� od ':'.
 *                Wtedy wypowied� jest lekko zmieniona, by umo�liwi� u�ycie free
 *                emote.
 * Argumenty    : string str - argument komendy 'powiedz'
 *                int lang - u�ywany j�zyk wypowiedzi
 * Zwraca       : int 1/0 - sukces/pora�ka
 */
public varargs int
emote_communicate(string str = "", int lang = 0)
{
    if ((!stringp(str)) || (!strlen(str)) || !wildmatch(":*:*", str))
        return 0;

    str = str[1..];

    if (str[0] == ' ')
        str = str[1..];

    string *exp = explode(str, ":");
#if PARSE_FREE_EMOTE
    exp[0] = implode(explode(exp[0] + ".", "."), " .");
    string *formated_emote_str = format_emote_str(exp[0]);
    string emote_str_ftp = implode(explode(implode(explode(formated_emote_str[0], " ,"), ",") + " .", " ."), ".");
    string emote_str_fot = implode(explode(implode(explode(formated_emote_str[1], " ,"), ",") + " .", " ."), ".");
#else
    string emote_str_ftp = exp[0];
    string emote_str_fot = exp[0];
#endif
    string formated_str = implode(explode(FORMAT_SPEECH(SPEECH_DRUNK(implode(exp[1..], ":"), TP)), " ,"), ",");

    //Wywali�em ponizszy komuikat z dwukropkami bo blokowa� u�ywanie dwukropka w p�niejszym
    //tekscie.

    if (this_player()->query_option(OPT_ECHO))
    {
#if OTHER_LANGUAGES
        if (lang)
        {
            write(JZIE+capitalize(TP->actor_race_sound()) +
            " po krasnoludzku " + pre_str+": "+ CL+ZIE+formated_str+CL+ "\n");
        }
        else
#endif OTHER_LANGUAGES
            write(koloruj_mowe(capitalize(TP->actor_race_sound()) +
                " " + emote_str_ftp +": ", formated_str + "\n", TP));
    }
    else
        write("Ju�.\n");

#if OTHER_LANGUAGES
    if(lang)
    {
        tumacz(str);
        return 1;
    }
#endif OTHER_LANGUAGES
    say(koloruj_mowe(VBFC_SAY(emote_str_fot) + ": ", formated_str + "\n", 1));

    return 1;
}
#endif FREE_EMOTE

/*
 * Function name: communicate
 * Description  : This function is called whenever the player wants to say
 *                something. We have to put it here rather than in a soul
 *                because people can use the single quote ' as alias for
 *                say.
 * Arguments    : string str - the command line argument.
 * Returns      : int 1/0 - success/failure.
 */
public varargs int
communicate(string str = "", int lang = 0)
{
    mixed tmp;

    if(!strlen(str))
    {
        write("W ostatniej chwili decydujesz si� nic nie m�wi�.\n");
        saybb(QCIMIE(this_player(), PL_MIA) + " wygl�da jakby chcia�" +
            this_player()->koncowka("", "a", "o") + " co� powiedzie�, " +
            "ale w ostatniej chwili si� rozmysli�" +
            this_player()->koncowka("", "a", "o") + ".\n");

        return 1;
    }

    CHECK_MOUTH_BLOCKED;

    //Sprawdzamy czy gracz u�ywa mowy z free emotem
    if (wildmatch(":*:*", str))
    {
        string tstr = str[1..];
        if (tstr[0] == ' ')
            tstr = tstr[1..];
        if (communicate_to(tstr, lang, 1))
            return 1;
    }
    else
    {
        if (communicate_to(str, lang, 0))
            return 1;
    }

#if FREE_EMOTE
    if(emote_communicate(str, lang))
        return 1;
#endif FREE_EMOTE

    if (this_player()->query_option(OPT_ECHO))
    {
#if OTHER_LANGUAGES
        //FIXME: W tym miejscu trzeba wprowadzi� zmiany dotycz�ce j�zyka.
        if (lang)
            write(koloruj_mowe(capitalize(this_object()->actor_race_sound()) + " po krasnoludzku: ",
                FORMAT_SPEECH(SPEECH_DRUNK(str,TP)) + "\n", TP));
            /* jak bedzie wiecej jezykow to doda sie automatyczne rozpoznawanie i
               moze ocene uma np. kaleczac krasnoludzka mowe mowisz */
        else
#endif OTHER_LANGUAGES
            write(koloruj_mowe(capitalize(TP->actor_race_sound()) + ": ",
                   FORMAT_SPEECH(SPEECH_DRUNK(str,TP)) + "\n", TP));
    }
    else
        write("Ju�.\n");

#if OTHER_LANGUAGES
    //FIXME: Rozpoznawanie j�zyka tu trzeba dopisa�
    if(lang)
    {
        tumacz(str);
        return 1;
    }
#endif OTHER_LANGUAGES

    say(koloruj_mowe(VBFC_SAY("") + ": ", FORMAT_SPEECH(SPEECH_DRUNK(str, TP)) + "\n", 1));

    return 1;
}

/**
 * Funkcja u�ywana w communicate do przekazania tekstu pozosta�ym gracz�
 * znajduj�cym si� na lokacji.
 */
public string
vbfc_say(string emote="", mixed language=0)
{
    object for_ob = vbfc_object();

#if FREE_EMOTE
#if PROCESS_FREE_EMOTE
    if(strlen(emote))
        emote = process_string(change_vbfc(emote));
#endif
    if(emote[0] != ' ')
        emote = " " + emote;
    if(emote[-1] == ' ')
        emote = emote[..-2];
#else
    emote = "";
#endif FREE_EMOTE

#if OTHER_LANGUAGES
    if(!intp(language))
        language = 0;
#else
    language = 0;
#endif OTHER_LANGUAGES

    if (CAN_SEE_IN_ROOM(for_ob) && TP->check_seen(for_ob))
        return TP->query_Imie(for_ob, PL_MIA) + " " + TP->race_sound() + emote;
    else
        return "S�yszysz dochodz�cy z ciemno�ci g�os " +
            (for_ob->query_met(TP) ? TP->query_met_name(PL_DOP)  :
            "jakie" + TP->koncowka("go�", "j�") + " " + TP->query_rasa(PL_DOP));
}

/**
 * Funkcja u�ywana w communicate_to do przekazywania tekstu pozosta�ym gracz�.
 */
public varargs string
vbfc_say_to(string emote="", mixed language=0, ...)
{
    object for_ob = vbfc_object();

#if FREE_EMOTE
#if PARSE_FREE_EMOTE
    if(strlen(emote))
        emote = process_string(change_vbfc(emote));
#endif PARSE_FREE_EMOTE
    if(emote[0] != ' ')
        emote  = " " + emote;
    if(emote[-1] == ' ')
        emote = emote[..-2];
#else
    emote = "";
#endif FREE_EMOTE

#if OTHER_LANGUAGES
    if(!intp(language))
        language = 0;
#else
    language = 0;
#endif OTHER_LANGUAGES

    object *oblist;
    string do_kogo;
    if(sizeof(argv) == 1 && !find_object(argv[0]))
        do_kogo = argv[0];
    else
    {
        oblist = map(argv, find_object);
        if(!sizeof(oblist))
            do_kogo = "siebie";
        else
        {
            //Sortujemy tak, �eby gracz do kt�rego idzie komunikat
            //zawsze by� pierwszy.
            if(member_array(for_ob, oblist) > 0)
            {
                oblist -= ({for_ob});
                oblist = ({for_ob}) + oblist;
            }
            do_kogo = FO_COMPOSITE_LIVE(oblist, for_ob, PL_DOP);
        }
    }

    if (CAN_SEE_IN_ROOM(for_ob) && TP->check_seen(for_ob))
        return TP->query_Imie(for_ob, PL_MIA) + " " + TP->race_sound() + " do " + do_kogo + emote;
    else
        return "S�yszysz dochodz�cy z ciemno�ci g�os " +
            (for_ob->query_met(TP) ? TP->query_met_name(PL_DOP)  :
            "jakie" + TP->koncowka("go�", "j�") + " " + TP->query_rasa(PL_DOP));
}

