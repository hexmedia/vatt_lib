/**
 * \file /cmd/live/communication/singing.c
 *
 * Kod �piewania by Artonul@Vatt'ghern
 * Data rozpoczecia projektu - 03.12.2006
 * Balansacja i male poprawki - 04.12.2006
 * Poprawki, i przeniesienie do soula communication.c - Krun
 *
 * TODO:
 *  - g�os nie mo�e �piewa�... Trzeba to zmieni�:)
 */

#pragma no_clone
#pragma no_inherit
#pragma strict_types

#include <exp.h>
#include <ss_types.h>

#define ILE_SPIEWAL         "_npc_i_ile_spiewal"
#define ILE_MOZE_SPIEWAC    "_npc_i_ile_moze_spiewac"

/* Prototypy */
public int czy_spiewaj(string str);  // Funkcja sprawdzajaca co chcemy zaspiewac

void ile_bedzie_spiewac(object spiewak); // Ustalenie w livingu limitu literek do wyspiewania
void podladuj_limit_spiewu(object spiewak); // Spowrotem podladowujemy livingowi limit literek
private int spiewanie(string str, string jak, object spiewak); // Funkcja przetwarzajaca nasz spiew
int spiewaj(string tekst, string jak, object spiewak); // Funkcja okreslajaca opis spiewu oraz wyswietlajaca go
string opis_spiewu(string jak, object spiewak); // Funkcja opisujaca spiew

/*
 * Nazwa funkcji: czy_spiewaj
 * Opis         : Funkcja sprawdza co chcemy zaspiewac i ewentualnie dodajaca belkot do
 *                naszego tekstu.
 * Argumenty    : string str - pobiera tekst wpisywany przez gracza po komendzie
 * Zwraca       : 1/0 - Prawda jesli wszystko sie powiedzie
 */
public int
czy_spiewaj(string str)
{
    string tekst;
    string jak = "domy^slnie";

    notify_fail("Za^spiewaj [jak:] co?\n");

    if (!strlen(str))
        return 0;

    if (parse_command(str, all_inventory(this_player()),
        "%w %s", jak, tekst) && (jak[-1..-1] == ":"))
    {
        jak = jak[..-2];

        if(TP->query_intoxicated() > 50)
            spiewanie(SPEECH_DRUNK(str[(strlen(jak)+1)..], TP), jak, TP);
        else
            spiewanie(str[(strlen(jak)+1)..], jak, TP);
    }
    else if (parse_command(str, all_inventory(this_player()),
        "%w ':' %s", jak,tekst))
    {
        if(TP->query_intoxicated() > 50)
            spiewanie(SPEECH_DRUNK(str[(member_array(":", explode(str, ""))+1)..], TP), jak, TP);
        else
            spiewanie(str[(member_array(":", explode(str, ""))+1)..], jak, TP);
    }
    else if(parse_command(str, all_inventory(this_player()),
        "%s", tekst))
    {
        if(TP->query_intoxicated() > 50)
            spiewanie(FORMAT_SPEECH(SPEECH_DRUNK(str, TP)), "domy^slnie", TP);
        else
            spiewanie(FORMAT_SPEECH(str), "domy^slnie", TP);
    }

    return 1;
}

/*
 * Nazwa funkcji: spiewanie
 * Opis         : Funkcja rozdziela tekst spiewu na literki i jesli przekraczamy limit to
 *                poczatkowo ucina pojedyncze, a pozniej wogole przestajemy spiewac. Tutaj
 *                tez przydzielane jest doswiadczenie do umiejetnosci muzykalnosc.
 * Argumenty    : string str - tekst do przetworzenia
 *                string jak - glos, jakim gracz chce spiewac
 *                object spiewak - obiekt spiewajacego gracza
 * Zwraca       : 1
 */
private int
spiewanie(string str, string jak, object spiewak)
{
    if(spiewak->query_prop(ILE_MOZE_SPIEWAC) == 0)
        ile_bedzie_spiewac(spiewak);

    if(spiewak->query_prop("blok_alarmu_spiewania") != 1)
    {
        spiewak->add_prop("blok_alarmu_spiewania", 1);
        set_alarm(30.0, 0.0, &podladuj_limit_spiewu(spiewak));
    }

    int ile_spiewal;
    int limit_gracza;
    int limit_spiewu;

    ile_spiewal = spiewak->query_prop(ILE_SPIEWAL);
    limit_gracza = spiewak->query_prop(ILE_MOZE_SPIEWAC);
    limit_spiewu = (limit_gracza + random(limit_gracza/2) - ile_spiewal);

    //Ca�y algorytm do przepisania bo jest co najmniej durny:P (Krun)

    if(limit_spiewu < 0)
    {
        write("Musisz chwil^e odetchn^a^c zanim b^edziesz " + TP->koncowka("m�g�","mog�a", "mog�o") +" dalej ^spiewa^c.\n");
        return 1;
    }
    else
    {
        //�o Jezu, troche to edytowa�em bo chyba troszke niepotrzebna p�tla by�a.

        str = str[0..(limit_spiewu)];

        spiewak->change_prop(ILE_SPIEWAL, ile_spiewal+strlen(str));

        spiewak->increase_ss(SS_MUSIC, ftoi(EXP_SPIEWANIE_MUSIC*itof(strlen(str))));

        //dodajemy troch� spragnienia :) V. - poprawi�em wz�r coby uwzgl�dnia� d�ugo�� tekstu [Krun]
        if(spiewak->query_soaked() > 5)
            spiewak->set_soaked(spiewak->query_soaked() - ftoi(itof(strlen(str))*0.4));

        spiewaj(str, jak, spiewak);
        return 1;
    }
}


/**
 * Funkcja u�ywana przez VBFC.
 * @param spiewak spiewajacy
 * @return rase lub imie spiewajacego jesli gracz go zna
 */
string
rasa_lub_imie(mixed spiewak, mixed przyp=0)
{
    if(!objectp(spiewak))
    {
        object tmp;
        if(stringp(spiewak))
        {
            if(tmp = find_player(spiewak))
                spiewak = tmp;
            else if(tmp = find_living(spiewak))
                spiewak = tmp;
            else if(tmp = find_object(spiewak))
                spiewak = tmp;
            else
                spiewak = TP;
        }
    }

    if(!intp(przyp))
    {
        if(stringp(przyp))
            przyp = atoi(przyp);
        else
            przyp = 0;
    }

    object fob = previous_object(-1);

    if(fob->query_met(spiewak))
        return capitalize(spiewak->query_name(przyp));
    else
        return spiewak->query_rasa(przyp);
}

/*
 * Nazwa funkcji: spiewaj
 * Opis         : Funkcja ustala koncowe opisy spiewu oraz wyswietla to co widza gracze.
 *                Dodatkowo zwraca moze wyswietlic graczowi ze danym glosem nie moze spiewac.
 * Argumenty    : string tekst - tekst do wyswietlenia
 *                string jak - okreslenie glosu jakim chce spiewac gracz
 *                object spiewak - obiekt spiewajacego
 * Zwraca       : 1
 */
int
spiewaj(string tekst, string jak, object spiewak)
{
    int ile_spiewal = spiewak->query_prop(ILE_SPIEWAL);
    int limit_gracza = spiewak->query_prop(ILE_MOZE_SPIEWAC);
    int limit_spiewu = 2*limit_gracza;
    int tempoi;
    int glosi;
    string gloss;
    string opis = opis_spiewu(jak, spiewak);

    if(ile_spiewal > 0 && ile_spiewal <= limit_gracza/4)
        tempoi = 1;
    else if(ile_spiewal > limit_gracza/4 && ile_spiewal <= limit_gracza/2)
        tempoi = 2;
    else if(ile_spiewal > limit_gracza/2 && ile_spiewal <= limit_gracza)
        tempoi = 3;
    else if(ile_spiewal > limit_gracza && ile_spiewal <= limit_gracza*14/10)
        tempoi = 4;
    else if(ile_spiewal > limit_gracza*14/10 && ile_spiewal <= limit_gracza*2+1)
        tempoi = 5;

    glosi = (spiewak->query_skill(SS_MUSIC)/7) - tempoi;

    switch(glosi)
    {
        case 6..7:
            gloss = ", z rzadka fa^lszuj^ac";
            break;

        case 3..5:
            gloss = ", momentami potwornie fa^lszuj^ac";
            break;

        case -5..2:
            gloss = ", potwornie fa^lszuj^ac";
            break;

        default:
            gloss = "";
    }

    write(koloruj_mowe("^Spiewasz " + opis  + gloss + ":", "\n\t" + tekst + "\n", TP));
    say(({koloruj_mowe(QCIMIE(spiewak, PL_MIA) + " ^spiewa " + opis + gloss + ":\n\t", tekst + "\n", 1),
        koloruj_mowe(QCIMIE(spiewak, PL_MIA) + " �piewa " + opis + gloss + ":\n\t", tekst + "\n", 1),
        koloruj_mowe("G�os " + QIORR(spiewak, PL_DOP) + " �piewa " + opis +
            gloss + ":\n\t", tekst + "\n", 1)}));
    return 1;

}

/*
 * Nazwa funkcji: opis_spiewu
 * Opis         : Funkcja zwraca opis spiewu do przyslowka zalezny od rasy, plci i
 *                umiejetnosci.
 * Argumenty    : string jak - przyslowek okreslajacy typ spiewu
 *                object spiewak - obiekt spiewajacego
 * Zwraca       : string
 */
string
opis_spiewu(string jak, object spiewak)
{
    int ile_spiewal = spiewak->query_prop(ILE_SPIEWAL);
    int limit_gracza = spiewak->query_prop(ILE_MOZE_SPIEWAC);
    int limit_spiewu = 2*limit_gracza;
    int poziom;

    if(spiewak->query_skill(SS_MUSIC) < 30)
    {
        poziom = 1;
    }
    else if(spiewak->query_skill(SS_MUSIC) < 75)
    {
        poziom = 2;
    }
    else
    {
        poziom = 3;
    }

    if(jak == "wolno" && ile_spiewal > limit_gracza)
    {
        jak = "domy^slnie";
    }

    if(jak != "wolno" && jak != "szybko" && jak != "weso^lo" && jak != "wysoko" && jak != "nisko" &&
       jak != "wesolo" && jak != "t�sknie" && jak != "smutno" && jak != "g�o�no" && 
        jak != "cicho" && jak != "mi�kko")
    {
        jak = "domy^slnie";
    }

    if(spiewak->query_race() ~= "cz^lowiek" && spiewak->query_wiek() < 17)
    {
        if(spiewak->query_gender() == G_FEMALE)
        {
            switch(jak)
            {
                case "wolno":
                    switch(poziom)
                    {
                        case 1:
                        return "powoli, staraj^ac si^e dok^ladnie intonowa^c ka^zde s^lowo";
                        break;

                        case 2:
                        return "powoli, uroczo intonuj^ac ka^zde s^lowo";
                        break;

                        case 3:
                        return "powoli, przepi^eknie intonuj^ac ka^zde s^lowo";
                        break;
                    }
                    break;

                    case "szybko":
                    switch(poziom)
                    {
                        case 1:
                        return "^zwawym tempem";
                        break;

                        case 2:
                        return "szybkim tempem";
                        break;

                        case 3:
                        return "szybko i rytmicznie zarazem";
                        break;
                    }
                    break;

                    case "weso^lo":
                    switch(poziom)
                    {
                        case 1:
                        return "weso^lo";
                        break;

                        case 2:
                        return "weso^lym g^losem";
                        break;

                       case 3:
                        return "g^losem napawaj^acym otoczenie rado^sci^a ^zycia";
                        break;
                    }
                    break;

                    case "wysoko":
                    switch(poziom)
                    {
                        case 1:
                        return "piskliwym g^losikiem";
                        break;

                        case 2:
                        return "wysokim, d^xwi^ecznym g^losem";
                        break;

                        case 3:
                        return "wysokim, czystym g^losem";
                        break;
                    }
                    break;

                    case "nisko":
                    switch(poziom)
                    {
                        case 1:
                        return "skrzekliwym g^losem";
                        break;

                        case 2:
                        return "czystym, niskim g^losem";
                        break;

                        case 3:
                        return "niskim, niemal^ze m^eskim g^losem";
                        break;
                    }
                    break;

                    case "t�sknie":
                    switch(poziom)
                    {
                        case 1:
                        return "t�sknie";
                        break;

                        case 2:
                        return "t�sknie, wzruszaj�cym tonem";
                        break;

                        case 3:
                        return "t�sknym, wyciskaj�cym �zy z oczu tonem";
                        break;
                    }
                    break;

                    case "smutno":
                    switch(poziom)
                    {
                        case 1:
                        return "smutnym g�osem";
                        break;

                        case 2:
                        return "smutno, lekko dr��cym g�osem";
                        break;

                        case 3:
                        return "smutno, dr��cym, przejmuj�cym g�osem";
                        break;
                    }
                    break;

                    case "g�o�no":
                    switch(poziom)
                    {
                        case 1:
                        return "krzykliwie";
                        break;

                        case 2:
                        return "g�o�no";
                        break;

                        case 3:
                        return "dono�nym g�osem";
                        break;
                    }
                    break;

                    case "cicho":
                    switch(poziom)
                    {
                        case 1:
                        return "cichym, ledwo s�yszalnym g�osem";
                        break;

                        case 2:
                        return "cicho, niemal szeptem";
                        break;

                        case 3:
                        return "cichym, lecz czystym g�osem";
                        break;
                    }
                    break;

                    case "mi�kko":
                    switch(poziom)
                    {
                        case 1:
                        return "mi�kkim g�osem";
                        break;

                        case 2:
                        return "mi�kkim, nieco rozmarzonym g�osem";
                        break;

                        case 3:
                        return "mi�kkim, rozmarzonym g�osem";
                        break;
                    }
                    break;

                    case "domy^slnie":
                    switch(poziom)
                    {
                        case 1:
                            return "skrzecz^acym g^losikiem";
                        break;

                        case 2:
                            return "d^xwi^ecznym g^losem";
                        break;

                        case 3:
                            return "przepi^eknym, elfim g^losem";
                        break;
                    }
                    break;
                }
            }
            else
            {
                switch(jak)
                {
                    case "wolno":
                    switch(poziom)
                    {
                        case 1:
                        return "powoli, staraj^ac si^e dok^ladnie intonowa^c ka^zde s^lowo";
                        break;

                        case 2:
                        return "powoli, dok^ladnie intonuj^ac ka^zde s^lowo";
                        break;

                        case 3:
                        return "powoli, wyra^xnie intonuj^ac ka^zde s^lowo";
                        break;
                    }
                    break;

                    case "szybko":
                    switch(poziom)
                    {
                        case 1:
                        return "^zwawym tempem";
                        break;

                        case 2:
                        return "szybkim tempem";
                        break;

                        case 3:
                        return "szybko i rytmicznie zarazem";
                        break;
                    }
                    break;

                    case "weso^lo":
                    switch(poziom)
                    {
                        case 1:
                        return "weso^lo";
                        break;

                        case 2:
                        return "weso^lym g^losem";
                        break;

                       case 3:
                        return "g^losem napawaj^acym otoczenie rado^sci^a ^zycia";
                        break;
                    }
                    break;

                    case "wysoko":
                    switch(poziom)
                    {
                        case 1:
                        return "skrzekliwym g^losem";
                        break;

                        case 2:
                        return "wysokim, d^xwi^ecznym g^losem";
                        break;

                        case 3:
                        return "bardzo wysokim, niemal^ze kobiecym g^losem";
                        break;
                    }
                    break;

                    case "nisko":
                    switch(poziom)
                    {
                        case 1:
                        return "oci^e^zale niskim g^losem";
                        break;

                        case 2:
                        return "burkliwym, niskim g^losem";
                        break;

                        case 3:
                        return "niskim, d^zwi^ecznym g^losem";
                        break;
                    }
                    break;

                    case "t�sknie":
                    switch(poziom)
                    {
                        case 1:
                        return "t�sknie";
                        break;

                        case 2:
                        return "t�sknie, wzruszaj�cym tonem";
                        break;

                        case 3:
                        return "t�sknym, wyciskaj�cym �zy z oczu tonem";
                        break;
                    }
                    break;

                    case "smutno":
                    switch(poziom)
                    {
                        case 1:
                        return "smutnym g�osem";
                        break;

                        case 2:
                        return "smutno, lekko dr��cym g�osem";
                        break;

                        case 3:
                        return "smutno, dr��cym, przejmuj�cym g�osem";
                        break;
                    }
                    break;

                    case "g�o�no":
                    switch(poziom)
                    {
                        case 1:
                        return "krzykliwie";
                        break;

                        case 2:
                        return "g�o�no";
                        break;

                        case 3:
                        return "dono�nym g�osem";
                        break;
                    }
                    break;

                    case "cicho":
                    switch(poziom)
                    {
                        case 1:
                        return "cichym, ledwo s�yszalnym g�osem";
                        break;

                        case 2:
                        return "cicho, niemal szeptem";
                        break;

                        case 3:
                        return "cichym, lecz czystym g�osem";
                        break;
                    }
                    break;

                    case "mi�kko":
                    switch(poziom)
                    {
                        case 1:
                        return "mi�kkim g�osem";
                        break;

                        case 2:
                        return "mi�kkim, nieco rozmarzonym g�osem";
                        break;

                        case 3:
                        return "mi�kkim, rozmarzonym g�osem";
                        break;
                    }
                    break;

                    case "domy^slnie":
                    switch(poziom)
                    {
                        case 1:
                        return "skrzecz^acym g^losem";
                        break;

                        case 2:
                        return "d^xwi^ecznym g^losem";
                        break;

                        case 3:
                        return "powa^znym, wibruj^acym g^losem";
                        break;
                    }
                    break;
                }
            }
    }
    else
    {
        switch(spiewak->query_race())
        {
            case "p^o^lelf":
            if(spiewak->query_gender() == G_FEMALE)
            {
                switch(jak)
                {
                    case "wolno":
                    switch(poziom)
                    {
                        case 1:
                        return "powoli, staraj^ac si^e dok^ladnie intonowa^c ka^zde s^lowo";
                        break;

                        case 2:
                        return "powoli, dok^ladnie intonuj^ac ka^zde s^lowo";
                        break;

                        case 3:
                        return "powoli, pi^eknie intonuj^ac ka^zde s^lowo";
                        break;
                    }
                    break;

                    case "szybko":
                    switch(poziom)
                    {
                        case 1:
                        return "^zwawym tempem";
                        break;

                        case 2:
                        return "szybkim tempem";
                        break;

                        case 3:
                        return "szybko i rytmicznie zarazem";
                        break;
                    }
                    break;

                    case "weso^lo":
                    switch(poziom)
                    {
                        case 1:
                        return "weso^lo";
                        break;

                        case 2:
                        return "weso^lym g^losem";
                        break;

                       case 3:
                        return "g^losem napawaj^acym otoczenie rado^sci^a ^zycia";
                        break;
                    }
                    break;

                    case "wysoko":
                    switch(poziom)
                    {
                        case 1:
                        return "piskliwym g^losikiem";
                        break;

                        case 2:
                        return "wysokim, d^xwi^ecznym g^losem";
                        break;

                        case 3:
                        return "wysokim, czystym g^losem";
                        break;
                    }
                    break;

                    case "nisko":
                    switch(poziom)
                    {
                        case 1:
                        return "skrzekliwym g^losem";
                        break;

                        case 2:
                        return "czystym, niskim g^losem";
                        break;

                        case 3:
                        return "niskim, niemal^ze m^eskim g^losem";
                        break;
                    }
                    break;

                    case "t�sknie":
                    switch(poziom)
                    {
                        case 1:
                        return "t�sknie";
                        break;

                        case 2:
                        return "t�sknie, wzruszaj�cym tonem";
                        break;

                        case 3:
                        return "t�sknym, wyciskaj�cym �zy z oczu tonem";
                        break;
                    }
                    break;

                    case "smutno":
                    switch(poziom)
                    {
                        case 1:
                        return "smutnym g�osem";
                        break;

                        case 2:
                        return "smutno, lekko dr��cym g�osem";
                        break;

                        case 3:
                        return "smutno, dr��cym, przejmuj�cym g�osem";
                        break;
                    }
                    break;

                    case "g�o�no":
                    switch(poziom)
                    {
                        case 1:
                        return "krzykliwie";
                        break;

                        case 2:
                        return "g�o�no";
                        break;

                        case 3:
                        return "dono�nym g�osem";
                        break;
                    }
                    break;

                    case "cicho":
                    switch(poziom)
                    {
                        case 1:
                        return "cichym, ledwo s�yszalnym g�osem";
                        break;

                        case 2:
                        return "cicho, niemal szeptem";
                        break;

                        case 3:
                        return "cichym, lecz czystym g�osem";
                        break;
                    }
                    break;

                    case "mi�kko":
                    switch(poziom)
                    {
                        case 1:
                        return "mi�kkim g�osem";
                        break;

                        case 2:
                        return "mi�kkim, nieco rozmarzonym g�osem";
                        break;

                        case 3:
                        return "mi�kkim, rozmarzonym g�osem";
                        break;
                    }
                    break;

                    case "domy^slnie":
                    switch(poziom)
                    {
                        case 1:
                        return "skrzecz^acym g^losikiem";
                        break;

                        case 2:
                        return "d^xwi^ecznym g^losem";
                        break;

                        case 3:
                        return "przepi^eknym, niemal^ze elfim g^losem";
                        break;
                    }
                    break;
                }
            }
            else
            {
                switch(jak)
                {
                    case "wolno":
                    switch(poziom)
                    {
                        case 1:
                        return "powoli, staraj^ac si^e dok^ladnie intonowa^c ka^zde s^lowo";
                        break;

                        case 2:
                        return "powoli, dok^ladnie intonuj^ac ka^zde s^lowo";
                        break;

                        case 3:
                        return "powoli, wyra^xnie intonuj^ac ka^zde s^lowo";
                        break;
                    }
                    break;

                    case "szybko":
                    switch(poziom)
                    {
                        case 1:
                        return "^zwawym tempem";
                        break;

                        case 2:
                        return "szybkim tempem";
                        break;

                        case 3:
                        return "szybko i rytmicznie zarazem";
                        break;
                    }
                    break;

                    case "weso^lo":
                    switch(poziom)
                    {
                        case 1:
                        return "weso^lo";
                        break;

                        case 2:
                        return "weso^lym g^losem";
                        break;

                       case 3:
                        return "g^losem napawaj^acym otoczenie rado^sci^a ^zycia";
                        break;
                    }
                    break;

                    case "wysoko":
                    switch(poziom)
                    {
                        case 1:
                        return "skrzekliwym g^losem";
                        break;

                        case 2:
                        return "wysokim, d^xwi^ecznym g^losem";
                        break;

                        case 3:
                        return "bardzo wysokim, niemal^ze kobiecym g^losem";
                        break;
                    }
                    break;

                    case "nisko":
                    switch(poziom)
                    {
                        case 1:
                        return "oci^e^zale niskim g^losem";
                        break;

                        case 2:
                        return "burkliwym, niskim g^losem";
                        break;

                        case 3:
                        return "niskim, d^zwi^ecznym g^losem";
                        break;
                    }
                    break;

                    case "t�sknie":
                    switch(poziom)
                    {
                        case 1:
                        return "t�sknie";
                        break;

                        case 2:
                        return "t�sknie, wzruszaj�cym tonem";
                        break;

                        case 3:
                        return "t�sknym, wyciskaj�cym �zy z oczu tonem";
                        break;
                    }
                    break;

                    case "smutno":
                    switch(poziom)
                    {
                        case 1:
                        return "smutnym g�osem";
                        break;

                        case 2:
                        return "smutno, lekko dr��cym g�osem";
                        break;

                        case 3:
                        return "smutno, dr��cym, przejmuj�cym g�osem";
                        break;
                    }
                    break;

                    case "g�o�no":
                    switch(poziom)
                    {
                        case 1:
                        return "krzykliwie";
                        break;

                        case 2:
                        return "g�o�no";
                        break;

                        case 3:
                        return "dono�nym g�osem";
                        break;
                    }
                    break;

                    case "cicho":
                    switch(poziom)
                    {
                        case 1:
                        return "cichym, ledwo s�yszalnym g�osem";
                        break;

                        case 2:
                        return "cicho, niemal szeptem";
                        break;

                        case 3:
                        return "cichym, lecz czystym g�osem";
                        break;
                    }
                    break;

                    case "mi�kko":
                    switch(poziom)
                    {
                        case 1:
                        return "mi�kkim g�osem";
                        break;

                        case 2:
                        return "mi�kkim, nieco rozmarzonym g�osem";
                        break;

                        case 3:
                        return "mi�kkim, rozmarzonym g�osem";
                        break;
                    }
                    break;

                    case "domy^slnie":
                    switch(poziom)
                    {
                        case 1:
                        return "skrzecz^acym g^losem";
                        break;

                        case 2:
                        return "d^xwi^ecznym g^losem";
                        break;

                        case 3:
                        return "powa^znym, wibruj^acym g^losem";
                        break;
                    }
                    break;
                }
            }
            break;

            case "elf":
            if(spiewak->query_gender() == G_FEMALE)
            {
                switch(jak)
                {
                    case "wolno":
                    switch(poziom)
                    {
                        case 1:
                        return "powoli, staraj^ac si^e dok^ladnie intonowa^c ka^zde s^lowo";
                        break;

                        case 2:
                        return "powoli, uroczo intonuj^ac ka^zde s^lowo";
                        break;

                        case 3:
                        return "powoli, przepi^eknie intonuj^ac ka^zde s^lowo";
                        break;
                    }
                    break;

                    case "szybko":
                    switch(poziom)
                    {
                        case 1:
                        return "^zwawym tempem";
                        break;

                        case 2:
                        return "szybkim tempem";
                        break;

                        case 3:
                        return "szybko i rytmicznie zarazem";
                        break;
                    }
                    break;

                    case "weso^lo":
                    switch(poziom)
                    {
                        case 1:
                        return "weso^lo";
                        break;

                        case 2:
                        return "weso^lym g^losem";
                        break;

                       case 3:
                        return "g^losem napawaj^acym otoczenie rado^sci^a ^zycia";
                        break;
                    }
                    break;

                    case "wysoko":
                    switch(poziom)
                    {
                        case 1:
                        return "piskliwym g^losikiem";
                        break;

                        case 2:
                        return "wysokim, d^xwi^ecznym g^losem";
                        break;

                        case 3:
                        return "wysokim, czystym g^losem";
                        break;
                    }
                    break;

                    case "nisko":
                    switch(poziom)
                    {
                        case 1:
                        return "skrzekliwym g^losem";
                        break;

                        case 2:
                        return "czystym, niskim g^losem";
                        break;

                        case 3:
                        return "niskim, niemal^ze m^eskim g^losem";
                        break;
                    }
                    break;

                    case "t�sknie":
                    switch(poziom)
                    {
                        case 1:
                        return "t�sknie";
                        break;

                        case 2:
                        return "t�sknie, wzruszaj�cym tonem";
                        break;

                        case 3:
                        return "t�sknym, wyciskaj�cym �zy z oczu tonem";
                        break;
                    }
                    break;

                    case "smutno":
                    switch(poziom)
                    {
                        case 1:
                        return "smutnym g�osem";
                        break;

                        case 2:
                        return "smutno, lekko dr��cym g�osem";
                        break;

                        case 3:
                        return "smutno, dr��cym, przejmuj�cym g�osem";
                        break;
                    }
                    break;

                    case "g�o�no":
                    switch(poziom)
                    {
                        case 1:
                        return "krzykliwie";
                        break;

                        case 2:
                        return "g�o�no";
                        break;

                        case 3:
                        return "dono�nym g�osem";
                        break;
                    }
                    break;

                    case "cicho":
                    switch(poziom)
                    {
                        case 1:
                        return "cichym, ledwo s�yszalnym g�osem";
                        break;

                        case 2:
                        return "cicho, niemal szeptem";
                        break;

                        case 3:
                        return "cichym, lecz czystym g�osem";
                        break;
                    }
                    break;

                    case "mi�kko":
                    switch(poziom)
                    {
                        case 1:
                        return "mi�kkim g�osem";
                        break;

                        case 2:
                        return "mi�kkim, nieco rozmarzonym g�osem";
                        break;

                        case 3:
                        return "mi�kkim, rozmarzonym g�osem";
                        break;
                    }
                    break;

                    case "domy^slnie":
                    switch(poziom)
                    {
                        case 1:
                        return "skrzecz^acym g^losikiem";
                        break;

                        case 2:
                        return "d^xwi^ecznym g^losem";
                        break;

                        case 3:
                        return "przepi^eknym, elfim g^losem";
                        break;
                    }
                    break;
                }
            }
            else
            {
                switch(jak)
                {
                    case "wolno":
                    switch(poziom)
                    {
                        case 1:
                        return "powoli, staraj^ac si^e dok^ladnie intonowa^c ka^zde s^lowo";
                        break;

                        case 2:
                        return "powoli, dok^ladnie intonuj^ac ka^zde s^lowo";
                        break;

                        case 3:
                        return "powoli, wyra^xnie intonuj^ac ka^zde s^lowo";
                        break;
                    }
                    break;

                    case "szybko":
                    switch(poziom)
                    {
                        case 1:
                        return "^zwawym tempem";
                        break;

                        case 2:
                        return "szybkim tempem";
                        break;

                        case 3:
                        return "szybko i rytmicznie zarazem";
                        break;
                    }
                    break;

                    case "weso^lo":
                    switch(poziom)
                    {
                        case 1:
                        return "weso^lo";
                        break;

                        case 2:
                        return "weso^lym g^losem";
                        break;

                       case 3:
                        return "g^losem napawaj^acym otoczenie rado^sci^a ^zycia";
                        break;
                    }
                    break;

                    case "wysoko":
                    switch(poziom)
                    {
                        case 1:
                        return "skrzekliwym g^losem";
                        break;

                        case 2:
                        return "wysokim, d^xwi^ecznym g^losem";
                        break;

                        case 3:
                        return "bardzo wysokim, niemal^ze kobiecym g^losem";
                        break;
                    }
                    break;

                    case "nisko":
                    switch(poziom)
                    {
                        case 1:
                        return "oci^e^zale niskim g^losem";
                        break;

                        case 2:
                        return "burkliwym, niskim g^losem";
                        break;

                        case 3:
                        return "niskim, d^zwi^ecznym g^losem";
                        break;
                    }
                    break;

                    case "t�sknie":
                    switch(poziom)
                    {
                        case 1:
                        return "t�sknie";
                        break;

                        case 2:
                        return "t�sknie, wzruszaj�cym tonem";
                        break;

                        case 3:
                        return "t�sknym, wyciskaj�cym �zy z oczu tonem";
                        break;
                    }
                    break;

                    case "smutno":
                    switch(poziom)
                    {
                        case 1:
                        return "smutnym g�osem";
                        break;

                        case 2:
                        return "smutno, lekko dr��cym g�osem";
                        break;

                        case 3:
                        return "smutno, dr��cym, przejmuj�cym g�osem";
                        break;
                    }
                    break;

                    case "g�o�no":
                    switch(poziom)
                    {
                        case 1:
                        return "krzykliwie";
                        break;

                        case 2:
                        return "g�o�no";
                        break;

                        case 3:
                        return "dono�nym g�osem";
                        break;
                    }
                    break;

                    case "cicho":
                    switch(poziom)
                    {
                        case 1:
                        return "cichym, ledwo s�yszalnym g�osem";
                        break;

                        case 2:
                        return "cicho, niemal szeptem";
                        break;

                        case 3:
                        return "cichym, lecz czystym g�osem";
                        break;
                    }
                    break;

                    case "mi�kko":
                    switch(poziom)
                    {
                        case 1:
                        return "mi�kkim g�osem";
                        break;

                        case 2:
                        return "mi�kkim, nieco rozmarzonym g�osem";
                        break;

                        case 3:
                        return "mi�kkim, rozmarzonym g�osem";
                        break;
                    }
                    break;

                    case "domy^slnie":
                    switch(poziom)
                    {
                        case 1:
                        return "skrzecz^acym g^losem";
                        break;

                        case 2:
                        return "d^xwi^ecznym g^losem";
                        break;

                        case 3:
                        return "powa^znym, wibruj^acym g^losem";
                        break;
                    }
                    break;
                }
            }
            break;

            case "krasnolud":
            if(spiewak->query_gender() == G_FEMALE)
            {
                switch(jak)
                {
                    case "wolno":
                    switch(poziom)
                    {
                        case 1:
                        return "powoli, staraj^ac si^e dok^ladnie intonowa^c ka^zde s^lowo";
                        break;

                        case 2:
                        return "powoli, szorstko intonuj^ac ka^zde s^lowo";
                        break;

                        case 3:
                        return "powoli, powa^znie intonuj^ac ka^zde s^lowo";
                        break;
                    }
                    break;

                    case "szybko":
                    switch(poziom)
                    {
                        case 1:
                        return "^zwawym tempem";
                        break;

                        case 2:
                        return "szybkim tempem";
                        break;

                        case 3:
                        return "szybko i rytmicznie zarazem";
                        break;
                    }
                    break;

                    case "weso^lo":
                    switch(poziom)
                    {
                        case 1:
                        return "weso^lo";
                        break;

                        case 2:
                        return "weso^lym g^losem";
                        break;

                       case 3:
                        return "g^losem napawaj^acym otoczenie rado^sci^a ^zycia";
                        break;
                    }
                    break;

                    case "wysoko":
                    switch(poziom)
                    {
                        case 1:
                        return "piskliwym g^losikiem";
                        break;

                        case 2:
                        return "wysokim, pustym g^losem";
                        break;

                        case 3:
                        return "wysokim, burkliwym g^losem";
                        break;
                    }
                    break;

                    case "nisko":
                    switch(poziom)
                    {
                        case 1:
                        return "skrzekliwym g^losem";
                        break;

                        case 2:
                        return "czystym, niskim g^losem";
                        break;

                        case 3:
                        return "niskim, b^ebni^acym w uszach g^losem";
                        break;
                    }
                    break;

                    case "g�o�no":
                    switch(poziom)
                    {
                        case 1:
                        return "krzykliwie";
                        break;

                        case 2:
                        return "g�o�no";
                        break;

                        case 3:
                        return "dono�nym g�osem";
                        break;
                    }
                    break;

                    case "cicho":
                    switch(poziom)
                    {
                        case 1:
                        return "cichym, ledwo s�yszalnym g�osem";
                        break;

                        case 2:
                        return "cicho, niemal szeptem";
                        break;

                        case 3:
                        return "cichym, lecz czystym g�osem";
                        break;
                    }
                    break;

                    case "domy^slnie":
                    switch(poziom)
                    {
                        case 1:
                        return "burkliwym g^losem";
                        break;

                        case 2:
                        return "pot^e^znym g^losem";
                        break;

                        case 3:
                        return "pot^eznym, d^xwi^ecz^acym w uszach g^losem";
                        break;
                    }
                    break;
                }
            }
            else
            {
                switch(jak)
                {
                    case "wolno":
                    switch(poziom)
                    {
                        case 1:
                        return "powoli, staraj^ac si^e dok^ladnie intonowa^c ka^zde s^lowo";
                        break;

                        case 2:
                        return "powoli, dok^ladnie intonuj^ac ka^zde s^lowo";
                        break;

                        case 3:
                        return "powoli, wyra^xnie intonuj^ac ka^zde s^lowo";
                        break;
                    }
                    break;

                    case "szybko":
                    switch(poziom)
                    {
                        case 1:
                        return "^zwawym tempem";
                        break;

                        case 2:
                        return "szybkim tempem";
                        break;

                        case 3:
                        return "szybko i rytmicznie zarazem";
                        break;
                    }
                    break;

                    case "weso^lo":
                    switch(poziom)
                    {
                        case 1:
                        return "weso^lo";
                        break;

                        case 2:
                        return "weso^lym g^losem";
                        break;

                       case 3:
                        return "g^losem napawaj^acym otoczenie rado^sci^a ^zycia";
                        break;
                    }
                    break;

                    case "wysoko":
                    switch(poziom)
                    {
                        case 1:
                        return "skrzekliwym g^losem";
                        break;

                        case 2:
                        return "^swircz^acym, prostym g^losem";
                        break;

                        case 3:
                        return "wysokim, pustym g^losem";
                        break;
                    }
                    break;

                    case "nisko":
                    switch(poziom)
                    {
                        case 1:
                        return "oci^e^zale niskim g^losem";
                        break;

                        case 2:
                        return "burkliwym, niskim g^losem";
                        break;

                        case 3:
                        return "niskim, d^zwi^ecznym g^losem";
                        break;
                    }
                    break;

                    case "g�o�no":
                    switch(poziom)
                    {
                        case 1:
                        return "krzykliwie";
                        break;

                        case 2:
                        return "g�o�no";
                        break;

                        case 3:
                        return "dono�nym g�osem";
                        break;
                    }
                    break;

                    case "cicho":
                    switch(poziom)
                    {
                        case 1:
                        return "cichym, ledwo s�yszalnym g�osem";
                        break;

                        case 2:
                        return "cicho, niemal szeptem";
                        break;

                        case 3:
                        return "cichym, lecz czystym g�osem";
                        break;
                    }
                    break;

                    case "domy^slnie":
                    switch(poziom)
                    {
                        case 1:
                        return "skrzecz^acym g^losem";
                        break;

                        case 2:
                        return "d^xwi^ecznym g^losem";
                        break;

                        case 3:
                        return "powa^znym, wibruj^acym g^losem";
                        break;
                    }
                    break;
                }
            }
            break;

            case "gnom":
            if(spiewak->query_gender() == G_FEMALE)
            {
                switch(jak)
                {
                    case "wolno":
                    switch(poziom)
                    {
                        case 1:
                        return "powoli, staraj^ac si^e dok^ladnie intonowa^c ka^zde s^lowo";
                        break;

                        case 2:
                        return "powoli, skrzekliwym g^losem";
                        break;

                        case 3:
                        return "powoli, rytmicznie intonuj^ac ka^zde s^lowo";
                        break;
                    }
                    break;

                    case "szybko":
                    switch(poziom)
                    {
                        case 1:
                        return "^zwawym tempem";
                        break;

                        case 2:
                        return "szybkim tempem";
                        break;

                        case 3:
                        return "szybko i rytmicznie zarazem";
                        break;
                    }
                    break;

                    case "weso^lo":
                    switch(poziom)
                    {
                        case 1:
                        return "weso^lo";
                        break;

                        case 2:
                        return "weso^lym g^losem";
                        break;

                       case 3:
                        return "skrzekliwym g^losem napawaj^acym otoczenie rado^sci^a ^zycia";
                        break;
                    }
                    break;

                    case "wysoko":
                    switch(poziom)
                    {
                        case 1:
                        return "piskliwym g^losikiem";
                        break;

                        case 2:
                        return "wysokim, d^xwi^ecznym g^losem";
                        break;

                        case 3:
                        return "wysokim, czystym g^losem";
                        break;
                    }
                    break;

                    case "nisko":
                    switch(poziom)
                    {
                        case 1:
                        return "skrzekliwym g^losem";
                        break;

                        case 2:
                        return "czystym, niskim g^losem";
                        break;

                        case 3:
                        return "niskim, niemal^ze krasnoludzkim g^losem";
                        break;
                    }
                    break;

                    case "g�o�no":
                    switch(poziom)
                    {
                        case 1:
                        return "krzykliwie";
                        break;

                        case 2:
                        return "g�o�no";
                        break;

                        case 3:
                        return "dono�nym g�osem";
                        break;
                    }
                    break;

                    case "cicho":
                    switch(poziom)
                    {
                        case 1:
                        return "cichym, ledwo s�yszalnym g�osem";
                        break;

                        case 2:
                        return "cicho, niemal szeptem";
                        break;

                        case 3:
                        return "cichym, lecz czystym g�osem";
                        break;
                    }
                    break;

                    case "domy^slnie":
                    switch(poziom)
                    {
                        case 1:
                        return "skrzecz^acym g^losikiem";
                        break;

                        case 2:
                        return "skrzecz^acym g^losem";
                        break;

                        case 3:
                        return "skrzekliwym g^losem, z syntetyczn^a wr^ecz dok^ladno^sci^a";
                        break;
                    }
                    break;
                }
            }
            else
            {
                switch(jak)
                {
                    case "wolno":
                    switch(poziom)
                    {
                        case 1:
                        return "powoli, staraj^ac si^e dok^ladnie intonowa^c ka^zde s^lowo";
                        break;

                        case 2:
                        return "powoli, dok^ladnie intonuj^ac ka^zde s^lowo";
                        break;

                        case 3:
                        return "powoli, wyra^xnie intonuj^ac ka^zde s^lowo";
                        break;
                    }
                    break;

                    case "szybko":
                    switch(poziom)
                    {
                        case 1:
                        return "^zwawym tempem";
                        break;

                        case 2:
                        return "szybkim tempem";
                        break;

                        case 3:
                        return "szybko i rytmicznie zarazem";
                        break;
                    }
                    break;

                    case "weso^lo":
                    switch(poziom)
                    {
                        case 1:
                        return "weso^lo";
                        break;

                        case 2:
                        return "weso^lym, skrzekliwym g^losem";
                        break;

                       case 3:
                        return "skrzekliwym g^losem napawaj^acym otoczenie rado^sci^a ^zycia";
                        break;
                    }
                    break;

                    case "wysoko":
                    switch(poziom)
                    {
                        case 1:
                        return "skrzekliwym g^losem";
                        break;

                        case 2:
                        return "wysokim, skrzekliwym g^losem";
                        break;

                        case 3:
                        return "bardzo skrzekliwym, wibruj^acym g^losem";
                        break;
                    }
                    break;

                    case "nisko":
                    switch(poziom)
                    {
                        case 1:
                        return "oci^e^zale niskim g^losem";
                        break;

                        case 2:
                        return "burkliwym, niskim g^losem";
                        break;

                        case 3:
                        return "niskim, skrzekliwym g^losem";
                        break;
                    }
                    break;

                    case "g�o�no":
                    switch(poziom)
                    {
                        case 1:
                        return "krzykliwie";
                        break;

                        case 2:
                        return "g�o�no";
                        break;

                        case 3:
                        return "dono�nym g�osem";
                        break;
                    }
                    break;

                    case "cicho":
                    switch(poziom)
                    {
                        case 1:
                        return "cichym, ledwo s�yszalnym g�osem";
                        break;

                        case 2:
                        return "cicho, niemal szeptem";
                        break;

                        case 3:
                        return "cichym, lecz czystym g�osem";
                        break;
                    }
                    break;

                    case "domy^slnie":
                    switch(poziom)
                    {
                        case 1:
                        return "skrzecz^acym g^losem";
                        break;

                        case 2:
                        return "d^xwi^ecznym g^losem";
                        break;

                        case 3:
                        return "powa^znym, syntetycznie dok^ladnym g^losem";
                        break;
                    }
                    break;
                }
            }
            break;

            case "nizio^lek":
            if(spiewak->query_gender() == G_FEMALE)
            {
                switch(jak)
                {
                    case "wolno":
                    switch(poziom)
                    {
                        case 1:
                        return "powoli, staraj^ac si^e dok^ladnie intonowa^c ka^zde s^lowo";
                        break;

                        case 2:
                        return "powoli, dok^ladnie intonuj^ac ka^zde s^lowo";
                        break;

                        case 3:
                        return "powoli, pi^eknie intonuj^ac ka^zde s^lowo";
                        break;
                    }
                    break;

                    case "szybko":
                    switch(poziom)
                    {
                        case 1:
                        return "^zwawym tempem";
                        break;

                        case 2:
                        return "szybkim tempem";
                        break;

                        case 3:
                        return "szybko i rytmicznie zarazem";
                        break;
                    }
                    break;

                    case "weso^lo":
                    switch(poziom)
                    {
                        case 1:
                        return "weso^lo";
                        break;

                        case 2:
                        return "weso^lym g^losem";
                        break;

                       case 3:
                        return "g^losem napawaj^acym otoczenie rado^sci^a ^zycia";
                        break;
                    }
                    break;

                    case "wysoko":
                    switch(poziom)
                    {
                        case 1:
                        return "piskliwym g^losikiem";
                        break;

                        case 2:
                        return "wysokim, d^xwi^ecznym g^losem";
                        break;

                        case 3:
                        return "wysokim, czystym g^losem";
                        break;
                    }
                    break;

                    case "nisko":
                    switch(poziom)
                    {
                        case 1:
                        return "skrzekliwym g^losem";
                        break;

                        case 2:
                        return "wymuszonym niskim g^losem";
                        break;

                        case 3:
                        return "powa^znych, niskim g^losem";
                        break;
                    }
                    break;

                    case "g�o�no":
                    switch(poziom)
                    {
                        case 1:
                        return "krzykliwie";
                        break;

                        case 2:
                        return "g�o�no";
                        break;

                        case 3:
                        return "dono�nym g�osem";
                        break;
                    }
                    break;

                    case "cicho":
                    switch(poziom)
                    {
                        case 1:
                        return "cichym, ledwo s�yszalnym g�osem";
                        break;

                        case 2:
                        return "cicho, niemal szeptem";
                        break;

                        case 3:
                        return "cichym, lecz czystym g�osem";
                        break;
                    }
                    break;

                    case "smutno":
                    switch(poziom)
                    {
                        case 1:
                        return "smutnym g�osem";
                        break;

                        case 2:
                        return "smutno, lekko dr��cym g�osem";
                        break;

                        case 3:
                        return "smutno, dr��cym, przejmuj�cym g�osem";
                        break;
                    }
                    break;

                    case "mi�kko":
                    switch(poziom)
                    {
                        case 1:
                        return "mi�kkim g�osem";
                        break;

                        case 2:
                        return "mi�kkim, nieco rozmarzonym g�osem";
                        break;

                        case 3:
                        return "mi�kkim, rozmarzonym g�osem";
                        break;
                    }
                    break;

                    case "domy^slnie":
                    switch(poziom)
                    {
                        case 1:
                        return "skrzecz^acym g^losikiem";
                        break;

                        case 2:
                        return "d^xwi^ecznym g^losem";
                        break;

                        case 3:
                        return "przepi^eknym, wibruj^acym g^losem";
                        break;
                    }
                    break;
                }
            }
            else
            {
                switch(jak)
                {
                    case "wolno":
                    switch(poziom)
                    {
                        case 1:
                        return "powoli, staraj^ac si^e dok^ladnie intonowa^c ka^zde s^lowo";
                        break;

                        case 2:
                        return "powoli, dok^ladnie intonuj^ac ka^zde s^lowo";
                        break;

                        case 3:
                        return "powoli, wyra^xnie intonuj^ac ka^zde s^lowo";
                        break;
                    }
                    break;

                    case "szybko":
                    switch(poziom)
                    {
                        case 1:
                        return "^zwawym tempem";
                        break;

                        case 2:
                        return "szybkim tempem";
                        break;

                        case 3:
                        return "szybko i rytmicznie zarazem";
                        break;
                    }
                    break;

                    case "weso^lo":
                    switch(poziom)
                    {
                        case 1:
                        return "weso^lo";
                        break;

                        case 2:
                        return "weso^lym g^losem";
                        break;

                       case 3:
                        return "g^losem napawaj^acym otoczenie rado^sci^a ^zycia";
                        break;
                    }
                    break;

                    case "wysoko":
                    switch(poziom)
                    {
                        case 1:
                        return "skrzekliwym g^losem";
                        break;

                        case 2:
                        return "wysokim, d^xwi^ecznym g^losem";
                        break;

                        case 3:
                        return "bardzo wysokim, niemal^ze kobiecym g^losem";
                        break;
                    }
                    break;

                    case "nisko":
                    switch(poziom)
                    {
                        case 1:
                        return "staraj^ac si^e ^spiewa^c jak najni^zszym g^losem";
                        break;

                        case 2:
                        return "skrzecz^acym, niskim g^losem";
                        break;

                        case 3:
                        return "niskim, powa^znym g^losem";
                        break;
                    }
                    break;

                    case "smutno":
                    switch(poziom)
                    {
                        case 1:
                        return "smutnym g�osem";
                        break;

                        case 2:
                        return "smutno, lekko dr��cym g�osem";
                        break;

                        case 3:
                        return "smutno, dr��cym, przejmuj�cym g�osem";
                        break;
                    }
                    break;

                    case "g�o�no":
                    switch(poziom)
                    {
                        case 1:
                        return "krzykliwie";
                        break;

                        case 2:
                        return "g�o�no";
                        break;

                        case 3:
                        return "dono�nym g�osem";
                        break;
                    }
                    break;

                    case "cicho":
                    switch(poziom)
                    {
                        case 1:
                        return "cichym, ledwo s�yszalnym g�osem";
                        break;

                        case 2:
                        return "cicho, niemal szeptem";
                        break;

                        case 3:
                        return "cichym, lecz czystym g�osem";
                        break;
                    }
                    break;

                    case "mi�kko":
                    switch(poziom)
                    {
                        case 1:
                        return "mi�kkim g�osem";
                        break;

                        case 2:
                        return "mi�kkim, nieco rozmarzonym g�osem";
                        break;

                        case 3:
                        return "mi�kkim, rozmarzonym g�osem";
                        break;
                    }
                    break;

                    case "domy^slnie":
                    switch(poziom)
                    {
                        case 1:
                        return "skrzecz^acym g^losem";
                        break;

                        case 2:
                        return "d^xwi^ecznym g^losem";
                        break;

                        case 3:
                        return "weso^lym, dzieci^ecym g^losem";
                        break;
                    }
                    break;
                }
            }
            break;

            default:
            if(spiewak->query_gender() == G_FEMALE)
            {
                switch(jak)
                {
                    case "wolno":
                    switch(poziom)
                    {
                        case 1:
                        return "powoli, staraj^ac si^e dok^ladnie intonowa^c ka^zde s^lowo";
                        break;

                        case 2:
                        return "powoli, dok^ladnie intonuj^ac ka^zde s^lowo";
                        break;

                        case 3:
                        return "powoli, pi^eknie intonuj^ac ka^zde s^lowo";
                        break;
                    }
                    break;

                    case "szybko":
                    switch(poziom)
                    {
                        case 1:
                        return "^zwawym tempem";
                        break;

                        case 2:
                        return "szybkim tempem";
                        break;

                        case 3:
                        return "szybko i rytmicznie zarazem";
                        break;
                    }
                    break;

                    case "weso^lo":
                    switch(poziom)
                    {
                        case 1:
                        return "weso^lo";
                        break;

                        case 2:
                        return "weso^lym g^losem";
                        break;

                       case 3:
                        return "g^losem napawaj^acym otoczenie rado^sci^a ^zycia";
                        break;
                    }
                    break;

                    case "wysoko":
                    switch(poziom)
                    {
                        case 1:
                        return "piskliwym g^losikiem";
                        break;

                        case 2:
                        return "wysokim, d^xwi^ecznym g^losem";
                        break;

                        case 3:
                        return "wysokim, czystym g^losem";
                        break;
                    }
                    break;

                    case "nisko":
                    switch(poziom)
                    {
                        case 1:
                        return "skrzekliwym g^losem";
                        break;

                        case 2:
                        return "czystym, niskim g^losem";
                        break;

                        case 3:
                        return "niskim, niemal^ze m^eskim g^losem";
                        break;
                    }
                    break;

                    case "domy^slnie":
                    switch(poziom)
                    {
                        case 1:
                        return "skrzecz^acym g^losikiem";
                        break;

                        case 2:
                        return "d^xwi^ecznym g^losem";
                        break;

                        case 3:
                        return "przepi^eknym, niemal^ze elfim g^losem";
                        break;
                    }
                    break;
                }
            }
            else
            {
                switch(jak)
                {
                    case "wolno":
                    switch(poziom)
                    {
                        case 1:
                        return "powoli, staraj^ac si^e dok^ladnie intonowa^c ka^zde s^lowo";
                        break;

                        case 2:
                        return "powoli, dok^ladnie intonuj^ac ka^zde s^lowo";
                        break;

                        case 3:
                        return "powoli, wyra^xnie intonuj^ac ka^zde s^lowo";
                        break;
                    }
                    break;

                    case "szybko":
                    switch(poziom)
                    {
                        case 1:
                        return "^zwawym tempem";
                        break;

                        case 2:
                        return "szybkim tempem";
                        break;

                        case 3:
                        return "szybko i rytmicznie zarazem";
                        break;
                    }
                    break;

                    case "weso^lo":
                    switch(poziom)
                    {
                        case 1:
                        return "weso^lo";
                        break;

                        case 2:
                        return "weso^lym g^losem";
                        break;

                       case 3:
                        return "g^losem napawaj^acym otoczenie rado^sci^a ^zycia";
                        break;
                    }
                    break;

                    case "wysoko":
                    switch(poziom)
                    {
                        case 1:
                        return "skrzekliwym g^losem";
                        break;

                        case 2:
                        return "wysokim, d^xwi^ecznym g^losem";
                        break;

                        case 3:
                        return "bardzo wysokim, niemal^ze kobiecym g^losem";
                        break;
                    }
                    break;

                    case "nisko":
                    switch(poziom)
                    {
                        case 1:
                        return "oci^e^zale niskim g^losem";
                        break;

                        case 2:
                        return "burkliwym, niskim g^losem";
                        break;

                        case 3:
                        return "niskim, d^zwi^ecznym g^losem";
                        break;
                    }
                    break;

                    case "t�sknie":
                    switch(poziom)
                    {
                        case 1:
                        return "t�sknie";
                        break;

                        case 2:
                        return "t�sknie, wzruszaj�cym tonem";
                        break;

                        case 3:
                        return "t�sknym, wyciskaj�cym �zy z oczu tonem";
                        break;
                    }
                    break;

                    case "smutno":
                    switch(poziom)
                    {
                        case 1:
                        return "smutnym g�osem";
                        break;

                        case 2:
                        return "smutno, lekko dr��cym g�osem";
                        break;

                        case 3:
                        return "smutno, dr��cym, przejmuj�cym g�osem";
                        break;
                    }
                    break;

                    case "g�o�no":
                    switch(poziom)
                    {
                        case 1:
                        return "krzykliwie";
                        break;

                        case 2:
                        return "g�o�no";
                        break;

                        case 3:
                        return "dono�nym g�osem";
                        break;
                    }
                    break;

                    case "cicho":
                    switch(poziom)
                    {
                        case 1:
                        return "cichym, ledwo s�yszalnym g�osem";
                        break;

                        case 2:
                        return "cicho, niemal szeptem";
                        break;

                        case 3:
                        return "cichym, lecz czystym g�osem";
                        break;
                    }
                    break;

                    case "mi�kko":
                    switch(poziom)
                    {
                        case 1:
                        return "mi�kkim g�osem";
                        break;

                        case 2:
                        return "mi�kkim, nieco rozmarzonym g�osem";
                        break;

                        case 3:
                        return "mi�kkim, rozmarzonym g�osem";
                        break;
                    }
                    break;

                    case "domy^slnie":
                    switch(poziom)
                    {
                        case 1:
                        return "skrzecz^acym g^losem";
                        break;

                        case 2:
                        return "d^xwi^ecznym g^losem";
                        break;

                        case 3:
                        return "powa^znym, wibruj^acym g^losem";
                        break;
                    }
                    break;
                }
            }
            break;

        }
    }
}

/*
 * Nazwa funkcji: podladuj_limit_spiewu
 * Opis         : Funkcja odejmuje od propa odpowiedzialnego za wykorzystanie limitu
 *                spiewania.
 * Argumenty    : object spiewak - obiekt livinga ktoremu podladowujemy limit
 */
void
podladuj_limit_spiewu(object spiewak)
{
    int podladowanie = (spiewak->query_prop(ILE_MOZE_SPIEWAC)/4) + random(10);

    spiewak->add_prop(ILE_SPIEWAL, spiewak->query_prop(ILE_SPIEWAL) - podladowanie);
    spiewak->remove_prop("blok_alarmu_spiewania");

    if(spiewak->query_prop(ILE_SPIEWAL) <= 0)
    {
        spiewak->add_prop(ILE_SPIEWAL, 0);
    }
    else
    {
        set_alarm(30.0, 0.0, &podladuj_limit_spiewu(spiewak));
        spiewak->add_prop("blok_alarmu_spiewania", 1);
    }
}

/*
 * Nazwa funkcji: ile_bedzie_spiewac
 * Opis         : Funkcja ustala w graczu jego limit mozliwosci spiewu wedlug rasy, plci
 *                itp.
 * Argumenty    : object spiewak - okresla gracza dla ktorego ustalamy limit
 */
void
ile_bedzie_spiewac(object spiewak)
{
    int dlugosc = 150;

    if(spiewak->query_race_name() ~= "cz^lowiek")
        dlugosc += 50;
    else if(spiewak->query_race_name() ~= "p^o^lelf")
        dlugosc += 65;
    else if(spiewak->query_race_name() ~= "elf")
        dlugosc += 80;
    else if(spiewak->query_race_name() ~= "krasnolud")
        dlugosc += 25;
    else if(spiewak->query_race_name() ~= "gnom")
        dlugosc += 5;
    else if(spiewak->query_race_name() ~= "nizio^lek")
        dlugosc += 15;

    if(spiewak->query_gender() == G_MALE)
        dlugosc = dlugosc*10/8;

    if(spiewak->query_skill(SS_MUSIC) < 5)
        dlugosc += 15;
    else if(spiewak->query_skill(SS_MUSIC) < 35)
        dlugosc += 30;
    else if(spiewak->query_skill(SS_MUSIC) < 75)
        dlugosc += 50;
    else
        dlugosc += 70;

    spiewak->add_prop(ILE_MOZE_SPIEWAC, dlugosc);
}
