/**
 * \file /cmd/live/social.c
 *
 * General commands for 'nonemotive social' behaviour.
 * The following commands are defined:
 *
 */

#pragma no_clone
#pragma no_inherit
#pragma save_binary
#pragma strict_types

inherit "/cmd/std/command_driver";

#include <cmdparse.h>
#include <composite.h>
#include <const.h>
#include <files.h>
#include <filter_funs.h>
#include <flags.h>
#include <formulas.h>
#include <language.h>
#include <macros.h>
#include <mail.h>
#include <std.h>
#include <stdproperties.h>
#include <time.h>
#include <options.h>
#include <colors.h>

nomask int sort_name(object a, object b);

/*
 * Function name: create
 * Description  : This function is called the moment this object is created
 *                and loaded into memory.
 */
void
create()
{
    seteuid(getuid(this_object()));
}

/* **************************************************************************
 * Return a proper name of the soul in order to get a nice printout.
 */
string
get_soul_id()
{
    return "social";
}

/* **************************************************************************
 * This is a command soul.
 */
int
query_cmd_soul()
{
    return 1;
}

/* **************************************************************************
 * The list of verbs and functions. Please add new in alfabetical order.
 */
mapping
query_cmdlist()
{
    return ([
             "do��cz"       : "dolacz",
             "dru�yna"      : "druzyna",

             "emote"        : "emote",
             "/me"          : "emote",

             "kto"          : "kto",

             "oddaj"        : "przeka�",
             "odpowiedz"    : "odpowiedz",
//             "ostatnio"     : "ostatnio",

             "porzu�"       : "porzuc",
             "przedstaw"    : "przedstaw",
             "przedstawieni": "przedstawieni",
             "przeka�"      : "przekaz",
             "przejd�"      : "przejd�",    //Narazie nieaktywne - do zmieniania pozycji w dru�ynie
             "przyzwij"     : "przyzwij",

             "spytaj"       : "zapytaj",

             "wesprzyj"     : "wesprzyj",
             "wezwij"       : "przyzwij",

             "zapami�taj"   : "zapamietaj",
             "zapami�tani"  : "zapamietani",
             "zapomnij"     : "zapomnij",
             "zapro�"       : "zapros",
             "zapytaj"      : "zapytaj"]);
}

/*
 * Function name: using_soul
 * Description:   Called once by the living object using this soul. Adds
 *                  sublocations responsible for extra descriptions of the
 *                  living object.
 */
public void
using_soul(object live)
{
}

/* **************************************************************************
 * Here follows some support functions.
 * **************************************************************************/

/* **************************************************************************
 * Here follows the actual functions. Please add new functions in the
 * same order as in the function name list.
 * **************************************************************************/

/*
 * dolacz - Dolacza do czyjejs druzyny
 */
varargs int
dolacz(string name)
{
    object leader, *oblist;
    int i;

    if (!name)
    {
        notify_fail("Musisz poda� imi� gracza, kt�ry ma przewodzi� dru�ynie.\n");
        return 0;
    }

    if (this_player()->query_leader() || sizeof(this_player()->query_team()))
    {
        write("Ju� jeste� w dru�ynie!\n");
        return 1;
    }

    oblist = parse_this(name, "'do' %l:" + PL_DOP);

    if (!(i = sizeof(oblist)))
    {
        notify_fail("Do��cz do kogo?\n");
        return 0;
    }

    if (i > 1)
    {
        write("Nie mo�esz by� prowadzon" + this_player()->koncowka("y", "a") +
            " jednocze�nie przez " + COMPOSITE_LIVE(oblist, PL_BIE) + ".\n");
        return 1;
    }

    leader = oblist[0];

    if (member_array(this_player(), leader->query_invited()) < 0)
    {
        write(leader->query_Imie(this_player(), PL_MIA) + " nie zaprosi�" +
            leader->koncowka("", "a") + " ci� do swej dru�yny.\n");
        return 1;
    }

#if 0
    if (leader->query_leader())
    {
        write(leader->query_Imie(this_player(), PL_MIA) + " jest ju� w " +
            "innej dru�ynie.\n");
        return 1;
    }

    /*
     * Can not have a leader with too low DIS
     */
    if (leader->query_stat(SS_DIS) + 20 < this_player()->query_stat(SS_DIS) &&
        !this_player()->query_wiz_level())
    {
        write("Nie bardzo ufasz w zdolno�ci przyw�dcze " +
            leader->query_imie(this_player(), PL_DOP) + ".\n");
         return 1;
    }
#endif

    if (leader->team_join(this_player()))
    {
        if (!this_player()->query_option(OPT_BRIEF))
        {
            write("W momencie wej�cia do dru�yny zaczynasz widzie� kr�tkie " +
            "opisy lokacji.\n");
            this_player()->add_prop(TEMP_BACKUP_BRIEF_OPTION, 1);
            this_player()->set_option(OPT_BRIEF, 1);
        }

        write("Do��czasz do dru�yny " +
            leader->query_imie(this_player(), PL_DOP) + ".\n");
        saybb(QCIMIE(this_player(), PL_MIA) + " do��cza do dru�yny " +
            QIMIE(leader, PL_DOP) + ".\n", ({ leader, this_player() }));
        tell_object(leader, this_player()->query_Imie(leader, PL_MIA) +
            " do��cza do twojej dru�yny.\n");
    }
    else
    {
        write("Nie mo�esz do��czyc do " +
                leader->query_imie(this_player(), PL_DOP));
    }  // Czy powyzszy komunikat powinien sie kiedykolwiek ukazywac ?

    return 1;
}

/*
 * druzyna - Podaje info o druzynie, ktorej gracz jest czlonkiem.
 */
varargs int
druzyna(string str)
{
    object leader;
    object *members;
    int i;

    if (stringp(str))
    {
        notify_fail("Po prostu 'dru�yna'.\n");
        return 0;
    }

    if (leader = (object)this_player()->query_leader())
    {
        write("Dru�yn� prowadzi " + leader->query_imie(this_player(), PL_MIA));
        members = (object *) leader->query_team();
        members = members - ({ this_player() });
        if (!(i = sizeof(members)))
             write(", za� ty jeste� jej jedynym cz�onkiem.\n");
        else
        if (i == 1)
            write(" i opr�cz ciebie jest w niej jeszcze " +
                members[0]->query_imie(this_player(), PL_MIA) + ".\n");
        else
            write(" i opr�cz ciebie s� w niej jeszcze: " +
                FO_COMPOSITE_LIVE(members, this_player(), PL_MIA) + ".\n");
    }
    else if (sizeof(members = (object *) this_player()->query_team()) > 0)
    {
        write("Przewodzisz dru�ynie, w kt�rej opr�cz ciebie jest jeszcze " +
            FO_COMPOSITE_LIVE(members, this_player(), PL_MIA) + ".\n");
    }
    else
    {
        notify_fail("Nie jeste� w �adnej dru�ynie.\n");
        return 0;
    }

    return 1;
}


/*
 * emote - Put here so NPC:s can emote (  No error messages if they do wrong,
 *           why waste cpu on NPC:s ? ;-)   )
 */
int
emote(string str)
{
    if (!stringp(str) ||
        !this_player()->query_npc())
    {
        return 0;
    }

    saybb(QCIMIE(this_player(), PL_MIA) + " " + str + "\n");

    return 1;
}

/*
 * Function name: index_arg
 * Description  : This function returns whether a particular letter is
 *                used in the argument the player passed to the function.
 * Arguments    : string str    - the arguments.
 *                string letter - the letter to search for.
 * Returns      : int 1/0 - true if the letter is used.
 */
nomask int
index_arg(string str, string letter)
{
    return (member_array(letter, explode(str, "")) != -1);
}

/*
 * Function name: get_name
 * Description  : This map function will return the name of the player for
 *                the 'who n' command. If the player is invis it will
 *                return the name in brackets and if the living is linkdead,
 *                an asterisk (*) is added.
 * Arguments    : object player - the player to return the name for.
 * Returns      : string - the name to print.
 */
nomask string
get_name(object player)
{
    string name = capitalize(player->query_real_name());

    /* If the player is linkdead, we add an asterisk (*) to the name.
     */
    if (!interactive(player) &&
        !player->query_npc())
    {
        name += "*";
    }

    /* If the living is invis, we put the name between breakets. */
    if (player->query_prop(OBJ_I_INVIS))
    {
            return ("(" + name + ")");
    }

    /* Je�li wiz ma w��czon� opcje invis k */
    if (player->query_prop(OBJ_I_WHO_INVIS))
        return ("<" + name + ">");

    /* Dla wiz�w.. Je�li gracz ma w��czon� opcje ukrywania w kto */
    if (player->query_option(OPT_HIDE_IN_WHO))
        return ("[" + name + "]");

    return name;
}

/**
 * Sprawdza czy kto� jest czarodziejem czy nim nie jest.
 */
private int
wyfiltruj_wizow(object ob)
{
    string imie;
    if(imie = ob->query_possessed())
        return SECURITY->query_wiz_rank(imie);
    return SECURITY->query_wiz_rank(ob->query_real_name());
}

/*
 * Function name: print_who
 * Description  : This function actually prints the list of people known.
 * Arguments    : string opts  - the command line arguments.
 *                object *list - the list of livings to display.
 *                int    size  - the number of people logged in.
 * Returns      : int 1 - always.
 */
nomask int
print_who(string opts, object *list, int size)
{
    int i, j;
    int scrw = this_player()->query_option(OPT_SCREEN_WIDTH);
    string to_write = "";
    string *title;
    string tmp;

    object *wizowie, *reszta;

    scrw = (scrw ? (scrw - 3) : 1000); // troszke musialem to zmienic bo sie przy szerokosc 0 krzaczylo
    list -= ({TP});
    if(!TP->query_wiz_level())
        list = filter(list, &not() @ &->query_option(OPT_HIDE_IN_WHO));
    list += ({TP});
    list = sort_array(list, &sort_name());

    wizowie =  filter(list, wyfiltruj_wizow);
    list = list - wizowie;

    if (!sizeof(list) && !sizeof(wizowie))
    {
        if (size == 1)
            to_write += "Przebywasz w �wiecie Vatt'gherna sam"
                      + this_player()->koncowka("", "a") + ".\n";
        else if (index_arg(opts, "c"))
            to_write += "Nie znasz �adnego czarodzieja spo�r�d "
                      + LANG_SNUM(size, PL_DOP, PL_ZENSKI)
                      + " os�b przebywaj�cych obecnie w �wiecie Vatt'gherna.\n";
        else if (index_arg(opts, "s"))
            to_write += "Nie znasz �adnego �miertelnika spo�r�d "
                      + LANG_SNUM(size, PL_DOP, PL_ZENSKI)
                      + " os�b przebywaj�cych obecnie w �wiecie Vatt'gherna.\n";
        /* No need to check for mwho here. */
        write(to_write);
        return 1;
    }
    to_write += set_color(COLOR_FG_CYAN);
    if (size == 1)
        to_write += "Przebywasz w �wiecie Vatt'gherna sam"
                  + this_player()->koncowka("", "a") + ":\n";
    else if (index_arg(opts, "c"))
        to_write += "Spo�r�d " + LANG_SNUM(size, PL_DOP, PL_ZENSKI)
                  + " os�b przebywaj�cych w �wiecie Vatt'gherna, "
                  + "znani tobie czarodzieje to:\n";
    else if (index_arg(opts, "s"))
        to_write += "Spo�r�d " + LANG_SNUM(size, PL_DOP, PL_ZENSKI)
                  + " os�b przebywaj�cych w �wiecie Vatt'gherna, "
                  + "znani tobie �miertelnicy to:\n";
    else
        to_write += "Spo�r�d " + LANG_SNUM(size, PL_DOP, PL_ZENSKI)
                  + " os�b przebywaj�cych w �wiecie Vatt'gherna, "
                  + "znane tobie to:\n";
    to_write += clear_color();

    /* By default we display only the names, unless the argument 'f' for
     * full was given.
     */
    if (index_arg(opts, "k"))
    {
        to_write += (sprintf("%-*#s\n", scrw,
            implode(map(list, get_name), "\n")));
        /* No need to check for mwho here. */
        write(to_write);
        return 1;
    }

    for(i = 0; i < sizeof(list); i++)
    {
#ifdef 0
       if (list[i]->query_prop(OBJ_I_INVIS))
       {
           to_write += (interactive(list[i]) || list[i]->query_npc() ? "" :
               "*") + "(" + capitalize(list[i]->query_real_name(PL_MIA))
             + ")\n";
       }
       else
       {
#endif
            string imie;

            if((imie=list[i]->query_possessed()))
            {
                list[i] = find_living(imie);
            }

            tmp = list[i]->query_presentation();

            if (list[i]->query_prop(OBJ_I_INVIS))
                tmp = "(" + tmp + ")";
            else if (list[i]->query_prop(OBJ_I_WHO_INVIS))
                tmp = "<" + tmp + ">";
            else if (list[i]->query_option(OPT_HIDE_IN_WHO))
                tmp = "[" + tmp + "]";
            if (!interactive(list[i]) && !list[i]->query_npc() && !imie)
            {
                tmp = ("*" + tmp);
            }
            if(TP->query_wiz_level() && imie)
                tmp = ("^" + tmp);

            if (strlen(tmp) < scrw)
            {
                to_write += (tmp + "\n");
            }
            else /* Split a too long title in a nice way. */
            {
                title = explode(break_string(tmp, (scrw - 2)), "\n");
                tmp = sprintf("%-*s\n", scrw, title[0]);

                title = explode(break_string(
                    implode(title[1..(sizeof(title) - 1)], " "),
                    (scrw - 8)), "\n");

                for(j = 0; j < sizeof(title); j++)
                    tmp += (sprintf("      %-*s\n", (scrw - 6), title[j]));

                to_write += (tmp);
            }
#ifdef 0
        }
#endif
    }

    if(sizeof(list) && sizeof(wizowie) && !index_arg(opts, "c"))
        to_write+= "\n" + set_color(COLOR_FG_CYAN) +
            "A spo�r�d czarodziej�w z Thanedd obecni w �wiecie s�:" +
            clear_color() + "\n";

    for(i = 0; i < sizeof(wizowie); i++)
    {
        string imie;

        if((imie=wizowie[i]->query_possessed()))
            wizowie[i] = find_living(imie);

        tmp = wizowie[i]->query_presentation();

        if (wizowie[i]->query_prop(OBJ_I_INVIS))
            tmp = "(" + tmp + ")";
        else if (wizowie[i]->query_prop(OBJ_I_WHO_INVIS))
            tmp = "<" + tmp + ">";
        else if (wizowie[i]->query_option(OPT_HIDE_IN_WHO))
            tmp = "[" + tmp + "]";

        if (!interactive(wizowie[i]) && !wizowie[i]->query_npc() && !imie)
            tmp = ("*" + tmp);

        if(TP->query_wiz_level() && imie)
            tmp = ("#" + tmp);

        if (strlen(tmp) < scrw)
        {
            to_write += (tmp + "\n");
        }
        else /* Split a too long title in a nice way. */
        {
            title = explode(break_string(tmp, (scrw - 2)), "\n");
            tmp = sprintf("%-*s\n", scrw, title[0]);

            title = explode(break_string(
                implode(title[1..(sizeof(title) - 1)], " "),
                (scrw - 8)), "\n");

            for(j = 0; j < sizeof(title); j++)
                tmp += (sprintf("      %-*s\n", (scrw - 6), title[j]));

            to_write += (tmp);
        }
//        }
    }

    setuid();
    seteuid(getuid());
    this_player()->more(to_write);

    return 1;
}

/*
 * Function name: sort_name
 * Description  : This sort function sorts on the name of the player. Since
 *                no two players can have the same name, we do not have to
 *                check for that.
 * Arguments    : object a - the playerobject to player a.
 *                object b - the playerobject to player b.
 * Returns      : int -1 - name of player a comes before that of player b.
 *                     1 - name of player b comes before that of player a.
 */
nomask int
sort_name(object a, object b)
{
    string aname = a->query_real_name();
    string bname = b->query_real_name();

    return ((aname == bname) ? 0 : ((aname < bname) ? -1 : 1));
}

static private int
moze_zobaczyc(object ob)
{
    return (TP->query_prop(LIVE_I_SEE_INVIS) >= ob->query_prop(OBJ_I_INVIS) &&
            TP->query_prop(LIVE_I_SEE_INVIS) >= ob->query_prop(OBJ_I_WHO_INVIS));
}

/*
 * kto - Pokazuje znane osoby ktore sa aktualnie w grze.
 */
int
kto(string opts)
{
    object  *list = users();
    object  npc;
    mapping rem;
    mapping memory = ([ ]);
    string  *names = ({ });
    int     index;
    int     size;
    int     size_list;

    if(TP->query_option(OPT_HIDE_IN_WHO) && !TP->query_wiz_level())
    {
        notify_fail("Maj�c w��czon� opcje ukrywanie w kto nie mo�esz zobaczy� " +
            "listy znajomych przebywaj�cych w chwili obecnej w �wiecie "+
            "Vatt'gherna.\n");
        return 0;
    }

    if(TP->query_prop(LIVE_I_UKRYWAL_SIE_W_KTO) && !TP->query_wiz_level())
    {
        NF("Musisz odczeka� chwilk� po wy��czeniu opcji ukrywania si� w kto, "+
            "dopiero po tej chwili mo�esz zobaczy� list� znajomych przebywaj�cych "+
            "w �wiecie Vatt'gherna.\n");
        return 0;
    }

    if (!stringp(opts))
    {
        opts = "";
    }

#ifdef STATUE_WHEN_LINKDEAD
#ifdef OWN_STATUE
    /* If there is a room where statues of linkdead people can be found,
     * we add that to the list.
     */
    list += (all_inventory(find_object(OWN_STATUE)) - list);
#endif OWN_STATUE
#endif STATUE_WHEN_LINKDEAD

    /* This filters out players logging in and such. */
    list = filter(list, &operator(==)(LIVING_OBJECT) @
        &function_exists("create_container"));

    size = sizeof(list);

    /* Player may indicate to see only wizards or mortals. */
    if (index_arg(opts, "c"))
    {
        list = filter(list, &wyfiltruj_wizow());
    }
    else if (index_arg(opts, "s"))
    {
        list = filter(list, &not() @ &wyfiltruj_wizow());
    }

    /* Wizards won't see the NPC's and wizards are not subject to the
     * met/nonmet system if that is active.
     */
     //ZAKOMENTOWANE BY LIL. "Zrywamy z boskoscia wizardow".
    if (this_player()->query_wiz_level())
    {
        return print_who(opts, list, size);
    }

    if (mappingp(rem = this_player()->query_remembered()))
    {
        memory += rem;
    }
    if (mappingp(rem = this_player()->query_introduced()))
    {
        memory += rem;
    }

#ifdef MET_ACTIVE
    index = -1;
    size_list = sizeof(list);
    while(++index < size_list)
    {
        if ((!(memory[list[index]->query_real_name()])) &&
            (!(list[index]->query_prop(LIVE_I_ALWAYSKNOWN))) &&
            (!(list[index]->query_wiz_level())) )
        {
            list[index] = 0;
        }
    }

    list = list - ({ 0 });
#endif MET_ACTIVE

    /* Don't add NPC's if the player wanted wizards. Here we also add the
     * player himself again, because that is lost during the met-check (when
     * enabled).
     */
    if (!index_arg(opts, "c"))
    {
#ifdef MET_ACTIVE
        list += ({ this_player() });
//        size++;   /* this_player() juz dodany */
#endif MET_ACTIVE
        names = m_indices(memory) - list->query_real_name();

        index = -1;
        size_list = sizeof(names);
        while(++index < size_list)
        {
            /* We check that the people found this way are NPC's since
             * we do not want linkdead people to show up this way They
             * are already in the list.
             */
             //A po co npce w 'kto'? :P Vera.
            /*if (objectp(npc = find_living(names[index])) &&
                npc->query_npc())
            {
                list += ({ npc });
                size++;
            }*/
        }
    }

    /* To mortals 'who' will only display players who are visible to them. */
    /* Niewikie zmiany Kruna, coby nie by�o wida� ludzi z opcja inivs k:P */
    list = filter(list, moze_zobaczyc);

    return print_who(opts, list, size);
}

/*
 * odpowiedz - Umozliwia graczom na odpowiedzenie czarodziejom, jesli ci
 *        im cos zatellowali.
 */
int
odpowiedz(string str)
{
    string *names, who;
    object target, *gracze;
    int i;

    /* Taki bajerek coby si� to nie wpieprza�o w odpowiedz
       w wiosce tworzenia postaci (Krun) */
    if(implode(explode(file_name(ENV(TP)), "/")[0..-2], "/") == "/d/Standard/login/wioska")
        return 0;

    /* Access failure. No command line argument. */
    if (!stringp(str))
    {
        notify_fail("Odpowiedz [komu] co?\n");
        return 0;
    }

    /* Wizard may block mortals from replying again. */
    if (this_player()->query_wiz_level() &&
        wildmatch("zablokuj *", str))
    {
        sscanf(lower_case(str), "zablokuj %s", str);

        gracze = users();
        i = sizeof(gracze) - 1;

        while ((gracze[i]->query_real_name(PL_CEL) != str) &&
               (--i >= 0))
            ;

        if (i == -1)
        {
            notify_fail("Nie ma takiego gracza.\n");
            return 0;
        }

        target = gracze[i];

        names = target->query_prop(PLAYER_AS_REPLY_WIZARD);
        who = this_player()->query_real_name(PL_CEL);
        if (!pointerp(names) ||
            (member_array(who, names) == -1))
        {
            write(target->query_name(PL_MIA) + " i tak nie " +
                target->koncowka("m�gl", "mogla") + " ci odpowiada�.\n");
            return 1;
        }

        names -= ({ who });
        if (sizeof(names))
        {
            target->add_prop(PLAYER_AS_REPLY_WIZARD, names);
        }
        else
        {
            target->remove_prop(PLAYER_AS_REPLY_WIZARD);
        }

        write("Twoje imi� zosta�o usuni�te z listy os�b, kt�rym " +
            target->query_name(PL_MIA) + " mo�e odpowiada�.\n");
        return 1;
    }

    /* See if any wizard has told anything to this player. */
    names = this_player()->query_prop(PLAYER_AS_REPLY_WIZARD);
    if (!pointerp(names) || !sizeof(names))
    {
        notify_fail("Mo�esz odpowiedzie� tylko wtedy, gdy kto� ci co� " +
            "powiedzia�.\n");
        return 0;
    }

    /* Wizowie wizom odpowiada� nie b�d�:)
     */
    if(TP->query_wiz_level())
    {
        NF("Tellowa� nie odpowiada�! Krun\n");
        return 0;
    }

    /* If the mortal wants to reply to someone in particular, see get the
     * name and see whether that is possible, else we take the first wizard
     * from the list.
     */

    if (sscanf(str, "%s %s", who, str) == 2)
    {
        if (member_array(lower_case(who), names) == -1)
        {
            str = who + " " + str;
            who = names[0];
        }
    }
    else
       who = names[0];

    gracze = users();
    i = sizeof(gracze) -1;

    while ((gracze[i]->query_real_name(PL_CEL) != who) && (--i >= 0))
        ;

    /* Wizard is no longer logged in. */
    if (i == -1)
    {
        write("Nie mo�esz odpowiedzie�, gdy� ta osoba nie jest ju� obecna.\n");
        return 1;
    }

    target = gracze[i];

    /* No point in replying to someone who is linkdead. */
    if (!interactive(target))
    {
        write(target->query_Imie(this_player(), PL_MIA) +
            " straci�" + target->koncowka("", "a") + " kontakt z " +
            "rzeczywisto�ci�.\n");
        return 1;
    }

    tell_object(target, this_player()->query_Imie(target, PL_MIA) +
        " odpowiada: " + str + "\n");
    if (this_player()->query_option(OPT_ECHO))
    {
        write("Odpowiadasz " + target->query_imie(this_player(), PL_CEL) +
            ": " + str + "\n");
    }
    else
    {
        write("Odpowiedzia�" + this_player()->koncowka("e�", "a�") +
            " " + target->query_imie(this_player(), PL_CEL) + ".\n");
    }
    return 1;
}

/**
 * ostatnio - Wyswietla informacje o graczu
 */
int
ostatnio(string str)
{
    object player;
    int duration;
    string konc;
    int npc;

    if (!stringp(str))
        str = this_player()->query_real_name();
    else
    {
        str = lower_case(str);
        if (!(this_player()->query_met(str)))
        {
            notify_fail("Nie znasz nikogo o tym imieniu.\n");
            return 0;
        }
    }

    if (SECURITY->query_wiz_rank(str))
    {
        notify_fail("Ta komenda nie dzia�a na czarodziei.\n");
        return 0;
    }

    if (!(SECURITY->exist_player(str)))
    {
        npc = 1;
    }

/*
    if (objectp(player = find_living(str)) &&
        player->query_npc())
*/
    if (npc)
    {
        player = find_living(str);
        if (objectp(player))
            konc = player->koncowka("y", "a", "e");
        else
            konc = "y";

        /*write("Ostatnie logowanie ze startem systemu: " +
              ctime(SECURITY->query_start_time() +
            (random(time() - SECURITY->query_start_time()) / 2)) + "\n");
        write("Aktywno��: aktywn" + konc + ", jak zwykle.\n");*/
        write("Ta komenda nie zadzia�a na t� istot�.\n");
        return 1;
    }

    if (objectp(player = find_player(str)))
    {
        write("Ostatnie logowanie : " + ctime(player->query_login_time()) +
             "\n");

//A na choler� to? Mieli�my to wywali�. Vera.
        /*if (interactive(player) &&
            CAN_SEE(this_player(), player))
        {
            if (query_idle(player) > 60)
            {
                write("Aktywno��          : " +
                    TIME2STR(query_idle(player), 2) + " nieaktywn" +
                    player->koncowka("y", "a") + "\n");
            }
            else
            {
                write("Aktywno��          : aktywn" +
                    player->koncowka("y", "a") +
                    "\n");
            }
        }
        else
        {
            write("Aktywno��          : straci�" +
                this_player()->koncowka("", "a") + " kontakt z " +
                "rzeczywisto�ci�.\n");
        }*/

        return 1;
    }

    player = SECURITY->finger_player(str);
    write("Ostatnie logowanie : " + ctime(player->query_login_time()) + "\n");
    duration = (file_time(PLAYER_FILE(str) + ".o") -
        player->query_login_time());
//To te�...
    /*if (duration < 86400)
    {
        write("Sp�dzony czas      : " + TIME2STR(duration, 3) + "\n");
    }
    else
    {
        write("Sp�dzony czas: nieznany\n");
    }*/
    player->remove_object();
    return 1;
}

/*
 * porzuc - Porzuc druzyne lub kogos, kto jest w druzynie.
 */
int
porzuc(string name)
{
    object ob, *members;
    mixed oblist;
    int i;

    if (!stringp(name))
    {
        notify_fail("Porzuc co? Mo�e dru�yn�?\n");
        return 0;
    }

    if (name ~= "dru�yn�")
    {
        if (objectp(ob = this_player()->query_leader()))
        {
            ob->team_leave(this_player());
            write("Porzucasz swoj� dru�yn�.\n");
            tell_object(ob, this_player()->query_Imie(ob, PL_MIA) +
                " porzuca twoj� dru�yn�.\n");

            if (this_player()->query_prop(TEMP_BACKUP_BRIEF_OPTION))
            {
                write("Od momentu wyj�cia z dru�yny z powrotem widzisz " +
                    "d�ugie opisy lokacji.\n");
                this_player()->remove_prop(TEMP_BACKUP_BRIEF_OPTION);
                this_player()->set_option(OPT_BRIEF, 0);
            }

            return 1;
        }

        if (i = sizeof(members = this_player()->query_team()))
        {
            write("Porzucasz dru�yn�, kt�rej przewodzi�" +
                this_player()->koncowka("e�", "a�") + ".\n");
            while(--i >= 0)
            {
                tell_object(members[i],
                    capitalize(this_player()->query_Imie(members[i], PL_MIA)) +
                    " rozwi�zuje dru�yn�.\n");
                this_player()->team_leave(members[i]);
                if (this_player()->query_prop(TEMP_BACKUP_BRIEF_OPTION))
                {
                    write("Od momentu wyj�cia z dru�yny z powrotem widzisz " +
                        "d�ugie opisy lokacji.\n");
                    this_player()->remove_prop(TEMP_BACKUP_BRIEF_OPTION);
                    this_player()->set_option(OPT_BRIEF, 0);
                }
            }
            return 1;
        }

        notify_fail("Nie jeste� cz�onkiem �adnej dru�yny.\n");
        return 0;
    }

    if (!(i = sizeof(members = this_player()->query_team())))
    {
        notify_fail("T� dru�yn� prowadzi kto inny... Mo�esz co najwy�ej " +
            this_player()->koncowka("samemu", "sama") + " porzuci� dru�yn�.\n");
        return 0;
    }

    notify_fail("Porzu� kogo?\n");
    if (!parse_command(name, members, "%l:" + PL_BIE, oblist))
        return 0;

//    oblist = NORMAL_ACCESS(oblist, 0, 0);
    i = sizeof(oblist);
    if (i < 2)
        return 0;

    if (i > 2)
    {
        write("Nie mo�esz porzuci� wi�cej ni� jedn� osob� na raz.\n");
        return 1;
    }

    this_player()->set_obiekty_zaimkow(oblist[1..1]);
    ob = oblist[1];

    this_player()->team_leave(ob);
    this_player()->remove_invited(ob); /* disallow him/her to rejoin. */
    write("Zmuszasz " + ob->query_imie(this_player(), PL_BIE) +
        " do opuszczenia dru�yny.\n");
    tell_object(ob, this_player()->query_Imie(ob, PL_MIA) +
        " zmusza ci� do opuszczenia dru�yny.\n");
    if (this_player()->query_prop(TEMP_BACKUP_BRIEF_OPTION))
    {
        write("Od momentu wyj�cia z dru�yny z powrotem widzisz " +
            "d�ugie opisy lokacji.\n");
        this_player()->remove_prop(TEMP_BACKUP_BRIEF_OPTION);
        this_player()->set_option(OPT_BRIEF, 0);
    }

    return 1;
}

/*
 * przedstaw - Przedstaw sie albo kogos innego.
 */
int
przedstaw(string str)
{
    string  kogo;
    string  komu = "";
    string  mianownik, dopelniacz;
    string  jako;
    string *tmp;
    object  przedstawiany;
    object *przedstawiani;
    object *odbiorcy;
    object *osoby;
    object *inni;
    int     wszyscy = 0;
    int     sam_sie = 0;
    int     i;
    int     size;
    int     jako_gildiowiec;

    notify_fail(capitalize(query_verb()) + " kogo [komu] [jako kogo]?\n");
    if (!stringp(str))
    {
        return 0;
    }

    kogo = explode(str, " ")[0];

    if(wildmatch("*jako*", str))
        jako_gildiowiec = 1;

    if (kogo ~= "si�" || kogo == "siebie")
    {
        przedstawiany = this_player();
        if(jako_gildiowiec)
        {
            if(wildmatch("*" + kogo + " jako*", str))
                sscanf(str, kogo + " jako %s", jako);
            else
                sscanf(str, kogo + " %s jako %s", komu, jako);
        }
        else
            sscanf(str, kogo + " %s", komu);
    }
    else
    {
        if (parse_command(str, environment(this_player()),
                 "%l:" + PL_BIE + " %s", przedstawiani, komu))
            przedstawiani = NORMAL_ACCESS(przedstawiani, 0, 0);

        if (!sizeof(przedstawiani))
        {
            notify_fail("Kogo chcesz przedstawi�?\n");
            return 0;
        }

        if (sizeof(przedstawiani) > 1)
        {
            write("Mo�esz przedstawi� naraz tylko jedn� osob�.\n");
            return 1;
        }

        przedstawiany = przedstawiani[0];
    }


    sam_sie = ((this_player() == przedstawiany) ? 1 : 0);

    if (!sam_sie && (!this_player()->query_met(przedstawiany)))
    {
        write("Ha! Najpierw sam" + this_player()->koncowka("", "a") +
            " " + przedstawiany->koncowka("go", "j�", "to", "ich", "je") +
            " poznaj.\n");
        return 1;
    }


    if (strlen(komu))
    {
        odbiorcy = parse_this(komu, "%l:" + PL_CEL) - ({ przedstawiany });
    }
    else
    {
        odbiorcy = FILTER_LIVE(all_inventory(environment(this_player()))) -
             ({ this_player(), przedstawiany });

        wszyscy = 1;
    }

    osoby = FILTER_CAN_SEE(odbiorcy, this_player());

    if (!wszyscy)
    {
        if (!sizeof(osoby))
        {
            write("Komu chcesz si� przedstawi�?\n");
            return 1;
        }
        i = -1;
        size = sizeof(osoby);

        while(++i < size)
            osoby[i]->reveal_me(1);
    }

    this_player()->reveal_me(1); przedstawiany->reveal_me(1);

    str = przedstawiany->query_presentation();
    size = sizeof(odbiorcy);

    if (!sam_sie)
    {
        if (wszyscy)
            this_player()->set_obiekty_zaimkow(przedstawiani);
        else
            this_player()->set_obiekty_zaimkow(przedstawiani, odbiorcy);

        write("Przedstawiasz " + przedstawiany->query_imie(this_player(),
            PL_BIE) + " " + (wszyscy ? "wszystkim" :
            FO_COMPOSITE_LIVE(odbiorcy, this_player(), PL_CEL)) + ".\n");

        przedstawiany->catch_msg(this_player()->query_Imie(przedstawiany,
            PL_MIA) + " przedstawia ci� " + (wszyscy ? "wszystkim"
            : FO_COMPOSITE_LIVE(FILTER_CAN_SEE(odbiorcy, przedstawiany),
            przedstawiany, PL_CEL)) + ".\n");

        i = -1;
        while(++i < size)
        {
            tell_object(odbiorcy[i],
                    this_player()->query_Imie(odbiorcy[i], PL_MIA) +
                    " przedstawia " + przedstawiany->query_imie(odbiorcy[i],
                    PL_BIE) + " s�owami:\nOto " + str + ".\n");
        }

        inni = FILTER_LIVE(all_inventory(environment(this_player()))) -
                 ({ this_player(), przedstawiany }) - odbiorcy;

        size = sizeof(inni);
        i = -1;
        while(++i < size)
        {
            tell_object(inni[i], this_player()->query_Imie(inni[i], PL_MIA) +
                " przedstawia " + przedstawiany->query_imie(inni[i], PL_BIE) +
                " " + FO_COMPOSITE_LIVE(FILTER_CAN_SEE(odbiorcy, inni[i]),
                inni[i], PL_CEL) + ".\n");
        }
    }
    else
    {
        if(jako_gildiowiec)
        {
            string guild_title;
            if(!(guild_title = TP->query_tytul_gildiowy(jako)))
            {
                notify_fail("Jako kto chcesz si� przedstawi�?\n");
                return 0;
            }
            else
            {
                tmp = explode(str, ", ");
                str = implode(tmp[0..-2], ", ") + ", " + guild_title + ", " + implode(tmp[-1..-1], ", ");
            }
        }

        i = -1;
        while(++i < size)
        {
            tell_object(odbiorcy[i],
                    this_player()->query_Imie(odbiorcy[i], PL_MIA) +
                    " przedstawia si� jako:\n" + str + ".\n");
        }

        if (!wszyscy)
            this_player()->set_obiekty_zaimkow(odbiorcy);

        if (this_player()->query_option(OPT_ECHO))
            write("Przedstawiasz si� " + (wszyscy ? "wszystkim"
                : FO_COMPOSITE_LIVE(osoby, przedstawiany, PL_CEL)) + ".\n");
        else
            write("Ju�.\n");


        inni = FILTER_LIVE(all_inventory(environment(this_player()))) -
                 ({ this_player() }) - odbiorcy;

        size = sizeof(inni);
        i = -1;
        while(++i < size)
        {
            tell_object(inni[i], this_player()->query_Imie(inni[i], PL_MIA) +
                " przedstawia si� " +
                FO_COMPOSITE_LIVE(FILTER_CAN_SEE(odbiorcy, inni[i]),
                inni[i], PL_CEL) + ".\n");
        }
    }

    if (!sizeof(osoby))
    {
        /*Ma�a przer�bka, �eby ten komunikat o braku sensu
                   przedstawiania si� nie pokazywa� si� przed bramami */
        object *brama=filter((all_inventory(environment(this_player()))),
                              &->is_gate());
        if(!sizeof(brama))
            write("Nie widzisz w tym co prawda najmniejszego sensu, gdy� "+
                "nie ma nikogo, kto m�g�by ci� us�ysze�.\n");
    }


    size = sizeof(odbiorcy);
    i = -1;

    mianownik = przedstawiany->query_real_name(PL_MIA);
    dopelniacz = przedstawiany->query_real_name(PL_BIE);

    while (++i < size)
        odbiorcy[i]->add_introduced(mianownik, dopelniacz);

    return 1;
}


/*
 * przedstawieni - Zwraca liste osob, ktore sie nam przedstawily.
 */
int
przedstawieni(string str)
{
    object ob;
    mapping tmp;

    tmp = this_player()->query_introduced();
    if (m_sizeof(tmp))
    {
        write("Przypominasz sobie " +
            COMPOSITE_WORDS(map(sort_array(m_values(tmp)), capitalize)) +
            ".\n");

        return 1;
    }
    else
    {
        write("Nie przypominasz sobie nikogo, kto by ci si� ostatnio "+
            "przedstawi�, a kogo by� nie pami�ta�" +
            this_player()->koncowka("", "a") + ".\n");
        return 1;
    }
}


/*
 * przekaz - Przekazywanie dowodzenia
 */
int
przekaz(string str)
{
    mixed *oblist;
    mixed *team;

    if (!str || !parse_command(str, environment(this_player()), " 'prowadzenie' %l:" + PL_CEL, oblist) || !sizeof(oblist))
    {
        notify_fail("Przeka^z prowadzenie <komu>?\n");
	return 0;
    }

    oblist = NORMAL_ACCESS(oblist, 0, 0);
    oblist -= ({ this_player() });

    if (!sizeof(oblist))
    {
        notify_fail("Przeka^z prowadzenie <komu>?\n");
	return 0;
    }

    if (sizeof(oblist) > 1)
    {
	notify_fail("Przekaza^c prowadzenie mo^zesz tylko jednej, konkretnej osobie.\n");
	return 0;
    }

    team = this_player()->query_team_others();

    if (this_player()->query_leader())
    {
	notify_fail("Przecie^z nie przewodzisz druzynie.\n");
	return 0;
    }

    if (member_array(oblist[0], team) == -1)
    {
	this_player()->catch_msg(QCIMIE(oblist[0], PL_MIA) + " nie jest cz^lonkiem twojej dru^zyny.\n");
	return 1;
    }

    foreach (object memb : team)
	this_player()->team_leave(memb);

    foreach (object memb : team - ({ oblist[0] }))
	oblist[0]->team_join(memb);
    oblist[0]->team_join(this_player());

    foreach (object memb : team - ({ oblist[0] }))
	memb->catch_msg(QCIMIE(this_player(), PL_MIA) + " przekazuje prowadzenie dru^zyny " + QIMIE(oblist[0], PL_CEL) + ".\n");
    oblist[0]->catch_msg(QCIMIE(this_player(), PL_MIA) + " przekazuje ci prowadzenie dru^zyny.\n");
    this_player()->catch_msg("Przekazujesz prowadzenie dru^zyny " + QIMIE(oblist[0], PL_CEL) + ".\n");

    return 1;
}

/*
 * przyzwij - Komenda dla graczy do komunikacji z czarodziejami.
 */
int
przyzwij(string str)
{
    object *us;
    object spec;
    int il;
    int flag = 0;
    int size;
    string *arg;
    string *alias;
    string wiz;
    string mess;

    if (!stringp(query_ip_number(this_player())))
        return notify_fail("Tylko prawdziwi gracze mog� przyzywa� czarodziej�w.\n");

    if (this_player() != this_interactive())
    {
        tell_object(this_interactive(),
            "Przyzywanie to czynno�� jak� gracze musz� wykona� " +
                "samodzielnie.\n");
        notify_fail("Niedopuszczone wezwanie, jako �e musisz " +
            "wykona� je samodzielnie.\n");
        return 0;
    }

    if (this_player()->query_wiz_level())
    {
        notify_fail("Wzywanie jest czym� zarezerwowanym dla graczy, ty "+
            "za� by� mo�e chcesz skorzysta� z komendy 'audience' lub 'wiz'.\n");
        return 0;
    }

    if (!stringp(str))
    {
        write("Sprawd� pomoc do komendy przyzwij ('?przyzwij'), �eby si� " +
            "dowiedzie�, jak dzia�a ta komenda. Ale uwa�aj! �miertelnicy " +
            "s� nara�eni na potworny gniew czarodziej�w w przypadku " +
            "nieuzasadnionego u�ycia.\n");
        return 1;
    }

    arg = explode(str, " ");
    if (sizeof(arg) < 1)
    {
        notify_fail("Sprawd� pomoc do komendy przyzwij ('?przyzwij'), " +
            "�eby si� dowiedzie�, jak dzia�a ta komenda.\n");
        return 0;
    }

    wiz = lower_case(arg[0]);
    str = capitalize(this_interactive()->query_real_name());
    mess = implode(arg[1..], " ") + "\n";

    object old_player = TP;
    int miej=0;
    switch(wiz)
    {
        case "miejscowych":
            miej = 1;
        case "wszystkich":
            us = filter(users(), &->query_wiz_level());
            il = -1;
            size = sizeof(us);

            while(++il < size)
            {
                if (query_ip_number(us[il]) &&
                    !(us[il]->query_prop(WIZARD_I_BUSY_LEVEL) & BUSY_C))
                {
                    set_this_player(us[il]);
                    tell_object(us[il], set_color(31, 1) +
                        "PRZYZWANIE " + (miej ? "miejscowych" : "kogokolwiek") + " od '" +
                        str + "': " + mess + clear_color());
                    flag = 1;
                }
            }
            set_this_player(old_player);
        break;

    default:

        setuid();
        seteuid(getuid());

        if ((IS_MAIL_ALIAS(wiz) &&
            sizeof(alias = EXPAND_MAIL_ALIAS(wiz))) ||
            ((SECURITY->query_domain_number(capitalize(wiz)) > -1) &&
            sizeof(alias = SECURITY->query_domain_members(capitalize(wiz)))))
        {
            wiz = capitalize(wiz);

            for (il = 0; il < sizeof(alias); il++)
            {
                if (!(spec = find_player(alias[il])))
                    continue;

                if (!spec->query_wiz_level() ||
                    !query_ip_number(spec) ||
                    (spec->query_prop(WIZARD_I_BUSY_LEVEL) & BUSY_C))
                {
                    continue;
                }

                set_this_player(spec);
                tell_object(spec, set_color(1)+set_color(31)+"PRZYZWANIE " + wiz + " od '" +
                    str +  "': " + mess+clear_color());
                set_this_player(old_player);
                if (!spec->query_invis())
                    flag = 1;
            }

            break;
        }


        us = users();
        il = sizeof(users()) - 1;
        while ((us[il]->query_real_name(PL_BIE) != wiz) &&
               (--il >= 0))
            ;

        wiz = capitalize(wiz);

        if (il == -1 ||
            !(spec = us[il])->query_wiz_level() ||
            !query_ip_number(spec) ||
            (spec->query_prop(WIZARD_I_BUSY_LEVEL) & BUSY_C) ||
            spec->query_prop(OBJ_I_INVIS) > 0)
        {
            break;
        }

        set_this_player(spec);
        tell_object(spec,set_color(1)+set_color(31)+ "PRZYZWANIE ciebie od '" + str + "': " + mess+clear_color());
        set_this_player(old_player);
        flag = 1;
        break;
    }

    /* Log the commune message in a public log. */
    SECURITY->commune_log(wiz + ": " + mess);

    if (flag)
        write("Masz wra�enie, i� modlitwa od ciebie zosta�a wys�uchana.\n");
    else
        write("Masz wra�enie, i� twa modlitwa nie zosta�a wys�uchana.\n");

    return 1;
}

/*
 * wesprzyj - Wspomoz przyjeciela w walce.
 */
int
wesprzyj(string str)
{
    object *obs;
    object friend;
    object victim;
    int    index;
    mixed  tmp;

    if (!CAN_SEE_IN_ROOM(this_player()))
        return notify_fail("Nie widzisz tu nic.\n");

    if (this_player()->query_ghost())
    {
        return notify_fail("O w�a�nie, zabit" + this_player()->koncowka("y", "a") +
            ". Oto, jak" + this_player()->koncowka("i", "a") +
            " jeste�.\n");
    }

    if (!stringp(str))
    {
        if (!sizeof(obs = this_player()->query_team_others()))
            return notify_fail("Wesprzyj kogo? Nie jeste� w �adnej dru�ynie.\n");

        obs = ({ this_player()->query_leader() }) - ({ 0 }) + obs;

        for (index = 0; index < sizeof(obs); index++)
        {
            if ((environment(this_player()) == environment(obs[index])) &&
                (objectp(victim = obs[index]->query_attack())))
            {
                friend = obs[index];
                break;
            }
        }

        if (!objectp(friend))
            return notify_fail("Nikt z twojej dru�yny nie jest zaanga�owany w walk�.\n");
    }
    else
    {
        obs = parse_this(str, "%l:" + PL_BIE);

        this_player()->set_obiekty_zaimkow(obs);

        if (sizeof(obs) > 1)
            return notify_fail("Zdecyduj si�, nie mo�esz wesprze� jednocze�nie " + COMPOSITE_LIVE(obs, PL_BIE) + ".\n");
        else if (sizeof(obs) == 0)
            return notify_fail("Wesprzyj kogo?\n");

        friend = obs[0];
    }

    if (friend == this_player())
    {
        write("Jasne! Wesprzyj si� sam" + this_player()->koncowka("", "a") +
            "!\n");
        return 1;
    }

    if (member_array(friend, this_player()->query_enemy(-1)) != -1)
    {
        write("Pomoc " + friend->query_imie(this_player(), PL_CEL) +
            " w zabiciu siebie? S� chyba prostsze sposoby na pope�nienie "+
            "samob�jstwa...\n");
        return 1;
    }

    victim = friend->query_attack();
    if (!objectp(victim))
    {
        write(friend->query_Imie(this_player(), PL_MIA) + " nie walczy z nikim.\n");
        return 1;
    }

    if (member_array(victim, this_player()->query_team_others()) != -1)
        return notify_fail("Ale jeste�cie razem w dru�ynie z " + victim->query_imie(this_player(), PL_NAR) + ".\n");

    if (this_player()->query_attack() == victim)
    {
        write("Ju� walczysz z " + victim->query_imie(this_player(), PL_NAR) +
            ".\n");
        return 1;
    }

    if (tmp = environment(this_player())->query_prop(ROOM_M_NO_ATTACK))
    {
        if (stringp(tmp))
            write(tmp);
        else
            write("Jaka� boska si�a nie pozwala ci zaatakowa�.\n");
        return 1;
    }

    if (tmp = victim->query_prop(OBJ_M_NO_ATTACK))
    {
        if (stringp(tmp))
            write(tmp);
        else
            write("To istnienie jest chronione przez jak�� bosk� si�� - " +
                "nie wa�ysz si� na� podnie�� r�ki.\n");
        return 1;
    }

    if ((!this_player()->query_npc()) &&
        (this_player()->query_met(victim)) &&
        (this_player()->query_prop(LIVE_O_LAST_KILL) != victim))
    {
        write("Zaatakowa� " + victim->query_imie(this_player(), PL_BIE) +
            "?!? Potwierd� to przez ponowne zaatakowanie.\n");
        this_player()->add_prop(LIVE_O_LAST_KILL, victim);
        return 1;
    }

    this_player()->reveal_me(1);

    say(QCIMIE(this_player(), PL_MIA) + " wspiera " + QIMIE(friend, PL_BIE) +
        " w walce z " + QIMIE(victim, PL_NAR) + ".\n",
        ({ this_player(), friend, victim }) );
    victim->catch_msg(set_color(victim, COLOR_FG_RED, COLOR_BOLD_ON) +
        this_player()->query_Imie(victim, PL_MIA) + " wspiera " +
        friend->query_Imie(victim, PL_DOP) + " w walce z TOB�!\n" + clear_color());
    friend->catch_msg(set_color(friend, COLOR_FG_GREEN) + this_player()->query_Imie(friend) +
        " wspiera ci� w walce z " + victim->query_imie(friend, PL_NAR) +
        ".\n" + clear_color());

    this_player()->attack_object(victim);
    this_player()->add_prop(LIVE_O_LAST_KILL, victim);

    return 1;
}


/**
 * zapamietaj - Zapami�tuje osob�, kt�ra si� nam przedstawi�a.
 */
int
zapamietaj(string str)
{
    object ob;
    mapping tmp;
    int num;
    string *imie;

    if (!strlen(str))
        return notify_fail("Zapami�taj kogo?\n");

    str = lower_case(str);

    if (this_player()->query_real_name(PL_BIE) == str)
    {
        write("Kogo jak kogo, ale siebie znasz wystarczaj�co dobrze.\n");
        return 1;
    }

    if (this_player()->query_wiz_level())
    {
        write("Ale� nie�miertelni znaj� ka�dego!\n");
        return 1;
    }

    switch (this_player()->add_remembered(str))
    {
        case -1:
            write("Nie wepchniesz ju� niczego wi�cej do swej biednej g�owy.\n");
            return 1;
        case 1:
            write("Ju�.\n");
            return 1;
        case 2:
            write("Wspominasz " + capitalize(str) + ".\n");
            return 1;
        default:
            notify_fail("Nie przypominasz sobie, �eby� ostatnio spotka�" +
                this_player()->koncowka("", "a") + " osob� o tym imieniu.\n");
            return 0;
    }
}

/*
 * zapamietani - Lista osob, ktore mozemy jeszcze zapamietac
 */
int
zapamietani(string str)
{
    mapping tmp;
    int num;

    if (this_player()->query_wiz_level())
        return notify_fail("Nie�miertelni, tacy jak ty, znaj� ka�dego.\n");

    tmp = this_player()->query_remembered();
    if (mappingp(tmp))
    {
        if (num = m_sizeof(tmp))
        {
            num = F_MAX_REMEMBERED(this_player()->query_stat(SS_INT)) - num;
            if (num < 0)
                num = 0;

            write("Pami�tasz ");
            write(COMPOSITE_WORDS(map(sort_array(m_values(tmp)), capitalize)) +
                    ".\n");
            if (num)
                write("Wydaje ci si�, �e tw�j umys� jest w stanie pomie�ci� " +
                    "jeszcze " + LANG_SNUM(num, PL_BIE, PL_NIJAKI_NOS) + " " +
                    (num > 1 ? ({ "imi�", "imion", "",
                    "imiona" })[LANG_PRZYP(num, PL_BIE, PL_NIJAKI_NOS)] :
                    "imi�") + ".\n");
            else
                write("Tw�j biedny umys� chyba nie pomie�ci ju� nikogo " +
                    "wi�cej.\n");

            return 1;
        }
    }

    write("Nikogo nie znasz.\n");

    return 1;
}


/*
 * zapomnij - zapomina kogos uprzednio zapamietanego
 */
int
zapomnij(string name)
{
    string *tab;

    if (!stringp(name))
        return notify_fail("Zapomnij kogo?\n");

    if (tab = this_player()->remove_remembered(name))
    {
        this_player()->add_introduced(tab[0], tab[1]);
        write("Ju�.\n");
        return 1;
    }
    else
        return notify_fail("Nie znasz nikogo o tym imieniu.\n");
}

/*
 * zapros - Zapros kogos do druzyny
 */
int
zapros(string name)
{
    object *oblist, member;
    int num, i;

    if (!name)
    {
        oblist = (object *) this_player()->query_invited();
        if (!oblist || !sizeof(oblist))
            write("Nie zaprosi�" + this_player()->koncowka("e�", "a�") +
                " nikogo do dru�yny.\n");
        else
        {
            if (sizeof(oblist) == 1)
                write("Zaprosi�" + this_player()->koncowka("e�", "a�") +
                     " " + oblist[0]->query_imie(this_player(), PL_BIE) +
                     ".\n");
            else
            {
                name = COMPOSITE_LIVE(oblist, PL_BIE);
                write("Zaprosi�" + this_player()->koncowka("e�", "a�") + " " +
                     LANG_SNUM(num = sizeof(oblist), PL_BIE, PL_ZENSKI) +
                     ({ " osob�", " os�b", " os�b", " osoby" })[LANG_PRZYP(num,
                     PL_BIE, PL_ZENSKI)] + ":\n" + name + ".\n");
            }
        }
        return 1;
    }

    if (this_player()->query_leader())
        return notify_fail("Nie mo�esz zaprasza� ludzi do dru�yny, je�li jej nie przewodzisz.\n");

    if (!parse_command(name, all_inventory(environment(this_player())), "%l:" + PL_BIE, oblist))
        return notify_fail("Zapro� kogo?\n");

    oblist = NORMAL_ACCESS(oblist, 0, 0);

    if (!(i = sizeof(oblist)))
        return notify_fail("Zapro� kogo?\n");

    this_player()->set_obiekty_zaimkow(oblist);

    if (i > 1)
    {
        write("Nie mo�esz zaprosi� jednocze�nie " + COMPOSITE_LIVE(oblist,
            PL_DOP) + ".\n");
        return 1;
    }

    member = oblist[0];

    if (member == this_player())
        return notify_fail("Nie musisz si� zaprasza� do w�asnej dru�yny!\n");

    if (!CAN_SEE(member, this_player()))
    {
        write("Ju�.\n");
        tell_object(member, "Kto� pr�buje ci� zaprosi� do jakiej� " +
            "dru�yny. Niestety nie widzisz kto to mo�e by�.\n");
        return 1;
    }

    this_player()->team_invite(member);
    tell_object(member, this_player()->query_Imie(member, PL_MIA) +
        " zaprasza ci� do swojej dru�yny.\n");
    this_player()->reveal_me(1);
    member->reveal_me(1);

    write("Ju�.\n");
    return 1;
}

/*
 * Zapytaj - zapytaj kogos o cos.
 */
int
zapytaj(string str)
{
    object *oblist;
    object *wizards;
    string msg;
    mixed tmp;
    int ix;

    if (!CAN_SEE_IN_ROOM(this_player()))
    {
        notify_fail("Nic tu nie widzisz.\n");
        return 0;
    }

    if (!stringp(str))
    {
        notify_fail("Zapytaj kogo o co?\n");
        return 0;
    }

    if (!parse_command(str, environment(this_player()),
                      "%l:" + PL_BIE + " 'o' %s",
                      oblist, msg))
    {
        if (!sizeof(oblist))
        {
            notify_fail("Nie ma tu nikogo takiego.\n");
            return 0;
        }

        if (!msg)
        {
            notify_fail("Chyba chcesz si� zapyta� _o_ cos?\n");
            return 0;
        }
    }

    oblist = NORMAL_ACCESS(oblist, 0, 0);

    if (!sizeof(oblist))
    {
        notify_fail("Nie ma tu nikogo takiego.\n");
        return 0;
    }

    if (sizeof(oblist) > 1)
    {
        notify_fail("Pytanie jednej osoby naraz ca�kowicie wystarczy.\n");
        return 0;
    }

    if (oblist[0] == this_player())
    {
        notify_fail("Siebie chcesz o to spyta�?\n");
        return 0;
    }

    if (!strlen(msg))
    {
        write("O co chcesz si� zapyta� "
            + oblist[0]->query_imie(this_player(), PL_BIE) + "?\n");
        return 1;
    }

    if (tmp = this_player()->query_prop(LIVE_M_MOUTH_BLOCKED))
    {
        write(stringp(tmp) ? tmp : "Nie mo�esz wydoby� z siebie g�osu.\n");
        return 1;
    }

    this_player()->set_obiekty_zaimkow(oblist);

    if (this_player()->query_option(OPT_ECHO))
    {
        write("Pytasz " + oblist[0]->query_imie(this_player(), PL_BIE)
            + " o " + msg + ".\n");
    }
    else
    {
        write("Ju�.\n");
    }

    /* If the actor is a mortal, give the message to all wizards. */
    if (!this_player()->query_wiz_level())
    {
        wizards = FILTER_IS_WIZARD(all_inventory(environment(this_player()))) -
             oblist;
        ix = sizeof(wizards);
        while (--ix >= 0)
        {
            wizards[ix]->catch_tell(this_player()->query_Imie(wizards[ix], PL_MIA) +
                " pyta " + oblist[0]->query_imie(wizards[ix], PL_BIE) +
                " o " + msg + ".\n");
        }
    }
    else
    {
        wizards = ({ });
    }

    this_player()->reveal_me(1);
    saybb(QCIMIE(this_player(), PL_MIA) + " pyta " + QIMIE(oblist[0], PL_BIE) +
        " o co�.\n", wizards + oblist + ({ this_player() }) );

    tell_object(oblist[0], this_player()->query_Imie(oblist[0], PL_MIA) +
                " pyta si� ciebie o " + msg + ".\n");
    oblist[0]->catch_question(msg);
    oblist[0]->reveal_me(1);

    return 1;
}
