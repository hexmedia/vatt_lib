/**
 * \file /cmd/live/fight.c
 *
 *  W pliku tym znajduj� si� lub te� docelowo b�d� znajdowa�
 * wszystkie komendy odpowiedzialne za prowadzenie walki:
 *
 * Dost�pne komendy:
 *
 *  - og�usz    - pr�ba og�uszenia wroga,
 *  - opcje     - opcje walki,
 *  - prze�am   - pr�ba prze�amania zas�ony,
 *  - przesta�  - zaprzestanie walki, zas�aniania,
 *  - walka     - alias do opcje walki,
 *  - wrogowie  - pokazanie listy obecnych wrog�w,
 *  - wyceluj   - wycelowanie w dany obszar hitlokacji,
 *  - wyzwij    - wyzwanie drugiego gracza na pojedynek,
 *  - zabij     - rozpocz�cie walki na �mier� i �ycie,
 *  - zablokuj  - pr�ba zablokowania wyj�cia - jednego,
 *  - zas�o�    - zas�oni�cie kogo� swoim cia�em.
 *
 * @author Krun
 * @date 06.04.2008
 */

#pragma no_reset
#pragma no_inherit
#pragma save_binary
#pragma strict_types

inherit "/cmd/std/command_driver";

#include <combat.h>
#include <colors.h>
#include <macros.h>
#include <options.h>
#include <ss_types.h>
#include <cmdparse.h>
#include <state_desc.h>
#include <filter_funs.h>
#include <stdproperties.h>

mixed health_state;

private nomask void create()
{
    health_state =  ({ SD_HEALTH("y"), SD_HEALTH("a"), SD_HEALTH("e") });
}

string
get_soul_id()
{
    return "fight";
}

int
query_cmd_soul()
{
    return 1;
}

mapping
query_cmdlist()
{
    return ([
            "opcje"     : "opcje_walki",
            "walka"     : "opcje_walki", //alias ma by�!!
            "og�usz"    : "zabij",

            "prze�am"   : "przelam",
            "przesta�"  : "przestan",

            "wrogowie"  : "wrogowie",
            "wyceluj"   : "wyceluj",
            "wyzwij"    : "wyzwij",

            "zabij"     : "zabij",
            "zablokuj"  : "zablokuj",
            "zas�o�"    : "zaslon",
            ]);
}


/**
 * opcje walki - Opcje dotycz�ce walki.
 */
public int opcje_walki(string str, int wyr = 0)
{
    int plec;
    string title, val;

    if(!wyr && query_verb() != "walka")
    {
        if(explode(str, " ")[0] != "walki")
            return 0;

        str = implode(explode(str, " ")[1..], " ");

        if(str == "")
            str = 0;
    }

    if(!str)
    {
        opcje_walki("og�uszanie",   -1);
        opcje_walki("opisywanie",   -1);
        opcje_walki("styl",         -1);
        opcje_walki("uciekaj",      -1);
        opcje_walki("atakowane",    -1);
        opcje_walki("walka",        -1);
        return 1;
    }

    plec = TP->query_gender();

    switch(str)
    {
        case "og^luszanie":
        case "og�uszanie graczy":
            title   = "Og^luszanie graczy",
            val     = (TP->query_option(OPT_FIGHT_MERCIFUL) ? "W��czone" : "Wy��czone");
            break;

        case "opisuj":
        case "opisywanie":
        case "opisywanie walki":
        case "opisywanie walki innych":
        case "opisywanie walk":
        case "opisywanie walk innych":
        case "opisuj walki":
        case "opisuj walki innych":
            title   = "Opisywanie walki innych";
            val     = (TP->query_option(OPT_FIGHT_BLOOD) ? "Wy��czone" : "W��czone");
            break;

        case "styl":
        case "styl walki":
            title = "Styl walki";
            switch(TP->query_option(OPT_FIGHT_STYLE))
            {
                case CB_STL_BRAK:                   val = "Brak";                   break;
                case CB_STL_SILNY_NEUTRALNY:        val = "Silny neutralny";        break;
                case CB_STL_SILNY_OFENSYWNY:        val = "Silny ofensywny";        break;
                case CB_STL_SILNY_DEFENSYWNY:       val = "Silny defensywny";       break;
                case CB_STL_SZYBKI_NEUTRALNY:       val = "Szybki neutralny";       break;
                case CB_STL_SZYBKI_OFENSYWNY:       val = "Szybki ofensywny";       break;
                case CB_STL_SZYBKI_DEFENSYWNY:      val = "Szybki defensywny";      break;
                case CB_STL_GRUPOWY_NEUTRALNY:      val = "Grupowy normalny";       break;
                case CB_STL_GRUPOWY_OFENSYWNY:      val = "Grupowy ofensywny";      break;
                case CB_STL_GRUPOWY_DEFENSYWNY:     val = "Grupowy defensywny";     break;
                default:
                    val = "B��d " + TP->query_option(OPT_FIGHT_STYLE) + ", " +
                        type_name(TP->query_option(OPT_FIGHT_STYLE)) + ".\n";
            }
            break;

        case "uciekaj":
        case "uciekaj przy":
        case "uciekaj przy kondycji":
            int wi = this_player()->query_option(OPT_FIGHT_WHIMPY);

            if(wi)
            {
                wi = wi * sizeof(health_state[plec]) / 100;
                val = capitalize(health_state[plec][wi]);
            }
            else
                val = "Nigdy";

            val     = "'" + val + "'";
            title   = "Uciekaj przy";
            break;

        case "atakowane":
        case "atakowany":
        case "atakowany obszar":
        case "atakowane miejsce":
            title   = "Atakowane miejsce";

            switch(TP->query_option(OPT_FIGHT_ATAKOWANE))
            {
                case 0:               val = "Brak";       break;
                case HA_GLOWA:        val = "G�owa";      break;
                case HA_KORPUS:       val = "Korpus";     break;
                case HA_L_REKA:       val = "Lewa r�ka";  break;
                case HA_P_REKA:       val = "Prawa r�ka"; break;
                case HA_L_NOGA:       val = "Lewa noga";  break;
                case HA_P_NOGA:       val = "Prawa noga"; break;
                default:
                    val = "B��d " + TP->query_option(OPT_FIGHT_ATAKOWANE) + ", " +
                        type_name(TP->query_option(OPT_FIGHT_ATAKOWANE)) + ".";
            }

            break;

        case "walka":
        case "czysta":
        case "honorowa":
        case "czysta walka":
        case "honorowa walka":
        case "walka czysta":
        case "walka honorowa":
            title   = "Czysta/Honorowa walka";
            val = (TP->query_option(OPT_FIGHT_HONOROWA) ? "W��czona" : "Wy��czona");
            break;
    }

    if(title && val)
    {
        if(wyr == -1)
            write("-->  "+ sprintf("%-30s: %-45s\n", title, val + ","));
        else
            write("-->  "+ title + ": " + val + "\n");

        return 1;
    }
    else if(!str)
        return 0;

    val = lower_case(val);

    if(sscanf(str, "og�uszanie innych graczy %s", val) == 1 ||
        sscanf(str, "og�uszanie graczy %s", val) == 1 ||
        sscanf(str, "og�uszanie %s", val) == 1)
    {
        if(val == "+" || val ~= "w��cz")
            TP->set_option(OPT_FIGHT_MERCIFUL, 1);
        else if(val == "-" || val ~= "wy��cz")
            TP->set_option(OPT_FIGHT_MERCIFUL, 0);
        else
            return NF("Nie rozumiem. Mam w��czy� czy wy��czy� og�uszanie innych graczy?\n");

        opcje_walki("og�uszanie", 1);
    }
    else if(sscanf(str, "opisywanie walki innych graczy %s", val) == 1 ||
        sscanf(str, "opisywanie walk innych graczy %s", val) == 1 ||
        sscanf(str, "opisywanie walki innych %s", val) == 1 ||
        sscanf(str, "opisywanie walk innych %s", val) == 1 ||
        sscanf(str, "opisywanie walki %s", val) == 1 ||
        sscanf(str, "opisywanie walk %s", val) == 1 ||
        sscanf(str, "opisywanie %s", val) == 1 ||
        sscanf(str, "opisuj walki innych graczy %s", val) == 1 ||
        sscanf(str, "opisuj walki innych %s", val) == 1 ||
        sscanf(str, "opisuj walki %s", val) == 1 ||
        sscanf(str, "opisuj %s", val) == 1)
    {
        if(val ~= "w��cz" || val == "+")
            TP->set_option(OPT_FIGHT_BLOOD, 0);
        else if(val ~= "wy��cz" || val == "-")
            TP->set_option(OPT_FIGHT_BLOOD, 1);
        else
            return NF("Nie rozumiem. Mam w��czy�(+) czy wy��czy�(-)?\n");

        opcje_walki("opisuj walki", 1);
    }
    else if(sscanf(str, "styl %s", val) == 1 || sscanf(str, "styl walki %s", val) == 1)
    {
        int opt;

        switch(val)
        {
            case "silny":
            case "silny neutralny":
                opt = CB_STL_SILNY_NEUTRALNY;
                break;
            case "silny ofensywny":
                opt = CB_STL_SILNY_OFENSYWNY;
                break;
            case "silny defensywny":
                opt = CB_STL_SILNY_DEFENSYWNY;
                break;
            case "szybki":
            case "szybki neutralny":
                opt = CB_STL_SZYBKI_NEUTRALNY;
                break;
            case "szybki ofensywny":
                opt = CB_STL_SZYBKI_OFENSYWNY;
                break;
            case "szybki defensywny":
                opt = CB_STL_SZYBKI_DEFENSYWNY;
                break;
            case "grupowy":
            case "grupowy neutralny":
                opt = CB_STL_GRUPOWY_NEUTRALNY;
                break;
            case "grupowy ofensywny":
                opt = CB_STL_GRUPOWY_OFENSYWNY;
                break;
            case "grupowu defensywny":
                opt = CB_STL_GRUPOWY_DEFENSYWNY;
                break;
            case "brak":
            case "brak stylu":
                opt = CB_STL_BRAK;
                break;
            default:
                NF("Taki styl walki nie istnieje. Dost�pne style: " +
                    "silny, szybki, grupowy wraz z podstylami neutralnym, ofensywnym i defensywnym oraz brak stylu.\n");
                return 0;
        }
        TP->set_option(OPT_FIGHT_STYLE, opt);

        opcje_walki("styl", 1);
    }
    else if(sscanf(str, "uciekaj przy %s", val) == 1 || sscanf(str, "uciekaj %s", val))
    {
        int wi;

        if(val == "nigdy" || val == "brak")
            this_player()->set_whimpy(0);
        else if(val == "?")
            write("nigdy, " + COMPOSITE_WORDS(health_state[plec]) + "\n");
        else
        {
            string *hs = secure_var(health_state[plec]);
            hs = map(hs, clear_color_format);
            wi = member_array(val, hs);

            if (wi < 0)
            {
                return notify_fail("Nie ma takiego poziomu kondycji. Dost�pne to:\n" +
                    break_string("nigdy, " + COMPOSITE_WORDS(health_state[plec]) + ".",
                    70, 3) + "\n");
            }

            wi = (100 * (wi + 1)) / sizeof(health_state[plec]);
            if (wi > 99)
                wi = 99;

            this_player()->set_option(OPT_FIGHT_WHIMPY, wi);
        }

        opcje_walki("uciekaj", 1);
    }
    else if(sscanf(str, "atakowane miejsce %s", val) == 1 || sscanf(str, "atakowany obszar %s", val) == 1 ||
        sscanf(str, "atakowane %s", val) == 1 || sscanf(str, "atakowany %s", val) == 1)
    {
        int ha;

        if(val == "brak")
            ha = 0;
        else if(val ~= "g�owa")
            ha = HA_GLOWA;
        else if(val == "korpus")
            ha = HA_KORPUS;
        else if(val ~= "lewa r�ka")
            ha = HA_L_REKA;
        else if(val ~= "prawa r�ka")
            ha = HA_P_REKA;
        else if(val == "lewa noga")
            ha = HA_L_NOGA;
        else if(val == "prawa noga")
            ha = HA_P_NOGA;
        else
        {
            write("Dost�pne s� tylko obszary istot humanoidalnych, takie jak:\n " +
                "    g�owa, korpus, prawa i lewa r�ka oraz prawa i lewa noga.\n"/*\t" + val + "\n"*/);
            return 1;
        }

        TP->set_option(OPT_FIGHT_ATAKOWANE, ha);

        opcje_walki("atakowane", 1);
    }
    else if(sscanf(str, "walka honorowa %s", val) == 1 || sscanf(str, "czysta walka %s", val) == 1 ||
        sscanf(str, "honorowa walka %s", val) == 1 || sscanf(str, "walka czysta %s", val) == 1 ||
        sscanf(str, "czysta %s", val) == 1 || sscanf(str, "honorowa %s", val) == 1 ||
        sscanf(str, "walka %s", val) == 1)
    {
        if(val ~= "w��cz" || val == "+")
            TP->set_option(OPT_FIGHT_HONOROWA, 1);
        else if(val ~= "wy��cz" || val == "-")
            TP->set_option(OPT_FIGHT_HONOROWA, 0);
        else
            return NF("Nie rozumiem. Mam w��czy�(+) czy wy��czy�(-)?\n");

        opcje_walki("walka", 1);
    }
    else
    {
        NF("Nie ma takiej opcji walki.\n");
        return 0;
    }

    return 1;
}

/**
 * prze�am - prze�amanie czyjej� zas�ony
 */
public int przelam(string str)
{
    NF("Co chcesz prze�ama�?\n");

    if(!str)
        return 0;

    int nk;
    object *kogo, *na_kim;
    if(!parse_command(str, AIE(TP), "'zas�on�' 'na' %l:" + PL_MIE, na_kim))
    {
        if(!parse_command(str, AIE(TP), "'zas�on�' %l:" + PL_BIE + " 'na' %l:" + PL_MIE, kogo, na_kim))
            return 0;
        else
        {
            nk = 1;
            kogo = NORMAL_ACCESS(kogo, 0, 0);
        }
    }

    NF("Zas�one na kim chcesz prze�ama�?\n");

    na_kim = NORMAL_ACCESS(na_kim, 0, 0);

    if(!na_kim)
        return 0;

    if(sizeof(na_kim) > 1)
    {
        NF("Nie mo�esz prze�ama� zas�ony na wi�cej ni� jednej osobie.\n");
        return 0;
    }

    if(nk)
    {
        if(!kogo)
            return 0;

        if(sizeof(kogo) > 1)
        {
            NF("Nie mo�esz prze�ama� zas�on kilku os�b na raz.\n");
            return 0;
        }
    }

    if(TP->przelam_zaslone(na_kim[0], kogo[0]))
    {
        write("Prze�amujesz zas�on� " + (nk ? kogo[0]->query_imie(TP, PL_DOP) + " " : "") + "na " +
            na_kim[0]->query_imie(TP, PL_MIE) + ".\n");
        saybb(QCIMIE(TP, PL_MIA) + " prze�amuje zas�one " + (nk ? QIMIE(kogo[0], PL_DOP) + " " : "") +
            "na " + QIMIE(na_kim[0], PL_MIE) + ".\n");
    }
    else
    {
        write("Nie uda�o ci si� prze�ama� zas�ony " + (nk ? kogo[0]->query_imie(TP, PL_DOP) + " " : "") +
            "na " + na_kim[0]->query_imie(TP, PL_MIE) + ".\n");
        saybb(QCIMIE(TP, PL_MIA) + " nie uda�o si� prze�ama� zas�ony " + (nk ? QIMIE(kogo[0], PL_DOP) + " " : "") +
            "na " + QIMIE(na_kim[0], PL_MIE) + ".\n");
    }

    return 1;
}

//Funkcja u�ywana przez przestan()
varargs void przestan_walczyc2(mixed i, object tp, int z_input=0)
{
    object *ens = ({});

    if(stringp(i))
        i = ({atoi(i)});
    else if(intp(i))
        i = ({i});

    if(z_input)
        i[0]--;

    foreach(int j : i)
    {
        object en = tp->query_enemy(j);
        ens += ({en});

        if(!en)
        {
            write("Nie wiem z kim chcesz przesta� walczy�.\n");
            return;
        }

        if(ENV(en) == ENV(tp))
        {
            write("Nie przestaniesz atakowa� osoby, kt�ra w�a�nie czycha na ciebie z broni�.\n");
            return;
        }

        tp->stop_fight(en);

        FILTER_OTHER_LIVE(AIE(tp))->guard_signal_stop_fight(tp, en);
    }

    write("Decydujesz si� nie atakowa� " + COMPOSITE_LIVE(ens, PL_DOP) + ".\n");
}

private int przestan_walczyc(string str)
{
    int i;
    string *exp;
    object *e, *pe;

    notify_fail("Czego chcesz zaprzesta�?\n");

    if(!stringp(str))
        return 0;

    pe = this_player()->query_enemy(-1);

    exp = explode(str, " ");
    if(!(exp[0] ~= "walczy�") && !(exp[0] ~= "atakowa�") && !(exp[0] ~= "goni�"))
        return 0;

    if (!pe || !sizeof(pe))
        return notify_fail("Nie atakujesz nikogo.\n");

    if(sizeof(pe) == 1)
    {
        przestan_walczyc2(0, TP);
        return 1;
    }

    if(parse_command((exp[0] ~= "walczy�" ? implode(exp[2..], " ") : implode(exp[1..], " ")), AIE(TP) + AI(TP),
        "%l:" + (exp[0] ~= "walczy�" ? PL_NAR : PL_BIE), e))
    {
        e = NORMAL_ACCESS(e, 0, 0);

        e = filter(e, &operator(!=)(-1, ) @ &member_array(, pe));

        if(!e)
            return notify_fail("Z kim chcesz przesta� walczy�?\n");

        przestan_walczyc2(map(e, &member_array(, pe)), TP);

        this_player()->set_obiekty_zaimkow(pe);
        return 1;
    }

    write("Osoby z kt�rymi walczysz:\n");
    for(int i = 0 ; i < sizeof(pe) ; i++)
        write(sprintf("%2d) %-60s\n", (i+1), pe[i]->query_Imie(TP, PL_NAR)));

    write("Wprowad� numer osoby z kt�r� chcesz przesta� walczy�:");
    input_to(&przestan_walczyc2(, TP, 1));

    return 1;
}

private int przestan_zaslaniac(string str)
{
    mixed ret;
    if(ret && !stringp(ret))
    {
        write("Przestajesz zas�ania�" + (objectp(ret) ? " " + ret->query_Imie(TP, PL_BIE) : "") + ".\n");
        return 1;
    }
    else if(stringp(ret))
        NF(ret);
}

/**
 * przesta� - Przesta� walczy� lub zas�ania�
 */
int
przestan(string str)
{
    NF("Czego chcesz zaprzesta�?\n");

    if(wildmatch("*walczy�*", str))
        return przestan_walczyc(str);
    else if(wildmatch("*zas�ania�*", str))
        return przestan_zaslaniac(str);

    return 0;
}

/**
 * wrogowie - pokazuje liste aktualnych wrog�w.
 */
public int
wrogowie(string str)
{
       //"wrogi" ? bahaha, to m�g� tylko Krun napisa� XD
       //pozdrawiam go zatem, V. :)
    object *wrogi = TP->query_attacked_by();
    wrogi -= ({TP->query_enemy()});
    wrogi += ({TP->query_enemy()});
    wrogi -= ({0});

    if(!sizeof(wrogi))
    {
        write("Nie masz w tej chwili �adnych wrog�w, z kt�rymi walczysz.\n");
        return 1;
    }

    write("Aktualnie twoimi wrogami s�: " +
        COMPOSITE_LIVE(wrogi, PL_MIA) + ".\n");

    return 1;
}

/**
 * wyceluj - celowanie w dany obszar hitlokacji przeciwnika
 */
public int wyceluj(string str)
{
    return 0;
}

/**
 * wyzwij - Wyzwanie jakiego� livinga na pojedynek do jakiego�
 *          stanu.
 */
public int wyzwij(string str)
{
    return 0;
}

/**
 * zabij - Zaatakuj kogo�, w celu zabicia b�d� og�uszenia go.
 */
varargs int
zabij(string str)
{
    object ob, *oblist;
    string str2;
    mixed  tmp;
    int i, size;

    if (!CAN_SEE_IN_ROOM(this_player()))
        return notify_fail("Nie widzisz tu nic.\n");

    if (this_player()->query_ghost())
    {
        return notify_fail("O w�asnie, zabit" + this_player()->koncowka("y", "a") +
            ". Oto, jak" + this_player()->koncowka("i", "a") +
            " jeste�.\n");
    }

    if (!stringp(str))
        return notify_fail(capitalize(query_verb()) + " kogo?\n");

    if (str == "siebie" || str~="si�")
    {
        write("Wstrzymujesz na chwil� oddech, ale zaraz dajesz sobie spok�j.\n"+
            "Popro� kogo�, by ci w tym pom�g�.\n");
        return 1;
    }

    if (!parse_command(str, all_inventory(environment(this_player())),
        "%i:" + PL_BIE, oblist) || !sizeof(oblist = NORMAL_ACCESS(oblist, 0, 0)))
    {
        notify_fail("Nie widzisz �adnej takiej osoby.\n");
        return 0;
    }

    if (sizeof(oblist) > 1)
    {
        notify_fail("Troch� konkretniej, nie mo�esz zabi� jednocze�nie " +
            COMPOSITE_LIVE(oblist, PL_DOP) + ".\n");
        return 0;
    }

    this_player()->set_obiekty_zaimkow(oblist);

    ob = oblist[0];

    if (!objectp(ob))
        return notify_fail(capitalize(str) + " ju� tu nie ma!\n");

    if (!living(ob))
    {
        write("To nie jest istota �ywa!\n");
        saybb(QCIMIE(this_player(), PL_MIA) + " pr�buje zabi� " + str + ".\n");
        return 1;
    }

    if (ob == this_player())
    {
        write("Co takiego? Siebie chcesz zaatakowa�?\n");
        return 1;
    }

    if(query_verb() ~= "og�usz" && !interactive(ob))
        return notify_fail("Nie mo�esz " + ob->koncowka("go", "jej", "jego") + " og�uszy�.\n");

    if (this_player()->query_attack() == ob)
    {
        write("Tak, tak.\n");
        return 1;
    }

    if(this_player()->query_old_fatigue() <= 10)
    {
        write("Jeste� zbyt zm�czon"+this_player()->koncowka("y","a","e")+
            ", by to zrobi�.\n");
        return 1;
    }

    if (tmp = environment(this_player())->query_prop(ROOM_M_NO_ATTACK))
    {
        if (stringp(tmp))
            TP->catch_msg(tmp);
        else
            write("Jaka� boska si�a nie pozwala ci zaatakowa�.\n");
        return 1;
    }

    if ((tmp = ob->query_prop(OBJ_M_NO_ATTACK)) || (tmp = ob->query_prop(PLAYER_M_PROTECTED_AFTER_LOGIN)))
    {
        if (stringp(tmp))
            TP->catch_msg(tmp);
        else
            write("To istnienie jest chronione przez jak�� bosk� si�� - " +
                "nie wa�ysz si� na� podnie�� r�ki.\n");
        return 1;
    }

    if (!this_player()->query_npc() && this_player()->query_met(ob) && (this_player()->query_prop(LIVE_O_LAST_KILL) != ob))
    {
        write("Zaatakowa� " + ob->query_imie(this_player(), PL_BIE) + "?!? Potwierd� to przez ponowne zaatakowanie.\n");
        this_player()->add_prop(LIVE_O_LAST_KILL, ob);
        return 1;
    }

    if (this_player()->reveal_me(0))
        write("Atakuj�c " + ob->query_imie(this_player(), PL_BIE) +
            " wychodzisz z ukrycia.\n");

    if(TP->query_prop(OBJ_M_NO_ATTACK) || TP->query_prop(PLAYER_M_PROTECTED_AFTER_LOGIN))
    {
        TP->remove_prop(OBJ_M_NO_ATTACK);
        write("Przestajesz by� chroniony przed atakiem.\n");
    }

    say(QCIMIE(this_player(), PL_MIA) + " atakuje " + QIMIE(ob, PL_BIE) + ".\n",
        ({ this_player(), ob }) );
    ob->catch_msg(set_color(ob, COLOR_FG_RED, COLOR_BOLD_ON) +
        this_player()->query_Imie(ob, PL_MIA) + " atakuje ci�!\n" + clear_color(ob));

    this_player()->attack_object(ob, (query_verb() ~= "og�usz"));
    this_player()->add_prop(LIVE_O_LAST_KILL, ob);

    //FIXME: Sygna� rozpocz�cia walki przenios�em do cb_attack, ale jest on wysy�any
    //       w zupe�nie inny spos�b dlatego t� wersj� b�dzie mo�na usun�� dopiero jak
    //       tam sko�cz�, a nie wiem czy si� wyrobie przed otwarciem.
//     TP->prepare_signal_fight(ob);

    return 1;
}

/**
 * zablokuj - zablokowanie jakiemu� livingowi przej�cia w danym kierunku, lub
 *            te� ca�kowite zablokowanie przej�cia.
 */
public int zablokuj(string str)
{
    return 0;
}

/**
 * zas�o� - Zas�oni�cie kogo� przed ciosami innych w�asnym cia�em.
 */
public int zaslon(string str)
{
    NF("Kogo chcesz zas�oni� [przed kim] ?\n");

    if(!str)
        return 0;

    int pd;
    object *kogo, *przed;

    if(!parse_command(str, AIE(TP), "%l:" + PL_BIE, kogo))
    {
        if(!parse_command(str, AIE(TP), "%l:" + PL_BIE + " 'przed' %l:" + PL_NAR, kogo, przed))
            return 0;
        else
        {
            przed = NORMAL_ACCESS(przed, 0, 0);
            pd = 1;
        }
    }

    kogo = NORMAL_ACCESS(kogo, 0, 0);

    if(!kogo)
        return 0;

    if(sizeof(kogo) > 1)
    {
        NF("Nie dasz rady zas�oni� wi�cej ni� jednej osoby na raz.\n");
        return 0;
    }

    if(pd)
    {
        if(!przed)
        {
            NF("Przed kim chcesz zas�oni� " + kogo->query_Imie(TP, PL_BIE) + ".\n");
            return 0;
        }

        if(sizeof(przed) > 1)
        {
            NF("Mo�esz zrobi� albo zas�on� przed wszystkimi albo przed pojedyncz� osob�.\n");
            return 0;
        }
    }

    object zas;
    if(zas = TP->query_zaslaniany())
        TP->command("przesta� zas�ania� " + OB_NAME(zas));

    if(TP->zaslon(kogo[0]))
    {
        write("Zas�aniasz " + kogo[0]->short(TP, PL_MIA) +
            (pd ? " przed " + przed[0]->short(TP, PL_BIE) : "") + ".\n");
        saybb(QCIMIE(TP, PL_MIA) + " zas�ania " + QIMIE(kogo[0], PL_MIA) +
            (pd ? " przed " + QIMIE(przed[0], PL_BIE) : "") + ".\n");
    }
    else
    {
        write("Nie udaje ci si� zas�oni� " + kogo[0]->short(TP, PL_MIA) +
            (pd ? " przed " + przed[0]->short(TP, PL_BIE) : "") + ".\n");
        saybb(QCIMIE(TP, PL_CEL) + " nie udaje si� zas�oni� " + QIMIE(kogo[0], PL_MIA) +
            (pd ? " przed " + QIMIE(przed[0], PL_BIE) : "") + ".\n");
    }

    return 1;
}