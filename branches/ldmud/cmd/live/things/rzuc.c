/**
 *
 *   Plik obs�uguj�cy rzucanie przedmiotami.
 *   Obiekt nale�y sklonowa� do inwentarza gracza.
 *
 *   Tak jak W:GW przykaza� testujemy zawsze jak�� sum�
 *   cechy i umiej�tno�ci (jednej b�d� kilku).
 *
 *   TODO: parowanie nadlatuj�cych przedmiot�w broni� (tarcz�)
 *   TODO: obra�enia od broni broni
 *   TODO: przygniatanie przez ci�kie rzeczy
 *
 *   Nowy ficzer:
 *       wywo�ywanie funkcji object co->throw_effect(object cel)
 *       co tj. rzucany przedmiot, cel tj. cel ;) ew. ENV.
 *       Mo�emy sobie tak� funkcj� napisa� w obiekcie, np.
 *       w jajku i poda�, �e si� rozbija. Lub czego jeszcze dusza
 *       zapragnie.
 *       v.
 *
 * @author Molder
 */

#pragma strict_types

#include <composite.h>
#include <cmdparse.h>
#include <pl.h>
#include <stdproperties.h>
#include <macros.h>
#include <filter_funs.h>
#include <ss_types.h>
#include <wa_types.h>
#include <sit.h>
#include <materialy.h>
#include <exp.h>

public int rzuc(string str);
void dorzuc_paraliz_i_rzucaj();
void real_throw(object co, object cel);
int czy_wolne_rece(object co);
int czy_da_rade(object co);
int obj_speed(object co);
int losowa_hitlokacja(object cel);
int throw_try_hit(object co, int speed, object cel);
void throw_do_dam(object co, int v, object cel, int hid);
void przewroc(object cel);
void dodaj_expa_za_rzucanie(object co, object cel, int powodzenie_rzutu);

mapping czym_rzucamy = ([]);
mapping w_kogo_rzucamy = ([]);

/**
 * Funkcja parsuj�ca komend� 'rzu�'.
**/
public int
rzuc(string str)
{
    object *itemy;
    object item1, *item2;

    notify_fail("Rzu� czym w kogo?\n");

    if (!strlen(str))
        return 0;

    mixed tmp;
    if((tmp = ENV(TP)->query_prop(ROOM_CANT_THROW)))
    {
        if(stringp(tmp))
            notify_fail(tmp);
        else
            notify_fail("Tu nie mo�esz rzuca�!\n");
        return 0;
    }

    if (parse_command(str, environment(TP), "%i:" + PL_NAR + " 'w' %l:" + PL_BIE, itemy, item2))
    {
        itemy = CMDPARSE_STD->normal_access(itemy, "manip_drop_access", this_object());

        if(!sizeof(itemy))
        {
            notify_fail("Nie masz niczego takiego.\n");
            return 0;
        }
        if(sizeof(itemy)>1 || (itemy[0]->query_prop(HEAP_I_IS) && itemy[0]->num_heap()!=1 ))
        {
            notify_fail("Mo�esz rzuci� tylko jednym przedmiotem na raz.\n");
            return 0;
        }
        if((tmp=itemy[0]->query_prop(OBJ_M_NO_THROW)) || (tmp=itemy[0]->query_prop(OBJ_M_NO_DROP)))
        {
            if(stringp(tmp))
                return notify_fail(tmp);
            else
                return notify_fail("Nie mo�esz tym rzuci�.\n");
            return 0;
        }

        if(itemy[0]->query_prop("_att_id"))
        {
            notify_fail("Najpierw wyci�gnij "+itemy[0]->koncowka("go", "j�", "je")
                    +" zza pasa.\n");
            return 0;
        }

        item2 = CMDPARSE_STD->normal_access(item2, 0, this_object());

        if (sizeof(item2)>1)
        {
            notify_fail("Mo�esz rzuci� tylko w jedn� osob� na raz.\n");
            return 0;
        }

        //poprawka b��du z go��biami w czyim� inv, lub w meblu, Vera
        if(!sizeof(item2) || ENV(item2[0]) != ENV(TP))
            return 0;

        if(!czy_wolne_rece(itemy[0]) || !czy_da_rade(itemy[0]))
            return 0;

        //poprawka b��du z rzucaniem w ciemno�ciach:
        if(!sizeof(FILTER_IS_SEEN(item2[0],({TP}))) || !sizeof(FILTER_CAN_SEE_IN_ROOM(({TP}))) || TP->query_prop(EYES_CLOSED))
        {
            notify_fail("Hmm, wygl�da na to, �e nic nie widzisz, jak�e "+
            "zatem mo�na rzuca� do czego� w takich warunkach?\n");
            return 0;
        }

        czym_rzucamy[TP->query_name()] = itemy[0];
        w_kogo_rzucamy[TP->query_name()] = item2[0];

        dorzuc_paraliz_i_rzucaj();

        return 1;
    }
    else if (parse_command(str, environment(TP), "%i:" + PL_BIE, itemy))
    {
        itemy = CMDPARSE_STD->normal_access(itemy, "manip_drop_access", this_object());

        if(sizeof(itemy)>1)
        {
            notify_fail("Mo�esz rzuci� tylko jeden przedmiot na raz.\n");
            return 0;
        }

        if(sizeof(itemy))
        {
            if(itemy[0]->query_prop(OBJ_M_NO_DROP))
            {
                notify_fail(itemy[0]->query_prop(OBJ_M_NO_DROP));
                return 0;
            }

            TP->set_obiekty_zaimkow(itemy);

            if(ENV(TP)->query_prop(ROOM_I_TYPE) & ROOM_TREE == ROOM_TREE)
            {
                string dol = MASTER_OB(ENV(ENV(TP)->query_drzewo()));

                if(itemy[0]->query_prop(HEAP_I_IS) && itemy[0]->num_heap()!=1)
                    itemy[0]->split_heap(1);//przy ka�dym MOVE to ma by�! V.

                itemy[0]->move(dol,1);
                write("Rzucasz " + COMPOSITE_DEAD(itemy, PL_BIE) + " na ziemi�.\n");
                say(QCIMIE(TP, PL_MIA) + " rzuca " + QCOMPDEAD(PL_NAR) + " na ziemi�.\n");
                tell_room(dol, "Z drzewa spada " + QCOMPDEAD(PL_MIA) + ".\n");
                itemy[0]->throw_effect(dol); //np. do jajka rozbijaj�cego si�

                return 1;
            }

            if(itemy[0]->query_prop(HEAP_I_IS) && itemy[0]->num_heap()!=1)
                itemy[0]->split_heap(1);//przy ka�dym MOVE to ma by�! V.
            itemy[0]->move(environment(TP),1);
            write("Rzucasz " + COMPOSITE_DEAD(itemy, PL_BIE) + " na ziemi�.\n");
            say(QCIMIE(TP, PL_MIA) + " rzuca " + QCOMPDEAD(PL_NAR) + " na ziemi�.\n");
            itemy[0]->throw_effect(ENV(TP));//np. do jajka rozbijaj�cego si�
            return 1;
        }
        else
        {
            notify_fail("Nie masz niczego takiego.\n");
            return 0;
        }
    }

    return 0;
}

void dorzuc_paraliz_i_rzucaj()
{
    /*Dajmy info o zamachu! V.*/
    object co = czym_rzucamy[TP->query_name()];
    object cel = w_kogo_rzucamy[TP->query_name()];
    object *tab = allocate(1);
    //object co_cel = cel->query_combat_object();

     if(co->query_prop(HEAP_I_IS) && co->num_heap()!=1)
        co->split_heap(1);

    tab[0] = co;
    TP->catch_msg("Bierzesz zamach mierz�c " + COMPOSITE_DEAD(tab, PL_NAR)
        + " w " + QIMIE(cel, PL_BIE)+".\n");
    cel->catch_msg(QCIMIE(TP, PL_MIA) + " bierze zamach mierz�c "
        + COMPOSITE_DEAD(tab, PL_NAR) + " prosto do ciebie!\n");
    saybb(QCIMIE(TP, PL_MIA) + " bierze zamach mierz�c "
        + COMPOSITE_DEAD(tab, PL_NAR) + " do " + QIMIE(cel, PL_DOP)+".\n",({cel,TP}));

    object p;

    setuid();
    seteuid(getuid(this_object()));

    p=clone_object("/std/paralyze.c");
    p->set_finish_object(TO);
    p->set_finish_fun("koniec_paralizu_wiec_rzucamy");

    if(czym_rzucamy[TP->query_name()]->query_prop(OBJ_I_VOLUME) < 100)
        p->set_remove_time(2);
    else
        p->set_remove_time(5);

    p->set_stop_verb("przesta�");
    p->set_stop_message("Przestajesz bra� zamach.\n");
    p->set_fail_message("Chwil�, w�a�nie bierzesz zamach.\n");
    p->move(TP,1);
}

void koniec_paralizu_wiec_rzucamy()
{
    object co = czym_rzucamy[TP->query_name()];
    object w_kogo = w_kogo_rzucamy[TP->query_name()];

    //uwaga! sprawdzamy czy cel nie zwial!
    if(ENV(w_kogo) != ENV(TP))
    {
        write("Cel umkn��!\n");
        return;
    }

    /* hmm pewnie nawet wydajniej by�oby nie usuwac... */
    czym_rzucamy = m_delete(czym_rzucamy, TP->query_name());
    w_kogo_rzucamy = m_delete(w_kogo_rzucamy, TP->query_name());

    real_throw(co, w_kogo);
}

/**
 * Funkcja pomocnicza - sprawdza, czy item jest ma�y, je�li tak, to
 * przecie� mo�na go zgubi� takim rzutem! Tj. ukrywamy przedmiot.
 *
 * @param co        przedmiot kt�rym rzucamy
 * @param gdzie     potencjalne miejsce chowania
 *
 * @author Vera
 */
void
przenies_przedmiot_na_lok(object co,object gdzie)
{
    if(co->query_prop(HEAP_I_IS) && co->num_heap()!=1)
       co->split_heap(1);//przy ka�dym MOVE to ma by�! V.

    co->move(gdzie);
    int a, is_cont=0, lvl=0;

    a=co->query_prop(OBJ_I_VOLUME);
    if(!a)
    {
        a=co->query_prop(CONT_I_VOLUME);
        is_cont=1;
    }

    switch(a)
    {
        case 1..5:      lvl = 55+random(40);    break;
        case 6..20:     lvl = 50+random(20);    break;
        case 21..60:    lvl = 40+random(20);    break;
        case 61..100:   lvl = 30+random(20);    break;
        case 101..450:  lvl = 10+random(20);     break;
        default:        lvl = 0;                break;
    }

    if(is_cont)
        co->add_prop(CONT_I_HIDDEN, lvl);
    else
        co->add_prop(OBJ_I_HIDE, lvl);

    co->throw_effect(gdzie);//np. do jajka rozbijaj�cego si�
}

/**
 * Funkcja obs�uguj�ca od pocz�tku do ko�ca rzut czym� w co�.
**/
void
real_throw(object co, object cel)
{
    int hid;
    mixed *hitloc;
    int v; /* predkosc */
    int rzut; /* czy rzucili�my i trafili�my */

    /* tablica potrzebna do makr opisow */
    object *tab = allocate(1);
    object co_cel = cel->query_combat_object();

    if(co->query_prop(HEAP_I_IS) && co->num_heap()!=1)
        co->split_heap(1);//przy ka�dym MOVE to ma by�! V.

    tab[0] = co;

        if(!co->query_prop(OBJ_I_WEIGHT) || !co->query_prop(OBJ_I_VOLUME)) //przed wywo�aniem obj_speed(co)
        {
            write("Wyst�pi� krytyczny b��d w rzucaniu tym obiektem. Zg�o� to "+
                  "natychmiast, podaj�c okoliczno�ci, "+
                 "w jakich do tego dosz�o.\n");
//            notify_wizards(co,"Ten obiekt nie ma podstawowych prop�w!!!\n");
            return;
        }

    v = obj_speed(co);

    if(v > 100)
    {
        TP->catch_msg("Z ogromn� si�� rzucasz " + COMPOSITE_DEAD(tab, PL_NAR)
            + " w " + QIMIE(cel, PL_BIE));
        cel->catch_msg(QCIMIE(TP, PL_MIA) + " rzuca w ciebie "
            + COMPOSITE_DEAD(tab, PL_NAR) + ", kt�r" + co->koncowka("y","a","e") +
            " z ogromn� pr�dko�ci� mknie w twoj� stron�");
        saybb(QCIMIE(TP, PL_MIA) + " z ogromn� si�� rzuca "
            + COMPOSITE_DEAD(tab, PL_NAR) + " w " + QIMIE(cel, PL_BIE),({cel,TP}));
    }
    else if(v > 20)
    {
       TP->catch_msg("Rzucasz " + COMPOSITE_DEAD(tab, PL_NAR) + " w "
            + QIMIE(cel, PL_BIE));
       cel->catch_msg(QCIMIE(TP, PL_MIA) +
            " rzuca w ciebie " + COMPOSITE_DEAD(tab, PL_NAR));
       saybb(QCIMIE(TP, PL_MIA) + " rzuca " + COMPOSITE_DEAD(tab, PL_NAR)
            + " w " + QIMIE(cel, PL_BIE),({cel,TP}));
    }
    else
    {
       TP->catch_msg("Z du�ym wysi�kiem rzucasz "
            + COMPOSITE_DEAD(tab, PL_NAR) + " w " + QIMIE(cel, PL_BIE));
        cel->catch_msg(QCIMIE(TP, PL_MIA) + " z du�ym wysi�kiem rzuca w ciebie "
            + COMPOSITE_DEAD(tab, PL_NAR) + ", kt�r" + co->koncowka("y","a","e") +
            " wolno szybuje w twoj� stron�");
       saybb(QCIMIE(TP, PL_MIA) + " z du�ym wysi�kiem rzuca "
            + COMPOSITE_DEAD(tab, PL_NAR) + " w " + QIMIE(cel, PL_BIE),({cel,TP}));
    }

    rzut = throw_try_hit(co,v,cel);
    hid = losowa_hitlokacja(cel);

    if(hid == 666)
        return;

    if(rzut == 0)
    {
        write(". Spud�owa�" + TP->koncowka("e�","a�")  + ".\n");
        cel->catch_msg(". Na szcz�cie niezbyt celnie.\n");
        saybb(". Niecelnie.\n",({cel,TP}));

        przenies_przedmiot_na_lok(co,environment(cel));
    }
    else if(rzut == -1)
    {
        write(", on" + cel->koncowka("","a","o") + " jednak zd��y�" +
            cel->koncowka("","a","o") + " si� uchyli�.\n");
        cel->catch_msg(", ale zdo�a�" + cel->koncowka("e�","a�") +
            " si� uchyli�.\n");
        saybb(", on" + cel->koncowka("","a","o") + " jednak zd��y�" +
            cel->koncowka("","a","o") + " si� uchyli�.\n",({cel,TP}));

        przenies_przedmiot_na_lok(co,environment(cel));
    }
    else
    {
        hitloc = co_cel->query_hitloc(hid);

        write(". Trafi�" + TP->koncowka("e�","a�") +
            " w " + hitloc[2] + ". ");
        cel->catch_msg(". Dosta�" + cel->koncowka("e�","a�") +
            " w " + hitloc[2] + ". ");
        saybb(". Trafi�" + TP->koncowka("","a") +
            " w " + hitloc[2] + ". ",({cel,TP}));

            //troch� konsekwentno�ci. :P Vera
        if(cel->query_prop(NPC_M_NO_ACCEPT_GIVE) || cel->query_prop(PLAYER_I_NO_ACCEPT_GIVE))
            przenies_przedmiot_na_lok(co,environment(cel));
        else
        {
            if(co->query_prop(HEAP_I_IS) && co->num_heap()!=1)
                co->split_heap(1);//przy ka�dym MOVE to ma by�! V.
            co->move(cel,1);
            co->throw_effect(cel);//np. do jajka rozbijaj�cego si�
        }

            throw_do_dam(co,v,cel,hid);
    }

    /* dwa punkty zm�czenia za ka�dy 1 kg, minimum 1 */
    TP->add_old_fatigue(- max(1 + random(4), (co->query_prop(OBJ_I_WEIGHT)/500)));

    dodaj_expa_za_rzucanie(co, cel, rzut);
}

/**
 * Okre�la czy trafili�my nasz cel, czy te� mo�e spud�owali�my
 * albo zrobi� on unik. Trafienie zale�y od naszej zr�czno�ci i rzucania,
 * a unik od pr�dko�ci pocisku oraz zr�czno�ci, unik�w i spostrzegawczo�ci
 * celu. Jest te� modyfikator od wielko�ci celu.
 *
 * @param co  - obiekt kt�rym rzucamy
 * @param v - pr�dko�� obiektu kt�rym rzucamy
 * @param cel - cel w kt�ry rzucamy
 * @return -1 gdy cel wykona� unik
 * @return 0 gdy spud�owali�my
 * @return 1 gdy trafili�my
 *
**/
int
throw_try_hit(object co, int v, object cel)
{
    int zr, rzut, spc, objc, unikc, zrc; /* staty i skile rzucajacego i celu*/
    int hit;

    /* cechy rzucaj�cego maj�ce wp�yw na trafienie */
    zr = TP->query_stat(SS_DEX);
    rzut = TP->query_skill(SS_THROWING);

    /* cechy tego w kogo rzucamy */
    zrc = cel->query_stat(SS_DEX);
    spc = TP->query_skill(SS_AWARENESS);
    unikc = cel->query_skill(SS_DEFENSE);
    objc = cel->query_prop(OBJ_I_VOLUME);

    /* jak celnie rzucamy, jak hit >= 100 to zawsze trafimy */
    hit = zr + rzut + random(10);

    /* za��my, �e standardowo obj�to�� gracza to 50 litr�w
        je�li jest wi�ksza b�d� mniejsza dodajemy modyfikator do trafienia,
        1 punkt za ka�de 10 litr�w r�nicy  */
    hit += (objc/1000 - 50)/10;

    /* to teraz losujemy czy trafili�my */
    hit  -= random(100);

    /* jesli trafiamy to przeciwnik probuje uniku */
    if(hit > 0)
    {
        /* czy uda� si� unik (zr�czno��, uniki, spostrzegawczo��) */
        if(v * 2 > (zrc + unikc + spc)/2 + random(50))
            return 1;
        else
            return -1;
    }
    else
        return 0;
}

/**
 * Funkcja zwraca wylosowany numer hitlokacji (zale�ny od szans trafienia).
 * @param cel - obiekt kt�remu losujemy hitlokacj�
 * @return wylosowany numer hitlokacji
 * @return 666 (b��d) gdy obiekt nie ma zdefiniowanych hitlokacji
**/
int
losowa_hitlokacja(object cel)
{
    object co_cel = cel->query_combat_object();
    int *hitlocs, sum, i, rand;
    mixed *hitloc;

    rand = random(100);
    hitlocs = co_cel->query_hitloc_id();

    if(sizeof(hitlocs) == 0)
    {
        write(".\nZg�o� b��d w: " + cel->query_name + ". Nie ma zdefiniowanych hitlokacji!\n");
        saybb("\n",({TP}));
        return 666;
    }

    for(i=0,sum=0; sum<=rand; i++)
    {
        hitloc = co_cel->query_hitloc(hitlocs[i]);
        sum += hitloc[1];
    }

    return hitlocs[i-1];
}

/**
 * Przyznaje obra�enia trafionemu celowi, ewentualnie go przewraca.
 * Obra�enia zale�� liniowo od masy pocisku i kwadratowo od pr�dko�ci.
 * Przed przewr�ceniem broni test zr�czno�ci i akrobatyki.
 *
 * @param co  - obiekt kt�rym rzucamy
 * @param v - pr�dko�� obiektu kt�rym rzucamy
 * @param cel - cel w kt�ry rzucamy
 * @param hid - w kt�r� hitlokacj� trafili�my
**/
void
throw_do_dam(object co, int v, object cel, int hid)
{
    object co_cel = cel->query_combat_object();
    object armor, p;
    int dam,ac;
    int zrc, akrobc;

    int waga = co->query_prop(OBJ_I_WEIGHT);

    /*  balans by Vera: (z v/10 na v/20), co daje mniej wi�cej dwukrotnie
        mniej obra�e�, czyli:
        kilogramowy odwa�nik rzucony z pr�dko�ci� 10 zadaje 0 punkt�w obra�e�,
        a z pr�dko�ci� 100 zadaje 50 punkt�w obra�e� */
    dam = (waga * (v/10) * (v/20))/1000;

    //write("\nDEBUG INFO: pr�dko�� - " + v + ", obra�enia (bez zbroi) - " + max(0,dam) + ".");

    if (function_exists("create_object", co) == "/std/weapon")
    {
        if(co->query_dt() == W_IMPALE);
        else if(co->query_dt() == W_SLASH);

        dam += co->query_pen();
    }

    if(objectp(co_cel->cb_query_armour(hid)))
    {
        armor = co_cel->cb_query_armour(hid);
        ac = armor->query_ac(hid,W_IMPALE);

        cel->catch_msg(armor->query_nazwa(PL_MIA) + " zatrzyma�" + armor->koncowka("","a","o") + " pocisk. ");
        dam -= ac;
    }

    write("\n");
    cel->catch_msg("\n");
    saybb("\n",({cel, TP}));

    if(dam > 0)
    {
        TP->attack_object(cel);
        TP->add_prop(LIVE_O_LAST_KILL, cel);

        cel->reduce_hp(hid, dam * 100);

        cel->check_killed(TP);
    }

    /* teraz sprawdzamy przewr�cenie */
    /* cechy celu, mo�e pozwol� mu utrzyma� si� na nogach */
    zrc = cel->query_stat(SS_DEX);
    akrobc = cel->query_skill(SS_ACROBAT);

    if((waga/1000) * (v/10) > zrc + akrobc)
        przewroc(cel);
}

/**
 * Wywo�ywana aby przewr�ci� obiekt (kt�ry nie le�y ani nie siedzi).
 * @param cel - living kt�rego przewracamy
**/
void przewroc(object cel)
{
    object p;

    if(cel->query_prop("_przewrocony") != 0)
       return;

    if(cel->query_prop(SIT_SIEDZACY) || cel->query_prop(SIT_LEZACY))
       return;

    this_player()->catch_msg(QCIMIE(cel, PL_MIA) + " przewraca si�.\n");
    cel->catch_msg("Tracisz r�wnowag� i przewracasz si�.\n");
    saybb(QCIMIE(cel, PL_MIA) + " rozpaczliwie zamacha�" + cel->koncowka("","a","o")
        + " r�kami i przewr�ci�" + cel->koncowka("","a","o")
        + " si� jak sta�" + cel->koncowka("","a","o") + ".\n", ({cel, TP}));

    setuid();
    seteuid(getuid(this_object()));

    for(int i=0;i<sizeof(ENV(cel)->query_sit());i++)
    {
        if(pointerp(ENV(cel)->query_sit()[i]))
        {
            if(member_array("na ziemi",ENV(cel)->query_sit()[i]) != -1)
            {
                cel->add_prop(OBJ_I_DONT_GLANCE, 1);
                cel->remove_prop(SIT_SIEDZACY);
                cel->add_prop(SIT_LEZACY, ({ "na ziemi" }) );
                return;
            }
            else if(member_array("na bruku",ENV(cel)->query_sit()[i]) != -1)
            {
                cel->add_prop(OBJ_I_DONT_GLANCE, 1);
                cel->remove_prop(SIT_SIEDZACY);
                cel->add_prop(SIT_LEZACY, ({ "na bruku" }) );
                return;
            }
        }
        else if(ENV(cel)->query_sit()[i] == "na ziemi")
        {
            cel->add_prop(OBJ_I_DONT_GLANCE, 1);
            cel->remove_prop(SIT_SIEDZACY);
            cel->add_prop(SIT_LEZACY, ({ "na ziemi" }) );
            return;
        }
        else if(ENV(cel)->query_sit()[i] == "na bruku")
        {
            cel->add_prop(OBJ_I_DONT_GLANCE, 1);
            cel->remove_prop(SIT_SIEDZACY);
            cel->add_prop(SIT_LEZACY, ({ "na bruku" }) );
            return;
        }
    }

    //A je�li nic, to dajemy ten nieszcz�sny parali�
    p=clone_object("/std/paralize/lezenie.c");
    p->move(cel,1);
    cel->add_prop("_przewrocony", 1);

    cel->command("przestan atakowac wszystkich"); //ze wzgledu na upadek :P Vera
}

/**
 * Oblicza pr�dko�� z jak� poszybuje rzucony przez nas obiekt.
 * @param co - obiekt kt�rym rzucamy
 * @return predko�� (co najmniej 10, ale raczej mniej ni� 200)
**/
int
obj_speed(object co)
{
    int sila, rzut, moc; /* skile rzucajacego */
    float zmeczenie, kondycja; /* stopien zmeczenia i poranienia rzucajacego */
    int waga, obj; /* cechy rzucanego obiektu */
    float  gestosc;

    int v; /* predkosc kt�r� w�a�nie wyliczamy*/

    sila = TP->query_stat(SS_STR);
    rzut = TP->query_skill(SS_THROWING);

    kondycja = TP->query_hp() / TP->query_max_hp();
    zmeczenie = itof(TP->query_old_fatigue())/itof(TP->query_old_max_fatigue());

    /* zale�nie od zm�czenia i poranienia */
    if((kondycja += 0.5) > 1.0)
        kondycja = 1.0;
    if((zmeczenie += 0.5) > 1.0)
        zmeczenie = 1.0;

    /* takiego powera ma nasz gracz
        Balans, by Vera: dodane dzielenie skilla na 2 (on i tak powinien
        s�u�y� raczej precyzji, a nie pr�dko�ci rzutu!)*/
    moc = ftoi(itof( (sila + rzut / 2) / 2) * kondycja * zmeczenie);

    /* teraz przyjrzyjmy si� obiektowi kt�rym rzucamy */
    waga = co->query_prop(OBJ_I_WEIGHT);
    obj = co->query_prop(OBJ_I_VOLUME);

    //od tego mamy materia�y!! Vera.
    if(sizeof(co->query_materialy()))
        gestosc = GESTOSC_MATERIALU(co->query_materialy()[0]);
    else //je�li jaki� debil nie uwzgl�dni� materia��w, to leci stary wz�r
        gestosc = itof(waga)/itof(obj);

    /* dla zbalansowania */
    if(gestosc > 4.0)
        gestosc = 4.0;

    /* i rzucamy - por�wnujemy moc gracza z mas� obiektu,
        i modyfikujemy w zale�no�ci od g�sto�ci obiektu */
    v = moc - (waga/1000);
    v = ftoi(itof(v) * sqrt(gestosc));

    if(v < 10)
        v = 10;

    v += random(10);

    /* niekt�re przedmioty s� stworzone do rzucania */
    v = ftoi((itof(100 + co->query_prop(OBJ_I_AERODYNAMICZNOSC))/100.0) * itof(v));

    return v;
}

/**
 * Sprawdza czy gracz ma wolne r�ce.
 * Je�li przedmiot jest niewielki mo�na rzuci� jedn� r�k�.
**/
int
czy_wolne_rece(object co)
{
    if((co->query_wielded()) == TP)
    {
        return NF("Nie mo�esz rzuci� " + co->short(TP, PL_NAR) + " poniewa� masz " +
            co->koncowka("go", "j�", "go", "ich", "je") + " na sobie.\n");
    }
    else if((co->query_worn()) == TP)
    {
        return NF("Nie mo�esz rzuci� " + co->query_nazwa(PL_NAR) + ", kt�r" +
            co->koncowka("y", "�", "e", "ych", "e") + " dobywasz.\n");
    }
    else if(HAS_FREE_HANDS(TP))
        return 1;
    else if(((co->query_prop(OBJ_I_VOLUME))>1000 + 50*(TP->query_stat(SS_DEX))) && !HAS_FREE_HANDS(TP) )
    {
        notify_fail("�eby tym rzuci� musisz mie� wolne obie r�ce.\n");
        return 0;
    }
    else if(!HAS_FREE_HAND(TP))
    {
        notify_fail("Nie masz wolnych r�k.\n");
        return 0;
    }
    else
        return 1;
}

/**
 * Sprawdza czy gracz ma do�� si�y i nie jest do�� zm�czony by rzuca�.
 */
int
czy_da_rade(object co)
{
    if((TP->query_stat(SS_STR)) * 1000 < co->query_prop(OBJ_I_WEIGHT))
    {
        notify_fail("Nie jeste� wystarczaj�co siln" + TP->koncowka("y","a","e") + ".\n");
        return 0;
    }

    if(TP->query_old_fatigue() < 5)
    {
        notify_fail("Jeste� zbyt zm�czon" + TP->koncowka("y","a","e") + ".\n");
        return 0;
    }

    return 1;
}

/**
 * dodaje do�wiadczenie do r�nych cech i umiej�tno�ci rzucaj�cego
 * oraz rzucanego.
 */
/* Uwaga: to jest piewsza wersja, na pewno potrzebne b�dzie zbalansowanie,
        tym niemniej skonsultuj si� z Molderem zanim co� tu zmienisz */
void dodaj_expa_za_rzucanie(object co, object cel, int powodzenie_rzutu)
{
    /* dodajemy tyle si�y ile zabieramy zm�czenia za rzucenie */

    if (powodzenie_rzutu == 0)  /* spud�owali�my */
    {
        TP->increase_ss(SS_DEX, EXP_RZUCANIE_PRZY_SPUDLOWANIU_DEX);
        TP->increase_ss(SS_THROWING, EXP_RZUCANIE_PRZY_SPUDLOWANIU_THROWING);
    }
    else if (powodzenie_rzutu == -1) /* przeciwnik zrobi� unik */
    {
        TP->increase_ss(SS_DEX, EXP_RZUCANIE_PRZY_UNIKU_PRZECIWNIKA_DEX);
        TP->increase_ss(SS_THROWING, EXP_RZUCANIE_PRZY_UNIKU_PRZECIWNIKA_THROWING);

        if(co->is_weapon())
        {
            cel->increase_ss(SS_DEX, EXP_RZUCANIE_UNIK_PRZED_POCISKIEM_DEX);
            cel->increase_ss(SS_DEFENSE, EXP_RZUCANIE_UNIK_PRZED_POCISKIEM_DEFENSE);
            cel->increase_ss(SS_AWARENESS, EXP_RZUCANIE_UNIK_PRZED_POCISKIEM_AWARENESS);
        }
        else
        {
            cel->increase_ss(SS_DEX, EXP_RZUCANIE_UNIK_PRZED_NIEBRONIA_DEX);
            cel->increase_ss(SS_DEFENSE, EXP_RZUCANIE_UNIK_PRZED_NIEBRONIA_DEFENSE);
            cel->increase_ss(SS_AWARENESS, EXP_RZUCANIE_UNIK_PRZED_NIEBRONIA_AWARENESS);
        }
    }
    else  /* trafili�my */
    {
        if(co->is_weapon())
        {
            TP->increase_ss(SS_DEX, EXP_RZUCANIE_TRAFILISMY_DEX);
            TP->increase_ss(SS_THROWING, EXP_RZUCANIE_TRAFILISMY_THROWING);
        }
        else
        {
            TP->increase_ss(SS_DEX,EXP_RZUCANIE_NIEBRONIA_TRAFILISMY_DEX);
            TP->increase_ss(SS_THROWING,EXP_RZUCANIE_NIEBRONIA_TRAFILISMY_THROWING);
        }

        cel->increase_ss(SS_AWARENESS, EXP_RZUCANIE_DOSTALEM_AWARENESS);
    }
}
