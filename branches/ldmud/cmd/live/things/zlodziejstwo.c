/**
 * \file /cmd/live/things/thief.c
 *
 * This soul holds the thief commands for the mud.
 *
 * The soul contains the following commands:
 *
 * - backstab
 * - steal
 *
 * Przet�umaczy�: Vera
 *
 */

#pragma no_clone
#pragma no_inherit
#pragma strict_types

#include <pl.h>
#include <sit.h>
#include <exp.h>
#include <files.h>
#include <tasks.h>
#include <money.h>
#include <macros.h>
#include <cmdparse.h>
#include <formulas.h>
#include <ss_types.h>
#include <wa_types.h>
#include <composite.h>
#include <filter_funs.h>
#include <stdproperties.h>

// Define where to log steals, or undef to not log.
#undef LOG_STEALS "/d/Standard/log/STEAL"

// Prototypes
private int query_the_value(object ob);

/************************************************************************
 * Here follows the support functions for the commands below            *
 ************************************************************************/

/*
 * Function name: remove_extra_skill
 * Description:   This will remove the awareness bonus given to a
 *                player whom a thief has visited.
 * Arguments:     object who - Player to remove the bonus from
 * Returns:       void
 */
public void
remove_extra_skill(object who)
{
    int tmp;

    if (!objectp(who))
    {
        return;
    }

    if ((tmp = who->query_prop(LIVE_I_VICTIM_ADDED_AWARENESS)) < 2)
    {
        who->set_skill_extra(SS_AWARENESS,
            who->query_skill_extra(SS_AWARENESS) - F_AWARENESS_BONUS);
        who->remove_prop(LIVE_I_VICTIM_ADDED_AWARENESS);
    }
    else
    {
        who->add_prop(LIVE_I_VICTIM_ADDED_AWARENESS, --tmp);
    }
}

/*
 * Function name: query_internal_value
 * Description:   This is used to determine the combined value of
 *                all the objects inside a container.
 * Arguments:     object ob - The container whos inventory we are checking
 * Returns:       int - The combined value of all objects inside ob
 */
private int
query_internal_value(object ob)
{
    object *inv = deep_inventory(ob);
    int sum = 0;
    int index = 0;

    if (!pointerp(inv) || !(index = sizeof(inv)))
    {
        return 0;
    }

    while( index-- )
    {
        if (!objectp(inv[index]))
            continue;

        sum += query_the_value(inv[index]);
    }

    return sum;
}

/*
 * Function name: query_the_value
 * Description:   This is used to determine the value of
 *                an object.
 * Arguments:     object ob - the object whos value we are checking
 * Returns:       int - ob's value
 */
private int
query_the_value(object ob)
{
    if (!objectp(ob))
    {
        return 0;
    }

    if (IS_HEAP_OBJECT(ob))
    {
        return ob->heap_value();
    }

    if (IS_HERB_OBJECT(ob))
    {
        return ob->query_herb_value();
    }

    if (IS_POTION_OBJECT(ob))
    {
        return ob->query_potion_value();
    }

    if (IS_CONTAINER_OBJECT(ob))
    {
        return (ob->query_value() + ob->query_internal_value(ob));
    }

    return ob->query_value();
}

/*
 * Function name: check_watchers_see_steal
 * Description:   Determines whether or not onlookers can see a thief
 *                (attemp to) steal from his victim.  Also handles the
 *                printing of messages if this is the case.
 * Arguments:
 *                object victim - the person we are stealing from
 *                object thief - the person committing the crime
 *                int chance - the thief's success value
 *                object *items - the items being stolen
 * Returns:       void
 * Note:          This _only_ gets called for successful thefts, don't
 *                  need to waste the processing otherwise.
 *                  Also: Wizards see all steals automatically in the room.
 */
private void
check_watchers_see_steal(object victim, object thief,
                         int chance, object *items)
{
    mixed  tmp;
    object *whom;
    object *wiz;
    object *watchers;
    int index;
    int notice;

    /* Sanity check! */
    if (!objectp(thief))
        return;

    whom  = FILTER_LIVE(all_inventory(environment(thief)));
    whom -= ({ 0, thief, victim });

    /* Filter any present wizards out */
    wiz   = FILTER_IS_WIZARD(whom);
    whom -= wiz;
    index = sizeof(whom);

    while(index--)
    {
        /* If we can't see, we can't notice */
        if (!CAN_SEE_IN_ROOM(whom[index]) ||
            !CAN_SEE(whom[index], thief)  ||
            !CAN_SEE(whom[index], victim) )
        {
            continue;
        }

        /* Some quick checks to see if we spotted it. */
        notice  = (12 * (whom[index]->query_skill(SS_AWARENESS) / 2));
        tmp     = ((600 * whom[index]->query_intoxicated()) /
                   whom[index]->intoxicated_max());
        notice -= MAX(tmp, 0);
        tmp     = ((200 * whom[index]->query_old_fatigue()) /
                   whom[index]->query_old_max_fatigue());
        notice -= MAX(tmp, 0);
        notice -= (objectp(whom[index]->query_attack()) ? 600 : 0);

        /* We didn't spot the theft. */
        if (notice < chance)
        {
            continue;
        }

        /* Generate the watcher message */
        tmp = "Dostrzegasz jak sprawne ruchy " + QIMIE(thief,PL_DOP) +
                " szybko pozbawiaj� " +
                QIMIE(victim,PL_DOP) + " czego�, co do niego nale�a�o.\n";

        whom[index]->catch_msg(tmp);

        whom[index]->increase_ss(SS_AWARENESS,EXP_KRADZIEZ_AWARENESS);
        whom[index]->increase_ss(SS_INT,EXP_KRADZIEZ_INT);

        /* Can we tell if we got spotted? */
        if (CAN_SEE(thief, whom[index]) && thief->resolve_task(
                TASK_DIFFICULT, ({ SS_AWARENESS }), whom[index],
                ({ TS_DIS })))
        {
            if (!pointerp(watchers))
            {
                watchers = ({ whom[index] });
            }
            else
            {
                watchers += ({ whom[index] });
            }
        }

        /* An extra hook to call, for a guild perhaps? */
        //whom[index]->hook_onlooker_stolen_object(thief, victim);

        /* Does the onlooker attack thieves? *
        if (whom[index]->query_prop(LIVE_I_ATTACK_THIEF))
        {
            if (CAN_SEE(whom[index], thief))
            {
                whom[index]->command("$say to " +
                    OB_NAME(thief) + " Stop it, you thief!");

                /* Do not switch targets in combat *
                if (!objectp(whom[index]->query_attack()))
                {
                    whom[index]->command("$kill " + OB_NAME(thief));
                }
            }
            else
            {
                whom[index]->command("$say Stop it, you thief!");
            }
        }*/
        //Zamiast tego piece of shit, dajemy signal:
        whom[index]->signal_kradziez(TP,items,victim);
    }

    if (tmp = sizeof(watchers))
    {
        /* Uh oh.. we got spotted, print the bad news.. */
        thief->catch_msg("Zdaje si�, �e " +
	    FO_COMPOSITE_LIVE(watchers, thief, PL_MIA) + " zauwa�y" +
            ((tmp > 1) ? "" : watchers[0]->koncowka("�", "�a", "�o")) +
            " kradzie�!\n");
    }

    if (sizeof(wiz))
    {
        /* We have a wizard audience, let them know what transpired. */
        wiz->catch_msg("Jako nieomylny czarodziej dostrzegasz " +
            QIMIE(thief,PL_BIE) + " kradn�cego " +
            COMPOSITE_DEAD(items,PL_BIE) + " " +QIMIE(victim,PL_CEL)+ ".\n");
    }
}


/*************************************************************************
 * Backstab  -  Nail 'em in the back I say
 */
private void
perform_backstab(string str, object whom)
{
    mixed tmp;
    object ob;
    object weapon;
    string one;
    string two;
    int stat;
    int skill;
    int aware;

    if (!objectp(whom))
    {
        return;
    }

    set_this_player(whom);

    this_player()->remove_prop(LIVE_I_BACKSTABBING);

    if (!strlen(str) || sscanf(str, "%s with %s", one, two) != 2)
    {
        write("Backstab whom with what?\n");
        return;
    }

    if (interactive(this_player()) &&
        (this_player()->query_skill(SS_BACKSTAB) < F_BACKSTAB_MIN_SKILL))
    {
        write("You lack the training for such an " +
            "endeavour, you would get caught for sure.\n");
        return;
    }

    if (!this_player()->query_prop(OBJ_I_HIDE))
    {
        write("You must be hidden in order to backstab someone.\n");
        return;
    }

    if (!CAN_SEE_IN_ROOM(this_player()))
    {
        if (tmp = environment(this_player())->query_prop(ROOM_S_DARK_MSG))
        {
            write(tmp + " backstab anyone here.\n");
            return;
        }
        write("You cannot see enough to backstab anyone here.\n");
        return;
    }

    if (this_player()->query_ghost())
    {
        write("Umm yes, killed. That's what you are.\n");
        return;
    }

    one = lower_case(one);
    if (!sizeof(tmp = parse_this(one, "[the] %l")))
    {
        write("You find no such living creature.\n");
        return;
    }

    if (sizeof(tmp) > 1)
    {
        write("Be specific, you cannot backstab " +
            COMPOSITE_LIVE(tmp, PL_DOP) + " at the same time.\n");
        return;
    }

    ob = tmp[0];
    if (!living(ob))
    {
        write(capitalize(LANG_THESHORT(ob)) + " isn't alive!\n");
        return;
    }

    if (ob->query_ghost())
    {
        write(ob->query_Imie(this_player()) + " is already dead!\n");
        return;
    }

    if (objectp(this_player()->query_attack()))
    {
        write("You cannot backstab while already fighting!");
        return;
    }

    if (tmp = environment(this_player())->query_prop(ROOM_M_NO_ATTACK))
    {
        write(stringp(tmp) ? tmp :
            "You sense a divine force preventing your attack.\n");
        return;
    }

    if (tmp = ob->query_prop(OBJ_M_NO_ATTACK))
    {
        if (stringp(tmp))
        {
            write(tmp);
        }
        else
        {
            write("You feel a divine force protecting this being, your " +
                "attack fails.\n");
        }

        return;
    }

    if (!sizeof(tmp = this_player()->query_weapon(-1)) ||
        !sizeof(tmp = FIND_STR_IN_ARR(two, tmp, PL_NAR)))
    {
        write("You need to be wielding a knife or dagger to " +
            "backstab with.\n");
        return;
    }

    if (sizeof(tmp) > 1)
    {
        write("Be specific, you cannot backstab with " +
            COMPOSITE_DEAD(tmp, PL_NAR) + " at the same time.\n");
        return;
    }

    weapon = tmp[0];
    if (weapon->query_wt() != W_KNIFE)
    {
        write("You need a knife or dagger to backstab with.\n");
        return;
    }

    if (weapon->query_wielded() != this_player())
    {
        write("You must be wielding the " + weapon->short() +
            "in order to backstab with it.\n");
        return;
    }

    if (this_player()->query_old_fatigue() < F_BACKSTAB_FATIGUE)
    {
        write("You are too tired to attempt a backstab " +
            "at this time.\n");
        return;
    }


    if ((this_player()->query_npc() == 0) &&
         this_player()->query_met(ob) &&
        (this_player()->query_prop(LIVE_O_LAST_KILL) != ob))
    {
        this_player()->add_prop(LIVE_O_LAST_KILL, ob);
        /* Only ask if the player did not use the real name of the target. */
        if (one != ob->query_real_name())
        {
            write("Backstab " + ob->query_the_name(this_player()) +
                "?!? Please confirm by typing again.\n");
            return;
        }
    }

    skill = this_player()->query_skill(SS_BACKSTAB);
    aware = ob->query_skill(SS_AWARENESS) / 2;

    if (ob->query_npc() && !aware)
    {
        tmp   = (2 * ob->query_stat(SS_INT)) / 3;
        aware = (tmp > 100 ? 100 : tmp);
    }

    if (tmp = ob->query_prop(LIVE_I_GOT_BACKSTABBED))
    {
        aware += (tmp * tmp * 2);
    }

    stat = MAX(120, this_player()->query_stat(SS_DEX));
    stat = ((stat + this_player()->query_skill(SS_WEP_KNIFE)) / 2);
    tmp  = F_BACKSTAB_HIT(skill, stat, aware, ob->query_skill(SS_DEFENCE));

    if (tmp < 1)
    {
        this_player()->add_old_fatigue(-(F_BACKSTAB_FATIGUE / 2));
        this_player()->reveal_me(1);
        this_player()->attack_object(ob);

        write("You lunge at " + ob->query_the_name(this_player()) +
            " with your " + weapon->short() + ", but miss and " +
            "reveal your intent!\n");
        ob->tell_watcher(QCTNAME(this_player()) + " lunges at " + QTNAME(ob) +
            " from behind, but misses.\n", this_player());
        tell_object(ob, this_player()->query_Art_name(ob) +
            " lunges at you from behind, but misses.\n");
    }
    else
    {
        stat = this_player()->query_stat(SS_STR);
        stat = ((stat > 120) ? 120 : stat);
        tmp = F_BACKSTAB_PEN( skill, this_player()->query_skill(SS_WEP_KNIFE),
            weapon->query_pen(), stat );
        tmp = ob->hit_me(tmp, W_IMPALE, this_player(), -1, A_BACK);
        this_player()->add_old_fatigue(-F_BACKSTAB_FATIGUE);

        switch(tmp[0])
        {
        case 70..98:
            one = "massacring you";
            two = "massacring " + ob->query_objective();
            break;
        case 45..69:
            one = "hurting you very badly";
            two = "hurting " + ob->query_objective() + " very badly";
            break;
        case 25..44:
            one = "hurting you rather badly";
            two = "hurting " + ob->query_objective() + " rather badly";
            break;
        case 10..24:
            one = "hurting you";
            two = "hurting " + ob->query_objective();
            break;
        case 5..9:
            one = "slighty hurting you";
            two = "slightly hurting " + ob->query_objective();
            break;
        case 1..4:
            one = "grazing you";
            two = "grazing " + ob->query_objective();
            break;
        case -1..0:
            one = "without harming you";
            two = "without harming " + ob->query_objective();
            break;
        default:
            one = "assassinating you";
            two = "assassinating " + ob->query_objective();
            break;
        }

        write("You sneak up on " + ob->query_the_name(this_player()) +
            " and " + (tmp[0] < 1 ? "try to " : "") +
            "drive your " + weapon->short() + " into " +
            ob->query_possessive() + " back, " + two + ".\n");
        ob->tell_watcher( QCTNAME(ob) + " cries out " +
            (tmp[0] > 20 ? "in pain " : "") + "as " +
            QTNAME(this_player()) + " " + (tmp[0] < 1 ?
              "tries to stab " : "stabs ") + ob->query_objective() +
            " in the " + tmp[1] + " from behind.\n", this_player() );
        tell_object(ob, this_player()->query_Imie(ob) + " " +
            (tmp[0] < 1 ? "tries to stab " : "stabs ") +
            "your " + tmp[1] + " from behind, " + one + ".\n");

        this_player()->reveal_me(1);
    }

    weapon->did_hit(1, tmp[1], tmp[0], ob, W_IMPALE, tmp[2], tmp[3]);

    if(ob->query_hp() <= 0.0)
    {
        ob->do_die(this_player());
    }
    else
    {
        if ((tmp = ob->query_prop(LIVE_I_GOT_BACKSTABBED)) < 5)
        {
            ob->add_prop(LIVE_I_GOT_BACKSTABBED, tmp + 1);
        }
    }
    return;
}

public int
backstab(string str)
{
    return 0; // komenda ta zosta�a wy��czona
    if (this_player()->query_prop(LIVE_I_BACKSTABBING))
    {
        write("You are already preparing for a backstab, be patient.\n");
        return 1;
    }

    write("You prepare to perform a backstab...\n");

    set_alarm( (2.0 + (rnd() * 2.0)), 0.0,
        &perform_backstab(str, this_player()));

    return 1;
}

/**
 * Kradzie� przerobi� i dostosowa� do Vatt'gherna - Verek.
 */
public int
ukradnij(string str)
{
    mixed tmp;
    object item;
    object victim;
    int chance;
    int notice;
    int result;
    int success;
    int caught;
    int *moveresult;
    int xp;
    string str1;
    string str2;
    string str3;
#ifdef LOG_STEALS
    int *log = ({ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 });
#endif LOG_STEALS
    string log10 = ""; //file_names skradzionych
    object *komu,*co; //do parse'a
    string strx, str_z_czego; //to tez
    object *inne_livingi; //<=spostrzegawcze inne livingi sie zczaj� ;)

    notify_fail(capitalize(query_verb()) + " komu co ?\n");

    if (!str)
        return 0;

    if (!parse_command(lower_case(str), ENV(TP),
         "%l:" + PL_CEL + " %s", komu,strx))
        return 0;

    komu = NORMAL_ACCESS(komu, 0, 0);
    if(!sizeof(komu) || !objectp(komu[0]))
        return 0;

    //to sie nie powinno zdazyc po parsie, ale just in case...
    if (komu[0] == this_player())
    {
        write("Siebie chcesz okra��? Ciekawe...\n");
        return 1;
    }

    if (sizeof(komu) > 1)
    {
        notify_fail("Mo�esz kra�� ekwipunek tylko jednej osobie naraz.\n");
        return 0;
    }

    if(!parse_command(strx, all_inventory(komu[0]),
          "%i:" + PL_BIE, co))
        return 0;

    if(strx ~= "wszystko")
        co = all_inventory(komu[0]);
    else
        co=CONTAINER_ACCESS(co,komu[0]);

    if(!sizeof(co))
        return 0;

    //a to daje dlatego, ze access nie wywala obiektow pasujacych do nazwy
    //lecz bedacych w naszym inventory :)
    //co = filter(co, &operator(!=)(-1, ) @ &member_array(,all_inventory(komu[0])));

      /*if(!objectp(co[0])) to sprawdzanie jest pozniej, na wypadek pomylki gracza.
          return 0;*/

    inne_livingi = FILTER_OTHER_LIVE( all_inventory(ENV(TP)) - ({komu[0]}) );

    /* It's a bit hard to go stealing things in the midst of combat. */
    if (objectp(this_player()->query_attack()))
    {
        write("Jeste� teraz zbyt zaj�t" + TP->koncowka("y","a","e") + " walk�!\n");
        return 1;
    }

    if(TP->query_mana() < F_UKRADNIJ_MANA_COST + 1)
    {
        notify_fail("Jeste� zbyt wyczerpan" + TP->koncowka("y", "a", "e") + " mentalnie.\n");
        return 0;
    }

    if(TP->query_mana() < F_UKRADNIJ_FATIGUE_COST + 1)
    {
        notify_fail("Jeste� zbyt zm�czon" + TP->koncowka("y", "a", "e") + ".\n");
        return 0;
    }

    if((TP->query_prop(SIT_SIEDZACY) && !komu[0]->query_prop(SIT_SIEDZACY)) ||
        (TP->query_prop(SIT_LEZACY)  && !komu[0]->query_prop(SIT_LEZACY)))
    {
        write("Musisz najpierw wsta�.\n");
        return 1;
    }

    /* If we cannot see, however are we going to steal? */
    if (!CAN_SEE_IN_ROOM(this_player()))
    {
        tmp = environment(this_player());

        if (str = tmp->query_prop(ROOM_S_DARK_MSG))
            write(str);
        else
            write("Jest tu zbyt ciemno!\n");

        return 1;
    }

    /*czarodziejom nie mo�na niczego ukra�c, co by unikn�� przypa�u,
        bo r�ne rzeczy mog� mie� wizowie nie dla graczy :P */
    /*if (komu[0]->query_wiz_level()) FIXME
    {
        komu[0]->catch_msg("Przy�apujesz " +
                QIMIE(TP,PL_BIE) + " pr�buj�c" +TP->koncowka("ego","�","e")+
                " ukra�� tw"+co[0]->koncowka("�j","oj�","oje") +" "+co[0]->short(PL_BIE) +
                "! (czarodziejom nie da si� niczego ukra��)\n");
        TP->catch_msg("Ups! Zdaje si�, �e " + QIMIE(komu[0],PL_MIA) +
                " przy�apa�"+komu[0]->koncowka("","a","o")+" ci� na kradzie�y!\n");
        return 1;
    }*/

    if (komu[0]->query_linkdead())
    {
        write("Hmmm... Zdaje si�, �e twoja ofiara lewituje gdzie� w czasoprzestrzeni.\n");
        return 1;
    }

    /* We wanna try to steal from a container inside someone/something */
#if 0
    if (str3)
    {
        //tmp = FIND_STR_IN_OBJECT(str3, place, PL_BIE);
        if (!sizeof(tmp))
        {
            TP->catch_msg("You don't see any " + str3 + " " + (victim ?
                "being carried by "+ victim->query_the_name(this_player()) :
                "inside " + place->short()) + " to " + query_verb() +
                " from.\n");
            return 1;
        }

        /* Suspect found matching the description... */
        place = tmp[0];

        /* Umm.. hello?  Try picking a container sometime... */
        if (!IS_CONTAINER_OBJECT(place))
        {
            TP->catch_msg("You cannot " + query_verb() + " from " +
                place->short() + ".\n");
            return 1;
        }

        /* Okay.. better.. try one that is open next time. */
        if (place->query_prop(CONT_I_CLOSED))
        {
            TP->catch_msg("The " + place->short() + " appears to be closed, " +
                "better look for something else to " + query_verb() +
                " from.\n");
            return 1;
        }
    }
#endif

    if(!objectp(co[0]))
    {
        TP->catch_msg("Przy "+QIMIE(komu[0],PL_MIE)+" nie znajdujesz niczego takiego.\n");

         /* Dajmy ofiarze szans� na zauwa�enie tej pr�by.... */
        tmp = komu[0]->resolve_task(TASK_DIFFICULT,
            ({ SS_AWARENESS, TS_DEX }), this_player(),
            ({ SS_PICK_POCKET, TS_DEX }));

         if (tmp > 0)
         {
            komu[0]->catch_msg("Przy�apujesz " + QIMIE(TP,PL_BIE) +
                " na podgl�daniu twojego ekwipunku!\n");
         }
         return 1;
    }

    /*if ((sizeof(co) > 1))
    {
        notify_fail("Mo�esz kra�� tylko jedn� rzecz naraz.\n");
        return 0;
    }*/

/*if(sizeof(co) >1)
{//dump_array(co);
foreach(object x : co - ({co[0]}))
{//write(komu[0]->short(PL_CEL)+" "+x->short(PL_BIE)+"\n");
    steal(komu[0]->short(PL_CEL)+" "+x->short(PL_BIE));
}
}
    /* Nie mo�emy ukra�� noszonych / dobytych rzeczy */
    tmp = co->query_wielded() + co->query_worn() + co->query_held();
    tmp -= ({0});
    //if((co->query_wielded() || co[0]->query_worn() || co[0]->query_held()))
    if(sizeof(tmp))
    {
        write("Nie mo�esz ukra�� rzeczy, kt�re s� noszone lub trzymane "+
            "przez t� osob�.\n");

        komu[0]->catch_msg(QCIMIE(this_player(),PL_MIA) +
            " pr�bowa� ukra�� tw"+co[0]->koncowka("�j","oj�","oje")+" "+
            COMPOSITE_DEAD(co,PL_BIE) + "!\n");

        return 1;
    }
//write("szansa: 1 "+chance+"\n");
    /* Setup the thieves base chances.. */
    tmp  = this_player()->query_skill(SS_SNEAK);
    tmp += this_player()->query_skill(SS_AWARENESS);
    chance += ((tmp / 2) + random(tmp /  2));
    chance += (6 * this_player()->query_skill(SS_PICK_POCKET));
    chance += (2 * MIN(this_player()->query_stat(SS_DEX), 120));
//write("szansa: 2 "+chance+"\n");
#ifdef LOG_STEALS
    log[0] = chance;
#endif LOG_STEALS

    /* If we have stolen within the last 10 minutes, lower our chances */
    tmp = this_player()->query_prop(LIVE_I_LAST_STEAL);
    if ((tmp = (300 + (tmp - time()))) > 0)
        chance -= tmp;

//write("szansa: 3 "+chance+"\n");
    /* Reset the last attempt value. */
    tmp = (time() + ((tmp > 0) ? ((tmp > 1200) ? 600 : (tmp / 2)) : 0));
    this_player()->add_prop(LIVE_I_LAST_STEAL, tmp);

    /* We may get a bonus for some reason. (guild?) */
    //chance += this_player()->hook_thief_steal_bonus(komu[0], place, item);

    /* We get nervous if there is a large audience */
    tmp = inne_livingi + ({komu});
    tmp = filter(tmp, &not() @ &->query_wiz_level());
    if ((tmp = sizeof(tmp) > 4))
    {
        tmp *= 50;
        chance -= MIN(tmp, 200);
    }
//write("szansa: 4 "+chance+"\n");
    /* We aren't stealing from a live victim, so we are more confident. */
    //chance += (objectp(komu[0]) ? 0 : 300);

    /* Drunkeness leads to sloppiness. */
    tmp = ((200 * this_player()->query_intoxicated()) /
           this_player()->intoxicated_max());
    chance -= MAX(tmp, 0);
//write("szansa: 5 "+chance+"\n");
    /* The more tired.. the less nimble the fingers. */
    tmp = ((100 * this_player()->query_old_fatigue()) /
           this_player()->query_old_max_fatigue());
    chance -= MAX(tmp, 0);
//write("szansa: 6 "+chance+"\n");
    /* Victim is underattack and blind, flailing, hard to steal from. */
    if (komu[0]->query_attack() && !CAN_SEE_IN_ROOM(komu[0]))
        chance /= 2;
//write("szansa: 7 "+chance+"\n");
    /* Modify our chances based on the object itself. */
    foreach(object x: co)
    {
        if (IS_HEAP_OBJECT(x))
        {
            chance -= (x->heap_light() * 300);
            chance -= (x->heap_weight() / 60);
            chance -= (x->heap_volume() / 60);
        }
        else
        {
            chance -= (x->query_prop(OBJ_I_LIGHT) * 300);
            chance -= (x->query_weight() / 60);
            chance -= (x->query_volume() / 60);
        }
    }
//write("szansa: 8 "+chance+"\n");
    /* Smaller players value their items more and are thus more wary */
    tmp = 0;
    foreach(object x: co)
        tmp += (query_the_value(x) / (komu[0]->query_npc() ? 100 :
        komu[0]->query_average_stat()));
//write(tmp+"\n");
    chance -= MAX(tmp, 0);
//write("szansa: 9 "+chance+"\n");

#ifdef LOG_STEALS
    log[1] = chance;
    log[2] = tmp;
#endif LOG_STEALS

    /* We're stealing from someone, not an easy task. */
    //if (objectp(victim))
    //{

    /* Int gives us a basic perception value. */
    tmp = (2 * komu[0]->query_stat(SS_INT));
    tmp = MIN(tmp, 240);
    notice += (2 * ((tmp / 2) + random(tmp)));

    /* And this gives us our basic chance to notice. */
    notice += (5 * komu[0]->query_skill(SS_AWARENESS));

#ifdef LOG_STEALS
    log[3] = notice;
#endif LOG_STEALS

    /* We give newbies a better chance to notice artificially. */
    if (!komu[0]->query_npc() && (komu[0]->query_average_stat() < 35))
    {
        tmp = (5 * (40 - komu[0]->query_average_stat()));
        notice += MAX(tmp, 0);
    }

    /* We might have a bonus to notice for some reason. (guild?) */
    //notice += victim->hook_victim_no_steal_bonus(item);

    /* We are stealing from a carried object, even easier to spot. *
    tmp = (victim != place ? 10 : 0);
    notice += tmp;
    chance -= tmp;*/

    /* Drunk victims make less aware victims. */
    tmp = ((200 * komu[0]->query_intoxicated()) /
            komu[0]->intoxicated_max());
    notice -= MAX(tmp, 0);

    /* If your tired.. less chance you'll notice the small things */
    tmp = ((100 * komu[0]->query_old_fatigue()) /
            komu[0]->query_old_max_fatigue());
    notice -= MAX(tmp, 0);

    /* We can't see.. that drops our chances noticebly */
    if (!CAN_SEE_IN_ROOM(komu[0]))
    {
        notice /= 2;
    }
    //}

#ifdef LOG_STEALS
    log[4] = notice;
#endif LOG_STEALS

    /* Can't have a negative chance.. that would be bad. */
    if (chance < 1)
    {
        chance = 1;
    }
    else
    {
        /* Mix it up and randomize the chance a bit */
        chance = ((chance / 2) + random(chance / 4) + random(chance / 4));
    }

    /* Can't have a negative notice value.. that would also be bad */
    if (notice < 1)
    {
        notice = 1;
    }
    else
    {
        /* Randomize the chance to notice.. */
        notice = ((notice / 2) + random(notice / 4) + random(notice / 4));
    }

#ifdef LOG_STEALS
    log[5] = chance;
    log[6] = notice;
#endif LOG_STEALS

    if ((result = (chance - notice)) < 1)
    {
        /* We botched it, but did we get caught? */
        if (this_player()->resolve_task(TASK_DIFFICULT,
                 ({ TS_DEX, SS_PICK_POCKET }), komu[0],
                ({ TS_DEX, SS_AWARENESS })) > 0)
        {
            success = 0;
            caught  = 0;
        }
        else
        {
            success = 0;
            caught  = 1;
        }
    }
    else if ((result = ((random(result) + result) -
                (random(1000) + random(1500)))) < 1)
    {
        /* We did it, but there is a chance we got spotted */
        if (this_player()->resolve_task(TASK_ROUTINE,
                 ({ TS_DEX, SS_PICK_POCKET }), komu[0],
                ({ TS_DEX, SS_AWARENESS })) > 0)
        {
            success = 1;
            caught  = 0;
        }
        else
        {
            success = 1;
            caught  = 1;
        }
    }
    else
    {
        /* We got off scott free */
        success = 1;
        caught  = 0;
    }

#ifdef LOG_STEALS
    log[7] = result;
#endif LOG_STEALS

    /* This should not happen.. But just in case.. */
    //if (!objectp(co[0]))
    if(!sizeof(filter(co, &operator(!=)(0) @ objectp)))
    {
        TP->catch_msg(QCIMIE(komu[0],PL_MIA)+" nie posiada niczego takiego.\n");

        if (caught)
            komu[0]->catch_msg(QCIMIE(this_player(),PL_MIA) +
            " pr�bowa� ci co� ukra��!!\n");

        return 1;
    }

    /* Lets make sure the item isn't protected against thievery..*/
    tmp = filter(co,&->query_prop(OBJ_M_NO_STEAL));
    if (/* !MAX(moveresult,0) &&na chuj?*/ sizeof(tmp))
    {
        if (stringp(tmp[0]))
            write(tmp[0]);
        else
            write("Nie mo�esz tego ukra��.\n");

        if (caught)
        {
            komu[0]->catch_msg("Przy�apujesz " +
                QCIMIE(TP,PL_BIE) + " pr�buj�c" +TP->koncowka("ego","�","e")+
                " ukra�� tw"+co[0]->koncowka("�j","oj�","oje") +" "+co[0]->short(PL_BIE) +
                "!\n");
            komu[0]->increase_ss(SS_AWARENESS,EXP_KRADZIONY_WIDZE_AWARENESS);
            komu[0]->increase_ss(SS_INT,EXP_KRADZIONY_WIDZE_INT);
        }
        return 1;
    }

    /* We succeeded, hurray! */
    if (success)
    {
        /* Lets try to move the item shall we? */
        moveresult = co->move(this_player());

#ifdef LOG_STEALS
        //log[8]="";
        foreach(int x : moveresult)
            log[8] += x; //FIXME
#endif LOG_STEALS



        if (!sizeof(moveresult -= ({0})))
        {
            /* A stolen item is no longer hidden. */
            co->remove_prop(OBJ_I_HIDE);

            /* A couple hook checks, for a guild perhaps.. */
            //this_player()->hook_thief_stolen_object(item, victim, place);
            //victim->hook_victim_stolen_object(item, this_player(), place);

            /* If you didn't get caught, you get some general exp. */
            if (!caught)
            {
                /* Only give experience when there are onlookers. The victim
                 * counts as an onlooker in this.
                 */
                if (sizeof(inne_livingi+({komu[0]})))
                {
                    result = ABS(result);
                    xp = F_STEAL_EXP(result);
                }
                /* je�li kradli�my ju� tej osobie przed chwil�, to mniej expa
                    dostajemy */
                tmp = komu[0]->query_prop(LIVE_AO_THIEF);
                if (pointerp(tmp))
                {
                    /* We did, reduce the exp. */
                    if (member_array(this_player(), tmp) != -1)
                    {
                        TP->increase_ss(SS_DEX,EXP_KRADNE_UDANE_DEX/3);
                        TP->increase_ss(SS_PICK_POCKET,EXP_KRADNE_UDANE_PICKPOCKET/3);
                    }
                    else
                    {
                        tmp += ({ this_player() });
                    }
                }
                else
                {
                    tmp = ({ this_player() });
                }

                komu[0]->add_prop(LIVE_AO_THIEF, tmp);

                /* Sanity check */
                //if (xp > 0)
                //{
#ifdef LOG_STEALS
                    log[9] = xp;
#endif LOG_STEALS
                    if(xp > 15)
                    {
                        TP->increase_ss(SS_DEX,EXP_KRADNE_UDANE_DEX);
                        TP->increase_ss(SS_PICK_POCKET,EXP_KRADNE_UDANE_PICKPOCKET);
                    }
                    else
                    {
                        TP->increase_ss(SS_DEX,EXP_KRADNE_UDANE_DEX-5);
                        TP->increase_ss(SS_PICK_POCKET,EXP_KRADNE_UDANE_PICKPOCKET-5);
                    }

                    //to log�w. STEAL_EXP_LOG i do ostatecznego loga tez
                    foreach(object x : co)
                        log10 += file_name(x)+"  ";
#ifdef STEAL_EXP_LOG

                    log_file(STEAL_EXP_LOG, sprintf(
                        "%12s %11s: %5d %s %s (%s)\n",
                        ctime(time())[4..15],
                        this_player()->query_real_name(), xp,
                        log10, interactive(komu[0]) ? komu[0]->query_real_name :
                        file_name(komu[0]) , file_name(komu[0])));
#endif STEAL_EXP_LOG
                //}
            }

            TP->catch_msg("Uda�o ci si� ukra�� " +QIMIE(komu[0],PL_CEL) +
                 " "+COMPOSITE_DEAD(co, PL_BIE)+ ".\n");
        }
        else //if (moveresult > 0)
        {
            success = 0;

            for(int xx = 0 ; xx < sizeof(co) ; xx++)
            {
                tmp = "Nie mo�esz ukra�� " + QIMIE(komu[0],PL_CEL) +
                    " " +COMPOSITE_DEAD(co[xx],PL_DOP) + ", poniewa� ";
                switch(moveresult[xx])
                {
                case 9: /* This should not happen, but just in case. */
                    tmp += " wyst�pi� b��d nr. 0x34w3. Zg�o� b��d!!!!\n";
                    break;
                case 1: case 4: case 5: case 8: case 10:
                    tmp +="tego nie ud�wigniesz!\n";
                    break;
                case 2: case 3: case 6:
                    tmp += "ten przedmiot jest zabezpieczony!\n";
                    break;
                default: /* This should not happen either. */
                    tmp += "wyst�puje jaki� egzotyczny b��d. nr. 0x4d3x3. " +
                        "Zg�o� to natychmiast!\n";
                    break;
                }
                TP->catch_msg(tmp);
            }
        }
        /*else  /* This should not happen either *
        {
            success = 0;
            TP->catch_msg("Nie mo�esz ukra�� " + QIMIE(komu[0],PL_CEL) +
                " " +co[0]->short(PL_DOP) + ", poniewa� wyst�pi� b��d o "+
                "numerze 0x42pp3. Zg�o� b��d!!!\n");
        }*/
    }
    else /* We failed. */
    {
        TP->catch_msg("Nie uda�o ci si� ukra�� "+QCIMIE(komu[0],PL_CEL) +
                " "+COMPOSITE_DEAD(co,PL_DOP) +"."+
        (sizeof(co)>1 ? (" Mo�e posz�oby ci lepiej, gdyby� nie by�"+
                    TP->koncowka("","a","")+" tak pazern"+
                    TP->koncowka("y","a","e")+".") : "") +"\n");
    }

    /* We picked a live victim and were caught, print the messages */
    if (caught)
    {
        if (success)
        {
            komu[0]->catch_msg("W ostatniej chwili zauwa�asz, �e " +
                QCIMIE(TP,PL_MIA) + " " +
                " kradnie ci " + COMPOSITE_DEAD(co,PL_BIE) + "!!\n");
            komu[0]->increase_ss(SS_AWARENESS,EXP_OKRADZIONY_WIDZE_AWARENESS);
            komu[0]->increase_ss(SS_INT,EXP_OKRADZIONY_WIDZE_INT);
        }
        else
        {
            komu[0]->catch_msg("Przy�apujesz " +
                QIMIE(TP,PL_BIE) + " pr�buj�c" +TP->koncowka("ego","�","e")+
                " ukra�� tw"+co[0]->koncowka("�j","oj�","oje") +" "+
                COMPOSITE_DEAD(co,PL_BIE) +"!!\n");
            komu[0]->increase_ss(SS_AWARENESS,EXP_KRADZIONY_WIDZE_AWARENESS);
            komu[0]->increase_ss(SS_INT,EXP_KRADZIONY_WIDZE_INT);
        }

        /* We got busted, but do we know we got busted? */
        if (this_player()->resolve_task(TASK_SIMPLE, ({ SS_AWARENESS }),
                komu[0], ({ TS_DIS })) > 0)
        {
            TP->catch_msg("Ups! Zdaje si�, �e " + QIMIE(komu[0],PL_MIA) +
                " przy�apa�"+komu[0]->koncowka("","a","o")+" ci� na kradzie�y!\n");
        }

        /* Does the victim attack thieves instinctively? *
        if (komu[0]->query_prop(LIVE_I_ATTACK_THIEF))
        {
            komu[0]->command("$say to " + OB_NAME(this_player()) +
                " Stop it you thief!");

            /* Don't switch targets in combat! *
            if (!objectp(komu[0]->query_attack()))
            {
                komu[0]->command("$kill " + OB_NAME(this_player()));
            }
        }*/
        //zamiast tego g�wna powy�ej, przesy�amy ofiarze sygna�.
        komu[0]->signal_kradziez(TP,co);

        /* Catching a thief makes us a bit more wary for awhile.. */
        tmp = komu[0]->query_prop(LIVE_I_VICTIM_ADDED_AWARENESS);
        if (tmp == 0)
        {
            komu[0]->set_skill_extra(SS_AWARENESS,
                (komu[0]->query_skill_extra(SS_AWARENESS) +
                F_AWARENESS_BONUS));

            /* 5 minutes to be precise */
            set_alarm(300.0, 0.0, &remove_extra_skill(komu[0]));
        }

        /* This ensures that we only get one bonus at a time */
        komu[0]->add_prop(LIVE_I_VICTIM_ADDED_AWARENESS, ++tmp);
    }

    /* We are only going to bother with this if we got the item*/
    if (success && !sizeof(moveresult-({0})))
    {
        //testujemy czy reszta livingow na lokacji zauwazy
        check_watchers_see_steal(komu[0], TP, chance, co);
    }

#ifdef LOG_STEALS
    tmp = sprintf("Thieves name:  %-11s   Time:  %-s\nPlace/Victim:  %-s\n"+
        "Item Stolen:   %-s\nT_Pick:  %3d  T_Steal: %3d  "+
        "T_Aware: %3d  T_Dex: %3d\nV_Aware: %3d  "+
        "V_Int:   %3d  V_Dex: %3d\n"+
        "B_Chance: %5d  E_Chance: %8d  "+
        "R_Chance: %8d\nB_Notice: %5d  E_Notice: %8d  "+
        "R_Notice: %8d\nItm_Val:  %5d  Result:   %8d  "+
        "Move_Res: %8d\nSuccess:  %5d  Caught:   %8d  "+
        "Gen_XP:   %8d\n%'-'75s\n\n",
        this_player()->query_name(), ctime(time()),
        interactive(komu[0]) ? komu[0]->query_name() :
        file_name(komu[0]), log10,
        this_player()->query_skill(SS_PICK_POCKET),
        this_player()->query_skill(SS_SNEAK),
        this_player()->query_skill(SS_AWARENESS),
        this_player()->query_stat(SS_DEX),
        komu[0]->query_skill(SS_AWARENESS),
        komu[0]->query_stat(SS_INT),
        komu[0]->query_stat(SS_DEX),
        log[0], log[1], log[5], log[3], log[4],
        log[6], log[2], log[7], log[8],
        success, caught, log[9], "");

    setuid();
    seteuid(getuid(this_object()));
    write_file(LOG_STEALS, tmp);
#endif LOG_STEALS

    return 1;
}
