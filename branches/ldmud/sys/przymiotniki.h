/**
 * \file /sys/przymiotniki.h
 *
 * Plik obs�ugi przymiotnik�w.
 *
 * @author Krun, krun@vattghern.com.pl
 * @date 15.9.2008 18:42:14
 */

#include "/sys/files.h"

#ifndef D___PRZYMIOTNIKI_H___D
#define D___PRZYMIOTNIKI_H___D

#define PRZYM_OBLICZ_MNOGA(p)         PRZYMIOTNIKI->oblicz_mnoga(p)
#define PRZYM_Z_MNOGA(p)              ({p, PRZYM_OBLICZ_MNOGA(p)})

#endif
//Pod tym bardzo prosz� nic nie definiowa� bo b�d� musia� by� niemi�y!
