#define H_DIR "/std/horses/"

#define H_R_SH H_DIR + "ride_sh.c"
#define H_L_SH H_DIR + "lead_sh.c"

#define HORSE_I_IS "_horse_i_is"
#define LIVE_O_ON_HORSE "_live_o_on_horse"
#define LIVE_O_HORSE_LEAD "_live_o_horse_lead"

#define H_SUBLOC_SIODLO "_sublokacja_na_siodlo_"
#define H_SUBLOC_OGLOWIE "_sublokacja_na_oglowie_"
#define H_SUBLOC_WODZE "_sublokacja_na_wodze_"
#define H_SUBLOC_JEZDZIEC "_sublokacja_na_dummy_jezdzca_"
#define H_SUBLOC_JUKI "_sublokacja_na_juki_"
#define H_SUBLOC_ENV(x) ("_sublokacja_jezdzcow_w_ENV_konia_"+OB_NUM(x)+"_")

#define H_STD_SIODLO H_DIR + "siodlo.c"
#define H_STD_OGLOWIE H_DIR + "oglowie.c"
#define H_STD_WODZE H_DIR + "wodze.c"
#define H_STD_JUKI H_DIR + "juki.c"

#define K_KON          ({"kon", "konia", "koniowi",         \
                          "konia", "koniem", "koniu"}),     \
                        ({"konie", "koni", "koniom",        \
                          "konie", "konmi", "koniach"}),    \
                        PL_MESKI_NOS_ZYW

#define K_KLACZ         ({"klacz", "klaczy", "klaczy",      \
                          "klacz", "klacza", "klaczy"}),    \
                        ({"klacze", "klaczy", "klaczom",    \
                          "klacze", "klaczami",             \
                          "klaczach"}), PL_ZENSKI

#define K_RUMAK         ({"rumak", "rumaka", "rumakowi",     \
                          "rumaka", "rumakiem", "rumaku"}),  \
                        ({"rumaki", "rumakow", "rumakom",    \
                          "rumaki", "rumakami","rumakach"}), \
                          PL_MESKI_NOS_ZYW

#define K_OGIER         ({"ogier", "ogiera", "ogierowi",     \
                          "ogiera", "ogierem", "ogierze"}),  \
                        ({"ogiery", "ogierow", "ogierom",    \
                          "ogierow", "ogierami","ogierach"}),\
                          PL_MESKI_NOS_ZYW

#define H_PRZYPISANY_KON this_player()->query_prop(LIVE_O_ON_HORSE)

