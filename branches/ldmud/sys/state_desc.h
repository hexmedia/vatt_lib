/**
 *  sys/state_desc.h
 *
 *  Holds all textual descriptions of state descriptions of livings.
 *
 *  Note that local changes to these arrays are done in
 *  /config/sys/state_desc2.h
 *
 *  TODO:
 *  - Przyda�o by si� wywali� niepotrzebne definicje bo przez nie jest tu tylko
 *    burdel! Zreszt� nie tylko tu
 */

#ifndef SD_DEFINED
#include "/config/sys/state_desc2.h"
#endif

#ifndef SD_DEFINED
#define SD_DEFINED

#ifndef SD_AV_TITLES
#define SD_AV_TITLES  ({                \
        ({"niema!","niema!"}),  \
        ({"niema!", "niema!"}),\
    ({"niema!","niema!"}),\
        ({"niema!","niema!"}),\
        ({"niema!","niema!"}),\
        ({"niema!","niema!"}),\
        ({"niema!","niema!"}),\
        ({"niema!","niema!"}),\
        ({"niema!","niema!"}),\
        ({"niema!","niema!"}),\
        ({"niema!","niema!"}),\
        ({"niema!","niema!"}),\
        ({"niema!","niema!"}),\
})
#endif

#define SD_STAT_NAMES       ({ "S", "Zr", "Wt", "Int", "Odw" })
#define SD_STAT_NAMES_MIA   ({ "si^la", "zr^eczno^s^c", "wytrzyma^lo^s^c",      \
                               "intelekt", "odwaga" })
#define SD_STAT_NAMES_BIE   ({  "si^l^e", "zr^eczno^s^c", "wytrzyma^lo^s^c",    \
                                "intelekt","odwag^e" })

//#define SD_DESCR_PER_STAT 13       Ilosc opisow cech
//#define SD_LEVELS_PER_STAT 12  Ilosc punktow stata na ceche (bylo w oryginale 17)

#define SD_STATLEV_STR(x) ({"bezsiln"+(x),                      \
                            "s^labiutk"+(x == "y" ? "i" : x),   \
                            "bardzo s^lab"+(x),                 \
                            "chucherkowat"+(x),                 \
                            "s�abowit"+(x),                     \
                            "w�t�"+(x),                         \
                            "s�ab"+(x),                         \
                            "jar" + (x),                        \
                            "krzepk" + (x == "y" ? "i" : x),    \
                            "siln" + (x),                       \
                            "mocn" + (x),                       \
                            "t^eg" + (x == "y" ? "i" : x),      \
                            "muskularn"+(x),                    \
                            "bardzo siln" + (x),                \
                            "pot^e^zn" + (x),                   \
                            "mocarn" + (x) })

#define SD_STATLEV_DEX(x) ({"ofermowat" + (x),              \
                            "nieskoordynowan" + (x),        \
                            "oci�a�" + (x),                \
                            "niezr^eczn" + (x),             \
                            "nieporadn" + (x),              \
                            "�lamazarn" + (x),              \
                            "niezgrabn" + (x),              \
                            "sprawn" + (x),                 \
                            "gi�tk" + (x == "y" ? "i" : x), \
                            "zwinn" + (x),                  \
                            "szybk" + (x == "y" ? "i" : x), \
                            "zr^eczn" + (x),                \
                            "fertyczn" + (x),              \
                            "gibk" + (x == "y" ? "i" : x),  \
                            "akrobatyczn" + (x),            \
                            "ekwilibrystyczn" + (x) })

#define SD_STATLEV_CON(x) ({"kruch" + (x),              \
                            "delikatn" + (x),           \
                            "cherlaw" + (x),            \
                            "rachityczn" + (x),         \
                            "chuderlaw" + (x),          \
                            "mizern" + (x),             \
                            "dobrze zbudowan" + (x),    \
                            "zaprawion" + (x),          \
                            "zahartowan" + (x),         \
                            "wytrzyma^l" + (x),         \
                            "niestrudzon"+(x),          \
                            "wytrwa�" + (x),            \
                            "niez�omn" + (x),           \
                            "tward" + (x),              \
                            "atletyczn" + (x),          \
                            "niezniszczaln" + (x) })

#define SD_STATLEV_INT(x) ({"g�upkowat" + (x),      \
                            "bezmy^sln" + (x),      \
                            "t^ep" + (x),           \
                            "durn" + (x),           \
                            "ograniczon" + (x),     \
                            "nierozs�dn" + (x),     \
                            "zacofan" + (x),        \
                            "niewykszta�con" + (x), \
                            "poj^etn" + (x),        \
                            "rozumn" + (x),         \
                            "bystr" + (x),          \
                            "wykszta�con" + (x),    \
                            "uczon" + (x),          \
                            "b^lyskotliw" + (x),    \
                            "genialn" + (x),        \
                            "wszechwiedz�c" + (x) })

#define SD_STATLEV_WIS(x)  SD_STATLEV_INT(x)

#define SD_STATLEV_DIS(x) ({"p�ochliw"+(x),                     \
                            "l�kliw" + (x),                     \
                            "trwo�liw" + (x),                   \
                            "zahukan" + (x),                    \
                            "tch^orzliw" + (x),                 \
                            "strachliw" + (x),                  \
                            "niepewn" + (x),                    \
                            "zdecydowan" + (x),                 \
                            "�mia�" + (x),                      \
                            "chwack" + (x == "y" ? "i" : x),    \
                            "odwa�n" + (x),                     \
                            "dzieln" + (x),                     \
                            "nieul�k�"+(x),                     \
                            "nieugi�t" + (x),                   \
                            "nieustraszon" + (x),               \
                            "bohatersk" + (x == "y" ? "i" : x)})

/* Observe that the denominators below are reversed for the first two
   of the statlev descs above.
*/
#define SD_STAT_DENOM   ({ "", "bardzo " })

 /*Bruta chyba mo�na wypieprzy� nie? Krun */
#define SD_BRUTE_FACT(x) ({ "pacyfist" + (x == "a" ? "ka" : "a"),   \
                "bardzo potuln" + x, "potuln" + x,      \
                "bardzo ^lagodn" + x, "^lagodn" + x,        \
                "spokojn" + x, "agresywn" + x,      \
                "bojow" + x, "bardzo bojow" + x,        \
                "brutaln" + x, "bardzo brutaln" + x,    \
                "^z^adn" + x + " krwi" })

#define SD_HEALTH(x)    ({  "ledwo ^zyw" + (x), "ci^e^zko rann" + (x), "w z^lej kondycji",    \
                            "rann" + (x), "lekko rann" + (x), "w dobrym stanie",        \
                            "w ^swietnej kondycji"})

#define SD_MANA(x)  ({  "u kresu si^l", "wyko^nczon" + (x), "wyczerpan" + (x),  \
                        "w z^lej kondycji", "bardzo zm^eczon" + (x),            \
                        "zm^eczon" + (x), "os^labion" + (x),                    \
                        "lekko os^labion" + (x), "w pe^lni si^l"})

#define SD_COMPARE_STR(x)                               \
            ({ "r^ownie siln" + x + " jak",             \
               "troszeczk^e silniejsz" + x + " ni^z",   \
               "silniejsz" + x + " ni^z",               \
               "du^zo silniejsz" + x + " ni^z" })
#define SD_COMPARE_DEX(x)                       \
            ({ ("r^ownie zr^eczn" + x + " jak"),        \
               ("troszeczk^e zr^eczniejsz" + x + " ni^z"),  \
               ("zr^eczniejsz" + x + " ni^z"),          \
               ("du^zo zr^eczniejsz" + x + " ni^z") })
#define SD_COMPARE_CON(x)                       \
            ({ ("r^ownie dobrze zbudowan" + x + " jak"),    \
               ("troszeczk^e lepiej zbudowan" + x + " ni^z"),   \
               ("lepiej zbudowan" + x + " ni^z"),       \
               ("du^zo lepiej zbudowan" + x + " ni^z") })
#define SD_COMPARE_DIS(x)                       \
            ({ ("r^ownie odwa^zn" + x + " jak"),        \
               ("troszeczk^e odwa^zniejsz" + x + " ni^z"),  \
               ("odwa^zniejsz" + x + " ni^z"),      \
               ("du^zo odwa^zniejsz" + x + " ni^z") })

#define SD_PANIC(x) ({  "bezpiecznie", "spokojnie", "nieswojo",     \
                        "nerwowo", "przera^zon" + (x) })

#define SD_FATIGUE(x)   ({  "w pe^lni wypocz^et" + (x), "wypocz^et" + (x),      \
                            "troch^e zm^eczon" + (x), "zm^eczon" + (x),         \
                            "bardzo zm^eczon" + (x), "nieco wyczerpan" + (x),   \
                            "wyczerpan" + (x), "bardzo wyczerpan" + (x),        \
                            "wycie^nczon" + (x), "ca^lkowicie wycie^nczon" + (x)})

#define SD_SOAK ({ "chce ci si^e bardzo pi^c", "chce ci si^e pi^c",     \
                   "troch^e chce ci si^e pi^c", "nie chce ci si^e pi^c" })

#define SD_STUFF(x) ({ "g^lodn" + (x), "najedzon" + (x)})

#define SD_INTOX(x) ({  "podchmielon" + (x), "podpit" + (x), "wstawion" + (x),   \
                        "pijan" + (x), "schlan" + (x), "nawalon" + (x) })

#define SD_HEADACHE ({ "lekkiego", "niemi^lego", "dra^zni^acego", "niez^lego", "straszliwego", "potwornego" })

#define SD_IMPROVE_MIN  (10)
#define SD_IMPROVE_MAX  (100000)

#define SD_IMPROVE  ({ "niezauwa�alne", "znikome", "minimalne", "nik�e", "�ladowe", "malutkie", \
               "bardzo ma�e", "nieznaczne", "ma�e", "niewielkie", "niedu^ze",       \
                   "znaczne", "du^ze", "poka�ne", "bardzo du^ze", "okaza�e", "wielkie",     \
                   "wspania^le", "ogromne", "kolosalne", "olbrzymie", "niewiarygodne",      \
                   "imponuj�ce", "niesamowite", "pora�aj�ce", "fenomenalne", "fantastyczne",\
               "niewyobra�alne"})

#define SD_GOOD_ALIGN   ({ "neutral", "agreeable", "trustworthy",     \
               "sympathetic", "nice", "sweet", "good",    \
               "devout", "blessed", "saintly", "holy" })


#define SD_EVIL_ALIGN   ({ "neutral", "disagreeable", "untrustworthy",    \
               "unsympathetic", "sinister", "wicked", "nasty",\
               "foul", "evil", "malevolent", "beastly",   \
               "demonic", "damned" })

#define SD_RECOVER_LEVEL ({ "wiecznie", "chwil^e", "troch^e","d^lugo","bardzo d^lugo"})

#define SD_ARMOUR(x) ({ "w znakomitym stanie",                                                              \
                        "lekko podniszczon" + (x),              \
                        "w kiepskim stanie",                   \
                        "w op^lakanym stanie",  \
                        "gotow" + (x) +                            \
                        "rozpa^s^c si^e w ka^zdej chwili" })

#define SD_ARMOUR_SIZE(x)   ({ \
        "zdecydowanie za du^z" + x, \
        "za du^z" + x,  \
        "troch^e za du^z" + x,  \
        "w twoim rozmiarze",    \
        "troch^e za ma^l" + x,  \
        "za ma^l" + x,  \
        "zdecydowanie za ma^l" + x })

#define SD_WEAPON_RUST(o) ({ \
           "brak komentarza",           \
               "postrzegasz na " + o->koncowka("nim", "niej", "nim", "nich", "nich") +\
               " ^slady rdzy",  \
           "spostrzegasz na " + o->koncowka("nim", "niej", "nim", "nich", "nich") +\
               " liczne ^slady rdzy", \
                o->koncowka("jest ca^ly pokryty", "jest ca^la pokryta", \
            "jest ca^le pokryte", "s^a cali pokryci", "s^a ca^le pokryte") + \
               " rdz^a",    \
           "wygl^ada jak po k^apieli w kwasie",\
                o->koncowka("jest", "jest", "jest", "s^a", "s^a") + \
               " tak zniszczon" + o->koncowka("y", "a", "e", "i", "e") + \
               ", ^ze mo^ze rozpa^s^c si^e w ka^zdej chwili" })

#define SD_WEAPON_COND(o) ({ \
            o->koncowka("jest", "jest", "jest", "s^a", "s^a") + "w znakomitym stanie",      \
            o->koncowka("jest", "jest", "jest", "s^a", "s^a") + " w dobrym stanie",         \
            "wida^c na " + o->koncowka("nim", "niej", "nim", "nich", "nich") +              \
            " ^slady walki",                                                                \
            "wida^c na " + o->koncowka("nim", "niej", "nim", "nich", "nich") + "            \
            liczne ^slady walki",                                                           \
            o->koncowka("jest", "jest", "jest", "s^a", "s^a") + " " +                       \
                "lekko poszczerb" + o->koncowka("iony", "iona", "ione", "ieni", "ione"),    \
            o->koncowka("jest", "jest", "jest", "s^a", "s^a") +                             \
                " poszczerb" + o->koncowka("iony", "iona", "ione", "ieni", "ione"),         \
            o->koncowka("jest", "jest", "jest", "s^a", "s^a") + " w z^lym stanie",          \
            o->koncowka("jest", "jest", "jest", "s^a", "s^a") + " w bardzo z^lym stanie",   \
            "wymaga" + o->koncowka("", "", "", "j^a", "j^a") + " natychmiastowej " +        \
                "konserwacji",                                                              \
            o->koncowka("mo�e", "mo�e", "mo�e", "mog�", "mog�") + " p^ekn^a^c w ka^zdej " + \
                "chwili" })

#define SD_ENC_WEIGHT   ({                                                                                  \
            "ci^e^zar twego ekwipunku wadzi ci troch^e",        \
            "ci^e^zar twego ekwipunku daje ci si^e we znaki",   \
            "ci^e^zar twego ekwipunku jest dosy^c k^lopotliwy", \
            "tw^oj ekwipunek jest wyj^atkowo ci^e^zki",         \
            "tw^oj ekwipunek jest niemi^losiernie ci^e^zki",    \
            "tw^oj ekwipunek prawie przygniata ci^e do ziemi"})

#define SD_SKILLEV  ({"ledwo", "mizernie", "marnie", "kiepsko", "s�abo", "troch�",     \
              "powierzchownie", "pobie�nie", "zno�nie", "poprawnie", "nale�ycie", \
              "przyzwoicie", "nieprzeci�tnie", "zadowalaj�co", "nie�le", "dobrze", \
              "wprawnie", "umiej�tnie", "fachowo", "biegle", "�wietnie", "imponuj�co", \
              "bezb��dnie", "wybornie", "wspaniale", "znakomicie", "doskonale", \
              "perfekcyjnie", "mistrzowsko", "fenomenalnie"})

#define SD_LEVEL_MAP ([                                                          \
                 "post^ep^ow"                    : SD_IMPROVE,                   \
                 "kaca"                          : SD_HEADACHE,                  \
                 "pija^nstwa"                    : SD_INTOX("y"),                \
                 "syto^sci"                      : SD_STUFF("y"),                \
                 "pragnienia"                    : SD_SOAK,                      \
                 "zm^eczenia"                    : SD_FATIGUE("y"),              \
                 "strachu"                       : SD_PANIC("y"),                \
                 "many"                          : SD_MANA("y"),                 \
                 "kondycji"                      : SD_HEALTH("y"),               \
                 "por^ownania si^ly"             : SD_COMPARE_STR("y"),          \
                 "por^ownania zr^ecznosci"       : SD_COMPARE_DEX("y"),          \
                 "por^ownania wytrzyma^lo^sci"   : SD_COMPARE_CON("y"),          \
                 "si^ly"                         : SD_STATLEV_STR("y"),          \
                 "zr^eczno^sci"                  : SD_STATLEV_DEX("y"),          \
                 "wytrzyma^lo^sci"               : SD_STATLEV_CON("y"),          \
                 "intelektu"                     : SD_STATLEV_INT("y"),          \
                 "odwagi"                        : SD_STATLEV_DIS("y"),          \
                 "jako^sci zbroi"                : SD_ARMOUR("y"),               \
                 "wielko^sci zbroi"              : SD_ARMOUR_SIZE("y"),          \
                 "zardzewienia"                  : SD_WEAPON_RUST(this_player()),\
                 "jako^sci broni"                : SD_WEAPON_COND(this_player()),\
                 "przeci^a^zenia"                : SD_ENC_WEIGHT,                \
                 "umiej^etno^sci"                : SD_SKILLEV,                   \
])
#endif
