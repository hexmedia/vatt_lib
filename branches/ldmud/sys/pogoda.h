/*
 * pogoda.h
 *
 * Plik zawieraj�cy definicje przydatne do obs�ugi pogody.
 */

#ifndef _JS_POGODA_H
#define _JS_POGODA_H

#define POGODA_TEMP         0
#define POGODA_TEMP_ODCZ    1   /* temperatura odczuwalna */
#define POGODA_TEMP_MIN     2
#define POGODA_TEMP_MAX     3
#define POGODA_WIATR        4
#define POGODA_WIATR_K      5   /* kierunek wiatru */
#define POGODA_WIATR_P      6   /* pr�dko�� wiatru */
#define POGODA_WILGOTNOSC   7
#define POGODA_WIDOCZNOSC   8
#define POGODA_WSCHOD       9
#define POGODA_ZACHOD       10
#define POGODA_POBRANIE     11
#define POGODA_MIASTO       12  /* miasto dla kt�rego s� przechowywane dane */
#define POGODA_MIASTOR      13  /* rzeczywiste miasto z kt�rego pobierane by�y dane */
#define POGODA_ZACHMURZENIE 14
#define POGODA_OPADY        15
#define POGODA_SIZE         16

/* typy zachmurzenia */
#define POG_ZACH_ZEROWE    0
#define POG_ZACH_LEKKIE    1
#define POG_ZACH_SREDNIE   2
#define POG_ZACH_DUZE      3
#define POG_ZACH_CALKOWITE 4

/* stopie� i rodzaj opad�w */
#define POG_OP_ZEROWE      0
#define POG_OP_D_L         1
#define POG_OP_D_S         2
#define POG_OP_D_C         3
#define POG_OP_D_O         4
#define POG_OP_S_L         5
#define POG_OP_S_S         6
#define POG_OP_S_C         7
#define POG_OP_G_L         8
#define POG_OP_G_S         9

/* typy wiatr�w */
#define POG_WI_ZEROWE      0
#define POG_WI_LEKKIE      1
#define POG_WI_SREDNIE     2
#define POG_WI_DUZE        3
#define POG_WI_BDUZE       4

/* rodzaje opad�w */
#define NIC_NIE_PADA       0
#define PADA_DESZCZ        1
#define PADA_SNIEG         2
#define PADA_GRAD          3

/* moc opad�w */
#define PADA_LEKKO         1
#define PADA_SREDNIO       2
#define PADA_MOCNO         3
#define PADA_BMOCNO        4

#include <files.h>
#include <stdproperties.h>

/* r�ne przydatne makra */
#define CO_PADA(lok) POGODA_OBJECT->get_plan(lok->query_prop(ROOM_I_WSP_X), lok->query_prop(ROOM_I_WSP_Y))->co_pada()
#define MOC_OPADOW(lok) POGODA_OBJECT->get_plan(lok->query_prop(ROOM_I_WSP_X), lok->query_prop(ROOM_I_WSP_Y))->moc_opadow()
#define ZACHMURZENIE(lok) POGODA_OBJECT->get_plan(lok->query_prop(ROOM_I_WSP_X), lok->query_prop(ROOM_I_WSP_Y))->get_zachmurzenie()
#define TEMPERATURA(lok) POGODA_OBJECT->get_plan(lok->query_prop(ROOM_I_WSP_X), lok->query_prop(ROOM_I_WSP_Y))->get_temperatura()
#define MOC_WIATRU(lok) POGODA_OBJECT->get_plan(lok->query_prop(ROOM_I_WSP_X), lok->query_prop(ROOM_I_WSP_Y))->get_wiatry()
#define CZY_JEST_SNIEG(lok) POGODA_OBJECT->get_plan(lok->query_prop(ROOM_I_WSP_X), lok->query_prop(ROOM_I_WSP_Y))->get_ilosc_sniegu()
#define CZY_JEST_WODA(lok) POGODA_OBJECT->get_plan(lok->query_prop(ROOM_I_WSP_X), lok->query_prop(ROOM_I_WSP_Y))->get_ilosc_wody()

#endif
