#ifndef ROZMIARY_H
#define ROZMIARY_H


//Maksymalna zmiana rozmiaru
#define MAX_RESIZE(o)       2
/*FIXME: Trzeba dopisa� rozr�nianie obiekt�w,
         bo niekt�re rzeczy powinny m�c by�
         resizowane wiecej ni� dwa razy a niekt�re
         mniej - nie mam jeszcze pomys�u na kryterium
         jak kto� ma to niech pisze albo zagada na mudzie
         (Krun) */

#define ROZMIARY ({"XXXS", "XXS", "XS", "S", "M", "L", "XL", "XXL", "XXXL"})
//*** cz�� g�rna
  // obw�d g�owy
#define ROZ_OB_GLOWY         0
  // obw�d szyi
#define ROZ_OB_SZYI          1
//*** ko�czyna g�rna
  // obw�d obr�czy ko�czyny g�rnej
#define ROZ_OB_OKG           2
  // d�ugo�� ramienia
#define ROZ_DL_RAMIENIA      3
  // obw�d ramienia
#define ROZ_OB_RAMIENIA      4
  // d�ugo�� przedramienia
#define ROZ_DL_PRZEDRAMIENIA 5
  // obw�d przedramienia
#define ROZ_OB_PRZEDRAMIENIA 6
  // obw�d nadgarstka
#define ROZ_OB_NADGARSTKA    7
  // d�ugo�� d�oni
#define ROZ_DL_DLONI         8
  // szeroko�� d�oni
#define ROZ_SZ_DLONI         9
//*** tu��w
  // d�ugo�� tu�owia
#define ROZ_DL_TULOWIA       10
  // obw�d w klatce piersiowej
#define ROZ_OB_W_KLATCE      11
  // obw�d w pasie
#define ROZ_OB_W_PASIE       12
  // obw�d w biodrach
#define ROZ_OB_W_BIODRACH    13
//*** ko�czyna dolna
  // d�ugo�� uda
#define ROZ_DL_UDA           14
  // obw�d uda
#define ROZ_OB_UDA           15
  // d�ugo�� goleni
#define ROZ_DL_GOLENI        16
  // obw�d goleni
#define ROZ_OB_GOLENI        17
  // obw�d kostki
#define ROZ_OB_KOSTKI        18
  // d�ugo�� stopy
#define ROZ_DL_STOPY         19
  // szeroko�� stopy
#define ROZ_SZ_STOPY         20

#define ROZ_MAX              20

#endif
