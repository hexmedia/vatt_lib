/**
 * \file /sys/exec.h
 *
 * This is the default include file with some prefedefined macros for the
 * command exec. It is included in your exec_obj.c by default _after_ the
 * exec.h from your home directory has been included. Provisions are made
 * so that definitions from this file, generic as they are, are only
 * created if they are not defined before to guard against redefinition
 * errors.
 *
 * The following definitions are included:
 *
 * ME      Always returns the objectpointer to you, the person executing
 *         the exec command, even if you switched this_player().
 * TP      this_player() [may have been altered using set_this_player(o)]
 * TI      this_interactive() [probably always the same as ME]
 * TO      this_object()
 * ENV(o)  environment(o)
 * INV(o)  all_inventory(o)
 * CAP(s)  capitalize(s)
 * LOW(s)  lower_case(s)
 */

#ifndef DEFAULT_EXEC_INCLUDE
#define DEFAULT_EXEC_INCLUDE

//pare dodatkow
#include <mxp.h>
#include <sit.h>
#include <money.h>
#include <units.h>
#include <colors.h>
#include <combat.h>
#include <pogoda.h>
#include <speech.h>
#include <filepath.h>
#include <materialy.h>
#include <object_types.h>

#ifndef ME
#define ME     (find_player(getuid(this_object())))
#endif  ME

#ifndef INV
#define INV(o) (all_inventory(o))
#endif  INV

#ifndef CAP
#define CAP(s) (capitalize(s))
#endif  CAP

#ifndef LOW
#define LOW(s) (lower_case(s))
#endif  LOW

/* No definitions beyond this line. */
#endif DEFAULT_EXEC_INCLUDE
