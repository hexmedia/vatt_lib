{a|b|c} - a dla pierwszej, b dla drugiej, c dla 3ciej osoby
{a/b/c} - losowe spo^sr^od a, b, c
[a|b|c|d|e] - rodzaje, a - m^eska, b - ^ze^nska, c - nijaka,
    d - mnoga m^eska, e - mnoga ^ze^nsko/nijaka
(a|b|c|d|e|f) - przymiotniki si^ly zadanych obra^ze^n.

%b - bro^n bij^acego
%m - bro^n bitego
%s - si^la ciosu - poziom obra^ze^n - WYCOFANE
%p - to gracz bity
%k - gracz bij^acy
%h - to hitlock w kt^ory dostaje
%t - tarcza
%z - zbroja
/ - oddziela losowe
| - oddziela pokazywane dla poszczeg^olnych graczy
WYPAROWANIE PRZEZ ZBROJE

0 mianownik kto? co?
1 dope^lniacz    kogo? czego?
2 celownik  komu? czemu?
3 biernik   kogo? co?
4 narz^ednik     z kim? z czym?
5 miejscownik o kim? o czym?

Si^la jak^a {wk^ladasz|%k:0 wk^lada} w {pchni^ecie/ci^ecie/cios/uderzenie} %b:4 nie wystarczaj^aca by przebi^c si^e przez %z:3 ochraniaj^ac{y|^a|e} {tw[^oj|oj^a|oje]|je[go|j|go]} %h:3.

Impet {pchni^ecie/ci^ecia/ciosu/uderzenia} %b:1 %k:1 zostaje ca^lkowicie wyhamowany przez {tw[^oj|oj^a|oje]|je[go|j|go]} %z:3.

{pchni^ecie/ci^ecie/cios/uderzenie} %b:1 %k:1, pomimo i^z celnie trafi^lo %p:3 w %h:3, nie wyrz^adzi^lo {ci|[mu|jej|mu]} krzywdy, gdy^z impet zosta^l zatrzymany przez %z:3.

{pchni^ecie/ci^ecie/cios/uderzenie} %b:1 %p:1 jest zbyt s^lab[y|a|e] ^zeby zrani^c %p:1 i zatrzymuje si^e na %z:5 chroni^ac[emu|ej|emu} {tw[^oj|oj^a|oje]|je[go|j|go]} %h:3.

{W|%p w}idz^ac nieuchronn[y|^a|e] {pchni^ecie/ci^ecie/cios/uderzenie} rozpaczliwym ruchem ustawi{asz|a} si^e tak, ^ze {%k:1 %:b:0|%b:0 %k:1}, trafiaj^ac w {tw[^oj|oj^a|oje]|je[go|j|go]} %h:0 ze^slizguje si^e ze zgrzytem po %z:2.

Pomimo celnie zadanego {pchni^ecia/ci^ecia/ciosu/uderzenia} %b:1 {twoja pozycja|pozycja %p:1} pozwala na bezpieczne przyj^ecie go na chroni^ac{y|^a|e} %h:3 %z:0.

W mgnieniu oka [|%p:0] ustawi[asz|a] %h:3 tak by {pchni^ecie/ci^ecie/cios/uderzenie} {%k:1 %b:1|%b:1 %k:1} zsun[^a^l|^e^la|^e^lo] si^e po ochraniaj^ac[ym|ej|ym] {ci^e|[go|j^a|go]} %z:1.

Dzi^eki wielkiemu szcz^e^sciu %z:0 ochraniaj^aca {tw[^oj|oj^a|oje] %h:3| %h:3 %p:1} przyj^e^la ca^ly impet {pchni^ecia/ci^ecia/ciosu/uderzenia} %k:1 i uchroni^l[|a|o] ci^e przed ran^a od %b:1.

{Tw[^oj|oja|oje] %b:3|%B:0 %k:1} odskakuje od chroni^ace{go|ej|go} %p:3 %z:1.


Tarczownictwo:

{D|%k:0 d}ostrzegaj^ac w ostatniej chwili {pchni^ecie/ci^ecie/cios/uderzenie} %k:1 przyjmuj{esz|e} [go|j^a|to] na sw[^oj|oj^a|oje] %t:3.

Przyjmuj^ac odpowiedni^a pozycj^e {|%p} ustawi{asz|a} %t:3 tak by ca^ly impet {pchni^ecia/ci^ecia/ciosu/uderzenia} %b:1 %k:1 zostaje wyparowan[y|a|e].

Dzi^eki sporej dawce szcz^e^scia wyprowadzon[y|a|e] przez %k:3 {pchni^ecie/ci^ecie/cios/uderzenie} %b:3, zostaje przez {ciebie|[niego|j^a|to]} zablokowany %t:4.

%b:0 %k ze^slizguje si^e po powierzchni {twoje[go|j|go]|je[go|j|go]} %t:5.

[P|%P:0 p]rzykuc[asz|a] lekko wznosz^ac %t:3 nad g^low^e i zatrzymuj^ac [nim|ni^a|tym] {pchni^ecie/ci^ecie/cios/uderzenie} %b:1 %k:1.

{R|%p:0 r}obi[sz|] krok w bok jednocze^snie sko^snie ustawiaj^ac %t:3, przez co {pchni^ecie/ci^ecie/cios/uderzenie} %b:1 %k:1 zsuwa si^e po [nim|niej|tym] nie odnosz^ac skutku.

{G|%p:1 g}wa^ltownie podryw[asz|a] %t:3 zbijaj^ac ju^z niemal si^egaj^ac[y|^a|e] %b:3 %k:1.

Szybkim wypadem {|%p:0} skrac[asz|a] dystans i blokuj[esz|3] %t:4 {pchni^ecie/ci^ecie/cios/uderzenie}  %b:1 %k:1 w chwili gdy dopiero go wyprowadz{a|asz}.

{Z|%p:0 z}zbij[a|asz] kantem %t:1 wymierzony przez %k:3 {pchni^ecie/ci^ecie/cios/uderzenie} %b:1.

Tracz^ac z oczu{ twoj| }%b:3{ |%k:1 }przezornie os^lani[a|asz] si^e %t:4 po kr^otej ze^slizguje si^e {pchni^ecie/ci^ecie/cios/uderzenie}.

{B|%P:0 b}^lyskawicznie os^lani[asz|a] %t:4 g^low^e w kt^or^a mierzy^l[|a|o {pchni^ecie/ci^ecie/cios/uderzenie} %b:1 %k:1.

{U|%p:0 u}mykaj^acym oku ruchem zbij[asz|a] wymierzone w {ciebie|ni[ego|^a|e]} %b:1 %k:1 niwecz^ac {pchni^ecie/ci^ecie/cios/uderzenie}.

{O|%p:0 o}s^lani[asz|a] si^e %t:4 przed {twoim|je[go|j|go] {pchni^eciem/ci^eciem/ciosem/uderzeniem} i cho^c zmusza {ci^e|[go|j^a|to]} [on|ona|to] do cofni^ecia si^e o krok, nie czyni {ci|[mu|jej|temu]} krzywdy.

Lekkie uniesienie %t:1 przez %p:3 wystarcza by zsun^e^lo si^e po {nim|niej|tym} {pchni^ecie/ci^ecie/cios/uderzenie}{ twojego| } %b:1{ |%k:1}.


Parowanie:

{L|%p:0 l}ekko podbij{asz|a} swoj^a broni^a %b:0 %k:1 przez co {pchni^ecie/ci^ecie/cios/uderzenie} mija {ci^e|[go|j^a|to]} nie czyni^ac krzywdy.

{S|%p s}tar{asz|a} si^e zablokowa^c {pchni^ecie/ci^ecie/cios/uderzenie} %b:1 %k:1 i cho^c ze^slizn{^a^l|^e^la|^e^lo} si^e nieco po {two[im|jej|im]|je[go|j|go} %m:5 to i tak nie czyni {ci|[mu|jej|mu]} krzywdy.

{N|%p:0 n}adstawi{asz|a} %m:3 wykr^ecaj^ac nadgarstek a^z po granic^e b^olu, dzi^eki temu udaje {ci|[mu|jej|mu]} si^e jednak sparowa^c wyprowadzone przez %k:1 {pchni^ecie/ci^ecie/cios/uderzenie} %b:1.

Robi^ac krok w ty^l %p:0 wykonuj{esz|e} jednocze^snie poprzeczn^a zastaw^e, po kt^orej ze^slizguje si^e {tw^oj %b:0|%b:0 %k:1}.

{P|%p:0 p}rzyjmuj{esz|e} {pchni^ecie/ci^ecie/cios/uderzenie} na %m:3 pozwalaj^ac by %b:0 %k:1 ze^slizgn[^a^l|^e^la|^e^lo] si^e po {twej|[jego|jej|jego]} broni.

{D|%p:0 d}esperacko zastawi{asz|a} si^e %m:4, a si^la zablokowanego {twojego | }{pchni^ecia/ci^ecia/ciosu/uderzenia}{ | %k:1} cofa {ci^e|[go|j^a|je]} o krok w ty^l.

{R|%p:0 r}obi{sz|} kr^otki wypad, wysoko podbijaj^ac swo[im|j^a|im] %m:4 %b:1 %k:1 nim {pchni^ecie/ci^ecie/cios/uderzenie} {ci^e|{jego|j^a|to]} dosi^egn[^a^l|^e^la|^e^lo] .

{O|%p:0 o}tacz{asz|a} si^e szerok^a zastaw^a, paruj^ac %m:4 {pchni^ecie/ci^ecie/cios/uderzenie} %b:1 %k:1.

Trac^ac na chwil^e {twoj^a | }bro^n %k:1 z oczu,{ |%p:0 }intuicyjnie zas^lani{asz|a} si^e %m: na kt^ory niemal w tej samej chwili spada {pchni^ecie/ci^ecie/cios/uderzenie} %b:1.

{U|%p:0 u}stawi{asz|a} sko^snie %m:0 pewnie odbijaj^ac [nim|ni^a|tym] {pchni^ecie/ci^ecie/cios/uderzenie} %b:1 %k:1.


UNIKI

{U|%p:0 u}skakuj{esz|e} lekko w bok, o w^los unikaj^ac {pchni^ecia/ci^ecia/ciosu/uderzenia} %b:1 %k:1.

Mocnym skr^etem tu^lowia{ | %p:0 }uchyl{asz|a} si^e przed {pchni^ecie/ci^ecie/cios/uderzenie} %k:1.

{B|%p:0 b}^lyskawicznie przykl^ek{asz|a} pod wymierzonym w g^low^e {pchni^eciem/ci^eciem/ciosem/uderzeniem} %b:1.

{S|%p:0 s}chylasz si^e, wtulaj^ac mocno g^low^e w ramiona unikaj^ac {pchni^ecia/ci^ecia/ciosu/uderzenia} %k:1, lecz i tak %b:0 z sykiem przecina powietrze tu^z przy {twoim|[jego|jej|tego]} uchu.

{R|%p:0 r}obisz oszcz^edny krok w ty^l, a {[tw^oj|twoja|twoje] | }%b:0{ | %k:1} tnie powietrze tu^z przed {tob^a|[nim|ni^a|tym]}.

{G|%p:0 g}wa^ltownie odskakuj{esz|e} w ty^l przed {pchni^eciem/ci^eciem/ciosem/uderzeniem} %k:1, ledwo odzyskuj^ac po tym r^ownowag^e.

{S|%p:0 s}kr^ec{asz|a} si^e mocno odchylaj^ac rami^e, w kt^ore wymierzon[y/a/e] by[^l|^lo|^lo] {pchni^eciee/ci^ecie/cios/uderzenie} %b:1 %k:1.

Oszcz^ednym p^o^lobrotem{ | %p:0 }schodzi{sz | }z linii ataku %k:1.

W ostatniej chwili{ | %p:0 }rob{isz|i} krok w ty^l unikaj^ac {twoje[go|j|go] | }%b:1{ | %k:1} mierz^ace[go|j|go] w {twoj^a|[jego|jej|jego]} {prawe/lewe} udo.

{U|%p:0 u}chyl{asz|a} si^e mocno w {lewo/prawo} przed {pchni^eciem/ci^eciem/ciosem/uderzeniem} %k:1.

{D|%p:0 d}esperacko uchyl{asz|a} si^e przed {pchni^eciem/ci^eciem/ciosem/uderzeniem} %k:1, %b:0 mimo to niemal ociera si^e o {ciebie|ni[ego|^a|e]}.


Rani^ace trafienia

%B:0 %k:1 wydaje si^e o w^los chybia^c celu. Jedynie delikatny wianuszek krwii ci^agn^acy si^e za [jego|jej|tego] czubkiem ^swiadczy, ^ze{ %p:0 | }zosta{^le^s|[^l|^la|^lo]} zranion[y|a|e] w %h:3.

%B:0 %k:1 niemal nie zwalnia (ledwo|lekko||mocno) zahaczaj^ac o{| tw^oj|} %h:0 {|%p:1}.

Zadane z rozmachem przez %k:3 {pchni^ecie/ci^ecie/cios/uderzenie} %b:1 %s %p:3 w %h:3.

Skr^ocony szybkim wypadem dystans daje %k:2 okazj^e do zadania {pchni^ecia/ci^ecia/ciosu/uderzenia} %b:4 prosto w %h:3 %p:1.

{W|%k:0 w}ykonuj{esz|e} kr^otki zw^od %b:4, by niemal natychmiast zada^c {pchni^ecie/ci^ecie/cios/uderzenie} wprost w nieos^loni^et{y|^a|e} %h3 %p:1.

{R|%k:0 r}ob{isz|i} krok w {lewo/prawo/prz^od} by obr^oci^c si^e na pi^ecie i zada^c zamaszyst{y|^a|e} {pchni^ecie/ci^ecie/cios/uderzenie}%b:4, kt^ore %s %p:3 w %h:3.

%p:0 nie do^s^c szybko uskakuje przed{ twoim | }{pchni^eciem/ci^eciem/ciosem/uderzeniem}{ | %k:1 } i {tw[^oj|oja|oje]|je[go|j|go]} %b:0 spada {ci|[mu|jej|mu]} na %h:3.
s
{Tw[^oj|oja|oje] %b:0|%B:0 %k:1}si^ega {%h:1 %p:1|twe[go|j|go] %h:1} zakre^slaj^ac w powietrzu krwawe p^o^lkole.

{Pchni^ecie/Ci^ecie/Cios/Uderzenie}{ twoje[go|j|go] | } %b:1{ | %k:1 } %s {ci^e|[go|j^a|to]} grz^ezn^ac na moment, lecz ju^z po chwili wyszarpni^et[y|a|e] ci^agnie za sob^a smug^e krwii.

{Z|%k:0 z}bij{asz|a} %b:4{ twoj^a | }bro^n{ | %n:1 }, a nast^epnie post^epuj^ac krok naprz^od  %s {pchni^eciem/ci^eciem/ciosem/uderzeniem} prosto w %:3.

{Z|%k:0 z}askakuj{esz %p:3|e ci^e} {szybkim/nag^lym/kr^otkim} {pchni^eciem/ci^eciem/ciosem/uderzeniem} %b:1 w %h:3, przez nie zastawi{asz|a} si^e w por^e.

{R|%k:0 r}ob{isz|i} krok w ty^l pozornie cofaj^ac si^e, lecz jednocze^snie wyprowadz{asz|a} {pchni^ecie/ci^ecie/cios/uderzenie}%b:4, [kt^ory|kt^ora|kt^ore] %s {ci^e|[go|j^a|je]} w %h:3.

{M|%k:0 m}arkuj{esz|e} cios w szyj^e, po czym przechodz^ac do {pchni^ecia/ci^ecia/uderzenia} %b:4 w %h:3, %s {ci^e|[go|j^a|je]} w %h:3.

Szybkim krokiem w ty^l{ | %k:0 }zwi^eksz{asz|a} dystans, by natychmiast wybi^c si^e lekko i zada^c {pchni^ecie/ci^ecie/cios/uderzenie} z g^ory. {Tw^oj|Je[go|j|go]} %b:0 %s {ci^e|[go|j^a|to]} w %h:3.

{N|%k:0 n}ie dostatecznie skrac{asz|a} dystans przed zadaniem (lekkiego|s^labego|silnego|mocnego) {pchni^ecia/ci^ecia/ciosu/uderzenia}, lecz dajesz rad^e si^egn^a^c{ tw^oj | }%h:3{ | %p:1 } ko^ncem swoje[go|j|go] %b:4.





(*) - opisy mog^ace z powodzeniem pos^lu^zy^c szerszej klasie broni (ca^lej podleg^lej pod danego uma)
(**) - opisy kt^ore pasuj^ace do broni podleg^lej pod wi^ecej ni^z jednego uma (ale niekoniecznie do ka^zdej!})


MIECZE
(x)(*) Przed zadaniem pchni^ecia {w|%k:0 w}znos{isz|i} wysoko %b:3 by omin^a^c{ twoj^a | }zastaw^e{ | %p:1 }. Mimo rozpaczliwo^sci takiego ataku, udaje {ci|[mu|jej|mu|im|im]} si^e ugodzi^c {ci^e|przeciwni[ka|czk^e|ka|k^ow|czki|%p]} w %h:3.

(x)(**) Wyprowadzaj^ac %b:4 atak z nad ramienia, nabieraj^ac impetu skr^etem tu^lowia. Silnym i o dziwo niewiele przez to wolniejszym ci^eciem{ | %k:0 } (lekko|bole^snie||ci^e^zko|bardzo ci^e^zko) rani{isz|i} {ci^e|%p:3} w %h:3

(x)(*) {Pewnym/Szybkim/Niespodziewanym/Dalekim} wykrokiem {p|%k:0 p}zachodz{isz|i} {ci^e|%p:1} z {lewej/prawej} by ci^a^c %b:4 od do^lu po %h:5.

(x)(**) Krew tryska z{ twoje[go|ej|go] | }%h:1{ | {%p:1} }po tym jak si^ega {ci^e|[go|j^a|je|ich|je]} ostrze %b:1.

(x)(*) Wywijaj^ac szeroki m^lynek {p|%k:0 p}rzechodz{isz|i} do {niewprawnego/rozpaczliwego/desperackiego} ci^ecia %b:4, kt^ore jednak jako^s si^ega {ci^e|%p:3} w %h:3.

(x)(*) %B:0 zakre^sla w{ twoim | }r^eku{ | %k:1 }kr^otki ^luk gdy b^lyskawicznie wyprowadzone z nadgarstka ci^ecie si^ega{ twoje[go|ej|go] | }%h:1{ | {%p:1} }.

(x){W|%k:0 w}ychyl{asz|a} si^e w {niepewnym/niebezpiecznym/rozpaczliwym} wypadzie dziobi^ac %p:3 sztychem %b:1 w %h:3.

(x)(*) Widz^ac luk^e w{ twojej | }obronie{ | %p:1 }{zdecydowanie/pewnie/mocno} tni{esz|e} %b:4 po %h:5, a jedynie ^zle oceniony dystans nie pozwala {ci|[mu|jej|mu|im|im]} w pe^lni wykorzysta^c impetu.

(x)(*) {U|%k:0 u}stawi{asz|a} parad^e przed{ twoim | }ciosem{ | %p:1 }, lecz w ostatniej chwili obrac{asz|a} %b:3 w nadgarstku pozwalaj^ac {ci|przeciwni[kowi|czce|kowi|kom|czkom]} p^oj^s^c za impetem uderzenia, by {swobodnie/z ^latwo^sci^a/spokojnie/mocno/pot^e^znie} ci^a^c z g^ory w %h:3.

MIECZE_BST
(x) Spokojnie utrzymuj^ac dystans {p|%k:0 p}ozwal{asz|a} by d^lo^n lekko ze^slizgn^e^la si^e po d^lugiej r^ekoje^sci %b:1, po czym trzymaj^ac za jej koniec spina{asz|a} si^e do dalekiego wypad w kierunku %p:1, a {celnym pchni^eciem/oszcz^ednym ci^eciem} %s {ci^e|[go|j^a|to|ich|je]} w %h:3.

(x) (*) {T|%k:0 t}ni{esz|e} b^lyskawicznie %b:4 z ^lokcia nabieraj^ac impetu skr^etem bioder, tak ^ze czubek ostrza trafia {ci^e|%p:3} {idealnie/dok^ladnie/precyzyjnie} w %h:3.

(x) (**) {K|%p:0 k}rokiem w ty^l unik{asz|a}{ twojego | }sko^snego ci^ecia{ | %k:1 }, lecz kolejny {szybki/b^lyskawiczny/natychmiastowy} cios w tej samej linii, lecz tym razem zadany od do^lu sprawia, ^ze %b:3 rozcina {ci|[mu|jej|mu|im|im]} %h:3.

(x) {C|%P:0 c}of{asz|a} si^e p^o^l kroku jednocze^snie niespodziewanie tn^ac {ci^e|%p:3} p^lasko. Wyko^zystuj^ac d^lugo^s^c %b:1 trafi{asz|a} {ci^e|[go|j^a|to|ich|je]} ko^ncem ostrza w %h:3.

(x)(*) Opuszczaj^ac sztych %b:1 {m|%k:0 m}arkuj{esz|e} pchni^ecie, lecz momentalnie podrywaj^ac bro^n tni{esz|e} {ci^e|%p:3} z do^lu po %h:5.

(x)(*) Myl^ac {ci^e|%p:3} g^orn^a fint^a, {|%k:0 }tni{esz|e} sko^snie przez %h:3.

--------------- START------------------

(x) (*) {S|%K:0 s}tar{asz|a} si^e zmyli^c {%p:3|ci^e|%p:3} {kr^otk^a/szybk^a/prost^a} fint^a, a gdy {[on|ona|ono|%p]|ty|[on|ona|ono|%p]} odruchowo zas^lani{a|asz|a} si^e, omij{asz|a} {[jego|jej|%p]|twoj^a|[jego|jej|%p]} zastaw^e p^lynnym p^o^lobrotem tn^ac (lekko |nieznacznie ||mocno |bardzo mocno )%b:1 w %h:3.

(x) (*) {K|%k:0 k}opi{esz|e} {%p:3|ci^e|%p:3} {niespodziewanie/nagle/b^lyskawicznie} w kolano i wykorzystuj^ac u^lamek sekundy gdy {[jego|jej|%p]|twoj^a|[jego|jej|%p]} garda jest nieco {sztywniejsza/opuszczona/s^labsza}, tni{esz|e} {^smia^lo/z bliska/z ^latwo^sci^a} wprost w %h:3 zadaj^ac [mu|jej|%p] (lekkie |nieznaczne |spore |du^ze |bardzo du^ze| pot^e^zne) obra^zenia.

MIECZE_2H

(TODO) (**) {B|%k:0 b}ierz{esz|e} szeroki zamach zza g^lowy jednocze^snie robi^ac krok w przeciwn^a stron^e przez co {cios/ci^ecie} si^egaj^ace {%p:3|ci^e|%p:3} silniej rani {[go|j^a|%p]|ci^e|[go|j^a|%p]} w %h:3 zadaj^ac [mu|jej|%p] (spore |du^ze |bardzo du^ze| pot^e^zne) obra^zenia.

(FIXME) (*) {S|%k:0 s}chodz{isz|i} p^o^lobrotem w bok nabieraj^ac impetu, po czym chwyt{asz|a} %b:3 powy^zej jelca i tak skr^ocon^a broni^a zadaje niespodziewanie szybkie pchni^ecie, kt^ore (lekko |nieznacznie ||mocno |bardzo mocno )rani {%p:3|ci^e|%p:3} w %h:3.

(x) (**) M^lynkuj^ac %b:4 nad g^low^a {s|%k:0 s}tar{asz|a} si^e trafi^c {%p:3|ci^e|%p:3} gdziekolwiek, wreszcie si^egaj^ac niemal przypadkiem ko^ncem ostrza w %h:3 zadaj^ac [mu|jej|%p] (lekkie |nieznaczne |spore) obra^zenia.

(x) (*) {T|%K:0 t}ni{esz|e} p^lasko z {prawej/lewej}, gdy za^s %b:0 zostaje {zablokowan[y|a|%:b] przez %p:3|przez ciebie zablokowan[y|a|%:b]|zablokowan[y|a|%:b] przez %p:3}, nie cofaj^ac broni unos{isz|i} r^ekoje^s^c na wysoko^s^c twarzy i pch{asz|a} z ca^lej si^ly prosto w{ | tw[^oj|oj^a|%p] | }%h:3{ %p:1|| %p:1} zadaj^ac [mu|jej|%p] (spore |du^ze |bardzo du^ze) obra^zenia.

(x, TODO - nada sie do toporow/mlotow) (**) {B|%K:0 b}ierz{esz|e} zamach zza ucha szar^zuj^ac na {%p:3|ciebie|%p:3}. Uderzenie barkiem odrzuca {[go|j^a|%p]|ci^e|[go|j^a|%p]} do ty^lu i nim odzyskuj{e|esz|e} r^ownowag^e {r|%k:0 r}^abi{esz|e} {[go|j^a|%p]|ci^e|[go|j^a|%p]} (mocno|pot^e^znie|z ca^lej si^ly) w %h:3.    

SZABLE
(x) (*) {R|%K:0 r}^abi{esz|e} zza ucha %b:4 {niemal przypadkiem/nie bez pomocy szcz^e^scia} si^egaj^ac {%p:1|ci^e|%p:1} ko^ncem ostrza w %h:3 zadaj^ac w ten spos^ob (lekkie|nieznaczne|spore) obra^zenia.

(x) {Z|%K:0 z} pozoru {spokojnie/po prostu/jedynie} os^laniaj^ac si^e %b:4, nagle prostuj{esz|e} ^lokie^c by samym ko^ncem ostrza ugodzi^c {%p:3|ci^e|%p:3} w %h:3 zadaj^ac w ten spos^ob (lekkie|nieznaczne|spore|du^ze|bardzo du^ze) obra^zenia.

(x) Jakby samym obrotem nadgarstka i lekkim krokiem {k|%k:0 k}ieruj{esz|e} ostrze %b:1 wprost ku{ |twoj[emu|ej|emu|im|im] | }%h:2{%p:1||%p:1} zadaj^ac {[mu|jej|%p]|ci|[mu|jej|%p]} (nieznaczne|drobne|lekkie|mocne|bardzo mocne) obra^zenia.

(x) (*) {P|%K:0 p}ozostawi{asz|a} {^swiadomie|nieopacznie} luk^e w obronie, {a|lecz} gdy {%p:0||%p:0} star{a|asz|a} si^e j^a wykorzysta^c, mi^ekkim skokiem w bok po^l^aczonym z p^o^lobrotem zachodz{isz|i} {[go|j^a|%p]|ci^e|[go|j^a|%p]} z flanki by bez trudno^sci ci^a^c %b:4 w %h:3 zadaj^ac w ten spos^ob (lekkie|nieznaczne|spore|du^ze|bardzo du^ze) obra^zenia.

SZTYLETY
(x) (**) {Z|%K:0 z}amierz{asz|a} si^e do szerokiego ci^ecia, lecz zamiast niego uderzasz {%p:3|ci^e|%p:3} drugim barkiem, a gdy odskakuj{ecie|^a} od siebie, wykr^ec{asz|a} si^e w p^o^lobrocie posy^laj^ac za {[nim|ni^a|%p]|tob^a|[nim|ni^a|%p]} pchni^ecie %b:1 trafiaj^ace w %h:3 zadaj^ac w ten spos^ob (lekkie|nieznaczne|spore|du^ze|bardzo du^ze) obra^zenia. 

(x) Schodz^ac z{ | twojej | }linii ataku{ %p:1|| %p:1}, {r|%k:0 r}ob{isz|i} p^o^lobr^ot ustawiaj^ac si^e do przeciwni[ka|czki|%p] plecami. B^lyskawicznie odwr^ocony ostrzem w d^o^l %b:0 wgryza{| ci|} si^e{ %p:2|| %p:2} w %h:3, a szybki odskok ponownie was rozdziela. 

TOPORY
(x) (*) {R|%K:0 r}^abi{esz|e} z g^ory %b:4 wykorzystuj^ac ci^e^zar broni by przebi^c si^e przez obron^e {%p:1|twoj^a|%p:1}. Niepowstrzymane ostrze si^ega celu i (nieznacznie |lekko ||mocno| bardzo mocno) rani {[go|j^a|%p]|ci^e|[go|j^a|%p]} w %h:3.

(X) (**) Bior^ac zamach %b:4 spod lewego ramienia {|%k:0 }atakuj{esz|e} frontalnie {%p:3|ci�|%p:3}. Ostrze rozchlapuje krew po tym jak wgryza {[mu|jej|%p]|ci|[mu|jej|%p]} si^e ono w %h:3 zadaj^ac (spore|du^ze|bardzo du^ze) obra^zenia.

TOPORY_2H
(**) {O|%K:0 o}brac{asz|a} %b:4 nad g^low^a pozwalaj^ac [mu|jej|%b] nabra^c ogromnego rozp^edu. Przymierzaj^ac si^e w ko^ncu do ciosu puszcz{asz|a} trzonek lew^a r^ek^a przez co bro^n zatacza jeszcze szerszy ^luk by wreszcie uderzy^c wprost w %h:3 %p:1 zadaj^ac (spore|du^ze|bardzo du^ze|ogromne) obrazenia.

{C|%K:0 c}wytaj^ac szeroko stylisko %b:1 wymierz{asz|a} {%p:2|ci|%p:2} cios ko^ncem trzonka, po czym poprawi{asz|a} od razu, tym razem ostrzem, trafiaj^ac {[go|j^a|%p]|ci^e|[go|j^a|%p]} w %h:3 zadaj^ac (spore|du^ze|bardzo du^ze|ogromne) obrazenia.


(*) {R|%K:0 r}obi^ac {kr^otki/szybki/lekki} krok w prz^od pr^obuj{esz|e} pchni^ecia %b:4. Bolesne uderzenie obucha trafia {%p:3|ci^e|%p:3} w %h:3, rani^ac [go|j^a|%p] (dotkliwie|mocno|bardzo mocno).

DRZEWCOWE_D

DRZEWCOWE_K

MACZUGI

MLOTY

MLOTY_2H

MIOTANE

STRZELECKIE





CIOSY SPECJALNE:
Niekt^ore fajne techniki ko^ncz^a si^e np obaleniem, czy wytr^aceniem broni co wp^lyn^e^loby drastycznie na dalszy przebieg walki, lub zadawane s^a w konkretne hitloki, b^ad^x obra^zenia s^a zadane inn^a cz^e^sci^a broni ni^z ostrze. Zatem te co dziwniejsze zebra^lem jako 'ciosy specjalne'. Mo^ze si^e kiedy^s gdzie^s przydadz^a.


MIECZE

MIECZE_BST
{O|%k:0 o}bracaj^ac si^e na lewej nodze schodz{isz|i} z linii ataku %p:1 dostawiasz drug^a nog^e i uderz{asz|a} za siebie g^lowic^a %b:1 mierz^ac w potylic^e przeciwni{ka|ka|czki|k^ow|czek}.

MIECZE_2H

SZABLE
{T|%k:0 t}ni{esz|e} {ci^e|%p:3} sko^snie przez pier^s, po czym robi^ac krok w ty^l bierz{esz|e} szeroki zamach i poprawi{asz|a} prostopadle zostawiaj^ac na {twoim|[jego|jej|jego|ich|ich]} korpusie krwawy krzy^z.

SZTYLETY
{U|%k:0 u}nieruchamiasz{asz|a}{ twoje | }prawe rami^e{ | %p:1 }oplataj^ac je ^lokciem jednocze^snie zadawa^c raz za razem pchni^ecia %b:4 mierz^ac w brzuch i pod pach^e.

{W|%k:0 w}wykonuj{esz|e} lew^a rek^a myl^acy ruch po czym b^lyskawicznie nurkuj{esz|e} pod %m:4 chwytaj^ac obur^acz %p:3 za {lew^a/praw^a} nog^e i przerzuc{asz|a} {ci^e|[go|je|j^a|ich|je]} sobie przez bark. Szybki obr^ot i {celny/precyzyjny/morderczy/bezlitosny} cios %b:1 definitywnie ko^ncz^a starcie.

TOPORY
{Z|%k:0 z}achacz{asz|a} ostrzem %b:1 o{ {tw^oj|twoj^a|twoje} | }%m:3{ | %p:1 }by natychmiast szarpn^a^c mocno, jednocze^snie wykr^ecaj^ac trzonek.{ Twoja | }bro^n{ | %p:1 }wylatuje w powietrze by upa^s^c daleko poza zasi^egiem r^eki.

TOPORY_2H

DRZEWCOWE_D
{N|%K:0 n}ajpierw pozornie celuj^ac w{ tw^oj | }korpus{ | %p:1 }, zmieniasz cel kieruj^ac grot %b:1 mi^edzy {twoje|[jego|jego|jej|ich|ich]} nogi, by silnie podrywaj^ac drzewce pos^la^c {ci^e|przeciwni[ka|ka|czk^e|k^ow|czki]} na ziemi^e. {{ Narazie ten opis odpada }}
DRZEWCOWE_K

MACZUGI

MLOTY

MLOTY_2H

MIOTANE

STRZELECKIE

