/**
 * \file file_name
 *
 * Plik zawieraj�cy opisy cios�w dla r�znych broni.
 *
 * @author Krun, krun@vattghern.com.pl
 * @author Avard, avard@vattghern.com.pl
 * @author Tabakista,opisy
 *
 * @date 8.9.2008 15:20:30
 */

#ifndef D__OPISY_WALKI__D
#define D__OPISY_WALKI__D

#include <files.h>
#include <wa_types.h>

#define OW_PRZESUNIECIE     5

#define OW_PCHNIECIE        W_IMPALE
#define OW_CIECIE           W_SLASH
#define OW_UDERZENIE        W_BLUDGEON
#define OW_WRECZ            (1 << 3)
#define OW_BRUDNA           (1 << 4)
#define OW_MIECZ            (1 << (W_SWORD + OW_PRZESUNIECIE))
#define OW_DRZEWCOWKA       (1 << (W_POLEARM + OW_PRZESUNIECIE))
#define OW_TOPOR            (1 << (W_AXE + OW_PRZESUNIECIE))
#define OW_SZTYLET          (1 << (W_KNIFE + OW_PRZESUNIECIE))
#define OW_MACZUGA          (1 << (W_CLUB + OW_PRZESUNIECIE))
#define OW_MLOT             (1 << (W_WARHAMMER + OW_PRZESUNIECIE))
#define OW_MAX              OW_MLOT

#define OW_OPISZ_CIOS(a, b, i, h, d, p)                                                     \
                        OPISY_WALKI->opisz_cios(a, b, i, h, d, p)

#define OW_OPISZ_WYPAROWANIE(a, b, i, h, d, z)                                              \
                        OPISY_WALKI->opisz_wyparowanie(a, b, i, h, d, z)

#define OW_OPISZ_TARCZOWNICTWO(a, b, i, h, d, t)                                            \
                        OPISY_WALKI->opisz_tarczownictwo(a, b, i, h, d, t)

#define OW_OPISZ_PAROWANIE(a, b, i, h, d, p)                                                \
                        OPISY_WALKI->opisz_parowanie(a, b, i, h, d, p)

#define OW_OPISZ_UNIK(a, b, i, h, d)                                                        \
                        OPISY_WALKI->opisz_unik(a, b, i, h, d)

#endif D__OPISY_WALKI__D