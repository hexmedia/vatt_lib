#pragma no_clone
#pragma no_inherit
#pragma save_binary

/*

Staropolskie miary pojemno�ci cia� sypkich

- �aszt = 30 korc�w = 60 p�korc�w = 120 �wierci = 240 miarek = 960 garncy = 3840 kwart = 15 360 kwaterek = 3618 litr�w
- Korzec = 2 p�korce = 4 �wierci = 8 miarek = 32 garnce = 128 kwart = 512 kwaterek = 120,6 litra
- P�korzec = 2 �wierci = 4 miarki = 16 garncy = 64 kwarty = 256 kwaterek = 60,3 litra
- �wier� warszawska = 2 miarki = 8 garncy = 32 kwarty = 128 kwaterek = 30,151 litra
- Miarka (faska, miara) = 4 garnce = 16 kwart = 64 kwaterek = 15,07 litra
- Garniec = 4 kwarty = 16 kwaterek = 3,7689 litra
- Kwarta = 4 kwaterki = 0,9422 litra
- Kwaterka = 0,2356 litra

Staropolskie miary pojemno�ci p�yn�w

- Beczka = 2 p�beczki = 14,4 konwi = 72 garnce = 144 p�garncy = 288 kwarty = 1152 kwaterki = 271,36 litra - w systemie miar wprowadzonym w 1764 roku. Wcze�niej r�nie w zale�no�ci od regionu i rodzaju p�ynu, np. beczka miodu ok. 48 l, beczka piwa mi�dzy 130 a 160 l, wina 130 - 160 l.
- P�beczka = 36 garncy = 144 kwarty = 576 kwaterek = 136,68 litra
- Anta� = �wier� beczki, w zale�no�ci od regionu od 35 do 90 litr�w
- Achtel = �sma cz�� beczki
- Bary�a - ok. 70 litr�w
- Konew = 5 garncy = 20 kwart = 80 kwaterek = 18,845 litra
- Garniec = 4 kwarty = 16 kwaterek = 3,7689 litra
- P�garniec = 2 kwarty = 8 kwaterek = 1,88445 litra
- Kwarta = 4 kwaterki = 0,9422 litra
- Kwaterka = 0,2356 litra

Staropolskie miary masy

- Szyffunt (funt morski) = 2,6 centnara = 13 kamieni = 416 funt�w = 832 grzywny = 13 312 �uty = 168,57 kg
- Centnar (cetnar) = 5 kamieni = 160 funt�w = 320 grzywny = 5120 �ut�w = 64,836 kg
- Kamie� = 32 funty = 64 grzywny = 1024 �ut�w = 12,967 kg
- Funt warszawski = 2 grzywny = 32 �uty = 0,4052 kg
- Grzywna (marka) = 16 �ut�w = 0,2026 kg
- �ut = 12,66 g

*/
