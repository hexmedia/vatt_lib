/**
 * Obiekt z funkcjami s�u��cymi do kolorowania
 * wszelkiego rodzaju rzeczy,
 * Autor: Krun
 * Data: 3.07.2007
 */

#include <colors.h>
#include <options.h>

#define ILOSC_KOLOROW   6
#define KOLOR1          set_color(COLOR_FG_GREEN, COLOR_BOLD_ON)
#define KOLOR2          set_color(COLOR_FG_GREEN)
#define KOLOR3          set_color(COLOR_FG_YELLOW)
#define KOLOR4          set_color(COLOR_FG_YELLOW, COLOR_BOLD_ON)
#define KOLOR5          set_color(COLOR_FG_RED)
#define KOLOR6          set_color(COLOR_FG_RED, COLOR_BOLD_ON)

string
koloruj(string text, int ilosc_kolorow, int nr_koloru)
{
    if(ilosc_kolorow == ILOSC_KOLOROW)
    {
        switch(nr_koloru)
        {
            case 1:
                return KOLOR1 + text + clear_color();
            case 2:
                return KOLOR2 + text + clear_color();
            case 3:
                return KOLOR3 + text + clear_color();
            case 4:
                return KOLOR4 + text + clear_color();
            case 5:
                return KOLOR5 + text + clear_color();
            case 6:
                return KOLOR6 + text + clear_color();
            default:
                return text;
        }
    }
    else if(ilosc_kolorow == ftoi(itof(ILOSC_KOLOROW)/2.0 + 0.99))
    {
        switch(nr_koloru)
        {
            case 1:
                return KOLOR2 + text + clear_color();
            case 2:
                return KOLOR4 + text + clear_color();
            case 3:
                return KOLOR6 + text + clear_color();
            default:
                return text;
        }
    }
    return text;
}

/**
 * Koloruje r�ne poziomy r�nych rzeczy:)
 * @param str tekst do pokolorwania
 * @param level poziom, z kt�rym mamy do czynienia(zaczynamy od 0)
 * @param ilosc ilo�� poziom�w
 * @param p czy najwy�szy poziom jest pierwszy
 * @param op kt�re poziomy zosta�y opuszczone(zwykle 1 lub ostatni).
 * @return pokolorowany string
 */
string
koloruj_poziom(string str, int level, int ilosc, int p=1, int *op=({}))
{
    //je�li od ko�ca to odwracamy liczbe
    if(!p)
    {
        level = ilosc-level;
        level++;
    }

    if(!level)
        return str;

    if((!sizeof(op) && level == 1) || level > ilosc)
        return str;

    int ilosc_kolorow = ILOSC_KOLOROW;

    //Pierwszego poziomu raczej nie kolorujemy, wi�c opuszamy go
    ilosc--;
    level--;

    if(ilosc < ilosc_kolorow)
    {
        ilosc_kolorow = ftoi(itof(ilosc_kolorow) / 2.0 + 0.99);
    }

    //Co� trzeba zrobi� je�li mamy nier�wn� liczb� leveli..
    //wi�c opuszczamy kilka w �rodku, tak coby jednak na ko�cu by� 
    //jasny czerwony.
    int a = ftoi(itof(ilosc) / itof(ilosc_kolorow) + 0.99);
    int mod = ilosc%ilosc_kolorow;

    if(mod)
    {
        int ilosc_opuszczanych = ilosc_kolorow - mod;
        int poc_op = ftoi(itof(ilosc) / 2.0 + 0.99) - ftoi((itof(ilosc_opuszczanych))/2.0 + 0.99);

        while(ilosc_opuszczanych--)
            op += ({poc_op+=a});
    }

    foreach(int o : clean_array(op))
        if(o >= level)
            level++;

    level = max(1, min(ilosc, level));

    int kol = ftoi(itof(level)/itof(a) + 0.99);

    return koloruj(str, ilosc_kolorow, kol);
}

/**
 * Funkcja pomocnicza do kolorwania tekst�w wysy�anych do wi�kszej ilo�ci
 * graczy.
 */
varargs string
set_color_vbfc(mixed color, mixed back=0, mixed rest=0)
{
    object fob;
    fob = previous_living(-1);
    if(!fob->query_option(OPT_COLORS))
        return "";

    if(stringp(color))
        color = atoi(color);
    if(stringp(back))
        back = atoi(back);
    if(stringp(rest))
        rest = atoi(rest);

    if(back && rest)
        return set_color(fob, color, back, rest);
    else if(!rest)
        return set_color(fob, color, back);
    else if(!back)
        return set_color(fob, color, rest);
    else
        return set_color(fob, color);
}

/**
 * Funkcja pozwala na pokolorowanie element�w tablicy na dowolny kolor
 * @param arr tablica do pokolorowania
 * @param col kolor na jaki pokolorowana ma zosta� tablica
 * @param bg kolor tla(opcjonalnie)
 * @param opt dodatkowa opcje kolorow(opcjonalnie)
 */
public mixed *
koloruj_tablice(string *arr, int col, int bg=0, int opt=0)
{
    int i;
    arr = secure_var(arr);
    for(i=0;i<sizeof(arr);i++)
    {
        arr[i] = set_color(col, bg, opt) + arr[i] + clear_color();
    }
    return arr;
}

