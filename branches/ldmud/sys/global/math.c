/*
 * /sys/global/math.c
 *
 * Some useful math routines.
 */

#pragma no_clone
#pragma no_inherit
#pragma no_shadow
#pragma save_binary
#pragma strict_types

#include <macros.h>

/*
 * Function name: find_exp
 * Description  : Find the solution to 2^x = sum.
 * Arguments    : sum - The sum to find the solution to.
 * Returns      : x
 */
public nomask int
find_exp(int sum)
{
    int x, i;

    x = 1;
    while (sum > x)
    {
        i++;
        x += x;
    }

    return i;
}

/*
 * Global variable used in the function quick_find_exp().
 */
static private mapping lookup =
    ([ 2:1, 4:2, 8:3, 16:4, 32:5, 64:6, 128:7, 256:8, 512:9 ]);

/*
 * Function name: quick_find_exp
 * Description  : This function is a quick hack that returns the solution
 *                to the function 2^x = sum for a rather limited set of
 *                sum. Accepted values are the powers of 2 ranging from 1
 *                to 512.
 * Arguments    : int sum - the sum to find x for.
 * Returns      : int - the matching x, else 0.
 */
public nomask int
quick_find_exp(int sum)
{
    return lookup[sum];
}

/*
 * Function name: decimal_to_binary
 * Description  : Convert a base 10 unsigned integer to its base 2
 *                representation.
 * Arguments    : num: The number to convert.
 * Returns      : A binary array with the base 2 number, MSB first.
 */
public nomask int *
decimal_to_binary(int num)
{
    int *bit_array = ({ });

    /* Should we also check for MAXINT here? */
    if (num <= 0)
    {
        return ({ 0 });
    }

    /* Put more significant bits on top of the array */
    while(num)
    {
        bit_array = ({ (num % 2) }) + bit_array;

        num /= 2;
    }

    return bit_array;
}

/*
 * Function name: binary_to_decimal
 * Description  : Convert a base 2 unsigned integer to its base 10
 *                representation.
 * Arguments    : A binary array of the base 2 number, MSB first.
 * Returns      : The unsigned decimal integer.
 */
public nomask int
binary_to_decimal(int *bit_array)
{
    int num = 0;
    int bit = -1;
    int size = sizeof(bit_array);

    while(++bit < size)
    {
        num += num + bit_array[bit];
    }

    return num;
}

/*
 * Function name: square_root
 * Description  : Find the x for which x^2 = y, the square root
 *                If y < 0, x will be 0
 * Arguments    : square - the square to find the root to
 * Returns      : int - the root
 */
public nomask int
square_root(int square)
{
    if(square <= 0)
        return 0;

    return ftoi(pow(itof(square), 0.5));
}

#define PRIMES   ({ 3, 7, 13, 17, 23, 37, 43, 47, 53, 67, 73 })
#define NAME_LEN 11
#define CHAR_a   ('a' - 1)

/*
 * Function name: name_to_random
 * Description  : With this function, each combination of name, seed and
 *                range will return the same value each time you call it.
 *                This way, for quests or other hints, the same player
 *                gets the same value each time.
 * Arguments    : string name  - the lower case name of the player
 *                int    seed  -
 *                int    range - the range to compute the number in
 * Returns      : int          - the randomized value.
 */
public nomask int
name_to_random(string name, int seed, int range)
{
    int index  = -1;
    int result = 0;
    int size;
    int char;

    /* Do not ever believe people follow your instructions. */
    name = lower_case(name);

    /* Player names cannot be larger than NAME_LEN characters, though
     * people may feed us with other strings.
     */
    if (strlen(name) > NAME_LEN)
        name = extract(name, 0, (NAME_LEN - 1));

    /* To find the result, we apply the same formula to each character
     * of the name, in addition with its rank, the seed and the first
     * primes ending with 3 or 7.
     */
    size = strlen(name);
    while(++index < size)
    {
        char = name[index] - CHAR_a;
        result += ((seed / char) + (char * PRIMES[NAME_LEN - index - 1]) +
            (seed % (index + 1)));
    }

    /* Reduce the result to a number in the range 0 .. (range - 1). */
    return (result % range);
}

/**
 * Przelicza liczby rzymskie na liczby arabskie.
 * Funkcja obs�uguje '|' nie obs�uguje jednak mno�enia przez 1000(wikipedia:liczby rzymskie).
 *
 * @param rzym Liczba rzymska do zamiany na arabska.
 *
 * @return Liczba arabska odpowiadaj�ca warto�ci� liczbie rzymskiej.
 *
 * @author Krun
 */
int
rzym_to_i(string rzym)
{
    int i, val, old_val, old_mn_val, tmp_val, mn, mn_val;
    mapping wartosci = (["I":1, "V":5, "X":10, "L":50, "C":100, "D":500, "M":1000]);

    rzym = upper_case(rzym);

    for(i=(strlen(rzym)-1);i>=0;i--)
    {
        if(rzym[i..i] == "|")
        {
            if(mn)
            {
                mn = 0;
                tmp_val = mn_val*100;
                /*if(old_mn_val > (tmp_val=mn_val*100))
                            val -= tmp_val;
                else        //Ble... Ktos mi wytlumaczy dlaczego to dziala jak dziala, a nie tak jak ja mysle?:P
                        val += tmp_val;*/
                val += tmp_val;
                old_val = tmp_val;
                mn_val = 0;
            }
            else
                mn = 1;

            continue;
        }
        tmp_val = wartosci[rzym[i..i]];
        if(mn)
        {
            if(old_mn_val > tmp_val)
                mn_val -= tmp_val;
            else
                mn_val += tmp_val;
            old_mn_val = tmp_val;
            continue;
        }
        if(old_val > tmp_val)
            val -= tmp_val;
        else
            val += tmp_val;
        old_val = tmp_val;
    }
    return val;
}

/**
 * Konwertuje liczby arabskie do rzymskich.
 * Nie wiem kto jest autorem, skrytp z netu, nie analizowa�em jak dzia�a ale dzia�a.
 *
 * @param n - liczba arabska
 *
 * @return string z odpowiadaj�c� jej liczb� rzymsk�.
 */
public string i_to_rzym(int n)
{
    int a, b, o, i, no = n;
    string c = "IVXLCDM", s;

    for(a=5, b=0 ; n ; b++ , a ^= 7)
    {
        for(o = n % a , n = n / a ^ 0 ; o-- ;)
        {
            i = (o > 2 ? b + n -(n &= ~1) + (o = 1) : b);
            s = c[i..i] + (s ?: "");
        }
    }

    return s;
}


/**
 * Przekszta�ca dane prawdopodobie�stwa tak,
 * aby ich suma by�a r�wna 1.
 *
 * @example
 * Mamy dane prawdopodobienstwa 0.2 i 0.3
 * wynikiem funkcji beda liczby 0.4 i 0.6 - tak
 * wiec stosunek liczba jest taki sam, a ich suma rowna 1.
 *
 * @param probs Tablica przekszta�canych prawdopodobie�stw
 *
 * @return Tablica z unormalizowanymi prawdopodobie�stawmi
 *
 * @author Rantaur
 */
float *normalize_probability(float *probs)
{
    int size = sizeof(probs);

    if(!probs || !size)
        return 0;

    float suma = 0.0;

    for(int i = 0; i < size; i++)
        suma += probs[i];

    if(suma == 1.0 || suma == 0.0)
        return probs;

    float W = 1.0/suma;
    float *ret = ({ });

    for(int i = 0; i < size; i++)
        ret += ({W*probs[i]});

    return ret;
}

/**
 * Losuje ze zbioru jedna wartosc biorac pod uwage podany
 * rozklad prawdopodobienstw.
 *
 * @param val_prob  Mapping zawierajacy elementy i odpowiadajace im
 *                  prawdopodobienstwa
 * @param dokl      Ile miejsc po przecinku uwzgledniamy (dla zapisu
 *                  prawdopodobienstwa w postaci P = 0.004). Standardowo 3.
 *
 * @return Wylosowany element
 *
 * @author Rantaur
 */
varargs mixed prob_random(mapping val_probs, int dokl=3)
{
    mixed *values = m_indexes(val_probs);

    float *probs = m_values(val_probs);
    probs = normalize_probability(probs);

    int size = sizeof(probs);
    int *tmp = allocate(size);

    float ddokl = pow(10.0, itof(dokl));

    int losowa = random(ftoi(ddokl))+1;

    tmp[0] = ftoi(ddokl*probs[0]);

    if(losowa > 0 && losowa <= tmp[0])
        return values[0];

    for(int i = 1; i < size; i++)
    {
        if(i == (size - 1))
            tmp[i] = ftoi(ddokl);
        else
            tmp[i] = tmp[i-1]+ftoi(ddokl*probs[i]);

        if(losowa > tmp[i-1] && losowa <= tmp[i])
            return values[i];
    }

    return 0;
}

/**
 * Oblicza wartosc funkcji liniowej y=ax+b
 * na podstawie 2 podanych punktow i zadanego argumentu
 *
 * @author Rantaur
 */
float linear_func(float x1, float y1, float x2, float y2, float arg)
{
    float c = (x1-x2);

    if(!c || c == 0.0)
        c = 1.0;

    float a = (y1-y2)/c;
    float b = y1-a*x1;

    return (a*arg+b);
}
