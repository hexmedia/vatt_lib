// Na razie to tylko przymiarka

#pragma no_clone
#pragma no_inherit
#pragma no_shadow
#pragma save_binary
#pragma struct_types

#include <wa_types.h>
#include <composite.h>
#include <pl.h>

mixed Sloty = ({
    TS_HEAD,
    TS_NECK,
    TS_L_SHOULDER,
    TS_R_SHOULDER,
    TS_SHOULDERS,
    TS_CHEST,
    TS_STOMACH,
    TS_CLOAK,
    TS_L_ARM,
    TS_R_ARM,
    TS_ARMS,
    TS_L_FOREARM,
    TS_R_FOREARM,
    TS_FOREARMS,
    TS_L_HAND,
    TS_R_HAND,
    TS_HANDS,
    TS_HIPS,
    TS_L_THIGH,
    TS_R_THIGH,
    TS_THIGHS,
    TS_L_SHIN,
    TS_R_SHIN,
    TS_SHINS,
    TS_L_FOOT,
    TS_R_FOOT,
    TS_FEET,
    TS_L_FINGER,
    TS_R_FINGER,
    TS_FINGERS,
    TS_WAIST,
    TS_TORSO
    });

mixed Odmiany = ([
    TS_HEAD : ({ "g�owa", "g�owy", "g�owie", "g�ow�", "g�ow�", "g�owie" }),
    TS_NECK : ({ "szyja", "szyi", "szyi", "szyj�", "szyj�", "szyi" }),
    TS_L_SHOULDER : ({ "lewy bark", "lewego barku", "lewemu barkowi", "lewy bark",
        "lewym barkiem", "lewym barku" }),
    TS_R_SHOULDER : ({ "prawy bark", "prawego barku", "prawemu barkowi", "prawy bark",
        "prawym barkiem", "prawym barku" }),
    TS_SHOULDERS : ({ "barki", "bark�w", "barkom", "barki", "barkami", "barkach" }),
    TS_CHEST : ({ "klatka piersiowa", "klatki piersiowej", "klatce piersiowej",
        "klatk� piersiow�", "klatk� piersiow�", "klatce piersiowej" }),
    TS_STOMACH : ({ "brzuch", "brzucha", "brzuchowi", "brzuch", "brzuchem", "brzuchu" }),
    TS_CLOAK : ({ "plecy", "plec�w", "plecom", "plecy", "plecami", "plecach" }),
    TS_L_ARM : ({ "lewe rami�", "lewego ramienia", "lewemu ramieniu", "lewe rami�",
        "lewym ramieniem", "lewym ramieniu" }),
    TS_R_ARM : ({ "prawe rami�", "prawego ramienia", "prawemu ramieniu", "prawe rami�",
        "prawym ramieniem", "prawym ramieniu" }),
    TS_ARMS : ({ "ramiona", "ramion", "ramionom", "ramiona",
        "ramionami", "ramionach" }),
    TS_L_FOREARM : ({ "lewe przedrami�", "lewego przedramienia", "lewemu przedramieniu",
        "lewe przedrami�", "lewym przedramieniem", "lewym przedramieniu" }),
    TS_R_FOREARM : ({ "prawe przedrami�", "prawego przedramienia", "prawemu przedramieniu",
        "prawe przedrami�", "prawym przedramieniem", "prawym przedramieniu" }),
    TS_FOREARMS : ({ "przedramiona", "przedramion", "przedramionom", "przedramiona",
        "przedramionami", "przedramionach" }),
    TS_L_HAND : ({ "lewa d�o�", "lewej d�oni", "lewej d�oni", "lew� d�o�",
        "lew� d�oni�", "lewej d�oni" }),
    TS_R_HAND : ({ "prawa d�on", "prawej d�oni", "prawej d�oni", "praw� d�o�",
        "praw� d�oni�", "prawej d�oni" }),
    TS_HANDS : ({ "d�onie", "d�oni", "d�oniom", "d�onie", "d�o�mi", "d�oniach" }),
    TS_HIPS : ({ "biodra", "bioder", "biodrom", "biodra", "biodrami", "biodrach" }),
    TS_L_THIGH : ({ "lewe udo", "lewego uda", "lewemu udu", "lewe udo",
        "lewym udem", "lewym udzie" }),
    TS_R_THIGH : ({ "prawe udo", "prawego uda", "prawemu udu", "prawe udo",
        "prawym udem", "prawym udzie" }),
    TS_THIGHS : ({ "uda", "ud", "udom", "uda", "udami", "udach" }),
    TS_L_SHIN : ({ "lewa gole�", "lewej goleni", "lewej goleni",
        "lew� gole�", "lew� goleni�", "lewej goleni" }),
    TS_R_SHIN : ({ "prawa gole�", "prawej goleni", "prawej goleni",
        "praw� gole�", "praw� goleni�", "prawej goleni" }),
    TS_SHINS : ({ "golenie", "goleni", "goleniom", "golenie", "goleniami", "goleniach" }),
    TS_L_FOOT : ({ "lewa stopa", "lewej stopy", "lewej stopie", "lew� stop�",
        "lew� stop�", "lewej stopie" }),
    TS_R_FOOT : ({ "prawa stopa", "prawej stopy", "prawej stopie", "praw� stop�",
        "praw� stop�", "prawej stopie" }),
    TS_FEET : ({ "stopy", "st�p", "stopom", "stopy", "stopami", "stopach" }),
    TS_L_FINGER : ({ "serdeczny palec lewej r�ki", "serdecznego palca lewej r�ki",
            "serdecznemu palcowi lewej r�ki", "serdeczny palec lewej r�ki",
            "serdecznym palcem lewej r�ki", "serdecznym palcu lewej r�ki" }),
    TS_R_FINGER : ({ "serdeczny palec prawej r�ki", "serdecznego palca prawej r�ki",
            "serdecznemu palcowi prawej r�ki", "serdeczny palec prawej r�ki",
            "serdecznym palcem prawej r�ki", "serdecznym palcu prawej r�ki" }),
    TS_FINGERS : ({ "serdeczne palce", "serdecznych palc�w",
            "serdecznym palcom", "serdeczne palce",
            "serdecznymi palcami", "serdecznych palcach" }),
    TS_WAIST : ({ "pas", "pasa", "pasowi", "pas", "pasem", "pasie" }),
    TS_TORSO : ({ "tu��w", "tu�owia", "tu�owiowi", "tu��w", "tu�owiem", "tu�owiu" })
    ]);


void
create()
{
    mixed graf = ([ ]);

    foreach(int i : Sloty)
    {
        graf[i] = ({ });

        foreach(int j : Sloty)
            if(i != j && (i & j) == j)
                graf[i] += ({ j });
    }

    Sloty = topological_sort_mapping(Sloty, graf);
}

public string
slot_name(int slot, int przyp = PL_MIA)
{
    return Odmiany[slot][przyp];
}

public string
composite_slots(int slots, int przyp = PL_MIA)
{
    string *words = ({ });

    foreach(int i : Sloty)
        if((slots & i) == i)
        {
            slots -= i;
            words += ({ Odmiany[i][przyp] });
        }

    return COMPOSITE_WORDS(words);
}
