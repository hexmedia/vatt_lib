/**
 * Plik obs�uguje konwersj� date z kalendarza gregoria�skiego do daty z
 * kalendarza ludzkiego a tym samym synchronizuje j� do daty ksie�ycowej.
 *
 * Skrypt jest wyrwany z programu hebcal i przepisany na lpc przez
 * Krun'a.
 *
 *     Nasze miesi�ce zaczynaj� sie troche inaczej ni�
 * te �ydowskie, �eby to si� troche bardziej zje�drza�o
 * z kalendarzem gregoria�skim, dlatego Tewet, kt�ry
 * jest miesi�cem na prze�omie gregoria�skiego grudnia i
 * stycznia obejmuje rol� naszego pierwszego miesi�ca roku:)
 *
 *
 * @author Krun
 * @date 04.04.2008
 */

#pragma no_clone
#pragma no_reset
#pragma no_inherit
#pragma save_binary
#pragma strict_types

#include <time.h>

#define NISAN       1
#define IYYAR       2
#define SIVAN       3
#define TAMUZ       4
#define AV          5
#define ELUL        6
#define TISHREI     7
#define CHESHVAN    8
#define KISLEV      9
#define TEVET       10
#define SHVAT       11
#define ADAR_I      12
#define ADAR_II     13

#define LEAP_YR_HEB(x)      ((1 + (x) * 7) % 19 < 7 ? 1 : 0)
#define MONTHS_IN_HEB(x)    (LEAP_YR_HEB(x) ? 13 :12)

#define MONTH_LENGHT(m, y)              \
    ({31, (PRZESTEPNY(y) ? 29 : 28), 31, 30, 31, 30, 31, 31, 30, 31, 30, 31})[m]

/**
 ** FUNKCJE ODPOWIEDZIALNE ZA KALENDARZ �YDOWSKI
 **/

static int hebrew_elapsed_days(int year)
{
    int m_elapsed, p_elapsed, h_elapsed, parts, day, alt_day;

    m_elapsed = 235 * ((year - 1) / 19) +
        12 * ((year - 1) % 19) +
        ((((year - 1) % 19) * 7) + 1) / 19;

    p_elapsed = 204 + (793 * (m_elapsed % 1080));

    h_elapsed = 5 + (12 * m_elapsed) +
        793 * (m_elapsed / 1080) +
        p_elapsed / 1080;

    parts = (p_elapsed % 1080) + 1080 * (h_elapsed % 24);

    day = 1 + 29 * m_elapsed + h_elapsed / 24;

    if ((parts >= 19440) ||
        ((2 == (day % 7)) && (parts >= 9924) && !(LEAP_YR_HEB(year))) ||
        ((1 == (day % 7)) && (parts >= 16789) && LEAP_YR_HEB(year - 1)))
        alt_day = day + 1;
    else
        alt_day = day;

    if ((alt_day % 7) == 0 ||
        (alt_day % 7) == 3 ||
        (alt_day % 7) == 5)
        return alt_day + 1;
    else
        return alt_day;
}

/* Number of days in the hebrew YEAR */
int days_in_heb_year( int year )
{
    return (hebrew_elapsed_days(year + 1) - hebrew_elapsed_days(year));
}

/* true if Cheshvan is long in hebrew YEAR */
int long_cheshvan( int year )
{
    return((days_in_heb_year(year) % 10) == 5);
}

/* true if Cheshvan is long in hebrew YEAR */
int short_kislev( int year )
{
    return ((days_in_heb_year(year) % 10) == 3);
}

int max_days_in_heb_month(int month, int year)
{
    if ( month == IYYAR ||
         month == TAMUZ ||
         month == ELUL ||
         month == TEVET ||
         month == ADAR_II ||
         (month == ADAR_I && !LEAP_YR_HEB( year )) ||
         (month == CHESHVAN && !long_cheshvan( year )) ||
         (month == KISLEV && short_kislev( year )))
        return 29;
    else
        return 30;
}

int max_days_in_ludz_month(int month, int year)
{
    int yl = LEAP_YR_HEB(year) + 12;

    month += 10;

    if(month > yl)
    {
        year++;
        month -= yl;
    }

    return max_days_in_heb_month(month, year);
}

int hebrew2abs(int dzien, int miesiac, int rok)
{
    int m;
    int tempabs = dzien;
    int ret;

    if (miesiac < TISHREI)
    {
        for (m = TISHREI; m <= MONTHS_IN_HEB (rok); m++)
            tempabs += max_days_in_heb_month(m, rok);

        for (m = NISAN; m < miesiac ; m++)
            tempabs += max_days_in_heb_month(m, rok);
    }
    else
    {
        for (m = TISHREI; m < miesiac; m++)
            tempabs += max_days_in_heb_month (m, rok);
    }


    ret = hebrew_elapsed_days(rok) - 1373429 + tempabs;

    return ret;
}

int *abs2greg(int theDate);

int *abs2ludz(int d, int przesuniecie)
{
    int *mmap =
        ({
            KISLEV, TEVET, SHVAT, ADAR_I, NISAN,
            IYYAR, SIVAN, TAMUZ,
            TISHREI, TISHREI, TISHREI, CHESHVAN
        });

    int *hebdate = allocate(3), *gregdate = allocate(3);

    int day, month, year;

    if( d >= 10555144 )
    {
        throw("Argument do abs2hebrew " + d + " poza zakresem.\n");
        return 0;
    }

    gregdate = abs2greg (d);
    hebdate[0] = 1;
    hebdate[1] = 7;
    year = 3760 + gregdate[2];

    while (hebdate[2] = year + 1,
           d >= hebrew2abs (hebdate[0], hebdate[1], hebdate[2]))
        year++;

    if( year >= 4635 && year < 10666  )
    {
        /* optimize search */
        month = mmap[gregdate[1] - 1];
    }
    else
    {
        /* we're outside the usual range, so assume nothing about hebrew/gregorian calendar drift... */
        month = TISHREI;
    }

    while (hebdate[1] = month,
           hebdate[0] = max_days_in_heb_month(month, year),
           hebdate[2] = year,
           d > hebrew2abs(hebdate[0], hebdate[1], hebdate[2]))
        month = (month % MONTHS_IN_HEB (year)) + 1;

    hebdate[0] = 1;

    day = (int) (d - hebrew2abs(hebdate[0], hebdate[1], hebdate[2]) + 1);
    if(day < 0)
    {
        throw("Cu� nie tak:P\n");
        return 0;
    }

    hebdate[0] = day;

    //Dodajemy orginalne warto�ci.
    hebdate += ({hebdate[1], hebdate[2]});

    //Robimy przesuni�cie z kalendarza �ydowskiego do mudowego kalendarza ludzkiego.
    hebdate[1] -= 10;

    if(hebdate[1] < 0)
        hebdate[1] += 12 + LEAP_YR_HEB(hebdate[2]);

    hebdate[2] -= przesuniecie;

    //Dni zostawiamy, tylko miesi�ce i lata si� zmieniaja, czyli w miesi�cach mamy dni tak jak w
    //orginalnych miesi�cach w

    return hebdate;
}

int yd2abs(int dr, int r);

int *dr2ludz(int dr, int r, int przesuniecie)
{
    int abs = yd2abs(dr, r);

    return abs2ludz(abs, przesuniecie);
}

/**
 ** FUNKCJE ODPOWIEDZIALNE ZA KALENDARZ GREGORIA�SKI
 **/
int dayOfYear(int dzien, int miesiac, int rok)
{
    int dOY = dzien + 31 * (miesiac - 1);
    if (miesiac > 2)
    {
        dOY -= (4 * miesiac + 23) / 10;
        if (PRZESTEPNY(rok))
            dOY++;
    }
    return dOY;
}

int yd2abs(int dr, int r)
{
    return ((dr + 365 * r - 365) + ((r - 1) / 4 - (r - 1) / 100 + (r - 1) / 400));
}

int greg2abs(int dzien, int miesiac, int rok)
{
    return yd2abs(dayOfYear(dzien, miesiac, rok), rok);
}

int *abs2greg( int theDate )
{
    int day, year, month, mlen;
    int rd, rm, rr;
    int d0, n400, d1, n100, d2, n4, d3, n1;

    d0      = theDate - 1;
    n400    = d0 / 146097;
    d1      = d0 % 146097;
    n100    = d1 / 36524;
    d2      = d1 % 36524;
    n4      = d2 / 1461;
    d3      = d2 % 1461;
    n1      = d3 / 365;

    day     = ((d3 % 365) + 1);
    year    = (400 * n400 + 100 * n100 + 4 * n4 + n1);

    if (4 == n100 || 4 == n1)
    {
        rm = 12;
        rd = 31;
        rr = year;

        return ({rd, rm, rr});
    }
    else
    {
        year++;
        month = 1;

        while ((mlen = MONTH_LENGHT(month-1, year)) < day)
        {
            day -= mlen;
            month++;
        }

        rr = year;
        rm = month;
        rd = day;

        return ({rd, rm, rr});
    }
}