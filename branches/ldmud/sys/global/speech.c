#pragma no_clone
#pragma no_inherit
#pragma no_shadow
#pragma save_binary
#pragma strict_types

#include <pl.h>
#include <macros.h>
#include <stdproperties.h>

/* Nazwa : ftlumacz()
 * Arg   : tekst - string
 * Opis  : Zamienia podanego stringa
 *          na tekst po pijaku. :-)
 * Zwraca: string.
 *
 * 15:04 2005-10-04
 *  By Folkien
 * Mocno zmodyfikowany by Delvert ;)
 */

mapping zamiana = ([
        "a" : ({ "a", "aa", "ay" }),
        "b" : ({ "b" }),
        "c" : ({ "ts", "c" }),
        "d" : ({ "d", "t", "d" }),
        "e" : ({ "e", "ee" }),
        "f" : ({ "f", "fs" }),
        "g" : ({ "g", "gh", "k" }),
        "h" : ({ "h" }),
        "i" : ({ "i", "ij", "j" }),
        "j" : ({ "j", "yj", "ji" }),
        "k" : ({ "kh", "k" }),
        "l" : ({ "hl", "l" }),
        "m" : ({ "m" }),
        "n" : ({ "n", "nn" }),
        "o" : ({ "o", "ou" }),
        "p" : ({ "p", "pp", "p" }),
        "q" : ({ "kiu", "kj", "ku" }),
        "r" : ({ "r", "gr" }),
        "s" : ({ "si", "ss" }),
        "t" : ({ "tf", "t" }),
        "u" : ({ "u" }),
        "v" : ({ "fa", "ff", "fu" }),
        "w" : ({ "f", "ff" }),
        "x" : ({ "x", "yks", "ks" }),
        "y" : ({ "y" }),
        "z" : ({ "s", "ssz", "ss" }),
        "A" : ({ "A", "AA", "AY" }),
        "B" : ({ "B" }),
        "C" : ({ "TS", "C" }),
        "D" : ({ "D", "T", "D" }),
        "E" : ({ "E", "EE" }),
        "F" : ({ "F", "FS" }),
        "G" : ({ "G", "GH", "K" }),
        "H" : ({ "H" }),
        "I" : ({ "I", "IJ", "J" }),
        "J" : ({ "J", "YJ", "JI" }),
        "K" : ({ "KH", "K" }),
        "L" : ({ "HL", "L" }),
        "M" : ({ "M" }),
        "N" : ({ "N", "NN" }),
        "O" : ({ "O", "OU" }),
        "P" : ({ "P", "PP", "P" }),
        "Q" : ({ "KIU", "KJ", "KU" }),
        "R" : ({ "R", "L", "L", "GR" }),
        "S" : ({ "SS", "SI", "SS" }),
        "T" : ({ "TF", "TT", "T" }),
        "U" : ({ "U" }),
        "V" : ({ "FA", "FF", "FU" }),
        "W" : ({ "F", "FF" }),
        "X" : ({ "X", "YKS", "KS" }),
        "Y" : ({ "Y" }),
        "Z" : ({ "S", "SSZ", "SS" }),
        "1" : ({ "jedn", "rass", "jet", "jetn" }),
        "2" : ({ "df", "f", "dfa", "tfa" }),
        "3" : ({ "tsi", "tsy", "ts", "t�" }),
        "4" : ({ "cit", "ciczt", "ery", "cte" }),
        "5" : ({ "pi�", "pi��", "pi", "pi�", "pp" }),
        "6" : ({ "ss", "sii", "ses", "s��", "se�ci" }),
        "7" : ({ "sie", "ssietm", "siesi" }),
        "8" : ({ "osss", "o�s", "osim", "o�sem" }),
        "9" : ({ "dzidzi", "tsiifii�", "tsiefie�", "dsifi��", "dssssfii", "dss" }),
        "0" : ({ "seoo", "eoo", "erhho", "sserrouu", "serrho" })
    ]);

mapping zamiana_pl = ([
        "�" : ({ "ou", "o" }),
        "�" : ({ "ci", "tsi", "csi" }),
        "�" : ({ "eu", "e�" }),
        "�" : ({ "�", "�y" }),
        "�" : ({ "ni", "n", "n" }),
        "�" : ({ "�u", "u" }),
        "�" : ({ "sii", "si" }),
        "�" : ({ "sz", "zsz" }),
        "�" : ({ "zii", "�", "si", "�s" }),
        "�" : ({ "OU", "ON" }),
        "�" : ({ "CI", "TSI", "CSI" }),
        "�" : ({ "EU", "E�" }),
        "�" : ({ "�", "�Y" }),
        "�" : ({ "NI", "N", "N" }),
        "�" : ({ "�U", "U" }),
        "�" : ({ "SII", "SI", "SSI" }),
        "�" : ({ "SZ", "ZSZ" }),
        "�" : ({ "ZII", "�", "SI", "�S" })
    ]);


string
find_znak(string z)
{
    string *help;

    if (help = zamiana[z])
        return help[random(sizeof(help))];

    if (help = zamiana_pl[z])
        return help[random(sizeof(help))];

    return "0";
}

/*nowa versja ftlumacz */

static string
ftlumacz(string tekst, int s_pijanstwa, int max_pijanstwo)
{
    string *str;
    string add;

    if (!tekst)
        return 0;

    str = explode(tekst, ""); //Tekst po literce

    if (s_pijanstwa > 50)
    {
        int size = sizeof(str), i = 0;
        int i_add = (10 - ((s_pijanstwa * 10) / max_pijanstwo)) >> 1;

        if (i_add==0)
            i_add++;

        for (; i < size; i += i_add)
            if ((add = "" + find_znak(str[i])) != "0" ) str[i] = "" + add;
        return implode(str,"");
    }
    else
        return tekst;
}


public string
speech_drunk(string co, object ob)
{
    int s_p = ob->query_intoxicated(), max_p = ob->intoxicated_max();

    if (s_p > max_p) s_p = max_p;
    if (co != "")
        return ftlumacz(co, s_p, max_p);

    return "";
}

/*
 *Formatuje tekst �adnie. Vera
 */
string
format_speech(string str)
{
    int x=0,y=0;

    if (!str)
	return 0;

    if(str[..0] == ".") //pierwszy znak
        str=str[1..];

    if(str[..0] == " ") //pierwszy znak
        str=str[1..];

    if(str[..0] == ":") //pierwszy znak
    {
        str=str[1..];
        x=1;
    }

    if(str[..0] == " ") //pierwszy znak
        str=str[1..];

    str=capitalize(str[..0])+str[1..];

    switch(str[-2..]) //blokada emotek
    {
        case ":)":
        case ":(":
        case ":]":
        case ":[":
        case ":>":
        case ":<":
        case ":P":
        case ":p":
        case ":D":
        case ":O":
        case ":S":
        case ":d":
        case ":o":
        case ":s":
        case ":|":
        case ":*":
        case ":}":
        case ":{":
        case ":\\":
        case ":/":
        case ";)":
        case ";(":
        case ";]":
        case ";[":
        case ";>":
        case ";<":
        case ";P":
        case ";p":
        case ";D":
        case ";O":
        case ";S":
        case ";d":
        case ";o":
        case ";s":
        case ";|":
        case ";*":
        case ";}":
        case ";{":
        case ";\\":
        case ";/":
        case "^^":
        case "):":
        case ");":
        case "(:":
        case "(;":
                  str=str[..-3];

                  break;
    }

    switch(str[-3..]) //blokada emotek trzyznakowych
    {
        case ":-)":
        case ";-)":
        case ":-(":
        case ";-(":
        case ":-P":
        case ";-P":
        case ":-p":
        case ";-p":
        case ":-D":
        case ";-D":
        case ";-d":
        case ";-D":
        case "*_*":
        case "-_-":
                   str=str[..-4];
                   break;
    }

    if(str[-1..] == " ") //ostatni znak
        str=str[..-2];

    switch(str[-1..])
    {
        case ".":
        case "!":
        case "?":
        case ":":
        case ",":
        case ";":
                  break;
        default:
                  str += ".";
    }

    if(x && !y)
        str=": "+str;

    return str+clear_color();
}
