/**
 * \file /sys/global/opisy_walki
 *
 * W pliku znajduj� si� funkcje odpowiadaj�ce za opisanie
 * walki.
 *
 * @author Krun, krun@vattghern.com.pl
 * @date 7.9.2008 11:18:52
 *
 * @version 1.0
 */

#pragma no_clone
#pragma no_reset
#pragma no_inherit
#pragma save_binary
#pragma strict_types

#include <colors.h>
#include <macros.h>
#include <wa_types.h>
#include <opisy_walki.h>

#define DBG(x)              find_player("krun")->catch_msg(set_color(find_player("krun"),                   \
                                COLOR_FG_RED) + "OPISY_WALKI: " + clear_color(find_player("krun")) + (x) + "\n")
/**
 * Funkcja zawieraj�ca wszystkie opisy cios�w.
 *
 * @param ind   index.
 * @param dt    typ obra�e�
 *
 * @return wszystkie dopuszczalne dla broni i obra�e� opisy cios�w.
 */
private nomask string *
opisy_ciosow(int ind, int dt)
{
    switch(ind)
    {
        case OW_WRECZ:
            return ({
                "Cios wr�cz.",
                });

        case OW_CIECIE:
            return ({
                "Wyprowadzaj�c %b:4 atak z nad ramienia, nabieraj�c impetu skr�tem tu�owia. Silnym i o dziwo " +
                "niewiele przez to wolniejszym ci�ciem{|%k:0} (lekko|bole�nie||ci�ko|bardzo ci�ko) rani{isz|i} " +
                "{%p:3|ci�|%p:3} w %h:3.",
                "{Pewnym/Szybkim/Niespodziewanym/Dalekim} wykrokiem {p|%k:0 p}zachodz{isz|i} {%p:1|ci�|%p:1} z " +
                "{lewej/prawej} by ci�� %b:4 od do�u po %h:5.",
                "Wywijaj�c szeroki m�ynek {p|%k:0 p}rzechodz{isz|i} do " +
                "{niewprawnego/rozpaczliwego/desperackiego} ci�cia %b:4, kt�re jednak jako� si�ga " +
                "{%p:3|ci�|%p:3} w %h:3.",
                "%B:0 zakre�la w{ twoim|} r�ku{ %k:1|| %k:1} kr�tki �uk gdy b�yskawicznie wyprowadzone z " +
                "nadgarstka ci�cie si�ga{| twoje[go|ej|go|%p]|} %h:1{ %p:1|| %p:1}.",
                "{W|%K:0 w}ychyl{asz|a} si� w {niepewnym/niebezpiecznym/rozpaczliwym} wypadzie dziobi�c %p:3 " +
                "sztychem %b:1 w %h:3.",
                "Widz�c luk� w{| twojej|} obronie{ %p:1|| %p:1} (lekko|s�abo||pewnie|zdecydowanie|mocno) " +
                "tnie{sz|} %b:4 po %h:5, a jedynie �le oceniony dystans nie pozwala {ci|[mu|jej|mu|im|im|%p]} " +
                "w pe�ni wykorzysta� impetu.",
                "{U|%K:0 u}stawi{asz|a} parad� przed{ twoim|} ciosem{ %p:1|| %p:1}, lecz w ostatniej chwili " +
                "obrac{asz|a} %b:3 w nadgarstku pozwalaj�c {ci|przeciwni[kowi|czce|kowi|kom|czkom|%p]} p�j�� za " +
                "impetem uderzenia, by (lekko|s�abo|swobodnie|z �atwo�ci�|mocno) ci�� z g�ry w " +
                "{[jego|jej|%p]|twoj�|[jego|jej|%p]}%h:3.",
                "{R|%K:0 r}zuc{asz|a} si� w{| twoj�|} stron�{ %p:1|| %p:1} ca�ym cia�em jednocze�nie tn�c zza " +
                "ucha %b:4. Rozp�dzone ostrze ci�gnie za sob� wianuszek krwi po tym rozcina " +
                "{[mu|jej|%p]|ci|[mu|jej|%p]} %h:3.",
                "{B|%K:0 b}ierze{sz|} szeroki zamach zza g�owy jednocze�nie robi�c krok w przeciwn� stron� " +
                "przez co {(lekki ||mocny )cios/(lekkie ||silne )ci�cie} si�gaj�ce " +
                "{[jego|jej|%p]|twoje[go|jej|%h]|[jego|jej|%p]}( lekko|| ci�ko) rani {ci�|[go|j�|je|%p]} w %h:3.",
                "{K|%P:0 k}rokiem w ty� unik{asz|a}{ twojego|} sko�nego ci�cia{| %k:1}, lecz kolejny "+
                "{szybki/b�yskawiczny/natychmiastowy} cios w tej samej linii, lecz tym razem zadany od do�u "+
                "sprawia, �e %b:3 (ledwo|lekko||bardzo|mocno) rozcina {[mu|jej|%p]|ci|[mu|jej|%p]} %h:3.",
                "{S|%K:0 s}tar{asz|a} si� zmyli� {%p:3|ci�|%p:3} {kr�tk�/szybk�/prost�} fint�, a gdy "+
                "{[on|ona|ono|%p]|ty|[on|ona|ono|%p]} odruchowo zas�ani{a|asz|a} si�, omij{asz|a} "+
                "{[jego|jej|%p]|twoj�|[jego|jej|%p]} zastaw� p�ynnym p�obrotem tn�c "+
                "(lekko |nieznacznie ||mocno |bardzo mocno )%b:1 w %h:3.",
                "{K|%K:0 k}opie{sz|} {%p:3|ci�|%p:3} {niespodziewanie/nagle/b�yskawicznie} w "+
                "kolano i wykorzystuj�c u�amek sekundy gdy {[jego|jej|%p]|twoj�|[jego|jej|%p]} "+
                "garda jest nieco {sztywniejsza/opuszczona/s�absza}, tni{esz|e} {�mia�o/z bliska/z "+
                "�atwo�ci�} wprost w %h:3 zadaj�c {je[mu|ej|%p]|ci|je[mu|ej|%p]} "+
                "(lekkie |nieznaczne ||spore |du�e |bardzo du�e | pot�ne )obra�enia.",
                "{P|%K:0 p}ozostawia{sz|} {�wiadomie|nieopacznie} luk� w obronie, {a|lecz} gdy {%p:0||%p:0} "+
                "star{a|asz|a} si� j� wykorzysta�, mi�kkim skokiem w bok po��czonym z p�obrotem "+
                "zachodz{isz|i} {[go|j�|%p]|ci�|[go|j�|%p]} z flanki by bez trudno�ci ci�� %b:4 w %h:3 "+
                "zadaj�c w ten spos�b (lekkie|nieznaczne|spore|du�e|bardzo du�e) obra�enia.",
                "Bior�c zamach %b:4 spod lewego ramienia {|%k:0 }atakuje{sz|} frontalnie {%p:3|ci�|%p:3}. "+
                "Ostrze rozchlapuje krew po tym jak wgryza {[mu|jej|%p]|ci|[mu|jej|%p]} si� ono w %h:3 "+
                "zadaj�c (spore|du�e|bardzo du�e) obra�enia."
                });

        case OW_PCHNIECIE:
            return ({
                "Przed zadaniem pchni�cia {w|%k:0 w}znos{isz|i} wysoko %b:3 by omin��{| twoj�|} "+
                "zastaw�{ %p:1|| %p:1}. Mimo rozpaczliwo�ci takiego ataku, udaje "+
                "{ci|[mu|jej|mu|im|im|%k]} si� ugodzi� {przeciwni[ka|czk�|ka|k�w|czki|%p]|ci�|[go|j�|je|%p]} "+
                "w %h:3.",
                "{Z|%K:0 z}amierz{asz|a} si� do szerokiego ci�cia, lecz zamiast niego uderzasz "+
                "{%p:3|ci�|%p:3} drugim barkiem, a gdy odskakuj{ecie|�} od siebie, wykr�c{asz|a} si� w "+
                "p�obrocie posy�aj�c za {[nim|ni�|%p]|tob�|[nim|ni�|%p]} pchni�cie %b:1 trafiaj�ce "+
                "w %h:3 zadaj�c w ten spos�b (lekkie|nieznaczne|spore|du�e|bardzo du�e) obra�enia."
                });

        case OW_UDERZENIE:
            return ({
                "Opis uderzenia."
                });

        case OW_MIECZ:
            return ({
                "Krew tryska z{| twoje[go|j|go|%p] |} %h:1{ %p:1|| %p:1} po tym jak si�ga " +
                "{ci�|[go|j�|je|ich|je|%p]} ostrze %b:1."
                });

        case (OW_MIECZ | OW_PCHNIECIE):
            return ({
                "{S|%K:0 s}chodz{isz|i} p�obrotem w bok nabieraj�c impetu, po czym chwyta{sz|} %b:3 powy�ej " +
                "jelca i tak skr�con� broni� zadaje niespodziewanie szybkie pchni�cie, kt�re " +
                "(ledwo muska|lekko rani|rani|ci�ko rani|bardzo ci�ko rani|masakruje){ %p:3| ci�| %p:3} w " +
                "%h:3.",
                "Spokojnie utrzymuj�c dystans {p|%k:0 p}ozwal{asz|a} by{| [jego|jej|%k]} d�o� lekko ze�lizgn�a " +
                "si� po d�ugiej r�koje�ci %b:1, po czym trzymaj�c za jej koniec spina{asz|a} si� do dalekiego " +
                "wypad w{| twoim|} kierunku{ %p:1|| %p:1}, a " +
                "(s�abym|lekkim|niepewnym|pewnym|silnym|bardzo mocnym) pchni�ciem " +
                "(ledwo muska|lekko rani|rani|bardzo ci�ko rani){sz [go|j�|je|%p]| ci�| [go|j�|je|%p]} w %h:3.",
                "{T|%K:0 t}ni{esz|e} p�asko z {prawej/lewej}, gdy za� %b:0 zostaje {zablokowan[y|a|%b] "+
                "przez %p:3|przez ciebie zablokowan[y|a|%b]|zablokowan[y|a|%b] przez %p:3}, nie cofaj�c "+
                "broni unos{isz|i} r�koje�� na wysoko�� twarzy i pch{asz|a} z ca�ej si�y prosto w{ | "+
                "tw[�j|oj�|%p] | }%h:3{ %p:1|| %p:1} zadaj�c [mu|jej|%p] (spore |du�e |bardzo du�e) "+
                "obra�enia.",
                "{Z|%K:0 z} pozoru {spokojnie/po prostu/jedynie} os�aniaj�c si� %b:4, nagle prostuj{esz|e} "+
                "�okie� by samym ko�cem ostrza ugodzi� {%p:3|ci�|%p:3} w %h:3 zadaj�c w ten "+
                "spos�b (lekkie|nieznaczne|spore|du�e|bardzo du�e) obra�enia.",
                });

        case (OW_MIECZ | OW_CIECIE):
            return ({
                "Spokojnie utrzymuj�c dystans {|%k:0 }pozwala{sz|} by{| [jego|jej|%k]} d�o� lekko ze�lizgn�a " +
                "si� po d�ugiej r�koje�ci %b:1, po czym trzymaj�c za jej koniec spina{asz|a} si� do dalekiego " +
                "wypad w{| twoim|} kierunku{ %p:1|| %p:1}, a " +
                "(s�abym|lekkim|niepewnym||pewnym|silnym|bardzo mocnym) pchni�ciem " +
                "(ledwo muska|lekko rani|rani|bardzo ci�ko rani) {[go|j�|%p]|ci�|[go|j�|%p]} w %h:3.",
                "{T|%K:0 t}ni{esz|e} b�yskawicznie %b:4 z �okcia nabieraj�c impetu skr�tem bioder, tak �e czubek "+
                "ostrza (ledwo trafia|trafia|precyzyjnie trafia) {%p:3|ci�|%p:3} w %h:3.",
                "{C|%P:0 c}of{asz|a} si� p� kroku jednocze�nie niespodziewanie tn�c {%p:3|ci�|%p:3} p�asko. " +
                "Wykorzystuj�c d�ugo�� %b:1( ledwo| lekko|| mocno| bardzo mocno) trafi{asz|a} " +
                "{[go|j�|%p]|ci�|[go|j�|%p]} ko�cem ostrza w %h:3.",
                "Myl�c {%p:3|ci�|%p:3} g�rn� fint�, {|%k:0 }(lekko|s�abo||pewnie|zdecydowanie|mocno) " +
                "tnie{sz [go|j�|%p]|ci�|[go|j�|%p]} sko�nie przez %h:3.",
                "M�ynkuj�c %b:4 nad g�ow� {s|%k:0 s}tar{asz|a} si� trafi� {%p:3|ci�|%p:3} "+
                "gdziekolwiek, wreszcie si�gaj�c niemal przypadkiem ko�cem ostrza w %h:3 "+
                "zadaj�c [mu|jej|%p] (lekkie |nieznaczne |spore) obra�enia.",
                "{B|%K:0 b}ierze{sz|} zamach zza ucha szar�uj�c na {%p:3|ciebie|%p:3}. Uderzenie "+
                "barkiem odrzuca {[go|j�|%p]|ci�|[go|j�|%p]} do ty�u i nim odzyskuje{|sz|} "+
                "r�wnowag� {r|%k:0 r}�bie{sz|} {[go|j�|%p]|ci�|[go|j�|%p]} "+
                "(mocno|pot�nie|z ca�ej si�y) w %h:3.",
                "{R|%K:0 r}�bi{esz|e} zza ucha %b:4 {niemal przypadkiem/nie bez pomocy szcz�cia} "+
                "si�gaj�c {%p:1|ci�|%p:1} ko�cem ostrza w %h:3 zadaj�c w ten spos�b "+
                "(lekkie|nieznaczne|spore) obra�enia.",
                "Jakby samym obrotem nadgarstka i lekkim krokiem {k|%k:0 k}ieruj{esz|e} ostrze "+
                "%b:1 wprost ku{ |twoj[emu|ej|emu|im|im] | }%h:2{%p:1||%p:1} zadaj�c "+
                "{[mu|jej|%p]|ci|[mu|jej|%p]} (nieznaczne|drobne|lekkie|mocne|bardzo mocne) "+
                "obra�enia.",
                });

        case (OW_TOPOR | OW_CIECIE):
            return ({
                "{B|%K:0 b}ierz{esz|e} zamach zza ucha szar�uj�c na {%p:3|ciebie|%p:3}. Uderzenie barkiem "+
                "odrzuca {[go|j�|%p]|ci�|[go|j�|%p]} do ty�u i nim odzyskuj{e|esz|e} r�wnowag� "+
                "{r|%k:0 r}�bi{esz|e} {[go|j�|%p]|ci�|[go|j�|%p]} (mocno|pot�nie|z ca�ej si�y) w %h:3.",
                "{R|%K:0 r}�bi{esz|e} z g�ry %b:4 wykorzystuj�c ci�ar broni by przebi� si� przez "+
                "obron� {%p:1|twoj�|%p:1}. Niepowstrzymane ostrze si�ga celu i "+
                "(nieznacznie |lekko ||mocno| bardzo mocno) rani {[go|j�|%p]|ci�|[go|j�|%p]} w %h:3.",
                });

        case (OW_TOPOR | OW_UDERZENIE):
            return ({
                "{R|%K:0 r}obi�c {kr�tki/szybki/lekki} krok w prz�d pr�buj{esz|e} pchni�cia %b:4. "+
                "Bolesne uderzenie obucha trafia {%p:3|ci�|%p:3} w %h:3, rani�c [go|j�|%p] "+
                "(nieznacznie|lekko|dotkliwie|mocno|bardzo mocno).",
                });

        case (OW_SZTYLET | OW_PCHNIECIE):
            return ({
                "Schodz�c z{ | twojej | }linii ataku{ %p:1|| %p:1}, {r|%k:0 r}ob{isz|i} p�obr�t "+
                "ustawiaj�c si� do przeciwni[ka|czki|%p] plecami. B�yskawicznie odwr�cony ostrzem w "+
                "d� %b:0 wgryza{| ci|} si�{ %p:2|| %p:2} w %h:3, a szybki odskok ponownie was rozdziela. ",
                });

        default:
            return ({});
    }
}

/**
 * Funkcja zawieraj�ca wszystkie opisy wyparowa� przez zbroje.
 *
 * @param ind   index.
 * @param dt    typ obra�e�
 *
 * @return opisy o podanym indexie
 */
private nomask string *
opisy_wyparowan(int ind, int dt)
{
    string *cios;

    if((dt & OW_CIECIE) == OW_CIECIE)
        cios = ({"ci�cie", "ci�cia", "ci�ciu", "ci�cie", "ci�ciem", "ci�ciu"});
    else if((dt & OW_PCHNIECIE))
        cios = ({"pchni�cie", "pchni�cia", "pchni�ciu", "pchni�cie", "pchni�ciem", "pchni�ciu"});
    else if((dt & OW_UDERZENIE))
        cios = ({"uderzenie", "uderzenia", "uderzeniu", "uderzenie", "uderzeniem", "uderzeniu"});

    switch(ind)
    {
        case (OW_PCHNIECIE | OW_CIECIE | OW_UDERZENIE):
            return  ({
                "{Tw[�j|oja|oje] %b:3|%B:0 %k:1} odskakuje od chroni�ce[go|ej|%z] %h:3 %p:1 %z:1.",
                "Si�a jak� {wk�adasz|%k:0 wk�ada} w {" + cios[PL_MIA] + "/cios} %b:4 nie jest wystarczaj�ca by przebi� si� " +
                "przez %z:3 ochraniaj�c[y|�|e|%z] {je[go|j|go|%p]|tw[�j|oj�|oje|%p]|je[go|j|go|%p]} %h:3.",
                "Impet{ twojego|} {" + cios[PL_DOP] + "/ciosu} %b:1{| %k:1} zostaje ca�kowicie wyhamowany przez "+
                "{tw[�j|oj�|oje|ich|je|%z]|je[go|j|%p]} %z:3.",
                "{Twoje {" + cios[PL_MIA] + "/cios}|{" + capitalize(cios[PL_MIA]) + "/cios}} {|%k:1} %b:1, pomimo i� celnie trafi�o "+
                "%p:3 w %h:3, nie wyrz�dzi�o {je[mu|j|%p]|ci|je[mu|j|%p]} krzywdy, gdy� impet zosta� "+
                "zatrzymany przez %z:3.",
                "{Twoje " + cios[PL_MIA] + "|" + capitalize(cios[PL_MIA]) + " %k:1} %b:1 jest zbyt s�abe �eby zrani� %p:1 i "+
                "zatrzymuje si� na %z:5 chroni�c[ym|ej|%z] {je[go|j|%p]|tw[�j|oj�|oje|%p]|je[go|j|%p]} %h:3.",
                "{W|%P w}idz�c nieuchronne " + cios[PL_MIA] + " rozpaczliwym ruchem ustawia{|sz|} si� tak, �e "+
                "{tw[�j|oja|oje|%b]|je[go|j|%k]} %b:0 trafiaj�c w {je[go|j|%p]|tw[�j|oj�|oje|%h]|je[go|j|%p]} "+
                "%h:0 ze�lizguje si� ze zgrzytem po %z:2.",
                "Pomimo celnie zadanego{| przez %k:3} {" + cios[PL_DOP] + "/ciosu} %b:1 {pozycja %p:1|twoja pozycja|pozycja %p:1} "+
                "pozwala na bezpieczne przyj�cie go na chroni�c[y|�|e|%z] %h:3 %z:0.",
                "W mgnieniu oka {|%p:0} ustawia{|sz|} %h:3 tak by{ twoje|} " + cios[PL_MIA] + " {|%k:1 %b:1|%b:1 %k:1} "+
                "zsun�o si� po ochraniaj�c[ym|ej|ym|%z] {[go|j�|je|%p]|ci�|[go|j�|je|%p]} %z:1.",
                "Dzi�ki wielkiemu szcz�ciu %z:0 ochraniaj�ca {|tw[�j|oj�|oje]|} %h:3 {%p:1||%p1} przyj�a ca�y "+
                "impet {" + cios[PL_DOP] + "/ciosu} %k:1 i uchroni�[|a|o|%z] ci� przed ran� od %b:1.",
                });

        default:
            return ({});
    }
}

/**
 * Funkcja zawieraj�ca wszystkie opisy tarczownictwa.
 *
 * @param ind   index.
 * @param dt    typ obra�e�
 *
 * @return opisy o podanym indexie
 */
private nomask string *
opisy_tarczownictwa(int ind, int dt)
{
    string *cios;

    if((dt & OW_CIECIE) == OW_CIECIE)
        cios = ({"ci�cie", "ci�cia", "ci�ciu", "ci�cie", "ci�ciem", "ci�ciu"});
    else if((dt & OW_PCHNIECIE))
        cios = ({"pchni�cie", "pchni�cia", "pchni�ciu", "pchni�cie", "pchni�ciem", "pchni�ciu"});
    else if((dt & OW_UDERZENIE))
        cios = ({"uderzenie", "uderzenia", "uderzeniu", "uderzenie", "uderzeniem", "uderzeniu"});

    switch(ind)
    {
        case OW_CIECIE:
            return ({
                "Lekkie uniesienie %t:1 przez %p:3 wystarcza by zsun�o si� po [nim|niej|%t] ostrze "+
                "{twoje[go|j|%b] |}%b:1{| %k:1}.",
                });

        case (OW_PCHNIECIE | OW_CIECIE | OW_UDERZENIE):
            return  ({
                "{P:1 g|G|%P:1 g}wa�townie podrywa{|sz|} %t:3 zbijaj�c ju� niemal si�gaj�c %b:3 %k:1.",
                "{Twoja %b:0|%B:0 %k:1} ze�lizguje si� po powierzchni {je[go|j|%p]|twoje[go|j|%t]|je[go|j|%p]} %t:5.".
                "{D|%K:0 d}ostrzegaj�c w ostatniej chwili " + cios[PL_MIA] + " %k:1 przyjmuj{e|esz|e} je na sw[�j|oj�|oje|%t] %t:3.",
                "Przyjmuj�c odpowiedni� pozycj� {|%p} ustawia{|sz|} %t:3 tak by ca�y impet " + cios[PL_DOP] + " "+
                "%b:1 %k:1 zosta� wyparowany.",
                "Dzi�ki sporej dawce szcz�cia wyprowadzone przez {ciebie|%k:3} " + cios[PL_MIA] + " %b:3, zostaje przez "+
                "{[niego|ni�|%p]|ciebie|[niego|ni�|%p]} zablokowany %t:4.",
                "{%P:0 p|P|%P:0 p}rzykuca{|sz|} lekko wznosz�c %t:3 nad g�ow� i zatrzymuj�c [nim|ni�|%t]"+
                "{ twoje|} " + cios[PL_MIA] + " %b:1 %k:1.",
                "{%P:0 r|R|%P:0 r}obi{|sz|} krok w bok jednocze�nie sko�nie ustawiaj�c %t:3, przez co " + cios[PL_MIA] + " "+
                "%b:1 %k:1 zsuwa si� po [nim|niej|%t] nie odnosz�c skutku.",
                "Szybkim wypadem{ %p:0|| %p:0} skraca{|sz|} dystans i blokuje{|sz|} %t:4{ twoje|} " + cios[PL_MIA] + " %b:1 "+
                "w chwili gdy dopiero go wyprowadza{sz|a}.",
                "{Z|%p:0 z}zbija{|sz|} kantem %t:1 " + cios[PL_MIA] + " wymierzone %b:1 przez {ciebie|%k:0}.",
                "Trac�c z oczu{ tw[�j|oj�|oje|%b]|} %b:3{| %k:1} przezornie os�ania{|sz|} si� %t:4 po kr�tej "+
                "ze�lizguje si� {tw�j|je[go|j|%k]} " + cios[PL_MIA] + ".",
                "{%P:0 b|B|%P:0 b}�yskawicznie os�ania{|sz|} %t:4 %h:3 w kt�r[y|�|e|%h] mierzy�o " + cios[PL_MIA] + " %b:1 %k:1.",
                "{%P:0 u|U|%P:0 u}mykaj�cym oku ruchem zbija{|sz|} wymierzone w {ni[ego|�|e|%p]|ciebie|ni[ego|�|e|%p]} "+
                "%b:1 %k:1 niwecz�c {twoje|je[go|j|%k]} " + cios[PL_MIA] + ".",
                "{%P:0 o|O|%p:0 o}s�ania{|sz|} si� %t:4 przed{ twoim|} " + cios[PL_NAR] + "{| %k:1} i cho� zmusza "+
                "{[go|j�|je|%p]|ci�|[go|j�|je|%p]} do cofni�cia si� o krok, nie czyni "+
                "{[mu|jej|%p]|ci|[mu|jej|%p]} krzywdy.",
                });

        default:
            return ({});
    }
}

/**
 * Funkcja zawieraj�ca wszystkie opisy parowa�.
 *
 * @param ind   index.
 * @param dt    typ obra�e�
 *
 * @return opisy o podanym indexie
 */
private nomask string *
opisy_parowan(int ind, int dt)
{
    string *cios;

    if((dt & OW_CIECIE) == OW_CIECIE)
        cios = ({"ci�cie", "ci�cia", "ci�ciu", "ci�cie", "ci�ciem", "ci�ciu"});
    else if((dt & OW_PCHNIECIE))
        cios = ({"pchni�cie", "pchni�cia", "pchni�ciu", "pchni�cie", "pchni�ciem", "pchni�ciu"});
    else if((dt & OW_UDERZENIE))
        cios = ({"uderzenie", "uderzenia", "uderzeniu", "uderzenie", "uderzeniem", "uderzeniu"});

    switch(ind)
    {
        case OW_CIECIE:
            return ({
                "{%P:0 p|P|%P:0 p}rzymuje{|sz|} ci�cie na %m:3 pozwalaj�c by %b:0 ze�lizng[��|�a|�o|%b] si� po " +
                "{twojej |}%b:1{| %k:1}.",
                "{%P:0 r|R|%P:0 r}obi{|sz|} kr�tki wypad, wysoko podbijaj�c swo[im|j�|%m] m:4{ tw[�j|oj�|oje|%b]|} "+
                "%b:1{| %k:1} nim {[go|j�|je|%p]|ci�|[go|j�|je|%p]} dosi�gn[��|�a|�o|%b].",
                });

        case (OW_CIECIE | OW_UDERZENIE):
            return ({
                "{%P:0 s|S|%P:0 s}tara{|sz|} si� zablokowa�{ twoje|} " + cios[PL_MIA] + " %b:1{| %k:1} i cho� ostrze "+
                "ze�lizn�o si� nieco po {je[go|j|%p]|two[im|jej|%m]|je[go|j|%p]} %m:5 to i tak nie czyni "+
                "{[mu|jej|%p]|ci|[mu|jej|%p]} �adnej krzywdy.",
                "{%D:0 d|D|%D:0 d}esperacko zastawia{|sz|} si� %m:4, a si�a{ twoj[ego|ej|%p]|} zablokowanego "+
                cios[PL_MIA] + " {|%k:1 }cofa {ci�|[go|j�|je|%p]} o krok w ty�.",
                "Trac�c na chwil�{ tw[�j|oj�|oje|%b]|} %b:3{| %k:1} z oczu,{ %p:0|| %p:0} intuicyjnie "+
                "zas�ania{|sz|} si� %m:4 na kt�ry niemal w tej samej chwili spada " + cios[PL_MIA] + "{ twoje[go|ej|%b]|} %b:1.",
                });

        case (OW_CIECIE | OW_PCHNIECIE):
            return ({
                });

        case (OW_PCHNIECIE | OW_CIECIE | OW_UDERZENIE):
            return  ({
                "{%P:0 r|R|%P:0 r}obi{|sz|} kr�tki wypad, wysoko podbijaj�c swo[im|j�|%m] "+
                "%m:4{ tw[�j|oj�|oje|%b]|} %b:1{| %k:1} nim {[go|j�|je|%p]|ci�|[go|j�|je|%p]} dosi�gn[��|�a|�o|%b].",
                "Robi�c krok w ty�{ %p:0|| %p:0} wykonuje{|sz|} jednocze�nie poprzeczn� zastaw�, po kt�rej "+
                "ze�lizguje si� {tw[�j|oja|oje|%b]| }%b:0{| %k:1}.",
                "{%P:0 l|L|%P:0 l}ekko podbija{|sz|} swo[im|j�|%m] %m:4 {je[go|j|%k]|tw[�j|oj�|oje|%b]|je[go|j|%k]} "+
                "%b:4 %k:1 przez co {twoje|j[ego|ej|%k]} " + cios[PL_MIA] + " mija "+
                "{[go|j�|je|%p]|ci�|[go|j�|je|%p]} nie czyni�c {[mu|jej|%p]|ci|[mu|jej|%p]} �adnej krzywdy.",
                "{%P:0 n|N|%P:0 n}adstawia{|sz|} %m:3 mocno wykr�caj�c nadgarstek, dzi�ki czemu udaje si� "+
                "{[mu|jej|%p]|ci|[mu|jej|%p]} sparowa� wyprowadzone przez {ciebie|%k:1} " + cios[PL_MIA] + " %b:1.",
                "{%P:0 o|O|%P:0 o}tacza{|sz|} si� szerok� zastaw�, odbijaj�c swo[im|j�|%m] %m:4{ twoje|} "+
                cios[PL_MIA] + "{| %k:1} %b:1.",
                "{%P:0 u|U|%P:0 u}stawia{|sz|} sko�nie sw[�j|oj�|oje|%m] %m:0 pewnie odbijaj�c ni[m|�|%m] "+
                cios[PL_MIA] + " {two[im|j�|%b] |}%b:1{| %k:1}.",
                });

        default:
            return ({});
    }
}

/**
 * Funkcja zawieraj�ca wszystkie opisy unik�w.
 *
 * @param ind   index.
 * @param dt    typ obra�e�
 *
 * @return opisy o podanym indexie
 */
private nomask string *
opisy_unikow(int ind, int dt)
{
    string *cios;

    if((dt & OW_CIECIE) == OW_CIECIE)
        cios = ({"ci�cie", "ci�cia", "ci�ciu", "ci�cie", "ci�ciem", "ci�ciu"});
    else if((dt & OW_PCHNIECIE))
        cios = ({"pchni�cie", "pchni�cia", "pchni�ciu", "pchni�cie", "pchni�ciem", "pchni�ciu"});
    else if((dt & OW_UDERZENIE))
        cios = ({"uderzenie", "uderzenia", "uderzeniu", "uderzenie", "uderzeniem", "uderzeniu"});

    switch(ind)
    {
        case OW_WRECZ:
            return ({
                "{%P:0 u|U|%P:0 u}skakuje{|sz|} lekko w bok, o w�os unikaj�c{ twojego|} " + cios[PL_MIA] + " %b:1{| %k:1}.",
                "Mocnym skr�tem tu�owia{ %p:0|| %p:0} uchyla{|sz|} si� przed{ twoim|} " + cios[PL_MIA] + " %b:1{| %k:1}.",
                "{%P:0 b|B|%P:0 b}�yskawicznie przykl�ka{|sz|} pod wymierzonym przez {ciebie|%k:3} w %h:3 " + cios[PL_NAR] + ".",
                "{%P:0 g|G|%P:0 g}wa�townie odskakuje{|sz|} w ty� przed{ twoim|} " + cios[PL_NAR] + "{| %k:1} ledwo "+
                "odzyskuj�c r�wnowag�.",
                "{%P:0 s|S|%P:0 s}kr�ca{|sz|} si� mocno odchylaj�c rami�, w kt�re wymierzone by�o{ twoje|} "+
                "ci�cie %b:1{| %k:1}.",
                "{%P:0 u|U|%P:0 u}chyla{|sz|a si� mocno w {lewo/prawo} przed{ twoim|} " + cios[PL_NAR] + "{| %k:1}.",
                "{%P:0 d|D|%P:0 d}esperacko uchyla{|sz|a si� przed{ twoim|} " + cios[PL_NAR] + "{| %k:1}, $b:0 mimo to "+
                "niemal ociera si� o tw[�j|oj�|oje|%h] %h:3.",
                "Oszcz�dnym p�obrotem{ %p:0|| %p:0} schodzi{|sz|} z linii{ twojego|} ataku{| %k:1}.",
                "W ostatniej chwili{ %p:0|| %p:0} robi{|sz|} krok w ty� unikaj�c{ twoje[go|j|%k]|} %b:1{| %k:1} "+
                "mierz�ce[go|j|%b] w {je[go|j|%p]|tw[�j|oj�|oje|%h]|je[go|j|%p]} %h:3.",
                });

        case OW_CIECIE:
            return ({
                "{%P:0 r|R|%P:0 r}obi{|sz|} oszcz�dny krok w ty�, a{ tw[�j|oja|oje|%k]|} %b:0{| %k:1} tnie "+
                "powietrze tu� przed {ni[m|�|%p]|tob�|ni[m|�|%p]}.",
                });

        case (OW_CIECIE | OW_UDERZENIE):
            return ({
                "{%P:0 s|S|%P:0 s}chyla{|sz|} si�, wtulaj�c mocno g�ow� w ramiona unikaj�c{ twojego|} "+
                cios[PL_DOP] + "{| %k:1}. %B:0 z sykiem przecina powietrze tu� przy {je[go|j|%p]|twoim|je[go|j|%p]} uchu.",
                });

        case (OW_CIECIE | OW_PCHNIECIE | OW_UDERZENIE):
            return ({
                "{%P:0 u|U|%P:0 u}skakuje{|sz|} lekko w bok, o w�os unikaj�c{ twojego|} " + cios[PL_MIA] + " %b:1{| %k:1}.",
                "Mocnym skr�tem tu�owia{ %p:0|| %p:0} uchyla{|sz|} si� przed{ twoim|} " + cios[PL_MIA] + " %b:1{| %k:1}.",
                "{%P:0 b|B|%P:0 b}�yskawicznie przykl�ka{|sz|} pod wymierzonym przez {ciebie|%k:3} w %h:3 " + cios[PL_NAR] + ".",
                "{%P:0 g|G|%P:0 g}wa�townie odskakuje{|sz|} w ty� przed{ twoim|} " + cios[PL_NAR] + "{| %k:1} ledwo "+
                "odzyskuj�c r�wnowag�.",
                "{%P:0 s|S|%P:0 s}kr�ca{|sz|} si� mocno odchylaj�c rami�, w kt�re wymierzone by�o{ twoje|} "+
                "ci�cie %b:1{| %k:1}.",
                "{%P:0 u|U|%P:0 u}chyla{|sz|a si� mocno w {lewo/prawo} przed{ twoim|} " + cios[PL_NAR] + "{| %k:1}.",
                "{%P:0 d|D|%P:0 d}esperacko uchyla{|sz|a si� przed{ twoim|} " + cios[PL_NAR] + "{| %k:1}, $b:0 mimo to "+
                "niemal ociera si� o tw[�j|oj�|oje|%h] %h:3.",
                "Oszcz�dnym p�obrotem{ %p:0|| %p:0} schodzi{|sz|} z linii{ twojego|} ataku{| %k:1}.",
                "W ostatniej chwili{ %p:0|| %p:0} robi{|sz|} krok w ty� unikaj�c{ twoje[go|j|%k]|} %b:1{| %k:1} "+
                "mierz�ce[go|j|%b] w {je[go|j|%p]|tw[�j|oj�|oje|%h]|je[go|j|%p]} %h:3.",
                });

        default:
            return ({});
    }
}

/**
 * Funkcja pomocnicza losuj�ca opisy.
 *
 * @param funkcja   funkcja z opisami
 * @param dt        typ obra�e�
 * @param br        typ broni
 */
private nomask mixed
wybierz_opis(function funkcja, int dt, int br)
{
    int d1, d2;
    string *opisy = ({});
    string opis;

    switch(dt)
    {
        case OW_CIECIE:
            d1 = OW_PCHNIECIE;
            d2 = OW_UDERZENIE;
            break;

        case OW_PCHNIECIE:
            d1 = OW_CIECIE;
            d2 = OW_UDERZENIE;
            break;

        case OW_UDERZENIE:
            d1 = OW_CIECIE;
            d2 = OW_PCHNIECIE;
            break;
    }

    opisy += funkcja(dt, dt);
    opisy += funkcja(dt | d1, dt);
    opisy += funkcja(dt | d2, dt);
    opisy += funkcja(dt | d1 | d2, dt);

    for(int i = br ; i < (OW_MAX << 1) ; i++)
    {
        if((i & br) == br &&
            ((i & dt) == dt || ((i & d1) != d1 && (i & d2) != d2)))
        {
            opisy += funkcja(i, dt);
        }
    }

    if(!sizeof(opisy))
        return "Brak opisu.";

    return opisy[random(sizeof(opisy))];
}

/**
 * Funkcja pozwala na wylosowanie i wybranie opisu ciosu.
 *
 * @param dt    Typ obra�e�
 * @param br    Typ broni
 * @param kto   Obiekt atakuj�cy
 *
 * @return Wylosowany opis ciosu
 */
public nomask string
wybierz_opis_ciosu(int dt, int br = OW_WRECZ, object kto = 0)
{

        object oldtp = TP;
    if(kto)
    {
        object bron;

        bron = kto->query_attack_object(br);

        if(objectp(bron))
            br =  (1 << (bron->query_wt() + OW_PRZESUNIECIE));
        else
            br = OW_WRECZ;
    }

    return wybierz_opis(&opisy_ciosow(), dt, br);
}

/**
 * Funkcja pozawala na wylosowanie i wybranie opisu wyparowania przez zbroje.
 *
 * @param dt    Typ obra�e�
 * @param br    Typ broni
 * @param kto   Obiekt atakuj�cy
 *
 * @return Wylosowany opis wyparowania przez zbroje.
 */
public nomask string
wybierz_opis_wyparowania(int dt, int br = OW_WRECZ, object kto = 0)
{
    if(kto)
    {
        object bron;

        bron = kto->query_attack_object(br);

        if(objectp(bron))
            br =  (1 << (bron->query_wt() + OW_PRZESUNIECIE));
        else
            br = OW_WRECZ;
    }

    return wybierz_opis(&opisy_wyparowan(), dt, br);
}

/**
 * Funkcja pozawala na wylosowanie i wybranie opisu tarczownictwo.
 *
 * @param dt    Typ obra�e�
 * @param br    Typ broni
 * @param kto   Obiekt atakuj�cy
 *
 * @return Wylosowany opis tarczownictwa.
 */
public nomask string
wybierz_opis_tarczownictwa(int dt, int br = OW_WRECZ, object kto = 0)
{
    if(kto)
    {
        object bron;

        bron = kto->query_attack_object(br);

        if(objectp(bron))
            br =  (1 << (bron->query_wt() + OW_PRZESUNIECIE));
        else
            br = OW_WRECZ;
    }

    return wybierz_opis(&opisy_tarczownictwa(), dt, br);
}

/**
 * Funkcja pozawala na wylosowanie i wybranie opisu parowania.
 *
 * @param dt    Typ obra�e�
 * @param br    Typ broni
 * @param kto   Obiekt atakuj�cy
 *
 * @return Wylosowany opis parowania.
 */
public nomask string
wybierz_opis_parowania(int dt, int br = OW_WRECZ, object kto = 0)
{
    if(kto)
    {
        object bron;

        bron = kto->query_attack_object(br);

        if(objectp(bron))
            br =  (1 << (bron->query_wt() + OW_PRZESUNIECIE));
        else
        {
            return "Wyst�pi� powa�ny b��d. Nast�pi�a pr�ba parowania wr�cz. Zg�o� go opisuj�c "+
                "okoliczno�ci w jakich do niego dosz�o. Szczeg�ln� uwag� zwr�� na to czym walczy� "+
                "tw�j przeciwnik, i czy naprawde nie masz dobytej broni.";
        }
    }

    return wybierz_opis(&opisy_parowan(), dt, br);
}

/**
 * Funkcja pozawala na wylosowanie i wybranie opisu uniku.
 *
 * @param dt    typ obra�e�
 * @param br    typ broni
 *
 * @return Wylosowany opis uniku.
 */
public nomask string
wybierz_opis_uniku(int dt, int br = OW_WRECZ, object kto = 0)
{
    if(kto)
    {
        object bron;

        bron = kto->query_attack_object(br);

        if(objectp(bron))
            br =  (1 << (bron->query_wt() + OW_PRZESUNIECIE));
        else
            br = OW_WRECZ;
    }

    return wybierz_opis(&opisy_unikow(), dt, br);
}

/**
 * Funkcja pomocnicza.
 */
private nomask string
podstaw_za(string opis, int co, object ob, object fo)
{
    int przyp = 0, uc = 0;
    string *ex, short;

    ex = explode(" " + opis, "%");
    opis = "";

    if(sizeof(ex))
    {
        for(int i = 0 ; i < sizeof(ex) ; i++)
        {
            if(lower_case(ex[i][0..0])[0] == co)
            {
                if(ex[i][0] != co)
                    uc = 1;

                if(ex[i][1] == ':')
                    przyp = atoi(ex[i][2..2]);

                if(fo)
                {
                    if(co == 'k' || co == 'p')
                        short = (uc ? ob->query_Imie(fo, przyp) : ob->query_imie(fo, przyp));
                    else
                        short = (uc ? capitalize(ob->short(fo, przyp)) : ob->short(fo, przyp));
                }
                else
                    short = (uc ? QCIORS(ob, przyp) : QIORS(ob, przyp));

                opis += short + ex[i][3..];
            }
            else
                opis += "%" + ex[i];
        }
    }

    return opis[2..];
}

/**
 * Funkcja pomocnicza podstawiaj�ca za %h:przypadek opis hitlokacji w podanym przypadku.
 *
 * @param opis      Opis w kt�rym podstawiamy.
 * @param how       W�a�ciciel hitlokacji(czyli osoba trafiona:)).
 * @param hid       Identyfikator hitlokacji.
 * @param fo        Dla kogo opis hitlokacji ma zosta� wy�wietlony.
 */
private string
podstaw_hitlokacje(string opis, object how, int hid, object fo = 0)
{
    int przyp = 0, uc = 0;
    string *ex, short;

    ex = explode(" " + opis, "%");
    opis = "";

    if(sizeof(ex))
    {
        for(int i = 0 ; i < sizeof(ex) ; i++)
        {
            if(lower_case(ex[i][0..0])[0] == 'h')
            {
                if(ex[i][0] != 'h')
                    uc = 1;

                if(ex[i][1] == ':')
                    przyp = atoi(ex[i][2..2]);

                short = how->query_hitloc_desc(hid, przyp);

                if(uc)
                    short = capitalize(short);

                opis += short + ex[i][3..];
            }
            else
                opis += "%" + ex[i];
        }
    }

    return opis[2..];
}

/**
 * Funkcja pomocnicza podstawiaj�ca za %b:przypadek opis ataku w podanym przypadku.
 *
 * @param opis      Opis w kt�rym podstawiamy.
 * @param how       W�a�ciciel ataku, czyli trafiaj�cy
 * @param aid       Identyfikator ataku
 * @param fo        Dla kogo opis hitlokacji ma zosta� wy�wietlony.
 */
private string
podstaw_atak(string opis, object how, int aid, object fo = 0)
{
    int przyp = 0, uc = 0;
    string *ex, short;

    ex = explode(" " + opis, "%");
    opis = "";

    if(sizeof(ex))
    {
        for(int i = 0 ; i < sizeof(ex) ; i++)
        {
            if(lower_case(ex[i][0..0])[0] == 'b')
            {
                if(ex[i][0] != 'b')
                    uc = 1;

                if(ex[i][1] == ':')
                    przyp = atoi(ex[i][2..2]);

                short = how->query_attack_desc(aid, przyp);

                if(uc)
                    short = capitalize(short);

                opis += short + ex[i][3..];
            }
            else
                opis += "%" + ex[i];
        }
    }

    return opis[2..];
}
/**
 * Funkcja pomocnicza, zwraca ko�c�wk� nazwy hitlokacji.
 *
 * @param hid   identyfikator hitlokacji
 * @param konc  tablica z ko�c�wkami
 *
 * @return ko�c�wk� nazwy hitlokacji
 */
private string
koncowka_hitlokacji(int hid, string *konc)
{
    int rodzaj = this_object()->query_hitloc_rodzaj(hid);

    if(rodzaj < 0)
    {
        if(rodzaj == -(PL_MESKI_OS+1))
            return konc[3];
        else
            return konc[4];
    }

    switch(rodzaj)
    {
        case PL_MESKI_OS:
        case PL_MESKI_NOS_ZYW:
        case PL_MESKI_NOS_NZYW:
            return konc[0];
        case PL_ZENSKI:
            return konc[1];
        case PL_NIJAKI_OS:
        case PL_NIJAKI_NOS:
            if(!konc[2])
                return konc[0];
            else
                return konc[2];
    }
}

/**
 * Funkcja pomocnicza, zwraca ko�c�wk� nazwy ataku.
 *
 * @param hid   identyfikator hitlokacji
 * @param konc  tablica z ko�c�wkami
 *
 * @return ko�c�wk� nazwy hitlokacji
 */
private string
koncowka_ataku(int aid, string *konc)
{
    int rodzaj = this_object()->query_attack_rodzaj(aid);

    if(rodzaj < 0)
    {
        if(rodzaj == -(PL_MESKI_OS+1))
            return konc[3];
        else
            return konc[4];
    }

    switch(rodzaj)
    {
        case PL_MESKI_OS:
        case PL_MESKI_NOS_ZYW:
        case PL_MESKI_NOS_NZYW:
            return konc[0];
        case PL_ZENSKI:
            return konc[1];
        case PL_NIJAKI_OS:
        case PL_NIJAKI_NOS:
            if(!konc[2])
                return konc[0];
            else
                return konc[2];
    }
}

/**
 * Funkcja pomocnicza cz�ciowo opisuj�ca zadany cios w takim stopniu w jakim
 * da si� to opisa� we wszystkich przypadkach, czyli przy zadaniu ciosu:)
 *
 * @param kto       kto zadaje cios.
 * @param komu      komu cios jest zadawany.
 * @param aid       identyfikator ataku kt�rym zadajemy cios
 * @param opis      pe�ny opis ciosu z miejscami do wype�nienia.
 * @param hid       identyfikator hitlokacji, w kt�r� trafi�o uderzenie.
 * @param pobr      procentowo wyra�one obra�enia.          (opt)
 * @param hitloc    hitlokacja w kt�re trafi�o uderzenie.   (opt)
 * @param b2        bro� drugiej osoby                      (opt)
 * @param tarcza    tarcza broni�cego                       (opt)
 * @param zbroja    zbroja broni�cego                       (opt)
 *
 * @return funkcja zwraca cz�ciow podmieniony string
 */
private nomask string *
formatuj_opis(object kto, object kogo, int aid, string opis, int hid, int pobr = 0,
    object b2 = 0, object tarcza = 0, object zbroja = 0)
{
    string *ex1, *ex2, *ex3, *opisy = ({}), opis2 = opis;
    mixed tmp;

    opis += "\n";

    //Najsampierw zr�bmy formatowanie niezale�ne od tego dla,
    //kogo przeznaczony jest opis.

    //Ko�c�wki potrzebne we wszystkich 3 wersjach wi�c bez r�nicy.
    ex1 = explode(opis, "[");

    opis = "";

    for(int i ; i < sizeof(ex1) ; i++)
    {
        ex2 = explode(ex1[i], "]");

        if(sizeof(ex2) < 2 && !(sizeof(ex2) == 1 && ex1[i][-1] == ']'))
        {
            opis += (i != 0 ? "[" : "") + ex1[i];
            continue;
        }

        ex3 = explode(ex2[0], "|");

        if(sizeof(ex3) < 2)
        {
            opis += (i != 0 ? "[" : "") + ex1[i];
            continue;
        }

        switch(ex3[-1])
        {
            case "%p":  tmp = kogo;     break;
            case "%k":  tmp = kto;      break;
            case "%m":  tmp = b2;       break;
            case "%z":  tmp = zbroja;   break;
            case "%t":  tmp = tarcza;   break;
            case "%b":  tmp = 1;        break;
            case "%h":  tmp = 2;        break;
            default:
                write("B��d ow1. \nex3[-1] = " + ex3[-1] + "\n\topis = \"" + opis2 + "\"\n");
        }

        if(objectp(tmp))
            opis += tmp->koncowkav(ex3[0..-2]) + (sizeof(ex2) > 1 ? ex2[1] : "");
        else if(tmp == 1)
            opis += koncowka_ataku(aid, ex3[0..-2]) + (sizeof(ex2) > 1 ? ex2[1] : "");
        else if(tmp == 2)
            opis += koncowka_hitlokacji(hid, ex3[0..-2]) + (sizeof(ex2) > 1 ? ex2[1] : "");
    }

    //Losowanie w 3 wersjach musi by� takie samo wi�c przed podzia�em

    ex1 = explode(opis, "{");

    opis = "";

    for(int i = 0 ; i < sizeof(ex1) ; i++)
    {
        ex2 = explode(ex1[i], "}");

        if(sizeof(ex2) < 2 && !(sizeof(ex2) == 1 && ex1[i][-1] == '}'))
        {
            opis += (i != 0 ? "{" : "") + ex1[i];
            continue;
        }

        ex3 = explode(ex2[0], "/");

        if(sizeof(ex3) < 2)
        {
            opis += (i != 0 ? "{" : "") + ex1[i];
            continue;
        }

        opis += ex3[random(sizeof(ex3))] + (sizeof(ex2) > 1 ? ex2[1] : "");
    }

    //Si�a ciosu nie jest r�nie widziana przez r�ne strony(WARNING a mo�e powinna??) wi�c okre�lamy
    //od razu
    ex1 = explode(opis, "(");

    opis = "";

    for(int i = 0 ; i < sizeof(ex1) ; i++)
    {
        int zm, k = -1, j;

        ex2 = explode(ex1[i], ")");

        if(sizeof(ex2) < 2 && !(sizeof(ex2) == 1 && ex1[i][-1] == ')'))
        {
            opis += (i != 0 ? "(" : "") + ex1[i];
            continue;
        }

        ex3 = explode(ex2[0], "|");

        if(sizeof(ex3) < 2)
        {
            opis += (i != 0 ? "(" : "") + ex1[i];
            continue;
        }

        zm = 100 / sizeof(ex3);

        while(j <= pobr)
        {
            j += zm;

            k++;
        }

        opis += ex3[k] + (sizeof(ex2) > 1 ? ex2[1] : "");
    }

    //A teraz robimy podzia�

    ex1 = explode(opis, "{");

    opisy = ({"", "", ""});

    for(int j = 0 ; j < 3 ; j++)
    {
        for(int i = 0 ; i < sizeof(ex1) ; i++)
        {
            ex2 = explode(ex1[i], "}");

            if(sizeof(ex2) < 2 && !(sizeof(ex2) == 1 && ex1[i][-1] == '}'))
            {
                opisy[j] += (i != 0 ? "{" : "") + ex1[i];
                continue;
            }

            ex3 = explode(ex2[0], "|");

            if(sizeof(ex3) < 2 && !(sizeof(ex3) == 1 && (ex2[0][-1] == '|' || ex2[0][0] == '|')))
            {
                opisy[j] += (i != 0 ? "{" : "") + ex1[i];
                continue;
            }

            if(sizeof(ex3) == 1)
                ex3 += ({""});

            if(sizeof(ex3) != sizeof(explode(ex2[0] + " ", "|")))
                ex3 += ({ex3[0]});

            if(j == 2 && sizeof(ex3) < 3)
                tmp = 1;
            else
                tmp = j;

            opisy[j] += ex3[tmp] + (sizeof(ex2) > 1 ? ex2[1] : "");
        }
    }

    opisy[0] = podstaw_hitlokacje(opisy[0], kogo, hid, kto);
    opisy[1] = podstaw_hitlokacje(opisy[1], kogo, hid, kogo);
    opisy[2] = podstaw_hitlokacje(opisy[2], kogo, hid, 0);
    opisy[0] = podstaw_atak(opisy[0], kto, aid, kto);
    opisy[1] = podstaw_atak(opisy[1], kto, aid, kogo);
    opisy[2] = podstaw_atak(opisy[2], kto, aid, 0);
    opisy[0] = podstaw_za(opisy[0], 'k', kto, kto);
    opisy[1] = podstaw_za(opisy[1], 'k', kto, kogo);
    opisy[2] = podstaw_za(opisy[2], 'k', kto, 0);
    opisy[0] = podstaw_za(opisy[0], 'p', kogo, kto);
    opisy[1] = podstaw_za(opisy[1], 'p', kogo, kogo);
    opisy[2] = podstaw_za(opisy[2], 'p', kogo, 0);

    DBG("Sformatowa�em opis \"" + opisy[0] + "\"\n");

    return opisy;
}

/**
 * Funkcja ma za zadanie opisa� trafiony cios.
 *
 * @param kto       kto zadaje cios.
 * @param komu      komu cios jest zadawany.
 * @param aid       identyfikator ataku kt�rym zadajemy cios
 * @param hid       identyfikator hitlokacji, w kt�r� trafi�o uderzenie.
 * @param dt        typ zadawanych obra�e�
 * @param pobr      procentowo wyra�one obra�enia.
 * @param bp        bro� przeciwnika                            (opt)
 *
 * @return 3 elementow� tablic� z opisanym ciosem: <ul>
 *  <li> 0 - dla @param kto </li>
 *  <li> 1 - dla @param kogo </li>
 *  <li> 2 - dla reszty </li>
 * </ul>
 */
nomask string *
opisz_cios(object kto, object kogo, int aid, int hid, int dt, int pobr, object bp = 0)
{
    return formatuj_opis(kto, kogo, aid, wybierz_opis_ciosu(dt, aid, kto), hid, pobr, bp);
}

/**
 * Funkcja ma za zadanie opisa� cios przyj�ty na zbroje.
 *
 * @param kto       kto zadaje cios.
 * @param komu      komu cios jest zadawany.
 * @param aid       identyfikator ataku kt�rym zadajemy cios
 * @param hid       identyfikator hitlokacji, w kt�r� trafi�o uderzenie.
 * @param dt        typ zadawanych obra�e�
 * @param zbroja    zbroja przez kt�r� uderzenie zosta�o wyparowane.
 * @param bp        bro� przeciwnika                            (opt)
 *
 * @return 3 elementow� tablic� z opisanym wyparowaniem przez zbroje: <ul>
 *  <li> 0 - dla @param kto </li>
 *  <li> 1 - dla @param kogo </li>
 *  <li> 2 - dla reszty </li>
 * </ul>
 */
nomask string *
opisz_wyparowanie(object kto, object kogo, int aid, int hid, int dt, object zbroja, object bp = 0)
{
    string *opisy = ({});

    opisy = formatuj_opis(kto, kogo, aid, wybierz_opis_wyparowania(dt, aid, kto), hid, 0, bp, 0, zbroja);

    opisy[0] = podstaw_za(opisy[0], 'z', zbroja, kto);
    opisy[1] = podstaw_za(opisy[1], 'z', zbroja, kogo);
    opisy[2] = podstaw_za(opisy[2], 'z', zbroja, 0);
    opisy[0] = podstaw_za(opisy[0], 'm', bp, kto);
    opisy[1] = podstaw_za(opisy[1], 'm', bp, kogo);
    opisy[2] = podstaw_za(opisy[2], 'm', bp, 0);

    return opisy;
}

/**
 * Funkcja ma za zadanie opisa� cios przyj�ty na tarcz�.
 *
 * @param kto       kto zadaje cios.
 * @param komu      komu cios jest zadawany.
 * @param aid       identyfikator ataku kt�rym zadajemy cios
 * @param hid       identyfikator hitlokacji, w kt�r� trafi�o uderzenie.
 * @param dt        typ zadawanych obra�e�
 * @param tarcza    tarcza na kt�r� przyj�ty zosta� cios.
 *
 * @return 3 elementow� tablic� z opisanym przyj�ciem na tarcz�: <ul>
 *  <li> 0 - dla @param kto </li>
 *  <li> 1 - dla @param kogo </li>
 *  <li> 2 - dla reszty </li>
 * </ul>
 */
nomask string *
opisz_tarczownictwo(object kto, object kogo, int aid, int hid, int dt, object tarcza)
{
    string *opisy = ({});

    opisy = formatuj_opis(kto, kogo, aid, wybierz_opis_tarczownictwa(dt, aid, kto), hid, 0, 0, tarcza);

    opisy[0] = podstaw_za(opisy[0], 't', tarcza, kto);
    opisy[1] = podstaw_za(opisy[1], 't', tarcza, kogo);
    opisy[2] = podstaw_za(opisy[2], 't', tarcza, 0);

    return opisy;
}

/**
 * Funkcja ma za zadanie opisa� cios odbity broni�.
 * Parowania s� mo�liwe tylko w przypadku kiedy obie walcz�ce osoby posiadaj� bro�.
 *
 * @param kto       kto zadaje cios.
 * @param komu      komu cios jest zadawany.
 * @param aid       identyfikator ataku kt�rym zadajemy cios
 * @param hid       identyfikator hitlokacji, w kt�r� trafi�o uderzenie.
 * @param dt        typ zadawanych obra�e�
 * @param bron      bro� odbijaj�ca cios
 *
 * @return 3 elementow� tablic� z opisanym parowaniem: <ul>
 *  <li> 0 - dla @param kto </li>
 *  <li> 1 - dla @param kogo </li>
 *  <li> 2 - dla reszty </li>
 * </ul>
 */
nomask string *
opisz_parowanie(object kto, object kogo, int aid, int hid, int dt, object bron)
{
    string *opisy = ({});

    opisy = formatuj_opis(kto, kogo, aid, wybierz_opis_parowania(dt, aid, kto), hid, 0, bron);

    opisy[0] = podstaw_za(opisy[0], 'm', bron, kto);
    opisy[1] = podstaw_za(opisy[1], 'm', bron, kogo);
    opisy[2] = podstaw_za(opisy[2], 'm', bron, 0);

    return opisy;
}

/**
 * Funkcja ma za zadanie opisa� unik.
 *
 * @param kto       kto zadaje cios.
 * @param komu      komu cios jest zadawany.
 * @param aid       identyfikator ataku kt�rym zadajemy cios
 * @param hid       identyfikator hitlokacji, w kt�r� trafi�o uderzenie.
 * @param dt        typ zadawanych obra�e�
 *
 * @return 3 elementow� tablic� z opisanym unikiem: <ul>
 *  <li> 0 - dla @param kto </li>
 *  <li> 1 - dla @param kogo </li>
 *  <li> 2 - dla reszty </li>
 * </ul>
 */
nomask string *
opisz_unik(object kto, object kogo, int aid, int hid, int dt)
{
    return formatuj_opis(kto, kogo, aid, wybierz_opis_uniku(dt, aid, kto), hid);
}