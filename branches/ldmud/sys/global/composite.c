/*
 *  Description routines, composite sentences
 *
 *
 * 2007-05-06 - Zmodyfikowa�em lekko funkcje lpc_describe oraz desc_same
 * tak, by ukryte obiekty by�y osobno w composicie od tych o tym samym
 * co prada shorcie, ale nieukrytych.
 * np. zamiast [dwie koszule] (przy czym jedna tylko byla ukryta)
 * mamy: koszula i [koszula].
 * 													Vera.
 */
#pragma save_binary

#include "/sys/language.h"
#include "/sys/stdproperties.h"
#include "/sys/macros.h"
#include "/sys/ss_types.h"

/*
 *  Globals
 */
mixed	*gLastObjects;
mixed	*OldArr; 	/* The old original array. */
mixed	*gLastArrayOfObjects; // Potrzebne //d
object	Fobject; 	/* The object for whom we describe the things. */
int	gPrzyp;		/* Przypadek */
string	*ktos = ({ "kto�", "kogo�", "komu�", "kogo�", "kim�", "kim�" });
string	*cos = ({ "co�", "czego�", "czemu�", "co�", "czym�", "czym�" });
string  *wiele = ({ "wiele", "wielu", "wielu", "wiele", "wieloma", "wielu" });

/*
 *  Prototypes
 */
string	composite(mixed arr, mixed sepfunc, string descfunc, object obj, mixed przyp);
string	lpc_describe(mixed uarr, string dfun, object dobj);
string  ext_short(object ob, object fob);
int     compute_amount(mixed arr);

static varargs string
desc_live_dead(mixed arr)
{
    return composite(arr, &ext_short(, Fobject), "desc_same", this_object(), -1);
}

mixed *get_last_objects() { return gLastObjects; }

void put_objects(object *arr) { OldArr = arr; }

varargs string
desc_live(mixed arr, mixed przyp, int no_someone)
{
    string str;

    Fobject = this_player();
    if (!intp(przyp))
        if (stringp(przyp))
            gPrzyp = atoi(przyp);
        else gPrzyp = 0;
    else
        gPrzyp = przyp;


    str = desc_live_dead(arr);
    if (!str && !no_someone)
        return ktos[gPrzyp];
    return str;
}

varargs string
desc_dead(mixed arr, mixed przyp, int no_something)
{
    string str;

    Fobject = this_player();
    if (!intp(przyp))
        if (stringp(przyp))
            gPrzyp = atoi(przyp);
        else gPrzyp = 0;
    else
        gPrzyp = przyp;
    str = desc_live_dead(arr);

    if (!str && !no_something)
	return cos[gPrzyp];
    return str;
}

varargs string
desc_sth(mixed arr, mixed przyp, int no_something)
{
    string str;

    Fobject = this_player();
    if (!intp(przyp))
        if (stringp(przyp))
            gPrzyp = atoi(przyp);
        else gPrzyp = 0;
    else
        gPrzyp = przyp;
    str = desc_live_dead(arr);

    if (!str && !no_something)
    {
        mixed arr1;
        if(sizeof(arr1 = filter(arr, &living())) >= sizeof(arr-arr1))
            return ktos[gPrzyp];
        else
            return cos[gPrzyp];
    }
    return str;
}

//d
string
desc_live_vbfc(...)
{
    return desc_live(map(argv[..-2], find_object), atoi(argv[sizeof(argv) - 1]));
}

//d
string
desc_dead_vbfc(...)
{
    return desc_dead(map(argv[..-2], find_object), atoi(argv[sizeof(argv) - 1]));
}

//d
string
desc_sth_vbfc(...)
{
    return desc_dead(map(argv[..-2], find_object), atoi(argv[sizeof(argv) - 1]));
}

public string
fo_desc_live(mixed arr, object for_ob, mixed przyp)
{
    string str;

    if (!for_ob)
	for_ob = previous_object(-1);
    if (!arr)
        arr = OldArr;

    Fobject = for_ob;
    if (!intp(przyp))
        if (stringp(przyp))
            przyp = atoi(przyp);
        else
            przyp = PL_MIA;

    gPrzyp = przyp;

    str = desc_live_dead(arr);
    if (!str)
	return ktos[gPrzyp];
    return str;
}

public string
fo_desc_dead(mixed arr, object for_ob, mixed przyp)
{
    string str;

    if (!for_ob)
        for_ob = previous_object(-1);

    if (!arr)
        arr = OldArr;

    Fobject = for_ob;
    if (!intp(przyp))
        if (stringp(przyp))
            przyp = atoi(przyp);
        else przyp = 0;
    gPrzyp = przyp;

    str = desc_live_dead(arr);
    if (!str)
	return cos[gPrzyp];
    return str;
}

public string
fo_desc_sth(mixed arr, object for_ob, mixed przyp)
{
    string str;

    if (!for_ob)
    for_ob = previous_object(-1);
    if (!arr)
        arr = OldArr;

    Fobject = for_ob;
    if (!intp(przyp))
        if (stringp(przyp))
            przyp = atoi(przyp);
        else
            przyp = PL_MIA;

    gPrzyp = przyp;

    str = desc_live_dead(arr);
    if (!str)
    return ktos[gPrzyp];
    return str;
}

public string
qcomp_live(mixed przyp)
{
    string str;

    Fobject = vbfc_object();

    if (!intp(przyp))
        if (stringp(przyp))
            przyp = atoi(przyp);
        else przyp = 0;
    gPrzyp = przyp;

    str = desc_live_dead(OldArr);
    if (!str)
	return ktos[gPrzyp];
    return str;
}

public string
qcomp_dead(mixed przyp)
{
    string str;

    Fobject = vbfc_object();

    if (!intp(przyp))
        if (stringp(przyp))
            przyp = atoi(przyp);
        else przyp = 0;
    gPrzyp = przyp;

    str = desc_live_dead(OldArr);
    if (!str)
	return cos[gPrzyp];
    return str;
}

public string
qcomp_sth(mixed przyp)
{
    string str;

    Fobject = vbfc_object();

    if (!intp(przyp))
        if (stringp(przyp))
            przyp = atoi(przyp);
        else przyp = 0;
    gPrzyp = przyp;

    str = desc_live_dead(OldArr);
    if (!str)
    return cos[gPrzyp];
    return str;
}

/*
 * Function:    desc_same
 * Description: Gives a textdescription of a set of nonunique objects.
 *              Normal case: "two swords", "a box" etc.
 *              The basic function to get the description of an objects is
 *              its 'short' function. Pluralforms are taken from:
 *              1) The 'plural_short' function of the object.
 *              2) If 1) is not defined then the singular form is transformed
 *                 into the pluralform, not always correctly.
 *              Articles are added to singular objects that does NOT have a
 *              capitalized short description and that are not heap objects.
 * Arguments:   oblist: Array of objectpointers to the nonunique objects.
 * Return:      Description string or 0 if no 'short' was not defined.
 */
string
desc_same(object *oblist) /* Used as dfun to composite() for composite_live */
{
    string str, sh, pre, aft;
    object ob;
    int wie, ile;
    mixed wiel;

    if (!sizeof(oblist))
        return "";

    pre = ""; aft = "";
    ob = oblist[0];

    if(!interactive(ob))
    {
        if (ob->query_prop(OBJ_I_HIDE))
        {
            pre = "[";
            aft = "]";
        }
        if (ob->query_prop(OBJ_I_INVIS))
        {
            pre = "(";
            aft = ")";
        }
    }

    sh = ob->short(Fobject, gPrzyp);
        //write("---\n");
    ile = compute_amount(oblist);
    if (ile > 1)
    {
        if((ob->query_herb() && ob->query_rosnie()) && (wiel=ob->query_slowo_grupy()))
            if(ile > wiel[1] && wiel)
                wie = 2;
        else if (ile > (Fobject->query_stat(SS_INT) / 2))
            wie = 1;

        //si� kurwa naszuka�em �adnych kilka godzin, przez jebane "[dwie brzozas]"
        //i "[pi�tna�cie groszies]", a wystarczy�o doda� to! Vera, 4 w nocy.
        //ob->odmien_plural_short(); //Zwalaj na innych Veru� zwalaj...
        // Oto i sprawdza dwie ludzie zamiast dwie kobiety!

        //    if(!living(ob))
        //        ob->odmien_plural_short();

        //FIXME: To tylko chwilowe obej�cie problemu! Jak najszybciej trzeba to zmieni�!
        // Jak kto� b�dzie mia� chwilke to niech na to popatrzy.
        // Trzeba troche pomiesza� w check_seen, bo to ono blokuje zwracanie plural_shorta.
        // W shorcie nie ma check_seen'a dlatego zwraca dobrze.. Nie mniej, trzaba sprawdzi�
        // check_seena


        str = ob->plural_short(!wie ? LANG_PRZYP(ile, gPrzyp, ob->query_rodzaj()) : PL_DOP);

        if (!stringp(str) && !stringp(sh))
            return 0;
        if (!str)
            str = sh;

        if (!wie)
            return pre + LANG_SNUM(ile, gPrzyp, ob->query_rodzaj()) + " " + str + aft;
        else if(wie == 2)
            return pre + wiel[0][gPrzyp] + " " + str + aft;
        else
            return pre + wiele[gPrzyp] + " " + str + aft;
    }
    else if (!stringp(sh))
        return 0;
    else if (ob->query_prop(HEAP_I_IS)) {//write("c\n");
        return pre + sh + aft;
	}
    else {//write("d\n");
        return pre + sh + aft;
	}
}

/*
 * Function:    composite
 * Description: Creates a composite description of objects
 * Arguments:   arr:        Array of the objects to describe
 *              sepfunc:    Function to call in objects to get its <name>
 *                          Objects with the same <names> are described
 *                          together.
 *              descfunc:   Function to call to get the actual description of
 *                          a group of 'same objects' i.e objects whose
 *                          sepfunc returned the same value.
 *              obj:        Object to call descfunc in.
 *
 * Returns:     A description string on the format:
 *              <desc>, <desc> and <desc>
 *              Where <desc> is what obj->descfunc() returns
 *
 */
varargs string
fo_composite(mixed arr, string sepfunc, string descfunc, object obj,
        object for_obj, mixed przyp, int i)
{
    if (!for_obj)
        for_obj = previous_object();

    Fobject = for_obj;
    if (!intp(przyp))
        if (stringp(przyp))
            gPrzyp = atoi(przyp);
        else gPrzyp = 0;
    else
        gPrzyp = przyp;
    return composite(arr, sepfunc, descfunc, obj, -1);
}

int
exclude_no_show(object ob)
{
    if (!ob || ob->query_no_show_composite())
	return 0;
    return 1;
}

int
exclude_invis(object ob)
{
    if (Fobject && !Fobject->query_wiz_level() &&
	    Fobject->query_prop(LIVE_I_SEE_INVIS) < ob->query_prop(OBJ_I_INVIS))
	return 0;
    return 1;
}

int
exclude_hidden(object ob)
{
    //Kto� mi wyt�umaczy dlaczego tu jest random??? I jak on to widzi...
    //Bo jak to KURWA mo�e dzia�a� dobrze skoro tu losuje inn� warto��
    //A przy sprawdzaniu czy mo�e pokaza� plural_shorta(check_seen)
    //wylosuje inn� warto��... I jeszcze zwalajcie kurde na mnie!:P
    //Ja b�d� takich b��d�w g�upich szuka�! Ehhh.. (Krun)
    //Wkurwiony jestom O!

    //Wywali�em ca�y poprzedni kod bo by�o za bardzo na�miecone... Jak co� to z svn'a czerpa�:P

    if(ob->check_seen(Fobject))
        return 1;
}

string
composite_sub(mixed arr, mixed sepfunc, string descfunc, object obj, mixed przyp)
{
    mixed *a;

    gLastObjects = ({ "** ERROR **" });
    Fobject = Fobject || TP;
    if (przyp != -1)
        if (!intp(przyp))
        {
            if (stringp(przyp))
            {
                gPrzyp = atoi(przyp);
            }
            else gPrzyp = 0;
        }
        else
            gPrzyp = przyp;

    if (objectp(arr))
	arr = ({ arr });

    arr = filter(arr, exclude_no_show);
    OldArr = arr;
    arr = filter(arr, exclude_invis);
    arr = filter(arr, exclude_hidden);

    if ((!pointerp(arr)) || (sizeof(arr) < 1))
    {
	Fobject = 0;
	return 0;
    }
    gLastArrayOfObjects = arr;

    /* Make an array of unique lists of objects
     */
    a = unique_array(arr, sepfunc);
    gLastObjects = a;
    return lpc_describe(a, descfunc, obj);
}

varargs string
composite(mixed arr, mixed sepfunc, string descfunc, object obj, mixed przyp)
{
    mixed a, b;
    int i;

    if (objectp(arr)) arr = ({ arr });
//write("-----b---------------------\n");
    a = unique_array(arr, "extra_desc_unique_fun");

    b = ({ });
    for (i = 0; i < sizeof(a); i++)
	b += ({ composite_sub(a[i], sepfunc, descfunc, obj, przyp) });
//dump_array(b);
    if (sizeof(b))
	return this_object()->composite_words(b, " oraz ");
    return 0;
}

/*
 * Function:    sort_similar
 * Description: sort an array in order shown to player
 * Arguments:   arr:        Array of the objects to sort
 *              sepfunc:    Function to call in objects to get its <name>
 *                          Objects with the same <names> are sorted
 *                          together.
 *
 * Returns:     0 or sorted array
 *
 */
mixed
sort_similar(mixed arr, string sepfunc)
{
    int b, i;
    mixed *a;

    if ((!pointerp(arr)) || (sizeof(arr) < 1)) return 0;

    a = unique_array(arr, sepfunc);

    if (!sizeof(a))
	return 0;

    b = a[0];
    for (i = 1; i < sizeof(a); i++)
	b += a[i];

    return b;

/*
    a = map(arr, &map_fun(, sepfunc));
    a = sort_array(a, "sort_fun", this_object());
    a = map(a, second_fun);
    return a;
*/
}


second_fun(arr) { return arr[1]; }

#if 0  /* If unique_array does not work */
/*
  unique300

   Input: array of objects
   Output: array of unique objects: ({
                                       ({uniqueobj, copyobj,...}),
                                       ({uniqueobj, copyobj,...}),
                                       ({uniqueobj, copyobj,...})
				    })
*/
#define S(a, b) sort_array(a, b, this_object())


unique300(arr, sepfunc)
{
    int a, b, c, d;

    if ((!pointerp(arr)) || (sizeof(arr) < 1))
	return 0;
    a = map(arr, &map_fun(, sepfunc));
    a = S(a, "sort_fun");
    if (a[0][1] == 0)
	a = slice_array(a, 1, sizeof(a));
    if ((!pointerp(arr)) || (sizeof(arr)<1))
	return 0;

    b = ({ ({ a[0][1] }) });
    for (c = 1, d = 0; c < sizeof(a); c++) {
	if (a[c][0] == a[c - 1][0])
	    b[d] = b[d] + ({ a[c][1] });
	else
	{
	    d++;
	    b = b + ({ ({ a[c][1] }) });
	}
    }
    return b;
}
#endif

int
sort_fun(int *a, int *b)
{
    if (a[0] < b[0]) return -1;
    if (a[0] == b[0]) return 0;
    return 1;
}

mixed
map_fun(mixed ob, string f)
{
    if (objectp(ob))
	return ({ call_other(ob, f), ob });
    else
	return ({ "", 0 });
}

int
no_invis(mixed str)
{
    return (stringp(str));
}

string
lpc_describe(mixed uarr, string dfun, object dobj)
{
//Zmiany by Vera.

// To ja poprosze o t�umacza!!!!!! (krun)

//write("-------------------lpc_Describe uarr:===============\n");
//dump_array(uarr);write("\n\n\n");
mixed *hidden_tmp = map(uarr,&filter(,&operator(!=)(0) @ &->query_prop(OBJ_I_HIDE)));
hidden_tmp = filter(hidden_tmp,&operator(!=)(0) @ sizeof);
uarr = map(uarr,&filter(,&operator(==)(0) @ &->query_prop(OBJ_I_HIDE)));
uarr = filter(uarr,&operator(!=)(0) @ sizeof);
//write("-------------------hidden_tmp:===============\n");
//dump_array(hidden_tmp);
uarr+=hidden_tmp;
//write("\n\n\n-------------------kszzzzzzzzzzzzzzzzzzzzzzz===============\n");
//dump_array(uarr);write("\n\n\n");
//write("dfun: "+dfun+" dobj: "+dobj->short(0)+"\n");
    mixed *a;
    string tmp;
    a = map(uarr, dfun, dobj);
    a = filter(a, no_invis);
    Fobject = 0;
    if (!a || sizeof(a) == 0)
	return 0;
    else {
	tmp = oblicz_koncowki(gLastArrayOfObjects[0]->query_prop(OBJ_S_EXTRA_DESC),
				gLastArrayOfObjects);
	if (strlen(tmp) && gPrzyp == PL_MIA)
	    tmp = sprintf(tmp, this_object()->composite_words(a));
	else tmp = this_object()->composite_words(a);
	return tmp;
    }
}

string
composite_words(string *wlist, string lacznik = " i ")
{
    int size = sizeof(wlist);

    if (!size)
        return "";

    if (size == 1)
        return wlist[0];

    if (size == 2)
        return wlist[0] + lacznik + wlist[1];

    return implode(wlist[0..-2], ", ") + lacznik + wlist[-1];
}

string *
decomposite_words(string str, string lacznik = " i ")
{
    string *ret;

    if (!stringp(str))
	return ({ });

    ret = explode(str, lacznik);

    if (sizeof(ret) == 1)
	return ret;

    return explode(ret[0], ", ") + ({ implode(ret[1..], " i ") });
}

string
ext_short(object ob, object fob)
{
    if(ob->query_prop(HEAP_I_IS))
        return ob->singular_short(fob);
    else
        return ob->short(fob);
}

int
compute_amount(mixed arr)
{
    int toReturn = 0;
    if (!pointerp(arr)) {
        return 0;
    }
    foreach (mixed ob : arr) {
        if (ob->query_prop(HEAP_I_IS)) {
            toReturn += ob->num_heap();
        }
        else {
            toReturn += 1;
        }
    }
    return toReturn;
}
