/**
 *  \file /sys/mudtime.h
 *
 * Definicje wspierajace uzywanie kalendarza i ruchow cial niebieskich.
 *
 *  dzielo Silvathraeca 1997 poprawione i przerobione przez Jeremiana.
 */

#ifndef MUDTIME_DEF
#define MUDTIME_DEF

#define MUDTIME_FILE    "/sys/global/mudtime"
#define MUDTIME_FILE2   "/sys/global/mudtime2"
#define KLUDZKI_FILE    "/sys/global/kalendarz_ludzki"

/* Kalendarze */
#define MT_KALENDARZ_ELFI       (1 << 0)
#define MT_KALENDARZ_LUDZKI     (1 << 1)

/* Pory dnia */

#define MT_SWIT                 11  /* Przed wschodem slonca */
#define MT_WCZESNY_RANEK        12  /* Po wschodzie slonca */
#define MT_RANEK                13  /* Przed poludniem */
#define MT_POLUDNIE             14  /* W poludnie */
#define MT_POPOLUDNIE           15  /* Po poludniu */
#define MT_WIECZOR              16  /* Przed zachodem slonca */
#define MT_POZNY_WIECZOR        17  /* Po zachodzie slonca */
#define MT_NOC                  18  /* W nocy */

/* Pory roku */

#define MT_WIOSNA               21
#define MT_LATO                 22
#define MT_JESIEN               23
#define MT_ZIMA                 24

/* Kierunki geograficzne (polozenie cial niebieskich) */

#define MT_N                    51
#define MT_NE                   52
#define MT_E                    53
#define MT_SE                   54
#define MT_S                    55
#define MT_SW                   56
#define MT_W                    57
#define MT_NW                   58

#define MT_ZENIT                61
#define MT_NADIR                62

#define MT_POD_HORYZONTEM       71

/* Fazy ksiezyca */

#define MT_PELNIA               81  /* W okolicy pelni */
#define MT_PO_PELNI             82
#define MT_PIERWSZA_KWADRA      83  /* Malejacy */
#define MT_PRZED_NOWIEM         84
#define MT_NOW                  85  /* Niewidoczny */
#define MT_PO_NOWIU             86
#define MT_OSTATNIA_KWADRA      87  /* Rosnacy */
#define MT_PRZED_PELNIA         88

/* Szerokosci geograficzne */

#define MT_BIEGUN_N             91  /* Polnocna strefa biegunowa */
#define MT_POLKULA_N            92  /* Polnocna strefa umiarkowana */
#define MT_ROWNIK               93  /* Strefa rownikowa */
#define MT_POLKULA_S            94  /* Poludniowa strefa umiarkowana */
#define MT_BIEGUN_S             95  /* Poludniowa strefa biegunowa */

/* Kalendarze */
#define MT_K_ELFI               (1 << 0)    /* Kalendarz elfi */
#define MT_K_LUDZKI             (1 << 1)    /* Kalendarz ludzki */

/* Miesi�ce */

//Elfie:

#define MT_M_ELFI_YULE              "Yule"
#define MT_M_ELFI_IMBAELK           "Imbeaelk"
#define MT_M_ELFI_BIRKE             "Birke"
#define MT_M_ELFI_BELLETEYN         "Belleteyn"
#define MT_M_ELFI_MIDAETE           "Midaete"
#define MT_M_ELFI_LAMMAS            "Lammas"
#define MT_M_ELFI_VELEN             "Velen"
#define MT_M_ELFI_SAOVINE           "Saovine"

#define MT_MIESIACE_ELFIE   ({MT_M_ELFI_YULE, MT_M_ELFI_IMBAELK, MT_M_ELFI_BIRKE,           \
                              MT_M_ELFI_BELLETEYN, MT_M_ELFI_MIDAETE, MT_M_ELFI_LAMMAS,     \
                              MT_M_ELFI_VELEN, MT_M_ELFI_SAOVINE})

#define MT_MIESIAC_ELFI(x)  MT_MIESIACE_ELFIE[x]

//Ludzkie:
// przy czym dzie� pocz�tkowy to 19 elementowa tablica dni w
// poszczeg�lnych latach.
#define MT_M_LUDZKI_STYCZEN         "Ianuarius"
#define MT_M_LUDZKI_LUTY            "Februarius"
#define MT_M_LUDZKI_MARZEC          "Martius"
#define MT_M_LUDZKI_KWIECIEN        "Aprilis"
#define MT_M_LUDZKI_MAJ             "Maius"
#define MT_M_LUDZKI_CZERWIEC        "Iunius"
#define MT_M_LUDZKI_LIPIEC          "Quintilis"
#define MT_M_LUDZKI_SIERPIEN        "Sextilis"
#define MT_M_LUDZKI_WRZESIEN        "September"
#define MT_M_LUDZKI_PAZDZIERNIK     "October"
#define MT_M_LUDZKI_LISTOPAD        "November"
#define MT_M_LUDZKI_GRUDZIEN        "December"
#define MT_M_LUDZKI_INTERCALARIS    "Intercalaris"

#define MT_MIESIACE_LUDZKIE     ({MT_M_LUDZKI_STYCZEN, MT_M_LUDZKI_LUTY, MT_M_LUDZKI_MARZEC,     \
                                  MT_M_LUDZKI_KWIECIEN, MT_M_LUDZKI_MAJ, MT_M_LUDZKI_CZERWIEC,   \
                                  MT_M_LUDZKI_LIPIEC, MT_M_LUDZKI_SIERPIEN,                      \
                                  MT_M_LUDZKI_WRZESIEN, MT_M_LUDZKI_PAZDZIERNIK,                 \
                                  MT_M_LUDZKI_LISTOPAD, MT_M_LUDZKI_GRUDZIEN,                    \
                                  MT_M_LUDZKI_INTERCALARIS})

#define MT_MIESIAC_LUDZKI(x)    MT_MIESIACE_LUDZKIE[x]

/* �wi�ta */

// �wi�ta elfie s� pierwszego ka�dego miesi�ca wi�c i nazywaj� si� tak jak miesi�ce wi�c
// nie ma potrzeby ich osobnego definiowania

#define MT_SWIETA_LUDZKIE ({})

/* Funkcje */

#define MT_PORA_DNIA_STR(x)     (MUDTIME_FILE->pora_dnia_str(x))
#define MT_PORA_ROKU_STR(x)     (MUDTIME_FILE->pora_roku_str(x))
#define MT_KIERUNEK_STR(x)      (MUDTIME_FILE->kierunek_str(x))
#define MT_FAZA_X_STR(x)        (MUDTIME_FILE->faza_ksiezyca_str(x))


#define MT_PORA_DNIA_STRB(x)    (MUDTIME_FILE->pora_dnia_strb(x))
#define MT_FAZA_X_STRB(x)       (MUDTIME_FILE->faza_ksiezyca_strb(x))

#define MT_POLOZENIE(x, y)      (MUDTIME_FILE->polozenie_ciala(x, y))
#define MT_POLOZENIE_LONG(x, y) (MUDTIME_FILE->polozenie_ciala_long(x, y))
#define MT_FAZA_X_LONG(x, y, z) (MUDTIME_FILE->faza_ksiezyca_long(x, y, z))

#define MT_CZAS_MUDOWY          (MUDTIME_FILE2->czas_na_mudzie())
#define MT_DATA_MUDOWA          (MUDTIME_FILE2->data_na_mudzie())
#define MT_PORA_DNIA            (MUDTIME_FILE2->pora_dnia())
#define MT_PORA_DNIAXY(x,y)     (MUDTIME_FILE2->pora_dnia(x,y))
#define MT_PORA_ROKU            (MUDTIME_FILE2->pora_roku())

#define MT_DZIEN                (MUDTIME_FILE2->query_day())
#define MT_GODZINA              (MUDTIME_FILE2->query_hour())
#define MT_NHOUR                (MUDTIME_FILE2->query_nhour())
#define MT_MINUTA               (MUDTIME_FILE2->query_minute())
#define MT_SEKUNDA              (MUDTIME_FILE2->query_second())

/* Koniec definicji... */
#endif MUDTIME_DEF
