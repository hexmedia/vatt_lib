/**
 * \file /sys/livings.h
 *
 * Tu klika definicji u�ywanych w livingach.
 *
 * @author Krun
 * @date Stycze� 2007
 */

#ifndef LIVINGS_H_DEFS
#define LIVINGS_H_DEFS

#define LV_DUCH     ({"duch", "ducha", "duchowi", "ducha", "duchem", "duchu"})
#define LV_DUCHY    ({"duchy", "duch�w", "duchom", "duchy", "duchami", "duchach"})
#define LV_STATUA   ({"rze�ba", "rze�by", "rze�bie", "rze�be", "rze�b�", "rze�bie"})
#define LV_STATUY   ({"rze�by", "rze�b", "rze�b�", "rze�by", "rze�bami", "rze�bach"})

#define LV_STANDARD_SUBLOCS ({"_subloc_misc_extra", "fryzura_na_glowie", "kwiaty_we_wlosach", "_eyes_closed",    \
        "subloc_zarost", "subloc_tatuaze", "wielded", "worn_a"})

#endif LIVINGS_H_DEFS