#define SIT_SIEDZACY        PLAYER_M_SIT_SIEDZACY
#define SIT_LEZACY          PLAYER_M_SIT_LEZACY

#define SIT_NIE_WSTAN       "_sit_nie_wstan"
#define PLAYER_O_SIEDZE     "_player_o_siedze"
#define PLAYER_O_LEZE       "_player_o_leze"
#define SIT_I_NUM_SIE       "_sit_i_num_sie"
#define SIT_I_NUM_LEZ       "_sit_i_num_lez"

/*Defaultowe miejsca, czyli te z wi�kszym priorytetem - gracz ma wi�ksz� szanse wylosowa� je przy upadku*/
#define DEFAULT_SIT_PLACES ({"na * ziemi", "na ziemi", "na * parkiecie", "na parkiecie", "na * pod�odze", "na pod�odze", "na * trakcie", "na trakcie", "na * dukcie", "na dukcie", "na * drodze", "na drodze", "na * deskach", "na deskach", "na * �cie�ce", "na �cie�ce", "na * �ci�ce", "na �ci�ce", "na * bruku", "na bruku", "na * p�ytach", "na * trawie", "na trawie"})

