/*
 * Jest to wilk eventowy. Pojawia si� (w stadzie) on na zmian� z ghoulem
 * (/d/Standard/Redania/Rinde/npc/ghoul.c)
 * tam, gdzie jest du�o cia� i s� spe�nione odpowiednie warunki
 * (w /std/corpse).
 * Event, acty i reszte opracowa� Vera, za� wilka opisa� i tchn�� w niego �ycie Ohm
 *
 * Koniec czerwca, roku pa�skiego 2007, niby lato, ale pi�dzi i kurewsko zimno
 */

inherit "/std/creature";
inherit "/std/combat/unarmed";
inherit "/std/act/attack";
inherit "/std/act/action";
inherit "/std/act/trigaction.c";
inherit "/std/act/domove";

#include <macros.h>
#include <stdproperties.h>
#include <pl.h>
#include <formulas.h>
#include <std.h>
#include <composite.h>
#include <filter_funs.h>
#include <cmdparse.h>
#include <mudtime.h>
#include <wa_types.h>
#include <ss_types.h>
#include <object_types.h>

object macierzysta_lok;
void spadamy();

int
czy_atakowac()
{
    if(TP->is_humanoid() && TP->query_rasa() != "wilk")
        return 1;
    else
        return 0;
}

string
zjedz()
{
    if(TO->query_attack())
        return "";

    object *trupki = filter(all_inventory(ENV(TO)),&->query_corpse());

    string msg = "";

    if(!sizeof(trupki))
        return "";
    else
    {
        object ktory = trupki[random(sizeof(trupki))];
        switch(random(8))
        {
            case 0:
                msg = QCSHORT(TO,PL_MIA)+" odrywa spory kawa� mi�sa z "+
                    QSHORT(ktory,PL_DOP)+".\n";
                break;
            case 1:
                msg = QCSHORT(TO,PL_MIA)+" gryzie �apczywie "+QSHORT(ktory,PL_BIE)+".\n";
                break;
            case 2:
                msg = QCSHORT(TO,PL_MIA)+" rzuca si� na "+QSHORT(ktory,PL_BIE)+
                    " i z szale�czym zapa�em zaczyna je po�era�.\n";
                break;
            case 3:
                //msg=QCSHORT(TO,PL_MIA)+" �lini�c si�, bierze obgryzion� ko��, "+
                //"po czym zaczyna wysysa� z niej szpik.\n";
                break;
            case 4:
                msg=QCSHORT(TO,PL_MIA)+" agresywnymi ruchami szcz�k odrywa "+
                    "kawa�ek cia�a, kt�ry po chwili znika w jego paszczy.\n";
                break;
            case 5:
                msg=QCSHORT(TO,PL_MIA)+" pochylaj�c si� nad ofiar�, "+
                    "zaczyna j� powoli obw�chiwa�.\n";
                break;
            case 6:
                msg=QCSHORT(TO,PL_MIA)+" z oczami pe�nymi nigdy nienasyconego "+
                    "g�odu po�yka k�s za k�sem.\n";
                break;
            case 7:
                msg="Rozchlapuj�c dooko�a lepk� ma�, kt�ra "+
                    "zapewne jest �lin�, "+QSHORT(TO,PL_MIA)+" gwa�townymi ruchami"+
                    " szarpie resztki cia�a.\n";
                break;
        }
    }

    tell_roombb(ENV(TO), msg);

    set_alarm(itof(random(5)+8),0.0,"zjedz");
    return "";
}

void
create_creature()
{
    add_prop(CONT_I_WEIGHT, 35000+random(15000));
    add_prop(CONT_I_HEIGHT, 40+random(20));
    add_prop(NPC_I_NO_RUN_AWAY, 0);
    set_stats (({ 30+random(30), 50+random(40), 40+random(40), 20+random(20), 20, 50+random(10)}));
    set_skill(SS_DEFENCE, 25+random(10));
    set_skill(SS_UNARM_COMBAT, 40+random(20));
    set_skill(SS_AWARENESS, 34+random(10));
    set_aggressive(&czy_atakowac());
    set_attack_chance(75+random(5));
    ustaw_odmiane_rasy("wilk");
    set_gender(G_MALE);

    random_przym("srebrzysty:srebrzy^sci ciemnoszary:ciemnoszarzy "+
             "jasny:ja^sni ciemny:ciemni "+
             "wyg^lodnia^ly:wyg^lodniali||"+
             "agresywny:agresywni nerwowy:nerwowi dziki:dzicy "+
            "gro^xny:gro^xni wychudzony:wychudzeni",2);

    set_long("Srebrnoszara ^sier^s^c pokrywaj^aca muskularne cia^lo "+
            "tego osobnika jest gruba i b�yszcz^aca. Pysk wyposa^zony w dwa "+
            "ostre k�y i rz^ad ostrych siekaczy,  wygladaj^a na mordercze "+
            "narz^edzie potrafi^ace przebi^c si^e nawet przez grub^a skorzan^a "+
            "zbroj^e my^sliwego. Spr^e^zysto^s^c ruch^ow ^l^aczy si^e idealnie "+
            " z du^za si^la, tworz^ac z tego zwierz^ecia ^swietnego ^lowc^e i "+
            "gro^znego przeciwnika.\n");

    set_attack_unarmed(0, 20, 22, W_SLASH,  33, "�apa", "lewy", "pazury");
    set_attack_unarmed(1, 20, 22, W_SLASH,  33, "�apa", "prawy", "pazury");
    set_attack_unarmed(1, 20, 22, W_IMPALE, 34, "szcz�ki");

    set_hitloc_unarmed(0, ({ 1, 1, 1 }), 10, "�eb");
    set_hitloc_unarmed(1, ({ 1, 1, 1 }), 30, "tu��w");
    set_hitloc_unarmed(2, ({ 1, 1, 1 }), 20, "�apa", ({"przedni", "lewy"}));
    set_hitloc_unarmed(3, ({ 1, 1, 1 }), 20, "�apa", ({"przedni", "prawy"}));
    set_hitloc_unarmed(4, ({ 1, 1, 1 }), 10, "�apa", ({"lewy", "tylny"}));
    set_hitloc_unarmed(5, ({ 1, 1, 1 }), 10, "�apa", ({"prawy", "tylny"}));

    set_cact_time(10);

    add_cact("warknij");
    add_cact("emote wyje przeci^agle.");
    set_act_time(10);
    add_act("emote podnosi do g^ory g^low^e i wyje g^lo^sno.");
    add_act("emote rozgl^ada si^e niespokojnie.");
    add_act("emote podnosi g^low^e i w^eszy uwa^znie.");

    //add_act(&zjedz());
    //add_act(&zjedz());

    set_alarm(itof(random(800)+860),0.0,"spadamy");
    set_alarm(2.0, 0.0, "zjedz");

    add_leftover("/std/leftover", "kie^l", 4, 0, 1, 1, 0, O_KOSCI);
    add_leftover("/std/leftover", "nerka", 1, 0, 0, 0, 0, O_JEDZENIE);
}

void
spadamy()
{
    if(query_attack())
    {
        set_alarm(150.0,0.0,"spadamy");
        return;
    }

    if(!query_leader())
    {
        //usuwamy cia�a
        object *trupki = filter(all_inventory(ENV(TO)),&->query_corpse());

        if(sizeof(trupki))
            foreach(object x : trupki)
                x->remove_object();

        if(sizeof(query_team_others()))
        {
            tell_room(ENV(TO),"Nasycone "+COMPOSITE_LIVE(query_team(),PL_BIE)+
                " odchodz� w nieznane.\n");
            foreach(object x : query_team())
                x->remove_object();
            TO->remove_object();
        }
        else
        {
            tell_room(ENV(TO),"Nasycony "+QSHORT(TO,PL_BIE)+" odchodzi "+
                "w nieznane.\n");
            remove_object();
        }
    }
}

void
przestan_sledzic()
{
    unset_follow();

    string *drozka = znajdz_sciezke(ENV(TO),macierzysta_lok);

    foreach(string x : drozka)
        command(x);
}

void
attacked_by(object wrog)
{
    ::attacked_by(wrog);

    if(!query_leader()) //jeste�my liderem, lub samotnikiem
    {
        macierzysta_lok = ENV(TO);
        set_follow(wrog,itof(random(3)+1) / 10.0);
        set_alarm(itof(random(5)+2),0.0,"przestan_sledzic");
    }
}
