inherit "/std/drzewo/kloda";

#include <materialy.h>
#include <object_types.h>

void create_kloda()
{
	ustaw_nazwe("kloda");
	dodaj_przym("bary�kowaty", "bary�kowaci");
	set_long("K�oda ma charakterystyczny, lekko beczu�kowaty"
			+" kszta�t - jest stosunkowo gruba jak na swoj� d�ugo��."
			+" Na jednym z jej ko�c�w zauwa�asz kilka jasnych p�at�w -"
			+" to zapewne pozosta�o�ci po odci�tych konarach.\n");
	
	ustaw_material(MATERIALY_DR_DAB, 100);
	set_type(O_DREWNO);
	
	setuid();
	seteuid(getuid());
}