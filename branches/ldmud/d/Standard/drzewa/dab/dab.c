inherit "/std/drzewo";

#include <materialy.h>
#include <mudtime.h>
#include <object_types.h>
#include <pl.h>
#include <stdproperties.h>

void create_tree()
{
	ustaw_nazwe(({"d�b", "d�bu", "d�bowi", "d�b", "d�bem", "d�bie"}),
		    ({"d�by", "d�b�w", "d�bom", "d�by", "d�bami", "d�bach"}),
		    PL_MESKI_NZYW);

	random_przym("kszta�tny:kszta�tni||powykr�cany:powykr�cani||roz�o�ysty:roz�o�y�ci||"
				+ "omsza�y:omszali", 1);

	set_long("@@dlugi@@");

	set_type(O_DREWNO);
	ustaw_material(MATERIALY_DR_DAB, 100);

	set_gestosc(0.75);
	add_prop(OBJ_I_WEIGHT, random(1450000)+50000);

	set_sciezka_galezi("/d/Standard/drzewa/dab/galaz.c");
	set_sciezka_klody("/d/Standard/drzewa/dab/kloda.c");
	set_cena(4.95);
		    
	setuid();
	seteuid(getuid());
}

string dlugi()
{
	string opis = "";

	if(query_mlode())
	{
		opis += "M�ody pie� d�wiga na sobie cie�ar powykr�canych,"
			+" niezbyt grubych konar�w.";
	}
	else
	{
		opis += "Masywne konary rozrastaj� si� na wszystkie strony"
			+" tworz�c zgrabn�, kulist� koron� drzewa.";
	}

	if(find_room()->pora_roku() == MT_WIOSNA)
	{
		opis += " Na ga��ziach drzewa dostrzegasz drobne, zielonkawe"
			+" p�czki, cho� gdzieniegdzie mo�na ju� zauwa�y�"
			+" m�odziutkie, ledwo rozwini�te listki.";
	}

	if(find_room()->pora_roku() == MT_LATO)
	{
		opis += " Ga��zie drzewa s� g�sto pokryte ciemnozielonymi"
			+" li��mi o falistych brzegach.";
	}

	if(find_room()->pora_roku() == MT_JESIEN)
	{
		if(query_sciete())
		{
			opis += " D�b ust�pi� nasta�ej jesieni, jego ga��zie"
				+" mieni� si� r�nobarwnymi li��mi.";
		}
		else
		{
			opis += " D�b ust�pi� nasta�ej jesieni, jego li�cie"
				+" przeobrazi�y si� w r�nobarwne strz�pki, kt�re"
				+" co jaki� czas, ta�cz�ce na wietrze, spadaj� na"
				+" ziemi�.";
		}
	}

        if(find_room()->pora_roku() == MT_ZIMA)
        {
                opis += " Nasta�y mr�z ogo�oci� drzewo ze wszelkich li�ci,"
			+" za� pomi�dzy fa�dami kory dostrzegasz niewielkie"
			+" ilo�ci �niegu.";
        }

	opis += "\n";

	return opis;

}
