/**
 * \file /d/Standard/pogoda/warunki.c
 *
 * Plik pobieraj�cy warunki z bazy wygenerowanej przez skrypt pythona
 *
 */

#pragma save_binary
#pragma strict_types

#include <pogoda.h>
#include <macros.h>

/**
 * GET_FROM_NEAREST
 *
 * Gdy ta definicja nie jest zakomentowana, to je�li system nie natrafi na �adn� pogod�,
 * kt�r� b�dzie m�g� pobra�, policzy sobie z okolicznych pog�d pobranych z yahoo:)
 */
#define GET_FROM_NEAREST

/**
 * ODLEGLOSC
 *
 * Maksymalna odleg�o�� brana pod uwag� przy obliczaniu
 * pogody dla miejsc dla, kt�rych nie jest ona pobierana
 * z bazy edytowanej przez skrypt pobieraj�cy pogode z yahoo
 * DEFINICJA TA ma sens jedynie w przypadku gdy zdefiniowana
 * jest definicja \def GET_FROM_NEAREST
 */
#define ODLEGLOSC   3

#define DBG(y, x) 0 /* find_player("krun")->catch_msg("Pogoda(" + (y) + ") :" + (x) + ".\n") */

mapping pogoda_wr;
mapping pogoda;
static private mapping niedodane = (["nied":({})]);

private int sprawdz(string otrz, string wzr)
{
    if(wildmatch("*" + wzr + "*", otrz))
        return 1;

    wzr = implode(explode(wzr, " "), "* *");

    if(wildmatch(wzr, otrz))
        return 1;

    return 0;
}

private int pobierz_godzine(string godzina)
{
    string *exp = explode(godzina, ":");

    return mktime(0, atoi(exp[1]), atoi(exp[0]));
}


private int pobierz_widocznosc(string widocznosc)
{
    string *exp;
    if(!strlen(widocznosc) || sizeof(exp = explode(widocznosc, " ")) < 2 || exp[1] != "km")
        return -1;
    else
        return atoi(widocznosc[0..-4]);
}

private int *pobierz_zachmurzenie_i_opady_z_pogody(string pog)
{
    int *z = ({}), *o = ({});

    //Mo�e by� wi�cej ni� jedna rzecz, a po��czone s� za pomoc� /
    string *pexp = explode(pog, "/");

    foreach(string p : pexp)
    {
        if(sprawdz(p, "Light Rain"))
        {
            DBG(1,p);
            o += ({POG_OP_D_L});
            z += ({POG_ZACH_SREDNIE});
        }
        else if(sprawdz(p, "Haevy Rain"))
        {
            DBG(2,p);
            o += ({POG_OP_D_C});
            z += ({POG_ZACH_CALKOWITE});
        }
        else if(sprawdz(p, "T-Storms"))
        {
            DBG(3,p);
            o += ({POG_OP_D_C});
            z += ({POG_ZACH_CALKOWITE});
        }
        else if(sprawdz(p, "Rain"))
        {
            DBG(4,p);
            o += ({POG_OP_D_S});
            z += ({POG_ZACH_DUZE});
        }
        else if(sprawdz(p, "Showers"))
        {
            DBG(5,p);
            o += ({POG_OP_D_L});
            z += ({POG_ZACH_SREDNIE});
        }
        else if(sprawdz(p, "Light Snow"))
        {
            DBG(6,p);
            o += ({POG_OP_S_L});
            z += ({POG_ZACH_SREDNIE});
        }
        else if(sprawdz(p, "Haevy Snow"))
        {
            DBG(7,p);
            o += ({POG_OP_S_C});
            z += ({POG_ZACH_CALKOWITE});
        }
        else if(sprawdz(p, "Snow"))
        {
            DBG(8,p);
            o += ({POG_OP_S_S});
            z += ({POG_ZACH_DUZE});
        }
        else if(sprawdz(p, "Fair"))
        {
            DBG(9,p);
            o += ({POG_OP_ZEROWE});
            z += ({POG_ZACH_ZEROWE});
        }
        else if(sprawdz(p, "Partly Cloudy"))
        {
            DBG(10,p);
            z += ({POG_ZACH_LEKKIE});
        }
        else if(sprawdz(p, "Mostly Cloudy"))
        {
            DBG(11,p);
            z += ({POG_ZACH_DUZE});
        }
        else if(sprawdz(p, "Cloudy"))
        {
            DBG(12,p);
            z += ({POG_ZACH_SREDNIE});
        }
        else if(sprawdz(p, "Sunny"))
        {
            DBG(13,p);
            z += ({POG_ZACH_ZEROWE});
        }
        else if(sprawdz(p, "Mist"))
        {
            DBG(14,p);
            z += ({POG_ZACH_SREDNIE});
        }
        else
        {
            if(!is_mapping_index("nied", niedodane))
                niedodane["nied"] = ({});
            else if(!pointerp(niedodane["nied"]))
                niedodane["nied"] = ({});

            if(member_array(p, niedodane["nied"]) == -1)
            {
                niedodane["nied"] += ({p});
                save_map(niedodane, "/d/Standard/pogoda/niedodane");
            }
        }
    }

    if(!sizeof(o))
        o += ({POG_OP_ZEROWE});
    if(!sizeof(z))
        z += ({POG_ZACH_ZEROWE});

    return ({z[0], o[0]});
}

private int pobierz_wiatr(int predkosc)
{
    switch(predkosc)
    {
        case 0..5       : return POG_WI_ZEROWE;
        case 6..20      : return POG_WI_LEKKIE;
        case 21..50     : return POG_WI_SREDNIE;
        case 51..110    : return POG_WI_DUZE;
        default         : return POG_WI_BDUZE;
    }
}

private mixed podziel_wartosci(mixed a, int dz)
{
    if(intp(a))
        return (a / dz);
    else if(floatp(a))
        return (a / itof(dz));
    else
        return a;
}

public nomask void oblicz_pogode(int x, int y)
{
    if(is_mapping_index(x, pogoda))
        return pogoda[x][y];

    int dz;
    mixed ret = allocate(POGODA_SIZE);

    for(int i = (x - ODLEGLOSC) ; i < (x + ODLEGLOSC) ; i++)
    {
        if(!is_mapping_index(i, pogoda))
            continue;

        for(int j = (y - ODLEGLOSC) ; j < (y + ODLEGLOSC) ; j++)
        {
            if(!is_mapping_index(j, pogoda[i]))
                continue;

            dz++;

            for(int k = 0 ; k < sizeof(ret) ; k++)
            {
                if( k == POGODA_MIASTO || k == POGODA_MIASTOR )
                    ret[k] = "automatycznie generowane";
                else if(intp(pogoda[i][j][k]) || floatp(pogoda[i][j][k]))
                    ret[k] += pogoda[i][j][k];
                else
                    ret[k] = pogoda[i][j][k];
            }
        }
    }

    pogoda[x] = ([]);
    pogoda[x][y] = ret;
}

public nomask void odnow()
{
    setuid();
    seteuid(getuid());
    niedodane = restore_map("/d/Standard/pogoda/niedodane");
    restore_object("/d/Standard/pogoda/warunki");

    pogoda = ([]);

    for(int i = 0 ; i < m_sizeof(pogoda_wr) ; i++)
    {
        int x, y;
        string miasto = m_indexes(pogoda_wr)[i];

        switch(lower_case(miasto))
        {
            case "rinde":
                x = 0;
                y = 0;
                break;

            case "bialy_most":
                x = -4;
                y = -2;
                break;

            case "piana":
                x = -4;
                y = 2;
                break;

            case "zakon":
                x = 0;
                y = 2;
                break;

            default:
                notify_wizards(TO, "System pogodowy pobra� pogod� dla nieznanego "+
                    "terenu - Pogoda nie zostaje dodana.\n");
        }

        int *op = pobierz_zachmurzenie_i_opady_z_pogody(pogoda_wr[miasto]["pogoda"]);

        if(!mappingp(pogoda[x]))
            pogoda[x] = ([]);

        pogoda[x][y] = allocate(POGODA_SIZE);
        pogoda[x][y][POGODA_TEMP]          = atoi(pogoda_wr[miasto]["temperatura_rzeczywista"]);
        pogoda[x][y][POGODA_TEMP_ODCZ]     = atoi(pogoda_wr[miasto]["temperatura_odczuwalna"]);
        pogoda[x][y][POGODA_TEMP_MIN]      = atoi(pogoda_wr[miasto]["temperatura_min"]);
        pogoda[x][y][POGODA_TEMP_MAX]      = atoi(pogoda_wr[miasto]["temperatura_max"]);
        pogoda[x][y][POGODA_WIATR_K]       = pogoda_wr[miasto]["wiatr_kierunek"];
        pogoda[x][y][POGODA_WIATR_P]       = atoi(pogoda_wr[miasto]["wiatr_predkosc"]);
        pogoda[x][y][POGODA_WIATR]         = pobierz_wiatr(pogoda[x][y][POGODA_WIATR_P]);
        pogoda[x][y][POGODA_WILGOTNOSC]    = atoi(pogoda_wr[miasto]["wilgotnosc"]);
        pogoda[x][y][POGODA_WIDOCZNOSC]    = pobierz_widocznosc(pogoda_wr[miasto]["widocznosc"]);
        pogoda[x][y][POGODA_WSCHOD]        = pobierz_godzine(pogoda_wr[miasto]["wschod"]);
        pogoda[x][y][POGODA_ZACHOD]        = pobierz_godzine(pogoda_wr[miasto]["zachod"]);
        pogoda[x][y][POGODA_POBRANIE]      = pobierz_godzine(pogoda_wr[miasto]["pobranie"]);
        pogoda[x][y][POGODA_MIASTO]        = miasto;
        pogoda[x][y][POGODA_MIASTOR]       = pogoda_wr[miasto]["miasto_real"];
        pogoda[x][y][POGODA_ZACHMURZENIE]  = op[0];
        pogoda[x][y][POGODA_OPADY]         = op[1];
    }
}

public nomask void create()
{
    odnow();

    //Coby si� nam co 15 min od�wie�a�o
    set_alarm(900.0, 900.0, &odnow());
}

public mixed get_val(int x, int y, int r)
{
    if(!mappingp(pogoda) || !m_sizeof(pogoda))
        return 0;

    if(r < 0 || r >= POGODA_SIZE)
        return 0;

#ifdef GET_FROM_NEAREST
    if(!is_mapping_index(x, pogoda) || !is_mapping_index(y, pogoda[x]))
    {   //Je�li strefa nie jest pobierana to pobieramy �redni� z najbli�szych
        //do niej stef.
        pogoda[x] = ([]);
        pogoda[x][y] = oblicz_pogode(x, y);
    }
#else
    x = (y = 0);
#endif

    if(!pointerp(pogoda[x][y]))
        return 0;

    return pogoda[x][y][r];
}

/**
 * Dzi�ki tej funkcji mo�emy pobra� temperature dla interesuj�cego nas obszaru
 */
public varargs int get_temperatura(int x, int y, int r = POGODA_TEMP_ODCZ)
{
    if(r != POGODA_TEMP_ODCZ && r != POGODA_TEMP && r != POGODA_TEMP_MAX && r != POGODA_TEMP_MIN)
        return -1;

    return get_val(x, y, r);
}

/**
 * Dzi�ki tej funkcji mo�emy pobra� zachmurzenie dla interesuj�cego nas obszaru.
 */
int
get_zachmurzenie(int x, int y)
{
    return get_val(x, y, POGODA_ZACHMURZENIE);
}

/**
 * Dzi�ki tej funkcji mo�emy pobra� opady dla interesuj�cego nas obszaru.
 */
int
get_opady(int x, int y)
{
    return get_val(x, y, POGODA_OPADY);
}

/**
 * Dzi�ki tej funkcji mo�emy sprawdzi� si�� wiatru.
 */
int
get_wiatry(int x, int y)
{
    return get_val(x, y, POGODA_WIATR);
}

/**
 * Dzi�ki tej funkcji mo�emy sprawdzi� wilgotno�� powietrza.
 */
int get_wilgotnosc(int x, int y)
{
    return get_val(x, y, POGODA_WILGOTNOSC);
}

/**
 * Dzi�ki tej funkcji mo�emy sprawdzi� o jakiej godzinie jest wsch�d s�o�ca.
 */
int get_wschod(int x, int y)
{
    return get_val(x, y, POGODA_WSCHOD);
}

/**
 * Dzi�ki tej funkcji mo�emy sprawdzi� o jakiej godzinie jest zach�d s�o�ca.
 */
int get_zachod(int x, int y)
{
    return get_val(x, y, POGODA_ZACHOD);
}

/**
 * Dzi�ki tej funkcji mo�emy sprawdzi� jaka jest widoczno��
 * @return -1 nieograniczona
 * @return x widoczno��
 */
int get_widocznosc(int x, int y)
{
    return get_val(x, y, POGODA_WIDOCZNOSC);
}

/**
 * Dzi�ki tej funkcji mo�emy dowiedzie� si� kiedy mia�a miejsce ostatnia
 * aktualizacja pogody, je�li jest to czas d�u�szy ni� 5h to
 * informuje o tym wiz�w, bo z pewno�ci� co� jest nie tak jak trzeba.
 */
int get_last_update(int x, int y)
{
    return get_val(x, y, POGODA_POBRANIE);
}