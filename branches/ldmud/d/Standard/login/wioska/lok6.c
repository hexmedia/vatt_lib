/*
 *  Elfia polanka wioski z etapu tworzenia postaci.
 *        By Lil & czesc dialogow Gjoef
 *                          Wed Mar 22 2006
 */

inherit "/d/Standard/login/wioska/std";

#include <std.h>
#include <stdproperties.h>
#include <macros.h>
#include <ss_types.h>
#include <composite.h>
#include <language.h>
#include <filter_funs.h>
#include <state_desc.h>
#include "login.h"

#define UBRANIA "/d/Standard/items/ubrania/"

object gift1, gift2, gift3, gift4; //prezenty :P
object npc;
int faza_reakcji=0; //jesli jest 1, to npc nie reaguje na wybory np. mowiac "Wspaniale."
                    //dotyczy to przypadkow kiedy gracz np. stwierdzi, ze nie chce byc
                    //danej rasy. Wtedy to by brzmialo bezsensu.
object byt=this_player();

public void
create_wioska_room()
{
    set_short("Zielona polana");

    add_exit("lok4",  "n");
    npc=clone_object(PATH+"wioska/npc/elf.c");
    npc->init_arg(0);
    npc->move(this_object());
    add_sit("na ziemi","na ziemi","z ziemi",0);

    add_event("Poruszone wiatrem trzciny wydaj� charakterystyczny dla siebie, "+
              "koj�cy szum.\n");
    add_event("W oczku wodnym co� plusn�o.\n");
    add_event("Ptaki �wierkaj� weso�o i beztrosko.\n");

    add_item(({"oczko","oczko wodne","malutkie oczko wodne"}),
               "Malutkie oczko wodne z krystalicznie czyst� wod� "+
               "zach�ca do pozostania w tym miejscu i zasmakowania "+
               "skarb�w ukrytych przez natur�.\n");
    add_item(({"drzewa","korony","korony drzew","roz�o�yste korony",
               "roz�o�yste korony drzew"}),
               "Roz�o�yste korony drzew, o grubych i wysokich pniach "+
               "okalaj� t� polan� z ka�dej strony.\n");
    add_item(({"traw�","wysok� traw�"}),
               "Wiecznie ko�ysz�ca si� wysoka trawa posiada niewiarygodn� "+
               "wr�cz czyst�, niczym nie ska�on� ziele�.\n");
    add_item(({"trzciny","dzikie trzciny"}),
               "Trzciny otaczaj� ten niewielki staw.\n");
    add_item("niebo",
               "Jego niesko�czony b��kit posiada wyj�tkow� g��bi�, lecz "+
               "przy tym jest zarazem jasny i wyra�ny.\n");
    add_item(({"tarcz�","kolorow� tarcz�"}),
               "Tarcza jest starannie zwi�zana sznurami i wygl�da "+
               "na mocn�. Namalowane na niej ko�a s� koloru ��tego, "+
               "niebieskiego i czerwonego. Jest w niej wbitych kilka strza�. "+
               "Wszystkie na czerwonym, �rodkowym polu.\n");
}

exits_description()
{
	return "Na p�nocy dostrzegasz plac.\n";
}

string
dlugi_opis()
{
    string str;
    str = "Mimo niedu�ych rozmiar�w tej polanie z pewno�ci� niczego nie brakuje. "+
          "Skryta mi�dzy g�sto rosn�cymi drzewami o roz�o�ystych koronach "+
          "daje poczucie bezpiecze�stwa, a wysoka trawa "+
          "rosn�ca na niemal�e ca�ej powierzchni tej polany kusi sw� "+
          "niesamowicie g��bok� zieleni� aby usi��� i zapomnie� o wszystkim innym. "+
          "Malutkie oczko wodne na �rodku mi�dzy "+
          "dzikimi trzcinami odbija posta� elfa dzier��cego �uk, na tle "+
          "jasnego koj�cego b��kitu nieba. Kilkadziesi�t st�p od niego "+
          "widnieje kolorowa tarcza skonstruowana ze s�omy i kilku belek.";
    return str;
}


/* Generalnie staramy si� tworzy� jak najmniej akcji npca
 * w jednej fazie. �eby na zafloodowa�o nagle ekranu...
 */
 // gjoef: sztywno *_* ... ale czy nie o to wlasnie chodzi?..
void faza0start()
{
    // tu moze strzela z luku, albo cos...

    npc->command("opusc luk");
    npc->command("usmiechnij sie na powitanie");
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Ju� my�la�em, �e nie przyjdziesz.");
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Czy jeste� gotow"+koncoweczka("y","a")+
             ", by do��czy� do �ywych?");
}

void fazaniebyt()
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Decyzja ju� zapad�a, a nam pozostaje tylko si� z ni� " +
             "pogodzi�. Odejd� st�d.");
}
void faza0tak()
{
    faza_reakcji=1;
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Czy chcesz kroczy� po �wiecie jako jed" +
             koncoweczka("en", "na") + " z nas?");
}
void faza0nie()
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Wr�� tu, gdy zmienisz zdanie. I nie spiesz si�... ");
    npc->command("powiedz Mamy czas.");
    npc->command("emote poleruje d�ugi cisowy �uk.");
}
void faza0inne()
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Hmmm... Po prostu powiedz, jeste� czy nie jeste�?");
}
void faza1tak()
{
    faza_reakcji=1;
    npc->command("pokiwaj glowa z uznaniem"); // z uznaniem?
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Niech tak si� stanie! Ale czy zechcesz wpierw wys�ucha� " +
             "tego, co mam ci do powiedzenia o innych rasach, w�r�d kt�rych " +
             "przyjdzie ci �y�?");
}
void faza1nie()
{
    faza_reakcji=1;
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Podj"+koncoweczka("��e�","�a�")+" decyzj�. "+
             "Czy jest ona s�uszna, os�dzisz sam"+koncoweczka("","a")+". "+
             "Teraz st�d odejd�."); // mozna jeszcze dodac
}
void faza1inne()
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Tak czy nie? Nie?");
}

/* Dobrze by by�o, gdyby ka�dy enpec opowiada� histori� z w�asnego
 * punktu widzenia, nie?
 * Historia wi�c ma by� subiektywna. Im bardziej, tym lepiej ;)
 *
 *
 * HISTORIA ROZBITA NA FAZY. PIERWSZA FAZA JEST NA SAMYM DOLE: faza2tak()
 * A OSTATNIA NA SAMEJ GORZE (faza2tak4()), wiec od konca czytamy ;)
 */
void faza2tak6()
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Ty jednak masz wyb�r. Wykorzystaj go m�drze! "+
             "Nie�atwo jest Aen Seidhe, miej tego �wiadomo��. " +
             "Przemy^sl i podejmij decyzj^e, "+
             "czy chcesz kroczy� po tym "+
             "�wiecie jako elf"+koncoweczka("","ka")+"?");
    npc->command("emote spogl�da na ciebie, a jego przenikliwy "+
             "wzrok przyprawia ci� o dreszcze.");
}
void faza2tak5()
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Ludzie rozmno�yli si� niczym szara�cza. A gdy zabrak�o " +
             "dla� miejsca, wydarli je nam! I tak jest po dzi� dzie�. "+
             "To my jeste�my traktowani jako obcy.");
    set_alarm(10.0,0.0,&faza2tak6());
}
void faza2tak4()
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Tak, to by�a okropna rze�. Starsi, roztropniejsi "+
             "z nas uciekli w G�ry Sine, pal�c za sob� wszystko to, "+
             "czego nie zdo�ali zabra�, nie pozwalaj�c, by "+
             "cokolwiek wpad^lo w r�ce Dh'oine.");
    set_alarm(13.0,0.0,&faza2tak5());
}
void faza2tak3()
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " I wtedy rozp�ta�a si� wojna. Dwie�cie lat temu "+
             "Aelirenn poprowadzi�a m�odych "+
             "elf�w do beznadziejnej walki o przetrwanie, tak " +
             "jakby rzeczywi^scie co^s da^lo si^e zrobi^c... I wszyscy " +
             "zgin^eli.");
    npc->command("emote wstrzymuje sw� opowie��, a "+
                 "jego niezmiernie smutna twarz przez chwil� "+
                 "zdradza jego uczucia.");
    set_alarm(13.0,0.0,&faza2tak4());
}
void faza2tak2()
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Wtedy nie widzieli�my ze strony Dh'oine �adnego " +
             "zagro�enia. Traktowali�my ich raczej jak co� ulotnego. "+
             "Jak noc, kt�ra zapada i przemija, a po niej zn�w wschodzi " +
             "s�o�ce... Ale ono nie wzesz�o.");
    set_alarm(14.0,0.0,&faza2tak3());
}
void faza2tak1()
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " S�uchaj zatem. My, elfy, przybyli�my na Kontynent... o ile " +
             "dobrze licz�, to oko�o tysi�ca pi�ciuset lat temu, tak. " +
             "Osiedliwszy si�, �yli�my w zgodzie i harmonii - my, krasno" +
             "ludy, gnomy i hobbity. A potem pojawili si� tu ludzie.");
    set_alarm(16.0,0.0,&faza2tak2());
}
void faza2tak() //Opowiada historie + ostateczne pytanie o rase
{
    faza_reakcji=1;
    npc->command("namysl sie");
    set_alarm(2.0,0.0,&faza2tak1());
}

void faza2nie()
{
    faza_reakcji=1;
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Rozumiem zatem, �e nieobca ci nasza sytuacja. To dobrze. " +
             "Zostajesz przy swoim wyborze, tak?");
}
void faza2inne()
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Zdecyduj si�, tak czy nie?");
}
void faza3tak() //Ostateczne pytanie o rase. Nastepnie pytanie o wzrost
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Bardzo dobrze. Ale zanim wejdziesz do �wiata �ywych, wpierw" +
             " musz� zada� ci kilka pyta�. Po pierwsze, jakiego chcesz " +
             "by� wzrostu?");
}
void faza3nie() // tak jak faza1nie()
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Podj"+koncoweczka("��e�","�a�")+" decyzj�. "+
             "Czy jest ona s�uszna, os�dzisz sam"+koncoweczka("","a")+". "+
             "Teraz st�d odejd�.");
}
void faza3inne()
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Zastan�w si� i odpowiedz. Czy zostajesz przy swoim wyborze?");
}
void faza4zle() //odpowiedz na wzrost, pytanie o wage
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Nie rozumiem. Ale je�li chcesz, zapytaj mnie o wzrost.");
}
void faza4dobrze()
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Tak b�dzie. A jakiej chcesz by� wagi?");
}
void faza5zle() //waga
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Doprawdy nie wiem, co masz na my�li.");
}
void faza5dobrze()
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Rozumiem... Dalej: ile chcesz mie� lat?");
}
void faza6nierozumiem() //wiek
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Que? Ile?");
}
void faza6zamlody()
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Hmmm... To chyba za ma�o, nieprawda�? Wybierz troch� " +
             "wi�cej.");
}
void faza6zastary()
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Zaprawd�, to za du�o.");
}
void faza6dobrze()
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Dobrze, b�dziesz mia�"+koncoweczka("","a")+
             " "+LANG_SNUM(wiek,0,1)+" "+slowo_lat+
             "... Jakiego koloru chcesz mie� oczy?");
}
void faza7zle() //kolor oczu
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Nie s�ysza�em o takim... "+
             "Ale wybierz co� z tych: "+
    implode(m_indices(plec==1? oczy_elfki:oczy_elfa), ", ")+".");
}
/* Po prostu idziemy dalej, czyli
 * pytamy o to, czy �yczy sobie posiada� w�osy W OG�LE,
 * czyli, czy gracz chce by� �ysy NA STA�E.
 */
void faza7dobrze8()
{
    write("\n\nAieeeeeeeeeee! Zg�os b��d nr 1.\n\n");
}
/* A tutaj omijamy pytanie o to, czy gracz chce w og�le
 * posiada� ow�osienie, poniewa� jest elfem lub p�elfem, a
 * oni nie mog� by� 'na sta�e' �ysi.
 * Oczywi�cie nikt im nie zabroni si� zgoli�, ale w�osy
 * b�d� ros�y im i tak... :)
 * Wi�c pytamy od razu o kolor w�os�w.
 */
void faza7dobrze9()
{
    faza8tak();
}

/* Je�li gracz chce posiada� w�osy w og�le, to jedziemy z pytaniem
 * o kolor w�os�w.
 */
void faza8tak()
{
   npc->command("powiedz do "+ OB_NAME(byt) +
             " A w�osy? Jaki chcesz mie� ich kolor?");
}

/* Kobietom nie zadajemy pyta� o zarost, wi�c omijamy kilka faz
 * i pytamy odrazu o budow� cia�a t� �ys� kobietk� <szok> ....
 */
void faza8nie1()
{
    write("\n\nAieeeeeeeeeee! Zg�os b��d nr 2.\n");
}

/* A m�czy�ni i tak musz� poda� kolor...zarostu. �eby by�o wiadomo
 * jaki kolor brody i w�s�w im da� (gdyby posiadali w�osy, to wyliczaloby
 * si� to z koloru w�os�w, ale �e wybrali, �e s� permanentnie �ysi...)
 */
void faza8nie2()
{
    write("\n\nAieeeeeeeeeee! Zg�os b��d nr 3.\n");
}
/* Tak czy nie? Inna odpowied�...
 */
void faza8inne()
{
    write("\n\nAieeeeeeeeeee! Zg�os b��d nr 4.\n");
}
/* kolor wlosow, pytanie o dlugosc
 */
void faza9zle()
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " C� to za kolor? Nie znam go. Znam natomiast "+
           //w przypadku innych ras jest s� to inne indeksy mapping�w
    implode(m_indices(wlosy_elfow), ", ")+".");
}
void faza9dobrze()
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Rozumiem... wybierz jeszcze ich d�ugo��.");
}
/* Ta faza jest tylko wtedy, je�li si� wybra�o permanentnie �ysego.
 * Bo i tak trzeba poda� kolor zarostu. Wi�c ta faza jest tylko dla m�czyzn
 */
void faza10zle()
{
    write("\n\nAieeeeeeeeeee! Zg�os b��d nr 5.\n");
}
void faza10dobrze()
{
    write("\n\nAieeeeeeeeeee! Zg�os b��d nr 6.\n");
}
/* W tej fazie gracz wybiera d�ugo�� w�os�w i dostaje pytanie
 * O w�sy, je�li jest m�czyzn�. Je�li jest elfem lub kobiet�,
 * to omijamy te pytania i skaczemy odrazu do pyt. o budowe cia�a
 */
void faza11zle()
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Nie znam takiej d�ugo�ci w�os�w.");
}
/* jesli jest: albo elfem, albo kobiet�, albo poni�ej 19 roku �ycia:
 */
void faza11dobrze1()
{
    faza13dobrze();
}
/* w przeciwnym wypadku:
 */
void faza11dobrze2()
{
    write("\n\nAieeeeeeeeeee! Zg�os b��d nr 7.\n");
}
/* d�ugo�� w�s�w:
 */
void faza12zle()
{
    write("\n\nAieeeeeeeeeee! Zg�os b��d nr 8.\n");
}
void faza12dobrze()
{
    write("\n\nAieeeeeeeeeee! Zg�os b��d nr 9.\n");
}
/* d�ugo�� brody
 * i pytanie o ceche budowy cia�a
 */
void faza13zle()
{
    write("\n\nAieeeeeeeeeee! Zg�os b��d nr 10.\n");
}
void faza13dobrze() // tu mnie zaczela bolec dynia
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " A�eby doko�czy� dzie�a, wybierz jedn� " +
             "cech� budowy przysz�ego cia�a.");
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Oto one: "+   //m["indeks"][2]
             COMPOSITE_WORDS2(dostepne_cechy_ciala," lub ")+".");
                             //lista filtrowana przez wybrana wage i wzrost
}


void faza14zle() //budowa ciala. Pytanie o ceche szczegolna
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Nie rozumiem... Wybierz co� z tych: " +
               COMPOSITE_WORDS2(dostepne_cechy_ciala," lub ")+".");
}
void faza14dobrze()
{
    mixed mapka=this_player()->query_gender()==1?cechy_szczegolne_kobiet:
         cechy_szczegolne_mezczyzn;
    mixed postmapka;
    postmapka=filter(mapka, &operator(==)(, 3) @ &sizeof());
    npc->command("powiedz do "+ OB_NAME(byt) +
         " I jeszcze jedno. Opr�cz tego mo�esz posiada� jak�� " +
         "szczeg�ln�, charakterystyczn� cech�, dla przyk�adu " +
         COMPOSITE_WORDS2(m_indexes(postmapka)," lub ")+". Czy chcesz?");
}
void faza15tak() //odpowiedz czy chce sie miec ceche szczegolna <opcjonalnie>
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Jaka zatem to b�dzie cecha?");
}
void faza15nie()
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Cia�o gotowe! Nast�pne pytanie...");
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Mo�esz wybra� sobie jakie� pochodzenie. Czy chcesz " +
             "je posiada�? "+
             "Oczywi�cie je�li nie wybierzesz teraz p�niej b�dziesz " +
             koncoweczka("m�g�", "mog�a") + " zmieni� sw�j " +
             "wyb�r, jednak�e ograniczy si� on tylko do miejsca, w kt�rym"+
             " b�dziesz si� znajdowa�"+koncoweczka("", "a")+".");
}
void faza15inne()
{
   npc->command("powiedz do "+ OB_NAME(byt) +
             " Chcesz czy nie chcesz?");
}
void faza16zle() //wybieranie cechy szczegolnej (jesli sie zgodzilo wczesniej)
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " A c� to za cecha?");
}
void faza16dobrze()
{
    npc->command("popatrz uwa�nie na "+ byt->query_name(PL_BIE));
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Najwa�niejsze, �eby� ty by�"+koncoweczka("","a")+
             " zadowolon"+koncoweczka("y","a")+".");
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Dalej... mo�esz wybra� sobie jakie� pochodzenie. Czy " +
             "chcesz je posiada�? "+
             "Oczywi�cie je�li nie wybierzesz teraz p�niej b�dziesz " +
             koncoweczka("m�g�", "mog�a") + " zmieni� sw�j " +
             "wyb�r, jednak�e ograniczy si� on tylko do miejsca, w kt�rym"+
             " b�dziesz si� znajdowa�"+koncoweczka("", "a")+".");
}
void faza17tak() //odpowiedz czy chce sie posiadac pochodzenie.
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Dobrze, dobrze, hmm... Sk�d wi�c chcesz pochodzi�?");
}
void faza17nie()
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Niech tak b�dzie... A teraz wybierz jedn� z " +
             LANG_SNUM(SS_NO_STATS, PL_CEL, PL_ZENSKI) + " cech "+
             "kt�ra b�dzie twoj� siln� stron�. Do wyboru " +
             "masz " + COMPOSITE_WORDS2(SD_STAT_NAMES_BIE, " oraz ") + ".");
}
void faza17inne()
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Tak czy nie? Nie czy tak?");
}
void faza18zle() //wybor pochodzenia jesli sie zgodzilo wczesniej.
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " D�ugo �yj�, ale o tym miejscu jeszcze nie s�ysza�em.");
}
void faza18dobrze()
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Tak jest. A teraz wybierz jedn� z sze�ciu cech, "+
             "kt�ra b�dzie twoj� siln� stron�. Do wyboru " +
             "masz " + COMPOSITE_WORDS2(SD_STAT_NAMES_BIE, " oraz ") + ".");
}
void faza19zle() //cechy
{
    npc->command("powiedz do "+ OB_NAME(byt) +
        " Wybierz jedn� z tych " + LANG_SNUM(SS_NO_STATS, PL_CEL, PL_ZENSKI) +
        "cech kt�re Ci poda�em.\n");
}

void faza19dobrze() //cechy
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Przejd�my teraz do umiej�tno�ci. Mianowicie mo�esz " +
             "wybra� dwie, w kt�rych b�dziesz si� specjalizowa�"+
             koncoweczka("","a")+". " +
             "Jaki jest tw�j pierwszy wyb�r?");
}
void faza20zle() //umiejetnosci - wybor pierwszej
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Ale ja nie znam takiej umiej�tno�ci.");
}
void faza20dobrze()
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " S�usznie. A druga umiej�tno��?");
}
void faza21zle() //umiejetnosci - wybor drugiej
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Nie znam takiej.");
}
/* nie mo�na wybra� dw�ch umiej�tno�ci takich samych pod rz�d.
 */
void faza21zle2()
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Przecie� wybra�"+koncoweczka("e�","a�")+" j� przed chwil�.");
}
void faza21dobrze()
{
    npc->command("pokiwaj powoli");
    npc->command("powiedz do "+ OB_NAME(byt) +
             " To wszystko! Wreszcie mo�esz wkroczy� do �wiata "+
             "�ywych. Gotow" +koncoweczka("y", "a")+"?");
}
void faza22tak() //podsumowanie + prezenty?:) No wlasnie...Jakie?
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Ah tak, mam tu co� jeszcze dla ciebie.");
    npc->command("daj monety, szate, buty i noz "+OB_NAME(byt));
}
void faza22nie()
{
    npc->command("powiedz do "+ OB_NAME(byt) +
    " M�drze. Id� do innych ras, �yj�c ich �yciem, b�dzie ci �atwiej.");
}
void faza22inne()
{
    npc->command("powiedz do "+ OB_NAME(byt) +
    " To znaczy tak, czy nie?");
}

/* A tutaj mamy list� reakcji na odpowiedzi :P
 * Pokazuje sie to za ka�dym razem (natychmiastowo!) po
 * pozytywnej lub negatywnej odpowiedzi.
 * Odpowiedzi b��dne (czyli wywo�uj�ce fazaXinne() nie s�
 * brane pod uwag�).
 */
void reakcje_na_odpowiedzi()
{
    switch(random(9))//im wiecej tym lepiej. 12-15 jest optymalne.
    {
      case 0:
             faza_reakcji==1?:npc->command("powiedz do "+ OB_NAME(byt) +
             " Dobrze."); break;
      case 1:
             npc->command("pokiwaj lekko"); break;
      case 2:
             npc->command(""); break;
      case 3:
             npc->command("potrzyj policzek ."); break;
      case 4:
             npc->command("pokiwaj lekko"); break;
      case 5:
             npc->command(""); break;
      case 6:
             npc->command("powiedz Tak s�dzi�em."); break;
      case 7:
             npc->command("emote spogl�da przez chwil� "+
                   "nieobecnym wzrokiem w dal.");
                                                            break;
      case 8:
             npc->command("pokiwaj nieznacznie"); break;

      default:
             npc->command("emote u�miecha si� domy�lnie."); break;
     }
}


void jaka_rasa()
{
    if(plec==1)
      {
        rasa="elfka";
      }
    else
      {
        rasa="elf";
      }
  race="elf";
}

void
przydziel_prezenty(object komu)
{
    gift1=clone_object("/d/Standard/items/bronie/noze/maly_poreczny.c");
    gift2=clone_object(UBRANIA+"buty/wysokie_skorzane_elf.c");
    gift3=clone_object(UBRANIA+"szaty/dluga_jasnobrazowa.c");
    gift2->wylicz_rozmiary(0,komu);
    gift3->wylicz_rozmiary(0,komu);
//    gift1->init_arg(0);
    gift1->move(npc);
//    gift2->init_arg(0);
    gift2->move(npc);
//    gift3->init_arg(0);
    gift3->move(npc);

}