/*
 *  Centrum wioski z etapu tworzenia postaci.
 *        By Lil. Fragmenty opisu Aneta.
 *                          Kwiecie� 2006
 */

inherit "/d/Standard/login/wioska/std";
inherit "/lib/drink_water";

#include <std.h>
#include <stdproperties.h>
#include <macros.h>
#include <ss_types.h>
#include <composite.h>
#include <filter_funs.h>
#include <language.h>
#include <state_desc.h>
#include "login.h"

#define UBRANIA "/d/Standard/items/ubrania/"

object npc;
int faza_reakcji=0; //jesli jest 1, to npc nie reaguje na wybory np. mowiac "Wspaniale."
                    //dotyczy to przypadkow kiedy gracz np. stwierdzi, ze nie chce byc
                    //danej rasy. Wtedy to by brzmialo bezsensu.
object byt=this_player();
object gift1, gift2, gift3; //prezenty :P

public void
create_wioska_room()
{
    set_short("Plac g��wny");

    add_item(({"kolumn�","poka�n� kolumn�"}),
            "Mglista kolumna bije ze �rodka placu w przestworza "+
            "przypruszone gwiezdnym py�em. Ozdabia j� enigmatyczny "+
            "malunek w�a u�o�onego na kszta�t �semki, "+
             "po�eraj�cego sw�j ogon.\n");
    add_item(({"malunek na kolumnie","enigmatyczny malunek na kolumnie",
            "malunek w�a na kolumnie"}),
            "W�� u�o�ony w znak niesko�czono�ci mocno kontrastuje swoj� "+
            "g��bok� czerni� z bia��, majestatyczn� kolumn�.\n");
    add_item(({"studni�","kamienn� studni�","studzienk�",
            "kamienn� studzienk�"}),
            "Kamienna studzienka wype�niona jest krystalicznie czyst� "+
            "wod�. Na jej zewn�trznej cz�ci wije si� z�oty bluszcz "+
            "oplataj�cy cia�o nagiej kobiety.\n");
     add_item(({"z�oty bluszcz na studzience",
            "z�oty bluszcz na studni","bluszcz na studni",
            "bluszcz na studzience"}),
            "Niewielki oplataj�cy cia�o bluszcz mieni si� z�otem.\n");
   add_item(({"cia�o nagiej kobiety na studni",
            "cia�o nagiej kobiety na studzience","cia�o kobiety na studni",
            "cia�o kobiety na studzience","kobiet� na studni",
            "kobiet� na studzience"}),
            "Ma�a i pop�kana rze�ba pi�knej nagiej kobiety zdobi "+
            "tutejsz� studni�.\n");
     add_exit("lok3",  ({"sklep","do sklepu","z zewn�trz"}));
    add_exit("lok5",  ({"ku�nia","do ku�ni","z zewn�trz"}));
    add_exit("lok2",({"kuchnia","do kuchni","z zewn�trz"}));
    add_exit("lok4","s");
    npc=clone_object(PATH+"wioska/npc/mezczyzna.c");
    npc->init_arg(0);
    npc->move(this_object());

//    gift1=clone_object

    add_sit("na ziemi","na ziemi","z ziemi",0);

    set_drink_places("ze studni");
}

exits_description()
{
	return "Dalsza cz�� placu ci�gnie si� na po�udnie, natomiast z przeciwnej "+
			"strony dostrzegasz sklep, ku�ni� oraz kuchni�.\n";
}

string
dlugi_opis()
{
    string str;

    str = "Oto centrum lewituj�ce wysoko ponad wszelkim bytem. Na "+
             "samym jego �rodku stoi kamienna studnia po brzegi "+
             "wype�niona krystaliczn� wod�, a tu� za ni� poka�na "+
             "kolumna bij�ca pionowo w g�r�. Rozpylony w powietrzu "+
             "drobny srebrzysty py�ek przeplata si� ze smugami "+
             "magicznej energii. Po�udniowa cz�� placu sk�pana jest "+
             "w bujnej ro�linno�ci, dzikie pn�cza poruszaj� si� w takt "+
             "melodii wiatru, zapraszaj�c w swe progi i odkrywaj�c "+
             "nieznane tajemnice. Zach�d migocze krwistoczerwonym "+
             "blaskiem, jakoby �ywot p�omieni tli� si� w oddali i "+
             "zach�ca� do posmakowania ognistej g��bi. Na p�nocy "+
             "placu dostrzegasz perliste fale magicznej aury, "+
             "mrugaj�ce i u�miechaj�ce si� �un� tajemniczo�ci. "+
             "Wsch�d k��bi si� w t�czowych barwach, kaskad� "+
             "r�nobarwnych odcieni migocze weso�o i nadzwyczaj "+
             "zachecaj�co. Drogi ko�cz� swoj� w�drowk� w tym jednym "+
             "miejscu, oplataj� si� wzajemnie peleryn� nieokie�znanej "+
             "energii i odbiegaj� w przeciwnych kierunkach, "+
             "rozpoczynaj�c wszystko od pocz�tku...";

    return str;
}

void
init()
{
    ::init();
    init_drink_water();
}

void
drink_effect(string skad)
{
    write("Zanurzasz d�onie w krystalicznie czyst� wod� ze studni "+
          "i podnosisz j� do ust.\n");
    saybb(QCIMIE(this_player(), PL_MIA) + " zanurza d�onie w wod� ze studni "+
           "i pije �yk.\n");
}

/* Generalnie staramy si� tworzy� jak najmniej akcji npca
 * w jednej fazie. �eby na zafloodowa�o nagle ekranu...
 */

void faza0start()
{
    faza_reakcji=1;
    npc->command("pokiwaj z usmiechem");
    npc->command("powiedz do "+ OB_NAME(byt) +
                " Witaj.");
    npc->command("popatrz uwaznie na byt astralny");
    npc->command("powiedz do "+ OB_NAME(byt) +
               " Hoho, ju� wiem o co chodzi! Pragniesz dosta� sw� "+
               "cielesn� form�?");
}

void fazaniebyt()
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Jak widz�, decyzja o wyborze rasy zosta�a "+
             "ju� podj�ta. Nic tu po tobie...");
}
void faza0tak()
{
    faza_reakcji=1;
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Mog� ci� uczyni� najpot�niejsz� z ras. Chcesz "+
             "zosta� cz�owiekiem?");
}
void faza0nie()
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Dobrze. Rozejrzyj si� zatem dooko�a i "+
             "wr�� do mnie jak zm�drzejesz.");
}
void faza0inne()
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Co mam przez to rozumie�? Tak czy nie?");
}
void faza1tak()
{
    faza_reakcji=1;
    npc->command("powiedz do "+ OB_NAME(byt) +
             " No, widz�, �e mam do czynienia z odpowiedzialn� "+
             "osob�. Lecz zanim przejdziemy do konkret�w, czuj� "+
             "si� w obowi�zku, by poinformowa� ci� o pewnych "+
             "sprawach. Czy zechcia�"+
             koncoweczka("by�","aby�")+
             " wys�ucha� tego, co mam "+
             "do powiedzenia na temat sytuacji ludzi i nieludzi?");
}
void faza1nie()
{
    faza_reakcji=1;
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Rozejrzyj si� zatem dooko�a i "+
             "wr�� do mnie kiedy zm�drzejesz.");
}
void faza1inne()
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " A niech mnie! Tak czy nie?");
}

/* Dobrze by by�o, gdyby ka�dy enpec opowiada� histori� z w�asnego
 * punktu widzenia, nie?
 * Historia wi�c ma by� subiektywna. Im bardziej, tym lepiej ;)
 *
 *
 * HISTORIA ROZBITA NA FAZY. PIERWSZA FAZA JEST NA SAMYM DOLE: faza2tak()
 * A OSTATNIA NA SAMEJ GORZE (faza2tak4()), wiec od konca czytamy ;)
 */
void faza2tak3()
{
    npc->command("powiedz do "+ OB_NAME(byt) +
         " Eh, ale dosy� tego gadania, bo ju� mnie si� "+
         "r�ce trz��� zaczynaj�, jak tylko pomy�l� o tych dzikusach. "+
         "Zapami�taj, �e oni s� obcy, a ty jeste� sw�j. W�a�nie...Jeste� "+
         "sw�j, czy nie?");
}
void faza2tak2()
{
    npc->command("powiedz do "+ OB_NAME(byt) +
         " Ja ju� jestem stary i zaszli mi nieraz za sk�r�. "+
         "Ty te� si� o nich przekonasz. Niekt�rzy pono chc� �y� w "+
         "naszej spo�eczno�ci, ale ja wiem swoje! He, he, poczuli jak "+
         "to jest, kiedy nie ma co do g�by w�o�y�, a wilki jeno "+
         "czekaj�, a� ci si� padnie pod jakim� d�bem, wi�c si� pchaj� "+
         "do naszych miast, na kt�re drzewiej, takie brzydkie s�owa "+
         "ryli w kamieniu!");
    set_alarm(19.0,0.0,&faza2tak3());
}
void faza2tak1()
{
    npc->command("powiedz do "+ OB_NAME(byt) +
         " Ekhem...My, ludzie przyp�yneli�my na ten kontynent "+
         "dawno temu...z pi�� lub sze�� stuleci da�oby si� zliczy�. "+
         "By� mo�e nie byli�my tu pierwsi, lecz nikt, klne si�, nikt "+
         "jak my nie potrafi tak zawalczy� o swoje! Przemierzaj�c "+
         "�cie�ki tego �wiata napotkasz pewnikiem wielokrotnie istoty "+
         "innej rasy, jednak zapami�taj sobie to, co ci o nich powiem: "+
         "Czy po�yjesz z nimi w zgodzie, czy te� "+
         "b�dziesz broni� ludzkiej godno�ci i t�pi� ich - "+
         "baczenie miej na nich! Czort tylko wie, jak to z nimi jest...");
    set_alarm(22.0,0.0,&faza2tak2());
}
void faza2tak() //Opowiada historie + ostateczne pytanie o rase
{
    faza_reakcji=1;
    npc->command("mlasnij lekko");
    set_alarm(3.0,0.0,&faza2tak1());
}

void faza2nie()
{
    faza_reakcji=1;
    npc->command("powiedz do "+ OB_NAME(byt) +
             " C�, zatem nie poopowiadam tym razem. Nie b�d� "+
             "si� narzuca�, bo pewnie masz wa�niejsze sprawy. "+
             "A wi�c, jeste� pew"+koncoweczka("ien","na")+
             " swego wyboru?");
}
void faza2inne()
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Do diab�a z tob�, tak czy nie?");
}
void faza3tak() //Ostateczne pytanie o rase. Nastepnie pytanie o wzrost
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Kapitalnie! Lecz zanim rozpoczniesz sw� w�dr�wk� "+
             "musz� dowiedzie� si� czego� wi�cej. B�d� ci zadawa� "+
             "teraz pytania o cechy zewn�trzne i wewn�trzne. "+
             "Po pierwsze - jakiego chcesz by� wzrostu?");
}
void faza3nie()
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Nie? Na pewno nie? No dobrze...To nie. Ehh, id� "+
             "wi�c do tych nieludzi i bodaj by ci twoja przysz�a "+
             "g�owa wszami zaros�a, tak, jak im wszystkim!");
}
void faza3inne()
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Nie rozumiem.");
}
void faza4zle() //odpowiedz na wzrost, pytanie o wage
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Nie bardzo rozumiem...Zapytaj mnie o wzrost, je�li "+
             "sprawia ci to problemy.");
}
void faza4dobrze()
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Ba! Tak"+koncoweczka("i","a")+
             " w�a�nie b�dziesz. A teraz powiedz jakiej chcesz "+
             "by� wagi.");
}
void faza5zle() //waga
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Eh, psia ma�. Nie bardzo rozumiem.");
}
void faza5dobrze()
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Doskonale. Ile wiosen chcesz sobie liczy�?");
}
void faza6nierozumiem() //wiek
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Nie rozumiem, ile?");
}
void faza6zamlody()
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " He, he! Tak"+koncoweczka("i","a")+
             " m�od"+koncoweczka("y","a")+
             " to ty nie mo�esz by�! Musisz wybra� nieco wi�cej.");
}
void faza6zastary()
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " He, he! Co, chcesz by� tak"+koncoweczka("i","a")+
             " star"+koncoweczka("y","a")+
             " jak ja? Wybierz mniej.");
}
void faza6dobrze()
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " B�dziesz mia�"+koncoweczka("","a")+
             " "+LANG_SNUM(wiek,0,1)+" "+slowo_lat+
             ". Pi�kny wiek...A jakiego koloru chcesz mie� oczy?");
}
void faza7zle() //kolor oczu
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Hmm, nie znam takiego koloru. "+
             "Znam tylko: "+
    implode(m_indices(plec==1? oczy_kobiety:oczy_mezczyzny), ", ")+".");
}
/* Po prostu idziemy dalej, czyli
 * pytamy o to, czy �yczy sobie posiada� w�osy W OG�LE,
 * czyli, czy gracz chce by� �ysy NA STA�E.
 */
void faza7dobrze8()
{
    faza_reakcji=1;
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Dobrze. "+
             "A co z w�osami? Chcesz je w og�le posiada�?");
}
/* A tutaj omijamy pytanie o to, czy gracz chce w og�le
 * posiada� ow�osienie, poniewa� jest elfem lub p�elfem, a
 * oni nie mog� by� 'na sta�e' �ysi.
 * Oczywi�cie nikt im nie zabroni si� zgoli�, ale w�osy
 * b�d� ros�y im i tak... :)
 * Wi�c pytamy od razu o kolor w�os�w.
 */
void faza7dobrze9()
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Ja tego nigdy nie powinienen m�wi�! Ja jestem cz�owiekiem!"+
             " To jaki� spisek!");
    npc->command("spanikuj"); //w przypadku elfa i polelfa tu
                              //ma byc pytanie od razu o kolor w�os�w.
}

/* Je�li gracz chce posiada� w�osy w og�le, to jedziemy z pytaniem
 * o kolor w�os�w.
 */
void faza8tak()
{
   npc->command("powiedz do "+ OB_NAME(byt) +
             " Jaki chcesz mie� ich naturalny kolor?");
}

/* Kobietom nie zadajemy pyta� o zarost, wi�c omijamy kilka faz
 * i pytamy odrazu o budow� cia�a t� �ys� kobietk� <szok> ....
 */
void faza8nie1()
{
   npc->command("powiedz do "+ OB_NAME(byt) +
             " Ha! �ysa kobieta! Eh, no. Tw�j wyb�r, "+
             "nic mi do tego...A teraz musisz wybra� "+
             "cech� budowy swego przysz�ego cia�a.");
   npc->command("powiedz do "+OB_NAME(byt)+
             " Masz do dyspozycji: "+   //m["indeks"][2]
             COMPOSITE_WORDS2(dostepne_cechy_ciala," lub ")+".");
                             //lista filtrowana przez wybrana wage i wzrost
}

/* A m�czy�ni i tak musz� poda� kolor...zarostu. �eby by�o wiadomo
 * jaki kolor brody i w�s�w im da� (gdyby posiadali w�osy, to wyliczaloby
 * si� to z koloru w�os�w, ale �e wybrali, �e s� permanentnie �ysi...)
 */
void faza8nie2()
{
   npc->command("powiedz do "+ OB_NAME(byt) +
             " Zostaniesz zatem �ysy, lecz i tak poda� musisz kolor swego zarostu. Wi�c jaki to b�dzie kolor?");
}
/* Tak czy nie? Inna odpowied�...
 */
void faza8inne()
{
   npc->command("powiedz do "+ OB_NAME(byt) +
             " Odpowiedz pozytywnie, je�li chcesz posiada� w�osy.");
}
/* kolor wlosow, pytanie o dlugosc
 */
void faza9zle()
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Eh, psia jucha! Nie znam takiego koloru! "+
             "Znam tylko: "+
           //w przypadku innych ras jest s� to inne indeksy mapping�w
    implode(m_indices(plec==1? wlosy_kobiety:wlosy_mezczyzny), ", ")+".");
}
void faza9dobrze()
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Wspaniale. "+
             "A teraz wybierz d�ugo�� w�os�w!");
}
/* Ta faza jest tylko wtedy, je�li si� wybra�o permanentnie �ysego.
 * Bo i tak trzeba poda� kolor zarostu. Wi�c ta faza jest tylko dla m�czyzn
 */
void faza10zle()
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Nie znam takiego koloru zarostu.");
}
void faza10dobrze()
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " A jakiej d�ugo�ci w�sy �yczysz sobie posiada�?");
}
/* W tej fazie gracz wybiera d�ugo�� w�os�w i dostaje pytanie
 * O w�sy, je�li jest m�czyzn�. Je�li jest elfem lub kobiet�,
 * to omijamy te pytania i skaczemy odrazu do pyt. o budowe cia�a
 */
void faza11zle()
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Nie znam takiej d�ugo�ci w�os�w.");
}
/* jesli jest: albo elfem, albo kobiet�, albo poni�ej 19 roku �ycia:
 */
void faza11dobrze1()
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Dobrze! A teraz musisz odpowiedzie�, jak� cech� budowy "+
             "cia�a chcesz posiada�.");
 npc->command("powiedz do "+ OB_NAME(byt) +
             " Masz do dyspozycji: "+   //m["indeks"][2]
             COMPOSITE_WORDS2(dostepne_cechy_ciala," lub ")+".");
                             //lista filtrowana przez wybrana wage i wzrost
}
/* w przeciwnym wypadku:
 */
void faza11dobrze2()
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Dobrze! A teraz odpowiedz jak d�ugie w�sy chcesz posiada�?"+
             " Mo�esz mie� je ogolone, je�li chcesz.");
}
/* d�ugo�� w�s�w:
 */
void faza12zle()
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Nie znam takiej d�ugo�ci w�s�w. Zapytaj mnie o to, "+
             "je�li masz z tym problem.");
}
void faza12dobrze()
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Dobrze! A teraz powiedz, jakiej d�ugo�ci brod� "+
             "�yczysz sobie posiada�. Co prawda wyb�r jest do�� "+
             "skromny, ale wierz�, �e znajdziesz co� dla siebie.");
}
/* d�ugo�� brody
 * i pytanie o ceche budowy cia�a
 */
void faza13zle()
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Nie znam takiej d�ugo�ci.");
}
void faza13dobrze()
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Nadszed� czas na to, by� "+
             "wybra�"+koncoweczka("","a")+
             " cech� charakterystyczn� "+
             "dla budowy twojego przysz�ego cia�a.");
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Masz do dyspozycji: "+   //m["indeks"][2]
             COMPOSITE_WORDS2(dostepne_cechy_ciala," lub ")+".");
                             //lista filtrowana przez wybrana wage i wzrost
}


void faza14zle() //budowa ciala. Pytanie o ceche szczegolna
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Nie znam takiej cechy. Masz do dyspozycji: "+
               COMPOSITE_WORDS2(dostepne_cechy_ciala," lub ")+".");
}
void faza14dobrze()
{
    npc->command("powiedz do "+ OB_NAME(byt) +
         " Mhmm...Czy �yczysz sobie posiada� jak�� cech� szczeg�ln�? "+
         "Mo�esz wybra� w�r�d nast�puj�cych cech: "+
         COMPOSITE_WORDS2(m_indexes(plec==1?cechy_szczegolne_kobiet:
         cechy_szczegolne_mezczyzn)," lub ")+". Odpowiedz tak lub nie.");
}
void faza15tak() //odpowiedz czy chce sie miec ceche szczegolna <opcjonalnie>
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " �wietnie! Zatem jaka to b�dzie cecha?");
}
void faza15nie()
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " A jednak nie b�dziesz wyj�tkow"+
             koncoweczka("y","a")+"...");
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Teraz musimy wybra� twoje pochodzenie! Czy chcesz "+
             "posiada� jakie�? "+
             "Pami�taj, p�niej tak�e b�dziesz "+
             "m"+koncoweczka("�g�","og�a")+" si� na nie "+
             "zdecydowa�, lecz do wyboru b�dziesz mia�"+
             koncoweczka("","a")+" jedynie miejsce, "+
             "w kt�rym si� b�dziesz aktualnie znajdowa�"+
             koncoweczka("","a")+".");
}
void faza15inne()
{
   npc->command("powiedz do "+ OB_NAME(byt) +
             " Hmmm...Tak czy nie?");
}
void faza16zle() //wybieranie cechy szczegolnej (jesli sie zgodzilo wczesniej)
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Nie znam takiej cechy szczeg�lnej.");
}
void faza16dobrze()
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Kapitalnie! W ko�cu kto� odwa�ny!");
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Teraz musimy wybra� twoje pochodzenie! Czy chcesz "+
             "posiada� jakie�? "+
             "Pami�taj, p�niej tak�e b�dziesz "+
             "m"+koncoweczka("�g�","og�a")+" si� na nie "+
             "zdecydowa�, lecz do wyboru b�dziesz mia�"+
             koncoweczka("","a")+" jedynie miejsce, "+
             "w kt�rym si� b�dziesz aktualnie znajdowa�"+
             koncoweczka("","a")+". Raz wybrane, ju� "+
             "nigdy nie ulegnie zmianie, to chyba oczywiste.");;
}
void faza17tak() //odpowiedz czy chce sie posiadac pochodzenie.
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " �wietnie! Zatem sk�d chcesz pochodzi�?");
}
void faza17nie()
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Sprytnie. Teraz musimy wybra� cech� og�ln�, kt�ra "+
             "b�dzie dominowa� z pocz�tku w twych cechach. Do wyboru "+
             "masz " + COMPOSITE_WORDS2(SD_STAT_NAMES_BIE, " oraz ") + ".");
}
void faza17inne()
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Nie rozumiem. Tak czy nie?");
}
void faza18zle() //wybor pochodzenia jesli sie zgodzilo wczesniej.
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Nie znam takiego miejsca, musisz wi�c wybra� sobie inne.");
}
void faza18dobrze()
{

    string pocho = powiedz_pochodzenie();

    npc->command("powiedz do "+ OB_NAME(byt) +
             " Doskonale! Eh, "+pocho[2..]+
             ", ciekawe miejsce... A teraz musisz "+
             "wybra� cech� og�ln�, kt�ra "+
             "b�dzie dominowa� z pocz�tku w twych cechach. Do wyboru "+
             "masz " + COMPOSITE_WORDS2(SD_STAT_NAMES_BIE, " oraz ") + ".");
}
void faza19zle() //cechy
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Nie ma takiej cechy! Przecie� m�wi�em ci jakie "+
             "masz do wyboru!");
}
void faza19dobrze() //cechy
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Rozumiem. A teraz umiej�tno�ci. Mo�esz wybra� "+
             "dwie spo�r�d mej skromnej listy umiej�tno�ci. "+
             "Wybierz wpierw pierwsz�.");
}
void faza20zle() //umiejetnosci - wybor pierwszej
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Przykro mi, nie znam takiej umiej�tno�ci.");
}
void faza20dobrze()
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Rozumiem. A jaki jest tw�j drugi wyb�r?");
}

void faza21zle() //umiejetnosci - wybor drugiej
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Nie znam takiej!");
}
/* nie mo�na wybra� dw�ch umiej�tno�ci takich samych pod rz�d.
 */
void faza21zle2()
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Nie mo�esz wybra� dw�ch takich samych z rz�du!");
}
void faza21dobrze()
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Ha, dobrze...Wygl�da na to, �e to by by�o na tyle."+
             " Czy jeste� got"+koncoweczka("�w","owa")+"?");
}
void faza22tak() //podsumowanie + prezenty?:) No wlasnie...Jakie?
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " To ju� koniec..Eh, co ja m�wi�! Dla ciebie to dopiero "+
             "pocz�tek! Gratuluj�. Masz co� na drog� i zmykaj.");
    npc->command("daj monety, koszule, spodnie i noz "+OB_NAME(byt));
}
void faza22nie()
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Rozumiem. Wr�� do mnie je�li si� namy�lisz, a zaczniemy "+
             "wszystko od nowa.");
}
void faza22inne()
{
    npc->command("powiedz do "+ OB_NAME(byt) +
             " Tak czy nie?");
}

/* A tutaj mamy list� reakcji na odpowiedzi :P
 * Pokazuje sie to za ka�dym razem (natychmiastowo!) po
 * pozytywnej lub negatywnej odpowiedzi.
 * Odpowiedzi b��dne (czyli wywo�uj�ce fazaXinne() nie s�
 * brane pod uwag�).
 */
void reakcje_na_odpowiedzi()
{
    switch(random(15))//im wiecej tym lepiej. 12-15 jest optymalne.
    {
      case 0:
             faza_reakcji==1?:npc->command("powiedz do "+ OB_NAME(byt) +
             " S�uszny wyb�r."); break;
      case 1:
             npc->command("powiedz O�esz."); break;
      case 2:
             npc->command("mlasnij lekko"); break;
      case 3:
             npc->command("potrzyj policzek ."); break;
      case 4:
             npc->command("pokiwaj powoli"); break;
      case 5:
             npc->command("powiedz A niech mnie!"); break;
      case 6:
             faza_reakcji==1?:npc->command("powiedz He, he. Tak my�la�em."); break;
      case 7:
             faza_reakcji==1?:npc->command("emote m�wi cicho do siebie: Hmm, interesuj�ce.");
                                                            break;
      case 8:
             npc->command("pokiwaj nieznacznie"); break;
      case 9:
             npc->command("usmiechnij sie krzywo"); break;
      case 10:
             npc->command("zasycz cicho"); break;
      case 11:
             npc->command("westchnij starczo"); break;
      case 12:
             faza_reakcji==1?:npc->command("powiedz No, no."); break;
      case 13:
             npc->command("powiedz"); break;
      case 14:
             faza_reakcji==1?:npc->command("powiedz �wietnie."); break;

      default:
             npc->command("emote u�miecha si� domy�lnie."); break;
     }
}

void jaka_rasa()
{
    if(plec==1)
      {
        rasa="kobieta";
      }
    else
      {
        rasa="m�czyzna";
      }
  race="cz�owiek";
}

void
przydziel_prezenty(object komu)
{
    gift1=clone_object("/d/Standard/items/bronie/noze/maly_poreczny.c");
    gift2=clone_object("/d/Standard/login/wioska/items/czarne_luzne_Lc.c");
    gift3=clone_object("/d/Standard/login/wioska/items/pozolkla_lniana_M.c");
    gift2->wylicz_rozmiary(0,komu);
    gift3->wylicz_rozmiary(0,komu);
//    gift1->init_arg(0);
    gift1->move(npc);
//    gift2->init_arg(0);
    gift2->move(npc);
//    gift3->init_arg(0);
    gift3->move(npc);

}
