/*
 *  Ku�nia krasnoludzka wioski z etapu tworzenia postaci.
 *        By Lil & Aneta (long lokacji)
 *                          Wed Mar 22 2006
 */

inherit "/d/Standard/login/wioska/std";

#include <std.h>
#include <stdproperties.h>
#include <language.h>
#include <macros.h>
#include <object_types.h>
#include <ss_types.h>
#include <sit.h>
#include <state_desc.h>
#include "login.h"

#define WIOSKA "/d/Standard/login/wioska/"
#define NARZEDZIA "/d/Standard/items/narzedzia/"
#define UBRANIA "/d/Standard/items/ubrania/"
#define SCIEZKA_SPODNI "/d/Standard/Redania/Rinde/przedmioty/ubrania/"

object gift1, gift2, gift3; //prezenty :P
object npc;
int faza_reakcji=0; //jesli jest 1, to npc nie reaguje na wybory np. mowiac "Wspaniale."
                    //dotyczy to przypadkow kiedy gracz np. stwierdzi, ze nie chce byc
                    //danej rasy. Wtedy to by brzmialo bez sensu.
object byt=this_player();

public void
create_wioska_room()
{
    set_short("Ku�nia krasnoludzka");

    add_exit("lok1",  ({"wyj�cie","do wyj�cia","z ku�ni"}));
    npc=clone_object(PATH+"wioska/npc/krasnolud.c");
    npc->init_arg(0);
    npc->move(this_object());
    add_sit("na ziemi","na ziemi","z ziemi",0);
    add_prop(ROOM_I_INSIDE,1);
    add_subloc("na �cianie",0,"ze �ciany");
    add_subloc_prop("na �cianie", SUBLOC_I_DLA_O, O_NARZEDZIA);
    add_subloc_prop("na �cianie", CONT_I_CANT_ODLOZ, 1);
    add_subloc_prop("na �cianie", CONT_I_CANT_POLOZ, 1);
    add_subloc_prop("na �cianie", SUBLOC_I_MOZNA_POWIES, 1);
    add_subloc_prop("na �cianie", CONT_I_MAX_RZECZY, 4);
    dodaj_rzecz_niewyswietlana("niskie ceglane palenisko");
    dodaj_rzecz_niewyswietlana("czarne po�yskuj�ce kowad�o");
    clone_object(WIOSKA+"items/palenisko")->move(this_object());

    clone_object(NARZEDZIA+"szczypce_dlugie_zelazne")->move(this_object(),
                 "na �cianie");
    clone_object(NARZEDZIA+"szczypce_dlugie_zelazne")->move(this_object(),
                 "na �cianie");
    clone_object(NARZEDZIA+"szczypce_dlugie_zelazne")->move(this_object(),
                 "na �cianie");
    clone_object(NARZEDZIA+"kowadlo_czarne_polyskujace")->move(this_object(),
                 "na �cianie");
}

exits_description()
{
	return "Wyj�cie st�d prowadzi na plac.\n";
}

string
dlugi_opis()
{
    string str;

    if(this_player()->query_prop(SIT_SIEDZACY))
      str = "Siedzisz ";
    else
      str = "Stoisz ";

    str+="po�rodku tajemniczego wn�trza ku�ni. Aura "+
          "gor�ca osiada na ka�dym, kto przekroczy progi. ";


    if(jest_rzecz_w_sublokacji("na �cianie","para d�ugich �elaznych szczypiec"))
       str+="Osmolone i zczernia�e �ciany przystrojone s� tu "+
          "zagadkowymi narz�dziami niewiadomego przeznaczenia. ";
    else
      str+="�ciany s� osmolone i zczernia�e do granic mo�liwo�ci. ";

     str+="Jest tu parno i duszno, a ha�asy kucia rozbrzmiewaj� "+
          "w spontanicznej melodii wygrywanej przez krasnoluda. "+
          "Raz po raz m�ot uderza o kowad�o, dope�niaj�c "+
          "akompaniamentu i zwi�kszaj�c panuj�cy rwetes. Obok "+
          "pieca rz�d za rz�dem ci�gn� si� stojaki na bronie, "+
          "a tu� za nimi, na wieszakach �ciennych wisz� "+
          "przer�ne zbroje.";

    return str;
}

void jaka_rasa()
{
    if(plec==1)
      {
        rasa="krasnoludka";
      }
    else
      {
        rasa="krasnolud";
      }
  race="krasnolud";
}

/* Generalnie staramy si� tworzy� jak najmniej akcji npca
 * w jednej fazie. �eby na zafloodowa�o nagle ekranu...
 */

void faza0start()
{
    faza_reakcji=1;
    npc->command("popatrz powoli na byt astralny");
    npc->command("powiedz do " + OB_NAME(byt) +
               " Widz�, �e chcesz"+
               " dosta� cia�o. Cia�o krasnoluda?");
}

void fazaniebyt()
{
    npc->command("powiedz do " + OB_NAME(byt) +
             " Widze, �e ju� podj"+koncoweczka("��e�","�a�")+
	     " decyzj�, nic tu po tobie!");
}
void faza0tak()
{
    faza_reakcji=1;
    npc->command("powiedz do " + OB_NAME(byt) +
             " No, chyba nadawa�"+koncoweczka("by�","aby�")+" si� na krasnolud"+
             koncoweczka("a","k�")+". Chcesz zosta� jedn"+koncoweczka("ym","�")+
             " z nas?");
}
void faza0nie()
{
    npc->command("powiedz do " + OB_NAME(byt) +
             " Ha, wiedzia�em, �e si� nie odwa�ysz.");
}
void faza0inne()
{
    npc->command("sapnij lekko");
    npc->command("powiedz do " + OB_NAME(byt) +
             " Hmm, chyba si� kurna nie zrozumieli�my. Tak czy nie?");
}
void faza1tak()
{
    faza_reakcji=1;
    npc->command("powiedz do " + OB_NAME(byt) +
             " Dobrze. Zostaniesz jedn"+koncoweczka("ym","�")+" z nas.");
    npc->command("pokiwaj posepnie");
    npc->command("powiedz do " + OB_NAME(byt) +
             " Mog� ci przybli�y� po trochu temat naszej rasy, je�li chcesz. "+
             "Chcesz?");
}
void faza1nie()
{
    faza_reakcji=1;
    npc->command("powiedz do " + OB_NAME(byt) +
             " Hehe, wiedzia�em, �e si� nie odwa�ysz.");
    npc->command("machnij");
}
void faza1inne()
{
    npc->command("powiedz do " + OB_NAME(byt) +
             " Kurna, nie rozumiem. Tak czy nie?");
}

/* Dobrze by by�o, gdyby ka�dy enpec opowiada� histori� z w�asnego
 * punktu widzenia, nie?
 * Historia wi�c ma by� subiektywna. Im bardziej, tym lepiej ;)
 *
 *
 *
 * HISTORIA ROZBITA NA FAZY. PIERWSZA FAZA JEST NA SAMYM DOLE: faza2tak()
 * A OSTATNIA NA SAMEJ GORZE (faza2tak4()), wiec od konca czytamy ;)
 */
void faza2tak4()
{
    npc->command("powiedz do " + OB_NAME(byt) +
         " Ale co ja ci tu b�d� prawi� za kazania, jak jaki� klecha ch�do�ony."+
         " Chcesz do��czy� do naszego ludu?");
    npc->command("emote spluwa obficie na ziemi�.");
}
void faza2tak3()
{
    npc->command("powiedz do " + OB_NAME(byt) +
         " M�wi� o nas, �e jeste�my zbyt wybuchowi i �e prostacy. Mo�e to i po trochu prawda, "+
         "mo�e �e� ... Kogo to obchodzi? My tam wiemy swoje! Dmucha� sobie w kasz� "+
         "�aden krasnolud nie daje! A i w popijawie si� chyba ka�dy z nas lubuje, "+
         "tak, jak i do bitki ... Czasem.");
    set_alarm(9.5,0.0,&faza2tak4());
}
void faza2tak2()
{
    npc->command("powiedz do " + OB_NAME(byt) +
         " Taaa, na czym to ja..Ach tak, ju� pami�tam.");
    //npc->command("westchnij cicho");
    set_alarm(3.0,0.0,&faza2tak3());
}
void faza2tak1()
{
    npc->command("powiedz do " + OB_NAME(byt) +
         " My nie zajmujemy si� takimi bzdurami. Mi�dzy nami, a "+
         "lud�mi r�nie bywa. Handlujemy z nimi, a owszem, bo pieni�dz "+
         "nie �mierdzi nawet z ich �ap, ale jak trzeba to i nieraz toporem "+
         "przez �eb takiego trzeba potraktowa�. R�nie to bywa...M�wi�em "+
         "to ju�?");
    npc->command("namysl sie");
    set_alarm(11.0,0.0,&faza2tak2());
}
void faza2tak() //Opowiada historie + ostateczne pytanie o rase
{
    faza_reakcji=1;
    npc->command("emote ociera pot z czo�a.");
    npc->command("powiedz do " + OB_NAME(byt) +
         " Ekheem...No, to my som krasnoludy z Mahakamu, tam te� sk�d i "+
         "nasi mniejsi bracia gnomy. Wi�kszo�� z nas w og�le nie "+
         "wy�azi poza g�ry, bo i po co? Wsz�dzie tylko jakie� "+
         "ch�do�one polityczne awantury. A bo to ichnia kr�lewna si� pu�ci�a, "+
         "czy te prowokanty elfy zn�w co� nabroi�y, e tam...");
    npc->command("machnij reka ciezko");
    set_alarm(14.0,0.0,&faza2tak1());
}
//koniec fazy opowiadania historii

void faza2nie()
{
    faza_reakcji=1;
    npc->command("powiedz do " + OB_NAME(byt) +
             " Jasne, rozumiem. "+
	     "A zatem, czy jeste� pew"+koncoweczka("ien","na")+
             " swego wyboru?");
}
void faza2inne()
{
    npc->command("powiedz do " + OB_NAME(byt) +
             " Kurna, tak czy nie?");
}
void faza3tak() //Ostateczne pytanie o rase. Nastepnie pytanie o wzrost
{
    npc->command("powiedz do " + OB_NAME(byt) +
                 " Hehe, wiedzia�em. Przejd�my teraz do konkret�w.");
    npc->command("emote g�adzi swoj� bujn� rud� brod�.");
    npc->command("powiedz do " + OB_NAME(byt) +
             " Jakiego chcesz by� wzrostu?");
}
void faza3nie()
{
    npc->command("powiedz do " + OB_NAME(byt) +
             " No, tak...Przypuszcza�em, �e si� nie odwa�ysz.");
    npc->command("wzrusz ramionami");
    npc->command("emote wraca do pracy.");
}
void faza3inne()

{
    npc->command("powiedz do " + OB_NAME(byt) +
             " Kurna, nie rozumiem.");
}
void faza4zle() //odpowiedz na wzrost, pytanie o wage
{
    npc->command("powiedz do " + OB_NAME(byt) +
             " Wystarczy, �e spytasz mnie o wzrost, a dowiesz "+
             "si�, jaki mo�esz wybra�.");
}
void faza4dobrze()
{
    npc->command("powiedz do " + OB_NAME(byt) +
             " Mhm, a teraz powiedz mi jakiej chcesz by� wagi.");
    npc->command("wstan");
}
void faza5zle() //waga
{
    npc->command("powiedz do " + OB_NAME(byt) +
             " M"+koncoweczka("�g�by�","og�aby�")+
		" m�wi� g�o�niej? Co� nie dos�ysza�em.");
}
void faza5dobrze()
{
    npc->command("powiedz do " + OB_NAME(byt) +
             " Mhm. Ile chcesz mie� lat?");
}
void faza6nierozumiem() //wiek
{
    npc->command("powiedz do " + OB_NAME(byt) +
             " Psia jucha, nie rozumiem, ile?");
}
void faza6zamlody()
{
    npc->command("powiedz do " + OB_NAME(byt) +
             " Hehe, a� tak"+koncoweczka("i","a")+
             " m�od"+koncoweczka("y","a")+
             " to ty nie mo�esz by�. Musisz wybra� conajmniej "+
		"dziewi�tna�cie.");
}
void faza6zastary()

{
    npc->command("powiedz do " + OB_NAME(byt) +
             " To zdecydowanie za du�o, w takim wieku nie "+
		"m"+koncoweczka("�g�by�","og�aby�")+
		" pozwoli� sobie na wiele przyjemno�ci. Wybierz mniej ni� "+
		"dziewi��dziesi�t, a wi�cej ni� dziewi�tna�cie.");
}
void faza6dobrze()
{
    npc->command("powiedz do " + OB_NAME(byt) +
             " Hmmm...B�dziesz mia�"+koncoweczka("","a")+
             " zatem "+LANG_SNUM(wiek,0,1)+" "+slowo_lat+
             ". A teraz powiedz mi, jakiego koloru chcesz mie� oczy?");
}
void faza7zle() //kolor oczu
{
    npc->command("powiedz do " + OB_NAME(byt) +
             " Kurna, nie znam takiego koloru. "+
             "Znam tylko: "+
    implode(m_indices(oczy_krasnoludow), ", ")+".");
}

/* Po prostu idziemy dalej, czyli
 * pytamy o to, czy �yczy sobie posiada� w�osy W OG�LE,
 * czyli, czy gracz chce by� �ysy NA STA�E.
 */
void faza7dobrze8()
{
    npc->command("powiedz do " + OB_NAME(byt) +
             " Mhmm, "+
             "a co z w�osami? Chcesz je w og�le posiada� "+
		"przysz�"+koncoweczka("y","a")+
                " krasnolud"+koncoweczka("zie","ko")+
		"?");
}
/* A tutaj omijamy pytanie o to, czy gracz chce w og�le
 * posiada� ow�osienie, poniewa� jest elfem lub p�elfem, a
 * oni nie mog� by� 'na sta�e' �ysi.
 * Oczywi�cie nikt im nie zabroni si� zgoli�, ale w�osy
 * b�d� ros�y im i tak... :)
 * Wi�c pytamy od razu o kolor w�os�w.
 */
void faza7dobrze9()
{
    npc->command("zaklnij"); //w przypadku elfa i polelfa tu
                              //ma byc pytanie od razu o kolor w�os�w.
    npc->command("powiedz Kurwa! Sta�o si� co� niespodziewanego! Zg�o� b��d!");
}

/* Je�li gracz chce posiada� w�osy w og�le, to jedziemy z pytaniem
 * o kolor w�os�w.
 */
void faza8tak()
{
   npc->command("powiedz do " + OB_NAME(byt) +
             " Mhmm, zatem jakiego koloru chcesz mie� w�osy?");
}

/* Kobietom nie zadajemy pyta� o zarost, wi�c omijamy kilka faz
 * i pytamy odrazu o budow� cia�a t� �ys� kobietk� <szok> ....
 */
void faza8nie1()
{

   npc->command("powiedz do " + OB_NAME(byt) +
                " �ysa krasnoludka? Ehh..."+
                "Twoj wyb�r!");
   faza11dobrze1();
}

/* A m�czy�ni i tak musz� poda� kolor...zarostu. �eby by�o wiadomo
 * jaki kolor brody i w�s�w im da� (gdyby posiadali w�osy, to wyliczaloby
 * si� to z koloru w�os�w, ale �e wybrali, �e s� permanentnie �ysi...)
 */
void faza8nie2()
{
   npc->command("powiedz do " + OB_NAME(byt) +
             " Zatem b�dziesz �ysym krasnoludem, musisz jednak wybra� kolor "+
	   "swej przysz�ej brody, jaki to b�dzie kolor?");
}
/* Tak czy nie? Inna odpowied�...
 */
void faza8inne()
{
   npc->command("powiedz do " + OB_NAME(byt) +
             " Kurna, odpowiedz pozytywnie, je�li chcesz posiada� w�osy.");
}
/* kolor wlosow, pytanie o dlugosc
 */
void faza9zle()
{
    npc->command("powiedz do " + OB_NAME(byt) +
             " Nie, ten kolor jest z�y. Wybierz kt�ry� z tych: "+
           //w przypadku innych ras jest s� to inne indeksy mapping�w
    implode(m_indices(wlosy_gnomow), ", ")+".");
}
void faza9dobrze()
{
    npc->command("powiedz do " + OB_NAME(byt) +
             " Mhmm, �wietnie..."+
             "A teraz wybierz d�ugo�� w�os�w.");
}
/* Ta faza jest tylko wtedy, je�li si� wybra�o permanentnie �ysego.
 * Bo i tak trzeba poda� kolor zarostu. Wi�c ta faza jest tylko dla m�czyzn
 */
void faza10zle()
{
    npc->command("zaklnij");
    npc->command("powiedz do " + OB_NAME(byt) +
             " Nie znam takiego koloru zarostu.");
}
void faza10dobrze()
{
    npc->command("powiedz do " + OB_NAME(byt) +
             " A jakiej d�ugo�ci w�sy chcesz posiada�?");
}
/* W tej fazie gracz wybiera d�ugo�� w�os�w i dostaje pytanie
 * O w�sy, je�li jest m�czyzn�. Je�li jest elfem lub kobiet�,
 * to omijamy te pytania i skaczemy odrazu do pyt. o budowe cia�a
 */
void faza11zle()
{
    npc->command("zaklnij");
    npc->command("powiedz do " + OB_NAME(byt) +
             " Nie znam takiej d�ugo�ci w�os�w.");
}


/* jesli jest: albo elfem, albo kobiet�, albo poni�ej 19 roku �ycia:
 */
void faza11dobrze1()
{
    npc->command("powiedz do " + OB_NAME(byt) +
             " Dobrze. Zajmijmy si� teraz budow� twego przysz�ego "+
             "krasnoludzkiego cia�a. "+
             "Jak� cech� budowy "+
		"cia�a chcesz posiadac?");
    npc->command("powiedz do " + OB_NAME(byt) +
             " Wybierz spo�r�d: "+   //m["indeks"][2]
             COMPOSITE_WORDS2(dostepne_cechy_ciala," lub ")+".");
                             //lista filtrowana przez wybrana wage i wzros
}
/* w przeciwnym wypadku:
 */
void faza11dobrze2()
{
    npc->command("powiedz do " + OB_NAME(byt) +
             " A co z w�sami? Jak d�ugie maj� by�? Je�li "+
		"chcesz mo�esz ich nie posiada� teraz, ale z "+
                "czasem i tak odrosn�.");
}
/* d�ugo�� w�s�w:
 */
void faza12zle()
{
    npc->command("powiedz do " + OB_NAME(byt) +
             " Psia jucha! Nie znam takiej d�ugo�ci! Je�li masz "+
		"z czym� jaki� problem wystarczy mnie spyta� o to.");
}
void faza12dobrze()
{
    npc->command("powiedz do " + OB_NAME(byt) +
             " A co z brod�? Jakiej d�ugo�ci ma by�?");
}
/* d�ugo�� brody
 * i pytanie o ceche budowy cia�a
 */
void faza13zle()
{
    npc->command("powiedz do " + OB_NAME(byt) +
             " Psia jucha! Nie znam takiej d�ugo�ci! "+
             "Je�li chcesz zapozna� si� z d�ugo�ciami brody, zapytaj "+
             "mnie o to.");
}
void faza13dobrze()

{
    npc->command("powiedz do " + OB_NAME(byt) +
             " Teraz musisz wybra� jak�� cech� budowy swego cia�a.");
    npc->command("mlasnij lekko");
    npc->command("powiedz do " + OB_NAME(byt) +
             " Masz do dyspozycji: "+   //m["indeks"][2]
             COMPOSITE_WORDS2(dostepne_cechy_ciala," lub ")+".");
                             //lista filtrowana przez wybrana wage i wzrost
}


void faza14zle() //budowa ciala. Pytanie o ceche szczegolna
{
    npc->command("powiedz do " + OB_NAME(byt) +
             " Psia jucha! Nie znam takiej cechy. Masz do dyspozycji: "+
               COMPOSITE_WORDS2(dostepne_cechy_ciala," lub ")+".");
}
void faza14dobrze()
{
    npc->command("powiedz do " + OB_NAME(byt) +
         " A czy chcesz by twoje cia�o posiada�o jak�� cech� charakterystyczn�?"+
         " Mo�esz wybra� w�r�d nast�puj�cych cech:");
    npc->command("emote bierze g��boki wdech.");
    npc->command("powiedz do " + OB_NAME(byt) + " "+COMPOSITE_WORDS2(m_indexes(plec==1?cechy_szczegolne_kobiet:
         cechy_szczegolne_mezczyzn)," lub ")+". ");
    npc->command("powiedz do " + OB_NAME(byt) + " Wi�c "+
             "jak b�dzie? Chcesz posiada� jak�� cech� charakterystyczn�?"+
             " Odpowiedz tak lub nie.");
}
void faza15tak() //odpowiedz czy chce sie miec ceche szczegolna <opcjonalnie>
{
    npc->command("powiedz do " + OB_NAME(byt) +
             " Mhm, no nie�le. Wi�c jaka to b�dzie cecha?");
}
void faza15nie()
{
    npc->command("powiedz do " + OB_NAME(byt) +
             " Dobrze, nie b�dziesz wyj�tkow"+
             koncoweczka("y","a")+"...");
    npc->command("powiedz do " + OB_NAME(byt) +
             " Teraz musimy wybra� twoje pochodzenie. Czy chcesz "+
             "posiada� jakie�? "+
             "Pami�taj"+
             " p�niej tak�e b�dziesz "+
             "m"+koncoweczka("�g�","og�a")+" si� na nie "+
             "zdecydowa�, lecz do wyboru b�dziesz mia�"+
             koncoweczka("","a")+" jedynie miejsce, "+
             "w kt�rym si� b�dziesz aktualnie znajdowa�"+
             koncoweczka("","a")+
             ". Pami�taj te�, �e raz wybrane miejsce ju� nigdy si� "+
             "nie zmieni.");
}
void faza15inne()
{
   npc->command("zaklnij");
   npc->command("powiedz do " + OB_NAME(byt) +
             " Psia jucha...Tak czy nie?");
}
void faza16zle() //wybieranie cechy szczegolnej (jesli sie zgodzilo wczesniej)
{
    npc->command("powiedz do " + OB_NAME(byt) +
             " Nie znam takiej cechy szczeg�lnej. Musisz wybra� w�r�d "+
             "tych, kt�re ci wcze�niej wymieni�em.");
}
void faza16dobrze()
{
    npc->command("powiedz do " + OB_NAME(byt) +
             " No, no. Coraz bardziej mnie przekonujesz do tego, �e posiadasz "+
             "jednak prawdziw� krasnoludzk� krew.");
      npc->command("powiedz do " + OB_NAME(byt) +
             " Teraz musimy wybra� twoje pochodzenie. Czy chcesz "+
             "posiada� jakie�? "+
             "Jednak pami�taj! P�niej tak�e b�dziesz "+
             "m"+koncoweczka("�g�","og�a")+" si� na nie "+
             "zdecydowa�, lecz do wyboru b�dziesz mia�"+
             koncoweczka("","a")+" jedynie miejsce, "+
             "w kt�rym si� b�dziesz aktualnie znajdowa�"+
             koncoweczka("","a")+".");
}
void faza17tak() //odpowiedz czy chce sie posiadac pochodzenie.
{
    npc->command("powiedz do " + OB_NAME(byt) +
             " Eh, sk�d wi�c chcesz pochodzi�?");
}
void faza17nie()
{
    npc->command("powiedz do " + OB_NAME(byt) +
             " Teraz musisz mi powiedzie� jak� cech� fizyczn� lub "+
             "mentaln� chcesz, by "+
             "wyr�nia�a si� w�r�d innych? Do wyboru "+
             "masz " + COMPOSITE_WORDS2(SD_STAT_NAMES_BIE, " oraz ") + ".");
}
void faza17inne()
{
    npc->command("zaklnij");
    npc->command("powiedz do " + OB_NAME(byt) +
             " Nie rozumiem. Tak czy nie?");
}
void faza18zle() //wybor pochodzenia jesli sie zgodzilo wczesniej.
{
    npc->command("powiedz do " + OB_NAME(byt) +
             " Nie s�ysza�em o takim miejscu, musisz wybra� inne.");
}
void faza18dobrze()
{
    string pocho = powiedz_pochodzenie();

    npc->command("powiedz do " + OB_NAME(byt) +
             " Hmm "+pocho[2..]+
             ", interesuj�ce. No ale wr��my do pyta�... "+
             "Jak� cech� fizyczn� lub mentaln� chcesz, by "+
             "wyr�nia�a si� w�r�d innych? Do wyboru "+
             "masz " + COMPOSITE_WORDS2(SD_STAT_NAMES_BIE, " oraz ") + ".");
}
void faza19zle() //cechy
{
    npc->command("zalam sie");
    npc->command("powiedz do " + OB_NAME(byt) +
             " Nie ma takiej cechy! Musisz wybra� kt�r�� z tych, "+
             "kt�re wymieni�em.");
}
void faza19dobrze() //cechy
{
    npc->command("powiedz do " + OB_NAME(byt) +
             " Przejd�my do twoich umiej�tno�ci, musisz wybra� sobie "+
             "dwie. Wyb�r jest do�� skromny, ale pewno "+
             "odnajdziesz co� dla siebie. Je�li chcesz pozna� list�, zapytaj "+
             "mnie o to. Jaka zatem b�dzie pierwsza twoja wybrana umiej�tno��?");
}
void faza20zle() //umiejetnosci - wybor pierwszej
{

    npc->command("powiedz do " + OB_NAME(byt) +
             " Nie znam takiej umiej�tno�ci!");
}
void faza20dobrze()
{
    npc->command("powiedz do " + OB_NAME(byt) +
             " Mhm, dobrze. A jaka b�dzie druga umiej�tno��?");
}

void faza21zle() //umiejetnosci - wybor drugiej
{
    npc->command("emote wygl�da na do�� podirytowanego.");
    npc->command("powiedz do " + OB_NAME(byt) +
             " Nie znam takiej! Wybierz inn�.");
}
/* nie mo�na wybra� dw�ch umiej�tno�ci takich samych pod rz�d.
 */
void faza21zle2()
{
    npc->command("powiedz do " + OB_NAME(byt) +
             " Hehe, t� ju� wybra�e� wcze�niej. Wybierz inn� teraz.");

}
void faza21dobrze()
{
    npc->command("powiedz do " + OB_NAME(byt) +
             " Ufff, chyba ju� wszystko za�atwili�my."+
             " Czy jeste� got"+koncoweczka("�w","owa")+"?");
    npc->command("powiedz do " + OB_NAME(byt) + " Mam nadziej�, �e tak, bo ja "+
             "ju� si� zm�czy�em i mam do��.");
}
void faza22tak() //podsumowanie + prezenty?:) No wlasnie...Jakie?
{
    npc->command("powiedz do " + OB_NAME(byt) +
             " Mhmm, nareszcie. Ehh, no dobra, mam dla ciebie "+
             "jeszcze jakie� podarunki. Oto one:");
    npc->command("daj monety, noz, spodnie i koszule "+OB_NAME(byt));
    npc->command("emote klepie ci� przyjacielsko w rami�, szepcz�c: Powodzenia "+
                koncoweczka("bracie","siostro")+".");
    npc->command("oczko");
}
void faza22nie()
{
    npc->command("powiedz do " + OB_NAME(byt) +
             " Teraz mi to m�wisz?!");
    npc->command("zaklnij");
}
void faza22inne()
{
    npc->command("powiedz do " + OB_NAME(byt) +
             " Psia ma�, nie rozumiem, tak czy nie?");
}



/* A tutaj mamy list� reakcji na odpowiedzi :P
 * Pokazuje sie to za ka�dym razem (natychmiastowo!) po
 * pozytywnej lub negatywnej odpowiedzi.
 * Odpowiedzi b��dne (czyli wywo�uj�ce fazaXinne() nie s�
 * brane pod uwag�).
 */
void reakcje_na_odpowiedzi()
{
    switch(random(17))//im wiecej tym lepiej. 12-15 jest optymalne.
    {
      case 0:
             switch(faza_reakcji)
             {
               case 1: break;
               default: npc->command("zamrucz cicho"); break;
             }
      case 1:
             switch(faza_reakcji)
             {
               case 1: break;
               default: npc->command("powiedz Mhmm."); break;
             }
      case 2:
             npc->command("zatrzyj rece"); break;
      case 3:
             npc->command("mrugnij"); break;
      case 4:
             npc->command("pokiwaj powoli"); break;
      case 5:
             switch(faza_reakcji)
             {
               case 1: break;
               default: npc->command("usmiechnij sie mimochodem"); break;
             }
      case 6:
             switch(faza_reakcji)
             {
               case 1: break;
               default: npc->command("beknij"); break;
             }
      case 7:
             switch(faza_reakcji)
             {
               case 1: break;
               default: npc->command("powiedz Hmmm.");
                                                            break;
             }
      case 8:
             npc->command("hmm"); break;
      case 9:
             npc->command("hmm"); break;
      case 10:
             npc->command("emote g�adzi swoj� majestatycznie d�ug� rud� brod�."); break;
      case 11..13:
             npc->command("emote g�adzi swoj� majestatycznie d�ug� rud� brod�."); break;
      case 14:
             switch(faza_reakcji)
             {
               case 1: break;
               default: npc->command("powiedz Mhm, �wietnie..."); break;
             }
      case 15:
             npc->command("mlasnij lekko"); break;
      case 16:
             switch(faza_reakcji)
             {
               case 1: break;
               default: npc->command("powiedz Nie�le.");break;
             }

      default:

             npc->command("wzdrygnij sie"); break;
     }
}

void
przydziel_prezenty(object komu)
{
    gift1=clone_object("/d/Standard/items/bronie/noze/maly_poreczny.c");
    gift2=clone_object(SCIEZKA_SPODNI+"spodnie_los_los_biedaka_krasnolud.c");
    gift3=clone_object("/d/Standard/login/wioska/items/pozolkla_lniana_M.c");
    gift2->wylicz_rozmiary(0,komu);
    gift3->wylicz_rozmiary(0,komu);
    gift1->move(npc);
    gift2->move(npc);
    gift3->move(npc);
}
