/*
 * Patelnia do kuchni w wiosce z etapu tworzenia postaci.
 *             Lil. Thursday 23 of March 2006
 */

inherit "/std/holdable_object";
#include <object_types.h>
#include <materialy.h>
#include <macros.h>
#include <pl.h>
#include <stdproperties.h>
#define OWNER "/d/Standard/login/wioska/npc/niziolek"

create_holdable_object()
{
    ustaw_nazwe("patelnia");
    dodaj_przym("wiekowy","wiekowi");
    dodaj_przym("obszerny","obszerni");
    set_long("Patelnia ta ma przepalone dno i kompletnie nie nadaje "+
             "si� ju� do u�ytku.\n");
    set_hit(2);
    set_pen(3);
    set_dt(W_BLUDGEON);
    set_wt(W_CLUB);
    set_hands(W_ANYH);
    set_likely_dull(MAXINT);
    set_likely_break(MAXINT);
    set_weapon_hits(9991999);
    ustaw_material(MATERIALY_MIEDZ, 100);
    set_type(O_KUCHENNE);
    add_prop(OBJ_I_WEIGHT, 1191);
    add_prop(OBJ_I_VOLUME, 2340);
    add_prop(OBJ_I_VALUE, 0);
    set_owners(({OWNER}));
}
