
/* Okragle kruche ciastko
   Wykonane przez Avarda dnia 11.05.06 */

inherit "/std/food.c";

#include <pl.h>
#include <macros.h>
#include <object_types.h>
#include <stdproperties.h>

void
create_food()
{
    ustaw_nazwe("ciastko");  
    dodaj_przym("okr^ag^ly","okr^agli");
    dodaj_przym("kruchy","kruche");

    set_long("Okr^ag^le i bardzo kruche ciastko o jasnobr^azowym kolorze. "+
             "Z wierzchu posypane jest delikatnym, bia^lym proszkiem.\n");
    set_amount(80);
}
