/*
 * Kocio�ek do kuchni w wiosce z etapu tworzenia postaci.
 *             Lil. Thursday 23 of March 2006
 */

inherit "/std/object";
#include <object_types.h>
#include <materialy.h>
#include <macros.h>
#include <pl.h>
#include <stdproperties.h>
#define OWNER "/d/Standard/login/wioska/npc/niziolek"

create_object()
{
    ustaw_nazwe("kocio�ek");
    dodaj_przym("niewielki","niewielcy");
    set_long("Czarny niewielki kocio�ek zamocowany jest tu� nad "+
             "kamiennym paleniskiem.\n");
    ustaw_material(MATERIALY_MIEDZ, 100);
    set_type(O_KUCHENNE);
    add_prop(OBJ_I_WEIGHT, 3891);
    add_prop(OBJ_I_VOLUME, 15340);
    add_prop(OBJ_I_VALUE, 26);
    add_prop(OBJ_I_NO_GET, "Kocio�ek jest przymocowany nad paleniskiem.\n");
//    add_prop(OBJ_I_DONT_GLANCE, 1);
    set_owners(({OWNER}));
}
