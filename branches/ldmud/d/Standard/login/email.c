/* Lil tu byla ;) ale tylko na chwilke i tylko w celu edycji funkcji write. :P
 *
 * ...I jeszcze dopisala pytanie o plec postaci, tuz po odmianie imienia
 *    przez przypadki... :)
 *
 * --- Zenek tez tu byl ---
 *
 */

inherit "/std/room";

#include <login.h>
#include <options.h>
#include <stdproperties.h>
#include <living_desc.h>
#include <const.h>
#include <pl.h>
#include <macros.h>

static void ask_player();
void next_query();

create_room()
{
    set_long("");
    set_short("");

    add_prop(ROOM_I_LIGHT, -10);
    add_prop(ROOM_I_INSIDE, 1);
    add_prop(ROOM_I_NIE_ZAPISUJ,1);
    add_prop(ROOM_S_DARK_LONG, "");
}

public void
enter_inv(object ob, object from)
{
    ::enter_inv(ob, from);

    if (ob->query_ghost() & GP_EMAIL)
        set_alarm(1.0, 0.0, &ask_player());
}


/*****************************************************************
 *
 * The questions to ask an entirely new player, which is not handled
 * in the configuration process.
 *
     Ask for email adress

 */
static string   *new_queries,   /* Kolejne pytania */
                *odmiana,       /* Pytania o przypadki */
                *przypadki;     /* Tablica z odmiana imienia */

/*
 * Function name: ask_player
 * Description:   Ask some questions of new players
 */
static void
ask_player()
{
    odmiana = ({ "q_odmien_dopelniacz", "q_odmien_celownik",
        "q_odmien_biernik", "q_odmien_narzednik",
        "q_odmien_miejscownik", "q_potwierdz_odmiane" });
    new_queries = ({ "dummy" }) + ({ "q_wybor_kodowania" }) + odmiana +
        ({ "q_wybor_plci" }) + (this_player()->query_mailaddr() ? ({}) : ({ "q_mail" }));
    przypadki = allocate(6);
    przypadki[0] = lower_case(this_player()->query_real_name());

    next_query();
    return;
}

/*
 * Function name: end_query
 * Description:
 * Return:
 */
static void
end_query()
{
    int tries = 0;
    this_player()->set_ghost((this_player()->query_ghost()) & (~GP_EMAIL));
    this_player()->catch_msg("\n\n");

    //Dla kazdego nowego gracza tworzy sie nowa kopia wioski...
    while (tries < 5)
    {
        object lok = clone_object("/d/Standard/login/wioska/lok1.c");
        if (MASTER_OB(lok) != file_name(lok))
        {
            TP->nowy_system_expa_ustawiony();
            this_player()->move_living("X", lok, 1,0);
            return;
        }
        tries++;
    }
    this_player()->catch_msg("\nNiestety, ale na chwil� obecn� stworzenie twojej" +
            "postaci nie jest mo�liwe, prosz� spr�bowa� za pi�� minut.\n");
    this_player()->quit();
}

/*
 * Function name: next_query
 * Description:   Asks the next question of the user interactively.
 */
void
next_query()
{
    while (1)
    {
        if (sizeof(new_queries) < 2)
            return end_query(); /* does not return */

        new_queries = slice_array(new_queries, 1, sizeof(new_queries));

        if (call_other(this_object(), new_queries[0] + "_pretext"))
        {
            input_to(new_queries[0]);
            return;
        }
    }
}

/*
 * Function name: again_query
 * Description:   Asks the same question again.
 */
static void
again_query()
{
    if (call_other(this_object(), new_queries[0] + "_pretext"))
    {
        input_to(new_queries[0]);
        return;
    }
    next_query();
}


int
q_mail_pretext()
{
    /*
     * Do not ask if there is already an email
     */
    if (this_player()->query_mailaddr())
        return 0;

    write("\nPodaj swoj email (lub 'nie mam'): ");
    return 1;
}

int
q_odmien_dopelniacz_pretext()
{
    write("\nPodaj jak odmienia si� Twoje imi� przez przypadki.\n" +
          "Postaraj si� to prawid�owo wpisa�, gdy� mo�e zaj�� sytuacja, gdy "+
          "inny gracz\nnie b�dzie m�g� wykona� na Tobie jakiej� komendy.\n" +
          "Pami�taj, �e imi� musi by� odpowiednie do konwencji tego muda, a tak�e "+
          "pasowa� do wybranej przez Ciebie p�niej rasy. W odmianie mo�esz u�y� " +
          "polskich liter jak � czy �, a nawet masz obowi�zek to zrobi� je�li imi� tego wymaga.\n" +
          "Jako mianownik podstawiam Twoje imi�.\n");

    write("Dope�niacz [kogo,czego nie ma?]: ");
    return 1;
}

int
q_odmien_celownik_pretext()
{
    write("Celownik [komu,czemu si� przygl�dam?]: ");
    return 1;
}

int
q_odmien_biernik_pretext()
{
    write("Biernik [kogo,co widz�?]: ");
    return 1;
}

int
q_odmien_narzednik_pretext()
{
    write("Narz�dnik [z kim,z czym id�?]: ");
    return 1;
}

int
q_odmien_miejscownik_pretext()
{
    write("Miejscownik [o kim, o czym m�wi�?]: ");
    return 1;
}

int
q_potwierdz_odmiane_pretext()
{
    write("\nOto jak wygl�da odmiana Twego imienia:\n");
    write("Mianownik:   " + capitalize(przypadki[0]) + "\n");
    write("Dope�niacz:  " + capitalize(przypadki[1]) + "\n");
    write("Celownik:    " + capitalize(przypadki[2]) + "\n");
    write("Biernik:     " + capitalize(przypadki[3]) + "\n");
    write("Narz�dnik:   " + capitalize(przypadki[4]) + "\n");
    write("Miejscownik: " + capitalize(przypadki[5]) + "\n");
    write("\nPrzeczytaj swe odpowiedzi jeszcze raz, upewniaj�c si�, �e s� " +
        "one w�a�ciwe.\nPosiadanie �le odmienionego imienia jest wbrew " +
        "zasadom. Je�li masz\nw�tpliwo�ci, co do poprawno�ci odmiany, " +
        "lepiej zapytaj kogo�.\n(najlepiej w og�le nie wybiera� " +
        "kontrowersyjnych w odmianie imion).\nPrzez pierwsze kilka godzin " +
        "gry b�dziesz w stanie poprawi� swoj� odmian�.\n\n" +
        "Czy odmiana jest prawid�owa[t/n]: ");

    return 1;
}

int
q_potwierdz_again_pretext()
{
    write("Odpowiedz [t]ak, lub [n]ie: ");
    return 1;
}

int
q_wybor_kodowania_pretext()
{
    write("\nWybierz rodzaj kodowania. Wybierz pierwsza opcje, jesli nie chcesz "+
          "widziec polskich znakow diakrytycznych. Pamietaj, ze gdy zajdzie taka "+
          "potrzeba - w kazdej chwili bedziesz mogl to zmienic w swoich 'opcjach'.\n\n");
    write("1. brak\t\tznaki: EEeeOOooAAaaSSssLLllZZzzXXxxCCccNNnn\n");
    this_player()->ustaw_czcionke(1);
    write("2. iso8859-2\tznaki: "+
             "E^Ee^eO^Oo^oA^Aa^aS^Ss^sL^Ll^lZ^Zz^zX^Xx^xC^Cc^cN^Nn^n\n");
    this_player()->ustaw_czcionke(2);
    write("3. cp1250\tznaki: "+
             "E^Ee^eO^Oo^oA^Aa^aS^Ss^sL^Ll^lZ^Zz^zX^Xx^xC^Cc^cN^Nn^n\n");
    this_player()->ustaw_czcionke(4);
    write("4. utf-8\tznaki: "+
             "E^Ee^eO^Oo^oA^Aa^aS^Ss^sL^Ll^lZ^Zz^zX^Xx^xC^Cc^cN^Nn^n\n");
    this_player()->ustaw_czcionke(0);
    write("Odpowiedz '1','2','3' lub '4': ");
    return 1;
}


int
q_wybor_plci_pretext()
{
   write("\nCzy �yczysz sobie przemierza� �wiat jako kobieta czy jako m�czyzna?\n"+
         "Odpowiedz 'k' lub 'm': ");
   return 1;
}

/*
 * Function:    q_mail
 * Description: This function is called using input_to, and sets the
 *              email adress of this player.
 */
static void
q_mail(string maddr)
{
    this_player()->set_mailaddr(maddr);
    next_query();
}

static int
wlasciwe_imie(string str)
{
    int x;

    str = plain_string(str);
    x = strlen(str);

    if (x < 3)
    {
        write("Za kr�tkie imi�.\n");
        return 0;
    }

    while (--x >= 0)
    {
        if (str[x] < 'a' || str[x] > 'z')
        {
            write("Imi� nie mo�e zawiera� �adnych spacji, apostrof�w, " +
                "ani innych znak�w specjalnych. Dopuszczalne s� tylko " +
                "ma�e litery, od a do �.\n\n");
            return 0;
        }
    }

    return 1;
}

static void
q_odmien_dopelniacz(string przyp)
{
    if (!wlasciwe_imie(przyp))
    {
        again_query();
        return ;
    }
    przypadki[1] = lower_case(przyp);
    next_query();
}

static void
q_odmien_celownik(string przyp)
{
    if (!wlasciwe_imie(przyp))
    {
        again_query();
        return ;
    }
    przypadki[2] = lower_case(przyp);
    next_query();
}

static void
q_odmien_biernik(string przyp)
{
    if (!wlasciwe_imie(przyp))
    {
        again_query();
        return ;
    }
    przypadki[3] = lower_case(przyp);
    next_query();
}

static void
q_odmien_narzednik(string przyp)
{
    if (!wlasciwe_imie(przyp))
    {
        again_query();
        return ;
    }
    przypadki[4] = lower_case(przyp);
    next_query();
}

static void
q_odmien_miejscownik(string przyp)
{
    if (!wlasciwe_imie(przyp))
    {
        again_query();
        return ;
    }
    przypadki[5] = lower_case(przyp);
    next_query();
}

static void
q_potwierdz_odmiane(string odp)
{
    string o;

    o = lower_case(odp[0..0]);

    if (o == "n")
    {
        new_queries = odmiana + new_queries[1..];
        again_query();
        return;
    }
    else if (o == "t")
    {
//         this_player()->ustaw_imie(przypadki);
         next_query();
    }
    else
    {
        new_queries = ({ "q_potwierdz_again" }) + new_queries[1..];
        again_query();
        return;
    }
}

static void
q_wybor_kodowania(string odp)
{
   string o;
   o=lower_case(odp[0..0]);

   if(o=="1")
       this_player()->ustaw_czcionke(0);
   else if(o=="2")
       this_player()->ustaw_czcionke(1);
   else if(o=="3")
       this_player()->ustaw_czcionke(2);
   else if(o=="4")
       this_player()->ustaw_czcionke(4);
   else
   {
        write("Nie bardzo rozumiem. Wpisz '1', '2','3' lub '4'.\n");
        again_query();
        return;
   }

   next_query();
}


static void
q_wybor_plci(string odp)
{
   string o;
   o=lower_case(odp[0..0]);

   if(o=="k")
   {
        this_player()->set_gender(G_FEMALE);
        this_player()->ustaw_imie(przypadki, PL_ZENSKI);
   }
   else
   if(o=="m")
   {
        this_player()->set_gender(G_MALE);
        this_player()->ustaw_imie(przypadki, PL_MESKI_OS);
   }
   else
   {
        write("Nie bardzo rozumiem. Wpisz 'k' lub 'm'.\n");
        again_query();
        return;
   }

    write("\nDobrze.\n\n"+
          "Witaj w etapie kreowania postaci Vatt'gherna!\n"+
          "To tutaj zadecydujesz "+
          "o rasie, wygl�dzie, cechach, umiej�tno�ciach oraz "+
          "wiele innych. Podstawow� form� komunikacji z innymi istotami "+
          "jest 'odpowiadanie' na zadawane pytania. Je�li utkniesz w "+
          "danym etapie - pami�taj, �e zawsze mo�esz zapyta� o to.\n"+
          "Wpisz '?', by uzyska� pomoc.\n");
    this_player()->set_option(OPT_ECHO, 1); //A zeby i w wiosce bylo ;)
    set_alarm(3.0,0.0,&next_query());
}

static void
q_potwierdz_again(string odp)
{
    q_potwierdz_odmiane(odp);
    return;
}
