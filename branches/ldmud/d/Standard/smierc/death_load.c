/**
 * \file /d/Standard/smierc/death_load.c
 *
 */

inherit "/std/object";

#include <macros.h>
#include <std.h>
#include <stdproperties.h>
#include <filepath.h>
#include "/d/Standard/login/login.h"
#include "smierci.h"

#pragma strict_types

private mapping smierci = ([]);

public  int dorzuc_smierc(string str);
public  int usun_smierc(string str);

public void
create_object()
{
    setuid();
    seteuid(getuid());

    ustaw_nazwe(({ "ko��", "ko�ci", "ko�ci", "ko��", "ko�ci�", "ko�ci" }),
        ({ "ko�ci", "ko�ci", "ko�ciom", "ko�ci", "ko��mi", "ko�ciach" }),
        PL_ZENSKI);

    dodaj_przym("d�ugi", "d�udzy");
    dodaj_przym("po��kly", "po��kli");

    set_long("Sadz�c po kszta�cie jest to ludzka ko�� udowa. " +
        "Stanowi ona magiczne medium, pozwalaj�ce dowolnie kreowa� " +
        "za�wiaty wedle w�asnej woli. S�u�y do tego komenda " +
        "'dodaj'.\n");

    add_prop(OBJ_I_WEIGHT, 1250);
    add_prop(OBJ_I_VOLUME, 930);
    add_prop(OBJ_I_VALUE, 0);
}

public void
init()
{
    add_action(&dorzuc_smierc(), "dodaj");
    add_action(&usun_smierc(), "usu�");
}

public int
dorzuc_smierc(string str)
{
    string tekst, plik, rasa;
    mixed *zawartosc;
    int ix, size;

    if (!strlen(str))
    {
        notify_fail("Sk�adnia: dodaj <plik> <rasa>.\n");
        return 0;
    }

    zawartosc = explode(str, " ");
    if (sizeof(zawartosc) != 2)
    {
        notify_fail("Sk�adnia: dodaj <plik> <rasa>.\n");
        return 0;
    }

    if (SECURITY->query_wiz_rank(this_player()->query_real_name()) < WIZ_ARCH &&
        !SECURITY->query_admin_team_member(TP->query_real_name(), "mg"))
    {
        write("Tylko cz�onkowie administracji i mg mog� dodawa� nowe �mierci.\n");
        return 1;
    }

    plik = zawartosc[0]; rasa = lower_case(zawartosc[1]);

    if (member_array(rasa, RACES) == -1)
    {
        write(sprintf("Nie ma takiej rasy: '%s'.\n", rasa));
        return 1;
    }

    plik = FTPATH(this_player()->query_path(), plik);
    tekst = read_file(plik);
    if (!tekst)
    {
        write(sprintf("Nie ma takiego pliku: '%s'.\n", plik));
        return 1;
    }

    zawartosc = explode(tekst, "\n\n");
    ix = -1; size = sizeof(zawartosc);

    while(++ix < size)
        if (strlen(zawartosc[ix]))
            zawartosc[ix] = implode(explode(zawartosc[ix], "\n"), " ");

    restore_object(SMIERCI_SAVE);

    if (!mappingp(smierci))
        smierci = ([ ]);

    if (smierci[rasa])
        smierci[rasa] += ({ zawartosc });
    else
        smierci[rasa] = ({ zawartosc });

    save_object(SMIERCI_SAVE);

    write("Opis dodany.\n");
    return 1;
}

public int usun_smierc(string str)
{
    string *exp;

    NF("Sk�adnia: usu� [wszystkie] <rasa> [<numer>].\n");

    if(!str)
        return 0;

    exp = explode(str, " ");

    if(!sizeof(exp))
        return 0;

    if(exp[0] == "wszystkie")
    {
        if(!is_mapping_index(exp[1], smierci))
            return NF("Nie ma takiej rasy.\n");

        smierci = m_delete(smierci, exp[1]);

        write("Wszystie �mierci rasy - " + exp[1] + " zosta�y usuni�te.\n");
    }
    else
    {
        if(!is_mapping_index(exp[0], smierci))
            return NF("Nie ma takiej rasy.\n");

        if(member_array(atoi(exp[1]), smierci[exp[0]]) == -1)
            return NF("Nie zdefiniowana zosta�a taka �mier� dla rasy - " + exp[0] + ".\n");

        smierci[exp[0]] = exclude_array(smierci[exp[0]], atoi(exp[1]), atoi(exp[1]));

        write("�mier� rasy - " + exp[0] + " zosta�a usuni�ta.\n");
    }

    return 1;
}