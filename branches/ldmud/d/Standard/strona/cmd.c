/**
 * \file /d/Standard/strona/cmd.c
 *
 * desc
 *
 * @author Krun <krun@vattghern.com.pl>
 * @date 20.7.2008 00:10:46
 */


#pragma no_reset
#pragma no_inherit
#pragma save_binary
#pragma strict_types

inherit "/cmd/std/command_driver";

#include <std.h>
#include <files.h>
#include <macros.h>

string
get_soul_id()
{
    return "strona";
}

int
query_cmd_soul()
{
    return 1;
}

mapping
query_cmdlist()
{
    return (["szasady" : "zasady",
             "hierarchia" : "hierarchia"]);
}

int
zasady(string arg)
{
    if(!file_exists("/doc/zasady/zasady.xml"))
        write("Zasady s� w chwili obecnej niedost�pne.\n");
    else
        TP->command("cat /doc/zasady/zasady.xml");

    return 1;
}

private int
filter_wiz(string imie)
{
    if(sizeof(explode(imie, "@")) > 1 || imie == "root" || imie == "strona")
        return 0;

    return 1;
}

private void
write_wiz_tab(string type, mixed arr, string imie, int d = 0)
{
    write("        <" + type + ">\n" +
        "            <name>" + imie + "</name>\n" +
        "            <surname>" + (arr[0] ?: "") + "</surname>\n" +
        "            <origin>" + (arr[1] ?: "") + "</origin>\n" +
        "            <wiztitle>" + (SECURITY->query_wiz_pretitle(imie) ?: "") + "</wiztitle>\n" +
        "            <webtitle>" + (arr[2] ?: "") + "</webtitle>\n" +
        "            <title>" + (arr[3] ?: "") + "</title>\n" +
        (d == 1 ? "            <domain>" + (SECURITY->query_wiz_dom(imie) ?: "") + "</domain>\n" : "") +
        "        </" + type + ">\n");
}

int
hierarchia(string arg)
{
    int i, ilosc, rm_player;
    object player;
    string *keeper, *arch, *liege, *steward, *mage, *wizard, *pilgrim, *retired, *apprentice, *exwiz, *aall;
    string surname, origin, title, wtitle;
    mapping all;

    // Z wszystkich musimy usun�� konta mailowe, narazie s� tylko na apprecie, ale b�g jeden wie co b�dzie kiedy�.
    keeper      = filter(SECURITY->query_wiz_list(WIZ_KEEPER), filter_wiz);
    arch        = filter(SECURITY->query_wiz_list(WIZ_ARCH), filter_wiz);
    liege       = filter(SECURITY->query_wiz_list(WIZ_LORD), filter_wiz);
    steward     = filter(SECURITY->query_wiz_list(WIZ_STEWARD), filter_wiz);
    mage        = filter(SECURITY->query_wiz_list(WIZ_MAGE), filter_wiz);
    wizard      = filter(SECURITY->query_wiz_list(WIZ_NORMAL), filter_wiz);
    retired     = filter(SECURITY->query_wiz_list(WIZ_RETIRED), filter_wiz);
    pilgrim     = filter(SECURITY->query_wiz_list(WIZ_PILGRIM), filter_wiz);
    apprentice  = filter(SECURITY->query_wiz_list(WIZ_APPRENTICE), filter_wiz);

    aall = keeper + arch + liege + steward + mage + wizard + pilgrim + retired + apprentice;

    all = ([]);

    ilosc = sizeof(aall);

    write("<?xml version=\"1.0\" encoding=\"ISO-8859-2\"?>\n");
    write("<xml>\n");
    write("    <ilosc>" + ilosc + "</ilosc>\n");
    write("    <pracujacych>" + sizeof(keeper + arch + liege + steward + mage + wizard) + "</pracujacych>\n");
    write("    <czarodzieje>\n");

    for(i = 0 ; i < ilosc ; i++)
    {
        player = find_player(aall[i]);

        if(!player)
            player = SECURITY->finger_player(aall[i]);

        if(!player)
            continue;

        all[aall[i]] = ({ (stringp(surname = player->query_surname()) ? clear_color_format(surname) : 0),
                          (stringp(origin  = player->query_origin()) ? clear_color_format(origin) : 0),
                          (stringp(wtitle  = player->query_web_title()) ? clear_color_format(wtitle) : 0),
                          (stringp(title   = player->query_title()) ? clear_color_format(title) : 0) });

        if(!interactive(player))
            player->remove_object();
    }

    all["jeremian"] = ({0, "z Rinde", "Przez d�ugi okres utrzymywa� serwer Vatt'gherna, " +
        "by� te� Przewodnicz�cym Kapitu�y, Ojciec Wsp�za�o�yciel", 0});
    all["molder"]   = ({0, 0, "To on w du�ej mierze odpowiada za wygl�d i wizj� Vatt'gherna. " +
        "By� najd�u�ej urz�duj�cym w historii Vatt'gherna Przewodnicz�cym Kapitu�y, Ojciec Wsp�za�o�yciel", 0});
    all["delvert"]  = ({0, 0, "Z�ota r�czka, zawsze wszystko wiedzia�, zawsze wszystko " +
        "zrobi�(jak mu si� chcia�o:P). Dzi�ki niemu na mudzie mamy polskie znaki diakrytyczne. " +
        "Ojciec Wsp�za�o�yciel", 0});
    all["lil"]      = ({0, 0, "Pomys�odawczyni i wsp�a�torka systemu tworzenia postaci, " +
        "wieloletnia opiekunka Redani.", 0});
    all["gregh"]    = ({0, 0, "Autor poprzedniej wersji &lt;a href=&quot;http://old.vattghern.com.pl&quot;&gt;strony&lt;/a&gt;, " +
        "i najlepszej szafy graj�cej na Vacie.", 0});
    all["gjoef"]    = ({0, 0, "Po odej�ciu Lil to on opiekowa� si� Redani� jak i Akademiami "+
        "w Aretuzie i Ban Ard, kt�rych by� Rektorem.", 0});

    exwiz = ({"jeremian", "molder", "delvert", "lil", "gregh", "gjoef"});

    foreach(string i : keeper)
    {
        if(!is_mapping_index(i, all))
            continue;

        write_wiz_tab("keeper", all[i], i);
    }

    foreach(string i : arch)
    {
        if(!is_mapping_index(i, all))
            continue;

        write_wiz_tab("arch", all[i], i);
    }

    foreach(string i : liege)
    {
        if(!is_mapping_index(i, all))
            continue;

        write_wiz_tab("liege", all[i], i, 1);
    }

    foreach(string i : steward)
    {
        if(!is_mapping_index(i, all))
            continue;

        write_wiz_tab("steward", all[i], i, 1);
    }

    foreach(string i : mage)
    {
        if(!is_mapping_index(i, all))
            continue;

        write_wiz_tab("mage", all[i], i, 1);
    }

    foreach(string i : wizard)
    {
        if(!is_mapping_index(i, all))
            continue;

        write_wiz_tab("wizard", all[i], i, 1);
    }

    foreach(string i : retired)
    {
        if(!is_mapping_index(i, all))
            continue;

        write_wiz_tab("retired", all[i], i, 1);
    }

    foreach(string i : pilgrim)
    {
        if(!is_mapping_index(i, all))
            continue;

        write_wiz_tab("pilgrim", all[i], i);
    }


    foreach(string i : apprentice)
    {
        if(!is_mapping_index(i, all))
            continue;

        write_wiz_tab("apprentice", all[i], i);
    }

    foreach(string i : exwiz)
    {
        if(!is_mapping_index(i, all))
            continue;

       write_wiz_tab("exwiz", all[i], i);
    }

    write("    </czarodzieje>\n");
    write("</xml>\n");

    return 1;
}