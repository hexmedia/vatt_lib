/* Autor: faeve
 * Opis : Jasmina
 * Data : 8.07.07 */

inherit "/std/lampa_uliczna";
#include <stdproperties.h>
#include <macros.h>
#include "dir.h"
void
create_street_lamp()
{
	ustaw_nazwe("lampka");
   set_long("@@dlugasny@@\n");
   set_extinguish_message("Jeden ze s^lu^z^acych gasi lampk^e.\n");
}

string
dlugasny()
{
	string str;

	str = "Niewielkie oliwne lampki os^loni^ete s^a odymionym szk^lem. ";
	
	if(environment(TO)->jest_dzien() == 1)
    {
		str += "Zgaszone teraz, eksponuj^a tylko ca^ly brud i kurz, jaki "+
            "zdo^la^ly na sobie zgromadzi^c. ";
	}
	else
	{
		str += "Rozpalone migoc^a lekko i daj^a r^ownie tyle dymu, co i "+
            "^swiat^la.";
	}

	return str;
}
