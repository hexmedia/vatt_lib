/*
 * Krzeslo do karczmy w Pianie
 * opis: Jasmina
 * zepsu^la: Faeve
 * dn. 16.07.07                */

inherit "/std/object.c";

#include "dir.h"
#include <pl.h>
#include <stdproperties.h>
#include <macros.h>
#include <filter_funs.h>
#include <materialy.h>
#include <object_types.h>

create_object()
{
    ustaw_nazwe("krzes^lo");

    dodaj_przym("stary", "starzy");
   set_type(O_MEBLE);
   ustaw_material(MATERIALY_DR_DAB);
   make_me_sitable("na","na starym krze^sle","ze starego krzes^la",1, 
       PL_MIE, "na");
    set_long("Krzes^lo jest ju^z stare, za^s krzywe, niezbyt r^owne "+
        "d^lugo^sci^a nogi powoduj^a jego niepewne chybotanie si^e. Oparcie "+
        "zosta^lo wr^ecz niemi^losiernie porysowane. \n");
    add_prop(OBJ_I_WEIGHT, 4521);
    add_prop(OBJ_I_VOLUME, 5990);
    add_prop(OBJ_I_VALUE, 2);
    add_prop(OBJ_M_NO_SELL, 1);

//    set_owners(({--------})); FIXME
}
