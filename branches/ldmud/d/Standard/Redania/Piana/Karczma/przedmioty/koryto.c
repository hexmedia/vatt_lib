/* Autor: Faeve
  Opis : Samaia
  Data : 24 marca 2008
*/

inherit "/std/container";

#include "/sys/macros.h"
#include "/sys/pl.h"
#include "/sys/materialy.h"
#include "/sys/object_types.h"
#include "/sys/formulas.h"
#include "dir.h"

void
create_container()
{
    ustaw_nazwe("koryto");
    dodaj_przym("ci^e^zki","ci^e^zcy");
    dodaj_przym("drewniany", "drewniani");

    set_long("Wydr^a^zone w pote^znym, pod^lu^znym klocu jasnego drewna "+
        "koryto idealnie pe^lni swoj^a funkcj^e pojenia lub karmienia "+
        "zwierz^at - jest bardzo ci^e^zkie, dzi^eki czemu nie^latwo je "+
        "przewr^ocic. Ponadto ustawione zosta^lo na stapilnej konstrukcji o "+
        "czterech nogach, wykonanej z tego samego materia^lu. Nieliczne "+
        "zadrapania sugeruj^a, ^ze nie jest ono specjalnie nowe, z "+
        "pewno^sci^a jednak wygl^ada schludnie i estetycznie mimo kilku "+
        "skaz.\n");
         
    add_prop(OBJ_I_WEIGHT, 7000);
    add_prop(OBJ_I_VOLUME, 7000);
    add_prop(OBJ_I_VALUE, 25);

    add_prop(OBJ_M_NO_GET, "Koryto jest zbyt ci^e^zkie, by je podnie^s^c. \n");

    ustaw_material(MATERIALY_DR_DAB, 100); 
    set_owners(({PIANA_NPC + "karczmarz"}));

    add_prop(OBJ_I_DONT_GLANCE, 1);;

}