/* opis Jasminy
 * dn. 9.07.07
 * faeve */

#include <pl.h>
#include <macros.h>
#include "dir.h"

inherit "/std/window.c";

string okno();

void
create_window()
{
    ustaw_nazwe("okno");

    set_window_id("OKNO_KARCZMA_PIANA");
    set_open_desc("");
    set_other_room(PIANA_KARCZMA + "lokacje/podworze.c");

    set_closed_desc("");
    set_window_desc(&okno());
    set_open(1);
    set_locked(0);

    set_pass_mess("Wyskakujesz przez niedu^ze okno na zewn^atrz.\n");
}

string
okno()
{
    string str;

    str = "Niezbyt du^ze okno ";

    object zaslony = present("zas^lony", ENV(TO));
    if(zaslony->query_prop("odslonieta") == 0)
    {
        str += "przes^loni^ete jest bur^a, niegdy^s zapewne brunatn^a "+
                "zas^lon^a, z kt^or^a czas nie obszed^l si^e zbyt ^laskawie - "+
                "materia^l jest okopcony i podarty. ";
    }
    else
    {
        str += "jest w tej chwili ods^loni^ete - bure, okopcone zas^lony "+
                "opadaj^a po obu stronach okien, za^s przez zamazane i zatarte "+
                "brudem szyby ";


        if(environment(TO)->jest_dzien() == 1)
        {
            str += "wpadaj^a snopy dziennego ^swiat^la. ";
        }
        else
        {
            str += "w ciemno^sci wida^c tylko odleg^le ^swiat^la z "+
                "innych budynk^ow w osadzie. ";
        }
    }

    str += "\n";

    return str;
}