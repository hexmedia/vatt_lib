/* Autor: faeve
 * Opis : Jasmina
 * Data : 16.07.07 */

inherit "/std/lampa_uliczna";
#include <stdproperties.h>
#include <macros.h>
#include "dir.h"
void
create_street_lamp()
{
	ustaw_nazwe("lampka");
   set_long("@@dlugasny@@\n");
   set_extinguish_message("Jeden ze s^lu^z^acych gasi lampk^e.\n");
}

string
dlugasny()
{
	string str;

	str = "Niedu^za, oliwna lampka, ";
    	
	if(environment(TO)->jest_dzien() == 1)
    {
		str += "obecnie zgaszona. ";
	}
	else
	{
		str += "rozpalona, daje bardzo ma^lo ^swiat^la, ledwie "+
            "rozja^sniaj^ac otoczenie wok^o^l siebie. ";
	}

	return str;
}
