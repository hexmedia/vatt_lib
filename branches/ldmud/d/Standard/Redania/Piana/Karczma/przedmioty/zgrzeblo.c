/* Autor: Faeve
  Opis : Jasmina
  Data : sierpie^n 2007
*/

inherit "/std/holdable_object";

#include "/sys/macros.h"
#include "/sys/pl.h"
#include "/sys/materialy.h"
#include "/sys/object_types.h"
#include "/sys/formulas.h"
#include "dir.h"

void
create_holdable_object()
{
    ustaw_nazwe("zgrzeb^lo");
    dodaj_przym("du^zy","duzi");
    dodaj_przym("prosty", "pro^sci");

    set_long("Jest to szczotka o bardzo grubym i sztywnym w^losiu, "+
        "s^lu^z^aca do wst^epnego wyczesywania czesania ko^nskiej "+
        "sier^sci. \n");
         
    add_prop(OBJ_I_WEIGHT, 450);
    add_prop(OBJ_I_VOLUME, 400);
    add_prop(OBJ_I_VALUE, 30);
    set_type(O_NARZEDZIA);

    ustaw_material(MATERIALY_WIKLINA, 30); //to niby te wlosie ;)
    ustaw_material(MATERIALY_DR_BUK, 70);
    set_owners(({PIANA_NPC + "karczmarz"}));

    add_prop(OBJ_I_DONT_GLANCE, 1);;

}