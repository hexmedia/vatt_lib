/* opis Jasminy
 * dn. 16.07.07
 * faeve */

#include <pl.h>
#include <macros.h>
#include "dir.h"

inherit "/std/window";

string okno();

void
create_window()
{
    ustaw_nazwe("okno");

    set_window_id("OKNO_KARCZMA_POKOJ_PIANA");
    set_open_desc("");
    set_closed_desc("");
    set_window_desc(&okno());
    set_open(0);
    set_locked(1);	

//    set_owners(({""}));
}

string
okno()
{
    string str;

    str = "Niedu^ze wykuszowe okno, obecnie ";

    object zaslony = present("brunatna zas^lona", ENV(TO));
    if(zaslony->query_prop("odslonieta") == 0)
    {
        str += "zas^loni^ete brunatn^a zas^lon^a, kt^ora pruje si^e u do^lu. ";
    }
    else
    {
        str += "ods^loni^ete, ";
        if(environment(TO)->jest_dzien() == 1)
        {
            str += "wpuszcza do pomieszczenia odrobin^e ^swiat^la przez "+
                "brudn^a, zamazan^a szyb^e. ";
        }
        else
        {
            str += "ukazuje brudn^a, zamazan^a szyb^e kryj^ac^a za "+
                "sob^a jedynie mrok nocy. ";
        }
    }

    str += "\n";

    return str;
}