/* Autor: Faeve
   Opis : Jasmina
   Data : 16.07.07 */

inherit "/std/object";

#include "dir.h"
#include <cmdparse.h>
#include <pl.h>
#include <macros.h>
#include <stdproperties.h>
#include <object_types.h>
#include <materialy.h>

int zwin_dywan(string str);
int rozwin_dywan(string str);

string opis_zwiniety();

void create_object()
{
    ustaw_nazwe("dywanik");
    dodaj_przym("dziurawy", "dziurawi");
    dodaj_przym("brunatny", "brunatni");

    set_long("Brunatny i niewyszukany, pe^len jest przetar^c i dziur - w "+
        "niekt^ore ^smia^lo zmie^sci si^e nawet bardzo gruby paluch. \n"+
        "@@opis_zwiniety@@\n");
    add_prop(OBJ_I_WEIGHT, 400);
    add_prop(OBJ_I_VOLUME, 250);
    add_prop(OBJ_I_VALUE, 5);

    make_me_sitable("na", "na dywaniku", "z dywanika",1);

    set_type(O_MEBLE);
    ustaw_material(MATERIALY_TK_MULINA);
    //set_owners(({----})); FIXME!!!
}

init()
{
  ::init();
  add_action(&zwin_dywan(), "zwi^n");
  add_action(&rozwin_dywan(), "rozwi^n");
}

int zwin_dywan(string str)
{
  object obj;

  if(!str) return 0;

  if(!parse_command(lower_case(str), environment(this_player()), "%o:"+PL_BIE, obj))
    return 0;

  if (obj != this_object())
    return 0;

  if(!this_object()->query_prop(OBJ_M_NO_GET))
    {
      notify_fail("Przecie^z dywanik jest ju^z zwiniety!\n");
      return 0;
    }
  write("Zwijasz dywanik w nieco niezgrabny rulon.\n");
  saybb(QCIMIE(this_player(), PL_MIA)+" zwija "+this_object()->short(PL_BIE)
    +" w niezgrabny rulon.\n");
  remove_prop(OBJ_M_NO_GET); 
  remove_prop("_siedzonko");
  return 1;
}

int rozwin_dywan(string str)
{
  object obj;

  if(!str) return 0;
  if(!parse_command(lower_case(str), environment(this_player()), "%o:"+PL_BIE, obj))
    return 0;

  if(obj != this_object())
    return 0;
  
  if(function_exists("create_container", environment(this_object())) != "/std/room")
    {
      notify_fail("Mo^ze najpierw go gdzie^s po^lo^zysz?\n");
      return 0;
    }

  if(!this_object()->query_prop(OBJ_M_NO_GET))
    {      
      write("Kilkoma sprawnymi ruchami rozwijasz dywanik.\n");
      saybb(QCIMIE(this_player(), PL_MIA)+" kilkoma sprawnymi ruchami rozwija "
        +this_object()->short(PL_BIE)+".\n");
      add_prop(OBJ_M_NO_GET, "Dywanik jst troch^e niepor^eczny. Mo^ze lepiej"
           +" by^loby go najpierw zwin�^c?\n");
      make_me_sitable("na", "na dywaniku", "z dywanika", 2);
      return 1;
    }
  notify_fail("Przecie^z dywanik jest ju^z rozwini^ety!\n");
  return 0;
}

string opis_zwiniety()
{
  if(!this_object()->query_prop(OBJ_M_NO_GET))
    return "W tej chwili jest zwini^ety.";
  return "W tej chwili jest rozwini^ety.";
}

public int
jestem_chodniczkiem_karczmy_piana()
{
        return 1;
}

