/* sto^lek do karczmy w Pianie
 * opis Jasminy
 * zepsu^la faeve
 * dn. 8.07.07 */

inherit "/std/object.c";

#include "/sys/stdproperties.h"
#include "/sys/macros.h"
#include "/sys/pl.h"
#include "/sys/materialy.h"
#include "/sys/object_types.h"

void
create_object(){
	ustaw_nazwe("sto^lek",PL_MESKI_NOS_NZYW);
	set_long("Prosty sto^lek, mog^acy z powodzeniem s^lu^zy^c za krzes^lo, "+
        "cho^c nie posiada oparcia. Okr^ag^ly, na trzech nogach - jest "+
        "brudny i porysowany. \n");
	dodaj_przym("prosty","pro^sci");
    dodaj_przym("drewniany","drewniani");
	set_type(O_MEBLE);
	ustaw_material(MATERIALY_DR_DAB, 100);
	add_prop(CONT_I_WEIGHT, 1176);
	make_me_sitable("na","na sto^lku","ze sto^lka",1);
	
}
public int
jestem_stolkiem_karczmy_piana()
{
        return 1;
}