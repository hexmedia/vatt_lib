/* Autor: Faeve
  Opis : Jasmina
  Data : sierpie^n 2007
*/

inherit "/std/holdable_object";

#include "/sys/macros.h"
#include "/sys/pl.h"
#include "/sys/materialy.h"
#include "/sys/object_types.h"
#include "/sys/formulas.h"
#include "dir.h"

void
create_holdable_object()
{
    ustaw_nazwe("grabie");
    dodaj_przym("stary","starzy");
    dodaj_przym("prosty", "pro^sci");

    dodaj_nazwy("narz^edzie");

    set_long("Proste grabie, pordzewia^le nieco na ko^ncach z^eb^ow. \n");
         
    add_prop(OBJ_I_WEIGHT, 1500);
    add_prop(OBJ_I_VOLUME, 1500);
    add_prop(OBJ_I_VALUE, 10);
    set_type(O_NARZEDZIA);

    ustaw_material(MATERIALY_RZ_STAL, 30);
    ustaw_material(MATERIALY_DR_DAB, 70);
    set_owners(({PIANA_NPC + "karczmarz"}));

    add_prop(OBJ_I_DONT_GLANCE, 1);

}

public int
jestem_narzedziem_karczmyp()
{
        return 1;
}