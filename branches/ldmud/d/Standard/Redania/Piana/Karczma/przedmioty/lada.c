/* Lada do karczmy w Pianie
   opis Jasminy
   zrobila Faeve, dn. 15.07.07 */
inherit "/std/container";

#include "dir.h"
#include "/sys/stdproperties.h"
#include "/sys/macros.h"
#include "/sys/pl.h"
#include "/sys/materialy.h"
#include "/sys/object_types.h"

void
create_container()
{
    ustaw_nazwe("lada");

    ustaw_material(MATERIALY_DR_DAB);
    set_type(O_MEBLE);
   set_long("Lada ci^agnie si^e wzd^lu^z zachodniej ^sciany. Jej blat jest "+
       "porysowany i brudny. W g^l^ebi, za lad^a dostrzec mo^zna rz^ad "+
       "p^o^lek zastawionych przer^o^znymi butelkami, obok za^s zawsze "+
       "p^lon^ace palenisko, nad kt^orym wisi kocio^lek - zwykle gotuje "+
       "si^e w nim jaka^s strawa. "+
        "@@opis_sublokacji|Na ladzie dostrzegasz |na|.\n|\n|" + PL_BIE+ "@@");
	     
    add_prop(CONT_I_WEIGHT, 43000);
    add_prop(CONT_I_VOLUME, 40000);
    add_prop(OBJ_I_VALUE, 45);
    
    add_subloc("na");
    add_subloc_prop("na", CONT_I_MAX_WEIGHT, 100000);
    add_subloc_prop("na", CONT_I_MAX_VOLUME, 150000);
    add_prop(CONT_I_DONT_SHOW_CONTENTS, 1);
    add_prop(CONT_I_CANT_WLOZ_DO, 1);
//    set_owners(({-----})); FIXME!

}

