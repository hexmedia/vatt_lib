/* Autor: Faeve
  Opis : Jasmina
  Data : sierpie^n 2007
*/

inherit "/std/holdable_object";

#include "/sys/macros.h"
#include "/sys/pl.h"
#include "/sys/materialy.h"
#include "/sys/object_types.h"
#include "/sys/formulas.h"
#include "dir.h"

void
create_holdable_object()
{
    ustaw_nazwe("miot^la");
    dodaj_przym("zwyk^ly","zwykli");
    dodaj_przym("prosty", "pro^sci");

    dodaj_nazwy("narz^edzie");

    set_long("Miot^la na d^lugim trzonku za koniec ma zwi^azan^a z "+
        "kr^otkich, mi^ekkich ga^l^ezi wi^azank^e. \n");
         
    add_prop(OBJ_I_WEIGHT, 1000);
    add_prop(OBJ_I_VOLUME, 1000);
    add_prop(OBJ_I_VALUE, 10);
    add_prop(OBJ_I_DONT_GLANCE, 1);
    set_type(O_NARZEDZIA);

    ustaw_material(MATERIALY_DR_BRZOZA, 30);
    ustaw_material(MATERIALY_DR_DAB, 70);
    set_owners(({PIANA_NPC + "karczmarz"}));

}
public int
jestem_narzedziem_karczmyp()
{
        return 1;
}