/* Autor: Faeve
   Opis : Jasmina
   Data : 16.07.07 */

inherit "/std/object";

#include "dir.h"
#include <cmdparse.h>
#include <pl.h>
#include <macros.h>
#include <stdproperties.h>
#include <object_types.h>
#include <materialy.h>

void create_object()
{
    ustaw_nazwe("prze^scierad^lo");
    dodaj_przym("szary", "szarzy");
    dodaj_przym("poplamiony", "poplamieni");

    set_long("Szare i poplamione prze^scierad^lo pruje si^e na brzegach, "+
        "posiada te^z kilka niewielkich dziur. \n");
    add_prop(OBJ_I_WEIGHT, 400);
    add_prop(OBJ_I_VOLUME, 185);
    add_prop(OBJ_I_VALUE, 5);

    ustaw_material(MATERIALY_TK_LEN);
    //set_owners(({----})); FIXME!!!
}