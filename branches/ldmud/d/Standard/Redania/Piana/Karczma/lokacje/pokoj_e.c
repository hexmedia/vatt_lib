/**
 * \file /d/Standard/Redania/Piana/Karczma/lokacje/pokoj_e.c
 *
 * @author kod - Faeve
 * @author opis - Jasmina
 * @date 16.07.07
 *
 *  jeden z pokoi do wynajecia w karczmie w Pianie
 *  ten na zachod od korytarza
 */

#include <stdproperties.h>
#include <mudtime.h>
#include <macros.h>
#include <object_types.h>
#include "dir.h"

inherit PIANA_STD;
inherit "/lib/peek";

void
create_piana()
{
    set_short("Niewielki pok^oj.");
    set_long("@@dlugi_opis@@");

    add_prop(ROOM_I_INSIDE, 1);

    add_item("pod^log^e","Zakurzona i skrzypi^aca pod^loga u^lo^zona "+
        "zosta^la z niezbyt r^owno dopasowanych desek.\n");
    add_item("dr^agi","Nied^lugie dr^agi przymocowane do po^ludniowej i "+
        "p^o^lnocnej ^sciany. Jeden z nich zosta^l w po^lowie u^lamany. Na "+
        "drugim z nich wisi oliwna lampka.\n");

    add_object(PIANA_KARCZMA + "przedmioty/stoliczek_pokoj");
    dodaj_rzecz_niewyswietlana("niedu^zy kwadratowy stoliczek");
    add_object(PIANA_KARCZMA + "przedmioty/lozko_pokoj_e");
    dodaj_rzecz_niewyswietlana("^l^o^zko");
    dodaj_rzecz_niewyswietlana("poprute prze^scierad^lo");
    add_object(PIANA_KARCZMA + "przedmioty/ciezkie_krzeslo");
    dodaj_rzecz_niewyswietlana("stare ci^e^zkie krzes^lo");
    add_object(PIANA_KARCZMA + "przedmioty/szafa");
    dodaj_rzecz_niewyswietlana("szafa");
    add_object(PIANA_KARCZMA + "przedmioty/lampka_wspolny");
    add_object(PIANA_KARCZMA + "przedmioty/ciemnoniebieska_zaslona");
    add_object(PIANA_KARCZMA + "przedmioty/okno_pokoj_e");
    add_object(PIANA_KARCZMA + "przedmioty/popruty_dywanik");
    dodaj_rzecz_niewyswietlana("popruty granatowy dywanik");

    add_door(PIANA_KARCZMA + "lokacje/drzwi/z_pokoju_e");

    set_niewyswietlane_mask(O_MEBLE);

    set_event_time(80.0);
    add_event("@@evenciki:"+file_name(TO)+"@@");
}

string
dlugi_opis()
{
    string str;

    str = "Pok^oj jest niewielki, za^s jego powierzchni^e umniejsza jeszcze "+
        "uko^snie biegn^acy dach, ^l^acz^acy si^e ze wschodni^a ^scian^a "+
        "niewiele powy^zej pod^logi. Na ^srodku zachodniej ^sciany "+
        "widniej^a niewielkie drzwiczki prowadz^ace na korytarz. Przy "+
        "p^o^lnocnym kra^ncu pomieszczenia stoi du^ze, zbite z solidnych "+
        "desek ^l^o^zko, wyposa^zone w siennik";

    if(jest_rzecz_w_sublokacji(0,"po^z^o^lk^le poprute prze^scierad^lo"))
    {
        str += " i po^z^o^lk^le, poprute prze^scierad^lo. ";
    }
    else
    {
        str += ". ";
    }

    str +=  "Tu^z obok niego, na zachodniej ^scianie ustawiono p^ekat^a "+
        "szaf^e z ciemnego drewna. ";

    int stolik, krzeslo;

    stolik  = jest_rzecz_w_sublokacji(0, "niedu�y kwadratowy stoliczek");
    krzeslo = jest_rzecz_w_sublokacji(0, "stare ci�kie krzes�o");


    if(stolik || krzeslo)
    {
        str += "Po przeciwnej stronie pokoju ";

        if(stolik)
        {
            str += "postawiono niedu�y, kwadratowy stoliczek ";
            if(krzeslo)
                str += "przy kt�rym znajduje si� stare, ci�kie krzes�o";

            str += ". ";
        }
        else
            str += "postawiono stare, cie�kie krzes�o. ";
    }

    str += "Do p^o^lnocnej i po^ludniowej ^sciany przymocowano tak^ze "+
        "nied^lugie dr^agi, jeden z nich jest w po^lowie u^lamany, drugi "+
        "za^s s^lu^zy za wieszad^lo dla oliwnej lampki. W uko^snym "+
        "dachu mie^sci si^e niedu^ze, wykuszowe okno ";

    object zaslony = present("zas^lony");
    if(zaslony->query_prop("odslonieta") == 0)
        str += "przes^loni^ete ciemnoniebiesk^a, brudn^a zas^lon^a. ";
    else
    {
        str += "ods^loni^ete,";

        if(jest_dzien())
            str += "wpuszczaj^ac dziennie ^swiat^lo przez niezbyt czyst^a szyb^e. ";
        else
            str += "ukazuj^ac przez niezbyt czyst^a szyb^e ciemno^s^c nocy. ";
    }

    str += "Pod^loga jest zakurzona i skrzypi^aca, przy ^l^o^zku ";

    if(jest_rzecz_w_sublokacji(0,"popruty granatowy dywanik"))
        str += "przykryta poprutym dywanikiem. ";
    else
    {
        str += "prezentuj^aca czystszy prostok^at, wcze^sniej zapewne "+
            "czym^s zakryty. ";
    }

    str += "\n";
    return str;
}



public string
exits_description()
{
    return "Na zachodniej ^scianie znajduj^a si^e drzwiczki prowadz^ace na "+
        "korytarz.\n";
}

string
evenciki()
{
   switch(pora_dnia())
   {
       case MT_SWIT:
       case MT_WCZESNY_RANEK:
       case MT_RANEK:
       case MT_POLUDNIE:
       case MT_POPOLUDNIE:
           return ({"Mucha przelecia^la ci ko^lo nosa i usiad^la na "+
           "szafie.\n","Paj^ak przebieg^l po ^scianie przy ^l^o^zku.\n",
           "Wiatr zastuka^l w okiennic^e.\n","Pod^loga skrzypn^e^la "+
           "cicho.\n"})[random(4)];
       default:
           return ({"Co^s przemkn^e^lo przez pok^oj.\n","Z korytarza "+
           "dochodzi ci^e jaki^s kr^otki stuk.\n","^L^o^zko zaskrzypia^lo "+
           "cicho.\n","Zdawa^lo ci si^e, ^ze zas^lona poruszy^la si^e "+
           "lekko.\n"})[random(4)];
   }
}
