/*
 * drzwi do karczmy w Pianie
 * opis: Jasmina
 * popsu^la: faeve
 * dn. 07.07.07
 */

inherit "/std/door";

#include <pl.h>
#include "dir.h"

void
create_door()
{
    ustaw_nazwe("wrota");
    dodaj_przym("drewniany","drewniani");

    set_other_room(PIANA_KARCZMA + "lokacje/podworze.c");
    set_door_id("DRZWI_DO_STAJNI_PIANA");
    set_door_desc("Wrota na podw^orzec zbite zosta^ly z nie do ko^nca "+
        "dopasowanych do siebie desek.\n");

    set_open_desc("");
    set_closed_desc("");

    set_pass_command(({"wyj^scie","wrota","podw^orze","zach^od"}),
        "przez wrota","na zewn^atrz");

    set_open(0);
    set_locked(0);

    set_key("KLUCZ_DRZWI_DO_STAJNI_PIANA");
    set_lock_name(({"zamek", "zamku", "zamkowi", "zamek", "zamkiem", "zamku"}), 0, PL_MESKI_NOS_NZYW);
}