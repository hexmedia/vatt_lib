/*
 * podworze karczmy w Pianie
 * opis by Jasmina
 * zepsula Faeve
 *
 */

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <pogoda.h>
#include "dir.h"

inherit PIANA_STD;

void
create_piana()
{
    set_short("Stajnia.");
    set_long("@@dlugi_opis@@");

    add_object(PIANA_KARCZMA + "lokacje/drzwi/ze_stajni");
    add_object(PIANA_KARCZMA + "przedmioty/miotla");
    add_object(PIANA_KARCZMA + "przedmioty/szpadel");
    add_object(PIANA_KARCZMA + "przedmioty/widly");
    add_object(PIANA_KARCZMA + "przedmioty/zgrzeblo");
    add_object(PIANA_KARCZMA + "przedmioty/grabie");
    add_object(PIANA_KARCZMA + "przedmioty/koryto");
    add_object(PIANA_KARCZMA + "przedmioty/worek");

add_item("przegrody","Przegrody dla koni. W g^l^ebi ka^zdej zauwa^zy^c "+
    "mo^zna worki z pasz^a i koryto na wod^e. \n");

add_item(({"st^og","siano"}),"Ogromny st^og usypany z siana, s^lu^z^acy "+
    "jako zapas jedzenia dla koni. \n");

add_item("stajni^e","Budynek stajni wzniesiono w ca^lo^sci z drewna, w "+
    "dodatku z niezbyt dobrze dopasowanych desek. Pomi^edzy szczelinami i w "+
    "dziurach po wielu s^ekach dostrzec mo^zna stogi siana i przegrody dla "+
    "koni znajduj^ace si^e wewn^atrz budynku. Dach pokryto s^lom^a, za^s "+
    "wej^scia broni^a proste, zbite z desek wrota. \n");

add_item("przegrody","W stajni mie^sci si^e osiem przegr^od dla koni - po "+
    "cztery na p^o^lnocnej i po^ludniowej ^scianie. Oddzielone s^a od "+
    "siebie drewnianymi p^o^l^sciankami, tak, by ka^zdy ko^n mia^l sw^oj "+
    "k^acik, a jednocze^snie nie czu^l odizolowany od innych. \n");

add_sit("na ziemi","na ziemi","z ziemi",6);
add_sit("na sianie","na sianie","z siana",4);


set_event_time(50.0);
add_event("@@evenciki:"+file_name(TO)+"@@");
}

string
dlugi_opis()
{
    string str;

    int i, il, il_narzedzi;
    object *inwentarz;

    inwentarz = all_inventory();
    il_narzedzi = 0;
    il = sizeof(inwentarz);

    for (i = 0; i < il; ++i) {
         if (inwentarz[i]->jestem_narzedziem_karczmyp()) {
                        ++il_narzedzi; }}

    str = "Pe^lna kurzu i ziaren zb^o^z stajnia nie nale^zy do du^zych "+
        "budynk^ow - mie^sci zaledwie osiem przegr^od dla koni, po cztery "+
        "na p^o^lnocnej i po^ludniowej ^scianie. Wolne miejsce pomi^edzy "+
        "rz^edami przegr^od w du^zej cz^e^sci zajmuje wielki st^og siana, "+
        "s^lu^z^acy jako zapas jedzenia dla tutejszych wierzchowc^ow. "; 
        
    if(jest_rzecz_w_sublokacji(0,"pojemny jutowy worek") &&
        jest_rzecz_w_sublokacji(0,"ci^e^zkie drewniane koryto"))
    {
        str += "Obok niego stoj^a pojemny worek, po brzegi pe^len "+
            "rezerw paszy dla zwierz^at oraz drewniane koryto. ";
    }
    if(jest_rzecz_w_sublokacji(0,"pojemny jutowy worek"))
    {
        str += "Obok niego stoi pojemny worek, po brzegi pe^len rezerw "+
            "paszy dla zwierz^at. ";
    }
    if(jest_rzecz_w_sublokacji(0,"ci^e^zkie drewniane koryto"))
    {
        str += "Tu^z obok niego stoi ci^e^zkie drewniane koryto. ";
    }



    if(il_narzedzi >= 3)
    {
        str += "W rogu obok boks^ow znajduje si^e u^lo^zony stosik "+
            "pospolitych narz^edzi gospodarskich. ";
    }
    if(il_narzedzi == 2)
    {
        if(jest_rzecz_w_sublokacji(0,"stare proste grabie") &&
            jest_rzecz_w_sublokacji(0,"zwyk^le proste wid^ly"))
        {
        str += "W rogu obok boks^ow stoj^a wid^ly i grabie. ";
        }
        if(jest_rzecz_w_sublokacji(0,"stare proste grabie") &&
            jest_rzecz_w_sublokacji(0,"zwyk^la prosta miot^la"))
        {
        str += "W rogu obok boks^ow stoj^a wid^ly i miot^la. ";
        }
        if(jest_rzecz_w_sublokacji(0,"stare proste grabie") &&
            jest_rzecz_w_sublokacji(0,"du^zy u^lamany szpadel"))
        {
        str += "W rogu obok boks^ow stoj^a grabie oraz szpadel. ";
        }
        if(jest_rzecz_w_sublokacji(0,"zwyk^la prosta miot^la") &&
            jest_rzecz_w_sublokacji(0,"zwyk^le proste wid^ly"))
        {
        str += "W rogu obok boks^ow stoj^a wid^ly i miot^la. ";
        }
        if(jest_rzecz_w_sublokacji(0,"zwyk^la prosta miot^la") &&
            jest_rzecz_w_sublokacji(0,"du^zy u^lamany szpadel"))
        {
        str += "W rogu obok boks^ow stoj^a miot^la oraz szpadel. ";
        }
        if(jest_rzecz_w_sublokacji(0,"du^zy u^lamany szpadel") &&
            jest_rzecz_w_sublokacji(0,"zwyk^le proste wid^ly"))
        {
        str += "W rogu obok boks^ow stoj^a wid^ly i szpadel. ";
        }
    }
    if(il_narzedzi == 1)
    {
        if(jest_rzecz_w_sublokacji(0,"stare proste grabie"))
        {
            str += "W rogu obok boks^ow stoj^a stare proste grabie. ";
        }

        if(jest_rzecz_w_sublokacji(0,"zwyk^le proste wid^ly"))
        {
            str += "W rogu obok boks^ow stoj^a zwyk^le proste wid^ly. ";
        }

        if(jest_rzecz_w_sublokacji(0,"zwyk^la prosta miot^la"))
        {
            str += "W rogu obok boks^ow stoi zwyk^la prosta miot^la. ";
        }

        if(jest_rzecz_w_sublokacji(0,"du^zy u^lamany szpadel"))
        {
            str += "W rogu obok boks^ow stoi du^zy u^lamany szpadel. ";
        }
    }

    if(il_narzedzi == 0)
    {
        str += "W rogu obok boks^ow zauwa^zy^c mo^zna miejsce troch^e mniej "+
            "zapylone i jakby wolne od wsz^edobylskich ziaren i k^los^ow. ";
    }

    str += "Dach pokryty zosta^l pospolit^a s^lomian^a strzech^a, przez "+
        "kt^or^a prze^swituj^a dziury, za^s pod^loga - zwyk^la uklepana "+
        "ziemia - ca^la przykryta jest py^lem oraz ziarnami i k^losami "+
        "zb^o^z.";
    str += "\n";

    return str;

}

public string
exits_description()
{
    return "Znajduj^ace si^e na zachodzie pomieszczenia drewniane wrota "+
        "prowadz^a na podw^orze. \n";
    }

string
drzwi()
{
    string str;

    str = "Mocne, okute drzwi wykonane z d^ebowego drewna. Teraz s^a ";

    if(TO->query_open() == 1)
        {
        str += "otwarte.";
        }
        else
            {
            str += "zamkni^ete.";
            }

    return str;
}

string
wrota()
{
    string str;

    str = "Mocne, okute drzwi wykonane z d^ebowego drewna. Teraz s^a ";

	if(TO->query_open() == 1)
	{
		str += "otwarte.";
	}
	else
	{
		str += "zamkni^ete.";
	}

	return str;
}

string
evenciki()
{
   switch(pora_dnia())
   {
       case MT_SWIT:
       case MT_WCZESNY_RANEK:
       case MT_RANEK:
       case MT_POLUDNIE:
       case MT_POPOLUDNIE:
           return ({"Du^za, zielonkawa mucha przelecia^la obok twojej "+
           "g^lowy.\n","Zapach siana wierci ci w nosie. \n","Przez dziur^e "+
           "w dachu do stajni wlecia^l wr^obel.\n","^Xd^xb^lo s^lomy "+
           "spad^lo z dachu na twoj^a g^low^e. \n"})[random(4)];
       default:
           return ({"Co^s poruszy^lo si^e w jednej z przegr^od.\n",
           "Dochodzi ci^e zapach ko^nskich odchod^ow.\n","Wiatr zastuka^l "+
           "we wrota.\n","Twych uszu dochodzi ciche r^zenie.\n"})[random(4)];
   }
}