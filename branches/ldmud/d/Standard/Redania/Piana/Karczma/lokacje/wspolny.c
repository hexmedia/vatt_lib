/* Autor: Faeve
   Opis : Jasmina
   Data : 15.07.07

   sala sypialna karczmy w Pianie */

#include <stdproperties.h>
#include <mudtime.h>
#include <macros.h>
#include "dir.h"

inherit PIANA_STD;
inherit "/lib/peek";

string dlugi_opis();

void
create_piana()
{
    set_short("Sala sypialna.");
    set_long("@@dlugi_opis@@");

    add_prop(ROOM_I_INSIDE, 1);

    add_item("pod^log^e","Brudna i zakurzona - krzywo po^lo^zone deski "+
        "trzeszcz^a i skrzypi^a przy ka^zdym kroku. \n");
    add_item("sznur","Zwyk^ly konopny sznur zwieszaj^acy si^e z jednej z "+
        "^zerdzi w sklepieniu. Powieszono na nim ma^l^a, oliwn^a lampk^e.\n");

    add_object(PIANA_KARCZMA + "przedmioty/stoliczek",6);
    dodaj_rzecz_niewyswietlana("niewielki okr^ag^ly stoliczek",6);
    add_object(PIANA_KARCZMA + "przedmioty/lozko",6);
    dodaj_rzecz_niewyswietlana("^l^o^zko",6);
    add_object(PIANA_KARCZMA + "przedmioty/chodniczek",6);
    dodaj_rzecz_niewyswietlana("niedu^zy brudnozielony chodniczek",6);
    add_object(PIANA_KARCZMA + "przedmioty/komoda");
    dodaj_rzecz_niewyswietlana("komoda");
    add_object(PIANA_KARCZMA + "przedmioty/lampka_wspolny");
    add_object(PIANA_KARCZMA + "przedmioty/zaslony");

    add_door(PIANA_KARCZMA + "lokacje/drzwi/ze_wspolnego");
    add_window(PIANA_KARCZMA + "przedmioty/okno_wspolny");

    set_event_time(80.0);
    add_event("@@evenciki:"+file_name(TO)+"@@");
}

string
dlugi_opis()
{
    string str;

    int i, il, il_stoliczkow;
    object *inwentarz;

    inwentarz = all_inventory();
    il_stoliczkow = 0;
    il = sizeof(inwentarz);

    for (i = 0; i < il; ++i) {
         if (inwentarz[i]->jestem_stoliczkiem_karczmy_piana()) {
                        ++il_stoliczkow; }}

    int p, li, li_chodniczkow;
    object *inwent;

    inwent = all_inventory();
    li_chodniczkow = 0;
    li = sizeof(inwent);

    for (p = 0; p < li; ++p) {
         if (inwent[p]->jestem_chodniczkiem_karczmy_piana()) {
                        ++li_chodniczkow; }}


    str = "Obszerna sala wsp^olna mie^sci w sobie sze^s^c ^l^o^zek "+
        "ustawionych po trzy na przeciwleg^lych ^scianach - i niemal^ze nic "+
        "wi^ecej. ";

    if(il_stoliczkow == 6)
    {
        str += "Przy ka^zdym ^l^o^zku stoi niewielki, okr^ag^ly stoliczek";
        if(li_chodniczkow == 6)
        {
            str += " postawiony na niedu^zym, brudnozielonym chodniczku. ";
        }
        if(li_chodniczkow > 1 && li_chodniczkow < 6)
        {
            str += ", a cz^e^s^c z nich postawiona zosta^la na brudnozielonych "+
                "chodniczkach. ";
        }
        if(li_chodniczkow == 1)
        {
            str += ", a jeden z nich postawiono na brudnozielonym chodniczku. ";
        }
        if(li_chodniczkow == 0)
        {
            str += ". ";
        }
    }

    if(il_stoliczkow > 1 && il_stoliczkow < 6)
    {
        {
            str += "Przy niekt^orych ^l^o^zkach stoj^a niewielkie, okr^ag^le "+
                "stoliczki";
        }
        if(li_chodniczkow == 6)
            {
                str += " postawione na brudnozielonych chodniczkach, przy "+
                    "pozosta^lych za^s - chodniczki le^z^a na pod^lodze, "+
                    "zbieraj^ac na sobie wszechobecny kurz. ";
            }
        if(li_chodniczkow == il_stoliczkow)
            {
                str += " postawione na brudnozielonych chodniczkach. ";
            }
        if(li_chodniczkow < il_stoliczkow)
            {
                str += ", cz^e^s^c z nich postawiono na brudnozielonych "+
                    "chodniczkach. ";
            }
    }

    if(il_stoliczkow == 1)
    {
        {
            str += "Przy jednym z ^l^o^zek stoi niewielki, okr^ag^ly "+
                "stoliczek";
        }
        if(li_chodniczkow > 1)
        {
            str += " postawiony na brudnozielonym chodniczku, przy "+
                    "pozosta^lych za^s ^l^o^zkach - chodniczki le^z^a na "+
                    "pod^lodze, zbieraj^ac na sobie wszechobecny kurz. ";
        }
        if(li_chodniczkow == il_stoliczkow)
        {
            str += " postawiony na brudnozielonym chodniczku. ";
        }
        if(li_chodniczkow == 0)
        {
            str += ". ";
        }
    }

    if(il_stoliczkow == 0)
    {
        if(li_chodniczkow == 6)
        {
            str += "Przy ka^zdym z ^l^o^zek po^lo^zono brudnozielony "+
                "chodniczek. ";
        }
        if(li_chodniczkow > 1 && li_chodniczkow < 6)
        {
            str += "Przy niekt^orych ^l^o^zkach po^lo^zono brudnozielone "+
                "chodniczki. ";
        }
        if(li_chodniczkow == 1)
        {
            str += "Przy jednym z ^l^o^zek po^lo^zono brudnozielony chodniczek. ";
        }
        if(li_chodniczkow == 1)
        {
            str += "";
        }
    }

    str += "Po^ludniowa ^sciana ^l^aczy si^e z uko^snie ^sci^etym dachem, w "+
        "kt^orym widnieje jedno, niezbyt wielkie wykuszowe okno ";

    object zaslony = present("zas^lony");
    if(zaslony->query_prop("odslonieta") == 0)
    {
        str += "zas^loni^ete brudnozielon^a zas^lon^a. ";
    }
    else
    {
        if(jest_dzien())
        {
            str += "wpuszczaj^ace przez zamazan^a szyb^e nieco "+
                "^swiat^la. ";
        }
        else
        {
            str += "ukazuj^ace przez zamazan^a szyb^e ciemno^s^c nocy. ";
        }
    }

    str += "Pod oknem stoi prosta, zbita z desek komoda posiadaj^aca "+
        "trzy szerokie szuflady - jedyny poza ^l^o^zkami wi^ekszy mebel "+
        "w pomieszczeniu. Brudna i zakurzona pod^loga skrzypi przy "+
        "ka^zdym kroku, za^s zamiast sufitu dostrzec mo^zna belki i "+
        "^zerdzie podtrzymuj^ace spadzisty dach. Z jednej z nich zwisa "+
        "konopny sznur, do kt^orego przywi^azano niewielk^a, oliwn^a "+
        "lampk^e roz^swietlaj^ac^a nieznacznie pomieszczenie. ";

    str += "\n";
    return str;
}

public string
exits_description()
{
    return "Na po^ludniowej ^scianie widniej^a krzywo powieszone na "+
        "zawiasach drzwi.\n";
}

string
evenciki()
{
   switch(pora_dnia())
   {
       case MT_SWIT:
       case MT_WCZESNY_RANEK:
       case MT_RANEK:
       case MT_POLUDNIE:
       case MT_POPOLUDNIE:
           return ({"Mysz przemkn^e^la pomi^edzy ^l^o^zkami.\n","Pod^loga "+
           "skrzypi cicho.\n","Twych nozdrzy dochodzi zapach jedzenia z "+
           "do^lu.\n","S^lycha^c jakie^s poruszenie w korytarzu.\n"})
           [random(4)];
       default:
           return ({"Co^s poruszy^lo si^e mi^edzy ^zerdziami.\n","Z jednego "+
           "z rog^ow pomieszczenia dochodzi twych uszu ciche popiskiwanie.\n",
           "Wiatr zastuka^l w okno.\n","Jedno z ^l^o^zek zaskrzypia^lo.\n"})
           [random(4)];
   }
}