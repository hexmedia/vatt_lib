#pragma strict_types

#include <macros.h>
#include "dir.h"
#include <exp.h>
#include <ss_types.h>

inherit "/std/paralyze";

void
create_paralyze()
{
    set_name("wspinasie");
    set_stop_verb("przesta�");
    set_stop_message("Przestajesz wspina^c si^e na drzewo.\n");
    set_fail_message("Jeste� teraz zaj�t"+TP->koncowka("y","a")+
                     " wspinaniem si� na drzewo. Musisz wpisa� "+
                "'przesta�' by m�c zrobi�, to co chcesz.\n");
    set_finish_object(this_object());
    set_finish_fun("koniec_wspinaczki");
    set_remove_time(6);
    setuid();
    seteuid(getuid());
}

void
koniec_wspinaczki(object player)
{
    player->catch_msg("Ko^nczysz wspinaczk^e.\n");
    saybb(QCIMIE(player,PL_MIA)+" ko^nczy wspinaczk^e.\n");
    player->add_old_fatigue(-25);
    player->increase_ss(SS_CLIMB,EXP_WSPINAM_UDANY_CLIMB);
	player->increase_ss(SS_STR,EXP_WSPINAM_UDANY_CLIMB_STR);
    player->increase_ss(SS_DEX,EXP_WSPINAM_UDANY_CLIMB_DEX);
	player->increase_ss(SS_CON,EXP_WSPINAM_UDANY_CLIMB_CON);
    player->move_living("M", PIANA_LOKACJE + "nadrzewie.c");
    saybb(QCIMIE(player,PL_MIA)+" przybywa z do�u.\n");
    remove_object();
    return;
}
