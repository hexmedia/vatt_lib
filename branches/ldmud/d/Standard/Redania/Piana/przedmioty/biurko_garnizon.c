/* Autor: Avard
   Opis : vortak
   Data : 6.08.2007 */

inherit "/std/container";

#include "/sys/stdproperties.h"
#include "/sys/macros.h"
#include "/sys/pl.h"
#include "/sys/materialy.h"
#include "/sys/object_types.h"
#include <mudtime.h>
#include "dir.h"

void
create_container()
{
    ustaw_nazwe("biurko");
    dodaj_przym("du^zy", "duzi");
    dodaj_przym("ciemnobr^azowy", "ciemnobr^azowi");
    ustaw_material(MATERIALY_DR_DAB);
    set_type(O_MEBLE);

    set_long("Do^s^c du^zy drewniany blat wspieraj^acy si^e na czterech "+
        "nogach. Ca^lo^s^c zosta^la dla zwi^ekszenia trwa^lo^sci oraz "+
        "zapewnienia minimum estetyki pomalowana grub^a warstw^a "+
        "ciemnobr^azowego lakieru. Pod blatem znajduj^a si^e prowadnice "+
        "umo^zliwiaj^ace umieszczenie tam szuflady, jednak po samej "+
        "szufladzie nigdzie nie wida^c ^sladu. "+
        "@@opis_sublokacji|Dostrzegasz na nim |na|.\n|\n|" + PL_BIE+ "@@");
         
    add_prop(CONT_I_WEIGHT, 10000);
    add_prop(CONT_I_VOLUME, 10000);
    add_prop(OBJ_I_VALUE, 456);
    
    add_subloc("na");
    add_subloc_prop("na", CONT_I_MAX_WEIGHT, 100000);
    add_subloc_prop("na", CONT_I_MAX_VOLUME, 150000);
    add_prop(CONT_I_DONT_SHOW_CONTENTS, 1);
    add_prop(CONT_I_CANT_WLOZ_DO, 1);

    //set_owners(({PIANA_NPC + "garnizonowy"}));
}