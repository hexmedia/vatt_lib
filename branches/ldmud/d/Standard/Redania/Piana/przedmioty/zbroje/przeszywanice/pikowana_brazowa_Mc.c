/* Autor: Valor
   Opis:  Faeve
   Data:  12.06.07
*/

inherit "/std/armour";

#include <formulas.h>
#include <macros.h>
#include <materialy.h>
#include <object_types.h>
#include <pl.h>
#include <stdproperties.h>
#include <wa_types.h>

void create_armour()
{               
    ustaw_nazwe("przeszywanica");
    dodaj_przym("pikowany","pikowani"); 
    dodaj_przym("br^azowy","br^azowi");

    set_long("Kaftan ten, uszyty z wielu warstw pikowanego br^azowego "+
        "jutowego p^l^otna, si^ega tu^z poni^zej po^sladk^ow osoby go "+
        "nosz^acej. R^ekawy ma d^lugie i do^s^c sztywne, jednak w miar^e "+
        "szerokie, co umo^zliwia swobodniejsze poruszanie r^ekami. Ma "+
        "do^s^c wysoki ko^lnierz si^egaj^acy tu^z pod ko^sci policzkowe "+
        "oraz podbr^odek, r^ownie^z niezbyt gibki. Przeszywanica "+
        "zaopatrzona jest w co^s na kszta^lt guzik^ow - z jednej strony "+
        "przyszyto kawa^lki sk^ory, przez kt^ore przewleka si^e rzemyki "+
        "zaczepione z drugiej. Dodatkowo w talii przewi^azana jest "+
        "szerokim i mocnym sk^orzanym pasem. \n");
             
    set_slots(A_BODY, A_ARMS, A_FOREARMS, A_STOMACH);
    add_prop(OBJ_I_WEIGHT, 4000);
    add_prop(OBJ_I_VOLUME, 4000);
    add_prop(OBJ_I_VALUE, 1200);
    add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    set_type(O_UBRANIA);
    ustaw_material(MATERIALY_JUTA, 100);
    set_size("M");
    set_ac(A_TORSO,3,3,8,A_ARMS,3,3,8,A_STOMACH,3,3,8,A_FOREARMS,3,3,8);
}