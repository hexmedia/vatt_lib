/**
 * 06.08.2007
 *  
 * Autor: Daarth
 * Opis : Samaia
 */
inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <pl.h>
#include <macros.h>
#include <materialy.h>
#include <object_types.h>

void
create_armour()
{  
	ustaw_nazwe("sukienka");

	dodaj_przym("czerwony","czerwoni");
       dodaj_przym("falbaniasty", "falbania^sci");
    
	set_long(
        "�liczna, czerwona sukieneczka pe^lna kokardek i koronek zosta^la "+
        "r^ownie^z przyozdobiona poka�nych rozmiarow plamami, a tak^ze "+
        "licznymi przetarciami. D^lugie r^ekawy zako^nczone zosta^ly "+
        "wstawkami ze z^lotego, b^lyszcz�cego materia^lu, ^zywo "+
        "odcinaj�cego si^e kolorystycznie od reszty. W pasie zosta^la "+
        "przewi�zana szerok� ta^sm^a, kt^ora ginie jednak pod stert� "+
        "falbanek.\n"
	);

    ustaw_material(MATERIALY_JEDWAB, 85);
    ustaw_material(MATERIALY_SATYNA , 5);
	ustaw_material(MATERIALY_BAWELNA, 10);
 
   set_slots(A_TORSO, A_ARMS, A_LEGS, A_BODY);
   add_prop(OBJ_I_WEIGHT, 600);
   add_prop(OBJ_I_VOLUME, 600);
   add_prop(OBJ_I_VALUE, 64);
   add_prop(ARMOUR_I_D_ROZCIAGLIWOSC, 40);
   add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
   add_prop(ARMOUR_I_DLA_PLCI, 1);

   set_size("S");
   set_type(O_UBRANIA);
}
  