inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <macros.h>
#include <materialy.h>
#include <object_types.h>
#include <pl.h>
void
create_armour()
{
	ustaw_nazwe("spodnie");
	dodaj_przym("skromny","skromni");
	dodaj_przym("czarny","czarni");

	set_long("Wcale atrakcyjne, czarne spodnie, z br^azowymi wstawkami "
		+"przy nogawkach i na po^sladkach, wygl^adaj^a raczej na mocne "
		+"i wytrzyma^le. Lekko zw^e^zaj^a si^e u do^lu, na tyle, by m^oc "
		+"w^lo^zy^c je w buty, dodatkowo ^sci^agane s^a solidnymi "
		+"rzemieniami. Ich uniwersalny kolor i swobodny kr^oj "
		+"sprawiaj^a, i^z te spodnie nosi^c mo^zna niemal z ka^zd^a "
		+"koszul^a, czy kurtk^a. \n");

	set_slots(A_LEGS, A_HIPS);
	add_prop(OBJ_I_WEIGHT, 1200);
	add_prop(OBJ_I_VOLUME, 1000);
	add_prop(OBJ_I_VALUE, 28);

	//A czemu inne rasy nie zaloza? (krun)
	add_prop(ARMOUR_S_DLA_RASY, "cz^lowiek");
	add_prop(ARMOUR_I_DLA_PLCI, 1);

	ustaw_material(MATERIALY_BAWELNA, 80);
	ustaw_material(MATERIALY_SK_SWINIA, 20);

	set_type(O_UBRANIA);

	set_size("S");

}