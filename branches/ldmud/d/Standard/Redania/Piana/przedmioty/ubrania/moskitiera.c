
/* Jest to moskitiera, kt�ra zas�ania twarz przed uk�szeniami
    komar�w bagiennych spod Piany.
            Vera.*/

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <macros.h>
#include <materialy.h>
#include <object_types.h>
#include <pl.h>

void
create_armour()
{
    ustaw_nazwe("moskitiera");
    dodaj_nazwy("kapelusz");

    dodaj_przym("siatkowa","siatkowe");
    dodaj_przym("szara","szare");

    set_long("Po przyjrzeniu si� stwierdzasz, �e jest to nic "+
            "innego, jak tylko skromnie wykonany s�omkowy "+
            "kapelusz o nadzwyczajnie szerokim rondzie, do kt�rego "+
            "wok� przytwierdzono siatkowy materia� maj�cy za zadanie "+
            "ochroni� twarz przed uk�szeniami namolnych owad�w.\n");

    set_slots(A_HEAD);
    add_prop(OBJ_I_WEIGHT, 400);
    add_prop(OBJ_I_VOLUME, 1000);
    add_prop(OBJ_I_VALUE, 35);
    set_type(O_UBRANIA);
    ustaw_material(MATERIALY_BAWELNA, 30);
    ustaw_material(MATERIALY_SLOMA, 70);
    add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    set_size("L");
}

string
query_auto_load()
{
   return ::query_auto_load();
}

public int
query_moskitiera()
{
    return 1;
}

