inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <macros.h>
#include <materialy.h>
#include <object_types.h>
#include <pl.h>

void
create_armour()
{
    ustaw_nazwe("koszula");

    dodaj_przym("d^lugi","d^ludzy");
    dodaj_przym("b^l^ekitny","b^l^ekitni");

    set_long("Koszula z mi^ekkiej bawe^lny w kolorze letniego, "
		+"popo^ludniowego nieba zosta^la uszyta tak, by si^ega^c "
		+"osobie j^a nosz^acej niemal do kolan. Ubranie mieni si^e "
		+"lekko w srebrzystych kolorach, dzi^eki niezliczonej "
		+"liczbie subtelnych haft^ow na ca^lej d^lugo^sci smuk^lych "
		+"r^ekaw^ow. Delikatny kr^oj, wci^ecie w talii, oraz konkretnie "
		+"zarysowany dekolt wskazuj^a na to, i^z koszula uszyta "
		+"zosta^la zdecydowanie dla p^lci ^ze^nskiej. \n");

    set_slots(A_TORSO, A_FOREARMS, A_ARMS, A_HIPS);

    add_prop(OBJ_I_WEIGHT, 500);
    add_prop(OBJ_I_VOLUME, 700);
    add_prop(OBJ_I_VALUE, 70);

    set_type(O_UBRANIA);

    ustaw_material(MATERIALY_BAWELNA, 100);

    add_prop(ARMOUR_S_DLA_RASY, "cz�owiek");
    add_prop(ARMOUR_I_DLA_PLCI, 1);

    set_size("M");
}