/* TODO: Zrobic to bardziej ubraniowo ;) */
inherit "/std/object";

#include <cmdparse.h>
#include <macros.h>
#include <materialy.h>
#include <pl.h>
#include <object_types.h>
#include <stdproperties.h>
#include <wa_types.h>

int zawiazana;        // Czy chusta jest zawiazana

string *gdzie_mozna = ({"na lewym ramieniu", "na prawym ramieniu", 
    "na ramieniu", "na g^lowie", "na lewym nadgarstku", 
    "na prawym nadgarstku", "na nadgarstku", "pod szyj�"});

int gdzie_index=0;    // Gdzie jest zawiazana

void create_object()
{
    ustaw_nazwe("chusta");
    
    dodaj_przym("karminowy", "karminowi");
    dodaj_przym("haftowany", "haftowani");
    

    set_long("Karminowa chusta, haftowana w fantazyjne wzory - kwiaty, "+
        "spirale, czy fale - za pomoc^a wielobarwnych nici, dzi^eki swej "+
        "kolorystyczej r^o^znorodno^sci ju^z z daleka rzuca si^e w oczy. "+
        "W ka^zdym rogu zosta^la dodatkowo wyko^nczona ^z^o^ltymi "+
        "fr^edzlami, zdecydowanie wi^ec chusta nie s^lu^zy do wycierania "+
        "nosa. Jest za to na tyle du^za, by zawi^aza^c j^a na g^lowie, "+
        "tudzie^z ramieniu, lub ewentualnie owin^a^c ni^a nadgarstek. \n");
    
    ustaw_material(MATERIALY_JEDWAB, 100);
    set_type(O_UBRANIA);
    
    add_prop(OBJ_I_WEIGHT, 6);
    add_prop(OBJ_I_VOLUME, 8);
    add_prop(OBJ_I_VALUE,  22);
}

init()
{
    ::init();
    add_action("zawiaz", "zawi�^z");
    add_action("odwiaz", "odwi�^z");
    add_action("wysmarkaj", "wysmarkaj");
    add_action("wytrzyj", "wytrzyj");
    add_action("pomoc", "?", 2);
    
}

string show_subloc(string subloc, object on_obj, object for_obj)
{
    if (wildmatch("rinde_chusta_subloc*", subloc))
    {
        if(zawiazana)
        {
            if (for_obj == on_obj)
                return capitalize(gdzie_mozna[gdzie_index])+" masz zawi�zan� "
                         +short(PL_BIE)+".\n";
 
            return capitalize(gdzie_mozna[gdzie_index])+ " ma zawi�zan� "
                    +short(PL_BIE)+".\n";
        }
    }
}

int zawiaz(string str)
{
    if(!str)
    {
    notify_fail("Co i gdzie chcesz zawi�za^c?\n");
    return 0;
    }
    
    object *co;
    string gdzie;
    if(!parse_command(lower_case(str), TP, " %i:"+PL_BIE+" %s ", co, gdzie))
    {
        notify_fail("Co i gdzie chcesz zawi�za^c?\n");
        return 0;
    }
    
    co = CMDPARSE_STD->normal_access(co, 0, 0);
    
    if(!co || (sizeof(co) == 0))
    {
    notify_fail("Co i gdzie chcesz zawi�za^c?\n");
    return 0;
    }
    
    if(sizeof(co) > 1)
    {
    notify_fail("Mo^zesz za^lo^zy^c tylko jedn� rzecz na raz!\n");
    return 0;
    }
    
    if(co[0]!=this_object())
    {
    return 0;
    }
    
    if(zawiazana)
    {
    notify_fail("Przecie^z chusta jest ju^z zawi�zana!\n");
    return 0;
    }
    
    
    if((gdzie_index = member_array(gdzie, gdzie_mozna)) == -1)
    {
    notify_fail("Gdzie chcesz zawi�za^c "+co[0]->short(PL_BIE)+"?\n");
    return 0;
    }
    
    /* Jesli na ramie - zmieniamy na prawe */
    if(gdzie_index == 2)
        gdzie_index = 1;

    /* Jesli na nadgarstek to na prawy */
    if(gdzie_index == 6)
        gdzie_index = 5;

    if(TP->query_subloc_obj("rinde_chusta_subloc_"+gdzie_mozna[gdzie_index]) != 0)
    {
        notify_fail("W tym miejscu masz ju^z zawi�zan� jedn� chust^e!\n");
        return 0;
    }
    
    /* Zawiazujemy na ramieniu */
    if(gdzie_index < 2)
    {
    TP->catch_msg("Sk^ladasz chust^e na p^o^l, po czym starannie zawi�zujesz j�"
                +" sobie "+gdzie_mozna[gdzie_index]+".\n");
        
    saybb(QCIMIE(TP, PL_MIA)+" sk^lada "+short(TP, PL_BIE)+" na p^o^l, po czym"
        +" starannie zawi�zuje j� sobie "+gdzie_mozna[gdzie_index]+".\n");
    }
    
    /* Zawiazujemy na glowie */
    if(gdzie_index == 3)
    {
    TP->catch_msg("Zwijasz "+short(TP, PL_BIE)+" w niezbyt szeroki pas,"
                +" po czym wi�^zesz j� sobie na g^lowie i szybkim ruchem"
                +" wyg^ladzasz wszystkie nier^owno�ci.\n");
    
    saybb(QCIMIE(TP, PL_MIA)+" zwija "+short(TP, PL_BIE)+" w niezbyt szeroki"
        +" pas, po czym zawi�zuje j� sobie na g^lowie i szybkim ruchem wyg^ladza"
        +" wszystkie jej nier^owno�ci.\n");
    }
    
    /* Zawiazujemy na nadgarstku */
    if(gdzie_index == 4 || gdzie_index == 5)
    {
        TP->catch_msg("Trzykrotnie sk^ladasz chust^e otrzymuj�c zgrabny pasek,"
                    +" kt^ory wi�^zesz �ci�le "+gdzie_mozna[gdzie_index]+".\n");

        saybb(QCIMIE(TP, PL_MIA)+" trzykrotnie sk^lada "+short(TP, PL_BIE)
            +" otrzymuj�c zgrabny pasek, kt^ory wi�^ze sobie "
            +gdzie_mozna[gdzie_index]+".\n");
    }
    
    /* Zawiazujemy na szyji */
    if(gdzie_index == 7)
    {
        TP->catch_msg("Uk^ladasz sobie chust^e pod szyj� i wi�^zesz z ty^lu mocny"
                    +" w^eze^l.\n");
        
        saybb(QCIMIE(TP, PL_MIA)+" uk^lada sobie chust^e pod szyj� i wi�^ze z ty^lu"
                    +" mocny w^eze^l.\n");
    }
    
    TP->add_subloc("rinde_chusta_subloc_"+gdzie_mozna[gdzie_index], this_object());
    zawiazana = 1;

    add_prop(OBJ_I_DONT_INV, 1);
    add_prop(OBJ_M_NO_GIVE, "Najpierw j� odwi�^z!\n");
    add_prop(OBJ_M_NO_DROP, "Najpierw j� odwi�^z!\n");
    
    return 1;
}

int odwiaz(string str)
{
    notify_fail("Co chcesz odwi�za^c?\n");
    
    if(!str)
    return 0;
    
    object *co;
    if(!parse_command(lower_case(str), environment(), " %i:"+PL_BIE, co))
    return 0;
    
    co = NORMAL_ACCESS(co, 0, 0);
    
    if(!co)
    return 0;
    
    if(sizeof(co) > 1)
    return 0;

    
    if(co[0]!=this_object())
    return 0;
    
    if(!zawiazana)
    {
    notify_fail("Ale^z chusta nie jest nawet zawi�zana!\n");
    return 0;
    }
    
    TP->catch_msg("Kilkoma sprawnymi ruchami odwi�zujesz za^lo^zon� "+gdzie_mozna[gdzie_index]
        +" chust^e.\n");
    saybb(QCIMIE(TP, PL_MIA)+" kilkoma sprawnymi ruchami odwi�zuje za^lo^zon� "
    +gdzie_mozna[gdzie_index]+" "+short(PL_BIE)+".\n");
    TP->remove_subloc("rinde_chusta_subloc_"+gdzie_mozna[gdzie_index]);
    zawiazana = 0;

    remove_prop(OBJ_M_NO_GIVE);
    remove_prop(OBJ_M_NO_DROP);
    remove_prop(OBJ_I_DONT_INV);
    
    return 1;
}

int
wysmarkaj(string str)
{
    notify_fail("W co chcesz si^e wysmarka^c?\n");

    if(!str)
        return 0;

    object *chusta;
    if(!parse_command(str, TP, " 'si^e' 'w' %i:"+PL_BIE, chusta))
        return 0;

    chusta = NORMAL_ACCESS(chusta, 0, 0);

    if(!sizeof(chusta))
        return 0;

    if(sizeof(chusta) > 1)
    {
        notify_fail("Jedna chusta powinna ci w zupe^lno�ci do tego"
                +" wystarczy^c.\n");
        return 0;
    }

    if(chusta[0] != TO)
        return 0;

    if(zawiazana)
    {
        notify_fail("Najpierw j� odwi�^z!\n");
        return 0;
    }

    write("Przyk^ladasz "+short(PL_BIE)+" do nosa i smarkasz w ni�"
         +" kilkakrotnie.\n");

    saybb(QCIMIE(TP, PL_MIA)+" przyk^lada "+short(PL_BIE)+" do nosa i"
            +" smarka w ni� kilkakrotnie.\n");

    return 1;
}

int
wytrzyj(string str)
{
    notify_fail("W co chcesz wytrze^c r^ece?\n");

    if(!str)
        return 0;

    object *chusta;
    if(!parse_command(str, TP, " 'r^ece' 'w' %i:"+PL_BIE, chusta))
        return 0;

    chusta = NORMAL_ACCESS(chusta, 0, 0);

    if(!sizeof(chusta))
        return 0;

    if(sizeof(chusta) > 1)
    {
        notify_fail("Jedna chusta powinna ci w zupe^lno�ci do tego"
                +" wystarczy^c.\n");
        return 0;
    }

    if(chusta[0] != TO)
        return 0;

    if(zawiazana)
    {
        notify_fail("Najpierw j� odwi�^z!\n");
        return 0;
    }

    write("Dok^ladnie, maj�c na uwadze ka^zdy palec, wycierasz d^lonie w "
            +short(PL_BIE)+".\n");

    saybb(QCIMIE(TP, PL_MIA)+" dok^ladnie, maj�c na uwadze ka^zdy palec,"
            +" wyciera d^lonie w "+short(PL_BIE)+".\n");

    return 1;
}

int pomoc(string str)
{                                                                              
    object pohf;

    notify_fail("Nie ma pomocy na ten temat.\n");

    if (!str) 
    return 0;

    if (!parse_command(str, this_player(), "%o:" + PL_MIA, pohf))
        return 0;

    if (pohf != this_object())
        return 0;

    write("*****************************************************\n");
    write("*                                                   *\n");
    write("*  Chust^e mo^zesz zawi�za^c:                          *\n");
    write("*                                                   *\n");
    write("*    - na kt^orym� z ramion                          *\n");
    write("*    - na g^lowie                                    *\n");
    write("*    - pod szyj�                                    *\n");
    write("*    - na kt^orym� z nadgarstk^ow                     *\n");
    write("*                                                   *\n");
    write("*  Mo^zesz tak^ze wytrze^c w ni� r^ec^e lub wysmarka^c    *\n");
    write("*  si^e z jej pomoc�.                                *\n");
    write("*                                                   *\n");
    write("*****************************************************\n");
    
    return 1;
}

