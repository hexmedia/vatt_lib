inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <macros.h>
#include <materialy.h>
#include <object_types.h>

string

create_armour()
{
    ustaw_nazwe_glowna("para");
    ustaw_nazwe("buty");
    dodaj_przym("zwyk^ly","zwykli");
    dodaj_przym("br^azowy","br^azowi");

    set_long("Pospolitego kroju buty, przypominaj^ace dwa po^l^aczone "
		+"ze sob^a skrawki sk^ory, z doszytymi do^n od spodu cienkimi "
		+"podeszwami, z pewno^sci^a nie nale^z^a do najwytrzymalszych. "
		+"Materia^l jest zdecydowanie niskiej jako^sci i bez uprzedniego "
		+"za^lo^zenia onuc, nie ma mo^zliwo^sci, by obuwie nie obciera^lo "
		+"st^op nosz^acej ich osoby. Si^egaj^a zaledwie kostek i trzymaj^a "
		+"si^e dzi^eki niewielkiej klamerce w kszta^lcie podkowy, kt^ora "
		+"wygl^ada jakby w ka^zdej chwili mog^la odpa^s^c, dzi^eki ma^lo "
		+"rzetelnemu przytwierdzeniu ich do buta. \n");

    set_slots(A_FEET);
    add_prop(OBJ_I_WEIGHT, 300);
    add_prop(OBJ_I_VOLUME, 900);
    add_prop(OBJ_I_VALUE, 36);

    add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
    add_prop(ARMOUR_I_DLA_PLCI, 0);

    set_size("L");

    ustaw_material(MATERIALY_SK_SWINIA, 95);
	ustaw_material(MATERIALY_MOSIADZ, 5);

    set_type(O_UBRANIA);
    set_likely_cond(23);
}