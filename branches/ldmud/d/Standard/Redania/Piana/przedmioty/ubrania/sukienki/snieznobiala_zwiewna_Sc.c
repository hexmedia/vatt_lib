/* Autor: Avard
   Opis : Samaia
   Data : 12.09.07 */
inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <pl.h>
#include <macros.h>
#include <materialy.h>
#include <object_types.h>

void
create_armour()
{  
	ustaw_nazwe("sukienka");

	dodaj_przym("^snie^znobia^ly","^snie^znobiali");
    dodaj_przym("zwiewny", "zwiewni");
    
	set_long("Ta lekka niczym mg^la sukienka zosta^la wykonana z niezwykle "+
        "lekkiego, delikatnego i jednocze^snie nieco przezroczystego "+
        "materia^lu. Jej szerokie, zwiewne r^ekawy, przypominaj^ace nieco "+
        "skrzyd^la motyla, a tak^ze d^o^l sukienki wyko^nczone s^a "+
        "subteln^a koronk^a obrazuj^ac^a szronowe malunki kwiat^ow, gwiazd, "+
        "ga^l^azek i innych przedziwnych bli^zej nieokre^slonych "+
        "kszta^lt^ow. Ca^lo^s^c przepasana jest szerok^a, ^snie^znobia^l^a "+
        "wst^eg^a, zwi^azan^a z ty^lu w kokard^e.");

    ustaw_material(MATERIALY_JEDWAB, 95);
    ustaw_material(MATERIALY_SATYNA , 5);
 
   set_slots(A_TORSO, A_ARMS, A_LEGS, A_BODY);
   add_prop(OBJ_I_WEIGHT, 600);
   add_prop(OBJ_I_VOLUME, 600);
   add_prop(OBJ_I_VALUE, 94);
   add_prop(ARMOUR_I_D_ROZCIAGLIWOSC, 40);
   add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
   add_prop(ARMOUR_I_DLA_PLCI, 1);

   set_size("S");
   set_type(O_UBRANIA);
   set_likely_cond(18);
}
  