inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <macros.h>
#include <materialy.h>
#include <object_types.h>

string

create_armour()
{  
    ustaw_nazwe_glowna("para");
    ustaw_nazwe("buty");
    dodaj_przym("czarny","czarni");
    dodaj_przym("zamszowy","zamszowi");


   set_long("Buty o twardej, lecz cienkiej podeszwie stworzono z my^sl^a "	
	   +"o chodzeniu w nich po ubitym terenie, najlepiej po mie^scie. Cholewka "
	   +"dok^ladnie obejmuje kostk^e utrudniaj^ac w pewnym stopniu obtarcie "
	   +"nogi. Niestety nie da si^e w nich chodzi^c po mokrej nawierzchni, "
	   +"poniewa^z zamsz szybko przemaka.\n");


    set_slots(A_FEET | A_SHINS);
    add_prop(OBJ_I_WEIGHT, 300);
    add_prop(OBJ_I_VOLUME, 1000);
    add_prop(OBJ_I_VALUE, 61);
    

    add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
    add_prop(ARMOUR_I_DLA_PLCI, 0);

    set_size("XL");
   
    ustaw_material(MATERIALY_SK_SWINIA, 100);
   
    set_type(O_UBRANIA);
    set_likely_cond(18);
}