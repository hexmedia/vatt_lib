inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <macros.h>
#include <materialy.h>
#include <object_types.h>
#include <pl.h>
void
create_armour()
{
	ustaw_nazwe("spodnie");
	dodaj_przym("szorstki","szorstcy");
	dodaj_przym("we^lniany","we^lniani");

	set_long("Grube, we^lniane nogawki tej cz^e^sci odzienia "+
			"mo^ze i czyni^a nierozpoznawalnymi tak wdzi^eki "+
			"jak i skazy figury oraz dra^zni^a sk^or^e szorstkimi "+
			"k^l^ebkami, lecz za to odwdzi^eczaj^a si^e zapewnieniem "+
			"ciep^la w najgorsze mrozy. Sama we^lna ufarbowana "+
			"zosta^la na jasnobr^azowy kolor. W^sr^od grubych "+
			"^scieg^ow dostrzec mo^zna dwie kieszenie kt^orych "+
			"wn^etrze okazuje si^e uszyte z szrstkiego lnu. \n");

	set_slots(A_LEGS, A_HIPS);
	add_prop(OBJ_I_WEIGHT, 900);
	add_prop(OBJ_I_VOLUME, 9000);
	add_prop(OBJ_I_VALUE, 25);

	add_prop(ARMOUR_S_DLA_RASY, "cz^lowiek");
	add_prop(ARMOUR_I_DLA_PLCI, 0);

	ustaw_material(MATERIALY_WELNA, 90);
	ustaw_material(MATERIALY_LEN, 5);

	set_type(O_UBRANIA);

	set_size("M");
    set_likely_cond(19);
}