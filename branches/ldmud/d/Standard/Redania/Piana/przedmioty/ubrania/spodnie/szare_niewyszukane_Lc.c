inherit "/std/armour";
        
#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <macros.h>
#include <materialy.h>
#include <object_types.h>
#include <pl.h>
void
create_armour()
{
	ustaw_nazwe("spodnie");
	dodaj_przym("szary","szarzy");
	dodaj_przym("niewyszukany","niewyszukani");
	
	set_long("Te sztampowe spodnie z szarego lnu zdaj^a si^e by^c "
		+"zwyk^lymi do granic mo^zliwo^sci. Nie zosta^ly wcale wyko^nczone, "
		+"co przejawia si^e w postrz^epionych kraw^edziach gubi^acych nitki, "
		+"za^s szwy zosta^ly wykonane niedbale i wygl^adaj^a na takie, "
		+"kt^ore w ka^zdej chwili mog^lyby p^ekn^a^c. Spodnie zdecydowanie "
		+"nie nale^z^a do najwygodniejszych, nie wspominaj^ac ju^z o "
		+"walorach estetycznych. \n");

	set_slots(A_LEGS, A_HIPS);
	add_prop(OBJ_I_WEIGHT, 900);
	add_prop(OBJ_I_VOLUME, 1100);
	add_prop(OBJ_I_VALUE, 18);

	add_prop(ARMOUR_S_DLA_RASY, "cz^lowiek");
	add_prop(ARMOUR_I_DLA_PLCI, 0);

	ustaw_material(MATERIALY_LEN, 100);

	set_type(O_UBRANIA);

	set_size("L");
    set_likely_cond(21);

}