/* Autor: Valor
   Opis : Zwierzaczek
   Data : 12.06.07 */

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <pl.h>
#include <macros.h>
#include <materialy.h>
#include <object_types.h>
void
create_armour()
{  
    ustaw_nazwe_glowna("para");
    ustaw_nazwe("buty");
    dodaj_przym("czarny","czarni");
    dodaj_przym("^zo^lnierski","^zo^lnierscy");

    set_long("Czarne obuwie si^egaj^ace prawie do kolana i dopasowane "+
        "idealnie do ^lydki. Typowo ^zo^lnierskie buty, kt^ore sprawdzaj^a "+
        "si^e zar^owno w pokazowych defiladach jak i w walce w terenie. "+
        "Perfekcyjnie wypastowane przez w^la^sciciela s^a wr^ecz jego "+
        "wizyt^owk^a.\n");
        
    set_slots(A_FEET);
    add_prop(OBJ_I_VOLUME, 1500);
    add_prop(OBJ_I_WEIGHT, 1500);
    add_prop(OBJ_I_VALUE, 60);
    add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    set_size("M");
    ustaw_material(MATERIALY_SK_CIELE, 100);
    set_type(O_UBRANIA);
}