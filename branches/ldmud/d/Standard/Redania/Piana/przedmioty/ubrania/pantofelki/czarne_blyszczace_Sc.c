/**
 * 06.08.2007
 *
 * Autor: Daarth
 * Opis : Samaia
 */

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <pl.h>
#include <macros.h>
#include <materialy.h>
#include <object_types.h>
#include <wa_types.h>
void
create_armour()
{
	ustaw_nazwe("pantofelki");
	set_long(
	 	"Buty, wykonane niew^atpliwie z dobrej jako^sci sk^ory, prze^zy^ly "+
        "ju^z swoje lata ^swietno^sci, mimo to wci^a^z nadaj^a si^e do "+
        "u^zytku. W gruncie rzeczy czas jedynie zmieni^l wygl^ad obuwia "+
        "rozja^sniaj^ac nieco jego czer^n, gdy^z jako^sciowo wci^a^z "+
        "wygladaj^a na mocne solidne. S^a spinane niewielk^a, acz ozdobn^a "+
        "klamerk^a na wysoko^sci kostki, w kszta^lcie kwiatu chryzantemy.\n"
	);
	dodaj_przym("czarny","czarni");
	dodaj_przym("b^lyszcz^acy","b^lyszcz^acy");
	ustaw_material(MATERIALY_SK_SWINIA, 100);


  set_slots(A_R_FOOT, A_L_FOOT);
  add_prop(OBJ_I_WEIGHT, 150);
  add_prop(OBJ_I_VOLUME, 150);
  add_prop(OBJ_I_VALUE, 72);
  add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
  add_prop(ARMOUR_I_DLA_PLCI, 1);

  set_size("S");
  set_type(O_UBRANIA);
  set_likely_cond(21);
}

