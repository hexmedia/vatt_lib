inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <macros.h>
#include <materialy.h>
#include <object_types.h>
#include <pl.h>

void
create_armour()
{
    ustaw_nazwe("koszula");

    dodaj_przym("jasny","ja^sni");
    dodaj_przym("sznurowany","sznurowani");

    set_long("Lekkie odzienie sznurowane grubym trokiem na piersi "+
        "przyjemnie przylega do sk^ory, delikatnie ^laskocz^ac mi^ekkimi "+
        "k^l^ebkami we^lny. Drobne oczka r^owno ukladaj^a si^e na d^lugich, "+
        "szerokich r^ekawach za^s lu^xne po^ly si^egaj^a niemal dwie "+
        "d^lonie poni^zej pasa. Nie nosz^a one wi^ekszych ^slad^ow "+
        "zu^zycia, czy nawet zabrudze^n. Jasny, jakby nieco ^z^o^ltawy, "+
        "naturalny kolor we^lny nie przes^loni^ety jest nawet ^sladem "+
        "farby. \n");

    set_slots(A_TORSO, A_FOREARMS, A_ARMS);

    add_prop(OBJ_I_WEIGHT, 420);
    add_prop(OBJ_I_VOLUME, 600);
    add_prop(OBJ_I_VALUE, 15);

    set_type(O_UBRANIA);

    ustaw_material(MATERIALY_WELNA, 100);

    add_prop(ARMOUR_S_DLA_RASY, "p�elf");
    add_prop(ARMOUR_I_DLA_PLCI, 0);

    set_size("M");
    set_likely_cond(21);
}