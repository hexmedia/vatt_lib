/* Autor: Avard
   Opis : Samaia
   Data : 3.09.2007 */

inherit "/std/container";

#include "/sys/stdproperties.h"
#include "/sys/macros.h"
#include "/sys/pl.h"
#include "/sys/materialy.h"
#include "/sys/object_types.h"
#include <mudtime.h>
#include "dir.h"

void
create_container()
{
    ustaw_nazwe("biurko");
    dodaj_przym("jasny", "ja^sni");
    dodaj_przym("jesionowy", "jesionowi");
    ustaw_material(MATERIALY_DR_JESION);
    set_type(O_MEBLE);

    set_long("Mebel wykonany z jasnego jesionu z pewno^sci^a liczy sobie "+
        "wiele wiosen, jednak wci^a^z dzielnie trzyma si^e na czterech "+
        "^ladnie oheblowanych nogach. Biurko posiada jedn^a ca^lkiem "+
        "spor^a szafk^e, a tak^ze dwie pojemne szuflady zamykane na klucz. "+
        "@@opis_sublokacji|Dostrzegasz na nim |na|.\n|\n|" + PL_BIE+ "@@");
         
    add_prop(CONT_I_WEIGHT, 10000);
    add_prop(CONT_I_VOLUME, 10000);
    add_prop(OBJ_I_VALUE, 456);
    
    add_subloc("na");
    add_subloc_prop("na", CONT_I_MAX_WEIGHT, 100000);
    add_subloc_prop("na", CONT_I_MAX_VOLUME, 150000);
    add_prop(CONT_I_DONT_SHOW_CONTENTS, 1);
    add_prop(CONT_I_CANT_WLOZ_DO, 1);


      add_subloc(({"szafka", "szafki", "szafce","szafk^e", "szafk^a","szafce"}));
  add_subloc_prop("szafka", CONT_I_MAX_VOLUME, 10000);
  add_subloc_prop("szafka", CONT_I_MAX_WEIGHT, 20000);
  add_subloc_prop("szafka", SUBLOC_S_OB_GDZIE, "wewn^atrz szafki");
  add_subloc_prop("szafka", SUBLOC_I_OB_PRZYP, PL_DOP);
  add_subloc_prop("szafka", CONT_M_OPENCLOSE, "szafk^e");
  add_subloc_prop("szafka", SUBLOC_I_RODZAJ, PL_ZENSKI);

  add_item("szafk^e","Ot, zwyk^la szafka, jakie maj^a zwyk^le biurka. "+
	 "@@opis_sublokacji|W jej wn^etrzu dostrzegasz |szafka|.\n|\n|" + PL_BIE+ "@@");

    //set_owners(({PIANA_NPC + "burmistrz"}));
}