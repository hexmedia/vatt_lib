/* Autor: Avard
   Opis : Samaia
   Data : 6.08.2007 */

inherit "/std/container";

#include "/sys/stdproperties.h"
#include "/sys/macros.h"
#include "/sys/pl.h"
#include "/sys/materialy.h"
#include "/sys/object_types.h"
#include <mudtime.h>
#include "dir.h"

void
create_container()
{
    ustaw_nazwe("lada");
    dodaj_przym("prosty", "pro^sci");
    dodaj_przym("solidny", "solidni");
    ustaw_material(MATERIALY_DR_DAB);
    set_type(O_MEBLE);

    set_long("Idealnym rozwi^azaniem dla sklepikarza, kt^ory nie jest w "+
        "stanie pomie^sci^c ca^lego swojego towaru, jest w^la^snie taka "+
        "lada - pe^lni^aca dodatkow^a funkcj^e szafki, a tak^ze wyposa^zona "+
        "liczne szufladki i schowki. Stoj^ace na niej liczne koszyczki, "+
        "pude^lka i pojemniczki prezentuj^a co drobniejsze zasoby sklepu. "+
        "@@opis_sublokacji|Dostrzegasz na nim |na|.\n|\n|" + PL_BIE+ "@@");
         
    add_prop(CONT_I_WEIGHT, 10000);
    add_prop(CONT_I_VOLUME, 10000);
    add_prop(OBJ_I_VALUE, 456);
    
    add_subloc("na");
    add_subloc_prop("na", CONT_I_MAX_WEIGHT, 100000);
    add_subloc_prop("na", CONT_I_MAX_VOLUME, 150000);
    add_prop(CONT_I_DONT_SHOW_CONTENTS, 1);
    add_prop(CONT_I_CANT_WLOZ_DO, 1);

    add_prop(OBJ_M_NO_GET,"Lada jest przytwierdzona do ziemi.\n");

    //set_owners(({BIALY_MOST_NPC + "karczmarz_bialy_most"}));
}