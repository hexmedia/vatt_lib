/*
 * faeve
 * 19.12.2007
 */

#include <pl.h>
#include <macros.h>
#include "dir.h"

inherit "/std/window";

void
create_window()
{
    ustaw_nazwe("okno");
    dodaj_przym("niedu^zy","nieduzi");

    set_window_id("OKNO_GARNIZON_PIANA");
    set_open_desc("");
    set_closed_desc("");
    set_window_desc(VBFC_ME("okno"));
    set_open(0);
    set_locked(1);

//    set_owners(({""}));
}

string
okno()
{
    string str;

    str = "Obramowane pomalowanym na jaki^s szaro-bury kolor drewnem "+
        "okienko, nie do^s^c, ^ze niedu^ze, to jeszcze tak przybrudzone, "+
        "^ze w^atpliwym jest, czy dostarcza odpowiedni^a ilo^s^c "+
        "^swiat^la. Nie posiada ^zadnej klamki, ani nic, czym mo^zna "+
        "by je otworzy^c czy chocia^z uchyli^c. ";

    str += "\n";

    return str;
}