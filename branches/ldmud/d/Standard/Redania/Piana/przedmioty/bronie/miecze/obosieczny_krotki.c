/* Autor: Valor
   Opis:  Faeve
   Dnia:  12.06.07
*/

inherit "/std/weapon";

#include <macros.h>
#include <stdproperties.h>
#include <wa_types.h>
#include <pl.h>
#include <materialy.h>
#include <object_types.h>

void create_weapon() 
{
        ustaw_nazwe("miecz");
        dodaj_przym("obosieczny", "obosieczni");
        dodaj_przym("kr^otki", "kr^otcy");
                    
        set_long("Kulista g^lowica i jelec tego miecza s^a wykonane z "+
            "drewna - g^ladkie i proste, ale dok^ladnie oheblowane, "+
            "wyg^ladzone i zaimpregnowane. Sam trzon opleciony jest "+
            "kawa^lkiem sk^ory, kt^ory ma wzmocni^c chwyt i zapobiec "+
            "wysuwaniu si^e miecza z d^loni. Obosieczna g^lownia jest "+
            "kr^otka, zako^nczona ostro zarysowanym szczytem, a przez jej "+
            "^srodek, od kr^otkiego, tr^ojk^atnego p^lazu a^z po sam "+
            "koniuszek ostrza biegnie gra^n. \n");
            set_hit(22);
            set_pen(21);

        set_wt(W_SWORD);
        set_dt(W_IMPALE | W_SLASH);
     
        add_prop(OBJ_I_VALUE, 2400);
        add_prop(OBJ_I_WEIGHT, 1500);
        add_prop(OBJ_I_VOLUME, 1500);   
        set_type(O_BRON_MIECZE);
        ustaw_material(MATERIALY_STAL, 70);
        ustaw_material(MATERIALY_DR_DAB, 20);
        ustaw_material(MATERIALY_SK_BYDLE, 10);
 }