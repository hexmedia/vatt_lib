/* Autor: Avard
   Opis : vortak
   Data : 06.08.2007 */

inherit "/std/object.c";

#include <pl.h>
#include <stdproperties.h>
#include <macros.h>
#include <filter_funs.h>
#include <materialy.h>
#include <object_types.h>
#include "dir.h"

create_object()
{
    ustaw_nazwe("krzes^lo");
                 
    dodaj_przym("stabilny","stabilni");
    dodaj_przym("drewniany","drewniani");
   

    make_me_sitable(({"w","na"}), "w fotelu", "z fotela", 1);

    set_long("Najzwyklejsze krzes^lo - drewniane siedzisko, takie te^z "+
        "oparcie. Dzi^eki - jak^zeby inaczej - drewnianym nogom stoi na "+
        "swym miejscu niezwykle stabilnie.\n");
    add_prop(OBJ_I_WEIGHT, 3000);
    add_prop(OBJ_I_VOLUME, 3000);
    add_prop(OBJ_I_VALUE, 340);
    ustaw_material(MATERIALY_DR_DAB, 100);
    set_type(O_MEBLE);

   // set_owners(({BIALY_MOST_NPC + "karczmarz_bialy_most"}));
}

public int
jestem_krzeslem_garnizonu_w_pianie()
{
        return 1;
}