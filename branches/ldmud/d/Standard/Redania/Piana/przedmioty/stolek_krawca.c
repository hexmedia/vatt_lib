/* Autor: Avard
   Opis : Samaia
   Data : 09.08.2007 */

inherit "/std/object.c";

#include <pl.h>
#include <stdproperties.h>
#include <macros.h>
#include <filter_funs.h>
#include <materialy.h>
#include <object_types.h>
#include "dir.h"

create_object()
{
    ustaw_nazwe("sto^lek");
                 
    dodaj_przym("ma^ly","mali");
    dodaj_przym("ko^slawy","ko^slawi");
   

    make_me_sitable(({"na"}), "na sto^lku", "ze sto^lka", 1);

    set_long("Ten krzywy sto^lek o trzech nogach, w tym jednej du^zo "+
        "kr^oszej od dw^och pozosta^lych, widocznie wiele prze^zy^l, na "+
        "co wskazuj^a r^ownie^z wbite we^n gwo^xdziki, kilka wyzieraj^acych "+
        "spod siebie warstw farby oraz wyryte w nim wzory i inicja^ly.\n");
    add_prop(OBJ_I_WEIGHT, 2000);
    add_prop(OBJ_I_VOLUME, 2000);
    add_prop(OBJ_I_VALUE, 10);
    ustaw_material(MATERIALY_DR_DAB, 100);
    set_type(O_MEBLE);

   set_owners(({PIANA_NPC + "krawiec"}));
}
