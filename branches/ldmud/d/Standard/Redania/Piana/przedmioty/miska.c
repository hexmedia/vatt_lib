/* Autor: Avard
   Opis : Samaia
   Data : 08.12.07*/

inherit "/std/receptacle";

#include "/sys/macros.h"
#include "/sys/pl.h"
#include "/sys/materialy.h"
#include "/sys/object_types.h"
#include "/sys/formulas.h"
#include "dir.h"

void
create_receptacle()
{
    ustaw_nazwe("miska");
    dodaj_przym("niewielki", "niewielcy");
    dodaj_przym("ceramiczny","ceramiczni");

    set_long("Ta niewielka, ceramiczna miska najwidoczniej nie mia^la "+
        "naj^latwiejszego ^zycia - jest ca^la podrapana i poobijana, "+
        "ponadto prezentuje rys^e na ca^lej swojej d^lugo^sci. Na jej "+
        "brzegach dostrzec mo^zna jakie^s zaschni^ete jedzenie, po czym "+
        "mo^zna wywnioskowa^c, i^z nale^zy do jakiego^s domowego "+
        "zwierz^atka. \n");
         
    add_prop(OBJ_I_VALUE, 0);
    add_prop(CONT_I_RIGID, 1);
    add_prop(CONT_I_MAX_VOLUME, 150);
    add_prop(CONT_I_MAX_WEIGHT, 300);
    add_prop(CONT_I_VOLUME, 50);
    add_prop(CONT_I_WEIGHT, 100);
    set_type(O_INNE);
    add_prop(CONT_I_NO_OPEN_DESC, 1);
    ustaw_material(MATERIALY_RZ_GLINA);
}
