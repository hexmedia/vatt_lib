inherit "/std/pattern";

#include <pattern.h>
#include "dir.h"

public void
create()
{
  set_pattern_domain(PATTERN_DOMAIN_TAILOR);
  set_item_filename(PIANA_UBRANIA +"buty/czarne_zamszowe_XLc");
  set_estimated_price(72);
  set_time_to_complete(6200);
  enable_want_sizes();
}