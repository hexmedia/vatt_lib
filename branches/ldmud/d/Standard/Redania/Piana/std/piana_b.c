
#include "dir.h"

inherit PIANA_STD;

#include <mudtime.h>
#include <language.h>
#include <stdproperties.h>
#include <macros.h>

void
create_brzeg()
{
}



nomask void
create_piana()
{
    set_long("@@dlugi_opis@@");
    set_polmrok_long("@@opis_nocy@@");
    add_sit("na brzegu", "na brzegu", "z brzegu", 0);

	add_prop(ROOM_I_TYPE,ROOM_IN_CITY);
    set_start_place(PIANA_SLEEP_PLACE);

    create_brzeg();

	set_event_time(300.0);
	

    /* prop�w o warto�ci 0 si� nie dodaje, ale jakby kto� mia� skorzysta�
     * z tego std do innych miast, musi te propy uwzgl�dni�...*/
    add_prop(ROOM_I_WSP_Y, 2); //Wsp�rz�dne, do systemu pogodowego.
    add_prop(ROOM_I_WSP_X, -4); //Rinde jest p�pkiem wszech�wiata ;)
    					//A Bia�y Most na zach�d od niego...
}
/*
public void
enter_inv(object ob, object from)
{
    ::enter_inv(ob, from);

    if (interactive(ob) && !ob->query_wiz_level()) {
        ob->set_default_start_location(PIANA_LOKACJE + "");
    }
}*/