#include "dir.h"

inherit REDANIA_STD;

#include <stdproperties.h>

void
create_piana()
{
}

nomask void
create_redania()
{
    add_prop(ROOM_I_TYPE,ROOM_IN_CITY);

    set_start_place(PIANA_SLEEP_PLACE);
    create_piana();
}
