/* Autor: Avard
   Opis : Samaia
   Data : 9.09.07
 */

#pragma unique
#pragma strict types

#include <std.h>
#include <composite.h>
#include <formulas.h>
#include <stdproperties.h>
#include <macros.h>
#include <mudtime.h>
#include <ss_types.h>
#include <money.h>
#include <pl.h>
#include <object_types.h>
#include "dir.h"
inherit "/lib/sklepikarz";

inherit PIANA_STD_NPC;

//int czy_walka();
//int walka = 0;

object kosz;

void
create_piana_humanoid()
{
    dodaj_przym("rudow^losy","rudow^losi");
    dodaj_przym("smuk^ly","smukli");

    dodaj_nazwy("kwiaciarka");

    set_long("@@dlugasny@@");
    ustaw_odmiane_rasy(PL_KOBIETA);
    set_gender(G_FEMALE);

    add_prop(CONT_I_WEIGHT, 52000);
    add_prop(CONT_I_HEIGHT, 173);
    add_prop(LIVE_I_NEVERKNOWN, 1);

    set_stats(({ 19, 70, 35, 55, 37}));
    set_skill(SS_UNARM_COMBAT, 50);
    set_skill(SS_DEFENCE, 45 + random(15));
    set_skill(SS_TRADING, 35 + random(15));
    set_skill(SS_AWARENESS, 40);
    add_armour(PIANA_UBRANIA + "sukienki/snieznobiala_zwiewna_Sc");

    config_default_sklepikarz();
    set_co_sprzedajemy(O_KWIATY);
	set_money_greed_buy(207);
	set_money_greed_sell(563);
	set_money_greed_change(119);

    set_store_room(PIANA_LOKACJE + "magazyn_kwiaciarki");

    add_ask(({"kwiaty", "kwiatki", "r^o^ze"}),VBFC_ME("pyt_o_kwiatki"));
    add_ask(({"wianki","wianuszki"}),VBFC_ME("pyt_o_wianki"));
    set_default_answer(VBFC_ME("default_answer"));

    set_act_time(10);
    add_act("usmiech czarujaco");
    add_act("emote trzepocze rz^esami uroczo.");
    add_act("emote rozgl^ada si^e dooko^la ze s^lodkim u^smiechem na ustach.");
    add_act("':wskazuj^ac na koszyk kwiat^ow: Mam takie pi^ekne kwiaty. Mo^ze kto^s zechce kupi^c?");
    add_act("emote odgarnia zb^l^akany lok z czo^la.");
    add_act("emota nuci cicho skoczn� melodyjk^e");
    add_act("usmiechnij sie zachecajaco");
    add_act("rozejrzyj sie zachecajaco");

    set_cact_time(10);
    add_cact("krzyknij Bogowie!");
    add_cact("zapiszcz przerazliwie");
    add_cact("krzyknij B^lagam, zostaw mnie!");
    add_cact("krzyknij Ratunku! Bij^a mnie!");


    setuid();
    seteuid(getuid());

    set_random_move(100);
    set_restrain_path(({PIANA_LOKACJE_ULICE, PIANA_LOKACJE + "rynek"}));

    set_alarm_every_hour("co_godzine");

    add_armour(PIANA_UBRANIA + "pantofelki/czarne_blyszczace_Sc.c");
}

string
pyt_o_kwiatki()
{
    if(!query_attack())
    {
        set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
        "':u^smiechaj^ac si^e uroczo: Pi^ekny. I tak tanio...");
    }
    else
    {
        set_alarm(0.5,0.0,"powiedz_gdy_jest", TP, "'Zostaw mnie!");
    }
    return "";
}
string
pyt_o_wianki()
{
    if(!query_attack())
    {
        set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
        "':u^smiechaj^ac si^e zach^ecaj^aco: Pi^ekne wianki w niskiej cenie.");
    }
    else
    {
        set_alarm(0.5,0.0,"powiedz_gdy_jest", TP, "'Zostaw mnie!");
    }
    return "";
}
string
default_answer()
{
     switch(random(10))
     {
     case 0: set_alarm(0.5, 0.0, "command_present", this_player(),
             "powiedz do "+ this_player()->query_name(PL_DOP) +
                  " Nie wiem...Ale mo^ze kwiatka?");
	     break;
     case 1: set_alarm(0.5, 0.0, "command_present", this_player(),
             "rozloz rece");
             break;
     case 2: set_alarm(1.5, 0.0, "command_present", this_player(),
             "hmm");
             break;
     case 3: set_alarm(0.5, 0.0, "command_present", this_player(),
             "popatrz z usmiechem na "+this_player()->query_imie(PL_DOP));
             break;
     case 4: set_alarm(0.5, 0.0, "command_present", this_player(),
             "zachichocz cicho");
             break;
     case 5: set_alarm(0.5, 0.0, "command_present", this_player(),
             "powiedz do "+ this_player()->query_name(PL_DOP) +
                  " Kup prosz^e kwiatka.");
             break;
     case 6..9: set_alarm(0.5, 0.0, "command_present", this_player(),
             "powiedz do "+ this_player()->query_name(PL_DOP) +
             " Ja nic tam nie wiem. Ale mo^ze kupi "+
	     this_player()->koncowka("pan","pani")+" kwiatka?");
	     break;
     }

  return "";
}

int
co_godzine()
{
    if(environment(this_object())->pora_roku() == MT_LATO||
       environment(this_object())->pora_roku() == MT_WIOSNA)
    {
        if (MT_GODZINA >= 23)
	    set_alarm(4.0,0.0, "destroooy");
    }
    else
    {
	if (MT_GODZINA >= 19)
	    set_alarm(3.0,0.0, "destroooy");
    }
}

void
destroooy()
{
    //tu zmienilem saybb na tell_roombb, bo saybb korzysta z this_playera
    tell_roombb(environment(), capitalize(TO->short(PL_MIA))+" wchodzi "+
          "do jednego z dom^ow.\n");
	remove_object();
}


string
dlugasny()
{
    string str;
    str = "By^c mo^ze nie jest ona zbyt ol^sniewaj^acej urody, jednak ma w "+
        "sobie co^s, co przyci^aga wzrok wielu os^ob. Mo^zliwe, i^z s^a to "+
        "ognistorude w^losy, zaplecione w ciasny warkocz, lub ogromne, "+
        "modre oczy, otoczone wianuszkiem d^lugich, czarnych rz^es. Jej "+
        "nieco zadarty nosek pokryty jest niezliczon^a liczb^a pieg^ow, co "+
        "sprawa, ^ze wygl^ada na jeszcze m^lodsz^a ni^z zapewne jest w "+
        "rzeczywisto^sci. Dziewczyna zaciska kurczowo drobne, blade d^lonie "+
        "na wiklinowym koszyku pe^lnym kwiat^ow i rozsy^la wok^o^l "+
        "promienne u^smiechy, zach^ecaj^ac do ich kupna. \n";
    return str;
}

void
init()
{
    ::init();
    init_sklepikarz();
}

void
return_introduce(string imie)
{
    object osoba;
    osoba = present(imie, environment());

        command("dygnij przed " + osoba->query_nazwa(PL_MIE));
	return;

}

void
emote_hook(string emote, object wykonujacy)
{
    switch (emote)
    {
        case "kopnij": set_alarm(0.5, 0.0, "zle", wykonujacy);
                    break;
        case "spoliczkuj": set_alarm(0.5, 0.0, "zle", wykonujacy);
                    break;
        case "opluj" : set_alarm(0.5, 0.0, "zle", wykonujacy);
                    break;
        case "szturchnij" : set_alarm(0.5, 0.0, "zle", wykonujacy);
                    break;
        case "poca^luj": set_alarm(0.5,0.0, "caluje",wykonujacy);
                    break;
        case "przytul": set_alarm(0.5,0.0, "tul",wykonujacy);
                    break;
        case "poglaszcz": set_alarm(0.5, 0.0, "tul", wykonujacy);
                    break;
        case "poklep": set_alarm(0.5, 0.0, "tul", wykonujacy);
                    break;
        case "usmiech": set_alarm(0.5, 0.0, "dobre", wykonujacy);
                    break;
        case "mrugnij": set_alarm(0.5, 0.0, "dobre", wykonujacy);
                    break;
        case "oczko": set_alarm(0.5, 0.0, "dobre", wykonujacy);
                    break;
    }
}
void
zle(object wykonujacy)
{
    switch(random(3))
    {
        case 0: command("zapiszcz krotko");break;
        case 1: command("'Hej, nie r^ob mi tak!");break;
        case 2: command("spojrzyj z wyrzutem na "+OB_NAME(wykonujacy));break;
    }
}
void
dobre(object wykonujacy)
{
    switch(random(4))
    {
        case 0: command("zarumien sie");break;
        case 1: command("emote pociera d^loni^a sw^oj policzek.");
        break;
        case 2: command("usmiechnij sie promiennie do "+OB_NAME(wykonujacy));
        break;
        case 3: command("zachichocz cicho");break;
    }
}
void
tul(object wykonujacy)
{
    switch(random(4))
    {
        case 0: command("':z u^smiechem: A mo^ze kupisz jaki^s kwiat?");break;
        case 1: command("emote trzepocze rz^esami ^slicznie.");break;
        case 2: command("zachichocz rado^snie");break;
    }
}
void
caluje(object wykonujacy)
{
    if(wykonujacy->query_gender() == 1)
    {
        switch(random(4))
        {
            case 0: command("zdziw sie");break;
            case 1: command("wytrzeszcz oczy");break;
            case 2: command("chrzaknij");break;
            case 3: command("'Lepiej co^s kup ode mnie.");break;
        }
    }
    else
    {
        switch(random(3))
        {
            case 0: command("emote spuszcza bezwiednie wzrok.");break;
            case 1: command("zarumien sie");break;
            case 2: command("rozejrzyj sie z zaklopotaniem");break;
        }
    }
}
/*
void
attacked_by(object wrog)
{
    if(walka==0)
    {
        set_alarm(10.0, 0.0, "czy_walka", 1);
        walka = 1;
        return ::attacked_by(wrog);
    }
    else
    {
        walka = 1;
        return ::attacked_by(wrog);
    }
}

int
czy_walka()
{
    if(!query_attack())
    {
        command("zadrzyj z przera^zenia.");
        walka = 0;
        return 1;
    }
    else
    {
       set_alarm(5.0, 0.0, "czy_walka", 1);
    }

}*/

/* sklepowe - np. taki hook mo^zemy ustawi^c: */
void
shop_hook_niczego_nie_skupujemy()
{
    command("powiedz Nie, ja niczego nie kupuj^e, tylko kwiatki sprzedaj^e.");
    notify_fail("");
}
