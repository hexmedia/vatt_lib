
/* Autor: Avard
   Opis : Samaia
   Data : 23.07.07 */

#pragma unique

#include <std.h>
#include <ss_types.h>
#include <macros.h>
#include <stdproperties.h>
#include <pl.h>
#include <money.h>
#include <object_types.h>
#include "dir.h"

inherit PIANA_STD_NPC;

//int czy_walka();
//int walka = 0;

void
create_piana_humanoid()
{
    ustaw_odmiane_rasy("dziewczynka");
    dodaj_nazwy("mieszczanka");
    dodaj_nazwy("kobieta");
    dodaj_nazwy("dziecko");
    set_gender(G_FEMALE);
    dodaj_przym("filigranowy","filigranowi");
    dodaj_przym("ciemnooki","ciemnoocy");

    set_long("Ma^la, zgrabna dziewczynka co chwil^e obrzuca kogo^s gniewnym "+
        "spojrzeniem ciemnych oczu, ^sciskaj^ac w pi^astce kamie^n. Jej "+
        "orzechowe loki, zwi^azane bia^l^a wst^a^zk^a, b^lyszcz^a pi^eknie "+
        "z^lotym blaskiem. Spod czerwonej, falbaniastej sukienki wystaj^a "+
        "spore b^lyszcz^ace pantofele, jednak wygl^ada jednak na to, ^ze "+
        "dziewczynce nie przeszkadza zbyt du^zy rozmiar but^ow, gdy^z z "+
        "dum^a ukazuje je przechodniom spod karminowych fa^ld sukienki. "+
        "Zapewne ka^zdy, komu si^e nie spodoba^ly, zapozna^l si^e z jednym "+
        "z jej pocisk^ow.\n");

    set_stats (({ 10, 90, 15, 53, 70 }));
    add_prop(CONT_I_WEIGHT, 25000);
    add_prop(CONT_I_HEIGHT, 120);

    set_skill(SS_UNARM_COMBAT, 50);

    set_act_time(60);
    add_act("emote zanurza patyk w ka^lu^zy.");
    add_act("emote przeciera pantofle r^ekawem sukienki.");
    add_act("emote zaczyna grzeba^c patykiem w ziemi, po czym wyciera go o sukienk^e.");
    add_act("jezyk");
    add_act("emote ta^nczy, o ma^lo nie gubi^ac zbyt du^zych but^ow.");


    set_cact_time(10);
    add_cact("rozplacz sie");
    add_cact("krzyknij Mamooo!");

    set_default_answer(VBFC_ME("default_answer"));

    add_armour(PIANA_UBRANIA + "sukienki/czerwona_falbaniasta_Sc");
    add_armour(PIANA_UBRANIA + "pantofelki/czarne_blyszczace_Sc");

    set_random_move(100);
    set_restrain_path(PIANA_LOKACJE_ULICE);
}

void
powiedz_gdy_jest(object player, string tekst)
{
    if (environment(this_object()) == environment(player))
        this_object()->command(tekst);
}
string
default_answer()
{
    if(!query_attack())
    {
        set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
            "':zaciskaj^ac w pi^e^sci kamie^n: Nie mog^e rozmawia^c z "+
            "nieznajomymi.");
    }
    else
    {
        set_alarm(0.5,0.0,"powiedz_gdy_jest", TP, "krzyknij Mamooo!");
    }
    return "";
}

void
add_introduced(string imie_mia, string imie_bie)
{
    set_alarm(2.0, 0.0, "return_introduce", find_player(imie_mia));
}
void
return_introduce(object ob)
{
    command("'Nie rozmawiam z nieznajomymi.");
}

void
emote_hook(string emote, object wykonujacy)
{
    switch (emote)
    {
        case "kopnij": set_alarm(1.5, 0.0, "nienajlepszy", wykonujacy);
                    break;
        case "spoliczkuj": set_alarm(1.5, 0.0, "nienajlepszy", wykonujacy);
                    break;
        case "opluj" : set_alarm(2.5, 0.0, "nienajlepszy", wykonujacy);
                    break;
        case "szturchnij" : set_alarm(2.5, 0.0, "nienajlepszy", wykonujacy);
                    break;
        case "poca^luj": set_alarm(1.5,0.0, "malolepszy",wykonujacy);
                    break;
        case "prychnij": set_alarm(1.0,0.0, "command", "prychnij na "+
            OB_NAME(wykonujacy));
                    break;
        case "przytul": set_alarm(1.5,0.0, "malolepszy",wykonujacy);
                    break;
        case "poglaszcz": set_alarm(0.5, 0.0, "malolepszy", wykonujacy);
                    break;
        case "poklep": set_alarm(0.5, 0.0, "malolepszy", wykonujacy);
                    break;
        case "usmiech": set_alarm(0.5, 0.0, "malolepszy", wykonujacy);
                    break;
        case "mrugnij": set_alarm(0.5, 0.0, "malolepszy", wykonujacy);
                    break;
    }
}

void
malolepszy(object wykonujacy)
{
    switch(random(2))
    {
        case 0: command("'");break;
        case 1: command("wytrzeszcz oczy");break;
    }
}
void
nienajlepszy(object wykonujacy)
{
    switch(random(2))
    {
        case 0: command("zmruz oczy gniewnie");break;
        case 1: command("'Bo ci co� zrobi�!");break;
    }
}
/*
void
attacked_by(object wrog)
{
    if(walka==0)
    {
        set_alarm(0.1,0.0,"command","krzyknij Mamooo!");
        set_alarm(10.0, 0.0, "czy_walka", 1);
        walka = 1;
        return ::attacked_by(wrog);
    }
    else
    {
        walka = 1;
        return ::attacked_by(wrog);
    }
}

int
czy_walka()
{
    if(!query_attack())
    {
        command("powiedz Po walce.");
        walka = 0;
        return 1;
    }
    else
    {
       set_alarm(5.0, 0.0, "czy_walka", 1);
    }

}*/