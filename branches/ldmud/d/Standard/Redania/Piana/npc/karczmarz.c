
/* Autor: Faeve
   Opis : Samaia
   Data : 22.09.07 */

#pragma unique

#include <macros.h>
#include <stdproperties.h>
#include <money.h>
#include <pl.h>
#include <formulas.h>
#include <std.h>
#include <ss_types.h>
#include <wa_types.h>
#include <mudtime.h>
#include <composite.h>
#include <filter_funs.h>

#include "dir.h"

inherit PIANA_STD_NPC;

/*int czy_walka();
int walka = 0; */

void
create_piana_humanoid()
{
    ustaw_odmiane_rasy("mezczyzna");
    dodaj_nazwy("karczmarz");
    set_gender(G_MALE);
    dodaj_przym("^zwawy","^zwawi");
    dodaj_przym("korpulentny","korpulentni");

    set_long("M^e^zczyzna, b^ed^acy bez w^atpienia gospodarzem tej karczmy "+
        "roztacza wok^o^l przyjazn^a, go^scinn^a atmosfer^e, za pomoc^a "+
        "u^smiechu goszcz^acego na jego ustach od rana do wieczora, oraz "+
        "mi^lemu usposobieniu przejawiaj^acemu si^e g^l^ownie w postaci "+
        "^zywego zainteresowania go^s^cmi. Jego ciemne w^losy ^sci^ete s^a "+
        "przy samej g^lowie, by nie przeszkadza^ly mu w pracy. M^e^zczyzna "+
        "od czasu do czasu, rozgl^adaj^ac si^e pogodnie po sali, wyciera "+
        "d^lonie w schludny wykrochmalony fartuch, kt^orym przepasa^l "+
        "t^egie brzuszysko.\n");

    set_stats (({ 50, 40, 60, 55, 70 }));

    set_skill(SS_UNARM_COMBAT, 50);

    add_prop(CONT_I_WEIGHT, 100000);
    add_prop(CONT_I_HEIGHT, 175);

add_armour(PIANA_UBRANIA + "spodnie/jasne_lniane_Lc");
add_armour(PIANA_UBRANIA + "koszule/cienka_luzna_Mc");
add_armour(PIANA_UBRANIA + "buty/zwykle_brazowe_Lc");

    set_act_time(60);
    add_act("rozejrzyj sie entuzjastycznie");
    add_act("podrepcz");
    add_act("':z u^smiechem: Czym mog^e s^lu^zy^c?");
    add_act("emote nuci pod nosem skoczn^a piosenk^e.");
    add_act("zanuc wesolo");
    add_act("usmiech przyjaznie");
    add_act("wyciera st^o^l r^abkiem fartucha.");

    set_cact_time(10);
    add_cact("zblednij");
    add_cact("krzyknij Stra^z! Stra^z, do mnie!");
    add_cact("krzyknij Stra^z! Bij^a!");
    add_cact("krzyknij A poszed^l won!");

    add_ask(({"dania","danie","napoje","jedzenie"}), VBFC_ME("pyt_o_dania"));
    add_ask(({"pian^e","rinde","bia^ly most"}), VBFC_ME("pyt_o_miasta"));

/***********************************************************

add_ask(({"pomoc","prac�","zadanie","zlecenie"})),
        VBFC_ME("pyt_o_zadanie")); QUEST!!!!!!!!!!!

************************************************************/

    set_default_answer(VBFC_ME("default_answer"));

    set_random_move(100);
    set_restrain_path(PIANA_LOKACJE_ULICE);
}

void
powiedz_gdy_jest(object player, string tekst)
{
    if (environment(this_object()) == environment(player))
        this_object()->command(tekst);
}

string
pyt_o_dania()
{
    if(!query_attack())
    {
        switch(random(3))
        {
            case 0: set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
        "':z u^smiechem: Oj, polecam.");
        case 1: set_alarm(0.5, 0.0, "command_present", this_player(),
            "pokiwaj z usmiechem");
        set_alarm(0.5, 0.0, "command_present", this_player(),
            "wskaz menu");
        case 2: set_alarm(0.5, 0.0, "command_present", this_player(),
            "pstryknij fachowo");
        set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
            "'Bardzo dobry wyb^or!");
        }
    }
    else
    {
        set_alarm(0.5,0.0,"powiedz_gdy_jest", TP, "'Zostaw mnie!");
    }
    return "";
}
string
pyt_o_miasta()
{
    if(!query_attack())
    {
        set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
        "'Moja karczma jest najlepsza w okolicy.");
        set_alarm(0.5, 0.0, "command_present", this_player(),
            "usmiech radosnie");
    }
    else
    {
        set_alarm(0.5,0.0,"powiedz_gdy_jest", TP, "'Zostaw mnie!");
    }
    return "";
}

string
default_answer()
{
    if(!query_attack())
    {
        switch(random(3))
        {
            case 0: set_alarm(0.5, 0.0, "command_present", this_player(),
            "zasmiej sie rubasznie");
            set_alarm(0.6, 0.0, "powiedz_gdy_jest", this_player(),
            "':z usmiechem: Tak, tak, ale nie wiem o co chodzi.");
            set_alarm(0.7, 0.0, "command_present", this_player(),
            "rozloz rece");

            case 1: set_alarm(0.5, 0.0, "command_present", this_player(),
            "potrzyj czolo");
            set_alarm(0.6, 0.0, "command_present", this_player(),
            "hmm");

            case 2: set_alarm(0.5, 0.0, "command_present", this_player(),
            "wzrusz ramionami");
            set_alarm(0.5, 0.0, "command_present", this_player(),
            "usmiech nieznacznie");
        }
    }
    else
    {
        set_alarm(0.5,0.0,"powiedz_gdy_jest", TP, "'Zostaw mnie!");
    }
    return "";
}

void
add_introduced(string imie_mia, string imie_bie)
{
    set_alarm(2.0, 0.0, "return_introduce", find_player(imie_mia));
}
void
return_introduce(object ob)
{
    command("emote kiwa g^low^a i mamrocze co^s cicho do siebie.");
}

void
emote_hook(string emote, object wykonujacy)
{
    switch (emote)
    {
        case "kopnij": set_alarm(1.5, 0.0, "nienajlepszy", wykonujacy);
                    break;
        case "spoliczkuj": set_alarm(1.5, 0.0, "nienajlepszy", wykonujacy);
                    break;
        case "opluj" : set_alarm(2.5, 0.0, "nienajlepszy", wykonujacy);
                    break;
        case "szturchnij" : set_alarm(2.5, 0.0, "nienajlepszy", wykonujacy);
                    break;
        case "poca^luj": set_alarm(1.5,0.0, "malolepszy",wykonujacy);
                    break;
        case "prychnij": set_alarm(1.0,0.0, "command", "spojrzyj "+
            "lekcewa^z^aco na "+OB_NAME(wykonujacy));
                    break;
        case "przytul": set_alarm(1.5,0.0, "malolepszy",wykonujacy);
                    break;
        case "poglaszcz": set_alarm(0.5, 0.0, "malolepszy", wykonujacy);
                    break;
        case "poklep": set_alarm(0.5, 0.0, "malolepszy", wykonujacy);
                    break;
        case "usmiech": set_alarm(0.5, 0.0, "malolepszy", wykonujacy);
                    break;
        case "mrugnij": set_alarm(0.5, 0.0, "malolepszy", wykonujacy);
                    break;
    }
}

void
malolepszy(object wykonujacy)
{
    switch(random(1))
    {
        case 0: command("emote rumieni si^e i spuszcza nie^smia^lo wzrok.");break;
    }
}
void
nienajlepszy(object wykonujacy)
{
    switch(random(1))
    {
        case 0: command("':j^akaj^ac si^e nerwowo: B-bo z-zawo^lam stra^z!");break;
    }
}

int
query_karczmarz()
{
	return 1;
}

/*void
attacked_by(object wrog)
{
    if(walka==0)
    {
        set_alarm(0.1,0.0,"command","krzyknij Pomocy!");
        set_alarm(10.0, 0.0, "czy_walka", 1);
        walka = 1;
        return ::attacked_by(wrog);
    }
    else
    {
        walka = 1;
        return ::attacked_by(wrog);
    }
} */

/* int
czy_walka()
{
    if(!query_attack())
    {
        command("powiedz Po walce.");
        walka = 0;
        return 1;
    }
    else
    {
       set_alarm(5.0, 0.0, "czy_walka", 1);
    }

} */
