
/* Autor: Sedzik i Avard
   Opis : Vayneela
   Data : 29.08.07 */

inherit "/std/humanoid.c";
inherit "/lib/straz/straznik.c";

#include "dir.h"
#include <std.h>
#include <ss_types.h>
#include <macros.h>
#include <stdproperties.h>
#include <pl.h>
#include <money.h>

#define MIASTO "z Piany"

#define BRON PIANA_BRONIE + "miecze/obosieczny_krotki.c"
#define ZBRO PIANA_ZBROJE + "przeszywanice/pikowana_brazowa_Mc.c"
#define BUTY PIANA_UBRANIA + "buty/czarne_zolnierskie_Mc.c"
#define SPOD PIANA_UBRANIA + "spodnie/grube_lniane_Mc.c"

object admin=TO->query_admin();

void
create_humanoid()
{
    ustaw_odmiane_rasy("m^e^zczyzna");
    dodaj_nazwy("stra積ik");

    random_przym(({"prosty", "pro軼i", "niepewny", "niepewni", "cichy", "cisi"}),
        ({"zielonooki", "zielonoocy", "niebieskooki", "niebieskoocy",
        "br您owooki", "br您owoocy", "czarnooki", "czarnoocy"}),
        ({"m這dy", "m這dzi", "wysoki", "wysocy"}),
        ({"czarnow這sy", "czarnow這si", "blondw這sy", "blondw這si"}), 2);

    set_gender(G_MALE);
    set_spolecznosc("Piana");

    set_long("Ju^z na pierwszy rzut oka wida^c, ^ze wygl^ad tego ^zo^lnierza jest "
        +"r^ownie prosty co jego charakter. Nie trudno si^e domy^sli^c, ^ze nie "
        +"zawacha si^e wysmarka^c w palce przy najznamienitszym towarzystwie, czy "
        +"te^z podrapa^c w krocze, kiedy poczuje tak^a potrzeb^e. Nie, zdecydowanie "
        +"nie jest wojakiem z powo^lania a pisk nawet najmiejszej myszy powoduje, "
        +"^ze chowa si^e za swych kompan^ow, przezornie z d^loni^a na r^ekoje^sci broni, "
        +"cz^esto zapominaj^ac jej doby^c. \n");

    set_stats(({80, 150,150, 150, 150}));

    set_attack_chance(100);
    set_aggressive(&check_living());

    add_prop(CONT_I_WEIGHT, 70000);
    add_prop(CONT_I_HEIGHT, 185);

    set_act_time(60);
    add_act("emote rozgl^ada si^e niepewnie. ");
    add_act("emote chrz^aka cicho i patrzy spode ^lba na towarzyszy. ");
    add_act("emote szura nog^a po ziemi. ");
    add_act("emote przeci^aga si^e lekko. ");
    add_act("emote czka cicho. ");

    set_cact_time(10);
    add_cact("emote odskakuje przera^zony! ");
    add_cact("emote naciera ponownie ponaglony rozkazem. ");
    add_cact("emote odmawia po cichu modlitwe. ");
    add_cact("emote rzuca Ci przera^zone spojrzenie. ");

    add_weapon(BRON);
    add_armour(ZBRO);
    add_armour(BUTY);
    add_armour(SPOD);

    add_prop(LIVE_I_NEVERKNOWN, 1);

    add_ask(({"zadanie", "prace"}), VBFC_ME("pyt_o_zadanie"));
    set_default_answer(VBFC_ME("default_answer"));

    set_skill(SS_WEP_SWORD, 30);
    set_skill(SS_UNARM_COMBAT, 30);
    set_skill(SS_PARRY, 20);
    set_skill(SS_DEFENCE, 10);
    set_skill(SS_AWARENESS, 64);
    set_skill(SS_BLIND_COMBAT, 5);

    create_straznik();
}

void
powiedz_gdy_jest(object player, string tekst)
{
    if(environment(this_object()) == environment(player))
        this_object()->command(tekst);
}
string
default_answer()
{
    set_alarm(2.0, 0.0, "powiedz_gdy_jest", this_player(),
        "emote odwraca wzrok i pogwizduje cicho.");
    return "";
}

string
pyt_o_zadanie()
{
    if(!query_attack())
    {
        set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
        "powiedz Z tym to do oficera chyba... ");
    }
    else
        set_alarm(0.5,0.0,"powiedz_gdy_jest", TP, "powiedz Eee... ");
    return "";
}

void
add_introduced(string imie_mia, string imie_bie)
{
    set_alarm(2.0, 0.0, "return_introduce", find_player(imie_mia));
}

void
return_introduce(object ob)
{
    command("powiedz Eee... ");
}

void
emote_hook(string emote, object wykonujacy)
{
    switch (emote)
    {
        case "kopnij":
            set_alarm(1.5, 0.0, "nienajlepszy", wykonujacy);
            break;
        case "spoliczkuj":
            set_alarm(1.5, 0.0, "nienajlepszy", wykonujacy);
            break;
        case "opluj" :
            set_alarm(2.5, 0.0, "nienajlepszy", wykonujacy);
            break;
        case "poca^luj":
            if(wykonujacy->query_gender() == 1)
                command("emote pr^e^zy dumnie pier^s, got闚 stan^a^c na baczno^s^c.");
            else
            {
                switch(random(3))
                {
                    case 0: command("emote rumieni si^e nieznacznie. "); break;
                    case 1: command("emote chrz^aka niepewnie. "); break;
                    case 2: command("emtoe macha r^ek^a niefrasobliwie. "); break;
                }
            }
            break;
        case "przytul":
            if(wykonujacy->query_gender() == 1)
                command("emote pr^e^zy dumnie pier^s, gotow stan^a^c na baczno^s^c.");
            else
            {
                switch(random(3))
                {
                    case 0: command("emote rumieni si^e nieznacznie. "); break;
                    case 1: command("emote chrz^aka niepewnie. "); break;
                    case 2: command("emtoe macha r^ek^a niefrasobliwie. "); break;
                }
            }

            break;
   }
}

void
nienajlepszy(object wykonujacy)
{
    switch(random(2))
    {
        case 0:
            TP->catch_msg(QCIMIE(TO, PL_MIA)+" wygra^za Ci pi^e^sci^a.\n");
            saybb(QCIMIE(TO, PL_MIA)+" wygra^za " +QIMIE(TP, PL_BIE)+" pi^e^sci^a.\n");
            break;
        case 1:
            command("emote wzdycha ci^e^zko i cofa si^e.");
            break;
   }
}