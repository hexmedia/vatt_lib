/* Autor:
   Opis :
   Data : */

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"
#include <pogoda.h>

inherit STREET_STD;

void create_ulica()
{
    set_short("W pobli^zu rynku");
    add_exit(PIANA_LOKACJE + "rynek","n",0,1,0);
    add_exit(PIANA_LOKACJE_ULICE + "u06","s",0,1,0);
    add_object(PIANA_DRZWI + "drzwi_do_garnizonu");
    add_object(PIANA_PRZEDMIOTY + "przystanek");

    add_prop(ROOM_I_INSIDE,0);
    add_object(PIANA_PRZEDMIOTY + "lampa_uliczna");
    add_item(({"p^lot","chat^e","chatk^e"}),"Niedu^za, drewniana chata "+
        "pokryta gontem otoczona jest wysokim raptem na trzy ^lokcie, "+
        "bielonym p^lotem zbitym z prostych, dok^ladnie oheblowanych desek. "+
        "Jaka� krzywo powieszona tabliczka dynda na jednym z jego �eber. "+
        "Mniej wi^ecej w po^lowie jego d^lugo^sci wstawiona jest zamkni^eta "+
        "od wewn^atrz furtka.\n");

    add_item(({"garnizon","budynek garnizonu","du^zy budynek"}),"Do^s^c "+
        "du^zy drewniany budynek. Widoczne w oknach kraty oraz umieszczony "+
        "nad drzwiami napis utwierdzaj^a ci^e w przekonaniu, ^ze tu "+
        "w^la^snie mie^sci si^e miejscowy garnizon.\n");

    add_cmd_item(({"napis nad drzwiami","napis nad drzwiami garnizonu",
        "napis nad drzwiami budynku","napis"}),"przeczytaj","Nad "+
        "drzwiami wej^sciowymi do drewnianego budynku kto^s nabazgra^l "+
        "du^zymi, nieco ko^slawymi literami: GARNIZON.\n");
    add_item(({"napis nad drzwiami","napis nad drzwiami garnizonu",
        "napis nad drzwiami budynku","napis"}),"Nad "+
        "drzwiami wej^sciowymi do drewnianego budynku kto^s nabazgra^l "+
        "du^zymi, nieco ko^slawymi literami: GARNIZON.\n");
}

public string
exits_description()
{
    return "Ulica ci^agnie si^e z po^ludnia na p^o^lnoc, w kierunku "+
        "rynku, za^s na wschodzie znajduje si^e wej^scie do garnizonu.\n";
}

string
dlugi_opis()
{
    string str;
    str = "Miejska uliczka, na kt^orej w^la^snie ";

    if(TP->query_prop(PLAYER_M_SIT_SIEDZACY))
        str += "siedzisz";
    else if(TP->query_prop(PLAYER_M_SIT_LEZACY))
        str += "le�ysz";
    else
        str += "stoisz";

    str += "jest dostatecznie szeroka, by m^og^l ni^a przejecha^c "+
        "za^ladowany kupiecki w^oz, jednak min^a^c si^e na niej da^lo "+
        "by rad^e najwy^zej dw^och je^xd^xc^ow. Wy^lo^zona przed laty "+
        "g^ladkimi kamieniami, wiedzie od widocznego na p^o^lnocy "+
        "miejskiego rynku na po^ludnie. Tam te^z robi si^e coraz "+
        "szersza - w^la^snie w tym miejscu zaczyna si^e trakt "+
        "^l^acz^acy Pian^e z niezby odleg^lym Bia^lym Mostem. Widoczne "+
        "na jej skrajach rynsztoki wype^lnione s^a ";

    if(pora_roku() == MT_WIOSNA || pora_roku() == MT_JESIEN)
        str += "b^lotnist^a, ^smierdz^ac^a brej^a. ";
    if(pora_roku() == MT_ZIMA)
        str += "zamarzaj^ac^a brej^a. ";

    if(pora_roku() == MT_LATO)
    {
        str += "brej^a, nad kt^or^a unosi si^e nieprawdopodobny wr^ecz "+
            "od^or. ";
    }

    str += "Na zachodzie widzisz otoczon^a wysokim p^lotem, na "+
        "kt�rym niedbale zawis�a jaka� tabliczka, niezbyt "+
        "okaza^l^a drewnian^a cha^lupin^e pokryt^a gontem, na wschodzie "+
        "za^s wyrasta niewiele wi^ekszy budynek miejscowego garnizonu, "+
        "o czym ^swiadczy odpowiedni napis umieszczony nad drzwiami "+
        "wej^sciowymi.\n";

    return str;
}
