
#include "dir.h"
inherit PIANA_STD;

#include <stdproperties.h>

public void
create_piana()
{
	set_short("Na ga��zi roz�o�ystego klonu");
	set_long("Ga��� jest gruba, lecz i tak jest to do�� "+
            "niestabilne miejsce. Wok� rozpo�ciera si� widok na ca�y plac.\n");
	add_sit("na ga��zi","na ga��zi","z ga��zi",2);
	add_exit(PIANA_LOKACJE_ULICE+"u02.c", ({ "d", "na d^o^l", "z drzewa" }), 0, 25);
        add_prop(CONT_I_MAX_VOLUME, 100000);
	add_prop(ROOM_I_INSIDE,0);
	add_prop(ROOM_I_TYPE,ROOM_TREE);

}
public string
exits_description()
{
    return "Na dole znajduje si^e plac.\n";
}

