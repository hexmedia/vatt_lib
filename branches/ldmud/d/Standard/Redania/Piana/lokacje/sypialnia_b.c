/* Autor: Avard
   Data : 23.09.07 */

#include "dir.h"

inherit PIANA_STD;

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <pogoda.h>

void
create_piana()
{
    set_short("Sypialnia burmistrza");
    add_prop(ROOM_I_INSIDE,1);
    add_object(PIANA_DRZWI + "drzwi_z_sypialni_burmistrza");
}

public string
exits_description()
{
    return "Drzwi prowadz^a na zewn^atrz.\n";
}

string
dlugi_opis()
{
    string str;
    str = "Nie powinno Ci^e tu by^c, zg^lo^s b^l^ad.\n";
    return str;
}
