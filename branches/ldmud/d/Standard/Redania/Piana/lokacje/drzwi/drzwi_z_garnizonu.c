/* Autor: Avard
   Opis : Samaia
   Data : 3.08.07 */

inherit "/std/door";

#include <pl.h>
#include <stdproperties.h>
#include "dir.h"

void
create_door()
{
    ustaw_nazwe("drzwi");
    dodaj_przym("ogromne","ogromni");
    dodaj_przym("ci^e^zki", "ci^e^zcy");

    set_other_room(PIANA_LOKACJE_ULICE + "u05");
    set_door_id("DRZWI_DO_GARNIZONU_PIANY");
    set_door_desc("Ogromne, z pewno^sci^a przewy^zszaj^ace niejednego "+
        "elfa, ci^e^zkie drzwi wykonane z ciemnego, d^ebowego drewna, "+
        "zosta^ly skrz^etnie okute w ^zelazne ramy, cho^c i tak nie "+
        "wygl^adaj^a na takie, kt^ore mo^zna by otworzy^c byle pchni^eciem. "+
        "Pod prost^a klamk^a znajduj^a si^e liczne skoble i zasuwki. Ca^le "+
        "wrota zosta^ly osadzone w pot^e^znej, metalowej o^scie^znicy, "+
        "kt^ora tym bardziej nie zach^eca do jakichkolwiek pr^ob wywa^zania "+
        "drzwi. \n");
    
    set_open_desc("");
    set_closed_desc("");

    set_pass_command(({"drzwi","wyj^scie"}),
        "przez ogromne ci^e^zkie drzwi","z garnizonu");

    set_open(0);
    set_locked(0);

    set_key("KLUCZ_DRZWI_DO_GARNIZONU_PIANY");
    set_lock_name(({"zamek", "zamku", "zamkowi", "zamek", "zamkiem", "zamku"}), 0, PL_MESKI_NOS_NZYW);

    add_prop(DOOR_I_HEIGHT, 250);
}