/* Autor: Avard
   Opis : Samaia
   Data : 3.08.07 */

inherit "/std/door";

#include <pl.h>
#include "dir.h"

void
create_door()
{
    ustaw_nazwe("drzwi");
    dodaj_przym("szeroki","szerocy");
    dodaj_przym("ci^e^zki", "ci^e^zcy");

    set_other_room(PIANA_LOKACJE + "gabinet.c");
    set_door_id("DRZWI_DO_BURMISTRZA_PIANY");
    set_door_desc("Dwuskrzyd^lowe, drewnianie drzwi, dla bezpecze^nstwa okute "+
        "w mocne, ^zelazne ramy, zosta^ly pomalowane jaskrawozielon^a "+
        "farb^a niesamowicie kontrastuj^ac^a z biel^a ^sciany, w kt^orej "+
        "je umieszczono. Rozleg^le, wielobarwne malunki przedstawiaj^ace "+
	 "prymitywne wyobra^zenie ro^slinnych motyw^ow nadaj^a wrotom "+
	 "i^scie wiejski urok i charakter. \n");

    set_open_desc("");
    set_closed_desc("");

    set_pass_command(({"drzwi","burmistrz","dom burmistrza","dom"}),
        "przez szerokie ci�kie drzwi","z zewn�trz");

    set_open(0);
    set_locked(0);

    set_key("KLUCZ_DRZWI_DO_BURMISTRZA_PIANY");
    set_lock_name(({"zamek", "zamku", "zamkowi", "zamek", "zamkiem", "zamku"}), 0, PL_MESKI_NOS_NZYW);
}