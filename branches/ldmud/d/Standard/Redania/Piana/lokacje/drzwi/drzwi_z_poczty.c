/* Autor: Avard
   Opis : Samaia
   Data : 3.08.07 */

inherit "/std/door";

#include <pl.h>
#include "dir.h"

void
create_door()
{
    ustaw_nazwe("drzwi");
    dodaj_przym("ci^e^zki", "ci^e^zcy");
    dodaj_przym("d^ebowy","d^ebowi");

    set_other_room(PIANA_LOKACJE + "rynek.c");
    set_door_id("DRZWI_DO_POCZTY_PIANY");
    set_door_desc("S^a to ci^e^zkie, d^ebowe drzwi, wyposa^zone w dwie "+
        "mosi^e^zne klamki, po jednej na ka^zdym skrzydle. Ponadto "+
        "posiadaj^a niewielkie ko^latki, kszta^ltem przypominaj^ace "+
        "go^l^ebie o rozpostartych szeroko skrzyd^lach. \n");

    set_open_desc("");
    set_closed_desc("");

    set_pass_command(({"drzwi","wyj^scie"}),
        "przez ci^e^zkie d^ebowe drzwi","z budynku poczty");

    set_open(0);
    set_locked(0);

    set_key("KLUCZ_DRZWI_DO_POCZTY_PIANY");
    set_lock_name(({"zamek", "zamku", "zamkowi", "zamek", "zamkiem", "zamku"}), 0, PL_MESKI_NOS_NZYW);
}