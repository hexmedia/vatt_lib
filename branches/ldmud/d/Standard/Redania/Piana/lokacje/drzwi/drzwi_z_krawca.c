/* Autor: Avard
   Opis : Samaia
   Data : 3.08.07 */

inherit "/std/door";

#include <pl.h>
#include "dir.h"

void
create_door()
{
    ustaw_nazwe("drzwi");
    dodaj_przym("niedu^zy","nieduzi");
    dodaj_przym("ciemnoczerwony", "ciemnoczerwoni");

    set_other_room(PIANA_LOKACJE_ULICE + "u04.c");
    set_door_id("DRZWI_DO_KRAWCA_PIANY");
    set_door_desc("W w^askiej futrynie pod niewysoko po^lo^zonym "+
        "stropem mieszcz^a si^e niedu^ze drzwi. Sk^ladajaj^a si^e z "+
        "kilku r^owno ocheblowanych, grubych desek, ^swie^zo pomalowanych "+
        "ciemnoczerwon^a farb^a. \n");

    set_open_desc("");
    set_closed_desc("");

    set_pass_command(({"drzwi","wyj^scie"}),
        "przez niedu^ze ciemnoczerwone drzwi","z zak�adu krawieckiegxo");

    set_open(0);
    set_locked(0);

    set_key("KLUCZ_DRZWI_DO_KRAWCA_PIANY");
    set_lock_name(({"zamek", "zamku", "zamkowi", "zamek", "zamkiem", "zamku"}), 0, PL_MESKI_NOS_NZYW);
}