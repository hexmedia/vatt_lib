#include <stdproperties.h>
#include <object_types.h>
#include <pl.h>
#include <macros.h>
#include <mudtime.h>
#include "dir.h"
#include "/sys/filter_funs.h"

inherit PIANA_STD;

create_piana()
{
    // TODO Itemy:
    // Szafa
    set_short("Legowisko");
    set_long(
        "Na twardo ubitej ziemi, stanowi^acej pod^log^e tego niewielkiego "+
        "pomieszczenia walaj^a si^e sterty cz^e^sciowo obgryzionych z "+
        "mi^esa ko^sci. Unosi si^e tutaj md^ly zapach zgnilizny zmieszany "+
        "z ostr^a woni^a jakiej^s istoty. ^Sciany, wzmocnione d^lugimi, "+
        "prostymi deskami s^a osmolone przez szalej^acy tu kiedy^s ogie^n. "+
        "W najdalszym k^acie, pod okopconymi resztkami drewnianej szafy "+
        "kto^s lub co^s u^lo^zy^lo sobie legowisko. Stanowi je sterta "+
        "zmi^etych szmat, przesi^akni^etych silnym zapachem potu istoty, "+
        "kt^ora od dawna ju^z nie wychodzi^la na ^swiat^lo dzienne.\n"
        );
    add_prop(ROOM_I_INSIDE, 1);

    add_sit(({"na ziemi","na pod^lodze"}), "na ziemi", "z ziemi", 0);
    add_sit(({"pod ^scian^a","przy ^scianie"}), "pod ^scian^a", 
        "spod ^sciany", 0);

    add_exit(PIANA_LOKACJE + "piwniczka.c", 
        ({({"wyj^scie","korytarz","w","piwniczka","piwnica"}), 
        "w stron^e wyj^scia"}),0,1,0);

    add_item(({"ko^sci","resztki"}),"Poszarza^le, zbr^azowia^le, "+
        "cz^e^sciowo obgryzione z mi^esa stanowi^a wyj^atkowo "+
        "nieprzyjemny widok, a unosz^acy si^e znad nich fetor "+
        "gnij^acego mi^esa przyprawia o md^lo^sci.\n");

    add_item("deski","D^lugie, niezbyt starannie oheblowane deski "+
        "stanowi^a podpor^e osmalonych ^scian, chroni^ac tym samym "+
        "pomieszczenie przed zawaleniem.\n");

    add_item(({"legowisko","szmaty"}),"Przesi^akni^ete smrodem "+
        "starego potu k^l^ebowisko starych i brudnych szmat, "+
        "u^lo^zonych w co^s, co ma zapewne imitowa^c ^l^o^zko.\n");


}
public string
exits_description()
{
    return "Na zachodzie zauwa^zasz w^aski, nikn^acy w ciemno^sciach "+
        "korytarz.\n";
}