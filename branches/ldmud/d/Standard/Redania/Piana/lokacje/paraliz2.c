inherit "/std/paralyze";
 
#include <pl.h>
#include <stdproperties.h>
#include <macros.h>
#include "dir.h"
object room1 = PIANA_LOKACJE + "chalupa.c";
object piwniczka = PIANA_LOKACJE + "piwniczka.c";

void create_paralyze()
{
	set_name("podnoszenie_paraliz");
	add_allowed("wejdz", 1);
	set_fail_message("Uda^lo ci si^e podnie^s^c fragment dachu. "+
	"Je^sli chcesz zrobi^c co^s innego po prostu 'przesta^n', mo^zesz "+
	"r^ownie^z sprobowa^c 'wej^s^c do otworu'.\n");	   
	 set_remove_time(5);
	 set_stop_message("");
	 set_stop_fun("upusc_dach");
	 set_stop_verb("przesta^n");
	 set_stop_object(this_object());
	
	 set_finish_fun("upusc_dach");
	 set_finish_object(this_object());
}

void remove_paralyze()
{
	remove_object();
}
int upusc_dach()
{
    room1->flaga_off();
    piwniczka->flaga_off();
    piwniczka->remove_exit("otwor");
    TP->remove_prop("_live_s_extra_short");
    room1->reset_podniesione();
    
    if (TP->query_prop("PIWNICZKA") != 1)
    {
        write("Fragment dachu wy^slizguje ci si^e z r^ak i z hukiem opada "+
               "na ziemi^e.\n");
        saybb("Fragment dachu wy^slizguje si^e "+
               QCIMIE(this_player(), PL_CEL) +
                " z r^ak i z hukim opada na ziemi^e."+
               "\n");
    }
    else
    {
	    write("Jeste^s zbyt zm^eczony, by d^lu^zej unosi^c deski. Puszczasz "+
        "je, a one z hukiem spadaj^a na miejsce.\n");
        saybb(QCIMIE(this_player(), PL_MIA) +
            " puszcza deski, a one z hukiem spadaj^a na miejsce.\n");
        tell_room(room1, "Przesuni^ety fragment dachu wraca na swoje miejsce.\n");
    }

    TP->remove_prop("PODNOSI");
    TP->remove_prop("PIWNICZKA");
	return 0;
}
