/* Autor: Avard
   Opis : Samaia
   Data : 08.09.07 */

#include "dir.h"

inherit PIANA_STD;

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <pogoda.h>

void
create_piana()
{
    set_short("Krawiec");
    add_prop(ROOM_I_INSIDE,1);
    add_object(PIANA_DRZWI + "drzwi_z_krawca");
    add_object(PIANA_PRZEDMIOTY + "stolek_krawca");
    add_object(PIANA_PRZEDMIOTY + "stoliczek_krawca");
    dodaj_rzecz_niewyswietlana("ma^ly ko^slawy sto^lek");
    dodaj_rzecz_niewyswietlana("rozchybotany drewniany stoliczek");
    add_npc(PIANA_NPC + "krawiec");

    add_item(({"p^o^lki"}),"Zajmuj^ace wi^eksz^a cz^e^s^c ^sciany p^o^lki "+
    "z nieheblowanych desek, z trudem d^xwigaj^a bele materia^lu, p^laty "+
    "sk^ory oraz ubrania na wszelkich mo^zliwych stadiach uko^nczenia. "+
    "Towar le^z^acy na tych znajduj^acych si^e pod samym sufitem jest "+
    "niemal nierozpoznawalny pod warstw^a kurzu i paj^eczyn. Do zadzior^ow "+
    "i drzazg w wielu miejscach poprzyczepia^ly s^e d^lugie, lu^xno "+
    "zwisaj^ace nitki w przer^o^znych kolorach.\n");

    add_item(({"wystaw^e"}),"Trzy drewniane, przytwierdzone trwale do "+
    "pod^logi wieszaki ustawione na wprost okna wystawowego, pe^lnia rol^e "+
    "manekin^ow. Szerokie nieudolnie nadaj^a im kszta^ly przypominaj^ace z "+
    "grubsza ludzk^a sylwetk^e. Naci^agni^eto na nie najlepsze ubrania "+
    "jakie dotrzec mo^zna w sklepie. Na dw^och z nich znajduj^a si^e bluzki "+
    "i spudnice, trzeci za^s d^xwiga ci^e^zk^a, skurzan^a kurtk^e. Nie ma "+
    "na nich nawet ^sladu kurzu, czy wygniece^n.\n");

    add_item(({"manekiny","tkaniny","przybory"}),"Przybory krawieckie, "+
    "materia^ly, szerokie wieszaki s^lu^z^ace za manekiny oraz gotowe "+
    "ubrania zdaj^a si^e wype^lnia^c ka^zd^a woln^a przestrze^n. Niemal "+
    "nie mo^zna zrobi^c kroku by nie otrze^c si^e, lub nie potr^aci^c "+
    "czego^s wystaj^acego z p^o^lki, lub wisz^acego na ^scianie. "+
    "Mnogo^s^c drobiazg^ow utrudnia a^z skupienie na czym^s wzroku, "+
    "czy odnalezienie konkretnej rzeczy.\n");
}

public string
exits_description()
{
    return "Wyj^scie prowadzi na ulic^e.\n";
}

string
dlugi_opis()
{
    string str;
    str = "Przestrze^n w tym niewielkim i dusznym, acz dobrze o^swietlonym "+
        "pomieszczeniu zosta^la zupe^lnie wykorzystana przez zwoje "+
        "rozmaitych tkanin, drewniane manekiny, gotowe odzienie, szpule "+
        "i szpuleczki, ig^ly, no^zyce oraz inne krawieckie przybory. "+
        "Wygl^ada na to, ^ze osoba urz^adzaj^aca ten pok^oj nie pomy^sla^la "+
        "o klientach, gdy^z wolnego miejsca pozosta^lo naprawd^e niewiele";
    if(jest_rzecz_w_sublokacji(0,"rozchybotany drewniany stoliczek"))
    {
        str += ", lecz do^s^c, by zmie^sci^l si^e tu jeszcze ma^ly "+
            "stoliczek, s^lu^z^acy za lad^e. ";
        if(jest_rzecz_w_sublokacji(0,"ma^ly ko^slawy sto^lek"))
        {
            str += "Przy nim za^s dostawiono ma^ly ko^slawy sto^lek. ";
        }
    }
    else
    {
        if(jest_rzecz_w_sublokacji(0,"ma^ly ko^slawy sto^lek"))
        {
            str += ", lecz do^s^c, by zmie^sci^l si^e tu jeszcze "+
                "ma^ly ko^slawy sto^lek";
        }
        str += ". ";
    }
    str += "Poobdzierane ^sciany zosta^ly niezdarnie zakryte blisko "+
        "tuzinem zagraconych p^o^lek, a wsz^edobylski kurz pokry^l "+
        "tkaniny co mniej przegl^adanych przez klient^ow ubra^n. Przed "+
        "przeszklon^a wystaw^a, ";
    if(jest_dzien() == 1)
    {
        str += "b^ed^ac^a r^ownie^z jedynym ^xr^od^lem ^swiat^la ";
    }
    str += "ustawiono cz^e^s^c towaru, wdzi^ecznie prezentuj^acego si^e "+
        "na drewnianych manekinach. ";
    str += "\n";
    return str;
}
