#include <stdproperties.h>
#include <object_types.h>
#include <pl.h>
#include <macros.h>
#include <mudtime.h>
#include "dir.h"
#include "/sys/filter_funs.h"


inherit PIANA_STD;

string deski();
object roomup = find_object(PIANA_LOKACJE + "chalupa.c");
int flag;
int flaga;
int test_deski();
int w_otwor(string str);
int podniesione = 0;
int testuj_sile();
object paraliz;
object paraliz2;
create_piana()
{
    set_short("Spalona piwniczka");
    set_long(
        "Niedu^za piwniczka, nie posiadaj^aca ani okien, ani innych "+
        "^xr^ode^l ^swiat^la. Jest tu stosunkowo nisko - nie trzeba by^c "+
        "imponuj^acego wzrostu by wyci^agni^etymi nad g^low^e r^ekoma "+
        "dotkn^a^c sufitu. Deski, kt^orymi wzmocnione s^a ^sciany s^a "+
        "osmolone - wyra^xnie wida^c, ^ze jaki^s czas temu szala^l tutaj "+
        "ogie^n, kt^ory strawi^l praktycznie ca^le wyposa^zenie kom^orki. "+
        "Jedynie pod jedn^a ze ^scian stoj^a resztki rega^lu s^lu^z^acego "+
        "zapewne do przechowywania r^o^znorakich powide^l, kompot^ow i "+
        "innych przysmak^ow, kt^ore mo^zna znale^s^c w niejednej podobnej "+
        "piwniczce. Miejsce sprawia^loby wra^zenie dawno opustosza^lego, "+
        "gdyby nie unosz^acy si^e w powietrzu ci^e^zki zaduch, "+
        "przypominaj^acy nieco wo^n dzikiego zwierz^ecia zmieszan^a z "+
        "zapachem padliny. "+
        "@@deski@@\n");
    add_prop(ROOM_I_INSIDE, 1);
    add_item(
        "rega^l",
        "Praktycznie ca^lkowicie spalony, drewniany rega^l. Przymocowany "+
        "do ^scian, co zabezpiecza^lo go przed przewr^oceniem si^e.\n"
    );
    add_item(
        "deski",
        "Sterta desek zas^lania otw^or w suficie, kt^ory niegdy^s "+
        "stanowi^l wyj^scie na g^or^e. Zdaje si^e, ^ze mo^zna by "+
        "spr^obowa^c nieco je unie^s^c.\n"
        );
    add_cmd_item("deski","unies",test_deski);
    add_exit(PIANA_LOKACJE + "legowisko.c",
        ({({"legowisko","korytarz","e"}), "w stron^e legowiska"}),0,1,0);
}
public string
exits_description()
{
    return "Na wschodzie widzisz w^aski korytarzyk nikn^acy w g^estych "+
        "ciemno^sciach.\n";
}




init()
{
    ::init();
    add_action(w_otwor, "wejd�");
	add_action(w_otwor, "przeci�nij si�");
}

int test_deski()
{
    podniesione = 0;
    object *tab = all_inventory();
    object *arr = FILTER_OTHER_LIVE(tab);
    //dump_array(arr);
    
    int i;
    for(i=0; i<sizeof(arr) ;i++)
    {
        if (arr[i]->query_prop("PODNOSI") == 1)
        {
            
            podniesione = 1;
        }
    }
    if (podniesione == 1)
    {
        write("Juz kto^s podnosi fragment dachu!\n");
    }
    else
    {
        roomup->flaga_on();
        roomup->set_podniesione();
        flaga = 1;
        paraliz = clone_object(PIANA_LOKACJE + "paraliz.c");
        TP->add_prop("PODNOSI", 1);
        TP->add_prop("PIWNICZKA",1);
        paraliz->move(TP);
        write("Podchodzisz do desek, i pewnym ruchem starasz si^e je unie^s^c.\n");
        saybb(QCIMIE(this_player(), PL_MIA) + " podchodzi do desek i stara si^e je "+
            "unie^s^c.\n");

        
    }
    
    return 1;

}
int
testuj_sile()
{
   if(TP->query_stat(0) > 60)
   {
       TP->add_prop("_live_s_extra_short", " podnosz^acy fragment dachu");
       write("Unosisz nieco deski, a twoim oczom ukazuje si^e w^aski otw^or."+
           " Mo^zesz spr^obowa^c przecisn^a^c si^e przez niego.\n");
       saybb(QCIMIE(this_player(), PL_MIA) +" unosi nieco deski, a twoim "+
           "oczom ukazuje si^e w^aski otw^or. Mo^zesz spr^obowa^c "+
           "przecisn^a^c si^e przez niego.\n");  
        flaga = 1;

        tell_room(roomup, "Fragment dachu nagle podnosi sie do g^ory!\n");
        paraliz2 = clone_object(PIANA_LOKACJE + "paraliz2.c");
        paraliz2->create_paralyze();
        paraliz2->move(TP);
        TP->add_prop("PODNOSI", 1);
   }
   else
    {
        TP->remove_prop("PODNOSI");
        TP->remove_prop("PIWNICZKA");
        write("Z rozczarowaniem stwierdzasz, ^ze deski s^a zbyt ci^e^zkie "+
            "i za nic nie dasz rady ich unie^s^c.\n");
        saybb(QCIMIE(this_player(), PL_MIA) +"z wyrazem rozczarowania "+
            "odchodzi od desek- s^a na tyle ci^e^zkie, ^ze nie da^l rady "+
            "ich unie^s^c.\n");
    }
   
   return 1;
}

string deski()
{
    flag = roomup->get_flaga();
    string str;
    if (flag == 1)
    {
        
        str = 
            "Miejsce, kiedy^s stanowi^ace wyj^scie na g^ore, "+
            "zas^loni^ete jest jakimi^s deskami, "+
            "teraz uniesionymi przez kogo^s nieco ku g^orze. ";
    }
    if (flag == 0)
    {
        str =
            "Miejsce, kiedy^s stanowi^ace wyj^scie na g^or^e "+
            "zas^loni^ete jest jakimi^s deskami. ";
    }
    return str;
}

int w_otwor(string str)
{

    if (flaga == 1)
        {
        if (str != "do otworu" && str != "przez otw�r")
        {
            write("Gdzie chcesz wej^s^c?\n");
        }
        else
        {
            
            if (TP->query_prop("PODNOSI"))
            {
                paraliz2->remove_paralyze();
                podniesione = 0;
                flaga = 0;
                TP->remove_prop("_live_s_extra_short");
                //piwniczka->remove_exit("otwor");
                write("Z trudem przeciskasz si^e przez w^aski otw^or. W ostatniej "+
                "chwili puszczasz fragment dachu, kt^ory z hukiem opada na "+
                "ziemi^e zas^laniaj^ac wyj^scie. \n");
            
                saybb(QCIMIE(this_player(), PL_MIA) +" z trudem przeciska si^e przez "+
                "w^aski otw^or. W momencie gdy ca^lkiem w nim niknie na ziemi^e "+
                "z hukiem opada trzymany przez niego fragment dachu.\n");
                TP->remove_prop("PODNOSI");
                TP->remove_prop("PIWNICZKA");
            }    
            else
            {
                write("Z trudem przeciskasz si^e przez w^aski otw^or.\n");
                saybb(QCIMIE(this_player(), PL_MIA) +" z trudem przeciska "+
               "si^e przez otw^or by po chwili ca^lkiem w nim znikn^a^c.\n");
            }
            roomup->flaga_off();
            tell_room(roomup, QCIMIE(this_player(), PL_MIA)+
                " wychodzi z otworu.\n"); 
            tell_room(roomup, "Przesuni^ety fragment dachu wraca na swoje miejsce.\n");
            TP->move(roomup);
            
        }
        return 1;
    }
    else
    {
        return 0;
    }
}


void reset_podniesione()
{
    podniesione = 0;
}

void flaga_on()
{
    //write("on.\n");
    flaga = 1;
}
void flaga_off()
{
    //write("off.\n");
    flaga = 0;
}
int get_flaga()
{
    //write("aktualna flaga: "+flaga+".\n");
    return flaga;
}
