/* Autor: Avard
   Opis : Samaia
   Data : 29.11.07 */

#include "dir.h"

inherit PIANA_STD;

#include <stdproperties.h>
#include <mail.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <pogoda.h>

#define MAILCHECKER "/secure/mail_checker"
#define MAILREADER "/secure/mail_reader"

void
create_piana()
{
    set_short("Urz^ad pocztowy");
    add_prop(ROOM_I_INSIDE,1);
    add_object(PIANA_DRZWI + "drzwi_z_poczty");
    add_object(PIANA_PRZEDMIOTY + "tablica");
    add_npc(PIANA_NPC + "pocztowiec");

    add_item(({"lamp^e","lampy"}),"Zwyk^la olejna lampa, na tyle ma^la by "+
    "porusza^c si^e przy mocniejszym przeci^agu, wywo^lanym cho^cby "+
    "otwarciem drzwi. Nie daje zbyt du^zo ^swiat^la, a jej w^at^le, "+
    "^z^o^lte przeb^lyski nie s^a w stanie ca^lkiem o^swietli^c "+
    "pomieszczenia.\n");
    add_item("zas^lony","Zupe^lnie po^z^o^lk^la zas^lona, miejscami "+
    "zaci^agni^eta i podziurawiona, przys^lania niedbale widok na "+
    "zewn^atrz zwieszaj^ac si^e z drewnianych karniszy, umocowanych "+
    "tu^z nad sufitem. \n");

}

public string
exits_description()
{
    return "Wyj^scie prowadzi na ulic^e.\n";
}

string
dlugi_opis()
{
    string str;
    str = "To przestronne pomieszczenie zajmuje zapewne ca^l^a "+
        "powierzchni^e budynku, na co wskazuj^a znajdujace si^e na ka^zdej "+
	 "ze ^scian, przys^loni^ete po^z^o^lk^lymi zas^lonami, okna. Ca^ly "+
	 "pok^oj zagracony jest stertami dokument^ow, kt^ore beztrosko "+
	 "sp^lywaj^a z biurka, licznymi paczkami, mniejszymi, czy wi^ekszymi, "+
	 "a tak^ze rega^lami przepe^lnionymi wszelkiego rodzaju piecz^eciami "+
	 "oraz kwitami. Z sufitu zwieszaj^a si^e olejne lampy, kt^ore o "+
	 "ka^zdej porze dnia jarz^a si^e md^lym, ^z^o^ltawym ^swiat^lem, za^s "+
        "kremowe ^sciany, kt^ore w zamierzch^lej przesz^lo^sci mog^ly "+
        "by^c bia^le, goszcz^a r^o^zne tablice i og^loszenia. ";
    str += "\n";
    return str;
}

init()
{
  object obj;
  ::init();

  if (present(READER_ID, this_player()))
    return 1;
  if ((obj = clone_object(MAILREADER)) != 0)
    {
	obj->move(this_player());
    }
}

leave_inv(object who, object where)
{
  object obj;

  if (who &&
      where &&
      where != this_object() &&
      (obj = present(READER_ID, who)) != 0)
    obj->remove_object();
  ::leave_inv(who, where);
}

