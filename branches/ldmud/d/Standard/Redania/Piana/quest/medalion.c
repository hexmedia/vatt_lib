inherit "/std/armour";
#include <colors.h>
#include <materialy.h>
#include <macros.h>
#include <stdproperties.h>

void
create_armour()
{
    ustaw_nazwe("medalion");
    
    dodaj_przym("niewielki", "niewielcy");
    dodaj_przym("srebrny", "srebrni");

    set_long("Niewielki, mocno ju^z zniszczony medalion ze srebra gorszej klasy raczej " +
    "nie zachwyca bogactwem ozd^ob. Wybito na nim podobizn^e koz^la - " +
    "mo^ze to herb rodowy, a mo^ze tylko niezrozumia^la wizja artystyczna rzemie^slnika.\n");
    
    add_prop(OBJ_I_WEIGHT, 100);
    add_prop(OBJ_I_VOLUME, 40);
    add_prop(OBJ_I_VALUE, 40);
    
    set_slots(A_NECK);
    
    ustaw_material(MATERIALY_SREBRO, 85);
    ustaw_material(MATERIALY_MIEDZ, 15);
    add_prop(ARMOUR_I_D_ROZCIAGLIWOSC, 100);
}