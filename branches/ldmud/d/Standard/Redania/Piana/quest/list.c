inherit "/std/object";
inherit "/lib/wiadomosc";

#include <colors.h>
#include <stdproperties.h>
#include <macros.h>

void
create_object()
{
    ustaw_nazwe("list");
    
    dodaj_przym("stary", "starzy");
    dodaj_przym("przemoczony", "przemoczeni");

    set_long("Zniszczona od wilgoci karta papieru. Daje si^e z niej " +
    "odczyta^c jedynie szcz^atki s^l^ow.\n");
    
    set_message(set_color(3) + "\"Dr  i bracie\n\nP   ^e do ciebie z R nde, nie        " +
    "mi si^e i id^e dalej  o  lic  sz k  ^ac pracy. Nap        ci b...\"\n\n" +
    "Dalej list staje si^e ca^lkowicie nieczytelny.\n" + clear_color());
}