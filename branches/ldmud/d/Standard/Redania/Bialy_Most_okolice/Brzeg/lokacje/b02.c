/*
 *Brzeg pod Bia�ym Mostem (w Bia�y_Most_okolice)
 *Vera
 */

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"
#include <pogoda.h>

inherit BRZEG_BIALY_MOST_STD;

void create_brzeg() 
{
    set_short("Przy pomo�cie");
    set_long("@@dlugasny@@");
    add_exit(BRZEG_BIALY_MOST_LOKACJE + "b03.c","ne",0,FATIGUE_BRZEG_BIALY_MOST,0);
	add_exit(BRZEG_BIALY_MOST_LOKACJE + "b01.c","w",0,FATIGUE_BRZEG_BIALY_MOST,0);

    add_prop(ROOM_I_INSIDE,0);
    add_item("pomost",
    "Drewniana k�adka szeroka na ponad s��e� wybiega do�� daleko w rzek�. "+
    "Lekko przegni�e bale poj�kuj� w rytm krok�w. Od�r �wie�ych ryb miesza "+
    "si� w tym miejscu z zapachem mu�u i ptasich odchod�w.\n");
    add_item(({"most","filar","filary"}),"@@filary@@");
}
public string
exits_description() 
{
    return "Brzeg ci�gnie si� na zachodzie oraz na "+
	"p�nocnym-wschodzie.\n";
}


string
dlugasny()
{
	string str="";

	if(jest_dzien())
	str+="Znajdujesz si� po�r�d niskich, zapuszczonych chat podgrodzia, "+
	"nieopodal mostu, widocznego z tego miejsca w pe�nej krasie. Smuk�e, "+
	"bia�e filary podtrzymuj� lekk� konstrukcje, zwie�czon� kunsztownymi, "+
	"lecz mocno ju� zniszczonymi barierkami. Ze p�nocnego wschodu na "+
	"zach�d rozci�ga si� nabrze�e, nad kt�rym g�ruj� mury miejskie. "+
	"Gdy odwr�cisz si� na po�udnie widzisz w�ski pomost wybiegaj�cy "+
	"daleko w wolno p�yn�c� rzek� nios�c� sporo mu�u. Mo�na te� okazjonalnie "+
	"dostrzec ga��zie, czy nawet pnie.";
	else
	str+="Z nielicznych okien nadbrze�nych chat s�czy si� md�e �wiat�o, "+
		"pozwalaj�ce zorientowa� si� na tyle by nie wpa�� do rzeki. Na "+
		"tle nieba wznosz� si� mroczne kszta�ty mostu.";


	str+="\n";
	return str;
}
