/*
 *Brzeg pod Bia�ym Mostem (w Bia�y_Most_okolice)
 *Vera
 */

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"
#include <pogoda.h>

inherit BRZEG_BIALY_MOST_STD;

void create_brzeg() 
{
    set_short("Post�j barek");
    set_long("@@dlugasny@@");
	add_exit(BIALY_MOST_LOKACJE_ULICE + "u11.c","e",0,7,0);
	add_item(({"most","filar","filary"}),"@@filary@@");
	add_item(({"barki","barkasy","promy"}),"@@barki@@");
    add_prop(ROOM_I_INSIDE,0);
}

string
barki()
{
	if(jest_dzien())
		return "Przer�nych kszta�t�w barki, barkasy i promy s� "+
		"miejscem nieustaj�cej pracy. Bandery i oznaczenia na "+
		"burtach s� w wi�kszo�ci Temerskie lub Reda�skie, lecz "+
		"zd��aj� si� tak�e go�cie z Cidaris czy Kaedwen. Celuj�ce "+
		"w niebo maszty upodoba�y sobie rybitwy robi�ce tyle "+
		"ha�asu ile s� tylko w stanie. Co kilka minut kt�ra� z "+
		"�odzi przy wt�rze jeszcze g�o�niejszych krzyk�w i "+
		"przekle�stw rozpoczyna podr� a jej miejsce niebawem zajmowane "+
		"jest przez kolejn�.\n";
	else
		return "Nieliczne barki i barkasy zacumowa�y tu na noc.\n";

}


public string
exits_description() 
{
    return "Na wschodzie widnieje most.\n";
}


string
dlugasny()
{
	string str="";

	if(jest_dzien())
	str+="Znajdujesz si� na przystani barek, ulokowanej poni�ej "+
	"mostu, widocznego z tego miejsca w pe�nej krasie. Smuk�e, "+
	"bia�e filary podtrzymuj� lekk� konstrukcj^e, zwie�czon� "+
	"kunsztownymi, lecz mocno ju� zniszczonymi barierkami. "+
	"Na wschodzie znajduj� si� w�skie schody oraz pochylnia "+
	"wiod�ce na wa�. Szeroki pomost, do kt�rego cumuj� barki, "+
	"pe�en jest robotnik�w przenosz�cych przer�ne towary. "+
	"Omywaj�ca go senna rzeka co chwila m�cona jest przez "+
	"kolejn� ��d� przybywaj�c�, b�d� odbijaj�c� z tego miejsca. "+
	"Wszechobecne rybitwy wt�ruj� przekrzykuj�cym si� kupcom, "+
	"przeklinaj�cym robotnikom i �omotowi bucior�w.";
	else
	str+="Z nielicznych okien nadbrze�nych chat s�czy si� md�e "+
	"�wiat�o, pozwalaj�ce zorientowa� si� na tyle by nie wpa�� "+
	"do rzeki. Na tle nieba wznosz� si� mroczne kszta�ty muru "+
	"miejskiego i mostu.";



	str+="\n";
	return str;
}
