/*
 *Brzeg pod Bia�ym Mostem (w Bia�y_Most_okolice)
 *Vera
 */

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"
#include <pogoda.h>

inherit BRZEG_BIALY_MOST_STD;

void create_brzeg() 
{
    set_short("Poni�ej mostu");
    set_long("@@dlugasny@@");
    add_exit(BRZEG_BIALY_MOST_LOKACJE + "b02.c","e",0,FATIGUE_BRZEG_BIALY_MOST,0);
	add_exit(BIALY_MOST_LOKACJE_ULICE + "u11.c","w",0,7,0);

    add_prop(ROOM_I_INSIDE,0);
    
    add_item(({"most","filar","filary"}),
    "Smuk�e, bia�e filary wynurzaj� si� z nurtu, po czym ��cz� delikatnymi "+
    "�ukami by w ko�cu si�gn�� kunsztownych, lecz mocno ju� zniszczonych "+
    "barierek. Elfia budowla przetrwa�a wiele lat u�ytkowania przez ludzi, "+
    "co zaowocowa�o do�� posuni�t� dewastacj�. W tej chwili odbywaj� si� "+
    "na nim dora�ne remonty oszpecaj�ce most sw� prowizoryczno�ci� i "+
    "toporno�ci�.\n");
    add_item(({"mu�","�mieci","pnie","resztki"}),
    "Niesione przez szerok�, wolno tocz�c� swe wody rzek�, pnie oraz "+
    "resztki materia��w pozosta�ych z remontu mostu zablokowa�y nurt a� "+
    "od pierwszego filaru. W powsta�ym w ten spos�b zakolu zebra�o si� "+
    "sporo mu�u i �mieci tworz�cych do�� intensywnie woniej�c� mieszank�. "+
    "Raki i drobne rybki dokonuj� ca�y czas inspekcji zbutwia�ych ro�lin, "+
    "cia� drobnych zwierz�t i resztek wyrzucanych do rzeki.\n");
}

public string
exits_description() 
{
    return "Na zachodzie widnieje most, za� brzeg ci�gnie si� na wsch�d.\n";
}


string
dlugasny()
{
	string str="";

	if(jest_dzien())
		str+="Znajdujesz si� po�r�d niskich, zapuszczonych chat "+
		"podgrodzia, u st�p wa�u wiod�cego na most, widoczny z tego miejsca "+
		"w pe�nej krasie. Smuk�e, bia�e filary podtrzymuj� lekk� konstrukcj^e, "+
		"zwie�czon� kunsztownymi, lecz mocno ju� zniszczonymi barierkami. "+
		"Na wschodzie rozci�ga si� nabrze�e, nad kt�rym g�ruj� mury miejskie. "+
		"Niesione przez szerok�, wolno tocz�c� swe wody rzek�, pnie oraz "+
		"resztki materia��w pozosta�ych z remontu mostu zablokowa�y nurt a� "+
		"od pierwszego filaru. W powsta�ym w ten spos�b zakolu zebra�o si� "+
		"sporo mu�u i �mieci tworz�cych do�� intensywnie woniej�c� mieszank�.";
	else
		str+="Z nielicznych okien nadbrze�nych chat s�czy si� md�e �wiat�o, "+
		"pozwalaj�ce zorientowa� si� na tyle by nie wpa�� do rzeki. Na "+
		"tle nieba wznosz� si� mroczne kszta�ty mostu.";
		//wywali�em z oryginalnego opisu 'mury miasta'. PRzecie� BM nie ma 
		//�adnych mur�w ;)
		


	str+="\n";
	return str;
}
