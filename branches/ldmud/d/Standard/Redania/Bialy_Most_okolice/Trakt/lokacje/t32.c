/*
 * Opis oraz kod automatycznie wygenerowany za pomoca VattGen,
 * Data: Friday 02nd of November 2007 04:17:02 PM
 */

#include <pogoda.h>
#include <stdproperties.h>
#include <mudtime.h>
#include "dir.h"

inherit TRAKT_BIALY_MOST_STD;

void
create_trakt()
{
    set_short("Trakt w^sr^od ^l^ak");
    set_long("@@dlugi_opis@@");
    add_exit(TRAKT_BIALY_MOST_LOKACJE + "t33.c","ne",0,FATIGUE_TRAKT,0);
    add_exit(TRAKT_BIALY_MOST_LOKACJE + "t31.c","s",0,FATIGUE_TRAKT,0);
    add_prop(ROOM_I_INSIDE,0);

}

public string
exits_description()
{
    return "Trakt prowadzi na p^o^lnocny wsch^od i po^ludnie.\n";
}

string
dlugi_opis()
{
    string str = "";

    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="Zag^l^ebienie na ^l^ace wype^lnia zaspa ^sniegu. Otoczenie z"+
             "amar^lo w zimowym letargu. Boki pokrytego ubitym ^sniegiem t"+
             "raktu porastaj^a kolczaste krzewy i nieliczne k^epy traw, kt"+
             "^ore zdo^la^ly przebi^c poprzez zalegaj^aca na ziemi warstw^"+
             "e bia^lego puchu. Wzd^lu^z drogi ci^agn^a si^e zaro^sla w po"+
             "staci nagich ga^l^ezi drzew i krzew^ow. Znajduj^ace si^e w p"+
             "obli^zu bagna skute zosta^ly przez szcz^eki mrozu. ";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str+="W zag^l^ebieniu na ^l^ace g^esto rozpleniaj^a si^e chwasty. "+
             "Otoczenie t^etni rozkwitaj^acym na wiosn^e ^zyciem. Boki ubi"+
             "tego traktu porastaj^a liczne krzewy i k^epy zielonych traw,"+
             " kt^ore p^lynnie ^l^acz^a si^e z okalaj^acymi ^l^akami. Wzd^"+
             "lu^z drogi ci^agn^a si^e zaro^sla rozstaczaj^ac s^lodk^a wo^"+
             "n wiosennego kwiecia. Znajduj^ace si^e w pobli^zu bagna ziel"+
             "eniej^a od kwitn^acej ro^slinno^sci . ";
    }
    else if(pora_roku() == MT_LATO)
    {
        str+="Zag^l^ebienie na ^l^ace ^sci^sle wype^lniaj^a chaszcze. Otoc"+
             "zenie t^etni rozkwit^lym ^zyciem. Boki ubitego traktu porast"+
             "aj^a liczne krzewy i k^epy zielonych traw, kt^ore p^lynnie ^"+
             "l^acz^a si^e z okalaj^acymi ^l^akami. Wzd^lu^z drogi ci^agn^"+
             "a si^e g^este zaro^sla daj^ace w^edrowcom odrobin^e cienia w"+
             " upalne, letnie dni. Znajduj^ace si^e w pobli^zu bagna wydzi"+
             "elaj^a odurzaj^acy zapach butwiej^acej ro^slinno^sci. ";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str+="Zag^l^ebienie na ^l^ace wype^lniaj^a zawiane przez wiatr zes"+
             "chni^ete li^scie. Otoczenie powoli zamiera w przygotowaniu d"+
             "o zbli^zaj^acej si^e zimy. Boki ubitego traktu porastaj^a li"+
             "czne krzewy i k^epy zielonych traw, kt^ore p^lynnie ^l^acz^a"+
             " si^e z okalaj^acymi ^l^akami. Wzd^lu^z drogi ci^agn^a si^e "+
             "g^este. Zaro^sla o i^scie jesiennych, ^zywych barwach. Znajd"+
             "uj^ace si^e w pobli^zu bagna wydzielaj^a odurzaj^acy zapach "+
             "zgni^lej ro^slinno^sci. ";
    }

    str+="\n";
    return str;
}