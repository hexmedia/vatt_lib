
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"
#include <pogoda.h>

inherit TRAKT_BIALY_MOST_STD;

void create_trakt() 
{
    set_short("Trakt nad brzegiem Pontaru");
    set_long("@@dlugasny@@");
    add_exit(TRAKT_BIALY_MOST_LOKACJE + "t18.c","ne",0,FATIGUE_TRAKT,0);
    add_exit(TRAKT_BIALY_MOST_LOKACJE + "t16.c","w",0,FATIGUE_TRAKT,0);

    add_prop(ROOM_I_INSIDE,0);
}
public string
exits_description() 
{
    return "Trakt prowadzi na p�nocny-wsch�d oraz na zach�d.\n";
}

string
dlugasny()
{
	string str="";

	if(CZY_JEST_SNIEG(TO))
	{
		if(jest_dzien())
		str+="�agodnie opadaj�cy ze szczytu niewysokiego pag�rka "+
		"trakt wystarczaj�co szeroki aby wymin�y si� na nim dwa "+
		"wy�adowane kupieckie wozy prowadzi �agodnymi zakr�tami ku "+
		"widocznym na zachodzie dymom unosz�cym si� nad "+
		"zasypanymi przez �nieg zabudowaniami. ";
		else
		str+="�agodnie opadaj�cy ze szczytu niewysokiego pag�rka "+
		"trakt wystarczaj�co szeroki aby wymin�y si� na nim dwa "+
		"wy�adowane kupieckie wozy prowadzi �agodnymi zakr�tami ku "+
		"ukrytemu w ciemno�ci zachodowi. ";
	}
	else
	{
	 if(ZACHMURZENIE(TO) <= POG_ZACH_LEKKIE)
	 {
		if(jest_dzien() && pora_dnia() <= MT_POPOLUDNIE)
			str+="�agodnie opadaj�cy ze szczytu niewysokiego pag�rka trakt "+
			"wystarczaj�co szeroki aby wymin�y si� na nim dwa wy�adowane "+
			"kupieckie wozy prowadzi �agodnymi zakr�tami ku widocznym w "+
			"promieniach s�o�ca na zachodzie zabudowaniom. ";
		else if(pora_dnia() == MT_WIECZOR)
			str+="�agodnie opadaj�cy ze szczytu niewysokiego pag�rka trakt "+
			"wystarczaj�co szeroki aby wymin�y si� na nim dwa wy�adowane "+
			"kupieckie wozy prowadzi �agodnymi zakr�tami ku p�on�cych "+
			"czerwieni� w �unie zachodu zabudowaniom. ";
		else
			str+=
			"�agodnie opadaj�cy ze szczytu niewysokiego pag�rka trakt "+
			"wystarczaj�co szeroki aby wymin�y si� na nim dwa wy�adowane "+
			"kupieckie wozy prowadzi �agodnymi zakr�tami ku majacz�cym na "+
			"zachodzie zabudowaniom. ";
	 }
	 else if(jest_dzien())	
		str+=
		"�agodnie opadaj�cy ze szczytu niewysokiego pag�rka trakt "+
		"wystarczaj�co szeroki aby wymin�y si� na nim dwa wy�adowane "+
		"kupieckie wozy prowadzi �agodnymi zakr�tami ku majacz�cym na "+
		"zachodzie zabudowaniom. ";
	 else 
		str+="�agodnie opadaj�cy ze szczytu niewysokiego pag�rka trakt "+
		"wystarczaj�co szeroki aby wymin�y si� na nim dwa wy�adowane "+
		"kupieckie wozy prowadzi �agodnymi zakr�tami ku ukrytemu w "+
		"ciemno�ci zachodowi. ";
	}



	str+="\n";
	return str;
}
