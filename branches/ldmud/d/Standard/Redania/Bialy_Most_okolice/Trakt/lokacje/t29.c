/*
 * Opis oraz kod automatycznie wygenerowany za pomoca VattGen,
 * Data: Friday 02nd of November 2007 03:45:28 PM
 */

#include <pogoda.h>
#include <stdproperties.h>
#include <mudtime.h>
#include "dir.h"

inherit TRAKT_BIALY_MOST_STD;

void
create_trakt()
{
    set_short("Trakt w^sr^od p^ol");
    set_long("@@dlugi_opis@@");
    add_exit(TRAKT_BIALY_MOST_LOKACJE + "t28.c","s",0,FATIGUE_TRAKT,0);
    add_exit(TRAKT_BIALY_MOST_LOKACJE + "t30.c","nw",0,FATIGUE_TRAKT,0);
    add_prop(ROOM_I_INSIDE,0);

}

public string
exits_description()
{
    return "Trakt prowadzi na p^o^lnocny zach^od i po^ludnie.\n";
}

string
dlugi_opis()
{
    string str = "";

    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="Du^ze zaspy ^sniegu, kt^ore niemal zamazuj^a ^slad ^scie^zki"+
             " ^swiadcz^a, ^ze droga ta jest bardzo rzadko u^zywana. W^ask"+
             "i, zryty koleinami trakt wije sie po^sr^od ^lagodnych wzg^or"+
             "z. Po bokach traktu rozci^agaj^a si^e o^snie^zone pola. Na l"+
             "e^snej ^scie^zce w niekt^orych miejscach mo^zna dostrzec pok"+
             "ryte szronem li^scie, za^s doko^la widnieje ^snieg. ";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str+="Ga^l^azki i zgni^le li^scie zalegaj^ace na drodze ^swiadcz^a"+
             ", ^ze droga ta bardzo rzadko jest u^zywana. W^aski, zryty ko"+
             "leinami trakt wije sie po^sr^od ^lagodnych wzg^orz. Dopiero "+
             "co nabieraj^ace z^locistych kolor^ow zbo^ze ro^snie na zbocz"+
             "u traktu. Na le^snej ^scie^zce kwitn^a pojedyncze stokrotki,"+
             " za^s ^sci^o^lka jest pokryta szyszkami oraz nieco suchymi l"+
             "i^s^cmi, kt^ore spad^ly z drzew. ";
    }
    else if(pora_roku() == MT_LATO)
    {
        str+="Ga^l^azki, zesz^loroczne resztki li^sci i bujnie rozro^sni^e"+
             "ta trawa ^swiadcz^a, ze droga ta bardzo rzadko jest u^zywana"+
             ". W^aski, zryty koleinami trakt wije sie po^sr^od ^lagodnych"+
             " wzg^orz. Z^lociste zbo^ze, gotowe ju^z do zebrania, zdobi z"+
             "bocze traktu. Na le^snej ^scie^zce rozkwit^ly bujne kwiaty, "+
             "za^s ^sci^o^lka jest pokryta dorodnymi szyszkami oraz suchym"+
             "i li^s^cmi, kt^ore spad^ly z drzew. ";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str+="Ga^l^azki, opadni^ete li^scie i wi^edn^aca ro^slinno^s^c, kt"+
             "^ora zaros^la ca^l^a drog^e ^swiadcz^a, ^ze jest ona bardzo "+
             "rzadko u^zywana. W^aski, zryty koleinami trakt wije sie po^s"+
             "r^od ^lagodnych wzg^orz. Zbocza traktu zion^a pustk^a po zeb"+
             "ranych ju^z plonach. Na le^snej ^scie^zce wi^edn^a powoli k"+
             "wiaty, za^s ^sci^o^lk^e pokrywa dywan zwi^ed^lych i opad^lyc"+
             "h z drzew li^sci. ";
    }

    str+="\n";
    return str;
}