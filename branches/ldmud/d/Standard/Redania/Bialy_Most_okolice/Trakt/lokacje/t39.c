/*
 * Opis oraz kod automatycznie wygenerowany za pomoca VattGen,
 * Data: Friday 02nd of November 2007 10:27:51 PM
 */

#include <pogoda.h>
#include <stdproperties.h>
#include <mudtime.h>
#include "dir.h"

inherit TRAKT_BIALY_MOST_STD;

void
create_trakt()
{
    set_short("Trakt w^sr^od ^l^ak");
    set_long("@@dlugi_opis@@");
    add_exit(TRAKT_BIALY_MOST_LOKACJE + "t38.c","se",0,FATIGUE_TRAKT,0);
    add_exit(TRAKT_BIALY_MOST_LOKACJE + "t40.c","n",0,FATIGUE_TRAKT,0);
    add_prop(ROOM_I_INSIDE,0);

}

public string
exits_description()
{
    return "Trakt prowadzi na p^o^lnoc i po^ludniowy wsch^od.\n";
}

string
dlugi_opis()
{
    string str = "";

    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="Na tle bia^lej, okolicy trakt odcina si^e wyra^xnie, ciemny,"+
             " ubity i twardy, pokryty koleinami woz^ow, tysi^acem ^slad^o"+
             "w ko^nskich kopyt i ludzkich st^op. Po obu stronach drogi r"+
             "osn^a krzaki, teraz pozbawione li^sci i przykryte grub^a war"+
             "stw^a ^sniegu. Trakt zmierza ^lagodnym spadem w d^o^l, po^sr"+
             "^od pag^ork^ow pokrytych sk^apana w bieli ^l^ak^a, by dalej "+
             "rozpocz^a^c sw^a m^ecz^ac^a w^edr^owk^e ostrym podej^sciem p"+
             "od g^or^e i znikn^a^c za du^z^a k^ep^a zieleni znajduj^acej "+
             "si^e na szczycie wzniesienia. ";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str+="Na tle jasnej zieleni, trakt odcina si^e wyra^xnie, ciemny, "+
             "ubity i twardy, pokryty koleinami woz^ow, tysi^acem ^slad^ow"+
             " ko^nskich kopyt i ludzkich st^op. Po obu stronach drogi g^e"+
             "sto rosn^a rozkwitaj^ace krzaki. Trakt zmierza ^lagodnym spa"+
             "dem w d^o^l, po^sr^od pag^ork^ow pokrytych jasnozielon^a ^l^"+
             "ak^a, by dalej rozpocz^a^c sw^a m^ecz^ac^a w^edr^owk^e ostry"+
             "m podej^sciem pod g^or^e i znikn^a^c za du^z^a k^ep^a zielen"+
             "i znajduj^acej si^e na szczycie wzniesienia. ";
    }
    else if(pora_roku() == MT_LATO)
    {
        str+="Na tle jasnej zieleni, trakt odcina si^e wyra^xnie, ciemny, "+
             "ubity i twardy, pokryty koleinami woz^ow, tysi^acem ^slad^ow"+
             " ko^nskich kopyt i ludzkich st^op. Po obu stronach drogi ros"+
             "n^a w pe^lni rozwini^ete, g^este krzaki. Trakt zmierza ^lago"+
             "dnym spadem w d^o^l, po^sr^od pag^ork^ow pokrytych soczysto-"+
             "zielon^a ^l^ak^a, by dalej rozpocz^a^c sw^a m^ecz^ac^a w^edr"+
             "^owk^e ostrym podej^sciem pod g^or^e i znikn^a^c za du^z^a k"+
             "^ep^a zieleni znajduj^acej si^e na szczycie wzniesienia. ";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str+="Na tle szaro^sci, trakt odcina si^e wyra^xnie, ciemny, ubity"+
             " i twardy, pokryty koleinami woz^ow, tysi^acem ^slad^ow ko^n"+
             "skich kopyt i ludzkich st^op. Po obu stronach drogi rosn^a z"+
             "ielone krzaki o jeszcze niezwi^edni^etych li^sciach. Trakt z"+
             "mierza ^lagodnym spadem w d^o^l, po^sr^od pag^ork^ow pokryty"+
             "ch ^l^ak^a, by dalej rozpocz^a^c sw^a m^ecz^ac^a w^edr^owk^e"+
             " ostrym podej^sciem pod g^or^e i znikn^a^c za du^z^a k^ep^a "+
             "zieleni znajduj^acej si^e na szczycie wzniesienia. ";
    }

    str+="\n";
    return str;
}