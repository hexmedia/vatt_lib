/*
 * Opis oraz kod automatycznie wygenerowany za pomoca VattGen,
 * Data: Friday 02nd of November 2007 11:25:18 PM
 */

#include <pogoda.h>
#include <stdproperties.h>
#include <mudtime.h>
#include "dir.h"

inherit TRAKT_BIALY_MOST_STD;

void
create_trakt()
{
    set_short("Ubity trakt");
    set_long("@@dlugi_opis@@");
    add_exit(TRAKT_BIALY_MOST_LOKACJE + "t50","s",0,FATIGUE_TRAKT,0);
    add_exit(PIANA_LOKACJE_ULICE + "u06.c",({"n","do miasta","z traktu"}),0,FATIGUE_TRAKT,0);
    add_prop(ROOM_I_INSIDE,0);

}

public string
exits_description()
{
    return "Trakt prowadzi na p^o^lnoc w stron^e miasta i na po^ludnie.\n";
}

string
dlugi_opis()
{
    string str = "";

    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="Otoczenie zamar^lo w zimowym letargu. W stertach pokruszonyc"+
             "h kamieni wznosz^acych si^e po obu stronach drogi z trudem m"+
             "o^zna rozpozna^c pozosta^lo^sci po niewysokich kamiennych mu"+
             "rkach zbudowanych niegdy^s z bry^l piaskowca a obecnie znisz"+
             "czonych przez wiatry i deszcze, poro^sni^etych przez ma^le k"+
             "rzaczki i ruderalne zielsko. Wzd^lu^z drogi ci^agn^a si^e za"+
             "ro^sla w postaci nagich ga^l^ezi drzew i krzew^ow. ";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str+="Otoczenie t^etni rozkwitaj^acym na wiosn^e ^zyciem. W sterta"+
             "ch pokruszonych kamieni wznosz^acych si^e po obu stronach dr"+
             "ogi z trudem mo^zna rozpozna^c pozosta^lo^sci po niewysokich"+
             " kamiennych murkach zbudowanych niegdy^s z bry^l piaskowca a"+
             " obecnie zniszczonych przez wiatry i deszcze, poro^sni^etych"+
             " przez ma^le krzaczki i ruderalne zielsko. Wzd^lu^z drogi ci"+
             "^agn^a si^e zaro^sla rozstaczaj^ac s^lodk^a wo^n wiosennego "+
             "kwiecia. ";
    }
    else if(pora_roku() == MT_LATO)
    {
        str+="Otoczenie t^etni rozkwit^lym ^zyciem. W stertach pokruszonyc"+
             "h kamieni wznosz^acych si^e po obu stronach drogi z trudem m"+
             "o^zna rozpozna^c pozosta^lo^sci po niewysokich kamiennych mu"+
             "rkach zbudowanych niegdy^s z bry^l piaskowca a obecnie znisz"+
             "czonych przez wiatry i deszcze, poro^sni^etych przez ma^le k"+
             "rzaczki i ruderalne zielsko. Wzd^lu^z drogi ci^agn^a si^e g^"+
             "este zaro^sla daj^ace w^edrowcom odrobin^e cienia w upalne, "+
             "letnie dni. ";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str+="Otoczenie powoli zamiera w przygotowaniu do zbli^zaj^acej si"+
             "^e zimy. W stertach pokruszonych kamieni wznosz^acych si^e p"+
             "o obu stronach drogi z trudem mo^zna rozpozna^c pozosta^lo^s"+
             "ci po niewysokich kamiennych murkach zbudowanych niegdy^s z "+
             "bry^l piaskowca a obecnie zniszczonych przez wiatry i deszcz"+
             "e, poro^sni^etych przez ma^le krzaczki i ruderalne zielsko. "+
             "Wzd^lu^z drogi ci^agn^a si^e g^este zaro^sla o i^scie jesien"+
             "nych, ^zywych barwach. ";
    }

    str+="\n";
    return str;
}