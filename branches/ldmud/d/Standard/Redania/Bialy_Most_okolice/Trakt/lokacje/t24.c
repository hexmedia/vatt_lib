/*
 * Opis oraz kod automatycznie wygenerowany za pomoca VattGen,
 * Data: Saturday 16th of June 2007 06:45:41 PM
 */

#include <pogoda.h>
#include <stdproperties.h>
#include <mudtime.h>
#include "dir.h"

inherit TRAKT_BIALY_MOST_STD;

void
create_trakt()
{
    set_short("Trakt w^sr^od p^ol");
    set_long("@@dlugi_opis@@");
    add_exit(TRAKT_BIALY_MOST_LOKACJE + "t23.c","e",0,FATIGUE_TRAKT,0);
    add_exit(TRAKT_BIALY_MOST_LOKACJE + "t25.c","w",0,FATIGUE_TRAKT,0);
    add_prop(ROOM_I_INSIDE,0);

}

public string
exits_description()
{
    return "Trakt prowadzi na wsch^od i zach^od.\n";
}

string
dlugi_opis()
{
    string str = "";

    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="Prawie wszystko przykryte jest spor^a warstw^a bielutkiego ^"+
             "sniegu, kt^ory przysypa^l okolic^e. Tylko w najbli^zszym s^a"+
             "siedztwie utwardzonej drogi pokryte ^sniegowym puchem ro^sli"+
             "ny nie potrafi^a przetrwa^c i wybi^c si^e nad ziemi^e, a wyj"+
             "^atki kt^orym to si^e uda^lo zosta^ly zadeptane przez liczny"+
             "ch w^edrowc^ow korzystaj^acych z tego traktu. Trakt jest do^"+
             "s^c szeroki a gdzieniegdzie, utworzy^ly sie g^l^ebokie kolei"+
             "ny wy^z^lobione przez przeje^zd^zaj^ace wozy. Wok^o^l jak ok"+
             "iem si^egn^a^c rozpo^sciera sie pofa^ldowany ^lagodnymi wzg^"+
             "orzami krajobraz. ";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str+="Otaczaj^aca ci^e przyroda budzi sie do ^zycia po zimowym ^sn"+
             "ie, a lekki wietrzyk przynosi do ciebie ^swie^zy zapach ro^s"+
             "lin. Niska, polna ro^slinno^s^c, przerzedza si^e w miar^e pr"+
             "zybli^zania do go^sci^nca, za^s ziele^n rosn^aca w najbli^zs"+
             "zym s^asiedztwie drogi wydaje si^e marna i niedojrza^la, a j"+
             "u^z na pewno zadeptana. Trakt jest do^s^c szeroki a gdzienie"+
             "gdzie, utworzy^ly sie g^l^ebokie koleiny wy^z^lobione przez "+
             "przeje^zd^zaj^ace wozy. Wok^o^l jak okiem si^egn^a^c rozpo^s"+
             "ciera sie pofa^ldowany ^lagodnymi wzg^orzami krajobraz. ";
    }
    else if(pora_roku() == MT_LATO)
    {
        str+="Ca^la przyroda w pe^lnym rozkwicie, zieleni otaczaj^acy ci^e"+
             " krajobraz, a w powietrzu mo^zesz wyczu^c intensywny zapach "+
             "ro^slin. Tylko w najbli^zszym s^asiedztwie utwardzonej drogi"+
             " ro^sliny nie potrafi^a przetrwa^c i wybi^c si^e nad ziemi^e"+
             ", a wyj^atki kt^orym to si^e uda^lo zosta^ly zadeptane przez"+
             " licznych w^edrowc^ow korzystaj^acych z tego traktu. Trakt j"+
             "est do^s^c szeroki a gdzieniegdzie, utworzy^ly sie g^l^eboki"+
             "e koleiny wy^z^lobione przez przeje^zd^zaj^ace wozy. Wok^o^l"+
             " jak okiem si^egn^a^c rozpo^sciera sie pofa^ldowany ^lagodny"+
             "mi wzg^orzami krajobraz. ";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str+="Krajobraz spowija lekka mgie^lka i dymy z ognisk palonych na"+
             " polach. Niska, polna ro^slinno^s^c, przerzedza si^e w miar^"+
             "e przybli^zania do go^sci^nca, za^s ziele^n rosn^aca w najbl"+
             "i^zszym s^asiedztwie drogi wydaje si^e marna i niedojrza^la,"+
             " a ju^z na pewno zadeptana. Trakt jest do^s^c szeroki a gdzi"+
             "eniegdzie, utworzy^ly sie g^l^ebokie koleiny wy^z^lobione pr"+
             "zez przeje^zd^zaj^ace wozy. Wok^o^l jak okiem si^egn^a^c roz"+
             "po^sciera sie pofa^ldowany ^lagodnymi wzg^orzami krajobraz. ";
    }

    str+="\n";
    return str;
}