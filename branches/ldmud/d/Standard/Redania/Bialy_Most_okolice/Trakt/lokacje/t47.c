/*
 * Opis oraz kod automatycznie wygenerowany za pomoca VattGen,
 * Data: Friday 02nd of November 2007 11:18:50 PM
 */

#include <pogoda.h>
#include <stdproperties.h>
#include <mudtime.h>
#include "dir.h"

inherit TRAKT_BIALY_MOST_STD;

void
create_trakt()
{
    set_short("Ubity trakt");
    set_long("@@dlugi_opis@@");
    add_exit(TRAKT_BIALY_MOST_LOKACJE + "t46.c","s",0,FATIGUE_TRAKT,0);
    add_exit(TRAKT_BIALY_MOST_LOKACJE + "t48.c","nw",0,FATIGUE_TRAKT,0);
    add_prop(ROOM_I_INSIDE,0);

}

public string
exits_description()
{
    return "Trakt prowadzi na p^o^lnocny zach^od i po^ludnie\n";
}

string
dlugi_opis()
{
    string str = "";

    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="Po obu stronach drogi rosn^a krzaki, teraz pozbawione li^sci"+
             " i przykryte grub^a warstw^a ^sniegu. Otoczenie zamar^lo w z"+
             "imowym letargu. Pomi^edzy kamykami po^lo^zonymi, by wzmocni^"+
             "c nawierzchnie traktu wida^c pozamarzane ka^lu^ze. Trakt jes"+
             "t do^s^c szeroki a gdzieniegdzie, utworzy^ly sie g^l^ebokie "+
             "koleiny wy^z^lobione przez przeje^zd^zaj^ace wozy. W niekt^o"+
             "rych miejscach koleiny s^a tak g^l^ebokie, i^z w^oz jad^acy "+
             "t^edy musi bardzo zwolnic by nie z^lama^c zawieszenia. ";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str+="Po obu stronach drogi g^esto rosn^a rozkwitaj^ace krzaki. Ot"+
             "oczenie t^etni rozkwitaj^acym na wiosn^e ^zyciem. Pomi^edzy "+
             "kamykami po^lo^zonymi, by wzmocni^c nawierzchnie traktu, zac"+
             "zyna kie^lkowa^c szorstka trawa i jakie^s inne pospolite zie"+
             "lska. Trakt jest do^s^c szeroki a gdzieniegdzie, utworzy^ly "+
             "sie g^l^ebokie koleiny wy^z^lobione przez przeje^zd^zaj^ace "+
             "wozy. W niekt^orych miejscach koleiny s^a tak g^l^ebokie, i^"+
             "z w^oz jad^acy t^edy musi bardzo zwolnic by nie z^lama^c zaw"+
             "ieszenia. ";
    }
    else if(pora_roku() == MT_LATO)
    {
        str+="Po obu stronach drogi rosn^a w pe^lni rozwini^ete, g^este kr"+
             "zaki. Otoczenie t^etni rozkwit^lym ^zyciem. Pomi^edzy kamyka"+
             "mi po^lo^zonymi, by wzmocni^c nawierzchnie traktu, ro^snie b"+
             "ujnie szorstka trawa i jakie^s inne pospolite zielska. Trakt"+
             " jest do^s^c szeroki a gdzieniegdzie, utworzy^ly sie g^l^ebo"+
             "kie koleiny wy^z^lobione przez przeje^zd^zaj^ace wozy. W nie"+
             "kt^orych miejscach koleiny s^a tak g^l^ebokie, i^z w^oz jad^"+
             "acy t^edy musi bardzo zwolnic by nie z^lama^c zawieszenia. ";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str+="Po obu stronach drogi rosn^a zielone krzaki o jeszcze niezwi"+
             "^edni^etych li^sciach. Otoczenie powoli zamiera w przygotowa"+
             "niu do zbli^zaj^acej si^e zimy. Pomi^edzy kamykami po^lo^zon"+
             "ymi, by wzmocni^c nawierzchnie traktu, ro^snie trawa i jakie"+
             "^s inne wysuszone, pospolite zielska. Trakt jest do^s^c szer"+
             "oki a gdzieniegdzie, utworzy^ly sie g^l^ebokie koleiny wy^z^"+
             "lobione przez przeje^zd^zaj^ace wozy. W niekt^orych miejscac"+
             "h koleiny s^a tak g^l^ebokie, i^z w^oz jad^acy t^edy musi ba"+
             "rdzo zwolnic by nie z^lama^c zawieszenia. ";
    }

    str+="\n";
    return str;
}