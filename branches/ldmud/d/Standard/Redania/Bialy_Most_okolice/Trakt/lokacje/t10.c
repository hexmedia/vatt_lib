
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"
#include <pogoda.h>

inherit TRAKT_BIALY_MOST_STD;

void create_trakt() 
{
    set_short("Szeroki trakt");
    set_long("@@dlugasny@@");
    add_exit(TRAKT_BIALY_MOST_LOKACJE + "t09.c","s",0,FATIGUE_TRAKT,0);
    add_exit(TRAKT_BIALY_MOST_LOKACJE + "t11.c","e",0,FATIGUE_TRAKT,0);

    add_prop(ROOM_I_INSIDE,0);
}
public string
exits_description() 
{
    return "Trakt ci�gnie si� na po�udnie oraz na wsch�d.\n";
}

string
dlugasny()
{
	string str="";

	if(CZY_JEST_SNIEG(TO))
		str+="Powierzchni� traktu pokrywa gruba i �liska warstwa lodu. "+
		"Pobocze porastaj� liczne zaro�la przypruszone bia�ymi p�atkami "+
		"wyci�gaj�ce swe w�t�e ga��zki ku niebu. Po�udniow� lini� "+
		"horyzontu kszta�tuje rzeka, a dryfuj�ce na niej kawa�ki kry "+
		"rozbijaj� si� o wystaj�ce ponad powierzchni� wody kamienie. ";
	else
		str+="Trakt wiedzie od po�o�onego na wschodzie miasta, ledwo "+
		"widocznego z tej odleg�o�ci, a� po niewielk� mie�cin� na zachodzie. "+
		"Po�udniow� lini� horyzontu kszta�tuje rzeka, a jej wody "+
		"z hukiem rozbijaj� si� o wystaj�ce ponad powierzchni� kamienie. ";

if((pora_roku() == MT_WIOSNA || pora_roku() == MT_LATO) && jest_dzien())
	{	str+="Ponad szczytem le��cego na wschodzie wzg�rza "+
			"zieleniej� delikatne li�cie drzew. ";
	}
	else if(pora_roku() == MT_JESIEN && jest_dzien())
		str+="Ponad szczytem le��cego na wschodzie wzg�rza "+
		"czerwieni� si� i z�oc� jesienne szczyty drzew. ";
	else if(pora_roku() == MT_ZIMA)
	{
		if(CZY_JEST_SNIEG(TO))
			str+="Ponad szczytem le��cego na wschodzie wzg�rza "+
			"stercz� pokryte szronem ga��zie drzew ";
		else
			str+="Ponad szczytem le��cego na wschodzie wzg�rza "+
			"stercz� ogo�ocone z li�ci ga��zie drzew. ";
	
	}
	else if(!jest_dzien())
		str+="Ponad szczytem le��cego na wschodzie wzg�rza majacz� "+
		"na tle rozgwie�d�onego nieba czarne ga��zie drzew. ";



if(dzien_noc())
	{
		if(ZACHMURZENIE(TO) <= POG_ZACH_SREDNIE)
			str+="Jasna krecha biegn�cej w kierunku p�nocnym piaszczystej, "+
			"niezbyt szerokiej �cie�ki oddziela rozci�gaj�ce si� na zach�d "+
			"od niej l�ni�ce w blasku ksi�yca pola uprawne od ciemnej "+
			"zieleni lasu pokrywaj�cej obszary na wschodzie. ";
		else
			str+="Jasna krecha biegn�cej w kierunku p�nocnym piaszczystej, "+
			"niezbyt szerokiej �cie�ki oddziela rozci�gaj�ce si� na zach�d "+
			"od niej ciemne pola uprawne od lasu pokrywaj�cego obszary na "+
			"wschodzie. ";
	}
	else
	{
		if(pora_roku() == MT_ZIMA)
			str+="Jasna krecha biegn�cej w kierunku p�nocnym piaszczystej, "+
			"niezbyt szerokiej �cie�ki oddziela rozci�gaj�ce si� na zach�d "+
			"od niej poro�ni�te jasnozielon� ozimin� od ciemnej zieleni "+
			"lasu pokrywaj�cej obszary na wschodzie. ";
		else if(pora_roku() == MT_WIOSNA)
			str+="Jasna krecha biegn�cej w kierunku p�nocnym piaszczystej, "+
			"niezbyt szerokiej �cie�ki oddziela rozci�gaj�ce si� na zach�d "+
			"od niej �wie�o zaorane pola uprawne od ciemnej zieleni "+
			"lasu pokrywaj�cej obszary na wschodzie. ";
		else if(pora_roku() == MT_LATO)
			str+="Jasna krecha biegn�cej w kierunku p�nocnym piaszczystej, "+
			"niezbyt szerokiej �cie�ki oddziela rozci�gaj�ce si� na zach�d "+
			"od niej faluj�ce z�ocistym �anem dojrza�ego zbo�a od ciemnej zieleni lasu pokrywaj�cej obszary na wschodzie. ";
		else
			str+="Jasna krecha biegn�cej w kierunku p�nocnym piaszczystej, "+
			"niezbyt szerokiej �cie�ki oddziela rozci�gaj�ce si� na zach�d "+
			"od niej ciemne pola uprawne od lasu pokrywaj�cego obszary na "+
			"wschodzie. ";
	}







	str+="\n";
	return str;
}
