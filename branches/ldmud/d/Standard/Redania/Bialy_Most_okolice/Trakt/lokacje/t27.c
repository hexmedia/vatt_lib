/*
 * Opis oraz kod automatycznie wygenerowany za pomoca VattGen,
 * Data: Thursday 01st of November 2007 05:56:16 PM
 */

#include <pogoda.h>
#include <stdproperties.h>
#include <mudtime.h>
#include "dir.h"

inherit TRAKT_BIALY_MOST_STD;

void
create_trakt()
{
    set_short("Trakt w^sr^od p^ol");
    set_long("@@dlugi_opis@@");
    add_exit(TRAKT_BIALY_MOST_LOKACJE + "t26.c","s",0,FATIGUE_TRAKT,0);
    add_exit(TRAKT_BIALY_MOST_LOKACJE + "t28.c","nw",0,FATIGUE_TRAKT,0);
    add_prop(ROOM_I_INSIDE,0);

}

public string
exits_description()
{
    return "Trakt prowadzi na p^o^lnocny zach^od i po^ludnie.\n";
}

string
dlugi_opis()
{
    string str = "";

    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="Pola uprawne rozci^agaj^ace si^e dooko^la traktu, pokrywa ^s"+
             "nie^znobia^ly puch po kt^orym od czasu do czasu przebiega ja"+
             "kie^s ma^le zwierz^atko. Prawie wszystko przykryte jest spor"+
             "^a warstw^a bielutkiego ^sniegu, kt^ory przysypa^l okolic^e."+
             " Pomi^edzy kamykami po^lo^zonymi, by wzmocni^c nawierzchnie "+
             "traktu wida^c pozamarzane ka^lu^ze. ";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str+="Pola uprawne rozci^agaj^ace si^e dooko^la traktu, pokrywa de"+
             "likatna m^loda ziele^n, w kt^orej buszuj^a obudzone z zimowe"+
             "go snu zwierz^eta. Otaczaj^aca ci^e przyroda budzi sie do ^z"+
             "ycia po zimowym ^snie, a lekki wietrzyk przynosi do ciebie ^"+
             "swie^zy zapach ro^slin. Pomi^edzy kamykami po^lo^zonymi, by "+
             "wzmocni^c nawierzchnie traktu, zaczyna kie^lkowa^c szorstka "+
             "trawa i jakie^s inne pospolite zielska. ";
    }
    else if(pora_roku() == MT_LATO)
    {
        str+="Pola uprawne rozci^agaj^ace si^e dooko^la traktu, pokrywa ci"+
             "emnozielona bujna ro^slinno^s^c, w kt^orej buszuj^a obudzone"+
             " z zimowego snu zwierz^eta. Ca^la przyroda w pe^lnym rozkwic"+
             "ie, zieleni otaczaj^acy ci^e krajobraz, a w powietrzu mo^zes"+
             "z wyczu^c intensywny zapach ro^slin. Pomi^edzy kamykami po^l"+
             "o^zonymi, by wzmocni^c nawierzchnie traktu, ro^snie bujnie s"+
             "zorstka trawa i jakie^s inne pospolite zielska. ";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str+="Pola uprawne rozci^agaj^ace si^e dooko^la traktu, s^a zaoran"+
             "e i przygotowane do zimy, a od czasu do czasu mo^zna zauwa^z"+
             "y^c jak przebiega po nich jakie^s drobne zwierz^atko. Krajob"+
             "raz spowija lekka mgie^lka i dymy z ognisk palonych na polac"+
             "h. Pomi^edzy kamykami po^lo^zonymi, by wzmocni^c nawierzchni"+
             "e traktu, ro^snie trawa i jakie^s inne wysuszone, pospolite "+
             "zielska. ";
    }

    str+="\n";
    return str;
}