/*
 * Opis oraz kod automatycznie wygenerowany za pomoca VattGen,
 * Data: Friday 02nd of November 2007 11:23:09 PM
 */

#include <pogoda.h>
#include <stdproperties.h>
#include <mudtime.h>
#include "dir.h"

inherit TRAKT_BIALY_MOST_STD;

void
create_trakt()
{
    set_short("Ubity trakt");
    set_long("@@dlugi_opis@@");
    add_exit(TRAKT_BIALY_MOST_LOKACJE + "t49.c","s",0,FATIGUE_TRAKT,0);
    add_exit(TRAKT_BIALY_MOST_LOKACJE + "t51.c","n",0,FATIGUE_TRAKT,0);
    add_prop(ROOM_I_INSIDE,0);

}

public string
exits_description()
{
    return "Trakt prowadzi na p^o^lnoc i po^ludnie.\n";
}

string
dlugi_opis()
{
    string str = "";

    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="Otoczenie zamar^lo w zimowym letargu. ^Snieg le^z^acy na dro"+
             "dze jest ubity i szary, w niekt^orych miejscach pojawi^l si^"+
             "e l^od wy^slizgany tysi^acami ko^nskich kopyt, podeszwami bu"+
             "t^ow i ko^lami woz^ow, a nier^owne koleiny, ci^agn^ace si^e "+
             "po obu stronach traktu ^swiadcz^a o tym, ^ze handel jest w t"+
             "ych okolicach do^s^c ^zywy. Pola uprawne otaczaj^ace trakt, "+
             "ci^agn^ace sie prawie po horyzont, pokryte s^a gestym ^snieg"+
             "iem gdzieniegdzie tworz^acym zaspy. ";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str+="Otoczenie t^etni rozkwitaj^acym na wiosn^e ^zyciem. Ziemia j"+
             "est ubita niemal^ze na kamie^n tysi^acami ko^nskich kopyt, b"+
             "osych i obutych st^op, Nier^owne koleiny, ci^agn^ace si^e po"+
             " obu stronach traktu ^swiadcz^a o tym, ^ze handel jest w tyc"+
             "h okolicach do^s^c ^zywy. Pola uprawne otaczaj^ace trakt, ci"+
             "^agn^ace sie prawie po horyzont, pokryte s^a ju^z ^swie^zo z"+
             "asian^a, kie^lkuj^ac^a ro^slinno^sci^a.";
    }
    else if(pora_roku() == MT_LATO)
    {
        str+="Otoczenie t^etni rozkwit^lym ^zyciem. Ziemia jest ubita niem"+
             "al^ze na kamie^n tysi^acami ko^nskich kopyt, bosych i obutyc"+
             "h st^op, Nier^owne koleiny, ci^agn^ace si^e po obu stronach "+
             "traktu ^swiadcz^a o tym, ^ze handel jest w tych okolicach do"+
             "^s^c ^zywy. Pola uprawne otaczaj^ace trakt, ci^agn^ace sie p"+
             "rawie po horyzont, pokryte s^a ju^z ^z^o^ltymi k^losami zb^o"+
             "^z. ";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str+="Otoczenie powoli zamiera w przygotowaniu do zbli^zaj^acej si"+
             "^e zimy. Ziemia jest ubita niemal^ze na kamie^n tysi^acami k"+
             "o^nskich kopyt, bosych i obutych st^op, Nier^owne koleiny, c"+
             "i^agn^ace si^e po obu stronach traktu ^swiadcz^a o tym, ^ze "+
             "handel jest w tych okolicach do^s^c ^zywy. Pola uprawne otac"+
             "zaj^ace trakt, ci^agn^ace sie prawie po horyzont, s^a br^azo"+
             "we od nagiej ziemi. ";
    }

    str+="\n";
    return str;
}