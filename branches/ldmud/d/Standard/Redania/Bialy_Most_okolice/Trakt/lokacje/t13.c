
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"
#include <pogoda.h>

inherit TRAKT_BIALY_MOST_STD;

void create_trakt() 
{
    set_short("Ubity trakt");
    set_long("@@dlugasny@@");
    add_exit(TRAKT_BIALY_MOST_LOKACJE + "t12.c","sw",0,FATIGUE_TRAKT,0);
    add_exit(TRAKT_BIALY_MOST_LOKACJE + "t14.c","se",0,FATIGUE_TRAKT,0);

    add_prop(ROOM_I_INSIDE,0);
}
public string
exits_description() 
{
    return "Trakt ci�gnie si� na po�udniowy-zach�d oraz na po�udniowy-wsch�d.\n";
}

string
dlugasny()
{
	string str="";

	str+="Trakt wij�cy si� nad brzegiem rzeki jest na tyle szeroki, i� "+
	"bez najmniejszego trudu mog� wymin�� si� na nim nie tylko "+
	"podr�ni przemierzaj�cy szlaki na w�asnych nogach, ale i "+
	"tak�e wi�ksze, wy�adowane towarami wozy. ";
	
	if(!CZY_JEST_SNIEG(TO) && !CO_PADA(TO))
		str+="Wy�o�on� dopasowanymi kamieniami powierzchni� traktu "+
		"pokrywa gruba warstwa szarego py�u nie "+
		"pozwalaj�ca wypatrze� nawet najbardziej bystremu oku "+
		"licznych dziur w nawierzchni. ";
	else
		str+"Szeroka droga nosi na sobie liczne �lady cz�stego "+
		"u�ytkowania, wiele fa�d i do�k�w wy��obionych przez "+
		"rozp�dzone wozy kupieckie, �wiadcz� i� od dawna trakt ten nie "+
		"przechodzi� �adnego remontu nawierzchni. ";


	if(dzien_noc())
	{
		if(ZACHMURZENIE(TO) <= POG_ZACH_SREDNIE)
			str+="Jasna krecha biegn�cej w kierunku p�nocnym piaszczystej, "+
			"niezbyt szerokiej �cie�ki oddziela rozci�gaj�ce si� na zach�d "+
			"od niej l�ni�ce w blasku ksi�yca pola uprawne od ciemnej "+
			"zieleni lasu pokrywaj�cej obszary na wschodzie. ";
		else
			str+="Jasna krecha biegn�cej w kierunku p�nocnym piaszczystej, "+
			"niezbyt szerokiej �cie�ki oddziela rozci�gaj�ce si� na zach�d "+
			"od niej ciemne pola uprawne od lasu pokrywaj�cego obszary na "+
			"wschodzie. ";
	}
	else
	{
		if(pora_roku() == MT_ZIMA)
			str+="Jasna krecha biegn�cej w kierunku p�nocnym piaszczystej, "+
			"niezbyt szerokiej �cie�ki oddziela rozci�gaj�ce si� na zach�d "+
			"od niej poro�ni�te jasnozielon� ozimin� od ciemnej zieleni "+
			"lasu pokrywaj�cej obszary na wschodzie. ";
		else if(pora_roku() == MT_WIOSNA)
			str+="Jasna krecha biegn�cej w kierunku p�nocnym piaszczystej, "+
			"niezbyt szerokiej �cie�ki oddziela rozci�gaj�ce si� na zach�d "+
			"od niej �wie�o zaorane pola uprawne od ciemnej zieleni "+
			"lasu pokrywaj�cej obszary na wschodzie. ";
		else if(pora_roku() == MT_LATO)
			str+="Jasna krecha biegn�cej w kierunku p�nocnym piaszczystej, "+
			"niezbyt szerokiej �cie�ki oddziela rozci�gaj�ce si� na zach�d "+
			"od niej faluj�ce z�ocistym �anem dojrza�ego zbo�a od ciemnej zieleni lasu pokrywaj�cej obszary na wschodzie. ";
		else
			str+="Jasna krecha biegn�cej w kierunku p�nocnym piaszczystej, "+
			"niezbyt szerokiej �cie�ki oddziela rozci�gaj�ce si� na zach�d "+
			"od niej ciemne pola uprawne od lasu pokrywaj�cego obszary na "+
			"wschodzie. ";
	}







	str+="\n";
	return str;
}
