/*
 * Opis oraz kod automatycznie wygenerowany za pomoca VattGen,
 * Data: Friday 02nd of November 2007 09:49:27 PM
 */

#include <pogoda.h>
#include <stdproperties.h>
#include <mudtime.h>
#include "dir.h"

inherit TRAKT_BIALY_MOST_STD;

void
create_trakt()
{
    set_short("Trakt w^sr^od ^l^ak");
    set_long("@@dlugi_opis@@");
    add_exit(TRAKT_BIALY_MOST_LOKACJE + "t37.c","se",0,FATIGUE_TRAKT,0);
    add_exit(TRAKT_BIALY_MOST_LOKACJE + "t39.c","nw",0,FATIGUE_TRAKT,0);
    add_prop(ROOM_I_INSIDE,0);

}

public string
exits_description()
{
    return "Trakt prowadzi na po^lnocny zach^od i po^ludniowy wsch^od.\n";
}

string
dlugi_opis()
{
    string str = "";

    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="Znajdujesz si^e na szerokim trakcie, wybudowanym na zboczu n"+
             "iezbyt wysokiego wzg^orza, kt^ore w tym momencie jest pokryt"+
             "e bia^la pierzyn^a ze ^sniegu. Okoliczne ^l^aki pokryte s^a "+
             "bia^lym, g^estym puchem, przykrywaj^acym wszystko delikatn^a"+
             " ko^lderk^a. Tylko w najbli^zszym s^asiedztwie utwardzonej d"+
             "rogi pokryte ^sniegowym puchem ro^sliny nie potrafi^a przetr"+
             "wa^c i wybi^c si^e nad ziemi^e, a wyj^atki kt^orym to si^e u"+
             "da^lo zosta^ly zadeptane przez licznych w^edrowc^ow korzysta"+
             "j^acych z tego traktu. ";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str+="Znajdujesz si^e na szerokim trakcie, wybudowanym na zboczu n"+
             "iezbyt wysokiego wzg^orza, kt^ore w tym momencie jest pokryt"+
             "e drobn^a, jasnozielon^a traw^a. Okoliczne ^l^aki pokryte s^"+
             "a kie^lkuj^ac^a ro^slinno^sci^a, gdzieniegdzie przebijaj^ac^"+
             "a sie spod cienkich warstewek stopionego ^sniegu. Niska, pol"+
             "na ro^slinno^s^c, przerzedza si^e w miar^e przybli^zania do "+
             "go^sci^nca, za^s ziele^n rosn^aca w najbli^zszym s^asiedztwi"+
             "e drogi wydaje si^e marna i niedojrza^la, a ju^z na pewno za"+
             "deptana.";
    }
    else if(pora_roku() == MT_LATO)
    {
        str+="Znajdujesz si^e na szerokim trakcie, wybudowanym na zboczu n"+
             "iezbyt wysokiego wzg^orza, kt^ore w tym momencie jest pokryt"+
             "e g^est^a, ciemnozielon^a traw^a. Okoliczne ^l^aki pokryte s"+
             "^a bujn^a ro^slinno^sci^a, w ^sr^od kt^orej zapewne kwitnie "+
             "tak^ze ^zycie drobnych zwierz^at. Tylko w najbli^zszym s^asi"+
             "edztwie utwardzonej drogi ro^sliny nie potrafi^a przetrwa^c "+
             "i wybi^c si^e nad ziemi^e, a wyj^atki kt^orym to si^e uda^lo"+
             " zosta^ly zadeptane przez licznych w^edrowc^ow korzystaj^acy"+
             "ch z tego traktu. ";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str+="Znajdujesz si^e na szerokim trakcie, wybudowanym na zboczu n"+
             "iezbyt wysokiego wzg^orza, kt^ore w tym momencie jest pokryt"+
             "e drobn^a, wysuszon^a traw^a. Okoliczne ^l^aki pokryte s^a w"+
             "ysuszon^a ro^slinno^sci^a, kt^or^a powoli zaczyna obumiera^c"+
             ". Niska, polna ro^slinno^s^c, przerzedza si^e w miar^e przyb"+
             "li^zania do go^sci^nca, za^s ziele^n rosn^aca w najbli^zszym"+
             " s^asiedztwie drogi wydaje si^e marna i niedojrza^la, a ju^z"+
             " na pewno zadeptana. ";
    }

    str+="\n";
    return str;
}