/*
 * Ta nudna lokacja traktu jest do�� wyj�tkowa, mo�na tu co� zerwa� ;)
 * Owoce dzikiej r�y konkretnie
 * Vera, opis z banku opisywaczy...Czyj� tam ;)
 * Thu Apr 5 13:12:56 2007
 */


#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"
#include <pogoda.h>

inherit TRAKT_BIALY_MOST_STD;

int sa_owoce = 20;


string dlugasny();

void create_trakt() 
{
	set_long("@@dlugasny@@");
    set_short("Trakt przy zaro�lach");
    add_exit(TRAKT_BIALY_MOST_LOKACJE + "t06.c","nw",0,FATIGUE_TRAKT,0);
    add_exit(TRAKT_BIALY_MOST_LOKACJE + "t08.c","ne",0,FATIGUE_TRAKT,0);

    add_prop(ROOM_I_INSIDE,0);
    
    set_alarm_every_hour("dodaj_owoce");
    
    add_item(({"zaro�la","kolczaste zaro�la"}),
    "Po przyjrzeniu si� im, stwierdzasz, �e to nic innego jak krzaki dzikiej "+
    "r�y.\n");
    add_item(({"krzaki","krzaki dzikiej r�y","dzik� r��","krzak"}),
    "@@opis_krzaczka@@");
}

string
opis_krzaczka()
{
	string str;
	//krzak r�y
	str="Krzak jest stosunkowo niewysoki, �redniego wzrostu "+
	"m�czyznie m�g�by si�ga� mniej wi�cej do pasa. Z kolczastych "+
	"br�zowawych ga��zek wyrastaj� sk�rzaste, soczy�cie zielone "+
	"listki, z zewn�trz nieco b�yszcz�ce. Jakby dla kontrastu "+
	"niekt�re ga��zki zako�czone s� kwiatostanami kilku jasnor�owych "+
	"kwiatk�w, im bli�ej �rodka, tym coraz bardziej zbli�aj�cych si� "+
	"odcieniem do bieli. Z samego �rodeczka wyrastaj� delitkane "+
	"��tawe pr�ciki, zako�czone male�kimi kuleczkami, na kt�rych "+
	"zgromadzony jest py�ek kwiatowy.";
	
	if(sa_owoce)
	str+=" Niekt�re ga��zie uginaj� si� �ukowato do ziemi, ledwie "+
	"d�wigaj�c niedu�e, ale do�� ci�kie czerwono-pomara�czowawe, kuliste "+
	"owoce.";

	str+="\n";
	return str;
}

int
dodaj_owoce()
{
	switch(random(5))
	{
		case 0: sa_owoce=random(20); break;
		default: break;
	}

	return 1;
}

public string
exits_description() 
{
    return "Trakt ci�gnie si� na p�nocny-zach�d oraz na p�nocny-wsch�d.\n";
}

string
dlugasny()
{
	string str;
	if(CZY_JEST_SNIEG(TO))
		str="Szlak skr�ca lekko na po�udnie, gdy� na jego drodze "+
	"stan�y g�ste, obsypane p�atkami �niegu, kolczaste zaro�la. "+
	"Spl�tane ga��zie i d�ugie kolce owych krzak�w skutecznie utrudniaj� "+
	"przepraw� przez nie, a ka�da pr�ba sforsowania tej masywnej "+
	"�ciany zako�czy�aby si� jedynie licznymi zadrapaniami i "+
	"powbijanymi w sk�r� kolcami. Zaro�la musia�y zapu�ci� w tym miejscu "+
	"swoje p�dy ju� do�� dawno, gdy� wystaj�ce ponad powierzchni� "+
	"zalegaj�cego na ziemi bia�ego puchu kamienie wyznaczaj�ce trakt "+
	"na jego ca�ej d�ugo�ci zosta�y przez budowniczych u�o�one tak, "+
	"aby omin�� owe nieprzychylne dla podr�nych miejsce. Ubity "+
	"�nieg le��cy na drodze i liczne �lady po ko�ach p�dz�cych t�dy "+
	"woz�w swiadcz� o cz�stym u�ywaniu tego traktu.\n";
	else
		str="Szlak skr�ca lekko na po�udnie, gdy� na jego drodze "+
	"stan�y g�ste, kolczaste zaro�la. Spl�tane ga��zie i d�ugie kolce "+
	"owych krzak�w skutecznie utrudniaj� przepraw� przez nie, a ka�da "+
	"pr�ba sforsowania tej masywnej �ciany zako�czy�aby si� jedynie "+
	"licznymi zadrapaniami i powbijanymi w sk�r� kolcami. Zaro�la "+
	"musia�y zapu�ci� w tym miejscu swoje p�dy ju� do�� dawno, gdy� "+
	"u�o�one na pod�o�u g�adkie kamienie wyznaczaj�ce trakt na "+
	"jego ca�ej d�ugo�ci zosta�y przez budowniczych u�o�one tak, aby "+
	"omin�� owe nieprzychylne dla podr�nych miejsce. Szeroka droga "+
	"nosi na sobie liczne �lady cz�stego u�ytkowania, wiele fa�d i "+
	"do�k�w wy��obionych przez rozp�dzone wozy kupieckie, �wiadcz� "+
	"i� od dawna trakt ten nie przechodzi� �adnego remontu nawierzchni.\n";
	
	return str;
}

int
zerwij(string str)
{
	notify_fail("Zerwij co?\n");
	
	if(str ~= "owoc" || str~="owoc z krzewu" || str~="owoc z zaro�li")
	{
		if(sa_owoce)
		{
			clone_object(TRAKT_BIALY_MOST_DZIKA_ROZA)->move(TP);
			sa_owoce--;
			write("Zrywasz owoc z krzewu.\n");
			saybb(QCIMIE(TP,PL_MIA)+" zrywa jaki� owoc z krzewu.\n");
			
			return 1;
		}
		else
		{
			write("Na krzaku nie ma ju� �adnych owoc�w.\n");
			return 1;
		}
	}
	return 0;
}

init()
{
	::init();
	add_action(zerwij,"zerwij");
}
