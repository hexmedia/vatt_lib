/*
 * Opis oraz kod automatycznie wygenerowany za pomoca VattGen,
 * Data: Friday 02nd of November 2007 09:49:27 PM
 */

#include <pogoda.h>
#include <stdproperties.h>
#include <mudtime.h>
#include "dir.h"

inherit TRAKT_BIALY_MOST_STD;

void
create_trakt()
{
    set_short("Trakt w^sr^od ^l^ak");
    set_long("@@dlugi_opis@@");
    add_exit(TRAKT_BIALY_MOST_LOKACJE + "t36.c","e",0,FATIGUE_TRAKT,0);
    add_exit(TRAKT_BIALY_MOST_LOKACJE + "t38.c","nw",0,FATIGUE_TRAKT,0);
    add_prop(ROOM_I_INSIDE,0);

}

public string
exits_description()
{
    return "Trakt prowadzi na p^o^lnocny zach^od i wsch^od.\n";
}

string
dlugi_opis()
{
    string str = "";

    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="Po obu stronach drogi rosn^a krzaki, teraz pozbawione li^sci"+
             " i przykryte grub^a warstw^a ^sniegu. Tylko w najbli^zszym s"+
             "^asiedztwie utwardzonej drogi pokryte ^sniegowym puchem ro^s"+
             "liny nie potrafi^a przetrwa^c i wybi^c si^e nad ziemi^e, a w"+
             "yj^atki kt^orym to si^e uda^lo zosta^ly zadeptane przez licz"+
             "nych w^edrowc^ow korzystaj^acych z tego traktu. Na szcz^e^sc"+
             "ie teraz b^loto normalnie rozci^agaj^ace si^e na ca^lej szer"+
             "oko^sci traktu jest zamarzni^ete, jednak latem przebrni^ecie"+
             " przez nie stanowi^c musi nie lada wyzwanie dla ci^e^zkich, "+
             "za^ladowanych po brzegi woz^ow kupieckich. ";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str+="Po obu stronach drogi g^esto rosn^a rozkwitaj^ace krzaki. Ni"+
             "ska, polna ro^slinno^s^c, przerzedza si^e w miar^e przybli^z"+
             "ania do go^sci^nca, za^s ziele^n rosn^aca w najbli^zszym s^a"+
             "siedztwie drogi wydaje si^e marna i niedojrza^la, a ju^z na "+
             "pewno zadeptana. G^l^ebokie b^loto rozdeptane przez licznych"+
             " podr^o^znych rozci^aga si^e na ca^lej szeroko^sci traktu, a"+
             " przebrni^ecie przez nie stanowi^c musi nie lada wyzwanie dl"+
             "a ci^e^zkich, za^ladowanych po brzegi woz^ow kupieckich, tak"+
             "^ze i piechur musi uwa^za^c, ^zeby nie wpa^s^c w g^l^ebokie "+
             "ka^lu^ze. ";
    }
    else if(pora_roku() == MT_LATO)
    {
        str+="Po obu stronach drogi rosn^a w pe^lni rozwini^ete, g^este kr"+
             "zaki. Tylko w najbli^zszym s^asiedztwie utwardzonej drogi ro"+
             "^sliny nie potrafi^a przetrwa^c i wybi^c si^e nad ziemi^e, a"+
             " wyj^atki kt^orym to si^e uda^lo zosta^ly zadeptane przez li"+
             "cznych w^edrowc^ow korzystaj^acych z tego traktu. G^l^ebokie"+
             " b^loto rozdeptane przez licznych podr^o^znych rozci^aga si^"+
             "e na ca^lej szeroko^sci traktu, a przebrni^ecie przez nie st"+
             "anowi^c musi nie lada wyzwanie dla ci^e^zkich, za^ladowanych"+
             " po brzegi woz^ow kupieckich, tak^ze i piechur musi uwa^za^c"+
             ", ^zeby nie wpa^s^c w g^l^ebokie ka^lu^ze. ";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str+="Po obu stronach drogi rosn^a zielone krzaki o jeszcze niezwi"+
             "^edni^etych li^sciach. Niska, polna ro^slinno^s^c, przerzedz"+
             "a si^e w miar^e przybli^zania do go^sci^nca, za^s ziele^n ro"+
             "sn^aca w najbli^zszym s^asiedztwie drogi wydaje si^e marna i"+
             " niedojrza^la, a ju^z na pewno zadeptana. G^l^ebokie b^loto "+
             "rozdeptane przez licznych podr^o^znych rozci^aga si^e na ca^"+
             "lej szeroko^sci traktu, a przebrni^ecie przez nie stanowi^c "+
             "musi nie lada wyzwanie dla ci^e^zkich, za^ladowanych po brze"+
             "gi woz^ow kupieckich, tak^ze i piechur musi uwa^za^c, ^zeby "+
             "nie wpa^s^c w g^l^ebokie ka^lu^ze. ";
    }

    str+="\n";
    return str;
}