/*
 * Opis oraz kod automatycznie wygenerowany za pomoca VattGen,
 * Data: Saturday 16th of June 2007 06:45:41 PM
 */

#include <pogoda.h>
#include <stdproperties.h>
#include <mudtime.h>
#include "dir.h"

inherit TRAKT_BIALY_MOST_STD;

void
create_trakt()
{
    set_short("Trakt w^sr^od p^ol");
    set_long("@@dlugi_opis@@");
    add_exit(TRAKT_BIALY_MOST_LOKACJE + "t21.c","s",0,FATIGUE_TRAKT,0);
    add_exit(TRAKT_BIALY_MOST_LOKACJE + "t23.c","w",0,FATIGUE_TRAKT,0);
    add_prop(ROOM_I_INSIDE,0);

}

public string
exits_description()
{
    return "Trakt prowadzi na po^ludnie i zach^od.\n";
}

string
dlugi_opis()
{
    string str = "";

    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="^L^aki i pola uprawne, pokryte srebrzystobia^lym puchem, wyg"+
             "l^adaj^a na opuszczone przez prawie wszelak^a zwierzyn^e. Po"+
             "la uprawne rozci^agaj^ace si^e dooko^la traktu, pokrywa ^sni"+
             "e^znobia^ly puch po kt^orym od czasu do czasu przebiega jaki"+
             "e^s ma^le zwierz^atko. Trakt wij^acy si^e mi^edzy ma^lymi pa"+
             "g^orkami, przecina w tym miejscu pola uprawne przykryte grub"+
             "^a pokryw^a ^snie^zna, na kt^orych gdzieniegdzie mo^zna zauw"+
             "a^zy^c ^slady drobnej zwierzyny. ";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str+="^L^aki i pola uprawne, pokryte kie^lkuj^aca ro^slinno^sci^a,"+
             " zaczynaj^a zape^lnia^c si^e budz^ac^a si^e do ^zycia faun^a"+
             ". Pola uprawne rozci^agaj^ace si^e dooko^la traktu, pokrywa "+
             "delikatna m^loda ziele^n, w kt^orej buszuj^a obudzone z zimo"+
             "wego snu zwierz^eta. Trakt wij^acy si^e mi^edzy ma^lymi pag^"+
             "orkami, przecina w tym miejscu lekko ju^z zazielenione pola "+
             "uprawne, dostarczaj^ace okolicznym mieszka^ncom po^zywienia."+
             " ";
    }
    else if(pora_roku() == MT_LATO)
    {
        str+="^L^aki i pola uprawne, pokryte bujnymi plonami, s^a wype^lni"+
             "one d^xwi^ekami przer^o^znych zwierz^at. Pola uprawne rozci^"+
             "agaj^ace si^e dooko^la traktu, pokrywa ciemnozielona bujna r"+
             "o^slinno^s^c, w kt^orej buszuj^a obudzone z zimowego snu zwi"+
             "erz^eta. Trakt wij^acy si^e mi^edzy ma^lymi pag^orkami, prze"+
             "cina w tym miejscu g^esto obsiane pola uprawne, dostarczaj^a"+
             "ce okolicznym mieszka^ncom po^zywienia. ";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str+="^L^aki i pola uprawne, ogo^locone z ro^slinno^sci, powoli ci"+
             "chn^a wraz ze zwierz^etami k^lad^acymi si^e do zimowego snu."+
             " Pola uprawne rozci^agaj^ace si^e dooko^la traktu, s^a zaora"+
             "ne i przygotowane do zimy, a od czasu do czasu mo^zna zauwa^"+
             "zy^c jak przebiega po nich jakie^s drobne zwierz^atko. Trakt"+
             " wij^acy si^e mi^edzy ma^lymi pag^orkami, przecina w tym mie"+
             "jscu przygotowane ju^z do zimy pola uprawne, dostarczaj^ace "+
             "okolicznym mieszka^ncom po^zywienia. ";
    }

    str+="\n";
    return str;
}