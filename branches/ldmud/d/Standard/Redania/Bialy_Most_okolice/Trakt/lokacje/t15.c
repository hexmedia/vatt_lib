
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"
#include <pogoda.h>

inherit TRAKT_BIALY_MOST_STD;

void create_trakt() 
{
    set_short("Trakt nad brzegiem Pontaru");
    set_long("@@dlugasny@@");
    add_exit(TRAKT_BIALY_MOST_LOKACJE + "t16.c","ne",0,FATIGUE_TRAKT,0);
    add_exit(TRAKT_BIALY_MOST_LOKACJE + "t14.c","w",0,FATIGUE_TRAKT,0);

    add_prop(ROOM_I_INSIDE,0);
}
public string
exits_description() 
{
    return "Trakt prowadzi na p�nocny-wsch�d i na zach�d.\n";
}

string
dlugasny()
{
	string str="";

	str+="Trakt wij�cy si� nad brzegiem rzeki jest na tyle szeroki, i� "+
	"bez najmniejszego trudu mog� wymin�� si� na nim nie tylko "+
	"podr�ni przemierzaj�cy szlaki na w�asnych nogach, ale i "+
	"tak�e wi�ksze, wy�adowane towarami wozy. ";

	if(!CZY_JEST_SNIEG(TO) && !CO_PADA(TO))
		str+="Wy�o�on� dopasowanymi kamieniami powierzchni� traktu "+
		"pokrywa gruba warstwa szarego py�u nie "+
		"pozwalaj�ca wypatrze� nawet najbardziej bystremu oku "+
		"licznych dziur w nawierzchni. ";


	if((pora_roku() == MT_WIOSNA || pora_roku() == MT_LATO) && jest_dzien())
	{	str+="Ponad szczytem le��cego na wschodzie wzg�rza "+
			"zieleniej� delikatne li�cie drzew. ";
	}
	else if(pora_roku() == MT_JESIEN && jest_dzien())
		str+="Ponad szczytem le��cego na wschodzie wzg�rza "+
		"czerwieni� si� i z�oc� jesienne szczyty drzew. ";
	else if(pora_roku() == MT_ZIMA)
	{
		if(CZY_JEST_SNIEG(TO))
			str+="Ponad szczytem le��cego na wschodzie wzg�rza "+
			"stercz� pokryte szronem ga��zie drzew ";
		else
			str+="Ponad szczytem le��cego na wschodzie wzg�rza "+
			"stercz� ogo�ocone z li�ci ga��zie drzew. ";
	
	}
	else if(!jest_dzien())
		str+="Ponad szczytem le��cego na wschodzie wzg�rza majacz� "+
		"na tle rozgwie�d�onego nieba czarne ga��zie drzew. ";



	if(dzien_noc())
	{
		if(ZACHMURZENIE(TO) <= POG_ZACH_SREDNIE)
			str+="Jasna krecha biegn�cej w kierunku p�nocnym piaszczystej, "+
			"niezbyt szerokiej �cie�ki oddziela rozci�gaj�ce si� na zach�d "+
			"od niej l�ni�ce w blasku ksi�yca pola uprawne od ciemnej "+
			"zieleni lasu pokrywaj�cej obszary na wschodzie. ";
		else
			str+="Jasna krecha biegn�cej w kierunku p�nocnym piaszczystej, "+
			"niezbyt szerokiej �cie�ki oddziela rozci�gaj�ce si� na zach�d "+
			"od niej ciemne pola uprawne od lasu pokrywaj�cego obszary na "+
			"wschodzie. ";
	}
	else
	{
		if(pora_roku() == MT_ZIMA)
			str+="Jasna krecha biegn�cej w kierunku p�nocnym piaszczystej, "+
			"niezbyt szerokiej �cie�ki oddziela rozci�gaj�ce si� na zach�d "+
			"od niej poro�ni�te jasnozielon� ozimin� od ciemnej zieleni "+
			"lasu pokrywaj�cej obszary na wschodzie. ";
		else if(pora_roku() == MT_WIOSNA)
			str+="Jasna krecha biegn�cej w kierunku p�nocnym piaszczystej, "+
			"niezbyt szerokiej �cie�ki oddziela rozci�gaj�ce si� na zach�d "+
			"od niej �wie�o zaorane pola uprawne od ciemnej zieleni "+
			"lasu pokrywaj�cej obszary na wschodzie. ";
		else if(pora_roku() == MT_LATO)
			str+="Jasna krecha biegn�cej w kierunku p�nocnym piaszczystej, "+
			"niezbyt szerokiej �cie�ki oddziela rozci�gaj�ce si� na zach�d "+
			"od niej faluj�ce z�ocistym �anem dojrza�ego zbo�a od ciemnej zieleni lasu pokrywaj�cej obszary na wschodzie. ";
		else
			str+="Jasna krecha biegn�cej w kierunku p�nocnym piaszczystej, "+
			"niezbyt szerokiej �cie�ki oddziela rozci�gaj�ce si� na zach�d "+
			"od niej ciemne pola uprawne od lasu pokrywaj�cego obszary na "+
			"wschodzie. ";
	}

	
	if(MOC_WIATRU(TO) > POG_WI_SREDNIE)
		str+="Na po�udniu wyra�nie widoczne s� p�yn�ce wody "+
		"Pontaru, kt�re z szumem rozbijaj� si� o wystaj�ce "+
		"ponad tafl� wody kamienie. ";
	else
		str+="Na po�udniu wyra�nie widoczne s� leniwie p�yn�ce wody "+
		"Pontaru, kt�re z cichym szumem rozbijaj� si� o wystaj�ce "+
		"ponad tafl� wody kamienie. ";



	str+="\n";
	return str;
}
