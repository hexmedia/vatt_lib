/*
 * Opis oraz kod automatycznie wygenerowany za pomoca VattGen,
 * Data: Saturday 16th of June 2007 06:45:41 PM
 */

#include <pogoda.h>
#include <stdproperties.h>
#include <mudtime.h>
#include "dir.h"

inherit TRAKT_BIALY_MOST_STD;

void
create_trakt()
{
    set_short("Trakt w^sr^od p^ol");
    set_long("@@dlugi_opis@@");
    add_exit(TRAKT_BIALY_MOST_LOKACJE + "t20.c","se",0,FATIGUE_TRAKT,0);
    add_exit(TRAKT_BIALY_MOST_LOKACJE + "t22.c","n",0,FATIGUE_TRAKT,0);
    add_prop(ROOM_I_INSIDE,0);

}

public string
exits_description()
{
    return "Trakt prowadzi na po^ludniowy-wsch^od i p^o^lnoc.\n";
}

string
dlugi_opis()
{
    string str = "";

    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="Tylko w najbli^zszym s^asiedztwie utwardzonej drogi pokryte "+
             "^sniegowym puchem ro^sliny nie potrafi^a przetrwa^c i wybi^c"+
             " si^e nad ziemi^e, a wyj^atki kt^orym to si^e uda^lo zosta^l"+
             "y zadeptane przez licznych w^edrowc^ow korzystaj^acych z teg"+
             "o traktu. Szeroki, zas^lany przez ^sniegowy puch oraz brudne"+
             " b^locko trakt ci^agnie si^e ^lagodnymi ^lukami. Ca^la okoli"+
             "ca usiana jest du^zymi kwadratami p^ol uprawnych oraz ^l^ak,"+
             " wykorzystywanych przez okolicznych mieszka^nc^ow. Trakt jes"+
             "t wy^z^lobiony ko^lami pojazd^ow, wida^c na nim tak^ze ^slad"+
             "y kopyt i podk^ow. ";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str+="Niska, polna ro^slinno^s^c, przerzedza si^e w miar^e przybli"+
             "^zania do go^sci^nca, za^s ziele^n rosn^aca w najbli^zszym s"+
             "^asiedztwie drogi wydaje si^e marna i niedojrza^la, a ju^z n"+
             "a pewno zadeptana. Szeroki, zas^lany z rzadka drobnymi kamyk"+
             "ami trakt ci^agnie si^e ^lagodnymi ^lukami. Ca^la okolica us"+
             "iana jest du^zymi kwadratami p^ol uprawnych oraz ^l^ak, wyko"+
             "rzystywanych przez okolicznych mieszka^nc^ow. Trakt jest wy^"+
             "z^lobiony ko^lami pojazd^ow, wida^c na nim tak^ze ^slady kop"+
             "yt i podk^ow. ";
    }
    else if(pora_roku() == MT_LATO)
    {
        str+="Tylko w najbli^zszym s^asiedztwie utwardzonej drogi ro^sliny"+
             " nie potrafi^a przetrwa^c i wybi^c si^e nad ziemi^e, a wyj^a"+
             "tki kt^orym to si^e uda^lo zosta^ly zadeptane przez licznych"+
             " w^edrowc^ow korzystaj^acych z tego traktu. Szeroki, zas^lan"+
             "y z rzadka drobnymi kamykami trakt ci^agnie si^e ^lagodnymi "+
             "^lukami. Ca^la okolica usiana jest du^zymi kwadratami p^ol u"+
             "prawnych oraz ^l^ak, wykorzystywanych przez okolicznych mies"+
             "zka^nc^ow. Trakt jest wy^z^lobiony ko^lami pojazd^ow, wida^c"+
             " na nim tak^ze ^slady kopyt i podk^ow. ";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str+="Niska, polna ro^slinno^s^c, przerzedza si^e w miar^e przybli"+
             "^zania do go^sci^nca, za^s ziele^n rosn^aca w najbli^zszym s"+
             "^asiedztwie drogi wydaje si^e marna i niedojrza^la, a ju^z n"+
             "a pewno zadeptana. Szeroki, zas^lany z rzadka drobnymi kamyk"+
             "ami trakt ci^agnie si^e ^lagodnymi ^lukami. Ca^la okolica us"+
             "iana jest du^zymi kwadratami p^ol uprawnych oraz ^l^ak, wyko"+
             "rzystywanych przez okolicznych mieszka^nc^ow. Trakt jest wy^"+
             "z^lobiony ko^lami pojazd^ow, wida^c na nim tak^ze ^slady kop"+
             "yt i podk^ow. ";
    }

    str+="\n";
    return str;
}