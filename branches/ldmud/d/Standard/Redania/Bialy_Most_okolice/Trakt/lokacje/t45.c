/*
 * Opis oraz kod automatycznie wygenerowany za pomoca VattGen,
 * Data: Friday 02nd of November 2007 11:12:08 PM
 */

#include <pogoda.h>
#include <stdproperties.h>
#include <mudtime.h>
#include "dir.h"

inherit TRAKT_BIALY_MOST_STD;

void
create_trakt()
{
    set_short("Ubity trakt");
    set_long("@@dlugi_opis@@");
    add_exit(TRAKT_BIALY_MOST_LOKACJE + "t44.c","se",0,FATIGUE_TRAKT,0);
    add_exit(TRAKT_BIALY_MOST_LOKACJE + "t46.c","nw",0,FATIGUE_TRAKT,0);
    add_prop(ROOM_I_INSIDE,0);

}

public string
exits_description()
{
    return "Trakt prowadzi na p^o^lnocny zach^od i po^ludniowy wsch^od.\n";
}

string
dlugi_opis()
{
    string str = "";

    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="^Snieg le^z^acy na drodze jest ubity i szary, w niekt^orych "+
             "miejscach pojawi^l si^e l^od wy^slizgany tysi^acami ko^nskic"+
             "h kopyt, podeszwami but^ow i ko^lami woz^ow, a nier^owne kol"+
             "einy, ci^agn^ace si^e po obu stronach traktu ^swiadcz^a o ty"+
             "m, ^ze handel jest w tych okolicach do^s^c ^zywy. Szeroki, u"+
             "bity trakt pokrywa gruba warstwa puszystego ^sniegu. Pod ^sn"+
             "iegiem widniej^a zarysy korzeni, kt^ore wyci^agn^e^ly si^e a"+
             "^z na drog^e. Okoliczne ^l^aki pokryte s^a bia^lym, g^estym "+
             "puchem, przykrywaj^acym wszystko delikatn^a ko^lderk^a. ";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str+="Ziemia jest ubita niemal^ze na kamie^n tysi^acami ko^nskich "+
             "kopyt, bosych i obutych st^op, Nier^owne koleiny, ci^agn^ace"+
             " si^e po obu stronach traktu ^swiadcz^a o tym, ^ze handel je"+
             "st w tych okolicach do^s^c ^zywy. Szeroki, ubity trakt poro^"+
             "sni^ety jest gdzieniegdzie wznosz^acymi si^e ro^slinami. Mi^"+
             "edzy zieleniej^ac^a si^e traw^a widniej^a korzenie, kt^ore w"+
             "yci^agn^e^ly si^e a^z na drog^e. Okoliczne ^l^aki pokryte s^"+
             "a kie^lkuj^ac^a ro^slinno^sci^a, gdzieniegdzie przebijaj^ac^"+
             "a sie spod cienkich warstewek stopionego ^sniegu. ";
    }
    else if(pora_roku() == MT_LATO)
    {
        str+="Ziemia jest ubita niemal^ze na kamie^n tysi^acami ko^nskich "+
             "kopyt, bosych i obutych st^op, Nier^owne koleiny, ci^agn^ace"+
             " si^e po obu stronach traktu ^swiadcz^a o tym, ^ze handel je"+
             "st w tych okolicach do^s^c ^zywy. Szeroki, ubity trakt poras"+
             "ta miejscami soczysta, zielona trawa. Mi^edzy wybuja^l^a tra"+
             "w^a widniej^a korzenie, kt^ore wyci^agn^e^ly si^e a^z na dro"+
             "g^e. Okoliczne ^l^aki pokryte s^a bujn^a ro^slinno^sci^a, w "+
             "^sr^od kt^orej zapewne kwitnie tak^ze ^zycie drobnych zwierz"+
             "^at. ";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str+="Ziemia jest ubita niemal^ze na kamie^n tysi^acami ko^nskich "+
             "kopyt, bosych i obutych st^op, Nier^owne koleiny, ci^agn^ace"+
             " si^e po obu stronach traktu ^swiadcz^a o tym, ^ze handel je"+
             "st w tych okolicach do^s^c ^zywy. Szeroki, ubity trakt pokry"+
             "ty jest stert^a r^o^znobarwnych li^sci. Mi^edzy opadni^etymi"+
             " li^s^cmi widniej^a zarysy korzeni, kt^ore wyci^agn^e^ly si^"+
             "e a^z na drog^e. Okoliczne ^l^aki pokryte s^a wysuszon^a ro^"+
             "slinno^sci^a, kt^or^a powoli zaczyna obumiera^c. ";
    }

    str+="\n";
    return str;
}