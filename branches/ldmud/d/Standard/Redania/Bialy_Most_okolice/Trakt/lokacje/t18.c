
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"
#include <pogoda.h>

inherit TRAKT_BIALY_MOST_STD;

void create_trakt() 
{
    set_short("Trakt nad brzegiem Pontaru");
    set_long("@@dlugasny@@");
    add_exit(TRAKT_BIALY_MOST_LOKACJE + "s05.c","nw",0,FATIGUE_SCIEZKA,0);
    add_exit(TRAKT_BIALY_MOST_LOKACJE + "t17.c","sw",0,FATIGUE_TRAKT,0);
    add_exit(TRAKT_RINDE_LOKACJE + "trakt35.c","e",0,FATIGUE_TRAKT,0);

    add_prop(ROOM_I_INSIDE,0);
}

public string
exits_description() 
{
    return "Trakt prowadzi na wsch�d oraz na po^ludniowy-zach^od, za� "+
    		"na p�nocnym zachodzie ci�gnie si� udeptana �cie�ka.\n";
}


string
dlugasny()
{
	string str="";

	if(CZY_JEST_SNIEG(TO))
	{
		if(jest_dzien())
		str+="�agodnie opadaj�cy ze szczytu niewysokiego pag�rka "+
		"trakt wystarczaj�co szeroki aby wymin�y si� na nim dwa "+
		"wy�adowane kupieckie wozy prowadzi �agodnymi zakr�tami ku "+
		"widocznym na zachodzie dymom unosz�cym si� nad "+
		"zasypanymi przez �nieg zabudowaniami. ";
		else
		str+="�agodnie opadaj�cy ze szczytu niewysokiego pag�rka "+
		"trakt wystarczaj�co szeroki aby wymin�y si� na nim dwa "+
		"wy�adowane kupieckie wozy prowadzi �agodnymi zakr�tami ku "+
		"ukrytemu w ciemno�ci zachodowi. ";
	}
	else
	{
	 if(ZACHMURZENIE(TO) <= POG_ZACH_LEKKIE)
	 {
		if(jest_dzien() && pora_dnia() <= MT_POPOLUDNIE)
			str+="�agodnie opadaj�cy ze szczytu niewysokiego pag�rka trakt "+
			"wystarczaj�co szeroki aby wymin�y si� na nim dwa wy�adowane "+
			"kupieckie wozy prowadzi �agodnymi zakr�tami ku widocznym w "+
			"promieniach s�o�ca na zachodzie zabudowaniom. ";
		else if(pora_dnia() == MT_WIECZOR)
			str+="�agodnie opadaj�cy ze szczytu niewysokiego pag�rka trakt "+
			"wystarczaj�co szeroki aby wymin�y si� na nim dwa wy�adowane "+
			"kupieckie wozy prowadzi �agodnymi zakr�tami ku p�on�cych "+
			"czerwieni� w �unie zachodu zabudowaniom. ";
		else
			str+=
			"�agodnie opadaj�cy ze szczytu niewysokiego pag�rka trakt "+
			"wystarczaj�co szeroki aby wymin�y si� na nim dwa wy�adowane "+
			"kupieckie wozy prowadzi �agodnymi zakr�tami ku majacz�cym na "+
			"zachodzie zabudowaniom. ";
	 }
	 else if(jest_dzien())	
		str+=
		"�agodnie opadaj�cy ze szczytu niewysokiego pag�rka trakt "+
		"wystarczaj�co szeroki aby wymin�y si� na nim dwa wy�adowane "+
		"kupieckie wozy prowadzi �agodnymi zakr�tami ku majacz�cym na "+
		"zachodzie zabudowaniom. ";
	 else 
		str+="�agodnie opadaj�cy ze szczytu niewysokiego pag�rka trakt "+
		"wystarczaj�co szeroki aby wymin�y si� na nim dwa wy�adowane "+
		"kupieckie wozy prowadzi �agodnymi zakr�tami ku ukrytemu w "+
		"ciemno�ci zachodowi. ";
	}

	if(MOC_WIATRU(TO) > POG_WI_SREDNIE)
		str+="Na po�udniu wyra�nie widoczne s� p�yn�ce wody "+
		"Pontaru, kt�re z szumem rozbijaj� si� o wystaj�ce "+
		"ponad tafl� wody kamienie. ";
	else
		str+="Na po�udniu wyra�nie widoczne s� leniwie p�yn�ce wody "+
		"Pontaru, kt�re z cichym szumem rozbijaj� si� o wystaj�ce "+
		"ponad tafl� wody kamienie. ";


	str+="\n";
	return str;
}
