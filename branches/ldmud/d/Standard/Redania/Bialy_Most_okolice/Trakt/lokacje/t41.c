/*
 * Opis oraz kod automatycznie wygenerowany za pomoca VattGen,
 * Data: Friday 02nd of November 2007 10:27:51 PM
 */

#include <pogoda.h>
#include <stdproperties.h>
#include <mudtime.h>
#include "dir.h"

inherit TRAKT_BIALY_MOST_STD;

void
create_trakt()
{
    set_short("Trakt w^sr^od ^l^ak");
    set_long("@@dlugi_opis@@");
    add_exit(TRAKT_BIALY_MOST_LOKACJE + "t42.c","n",0,FATIGUE_TRAKT,0);
    add_exit(TRAKT_BIALY_MOST_LOKACJE + "t40.c","sw",0,FATIGUE_TRAKT,0);
    add_prop(ROOM_I_INSIDE,0);

}

public string
exits_description()
{
    return "Trakt prowadzi na p^o^lnoc i po^ludniowy zach^od.\n";
}

string
dlugi_opis()
{
    string str = "";

    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="Trakt zmierza ^lagodnym spadem w d^o^l, po^sr^od pag^ork^ow "+
             "pokrytych sk^apana w bieli ^l^ak^a, by dalej rozpocz^a^c sw^"+
             "a m^ecz^ac^a w^edr^owk^e ostrym podej^sciem pod g^or^e i zni"+
             "kn^a^c za du^z^a k^ep^a zieleni znajduj^acej si^e na szczyci"+
             "e wzniesienia. Zaro^sla ci^agn^ace si^e wzd^lu^z drogi zupe^"+
             "lnie ogo^loci^ly si^e z li^sci. Pomi^edzy kamykami po^lo^zon"+
             "ymi, by wzmocni^c nawierzchnie traktu wida^c pozamarzane ka^"+
             "lu^ze. Ca^la okolica jest pokryta ^l^akami i nigdzie nie wid"+
             "a^c ^sladu po ludzkich osadach a jedynymi ^zywymi osobami ja"+
             "kie napotykasz s^a podr^o^zuj^acy ze swym towarem kupcy. ";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str+="Trakt zmierza ^lagodnym spadem w d^o^l, po^sr^od pag^ork^ow "+
             "pokrytych jasnozielon^a ^l^ak^a, by dalej rozpocz^a^c sw^a m"+
             "^ecz^ac^a w^edr^owk^e ostrym podej^sciem pod g^or^e i znikn^"+
             "a^c za du^z^a k^ep^a zieleni znajduj^acej si^e na szczycie w"+
             "zniesienia. G^este zaro^sla ci^agn^ace wzd^lu^z drogi zielen"+
             "i^a si^e soczy^scie, przyci^agaj^ac wielobarwnymi kwiatami r"+
             "oje pszcz^o^l i trzmieli. Pomi^edzy kamykami po^lo^zonymi, b"+
             "y wzmocni^c nawierzchnie traktu, zaczyna kie^lkowa^c szorstk"+
             "a trawa i jakie^s inne pospolite zielska. Ca^la okolica jest"+
             " pokryta ^l^akami i nigdzie nie wida^c ^sladu po ludzkich os"+
             "adach a jedynymi ^zywymi osobami jakie napotykasz s^a podr^o"+
             "^zuj^acy ze swym towarem kupcy. ";
    }
    else if(pora_roku() == MT_LATO)
    {
        str+="Trakt zmierza ^lagodnym spadem w d^o^l, po^sr^od pag^ork^ow "+
             "pokrytych soczysto-zielon^a ^l^ak^a, by dalej rozpocz^a^c sw"+
             "^a m^ecz^ac^a w^edr^owk^e ostrym podej^sciem pod g^or^e i zn"+
             "ikn^a^c za du^z^a k^ep^a zieleni znajduj^acej si^e na szczyc"+
             "ie wzniesienia. G^este zaro^sla ci^agn^ace wzd^lu^z drogi zi"+
             "eleni^a si^e soczy^scie, goszcz^ac wszelkiego rodzaju owady "+
             "i zwierzyn^e. Pomi^edzy kamykami po^lo^zonymi, by wzmocni^c "+
             "nawierzchnie traktu, ro^snie bujnie szorstka trawa i jakie^s"+
             " inne pospolite zielska. Ca^la okolica jest pokryta ^l^akami"+
             " i nigdzie nie wida^c ^sladu po ludzkich osadach a jedynymi "+
             "^zywymi osobami jakie napotykasz s^a podr^o^zuj^acy ze swym "+
             "towarem kupcy. ";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str+="Trakt zmierza ^lagodnym spadem w d^o^l, po^sr^od pag^ork^ow "+
             "pokrytych ^l^ak^a, by dalej rozpocz^a^c sw^a m^ecz^ac^a w^ed"+
             "r^owk^e ostrym podej^sciem pod g^or^e i znikn^a^c za du^z^a "+
             "k^ep^a zieleni znajduj^acej si^e na szczycie wzniesienia. Za"+
             "ro^sla ci^agn^ace wzd^lu^z drogi urozmaicaj^a og^oln^a monot"+
             "onni^e wszystkimi barwami z^lota, czerwieni i br^azu. Pomi^e"+
             "dzy kamykami po^lo^zonymi, by wzmocni^c nawierzchnie traktu,"+
             " ro^snie trawa i jakie^s inne wysuszone, pospolite zielska. "+
             "Ca^la okolica jest pokryta ^l^akami i nigdzie nie wida^c ^sl"+
             "adu po ludzkich osadach a jedynymi ^zywymi osobami jakie nap"+
             "otykasz s^a podr^o^zuj^acy ze swym towarem kupcy. ";
    }

    str+="\n";
    return str;
}