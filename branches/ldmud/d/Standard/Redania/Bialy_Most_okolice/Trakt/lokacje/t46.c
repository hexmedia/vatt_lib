/*
 * Opis oraz kod automatycznie wygenerowany za pomoca VattGen,
 * Data: Friday 02nd of November 2007 11:12:08 PM
 */

#include <pogoda.h>
#include <stdproperties.h>
#include <mudtime.h>
#include "dir.h"

inherit TRAKT_BIALY_MOST_STD;

void
create_trakt()
{
    set_short("Ubity trakt");
    set_long("@@dlugi_opis@@");
    add_exit(TRAKT_BIALY_MOST_LOKACJE + "t45.c","se",0,FATIGUE_TRAKT,0);
    add_exit(TRAKT_BIALY_MOST_LOKACJE + "t47.c","n",0,FATIGUE_TRAKT,0);
    add_prop(ROOM_I_INSIDE,0);

}

public string
exits_description()
{
    return "Trakt prowadzi na p^o^lnoc i po^ludniowy wsch^od\n";
}

string
dlugi_opis()
{
    string str = "";

    if(CZY_JEST_SNIEG(this_object()))
    {
        str+=" . Ca^la okolica jest pokryta ^l^akami i nigdzie nie wida^c "+
             "^sladu po ludzkich osadach a jedynymi ^zywymi osobami jakie "+
             "napotykasz s^a podr^o^zuj^acy ze swym towarem kupcy. Trakt z"+
             "mierza ^lagodnym spadem w d^o^l, po^sr^od pag^ork^ow pokryty"+
             "ch sk^apana w bieli ^l^ak^a, by dalej rozpocz^a^c sw^a m^ecz"+
             "^ac^a w^edr^owk^e ostrym podej^sciem pod g^or^e i znikn^a^c "+
             "za du^z^a k^ep^a zieleni znajduj^acej si^e na szczycie wznie"+
             "sienia. ";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str+="Wzd^lu^z traktu ci^agnie si^e niezbyt g^l^eboki r^ow poro^sn"+
             "i^ety traw^a. Ca^la okolica jest pokryta ^l^akami i nigdzie "+
             "nie wida^c ^sladu po ludzkich osadach a jedynymi ^zywymi oso"+
             "bami jakie napotykasz s^a podr^o^zuj^acy ze swym towarem kup"+
             "cy. Trakt zmierza ^lagodnym spadem w d^o^l, po^sr^od pag^ork"+
             "^ow pokrytych jasnozielon^a ^l^ak^a, by dalej rozpocz^a^c sw"+
             "^a m^ecz^ac^a w^edr^owk^e ostrym podej^sciem pod g^or^e i zn"+
             "ikn^a^c za du^z^a k^ep^a zieleni znajduj^acej si^e na szczyc"+
             "ie wzniesienia. ";
    }
    else if(pora_roku() == MT_LATO)
    {
        str+="Wzd^lu^z traktu ci^agnie si^e niezbyt g^l^eboki r^ow poro^sn"+
             "i^ety traw^a. Ca^la okolica jest pokryta ^l^akami i nigdzie "+
             "nie wida^c ^sladu po ludzkich osadach a jedynymi ^zywymi oso"+
             "bami jakie napotykasz s^a podr^o^zuj^acy ze swym towarem kup"+
             "cy. Trakt zmierza ^lagodnym spadem w d^o^l, po^sr^od pag^ork"+
             "^ow pokrytych soczysto-zielon^a ^l^ak^a, by dalej rozpocz^a^"+
             "c sw^a m^ecz^ac^a w^edr^owk^e ostrym podej^sciem pod g^or^e "+
             "i znikn^a^c za du^z^a k^ep^a zieleni znajduj^acej si^e na sz"+
             "czycie wzniesienia. ";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str+="Wzd^lu^z traktu ci^agnie si^e niezbyt g^l^eboki r^ow poro^sn"+
             "i^ety traw^a. Ca^la okolica jest pokryta ^l^akami i nigdzie "+
             "nie wida^c ^sladu po ludzkich osadach a jedynymi ^zywymi oso"+
             "bami jakie napotykasz s^a podr^o^zuj^acy ze swym towarem kup"+
             "cy. Trakt zmierza ^lagodnym spadem w d^o^l, po^sr^od pag^ork"+
             "^ow pokrytych ^l^ak^a, by dalej rozpocz^a^c sw^a m^ecz^ac^a "+
             "w^edr^owk^e ostrym podej^sciem pod g^or^e i znikn^a^c za du^"+
             "z^a k^ep^a zieleni znajduj^acej si^e na szczycie wzniesienia"+
             ". ";
    }

    str+="\n";
    return str;
}