/*
 * Opis oraz kod automatycznie wygenerowany za pomoca VattGen,
 * Data: Friday 02nd of November 2007 11:18:50 PM
 */

#include <pogoda.h>
#include <stdproperties.h>
#include <mudtime.h>
#include "dir.h"

inherit TRAKT_BIALY_MOST_STD;

void
create_trakt()
{
    set_short("Ubity trakt");
    set_long("@@dlugi_opis@@");
    add_exit(TRAKT_BIALY_MOST_LOKACJE + "t48.c","se",0,FATIGUE_TRAKT,0);
    add_exit(TRAKT_BIALY_MOST_LOKACJE + "t50.c","n",0,FATIGUE_TRAKT,0);
    add_prop(ROOM_I_INSIDE,0);

}

public string
exits_description()
{
    return "Trakt prowadzi na p^o^lnoc i po^ludniowy wsch^od.\n";
}

string
dlugi_opis()
{
    string str = "";

    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="Trakt wij^acy si^e mi^edzy ma^lymi pag^orkami, przecina w ty"+
             "m miejscu pola uprawne przykryte grub^a pokryw^a ^snie^zna, "+
             "na kt^orych gdzieniegdzie mo^zna zauwa^zy^c ^slady drobnej z"+
             "wierzyny. Pola uprawne otaczaj^ace trakt, ci^agn^ace sie pra"+
             "wie po horyzont, pokryte s^a gestym ^sniegiem gdzieniegdzie "+
             "tworz^acym zaspy. Na po^ludniu rozci^agaj^a si^e niezmierzon"+
             "e po^lacie p^ol, kt^ore przykryte bia^lym ^sniegiem wygl^ada"+
             "j^a jak tafla ogromnego jeziora, na kt^orym panuje ca^lkowit"+
             "a cisza i spok^oj, m^acona od czasu do czasu przez mocniejsz"+
             "y podmuch zimnego wiatru. ";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str+="Trakt wij^acy si^e mi^edzy ma^lymi pag^orkami, przecina w ty"+
             "m miejscu lekko ju^z zazielenione pola uprawne, dostarczaj^a"+
             "ce okolicznym mieszka^ncom po^zywienia. Pola uprawne otaczaj"+
             "^ace trakt, ci^agn^ace sie prawie po horyzont, pokryte s^a j"+
             "u^z ^swie^zo zasian^a, kie^lkuj^ac^a ro^slinno^sci^a. Na po^"+
             "ludniu rozci^agaj^a si^e niezmierzone po^lacie p^ol, kt^ore "+
             "b^ed^ac delikatnie zazielenione, s^a prawdziwym rajem dla ok"+
             "olicznych ptak^ow i zwierz^at, szukaj^acych jedzenia po zimo"+
             "wym ^snie. ";
    }
    else if(pora_roku() == MT_LATO)
    {
        str+="Trakt wij^acy si^e mi^edzy ma^lymi pag^orkami, przecina w ty"+
             "m miejscu g^esto obsiane pola uprawne, dostarczaj^ace okolic"+
             "znym mieszka^ncom po^zywienia. Pola uprawne otaczaj^ace trak"+
             "t, ci^agn^ace sie prawie po horyzont, pokryte s^a ju^z ^z^o^"+
             "ltymi k^losami zb^o^z. Na po^ludniu rozci^agaj^a si^e niezmi"+
             "erzone po^lacie p^ol, kt^ore b^ed^ac pi^eknie zazielenione, "+
             "s^a prawdziwym rajem dla okolicznych ptak^ow i zwierz^at wyd"+
             "aj^acych piski, skrzeki i mruczenia s^lyszalne nawet z du^ze"+
             "j odleg^lo^sci, dzi^eki delikatnemu ciep^lemu wietrzykowi. ";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str+="Trakt wij^acy si^e mi^edzy ma^lymi pag^orkami, przecina w ty"+
             "m miejscu przygotowane ju^z do zimy pola uprawne, dostarczaj"+
             "^ace okolicznym mieszka^ncom po^zywienia. Pola uprawne otacz"+
             "aj^ace trakt, ci^agn^ace sie prawie po horyzont, s^a br^azow"+
             "e od nagiej ziemi. Na po^ludniu rozci^agaj^a si^e niezmierzo"+
             "ne po^lacie p^ol, nad kt^orymi snuje si^e lekka mgie^lka nad"+
             "aj^aca im do^s^c niepokoj^acy wygl^ad, a lekki ch^lodny wiet"+
             "rzyk przywiewa zapachy pal^acych sie ognisk przy kt^orych ch"+
             "^lopi ogrzewaj^a sie w przerwach od pracy. ";
    }

    str+="\n";
    return str;
}