/*
 * Opis oraz kod automatycznie wygenerowany za pomoca VattGen,
 * Data: Friday 02nd of November 2007 11:12:08 PM
 */

#include <pogoda.h>
#include <stdproperties.h>
#include <mudtime.h>
#include "dir.h"

inherit TRAKT_BIALY_MOST_STD;

void
create_trakt()
{
    set_short("Trakt w^sr^od ^l^ak");
    set_long("@@dlugi_opis@@");
    add_exit(TRAKT_BIALY_MOST_LOKACJE + "t43.c","e",0,FATIGUE_TRAKT,0);
    add_exit(TRAKT_BIALY_MOST_LOKACJE + "t45.c","nw",0,FATIGUE_TRAKT,0);
    add_prop(ROOM_I_INSIDE,0);

}

public string
exits_description()
{
    return "Trakt prowadzi na p^o^lnocny zach^od i wsch^od.\n";
}

string
dlugi_opis()
{
    string str = "";

    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="Zamarzni^ete b^loto pokrywa ca^lkowicie ziemi^e, tworz^ac ^s"+
             "lisk^a nawierzchni^e. Na zboczach traktu znajduj^a si^e krze"+
             "wy bez li^sci, pokryte ^sniegiem. Wok^o^l rozpo^scieraj^acy "+
             "si^e krajobraz tonie w bieli, kt^orej natura nie posk^api^la"+
             " w okolicy, a przypr^oszaj^ac okoliczne ^l^aki na^lo^zy^la ^"+
             "snie^zny dywan bezlito^snie mro^z^ac przyrod^e znajduj^ac^a "+
             "si^e pod nim. ^Snieg le^z^acy na drodze jest ubity i szary, "+
             "w niekt^orych miejscach pojawi^l si^e l^od wy^slizgany tysi^"+
             "acami ko^nskich kopyt, podeszwami but^ow i ko^lami woz^ow, a"+
             " nier^owne koleiny, ci^agn^ace si^e po obu stronach traktu ^"+
             "swiadcz^a o tym, ^ze handel jest w tych okolicach do^s^c ^zy"+
             "wy. ";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str+="Na zboczach traktu pojawiaj^a si^e s^labo rozwini^ete kwiaty"+
             " polne oraz krzewy. Wok^o^l rozpo^sciera si^e krajobraz pe^l"+
             "en kwitn^acej zieleni i szumi^acej lekko na wietrze ^swie^ze"+
             "j, nielicznej jeszcze trawie - w^la^snie dane jest ci ogl^ad"+
             "a^c budz^ace si^e do ^zycia pierwsze polne kwiaty, kt^orym p"+
             "rzy^spiewuje weso^ly ptasi trel wzywaj^acy przyrod^e do prze"+
             "budzenia si^e. Ziemia jest ubita niemal^ze na kamie^n tysi^a"+
             "cami ko^nskich kopyt, bosych i obutych st^op, Nier^owne kole"+
             "iny, ci^agn^ace si^e po obu stronach traktu ^swiadcz^a o tym"+
             ", ^ze handel jest w tych okolicach do^s^c ^zywy. ";
    }
    else if(pora_roku() == MT_LATO)
    {
        str+="Na zboczach traktu wida^c dorodne krzewy oraz barwne kwiaty."+
             " Wok^o^l rozpo^sciera si^e krajobraz pe^len soczystej zielen"+
             "i i szumi^acej lekko na wietrze trawie, a weso^ly ptasi trel"+
             " przy^spiewuje tej polnej bujnej przyrodzie dope^lniaj^ac bo"+
             "gactwa okolicznej natury. Ziemia jest ubita niemal^ze na kam"+
             "ie^n tysi^acami ko^nskich kopyt, bosych i obutych st^op, Nie"+
             "r^owne koleiny, ci^agn^ace si^e po obu stronach traktu ^swia"+
             "dcz^a o tym, ^ze handel jest w tych okolicach do^s^c ^zywy. ";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str+="Zbocza traktu zape^lniaj^a powoli wi^edn^ace kwiaty oraz krz"+
             "ewy. Wok^o^l rozpo^sciera si^e krajobraz pe^len br^azowo-poc"+
             "hmurnych barw, kt^oremu przy^spiewuje czasem krakanie wrony "+
             "i szumi^ace na wietrze nieliczne podgni^le k^epki traw, kt^o"+
             "re jeszcze przetrwa^ly. Ziemia jest ubita niemal^ze na kamie"+
             "^n tysi^acami ko^nskich kopyt, bosych i obutych st^op, Nier^"+
             "owne koleiny, ci^agn^ace si^e po obu stronach traktu ^swiadc"+
             "z^a o tym, ^ze handel jest w tych okolicach do^s^c ^zywy. ";
    }

    str+="\n";
    return str;
}