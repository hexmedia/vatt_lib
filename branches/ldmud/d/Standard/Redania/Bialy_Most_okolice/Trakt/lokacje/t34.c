/*
 * Opis oraz kod automatycznie wygenerowany za pomoca VattGen,
 * Data: Friday 02nd of November 2007 09:08:17 PM
 */

#include <pogoda.h>
#include <stdproperties.h>
#include <mudtime.h>
#include "dir.h"

inherit TRAKT_BIALY_MOST_STD;

void
create_trakt()
{
    set_short("Trakt w^sr^od ^l^ak");
    set_long("@@dlugi_opis@@");
    add_exit(TRAKT_BIALY_MOST_LOKACJE + "t33","s",0,FATIGUE_TRAKT,0);
    add_exit(TRAKT_BIALY_MOST_LOKACJE + "t35.c","nw",0,FATIGUE_TRAKT,0);
    add_prop(ROOM_I_INSIDE,0);

}

public string
exits_description()
{
    return "Trakt prowadzi na p^o^lnocny zach^od i po^ludnie.\n";
}

string
dlugi_opis()
{
    string str = "";

    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="Szeroki, zas^lany przez ^sniegowy puch oraz brudne b^locko t"+
             "rakt ci^agnie si^e ^lagodnymi ^lukami. Wzd^lu^z traktu wiedz"+
             "ie wype^lniony wszechobecnym ^sniegiem r^ow. Okoliczne ^l^ak"+
             "i pokryte s^a bia^lym, g^estym puchem, przykrywaj^acym wszys"+
             "tko delikatn^a ko^lderk^a. ^L^aki otaczaj^ace trakt, ci^agn^"+
             "ace sie a^z po horyzont, pokryte s^a ju^z g^estym, zbitym i "+
             "zmro^zonym ^sniegiem. Na tle bia^lej okolicy trakt odcina si"+
             "^e wyra^xnie, ciemny, ubity i twardy, pokryty koleinami woz^"+
             "ow, tysi^acem ^slad^ow ko^nskich kopyt i ludzkich st^op. ";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str+="Szeroki, zas^lany z rzadka drobnymi kamykami trakt ci^agnie "+
             "si^e ^lagodnymi ^lukami. Wzd^lu^z traktu wiedzie zarastaj^ac"+
             "y chwastami r^ow. Okoliczne ^l^aki pokryte s^a kie^lkuj^ac^"+
             "a ro^slinno^sci^a, gdzieniegdzie przebijaj^ac^a sie spod cie"+
             "nkich warstewek stopionego ^sniegu. ^L^aki otaczaj^ace trakt"+
             ", ci^agn^ace sie a^z po horyzont, pokryte s^a ju^z ^swie^z^a"+
             " kie^lkuj^ac^a ro^slinno^sci^a. Na tle jasnej zieleni trakt "+
             "odcina si^e wyra^xnie, ciemny, ubity i twardy, pokryty kolei"+
             "nami woz^ow, tysi^acem ^slad^ow ko^nskich kopyt i ludzkich s"+
             "t^op. ";
    }
    else if(pora_roku() == MT_LATO)
    {
        str+="Szeroki, zas^lany z rzadka drobnymi kamykami trakt ci^agnie "+
             "si^e ^lagodnymi ^lukami. Wzd^lu^z traktu wiedzie ca^lkiem za"+
             "ro^sni^ety chwastami r^ow. Okoliczne ^l^aki pokryte s^a buj"+
             "n^a ro^slinno^sci^a, w ^sr^od kt^orej zapewne kwitnie tak^ze"+
             " ^zycie drobnych zwierz^at. ^L^aki otaczaj^ace trakt, ci^agn"+
             "^ace sie a^z po horyzont, pokryte s^a ju^z bij^a, gest^a i c"+
             "iemnozielon^a ro^slinno^sci^a. Na tle jasnej zieleni trakt o"+
             "dcina si^e wyra^xnie, ciemny, ubity i twardy, pokryty kolein"+
             "ami woz^ow, tysi^acem ^slad^ow ko^nskich kopyt i ludzkich st"+
             "^op. ";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str+="Szeroki, zas^lany z rzadka drobnymi kamykami trakt ci^agnie "+
             "si^e ^lagodnymi ^lukami. Wzd^lu^z traktu wiedzie zaro^sni^et"+
             "y wi^edn^acymi chwastami i pokryty opad^lymi li^s^cmi z drze"+
             "w r^ow. Okoliczne ^l^aki pokryte s^a wysuszon^a ro^slinno^s"+
             "ci^a, kt^or^a powoli zaczyna obumiera^c. ^L^aki otaczaj^ace "+
             "trakt, ci^agn^ace sie a^z po horyzont, pokryte s^a obumieraj"+
             "^ac^a przed zim^a ro^slinno^sci^a. Na tle szaro^sci trakt od"+
             "cina si^e wyra^xnie, ciemny, ubity i twardy, pokryty koleina"+
             "mi woz^ow, tysi^acem ^slad^ow ko^nskich kopyt i ludzkich st^"+
             "op. ";
    }

    str+="\n";
    return str;
}