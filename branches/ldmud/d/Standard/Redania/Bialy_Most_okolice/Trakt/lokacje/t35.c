/*
 * Opis oraz kod automatycznie wygenerowany za pomoca VattGen,
 * Data: Friday 02nd of November 2007 09:33:52 PM
 */

#include <pogoda.h>
#include <stdproperties.h>
#include <mudtime.h>
#include "dir.h"

inherit TRAKT_BIALY_MOST_STD;

void
create_trakt()
{
    set_short("Trakt w^sr^od ^l^ak");
    set_long("@@dlugi_opis@@");
    add_exit(TRAKT_BIALY_MOST_LOKACJE + "t34.c","se",0,FATIGUE_TRAKT,0);
    add_exit(TRAKT_BIALY_MOST_LOKACJE + "t36.c","nw",0,FATIGUE_TRAKT,0);
    add_prop(ROOM_I_INSIDE,0);

}

public string
exits_description()
{
    return "Trakt prowadzi na p^o^lnocny zach^od i po^ludniowy wsch^od.\n";
}

string
dlugi_opis()
{
    string str = "";

    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="Trakt jest do^s^c szeroki a gdzieniegdzie, utworzy^ly sie g^"+
             "l^ebokie koleiny wy^z^lobione przez przeje^zd^zaj^ace wozy. "+
             "Wok^o^l rozpo^scieraj^acy si^e krajobraz tonie w bieli, kt^o"+
             "rej natura nie posk^api^la w okolicy, a przypr^oszaj^ac okol"+
             "iczne ^l^aki na^lo^zy^la ^snie^zny dywan bezlito^snie mro^z^"+
             "ac przyrod^e znajduj^ac^a si^e pod nim. Otoczenie zamar^lo w"+
             " zimowym letargu. Droga zryta jest licznymi koleinami wy^z^l"+
             "obionymi w zastyg^lej od mrozu ziemi. Wystaj^ace dooko^la z "+
             "ziemi kikuty ro^slin pokry^ly p^latki ^sniegu. ";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str+="Trakt jest do^s^c szeroki a gdzieniegdzie, utworzy^ly sie g^"+
             "l^ebokie koleiny wy^z^lobione przez przeje^zd^zaj^ace wozy. "+
             "Wok^o^l rozpo^sciera si^e krajobraz pe^len kwitn^acej zielen"+
             "i i szumi^acej lekko na wietrze ^swie^zej, nielicznej jeszcz"+
             "e trawie - w^la^snie dane jest ci ogl^ada^c budz^ace si^e do"+
             " ^zycia pierwsze polne kwiaty, kt^orym przy^spiewuje weso^ly"+
             " ptasi trel wzywaj^acy przyrod^e do przebudzenia si^e. Otocz"+
             "enie t^etni rozkwitaj^acym na wiosn^e ^zyciem. Droga z powod"+
             "u szybko topniej^acego ^sniegu rozmi^ek^la, co w znacznym st"+
             "opniu utrudnia podr^o^zowanie. Dooko^la rosn^a spragnione s^"+
             "lo^nca m^lode ro^slinki . ";
    }
    else if(pora_roku() == MT_LATO)
    {
        str+="Trakt jest do^s^c szeroki a gdzieniegdzie, utworzy^ly sie g^"+
             "l^ebokie koleiny wy^z^lobione przez przeje^zd^zaj^ace wozy. "+
             "Wok^o^l rozpo^sciera si^e krajobraz pe^len soczystej zieleni"+
             " i szumi^acej lekko na wietrze trawie, a weso^ly ptasi trel "+
             "przy^spiewuje tej polnej bujnej przyrodzie dope^lniaj^ac bog"+
             "actwa okolicznej natury. Otoczenie t^etni rozkwit^lym ^zycie"+
             "m. Droga zryta jest licznymi koleinami wy^z^lobionymi w such"+
             "ej ziemi. Dooko^la bujnie rosn^a przer^o^zne ro^sliny. ";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str+="Trakt jest do^s^c szeroki a gdzieniegdzie, utworzy^ly sie g^"+
             "l^ebokie koleiny wy^z^lobione przez przeje^zd^zaj^ace wozy. "+
             "Wok^o^l rozpo^sciera si^e krajobraz pe^len br^azowo-pochmurn"+
             "ych barw, kt^oremu przy^spiewuje czasem krakanie wrony i szu"+
             "mi^ace na wietrze nieliczne podgni^le k^epki traw, kt^ore je"+
             "szcze przetrwa^ly. Otoczenie powoli zamiera w przygotowaniu "+
             "do zbli^zaj^acej si^e zimy. Droga z powodu deszczy rozmi^ek^"+
             "la, co w znacznym stopniu utrudnia podr^o^zowanie. Rosn^ace "+
             "dooko^la ro^sliny wi^edn^a i trac^a intensywne kolory. ";
    }

    str+="\n";
    return str;
}