/*
 * Opis oraz kod automatycznie wygenerowany za pomoca VattGen,
 * Data: Friday 02nd of November 2007 03:45:28 PM
 */

#include <pogoda.h>
#include <stdproperties.h>
#include <mudtime.h>
#include "dir.h"

inherit TRAKT_BIALY_MOST_STD;

void
create_trakt()
{
    set_short("Trakt w^sr^od p^ol");
    set_long("@@dlugi_opis@@");
    add_exit(TRAKT_BIALY_MOST_LOKACJE + "t30.c","se",0,FATIGUE_TRAKT,0);
    add_exit(TRAKT_BIALY_MOST_LOKACJE + "t32.c","n",0,FATIGUE_TRAKT,0);
    add_prop(ROOM_I_INSIDE,0);

}

public string
exits_description()
{
    return "Trakt prowadzi na p^o^lnoc i po^ludniowy wsch^od\n";
}

string
dlugi_opis()
{
    string str = "";

    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="Droga zryta jest licznymi koleinami wy^z^lobionymi w zastyg^"+
             "lej od mrozu ziemi. W stertach pokruszonych kamieni wznosz^a"+
             "cych si^e po obu stronach drogi z trudem mo^zna rozpozna^c p"+
             "ozosta^lo^sci po niewysokich kamiennych murkach zbudowanych "+
             "niegdy^s z bry^l piaskowca a obecnie zniszczonych przez wiat"+
             "ry i deszcze, poro^sni^etych przez ma^le krzaczki i ruderaln"+
             "e zielsko. Wzd^lu^z drogi ci^agn^a si^e zaro^sla w postaci n"+
             "agich ga^l^ezi drzew i krzew^ow. ";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str+="Droga z powodu szybko topniej^acego ^sniegu rozmi^ek^la, co "+
             "w znacznym stopniu utrudnia podr^o^zowanie. W stertach pokru"+
             "szonych kamieni wznosz^acych si^e po obu stronach drogi z tr"+
             "udem mo^zna rozpozna^c pozosta^lo^sci po niewysokich kamienn"+
             "ych murkach zbudowanych niegdy^s z bry^l piaskowca a obecnie"+
             " zniszczonych przez wiatry i deszcze, poro^sni^etych przez m"+
             "a^le krzaczki i ruderalne zielsko. Wzd^lu^z drogi ci^agn^a s"+
             "i^e zaro^sla rozstaczaj^ac s^lodk^a wo^n wiosennego kwiecia."+
             " ";
    }
    else if(pora_roku() == MT_LATO)
    {
        str+="Droga zryta jest licznymi koleinami wy^z^lobionymi w suchej "+
             "ziemi . W stertach pokruszonych kamieni wznosz^acych si^e po"+
             " obu stronach drogi z trudem mo^zna rozpozna^c pozosta^lo^sc"+
             "i po niewysokich kamiennych murkach zbudowanych niegdy^s z b"+
             "ry^l piaskowca a obecnie zniszczonych przez wiatry i deszcze"+
             ", poro^sni^etych przez ma^le krzaczki i ruderalne zielsko. W"+
             "zd^lu^z drogi ci^agn^a si^e g^este zaro^sla daj^ace w^edrowc"+
             "om odrobin^e cienia w upalne, letnie dni. ";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str+="Droga z powodu deszczy rozmi^ek^la, co w znacznym stopniu ut"+
             "rudnia podr^o^zowanie. W stertach pokruszonych kamieni wznos"+
             "z^acych si^e po obu stronach drogi z trudem mo^zna rozpozna^"+
             "c pozosta^lo^sci po niewysokich kamiennych murkach zbudowany"+
             "ch niegdy^s z bry^l piaskowca a obecnie zniszczonych przez w"+
             "iatry i deszcze, poro^sni^etych przez ma^le krzaczki i ruder"+
             "alne zielsko. Wzd^lu^z drogi ci^agn^a si^e g^este zaro^sla o"+
             " i^scie jesiennych, ^zywych barwach. ";
    }

    str+="\n";
    return str;
}