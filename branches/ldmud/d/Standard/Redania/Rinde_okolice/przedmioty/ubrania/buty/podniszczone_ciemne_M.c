/*
 * Podniszczone ciemne buty.
 *
 * Autor: Lil, opis: Dinrahia(tudzie�: Aneta)
 */

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <macros.h>
#include <materialy.h>
#include <object_types.h>

void
create_armour()
{
    ustaw_nazwe("buty");
    ustaw_nazwe_glowna("para");
    dodaj_przym("podniszczony","podniszczeni");
    dodaj_przym("ciemny","ciemni");
    set_long("Buty te zosta�y uszyte z my�l� o podr�nikach. Nie "+
             "s� wi�c eleganckie, ale za to maj� wielk� zalet�, jak� "+
             "jest wygoda zapewniona dzi�ki mi�kko�ci ciemnej nied�wiedziej "+
             "sk�ry, z kt�rej zosta�y skrojone.\n");
    set_condition(1); //w koncu sa podniszczone ;)
    set_slots(A_FEET | A_SHINS);
    add_prop(OBJ_I_WEIGHT, 555);
    add_prop(OBJ_I_VOLUME, 1000);
    add_prop(OBJ_I_VALUE, 12); 
    set_size("M");
    /* buty na nodze tylko w niewielkim stopniu zwi�kszaj� swoj� obj�to�� */
    add_prop(ARMOUR_F_PRZELICZNIK, 10.0);
    /* ustawiamy materia� */
    ustaw_material(MATERIALY_SK_NIEDZWIEDZ);
    /* buciki troch� bardziej powi�kszaj� rozmiar ni� normalne ubrania */
    add_prop(ARMOUR_F_POW_DL_STOPA, 2.5);
    add_prop(ARMOUR_F_POW_SZ_STOPA, 2.5);
    set_type(O_UBRANIA);

    set_likely_cond(23);
}
string
query_auto_load()
{
   return ::query_auto_load();
} 
