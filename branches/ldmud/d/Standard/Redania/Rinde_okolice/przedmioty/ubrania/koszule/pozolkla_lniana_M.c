/* Napisa� kiedy� Nunas, wyszperane z jego katalogu i przerobione przez lil ;)
 */

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include "/sys/macros.h"
#include "materialy.h"
#include "object_types.h"

void
create_armour()
{
    ustaw_nazwe("koszula");
    dodaj_przym("po��k�y", "po��lkli");
    dodaj_przym("lniany","lniani");
    set_long("@@dlugi@@\n");
    set_slots(A_TORSO, A_L_ARM, A_R_ARM);
    add_prop(OBJ_I_WEIGHT, 812);
    add_prop(OBJ_I_VOLUME, 1100);
    add_prop(OBJ_I_VALUE, 10);
    ustaw_material(MATERIALY_LEN);
    set_type(O_UBRANIA);
    add_prop(ARMOUR_I_D_ROZCIAGLIWOSC, 50);
    add_prop(ARMOUR_I_U_ROZCIAGLIWOSC, 50);
    set_size("M");
    //add_prop(ARMOUR_F_PRZELICZNIK, 60.0); //koszula jest bardzo rozciagliwa.

    set_likely_cond(24);

}
string
query_auto_load()
{
   return ::query_auto_load();
} 


string dlugi()
{
    string str;
    str="Jest to prosta, lniana koszula, noszona ch�tnie " +
        "przez ubogich. Koszula ta sznurowana jest " +
        "pod szyj�, a jej r�kawy rozszerzaj� si� ku do�owi. Len, z " +
        "kt�rego jest zrobiona prawdopodobnie by� kiedy� bia�y, " +
        "jednak teraz nieco z��k�; prawdopodobnie jest to skutek " +
        "dzia�ania czasu - ubi�r nie wygl�da na nowy. Przypuszczenie " +
        "to potwierdzaj� postrz�pione ko�ce r�kaw�w oraz ";
    switch(random(4,atoi(OB_NUM(this_object()))))
    {
       case 0: str+="po�atana dziura z ty�u koszuli.";break;
       case 1: str+="po�atana dziura z przodu koszuli.";break;
       case 2: str+="po�atana dziura z boku koszuli.";break;
       case 3: str+="liczne po�atane dziury.";break;
    }
    return str;
}

