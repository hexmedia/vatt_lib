/*  Autor: Avard
    Opis : Aine
    Data : 01.12.07
    Info : Poludnica, wystepuje na polach/lakach w... poludnie. ;) */

inherit "/std/creature";
inherit "/std/combat/unarmed";
inherit "/std/act/attack";
inherit "/std/act/action";
inherit "/std/act/ask_gen";
inherit "/std/act/domove";
#include <std.h>
#include <ss_types.h>
#include <macros.h>
#include <stdproperties.h>
#include <pl.h>
#include <money.h>
#include <object_types.h>
#include "dir.h"

int
czy_atakowac()
{
    if(TP->is_humanoid() && TP->query_rasa() != "po^ludnica")
        return 1;
    else
        return 0;
}

void
create_creature()
{
    ustaw_odmiane_rasy("po^ludnica");
    set_gender(G_FEMALE);

    random_przym(({"przygarbiony", "przygarbieni", "chudy", "chudzi",
        "wychudzony", "wychudzeni"}),({"ma^ly", "mali"}),
        ({"blady", "bladzi", "brudny", "brudni", "^zylasty","^zyla^sci"}),
        ({"sponiewierany", "sponiewierani", "cuchn^acy", "cuchn^acy"}),
        ({"przera^zaj^acy", "przera^zaj^acy","okrutny", "okrutni",
        "rozszala^ly", "rozszalali", "podst^epny","podst^epni",
        "w^sciek^ly", "w^sciekli"}), 2);

    set_long("^Slady rozk^ladu w postaci zgnilizny i odchodz^acych p^lat^ow "+
        "sk^ory bezskutecznie chowaj^a si^e pod strz^epami brudnej z ziemi, "+
        "bia^lej szaty, kt^ora spoczywa na tym niegdy^s kobiecym ciele. "+
        "D^lugie, ^zylaste r^ece pod^a^zaj^ace za pustym i martwym "+
        "spojrzeniem napinaj^a si^e i wyci^agaj^a, przecinaj^ac g^lucho "+
        "powietrze. Zapach, przygarbiona sylwetka i spos^ob poruszania si^e "+
        "upiora w sw^oj groteskowy spos^ob wydaj^a si^e demoniczne i "+
        "sprawiaj^a, ^ze nie trudno ci spostrzec i^z po^ludnica ta nie "+
        "zadowoli si^e samym faktem spotkania z tob^a. \n");

    set_stats (({20+random(10),20+random(10),30+random(30),30+random(10),
                100+random(10)}));
    add_prop(CONT_I_WEIGHT, 35000+random(15000));
    add_prop(CONT_I_HEIGHT, 150+random(30));
    add_prop(LIVE_I_NEVERKNOWN, 1);
    set_aggressive(&czy_atakowac());
    set_attack_chance(91+random(10));
    set_npc_react(1);

    set_cact_time(10);

    add_cact("emote ^smieje si^e upiornie, rozkoszuj^ac si^e walk^a.");
    add_cact("wrzasnij przerazliwie");
    add_cact("emote wiruje w swoim upiornym ta^ncu.");
    add_cact("emote obraca si^e na palcach w swoim ^smiertelnym ta^ncu.");
    set_act_time(20+random(4));
    add_act("emote wiruje w swoim upiornym ta^ncu.");
    add_act("zasmiej sie demonicznie");
    add_act("emote ozgl^ada si^e szukaj^ac kolejnej ofiary.");
    add_act("emote obraca si^e na palcach w swoim groteskowym ta^ncu.");

    set_default_answer(VBFC_ME("default_answer"));

    set_skill(SS_2H_COMBAT,10+random(24));
    set_skill(SS_UNARM_COMBAT, 50+random(10));
    set_skill(SS_PARRY, 5);
    set_skill(SS_DEFENCE,5+random(5));
    set_skill(SS_AWARENESS,64+random(10));
    set_skill(SS_BLIND_COMBAT,20+random(50));
    set_skill(SS_HIDE,30+random(40));
    set_skill(SS_SNEAK,30+random(30));

    set_attack_unarmed(0, 15, 22, W_SLASH,  50, "d�o�", "lewy", "pazury");
    set_attack_unarmed(1, 15, 22, W_SLASH,  50, "d�o�", "prawy", "pazury");

    set_hitloc_unarmed(A_HEAD,          ({ 1, 1, 1 }),  4, "g�owa");
    set_hitloc_unarmed(A_CHEST,         ({ 1, 1, 1 }), 14, "klatka piersiowa");
    set_hitloc_unarmed(A_STOMACH,       ({ 1, 1, 1 }), 13, "brzuch");
    set_hitloc_unarmed(A_BACK,          ({ 1, 1, 1 }),  3, "plecy");
    set_hitloc_unarmed(A_L_SHOULDER,    ({ 1, 1, 1 }),  5, "bark",       "lewy");
    set_hitloc_unarmed(A_R_SHOULDER,    ({ 1, 1, 1 }),  5, "bark",       "prawy");
    set_hitloc_unarmed(A_L_ARM,         ({ 1, 1, 1 }),  6, "rami�",      "lewy");
    set_hitloc_unarmed(A_R_ARM,         ({ 1, 1, 1 }),  6, "rami�",      "prawy");
    set_hitloc_unarmed(A_L_FOREARM,     ({ 1, 1, 1 }),  5, "przedrami�", "lewy");
    set_hitloc_unarmed(A_R_FOREARM,     ({ 1, 1, 1 }),  5, "przedrami�", "prawy");
    set_hitloc_unarmed(A_L_HAND,        ({ 1, 1, 1 }),  3, "d�o�",       "lewy");
    set_hitloc_unarmed(A_R_HAND,        ({ 1, 1, 1 }),  3, "d�o�",       "prawy");
    set_hitloc_unarmed(A_L_THIGH,       ({ 1, 1, 1 }),  6, "udo",        "lewy");
    set_hitloc_unarmed(A_R_THIGH,       ({ 1, 1, 1 }),  6, "udo",        "prawy");
    set_hitloc_unarmed(A_L_SHIN,        ({ 1, 1, 1 }),  5, "�ydka",      "lewy");
    set_hitloc_unarmed(A_R_SHIN,        ({ 1, 1, 1 }),  5, "�ydka",      "prawy");
    set_hitloc_unarmed(A_L_FOOT,        ({ 1, 1, 1 }),  3, "stopa",      "lewy");
    set_hitloc_unarmed(A_R_FOOT,        ({ 1, 1, 1 }),  3, "stopa",      "prawy");

    add_prop(LIVE_I_SEE_DARK, 1);
    add_prop(LIVE_I_NO_CORPSE, 1);
    add_prop(LIVE_I_DIE_TEXT, ({"pada na ziemi^e z g^luchym krzykiem na "+
        "ustach, a jej cia^lo na zawsze ju^z ^l^aczy si^e z ziemi^a",
        "pada na ziemi^e kul^ac si^e w agonii, po czym znika niczym z^ly " +
        "sen", "pada martwo na ziemi^e po czym zaczyna "+
        "znika^c z ledwo zauwa^zalnym wyrazem ulgi na swojej bladej "+
        "twarzy..", "pada na ziemie z szeroko otwartymi, martwymi "+
        "oczami, po czym zaczyna si^e rozp^lywa^c niczym poranna mg^la"}));

    set_random_move(5);
    set_restrain_path(LAKA_RINDE_LOKACJE);
    set_alarm_every_hour("godz");

//     add_leftover("/std/leftover", "pazur", 1+random(2), 0, 1, 1, 0, O_KOSCI);
}

string default_answer()
{
    command("zawyj upiornie");
    return "";
}

void
godz()
{
    if((MT_GODZINA) >= 13 || (MT_GODZINA) < 11)
    {
        command("emote rozp^lywa si^e w powietrzu.");
        remove_object();
    }
}