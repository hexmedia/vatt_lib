/* Autor: Veila
   Data : 25.09.2008
   Opis : Autor zbiorowy, przerobione z systemu losowego Very */
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <mudtime.h> 
#include <pogoda.h>
#include "dir.h"

inherit LAKA_RINDE_STD;

string dlugi_opis();

void create_laka() 
{
    set_short("Gdzie^s w^sr^od ^l^ak.");
    set_long("@@dlugi_opis@@");
    add_exit(LAKA_RINDE_LOKACJE + "laka2.c","w",0,LAKA_RO_FATIG,1);
    add_exit(TRAKT_RINDE_LOKACJE + "trakt37.c","e",0,LAKA_RO_FATIG,1);
    add_prop(ROOM_I_INSIDE,0);
}

string
dlugi_opis()
{
    string str = "";
        
    string noc_ogolny = "Ciemno�� tylko pot�guje "+
            "monotoni� tego miejsca. Jest tak "+
            "ciemno, �e nic praktycznie nie wida�, jedynie w odleg�o�ci nie "+
            "wi�kszej ni� wyci�gni�cie r�ki - jakie� ro�liny i pozamykane "+
            "na noc p�czki kwiat�w. Nietrudno wdepn�� w dziur�, czy te� "+
            "odchody dzikich zwierz�t. O tym, �e jeste� na ��ce "+
            "przypominaj� jedynie pl�cz�ce si� wok� n�g �odygi ro�lin, "+
            "jednak w ciemno�ciach nie spos�b rozpozna� ich gatunk�w. "+
            "Gdzie� na wschodzie majacz� mury miejskie, od "+
            "kt�rych bije niedu�a �una pochodni i latar� ulicznych. ";

    if(jest_dzien() == 1)
    {
        if(CZY_JEST_SNIEG(this_object()))
        {
            str+="Dooko^la s^lycha^c ciche popiskiwanie dzikich zwierz^atek, "+
            "kt^ore zwinnie poruszaj^a si^e pomi^edzy wysokimi zaro^slami, "+
            "sprawnie omijaj^ac przer^o^zne przeszkody. "+
            "Na po^ludniowym-wschodzie ten monotonny krajobraz przecina ciemna "+
            "kresa traktu okolonego rz^edami dostojnych, pot^e^znych drzew. "+
            "Co jaki^s czas na twoj^a twarz pada cie^n pochodz^acy od "+
            "przelatuj^acych ptak^ow ^spiesz^acych do pobliskiego lasu, "+
            "kt^orego zarys maluje si^e gdzie^s daleko na zachodzie. W przeciwnym "+
            "kierunku, ponad trawami i pojedynczymi krzakami, wy�aniaj^a "+
            "si^e mury miasta. "+
            "Nieliczne tropy zwierz^at, kieruj^ace si^e g^l^ownie w stron^e "+
            "niewielkiej zaspy le^z^acej na skraju lasu tworz^a na tej "+
            "ol^sniewaj^acej powierzchni asymetryczny wz^or. "+
            "Pomijaj�c mury miasta w dali na "+
            "wschodzie, od kt�rych bije niedu�a �una pochodni i "+
            "latar� ulicznych, okolica wygl�da na wymar��.";
          
        }
        else if(pora_roku() == MT_WIOSNA)
        {
            str+="To miejsce jest paradoksalne - przenikaj� si� tu "+
            "monotonia i r�norodno��. Nie jest w �aden spos�b "+
            "charakterystyczne, wr�cz przeciwnie - wszystko jest takie same "+
            "- wok� rozpo�ciera si� ogromna ��ka, a dopiero gdzie� na "+
            "po�udniowo-zachodniej cz�ci horyzontu widnieje las. "+
            "R�norodno�� za� zawiera si� w niezwyk�ej rozmaito�ci ro�lin "+
            "i barw przeplataj�cych si� nawzajem. Jest tu ca�e mn�stwo "+
            "mak�w i b�awatk�w, poprzetykanych ��t� naw�oci� i "+
            "zielonkawymi k�osami traw. "+
            "Niewysokie, spl^atane zaro^sla pokryte zaledwie "+
            "mgie^lk^a zieleni tworz^a na po^ludniowym-zachodzie, na "+
            "skraju por^eby "+
            "zwarty mur daj^acy schronienie wielu ptakom i drobnej "+
            "zwierzynie. "+
            "Rz^ad dostojnych, grubych drzew okrytych "+
            "drobnymi, zielonymi listeczkami wyznacza ju^z z daleka "+
            "tras^e traktu majacz^acego w oddali na po^ludniowym-"+
            "wschodzie.";
        }
        else if(pora_roku() == MT_LATO)
        {
            str+="Wsz^edzie, gdzie nie si^egniesz wzrokiem widzisz zielone po^lacie "+
            "bezkresnych ^l^ak. Kolorowe kwiaty, szumi^a cicho poruszone "+
            "podmuchem ciep^lego wiatru, kt^ory delikatnie muska twoj^a twarz, "+
            "przypominaj^ac o nieustaj^acym upale. "+
            "Co jaki^s czas na twoj^a twarz pada cie^n pochodz^acy "+
            "od przelatuj^acych ptak^ow ^spiesz^acych do pobliskiego lasu, "+
            "kt^orego zarys maluje si^e gdzie^s daleko na zachodzie. "+
            "Drzewa rosn^ace wzd^lu^z widocznego "+
            "na po^ludniowym-wschodzie traktu pokryte g^estym listowiem "+
            "i kwiatami wydzielaj^a s^lodki intensywny zapach "+
            "docieraj^acy chwilami z podmuchami wiatru a^z tu. "+
            "Kolczaste zaro^sla na po^ludniowym-"+
            "zachodzie, "+
            "obsypane bia^lymi kwiatkami i czerwonymi owocami ca^le a^z "+
            "dr^z^a od bezustannego ptasiego ^swiergotu milkn^acego "+
            "tylko wtedy gdy na niebie pojawia si^e majestatyczna "+
            "sylwetka myszo^lowa. ";
        }
        else
        {
            str+="Rozci^agaj^aca si^e a^z po horyzont, bezkresna r^ownina "+
            "pokryta namok^lymi trawami wydziela wo^n butwiej^acych "+
            "ro^slin, a wyp^lowia^le od wiatru i deszczu k^epy suchych "+
            "badyli, kt^ore opar^ly si^e apetytowi pas^acych si^e tutaj "+
            "zwierz^at chwiej^a si^e sm^etnie w podmuchach wiatru. "+
            "Dooko^la s^lycha^c ciche popiskiwanie "+
            "dzikich zwierz^atek, kt^ore zwinnie poruszaj^a si^e pomi^edzy "+
            "wysokimi zaro^slami, sprawnie omijaj^ac przer^o^zne przeszkody. "+
            "Dostojne drzewa chroni^ace drapie^znymi, "+
            "bezlistnymi paluchami ga^l^ezi trakt majacz^a za "+
            "butwiej^acymi ^l^akami gdzie^s na po^ludniu. "+
            "Co jaki^s czas na twoj^a twarz pada cie^n pochodz^acy "+
            "od przelatuj^acych ptak^ow ^spiesz^acych do pobliskiego lasu, "+
            "kt^orego zarys maluje si^e gdzie^s daleko na zachodzie. W "+
            "przeciwnym kierunku, ponad trawami i pojedynczymi krzakami, "+
            "wy�aniaj^a si^e mury miasta. ";
        }
    }
    else
    {
        if(CZY_JEST_SNIEG(this_object()))
        {
            str+="Wsz^edzie, gdzie nie si^egniesz wzrokiem widzisz pokryte puchow^a "+
           "pierzyn^a po^lacie bezkresnych ^l^ak. "+
           "Na po^ludniowym-wschodzie ten monotonny krajobraz przecina ciemna "+
           "kresa traktu okolonego rz^edami dostojnych, pot^e^znych drzew. "+
           "Pokryte ^sniegiem ro^sliny i wysokie trawy, kt^orych suche ko^nce "+
           "wystaj^a ponad bia^ly ko�uch, z niecierpliwo^sci^a oczekuj^ac na "+
           "pierwsze oznaki wiosny. "+
           "Mroczny zarys pobliskiego "+
           "pobliskiego lasu maluje si^e gdzie^s daleko na zachodzie. "+
           "W przeciwnym kierunku, ponad trawami i pojedynczymi krzakami, "+
           "wy�aniaj^a si^e mury miasta.";
        }
        else if(pora_roku() == MT_WIOSNA)
        {
            str+="Niska, ciemna murawa pokrywaj^aca delikatnym kobiercem "+
            "ziemi^e, przetykana gdzieniegdzie plamami ciemniejszej "+
            "zieleni k^ep jakiego^s zielska l^sni w blasku ksi^e^zyca "+
            "od rosy pokrywaj^acej ka^zd^a trawk^e i barwne g^l^owki "+
            "kwitn^acych kwiat^ow. W tej soczystej obfito^sci ro^slin "+
            "chroni^a si^e liczne owady, kt^orych jednostajne cykanie "+
            "wraz z pohukiwaniem przelatuj^acych bezszelestnie s^ow "+
            "tworzy niepowtarzaln^a muzyk^e bezkresnych, ciemnych ^l^ak "+
            "rozci^agaj^acych si^e na p^o^lnocy a^z po horyzont. "+
            "Bezkres tej r^owniny m^aci tylko majacz^acy na "+
            "wschodzie ciemny mur Rinde. Niewysokie, "+
            "spl^atane zaro^sla majacz^ace ciemn^a plam^a na tle lasu "+
            "na po^ludniowym-zachodzie daj^a schronienie wielu ptakom i "+
            "drobnej "+
            "zwierzynie. Doskonale widoczny w blasku ksi^e^zyca rz^ad "+
            "dostojnych, grubych drzew wyznacza ju^z z daleka tras^e "+
            "traktu majacz^acego w oddali na po^ludniowym-"+
            "wschodzie. ";
        }
        else if(pora_roku() == MT_LATO)
        {
            str+=noc_ogolny+"Kolczaste zaro^sla na "+
            "wschodzie, majacz^ace ciemn^a plam^a na tle ponurego lasu "+
            "na po^ludniowym-zachodzie ^spi^a teraz spokojnie. Drzewa "+
            "rosn^ace "+
            "wzd^lu^z widocznego na po^ludniowym-wschodzie traktu "+
            "b^lyszcz^ace jak srebrne kolumny w blasku ksi^e^zyca "+
            "wydzielaj^a s^lodki intensywny zapach docieraj^acy "+
            "chwilami z podmuchami wiatru a^z tu. ";
        }

        else
        {
            str +=noc_ogolny;
        }
    }
    str+="\n";
    return str;
}

