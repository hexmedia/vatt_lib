/* Autor: Veila
   Data : 25.09.2008
   Opis : Autor zbiorowy, przerobione z systemu losowego Very */
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <mudtime.h> 
#include <pogoda.h>
#include "dir.h"

inherit LAKA_RINDE_STD;

string dlugi_opis();

void create_laka() 
{
    set_short("Gdzie^s w^sr^od ^l^ak.");
    set_long("@@dlugi_opis@@");
    add_exit(LAKA_RINDE_LOKACJE + "laka30.c","w",0,LAKA_RO_FATIG,1);
    add_exit(LAKA_RINDE_LOKACJE + "laka35.c","sw",0,LAKA_RO_FATIG,1);
    add_exit(LAKA_RINDE_LOKACJE + "laka34.c","s",0,LAKA_RO_FATIG,1);
    add_exit(TRAKT_RINDE_LOKACJE + "trakt8.c","se",0,LAKA_RO_FATIG,1);
    add_exit(LAKA_RINDE_LOKACJE + "laka28.c","e",0,LAKA_RO_FATIG,1);
    add_exit(LAKA_RINDE_LOKACJE + "laka23.c","nw",0,LAKA_RO_FATIG,1);
    add_prop(ROOM_I_INSIDE,0);
    add_npc(LAKA_RINDE_LIVINGI + "zajac");

    set_alarm_every_hour("co_godzine");
}

string
dlugi_opis()
{
    string str = "";
        
    string noc_ogolny = "Ciemno�� tylko pot�guje "+
            "monotoni� tego miejsca. Jest tak "+
            "ciemno, �e nic praktycznie nie wida�, jedynie w odleg�o�ci nie "+
            "wi�kszej ni� wyci�gni�cie r�ki - jakie� ro�liny i pozamykane "+
            "na noc p�czki kwiat�w. Nietrudno wdepn�� w dziur�, czy te� "+
            "odchody dzikich zwierz�t. O tym, �e jeste� na ��ce "+
            "przypominaj� jedynie pl�cz�ce si� wok� n�g �odygi ro�lin, "+
            "jednak w ciemno�ciach nie spos�b rozpozna� ich gatunk�w. "+
            "Gdzie� na wschodzie majacz� mury miejskie, od "+
            "kt�rych bije niedu�a �una pochodni i latar� ulicznych. ";

    if(jest_dzien() == 1)
    {
        if(CZY_JEST_SNIEG(this_object()))
        {
            str+="Monotonia ��ki zim� jest beznami�tna. "+
            "W chwilach, gdy na niebie pojawia si� s�o�ce, jego promienie "+
            "dodaj� temu miejscu nieco rado�ci i �ycia - odbijaj� si� weso�o "+
            "na �niegu, skrz�c niczym ma�e brylanciki. " +
            "Gruba warstwa zlodowacia^lego ^sniegu pokrywa ^l^aki poczynaj^ac "+
            "od muru Rinde na wschodzie, a^z do lasu ciemniej^acego na "+
            "po^ludniowym-zachodzie. " +
            "Co jaki^s czas na twoj^a twarz pada cie^n pochodz^acy od "+
            "przelatuj^acych ptak^ow ^spiesz^acych do pobliskiego lasu, "+
            "kt^orego zarys maluje si^e gdzie^s daleko na zachodzie. W przeciwnym "+
            "kierunku, ponad trawami i pojedynczymi krzakami, wy�aniaj^a "+
            "si^e mury miasta. " +
            "Pokryte ^sniegiem ro^sliny i wysokie trawy, kt^orych suche ko^nce "+
            "wystaj^a ponad bia^ly ko�uch, z niecierpliwo^sci^a oczekuj^ac na "+
            "pierwsze oznaki wiosny. " +
            "Mroczny zarys pobliskiego "+
            "pobliskiego lasu maluje si^e gdzie^s daleko na zachodzie. "; 
          
        }
        else if(pora_roku() == MT_WIOSNA)
        {
            str+="Wsz^edzie, gdzie nie si^egniesz wzrokiem widzisz zielone "+
            "po^lacie bezkresnych ^l^ak. Bezwiednie ko^lysz^ace si^e na "+
            "wietrze ro^sliny u^spione i skulone spokojnie czekaj^a na "+
            "powr^ot s^lo^nca wraz z ^zyciodajnym ^swiat^lem. ",
            "Niska, jasnozielona murawa pokrywaj^aca delikatnym "+
            "kobiercem ziemi^e, przetykana gdzieniegdzie plamami "+
            "ciemniejszej zieleni k^ep jakiego^s zielska skrzy si^e "+
            "ca^la od rosy pokrywaj^acej ka^zd^a trawk^e i barwne "+
            "g^l^owki kwitn^acych kwiat^ow. W tej soczystej obfito^sci "+
            "ro^slin chroni^a si^e liczne owady, kt^orych jednostajne "+
            "brz^eczenie i cykanie wraz ze ^spiewem bujaj^acych wysoko "+
            "na niebie skowronk^ow tworzy niepowtarzaln^a muzyk^e "+
            "bezkresnych, zielonych ^l^ak rozci^agaj^acych si^e na "+
            "p^o^lnocy a^z po horyzont. "+
            "Dooko^la s^lycha^c ciche popiskiwanie "+
            "dzikich zwierz^atek, kt^ore zwinnie poruszaj^a si^e pomi^edzy "+
            "wysokimi zaro^slami, sprawnie omijaj^ac przer^o^zne przeszkody. ";
        }
        else if(pora_roku() == MT_LATO)
        {
            str+="To miejsce jest paradoksalne - przenikaj� si� tu "+
            "monotonia i r�norodno��. Nie jest w �aden spos�b "+
            "charakterystyczne, wr�cz przeciwnie - wszystko jest takie same "+
            "- wok� rozpo�ciera si� ogromna ��ka, a dopiero gdzie� na "+
            "po�udniowo-zachodniej cz�ci horyzontu widnieje las. "+
            "R�norodno�� za� zawiera si� w niezwyk�ej rozmaito�ci ro�lin "+
            "i barw przeplataj�cych si� nawzajem. Jest tu ca�e mn�stwo "+
            "mak�w i b�awatk�w, poprzetykanych ��t� naw�oci� i "+
            "zielonkawymi k�osami traw. "+
            "Dooko^la s^lycha^c ciche "+
            "popiskiwanie dzikich zwierz^atek, kt^ore zwinnie poruszaj^a si^e "+
            "pomi^edzy wysokimi zaro^slami, sprawnie omijaj^ac przer^o^zne "+
            "przeszkody. "+
            "Drzewa rosn^ace wzd^lu^z widocznego "+
            "na po^ludniowym-wschodzie traktu pokryte g^estym listowiem "+
            "i kwiatami wydzielaj^a s^lodki intensywny zapach "+
            "docieraj^acy chwilami z podmuchami wiatru a^z tu.";
        }
        else
        {
            str+="Wsz^edzie, gdzie nie si^egniesz wzrokiem widzisz ^z^o^lte po^lacie "+
            " bezkresnych ^l^ak. Zwi^edni^ete i po^z�^lk^le ju^z ro^sliny, "+
            "szumi^a cicho poruszone ch^lodnym wiatrem, kt^ory przypomina o "+
            "zbli^zaj^acej si^e zimie. "+
            "Dooko^la s^lycha^c ciche popiskiwanie "+
            "dzikich zwierz^atek, kt^ore zwinnie poruszaj^a si^e pomi^edzy "+
            "wysokimi zaro^slami, sprawnie omijaj^ac przer^o^zne przeszkody. "+
            "Ja^sniejsz^a plam^a majacz^a na "+
            "tle ciemnego lasu kolczaste zaro^sla le^z^ace na skraju "+
            "por^eby. Dostojne drzewa chroni^ace drapie^znymi, "+
            "bezlistnymi paluchami ga^l^ezi trakt majacz^a za "+
            "butwiej^acymi ^l^akami na po�udniu. "+
            "Co jaki^s czas na twoj^a twarz pada cie^n pochodz^acy "+
            "od przelatuj^acych ptak^ow ^spiesz^acych do pobliskiego lasu, "+
            "kt^orego zarys maluje si^e gdzie^s daleko na zachodzie. ";
        }
    }
    else
    {
        if(CZY_JEST_SNIEG(this_object()))
        {
            str+="Monotonia ��ki zim� jest beznami�tna. "+
           "Gruba warstwa zlodowacia^lego ^sniegu pokrywa ^l^aki poczynaj^ac "+
           "od muru Rinde na wschodzie, a^z do lasu ciemniej^acego na "+
           "po^ludniowym-zachodzie. "+
           "Nieliczne tropy zwierz^at, kieruj^ace si^e g^l^ownie w stron^e "+
           "niewielkiej zaspy le^z^acej na skraju lasu tworz^a na tej "+
           "ol^sniewaj^acej powierzchni asymetryczny wz^or. "+
           "Wok� roztacza si� ogromna �nie�na pustynia. "+
           "Pomijaj�c mury miasta w dali na "+
           "wschodzie, od kt�rych bije niedu�a �una pochodni i "+
           "latar� ulicznych, okolica wygl�da na wymar��.";
        }
        else if(pora_roku() == MT_WIOSNA)
        {
            str+=noc_ogolny;
        }
        else if(pora_roku() == MT_LATO)
        {
            str+=noc_ogolny+"Kwiaty szumi^a cicho, poruszone podmuchem "+
            "ch^lodnego wiatru, kt^ory delikatnie muska twoj^a twarz, "+
            "och^ladzaj^ac przy tym cia^lo po nieustaj^acym upale. "+
            "Zapach siana, zi^o^l i kwiat^ow przesyca lekko, "+
            "drgaj^ace ponad ^l^akami, rozgrzane powietrze pe^lne "+
            "cykania wsz^edobylskich owad^ow i pohukiwania s^ow. ";
        }

        else
        {
            str +=noc_ogolny;
        }
    }
    str+="\n";
    return str;
}

int
co_godzine()
{
    if (MT_GODZINA == 11)
    {
        set_alarm(1.0,0.0, "odnow_poludnice");
    }
}
void
odnow_poludnice()
{
    LOAD_ERR(LAKA_RINDE_LIVINGI + "poludnica");

    object poludnica;
    poludnica = clone_object(LAKA_RINDE_LIVINGI + "poludnica");
    tell_roombb("^Swiat rozp^lywa ci si^e przed oczami, a z powsta^ej mg^ly "+
        "materializuje si^e "+QCIMIE(poludnica, PL_MIA)+".\n");
    poludnica->init_arg(0);
    poludnica->start_me();
    poludnica->move(TO);
}

