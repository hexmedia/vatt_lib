/* Autor: Veila
   Data : 25.09.2008
   Opis : Autor zbiorowy, przerobione z systemu losowego Very */
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <mudtime.h> 
#include <pogoda.h>
#include "dir.h"

inherit LAKA_RINDE_STD;

string dlugi_opis();

void create_laka() 
{
    set_short("Gdzie^s w^sr^od ^l^ak.");
    set_long("@@dlugi_opis@@");
    add_exit(LAKA_RINDE_LOKACJE + "laka36.c","w",0,LAKA_RO_FATIG,1);
    add_exit(LAKA_RINDE_LOKACJE + "laka42.c","sw",0,LAKA_RO_FATIG,1);
    add_exit(LAKA_RINDE_LOKACJE + "laka41.c","s",0,LAKA_RO_FATIG,1);
    add_exit(TRAKT_RINDE_LOKACJE + "trakt9.c","se",0,LAKA_RO_FATIG,1);
    add_exit(LAKA_RINDE_LOKACJE + "laka34.c","e",0,LAKA_RO_FATIG,1);
    add_exit(LAKA_RINDE_LOKACJE + "laka29.c","ne",0,LAKA_RO_FATIG,1);
    add_exit(LAKA_RINDE_LOKACJE + "laka30.c","n",0,LAKA_RO_FATIG,1);
    add_exit(LAKA_RINDE_LOKACJE + "laka31.c","nw",0,LAKA_RO_FATIG,1);
    add_prop(ROOM_I_INSIDE,0);
}

string
dlugi_opis()
{
    string str = "";
        
    string noc_ogolny = "Ciemność tylko potęguje "+
            "monotonię tego miejsca. Jest tak "+
            "ciemno, że nic praktycznie nie widać, jedynie w odległości nie "+
            "większej niż wyciągnięcie ręki - jakieś rośliny i pozamykane "+
            "na noc pączki kwiatów. Nietrudno wdepnąć w dziurę, czy też "+
            "odchody dzikich zwierząt. O tym, że jesteś na łące "+
            "przypominają jedynie plączące się wokół nóg łodygi roślin, "+
            "jednak w ciemnościach nie sposób rozpoznać ich gatunków. "+
            "Gdzieś na wschodzie majaczą mury miejskie, od "+
            "których bije nieduża łuna pochodni i latarń ulicznych. ";

    if(jest_dzien() == 1)
    {
        if(CZY_JEST_SNIEG(this_object()))
        {
            str+="Łąka zimą jest beznamiętna. "
            "Dooko^la s^lycha^c ciche popiskiwanie dzikich "+
            "zwierz^atek, kt^ore zwinnie poruszaj^a si^e pomi^edzy wysokimi "+
            "zaro^slami, sprawnie omijaj^ac przer^o^zne przeszkody. W chwilach, "+
            "gdy na niebie pojawia się słońce, jego promienie dodają temu "+
            "miejscu nieco radości i życia - odbijają się wesoło na śniegu, "+
            "skrząc niczym małe brylanciki. "+
            "Wszystko dookoła jest takie same, dopiero na zachodzie widać "+
            "ścianę lasu. "+
            "Co jaki^s czas na twoj^a twarz pada cie^n pochodz^acy od "+
            "przelatuj^acych ptak^ow ^spiesz^acych do pobliskiego lasu, "+
            "kt^orego zarys maluje si^e gdzie^s daleko na zachodzie. "+
            "Wokół roztacza się ogromna biała pustynia. Gdzieniegdzie spod "+
            "śnieżnej pierzyny wystają jakieś suche łodygi traw i kwiatów. "
            "Pomijając mury miasta w dali na "+
            "wschodzie, od których bije nieduża łuna pochodni i "+
            "latarń ulicznych, okolica wygląda na wymarłą.";
          
        }
        else if(pora_roku() == MT_WIOSNA)
        {
            str+="Wsz^edzie, gdzie nie si^egniesz wzrokiem widzisz zielone "+
            "po^lacie bezkresnych ^l^ak. "+
            "Niska, jasnozielona murawa pokrywaj^aca delikatnym "+
            "kobiercem ziemi^e, przetykana gdzieniegdzie plamami "+
            "ciemniejszej zieleni k^ep jakiego^s zielska skrzy si^e "+
            "ca^la od rosy pokrywaj^acej ka^zd^a trawk^e i barwne "+
            "g^l^owki kwitn^acych kwiat^ow. W tej soczystej obfito^sci "+
            "ro^slin chroni^a si^e liczne owady, kt^orych jednostajne "+
            "brz^eczenie i cykanie wraz ze ^spiewem bujaj^acych wysoko "+
            "na niebie skowronk^ow tworzy niepowtarzaln^a muzyk^e "+
            "bezkresnych, zielonych ^l^ak rozci^agaj^acych si^e na "+
            "p^o^lnocy a^z po horyzont. Bezkres tej r^owniny m^aci "+
            "tylko majacz^acy na wschodzie ciemny mur "+
            "Rinde. "+
            "Rz^ad dostojnych, grubych drzew okrytych "+
            "drobnymi, zielonymi listeczkami wyznacza ju^z z daleka "+
            "tras^e traktu majacz^acego w oddali na po^ludniowym-"+
            "wschodzie.";
        }
        else if(pora_roku() == MT_LATO)
        {
            str+="Rozci^agaj^ace si^e a^z po p^o^lnocny horyzont morze coraz "+
            "wy^zszych traw faluje w rytm powiew^ow wiatru, a z jego "+
            "trawiastych fal wy^lania si^e na wschodzie jak "+
            "okr^et mur Rinde. "+
            "Dooko^la s^lycha^c ciche "+
            "popiskiwanie dzikich zwierz^atek, kt^ore zwinnie poruszaj^a si^e "+
            "pomi^edzy wysokimi zaro^slami, sprawnie omijaj^ac przer^o^zne "+
            "przeszkody. "+
            "Co jaki^s czas na twoj^a twarz pada cie^n pochodz^acy "+
            "od przelatuj^acych ptak^ow ^spiesz^acych do pobliskiego lasu, "+
            "kt^orego zarys maluje si^e gdzie^s daleko na zachodzie. W "+
            "przeciwnym kierunku, ponad trawami i pojedynczymi krzakami, "+
            "wyłaniaj^a si^e mury miasta. "+
            "Kolczaste zaro^sla na po^ludniowym-"+
            "zachodzie, "+
            "obsypane bia^lymi kwiatkami i czerwonymi owocami ca^le a^z "+
            "dr^z^a od bezustannego ptasiego ^swiergotu milkn^acego "+
            "tylko wtedy gdy na niebie pojawia si^e majestatyczna "+
            "sylwetka myszo^lowa. ";
        }
        else
        {
            str+="Wsz^edzie, gdzie nie si^egniesz wzrokiem widzisz ^z^o^lte po^lacie "+
            "bezkresnych ^l^ak. "+
            "Jak olbrzymie zwierz^e przyczajone w ciemno^sci majaczy po "+
            "wschodniej stronie r^owniny ciemny, zamglony "+
            "mur Rinde. Rozmi^ek^l^a, b^lotnist^a ziemi^e mlaszcz^ac^a "+
            "pod stopami przy najdrobniejszym poruszeniu pokrywa jaka^s "+
            "o^slizg^la mi^ekka masa. "+
            "Ja^sniejsz^a plam^a majacz^a na "+
            "tle ciemnego lasu kolczaste zaro^sla le^z^ace na skraju "+
            "por^eby. Dostojne drzewa chroni^ace drapie^znymi, "+
            "bezlistnymi paluchami ga^l^ezi trakt majacz^a za "+
            "butwiej^acymi ^l^akami na południu. "+
            "Co jaki^s czas na twoj^a twarz pada cie^n pochodz^acy "+
            "od przelatuj^acych ptak^ow ^spiesz^acych do pobliskiego lasu, "+
            "kt^orego zarys maluje si^e gdzie^s daleko na zachodzie. W "+
            "przeciwnym kierunku, ponad trawami i pojedynczymi krzakami, "+
            "wyłaniaj^a si^e mury miasta. ";
        }
    }
    else
    {
        if(CZY_JEST_SNIEG(this_object()))
        {
            str+="Wsz^edzie, gdzie nie si^egniesz wzrokiem widzisz pokryte puchow^a "+
           "pierzyn^a po^lacie bezkresnych ^l^ak. "+
           "Na po^ludniowym-wschodzie ten monotonny krajobraz przecina ciemna "+
           "kresa traktu okolonego rz^edami dostojnych, pot^e^znych drzew. "+
           "Pokryte ^sniegiem ro^sliny i wysokie trawy, kt^orych suche ko^nce "+
           "wystaj^a ponad bia^ly kożuch, z niecierpliwo^sci^a oczekuj^ac na "+
           "pierwsze oznaki wiosny. "+
           "Mroczny zarys pobliskiego "+
           "pobliskiego lasu maluje si^e gdzie^s daleko na zachodzie. "+
           "W przeciwnym kierunku, ponad trawami i pojedynczymi krzakami, "+
           "wyłaniaj^a si^e mury miasta.";
        }
        else if(pora_roku() == MT_WIOSNA)
        {
            str+=noc_ogolny;
        }
        else if(pora_roku() == MT_LATO)
        {
            str+=noc_ogolny+"Kolczaste zaro^sla na "+
            "wschodzie, majacz^ace ciemn^a plam^a na tle ponurego lasu "+
            "na po^ludniowym-zachodzie ^spi^a teraz spokojnie. Drzewa "+
            "rosn^ace "+
            "wzd^lu^z widocznego na po^ludniowym-wschodzie traktu "+
            "b^lyszcz^ace jak srebrne kolumny w blasku ksi^e^zyca "+
            "wydzielaj^a s^lodki intensywny zapach docieraj^acy "+
            "chwilami z podmuchami wiatru a^z tu. ";
        }

        else
        {
            str +=noc_ogolny+"Ja^sniejsz^a plam^a majacz^a na "+
            "tle ciemnego lasu kolczaste zaro^sla le^z^ace na skraju "+
            "por^eby. Dostojne drzewa chroni^ace drapie^znymi, "+
            "bezlistnymi paluchami ga^l^ezi trakt majacz^a za "+
            "butwiej^acymi ^l^akami na południu. ";
        }
    }
    str+="\n";
    return str;
}
