/* Autor: Veila
   Data : 25.09.2008
   Opis : Autor zbiorowy, przerobione z systemu losowego Very */
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <mudtime.h> 
#include <pogoda.h>
#include "dir.h"

inherit LAKA_RINDE_STD;

string dlugi_opis();

void create_laka() 
{
    set_short("Gdzie^s w^sr^od ^l^ak.");
    set_long("@@dlugi_opis@@");
    add_exit(LAKA_RINDE_LOKACJE + "laka48.c","w",0,LAKA_RO_FATIG,1);
    add_exit(TRAKT_RINDE_LOKACJE + "trakt12.c","sw",0,LAKA_RO_FATIG,1);
    add_exit(TRAKT_RINDE_LOKACJE + "trakt11.c","s",0,LAKA_RO_FATIG,1);
    add_exit(TRAKT_RINDE_LOKACJE + "trakt10.c","e",0,LAKA_RO_FATIG,1);
    add_exit(TRAKT_RINDE_LOKACJE + "trakt9.c","ne",0,LAKA_RO_FATIG,1);
    add_exit(LAKA_RINDE_LOKACJE + "laka41.c","n",0,LAKA_RO_FATIG,1);
    add_exit(LAKA_RINDE_LOKACJE + "laka42.c","nw",0,LAKA_RO_FATIG,1);
    add_prop(ROOM_I_INSIDE,0);
}

string
dlugi_opis()
{
    string str = "";
        
    string noc_ogolny = "Ciemność tylko potęguje "+
            "monotonię tego miejsca. Jest tak "+
            "ciemno, że nic praktycznie nie widać, jedynie w odległości nie "+
            "większej niż wyciągnięcie ręki - jakieś rośliny i pozamykane "+
            "na noc pączki kwiatów. Nietrudno wdepnąć w dziurę, czy też "+
            "odchody dzikich zwierząt. O tym, że jesteś na łące "+
            "przypominają jedynie plączące się wokół nóg łodygi roślin, "+
            "jednak w ciemnościach nie sposób rozpoznać ich gatunków. "+
            "Gdzieś na wschodzie majaczą mury miejskie, od "+
            "których bije nieduża łuna pochodni i latarń ulicznych. ";

    if(jest_dzien() == 1)
    {
        if(CZY_JEST_SNIEG(this_object()))
        {
            str+="Wsz^edzie, gdzie nie si^egniesz wzrokiem widzisz pokryte puchow^a "+
            "pierzyn^a po^lacie bezkresnych ^l^ak. "+
            "W chwilach, gdy na niebie pojawia się słońce, jego promienie "+
            "dodają temu miejscu nieco radości i życia - odbijają się wesoło "+
            "na śniegu, skrząc niczym małe brylanciki. "+
            "Pomijając mury miasta bielące się na wschodzie, okolica wygląda "+
            "na wymarłą. "+
            "Co jaki^s czas na twoj^a twarz pada cie^n pochodz^acy od "+
            "przelatuj^acych ptak^ow ^spiesz^acych do pobliskiego lasu, "+
            "kt^orego zarys maluje si^e gdzie^s daleko na zachodzie. "+
            "Pokryte ^sniegiem ro^sliny i wysokie trawy, kt^orych suche ko^nce "+
            "wystaj^a ponad bia^ly kożuch, z niecierpliwo^sci^a oczekuj^ac na "+
            "pierwsze oznaki wiosny. "; 
          
        }
        else if(pora_roku() == MT_WIOSNA)
        {
            str+="To miejsce jest paradoksalne - przenikają się tu "+
            "monotonia i różnorodność. Nie jest w żaden sposób "+
            "charakterystyczne, wręcz przeciwnie - wszystko jest takie same "+
            "- wokół rozpościera się ogromna łąka, a dopiero gdzieś na "+
            "południowo-zachodniej części horyzontu widnieje las. "+
            "Różnorodność zaś zawiera się w niezwykłej rozmaitości roślin "+
            "i barw przeplatających się nawzajem. Jest tu całe mnóstwo "+
            "maków i bławatków, poprzetykanych żółtą nawłocią i "+
            "zielonkawymi kłosami traw. "+
            "Niska, jasnozielona murawa pokrywaj^aca delikatnym "+
            "kobiercem ziemi^e, przetykana gdzieniegdzie plamami "+
            "ciemniejszej zieleni k^ep jakiego^s zielska skrzy si^e "+
            "ca^la od rosy pokrywaj^acej ka^zd^a trawk^e i barwne "+
            "g^l^owki kwitn^acych kwiat^ow. W tej soczystej obfito^sci "+
            "ro^slin chroni^a si^e liczne owady, kt^orych jednostajne "+
            "brz^eczenie i cykanie wraz ze ^spiewem bujaj^acych wysoko "+
            "na niebie skowronk^ow tworzy niepowtarzaln^a muzyk^e "+
            "bezkresnych, zielonych ^l^ak rozci^agaj^acych si^e na "+
            "p^o^lnocy a^z po horyzont. "+
            "Mroczny zarys pobliskiego lasu maluje si^e gdzie^s "+
            "daleko na zachodzie. W przeciwnym kierunku, ponad trawami i "+
            "pojedynczymi krzakami, wyłaniaj^a si^e mury miasta. ";
        }
        else if(pora_roku() == MT_LATO)
        {
            str+="To miejsce jest paradoksalne - przenikają się tu "+
            "monotonia i różnorodność. Nie jest w żaden sposób "+
            "charakterystyczne, wręcz przeciwnie - wszystko jest takie same "+
            "- wokół rozpościera się ogromna łąka, a dopiero gdzieś na "+
            "południowo-zachodniej części horyzontu widnieje las. "+
            "Różnorodność zaś zawiera się w niezwykłej rozmaitości roślin "+
            "i barw przeplatających się nawzajem. Jest tu całe mnóstwo "+
            "maków i bławatków, poprzetykanych żółtą nawłocią i "+
            "zielonkawymi kłosami traw. "+
            "Dooko^la s^lycha^c ciche "+
            "popiskiwanie dzikich zwierz^atek, kt^ore zwinnie poruszaj^a si^e "+
            "pomi^edzy wysokimi zaro^slami, sprawnie omijaj^ac przer^o^zne "+
            "przeszkody. "+
            "Drzewa rosn^ace wzd^lu^z widocznego "+
            "na po^ludniowym-wschodzie traktu pokryte g^estym listowiem "+
            "i kwiatami wydzielaj^a s^lodki intensywny zapach "+
            "docieraj^acy chwilami z podmuchami wiatru a^z tu.";
        }
        else
        {
            str+="Wsz^edzie, gdzie nie si^egniesz wzrokiem widzisz ^z^o^lte po^lacie "+
            " bezkresnych ^l^ak. Zwi^edni^ete i po^zó^lk^le ju^z ro^sliny, "+
            "szumi^a cicho poruszone ch^lodnym wiatrem, kt^ory przypomina o "+
            "zbli^zaj^acej si^e zimie. "+
            "Dooko^la s^lycha^c ciche popiskiwanie "+
            "dzikich zwierz^atek, kt^ore zwinnie poruszaj^a si^e pomi^edzy "+
            "wysokimi zaro^slami, sprawnie omijaj^ac przer^o^zne przeszkody. "+
            "Ja^sniejsz^a plam^a majacz^a na "+
            "tle ciemnego lasu kolczaste zaro^sla le^z^ace na skraju "+
            "por^eby. Dostojne drzewa chroni^ace drapie^znymi, "+
            "bezlistnymi paluchami ga^l^ezi trakt majacz^a za "+
            "butwiej^acymi ^l^akami na południu. "+
            "Co jaki^s czas na twoj^a twarz pada cie^n pochodz^acy "+
            "od przelatuj^acych ptak^ow ^spiesz^acych do pobliskiego lasu, "+
            "kt^orego zarys maluje si^e gdzie^s daleko na zachodzie. ";
        }
    }
    else
    {
        if(CZY_JEST_SNIEG(this_object()))
        {
            str+="Monotonia łąki zimą jest beznamiętna. "+
           "Gruba warstwa zlodowacia^lego ^sniegu pokrywa ^l^aki poczynaj^ac "+
           "od muru Rinde na wschodzie, a^z do lasu ciemniej^acego na "+
           "po^ludniowym-zachodzie. "+
           "Nieliczne tropy zwierz^at, kieruj^ace si^e g^l^ownie w stron^e "+
           "niewielkiej zaspy le^z^acej na skraju lasu tworz^a na tej "+
           "ol^sniewaj^acej powierzchni asymetryczny wz^or. "+
           "Wokół roztacza się ogromna śnieżna pustynia. "+
           "Pomijając mury miasta w dali na "+
           "wschodzie, od których bije nieduża łuna pochodni i "+
           "latarń ulicznych, okolica wygląda na wymarłą.";
        }
        else if(pora_roku() == MT_WIOSNA)
        {
            str+=noc_ogolny+"Niska, ciemna murawa pokrywaj^aca delikatnym kobiercem "+
            "ziemi^e, przetykana gdzieniegdzie plamami ciemniejszej "+
            "zieleni k^ep jakiego^s zielska l^sni w blasku ksi^e^zyca "+
            "od rosy pokrywaj^acej ka^zd^a trawk^e i barwne g^l^owki "+
            "kwitn^acych kwiat^ow. W tej soczystej obfito^sci ro^slin "+
            "chroni^a si^e liczne owady, kt^orych jednostajne cykanie "+
            "wraz z pohukiwaniem przelatuj^acych bezszelestnie s^ow "+
            "tworzy niepowtarzaln^a muzyk^e bezkresnych, ciemnych ^l^ak "+
            "rozci^agaj^acych si^e na p^o^lnocy a^z po horyzont. ";
        }
        else if(pora_roku() == MT_LATO)
        {
            str+=noc_ogolny;
        }

        else
        {
            str +=noc_ogolny+"Ja^sniejsz^a plam^a majacz^a na "+
            "tle ciemnego lasu kolczaste zaro^sla le^z^ace na skraju "+
            "por^eby. Dostojne drzewa chroni^ace drapie^znymi, "+
            "bezlistnymi paluchami ga^l^ezi trakt majacz^a za "+
            "butwiej^acymi ^l^akami na południu. ";
        }
    }
    str+="\n";
    return str;
}
    str+="\n";
    return str;
}
