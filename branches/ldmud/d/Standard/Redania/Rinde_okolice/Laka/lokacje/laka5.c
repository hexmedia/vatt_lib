/* Autor: Veila
   Data : 25.09.2008
   Opis : Autor zbiorowy, przerobione z systemu losowego Very */
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <mudtime.h> 
#include <pogoda.h>
#include "dir.h"

inherit LAKA_RINDE_STD;

string dlugi_opis();

void create_laka() 
{
    set_short("Gdzie^s w^sr^od ^l^ak.");
    set_long("@@dlugi_opis@@");
    add_exit(LAKA_RINDE_LOKACJE + "laka7.c","sw",0,LAKA_RO_FATIG,1);
    add_exit(LAKA_RINDE_LOKACJE + "laka6.c","s",0,LAKA_RO_FATIG,1);
    add_exit(LAKA_RINDE_LOKACJE + "laka4.c","e",0,LAKA_RO_FATIG,1);
    add_exit(LAKA_RINDE_LOKACJE + "laka3.c","ne",0,LAKA_RO_FATIG,1);
    add_prop(ROOM_I_INSIDE,0);
}

string
dlugi_opis()
{
    string str = "";
    
    string noc_ogolny = "Ciemność tylko potęguje "+
            "monotonię tego miejsca. Jest tak "+
            "ciemno, że nic praktycznie nie widać, jedynie w odległości nie "+
            "większej niż wyciągnięcie ręki - jakieś rośliny i pozamykane "+
            "na noc pączki kwiatów. Nietrudno wdepnąć w dziurę, czy też "+
            "odchody dzikich zwierząt. O tym, że jesteś na łące "+
            "przypominają jedynie plączące się wokół nóg łodygi roślin, "+
            "jednak w ciemnościach nie sposób rozpoznać ich gatunków. "+
            "Gdzieś na wschodzie majaczą mury miejskie, od "+
            "których bije nieduża łuna pochodni i latarń ulicznych. ";


    if(jest_dzien() == 1)
    {
        if(CZY_JEST_SNIEG(this_object()))
        {
            str+="Wsz^edzie, gdzie nie si^egniesz wzrokiem widzisz pokryte puchow^a "+
            "pierzyn^a po^lacie bezkresnych ^l^ak. "+
            "W chwilach, gdy na niebie pojawia się słońce, jego promienie "+
            "dodają temu miejscu nieco radości i życia - odbijają się wesoło "+
            "na śniegu, skrząc niczym małe brylanciki. "+
            "Pomijając mury miasta bielące się na wschodzie, okolica wygląda "+
            "na wymarłą. "+
            "Co jaki^s czas na twoj^a twarz pada cie^n pochodz^acy od "+
            "przelatuj^acych ptak^ow ^spiesz^acych do pobliskiego lasu, "+
            "kt^orego zarys maluje si^e gdzie^s daleko na zachodzie. "+
            "Pokryte ^sniegiem ro^sliny i wysokie trawy, kt^orych suche ko^nce "+
            "wystaj^a ponad bia^ly kożuch, z niecierpliwo^sci^a oczekuj^ac na "+
            "pierwsze oznaki wiosny. "; 
          
        }
        else if(pora_roku() == MT_WIOSNA)
        {
            str+="Wsz^edzie, gdzie nie si^egniesz wzrokiem widzisz zielone "+
            "po^lacie bezkresnych ^l^ak. Bezwiednie ko^lysz^ace si^e na "+
            "wietrze ro^sliny u^spione i skulone spokojnie czekaj^a na "+
            "powr^ot s^lo^nca wraz z ^zyciodajnym ^swiat^lem. ",
            "Niska, jasnozielona murawa pokrywaj^aca delikatnym "+
            "kobiercem ziemi^e, przetykana gdzieniegdzie plamami "+
            "ciemniejszej zieleni k^ep jakiego^s zielska skrzy si^e "+
            "ca^la od rosy pokrywaj^acej ka^zd^a trawk^e i barwne "+
            "g^l^owki kwitn^acych kwiat^ow. W tej soczystej obfito^sci "+
            "ro^slin chroni^a si^e liczne owady, kt^orych jednostajne "+
            "brz^eczenie i cykanie wraz ze ^spiewem bujaj^acych wysoko "+
            "na niebie skowronk^ow tworzy niepowtarzaln^a muzyk^e "+
            "bezkresnych, zielonych ^l^ak rozci^agaj^acych si^e na "+
            "p^o^lnocy a^z po horyzont. "+
            "Dooko^la s^lycha^c ciche popiskiwanie "+
            "dzikich zwierz^atek, kt^ore zwinnie poruszaj^a si^e pomi^edzy "+
            "wysokimi zaro^slami, sprawnie omijaj^ac przer^o^zne przeszkody. ";
        }
        else if(pora_roku() == MT_LATO)
        {
            str+="Zapach siana, zi^o^l i kwiat^ow przesyca lekko, "+
            "rozgrzane powietrze pe^lne brz^eczenia i cykania "+
            "wsz^edobylskich owad^ow i ^swiergotliwego jazgotu "+
            "uganiaj^acych si^e za nimi ptak^ow. "+
            "Wsz^edzie, gdzie nie si^egniesz wzrokiem widzisz zielone po^lacie "+
            "bezkresnych ^l^ak. Kolorowe kwiaty, szumi^a cicho poruszone "+
            "podmuchem ciep^lego wiatru, kt^ory delikatnie muska twoj^a twarz, "+
            "przypominaj^ac o nieustaj^acym upale. "+
            "Drzewa rosn^ace wzd^lu^z widocznego "+
            "na po^ludniowym-wschodzie traktu pokryte g^estym listowiem "+
            "i kwiatami wydzielaj^a s^lodki intensywny zapach "+
            "Kolczaste zaro^sla na po^ludniowym-"+
            "zachodzie, "+
            "obsypane bia^lymi kwiatkami i czerwonymi owocami ca^le a^z "+
            "dr^z^a od bezustannego ptasiego ^swiergotu milkn^acego "+
            "tylko wtedy gdy na niebie pojawia si^e majestatyczna "+
            "sylwetka myszo^lowa. ";
        }
        else 
        {
            str+="Rozci^agaj^aca si^e a^z po horyzont, bezkresna r^ownina "+
            "pokryta namok^lymi trawami wydziela wo^n butwiej^acych "+
            "ro^slin, a wyp^lowia^le od wiatru i deszczu k^epy suchych "+
            "badyli, kt^ore opar^ly si^e apetytowi pas^acych si^e tutaj "+
            "zwierz^at chwiej^a si^e sm^etnie w podmuchach wiatru. "+
            "Dooko^la s^lycha^c ciche popiskiwanie "+
            "dzikich zwierz^atek, kt^ore zwinnie poruszaj^a si^e pomi^edzy "+
            "wysokimi zaro^slami, sprawnie omijaj^ac przer^o^zne przeszkody. "+
            "Dostojne drzewa chroni^ace drapie^znymi, "+
            "bezlistnymi paluchami ga^l^ezi trakt majacz^a za "+
            "butwiej^acymi ^l^akami gdzie^s na po^ludniu. "+
            "Co jaki^s czas na twoj^a twarz pada cie^n pochodz^acy "+
            "od przelatuj^acych ptak^ow ^spiesz^acych do pobliskiego lasu, "+
            "kt^orego zarys maluje si^e gdzie^s daleko na zachodzie. W "+
            "przeciwnym kierunku, ponad trawami i pojedynczymi krzakami, "+
            "wyłaniaj^a si^e mury miasta. ";
        }
    }
    else
    {
        if(CZY_JEST_SNIEG(this_object()))
        {
            str+="Wsz^edzie, gdzie nie si^egniesz wzrokiem widzisz pokryte puchow^a "+
           "pierzyn^a po^lacie bezkresnych ^l^ak. "+
           "Na po^ludniowym-wschodzie ten monotonny krajobraz przecina ciemna "+
           "kresa traktu okolonego rz^edami dostojnych, pot^e^znych drzew. "+
           "Pokryte ^sniegiem ro^sliny i wysokie trawy, kt^orych suche ko^nce "+
           "wystaj^a ponad bia^ly kożuch, z niecierpliwo^sci^a oczekuj^ac na "+
           "pierwsze oznaki wiosny. "+
           "Mroczny zarys pobliskiego "+
           "pobliskiego lasu maluje si^e gdzie^s daleko na zachodzie. "+
           "W przeciwnym kierunku, ponad trawami i pojedynczymi krzakami, "+
           "wyłaniaj^a si^e mury miasta.";

        }
        else if(pora_roku() == MT_WIOSNA)
        {
            str+=noc_ogolny;
        }
        else if(pora_roku() == MT_LATO)
        {
            str+=noc_ogolny+"Kwiaty szumi^a cicho, poruszone podmuchem "+
            "ch^lodnego wiatru, kt^ory delikatnie muska twoj^a twarz, "+
            "och^ladzaj^ac przy tym cia^lo po nieustaj^acym upale. "+
            "Zapach siana, zi^o^l i kwiat^ow przesyca lekko, "+
            "drgaj^ace ponad ^l^akami, rozgrzane powietrze pe^lne "+
            "cykania wsz^edobylskich owad^ow i pohukiwania s^ow. ";
        }

        else
        {
            str +=noc_ogolny+"Jak olbrzymie zwierz^e przyczajone w ciemno^sci majaczy po "+
            "wschodniej stronie r^owniny ciemny, zamglony "+
            "mur Rinde. Rozmi^ek^l^a, b^lotnist^a ziemi^e mlaszcz^ac^a "+
            "pod stopami przy najdrobniejszym poruszeniu pokrywa jaka^s "+
            "o^slizg^la mi^ekka masa. ";

        }
    }
    str+="\n";
    return str;
}
