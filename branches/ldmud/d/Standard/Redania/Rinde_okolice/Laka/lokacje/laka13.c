/* Autor: Veila
   Data : 25.09.2008
   Opis : Autor zbiorowy, przerobione z systemu losowego Very */
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <mudtime.h> 
#include <pogoda.h>
#include "dir.h"

inherit LAKA_RINDE_STD;

string dlugi_opis();

void create_laka() 
{
    set_short("Gdzie^s w^sr^od ^l^ak.");
    set_long("@@dlugi_opis@@");
    add_exit(LAKA_RINDE_LOKACJE + "laka17.c","sw",0,LAKA_RO_FATIG,1);
    add_exit(LAKA_RINDE_LOKACJE + "laka16.c","s",0,LAKA_RO_FATIG,1);
    add_exit(LAKA_RINDE_LOKACJE + "laka15.c","se",0,LAKA_RO_FATIG,1);
    add_exit(LAKA_RINDE_LOKACJE + "laka12.c","e",0,LAKA_RO_FATIG,1);
    add_exit(LAKA_RINDE_LOKACJE + "laka9.c","ne",0,LAKA_RO_FATIG,1);
    add_exit(LAKA_RINDE_LOKACJE + "laka10.c","n",0,LAKA_RO_FATIG,1);
    add_prop(ROOM_I_INSIDE,0);
}

string
dlugi_opis()
{
    string str = "";
        
    string noc_ogolny = "Ciemno�� tylko pot�guje "+
            "monotoni� tego miejsca. Jest tak "+
            "ciemno, �e nic praktycznie nie wida�, jedynie w odleg�o�ci nie "+
            "wi�kszej ni� wyci�gni�cie r�ki - jakie� ro�liny i pozamykane "+
            "na noc p�czki kwiat�w. Nietrudno wdepn�� w dziur�, czy te� "+
            "odchody dzikich zwierz�t. O tym, �e jeste� na ��ce "+
            "przypominaj� jedynie pl�cz�ce si� wok� n�g �odygi ro�lin, "+
            "jednak w ciemno�ciach nie spos�b rozpozna� ich gatunk�w. "+
            "Gdzie� na wschodzie majacz� mury miejskie, od "+
            "kt�rych bije niedu�a �una pochodni i latar� ulicznych. ";

    if(jest_dzien() == 1)
    {
        if(CZY_JEST_SNIEG(this_object()))
        {
            str+="Monotonia ��ki zim� jest beznami�tna. "+
            "W chwilach, gdy na niebie pojawia si� s�o�ce, jego promienie "+
            "dodaj� temu miejscu nieco rado�ci i �ycia - odbijaj� si� weso�o "+
            "na �niegu, skrz�c niczym ma�e brylanciki. " +
            "Gruba warstwa zlodowacia^lego ^sniegu pokrywa ^l^aki poczynaj^ac "+
            "od muru Rinde na wschodzie, a^z do lasu ciemniej^acego na "+
            "po^ludniowym-zachodzie. " +
            "Co jaki^s czas na twoj^a twarz pada cie^n pochodz^acy od "+
            "przelatuj^acych ptak^ow ^spiesz^acych do pobliskiego lasu, "+
            "kt^orego zarys maluje si^e gdzie^s daleko na zachodzie. W przeciwnym "+
            "kierunku, ponad trawami i pojedynczymi krzakami, wy�aniaj^a "+
            "si^e mury miasta. " +
            "Pokryte ^sniegiem ro^sliny i wysokie trawy, kt^orych suche ko^nce "+
            "wystaj^a ponad bia^ly ko�uch, z niecierpliwo^sci^a oczekuj^ac na "+
            "pierwsze oznaki wiosny. " +
            "Mroczny zarys pobliskiego "+
            "pobliskiego lasu maluje si^e gdzie^s daleko na zachodzie. ";
                    
        }
        else if(pora_roku() == MT_WIOSNA)
        {
            str+="To miejsce jest paradoksalne - przenikaj� si� tu "+
            "monotonia i r�norodno��. Nie jest w �aden spos�b "+
            "charakterystyczne, wr�cz przeciwnie - wszystko jest takie same "+
            "- wok� rozpo�ciera si� ogromna ��ka, a dopiero gdzie� na "+
            "po�udniowo-zachodniej cz�ci horyzontu widnieje las. "+
            "R�norodno�� za� zawiera si� w niezwyk�ej rozmaito�ci ro�lin "+
            "i barw przeplataj�cych si� nawzajem. Jest tu ca�e mn�stwo "+
            "mak�w i b�awatk�w, poprzetykanych ��t� naw�oci� i "+
            "zielonkawymi k�osami traw. "+
            "Niewysokie, spl^atane zaro^sla pokryte zaledwie "+
            "mgie^lk^a zieleni tworz^a na po^ludniowym-zachodzie, na "+
            "skraju por^eby "+
            "zwarty mur daj^acy schronienie wielu ptakom i drobnej "+
            "zwierzynie. "+
            "Rz^ad dostojnych, grubych drzew okrytych "+
            "drobnymi, zielonymi listeczkami wyznacza ju^z z daleka "+
            "tras^e traktu majacz^acego w oddali na po^ludniowym-"+
            "wschodzie.";
        }
        else if(pora_roku() == MT_LATO)
        {
            str+="Zapach siana, zi^o^l i kwiat^ow przesyca lekko, "+
            "rozgrzane powietrze pe^lne brz^eczenia i cykania "+
            "wsz^edobylskich owad^ow i ^swiergotliwego jazgotu "+
            "uganiaj^acych si^e za nimi ptak^ow. "+
            "Wsz^edzie, gdzie nie si^egniesz wzrokiem widzisz zielone po^lacie "+
            "bezkresnych ^l^ak. Kolorowe kwiaty, szumi^a cicho poruszone "+
            "podmuchem ciep^lego wiatru, kt^ory delikatnie muska twoj^a twarz, "+
            "przypominaj^ac o nieustaj^acym upale. "+
            "Drzewa rosn^ace wzd^lu^z widocznego "+
            "na po^ludniowym-wschodzie traktu pokryte g^estym listowiem "+
            "i kwiatami wydzielaj^a s^lodki intensywny zapach "+
            "Kolczaste zaro^sla na po^ludniowym-"+
            "zachodzie, "+
            "obsypane bia^lymi kwiatkami i czerwonymi owocami ca^le a^z "+
            "dr^z^a od bezustannego ptasiego ^swiergotu milkn^acego "+
            "tylko wtedy gdy na niebie pojawia si^e majestatyczna "+
            "sylwetka myszo^lowa. ";
        }
        else
        {
            str+="Wsz^edzie, gdzie nie si^egniesz wzrokiem widzisz ^z^o^lte po^lacie "+
            "bezkresnych ^l^ak. "+
            "Jak olbrzymie zwierz^e przyczajone w ciemno^sci majaczy po "+
            "wschodniej stronie r^owniny ciemny, zamglony "+
            "mur Rinde. Rozmi^ek^l^a, b^lotnist^a ziemi^e mlaszcz^ac^a "+
            "pod stopami przy najdrobniejszym poruszeniu pokrywa jaka^s "+
            "o^slizg^la mi^ekka masa. "+
            "Ja^sniejsz^a plam^a majacz^a na "+
            "tle ciemnego lasu kolczaste zaro^sla le^z^ace na skraju "+
            "por^eby. Dostojne drzewa chroni^ace drapie^znymi, "+
            "bezlistnymi paluchami ga^l^ezi trakt majacz^a za "+
            "butwiej^acymi ^l^akami na po�udniu. "+
            "Co jaki^s czas na twoj^a twarz pada cie^n pochodz^acy "+
            "od przelatuj^acych ptak^ow ^spiesz^acych do pobliskiego lasu, "+
            "kt^orego zarys maluje si^e gdzie^s daleko na zachodzie. W "+
            "przeciwnym kierunku, ponad trawami i pojedynczymi krzakami, "+
            "wy�aniaj^a si^e mury miasta. ";
        }
    }
    else
    {
        if(CZY_JEST_SNIEG(this_object()))
        {
            str+="Monotonia ��ki zim� jest beznami�tna. "+
           "Gruba warstwa zlodowacia^lego ^sniegu pokrywa ^l^aki poczynaj^ac "+
           "od muru Rinde na wschodzie, a^z do lasu ciemniej^acego na "+
           "po^ludniowym-zachodzie. "+
           "Nieliczne tropy zwierz^at, kieruj^ace si^e g^l^ownie w stron^e "+
           "niewielkiej zaspy le^z^acej na skraju lasu tworz^a na tej "+
           "ol^sniewaj^acej powierzchni asymetryczny wz^or. "+
           "Wok� roztacza si� ogromna �nie�na pustynia. "+
           "Pomijaj�c mury miasta w dali na "+
           "wschodzie, od kt�rych bije niedu�a �una pochodni i "+
           "latar� ulicznych, okolica wygl�da na wymar��.";

        }
        else if(pora_roku() == MT_WIOSNA)
        {
            str+="Niska, ciemna murawa pokrywaj^aca delikatnym kobiercem "+
            "ziemi^e, przetykana gdzieniegdzie plamami ciemniejszej "+
            "zieleni k^ep jakiego^s zielska l^sni w blasku ksi^e^zyca "+
            "od rosy pokrywaj^acej ka^zd^a trawk^e i barwne g^l^owki "+
            "kwitn^acych kwiat^ow. W tej soczystej obfito^sci ro^slin "+
            "chroni^a si^e liczne owady, kt^orych jednostajne cykanie "+
            "wraz z pohukiwaniem przelatuj^acych bezszelestnie s^ow "+
            "tworzy niepowtarzaln^a muzyk^e bezkresnych, ciemnych ^l^ak "+
            "rozci^agaj^acych si^e na p^o^lnocy a^z po horyzont. "+
            "Bezkres tej r^owniny m^aci tylko majacz^acy na "+
            "wschodzie ciemny mur Rinde. Niewysokie, "+
            "spl^atane zaro^sla majacz^ace ciemn^a plam^a na tle lasu "+
            "na po^ludniowym-zachodzie daj^a schronienie wielu ptakom i "+
            "drobnej "+
            "zwierzynie. Doskonale widoczny w blasku ksi^e^zyca rz^ad "+
            "dostojnych, grubych drzew wyznacza ju^z z daleka tras^e "+
            "traktu majacz^acego w oddali na po^ludniowym-"+
            "wschodzie. ";

        }
        else if(pora_roku() == MT_LATO)
        {
            str+=noc_ogolny+"Kolczaste zaro^sla na "+
            "wschodzie, majacz^ace ciemn^a plam^a na tle ponurego lasu "+
            "na po^ludniowym-zachodzie ^spi^a teraz spokojnie. Drzewa "+
            "rosn^ace "+
            "wzd^lu^z widocznego na po^ludniowym-wschodzie traktu "+
            "b^lyszcz^ace jak srebrne kolumny w blasku ksi^e^zyca "+
            "wydzielaj^a s^lodki intensywny zapach docieraj^acy "+
            "chwilami z podmuchami wiatru a^z tu. ";
        }

        else
        {
            str +=noc_ogolny;
        }
    }
    str+="\n";
    return str;
}
