/* Autor: Veila
   Data : 25.09.2008
   Opis : Autor zbiorowy, przerobione z systemu losowego Very */
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <mudtime.h> 
#include <pogoda.h>
#include "dir.h"

inherit LAKA_RINDE_STD;

string dlugi_opis();

void create_laka() 
{
    set_short("^L^aka na skraju por^eby");
    set_long("@@dlugi_opis@@");
    add_exit(LAS_RINDE_LOKACJE +"wlas9.c","w",0,LAKA_RO_FATIG,1);
    add_exit(LAS_RINDE_LOKACJE +"wlas12.c","sw",0,LAKA_RO_FATIG,1);
    add_exit(LAKA_RINDE_LOKACJE + "laka38.c","nw",0,LAKA_RO_FATIG,1);
    add_exit(LAKA_RINDE_LOKACJE + "laka37.c","n",0,LAKA_RO_FATIG,1);
    add_exit(LAKA_RINDE_LOKACJE + "laka36.c","ne",0,LAKA_RO_FATIG,1);
    add_exit(LAKA_RINDE_LOKACJE + "laka42.c","e",0,LAKA_RO_FATIG,1);
    add_exit(LAKA_RINDE_LOKACJE + "laka48.c","se",0,LAKA_RO_FATIG,1);
    add_exit(LAKA_RINDE_LOKACJE + "laka49.c","s",0,LAKA_RO_FATIG,1);

    add_prop(ROOM_I_INSIDE,0);

}

string
dlugi_opis()
{
    string str = "";
        
    string noc_ogolny = "Ciemność tylko potęguje "+
            "monotonię tego miejsca. Jest tak "+
            "ciemno, że nic praktycznie nie widać, jedynie w odległości nie "+
            "większej niż wyciągnięcie ręki - jakieś rośliny i pozamykane "+
            "na noc pączki kwiatów. Nietrudno wdepnąć w dziurę, czy też "+
            "odchody dzikich zwierząt. O tym, że jesteś na łące "+
            "przypominają jedynie plączące się wokół nóg łodygi roślin, "+
            "jednak w ciemnościach nie sposób rozpoznać ich gatunków. "+
            "Gdzieś na wschodzie majaczą mury miejskie, od "+
            "których bije nieduża łuna pochodni i latarń ulicznych. ";

    if(jest_dzien() == 1)
    {
        if(CZY_JEST_SNIEG(this_object()))
        {
            str+="Dooko^la s^lycha^c ciche popiskiwanie dzikich zwierz^atek, "+
            "kt^ore zwinnie poruszaj^a si^e pomi^edzy wysokimi zaro^slami, "+
            "sprawnie omijaj^ac przer^o^zne przeszkody. "+
            "Na po^ludniowym-wschodzie ten monotonny krajobraz przecina ciemna "+
            "kresa traktu okolonego rz^edami dostojnych, pot^e^znych drzew. "+
            "Co jaki^s czas na twoj^a twarz pada cie^n pochodz^acy od "+
            "przelatuj^acych ptak^ow ^spiesz^acych do pobliskiego lasu, "+
            "kt^orego zarys maluje si^e gdzie^s daleko na zachodzie. W przeciwnym "+
            "kierunku, ponad trawami i pojedynczymi krzakami, wyłaniaj^a "+
            "si^e mury miasta. "+
            "Nieliczne tropy zwierz^at, kieruj^ace si^e g^l^ownie w stron^e "+
            "niewielkiej zaspy le^z^acej na skraju lasu tworz^a na tej "+
            "ol^sniewaj^acej powierzchni asymetryczny wz^or. "+
            "Pomijając mury miasta w dali na "+
            "wschodzie, od których bije nieduża łuna pochodni i "+
            "latarń ulicznych, okolica wygląda na wymarłą.";
          
        }
        else if(pora_roku() == MT_WIOSNA)
        {
            str+="To miejsce jest paradoksalne - przenikają się tu "+
            "monotonia i różnorodność. Nie jest w żaden sposób "+
            "charakterystyczne, wręcz przeciwnie - wszystko jest takie same "+
            "- wokół rozpościera się ogromna łąka, a dopiero gdzieś na "+
            "południowo-zachodniej części horyzontu widnieje las. "+
            "Różnorodność zaś zawiera się w niezwykłej rozmaitości roślin "+
            "i barw przeplatających się nawzajem. Jest tu całe mnóstwo "+
            "maków i bławatków, poprzetykanych żółtą nawłocią i "+
            "zielonkawymi kłosami traw. "+
            "Niewysokie, spl^atane zaro^sla pokryte zaledwie "+
            "mgie^lk^a zieleni tworz^a na po^ludniowym-zachodzie, na "+
            "skraju por^eby "+
            "zwarty mur daj^acy schronienie wielu ptakom i drobnej "+
            "zwierzynie. "+
            "Rz^ad dostojnych, grubych drzew okrytych "+
            "drobnymi, zielonymi listeczkami wyznacza ju^z z daleka "+
            "tras^e traktu majacz^acego w oddali na po^ludniowym-"+
            "wschodzie.";
        }
        else if(pora_roku() == MT_LATO)
        {
            str+="Wsz^edzie, gdzie nie si^egniesz wzrokiem widzisz zielone po^lacie "+
            "bezkresnych ^l^ak. Kolorowe kwiaty, szumi^a cicho poruszone "+
            "podmuchem ciep^lego wiatru, kt^ory delikatnie muska twoj^a twarz, "+
            "przypominaj^ac o nieustaj^acym upale. "+
            "Co jaki^s czas na twoj^a twarz pada cie^n pochodz^acy "+
            "od przelatuj^acych ptak^ow ^spiesz^acych do pobliskiego lasu, "+
            "kt^orego zarys maluje si^e gdzie^s daleko na zachodzie. "+
            "Drzewa rosn^ace wzd^lu^z widocznego "+
            "na po^ludniowym-wschodzie traktu pokryte g^estym listowiem "+
            "i kwiatami wydzielaj^a s^lodki intensywny zapach "+
            "docieraj^acy chwilami z podmuchami wiatru a^z tu. "+
            "Kolczaste zaro^sla na po^ludniowym-"+
            "zachodzie, "+
            "obsypane bia^lymi kwiatkami i czerwonymi owocami ca^le a^z "+
            "dr^z^a od bezustannego ptasiego ^swiergotu milkn^acego "+
            "tylko wtedy gdy na niebie pojawia si^e majestatyczna "+
            "sylwetka myszo^lowa. ";
        }
        else
        {
            str+="Wsz^edzie, gdzie nie si^egniesz wzrokiem widzisz ^z^o^lte po^lacie "+
            "bezkresnych ^l^ak. "+
            "Jak olbrzymie zwierz^e przyczajone w ciemno^sci majaczy po "+
            "wschodniej stronie r^owniny ciemny, zamglony "+
            "mur Rinde. Rozmi^ek^l^a, b^lotnist^a ziemi^e mlaszcz^ac^a "+
            "pod stopami przy najdrobniejszym poruszeniu pokrywa jaka^s "+
            "o^slizg^la mi^ekka masa. "+
            "Ja^sniejsz^a plam^a majacz^a na "+
            "tle ciemnego lasu kolczaste zaro^sla le^z^ace na skraju "+
            "por^eby. Dostojne drzewa chroni^ace drapie^znymi, "+
            "bezlistnymi paluchami ga^l^ezi trakt majacz^a za "+
            "butwiej^acymi ^l^akami na południu. "+
            "Co jaki^s czas na twoj^a twarz pada cie^n pochodz^acy "+
            "od przelatuj^acych ptak^ow ^spiesz^acych do pobliskiego lasu, "+
            "kt^orego zarys maluje si^e gdzie^s daleko na zachodzie. W "+
            "przeciwnym kierunku, ponad trawami i pojedynczymi krzakami, "+
            "wyłaniaj^a si^e mury miasta. ";
        }
    }
    else
    {
        if(CZY_JEST_SNIEG(this_object()))
        {
            str+="Monotonia łąki zimą jest beznamiętna. "+
           "Gruba warstwa zlodowacia^lego ^sniegu pokrywa ^l^aki poczynaj^ac "+
           "od muru Rinde na wschodzie, a^z do lasu ciemniej^acego na "+
           "po^ludniowym-zachodzie. "+
           "Nieliczne tropy zwierz^at, kieruj^ace si^e g^l^ownie w stron^e "+
           "niewielkiej zaspy le^z^acej na skraju lasu tworz^a na tej "+
           "ol^sniewaj^acej powierzchni asymetryczny wz^or. "+
           "Wokół roztacza się ogromna śnieżna pustynia. "+
           "Pomijając mury miasta w dali na "+
           "wschodzie, od których bije nieduża łuna pochodni i "+
           "latarń ulicznych, okolica wygląda na wymarłą.";
        }
        else if(pora_roku() == MT_WIOSNA)
        {
            str+=noc_ogolny;
        }
        else if(pora_roku() == MT_LATO)
        {
            str+=noc_ogolny+"Kolczaste zaro^sla na "+
            "wschodzie, majacz^ace ciemn^a plam^a na tle ponurego lasu "+
            "na po^ludniowym-zachodzie ^spi^a teraz spokojnie. Drzewa "+
            "rosn^ace "+
            "wzd^lu^z widocznego na po^ludniowym-wschodzie traktu "+
            "b^lyszcz^ace jak srebrne kolumny w blasku ksi^e^zyca "+
            "wydzielaj^a s^lodki intensywny zapach docieraj^acy "+
            "chwilami z podmuchami wiatru a^z tu. ";
        }

        else
        {
            str +=noc_ogolny;
        }
    }
    str+="\n";
    return str;
}
