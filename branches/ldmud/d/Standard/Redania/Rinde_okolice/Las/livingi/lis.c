/* Autor: Avard
   Opis : Tinardan
   Data : 24.11.06 */

inherit "/std/zwierze";
inherit "/std/act/action";
inherit "/std/act/trigaction.c";

#include <macros.h>
#include <stdproperties.h>
#include <pl.h>
#include <formulas.h>
#include <std.h>
#include <composite.h>
#include <filter_funs.h>
#include <cmdparse.h>
#include <mudtime.h>
#include <wa_types.h>
#include <ss_types.h>
#include <object_types.h>
#include "dir.h"

void
create_zwierze()
{
    ustaw_odmiane_rasy("lis");
    set_gender(G_MALE);

    set_long("@@dlugasny@@");

    random_przym("okr^aglutki:okr^aglutcy wychudzony:wychudzeni "+
        "drobny:drobni grubiutki:grubiutcy||rudy:rudzi "+
        "wylinia^ly:wyliniali||m^lody:m^lodzi stary:starzy||"+
        "ruchliwy:ruchliwi spokojny:spokojni",2);

    set_act_time(30);
    add_act("emote przypatruje ci si^e uwa^znie.");
    add_act("emote zamiata ziemi^e rud^a kit^a.");
    add_act("emote strzy^ze niewinnie uszami.");
    add_act("emote fuka gniewnie.");

    set_stats (({15, 30, 15, 20, 30}));

    add_prop(LIVE_I_NEVERKNOWN, 1);
    add_prop(CONT_I_WEIGHT, 6000);
    add_prop(CONT_I_HEIGHT, 30);

    set_attack_unarmed(0, 15, 15, W_SLASH,   30, "�apa", "lewy",  "pazury");
    set_attack_unarmed(1, 15, 15, W_SLASH,   30, "�apa", "prawy", "pazury");
    set_attack_unarmed(2, 15, 15, W_IMPALE,  40, "z�by");

    set_hitloc_unarmed(0, ({ 1, 1, 1 }), 10, "g�owa");
    set_hitloc_unarmed(1, ({ 1, 1, 1 }), 30, "tu��w");
    set_hitloc_unarmed(2, ({ 1, 1, 1 }), 20, "�apa", ({"lewy", "przedni"}));
    set_hitloc_unarmed(3, ({ 1, 1, 1 }), 20, "�apa", ({"prawy", "przedni"}));
    set_hitloc_unarmed(4, ({ 1, 1, 1 }), 10, "�apa", ({"tylny", "lewy"}));
    set_hitloc_unarmed(5, ({ 1, 1, 1 }), 10, "�apa", ({"tylny", "przedni"}));

    set_random_move(100);
    set_restrain_path(LAS_RINDE_LOKACJE);
}
void
emote_hook(string emote, object wykonujacy, string przyslowek)
{
    switch (emote)
    {
        case "kopnij":
        case "spoliczkuj":
        case "opluj":
        case "nadepnij":
        case "poca^luj":
        case "przytul":
        case "pog^laszcz":
        case "szturchnij":
            set_alarm(1.0, 0.0, "zly", wykonujacy);
            break;
    }
}

void
zly(object kto)
{
   TO->run_away();
}

void
attacked_by(object wrog)
{
    ::attacked_by(wrog);
}
string
dlugasny()
{
    string str;
    str ="P^lomienne futro i pot^e^zna kita to chyba jego wizyt^owka � "+
        "nikt nie mo^ze pomyli^c tego zwierz^ecia z ^zadnym innym. Bystre "+
        "oczy rozgl^adaj^a si^e uwa^znie. Cia^lo na kr^otkich n^o^zkach "+
        "jest niemal przygi^ete do ziemi. Lis przemyka szybko, czujnie "+
        "spogl^adaj^ac na boki. Spiczasta mordka nadaje mu wyraz m^adro^sci "+
        "i sprytu. Bia^ly ko^lnierz pod brod^a mocno kontrastuje z "+
        "pomara^nczowym futrem porastaj^acym pozosta^le cz^e^sci cia^la. "+
        "Mimo ognistego koloru, gdyby nie spiczaste uszy, trudno by go "+
        "by^lo zauwa^zy^c � chowa si^e to ";

    if(environment(this_object())->pora_roku() != MT_ZIMA)
        str +="w trawie, ";
    else
        str +="za ^sniegow^a zasp^a, ";

    str +="to za wi^ekszym pniakiem.\n";
    return str;
}
