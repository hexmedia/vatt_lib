/* Autor: Avard
   Opis : Eria
   Data : 17.03.07 */

inherit "/std/zwierze";
inherit "/std/act/action";
inherit "/std/act/trigaction.c";

#include <macros.h>
#include <stdproperties.h>
#include <pl.h>
#include <formulas.h>
#include <std.h>
#include <composite.h>
#include <filter_funs.h>
#include <cmdparse.h>
#include <mudtime.h>
#include <wa_types.h>
#include <ss_types.h>
#include <object_types.h>
#include "dir.h"

void
create_zwierze()
{
    ustaw_odmiane_rasy("jele�");
    set_gender(G_MALE);

    set_long("Ten niezwykle pi^ekny okaz jelenia, kt^orego barwa zbli^zona "+
        "jest do ciemnego odcienia br^azu, rozgl^ada si^e nieufnie po "+
        "okolicy w poszukiwaniu potencjalnego zagro^zenia. Jego silnie "+
        "rozga^l^ezione poro^ze, dowodzi o tym, i^z jest to doros^ly "+
        "osobnik, kt^ory niejednego mo^ze odstraszy^c, a jego dumna "+
        "postawa dodatkowo mu w tym pomaga. Co jaki^s czas, zwierz^e "+
        "nachyla si^e ku ziemi w poszukiwaniu po^zywienia, po czym podnosi "+
        "si^e i zaczyna co^s prze^zuwa^c. Na jego g^ladkiej i l^sni^acej "+
        "sier^sci mo^zna zauwa^zy^c ta^ncz^ace promienie s^lo^nca, kt^ore "+
        "sprawiaj^a, ^ze wydaje si^e ona jeszcze bardziej cenna.\n");

    random_przym("wielki:wielcy pi^ekny:pi^ekni||br^azowy:br^azowi||m^lody:m^lodzi stary:starzy||"+
        "ruchliwy:ruchliwi spokojny:spokojni",2);

    set_act_time(30);
    add_act("emote skubie spokojnie mech i traw^e.");
    add_act("emote rozgl^ada si^e nieufnie.");
    add_act("emote podnosi g^low^e i w^eszy uwa^znie.");

    set_stats (({40, 40, 20, 25, 20}));

    add_prop(LIVE_I_NEVERKNOWN, 1);
    add_prop(CONT_I_WEIGHT, 320000+random(20000));
    add_prop(CONT_I_HEIGHT, 120+random(20));

    set_attack_unarmed(0, 10, 10, W_BLUDGEON,          20, "racica", ({"lewy",  "przedni"}));
    set_attack_unarmed(1, 10, 10, W_BLUDGEON,          20, "racica", ({"prawy", "przedni"}));
    set_attack_unarmed(2, 10, 10, W_BLUDGEON,           5, "racica", ({"lewy",  "tylny"}));
    set_attack_unarmed(3, 10, 10, W_BLUDGEON,           5, "racica", ({"prawy", "tylny"}));
    set_attack_unarmed(5, 10, 10, W_BLUDGEON|W_IMPALE, 50, "poro�e", ({"masywne", "szerokie"}));

    set_hitloc_unarmed(0, ({ 1, 1, 1 }), 10, "g�owa");
    set_hitloc_unarmed(1, ({ 1, 1, 1 }), 30, "tu��w");
    set_hitloc_unarmed(2, ({ 1, 1, 1 }), 20, "noga", ({"lewy", "przedni"}));
    set_hitloc_unarmed(3, ({ 1, 1, 1 }), 20, "noga", ({"prawy", "przedni"}));
    set_hitloc_unarmed(4, ({ 1, 1, 1 }), 10, "noga", ({"tylny", "lewy"}));
    set_hitloc_unarmed(5, ({ 1, 1, 1 }), 10, "noga", ({"tylny", "przedni"}));

    set_random_move(100);
    set_restrain_path(LAS_RINDE_LOKACJE);
}
void
emote_hook(string emote, object wykonujacy, string przyslowek)
{
    switch (emote)
    {
        case "kopnij":
        case "spoliczkuj":
        case "opluj":
        case "nadepnij":
        case "poca^luj":
        case "przytul":
        case "pog^laszcz":
        case "szturchnij":
            set_alarm(1.0, 0.0, "zly", wykonujacy);
            break;
    }
}

void
zly(object kto)
{
   //TO->run_away();
}

void
attacked_by(object wrog)
{
    ::attacked_by(wrog);
}
