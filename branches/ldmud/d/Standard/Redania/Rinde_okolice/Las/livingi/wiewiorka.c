/* Autor: Avard
   Opis : Eria
   Data : 24.11.06 */

inherit "/std/zwierze";
inherit "/std/act/action";
inherit "/std/act/trigaction.c";

#include <macros.h>
#include <stdproperties.h>
#include <pl.h>
#include <formulas.h>
#include <std.h>
#include <composite.h>
#include <filter_funs.h>
#include <cmdparse.h>
#include <mudtime.h>
#include <wa_types.h>
#include <ss_types.h>
#include <object_types.h>
#include "dir.h"

void
create_zwierze()
{
    ustaw_odmiane_rasy("wiewiorka");
    set_gender(G_MALE);

    set_long("To niewielkich rozmiar^ow zwierz^atko, kt^orego pyszczek i "+
        "w^asiki nieustannie si^e poruszaj^a jakby stara^ly si^e co^s "+
        "wyw^acha^c, bez w^atpienia jest najzwyklejsz^a w ^swiecie "+
        "wiewi^ork^a. Krotkie ^lapki zako^nczone s^a ostrymi pazurkami, "+
        "kt^orymi nerwowo przebiera. Ruda sier^s^c wiewi^orki jest "+
        "szorstka, jedynie puszysty, poka^xny ogonek stanowi wyj^atek od "+
        "tej regu^ly. Brzuszek gryzionia jest bia^ly, a reszta rudej "+
        "sier^sci przeplata si^e miejscami z tak^a o czarnej lub szarej "+
        "barwie. W^lochate uszy stworzenia, strzy^z^a lekko i nas^luchuj^a "+
        "zewsz^ad dobiegaj^acych odg^los^ow.");

    random_przym("puszysty:puszy^sci niewielki:niewielcy drobny:drobni "+
        "puchaty:puchaci mi�kki:mi^ekcy ma^ly:mali lekki:lekcy||rudy:rudzi "+
        "szorstkow^losy:szorstkow^losi||czarnooki:czarnoocy "+
        "p^lochliwy:p^lochliwi ruchliwy:ruchliwi",2);

    set_act_time(30);
    add_act("emote unosi ogon do g�ry.");
    add_act("emote umyka w podskokach par� krok�w po czym przystaje.");
    add_act("emote szybko obraca g�ow� w twoim kierunku.");

    set_stats (({1, 40, 1, 20, 1}));

    add_prop(LIVE_I_NEVERKNOWN, 1);
    add_prop(CONT_I_WEIGHT, 800);
    add_prop(CONT_I_HEIGHT, 10);
    add_prop(CONT_I_VOLUME, 800);

    //Wiewi�rki raczej nie walcz� tylko uciekaj�. [Krun]
#if 0
    set_attack_unarmed(0, 15, 15, W_SLASH,  50, "praw� �apk�");
    set_attack_unarmed(1, 15, 15, W_SLASH,  50, "lew� �apk�");
#endif

    set_hitloc_unarmed(0, ({ 1, 1, 1 }), 10, "g�owa");
    set_hitloc_unarmed(1, ({ 1, 1, 1 }), 30, "tu��w");
    set_hitloc_unarmed(2, ({ 1, 1, 1 }), 20, "�apka", ({"lewy", "przedni"}));
    set_hitloc_unarmed(3, ({ 1, 1, 1 }), 20, "�apka", ({"prawy", "przedni"}));
    set_hitloc_unarmed(4, ({ 1, 1, 1 }), 10, "�apka", ({"tylny", "lewy"}));
    set_hitloc_unarmed(5, ({ 1, 1, 1 }), 10, "�apka", ({"tylny", "przedni"}));


    set_random_move(1);
    set_restrain_path(LAS_RINDE_LOKACJE);

    add_leftover("/std/leftover", "ogon", 1, 0, 1, 1, 0, O_KOSCI);
}

void
emote_hook(string emote, object wykonujacy, string przyslowek)
{
    switch (emote)
    {
        case "kopnij":
        case "spoliczkuj":
        case "opluj":
        case "nadepnij":
        case "poca^luj":
        case "przytul":
        case "pog^laszcz":
        case "szturchnij":
            set_alarm(1.0, 0.0, "zly", wykonujacy);
            break;
    }
}

void
zly(object kto)
{
   TO->run_away();
}

#if 0
void
attacked_by(object wrog)
{
    TO->run_away();
    ::attacked_by(wrog);
}
#endif