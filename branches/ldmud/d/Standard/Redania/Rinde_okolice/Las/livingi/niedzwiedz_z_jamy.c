/*
 * Jest to uroczy mi� siedz�cy sobie w jamie w lesie okolic Rinde. ;)
 * Nie wychodzi stamt�d, �pi na zim�, a jama ta to chyba nasze pierwsze
 * expowisko!
 *
 * Misia napisa� Rantaur, popraweczki Very.
 */

inherit "/std/zwierze";
inherit "/std/act/action";

#include <macros.h>
#include <mudtime.h>
#include <ss_types.h>
#include <filter_funs.h>
#include <object_types.h>
#include <stdproperties.h>

int spi=1;

void create_zwierze()
{
    ustaw_odmiane_rasy("niedzwiedz");
    set_gender(G_MALE);

    dodaj_przym("wielki", "wielcy");
    dodaj_przym("br�zowy", "br�zowi");

    set_long("@@dlugi@@");

    set_act_time(30);
    add_act("@@eventy@@");

    set_cact_time(25);
    add_cact("emote ryczy przera�liwie.");
    add_cact("emote na kr�tk� chwil� staje na dw�ch �apach prezentuj�c swoj� wielko��.");
    add_cact("emote charczy nerwowo.");
    add_cact("emote miota si� w�ciekle.");
    add_cact("emote wycofuje si� o kilka krok�w.");

    add_prop(CONT_I_WEIGHT, 600000);
    add_prop(CONT_I_VOLUME, 450000);
    add_prop(NPC_I_NO_RUN_AWAY,1);
    add_prop(LIVE_I_SEE_DARK,1);

    set_aggressive("@@czy_atakujemy@@");

    set_stats(({64+random(15), 30+random(10), 80+random(20), 25, 82+random(7)}));
    set_skill(SS_DEFENCE, 25+random(5));
    set_skill(SS_UNARM_COMBAT, 50+random(15));
    set_skill(SS_BLIND_COMBAT, 59+random(10));
    set_skill(SS_AWARENESS, 44+random(4));

    set_attack_unarmed(0, 15, 15, W_SLASH,  40, "�apa", "lewy",  "pazury");
    set_attack_unarmed(1, 15, 15, W_SLASH,  40, "�apa", "prawy", "pazury");
    set_attack_unarmed(2, 15, 15, W_IMPALE, 20, "k�y");

    set_hitloc_unarmed(0, ({ 1, 1, 1 }), 10, "�eb");
    set_hitloc_unarmed(1, ({ 1, 1, 1 }), 30, "tu��w");
    set_hitloc_unarmed(2, ({ 1, 1, 1 }), 20, "�apa", ({"lewy", "przedni"}));
    set_hitloc_unarmed(3, ({ 1, 1, 1 }), 20, "�apa", ({"prawy", "przedni"}));
    set_hitloc_unarmed(4, ({ 1, 1, 1 }), 10, "�apa", ({"tylny", "lewy"}));
    set_hitloc_unarmed(5, ({ 1, 1, 1 }), 10, "�apa", ({"tylny", "przedni"}));

    set_alarm(1.0, 0.0, "czy_isc_spac");
    set_alarm_every_hour("czy_isc_spac");

    add_leftover("/std/leftover", "kie^l", 4, 0, 1, 1, 0, O_KOSCI);
    add_leftover("/std/leftover", "czaszka", 1, 0, 1, 1, 0, O_KOSCI);
    add_leftover("/std/leftover", "serce", 1, 0, 0, 0, 0, O_JEDZENIE);
}

string dlugi()
{
    if(spi)
        return "Masywne cielsko porusza si� miarowo"
                +" w rytm oddechu zwierz�cia. �pi�cy"
                +" nied�wied� przypomina, z pozoru niegro�n�,"
                +" kup� furta, jednak wolisz nie my�le� jak"
                +" zachowa�by si� gdyby kto� raczy� przeszkodzi�"
                +" mu w wypoczynku. Jego kr�tkie, p�okr�g�e uszy"
                +" od czasu do czasu poruszaj� si� zupe�nie jakby"
                +" nas�uchiwa�y przybycia owego wichrzyciela.\n";
    else
        return "L�ni�ce, br�zowe futro mieni si� refleksami przy"
                +" ka�dym ruchu zwierz�cia. Mimo, i� sprawiaj� one"
                +" wra�enie powolnych i leniwych to z pewno�ci�"
                +" nied�wied� jest te� zdolny do sprawnego ataku na"
                +" upatrzon� ofiar�, b�dz co b�dz wielu ju� zgin�o"
                +" rozszarpanych w�a�nie nied�wiedzimi pazurami."
                +" Jego czarne oczy wodz� niespokojnie po okolicy, a"
                +" z masywnego pyska od czasu do czasu wydobywa si�"
                +" cichy pomruk.\n";
}

string eventy()
{
    string *spi = ({"emote przewraca si� na drugi bok.",
                    "emote pomrukuje cicho.",
                    "emote przekr�ca lekko �eb i wydaje z siebie"
                    +" seri� rytmicznych chrapni��.",
                    "emote nieznacznie poruszy� uszami."});

    string *niespi = ({"emote leniwie grzebie �ap� w ziemi.",
                    "emote mruczy niespokojnie.",
                    "emote zatrzymuje nagle wzrok w sobie tylko znanym punkcie.",
                    "emote kilkakrotnie ociera sw�j pysk �ap�."});

    if(spi)
    {
        int index = random(sizeof(spi));
        return spi[index];
    }
    else
    {
        int index = random(sizeof(niespi));
        return niespi[index];
    }
}

int czy_atakujemy()
{
   return !spi;
}

void set_spi(int i)
{
    spi = i;
}

void zaatakuj(object kogo)
{
    kogo->catch_msg(capitalize(short(kogo))+" nagle rzuca si� na ciebie!\n");
    saybb(capitalize(short(kogo))+" nagle rzuca si� na "+QIMIE(kogo, PL_BIE)+"!\n",
            kogo);
    attack_object(kogo);
}

void wstan()
{
    if(!spi)
        return;

    spi = 0;
    command("emote wydaje z siebie chrapliwy ryk, po czym leniwie staje na �apach.");

    if(query_attack() != 0)
        return;

    object *livingi = FILTER_LIVE(all_inventory(ENV(TO)));
    livingi -= ({TO});
    livingi = FILTER_CAN_SEE(livingi, TO);

    int s;
    if((s = sizeof(livingi)) > 0)
    {
        int i = random(s);
        set_alarm(2.0, 0.0, "zaatakuj", livingi[i]);
    }
}

void idz_spac()
{
    if(!spi)
        return;

    spi = 1;
    command("emote kr�ci si� przez chwil� po okolicy, po czym uk�ada si� do snu.");
}

void czy_isc_spac()
{
    if( (environment()->pora_roku() == MT_ZIMA) || (environment()->dzien_noc()) && (query_attack() == 0) )
        idz_spac();
    else
        wstan();
}

void attacked_by(object wrog)
{
    wstan();
    ::attacked_by(wrog);
}

void
emote_hook(string emote, object wykonujacy, string przyslowek)
{
    switch (emote)
    {
        case "kopnij":
        case "spoliczkuj":
        case "opluj":
        case "nadepnij":
        case "poca^luj":
        case "przytul":
        case "pog^laszcz":
        case "szturchnij":
            set_alarm(1.0, 0.0, "zly", wykonujacy);
            break;
    }
}

void zly(object kto)
{
    if(kto->query_skill(SS_ANI_HANDL) > 25+random(11))
        command(eventy());
    else
        wstan();
}
