
/* Autor: Avard
   Data : 10.03.07
   Opis : vortak */
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <pogoda.h>
#include "dir.h"

inherit WLAS_RINDE_STD;

void create_las() 
{
    set_short("Na skraju wykarczowanego lasu");
    add_exit(LAS_RINDE_LOKACJE + "wlas2.c","ne",0,WLAS_RO_FATIG,1);
    add_exit(LAS_RINDE_LOKACJE + "wlas5.c","e",0,WLAS_RO_FATIG,1);
    add_exit(LAS_RINDE_LOKACJE + "las1.c","se",0,WLAS_RO_FATIG,1);
    add_exit(LAS_RINDE_LOKACJE + "las2.c","sw",0,WLAS_RO_FATIG,1);
    add_prop(ROOM_I_INSIDE,0);
}
public string
exits_description() 
{
    return "Wykarczowany las wida^c na wschodzie i "+
        "p^o^lnocnym-wschodzie, za^s wejscie do lasu znajduje si^e na "+
        "po^ludniowym-zachodzie i po^ludniowym-wschodzie.\n";
}

string
dlugi_opis()
{
    string str;
    if(jest_dzien() == 1)
    {
        if(pora_roku() != MT_ZIMA)
        {
            str = "Na po�udnie od tego miejsca wyrasta pot^e^zna, zielona "+
            "^sciana lasu, dzie^n po dniu tocz^acego nier^owna walk^e "+
            "z pot^e^znymi ostrzami drwalskich topor^ow. Z pokrywaj^acej "+
            "ziemi^e grubej warstwy le^snej ^sci^o^lki wy^laniaj^a si^e "+
            "pniaki - smutni ^swiadkowie czas^ow, gdy szumi^ace drzewa "+
            "dawa^ly schronienie drobnej zwierzynie. Jednak przyroda nie "+
            "poddaje si^e - tu i ^owdzie wy^laniaj^a si^e kar^lowate krzewy,"+
            " gdzieniegdzie wida^c m^lode, si^egaj^ace nie wy^zej ni^z do "+
            "pasa drzewka - najlepszy dow^od na to, ^ze wcze^sniej czy "+
            "p^o^xniej las si^e odrodzi. Cichy szum lasu i s^lyszalny "+
            "wci^a^z ^spiew ptak^ow zag^lusza tylko dobiegaj^acy z "+
            "po^ludnia cichy gwar - zapewne w^lasnie tam znajduje sie "+
            "tartak. Daleko na p�nocnym-wschodzie majacz^a, ledwo st^ad "+
            "widoczne, miejskie mury.";
        }
        else
        {
            str = "Na po�udnie od tego miejsca wyrasta pot^e^zna, snie^znobia^la "+
            "^sciana lasu, dzie^n po dniu tocz^acego nier^own^a walk^e z "+
            "pot^e^znymi ostrzami drwalskich topor^ow. Z grubej warstwy "+
            "^sniegu zalegaj^acego na ziemi wy^laniaj^a si^e pniaki - "+
            "smutni ^swiadkowie czas^ow, gdy szumi^ace drzewa dawa^ly "+
            "schronienie drobnej zwierzynie. Jednak przyroda nie poddaje "+
            "si^e - tu i ^owdzie wy^l^aniaj^a sie kar^lowate krzewy, "+
            "gdzieniegdzie wida^c m^lode, si^egaj^ace nie wy^zej ni^z do "+
            "pasa drzewka - najlepszy dow^od na to, ^ze wcze^sniej czy "+
            "p^o^xniej las si^e odrodzi. G^lo^sne krakanie wrony zag^lusza "+
            "od czasu do czasu dobiegaj^acy z po^ludnia cichy gwar - "+
            "zapewne w^lasnie tam znajduje si^e tartak. Daleko na "+
            "p�nocnym-wschodzie majacz^a, ledwo st^ad widoczne, miejskie "+
            "mury.";
         }
    }
    if(jest_dzien() == 0)
    {
        if(pora_roku() != MT_ZIMA)
        {
            str = "Na po�udnie od tego miejsca wyrasta pot^e^zna, zielona "+
            "^sciana lasu, dzie^n po dniu tocz^acego nier^owna walk^e z "+
            "pot^e^znymi ostrzami drwalskich topor^ow. Z pokrywaj^acej "+
            "ziemi^e grubej warstwy le^snej ^sci^o^lki wy^laniaj^a si^e "+
            "pniaki - smutni ^swiadkowie czas^ow, gdy szumi^ace drzewa "+
            "dawa^ly schronienie drobnej zwierzynie. Jednak przyroda nie "+
            "poddaje si^e - tu i ^owdzie wy^laniaj^a si^e kar^lowate "+
            "krzewy, gdzieniegdzie wida^c m^lode, si^egaj^ace nie wy^zej "+
            "ni^z do pasa drzewka - najlepszy dow^od na to, ^ze wcze^sniej "+
            "czy p^o^xniej las si^e odrodzi.  Daleko na p�nocnym-wschodzie "+
            "majacz^a, ledwo st^ad widoczne, miejskie mury.";
        }
        else
        {
            str = "Na po�udnie od tego miejsca wyrasta pot^e^zna, snie^znobia^la "+
            "^sciana lasu, dzie^n po dniu tocz^acego nier^own^a walk^e z "+
            "pot^e^znymi ostrzami drwalskich topor^ow. Z grubej warstwy "+
            "^sniegu zalegaj^acego na ziemi wy^laniaj^a si^e pniaki - "+
            "smutni ^swiadkowie czas^ow, gdy szumi^ace drzewa dawa^ly "+
            "schronienie drobnej zwierzynie. Jednak przyroda nie poddaje "+
            "si^e - tu i ^owdzie wy^l^aniaj^a sie kar^lowate krzewy, "+
            "gdzieniegdzie wida^c m^lode, si^egaj^ace nie wy^zej ni^z do "+
            "pasa drzewka - najlepszy dow^od na to, ^ze wcze^sniej czy "+
            "p^o^xniej las si^e odrodzi. Daleko na p�nocnym-wschodzie "+
            "majacz^a, ledwo st^ad widoczne, miejskie mury.";
         }
    }
    str += "\n";
    return str;
}
string
opis_nocy()
{
    string str;
    if(pora_roku() != MT_ZIMA)
    {
        str = "Na po�udnie od tego miejsca wyrasta pot^e^zna, zielona "+
            "^sciana lasu, dzie^n po dniu tocz^acego nier^owna walk^e z "+
            "pot^e^znymi ostrzami drwalskich topor^ow. Z pokrywaj^acej "+
            "ziemi^e grubej warstwy le^snej ^sci^o^lki wy^laniaj^a si^e "+
            "pniaki - smutni ^swiadkowie czas^ow, gdy szumi^ace drzewa "+
            "dawa^ly schronienie drobnej zwierzynie. Jednak przyroda nie "+
            "poddaje si^e - tu i ^owdzie wy^laniaj^a si^e kar^lowate "+
            "krzewy, gdzieniegdzie wida^c m^lode, si^egaj^ace nie wy^zej "+
            "ni^z do pasa drzewka - najlepszy dow^od na to, ^ze wcze^sniej "+
            "czy p^o^xniej las si^e odrodzi. Daleko na p�nocnym-wschodzie "+
            "majacz^a, ledwo st^ad widoczne, miejskie mury.";
    }
    else
    {
        str = "Na po�udnie od tego miejsca wyrasta pot^e^zna, snie^znobia^la "+
            "^sciana lasu, dzie^n po dniu tocz^acego nier^own^a walk^e z "+
            "pot^e^znymi ostrzami drwalskich topor^ow. Z grubej warstwy "+
            "^sniegu zalegaj^acego na ziemi wy^laniaj^a si^e pniaki - "+
            "smutni ^swiadkowie czas^ow, gdy szumi^ace drzewa dawa^ly "+
            "schronienie drobnej zwierzynie. Jednak przyroda nie poddaje "+
            "si^e - tu i ^owdzie wy^l^aniaj^a sie kar^lowate krzewy, "+
            "gdzieniegdzie wida^c m^lode, si^egaj^ace nie wy^zej ni^z do "+
            "pasa drzewka - najlepszy dow^od na to, ^ze wcze^sniej czy "+
            "p^o^xniej las si^e odrodzi. Daleko na p�nocnym-wschodzie "+
            "majacz^a, ledwo st^ad widoczne, miejskie mury.";
      }
    str += "\n";
    return str;
}
