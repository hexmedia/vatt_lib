/* Autor: Avard
   Data : 24.11.06
   Opis : Tinardan 
   TODO: Podpiac bagno */
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <pogoda.h>
#include "dir.h"
inherit LAS_RINDE_STD;
void create_las() 
{
    set_short("Moczary");
    add_exit(LAS_RINDE_LOKACJE + "las6.c","w",0,LAS_RO_FATIG,1);
    add_exit(LAS_RINDE_LOKACJE + "las12.c","s",0,LAS_RO_FATIG,1);
    
    add_prop(ROOM_I_INSIDE,0);
}
string
dlugi_opis()
{
    string str;
    if(jest_dzien() == 1)
    {
        if(pora_roku() != MT_ZIMA)
        {
            str = "Ziemia jest mi^ekka i faluje delikatnie przy ka^zdym, "+
                "nawet najdrobniejszym ruchu. Dziwaczne kwiaty, spl^atane "+
                "korzenie, k^epy topianu i tataraku, g^esta trawa, to tylko "+
                "wierzchnia warstwa, przykrywka. Pod spodem kot^luje si^e i "+
                "bulgoce, przelewa i chlupie bagnisko. Gdzieniegdzie "+
                "m^etna, czarna woda przebija si^e i wyp^lywa niewielkimi "+
                "ka^lu^zami. Drzewa rosn^a tu z rzadka, dziwaczne, "+
                "powyginane, o korzeniach wystaj^acych ponad powierzchni^e. "+
                "Ziemia gnie si^e pod ci^e^zarem ^zywych istot, wydaj^ac z "+
                "g^luche pomruki i wypuszczaj^ac niewielkie b^abelki, "+
                "zniekszta^lcaj^ace smolist^a czer^n bagiennych bajorek. Po "+
                "spl^atanej trawie przemykaj^a chy^lkiem drobne zwierz^eta, "+
                "na tyle lekkie, by bez problemu porusza^c si^e po "+
                "niepewnym gruncie. Ich drobne n^o^zki cichutko mlaszcz^a w "+
                "nasi^akni^etych wod^a, butwiej^acych ro^slinach i "+
                "cieniutkiej warstewce ziemi oddzielaj^acej ci^e od "+
                "mrocznej toni.";
        }
        else
        {
            str = "Wszystko, jak okiem si^egn^a^c okryte jest bia^lym "+
                "puchem. Najr^o^zniejsze, drobniutkie ^slady m^ac^a jego "+
                "nieskaziteln^a powierzchni^e, ^lami^a j^a szlaczkami "+
                "trop^ow. Drzewa wygl^adaj^a tu dziwacznie, poskr^ecane "+
                "konary wychylaj^a si^e spod ^sniegowych czap, a ich "+
                "korzenie wystaj^a z ziemi nadspodziewanie wysoko. Ten "+
                "sp^lachetek lasu wygl^ada na zak^atek spokojny i cichy, "+
                "miejsce, gdzie nic nikomu nie zagra^za. Jednak^ze ziemia "+
                "skrzypi dziwnie pod twoim ci^e^zarem i masz wra^zenie, "+
                "^ze delikatnie ugina si^e. Zupe^lnie, jakby pod spodem "+
                "zalega^la cieniutka warstwa lodu.";
       }
    }
    if(jest_dzien() == 0)
    {
        if(pora_roku() != MT_ZIMA)
        {
            str = "Ziemia jest mi^ekka i faluje delikatnie przy ka^zdym, "+
                "nawet najdrobniejszym ruchu. Dziwaczne kwiaty, spl^atane "+
                "korzenie, k^epy topianu i tataraku, g^esta trawa, to tylko "+
                "wierzchnia warstwa, przykrywka. Pod spodem kot^luje si^e i "+
                "bulgoce, przelewa i chlupie bagnisko. Gdzieniegdzie "+
                "m^etna, czarna woda przebija si^e i wyp^lywa niewielkimi "+
                "ka^lu^zami. Drzewa rosn^a tu z rzadka, dziwaczne, "+
                "powyginane, o korzeniach wystaj^acych ponad powierzchni^e. "+
                "Ziemia gnie si^e pod ci^e^zarem ^zywych istot, wydaj^ac z "+
                "g^luche pomruki i wypuszczaj^ac niewielkie b^abelki, "+
                "zniekszta^lcaj^ace smolist^a czer^n bagiennych bajorek. Po "+
                "spl^atanej trawie przemykaj^a chy^lkiem drobne zwierz^eta, "+
                "na tyle lekkie, by bez problemu porusza^c si^e po "+
                "niepewnym gruncie. Ich drobne n^o^zki cichutko mlaszcz^a w "+
                "nasi^akni^etych wod^a, butwiej^acych ro^slinach i "+
                "cieniutkiej warstewce ziemi oddzielaj^acej ci^e od "+
                "mrocznej toni.";
        }
        else
        {
            str = "Wszystko, jak okiem si^egn^a^c okryte jest bia^lym "+
                "puchem. Najr^o^zniejsze, drobniutkie ^slady m^ac^a jego "+
                "nieskaziteln^a powierzchni^e, ^lami^a j^a szlaczkami "+
                "trop^ow. Drzewa wygl^adaj^a tu dziwacznie, poskr^ecane "+
                "konary wychylaj^a si^e spod ^sniegowych czap, a ich "+
                "korzenie wystaj^a z ziemi nadspodziewanie wysoko. Ten "+
                "sp^lachetek lasu wygl^ada na zak^atek spokojny i cichy, "+
                "miejsce, gdzie nic nikomu nie zagra^za. Jednak^ze ziemia "+
                "skrzypi dziwnie pod twoim ci^e^zarem i masz wra^zenie, "+
                "^ze delikatnie ugina si^e. Zupe^lnie, jakby pod spodem "+
                "zalega^la cieniutka warstwa lodu.";
       }
    }
    str += "\n";
    return str;
}
string
opis_nocy()
{
    string str;
    if(pora_roku() != MT_ZIMA)
        {
            str = "Ziemia jest mi^ekka i faluje delikatnie przy ka^zdym, "+
                "nawet najdrobniejszym ruchu. Dziwaczne kwiaty, spl^atane "+
                "korzenie, k^epy topianu i tataraku, g^esta trawa, to tylko "+
                "wierzchnia warstwa, przykrywka. Pod spodem kot^luje si^e i "+
                "bulgoce, przelewa i chlupie bagnisko. Gdzieniegdzie "+
                "m^etna, czarna woda przebija si^e i wyp^lywa niewielkimi "+
                "ka^lu^zami. Drzewa rosn^a tu z rzadka, dziwaczne, "+
                "powyginane, o korzeniach wystaj^acych ponad powierzchni^e. "+
                "Ziemia gnie si^e pod ci^e^zarem ^zywych istot, wydaj^ac z "+
                "g^luche pomruki i wypuszczaj^ac niewielkie b^abelki, "+
                "zniekszta^lcaj^ace smolist^a czer^n bagiennych bajorek. Po "+
                "spl^atanej trawie przemykaj^a chy^lkiem drobne zwierz^eta, "+
                "na tyle lekkie, by bez problemu porusza^c si^e po "+
                "niepewnym gruncie. Ich drobne n^o^zki cichutko mlaszcz^a w "+
                "nasi^akni^etych wod^a, butwiej^acych ro^slinach i "+
                "cieniutkiej warstewce ziemi oddzielaj^acej ci^e od "+
                "mrocznej toni.";
        }
        else
        {
            str = "Wszystko, jak okiem si^egn^a^c okryte jest bia^lym "+
                "puchem. Najr^o^zniejsze, drobniutkie ^slady m^ac^a jego "+
                "nieskaziteln^a powierzchni^e, ^lami^a j^a szlaczkami "+
                "trop^ow. Drzewa wygl^adaj^a tu dziwacznie, poskr^ecane "+
                "konary wychylaj^a si^e spod ^sniegowych czap, a ich "+
                "korzenie wystaj^a z ziemi nadspodziewanie wysoko. Ten "+
                "sp^lachetek lasu wygl^ada na zak^atek spokojny i cichy, "+
                "miejsce, gdzie nic nikomu nie zagra^za. Jednak^ze ziemia "+
                "skrzypi dziwnie pod twoim ci^e^zarem i masz wra^zenie, "+
                "^ze delikatnie ugina si^e. Zupe^lnie, jakby pod spodem "+
                "zalega^la cieniutka warstwa lodu.";
       }
    str += "\n";
    return str;
}

/* Eventy w lato:
Jedno ze smolistych bajorek wydaje z siebie przeci�g�y bulgot. 
Co� szele�ci w k�pie tataraku. 
(a oto co si� dzieje, je�li kto� zatrzyma si� na tej lokacji: ) 
Zapadasz si� po kostki. Woda chlupocze cicho. (Mo�na wyj�� samemu) 
Czujesz jak spl�tane korzenie p�kaj� pod twoim ci�arem. Zapadasz si� po kolana. (Mo�na wyj�� samemu, po trzech pr�bach) 
Z nag�ym chlu�ni�ciem zapadasz si� w bagnie a� po biodra. Pod stopami nie czujesz gruntu. (Je�li masz du�o si�y, mo�na wyj�� po czterech pr�bach, albo je�li kto� rzuci ci lin�. Je�li nie, a pr�bujesz wyj�� samemu: Bez�adna szamotanina powoduje, �e zapadasz si� jeszcze g��biej) 
Bagno wsysa ci� i orientujesz si�, �e jeste� zatopiony/a ju� po piersi. (mo�na wyj��, je�li kto� rzuci ci lin�) 
Ciemna woda dosi�ga twojej szyi. (Je�li kto� rzuci ci lin�, masz 70% szans, ze uda ci si� j� z�apa� i wyj��) 
Czarna to� zamyka ci si� nad g�ow�. Przez kilka chwil twoje �ci�ni�te p�uca walcz� o oddech. (pies pogrzebany) 
Wiotczejesz nagle i wszystko rozp�ywa si� w lepkiej, cichej ciemno�ci. (i po ptakach) 

Eventy w zimie
Wrona przysiad�a na powyginanym konarze jednego z drzew i przypatruje ci si� uwa�nie. 
�nieg podo tob� poskrzypuje podejrzanie. 
(i zaczynamy mie� problemy): 
Czujesz nagle, jakby ziemia usuwa�a ci si� spod n�g (je�li ewakuujesz si� szybko, nic ci si� nie stanie) 
L�d znajduj�cy si� pod �niegiem p�ka z nag�ym trzaskiem, a ty zapadasz si� w lodowat� to�. (kto� rzuci lin� � posta� wywinie si� po prostu ci�kim katarem. Je�li si� czym� rozgrzeje. Je�li nie proponowa�abym ci�sz� chorob�. Zapalenie p�uc?) 
Nie wyczuwasz pod stopami pod�o�a. Bagno, kt�re znajdowa�o si� pod �niegiem nieub�aganie ci� wci�ga. Ponad powierzchni� wystaj� ci tylko g�owa i ramiona. (lina, 60% na z�apanie zgrabia�ymi r�koma) 
Czarna to� zamyka si� na d tob�, a lodowata woda wdziera do p�uc. (pies pogrzebany) 
Wiotczejesz nagle i wszystko rozp�ywa si� w lepkiej, cichej ciemno�ci. (i po ptakach) */
