/* Autor: Avard
   Data : 21.12.06
   Opis : Yran */
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"
inherit LAS_RINDE_STD;
void create_las() 
{
    set_short("W niewielkim zagajniku");
    add_exit(LAS_RINDE_LOKACJE + "las63.c","ne",0,LAS_RO_FATIG,1);
    add_exit(LAS_RINDE_LOKACJE + "las69.c","e",0,LAS_RO_FATIG,1);
    add_exit(TRAKT_RINDE_LOKACJE + "trakt35.c","s",0,LAS_RO_FATIG,1);
    add_exit(TRAKT_RINDE_LOKACJE + "trakt34.c","se",0,LAS_RO_FATIG,1);
    add_npc(LAS_RINDE_LIVINGI+"borsuk");
    add_prop(ROOM_I_INSIDE,0);
}
string
dlugi_opis()
{ 
    string str;
    if(jest_dzien() == 1)
    {
        if(pora_roku() != MT_ZIMA)
        {
            str = "Brzozy o ^snie^znobia^lych konarach pn^a swe "+
				"roz^lo^zyste, li^sciaste ga^l^azki w g^ore tworz^ac wiele "+
				"pozawijanych sklepie^n. Krzaki, zaro^sla i m^lode brzozy "+
				"sprawiaj^a, ^ze w^edr^owka pomimo r^owninnego terenu mo^ze "+
				"by^c bardzo ci^e^zka. Zielone k^epy wysokich traw "+
				"porastaj^a ka^zda mo^zliw^a przestrze^n utrudniaj^ac "+
				"przej^scie mi^edzy nimi. Otaczaj^aca fauna co jaki^s czas "+
				"przypomina^c o swej obecno^sci przy pomocy wszelkiego "+
				"rodzaju d^xwi^ek^ow wydawanych przez okoliczn^a zwierzyne "+
				"i ptactwo.";
        }
        else
        {
            str = "Brzozy o ^snie^znobia^lych konarach pn^a swe "+
				"roz^lo^zyste, nagie ga^l^azki w g^ore tworz^ac wiele "+
				"pozawijanych sklepie^n. Krzaki, zaro^sla i m^lode brzozy "+
				"sprawiaj^a, ^ze w^edr^owka pomimo r^owninnego terenu mo^ze "+
				"by^c bardzo ci^e^zka. Zolte k^epki traw nie^smialo "+
				"wystaj^a z wysokiego ^sniegu kt^ory w po^l^aczeniu z "+
				"licznymi zaro^slami tworzy przeszkod^e niemal nie do "+
				"przej^scia. Otaczaj^aca fauna co jaki^s czas "+
				"przypomina^c o swej obecno^sci przy pomocy wszelkiego "+
				"rodzaju d^xwi^ek^ow wydawanych przez okoliczn^a zwierzyne "+
				"i ptactwo.";
        }
    }
    if(jest_dzien() == 0)
    {
        if(pora_roku() != MT_ZIMA)
        {
            str = "Brzozy o ^snie^znobia^lych konarach pn^a swe "+
				"roz^lo^zyste, li^sciaste ga^l^azki w g^ore tworz^ac wiele "+
				"pozawijanych sklepie^n. Krzaki, zaro^sla i m^lode brzozy "+
				"sprawiaj^a, ^ze w^edr^owka pomimo r^owninnego terenu mo^ze "+
				"by^c bardzo ci^e^zka. Zielone k^epy wysokich traw "+
				"porastaj^a ka^zda mo^zliw^a przestrze^n utrudniaj^ac "+
				"przej^scie mi^edzy nimi. Otaczaj^aca fauna co jaki^s czas "+
				"przypomina^c o swej obecno^sci przy pomocy wszelkiego "+
				"rodzaju d^xwi^ek^ow wydawanych przez okoliczn^a zwierzyne "+
				"i ptactwo.";
        }
        else
        {
            str = "Brzozy o ^snie^znobia^lych konarach pn^a swe "+
				"roz^lo^zyste, nagie ga^l^azki w g^ore tworz^ac wiele "+
				"pozawijanych sklepie^n. Krzaki, zaro^sla i m^lode brzozy "+
				"sprawiaj^a, ^ze w^edr^owka pomimo r^owninnego terenu mo^ze "+
				"by^c bardzo ci^e^zka. Zolte k^epki traw nie^smialo "+
				"wystaj^a z wysokiego ^sniegu kt^ory w po^l^aczeniu z "+
				"licznymi zaro^slami tworzy przeszkod^e niemal nie do "+
				"przej^scia. Otaczaj^aca fauna co jaki^s czas "+
				"przypomina^c o swej obecno^sci przy pomocy wszelkiego "+
				"rodzaju d^xwi^ek^ow wydawanych przez okoliczn^a zwierzyne "+
				"i ptactwo.";
        }
    }
    str +="\n";
    return str;
}
string
opis_nocy()
{
    string str;
    if(pora_roku() != MT_ZIMA)
        {
            str = "Brzozy o ^snie^znobia^lych konarach pn^a swe "+
				"roz^lo^zyste, li^sciaste ga^l^azki w g^ore tworz^ac wiele "+
				"pozawijanych sklepie^n. Krzaki, zaro^sla i m^lode brzozy "+
				"sprawiaj^a, ^ze w^edr^owka pomimo r^owninnego terenu mo^ze "+
				"by^c bardzo ci^e^zka. Zielone k^epy wysokich traw "+
				"porastaj^a ka^zda mo^zliw^a przestrze^n utrudniaj^ac "+
				"przej^scie mi^edzy nimi. Otaczaj^aca fauna co jaki^s czas "+
				"przypomina^c o swej obecno^sci przy pomocy wszelkiego "+
				"rodzaju d^xwi^ek^ow wydawanych przez okoliczn^a zwierzyne "+
				"i ptactwo.";
        }
        else
			{
            str = "Brzozy o ^snie^znobia^lych konarach pn^a swe "+
				"roz^lo^zyste, nagie ga^l^azki w g^ore tworz^ac wiele "+
				"pozawijanych sklepie^n. Krzaki, zaro^sla i m^lode brzozy "+
				"sprawiaj^a, ^ze w^edr^owka pomimo r^owninnego terenu mo^ze "+
				"by^c bardzo ci^e^zka. Zolte k^epki traw nie^smialo "+
				"wystaj^a z wysokiego ^sniegu kt^ory w po^l^aczeniu z "+
				"licznymi zaro^slami tworzy przeszkod^e niemal nie do "+
				"przej^scia. Otaczaj^aca fauna co jaki^s czas "+
				"przypomina^c o swej obecno^sci przy pomocy wszelkiego "+
				"rodzaju d^xwi^ek^ow wydawanych przez okoliczn^a zwierzyne "+
				"i ptactwo.";
        }
    str += "\n";
    return str;
}
