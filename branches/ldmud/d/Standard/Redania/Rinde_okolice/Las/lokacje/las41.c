/* Autor: Avard
   Data : 02.12.06
   Opis : Yran */
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <pogoda.h>
#include "dir.h"

inherit LAS_RINDE_STD;

void create_las() 
{
    set_short("G^esty las");
    add_exit(LAS_RINDE_LOKACJE + "las31.c","n",0,LAS_RO_FATIG,1);
    add_exit(LAS_RINDE_LOKACJE + "las42.c","w",0,LAS_RO_FATIG,1);
    add_exit(LAS_RINDE_LOKACJE + "las51.c","se",0,LAS_RO_FATIG,1);
    add_exit(LAS_RINDE_LOKACJE + "las52.c","s",0,LAS_RO_FATIG,1);
    add_npc(LAS_RINDE_LIVINGI + "zajac");
    add_prop(ROOM_I_INSIDE,0);
}

string
dlugi_opis()
{
    string str;
    if(jest_dzien() == 1)
    {
        if(pora_roku() != MT_ZIMA)
        {
            str = "Nisko zwisaj^ace ga^l^ezie roz^lo^zystych drzew pomimo "+
                "r^owninnego terenu znacznie utrudniaj^a w^edr^owke. Smukle "+
                "brzozy o ^snie^znobia^lych konarach pn^a swe liche "+
                "ga^l^azki w strone nieba, chc^ac z^lapa^c za dnia swoimi "+
                "zielonymi listkami jak najwi^ecej promieni s^lonecznych "+
                "jednak skutecznie utrudniaj^a im to majestatycznie "+
                "rosn^ace ponad nimi wysokie d^eby o grubych pniach. Na "+
                "pod^lo^zu le^z^a r^o^znej wielko^sci po^lamane, suche "+
                "badyle, kt^ore pospada^ly z drzew podczas silniejszych "+
                "podmuch^ow wiatru, kt^ore do^s^c cz^esto nawiedzaj^a "+
                "okolic^e. Do twych uszu dochodzi d^xwi^ek brz^eczenia "+
                "ma^lych mieszka^nc^ow lasu lataj^acych w g^estwinie "+
                "mi^edzy rosn^acymi gdzieniegdzie kwiatami.";
        }
        else
        {
            str = "Nisko zwisaj^ace bezlistne ga^l^ezie roz^lo^zystych "+
                "drzew pomimo r^owninnego terenu znacznie utrudniaj^a "+
                "w^edr^owke. Smukle brzozy o ^snie^znobia^lych konarach "+
                "pn^a swe liche ga^l^azki w strone nieba, chc^ac z^lapa^c "+
                "za dnia jak najwi^ecej promieni s^lonecznych jednak "+
                "skutecznie utrudniaj^a im to wysokie d^eby o grubych "+
                "pniach. Na pod^lo^zu zalega gruba warstwa ^swierzo "+
                "nasypanego ^sniegu, a nieustannie wiej^acy porywisty "+
                "wiatr sprawia, ^ze mi^ekki puch pod wp^lywem jego "+
                "si^ly unosi si^e tworz^ac zamie^c, kt^ora znacznie "+
                "ogranicza widoczno^s^c.";
        }
    }
    if(jest_dzien() == 0)
    {
        if(pora_roku() != MT_ZIMA)
        {
            str = "Nisko zwisaj^ace ga^l^ezie roz^lo^zystych drzew "+
                "pomimo r^owninnego terenu znacznie utrudniaj^a "+
                "w^edr^owke. Smukle brzozy o ^snie^znobia^lych konarach "+
                "pn^a swe liche ga^l^azki w strone nieba, chc^ac "+
                "z^lapa^c za dnia swoimi zielonymi listkami jak "+
                "najwi^ecej promieni s^lonecznych jednak skutecznie "+
                "utrudniaj^a im to majestatycznie rosn^ace ponad nimi "+
                "wysokie d^eby o grubych pniach. Na pod^lo^zu le^z^a "+
                "r^o^znej wielko^sci po^lamane, suche badyle, kt^ore "+
                "pospada^ly z drzew podczas silniejszych podmuch^ow "+
                "wiatru, kt^ore do^s^c cz^esto nawiedzaj^a okolic^e.";
        }
        else
        {
            str = "Nisko zwisaj^ace bezlistne ga^l^ezie roz^lo^zystych "+
                "drzew pomimo r^owninnego terenu znacznie utrudniaj^a "+
                "w^edr^owke. Smukle brzozy o ^snie^znobia^lych konarach "+
                "pn^a swe liche ga^l^azki w strone nieba, chc^ac z^lapa^c "+
                "za dnia jak najwi^ecej promieni s^lonecznych jednak "+
                "skutecznie utrudniaj^a im to wysokie d^eby o grubych "+
                "pniach. Na pod^lo^zu zalega gruba warstwa ^swierzo "+
                "nasypanego ^sniegu, a nieustannie wiej^acy porywisty "+
                "wiatr sprawia, ^ze mi^ekki puch pod wp^lywem jego "+
                "si^ly unosi si^e tworz^ac zamie^c, kt^ora znacznie "+
                "ogranicza widoczno^s^c.";
        }
    }
    str +="\n";
    return str;
}
string
opis_nocy()
{
    string str;
    if(pora_roku() != MT_ZIMA)
        {
            str = "Nisko zwisaj^ace ga^l^ezie roz^lo^zystych drzew "+
                "pomimo r^owninnego terenu znacznie utrudniaj^a "+
                "w^edr^owke. Smukle brzozy o ^snie^znobia^lych konarach "+
                "pn^a swe liche ga^l^azki w strone nieba, chc^ac "+
                "z^lapa^c za dnia swoimi zielonymi listkami jak "+
                "najwi^ecej promieni s^lonecznych jednak skutecznie "+
                "utrudniaj^a im to majestatycznie rosn^ace ponad nimi "+
                "wysokie d^eby o grubych pniach. Na pod^lo^zu le^z^a "+
                "r^o^znej wielko^sci po^lamane, suche badyle, kt^ore "+
                "pospada^ly z drzew podczas silniejszych podmuch^ow "+
                "wiatru, kt^ore do^s^c cz^esto nawiedzaj^a okolic^e.";
        }
        else
        {
            str = "Nisko zwisaj^ace bezlistne ga^l^ezie roz^lo^zystych "+
                "drzew pomimo r^owninnego terenu znacznie utrudniaj^a "+
                "w^edr^owke. Smukle brzozy o ^snie^znobia^lych konarach "+
                "pn^a swe liche ga^l^azki w strone nieba, chc^ac z^lapa^c "+
                "za dnia jak najwi^ecej promieni s^lonecznych jednak "+
                "skutecznie utrudniaj^a im to wysokie d^eby o grubych "+
                "pniach. Na pod^lo^zu zalega gruba warstwa ^swierzo "+
                "nasypanego ^sniegu, a nieustannie wiej^acy porywisty "+
                "wiatr sprawia, ^ze mi^ekki puch pod wp^lywem jego "+
                "si^ly unosi si^e tworz^ac zamie^c, kt^ora znacznie "+
                "ogranicza widoczno^s^c.";
        }
    str +="\n";
    return str;
}
