
/* Autor: Avard
   Data : 09.03.07
   Opis : vortak */
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <pogoda.h>
#include "dir.h"

inherit WLAS_RINDE_STD;

void create_las() 
{
    set_short("Na zach^od od ^l^aki");
    add_exit(LAS_RINDE_LOKACJE + "wlas1.c","nw",0,WLAS_RO_FATIG,1);
    add_exit(LAS_RINDE_LOKACJE + "wlas4.c","w",0,WLAS_RO_FATIG,1);
    add_exit(LAS_RINDE_LOKACJE + "wlas8.c","sw",0,WLAS_RO_FATIG,1);
    add_exit(LAS_RINDE_LOKACJE + "wlas7.c","s",0,WLAS_RO_FATIG,1);
    add_exit(LAKA_RINDE_LOKACJE + "laka38.c","se",0,WLAS_RO_FATIG,1);
    add_exit(LAKA_RINDE_LOKACJE + "laka33.c","e",0,WLAS_RO_FATIG,1);
    add_exit(LAKA_RINDE_LOKACJE + "laka26.c","ne",0,WLAS_RO_FATIG,1);
    add_exit(LAKA_RINDE_LOKACJE + "laka27.c","n",0,WLAS_RO_FATIG,1);
    add_npc(LAS_RINDE_LIVINGI+"wiewiorka");
    add_prop(ROOM_I_INSIDE,0);
}
public string
exits_description() 
{
    return "Wykarczowany las wida^c na p^o^lnocnym-zachodzie, zachodzie, "+
        "po^ludniowym-zachodzie i po^ludniu, za^s ^l^aki znajduj^a si^e na "+
        "po^ludniowym-wschodzie, wschodzie, p^o^lnocnym-wschodzie i "+
        "p^o^lnocy.\n";
}

string
dlugi_opis()
{
    string str;
    if(pora_roku() != MT_ZIMA)
    {
        str = "G^esta, si^egaj^aca niemal do kolan trawa bujnie ro^snie "+
            "na ^zyznej glebie. Tu i ^owdzie widzisz pnie pot^e^znych "+
            "drzew, kt^ore przegra^ly nier^own^a walk^e z ostrzami "+
            "drwalskich topor^ow. Teraz s^a ledwo widoczne - "+
            "zas^loni^ete rosn^acymi ku s^lo^ncu m^lodymi drzewkami. "+
            "Im dalej na po^ludnie tym wi^ecej pniak^ow pami^etaj^acych "+
            "wci^a^z czasy gdy w tym miejscu szumia^l g^esty las. "+
            "Jednak na zachodzie widzisz ciemn^a ^scian^e lasu - tam "+
            "jeszcze drwale poszukuj^acy drewna nie dotarli. Na "+
            "wschodzie, a^z po horyzont rozci^aga si^e faluj^acy na "+
            "wietrze dywan kwiat^ow, ";
        if(pora_roku() == MT_WIOSNA)
        {
            str +="soczy^scie zielonej ";
        }
        if(pora_roku() == MT_LATO)
        {
            str +="zielonej ";
        }
        if(pora_roku() == MT_JESIEN)
        {
            str += "ciemno zielonej ";
        }
        str += "trawy gdzieniegdzie poprzetykanej niewielkimi "+
            "krzakami. Daleko na wschodzie majacz^a niewyra^xnie "+
            "st^ad widoczne mury jakiego^s miasta.";
    }
    else
    {
       str = "Ziemia pokryta jest grub^a ko^ldr^a ^snie^znobia^lego puchu. "+
           "Tu i ^owdzie widzisz wystaj^ace spod warstwy ^sniegu pnie "+
           "pot^e^znych drzew, kt^ore przegra^ly nier^own^a walk^e z "+
           "ostrzami drwalskich topor^ow. Teraz s^a ledwo widoczne - "+
           "zas^loni^ete rosn^acymi ku s^lo^ncu m^lodymi drzewkami. Im "+
           "dalej na po^ludnie tym wi^ecej pniak^ow pami^etaj^acych "+
           "wci^a^z czasy gdy w tym miejscu szumia^l g^esty las. Jednak "+
           "na zachodzie widzisz ciemn^a ^scian^e przysypanego ^snie^zbym "+
           "puchem lasu - tam jeszcze drwale poszukuj^acy drewna nie "+
           "dotarli. Na wschodzie, a^z po horyzont rozci^aga si^e bia^la "+
           "pustynia- ^l^aka, latem zielona, teraz jest we wszystkich "+
           "odcieniach bieli i szaro^sci. Daleko na wschodzie majacz^a "+
           "niewyra^xnie st^ad widoczne mury jakiego^s miasta.";
    }
    str += "\n";
    return str;
}
string
opis_nocy()
{
    string str;
    if(pora_roku() != MT_ZIMA)
    {
        str = "G^esta, si^egaj^aca niemal do kolan trawa bujnie ro^snie "+
            "na ^zyznej glebie. Tu i ^owdzie widzisz pnie pot^e^znych "+
            "drzew, kt^ore przegra^ly nier^own^a walk^e z ostrzami "+
            "drwalskich topor^ow. Teraz s^a ledwo widoczne - "+
            "zas^loni^ete rosn^acymi ku s^lo^ncu m^lodymi drzewkami. "+
            "Im dalej na po^ludnie tym wi^ecej pniak^ow pami^etaj^acych "+
            "wci^a^z czasy gdy w tym miejscu szumia^l g^esty las. "+
            "Jednak na zachodzie widzisz ciemn^a ^scian^e lasu - tam "+
            "jeszcze drwale poszukuj^acy drewna nie dotarli. Na "+
            "wschodzie, a^z po horyzont rozci^aga si^e faluj^acy na "+
            "wietrze dywan kwiat^ow, ";
        if(pora_roku() == MT_WIOSNA)
        {
            str +="soczy^scie zielonej ";
        }
        if(pora_roku() == MT_LATO)
        {
            str +="zielonej ";
        }
        if(pora_roku() == MT_JESIEN)
        {
            str += "ciemno zielonej ";
        }
        str += "trawy gdzieniegdzie poprzetykanej niewielkimi "+
            "krzakami. Daleko na wschodzie majacz^a niewyra^xnie "+
            "st^ad widoczne mury jakiego^s miasta.";
    }
    else
    {
       str = "Ziemia pokryta jest grub^a ko^ldr^a ^snie^znobia^lego puchu. "+
           "Tu i ^owdzie widzisz wystaj^ace spod warstwy ^sniegu pnie "+
           "pot^e^znych drzew, kt^ore przegra^ly nier^own^a walk^e z "+
           "ostrzami drwalskich topor^ow. Teraz s^a ledwo widoczne - "+
           "zas^loni^ete rosn^acymi ku s^lo^ncu m^lodymi drzewkami. Im "+
           "dalej na po^ludnie tym wi^ecej pniak^ow pami^etaj^acych "+
           "wci^a^z czasy gdy w tym miejscu szumia^l g^esty las. Jednak "+
           "na zachodzie widzisz ciemn^a ^scian^e przysypanego ^snie^zbym "+
           "puchem lasu - tam jeszcze drwale poszukuj^acy drewna nie "+
           "dotarli. Na wschodzie, a^z po horyzont rozci^aga si^e bia^la "+
           "pustynia- ^l^aka, latem zielona, teraz jest we wszystkich "+
           "odcieniach bieli i szaro^sci. Daleko na wschodzie majacz^a "+
           "niewyra^xnie st^ad widoczne mury jakiego^s miasta.";
    }
    str += "\n";
    return str;
}
