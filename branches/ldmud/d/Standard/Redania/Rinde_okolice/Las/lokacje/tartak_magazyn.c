inherit "/std/room";

#include <stdproperties.h>
#include <macros.h>
#define BRONIER (RINDE + "przedmioty/bronie/")
#define BRONIEO "/d/Standard/items/bronie/"
#define ZBROJEO "/d/Standard/items/zbroje/"
#define LAMPA "/d/Standard/items/narzedzia/lampa_niewielka_poreczna"

void create_room() 
{
    set_short("Tajny magazyn");
    set_long("Jeste� w tajnym magazynie. Nie ma st�d �adnego wyj�cia!\n");
    add_prop(ROOM_I_INSIDE,1);
}