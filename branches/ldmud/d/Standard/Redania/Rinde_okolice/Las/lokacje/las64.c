/* Autor: Avard
   Data : 01.12.06
   Opis : Yran */
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"
inherit LAS_RINDE_STD;
void create_las() 
{
    set_short("Las");
    add_exit(LAS_RINDE_LOKACJE + "las65.c","w",0,LAS_RO_FATIG,1);
    add_exit(LAS_RINDE_LOKACJE + "las69.c","se",0,LAS_RO_FATIG,1);
    add_npc(LAS_RINDE_LIVINGI + "lis");
    add_prop(ROOM_I_INSIDE,0);
}
string
dlugi_opis()
{
    string str;
    if(jest_dzien() == 1)
    {
        if(pora_roku() != MT_ZIMA)
        {
            str = "Roz^lo^zyste d^eby pn^a swe korony majestatycznie "+
                "ku g^orze, lekko uginaj^ac si^e pod naporem nawet "+
                "najmniejszego wiatru. ^Snie^znobia^le konary brz^oz"+
                "pr^obuj^a wystawi^c zielone listki do s^lo^nca, jednak "+
                "skutecznie utrudniaj^a im to inne, wynio^sle rosn^ace, "+
                "wysokie drzewa zabieraj^ac swoimi li^sciastymi ga^l^eziami "+
                "wi^ekszo^s^c ^swiat^la. Otaczaj^aca fauna co jaki^s czas "+
                "przypomina^c o swej obecno^sci przy pomocy wszelkiego "+
                "rodzaju d^xwi^ek^ow wydawanych przez okoliczn^a zwierzyne "+
                "i ptactwo.";
        }
        else
        {
           str = "Roz^lo^zyste d^eby pn^a swe korony majestatycznie "+
                "ku g^orze, lekko uginaj^ac si^e pod naporem nawet "+
                "najmniejszego, lodowatego wiatru. ^Snie^znobia^le konary "+
                "brz^oz pozbawione jakiegokolwiek ulistnienia zapad^ly w sen"+
                "zimowy. Niemal ca^le niebo zakrywaj^a nagie ga^l^ezie "+
                "wysokich drzew nie pozwalaj^ac dotrzec ^swiatlu do "+
                "dolnych cz^e^sci lasu. Otaczaj^aca fauna co jaki^s czas "+
                "przypomina^c o swej obecno^sci przy pomocy wszelkiego "+
                "rodzaju d^xwi^ek^ow wydawanych przez okoliczn^a zwierzyne "+
                "i ptactwo.";
        }
    }
    if(jest_dzien() == 0)
    {
        if(pora_roku() != MT_ZIMA)
        {
            str = "Roz^lo^zyste d^eby pn^a swe korony majestatycznie "+
                "ku g^orze, lekko uginaj^ac si^e pod naporem nawet "+
                "najmniejszego wiatru. ^Snie^znobia^le konary brz^oz"+
                "pr^obuj^a wystawi^c zielone listki do s^lo^nca, jednak "+
                "skutecznie utrudniaj^a im to inne, wynio^sle rosn^ace, "+
                "wysokie drzewa zabieraj^ac swoimi li^sciastymi ga^l^eziami "+
                "wi^ekszo^s^c ^swiat^la. Otaczaj^aca fauna co jaki^s czas "+
                "przypomina^c o swej obecno^sci przy pomocy wszelkiego "+
                "rodzaju d^xwi^ek^ow wydawanych przez okoliczn^a zwierzyne "+
                "i ptactwo.";
        }
        else
        {
            str = "Roz^lo^zyste d^eby pn^a swe korony majestatycznie "+
                "ku g^orze, lekko uginaj^ac si^e pod naporem nawet "+
                "najmniejszego, lodowatego wiatru. ^Snie^znobia^le konary "+
                "brz^oz pozbawione jakiekogolwiek ulistnienia zapad^ly w sen"+
                "zimowy. Niemal ca^le niebo zakrywaj^a nagie ga^l^ezie "+
                "wysokich drzew nie pozwalaj^ac dotrze^c ^swiatlu do "+
                "dolnych cz^e^sci lasu. Otaczaj^aca fauna co jaki^s czas "+
                "przypomina^c o swej obecno^sci przy pomocy wszelkiego "+
                "rodzaju d^xwi^ek^ow wydawanych przez okoliczn^a zwierzyne "+
                "i ptactwo.";
        }
    }
    str +="\n";
    return str;
}
string
opis_nocy()
{
    string str;
    if(pora_roku() != MT_ZIMA)
        {
            str = "Roz^lo^zyste d^eby pn^a swe korony majestatycznie "+
                "ku g^orze, lekko uginaj^ac si^e pod naporem nawet "+
                "najmniejszego wiatru. ^Snie^znobia^le konary brz^oz"+
                "pr^obuj^a wystawi^c zielone listki do s^lo^nca, jednak "+
                "skutecznie utrudniaj^a im to inne, wynio^sle rosn^ace, "+
                "wysokie drzewa zabieraj^ac swoimi li^sciastymi ga^l^eziami "+
                "wi^ekszo^s^c ^swiat^la. Otaczaj^aca fauna co jaki^s czas "+
                "przypomina^c o swej obecno^sci przy pomocy wszelkiego "+
                "rodzaju d^xwi^ek^ow wydawanych przez okoliczn^a zwierzyne "+
                "i ptactwo.";
        }
        else
        {
            str = "Roz^lo^zyste d^eby pn^a swe korony majestatycznie "+
                "ku g^orze, lekko uginaj^ac si^e pod naporem nawet "+
                "najmniejszego, lodowatego wiatru. ^Snie^znobia^le konary "+
                "brz^oz pozbawione jakiekogolwiek ulistnienia zapadly w sen"+
                "zimowy. Niemal ca^le niebo zakrywaj^a nagie ga^l^ezie "+
                "wysokich drzew nie pozwalaj^ac dotrze^c ^swiatlu do "+
                "dolnych cz^e^sci lasu. Otaczaj^aca fauna co jaki^s czas "+
                "przypomina^c o swej obecno^sci przy pomocy wszelkiego "+
                "rodzaju d^xwi^ek^ow wydawanych przez okoliczn^a zwierzyne "+
                "i ptactwo.";
        }
    str += "\n";
    return str;
}
