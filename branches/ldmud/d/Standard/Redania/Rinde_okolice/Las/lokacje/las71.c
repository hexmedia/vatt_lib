/* Autor: Avard
   Data : 22.06.06
   Opis : Tinardan */
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <pogoda.h>
#include "dir.h"

inherit LAS_RINDE_STD;
inherit "/lib/drink_water";

void create_las() 
{
    set_short("Las nad brzegiem rzeki");
    add_exit(LAS_RINDE_LOKACJE + "las72.c","w",0,LAS_RO_FATIG,1);
    add_exit(TRAKT_RINDE_LOKACJE + "trakt22.c","nw",0,TRAKT_RO_FATIG,0);
    add_exit(TRAKT_RINDE_LOKACJE + "trakt21.c","n",0,TRAKT_RO_FATIG,0);
    add_exit(TRAKT_RINDE_LOKACJE + "trakt20.c","ne",0,TRAKT_RO_FATIG,0);

    add_prop(ROOM_I_INSIDE,0);
    add_event("@@pora_rok:"+file_name(TO)+"@@");
    set_drink_places(({"z rzeki","z pontaru","z Pontaru"}));
    add_sit(({"nad rzek^a","nad pontarem","nad Pontarem"}),
        "nad rzek^a","z nad rzeki",0);
    add_item(({"Pontar","pontar","rzek^e"}),"P^lyn^acy tu od tysi^ecy lat "+
    "szeroki Pontar zdaje si^e by^c wci^a^z nieujarzmionym i niezdobytym, "+
    "wyznacza nie tylko granice mi^edzy kr^olestwami Redanii i Temeri, "+
    "ale i skutecznie ogranicza ludzkie zap^edy w wyrywaniu z r^ak "+
    "matki Natury wszystkiego, co sie jej nale^zy. Gdzieniegdzie "+
    "pojawiaj^ace si^e z nienacka niewielkie zawirowania wody na "+
    "spokojnie, jakby leniwie poruszaj^acej si^e m^etnej tafli swiadcz^a "+
    "o nieprzywidywalno^sci i zdradzieckiej naturze nurtu, kt^ory zapewne "+
    "pochlon^a^l niejedno istnienie bezmy^slnie probuj^ace zmierzy^c si^e z "+
    "pot^eg^a rzeki.\n");
}
void
init()
{
    ::init();
    init_drink_water(); 
} 


string
dlugi_opis()
{
    string str;
    if(jest_dzien() == 1)
    {
        if(pora_roku() != MT_ZIMA)
        {
            str = "Pontar, niczym kr^ol tej krainy, rozlewa si^e szeroko, "+
            "ka^z^ac p^lyn^a^c swym wodom leniwie i dostojnie. Drzewa "+
            "przycupn^e^ly tu^z przy piaszczystym brzegu, nachylaj^ac "+
            "^lapczywie d^lugie ga^l^ezie ku aksamitnej powierzchni wody. "+
            "W korzeniach jednego z nich wida^c kr^olicz^a nork^e, w pie^n "+
            "innego zawzi^ecie stuka jaki^s nadgorliwy dzi^ecio^l. ";
            if(this_player()->query_headache() > 1)
            {
                str += "Przykro stwierdzi^c, ale do tego jednostajnego "+
                "stukotu zaczyna bole^c ci^e g^lowa. ";
            }
            str += "Gruba warstwa li^sci pokrywa le^sne pod^lo^ze, ale do^s^c "+
            "p^lytkie w tym miejscu dno rzeki jest zupe^lnie czyste  "+
            "najwyra^xniej pr^ad Pontaru nie jest tak ^lagodny, jakby si^e "+
            "na pierwszy rzut oka mog^lo zdawa^c. Panuje tu atmosfera "+
            "przedziwnego spokoju, a wok^o^l rozchodzi si^e ^swie^zy, "+
            "le^sny zapach. Gdzie^s na p^o^lnocy majaczy trakt.";
        }
        else
        {
            str = "Pontar, niczym kr^ol tej krainy, rozlewa si^e szeroko, "+
            "ale jego wody skute s^a lodem i uwi^ezione pod jego g^ladk^a "+
            "powierzchni^a. Jednak gro^xne, granatowe fale gdzieniegdzie "+
            "rozbi^ly l^od i teraz przelewaj^a si^e przeze� szumi^ac "+
            "gro^xnie i w^sciekle. Drzewa przycupn^e^ly tu^z przy pokrytym "+
            "grub^a warstw^a ^sniegu brzegu, nachylaj^ac smutno swe "+
            "bezlistne ga^l^ezie ku nieskazitelnie bia^lej powierzchni "+
            "lodu. Gdzie^s wysoko, po^sr^od ga^l^ezi kracze ponuro wrona, "+
            "wy^spiewuj^ac sw^a odwieczn^a, skrzekliw^a pie^s� Na "+
            "powierzchni ^sniegu wyra^xnie odbijaj^a si^e tropy "+
            "przer^o^znych zwierz^at le^snych, kt^ore najwyra^xniej "+
            "czuj^a si^e w tym zak^atku zupe^lnie bezpieczne.\n";
	  }
    }
    if(jest_dzien() == 0)
    {
        if(pora_roku() != MT_ZIMA)
        {
            str = "Pontar, niczym kr^ol tej krainy, rozlewa si^e szeroko, "+
            "ka^z^ac p^lyn^a^c swym wodom leniwie i dostojnie. Drzewa "+
            "przycupn^e^ly tu^z przy piaszczystym brzegu, nachylaj^ac "+
            "^lapczywie d^lugie ga^l^ezie ku aksamitnej powierzchni wody. "+
            "W korzeniach jednego z nich wida^c kr^olicz^a nork^e, w pie^n "+
            "innego zawzi^ecie stuka jaki^s nadgorliwy dzi^ecio^l. ";
            if(this_player()->query_headache() > 1)
            {
                str += "Przykro stwierdzi^c, ale do tego jednostajnego "+
                "stukotu zaczyna bole^c ci^e g^lowa. ";
            }
            str += "Gruba warstwa li^sci pokrywa le^sne pod^lo^ze, ale do^s^c "+
            "p^lytkie w tym miejscu dno rzeki jest zupe^lnie czyste  "+
            "najwyra^xniej pr^ad Pontaru nie jest tak ^lagodny, jakby si^e "+
            "na pierwszy rzut oka mog^lo zdawa^c. Panuje tu atmosfera "+
            "przedziwnego spokoju, a wok^o^l rozchodzi si^e ^swie^zy, "+
            "le^sny zapach. Gdzie^s na p^o^lnocy majaczy trakt.";
        }
        else
        {
            str = "Pontar, niczym kr^ol tej krainy, rozlewa si^e szeroko, "+
            "ale jego wody skute s^a lodem i uwi^ezione pod jego g^ladk^a "+
            "powierzchni^a. Jednak gro^xne, granatowe fale gdzieniegdzie "+
            "rozbi^ly l^od i teraz przelewaj^a si^e przeze� szumi^ac "+
            "gro^xnie i w^sciekle. Drzewa przycupn^e^ly tu^z przy pokrytym "+
            "grub^a warstw^a ^sniegu brzegu, nachylaj^ac smutno swe "+
            "bezlistne ga^l^ezie ku nieskazitelnie bia^lej powierzchni "+
            "lodu. Gdzie^s wysoko, po^sr^od ga^l^ezi kracze ponuro wrona, "+
            "wy^spiewuj^ac sw^a odwieczn^a, skrzekliw^a pie^s� Na "+
            "powierzchni ^sniegu wyra^xnie odbijaj^a si^e tropy "+
            "przer^o^znych zwierz^at le^snych, kt^ore najwyra^xniej czuj^a "+
            "si^e w tym zak^atku zupe^lnie bezpieczne.\n";
        }
    }
    str +="\n";
    return str;
}
string
opis_nocy()
{
    string str;
    if(pora_roku() != MT_ZIMA)
    {
        str = "Pontar, niczym kr^ol tej krainy, rozlewa si^e szeroko, "+
            "ka^z^ac p^lyn^a^c swym wodom leniwie i dostojnie. Drzewa "+
            "przycupn^e^ly tu^z przy piaszczystym brzegu, nachylaj^ac "+
            "^lapczywie d^lugie ga^l^ezie ku aksamitnej powierzchni wody. "+
            "W korzeniach jednego z nich wida^c kr^olicz^a nork^e, w pie^n "+
            "innego zawzi^ecie stuka jaki^s nadgorliwy dzi^ecio^l. ";
        if(this_player()->query_headache() > 1)
        {
            str += "Przykro stwierdzi^c, ale do tego jednostajnego stukotu "+
                "zaczyna bole^c ci^e g^lowa. ";
        }
        str += "Gruba warstwa li^sci pokrywa le^sne pod^lo^ze, ale do^s^c "+
            "p^lytkie w tym miejscu dno rzeki jest zupe^lnie czyste  "+
            "najwyra^xniej pr^ad Pontaru nie jest tak ^lagodny, jakby si^e "+
            "na pierwszy rzut oka mog^lo zdawa^c. Panuje tu atmosfera "+
            "przedziwnego spokoju, a wok^o^l rozchodzi si^e ^swie^zy, "+
            "le^sny zapach. Gdzie^s na p^o^lnocy majaczy trakt.";
    }
    else
    {
        str = "Pontar, niczym kr^ol tej krainy, rozlewa si^e szeroko, ale "+
            "jego wody skute s^a lodem i uwi^ezione pod jego g^ladk^a "+
            "powierzchni^a. Jednak gro^xne, granatowe fale gdzieniegdzie "+
            "rozbi^ly l^od i teraz przelewaj^a si^e przeze� szumi^ac "+
            "gro^xnie i w^sciekle. Drzewa przycupn^e^ly tu^z przy pokrytym "+
            "grub^a warstw^a ^sniegu brzegu, nachylaj^ac smutno swe "+
            "bezlistne ga^l^ezie ku nieskazitelnie bia^lej powierzchni "+
            "lodu. Gdzie^s wysoko, po^sr^od ga^l^ezi kracze ponuro wrona, "+
            "wy^spiewuj^ac sw^a odwieczn^a, skrzekliw^a pie^s� Na "+
            "powierzchni ^sniegu wyra^xnie odbijaj^a si^e tropy "+
            "przer^o^znych zwierz^at le^snych, kt^ore najwyra^xniej czuj^a "+
            "si^e w tym zak^atku zupe^lnie bezpieczne.\n";
    }
    str += "\n";
    return str;
}
string
pora_rok()
{
    switch (pora_roku())
    {
         case MT_LATO:
         case MT_WIOSNA:
             return ({"Wa^zka przelecia^la tu^z ko^lo twojego nosa.\n",
             "W wodzie trzepocze przez chwil^e jaka^s ryba, po czym niknie "+
             "w g^l^ebinach.\n",
             "Fale Pontaru szumi^a cicho.\n",
             "Powietrze pachnie lasem i wod^a.\n"})[random(4)];
         case MT_JESIEN:
             return ({"Bezlistne ga^l^ezie muskaj^a tw^oj policzek.\n",
             "Drzewa poskrzypuj^a cicho.\n"})[random(2)];
         case MT_ZIMA:
             return ({"Fale Pontaru hucz^a gro^xnie.\n",
             "Bezlistne ga^l^ezie muskaj^a tw^oj policzek.\n",
             "Kra z trzaskiem od^lamuje si^e od brzegu i odp^lywa niesiona "+
             "silnym nurtem.\n"})[random(3)];
    }
}

