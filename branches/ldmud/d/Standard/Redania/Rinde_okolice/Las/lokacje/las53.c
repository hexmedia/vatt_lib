/* Autor: Avard
   Data : 30.11.06
   Opis : Yran */
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <pogoda.h>
#include "dir.h"
inherit LAS_RINDE_STD;
void create_las() 
{
    set_short("Las");
    add_exit(LAS_RINDE_LOKACJE + "las43.c","n",0,LAS_RO_FATIG,1);
    add_exit(LAS_RINDE_LOKACJE + "las61.c","se",0,LAS_RO_FATIG,1);
    add_exit(LAS_RINDE_LOKACJE + "las62.c","s",0,LAS_RO_FATIG,1);
    add_npc(LAS_RINDE_LIVINGI + "lis");
    
    add_prop(ROOM_I_INSIDE,0);
}
string
dlugi_opis()
{
    string str;
    if(jest_dzien() == 1)
    {
        if(pora_roku() != MT_ZIMA)
        {
            str = "Roz^lo^zyste konary drzew tworz^a wysoko nad twoj^a "+
                "g^low^a liczne sklepienia. G^esto poskr^ecane i pozawijane "+
                "ga^l^azki pe^lne ";
            if(pora_roku() == MT_JESIEN)
            {
                str +="kolorowych ";
            }
            else
            {
                str +="zielonych ";
            }
            str +="li^sci przepuszczaj^a niewiele promieni s^lonecznych, "+
                "kt^ore rzucaj^a stosunkowo ma^lo ^swiat^la na najbli^zsz^a "+
                "okolic^e. Lekkie podmuchy wiatru ko^lysz^a rosn^acymi "+
                "woko^l drzewami, kt^ore w dzikim ta^ncu bujaj^a si^e na "+
                "wietrze. Pod^lo^ze pokrywaj^a ma^le ga^l^azki i nieliczne "+
                "k^epy zielonych traw, z kt^orych co jaki^s czas "+
                "dochodz^ac^a odg^losy ma^lych zwierz^atek ukrywaj^acych "+
                "si^e przed wyg^lodnia^lymi drapnie^znikami czekaj^acymi "+
                "tylko na okazje, by w dzikim szale rzuci^c si^e na "+
                "bezbronn^a istot^e.";
        }
        else
        {
            str = "Roz^lo^zyste konary drzew tworz^a wysoko nad twoj^a "+
                "g^low^a liczne sklepienia. G^esto poskr^ecane i pozawijane "+
                "bezlistne ga^l^azki przepuszczaj^a niewiele promieni "+
                "s^lonecznych, kt^ore rzucaj^a stosunkowo ma^lo ^swiat^la "+
                "na najbli^zsz^a okolic^e. Lekkie podmuchy wiatru "+
                "ko^lysz^a rosn^acymi woko^l drzewami, kt^ore w dzikim "+
                "ta^ncu bujaj^a si^e na wietrze. Pod^lo^ze pokrywa "+
                "warstwa bia^lego puchu przez kt^ory zdo^la^ly przebi^c "+
                "si^e nieliczne k^epy lekko zielonakwych traw, z kt^orych "+
                "co jaki^s czas dochodz^ac^a odg^losy ma^lych zwierz^atek "+
                "ukrywaj^acych si^e przed wyg^lodnia^lymi drapnie^znikami "+
                "czekaj^acymi tylko na okazje, by w dzikim szale rzuci^c "+
                "si^e na bezbronn^a istot^e.";
       }
    }
    if(jest_dzien() == 0)
    {
        if(pora_roku() != MT_ZIMA)
        {
            str = "Roz^lo^zyste konary drzew tworz^a wysoko nad twoj^a "+
                "g^low^a liczne sklepienia. G^esto poskr^ecane i pozawijane "+
                "ga^l^azki pe^lne ";
            if(pora_roku() == MT_JESIEN)
            {
                str +="kolorowych ";
            }
            else
            {
                str +="zielonych ";
            }
            str +="li^sci przepuszczaj^a zaledwie nik^ly blask ksi^ezyca, "+
                "kt^ory rzucaja stosunkowo ma^lo ^swiat^la na najbli^zsz^a "+
                "okolic^e. Lekkie podmuchy wiatru ko^lysz^a rosn^acymi "+
                "woko^l drzewami, kt^ore w dzikim ta^ncu bujaj^a si^e na "+
                "wietrze. Pod^lo^ze pokrywaj^a ma^le ga^l^azki i nieliczne "+
                "k^epy zielonych traw, z kt^orych co jaki^s czas "+
                "dochodz^ac^a odg^losy ma^lych zwierz^atek ukrywaj^acych "+
                "si^e przed wyg^lodnia^lymi drapnie^znikami czekaj^acymi "+
                "tylko na okazje, by w dzikim szale rzuci^c si^e na "+
                "bezbronn^a istot^e.";
        }
        else
        {
            str = "Roz^lo^zyste konary drzew tworz^a wysoko nad twoj^a "+
                "g^low^a liczne sklepienia. G^esto poskr^ecane i pozawijane "+
                "bezlistne ga^l^azki przepuszczaj^a zaledwie nik^ly blask "+
                "ksi^ezyca, kt^ory rzucaja stosunkowo ma^lo ^swiat^la na "+
                "najbli^zsz^a okolic^e. Lekkie podmuchy wiatru ko^lysz^a "+
                "rosn^acymi woko^l drzewami, kt^ore w dzikim ta^ncu bujaj^a "+
                "si^e na wietrze. Pod^lo^ze pokrywa warstwa bia^lego puchu "+
                "przez kt^ory zdo^la^ly przebi^c si^e nieliczne k^epy lekko "+
                "zielonakwych traw, z kt^orych co jaki^s czas dochodz^ac^a "+
                "odg^losy ma^lych zwierz^atek ukrywaj^acych si^e przed "+
                "wyg^lodnia^lymi drapnie^znikami czekaj^acymi tylko na "+
                "okazje, by w dzikim szale rzuci^c si^e na bezbronn^a "+
                "istot^e.";
       }
    }
    str += "\n";
    return str;
}
string
opis_nocy()
{
    string str;
    if(pora_roku() != MT_ZIMA)
        {
            str = "Roz^lo^zyste konary drzew tworz^a wysoko nad twoj^a "+
                "g^low^a liczne sklepienia. G^esto poskr^ecane i pozawijane "+
                "ga^l^azki pe^lne ";
            if(pora_roku() == MT_JESIEN)
            {
                str +="kolorowych ";
            }
            else
            {
                str +="zielonych ";
            }
            str +="li^sci przepuszczaj^a zaledwie nik^ly blask ksi^ezyca, "+
                "kt^ory rzucaja stosunkowo ma^lo ^swiat^la na najbli^zsz^a "+
                "okolic^e. Lekkie podmuchy wiatru ko^lysz^a rosn^acymi "+
                "woko^l drzewami, kt^ore w dzikim ta^ncu bujaj^a si^e na "+
                "wietrze. Pod^lo^ze pokrywaj^a ma^le ga^l^azki i nieliczne "+
                "k^epy zielonych traw, z kt^orych co jaki^s czas "+
                "dochodz^ac^a odg^losy ma^lych zwierz^atek ukrywaj^acych "+
                "si^e przed wyg^lodnia^lymi drapnie^znikami czekaj^acymi "+
                "tylko na okazje, by w dzikim szale rzuci^c si^e na "+
                "bezbronn^a istot^e.";
        }
        else
        {
            str = "Roz^lo^zyste konary drzew tworz^a wysoko nad twoj^a "+
                "g^low^a liczne sklepienia. G^esto poskr^ecane i pozawijane "+
                "bezlistne ga^l^azki przepuszczaj^a zaledwie nik^ly blask "+
                "ksi^ezyca, kt^ory rzucaja stosunkowo ma^lo ^swiat^la na "+
                "najbli^zsz^a okolic^e. Lekkie podmuchy wiatru ko^lysz^a "+
                "rosn^acymi woko^l drzewami, kt^ore w dzikim ta^ncu bujaj^a "+
                "si^e na wietrze. Pod^lo^ze pokrywa warstwa bia^lego puchu "+
                "przez kt^ory zdo^la^ly przebi^c si^e nieliczne k^epy lekko "+
                "zielonakwych traw, z kt^orych co jaki^s czas dochodz^ac^a "+
                "odg^losy ma^lych zwierz^atek ukrywaj^acych si^e przed "+
                "wyg^lodnia^lymi drapnie^znikami czekaj^acymi tylko na "+
                "okazje, by w dzikim szale rzuci^c si^e na bezbronn^a "+
                "istot^e.";
       }
    str += "\n";
    return str;
}