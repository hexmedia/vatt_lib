/* Autor: Avard
   Data : 26.11.06
   Opis : Tinardan */

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <pogoda.h>
#include "dir.h"

inherit LAS_RINDE_STD;

void create_las() 
{
    set_short("W^aw^oz w lesie");
    add_exit(LAS_RINDE_LOKACJE + "las27.c","w",0,LAS_RO_FATIG,1);
    add_exit(LAS_RINDE_LOKACJE + "las16.c","ne",0,LAS_RO_FATIG,1);

    add_prop(ROOM_I_INSIDE,0);
}

string
dlugi_opis()
{
    string str;
    if(jest_dzien() == 1)
    {
        if(pora_roku() != MT_ZIMA)
        {
            str = "Najprawdopodobniej kiedy^s t^edy p^lyn^e^la jaka^s "+
                "rzeka, ale teraz pozosta^la po niej charakterystyczna "+
                "rze^xba terenu. Na dnie zalega spora warstewka rozmok^lej "+
                "gliny, lepi^acej si^e do ";
            if(this_player()->query_armour(TS_FEET))
            {
                str +="but^ow, ";
            }
            else
            {
                str +="st^op, ";
            }
            str +="rdzawej i g^estej. Panuje tu ponury p^o^lmrok - "+
                "spl^atane ga^l^ezie drzew, g^l^ownie buk^ow i klon^ow, "+
                "tworz^a ponad w^awozem swego rodzaju dach, przez kt^ory "+
                "jedynie z rzadka przedostaje si^e rachityczny promie^n "+
                "s^lonecznego ^swiat^la. Dna nie porastaj^a ^zadne "+
                "ro^sliny, a glina miesza si^e z butwiej^acymi li^s^cmi i "+
                "u^lomkami ga^l^ezi. Na obu ^scianach biegnie wyra^xny "+
                "wz^or jasnego br^azu i czerni ukazuj^acy przez jakie "+
                "warstwy ziemi woda musia^la niegdy^s przebrn^a^c. Na "+
                "powierzchni, niczym naro^sle, przyczepione rosn^a niskie "+
                "krzewy szak^laku. ";
            /*Na jego ga^l^eziach b^lyszcz^a ciemnogranatowe, drobniutkie "+
            "owoce (maj, czerwiec; owoce s^a truj^ace). */
        }
        else
        {
            str = "Najprawdopodobniej kiedy^s t^edy p^lyn^e^la jaka^s "+
                "rzeka, ale teraz pozosta^la po niej charakterystyczna "+
                "rze^xba terenu. Na dnie, wymieszana ze ^sniegiem, zalega "+
                "spora warstewka rozmok^lej gliny, lepi^acej si^e do ";
            if(this_player()->query_armour(TS_FEET))
            {
                str +="but^ow, ";
            }
            else
            {
                str +="st^op, ";
            }
            str +="rdzawej i g^estej. Panuje tu ponury p^o^lmrok Spl^atane "+
                "ga^l^ezie drzew, g^l^ownie buk^ow i klon^ow, tworz^a ponad "+
                "w^awozem swego rodzaju dach, przez kt^ory jedynie z rzadka "+
                "przedostaje si^e rachityczny promie^n s^lonecznego "+
                "^swiat^la i nawet drobniutkie p^latki ^sniegu maj^a "+
                "trudno^sci, by dotrze^c na sam d^o^l. Na obu ^scianach "+
                "biegnie wyra^xny wz^or jasnego br^azu i czerni ukazuj^acy "+
                "przez jakie warstwy ziemi woda musia^la niegdy^s "+
                "przebrn^a^c.";
        }
    }
    if(jest_dzien() == 0)
    {
        if(pora_roku() != MT_ZIMA)
        {
            str = "Najprawdopodobniej kiedy^s t^edy p^lyn^e^la jaka^s "+
                "rzeka, ale teraz pozosta^la po niej charakterystyczna "+
                "rze^xba terenu. Na dnie zalega spora warstewka rozmok^lej "+
                "gliny, lepi^acej si^e do ";
            if(this_player()->query_armour(TS_FEET))
            {
                str +="but^ow, ";
            }
            else
            {
                str +="st^op, ";
            }
            str +="rdzawej i g^estej. Panuje tu ponury p^o^lmrok - "+
                "spl^atane ga^l^ezie drzew, g^l^ownie buk^ow i klon^ow, "+
                "tworz^a ponad w^awozem swego rodzaju dach, przez kt^ory "+
                "jedynie z rzadka przedostaje si^e rachityczny promie^n "+
                "ksi^e^zycowego ^swiat^la. Dna nie porastaj^a ^zadne "+
                "ro^sliny, a glina miesza si^e z butwiej^acymi li^s^cmi i "+
                "u^lomkami ga^l^ezi. Na obu ^scianach biegnie wyra^xny "+
                "wz^or jasnego br^azu i czerni ukazuj^acy przez jakie "+
                "warstwy ziemi woda musia^la niegdy^s przebrn^a^c. Na "+
                "powierzchni, niczym naro^sle, przyczepione rosn^a niskie "+
                "krzewy szak^laku. ";
            /*Na jego ga^l^eziach b^lyszcz^a ciemnogranatowe, drobniutkie "+
            "owoce (maj, czerwiec; owoce s^a truj^ace). */
        }
        else
        {
            str = "Najprawdopodobniej kiedy^s t^edy p^lyn^e^la jaka^s "+
                "rzeka, ale teraz pozosta^la po niej charakterystyczna "+
                "rze^xba terenu. Na dnie, wymieszana ze ^sniegiem, zalega "+
                "spora warstewka rozmok^lej gliny, lepi^acej si^e do ";
            if(this_player()->query_armour(TS_FEET))
            {
                str +="but^ow, ";
            }
            else
            {
                str +="st^op, ";
            }
            str +="rdzawej i g^estej. Panuje tu ponury p^o^lmrok Spl^atane "+
                "ga^l^ezie drzew, g^l^ownie buk^ow i klon^ow, tworz^a ponad "+
                "w^awozem swego rodzaju dach, przez kt^ory jedynie z rzadka "+
                "przedostaje si^e rachityczny promie^n ksi^e^zycowego "+
                "^swiat^la i nawet drobniutkie p^latki ^sniegu maj^a "+
                "trudno^sci, by dotrze^c na sam d^o^l. Na obu ^scianach "+
                "biegnie wyra^xny wz^or jasnego br^azu i czerni ukazuj^acy "+
                "przez jakie warstwy ziemi woda musia^la niegdy^s "+
                "przebrn^a^c.";
        }
    }
    str +="\n";
    return str;
}
string
opis_nocy()
{
    string str;
    str = "Nie jeste^s w stanie tu nic dojrze^c, panuje tutaj kompletna "+
        "ciemno^s^c.\n";
    return str;
}

