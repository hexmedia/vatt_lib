/* Autor: Avard
   Data : 30.06.06
   Opis : Tinardan */
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <pogoda.h>
#include "dir.h"

inherit LAS_RINDE_STD;

void create_las() 
{
    set_short("Las w pobli^zu traktu");
    add_exit(LAS_RINDE_LOKACJE + "las48.c","nw",0,LAS_RO_FATIG,1);
    add_exit(LAS_RINDE_LOKACJE + "las47.c","n",0,LAS_RO_FATIG,1);
    add_exit(TRAKT_RINDE_LOKACJE + "trakt23.c","w",0,TRAKT_RO_FATIG,0);
    add_exit(TRAKT_RINDE_LOKACJE + "trakt22.c","sw",0,TRAKT_RO_FATIG,0);
    add_exit(TRAKT_RINDE_LOKACJE + "trakt21.c","s",0,TRAKT_RO_FATIG,0);
    add_exit(TRAKT_RINDE_LOKACJE + "trakt20.c","se",0,TRAKT_RO_FATIG,0);
    add_npc(LAS_RINDE_LIVINGI+"borsuk");
    add_prop(ROOM_I_INSIDE,0);

}

string
dlugi_opis()
{
    string str;
    if(jest_dzien() == 1)
    {
    if(pora_roku() != MT_ZIMA)
    {
        str = "Drzewa, g^l^ownie buki, wybuja^ly wysoko, ale ich pnie s^a "+
            "cienkie i zn^ekane  wida^c, ^ze podr^o^znicy korzystaj^acy z "+
            "niedalekiego traktu nie stroni^a od schronienia na brzegach "+
            "lasu";
        if(jest_rzecz_w_sublokacji(0, "dziurawy buklak") || 
            jest_rzecz_w_sublokacji (0, "para d^lugich czarnych but^ow") || 
            jest_rzecz_w_sublokacji(0, "para wysokich sznurowanych but^ow") || 
            jest_rzecz_w_sublokacji(0, "stare szarawe onuce") ||
            jest_rzecz_w_sublokacji(0, "zgni^la kapusta"))
        {
            str += " i nie bardzo przy tym przejmuj^ac si^e przyrod^a. "+
                "Walaj^a si^e tu r^o^znego rodzaju ^smiecie";
        }
        if(jest_rzecz_w_sublokacji(0, "dziurawy buklak"))
        {
            str += ", dziurawe buk^laki";
        }
        if(jest_rzecz_w_sublokacji(0, "para d^lugich czarnych but^ow") ||
            jest_rzecz_w_sublokacji(0, "para wysokich sznurowanych but^ow")) 
        {
            str += ", zu^zyte buty";
        }
        if(jest_rzecz_w_sublokacji(0, "zgni^la kapusta"))
        {
            str +=", zgni^le kapusty";
        }
        if(jest_rzecz_w_sublokacji(0, "stare szarawe onuce"))
        {
            str += " i przetarte onuce nie pierwszej ^swie^zo^sci, a "+
                "niekt^orzy nie zawahaliby si^e u^zy^c stwierdzenia, "+
                "^ze tak^ze i nie dziesi^atej";
        }
        str += ". Trawa jest kr^ociutka i mocno wydeptana, a po^sr^od jej "+
        "nie^smia^lych k^los^ow mo^zna zobaczy^c pozosta^lo^sci po "+
        "niedawnych ogniskach i obozowiskach. ^Swiergot ptak^ow jest s^laby "+
        "i odleg^ly, zwierz^eta schroni^ly si^e raczej w miejscu mniej "+
        "dost^epnym dla niepo^z^adanych osobnik^ow. Dalej na p^o^lnoc las "+
        "robi si^e g^estszy i bardziej dziki, a li^scie, rosn^ace co raz "+
        "g^e^sciej i przepuszczaj^ace co raz mniej promieni s^lonecznych.";
    }
    else
    {
        str = "Drzewa, g^l^ownie buki, wybuja^ly wysoko, ale ich pnie s^a "+
            "cienkie i zn^ekane  wida^c, ^ze podr^o^znicy korzystaj^acy z "+
            "niedalekiego traktu nie stroni^a od schronienia na brzegach "+
            "lasu i nie bardzo przejmuj^a si^e przyrod^a";
        if(jest_rzecz_w_sublokacji(0, "dziurawy buklak") || 
            jest_rzecz_w_sublokacji (0, "para d^lugich czarnych but^ow") || 
            jest_rzecz_w_sublokacji(0, "para wysokich sznurowanych but^ow") || 
            jest_rzecz_w_sublokacji(0, "stare szarawe onuce") ||
            jest_rzecz_w_sublokacji(0, "zgni^la kapusta"))
        {
            str += ". Walaj^a si^e tu r^o^znego rodzaju ^smiecie";
        }
        if(jest_rzecz_w_sublokacji(0, "dziurawy buklak"))
        {
            str += ", dziurawe buk^laki";
        }
        if(jest_rzecz_w_sublokacji(0, "para d^lugich czarnych but^ow") ||
            jest_rzecz_w_sublokacji(0, "para wysokich sznurowanych but^ow"))
        {
            str += ", zu^zyte buty";
        }
        if(jest_rzecz_w_sublokacji(0, "zgni^la kapusta"))
        {
            str +=", zgni^le kapusty";
        }
        if(jest_rzecz_w_sublokacji(0, "stare szarawe onuce"))
        {
            str += " i przetarte onuce nie pierwszej ^swie^zo^sci, a "+
                "niekt^orzy nie zawahaliby si^e u^zy^c stwierdzenia, ^ze "+
                "tak^ze i nie dziesi^atej. ";
        }
        str += "^sniegu w^la^sciwie nie ma, jedynie na wp^o^l zamarzni^ete, "+
            "brudne b^locko przypr^oszone kilkoma ^swie^zszymi p^latkami. "+
            "Nie trzeba si^e zbytnio przygl^ada^c, aby znale^x^c "+
            "pozosta^lo^sci po niedawnych ogniskach i obozowiskach. Dalej "+
            "na p^o^lnoc las robi si^e g^estszy i bardziej dziki, a "+
            "ga^l^ezie spl^atane ze sob^a i swobodne.";
       }
       }
//Tu sie noc zaczyna
       if(jest_dzien() == 0)
       {
       if(pora_roku() != MT_ZIMA)
    {
        str = "Drzewa, g^l^ownie buki, wybuja^ly wysoko, ale ich pnie s^a "+
            "cienkie i zn^ekane  wida^c, ^ze podr^o^znicy korzystaj^acy z "+
            "niedalekiego traktu nie stroni^a od schronienia na brzegach "+
            "lasu i nie bardzo przy tym przejmuj^ac si^e przyrod^a";
        if(jest_rzecz_w_sublokacji(0, "dziurawy buklak") ||
            jest_rzecz_w_sublokacji (0, "para d^lugich czarnych but^ow") || 
            jest_rzecz_w_sublokacji(0, "para wysokich sznurowanych but^ow") || 
            jest_rzecz_w_sublokacji(0, "stare szarawe onuce") ||
            jest_rzecz_w_sublokacji(0, "zgni^la kapusta"))
        {
            str += ". Walaj^a si^e tu r^o^znego rodzaju ^smiecie";
        }
        if(jest_rzecz_w_sublokacji(0, "dziurawy buklak"))
        {
            str += ", dziurawe buk^laki";
        }
        if(jest_rzecz_w_sublokacji(0, "para d^lugich czarnych but^ow") ||
           jest_rzecz_w_sublokacji(0, "para wysokich sznurowanych but^ow")) 
        {
            str += ", zu^zyte buty";
        }
        if(jest_rzecz_w_sublokacji(0, "zgni^la kapusta"))
        {
            str +=", zgni^le kapusty";
        }
        if(jest_rzecz_w_sublokacji(0, "stare szarawe onuce"))
        {
            str += " i przetarte onuce nie pierwszej ^swie^zo^sci, a "+
                "niekt^orzy nie zawahaliby si^e u^zy^c stwierdzenia, "+
                "^ze tak^ze i nie dziesi^atej";
        }
        str += ". Trawa jest kr^ociutka i mocno wydeptana, a po^sr^od jej "+
        "nie^smia^lych k^los^ow mo^zna zobaczy^c pozosta^lo^sci po "+
        "niedawnych ogniskach i obozowiskach. ^Swiergot ptak^ow jest s^laby "+
        "i odleg^ly, zwierz^eta schroni^ly si^e raczej w miejscu mniej "+
        "dost^epnym dla niepo^z^adanych osobnik^ow. Dalej na p^o^lnoc las "+
        "robi si^e g^estszy i bardziej dziki, a li^scie, rosn^ace co raz "+
        "g^e^sciej i przepuszczaj^ace co raz mniej promieni s^lonecznych.";
    }
    else
    {
        str = "Drzewa, g^l^ownie buki, wybuja^ly wysoko, ale ich pnie s^a "+
            "cienkie i zn^ekane  wida^c, ^ze podr^o^znicy korzystaj^acy z "+
            "niedalekiego traktu nie stroni^a od schronienia na brzegach "+
            "lasu i nie bardzo przejmuj^a si^e przyrod^a";
        if(jest_rzecz_w_sublokacji(0, "dziurawy buklak") || 
            jest_rzecz_w_sublokacji (0, "para d^lugich czarnych but^ow") || 
            jest_rzecz_w_sublokacji(0, "para wysokich sznurowanych but^ow") ||
            jest_rzecz_w_sublokacji(0, "stare szarawe onuce") ||
            jest_rzecz_w_sublokacji(0, "zgni^la kapusta"))
        {
            str += ". Walaj^a si^e tu r^o^znego rodzaju ^smiecie";
        }
        if(jest_rzecz_w_sublokacji(0, "dziurawy buklak"))
        {
            str += ", dziurawe buk^laki";
        }
        if(jest_rzecz_w_sublokacji(0, "para d^lugich czarnych but^ow") ||
            jest_rzecz_w_sublokacji(0, "para wysokich sznurowanych but^ow"))
        {
            str += ", zu^zyte buty";
        }
        if(jest_rzecz_w_sublokacji(0, "zgni^la kapusta"))
        {
            str +=", zgni^le kapusty";
        }
        if(jest_rzecz_w_sublokacji(0, "stare szarawe onuce"))
        {
            str += " i przetarte onuce nie pierwszej ^swie^zo^sci, a "+
                "niekt^orzy nie zawahaliby si^e u^zy^c stwierdzenia, "+
                "^ze tak^ze i nie dziesi^atej. ";
        }
        str += "^sniegu w^la^sciwie nie ma, jedynie na wp^o^l zamarzni^ete, "+
            "brudne b^locko przypr^oszone kilkoma ^swie^zszymi p^latkami. "+
            "Nie trzeba si^e zbytnio przygl^ada^c, aby znale^x^c "+
            "pozosta^lo^sci po niedawnych ogniskach i obozowiskach. Dalej "+
            "na p^o^lnoc las robi si^e g^estszy i bardziej dziki, a "+
            "ga^l^ezie spl^atane ze sob^a i swobodne.";
       }
       }
       str += "\n";
       return str;
}
string
opis_nocy()
{
    string str;
    if(pora_roku() != MT_ZIMA)
    {
        str = "Drzewa, g^l^ownie buki, wybuja^ly wysoko, ale ich pnie s^a "+
            "cienkie i zn^ekane  wida^c, ^ze podr^o^znicy korzystaj^acy z "+
            "niedalekiego traktu nie stroni^a od schronienia na brzegach "+
            "lasu i nie bardzo przy tym przejmuj^ac si^e przyrod^a";
        if(jest_rzecz_w_sublokacji(0, "dziurawy buklak") ||
            jest_rzecz_w_sublokacji (0, "para d^lugich czarnych but^ow") || 
            jest_rzecz_w_sublokacji(0, "para wysokich sznurowanych but^ow") || 
            jest_rzecz_w_sublokacji(0, "stare szarawe onuce") ||
            jest_rzecz_w_sublokacji(0, "zgni^la kapusta"))
        {
            str += ". Walaj^a si^e tu r^o^znego rodzaju ^smiecie";
        }
        if(jest_rzecz_w_sublokacji(0, "dziurawy buklak"))
        {
            str += ", dziurawe buk^laki";
        }
        if(jest_rzecz_w_sublokacji(0, "para d^lugich czarnych but^ow") ||
           jest_rzecz_w_sublokacji(0, "para wysokich sznurowanych but^ow")) 
        {
            str += ", zu^zyte buty";
        }
        if(jest_rzecz_w_sublokacji(0, "zgni^la kapusta"))
        {
            str +=", zgni^le kapusty";
        }
        if(jest_rzecz_w_sublokacji(0, "stare szarawe onuce"))
        {
            str += " i przetarte onuce nie pierwszej ^swie^zo^sci, a "+
                "niekt^orzy nie zawahaliby si^e u^zy^c stwierdzenia, "+
                "^ze tak^ze i nie dziesi^atej";
        }
        str += ". Trawa jest kr^ociutka i mocno wydeptana, a po^sr^od jej "+
        "nie^smia^lych k^los^ow mo^zna zobaczy^c pozosta^lo^sci po "+
        "niedawnych ogniskach i obozowiskach. ^Swiergot ptak^ow jest s^laby "+
        "i odleg^ly, zwierz^eta schroni^ly si^e raczej w miejscu mniej "+
        "dost^epnym dla niepo^z^adanych osobnik^ow. Dalej na p^o^lnoc las "+
        "robi si^e g^estszy i bardziej dziki, a li^scie, rosn^ace co raz "+
        "g^e^sciej i przepuszczaj^ace co raz mniej promieni s^lonecznych.";
    }
    else
    {
        str = "Drzewa, g^l^ownie buki, wybuja^ly wysoko, ale ich pnie s^a "+
            "cienkie i zn^ekane  wida^c, ^ze podr^o^znicy korzystaj^acy z "+
            "niedalekiego traktu nie stroni^a od schronienia na brzegach "+
            "lasu i nie bardzo przejmuj^a si^e przyrod^a";
        if(jest_rzecz_w_sublokacji(0, "dziurawy buklak") || 
            jest_rzecz_w_sublokacji (0, "para d^lugich czarnych but^ow") || 
            jest_rzecz_w_sublokacji(0, "para wysokich sznurowanych but^ow") ||
            jest_rzecz_w_sublokacji(0, "stare szarawe onuce") ||
            jest_rzecz_w_sublokacji(0, "zgni^la kapusta"))
        {
            str += ". Walaj^a si^e tu r^o^znego rodzaju ^smiecie";
        }
        if(jest_rzecz_w_sublokacji(0, "dziurawy buklak"))
        {
            str += ", dziurawe buk^laki";
        }
        if(jest_rzecz_w_sublokacji(0, "para d^lugich czarnych but^ow") ||
            jest_rzecz_w_sublokacji(0, "para wysokich sznurowanych but^ow"))
        {
            str += ", zu^zyte buty";
        }
        if(jest_rzecz_w_sublokacji(0, "zgni^la kapusta"))
        {
            str +=", zgni^le kapusty";
        }
        if(jest_rzecz_w_sublokacji(0, "stare szarawe onuce"))
        {
            str += " i przetarte onuce nie pierwszej ^swie^zo^sci, a "+
                "niekt^orzy nie zawahaliby si^e u^zy^c stwierdzenia, "+
                "^ze tak^ze i nie dziesi^atej. ";
        }
        str += "^sniegu w^la^sciwie nie ma, jedynie na wp^o^l zamarzni^ete, "+
            "brudne b^locko przypr^oszone kilkoma ^swie^zszymi p^latkami. "+
            "Nie trzeba si^e zbytnio przygl^ada^c, aby znale^x^c "+
            "pozosta^lo^sci po niedawnych ogniskach i obozowiskach. Dalej "+
            "na p^o^lnoc las robi si^e g^estszy i bardziej dziki, a "+
            "ga^l^ezie spl^atane ze sob^a i swobodne.";
       }
    str += "\n";
    return str;
}
