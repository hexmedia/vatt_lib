/* Autor: Avard
   Data : 30.06.06
   Opis : Tinardan */
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <pogoda.h>
#include "dir.h"

inherit LAS_RINDE_STD;

void create_las() 
{
    set_short("Bukowy zagajnik");
    add_exit(LAS_RINDE_LOKACJE + "las35.c","n",0,LAS_RO_FATIG,1);
    add_exit(LAS_RINDE_LOKACJE + "las36.c","nw",0,LAS_RO_FATIG,1);
    add_exit(LAS_RINDE_LOKACJE + "las57.c","s",0,LAS_RO_FATIG,1);
    add_exit(TRAKT_RINDE_LOKACJE + "trakt23.c","sw",0,TRAKT_RO_FATIG,0);
    add_npc(LAS_RINDE_LIVINGI+"wiewiorka");

    add_prop(ROOM_I_INSIDE,0);
    add_item(({"grupka zawilc^ow","zawilce","kwiaty","bia^le zawilce",
        "kobierzec","kwiatki"}), "@@kwiaty@@");
}

string
dlugi_opis()
{
    string str;
    if(jest_dzien() == 1)
    {
    if(pora_roku() != MT_ZIMA)
    {
        str = "Tu^z nad ziemi^a unosi si^e delikatna mgie^lka, a "+
            "w^la^sciwie kropelki rosy, kt^ore, w cieniu rzucanym przez "+
            "wysokie, bujnie rosn^ace buki niemal nigdy nie znikaj^a. ";
        if(pora_roku() == MT_WIOSNA)
        {
            str += "Ca^le pod^lo^ze pokryte jest bia^lymi, drobniutkimi "+
                "zawilcami, tak ^ze wygl^ada jak zielony dywan, na kt^orym "+
                "kto^s wymalowa^l ca^le mn^ostwo bia^lych groszk^ow.";
        }
        str += "Same buki s^a wysokie i zdrowe, ich korony "+
            "zdaj^a si^e si^ega^c nieba, a li^scie rosn^a tak g^esto, ^ze "+
            "promienie s^loneczne przedzieraj^a si^e przez nie z niema^lym "+
            "trudem. Gdzie^s z g^ory s^lycha^c ^swiergot sikorki  "+
            "zapami^eta^ly i melodyjny, zupe^lnie jakby ptaszyna si^e z "+
            "kim^s przekomarza^la. Woko^lo roznosi si^e charakterystyczny, "+
            "le^sny zapach.";
    }
    else
    {
        str = "Ziemia pokryta jest grub^a pokryw^a ^sniegu, po kt^orym hasa "+
            "weso^lo kilka wr^obli, ^cwierkaj^ac zupe^lnie jakby na "+
            "przek^or zimie i mro^xnej pogodzie. Buki s^a wysokie i zdrowe, "+
            "ich korony zdaj^a si^e si^ega^c nieba, a ga^l^ezie tak ze "+
            "sob^a spl^atane, ^ze niemal tworz^a dach, cho^c nie na tyle "+
            "g^esty, by dawa^l jak^akolwiek ochron^e przed ^sniegiem. "+
            "Gdzie^s na g^orze s^lycha^c krakanie wron, kt^orych wida^c "+
            "ca^le stada kr^a^z^ace po niebosk^lonie. Niebo jest szare i "+
            "pokryte grub^a warstw^a chmur, kt^ore z tej odleg^lo^sci "+
            "wygl^adaj^a niczym siwe k^l^eby we^lny.";
    }
    }
//Tu sie noc zaczyna
    if(jest_dzien() == 0)
    {
    if(pora_roku() != MT_ZIMA)
    {
        str = "Tu^z nad ziemi^a unosi si^e delikatna mgie^lka, a "+
            "w^la^sciwie kropelki rosy, kt^ore, w cieniu rzucanym przez "+
            "wysokie, bujnie rosn^ace buki niemal nigdy nie znikaj^a. ";
        if(pora_roku() == MT_WIOSNA)
        {
            str += "Ca^le pod^lo^ze pokryte jest bia^lymi, drobniutkimi "+
                "zawilcami, tak ^ze wygl^ada jak zielony dywan, na kt^orym "+
                "kto^s wymalowa^l ca^le mn^ostwo bia^lych groszk^ow.";
        }
            str += "Same buki s^a wysokie i zdrowe, ich korony "+
                "zdaj^a si^e si^ega^c nieba, a li^scie rosn^a tak g^esto, "+
                "^ze promienie ksi^e^zyca i ^swiat^lo gwiazd przedzieraj^a "+
                "si^e przez nie z niema^lym trudem. Woko^lo roznosi si^e "+
                "charakterystyczny, le^sny zapach.";
    }
    else
    {
        str = "Ziemia pokryta jest grub^a pokryw^a ^sniegu, na kt^orym "+
            "wida^c ^slady jakich^s drobnych ptaszk^ow. Buki s^a wysokie i "+
            "zdrowe, ich korony zdaj^a si^e si^ega^c nieba, a ga^l^ezie tak "+
            "ze sob^a spl^atane, ^ze niemal tworz^a dach, cho^c nie na tyle "+
            "g^esty, by dawa^l jak^akolwiek ochron^e przed ^sniegiem. "+
            "Gdzie^s na g^orze s^lycha^c krakanie wron, kt^orych wida^c "+
            "ca^le stada kr^a^z^ace po niebosk^lonie. Niebo jest szare i "+
            "pokryte grub^a warstw^a chmur, kt^ore z tej odleg^lo^sci "+
            "wygl^adaj^a niczym siwe k^l^eby we^lny.";
    }
    }
    str += "\n";
    return str;
}
string
opis_nocy()
{
    string str;
    if(pora_roku() != MT_ZIMA)
    {
        str = "Tu^z nad ziemi^a unosi si^e delikatna mgie^lka, a "+
            "w^la^sciwie kropelki rosy, kt^ore, w cieniu rzucanym przez "+
            "wysokie, bujnie rosn^ace buki niemal nigdy nie znikaj^a. ";
        if(pora_roku() == MT_WIOSNA)
        {
            str += "Ca^le pod^lo^ze pokryte jest bia^lymi, drobniutkimi "+
                "zawilcami, tak ^ze wygl^ada jak zielony dywan, na kt^orym "+
                "kto^s wymalowa^l ca^le mn^ostwo bia^lych groszk^ow.";
        }
            str += "Same buki s^a wysokie i zdrowe, ich korony "+
                "zdaj^a si^e si^ega^c nieba, a li^scie rosn^a tak g^esto, "+
                "^ze promienie ksi^e^zyca i ^swiat^lo gwiazd przedzieraj^a "+
                "si^e przez nie z niema^lym trudem. Woko^lo roznosi si^e "+
                "charakterystyczny, le^sny zapach.";
    }
    else
    {
        str = "Ziemia pokryta jest grub^a pokryw^a ^sniegu, na kt^orym "+
            "wida^c ^slady jakich^s drobnych ptaszk^ow. Buki s^a wysokie i "+
            "zdrowe, ich korony zdaj^a si^e si^ega^c nieba, a ga^l^ezie tak "+
            "ze sob^a spl^atane, ^ze niemal tworz^a dach, cho^c nie na tyle "+
            "g^esty, by dawa^l jak^akolwiek ochron^e przed ^sniegiem. "+
            "Gdzie^s na g^orze s^lycha^c krakanie wron, kt^orych wida^c "+
            "ca^le stada kr^a^z^ace po niebosk^lonie. Niebo jest szare i "+
            "pokryte grub^a warstw^a chmur, kt^ore z tej odleg^lo^sci "+
            "wygl^adaj^a niczym siwe k^l^eby we^lny.";
    }
    str += "\n";
    return str;
}
string
kwiaty()
{
    switch (pora_roku())
    {
    case MT_LATO:
    case MT_JESIEN:
    case MT_ZIMA:
        return ("Nie zauwa^zasz niczego takiego.\n");
    case MT_WIOSNA:
        return ("Wygl^adaj^a niczym p^latki ^sniegu, zagubione w^sr^od "+
        "ciemnej zieleni. Male^nkie kwiatuszki, o delikatnych p^latkach i "+
        "^z^o^ltych ^srodkach unosz^ace swe g^l^owki wysoko do s^lo^nca. "+
        "Ich li^scie przypominaj^a troch^e kszta^ltem li^s^c klonu, ale "+
        "s^a bardziej poszarpane i nieregularne. Zawilc^ow jest tutaj "+
        "ca^le mn^ostwo, tak, ^ze tworz^a niemal^ze swoisty kobierzec, "+
        "pokrywaj^acy szczelnie ka^zd^a pi^ed^x ziemi, okalaj^acy grube "+
        "pnie drzew i ukrywaj^acy pu^lapki czyhaj^ace na nieostro^znych "+
        "w^edrowc^ow.\n");
    
    }
}