/* Autor: Avard
   Data : 29.11.06
   Opis : Yran */
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <pogoda.h>
#include "dir.h"
inherit LAS_RINDE_STD;
void create_las() 
{
    set_short("W lesie");
    add_exit(LAS_RINDE_LOKACJE + "las41.c","n",0,LAS_RO_FATIG,1);
    add_exit(LAS_RINDE_LOKACJE + "las61.c","sw",0,LAS_RO_FATIG,1);
    add_npc(LAS_RINDE_LIVINGI+"borsuk");
    
    add_prop(ROOM_I_INSIDE,0);
    add_sit(({"na drzewie","na powalonym drzewie",
        "na przewr^oconym drzewie"}),"na powalonym drzewie",
        "z powalonego drzewa",3);
}
string
dlugi_opis()
{
    string str;
    if(jest_dzien() == 1)
    {
        if(pora_roku() != MT_ZIMA)
        {
            str = "Spl^atane ga^l^ezie drzew i du^ze li^scie sprawiaj^a, "+
                "^ze jest tu niemal ca^lkowicie ciemno. Na szcz^e^scie "+
                "pierwsze ga^l^ezie rosn^a na takiej wysoko^sci, ^ze w "+
                "^zadnym stopniu nie utrudniaj^a w^edr^owki. Pojedy^ncze "+
                "k^epy traw rosn^acych gdzieniegdzie s^a znakomitym "+
                "schronieniem dla ma^lych zwierz^atek przed drapie^znikami "+
                "czaj^acymi sie woko^l w poszukiwaniu po^zywienia. "+
                "Pod^lo^ze pokrywaj^a ma^le ga^l^azki, kt^ore pod naporem "+
                "ci^e^zaru przechodz^acych tedy zwierz^at w wielu "+
                "miejscach s^a po^lamane.";
        }
        else
        {
            str = "Spl^atane ga^l^ezie drzew sprawiaj^a, ^ze przebija si^e "+
                "tu niewiele promieni s^lonecznych. Na szcz^e^scie pierwsze "+
                "ga^l^ezie rosn^a na takiej wysoko^sci, ^ze w ^zadnym "+
                "stopniu nie utrudniaj^a w^edr^owki. Pojedy^ncze k^epy traw "+
                "przebijaj^a si^e poprzez warstw^a ^sniegu. Pod^lo^ze "+
                "pokrywa bia^ly puch, na kt^orym wyra^xnie zaznaczone s^a "+
                "^slady pozostawione przez le^sne zwierz^eta.";
       }
    }
    if(jest_dzien() == 0)
    {
        if(pora_roku() != MT_ZIMA)
        {
            str = "Spl^atane ga^l^ezie drzew i du^ze li^scie sprawiaj^a, "+
                "^ze jest tu niemal ca^lkowicie ciemno. Na szcz^e^scie "+
                "pierwsze ga^l^ezie rosn^a na takiej wysoko^sci, ^ze w "+
                "^zadnym stopniu nie utrudniaj^a w^edr^owki. Pojedy^ncze "+
                "k^epy traw rosn^acych gdzieniegdzie s^a znakomitym "+
                "schronieniem dla ma^lych zwierz^atek przed drapie^znikami "+
                "czaj^acymi sie woko^l w poszukiwaniu po^zywienia. "+
                "Pod^lo^ze pokrywaj^a ma^le ga^l^azki, kt^ore pod naporem "+
                "ci^e^zaru przechodz^acych tedy zwierz^at w wielu "+
                "miejscach s^a po^lamane.";
        }
        else
        {
            str = "Spl^atane ga^l^ezie drzew sprawiaj^a, ^ze przebija si^e "+
                "tu jedynie nik^ly blask ksi^ezyca. Na szcz^e^scie pierwsze "+
                "ga^l^ezie rosn^a na takiej wysoko^sci, ^ze w ^zadnym "+
                "stopniu nie utrudniaj^a w^edr^owki. Pojedy^ncze k^epy traw "+
                "przebijaj^a si^e poprzez warstw^a ^sniegu. Pod^lo^ze "+
                "pokrywa bia^ly puch, na kt^orym wyra^xnie zaznaczone s^a "+
                "^slady pozostawione przez le^sne zwierz^eta.";
       }
    }
    str += "\n";
    return str;
}
string
opis_nocy()
{
    string str;
    if(pora_roku() != MT_ZIMA)
        {
            str = "Spl^atane ga^l^ezie drzew i du^ze li^scie sprawiaj^a, "+
                "^ze jest tu niemal ca^lkowicie ciemno. Na szcz^e^scie "+
                "pierwsze ga^l^ezie rosn^a na takiej wysoko^sci, ^ze w "+
                "^zadnym stopniu nie utrudniaj^a w^edr^owki. Pojedy^ncze "+
                "k^epy traw rosn^acych gdzieniegdzie s^a znakomitym "+
                "schronieniem dla ma^lych zwierz^atek przed drapie^znikami "+
                "czaj^acymi sie woko^l w poszukiwaniu po^zywienia. "+
                "Pod^lo^ze pokrywaj^a ma^le ga^l^azki, kt^ore pod naporem "+
                "ci^e^zaru przechodz^acych tedy zwierz^at w wielu "+
                "miejscach s^a po^lamane.";
        }
        else
        {
            str = "Spl^atane ga^l^ezie drzew sprawiaj^a, ^ze przebija si^e "+
                "tu jedynie nik^ly blask ksi^ezyca. Na szcz^e^scie pierwsze "+
                "ga^l^ezie rosn^a na takiej wysoko^sci, ^ze w ^zadnym "+
                "stopniu nie utrudniaj^a w^edr^owki. Pojedy^ncze k^epy traw "+
                "przebijaj^a si^e poprzez warstw^a ^sniegu. Pod^lo^ze "+
                "pokrywa bia^ly puch, na kt^orym wyra^xnie zaznaczone s^a "+
                "^slady pozostawione przez le^sne zwierz^eta.";
       }
    str += "\n";
    return str;
}