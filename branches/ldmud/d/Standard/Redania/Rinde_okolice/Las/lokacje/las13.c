/* Autor: Avard
   Data : 01.12.06
   Opis : Tinardan */

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <pogoda.h>
#include "dir.h"

inherit LAS_RINDE_STD;

void create_las() 
{
    set_short("G^l^eboki las");
    add_exit(LAS_RINDE_LOKACJE + "las6.c","ne",0,LAS_RO_FATIG,1);
    add_exit(LAS_RINDE_LOKACJE + "las7.c","nw",0,LAS_RO_FATIG,1);
    add_exit(LAS_RINDE_LOKACJE + "las20.c","sw",0,LAS_RO_FATIG,1);
    add_prop(ROOM_I_INSIDE,0);
}

string
dlugi_opis()
{
    string str;
    if(jest_dzien() == 1)
    {
        if(pora_roku() != MT_ZIMA)
        {
            str = "G^esto rosn^ace buki, brzozy, klony i graby mieszaj^a "+
                "si^e ze sob^a i spl^atuj^a koronami. Wygl^adaj^a do^s^c "+
                "malowniczo, cho^c ca^la gromada po^lamanych konar^ow i "+
                "pr^ochniej^acych pni zalegaj^acych wok^o^l drzew nieco "+
                "psuje ten efekt. Gdzieniegdzie, nie^smia^lo wychylaj^a "+
                "si^e p^eki przer^o^znych paproci. Gdzie^s w ich g^aszczu "+
                "przemykaj^a cicho niewielkie zwierz^atka � s^lycha^c "+
                "tylko jednostajny szelest, gdy tr^acaj^a uschni^ete "+
                "li^scie. Najwyra^xniej "+
                "zwierz^eta czuj^a si^e tu zupe^lnie bezpieczne, z dala od "+
                "dr^og i ludzkich siedzib. W lesie panuje szczeg^olne "+
                "wra^zenie spokoju, ptaki po^spiewuj^a weso^lo, na grubym "+
                "pok^ladzie butwiej^acych li^sci s^a widoczne liczne tropy "+
                "wi^ekszych zwierz^at. Wok^o^l rozchodzi si^e szczeg^olny "+
                "zapach pr^ochniej^acego drewna, wilgoci i lasu.";
        }
        else
        {
            str = "G^esto rosn^ace buki, brzozy, klony i graby mieszaj^a "+
                "si^e ze sob^a i spl^atuj^a koronami. Wygl^adaj^a do^s^c "+
                "malowniczo, cho^c ca^la gromada po^lamanych konar^ow i "+
                "pr^ochniej^acych pni zalegaj^acych wok^o^l drzew nieco "+
                "psuje ten efekt. Tworz^a one spore kupki, nie do ko^nca "+
                "okryte ^sniegiem. Spod bia^lego puchu, nie^smia^lo, "+
                "wychylaj^a si^e gdzieniegdzie li^scie paproci, poszarza^le "+
                "i zeschni^ete, stercz^ace niczym swoiste flagi. Bia^la "+
                "powierzchnia pokryta jest tropami drobnych zwierz^at, "+
                "gdzieniegdzie ca^lkiem rozkopana, tak, ^ze wida^c pod "+
                "ni^a ciemn^a ziemi^e. Najwyra^xniej czuj^a si^e tu "+
                "zupe^lnie bezpieczne, z dala od dr^og i ludzkich siedzib.";
        }
    }
    if(jest_dzien() == 0)
    {
        if(pora_roku() != MT_ZIMA)
        {
            str = "G^esto rosn^ace buki, brzozy, klony i graby mieszaj^a "+
                "si^e ze sob^a i spl^atuj^a koronami. Wygl^adaj^a do^s^c "+
                "malowniczo, cho^c ca^la gromada po^lamanych konar^ow i "+
                "pr^ochniej^acych pni zalegaj^acych wok^o^l drzew nieco "+
                "psuje ten efekt. Gdzieniegdzie, nie^smia^lo wychylaj^a "+
                "si^e p^eki przer^o^znych paproci. Gdzie^s w ich g^aszczu "+
                "przemykaj^a cicho niewielkie zwierz^atka � s^lycha^c "+
                "tylko jednostajny szelest, gdy tr^acaj^a uschni^ete "+
                "li^scie. Najwyra^xniej "+
                "zwierz^eta czuj^a si^e tu zupe^lnie bezpieczne, z dala od "+
                "dr^og i ludzkich siedzib. W lesie panuje szczeg^olne "+
                "wra^zenie spokoju, ptaki po^spiewuj^a weso^lo, na grubym "+
                "pok^ladzie butwiej^acych li^sci s^a widoczne liczne tropy "+
                "wi^ekszych zwierz^at. Wok^o^l rozchodzi si^e szczeg^olny "+
                "zapach pr^ochniej^acego drewna, wilgoci i lasu.";
        }
        else
        {
            str = "G^esto rosn^ace buki, brzozy, klony i graby mieszaj^a "+
                "si^e ze sob^a i spl^atuj^a koronami. Wygl^adaj^a do^s^c "+
                "malowniczo, cho^c ca^la gromada po^lamanych konar^ow i "+
                "pr^ochniej^acych pni zalegaj^acych wok^o^l drzew nieco "+
                "psuje ten efekt. Tworz^a one spore kupki, nie do ko^nca "+
                "okryte ^sniegiem. Spod bia^lego puchu, nie^smia^lo, "+
                "wychylaj^a si^e gdzieniegdzie li^scie paproci, poszarza^le "+
                "i zeschni^ete, stercz^ace niczym swoiste flagi. Bia^la "+
                "powierzchnia pokryta jest tropami drobnych zwierz^at, "+
                "gdzieniegdzie ca^lkiem rozkopana, tak, ^ze wida^c pod "+
                "ni^a ciemn^a ziemi^e. Najwyra^xniej czuj^a si^e tu "+
                "zupe^lnie bezpieczne, z dala od dr^og i ludzkich siedzib.";
        }
    }
    str +="\n";
    return str;
}
string
opis_nocy()
{
    string str;
    if(pora_roku() != MT_ZIMA)
        {
            str = "G^esto rosn^ace buki, brzozy, klony i graby mieszaj^a "+
                "si^e ze sob^a i spl^atuj^a koronami. Wygl^adaj^a do^s^c "+
                "malowniczo, cho^c ca^la gromada po^lamanych konar^ow i "+
                "pr^ochniej^acych pni zalegaj^acych wok^o^l drzew nieco "+
                "psuje ten efekt. Gdzieniegdzie, nie^smia^lo wychylaj^a "+
                "si^e p^eki przer^o^znych paproci. Gdzie^s w ich g^aszczu "+
                "przemykaj^a cicho niewielkie zwierz^atka � s^lycha^c "+
                "tylko jednostajny szelest, gdy tr^acaj^a uschni^ete "+
                "li^scie. Najwyra^xniej "+
                "zwierz^eta czuj^a si^e tu zupe^lnie bezpieczne, z dala od "+
                "dr^og i ludzkich siedzib. W lesie panuje szczeg^olne "+
                "wra^zenie spokoju, ptaki po^spiewuj^a weso^lo, na grubym "+
                "pok^ladzie butwiej^acych li^sci s^a widoczne liczne tropy "+
                "wi^ekszych zwierz^at. Wok^o^l rozchodzi si^e szczeg^olny "+
                "zapach pr^ochniej^acego drewna, wilgoci i lasu.";
        }
        else
        {
            str = "G^esto rosn^ace buki, brzozy, klony i graby mieszaj^a "+
                "si^e ze sob^a i spl^atuj^a koronami. Wygl^adaj^a do^s^c "+
                "malowniczo, cho^c ca^la gromada po^lamanych konar^ow i "+
                "pr^ochniej^acych pni zalegaj^acych wok^o^l drzew nieco "+
                "psuje ten efekt. Tworz^a one spore kupki, nie do ko^nca "+
                "okryte ^sniegiem. Spod bia^lego puchu, nie^smia^lo, "+
                "wychylaj^a si^e gdzieniegdzie li^scie paproci, poszarza^le "+
                "i zeschni^ete, stercz^ace niczym swoiste flagi. Bia^la "+
                "powierzchnia pokryta jest tropami drobnych zwierz^at, "+
                "gdzieniegdzie ca^lkiem rozkopana, tak, ^ze wida^c pod "+
                "ni^a ciemn^a ziemi^e. Najwyra^xniej czuj^a si^e tu "+
                "zupe^lnie bezpieczne, z dala od dr^og i ludzkich siedzib.";
        }
        str += "\n";
    return str;
}

