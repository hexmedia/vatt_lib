/* Autor: Avard
   Data : 21.11.06
   Opis : Tinardan */

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <pogoda.h>
#include "dir.h"

inherit LAS_RINDE_STD;

void create_las() 
{
    set_short("Le^sna d^abrowa");
    add_exit(LAS_RINDE_LOKACJE + "las9.c","n",0,LAS_RO_FATIG,1);
    add_exit(LAS_RINDE_LOKACJE + "las26.c","sw",0,LAS_RO_FATIG,1);
    add_exit(LAS_RINDE_LOKACJE + "las25.c","s",0,LAS_RO_FATIG,1);
    add_exit(LAS_RINDE_LOKACJE + "las24.c","se",0,LAS_RO_FATIG,1);
    add_npc(LAS_RINDE_LIVINGI+"borsuk");

    add_prop(ROOM_I_INSIDE,0);
}

string
dlugi_opis()
{
    string str;
    if(jest_dzien() == 1)
    {
        if(pora_roku() != MT_ZIMA)
        {
            str = "Drzewa rosn^a do^s^c daleko od siebie, pot^e^zne d^eby o "+
                "sp^ekanej korze i kr^otkim pniu, roz^lo^zyste i dostojne. "+
                "Ziemi^e pokrywa do^s^c gruba warstwa li^sci, upstrzona "+
                "gdzieniegdzie k^epkami trawy. Las nie le^zy daleko od "+
                "ludzkich b^ad^x elfich sadyb � wida^c to wyra^xnie po "+
                "wydeptanych mi^edzy drzewami ^scie^zkach, cho^c trzeba "+
                "przyzna^c, ^ze s^a one jedynym znakiem obecno^sci "+
                "jakichkolwiek humanoid^ow w tej okolicy. Pnie u do^lu s^a "+
                "omsza^le i zdrowe, a le^sne zwierz^eta przemykaj^a gdzie^s "+
                "co chwil^e.";
        }
        else
        {
            str = "Drzewa rosn^a do^s^c daleko od siebie, pot^e^zne d^eby o "+
                "sp^ekanej korze i kr^otkim pniu, roz^lo^zyste i dostojne. "+
                "^Snieg zalega warstw^a do^s^c grub^a, ale gdzieniegdzie "+
                "wida^c jeszcze jesienne li^scie, br^azowe i pokryte "+
                "delikatn^a mgie^lk^a szronu. Las nie le^zy daleko od "+
                "ludzkich b^ad^x elfich sadyb � wida^c to wyra^xnie po "+
                "wydeptanych na ^sniegu ^scie^zkach, cho^c trzeba "+
                "przyzna^c, ^ze s^a one jedynym znakiem obecno^sci "+
                "jakichkolwiek humanoid^ow w tej okolicy. Pnie u do^lu "+
                "s^a omsza^le i zdrowe, a le^sne zwierz^eta przemykaj^a "+
                "gdzie^s co chwil^e.";
        }
    }
    if(jest_dzien() == 0)
    {
        if(pora_roku() != MT_ZIMA)
        {
            str = "Drzewa rosn^a do^s^c daleko od siebie, pot^e^zne d^eby o "+
                "sp^ekanej korze i kr^otkim pniu, roz^lo^zyste i dostojne. "+
                "Ziemi^e pokrywa do^s^c gruba warstwa li^sci, upstrzona "+
                "gdzieniegdzie k^epkami trawy. Las nie le^zy daleko od "+
                "ludzkich b^ad^x elfich sadyb � wida^c to wyra^xnie po "+
                "wydeptanych mi^edzy drzewami ^scie^zkach, cho^c trzeba "+
                "przyzna^c, ^ze s^a one jedynym znakiem obecno^sci "+
                "jakichkolwiek humanoid^ow w tej okolicy. Pnie u do^lu s^a "+
                "omsza^le i zdrowe, a le^sne zwierz^eta przemykaj^a gdzie^s "+
                "co chwil^e.";
        }
        else
        {
            str = "Drzewa rosn^a do^s^c daleko od siebie, pot^e^zne d^eby o "+
                "sp^ekanej korze i kr^otkim pniu, roz^lo^zyste i dostojne. "+
                "^Snieg zalega warstw^a do^s^c grub^a, ale gdzieniegdzie "+
                "wida^c jeszcze jesienne li^scie, br^azowe i pokryte "+
                "delikatn^a mgie^lk^a szronu. Las nie le^zy daleko od "+
                "ludzkich b^ad^x elfich sadyb � wida^c to wyra^xnie po "+
                "wydeptanych na ^sniegu ^scie^zkach, cho^c trzeba "+
                "przyzna^c, ^ze s^a one jedynym znakiem obecno^sci "+
                "jakichkolwiek humanoid^ow w tej okolicy. Pnie u do^lu "+
                "s^a omsza^le i zdrowe, a le^sne zwierz^eta przemykaj^a "+
                "gdzie^s co chwil^e.";
        }
    }
    str +="\n";
    return str;
}
string
opis_nocy()
{
    string str;
    if(pora_roku() != MT_ZIMA)
        {
            str = "Drzewa rosn^a do^s^c daleko od siebie, pot^e^zne d^eby o "+
                "sp^ekanej korze i kr^otkim pniu, roz^lo^zyste i dostojne. "+
                "Ziemi^e pokrywa do^s^c gruba warstwa li^sci, upstrzona "+
                "gdzieniegdzie k^epkami trawy. Las nie le^zy daleko od "+
                "ludzkich b^ad^x elfich sadyb � wida^c to wyra^xnie po "+
                "wydeptanych mi^edzy drzewami ^scie^zkach, cho^c trzeba "+
                "przyzna^c, ^ze s^a one jedynym znakiem obecno^sci "+
                "jakichkolwiek humanoid^ow w tej okolicy. Pnie u do^lu s^a "+
                "omsza^le i zdrowe, a le^sne zwierz^eta przemykaj^a gdzie^s "+
                "co chwil^e.";
        }
        else
        {
            str = "Drzewa rosn^a do^s^c daleko od siebie, pot^e^zne d^eby o "+
                "sp^ekanej korze i kr^otkim pniu, roz^lo^zyste i dostojne. "+
                "^Snieg zalega warstw^a do^s^c grub^a, ale gdzieniegdzie "+
                "wida^c jeszcze jesienne li^scie, br^azowe i pokryte "+
                "delikatn^a mgie^lk^a szronu. Las nie le^zy daleko od "+
                "ludzkich b^ad^x elfich sadyb � wida^c to wyra^xnie po "+
                "wydeptanych na ^sniegu ^scie^zkach, cho^c trzeba "+
                "przyzna^c, ^ze s^a one jedynym znakiem obecno^sci "+
                "jakichkolwiek humanoid^ow w tej okolicy. Pnie u do^lu "+
                "s^a omsza^le i zdrowe, a le^sne zwierz^eta przemykaj^a "+
                "gdzie^s co chwil^e.";
        }
    str +="\n";
    return str;
}

