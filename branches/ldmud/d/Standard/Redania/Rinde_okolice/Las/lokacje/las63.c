/* Autor: Avard
   Data : 01.12.06
   Opis : Yran */

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <pogoda.h>
#include "dir.h"

inherit LAS_RINDE_STD;

void create_las() 
{
    set_short("G^esty las");
    add_exit(LAS_RINDE_LOKACJE + "las54.c","n",0,LAS_RO_FATIG,1);
    add_exit(LAS_RINDE_LOKACJE + "las70.c","sw",0,LAS_RO_FATIG,1);
    add_exit(LAS_RINDE_LOKACJE + "las69.c","s",0,LAS_RO_FATIG,1);
    add_exit(TRAKT_RINDE_LOKACJE + "trakt33.c","se",0,LAS_RO_FATIG,1);
    add_prop(ROOM_I_INSIDE,0);
}

string
dlugi_opis()
{
    string str;
    if(jest_dzien() == 1)
    {
        if(pora_roku() != MT_ZIMA)
        {
            str = "G^esto ro^sn^ace wysokie d^eby, brzozy o "+
                "^snie^znobia^lych konarach i buki pn^a si^e wysoko w "+
                "g^or^e przys^laniaj^ac widok na najbli^zsz^a okolic^e. "+
                "Pod^lo^ze pokryte jest ma^lymi ga^l^azkami chrzeszcz^acymi "+
                "i ^lami^acymi si^e pod wp^lywem ka^zdego nawet "+
                "najmniejszego kroku, a d^xwi^ek ten niesie si^e dzi^eki "+
                "znakomitej akustyce okolicy hen daleko. Na ^sci^o^lce bez "+
                "trudu mo^zna zauwa^zy^c liczne ^slady le^snych "+
                "mieszka^nc^ow, kt^orzy sp^loszoni twoj^a obecno^sci^a "+
                "po^spiesznie przemykaj^a pomi^edzy grubymi pniami drzew "+
                "znikaj^ac w g^estwinie.";
        }
        else
        {
            str = "G^esto ro^sn^ace bezlistne wysokie d^eby, brzozy o "+
                "^snie^znobia^lych konarach i buki pn^a si^e wysoko w "+
                "g^or^e przys^laniaj^ac swoimi roz^lo^zystymi nagimi "+
                "ga^l^eziami widok na najbli^zsz^a okolic^e. Pod^lo^ze "+
                "pokryte jest bia^lym puchem, a le^zace na nim ma^le "+
                "ga^l^azkami, kt^ore pospada^ly z drzew podczas "+
                "silniejszych podmuch^ow wiatru chrzeszcz^a i ^lami^a "+
                "si^e pod wp^lywem ka^zdego nawet najmniejszego kroku, a "+
                "d^xwi^ek ten niesie si^e dzi^eki znakomitej akustyce "+
                "okolicy hen daleko. Na sniegu zalegaj^acym na ziemi bez "+
                "trudu mo^zna zauwa^zy^c liczne ^slady le^snych "+
                "mieszka^nc^ow, kt^orzy sp^loszoni twoj^a obecno^sci^a "+
                "po^spiesznie przemykaj^a pomi^edzy grubymi pniami drzew.";
        }
    }
    if(jest_dzien() == 0)
    {
        if(pora_roku() != MT_ZIMA)
        {
            str = "G^esto ro^sn^ace wysokie d^eby, brzozy o "+
                "^snie^znobia^lych konarach i buki pn^a si^e wysoko w "+
                "g^or^e przys^laniaj^ac widok na najbli^zsz^a okolic^e. "+
                "Pod^lo^ze pokryte jest ma^lymi ga^l^azkami chrzeszcz^acymi "+
                "i ^lami^acymi si^e pod wp^lywem ka^zdego nawet "+
                "najmniejszego kroku, a d^xwi^ek ten niesie si^e dzi^eki "+
                "znakomitej akustyce okolicy hen daleko. Na ^sci^o^lce bez "+
                "trudu mo^zna zauwa^zy^c liczne ^slady le^snych "+
                "mieszka^nc^ow, kt^orzy sp^loszoni twoj^a obecno^sci^a "+
                "po^spiesznie przemykaj^a pomi^edzy grubymi pniami drzew "+
                "znikaj^ac w g^estwinie.";
        }
        else
        {
            str = "G^esto ro^sn^ace bezlistne wysokie d^eby, brzozy o "+
                "^snie^znobia^lych konarach i buki pn^a si^e wysoko w "+
                "g^or^e przys^laniaj^ac swoimi roz^lo^zystymi nagimi "+
                "ga^l^eziami widok na najbli^zsz^a okolic^e. Pod^lo^ze "+
                "pokryte jest bia^lym puchem, a le^zace na nim ma^le "+
                "ga^l^azkami, kt^ore pospada^ly z drzew podczas "+
                "silniejszych podmuch^ow wiatru chrzeszcz^a i ^lami^a "+
                "si^e pod wp^lywem ka^zdego nawet najmniejszego kroku, a "+
                "d^xwi^ek ten niesie si^e dzi^eki znakomitej akustyce "+
                "okolicy hen daleko. Na sniegu zalegaj^acym na ziemi bez "+
                "trudu mo^zna zauwa^zy^c liczne ^slady le^snych "+
                "mieszka^nc^ow, kt^orzy sp^loszoni twoj^a obecno^sci^a "+
                "po^spiesznie przemykaj^a pomi^edzy grubymi pniami drzew.";
        }
    }
    str +="\n";
    return str;
}
string
opis_nocy()
{
    string str;
    if(pora_roku() != MT_ZIMA)
        {
            str = "G^esto ro^sn^ace wysokie d^eby, brzozy o "+
                "^snie^znobia^lych konarach i buki pn^a si^e wysoko w "+
                "g^or^e przys^laniaj^ac widok na najbli^zsz^a okolic^e. "+
                "Pod^lo^ze pokryte jest ma^lymi ga^l^azkami chrzeszcz^acymi "+
                "i ^lami^acymi si^e pod wp^lywem ka^zdego nawet "+
                "najmniejszego kroku, a d^xwi^ek ten niesie si^e dzi^eki "+
                "znakomitej akustyce okolicy hen daleko. Na ^sci^o^lce bez "+
                "trudu mo^zna zauwa^zy^c liczne ^slady le^snych "+
                "mieszka^nc^ow, kt^orzy sp^loszoni twoj^a obecno^sci^a "+
                "po^spiesznie przemykaj^a pomi^edzy grubymi pniami drzew "+
                "znikaj^ac w g^estwinie.";
        }
        else
        {
            str = "G^esto ro^sn^ace bezlistne wysokie d^eby, brzozy o "+
                "^snie^znobia^lych konarach i buki pn^a si^e wysoko w "+
                "g^or^e przys^laniaj^ac swoimi roz^lo^zystymi nagimi "+
                "ga^l^eziami widok na najbli^zsz^a okolic^e. Pod^lo^ze "+
                "pokryte jest bia^lym puchem, a le^zace na nim ma^le "+
                "ga^l^azkami, kt^ore pospada^ly z drzew podczas "+
                "silniejszych podmuch^ow wiatru chrzeszcz^a i ^lami^a "+
                "si^e pod wp^lywem ka^zdego nawet najmniejszego kroku, a "+
                "d^xwi^ek ten niesie si^e dzi^eki znakomitej akustyce "+
                "okolicy hen daleko. Na sniegu zalegaj^acym na ziemi bez "+
                "trudu mo^zna zauwa^zy^c liczne ^slady le^snych "+
                "mieszka^nc^ow, kt^orzy sp^loszoni twoj^a obecno^sci^a "+
                "po^spiesznie przemykaj^a pomi^edzy grubymi pniami drzew.";
        }
    str += "\n";
    return str;
}

