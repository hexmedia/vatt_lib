/* Autor: Avard
   Data : 21.11.06
   Opis : Tinardan */

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <pogoda.h>
#include "dir.h"

inherit LAS_RINDE_STD;

void create_las() 
{
    set_short("Le^sna d^abrowa");
    add_exit(LAS_RINDE_LOKACJE + "las2.c","ne",0,LAS_RO_FATIG,1);
    add_exit(LAS_RINDE_LOKACJE + "las3.c","nw",0,LAS_RO_FATIG,1);
    add_exit(LAS_RINDE_LOKACJE + "las16.c","s",0,LAS_RO_FATIG,1);

    add_prop(ROOM_I_INSIDE,0);
}

string
dlugi_opis()
{
    string str;
    if(jest_dzien() == 1)
    {
        if(pora_roku() != MT_ZIMA)
        {
            str = "Drzewa rosn^a do^s^c daleko od siebie, pot^e^zne d^eby o "+
                "sp^ekanej korze i kr^otkim pniu, roz^lo^zyste i dostojne. "+
                "Ziemi^e pokrywa do^s^c gruba warstwa li^sci, upstrzona "+
                "gdzieniegdzie k^epkami trawy. Las nie le^zy daleko od "+
                "ludzkich b^ad^x elfich sadyb � wida^c to wyra^xnie po "+
                "wydeptanych mi^edzy drzewami ^scie^zkach, cho^c trzeba "+
                "przyzna^c, ^ze s^a one jedynym znakiem obecno^sci "+
                "jakichkolwiek humanoid^ow w tej okolicy. Pnie u do^lu s^a "+
                "omsza^le i zdrowe, a le^sne zwierz^eta przemykaj^a gdzie^s "+
                "co chwil^e pozostawiaj^ac po sobie na mi^ekkim pod^lo^zu "+
                "wyra^xny trop.";
        }
        else
        {
            str = "Drzewa rosn^a do^s^c daleko od siebie, pot^e^zne d^eby o "+
                "sp^ekanej korze i kr^otkim pniu, roz^lo^zyste i dostojne. "+
                "^Snieg zalega warstw^a do^s^c grub^a, ale gdzieniegdzie "+
                "wida^c jeszcze jesienne li^scie, br^azowe i pokryte "+
                "delikatn^a mgie^lk^a szronu. Las nie le^zy daleko od "+
                "ludzkich b^ad^x elfich sadyb � wida^c to wyra^xnie po "+
                "wydeptanych na ^sniegu ^scie^zkach, cho^c trzeba "+
                "przyzna^c, ^ze s^a one jedynym znakiem obecno^sci "+
                "jakichkolwiek humanoid^ow w tej okolicy. Pnie u do^lu "+
                "s^a omsza^le i zdrowe, a le^sne zwierz^eta przemykaj^a "+
                "gdzie^s co chwil^e pozostawiaj^ac po sobie na bia^lym "+
                "puchu wyra^xny trop.";
        }
    }
    if(jest_dzien() == 0)
    {
        if(pora_roku() != MT_ZIMA)
        {
            str = "Drzewa rosn^a do^s^c daleko od siebie, pot^e^zne d^eby o "+
                "sp^ekanej korze i kr^otkim pniu, roz^lo^zyste i dostojne. "+
                "Ziemi^e pokrywa do^s^c gruba warstwa li^sci, upstrzona "+
                "gdzieniegdzie k^epkami trawy. Las nie le^zy daleko od "+
                "ludzkich b^ad^x elfich sadyb � wida^c to wyra^xnie po "+
                "wydeptanych mi^edzy drzewami ^scie^zkach, cho^c trzeba "+
                "przyzna^c, ^ze s^a one jedynym znakiem obecno^sci "+
                "jakichkolwiek humanoid^ow w tej okolicy. Pnie u do^lu s^a "+
                "omsza^le i zdrowe, a le^sne zwierz^eta przemykaj^a gdzie^s "+
                "co chwil^e pozostawiaj^ac po sobie na mi^ekkim pod^lo^zu "+
                "wyra^xny trop.";
        }
        else
        {
            str = "Drzewa rosn^a do^s^c daleko od siebie, pot^e^zne d^eby o "+
                "sp^ekanej korze i kr^otkim pniu, roz^lo^zyste i dostojne. "+
                "^Snieg zalega warstw^a do^s^c grub^a, ale gdzieniegdzie "+
                "wida^c jeszcze jesienne li^scie, br^azowe i pokryte "+
                "delikatn^a mgie^lk^a szronu. Las nie le^zy daleko od "+
                "ludzkich b^ad^x elfich sadyb � wida^c to wyra^xnie po "+
                "wydeptanych na ^sniegu ^scie^zkach, cho^c trzeba "+
                "przyzna^c, ^ze s^a one jedynym znakiem obecno^sci "+
                "jakichkolwiek humanoid^ow w tej okolicy. Pnie u do^lu "+
                "s^a omsza^le i zdrowe, a le^sne zwierz^eta przemykaj^a "+
                "gdzie^s co chwil^e pozostawiaj^ac po sobie na bia^lym "+
                "puchu wyra^xny trop.";
        }
    }
    str +="\n";
    return str;
}
string
opis_nocy()
{
    string str;
    if(pora_roku() != MT_ZIMA)
        {
            str = "Drzewa rosn^a do^s^c daleko od siebie, pot^e^zne d^eby o "+
                "sp^ekanej korze i kr^otkim pniu, roz^lo^zyste i dostojne. "+
                "Ziemi^e pokrywa do^s^c gruba warstwa li^sci, upstrzona "+
                "gdzieniegdzie k^epkami trawy. Las nie le^zy daleko od "+
                "ludzkich b^ad^x elfich sadyb � wida^c to wyra^xnie po "+
                "wydeptanych mi^edzy drzewami ^scie^zkach, cho^c trzeba "+
                "przyzna^c, ^ze s^a one jedynym znakiem obecno^sci "+
                "jakichkolwiek humanoid^ow w tej okolicy. Pnie u do^lu s^a "+
                "omsza^le i zdrowe, a le^sne zwierz^eta przemykaj^a gdzie^s "+
                "co chwil^e pozostawiaj^ac po sobie na mi^ekkim pod^lo^zu "+
                "wyra^xny trop.";
        }
        else
        {
            str = "Drzewa rosn^a do^s^c daleko od siebie, pot^e^zne d^eby o "+
                "sp^ekanej korze i kr^otkim pniu, roz^lo^zyste i dostojne. "+
                "^Snieg zalega warstw^a do^s^c grub^a, ale gdzieniegdzie "+
                "wida^c jeszcze jesienne li^scie, br^azowe i pokryte "+
                "delikatn^a mgie^lk^a szronu. Las nie le^zy daleko od "+
                "ludzkich b^ad^x elfich sadyb � wida^c to wyra^xnie po "+
                "wydeptanych na ^sniegu ^scie^zkach, cho^c trzeba "+
                "przyzna^c, ^ze s^a one jedynym znakiem obecno^sci "+
                "jakichkolwiek humanoid^ow w tej okolicy. Pnie u do^lu "+
                "s^a omsza^le i zdrowe, a le^sne zwierz^eta przemykaj^a "+
                "gdzie^s co chwil^e pozostawiaj^ac po sobie na bia^lym "+
                "puchu wyra^xny trop.";
        }
    str +="\n";
    return str;
}

