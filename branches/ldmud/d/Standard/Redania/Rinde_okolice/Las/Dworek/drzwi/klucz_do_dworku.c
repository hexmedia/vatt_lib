/* Autor: Avard
   Data : 03.02.06
   Info : Klucz do stajni Avarda i Faeve */

inherit "/std/key";

#include <stdproperties.h>
#include <pl.h>
#include "dir.h"

void
create_key()
{
    ustaw_nazwe("klucz");
    dodaj_przym("niedu^zy", "nieduzi");
    dodaj_przym("br^azowy", "br^azowi");
    set_long("Niedu^zy, wykonany z br^azu klucz, o do^s^c skomplikowanie "+
        "wykonanym j^ezyczku - tak, by trudno by^lo go podrobi^c. G^l^owka "+
        "klucza to trzy stykaj^ace si^e ze sob^a okr^egi. Do jednego z nich "+
        "przyczepiony jest ^la^ncuszek, dzi^eki kt^oremu klucz mo^zna "+
        "gdzie^s zawiesi^c.\n");
    
    set_key("KLUCZ_DRZWI_DO_DOMU_AVARDA_I_FAEVE");
}