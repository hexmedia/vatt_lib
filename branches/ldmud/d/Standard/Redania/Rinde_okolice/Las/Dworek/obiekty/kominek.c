/* Autor: Avard
   Opis : Ivrin
   Data : 02.09.06
   Info : Bezczelnie zerzniete z karczmy "Baszta" */

inherit "/std/object";

#include <pl.h>
#include <macros.h>
#include <filter_funs.h>
#include <stdproperties.h>
#include <composite.h>
#include <cmdparse.h>
#include <object_types.h>

int ile_pali=0,pali_sie=0;

nomask void
create_object()
{
    ustaw_nazwe("kominek");
    dodaj_przym("murowany","murowani");
    set_long("Murowany z ciemnej ceg^ly kominek jest obszerny i daje du^zo "+
        "ciep^la. Pod paleniskiem znajduj^e si^e wn^eka na szczapy drewna, "+
        "natomiast na gzymsie ustawiono kilka r^o^znych - prostych i "+
        "zdobionych, stalowych i srebrnych lichtarzy. "+
        "@@czy_plone_w_longu@@\n");

    setuid();
    seteuid(getuid());
    add_prop(OBJ_I_VOLUME, 16800);
    add_prop(OBJ_I_NO_GET, "Nie da si^e go podnie�^c.\n");
    add_prop(OBJ_I_WEIGHT, 10000);
    set_type(O_MEBLE);
}

string
czy_plone_w_longu()
{
    string str;
    if(ile_pali>15)
        str="W tej chwili pali si^e do�^c du^zym ogniem.";
    else if(ile_pali>10)
        str="W tej chwili pali si^e w nim ogie^n.";
    else if(ile_pali>5)
        str="W tej chwili pali si^e w nim ma^ly ogie^n.";
    else if(ile_pali>0)
        str="W tej chwili pali si^e w nim skromny ogie^n.";
    else
        str="W tej chwili jest nieczynny.";

    return str;
}

void
init()
{
    ::init();
    add_action("dokladamy","do^l^o^z");
    add_action("dokladamy","pod^l^o^z");
}

int
dokladamy(string str)
{
    mixed kominek_ob;
    object *co;
    //co=filter(co,all_inventory(TP));
    
    notify_fail(capitalize(query_verb())+" co do czego?\n");
    
    if(!stringp(query_verb()))
        return 0;
    if(!strlen(str))
        return 0;
        
   co=INV_ACCESS(co);
   if(!parse_command(str, environment(TP),
        "%i:"+PL_BIE+" 'do' %i:"+PL_DOP, co, kominek_ob))
        return 0;     
}


jestem_komineczkiem_w_salonie_avarda()
{
    return 1;
}

void eventy()
{
    string str;
    switch(random(8))
    {
        case 0: str="P^lomie^n w kominku grzeje przyjemnie."; break;
        case 1: str="Ogniste j^ezyki w kominku skacz� figlarnie."; break;
        case 2: str="Drewno w kominku trzaska pod wp^lywem ciep^la."; break;
        case 3: str="Z kominka dochodz� twych uszu odg^losy trzaskaj�cych "+
                    "ga^l�zek."; break;
        case 4: str="Czujesz nagrzane przez kominek powietrze."; break;
        case 5: str="Ciep^lo bij�ce z kominka grzeje ci^e przyjemnie."; break;
        case 7: str="Powoli robi si^e coraz cieplej."; break;
    }
    str+="\n";
    
    if(pali_sie)
        tell_room(environment(TO),str);
    
    float f;
    switch(random(10))
    {
        case 0:f=80.0;break;
        case 1:f=120.0;break;
        case 2:f=150.0;break;
        case 3:f=200.0;break;
        case 4:f=240.0;break;
        case 5:f=300.0;break;
        case 6:f=360.0;break;
        case 7:f=400.0;break;
        case 8:f=430.0;break;
        case 9:f=660.0;break;
    }    
    ile_pali++;
    
    if(ile_pali>20)
    {
        tell_room(environment(TO),"Ogie^n w kominku powoli przygasa.\n");
        pali_sie=0;
        ile_pali=0;
    }    
    else
        set_alarm(f,0.0,"eventy");
}
    

void
palimy()
{
    if(!ile_pali) 
        ile_pali=1;
    pali_sie=1;
    set_alarm(2.0,0.0,"eventy");
}

int
query_ile_pali()
{
    return ile_pali;
}
int
query_pali_sie()
{
    return pali_sie;
}