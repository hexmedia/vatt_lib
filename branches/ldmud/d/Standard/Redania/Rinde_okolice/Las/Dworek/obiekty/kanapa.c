/* wykonane przez valora dnia 2.09.06
opis napisany przez valora
*/

inherit "/std/object.c";

#include <pl.h>
#include <stdproperties.h>
#include <macros.h>
#include <filter_funs.h>
#include <materialy.h>

create_object()
{
    ustaw_nazwe("kanapa"),
                 
    dodaj_przym("d�ugi","d�udzy");
    dodaj_przym("sk�rzany","sk�rzani");
   
    make_me_sitable("na kanapie","na kanapie","z kanapy",3);

    set_long("D^luga kanapa obita zosta^la ciemnobr^azow^a sk^or^a. "+
        "Wyrze^xbione z bukowego drewna przednie cz^e^sci pod^lokietnik^ow "+
        "przedstawiaj^a wij^ace si^e w^e^ze, dooko^la kt^orych umieszczono "+
        "miedziane nity maj^ace za zadanie utrzyma^c sk^or^e napi^et^a, a "+
        "jednocze^snie traktowane s^a jako ozdoby. Oparcie posiada cztery "+
        "niedu^ze wg^l^ebienia, a w ka^zdym z nich umieszczono okr^ag^ly "+
        "guzik przytrzymuj^acy obicie. Niebywa^la spr^e^zysto^s^c i spore "+
        "rozmiary sprawiaj^a, ^ze kanapa jest niezwykle wygodna oraz mo^ze "+
        "pomie^sci^c nawet trzy osoby.\n");
        
    add_prop(OBJ_I_WEIGHT, 50000);
    add_prop(OBJ_I_VOLUME, 10000);
    add_prop(OBJ_I_VALUE, 5000);
    ustaw_material(MATERIALY_DR_BUK, 40);
    ustaw_material(MATERIALY_MIEDZ, 20);
    ustaw_material(MATERIALY_SK_BYDLE, 40);
}
