
/* Autor: Avard
   Data : 03.09.06
   Info : Drzwi do stajni w dworku */

inherit "/std/door";

#include <pl.h>
#include "dir.h"

void
create_door()
{
    ustaw_nazwe("wrota");
    dodaj_przym("dwudzielny","dwudzielni");
    dodaj_przym("zdobiony", "zdobieni");

    set_other_room(DWOREK_LOKACJE + "stajnia.c");
    set_door_id("DRZWI_DO_STAJNI_DWORKU_AVARDA");
    set_door_desc("Dwudzielne wrota na ozdobnych zawiasach z ciemnej "+
        "stali, zamykane na k^l^odk^e.\n");

    set_open_desc("");
    set_closed_desc("");
    set_open(0);
    set_locked(1);

    set_pass_command(({"wrota","dwudzielne wrota","zdobione wrota",
        "dwudzielne zdobione wrota","stajnia"}),"przez zdobione wrota",
        "z zewn^atrz");

    set_key("KLUCZ_DRZWI_DO_DOMU_AVARDA_I_FAEVE");
    set_lock_name(({"k^l^odka", "k��dki", "k��dce", "k��dka", "k��dk�", "k��dce"}), 0, PL_ZENSKI);
    set_lock_mess("zamyka wrota na k^l^odk^e.\n",
        "Zamykasz wrota na k^l^odk^e.\n",
        "Kto^s z drugiej strony zamyka wrota na k^l^odk^e.\n");
}
