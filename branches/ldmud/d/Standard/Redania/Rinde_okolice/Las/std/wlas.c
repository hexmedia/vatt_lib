
/* Autor: Avard
   Data : 24.09.06
   Info : Std do lokacji wykarczowanego lasu w poblizu Rinde */
#include "dir.h"

inherit REDANIA_STD;

#include <mudtime.h>
#include <language.h>
#include <macros.h>
#include <stdproperties.h>

void
create_las()
{
}

nomask void
create_redania()
{
    set_long("@@dlugi_opis@@");
    set_polmrok_long("@@opis_nocy@@");

    /*add_item(({"drzewa","las"}), "Tak, tutaj powinien byc jakis opis :P\n");
  FIXME TODO  add_item(({"pie^nki","pnie"}), "Tak, tutaj powinien byc jakis opis :P\n");
*/
    add_event("@@event_lasu:"+file_name(TO)+"@@");

    set_event_time(300.0);

    add_sit(({"na trawie","na ziemi"}),"na trawie","z trawy",0);
    add_sit("pod drzewem","pod jednym z drzew","spod drzewa",0);
    add_sit("na pniu","na jednym z pni","z pniaka",1);
    add_prop(ROOM_I_TYPE, ROOM_FOREST);

    add_subloc(({"drzewo", "drzewa", "drzewu", "drzewo",
        "drzewem", "drzewie"}));
    add_subloc_prop("drzewo", SUBLOC_I_MOZNA_POLOZ, 1);
    add_subloc_prop("drzewo", SUBLOC_I_MOZNA_ODLOZ, 1);
    add_subloc_prop("drzewo", SUBLOC_I_TYP_POD, 1);

    create_las();
}
public int
unq_no_move(string str)
{
    notify_fail("Niestety, g^este zaro^sla nie pozwalaj^a ci "+
        "p^oj^s^c w tym kierunku.\n");
    return 0;
}
string
event_lasu()
{
    switch (pora_dnia())
    {
	case MT_RANEK:
	case MT_POLUDNIE:
	case MT_POPOLUDNIE:
        return process_string(({"Nad twoj^a g^low^a przelecia^l jaki^s "+
        "niedu^zy ptak.\n",
        "Gdzie^s niedaleko s^lyszysz g^lo^sne pokrzykiwania.\n",
        "Wysoko nad tob^a z szeroko rozpostartymi skrzyd^lami przelatuje "+
        "jastrz^ab.\n",
        "Dochodzi ci� intensywny zapach �ywicy i �wie�o �ci�tych drzew.\n",
        "@@pora_roku_dzien@@"})[random(5)]);
	default: // wieczor, noc
        return process_string(({"Masz wra^zenie, ^ze obserwuje ci^e jakie^s "+
        "zwierz^e.\n","W oddali s^lyszysz ryk dzikiego zwierz^ecia.\n",
        "Dochodzi ci� intensywny zapach �ywicy i �wie�o �ci�tych drzew.\n",
        "@@pora_roku_noc@@"})[random(3)]);
    }
}
string
pora_roku_dzien()
{
    switch (pora_roku())
    {
         case MT_LATO:
         case MT_WIOSNA:
             return ({"Z lasu wychyli^la si^e smuk^la sarna, jednak "+
             "sp^loszona twoj^a obecno^sci^a zaszy^la si^e w g^estwinie.\n",
             "W oddali s^lycha^c rytmiczne uderzenia drwalskich topor^ow.\n",
             "Dobiega do ciebie coraz wyra^xniejszy szum drzew.\n",
             "Ptasi trel wznosi si^e w weso^lym crescendo, po czym znowu "+
             "cichnie i uspokaja si^e.\n",
             "Ciep^ly wiatr muska twoj^a twarz.\n",
             "Z oddali dobiegaj^a rytmiczne uderzenia topora.\n"})[random(6)];
         case MT_JESIEN:
             return ({"Zimny wiatr muska ci^e po twarzy.\n",
             "Trawa ko^lysze si^e pod mu^sni^eciem wiatru.\n"})
             [random(2)];
         case MT_ZIMA:
             return ({"Zimny wiatr muska ci^e po twarzy.\n",
             "Lodowaty wiatr muska ci^e po twarzy.\n"})[random(2)];
    }
}
string
pora_roku_noc()
{
    switch (pora_roku())
    {
         case MT_LATO:
         case MT_WIOSNA:
             return ({"Trawa ko^lysze si^e pod mu^sni^eciem wiatru.\n"})
             [random(1)];
         case MT_JESIEN:
             return ({"Zimny wiatr muska ci^e po twarzy.\n",
             "Trawa ko^lysze si^e pod mu^sni^eciem wiatru.\n"})
             [random(2)];
         case MT_ZIMA:
             return ({"Zimny wiatr muska ci^e po "+
             "twarzy.\n","Lodowaty wiatr muska ci^e po twarzy.\n"})[random(2)];
    }
}

