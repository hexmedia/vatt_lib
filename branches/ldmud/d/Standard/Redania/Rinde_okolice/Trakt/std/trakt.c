
/* Std do lokacji traktu w poblizu Rinde
   Made by Avard, 04.07.06, ku chwale Swiatowida! */

#include "dir.h"

inherit REDANIA_STD;

#include <mudtime.h>
#include <language.h>
#include <macros.h>
#include <stdproperties.h>
#include <pogoda.h>

void
create_trakt()
{
}

nomask void
create_redania()
{
    set_long("@@dlugi_opis@@");
    set_polmrok_long("@@opis_nocy@@");

    /*add_item(({"trakt","droge"}), "\n"); TODO FIXME*/

    add_event("@@event_traktu:"+file_name(TO)+"@@");
	add_prop(ROOM_I_TYPE, ROOM_TRACT);
    set_event_time(300.0);

    add_sit("na ziemi","na ziemi","z ziemi",0);

    dodaj_ziolo("arcydziegiel.c");
    dodaj_ziolo("babka_lancetowata.c");
	dodaj_ziolo(({"czarny_bez-lisc.c",
				"czarny_bez-kwiaty.c",
				"czarny_bez-owoce.c"}));
    dodaj_ziolo(({"krwawnik_ziele.c",
                "krwawnik_kwiatostan.c"}));
	dodaj_ziolo("piolun.c");

    create_trakt();
}

string
event_traktu()
{
    return "";
}
/*
string
event_traktu()
{
    switch (pora_dnia())
    {
	case MT_RANEK:
	case MT_POLUDNIE:
	case MT_POPOLUDNIE:
	    return ({"\n",
        "@@pora_roku_dzien@@"})[random(5)];
	default: // wieczor, noc
	    return ({"\n",
        "@@pora_roku_noc@@"})[random(5)];
    }
}

string
pora_rok_dzien()
{
    switch (pora_roku())
    {
         case MT_LATO:
         case MT_WIOSNA:
             return ({"\n"})[random(3)];
         case MT_JESIEN:
             return ({"\n"})[random(5)];
         case MT_ZIMA:
             return ({"\n"})[random(7)];
    }
}
string
pora_rok_noc()
{
    switch (pora_roku())
    {
         case MT_LATO:
         case MT_WIOSNA:
             return ({"\n"})[random(1)];
         case MT_JESIEN:
             return ({"\n"})[random(5)];
         case MT_ZIMA:
             return ({"\n"})[random(7)];
    }
}*/
