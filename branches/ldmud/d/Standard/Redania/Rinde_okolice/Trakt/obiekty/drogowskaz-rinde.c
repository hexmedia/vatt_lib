
inherit "/std/object.c";

#include <pl.h>
#include <stdproperties.h>
#include <macros.h>
#include <filter_funs.h>

create_object()
{

    ustaw_nazwe("drogowskaz");
                 
    dodaj_przym("drewniany","drewniani");
    dodaj_przym("niewielki","niewielcy");
      
    set_long("    *************************\n"+
             "    *                       *\n"+
             "    *           RINDE       *\n"+
             "    *             |         *\n"+
             "    *             |         *\n"+
             "    *   BIALY     |         *\n"+
             "    *    MOST-----O         *\n"+
             "    *              \\        *\n"+
             "    *               \\       *\n"+
             "    *                \\      *\n"+
             "    *              MURIVEL  *\n"+
             "    *                       *\n"+
             "    *                       *\n"+
             "    *************************\n"+
             "              |    |         \n"+
             "              |    |         \n"+
             "              |    |         \n");
    add_prop(OBJ_I_NO_GET, "Ten drogowskaz jest mocno wbity "+
                           "w ziemi^e, nie masz do^s^c si^ly by go "+
                           "wyrwa^c.\n");

}
