
/* Autor: Sedzik
   Opis : Zwierzaczek
   Data : 18.03.07 */

inherit "/std/weapon";

#include <stdproperties.h>
#include <wa_types.h>
#include <pl.h>
#include <materialy.h>

void
create_weapon() 
{
     ustaw_nazwe("pa^lka");
                    
     dodaj_przym("prosty","pro^sci");
     dodaj_przym("ciemny","ciemni");
                    
     set_long("Prosta bro^n, ale jednak bro^n, kt^ora niejednemu pewnie czaszk^e "
	 +"rozbi^la. Umiej^etnie u^zyta, mo^ze by^c zab^ojcza, a je^sli nawet nie, to na "
	 +"pewno zostaj^a dotkliwe si^nce i obra^zenia. Wykonana zosta^la z ciemnego "
	 +"drewna, a w niekt^orych miejscach z^lowr^o^zbnie odstaj^a drzazgi.\n");

     set_hit(18);
     set_pen(13);

     set_wt(W_CLUB);
     set_dt(W_BLUDGEON);

     set_hands(W_ANYH);
     
     add_prop(OBJ_I_VALUE, 800);
     add_prop(OBJ_I_WEIGHT, 1500);
     add_prop(OBJ_I_VOLUME, query_prop(OBJ_I_WEIGHT)/5);
	 ustaw_material(MATERIALY_DR_DAB);
 }

