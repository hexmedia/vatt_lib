/*
 * 
 * kaftan dla pana ze swiatyni
 * Faeve - Haloween '06
 * 
 */

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <macros.h>
#include <materialy.h>
#include <object_types.h>
#include <pl.h>

void
create_armour()
{
    ustaw_nazwe("kaftan");


    dodaj_przym("sk^orzany","sk^orzani");
    dodaj_przym("cwiekowany","cwiekowani");

    set_long("Siegaj^acy bioder, uszyty z grubej, czarnej sk^ory kaftan nie "+
    "jest ani modny, ani zgrabny, ani tym bardziej ^ladny. Wr^ecz "+
    "przeciwnie - nosi ^slady ewidentnego zu^zycia i zaniedbania. Musia^l "+
    "nie by^c czyszczony od dawien dawna. Zdaje si^e, ^ze w^la^sciciel "+
    "zapomnia^l tak^ze o zabiegach konserwacyjnych, gdy^z sk^ora jest "+
    "miejscami poprzecierana i jakby za^lamana. Mimo to, kaftan "+
    "poprzebijany, g^l^ownie na ramionach i ^lokciach, stercz^acymi na "+
    "grubo^s^c kciuka ^cwiekami, mo^ze stanowi^c pewn^a ochron^e. "+
    "Jednak nie jest najlepszym pomys^lem powierza^c mu swoje ^zycie. \n");

    set_slots(A_TORSO, A_ARMS);
    set_ac(A_TORSO, 1,1,1, A_ARMS, 1,1,1);
    
    add_prop(OBJ_I_WEIGHT, 1700); 
    add_prop(OBJ_I_VOLUME, 1000);
    add_prop(OBJ_I_VALUE, 217);
    
    set_type(O_UBRANIA);
    
    ustaw_material(MATERIALY_SK_NIEDZWIEDZ, 95);
    ustaw_material(MATERIALY_STAL, 5);
    
    add_prop(ARMOUR_S_DLA_RASY, "czlowiek"); 
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    set_size("L");
}
