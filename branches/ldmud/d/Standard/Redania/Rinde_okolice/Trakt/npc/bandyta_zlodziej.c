
/* Autor: Avard
   Opis : Eria
   Data : 17.03.07 */

inherit "/std/humanoid.c";

#include <std.h>
#include <ss_types.h>
#include <macros.h>
#include <stdproperties.h>
#include <pl.h>
#include "dir.h"

void
create_humanoid()
{
    ustaw_odmiane_rasy("m^e^zczyzna");
    set_gender(G_MALE);
    dodaj_nazwy("bandyta");
    dodaj_nazwy("zb^oj");
    random_przym("oszpecony:oszpeceni brudny:brudni "+
        "^smierdz^acy:^smierdz^acy zaniedbany:zaniedbani "+
        "zakrwawiony:zakrwawieni zaro^sni^ety:zaro^sni^eci||"+
        "agresywny:agresywni nerwowy:nerwowi wysoki:wysocy "+
        "niski:niscy chudy:chudzi wychudzony:wychudzeni blady:bladzi",2);

    set_long("Spogl^adaj^ac na tego m^e^zczyzn^e, pierwsze co rzuca si^e "+
        "w oczy jest d^luga, szeroka blizna, kt^ora oszpeca jego i tak "+
        "ma^lo atrakcyjn^a twarz. Zaschni^ete plamy krwi na jego czole, "+
        "nosie i policzku nie ^swiadcz^a o nim najlepiej, a krzywy "+
        "u^smieszek, kt^ory co rusz pojawia si^e na jego twarzy, mo^ze "+
        "przyprawia^c o dreszcze, a nie^swie^zy zapach niejednego "+
        "zniech^eci. Brudne i podarte ubrania, kt^ore z trudem "+
        "przypominaj^a odzienie, ozdobione s^a zaschni^etymi jak i "+
        "zar^owno ^swie^zymi ^sladami po krwi, a przez plecy "+
        "przewieszon^a ma r^ownie^z nieciekawie wygl^adaj^ac^a torb^e. "+
        "M^e^zczyzna nie wygl^ada na takiego, kt^ory ma dobre intencje, "+
        "na co wskazuj^a nie tylko gro^xne pomruki, nieprzyjazny wygl^ad "+
        "tej osoby, a tak^ze b^lyszcz^acy sztylet, kt^ory trzyma w r^eku.\n");
    set_stats (({55,150,55,40,95}));//FIXME balans przed otwarciem
    add_prop(CONT_I_WEIGHT, 60000);
    add_prop(CONT_I_HEIGHT, 175);
    add_prop(LIVE_I_NEVERKNOWN, 1);
    set_aggressive(1);
    set_attack_chance(100);
    set_npc_react(0);

    set_cact_time(10);
    add_cact("emote warczy gro^xnie.");
    add_cact("emote spogl�da na ciebie wrogo.");
    add_cact("emote spluwa flegm� na ziemie.");
    add_cact("'^Smiesz ze mn^a walczy�? Ty?! ^Smiesz na mnie bro� podnosi�?");
    add_cact("zasmiej sie glosno");
    add_cact("Niejednego ju� obdar�em ze sk�ry");
    add_cact("powiedz I tak ze mn^a nie wygrasz!");
    add_cact("splun");
    add_cact("powiedz Ze mn^a si� nie zaczyna!");
    add_cact("powiedz Poddaj si� to szybko zginiesz.");
    add_cact("powiedz Nawet nie pr�buj si� stawia�.");

    add_armour(TRAKT_RINDE_OBIEKTY +"czarne_luzne_Lc.c");
    add_armour(TRAKT_RINDE_OBIEKTY +"stary_cwiekowany_Lc.c");
    add_armour(TRAKT_RINDE_OBIEKTY +"znoszone_skorzane_Lc.c");
    add_weapon(TRAKT_RINDE_OBIEKTY +"czarny_zakrzywiony.c");
    add_armour(TRAKT_RINDE_OBIEKTY +"lekkie_skorzane_Lc.c");
    set_default_answer(VBFC_ME("default_answer"));

    set_skill(SS_DEFENCE, 20 + random(6));
    set_skill(SS_PARRY, 40 + random(8));
    set_skill(SS_WEP_KNIFE, 70 + random(11));

}
void
powiedz_gdy_jest(object player, string tekst)
{
    if (environment(this_object()) == environment(player))
        this_object()->command(tekst);
}

string
default_answer()
{
    set_alarm(2.0, 0.0, "powiedz_gdy_jest", this_player(),
        "powiedz do "+ this_player()->query_name(PL_BIE) +
        "Chyba nie my^slisz, ^ze b^ede odpowiada^l na twoje pytania?");
    return "";
}

void
add_introduced(string imie_mia, string imie_bie)
{
    set_alarm(2.0, 0.0, "return_introduce", find_player(imie_mia));
}
void
return_introduce(object ob)
{
    command("'A co mnie to obchodzi?");
}

void
emote_hook(string emote, object wykonujacy, string przyslowek)
{
    switch (emote)
    {
        case "kopnij":
        case "spoliczkuj":
        case "opluj":
        case "nadepnij":
        case "poca^luj":
        case "przytul":
        case "pog^laszcz":
        case "szturchnij":
            set_alarm(1.0, 0.0, "command", "powiedz Nie wa� si� wi�cej tego robi�, gnido!");
            break;
    }
}
