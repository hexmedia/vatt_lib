
/* Autor: Sedzik
   Opis : Valor
   Data : 19.03.07 */

inherit "/std/humanoid.c";

#include <std.h>
#include <ss_types.h>
#include <macros.h>
#include <stdproperties.h>
#include <pl.h>
#include <money.h>
#include "dir.h"

void
create_humanoid()
{
    ustaw_odmiane_rasy("krasnolud");
    set_gender(G_MALE);
    dodaj_nazwy("bandyta");
    dodaj_nazwy("zb^oj");
    random_przym("oszpecony:oszpeceni brudny:brudni "+
        "^smierdz^acy:^smierdz^acy zaniedbany:zaniedbani "+
        "zakrwawiony:zakrwawieni w^lochaty:w^lochaci brodaty:brodaci||"+
        "agresywny:agresywni umi^e^sniony:umi^e^snieni "+
        "niski:niscy kr^epy:kr^epi ogorza^ly:ogorzali hardy:hardzi",2);

    set_long("Na pierwszy rzut oka krasnolud przypomina okr^ag^la w^lochata kul^e "
	+"z wielkim nosem i wy^lupiastymi oczami. Jego kasztanowe w^losy si^egaj^ace "
	+"do pasa posklejane s^a krwi^a i brudem, natomiast zapleciona w gruby "
	+"warkocz broda w kt^orej mo^zna dostrzec kawa^lki jedzenia niemal si^ega "
	+"ziemi. Kr^epa postura i kr^otkie ko^nczyny nie przeszkadzaj^a mu w zwinnym "
	+"poruszaniu si^e, a toporek kt^ory trzyma w r^ece wygl^ada jakby by^l gotowy "
	+"do u^zytku w ka^zdym momencie.\n");

    set_stats (({ 110, 50, 150, 35, 100 }));
    add_prop(CONT_I_WEIGHT, 90000);
    add_prop(CONT_I_HEIGHT, 130);

    set_cact_time(10);
    add_cact("krzyknij Psia ma^c!");
	add_cact("powiedz Psi synu!");
	add_cact("krzyknij Kruczym na ^zarcie z ciebie nie stanie!");
	add_cact("krzyknij Ju^ze^s martwy!");

    set_default_answer(VBFC_ME("default_answer"));


    add_armour(TRAKT_RINDE_OBIEKTY +"czarne_luzne_Lk.c");
    add_armour(TRAKT_RINDE_OBIEKTY +"stary_cwiekowany_Lk.c");
    add_armour(TRAKT_RINDE_OBIEKTY +"znoszone_skorzane_Lk.c");
    add_armour(TRAKT_RINDE_OBIEKTY +"lekkie_skorzane_Lk.c");
    add_weapon(TRAKT_RINDE_OBIEKTY +"dlugi_lekki.c");

    set_skill(SS_DEFENCE, 20 + random(6));
    set_skill(SS_PARRY, 40 + random(8));
    set_skill(SS_WEP_AXE, 70 + random(11));

    add_prop(LIVE_I_NEVERKNOWN, 1);

	set_aggressive(1);
    set_attack_chance(100);
    set_npc_react(0);
}


void
powiedz_gdy_jest(object player, string tekst)
{
    if (environment(this_object()) == environment(player))
        this_object()->command(tekst);
}

string
default_answer()
{
    set_alarm(2.0, 0.0, "powiedz_gdy_jest", this_player(),
        "powiedz Zdychaj psie!");
    return "";
}



void
add_introduced(string imie_mia, string imie_bie)
{
    set_alarm(2.0, 0.0, "return_introduce", find_player(imie_mia));
}
void
return_introduce(object ob)
{
    command("powiedz Tyle ci^e to obchodzi!");
}

void
emote_hook(string emote, object wykonujacy)
{
    switch (emote)
    {
        case "kopnij": set_alarm(1.5, 0.0, "nienajlepszy", wykonujacy);
                    break;
        case "spoliczkuj": set_alarm(1.5, 0.0, "nienajlepszy", wykonujacy);
                    break;
        case "opluj" : set_alarm(2.5, 0.0, "nienajlepszy", wykonujacy);
                    break;
        case "poca^luj":
              {
              if(wykonujacy->query_gender() == 1)
                switch(random(1))
                 {
                 case 0: command("emote u^smiecha si^e obrzydliwie."); break;
                }
              else
                {
                   switch(random(1))
                   {
                   case 0:command("'Kruczym ci^e rzuc^e!"); break;
                   }
                }

                break;
                }
        case "prychnij":
              {
                switch(random(1))
                 {
                 case 0:command("spojrzyj hardo "+
                     "na "+ OB_NAME(wykonujacy)); break;
                 }
                break;
               }
        case "przytul": set_alarm(0.5, 0.0, "malolepszy", wykonujacy);
                   break;
    }
}

void
malolepszy(object wykonujacy)
{
    switch(random(1))
    {
        case 0: set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
			"'Chyba ci sw^edzi!");
    }
}

void
nienajlepszy(object wykonujacy)
{
  switch(random(2))
    {
    case 0:
      command("'Zdychaj!");
      break;
    case 1:
      command("'Kruczym!");
      break;
    }
}
