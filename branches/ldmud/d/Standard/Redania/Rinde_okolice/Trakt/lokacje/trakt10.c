/* Autor: Avard
   Opis : Brzozek
   Data : 10.11.06 */ 
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"
#include <pogoda.h>

inherit TRAKT_RINDE_STD;

void create_trakt() 
{
    set_short("Trakt po�r�d ��k");
    add_exit(TRAKT_RINDE_LOKACJE + "trakt9.c","n",0,TRAKT_RO_FATIG,0);
    add_exit(TRAKT_RINDE_LOKACJE + "trakt11.c","sw",0,TRAKT_RO_FATIG,0);
    add_exit(LAKA_RINDE_LOKACJE + "laka41.c","nw",0,TRAKT_RO_FATIG,0);
    add_exit(LAKA_RINDE_LOKACJE + "laka47.c","w",0,TRAKT_RO_FATIG,0);

    add_prop(ROOM_I_INSIDE,0);
}
public string
exits_description() 
{
    return "Trakt prowadzi na p^o^lnoc i po^ludniowy-zach^od.\n";
}

string
dlugi_opis()
{
    string str;
    if(jest_dzien())
    {
        if(pora_roku() != MT_ZIMA)
        {
            str = "Szeroki, wygodny trakt wy�o�ony dopasowanymi kamieniami "+
            "biegnie od bram Rinde, w kierunku po�udniowo-zachodnim, "+
            "skrajem wzg�rza �agodnie opadaj�cego w stron� Pontaru. W "+
            "stertach pokruszonych kamieni wznosz�cych si� po obu stronach "+
            "drogi z trudem mo�na rozpozna� pozosta�o�ci po niewysokich "+
            "kamiennych murkach zbudowanych niegdy� z bry� piaskowca, a "+
            "obecnie zniszczonych przez wiatry i deszcze, poro�ni�tych "+
            "przez ma�e krzaczki i ruderalne zielsko, a i nie oszcz�dzanych "+
            "przez w�drowc�w przysiadaj�cych tutaj w oczekiwaniu na "+
            "otwarcie bram miasta. Widoczne na skraju �agodnego wzg�rza "+
            "na zachodzie podobne kamieniste pozosta�o�ci ci�gn� si� "+
            "wzd�u� le��cego tam traktu do Bia�ego Mostu, a pomi�dzy "+
            "wzg�rzami ograniczone od wschodu i zachodu stromymi skarpami "+
            "rozci�ga si� bagniste rozlewisko Pontaru poro�ni�te trawami i "+
            "nielicznymi rachitycznymi drzewkami. Na zachodzie a� po "+
            "widoczn� w oddali zielon� kres� lasu, rozpo�cieraj� si� ";
            if(pora_roku() == MT_LATO)
            {
                str += "malownicze ";
            }
            if(pora_roku() == MT_WIOSNA)
            {
                str += "rozdzwonione �wiergotem ptak�w ";
            }
            if(pora_roku() == MT_JESIEN)
            {
                str += "z�ociste ";
            }
            if(CO_PADA(this_object()) == PADA_DESZCZ)
            {
                str += "szare w strugach deszczu ";
            }
            str += "��ki. ";
        }
        else
        {
            str = "Szeroki, wygodny trakt wy�o�ony dopasowanymi kamieniami "+
            "biegnie od bram Rinde, w kierunku po�udniowo-zachodnim, "+
            "skrajem wzg�rza �agodnie opadaj�cego w stron� Pontaru. �nie�ne "+
            "zaspy wznosz�ce si� po obu stronach drogi os�aniaj� nieco "+
            "przed wiej�cym wiatrem ale nie zach�caj� do odpoczynku. "+
            "Widoczne na skraju �agodnego wzg�rza na zachodzie podobne "+
            "zaspy ci�gn� si� wzd�u� le��cego tam traktu do Bia�ego Mostu, "+
            "a pomi�dzy wzg�rzami ograniczone od wschodu i zachodu stromymi "+
            "skarpami rozci�ga si� bagniste rozlewisko Pontaru pokryte "+
            "cienk� warstewk� kruchego lodu w kt�rym tkwi� uwi�zione tu i "+
            "�wdzie rachityczne drzewka. Na zachodzie a� po widoczn� w "+
            "oddali ciemn� kres� lasu, rozpo�ciera si� bia�a, l�ni�ca w "+
            "promieniach s�o�ca, pokryta �niegiem r�wnina.";
        }
    }
    else
    {
        if(pora_roku() != MT_ZIMA)
        {
            str = "Szeroki, wygodny trakt wy�o�ony dopasowanymi kamieniami "+
            "biegnie od bram Rinde, w kierunku po�udniowzachodnim, skrajem "+
            "wzg�rza �agodnie opadaj�cego w stron� Pontaru. W stertach "+
            "pokruszonych kamieni wznosz�cych si� po obu stronach drogi z "+
            "trudem mo�na rozpozna� pozosta�o�ci po niewysokich kamiennych "+
            "murkach zbudowanych niegdy� z bry� piaskowca a obecnie "+
            "zniszczonych przez wiatry i deszcze, poro�ni�tych przez ma�e "+
            "krzaczki i ruderalne zielsko a i nie oszcz�dzanych przez "+
            "w�drowc�w przysiadaj�cych tutaj w oczekiwaniu na otwarcie bram "+
            "miasta. Widoczne na skraju �agodnego wzg�rza na zachodzie "+
            "podobne kamieniste pozosta�o�ci ci�gn� si� wzd�u� le��cego "+
            "tam traktu do Bia�ego Mostu a pomi�dzy wzg�rzami ograniczone "+
            "od wschodu i zachodu stromymi skarpami rozci�ga si� bagniste "+
            "rozlewisko Pontaru poro�ni�te trawami i nielicznymi "+
            "rachitycznymi drzewkami. Na zachodzie a� po majacz�c� w "+
            "ciemno�ci ciemn� kres� lasu, rozpo�cieraj� si� ";
            if(CO_PADA(this_object()) == PADA_DESZCZ)
            {
                str += "szare w strugach deszczu ";
            }
            else
            {
                str += "ciemne ";
            }
            str += "��ki. ";
        }
        else
        {
            str = "Szeroki, wygodny trakt wy�o�ony dopasowanymi kamieniami "+
            "biegnie od bram Rinde, w kierunku po�udniowo-zachodnim, "+
            "skrajem wzg�rza �agodnie opadaj�cego w stron� Pontaru. �nie�ne "+
            "zaspy wznosz�ce si� po obu stronach drogi os�aniaj� nieco "+
            "przed wiej�cym wiatrem ale nie zach�caj� do odpoczynku. "+
            "Widoczne na skraju �agodnego wzg�rza na zachodzie podobne "+
            "zaspy ci�gn� si� wzd�u� le��cego tam traktu do Bia�ego Mostu, "+
            "a pomi�dzy wzg�rzami ograniczone od wschodu i zachodu stromymi "+
            "skarpami rozci�ga si� bagniste rozlewisko Pontaru pokryte "+
            "cienk� warstewk� kruchego lodu w kt�rym tkwi� uwi�zione tu i "+
            "�wdzie rachityczne drzewka. Na zachodzie a� po majacz�c� w "+
            "ciemno�ci ciemn� kres� lasu, rozpo�ciera si� bia�a, l�ni�ca w "+
            "blasku ksi�yca, pokryta �niegiem r�wnina ";
        }
    }
    str += "\n";
    return str;
}
string
opis_nocy()
{
    string str;
    if(pora_roku() != MT_ZIMA)
    {
        str = "Szeroki, wygodny trakt wy�o�ony dopasowanymi kamieniami "+
        "biegnie od bram Rinde, w kierunku po�udniowo-zachodnim, skrajem "+
        "wzg�rza �agodnie opadaj�cego w stron� Pontaru. W stertach "+
        "pokruszonych kamieni wznosz�cych si� po obu stronach drogi z "+
        "trudem mo�na rozpozna� pozosta�o�ci po niewysokich kamiennych "+
        "murkach zbudowanych niegdy� z bry� piaskowca a obecnie "+
        "zniszczonych przez wiatry i deszcze, poro�ni�tych przez ma�e "+
        "krzaczki i ruderalne zielsko a i nie oszcz�dzanych przez "+
        "w�drowc�w przysiadaj�cych tutaj w oczekiwaniu na otwarcie bram "+
        "miasta. Widoczne na skraju �agodnego wzg�rza na zachodzie "+
        "podobne kamieniste pozosta�o�ci ci�gn� si� wzd�u� le��cego "+
        "tam traktu do Bia�ego Mostu a pomi�dzy wzg�rzami ograniczone "+
        "od wschodu i zachodu stromymi skarpami rozci�ga si� bagniste "+
        "rozlewisko Pontaru poro�ni�te trawami i nielicznymi "+
        "rachitycznymi drzewkami. Na zachodzie a� po majacz�c� w "+
        "ciemno�ci ciemn� kres� lasu, rozpo�cieraj� si� ";
        if(CO_PADA(this_object()) == PADA_DESZCZ)
        {
            str += "szare w strugach deszczu ";
        }
        else
        {
            str += "ciemne ";
        }
        str += "��ki. ";
     }
     else
     {
        str = "Szeroki, wygodny trakt wy�o�ony dopasowanymi kamieniami "+
        "biegnie od bram Rinde, w kierunku po�udniowo-zachodnim, "+
        "skrajem wzg�rza �agodnie opadaj�cego w stron� Pontaru. �nie�ne "+
        "zaspy wznosz�ce si� po obu stronach drogi os�aniaj� nieco "+
        "przed wiej�cym wiatrem ale nie zach�caj� do odpoczynku. "+
        "Widoczne na skraju �agodnego wzg�rza na zachodzie podobne "+
        "zaspy ci�gn� si� wzd�u� le��cego tam traktu do Bia�ego Mostu, "+
        "a pomi�dzy wzg�rzami ograniczone od wschodu i zachodu stromymi "+
        "skarpami rozci�ga si� bagniste rozlewisko Pontaru pokryte "+
        "cienk� warstewk� kruchego lodu w kt�rym tkwi� uwi�zione tu i "+
        "�wdzie rachityczne drzewka. Na zachodzie a� po majacz�c� w "+
        "ciemno�ci ciemn� kres� lasu, rozpo�ciera si� bia�a, l�ni�ca w "+
        "blasku ksi�yca, pokryta �niegiem r�wnina ";
     }
     str +="\n";
     return str;
}