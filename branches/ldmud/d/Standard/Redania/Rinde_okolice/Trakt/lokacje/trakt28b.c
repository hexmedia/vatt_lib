/* Autor: Faeve
   Opis : Faeve
   Data : 13.1.2007 */
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"

inherit TRAKT_RINDE_STD;

void create_trakt() 
{
    set_short("�cie�ka mi�dzy drzewami.");
    add_exit(TRAKT_RINDE_LOKACJE + "trakt28.c","s",0,TRAKT_RO_FATIG,0);
    add_exit(DWOREK_LOKACJE + "ogrod","nw",0,TRAKT_RO_FATIG,0);
set_noshow_obvious(1);

    add_prop(ROOM_I_INSIDE,0);
add_item("drzewa","Jest ich tu ca�e mn�stwo, a rosn� tak g�sto, �e trudno przebi� si� "+
"mi�dzy ga��ziami nie wyd�ubuj�c sobie przy okazji oka.\n");
add_item(({"mech","�ci�k�","igliwie"}),"@@sciolka@@");
}

string
dlugi_opis()
{
    string str;

    if(jest_dzien())
    {
        if(pora_roku() != MT_ZIMA)
        {
    str = "Las w tym miejscu jest wyj�tkowo trudny do przej�cia - t�oczy si� "+
"tu ca�a masa ja�owc�w i jode� na przemian z d�bami i leszczynami. "+
"Stanowiacy �ci�k�, soczy�cie zielony mech oraz igliwie doskonale wyciszaj� "+
"odg�os krok�w, dzi�ki czemu miejsce wydaje si� by� jeszcze bardziej "+
"tajemnicze. W dali, na p�nocnym zachodzie las si� przerzedza, a mi�dzy "+
"drzewami od czasu do czasu wida� co�, jakby b�yski promieni s�onecznych, "+
"odbijaj�cych si� od dach�wek jakiego� domostwa. Nie by�oby w tym nic "+
"dziwnego, gdyby nie fakt, �e jest to praktycznie �rodek lasu. Nie wida� tu "+
"�adnych �lad�w bytno�ci zwierz�t, a tym bardziej ludzi. Nie s�ycha� nawet "+
"�wiergotu ptak�w, zazwyczaj wszechobecnych. Tylko gdzie�, mi�dzy drzewami, "+
"w delikatnych promieniach s�onecznych pob�yskuje misternie wysnuta, ogromna "+
"sie� paj�cza.";
        }
        else
        {
    str = "Las w tym miejscu jest wyjatkowo trudny do przej�cia - t�oczy si� "+
"tu ca�a masa ja�owc�w i jode� na przemian z d�bami i leszczynami. W dodatku "+
"wszystkie pokryte s� �niegiem, co jeszcze bardziej utrudnia w�dr�wk�. Na "+
"p�nocnym zachodzie las si� jakby nieco przerzedza i, co dziwne - wygl�da "+
"jakby by� tam jaki� budynek - w dali jaka� nik�a czerwie�, odbijaj�ca od "+
"czasu do czasu promienie s�oneczne, kontrastuje z nieskaziteln� biel� "+
"�niegu. Nie by�oby w tym nic dziwnego, gdyby nie fakt, �e jest to "+
"praktycznie �rodek lasu. Nie wida� tu �adnych �lad�w bytno�ci zwierz�t, a "+
"tym bardziej ludzi. Nie s�ycha� nawet �wiergotu ptak�w, zazwyczaj "+
"wszechobecnych. ";
        }
    }
    else
    {
        if(pora_roku() != MT_ZIMA)
        {
    str = "Las w tym miejscu jest wyj�tkowo trudny do przej�cia - t�oczy si� "+
"tu ca�a masa ja�owc�w i jode� na przemian z d�bami i leszczynami. A co "+
"gorsza - prawie nic nie wida�! Niemal po omacku mo�na si� jedynie kierowa�, "+
"id�c wzd�u� niewielkiego parowu, kt�ry zdaje si� by� �cie�k�. Jest tu "+
"niesamowicie cicho. Nie by�oby to dziwne, gdyby nie fakt, �e jest to "+
"praktycznie �rodek lasu. Nie wida� tu �adnych �lad�w bytno�ci zwierz�t, a "+
"tym bardziej ludzi. Nie s�ycha� nawet odg�os�w ptak�w nocnych czy "+
"nietoperzy, zazwyczaj wszechobecnych. ";
        }
        else
        {
    str = "Las w tym miejscu jest wyj�tkowo trudny do przej�cia - t�oczy si� "+
"tu ca�a masa ja�owc�w i jode�, na przemian z d�bami i leszczynami. Nie do��, "+
"�e jest ciemno, to wszystkie drzewa pokryte s� grub� pierzyn� �niegu, co "+
"jeszcze bardziej utrudnia w�dr�wk�. Niemal po omacku, mo�na si� jedynie "+
"kierowa�, id�c wzd�u� niewielkiego parowu, kt�ry zdaje si� by� scie�k� - "+
"teraz pokrytego �niegiem i prawie niewyczuwalnego. Panuje tu "+
"niewypowiedziana cisza. Nie by�oby to dziwne, gdyby nie fakt, �e jest to "+
"praktycznie �rodek lasu. Nie wida� tu �adnych �lad�w bytno�ci zwierz�t, a "+
"tym bardziej ludzi. Nie s�ycha� nawet odg�os�w ptak�w nocnych, zazwyczaj "+
"wszechobecnych.";
        }
    }
    str += "\n";

    return str;
}

string
opis_nocy()
{
    string str;
    if(pora_roku() != MT_ZIMA)
    {
        str = "Las w tym miejscu jest wyj�tkowo trudny do przej�cia - t�oczy "+
"si� tu ca�a masa ja�owc�w i jode� na przemian z d�bami i leszczynami. A co "+
"gorsza - prawie nic nie wida�! Niemal po omacku mo�na si� jedynie kierowa�, "+
"id�c wzd�u� niewielkiego parowu, kt�ry zdaje si� by� �cie�k�. Jest tu "+
"niesamowicie cicho. Nie by�oby to dziwne, gdyby nie fakt, �e jest to "+
"praktycznie �rodek lasu. Nie wida� tu �adnych �lad�w bytno�ci zwierz�t, a "+
"tym bardziej ludzi. Nie s�ycha� nawet odg�os�w ptak�w nocnych, zazwyczaj "+
"wszechobecnych. ";
    }
    else
    {
        str = "Las w tym miejscu jest wyj�tkowo trudny do przej�cia - t�oczy "+
"si� tu ca�a masa ja�owc�w i jode�, na przemian z d�bami i leszczynami. Nie "+
"do��, �e jest ciemno, to wszystkie drzewa pokryte s� grub� pierzyn� �niegu, "+
"co jeszcze bardziej utrudnia w�dr�wk�. Niemal po omacku, mo�na si� jedynie "+
"kierowa�, id�c wzd�u� niewielkiego parowu, kt�ry zdaje si� by� scie�k� - "+
"teraz pokrytego �niegiem i prawie niewyczuwalnego. Panuje tu "+
"niewypowiedziana cisza. Nie by�oby to dziwne, gdyby nie fakt, �e jest to "+
"praktycznie �rodek lasu. Nie wida� tu �adnych �lad�w bytno�ci zwierz�t, a "+
"tym bardziej ludzi. Nie s�ycha� nawet odg�os�w ptak�w nocnych, zazwyczaj "+
"wszechobecnych.";
    }
    str += "\n";
    return str;
}



string
sciolka()
{
string str;
if(jest_dzien() == 1)
{
   if(pora_roku() != MT_ZIMA)
   {
       str = "Soczy�cie zielony mech, przysypany jest gdzieniegdzie igliwiem. "+
"Stanowi siedlisko wszelkiego robactwa, dlatego chyba nie warto si� w niego "+
"zag��bia�.\n";
   }
   else
   {
       str = "Teraz wszystko pokryte jest �nie�nobia�� pierzyn�. Jedynie "+
"miejscami mo�na dojrze� jakie� szyszki, ig�y czy inne fragmenty �ci�ki.\n";
   }
}
else
{
   str = "Jest tak ciemno, �e nic nie mo�esz tutaj dostrzec.\n";
}

return str;
}

