/* Autor: Avard
   Opis : Yran
   Data : 10.01.07 */
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"

inherit TRAKT_RINDE_STD;
inherit "/lib/drink_water";

void create_trakt() 
{
    set_short("Trakt");
    add_exit(TRAKT_RINDE_LOKACJE + "trakt19.c","e",0,TRAKT_RO_FATIG,0);
    add_exit(TRAKT_RINDE_LOKACJE + "trakt21.c","w",0,TRAKT_RO_FATIG,0);
    add_exit(LAS_RINDE_LOKACJE + "las56.c","ne",0,TRAKT_RO_FATIG,0);
    add_exit(LAS_RINDE_LOKACJE + "las57.c","nw",0,TRAKT_RO_FATIG,0);
    add_exit(LAS_RINDE_LOKACJE + "las71.c","sw",0,TRAKT_RO_FATIG,0);

    set_drink_places(({"z rzeki","z pontaru","z Pontaru"}));
    add_sit(({"nad rzek^a","nad pontarem","nad Pontarem"}),
        "nad rzek^a","z nad rzeki",0);

    add_prop(ROOM_I_INSIDE,0);
    add_item(({"Pontar","pontar","rzek^e"}),"P^lyn^acy tu od tysi^ecy lat "+
    "szeroki Pontar zdaje si^e by^c wci^a^z nieujarzmionym i niezdobytym, "+
    "wyznacza nie tylko granice mi^edzy kr^olestwami Redanii i Temeri, "+
    "ale i skutecznie ogranicza ludzkie zap^edy w wyrywaniu z r^ak "+
    "matki Natury wszystkiego, co sie jej nale^zy. Gdzieniegdzie "+
    "pojawiaj^ace si^e z nienacka niewielkie zawirowania wody na "+
    "spokojnie, jakby leniwie poruszaj^acej si^e m^etnej tafli swiadcz^a "+
    "o nieprzywidywalno^sci i zdradzieckiej naturze nurtu, kt^ory zapewne "+
    "pochlon^a^l niejedno istnienie bezmy^slnie probuj^ace zmierzy^c si^e z "+
    "pot^eg^a rzeki.\n");
}
public string
exits_description() 
{
    return "Trakt prowadzi na zach^od i wsch^od.\n";
}
void
init()
{
    ::init();
    init_drink_water(); 
} 

string
dlugi_opis()
{
    string str;
    if(jest_dzien() == 1)
    {
        if(pora_roku() != MT_ZIMA)
        {
            str = "Na po^lnocy rozpo^sciera si^e sciana rozleg^lego "+
                "li^sciastego lasu, kt^orego czubki wysokich drzew "+
                "p^lynnie ^l^acz^a sie z linia nieba. Z rosn^acego zaraz "+
                "przy trakcie niewielkiego zagajnika do twych uszu dochodzi "+
                "dono^sne krakanie wron, za kt^orych przyk^ladem posz^ly "+
                "inne zwierz^eta tworz^ac d^xwi^eczn^a melodie. Poludniow^a "+
                "lini^e horyzontu kszta^ltuje rzeka Pontar. Jej leniwie "+
                "p^lyn^ace wody z hukiem rozbijaj^a si^e o wystaj^ace ponad "+
                "powierzchni^e kamienie. Boki szerokiego, ubitego traktu "+
                "porastaj^a niskie krzaczki i k^epy traw.";
        }
        else
        {
            str = "Na po^lnocy rozpo^sciera si^e sciana rozleg^lego lasu, "+
                "kt^orego nagie, bezlistne czubki wysokich drzewa p^lynnie "+
                "^l^acz^a sie z linia nieba. Z rosn^acego zaraz przy "+
                "trakcie niewielkiego zagajnika do twych uszu dochodzi "+
                "dono^sne krakanie wron, za kt^orych przyk^ladem posz^ly "+
                "inne zwierz^eta tworz^ac d^xwi^eczn^a melodie. Poludniow^a "+
                "lini^e horyzontu kszta^ltuje rzeka Pontar. Jej leniwie "+
                "p^lyn^ace wody z hukiem rozbijaj^a si^e o wystaj^ace ponad "+
                "powierzchni^e kamienie, a dryfuj^ace r^o^znej wielko^sci "+
                "kawa^lki kry bezw^ladnie obij^a si^e o brzeg. Trakt "+
                "pokryty jest ubit^a warstw^a ^sniegu z licznymi ^sladami "+
                "zwierz^at, kt^ore podczas zimowej zawieruchu zb^l^adzi^ly "+
                "opuszczaj^a^c swoje naturalne, le^sne ^srodowisko.";
        }
    }
    if(jest_dzien() == 0)
    {
        if(pora_roku() != MT_ZIMA)
        {
            str = "Na po^lnocy rozpo^sciera si^e sciana rozleg^lego "+
                "li^sciastego lasu, kt^orego czubki wysokich drzewa "+
                "p^lynnie ^l^acz^a sie z linia nieba. Z rosn^acego zaraz "+
                "przy trakcie niewielkiego zagajnika do twych uszu dochodzi "+
                "pochukiwanie s^ow tworz^ace d^xwi^eczn^a melodie. "+
                "Poludniow^a lini^e horyzontu kszta^ltuje rzeka Pontar. Jej "+
                "leniwie p^lyn^ace wody z hukiem rozbijaj^a si^e o "+
                "wystaj^ace ponad powierzchni^e kamienie. Boki szerokiego, "+
                "ubitego traktu porastaj^a niskie krzaczki i k^epy traw.";
        }
        else
        {
            str = "Na po^lnocy rozpo^sciera si^e sciana rozleg^lego lasu, "+
                "kt^orego nagie, bezlistne czubki wysokich drzewa p^lynnie "+
                "^l^acz^a sie z linia nieba. Z rosn^acego zaraz przy "+
                "trakcie niewielkiego zagajnika do twych uszu dochodzi "+
                "pochukiwanie s^ow tworz^ace d^xwi^eczn^a melodie. "+
                "Poludniow^a lini^e horyzontu kszta^ltuje rzeka Pontar. "+
                "Jej leniwie p^lyn^ace wody z hukiem rozbijaj^a si^e o "+
                "wystaj^ace ponad powierzchni^e kamienie, a dryfuj^ace "+
                "r^o^znej wielko^sci kawa^lki kry bezw^ladnie obij^a "+
                "si^e o brzeg. Trakt pokryty jest ubit^a warstw^a "+
                "^sniegu z licznymi ^sladami zwierz^at, kt^ore podczas "+
                "zimowej zawieruchu zb^l^adzi^ly opuszczaj^a^c swoje "+
                "naturalne, le^sne ^srodowisko.";
        }
    }
    str += "\n";
    return str;
}
string
opis_nocy()
{
    string str;
    if(pora_roku() != MT_ZIMA)
        {
            str = "Na po^lnocy rozpo^sciera si^e sciana rozleg^lego "+
                "li^sciastego lasu, kt^orego czubki wysokich drzewa "+
                "p^lynnie ^l^acz^a sie z linia nieba. Z rosn^acego zaraz "+
                "przy trakcie niewielkiego zagajnika do twych uszu dochodzi "+
                "pochukiwanie s^ow tworz^ace d^xwi^eczn^a melodie. "+
                "Poludniow^a lini^e horyzontu kszta^ltuje rzeka Pontar. Jej "+
                "leniwie p^lyn^ace wody z hukiem rozbijaj^a si^e o "+
                "wystaj^ace ponad powierzchni^e kamienie. Boki szerokiego, "+
                "ubitego traktu porastaj^a niskie krzaczki i k^epy traw.";
        }
        else
        {
            str = "Na po^lnocy rozpo^sciera si^e sciana rozleg^lego lasu, "+
                "kt^orego nagie, bezlistne czubki wysokich drzewa p^lynnie "+
                "^l^acz^a sie z linia nieba. Z rosn^acego zaraz przy "+
                "trakcie niewielkiego zagajnika do twych uszu dochodzi "+
                "pochukiwanie s^ow tworz^ace d^xwi^eczn^a melodie. "+
                "Poludniow^a lini^e horyzontu kszta^ltuje rzeka Pontar. "+
                "Jej leniwie p^lyn^ace wody z hukiem rozbijaj^a si^e o "+
                "wystaj^ace ponad powierzchni^e kamienie, a dryfuj^ace "+
                "r^o^znej wielko^sci kawa^lki kry bezw^ladnie obij^a "+
                "si^e o brzeg. Trakt pokryty jest ubit^a warstw^a "+
                "^sniegu z licznymi ^sladami zwierz^at, kt^ore podczas "+
                "zimowej zawieruchu zb^l^adzi^ly opuszczaj^a^c swoje "+
                "naturalne, le^sne ^srodowisko.";
        }
    str += "\n";
    return str;
}