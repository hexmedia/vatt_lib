/* Autor: Avard
   Opis : Brzozek
   Data : 11.11.06*/
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"
#include <pogoda.h>

inherit TRAKT_RINDE_STD;
inherit "/lib/drink_water";

void create_trakt() 
{
    set_short("Trakt");
    add_exit(TRAKT_RINDE_LOKACJE + "trakt11.c","e",0,TRAKT_RO_FATIG,0);
    add_exit(TRAKT_RINDE_LOKACJE + "trakt13.c","s",0,TRAKT_RO_FATIG,0);
    add_exit(LAKA_RINDE_LOKACJE + "laka54.c","w",0,TRAKT_RO_FATIG,0);
    add_exit(LAKA_RINDE_LOKACJE + "laka49.c","nw",0,TRAKT_RO_FATIG,0);
    add_exit(LAKA_RINDE_LOKACJE + "laka48.c","n",0,TRAKT_RO_FATIG,0);
    add_exit(LAKA_RINDE_LOKACJE + "laka47.c","ne",0,TRAKT_RO_FATIG,0);

    add_prop(ROOM_I_INSIDE,0);
    set_drink_places(({"z rzeki","z pontaru","z Pontaru"}));
    add_sit(({"nad rzek^a","nad pontarem","nad Pontarem"}),
        "nad rzek^a","z nad rzeki",0);
    add_item(({"Pontar","pontar","rzek^e"}),"P^lyn^acy tu od tysi^ecy lat "+
    "szeroki Pontar zdaje si^e by^c wci^a^z nieujarzmionym i niezdobytym, "+
    "wyznacza nie tylko granice mi^edzy kr^olestwami Redanii i Temeri, "+
    "ale i skutecznie ogranicza ludzkie zap^edy w wyrywaniu z r^ak "+
    "matki Natury wszystkiego, co sie jej nale^zy. Gdzieniegdzie "+
    "pojawiaj^ace si^e z nienacka niewielkie zawirowania wody na "+
    "spokojnie, jakby leniwie poruszaj^acej si^e m^etnej tafli swiadcz^a "+
    "o nieprzywidywalno^sci i zdradzieckiej naturze nurtu, kt^ory zapewne "+
    "pochlon^a^l niejedno istnienie bezmy^slnie probuj^ace zmierzy^c si^e z "+
    "pot^eg^a rzeki.\n");
}
public string
exits_description() 
{
    return "Trakt prowadzi na po^ludnie i wsch^od.\n";
}
void
init()
{
    ::init();
    init_drink_water(); 
} 

string
dlugi_opis()
{
    string str;
    if(jest_dzien() == 1)
    {
    if(pora_roku() != MT_ZIMA)
    {
        str = "Biegn�cy skrajem niewysokiej, piaszczystej skarpy trakt, "+
        "wystarczaj�co szeroki, aby bez trudu min�y si� na nim dwa "+
        "wy�adowane kupieckie wozy skr�ca tu na zach�d oddalaj�c si� "+
        "od rzeki. Wy�o�on� dopasowanymi kamieniami nier�wn� nawierzchni� "+
        "drogi chroni� nieco od wiatru i deszczu grube drzewa rosn�ce po "+
        "jej p�nocnozachodniej stronie. Brz�czenie owad�w i szczebiot "+
        "ptak�w zamieszkuj�cych spl�tany g�szcz ga��zi i g�ste kolczaste "+
        "zaro�la, odgradzaj�ce dukt od bezkresnych r�wnin ci�gn�cych si� "+
        "a� po p�nocny horyzont, ";
        if(this_player()->query_headache() > 1)
        {
            str += "mo�e przyprawi� o b�l g�owy. ";
        }
        if(this_player()->query_intoxicated() > 1)
        {
            str += "nape�nia powietrze irytuj�c� kakofoni� d�wi�k�w. ";
        }
        else
        {
            str += "nape�nia powietrze mi�ym dla ucha brz�czeniem. ";
        }
        str += "Korzenie grubych drzew rosn�cych kiedy� wzd�u� drogi od "+
        "strony Pontaru zosta�y podmyte przez wzburzone wody a porywiste "+
        "wiatry doko�czy�y dzie�a obalaj�c pot�ne pnie zalegaj�ce teraz "+
        "na niewysokiej skarpie opadaj�cej stromo w kierunku wody. "+
        "Po�amane ga��zie tych drzew si�gaj� a� do ";
        if(CO_PADA(this_object()) == PADA_DESZCZ)
        {
            str += "wzburzonych w�d rzeki. ";
        }
        if(MOC_OPADOW(this_object()) == NIC_NIE_PADA)
        {
            str += "leniwie p�yn�cych b��kitnych w�d rzeki. ";
        } 
        str += "Brunatne b�ocko wype�niaj�ce olbrzymi� dziur� na �rodku "+
        "traktu, utworzon� przez korzenie upadaj�cego drzewa, pokrywa "+
        "prawie wszystko dooko�a - kamienie, kt�rymi wy�o�ona jest "+
        "nawierzchnia, pobocza wraz z zalegaj�cymi na nich �mieciami "+
        "pozostawionymi przez odpoczywaj�cych w�drowc�w, cz�ciowo pnie "+
        "drzew a nawet olbrzymie korzenie.";
    }
    else
    {
        str = "Biegn�cy skrajem niewysokiej skarpy przypominaj�cej o tej "+
        "porze roku zasp� �nie�n� trakt wystarczaj�co szeroki, aby bez "+
        "trudu min�y si� na nim dwa wy�adowane kupieckie wozy skr�ca tu na "+
        "zach�d oddalaj�c si� od rzeki. Wy�o�on� dopasowanymi kamieniami "+
        "nier�wn� nawierzchni� drogi chroni� nieco od wiatru i �niegu grube "+
        "drzewa rosn�ce po jej p�nocnozachodniej stronie a spoza "+
        "porastaj�cych przestrze� pomi�dzy pniami kolczastych zaro�li "+
        "prze�wituje miejscami o�lepiaj�co bia�a r�wnina ci�gn�ca si� a� po "+
        "p�nocny horyzont. Ochryp�e krakanie wron m�ci od czasu do czasu "+
        "panuj�c� tutaj mi�� cisz�. Pokryte �niegiem i lodem olbrzymie pnie "+
        "drzew le��ce na skarpie nad Pontarem zanurzaj� swoje ga��zie w "+
        "lodowatych wodach rzeki. Zamarzni�ta brunatna bryja w niczym ju� "+
        "nieprzypominaj�ca �niegu pokrywa ca�� nawierzchni� drogi i oba "+
        "pobocza skutecznie utrudniaj�c w�dr�wk� i zniech�caj�c do "+
        "odpoczynku.";
    }
    }

    if(jest_dzien() == 0)
    {
    if(pora_roku() != MT_ZIMA)
    {
        str = "Biegn�cy skrajem niewysokiej, piaszczystej skarpy trakt, "+
        "wystarczaj�co szeroki, aby bez trudu min�y si� na nim dwa "+
        "wy�adowane kupieckie wozy skr�ca tu na zach�d oddalaj�c si� od "+
        "rzeki. Wy�o�on� dopasowanymi kamieniami nier�wn� nawierzchni� "+
        "drogi chroni� nieco od wiatru i deszczu grube drzewa rosn�ce po "+
        "jej p�nocnozachodniej stronie. Brz�czenie owad�w i pohukiwanie "+
        "s�w zamieszkuj�cych spl�tany g�szcz ga��zi i g�ste kolczaste "+
        "zaro�la odgradzaj�ce dukt od bezkresnych r�wnin ci�gn�cych si� a� "+
        "po p�nocny horyzont, ";
        if(this_player()->query_headache() > 1)
        {
            str += "mo�e przyprawi� o b�l g�owy. ";
        }
        if(this_player()->query_intoxicated() > 1)
        {
            str += "nape�nia powietrze irytuj�c� kakofoni� d�wi�k�w. ";
        }
        else
        {
            str += "nape�nia powietrze mi�ym dla ucha brz�czeniem. ";
        }
        str += "Korzenie grubych drzew rosn�cych kiedy� wzd�u� drogi od "+
        "strony Pontaru zosta�y podmyte przez wzburzone wody a porywiste "+
        "wiatry doko�czy�y dzie�a obalaj�c pot�ne pnie zalegaj�ce teraz na "+
        "niewysokiej skarpie opadaj�cej stromo w kierunku wody. Po�amane "+
        "ga��zie tych drzew si�gaj� a� do ";
        if(CO_PADA(this_object()) == PADA_DESZCZ)
        {
            str += "wzburzonych w�d rzeki. ";
        }
        if(MOC_OPADOW(this_object()) == NIC_NIE_PADA)
        {
            str += "leniwie p�yn�cych b��kitnych w�d rzeki. ";
        } 
        str += "Brunatne b�ocko wype�niaj�ce olbrzymi� dziur� na �rodku "+
        "traktu utworzon� przez korzenie upadaj�cego drzewa pokrywa prawie "+
        "wszystko dooko�a - kamienie, kt�rymi wy�o�ona jest nawierzchnia, "+
        "pobocza wraz z zalegaj�cymi na nich �mieciami pozostawionymi przez "+
        "odpoczywaj�cych w�drowc�w, cz�ciowo pnie drzew a nawet olbrzymie "+
        "korzenie przewr�conych lip.";
    }
    else
    {
        str = "Biegn�cy skrajem niewysokiej skarpy przypominaj�cej o tej "+
        "porze roku zasp� �nie�n� trakt wystarczaj�co szeroki, aby bez "+
        "trudu min�y si� na nim dwa wy�adowane kupieckie wozy skr�ca tu "+
        "na zach�d oddalaj�c si� od rzeki. Wy�o�on� dopasowanymi "+
        "kamieniami nier�wn� nawierzchni� drogi chroni� nieco od wiatru i "+
        "�niegu grube drzewa rosn�ce po jej p�nocnozachodniej stronie, a "+
        "spoza porastaj�cych przestrze� pomi�dzy pniami kolczastych zaro�li "+
        "prze�wituje miejscami srebrzy�cie po�yskuj�ca w blasku ksi�yca "+
        "r�wnina ci�gn�ca si� a� po p�nocny horyzont. Ochryp�e "+
        "pohukiwanie s�w m�ci od czasu do czasu panuj�cy tutaj koj�cy "+
        "spok�j. Pokryte �niegiem i lodem olbrzymie pnie drzew le��ce na "+
        "skarpie nad Pontarem zanurzaj� swoje ga��zie w lodowatych wodach "+
        "rzeki. Zamarzni�ta brunatna bryja w niczym ju� nieprzypominaj�ca "+
        "�niegu pokrywa ca�� nawierzchni� drogi i oba pobocza skutecznie "+
        "utrudniaj�c w�dr�wk� i zniech�caj�c do odpoczynku.";
    }
    }
    str += "\n";
    return str;
}

string
opis_nocy()
{
    string str;
    if(pora_roku() != MT_ZIMA)
    {
        str = "Biegn�cy skrajem niewysokiej, piaszczystej skarpy trakt, "+
        "wystarczaj�co szeroki, aby bez trudu min�y si� na nim dwa "+
        "wy�adowane kupieckie wozy skr�ca tu na zach�d oddalaj�c si� od "+
        "rzeki. Wy�o�on� dopasowanymi kamieniami nier�wn� nawierzchni� "+
        "drogi chroni� nieco od wiatru i deszczu grube drzewa rosn�ce po "+
        "jej p�nocnozachodniej stronie. Brz�czenie owad�w i pohukiwanie "+
        "s�w zamieszkuj�cych spl�tany g�szcz ga��zi i g�ste kolczaste "+
        "zaro�la odgradzaj�ce dukt od bezkresnych r�wnin ci�gn�cych si� a� "+
        "po p�nocny horyzont, ";
        if(this_player()->query_headache() > 1)
        {
            str += "mo�e przyprawi� o b�l g�owy. ";
        }
        if(this_player()->query_intoxicated() > 1)
        {
            str += "nape�nia powietrze irytuj�c� kakofoni� d�wi�k�w. ";
        }
        else
        {
            str += "nape�nia powietrze mi�ym dla ucha brz�czeniem. ";
        }
        str += "Korzenie grubych drzew rosn�cych kiedy� wzd�u� drogi od "+
        "strony Pontaru zosta�y podmyte przez wzburzone wody a porywiste "+
        "wiatry doko�czy�y dzie�a obalaj�c pot�ne pnie zalegaj�ce teraz na "+
        "niewysokiej skarpie opadaj�cej stromo w kierunku wody. Po�amane "+
        "ga��zie tych drzew si�gaj� a� do ";
        if(CO_PADA(this_object()) == PADA_DESZCZ)
        {
            str += "wzburzonych w�d rzeki. ";
        }
        if(MOC_OPADOW(this_object()) == NIC_NIE_PADA)
        {
            str += "leniwie p�yn�cych b��kitnych w�d rzeki. ";
        } 
        str += "Brunatne b�ocko wype�niaj�ce olbrzymi� dziur� na �rodku "+
        "traktu utworzon� przez korzenie upadaj�cego drzewa pokrywa prawie "+
        "wszystko dooko�a - kamienie, kt�rymi wy�o�ona jest nawierzchnia, "+
        "pobocza wraz z zalegaj�cymi na nich �mieciami pozostawionymi przez "+
        "odpoczywaj�cych w�drowc�w, cz�ciowo pnie drzew a nawet olbrzymie "+
        "korzenie przewr�conych lip.";
    }
    else
    {
        str = "Biegn�cy skrajem niewysokiej skarpy przypominaj�cej o tej "+
        "porze roku zasp� �nie�n� trakt wystarczaj�co szeroki, aby bez "+
        "trudu min�y si� na nim dwa wy�adowane kupieckie wozy skr�ca tu "+
        "na zach�d oddalaj�c si� od rzeki. Wy�o�on� dopasowanymi "+
        "kamieniami nier�wn� nawierzchni� drogi chroni� nieco od wiatru i "+
        "�niegu grube drzewa rosn�ce po jej p�nocnozachodniej stronie, a "+
        "spoza porastaj�cych przestrze� pomi�dzy pniami kolczastych zaro�li "+
        "prze�wituje miejscami srebrzy�cie po�yskuj�ca w blasku ksi�yca "+
        "r�wnina ci�gn�ca si� a� po p�nocny horyzont. Ochryp�e "+
        "pohukiwanie s�w m�ci od czasu do czasu panuj�cy tutaj koj�cy "+
        "spok�j. Pokryte �niegiem i lodem olbrzymie pnie drzew le��ce na "+
        "skarpie nad Pontarem zanurzaj� swoje ga��zie w lodowatych wodach "+
        "rzeki. Zamarzni�ta brunatna bryja w niczym ju� nieprzypominaj�ca "+
        "�niegu pokrywa ca�� nawierzchni� drogi i oba pobocza skutecznie "+
        "utrudniaj�c w�dr�wk� i zniech�caj�c do odpoczynku.";
    }
    str +="\n";
    return str;
}