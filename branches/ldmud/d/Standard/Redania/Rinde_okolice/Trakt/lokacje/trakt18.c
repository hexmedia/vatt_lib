/* Autor: Avard
   Opis : vortak
   Data : 10.10.06*/
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"

inherit TRAKT_RINDE_STD;
inherit "/lib/drink_water";


void create_trakt() 
{
    set_short("Trakt nad rzek^a.");
    add_exit(TRAKT_RINDE_LOKACJE + "trakt19.c","sw",0,TRAKT_RO_FATIG,0);
    add_exit(LAS_RINDE_LOKACJE + "las45.c","n",0,TRAKT_RO_FATIG,0);
    add_exit(LAS_RINDE_LOKACJE + "las56.c","w",0,TRAKT_RO_FATIG,0);
    add_exit(TRAKT_RINDE_LOKACJE + "trakt17.c","ne",0,TRAKT_RO_FATIG,0);

    add_prop(ROOM_I_INSIDE,0);
    add_event("@@pora_rok:"+file_name(TO)+"@@");
    add_item(({"^scie^zk^e","le^sn^a ^scie^zk^e",
        "wydeptan^a ^scie^zk^e"}),"@@sciezka@@");
    add_item(({"rozlewisko","rozlewisko Pontaru",
        "rozlewisko pontaru","rozlewisko na po^ludniu"}),"@@rozlewisko@@");
    add_sit(({"nad rzek^a","nad pontarem","nad Pontarem"}),
        "nad rzek^a","z nad rzeki",0);
    add_item(({"Pontar","pontar","rzek^e"}),"P^lyn^acy tu od tysi^ecy lat "+
    "szeroki Pontar zdaje si^e by^c wci^a^z nieujarzmionym i niezdobytym, "+
    "wyznacza nie tylko granice mi^edzy kr^olestwami Redanii i Temeri, "+
    "ale i skutecznie ogranicza ludzkie zap^edy w wyrywaniu z r^ak "+
    "matki Natury wszystkiego, co sie jej nale^zy. Gdzieniegdzie "+
    "pojawiaj^ace si^e z nienacka niewielkie zawirowania wody na "+
    "spokojnie, jakby leniwie poruszaj^acej si^e m^etnej tafli swiadcz^a "+
    "o nieprzywidywalno^sci i zdradzieckiej naturze nurtu, kt^ory zapewne "+
    "pochlon^a^l niejedno istnienie bezmy^slnie probuj^ace zmierzy^c si^e z "+
    "pot^eg^a rzeki.\n");
    set_drink_places(({"z rzeki","z pontaru","z Pontaru"}));


}
void
init()
{
    ::init();
    init_drink_water(); 
} 


public string
exits_description() 
{
    if(jest_dzien() == 1)
    {
        return "Trakt prowadzi na p^o^lnocny-wsch^od i po^ludniowy-zach^od, "+
            "za^s na p^o^lnocy wida^c le^sn^a ^scie^zk^e.\n";
    }
    if(jest_dzien() == 0)
    {
        return "Trakt prowadzi na p^o^lnocny-wsch^od i po^ludniowy-zach^od.\n";
    }
}

string
dlugi_opis()
{
    string str;
    if(jest_dzien() == 1)
    {
    if(pora_roku() != MT_ZIMA)
    {
        str = "Trakt jest szeroki, wygodny i wy^lo^zony g^ladkimi "+
            "kamieniami. Tak przynajmniej by^lo kiedy^s - teraz jedynie "+
            "mo^zna powiedzie^c, ^ze jest szeroki. Dwa kupieckie wozy "+
            "na pewno si^e wymin^a, no chyba ^ze wo^xnica nie da rady "+
            "wyjecha^c z g^l^ebokich, wy^z^lobionych w twardym kamieniu "+
            "przez wiele lat kolein. Po niekt^orych kamieniach pozosta^lo "+
            "jedynie wspomnienie - teraz wyrwy po nich wype^lnione s^a "+
            "g^estym, ciemnym b^lotem. Podr^o^z podskakuj^acym na takich "+
            "wertepach wozem na pewno do wygodnych nie nale^zy, a i piechur "+
            "powinien uwa^za^c ^zeby nogi nie skr^eci^c. P^o^lnocny skraj "+
            "duktu porastaj^a niezbyt g^este krzaki i rzadko rosn^ace "+
            "powykrzywiane buki w kierunku kt^orych prowadzi ledwo widoczna "+
            "^scie^zka. Trakt ci^agnie si^e z po^ludniowego zachodu na "+
            "p^o^lnocny wsch^od wzd^lu^z brzegu Pontaru, za^s od zachodu "+
            "ograniczony jest niezbyt g^estym w tym miejscu bukowym lasem, "+
            "w kt^orym schronienie znajduj^a ca^le ptasie rodziny "+
            "umilaj^ace podr^o^z ^spiewami.";
    }
    else
    {
        str = "Trakt jest szeroki, wygodny i wy^lo^zony g^ladkimi "+
            "kamieniami. Tak przynajmniej by^lo kiedy^s - teraz jedynie "+
            "mo^zna powiedzie^c, ^ze jest szeroki. Dwa kupieckie wozy "+
            "na pewno si^e wymin^a, no chyba ^ze wo^xnica nie da rady "+
            "wyjecha^c z g^l^ebokich, wy^z^lobionych w twardym kamieniu "+
            "przez wiele lat kolein. Po niekt^orych kamieniach pozosta^lo "+
            "jedynie wspomnienie - teraz wyrwy po nich wype^lnione s^a "+
            "zamar^xni^et^a ciemn^a brej^a. Podr^o^z podskakuj^acym na "+
            "takich wertepach wozem na pewno do wygodnych nie nale^zy, a i "+
            "piechur powinien uwa^za^c ^zeby nogi nie skr^eci^c. P^o^lnocny "+
            "skraj duktu porastaj^a niezbyt g^este krzaki i rzadko "+
            "rosn^ace, przykryte warstw^a ^sniegu powykrzywiane buki. Trakt "+
            "ci^agnie si^e z po^ludniowego zachodu na p^o^lnocny wsch^od "+
            "wzd^lu^z brzegu pokrytego grub^a warstw^a lodu Pontaru, za^s "+
            "od zachodu ograniczony jest niezbyt g^estym w tym miejscu "+
            "bukowym lasem.";
    }
    }
    if(jest_dzien() == 0)
    {
    if(pora_roku() != MT_ZIMA)
    {
        str = "Trakt jest szeroki, wygodny i wy^lo^zony g^ladkimi "+
            "kamieniami. Tak przynajmniej by^lo kiedy^s - teraz jedynie "+
            "mo^zna powiedzie^c, ^ze jest szeroki. Dwa kupieckie wozy "+
            "na pewno si^e wymin^a, no chyba ^ze wo^xnica nie da rady "+
            "wyjecha^c z g^l^ebokich, wy^z^lobionych w twardym kamieniu "+
            "przez wiele lat kolein. Po niekt^orych kamieniach pozosta^lo "+
            "jedynie wspomnienie - teraz wyrwy po nich wype^lnione s^a "+
            "g^estym, ciemnym b^lotem. Podr^o^z podskakuj^acym na takich "+
            "wertepach wozem na pewno do wygodnych nie nale^zy, a i piechur "+
            "powinien uwa^za^c ^zeby nogi nie skr^eci^c. P^o^lnocny skraj "+
            "duktu porastaj^a niezbyt g^este krzaki i rzadko rosn^ace "+
            "powykrzywiane buki w kierunku kt^orych prowadzi ledwo widoczna "+
            "^scie^zka. Trakt ci^agnie si^e z po^ludniowego zachodu na "+
            "p^o^lnocny wsch^od wzd^lu^z brzegu Pontaru, za^s od zachodu "+
            "ograniczony jest niezbyt g^estym w tym miejscu bukowym lasem, "+
            "w kt^orym schronienie znajduj^a ca^le ptasie rodziny "+
            "umilaj^ace podr^o^z ^spiewami.";
    }
    else
    {
        str = "Trakt jest szeroki, wygodny i wy^lo^zony g^ladkimi "+
            "kamieniami. Tak przynajmniej by^lo kiedy^s - teraz jedynie "+
            "mo^zna powiedzie^c, ^ze jest szeroki. Dwa kupieckie wozy "+
            "na pewno si^e wymin^a, no chyba ^ze wo^xnica nie da rady "+
            "wyjecha^c z g^l^ebokich, wy^z^lobionych w twardym kamieniu "+
            "przez wiele lat kolein. Po niekt^orych kamieniach pozosta^lo "+
            "jedynie wspomnienie - teraz wyrwy po nich wype^lnione s^a "+
            "zamar^xni^et^a ciemn^a brej^a. Podr^o^z podskakuj^acym na "+
            "takich wertepach wozem na pewno do wygodnych nie nale^zy, a i "+
            "piechur powinien uwa^za^c ^zeby nogi nie skr^eci^c. P^o^lnocny "+
            "skraj duktu porastaj^a niezbyt g^este krzaki i rzadko "+
            "rosn^ace, przykryte warstw^a ^sniegu powykrzywiane buki. Trakt "+
            "ci^agnie si^e z po^ludniowego zachodu na p^o^lnocny wsch^od "+
            "wzd^lu^z brzegu pokrytego grub^a warstw^a lodu Pontaru, za^s "+
            "od zachodu ograniczony jest niezbyt g^estym w tym miejscu "+
            "bukowym lasem.";
    }
    }
    str += "\n";
    return str;
}
string
opis_nocy()
{
    string str;
    if(pora_roku() != MT_ZIMA)
    {
        str = "Trakt jest szeroki, wygodny i wy^lo^zony g^ladkimi "+
            "kamieniami. Tak przynajmniej by^lo kiedy^s - teraz jedynie "+
            "mo^zna powiedzie^c, ^ze jest szeroki. Dwa kupieckie wozy "+
            "na pewno si^e wymin^a, no chyba ^ze wo^xnica nie da rady "+
            "wyjecha^c z g^l^ebokich, wy^z^lobionych w twardym kamieniu "+
            "przez wiele lat kolein. Po niekt^orych kamieniach pozosta^lo "+
            "jedynie wspomnienie - teraz wyrwy po nich wype^lnione s^a "+
            "g^estym, ciemnym b^lotem. Podr^o^z podskakuj^acym na takich "+
            "wertepach wozem na pewno do wygodnych nie nale^zy, a i piechur "+
            "powinien uwa^za^c ^zeby nogi nie skr^eci^c. P^o^lnocny skraj "+
            "duktu porastaj^a niezbyt g^este krzaki i rzadko rosn^ace "+
            "powykrzywiane buki w kierunku kt^orych prowadzi ledwo widoczna "+
            "^scie^zka. Trakt ci^agnie si^e z po^ludniowego zachodu na "+
            "p^o^lnocny wsch^od wzd^lu^z brzegu Pontaru, za^s od zachodu "+
            "ograniczony jest niezbyt g^estym w tym miejscu bukowym lasem, "+
            "w kt^orym schronienie znajduj^a ca^le ptasie rodziny "+
            "umilaj^ace podr^o^z ^spiewami.";
    }
    else
    {
        str = "Trakt jest szeroki, wygodny i wy^lo^zony g^ladkimi "+
            "kamieniami. Tak przynajmniej by^lo kiedy^s - teraz jedynie "+
            "mo^zna powiedzie^c, ^ze jest szeroki. Dwa kupieckie wozy "+
            "na pewno si^e wymin^a, no chyba ^ze wo^xnica nie da rady "+
            "wyjecha^c z g^l^ebokich, wy^z^lobionych w twardym kamieniu "+
            "przez wiele lat kolein. Po niekt^orych kamieniach pozosta^lo "+
            "jedynie wspomnienie - teraz wyrwy po nich wype^lnione s^a "+
            "zamar^xni^et^a ciemn^a brej^a. Podr^o^z podskakuj^acym na "+
            "takich wertepach wozem na pewno do wygodnych nie nale^zy, a i "+
            "piechur powinien uwa^za^c ^zeby nogi nie skr^eci^c. P^o^lnocny "+
            "skraj duktu porastaj^a niezbyt g^este krzaki i rzadko "+
            "rosn^ace, przykryte warstw^a ^sniegu powykrzywiane buki. Trakt "+
            "ci^agnie si^e z po^ludniowego zachodu na p^o^lnocny wsch^od "+
            "wzd^lu^z brzegu pokrytego grub^a warstw^a lodu Pontaru, za^s "+
            "od zachodu ograniczony jest niezbyt g^estym w tym miejscu "+
            "bukowym lasem.";
    }
    str += "\n";
    return str;
}

string
sciezka()
{
    if(jest_dzien() == 1)
    {
        if(pora_roku() != MT_ZIMA)
        {
            return ("Wydeptana w trawie, ledwo teraz widoczna ^scie^zka. Na "+
                "tyle szeroka, ^ze mo^zna by si^e pokusi^c, by przejecha^c "+
                "t^edy wozem.\n");
        }
        else
        {
            return ("Pod warstw^a ^sniegu znajdujesz ^scie^zk^e. Na tyle "+
                "szerok^a, ^ze mo^zna by si^e pokusi^c o przejechanie t^edy "+
                "wozem.\n");
        }
    }
    if(jest_dzien() == 0)
    {
        return ("Nie zauwa^zasz niczego takiego.\n");
    }
}
string
rozlewisko()
{
    if(pora_roku() == MT_WIOSNA)
        return ("Wydaje si^e, ^ze trakt zosta^l wytyczony zbyt blisko "+
        "rzeki. Wraz z wiosennymi roztopami, gdy poziom wody w rzece "+
        "wzr^os^l trakt zosta^l zalany. Widzisz jedynie g^ladk^a "+
        "powierzchni^e wody i wystaj^ace ponad ni^a k^epy krzak^ow.\n");
    if(pora_roku() != MT_WIOSNA)
        return ("Nie zauwa^zasz niczego takiego.\n");
}
string
pora_rok()
{
    switch (pora_roku())
    {
         case MT_LATO:
         case MT_WIOSNA:
             return ({"Pontar szumi koj^aco.\n","Pontar szumi cichutko.\n",
             "^Spiew ptak^ow ucich^l na chwil^e.\n"})
             [random(2)];
         case MT_JESIEN:
             return ({"Gdzie^s niedaleko s^lycha^c smutne krakanie wron.\n"})
             [random(1)];
    }
}
