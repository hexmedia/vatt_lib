/* Autor: Avard
   Opis : Brzozek
   Data : 22.12.06*/
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"
#include <pogoda.h>

inherit TRAKT_RINDE_STD;

void create_trakt() 
{
    set_short("Placyk u bram miasta");
    add_exit(TRAKT_RINDE_LOKACJE + "trakt5.c","se",0,TRAKT_RO_FATIG,0);
    add_exit(TRAKT_RINDE_LOKACJE + "trakt8.c","w",0,TRAKT_RO_FATIG,0);
    add_exit(TRAKT_RINDE_LOKACJE + "trakt7.c","n",0,TRAKT_RO_FATIG,0);
    add_sit("na murku","na murku","z murku",10);
    add_object(TRAKT_RINDE_OBIEKTY + "przystanek.c");
    add_object(TRAKT_RINDE_OBIEKTY + "drogowskaz-rinde.c"); //Nie ukrywamy go!
    add_prop(ROOM_I_INSIDE,0);


    //piece of shit...
    LOAD_ERR(DYLEK_PIANA_RINDE+"dylek_in.c");
    object *dupy;
    if(find_object(DYLEK_PIANA_RINDE+"przyglup.c")) {
    dupy = object_clones(find_object(DYLEK_PIANA_RINDE+"przyglup.c"));
    if(sizeof(dupy))
        foreach(object dupa : dupy)
            dupa->remove_object();
    }
    if(find_object(DYLEK_PIANA_RINDE+"dylek.c"))    {
    dupy = object_clones(find_object(DYLEK_PIANA_RINDE+"dylek.c"));
    if(sizeof(dupy))
        foreach(object dupa : dupy)
            dupa->remove_object();
    }
    object dyl = clone_object(DYLEK_PIANA_RINDE+"dylek.c");
    dyl->move(TO);
    dyl->start();


}
public string
exits_description() 
{
    return "Trakt prowadzi na zach^od, po^ludniowy-wsch^od i na p^o^lnoc w "+
        "kierunku bram miasta.\n";
}

string
dlugi_opis()
{
    string str;
    if(jest_dzien() == 1)
    {
        if(pora_roku() != MT_ZIMA)
        {
            str = "Po opuszczeniu po^ludniowej bramy Rinde trakt rozszerza "+
                "si^e w obszerny, wy^lo^zony dopasowanymi kamieniami "+
                "placyk, z kt^orego bior^a pocz^atek drogi: jedna biegn^aca "+
                "dalej na zach^od i druga "+
                "prowadz^aca w kierunku wschodnim. "+
                "G^l^ebokie koleiny wype^lnione teraz ";
            if(CO_PADA(TO) == PADA_DESZCZ)
            {
                str += "po brzegi deszcz^owk^a ";
            }
            else
            {
                if(pora_roku() == MT_WIOSNA || pora_roku() == MT_JESIEN)
                {
                    str += "br^azowym b^lotem ";
                }
                if(pora_roku() == MT_LATO)
                {
                    str += "szarym py^lem ";
                }
            }
            str += "wy^z^lobi^ly liczne kupieckie wozy wykorzystuj^ace to "+
                "miejsce do oczekiwania na otwarcie bram miasta o poranku, "+
                "a czarne plamy spalenizny ozdabiaj^ace w kilku miejscach "+
                "pobocza wspominaj^a jeszcze rozpalone przez podr^o^znych "+
                "ogniska. W kupkach gruzu ci^agn^acych si^e po obu stronach "+
                "traktu, poro^sni^etych chwastami i ruderalnym zielskiem z "+
                "trudem mo^zna rozpozna^c pozosta^lo^sci po niskich, "+
                "kamiennych murkach wybudowanych kiedy^s wzd^lu^z drogi. "+
                "Obok z ziemi wyrasta wbita tam ma�a tabliczka. "+
                "Za rozci^agaj^acym si^e na po^ludniu u st^op stromej "+
                "skarpy bagnistym rozlewiskiem Pontaru poro^sni^etym przez "+
                "bagienne trawy i rachityczne drzewka po^lyskuj^a w oddali "+
                "wody majestatycznej rzeki. Ca^lkowicie pokryte ";
            if(pora_roku() == MT_WIOSNA)
            {
                str += "r^o^zowo-bia^lymi kwiatami, ";
            }
            if(pora_roku() == MT_LATO)
            {
                str += "zielonymi listkami, ";
            }
            if(pora_roku() == MT_JESIEN)
            {
                str += "po^z^o^lk^lymi listkami, ";
            }
            str += "fantazyjnie powyginane ga^l^ezie niedu^zego drzewka "+
                "czepiaj^acego si^e rozpaczliwie korzeniami rozkruszonego "+
                "murka na p^o^lnocnym zachodzie nie przes^laniaj^a zbytnio "+
                "ci^agn^acych si^e a^z po horyzont ";
            if(pora_roku() == MT_LATO || pora_roku() == MT_WIOSNA)
            {
                str += "trawiastych r^ownin.";
            }
            if(pora_roku() == MT_JESIEN)
            {
                str += "jasnozielonych trawiastych r^ownin.";
            }
            if(CO_PADA(TO) == PADA_DESZCZ)
            {
                str += "zrudzia^lych od deszcz^ow trawiastych r^ownin.";
            }
        }
        else
        {
            str = "Po opuszczeniu po^ludniowej bramy Rinde trakt rozszerza "+
                "si^e w obszerny, wy^lo^zony dopasowanymi kamieniami "+
                "placyk, z kt^orego bior^a pocz^atek drogi: jedna biegn^aca "+
                "dalej na zach^od i druga "+
                "prowadz^aca w kierunku wschodnim. "+
                "G^l^ebokie koleiny wype^lnione teraz brunatn^a, "+
                "zamarzni^et^a bryj^a wy^z^lobi^ly liczne kupieckie wozy "+
                "wykorzystuj^ace to miejsce do oczekiwania na otwarcie bram "+
                "miasta o poranku, a czarne plamy spalenizny ozdabiaj^ace w "+
                "kilku miejscach pobocza wspominaj^a jeszcze rozpalone "+
                "przez podr^o^znych "+
                "ogniska. W kupkach gruzu ci^agn^acych si^e po obu stronach "+
                "traktu, poro^sni^etych chwastami i ruderalnym zielskiem z "+
                "trudem mo^zna rozpozna^c pozosta^lo^sci po niskich, "+
                "kamiennych murkach wybudowanych kiedy^s wzd^lu^z drogi. "+
                "Obok z ziemi wyrasta wbita tam ma�a tabliczka. "+
                "Za rozci^agaj^acym si^e na po^ludniu u st^op stromej "+
                "skarpy bagnistym rozlewiskiem Pontaru poro^sni^etym przez "+
                "bagienne trawy i rachityczne drzewka po^lyskuje biel^a "+
                "skuta lodem powierzchnia pot^e^znej rzeki. Ca^lkowicie "+
                "pokryte lodow^a skorup^a, fantazyjnie powyginane ga^l^ezie "+
                "niedu^zego drzewka czepiaj^acego si^e rozpaczliwie "+
                "korzeniami rozkruszonego murka na p^o^lnocnym zachodzie "+
                "nie przes^laniaj^a zbytnio ci^agn^acych si^e a^z po "+
                "horyzont pokrytych ^sniegiem r^ownin. ";
        }
    str +="\n";
    return str;
    }
    if(jest_dzien() == 0)
    {
        if(pora_roku() != MT_ZIMA)
        {
            str = "Po opuszczeniu po^ludniowej bramy Rinde trakt rozszerza "+
                "si^e w obszerny, wy^lo^zony dopasowanymi kamieniami "+
                "placyk, z kt^orego bior^a pocz^atek drogi: jedna biegn^aca "+
                "dalej na zach^od i druga "+
                "prowadz^aca w kierunku wschodnim. "+
                "G^l^ebokie koleiny wype^lnione teraz ";
            if(CO_PADA(TO) == PADA_DESZCZ)
            {
                str += "po brzegi deszcz^owk^a ";
            }
            else
            {
                if(pora_roku() == MT_WIOSNA || pora_roku() == MT_JESIEN)
                {
                    str += "br^azowym b^lotem ";
                }
                if(pora_roku() == MT_LATO)
                {
                    str += "szarym py^lem ";
                }
            }
            str += "wy^z^lobi^ly liczne kupieckie wozy wykorzystuj^ace to "+
                "miejsce do oczekiwania na otwarcie bram miasta o poranku, "+
                "a czarne plamy spalenizny ozdabiaj^ace w kilku miejscach "+
                "pobocza wspominaj^a jeszcze rozpalone przez podr^o^znych "+
                "ogniska. W kupkach gruzu ci^agn^acych si^e po obu stronach "+
                "traktu, poro^sni^etych chwastami i ruderalnym zielskiem z "+
                "trudem mo^zna rozpozna^c pozosta^lo^sci po niskich, "+
                "kamiennych murkach wybudowanych kiedy^s wzd^lu^z drogi. "+
                "Obok z ziemi wyrasta wbita tam ma�a tabliczka. "+
                "Za rozci^agaj^acym si^e na po^ludniu u st^op stromej "+
                "skarpy bagnistym rozlewiskiem Pontaru poro^sni^etym przez "+
                "bagienne trawy i rachityczne drzewka po^lyskuj^a w oddali "+
                "wody majestatycznej rzeki. Ca^lkowicie pokryte ";
            if(pora_roku() == MT_WIOSNA)
            {
                str += "r^o^zowo-bia^lymi kwiatami, ";
            }
            if(pora_roku() == MT_LATO)
            {
                str += "zielonymi listkami, ";
            }
            if(pora_roku() == MT_JESIEN)
            {
                str += "po^z^o^lk^lymi listkami, ";
            }
            str += "fantazyjnie powyginane ga^l^ezie niedu^zego drzewka "+
                "czepiaj^acego si^e rozpaczliwie korzeniami rozkruszonego "+
                "murka na p^o^lnocnym zachodzie nie przes^laniaj^a zbytnio "+
                "ci^agn^acych si^e a^z po horyzont";
			if(CO_PADA(TO) == PADA_DESZCZ)
            {
                str += " zrudzia^lych od deszcz^ow trawiastych r^ownin";
            }
            else if(pora_roku() == MT_LATO || pora_roku() == MT_WIOSNA)
            {
                str += " trawiastych r^ownin";
            }
            else if(pora_roku() == MT_JESIEN)
            {
                str += " jasnozielonych trawiastych r^ownin";
            }
			str +=".";
        }
        else
        {
            str = "Po opuszczeniu po^ludniowej bramy Rinde trakt rozszerza "+
                "si^e w obszerny, wy^lo^zony dopasowanymi kamieniami "+
                "placyk, z kt^orego bior^a pocz^atek drogi: jedna biegn^aca "+
                "dalej na zach^od i druga "+
                "prowadz^aca w kierunku wschodnim. "+
                "G^l^ebokie koleiny wype^lnione teraz brunatn^a, "+
                "zamarzni^et^a bryj^a wy^z^lobi^ly liczne kupieckie wozy "+
                "wykorzystuj^ace to "+
                "miejsce do oczekiwania na otwarcie bram miasta o poranku, "+
                "a czarne plamy spalenizny ozdabiaj^ace w kilku miejscach "+
                "pobocza wspominaj^a jeszcze rozpalone przez podr^o^znych "+
                "ogniska. W kupkach gruzu ci^agn^acych si^e po obu stronach "+
                "traktu, poro^sni^etych chwastami i ruderalnym zielskiem z "+
                "trudem mo^zna rozpozna^c pozosta^lo^sci po niskich, "+
                "kamiennych murkach wybudowanych kiedy^s wzd^lu^z drogi. "+
                "Obok z ziemi wyrasta wbita tam ma�a tabliczka. "+
                "Za rozci^agaj^acym si^e na po^ludniu u st^op stromej "+
                "skarpy bagnistym rozlewiskiem Pontaru poro^sni^etym przez "+
                "bagienne trawy i rachityczne drzewka po^lyskuje biel^a "+
                "skuta lodem powierzchnia pot^e^znej rzeki. Ca^lkowicie "+
                "pokryte lodow^a skorup^a, fantazyjnie powyginane ga^l^ezie "+
                "niedu^zego drzewka czepiaj^acego si^e rozpaczliwie "+
                "korzeniami rozkruszonego murka na p^o^lnocnym zachodzie "+
                "nie przes^laniaj^a zbytnio ci^agn^acych si^e a^z po "+
                "horyzont pokrytych ^sniegiem r^ownin."; 
        }   
    str +="\n";
    return str;
    }
}
string
opis_nocy()
{
    string str;
    if(pora_roku() != MT_ZIMA)
        {
            str = "Po opuszczeniu po^ludniowej bramy Rinde trakt rozszerza "+
                "si^e w obszerny, wy^lo^zony dopasowanymi kamieniami "+
                "placyk, z kt^orego bior^a pocz^atek drogi: jedna biegn^aca "+
                "dalej na zach^od i druga "+
                "prowadz^aca w kierunku wschodnim. "+
                "G^l^ebokie koleiny wype^lnione teraz ";
            if(CO_PADA(TO) == PADA_DESZCZ)
            {
                str += "po brzegi deszcz^owk^a ";
            }
            else
            {
                if(pora_roku() == MT_WIOSNA || pora_roku() == MT_JESIEN)
                {
                    str += "br^azowym b^lotem ";
                }
                if(pora_roku() == MT_LATO)
                {
                    str += "szarym py^lem ";
                }
            }
            str += "wy^z^lobi^ly liczne kupieckie wozy wykorzystuj^ace to "+
                "miejsce do oczekiwania na otwarcie bram miasta o poranku, "+
                "a czarne plamy spalenizny ozdabiaj^ace w kilku miejscach "+
                "pobocza wspominaj^a jeszcze rozpalone przez podr^o^znych "+
                "ogniska. W kupkach gruzu ci^agn^acych si^e po obu stronach "+
                "traktu, poro^sni^etych chwastami i ruderalnym zielskiem z "+
                "trudem mo^zna rozpozna^c pozosta^lo^sci po niskich, "+
                "kamiennych murkach wybudowanych kiedy^s wzd^lu^z drogi. "+
                "Obok z ziemi wyrasta wbita tam ma�a tabliczka. "+
                "Za rozci^agaj^acym si^e na po^ludniu u st^op stromej "+
                "skarpy bagnistym rozlewiskiem Pontaru poro^sni^etym przez "+
                "bagienne trawy i rachityczne drzewka po^lyskuj^a w oddali "+
                "wody majestatycznej rzeki. Ca^lkowicie pokryte ";
            if(pora_roku() == MT_WIOSNA)
            {
                str += "r^o^zowo-bia^lymi kwiatami, ";
            }
            if(pora_roku() == MT_LATO)
            {
                str += "zielonymi listkami, ";
            }
            if(pora_roku() == MT_JESIEN)
            {
                str += "po^z^o^lk^lymi listkami, ";
            }
            str += "fantazyjnie powyginane ga^l^ezie niedu^zego drzewka "+
                "czepiaj^acego si^e rozpaczliwie korzeniami rozkruszonego "+
                "murka na p^o^lnocnym zachodzie nie przes^laniaj^a zbytnio "+
                "ci^agn^acych si^e a^z po horyzont ";
            if(pora_roku() == MT_LATO || pora_roku() == MT_WIOSNA)
            {
                str += "trawiastych r^ownin.";
            }
            if(pora_roku() == MT_JESIEN)
            {
                str += "jasnozielonych trawiastych r^ownin.";
            }
            if(CO_PADA(TO) == PADA_DESZCZ)
            {
                str += "zrudzia^lych od deszcz^ow trawiastych r^ownin.";
            }
        }
        else
        {
            str = "Po opuszczeniu po^ludniowej bramy Rinde trakt rozszerza "+
                "si^e w obszerny, wy^lo^zony dopasowanymi kamieniami "+
                "placyk, z kt^orego bior^a pocz^atek drogi: jedna biegn^aca "+
                "dalej na zach^od i druga "+
                "prowadz^aca w kierunku wschodnim. "+
                "G^l^ebokie koleiny wype^lnione teraz brunatn^a, "+
                "zamarzni^et^a bryj^a wy^z^lobi^ly liczne kupieckie wozy "+
                "wykorzystuj^ace to miejsce do oczekiwania na otwarcie bram "+
                "miasta o poranku, a czarne plamy spalenizny ozdabiaj^ace w "+
                "kilku miejscach pobocza wspominaj^a jeszcze rozpalone "+
                "przez podr^o^znych "+
                "ogniska. W kupkach gruzu ci^agn^acych si^e po obu stronach "+
                "traktu, poro^sni^etych chwastami i ruderalnym zielskiem z "+
                "trudem mo^zna rozpozna^c pozosta^lo^sci po niskich, "+
                "kamiennych murkach wybudowanych kiedy^s wzd^lu^z drogi. "+
                "Obok z ziemi wyrasta wbita tam ma�a tabliczka. "+
                "Za rozci^agaj^acym si^e na po^ludniu u st^op stromej "+
                "skarpy bagnistym rozlewiskiem Pontaru poro^sni^etym przez "+
                "bagienne trawy i rachityczne drzewka po^lyskuje biel^a "+
                "skuta lodem powierzchnia pot^e^znej rzeki. Ca^lkowicie "+
                "pokryte lodow^a skorup^a, fantazyjnie powyginane ga^l^ezie "+
                "niedu^zego drzewka czepiaj^acego si^e rozpaczliwie "+
                "korzeniami rozkruszonego murka na p^o^lnocnym zachodzie "+
                "nie przes^laniaj^a zbytnio ci^agn^acych si^e a^z po "+
                "horyzont pokrytych ^sniegiem r^ownin.";
        }
    str +="\n";
    return str;
}