/* Autor: Avard
   Opis : Tinardan
   Data : 17.03.07 */
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"

inherit TRAKT_RINDE_STD;

void create_trakt() 
{
    set_short("Trakt");
    add_exit(TRAKT_RINDE_LOKACJE + "trakt6.c","s",0,TRAKT_RO_FATIG,0);
    add_exit(LAKA_RINDE_LOKACJE + "laka28.c","w",0,TRAKT_RO_FATIG,0);
    add_object(TRAKT_RINDE_DRZWI + "bramas.c"); 
    add_prop(ROOM_I_INSIDE,0);
}
public string
exits_description() 
{
    return "Trakt prowadzi na po^ludnie, za^s na p^o^lnocy znajduje si^e "+
        "brama miasta.\n";
}

string
dlugi_opis()
{
    string str;
    object brama = present("brama", TO);
    if(jest_dzien() == 1)
    {
        if(pora_roku() != MT_ZIMA)
        {
            str = "Mury miasta Rinde wznosz� si^e ponad tob�, szare, "+
                "nier^owne, pot^e^zne. Ich chropaw� powierzchni^e porasta "+
                "mech, a gdzieniegdzie, znalaz^lszy sobie odpowiednie "+
                "wg^l^ebienie, nie�mia^lo wychyla si^e bladozielona trawka. "+
                "Brama prowadz�ca do miasta jest ";
            if(brama->query_open() == 1)
            {
                str += "otwarta szeroko. ";
            }
            else
            {
                str += "zamkni^eta. ";
            }
            str += "Jej stalowe, grube skrzyd^la dodatkowo wzmocnione s� "+
                "pot^e^znymi ^cwiekami i sztabami. Z jednej strony widnieje "+
                "niewielka furtka, wpasowuj�ca si^e tak idealnie w szaro�^c "+
                "bramy, ^ze niemal niewidoczna. Trakt biegnie wprost na "+
                "p^o^lnoc, jest zadbany i wygodny do podr^o^zowania nawet "+
                "dla ci^e^zkich woz^ow. Kamienie pouk^ladano r^owno i na "+
                "tyle zwarcie, by nie tworzy^ly si^e pomi^edzy nimi "+
                "zdradliwe dziury i ka^lu^ze. Go�ciniec jest na tyle "+
                "szeroki, by bez problemu mog^ly si^e na nim min�^c dwa "+
                "spore pojazdy nie tarasuj�c przy tym drogi i nie ryzykuj�c "+
                "wypadni^eciem na pobocze.";
        }
        else
        {
            str = "Mury miasta Rinde wznosz� si^e ponad tob�, szare, "+
                "nier^owne, pot^e^zne. Ich chropaw� powierzchni^e porasta "+
                "mech, a gdzieniegdzie wida^c spore kopczyki �niegu, kt^ore "+
                "utworzy^ly si^e wykorzystuj�c zag^l^ebienia i nier^owno�ci "+
                "�cian. Brama prowadz�ca do miasta jest ";
            if(brama->query_open() == 1)
            {
                str += "otwarta szeroko. ";
            }
            else
            {
                str += "zamkni^eta. ";
            }
            str += "Jej stalowe, grube skrzyd^la dodatkowo wzmocnione s� "+
                "pot^e^znymi ^cwiekami i sztabami. Z jednej strony widnieje "+
                "niewielka furtka, wpasowuj�ca si^e tak idealnie w szaro�^c "+
                "bramy, ^ze niemal niewidoczna. Trakt biegnie wprost na "+
                "p^o^lnoc, jest zadbany i wygodny do podr^o^zowania nawet "+
                "dla ci^e^zkich woz^ow. �nieg na go�ci^ncu jest szary, a "+
                "jego warstwa nie jest gruba, tote^z mo^zna dostrzec, i^z "+
                "kamienie pouk^ladano r^owno i na tyle zwarcie, by nie "+
                "tworzy^ly si^e pomi^edzy nimi zdradliwe dziury. Droga jest "+
                "na tyle szeroka, by bez problemu mog^ly si^e na nim min�^c "+
                "dwa spore pojazdy nie tarasuj�c przy tym drogi i nie "+
                "ryzykuj�c wypadni^eciem na pobocze.";
        }    
    }
    if(jest_dzien() == 0)
    {
        if(pora_roku() != MT_ZIMA)
        {
            str = "Mury miasta Rinde wznosz� si^e ponad tob�, szare, "+
                "nier^owne, pot^e^zne. Ich chropaw� powierzchni^e porasta "+
                "mech, a gdzieniegdzie, znalaz^lszy sobie odpowiednie "+
                "wg^l^ebienie, nie�mia^lo wychyla si^e bladozielona trawka. "+
                "Brama prowadz�ca do miasta jest ";
            if(brama->query_open() == 1)
            {
                str += "otwarta szeroko. ";
            }
            else
            {
                str += "zamkni^eta. ";
            }
            str += "Jej stalowe, grube skrzyd^la dodatkowo wzmocnione s� "+
                "pot^e^znymi ^cwiekami i sztabami. Z jednej strony widnieje "+
                "niewielka furtka, wpasowuj�ca si^e tak idealnie w szaro�^c "+
                "bramy, ^ze niemal niewidoczna. Trakt biegnie wprost na "+
                "p^o^lnoc, jest zadbany i wygodny do podr^o^zowania nawet "+
                "dla ci^e^zkich woz^ow. Kamienie pouk^ladano r^owno i na "+
                "tyle zwarcie, by nie tworzy^ly si^e pomi^edzy nimi "+
                "zdradliwe dziury i ka^lu^ze. Go�ciniec jest na tyle "+
                "szeroki, by bez problemu mog^ly si^e na nim min�^c dwa "+
                "spore pojazdy nie tarasuj�c przy tym drogi i nie ryzykuj�c "+
                "wypadni^eciem na pobocze.";
        }
        else
        {
            str = "Mury miasta Rinde wznosz� si^e ponad tob�, szare, "+
                "nier^owne, pot^e^zne. Ich chropaw� powierzchni^e porasta "+
                "mech, a gdzieniegdzie wida^c spore kopczyki �niegu, kt^ore "+
                "utworzy^ly si^e wykorzystuj�c zag^l^ebienia i nier^owno�ci "+
                "�cian. Brama prowadz�ca do miasta jest ";
            if(brama->query_open() == 1)
            {
                str += "otwarta szeroko. ";
            }
            else
            {
                str += "zamkni^eta. ";
            }
            str += "Jej stalowe, grube skrzyd^la dodatkowo wzmocnione s� "+
                "pot^e^znymi ^cwiekami i sztabami. Z jednej strony widnieje "+
                "niewielka furtka, wpasowuj�ca si^e tak idealnie w szaro�^c "+
                "bramy, ^ze niemal niewidoczna. Trakt biegnie wprost na "+
                "p^o^lnoc, jest zadbany i wygodny do podr^o^zowania nawet "+
                "dla ci^e^zkich woz^ow. �nieg na go�ci^ncu jest szary, a "+
                "jego warstwa nie jest gruba, tote^z mo^zna dostrzec, i^z "+
                "kamienie pouk^ladano r^owno i na tyle zwarcie, by nie "+
                "tworzy^ly si^e pomi^edzy nimi zdradliwe dziury. Droga jest "+
                "na tyle szeroka, by bez problemu mog^ly si^e na nim min�^c "+
                "dwa spore pojazdy nie tarasuj�c przy tym drogi i nie "+
                "ryzykuj�c wypadni^eciem na pobocze.";
        }    
    }

    str +="\n";
    return str;
}
string
opis_nocy()
{
    string str;
    object brama = present("brama", TO);
    if(pora_roku() != MT_ZIMA)
        {
            str = "Mury miasta Rinde wznosz� si^e ponad tob�, szare, "+
                "nier^owne, pot^e^zne. Ich chropaw� powierzchni^e porasta "+
                "mech, a gdzieniegdzie, znalaz^lszy sobie odpowiednie "+
                "wg^l^ebienie, nie�mia^lo wychyla si^e bladozielona trawka. "+
                "Brama prowadz�ca do miasta jest ";
            if(brama->query_open() == 1)
            {
                str += "otwarta szeroko. ";
            }
            else
            {
                str += "zamkni^eta. ";
            }
            str += "Jej stalowe, grube skrzyd^la dodatkowo wzmocnione s� "+
                "pot^e^znymi ^cwiekami i sztabami. Z jednej strony widnieje "+
                "niewielka furtka, wpasowuj�ca si^e tak idealnie w szaro�^c "+
                "bramy, ^ze niemal niewidoczna. Trakt biegnie wprost na "+
                "p^o^lnoc, jest zadbany i wygodny do podr^o^zowania nawet "+
                "dla ci^e^zkich woz^ow. Kamienie pouk^ladano r^owno i na "+
                "tyle zwarcie, by nie tworzy^ly si^e pomi^edzy nimi "+
                "zdradliwe dziury i ka^lu^ze. Go�ciniec jest na tyle "+
                "szeroki, by bez problemu mog^ly si^e na nim min�^c dwa "+
                "spore pojazdy nie tarasuj�c przy tym drogi i nie ryzykuj�c "+
                "wypadni^eciem na pobocze.";
        }
        else
        {
            str = "Mury miasta Rinde wznosz� si^e ponad tob�, szare, "+
                "nier^owne, pot^e^zne. Ich chropaw� powierzchni^e porasta "+
                "mech, a gdzieniegdzie wida^c spore kopczyki �niegu, kt^ore "+
                "utworzy^ly si^e wykorzystuj�c zag^l^ebienia i nier^owno�ci "+
                "�cian. Brama prowadz�ca do miasta jest ";
            if(brama->query_open() == 1)
            {
                str += "otwarta szeroko. ";
            }
            else
            {
                str += "zamkni^eta. ";
            }
            str += "Jej stalowe, grube skrzyd^la dodatkowo wzmocnione s� "+
                "pot^e^znymi ^cwiekami i sztabami. Z jednej strony widnieje "+
                "niewielka furtka, wpasowuj�ca si^e tak idealnie w szaro�^c "+
                "bramy, ^ze niemal niewidoczna. Trakt biegnie wprost na "+
                "p^o^lnoc, jest zadbany i wygodny do podr^o^zowania nawet "+
                "dla ci^e^zkich woz^ow. �nieg na go�ci^ncu jest szary, a "+
                "jego warstwa nie jest gruba, tote^z mo^zna dostrzec, i^z "+
                "kamienie pouk^ladano r^owno i na tyle zwarcie, by nie "+
                "tworzy^ly si^e pomi^edzy nimi zdradliwe dziury. Droga jest "+
                "na tyle szeroka, by bez problemu mog^ly si^e na nim min�^c "+
                "dwa spore pojazdy nie tarasuj�c przy tym drogi i nie "+
                "ryzykuj�c wypadni^eciem na pobocze.";
        }    


    str +="\n";
    return str;
}
