/* Autor: Avard
   Opis : Yran
   Data : 29.11.06 */
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"

inherit TRAKT_RINDE_STD;
inherit "/lib/drink_water";
void create_trakt() 
{
    set_short("Trakt przy rzece");
    add_exit(TRAKT_RINDE_LOKACJE + "trakt33.c","ne",0,TRAKT_RO_FATIG,0);
    add_exit(TRAKT_RINDE_LOKACJE + "trakt35.c","w",0,TRAKT_RO_FATIG,0);
    add_exit(LAS_RINDE_LOKACJE + "las70.c","nw",0,TRAKT_RO_FATIG,0);
    add_exit(LAS_RINDE_LOKACJE + "las69.c","n",0,TRAKT_RO_FATIG,0);
    set_drink_places(({"z rzeki","z pontaru","z Pontaru"}));
    add_prop(ROOM_I_INSIDE,0);

    add_item(({"Pontar","pontar","rzek^e"}),"P^lyn^acy tu od tysi^ecy lat "+
    "szeroki Pontar zdaje si^e by^c wci^a^z nieujarzmionym i niezdobytym, "+
    "wyznacza nie tylko granice mi^edzy kr^olestwami Redanii i Temeri, "+
    "ale i skutecznie ogranicza ludzkie zap^edy w wyrywaniu z r^ak "+
    "matki Natury wszystkiego, co sie jej nale^zy. Gdzieniegdzie "+
    "pojawiaj^ace si^e z nienacka niewielkie zawirowania wody na "+
    "spokojnie, jakby leniwie poruszaj^acej si^e m^etnej tafli swiadcz^a "+
    "o nieprzywidywalno^sci i zdradzieckiej naturze nurtu, kt^ory zapewne "+
    "pochlon^a^l niejedno istnienie bezmy^slnie probuj^ace zmierzy^c si^e z "+
    "pot^eg^a rzeki.\n");
}

void
init()
{
    ::init();
    init_drink_water(); 
} 

public string
exits_description() 
{
    return "Trakt prowadzi na zach^od i p^o^lnocny-wsch^od.\n";
}

string
dlugi_opis()
{
    string str;
    if(jest_dzien() == 1)
    {
        if(pora_roku() != MT_ZIMA)
        {
            str = "Znajdujesz si^e w pobli^zu ^sciany rozleg^lego lasu, "+
                "kt^ory zas^lania poka^xny kawa^lek p^o^lnocnego horyzontu. "+
                "Z drugiej strony, stosunkowo blisko ciebie, rozpo^sciera "+
                "sie widok na leniwie p^lyn^ace wody Pontaru, kt^ore z "+
                "cichym szumem rozbijaj^a si^e o wystaj^ace ponad "+
                "powierzchnie wody kamienie. Scie^zka zmierza ^lagodnym "+
                "spadem w d^o^l na zach^od by dalej  rozpocz^a^c sw^a "+
                "m^ecz^ac^a w^edr^owk^e ostrym podej^sciem pod g^orem i "+
                "znikn^a^c za rosn^acym przy niej roz^lo^zystym, "+
                "kolczastym krzakiem. Na wschodzie delikatn^a lini^a "+
                "zaznacza si^e czysta, b^lekitna woda rzeki, a^z w ko^ncu "+
                "gwa^ltownie skr^eca w bok poczym dalszy jej bieg "+
                "przys^laniaj^a rosn^ace przy brzegu wierzby zanurzaj^ace w "+
                "wodzie swe bezw^ladnie opadaj^ace ga^l^ezie.";
        }
        else
        {
            str = "Znajdujesz si^e w pobli^zu ^sciany rozleg^lego lasu, "+
                "kt^ory zas^lania poka^xny kawa^lek p^o^lnocnego horyzontu. "+
                "Z drugiej strony, stosunkowo blisko ciebie, rozpo^sciera "+
                "sie widok na cz^e^sciowo pokryt^a kr^a rzek^e. Pokryta "+
                "warstw^a bia^lego puchu  scie^zka zmierza ^lagodnym spadem "+
                "w d^o^l na zach^od by dalej  rozpocz^a^c sw^a m^ecz^ac^a "+
                "w^edr^owk^e ostrym podej^sciem pod g^orem i znikn^a^c za "+
                "rosn^acym przy niej bezlistnym krzakiem. Na wschodzie "+
                "delikatn^a lini^a zaznacza si^e przebijaj^aca pomi^edzy "+
                "warstwami lodu woda rzeki, a^z wko^ncu gwa^ltownie skr^eca "+
                "w bok poczym dalszy jej bieg przys^laniaj^a rosn^ace przy "+
                "brzegu wierzby zanurzaj^ace w wodzie swe nagie, "+
                "bezw^ladnie opadaj^ace ga^l^ezie.";
        }    
    }
    if(jest_dzien() == 0)
    {
        if(pora_roku() != MT_ZIMA)
        {
            str = "Znajdujesz si^e w pobli^zu ^sciany rozleg^lego lasu, "+
                "kt^ory zas^lania poka^xny kawa^lek p^o^lnocnego horyzontu. "+
                "Z drugiej strony, stosunkowo blisko ciebie, rozpo^sciera "+
                "sie widok na leniwie p^lyn^ace wody Pontaru, kt^ore z "+
                "cichym szumem rozbijaj^a si^e o wystaj^ace ponad "+
                "powierzchnie wody kamienie. Scie^zka zmierza ^lagodnym "+
                "spadem w d^o^l na zach^od by dalej  rozpocz^a^c sw^a "+
                "m^ecz^ac^a w^edr^owk^e ostrym podej^sciem pod g^orem i "+
                "znikn^a^c za rosn^acym przy niej roz^lo^zystym, "+
                "kolczastym krzakiem. Na wschodzie delikatn^a lini^a "+
                "zaznacza si^e czysta, b^lekitna woda rzeki, a^z w ko^ncu "+
                "gwa^ltownie skr^eca w bok poczym dalszy jej bieg "+
                "przys^laniaj^a rosn^ace przy brzegu wierzby zanurzaj^ace w "+
                "wodzie swe bezw^ladnie opadaj^ace ga^l^ezie.";
        }
        else
        {
            str = "Znajdujesz si^e w pobli^zu ^sciany rozleg^lego lasu, "+
                "kt^ory zas^lania poka^xny kawa^lek p^o^lnocnego horyzontu. "+
                "Z drugiej strony, stosunkowo blisko ciebie, rozpo^sciera "+
                "sie widok na cz^e^sciowo pokryt^a kr^a rzek^e. Pokryta "+
                "warstw^a bia^lego puchu  scie^zka zmierza ^lagodnym spadem "+
                "w d^o^l na zach^od by dalej  rozpocz^a^c sw^a m^ecz^ac^a "+
                "w^edr^owk^e ostrym podej^sciem pod g^orem i znikn^a^c za "+
                "rosn^acym przy niej bezlistnym krzakiem. Na wschodzie "+
                "delikatn^a lini^a zaznacza si^e przebijaj^aca pomi^edzy "+
                "warstwami lodu woda rzeki, a^z wko^ncu gwa^ltownie skr^eca "+
                "w bok poczym dalszy jej bieg przys^laniaj^a rosn^ace przy "+
                "brzegu wierzby zanurzaj^ace w wodzie swe nagie, "+
                "bezw^ladnie opadaj^ace ga^l^ezie.";
        }    
    }
    str +="\n";
    return str;
}
string
opis_nocy()
{
    string str;
    if(pora_roku() != MT_ZIMA)
    {
        str = "Znajdujesz si^e w pobli^zu ^sciany rozleg^lego lasu, "+
            "kt^ory zas^lania poka^xny kawa^lek p^o^lnocnego horyzontu. "+
            "Z drugiej strony, stosunkowo blisko ciebie, rozpo^sciera "+
            "sie widok na leniwie p^lyn^ace wody Pontaru, kt^ore z "+
            "cichym szumem rozbijaj^a si^e o wystaj^ace ponad "+
            "powierzchnie wody kamienie. Scie^zka zmierza ^lagodnym "+
            "spadem w d^o^l na zach^od by dalej  rozpocz^a^c sw^a "+
            "m^ecz^ac^a w^edr^owk^e ostrym podej^sciem pod g^orem i "+
            "znikn^a^c za rosn^acym przy niej roz^lo^zystym, "+
            "kolczastym krzakiem. Na wschodzie delikatn^a lini^a "+
            "zaznacza si^e czysta, b^lekitna woda rzeki, a^z w ko^ncu "+
            "gwa^ltownie skr^eca w bok poczym dalszy jej bieg "+
            "przys^laniaj^a rosn^ace przy brzegu wierzby zanurzaj^ace w "+
            "wodzie swe bezw^ladnie opadaj^ace ga^l^ezie.";     
    }
    else
    {
        str = "Znajdujesz si^e w pobli^zu ^sciany rozleg^lego lasu, "+
            "kt^ory zas^lania poka^xny kawa^lek p^o^lnocnego horyzontu. "+
            "Z drugiej strony, stosunkowo blisko ciebie, rozpo^sciera "+
            "sie widok na cz^e^sciowo pokryt^a kr^a rzek^e. Pokryta "+
            "warstw^a bia^lego puchu  scie^zka zmierza ^lagodnym spadem "+
            "w d^o^l na zach^od by dalej  rozpocz^a^c sw^a m^ecz^ac^a "+
            "w^edr^owk^e ostrym podej^sciem pod g^orem i znikn^a^c za "+
            "rosn^acym przy niej bezlistnym krzakiem. Na wschodzie "+
            "delikatn^a lini^a zaznacza si^e przebijaj^aca pomi^edzy "+
            "warstwami lodu woda rzeki, a^z wko^ncu gwa^ltownie skr^eca "+
            "w bok poczym dalszy jej bieg przys^laniaj^a rosn^ace przy "+
            "brzegu wierzby zanurzaj^ace w wodzie swe nagie, "+
            "bezw^ladnie opadaj^ace ga^l^ezie.";
    }    
    str +="\n";
    return str;
}