/* Autor: Avard
   Opis : Brz^ozek
   Data : 15.10.06 */
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"
#include <pogoda.h>
#define DEF_DIRS ({"p�noc"})
inherit TRAKT_RINDE_STD;
inherit "/lib/drink_water";

void create_trakt() 
{
    set_short("Trakt");
    add_exit(TRAKT_RINDE_LOKACJE + "trakt13.c","e",0,TRAKT_RO_FATIG,0);
    add_exit(TRAKT_RINDE_LOKACJE + "trakt15.c","nw",0,TRAKT_RO_FATIG,0);
    add_exit(LAKA_RINDE_LOKACJE + "laka54.c","n",0,TRAKT_RO_FATIG,0);

    add_prop(ROOM_I_INSIDE,0);
    set_drink_places(({"z rzeki","z pontaru","z Pontaru"}));
    add_sit(({"nad rzek^a","nad pontarem","nad Pontarem"}),
        "nad rzek^a","z nad rzeki",0);
    add_item(({"Pontar","pontar","rzek^e"}),"P^lyn^acy tu od tysi^ecy lat "+
    "szeroki Pontar zdaje si^e by^c wci^a^z nieujarzmionym i niezdobytym, "+
    "wyznacza nie tylko granice mi^edzy kr^olestwami Redanii i Temeri, "+
    "ale i skutecznie ogranicza ludzkie zap^edy w wyrywaniu z r^ak "+
    "matki Natury wszystkiego, co sie jej nale^zy. Gdzieniegdzie "+
    "pojawiaj^ace si^e z nienacka niewielkie zawirowania wody na "+
    "spokojnie, jakby leniwie poruszaj^acej si^e m^etnej tafli swiadcz^a "+
    "o nieprzywidywalno^sci i zdradzieckiej naturze nurtu, kt^ory zapewne "+
    "pochlon^a^l niejedno istnienie bezmy^slnie probuj^ace zmierzy^c si^e z "+
    "pot^eg^a rzeki.\n");
}
public string
exits_description() 
{
    return "Trakt prowadzi na p^o^lnocny-zach^od i wsch^od.\n";
}
void
init()
{
    ::init();
    init_drink_water(); 
} 

int unq_no_move(string str)
{
    ::unq_no_move(str);

    if (member_array(query_verb(), DEF_DIRS) != -1 && 
        pora_roku() == MT_WIOSNA)
        notify_fail("Ostre kolce tarniny rani� ci� bole�nie i zmuszaj� "+
        "do rezygnacji z tej drogi.\n");
    return 0;
}
string
dlugi_opis()
{
    string str;
    if(pora_roku() != MT_ZIMA)
    {
        str = "Biegn^acy z p^o^lnocnego wschodu, od bram Rinde trakt, "+
            "wystarczaj^aco szeroki, aby bez trudu min^e^ly si^e na nim "+
            "dwa wy^ladowane, kupieckie wozy skr^eca tutaj na p^o^lnocny "+
            "zach^od w kierunku por^eby i niknie dalej w do^s^c g^estym "+
            "lesie porastaj^acym brzegi Pontaru. Grube drzewa rosn^ace "+
            "po obu jego stronach splataj^a swe ga^l^ezie tworz^ac g^esty "+
            "dach nad wy^lo^zon^a dopasowanymi kamieniami niezbyt r^own^a "+
            "drog^a. ";
        if(sizeof(FILTER_LIVE(all_inventory()))>2)
        {
            str += "Harmider spowodowany przez przemieszczaj^acych si^e "+
               "w^edrowc^ow nape^lnia przestrze^n cienistego tunelu "+
                "irytuj^ac^a kakofoni^a d^xwi^ek^ow. ";
        }
        else
        {
            str += "Brz^eczenie owad^ow i ^swiergot uganiaj^acych "+
                "si^e za nimi ptak^ow nape^lniaj^a przestrze^n "+
                "cienistego tunelu mi^lymi dla ucha d^xwi^ekami. ";
        }
        str += "Brunatne b�ocko wype�niaj�ce dziury po wyrwanych kamieniach "+
            "pokrywa prawie wszystko dooko�a - kamienie, kt�rymi wy�o�ona "+
            "jest droga, mech rosn�cy w przerwach pomi�dzy nimi, pobocze i "+
            "zalegaj�ce na nim �mieci pozostawione przez odpoczywaj�cych "+
            "tutaj podr�nik�w a nawet cz�ciowo pnie drzew rosn�cych "+
            "wzd�u� traktu. Zielone, kolczaste krzewy wype�niaj�ce prawie "+
            "ca�kowicie przestrze� miedzy drzewami po obu stronach drogi i "+
            "nieliczne, rachityczne zielska rosn�ce na poboczach to jedyna "+
            "ro�linno��, kt�ra nie uleg�a zadeptaniu przez watahy "+
            "w�drowc�w. Za zwartym murem zaro�li po p�nocnej stronie "+
            "traktu wida� tylko ";
        if(ZACHMURZENIE(this_object()) == POG_ZACH_ZEROWE)
        {
            str +="b��kitne niebo, ";
        }
        if(ZACHMURZENIE(this_object()) == POG_ZACH_SREDNIE ||
           ZACHMURZENIE(this_object()) == POG_ZACH_DUZE ||
           ZACHMURZENIE(this_object()) == POG_ZACH_CALKOWITE)
        {
            str +="zachmurzone niebo, ";
        }
        if(CO_PADA(this_object()) == PADA_DESZCZ && 
           MOC_OPADOW(this_object()) == PADA_LEKKO)
        {
            str +="drobne kropelki deszczu, ";
        }
        else
        {
            str +="strugi ulewnego deszczu, ";
        }
        str +="a zza znacznie ni�szych krzak�w po po�udniowej jego stronie "+
        "dobiega ";
        if(CO_PADA(this_object()) == NIC_NIE_PADA)
        {
            str +="jednostajny, melodyjny szum p�yn�cych w�d Pontaru. ";
        }
        else
        {
            str +="gro�ne huczenie wzburzonych w�d Pontaru ";
        }
    }
    else
    {
        str = "Biegn^acy od bram Rinde na p^o^lnocnym wschodzie trakt, "+
            "wystarczaj^aco szeroki, aby bez trudu min^e^ly si^e na nim "+
            "dwa wy^ladowane, kupieckie wozy skr^eca tutaj na p^o^lnocny "+
            "zach^od w kierunku por^eby i niknie dalej w do^s^c g^estym "+
            "lesie porastaj^acym brzegi Pontaru. Obficie pokryte "+
            "^sniegiem i lodem grube drzewa rosn^ace po obu jego "+
            "stronach splataj^a swe ga^l^ezie tworz^ac g^esty dach "+
            "nad wy^lo^zon^a dopasowanymi kamieniami niezbyt r^own^a "+
            "drog^a. ";
        if(jest_dzien() == 1)
        {
            str +="Ochryp^le krakanie wron ";
        }
        else
        {
            str +="Trzeszczenie marzn^acych drzew ";
        }
        str += "m^aci panuj^ac^a w tym ";
        if(jest_dzien() == 1)
        {
            str +="ponurym ";
        }
        else
        {
            str +="ciemnym ";
        }
        str +="tunelu cisz^e. Oba pobocza pokryte zamarzni^et^a, brunatn^a "+
            "bryj^a w niczym ju^z nie przypominaj^ac^a ^sniegu nie "+
            "zach^ecaj^a do odpoczynku. Za przerw^a w okrytych grub^a "+
            "warstw^a ^sniegu zaro^slach po p^o^lnocnej stronie traktu ";
        if(jest_dzien() == 1)
        {
            str +="l^sni o^slepiaj^ac^a biel^a ";
        }
        else
        {
            str +="bieli si^e w ciemno^sciach ";
        }
        str +="pokryta ^sniegiem, ci^agn^aca si^e a^z po horyzont r^ownina, "+
            "a zza znacznie ni^zszych krzak^ow po po^ludniowej stronie "+
            "traktu dobiega ciche potrzaskiwanie p^ekaj^acego lodu "+
            "pokrywaj^acego Pontar.";
    }
    str += "\n";
    return str;
}

string
opis_nocy()
{
    string str;
    if(pora_roku() != MT_ZIMA)
    {
        str = "Biegn^acy z p^o^lnocnego wschodu, od bram Rinde trakt, "+
            "wystarczaj^aco szeroki, aby bez trudu min^e^ly si^e na nim "+
            "dwa wy^ladowane, kupieckie wozy skr^eca tutaj na p^o^lnocny "+
            "zach^od w kierunku por^eby i niknie dalej w do^s^c g^estym "+
            "lesie porastaj^acym brzegi Pontaru. Grube drzewa rosn^ace "+
            "po obu jego stronach splataj^a swe ga^l^ezie tworz^ac g^esty "+
            "dach nad wy^lo^zon^a dopasowanymi kamieniami niezbyt r^own^a "+
            "drog^a. ";
        if(sizeof(FILTER_LIVE(all_inventory()))>=2)
        {
            str += "Harmider spowodowany przez przemieszczaj^acych si^e "+
                "w^edrowc^ow nape^lnia przestrze^n mrocznego tunelu "+
                "irytuj^ac^a kakofoni^a d^xwi^ek^ow. ";
        }
        else
        {
            str += "Brz^eczenie owad^ow i pohukiwanie male^nkich "+
                "s^owek nape^lnia przestrze^n mrocznego tunelu "+
                "nieprzyjemnymi d^xwi^ekami mog^acymi napawa^c "+
                "strachem podczas d^lugich w^edr^owek traktem. "; 
        }
        str += "Brunatne b^locko pokrywa prawie wszystko dooko^la - "+
            "kamienie, kt^orymi wy^lo^zona jest droga, mech rosn^acy w "+
            "przerwach pomi^edzy nimi, pobocze, a nawet cz^e^sciowo pnie "+
            "drzew rosn^acych wzd^lu^z traktu. Zielone, kolczaste krzewy "+
            "wype^lniaj^ace prawie ca^lkowicie przestrze^n miedzy drzewami "+
            "po obu stronach drogi i nieliczne, rachityczne zielska "+
            "rosn^ace na poboczach to jedyna ro^slinno^s^c, kt^ora nie "+
            "uleg^la zadeptaniu przez watahy w^edrowc^ow. Za wyr^abanym "+
            "jakim^s ostrym narz^edziem i systematycznie poszerzanym "+
            "przej^sciem w zaro^slach po p^o^lnocnej stronie traktu "+
            "ci^agn^aca si^e a^z po horyzont r^ownina faluje ^lanem "+
            "poszarza^lych traw, a zza znacznie ni^zszych krzak^ow "+
            "po poudniowej jego stronie dobiega ";
        if(CO_PADA(this_object()) == PADA_DESZCZ)
        {
            str += "wzburzonych w�d rzeki. ";
        }
        if(MOC_OPADOW(this_object()) == NIC_NIE_PADA)
        {
            str += "leniwie p�yn�cych b��kitnych w�d rzeki. ";
        } 
    }
    else
    {
        str = "Biegn^acy od bram Rinde na p^o^lnocnym wschodzie trakt, "+
            "wystarczaj^aco szeroki, aby bez trudu min^e^ly si^e na nim "+
            "dwa wy^ladowane, kupieckie wozy skr^eca tutaj na p^o^lnocny "+
            "zach^od w kierunku por^eby i niknie dalej w do^s^c g^estym "+
            "lesie porastaj^acym brzegi Pontaru. Obficie pokryte "+
            "^sniegiem i lodem grube drzewa rosn^ace po obu jego "+
            "stronach splataj^a swe ga^l^ezie tworz^ac g^esty dach "+
            "nad wy^lo^zon^a dopasowanymi kamieniami niezbyt r^own^a "+
            "drog^a. Trzeszczenie marzn^acych drzew m^aci panuj^ac^a w tym "+
            "ciemnym tunelu cisz^e. Oba pobocza pokryte zamarzni^et^a, "+
            "brunatn^a bryj^a w niczym ju^z nie przypominaj^ac^a ^sniegu "+
            "nie zach^ecaj^a do odpoczynku. Za przerw^a w okrytych grub^a "+
            "warstw^a ^sniegu zaro^slach po p^o^lnocnej stronie traktu "+
            "bieli si^e w ciemno^sciach pokryta ^sniegiem, ci^agn^aca si^e "+
            "a^z po horyzont r^ownina, a zza znacznie ni^zszych krzak^ow "+
            "po po^ludniowej stronie traktu dobiega ciche potrzaskiwanie "+
            "p^ekaj^acego lodu pokrywaj^acego Pontar.";
    }
    str += "\n";
    return str;
}

string
event_pora_roku()
{
    switch (pora_roku())
    {
        case MT_JESIEN:
        case MT_LATO:
        case MT_WIOSNA:
            return ({"Promienie s^loneczne przedzieraj^a si^e pomi^edzy "+
            "ga^l^eziami i maluj^a z^lociste plamy na brunatnych "+
            "kamieniach traktu.\n"})[random(1)];
        case MT_ZIMA:
            return ({"Wielka pecyna mokrego ^sniegu spada ci wprost za "+
            "ko^lnierz.\n","D^lugie, ^za^losne wycie wyg^lodnia^lego "+
            "wilczyska dudni w^sr^od rosn^acych wzd^lu^z traktu drzew.\n",
            "P^latki ^sniegu opadaj^a powoli zas^laniaj^ac bia^l^a "+
            "warstewk^a brunatn^a bryj^e pokrywaj^ac^a drog^e.\n",
            "Promienie s^loneczne przedzieraj^a si^e pomi^edzy ga^l^eziami "+
            "i maluj^a z^lociste plamy na brunatnych kamieniach traktu.\n"})
            [random(4)];
    }
}