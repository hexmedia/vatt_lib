/*
 * Opis oraz kod automatycznie wygenerowany za pomoca VattGen,
 * Data: Wednesday 05th of December 2007 07:40:21 PM
 */

#include <pogoda.h>
#include <stdproperties.h>
#include <mudtime.h>
#include "dir.h"

inherit PIANA_OKOLICE_TRAKT_STD;

void
create_trakt()
{
    set_short("Ubity trakt");
    set_long("@@dlugi_opis@@");
    add_exit(PIANA_OKOLICE_TRAKT_LOKACJE + "droga_07.c","se",0,FATIGUE,0);
    add_exit(PIANA_OKOLICE_TRAKT_LOKACJE + "droga_09.c","n",0,FATIGUE,0);
    add_exit(PIANA_BAGNO_LOKACJE + "bagno_08.c","s",0,FATIGUE,0);
    add_exit(PIANA_BAGNO_LOKACJE + "bagno_05.c","w",0,FATIGUE,0);
    add_exit(PIANA_BAGNO_LOKACJE + "bagno_02.c","nw",0,FATIGUE,0);
    add_prop(ROOM_I_INSIDE,0);

}

public string
exits_description()
{
    return "Trakt prowadzi na p^o^lnoc i po^ludniowy wsch^od.\n";
}

string
dlugi_opis()
{
    string str = "";

    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="Boki pokrytego ubitym ^sniegiem traktu porastaj^a kolczaste "+
             "krzewy i nieliczne k^epy traw, kt^ore zdo^la^ly przebi^c pop"+
             "rzez zalegaj^aca na ziemi warstw^e bia^lego puchu. Obok cieb"+
             "ie stoi roztrzaskane przez piorun drzewo, rozpo^lowione prze"+
             "z nieobliczalny ^zywio^l, kompletnie nagie i przypr^oszone ^"+
             "sniegiem, sprawia niemi^le i smutne wra^zenie. Droga staje s"+
             "i^e w tym miejscu nieco podmok^la, za^s krzaki po obu jej st"+
             "ronach ust^epuj^a miejsca suchym patykom po pokrzywach, kt^o"+
             "re czekaj^a wiosny by ponownie m^oc oparzy^c jakiego^s beztr"+
             "oskiego, lub nieopatrznego w^edrowca. Ziemi^e mi^edzy resztk"+
             "ami ro^slin pokrywa tu i ^owdzie ^snieg. ";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str+="Boki ubitego traktu porastaj^a liczne krzewy i k^epy zielony"+
             "ch traw, kt^ore p^lynnie ^l^acz^a si^e z okalaj^acymi ^l^aka"+
             "mi. Obok ciebie stoi roztrzaskane przez piorun drzewo, mimo,"+
             " ^ze piorun rozczepi^l tego kasztana niemal na p^o^l, wypusz"+
             "cza on nadal p^aczki i listki - najwidoczniej uderzenie nie "+
             "zdo^la^lo zabi^c drzewa. Droga staje si^e w tym miejscu niec"+
             "o podmok^la, za^s krzaki po obu jej stronach ust^epuj^a miej"+
             "sca m^lodym pokrzywom, kt^ore ju^z czyhaj^a by oparzy^c bezt"+
             "roskiego, lub nieopatrznego w^edrowca. Mi^edzy rozkwitaj^acy"+
             "mi ro^slinami przebiega cieniutka str^o^zka wody. ";
    }
    else if(pora_roku() == MT_LATO)
    {
        str+="Boki ubitego traktu porastaj^a liczne krzewy i k^epy zielony"+
             "ch traw, kt^ore p^lynnie ^l^acz^a si^e z okalaj^acymi ^l^aka"+
             "mi. Obok ciebie stoi roztrzaskane przez piorun drzewo, mimo,"+
             " ^ze piorun rozczepi^l tego kasztana niemal na p^o^l, pokryt"+
             "y jest on zielonym listowiem - najwidoczniej uderzenie nie z"+
             "do^la^lo zabi^c drewa. Droga staje si^e w tym miejscu nieco "+
             "podmok^la, za^s krzaki po obu jej stronach ust^epuj^a miejsc"+
             "a g^estym pokrzywom, kt^ore czyhaj^a by oparzy^c beztroskieg"+
             "o, lub nieopatrznego w^edrowca. Mi^edzy rozkwit^lymi ro^slin"+
             "ami przebiega ^slad po p^lyn^acej tu niegdy^s stru^zce wody."+
             " ";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str+="Boki ubitego traktu porastaj^a liczne krzewy i k^epy zielony"+
             "ch traw, kt^ore p^lynnie ^l^acz^a si^e z okalaj^acymi ^l^aka"+
             "mi. Obok ciebie stoi roztrzaskane przez piorun drzewo, mimo,"+
             " ^ze piorun rozczepi^l tego kasztana niemal na p^o^l, pokryt"+
             "y jest on ^z^o^ltymi i czerwonym listowiem - najwidoczniej u"+
             "derzenie nie zdo^la^lo zabi^c drzewa. Droga staje si^e w tym"+
             " miejscu nieco podmok^la, za^s krzaki po obu jej stronach us"+
             "t^epuj^a miejsca podsychaj^acym pokrzywom, kt^ore nadal czyh"+
             "aj^a by oparzy^c beztroskiego, lub nieopatrznego w^edrowca. "+
             "Mi^edzy z^z^o^lkni^etymi ro^slinami przebiega ^slad po nieda"+
             "wno p^lyn^acej tu stru^zce wody. ";
    }

    str+="\n";
    return str;
}