/* Autor: Avard
   Opis : Samaia + Faeve
   Data : 5.12.07
   Info : Stad ma kursowac prom do Oxenfurtu, a w przyszlosci do Novigradu
          i innych ciekawych miejsc. ;) */

#include <pogoda.h>
#include <stdproperties.h>
#include <mudtime.h>
#include "dir.h"

inherit PIANA_OKOLICE_TRAKT_STD;

void
create_trakt()
{
    set_short("Przy molo");
    set_long("@@dlugi_opis@@");
    add_exit(PIANA_OKOLICE_TRAKT_LOKACJE + "droga_09.c","se",0,FATIGUE,0);
    add_exit(PIANA_BAGNO_LOKACJE + "bagno_02.c","s",0,FATIGUE,0);
    add_prop(ROOM_I_INSIDE,0);

}

public string
exits_description()
{
    return "Trakt prowadzi na po^ludniowy wsch^od.\n";
}

string
dlugi_opis()
{
    string str;
    str ="Leniwie szemrz^acy Pontar rozlewa si^e tutaj szeroko, gubi^ac "+
        "daleko na po^ludniu sw^oj ^zwawy, bystry bieg, w wyniku czego "+
        "wzd^lu^z brzegu ci^agn^a si^e ogromne b^lotniste mokrad^la. "+
        "Przywiewana raz po raz bagienna wo^n przywodzi na my^sl torf i "+
        "zgni^le ro^sliny, za^s wystaj^ace tu i ^owdzie z zaro^sli "+
        "spr^ochnia^le pnie drzew goszcz^a dzikie zwierz^eta i insekty, "+
        "s^lu^z^ac im za schronienie. ";
    //Tu^z do brzegu dobudowano szerokie, drewniane molo, do niego za^s 
    //przycumowano poka^xny prom.
    str+=" Pomost od bagniska rozdziela do^s^c szeroki pas wysokiej, "+
        "soczy^scie zielonej trzciny, b^ed^acej schroniskiem ptactwa, i "+
        "ko^lysz^acej si^e bardziej lub mniej leniwie w takt szumi^acego "+
        "wiatru i fal wywo^lywanych przez przep^lywaj^ace ^lodzie.";
    str+="\n";
    return str;
}