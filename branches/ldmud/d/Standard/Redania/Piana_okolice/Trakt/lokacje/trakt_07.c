/*
 * Opis oraz kod automatycznie wygenerowany za pomoca VattGen,
 * Data: Saturday 03rd of November 2007 12:39:14 PM
 */

#include <pogoda.h>
#include <stdproperties.h>
#include <mudtime.h>
#include "dir.h"

inherit PIANA_OKOLICE_TRAKT_STD;

void
create_trakt()
{
    set_short("Brukowany trakt");
    set_long("@@dlugi_opis@@");
    add_exit(PIANA_OKOLICE_TRAKT_LOKACJE + "trakt_06.c","sw",0,FATIGUE,0);
    add_exit(PIANA_OKOLICE_TRAKT_LOKACJE + "trakt_08.c","e",0,FATIGUE,0);
    add_prop(ROOM_I_INSIDE,0);

}

public string
exits_description()
{
    return "Trakt prowadzi na po^ludniowy zach^od i wsch^od.\n";
}

string
dlugi_opis()
{
    string str = "";

    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="Trakt, bezspornie ^swie^zo u^lo^zony, pozbawiony jakichkolwi"+
             "ek skaz i dziur, wije si^e ^lagodnie, to wznosz^ac si^e po w"+
             "zg^orzu, to opadaj^ac w dolinie. Okoliczne pola uprawne pokr"+
             "yte s^a bia^lym, g^estym puchem, przykrywaj^acym wszystko de"+
             "likatn^a ko^lderk^a. Trakt jest zadbany i od^snie^zony. ";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str+="Trakt, bezspornie ^swie^zo u^lo^zony, pozbawiony jakichkolwi"+
             "ek skaz i dziur, wije si^e ^lagodnie, to wznosz^ac si^e po w"+
             "zg^orzu, to opadaj^ac w dolinie. Okoliczne pola uprawne pokr"+
             "yte s^a kie^lkuj^ac^a ro^slinno^sci^a, gdzieniegdzie przebij"+
             "aj^ac^a sie spod cienkich warstewek stopionego ^sniegu. Trak"+
             "t jest zadbany, a spomi^edzy kostek usuni^eto wi^ekszo^s^c k"+
             "witn^acych chwast^ow.";
    }
    else if(pora_roku() == MT_LATO)
    {
        str+="Trakt, bezspornie ^swie^zo u^lo^zony, pozbawiony jakichkolwi"+
             "ek skaz i dziur, wije si^e ^lagodnie, to wznosz^ac si^e po w"+
             "zg^orzu, to opadaj^ac w dolinie. Okoliczne pola uprawne pokr"+
             "yte s^a bujn^a ro^slinno^sci^a, w ^sr^od kt^orej zapewne kwi"+
             "tnie tak^ze ^zycie drobnych zwierz^at. Trakt jest zadbany i "+
             "tylko w niewielu miejscach spomi^edzy kostek wyrastaj^a niel"+
             "iczne chwasty.";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str+="Trakt, bezspornie ^swie^zo u^lo^zony, pozbawiony jakichkolwi"+
             "ek skaz i dziur, wije si^e ^lagodnie, to wznosz^ac si^e po w"+
             "zg^orzu, to opadaj^ac w dolinie. Okoliczne pola uprawne pokr"+
             "yte s^a wysuszon^a ro^slinno^sci^a, kt^or^a powoli zaczyna o"+
             "bumiera^c. Trakt jest zadbany, cho^c g^esto pokryty listowie"+
             "m opadni^etym z drzew. ";
    }

    str+="\n";
    return str;
}