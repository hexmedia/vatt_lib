/*
 * Opis oraz kod automatycznie wygenerowany za pomoca VattGen,
 * Data: Wednesday 05th of December 2007 06:42:12 PM
 */

#include <pogoda.h>
#include <stdproperties.h>
#include <mudtime.h>
#include "dir.h"

inherit PIANA_OKOLICE_TRAKT_STD;

void
create_trakt()
{
    set_short("Utwardzony trakt");
    set_long("@@dlugi_opis@@");
    add_exit(PIANA_OKOLICE_TRAKT_LOKACJE + "droga_02.c","n",0,FATIGUE,0);
    add_exit(PIANA_LOKACJE_ULICE + "u04.c",({"s","do miasta","z traktu"}),0,FATIGUE,0);

    add_prop(ROOM_I_INSIDE,0);

}

public string
exits_description()
{
    return "Trakt prowadzi na p^o^lnoc i na po^ludnie - w stron^e miasta"+
           ".\n";
}

string
dlugi_opis()
{
    string str = "";

    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="Na szcz^e^scie teraz b^loto normalnie rozci^agaj^ace si^e na"+
             " ca^lej szeroko^sci traktu jest zamarzni^ete, jednak latem p"+
             "rzebrni^ecie przez nie stanowi^c musi nie lada wyzwanie dla "+
             "ci^e^zkich, za^ladowanych po brzegi woz^ow kupieckich. Pomi^"+
             "edzy kamykami po^lo^zonymi by wzmocni^c nawierzchni^e traktu"+
             " wida^c pozamarzane ka^lu^ze. Ziemi^e mi^edzy resztkami ro^s"+
             "lin pokrywa tu i ^owdzie ^snieg. Droga zryta jest licznymi k"+
             "oleinami wy^z^lobionymi w zastyg^lej od mrozu ziemi. ";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str+="G^l^ebokie b^loto rozdeptane przez licznych podr^o^znych roz"+
             "ci^aga si^e na ca^lej szeroko^sci traktu, a przebrni^ecie pr"+
             "zez nie stanowi^c musi nie lada wyzwanie dla ci^e^zkich, za"+
             "^ladowanych po brzegi woz^ow kupieckich, tak^ze i piechur mus"+
             "i uwa^za^c, ^zeby nie wpa^s^c w g^l^ebokie ka^lu^ze. Pomi^ed"+
             "zy kamykami po^lo^zonymi, by wzmocni^c nawierzchni^e traktu, "+
             "zaczyna kie^lkowa^c szorstka trawa i jakie^s inne pospolite "+
             "zielska. Mi^edzy rozkwitaj^acymi ro^slinami przebiega cieniu"+
             "tka str^o^zka wody. ";
    }
    else if(pora_roku() == MT_LATO)
    {
        str+="G^l^ebokie b^loto rozdeptane przez licznych podr^o^znych roz"+
             "ci^aga si^e na ca^lej szeroko^sci traktu, a przebrni^ecie pr"+
             "zez nie stanowi^c musi nie lada wyzwanie dla ci^e^zkich, za"+
             "^ladowanych po brzegi woz^ow kupieckich, tak^ze i piechur mus"+
             "i uwa^za^c, ^zeby nie wpa^s^c w g^l^ebokie ka^lu^ze. Pomi^ed"+
             "zy kamykami po^lo^zonymi, by wzmocni^c nawierzchni^e traktu, "+
             "ro^snie bujnie szorstka trawa i jakie^s inne pospolite ziels"+
             "ka. Mi^edzy rozkwit^lymi ro^slinami przebiega ^slad po p^lyn"+
             "^acej tu niegdy^s stru^zce wody. Droga zryta jest licznymi k"+
             "oleinami wy^z^lobionymi w suchej ziemi. ";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str+="G^l^ebokie b^loto rozdeptane przez licznych podr^o^znych roz"+
             "ci^aga si^e na ca^lej szeroko^sci traktu, a przebrni^ecie pr"+
             "zez nie stanowi^c musi nie lada wyzwanie dla ci^e^zkich, za"+
             "^ladowanych po brzegi woz^ow kupieckich, tak^ze i piechur mus"+
             "i uwa^za^c, ^zeby nie wpa^s^c w g^l^ebokie ka^lu^ze. Pomi^ed"+
             "zy kamykami po^lo^zonymi, by wzmocni^c nawierzchni^e traktu, "+
             "ro^snie trawa i jakie^s inne wysuszone, pospolite zielska. M"+
             "i^edzy z^z^o^lkni^etymi ro^slinami przebiega ^slad po niedaw"+
             "no p^lyn^acej tu stru^zce wody. ";
    }

    str+="\n";
    return str;
}