/*
 * Opis oraz kod automatycznie wygenerowany za pomoca VattGen,
 * Data: Saturday 03rd of November 2007 11:13:47 AM
 */

#include <pogoda.h>
#include <stdproperties.h>
#include <mudtime.h>
#include "dir.h"

inherit PIANA_OKOLICE_TRAKT_STD;

void
create_trakt()
{
    set_short("Brukowany trakt");
    set_long("@@dlugi_opis@@");
    add_exit(PIANA_OKOLICE_TRAKT_LOKACJE + "trakt_02.c","sw",0,FATIGUE,0);
    add_exit(PIANA_OKOLICE_TRAKT_LOKACJE + "trakt_04.c","se",0,FATIGUE,0);
    add_prop(ROOM_I_INSIDE,0);

}

public string
exits_description()
{
    return "Trakt prowadzi na po^ludniowy zach^od i po^ludniowy wsch^od.\n";
}

string
dlugi_opis()
{
    string str = "";

    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="Na po^ludniu rozci^agaj^a si^e niezmierzone po^lacie p^ol, k"+
             "t^ore przykryte bia^lym ^sniegiem wygl^adaj^a jak tafla ogro"+
             "mnego jeziora, na kt^orym panuje ca^lkowita cisza i spok^oj,"+
             " m^acona od czasu do czasu przez mocniejszy podmuch zimnego "+
             "wiatru. Trakt, bez wyrw czy kolein, wygl^ada na wyj^atkowo z"+
             "adbany i dobrze utrzymany. Na otaczaj^acych trakt polach upr"+
             "awnych, wida^c jedynie ^slady drobnych zwierz^at, zostawiony"+
             "ch na mi^ekkim puchu okrywaj^acym ca^l^a okolice. Pola upraw"+
             "ne rozci^agaj^ace si^e dooko^la traktu, pokrywa ^snie^znobia"+
             "^ly puch po kt^orym od czasu do czasu przebiega jakie^s ma^l"+
             "e zwierz^atko. . ";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str+="Na po^ludniu rozci^agaj^a si^e niezmierzone po^lacie p^ol, k"+
             "t^ore b^ed^ac delikatnie zazielenione, s^a prawdziwym rajem "+
             "dla okolicznych ptak^ow i zwierz^at, szukaj^acych jedzenia p"+
             "o zimowym ^snie. Trakt, bez wyrw czy kolein, wygl^ada na wyj"+
             "^atkowo zadbany i dobrze utrzymany. Na otaczaj^acych trakt p"+
             "olach uprawnych, dostrzec mo^zna pracuj^acych w pocie czo^la"+
             " ch^lop^ow, przygotowuj^acych ziemie pod zasiew. Pola uprawn"+
             "e rozci^agaj^ace si^e dooko^la traktu, pokrywa delikatna m^l"+
             "oda ziele^n, w kt^orej buszuj^a obudzone z zimowego snu zwie"+
             "rz^eta. Wzd^lu^z traktu ci^agnie si^e niezbyt g^l^eboki r^ow"+
             " poro^sni^ety traw^a. ";
    }
    else if(pora_roku() == MT_LATO)
    {
        str+="Na po^ludniu rozci^agaj^a si^e niezmierzone po^lacie p^ol, k"+
             "t^ore b^ed^ac pi^eknie zazielenione, s^a prawdziwym rajem dl"+
             "a okolicznych ptak^ow i zwierz^at wydaj^acych piski, skrzeki"+
             " i mruczenia s^lyszalne nawet z du^zej odleg^lo^sci, dzi^eki"+
             " delikatnemu ciep^lemu wietrzykowi. Trakt, bez wyrw czy kole"+
             "in, wygl^ada na wyj^atkowo zadbany i dobrze utrzymany. Na ot"+
             "aczaj^acych trakt polach uprawnych, dostrzec mo^zna pracuj^a"+
             "cych w pocie czo^la ch^lop^ow, piel^egnuj^acych zasiane pola"+
             ". Pola uprawne rozci^agaj^ace si^e dooko^la traktu, pokrywa "+
             "ciemnozielona bujna ro^slinno^s^c, w kt^orej buszuj^a obudzo"+
             "ne z zimowego snu zwierz^eta. Wzd^lu^z traktu ci^agnie si^e "+
             "niezbyt g^l^eboki r^ow poro^sni^ety traw^a. ";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str+="Na po^ludniu rozci^agaj^a si^e niezmierzone po^lacie p^ol, n"+
             "ad kt^orymi snuje si^e lekka mgie^lka nadaj^aca im do^s^c ni"+
             "epokoj^acy wygl^ad, a lekki ch^lodny wietrzyk przywiewa zapa"+
             "chy pal^acych sie ognisk przy kt^orych ch^lopi ogrzewaj^a si"+
             "e w przerwach od pracy. Trakt, bez wyrw czy kolein, wygl^ada"+
             " na wyj^atkowo zadbany i dobrze utrzymany. Na otaczaj^acych "+
             "trakt polach uprawnych, dostrzec mo^zna pracuj^acych w pocie"+
             " czo^la ch^lop^ow, zbieraj^acych ju^z ostatnie plony w tym r"+
             "oku. Pola uprawne rozci^agaj^ace si^e dooko^la traktu, s^a z"+
             "aorane i przygotowane do zimy, a od czasu do czasu mo^zna za"+
             "uwa^zy^c jak przebiega po nich jakie^s drobne zwierz^atko. W"+
             "zd^lu^z traktu ci^agnie si^e niezbyt g^l^eboki r^ow poro^sni"+
             "^ety traw^a. ";
    }

    str+="\n";
    return str;
}