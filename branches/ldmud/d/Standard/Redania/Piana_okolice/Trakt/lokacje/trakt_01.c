/*
 * Opis oraz kod automatycznie wygenerowany za pomoca VattGen,
 * Data: Saturday 03rd of November 2007 11:13:47 AM
 */

#include <pogoda.h>
#include <stdproperties.h>
#include <mudtime.h>
#include "dir.h"

inherit PIANA_OKOLICE_TRAKT_STD;

void
create_trakt()
{
    set_short("Brukowany trakt");
    set_long("@@dlugi_opis@@");
    add_exit(PIANA_LOKACJE_ULICE + "przed_spalona_chalupa",({"w","do miasta","z traktu"}),0,FATIGUE,0);
    add_exit(PIANA_OKOLICE_TRAKT_LOKACJE + "trakt_02.c","e",0,FATIGUE,0);
    add_prop(ROOM_I_INSIDE,0);

}

public string
exits_description()
{
    return "Trakt prowadzi na zach^od w stron^e miasta i na wsch^od.\n";
}

string
dlugi_opis()
{
    string str = "";

    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="Krakanie wron, zamieszkuj^acych spl^atany g^aszcz ga^l^ezi i"+
             " g^este kolczaste zaro^sla odgradzaj^ace dukt od bezkresnych"+
             " r^ownin ci^agn^acych si^e a^z po horyzont, nape^lnia powiet"+
             "rze irytuj^acym dla ucha d^xwi^ekiem. Na otaczaj^acych trakt"+
             " polach uprawnych, wida^c jedynie ^slady drobnych zwierz^at,"+
             " zostawionych na mi^ekkim puchu okrywaj^acym ca^l^a okolice."+
             " Ca^le pole przykrywa gruba warstwa bia^lego puchu. Rozci^ag"+
             "aj^ace si^e po obu stronach pola pokry^ly si^e warstw^a ^sni"+
             "e^znobia^lego puchu. ";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str+="Brz^eczenie owad^ow i szczebiot ptak^ow, zamieszkuj^acych sp"+
             "l^atany g^aszcz ga^l^ezi i g^este kolczaste zaro^sla odgradz"+
             "aj^ace dukt od bezkresnych r^ownin ci^agn^acych si^e a^z po "+
             "horyzont, nape^lnia powietrze mi^lym dla ucha brz^eczeniem. "+
             "Na otaczaj^acych trakt polach uprawnych, dostrzec mo^zna pra"+
             "cuj^acych w pocie czo^la ch^lop^ow, przygotowuj^acych ziemie"+
             " pod zasiew. Pole uprawne niedawno zosta^lo zorane i zasiane"+
             " zbo^zem. Rozci^agaj^ace si^e po obu stronach pola pszenicy "+
             "przyjemnie koj^a wzrok jaskraw^a zieleni^a. ";
    }
    else if(pora_roku() == MT_LATO)
    {
        str+="Brz^eczenie owad^ow i szczebiot ptak^ow, zamieszkuj^acych sp"+
             "l^atany g^aszcz ga^l^ezi i g^este kolczaste zaro^sla odgradz"+
             "aj^ace dukt od bezkresnych r^ownin ci^agn^acych si^e a^z po "+
             "horyzont, nape^lnia powietrze mi^lym dla ucha brz^eczeniem. "+
             "Na otaczaj^acych trakt polach uprawnych, dostrzec mo^zna pra"+
             "cuj^acych w pocie czo^la ch^lop^ow, piel^egnuj^acych zasiane"+
             " pola. Na polu bujnie ro^snie zbo^ze z^lotego koloru. Rozci^"+
             "agaj^ace si^e po obu stronach pola pszenicy szumi^a cicho. ";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str+="Brz^eczenie owad^ow i szczebiot ptak^ow, zamieszkuj^acych sp"+
             "l^atany g^aszcz ga^l^ezi i g^este kolczaste zaro^sla odgradz"+
             "aj^ace dukt od bezkresnych r^ownin ci^agn^acych si^e a^z po "+
             "horyzont, nape^lnia powietrze mi^lym dla ucha brz^eczeniem. "+
             "Na otaczaj^acych trakt polach uprawnych, dostrzec mo^zna pra"+
             "cuj^acych w pocie czo^la ch^lop^ow, zbieraj^acych ju^z ostat"+
             "nie plony w tym roku. Na polu zosta^ly wystaj^ace ko^nc^owki"+
             " ^sci^etego zbo^za jako ^slad po ^zniwach. Rozci^agaj^ace si"+
             "^e po obu stronach pola pszenicy z^loc^a si^e w ostatnich pr"+
             "omieniach jesiennego s^lo^nca. ";
    }

    str+="\n";
    return str;
}