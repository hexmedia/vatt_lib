/*
 * Opis oraz kod automatycznie wygenerowany za pomoca VattGen,
 * Data: Wednesday 21st of November 2007 07:19:55 PM
 */

#include <pogoda.h>
#include <stdproperties.h>
#include <mudtime.h>
#include "dir.h"

inherit PIANA_OKOLICE_TRAKT_STD;
#define DEF_DIRS ({"wsch^od"})

void
create_trakt()
{
    set_short("Trakt w^sr^od ^l^ak");
    set_long("@@dlugi_opis@@");
    add_exit(PIANA_OKOLICE_TRAKT_LOKACJE + "trakt_13.c","w",0,FATIGUE,0);
//    add_exit(PIANA_OKOLICE_TRAKT_LOKACJE + "trakt_15.c","e",0,FATIGUE,0);
    add_prop(ROOM_I_INSIDE,0);

    add_npc(TRAKT_RINDE_NPC+"bandyta_duzy");
    add_npc(TRAKT_RINDE_NPC+"bandyta_krasnolud");
    add_npc(TRAKT_RINDE_NPC+"bandyta_krasnolud");
    add_npc(TRAKT_RINDE_NPC+"bandyta_nozownik");
    add_npc(TRAKT_RINDE_NPC+"bandyta_nozownik");
    add_npc(TRAKT_RINDE_NPC+"bandyta_pijak");
    add_npc(TRAKT_RINDE_NPC+"bandyta_tepy");
    add_npc(TRAKT_RINDE_NPC+"bandyta_zlodziej");
    add_npc(TRAKT_RINDE_NPC+"bandyta_zlodziej");
}

public string
exits_description()
{
    return "Trakt prowadzi na zach^od i wsch^od.\n";
}
int unq_no_move(string str)
{
    ::unq_no_move(str);

    if (member_array(query_verb(), DEF_DIRS) != -1)
        notify_fail("Bandyci blokuj^a ci drog^e.\n");
    return 0;
}
string
dlugi_opis()
{
    string str = "";

    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="Otoczenie zamar^lo w zimowym letargu. Na niewielkiej ^l^ace,"+
             " tu^z przy trakcie wida^c wyra^xne ^slady obozowania wi^eksz"+
             "ej liczby os^ob, ^snieg st^luczony jest ko^lami woz^ow i ko^"+
             "nskimi kopytami, a w chronionym od wiatru miejscu czerni^a s"+
             "i^e dwa kr^egi po sporych ogniskach. Ca^la okolica jest pokr"+
             "yta ^l^akami i nigdzie nie wida^c ^sladu po ludzkich osadach"+
             " a jedynymi ^zywymi osobami jakie napotykasz s^a podr^o^zuj^"+
             "acy ze swym towarem kupcy. Trakt bez w^atpienia zosta^l nied"+
             "awno wybudowany, czego dowodzi r^owno u^lo^zony, jeszcze nie"+
             " zniszczony przez ko^la i kopyta, kamie^n. ";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str+="Otoczenie t^etni rozkwitaj^acym na wiosn^e ^zyciem. Na niewi"+
             "elkiej ^l^ace, tu^z przy trakcie wida^c wyra^xne ^slady oboz"+
             "owania wi^ekszej liczby os^ob, m^loda trawa st^luczona jest "+
             "ko^lami woz^ow i ko^nskimi kopytami, a w chronionym od wiatr"+
             "u miejscu czerni^a si^e dwa kr^egi po sporych ogniskach. Ca^"+
             "la okolica jest pokryta ^l^akami i nigdzie nie wida^c ^sladu"+
             " po ludzkich osadach a jedynymi ^zywymi osobami jakie napoty"+
             "kasz s^a podr^o^zuj^acy ze swym towarem kupcy. Trakt bez w^a"+
             "tpienia zosta^l niedawno wybudowany, czego dowodzi r^owno u^"+
             "lo^zony, jeszcze nie zniszczony przez ko^la i kopyta, kamie^"+
             "n. ";
    }
    else if(pora_roku() == MT_LATO)
    {
        str+="Otoczenie t^etni rozkwit^lym ^zyciem. Na niewielkiej ^l^ace,"+
             " tu^z przy trakcie wida^c wyra^xne ^slady obozowania wi^eksz"+
             "ej liczby os^ob, wysoka trawa st^luczona jest ko^lami woz^ow"+
             " i ko^nskimi kopytami, a w chronionym od wiatru miejscu czer"+
             "ni^a si^e dwa kr^egi po sporych ogniskach. Ca^la okolica jes"+
             "t pokryta ^l^akami i nigdzie nie wida^c ^sladu po ludzkich o"+
             "sadach a jedynymi ^zywymi osobami jakie napotykasz s^a podr^"+
             "o^zuj^acy ze swym towarem kupcy. Trakt bez w^atpienia zosta^"+
             "l niedawno wybudowany, czego dowodzi r^owno u^lo^zony, jeszc"+
             "ze nie zniszczony przez ko^la i kopyta, kamie^n. ";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str+="Otoczenie powoli zamiera w przygotowaniu do zbli^zaj^acej si"+
             "^e zimy. Na niewielkiej ^l^ace, tu^z przy trakcie wida^c wyr"+
             "a^xne ^slady obozowania wi^ekszej liczby os^ob, przyschni^et"+
             "a trawa st^luczona jest ko^lami woz^ow i ko^nskimi kopytami,"+
             " a w chronionym od wiatru miejscu czerni^a si^e dwa kr^egi p"+
             "o sporych ogniskach. Ca^la okolica jest pokryta ^l^akami i n"+
             "igdzie nie wida^c ^sladu po ludzkich osadach a jedynymi ^zyw"+
             "ymi osobami jakie napotykasz s^a podr^o^zuj^acy ze swym towa"+
             "rem kupcy. Trakt bez w^atpienia zosta^l niedawno wybudowany,"+
             " czego dowodzi r^owno u^lo^zony, jeszcze nie zniszczony prze"+
             "z ko^la i kopyta, kamie^n. ";
    }

    str+="\n";
    return str;
}
