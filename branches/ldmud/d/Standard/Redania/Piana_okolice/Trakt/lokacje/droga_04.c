/*
 * Opis oraz kod automatycznie wygenerowany za pomoca VattGen,
 * Data: Wednesday 05th of December 2007 06:59:59 PM
 */

#include <pogoda.h>
#include <stdproperties.h>
#include <mudtime.h>
#include "dir.h"

inherit PIANA_OKOLICE_TRAKT_STD;

void
create_trakt()
{
    set_short("Utwardzony trakt");
    set_long("@@dlugi_opis@@");
    add_exit(PIANA_OKOLICE_TRAKT_LOKACJE + "droga_03.c","s",0,FATIGUE,0);
    add_exit(PIANA_OKOLICE_TRAKT_LOKACJE + "droga_05.c","n",0,FATIGUE,0);
    add_exit(PIANA_BAGNO_LOKACJE + "bagno_18.c","w",0,FATIGUE,0);
    add_exit(PIANA_BAGNO_LOKACJE + "bagno_15.c","nw",0,FATIGUE,0);
    add_prop(ROOM_I_INSIDE,0);

}

public string
exits_description()
{
    return "Trakt prowadzi na p^o^lnoc i po^ludnie.\n";
}

string
dlugi_opis()
{
    string str = "";

    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="Wzd^lu^z drogi ci^agn^a si^e zaro^sla w postaci nagich ga^l^"+
             "ezi drzew i krzew^ow. Trakt jest bardzo zaniedbany, pe^lno t"+
             "u zastygni^etych w zmarzni^etej ziemi kolein i zasp na kraw^"+
             "edziach drogi. Trakt zosta^l dok^ladnie wydeptany przez setk"+
             "i przechodz^acych t^edy st^op i gdzieniegdzie tylko pokrywa "+
             "go kilka plam szarego ^sniegu. Pod ^sniegiem widniej^a zarys"+
             "y korzeni, kt^ore wyci^agn^e^ly si^e a^z na drog^e. ";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str+="Wzd^lu^z drogi ci^agn^a si^e zaro^sla rozstaczaj^ac s^lodk^a"+
             " wo^n wiosennego kwiecia. Trakt jest bardzo zaniedbany, pe^l"+
             "no tu kolein i rosn^acych prosto na drodze chwast^ow. Trakt "+
             "zosta^l dok^ladnie wydeptany przez setki przechodz^acych t^e"+
             "dy st^op i gdzieniegdzie tylko porasta go kilka k^ep m^lodej"+
             " trawy. Mi^edzy zieleniej^ac^a si^e traw^a widniej^a korzeni"+
             "e, kt^ore wyci^agn^e^ly si^e a^z na drog^e. ";
    }
    else if(pora_roku() == MT_LATO)
    {
        str+="Wzd^lu^z drogi ci^agn^a si^e g^este zaro^sla daj^ace w^edrow"+
             "com odrobin^e cienia w upalne, letnie dni. Trakt jest bardzo"+
             " zaniedbany, pe^lno tu kolein i rozplenionych na drodze chwa"+
             "st^ow. Trakt zosta^l dok^ladnie wydeptany przez setki przech"+
             "odz^acych t^edy st^op i gdzieniegdzie tylko porasta go kilka"+
             " k^ep niezbyt wysokiej trawy. Mi^edzy wybuja^l^a traw^a widn"+
             "iej^a korzenie, kt^ore wyci^agn^e^ly si^e a^z na drog^e. ";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str+="Wzd^lu^z drogi ci^agn^a si^e g^este zaro^sla o i^scie jesien"+
             "nych, ^zywych barwach. Trakt jest bardzo zaniedbany, pe^lno "+
             "tu kolein, li^sci i ga^l^azek na drodze. Trakt zosta^l dok^l"+
             "adnie wydeptany przez setki przechodz^acych t^edy st^op i gd"+
             "zieniegdzie tylko porasta go kilka k^ep zszarza^lej trawy. M"+
             "i^edzy opadni^etymi li^s^cmi widniej^a zarysy korzeni, kt^or"+
             "e wyci^agn^e^ly si^e a^z na drog^e. ";
    }

    str+="\n";
    return str;
}