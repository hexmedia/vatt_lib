/*
 * Opis oraz kod automatycznie wygenerowany za pomoca VattGen,
 * Data: Saturday 03rd of November 2007 12:39:14 PM
 */

#include <pogoda.h>
#include <stdproperties.h>
#include <mudtime.h>
#include "dir.h"

inherit PIANA_OKOLICE_TRAKT_STD;

void
create_trakt()
{
    set_short("Brukowany trakt");
    set_long("@@dlugi_opis@@");
    add_exit(PIANA_OKOLICE_TRAKT_LOKACJE + "trakt_05.c","w",0,FATIGUE,0);
    add_exit(PIANA_OKOLICE_TRAKT_LOKACJE + "trakt_07.c","ne",0,FATIGUE,0);
    add_prop(ROOM_I_INSIDE,0);

}

public string
exits_description()
{
    return "Trakt prowadzi na zach^od i p^o^lnocny wsch^od.\n";
}

string
dlugi_opis()
{
    string str = "";

    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="Zaro^sla ci^agn^ace si^e wzd^lu^z drogi zupe^lnie ogo^loci^l"+
             "y si^e z li^sci. Pola uprawne otaczaj^ace trakt, ci^agn^ace "+
             "sie prawie po horyzont, pokryte s^a gestym ^sniegiem gdzieni"+
             "egdzie tworz^acym zaspy. Trakt jest zadbany i od^snie^zony. "+
             "Na po^ludniu rozci^agaj^a si^e niezmierzone po^lacie p^ol, k"+
             "t^ore przykryte bia^lym ^sniegiem wygl^adaj^a jak tafla ogro"+
             "mnego jeziora, na kt^orym panuje ca^lkowita cisza i spok^oj,"+
             " m^acona od czasu do czasu przez mocniejszy podmuch zimnego "+
             "wiatru. ";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str+="G^este zaro^sla ci^agn^ace wzd^lu^z drogi zieleni^a si^e soc"+
             "zy^scie, przyci^agaj^ac wielobarwnymi kwiatami roje pszcz^o^"+
             "l i trzmieli. Pola uprawne otaczaj^ace trakt, ci^agn^ace sie"+
             " prawie po horyzont, pokryte s^a ju^z ^swie^zo zasian^a, kie"+
             "^lkuj^ac^a ro^slinno^sci^a. Trakt jest zadbany, a spomi^edzy"+
             " kostek usuni^eto wi^ekszo^s^c kwitn^acych chwast^ow. Na po^"+
             "ludniu rozci^agaj^a si^e niezmierzone po^lacie p^ol, kt^ore "+
             "b^ed^ac delikatnie zazielenione, s^a prawdziwym rajem dla ok"+
             "olicznych ptak^ow i zwierz^at, szukaj^acych jedzenia po zimo"+
             "wym ^snie. ";
    }
    else if(pora_roku() == MT_LATO)
    {
        str+="G^este zaro^sla ci^agn^ace wzd^lu^z drogi zieleni^a si^e soc"+
             "zy^scie, goszcz^ac wszelkiego rodzaju owady i zwierzyn^e. Po"+
             "la uprawne otaczaj^ace trakt, ci^agn^ace sie prawie po horyz"+
             "ont, pokryte s^a ju^z ^z^o^ltymi k^losami zb^o^z. Trakt jest"+
             " zadbany i tylko w niewielu miejscach spomi^edzy kostek wyra"+
             "staj^a nieliczne chwasty. Na po^ludniu rozci^agaj^a si^e nie"+
             "zmierzone po^lacie p^ol, kt^ore b^ed^ac pi^eknie zazielenion"+
             "e, s^a prawdziwym rajem dla okolicznych ptak^ow i zwierz^at "+
             "wydaj^acych piski, skrzeki i mruczenia s^lyszalne nawet z du"+
             "^zej odleg^lo^sci, dzi^eki delikatnemu ciep^lemu wietrzykowi"+
             ". ";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str+="Zaro^sla ci^agn^ace wzd^lu^z drogi urozmaicaj^a og^oln^a mon"+
             "otonni^e wszystkimi barwami z^lota, czerwieni i br^azu. Pola"+
             " uprawne otaczaj^ace trakt, ci^agn^ace sie prawie po horyzon"+
             "t, s^a br^azowe od nagiej ziemi. Trakt jest zadbany, cho^c g"+
             "^esto pokryty listowiem opadni^etym z drzew. Na po^ludniu ro"+
             "zci^agaj^a si^e niezmierzone po^lacie p^ol, nad kt^orymi snu"+
             "je si^e lekka mgie^lka nadaj^aca im do^s^c niepokoj^acy wygl"+
             "^ad, a lekki ch^lodny wietrzyk przywiewa zapachy pal^acych s"+
             "ie ognisk przy kt^orych ch^lopi ogrzewaj^a sie w przerwach o"+
             "d pracy. ";
    }

    str+="\n";
    return str;
}