/* Autor: Avard
   Opis : Aine
   Data : 15.01.08 */

inherit "/std/door";

#include <pl.h>
#include "dir.h"

void
create_door()
{
    ustaw_nazwe("drzwi"); 
    
    set_other_room(PIANA_BAGNO_LOKACJE + "bagno_13.c");
    set_door_id("DRZWI_DO_CHATKI_NA_BAGNACH_PIANY");
    set_door_desc("Ma^le, pomalowane na zielono drzwi spokojnie "+
        "komponuj^ace si^e w ciemny kolor drewnianych belek chatki. \n");

    set_open_desc("");
    set_closed_desc("");
        
    set_pass_command(({({"wyj^scie","bagno","drzwi"}),"przez drzwi",
        "wychodz^ac z chatki."}));

    set_open(0);
    set_locked(0);

    set_lock_name("zamek");
}