/* Autor: Avard
   Opis : Sniegulak
   Data : 28.04.07 */

inherit "/std/window";

#include <pl.h>
#include <macros.h>
#include "dir.h"

void
create_window()
{
    ustaw_nazwe("okno");
    dodaj_przym("brudny", "brudni");

    set_other_room(PIANA_BAGNO_CHATKA_LOKACJE + "chatka");
    set_window_id("OKNO_W_CHATCE_PUSTELNIKA_NA_BAGNACH_PIANY");
    set_open(0);
    set_locked(1);
    set_open_desc("");
    set_closed_desc("");
    set_window_desc("Zza zaparowanej szybki tego ma^lego okienka ledwo "+
        "mo^zna cokolwiek dostrzec. Jego drewniana rama, przybrudzona od "+
        "osadzaj^acej si^e sadzy z pieca, jest gdzieniegdzie u^lamana, "+
        "przez co wystaj^a z niej d^lugie drzazgi zmuszaj^ace do "+
        "zachowania ostro^zno^sci. Panuj^acy w chatce zaduch ^swiadczy "+
        "o tym, i^z okno to od dawna nie by^lo otwierane, a urwana klamka "+
        "w wymowny spos^ob sugeruje by dalej pozosta^lo zamkni^ete. \n");

    ustaw_pietro(0);

    set_pass_command("okno");
    set_pass_mess("przez niedu^ze okno na zewn^atrz");
}

