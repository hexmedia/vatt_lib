/* Autor: Avard
   Opis : Sniegulak
   Data : 28.04.07 */

inherit "/std/window";

#include <pl.h>
#include <macros.h>
#include "dir.h"

void
create_window()
{
    ustaw_nazwe("okno");
    dodaj_przym("brudny", "brudni");

    set_other_room(PIANA_BAGNO_LOKACJE + "bagno_13");
    set_window_id("OKNO_W_CHATCE_PUSTELNIKA_NA_BAGNACH_PIANY");
    set_open(0);
    set_locked(1);
    set_open_desc("");
    set_closed_desc("");
    set_window_desc("Jest to ma^le, ledwo widoczne spod opadaj^acej nisko "+
        "strzechy okienko. Jest ono ca^lkowicie zaparowane od ^srodka, "+
        "dlatego te^z nie mo^zesz nic przez nie dostrzec. Brud jaki "+
        "osadzi^l si^e na jego powierzchni ^swiadczy o tym, i^z "+
        "utrzymywanie porz^adku nie nale^zy do mocnej strony osoby "+
        "mieszkaj^acej w tej chatce.\n");

    ustaw_pietro(0);

    set_pass_command("okno");
    set_pass_mess("przez niedu^ze okno na zewn^atrz");
}

