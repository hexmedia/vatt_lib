/* Autor: Avard
   Data : 02.01.08
   Opis : Aine */

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <pogoda.h>
#include "dir.h"

inherit "/std/room.c";

void 
create_room()
{
    set_short("W chatce");
    set_long("@@dlugi_opis@@");
    add_prop(ROOM_I_INSIDE,1);
    add_object(PIANA_BAGNO_CHATKA_PRZEDMIOTY + "stol.c");
    add_object(PIANA_BAGNO_CHATKA_PRZEDMIOTY + "lezanka.c");
    add_object(PIANA_BAGNO_CHATKA_PRZEDMIOTY + "piec.c");
    add_door(PIANA_BAGNO_CHATKA_DRZWI + "z_chatki.c");
    add_window(PIANA_BAGNO_CHATKA_DRZWI + "okno_wewnatrz.c");
    dodaj_rzecz_niewyswietlana("ci^e^zki drewniany st^o^l");
    dodaj_rzecz_niewyswietlana("^smierdz^aca niewygodna le^zanka");
    dodaj_rzecz_niewyswietlana("ogromne kaflowe palenisko");
}
public string
exits_description() 
{
    return "Drzwi wyj^sciowe prowadz^a na zewn^atrz.\n";
}

string
dlugi_opis()
{
    string str;
    str = "Silny zapach popodwieszanych pod stropem wi^azek zi^o^l dra^zni "+
        "twoje nozdrza. Wn^etrze chatki jest bardzo ma^le i ciasne, a "+
        "panuj^acy w niej p^o^lmrok nadaje mu du^zo tajemniczo^sci. "+
        "Naprzeciwko drzwi, cicho w k^acie drzemie ogromne kaflowe "+
        "palenisko, w kt^orym przyjemnie trzeszczy muskane ogniem drewno. ";
    if(jest_rzecz_w_sublokacji(0,"^smierdz^aca niewygodna le^zanka"))
    {
        str += "Tu^z obok pieca na ziemi znajduje si^e stara, brudnawa "+
            "le^zanka, delikatnie przypruszona przez suche listki zi^o^l. ";
    }
    if(jest_rzecz_w_sublokacji(0,"ci^e^zki drewniany st^o^l"))
    {
        str += "Na samym ^srodku izby, pokryty kurzem stoi drewniany, "+
            "ogromny st^o^l, kt^orego nogi wyrze^xbione zosta^ly na wz^or "+
            "wrastaj^acych w ziemi^e korzeni. ";
    }
    str += "W pomieszczeniu panuje okropny gor^ac i duchota, a ma^le, "+
        "zaparowane i pokryte paj^eczyn^a okienka szczelnie nie "+
        "przepuszczaj^a ani ksztyny ^swie^zego powietrza.\n";    
    return str;
}
