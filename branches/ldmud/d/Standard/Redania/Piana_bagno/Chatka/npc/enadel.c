/* Opis (swietny do recytacji) i scenariusz questa: Aine
 * Kod: Cega, Vera
 *
 * Sprobujmyz rozpisac sobie zadanie na poszczegolne checkpointy:
 * 0. start! ruszamy na poszukiwania Rieny
 * 1. wyciagniecie informacji z burmistrza
 *      - mo�emy te� zapyta� sklepikarza o rien�, ale za to nie ma punkt�w.
 * 2. otrzymanie zlecenia od rybiary...
 * 3. ...i dostarczenie jej tych cholernych ryb. tu Dora mowi nam straszna
prawde!
 * 4. opowiedzenie calej historii Enadelowi
 */

#pragma strict_types

inherit "/std/humanoid.c";

#include <macros.h>
#include <stdproperties.h>
#include <exp.h>
#include <ss_types.h>
#include "dir.h"

void
create_humanoid()
{
    ustaw_odmiane_rasy(PL_MEZCZYZNA);

    set_gender(G_MALE);

    dodaj_przym("stary", "starzy");
    dodaj_przym("ponury", "ponurzy");

    ustaw_imie( ({"enadel", "enadela", "enadelowi",
          "enadela", "enadelem", "enadelu"}), PL_MESKI_OS);

    set_long("Oczom twoim ukazuje sie stary, ponury m^e^zczyzna. " +
        "Czas wyra^xnie odbi^l pi^etno na twarzy tego cz^lowieka. " +
        "Liczne zmarszczki niemal ca^lkowicie j^a przys^laniaj^a, " +
        "sprawiaj^ac, i^z jego wiek jest niemal nie do odgadni^ecia. " +
        "Przygarbione plecy, lekko ugi^ete kolana sprawiaj^a, ^ze " +
        "cz^lowiek ten wydaje si^e kurczy^c sam w sobie, jakby " +
        "chcia^l uciec w siebie przed otaczaj^ac^a go " +
        "rzeczywisto^sci^a. Na jego plecy opada kaskada siwych, " +
        "d^lugich i l^sni^acych niczym ksie^zycowa po^swiata " +
        "w^los^ow. Spod ich kurtyny, kt^ore tak^ze przys^laniaj^a " +
        "cz^e^s^c twarzy m^e^zczyzny co chwil^e wida^c jego oczy " +
        " - zielone niczym ^swie^za, wiosenna trawa, kt^ore wydaj^a " +
        "si^e jako jedyne nie ulec okrutnie p^edz^acemu czasu, a na " +
        "kt^orych dnie mo^zna dostrzec ogromny smutek i t^esknot^e " +
        "za czym^s, co zosta^lo ju^z tylko cichym echem jego " +
        "wspomnie^n.\n");

    set_stats( ({ 65, 31, 70, 25, 58 }) );

    set_chat_time(67);
    add_chat(":z t^esknot^a w g^losie: Gdybym tylko m^og^l cofn^a^c " +
        "czas...");
    add_chat(":cicho, jakby do siebie: Dlaczego ci^e nie by^lo... Dlaczego...");

    set_act_time(139);
    add_act("emote dorzuca drwa do ognia.");
    add_act("emote podwiesza pod sufitem wi^azk^e suszonych " +
        "zi^o^l.");
    add_act("emote kulej^ac podchodzi do okna, przeciera " +
        "d^loni^a szyb^e i spogl^ada gdzie^s nieobecnym " +
        "wzrokiem.");
    add_act("emote wzdycha bezg^lo^snie.");

    add_prop(CONT_I_WEIGHT, 92000);
    add_prop(CONT_I_HEIGHT, 172);

    set_default_answer(VBFC_ME("default_answer"));
    add_ask("pomoc", VBFC_ME("pyt_pomoc"));
    add_ask("kobiet^e", VBFC_ME("pyt_kobieta"));
    add_ask("rien^e", VBFC_ME("pyt_riena"));
}

void
gdy_jest(object kto, string co)
{
    if (environment(TO) == environment(kto) && CAN_SEE(TO, kto))
        command(co);
}

string
default_answer()
{
    if(!query_attack())
    {
        set_alarm(1.0, 0.0, "gdy_jest", TP, "powiedz do " + OB_NAME(TP) +
         " Nie mam czasu, nie widzisz?");
    }
    else
    {
        set_alarm(0.5,0.0,"gdy_jest", TP, "'Zostaw mnie!");
    }
    return "";
}

string
pyt_pomoc()
{
    if(!query_attack())
    {
        set_alarm(1.5, 0.0, "gdy_jest", TP, "powiedz do " + OB_NAME(TP) +
        " Ty mnie pytasz o pomoc? Widzisz, od czasu kiedy ona... A z " +
        "reszt^a. Nie m^owmy o tej kobiecie.");
        TP->add_prop("zapytal_enadela_o_pomoc_okurwa", 1);
    }
    else
    {
        set_alarm(0.5,0.0,"gdy_jest", TP, "'Zostaw mnie!");
    }
    return "";
}

string
pyt_riena()
{
    if (TP->query_prop("zapytal_enadela_o_kobiete_okurwa") == 0)
    {
        set_alarm(1.5, 0.0, "command", "zmarszcz brwi gniewnie");
        set_alarm(2.5, 0.0, "gdy_jest", TP, "powiedz do " + OB_NAME(TP) +
        " Sk^ad o niej wiesz?");
        return "";
    }

    set_alarm(1.5, 0.0, "gdy_jest", TP, "powiedz :spokojnie:" +
    " C^o^z, kocha^lem j^a. Ca^lym sercem. To by^lo lata temu, a "+
        "ja pomimo up^lywu " +
    "czasu nie mog^e o niej zapomnie^c. Wszystko zacz^e^lo si^e w "+
        "Bia^lym Mo^scie.");
    set_alarm(10.0, 0.0, "command", "emote zak^lada z namys^lem "+
        "w^losy za ucho.");
    set_alarm(13.0, 0.0, "gdy_jest", TP, "powiedz Ja by^lem tam zwyk^lym " +
    "zielarzem, klepa^lem straszn^a bied^e!");
    set_alarm(16.5, 0.0, "command", "usmiech nieznacznie do " + OB_NAME(TP));
    set_alarm(17.0, 0.0, "gdy_jest", TP, "powiedz :cicho: Lecz jej nigdy to " +
    "nie przeszkadza^lo...");
    set_alarm(19.0, 0.0, "gdy_jest", TP, "westchnij .");
    set_alarm(20.5, 0.0, "gdy_jest", TP, "powiedz do " + OB_NAME(TP) +
    " Riena by^la c^ork^a burmistrza. Jednak on nie rozumia^l i nie "+
    "akceptowa^l naszej mi^lo^sci.");
    set_alarm(27.0, 0.0, "gdy_jest", TP, "powiedz :wolno: Chcieli^smy "+
        "uciec. "+
    "Nie liczy^lo si^e dok^ad, liczy^lo si^e tylko to, ^ze b^edziemy razem. " +
    "Um^owili^smy si^e przy p^o^lnocnej bramie miasta. Noc mia^la by^c "+
        "nasz^a "+
    "przyjaci^o^lk^a, os^laniaj^ac^a nas bezpiecznie swoj^a ciemno^sci^a.");
    set_alarm(30.0, 0.0, "gdy_jest", TP, "emote milknie na chwil^e, a jego " +
    "wzrok staje si^e matowy i nieobecny.");
    set_alarm(36.0, 0.0, "gdy_jest", TP, "powiedz :kr^otko: Nie przysz^la");
    set_alarm(39.0, 0.0, "gdy_jest", TP, "powiedz :beznami^etnie: Czeka^lem " +
    "tam... Sam ju^z nie pami^etam ile. Czas wtedy jakby zamar^l. Gdy " +
    "zobaczy^lem w oddali sylwetk^e p^edz^ac^a na koniu w moj^a stron^e, " +
    "serce zabi^lo mi mocniej. Kiedy jednak w sylwetce nie rozpoznalem " +
    "Rieny ale pos^la^nca wiedzia^lem ju^z, ^ze to mocniejsze bicie serca " +
    "by^lo r^ownocze^snie ostatnim...");
    set_alarm(53.0, 0.0, "gdy_jest", TP, "powiedz Zamiast wsp^olnego " +
    "^zycia da^la mi to");
    set_alarm(55.0, 0.0, "gdy_jest", TP, "emote powoli wyjmuje kawa^lek "+
        "starej, mocno ju^z zniszczonej kartki."); //skad, cholera?
    set_alarm(57.0, 0.0, "gdy_jest", TP, "powiedz \"Enadelu... To wszystko "+
        "nie ma " +
    "najmniejszego sensu. To wszystko, co do ciebie czu^lam, by^lo jedynie " +
    "szczeniackim zauroczeniem. Nie czekaj na mnie i ^zegnaj...\"");
    set_alarm(60.0, 0.0, "gdy_jest", TP, "emote ostro^znie chowa list do " +
    "kieszeni, jakby by^l jego najwi^ekszym skarbem.");
    set_alarm(65.0, 0.0, "gdy_jest", TP, "powiedz :cicho:" +
    " Odda^lbym wszystko, by dowiedzie^c si^e co odmieni^lo jej uczucia");
    set_alarm(67.0, 0.0, "gdy_jest", TP, "popatrz wymownie na " + OB_NAME(TP));
    set_alarm(70.0, 0.0, "gdy_jest", TP, "powiedz do " + OB_NAME(TP) +
    " Jednak wszystko co mam to par^e monet i wiedza na temat sekret^ow " +
    "natury. Je^sli dalej pragniesz mi pom^oc... Id^x. Ja od wielu lat nie " +
    "mam ju^z odwagi ani si^ly opuszcza^c mojej samotni");
    set_alarm(70.0, 0.0, "nowy_robiacy", TP);
}


string
pyt_kobieta()
{
    if(!query_attack())
    {
        if (TP->add_prop("zapytal_enadela_o_kobiete_okurwa"))
        {
            pyt_riena();
            return "";
        }

        if (TP->query_prop("zapytal_enadela_o_pomoc_okurwa"))
        {
            set_alarm(1.0, 0.0, "command", "popatrz dlugo na " + OB_NAME(TP));
            set_alarm(3.5, 0.0, "gdy_jest", TP, "powiedz do " + OB_NAME(TP) +
            " Widzisz... ^Zycie nie zawsze uk^lada nasze ^scie^zki tak, " +
            "jak sami by^smy tego chcieli. Riena by^la moj^a... Nie, "+
            "niewa^zne");
            TP->add_prop("zapytal_enadela_o_kobiete_okurwa", 1);
        }
        else
            set_alarm(1.5, 0.0, "gdy_jest", TP, "powiedz do " + OB_NAME(TP) +
            " Co? Jaka kobieta?");
    }
    else
    {
        set_alarm(0.5,0.0,"gdy_jest", TP, "'Zostaw mnie!");
    }
    return "";
}

void
emote_hook(string emote, object wykonujacy)
{
    switch (emote)
    {
        case "kopnij": set_alarm(1.0, 0.0, "zle", wykonujacy);
                    break;
        case "spoliczkuj": set_alarm(1.0, 0.0, "zle", wykonujacy);
                    break;
        case "opluj" : set_alarm(1.0, 0.0, "zle", wykonujacy);
                    break;
        case "szturchnij" : set_alarm(1.0, 0.0, "zle", wykonujacy);
                    break;
        case "nadepnij" : set_alarm(1.0, 0.0, "zle", wykonujacy);
                    break;
        case "splun" : set_alarm(1.0, 0.0, "zle", wykonujacy);
                    break;
        case "poca^luj": set_alarm(1.0,0.0, "calusne",wykonujacy);
                    break;
        case "przytul": set_alarm(1.0,0.0, "calusne",wykonujacy);
                    break;
        case "poglaszcz": set_alarm(1.0, 0.0, "calusne", wykonujacy);
                    break;
        case "u^sciskaj": set_alarm(1.0, 0.0, "calusne", wykonujacy);
                    break;
        case "poklep": set_alarm(1.0, 0.0, "dobre", wykonujacy);
                    break;
        case "usmiech": set_alarm(1.0, 0.0, "dobre", wykonujacy);
                    break;
        case "mrugnij": set_alarm(1.0, 0.0, "dobre", wykonujacy);
                    break;
        case "oczko": set_alarm(1.0, 0.0, "dobre", wykonujacy);
                    break;
    }
}

void
zle(object wykonujacy)
{
    command("':odsuwaj^ac si^e gwa^ltownie: Hej^ze! C^o^z ci jestem winien?!");
}

void
dobre(object kto)
{
    command("zmarszcz brwi . na widok " + OB_NAME(kto));
}

void
calusne(object kto)
{
    switch (random(3))
    {
        case 0: gdy_jest(kto, "':do " + OB_NAME(kto) + " spogl^adaj^ac " +
            "na^n dziwnie: C^o^z, takie czu^lo^sci pozostaw dla osoby " +
            "tobie bliskiej...");
            break;
        case 1: gdy_jest(kto, "':do " + OB_NAME(kto) + " szybko: Hola, " +
            "z daleka ode mnie z takimi czu^lo^sciami!");
            break;
        case 2: command("popatrz z odraza na " + OB_NAME(kto));
            break;
    }

    if (kto->query_gender() == 0)
    {
        set_alarm(5.0, 0.0, "command", "splun");
        set_alarm(6.5, 0.0, "gdy_jest", kto, "':do " + OB_NAME(kto) +
            " z przek^asem: " +
        "Dobrze... Mo^ze chcesz o tym porozmawia^c? Mo^ze jako^s ci pom^oc? " +
        "Zdaje si^e, ze mam tu gdzie^s odpowiednie zio^la na "+
        "twoj^a dolegliwo^s^c...");
    }
}

// to bedzie wywolywane z enter_inv chatki
void
przylazl(object kto)
{
    string *cp, *x;
    cp = explode(read_file(ZIELARZ_ROBIACY), "#");

    if (CAN_SEE(TO, kto) && calling_function() == "enter_inv" && interactive(kto))
    {
        if (member_array(lower_case(TP->query_name()), cp) != -1)
        {
            set_alarm(1.0, 0.0, "powiedz_gdy_jest", kto, "popatrz nagle na " +
                OB_NAME(kto));

    if (explode(cp[member_array(lower_case(TP->query_name()), cp) + 1],
             "")[2] != "0")
    {
            set_alarm(3.0, 0.0, "powiedz_gdy_jest", kto, "powiedz do " +
                OB_NAME(kto) + " Czy wiesz co^s o Rienie? Opowiedz mi "+
                "o niej, szybko!");
    }
    else
            set_alarm(3.0, 0.0, "powiedz_gdy_jest", kto, "powiedz do " +
                OB_NAME(kto) + " Czy masz dla mnie jakie^s wie^sci?");
        }
        else
            set_alarm(1.5, 0.0, "powiedz_gdy_jest", kto, "powiedz :do " +
                OB_NAME(kto) + " przez zaci^sni^ete wargi: " +
                "Odejd^x! Dlaczego zak^l^ocasz m^oj spok^oj?!");
    }
}

//////////////////////////////////////////////////////////////////////////////

void
wmsg(string msg)
{
    if (TP->query_wiz_level())
        TP->catch_msg(msg);
}

void
checkpoint(int ktory)
{
    string *cp, *x;

    cp = explode(read_file(ZIELARZ_ROBIACY), "#");

    if (ktory < 1 || ktory >
            sizeof(explode(cp[member_array(lower_case(TP->query_name()),
                cp) + 1], "")))
        return;

    if (member_array(lower_case(TP->query_name()), cp) != -1 &&
        explode(cp[member_array(lower_case(TP->query_name()), cp) + 1],
            "")[ktory - 1] == "0")
    {
        x = explode(cp[member_array(lower_case(TP->query_name()), cp) + 1],
                    "");
        x[ktory - 1] = "1";
        cp[member_array(lower_case(TP->query_name()), cp) + 1] = implode(x,
            "");
        write_bytes(ZIELARZ_ROBIACY, 0, implode(cp, "#"));
        wmsg(set_color(42) + "***CHECKPOINT " + ktory + "***\n" +
                clear_color());
    }
}

int
nowy_robiacy(object kto)
{
    if (ENV(TO)==ENV(kto))
    {
        write_file(ZIELARZ_ROBIACY, lower_case(kto->query_name()) + "#000#");
        wmsg(set_color(42) + "***CHECKPOINT 0***\n" + clear_color());
        return 1;
    }
    else
        return 0;
}

void
usun_robiacego(object kto)
{
    int i;
    string *arr;

    arr = explode(read_file(ZIELARZ_ROBIACY), "#");
    i = member_array(lower_case(kto->query_name()), arr);

    if (i != -1)
        arr = exclude_array(arr, i, i+1);

    rm(ZIELARZ_ROBIACY);
    write_file(ZIELARZ_ROBIACY, implode(arr, "#") + "#");
}

void
exp_dla(object kto, int ile)
{
    /* Komentarz: zmienna ile przyjmuje wartosci calkowite do 3, wskazujac
     * ile z mozliwych punktow spelnil gracz. W przypadku wzorowo wykonane-
     * go questa nagroda jest 6000 punktow (3*EXP_QUEST_ENADEL)
     */
    kto->increase_ss(SS_INT, ile*EXP_QUEST_ENADEL);

    TP->catch_msg(QCIMIE(TO, 0) + " uczy ci^e blabla.");
}

int
opowiedz(string str)
{
    string komu, oczym, *cp, *x;
    object obkomu;
    int y = 0;

    if (member_array(lower_case(TP->query_name(0)),
        explode(read_file(ZIELARZ_ROBIACY),"#")) == -1)
    {
        notify_fail("S^lucham?\n");
        return 0;
    }

    if (!str || str == "")
    {
        notify_fail("Komu i o czym chcesz opowiedzie^c?\n");
        return 0;
    }

    if (sscanf(str, "%s o %s", komu, oczym) != 2)
    {
        notify_fail("Komu i o czym chcesz opowiedzie^c?\n");
        return 0;
    }

    if (!parse_command(komu, all_inventory(ENV(TP)), "%o:2", obkomu))
    {
        notify_fail("Komu i o czym chcesz opowiedzie^c?\n");
        return 0;
    }

    if (obkomu != TO)
    {
        notify_fail("Komu i o czym chcesz opowiedzie^c?\n");
        return 0;
    }

    oczym = LC(oczym);
    saybb(QCIMIE(TP, 0) + " opowiada " + QIMIE(obkomu, 2) + " o czym^s.\n");

    if (oczym ~= "rienie" ||
        oczym ~= "losach rieny" ||
        oczym ~= "losie rieny" ||
        oczym ~= "^smierci rieny")
    {
        cp = explode(read_file(ZIELARZ_ROBIACY), "#");
        if (member_array(lower_case(TP->query_name()), cp)!= -1)
        {
            switch (explode(cp[member_array(lower_case(TP->query_name()),
                    cp) + 1], "")[2])
            {
                case "0":
                    notify_fail("Nie dowiedzia^l" +
                        TP->koncowka("e", "a", "o")+
                    "^s si^e niczego nowego na temat Rieny.\n");
                    return 0;
                    break;
                case "X":
                case "1":

                x = explode(cp[member_array(lower_case(TP->query_name()),
                     cp) + 1], "");
                for (int i = 0; i < sizeof(x); i++)
                {
                    if (x[i] == "1")
                        y++;
                }

                write_file(ZIELARZ_ZROBILI, lower_case(TP->query_name()) + "#");
                wmsg(set_color(42) + "***FIN***\n" + clear_color());
                TP->catch_msg("Opowiadasz " + QIMIE(obkomu, 2) +
                " histori^e Rieny.\n");
                set_alarm(3.0, 0.0, "command", "zwies glowe .");
                set_alarm(8.0, 0.0, "powiedz_gdy_jest", TP, "powiedz do " +
                OB_NAME(TP) + " Niczego ju^z nie zmieni^e. Nie by^lo nam "+
                            "dane " +
                "zazna^c szcz^e^scia... Ale dzi^ekuj^e " +
                "ci. Teraz moja kolej.");
                set_alarm(10.5, 0.0, "exp_dla", TP, y);
                usun_robiacego(TP);
                return 1;
                break;
            }
        }
    }
    else
    {
        TP->catch_msg("Opowiadasz " + QIMIE(obkomu, 2) + " o czym^s.\n");
        set_alarm(3.0, 0.0, "command", "wzrusz ramionami");
        set_alarm(5.0, 0.0, "powiedz_gdy_jest", TP, "powiedz do " +
            OB_NAME(TP) + " O Rienie, chc^e dowiedzie^c si^e czego^s "+
                "o Rienie!");
        return 1;
    }
}

void
init()
{
    ::init();
    add_action("opowiedz", "opowiedz");
}
