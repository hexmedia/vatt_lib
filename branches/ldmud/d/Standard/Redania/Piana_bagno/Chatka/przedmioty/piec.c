
// kod popelniony przez Seda, opisy popelnione przez Aine 
// 04.01.2008

inherit "/std/object.c";

#include <pl.h>
#include <stdproperties.h>
#include <macros.h>
#include <filter_funs.h>
#include <materialy.h>
#include <object_types.h>

void
create_object()
{
    dodaj_przym("ogromny","ogromni");
    dodaj_przym("kaflowy","kaflowi");
    ustaw_nazwe("palenisko");

    set_long("Ogromny kaflowy piec drzemie spokojnie w k^acie chatki. "
    +"Co jaki^s czas z jego wn^etrza wydobywa si^e przyjemne trzaskanie "
    +"muskaj^acego przez jezyki ognia drewna. G^ladkie kafle "
    +"pokrywa t^lusta para, kt^ora bacznie przys^lania ich kolor. "
    +"Gor^ac bij^acy od paleniska sprawia, i^z w pomieszczeniu jest "
    +"niezwykle duszno.\n");
               
   add_prop(OBJ_I_WEIGHT, 500000);
   add_prop(OBJ_I_VOLUME, 50000);
   add_prop(OBJ_I_VALUE, 70);

   ustaw_material(MATERIALY_RZ_GLINA, 100);

   set_type(O_MEBLE); 
}

