/* Autor: Sed
   Opis : Aine
   Data : 03.01.2008 */

inherit "/std/object.c";

#include <pl.h>
#include <stdproperties.h>
#include <macros.h>
#include <filter_funs.h>
#include <materialy.h>
#include <object_types.h>

void
create_object()
{
   ustaw_nazwe("le^zanka");
   dodaj_przym("^smierdz^acy","^smierdz^acy");
   dodaj_przym("niewygodny","niewygodni");

   make_me_sitable(({"na"}), "na le^zance", "z le^zanki", 2);

   set_long("Jest to nic innego jak wypchany sianem, ogromny w^or "
   +"przyrzucony paroma szarymi kocami. Intensywny zapach "
   +"potu i zi^o^l przeszy^l j^a na wylot sprawiaj^ac, i^z le^zenie "
   +"na niej nie nale^zy do najprzyjemniejszych rzeczy. \n");

   add_prop(OBJ_I_WEIGHT, 5000);
   add_prop(OBJ_I_VOLUME, 5000);
   add_prop(OBJ_I_VALUE, 5);

   ustaw_material(MATERIALY_SLOMA, 85);
   ustaw_material(MATERIALY_TK_WELNA, 10);
   ustaw_material(MATERIALY_LEN, 5);

   set_type(O_MEBLE);
}

