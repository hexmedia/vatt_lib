#include <pogoda.h>
#include <stdproperties.h>
#include <mudtime.h>
#include <macros.h>
#include "dir.h"
inherit PIANA_BAGNO_STD;

void
create_bagno()
{
    set_short("Cuchn^ace bagnisko");
    set_long("@@dlugi_opis@@");
    add_exit(PIANA_BAGNO_LOKACJE + "bagno_09.c","n",0,FATIGUE,0);
    add_exit(PIANA_BAGNO_LOKACJE + "bagno_12.c","w",0,FATIGUE,0);
    add_exit(PIANA_BAGNO_LOKACJE + "bagno_14.c","e",0,FATIGUE,0);
    add_exit(PIANA_BAGNO_LOKACJE + "bagno_16.c","sw",0,FATIGUE,0);
    add_exit(PIANA_BAGNO_LOKACJE + "bagno_17.c","se",0,FATIGUE,0);
    add_door(PIANA_BAGNO_CHATKA_DRZWI + "do_chatki");
    add_window(PIANA_BAGNO_CHATKA_DRZWI + "okno_zewnatrz.c");

    add_item(({"chatk^e","domek","dom","chat^e"}),"S^lomiana strzecha "+
        "niemal ca^lkowicie przys^lania okna tej starej, nieco zaniedbanej "+
        "chatki. Pi^ekne warkocze mszenia uszczelniaj^ace przerwy mi^edzy "+
        "grubymi balami drewna przykry^l ju^z czas sprawiaj^ac, i^z teraz "+
        "s^a one nieco mniej wyra^xne i tylko wprawne oko mo^ze dostrzec "+
        "spod brudu ich misterne wykonanie. Do ^srodka chatki prowadz^a "+
        "ma^le, pomalowane na zielono drzwi, na kt^orych powierzchni "+
        "wyrze^xbiono posta^c kobiety stoj^acej na wzg^orzu. Po obu "+
        "stronach drzwi zwisaj^a grube p^eki jakich^s nieznanych zio^l, "+
        "kt^ore bez wzgl^edu na por^e roku otulaj^a chatk^e swoim "+
        "intensywnym i tajemniczym zapachem. Tu^z nad dachem widnieje "+
        "ma^ly, kamienny komin z kt^orego leniwie wydobywa si^e szarawy "+
        "dym, kt^ory pod wp^lywem cie^zkiego powietrza osiada delikatnie "+
        "nad brudn^a, bagienn^a ziemi^a,chc^ac jakby schowa^c domek w tym "+
        "ponurym krajobrazie.\n");

    add_prop(ROOM_I_INSIDE,0);
    
}


public string
exits_description()
{
    return "Ledwno widoczna dru^zka prowadzi na wsch^od, po^ludnie i "+
    "po^ludniowy-wsch^od, znajduje si^e tutaj tak^ze wej^scie do chatki.\n";
}

string
dlugi_opis()
{
    string str = "";
    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="Cisza i spok^oj jakie wprowadzila panuj^aca tu zima co "+
            "chwil^e zostaj^a zm^acona przez przera^xliwy krzyk jakiego^s "+
            "zb^l^akanego ptaka. Z ka^zdym mocniejszym podmuchem wiatru "+
            "^snieg dostaje si^e za twoje ubranie, dra^zni^ac i wywo^luj^ac "+
            "nieprzyjemny dreszcz. Stare konary drzew, kt^ore niegdy^s "+
            "mog^ly zachwyca^c swoim ogromem, teraz stoj^a pojedynczo i "+
            "spogladaj^a na rozleg^le tereny dooko^la, niczym zakl^eci, "+
            "u^spieni stra^znicy. Mi^ekkie pod^lo^ze w zdradliwy spos^ob "+
            "prowadzi twoje kroki mi^edzy martwymi, przegnitymi krzewami, "+
            "kt^orych cienie rozciagaj^a si^e na ziemi w zawi^lych, na "+
            "sw^oj spos^ob groteskowych mozaikach. Z panuj^acego tu mroku, "+
            "gdzie^s mi^edzy konarami wy^lania si^e ma^la, drewniana chatka.";

    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str+="B^loto z ka^zdym krokiem wydaje si^e by^c coraz to bardziej "+
            "grz^askie, i w niemi^losierny spos^ob spowalnia ka^zdy tw^oj "+
            "ruch. G^luchym chl^upni^eciom twoich krok^ow wt^oruj^a "+
            "pokrzykiwania jakich^s wiosennych ptak^ow, kt^ore niefortunnie "+
            "trafi^ly w ten martwy, cuchn^acy krajobraz. Stare konary "+
            "drzew, kt^ore niegdy^s mog^ly zachwyca^c swoim ogromem, teraz "+
            "stoj^a pojedynczo i spogladaj^a na rozleg^le tereny dooko^la, "+
            "niczym zakl^eci, u^spieni stra^znicy. Mi^ekkie pod^lo^ze w "+
            "zdradliwy spos^ob prowadzi twoje kroki mi^edzy martwymi, "+
            "przegnitymi krzewami, kt^orych cienie rozciagaj^a si^e na "+
            "ziemi w zawi^lych, na sw^oj spos^ob groteskowych mozaikach. "+
            "Z panuj^acego tu mroku, gdzie^s mi^edzy konarami wy^lania si^e "+
            "ma^la, drewniana chatka.  ";

    }
    else if(pora_roku() == MT_LATO)
    {
        str+="B^loto z ka^zdym krokiem wydaje si^e by^c coraz to "+
            "bardziej grz^askie, i w niemi^losierny spos^ob spowalnia "+
            "ka^zdy tw^oj ruch. G^luchym chl^upni^eciom twoich krok^ow "+
            "wt^oruj^a pokrzykiwania jakich^s ptak^ow, kt^ore niefortunnie "+
            "trafi^ly w ten martwy, cuchn^acy krajobraz. Stare konary "+
            "drzew, kt^ore niegdy^s mog^ly zachwyca^c swoim ogromem, teraz "+
            "stoj^a pojedynczo i spogladaj^a na rozleg^le tereny dooko^la, "+
            "niczym zakl^eci, u^spieni stra^znicy. Mi^ekkie pod^lo^ze w "+
            "zdradliwy spos^ob prowadzi twoje kroki mi^edzy martwymi, "+
            "przegnitymi krzewami, kt^orych cienie rozciagaj^a si^e na "+
            "ziemi w zawi^lych, na sw^oj spos^ob groteskowych mozaikach. "+
            "Z panuj^acego tu mroku, gdzie^s miedzy konarami wy^lania si^e "+
            "ma^la, drewniana chatka.  ";

    }
    else //jesien i zima gdy nie ma sniegu
    {
        str+="B^loto z ka^zdym krokiem wydaje si^e by^c coraz to "+
            "bardziej grz^askie, i w niemi^losierny spos^ob spowalnia "+
            "ka^zdy tw^oj ruch. G^luchym chl^upni^eciom twoich krok^ow "+
            "wt^oruj^a pokrzykiwania jakich^s ptak^ow, kt^ore niefortunnie "+
            "trafi^ly w ten martwy, cuchn^acy krajobraz. Stare konary "+
            "drzew, kt^ore niegdy^s mog^ly zachwyca^c swoim ogromem, teraz "+
            "stoj^a pojedynczo i spogladaj^a na rozleg^le tereny dooko^la, "+
            "niczym zakl^eci, u^spieni stra^znicy. Mi^ekkie pod^lo^ze w "+
            "zdradliwy spos^ob prowadzi twoje kroki mi^edzy martwymi, "+
            "przegnitymi krzewami, kt^orych cienie rozciagaj^a si^e na "+
            "ziemi w zawi^lych, na sw^oj spos^ob groteskowych "+
            "mozaikach. Z panuj^acego tu mroku, gdzie^s miedzy konarami "+
            "wy^lania si^e ma^la, drewniana chatka.  ";
    }
    str+="\n";
    return str;
}    