#include <pogoda.h>
#include <stdproperties.h>
#include <mudtime.h>
#include <macros.h>
#include "dir.h"
inherit PIANA_BAGNO_STD;

void
create_bagno()
{
    set_short("Cuchn^ace bagnisko");
    set_long("@@dlugi_opis@@");
    add_exit(PIANA_BAGNO_LOKACJE + "bagno_11.c","ne",0,FATIGUE,0);
    add_exit(PIANA_BAGNO_LOKACJE + "bagno_17.c","sw",0,FATIGUE,0);
    add_exit(PIANA_BAGNO_LOKACJE + "bagno_18.c","s",0,FATIGUE,0);
    add_exit(PIANA_OKOLICE_TRAKT_LOKACJE + "droga_05.c","e",0,FATIGUE,0);
    add_exit(PIANA_OKOLICE_TRAKT_LOKACJE + "droga_04.c","se",0,FATIGUE,0);
    add_npc(PIANA_BAGNO_LIVINGI + "los");  

    add_prop(ROOM_I_INSIDE,0);
    
}


public string
exits_description()
{
    return "Ledwo widoczna dr^o^zka prowadzi na p^o^lnocny-wch^od,"+
    " po^ludniowy-zach^od oraz na po^ludnie. Mo^zesz tak^ze dostrzec"+
    " wydeptan^a scie^zk^e na wschodzie i na po^ludniowym wschodzie.\n";
}

string
dlugi_opis()
{
    string str = "";

    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="Zmro^zona zimowym ch^lodem ziemia w tej cz^e^sci bagien wydaje si^e by^c"+ 
        " pewniejszym gruntem, jednak z ka^zdym krokiem nale^zy zachowa^c ostro^zno^s^c."+
        " Okryte ^snie^zn^a pierzyn^a bagno jest jakby nieco u^spione."+
        " Smr^od w tym miejscu nieco ust^api^l daj^ac mo^zliwo^s^c zaczerpni^ecia"+
        " pe^lniejszego oddechu bez nara^zania si^e na nudno^sci. Cienie padaj^ace z"+
        " pojedy^nczych, martwo wystaj^acych z ziemi pni igraj^a z Twoim wzrokiem..."+
        " Drzewa i krzewy sk^apane szaro^sci^a pochylaj^a si^e nad sm^etn^a, grz^ask^a"+
        " ziemi^a w ge^scie rozpaczy lub te^z politowania. ";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str+="Nieco r^owniejsza i mniej b^lotnista ziemia w tej cz^e^sci bagien wydaje"+
        " si^e by^c pewniejszym gruntem, jednak z ka^zdym krokiem nale^zy zachowa^c"+ 
        " ostro^zno^s^c. Budz^aca si^e do ^zycia ro^slinno^s^c bynajmniej nie doda^la uroku"+
        " temu miejscu. Smr^od nieco ust^api^l daj^ac mo^zliwo^s^c zaczerpni^ecia pe^lniejszego oddechu"+
        " bez nara^zania si^e na nudno^sci. Cienie padaj^ace z pojedy^nczych, martwo wystaj^acych"+
        " z ziemi pni igraj^a z twoim wzrokiem... Drzewa i krzewy sk^apane szaro^sci^a pochylaj^a"+
        " si^e nad sm^etn^a, grz^ask^a ziemi^a w ge^scie rozpaczy lub te^z politowania. ";
    }
    else if(pora_roku() == MT_LATO)
    {
        str+="Nieco r^owniejsza i mniej b^lotnista ziemia w tej cz^e^sci bagien wydaje"+
        " si^e by^c pewniejszym gruntem, jednak z ka^zdym krokiem nale^zy zachowa^c"+
        " ostro^zno^s^c. Ledwo widoczna, ^zywa ro^slinno^s^c bynajmniej nie doda^la uroku"+
        " i ^swie^zo^sci miejscu, w kt^orym si^e znajdujesz. Na szcz^e^scie smr^od"+
        " nieco ust^api^l daj^ac mo^zliwo^s^c zaczerpni^ecia pe^lniejszego oddechu"+
        " bez nara^zania si^e na nudno^sci. Cienie padaj^ace z pojedy^nczych, martwo wystaj^acych"+
        " z ziemi pni igraj^a z twoim wzrokiem... Drzewa i krzewy sk^apane szaro^sci^a pochylaj^a"+
        " si^e nad sm^etn^a, grz^ask^a ziemi^a w ge^scie rozpaczy lub te^z politowania. ";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str+="Nieco r^owniejsza i mniej b^lotnista ziemia w tej cz^e^sci bagien wydaje"+
        " si^e by^c pewniejszym gruntem, jednak z ka^zdym krokiem nale^zy zachowa^c"+ 
        " ostro^zno^s^c, poniewa^z rozk^ladaj^ace si^e, namokni^ete li^scie i trawy uczyni^ly"+
        " j^a ^slisk^a. Na szcz^e^scie smr^od w tym miejscu nieco ust^api^l daj^ac mo^zliwo^s^c"+
        " zaczerpni^ecia pe^lniejszego oddechu bez nara^zania si^e na nudno^sci. Cienie padaj^ace"+
        " z pojedy^nczych, martwo wystaj^acych z ziemi pni igraj^a z twoim wzrokiem... Drzewa"+
        " i krzewy sk^apane szaro^sci^a pochylaj^a si^e nad sm^etn^a, grz^ask^a ziemi^a w ge^scie"+
        " rozpaczy lub te^z politowania. ";
    }

    str+="\n";
    return str;
    }
        