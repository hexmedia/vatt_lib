#include <pogoda.h>
#include <stdproperties.h>
#include <mudtime.h>
#include <macros.h>
#include "dir.h"
inherit PIANA_BAGNO_STD;
inherit "/d/std/bagno.c";

void
create_bagno()
{
    set_short("Grz^askie moczary");
    set_long("@@dlugi_opis@@");
    add_exit(PIANA_OKOLICE_TRAKT_LOKACJE + "droga_09.c","ne",0,FATIGUE,0);
    add_exit(PIANA_OKOLICE_TRAKT_LOKACJE + "droga_08.c","e",0,FATIGUE,0);
    add_exit(PIANA_BAGNO_LOKACJE + "bagno_02.c","n",0,FATIGUE,0);
    add_exit(PIANA_BAGNO_LOKACJE + "bagno_08.c","se",0,FATIGUE,0);
    add_exit(PIANA_BAGNO_LOKACJE + "bagno_07.c","sw",0,FATIGUE,0);
    add_exit(PIANA_BAGNO_LOKACJE + "bagno_01.c","nw",0,FATIGUE,0);
    add_exit(PIANA_BAGNO_LOKACJE + "bagno_04.c","w",0,FATIGUE,0);


    add_prop(ROOM_I_INSIDE,0);
	ustaw_trudnosc_bagna(2)
    
}


public string
exits_description()
{
    return "Ledwno widoczna dr^o^zka prowadzi na p^o^lnoc, zach^od,"+
    " po^ludniowy-wsch^od, po^ludniowy-zach^od, p^o^lnocny-zach^od."+
    " Mo^zesz tak^ze dostrzec wydeptan^a scie^zk^e prowadz^ac^a w st"+
    "rone traktu na p^o^lnocnym-wschodzie i na wschodzie.\n";
}

string
dlugi_opis()
{
    string str = "";

    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="Brudne, szare ^slady szlamu z bagniska wyra^xnie odciskaj^a s"+
        "woje pi^etno na bia^lej niewinno^sci ^sniegu, czyni^ac krajobraz do"+
        "oko^la jeszcze bardziej przygn^ebiaj^acym. Co jaki^s czas, gdzie^s"+
        " w martwych krzewach i wysokich bagiennych, teraz suchych trawach"+
        " dostrzegasz dwa jasne punkciki... Oczy, kt^ore przez ten u^lamek"+
        " chwili, gdy s^a dostrzegalne, wydaj^a si^e bacznie ^sledzi^c two"+
        "je kroki, jakby czekaj^ac na jedno twoje nierozwa^zne posuni^ecie."+
        " Brunatne fa^ldy b^lota zlewaj^a si^e ze sob^a czyni^ac w^edr^owk^"+
        "e tedy jeszcze trudniejsz^a i jeszcze bardziej zdradliw^a. ";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str+="Brudne, szare ^slady szlamu z bagniska wzbijaj^a si^e w g^"+
        "ore z ka^zdym Twoim krokiem, brudz^ac Ci ubranie. ^Snieg, kt^ory"+
        " jeszcze niedawno spowija^l te tereny, po roztopieniu sprawi^l, i"+
        "^z ziemia jest jeszcze bardziej grz^aska, a co za tym idzie jeszc"+
        "ze bardziej niebezpieczna.Co jaki^s czas, gdzie^s w martwych krzew"+
        "ach i wysokich bagiennych trawach dostrzegasz dwa jasne punkciki..."+
        " Oczy, kt^ore przez ten u^lamek chwili, gdy s^a dostrzegalne, wydaj"+
        "^a si^e bacznie ^sledzi^c twoje kroki, jakby czekaj^ac na jedno two"+
        "je nierozwa^zne posuni^ecie. Brunatne fa^ldy b^lota zlewaj^a si^e z"+
        "e sob^a czyni^ac w^edr^owk^e tedy jeszcze trudniejsz^a i jeszcze ba"+
        "rdziej zdradliw^a. ";
    }
    else if(pora_roku() == MT_LATO)
    {
        str+="Brudne, szare ^slady szlamu z bagniska wzbijaj^a si^e w g^"+
        "ore z ka^zdym Twoim krokiem, brudz^ac Ci ubranie. Letni deszcz,"+
        " kt^ory co pewien czas obmywa te tereny sprawi^l, i^z ziemia je"+
        "st jeszcze bardziej grz^aska co za tym idzie jeszcze bardziej n"+
        "iebezpieczna.Co jaki^s czas, gdzie^s w martwych krzewach i wyso"+
        "kich bagiennych trawach dostrzegasz dwa jasne punkciki... Oczy,"+
        " kt^ore przez ten u^lamek chwili, gdy s^a dostrzegalne, wydaj^a"+
        " si^e bacznie ^sledzi^c toje kroki, jakby czekaj^ac na jedno tw"+
        "oje nierozwa^zne posuni^ecie. Brunatne fa^ldy b^lota zlewaj^a si"+
        "^e ze sob^a czyni^ac w^edr^owk^e tedy jeszcze trudniejsz^a i jes"+
        "zcze bardziej zdradliw^a. ";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str+="Brudne, szare ^slady szlamu z bagniska wzbijaj^a si^e w g^o"+
        "re z ka^zdym twoim krokiem, brudz^ac ci ubranie. Mocny deszcz, kt"+
        "^ory co pewien czas obmywa te tereny sprawi^l, i^z ziemia jest je"+
        "szcze bardziej grz^aska co za tym idzie jeszcze bardziej niebezpi"+
        "eczna. Co jaki^s czas, gdzie^s w martwych krzewach i wysokich bag"+
        "iennych trawach dostrzegasz dwa jasne punkciki... Oczy, kt^ore pr"+
        "zez ten u^lamek chwili, gdy s^a dostrzegalne, wydaj^a si^e baczni"+
        "e ^sledzi^c twoje kroki, jakby czekaj^ac na jedno twoje nierozwa^"+
        "zne posuni^ecie. Brunatne fa^ldy b^lota zlewaj^a si^e ze sob^a czy"+
        "ni^ac w^edr^owk^e t^edy jeszcze trudniejsz^a i jeszcze bardziej zd"+
        "radliw^a. ";
    }

    str+="\n";
    return str;
    }
        