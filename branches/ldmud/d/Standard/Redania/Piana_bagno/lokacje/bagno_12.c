#include <pogoda.h>
#include <stdproperties.h>
#include <mudtime.h>
#include <macros.h>
#include "dir.h"
inherit PIANA_BAGNO_STD;
inherit "/lib/drink_water";
void
create_bagno()
{
    set_short("Martwy, bagnisty las");
    set_long("@@dlugi_opis@@");
    add_exit(PIANA_BAGNO_LOKACJE + "bagno_13.c","e",0,FATIGUE,0);
    add_exit(PIANA_BAGNO_LOKACJE + "bagno_16.c","s",0,FATIGUE,0);
    add_prop(ROOM_I_INSIDE,0);
    set_drink_places(({"z rzeki","z pontaru","z Pontaru"}));
    add_sit(({"nad rzek^a","nad pontarem","nad Pontarem"}),
        "nad rzek^a","z nad rzeki",0);

    add_item(({"Pontar","pontar","rzek^e"}),"P^lyn^acy tu od tysi^ecy lat "+
    "szeroki Pontar zdaje si^e by^c wci^a^z nieujarzmionym i niezdobytym, "+
    "wyznacza nie tylko granice mi^edzy kr^olestwami Redanii i Temeri, "+
    "ale i skutecznie ogranicza ludzkie zap^edy w wyrywaniu z r^ak "+
    "matki Natury wszystkiego, co sie jej nale^zy. Gdzieniegdzie "+
    "pojawiaj^ace si^e z nienacka niewielkie zawirowania wody na "+
    "spokojnie, jakby leniwie poruszaj^acej si^e m^etnej tafli swiadcz^a "+
    "o nieprzywidywalno^sci i zdradzieckiej naturze nurtu, kt^ory zapewne "+
    "pochlon^a^l niejedno istnienie bezmy^slnie probuj^ace zmierzy^c si^e z "+
    "pot^eg^a rzeki.\n");
    
}
void
init()
{
    ::init();
    init_drink_water(); 
} 

public string
exits_description()
{
    return "Wszechogarniaj^aca ciemno^s^c sprawia, ^ze nie jeste^s"+
    " w stanie dostrzec st^ad ^zadnego wyj^scia.\n";
}

string
dlugi_opis()
{
    string str = "";

    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="Martwe drzewa w tej cz^e^sci moczar wydaj^a si^e nad wyraz wysokie."+
        " Ich ga^l^ezie zachodz^a na siebie, tworz^ac pewnego rodzaju kopu^l^e,"+
        " w dzie^n szczelnie odcinaj^ac^a promienie s^loneczne, w noc blask"+
        " ksie^zyca. Wiatr graj^acy w^sr^od suchych bagnistych traw zawodzi"+
        " z ka^zdym mocnejszym podmuchem, ods^laniaj^ac rozk^la^daj^ace si^e"+
        " cia^la ofiar w^lasnej nieostro^zno^sci. Ciemno^s^c z ka^zd^a chwil^a"+
        " zdaje si^e nasila^c mami^ac twoj wzrok, a przemieszczaj^ace si^e na tle"+
        " pni cienie sprawiaj^a, ^ze przestrze^n z ka^zdym mrugni^eciem staje si^e"+
        " na nowo obca. W mroku jedynym punktem odniesienia daj^acym ci "+
        "mo^zliwo^s^c zorientowania si^e gdzie jeste^s jest rzeka Pontar, "+
        "cicho szumi^aca gdzie^s na zachodzie.";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str+="Martwe drzewa w tej cz^e^sci moczar wydaj^a si^e nad wyraz wysokie."+
        " Ich ga^l^ezie zachodz^a na siebie, tworz^ac pewnego rodzaju kopu^l^e,"+
        " w dzie^n szczelnie odcinaj^ac^a promienie s^loneczne, w noc blask"+
        " ksie^zyca. Wiatr graj^acy w^sr^od suchych bagnistych traw zawodzi"+
        " z ka^zdym mocnejszym podmuchem, ods^laniaj^ac rozk^la^daj^ace si^e"+
        " cia^la ofiar w^lasnej nieostro^zno^sci. Ciemno^s^c z ka^zd^a chwil^a"+
        " zdaje si^e nasila^c mami^ac twoj wzrok, a przemieszczaj^ace si^e na tle"+
        " pni cienie sprawiaj^a, ^ze przestrze^n z ka^zdym mrugni^eciem staje si^e"+
        " na nowo obca. W mroku jedynym punktem odniesienia daj^acym ci "+
        "mo^zliwo^s^c zorientowania si^e gdzie jeste^s jest rzeka Pontar, "+
        "cicho szumi^aca gdzie^s na zachodzie.";
    }
    else if(pora_roku() == MT_LATO)
    {
        str+="Martwe drzewa w tej cz^e^sci moczar wydaj^a si^e nad wyraz wysokie."+
        " Ich ga^l^ezie zachodz^a na siebie, tworz^ac pewnego rodzaju kopu^l^e,"+
        " w dzie^n szczelnie odcinaj^ac^a promienie s^loneczne, w noc blask"+
        " ksie^zyca. Wiatr graj^acy w^sr^od suchych bagnistych traw zawodzi"+
        " z ka^zdym mocnejszym podmuchem, ods^laniaj^ac rozk^la^daj^ace si^e"+
        " cia^la ofiar w^lasnej nieostro^zno^sci. Ciemno^s^c z ka^zd^a chwil^a"+
        " zdaje si^e nasila^c mami^ac twoj, wzrok a przemieszczaj^ace si^e na tle"+
        " pni cienie sprawiaj^a, ^ze przestrze^n z ka^zdym mrugni^eciem staje si^e"+
        " na nowo obca. W mroku jedynym punktem odniesienia daj^acym ci "+
        "mo^zliwo^s^c zorientowania si^e gdzie jeste^s jest rzeka Pontar, "+
        "cicho szumi^aca gdzie^s na zachodzie.";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str+="Martwe drzewa w tej cz^e^sci moczar wydaj^a si^e nad wyraz wysokie."+
        " Ich ga^l^ezie zachodz^a na siebie, tworz^ac pewnego rodzaju kopu^l^e,"+
        " w dzie^n szczelnie odcinaj^ac^a promienie s^loneczne, w noc blask"+
        " ksie^zyca. Wiatr graj^acy w^sr^od suchych bagnistych traw zawodzi"+
        " z ka^zdym mocnejszym podmuchem, ods^laniaj^ac rozk^la^daj^ace si^e"+
        " cia^la ofiar w^lasnej nieostro^zno^sci. Ciemno^s^c z ka^zd^a chwil^a"+
        " zdaje si^e nasila^c mami^ac twoj wzrok, a przemieszczaj^ace si^e na tle"+
        " pni cienie sprawiaj^a, ^ze przestrze^n z ka^zdym mrugni^eciem staje si^e"+
        " na nowo obca. W mroku jedynym punktem odniesienia daj^acym ci "+
        "mo^zliwo^s^c zorientowania si^e gdzie jeste^s jest rzeka Pontar, "+
        "cicho szumi^aca gdzie^s na zachodzie.";

    }

    str+="\n";
    return str;
    }
        