#include <pogoda.h>
#include <stdproperties.h>
#include <mudtime.h>
#include <macros.h>
#include "dir.h"
inherit PIANA_BAGNO_STD;

void
create_bagno()
{
    set_short("Las po^sr^od bagien");
    set_long("@@dlugi_opis@@");
    add_exit(PIANA_BAGNO_LOKACJE + "bagno_10.c","n",0,FATIGUE,0);
    add_exit(PIANA_BAGNO_LOKACJE + "bagno_18.c","se",0,FATIGUE,0);
    add_exit(PIANA_BAGNO_LOKACJE + "bagno_17.c","s",0,FATIGUE,0);
    add_exit(PIANA_BAGNO_LOKACJE + "bagno_13.c","w",0,FATIGUE,0);
    add_exit(PIANA_BAGNO_LOKACJE + "bagno_09.c","nw",0,FATIGUE,0);

    


    add_prop(ROOM_I_INSIDE,0);
    
}


public string
exits_description()
{
    return "Ledwo widoczna dru^zka prowadzi na p^o^lnoc, po^ludnie i po^ludniowy-wsch^od,"+
    " zach^od i  p^o^lnocny-zach^od.\n";
}

string
dlugi_opis()
{
    string str = "";

    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="Mro^xne powietrze z ka^zdym oddechem przynosi ulge i pewnego"+
        " rodzaju oczyszczenie po czasie sp^edzonym w innych cz^e^sciach bag"+
        "ien. Nieco twardsza ziemia w tym miejscu pokryta zosta^la mi^ekkim"+
        " ^snie^znym puchem, na k^torego powierzchni zwierz^ece tropy poprzez"+
        " zawi^l^a pl^atanine utworzy^ly pi^ekn^a mozaik^e. Martwy krajobraz"+
        " dominuj^acy na terenach dooko^la zosta^l tutaj wyparty przez"+
        " pragn^ac^a ^zycia przyrode, teraz spokojnie drzemi^ac^a pod ^sniego"+
        "w^a pierzyn^a. Spok^oj i cisze tego miejsca podkre^sla wij^aca si^e"+
        " mi^edzy drzewami mg^la, kt^ora delikatnie muska wystaj^ace spod ^sniegu"+
        " ro^sliny, jakby chcia^la utula^c je do snu. ";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str+="Ch^lodne powietrze z ka^zdym oddechem przynosi ulge i pewnego"+
        " rodzaju oczyszczenie po czasie sp^edzonym w innych cz^e^sciach bag"+
        "ien. Nieco twardsza ziemia w tym miejscu pokryta zosta^la wzbijaj^acy"+
        "mi si^e do nieba g^l^owkami ro^slin, kt^ore ju^z niebawem rozwin^a swoje"+
        " listki by m^oc w pe^lni manifestowa^c swoj^a ch^e^c istnienia. Martwy"+ 
        " krajobraz dominuj^acy na terenach dooko^la zosta^l tutaj wyparty przez"+
        " pragn^ac^a ^zycia przyrode, teraz t^etni^ac^a ^zyciem. ";
    }
    else if(pora_roku() == MT_LATO)
    {
        str+="Twardszy grunt w tym miejscu pokryty zosta^l zieleni^a t^etni^acej"+
        " ^zyciem natury. Spokojne kwilenie jakiego^s ptaka i szum ko^ly"+
        "sanych na wietrze, licznych li^sci wydaje si^e wyrywa^c i walczy^c"+
        " z ponurym i ciemnym krajobrazem bagien. Para unosz^aca si^e nad ziemi^a"+
        "wij^e si^e w swoim gro^teskowym ta^ncu przenikaj^ac miedzy drzewami"+
        "i krzewami delikatnie muskaj^ac je, jakby chcia^la pomo^c im w zmaganiach"+
        " z zastan^a rzeczywisto^sci^a. ";
        }
  
     else //jesien i zima gdy nie ma sniegu
    {
        str+="Ch^lodne powietrze z ka^zdym oddechem przynosi ulge i pewnego"+
        " rodzaju oczyszczenie po czasie sp^edzonym w innych cz^e^sciach bag"+
        "ien. Nieco twardsza ziemia w tym miejscu pokryta zosta^la szarymi,"+
        " po^z^o^lk^lymi li^s^cmi. Martwy krajobraz dominuj^acy na terenach"+
        " dooko^la zosta^l tutaj wyparty przez pragn^ac^a ^zycia przyrode, teraz"+
        " spokojnie u^spion^a. Spok^oj i cisze tego miejsca podkre^sla wij^aca si^e"+
        " mi^edzy drzewami mg^la, kt^ora delikatnie muska drzemi^ace"+
        " ro^sliny, jakby chcia^la utula^c je do snu. ";
    }

    str+="\n";
    return str;
    }
        