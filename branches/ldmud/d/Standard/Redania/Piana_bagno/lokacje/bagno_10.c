#include <pogoda.h>
#include <stdproperties.h>
#include <mudtime.h>
#include <macros.h>
#include "dir.h"
inherit PIANA_BAGNO_STD;
inherit "/d/std/bagno.c";

void
create_bagno()
{
    set_short("Cuchn^ace bagnisko");
    set_long("@@dlugi_opis@@");
    add_exit(PIANA_BAGNO_LOKACJE + "bagno_08.c","ne",0,FATIGUE,0);
    add_exit(PIANA_BAGNO_LOKACJE + "bagno_14.c","s",0,FATIGUE,0);
    add_exit(PIANA_BAGNO_LOKACJE + "bagno_13.c","sw",0,FATIGUE,0);
    add_exit(PIANA_BAGNO_LOKACJE + "bagno_07.c","nw",0,FATIGUE,0);

    add_prop(ROOM_I_INSIDE,0);
	ustaw_trudnosc_bagna(2)
    
}


public string
exits_description()
{
    return "Ledwno widoczna dr^o^zka prowadzi na wsch^od, po^ludnie i po^ludniowy-wsch^od.\n";
}

string
dlugi_opis()
{
    string str = "";

    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="Cisza i spok^oj jak^a wprowadzi^la panuj^aca tu zima co chwil^e zostaj^e"+
        " zm^acona przez przera^xliwy krzyk jakiego^s zb^l^akanego ptaka. Zmro^zona"+
        " zimowym ch^lodem ziemia w tej cz^e^sci bagien wydaje si^e by^c pewniejszym"+
        " gruntem, jednak z ka^zdym krokiem nale^zy zachowa^c ostro^zno^s^c. Z pewno"+
        "^sci^a bagna te poch^lon^e^ly ju^z niejednego, nieroztropnego podr^o^znika."+
       " Smr^od w tym miejscu nieco ust^api^l daj^ac mo^zliwo^s^c zaczerpni^ecia"+ 
        " pe^lniejszego oddechu bez nara^zania si^e na nudno^sci. ";
    }    
    else if(pora_roku() == MT_WIOSNA)
    {
        str+="Martwe krzewy, przegni^le pnie, zb^l^akany krzyk ptaka - to obraz"+
        " niegdy^s rosn^acego tu, pot^e^znego boru, w kt^orym wiosna rozpo^sciera^la"+
        " obficie swoje dobra, a w kt^orym teraz tylko w nielicznych miejscach"+
        " mo^zna odnale^x^c jej ciche i delikatne dzia^lania. Cuchn^aca, wilgotna"+
        "  ziemia niczym spragniona czyjego^s ^zycia ^lapie ka^zdy tw^oj krok i zdaje si^e"+
        " go poch^lania^c. Smr^od w tym miejscu nieco ust^api^l daj^ac mo^zliwo^s^c"+
        " zaczerpni^ecia pe^lniejszego oddechu bez nara^zania si^e na nudno^sci. ";
    }
    else if(pora_roku() == MT_LATO)
    {
        str+="Smr^od w tym miejscu nieco ust^api^l daj^ac mo^zliwo^s^c zaczerpni^ecia"+
        " pe^lniejszego oddechu bez nara^zania si^e na nudno^sci. Cuchn^aca, wilgotna"+
        " ziemia niczym spragniona czyjego^s ^zycia ^lapie ka^zdy tw^oj krok i zdaje si^e"+
        " go poch^lania^c. Para unosz^aca si^e nad ziemi^a wij^e si^e w swoim gro^teskowym"+
        " ta^ncu mami^ac z^ludnie twoje zmys^ly i jakby zapraszaj^ac by^s wraz z ni^a odda^l"+
        " si^e ta^ncu na tej nios^acej nie^bezpiecze^nstwo przestrzeni... ";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str+="Martwe krzewy, przegni^le pnie, zb^l^akany krzyk ptaka - to obraz"+
        " niegdy^s rosn^acego tu, pot^e^znego boru, w kt^orym natura rozpo^sciera^la"+
        " obficie swoje dobra, a w kt^orym teraz tylko w nielicznych miejscach"+
        " mo^zna odnale^x^c jej ciche i delikatne dzia^lania. Cuchn^aca, wilgotna"+
        " ziemia niczym spragniona czyjego^s ^zycia ^lapie ka^zdy tw^oj krok i zdaje si^e"+
        " go poch^lania^c. Smr^od w tym miejscu nieco ust^api^l daj^ac mo^zliwo^s^c"+
        " zaczerpni^ecia pe^lniejszego oddechu bez nara^zania si^e na nudno^sci. Brunatne"+
        " fa^ldy b^lota zlewaj^a si^e ze sob^a czyni^ac w^edr^owk^e t^edy jeszcze trudniejsz^a"+
        " i jeszcze bardziej zdradliw^a. ";
    }

    str+="\n";
    return str;
    }
        