#include <pogoda.h>
#include <stdproperties.h>
#include <mudtime.h>
#include <macros.h>
#include "dir.h"
inherit PIANA_BAGNO_STD;

void
create_bagno()
{
    set_short("Grz^askie bagnisko");
    set_long("@@dlugi_opis@@");
    add_exit(PIANA_OKOLICE_TRAKT_LOKACJE + "droga_02.c","se",0,FATIGUE,0);
    add_exit(PIANA_BAGNO_LOKACJE + "bagno_19.c","w",0,FATIGUE,0);
    add_exit(PIANA_BAGNO_LOKACJE + "bagno_17.c","n",0,FATIGUE,0);
    add_exit(PIANA_BAGNO_LOKACJE + "bagno_18.c","ne",0,FATIGUE,0);

    add_prop(ROOM_I_INSIDE,0);
    
}


public string
exits_description()
{
    return "Ledwo widoczna dr^o^zka prowadzi na zach^od, p^o^lnoc i p^o^lnocny-"+
    "wsch^od. Z tego miejsca mo^zesz r^ownie^z dostrzec ^scie^zk^e prowadz^ac^a"+
    " do traktu na po^ludniowym wschodzie.\n";
}

string
dlugi_opis()
{
    string str = "";

    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="Cisza i spok^oj jak^a wprowadzila panuj^aca tu zima co chwil^e zostaj^e"+
        " zm^acona przez przera^xliwy krzyk jakiego^s zb^l^akanego ptaka. Z ka^zdym"+
        " mocniejszym podmuchem wiatru ^snieg dostaje si^e za Twoje ubranie, dra^zni^ac"+
        " i wywo^luj^ac nieprzyjemny dreszcz. Stare konary drzew, kt^ore niegdy^s mog^ly"+
        " zachwyca^c swoim ogromem, teraz stoj^a pojedy^nczo i spogladaj^a na rozleg^le"+
        " tereny dooko^la, niczym zakl^eci, u^spieni stra^znicy. Mi^ekkie pod^lo^ze w zdradliwy"+
        " spos^ob prowadzi twoje kroki mi^edzy martwymi, przegnitymi krzewami, kt^orych cienie"+
        " rozciagaj^a si^e na pod^lo^zu w zawi^lych, na sw^oj spos^ob groteskowych mozaikach. ";

    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str+="B^loto z ka^zdym krokiem wydaje si^e by^c coraz to bardziej grz^askie, i w"+
        " niemi^losierny spos^ob spowalnia ka^zdy tw^oj ruch. G^luchym chl^upni^eciom"+
        " twoich krok^ow wt^oruj^a pokrzykiwania jaki^s wiosennych ptak^ow, kt^ore niefortunnie"+
        " trafi^ly w ten martwy, cuchn^acy krajobraz. Stare konary drzew, kt^ore niegdy^s mog^ly"+
        " zachwyca^c swoim ogromem, teraz stoj^a pojedy^nczo i spogladaj^a na rozleg^le tereny"+
        " dooko^la, niczym zakl^eci, u^spieni stra^znicy. Mi^ekkie pod^lo^ze w zdradliwy"+
        " spos^ob prowadzi twoje kroki mi^edzy martwymi, przegnitymi krzewami, kt^orych cienie"+
        " rozciagaj^a si^e na pod^lo^zu w zawi^lych, na sw^oj spos^ob groteskowych mozaikach. ";
    }
    else if(pora_roku() == MT_LATO)
    {
        str+="B^loto z ka^zdym krokiem wydaje si^e by^c coraz to bardziej grz^askie, i w"+
        " niemi^losierny spos^ob spowalnia ka^zdy tw^oj ruch. G^luchym chl^upni^eciom"+
        " twoich krok^ow wt^oruj^a pokrzykiwania jaki^s ptak^ow, kt^ore niefortunnie"+
        " trafi^ly w ten martwy, cuchn^acy krajobraz. Stare konary drzew, kt^ore niegdy^s mog^ly"+
        " zachwyca^c swoim ogromem, teraz stoj^a pojedy^nczo i spogladaj^a na rozleg^le tereny"+
        " dooko^la, niczym zakl^eci, u^spieni stra^znicy. Mi^ekkie pod^lo^ze w zdradliwy"+
        " spos^ob prowadzi twoje kroki mi^edzy martwymi, przegnitymi krzewami, kt^orych cienie"+
        " rozciagaj^a si^e na pod^lo^zu w zawi^lych, na sw^oj spos^ob groteskowych mozaikach. ";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str+="B^loto z ka^zdym krokiem wydaje si^e by^c coraz to bardziej grz^askie, i w"+
        " niemi^losierny spos^ob spowalnia ka^zdy tw^oj ruch. G^luchym chl^upni^eciom"+
        " twoich krok^ow wt^oruj^a pokrzykiwania jaki^s ptak^ow, kt^ore niefortunnie"+
        " trafi^ly w ten martwy, cuchn^acy krajobraz. Stare konary drzew, kt^ore niegdy^s mog^ly"+
        " zachwyca^c swoim ogromem, teraz stoj^a pojedy^nczo i spogladaj^a na rozleg^le tereny"+
        " dooko^la, niczym zakl^eci, u^spieni stra^znicy. Mi^ekkie pod^lo^ze w zdradliwy"+
        " spos^ob prowadzi twoje kroki mi^edzy martwymi, przegnitymi krzewami, kt^orych cienie"+
        " rozciagaj^a si^e na pod^lo^zu w zawi^lych, na sw^oj spos^ob groteskowych mozaikach. ";
    }

    str+="\n";
    return str;
    }
        