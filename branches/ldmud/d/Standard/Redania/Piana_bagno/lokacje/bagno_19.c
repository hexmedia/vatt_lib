#include <pogoda.h>
#include <stdproperties.h>
#include <mudtime.h>
#include <macros.h>
#include "dir.h"
inherit PIANA_BAGNO_STD;

void
create_bagno()
{
    set_short("Martwy, bagnisty las");
    set_long("@@dlugi_opis@@");
    add_exit(PIANA_BAGNO_LOKACJE + "bagno_16.c","nw",0,FATIGUE,0);
    add_exit(PIANA_BAGNO_LOKACJE + "bagno_17.c","ne",0,FATIGUE,0);
    add_exit(PIANA_BAGNO_LOKACJE + "bagno_20.c","e",0,FATIGUE,0);


    add_prop(ROOM_I_INSIDE,0);
    
}


public string
exits_description()
{
    return "Wszechogarniaj^aca ciemno^s^c sprawia, ^ze nie jeste^s"+
    " w stanie dostrzec st^ad ^zadnego wyj^scia.\n";
}

string
dlugi_opis()
{
    string str = "";

    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="Wiecznie panuj^aca tu ciemno^s^c podst^epnie zataja niebezpiecze"+
        "^nstwa tego podmok^lego terenu. Ta^ncz^ace na wietrze, martwe ga^l^ezie"+
        " krzew^ow i drzew tn^a g^lucho powietrze, zostawiaj^ac po sobie tylko"+
        " echo tego bezg^lo^snego, ^za^losnego gestu. Pojedy^ncze k^epki trawy"+
        " rozci^agni^ete na powierzchni bagna w pewien spos^ob wyznaczaj^a drog^e"+
        " gdzie powinien by^c postawiony krok ka^zdego, kto odwa^zy^l si^e zapu^s"+
        "ci^c a^z tutaj, a grz^askie bagna zdaj^a si^e tylko czeka^c na ka^zdy"+
        " b^ledny ruch. Zapach gnij^acych ro^slin i rozk^ladaj^acych si^e zw^lok"+
        " ofiar tego miejsca z ka^zd^a chwil^a staj^a si^e coraz bardziej nie do"+
        " zniesienia. ";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str+="Wiecznie panuj^aca tu ciemno^s^c podst^epnie zataja niebezpiecze"+
        "^nstwa tego podmok^lego terenu. Ta^ncz^ace na wietrze, martwe ga^l^ezie"+
        " krzew^ow i drzew tn^a g^lucho powietrze, zostawiaj^ac po sobie tylko"+
        " echo tego bezg^lo^snego, ^za^losnego gestu. Pojedy^ncze k^epki trawy"+
        " rozci^agni^ete na powierzchni bagna w pewien spos^ob wyznaczaj^a drog^e"+
        " gdzie powinien by^c postawiony krok ka^zdego, kto odwa^zy^l si^e zapu^s"+
        "ci^c a^z tutaj, a grz^askie bagna zdaj^a si^e tylko czeka^c na ka^zdy"+
        " b^ledny ruch. Zapach gnij^acych ro^slin i rozk^ladaj^acych si^e zw^lok"+
        " ofiar tego miejsca z ka^zd^a chwil^a staj^a si^e coraz bardziej nie do"+
        " zniesienia. ";
    }
    else if(pora_roku() == MT_LATO)
    {
        str+="Wiecznie panuj^aca tu ciemno^s^c podst^epnie zataja niebezpiecze"+
        "^nstwa tego podmok^lego terenu. Ta^ncz^ace na wietrze, martwe ga^l^ezie"+
        " krzew^ow i drzew tn^a g^lucho powietrze, zostawiaj^ac po sobie tylko"+
        " echo tego bezg^lo^snego, ^za^losnego gestu. Pojedy^ncze k^epki trawy"+
        " rozci^agni^ete na powierzchni bagna w pewien spos^ob wyznaczaj^a drog^e"+
        " gdzie powinien by^c postawiony krok ka^zdego, kto odwa^zy^l si^e zapu^s"+
        "ci^c a^z tutaj, a grz^askie bagna zdaj^a si^e tylko czeka^c na ka^zdy"+
        " b^ledny ruch. Zapach gnij^acych ro^slin i rozk^ladaj^acych si^e zw^lok"+
        " ofiar tego miejsca z ka^zd^a chwil^a staj^a si^e coraz bardziej nie do"+
        " zniesienia. ";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str+="Wiecznie panuj^aca tu ciemno^s^c podst^epnie zataja niebezpiecze"+
        "^nstwa tego podmok^lego terenu. Ta^ncz^ace na wietrze, martwe ga^l^ezie"+
        " krzew^ow i drzew tn^a g^lucho powietrze, zostawiaj^ac po sobie tylko"+
        " echo tego bezg^lo^snego, ^za^losnego gestu. Pojedy^ncze k^epki trawy"+
        " rozci^agni^ete na powierzchni bagna w pewien spos^ob wyznaczaj^a drog^e"+
        " gdzie powinien by^c postawiony krok ka^zdego, kto odwa^zy^l si^e zapu^s"+
        "ci^c a^z tutaj, a grz^askie bagna zdaj^a si^e tylko czeka^c na ka^zdy"+
        " b^ledny ruch. Zapach gnij^acych ro^slin i rozk^ladaj^acych si^e zw^lok"+
        " ofiar tego miejsca z ka^zd^a chwil^a staj^a si^e coraz bardziej nie do"+
        " zniesienia. ";
    }

    str+="\n";
    return str;
    }
        