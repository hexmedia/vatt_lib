#include <pogoda.h>
#include <stdproperties.h>
#include <mudtime.h>
#include <macros.h>
#include "dir.h"
inherit PIANA_BAGNO_STD;
inherit "/lib/drink_water";
void
create_bagno()
{
    set_short("Cuchn^ace bagnisko");
    set_long("@@dlugi_opis@@");
    add_exit(PIANA_BAGNO_LOKACJE + "bagno_02.c","e",0,FATIGUE,0);
    add_exit(PIANA_BAGNO_LOKACJE + "bagno_04.c","s",0,FATIGUE,0);
    add_exit(PIANA_BAGNO_LOKACJE + "bagno_05.c","se",0,FATIGUE,0);
    add_prop(ROOM_I_INSIDE,0);

    set_drink_places(({"z rzeki","z pontaru","z Pontaru"}));
    add_sit(({"nad rzek^a","nad pontarem","nad Pontarem"}),
        "nad rzek^a","z nad rzeki",0);

    add_item(({"Pontar","pontar","rzek^e"}),"P^lyn^acy tu od tysi^ecy lat "+
    "szeroki Pontar zdaje si^e by^c wci^a^z nieujarzmionym i niezdobytym, "+
    "wyznacza nie tylko granice mi^edzy kr^olestwami Redanii i Temeri, "+
    "ale i skutecznie ogranicza ludzkie zap^edy w wyrywaniu z r^ak "+
    "matki Natury wszystkiego, co sie jej nale^zy. Gdzieniegdzie "+
    "pojawiaj^ace si^e z nienacka niewielkie zawirowania wody na "+
    "spokojnie, jakby leniwie poruszaj^acej si^e m^etnej tafli swiadcz^a "+
    "o nieprzywidywalno^sci i zdradzieckiej naturze nurtu, kt^ory zapewne "+
    "pochlon^a^l niejedno istnienie bezmy^slnie probuj^ace zmierzy^c si^e z "+
    "pot^eg^a rzeki.\n");
}
void
init()
{
    ::init();
    init_drink_water(); 
} 

public string
exits_description()
{
    return "Ledwo widoczna dr^o^zka prowadzi na wsch^od,"+
    " po^ludnie i po^ludniowy-wsch^od.\n";
}

string
dlugi_opis()
{
    string str = "";

    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="Okryte ^snie^zn^a pierzyn^a bagno wydaje si^e by^c"+
        " jakby nieco u^spione. Biel spowijaj^aca szaro^s^c tego p"+
        "onurego miejsca zdaje si^e celowo ukrywa^c brzydote jak^"+
        "a niesie ze sob^a bagnisko, jednak brunatne, namokni^ete"+
        " szlamem plamy bynajmniej nie chc^a da^c o sobie zapomnie"+
        "^c. Miekkie podlo^ze w zdradliwy spos^ob prowadzi twoje k"+
        "rok i miedzy martwymi, przegnitymi krzewami, kt^orych cie"+
        "nie rozciagaj^a si^e na pod^lo^zu w zawi^lych, na sw^oj s"+
        "pos^b groteskowych mozaikach. Stare konary drzew, kt^ore n"+
        "iegdy^s mog^ly zachwyca^c swoim ogromem, teraz stoj^a poje"+
        "dy^nczo i spogladaj^a na rozleg^le tereny dooko^la, niczym"+
        " zakl^eci, u^spieni stra^znicy. Na zachodzie s^lycha^c "+
        "niespokojny i gwa^ltowny szum Pontaru. ";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str+="Budz^aca si^e do ^zycia ro^slinno^s^c bynajmniej nie do"+
        "da^la uroku i ^swie^zo^sci miejscu, w kt^orym si^e znajdujesz."+
        " Niedawny jeszcze ^snieg spowijaj^acy te tereny po roztopieniu,"+
        " sprawi^l, i^z ziemia jest jeszcze bardziej grz^aska, a co za"+
        " tym idzie... jeszcze bardziej niebezpieczna. Mi^ekkie podlo^ze"+
        " w zdradliwy spos^ob prowadzi twoje kroki miedzy martwymi, przeg"+
        "nitymi krzewami, kt^orych cienie rozciagaj^a si^e na pod^lo^zu w"+
        " zawi^lych, na sw^oj spos^b groteskowych mozaikach. Stare konary"+
        " drzew, kt^ore niegdy^s mog^ly zachwyca^c swoim ogromem, teraz s"+
        "toj^a pojedy^nczo i spogladaj^a na rozleg^le tereny dooko^la, ni"+
        "czym zakl^eci, u^spieni stra^znicy. Na zachodzie s^lycha^c "+
        "niespokojny i gwa^ltowny szum Pontaru. ";
    }
    else if(pora_roku() == MT_LATO)
    {
        str+="Powietrze w tym miejscu zdaje si^e by^c znacznie g^estsze"+
        " od duchoty i smrodu rozk^ladaj^acych si^e ro^slin. Gor^ac panuj"+
        "^acy tutaj za dnia wydaj^e si^e nie ust^epowa^c nawet w nocy, p"+
        "odtrzymywany ci^e^zkim oddechem paruj^acej, grz^askiej ziemi, k"+
        "t^ora w zdradliwy spos^ob prowadzi twoje kroki mi^edzy martwymi,"+
        " przegnitymi krzewami, kt^orych cienie rozciagaj^a si^e na pod^"+
        "lo^zu w zawi^lych, na sw^oj spos^ob groteskowych mozaikach. Sta"+
        "re konary drzew, kt^ore niegdy^s mog^ly zachwyca^c swoim ogromem,"+
        " teraz stoj^a pojedy^nczo i spogladaj^a na rozleg^le tereny dooko"+
        "^la, niczym zakl^eci, u^spieni stra^znicy. Na zachodzie s^lycha^c "+
        "niespokojny i gwa^ltowny szum Pontaru. ";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str+="Powietrze w tym miejscu zdaje si^e by^c znacznie g^estsze"+
        " od duchoty i smrodu rozk^ladaj^acych si^e ro^slin. Mi^ekkie pod"+
        "^lo^ze w zdradliwy spos^ob prowadzi twoje kroki mi^edzy martwymi,"+
        " przegnitymi krzewami, kt^orych cienie rozciagaj^a si^e na pod^lo"+
        "^zu w zawi^lych, na sw^oj spos^ob groteskowych mozaikach. Stare k"+
        "onary drzew, kt^ore niegdy^s mog^ly zachwyca^c swoim ogromem, te"+
        "raz stoj^a pojedy^nczo i spogladaj^a na rozleg^le tereny dooko^l"+
        "a, niczym zakl^eci, u^spieni stra^znicy. Na zachodzie s^lycha^c "+
        "niespokojny i gwa^ltowny szum Pontaru.";
    }

    str+="\n";
    return str;
}
