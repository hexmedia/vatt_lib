#include <pogoda.h>
#include <stdproperties.h>
#include <mudtime.h>
#include <macros.h>
#include "dir.h"
inherit PIANA_BAGNO_STD;

void
create_bagno()
{
    set_short("Las po^sr^od bagien");
    set_long("@@dlugi_opis@@");
    add_exit(PIANA_BAGNO_LOKACJE + "bagno_13.c","s",0,FATIGUE,0);
    add_exit(PIANA_BAGNO_LOKACJE + "bagno_06.c","nw",0,FATIGUE,0);
    add_exit(PIANA_BAGNO_LOKACJE + "bagno_14.c","se",0,FATIGUE,0);
    add_prop(ROOM_I_INSIDE,0);
    add_npc(PIANA_BAGNO_LIVINGI + "los");   
    
}


public string
exits_description()
{
    return "Ledwo widoczna dr^o^zka prowadzi na p^o^lnocny-zach^od, "+
        "po^ludnie i po^ludniowy-wsch^od.\n";
}

string
dlugi_opis()
{
    string str = "";

    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="Nieco twardsza ziemia w tym miejscu pokryta zosta^la mi^ekkim"+
        " ^snie^znym puchem, na k^torego powierzchni zwierz^ece tropy poprzez"+
        " zawi^l^a pl^atanine utworzy^ly pi^ekn^a mozaik^e. Martwy krajobraz"+
        " widoczny w innych cz^e^sciach bagien zosta^l tutaj zdominowany przez"+
        " pragn^ac^a ^zycia przyrode, teraz spokojnie drzemi^ac^a pod ^sniego"+
        "w^a pierzyn^a. Spok^oj i cisze tego miejsca podkre^sla wij^aca si^e"+
        " mi^edzy drzewami mg^la, kt^ora delikatnie muska wystaj^ace spod ^sniegu"+
        " ro^sliny, jakby chcia^la utula^c je do snu. ";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str+="Twardszy grunt w tym miejscu pokryty zosta^l zieleni^a budz^acej"+
        " si^e do ^zycia przyrody. Spokojne kwilenie jakiego^s ptaka i szum ko^ly"+
        "sanych na wietrze, niedawno obudzonych li^sci wydaje si^e wyrywa^c i walczy^c"+
        " z ponurym i ciemnym krajobrazem bagien. Na ^swie^zej ziemi gdzieniegdzie"+
        " dostrzec mo^zna zwierz^ece tropy, kt^ore swoj^a pl^atanin^a stworzy^ly"+
        " pi^ekn^a mozaik^e. Spok^oj i cisze tego miejsca podkre^sla wij^aca si^e"+
        " mi^edzy drzewami mg^la, kt^ora delikatnie muska wznosz^ace si^e w g^ore"+
        " ro^sliny, jakby chcia^la pom^oc im w zmaganiach z zastan^a rzeczywisto^sci^a. ";
    }
    else if(pora_roku() == MT_LATO)
    {
        str+="Twardszy grunt w tym miejscu pokryty zosta^l zieleni^a t^etni^acej"+
        " ^zyciem natury. Spokojne kwilenie jakiego^s ptaka i szum ko^ly"+
        "sanych na wietrze, licznych li^sci wydaje si^e wyrywa^c i walczy^c"+
        " z ponurym i ciemnym krajobrazem bagien. Na ^swie^zej ziemi gdzieniegdzie"+
        " dostrzec mo^zna zwierz^ece tropy, kt^ore swoj^a pl^atanin^a stworzy^ly"+
        " pi^ekn^a mozaik^e. Spok^oj i cisze tego miejsca podkre^sla wij^aca si^e"+
        " mi^edzy drzewami lekka mg^la, kt^ora delikatnie muska ko^lysz^ace si^e"+
        " ro^sliny, jakby chcia^la pomo^c im w zmaganiach z zastan^a rzeczywisto^sci^a. ";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str+="Z ulg^a twoje stopy witaj^a twardszy i pewniejszy grunt. Spokojne kwi"+
        "lenie jakiego^s ptaka i szum ko^lysanych na wietrze, pozosta^lych na drzew"+
        "ach li^sci wydaje si^e wyrywa^c i walczy^c z ponurym i ciemnym krajobrazem ba"+
        "gien. Na ^swie^zej ziemi gdzieniegdzie dostrzec mo^zna zwierz^ece tropy, kt^o"+
        "re swoj^a pl^atanin^a stworzy^ly pi^ekn^a mozaik^e. Spok^oj i cisze tego miejs"+
        "ca podkre^sla wij^aca si^e mi^edzy drzewami mg^la, kt^ora delikatnie muska wzno"+
        "sz^ace si^e w g^ore ro^sliny, jakby chcia^la pom^oc im w zmaganiach z zastan^a"+
        " rzeczywisto^sci^a. ";

    }

    str+="\n";
    return str;
    }
        