#include <pogoda.h>
#include <stdproperties.h>
#include <mudtime.h>
#include <macros.h>
#include "dir.h"
inherit PIANA_BAGNO_STD;

void
create_bagno()
{
    set_short("Cuchn^ace bagnisko");
    set_long("@@dlugi_opis@@");
    add_exit(PIANA_OKOLICE_TRAKT_LOKACJE + "droga_08.c","n",0,FATIGUE,0);
    add_exit(PIANA_OKOLICE_TRAKT_LOKACJE + "droga_07.c","e",0,FATIGUE,0);
    add_exit(PIANA_BAGNO_LOKACJE + "bagno_05.c","nw",0,FATIGUE,0);
    add_exit(PIANA_BAGNO_LOKACJE + "bagno_10.c","sw",0,FATIGUE,0);
    add_prop(ROOM_I_INSIDE,0);
    
}


public string
exits_description()
{
    return "Ledwo widoczna dr^o^zka prowadzi na p^o^lnocny-zach^od,"+
    " po^ludniowy-zach^od. Mo^zesz tak^ze dostrzec ^scie^zke prowadz^ac^a"+
    " do traktu na p^o^lnocy i na wschodzie.\n";
}

string
dlugi_opis()
{
    string str = "";

    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="Cisza i spok^oj jak^a wprowadzila panuj^aca tu zima co chwil^e zostaj^e"+
        " zm^acona przez przera^xliwy krzyk jakiego^s zb^l^akanego ptaka. Z ka^zdym"+
        " mocniejszym podmuchem wiatru ^snieg dostaje si^e za twoje ubranie, dra^zni^ac"+
        " i wywo^luj^ac nieprzyjemny dreszcz. Stare konary drzew, kt^ore niegdy^s mog^ly"+
        " zachwyca^c swoim ogromem, teraz stoj^a pojedy^nczo i spogladaj^a na rozleg^le"+
        " tereny dooko^la, niczym zakl^eci, u^spieni stra^znicy. Miekkie podlo^ze w zdradliwy"+
        " spos^ob prowadzi twoje kroki miedzy martwymi, przegnitymi krzewami, kt^orych cienie"+
        " rozciagaj^a si^e na pod^lo^zu w zawi^lych, na sw^oj spos^b groteskowych mozaikach. ";

    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str+="B^loto z ka^zdym krokiem wydaj^e si^e by^c coraz to bardziej grz^askie, i w"+
        " niemi^losierny spos^ob spowalnia ka^zdy tw^oj ruch. G^luchym chl^upni^eciom"+
        " twoich krok^ow wt^oruj^a pokrzykiwania jaki^s wiosennych ptak^ow, kt^ore niefortunnie"+
        " trafi^ly w ten martwy, cuchn^acy krajobraz. Stare konary drzew, kt^ore niegdy^s mog^ly"+
        " zachwyca^c swoim ogromem, teraz stoj^a pojedy^nczo i spogladaj^a na rozleg^le tereny"+
        " dooko^la, niczym zakl^eci, u^spieni stra^znicy. Miekkie podlo^ze w zdradliwy"+
        " spos^ob prowadzi twoje stopy miedzy martwymi, przegnitymi krzewami, kt^orych cienie"+
        " rozciagaj^a si^e na pod^lo^zu w zawi^lych, na sw^oj spos^b groteskowych mozaikach. ";
    }
    else if(pora_roku() == MT_LATO)
    {
        str+="B^loto z ka^zdym krokiem wydaj^e si^e by^c coraz to bardziej grz^askie, i w"+
        " niemi^losierny spos^ob spowalnia ka^zdy tw^oj ruch. G^luchym chl^upni^eciom"+
        " twoich krok^ow wt^oruj^a pokrzykiwania jaki^s ptak^ow, kt^ore niefortunnie"+
        " trafi^ly w ten martwy, cuchn^acy krajobraz. Stare konary drzew, kt^ore niegdy^s mog^ly"+
        " zachwyca^c swoim ogromem, teraz stoj^a pojedy^nczo i spogladaj^a na rozleg^le tereny"+
        " dooko^la, niczym zakl^eci, u^spieni stra^znicy. Miekkie podlo^ze w zdradliwy"+
        " spos^ob prowadzi twoje stopy miedzy martwymi, przegnitymi krzewami, kt^orych cienie"+
        " rozciagaj^a si^e na pod^lo^zu w zawi^lych, na sw^oj spos^b groteskowych mozaikach. ";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str+="B^loto z ka^zdym krokiem wydaj^e si^e by^c coraz to bardziej grz^askie, i w"+
        " niemi^losierny spos^ob spowalnia ka^zdy tw^oj ruch. G^luchym chl^upni^eciom"+
        " twoich krok^ow wt^oruj^a pokrzykiwania jaki^s ptak^ow, kt^ore niefortunnie"+
        " trafi^ly w ten martwy, cuchn^acy krajobraz. Stare konary drzew, kt^ore niegdy^s mog^ly"+
        " zachwyca^c swoim ogromem, teraz stoj^a pojedy^nczo i spogladaj^a na rozleg^le tereny"+
        " dooko^la, niczym zakl^eci, u^spieni stra^znicy. Miekkie podlo^ze w zdradliwy"+
        " spos^ob prowadzi twoje stopy miedzy martwymi, przegnitymi krzewami, kt^orych cienie"+
        " rozciagaj^a si^e na pod^lo^zu w zawi^lych, na sw^oj spos^b groteskowych mozaikach. ";
    }

    str+="\n";
    return str;
    }
        