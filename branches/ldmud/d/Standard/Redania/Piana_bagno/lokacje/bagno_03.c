#include <pogoda.h>
#include <stdproperties.h>
#include <mudtime.h>
#include <macros.h>
#include "dir.h"
inherit PIANA_BAGNO_STD;
inherit "/lib/drink_water";
void
create_bagno()
{
    set_short("Cuchn^ace bagnisko");
    set_long("@@dlugi_opis@@");
    add_exit(PIANA_BAGNO_LOKACJE + "bagno_04.c","e",0,FATIGUE,0);
    add_exit(PIANA_BAGNO_LOKACJE + "bagno_06.c","s",0,FATIGUE,0);
    add_exit(PIANA_BAGNO_LOKACJE + "bagno_07.c","se",0,FATIGUE,0);

    add_prop(ROOM_I_INSIDE,0);
    set_drink_places(({"z rzeki","z pontaru","z Pontaru"}));
    add_sit(({"nad rzek^a","nad pontarem","nad Pontarem"}),
        "nad rzek^a","z nad rzeki",0);

    add_item(({"Pontar","pontar","rzek^e"}),"P^lyn^acy tu od tysi^ecy lat "+
    "szeroki Pontar zdaje si^e by^c wci^a^z nieujarzmionym i niezdobytym, "+
    "wyznacza nie tylko granice mi^edzy kr^olestwami Redanii i Temeri, "+
    "ale i skutecznie ogranicza ludzkie zap^edy w wyrywaniu z r^ak "+
    "matki Natury wszystkiego, co sie jej nale^zy. Gdzieniegdzie "+
    "pojawiaj^ace si^e z nienacka niewielkie zawirowania wody na "+
    "spokojnie, jakby leniwie poruszaj^acej si^e m^etnej tafli swiadcz^a "+
    "o nieprzywidywalno^sci i zdradzieckiej naturze nurtu, kt^ory zapewne "+
    "pochlon^a^l niejedno istnienie bezmy^slnie probuj^ace zmierzy^c si^e z "+
    "pot^eg^a rzeki.\n");

    set_alarm_every_hour("utopce");
    
}
void
init()
{
    ::init();
    init_drink_water(); 
} 
void
utopce()
{
    if(MT_GODZINA >= 18)
    {
        add_npc(PIANA_BAGNO_LIVINGI + "utopiec");
        add_npc(PIANA_BAGNO_LIVINGI + "utopiec");
    }
}
public string
exits_description()
{
    return "Ledwo widoczna dr^o^zka prowadzi na wsch^od, po^ludnie i p"+
    "o^ludniowy-wsch^od.\n";
}

string
dlugi_opis()
{
    string str = "";

    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="Z ka^zdym mocniejszym podmuchem wiatru ^snieg dostaje si"+
        "^e za twoje ubranie. Biel spowijaj^aca szaro^s^c tego ponurego"+
        " miejsca zdaje si^e celowo ukrywa^c brzydote jak^a niesie ze s"+
        "ob^a bagnisko, jednak brunatne, namokni^ete szlamem plamy byna"+
        "jmniej nie chc^a da^c o sobie zapomnie^c. Mg^la nadci^agaj^aca z "+
        "zach^odu, znad powierzchni w^od Pontaru osadza si^e g^esto nad "+
        "ziemi^a.Otaczaj^ace cie z ka^"+
        "zdej strony bagniska bez wzgl^edu na pore roku czy pore dnia sk"+
        "^apane s^a w p^o^lmroku, kt^ory zdradliwie zataja niebezpiecze^"+
        "nstwa tego podmok^lego terenu. Cienie padaj^ace z pojedy^nczyc"+
        "h, martwo wystaj^acych z ziemi pni igraj^a z twoim wzrokiem... "; 
      
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str+="B^loto z ka^zdym krokiem wydaj^e si^e by^c coraz to bardziej"+
        " grz^askie, i w niemi^losierny spos^ob spowalnia ka^zdy Tw^oj ruch."+
        " G^luchym chl^upni^eciom twoich krok^ow wturuj^a pokrzykiwania jak"+
        "i^s wiosennych ptak^ow, kt^ore niefortunnie trafi^ly w ten martwy,"+
        " cuchn^acy krajobraz. Mg^la nadci^agaj^aca z "+
        "zach^odu, znad powierzchni w^od Pontaru osadza si^e g^esto nad "+
        "ziemi^a. Otaczaj^ace cie z ka^zdej strony bagniska be"+
        "z wzgl^edu na pore roku czy pore dnia sk^apane s^a w p^o^lmroku, k"+
        "t^ory zdradliwie zataja niebezpiecze^nstwa tego podmok^lego terenu."+
        " Cienie padaj^ace z pojedy^nczych, martwo wystaj^acych z ziemi pni"+
        " igraj^a z twoim wzrokiem... ";
    }
    else if(pora_roku() == MT_LATO)
    {
        str+="Gor^ac panuj^acy tutaj za dnia wydaj^e si^e nie ust^epowa^c"+
        " nawet w nocy, podtrzymywany ci^e^zkim oddechem paruj^acej, grz^a"+
        "skiej ziemi, kt^ora z ka^zdym twoim krokiem wydaje si^e by^c cora"+
        "z bardziej grz^aska, uniemo^zliwiaj^ac w tej spos^b spokojne ^poru"+
        "szanie si^e, bez nadmiernego zm^eczenia. Mg^la nadci^agaj^aca z "+
        "zach^odu, znad powierzchni w^od Pontaru osadza si^e g^esto nad "+
        "ziemi^a. Otaczaj^ace cie z ka^zdej"+
        " strony bagniska bez wzgl^edu na pore roku czy pore dnia sk^apane"+
        " s^a w p^o^lmroku, kt^ory zdradliwie zataja niebezpiecze^nstwa tego"+
        " podmok^lego terenu. ";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str+="B^loto z ka^zdym krokiem wydaje si^e by^c coraz to bardziej"+
        " grz^askie, i w niemi^losierny spos^ob spowalnia ka^zdy tw^oj ruch."+
        " G^luchym chl^upni^eciom twoich krok^ow wt^oruj^a pokrzykiwania ja"+
        "ki^s zb^lakanych ptak^ow, kt^ore niefortunnie trafi^ly w ten martwy"+
        ", cuchn^acy krajobraz. Mg^la nadci^agaj^aca z "+
        "zach^odu, znad powierzchni w^od Pontaru osadza si^e g^esto nad "+
        "ziemi^a. Otaczaj^ace cie z ka^zdej strony bagniska"+
        " bez wzgl^edu na por^e roku czy por^e dnia sk^apane s^a w p^o^lmro"+
        "ku, kt^ory zdradliwie zataja niebezpiecze^nstwa tego podmok^lego te"+
        "renu. Cienie padaj^ace z pojedy^nczych, martwo wystaj^acych z ziemi"+
        " pni igraj^a z twoim wzrokiem... ";
    }

    str+="\n";
    return str;
    }
        