#include <pogoda.h>
#include <stdproperties.h>
#include <mudtime.h>
#include <macros.h>
#include "dir.h"
inherit PIANA_BAGNO_STD;
inherit "/lib/drink_water";

void
create_bagno()
{
    set_short("Przy bajorze");
    set_long("@@dlugi_opis@@");
    add_exit(PIANA_BAGNO_LOKACJE + "bagno_13.c","nw",0,FATIGUE,0);
    add_exit(PIANA_BAGNO_LOKACJE + "bagno_14.c","n",0,FATIGUE,0);
    add_exit(PIANA_BAGNO_LOKACJE + "bagno_15.c","ne",0,FATIGUE,0);
    add_exit(PIANA_BAGNO_LOKACJE + "bagno_19.c","sw",0,FATIGUE,0);
    add_exit(PIANA_BAGNO_LOKACJE + "bagno_20.c","s",0,FATIGUE,0);
    add_exit(PIANA_BAGNO_LOKACJE + "bagno_18.c","e",0,FATIGUE,0);
    
    set_drink_places(({"z bajora","z bajorka"}));

    add_prop(ROOM_I_INSIDE,0);
    
}
void
init()
{
    ::init();
    init_drink_water(); 
} 

public string
exits_description()
{
    return "Ledwo widoczna dr^o^zka prowadzi na p^o^lnocny-zach^od, p^o^lnoc,"+
    " p^o^lnocny-wsch^od, po^ludniowy-zach^od, po^ludnie i wsch^od.\n";
}

string
dlugi_opis()
{
    string str = "";

    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="Krzewy i drzewa niemal ca^lkowicie ust^api^ly miejsce wolnej prze"+
        "strzeni, tworz^ac jakby b^lotnist^a polanke, na ^kt^orej ^srodku znajduje"+
        " sie ogromne, brudne bajoro. Jego brzegi pokryte s^a przegni^lymi li^s^cmi"+
        " i pojed^ynczymi k^lodami rosn^acych tu niegdy^s drzew. Pod^lo^ze jest tutaj"+
        " do^s^c ^sliskie dlatego z ka^zdym krokiem nale^zy zachowa^c ostro^zno^s^c."+
        " Z pewno^sci^a bagna te poch^lon^e^ly ju^z niejednego, nieroztropnego"+
        " podr^o^znika, a niepozorne bajoro gdyby tylko potrafi^lo m^owi^c, opowie"+
        "dzia^lo by niejedn^a histori^e o tajemnicach jakie spoczywaj^a na jego dnie. ";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str+="Krzewy i drzewa niemal ca^lkowicie ust^api^ly miejsce wolnej prze"+
        "strzeni, tworz^ac jakby b^lotnist^a polanke, na ^kt^orej ^srodku znajduje"+
        " sie ogromne, brudne bajoro. Jego brzegi pokryte s^a przegni^lymi li^s^cmi"+
        " i pojed^ynczymi k^lodami rosn^acych tu niegdy^s drzew. Pod^lo^ze jest tutaj"+
        " do^s^c ^sliskie dlatego z ka^zdym krokiem nale^zy zachowa^c ostro^zno^s^c."+
        " Z pewno^sci^a bagna te poch^lon^e^ly ju^z niejednego, nieroztropnego"+
        " podr^o^znika, a niepozorne bajoro gdyby tylko potrafi^lo m^owi^c, opowie"+
        "dzia^lo by niejedn^a histori^e o tajemnicach jakie spoczywaj^a na jego dnie. ";
    }
    else if(pora_roku() == MT_LATO)
    {
        str+="Krzewy i drzewa niemal ca^lkowicie ust^api^ly miejsce wolnej prze"+
        "strzeni, tworz^ac jakby b^lotnist^a polanke, na ^kt^orej ^srodku znajduje"+
        " sie ogromne, brudne bajoro. Jego brzegi pokryte s^a przegni^lymi li^s^cmi"+
        " i pojed^ynczymi k^lodami rosn^acych tu niegdy^s drzew. Pod^lo^ze jest tutaj"+
        " do^s^c ^sliskie dlatego z ka^zdym krokiem nale^zy zachowa^c ostro^zno^s^c."+
        " Z pewno^sci^a bagna te poch^lon^e^ly ju^z niejednego, nieroztropnego"+
        " podr^o^znika, a niepozorne bajoro gdyby tylko potrafi^lo m^owi^c, opowie"+
        "dzia^lo by niejedn^a histori^e o tajemnicach jakie spoczywaj^a na jego dnie. ";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str+="Krzewy i drzewa niemal ca^lkowicie ust^api^ly miejsce wolnej prze"+
        "strzeni, tworz^ac jakby b^lotnist^a polanke, na ^kt^orej ^srodku znajduje"+
        " sie ogromne, brudne bajoro. Jego brzegi pokryte s^a przegni^lymi li^s^cmi"+
        " i pojed^ynczymi k^lodami rosn^acych tu niegdy^s drzew. Pod^lo^ze jest tutaj"+
        " do^s^c ^sliskie dlatego z ka^zdym krokiem nale^zy zachowa^c ostro^zno^s^c."+
        " Z pewno^sci^a bagna te poch^lon^e^ly ju^z niejednego, nieroztropnego"+
        " podr^o^znika, a niepozorne bajoro gdyby tylko potrafi^lo m^owi^c, opowie"+
        "dzia^lo by niejedn^a histori^e o tajemnicach jakie spoczywaj^a na jego dnie. ";
    }

    str+="\n";
    return str;
    }
        