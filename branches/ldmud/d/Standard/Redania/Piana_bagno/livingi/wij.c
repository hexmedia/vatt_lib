/* Autor: Avard
   Opis : Fenek
   Data : 28.12.07
   Info : W przyszlosci, jak beda ziola na odtrutki i medycy to warto
          zrobic mu trujace ataki. ;) */

inherit "/std/creature";
inherit "/std/combat/unarmed";
inherit "/std/act/attack";
inherit "/std/act/action";
inherit "/std/act/ask_gen";
inherit "/std/act/domove";
#include <std.h>
#include <ss_types.h>
#include <macros.h>
#include <stdproperties.h>
#include <pl.h>
#include <money.h>
#include <object_types.h>
#include "dir.h"


int
czy_atakowac()
{
    if(TP->is_humanoid() && TP->query_rasa() != "utopiec")
        return 1;
    else
        return 0;
}


void
create_creature()
{
    set_living_name("wijzbagienobokpiany");
    ustaw_odmiane_rasy("wij");
    dodaj_nazwy("skolopendromorf");
    set_gender(G_MALE);

    random_przym(({"przera^zaj^acy","przera^zaj^acy", "okrutny","okrutni",
        "w^sciek^ly", "w^sciekli"}),({"szybki","szybcy","ruchliwy",
        "ruchliwi"}),({"wielki","wielcy","ogromny","ogromni","d^lugi",
        "d^ludzy"}), 2);

    set_long("Ponad dwumetrowe, podzielone na segmenty cia^lo, z kt^orych "+
        "ka^zdy pokryty grub^a warstw^a  chitynowego, b^lyszcz^acego "+
        "pancerza, zdaje si^e zupe^lnie nie kr^epowa^c ruch^ow tego "+
        "wyro^sni^etego paskudztwa. Dziesi^atki masywnych, pokrytych "+
        "drobnymi haczykami odn^o^zy niczym fala, harmonijnie poruszaj^ac "+
        "si^e sprawiaj^a wra^zenie, jakby wij z niesamowit^a pr^edko^sci^a "+
        "wr^ecz p^lyn^a^l ponad gruntem. Z owalnego ^lba, zaopatrzonego w "+
        "masywne szcz^eki z kolcami jadowymi, wyrastaj^a d^lugie czu^lki, "+
        "kt^orymi bada otoczenie w poszukiwaniu smakowitego k^asku. \n");

    set_stats (({40+random(30),40+random(20),70+random(30),30+random(10),
                100+random(10)}));
    add_prop(CONT_I_WEIGHT, 100000+random(10000));
    add_prop(CONT_I_HEIGHT, 50+random(10));
    add_prop(LIVE_I_NEVERKNOWN, 1);
    set_aggressive(&czy_atakowac());
    set_attack_chance(91+random(10));
    set_npc_react(1);

    set_skill(SS_2H_COMBAT,     50 + random(24));
    set_skill(SS_SWIM,          80);
    set_skill(SS_UNARM_COMBAT,  70 + random(14));
    set_skill(SS_PARRY,         26);
    set_skill(SS_DEFENCE,       24);
    set_skill(SS_AWARENESS,     64 + random(10));
    set_skill(SS_BLIND_COMBAT,  50 + random(50));
    set_skill(SS_HIDE,          30 + random(40));
    set_skill(SS_SNEAK,         30 + random(30));

    set_attack_unarmed(0, 20, 22, W_SLASH,  100, "kolce", "jadowy");

    set_hitloc_unarmed(0, ({ 1, 1, 1 }), 10, "�eb");
    set_hitloc_unarmed(1, ({ 5, 6, 4 }), 80,
        ({({"jeden z segment�w", "jednego z segment�w", "jednemu z segment�w", "jeden z segment�w",
          "jednym z segment�w", "jendym z segment�w"}), PL_MESKI_NOS_NZYW}));
    set_hitloc_unarmed(2, ({ 1, 1, 1 }), 10, "odn�a");

    add_prop(LIVE_I_SEE_DARK, 1);

    set_random_move(10);

   /* add_leftover("/std/leftover", "odn^o^ze", 2, 0, 0, 0, 0, O_JEDZENIE);
    add_leftover("/std/leftover", "z^ab", random(5) + 2, 0, 1, 0, 0, O_KOSCI);
    add_leftover("/std/leftover", "czaszka", 1, 0, 1, 1, 0, O_KOSCI);
    add_leftover("/std/leftover", "ko^s^c", random(3) + 2, 0, 1, 1, 0, O_KOSCI);

    if(random(2))//zielarz tego czasem potrzebuje, wi^ec niech b^edzie trudniej
        add_leftover("/std/leftover", "serce", 1, 0, 0, 1, 0, O_JEDZENIE);*/
}

