inherit "/std/dylizans_in.c";
#pragma unique
#include <stdproperties.h>
#include <mudtime.h>

string
sianko()
{
    string str;
    if(pora_roku() == MT_LATO || pora_roku() == MT_WIOSNA)
        str="Wsz�dzie wok� porozrzucane jest �wie�e siano, kt�rego "+
        "intensywny zapach uderza ci do nozdrzy.\n";
    else
        str="Wsz�dzie wok� porozrzucane jest jakie� siano, "+
        "miejscami mo�e odrobin� nadgni�e.\n";

    return str;
}

void create_wnetrze()
{
	set_short("Na wozie");
	set_long("Znajdujesz sie na niezbyt ekskluzywnym, wiejskim wozie."
	+" Na jego przedzie znajduje si� niewielkie siedzonko"
	+" przeznaczone zapewne dla wo�nicy, wi�c chyba niestety musisz"
	+" zadowoli� si� sianem, kt�rego tu pe�no.\n");
	
	add_sit("na sianie", "na sianie", "z siana",4);
	add_sit("na przedzie", "na przedzie wozu", "z miejsca wo�nicy",1);
	add_prop(ROOM_I_INSIDE,1);
	/* Dodajemy komendy dla wysiadania z dyli�ansu
	    "wyskocz" jest zarezerwowane dla wyskakiwania z dyli�ansu 
	    w trakcie jazdy*/
	dodaj_komende("zejd�", "schodzi", "zej��");
	dodaj_komende("zeskocz", "zeskakuje", "zeskoczy�");
	
	/* Dodajemy eventy jazdy... */
	dodaj_event_jazdy("W�z ko�ysze si� niespokojnie.");
	dodaj_event_jazdy("Konie prychaj� cicho.");
        dodaj_event_jazdy("W�z podskoczy� na wyboju.");
	dodaj_event_jazdy("Wo�nica kr�tkimi cmokni�ciami i uderzeniami bata pop�dza konie.");
	
	/* I czas co jaki si� pojawiaj� */
	set_event_time(15);
	
    add_item("siano",
        "@@sianko@@");
}

int
test_na_usiadz(string str)
{
    if(!interactive(this_player()))
        return 0;

    if(str ~= "na przedzie" || str ~= "na przedzie wozu")
    {
        write("Ale to jest miejsce przeznaczone dla wo�nicy!\n");
        return 1;
    }

    if(!str)
    {
        this_player()->command("usiadz na sianie");
        return 1;
    }
}

int
test_na_poloz(string str)
{
    if(!interactive(this_player()))
        return 0;

    if(str ~= "si� na przedzie" || str ~= "si� na przedzie wozu")
    {
        write("Ale to jest miejsce przeznaczone dla wo�nicy!\n");
        return 1;
    }

    if(str~="si�")
    {
        this_player()->command("poloz sie na sianie");
        return 1;
    }
}

init()
{
  ::init();
    add_action("test_na_usiadz","usi�d�");
    add_action("test_na_poloz","po��");
}
