//zapiprzyłem spodnie z Piany, tylko je powiększyłem.
inherit "/std/armour";
        
#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <macros.h>
#include <materialy.h>
#include <object_types.h>
#include <pl.h>
void
create_armour()
{
	ustaw_nazwe("spodnie");
	dodaj_przym("gruby","grubi");
	dodaj_przym("po^latany","po^latani");
	
	set_long("Szerokie nogawki ci^e^zkich portek uszytych z "
		+"szarego lnu niemal ca^le pokryte s^a ^latami wszelkich "
		+"kszta^lt^ow, rozmiar^ow i odcieni. Kilka z nich jest "
		+"nawet z cienkiej ^swi^nskiej sk^ory. Na kolanach "
		+"ponaszywano je ju^z jedna na drugiej, tak ^ze sta^ly si^e "
		+"a^z sztywne. Zapina si^e je w kroku na kilka blaszanych " 
		+"guzik^ow, o dziwo kompletnych, jeden czy dwa cz^e^s^cr^o^zn^a "
		+"si^e nieco od innych. \n");

	set_slots(A_LEGS, A_HIPS);
	add_prop(OBJ_I_WEIGHT, 1000);
	add_prop(OBJ_I_VOLUME, 1900);
	add_prop(OBJ_I_VALUE, 6);

	add_prop(ARMOUR_S_DLA_RASY, "cz^lowiek");
	add_prop(ARMOUR_I_DLA_PLCI, 0);

	ustaw_material(MATERIALY_LEN, 95);
	ustaw_material(MATERIALY_SK_SWINIA, 5);

	set_type(O_UBRANIA);

	set_size("XXL");

}