//FIXME : rzuc w ciumka�� mieczem - b�dzie t�ok + brak reakcji na walk� (np. z tego propa z lokacji).

inherit "/std/dylizans.c";
#pragma unique
#include <pl.h>
#include <macros.h>
#include <stdproperties.h>
#include "../dir.h"

string
dlugi()
{
    string str;
    if(!query_ruszam_sie())
        str="Zwyk�y drabiniasty w�z nie wyr�nia si� niczym szczeg�lnym."
	+" Pod p�ociennym namiotem dostrzegasz ca�kiem sporo siana, co powinno"
	+" umili� podr� klientom. Zaprz�gni�te do wozu"
	+" dwa gniade konie wygl�daj� na wypocz�te i gotowe do podr�y.\n";
    else
        str="Zwyk�y drabiniasty w�z chyba nie wyr�nia si� niczym szczeg�lnym, "+
        "cho� ci�ko oceni� to w tym momencie. Pod p�ociennym namiotem "+
        "dostrzegasz ca�kiem sporo siana, co zapewne umila podr� klientom. "+
        "Zaprz�gni�te do wozu dwa gniade konie nie wygl�daj� na zm�czone, cho� "+
        "chrapi� przera�liwie.\n";
    return str;
}

/* ACHTUNG: po sklonowaniu dylizansu na lokacje trzeba na nim
 wywoa funkcje start(), zeby zaczal jezdzic */
void create_dylizans()
{
        setuid();
        seteuid(getuid());
	ustaw_nazwe("woz");
	set_long("@@dlugi@@");

	dodaj_przym("niewielki", "niewielcy");
	dodaj_przym("drabiniasty", "drabinia�ci");

	add_prop(OBJ_I_WEIGHT, 15000);
	add_prop(OBJ_I_VOLUME, 337500);

	/* Dodajemy komendy za pomoca ktorych mozna wsiasc do dylizansu*/
	dodaj_komende("wskocz", "wskakuje", "wskoczy�", "na", PL_BIE);
        dodaj_komende("wejd�", "wchodzi", "wej��", "do", PL_DOP);
        dodaj_komende("wejd�", "wchodzi", "wej��", "na", PL_BIE);

	/* Ustawiamy cene w groszach */
	ustaw_cene(9);

	/* Ustawiamy czas spedzany na postoju w sekundach */
	ustaw_czas_postoju(40);

	/* Ustawiamy ilosc miejsc w dylizansie */
	ustaw_ilosc_miejsc(4);

	/* Okreslamy trase dyliżansu
	      ** - postoj */
	//ustaw_trase( ({"**", "w", "n", "**", "e", "e", "**"}) );
	ustaw_trase( ({"**","w","sw","s","sw","w","s","w","nw","sw","s",
        "sw","sw","w","w","w","n","w","nw","w","sw","w","s","w","nw","sw",
        "w","sw","w","w","sw","w","sw","w","nw","sw","sw","w","s","w","sw",
        "nw","sw","sw","nw","w","w","w","sw","w","nw","**","nw","ne","n",
        "n","n","nw","n","nw","nw","n","ne","n","nw","nw","w","nw","nw",
        "n","ne","n","n","w","nw","nw","n","nw","nw","n","n","n","n","**"}));
	/* Ustawiamy nazwy kolejnych postojow, przypadek gram. dowolny,
	    wazne tylko, aby zgadzalo sie z tym co gada woznica */
	ustaw_postoje( ({"podbramia Rinde", "Bia�ego Mostu", "Piany"}) );

	/* Okreslamy szybkosc dylizansu - czas jaki spedza na lokacji podczas
jazdy */
	ustaw_szybkosc(3);

	/* Podajemy sciezke do woznicy... (dylizans klonuje npca sam)*/
	ustaw_woznice(DYLEK_PIANA_RINDE+"przyglup.c");

	/* Podajemy sciezke do wnetrza dylizansu */
	ustaw_wnetrze(DYLEK_PIANA_RINDE+"dylek_in.c");

	/* Ustawiamy komunikat na wjechanie dylizansu na lokacje... */
	set_enter_msg("@@ret_Kierunek_z@@ wy�ania si� "+short());

	/* ... i na wyjechanie ( w obu mozna uzywac vbfc)*/
	set_leave_msg("Przy rytmicznym t�tencie ko�skich kopyt w�z oddala si�@@ret_kierunek_na@@");

	/* Przydatne funkcje:
	     * ret_postoj() - zwraca nam aktualny postoj
	     * ret_nastepny_postoj() - zwraca nastepny postoj
	     * ret_kierunek_na - zwraca kierunek w ktorym wyjezdza dylizans
	     * ret_kierunek_z - zwraca kierunek z ktorego przyjechal dylizans
	*/
}

int wsiadz_inne()
{
	if(TP->query_race_name() ~= "elf")
	{
		woznica->command("'Tylko mi si� nie wychylaj, co bym k�opot�w "+
                "przez ciebie nie mia�!");
		//return 1;
	}
}

/* A tu zestawy glownie dla wosnicy... */

/* Akcje wykonywane kiedy woznica wyszedl z dylizansu i jest juz na zewnatrz*/
void msg_enter_out()
{
        if(ENV(TO)->dzien_noc())
	woznica->command("':cicho: Dojechalim do "+ret_postoj()+
                ". Za chwilunie w stronem "+ret_nastepny_postoj()+
                " zmierzamy.");
        else
        woznica->command("krzyknij Dojechalim do "+ret_postoj()+
                "! Za chwilunie w stronem "+ret_nastepny_postoj()+
                " zmierzamy.");
	woznica->command("pociagnij nosem poteznie");
}

/* Kiedy wchodzi do dylizansu i jest w srodku*/
void msg_enter_in()
{
	woznica->command("powiedz Nu, jadziem! Ino siem nie wychyla�!");
	woznica->command("usiadz na przedzie");

	woznica->command("emote chwyta lejce i �ywymi okrzykami pop�dza konie.");
}

/* Kiedy wchodzi do dylizansu, ale jest jeszcze na zewnatrz :P */
void msg_leave_out()
{
        if(ENV(TO)->dzien_noc())
            woznica->command("':cicho: No, to byndziem ruszali.");
        else
	   woznica->command("krzyknij Oodjazd!");

	woznica->command("emote rozglada si� w nadziei na "+
                "znalezienie jeszcze jednego klienta.");
}

/* No..i kiedy wychodzi z dyliżansu, ale jest jeszcze w srodku */
void msg_leave_in()
{
	woznica->command("powiedz Dotarlim do "+ret_postoj()+"!");
	woznica->command("wstan");
	woznica->command("podrap sie w zadek");
}

/* Akcje wykonywane kiedy gracza nie stać na podróż */
void msg_niestac()
{
	woznica->command("powiedz do "+TP->query_real_name(1)
		+" Ooo bratku, na taka podr� to cie chiba nista�.");
        woznica->command("wskaz na mala tabliczke");
}
