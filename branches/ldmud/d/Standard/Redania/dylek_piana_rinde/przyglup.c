
/* Autor: vera
   Opis : cholera wie, chyba fenek
   Data : 11.12.07

    pierdoln��em se tyciego que�cika u niego, a co ;]
    trzeba poczeka�, a� powie, �e g�odny, wtedy zapyta� to powie,
    przynie�� cokolwiek i wtedy powie, �e chcia�by jeszcze to popi�,
    najlepiej '�wie�ym mlekiem prosto od krowy' - jak mu si� taki
    przyniesie to dopiero dostajemy kapk� expa. ;)

*/
#pragma unique
#pragma strict_types
#include <std.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <stdproperties.h>
#include <pl.h>
#include <money.h>
#include <object_types.h>
#include <exp.h>
#include "../dir.h"

#define ZROBILI ("/d/Standard/questy/woznica/zrobili")
#define ROBIACY ("/d/Standard/questy/woznica/robiacy")
#define ROBIACY_2STAGE ("/d/Standard/questy/woznica/robiacy_2stage")
inherit "/std/dylizans_woznica.c";
// int czy_walka();
// int walka = 0;
int powiedzial_o_jedzeniu = 0;

int
nowy_robiacy(object kto)
{
    if (ENV(TO)==ENV(kto))
    {
        write_file(ROBIACY,kto->query_real_name()+ "#");
        return 1;
    }
    else
        return 0;
}

void
usun_robiacego(object kto)
{
    int i;
    string *arr;

    arr = explode(read_file(ROBIACY_2STAGE), "#");
    i = member_array(kto->query_real_name(), arr);

    if (i != -1)
        arr = exclude_array(arr, i, i+1);

    rm(ROBIACY_2STAGE);
    write_file(ROBIACY_2STAGE, implode(arr, "#") + "#");
}

void
usun_robiacego_1stage(object kto)
{
    int i;
    string *arr;

    arr = explode(read_file(ROBIACY), "#");
    i = member_array(kto->query_real_name(), arr);

    if (i != -1)
        arr = exclude_array(arr, i, i+1);

    rm(ROBIACY);
    write_file(ROBIACY, implode(arr, "#") + "#");
}

int
teraz_picie(object kto)
{
    if (ENV(TO)==ENV(kto))
    {
        write_file(ROBIACY_2STAGE, kto->query_real_name() + "#");
        usun_robiacego_1stage(TP);
        return 1;
    }
    else
        return 0;
}

void
daj_expa(object komu,object zaco)
{
    set_alarm(1.0, 0.0, "powiedz_gdy_jest", komu, "powiedz do " +
                OB_NAME(komu) + " Mmm, o to chodzi�o! Dzi�ki ci!");
    komu->catch_msg("Czujesz si� wzbogacon"+TP->koncowka("y","a","e")+
        " o nowe do�wiadczenie.\n");
    komu->increase_ss(3,EXP_QUEST_CIUMKALA_INT);
    zaco->remove_object();
}

void
enter_inv(object ob, object from)
{
    ::enter_inv(ob, from);

    if(!interactive(from))
        return;

    //sprawd�my wpierw, czy quest zosta� zrobiony
    if (member_array(TP->query_real_name(),
            explode(read_file(ZROBILI), "#")) != -1)
    {
        set_alarm(1.0, 0.0, "command", "odloz " + OB_NAME(ob));
        return;
    }

    //a mo�e to wr�g? wrog�w nie obs�ugujemy z questem.
    if (member_array(TP->query_real_name(),query_wrogowie()) != -1)
    {
        set_alarm(1.0, 0.0, "command", "odloz " + OB_NAME(ob));
        return;
    }

    if (ob->query_food())
    {
        if (member_array(TP->query_real_name(),
            explode(read_file(ROBIACY), "#")) != -1)
        {
            if(ob->query_amount() > 600)
            {
            set_alarm(1.0, 0.0, "powiedz_gdy_jest", TP, "powiedz do " +
                OB_NAME(TP) + " Dzi�ki ci! My�la�em, �e ju� zdechn� tu z "+
                "g�odu... Hmmm... Gdybym m�g� ci� jeszcze tylko prosi� "+
                "o co� do popicia...Och, jak mi si� chce �wie�ego mleka.");
            ob->remove_object();
            teraz_picie(TP);
            }
            else
            {
            set_alarm(1.0, 0.0, "powiedz_gdy_jest", TP, "powiedz do " +
                OB_NAME(TP) + " Dzi�ki! My�la�em, �e ju� zdechn� tu z "+
                "g�odu... Hmmm... Ale ja si� chyba tym nie najem, no "+
                "patrzaj na mnie - chop jak d�b...");
            set_alarm(2.0, 0.0, "command", "odloz " + OB_NAME(ob));
            //ob->remove_object();
            }
        }
        else
        {
            set_alarm(2.0, 0.0, "command", "odloz " + OB_NAME(ob));
        }
        return;
    }
    if(ob->query_beczulka() && ob->query_ilosc_plynu() > 10)
    {
        if(member_array(lower_case(TP->query_name()),
            explode(read_file(ROBIACY_2STAGE), "#")) != -1)
        {
            //if(ob->query_opis_plynu() ~="�wie�ego mleka prosto od krowy")
            if(wildmatch("*^swie^zego mleka*",ob->query_opis_plynu()))
            {
                //FANTASTICO, �EL DON. QUEST OVER.
                set_alarm(1.0,0.0,"powiedz_gdy_jest",TP,
                "usmiechnij sie szeroko");
                string naz = ob->short(PL_DOP);
                set_alarm(2.0,0.0,"powiedz_gdy_jest",TP,
                "emote jednym haustem wychyla zawarto�� "+naz+".");
                set_alarm(3.0,0.0,"daj_expa",TP,ob);
                write_file(ZROBILI, lower_case(TP->query_name()) + "#");
                usun_robiacego(TP);
            }
            else
            {
            set_alarm(1.0, 0.0, "powiedz_gdy_jest", TP, "powiedz do " +
                OB_NAME(TP) + " Yhmm, no dzi�ki, ale wola�bym �wie�e "+
                "mleko.");
            set_alarm(2.0, 0.0, "command", "odloz " + OB_NAME(ob));
            if(ENV(ob) == TO)
                ob->remove_object(); //just in case
            }
        }
        else
            set_alarm(2.0, 0.0, "command", "odloz " + OB_NAME(ob));
    }
    else
        set_alarm(2.0, 0.0, "command", "odloz " + OB_NAME(ob));

}

string
pierdolnik()
{
    object *gracze = ({});

    if(ENV(TO))
        gracze = FILTER_PLAYERS(all_inventory(ENV(TO)));

    if(!sizeof(gracze))
        return "";

    object gracz = gracze[random(sizeof(gracze))];

    if(gracz->query_gender() && gracz->query_race_name() ~= "cz�owiek") //kobitka i cz�ek
        return "oczko kobiecie";
    else if(gracz->query_race_name() ~= "elf")
        return "popatrz nieufnie na "+OB_NAME(gracz);
    else if(gracz->query_race_name() ~= "nizio�ek" ||
             gracz->query_race_name() ~= "krasnolud")
        return "usmiechnij sie do "+OB_NAME(gracz);
    else //p�elf albo gnom
        return "popatrz ciekawsko na "+OB_NAME(gracz);
}

void
zeruj_powiedzial()
{
    powiedzial_o_jedzeniu = 0;
}

string
questowy()
{
    switch(random(3))
    {
        case 0:
                powiedzial_o_jedzeniu = 1;
                set_alarm(30.0,0.0,"zeruj_powiedzial");
                return "'Ech, przyda�oby si� co� na z�b."; break;
        case 1:
                powiedzial_o_jedzeniu = 1;
                set_alarm(30.0,0.0,"zeruj_powiedzial");
                return "':klepi�c si� po brzuchu: Zjad�bym co�.";  break;
        case 2:
                powiedzial_o_jedzeniu = 1;
                set_alarm(30.0,0.0,"zeruj_powiedzial");
                return "':klepi�c si� po brzuchu: Je��! Je��! Je��!.";  break;
        default: break;
    }
}

void
create_woznica()
{
    setuid();
    seteuid(getuid());

    set_living_name("Ciumka�a");
    set_title(", wo�nica");
    ustaw_imie(({"ciumka�a","ciumka�y","ciumkale","ciumka��","ciumka��","ciumkale"}),
            PL_ZENSKI);
    ustaw_odmiane_rasy("mezczyzna");
    dodaj_nazwy("przyglup");
    set_gender(G_MALE);
    dodaj_przym("postawny","postawni");
    dodaj_przym("przyg�upi","przyg�upi");
    //set_spolecznosc("Dupa");
    set_long("Wyd�u�ona, nieproporcjonalnie du�a twarz, "+
    "wiecznie rozchylone, ob�linione usta oraz malutkie, "+
    "g��boko osadzone, czarne oczy t�pym spojrzeniem "+
    "omiataj�ce okolic� z pewno�ci� uniemo�liwiaj� zaliczenie "+
    "tego m�czyzny do miejscowych my�licieli i zdaj� si� by� "+
    "doskona�ym przyk�adem na to, i� matka natura nie zawsze "+
    "obdziela wszystkich po r�wno intelektem i m�dro�ci�. "+
    "Po�atane spodnie i przepocona, cienka koszula �ci�le "+
    "przylegaj�ca do cia�a uwidacznia pot�ny tors i d�ugie "+
    "masywne r�ce, kt�rymi co jaki� czas m�czyzna czochra si� "+
    "zawzi�cie po g�owie chc�c pozby� sie zapewne dokuczliwych "+
    "�yj�tek beztrosko harcuj�cych po�r�d g�szczu jego "+
    "brudnych, sko�tunionych w�os�w.\n");
    set_stats (({ 150+random(20), 30, 165+random(10), 4, 120+random(20) }));
    add_prop(CONT_I_WEIGHT, 125003);
    add_prop(CONT_I_HEIGHT, 205);
    add_prop(NPC_I_NO_RUN_AWAY, 1);

    set_act_time(50);
    add_act("emote smarka w palce, po czym energicznym ruchem wyciera "+
            "r�k� o nogawk� swoich spodni.");
    add_act("emote beka dono�nie.");
    add_act("mlasnij");
    add_act("podrap sie po jajach");
    add_act("podrap sie w zadek");
    add_act("@@questowy@@");
    add_act("popatrz na kobiete");
    add_act("@@pierdolnik@@");
    add_act("emote mruczy co^s do siebie pod nosem.");
    add_prop(NPC_M_NO_ACCEPT_GIVE, 0);
    set_cact_time(10);
    add_cact("krzyknij Pomocy!");
    add_cact("emote wpada w sza�.");
    add_cact("warknij");

    set_default_answer(VBFC_ME("default_answer"));

    add_armour(REDANIA+"dylek_piana_rinde/ubranka/koszula.c");
    add_armour(REDANIA+"dylek_piana_rinde/ubranka/spodnie.c");

    add_ask(({"jedzenie","co� do jedzenia","jedzonko","�arcie",
                "co� na z�b"}),
        VBFC_ME("pyt_o_jedzenie"));
    add_ask(({"picie","co� do picia","co� do popicia","mleko",
                "�wie�e mleko", "mleko prosto od krowy","mleko od krowy"}),
        VBFC_ME("pyt_o_picie"));
    add_ask(({"w�z","sw�j w�z","robot�","prac�"}),
        VBFC_ME("pyt_o_woz"));

}

void
powiedz_gdy_jest(object player, string tekst)
{
    if (environment(this_object()) == environment(player))
        this_object()->command(tekst);
}
string
default_answer()
{
    if(!query_attack())
    {
        if(random(2))
        set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
            "usmiechnij sie glupawo");
        else
        set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
            "wzrusz ramionami");
    }
    else
    {
        set_alarm(0.5,0.0,"powiedz_gdy_jest", TP, "krzyknij");
    }
    return "";
}

string
pyt_o_jedzenie()
{
    //a mo�e to wr�g? wrog�w nie obs�ugujemy z questem.
    if (member_array(TP->query_real_name(),query_wrogowie()) != -1)
    {
        command("wzrusz ramionami");
        return "";
    }

    if(!query_attack())
    {
        if(powiedzial_o_jedzeniu)
        {
            if (member_array(lower_case(TP->query_name(0)),
                    explode(read_file(ZROBILI), "#"))!= -1)
            {
                set_alarm(1.0, 0.0, "powiedz_gdy_jest", TP, "powiedz do " +
                OB_NAME(TP) + " Jeszcze raz dzi�ki ci!");
                return "";
            }
            else
            {
                    //czyli robi questa, ale nie przyniosl jedzenia jeszcze
                if (member_array(lower_case(TP->query_name()),
                    explode(read_file(ROBIACY), "#"))!= -1)
                        set_alarm(1.0, 0.0, "powiedz_gdy_jest", TP, "powiedz do " +
                    OB_NAME(TP) + " Ja to tam wymagaj�cy nie jestem, byle da�o "+
                        "si� to zje�� i by�o co, hej!");
                else if (member_array(lower_case(TP->query_name()),
                    explode(read_file(ROBIACY_2STAGE), "#"))!= -1)//przyni�s� picie
                        set_alarm(1.0, 0.0, "powiedz_gdy_jest", TP, "powiedz do " +
                    OB_NAME(TP) + " Nuu, dzi�ki, ale chcia�em se jeszcze czym� to "+
                        "popi�...Ech, co ja bym da� za mleko prosto od krowy, jak "+
                        "w domu...");
                else //czyli jeszcze nic a nic nie zrobi�
                {
                    nowy_robiacy(TP);

                    switch(random(2))
                    {
                    case 0: set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
                        "'No, zjad�bym cosik, cokolwiek co da si� prze�kn��.");break;
                    case 1: set_alarm(0.5,0.0,"powiedz_gdy_jest", TP,
                        "'Te� bym co� zjad�.");break;
                    }
                }
            }
        }
        else
        {
            set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
                "wzrusz ramionami");
        }
    }
    else
    {
        set_alarm(0.5,0.0,"powiedz_gdy_jest", TP, "krzyknij");
    }
    return "";
}

string
pyt_o_picie()
{
    //a mo�e to wr�g? wrog�w nie obs�ugujemy z questem.
    if (member_array(TP->query_real_name(),query_wrogowie()) != -1)
    {
        command("wzrusz ramionami");
        return "";
    }

    if(!query_attack())
    {
        if (member_array(lower_case(TP->query_name(0)),
                explode(read_file(ZROBILI), "#"))!= -1)
        {
            set_alarm(1.0, 0.0, "powiedz_gdy_jest", TP, "powiedz do " +
            OB_NAME(TP) + " Jeszcze raz dzi�ki ci!");
        }
        else if (member_array(lower_case(TP->query_name()),
               explode(read_file(ROBIACY_2STAGE), "#"))!= -1)
        {
            set_alarm(1.0, 0.0, "powiedz_gdy_jest", TP, "powiedz do " +
            OB_NAME(TP) + " No, napi�bym si� jeszcze �wie�ego mleka.");
        }
        else
            set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
                "wzrusz ramionami");
    }
    return "";
}

string
pyt_o_woz()
{
    if(!query_attack())
    {
        switch(random(2))
        {
            case 0: set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
                "'No, jak wida� hehe, je�d�e se w te i nazad i tak w ko�o, kruca.");break;
            case 1: set_alarm(0.5,0.0,"powiedz_gdy_jest", TP,
                "':pr�buj�c skry� za�enowanie: Eeee....nooo...Jakby to...zapierdalam se w k�ko z Piany "+
                "do Rinde, a z Rinde do Piany..."); break;
        }
    }
    else
    {
        set_alarm(0.5,0.0,"powiedz_gdy_jest", TP, "'Zostaw mnie!");
    }
    return "";
}
/*
void
add_introduced(string imie_mia, string imie_bie)
{
    set_alarm(2.0, 0.0, "return_introduce", find_player(imie_mia));
}*/
void
return_introduce(object ob)
{
    command("emote u�miecha si� g�upawo, a w k�cikach jego sp�kanych ust "+
        "dostrzegasz zbieraj�c� si� �lin�.");
    command("przedstaw si�");
}

void
emote_hook(string emote, object wykonujacy)
{
    switch (emote)
    {
        case "kopnij": set_alarm(1.5, 0.0, "kopie", wykonujacy);
                    break;
        case "opluj" : set_alarm(2.5, 0.0, "kopie", wykonujacy);
                    break;
        case "spoliczkuj": set_alarm(1.5, 0.0, "policzkuje", wykonujacy);
                    break;
        case "poca^luj": set_alarm(1.5,0.0, "caluje",wykonujacy);
                    break;
        case "przytul":
        case "obejmij": set_alarm(1.5,0.0, "malolepszy",wykonujacy);
                    break;
        case "poglaszcz": set_alarm(0.5, 0.0, "malolepszy", wykonujacy);
                    break;
        case "poklep": set_alarm(0.5, 0.0, "malolepszy", wykonujacy);
                    break;
        case "usmiech": set_alarm(0.5, 0.0, "malolepszy", wykonujacy);
                    break;
        case "mrugnij": set_alarm(0.5, 0.0, "malolepszy", wykonujacy);
                    break;
    }
}

void
kopie(object wykonujacy)
{
    switch(random(4))
    {
        case 0: command("'Dlaczego to robisz?");break;
        case 1: command("'Przesta� prosz�.");break;
        case 2: command("'Ej�e!");break;
        case 3: command("zdziw sie");break;
    }
}

void
policzkuje(object wykonujacy)
{
    switch(random(2))
    {
        case 0: command("spoliczkuj "+OB_NAME(wykonujacy));break;
        case 1: command("emote wystawia �rodkowy palec.");break;
    }
}

void
caluje(object wykonujacy)
{
    if(wykonujacy->query_gender() == 1)
    {
        switch(random(2))
        {
            case 0: command("emote zaczyna si� �lini�.");break;
            case 1: command("usmiechnij sie glupio");break;
        }
    }
    else
    {
        switch(random(2))
        {
            case 0: command("emote wyciera usta.");break;
            case 1: command("spojrzyj ze zdziwieniem na "+
                OB_NAME(wykonujacy));break;
        }
    }
}

void
malolepszy(object wykonujacy)
{
    switch(random(3))
    {
        case 0: command("pokiwaj");break;
        case 1: command("przytul "+OB_NAME(wykonujacy)+" mocno");break;
        case 2: command("obejmij "+OB_NAME(wykonujacy)+" entuzjastycznie");break;
    }
}

/*
void
attacked_by(object wrog)
{




    if(walka==0)
    {
        set_alarm(10.0, 0.0, "czy_walka", 1);
        walka = 1;
        return ::attacked_by(wrog);
    }
    else
    {
        walka = 1;
        return ::attacked_by(wrog);
    }
}

int
czy_walka()
{
    if(!query_attack())
    {
        command("powiedz Po walce.");
        walka = 0;
        return 1;
    }
    else
    {
       set_alarm(5.0, 0.0, "czy_walka", 1);
    }

}*/
