inherit "/std/pattern";

#include <pattern.h>
#include "dir.h"

public void
create()
{
    set_pattern_domain(PATTERN_DOMAIN_BLACKSMITH);
    set_item_filename(ZAKON_ZBROJE + "zbroje/gruba_lamelkowa_Lc.c");
    set_estimated_price(5000);
    set_time_to_complete(10000);
    enable_want_sizes();
}
