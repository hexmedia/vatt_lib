inherit "/std/pattern";

#include <pattern.h>
#include "dir.h"

public void
create()
{
    set_pattern_domain(PATTERN_DOMAIN_BLACKSMITH);
    set_item_filename(ZAKON_ZBROJE + "koszulki/krotka_kolcza_Lc.c");
    set_estimated_price(2160);
    set_time_to_complete(10800);
    enable_want_sizes();
}
