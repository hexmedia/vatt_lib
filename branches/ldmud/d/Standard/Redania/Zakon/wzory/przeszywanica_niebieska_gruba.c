inherit "/std/pattern";

#include <pattern.h>
#include "dir.h"

public void
create()
{
    set_pattern_domain(PATTERN_DOMAIN_BLACKSMITH);
    set_item_filename(ZAKON_ZBROJE + "przeszywanice/niebieska_gruba_Mc");
    set_estimated_price(1550);
    set_time_to_complete(7200);
    enable_want_sizes();
}
