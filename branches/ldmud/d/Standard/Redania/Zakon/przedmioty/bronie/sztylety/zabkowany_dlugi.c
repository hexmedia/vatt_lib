/* autor: Valor
   opis: Sniegulak
   wykonane: 06.05.07
*/
inherit "/std/weapon";
#include <macros.h>
#include <stdproperties.h>
#include <wa_types.h>
#include <pl.h>
#include <materialy.h>
#include <object_types.h>
    
void create_weapon() 
{
        ustaw_nazwe("n^o^z");
        dodaj_przym("z^abkowany", "z^abkowani");
        dodaj_przym("d^lugi", "d^ludzy");
                    
        set_long("Ogl^adaj^ac te bro^n na pierwszy rzut oka zauwa^zasz "+
            "ma^le z^abki wykute po jednej stronie ostrza. Ich lekko "+
            "zagi^ete ko^nc^owki przy wyci^aganiu sztyletu z rany zadaj^a "+
            "szarpane rany, i powoduj^a zwi^ekszone krwawienie. Ostrze jest "+
            "wyj^atkowo d^lugie, i do^s^c grube by przebi^c mocn^a "+
            "kolczug^e. Zrobione ze stali o b^lyszcz^acej powierzchni "+
            "odbija najdrobniejszy promyk ^swiat^la. R^ekoje^s^c sztyletu "+
            "jest wyprofilowana i posiada niewielki jelec pomocny w "+
            "parowaniu s^labszych cios^ow. G^lownia sztyletu odlana "+
            "jest w kszta^lcie diamentu i zako^nczona ma^lym dziwnie "+
            "wygl^adaj^acym kolcem. \n");
        
        set_hit(29);
        set_pen(16);
        set_wt(W_KNIFE);
        set_dt(W_IMPALE | W_SLASH);
        set_hands(A_ANY_HAND);
     
        add_prop(OBJ_I_VALUE, 1600);
        add_prop(OBJ_I_WEIGHT, 400);
        add_prop(OBJ_I_VOLUME, 400);   
        set_type(O_BRON_SZTYLETY);
        ustaw_material(MATERIALY_STAL, 90);
        ustaw_material(MATERIALY_SK_BYDLE, 10);
}
 