
/* Autor: Avard
   Opis : Sniegulak
   Data : 25.04.07 */
inherit "/std/weapon";

#include "/sys/formulas.h"
#include <stdproperties.h>
#include <wa_types.h>
#include <pl.h>
#include <materialy.h>
#include <object_types.h>


void
create_weapon()
{
    ustaw_nazwe("miecz");
                    
    dodaj_przym("karmazynowy","karmazynowi");
    dodaj_przym("d^lugi","d^ludzy");

    set_long("Bro^n wykonana niepoznan^a dot^ad powszechnie ludziom "+
        "technik^a kowalstwa jest receptura b^ed^aca w posiadaniu Zakonu "+
        "Karmazynowych Ostrzy. Wed^lug nieoficjalnych ^xr^ode^l receptura "+
        "pochodzi z samego Mahakamu. Jak zosta^la ona wyci^agni^eta spod "+
        "pilnie strze^zonych g^or krasnoludzkich na ^swiat^lo dzienne - "+
        "tego nie wiadomo. Wiadomym jest natomiast, ^ze dzi^eki tej "+
        "szczeg^olnej recepturze znakiem rozpoznawczym wspomnianego wy^zej "+
        "Zakonu na polu bitwy s^a w^la^snie ostrza, w blasku s^lo^nca "+
        "po^lyskuj^ace delikatnym, karmazynowym odcieniem. D^luga klinga "+
        "wypolerowana i naostrzona, tak ^ze potrafi^laby przeci^a^c "+
        "niejedn^a grub^a ga^l^a^x jest d^luga na trzy ^lokcie. Bro^n "+
        "jest idealnie wywa^zona za pomoc^a masywniejszego trzonu, "+
        "ob^lo^zonego dziwna sk^or^a tak by nawet spocona d^lo^n mog^la "+
        "pewnie trzyma^c ten miecz. Jelec jest zagi^ety lekko w stron^e "+
        "sztychu. Dok^ladno^s^c i dba^lo^s^c wykonania, wskazuje i^z bro^n "+
        "t^a wykona^l mistrz kowalski.\n");

    add_prop(OBJ_I_WEIGHT, 2500);
    add_prop(OBJ_I_VOLUME, 2500);
    add_prop(OBJ_I_VALUE, 30000);
    add_prop(OBJ_M_NO_SELL, 1);
   
    set_hit(24);
    set_pen(31);

    set_wt(W_SWORD);
    set_dt(W_IMPALE | W_SLASH);
    ustaw_material(MATERIALY_STAL, 80);
    ustaw_material(MATERIALY_SK_SWINIA, 20);
    set_type(O_BRON_MIECZE);

    set_hands(A_ANY_HAND);
}
