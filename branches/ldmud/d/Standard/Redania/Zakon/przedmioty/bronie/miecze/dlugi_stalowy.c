/* autor: Valor
   opis: Sniegulak
   wykonane: 06.05.07
*/
inherit "/std/weapon";
#include <macros.h>
#include <stdproperties.h>
#include <wa_types.h>
#include <pl.h>
#include <materialy.h>
#include <object_types.h>
    
void create_weapon() 
{
        ustaw_nazwe("miecz");
        dodaj_przym("d^lugi", "d^ludzy");
        dodaj_przym("stalowy", "stalowi");
                    
        set_long("Prostota tej broni jest od razu widoczna. Brak "+
            "jakichkolwiek zdobie^n sugeruje i^z jest to zwyk^ly "+
            "przeci^etny miecz, lecz gdy przyjrzysz si^e dok^ladnie, "+
            "wida^c ^ze zosta^l ona wykuty z du^z^a precyzj^a. G^lownia "+
            "wykonana z hartowanej stali jest w troch^e innym odcieniu ni^z "+
            "p^laz co mo^ze wskazywa^c na inna obr^obk^e materia^lu. Sztych "+
            "b^ed^acy idealnie wyprofilowany, tak by mieczem tym mo^zna "+
            "by^lo wykonywa^c dok^ladne pchni^ecia i jednocze^snie ci^a^c z "+
            "zamachu. Jelec troch^e masywniejszy razem z trzonem "+
            "wywa^zaj^a odpowiednio bro^n. \n");
        
        set_hit(23);
        set_pen(25);
        set_wt(W_SWORD);
        set_dt(W_IMPALE | W_SLASH);
        set_hands(A_ANY_HAND);
     
        add_prop(OBJ_I_VALUE, 5200);
        add_prop(OBJ_I_WEIGHT, 1000);
        add_prop(OBJ_I_VOLUME, 1000);   
        set_type(O_BRON_MIECZE);
        ustaw_material(MATERIALY_STAL, 90);
        ustaw_material(MATERIALY_SK_BYDLE, 10);
}