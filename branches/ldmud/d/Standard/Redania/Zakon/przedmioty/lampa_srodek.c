/* Autor: Avard
   Opis : Sniegulak
   Data : 25.04.07 */

inherit "/std/lampa_uliczna";
#include <stdproperties.h>
#include "dir.h"

void
create_street_lamp()
{
    set_long("Przyczepiona do ^sciany oliwna lampa o^swietla komnat^e w "+
        "nocy. Jej prostota i brak zdobie^n ^swiadczy^c mog^a, ^ze "+
        "w^la^sciciel przedk^lada funkcjonalno^s^c nad wygl^ad.\n");
    set_light_message("Jeden z giermk^ow zapala lamp^e.\n");
    set_extinguish_message("Jeden z giermk^ow gasi lamp^e.\n");
}