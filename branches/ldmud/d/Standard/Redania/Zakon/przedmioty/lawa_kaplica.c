/* Autor: Avard
   Opis : Sniegulak
   Data : 25.04.07 */

inherit "/std/object";

#include "dir.h"
#include <cmdparse.h>
#include <pl.h>
#include <macros.h>
#include <stdproperties.h>
#include <object_types.h>
#include <materialy.h>

void create_object()
{
    ustaw_nazwe("^lawa");
    dodaj_przym("d^lugi", "d^ludzy");
    dodaj_przym("d^ebowy", "d^ebowi");

    set_long("Masywna ^lawa, starannie wykonana z mocnego d^ebowego drewna "+
        "pomie^sci kilka os^ob. Nie posiada zdobie^n a jedynie surow^a "+
        "prostot^e wykonczenia.\n");
    add_prop(OBJ_I_WEIGHT, 7500);
    add_prop(OBJ_I_VOLUME, 7500);
    add_prop(OBJ_I_VALUE, 300);
    set_type(O_MEBLE);
    ustaw_material(MATERIALY_DR_DAB);
    make_me_sitable("na", "na ^lawie", "z ^lawy", 2);
    //set_owners(({RINDE_NPC + "krepp"}));TODO FIXME TRALALA ACHTUNG!!
}
