/* autor: Valor
   opis: ^Sniegulak
   dnia: 9 maja 07
*/
inherit "/std/armour";
#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <macros.h>
#include <materialy.h>
#include <object_types.h>
#include <pl.h>
void
create_armour()
{
    ustaw_nazwe("nagolenniki");
    dodaj_przym("rynienkowy","rynienkowi");
    dodaj_przym("lekki","lekcy");
    set_long("Nagolenniki te zrobione s^a z odlanych ze stali rynien. "+
        "Rozgrzane i potem kute przybra^ly odpowiedni kszta^lt pasuj^acy "+
        "prawie na ka^zdy gole^n, nast^epnie zosta^ly przyozdobione "+
        "r^o^znymi zawi^lymi wzorami. Czerwone i mocne obicie, chroni "+
        "dodatkowo nog^e, a du^za ilo^s^c pask^ow pozawala na lepsze "+
        "umiejscowienie zbroi. \n");
    set_slots(A_SHINS);
    
    add_prop(OBJ_I_WEIGHT, 2500); 
    add_prop(OBJ_I_VOLUME, 3000);
    add_prop(OBJ_I_VALUE, 1700);
    
    set_type(O_UBRANIA);
    
    ustaw_material(MATERIALY_STAL, 100);
    set_size("L");
    add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    set_ac(A_SHINS, 15,15,12);
}
