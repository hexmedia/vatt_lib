/* Acheron */
inherit "/std/armour";

#include <stdproperties.h>
#include <wa_types.h>
#include <formulas.h>
#include <macros.h>
#include </sys/materialy.h>
#include <object_types.h>

void
create_armour()
{
    ustaw_nazwe("przeszywanica");
    set_type(O_ZBROJE);

    dodaj_przym("z^o^lty","^z^o^lci");
    dodaj_przym("d^lugi","d^ludzy");

    ustaw_material(MATERIALY_BAWELNA, 80);
    ustaw_material(MATERIALY_SK_SWINIA, 20);

    set_long("D^luga przeszywanica koloru z^o^ltego stanowi ochrone dla " +
             "tu^lowia oraz r�k, za� p^laty sk^ory i bawe^lny przyszyte do " +
             "dolnej cz^e�ci tego aketonu, rozci^ete dodatkowo po �rodku " +
             "dla wygody, w pewnym stopniu chroni� tak^ze uda osoby kt^ora " +
             "j� nosi. Przeszywanica cho^c w miare dobrze chroni przed " +
             "atakami broni� obuchow�, jest praktycznie bezu^zyteczna w " +
             "spotkaniu z ostr� kling� miecza.\n");

    set_slots(A_TORSO, A_FOREARMS, A_ARMS, A_THIGHS);
    set_ac(A_TORSO | A_FOREARMS | A_ARMS, 3,3,8, A_THIGHS, 2,2,6);

    add_prop(OBJ_I_VOLUME,4500);
    add_prop(OBJ_I_VALUE,1400);
    add_prop(OBJ_I_WEIGHT, 4500);
    add_prop(ARMOUR_F_POW_PIERSI, 1);
    add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    set_size("M");

    set_likely_cond(12);
}

