inherit "/std/armour";
#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <macros.h>
#include <materialy.h>
#include <object_types.h>
void
create_armour() 
{
    ustaw_nazwe_glowna("para");
    ustaw_nazwe("spodnie");
    dodaj_przym("delikatny","delikatni");
    dodaj_przym("cienki","ciency");
      
    set_long("Wykonane z delikatnego, mi^ekkiego w dotyku materia^lu "+
        "spodnie, posiadaj^a wyj^atkowo d^lugie nogawki. Ubranie jest "+
        "czyste i wygl^ada na ^swie^zo wyprane, co prawda nie nale^zy do "+
        "odzie^zy ochronnej czy wyj^sciowej, poniewa^z cienki materia^l "+
        "nie ochroni przed wiatrem czy mrozem, lecz na  nogach "+
        "w^la^sciciela b^ed^a sie prezentowa rewelacyjnie. \n");

    set_slots(A_LEGS, A_HIPS);
    add_prop(OBJ_I_VOLUME, 200);
    add_prop(OBJ_I_VALUE, 100);
    add_prop(OBJ_I_WEIGHT, 100);
    add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    set_size("S");
    ustaw_material(MATERIALY_BAWELNA, 100);
    set_type(O_UBRANIA);
}
