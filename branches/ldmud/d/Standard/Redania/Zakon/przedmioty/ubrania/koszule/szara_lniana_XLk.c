
/* 
opis popelniony przez Grypina, zakodowany przez Seda 06.04.2007
*/

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <macros.h>
#include <materialy.h>

void
create_armour()
{  
   ustaw_nazwe("koszula");
    dodaj_przym("szary", "szarzy");
    dodaj_przym("lniany","lniani");


    set_long("Do�� obszerna koszula zapinana jest na drewniane guziki. "
	+"Szary len wykorzystany do uszycia tego ubrania jest gruby i wytrzyma�y, "
	+"ale nie nale�y do modnych ani �adnych - mimo to napewno jest funkcjonalny "
	+"i dobrze chroni cia�o przed zabrudzeniem. Koszul� uszyto tak by nawet "
	+"osoby szerokie w barkach mog�y spokojnie si� w ni� zmie�ci� i jeszcze "
	+"zostawi� troch� luzu. Liczne plamy i lekkie rozdarcia powoduj� i� ubranie "
	+"nadaje si� g��wnie do u�ytku jako str�j roboczy.\n");

    set_slots(A_TORSO | A_SHOULDERS | A_FOREARMS | A_ARMS);
    add_prop(OBJ_I_WEIGHT, 560);
    add_prop(OBJ_I_VOLUME, 2400);
    add_prop(OBJ_I_VALUE, 2300);

	set_size("XL");
    add_prop(ARMOUR_S_DLA_RASY, "krasnolud");
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    add_prop(ARMOUR_F_PRZELICZNIK, 20.0);
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    ustaw_material(MATERIALY_LEN, 95);
	ustaw_material(MATERIALY_DR_BUK, 5);

    set_likely_cond(24);
}


