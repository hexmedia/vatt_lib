/* Autor: Avard
   Data : 06.05.07 */

inherit "/std/object.c";

#include "dir.h"
#include <pl.h>
#include <stdproperties.h>
#include <macros.h>
#include <filter_funs.h>
int przeczytaj_menu();

create_object()
{
    ustaw_nazwe("tabliczka");
    dodaj_nazwy("tablica");
    dodaj_nazwy("menu");

    set_long("     _________________________________________\n    / o\t\t       MENU\t\t    o \\\n   |"+
             "\t\tDania g^l^owne:\t\t      |\n"+
             "   |\tBaranina z ziemniakami\t20 groszy     |\n"+
             "   |\tSztuka mi^esa wo^lowego\t12 groszy     |\n"+
             "   |\tPierogi ze skwarkami\t9 groszy      |\n"+
             "   |\tKluski ze skwarkami\t8 groszy      |\n"+
             "   |\t\t\t\t\t      |\n   |\t\t\t\t\t      |\n"+
             "   |\t\tZupy:\t\t\t      |\n"+
             "   |\tGulasz z zaj^aca\t\t5 groszy      |\n"+
             "   |\tRos� z kury\t\t7 groszy      |\n"+
             "   |\tGroch�wka z boczkiem\t5 groszy      |\n   |\t\t\t\t\t      |\n"+
             "   |\t\tNapoje:\t\t\t      |\n"+
             "   |\tWoda\t\t\t4 grosze      |\n"+
             "   |\tKompot jab�kowy\t\t3 grosze      |\n"+
             "   |\tJasne piwo\t\t7 groszy      |\n"+
             "   |\tCiemne piwo\t\t8 groszy      |\n"+
             "   \\_________________________________________/\n"
              
              
              
              
              
              
              );

    add_prop(OBJ_I_WEIGHT, 485);
    add_prop(OBJ_I_NO_GET, "Tabliczka zosta^la mocno przybita do kontuaru.\n");
    add_prop(OBJ_I_DONT_GLANCE,1);

    add_cmd_item(({"menu","tabliczk^e"}), "przeczytaj", przeczytaj_menu,
                   "Przeczytaj co?\n"); 
}

int
przeczytaj_menu()
{
   return long();
}
