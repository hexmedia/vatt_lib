/* Autor: Avard
   Opis : Sniegulak
   Data : 25.04.07 */
inherit "/std/receptacle";

#include "/sys/stdproperties.h"
#include "/sys/macros.h"
#include "/sys/pl.h"
#include "/sys/materialy.h"
#include "/sys/object_types.h"

void
create_receptacle()
{
    ustaw_nazwe("skrzynia");
    dodaj_przym("du^zy", "duzi");
    dodaj_przym("prosty", "pro^sci");
    ustaw_material(MATERIALY_DR_DAB);
    set_type(O_MEBLE);
    set_long("Du^za prosta skrzynia zbita z heblowanych, pomalowanych na "+
        "czarno desek s^lu^zy zapewne cz^lonkom Zakonu na przechowywanie "+
        "w niej wszelakich zapasowych b^ad^x zb^ednych zbroi.\n");
	     
    add_prop(OBJ_I_VALUE, 1000);
    add_prop(CONT_I_RIGID, 1);
    add_prop(CONT_I_MAX_VOLUME, 15000);
    add_prop(CONT_I_MAX_WEIGHT, 60000);
    add_prop(CONT_I_VOLUME, 5000);
    add_prop(CONT_I_WEIGHT, 10000);
    add_prop(CONT_I_NO_OPEN_DESC, 1);

}
public int
jestem_skrzynia_zakonu()
{
        return 1;
}


