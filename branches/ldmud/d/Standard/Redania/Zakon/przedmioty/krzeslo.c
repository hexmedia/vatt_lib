/* Autor: Avard
   Opis : Sniegulak
   Data : 25.04.07 */

inherit "/std/object";

#include "dir.h"
#include <cmdparse.h>
#include <pl.h>
#include <macros.h>
#include <stdproperties.h>
#include <object_types.h>
#include <materialy.h>

void create_object()
{
    ustaw_nazwe("krzes^lo");
    dodaj_przym("masywny", "masywni");
    dodaj_przym("ciemny", "ciemni");

    set_long("Masywne krzes^lo zrobione z ciemnego drewna, delikatnie "+
        "rzezbione zosta^lo najprawdopodobniej wykonane na zam^owienie "+
        "dla jakiegos bogatszej osoby. Szerokie siedziska pokryte "+
        "ciemnoczerwonym materia^lem wygl^adaj^a na wygodnie i zach^ecaj^a "+
        "do chwilowego chocia^z odpoczynku.\n");
    add_prop(OBJ_I_WEIGHT, 2500);
    add_prop(OBJ_I_VOLUME, 2500);
    add_prop(OBJ_I_VALUE, 1450);
    set_type(O_MEBLE);
    ustaw_material(MATERIALY_DR_MAHON);
    make_me_sitable("na", "na krze^sle", "z krzes^la", 1);
    //set_owners(({RINDE_NPC + "krepp"}));TODO FIXME TRALALA ACHTUNG!!
}
public int
jestem_krzeslem_zakonu()
{
        return 1;
}
