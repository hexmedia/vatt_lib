/* Autor: Avard
   Opis : Sniegulak
   Data : 25.04.07 */

inherit "/std/receptacle";

#include "dir.h"
#include <cmdparse.h>
#include <pl.h>
#include <macros.h>
#include <stdproperties.h>
#include <object_types.h>
#include <materialy.h>

void create_receptacle()
{
    ustaw_nazwe("biurko");
    dodaj_przym("masywny", "masywni");
    dodaj_przym("ciemny", "ciemni");

    set_long("Masywne drewniane biurko wykonane z ciemnego drewna, nale^zy "+
        "do naprawd^e ^ladnych okaz^ow mebli. Lakierowana powierzchnia "+
        "wyczyszczona i wypolerowana odbija delikatnie ^swiat^lo, a masywne "+
        "boki pokryte zosta^ly zdobieniami w kszta^lcie winogron. "+
        "@@opis_sublokacji|Dostrzegasz na nim |na|.\n|\n|" + PL_BIE+ "@@");
    add_prop(OBJ_I_WEIGHT, 5000);
    add_prop(OBJ_I_VOLUME, 5000);
    add_prop(OBJ_I_VALUE, 950);
    add_prop(CONT_I_RIGID, 1);
    add_prop(CONT_I_MAX_VOLUME, 15000);
    add_prop(CONT_I_MAX_WEIGHT, 30000);
    add_prop(CONT_I_NO_OPEN_DESC, 1);
    set_type(O_MEBLE);
    add_subloc("na", 0, "ze");
    add_subloc_prop("na", SUBLOC_I_MOZNA_POLOZ, 1);
    add_prop(CONT_I_DONT_SHOW_CONTENTS, 1);
    set_key("KLUCZ_DRZWI_DO_KWATERY_ZAKONU_KARMAZYNOWYCH_OSTRZY");

    //set_owners(({RINDE_NPC + "krepp"}));TODO FIXME TRALALA ACHTUNG!!
}
