/* Autor: Avard
   Opis : Sniegulak
   Data : 25.04.07 */

inherit "/std/object";

#include "dir.h"
#include <cmdparse.h>
#include <pl.h>
#include <macros.h>
#include <stdproperties.h>
#include <object_types.h>
#include <materialy.h>

void create_object()
{
    ustaw_nazwe("fotel");
    dodaj_przym("du^zy", "duzi");
    dodaj_przym("ciemny", "ciemni");

    set_long("Du^zy fotel wykonany zosta^l z drewnianej ramy w kolorze "+
        "ciemnego br^azu, obitej garbowan^a, ciel^ec^a skor^a, zafarbowana "+
        "na karmazynowy kolor. Siedzisko wygl^ada na solidne a zarazem "+
        "bardzo wygodne dzi^eki wyprofilowaniu ca^lej konstrukcji, tak by "+
        "przylega^la do cia^la i zapewnia^la maksymalny komfort. Cztery "+
        "nogi, delikatnie zdobione, zosta^ly zrobione z tego samego drewna "+
        "co rama.\n");
    add_prop(OBJ_I_WEIGHT, 5000);
    add_prop(OBJ_I_VOLUME, 5000);
    add_prop(OBJ_I_VALUE, 1550);
    set_type(O_MEBLE);
    ustaw_material(MATERIALY_DR_MAHON);
    make_me_sitable(({"w","na"}), "w fotelu", "z fotela", 1);
}