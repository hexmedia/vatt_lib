/* Autor: Avard
   Opis : Sniegulak
   Data : 25.04.07 */

inherit "/std/object";

#include "dir.h"
#include <cmdparse.h>
#include <pl.h>
#include <macros.h>
#include <stdproperties.h>
#include <object_types.h>
#include <materialy.h>

void create_object()
{
    ustaw_nazwe("krzes^lo");
    dodaj_przym("wysoki", "wysocy");
    dodaj_przym("rze^xbiony", "rze^xbieni");

    set_long("Masywne krzes^lo z czarnego d^ebu zwie^nczone jest wysokim "+
        "oparciem, kt^ore wyobra^za trzy skrzy^zowane i zwr^ocone ku "+
        "do^lowi miecze. Siedzisko wprawdzie wypchano czym^s i powleczono "+
        "suknem, lecz nie jest zbyt mi^ekkie.\n");
    add_prop(OBJ_I_WEIGHT, 2500);
    add_prop(OBJ_I_VOLUME, 2500);
    add_prop(OBJ_I_VALUE, 1450);
    set_type(O_MEBLE);
    ustaw_material(MATERIALY_DR_DAB);
    make_me_sitable("na", "na krze^sle", "z krzes^la", 1);
    //set_owners(({RINDE_NPC + "krepp"}));TODO FIXME TRALALA ACHTUNG!!
}
public int
jestem_krzeslem_zakonu()
{
        return 1;
}
