/* Autor: Avard
   Opis : Sniegulak
   Data : 25.04.07 */

inherit "/std/object";

#include "dir.h"
#include <cmdparse.h>
#include <pl.h>
#include <macros.h>
#include <stdproperties.h>
#include <object_types.h>
#include <materialy.h>

void create_object()
{
    ustaw_nazwe("garnek");
    dodaj_nazwy("gar");
    dodaj_nazwy("rondel");
    dodaj_nazwy("chochla");

    set_long("Poustawiane na p^lycie ^zeliwnej wype^lnione s^a jedzeniem "+
        "podgrzewanym dla ^zo^lnierzy przychodz^acych do tej kuchni.\n");
    add_prop(OBJ_I_WEIGHT, 25000);
    add_prop(OBJ_I_VOLUME, 25000);
    add_prop(OBJ_I_VALUE, 0);
    set_type(O_MEBLE);
    add_prop(OBJ_M_NO_GET, "Przedmiot jest tak rozgrzany, ^ze przy pr^obie "+
        "zabrania go z p^lyty poparzy^le^s sobie d^lo^n, a naczynie nadal "+
        "znajduje si^e na p^lycie.\n");
    add_prop(OBJ_I_DONT_SHOW_IN_LONG,1);
}
