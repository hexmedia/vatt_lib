/* Autor: Avard
   Opis : Sniegulak
   Data : 9.04.07 */

inherit "/std/object";

#include "/sys/stdproperties.h"
#include "/sys/macros.h"
#include "/sys/pl.h"
#include "/sys/materialy.h"
#include "/sys/object_types.h"

void
create_object()
{
    ustaw_nazwe("o^ltarz");
    dodaj_przym("wypolerowany", "wypolerowani");
    dodaj_przym("marmurowy", "marmurowi");
    ustaw_material(MATERIALY_MARMUR);
    set_type(O_INNE);
    set_long("Przed sob^a masz idealnie ociosany marmurowy blok, "+
        "wypolerowane ^sciany nie posiadaj^a ^zadnych zdobie^n, a prostota "+
        "wykonania widoczna w pomara^nczowym ^swietle padaj^acym z "+
        "P^lomienia Wiecznego Ognia ^swiadczy, i^z jest to jedynie "+
        "piedesta^l dla srebrnego znicza umieszczonego na ^srodku "+
        "o^ltarza.\n");
             
    add_prop(OBJ_I_WEIGHT, 50000); 
    add_prop(OBJ_I_VOLUME, 100000); 
    add_prop(OBJ_I_VALUE, 200);    
    add_prop(OBJ_I_DONT_SHOW_IN_LONG, 1);
    add_prop(OBJ_I_NO_GET,"Nie dasz rady tego podnie^s^c.\n");

    add_item(({"znicz"}),"Srebrna misa o ^srednicy ^lokcia zosta^la "+
        "przynitowana do powierzchni marmurowego bloku, wype^lniona oliw^a "+
        "i grubym, sztywnym, zapalonym knotem pozwala Wiecznemu Ogniowi na "+
        "sta^l^a obecno^s^c w tym pomieszczeniu.\n");
    add_item(({"ogie^n","wieczny ogie^n","Wieczny Ogie^n","Wieczny ogie^n"}),
        "Jasno i dumnie panuj^acy na o^ltarzu. Wskazuj^acy drog^e w "+
        "ciemno^sciach, nios^acy zapowied^x lepszego jutra, przetrwania. "+
        "Nie gasn^acy p^lomie^n, kt^ory tli si^e w sercach wyznawc^ow, "+
        "niesie im nadziej^e, tak jak wszystkim kt^orzy pob^l^adzili w "+
        "mroku.\n");

    make_me_sitable("na","na o^ltarzu","z o^ltarza",6);
}