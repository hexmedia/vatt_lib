
/* Autor: Avard
   Data : 24.04.07
   Opis : Sniegulak */

inherit "/std/door";

#include <pl.h>
#include "dir.h"
#include <stdproperties.h>

void
create_door()
{
    ustaw_nazwe("drzwi");
    dodaj_przym("du�y", "duzi");
    dodaj_przym("drewniany","drewniani");

    set_other_room(ZAKON_LOKACJE + "lok2.c");
    set_door_id("DRZWI_DO_ZAKONU_KARMAZYNOWYCH_OSTRZY");
    set_door_desc("Zbudowane z oheblowanych drewnianych desek, "+
        "po^l^aczonych ze sob^a stalowymi okuciami wygl^adaj^a "+
        "bardzo solidnie. Du^ze stalowe zawiasy posmarowane s^a "+
        "jakim^s t^luszczem, by nie wydawa^c przy otwieraniu "+
        "^zadnych d^xwi^ek^ow.\n");

    set_open_desc("");
    set_closed_desc("");

    set_pass_command(({"drzwi","wyj^scie","dziedziniec"}),
        "przez drewniane okute drzwi","wychodz^ac z du�ego budynku");

    set_key("KLUCZ_DRZWI_DO_ZAKONU_KARMAZYNOWYCH_OSTRZY");
    set_lock_name(({"zamek", "zamku", "zamkowi", "zamek", "zamkiem", "zamku"}), 0, PL_MESKI_NOS_NZYW);
    add_prop(DOOR_I_HEIGHT, 240);
}