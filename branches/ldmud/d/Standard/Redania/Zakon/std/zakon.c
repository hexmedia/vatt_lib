
#include "dir.h"

inherit REDANIA_STD;

#include <mudtime.h>
#include <language.h>
#include <stdproperties.h>

void
create_zakon()
{
}

nomask void
create_redania()
{
    set_long("@@dlugi_opis@@");
    set_polmrok_long("@@opis_nocy@@");

    add_prop(ROOM_I_TYPE,ROOM_NORMAL);
    add_prop(ROOM_I_INSIDE,1);

    set_start_place(0);

    create_zakon();
}

public int
prevent_enter(object ob)
{
    if(ob->query_gildia(GUILD_NAME))
        ob->set_special_start_location(ZAKON_SLEEP_PLACE);

    return ::prevent_enter(ob);
}