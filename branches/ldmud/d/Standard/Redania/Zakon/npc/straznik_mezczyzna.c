/* Autor: Avard
   Opis : Sniegulak
   Data : 6.05.07 */

#include <std.h>
#include <ss_types.h>
#include <macros.h>
#include <stdproperties.h>
#include <pl.h>
#include "dir.h"

inherit ZAKON_STD_NPC;

int czy_walka();
int walka = 0;

void
create_zakon_humanoid()
{
    ustaw_odmiane_rasy("m�czyzna");
    set_gender(G_MALE);
    dodaj_nazwy("stra^znik");
    dodaj_nazwy("zakonnik");

    set_long("@@dlugasny@@");

    dodaj_przym("wysoki","wysocy");
    dodaj_przym("czujny","czujni");

    set_stats (({ 165+random(20), 170+random(20), 160+random(20), 80, 95 }));

    add_prop(CONT_I_WEIGHT, 80000);
    add_prop(CONT_I_HEIGHT, 180);

    add_prop(LIVE_I_NEVERKNOWN, 1);

    set_act_time(30);
    add_act("emote przest^epuje z nogi na nog^e.");
    add_act("otrzyj pot z czola");
    add_act("emote poprawia sobie zbroj^e. ");
    add_act("spojrzyj");
    add_act("emote mruczy cicho pod nosem.");

    set_cact_time(30);
    add_cact("I popami�tasz to!");
    add_cact("Ostatni raz w tych rekach trzymasz bro�!");

    add_armour(ZAKON_ZBROJE + "buty/mocne_wysokie_Lc.c");
    add_armour(ZAKON_ZBROJE + "kaptury/obszerny_kolczy_Lc.c");
    add_armour(ZAKON_ZBROJE + "kolczugi/luskowa_rycerska_Lc.c");
    add_armour(ZAKON_ZBROJE + "kirysy/stalowy_blyszczacy_Lc.c");
    add_armour(ZAKON_ZBROJE + "spodnie/lekkie_kolcze_Lc.c");
    add_armour(ZAKON_ZBROJE + "naramienniki/czernione_lekkie_Lc.c");
    add_weapon(ZAKON_BRONIE + "mloty/maly_prosty.c");

    add_ask(({"zakon"}), VBFC_ME("pyt_o_zakon"));
    add_ask(({"bro^n","bronie"}), VBFC_ME("pyt_o_bron"));
    add_ask(({"prac^e"}), VBFC_ME("pyt_o_prace"));
    add_ask(({"zadanie"}), VBFC_ME("pyt_o_zadanie"));
    set_default_answer(VBFC_ME("default_answer"));

    set_alarm_every_hour("co_godzine");

    set_skill(SS_DEFENCE, 50 + random(10));
    set_skill(SS_PARRY, 50 + random(10));
    set_skill(SS_WEP_WARHAMMER, 60 + random(20));
}

void
powiedz_gdy_jest(object player, string tekst)
{
    if(environment(this_object()) == environment(player))
        this_object()->command(tekst);
}

string
pyt_o_zakon()
{
    if(!query_attack())
    {
        set_alarm(1.0, 0.0, "powiedz_gdy_jest", this_player(),
        "powiedz Zakon Karmazynowych chroni biednych i potrzebuj^acych.");
    }
    else
    {
        set_alarm(1.0,0.0,"powiedz_gdy_jest", this_player(),
            "powiedz Won mi st^ad!");
    }
    return "";

}
string
pyt_o_bron()
{
    if(!query_attack())
    {
        set_alarm(1.0, 0.0, "powiedz_gdy_jest", this_player(),
            "powiedz Przed wej^sciem nale^zy przekaza^c nam bro^n. ");
    }
    else
    {
        set_alarm(1.0,0.0,"powiedz_gdy_jest", this_player(),
            "powiedz Won mi st^ad!");
    }
    return "";

}

string
pyt_o_prace()
{
    if(!query_attack())
    {
        set_alarm(1.0, 0.0, "powiedz_gdy_jest", this_player(),
            "powiedz Chcesz pracowa^c? Id^x i porozmawiaj z kim^s innym. ");
    }
else
    {
        set_alarm(1.0,0.0,"powiedz_gdy_jest", this_player(),
            "powiedz Won mi st^ad!");
    }
    return "";

}

string
pyt_o_zadanie()
{
    if(!query_attack())
    {
        set_alarm(2.0, 0.0, "powiedz_gdy_jest", this_player(),
            "powiedz Tak mo^zesz mi pomoc... Id^x i nie wracaj. ");
    }
    else
    {
        set_alarm(1.0,0.0,"powiedz_gdy_jest", this_player(),
            "powiedz Won mi st^ad!");
    }
    return "";
}
string
default_answer()
{
    if(!query_attack())
    {
        switch(random(2))
        {
            case 0:set_alarm(2.0, 0.0, "powiedz_gdy_jest", this_player(),
                "powiedz Id^x i nie zadawaj g^lupich pyta^n. "); break;
            case 1:set_alarm(2.0, 0.0, "powiedz_gdy_jest", this_player(),
                "powiedz Nie mam zamiaru z tob^a rozmawia^c. "); break;
        }
    }
    else
    {
        set_alarm(1.0,0.0,"powiedz_gdy_jest", this_player(),
            "powiedz Won mi st^ad!");
    }
    return "";
}

void
emote_hook(string emote, object wykonujacy)
{
    switch (emote)
    {
        case "kopnij": set_alarm(1.5, 0.0, "nienajlepszy", wykonujacy);
                    break;
        case "spoliczkuj": set_alarm(1.5, 0.0, "nienajlepszy", wykonujacy);
                    break;
        case "opluj" : set_alarm(2.5, 0.0, "nienajlepszy", wykonujacy);
                    break;
        case "poca^luj":
        {
            if(wykonujacy->query_gender() == 1)
            {
                switch(random(1))
                {
                    case 0:set_alarm(0.5,0.0,"command","powiedz Mo^ze "+
                        "spotkamy si^e po s^lu^zbie?"); break;
                }
            }
            else
            {
                switch(random(2))
                {
                    case 0:set_alarm(0.5,0.0,"command","powiedz ^Zeby mi to "+
                        "by^lo ostatni raz!");break;
                    case 1:set_alarm(0.5,0.0,"command","powiedz Id^x precz "+
                        "bo ci^e na kopniakach st^ad wyrzuc^e!");break;
                }
             }
         }break;

        case "przytul": set_alarm(2.5, 0.0, "malolepszy", wykonujacy);
                   break;
        case "pog^laszcz": set_alarm(2.5, 0.0, "malolepszy", wykonujacy);
                   break;
    }
}

void
malolepszy(object wykonujacy)
{
    if(wykonujacy->query_gender() == 1)
    {
        set_alarm(1.0,0.0,"powiedz_gdy_jest",wykonujacy,"powiedz Mo^ze po "+
            "s^lu^zbie si^e zobaczymy?");
    }
    else
    {
        set_alarm(1.0,0.0,"powiedz_gdy_jest",wykonujacy,"powiedz  ^Lapy "+
            "przy sobie zbere^xniku!");
    }
}

void
nienajlepszy(object kto)
{
   switch(random(2))
   {
      case 0: set_alarm(0.5, 0.0, "powiedz_gdy_jest", kto,
          "powiedz Bo kruca zawo^lam Zakonnych! ");break;
      case 1: set_alarm(0.5, 0.0, "powiedz_gdy_jest", kto,
          "powiedz Won mi st^ad! ");break;
   }
}

void attacked_by(object wrog)
{
    if(walka == 0)
    {
        walka = 1;
        set_alarm(0.5,0.0,"command","krzyknij Rycerstwo atakuj^a!");
        set_alarm(15.0, 0.0, "czy_walka", 1);
        return ::attacked_by(wrog);
    }
    else
    {
        return ::attacked_by(wrog);
    }
}
int
czy_walka()
{
    if(!query_attack())
    {
        set_alarm(0.5,0.0,"command","krzyknij Ha! Kto^s jeszcze chce "+
            "dosta^c?!");
        walka = 0;
        return 1;
    }
    else
    {
        set_alarm(15.0, 0.0, "czy_walka", 1);
    }
}

string
dlugasny()
{
    string str;
    str = "Stra^znik na kt^orego spogl^adasz jest odpowiedzialny za "+
        "pilnowanie drzwi prowadz^acych do du^zego budynku. W^ladczy "+
        "g^los i charyzmatyczne spojrzenie pozwalaj^a mu zatrzyma^c na "+
        "inspekcje nawet najbardziej niepos^lusznego odwiedzaj^acego. "+
        "Ubrany jest w standardowa rycerska kolczug^e na kt^ora za^lo^zony "+
        "ma stalowy kirys z naramiennikami. G^lowa chroniona przez ci^e^zki "+
        "he^lm przys^lania troch^e czujny wzrok, lecz nie przeszkadza w "+
        "pe^lnieniu obowi^azk^ow. ";
   /* if(!query_attack())
    {
        str += "U pasa w ma przypi^ety ma�y prosty m�ot.";
    }*/
    return str;
}

public void
init()
{
     ::init();

     add_action("blok", "", 1);
}

int
blok(string str)
{
    string verb = query_verb();

    if(!TP->query_ranga_gildiowa(GUILD_NAME))
        string *nie_dozwolone = ({"nw","p�nocny-zach�d","ne","p�nocny-wsch�d"});

	if(TP->query_spolecznosc() ~= "Zakon Karmazynowych Ostrzy")
		return 0;

    if(TP->query_leader())
        if((TP->query_leader())->query_ranga_gildiowa(GUILD_NAME))
            return 0;

    if(member_array(verb,nie_dozwolone) == -1)
        return 0;

    TP->catch_msg(QCIMIE(TO,PL_MIA)+" staje przed tob� uniemo�liwiaj�c ci "+
        "przej�cie w tamtym kierunku.\n");
    say(QCIMIE(TO,PL_MIA)+" staje przed "+QIMIE(TP,PL_NAR)+" uniemo�liwiaj�c "+
        TP->koncowka("mu","jej","temu") + " " + "przej�cie w tamtym kierunku.\n");
    return 1;
}
nomask int
query_reakcja_na_walke()
{
    return 1;
}
