/* Autor: Valor
   Opis : Sniegulak
   Data : 5.05.07 */

#pragma unique

#include <macros.h>
#include <stdproperties.h>
#include <money.h>
#include <pl.h>
#include <formulas.h>
#include <std.h>
#include <ss_types.h>
#include <wa_types.h>
#include <composite.h>
#include <filter_funs.h>
#include "dir.h"
inherit ZAKON_STD_NPC;

int czy_walka();
int walka = 0;

int i;
int k;

void
create_zakon_humanoid()
{
    set_living_name("wysoki_opalony_mlodzieniec");
    ustaw_odmiane_rasy("m^lodzieniec");
    set_gender(G_MALE);
    dodaj_nazwy("m^e^zczyzna");
    set_long("Ubrany w mi^ekki kaftan z prostymi r^ekawami, cie^nkie "+
        "delikatne spodnie, i sk^orzane czarne buty m^lodzieniec pe^lni "+
        "funkcje od^xwiernego. Patrz^ac na jego twarz, kt^ora jeszcze nie "+
        "zd^a^zy^la pokry^c si^e zarostem, od razu mo^zna zauwa^zy^c, i^z "+
        "nie bardzo skupia si^e na powierzonym mu zadaniu. D^lugie "+
        "tyczkowate nogi, i cherlawa budowa cia^la zapewne s^a dla tego, "+
        "na oko wygl^adaj^acego na trzyna^scie lat ch^lopaka, "+
        "przekle^nstwem. ");
    dodaj_przym("wysoki","wysocy");
    dodaj_przym("opalony","opaleni");
    set_act_time(30);
    add_act("emote spogl^ada w stron^e okna i mruczy co^s pod nosem.");
    add_act("emote ziewa i zaczyna ogl^ada^c swoje paznokcie.");
    set_default_answer(VBFC_ME("default_answer"));
    set_stats ( ({ 45, 65+random(50), 45+random(10), 55, 60 }) );
    set_skill(SS_DEFENCE, 10 + random(4));
    set_skill(SS_PARRY, 10 + random(10));
    set_skill(SS_UNARM_COMBAT, 10 + random(5));
    add_armour(ZAKON_UBRANIA + "buty/proste_skorzane_Sc.c");
    add_armour(ZAKON_UBRANIA + "kaftany/miekki_wygodny_Sc.c");
    add_armour(ZAKON_UBRANIA + "spodnie/delikatne_cienkie_Sc.c");

    add_prop(LIVE_I_NEVERKNOWN, 1);
    add_prop(CONT_I_WEIGHT, 55000);
    add_prop(CONT_I_HEIGHT, 170);

    add_ask(({"zakon","Zakon","Zakon Karmazynowych Ostrzy",
        "zakon karmazynowych ostrzy","Zakon karmazynowych ostrzy",
        "Zakon Karmazynowych ostrzy","Zakon karmazynowych Ostrzy",
        "Zakon rycerski","zakon rycerski"}),VBFC_ME("pyt_o_zakon"));
    add_ask(({"prace"}),VBFC_ME("pyt_o_prace"));
    add_ask(({"zadanie"}),VBFC_ME("pyt_o_zadanie"));
    set_default_answer(VBFC_ME("default_answer"));
}
string
default_answer()
{
    if(!query_attack())
    {
        set_alarm(2.0, 0.0, "command_present", this_player(),
            "powiedz do " + OB_NAME(this_player()) +
            " Nic nie wiem na ten temat, je^sli chcecie mog^e kogo^s "+
            "zawo^la^c.");
    }
    else
    {
        set_alarm(0.5,0.0,"powiedz_gdy_jest", TP, "powiedz Zostaw mnie w "+
            "spokoju!");
    }
    return "";
}
string
pyt_o_zakon()
{
    if(!query_attack())
    {
        set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
        "powiedz Tak, kiedy^s b^ed^e rycerzem.");
        set_alarm(1.5, 0.0, "powiedz_gdy_jest", this_player(),
        "usmiech marzycielsko");
    }
    else
    {
         set_alarm(0.5,0.0,"powiedz_gdy_jest", TP, "powiedz Zostaw mnie w "+
            "spokoju!");
    }
    return "";
}
string
pyt_o_prace()
{
    if(!query_attack())
    {
        set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
        "powiedz Tutaj nie znajdziesz pracy.");
    }
    else
    {
         set_alarm(0.5,0.0,"powiedz_gdy_jest", TP, "powiedz Zostaw mnie w "+
            "spokoju!");
    }
    return "";
}
string
pyt_o_zadanie()
{
    if(!query_attack())
    {
        set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
        "powiedz Mam za zadanie informowa^c kto przyszed^l.");
    }
    else
    {
         set_alarm(0.5,0.0,"powiedz_gdy_jest", TP, "powiedz Zostaw mnie w "+
            "spokoju!");
    }
    return "";
}
void
emote_hook(string emote, object wykonujacy, string przyslowek)
{
    switch (emote)
    {
        case "pocaluj":
        {
            if(wykonujacy->query_gender() == 1)
                switch(random(2))
            {
                case 0: command("'Och... M^oj pierwszy, Tatko "+
                    "m^owi^l ^ze rycerzom dobrze si^e wiedzie.");
                break;
                case 1: command("zaczerwien sie");
                break;
            }
            else
            {
                switch(random(2))
                {
                    case 0: command("'Blee... Nie wygl^adasz mi "+
                        "na mojego tat^e a ju^z tym bardziej na mam^e!");
                    break;
                    case 1: command("skrzyw sie");
                    break;
                }
            }
        } break;
        case "usmiech":
        {
            if(wykonujacy->query_gender() == 1)
            {
                command("emote u^smiecha si^e szeroko i czerwienieje "+
                    "na twarzy.");
            }
            else
            {
                command("emote u^smiecha si^e nieznacznie.");
            }
        }break;

        case "przytul":
        case "poglaszcz":
        {
         if(wykonujacy->query_gender() == 1)
            {
             command("'Dzi^ekuje, mi^la Pani.");
            }
            else
            {
             command("'Prosz^e trzyma^c r^ece przy sobie.");
            }
        }break;
        case "szturchnij":
        case "kopnij":
        {
            set_alarm(2.0, 0.0, "taki_sobie", wykonujacy);
        }break;
    }
}
void
taki_sobie(object kto)
{
   command("'Zaraz poprosz^e stra^znik^ow ^zeby ci^e wyrzucili");
}

void
attacked_by(object wrog)
{
    if(walka==0)
    {
        set_alarm(0.5,0.0,"command","krzyknij Rycerze! Na pomoc!");
        set_alarm(1.0,0.0,"do_gongu");
        set_alarm(30.0,0.0,"czy_walka");
        return ::attacked_by(wrog);
    }
    else
    {
        return ::attacked_by(wrog);
    }
}

int
czy_walka()
{
    if(!query_attack())
    {
        set_alarm(1.0,0.0,"do_wejscia");
        walka = 0;
        return 1;
    }
    else
    {
        set_alarm(1.0,0.0,"do_gongu");
        set_alarm(30.0, 0.0, "czy_walka");
    }

}

void
zerujemy()
{
    int i = 0;
    int k = 0;
    int l = 0;
}

void
do_gongu()
{
    int i = 0;
    mixed droga=znajdz_sciezke(environment(this_object()),
        ZAKON_LOKACJE + "lok6");
    this_object()->command(droga[i]);
    if(i==sizeof(droga)-1)
    {
        set_alarm(0.5,0.0,"command","uderz w gong");
        set_alarm(300.0,0.0,"zerujemy");
        return;
    }
    else
    {
        i++;
        set_alarm(15.0,0.0,"do_gongu");
        return;
    }
}

void
do_wejscia()
{
    int k = 0;
    mixed droga=znajdz_sciezke(environment(this_object()),
        ZAKON_LOKACJE + "lok3");
    this_object()->command(droga[k]);
    if(k==sizeof(droga)-1)
    {
        set_alarm(300.0,0.0,"zerujemy");
        return;
    }
    else
    {
        k++;
        set_alarm(15.0,0.0,"do_wejscia");
        return;
    }
}
nomask int
query_reakcja_na_walke()
{
    return 1;
}