/* Autor: Avard
   Data : 24.04.07
   Opis : Sniegulak */

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <pogoda.h>
#include "dir.h"

inherit ZAKON_STD_D;

void create_zakon() 
{
    set_short("Dziedziniec");
    add_exit(ZAKON_LOKACJE + "lok1.c","s",0,1,0);
    add_exit(ZAKON_LOKACJE + "plac.c","w",0,1,0);
    add_object(ZAKON_DRZWI + "drzwi_do_zakonu");
    add_object(ZAKON_DRZWI + "drzwi_do_kuzni");

    if(jest_dzien() == 1)
    {
        add_event("Do twego ucha dochodzi cichy stukot m^lotka.\n");
    }
    add_event("S^lyszysz stukot kopyt.\n");

    add_item(({"budynek","ku^xni^e"}),"@@kuznia@@");

    add_item(({"drugi budynek","du^zy budynek", "fort", "fortyfikacj^e"}),
        "Pot^e^zny i wysoki, dwupi^etrowy budynek jest najwi^ekszy po tej "+
        "stronie palisady. Dok^ladnie wyko^nczony z pobielonymi na dole "+
        "^scianami prezentuje si^e okazale. Okna du^zych rozmiar^ow "+
        "zapewne wpuszczaj^a do ^srodka wystarczaj^aco ^swiat^la by "+
        "mo^zna w dzie^n nie u^zywa^c lamp, ani ^luczyw. Masywne "+
        "dwuskrzyd^lowe drzwi zagradzaj^a wej^scie.\n");

    add_item(({"drog^e","ulic^e"}),"W^askie przej^scie wy^lo^zone zosta^lo "+
        "drewnianymi kocimi ^lbami, dzi^eki czemu w zimie mo^zna ^latwiej "+
        "usun^a^c zalegaj^acy ^snieg, a w czasie deszczu pod^lo^ze jest w "+
        "miar^e suche i mo^zna chodzi^c po nim bez wi^ekszych problem^ow.\n");
    
    if(pora_roku() == MT_ZIMA)
    {
        add_item(({"zasp^e","^snie^zn^a zasp^e"}),"Wida^c, ^ze kto^s zebra^l"+
            " ca^ly ^snieg zalegaj^acy na przej^sciu i przeni^os^l go na "+
            "wschodni^a stron^e poza obr^eb drogi tak by nie przeszkadza^la "+
            "w chodzeniu.\n");
    }
    add_object(ZAKON_PRZEDMIOTY + "lampa_uliczna");
}

public string
exits_description() 
{
    return "Na po^ludniu wida^c dalsz^a cz^e^s^c dziedzi^nca, na zachodzie "+
        "znajduje si^e plac, wida^c tu tak^ze drzwi prowadz^ace do warowni "+
        "oraz drzwi do ku^xni.\n";
}

string
dlugi_opis()
{
    string str;
    if(pora_roku() == MT_ZIMA)
    {
        str = "Znajdujesz sie obok sporych rozmiar^ow budynku z kamiennym "+
            "kominem";
        if(jest_dzien() == 1)
        {
            str += ", z kt^orego unosi si^e cie^nk^a stru^zka dymu, a od "+
            "jego strony dochodzi ci^e odg^los uderze^n m^lota w metal. ";
        }
        else
        {
            str += ". ";
        }
        str += "Szerokie przej^scie wy^lo^zone jest kocimi ^lbami, dzi^eki "+
            "czemu w zimie mo^zna ^latwiej usun^a^c zalegaj^acy ^snieg, "+
            "kt^ory obecnie zosta^l zebrany na wi^eksz^a zasp^e po "+
            "wschodniej stronie przej^scia, pod ^scian^a budynku. Na "+
            "wschodzie mo^zesz dostrzec do^s^c du^zych rozmiar^ow plac, "+
            "a na p^o^lnocny znajduje si^e masywny, dwupi^etrowy budynek, "+
            "do kt^orego prowadz^a grube, drewniane, dwuskrzyd^lowe drzwi. "+
            "Dooko^la panuje porz^adek - wida^c, ^ze kto^s stara si^e "+
            "zachowa^c to miejsce w przyzwoitym stanie.\n";
    }
    else
    {
        str = "Znajdujesz sie obok sporych rozmiar^ow budynku z kamiennym "+
            "kominem";
        if(jest_dzien() == 1)
        {
            str += ", z kt^orego unosi si^e cie^nk^a stru^zka dymu, a od "+
            "jego strony dochodzi ci^e odg^los uderze^n m^lota w metal. ";
        }
        else
        {
            str += ". ";
        }
        if(CO_PADA(TO) == NIC_NIE_PADA)
        {
            str += "Szerokie przej^scie wy^lo^zone jest kocimi ^lbami, "+
                "dzi^eki czemu jest tu sucho i czysto. ";
        }
        else
        {
            str += "Szerokie przej^scie wy^lo^zone jest kocimi ^lbami, "+
                "dzi^eki czemu nawet teraz w czasie deszczu pod^lo^ze jest "+
                "w miar^e suche i mo^zna chodzi^c po nim bez wi^ekszych "+
                "problem^ow. ";
        }
        str += "Na wschodzie mo^zesz dostrzec do^s^c du^zych rozmiar^ow "+
            "plac, a na p^o^lnocny znajduje si^e masywny, dwupi^etrowy "+
            "budynek, do kt^orego prowadz^a grube, drewniane, dwuskrzyd^lowe"+
            " drzwi. Przy nich dostrzegasz drewniany stojak na kt^orym "+
            "mo^zna powiesi^c bro^n. Dooko^la panuje porz^adek - wida^c, ^ze"+
            " kto^s stara si^e zachowa^c to miejsce w przyzwoitym stanie.\n";
    }
    return str;
}
string
kuznia()
{
    string str;
    str = "Do^s^c du^zych rozmiar^ow budynek zbudowany w ca^lo^sci z "+
        "drewna. Dach pokryty zaimpregnowanym drewnianym gontem ozdabia "+
        "kamienny komin";
    if(jest_dzien() == 1)
    {
        str += ", z kt^orego unosi sie cienka stru^zka dymu. Od "+
            "strony tego budynku dochodzi ci^e stukot m^lota uderzaj^acego "+
            "w metal z czego mo^zna wywnioskowa^c, ^ze znajduje si^e tam "+
            "ku^xnia.\n";
    }
    else
    {
        str += ". \n";
    }
    return str;
}