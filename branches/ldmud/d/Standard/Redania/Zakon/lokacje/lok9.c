/* Autor: Avard
   Data : 24.04.07
   Opis : Sniegulak */

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <pogoda.h>
#include "dir.h"

inherit ZAKON_STD;

void create_zakon() 
{
    set_short("Sala");
    add_exit(ZAKON_LOKACJE + "lok10.c","sw",0,1,0);
    add_exit(ZAKON_LOKACJE + "lok11.c","se",0,1,0);
    add_exit(ZAKON_LOKACJE + "kaplica.c","e",0,1,0);
    add_object(ZAKON_DRZWI + "okno_lok9");
    add_object(ZAKON_DRZWI + "drzwi_do_kwatery");
    add_object(ZAKON_PRZEDMIOTY + "wielki_stol");
    add_object(ZAKON_PRZEDMIOTY + "krzeslo",9);
    add_item(({"chor^agwie","sztandary","chor^agwie rycerskie"}),"Do ^scian "+
        "pomieszczenia przybito r^o^zne chor^agwie rycerskie, o "+
        "przer^o^znych kolorach, Wida^c w^sr^od nich czarno-bia^le "+
        "szachownice, czerwone smoki na zielono-bia^lym tle, niebieskie "+
        "lilie na czarnych tarczach, a nawet gdzieniegdzie mo^zna "+
        "zobaczy^c g^low^e jednoro^zca. Wszystkie zebrane w jednym pokoju, "+
        "zapewne s^a pami^atkami przekazywanymi kolejnym cz^lonkom rycerzy "+
        "Zakonu Karmazynowych Ostrzy.\n");
    dodaj_rzecz_niewyswietlana("masywne ciemne krzes^lo",9);

    add_npc(ZAKON_NPC + "straznik");
    add_npc(ZAKON_NPC + "straznik");

    add_object(ZAKON_PRZEDMIOTY + "tablica");
}

public string
exits_description() 
{
    return "Korytarz prowadzi na po^ludniowy-zach^od i po^ludniowy-wsch^od"+
        ", na zachodniej ^scianie znajduj^a si^e drzwi prowadz^ace do "+
        "kwatery kapitana, za^s na wschodzie wida^c kaplic^e.\n";
}

string
dlugi_opis()
{
    string str;
    int i, il, il_krzesel;
    object *inwentarz;
   
    inwentarz = all_inventory();
    il_krzesel = 0;
    il = sizeof(inwentarz);
   
    for (i = 0; i < il; ++i) {
         if (inwentarz[i]->jestem_krzeslem_zakonu()) {
                        ++il_krzesel; }}

    str = "Przechodnie pomieszczenie, w kt^orym si^e znajdujesz ^l^aczy "+
        "kaplic^e i pok^oj nale^z^acy do Kapitana Zakonu z korytarzami "+
        "prowadz^acymi w g^l^ab budynku. Panuje tu ca^lkiem inny wystr^oj "+
        "ni^z pi^etro ni^zej, ^sciany przyozdobione chor^agwiami, "+
        "^swieczniki w rogach pomieszczenia, a na ^srodku pokoju znajduje "+
        "si^e poka^xnych rozmiar^ow drewniany st^o^l";
    if(il_krzesel == 0)
    {
        str += ". ";
    }
    if(il_krzesel == 1)
    {
        str += ", a przy nim jedno masywne krzes^lo. ";
    }
    if(il_krzesel == 2)
    {
        str += ", a przy nim dwa masywne krzes^la. ";
    }
    if(il_krzesel == 3)
    {
        str += ", a przy nim trzy masywne krzes^la. ";
    }
    if(il_krzesel == 4)
    {
        str += ", a przy nim cztery masywne krzes^la. ";
    }
    if(il_krzesel == 5)
    {
        str += ", dooko^la kt^orego stoi pi^e^c masywnych krzese^l. ";
    }
    if(il_krzesel == 6)
    {
        str += "dooko^la kt^orego stoi sze^s^c masywnych krzese^l. ";
    }
    if(il_krzesel == 7)
    {
        str += "dooko^la kt^orego stoi siedem masywnych krzese^l. ";
    }
    if(il_krzesel == 8)
    {
        str += "dooko^la kt^orego stoi osiem masywnych krzese^l. ";
    }
    if(il_krzesel == 9)
    {
        str += "dooko^la kt^orego stoi dziewi^e^c masywnych krzese^l. ";
    }
    str += "Na zachodniej ^scianie wida^c masywne drewniane drzwi.\n";
    return str;
}

