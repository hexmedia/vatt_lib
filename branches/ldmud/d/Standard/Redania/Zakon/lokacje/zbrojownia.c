/* Autor: Avard
   Data : 24.04.07
   Opis : Sniegulak */

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <pogoda.h>
#include "dir.h"

inherit ZAKON_STD;

void create_zakon() 
{
    set_short("Zbrojownia");
    add_object(ZAKON_PRZEDMIOTY + "lampa_srodek");
    add_object(ZAKON_DRZWI + "okno_zbrojownia");
    add_object(ZAKON_DRZWI + "drzwi_ze_zbrojowni");
    add_object(ZAKON_PRZEDMIOTY + "kosz");
    add_object(ZAKON_PRZEDMIOTY + "skrzynia",2);
    add_object(ZAKON_PRZEDMIOTY + "stojak",2);
    dodaj_rzecz_niewyswietlana("wiklinowy pleciony kosz");
    dodaj_rzecz_niewyswietlana("drewniany prosty stojak",2);
    dodaj_rzecz_niewyswietlana("du^za prosta skrzynia",2);
    add_prop(ROOM_I_ALWAYS_LOAD, 1);

    add_item(({"okazy","okazy broni"}),"Na ^scianach zawieszono r^o^zne "+
        "rodzaje broni i zbroi. Zaczynaj^ac od topor^ow i mieczy, kolczug "+
        "i kirys^ow, ko^ncz^ac na halabardach i morgensternach, tarczach i "+
        "he^lmach, wszystkie s^a lekko za^sniedzia^le, niekt^ore pokryte "+
        "patyn^a, inne za^s t^epe i wyszczerbione, s^lu^z^a za eksponaty i "+
        "nie nadawa^ly by si^e do walki.\n");
}

public string
exits_description() 
{
    return "Drzwi wyj^sciowe prowadz^a do jakiej^s sali.\n";  
}

string
dlugi_opis()
{
    string str;
    int i, z, il, zl, il_stojakow, il_skrzyn;
    object *inwentarz;
   
    inwentarz = all_inventory();
    il_stojakow = 0;
    il_skrzyn = 0;
    il = sizeof(inwentarz);
   
    for (i = 0; i < il; ++i) 
    {
        if (inwentarz[i]->jestem_stojakiem_zakonu())
            {
            ++il_stojakow;
            }
    }
    for (z = 0; z < il; ++z) 
    {
        if (inwentarz[z]->jestem_skrzynia_zakonu()) 
            {
            ++il_skrzyn; 
            }
    }
    str = "Ju^z na pierwszy rzut oka wida^c, i^z ten pok^oj jest swego "+
        "rodzaju zbrojowni^a rycerzy stacjonuj^acych w tym budynku. ";
    if(jest_rzecz_w_sublokacji(0,"wiklinowy pleciony kosz") || 
        jest_rzecz_w_sublokacji(0,"drewniany prosty stojak") ||
        jest_rzecz_w_sublokacji(0,"otwarta du^za prosta skrzynia") ||
        jest_rzecz_w_sublokacji(0,"zamkni^eta du^za prosta skrzynia"))
    {
        if(il_skrzyn > 0)
        {
            if(il_skrzyn == 2)
            {
                str += "Na pod^lodze pod ^scian^a stoj^a dwie du^zych "+
                    "rozmiar^ow skrzynie";
            }
            if(il_skrzyn == 1)
            {
                str += "Na pod^lodze pod ^scian^a stoi du^zych rozmiar^ow "+
                    "skrzynia";
            }
            if(jest_rzecz_w_sublokacji(0,"wiklinowy pleciony kosz"))
            {
                str += " i mniejszy wiklinowy, pleciony kosz";
            }
            if(il_stojakow > 0)
            {
                str += ", a obok ";
                if(il_skrzyn == 2)
                {
                    str += "nich ";
                }
                if(il_skrzyn == 1)
                {
                    str += "niej ";
                }
                if(il_stojakow == 2)
                {
                    str += "znajduj^a sie dwa stojaki na bro^n. ";
                }
                if(il_stojakow == 1)
                {
                    str += "znajduje si^e jeden stojak na bro^n. ";
                }
            }
            else
            {
                str += ". ";
            }
        }
        else
        {
            if(il_stojakow > 0)
            {
                if(il_stojakow == 2)
                {
                    str += "Na pod^lodze pod ^scian^a stoj^a dwa drewniane "+
                    "stojaki na bro^n";
                }
                if(il_stojakow == 1)
                {
                    str += "Na pod^lodze pod ^scian^a stoi drewniany stojak "+
                        "na bro^n";
                }
                if(jest_rzecz_w_sublokacji(0,"wiklinowy pleciony kosz"))
                {
                    str += ", a obok nich wiklinowy, pleciony kosz. ";
                }
                else
                {
                    str += ". ";
                }
            }
            else
            {
                if(jest_rzecz_w_sublokacji(0,"wiklinowy pleciony kosz"))
                {
                    str += "Na pod^lodze le^zy wiklinowy, pleciony kosz. ";
                }
            }
        }
    }
    str += "^Sciany "+
        "przyozdobione s^a r^o^znymi okazami broni i zbroi, na sta^le "+
        "przytwierdzone do ^scian s^a wystrojem pomieszczenia a nie "+
        "u^zytkowymi egzemplarzami. Du^ze okno znajduj^ace si^e na "+
        "zachodniej ^scianie roz^swietla pok^oj i pozwala na dok^ladniejsze "+
        "wybranie ekwipunku, a w razie nocy zawieszone, proste lampy "+
        "mog^a dodatkowo o^swietla^c pomieszczenie.\n";
    return str;
}

