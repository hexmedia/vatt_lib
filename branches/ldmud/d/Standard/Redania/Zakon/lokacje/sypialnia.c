/* Autor: Avard
   Data : 24.04.07
   Opis : Sniegulak */

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <pogoda.h>
#include "dir.h"

inherit ZAKON_STD;

void create_zakon() 
{
    set_short("Sypialnia");
    add_exit(ZAKON_LOKACJE + "lok12.c","w",0,1,0);
    add_object(ZAKON_PRZEDMIOTY + "stolek",4);
    add_object(ZAKON_PRZEDMIOTY + "stolik");
    add_object(ZAKON_PRZEDMIOTY + "lozko",8);
    add_object(ZAKON_PRZEDMIOTY + "dywan");
    add_object(ZAKON_PRZEDMIOTY + "lampa_srodek");
    dodaj_rzecz_niewyswietlana("niski nieoheblowany sto^lek",4);
    dodaj_rzecz_niewyswietlana("ma^ly drewniany stolik");
    dodaj_rzecz_niewyswietlana("pi^etrowe mocne ^l^o^zko",8);
    dodaj_rzecz_niewyswietlana("gruby we^lniany dywan");
}

public string
exits_description() 
{
    return "Na zachodzie znajduje si^e jaka^s sala.\n";  
}

string
dlugi_opis()
{
    string str;
    int i, il, il_stolkow;
    object *inwentarz;
   
    inwentarz = all_inventory();
    il_stolkow = 0;
    il = sizeof(inwentarz);
   
    for (i = 0; i < il; ++i) {
         if (inwentarz[i]->jestem_stolkiem_zakonu()) {
                        ++il_stolkow; }}

    str = "Znajdujesz sie w pomieszczeniu sypialnym nale^z^acym do "+
        "cz^lonk^ow Zakonu. Du^za sala zastawiona zosta^la pi^etrowymi "+
        "^l^ozkami, na kt^orych ^spi^a rycerze i ich giermkowie. Mrok "+
        "normalnie panuj^acy w tym pokoju roz^swietlaj^a ";
    if(jest_dzien() == 1)
    {
        str += "s^labe promyki s^lo^nca przebijaj^ace si^e przez okna "+
            "zas^loni^ete kotarami. ";
    }
    else
    {
        str += "proste lampy zawieszone na ^scianach. ";
    }
    if(jest_rzecz_w_sublokacji(0, "gruby we^lniany dywan"))
    {
        str += "Pod^loga wykonana z drewnianych desek, przykryta "+
            "zosta^la grubym we^lnianym dywanem o barwie ciemnej czerwieni. ";
    }
    if(jest_rzecz_w_sublokacji(0, "ma^ly drewniany stolik"))
    {
        str += "Po ^srodku sali wida^c ma^ly drewniany stolik";
        if(il_stolkow == 4)
        {
            str += "i przystawione do niego cztery sto^lki.";
        }
        if(il_stolkow == 3)
        {
            str += "i przystawione do niego trzy sto^lki.";
        }
        if(il_stolkow == 2)
        {
            str += "i przystawione do niego dwa sto^lki.";
        }
        if(il_stolkow == 1)
        {
            str += "i przystawione do niego jedno sto^lek.";
        }
        if(il_stolkow == 0)
        {
            str += ".";
        }
    }
    else
    {
        if(il_stolkow == 4)
        {
            str += "Na ^srodku sali stoj^a cztery niskie nieoheblowane "+
                "sto^lki.";
        }
        if(il_stolkow == 3)
        {
            str += "Na ^srodku sali stoj^a trzy niskie nieoheblowane "+
                "sto^lki.";
        }
        if(il_stolkow == 2)
        {
            str += "Na ^srodku sali stoj^a dwa niskie nieoheblowane "+
                "sto^lki.";
        }
        if(il_stolkow == 1)
        {
            str += "Na ^srodku sali stoi niski nieoheblowany sto^lek.";
        }
        if(il_stolkow == 0)
        {
            str += "";
        }
    }
    str += "\n";
    return str;
}