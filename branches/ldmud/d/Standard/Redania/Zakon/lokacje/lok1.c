/* Autor: Avard
   Data : 24.04.07
   Opis : Sniegulak */

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <pogoda.h>
#include "dir.h"

inherit ZAKON_STD_D;

void create_zakon() 
{
    set_short("Dziedziniec");
    add_exit(ZAKON_LOKACJE + "lok2.c","n",0,1,0);
    add_object(ZAKON_DRZWI + "wrota_do_stajni");
    add_object(ZAKON_DRZWI + "brama_z");
    add_npc(ZAKON_NPC + "straznik");
    add_npc(ZAKON_NPC + "straznik");
    
    if(jest_dzien() == 1)
    {
        add_event("Do twego ucha dochodzi cichy stukot m^lotka.\n");
    } 
    add_event("S^lyszysz stukot kopyt.\n");

    add_item(({"budynek","stajni^e"}),"Jest to do^s^c poka^xnych rozmiar^ow "+
        "budynek, zbudowany w ca^lo^sci z drewna. Dach pokryty strzech^a "+
        "jest solidny i pokryty dodatkowymi drewnianymi ^latami maj^acymi "+
        "za zadanie utrzymywa^c s^lom^e zbit^a razem i chroni^c ja przed "+
        "porywistym wiatrem. Od strony tego budynku dochodzi ci^e zapach "+
        "s^lomy i koni, z czego mo^zna wywnioskowa^c, i^z znajduje si^e "+
        "tam stajnia.\n");

    add_item(({"drog^e","ulic^e"}),"W^askie przej^scie wy^lo^zone zosta^lo "+
        "drewnianymi kocimi ^lbami, dzi^eki czemu w zimie mo^zna ^latwiej "+
        "usun^a^c zalegaj^acy ^snieg, a w czasie deszczu pod^lo^ze jest w "+
        "miar^e suche i mo^zna chodzi^c po nim bez wi^ekszych problem^ow.\n");
    
    if(pora_roku() == MT_ZIMA)
    {
        add_item(({"zasp^e","^snie^zn^a zasp^e"}),"Wida^c, ^ze kto^s zebra^l"+
            " ca^ly ^snieg zalegaj^acy na przej^sciu i przeni^os^l go na "+
            "wschodni^a stron^e poza obr^eb drogi tak by nie przeszkadza^la "+
            "w chodzeniu.\n");
    }
    add_item(({"wa^l","wa^l obronny","palisad^e","mury","mur"}),"Wysoka na "+
        "dziesi^e^c ^lokci palisada ma za zadanie chroni^c teren "+
        "znajduj^acy si^e za ni^a przed atakami pieszych jednostek. "+
        "Drzewa u^zyte do jej budowy zosta^ly ociosane z ga^l^ezi, "+
        "potem zaostrzone u g^ory, a nast^epnie g^l^eboko wkopane w "+
        "pod^lo^ze.\n");
    add_object(ZAKON_PRZEDMIOTY + "lampa_uliczna");
}

public string
exits_description() 
{
    return "Na po^ludniu znajduje si^e brama palisady, na p^o^lnocy wida^c "+
        "dalsz^a cz^e^s^c dziedzi^nca, znajduj^a si^e tu tak^ze wrota do "+
        "stajni.\n";
}

string
dlugi_opis()
{
    string str;
    if(pora_roku() == MT_ZIMA)
    {
        str = "Znajdujesz sie obok poka^xnych rozmiar^ow budynku, z "+
            "kt^orego dochodzi do ciebie zapach s^lomy i koni. W^askie "+
            "przej^scie wy^lo^zone jest kocimi ^lbami, dzi^eki czemu w "+
            "zimie mo^zna ^latwiej usun^a^c zalegaj^acy ^snieg, kt^ory "+
            "obecnie zosta^l zebrany na wi^eksz^a zasp^e po wschodniej "+
            "stronie przej^scia. Na p^o^lnocnym zachodzie mo^zesz "+
            "dostrzec do^s^c du^zych rozmiar^ow plac a na p^o^lnocnym "+
            "wschodzie budynek z kamiennym kominem";
        if(jest_dzien() == 1)
        {
            str += ", z kt^orego unosi si^e cienka stru^zka dymu. ";
        }
        else
        {
            str += ". ";
        }
        str += "Dooko^la panuje porz^adek - "+
            "wida^c ^ze kto^s stara si^e zachowa^c to miejsce w "+
            "przyzwoitym stanie.\n";
    }
    else
    {
        str = "Znajdujesz sie obok poka^xnych rozmiar^ow, drewnianego "+
            "budynku, z kt^orego dochodzi do ciebie zapach s^lomy i koni. ";
        if(CO_PADA(TO) == NIC_NIE_PADA)
        {
            str += "W^askie przej^scie prowadz^ace od bramy w palisadzie "+
                "wy^lo^zone jest kocimi ^lbami, "+
                "dzi^eki czemu jest tu sucho i czysto. ";
        }
        else
        {
            str += "W^askie przej^scie prowadz^ace od bramy w palisadzie "+
                "wy^lo^zone jest kocimi ^lbami, "+
                "dzi^eki czemu nawet teraz w czasie deszczu pod^lo^ze jest "+
                "w miar^e suche i mo^zna chodzi^c po nim bez wi^ekszych "+
                "problem^ow. ";
        }
        str += "Na p^o^lnocnym zachodzie mo^zesz dostrzec do^s^c du^zych "+
            "rozmiar^ow plac a na p^o^lnocnym wschodzie budynek z kamiennym "+
            "kominem";
        if(jest_dzien() == 1)
        {
            str += ", z kt^orego unosi si^e cienka stru^zka dymu. ";
        }
        else
        {
            str += ". ";
        } 
        str += "Dooko^la "+
            "panuje porz^adek - wida^c ^ze kto^s stara si^e zachowa^c to "+
            "miejsce w przyzwoitym stanie.\n";
    }
    return str;
}

void
prevent_enter(object ob)
{
    //Tylko gracz� ustawiamy startlocka, wykluczamy wiz�w i nie cz�onk�w zakonu.
    if(interactive(ob) && !ob->query_wiz_level() && ob->query_gildia(GUILD_NAME))
        ob->set_default_start_location(ZAKON_LOKACJE + "sypialnia.c");
}