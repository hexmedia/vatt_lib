#include "dir.h"

inherit REDANIA_STD;

#include <mudtime.h>
#include <language.h>
#include <stdproperties.h>

string dlugi_opis();
// public function event_ulicy_ridne();

void
create_rinde()
{
}

nomask void
create_redania()
{
    set_long(&dlugi_opis()); // no i teraz nie trzeba dawac set_long

    // tutaj np. eventy i inne rzeczy wspolne dla lokacji rinde
    add_item(({"miasto","miasteczko"}), "Tak, to jest w�a�nie Rinde!\n");

// //     add_event("@@event_ulicy_rinde@@");

// //     set_event_fun(event_ulicy_rinde());
    set_event_time(300.0, 50.0);

    set_alarm_every_hour("dzwon");

    set_start_place(RINDE_SLEEP_PLACE);
    add_prop(ROOM_I_TYPE, ROOM_IN_CITY);
    create_rinde();
}

// Dzwon wybijajacy godzine, slychac to na ulicach. Lil.
int
dzwon()
{
    int godzinka = MT_NHOUR;

    if (godzinka >= 4 && godzinka <= 22)
    {
        if (godzinka > 12) godzinka -= 12;
        {
            tell_room(this_object(), "Bim! Bam! Ratuszowy dzwon wybija godzin^e " +
                LANG_SORD(godzinka, PL_BIE, PL_ZENSKI) + ".\n");
        }
    }
}

string dlugi_opis() { return "Rinde"; }