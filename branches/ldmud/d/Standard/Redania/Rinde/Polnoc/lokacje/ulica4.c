/* LIL */
#include <stdproperties.h>
#include <mudtime.h>
#include <macros.h>
#include <pogoda.h>
#include "dir.h"

inherit STREET_STD;

string
koleiny()
{
    string str = "Koleist� ju^z ulic^e wci�^z orz� wozy przeje^zd^zaj�ce t^edy, "+
       "tworz�c pod^lu^zne bruzdy";

    if (CZY_JEST_WODA(TO))
        str += "wype^lnione w tej chwili ka^lu^zami";
    str += ".\n";
    return str;
}

void create_street()
{
    set_short("Ulica przed gospod�");
    add_exit("ulica3.c","p^o^lnocny-zach^od");
    add_exit("ulica5.c","po^ludnie");
    add_exit("ulica6.c","po^ludniowy-wsch^od");
//    add_exit("karczma/sala.c","karczma");
    add_object(POLNOC_DRZWI + "karczma");
    add_prop(ROOM_I_INSIDE,0);
    add_sit("na ziemi","na ziemi","z ziemi",0);
    add_sit("na stopniu","na stopniu do gospody","ze stopnia",2);

    add_item(({"ziemi^e","utwardzon� ziemi^e","ulic^e","utwardzon� ulic^e"}),
           "Wy^z^lobiona licznymi koleinami ulica ci�gnie prosto jak strza^la "+
       "od bramy p^o^lnocnej, w kierunku bramy wschodniej, "+
       "przecinaj�c w ten spos^ob obrze^za miasta i znacznie odci�^zaj�c "+
       "tym samym centrum miasta.\n");
    add_item(({"koleiny","pot^e^zne koleiny","koleiny na drodze","koleiny na ulicy",
           "pot^e^zne koleiny na drodze","pot^e^zne koleiny na ulicy"}),
           "@@koleiny@@");
    add_item("kamienice",
           "Ci�gn�ce si^e wzd^lu^z ulicy kamienice zapewne "+
       "pami^etaja lepsze czasy. S� szare i maj� "+
       "obdrapane �ciany.\n");
    add_item(({"gospod^e","karczm^e","najwi^eksz� budowl^e"}),
           "Ta poka�na budowla ma dwa pi^etra. Parter ci�gni^e si^e przez "+
       "ca^l� d^lugo�^c ulicy, a na po^ludniowym wschodzie ko^nczy si^e "+
       "dobudowan� stajni�. Do karczmy prowadz� dwa stopnie "+
       "wyrastaj�ce niemal^ze z samej ulicy. Skromny szyld ko^lysz�cy si^e "+
       "nad drzwiami zdradza nazw^e \"Baszta\", a nad nim "+
       "dostrzegasz balustrad^e, zapewne karczemnego tarasiku dla go�ci.\n");
    add_item(({"dr^o^zk^e","w�sk� dr^o^zk^e"}), "Wiedzie ona na po^ludnie, do centrum.\n");
    add_item(({"stopie^n","stopnie","kr^otki stopie^n","kr^otkie stopnie"}),
           "Dwa kr^otkie drewniane stopnie wystaj� tu^z z ulicy i prowadz� do "+
       "karczemnych drzwi.\n");

    add_object(KARCZMA_OBIEKTY+"szyld");
    add_window(POLNOC_DRZWI + "okno_do_karczmy.c");

    add_object(RINDE+"przedmioty/lampa_uliczna");

}
public string
exits_description()
{
    return "Ulica prowadzi na p^o^lnocny-zach^od, po^ludnie oraz "+
           "po^ludniowy-wsch^od. Jest tutaj tak^ze wej^scie do karczmy.\n";
}


string dlugi_opis()
{
    string str;
    str="Poka�na pi^etrowa budowla na obrze^zach miasta "+
        "stoi tu^z przy ulicy. Mocarne z metalowymi "+
    "wyko^nczeniami i zawiasami drzwi dziel� od "+
    "samej drogi jedynie dwa, kr^otkie stopnie. "+
    "Obok, po obu stronach maj� swoje miejsca dwa, "+
    "du^zych rozmiar^ow okna, przez kt^ore ";
    if(!dzien_noc())
      str+="mo^zna dojrze^c nieco karczemnego gwaru";
    else
      str+="s�czy si^e �wiat^lo, o�wietlaj�c do�^c mocno ulic^e";

    str+=". Na pi^etrze, nad ko^lysz�cym si^e szyldem, "+
         "ledwie widoczna st�d balustrada zdradza "+
     "istnienie tarasu gospody. Utwardzona, "+
     "koleista ulica ci�gnie si^e prosto jak "+
     "strza^la od p^o^lnocnej bramy w stron^e bramy "+
     "wschodniej, odci�^zaj�c tym samym centrum "+
     "miasta od przeje^zd^zaj�cych tu co rusz woz^ow. "+
     "Jest tu jeszcze jedna, bardzo w�ska dr^o^zka "+
     "wiod�ca w stron^e centralnego placu. Jak okiem "+
     "si^egn�^c, wzd^lu^z ulicy znajduj� si^e kamienice, "+
     "a po drugiej stronie s^lawna gospoda, z "+
     "przybudowan� do^n stajni� na po^ludniowym "+
     "wschodzie.";




    str+="\n";
    return str;
}

/*
OBIEKTY:
mocarne drzwi
Wygl�daj� na solidn� robot^e, acz sprawiaj� wra^zenie jakby wiecznie nie domykaj�cych si^e. Gospoda jest czynna ca^l� dob^e i w �wi^eta, dlatego te^z tak mocne drzwi s^lu^z� chyba w g^l^ownej mierze jako ozdoba.

skromny szyld
Unosz�cy si^e nad drzwiamy skromny, lekki szyld porusza si^e co jaki� czas przy nawet najl^zejszym podmuchu wiatru. Na szcz^e�cie nie skrzypi, jak to maj� w zwyczaju wszystkie takowe szyldy, cho^c jest uczepiony belki ^la^ncuchami. Na jego powierzchni, bez ^zadnych ozd^ob napisano najzwyklejsz� w �wiecie czcionk�, du^zymi literami nazw^e karczmy: BASZTA.

dwa okna
*/
