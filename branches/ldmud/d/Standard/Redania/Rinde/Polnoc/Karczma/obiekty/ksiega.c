/* Jest to ksi�ga...  Vera
 */

inherit "/std/book";

#include "dir.h"
#include <materialy.h>
#include <stdproperties.h>
#include <macros.h>
#include <object_types.h>

create_book()
{
    ustaw_nazwe("ksi�ga");
    dodaj_przym("stary","starzy");
    set_long("Widoczne zadrapania, zagi�cia i po��k�y kolor "+
             "�wiadcz� o wiekowo�ci tej ksi�gi. Obita jest w star�, "+
             "maj�c� ju� lata swej �wietno�ci dawno za sob� sk�r�.\n");
    add_prop(OBJ_I_VALUE, 400);
    add_prop(OBJ_I_NO_GET, "Ksi�ga jest przymocowana �a�cuchem do sto�u!\n");
    add_prop(OBJ_I_WEIGHT, 7001);
    add_prop(OBJ_I_VOLUME, 1999);
    setuid();
    seteuid(getuid());
    set_type(O_KSIAZKI);
    ustaw_material(MATERIALY_LEN);
    set_max_pages(3);
    add_prop(OBJ_I_DONT_GLANCE,1);
    set_owners(({DAMIR,RINDE_NPC+"pomagacz_damira"}));

}

void
read_book_at_page(int strona)
{
    switch (strona)
    {
        case 1: write("\t\t\tSPIS TRE�CI:\n\n"+
                "\t\t 1.   Ten spis.\n"+
                "\t\t 2.   Wst�p.\n"+
                "\t\t 3.   Zasady.\n\n");
                break;

        case 2: write("\t\tWST�P:\n"+
                      "Witaj �miertelniku. Wkroczy�e� w�a�nie "+
                      "do �wiata w drugiej fazie test�w. Odnajdziesz tu "+
                      "zapewne mn�stwo b��d�w. Nie krepuj si� "+
                      "z u�yciem komendy 'zg�o�' oraz "+
                      "(w ekstramalnych sytuacjach) 'przyzwij'.\n"+
                      "Pozdrawiamy, Czarodzieje z Thanedd.\n");break;
        case 3: TP->command("?zasady"); break;
    }
}
