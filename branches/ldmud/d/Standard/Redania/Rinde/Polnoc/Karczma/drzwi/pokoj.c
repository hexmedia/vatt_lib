inherit "/std/door";

#include <pl.h>
#define KOD_DRZWI "gorakarczmy"
#include "dir.h"

void
create_door()
{
    ustaw_nazwe( ({ "drzwi", "drzwi", "drzwiom", "drzwi", "drzwiami",
        "drzwiach" }), PL_NIJAKI_OS);

    dodaj_przym("drewniany", "drewniani");

    set_other_room(KARCZMA_LOKACJE + "gora.c");

    set_door_id(KOD_DRZWI);

    set_door_desc("Masz przed sob� drewnine drzwi. Niezbyt dobrze zbita "+
       "kupa desek i nic wiecej. Zuwa�asz zasuwke, kt�ra mo�esz "+
       "zamkn�� drzwi.\n");

    set_open_desc("");
    set_closed_desc("");

    set_pass_command(({"drzwi", "wyj�cie"}),"przez drzwi na zewn�trz","z pokoju");
    set_pass_mess("Wychodzisz przez drewniane drzwi na po�udnie.\n");

    set_lock_command("zasu� zasuwk� %i:" + PL_DOP, "zasun��");
    set_lock_mess("zasuwa zasuwk� w drewnianych drzwiach.\n", "Zasuwasz zasuwk� w drewnianych drzwiach.\n",
        "S�yszysz jaki� szcz�k po drugiej stronie drzwi... jakby kto� przesuwal zasuwk�?\n");

    set_unlock_command("odsu� zasuwk� %i:" + PL_DOP, "odsun��");
    set_unlock_mess("odsuwa zasuwk� w drewnianych drzwiach.\n", "Odsuwasz zasuwk� w drewnianych drzwiach.\n",
        "S�yszysz jaki� szcz�k po drugiej stronie drzwi... jakby kto� przesuwa� zasuwk�?\n");

    //Pe�na nazwa zamka - lub tylko mianownik je�li jest w s�owniku
    set_lock_name(({"zamek", "zamku", "zamkowi", "zamek", "zamkiem", "zamku"}), 0, PL_MESKI_NOS_NZYW);
    set_lock_desc("Ohh, Zwykla zasuwka... taka jak w kazdym przykladzie. "+
       "Z pewnoscia nie jest w stanie za duzo wytrzymac.\n");

    //set_locked(1);
    //set_pick(88);
}
