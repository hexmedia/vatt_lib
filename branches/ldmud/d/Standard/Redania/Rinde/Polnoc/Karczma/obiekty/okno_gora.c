inherit "/std/window";

#include <pl.h>
#include <formulas.h>
#define KOD_OKNA "asdfwaerqwefasdvzxcvfqtdsafadsfsfasf"
#include "dir.h"

void
create_window()
{
    ustaw_nazwe( ({ "okno", "okna", "oknu", "okno", "oknem",
        "oknie" }), ({ "okna", "okien", "oknom", "okna", "oknami", "oknach" }),
        PL_NIJAKI_OS);

    dodaj_przym("du�y", "duzi");

    set_window_id(KOD_OKNA);
    set_open(0);
    set_open_desc("");
    set_closed_desc("");
    set_window_desc("Jest to ca�kiem spore okno, za kt�rym widzisz dach stajni "+
        "oraz ulice przed karczm�.\n");

    ustaw_pietro(1);

    set_other_room(KARCZMA_LOKACJE + "podworze");
}
