/* Misa,
   Mo�esz si� z niej napi�,napelnij ja,
   przemy� si� w niej i wyla� przez okno jej zawarto �.
    Zaprojektowana specjalnie dla karczmy Baszta,
      na polecenie Damira, karczmarza;)                  */

inherit "/std/beczulka.c";
#include <pl.h>
#include <cmdparse.h>
#include <stdproperties.h>
#include <wa_types.h>
#include <formulas.h>
#include <macros.h>
#include <composite.h>
#include "dir.h"

public int umyj(string str);
public int napelnij(string str);
int sukces(object obj1, object obj2);
object okno;

void
create_beczulka()
{
    set_pojemnosc(6000);
    //set_opis_plynu("krystalicznie czystej wody");
    set_opis_plynu("m�tnej wody");
    set_nazwa_plynu_dop("wody");
    set_vol(0);
    set_ilosc_plynu(5000);
    set_dlugi_opis("Bia�a, obt�uczona misa.");
        
    ustaw_nazwe( ({ "misa", "misy", "misie",
                    "mis�", "mis ", "misie" }),
                 ({ "misy", "mis", "miso", "misy",
                    "misami", "misach" }), PL_ZENSKI);
    dodaj_przym( "bia�y", "biali" );
    dodaj_przym( "obt�uczony", "obt�uczeni" );


   add_prop(OBJ_M_NO_GET, 1);
//   add_prop(OBJ_I_DONT_GLANCE,1);
    set_owners(({DAMIR,RINDE_NPC+"pomagacz_damira"}));


}

public void
init()
{
     ::init();
     
     add_action(umyj, "umyj");
     add_action(napelnij, "nape�nij");
}

private int
napij(string str)
{
    object *obs;
    int ret;
    
    notify_fail("Napij si� z czego?\n");
    
    if (!strlen(str))
        return 0;
    
    if (!parse_command(str, environment(this_player()), 
        "'si�' 'z' %i:" + PL_DOP, obs))
        return 0;
    
    obs = NORMAL_ACCESS(obs, 0, 0);
    
    if (!sizeof(obs))
        return 0;
        
    if (sizeof(obs) > 1)
    {
        notify_fail("Mo�esz si� napi� z tylko jednej rzeczy naraz.\n");
        return 0;
    }
    
    ret = obs[0]->drink_from_it(this_player());
    if (ret)
        this_player()->set_obiekty_zaimkow(obs);
    return ret;
}

public int
wylej(string str)
{
    object *obs;
    int ret;
    
    notify_fail("Wylej co? Moze zawarto^s^c " + query_nazwa(PL_DOP) + ".\n");
    
    if (!strlen(str))
        return 0;
        
    if (!parse_command(str, environment(this_player()), 
        "'zawarto^s^c' %i:" + PL_DOP, obs))
        return 0;
    
    obs = NORMAL_ACCESS(obs, 0, 0);
    
    if (!sizeof(obs))
        return 0;
        
    if (sizeof(obs) > 1)
    {
        notify_fail("Mo^zesz wyla^c zawarto^s^c tylko jednej rzeczy naraz.\n");
        return 0;
    }

    if (sizeof(obs) > 1)
    {
        notify_fail("Mo�esz wyla� zawarto � tylko jednej rzeczy naraz.\n");
        return 0;
    }

    ret = obs[0]->wylej_from_it();
    if (ret)
        this_player()->set_obiekty_zaimkow(obs);

    return ret;
}


public int
wylej_from_it()
{
    object *okna;
    if (!query_ilosc_plynu())
    {
        write(capitalize(short()) + " jest zupe�nie pust" + 
            koncowka("y", "a", "e") + ".\n");
        return 1;
    }

    okna = filter(all_inventory(environment( this_object() ) ),
             &operator(!=)(0,) @ &->query_window_desc());

    okna = filter(okna, &operator(!=)(0,) @ &->query_open());

    if (!sizeof(okna))
    {
        notify_fail("Gdzie chcesz to wyla^c? Otw^orz najpierw okno.\n");
        return 0;
    }
    
    write("Chwytasz " + short(PL_BIE) + " i z zamachem wylewasz ca�� " + 
          "jej zawarto�� przez okno.\n");
    say(QCIMIE(this_player(), PL_MIA) + " chwyta " +
        QSHORT(this_object(), PL_BIE) + " i bior�c szeroki zamach wylewa ca�� " + 
        "jej zawarto � przez okno.\n");
    tell_room(KARCZMA_LOKACJE + "podworze", "Z jednego z okien "+
              "pi�tra karczmy kto� w�a�nie wyla� jakie� pomyje.\n");
        
    set_ilosc_plynu(0);
    set_vol(0);
    set_opis_plynu("");
    set_nazwa_plynu_dop("");
    
    return 1;
}

public int
umyj(string str)
{
    int myj;
    int nowa;

    if (!str || !(str ~= "si^e w misie") )
    {
        notify_fail("Chcesz mo�e umy� si� w misie?\n");
        return 0;
    }  
    if (!query_ilosc_plynu())
    {
        write(capitalize(short()) + " jest zupe�nie pust" + 
            koncowka("y", "a", "e") + ".\n");
        return 1;
    }
    if (query_nazwa_plynu_dop() != "wody")
    {
        write("Nie u�yjesz chyba " + 
            query_nazwa_plynu_dop() + " do mycia?\n");
        return 1;
    }

    myj = MIN(query_ilosc_plynu(), 550);
    nowa = query_ilosc_plynu() - myj;



    write("Wk�adasz d�onie do " + query_opis_plynu() + " i kilkoma szybkimi " + 
          "ruchami ochlapujesz sobie twarz. Przecierasz oczy i gwa�townym skr�tem " +
          "g�owy strzepujesz krople " + query_nazwa_plynu_dop() + " na pod�og�.\n");
    say(QCIMIE(this_player(), PL_MIA) + " zanurza r�ce w " +
        QSHORT(this_object(), PL_MIE) + " i kilkoma ruchami ochlapuje sobie " + 
        "twarz. Szybko przeciera oczy i gwa�townym skr�tem g�owy strzepuje " +
        "krople " + query_nazwa_plynu_dop() + " na pod�og�.\n");
        
    if (nowa == 0)
    {
        set_vol(0);
        set_opis_plynu("");
        set_nazwa_plynu_dop("");
    }

    set_ilosc_plynu(nowa);


return 1;
}


private int
napelnij(string str)
{
    object *obs1;
    object *obs2;
    object obj1;
    object obj2;

    notify_fail("Nape�nij co z czego?\n");
    
    if (!strlen(str))
        return 0;
    
    if (!parse_command(str, deep_inventory(environment(this_player())), 
        "%i:" + PL_BIE + " 'z' %i:" + PL_DOP, obs1, obs2))
        return 0;
    
    obs1 = NORMAL_ACCESS(obs1, 0, 0);
    obs2 = NORMAL_ACCESS(obs2, 0, 0);

    this_player()->set_obiekty_zaimkow(obs1, obs2);

    if (!sizeof(obs1))
        return 0;
    if (!sizeof(obs2))
        return 0;
        
    if (sizeof(obs1) > 1)
    {
        notify_fail("Mo�esz nape�ni� tylko jedn  rzecz naraz.\n");
        return 0;
    }
    if (sizeof(obs2) > 1)
    {
        notify_fail("Mo�esz nape�nia� tylko z jednej rzeczy naraz.\n");
        return 0;
    }
    obj1 = obs1[0];
    obj2 = obs2[0];

    if (obj1 == obj2)
    {
        notify_fail("Do nape�niania potrzebne ci b�d  dwa r�ne przedmioty.\n");
        return 0;
    }
    if (living(obj1))
    {
        return 0;
    }
    if (living(obj2))
    {
        return 0;
    }
    if (!obj1->query_pojemnosc())
    {
        return 0;
    }
    if (!obj2->query_nazwa_plynu_dop())
    {
        return 0;
    }
    if (obj2->query_nazwa_plynu_dop()=="")
    {
        notify_fail(capitalize(obj2->short()) + " nie zawiera nic" +
                    " czym mo�na by cokolwiek nape�ni�.\n");
        return 0;
    }
    if (obj1->query_ilosc_plynu() == obj1->query_pojemnosc())
    {
        notify_fail(capitalize(obj1->short()) + " jest ju� pe�n" +
                    obj1->koncowka("y", "a", "ne", "ni", "ne") + "\n");
        return 0;
    }
    if (obj1->query_ilosc_plynu())
    {
        if (obj1->query_vol()==obj2->query_vol()&&
            obj1->query_opis_plynu()==obj2->query_opis_plynu()&&
            obj1->query_nazwa_plynu_dop()==obj2->query_nazwa_plynu_dop())
        {
            sukces(obj1,obj2);
            return 1;
        }

        notify_fail("Musisz najpierw opr�ni� " + obj1->short(PL_BIE) + ".\n");
        return 0;
    }

    sukces(obj1,obj2);
    return 1;
}

int 
sukces(object obj1, object obj2)
{
    int nowa;

    write("Nape�niasz " + obj1->short(PL_BIE) + " z " + obj2->short(PL_DOP) + ".\n");
    say(QCIMIE(this_player(), PL_MIA) + " nape�nia " +
        QSHORT(obj1, PL_BIE) + " z " + QSHORT(obj2, PL_DOP) + ".\n");

    nowa = MIN(obj1->query_pojemnosc() - obj1->query_ilosc_plynu(), obj2->query_ilosc_plynu()); 

    obj1->set_ilosc_plynu(obj1->query_ilosc_plynu() + nowa);
    obj1->set_vol(obj2->query_vol());
    obj1->set_opis_plynu(obj2->query_opis_plynu());
    obj1->set_nazwa_plynu_dop(obj2->query_nazwa_plynu_dop());

    if (nowa == obj2->query_ilosc_plynu())
    {
        obj2->set_ilosc_plynu(0);
        obj2->set_vol(0);
        obj2->set_opis_plynu("");
        obj2->set_nazwa_plynu_dop("");
        return 1;
    }

    obj2->set_ilosc_plynu(obj2->query_ilosc_plynu() - nowa);

    return 1;
}
