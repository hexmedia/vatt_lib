/*    Taras karczmy Baszta, Rinde.
              Lil & Alcyone 16.6.2005 */

//Chujowy opis, wiem. L.

#include "dir.h"

inherit RINDE_STD;

#include <stdproperties.h>
#include <mudtime.h>
#include <macros.h>


int wyskocz_przez_taras();

void
create_rinde()
{
    int ind;
    mixed exits;

    set_short("Ma�y taras");
    set_long("@@dlugi_opis@@");

    add_cmd_item(({"przez barierk�"}), "przeskocz", wyskocz_przez_taras,
                    "Przez co chcesz przeskoczy�?\n");
    add_cmd_item(({"przez taras", "na ulic^e", "przez barierk^e"}),
                    "wyskocz", wyskocz_przez_taras, "Gdzie chcesz wyskoczy�?\n");
// add_sit("przy stole", "z hukiem przy stole", "od sto^lu", 2);
    add_sit("na pod�odze", "na pod�odze", "z pod�ogi", 0);
    add_exit("gora", "polnoc", 0, 0);
    //add_exit(POLNOC_LOKACJE + "ulica4", "d", 0, 1, 0);
    //Po co przeskakiwac barierke jezeli mozna zejsc bez straty hp na dol?
    // Avard
    add_prop(ROOM_I_INSIDE,0);

    add_object(MEBLER+"krzeslo_piekarnia");
    add_object(MEBLER+"krzeslo_piekarnia");
    add_object(MEBLER+"stolik_balkonowy");
    dodaj_rzecz_niewyswietlana("drewniane krzes�o", 2);
    dodaj_rzecz_niewyswietlana("niewielki stolik", 1);

   // wyciag z szyszki ]:>
 /*   exits = find_object(POLNOC_LOKACJE + "ulica4")->query_exit();
    if (!sizeof(exits)) {
        return;
    }
    for (ind = 0; ind < sizeof(exits); ind += 3) {
        if ((exits[ind+1] ~= "p�noc") ||
                (exits[ind+1] ~= "p�nocny-wsch�d") ||
                (exits[ind+1] ~= "wsch�d") ||
                (exits[ind+1] ~= "po�udniowy-wsch�d") ||
                (exits[ind+1] ~= "po�udnie") ||
                (exits[ind+1] ~= "po�udniowy-zach�d") ||
                (exits[ind+1] ~= "zach�d") ||
                (exits[ind+1] ~= "p�nocny-zach�d")) {
            add_exit(exits[ind], exits[ind+1], 1, 1, 1);
                    }
    }*/


}
public string
exits_description()
{
    return "Wyj^scie z balkonu jest na p^o^lnocy i prowadzi do korytarza "+
           "karczmy.\n";
}

int
wyskocz_przez_taras()
{
    saybb(QCIMIE(this_player(), PL_MIA) + " przeskakuje przez barierk^e "+
        "balkonu karczmy i l^aduje na bruku tu^z przed tob^a.\n");
    write("Szybkim skokiem pokonujesz barierk� i l�dujesz pokracznie na bruku.\n");

    this_player()->move(POLNOC_LOKACJE + "ulica4");
    this_player()->do_glance(this_player()->query_option(2));
    this_player()->reduce_hp(0, 2000 + random(1000), 10);

    return 1;
}

string
dlugi_opis()
{
    string str;
    int i, il, il_krzesel, il_stolikow;
    object *inwentarz;

    str =  "Metalowa por�cz, kt�r� zdobi� proste i skromne "+
        "ornamenty, dok�adnie ogradza niewielki taras. "+
        "Na jego kamiennej pod�odze znajduj� si� wyblak�e ju� "+
        "wzory. ";

    inwentarz = all_inventory();
    il_krzesel = 0;
    il_stolikow = 0;
    il = sizeof(inwentarz);

    for (i = 0; i < il; ++i) {
		if (inwentarz[i]->czy_jestem_stolikiem()) {
			++il_stolikow;
		}
		if (inwentarz[i]->test_rwopk1()) {
			++il_krzesel;
		}
	}

   if ((il_stolikow == 0) && (il_krzesel == 1))
    {str += "Pod �cian� stoi drewniane krzes�o. ";}
    if ((il_stolikow == 0) && (il_krzesel == 2))
    {str += "Pod �cian� stoj� dwa drewniane krzes�a. ";}
    if ((il_stolikow == 1) && (il_krzesel == 1))
    {str += "Pod �cian� stoi niewielki stolik i drewniane krzes�o. ";}
    if ((il_stolikow == 1) && (il_krzesel == 2))
    {str += "Pod �cian� stoi niewielki stolik i dwa drewniane krzes�a. ";}
    if ((il_stolikow == 1) && (il_krzesel == 0))
    {str += "Pod �cian� stoi niewielki stolik. ";}


   /*if (il_stolikow == 1) {
			str += "Pod �cian� stoi male�ki stolik ";
		}
   if (il_krzesel == 1)  {str += "i drewniane krzes�o. ";}
   if (il_krzesel == 2)  {str += "i dwa drewniane krzes�a. ";}*/


	  str += "Widok rozci�gaj�cy si� wok� przedstawia niezbyt "+
	         "zadban� i cicha ulic�, na ktor� wychodzi balkon.\n";


	return str;
}
