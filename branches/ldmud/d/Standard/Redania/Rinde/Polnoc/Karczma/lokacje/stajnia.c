/* Stajnia karczmy Baszta, Rinde.
   Opisy - Alcyone & Lil   */

#include "dir.h"
inherit RINDE_STD;
#include <macros.h>
#include <stdproperties.h>
#include <mudtime.h>

string query_opis_swiatla();
string query_czy_cos_jest_na_szafce();

void
create_rinde()
{
   set_short("Stajnia.");
   set_long("Zaduch panuj�cy w tym du�ym pomieszczeniu jest "+
            "do�� uci��liwy. Przez drewniane deski @@query_opis_swiatla@@. "+
	    "Wn�trze jest na tyle przestronne, �e z �atwo�ci� mieszcz� "+
	    "si� tutaj po obu stronach boksy dla koni w ten spos�b, by "+
	    "pomi�dzy nimi powsta�o niezbyt szerokie przej�cie. Pod "+
	    "jedn� ze �cian ustawiono drewnian� "+
	    "szafk�@@query_czy_cos_jest_na_szafce@@. Dooko�a unosi "+
	    "si� przyjemny zapach siana, kt�re jest porozrzucane "+
	    "na ziemi i u�o�one w k�cie.\n");

   add_item(({"boksy","boks"}),
            "Du�e drewniane boksy wykonane s� z ciemnego starannie "+
	    "oheblowanego drewna, kt�re idealnie nadaje si� do "+
	    "miejsc takich jak te.\n");
   add_item("siano",
            "Z�ociste, pachn�ce siano porozrzucane jest na ziemi, ale tak�e w "+
	    "jednym z wolnych k�t�w starannie u�o�ono je w niedu�y stog.\n");


   add_object(KARCZMA_OBIEKTY+"szafka_stajenna");

   add_prop(ROOM_I_INSIDE,1);
   add_sit("na stogu siana","na stogu siana","ze stogu siana",5);

   add_exit("podworze",({"podw^orze","na podw�rze","ze stajni"}));
   add_exit(POLNOC_LOKACJE+"ulica6", ({"wyj^scie","w kierunku wyj^scia",
                                    "ze stajni"}));
}
public string
exits_description()
{
    return "Wyj^scie prowadzi na ulic^e, jest tutaj tak^ze wej^scie na "+
           "podw^orze.\n";
}

string
query_opis_swiatla()
{
    string str;
    if(jest_dzien() == 1)
    {
        str = "z trudem przeciskaj� si� jasne promienie swiat�a, "+
	          "rozja�niaj�c nieco p�mrok panuj�cy w �rodku";
    }
    else
    {
        str = "wpada delikatny blask ulicznych latarni, a lampy zawieszone "+
	          "przy suficie daj� nik�e swiat�o";
    }
}

string
query_czy_cos_jest_na_szafce()
{
   if(sizeof(present("szafka", TO)->subinventory("na")) > 0)
   return ", na kt�rej pouk�adane s� jakie� przedmioty";

   else
   return "";
}
