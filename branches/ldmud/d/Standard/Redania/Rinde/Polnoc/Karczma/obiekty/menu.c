/**
 * \file /d/Standard/Redania/Rinde/Polnoc/Karczma/obiekty/menu.c
 *
 * Menu karczmy Baszta.
 *
 * Lil 06.07.2005
 *
 */

inherit "/std/object.c";

#include "dir.h"
#include <pl.h>
#include <colors.h>
#include <macros.h>
#include <filter_funs.h>
#include <stdproperties.h>
int przeczytaj_menu();

create_object()
{
    ustaw_nazwe("tabliczka");
    dodaj_nazwy("tablica");
    dodaj_nazwy("menu");

    set_long("\n" +
        "     _________________________________________\n"+
        "   / o                 MENU                  o \\\n"+
        "   |                                           |\n"+
        "   |       " + SET_COLOR(COLOR_FG_CYAN) + "Dania g��wne:" + CLEAR_COLOR+
        "                       |\n"+
        "   |   Jajecznica z serem          3 grosze    |\n"+
        "   |   Ry�                         6 groszy    |\n"+
        "   |   Bigos                       3 groszy    |\n"+
        "   |   Sa�atka owocowa             5 groszy    |\n"+
        "   |   Zupa pomidorowa             9 groszy    |\n"+
        "   |                                           |\n"+
        "   |       " + SET_COLOR(COLOR_FG_CYAN) + "Zak�ski:" + CLEAR_COLOR+
        "                            |\n"+
        "   |   Kie�basa                    5 groszy    |\n"+
        "   |   Chleb                       6 groszy    |\n"+
        "   |                                           |\n"+
        "   |       " + SET_COLOR(COLOR_FG_CYAN) + "Napoje:" + CLEAR_COLOR+
        "                             |\n"+
        "   |   Mleko                       3 grosze    |\n"+
        "   |   Sok wi�niowy                4 grosze    |\n"+
        "   |   Woda                        1 grosze    |\n"+
        "   |   Jasne piwo                 10 groszy    |\n"+
        "   |     w beczu�ce 10l         180 groszy     |\n"+
        "   |   Reda�skie piwo            12 groszy     |\n"+
        "   |   Czerwone wino               6 groszy    |\n"+
        "   |     w buk�aku 1,5l          37 groszy     |\n"+
        "   |   Jab�ecznik                 6 grosze     |\n"+
        "   |     w buk�aku 2l            44 groszy     |\n"+
        "   |   W�dka                      1 groszy     |\n"+
        "   |     w buk�aku 1l            30 groszy     |\n"+
        "   | o                                      o  |\n"+
        "   \\___________________________________________/\n");

    add_prop(OBJ_I_WEIGHT, 485);
    add_prop(OBJ_I_NO_GET, "Tabliczka zosta^la mocno przybita do kontuaru.\n");
    add_prop(OBJ_I_DONT_GLANCE,1);

    add_cmd_item(({"menu","tabliczk^e"}), "przeczytaj", przeczytaj_menu, "Przeczytaj co?\n");
}

int
przeczytaj_menu()
{
   return long();
}
