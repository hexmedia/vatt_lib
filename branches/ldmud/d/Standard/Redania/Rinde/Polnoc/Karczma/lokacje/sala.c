/**
 * \file /d/Standard/Redania/Rinde/Polnoc/Karczma/lokacje/sala.c
 *
 * Karczma "Baszta"
 *   Gregh & Lil
 *      02.2005,
 *      opisy Lil&Zwierzaczek
 */
#include "dir.h"

inherit RINDE_STD;
inherit "/lib/peek";
inherit "/lib/pub_new";

#define SIEDZACY "_siedzacy"
#include <macros.h>
#include <filter_funs.h>
#include <ss_types.h>
#include <stdproperties.h>
#include <wa_types.h>
#include <cmdparse.h>
#include <pl.h>
#include <options.h>
#include <formulas.h>

int zasiedzenie;
string query_opis_kotary();

string query_opis_drzwiczek();

int wyjdz_na_zewnatrz();

void
create_rinde()
{
    set_short("Sala g^l^owna");
    set_long("@@dlugi_opis@@");

    add_sit("przy barze", "przy barze", "od baru", 3);

    add_exit("kuchnia", ({"polnoc","do kuchni","z sali g��wnej"}));
    add_exit("gora", ({"gora","schodami na g�r�","z do�u"}), wyjdz_na_zewnatrz, 0);

    add_prop(ROOM_I_INSIDE,1);

    add_item(({"lad�","bar"}),
        "Wysoka, wykonana z mahoniowego drewna lada o nieco zarysowanym ju� "+
        "blacie, dodaje temu miejscu niejakiego uroku i charakteru typowej, "+
        "miejskiej karczmy. Na jej przodzie stoj� trzy grube belki podpieraj�ce "+
        "sufit. Jedna z nich zaopatrzona jest w spor� tabliczk�, s�u��c� za menu.\n");
    add_item("pochodnie", "@@opisik_pochodni@@");

    add_peek("przez okno", POLNOC_LOKACJE+"ulica4.c");
    add_peek("przez pierwsze okno", POLNOC_LOKACJE+"ulica4.c");
    add_peek("przez drugie okno", POLNOC_LOKACJE+"ulica4.c");

    add_npc(RINDE_NPC + "karczmarz_baszty");

    add_object(KARCZMA_OBIEKTY + "wiekszy_stol");
    add_object(KARCZMA_OBIEKTY + "mniejszy_stol");
    add_object(KARCZMA_OBIEKTY + "menu");
    add_object(KARCZMA_OBIEKTY + "kominek");

    add_door(KARCZMA_DRZWI + "wyjsciowe");
    add_door(KARCZMA_DRZWI + "za_kotare.c");
    add_door(KARCZMA_DRZWI + "na_podworze.c");

    add_window(POLNOC_DRZWI + "okno_z_karczmy.c");

    dodaj_rzecz_niewyswietlana("du�y st�");
    dodaj_rzecz_niewyswietlana("d�bowy st�");
    dodaj_rzecz_niewyswietlana("niewielki kominek");

    add_drink("mleko", 0, "�wie�ego mleka", 400, 0, 8, "kubek", "mleka", "Drewniany kubek.");
    add_drink(({"sok wi�niowy","sok"}), 0, "soku wi�niowego", 400, 0, 10,
        "kubek", "soku", "Drewniany kubek.");
    add_drink("woda", 0, "krystalicznie czystej wody", 500, 0, 2, "kubek", "wody","Drewniany kubek.");
    add_drink("piwo", ({ "jasny", "ja�ni"}), "jasno-bursztynowego, " +
        "wspaniale pieni�cego si� piwa", 700, 4, 15, "kufel", "piwa",
        "Jest to kufel wykonany z krystalicznego szk�a, kt�rego uchwyt " +
        "wykonano z dobrego d�bowego drewna.",10000);
    add_drink("piwo", ({ "reda�ski", "reda�scy" }), "jasno-bursztynowego, " +
        "wspaniale pieni�cego si� piwa", 750, 5, 16, "kufel", "piwa",
        "Jest to kufel wykonany z krystalicznego szk�a, kt�rego uchwyt " +
        "wykonano z dobrego d�bowego drewna.");
    add_drink("wino", ({ "czerwony", "czerwoni" }), "czerwonego wina",
        350, 10, 18, "kubek", "wina", "Drewniany kubek.", 1500);
    add_drink("jab�ecznik", 0, "jab�ecznika, s�ynnego w ca�ej Redanii",
        400, 7, 17, "kubek", "jab^lecznika", "Drewniany kubek.", 2000);
    add_drink("w�dka", ({ "zimny", "zimni" }), "w�dki", 50, 45, 20,
        "kieliszek", "w�dki", "Niewielki, wykonany ze szk�a kieliszek.", 1000);

    add_food(({"jajecznica z serem","jajecznica"}), 0, "jajecznicy z ��tym "+
       "serem", 450, 45, 8,"talerz", "jajecznicy", "Jest to w miar� czysty, "+
       "lekko obt�uczony talerz.", 200);
    add_food("ry�", ({ "ciemny", "ciemni" }), "ry�u", 500, 45, 13,
        "miska", "ry�u", "Jest to zwyk�a, drewniana miska.", 200);
    add_food("bigos", 0, "bigosu", 500, 45, 6,
       "talerz", "bigosu", "Jest to w miar� czysty, lekko obt�uczony talerz.", 200);
    add_food(({"sa�atka owocowa","sa�atka"}), 0, "sa�atki owocowej", 400, 45, 14,
        "miseczka", "sa�atki", "Jest to zwyk�a miseczka.", 200);
    add_food(({"zupa pomidorowa","zupa"}), 0, "zupy pomidorowej", 770, 45, 12,
        "miska", "zupy", "Jest to zwyk�a, drewniana miska.", 200);

    add_wynos_food("kie�basa", ({ ({ "ja�owcowy" }), ({ "ja�owcowi" }) }),
        300, 5, "Zrobiona z jakiego� biednego zwierzaka.\n", 2400, ({ "zepsuty", "zepsuci" }));
    add_wynos_food("chleb", ({ ({"�ytni"}), ({"�ytni"}) }), 800, 6, "Bochenek chleba.\n",
        2500, ({ "sple�nia�y", "sple�niali" }));

}

public string
exits_description()
{
    return "Wyj�cie z karczmy prowadzi na ulic�, na p�nocy wida� kuchni�"+
           ", za� na g�rze korytarz karczemny.\n";
}

string
opisik_pochodni()
{
    string str;
    if(dzien_noc())
        str="Nieliczne pochodnie wetkni�te tu� pod sufitem rzucaj� blade "+
            "�wiat�o na okolic�.";
    else
        str="Nieliczne pochodnie wisz� tu� pod sufitem na ka�dej ze �cian.";

    str+="\n";
    return str;
}

string
query_opis_kotary()
{
    if (present("kotara", TO)->query_open())
        return "odsuni^et^a";

    return "zasuni^et^a";
}

string
query_opis_drzwiczek()
{
    if (present("drzwiczki")->query_open())
        return "otwarte";

    return "zamkni^ete";
}
string
query_opis_kominka()
{
    if(present("kominek", TO)->query_pali_sie())
      return "w kt�rym j�zyki p�omieni skacz� figlarnie, ";
    else
      return "";
}

int
wyjdz_na_zewnatrz()
{
    write("Pod^a^zasz schodami na pi^etro.\n");
    return 0;
}


void
init()
{
    ::init();
    init_pub();
    init_peek();
}


string dlugi_opis()
{
    string str;
    int i, il, il_stolow;
    object *inwentarz;

    inwentarz = all_inventory();
    il_stolow = 0;
    il = sizeof(inwentarz);

    for (i = 0; i < il; ++i)
        if (inwentarz[i]->jestem_stolem_karczmy())
            ++il_stolow;

    if(dzien_noc())
    {
        str = "O�wietlenie daj� pochodnie umieszczone na �cianach oraz �wiat�a lamp "+
            "ulicznych przedzieraj�cych si� przez du�e okna z lekkimi, nieco "+
            "po��k�ymi od dymu unosz�cego si� w powietrzu firankami";
    }
    else
    {
        str = "Promienie s�oneczne przedostaj� si� tutaj przez du�e okna z lekkimi, "+
            "nieco po��k�ymi od dymu unosz�cego si� w powietrzu firankami "+
            "doskonale o�wietlaj�c ka�dy k�t";
    }

    if(sizeof(FILTER_LIVE(all_inventory()))>=13)
    {
        str += ". Du�� sal� karczmy wype�niaj� najr�niejsi go�cie. T�ok sk�adaj�cy si� ze "+
            "sta�ych bywalc�w, a tak�e z przypadkowych go�ci utrudnia rozejrzenie si� "+
           "po pomieszczeniu. ";
    }
    else if(sizeof(FILTER_LIVE(all_inventory()))>=5)
    {
        if(dzien_noc())
            str+=". ";
        else
            str += " tej du�ej sali. ";
    }
    else if((sizeof(FILTER_LIVE(all_inventory()))<=4 &&
        jest_rzecz_w_sublokacji(0, "Damir")) ||
        (sizeof(FILTER_LIVE(all_inventory()))<=4 &&
        jest_rzecz_w_sublokacji(0, "pulchny spocony m�czyzna")))
    {
        str += ". Du�a sala karczmy �wieci pustkami, jednak mimo tego karczmarz jak "+
            "zwykle jest czym� poch�oni�ty. ";
    }
    else
        str += ". Du�a sala karczmy �wieci pustkami, nawet gospodarza nie ma za barem. ";

    if (il_stolow == 0)
    {
        if (jest_rzecz_w_sublokacji(0, "Damir") ||
                jest_rzecz_w_sublokacji(0, "pulchny spocony m�czyzna"))
        {
            str += "O dziwo, nie ma tu ustawionych �adnych sto��w dla go�ci. "+
                "Pr�cz kilku sto�k�w niedaleko karczmarza, nie ma tu gdzie usi���. ";
        }
        else
        {
            str += "O dziwo, nie ma tu ustawionych �adnych sto��w dla go�ci. "+
                "Pr�cz kilku sto�k�w przy barze, nie ma tu gdzie usi���. ";
        }
    }
    else if (il_stolow == 1)
    {
        if (jest_rzecz_w_sublokacji(0, "Damir") ||
                jest_rzecz_w_sublokacji(0, "pulchny spocony m�czyzna"))
        {
            str += "Dla go�ci ustawiono jeden prostok�tny st�, "+
                "a maj�c na uwadze samotnych bywalc�w przygotowano kilka miejsc "+
                "niedaleko karczmarza. ";
        }
        else
        {
            str += "Dla go�ci ustawiono jeden prostok�tny st�, a maj�c na uwadze "+
                "samotnych bywalc�w przygotowano kilka miejsc przy barze. ";
        }
    }
    else
    {
        if (jest_rzecz_w_sublokacji(0, "Damir") ||
                jest_rzecz_w_sublokacji(0, "pulchny spocony m�czyzna"))
        {
            str += "Dla go�ci ustawiono dwa prostok�tne sto�y, a maj�c na uwadze "+
                "samotnych bywalc�w przygotowano kilka miejsc "+
                "niedaleko karczmarza. ";
        }
        else
        {
            str += "Dla go�ci ustawiono dwa prostok�tne sto�y, a maj�c na uwadze "+
                "samotnych bywalc�w przygotowano kilka miejsc przy barze. ";
        }
    }

    str += "Na wysokiej, wykonanej z mahoniowego drewna ladzie stoj� trzy grube "+
            "belki podpieraj�ce sufit. Jedna z nich zaopatrzona jest w spor� "+
            "tabliczk�, s�u��c� za menu. ";

    if(jest_rzecz_w_sublokacji(0,"niewielki kominek"))
    {
        str += "Tu� obok wi�kszego sto�u, przy �cianie stoi niewielki "+
            "kominek, "+query_opis_kominka()+
            "a zaraz za nim pn� si� na pi�tro w�skie, proste schody. ";
    }
    else
    {
        str+="Natomiast z lewej strony pn� si� na pi�tro w�skie, proste schody. ";
    }

    str+="Z ka�dej strony s� tutaj przej�cia do innych pomieszcze�. "+
        "Pod schodami dostrzegasz niewielkie "+query_opis_drzwiczek()+
        " drzwiczki, za� z przeciwnej strony "+query_opis_kotary()+" w tej "+
        "chwili kotar�"+
        //, za kt�r� prowadzi przej�cie do s�siedniego "+
        //"pomieszczenia.
        ". Za barem, w p�nocnej �cianie dostrzegasz tak�e wej�cie, zapewne "+
        "do kuchni, a w �cianie po�udniowej mi�dzy oknami s� tak�e "+
        "masywne drzwi prowadz�ce na zewn�trz.";

   str += "\n";
   return str;
}

public void
enter_inv(object ob, object from)
{
    ::enter_inv(ob, from);

    if (interactive(ob) && !ob->query_wiz_level())
        ob->set_default_start_location("/d/Standard/Redania/Rinde/Polnoc/Karczma/lokacje/start.c");
}
