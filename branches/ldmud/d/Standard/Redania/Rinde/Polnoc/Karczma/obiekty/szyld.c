/* Szyld karczmy Baszta.
   Lil 08.10.2005               */

inherit "/std/object.c";

#include "dir.h"
#include <pl.h>
#include <stdproperties.h>
#include <macros.h>
#include <materialy.h>


create_object()
{
    ustaw_nazwe("szyld");
    dodaj_przym("skromny","skromni");
    set_long("Unosz�cy si� nad drzwiami skromny, lekki szyld "+
             "porusza si� co jaki� czas przy nawet najl�ejszym "+
	     "podmuchu wiatru. Na szcz�cie nie skrzypi, jak to "+
	     "maj� w zwyczaju wszystkie takowe szyldy, cho� jest "+
	     "uczepiony belki �a�cuchami. Na jego powierzchni, "+
	     "bez �adnych ozd�b napisano najzwyklejsz� w �wiecie "+
	     "czcionk�, du�ymi literami nazw� karczmy: BASZTA.\n");

    ustaw_material(MATERIALY_DR_SOSNA);
    add_prop(OBJ_I_WEIGHT, 985);
    add_prop(OBJ_I_VOLUME, 1091);
    add_prop(OBJ_I_VALUE, 3);
    add_prop(OBJ_I_NO_GET, "Szyld dynda sobie nad twoj� g�ow�. Raczej go nie si�gniesz.\n");
    add_prop(OBJ_I_DONT_GLANCE,1);


}
