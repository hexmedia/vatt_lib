inherit "/std/door";

#include <pl.h>
#include "dir.h"

void
create_door()
{
    ustaw_nazwe( ({"drzwiczki", "drzwiczek","drzwiczkom",
                "drzwiczki","drzwiczkami","drzwiczkach"}), PL_NIJAKI_OS);

    dodaj_przym("ma�y", "mali");
    dodaj_przym("niski", "niscy");
    set_other_room(KARCZMA_LOKACJE + "sala.c");
    set_door_id("drzwiczki_na_podworze");
    set_door_desc("Drewniane drzwiczki, do^s^c niskie i ma^le.\n");
    set_open_desc("");
    set_closed_desc("");
    set_pass_command("drzwiczki", "przez drzwiczki do karczmy", "z podw�rza");

    set_fail_pass("Drzwiczki s^a zamkni^ete.\n");

    set_open_mess("otwiera drzwiczki do karczmy.\n",
        "Otwierasz niskie ma�e drzwiczki.\n",
        "Kto� otwiera drzwiczki z drugiej strony.\n");

    set_fail_open("Drzwiczki s^a ju^z otwarte.\n");

    set_close_mess("zamyka drzwiczki prowadz^ace do karczmy.\n",
        "Zamykasz drzwiczki.\n",
        "Kto^s zamyka drzwiczki z drugiej strony.\n");

    set_fail_close("Drzwiczki s^a ju^z zamkni^ete.\n");
}
