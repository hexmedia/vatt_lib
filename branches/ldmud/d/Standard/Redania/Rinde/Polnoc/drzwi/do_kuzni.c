
/*
 * Drzwi do kuzni w Rinde
 * Wykonane przez Avarda, dnia 04.06.06
 */

inherit "/std/door";

#include <pl.h>
#include "dir.h"
#include <stdproperties.h>

void
create_door()
{
    ustaw_nazwe("drzwi");
    dodaj_przym("pot^e^zny","pot^e^zni");

    set_other_room(KUZNIA_LOKACJE +"kuznia.c");
    set_door_id("DRZWI_DO_KUZNI_W_RINDE");
    set_door_desc("Pot^e^zne drzwi wykonane z mocnego d^ebu i dodatkowo "+
        "okute ^zelazem nie zniszcz^a si^e zbyt ^latwo. Jest na nich "+
        "zawieszona drewniana tabliczka. \n");

    set_open_desc("");
    set_closed_desc("");
    //set_pass_mess("przez pot^e^zne drzwi do ku^xni");
    set_pass_command("ku�nia","przez pot�ne drzwi do ku�ni","z zewn�trz");

    set_key("KLUCZ_DRZWI_DO_KUZNI");
    set_lock_name(({"zamek", "zamku", "zamkowi", "zamek", "zamkiem", "zamku"}), 0, PL_MESKI_NOS_NZYW);
    add_prop(DOOR_I_HEIGHT, 240);
}
