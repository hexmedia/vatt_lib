/* Autor: Sed
  Opis : Sed
  Data : 11.07.08 */

inherit "/std/window";

#include <pl.h>
#include <macros.h>
#include <formulas.h>

#include "dir.h"

void
create_window()
{
   ustaw_nazwe("okno");
   dodaj_przym("du^zy", "duzi");

   set_other_room(POLNOC + "Karczma/lokacje/sala");
  set_window_id("OKNO_W_KARCZMIE_W_POLNOCNEJ_CZESCI_RINDE_WYCHODZACE_NA_PLAC4");
   set_open(0);
   set_locked(0);
   set_open_desc("");
   set_closed_desc("");
   set_window_desc("Du^za drewniana rama, z dwoma szklanymi skrzyd^lami, "+
       "przez kto^re dobrze widoczne jest niemalz^e cal^e gl^o^wne "+
       "pomieszczenie karczmy. \n");

   ustaw_pietro(0);

   set_pass_command("okno");
   set_pass_mess("przez duz^e okno do karczmy");
}