
/* Tabliczka, made by Avard 28.05.06 */
inherit "/std/object.c";

#include "dir.h"
#include <pl.h>
#include <stdproperties.h>
#include <macros.h>
#include <filter_funs.h>
#include <materialy.h>
#include <object_types.h>

create_object()
{
    ustaw_nazwe("tabliczka");
    dodaj_przym("niewielki","niewielcy");
    dodaj_przym("drewniany","drewniani");


    set_long("Niewielka, przybita do drzwi tabliczka wykonana z bukowego "+
        "drewna. Jest na niej co^s napisane.\n");
    add_item("napis na", "\"Zak^lad fryzjerski serdecznie zaprasza o "+
        "porach dziennych wszystkich klient^ow. Zapewniamy profesjonalne "+
	     "strzy^zenie, golenie oraz wiele innych sposob^ow na "+
	     "uczynienie ci^e pi^eknym!\" \n", PL_MIE);
    add_cmd_item(({"napis na tabliczce","tabliczke"}), "przeczytaj",
        "\"Zak^lad fryzjerski serdecznie zaprasza o "+
        "porach dziennych wszystkich klient^ow. Zapewniamy profesjonalne "+
        "strzy^zenie, golenie oraz wiele innych sposob^ow na "+
        "uczynienie ci^e pi^eknym!\"\n");
    add_prop(OBJ_I_WEIGHT, 500);
    add_prop(OBJ_I_VOLUME, 1000);
    add_prop(OBJ_I_VALUE, 50);
    add_prop(OBJ_M_NO_SELL, 1);
    add_prop(OBJ_I_NO_GET, "Niestety tabliczka jest zbyt mocno "+
        "przytwierdzona do drzwi aby� "+
        "mog�"+this_player()->koncowka("","a")+
        "ja wzi^a^c.\n");
    ustaw_material(MATERIALY_DR_BUK);
    set_type(O_SCIENNE);
}
