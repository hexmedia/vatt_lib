inherit "/std/pattern";

#include <pattern.h>
#include "dir.h"

public void
create()
{
    set_pattern_domain(PATTERN_DOMAIN_BLACKSMITH);
    set_item_filename(RINDE_BRONIE + "mloty/poreczny_zelazny");
    set_estimated_price(2500);
    set_time_to_complete(7200);
    //enable_want_sizes();
}
