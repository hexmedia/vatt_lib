inherit "/std/pattern";

#include <pattern.h>
#include "dir.h"

public void
create()
{
    set_pattern_domain(PATTERN_DOMAIN_BLACKSMITH);
    set_item_filename(RINDE_BRONIE + "miecze/masywny_stalowy");
    set_estimated_price(3300);
    set_time_to_complete(7200);
    //enable_want_sizes();
}
