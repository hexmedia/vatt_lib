inherit "/std/door";

#include <pl.h>
#include "dir.h"
#include <materialy.h>
#include <object_types.h>

void
create_door()
{
    ustaw_nazwe( ({ "drzwi", "drzwi", "drzwiom", "drzwi", "drzwiami",
        "drzwiach" }), PL_NIJAKI_OS);

    dodaj_przym("wzmacniany", "wzmacniani");

    set_other_room(GARNIZON_LOKACJE + "piwnica-hall.c");

    set_door_id(KOD_DRZWI_P_W1);

    set_door_desc("Wzmacniane stalowymi sztabami, drewniane drzwi zbite z d�bowych, grubych desek. " +
                  "Na ca�ej ich p�aszczy�nie poumieszczane s� du�e nity, po��czone szerokimi " +
                  "metalowymi komponentami.\n");

    set_open_desc("We wschodniej �cianie widniej� otwarte wzmacniane drzwi.\n");
    set_closed_desc("We wschodniej �cianie widniej� zamkni�te wzmacniane drzwi.\n");

    set_pass_command("wsch�d","przez wzmacniane drzwi na wsch�d", "przez wzmacniane drzwi");
    //set_pass_mess("przez wzmacniane drzwi na wsch�d");

    set_key(KOD_KLUCZA_P_W1);
    set_lock_name(({"zamek", "zamku", "zamkowi", "zamek", "zamkiem", "zamku"}), 0, PL_MESKI_NOS_NZYW);

    ustaw_material(MATERIALY_STAL, 60);
    ustaw_material(MATERIALY_DR_DAB, 40);
    set_type(O_INNE);
}
