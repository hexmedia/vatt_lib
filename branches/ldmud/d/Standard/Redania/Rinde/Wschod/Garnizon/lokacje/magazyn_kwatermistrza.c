#include "dir.h"
#include <stdproperties.h>

inherit RINDE_STD;
inherit "/lib/store_support";
void create_rinde()
{
    set_short("Magazyn kwatermistrza");
    set_long("Jeste� w kieszeni bez dna.\n");

    add_prop(ROOM_I_INSIDE,1);

}

void                       /* wywolywane za kazdym razem, jak jakis */
enter_inv(object ob, object skad)  /* obiekt wchodzi do wnetrza tego obiektu */
{                  /* ...czyli do pokoju */

    ::enter_inv(ob, skad); /* trzeba ZAWSZE wywolac gdy redefiniujemy enter_inv */
    
    store_update(ob); /* dba o to, zeby magazynie nie bylo za duzo rzeczy */
}
void
leave_inv(object ob, object to)
{
    //::leave_env(ob,to);
    store_add_item(ob);
}