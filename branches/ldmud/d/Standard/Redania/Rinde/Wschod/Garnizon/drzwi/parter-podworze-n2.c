inherit "/std/door";

#include <pl.h>
#include "dir.h"
#include <materialy.h>
#include <object_types.h>

void
create_door()
{
  ustaw_nazwe("wrota");

  dodaj_przym("drewniany", "drewniani");

  set_other_room(GARNIZON_LOKACJE + "parter-n2.c");

  set_door_id(KOD_DRZWI_P_NP);

  set_door_desc("Grube, drewniane dechy zbite s� niezwykle r�wno. Przytwierdzone du�ymi gwo�dziami " +
      "szerokie belki dodatkowo wzmacniaj� jeszcze te solidne wrota. " +
      "Ca�o�� wisi na trzech ogromnych, metalowych zawiasach.\n");

  set_open_desc("");
  set_closed_desc("");

  set_pass_command("p�noc", "przez drewniane drzwi do p�nocnego skrzyd�a garnizonu",
    "zza drewnianych drzwi, z p�nocnego skrzyd�a garnizonu");
//   set_pass_mess("przez drewniane drzwi do p�nocnego skrzyd�a garnizonu");

  set_key(KOD_KLUCZA_P_NP);
  set_lock_name(({"zamek", "zamku", "zamkowi", "zamek", "zamkiem", "zamku"}), 0, PL_MESKI_NOS_NZYW);

  ustaw_material(MATERIALY_STAL, 5);
  ustaw_material(MATERIALY_DR_DAB, 95);
  set_type(O_INNE);
  set_open(0);
  set_locked(1);
}

public int
is_drzwi_parter_podworze_n2()
{
    return 1;
}
