#include "../dir.h"

/* drzwi mi�dzy przedsionkiem a parter-hall */
#define KOD_KLUCZA_P_D      "_gr_kod_klucza_p_d"
#define KOD_DRZWI_P_D       "_gr_kod_drzwi_p_d"

/* drzwi mi�dzy przedsionkiem a wyj�ciem */
#define KOD_KLUCZA_P_W      "_gr_kod_klucza_p_w"
#define KOD_DRZWI_P_W       "_gr_kod_drzwi_p_w"

/* drzwi mi�dzy parter-hall a n1 */
#define KOD_KLUCZA_P_G      "_gr_kod_klucza_p_g"
#define KOD_DRZWI_P_G       "_gr_kod_drzwi_p_g"

/* drzwi mi�dzy dy�urk� a n1 */
#define KOD_KLUCZA_D_N      "_gr_kod_klucza_d_n"
#define KOD_DRZWI_D_N       "_gr_kod_drzwi_d_n"

/* drzwi mi�dzy zbrojowni� a n1 */
#define KOD_KLUCZA_Z_N      "_gr_kod_klucza_z_n"
#define KOD_DRZWI_Z_N       "_gr_kod_drzwi_z_n"

/* drzwi mi�dzy kuchni� a n2 */
#define KOD_KLUCZA_K_N      "_gr_kod_klucza_k_n"
#define KOD_DRZWI_K_N       "_gr_kod_drzwi_k_n"

/* drzwi mi�dzy sal� rozpraw a s2 */
#define KOD_KLUCZA_S_S      "_gr_kod_klucza_s_s"
#define KOD_DRZWI_S_S       "_gr_kod_drzwi_s_s"

/* drzwi mi�dzy sekretariatem a s1 */
#define KOD_KLUCZA_Se_S     "_gr_kod_klucza_se_s"
#define KOD_DRZWI_Se_S      "_gr_kod_drzwi_se_s"

/* drzwi mi�dzy sal� stra�nik�w a hallem na pi�trze */
#define KOD_KLUCZA_S_H     "_gr_kod_klucza_s_h"
#define KOD_DRZWI_S_H      "_gr_kod_drzwi_s_h"

/* drzwi mi�dzy p1 a e1 na pi�trze */
#define KOD_KLUCZA_K_P     "_gr_kod_klucza_k_p"
#define KOD_DRZWI_K_P      "_gr_kod_drzwi_k_p"

/* drzwi mi�dzy p2 a e2 na pi�trze */
#define KOD_KLUCZA_K_Pb     "_gr_kod_klucza_k_pb"
#define KOD_DRZWI_K_Pb      "_gr_kod_drzwi_k_pb"

/* drzwi mi�dzy hallem a w1 w piwnicach */
#define KOD_KLUCZA_P_W1     "_gr_kod_klucza_p_w1"
#define KOD_DRZWI_P_W1      "_gr_kod_drzwi_p_w1"

/* drzwi mi�dzy c1 a w2 w piwnicach */
#define KOD_KLUCZA_P_C1     "_gr_kod_klucza_p_c1"
#define KOD_DRZWI_P_C1      "_gr_kod_drzwi_p_c1"

/* drzwi mi�dzy c2 a w2 w piwnicach */
#define KOD_KLUCZA_P_C2     "_gr_kod_klucza_p_c2"
#define KOD_DRZWI_P_C2      "_gr_kod_drzwi_p_c2"

/* drzwi mi�dzy c3 a w1 w piwnicach */
#define KOD_KLUCZA_P_C3     "_gr_kod_klucza_p_c3"
#define KOD_DRZWI_P_C3      "_gr_kod_drzwi_p_c3"

/* drzwi mi�dzy hallem a magazynem w piwnicach */
#define KOD_KLUCZA_P_M     "_gr_kod_klucza_p_m"
#define KOD_DRZWI_P_M      "_gr_kod_drzwi_p_m"

/* drzwi mi�dzy hallem a spi�arni� w piwnicach */
#define KOD_KLUCZA_P_S     "_gr_kod_klucza_p_s"
#define KOD_DRZWI_P_S      "_gr_kod_drzwi_p_s"

/* drzwi mi�dzy n2 a podw�rzem */
#define KOD_KLUCZA_P_NP     "_gr_kod_klucza_p_np"
#define KOD_DRZWI_P_NP      "_gr_kod_drzwi_p_np"

/* drzwi mi�dzy s2 a podw�rzem */
#define KOD_KLUCZA_P_SP     "_gr_kod_klucza_p_sp"
#define KOD_DRZWI_P_SP      "_gr_kod_drzwi_p_sp"
