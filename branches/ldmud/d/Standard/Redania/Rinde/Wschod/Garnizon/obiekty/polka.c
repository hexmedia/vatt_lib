#pragma strict_types

inherit "/std/container";

#include <pl.h>
#include "dir.h"
#include <macros.h>
#include <filter_funs.h>
#include <stdproperties.h>
#include <composite.h>
#include <cmdparse.h>
#include "/sys/materialy.h"
#include "/sys/object_types.h"

nomask void
create_container()
{
    ustaw_nazwe("p�ka");

    dodaj_przym("marmurowy", "marmurowi");
    dodaj_przym("powycierany", "powycierani");

    set_long("Wykonana z jasnego marmuru, szeroka p�ka wmurowana jest w �cian� bezpo�rednio " +
        "pod otwart� przestrzeni� zakratowanego okienka.@@opis_sublokacji| Na p�ce " +
        "widzisz |na|.| P�ka jest pusta.||" + PL_BIE + "@@\n");

    add_prop(OBJ_I_NO_GET, "Nie da si� jej ruszy�. Jest zbyt solidnie wmurowana w �cian�.\n");
    add_prop(CONT_I_CANT_WLOZ_DO, 1);
    add_prop(CONT_I_DONT_SHOW_CONTENTS, 1);

    add_subloc("na");
    add_subloc_prop("na", CONT_I_MAX_WEIGHT, 2000);
    add_subloc_prop("na", CONT_I_MAX_VOLUME, 5000);

    ustaw_material(MATERIALY_MARMUR);
    set_type(O_INNE);
}
