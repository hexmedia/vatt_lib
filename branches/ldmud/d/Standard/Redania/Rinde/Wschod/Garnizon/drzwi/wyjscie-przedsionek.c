inherit "/std/door";

#include <pl.h>
#include "dir.h"
#include <materialy.h>
#include <object_types.h>

void
create_door()
{
    ustaw_nazwe(({"wrota", "wr�t", "wrotom", "wrota", "wrotami", "wrotach"}),
        PL_NIJAKI_OS);

    dodaj_przym("pot�ny", "pot�ni");

    set_other_room(GARNIZON_LOKACJE + "przedsionek.c");

    set_door_id(KOD_DRZWI_P_W);

    set_door_desc("Wykonane z grubych, d�bowych bali wrota wygl�daj� na " +
        "bardzo solidne. Ogromna zasuwa wraz z du�ym zamkiem pog��biaj� " +
        "jeszcze to wra�enie.\n");

    set_open_desc("");
    set_closed_desc("");

    set_pass_command("garnizon","przez pot�ne wrota do garnizonu",
                     "z ulicy");


    set_lock_mess("przekr�ca klucz w du�ym zamku, co spowodowa�o zasuni�cie ogromnej zasuwy.\n",
        "Przekr�casz klucz w du�ym zamku, co spowodowa�o zasuni�cie ogromnej zasuwy.\n",
        "S�yszysz szcz�k przesuwanej zasuwy.\n");

    set_unlock_mess("przekr�ca klucz w du�ym zamku, co spowodowa�o odsuni�cie ogromnej zasuwy.\n",
        "Przekr�casz klucz w du�ym zamku, co spowodowa�o odsuni�cie ogromnej zasuwy.\n",
        "S�yszysz szcz�k przesuwanej zasuwy.\n");

    set_key(KOD_KLUCZA_P_W);

    ustaw_material(MATERIALY_STAL, 5);
    ustaw_material(MATERIALY_DR_DAB, 95);
    set_type(O_INNE);
}
