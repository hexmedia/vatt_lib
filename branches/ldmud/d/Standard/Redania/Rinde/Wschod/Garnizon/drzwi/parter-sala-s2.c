inherit "/std/door";

#include <pl.h>
#include "dir.h"
#include <materialy.h>
#include <object_types.h>

void
create_door()
{
    ustaw_nazwe( ({ "drzwi", "drzwi", "drzwiom", "drzwi", "drzwiami",
        "drzwiach" }), PL_NIJAKI_OS);

    dodaj_przym("prosty", "pro�ci");

    set_other_room(GARNIZON_LOKACJE + "parter-s2.c");

    set_door_id(KOD_DRZWI_S_S);

    set_door_desc("Proste, drewniane drzwi zbite z d�bowych, grubych desek. Przyczepiona " +
                  "jest do nich tabliczka z napisem 'Wyj�cie'.\n");

    set_open_desc("W zachodniej �cianie widniej� otwarte drewniane drzwi.\n");
    set_closed_desc("W zachodniej �cianie widniej� zamkni�te drewniane drzwi.\n");

    set_pass_command(({"wyj�cie", "korytarz"}), "przez drewniane drzwi na korytarz", "z korytarza");
//     set_pass_mess("przez drewniane drzwi na korytarz");

    set_key(KOD_KLUCZA_S_S);
    set_lock_name(({"zamek", "zamku", "zamkowi", "zamek", "zamkiem", "zamku"}), 0, PL_MESKI_NOS_NZYW);

    ustaw_material(MATERIALY_STAL, 1);
    ustaw_material(MATERIALY_DR_DAB, 99);
    set_type(O_INNE);
}
