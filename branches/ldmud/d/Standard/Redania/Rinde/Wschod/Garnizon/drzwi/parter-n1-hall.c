inherit "/std/door";

#include <pl.h>
#include "dir.h"
#include <materialy.h>
#include <object_types.h>

void
create_door()
{
    ustaw_nazwe( ({ "drzwi", "drzwi", "drzwiom", "drzwi", "drzwiami",
        "drzwiach" }), PL_NIJAKI_OS);

    dodaj_przym("okratowany", "okratowani");

    set_other_room(GARNIZON_LOKACJE + "parter-hall.c");

    set_door_id(KOD_DRZWI_P_G);

    set_door_desc("Wykonane z grubych, metalowych pr�t�w drzwi wygl�daj� na " +
                  "bardzo solidne. Ogromna zasuwa wraz z du�ym zamkiem pog��biaj� " +
                  "jeszcze to wra�enie.\n");

    set_open_desc("W po�udniowy portal wprawione s� otwarte okratowane drzwi wykonane " +
                  "z grubych, metalowych pr�t�w.\n");
    set_closed_desc("W po�udniowy portal wprawione s� zamkni�te okratowane drzwi wykonane " +
                    "z grubych, metalowych pr�t�w.\n");

    set_pass_command("po�udnie", "przez okratowane drzwi na po�udnie", "z po�udnia, zza okratowanych drzwi");
//     set_pass_mess("przez okratowane drzwi na po�udnie");

    set_lock_mess("przekr�ca klucz w du�ym zamku, co spowodowa�o zasuni�cie ogromnej zasuwy.\n",
        "Przekr�casz klucz w du�ym zamku, co spowodowa�o zasuni�cie ogromnej zasuwy.\n",
        "S�yszysz szcz�k przesuwanej zasuwy.\n");

    set_unlock_mess("przekr�ca klucz w du�ym zamku, co spowodowa�o odsuni�cie ogromnej zasuwy.\n",
        "Przekr�casz klucz w du�ym zamku, co spowodowa�o odsuni�cie ogromnej zasuwy.\n",
        "S�yszysz szcz�k przesuwanej zasuwy.\n");

    set_key(KOD_KLUCZA_P_G);
    set_lock_name(({"zamek", "zamku", "zamkowi", "zamek", "zamkiem", "zamku"}), 0, PL_MESKI_NOS_NZYW);

    ustaw_material(MATERIALY_STAL);
    set_type(O_INNE);
}
