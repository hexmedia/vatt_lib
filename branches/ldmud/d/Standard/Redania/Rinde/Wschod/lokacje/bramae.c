#include <stdproperties.h>
#include "dir.h"

inherit STREET_STD;

void
create_street()
{
    set_short("Przy wschodniej bramie");

    add_exit("ulica5.c","zach^od");
    add_exit("aaa.c","wsch^od");

    add_object(RINDE+"przedmioty/lampa_uliczna");
    add_object(WSCHOD_DRZWI+"bramae.c");
}
public string
exits_description()
{
    return "Ulica prowadzi na zach^od, za^s na wschodzie znajduje si^e "+
           "brama miasta.\n";
}

string
dlugi_opis()
{
    string str;

    str = "Widoczna na wschodzie pot�na brama wyrasta dumnie po�rodku " +
	"kamiennego muru okalaj�cego miasto. Droga prowadz�ca zza " +
	"miasta przez bram� od razu przechodzi w szerok� ulic�, zapewne " +
	"jedn� z najwi�kszych i najwa�niejszych w mie�cie, kt�ra " +
	"prowadzi prosto na zach�d ko�o widocznego st�d ratusza na " +
	"rynku g��wnym, a� do �wi�tyni na zachodzie, przecinaj�c miasto " +
	"na p�. W tym miejscu dominuje lu�na zabudowa, na kt�r� " +
	"sk�adaj� si� przewa�nie drewniane, �redniej wielko�ci domy. " +
	"Bardziej na zach�d zabudowa przechodzi w typowo miejsk�, " +
	"sk�adaj�c� si� z wi�kszych dom�w i kamienic.\n";

    return str;
}
