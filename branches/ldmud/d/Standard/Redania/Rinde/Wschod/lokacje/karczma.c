/*
 * Przerobi� Vera.
 */
#include "dir.h"

inherit RINDE_STD;
inherit "/lib/pub_new";

#include <stdproperties.h>
#include <macros.h>
#include <sit.h>

void
create_rinde()
{
    set_short("Karczma \"Garnizonowa\"");
    set_long("@@dlugi_opis@@");

    add_item(({"pochodnie","wielkie pochodnie"}),
            "Wielkie pochodnie wisz� na �cianach nad sufitem, o�wietlaj�c "+
            "te zadymione pomieszczenie.\n");

    add_npc(RINDE_NPC + "karczmarz_garnizonowej");

    add_object(WSCHOD_DRZWI+"z_garnizonowej");

    add_prop(ROOM_I_INSIDE,1);
    add_object(MEBLE+"lawka_prosta_drewniana");
    add_object(MEBLE+"lawka_prosta_drewniana");
    add_object(WSCHOD_OBIEKTY+"cennik_karczmy");
    dodaj_rzecz_niewyswietlana("prosta drewniana �awka",2);
    dodaj_rzecz_niewyswietlana("wielki cennik",1);


    add_drink("piwo", 0, "piwa o kolorze bursztynowym", 650, 5, 15, "kufel", "piwa",
	"Jest to kufel wykonany z jakiego� ciemnego szk�a, lepi�cego si� "+
	"w d�oni.");
    add_drink("wino", ({ "cienki", "ciency" }), "cienkiego wina",
	310, 7, 16, "kubek", "wina",
	   "Jest to drewniany, niedomyty kubek.");
    add_drink("siepacz", 0, "czego� ��tawego, o kr�c�cym w nosie"+
           " zapachu",
	340, 40, 22, "kubek", "siepacza",
	 "Jest to drewniany, niedomyty kubek.");
    add_drink("w�dka", ({ "gorzki", "gorzcy" }), "gorzkiej w�dki", 50, 45, 20,
	"kieliszek", "w�dki",
	 "Niewielki, wykonany z ciemnego szk�a kieliszek.");

    add_food("baranina", 0, "przyprawionej czym� ciemnym baraniny", 550, 45, 90,
	"talerz", "baraniny", "Poobijany z kilku stron talerz, z "+
	 "zaciekami na bokach.", 200);
    add_food("gulasz", ({ "ciemny", "ciemni" }), "ciemnego gulaszu", 650, 45, 40,
	"miska", "gulaszu", "Jest to zwyk�a, drewniana miska.", 200);
    add_food("w�tr�bka", 0, "w�tr�bki", 650, 45, 35,
	"talerz", "w�tr�bki", "Poobijany z kilku stron talerz, z "+
	 "zaciekami na bokach.", 200);
}
public string
exits_description()
{
    return "Wyj^scie z karczmy prowadzi na ulice.\n";
}

void
init()
{
    ::init();
    init_pub();
}


string
dlugi_opis()
{
    string str;

    str = "Ruch w tym pomieszczeniu karczmy \"Garnizonowej\" jest znacznie " +
	"intensywniejszy ni� w innych gospodach. Wynika to prawdopodobnie " +
	"z tego i� jest to karczma dla plebsu i stra�nik�w b�d�ca " +
	"jedynie uzupe�nieniem ca�o�ci gosp�d w Rinde. Klient�w " +
	"obs�uguj� �redniego wieku kelnerki. Wyra�nie wyczuwasz tu mocn� " +
	"wo� taniego piwa i wymiocin klient�w. Ca�e pomieszczenie zosta�o " +
	"pomalowane na zielono. ";
    if(jest_rzecz_w_sublokacji(0,"prosta drewniana �awka"))
       str+="Bywalcy tego przybytku siedz� na " +
	"niewygodnych �awkach obok kt�rych zauwa�asz wielki cennik. ";
    else
       str+="O dziwo, miejsca na �awki s� puste, co wida� po fragmentach "+
            "czystej posadzki. Na �cianie dostrzegasz wielki cennik. ";

    str+="�r�d�o �wiat�a stanowi tu kilka wielkich pochodni zaczepionych " +
	"przy �cianach. ";
    if(jest_rzecz_w_sublokacji(0,"prosta drewniana �awka"))
       str+="Pod �awkami spostrzegasz mn�stwo szk�a po " +
	"rozbitych butelkach i kuflach.";


    str+="\n";
    return str;

}

void wypad(object kogo)
{

    kogo->remove_prop(SIT_SIEDZACY);
    kogo->remove_prop(OBJ_I_DONT_GLANCE);
    tell_room(WSCHOD_LOKACJE + "ulica4", "Z karczmy w�a�nie "+
          "wyrzucono " + QCIMIE(kogo,PL_BIE) + ".\n");
    kogo->move_living("M",WSCHOD_LOKACJE+"ulica4",1,0);

    kogo->reduce_hp(0, 4000 + random(2000), 10);

    write("Zosta�"+kogo->koncowka("e�","a�")+" wyrzucon"+
        kogo->koncowka("y","a","e")+" z karczmy.\n");
}

void wywalamy(object kogo)
{
    object npc = present("gross", TO);

    if(kogo->query_race()~="p�elf")
    {
        npc->command("powiedz Patrzcie go! Kolejny mieszaniec.");
        npc->command("prychnij");
    }
    else if(kogo->query_race()=="elf")
    {
        npc->command("powiedz O�esz kurwa! Elf!");
        saybb("Karczmarz dos�ownie wykopuje "+QCIMIE(kogo,PL_BIE)+
            " na zewn�trz.");

        switch(random(3))
        {
           case 0: npc->command("krzyknij Kartk� baranie przeczytaj, ch�do�ony "+
                    "w dup� ma� elfie!"); break;
           case 1: npc->command("krzyknij Spierdalaj do lasu!"); break;
           case 2: npc->command("krzyknij I �ebym ci� tu wi�cej nie widzia�, "+
                   "bo w rzy� tak nakopi�, �e ci� w�asna kurwa jego ma� "+
                   "Le�na Pani nie rozpozna!"); break;
       }
       wypad(kogo);
    }
}

command_present(object ob)
{
    if (environment(ob) == this_object() &&
        !ob->query_prop(OBJ_I_INVIS)) //�eby nie zdradza� obecno�ci wiz�w;)
        return wywalamy(ob);

    return 0;
}


public void
enter_inv(object ob, object from)
{
    ::enter_inv(ob, from);
    if(jest_rzecz_w_sublokacji(0,"niski zgrabny m�czyzna"))
        set_alarm(3.0, 0.0,"command_present",ob);
}
