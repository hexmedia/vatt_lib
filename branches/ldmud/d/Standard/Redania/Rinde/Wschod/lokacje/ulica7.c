#include <stdproperties.h>
#include "dir.h"

inherit STREET_STD;

void
create_street()
{
    set_short("Uliczka przed szpitalem");
    set_long("W�ska uliczka prowadzi z p�nocy, od g��wnego placu, "
            + "na po�udnie. Tabliczka nad wej�ciem do stoj�cego osobno, niskiego "
            + "budynku za�wiadcza o tym, �e to w�a�nie tutaj znajduje si� miejski szpital.\n");

    add_item(({"napis","tabliczk�","szyld"}),"         Szpital\nimienia \"Cudem Ocalonego\"\n");
    add_item(({"szpital","budynek","budynek szpitala"}),"Pi�trowy budynek "
            + "szpitala swoje najlepsze czasy ma ju� za sob�. "
            + "Tynk ju� tylko gdzieniegdzie trzyma si� �ciany, ceg�y kruszej�, a co znaczniejsze "
            + "ubytki dach�wek za�atane s� s�om�.\n");

    add_exit("ulica6.c","p�noc");
    add_exit(POLUDNIE_LOKACJE + "ulica9.c", "po�udnie");
    add_object(SZPITAL_DRZWI + "drzwi_do_szpitala");

    add_prop(ROOM_I_INSIDE,0);
}

public string
exits_description()
{
    return "Ulica prowadzi na p^o^lnoc i po^ludnie, jest tu tak^ze wej^scie "+
        "do szpitala.\n";
}

