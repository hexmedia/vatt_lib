
#include "dir.h"
inherit RINDE_STD;

#include <stdproperties.h>

string
dlugi_opis();

void create_rinde()
{
    set_short("Sala zabiegowa.");
    set_long("@@dlugi_opis@@");

    add_object(SZPITAL_DRZWI + "drzwi_z_sali");
    add_object(MEBLER + "duzy_stalowy_stol");

    add_prop(ROOM_I_INSIDE,1);
    dodaj_rzecz_niewyswietlana("du^zy stalowy st^o^l");
}

public string
exits_description()
{
    return "Wyj^scie z sali zabiegowej prowadzi do poczekalni.\n";
}

string
dlugi_opis()
{
    string str = "";

    if (jest_rzecz_w_sublokacji(0, "du^zy stalowy st^o^l"))
        str += "Jedynym meblem w tym pomieszczeniu jest stoj�cy "
        + "dok�adnie po�rodku du�y stalowy st�. Gruba warstwa kurzu " +
        "pokrywa zar�wno jego powierzchni�, jak i drewnian� pod�og�.\n";
    else
        str += "Pokryte grub^a warstw^a kurzu pomieszczenie jest ca^lkowicie pozbawione wyposa^zenia.\n";

    return str;
}
