/* Autor: Avard
   Data : 03.02.06
   Info : Klucz do stajni Avarda i Faeve */

inherit "/std/key";

#include <stdproperties.h>
#include <pl.h>

void
create_key()
{
    ustaw_nazwe("klucz");
    dodaj_przym("niedu^zy", "nieduzi");
    dodaj_przym("stalowy", "stalowi");
    set_long("Zwyczajny, stalowy klucz o niewielkim rozmiarze.\n");
    
    set_key("KLUCZ_DRZWI_DO_SZPITALA");
}