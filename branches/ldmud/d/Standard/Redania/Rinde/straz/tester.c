inherit "/std/humanoid";

#include <macros.h>
#include <stdproperties.h>
#include <pl.h>

void
create_humanoid()
{
    ustaw_odmiane_rasy(PL_MEZCZYZNA);
    dodaj_przym("glupi", "glupi");
    dodaj_przym("testowy", "testowy");
    set_gender(G_MALE);
    add_object("/d/Aretuza/rantaur/ready/mlot");
}

void go()
{
    this_object()->command("s");
    this_object()->command("n");
}

void dob()
{
    this_object()->command("dobadz broni");
}

void op()
{
    this_object()->command("opusc bron");
}
