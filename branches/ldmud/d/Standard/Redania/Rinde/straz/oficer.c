inherit "/std/humanoid.c";
inherit "/lib/straz/straznik.c";

#include "dir.h"

#include <ss_types.h>
#include <macros.h>
#include <stdproperties.h>
#include <pl.h>

#define MIASTO "z Rinde"

#define HELM RINDE+"przedmioty/zbroje/helmy/lekki_oksydowany_M.c"
#define MIECZ RINDE+"przedmioty/bronie/miecze/dlugi_stalowy.c"
#define ZBRO RINDE+"przedmioty/zbroje/kolczugi/szara_matowa_M.c"
#define BUTY RINDE+"przedmioty/ubrania/buty/wysokie_skorzane_M.c"
#define SPOD RINDE+"przedmioty/zbroje/spodnie/wzmacniane_skorzane_M.c"

void
create_humanoid()
{
	ustaw_odmiane_rasy("oficer");
	dodaj_nazwy("cz這wiek");
	set_gender(G_MALE);

	random_przym("ros造:ro郵i przygarbiony:przygarbieni||m皻nooki:m皻noocy||造sawy:造sawi||"
			+"starszy:starszi||energiczny:energiczni", 2);

	set_long("Cz這wiek o poci庵貫j, nosz帷ej ju� 郵ady staro軼i twarzy rozgl康a"
		+" si� dooko豉 bystrym wzrokiem. Mimo, iz najlepsze lata ma ju� za sob�,"
		+" to wci捫 jest pe貫n krzepy o czym 鈍iadcz� jego gromkie okrzyki"
		+" strofuj帷e m這dych stra積ik闚. W przeciwie雟twie do reszty gwardzist闚,"
		+" ten wydaje si� mie� nieco wi璚ej og豉dy i kultury, cho� mo瞠 to tylko z逝dne"
		+" wra瞠nie spowodowane jego leciwym wiekiem...\n");

	add_prop(LIVE_I_NEVERKNOWN, 1);
	add_prop(CONT_I_WEIGHT, 75000);
	add_prop(CONT_I_HEIGHT, 175);
	add_prop(NPC_I_NO_FEAR,1);

	set_stats(({ 115, 120, 170, 150, 150}));
	set_spolecznosc("Rinde");
	add_weapon(MIECZ);
	add_armour(HELM);
	add_armour(ZBRO);
	add_armour(BUTY);
	add_armour(SPOD);

	set_attack_chance(100);
	set_aggressive(&check_living());

	set_act_time(20+random(4));
	add_act("emote rozgl康a sie po okolicy.");
	add_act("emote poprawia zbroj�.");
	add_act("emote odprowadza wzrokiem przechodz帷� nieopodal dziewk�.");
	add_act("mlasnij");
	add_act("oczko do drugiej kobiety");
	add_act("oczko do kobiety");
	add_act("obliz sie oblesnie");
	add_act("westchnij ciezko");
	add_act("':do siebie: Pogrucha豚ym z tamt� cizi�, hehe.");
	add_act("beknij");
	add_act("':cicho: Ysz kurwa. Co za los.");
	add_act("popatrz spode lba na elfa");
	add_act("powiedz Rusza� si� to wcze郾iej sko鎍zym.");
	add_act("':pod nosem: Ej瞠, mo瞠 kogo� ubijem w ciemnym zau趾u? Hehe, "+
		"zawsze to jaka� rozrywka, no nie?");

	set_cact_time(9+random(3));
	add_cact("powiedz Osz kurwa!");
	add_cact("krzyknij Bi�, nie 瘸這wa�, kurwa wasza ma�!");
	add_cact("powiedz Zabij na 鄉ier�!");
	add_cact("splun");
	add_cact("krzyknij Bijem siem kurwa!");

	add_ask(({"ratusz"}), VBFC_ME("pyt_o_ratusz"));
	add_ask(({"鈍i徠ynie"}), VBFC_ME("pyt_o_swiatynie"));
	add_ask(({"stra積ik闚","stra�"}), VBFC_ME("pyt_o_straz"));
	add_ask(({"garnizon"}), VBFC_ME("pyt_o_garnizon"));
	add_ask(({"burmistrza","Nevilla","nevilla"}),VBFC_ME("pyt_o_burmistrza"));
	set_default_answer(VBFC_ME("default_answer"));

    set_skill(SS_WEP_SWORD,78);
    set_skill(SS_UNARM_COMBAT, 50);
    set_skill(SS_PARRY, 66);
    set_skill(SS_DEFENCE,44);
    set_skill(SS_AWARENESS,64);
    set_skill(SS_BLIND_COMBAT,30);

	create_straznik();
}

void
hook_bron_do_odebrania()
{
	set_alarm(0.5, 0.0, "command_present", TP,
   	       	command("powiedz do "+TP->short(TO, PL_DOP) +
	       " Bro^n b^edzie do odebrania w sekretariacie w garnizonie."));
}

string
hook_okrzyk_do_walki(object wrog)
{
	string str;
	switch(random(6))
    {
        case 0: str="powiedz Lejem "+wrog->koncowka("go","j^a","to")+
                    ", kurwa wasza ma^c!"; break;
        case 1: str="krzyknij Do boju!"; break;
        case 2: str="krzyknij Do ataku!"; break;
        case 3: str="krzyknij Gi� "+
					wrog->koncowka("psie","suko","psie")+"!"; break;
		case 4: str="krzyknij"; break;
		case 5: str="powiedz Psia ma�!"; break;
    }
	return str;
}

void
hook_uciekl_podczas_walki(object kto)
{
	switch(random(6))
    {
        case 0: command("krzyknij Spierdoli�"+kto->koncowka("","a","o")+
				" nam! Za "+kto->koncowka("nim", "ni�")+"!"); break;
        case 1: command("krzyknij Za ni"+kto->koncowka("m","�","m")+
						"m psia kurwa ma�!"); break;
        case 2: command("krzyknij Uciek"+kto->koncowka("","a","o")+"! Zapierdala� "+
                                         "豉pserdaki kurwa jego!"); break;
		case 3: command("krzyknij Goni� "+kto->koncowka("go","j�","to")+"!"); break;
		case 4: command("krzyknij Dalej psie pomioty, goni� "+
					kto->koncowka("go","j�","to")+"!"); break;
		case 5: break;
        }
}

void
hook_ostrzegalem()
{
	command("powiedz Nosz kurwa wasza ma^c. Ostrzega^lem!");
}

void
hook_posluszny_gracz(object kto)
{
	switch(random(4))
    {
        case 0:
            command("powiedz No! Hehe, grzeczn"+kto->koncowka("y","a","e")+
                     " "+kto->koncowka("ch^lopiec","dziewczynka","co^s")+".");
            break;
        case 1:
            command("powiedz No! I tak ma by^c kurwa wasza ma^c.");
            break;
        case 2:
            command("powiedz No! Porz^adek musi by^c.");
            break;
        case 3:
            command("powiedz No! I 瞠by mi to by這 "+
            			"ostatni raz, bo zawi郾iesz na stryczku! He, he.");
            break;
    }
}

string
hook_opusc_bron(object kto)
{
	string str;
	switch(random(5))
    {
        case 0: str="Ej^ze! Opu^s^c bro^n! Natychmiast!"; break;
        case 1: str="Hola! Opu^s^c bro^n!"; break;
        case 2: str="Opu^s^c bro^n! I to ju^z!"; break;
        case 3: str="Opu^s^c bro^n do kurwy n^edzy!"; break;
        case 4: str="Hej, opu^scisz bro^n czy ja mam to za ciebie zrobi^c? He, he."; break;
    }
	return str;
}

string
hook_zlapalismy_zlodzieja(object player)
{
	string str;
	switch(random(4))
	{
		case 0: str="Mamy ci� gagatku!";break;
		case 1: str="To "+player->koncowka("ten","ta","to")+
				"!"; break;
		case 2: str="Ha, ju� nam si� nie wywiniesz z這dziej"+
				player->koncowka("u","ko","u")+"!"; break;
		case 3: str="R帷zki ci� swierzbi�, ma�"+
				player->koncowka("y","a","y")+" z這dziej"+
				player->koncowka("u","ko","u")+"? He he, "+
				"my co� na to zaradzimy."; break;
	}
	return str;
}

int
query_oficer()
{
    return 1;
}

string
query_magazyn()
{
	return "/d/Standard/Redania/Rinde/Wschod/Garnizon/lokacje/magazyn_kwatermistrza";
}

init_living()
{
  ::init_living();
  ::init_straznik();
}

void powiedz_gdy_jest(object player, string tekst)
{
	if (environment(this_object()) == environment(player))
        this_object()->command(tekst);
}

string pyt_o_ratusz()
{
        set_alarm(0.5, 0.0, "powiedz_gdy_jest", TP,
            "powiedz do "+ TP->query_name(PL_DOP) +" Jak mo^zna "+
            "nie wiedzie^c "+
            "pan"+TP->koncowka("ie","i")+" gdzie ratusz stoi! "+
            "Przecie to samo centrum miasta z t^a ceglan^a wie^z^a ino.");
        return "";
}

string pyt_o_swiatynie()
{
        set_alarm(0.5, 0.0, "powiedz_gdy_jest", TP,
            "powiedz do "+ OB_NAME(TP) +
            " Pan"+TP->koncowka("ie","i")+" widzi "+
            "pan"+TP->koncowka("","i")+" ino tamt^a bia^l^a "+
            "wie^ze. To ino tam.");
        return "";
}

string pyt_o_straz()
{
        set_alarm(0.5, 0.0, "powiedz_gdy_jest", TP,
            "powiedz do "+ TP->query_name(PL_DOP) +" Tak, to ja!");
        return "";
}

string pyt_o_garnizon()
{
        set_alarm(0.5, 0.0, "powiedz_gdy_jest", TP,
            "powiedz do "+ TP->query_name(PL_DOP) +
            " Garnizon pan"+TP->koncowka("ie","i")+" to "+
            "ten du^zy kamienny budynek na ws... wsch... Tam gdzie s^lo^nce "+
            "si^e pojawia!");
        return "";
}

string pyt_o_burmistrza()
{
        set_alarm(0.5, 0.0, "powiedz_gdy_jest", TP,
            "powiedz do "+ TP->query_name(PL_DOP) +" Neville "+
            "jest naszym burmistrzem!");
        return "";
}

string default_answer()
{
        set_alarm(2.0, 0.0, "powiedz_gdy_jest", TP,
        "powiedz Nie zawracaj pan"+TP->koncowka("ie","i")+" dupy.");
        return "";
}

void emote_hook(string emote, object wykonujacy, string przyslowek)
{
	switch (emote)
	{
		case "klepnij":
		case "poca逝j":
		case "pog豉szcz":
		case "po豉skocz":
		case "przytul":
		{
			if(wykonujacy->query_gender())
				set_alarm(1.0, 0.0, "kobieta_dobre", wykonujacy);
			else
				set_alarm(1.0, 0.0, "facet_zle", wykonujacy);
			break;
		}
		case "kopnij":
		case "opluj":
		case "nadepnij":
		{
			if(wykonujacy->query_gender())
				set_alarm(1.0, 0.0, "kobieta_zle", wykonujacy);
			else
				set_alarm(1.0, 0.0, "facet_zle", wykonujacy);
			break;
		}
		case "szturchnij":
		{
			set_alarm(1.0, 0.0, "powiedz_gdy_jest", TP,
                        "powiedz do "+wykonujacy->query_name(PL_DOP)
				     +" Taaa?");
			break;
		}
	}
}

void kobieta_dobre(object wykonujacy)
{
	string rasa = wykonujacy->query_race();
	if(rasa ~= "krasnolud" || rasa ~= "gnom" || rasa ~= "nizio貫k")
	{
		set_alarm(1.0, 0.0, "powiedz_gdy_jest", TP,
                        "powiedz do "+wykonujacy->query_name(PL_DOP)+
			" Won pokurczu, albo ci 貫b ukr璚e!");
		command("kopnij "+wykonujacy->query_real_name(3));
	}
	else
	{
		command("klepnij "+wykonujacy->query_real_name(3)+" bezczelnie w po郵adek");
		set_alarm(1.0, 0.0, "powiedz_gdy_jest", TP,
                        "powiedz do "+wykonujacy->query_name(PL_DOP)
			+" Mo瞠 zajrzysz do mnie po robocie?");
		command("usmiechnij sie oblesnie");
	}
}

void kobieta_zle(object wykonujacy)
{
	string rasa = wykonujacy->query_race();
	if(rasa ~= "krasnolud" || rasa ~= "gnom" || rasa ~= "nizio貫k")
	{
		set_alarm(1.0, 0.0, "powiedz_gdy_jest", TP,
                        "powiedz do "+wykonujacy->query_name(PL_DOP)+
			" Won pokurczu, albo ci 貫b ukr璚e!");
		command("kopnij "+wykonujacy->query_real_name(3));
	}
	else
	{
		set_alarm(1.0, 0.0, "powiedz_gdy_jest", TP,
                        "powiedz do "+wykonujacy->query_name(PL_DOP)+
				" Te, male鎥a! Nie podskakuj!");
		command("spojrzyj z usmieszkiem na "+wykonujacy->query_real_name(3));
	}
}

void facet_zle(object wykonujacy)
{
	int  i = random(2);

	switch (i)
	{
		case 0:
		{
			//command("powiedz do "+OB_NAME(wykonujacy)+
                        set_alarm(1.0, 0.0, "powiedz_gdy_jest", TP,
                        "powiedz do "+wykonujacy->query_name(PL_DOP)+
                        " Spieprzaj, psie nasienie, albo ci� w豉sna matka "+
                        "nie pozna!");
			command("spojrzyj beznamietnie na "+OB_NAME(wykonujacy));
			break;
		}

		case 1:
		{
			set_alarm(1.0, 0.0, "powiedz_gdy_jest", TP,
                        "powiedz do "+wykonujacy->query_name(PL_DOP)+
				" Spr鏏uj tego jeszcze raz, to ci jaja rzyci� wyjd�.");
			command("kopnij "+OB_NAME(wykonujacy));
			break;
		}
	}
}

public string
query_auto_load()
{
    return 0;
}
