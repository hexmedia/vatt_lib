inherit "/std/humanoid.c";
inherit "/lib/straz/straznik.c";

#include "dir.h"

#include <macros.h>
#include <pl.h>
#include <ss_types.h>
#include <stdproperties.h>

#define MIASTO "z Rinde"

#define HELM RINDE+"przedmioty/zbroje/helmy/lekki_oksydowany_M.c"
#define MIECZ RINDE+"przedmioty/bronie/miecze/masywny_stalowy.c"
#define ZBRO RINDE+"przedmioty/zbroje/kolczugi/szara_matowa_M.c"
#define BUTY RINDE+"przedmioty/ubrania/buty/wysokie_skorzane_M.c"
#define SPOD RINDE+"przedmioty/zbroje/spodnie/wzmacniane_skorzane_M.c"

object admin=TO->query_admin();

void random_long();

void
create_humanoid()
{
    ustaw_odmiane_rasy("straznik");
    dodaj_nazwy("cz這wiek");
    set_gender(G_MALE);

    random_przym("znudzony:znudzeni energiczny:energiczni zblazowany:zblazowani"+
        " krzepki:krzepcy ospa造:ospali||muskularny:muskularni ros造:ro郵i"+
        " ciemnow這sy:ciemnow這si pryszczaty:pryszczaci kr鏒kow這sy:kr鏒kow這si", 2);

    random_long();
    set_spolecznosc("Rinde");
    add_prop(LIVE_I_NEVERKNOWN, 1);
    add_prop(CONT_I_WEIGHT, 75000);
    add_prop(CONT_I_HEIGHT, 178);
    add_prop(NPC_I_NO_FEAR, 1);

    add_weapon(MIECZ);
    add_armour(HELM);
    add_armour(ZBRO);
    add_armour(BUTY);
    add_armour(SPOD);

    set_stats(({100, 150,150, 150, 150}));

    set_attack_chance(100);
    set_aggressive(&check_living());

    set_act_time(20+random(9));
    add_act("emote rozgl康a si� szukaj帷 zaczepki.");
    add_act("oczko do kobiety");
    add_act("ziewnij");
    add_act("beknij");
    add_act("podrap sie po jajach");
    add_act("popatrz na elfke");
    add_act("popatrz spode lba na elfa");
    add_act("powiedz A mog貫m pos逝cha� matki, a nie ojca...");
    add_act("popatrz spode lba na krasnoluda");
    add_act("mlasnij");
    add_act("powiedz Eh, kiedy to ja ostatnio ch璠o篡貫m...?");
    add_act("':pod nosem: Pieprzony oficer, zachcia這 mu si� 豉zi� po mie軼ie.");
    add_act("skrzyw sie do oficera");
    add_act("pokiwaj niemrawo");
    add_act("':pod nosem: Zaraza. Mam chu� na jak捷 p馧elfk�.");
    add_act("powiedz W rzy� by wsadzi� taki patrol. Nic si� nie dzieje.");
    add_act("popatrz na kobiete");
    add_act("powiedz Ale nudy.");
    add_act("emote poprawia przypi皻y u boku miecz.");
    add_act("emote g這郾o zbiera flegm� i pluje na ziemi�.");

    set_cact_time(11+random(9));
    add_cact("krzyknij Wszystkie gnaty ci 鄉ieciu po豉miem!");
    add_cact("powiedz O tak! To lubi�!");
    add_cact("podskocz gwa速ownie");
    add_cact("powiedz No kompania! To si� kurna zabawimy!");
    add_cact("krzyknij Bij, zabij!");

    add_ask(({"ratusz"}), VBFC_ME("pyt_o_ratusz"));
    add_ask(({"鈍i徠ynie"}), VBFC_ME("pyt_o_swiatynie"));
    add_ask(({"stra積ik闚","stra�"}), VBFC_ME("pyt_o_straz"));
    add_ask(({"garnizon"}), VBFC_ME("pyt_o_garnizon"));
    add_ask(({"burmistrza","Nevilla","nevilla"}),VBFC_ME("pyt_o_burmistrza"));
    set_default_answer(VBFC_ME("default_answer"));

    set_skill(SS_WEP_SWORD,68);
    set_skill(SS_UNARM_COMBAT, 50);
    set_skill(SS_PARRY, 46);
    set_skill(SS_DEFENCE,24);
    set_skill(SS_AWARENESS,64);
    set_skill(SS_BLIND_COMBAT,20);

    create_straznik();
}

void powiedz_gdy_jest(object player, string tekst)
{
    if(environment(this_object()) == environment(player))
        this_object()->command(tekst);
}

string pyt_o_ratusz()
{
    set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
        "powiedz do "+ this_player()->query_name(PL_DOP) +" Jak mo^zna "+
        "nie wiedzie^c "+
        "pan"+this_player()->koncowka("ie","i")+" gdzie ratusz stoi! "+
        "Przecie to samo centrum miasta z t^a ceglan^a wie^z^a ino.");
    return "";
}

string pyt_o_swiatynie()
{
    set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
        "powiedz do "+ OB_NAME(this_player()) +
        " Pan"+this_player()->koncowka("ie","i")+" widzi "+
        "pan"+this_player()->koncowka("","i")+" ino tamt^a bia^l^a "+
        "wie^ze. To ino tam.");
    return "";
}

string pyt_o_straz()
{
    set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
        "powiedz do "+ this_player()->query_name(PL_DOP) +" Tak, to ja!");
    return "";
}

string pyt_o_garnizon()
{
    set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
        "powiedz do "+ this_player()->query_name(PL_DOP) +
        "Garnizon pan"+this_player()->koncowka("ie","i")+" to "+
        "ten du^zy kamienny budynek na ws... wsch... Tam gdzie s^lo^nce "+
        "si^e pojawia!");
    return "";
}

string pyt_o_burmistrza()
{
    set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
        "powiedz do "+ this_player()->query_name(PL_DOP) +" pan Neville "+
        "jest naszym burmistrzem!");
    return "";
}

string default_answer()
{
    set_alarm(2.0, 0.0, "powiedz_gdy_jest", this_player(),
        "rozloz rece");
    return "";
}

int
query_straznik()
{
    return 1;
}

init_living()
{
  ::init_straznik();
}

void random_long()
{
	int i = random(3);
	switch (i)
	{
		case 0:
		{
			set_long("Stra積ik powoli wodzi wzrokiem po okolicy najwyra幡iej"
		+" szukaj帷 jakiej� atrakcji. Jego sylwetka 鈍iadczy o tym,"
		+" i� musi by� on naprawd� zahartowany i zapewne w豉郾ie"
		+" dlatego zosta� naj皻y jako gwardzista miasta Rinde."
		+" Wydaje ci si�, 瞠 walka z nim mog豉by przyprawi� sporo"
		+" k這pot闚.\n");
		break;
		}

		case 1:
		{
		set_long("M篹czyzna co chwil� mruczy pod nosem jakie� przekle雟twa,"
		+" najwyra幡iej narzekaj帷 na prze這穎nego, tudzie� na ca陰"
		+" swoj� robot�. Leniwie, oci篹a貫 ruchy i niezbyt przyjemne spojrzenie"
		+" wskazuj� na to, 瞠 stra積ik jest ju� dostatecznie znu穎ny i"
		+" zdenerwowany, wi璚 lepiej by這by nie wchodzi� mu w drog�.\n");
		break;
		}

		case 2:
		{
		set_long("Biegn帷a po prawym policzku szrama, ma貫,"
		+" rozbiegane oczka i dope軟iaj帷y ca這嗆 oble郾y u鄉iech sprawiaj�, 瞠"
		+" na pierwszy rzut oka nietrudno pomyli� tego cz這wieka z pospolitym"
		+" bandyt�. Najwyra幡iej burmistrz miasta uzna�, i� dobra stra� to tania stra�,"
		+" a ka盥y charakter mo積a z豉godzi� odpowiednim wynagrodzeniem.\n");
		break;
		}
	}
}

void emote_hook(string emote, object wykonujacy, string przyslowek)
{
	switch (emote)
	{
		case "klepnij":
		case "poca逝j":
		case "pog豉szcz":
		case "po豉skocz":
		case "przytul":
		{
			if(wykonujacy->query_gender())
				set_alarm(1.0, 0.0, "kobieta_dobre", wykonujacy);
			else
				set_alarm(1.0, 0.0, "facet_zle", wykonujacy);
			break;
		}
		case "kopnij":
		case "opluj":
		case "nadepnij":
		{
			if(wykonujacy->query_gender())
				set_alarm(1.0, 0.0, "kobieta_zle", wykonujacy);
			else
				set_alarm(1.0, 0.0, "facet_zle", wykonujacy);
			break;
		}
		case "szturchnij":
		{
			set_alarm(1.0, 0.0, "powiedz_gdy_jest", TP,
                        "powiedz do "+wykonujacy->query_name(PL_DOP)
				     +" Taaa?");
			break;
		}
	}
}

void kobieta_dobre(object wykonujacy)
{
	string rasa = wykonujacy->query_race();
	if(rasa ~= "krasnolud" || rasa ~= "gnom" || rasa ~= "nizio貫k")
	{
		set_alarm(1.0, 0.0, "powiedz_gdy_jest", TP,
                        "powiedz do "+wykonujacy->query_name(PL_DOP)+
			" Won pokurczu, albo ci 貫b ukr璚e!");
		command("kopnij "+wykonujacy->query_real_name(3));
	}
	else
	{
		command("klepnij "+wykonujacy->query_real_name(3)+" bezczelnie w po郵adek");
		set_alarm(1.0, 0.0, "powiedz_gdy_jest", TP,
                        "powiedz do "+wykonujacy->query_name(PL_DOP)
			+" Mo瞠 zajrzysz do mnie po robocie?");
		command("usmiechnij sie oblesnie");
	}
}

void kobieta_zle(object wykonujacy)
{
	string rasa = wykonujacy->query_race();
	if(rasa ~= "krasnolud" || rasa ~= "gnom" || rasa ~= "nizio貫k")
	{
		set_alarm(1.0, 0.0, "powiedz_gdy_jest", TP,
                        "powiedz do "+wykonujacy->query_name(PL_DOP)+
			" Won pokurczu, albo ci 貫b ukr璚e!");
		command("kopnij "+wykonujacy->query_real_name(3));
	}
	else
	{
		set_alarm(1.0, 0.0, "powiedz_gdy_jest", TP,
                        "powiedz do "+wykonujacy->query_name(PL_DOP)+
				" Te, male鎥a! Nie podskakuj!");
		command("spojrzyj z usmieszkiem na "+wykonujacy->query_real_name(3));
	}
}

void facet_zle(object wykonujacy)
{
	int  i = random(5);

	switch (i)
	{
		case 0..1:
		{
			set_alarm(1.0, 0.0, "powiedz_gdy_jest", TP,
                        "powiedz do "+wykonujacy->query_name(PL_DOP)+
                        " Spieprzaj, psie nasienie, albo ci� w豉sna matka "+
                        "nie pozna!");
			command("spojrzyj beznamietnie na "+OB_NAME(wykonujacy));
			break;
		}

		case 2..3:
		{
			set_alarm(1.0, 0.0, "powiedz_gdy_jest", TP,
                        "powiedz do "+wykonujacy->query_name(PL_DOP)+
				" Spr鏏uj tego jeszcze raz, to ci jaja rzyci� wyjd�.");
			command("kopnij "+OB_NAME(wykonujacy));
			break;
		}
        case 4:
        {
        	admin->add_enemy(wykonujacy->query_real_name(),1);
			command("zabij "+OB_NAME(wykonujacy));
        	break;
        }
	}
}

public string
query_auto_load()
{
    return 0;
}
