
/* Autor nieznany, poprawki by Avard */
#include <stdproperties.h>
#include "dir.h"

inherit STREET_STD;

void
create_street()
{
    set_short("Uliczka w pobli^zu karczmy");

    add_exit(CENTRUM_LOKACJE + "placsw.c","p^o^lnoc");
    add_exit("ulica4.c","po^ludnie");
    add_exit("ulica2.c","wsch^od");

    add_object(RINDE+"przedmioty/lampa_uliczna");

}
public string
exits_description()
{
    return "Dalsza cz^e^s^c placu widoczna jest na p^o^lnocy, "+
           "za^s na wschodzie i po^ludniu znajduj^a si^e ulice.\n";
}
string
dlugi_opis()
{
    string str;
    if(!dzien_noc())
    {
    str = "Ta do^s^c szeroka w tym miejscu ulica prowadzi na wsch^od w "+
        "kierunku du^zego i jasno o^swietlonego budynku, kt^orego drzwi "+
        "nieustannie si^e otwieraj^a. Z jego du^zych okien s^aczy si^e "+
        "migotliwe ^swiat^lo, kt^ore pali si^e nawet i za dnia, "+
        "przeplataj^ac si^e z delikatnymi promieniami s^lo^nca. Droga, "+
        "wy^lo^zona jest ciemnymi kamieniami, kt^ore u^lo^zone s^a "+
        "niezwykle dok^ladnie, a pomi^edzy nimi mo^zna dostrzec "+
        "po^z^o^lk^l^a traw^e. Ulica biegnie dalej na p^o^lnoc w stron^e "+
        "placu g^l^ownego miasta. Id^ac w przeciwn^a stron^e, mo^zna "+
        "doj^s^c do wysokich bram miasta, kt^orych zarys "+
        "maluje si^e ju^z tutaj.";
    }
    else
    {
    str = "Ta do^s^c szeroka w tym miejscu ulica prowadzi na wsch^od w "+
        "kierunku du^zego i jasno o^swietlonego budynku, kt^orego drzwi "+
        "nieustannie si^e otwieraj^a. Z jego du^zych okien s^aczy si^e "+
        "migotliwe ^swiat^lo, przeplataj^ac si^e z delikatnym blaskiem "+
        "ksi^e^zyca. Droga, wy^lo^zona jest ciemnymi kamieniami, kt^ore "+
        "u^lo^zone s^a niezwykle dok^ladnie, a pomi^edzy nimi mo^zna "+
        "dostrzec po^z^o^lk^l^a traw^e. Ulica biegnie dalej na p^o^lnoc "+
        "w stron^e placu g^l^ownego miasta. Id^ac w przeciwn^a stron^e, "+
        "mo^zna doj^s^c do wysokich bram miasta, kt^orych zarys "+
        "maluje si^e ju^z tutaj.";
    }
    str +="\n";


    return str;
}
