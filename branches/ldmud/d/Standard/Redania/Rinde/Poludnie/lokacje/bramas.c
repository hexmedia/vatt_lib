/* Opisy by Alcyone */
#include <stdproperties.h>
#include "dir.h"

inherit STREET_STD;

void create_street()
{
    set_short("Szeroka uliczka");

    add_exit("ulica4.c","p^o^lnoc");
    add_exit("ulica6.c","p^o^lnocny-zach^od");
    add_exit("placsw.c","wsch^od");
    add_sit("na bruku","na bruku","z bruku",0);

    add_item(({"budynki", "domki"}),
        "Wznosz�ce si� dooko�a ma�e domki mieszkalne nie przyci�gaj� "+
        "zbytnio niczyjej uwagi. Wi�ksze zainteresowanie budz� wysokie"+
        "budynki, kt�re wyra�nie odznaczaj� si� w�r�d niskich dach�w. "+
        "Pe�ni� one funkcj� wa�niejszych o�rodk�w miasta.\n");

    add_item("placyk",
        "Id�c w kierunku wschodnim mo�na doj�� do jednego z mniejszych"+
        "placyk�w Rinde, na kt�rym za dnia panuje spory tlok.\n");
    add_item("dach",
        "Majacz�cy w oddali dach jednego z wi�kszych zabudowa�, nale�y do "+
        "jednej z kilku karczm Rinde. Mo�na wywnioskowa�, �e karczma ta"+
        "jest odwiedzana do�� cz�sto, gdy� ci�gle mijaj� si� rozchichotane"+
        "osoby.\n");
    add_item("mury",
        "Wznosz�ca si� na po�udniu pot�na brama miasta pilnowana jest"+
        "przez kilku ros�ych stra�nik�w. Ca�e Rinde otoczone jest wysokimi "+
        "murami z ciemnej, w wielu miejscach poniszczonej ju� ceg�y.\n");

    add_event("Obok ciebie przesz�o kilka os�b zataczaj�cych si� lekko.\n");
    add_event("Pod murami rozleg� si� g�o�ny pisk.\n");
    add_event("Z oddali dochodzi g�o�ne nawo�ywanie.\n");
    add_event("Ma�e dziecko przebieg�o obok ciebie szturchaj�c ci� " +
        "przypadkiem.\n");
    add_event("Obok ciebie przeszed� zakapturzony m�czyzna, spiesz�c "+
        "w kierunku bramy.\n");
    add_event("Z oddali dochodz� g�o�ne krzyki.\n");
    add_event("Tu� obok ciebie rozleg� si� dono�ny huk.\n");

    add_prop(ROOM_I_INSIDE,0);
    add_object(RINDE+"przedmioty/lampa_uliczna");
    add_object(POLUDNIE_DRZWI+"bramas_out.c");
}
public string
exits_description()
{
    return "Ulica prowadzi na p^o^lnoc i p^o^lnocny-zach^od, na wschodzie "+
           "znajduje si^e plac, za^s na po^ludniu brama miasta.\n";
}

string dlugi_opis()
{
    string str;

    str = "Biegn�ca z p�nocy ulica w tym miejscu poszerza si� nieznacznie, "+
        "ze wzgl�du na ruch jaki panuje tutaj za dnia. Rozga��zia si� ona "+
        "w czterech kierunkach i prowadzi do wi�kszych budynk�w miasta, " +
        "kt�re wznosz� si� pomi�dzy zwyk�ymi domkami mieszkalnymi. Id�c " +
        "w kierunku wschodnim mo�na doj�� do jednego z mniejszych placyk�w "+
        "znajduj�cych si� w Rinde. Na p�nocy mo�na dostrzec maluj�cy si� "+
        "w oddali dach kt�rej� z kilku karczm, kt�ra odwiedzana jest do�� "+
        "ch�tnie, co mo�na wywnioskowa� po roze�mianych i obijaj�cych si� "+
        "o siebie osobach, id�cych w stron� po�udniowej bramy. Id�c w "+
        "tamtym kierunku mo�na napotka� wysokie mury miasta i stra�nik�w "+
        "pilnuj�cych przej�cia.\n";
    return str;
}
