
// kod popelniony przez Seda, opisy popelnione przez Yrana
// na forum Vatt`gherna, lekko poprawione przez Seda 07.01.2007
//TODO Dodanie ziolek na polki i do koszy

#include "dir.h"
inherit RINDE_STD; /* ten obiekt jest pokojem */
inherit "/lib/sklep.c"; /* ten obiekt jest sklepem */

#define SUBLOC_SCIANA ({"^sciana","^sciany","^scianie","^scian^e","^scian^a","^scianie","na"})

#include <macros.h>
#include <stdproperties.h> /* standardowe wlasciwosci */
#include <object_types.h> /* standardowe typy objektow */
#include <filter_funs.h>
#include <pl.h>

void
create_rinde()
{
    set_short("W aptece");
    set_long("@@dlugi_opis@@");
    add_prop(ROOM_I_INSIDE, 1);

    add_item(({ "instrukcję", "tabliczkę" }), "@@standard_shop_sign@@");

    add_item("dzwoneczek",
            "Spogl^adasz na ma^ly metalowy dzwoneczek w kszta^lcie odwr^oconego "
            +"kielicha. W jego ^srodku znajduje si^e niewielkie serce, "
            +"kt^ore podczas potrz^asni^ecia dzwonkiem obija si^e o ^scianki "
            +"wywo^luj^ac dono^sny d^xwi^ek.\n");

    set_event_time(500.0);
        add_event("W powietrzu unosi sie delikatny posmak dymu tytoniowego.\n");
        add_event("Stara, przegni^la w wielu miejscach pod^loga pod wp^lywem "
        +"twojego ci^e^zaru zaczyna cicho trzeszcze^c.\n");

    add_subloc(SUBLOC_SCIANA, "na");
    add_subloc_prop(SUBLOC_SCIANA, CONT_I_CANT_ODLOZ, 1);
    add_subloc_prop(SUBLOC_SCIANA, CONT_I_CANT_POLOZ, 1);
    add_subloc_prop(SUBLOC_SCIANA, SUBLOC_I_MOZNA_POWIES, 1);
    add_subloc_prop(SUBLOC_SCIANA, SUBLOC_I_DLA_O, O_SCIENNE);
    add_subloc_prop(SUBLOC_SCIANA, CONT_I_MAX_RZECZY, 0);

    add_object(POLUDNIE_OBIEKTY+"kosz.c", 3, 0);
    add_object(POLUDNIE_OBIEKTY+"biurko.c", 1, 0);
    add_object(POLUDNIE_OBIEKTY+"polka.c", 3, 0, SUBLOC_SCIANA);
    add_object(POLUDNIE_DRZWI+"drzwi_z_apteki.c", 1, 0);

    dodaj_rzecz_niewyswietlana("biurko", 1);
    dodaj_rzecz_niewyswietlana("kosz", 3);
    dodaj_rzecz_niewyswietlana("p^o^lka", 3);

    add_npc(RINDE_NPC+"zielarz", 1);
    add_object("/d/Standard/items/kosz_na_smieci");
}

string
dlugi_opis()
{
    int i, il, il_koszy;
    object *inwentarz;

    inwentarz = all_inventory();
    il_koszy = 0;
    il = sizeof(inwentarz);

    for (i = 0; i <il; ++i)
        if (inwentarz[i]->jestem_koszem_apteki()) {++il_koszy;}

    string str;
    str = "Apteka otoczona jest d^lugimi, drewnianymi, przymocowanymi "
        +"do pobielonych ^scian p^o^lkami, na kt^orych pouk^ladano r^o^znej "
        +"wielko^sci pojemniki i s^loiki pe^lne wszelakich zi^o^l "
        +"i drobnych kwiat^ow. ";

    if(il_koszy == 1)
    {
        str += "Pod jedn^a z nich miesci si^e wiklinowy kosz, "
            +"z kt^orego dociera zapach ro^slin wype^lniaj^acy ca^l^a izb^e. ";
    }

    if(il_koszy == 2)
    {
        str +="Pod jedn^a z nich mieszcz^a si^e dwa wiklinowe kosze, "
            +"z kt^orych dociera zapach ro^slin wype^lniaj^acy ca^l^a izb^e. ";
    }

    if(il_koszy == 3)
    {
        str +="Pod jedn^a z nich mieszcz^a si^e trzy wiklinowe kosze, "
            +"z kt^orych dociera zapach ro^slin wype^lniaj^acy ca^l^a izb^e. ";
    }

    str += "Z sufitu za^s zwisa kilka powieszonych na lnianym sznurku "
        +"p^eczk^ow suszonego czosnku i szerokich, zielonych li^sci "
        +"dodaj^acych jedynie kolejny zapach do tej ca^lej egzotycznej "
        +"mieszanki, kt^ora w po^l^aczeniu z unosz^acym si^e w powietrzu "
        +"lekkim posmakiem tytoniu fajkowego sprawia, ^ze wymieszane "
        +"ze sob^a r^o^zne wonie tworz^a niepowtarzalny od^or. ";

    if(jest_rzecz_w_sublokacji(0, "biurko"))
    {
        str +="Przy frontowej ^scianie znajduje si^e machoniowe, zdobione "
            +"ro^slinnymi ornamentami biurko. ";
    }

    str +="Chat^e o^swietlaj^a dwie spore, olejne lampy przymocowane "
        +"do frontowej ^sciany, kt^ore przez przybrudzone, pokryte warstw^a "
        +"kurzu szybki rzucaj^a nik^ly blask, pobie^znie zagl^adaj^ac "
        +"w k^aty pomieszczenia. Trzeszcz^aca pod wp^lywem ka^zdego kroku "
        +"pod^loga zbita jest ze spr^ochnia^lych w wielu miejscach desek. "
        +"Nad wej^sciem wisi ma^ly dzwoneczek, kt^ory przy ka^zdym "
        +"otworzeniu i zamkni^eciu solidnych, d^ebowych drzwi z wisz^acym "
        +"na zewn^atrz szyldem, dono^snie oznajmia wszystkim zebranym "
        +"w ^srodku, ^ze do apteki w^la^snie kto^s wszed^l. \n";
    return str;

}

void
init()
{
    ::init(); /* MUSI byc wywolane w kazdym inicie */
    init_sklep(); /* dodanie komend takich jak 'kup', 'sprzedaj' itp */
}
public string
exits_description()
{
    return "Wyj^scie prowadzi na ulice.\n";
}

