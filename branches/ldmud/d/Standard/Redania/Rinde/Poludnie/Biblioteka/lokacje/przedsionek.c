/**
 * \file /d/Standard/Redania/Rinde/Poludnie/Biblioteka/lokacje/przedsionek.c
 *
 * Przedsionek biblioteki w Rinde.
 *
 * @author Krun - kod
 * @author Tabakista - opisy
 * @date Listopad 2007
 */

/**
 * FIXME: EVENT^OW TE^Z TU NIE ZROBI^LEm
 * - Cisz^e panuj�c� w tym miejscu rozpraszaj� jedynie przyt^lumione d�wi^eki dobiegaj�ce z ulicy.
 * - Do zapachu kurzu i inkaustu do^l�cza nik^ly sw�d dymu.
 * - Korniki cicho cykaj� w �cianach.
 */
inherit "/std/room.c";

#include "dir.h"
#include <macros.h>
#include <stdproperties.h>
#include <object_types.h>

#define SUBLOC_SCIANA   "_subloc_sciana"

string dlugi();
string opis_okna();

void
create_room()
{
    set_short("Przedsionek Biblioteki");

    set_long(&dlugi());

    add_sit("na pod^lodze", "na pod^lodze", "z pod^logi", 0);

    /* dodajemy sublokacj^e typu 'na', jednak dosy^c specyficzn� */
    add_subloc(SUBLOC_SCIANA);
    /* nie chcemy, by mo^zna by^lo na �cian^e odk^lada^c ani k^la�^c rzeczy */
    add_subloc_prop(SUBLOC_SCIANA, CONT_I_CANT_ODLOZ, 1);
    add_subloc_prop(SUBLOC_SCIANA, CONT_I_CANT_POLOZ, 1);
    /* oczywi�cie mo^zna na �cianie wiesza^c przedmioty */
    add_subloc_prop(SUBLOC_SCIANA, SUBLOC_I_MOZNA_POWIES, 1);
    /* ale tylko typu �ciennego */
    add_subloc_prop(SUBLOC_SCIANA, SUBLOC_I_DLA_O, O_SCIENNE);
    add_subloc_prop(SUBLOC_SCIANA, CONT_I_MAX_RZECZY, 1);

    add_object(RINDE_BIBLIOTEKA_PRZEDMIOTY + "lampa.c", 1, 0, SUBLOC_SCIANA);
    add_object(RINDE_BIBLIOTEKA_PRZEDMIOTY + "biurko.c");
    add_object(RINDE_BIBLIOTEKA_DRZWI + "z_biblioteki");
    add_object(RINDE_BIBLIOTEKA_DRZWI + "do_glownej");

    add_item(({"okno", "okienko"}), &opis_okna());

    dodaj_rzecz_niewyswietlana("masywne brunatne biurko");

    add_prop(ROOM_I_INSIDE, 1);

    add_npc(RINDE_NPC + "bibliotekarz.c");

    add_exit(RINDE_BIBLIOTEKA + "magazyn.c", ({"magazyn", "do magazynu ksi�^zek", "z magazynu ksi�^zek"}), 0, 0);
    add_exit(RINDE_BIBLIOTEKA + "magazyn.c", ({"magazyn ksi�^zek", "do magazynu ksi�^zek", "z magazynu ksi�^zek"}), 0, 0);
}

string exits_description()
{
    return "Wyj�cie prowadzi przez ciemne dwuskrzyd^lowe drzwi drzwi na "+
        "ulic^e, za� niskie drzwi naprzeciw wiod� do czytelni. " +
        "Obok nich znajduje si^e wej�cie do magazynu ksi�^zek.\n";
}

string dlugi()
{
    return "Snopy �wiat^la wpadaj�ce przez niewielkie okienko nad "+
        "drzwiami padaj� wprost na znajduj�ce si^e naprzeciw wej�cia "+
        "masywne biurko, zdaj�ce stapia^c si^e, w panuj�cym tutaj "+
        "p^o^lmroku, z pociemnia^lymi ze staro�ci deskami pokrywaj�cymi "+
        "pod^log^e i �ciany. Ponad tym meblem, zarzuconym pergaminami, "+
        "ksi�^zkami i g^esimi pi^orami, w rogach tego niewielkiego "+
        "pomieszczenia dostrzec mo^zna girlandy paj^eczyn mozolnie "+
        "zbieraj�cych si^e przez ca^le lata. Przy nim na wbitym w "+
        "�cian^e haku wisi wys^lu^zona blaszana lampa. Suche powietrze "+
        "przesycone jest zapachem kurzu i inkaustu. D�wi^eki "+
        "dobiegaj�ce z ulicy s� tu mocno przyt^lumione.\n";
}

string opis_okna()
{
    if(jest_dzien())
    {
        return "Niewiekie, okr�g^le okienko, o szybkach pokrytych paj^eczynami "+
            "i sadz� z �wiec wpuszcza mimo to nieco dziennego �wiat^la.\n";
    }
    else
    {
        return "Niewiekie, okr�g^le okienko, o szybkach pokrytych paj^eczynami i "+
            "sadz� z �wiec wpuszcza mimo to nieco �wiat^la ulicznych latar^n.\n";
    }
}