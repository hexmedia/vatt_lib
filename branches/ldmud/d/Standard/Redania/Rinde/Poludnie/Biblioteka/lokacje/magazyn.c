/**
 * \file /d/Standard/Redania/Rinde/Poludnie/Biblioteka/lokacje/magazyn.c
 *
 * Magazyn ksi��ek w bibliotece w Rinde
 *
 * @author Krun - kod
 * @author Tabakista - opisy
 * @date Listopad 2007
 */

inherit "/std/room.c";

#include "dir.h"
#include <macros.h>
#include <stdproperties.h>

string dlugi();
string opis_regalow();

void
create_room()
{
    set_short("Magazyn ksi��ek.");
    set_long(&dlugi());
    set_event_time(300.0);
    add_event("Nad tob�, niemal bezszelestnie przemyka �ma.\n");
    add_event("Ca�kowita, niezm�cona cisza staje si� coraz uci��liwsza.\n");
    add_event("Ciche skrzypni�cie pod�ogi pod twoj� stop� wydaje si� "+
        "przera�liwym ha�asem w panuj�cej tutaj ciszy.\n");

    add_prop(ROOM_I_LIGHT, 1);

    add_item(({"rega�y", "p�ki", "rz�dy"}), &opis_regalow());

    add_sit("na pod�odze", "na pod�odze", "z pod�ogi", 0);

    add_prop(ROOM_I_INSIDE, 1);

    add_exit(RINDE_BIBLIOTEKA + "przedsionek.c", ({"przedsionek", "do przedsionka", "z przedsionka"}), 0, 0);
    add_exit(RINDE_BIBLIOTEKA + "przedsionek.c", ({"wyj�cie", "do przedsionka", "z przedsionka"}), 0, 0);
}

string exits_description()
{
    return "Jedyne wyj�cie prowadzi do przedsionka biblioteki.\n";
}

//Funkcja wyszukuje najmocniejsze �r�d�o �wiat�a
object znajdz_zrodlo_swiatla(int i)
{
    int last_light = 0;
    object *inv = all_inventory(TO);
    object ret;

    foreach(object o : inv)
    {
        int o_light, r_light, c_light;
        o_light = o->query_prop(OBJ_I_LIGHT);
        r_light = o->query_prop(ROOM_I_LIGHT);
        c_light = o->query_prop(CONT_I_LIGHT);

        if(last_light < o_light)
        {
            ret = o;
            last_light = o_light;
        }
        else if(last_light < c_light)
        {
            ret = o;
            last_light = c_light;
        }
        else if(last_light < r_light)
        {
            ret = o;
            last_light = r_light;
        }
    }

    if(i==1)
        return ret;

    //no a teraz musimy znale�� co �wieci.
    inv = ret->all_inventory();

    foreach(object o : inv)
    {
        int o_light, r_light, c_light;
        o_light = o->query_prop(OBJ_I_LIGHT);
        r_light = o->query_prop(ROOM_I_LIGHT);
        c_light = o->query_prop(CONT_I_LIGHT);

        if(last_light < o_light)
        {
            ret = o;
            last_light = o_light;
        }
        else if(last_light < c_light)
        {
            ret = o;
            last_light = c_light;
        }
        else if(last_light < r_light)
        {
            ret = o;
            last_light = r_light;
        }
    }

    return ret;
}

string dlugi()
{
    if(TO->query_prop(ROOM_I_LIGHT) > 1)
    { //sie co� swieci.
        object zrodlo = znajdz_zrodlo_swiatla(0);
        object wlasciciel_swiatla = znajdz_zrodlo_swiatla(1);
        return "�wiat�o " + zrodlo->short(TP, PL_CEL) +
            (wlasciciel_swiatla ? " trzymanej przez " + wlasciciel_swiatla->short(TP, PL_DOP) + " " : " ") +
            "wy�ania z mroku kilka najbli�szych rega��w, na kt�rych p�kach le�y "+
            "po kilka ksi���k, czy pergamin�w. Nietrudno tak�e dostrzec zupe�nie "+
            "puste, pokryte jedynie grub� warstw� kurzu. Zalega on tak�e pod�og�, "+
            "wymieciony jedynie butami przy najbli�szych wej�ciu rega�ach. Dalej, "+
            "za kr�giem �wiat�a widoczne s� tylko mroczne kontury kolejnych rz�d�w. "+
            "S�dz�c po odbijaj�cym si� odg�osie twoich krok�w ta sala jest du�a, "+
            "lecz nie spos�b dostrzec jak bardzo. Cisza tutaj panuj�ca, a� dzwoni "+
            "w uszach. Zapach kurzu jest jest tym bardziej uci��liwy, "+
            "�e powietrze jest bardzo suche.\n";
    }
    else
    { //sie nie �wieci.. Ciemno�� widze;P
        return "S�dz�c po odbijaj�cym si� odg�osie twoich krok�w ta sala jest du�a, "+
            "lecz nie spos�b dostrzec co� wi�cej ni� pierwsze rz�dy rega��w, a i te "+
            "s� jedynie mrocznymi konturami. Cisza tutaj panuj�ca, a� dzwoni w "+
            "uszach. Zapach kurzu jest jest tym bardziej uci��liwy, �e powietrze "+
            "jest bardzo suche.\n";
    }
}

string opis_regalow()
{
    if(TO->query_prop(ROOM_I_LIGHT) > 1)
    {
        return "Kilka rega��w, kt�re znalaz�y si� w kr�gu �wiat�a "+
            "zalegaj� tylko pojedyncze ksi��ki, a niejedna p�ka "+
            "jest ca�kowicie pusta. Dalsze rz�dy, skryte ju� w "+
            "mroku nie zdradzaj� swojej zawarto�ci.\n";
    }
    else
    {
        return "Na dw�ch najbli�szych rega�ach dostrzegasz niewyra�ne "+
            "kontury le��cych na nich pojedynczych ksi��ek, jednak "+
            "kolejne rz�dy s� tylko mrocznymi zarysami w niczym "+
            "innym nie zm�conej ciemno�ci.\n";
    }
}