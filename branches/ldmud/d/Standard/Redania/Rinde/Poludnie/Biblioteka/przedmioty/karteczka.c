/**
 * \file /d/Standard/Redania/Rinde/Poludnie/przedmioty/karteczka.c
 *
 * Taki fajny bajer w posiadaniu bibliotekarza.
 *
 * @author Krun - kod
 * @author Tabakista - opisy
 * @date Grudzie� 2007
 */

inherit "/std/object.c";

#include <materialy.h>

void
create_object()
{
    ustaw_nazwe("karteczka");

    dodaj_przym("papierowy", "papierowi");

    set_long("�wistek papieru wielko�ci oko�o dw�ch d�oni pokryty zosta� "+
        "dziesi�tkami linii, �agodnych i zamaszystych znak�w. Ich na "+
        "pierwszy rzut oka przypadkowe rozmieszczenie przeczy temu "+
        "jakoby mia�oby to by� pismo. Pr�by odwr�cenia do g�ry nogami, "+
        "czy na bok, nie zdradzaj� co mia�aby przedstawia� dziwna pl�tanina "+
        "linii. Dopiero po chwili dostrzegasz osobliwy szczeg�. Niemal "+
        "natychmiast, �ledz�c wzrokiem kolejne elementy, odkrywasz sprytnie "+
        "zakamuflowan� tre�� obrazka. Niew�tpliwie dowodzi ona pomys�owo�ci "+
        "artysty i jest na sw�j spos�b interesuj�ca, lecz mo�na w�tpi� czy "+
        "kobieta by�a w stanie zrobi� ze swoim cia�em co� podobnego jak na "+
        "ilustracji.\n");

    ustaw_material(MATERIALY_PAPIER);
}