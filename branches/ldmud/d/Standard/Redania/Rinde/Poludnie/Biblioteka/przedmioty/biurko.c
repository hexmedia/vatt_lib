/**
 * \file /d/Standard/Redania/Rinde/Poludnie/Biblioteka/przedmioty/biurko.c
 *
 * Biurko bibliotekarza z Rinde.
 *
 * @author Krun - kod
 * @author Tabakista - opis
 * @date Listopad 2007
 */

inherit "/std/container.c";

#include <pl.h>
#include <macros.h>
#include <materialy.h>
#include <stdproperties.h>

void
create_container()
{
    ustaw_nazwe("biurko");

    set_long("Ci�ki mebel z pociemnia�ego, jesionowego drewna wypaczy� si� "+
        "i pop�ka� w kilku miejscach, a blat ugi�� w d� jednak proste, lecz "+
        "solidne okucia nie pozwalaj� mu zbyt si� odkszta�ci�. Powierzchnia "+
        "biurka, tam gdzie jest widoczny spod stert ksi��ek i pergamin�w, "+
        "pokrywaj� plamy inkaustu oraz liczne rysy i zadrapania.\n");

    dodaj_przym("masywny", "masywni");
    dodaj_przym("brunatny", "brunatni");

    ustaw_material(MATERIALY_DR_JESION, 90);
    ustaw_material(MATERIALY_RZ_STAL, 10);

    make_me_sitable("przy", "przy " + TO->short(PL_MIE), "od " + TO->short(PL_DOP), 4, PL_MIE, "przy");

    add_prop(CONT_I_MAX_VOLUME, 20000);
    add_prop(CONT_I_VOLUME, 16800);

    add_prop(OBJ_I_NO_GET, "Nie da si� go podnie��.\n");
    add_prop(CONT_I_CANT_WLOZ_DO, 1); /* By nie mo�na by�o nic do niego w�o�y� komend� 'wloz do' */

    add_prop(CONT_I_WEIGHT, 10000);
    add_prop(CONT_I_MAX_WEIGHT, 20000);
    add_prop(CONT_I_DONT_SHOW_CONTENTS, 1);

    add_subloc("na");
    add_subloc_prop("na", CONT_I_MAX_WEIGHT, 44641);

    //W 100% z drewna d�bowego
    ustaw_material(MATERIALY_DR_DAB);
}