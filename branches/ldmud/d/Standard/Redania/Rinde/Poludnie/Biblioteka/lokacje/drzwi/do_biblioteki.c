/*
 * Drzwi do biblioteki
 * Faeve, 27.03.08
 *
 */

inherit "/std/door";

#include <pl.h>
#include "dir.h"

void
create_door()
{
    ustaw_nazwe("drzwi");
    dodaj_przym("ciemny", "ciemni");
    dodaj_przym("dwuskrzyd^lowy","dwuskrzyd^lowi");

    set_other_room(POLUDNIE +"Biblioteka/lokacje/przedsionek");
    set_door_id("DRZWI_DO_BIBLIOTEKI_RINDE");
    set_door_desc("Wielkie i dwuskrzyd^lowe, wykonane z pomalowanego na "+
        "ciemny br^az drewna i poci^agni^ete jakim^s matowym impregranetm. "+
        "Prezentuj^a si^e wyj^atkowo elegancko, pomimo braku jakichkolwiek "+
        "zdobie^n.\n");

    set_open_desc("");
    set_closed_desc("");

    set_pass_command(({"drzwi","biblioteka","przedsionek"}),"do biblioteki","z zewn^atrz");
    //set_pass_mess("przez drewniane drzwi do lombardu");

    set_key("KLUCZ_DRZWI_DO_BIBLIOTEKI_RINDE");
    set_lock_name(({"zamek", "zamku", "zamkowi", "zamek", "zamkiem", "zamku"}), 0, PL_MESKI_NOS_NZYW);
}
