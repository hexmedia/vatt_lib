
// kod popelniony przez Seda, opisy popelnione przez Yrana 
// na forum Vatt`gherna, lekko poprawione przez Seda 07.01.2007

#pragma strict_types 

inherit "/std/container";

#include "dir.h"
#include <pl.h>
#include <macros.h>
#include <filter_funs.h>
#include <stdproperties.h>
#include <composite.h>
#include <cmdparse.h>
#include <object_types.h>

nomask void
create_container()
{
    ustaw_nazwe("biurko");

    set_long("Machoniowe biurko to bardzo prosta konstrukcja. Zbite jest "
	+"z zaledwie trzech szerokich desek, z czego dwie pe^lnia rol^e n^o^zek, "
	+"a trzecia blatu. Boki owego mebla zdobione s^a wyrytymi w dr^ewnie "
	+"ro^slinnymi ornamentami, kt^ore dodaj^a odrobiny elegancji temu, "
	+"na pierwszy rzut oka, prostacko wygl^adaj^acemu przedmiotowi. "
	+"@@opis_sublokacji| Na blacie |na|.|||lezy |leza |lezy @@\n");

    add_item(({ "plyte", "blat", "powierzchnie" }), "\n");
    add_item(({ "noge", "nogi" }), "Drewniane nogi sa lekko krzywe, " +
                    "a jednak caly stol zdaje sie byc stabilny.\n");
					
    setuid();
    seteuid(getuid());
	
    add_prop(CONT_I_MAX_VOLUME, 20000);
    add_prop(CONT_I_VOLUME, 16800);
    add_prop(CONT_I_CANT_WLOZ_DO, 1);
    add_prop(CONT_I_WEIGHT, 10000);
    add_prop(CONT_I_MAX_WEIGHT, 20000);
    add_prop(CONT_I_DONT_SHOW_CONTENTS, 1);
	
    add_subloc("na");
    set_owners(({RINDE_NPC+"zielarz"}));
}

public int
query_type()
{
        return O_MEBLE;
}

