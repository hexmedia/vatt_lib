/* Szyld lombardu na poludniu Rinde.
   Saturday 08 of April 2006  Lil              */

inherit "/std/object.c";
#include "dir.h"
#include <pl.h>
#include <stdproperties.h>
#include <macros.h>
#include <materialy.h>

int przeczytaj_szyld();

create_object()
{
   ustaw_nazwe("szyld");
   dodaj_przym("ma�y","mali");
   set_long("Sporych rozmiar�w szyld z napisem g�osz�cym: " +
            "\"Lombard u Ferela zaprasza!\"\n");
   ustaw_material(MATERIALY_DR_SOSNA);
   add_prop(OBJ_I_WEIGHT, 1985);
   add_prop(OBJ_I_VOLUME, 451);
   add_prop(OBJ_I_VALUE, 2);
   add_prop(OBJ_I_NO_GET, "Szyld dynda sobie wysoko "+
             "nad twoj� g�ow�. Raczej go nie si�gniesz.\n");
   add_prop(OBJ_I_DONT_GLANCE,1);
   add_cmd_item(({"szyld","szyld lombardu","szyld nad lombardem"}),
                "przeczytaj", przeczytaj_szyld,
                  "Przeczytaj co?\n");
}

int
przeczytaj_szyld()
{
  write("Lombard u Ferela zaprasza!\n");
  return 1;
}