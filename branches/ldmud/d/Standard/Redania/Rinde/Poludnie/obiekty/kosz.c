
// kod popelniony przez Seda, opisy popelnione przez Yrana 
// na forum Vatt`gherna, lekko poprawione przez Seda 07.01.2007

#pragma strict_types

inherit "/std/container";

#include "dir.h"
#include <pl.h>
#include <macros.h>
#include <filter_funs.h>
#include <stdproperties.h>
#include <composite.h>
#include <cmdparse.h>
#include <object_types.h>
#include <materialy.h>
nomask void
create_container()
{
    ustaw_nazwe("kosz");

    set_long("Wyklinowy kosz nie posiada ^zadnej pokrywy przez co wida^c "
			+"ca^la zawarto^s^c tego aromatycznego pojemnika. "
			+"@@opis_sublokacji|Dostrzegasz w nim |w|.\n|\n||" + PL_BIE+"@@");
    setuid();
    seteuid(getuid());
	add_prop(CONT_I_RIGID, 1);
    add_prop(CONT_I_MAX_VOLUME, 5000);
    add_prop(CONT_I_VOLUME, 2500);

    add_prop(CONT_I_WEIGHT, 750);
    add_prop(CONT_I_MAX_WEIGHT, 3750);
    set_type(O_MEBLE);
    ustaw_material(MATERIALY_RZ_WIKLINA);
    //add_prop(CONT_I_DONT_SHOW_CONTENTS, 1);
    set_owners(({RINDE_NPC+"zielarz"}));
}

