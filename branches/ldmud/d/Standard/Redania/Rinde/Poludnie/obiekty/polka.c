
// kod popelniony przez Seda, opisy popelnione przez Yrana 
// na forum Vatt`gherna, lekko poprawione przez Seda 07.01.2007

#pragma strict_types

inherit "/std/container";

#include "dir.h"
#include <pl.h>
#include <macros.h>
#include <filter_funs.h>
#include <stdproperties.h>
#include <composite.h>
#include <cmdparse.h>
#include <object_types.h>
#include <materialy.h>

nomask void
create_container()
{
    ustaw_nazwe("polka");

    set_long("D^luga, drewniana p^o^lka jest solidnie przymocowana "
		+"do pobielonej ^sciany. @@opis_sublokacji|Dostrzegasz na niej "+
            "|na|.\n|\n||" + PL_BIE+"@@");

    setuid();
    seteuid(getuid());
	add_prop(OBJ_I_NO_GET, "P^o^lka jest przymocowana do ^sciany bardzo solidnie.\n");
    add_prop(CONT_I_MAX_VOLUME, 20000);
    add_prop(CONT_I_VOLUME, 16800);
    add_prop(CONT_I_CANT_WLOZ_DO, 1); 
    add_prop(CONT_I_WEIGHT, 10000);
    add_prop(CONT_I_MAX_WEIGHT, 20000);
    add_prop(CONT_I_DONT_SHOW_CONTENTS, 1);
    ustaw_material(MATERIALY_DR_KLON);
    set_type(O_SCIENNE);
	
    add_subloc("na");
    set_owners(({RINDE_NPC+"zielarz"}));
}
