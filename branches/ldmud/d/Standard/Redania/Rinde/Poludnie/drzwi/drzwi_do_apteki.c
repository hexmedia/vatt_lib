
/*Drzwi apteki by Sed 06.03.07*/

inherit "/std/door";

#include <pl.h>
#include "dir.h"

void
create_door()
{
    ustaw_nazwe("drzwi");
    dodaj_przym("drewniany", "drewniani");

    set_other_room(POLUDNIE_LOKACJE+"apteka.c");

    set_door_id("DRZWI_Z_APTEKI_RINDE");
    set_door_desc("Masz przed sob^a solidne drewniane drzwi.\n");

    set_open_desc("");
    set_closed_desc("");

    set_pass_command("apteka", "przez drewniane drzwi do apteki", "z ulicy");
}

