// opis: valor

inherit "/std/container.c";

#include <pl.h>
#include <stdproperties.h>
#include <macros.h>
#include <filter_funs.h>
#include <object_types.h>
#include <materialy.h>
#include <mudtime.h>
#include <composite.h>
#include <sit.h>
#include "dir.h"

string
dlugi_opis();

void
create_container()
{
    ustaw_nazwe("biurko");
    dodaj_przym("stary", "starzy");
    dodaj_przym("zniszczony", "zniszczeni");
    
    set_long("@@dlugi_opis@@");

    make_me_sitable("na", "na starym zniszczonym biurku",
        "ze starego zniszczonego biurka", 2, PL_MIE, "na");
    make_me_sitable("przy", "przy starym zniszczonym biurku",
        "od starego zniszczonego biurka", 1, PL_MIE, "przy");

    ustaw_material(MATERIALY_DR_LIPA);
    
    add_prop(CONT_I_WEIGHT, 40000);
    add_prop(CONT_I_VOLUME, 90000);
    add_prop(CONT_I_MAX_VOLUME, 160000);
    add_prop(CONT_I_CANT_WLOZ_DO, 1);
    add_prop(CONT_I_MAX_WEIGHT, 100000);
    add_prop(CONT_I_DONT_SHOW_CONTENTS, 1);

    add_subloc("na");
    
    add_object("/d/Standard/Redania/Rinde/Centrum/Ratusz/obiekty/administracja_lampka", 1, 0, "na");

    set_owners(({RINDE_NPC + "sekretarz"}));
}

public int
query_type()
{
    return O_MEBLE;
}

string
dlugi_opis()
{
    string str = "To niezwykle skromne biurko, wydawa^c by si^e mog^lo, " +
        "zosta^lo zbite z kilku desek. Blat naznaczony wieloma p^ekni^eciami"+
        " i rysami polakierowano, by ukry^c zniszczenia. Po prawej stronie " +
        "znajduje si^e kilka szuflad, kt^ore wygl^adaj^a jakby mia^ly zaraz "+
        "wylecie^c po wielu latach intensywnego u^zytkowania. ";

    switch (sizeof(all_inventory(this_object())))
    {
        case 0:
            str += "\n";
            break;
            
        case 1:
            str += "Le^zy na nim " +
            this_object()->subloc_items("na")[..-1] + ".\n";
            break;
            
        default:
            str += "Le^z^a na nim " +
            this_object()->subloc_items("na")[..-1] + ".\n";
            break;
    }
    
    return str;
}
