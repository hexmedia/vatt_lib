// ciuch sekretarza

#include "dir.h"
#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <pl.h>
#include <macros.h>
#include <materialy.h>
#include <object_types.h>

inherit "/std/armour";

void
create_armour()
{
    ustaw_nazwe("szata");

    dodaj_przym("lu^xny", "lu^xni");
    dodaj_przym("szary", "szarzy");

    set_long("Ta skromna, prosta szata nie wyr^o^znia si^e niczym spo^sr^od "+
    "innych ubra^n tego typu. Uszyta jest z taniego, farbowanego na szaro " +
    "lnu. Jej zalet^a s^a g^l^ebokie kieszenie, zdolne pomie^sci^c kilka " +
    "wi^ekszych drobiazg^ow. Cho^c miejscami jest zmechacona i powycierana, "+
    "to nadal nadaje si^e do noszenia i chyba jeszcze d^lugo ci pos^lu^zy.\n");

    ustaw_material(MATERIALY_LEN);

    add_prop(ARMOUR_I_D_ROZCIAGLIWOSC, 100);
    add_prop(ARMOUR_I_U_ROZCIAGLIWOSC, 100);

    set_slots(A_ROBE);
    
    add_subloc(({"kiesze�", "kieszeni", "kieszeni", "kiesze�", "kieszeni�",
        "kieszeni"}));
    add_subloc_prop("kiesze�", SUBLOC_S_OB_GDZIE, "wewn�trz pojemnej kieszeni");
    add_subloc_prop("kiesze�", SUBLOC_I_OB_PRZYP, PL_DOP);
    add_item("kiesze� w", "G^leboka kiesze^n szaty" +
        "@@opis_sublokacji| zawieraj�ca |kiesze�|.|.|" + PL_BIE + "@@\n", PL_MIE);

    set_owners(({RINDE_NPC + "sekretarz"}));
}
