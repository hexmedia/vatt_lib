inherit "/std/board";

#include "dir.h"
#include <materialy.h>

create_board()
{
    dodaj_przym("du�y","duzi");
    set_long("Oblepiona notkami, drewniana tablica stanowi "+
        "najwa�niejszy punkt tej izby. Ka�dy mo^ze doda^c tu co^s " +
        "od siebie, tote^z ca�� powierzchni^e pokrywa nietypowa "+
        "mieszanka og�osze�, spostrze^ze^n - bynajmniej nie tylko " +
        "inteligentnych, obelg, donos^ow, pochwa^l, reklam, propozycji " +
        "pracy oraz �yciowych rozmy�la�.\n");

    set_board_name(RATUSZ+"notki");

    set_silent(1);
    set_num_notes(100);
    ustaw_material(MATERIALY_DR_DAB, 90);
    ustaw_material(MATERIALY_MOSIADZ, 10);

    set_owners(({RINDE_NPC + "sekretarz"}));
}
