// 7 I - pewne problemy z kwestia lampki w opisie
// 28 - ale jakie? powinienem byl pisac bardziej do rzeczy :P gee.
// branie dokumentow z polki jak w przedswiatynnej alejce.

#include <stdproperties.h>
#include <mudtime.h>
#include <macros.h>
#include "dir.h"

inherit RINDE_STD;
inherit "/lib/peek";

#define SCIEZKA "/d/Standard/Redania/Rinde/Centrum/Ratusz/obiekty/"
#define JEST(x) jest_rzecz_w_sublokacji(0, (x)->short())

string dlugi_opis();
int wez(string str);

string
okienko()
{
    if (find_object(SCIEZKA + "administracja_okno")->query_open())
        return "ma^le okienko";
    else
        return "szczeliny zakmni^etego okienka";
}

string
pozycja()
{
    if (find_living("rindesekretarz")->query_prop("_sit_siedzacy") ==
        present("stare zniszczone biurko"))
        return ", pochylony nad blatem biurka, siedzi";
    else
        if (find_living("rindesekretarz")->query_prop("_sit_siedzacy"))
            return " siedzi";
        else
            return " stoi przy biurku";
}

void
create_rinde()
{
    set_short("administracja");
    set_long("@@dlugi_opis@@");

    add_prop(ROOM_I_INSIDE, 1);

    add_object("/d/Standard/Redania/Rinde/Centrum/Ratusz/drzwi/administracja");
    add_object(SCIEZKA + "administracja_okno");
    add_object(SCIEZKA + "administracja_biurko");
    add_npc(RINDE + "npc/sekretarz", 1, "czy_npca");

    dodaj_rzecz_niewyswietlana("ma^le kwadratowe okienko");
    dodaj_rzecz_niewyswietlana("stare zniszczone biurko");
    dodaj_rzecz_niewyswietlana("d^ebowe solidne drzwi");
    dodaj_rzecz_niewyswietlana("ma^la olejna lampka");

    add_item(({"p^o^lk^e", "p^o^lki", "rega^l", "rega^ly", "dokumenty na p^o^lce"}),
        "Sterty pokrytych kurzem dokument^ow " +
        "ci^agn^a si^e na wysokich p^o^lkach rega^lu pod ^scian^a.\n");
    add_item(({"pod^log^e", "posadzk^e", "kamienn^a posadzk^e"}), "Zimna, " +
        "kamienna posadzka sw^a matow^a szaro^sci^a sprawia, ^ze ca^le " +
        "pomieszczenie wydaje si^e by^c ponure i nieprzyjemne.\n");
    add_item(({"zwoje", "zwoje na biurku", "zw^oj", "zw^oj na biurku"}),
        "Na starym zniszczonym biurku zalega sterta zwoj^ow, chyba w " +
        "wi^ekszo^sci niezapisanych.\n");

    add_sit("na posadzce", "na posadzce", "z posadzki", 0);

    add_peek("przez okienko", "/d/Standard/Redania/Rinde/Centrum/lokacje/placs.c");
    add_peek("przez okno", "/d/Standard/Redania/Rinde/Centrum/lokacje/placs.c");

    //eventy?
}

int
czy_npca()
{
    switch (pora_roku())
    {
    	case MT_LATO:
    	case MT_WIOSNA:
	        return MT_GODZINA >= 6 && MT_GODZINA <= 22;
        case MT_JESIEN:
        case MT_ZIMA:
	    return MT_GODZINA >= 6 && MT_GODZINA <= 22;
    }
}

public string
exits_description()
{
    return "Wyj^scie z pomieszczenia prowadzi na korytarz.\n";
}

string
dlugi_opis()
{
    string str = "W przesi^akni^etym zapachem inkaustu pomieszczeniu " +
        "panuje p^o^lmrok. ";

    if (!dzien_noc())
        str += "Jedynie wpadaj^ace tu przez " + okienko() + " promienie " +
            "o^swietlaj^a sterty dokument^ow na zakurzonych p^o^lkach " +
            "podpieraj^acych ^scian^e. ";
    else
        if (present("ma�a olejna lampka", ({TO, present("biurko")})) || present("ma�a olejna zapalona lampka", ({TO,
            present("biurko")})) || present("lampka", ({TO, present("biurko")})))
        {
            if (present("lampka", ({TO, present("biurko")}))->
                query_prop(OBJ_I_LIGHT) == 1 || present("ma�a olejna zapalona lampka")->
                query_prop(OBJ_I_LIGHT) == 1)
            {
                    str += "Jedynie ma^la lampka olejna rzuca nik^le " +
                        "^swiat^lo na sterty dokument^ow na zakurzonych " +
                        "p^o^lkach podpieraj^acych ^scian^e. ";
            }
            else
            {
                str += "Nawet ma^la lampka olejna stoi zgaszona. ";
            }
        }
        else
        {
            if (JEST(SCIEZKA +"administracja_lampka"))
            {
                if (find_object(SCIEZKA + "administracja_lampka")->
                    query_prop(OBJ_I_LIGHT) == 1)
                        str += "Jedynie ma^la lampka olejna rzuca nik^le " +
                        "^swiat^lo na sterty dokument^ow na zakurzonych " +
                        "p^o^lkach podpieraj^acych ^scian^e. ";
                else
                    str += "Nawet ma^la lampka olejna stoi zgaszona. ";
            }

            else
                str += "";
         }

    str += "Zimna, kamienna posadzka swoj^a matow^a szaro^sci^a sprawia, ^ze " +
        "wszystko tu wydaje si^e by^c ponure i nieprzyjemne. ";

    if (JEST(SCIEZKA + "administracja_biurko"))
        if (jest_rzecz_w_sublokacji(0, "Rindesekretarz") ||
            jest_rzecz_w_sublokacji(0, "wysoki szczup^ly m^e^zczyzna"))
            str += "Naprzeciw wej^sciowych drzwi siedzi przy biurku sekretarz. ";
        else
            str += "Naprzeciw wej^sciowych drzwi stoi stare, zniszczone biurko. ";
    else
        if (jest_rzecz_w_sublokacji(0, "Rindesekretarz") ||
            jest_rzecz_w_sublokacji(0, "wysoki zamy^slony m^e^zczyzna"))
            str += "Po pokoju przechadza si^e sekretarz. ";
        else
            str += "Opr^ocz tego pok^oj wygl^ada, jakby czego^s - lub " +
                "kogo^s - w nim brakowa^lo. ";

    str += "\n";

    return str;
}

int wez(string str)
{
    object doku;

    if (str ~= "dokument z p^o^lki" || str ~= "dokument z rega^lu")
    {
        write("Bierzesz jeden z dokument^ow le^z^acych na p^o^lce.\n");
        saybb(QCIMIE(this_player(), PL_MIA) + " bierze jeden z dokument^ow " +
        "le^z^acych na p^o^lce.\n");

       doku = clone_object("/d/Standard/Redania/Rinde/Centrum/Ratusz/obiekty/administracja_doku");
       doku->set_owners(({"/d/Standard/Redania/Rinde/npc/sekretarz"}));
       doku->move(this_player());

        return 1;
    }

    return 0;
}

void
init()
{
    ::init();
    init_peek();

    add_action(wez, "we^x");
}
