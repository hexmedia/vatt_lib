// na podst. przykladu jeremiana

inherit "/std/container";

#include "dir.h"
#include <pl.h>
#include <macros.h>
#include <filter_funs.h>
#include <stdproperties.h>
#include <composite.h>
#include <cmdparse.h>
#include <object_types.h>
#include <materialy.h>

nomask void
create_container()
{
    ustaw_nazwe("rega^l");

    dodaj_przym("wielki", "wielcy");
    dodaj_przym("hebanowy", "hebanowi");

    set_long("Ten wielki rega^l z hebanu przys^lania ca^l^a ^scian^e. " +
        "Na jego p^o^lkach burmistrz gromadzi wszystkie dokumenty, listy " +
        "i mas^e innych, bardziej lub mniej wa^znych papier^ow.\n");
        
    ustaw_material(MATERIALY_DR_HEBAN);

    setuid();
    seteuid(getuid());
    add_prop(CONT_I_MAX_VOLUME, 200000);
    add_prop(CONT_I_VOLUME, 150000);

    add_prop(OBJ_I_NO_GET, "W ^zaden spos^ob nie uda ci si^e go podnie^s^c.\n");

    add_prop(CONT_I_WEIGHT, 100000);
    add_prop(CONT_I_MAX_WEIGHT, 200000);
    add_prop(CONT_I_CANT_WLOZ_DO, 1);

    add_subloc(({"pierwsza p^o^lka", "pierwszej p^o^lki", "pierwszej p^o^lce",
                 "pierwsz^a p^o^lk^e", "pierwsz^a p^o^lk^a", "pierwszej p^o^lce", "na"}));
    add_subloc(({"druga p^o^lka", "drugiej p^o^lki", "drugiej p^o^lce",
                 "drug^a p^o^lk^e", "drug^a p^o^lk^a", "drugiej p^o^lce", "na"}));
    add_subloc(({"trzecia p^o^lka", "trzeciej p^o^lki", "trzeciej p^o^lce",
                 "trzeci^a p^o^lk^e", "trzeci^a p^o^lk^a", "trzeciej p^o^lce", "na"}));

    add_subloc("pod");
    add_subloc("na");

    add_subloc_prop("pod", CONT_I_MAX_VOLUME, 500);
    add_subloc_prop("na", CONT_I_MAX_WEIGHT, 5000);

    add_prop(CONT_I_DONT_SHOW_CONTENTS, 1);

    add_item(({"p^o^lk^e", "pierwsz^a p^o^lk^e"}), "@@opis_sublokacji|" +
    "Na pierwszej p^o^lce wielkiego hebanowego rega^lu |pierwsza p^o^lka|.|" +
    "Pierwsza p^o^lka wielkiego hebanowego rega^lu jest pusta.|0|le^zy |le^z" +
    "^a |le^zy @@\n", PL_DOP);
    add_item("drug^a p^o^lk^e", "@@opis_sublokacji|" +
    "Na drugiej p^o^lce wielkiego hebanowego rega^lu |druga p^o^lka|.|" +
    "Druga p^o^lka wielkiego hebanowego rega^lu jest pusta.|0|le^zy |le^z" +
    "^a |le^zy @@\n", PL_DOP);
    add_item("trzeci^a p^o^lk^e", "@@opis_sublokacji|" +
    "Na trzeciej p^o^lce wielkiego hebanowego rega^lu |trzecia p^o^lka|.|" +
    "Trzecia p^o^lka wielkiego hebanowego rega^lu jest pusta.|0|le^zy | le^z" +
    "^a |le^zy @@\n", PL_DOP);
    
    add_subloc_prop("pierwsza p^o^lka", SUBLOC_S_OB_GDZIE, "na pierwszej p^o^lce");
    add_subloc_prop("pierwsza p^o^lka", SUBLOC_I_OB_PRZYP, PL_DOP);
    add_subloc_prop("druga p^o^lka", SUBLOC_S_OB_GDZIE, "na drugiej p^o^lce");
    add_subloc_prop("druga p^o^lka", SUBLOC_I_OB_PRZYP, PL_DOP);
    add_subloc_prop("trzecia p^o^lka", SUBLOC_S_OB_GDZIE, "na trzeciej p^o^lce");
    add_subloc_prop("trzecia p^o^lka", SUBLOC_I_OB_PRZYP, PL_DOP);

    set_owners(({RINDE_NPC + "sekretarz"}));
}

public int
query_type()
{
    return O_MEBLE;
}

/*
void
leave_inv(object ob, object to)
{
    if (!member_array(query_name(to), RADNI) && !member_array(query_name(to),
        CZARODZIEJE) && query_name(to) != BURMISTRZ)
    find_living("neville")->command("powiedz Hej, od^l^o^z to na miejsce!");
    this_player()->ad... ech *_* */