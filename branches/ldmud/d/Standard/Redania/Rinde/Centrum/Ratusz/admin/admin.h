#define PATH			"/d/Standard/Redania/Rinde/Centrum/Ratusz/"
#define LPATH                   PATH + "admin/"

#define MAIN			LPATH + "main.c"

#define OSOBY                   LPATH + "osoby"

#define P_NOWE                  LPATH + "p_current/"
#define P_AKTUALNE              LPATH + "p_current/"
#define P_ODRZUCONE             LPATH + "p_denied/"
#define P_ZAAKCEPTOWANE         LPATH + "p_accepted/"
#define P_ARCHIWALNE            LPATH + "p_archive/"

#define SCIEZKA "/d/Standard/Redania/Rinde/Centrum/Ratusz/"
#define DOKUMENT (SCIEZKA+"admin/dokument.c")

#define WSZYSTKIE_P       0
#define NOWE_P            1
#define AKTUALNE_P        1
#define ODRZUCONE_P       3
#define ZAAKCEPTOWANE_P   4

#define ARCHIWALNE_P      9

#define POPARCIE 1

//Oto kto ma obecnie prawo do zarządzania tym:
#define CZARODZIEJE \
    ({"Vera","Faeve"})

#define RADNI \
    ({ })
