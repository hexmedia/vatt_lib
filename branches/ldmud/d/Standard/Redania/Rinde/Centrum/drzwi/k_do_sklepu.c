/*
 * Niewielki stalowy klucz
 * Do drzwi_do_sklepu
 * Wykonany przez Avarda, dnia 09.06.06
 */

inherit "/std/key";

#include <stdproperties.h>
#include <pl.h>

#include "dir.h"

void
create_key()
{
    ustaw_nazwe("klucz");

    set_long("Niewielki, stalowy klucz. \n");

    dodaj_przym("niewielki", "niewielcy");
    dodaj_przym("stalowy", "stalowi");

    set_key("KLUCZ_DRZWI_DO_SKLEPU_RINDE");

    set_owners(({RINDE_NPC + "sklepikarz"}));
}