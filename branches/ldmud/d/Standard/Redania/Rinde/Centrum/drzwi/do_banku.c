inherit "/std/door";

#include "dir.h"
#include <pl.h>


void
create_door()
{
    ustaw_nazwe("drzwi");
    dodaj_przym("ci^e^zki","ci^e^zcy");
    dodaj_przym("d^ebowy", "d^ebowi");

    set_other_room(BANK_LOKACJE +"bank3.c");
    set_door_id("DRZWI_DO_BANKU_W_RINDE");
    set_door_desc("Ci^e^zkie i grube drzwi z twardego d^ebu, wzmocnione " +
    "stalowymi okuciami i ubezpieczone solidnym zamkiem.\n");

    set_pass_command("bank","do banku","z zewn^atrz");
    set_open_desc("");
    set_closed_desc("");

    set_key("KLUCZ_DRZWI_DO_BANKU_RINDE");
    set_lock_name(({"zamek", "zamku", "zamkowi", "zamek", "zamkiem", "zamku"}), 0, PL_MESKI_NOS_NZYW);
}
