#include "../dir.h"

#define BANK          (CENTRUM + "Bank/")
#define BANK_LOKACJE  (BANK + "lokacje/")

#define RATUSZ          (CENTRUM + "Ratusz/")
#define RATUSZ_LOKACJE  (RATUSZ + "lokacje/")
#define RATUSZ_DRZWI    (RATUSZ + "drzwi/")
#define RATUSZ_OBIEKTY  (RATUSZ + "obiekty/")
