inherit "/std/object.c";

#include "dir.h"
#include <pl.h>
#include <stdproperties.h>
#include <macros.h>
#include <filter_funs.h>
#include <object_types.h>
#include <materialy.h>
#include <mudtime.h>
#include <pogoda.h>
string dlugi_opis();

create_object()
{
    ustaw_nazwe("�awa");

    dodaj_przym("elegancki","eleganccy");
    dodaj_przym("kamienny","kamienni");

    make_me_sitable("na","na kamiennej ^lawie","z kamiennej ^lawy",3, PL_MIE, "na");

    add_item(({"zdobienia","zdobienia na","motywy","motywy na","ornamenty",
               "ornamenty na"}),
                "Zdobienia o geometrycznych kszta^ltach, wykute delikatnymi "+
               "uderzeniami d^luta jakiego^s artysty, uleg^ly wp^lywowi "+
               "czasu - lekko zatarte i poro^sniete mchem, kt^ory ujmuje "+
               "im elegancji, lecz dodaje jakiego^s ciep^la i subtelnego uroku.\n");
   
  
    ustaw_material(MATERIALY_MARMUR);

    set_long("@@dlugi_opis@@");
    
    add_prop(OBJ_I_WEIGHT, 400000);
    add_prop(OBJ_I_VOLUME, 52513);
    add_prop(OBJ_I_VALUE, 269);
 // add_prop(OBJ_I_NO_GET, 1);
    set_owners(({RINDE_NPC+"sekretarz"}));//FIXME dodac burmistrza
}

public int
jestem_lawa_placu_rinde()
{
        return 1;
}

public int
query_type()
{
    return O_MEBLE;
}

string dlugi_opis()
{
    string str;
    str="Kamie�, mimo �e szary, wygl�da na dobry, elegancki "+
        "i mocny. �awa nie posiada oparcia, ale jest szeroka "+
	"i do�� wygodna. ";
    if(CZY_JEST_SNIEG(environment(this_object())))
      str+="Pod warstw� �niegu da si� zauwa�y� umieszczone po "+
           "brzegach rze�bienia o geometrycznym motywie i "+
	   "pot�ne nogi niczym �apy jakiego� zwierz�cia";
    else
      str+="Po brzegach widniej� rze�bienia o geometrycznym "+
           "motywie, a nogi ukszta�towane s� na kszta�t pot�nych "+
	   "�ap. Przy samym dole i od strony fontanny wkrada "+
	   "si� ciemnozielony mech, zacieraj�c nieco wzory i "+
	   "dodaj�c kszta�tom �awy nieco mi�kko�ci";
	   
    str+=".\n";
    return str;
}