// o ja pi* - molder.

#include "dir.h"

inherit RINDE_STD;
inherit "/lib/bank_pl";
inherit "/lib/depozyt";

#include <stdproperties.h>
#include <object_types.h>
#include <composite.h>
#include <pl.h>
#include <materialy.h>

string meble();
//object tabliczka;
//void dodaj_tabliczke();
int pomoc(string arg);

void
create_rinde()
{
    set_short("Ma�e zakurzone pomieszczenie");
    set_long("Niskie pomieszczenie, kt�rego skrzypi�c� pod�og� pokrywa " +
                "spora warstwa kurzu, sprawia wra�enie ubogiej, wiejskiej chatki. " +
                "W powietrzu unosi si� dra^zni^acy nozdrza zapach dymu." +
                "@@meble@@ Po lewej stronie "+
                "znajduje si� niewielka tabliczka zawieszona na �cianie"+
                ". Przy suficie wprawiono "+
                "ma�e okienko wpuszczaj�ce do �rodka niewiele �wiat�a, " +
                "gdy� przys�oni�te jest ciemn� zas�on�.\n");

    add_object(RINDE + "przedmioty/meble/fotel_wielki_drewniany.c");
    add_object(RINDE + "przedmioty/meble/biurko_spore.c");

    add_item("pod�og�",
                "Drewniana pod�oga, kt�ra skrzypi cicho pod naporem twoich st�p, " +
                "pokryta jest spor� warstw� kurzu.\n");

    add_item(({"okienko","zas^lon�" }),
                "Ma�e okienko wprawione zosta�o tu� przy suficie. Wpuszcza do " +
                "�rodka niewiele �wiat�a, gdy� przys�oni�te jest star�, "+
                "ciemn� zas^lon�.\n");

    add_event("Zza drzwi dochodzi cichy brz�k przeliczanych monet.\n");
    add_event("Pod�oga skrzypi cicho pod naporem twoich st�p.\n");
    add_event("Drzwi z trzaskiem obi�y si� o futryn�.\n");
    add_event("Zas�ona zako�ysa�a si� lekko, poruszona przez wiatr.\n");

    add_object(CENTRUM_DRZWI + "z_banku.c");

    //dodaj_tabliczke(); //vera.
    add_item(({"tabliczk�","niewielk� tabliczk�"}), "@@standard_bank_sign@@");
    add_cmd_item("napis na tabliczce", "przeczytaj", "@@standard_bank_sign@@");
    add_cmd_item("tabliczk^e", "przeczytaj", "@@standard_bank_sign@@");

    add_prop(ROOM_I_INSIDE,1);

    config_default_trade();
    config_trade_data();
    set_bank_fee(5);

    setuid();
    seteuid(getuid());
    config_depozyty();  //To musi by� wywo�ane przed wszystkimi funkcjami z \file /lib/depozyt

    dodaj_rodzaj_skrzynki(({"czarny", "czarni", "bukowy", "bukowi"}),
        "�redniej wielko�ci skrzyneczka wykonana zosta�a z drewna bukowego, "+
        "kt�rego jasny kolor maskuje czarna farba, gdzieniegdzie naddrapana. "+
        "Prostok�tne �cianki wyko�czone na brzegach zosta�y mistern� p�askorze�b�, "+
        "przedstawiaj�c� wij�cy si� bluszcz i zbite ze soba za pomoc� gwo�dzik�w. "+
        "Dodatkowo zabezpieczono je w rogach metalowymi okuciami chroni�cymi "+
        "przed zniszczeniem, czy tez otwarciem innym sposobem ni�li kluczykiem. "+
        "Frontowa �cianka skrzynki ozdobiona jest bluszczem, kt�rego listki "+
        "misternie okalaj� zamek. Ca�o�� wydaje si� solidn� robot�, nie "+
        "pozostawiaj�c� zastrze�e� co do bezpiecze�stwa przechowywanych tu skarb�w.\n", "skrzynka");

    ustaw_koszt_zalozenia_skrzynki_depozytowej(2400);       //10 koron za za�o�enie
    ustaw_koszt_skorzystania_ze_skrzynki_depozytowej(60);   //3 denary za skorzystanie
    ustaw_koszt_powiekszenia_skrzynki_depozytowej(720);     //3 korony za powi�kszenie
    ustaw_koszt_zlikwidowania_skrzynki_depozytowej(0);
}

public string
exits_description()
{
    return "Wyj^scie st^ad prowadzi na plac.\n";
}

void
init()
{
    ::init();
    init_bank();
    init_depozyt();
    add_action(pomoc, "?", 1);
    add_action(pomoc, "pomoc");
}

int pomoc(string arg)
{
    if(arg && arg != "bank")
        return 0;

    write(check_call(standard_bank_sign()));
    write("\n " + sprintf("%|79s", "----------") + "\n\n");
    depozyt_pomoc(arg);
    return 1;
}

public void
enter_inv(object ob, object from)
{
    ::enter_inv(ob, from);

    if (!objectp(ob))
        return;

    if (ob->query_type() == O_MEBLE)
        dodaj_rzecz_niewyswietlana(ob->short());
}

public void
leave_inv(object ob, object to)
{
    ::leave_inv(ob, to);

    if (!objectp(ob))
        return;
    if (ob->query_type() == O_MEBLE)
        usun_rzecz_niewyswietlana(ob->short());
}

string
meble()
{
    int i, il, ile;
    object *obarr;

    obarr = subinventory(0);
    obarr = filter(obarr, &operator(==)(, O_MEBLE) @ &->query_type());

    if ((il = sizeof(obarr)))
    {
        ile = 0;
        for (i = 0; i < il; ++i)
        {
            if (obarr[i]->short() ~= obarr[0]->short())
                ++ile;
        }
        return " Po�rodku pomieszczenia " + ilosc(ile, "stoi", "stoj�", "stoi") + " " + COMPOSITE_DEAD(obarr, PL_MIA) + ".";
    }
    return "";
}

/*void
dodaj_tabliczke()
{
    tabliczka=clone_object("/std/object");
    tabliczka->ustaw_nazwe("tabliczka");
    tabliczka->dodaj_przym("niewielki","niewielcy");
    tabliczka->ustaw_material(MATERIALY_DR_DAB);
    tabliczka->set_type(O_MEBLE);
    tabliczka->set_long("@@standard_bank_sign@@");
    tabliczka->add_prop(OBJ_I_WEIGHT, 9860);
    tabliczka->add_prop(OBJ_I_VOLUME, 13000);
    tabliczka->add_prop(OBJ_I_VALUE, 10);
    tabliczka->move(this_object());
}*/
