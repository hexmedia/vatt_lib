#pragma unique

#include <std.h>
#include <composite.h>
#include <formulas.h>
#include <stdproperties.h>
#include <macros.h>
#include <mudtime.h>
#include <ss_types.h>
#include <money.h>
#include <pl.h>
#include <object_types.h>
#include "dir.h"

inherit RINDE_NPC_STD;

void
create_rinde_humanoid()
{
    dodaj_przym("niski","niscy");
    dodaj_przym("zapracowany","zapracowani");
    ustaw_odmiane_rasy(PL_CZLOWIEK);
    set_gender(G_MALE);
    set_long("Ju^z na pierwszy rzut oka wida^c, ^ze karze^l nie nale^zy do "+
        "os^ob przyja^xnie nastawionych do innych. Ch^l^od jego spojrzenia "+
        "i grymas ust u�o^zonych tak, jak gdyby bankierowi podsuni^eto pod "+
        "nos co^s cuchn^acego ju^z od progu upewnia, �e do banku przychodzi "+
        "si^e tylko w interesach, a i to interesach takich, jakie w danej "+
        "chwili odpowiadaj^a jego wla^scicielowi. Nie jest uzbrojony, ale "+
        "tu^z przy jego r^ece le^zy niewielki dzwoneczek, kt^ory zapewne "+
        "dzia^la r^ownie skutecznie jak be^lt wystrzelony z kuszy.\n");

   ustaw_nazwe(({"ruygen","ruygena","ruygenowi","ruygena","ruygenem","ruygenie"}),
    PL_MESKI_OS);

    ustaw_imie(({"ruygen","ruygena","ruygenowi","ruygena","ruygenem","ruygenie"}),
    PL_MESKI_OS);
    set_stats (({ 40, 40, 40, 35, 40 }));

    add_prop(CONT_I_WEIGHT, 52000);
    add_prop(CONT_I_HEIGHT, 173);
    add_prop(LIVE_I_NEVERKNOWN,1);

    set_skill(SS_DEFENCE, 10 + random(5));
    set_skill(SS_UNARM_COMBAT, 10 + random(5));
    set_skill(SS_SWIM, 15);
    set_skill(SS_PARRY, 5 + random(4));

    set_cact_time(8);
    set_act_time(23 + random(20));

    add_cact("krzyknij Ha! Ha! My^slisz, ^ze si^e ciebie boj^e?!");
    add_cact("krzyknij Stra^ze do mnie!");
    set_chat_time(12);
    add_chat("Mhm, nast^epny prosz^e.");
    add_chat("To jest bank! Tu si^e pracuje, prosz^e o cisz^e!");
    add_chat("Wp^laca, czy nie?");
    add_act("emote zamy^sla si^e na moment.");
    add_act("emote przegl^ada uwa^znie jakie^s dokumenty.");
    add_act("emote przelicza stos monet.");


    set_alignment(250);
    set_whimpy(60);
}
