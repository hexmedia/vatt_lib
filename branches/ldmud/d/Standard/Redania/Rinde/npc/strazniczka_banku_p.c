
/* Straznik - polelfka z banku w Rinde   *
 * Made by Avard 04.06.07                *
 * Opis by Tinardan & Avard              */

#include <std.h>
#include <ss_types.h>
#include <macros.h>
#include <stdproperties.h>
#include <pl.h>
#include "dir.h"

inherit RINDE_NPC_STD;

void
create_rinde_humanoid()
 {
     ustaw_odmiane_rasy(PL_POLELFKA);
     set_gender(G_FEMALE);
     dodaj_nazwy("strazniczka");
     dodaj_przym("przysadzisty","przysadzi^sci");
     dodaj_przym("ciemnosk^ory","ciemnosk^orzy");

     set_long("Wydaje si^e, ^ze ta kobieta odziedziczy^la same najgorsze "+
         "cechy obu ras. Jest po ludzku kr^epa i umi^e^sniona, a elfie "+
         "oczy, zwykle tak pe^lne dumy i tajemnicy, u niej wygl^adaj^a "+
         "niczym dwie w^askie szparki pe^lne z^lo^sci. Na g^lowie ma stary, "+
         "powgniatany he^lm, a cia^lo, a^z do kolan skrywa lekko "+
         "przerdzewia^la kolczuga. W prawej r^ece trzyma nadziewany kolcami "+
         "morgenstern na ^la^ncuchu, a za pasem ukryty b^lyszczy sztylet.\n");

     set_stats (({ 65, 100, 60, 50, 80 }));
     add_prop(CONT_I_WEIGHT, 75000);
     add_prop(CONT_I_HEIGHT, 175);
     add_prop(LIVE_I_NEVERKNOWN, 1);

    set_act_time(30);
	add_act("emote rozgl^ada si^e.");
	add_act("emote mru^zy oczy.");
    add_act("emote spluwa na ziemi^e.");

	set_cchat_time(20);
	add_cchat("Teraz dowiesz si^e co to jest prawdziwy b^ol!");
	add_cchat("To ostatnia rzecz jak^a "+
        "zrobi^l"+this_player()->koncowka("e^s","a^s")+
        "w ^zyciu!");

    //add_armour(cos);
    //add_armour(cos);
    //clone_object(sdf)->move(npc);

    add_ask(({"bank"}), VBFC_ME("pyt_o_bank"));
	add_ask(({"ruygena","bankiera"}), VBFC_ME("pyt_o_bankiera"));
	add_ask(({"stra^znik^ow","stra^z"}), VBFC_ME("pyt_o_straz"));
	set_default_answer(VBFC_ME("default_answer"));

 }

	void
	powiedz_gdy_jest(object player, string tekst)
	{
		if (environment(this_object()) == environment(player))
		this_object()->command(tekst);
	}

	string
	pyt_o_bank()
	{
		set_alarm(2.0, 0.0, "powiedz_gdy_jest", this_player(),
		"'^Slep"+this_player()->koncowka("y^s","a^s")+
        "? Jeste^s w nim.");
		return "";
	}
	string
	pyt_o_bankiera()
	{
		set_alarm(2.0, 0.0, "powiedz_gdy_jest", this_player(),
			"'P^lac^a, reszta mnie nie obchodzi.");
		return "";
	}
	string
	pyt_o_straz()
	{
		set_alarm(2.0, 0.0, "powiedz_gdy_jest", this_player(),
			"'Nic ci do nich.");
		return "";
	}
	string
	default_answer()
	{
		set_alarm(2.0, 0.0, "powiedz_gdy_jest", this_player(),
        "powiedz do "+ this_player()->query_name(PL_BIE) +
        "My^slisz, ^ze ci odpowiem?");
		return "";
	}


    void
    add_introduced(string imie_mia, string imie_bie)
    {
        set_alarm(2.0, 0.0, "return_introduce", find_player(imie_mia));
    }
    void
    return_introduce(object ob)
    {
    command("wzrusz ramionami");
    }


    void
emote_hook(string emote, object wykonujacy)
{
    switch (emote)
    {
        case "kopnij": set_alarm(1.5, 0.0, "nienajlepszy", wykonujacy);
			        break;
        case "spoliczkuj": set_alarm(1.5, 0.0, "nienajlepszy", wykonujacy);
                    break;
        case "opluj" : set_alarm(2.5, 0.0, "nienajlepszy", wykonujacy);
                    break;
		case "poca^luj":
              {
              if(wykonujacy->query_gender() == 1)
                switch(random(1))
                 {
                 case 0:command("zabij "+OB_NAME(wykonujacy)); break;
                 }
              else
                {
                   switch(random(2))
                   {
                   case 0:command("powiedz Ka^zdy, kt^ory to zrobi^l ju^z "+
                                  "nie ^zyje!"); break;
                          command("zabij "+OB_NAME(wykonujacy)); break;
                   case 1:command("spojrzyj z obrzydzeniem na "+
                        OB_NAME(wykonujacy));
                          command("zabij "+OB_NAME(wykonujacy)); break;
                   }
                }

                break;
                }
        case "przytul": set_alarm(2.5, 0.0, "malolepszy", wykonujacy);
                   break;
        case "pog^laszcz": set_alarm(2.5, 0.0, "malolepszy", wykonujacy);
                   break;
    }
}

void
malolepszy(object wykonujacy)
{

   switch(random(2))
   {
     case 0: command("spojrzyj gniewnie na "+OB_NAME(wykonujacy)); break;
     case 1: command("powiedz Nie pozwalaj sobie."); break;

   }
}

void
nienajlepszy(object wykonujacy)
{
   switch(random(1))
   {
      case 0: command("zabij "+OB_NAME(wykonujacy)); break;
   }
}

