/*
 * Autor: Rantaur
 * Data 25.05.2006
 * Opis: Milutki golabek do postawienia na rynku, wcina pieczywo, mozna
 * go straszyc i wogle :P
 * Long pierwotnie napisany przez Tinardan (08.08.05) i
 * nieco przerobiony przez Rantaura.
 */

inherit "/std/creature.c";
inherit "/std/act/action.c";

#include "dir.h"

#include <macros.h>
#include <pl.h>
#include <stdproperties.h>
#include <object_types.h>
#include <cmdparse.h>
#include <mudtime.h>

int nakarm(string str);
int przeplosz(string str);
int emoty(string str);
void remove_me();
void eat_it(object food, int all);

string *jedzenie = ({"bu�ka", "buleczka", "chleb", "ciasto", "ciastko", "ciasteczko", "placek", "piernik", "pierniczek", "kawa^lek chleba"});

void
create_creature()
{
  ustaw_odmiane_rasy("golab");

  random_przym("szaropi�ry:szaropi�rzy bia�y:biali nakrapiany:nakrapiani||"
        +"niewielki:niewielcy t�usty:t�u�ci wychudzony:wychudzeni||m�ody:m�odzi"
        +" stary:starzy||ruchliwy:ruchliwi||jednooki:jednoocy||napuszony:napuszeni", 2);

  set_long("Ptak gruchaj�c weso�o, spaceruje spokojnie po brukowanej uliczce pusz�c dumnie po�yskuj�ce"
    +" na szyi pi�ra. Sprawia wra�enie, jakby nie ba� si� ludzi, jednak ca�y czas czujnie obserwuje"
    +" ka�da mijaj�c� go osob�. Go��b co jaki� czas rozk�ada swe delikatne skrzyd�a, demonstruj�c"
    +" ich harmonijna budow�.\n");
  add_prop(LIVE_I_NEVERKNOWN, 1);

  set_stats (({1, 35, 1, 5, 1}));
  set_hitloc_unarmed(0, ({1, 1, 1}), 40, "g�owa");
  set_hitloc_unarmed(1, ({1, 1, 1}), 60, "cia�o");

  add_prop(CONT_I_WEIGHT, random(120)+500);
  add_prop(CONT_I_VOLUME, 3*query_prop(CONT_I_WEIGHT)/5);
  add_prop(OBJ_M_NO_ATTACK, "@@zaatakowany@@");

  set_act_time(14);

  add_act("emote przechadza si� po placu, rado^snie gruchaj^ac.");
  add_act("emote zrywa si� nagle do lotu, by po chwili wyl�dowa� kilka krok�w od ciebie.");
  add_act("emote dziobie kilkakrotnie w bruku, wyra�nie czego� szukaj�c.");
  add_act("emote podskakuje zabawnie, uciekaj�c przed nogami przechodni�w.");
  add_act("emote przygl�da ci si^e czarnym, wypuk�ym okiem.");

  set_alarm_every_hour(&remove_me());
}

init()
{
  ::init();
  add_action(&nakarm(), "nakarm");
  add_action(&przeplosz(), "przep�osz");
  add_action(&emoty(), "kopnij");
  add_action(&emoty(), "opluj");
  add_action(&emoty(), "poca�uj");
  add_action(&emoty(), "pociesz");
  add_action(&emoty(), "pog�aszcz");
  add_action(&emoty(), "pog�ad�");
  add_action(&emoty(), "przytul");
  add_action(&emoty(), "szturchnij");
}

void eat_it(object food, int all)
{
  if(food->query_type() != O_JEDZENIE)
    {
      write("Ale� "+food->query_nazwa(PL_MIA)+" nie nadaje si� do jedzenia!\n");
      return;
    }

  if(member_array(food->query_nazwa(), jedzenie) == -1)
    {
      if(all)
	{
	  write("Rzucasz go��biom nieco "+food->short(PL_DOP)+". Zaciekawione"
		+" ptaki rychle przydreptuj� do jedzenia i dziobi� je kilka razy,"
		+" jednak po chwili rozchodz� si� pozostawiaj�c "+food->query_nazwa(3)
		+" niemal nietkni�t"+food->koncowka("y", "�", "e")+". Widocznie nie jest"
		+" to przysmak go��bi.\n");
	  saybb(QCIMIE(this_player(), PL_MIA)+" rzuca go��biom nieco "+food->short(PL_DOP)
		+". Zaciekawione ptaki rychle przydreptuj� do jedzenia i dziobi� je kilka"
		+" razy, jednak po chwili rozchodz� si� pozostawiaj�c "+food->query_nazwa(3)
		+" niemal nietkni�t"+food->koncowka("y", "�", "e")+". Widocznie nie jest"
		+" to przysmak go��bi.\n");
	  return;
	}
      else
	{
	  write("Rzucasz "+this_object()->short(PL_CEL)+" nieco "+food->short(PL_DOP)
		+". Zaciekawiony ptak rychle przydreptuje do jedzenia i dziobie je kilka"
		+" razy, jednak po chwili odchodzi, rozgladaj�c sie za czym� innym. Widocznie "
		+food->query_nazwa(PL_MIA)+" nie jest przysmakiem go��bi.\n");
	  saybb(QCIMIE(this_player(), PL_MIA)+" rzuca "+this_object()->short(PL_CEL)
		+" nieco "+food->short(PL_DOP)+". Zaciekawiony ptak rychle przydreptuje do"
		+" jedzenia i dziobie je kilka razy, jednak po chwili odchodzi, rozgladaj�c"
		+" si� za czym� innym. Widocznie "+food->query_nazwa(PL_MIA)+" nie jest"
		+" przysmakiem go��bi.\n");
	  return;
	}
    }

  int amount = food->query_amount();
  if(all)
    {
      write("Rzucasz go��biom nieco "+food->short(PL_DOP)+". Ptaki"
	    +" niemal natychmiast zlatuj� si� w jedno miejsce i przepychaj�c"
	    +" si� mi�dzy sob� zaciekle walcz� o ka�dy k�s pokarmu. Po chwili"
	    +" na bruku nie dostrzegasz ju� ani �ladu rzucone"
	    +food->koncowka("go", "j")+" wcze�niej "+food->query_nazwa(1)+".\n");
      saybb(QCIMIE(this_player(), PL_MIA)+" rzuca go��biom nieco "
	    +food->short(PL_DOP)+". Ptaki niemal natychmiast zlatuj� si� w"
	    +"jedno miejsce i przepychaj�c si� mi�dzy sob� zaciekle walcz� o ka�dy"
	    +" k�s pokarmu. Po chwili na bruku nie dostrzegasz ju� ani �ladu"
	    +" rzucone"+food->koncowka("go", "j")+" wcze�niej "
	    +food->query_nazwa(1)+".\n");
    }
  else
    {
      write("Rzucasz "+ this_object()->short(PL_CEL)+" nieco "+
	    food->short(PL_DOP)+". Ptak �wawo przydreptuje do jedzenia i"
	    +" rado�nie gruchaj�c zaczyna energicznie dzioba� "+
	    food->query_nazwa(3)+". Po chwili na bruku nie wida� ju� ani"+
	    " jednego okruszka.\n");
      saybb(QCIMIE(this_player(), PL_MIA)+" rzuca "+this_object()->short(PL_CEL)
	    +" nieco "+food->short(PL_DOP)+". Ptak �wawo przydreptuje do"
	    +" jedzenia i rado�nie gruchaj�c zaczyna energicznie dzioba� "
	    +food->query_nazwa(3)+". Po chwili na bruku nie wida� ju� ani"
	    +" jednego okruszka.\n");
    }
  if(amount - 30 <= 0)
    food->remove_object();
  else
    food->set_amount(amount - 30);
}

int nakarm(string str)
{
  object food;
  object *obj;
  if(!str)
    {
      notify_fail("Nakarm kogo czym?\n");
      return 0;
    }

  if(!parse_command(lower_case(str), environment(this_player()),"%i:"+PL_BIE+" %o:"+ PL_NAR, obj, food))
    {
      notify_fail("Nakarm kogo czym?\n");
      return 0;
    }
	  obj = CMDPARSE_STD->normal_access(obj, 0, 0);
	  if(sizeof(obj) == 0)
	    {
	      notify_fail("Kt�rego go��bia chcesz nakarmi�?\n");
	      return 0;
	    }

	  if(sizeof(obj) > 1)
	    {
	      eat_it(food, 1);
	      return 1;
	    }

	  if(obj[0] == this_object())
	    {
	      eat_it(food, 0);
	      return 1;
	    }
	  else return 0;
}

int przeplosz(string str)
{
  if(!str)
    {
      notify_fail("Co chcesz przeploszy�?\n");
      return 0;
    }
  mixed golebie;
  if(!parse_command(lower_case(str), environment(this_player()), "%i:"+PL_BIE, golebie))
    {
      notify_fail("Co chcesz przeploszy�?\n");
      return 0;
    }
  golebie = CMDPARSE_STD->normal_access(golebie, 0, 0);
  int szansa = random(101);
  if(szansa < 40)
    {
      if(sizeof(golebie) > 1)
	{
	  write("Tupi�c g�o�no podbiegasz do spaceruj�cych po placu"
		+" go��bi. Wystraszone ptaki niemal natychmiast zrywaj�"
		+" si� do lotu, zataczaj� nad placem kilka obszernych k�,"
		+" po czym jakgdyby nigdy nic przysiadaj� na drugim ko�cu placu.\n");
	  saybb(QCIMIE(this_player(), PL_MIA)+" g�o�no tupi�c podbiega do"
		+" spaceruj�cych po placu go��bi. Wystraszone ptaki niemal"
		+" natychmiast zrywaj� si� do lotu, zataczaj� nad placem"
		+" kilka obszernych k�, po czym jakgdyby nigdy nic przysiadaj� na"
		+" drugim ko�cu placu.\n");
	  return 1;
	}
      if(golebie[0] == this_object())
	{

	    {
	      write("Tupi�c g�o�no podbiegasz do "+this_object()->short(PL_DOP)
		    +". Wystraszony ptak zrywaj�c si� do lotu rozpo�ciera szeroko"
		    +" skrzyd�a, wzbija si� w powietrze i zatacza kilka obszernych k�"
		    +" nad placem, aby po chwili przysi��� na drugim ko�cu placu.\n");
	      saybb(QCIMIE(this_player(), PL_MIA)+" g�o�no tupi�c podbiega do "
		    +this_object()->short(PL_DOP)+". Wystraszony ptak zrywaj�c si�"
		    +" do lotu rozpo�ciera szeroko skrzyd�a, wzbija si� w powietrze"
		    +" i zatacza kilka obszernych k� nad placem, aby po chwili"
		    +" przysi��� na drugim ko�cu placu.\n");
	      return 1;
	    }

	} else return 0;
    }
  else // Jezeli nie przestraszylismy...
    {
      if(sizeof(golebie) > 1)
	{
	  write("Tupi�c g�o�no podbiegasz do spaceruj�cych po placu"
		+" go��bi, te uciekaj� o kilka krokow i za chwile,"
		+" zupe�nie ci� ignorujac wracaj� do swoich zaj��.\n");
	  saybb(QCIMIE(this_player(), PL_MIA)+" g�o�no tupi�c podbiega"
		+" do spaceruj�cych po placu go��bi, te uciekaj� od "
		+this_player()->koncowka("niego", "niej")+" o kilka"
		+" krokow, by za chwile, zupe�nie "+this_player()->koncowka("go", "j�")
		+" ignoruj�c, powr�ci� do swoich zaj��.\n");
	  return 1;
	}
      if(golebie[0] == this_object())
	{
	  write("Tupi�c g�o�no podbiegasz do "+this_object()->short(PL_DOP)
		+", ten odwraca si� gwa�townie w twoj� stron�, jednak najwyra�niej"
		+" jest on ju� przyzwyczajony do takich dziwnych zachowa�, gdy� po"
		+" chwili, zupe�nie ci� ignoruj�c, wraca do swoich zaj��.\n");
	  saybb(QCIMIE(this_player(), PL_MIA)+" g�o�no tupi�c podbiega do "
		+this_object()->short(PL_DOP)+", ten odwraca sie gwa�townie w "
			+this_player()->koncowka("jego", "jej")+" stron�, jednak"
		+" najwyra�niej ptak jest ju� przyzwyczajony do takich dziwnych"
		+" zachowa�, gdy� po chwili, zupe�nie ignoruj�c "
		+QCIMIE(this_player(), PL_BIE)+" wraca on do swoich zaj��.\n");
	  return 1;
	} else return 0;
    }
}

int emoty(string str)
{
  object golab;
  if(!str)
    return 0;
  if(!parse_command(lower_case(str), environment(this_player()), "%o:"+PL_BIE, golab))
    return 0;
  if(golab != this_object())
    return 0;
  string verb = query_verb();
  string *verb_array = ({"kopnij","poca�uj","pociesz","pog�ad�","pog�aszcz","przytul","szturchnij"});
  if(verb == "opluj")
    {
      int i = random(10);
      if(i > 7)
	{
	  return 0;
	}
      else
	{
	  write("Pr�bujesz oplu� go��bia, ale ten w ostatniej chwili uskakuje przed tob�.\n");
	  saybb(QCIMIE(this_player(), PL_MIA)+" pr�buje oplu� go��bia, ale ten w ostatniej chwili odskakuje od "+this_player()->koncowka("niego", "niej")+".\n");
	  return 1;
	}
    }
  if(member_array(verb, verb_array) != -1)
    {
      write("Pr�bujesz zbli�y� si� do "+this_object()->short(PL_DOP)+", ale ten natychmiast od ciebie odskakuje.\n");
      return 1;
    }
}

string zaatakowany()
{
  set_alarm(0.1, 0.0, &write("Pr�bujesz zaatakowa� "+
			     this_object()->short(PL_BIE)+", lecz ten"
			     +" w ostatniej chwili wzbija si� w powietrze"
			     +" i przysiada na pobliskim budynku.\n"));
  saybb(capitalize(this_object()->short(PL_MIA))+" w ostatniej chwili wzbija si� w powietrze i"
	+" przysiada na pobliskim budynku.\n");
  return "";
}

void remove_me()
{
  if(environment(this_object())->dzien_noc())
    this_object()->remove_object();
}

public string
query_auto_load()
{
    return 0;
}
