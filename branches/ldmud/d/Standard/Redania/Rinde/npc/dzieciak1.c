
/* Dzieciak z Rinde        *
 * Made by Avard 14.06.06  *
 * Opis by Tinardan        */

#pragma unique

#include <std.h>
#include <ss_types.h>
#include <macros.h>
#include <stdproperties.h>
#include <pl.h>
#include "dir.h"

inherit RINDE_NPC_STD;

int czy_walka();

int walka = 0;

void
create_rinde_humanoid()
{
     set_living_name("dzieciak1");
     ustaw_odmiane_rasy("dziewczynka");
     set_gender(G_FEMALE);
     dodaj_nazwy("dziecko");
     dodaj_przym("ma^ly", "mali");
     dodaj_przym("umorusany", "umorusani");

     set_long("Dziecko ma umorusan^a buzi^e i ubrudzone r^aczki, a sukienka "+
         "pokryta jest warstw^a kurzu, spod kt^orego wyzieraj^a liczne "+
         "dziury i ^laty. Jednak ciemne w^losy dziewczynki spi^ete s^a w "+
         "schludne warkoczyki, a jej oczy b^lyszcz^a weso^lo, gdy obrzuca "+
         "przechodni^ow radosnym spojrzeniem.\n");

     set_stats(({10, 10, 10, 10, 10}));
     add_prop(CONT_I_WEIGHT, 35000);
     add_prop(CONT_I_HEIGHT, 135);
     add_prop(CONT_I_VOLUME, 35000);
     add_prop(LIVE_I_NEVERKNOWN, 1);

    set_act_time(10);
	add_act("zachichocz radosnie");
	add_act("spojrzyj wesolo na mezczyzne");
	add_act("spojrzyj z usmiechem na kobiete");
    add_act("usmiech do kobiety");
    add_act("usmiech do mezczyzny");
    add_act("emote ociera twarz brudn^a r^aczk^a.");
    add_act("emote wyciera brudne r^aczki o i tak ju^z sfatygowan^a "+
        "sukienk^e.");

	set_cact_time(10);
	add_cact("emote piszczy i zakrywa g^low^e r^ekami.");
	add_cact("krzyknij Maaaamo!");



    add_armour("/d/Standard/Redania/Rinde/przedmioty/ubrania/" +
        "koszulka_dla_dzieciaka4.c");
    add_armour("/d/Standard/Redania/Rinde/przedmioty/ubrania/" +
        "spodenki_dla_dzieciaka4.c");
    //add_armour(cos);
    //add_armour(cos);
    //clone_object(sdf)->move(npc);

    set_default_answer(VBFC_ME("default_answer"));
}

void
powiedz_gdy_jest(object player, string tekst)
{
	if (environment(this_object()) == environment(player))
	this_object()->command(tekst);
}

string
default_answer()
{
	set_alarm(2.0, 0.0, "powiedz_gdy_jest", this_player(),
    "emote rozdziawia buzi^e w niemym ge^scie niezrozumienia.");

    return "";
}

void
add_introduced(string imie_mia, string imie_bie)
{
    set_alarm(2.0, 0.0, "return_introduce", find_player(imie_mia));
}

void
return_introduce(object ob)
{
    command("powiedz ^Ladne imi^e!");
    command("zachichocz");
}


void
emote_hook(string emote, object wykonujacy)
{
    switch (emote)
    {
        case "kopnij":
        case "spoliczkuj": set_alarm(1.5, 0.0, "nienajlepszy", wykonujacy);
            break;

        case "opluj" : set_alarm(2.5, 0.0, "nienajlepszy", wykonujacy);
            break;

		case "poca^luj":
        {
            write("Brudna buzia dziewczynki oblewa si^e "+
                     "rumie^ncem.\n");
            saybb("Brudna buzia dziewczynki oblewa si^e "+
                            "rumie^ncem.\n");
        }
            break;

        case "przytul":
        {
            write("Dziewczynka wtula si^e ufnie w twoje rami^e.\n");
            saybb("Dziewczynka wtula si^e ufnie w "+
                "rami^e "+ QIMIE(this_player(),PL_DOP) +".\n");
        }
            break;

        case "pog^laszcz": set_alarm(2.5, 0.0, "malolepszy", wykonujacy);
            break;
    }
}


void
nienajlepszy(object wykonujacy)
{
    write("Wielkie niebieskie oczy dziewczynki zachodz^a ^lzami, a bu^xka "+
        "wykrzywia si^e w wyrazie zdumienia i b^olu.\n");
    saybb("Wielkie niebieskie oczy dziewczynki zachodz^a ^lzami, a bu^xka "+
        "wykrzywia si^e w wyrazie zdumienia i b^olu.\n");
}

void
attacked_by(object wrog)
{
    if(walka==0)
    {
	    write("Dziewczynka piszczy i zakrywa g^low^e r^ekami.\n");
        saybb("Dziewczynka piszczy i zakrywa g^low^e r^ekami.\n");

        set_alarm(5.0, 0.0, "czy_walka", 1);
        walka = 1;

        return ::attacked_by(wrog);
    }

    else if(walka == 1)
    {
        set_alarm(1.0, 0.0, "command", "rozplacz sie");
        set_alarm(10.0, 0.0, "czy_walka", 1);
        walka = 1;

        return ::attacked_by(wrog);
    }

}

int
czy_walka()
{
    if(!query_attack())
    {

        this_object()->command("emote rozgl^ada si^e ze ^lzami w oczach.");
        walka = 0;

        return 1;
    }
    else
        set_alarm(5.0, 0.0, "czy_walka", 1);

}
