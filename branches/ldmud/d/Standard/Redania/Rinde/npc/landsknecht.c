
/* Autor: Avard
   Opis : Kotra
   Data : 27.07.06
   Info : Straznik, landsknecht do Rinde */

#include <std.h>
#include <ss_types.h>
#include <macros.h>
#include <stdproperties.h>
#include <pl.h>
#include "dir.h"

inherit RINDE_NPC_STD;

void
create_rinde_humanoid()
{
     ustaw_odmiane_rasy("m^e^zczyzna");
     set_gender(G_MALE);
     dodaj_nazwy("straznik");
     dodaj_nazwy("landsknecht");
     dodaj_przym("wysoki","wysocy");
     dodaj_przym("zdeterminowany","zdeterminowani");

     set_long("Ten stra^znik miejski bardziej przypomina najemnego "+
         "landsknechta, ni^xli typowego stra^znika. Wysoki na sze^s^c "+
         "st^op ^zo^lnierz, odziany jest w niezwykle skomplikowany i "+
         "rzucaj^acy si^e w oczy str^oj. Spod srebrnego kirysu "+
         "landsknechta wystaje obszerna, bufiasta koszula, z d^lugimi i "+
         "jednocze^snie poci^etymi r^ekawami b^l^ekitno-z^lotego koloru. "+
         "Szeroki, bogato zdobiony pas oraz po^nczochy w ubarwieniu "+
         "identycznym co koszuli, r^o^zni si^e od owej tylko mocniejszym "+
         "stopniem przylegania do cia^la, chroni^a swojego posiadacza od "+
         "bioder a^z po same stopy, na kt^orych to landsknecht nosi proste "+
         "i p^laskie buty. No g^lowie stra^znik nosi p^laski beret, spod "+
         "kt^orego wylewaj^a si^e blond loki, z wetkni^etym we^n czarnym "+
         "pi^orkiem. Cz^e^s^c twarzy kt^ora nie jest zas^loni^eta ani "+
         "blond lokami, ani sumiastymi, r^ownie^z blond, w^asami, przejawia "+
         "pewne zdeterminowanie, widoczne g^l^ownie w ciemnych br^azowych "+
         "oczach.\n");

    set_stats (({ 170, 80, 120, 30, 80 }));
    set_skill(SS_DEFENCE, 50 + random(6));
    set_skill(SS_PARRY, 60 + random(8));
    set_skill(SS_WEP_SWORD, 80 + random(11));

     add_prop(CONT_I_WEIGHT, 80000);
     add_prop(CONT_I_HEIGHT, 180);
     add_prop(LIVE_I_NEVERKNOWN, 1);

    set_act_time(40);
    add_act("emote rozgl^ada si^e uwa^znie po otoczeniu.");
    add_act("emote mru^zy oczy i przez chwile skupia wzrok na jakim^s "+
        "oddalonym punkcie.");
    add_act("ziewnij skrycie");
    add_act("emote poprawia u^lo^zenie broni.");

    set_cchat_time(10);
    add_cchat("Ju^z jeste^s martw"+this_player()->koncowka("y","a")+"!");
    add_cchat("Wyda^l"+this_player()->koncowka("e^s","a^s")+" na siebie wyrok "+
        "^smierci!");
    add_cchat("Gi^n!");

    add_armour("/d/Standard/items/ubrania/berety/plaski_elastyczny_XLc.c");
    add_armour("/d/Standard/Redania/Rinde/przedmioty/zbroje/kirysy/srebrny_elegancki_XLc.c");
    //clone_object(sdf)->move(npc);

    add_ask(({"ratusz"}), VBFC_ME("pyt_o_ratusz"));
    add_ask(({"^swi^atynie"}), VBFC_ME("pyt_o_swiatynie"));
    add_ask(({"stra^znik^ow","stra^z"}), VBFC_ME("pyt_o_straz"));
    add_ask(({"garnizon"}), VBFC_ME("pyt_o_garnizon"));
    add_ask(({"burmistrza","Nevilla","nevilla"}),VBFC_ME("pyt_o_burmistrza"));
    set_default_answer(VBFC_ME("default_answer"));

 }

    void
    powiedz_gdy_jest(object player, string tekst)
    {
        if (environment(this_object()) == environment(player))
        this_object()->command(tekst);
    }

    string
    pyt_o_ratusz()
    {
        set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
        "'Stoisz przed nim.");
        set_alarm(1.8, 0.0, "powiedz_gdy_jest", this_player(),
        "emote u^smiecha si^e z politowaniem.");
        return "";
    }
    string
    pyt_o_swiatynie()
    {
        set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
            "'Jest w zachodniej cz^e^sci miasta.");
        return "";
    }
    string
    pyt_o_straz()
    {
        set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
            "'Zosta^lem naj^ety do stra^zy przez tutejszego "+
            "burmistrza.");
        set_alarm(2.0, 0.0, "powiedz_gdy_jest", this_player(),
            "'Ca^lkiem nie^xle p^laci, wi^ec nie narzekam.");

        return "";
    }
    string
    pyt_o_garnizon()
    {
        set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
            "'Garnizon jest we wschodniej cz^e^sci miasta.");
        set_alarm(2.0, 0.0, "powiedz_gdy_jest", this_player(),
            "'To dosy^c du^zy, murowany budynek, wej^scie jest "+
            "zaraz przy placu.");

        return "";
    }
    string
    pyt_o_burmistrza()
    {
        set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
            "'Naj^a^l mnie do stra^zy i nie^xle p^laci.");
        return "";
    }
    string
    default_answer()
    {
        set_alarm(2.0, 0.0, "powiedz_gdy_jest", this_player(),
        "powiedz do "+ this_player()->query_name(PL_BIE) +
        "Niestety nie moge ci pom^oc w tej kwestii.");
        return "";
    }


    void
    add_introduced(string imie_mia, string imie_bie)
    {
        set_alarm(2.0, 0.0, "return_introduce", find_player(imie_mia));
    }
    void
    return_introduce(object ob)
    {
    command("powiedz Mi^lo pozna^c.");
    }


    void
emote_hook(string emote, object wykonujacy)
{
    switch (emote)
    {
        case "kopnij": set_alarm(1.5, 0.0, "nienajlepszy", wykonujacy);
                    break;
        case "spoliczkuj": set_alarm(1.5, 0.0, "nienajlepszy", wykonujacy);
                    break;
        case "opluj" : set_alarm(2.5, 0.0, "nienajlepszy", wykonujacy);
                    break;
        case "poca^luj":
              {
              if(wykonujacy->query_gender() == 1)
                switch(random(2))
                 {
                 case 0:command("powiedz Mi^lo mi, ale ja mam "+
                     "^zon^e."); break;
                 case 1:command("usmiechnij sie lekko"); break;
                }
              else
                {
                   switch(random(2))
                   {
                   case 0:command("emote czerwienieje ze z^lo^sci."); break;
                   case 1:command("spojrzyj z obrzydzeniem na "+
                        OB_NAME(wykonujacy)); break;
                   }
                }

                break;
                }
        case "prychnij":
              {
                switch(random(1))
                 {
                 case 0:command("spojrzyj lekcewazaco "+
                     "na "+ OB_NAME(wykonujacy)); break;
                 }
                break;
               }
        case "przytul": set_alarm(2.5, 0.0, "malolepszy", wykonujacy);
                   break;
        case "pog^laszcz": set_alarm(2.5, 0.0, "malolepszy", wykonujacy);
                   break;
    }
}

void
malolepszy(object wykonujacy)
{

   switch(random(2))
   {
     case 0: command("powiedz Prosze sobie nie pozwala^c."); break;
     case 1: command("powiedz Nie powtarzaj tego."); break;

   }
}

void
nienajlepszy(object wykonujacy)
{
   switch(random(5))
   {
      case 0..3: command("kopnij "+OB_NAME(wykonujacy));
              command("powiedz Wyno^s si^e!");break;
      case 4: command("zabij "+OB_NAME(wykonujacy)); break;
   }
}

