inherit "/std/zwierze.c";
inherit "/std/act/action";
inherit "/std/act/trigaction.c";

#include <macros.h>
#include <stdproperties.h>
#include <money.h>
#include <pl.h>
#include <formulas.h>
#include <std.h>
#include <ss_types.h>
#include <wa_types.h>
#include <object_types.h>
#include <composite.h>
#include <filter_funs.h>

#include "dir.h"

void
create_zwierze()
{
    add_leftover("/std/leftover", "sk�ra", 1, 0, 1, 1, 5 + random(5), O_SKORY);

    ustaw_odmiane_rasy("kot");
    set_gender(G_MALE);

    set_long("Ten do�� du�y kot wygl�da niezwykle mi�o. Ruda sier�� " +
        "zwierz�cia przeplata si� w wielu miejscach z niezwykle bia�ym " +
        "futerkiem, kt�re w ogole nie jest zabrudzone czy nawet " +
        "przykurzone. Jego w�a�ciciel musi o niego dba�, gdy� kot " +
        "mruczy ci�gle z zadowoleniem i mru�y leniwie du�e ��te oczy. " +
        "D�ugi i gruby ogon co chwil� unosi si� nieznacznie, po czym " +
        "spokojnie opada szoruj�c po ziemi.\n");

    dodaj_przym("rudy", "rudzi");
    dodaj_przym("puszysty", "puszy�ci");

    set_act_time(30);
    add_act("emote mruczy cicho.");
    add_act("emote rozgl�da si� leniwie mru��c lekko oczy.");
    add_act("emote �asi si� o twoj� nog�.");
    add_act("emote miauczy cicho.");
    add_act("emote li�e swoj� �apk�.");
    add_act("emote z zainteresowaniem �ledzi jaki� szybko poruszaj�cy " +
        "si� punkt.");

    set_stats ( ({ 65, 31, 70, 25, 58 }) );

    set_skill(SS_DEFENCE, 10 + random(4));
    set_skill(SS_WEP_CLUB, 33 + random(10));
    set_skill(SS_PARRY, 10 + random(10));
    set_skill(SS_UNARM_COMBAT, 15 + random(5));

    set_attack_unarmed(0,   5,  5 , W_SLASH, 100, "pazurki");

    set_hitloc_unarmed(0, ({ 0, 0, 5 }), 20, "g�owa");
    set_hitloc_unarmed(1, ({ 0, 0, 5 }), 80, "brzuch");

    add_prop(LIVE_I_NEVERKNOWN, 1);
    add_prop(CONT_I_WEIGHT, 1000);
    add_prop(CONT_I_HEIGHT, 45);
}

void
emote_hook(string emote, object wykonujacy, string przyslowek)
{
    switch (emote)
    {
        case "kopnij":
        case "spoliczkuj":
        case "opluj":
	case "nadepnij":
	    set_alarm(1.0, 0.0, "zly", wykonujacy);
            break;

        case "szturchnij":
	    set_alarm(1.0, 0.0, "taki_sobie", wykonujacy);
            break;

        case "za�miej":
        case "poca�uj":
        case "przytul":
        case "pog�aszcz":
	    set_alarm(1.0, 0.0, "dobry", wykonujacy);
            break;
    }
}

void
zly(object kto)
{
    command("emote syczy cicho.");
    command("emote obna�a gro�nie ostre pazury.");
}

void
taki_sobie(object kto)
{
}

void
dobry(object kto)
{
    command("emote mruczy z zadowoleniem.");
    command("emote �asi si� o ciebie.");
}

void
attacked_by(object wrog)
{
    ::attacked_by(wrog);
}
