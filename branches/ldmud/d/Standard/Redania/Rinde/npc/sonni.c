
/* Autor: Avard
   Opis : Tinardan
   Data : 27.07.06
   Info : Sonni, spaceruje z Darla po Rinde. Kurier, jak bedzie Oxenfurt
          to moze jezdzic tam co jakis czas. */

#pragma unique

#include <std.h>
#include <ss_types.h>
#include <macros.h>
#include <stdproperties.h>
#include <pl.h>
#include <money.h>
#include "dir.h"

inherit RINDE_NPC_STD;

object darla;

void buzi_darla();
void przytulenie_darla();
void zle_darla();
/* To do emote_hook */
void pijemy();
int czy_walka();
int walka = 0;
int i;
int k;
int l;

void
create_rinde_humanoid()
{
    set_living_name("sonni");
    ustaw_odmiane_rasy("m^e^zczyzna");
    set_gender(G_MALE);
    ustaw_imie(({"sonni","sonniego","sonniemu","sonniego","sonnim",
        "sonnim"}), PL_MESKI_OS);
    dodaj_przym("kr^otkow^losy","kr^otkow^losi");
    dodaj_przym("ciemnooki","ciemnoocy");

    set_long("@@dlugasny@@");
    set_stats (({ 80, 60, 80, 40, 25, 100 }));
    add_prop(CONT_I_WEIGHT, 68000);
    add_prop(CONT_I_HEIGHT, 170);

    set_act_time(60);
    add_act("@@eventy@@");

    set_cact_time(10);
    add_cact("krzyknij Niech mi kto^s pomo^ze!");
    add_cact("emote pr^obuje zas^loni^c si^e r^ekoma.");

    add_armour(RINDE_UBRANIA+"koszule/blekitna_cerowana_Mc.c");
    add_armour(RINDE_UBRANIA+"spodnie/plocienne_granatowe_Mc.c");
    add_armour(RINDE_UBRANIA+"buty/brazowe_sznurowane_Mc.c");
    MONEY_MAKE_D(1)->move(this_object());
    MONEY_MAKE_G(20)->move(this_object());


    add_ask(({"ratusz"}), VBFC_ME("pyt_o_ratusz"));
    add_ask(({"^swi^atynie"}), VBFC_ME("pyt_o_swiatynie"));
    add_ask(({"prace"}), VBFC_ME("pyt_o_prace"));
    add_ask(({"garnizon"}), VBFC_ME("pyt_o_garnizon"));
    add_ask(({"burmistrza","Nevilla","nevilla"}),VBFC_ME("pyt_o_burmistrza"));
    add_ask(({"darl^e","Darl^e","drobn^a kobiete",
        "drobn^a zielonook^a kobiete","zielonook^a kobiete"}),
        VBFC_ME("pyt_o_darle"));
    add_ask(({"karczm^e","baszt^e","garnizonow^a"}),VBFC_ME("pyt_o_karczme"));
    add_ask(({"sonniego","Sonniego","kr^otkow^losego m^e^zczyzn^e",
        "kr^otkow^losego ciemnookiego","ciemnookiego m^e^zczyzn^e"}),
        VBFC_ME("pyt_o_sonniego"));
    set_default_answer(VBFC_ME("default_answer"));

  //  set_alarm_every_hour("co_godzine"); FIXME, u Darli tez

}/*
int
co_godzine()
{
    if (MT_GODZINA == 17)
    {
        set_alarm(0.1,0.0, "do_darli");
    }
    if (MT_GODZINA == 22)
    {
        set_alarm(0.1,0.0, "do_karczmy");
    }
    if (MT_GODZINA == 23)
    {
        set_alarm(0.1,0.0, "do_domu");
    }
}

*/
void
powiedz_gdy_jest(object player, string tekst)
{
    if (environment(this_object()) == environment(player))
        this_object()->command(tekst);
}

string
pyt_o_ratusz()
{
    if(!query_attack())
    {
        set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
        "'Jest w centrum miasta, nie spos^ob nie trafi^c.");
    }
    else
    {
        set_alarm(0.5,0.0,"powiedz_gdy_jest", TP, "powiedz Zostaw mnie w "+
            "spokoju!");
    }
    return "";
}
string
pyt_o_swiatynie()
{
    if(!query_attack())
    {
        set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
        "'To du^za budowla z wie^z^a na zachodzie miasta.");
    }
    else
    {
        set_alarm(0.5,0.0,"powiedz_gdy_jest", TP, "powiedz Zostaw mnie w "+
            "spokoju!");
    }
    return "";
}
string
pyt_o_prace()
{
    if(!query_attack())
    {
        set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
        "'Neville zatrudnia mnie jako go^nca, m^ecz^aca praca, "+
        "ale kto^s musi, prawda?");
    }
    else
    {
        set_alarm(0.5,0.0,"powiedz_gdy_jest", TP, "powiedz Zostaw mnie w "+
            "spokoju!");
    }
    return "";
}
string
pyt_o_garnizon()
{
    if(!query_attack())
    {
        set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
        "'Garnizon jest we wschodniej cz^e^sci miasta.");
    }
    else
    {
        set_alarm(0.5,0.0,"powiedz_gdy_jest", TP, "powiedz Zostaw mnie w "+
            "spokoju!");
    }
    return "";
}
string
pyt_o_burmistrza()
{
    if(!query_attack())
    {
        set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
        "'Neville nie jest z^lym burmistrzem, mo^zesz "+
        "go spotka^c w ratuszu.");
    }
    else
    {
        set_alarm(0.5,0.0,"powiedz_gdy_jest", TP, "powiedz Zostaw mnie w "+
            "spokoju!");
    }
    return "";
}
string
pyt_o_darle()
{
    if(!query_attack())
    {
        set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
        "'To najwa^zniejsza osoba w moim ^zyciu.");
        set_alarm(2.5, 0.0, "powiedz_gdy_jest", this_player(),
        "powiedz Jest wspania^la, nie wiem co bym zrobi^l gdyby "+
        "jej zabrak^lo.");
        set_alarm(4.5, 0.0, "powiedz_gdy_jest", this_player(),
        "powiedz Mam nadzieje, ^ze ju^z nied^lugo si^e pobierzemy.");
        set_alarm(6.5, 0.0, "powiedz_gdy_jest", this_player(),
        "spojrzyj z miloscia na darle");
    }
    else
    {
        set_alarm(0.5,0.0,"powiedz_gdy_jest", TP, "powiedz Zostaw mnie w "+
            "spokoju!");
    }
    return "";
}
string
default_answer()
{
    set_alarm(2.0, 0.0, "powiedz_gdy_jest", this_player(),
        "rozloz rece bezradnie");
    return "";
}
string
pyt_o_karczme()
{
    if(!query_attack())
    {
        set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
        "'Je^sli chodzi o karczmy to polecam Baszt^e, jest "+
        "na p^o^lnocy miasta, zaraz obok stajni.");
    }
    else
    {
         set_alarm(0.5,0.0,"powiedz_gdy_jest", TP, "powiedz Zostaw mnie w "+
            "spokoju!");
    }
    return "";
}
string
pyt_o_sonniego()
{
    if(!query_attack())
    {
        set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
        "'To ja, o co chodzi?");
        return "";
    }
    else
    {
        set_alarm(0.5,0.0,"powiedz_gdy_jest", TP, "powiedz Zostaw mnie w "+
            "spokoju!");
        return "";
    }
}


void
add_introduced(string imie_mia, string imie_bie)
{
    set_alarm(2.0, 0.0, "return_introduce", find_player(imie_mia));
}
void
return_introduce(object ob)
{
    command("przedstaw sie "+ OB_NAME(ob));
}

void
emote_hook(string emote, object wykonujacy)
{
    switch (emote)
    {
        case "kopnij": set_alarm(1.5, 0.0, "nienajlepszy", wykonujacy);
                    break;
        case "spoliczkuj": set_alarm(1.5, 0.0, "nienajlepszy", wykonujacy);
                    break;
        case "opluj" : set_alarm(2.5, 0.0, "nienajlepszy", wykonujacy);
                    break;
        case "poca^luj":
              {
              if(wykonujacy->query_gender() == 1)
                switch(random(1))
                 {
                 case 0: buzi_darla(); break;
                }
              else
                {
                   switch(random(1))
                   {
                   case 0:command("'Uznam to za przyjacielski "+
                       "gest."); break;
                   }
                }

                break;
                }
        case "prychnij":
              {
                switch(random(1))
                 {
                 case 0:command("spojrzyj lekcewazaco "+
                     "na "+ OB_NAME(wykonujacy)); break;
                 }
                break;
               }
        case "przytul": set_alarm(0.5, 0.0, "malolepszy", wykonujacy);
                   break;
    }
}

void
malolepszy(object wykonujacy)
{
    switch(random(1))
    {
        case 0: przytulenie_darla(); break;
    }
}

void
nienajlepszy(object wykonujacy)
{
  switch(random(3))
    {
    case 0:
      zle_darla();
      break;
    case 1:
      command("emote przewraca oczami i odchodzi kilka "+
          "krok^ow.");
      break;
    case 2:
      command("spojrzyj z poblazaniem na "+wykonujacy->query_real_name());
      break;
    }
}
string
dlugasny()
{
    string str;

    str = "Nie ma w nim nic szczeg^olnego, mo^ze tylko to, ^ze jest "+
         "do^s^c wysoki, a jego ramiona s^a wyprostowane i pewne, oraz ^ze "+
         "ubranie wygl^ada schludnie i skromnie, ale nie biednie, a ciemne "+
         "oczy b^lyszcz^a na opalonej twarzy niczym dwie spokojne, pe^lne "+
         "ciep^la latarnie. Krok ma r^owny i spr^e^zysty, ale nie kojarzy "+
         "si^e ze sztywnym kroczeniem wojskowych ani rozko^lysanym chodem "+
         "marynarzy. Ciemna sk^ora doskonale harmonizuje z br^azem jego "+
         "oczu i ciemnymi w^losami, kt^orych ko^nc^owki s^a lekko "+
         "sp^lowia^le od s^lo^nca i po^lyskuj^a z^lotem gdy padnie na nie ";

    if(environment(this_object())->pora_dnia() == MT_WCZESNY_RANEK ||
       environment(this_object())->pora_dnia() == MT_RANEK ||
       environment(this_object())->pora_dnia() == MT_POLUDNIE ||
       environment(this_object())->pora_dnia() == MT_POPOLUDNIE)
    {
        str += "jego promie^n. ";
    }
    else
    {
        str += "ksi^e^zycowy promie^n. ";
    }
    if(present("darla", environment())&&!query_attack())
    {
        str += "Jedn^a r^ek^a obejmuje id^ac^a ko^lo niego, drobn^a "+
            "dziewczyn^e. U^smiecha si^e lekko ciep^lym u^smiechem, "+
            "gdy ta k^ladzie g^low^e na jego ramieniu.\n";
    }
    else
    {
        str += "\n";
    }
    return str;
}

buzi_darla()
{
    if(present("darla", environment()))
    {
        this_object()->command("':marszcz�c brwi i odsuwaj�c si^e "+
            "znacz^aco: Bardzo prosz^e tego nie robi^c.");
    }
    else
    {
        this_object()->command("':u^smiechaj� si^e ^lagodnie: "+
            "Przykro mi, ale ja mam ju^z ukochan^a.");
    }
}

przytulenie_darla()
{
    if(present("darla", environment()))
    {
        this_player()->catch_msg("Kiedy go obejmujesz, masz "+
            "wra^zenie, ^ze w og^ole go nie ma obok ciebie. Spogl^adaj^a "+
            "na siebie z "+QIMIE(find_living("darla"),PL_NAR)+" i "+
            "u^smiechaj^a si^e tajemniczo.\n");
        saybb(QCIMIE(this_object(),PL_MIA)+" i "+
            ""+QIMIE(find_living("darla"),PL_MIA)+" u^smiechaj^a si^e "+
            "do siebie tajemniczo.\n");
    }
    else
    {
        this_object()->command("emote u^smiecha si^e ^lagodnie, ale jego "+
            "oczy s^a wpatrzone gdzie^s w dal.");
    }
}

zle_darla()
{
  if(present("darla", environment()))
    {
      tell_roombb(environment(), QCIMIE(this_object(),PL_MIA)+" os^lania "+
          QIMIE(find_living("darla"),PL_BIE)+" obronn^a r^ek^a.\n");
    }
    else
    {
        this_object()->command("emote odsuwa si^e stanowczo.");
    }
}

void
attacked_by(object wrog)
{
    if(walka==0)
    {
      if(present("darla", environment()))
        {
    this_player()->catch_msg(QCIMIE(this_object(),PL_MIA)+" odpycha "+
        ""+QIMIE(find_living("darla"),PL_BIE)+" jak najdalej i staje "+
        "pomi^edzy tob^a, a ni^a.\n");
    saybb(QCIMIE(this_object(),PL_MIA)+" odpycha "+
        ""+QIMIE(find_living("darla"),PL_BIE)+" jak najdalej i staje "+
        "pomi^edzy "+QIMIE(this_player(),PL_NAR)+", a ni^a.\n");
    command("krzyknij Na pomoc, morduj^a!");
    set_alarm(5.0, 0.0, "czy_walka", 1);
    walka = 1;
        }
        else
        {
    command("krzyknij Na pomoc, morduj^a!");
    set_alarm(10.0, 0.0, "czy_walka", 1);

    walka = 1;
        }
    return ::attacked_by(wrog);
    }

    else
    if(walka == 1)
    {
      return ::attacked_by(wrog);
    }

}

int
czy_walka()
{
    if(!query_attack())
    {

       this_object()->command("sapnij");

       walka = 0;
       return 1;
    }
    else
    {
       set_alarm(5.0, 0.0, "czy_walka", 1);
    }

}

string
eventy()
{
    if(present("darla", environment()))
    {
        switch(random(5))
        {
            case 0: tell_roombb(environment(), QCIMIE(this_object(),PL_MIA)+
                " i "+QIMIE(find_living("darla"),PL_MIA)+" spogl^adaj^a na "+
                "siebie czule.\n");break;
            case 1: tell_roombb(environment(), QCIMIE(this_object(),PL_MIA)+
                " i "+QIMIE(find_living("darla"),PL_MIA)+" ^smiej^a si^e "+
                "razem z jakiego^s sobie tylko wiadomego dowcipu.\n");break;
            case 2: tell_roombb(environment(), QCIMIE(this_object(),PL_MIA)+
                " pochyla si^e i szepcze "+QIMIE(find_living("darla"),PL_CEL)+
                " co^s do ucha. Ta rumieni si^e.\n");break;
            case 3: tell_roombb(environment(), "D^lonie "+
                ""+QIMIE(this_object(),PL_DOP)+" i "+
                ""+QIMIE(find_living("darla"),PL_DOP)+" splataj^a si^e w "+
                "ciasnym u^scisku.\n");break;
            case 4: tell_roombb(environment(), QCIMIE(this_object(),PL_MIA)+
                " i "+QIMIE(find_living("darla"),PL_MIA)+" u^smiechaj^a si^e "+
                "do siebie ciep^lo.\n");break;
        }
    }
    else
    {
        if(environment(this_object())->pora_dnia() == MT_WIECZOR ||
           environment(this_object())->pora_dnia() == MT_POZNY_WIECZOR ||
           environment(this_object())->pora_dnia() == MT_NOC)
        {
        switch(random(5))
            {
            case 0: command("emote pogwizduje cicho jak^a^s znan^a "+
                "melodyjk^e.");break;
            case 1: command("emote u^smiecha si^e do siebie.");break;
            case 2: command("'Pi^ekna noc, "+
                "prawda?");break;
            case 3: command("'Ech, gdyby nie praca, to ja i "+
                "Darla...");break;
            case 4: command("spojrzyj tesknie");break;
            }
        }
        else
        {
            switch(random(5))
            {
            case 0: command("emote pogwizduje cicho jak^a^s znan^a "+
                "melodyjk^e.");break;
            case 1: command("emote u^smiecha si^e do siebie.");break;
            case 2: command("'Pi^ekny dzi^s dzie^n mamy, "+
                "nieprawda^z?");break;
            case 3: command("'Ech, gdyby nie praca, to ja i "+
                "Darla...");break;
            case 4: command("spojrzyj tesknie");break;
            }
        }
    }
}/*
void
do_darli()
{
    if(query_attack())
    {
        set_alarm(10.0,0.0,"do_darli");
        return;
    }
    else
    {
        int i = 0;
        mixed droga=znajdz_sciezke(environment(this_object()),
            ZACHOD_LOKACJE + "ulica8");
        this_object()->command(droga[i]);
        if(i==sizeof(droga)-1)
        {
            set_alarm(300.0,0.0,"zerujemy");
            return;
        }
        else
        {
            i++;
            set_alarm(15.0,0.0,"do_darli");
            return;
        }
    }
}
void
zerujemy()
{
    int i = 0;
    int k = 0;
    int l = 0;
}
void
do_karczmy()
{
    if(query_attack() || find_living("darla")->query_attack())
    {
        set_alarm(10.0,0.0,"do_karczmy");
        return;
    }
    else
    {
        int k = 0;
        mixed droga2=znajdz_sciezke(environment(this_object()),
            POLNOC + "Karczma/lokacje/sala");
        command(droga2[k]);
        if(k==sizeof(droga2)-1)
        {
            set_alarm(5.0,0.0,&pijemy());
            set_alarm(300.0,0.0,"zerujemy");
            return;
        }
        else
        {
            k++;
            set_alarm(15.0,0.0,"do_karczmy");
            return;
        }
    }
}

void
pijemy()
{
    if(query_attack())
    {
        set_alarm(5.0,0.0,"pijemy");
    }
    else
    {
        set_alarm(0.0,0.0,"command","usiadz przy barze");
        MONEY_MAKE_G(5)->move(this_object());
        set_alarm(1.0,0.0,"command","zamow jasne piwo");
        set_alarm(2.0,0.0,"command","chwyc kufel");
        set_alarm(3.0,0.0,"pijemy2");
    }
}
void
do_domu()
{
    command("wstan");
    if(query_attack())
    {
        set_alarm(5.0,0.0,"do_domu");
        return;
    }
    else
    {
        int l = 0;
        mixed droga3=znajdz_sciezke(environment(this_object()),
            POLNOC_LOKACJE + "ulica4");
        command(droga3[l]);
        if(l==sizeof(droga3)-1)
        {
            set_alarm(5.0,0.0,"destroooy");
            set_alarm(300.0,0.0,"zerujemy");
            return;
        }
        else
        {
            k++;
            set_alarm(15.0,0.0,"do_domu");
            return;
        }
    }
}
void
destroooy()
{
    tell_roombb(environment(),QCIMIE(TO, PL_MIA)+" wchodzi "+
        "do jednej z kamienic.\n");
    remove_object();
}

void
pijemy2()
{
    object kufel = present("kufel", TO);
    if(!query_attack())
    {
        set_alarm(5.0,0.0,"command","napij sie z kufla");
        if(kufel->query_ilosc_plynu() > 0)
        {
            set_alarm(60.0,0.0,"pijemy2");
        }
        else
        {
            set_alarm(1.0,0.0,"command","odloz kufel");
            set_alarm(60.0,0.0,"do_domu");
        }
    }
    else
    {
        set_alarm(1.0,0.0,"command","odloz kufel");
        set_alarm(5.0,0.0,"pijemy");
    }
}
*/
