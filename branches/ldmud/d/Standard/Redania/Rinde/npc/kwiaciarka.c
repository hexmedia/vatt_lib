/* Kwiaciarka chodzaca po placu Rinde w dzien i sprzedajaca kwiaty oczywiscie ;p
 * W wiekszosci opis by Tinardan, reszta Lil.
 */

#pragma unique
#pragma strict types

#include <std.h>
#include <composite.h>
#include <formulas.h>
#include <stdproperties.h>
#include <macros.h>
#include <mudtime.h>
#include <ss_types.h>
#include <money.h>
#include <pl.h>
#include <object_types.h>
#include "dir.h"

inherit RINDE_NPC_STD;
inherit "/lib/sklepikarz";

int czy_walka();
int walka = 0;
string dlugasny();
object kosz;
string zalezne_od_plci();

void
create_rinde_humanoid()
{
    dodaj_przym("ho�y","hozi");
    dodaj_przym("rumianolicy","rumianolici");

    set_long("@@dlugasny@@");
    ustaw_odmiane_rasy(PL_KOBIETA);
    set_gender(G_FEMALE);

    add_prop(CONT_I_WEIGHT, 52000);
    add_prop(CONT_I_HEIGHT, 173);
    add_prop(NPC_I_NO_RUN_AWAY, 1);
    add_prop(LIVE_I_NEVERKNOWN, 1);

    set_stats(({ 19, 75, 35, 50, 37}));
    set_skill(SS_DEFENCE, 45 + random(15));
    set_skill(SS_TRADING, 35 + random(15));
    set_skill(SS_AWARENESS, 40);
    add_armour(RINDE_UBRANIA + "sukienka_lniana_prosta.c");
    add_armour(RINDE_UBRANIA + "szal_snieznobialy_dlugi.c");

    config_default_sklepikarz();
    set_co_sprzedajemy(O_KWIATY);
	set_money_greed_buy(208);
	set_money_greed_sell(1277);
	set_money_greed_change(120);

    kosz = clone_object(RINDE+"przedmioty/kosz_kwiaciarki");
    kosz->move(this_object());
    set_store_room(kosz);

    add_ask(({"kwiaty", "kwiatki", "r�e"}),VBFC_ME("pyt_o_kwiatki"));
    set_default_answer(VBFC_ME("default_answer"));

    set_chat_time(12);
    add_chat("Pi�kne kwiaty!");
    add_chat("Kwiaty w ka�dym kolorze, niedrogo!");
    add_chat("Kup kwiaty ukochanej osobie!");

    set_cchat_time(5);
    add_cchat("Pomocy!");
    add_cchat("Prosz�! Przesta�!");
    add_cchat("Stra�e! Na pomoc!");

    set_act_time(10);
    add_act("'Pi�kne kwiaty, najr�niejsze, "+
            "najpi�kniejsze! Tanio!");
    add_act("'Kwiaty pachn�ce, �wie�utkie, kolorowe!");
    add_act("krzyknij Kwiaty pachn�ce, �wie�utkie, kolorowe!");
    add_act("emote przechadza si� spokojnym krokiem zachwalaj�c "+
            "sw�j towar.");
    add_act(VBFC_ME("zalezne_od_plci"));
    add_act("emota nuci cicho skoczn� melodyjk�");
    add_act("usmiechnij sie zachecajaco");
    add_act("krzyknij Kwiaty pachn�ce, �wie�utkie, kolorowe!");
    add_act("westchnij cicho");
    add_act("emote strzepuje z siebie p�atki kwiat�w i fragmenty li�ci.");
    add_act("emote poprawia u�o�enie kwiat�w w trzymanym koszu.");
    add_act("emote wr�cza kwiat jednemu z przechodni�w dostaj�c w "+
            "zamian gar�� groszy.");

//    set_store_room(RINDE+"npc/kwiaciarka.c");
    setuid();
    seteuid(getuid());

    set_random_move(100);
    set_monster_home(CENTRUM_LOKACJE + "placw");
    set_restrain_path( ({ POLNOC_LOKACJE + "ulica1", ZACHOD_LOKACJE + "ulica5",
	    CENTRUM_LOKACJE + "placnw", CENTRUM_LOKACJE + "placw",
	    CENTRUM_LOKACJE + "placsw", ZACHOD_LOKACJE + "ulica1",
	    CENTRUM_LOKACJE + "placs", POLUDNIE_LOKACJE + "ulica1" }) );

    set_alarm_every_hour("co_godzine");
}

void
start_me()
{
    command("chwyc kosz");
}

string
pyt_o_kwiatki()
{
    set_alarm(1.0, 0.0, "command_present", this_player(),
        "powiedz do " + this_player()->query_name(PL_DOP) +
	" Kwiatki, kwiatuszki? Taaak! Mam ich ca�y koszyk, kupisz jaki?");
    return "";
}

string
default_answer()
{
     switch(random(10))
     {
     case 0: set_alarm(0.5, 0.0, "command_present", this_player(),
             "powiedz do "+ this_player()->query_name(PL_DOP) +
                  " Nie wiem...Ale mo�e kwiatka?");
	     break;
     case 1: set_alarm(0.5, 0.0, "command_present", this_player(),
             "rozloz rece");
             break;
     case 2: set_alarm(1.5, 0.0, "command_present", this_player(),
             "hmm");
             break;
     case 3: set_alarm(0.5, 0.0, "command_present", this_player(),
             "popatrz z usmiechem na "+this_player()->query_imie(PL_DOP));
             break;
     case 4: set_alarm(0.5, 0.0, "command_present", this_player(),
             "zachichocz cicho");
             break;
     case 5: set_alarm(0.5, 0.0, "command_present", this_player(),
             "powiedz do "+ this_player()->query_name(PL_DOP) +
                  " Kup prosz� kwiatka.");
             break;
     case 6..9: set_alarm(0.5, 0.0, "command_present", this_player(),
             "powiedz do "+ this_player()->query_name(PL_DOP) +
             " Ja nic tam nie wiem. Ale mo�e kupi "+
	     this_player()->koncowka("pan","pani")+" kwiatka?");
	     break;
     }

  return "";
}

int
co_godzine()
{
    if(environment(this_object())->pora_roku() == MT_LATO||
       environment(this_object())->pora_roku() == MT_WIOSNA)
    {
        if (MT_GODZINA >= 23)
	    set_alarm(4.0,0.0, "destroooy");
    }
    else
    {
	if (MT_GODZINA >= 19)
	    set_alarm(3.0,0.0, "destroooy");
    }
}

void
destroooy()
{
    //tu zmienilem saybb na tell_roombb, bo saybb korzysta z this_playera
    tell_roombb(environment(), capitalize(TO->short(PL_MIA))+" wchodzi "+
          "do jednej z kamienic.\n");
	remove_object();
}


string dlugasny()
{
    string str;
    str="Kiedy si� spogl�da na t� dziewczyn�, zdaje si�, "+
        "�e ogromna energia p�yn�ca w jej �y�ach wraz z krwi�, "+
	"tryska z niej ca�ej, przez sk�r�, przy ka�dym "+
	"u�miechu, w spojrzeniu wielkich ciemnoniebieskich oczu. ";

    if(environment(this_object())->pora_roku() == MT_LATO||
       environment(this_object())->pora_roku() == MT_WIOSNA)
    str+="Bosonoga, z rozwianym w�osem";
    else
    str+="Z rozwianym w�osem";

   /* str+=" jasnym niczym s�oma, w rozche�stanej, falbaniastej "+
         "sukience";*/
     str+=" jasnym niczym s�oma, w prostej lnianej "+
         "sukience";

    if(environment(this_object())->pora_roku() == MT_ZIMA ||
       environment(this_object())->pora_roku() == MT_JESIEN)
    str+=" oraz szalem narzuconym na kr�g�e ramiona";

    str+=" biega po placu w t� i z powrotem, zach�caj�c do "+
         "kupna kwiat�w u�o�onych w sporym koszu.";


    str+=" Kwiaty s� �wie�e ";

    if(environment(this_object())->pora_roku() == MT_ZIMA)
    str+="cho� sprawiaj� wra�enie przywi�d�ych i w�t�ych";
    else
    str+="i barwne";

    str+="; przesypuj� si� kolorow� fontann� przez brzegi "+
         "wiklinowego kosza. Dziewczyna rozdaje naoko�o "+
	 "u�miechy i zach�ca do kupna, zachwalaj�c sw�j towar.\n";

    return str;
}

string zalezne_od_plci()
{
    string str;

    str = "";

   if(filter(all_inventory(environment()), &living())[0]->query_gender()==G_MALE)
   {
     switch(random(4))
     {
       case 0: str+="':przymilnym g�osem: Kup pan r�� "+
                    "dla swej lubej, czerwona r�a od serca, "+
		    "kobieta na pewno nie pogardzi takim podarunkiem.";
	       break;
       case 1: str+="':u�miechaj�c si� szeroko: Mam tu tyle "+
                    "kwiat�w, a �adnego m�czyzny. Mo�e kupi�by pan "+
		    "ma�y bukiecik?"; break;
       case 2: str+="':szeptem: Bia�e r�e dla lic jasnych, chabry "+
                    "dla oczu jak niebo, stokrotki dla �ez przelanych, "+
		    "a r�e czerwone dla ust! W kwiatach tkwi kobieca "+
		    "dusza. Kup pan jeden kwiatek."; break;
       case 3: str+="':wyci�gaj�c kwiat i dotykaj�c nim ciebie lekko po "+
                    "policzku: Ten go�dzik pachnie pi�kniej ni� "+
		    "marzenia o zmierzchu. Dziewczynie by si� spodoba�.";
	       break;
    }
   }

   else
   {
     switch(random(6))
     {
       case 0..1: str+="':wyci�gaj� r�k� z kwiatem: Ta r�a "+
                    "czerwona jest jak pani pi�kne usteczka. Niech pani "+
		    "kupi, a b�dzie z ni� pani do twarzy.";
	            break;
       case 2..3: str+="'Mo�e pi�kny kwiat dla pi�knej damy?"; break;
       case 4: str+="':u�miechaj�c si� tajemniczo: Mawiaj�, "+
                    "�e rozsypane p�atki kwietne na �o�u o zakl�cie, "+
		    "by m�czyzn� do� sprowadzi�."; break;
       case 5: str+="'Kwiat mo�na przypi�� do sukni, wpi�� "+
                    "we w�osy... Kupi pani jeden?"; break;
    }
   }

   return str;
}

void
init()
{
    ::init();
    init_sklepikarz();
}

void
return_introduce(string imie)
{
    object osoba;
    osoba = present(imie, environment());

        command("dygnij przed " + osoba->query_nazwa(PL_MIE));
	return;

}

void
emote_hook(string emote, object wykonujacy)
{
    switch (emote)
    {
        case "kopnij": set_alarm(0.5, 0.0, "nienajlepszy", wykonujacy);
                    break;
        case "spoliczkuj": set_alarm(0.5, 0.0, "nienajlepszy", wykonujacy);
                    break;
        case "opluj" : set_alarm(0.5, 0.0, "nienajlepszy", wykonujacy);
                    break;
	case "poca�uj":
	      {
	      if(wykonujacy->query_gender() == G_FEMALE)
	        switch(random(2))
		 {
		 case 0:command("'Mo�e kupi pani kwiatka?");break;
                 case 1:command("popatrz z politowaniem na "+
		        wykonujacy->short(PL_DOP));
			break;
                }
	      else
	        {
		   switch(random(3))
		   {
		   case 0:command("emote u�miecha si� zalotnie i ca�uje "+
		          "ci� delikatnie w usta.");break;
                   case 1:write("Ho�a rumianolica kobieta przywiera "+
		          "wargami do twoich ust.\n");
			  saybb("Ho�a rumianolica kobieta przywiera "+
		          "wargami do ust "+wykonujacy->short(PL_DOP)+".\n");
			  break;
		   case 2:command("emote rozgl�da si� nerwowo w oko�o, "+
		          "a potem szepcze: Chod�my gdzie�, gdzie nikt "+
			  "nam nie b�dzie przeszkadza�.");break;
                   }
		}
		break;
                }
	case "przytul": set_alarm(0.5, 0.0, "malolepszy", wykonujacy);
                   break;
        case "pog�aszcz": set_alarm(0.5, 0.0, "malolepszy", wykonujacy);
                   break;
    }
}

void
malolepszy(object wykonujacy)
{

   switch(random(5))
   {
     case 0: command("usmiechnij sie wdziecznie"); break;
     case 1: command("usmiechnij sie nieznacznie"); break;
     case 2: command("hmm"); break;
     case 3: command("pokrec energicznie"); break;
     case 4: write("Ho�a rumianolica kobieta bierze jeden "+
             "kwiat z koszyka i zachwala jej urod�, namawiaj�c "+
	     "ci� do jej zakupu.\n");
	     saybb("Ho�a rumianolica kobieta bierze jeden "+
	     "kwiat z koszyka i zachwala jej urod�, namawiaj�c "+
	     wykonujacy->short(PL_BIE)+" do jej zakupu.\n");
	     break;
   }
}

void
nienajlepszy(object kto)
{
   switch(random(4))
   {
      case 0: command("emote ucieka na drugi koniec placu.");
	      break;
      case 1: set_alarm(0.5, 0.0, "command_present", kto,
              "prychnij");
	      break;
      case 2: command("emote uderza w g�o�ny lament, "+
                      "szukaj�c ratunku.");

	      break;
      case 3: command("emote krzywi si� gro�nie i odsuwa "+
               "si� od ciebie jak mo�e najdalej.");
	      break;
   }
}

void
attacked_by(object wrog)
{
    if(walka==0)
    {
    command("emote krzyczy rozpaczliwie, a w jej "+
            "oczach pojawiaj� si� �zy.");

    set_alarm(1.0, 0.0, "pomoc_nadchodzi", 1);
    set_alarm(5.0, 0.0, "czy_walka", 1);
    walka = 1;
    return ::attacked_by(wrog);
    }

    else if(walka == 1)
    {
      set_alarm(1.0, 0.0, "command", "emote niepewnie "+
                "zas�ania si� r�k� staraj�c si� unikn�� ciosu.");
      set_alarm(10.0, 0.0, "czy_walka", 1);
      walka = 1;
      return ::attacked_by(wrog);
    }

}

int
czy_walka()
{
    if(!query_attack())
    {

       this_object()->command("powiedz Ehh.");

       walka = 0;
       return 1;
    }
       else
       {
       set_alarm(5.0, 0.0, "czy_walka", 1);
       }
}

public void
do_die(object killer)
{
    command("westchnij cicho");
	command("emote upuszcza drogocenny koszyk, "+
	        "a kwiaty rozsypuj� si� na bruk niczym stado "+
                "kolorowych ptak�w.");
	command("odloz wszystko");

        ::do_die(killer);
}

/* sklepowe - np. taki hook mo�emy ustawi�: */
void
shop_hook_niczego_nie_skupujemy()
{
    command("powiedz Nie, ja niczego nie kupuj�, tylko kwiatki sprzedaj�.");
    notify_fail("");
}

public int
is_kwiaciarka_rinde()
{
    return 1;
}

public string
query_auto_load()
{
    return 0;
}
