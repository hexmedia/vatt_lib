//dnia 26.03.2008 dodalem prace z noszeniem workow. Vera.
#pragma unique

#include <ss_types.h>
#include <macros.h>
#include <stdproperties.h>
#include <money.h>
#include "dir.h"

inherit RINDE_NPC_STD;
inherit "/lib/sklepikarz.c";

//do ceny jest dodawany jeszcze random
#define ZAPLATA (10+random(25))
#define LISTA (PIANA+"lista_pracownika_poczty")

void
create_rinde_humanoid()
{
    ustaw_odmiane_rasy("m�czyzna");
    dodaj_nazwy("pracownik poczty");
    dodaj_nazwy("pracownik");

    dodaj_przym("wysoki", "wysocy");
    dodaj_przym("ciemnow^losy", "ciemnow^losi");

    set_long("Wysoki, ciemnow^losy m^e^zczyzna sprawnie porusza si^e mi^edzy stosami " +
             "list^ow i paczek. Jego m^etny wzrok ci^agle biega po okolicy, przeszukuj^ac " +
             "najmniejszy nawet zakamarek. Pracownik bezustannie poprawia swoj^a " +
             "szar^a, przybrudzon^a szat^e i ocenia jej stan, kt^ory nie jest ju^z najlepszy. " +
             "Na twarzy m^e^zczyzny nigdy nie pojawia si^e u^smiech, ale jego niebieskie " +
             "oczy wydaj^a si^e by^c ci^agle roze^smiane.\n");

    ustaw_imie(({"donis", "donisa", "donisowi", "donisa", "donisem",
	"donisie"}), PL_MESKI_OS);
    set_living_name("donis");
    set_surname("Nerrisent");
    set_title(", Pracownik poczty w Rinde");

    set_gender(G_MALE);

    add_prop(CONT_I_WEIGHT, 96000);//zeby mogl przyjac duzy worek
    add_prop(CONT_I_HEIGHT, 180);

    set_skill(SS_DEFENCE, 30);
    set_skill(SS_UNARM_COMBAT, 15);
    set_stats(({43, 40, 32, 60, 50}));

    set_chat_time(52);
    add_chat("Listy, listy, ci^agle te listy...");
    add_chat("Nasza poczta jest niezawodna!");

    add_chat("I zn^ow nie mam transportu, ech.");
    add_chat("Przyda^lby si^e kto^s to pomocy.");

    set_act_time(70);
    add_act("emote drapie si^e po g^lowie.");
    add_act("emote duma nad czym^s.");
    add_act("emote ziewa dyskretnie.");
    add_act("emote poprawia swoj^a szat^e.");
    add_act("emote mruczy pod nosem.");
    add_act("emote ogl^ada jedn^a z paczek.");

    set_cchat_time(18);
    add_cchat("Ja ci pok^a^z^e!");
    add_cchat("A niech ci^e!");

    set_cact_time(7);
    add_cact("emote ryczy w^sciekle.");
    add_cact("emote sapie gro^xnie.");

    add_armour("/d/Standard/items/ubrania/szaty/dluga_jasnobrazowa");

    set_default_answer(VBFC_ME("default_answer"));

    add_ask(({"zdrowie", "^zycie"}), VBFC_ME("pyt_o_zdrowie"));
    add_ask(({"zadanie","prace","transport","pomoc"}), VBFC_ME("pyt_o_prace"));
    add_ask(({"go^l^ebie", "go^l^ebia"}), VBFC_ME("pyt_o_golebia"));

    config_default_sklepikarz();
    //set_money_greed_buy(110);
    //set_money_greed_sell(110);

    set_money_greed_buy(210);
    set_money_greed_sell(511);

    set_co_skupujemy(0);

    set_store_room("/d/Standard/Redania/Rinde/Polnoc/lokacje/magazyn_golebi.c");

    add_prop(NPC_M_NO_ACCEPT_GIVE, 0);
}

void
usun_z_listy(string x)
{
    int i;
    string *arr;

    arr = explode(read_file(LISTA), "#");
    i = member_array(x, arr);

    if (i != -1)
        arr = exclude_array(arr, i, i+2);

    rm(LISTA);
    write_file(LISTA, implode(arr, "#") + "#");
}
string
pyt_o_prace()
{
    if(!query_attack())
    {
        int i = member_array(TP->query_real_name(),
            explode(read_file(LISTA), "#"));
      if (i != -1)
      {
        if(explode(read_file(LISTA), "#")[i+1] == "z_rinde")
            set_alarm(1.0, 0.0, "command_present", this_player(),
                   "powiedz do "+OB_NAME(TP) +
                   " Przecie^z da^lem ci ju^z worek! Nie m^ow, ^ze go zgubi^l"+
                  TP->koncowka("e^s","a^s","e^s")+"! To b^edzie ci^e s^lono "+
                  "kosztowa^c!");
        else if(explode(read_file(LISTA), "#")[i+1] == "z_piany")
            set_alarm(1.0, 0.0, "command_present", this_player(),
                   "powiedz do "+OB_NAME(TP) +
                   " Hmm...Ty chyba mia�"+TP->koncowka("e�","a�","e�")+
                    " mi przynie�� jaki� worek. Nie m^ow, ^ze go zgubi^l"+
                  TP->koncowka("e^s","a^s","e^s")+"! To b^edzie ci^e s^lono "+
                  "kosztowa^c!");
        set_alarm(2.0, 0.0, "command_present", this_player(),
                   "powiedz do "+OB_NAME(TP) +
                   " Eech...Zap^la^c mi za to i zapomnimy o tym. Trzy korony.");

        return "";
      }

        object worek = clone_object(RINDE_PRZEDMIOTY+"worek_poczta");
        command("powiedz do "+OB_NAME(TP) +
                   " ^Swietnie si^e sk^lada. W^la^snie szuka^lem kogo^s do "+
                  "pomocy. Trzymaj ten worek i zanie^s go czym pr^edzej na "+
                "poczt^e w Pianie.");
        worek->move(TP,1);//jako ze to praca dla zoltkow, nie robmy testow
        write_file(LISTA,TP->query_real_name()+ "#"+"z_rinde"+"#");
        TP->catch_msg(QCIMIE(TO,PL_MIA)+" wr^ecza ci "+worek->short(PL_BIE)+".\n");
        tell_room(ENV(TO),QCIMIE(TO,PL_MIA)+" wr^ecza jaki^s worek "+QIMIE(TP,PL_BIE)+".\n",({TP}));

    }
    else
    {
        set_alarm(0.5,0.0,"powiedz_gdy_jest", TP, "'Zostaw mnie!");
    }
    return "";
}

int
zaplac(string str)
{
    if (member_array(TP->query_real_name(),
            explode(read_file(LISTA), "#")) == -1)
    {
       command("'Nie, nie trzeba..");
      return 1;
    }

    if(!MONEY_ADD(TP,-720))
    {
      set_alarm(1.0, 0.0, "command_present", this_player(),
                   "powiedz do "+OB_NAME(TP) +
                   " Trzy korony powiedzia^lem!");
      return 1;
    }


    usun_z_listy(TP->query_real_name());
    write("P^lacisz pracownikowi poczty za zgubiony worek.\n");
    set_alarm(1.0, 0.0, "command_present", this_player(),
                   "powiedz do "+OB_NAME(TP) +
                   " Ehe, dobra. Ale ^zeby mi to by^lo ostatni raz!");
    return 1;
}
void
udalo_sie(object from, int ile,object ob)
{
    command("powiedz Dzi^eki! Prosz^e, oto twoja zap^lata.");
    from->catch_msg(QCIMIE(TO, PL_MIA)+" wyp^laca ci "+MONEY_TEXT(({ile,0,0}),PL_BIE)+".\n");
    tell_room(ENV(TO), QCIMIE(TO, PL_MIA)+" daje "+
        QIMIE(from, PL_CEL)+" jakie� monety.\n", from);

   ob->zniszcz();
}
void
nieudalo_sie(object ob,object kto)
{
    command("powiedz No c^o^z. Trudno. Mo^ze kto inny dostarczy te listy.");
    ob->zniszcz();
}
void
enter_inv(object ob, object from)
{
    ::enter_inv(ob, from);

    if(!interactive(from))
        return;

    if(ob->query_worek_z_piany())
    {
      int ile = ZAPLATA;
      if(MONEY_ADD(from,ile))
      {
        usun_z_listy(from->query_real_name());
        set_alarm(1.0,0.0,"udalo_sie",from,ile,ob);
      }
      return;
    }
    else if(ob->query_worek_z_rinde())
    {
        usun_z_listy(from->query_real_name());
        set_alarm(1.0,0.0,"nieudalo_sie",ob,from);
        return;
    }
    else
      set_alarm(1.0, 0.0, "command", "odloz " + OB_NAME(ob));

      return;
}


string
pyt_o_golebia()
{
    if(!query_attack())
    {
            set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
                "'Tak, sprzedaj^e go^l^ebie pocztowe.");
    }
    else
    {
        set_alarm(0.5,0.0,"powiedz_gdy_jest", TP, "'Zostaw mnie!");
    }
    return "";
}

string
pyt_o_zdrowie()
{
    if(!query_attack())
    {
            set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
                "'Dzi^ekuj^e, nie narzekam.");
    }
    else
    {
        set_alarm(0.5,0.0,"powiedz_gdy_jest", TP, "'Zostaw mnie!");
    }
    return "";
}

void
emote_hook(string emote, object wykonujacy, string przyslowek = 0)
{
    switch (emote)
    {
    case "uk^lo^n":
      command("kiwnij glowa");
      break;
    case "zata^ncz":
    case "u^sciskaj":
    case "szturchnij":
    case "pogr^o^x":
    case "po^laskocz":
    case "obejmij":
    case "klepnij":
    case "gr^o^x":
    case "b^lagaj":
    case "pog^laszcz":
    case "poca^luj":
    case "przytul":
      switch (random(3))
      {
      case 1:
        command("skrzyw sie");
        break;
      case 2:
        command("fuknij");
        break;
      case 3:
        command("zacisnij piesci");
      }
      break;
    case "spoliczkuj":
    case "kopnij":
    case "opluj":
      set_alarm(1.0, 0.0, "wkop_mu", wykonujacy);
      break;
    case "za^smiej":
      set_alarm(1.0, 0.0, "return_laugh", wykonujacy);
      break;
    case "przywitaj":
    case "powitaj":
      set_alarm(1.0, 0.0, "przywitaj", wykonujacy);
      break;
    }
}

void
wkop_mu(object kto)
{
    switch (random(3))
    {
    case 0:
      command("powiedz Jak ^smia^le^s to zrobi^c, chamie?!");
      break;
    case 1:
      command("powiedz Zr^ob tak jeszcze raz, a po^za^lujesz!");
      break;
    case 2:
      command("powiedz Spieprzaj!");
    }

    command("kopnij " + OB_NAME(kto));
}

void
return_laugh(object kto)
{
    switch(random(2))
    {
    case 0:
      command("powiedz ^Smiej si^e, ^smiej.");
      break;
    case 1:
      command("powiedz ostro Zamknij si^e.");
    }
}

void
return_introduce(string imie)
{
    object osoba;

    osoba = present(imie, environment());

    if (osoba)
      {
      command("powiedz Ach tak, mi^lo mi.");
      command("przedstaw sie");
      }

    else
      command("wzrusz ramionami");
}

void
przywitaj(object kto)
{
      command("powiedz Dzie^n dobry.");
}

void
shop_hook_niczego_nie_skupujemy()
{
    command("powiedz Ale� ja niczego nie skupuj�! Ja tylko go��bie "+
    	"pocztowe sprzedaj�!");
}

void
shop_hook_list_empty_store(string str)
{
    command("powiedz Obecnie nie mam ju� �adnych go��bi na sprzeda�.");
}

void
init()
{
    ::init();
    init_sklepikarz();
    add_action(zaplac,"zap�a�");
}

