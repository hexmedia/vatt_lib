/**
 * \file /d/Standard/Redania/Rinde/Poludnie/Biblioteka/npc/bibliotekarz.c
 *
 * Bibliotekarz z Rinde
 *
 * @author Krun - kod
 * @author Tabakista  - opisy
 * @date Grudzie� 2007
 */

#pragma unique

inherit RINDE_NPC_STD;

#include "dir.h"
#include <macros.h>
#include <stdproperties.h>

void
create_()
{
    ustaw_odmiane_rasy("krasnolud");

    //FIXME:
    dodaj_przym("m�ody", "m�odzi");
    dodaj_przym("bladosk�ry", "bladosk�rzy");

    ustaw_imie(({"brin", "brina", "brinowi", "brina", "brinem", "brinie"}), PL_MESKI_NOS_NZYW);
    set_surname("Vivaldi");
    set_origin("z Mahakamu");

    //FIXME:
    set_long("Dwudziestokilkuletni, na oko, m�czyzna zatrzymuje na "+
             "tobie spojrzenie szarych oczu osadzonych w bladej, poci�g�ej "+
             "twarzy. Jasne, s�omiane w�osy nosi kr�tko obci�te. Ubrany jest "+
             "w bezkszta�ty w�r z br�zowego lnu, kt�ry przy sporej dozie "+
             "uprzejmo�ci mo�na nazwa� szat�. Wprawdzie niemal doskonale ukrywa "+
             "ona budow� cia�a, lecz wydaje si� on raczej szczup�y, ni� dobrze "+
             "zbudowany. Co chwila odrywa si� od wykonywanej bez entuzjazmu pracy, "+
             "rozgl�da dooko�a, czy zajmuje cho�by uk�adaniem pi�r w ka�amarzu. "+
             "Jego smuk�e d�onie, od niechcenia przewracaj�ce strony ksi��ek, "+
             "czy uk�adaj�ce lu�ne kartki, pokryte s� plamami inkaustu.\n");

    set_act_time(10);
    add_act("emote w skupienu kontempluje tre�� niewielkiej kartki.");
    add_act("@@for_women_only@@");
    add_act("emote bez przekonania przek�ada kolejne, le��ce na biurku kartki.");
    add_act("ziewnij szeroko");
    add_act("emote odchyla si� na krze�le i patrzy t�po w sufit.");

    //FIXME:
    set_cact_time(3);
    add_cact("emote kuli si�.");
    add_cact("powiedz :dr��cym g�osem: Nie bij mnie!");

    add_armour(RINDE_BIBLIOTEKA_UBRANIA + "obszerna_przybrudzona_szata.c");
    add_object(RINDE_BIBLIOTEKA_PRZEDMIOTY + "karteczka.c");

    set_stats(({70, 70, 70, 70, 70})); //FIXME: Do balansu.

    add_ask(({"zadanie", "pomoc"}), VBFC_ME("pyt_o_pomoc"));
    add_ask("prac�", VBFC_ME("pyt_o_prace"));
    add_ask(({"ksi��ki", "bibliotek�"}), VBFC_ME("pyt_o_ksiazki"));

    set_default_answer(VBFC_ME("def_answer"));

    set_alarm(1.0, 0.0, &(TO)->command("usiadz przy biurku"));
}

public string
for_woman_only()
{
    if(TP->query_gener() == G_FEMALE)
        return "emote udaj�c, �e co� czyta, �ledzi ci� uwa�nie wzrokiem.";

    return "";
}

public string
def_answer()
{
    switch(random((TP->query_gender() == G_FEMALE ? 4 : 2)))
    {
    case 0:
        TO->command("powiedz do " + OB_NAME(TP) + " Poszukaj w ksi��kach. "+
                    "Mo�e b�dziesz mie� szcz�cie. Tylko nie wno� otwartego ognia do magazynu.");
        break;
    case 1:
        TO->command("powiedz do " + OB_NAME(TP) + " Nie wiem. Nie nadaj� si� na bibliotekarza.");
        TO->command("emote rozk�ada r�ce bezradnie.");
        break;
    default:
        TO->command("powiedz do " + OB_NAME(TP) + " Mo�emy to om�wi� dok�adniej wieczorem?");
        TO->command("pu�� oczko do " + OB_NAME(TP));
    }

    return "";
}

public string
pyt_o_prace()
{
    TO->command("powiedz do " + OB_NAME(TP) + " Dobrze p�ac�, ale pozatym....");
    TO->command("emote wzdycha ci�ko.");
    TO->command("powiedz do " + OB_NAME(TP) + " Jestem zbyt m�ody, aby siedzie� w bibliotece.");

    return "";
}

public string
pyt_o_pomoc()
{
    TO->command("powiedz do " + OB_NAME(TP) + " Tu nie ma zbyt wiele do roboty... " +
                "W dzisiejszych czasach ludzie niewiele czytaj�.");
    TO->command("emote macha r�k� z rezygnacj�.");

    return "";
}

public string
pyt_o_ksiazki()
{
    TO->command("powiedz do " + OB_NAME(TP) + " Przy bibliotekach Novigradu "+
                "czy Oxenfurtu, to co tutaj mamy to tylko kilka tomiszcz.");
    TO->command("powiedz do " + OB_NAME(TP) + " Jednak jak chcesz mo�esz sobie "+
                "czego� poszuka�. Mo�e akurat si� znajdzie?");

    return "";
}

void
emote_hook(string emote, object wykonujacy, string przyslowek)
{
    switch (emote)
    {
    case "kopnij":
    case "opluj":
        set_alarm(0.5, 0.0, "command_gdy_jest", "powiedz :do " + OB_NAME(wykonujacy) + " z rezygnacj�: " +
                  "Jasne pou�ywaj sobie. W ko�cu jestem tylko niewiele ponad pisarczykiem.", wykonujacy);
        break;
    case "poca�uj":
    case "przytul":
    case "pog�aszczaj":
    case "pog�askaj":
        if(wykonujacy->query_gender() == G_FEMALE)
        {
            set_alarm(0.5, 0.0, "command_gdy_jest", "powiedz :do " + OB_NAME(wykonujacy) + " u�miechaj�c " +
                      "si� zalotnie: Mo�e um�wimy si� na wiecz�r?", wykonujacy);
        }
        else
        {
            set_alarm(0.5, 0.0, "command_gdy_jest", "powiedz do " + OB_NAME(wykonujacy) + " " +
                      "Odch�do� si� odemnie.", wykonujacy);
        }
    case "uk�o�":
        set_alarm(0.5, 0.0, "command", "pokiwaj powitalnie do "
                  + OB_NAME(wykonujacy));
        break;
    case "kopnij":
    case "opluj":
        set_alarm(0.5, 0.0, "command", "kopnij "+OB_NAME(wykonujacy));
        set_alarm(1.0, 0.0, "command", "spojrzyj zimno na "+OB_NAME(wykonujacy));
        break;
    }
}