
/*
 * Sklepikarz w Rinde.
 * Wykonany przez Avarda dnia 07.06.06
 * Opis longa by Alcyone
 */

#pragma unique

#include <std.h>
#include <ss_types.h>
#include <macros.h>
#include <stdproperties.h>
#include <pl.h>
#include <object_types.h>
#include <object_types.h>
#include "dir.h"

inherit RINDE_NPC_STD;
inherit "/lib/sklepikarz.c";

int czy_walka();
int walka = 0;

#define HACZYKOWY "/d/Standard/Redania/Piana/npc/krawiec"
#define ZROBILI "/d/Standard/questy/utopce/zrobili"
#define ROBIACY "/d/Standard/questy/utopce/robiacy"
#define MEDALION "/d/Standard/Redania/Piana/quest/medalion"
#define WZIELI "/d/Standard/questy/utopce/wzieli"
#define WZIELI_LIST "/d/Standard/questy/utopce/wzielil"
#define LIST "/d/Standard/Redania/Piana/quest/list"

void
create_rinde_humanoid()
{
	ustaw_odmiane_rasy(PL_MEZCZYZNA);
	set_gender(G_MALE);
	ustaw_imie(({"eugeniusz","eugeniusza","eugeniuszowi","eugeniusza",
        "eugeniuszem","eugeniuszu"}), PL_MESKI_OS);
    set_origin("z Rinde");
    dodaj_nazwy("staruszek");
	dodaj_nazwy("sklepikarz");
    dodaj_przym("stary","stary");
    dodaj_przym("niski","niscy");


	set_long("Ten niezbyt wysoki m^e^zczyzna, wida^c ma ju^z swoje lata, "+
        "co bez w^atpienia mo^za stwierdzi^c po jego rzadkich, siwych "+
        "w^losach i starej pomarszczonej twarzy. Mimo to przygarbiony "+
        "staruszek bez trudu radzi sobie z klientami i bacznie przygl^ada "+
        "si^e wszystkim ruchom potencjalnych nabywc^ow jego d^obr. Du^ze "+
        "d^lonie sprawnie obracaj^a monety, kt^ore szybko laduja gdzies pod "+
        "lad^a w wiadomym tylko dla niego miejscu. M^e^zczyzna ma na sobie "+
        "do^s^c rzucaj^ace si^e w oczy, zielone spodnie, ktore wydaj^a si^e "+
        "zupe^lnie nie pasowa^c do tej osoby.\n");

	set_stats (({ 50, 30, 45, 45, 20 }));

    add_prop(CONT_I_WEIGHT, 75000);
    add_prop(CONT_I_HEIGHT, 160);

    set_act_time(30);
    add_act("ziewnij");
    add_act("podrap sie po plecach");
    add_act("rozejrzyj sie leniwie");
    add_act("emote g^laszcze si^e po g^lowie jakby chcia^l poprawi^c fryzur^e.");

	set_cact_time(10);
	add_cact("krzyknij Pomocy! Ratunku!");
	add_cact("powiedz Zostaw mnie, prosze!");
	add_cact("powiedz Nie r^ob mi tego!");

	add_armour(RINDE_UBRANIA + "buty/brazowe_skorzane_Mc.c");
	add_armour(RINDE_UBRANIA + "koszule/zielonkawa_szeroka_Mc.c");
	add_armour(RINDE_UBRANIA + "spodnie/dlugie_zielone_Mc.c");
       add_object(CENTRUM_DRZWI + "k_do_sklepu.c");

	set_skill(SS_PARRY, 60 + random(10));
    set_skill(SS_UNARM_COMBAT, 45 + random(5));
    set_skill(SS_AWARENESS, 40 + random(10));

    config_default_sklepikarz();

    set_money_greed_buy(215);
    set_money_greed_sell(575);
	set_money_greed_change(116);
    set_store_room(CENTRUM_LOKACJE + "magazyn_sklepowy.c");

    set_co_skupujemy(O_UBRANIA|O_BUTY|O_KUCHENNE|O_LINY|O_LAMPY|O_POCHODNIE|
    				O_ZBROJE|O_BRON_MIECZE|O_BRON_MIECZE_2H|O_BRON_SZABLE|
    				O_BRON_SZTYLETY|O_BRON_TOPORY|O_BRON_TOPORY_2H|
    				O_BRON_DRZEWCOWE_D|O_BRON_DRZEWCOWE_K|O_BRON_MACZUGI|O_BRON_MLOTY);
    //set_co_skupujemy(MAXINT);



	add_ask(({"sklep"}), VBFC_ME("pyt_o_sklep"));
	add_ask(({"wysokiego brodatego m^e^zczyzn^e", "brodatego wysokiego m^e^zczyzn^e",
	"milona", "wysokiego brodatego cz^lowieka", "brodatego wysokiego cz^lowieka"}),
	VBFC_ME("pyt_milon"));
	set_default_answer(VBFC_ME("default_answer"));

    setuid();
    seteuid(getuid());
}
void
powiedz_gdy_jest(object player, string tekst)
{
	if (environment(this_object()) == environment(player))
	this_object()->command(tekst);
}

string
pyt_o_sklep()
{
	set_alarm(2.0, 0.0, "powiedz_gdy_jest", this_player(),
	"':z nieukrywan^a dum^a: Tak, to m^oj sklep.");
	return "";
}

string
pyt_milon()
{
    string *cp;

    if (TP->query_prop("pytal_sklepikarza_o_milona_do_cholery") == 0)
    {
        cp = explode(read_file(ROBIACY), "#");
        if (member_array(lower_case(TP->query_name()), cp)!= -1)
        {
            if (cp[member_array(lower_case(TP->query_name()), cp) + 1] == "10000000")
            {
                cp[member_array(lower_case(TP->query_name()), cp) + 1] = "11000000";
                write_bytes(ROBIACY, 0, implode(cp, "#"));
             // TP->catch_msg(set_color(42) + "***CHECKPOINT 2***\n" + clear_color());
            }
        }
        TP->add_prop("pytal_sklepikarza_o_milona_do_cholery", 1);
	    set_alarm(2.0, 0.0, "powiedz_gdy_jest", this_player(),
    	"powiedz do " + OB_NAME(TP) + " Raz zrobi^l dla mnie " +
    	"kilka skrzy^n. Je^sli go pan" + TP->koncowka("", "i") + " szuka, pewnie siedzi " +
    	"w kt^orej^s ober^zy i chleje");
    	set_alarm(5.0, 0.0, "powiedz_gdy_jest", this_player(),
    	"mrugnij porozumiewawczo do " + OB_NAME(TP));
    }
    else
        set_alarm(2.0, 0.0, "powiedz_gdy_jest", this_player(),
    	"powiedz do " + OB_NAME(TP) + " M^owi^lem ju^z, prosz^e go szuka^c go w karczmie");
}

string
default_answer()
{
	set_alarm(2.0, 0.0, "powiedz_gdy_jest", this_player(),
       "powiedz do "+ this_player()->query_name(PL_DOP) +
       " Niestety nie potrafi^e na to odpowiedzie^c.");
	return "";
}

void
add_introduced(string imie_mia, string imie_bie)
{
        set_alarm(2.0, 0.0, "return_introduce", find_player(imie_mia));
}
void
return_introduce(object ob)
{
    command("przedstaw sie " + OB_NAME(ob));

}

void
emote_hook(string emote, object wykonujacy)
{
    switch (emote)
    {
        case "kopnij": set_alarm(1.5, 0.0, "nienajlepszy", wykonujacy);
			        break;
        case "spoliczkuj": set_alarm(1.5, 0.0, "nienajlepszy", wykonujacy);
                    break;
        case "opluj" : set_alarm(2.5, 0.0, "nienajlepszy", wykonujacy);
                    break;
		case "poca^luj":
              {
              if(wykonujacy->query_gender() == 1)
                switch(random(2))
                 {
                 case 0:command("zarumien sie ");
						command("':nie^smia^lo: No wie pani.");break;
                 case 1:command("zarumien sie");
                        command("popatrz z usmiechem na "+
                        OB_NAME(wykonujacy));
                        break;
                }
              else
                {
                   switch(random(3))
                   {
                   case 0:command("':niepewnie: Nie wiem co "+
                       "chcia^l pan przez to powiedzie^c, ale prosze "+
                       "tego wi^ecej nie robi^c.");
							break;
                   case 1:command("spojrzyj niepewnie na "+
                        OB_NAME(wykonujacy));
							break;
                   case 2:command("'To wcale nie by^lo "+
                       "przyjemne.");
							break;
                   }
                }

                break;
                }
        case "przytul": set_alarm(2.5, 0.0, "malolepszy", wykonujacy);
                   break;
        case "pog^laszcz": set_alarm(2.5, 0.0, "malolepszy", wykonujacy);
                   break;
    }
}

void
malolepszy(object wykonujacy)
{

   switch(random(4))
   {
     case 0: command("usmiechnij sie niepewnie"); break;
     case 1: command("usmiechnij sie nieznacznie"); break;
     case 2: command("hmm"); break;
	 case 3: command("namysl sie ."); break;

   }
}

void
nienajlepszy(object kto)
{
   switch(random(3))
   {
      case 0: command("powiedz Zr^ob to jeszcze raz, a wezw^e stra^z.");
              break;
      case 1: set_alarm(0.5, 0.0, "powiedz_gdy_jest", kto,
              "prychnij");
              break;
      case 2: command("powiedz Wyjd^z st^ad.");
              break;
   }
}

void
attacked_by(object wrog)
{
    if(walka==0)
    {
	command("krzyknij Stra^z! Ratunku!");

    set_alarm(5.0, 0.0, "czy_walka", 1);
    walka = 1;
    return ::attacked_by(wrog);
    }

    else if(walka == 1)
    {
      set_alarm(1.0, 0.0, "command", "krzyknij Pomocy!");
      walka = 1;

      return ::attacked_by(wrog);
    }

}

int
czy_walka()
{
    if(!query_attack())
    {

       this_object()->command("sapnij");

       walka = 0;
       return 1;
    }
       else
       {
       set_alarm(5.0, 0.0, "czy_walka", 1);
       }

}

void
shop_hook_stole_items(object *co)
{
    command("powiedz Nie skupuj� takich podejrzanych rzeczy.");
}

void
shop_hook_tego_nie_skupujemy(object ob)
{
    command("powiedz Nie skupuj� �adnych " + ob->query_pnazwa(PL_DOP)+".");
}

void
init()
{
    ::init();
    init_sklepikarz();
}

