/**
 * Drzwi wyjsciowe z swiatyni Kreve
 * Wykonane przez Avarda, dnia 16.03.06
 */

inherit "/std/door";

#include "dir.h"
#include <pl.h>

void
create_door()
{
    ustaw_nazwe("wrota");
    dodaj_przym("wielki", "wielcy");

    set_other_room(ZACHOD_LOKACJE +"ulica8.c");
    set_door_id("DRZWI_DO_SWIATYNI");

    set_door_desc("Wielkie wrota �wiatynne wykonano z kilku warstw grubej "+
        "stali, po czym pokryto inskrypcjami oraz rycinami "+
        "przedstawiajacymi siedem dni pobytu Kreve w�r^od ludzi. \n");

    set_open_desc("Na po^ludniowej ^scianie znajduj^a si^e otwarte "+
        "wielkie wrota.\n");
    set_closed_desc("Na po^ludniowej ^scianie znajduj^a si^e zamkni^ete "+
        "wielkie wrota.\n");

    set_pass_command(({"wyj^scie","wyjd^x ze ^swi^atyni","ulica",
     "przejd^x przez wrota"}),"przez wielkie wrota do wyj�cia",
     "ze �wi�tyni");

    set_key("KLUCZ_DRZWI_DO_SWIATYNI");
    set_lock_name(({"zamek", "zamku", "zamkowi", "zamek", "zamkiem", "zamku"}), 0, PL_MESKI_NOS_NZYW);
}
