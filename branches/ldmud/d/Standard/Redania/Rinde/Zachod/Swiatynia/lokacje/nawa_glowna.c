
/* Nawa glowna swiatyni Kreve, w Rinde
   Made by Avard, 01.06.06
   Opis by Tinardan */

#include "dir.h"

inherit RINDE_STD;

#include <stdproperties.h>
#include <macros.h>
#include <filter_funs.h>

void
create_rinde()
{
    set_short("Przed o^ltarzem Kreve");

    add_event("Gdzie^s z ty^lu rozlega si^e cichy kaszel.\n");
    add_event("P^lomie� na r^ekach pos^agu dr^zy i faluje, tworz^ac na "+
        "posadzce fantastyczne kszta^lty.\n");

    add_exit("nawa_boczna_e", "e");
    add_exit("nawa_boczna_w", "w");
    add_exit("przedsionek", "s");

    add_prop(ROOM_I_INSIDE, 1);

    add_npc(RINDE_NPC + "krepp", 1);
    add_object(SWIATYNIA_OBIEKTY + "posag.c");
    dodaj_rzecz_niewyswietlana("marmurowy bia^ly pos^ag", 1);

    add_object(MEBLER +"lawa_solidna_debowa.c", 6);
    dodaj_rzecz_niewyswietlana("solidna d^ebowa ^lawa", 99);
    set_alarm_every_hour("co_godzine");

}
public string
exits_description()
{
    return "Na zachodzie i wschodzie znajduj^a sie nawy boczne ^swi^atyni, "+
           "za^s na po^ludniu jest przedsionek.\n";
}


string
dlugi_opis()
{
    string str;
    int i, il, il_law;
    object *inwentarz;

    inwentarz = all_inventory();
    il_law = 0;
    il = sizeof(inwentarz);

    for (i = 0; i < il; ++i) {
         if (inwentarz[i]->jestem_lawa_swiatyni()) {
                        ++il_law; }}

    str = "Przestrze^n jest ogromna, zapieraj^aca dech w piersiach, "+
        "niewyobra^zalna. S^lycha^c tu najmniejszy szelest i szept, "+
        "tote^z ludzie, przekraczaj^ac pr^og ^swi^atyni, wstrzymuj^a "+
        "oddech i z pokor^a pochylaj^a g^lowy, zupe^lnie jakby b^ostwo "+
        "mia^lo si^e rozgniewa^c za ka^zdy g^lo^sniejszy d^xwi^ek. "+
        "Sufit niknie gdzie^s w pomroce, a srebrne, bogato rze^xbione "+
        "kandelabry ze ^swiecami zwisaj^a nisko nad g^lowami wiernych. "+
        "Mimo tego, w pomieszczeniu panuje p^o^lmrok, szary kamie^n, "+
        "kt^orym wy^lo^zona jest pod^loga i z kt^orego wzniesione s^a "+
        "^sciany, jest zimny i nieprzyjazny. ";

    if (il_law == 0)
    {
        str += "";
    }
    if (il_law == 1)
    {
        str += "Przed o^ltarzem stoi drewniana ^lawa z kl^ecznikiem, jakby "+
            "przeznaczona dla grzesznik^ow i pokutuj^acych. ";
    }
    if (il_law == 2)
    {
        str += "Przed o^ltarzem stoj^a dwie drewniane ^lawy z kl^ecznikami, "+
            "jakby przeznaczone dla grzesznik^ow i pokutuj^acych. ";
    }
    if (il_law == 3)
    {
        str += "Przed o^ltarzem stoj^a trzy drewniane ^lawy z kl^ecznikami, "+
            "jakby przeznaczone dla grzesznik^ow i pokutuj^acych. ";
    }
    if (il_law == 4)
    {
        str += "Przed o^ltarzem stoj^a cztery drewniane ^lawy z kl^ecznikami, "+
            "jakby przeznaczone dla grzesznik^ow i pokutuj^acych. ";
    }
    if (il_law > 4)
    {
        str += "Przed o^ltarzem stoi d^lugi rz^ad drewnianych ^law z "+
            "kl^ecznikami, jakby przeznaczonych dla grzesznik^ow i "+
            "pokutuj^acych. ";
    }

    if (jest_rzecz_w_sublokacji(0, "Krepp") ||
        jest_rzecz_w_sublokacji(0, "Oty^ly leciwy m^e^zczyzna"))

    {
        str +="Tu^z obok kap^lan, odziany w d^lug^a, brunatn^a szat^e "+
            "przest^epuje niecierpliwie z nogi na nog^e i popatruje z "+
            "wy^zszo^sci^a na t^lumek nieustannie kr^ec^acy si^e po "+
            "^swi^atyni. ";
    }
    else
    {
        str +="";
    }

        str +="To miejsce jest "+
        "ponure i przyt^laczaj^ace swym ogromem, jednak najwi^eksze "+
        "wra^zenie robi o^ltarz. Olbrzymia sylwetka boga Kreve, w jego "+
        "antropomorficznej postaci, g^oruje nad pomieszczeniem. "+
        "B^og, wykuty z bia^lego marmuru, pot^e^zny i o surowym obliczu, "+
        "przes^lania okna i rzuca d^lugi cie^n na kamienn^a pod^log^e. W "+
        "z^l^aczonych d^loniach trzyma mis^e, w kt^orej pe^lza ogie^n, nie "+
        "gaszony od wielu pokole^n.\n";

    return str;
}
int
co_godzine()
{
    LOAD_ERR(SWIATYNIA_LOKACJE+"nawa_boczna_w.c");
    LOAD_ERR(SWIATYNIA_LOKACJE+"nawa_boczna_e.c");
    LOAD_ERR(SWIATYNIA_LOKACJE+"przedsionek.c");
    object *gracze = FILTER_PLAYERS(all_inventory(this_object()));

    if((present("krepp", find_object(SWIATYNIA_LOKACJE+"nawa_boczna_w")) ||
       present("krepp", find_object(SWIATYNIA_LOKACJE+"nawa_boczna_e")) ||
       present("krepp", find_object(SWIATYNIA_LOKACJE+"przedsionek"))) &&
       (MT_GODZINA == 23))
    {
        tell_room(QCIMIE(find_living("krepp"),PL_MIA)+ " przybywa "+
            "i wyprowadza ci^e na ulic^e, m^owi^ac: Zamykam ^swi^atynie, "+
            "zapraszam jutro.\n");
        gracze->move(ZACHOD_LOKACJE+"ulica8");
        find_living("krepp")->move(ZACHOD_LOKACJE+"ulica8");
    }
    if(present("krepp", TO) && (MT_GODZINA == 23))
    {
        tell_roombb(this_object(), QCIMIE(find_living("krepp"),PL_MIA)+ " wyprowadza ci^e "+
            "na ulic^e, m^owi^ac: Zamykam ^swi^atynie, zapraszam jutro.\n");
        gracze->move(ZACHOD_LOKACJE+"ulica8");
        find_living("krepp")->move(ZACHOD_LOKACJE+"ulica8");
    }
}
