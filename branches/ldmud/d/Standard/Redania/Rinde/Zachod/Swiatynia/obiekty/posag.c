
/* Posag Kreve do swiatyni w Rinde
Made by Avard 01.06.06 
Opis by Tinardan */
inherit "/std/object.c";

#include "dir.h"
#include <pl.h>
#include <stdproperties.h>
#include <macros.h>
#include <filter_funs.h>
#include <materialy.h>
#include <object_types.h>

create_object()
{
    ustaw_nazwe("posag");
    dodaj_przym("marmurowy","marmurowi");
    dodaj_przym("bia^ly","biali");

    set_long("Bia^ly marmur, odpowiednio o^swietlony i zadbany, potrafi "+
        "dawa^c wra^zenie przytulno^sci, spokoju i dostatku. Jednak^ze w "+
        "tym wypadku jest zupe^lnie inaczej. Przez nigdy nie rozja^sniony "+
        "s^lo^ncem mrok, i przez migotliwy blask ognia biel "+
        "wydaje si^e by^c przydymiona i zaniedbana, mimo i^z wiadomo, "+
        "^ze pos^ag jest oczkiem w g^lowie kap^lan^ow. Jednak najbardziej "+
        "niesamowita jest twarz pos^agu, cieniami rzucanymi przez wiecznie "+
        "p^lon^acy ogie^n, wygl^ada zupe^lnie jak ^zywa. Surowe oblicze "+ 
        "Kreve spogl^ada na wszystkich uwa^znym wzrokiem. Spokojnym, ale "+
        "tak^ze wszechwiedz^acym. Rze^xbiarz tak wyku^l ^xrenica b^ostwa, "+
        "^ze wydaj^a si^e pod^a^za^c za patrz^acym, gdziekolwiek by si^e "+
        "nie ruszy^l, a w samym ich centrum czai si^e gro^xba i "+
        "okrucie^nstwo. Na boskiej piersi widnieje znak Kreve b^lyskawica, "+
        "wykuta z najszczerszego z^lota i wypolerowana tak, by odbija^la "+
        "ka^zde, cho^cby najmniejsze ^xr^od^lo ^swiat^la. W stulonych "+
        "d^loniach boga pe^lga w wype^lnionej po brzegi oliw^a, ogie^n, "+
        "symbol i pami^atka, Kreve, Tego, Kt^ory Zst^api^l W B^lyskawicy. "+
        "Czo^lo b^ostwa jest zmarszczone, jakby martwi^l go panuj^acy na "+
        "^swiecie chaos i rozpr^e^zenie obyczaj^ow.\n");

    add_cmd_item(({"pos^agu","bi^alego pos^agu","marmurowego pos^agu",
        "marmurowego bia^lego pos^agu"}), "dotknij", "Dotykasz pos^agu. "+
        "Marmur wydaje si^e nadzwyczaj zimny i chropowaty.\n");

    add_prop(OBJ_I_WEIGHT, 1000000);
    add_prop(OBJ_I_VOLUME, 100000);
    add_prop(OBJ_I_VALUE, 1000);
    add_prop(OBJ_M_NO_SELL, 1);
    add_prop(OBJ_I_NO_GET, "Niestety pos^ag jest zbyt ci^e^zki. \n");
    ustaw_material(MATERIALY_MARMUR);
    set_type(O_INNE);
}