
/* Posag rycerza do swiatyni w Rinde
Made by Avard 02.06.06 
Opis by Tinardan*/

inherit "/std/object.c";

#include "dir.h"
#include <pl.h>
#include <stdproperties.h>
#include <macros.h>
#include <filter_funs.h>
#include <materialy.h>
#include <object_types.h>

create_object()
{
    ustaw_nazwe("posag");
    dodaj_przym("marmurowy","marmurowi");
    dodaj_przym("bia^ly","biali");

    set_long("Patrz^ac na ten pos^ag ma si^e nieodparte wra^zenie, ^ze co^s " +
    "z nim jest nie tak. Mo^ze sprawia to postawa kap^lana, dumnie " +
    "wypr^e^zonego w oczyszczaj^acym ogniu, wyci^agaj^acego r^ece w g^or^e " +
    "w niemej chwale swego b^ostwa, mo^ze sam marmurowy ogie^n - wij^acy " +
    "si^e i spl^atany niczym cielska olbrzymich w^e^zy, oplataj^acych nogi " +
    "i korpus postaci. Oblicze kap^lana jest ponure i natchnione " +
    "jednocze^snie, natchnione w uwielbieniu dla Kreve i surowe dla tych, " +
    "kt^orzy o^smiel^a si^e mu sprzeciwi^c. Jego wyci^agni^ete d^lonie, " +
    "pokryte delikatnym mchem, kt^ory odwa^zy^l si^e przest^api^c progi " +
    "nawet tej ^swi^atyni, rzucaj^a wielkie cienie na pod^log^e i " +
    "zgromadzonych na niej wiernych. Cienie uk^ladaj^ace si^e na posadzce " +
    "niczym p^etla i zaciskaj^ace si^e wok^o^l, pe^lnych strachu i pokory, " +
    "ludzi.\n");

    add_prop(OBJ_I_WEIGHT, 1000000);
    add_prop(OBJ_I_VOLUME, 100000);
    add_prop(OBJ_I_VALUE, 1000);
    add_prop(OBJ_M_NO_SELL, 1);
    add_prop(OBJ_I_NO_GET, "Niestety pos^ag jest zbyt ci^e^zki. \n");
    ustaw_material(MATERIALY_MARMUR);
    set_type(O_INNE);
}
