
/* Nawa boczna zachodnia swiatyni Kreve, w Rinde
   Made by Avard, 01.06.06
   Opis by Tinardan */
#include "dir.h"

inherit RINDE_STD;
inherit "/lib/guild_support";

#include <stdproperties.h>
#include <filter_funs.h>
#include <macros.h>
#include <pl.h>
#include <mudtime.h>

void
create_rinde()
{
    set_short("Lewa nawa boczna");

    add_event("P^lomie^n chwieje si^e, przez co twarz pos^agu wydaje si^e "+
        "jeszcze sro^zsza.\n");
    add_event("Dochodz^a ci^e szepty modlitw.\n");
    add_event("Ogie^n huczy jednostajnie.\n");

    add_exit(SWIATYNIA_LOKACJE + "nawa_glowna", "e");

    add_prop(ROOM_I_INSIDE, 1);
    add_object(SWIATYNIA_OBIEKTY + "posag_w.c");
    dodaj_rzecz_niewyswietlana("marmurowy bia^ly pos^ag", 1);

    add_item(({"sklepienie", "freski", "fresk"}),
        "Kolorowe, ale wznios^le i powa^zne freski na sklepieniu " +
        "przedstawiaj^a wielkiego Kreve we wszystkich chyba znanych " +
        "wcieleniach.\n");
    set_alarm_every_hour("co_godzine");

    create_guild_support();
}

void
init()
{
    init_guild_support();
    ::init();
}

public string
exits_description()
{
    return "Na wschodzie znajduje si^e nawa g^l^owna ^swi^atyni.\n";
}

string
dlugi_opis()
{
    string str;

    str = "Cho^c w por^ownaniu do nawy g^l^ownej, jest to miejsce niewielkie "+
        "i spokojne, mog^loby z powodzeniem gra^c rol^e osobnej ^swi^atyni. "+
        "Wysokie sklepienie zdobione jest bogatymi freskami o nasyconych "+
        "barwach i eleganckiej kresce. Wszystko to na chwa^l^e b^ostwa, jest "+
        "wi^ec Kreve Triumfuj^acy, Kreve, Kt^ory Zst^api^l w Postaci "+
        "B�yskawicy, Kreve Obro^nca ^Ladu i Porz^adku. Nad pomieszczeniem "+
        "g^oruje pos^ag du^zo mniejszy ni� w g^l^ownej komnacie ^swi^atyni, "+
        "ale i tak imponuj^acy Rycerza Zakonu Bia^lej R^o^zy, wspartego na "+
        "mieczu, z powa^znym wzrokiem utkwionym gdzie^s w przestrzeni. "+
        "^Swiat^lo pada z ogromnej misy pe^lnej p^lomieni, zawieszonej nad "+
        "g^low^a pos^agu niczym aureola chwa^ly. ";

    if(sizeof(FILTER_LIVE(all_inventory()))>=2)
    {
        str +="Kamienna posadzka odbija ka^zdy, najl^zejszy nawet krok i "+
        "ludzie pod^swiadomie st^apaj^a cicho, pokornie schylaj^ac g^lowy "+
        "przed boskim i rycerskim majestatem. ";
    }
    str += "\n";

    return str;
}
int
co_godzine()
{
    LOAD_ERR(SWIATYNIA_LOKACJE+"nawa_boczna_e.c");
    LOAD_ERR(SWIATYNIA_LOKACJE+"nawa_glowna.c");
    LOAD_ERR(SWIATYNIA_LOKACJE+"przedsionek.c");
    object *gracze = FILTER_PLAYERS(all_inventory(this_object()));

    if((present("krepp", find_object(SWIATYNIA_LOKACJE+"nawa_boczna_e")) ||
       present("krepp", find_object(SWIATYNIA_LOKACJE+"nawa_glowna")) ||
       present("krepp", find_object(SWIATYNIA_LOKACJE+"przedsionek"))) &&
       (MT_GODZINA == 23))
    {
        tell_room(QCIMIE(find_living("krepp"),PL_MIA)+ " przybywa ze wschodu"+
            " i wyprowadza ci^e na ulic^e, m^owi^ac: Zamykam ^swi^atynie,"+
            " zapraszam jutro.\n");
        gracze->move(ZACHOD_LOKACJE+"ulica8");
        find_living("krepp")->move(ZACHOD_LOKACJE+"ulica8");
    }
    if(present("krepp", TO) && (MT_GODZINA == 23))
    {
        tell_roombb(this_object(), QCIMIE(find_living("krepp"),PL_MIA)+ " wyprowadza ci^e "+
            "na ulic^e, m^owi^ac: Zamykam ^swi^atynie, zapraszam jutro.\n");
        gracze->move(ZACHOD_LOKACJE+"ulica8");
        find_living("krepp")->move(ZACHOD_LOKACJE+"ulica8");
    }
}
