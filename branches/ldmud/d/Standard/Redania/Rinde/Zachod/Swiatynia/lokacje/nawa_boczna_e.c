
/* Nawa boczna wschodnia swiatyni Kreve, w Rinde
   Made by Avard, 01.06.06
   Opis by Tinardan */

#include "dir.h"

inherit RINDE_STD;

#include <stdproperties.h>
#include <filter_funs.h>
#include <mudtime.h>
#include <macros.h>

void
create_rinde()
{
    set_short("Prawa nawa boczna");

    add_event("W powietrzu czu^c wilgo^c i dra^zni^acy zapach mokrego " +
        "kamienia.\n");
    add_event("Z sasi^edniej sali dochodzi ci^e st^lumiony szept modlitw.\n");
    add_event("St^apania wiernych nios^a si^e cicho po posadzce.\n");

    add_exit(SWIATYNIA_LOKACJE + "nawa_glowna", "w");

    add_prop(ROOM_I_INSIDE, 1);
    add_object(SWIATYNIA_OBIEKTY + "posag_e.c");
    dodaj_rzecz_niewyswietlana("marmurowy bia^ly pos^ag", 1);

    add_item("p^laskorze^xby",
        "Zdobi^ace ^sciany i sufit pomieszczenia p^laskorze^xby przedstawiaj^a "+
        "sceny z ^zycia Rycerzy Bia^lej R^o^zy i kap^lan^ow boga Kreve.\n");
    set_alarm_every_hour("co_godzine");
}
public string
exits_description()
{
    return "Na zachodzie znajduje si^e nawa g^l^owna ^swi^atyni.\n";
}


string
dlugi_opis()
{
    string str;

    if(sizeof(FILTER_LIVE(all_inventory()))>=2)
        str = "Prawa nawa jest naprawd^e miejscem cichym, ";
    else
        str = "Prawa nawa jest naprawd^e miejscem cichym i odludnym, ";

    str += "cho^c do^s^c przestronnym. Brak tu " +
    "fresk^ow i mimo ^ze sufit bogato zdobiony " +
    "jest p^laskorze^xbami, to nie ma tu kolor^ow i ciep^la. Kamienna " +
    "posadzka jest zimna i nie^ladna, ale schludnie wypolerowana. Jest to " +
    "najmniejsza ze ^swi^atynnych sal, przera^zaj^aca swoimi ponurymi " +
    "barwami, jednak^ze nie mniej imponuj^aca ni^z dwie pozosta^le. P^laskorze^xby " +
    "na suficie i ^scianach wyobra^zaj^a okrutne sceny z ^zycia " +
    "wybra^nc^ow Kreve, Rycerzy Bia^lej R^o^zy i jego kap^lan^ow. Ponad " +
    "pomieszczeniem wznosi si^e pos^ag kap^lana stoj^acego w boskim " +
    "ogniu Kreve, pos^ag o twarzy ponurej i surowej, o barwach bieli i " +
    "szaro^sci kosztownych marmur^ow.\n";

    return str;
}
int
co_godzine()
{
    LOAD_ERR(SWIATYNIA_LOKACJE+"nawa_boczna_w.c");
    LOAD_ERR(SWIATYNIA_LOKACJE+"nawa_glowna.c");
    LOAD_ERR(SWIATYNIA_LOKACJE+"przedsionek.c");
    object *gracze = FILTER_PLAYERS(all_inventory(this_object()));

    if((present("krepp", find_object(SWIATYNIA_LOKACJE+"nawa_boczna_w")) ||
       present("krepp", find_object(SWIATYNIA_LOKACJE+"nawa_glowna")) ||
       present("krepp", find_object(SWIATYNIA_LOKACJE+"przedsionek"))) &&
       (MT_GODZINA == 23))
    {
        tell_room(QCIMIE(find_living("krepp"),PL_MIA)+ " przybywa z zachodu "+
            "i wyprowadza ci^e na ulic^e, m^owi^ac: Zamykam ^swi^atynie,"+
            " zapraszam jutro.\n");
        gracze->move(ZACHOD_LOKACJE+"ulica8");
        find_living("krepp")->move(ZACHOD_LOKACJE+"ulica8");
    }
    if(present("krepp", TO) && (MT_GODZINA == 23))
    {
        tell_roombb(this_object(), QCIMIE(find_living("krepp"),PL_MIA)+ " wyprowadza ci^e "+
            "na ulic^e, m^owi^ac: Zamykam ^swi^atynie, zapraszam jutro.\n");
        gracze->move(ZACHOD_LOKACJE+"ulica8");
        find_living("krepp")->move(ZACHOD_LOKACJE+"ulica8");
    }
}
