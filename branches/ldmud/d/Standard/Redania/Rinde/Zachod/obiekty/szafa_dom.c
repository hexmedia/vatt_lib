/*
 * Autor: Rantaur
 * Data: 18.06.06
 * Opis: Szafa do domu swiatynnego w Rinde
 */

inherit "/std/receptacle";

#include "dir.h"
#include <pl.h>
#include <macros.h>
#include <stdproperties.h>
#include <object_types.h>

void create_container()
{
  ustaw_nazwe("szafa");
  dodaj_przym("okaza�y", "okalazli");
  dodaj_przym("zdobny", "zdobni");

  set_long("Si�gaj�ca a� pod sufit szafa zamkni�ta jest wypuk�ymi,"
	+" dwuskrzyd�owymi drzwiami, kt�re pokrywaj� wymy�lne,"
	+" ro�linne wzory w formie splataj�cych si� ga��zek bluszczu."
	+" Mebel ustawiony jest na niewielkich, lekko wykr�conych"
	+" do wewn�trz n�kach - dziwne, �e s� one w stanie utrzyma�"
	+" taki ci�ar.\n");

  add_prop(CONT_I_WEIGHT, 35000);
  add_prop(CONT_I_VOLUME, 250000);
  add_prop(CONT_I_MAX_WEIGHT, 80000);
  add_prop(OBJ_I_NO_GET, "Chyba �artujesz...\n");
  add_prop(CONT_I_CLOSED, 1);

  set_no_show_composite(1);

  set_type(O_MEBLE);
    set_owners(({RINDE_NPC+"krepp"}));
}
