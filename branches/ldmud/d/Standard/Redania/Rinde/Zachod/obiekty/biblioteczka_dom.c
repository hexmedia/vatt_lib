/*
 * Autor: Rantaur
 * Datum: 05.06.2006
 * Biblioteczka do domu swiatynnego w Rinde
 *
 * >> Do biblioteczki w przyszlosci mozna wwalic jakies ksiazki/zwoje etc.
 * o ile komus bedzie sie chcialo napisac :P
 */

inherit "/std/container";

#include "dir.h"
#include <composite.h>
#include <macros.h>
#include <pl.h>
#include <stdproperties.h>
#include <object_types.h>

string pokaz_rzeczy();

void create_container()
{
  ustaw_nazwe("biblioteczka");
  dodaj_przym("niewysoki", "niewysocy");

  set_long("Wysoki na oko�o cztery �okcie rega� wykonany jest z ciemnego, d�bowego drewna,"
	   +" kt�re b�yszczy si� za ka�dym razem, gdy na� spojrzysz."
	   +" Mebel pozbawiony jest jakichkolwiek zdobie�, jednak mimo"
	   +" to prezentuje sie bardzo estetycznie i z pewno�ci� doda" 
	   +" elegancji ka�demu pomieszczeniu. Biblioteczk� wype�niaj�"
	   +" opas�e, pokryte do�� grub� warstw� kurzu ksi�gi - najwyra�niej"
	   +" ich w�a�ciciel ju� dawno zaprzesta� ich lektury.\n"
	   +"@@pokaz_rzeczy@@");

  add_item(({"ksi�gi w", "ksi��ki w"}), "R�nej grubo�ci ksi�gi wype�niaj�"
		  +" ka�d� p�k� biblioteczki. Na ich pokrytych kurzem,"
		  +" kunsztownie zdobionych garbach odczytujesz takie"
		  +" tytu�y jak: \"Poczet kr�l�w reda�skich\","
		  +" \"Wierzenia i rytua�y\", \"Zio�olecznictwo stosowane"
		  +"\".\n", PL_MIE);

  add_prop(CONT_I_WEIGHT, 1420);
  add_prop(CONT_I_VOLUME, 24000);
  add_prop(OBJ_I_NO_GET, "Przepe�niona ksi��kami biblioteczka wygl�da"
	   +" na dosy� ci�k� i niepor�czn�, chyba nie b�dziesz w stanie"
	   +" jej tak po prostu zabra�.\n");
  add_prop(CONT_I_DONT_SHOW_CONTENTS, 1);

  set_type(O_MEBLE);

  add_subloc("na");
  add_subloc_prop("na", CONT_I_MAX_WEIGHT, 700);
  add_subloc_prop("na", CONT_I_MAX_VOLUME, 1100);
   set_owners(({RINDE_NPC+"krepp"}));
}

string pokaz_rzeczy()
{
  string str = "";
  mixed *na_biblioteczce = this_object()->subinventory("na");
  mixed *w_biblioteczce = this_object()->subinventory();
  int size_na = sizeof(na_biblioteczce);
  int size_w = sizeof(w_biblioteczce);

  if (!size_w && !size_na)
  return "";

  if(size_na > 0 && size_w > 0)
    {
      str += "Na biblioteczce niedbale po�o�ono ";
      str += COMPOSITE_DEAD(na_biblioteczce, PL_BIE);
      str += ", za� w jej wn�trzu dostrzegasz ";
      str += COMPOSITE_DEAD(w_biblioteczce, PL_BIE);
      str += ".\n";
      return str;
    }

  if(size_na > 0)
    {
      str += "Na biblioteczce niedbale po�o�ono ";
      str += COMPOSITE_DEAD(na_biblioteczce, PL_BIE);
      str += ".\n";
      return str;
    }
  
  if(size_w > 0)
    {
      str += "W jej wn�trzu dostrzegasz ";
      str += COMPOSITE_DEAD(w_biblioteczce, PL_BIE);
      str += ".\n";
      return str;
    }
}


