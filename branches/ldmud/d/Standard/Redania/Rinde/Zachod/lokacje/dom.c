/*
 * Autor: Rantaur
 * Data: 18.06.06
 * Opis: Lokacja domu swiatynnego w Rinde
 */
#include "dir.h"

inherit RINDE_STD;

#include <pl.h>
#include <macros.h>
#include <stdproperties.h>
#include <mudtime.h>

string jaka_pora();
string jest_dywan();
string jest_krzeslo();

void create_rinde()
{
  set_short("Dom �wi�tynny");
  set_long("@@jaka_pora@@ Mimo i� w pokoju nie ma jaki�"
	   +" szczeg�lnych ozd�b, to sprawia on wra�enie przytulnego,"
	   +" a za razem bardzo eleganckiego. Zachodni� scian� zajmuje"
	   +" okaza�a szafa oraz niewysoka biblioteczka wype�niona przer�nymi"
	   +" ksi�gami. Pod oknem za� stoi masywne, d�bowe biurko @@jest_krzeslo@@"
	   +" @@jest_dywan@@\n");

  add_object(ZACHOD_DRZWI+"z_domu.c");

  add_prop(ROOM_I_INSIDE, 1);

  add_object(RINDE+"przedmioty/dywan_dom.c");

  add_object(MEBLER+"krzeslo_dom.c");

  add_object(ZACHOD_OBIEKTY+"biblioteczka_dom.c");
  add_object(ZACHOD_OBIEKTY+"biurko_dom.c");
  add_object(ZACHOD_OBIEKTY+"szafa_dom.c");
  add_object(ZACHOD_OBIEKTY+"lozko_dom.c");

  dodaj_rzecz_niewyswietlana("ciemne drewniane biurko");
  dodaj_rzecz_niewyswietlana("du�e mi�kkie �o�e");
  dodaj_rzecz_niewyswietlana("niewysoka biblioteczka");
  dodaj_rzecz_niewyswietlana("bia�y kosmaty dywan");
  dodaj_rzecz_niewyswietlana("wygodne rze�bione krzes�o");

}
public string
exits_description()
{
    return "Wyj^scie prowadzi na ulice.\n";
}

string jaka_pora()
{
  string str;

  if (!this_object()->dzien_noc())
    {
      str = "Wpadaj�ce przez okno promienie �wiat�a rozpraszaj� si�"
	+" na wszechobecnych drobinkach kurzu, tworz�c delikatne,"
	+" migocz�ce smugi, ktore obijaj� si� od l�ni�cych powierzchni"
	+" znajduj�cych si� tu mebli.";
      return str;
    }
  else
    {
      str = "Blade �wiat�o gwiazd zagl�da nie�mia�o do pomieszczenia,"
	+" otulaj�c ka�dy przedmiot srebrzyst�, eteryczn� po�wiat�, kt�ra"
	+" rodzi kr�tkie, sennie drgaj�ce cienie.";
      return str;
    }
}

string jest_dywan()
{
  string str;
  object dywan = present("dywan", TO);

  if(!dywan)
    {
      str = "Pokryta l�ni�cym lakierem, ciemna pod�oga wygl�da zimno i"
	+" nieprzyjemnie - przyda�by tu si� jaki� dywan.";
      return str;
    }

  if(dywan && (!dywan->query_prop(OBJ_M_NO_GET)))
    {
      str = "W g��bi pomieszczenia, tu�"
	+" przy znajduj�cym si� po lewej stronie �o�u, le�y zwiniety"
	+" dywan, nie wyglada on zbyt estetycznie.";
      return str;
    }
 str = "W g��bi pomieszczenia, tu�"
    +" przy znajduj�cym si� po lewej stronie �o�u, le�y bia�y,"
    +" kosmaty dywan, kt�ry doskonale konstastuje z ciemnobr�zow� pod�og�.";
  return str;
}

string jest_krzeslo()
{
  if(present("krzes�o", TO))
    return ", o dziwo nie zauwa�asz przy nim �adnego krzes�a.";
  return "z dostawionym do� krzes�em o lekko kremowym obiciu.";
}
