inherit "/std/pattern";

#include <pattern.h>
#include "dir.h"

public void
create()
{
  set_pattern_domain(PATTERN_DOMAIN_TAILOR);
  set_item_filename(RINDE_UBRANIA +"buty/wygodne_czarne_Mc.c");
  set_estimated_price(139);
  set_time_to_complete(7200);
  enable_want_sizes();
}
