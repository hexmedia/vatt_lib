
#include "dir.h"

inherit RINDE_STD;
inherit "/lib/store_support";
#include <stdproperties.h>
#include <macros.h>
#define REP_MAT "/d/Standard/items/materialy/"

void create_rinde()
{
    set_short("Tajny magazyn");
    set_long("Jeste� w tajnym magazynie. Nie ma st�d �adnego wyj�cia!\n");
    add_prop(ROOM_I_INSIDE,1);
    add_prop(ROOM_I_ALWAYS_LOAD, 1);

    add_object(REP_MAT + "sk_swinska");
    add_object(REP_MAT + "sk_swinska");
    add_object(REP_MAT + "sk_krolicza");
    add_object(REP_MAT + "sk_cieleca");
    add_object(REP_MAT + "sk_cieleca");
    add_object(REP_MAT + "sk_wilcza");
    add_object(REP_MAT + "sk_niedzwiedzia");
    add_object(REP_MAT + "tk_atlas");
    add_object(REP_MAT + "tk_jedwab");
    add_object(REP_MAT + "tk_len");
    add_object(REP_MAT + "tk_szyfon");
    add_object(REP_MAT + "tk_welna");
    add_object(REP_MAT + "sk_wilcza");
    add_object(REP_MAT + "sk_niedzwiedzia");
    add_object(REP_MAT + "tk_atlas");
    add_object(REP_MAT + "tk_jedwab");
    add_object(REP_MAT + "tk_len");
    add_object(REP_MAT + "tk_szyfon");
    add_object(REP_MAT + "tk_welna");
    add_object(REP_MAT + "zelazo_sztabka");
    //add_object(RINDE_NARZEDZIA + "nozyczki");<--szukaj w Inv. craftsmana!
    add_object(RINDE_UBRANIA +"czarna_jedwabna_chusta.c",3);
}

void                       /* wywolywane za kazdym razem, jak jakis */
enter_inv(object ob, object skad)  /* obiekt wchodzi do wnetrza tego obiektu */
{                  /* ...czyli do pokoju */

    ::enter_inv(ob, skad); /* trzeba ZAWSZE wywolac gdy redefiniujemy enter_inv */
    
    store_update(ob); /* dba o to, zeby magazynie nie bylo za duzo rzeczy */
}
void
leave_inv(object ob, object to)
{
    //::leave_env(ob,to);
    store_add_item(ob);
}
