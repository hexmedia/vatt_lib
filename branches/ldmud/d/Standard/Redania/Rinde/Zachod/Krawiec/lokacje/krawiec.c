
/* Autor: Avard
   Opis : Eria
   Data : 31.03.07 */

#include "dir.h"

inherit RINDE_STD;

#include <stdproperties.h>
#include <mudtime.h>
#include <wa_types.h>
#include <ss_types.h>
#include <macros.h>
#include <pattern.h>

string dlugi();

void
create_rinde()
{
    set_short("Warsztat krawca");
    set_long(&dlugi());

    add_prop(ROOM_I_INSIDE,1);

    add_npc(RINDE_NPC + "krawiec");

    add_object(ZACHOD_DRZWI + "drzwi_z_krawca.c");
    add_object(MEBLER + "stol_krawca");
    add_object(MEBLER + "lada_do_krawca");
    dodaj_rzecz_niewyswietlana("niska pod^lu^zna lada",1);
    dodaj_rzecz_niewyswietlana("du^zy drewniany st^o^l",1);
}

public string
exits_description()
{
    return "Wyj^scie z warsztatu prowadzi na ulic^e.\n";
}

string
dlugi()
{
    string str;
    str = "Znajdujesz si^e w do^s^c du^zym pomieszczeniu, ";
    if(jest_dzien() == 1)
    {
        str += "za dnia, jasno o^swietlonym ^swiat^lem s^lonecznym ";
    }
    else
    {
        str += "noc^a jasno o^swietlonym blaskiem ksi^e^zyca ";
    }
    str += "wpadaj^acym przez du^ze okna, przys^loni^ete jedynie zwiewn^a, "+
        "prze^xroczyst^a tkanin^a. ";
    if(jest_rzecz_w_sublokacji(0, "niska pod^lu^zna lada") == 1
        && jest_rzecz_w_sublokacji(0,"du^zy drewniany st^o^l") == 0)
    {
        /*Miejsce to jest niezwykle zagracone, przez r^o^znego rodzaju "+
        "tkaniny, materia^ly i co dziwniejsze rodzaje sk^or, kt^ore cz^esto "+
        "le^z^a na ziemi, pouk^ladane niedbale na niema^l^a kupk^e. Szafki "+
        "i p^o^lki r^ownie^z zajmuj^a podobne rzeczy, kt^ore a^z uginaj^a "+
        "si^e pod nadmiarem ci^e^zaru. */
        str += "Na ^srodku pomieszczenia znajduje si^e sporej wielko^sci "+
            "lada.";
    }
    if(jest_rzecz_w_sublokacji(0, "niska pod^lu^zna lada") == 1
        && jest_rzecz_w_sublokacji(0,"du^zy drewniany st^o^l") == 1)
    {
        str += "Na ^srodku pomieszczenia znajduje si^e sporej wielko^sci "+
            "lada, a za ni^a jeszcze wi^ekszy st^o^l, na kt^orym "+
            "poniewieraj^a si^e jakie^s przybory do szycia, co mo^zna "+
            "^latwo wywnioskowa^c po kilku parach no^zyc czy te^z po "+
            "srebrnych, po^lyskuj^acych igie^lkach. ";
    }
    if(jest_rzecz_w_sublokacji(0, "niska pod^lu^zna lada") == 0
        && jest_rzecz_w_sublokacji(0,"du^zy drewniany st^o^l") == 1)
    {
        str += "Po przeciwleg^lej stronie od drzwi wej^sciowych stoi "+
            "do^s^c du^zy st^o^l, na kt^orym poniewieraj^a si^e jakie^s "+
            "przybory do szycia, co mo^zna ^latwo wywnioskowa^c po kilku "+
            "parach no^zyc czy te^z po srebrnych, po^lyskuj^acych "+
            "igie^lkach. ";
    }
    str += "W pomieszczeniu panuje duchota i unosi sie do^s^c specyficzny "+
        "zapach, kt^ory z pewno^sci^a pochodzi od niekt^orych dziwnie "+
        "wygl^adaj^acych materia^l^ow. \n";
    return str;
}
