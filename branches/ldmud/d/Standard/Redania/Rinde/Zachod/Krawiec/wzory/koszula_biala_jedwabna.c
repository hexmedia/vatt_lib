inherit "/std/pattern";

#include <pattern.h>
#include "dir.h"

public void
create()
{
  set_pattern_domain(PATTERN_DOMAIN_TAILOR);
  set_item_filename(RINDE_UBRANIA +"koszule/biala_jedwabna_Mc");
  set_estimated_price(120);
  set_time_to_complete(7200);
  enable_want_sizes();
}