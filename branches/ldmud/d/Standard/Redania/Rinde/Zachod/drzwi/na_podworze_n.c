
/* Autor: Avard
   Data : 10.08.06
   Info : Brama na jednej z ulic Rinde */

inherit "/std/door";

#include <pl.h>
#include "dir.h"
#include <stdproperties.h>

void
create_door()
{
    ustaw_nazwe("brama");
    dodaj_przym("drewniany", "drewnani");
    dodaj_przym("dwudrzwiowy","dwudrzwiowi");

    set_other_room(ZACHOD_LOKACJE+"ulica6");
    set_door_id("BRAMA_NA_PODWORZE_RINDE_N");
    set_door_desc("Dwudrzwiowa brama zosta^la umiejscowiona w "+
        "po^ludniowej ^scianie.\n");

    set_open_desc("");
    set_closed_desc("");

    set_pass_command(({"brama","po^ludnie","po^ludniowa brama",
        "przejdz przez bram^e","przejdz przez po^ludniow^a bram^e"}),
        "przez bram^e na po^ludnie","zza bramy z po^ludnia");

    set_key("KLUCZ_DO_BRAMY_NA_PODWORZE_RINDE_N");
    set_lock_name(({"zamek", "zamku", "zamkowi", "zamek", "zamkiem", "zamku"}), 0, PL_MESKI_NOS_NZYW);
    add_prop(DOOR_I_HEIGHT, 240);
}
