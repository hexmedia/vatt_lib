/*
 * Autor: Rantaur
 * Data: 18.06.06
 * Opis: Drzwi wejsciowe do domu swiatynnego w Rinde
 */

inherit "/std/door";

#include <pl.h>
#include "dir.h"
void
create_door()
{
    ustaw_nazwe("drzwi");

    dodaj_przym("drewniany", "drewniani");

    set_other_room(ZACHOD_LOKACJE+"dom.c");

    set_door_id("DRZWI_RINDE_DOM_SWIATYNNY");

    set_door_desc("Matowe drzwi najlepsze lata maja ju� z"
        +" pewno�ci� za sob�, jednak mimo to wci�� wygl�daj� bardzo"
        +" solidnie. Pod mosi�na klamka zauwa�asz niewielk� dziurk�"
        +" na klucz.\n");

 /* set_open_desc("Prowadz�ce do domu drzwi s� otwarte.\n");
    set_closed_desc("Prowadz�ce do domu drzwi s� zamkni�te.\n"); */

    set_open_desc("");
    set_closed_desc("");

    set_lock_desc("Zamek w tych drzwiach nie jest niczym niezwyk�ym."
        +" Ot, zwyk�a dziurka na klucz.\n");

    set_pass_command(({"drzwi","dom","dom ^swi^atynny","wejd^x do domu",
        "wejd^x do domu ^swi^atynnego","przejd^x przez drzwi do domu",
        "przejd^x przez drzwi do domu ^swi^atynnego"}),"przez drzwi",
        "z zewn^atrz");

    set_key("KEY_RINDE_DOM_SWIATYNNY_DRZWI");
    set_lock_name(({"zamek", "zamku", "zamkowi", "zamek", "zamkiem", "zamku"}), 0, PL_MESKI_NOS_NZYW);

    set_open(0);
    set_locked(1);
}
