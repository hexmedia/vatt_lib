inherit "/std/key";

#include <stdproperties.h>
#include <pl.h>
#include "dir.h"

void
create_key()
{
  ustaw_nazwe("klucz");
  dodaj_przym("�elazny", "�ela�ni");
  dodaj_przym("b�yszcz�cy", "b�yszcz�cy");

  set_long("Do�� d�ugi klucz posiada szereg z�bk�w ro�nej"
          +" d�ugo�ci.\n");

  set_key("KEY_RINDE_DOM_SWIATYNNY_DRZWI");

  add_prop(OBJ_I_WEIGHT, 30);
  add_prop(OBJ_I_VOLUME, 21);

    set_owners(({RINDE_NPC + "krepp"}));
}
