/* Jest to kosz kwiaciarki chodzacej do placu Rinde. Kosz jest chwytywalnym
   pojemnikiem, a zarazem magazynem (handlowym) na kwiaty.
    Lil. */

inherit "/std/holdable_container";
inherit "/lib/store_support";

#include <materialy.h>
#include <macros.h>
#include <pl.h>
#include <object_types.h>
#include <stdproperties.h>
#include "dir.h"

string dlugi_dlugasny();

create_holdable_container()
{
    ustaw_nazwe("kosz");
    dodaj_przym("wiklinowy","wiklinowi");
    set_long("@@dlugi_dlugasny@@");
    
    setuid();
    seteuid(getuid());
    
    add_prop(CONT_I_VOLUME, 16800);
    add_prop(CONT_I_WEIGHT, 1040);
    add_prop(CONT_I_MAX_WEIGHT, 20000);
    add_prop(CONT_I_MAX_VOLUME, 20000);
    set_hands(W_BOTH);
    add_subloc("w");
    add_subloc_prop("w", CONT_I_MAX_VOLUME, 500);
    add_subloc_prop("w", CONT_I_MAX_WEIGHT, 500);
    
    ustaw_material(MATERIALY_WIKLINA);

    set_default_stock( ({ RINDE + "przedmioty/kwiaty/czarna_roza", 13,
			RINDE + "przedmioty/kwiaty/czerw_roza", 25,
			RINDE + "przedmioty/kwiaty/fio_lilia", 15 }) );
    reset_store();
}



void                               /* wywolywane za kazdym razem, jak jakis */
enter_inv(object ob, object skad)  /* obiekt wchodzi do wnetrza tego obiektu */
{                                  /* ...czyli do pokoju */

    ::enter_inv(ob, skad); /* trzeba ZAWSZE wywolac gdy redefiniujemy enter_inv */

    store_update(ob); /* dba o to, zeby magazynie nie bylo za duzo rzeczy */
}


string
dlugi_dlugasny()
{
    string str;
    
    /*if(subloc_items("w"))
    {
    str+="Spory wiklinowy kosz po brzegi wype�niony barwnymi ";
         
    if(environment(this_ob"przywi�d�ymi "
     "kwiatami."
    }      
    MNIEJSZA Z TYM ;P */
 
    str="R�czka jest szeroka i nieco ja�niejsza, musia�a by� "+
      "dopleciona nieco p�niej. Mimo i� wiklina po brzegach "+
      "�amie si� i kruszy, jednak sama konstrukcja jest mocna "+
      "i zapowiada si� na to, �e wytrzyma jeszcze wiele lat.\n";
    return str;
}