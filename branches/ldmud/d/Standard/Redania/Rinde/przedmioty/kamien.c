inherit "/std/object.c";

#include "dir.h"
#include <pl.h>
#include <stdproperties.h>
#include <macros.h>
#include <filter_funs.h>
#include <materialy.h>

string dlugi_opis();

string *losowy_przym()
{
   string *lp;
   string *lm;
   int i;
   lp=({ "niepozorny", "ma^ly", "czarny", "szary", "brunatny",
   "niewielki", "okr�g�y", "owalny", "g�adki", "pod�u�ny", "czerwonawy"});
   lm=({"niepozorni", "mali", "czarni", "szarzy",  "brunatni",   
   "niewielcy", "okr�gli", "owalni", "g�adcy", "pod�u�ni", "czerwonawi"});
   i=random(11);
   return ({ lp[i] , lm[i] });
}

void
create_object()
{
   string *przm = losowy_przym();

    ustaw_nazwe("kamien");

    dodaj_przym( przm[0], przm[1] );
    set_long(dlugi_opis());

    add_prop(OBJ_I_WEIGHT, 3+random(200));
    add_prop(OBJ_I_VOLUME, query_prop(OBJ_I_VOLUME)+random(100));
    add_prop(OBJ_I_VALUE, 0);
    ustaw_material(MATERIALY_GRANIT);
} 
string dlugi_opis()
{
	string str;

	switch(random(4))
	{
	 case 0 : str="Zwyk�y "+this_object()->short()+", jakich "+
		  "wsz�dzie pe�no.\n";break;
	 case 1 : str="To wspania�y okaz kamieni. Cechuje go "+
		  "wymy�lny kszta�t i niejednolity kolor. Potocznie "+
		  "zwane s� przez plebs otoczakami.\n";break;
	 case 2 : str="Jest niezwyczajny w swej zwyczajno�ci. "+
		  "Inspiruj�ce...\n";break;
   /*odjebalo mi...Opisywanie zwyklych przedmiotow to straszna nuda :P */
	 case 3 : str="Oto jest kamie�. Mo�e nie jest jedynym "+
		  "w swoim rodzaju, lecz na pewno ma to co�, czego "+
		  "mog�yby pozazdro�ci� wszystkie inne kamienie! "+
		  "Mo�e kszta�t, lub kolor? W ka�dym b�d� razie "+
		  "kamienie S� bardzo wa�nym surowcem, cz�sto wykorzystywanym "+
		  "w nielada trudnych zadaniach, takich jak le�enie "+
		  "w szeregach, rz�d za rz�dem na go�ci�cach, "+
		  "bez wzgl�du na por� dnia, pogod�, lub jakikolwiek "+
		  "inny dyskomfort. Maj� tak�e swoje szerokie "+
		  "zastosowanie w dziedzinach batalistycznych - "+
		  "raczej nikt by nie chcia�, by jego g�owa "+
		  "do�wiadczy�a spotkania bliskiego stopnia z kamieniem. "+
		  "Jednak wszystko to nie dotyczy tego niewinnego, "+
		  "bezbronnego i jak�e skromnego kamyczka, kt�ry "+
		  "stara si� nikomu nie wchodzi� w drog�. "+
		  "P�ki co jest jeszcze zbyt m�ody, aczkolwiek wygl�da "+
		  "na takiego, co widzia� ju� to i owo, jest w �wietnej "+
		  "kondycji, a co najwa�niejsze: ma ambicj�, by dorosn�� "+
		  "i sta� si� wielkim jak Ty! Wtedy dopiero by�oby na "+
		  "co patrze�... A zatem, je�li troszczysz si� o los "+
		  "innych (nawet, je�li s� kamieniami) - zostaw go "+
		  "czym pr�dzej i daj w b�ogim spokoju dorosn��, do "+
		  "rozmiar�w, dzi�ki kt�rym, mi�dzy innymi, b�dzie ju� "+
		  "m�g� pos�u�y� za kostk� brukow�...\n";break;
	}
   return str;
}
