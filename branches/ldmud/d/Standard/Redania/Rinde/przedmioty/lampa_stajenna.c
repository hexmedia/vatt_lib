/* Takie tam pierdo�y le��ce na szafce w stajni karczmy Baszta.
                           Lil & Alcyone 04.2005*/

inherit "/std/object.c";

#include "dir.h"
#include <pl.h>
#include <stdproperties.h>
#include <macros.h>
#include <filter_funs.h>

int zapal_lampe();

create_object()
{
    ustaw_nazwe(({"lampa","lampy","lampie","lamp�","lamp�","lampie"}),
               ({"lampy","lamp","lampom","lampy","lampami","lampach"}),
                  PL_ZENSKI);
                 
    dodaj_przym("niewielki","niewielcy");
    dodaj_przym("zniszczony","zniszczeni");

    set_long("Stara, pordzewia�a lampa nie nadaje si� ju� do uzytku. "+
             "Pop�kana i powgniatana mog�aby zrobi� wi�cej szkody ni�li "+
	     "po�ytku. Niewielka paj�czyna w jej wn�trzu �wiadczy o "+
	     "tym, �e s�u�y ona teraz za mieszkanie dla ma�ych owad�w.\n");

    add_prop(OBJ_I_WEIGHT, 132);
    add_prop(OBJ_I_VOLUME, 422);
    add_prop(OBJ_I_VALUE, 0);

    add_cmd_item(({"lamp�","niewielk� lamp�","zniszczon� lamp�",
                   "niewielk� zniszczon� lamp�"}), "zapal", zapal_lampe,
                   "Zapal co?\n"); 
}

int
zapal_lampe()
{
   write("Pr�bujesz zapali� "+this_object()->short(PL_BIE)+
         ", jednak okazuje si� to niewykonalne.\n");
   saybb(QCIMIE(this_player(), PL_MIA) + " pr�buje zapali� "+this_object()->short(PL_BIE)+
         ", jednak okazuje si� to niewykonalne.\n");
   return 1;
}