
/* szerokie zabrudzone spodenki kolcze made by ulryk 26.05.06 
   poprawki by Avard */
/* Uzyte:
Ochroniarze fryzjera z Rinde 
(/d/Standard/Redania/Rinde/npc/krasnolud_fryzjera1.c)
(/d/Standard/Redania/Rinde/npc/krasnolud_fryzjera2.c)
(/d/Standard/Redania/Rinde/npc/krasnolud_fryzjera3.c)
*/

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <macros.h>
#include <wa_types.h>
#include <materialy.h>
#include <pl.h>
#include <object_types.h>

void
create_armour()
{
    ustaw_nazwe("spodenki kolcze");
    dodaj_nazwy(({"spodenki","spodenek","spodenkom","spodenki","spodenkami",
        "spodenkach"}), PL_MESKI_NOS_NZYW);
    dodaj_przym("szeroki","szerocy");
    dodaj_przym("zabrudzony","zabrudzeni");
    set_long("Spodenki te na pierwszy rzut oka wydaj^a si^e niezbyt solidne, "+
        "jednak nic bardziej mylnego. S^a splecione z bardzo du^zych i "+ 
        "grubych jak na kolczuge metalowych k^o^lek, kt^ore potrafi^a "+
        "sparowa^c naprawde mocne ciosy i ciecia. Nie pomog^a jednak w "+
        "przypadku pchni^ecia sztyletem, b^ad^x trafieniu przez strza^le. "+
        "Jak wida^c w^la^sciciel nie troszczy^l si^e o nie specjalnie, gdy^z "+
        "to co wydaje si^e by^c rdz^a tak naprawde jest zaschni^etym "+
        "piwem. \n");

    set_slots(A_THIGHS, A_LEGS, A_HIPS);
    ustaw_material(MATERIALY_STAL);
    set_type(O_ZBROJE);
    add_prop(OBJ_I_WEIGHT, 3300);
    add_prop(OBJ_I_VOLUME, 3100);
    add_prop(OBJ_I_VALUE, 400);
    add_prop(ARMOUR_S_DLA_RASY, "krasnolud");
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    set_size("M");
    set_ac(A_LEGS,11,12,3, A_HIPS,11,12,3);

    set_likely_cond(11);
}
