
/* Autor: Avard
   Opis : Ghallerian
   Data : 19.06.05 */

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <macros.h>
#include <materialy.h>
#include <object_types.h>

void
create_armour()
{
    ustaw_nazwe("helm");
    dodaj_przym("du^zy","duzi");
    dodaj_przym("czarny","czarni");
    set_long("Wykonany starannie i dok^ladnie he^lm zyska^l doskona^le "+
        "kszta^lty. Wydawa^c by sie mog^lo, ^ze nie da si^e tak wspaniale "+
        "wymodelowa^c stali. A jednak p^latnerz dokona^l tego. He^lm jest "+
        "do^s^c wysoki i ko^nczy si^e szpicem. Od ^srodka wylo^zony jest "+
        "mi^ekk^a sk^or^a wi^ec przyjemnie si^e go nosi.\n");

    set_slots(A_HEAD);
    add_prop(OBJ_I_WEIGHT, 500);
    add_prop(OBJ_I_VOLUME, 500);
    add_prop(OBJ_I_VALUE, 2640);
    ustaw_material(MATERIALY_STAL, 70);
    ustaw_material(MATERIALY_SK_SWINIA, 30);
    set_type(O_ZBROJE);
    set_ac(A_HEAD,13,15,15,);
    add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    set_size("M");
}
