#include "/sys/stdproperties.h"
#include "/sys/macros.h"
#include <object_types.h>

inherit "/std/object";
inherit "/lib/kwiat";

int zgniec(string str);

void
create_object()
{
 ustaw_nazwe("lilia");
 dodaj_nazwy("kwiat");
 dodaj_przym("fioletowy", "fioletowi");

 set_long("Kwiat ten jest niezwyk�ej urody. Cieniutkie, lancetowate " +
          "p�atki zawijaj� si� stopniowo ku do�owi nadaj�c p�kowi wyj�tkowy kszta�t. " +
          "Barwa p�atk�w r�wnie� nie pozostawia wiele do �yczenia - " +
          "wewn�trz p�ka s� krystalicznie bia�e, za� ich brzegi zabarwione " +
          "s� na intensywny, fioletowy kolor. Ro�lina wydziela bardzo silny i " +
          "przyjemny aromat.\n");

 add_prop(OBJ_I_WEIGHT, 47);
 add_prop(OBJ_I_VOLUME, 73);
 add_prop(OBJ_I_VALUE, 18);

 set_type(O_KWIATY);
 set_wplatalny(1);

   jak_pachnie="@@zapach@@";
   jak_wacha_jak_sie_zachowuje="@@reakcja@@";
}

init()
{
 ::init();
 init_kwiat();
 add_action(zgniec, "zgnie�");
}

int
zgniec(string str)
{
 object lilia;

 if (parse_command(str, all_inventory(this_player()), "%o:" + PL_BIE, lilia) &&
    lilia == this_object())
 {
  write("Zamykasz w d�oni delikatne p�atki fioletowej lilii " +
        "i gwa�townym ruchem mia�d�ysz kwiat w palcach " +
        "wy�adowywuj�c sw� w�ciek�o��. " +
        "Chwil� po tym lekkim ruchem r�ki odrzucasz zniszczony " +
        "kwiat na bok.\n");
  saybb(QCIMIE(this_player(), PL_MIA)+" mru�y oczy z w�ciek�o�ci� " +
        "mia�d��c w d�oni delikatne p�atki fioletowej lilii. " +
        "Chwil� potem zniszczony kwiat l�duje na ziemi, a jego " +
        "pomi�te p�atki wiruj� przez chwil� w powietrzu" +
        (environment(this_player())->query_prop(ROOM_I_INSIDE) ?
        " i opadaj� na ziemi�.\n" : " po czym odlatuj� w dal niesione " +
        "podmuchem wiatru.\n"));
  remove_object();
  return 1;
 }
 notify_fail("Zgnie� co ?\n");
 return 0;
}

string
zapach(string str)
{
        str = "Zamykaj�c oczy unosisz powoli kwiat ku swej twarzy i " +
        "delektujesz si� jego intensywnym, zmys�owym zapachem, kt�ry " +
        "przenika ka�d� czastk� twego cia�a.";
  return str;
}

string
reakcja(string str)
{
   str = "zbli�a do twarzy fioletow� lili� " +
          "i napawa si� jej delikatnym zapachem.";
  return str;
}
