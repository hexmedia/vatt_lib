/*zrobione przez valor dnia 3 listopada 2006
*/
inherit "/std/armour";
#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <object_types.h>
#include <macros.h>
#include <materialy.h>
void
create_armour()
{  
    ustaw_nazwe("koszula");
    dodaj_przym("b^l^ekitny","b^l^ekitni");
    dodaj_przym("cerowany", "cerowani");
    set_long("Koszula ta uszyta z pofarbowanego na b^l^ekitno lnu wi^azana "+
        "jest pod szyj^a rzemieniem. Zapewne mia^la kiedy^s d^lugie r^ekawy, "+
        "lecz zosta^ly obci^ete pozostawiaj^ac po sobie kr^otkie do ^lokci, "+
        "postrz^epione r^ekawki, z wisz^acymi gdzieniegdzie pojedynczymi "+
        "fr^edzelkami. Miejscami dostrzec mo^zna ^slady po cerowaniu oraz "+
        "ma^la ^latke pod lew^a pach^a. Mimo swej prostoty koszula jest "+
        "raczej wygodna a jej spory rozmiar zapewnia komfort noszenia.\n");
 
    set_slots(A_TORSO, A_FOREARMS, A_ARMS);
    add_prop(OBJ_I_WEIGHT, 200); 
    add_prop(OBJ_I_VOLUME, 200);
    add_prop(OBJ_I_VALUE, 50);
    add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    set_size("M");
    ustaw_material(MATERIALY_LEN, 100);
    set_type(O_UBRANIA);
    set_likely_cond(22);
}