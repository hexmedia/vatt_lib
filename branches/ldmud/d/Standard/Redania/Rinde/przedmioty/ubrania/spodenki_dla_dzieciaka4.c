/*zrobione przez valor dnia 11 lipca
*/

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <macros.h>
#include <materialy.h>
#include <object_types.h>


void
create_armour() 
{
      ustaw_nazwe_glowna("para");
      ustaw_nazwe("spodnie");
      

     dodaj_przym("jasnobr^azowy","jasnobra^azowi");
     dodaj_przym("wytarty","wytarci");
      
     set_long("Spodenki te wykonano z jasnobr^azowej tkaniny. Niegdy^s czysty "+
		 "i barwny materia^l zosta^l przykryty grub^a warstw^a kurzu i brudu, "+
		 "a kolana spodni wypchane, pozbawione swego dawnego koloru. W paru "+
		 "miejscach mo^zna rownie^z dostrzec zszycia oraz ma^l^a ^latk^e "+
		 "naszyt^a na nogawce. \n");

     set_slots(A_LEGS);
     add_prop(OBJ_I_VOLUME, 200);
     add_prop(OBJ_I_VALUE, 50);
     add_prop(OBJ_I_WEIGHT, 100);

     add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
     add_prop(ARMOUR_I_DLA_PLCI, 0);
     
     set_size("XS");

     ustaw_material(MATERIALY_LEN, 100);

     set_type(O_UBRANIA);

     set_likely_cond(23);
}