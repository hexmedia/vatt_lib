inherit "/std/armour";

#include <pl.h>
#include <wa_types.h>
#include <stdproperties.h>
#include <materialy.h>
#include <macros.h>
#include <object_types.h>

void
create_armour()
{
    ustaw_nazwe("spodnie");
   ustaw_nazwe_glowna("para");

    dodaj_przym("czarny", "czarni");
    dodaj_przym("zwyk^ly", "zwykli");

    set_long("Najzwyklejsze czarne spodnie, wykonane zosta^ly z jakiego� " +
	"nierozci�gliwego materia^lu, pozwalajac na lu�ne u^lo^zenie. U g^ory " +
	"znajduje si^e kilka szlufek, w razie jakby osoba je nosz�ca " +
	"potrzebowa^la do nich paska, z kolei nogawki posiadaj� lekkie " +
	"rozci^ecie, ^zeby dobrze uk^lada^c si^e na wszelkiego rodzaju butach.\n");

    set_slots(A_LEGS);
//    set_ac();

   add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
   set_size("M");
   add_prop(OBJ_I_VALUE, 67);
   add_prop(OBJ_I_WEIGHT, 1384);
   add_prop(OBJ_I_VOLUME, 750);
   set_type(O_UBRANIA);
   ustaw_material(MATERIALY_LEN);
   set_likely_cond(21);
}