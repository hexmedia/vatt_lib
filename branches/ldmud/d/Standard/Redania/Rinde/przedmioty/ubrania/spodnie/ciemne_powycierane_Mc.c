
/* Autor: Avard
   Opis : Arivia
   Data : 31.03.07 */

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <pl.h>
#include <macros.h>
#include <materialy.h>
#include <object_types.h>

void
create_armour()
{
    ustaw_nazwe("spodnie");
    ustaw_nazwe_glowna("para");

    dodaj_przym("ciemny","ciemni");
    dodaj_przym("powycierany","powycierani");
 

    set_long("Ju^z na pierwszy rzut oka wida^c, ^ze spodnie licz^a sobie "+
        "dobrych par^e lat, a ^swiadczy o tym odbarwiony i miejscami "+
        "przetarty materia^l. To skromne ubraie zosta^lo uszyte z "+
        "zabarwionej na ciemnobr^azowy kolor bawe^lny, na kt^orej up^lyw "+
        "czasu wyra^xnie odcisn^a^l swoje pi^etno. Poza czarnymi "+
        "rzemieniami, s^lu^z^acymi do zasznurowania spodni, nie da si^e "+
        "zauwa^zy^c ^zadnych ozd^ob, kt^ore cho^cby w niewielkim stopniu "+
        "mog^ly uatrakcyjni^c odzienie.\n");

    set_slots(A_LEGS, A_HIPS);
    add_prop(OBJ_I_VOLUME, 1000);
    add_prop(OBJ_I_WEIGHT, 837);
    add_prop(OBJ_I_VALUE, 75);
    ustaw_material(MATERIALY_LEN, 100); 
    set_type(O_UBRANIA);
    add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    set_size("M");
    set_likely_cond(23);
}
