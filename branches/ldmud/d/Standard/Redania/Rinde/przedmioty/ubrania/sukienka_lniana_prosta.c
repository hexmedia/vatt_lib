/* Lnian� prost� sukienk� u�ywa si� w:
 *  - jako ubranie dla kwiaciarki chodz�cej po placu Rinde.
 *
 */

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <macros.h>
#include <materialy.h>
#include <object_types.h>

void
create_armour()
{  
      /* By Alcyone, Lipiec 2005 */ 
  
    ustaw_nazwe("sukienka");
    dodaj_przym("lniany","lniani");
    dodaj_przym("prosty", "pro�ci");
    set_long("Zwyk�a lniana sukienka powinna si�ga� "+
             "d�ugo�ci� a� do kostek. "+
             "Przy cienutkich rami�czkach, kt�re j� trzymaj�, "+
	     "przyszyto male�kie, drobne r�yczki. Farbowany "+
	     "na zielono len, jest mi�kki i wygl�da na do�� "+
             "wytrzyma�y. Cienki i zwiewny materia� nadaje si� "+
	     "raczej na cieplejsze i pogodne "+
             "dni. Sukienka nie wygl�da na atrakcyjn�, ale jest "+
	     "funkcjonalna i nie kr�puje "+
             "zbytnio ruch�w. Jest prosta i lekko skrojona w pasie, "+
	     "a dekolt nie ods�ania wiele.\n");


    set_type(O_UBRANIA);
    ustaw_material(MATERIALY_LEN);
    set_slots(A_BODY, A_LEGS);
    add_prop(ARMOUR_F_PRZELICZNIK, 40.0); //rozciagliwosc
    add_prop(ARMOUR_I_D_ROZCIAGLIWOSC, 40); //rozciagliwosc w dol na 40%
    add_prop(ARMOUR_I_U_ROZCIAGLIWOSC, 30); //rozciagliwosc w gore na 30%
    add_prop(OBJ_I_WEIGHT, 730);
    add_prop(OBJ_I_VOLUME, 1190);
    add_prop(OBJ_I_VALUE, 12);

    set_likely_cond(19);
}