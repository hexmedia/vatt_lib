/*
 * Nosi go m.in. piekarz w Rinde
 */

inherit "/std/armour";

#include <wa_types.h>
#include <formulas.h>
#include <stdproperties.h>
#include <macros.h>
#include <materialy.h>
#include <object_types.h>

void
create_armour()
{
    ustaw_nazwe("fartuch");

    dodaj_przym("bia�y", "biali");
    dodaj_przym("przybrudzony", "przybrudzeni");

    set_long("Fartuch jest wprost idealny dla karczmarza lub jakiej� " +
	"kelnerki, a na pewno dla osoby maj�cej zwi�zek z potrawami i " +
	"trunkami. Dzi�ki niemu w szybki spos�b mo�na otrze� brudne " +
	"d�onie lub przetrze� przybrudzone naczynie. Po tym fartuchu " +
	"bez problemu mo�na stwierdzi�, ze by� cz�sto u�ywany, bowiem " +
	"liczne przybrudzenia pokrywaj� ca�� jego powierzchni�.\n");

    set_slots(A_STOMACH,A_TORSO);
//    set_ac();
    ustaw_material(MATERIALY_SK_BYDLE, 80);
    ustaw_material(MATERIALY_LEN, 20);
    set_type(O_UBRANIA);
    add_prop(ARMOUR_I_D_ROZCIAGLIWOSC, 100); // zeby sie nie srac...
    add_prop(ARMOUR_I_U_ROZCIAGLIWOSC, 100);
    add_prop(OBJ_I_VOLUME,6300);
    add_prop(OBJ_I_VALUE,5);
    add_prop(OBJ_I_WEIGHT,5500);

    set_likely_cond(23);
}