
/* Brazowa haftowana szata,
   Wykonana przez Alcyone dnia 13.03.06 */


inherit "/std/armour";
#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <pl.h>
#include <macros.h>
#include <materialy.h>
#include <object_types.h>

void
create_armour()
{               
    ustaw_nazwe("szata");
    dodaj_przym("br^azowy","br^azowi"); 
    dodaj_przym("haftowany","haftowani");

    set_long("Do^s^c mocno pofa^ldowana szata, zosta^la uszyta z "+
        "mi^ekkiego i przyjemnego w dotyku bawe^lnianego materia^lu. "+
        "Gruba tkanina ozdobiona jest ci^agn^acym si^e od do^lu barwnym "+
        "haftem, przedstawiaj^acym ta^ncz^ace p^lomienie ognia. Wyszyto je "+
        "w taki spos^ob, ^ze wygl^adaj^a jakby nieustannie migota^ly. Takie "+
        "wzory s^a charakterystyczne dla kap^lan^ow swi^atyni Kreve i tylko "+
        "oni winni nosi^c taki str^oj. Szata wygl^ada na pow^l^oczyst^a, o "+
        "czym swiadcz^a spore zabrudzenia widniej^ace na jej dole.\n");
             
    set_slots(A_ROBE);
    add_prop(OBJ_I_WEIGHT, 4000);
    add_prop(OBJ_I_VOLUME, 3000);
    add_prop(OBJ_I_VALUE, 250);
    set_type(O_UBRANIA);
    ustaw_material(MATERIALY_BAWELNA, 100);
    add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    add_prop(ARMOUR_I_U_ROZCIAGLIWOSC, 40);
    set_size("M");
    set_likely_cond(19);
}