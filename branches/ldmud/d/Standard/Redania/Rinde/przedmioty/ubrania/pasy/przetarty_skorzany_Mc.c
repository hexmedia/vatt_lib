/*
 * pasek dla stra�nika swiatynnego - brudasa
 *
 * by Faeve - 6.01.07
 *
 */

inherit "/std/belt";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <pl.h>
#include <macros.h>
#include <materialy.h>
#include <object_types.h>

void
create_belt()
{
    ustaw_nazwe("pas");
    dodaj_przym("przetarty","przetarci");
    dodaj_przym("sk�rzany", "sk�rzani");

    set_long("Sk^ora, z kt^orej ten pasek wykonano, w wielu miejscach "+
        "jest ju^z przetarta i mocno pop^ekana, a metalow^a klamr^e zacz^e^la "+
        "zjada^c rdza. Mimo to pas, dotkni^ety ju^z nieugi^etym dzia^laniem "+
        "czasu oraz brakiem zainteresowania ze strony w^la^sciciela, jest "+
        "w stanie jako-tako utrzyma^c portki we w^la^sciwym miejscu. Kwestia "+
        "tego tylko, ^ze osoba go nosz^aca na pewno nie b^edzie prezentowa^c "+
        "si^e dobrze.\n");

    add_prop(OBJ_I_VOLUME, 300);
    add_prop(OBJ_I_WEIGHT, 137);
    add_prop(OBJ_I_VALUE, 6);
    add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    add_prop(ARMOUR_I_U_ROZCIAGLIWOSC, 60);
    add_prop(ARMOUR_I_D_ROZCIAGLIWOSC, 60);
    set_size("M");

    set_max_slots_zat(2);
    set_max_slots_prz(1);
    set_max_weight_zat(3800);
    set_max_weight_prz(900);
    set_max_volume_zat(2000);
    set_max_volume_prz(2000);

    ustaw_material(MATERIALY_SK_SWINIA, 95);
    ustaw_material(MATERIALY_STAL, 5);
    set_likely_cond(22);
}