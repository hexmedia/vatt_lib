
/* Autor: Avard
   Opis : Tinardan
   Data : 19.05.06 */

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <pl.h>
#include <macros.h>
#include <materialy.h>
#include <object_types.h>
void
create_armour()
{  
    ustaw_nazwe_glowna("para");
    ustaw_nazwe("buty");

    dodaj_przym("wygodny","wygodni");
    dodaj_przym("czarny","czarni");
    set_long("Uszyte z twardej, l^sni^acej sk^ory. S^a do^s^c wysokie, "+
        "cz^lowiekowi si^ega^lyby za kolana. Cholewy u g^ory s^a wywini^ete, "+
        "a po zewn^etrznych bokach widnieje szereg ^zelaznych klamr, kt^ore "+
        "spinaj^a but. Noski s^a okr^ag^le i nieco zdarte. W grubej, "+
        "specjalnie wzmacnianej podeszwie widoczne s^a niewielkie g^l^owki "+
        "gwo^xdzi, kt^orymi ta zosta^la przytwierdzona. Zar^owno materia^l "+
        "jak i szwy s^a niezwykle mocne i profesjonalnie wykonane. \n");
		
    set_slots(A_FEET);
    add_prop(OBJ_I_VOLUME, 1300);
    add_prop(OBJ_I_WEIGHT, 500);
    add_prop(OBJ_I_VALUE, 65);
    add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    add_prop(ARMOUR_I_D_ROZCIAGLIWOSC, 30);
    add_prop(ARMOUR_I_U_ROZCIAGLIWOSC, 40);
    set_size("M");
    ustaw_material(MATERIALY_SK_CIELE, 90);
    ustaw_material(MATERIALY_ZELAZO, 10);
    set_type(O_UBRANIA);
    set_likely_cond(18);
}