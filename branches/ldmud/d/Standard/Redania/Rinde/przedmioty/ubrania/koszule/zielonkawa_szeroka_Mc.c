
/* Autor: Avard
   Opis : Adves
   Data : 07.06.06 */

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <macros.h>
#include <materialy.h>
#include <object_types.h>
#include <pl.h>

void
create_armour()
{
    ustaw_nazwe("koszula");
    dodaj_przym("zielonkawy","zielonkawi");
    dodaj_przym("szeroki","szerocy");
    set_long("Zielonkawa koszula, na kt^or^a spogl^adasz, nie posiada "+
        "niemal ^zadnych zdobie^n, jedyn^a ozdoba jest pi^e^c ogromnych "+
        "guzik^ow, solidnie przyszytych grub^a nici^a do zielonego "+
        "materia^lu. Ka^zdy z nich dodatkowo jest innego kszta^ltu, od "+
        "prostych, geometrycznych, a^z po skomplikowane motywy ro^slinne. "+
        "Zabawny ko^lnierzyk opada na ramiona nosz^acej go osoby, a spora "+
        "ilo^s^c materia^lu, kt^orej u^zy^l krawiec do uszycia tego "+
        "ubrania, pozwala na to, by jego w^la^sciciel czu^l si^e naprawde "+
        "swobodnie. Szeroko^s^c, mi^ekko^s^c materia^lu i oryginalny "+
        "wygl^ad sprawiaj^a, ^ze nie zwracasz nawet wi^ekszej uwagi na "+
        "niestaranne szycia ci^agn^ace si^e wzd^lu^z r^ekaw^ow. \n");

    set_slots(A_TORSO, A_FOREARMS, A_ARMS);
    add_prop(OBJ_I_WEIGHT, 300);
    add_prop(OBJ_I_VOLUME, 200);
    add_prop(OBJ_I_VALUE, 25);
    set_type(O_UBRANIA);
    ustaw_material(MATERIALY_LEN, 100);
    add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    set_size("M");
}