/* Artonul

    'wyglancuj'
 */
inherit "/std/armour";

#include <wa_types.h>
#include <formulas.h>
#include <stdproperties.h>
#include <macros.h>

void
create_armour()
{
    ustaw_nazwe("buty");

    dodaj_przym("b�yszcz�cy", "b�yszcz�cy");
    dodaj_przym("sk�rzany", "sk�rzani");

    set_long("Buty te s� na pewno dzie�em bardzo dobrego szewca. Wykonano " +
	"je z wysokiej jako�ci, czernionej sk�ry i przybito gwo�dzikami " +
	"do mocnych metalowo-drewnianych cholew, we wn�trzu kt�rych " +
	"zauwa�asz jakie� szmateczki, kt�re prawdopodobnie mog� pos�u�y� " +
	"do 'wyglancowania' tych but�w.\n");

    set_slots(A_FEET);
//    set_ac();
set_likely_cond(19);
}
