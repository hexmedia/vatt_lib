
/* Autor: Avard 
   Data : 08.03.2006 */

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <pl.h>
#include <object_types.h>
#include <macros.h>
#include <materialy.h>

void
create_armour()
{ 

    ustaw_nazwe("plaszcz");
    dodaj_przym("czarny","czarni");
    dodaj_przym("elegancki", "eleganccy");
    set_long("Obszerny p^laszcz, zrobiono z ci^e^zkiej czarnej sk^ory. "+
        "Posiada du^zy ko^lnierz, kt^ory lu^xno opiera si^e na ramionach. "+
        "Po wewn^etrznej stronie p^laszcza, mo^zna zauwa^zy� wiele "+
        "ma^lych kieszonek, w kt^orych zapewne da si^e schowa� "+
        "jakie^s drobne przedmioty. Od samej g^ory po lewej stronie "+
        "przymocowane sa dwie du^ze, srebrne klamry, kt^ore z pewno^sci^a "+
        "mo^zna zapi^a�. P^laszcz jest d^lugi i powinien si^ega� do kostek. "+
        "Jest wygodny i nie kr^epuje ruch^ow, a silnym osobom, "+
        "jego ci^e^zar nie powinien sprawia� ^zadnych trudno^sci.\n");



    ustaw_material(MATERIALY_SREBRO, 10);
    ustaw_material(MATERIALY_SK_WILK, 90);
    set_type(O_UBRANIA);
    set_slots(A_CLOAK);
    add_prop(OBJ_I_WEIGHT, 2200);
    add_prop(OBJ_I_VOLUME, 1000);
    add_prop(OBJ_I_VALUE, 250);
    set_size("M");
    set_likely_cond(19);
}
 