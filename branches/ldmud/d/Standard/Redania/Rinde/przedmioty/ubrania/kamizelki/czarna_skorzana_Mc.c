
/* Autor: Avard
   Opis : Tinardan
   Data : 19.05.06 */

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <pl.h>
#include <macros.h>
#include <materialy.h>
#include <object_types.h>
void
create_armour()
{
    ustaw_nazwe("kamizelka");
    dodaj_przym("czarny","czarni");
    dodaj_przym("sk^orzany","sk^orzani");

    set_long("Mocne szwy, kt^ore spajaj^a cz^e^sci kamizelki, eleganckie "+
        "wyko^nczenia i najwy^zszego gatunku sk^ora ^swiadcz^a o tym, ^ze "+
        "wykona^l to ubranie mistrz krawiecki, kt^ory prawdziwie zna^l si^e "+
        "na rzeczy. Z przodu przymocowa^l grube rzemienie, kt^ore zwi^azuj^a "+
        "po^ly kamizelki s^a elegancko wyko^nczone niewielkimi fr^edzlami. Od "+
        "spodu przyszyty jest gruby aksamit, kt^ory chroni nosz^acego przed "+
        "twardo^sci^a garbowanej sk^ory.\n");

    set_slots(A_TORSO, A_SHOULDERS);
    add_prop(OBJ_I_VOLUME, 1550);
    add_prop(OBJ_I_WEIGHT, 400);
    add_prop(OBJ_I_VALUE, 140);
    add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    add_prop(ARMOUR_I_U_ROZCIAGLIWOSC, 40);
    set_size("M");
    ustaw_material(MATERIALY_AKSAMIT, 40);
    ustaw_material(MATERIALY_SK_SWINIA, 60);
    set_type(O_UBRANIA);
    set_likely_cond(17);

}
