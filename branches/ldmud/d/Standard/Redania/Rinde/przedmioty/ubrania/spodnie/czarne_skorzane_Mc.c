
/* Autor: Avard
   Opis : Ghallerian
   Data : 20.06.2005  */

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <pl.h>
#include <macros.h>
#include <materialy.h>
#include <object_types.h>

void
create_armour()
{
    ustaw_nazwe("spodnie");
    ustaw_nazwe_glowna("para");

    dodaj_przym("czarny","czarni");
    dodaj_przym("sk^orzany","sk^orzani");

    set_long("Sk^ora, z kt^orej zosta^ly wykonane spodnie jest przedniego "+
        "rodzaju. Mi^ekko^s� w dotyku, a zarazem wytrzyma^lo^s� sprawiaj^a, "+
        "^ze jest ^swietnym materia^lem na ubranie. Spodnie posiadaj^a "+
        "zdobne szwy po obu bokach. Zapewne wplecione zosta^ly w nie srebrne "+
        "nitki. Czer^n i srebro wy^smienicie komponuj^a si^e i sprawiaj^a, "+
        "^ze spodnie wygl^adaj^a wyj^atkowo elegancko. Do spodni do^l^aczony "+
        "jest pasek dzi^eki, kt^oremu mo^zna bezpiecznie je nosi�. Po lewej "+
        "stronie posiadaj^a jedn^a kiesze^n, ale za to bardzo pojemn^a.\n");

    set_slots(A_LEGS, A_HIPS);
    add_prop(OBJ_I_VOLUME, 1000);
    add_prop(OBJ_I_WEIGHT, 837);
    add_prop(OBJ_I_VALUE, 75);
    ustaw_material(MATERIALY_SK_SWINIA, 100); /* Do zmiany, bo za cholere nie
	wiem jaka tu moze byc skora :P */
    set_type(O_UBRANIA);
    add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    set_size("M");
    set_likely_cond(19);
}