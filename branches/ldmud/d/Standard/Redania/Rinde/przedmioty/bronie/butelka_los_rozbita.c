/**
 * Rozbita butelka
 *  jako bro�, jako �mie�..
 *               by Lil  01.05
 *
 * TODO:
 *  - przerobi� na random_przym
 */

inherit "/std/weapon.c";

#include <pl.h>
#include <macros.h>
#include <wa_types.h>
#include <stdproperties.h>

object wielder;

string *losowy_przym()
{
  string *lp;
   string *lm;
   int i;
   lp=({ "zielony", "br�zowy", "brudny"});
   lm=({"zieloni", "br�zowi", "brudni"});
   i=random(3);
   return ({ lp[i] , lm[i] });
 }

    void
    create_holdable_object()
{
   string *przm = losowy_przym();
    ustaw_nazwe(({ "butelka","butelki","butelce","butelk�","butelk�","butelce"}),
                                ({"butelki","butelek","butelkom","butelki",
                                   "butelkami","butelkach"}),PL_ZENSKI);
   dodaj_przym( przm[0], przm[1] );
   dodaj_przym("rozbity", "rozbici");
   set_long("Ta rozbita butelka nie nada si� ju� do niczego dobrego, "+
            "jednak spogl�daj�c na� u�wiadamiasz sobie fakt, i� w r�ku "+
	    "miejscowych pijaczk�w, chwytana za ocala�� jeszcze szyjk�, "+
	    "sta� si� mo�e niebezpieczn� broni�.\n");
   set_hit(11);
   set_pen(5);
   set_wt(W_CLUB);
   set_dt(W_SLASH);
   set_hands(W_ANYH);
   add_prop(OBJ_I_WEIGHT, 198);
   add_prop(OBJ_I_VALUE, 101);
   set_dull(3);
   set_likely_dull(MAXINT);
   set_likely_break(MAXINT);
   //set_weapon_hits(1550);

   remove_adj("zniszczony");
   odmien_short();
   odmien_plural_short();
}

public int
test_jestem_rozbita_butelka1()
{
        return 1;
}

