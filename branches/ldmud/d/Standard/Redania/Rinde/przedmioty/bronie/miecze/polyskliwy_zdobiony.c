
/* Autor: Avard
   Opis : Artonul
   Data : 22.07.05
   Info : Miecz dla straznikow Rinde */

inherit "/std/weapon";

#include "/sys/formulas.h"
#include <stdproperties.h>
#include <wa_types.h>
#include <pl.h>
#include <materialy.h>
#include <object_types.h>


void
create_weapon()
{
   ustaw_nazwe("miecz");

     dodaj_przym( "po�yskliwy", "po�yskliwi" );
     dodaj_przym( "zdobiony", "zdobieni" );

     set_long("Ten kr^otki miecz jest standardow^a cz^e^sci^a wyposa^zenia "+
         "bardziej licz^acych si^e stra^znik�w miasta Rinde. Ocynkowana i "+
         "pokryta miedzi^a r^ekoje^s^c zapewnia pewny chwyt broni, a dobrze "+
         "wywa^zone, zw^e^zaj^ace si^e, l^sni^ace i dok^ladnie wypolerowane "+
         "ostrze nadaje tej broni nies^lychane wr^ecz jak na tak kr^otki "+
         "miecz warto^sci bojowe.\n");

     set_hit(26);
     set_pen(25);

     add_prop(OBJ_I_WEIGHT, 1500);
     add_prop(OBJ_I_VOLUME, 1500);
     add_prop(OBJ_I_VALUE, 3140);

     set_wt(W_SWORD);
     set_dt(W_IMPALE | W_SLASH);

     set_hands(A_ANY_HAND);
     ustaw_material(MATERIALY_STAL, 90);
     ustaw_material(MATERIALY_MIEDZ, 10);
     set_type(O_BRON_MIECZE);
}