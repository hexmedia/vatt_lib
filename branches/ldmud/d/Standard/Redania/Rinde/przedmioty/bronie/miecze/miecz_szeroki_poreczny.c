/*miecz m.in. dla krasnolud�w fryzjera
* by Sehtril 2005, drobne poprawki Lil.
* Update do dzisiejszych standardow - Avard 03.06.06
*
* Sprzedawany tak�e w sklepie.
*/
inherit "/std/weapon";

#include <wa_types.h>
#include <macros.h>
#include <stdproperties.h>
#include <materialy.h>
#include <object_types.h>

void
create_weapon()
{
   ustaw_nazwe("miecz");

    dodaj_przym("szeroki","szerocy");
    dodaj_przym("por�czny","por�czni");
    set_long("Ten miecz nadaje si� doskonale do parowania nawet mocnych "+
    "cios�w. R�koje�� owini�ta zosta�a sk�r� z drobnymi kolcami, dzi�ki "+
    "czemu chwyt jest pewny. W r�kach wprawnego wojownika mo�e stanowi� "+
    "do�� gro�n� bro�.\n");
    set_hit(24);
    set_pen(26);
    set_wt(W_SWORD);
    set_dt(W_IMPALE | W_SLASH);
    set_hands(W_ANYH);
    add_prop(OBJ_I_WEIGHT, 5000);
    add_prop(OBJ_I_VOLUME, 4000);
    add_prop(OBJ_I_VALUE, 2000);
    ustaw_material(MATERIALY_STAL, 90);
    ustaw_material(MATERIALY_SK_SWINIA, 10);
}
