
/* Autor: Avard
   Opis : Rantaur
   Data : 16.02.07 */

inherit "/std/pochwa";

#include "/sys/formulas.h"
#include <stdproperties.h>
#include <object_types.h>
#include <materialy.h>
#include <macros.h>
#include <cmdparse.h>
#include <pl.h>

#define POCHWA_PD_PLECY         1
#define POCHWA_PD_UDA           2
#define POCHWA_PD_GOLENIE       4
#define POCHWA_PD_PAS           8

#define POCHWA_NA_PLECACH	"_pochwa_na_plecach"
#define POCHWA_PRZY_UDZIE_P	"_pochwa_przy_udzie_p"
#define POCHWA_PRZY_UDZIE_L	"_pochwa_przy_udzie_l"
#define POCHWA_PRZY_GOLENI_P	"_pochwa_przy_goleni_p"
#define POCHWA_PRZY_GOLENI_L	"_pochwa_przy_goleni_l"
#define POCHWA_W_PASIE_P	"_pochwa_w_pasie_p"
#define POCHWA_W_PASIE_L	"_pochwa_w_pasie_l"
#define POCHWA_SUBLOC		"_pochwa_subloc"

void
create_pochwa()
{
    ustaw_nazwe("pochwa");
    dodaj_przym("kr�tki","kr�tcy");
    dodaj_przym("prosty","pro�ci");

    set_long("Lekko wygi�ta pochwa wykonana jest z twardej, ciemnobr�zowej "+
    "sk�ry. Nie dostrzegasz na niej �adnych zdobie� opr�cz niewielkiego, "+
    "srebrzystego symbolu wyobra�aj�cego or�a, cho� najpewniej i on jest na "+
    "niej umieszczony z czysto formalnych wzgled�w. Kraw�dzie pochwy s� "+
    "wzmocnione metalowymi okuciami tak, aby mog�a ona jak najd�u�ej s�u�y� "+
    "swemu w�a�cicielowi.\n");

    set_przypinane_do(POCHWA_PD_PLECY | POCHWA_PD_PAS );
    add_subloc_prop(0, SUBLOC_I_DLA_O, O_BRON_MIECZE);
    set_type(O_POCHWY);

    add_prop(OBJ_I_WEIGHT, 2000);
    add_prop(OBJ_I_VOLUME, 2500);
    add_prop(CONT_I_REDUCE_VOLUME,125);
    add_prop(CONT_I_MAX_WEIGHT,10000);
    add_prop(CONT_I_MAX_VOLUME,10000);
    add_prop(CONT_I_MAX_RZECZY,1);
}