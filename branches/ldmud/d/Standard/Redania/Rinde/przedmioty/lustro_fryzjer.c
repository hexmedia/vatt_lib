inherit "/std/object.c";

#include "dir.h"
#include <pl.h>
#include <stdproperties.h>
#include <macros.h>
#include <object_types.h>
#include <filter_funs.h>

create_object()
{
    ustaw_nazwe("lustro");

    dodaj_przym("ogromny","ogromni");
    dodaj_przym("wspania�y","wspaniali");

    set_long("Takiego cacuszka mog� pozazdro�ci� nawet "+
             "kr�lowie. Wysokie na dziesi�c st�p i szerokie na "+
	     "s��e�, musia�o kosztowa� fortun�. Powierzchnia "+
	     "jest g�adka niczym tafla wody w bezwietrzny "+
	     "dzie� i odbicie w lustrze jest niemal doskona�e. "+
	     "Oczko w g�owie fryzjera, arcydzie�o wykonane "+
	     "zdolnymi r�koma krasnoludzkich mistrz�w.\n");
    add_prop(OBJ_I_WEIGHT, 71250);
    add_prop(OBJ_I_VOLUME, 22500);
    add_prop(OBJ_I_VALUE, 9960);
    set_type(O_SCIENNE);
    set_owners(({RINDE_NPC + "fryzjer"}));
}

public int
jestem_lustrem_fryzjera()
{
    return 1;
}

void
init()
{
    add_action("prz_lus", "przejrzyj");
    ::init();
}

int
prz_lus(string tekst)
{
    object lustro;
    int i;
    
    notify_fail("Gdzie chcesz si� przejrze�?\n");

    if (!tekst || !parse_command(tekst, environment(this_player()), "'si�' 'w' %o:" + PL_MIE, lustro))
        return 0;

    if (lustro != this_object())
        return 0;
	
    if (sizeof(this_player()->query_armour(TS_HEAD)))
    {
        write("Wygl^adasz nawet ladnie. " +
	    this_player()->koncowka("M^og^lby^s", "Mog^laby^s", "Mog^loby^s") +
	    " obejrze^c swoj^a wspania^l^a fryzur^e, gdyby nie " +
	    this_player()->query_armour(TS_HEAD)[0]->short(PL_MIA) +
	    " na g^lowie.\n");
	return 1;
    }
    else
    {
	write("Wygladasz nawet ^ladnie. " +
	    this_player()->opis_wlosow(this_player()));
        return 1;
    }

//    write("Wygladasz calkiem normalnie.\n");
//    return 1;
}





