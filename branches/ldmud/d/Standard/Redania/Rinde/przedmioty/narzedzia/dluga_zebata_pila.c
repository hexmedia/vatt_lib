inherit "/std/holdable_object";

#include <materialy.h>
#include <object_types.h>
#include <stdproperties.h>

void create_holdable_object()
{
	ustaw_nazwe("pi�a");
	
	dodaj_przym("d�ugi", "d�udzy");
	dodaj_przym("z�baty", "z�baci");
	
	set_long("D�ugie na dwa �okcie ostrze pi�y jest umocowane w drewnianej"
			+" r�czce. Sama r�czka jest bardzo starannie oheblowana i"
			+" dok�adnie pokryta l�ni�cym lakierem, kt�ry zapewne ma"
			+" przed�u�y� �ywot narz�dzia.\n");
	
	set_type(O_NARZEDZIA);
	ustaw_material(MATERIALY_STAL, 90);
	ustaw_material(MATERIALY_DR_SOSNA, 10);
	
	add_prop(OBJ_I_WEIGHT, 1503);
	add_prop(OBJ_I_VOLUME, 630);
	add_prop(OBJ_I_VALUE, 121);
	
	//set_hands(W_ANYH);
}

int query_pila()
{
	return 1;
}
	
