/*krzeslo poczatkowo do kuzni Rinde,
   Lil&Tinardan 18.08.2005 */
inherit "/std/object.c";

#include "dir.h"
#include <pl.h>
#include <stdproperties.h>
#include <macros.h>
#include <filter_funs.h>
#include <object_types.h>
#include <materialy.h>

create_object()
{
    ustaw_nazwe("krzes�o");
    dodaj_przym("drewniany","drewniani");
    dodaj_przym("prosty","pro�ci");
   make_me_sitable("na","na drewnianym prostym krze�le","z krzes�a",1);
    set_long("Proste, drewniane krzes�o, sklecone solidnie, "+
             "ale bez dba�o�ci o estetyk�. �wietnie spe�nia "+
	     "swoj� funkcj�, ale niewiele poza tym. Jest "+
	     "pokryte warstw� kurzu i cho� prawdopodobnie "+
	     "niegdy� by�o jasne, teraz przypomina kolorem "+
	     "szar�, ja�ow� ziemi�.\n");
    add_prop(OBJ_I_WEIGHT, 6250);
    add_prop(OBJ_I_VOLUME, 12500);
    add_prop(OBJ_I_VALUE, 2);
    set_type(O_MEBLE);
    ustaw_material(MATERIALY_DR_DAB);
    set_owners(({RINDE_NPC+"pracownik_poczty"}));
}
