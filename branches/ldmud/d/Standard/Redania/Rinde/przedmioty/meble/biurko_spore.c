/* Biurko w banku
 * By Adept Bara
 * 04 01 06
 */

inherit "/std/container";

#include "dir.h"
#include <pl.h>
#include <macros.h>
#include <cmdparse.h>
#include <object_types.h>
#include <filter_funs.h>
#include <stdproperties.h>
#include <composite.h>

nomask void
create_container()
{
    ustaw_nazwe(({"biurko","biurka","biurku","biurko","biurkiem",
    "biurku"}),({"biurka","biurek","biurka","biurka","biurkami",
    "biurkach"}),PL_NIJAKI_NOS);

    dodaj_przym("spory","sporzy");

    set_long("Spore drewniane biurko. Po "+
             "resztach ciemnej farby, ktor^a zosta^lo pomalowane wida^c, i^z prze^zy^lo ju^z ono "+
             "swoje najlepsze lata. Blat zawalony jest spor^a ilo^sci^a papier^ow i ro^znych "+
             "dokument^ow, starannie na nim pouk^ladanych."+
             "@@opis_sublokacji| Na blacie |na|.|||lezy |leza |lezy @@\n");

    setuid();
    seteuid(getuid());
    add_prop(CONT_I_MAX_VOLUME, 2000); //do poprawki
    add_prop(CONT_I_VOLUME, 1680);  // do poprawki

    add_prop(CONT_I_CANT_WLOZ_DO, 1); 

    add_prop(CONT_I_WEIGHT, 60000);
    add_prop(CONT_I_MAX_WEIGHT, 120000); // do poprawki

    add_prop(CONT_I_DONT_SHOW_CONTENTS, 1);

    add_subloc("na");
    set_owners(({RINDE_NPC+"ruygen"}));
}

public int
query_type()
{
        return O_MEBLE;
}
