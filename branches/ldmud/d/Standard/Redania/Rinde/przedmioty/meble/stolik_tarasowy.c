/* Stolik z tarasu gospody Baszta, Rinde.
   Lil  17.06.2005  */

#pragma strict_types

inherit "/std/container";

#include <pl.h>
#include <macros.h>
#include <filter_funs.h>
#include <stdproperties.h>
#include <composite.h>                                                         
#include <cmdparse.h>
#include "dir.h"

string czy_cos_jest_na_stoliku();

static int newLine;

nomask void
create_container()

{
   ustaw_nazwe(({"stolik","stolika","stolikowi","stolik","stolikiem",
    "stoliku"}),({"stoliki","stolik�w","stolikom","stoliki","stolikami",
    "stolikach"}),PL_MESKI_NOS_NZYW);
	           			       
   set_long("JESZCZE NIE OPISANY PRZEDMIOT! NATYCHMIAST GO OPISZ!\n" +  
            "@@czy_cos_jest_na_stoliku@@");
   dodaj_przym("ma^ly","mali");

   set_owners(({RINDE+"npc/karczmarz_baszty"})); 
   setuid();
   seteuid(getuid());

   add_prop(CONT_I_MAX_VOLUME, 24000);
   add_prop(CONT_I_MAX_WEIGHT, 24000);
   add_prop(CONT_I_VOLUME, 9901);
   add_prop(CONT_I_WEIGHT, 9009);
//   add_prop(OBJ_I_NO_GET, "Jest zbyt ci�ki.\n");
   add_prop(OBJ_I_DONT_GLANCE,1);
   add_prop(CONT_I_DONT_SHOW_CONTENTS, 1);
   add_prop(CONT_I_CANT_WLOZ_DO, 1);
   add_subloc("na");
   add_subloc_prop("na", CONT_I_MAX_WEIGHT, 10500);
   
}

string
czy_cos_jest_na_stoliku()
{
	newLine = 0;
   if (sizeof(subinventory("na")) > 0) {
       newLine = 1;
       return "Na jego blacie zauwa�asz: "
              + this_object()->subloc_items("na") + ".\n";
   }
      if (newLine)
       {
	 return "\n";
       }
   return "";
}
