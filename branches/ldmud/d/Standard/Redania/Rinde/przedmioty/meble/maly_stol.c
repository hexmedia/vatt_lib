#pragma strict_types

inherit "/std/container";

#include "dir.h"
#include <pl.h>
#include <macros.h>
#include <filter_funs.h>
#include <stdproperties.h>
#include <composite.h>
#include <cmdparse.h>
#include "/sys/materialy.h"
#include "/sys/object_types.h"

nomask void
create_container()
{
  ustaw_nazwe("st�");

  dodaj_przym("ma�y","mali");

  set_long("Ma�y, drewniany st�. Kwadratowy blat, mimo wyra�nych �lad�w cz�stego u�ywania, " +
      "wygl�da na solidnie wykonany.@@opis_sublokacji| Widzisz na nim |na|.|||" + PL_BIE + "@@ " +
      "Ca�o�� trzyma si� na czterech grubych, prostych nogach.@@opis_sublokacji| " +
      "Pomi�dzy nimi widzisz |pod|.|||" + PL_BIE + "@@\n");

  add_prop(CONT_I_MAX_VOLUME, 30000);
  add_prop(CONT_I_VOLUME, 16800);
  add_prop(CONT_I_CANT_WLOZ_DO, 1);
  add_prop(CONT_I_WEIGHT, 10000);
  add_prop(CONT_I_MAX_WEIGHT, 30000);
  add_prop(CONT_I_DONT_SHOW_CONTENTS, 1);

  add_subloc("na");
  add_subloc_prop("na", CONT_I_MAX_VOLUME, 4700);
  add_subloc_prop("na", CONT_I_MAX_WEIGHT, 8000);

  add_subloc("pod");
  add_subloc_prop("pod", CONT_I_MAX_VOLUME, 8500);
  add_subloc_prop("pod", CONT_I_MAX_WEIGHT, 12000);

  ustaw_material(MATERIALY_DR_DAB);
  set_type(O_MEBLE);
  set_owners(({RINDE + "straz/straznik",RINDE+"straz/oficer"}));
}
