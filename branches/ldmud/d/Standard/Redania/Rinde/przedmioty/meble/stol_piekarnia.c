/* stol do piekarni Rinde,
      Lil*/


#pragma strict_types

inherit "/std/container";

#include "dir.h"
#include <pl.h>
#include <macros.h>
#include <filter_funs.h>
#include <stdproperties.h>
#include <composite.h>
#include <cmdparse.h>
#include <object_types.h>
#include <materialy.h>

nomask void
create_container()
{
    ustaw_nazwe("st�");
    dodaj_przym("solidny","solidni");

    set_long("St� l�ni czysto�ci�, a jego s�katy blat wygl�da do�� solidnie. "+
             "@@opis_sublokacji| Na blacie |na|.|||le�y |le�� |le�y @@\n");

    add_item(({ "blat", "powierzchni� sto�u" }), "S�kata powierzchnia nie jest zbyt r�wna, jednak "+
                                  "na pewno utrzyma niejeden ci�ar.\n");
    ustaw_material(MATERIALY_DR_DAB);
    setuid();
    seteuid(getuid());
    add_prop(CONT_I_MAX_VOLUME, 20000);
    add_prop(CONT_I_VOLUME, 16800);

    add_prop(CONT_I_CANT_WLOZ_DO, 1); /* By nie mo�na by�o nic do niego w�o�y� komend� 'wloz do' */

    add_prop(CONT_I_WEIGHT, 10000);
    add_prop(CONT_I_MAX_WEIGHT, 20000);

    add_prop(CONT_I_DONT_SHOW_CONTENTS, 1);

    /* dodajemy sublokacj� 'na', by mo�na by�o co� na st� k�a�� */
    add_subloc("na");
    set_owners(({RINDE_NPC + "piekarz"}));

}

public int
query_type()
{
	return O_MEBLE;
}

public int
test_rwops1()
{
	return 1;
}
