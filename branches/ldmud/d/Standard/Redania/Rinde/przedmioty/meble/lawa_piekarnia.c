/* lawa w piekarni. Lil. */

#pragma strict_types
inherit "/std/object";
#include <pl.h>
#include <macros.h>
#include <filter_funs.h>                                                       
#include <stdproperties.h>
#include <composite.h>
#include <cmdparse.h>
#include <object_types.h>
#include "dir.h"
#include <materialy.h>

nomask void
create_object()
{
    ustaw_nazwe("�awa");

    dodaj_przym("d�bowy","d�bowi");
   ustaw_material(MATERIALY_DR_DAB);
    set_long("@@longg@@");
    make_me_sitable("na","na drewnianej �awie","z �awy",3);
    set_owners(({RINDE_NPC+"piekarz"}));
}

public int
query_type()
{                                                                              
        return O_MEBLE;
}

string
longg()
{
	string str;
	
	//if(this_object(environment(RINDE+"wschod/piekarnia.c"))
	if (file_name(environment(this_object())) == WSCHOD_LOKACJE+"piekarnia.c")
	str = "Solidna, d�bowa �awa stoi tu� obok najwi�kszego sto�u. Rze�bienia, kt�rymi " +
			"jest pokryta, przedstawiaj� wypieki serwowane przez piekarza.\n";
	else
	{
	str = "Jest to solidna, d�bowa �awa. Rze�bienia, kt�rymi " +
			"jest pokryta, przedstawiaj� wypieki serwowane przez piekarza.\n";
	}
	return str;
}

public int
test_rwopl1()
{
        return 1;
}

