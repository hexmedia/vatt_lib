#pragma strict_types

inherit "/std/container";

#include "dir.h"
#include <pl.h>
#include <macros.h>
#include <filter_funs.h>
#include <stdproperties.h>
#include <composite.h>
#include <cmdparse.h>
#include "/sys/materialy.h"
#include "/sys/object_types.h"

nomask void
create_container()
{
  ustaw_nazwe("st�");

  dodaj_przym("du�y","duzi");
  dodaj_przym("stalowy", "stalowi");

  set_long("Du^zy, solidnie wygl^adaj^acy st^o^l przeznaczony jest do operowania na^n rannych. " +
      "Wy^z^lobione rowki wzd^lu^z szerokiego blatu z nierdzewnej stali " +
      "s^lu^z^a odprowadzaniu krwi na pod^log^e.@@opis_sublokacji| Widzisz na nim |na|.|||" +
      PL_BIE + "@@\n");

  add_prop(CONT_I_MAX_VOLUME, 300000);
  add_prop(CONT_I_VOLUME, 228000);
  add_prop(CONT_I_CANT_WLOZ_DO, 1);
  add_prop(CONT_I_WEIGHT, 100000);
  add_prop(CONT_I_MAX_WEIGHT, 400000);
  add_prop(CONT_I_DONT_SHOW_CONTENTS, 1);

  add_subloc("na");
  add_subloc_prop("na", CONT_I_MAX_VOLUME, 72000);
  add_subloc_prop("na", CONT_I_MAX_WEIGHT, 100000);

  ustaw_material(MATERIALY_STAL);
  set_type(O_MEBLE);
}
