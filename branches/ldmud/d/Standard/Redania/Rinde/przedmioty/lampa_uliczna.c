//Oto lampa uliczna do Rinde. Lil :P

inherit "/std/lampa_uliczna";
#include "dir.h"
#include <stdproperties.h>

void
create_street_lamp()
{
    set_long("@@dluuuuuuuugi@@");
    set_light_message("Str� zdejmuje ze s�upa lamp�, "+
                      "nape�nia j� olejem, po czym "+
                      "odwiesza z powrotem. Lampa swym "+
		      "delikatnym �wiat�em rozja�nia "+
                      "nieco okolic� i rozwiewa cz�ciowo "+
		      "mroki ulicy.\n");
    set_extinguish_message("Str� zdejmuje ze s�upa lamp�, "+
                           "gasi j�, po czym odwiesza z "+
                           "powrotem.\n");
    set_owners(({RINDE_NPC + "sekretarz"}));//FIXME dodac burmistrza
}

string dluuuuuuuugi()
{
    string str;
    str = "�elazo zosta�o zmy�lnie powyginane i uformowane "+
        "we wzory. Wok� g��wnego s�upa wije si� kilka "+
	"mniejszych pr�t�w, z kt�rych wyrastaj� "+
	"male�kie �elazne li�cie i kwiaty. Mimo pozornej "+
	"krucho�ci ozd�b, lampa i rze�bienia s� naprawd� "+
	"mocne i solidnie wykonane. Na jej szczycie, "+
        "na haczyku wisi lampa ";
	 
	 
    if(this_object()->query_prop(OBJ_I_LIGHT))
      str += "p�on�ca teraz spokojnym "+
      // (deszcz/�nieg: rozedrganym  wiatr : rozko�ysanym przez wiatr )
           "p�omieniem o ciep�ym widmie barwnym.";
      
    else
      str += "zapalana na noc przez str�a.";

    str += "\n";
    return str;
}
