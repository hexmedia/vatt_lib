/* Autor: 
   Data : 
   Opis : */

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"
inherit LAS_WIEWIOREK_STD;

void create_las() 
{
    set_short("Short lokacji - Las 1");
    add_exit(LAS_WIEWIOREK_LOKACJE + "trakt1","nw",0,LAS_RO_FATIG,1);
    add_exit(LAS_WIEWIOREK_LOKACJE + "trakt2","n",0,LAS_RO_FATIG,1);
    add_exit(LAS_WIEWIOREK_LOKACJE + "las2","e",0,LAS_RO_FATIG,1);
    add_exit(LAS_WIEWIOREK_LOKACJE + "las3","se",0,LAS_RO_FATIG,1);
    
    add_prop(ROOM_I_INSIDE,0);
}

string
dlugi_opis()
{
    string str = "";

    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="Opis w zimie, gdy jest ^snieg. ";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str+="Opis wiosn^a. ";
    }
    else if(pora_roku() == MT_LATO)
    {
        str+="Opis latem.";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str+="Opis jesieni� i zim� gdy nie ma �niegu.";
    }

    str+="\n";
    return str;
}
