
/* Autor: Avard
   Data : 30.08.2008
   Opis : Edrain */

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"
inherit LAS_WIEWIOREK_STD;

void create_las() 
{
    set_short("Short lokacji - Las 43");
    add_exit(LAS_WIEWIOREK_LOKACJE + "las20","sw",0,LAS_H_FATIG,1);
    add_exit(LAS_WIEWIOREK_LOKACJE + "las42","n",0,LAS_H_FATIG,1);
    
    add_prop(ROOM_I_INSIDE,0); 

}

string
dlugi_opis()
{
    string str = "";

    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="Znajdujesz si^e w jasnym, ale bardzo g^estym lesie, gdzie "+
            "poskr^ecane pnie wi^az^ow z licznymi bruzdami ciekawi^a sw^a "+
            "niezwyk^lo^sci^a, a  rozga^l^ezione d^eby i strzeliste sosny "+
            "stanowi^ace swoje przeciwie^nstwo uzupe^lniaj^a flor^e lasu. ";
        if(jest_dzien() == 1)
        {
            str += "Ca^lo ci dope^lniaj^a liczne krzewy - leszczyna, "+
                "ja^lowiec, czy czeremcha obecnie bez li^sci, ale z "+
                "resztkami owoc^ow, kt^ore stanowi^a po^zywienie ptak^ow w "+
                "zim^e. Runo pokryte jest obecnie grub^a warstw^a bia^lego "+
                "^sniegu, kt^ory skrzy si^e w ^swietle t^eczowymi "+
                "iskierkami dope^lniaj^ac wra^zenia ba^sniowo^sci.";
        }
        if(jest_dzien() == 0)
        {
            str += "Ca^lo^sci dope^lniaj^a liczne krzewy strasz^ace "+
                "bezlistnymi ramionami. Runo pokryte jest obecnie grub� "+
                "arstw� bia^lego ^sniegu, kt^ory w srebrzystym ^swietle"+
                "ksi^e^zyca skrzy si^e t^eczowymi iskierkami dope^lniaj^ac "+
                "wra^zenia ba^sniowo^sci.";
        }
    }
    else if(pora_roku() == MT_WIOSNA && pora_roku() == MT_LATO)
    {
        str+="Znajdujesz si^e w jasnym, ale mimo to bardzo g^estym "+
            "lesie, gdzie poskr^ecane pnie wi^az^ow z licznymi bruzdami "+
            "ciekawi^a sw^a niezwyk^lo^sci^a. Rozga^l^ezione d^eby i "+
            "strzeliste sosny stanowi^ace swoje przeciwie^nstwo "+
            "uzupe^lniaj^a flor^e lasu. ";
        if(jest_dzien() == 1)
        {
            str += "Ca^lo^sci dope^lniaj^a liczne krzewy - leszczyna, "+
                "ja^lowiec, czy czeremcha. Runo, g^esto usiane k^epkami "+
                "fio^lk^ow i zawilc^ow, a przede wszystkim mchem, "+
                "paprociami, wrzosami i dzwonkami le nymi, ^ludzi swym "+
                "pi^eknem i sprawia wra^zenie, ^ze miejsce to jest "+
                "magiczne. Ca^ly las topi si^e w jasnym ^swietle i wydaje "+
                "si^e jakby t^etni^l w^lasnym, wewn^etrznym ^zyciem. Liczne "+
                "ptaki ^spiewaj^ace w ga^l^eziach, oraz ma^le zwierz^atka, "+
                "zamieszkuj^ace najni^zsze partie, nape^lniaj^a las ^zyciem "+
                "i harmoni^a.";
        }
        else
        {
            str += "Ca^lo^sci dope^lniaj^a krzewy, kt^orych li^scie "+
                "pob^lyskuj^a w ^swietle ksi^e^zyca. Runo, usiane mchem, "+
                "drobnymi ro^slinami i paprociami, ^ludzi swym pi^eknem i "+
                "sprawia wra^zenie, ^ze miejsce to jest magiczne. Ca^ly las "+
                "topi si^e w srebrzystym ^swietle ksi^e^zyca i wydaje si^e "+
                "jakby t^etni^l w^lasnym, wewn^etrznym ^zyciem.";
        }
    }
    else //jesien i zima gdy nie ma sniegu

    {
        str+="Znajdujesz si^e w jasnym, ale bardzo g^estym lesie, gdzie "+
            "poskr^ecane pnie wi^az^ow z licznymi bruzdami ciekawi^a sw^a "+
            "niezwyk^lo^sci^a, a rozga^l^ezione d^eby i strzeliste sosny "+
            "stanowi^ace swoje przeciwie^nstwo uzupe^lniaj^a flor^e lasu. ";
        if(jest_dzien() == 1)
        {
            str += "Ca^lo^sci dope^lniaj^a liczne krzewy - leszczyna, "+
                "ja^lowiec, czy czeremcha oraz runo g^esto usiane li^s^cmi "+
                "i szyszkami, a przede wszystkim mchem, paprociami, "+
                "wrzosami i dzwonkami le^snymi. Ca^ly las topi si^e w "+
                "jasnym ^swietle i wydaje jakby t^etni^l wewn^etrznym "+
                "^zyciem. Liczne ptaki ^spiewaj^ace w ga^l^eziach, oraz "+
                "ma^le zwierz^atka runa nape^lniaj^a las ^zyciem i "+
                "harmoni^a.";
        }
        else
        {
            str += "Ca^lo^sci dope^lniaj^a liczne krzewy, kt^orych "+
                "ga^l^ezie strasz^a ju^z prawie bezlistnymi ramionami. "+
                "Ca^ly las topi si^e w srebrzystym ^swietle ksi^e^zyca i "+
                "wydaje jakby t^etni^l wewn^etrznym ^zyciem.";
        } 
    }
    str+="\n";
    return str;
}
