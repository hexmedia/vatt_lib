
/* Autor: Avard
   Data : 30.08.2008
   Opis : Edrain */

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"
inherit LAS_WIEWIOREK_STD;

void create_las() 
{
    set_short("Short lokacji - Las 44");
    add_exit(LAS_WIEWIOREK_LOKACJE + "las45","e",0,LAS_H_FATIG,1);
    add_exit(LAS_WIEWIOREK_LOKACJE + "las24","se",0,LAS_H_FATIG,1);
    add_exit(LAS_WIEWIOREK_LOKACJE + "las22","sw",0,LAS_H_FATIG,1);
    
    add_prop(ROOM_I_INSIDE,0); 

}

string
dlugi_opis()
{
    string str = "";

    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="Gdzie nie spojrzysz - rozci�ga si� przed tob� platanina ga��zi, g�stwina lasu zamieszkana w czasie lata przez wiele ciekawych zwierz�t aktualnie wydaje si� ca�kowicie u�piona. D�by przeplataj�ce si� z grabami, wi�zami, sosnami, czy jod�ami stanowi� najliczniejsz� grup� gatunk�w w tym miejscu, a drzewa ca�kowicie sk�pane w jasno�ci [noc: �wietle ksi�yca] pot�gowanej odblaskiem �wiat�a od bielutkiego �niegu nadaj� temu miejscu wewn�trznej swietlisto�ci. [dzie�: Wiciokrzew, leszczyna czy ja�owiec to g��wne elementy podszytu le�nego / noc: R�ne krzewy rozsiane mi�dzy drzewami wdzi�cznie dope�niaj� krajobrazu], natomiast pod nogami rozpo�ciera si� bia�y dywan, kt�ry t�umi odg�osy krok�w nie zak��caj�c cicho�ci tego miejsca.";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str+="Opis wiosn^a. ";
    }
    else if(pora_roku() == MT_LATO)
    {
        str+="Opis latem.";
    }
    else //jesien i zima gdy nie ma sniegu

    {
        str+="Opis jesieni� i zim� gdy nie ma �niegu.";
    }

    str+="\n";
    return str;
}
