/* Autor: Avard 
   Data : 26.06.08
   Opis : Std do lokacji lasu wiewi�rek na po�udniowy wsch�d od Rinde, 
          a na zach�d od Murivel 
          >> Las jest mieszany, mozna dawac drzewa lisciaste i iglaste << */

#include "dir.h"

inherit REDANIA_STD;

#include <macros.h>
#include <mudtime.h>
#include <language.h>
#include <stdproperties.h>
#include <filter_funs.h>

void
create_las()
{
}

nomask void
create_redania()
{
    set_long("@@dlugi_opis@@");
    set_polmrok_long("@@opis_nocy@@");

    add_event("@@event_lasu:"+file_name(TO)+"@@");

    set_event_time(300.0);

    add_sit(({"na trawie","na ziemi"}),"na trawie","z trawy",0);
    add_sit("pod drzewem","pod jednym z drzew","spod drzewa",0);
    add_prop(ROOM_I_TYPE, ROOM_FOREST);

    add_subloc(({"drzewo", "drzewa", "drzewu", "drzewo",
        "drzewem", "drzewie"}));
    add_subloc_prop("drzewo", SUBLOC_I_MOZNA_POLOZ, 1);
    add_subloc_prop("drzewo", SUBLOC_I_MOZNA_ODLOZ, 1);
    add_subloc_prop("drzewo", SUBLOC_I_TYP_POD, 1);

    ustaw_liste_drzew(({"dab", "klon", "brzoza"}));

    dodaj_ziolo(({"czarny_bez-lisc.c",
				"czarny_bez-kwiaty.c",
				"czarny_bez-owoce.c"}));
	dodaj_ziolo("arcydziegiel.c");
	dodaj_ziolo("babka_lancetowata.c");
	dodaj_ziolo("piolun.c");

    create_las();
}

public int
unq_no_move(string str)
{
    notify_fail("Niestety, g^este zaro^sla nie pozwalaj^a ci "+
        "p^oj^s^c w tym kierunku.\n");

    string vrb = query_verb();

    if(vrb ~= "g�ra" || vrb ~= "d�")
        notify_fail("Nie mo�esz tam si� uda�!\n");

    return 0;
}

string
event_lasu()
{
    switch (pora_dnia())
    {
        case MT_RANEK:
        case MT_POLUDNIE:
        case MT_POPOLUDNIE:
            return process_string(({"Promyki ^swiat^la nie^smia^lo "+
            "przedzieraj^a si^e przez ga^l^ezie.\n",
            "@@pora_roku_dzien@@"})[random(2)]);
        default: // wieczor, noc
            return process_string(({"K^atem oka dostrzegasz jaki^s ruch, ale "+
            "gdy odwracasz g^low^e las jest cichy i pusty.\n",
            "Wrona z zapa^lem wyskrzekuje swoj^a pie^s^n.\n",
            "Masz wra^zenie, ^ze obserwuje ci^e jakie^s zwierz^e.\n",
            "W oddali s^lyszysz ryk dzikiego zwierz^ecia.\n",
            "@@pora_roku_noc@@"})[random(5)]);
    }
}
string
pora_roku_dzien()
{
    switch (pora_roku())
    {
         case MT_LATO:
         case MT_WIOSNA:
             return ({
                "Li^scie drzew delikatnie poruszaj^a si^e na wietrze.\n",
                "Gdzie^s wysoko w koronach drzew ptaki weso^lo ^cwierkaj^a.\n",
                "Tu^z nad tob^a dwie wiewi^orki ^scigaj^a si^e po drzewach.\n",
                "Z g^estwiny nagle zrywa si^e ptak i z gwa^ltownym krzykiem odlatuje w przestworza.\n",
                "Trawa ko^lysze si^e pod mu^sni^eciem wiatru.\n",
                "Ptasi trel wznosi si^e w weso^lym crescendo, po czym znowu cichnie i uspokaja si^e.\n",
                "Z g^l^ebi lasu dobiega ci^e melodyjny ^swiergot ptak^ow.\n",
                "Ciep^ly wiatr muska twoj^a twarz.\n"
                })[random(8)];
         case MT_JESIEN:
             return ({
                "Bezlistne ga^l^ezie muskaj^a tw^oj policzek.\n",
                "Wysoko w g^orze wiatr porusza ga^leziami.\n",
                "Zimny wiatr muska ci^e po twarzy.\n",
                "Poruszane wiatrem ga^lezie szeleszcz^a suchymi li^s^cmi.\n",
                "Porwane przez wiatr li^s^cie zaczynaj^a unosi^c si^e ku "+
                "g^orze.\n",
                "Trawa ko^lysze si^e pod mu^sni^eciem wiatru.\n"
                })[random(6)];
         case MT_ZIMA:
             return ({
                "Bezlistne ga^l^ezie muskaj^a tw^oj policzek.\n",
                "Zimny wiatr muska ci^e po twarzy.\n",
                "Lodowaty wiatr muska ci^e po twarzy.\n",
                "Wysoko w g^orze wiatr porusza ga^leziami.\n",
                "Niedaleko ciebie kupka ^sniegu spada z ga^l^ezi i upada na "+
                "ziemi^e.\n",
                "Jaka^s zagubiona wrona przysiad^la na ga^l^ezi i "+
                "kracze zapami^etale.\n",
                "Z ga^l^ezi spada grudka ^sniegu, "+
                "niemal trafiaj^ac ci^e w g^low^e.\n"
                })[random(7)];
    }
}
string
pora_roku_noc()
{
    switch (pora_roku())
    {
         case MT_LATO:
         case MT_WIOSNA:
             return ({"Li^scie drzew delikatnie poruszaj^a si^e na "+
             "wietrze.\n","Trawa ko^lysze si^e pod mu^sni^eciem wiatru.\n"})
             [random(2)];
         case MT_JESIEN:
             return ({"Bezlistne ga^l^ezie muskaj^a tw^oj policzek.\n",
             "Wysoko w g^orze wiatr porusza ga^leziami.\n",
             "Zimny wiatr muska ci^e po twarzy.\n",
             "Poruszane wiatrem ga^lezie szeleszcz^a suchymi li^s^cmi.\n",
             "Porwane przez wiatr li^s^cie zaczynaj^a unosi^c si^e ku "+
             "g^orze.\n","Trawa ko^lysze si^e pod mu^sni^eciem wiatru.\n"})
             [random(5)];
         case MT_ZIMA:
             return ({"Bezlistne ga^l^ezie muskaj^a tw^oj policzek.\n",
             "Z ga^l^ezi spada grudka ^sniegu, niemal "+
             "trafiaj^ac ci^e w g^low^e.\n","Zimny wiatr muska ci^e po "+
             "twarzy.\n","Lodowaty wiatr muska ci^e po twarzy.\n",
             "Wysoko w g^orze wiatr porusza ga^leziami.\n",
             "Niedaleko ciebie kupka ^sniegu spada z ga^l^ezi i upada na "+
             "ziemi^e.\n"})[random(7)];
    }
}
