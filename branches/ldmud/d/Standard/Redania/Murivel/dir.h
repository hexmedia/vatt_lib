#include "../dir.h"



#define MURIVEL_PRZEDMIOTY    (MURIVEL + "przedmioty/")
#define MURIVEL_BRONIE        (MURIVEL_PRZEDMIOTY + "bronie/")
#define MURIVEL_ZBROJE        (MURIVEL_PRZEDMIOTY + "przedmioty/zbroje/")
#define MURIVEL_UBRANIA       (MURIVEL_PRZEDMIOTY + "przedmioty/ubrania/")
#define MURIVEL_NARZEDZIA     (MURIVEL_PRZEDMIOTY + "przedmioty/narzedzia/")

#define MURIVEL_ZACHOD_DRZWI       (MURIVEL_ZACHOD + "drzwi/")
#define MURIVEL_ZACHOD_OBIEKTY     (MURIVEL_ZACHOD + "obiekty/")
#define MURIVEL_ZACHOD_BOGATE      (MURIVEL_ZACHOD + "bogate/")

#define MURIVEL_WSCHOD              (MURIVEL + "Wschod/")
#define MURIVEL_WSCHOD_DRZWI        (MURIVEL_WSCHOD + "drzwi/")
#define MURIVEL_WSCHOD_OBIEKTY      (MURIVEL_WSCHOD + "obiekty/")
#define MURIVEL_WSCHOD_ULICE        (MURIVEL_WSCHOD + "ulice/")
#define MURIVEL_WSCHOD_KARCZMA      (MURIVEL_WSCHOD + "Karczma/")
#define MURIVEL_WSCHOD_PARK         (MURIVEL_WSCHOD + "Park/")
#define MURIVEL_WSCHOD_ZAJAZD        (MURIVEL_WSCHOD + "Zajazd/")

#define MURIVEL_POLUDNIE            (MURIVEL + "Poludnie/")
#define MURIVEL_POLUDNIE_DRZWI      (MURIVEL_POLUDNIE + "drzwi/")
#define MURIVEL_POLUDNIE_OBIEKTY    (MURIVEL_POLUDNIE + "obiekty/")
#define MURIVEL_POLUDNIE_ULICE      (MURIVEL_POLUDNIE + "ulice/")
#define MURIVEL_POLUDNIE_SPALONE    (MURIVEL_POLUDNIE + "spalone/")

#define MURIVEL_STD         (MURIVEL + "std/murivel.c")
#define MURIVEL_STD_NPC     (MURIVEL + "std/npc.c")


//TAKA DEFINICJA POWINNA BY� WSZ�DZIE JE�LI CHCEMY BY MIEJSCE STARTOWE MOG�O BY�
//ODPOWIEDNIO USTAWIONE
#ifdef SLEEP_PLACE
#undef SLEEP_PLACE
#endif SLEEP_PLACE

#define SLEEP_PLACE         (MURIVEL_WSCHOD + "Karczma/lokacje/sala.c")


