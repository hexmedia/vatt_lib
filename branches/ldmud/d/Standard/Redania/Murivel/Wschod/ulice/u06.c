/* Autor: 
   Opis : 
   Data : */

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"
#include <pogoda.h>

inherit MURIVEL_STD;

void create_ulica() 
{
    set_short("Ulica");

    add_exit(MURIVEL_WSCHOD_ULICE + "u07","n",0,1,0);
    add_exit(MURIVEL_WSCHOD_ULICE + "u05","sw",0,1,0);

    add_prop(ROOM_I_INSIDE,0);
    add_object(MURIVEL_PRZEDMIOTY + "lampa_uliczna");
}

public string
exits_description() 
{
    return "Uliczka biegnie z po^ludniowego zachodu na p^o^lnoc. \n";
}

string
dlugi_opis()
{
    string str;
    if(jest_dzien())
    {
        str = "Dzie^n.";
    }
    else
    {
        str = "Noc.";
    }
    str += "\n";
    return str;
}
