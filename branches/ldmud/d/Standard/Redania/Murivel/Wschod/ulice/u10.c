/* Autor: 
   Opis : 
   Data : */

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"
#include <pogoda.h>

inherit MURIVEL_STD;

void create_ulica() 
{
    set_short("Ulica");

    add_exit(MURIVEL_WSCHOD_ULICE + "u04","w",0,1,0);
    add_exit(MURIVEL_WSCHOD_ULICE + "u11","se",0,1,0);

    add_object(MURIVEL_WSCHOD_DRZWI + "do_cukierni");

    //plus wejscie do piekarni

    add_prop(ROOM_I_INSIDE,0);
    add_object(MURIVEL_PRZEDMIOTY + "lampa_uliczna");
}

public string
exits_description() 
{
    return "Uliczka prowadzi na z zachodu na po^ludniowy wsch^od, za^s na "+
        "p^o^lnocnym wschodzie znajduje si^e piekarnia. \n";
}

string
dlugi_opis()
{
    string str;
    if(jest_dzien())
    {
        str = "Dzie^n.";
    }
    else
    {
        str = "Noc.";
    }
    str += "\n";
    return str;
}
