/* Autor: 
   Opis : 
   Data : */

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"

inherit "/std/room";

void create_room() 
{
    set_short("W parku.");

    add_exit(MURIVEL_WSCHOD_PARK +"lokacje/p4.c","ne",0,1,0);
    add_exit(MURIVEL_WSCHOD_PARK +"lokacje/p2.c","se",0,1,0);
    add_exit(MURIVEL_WSCHOD_PARK +"lokacje/fontanna.c","e",0,1,0);


    add_prop(ROOM_I_INSIDE,1);
}

public string
exits_description() 
{
    return "Alejka biegnie na p^o^lnocny oraz po^ludniowy wsch^od, na "+
        "wschodzie znajduje si^e fontanna. Na zachodzie widnieje furtka "+
        "wyj^sciowa z parku. \n";
}
//ten opis wyjscia trzeba poprawic, tak tylko napisalam ;p

string
dlugi_opis()
{
    string str;
    
    str = "\n";

    return str;
}
