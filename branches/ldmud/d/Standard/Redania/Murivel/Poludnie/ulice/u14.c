/* Autor: 
   Opis : 
   Data : */

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"
#include <pogoda.h>

inherit MURIVEL_STD;

void
create_ulica()
{
    set_short("Uliczka");
    add_exit(MURIVEL_POLUDNIE_ULICE + "u15.c", "ne");
    add_exit(MURIVEL_POLUDNIE_ULICE + "u17.c", "se");
    add_exit(MURIVEL_POLUDNIE_ULICE + "u10.c", "w");

    add_object(MURIVEL_POLUDNIE_DRZWI + "do_domu2");

    add_object(MURIVEL_PRZEDMIOTY + "lampa_uliczna");

}

public string
exits_description() 
{
    return "Uliczka prowadzi na p^o^lnocny wsch^od, po^ludniowy wsch^od "+
        "oraz zach^od. Na po^ludniu znajduje si^e wej^scie do jakiego^s "+
        "domu. \n";
}

string
dlugi_opis()
{
    string str;
    if(jest_dzien())
    {
        str = "Dzie^n.";
    }
    else
    {
        str = "Noc.";
    }
    str += "\n";
    return str;
}