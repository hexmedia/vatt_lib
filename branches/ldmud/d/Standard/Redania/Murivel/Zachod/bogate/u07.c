/* Autor: 
   Opis : 
   Data : */

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"
#include <pogoda.h>

inherit MURIVEL_STD;

void create_ulica() 
{
    set_short("Ulica");

    add_exit(MURIVEL_ZACHOD_BOGATE + "u06","n",0,1,0);
    add_exit(MURIVEL_ZACHOD_BOGATE + "u08","e",0,1,0);

    add_object(MURIVEL_ZACHOD_DRZWI + "do_banku");

    // plus bank na zachodzie

    add_prop(ROOM_I_INSIDE,0);
    add_object(MURIVEL_PRZEDMIOTY + "lampa_uliczna");
}

public string
exits_description() 
{
    return "Brukowana uliczka wiedzie na p^o^lnoc oraz na wsch^od. Po "+
        "stronie zachodniej widnieje budynek banku. \n";
}

string
dlugi_opis()
{
    string str;
    if(jest_dzien())
    {
        str = "Dzie^n.";
    }
    else
    {
        str = "Noc.";
    }
    str += "\n";
    return str;
}
