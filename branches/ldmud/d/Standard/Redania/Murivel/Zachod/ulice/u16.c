/* Autor: 
   Opis : 
   Data : */

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"
#include <pogoda.h>

inherit MURIVEL_STD;

void create_ulica() 
{
    set_short("Ulica");

    add_exit(MURIVEL_ZACHOD_ULICE + "u15","w",0,1,0);
    add_exit(MURIVEL_ZACHOD_ULICE + "u17","e",0,1,0);
    add_exit(MURIVEL_ZACHOD_ULICE + "u14","nw",0,1,0);
    add_exit(MURIVEL_ZACHOD_ULICE + "u18","ne",0,1,0);

    add_object(MURIVEL_ZACHOD_DRZWI + "do_ratusza");

//    add_object(""); drzwi do ratusza



    add_prop(ROOM_I_INSIDE,0);
    add_object(MURIVEL_PRZEDMIOTY + "lampa_uliczna");
}

public string
exits_description() 
{
    return "Uliczka prowadzi na zach^od, p^o^lnocny zach^od, p^o^lnocny "+
        "wsch^od oraz wsch^od, na p^o^lnocy za^s znajduj^a si^e drzwi "+
        "ratusza.\n";
}

string
dlugi_opis()
{
    string str;
    if(jest_dzien())
    {
        str = "Dzie^n.";
    }
    else
    {
        str = "Noc.";
    }
    str += "\n";
    return str;
}
