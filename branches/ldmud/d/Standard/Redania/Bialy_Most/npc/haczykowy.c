/*
 * opis Valora
 * zrobila faeve
 * bo le^n ogl^ada film.
 * dn. 14.06.07
 *
 */
// #pragma unique

#include <macros.h>
#include <stdproperties.h>
#include <money.h>
#include <pl.h>
#include <formulas.h>
#include <std.h>
#include <ss_types.h>
#include <wa_types.h>
#include <composite.h>
#include <filter_funs.h>
#include <object_types.h>
#include <pogoda.h>
#include "dir.h"

inherit BIALY_MOST_STD_NPC;
inherit "/lib/sklepikarz.c";

int walka = 0;

int
co_godzine();

void
create_bm_humanoid()
{
    ustaw_odmiane_rasy("m^e^zczyzna");
    dodaj_nazwy("kupiec");
    set_gender(G_MALE);

    set_long("Wy^lupiaste oczy i wzrost jak u m^lodego ch^lopca to dwie "+
		"najbardziej rzucaj^ace si^e w oczy cechy tego m^e^zczyzny. Zaraz po "+
		"nich jest chciwy u^smieszek, kt^ory zdaje si^e nigdy nie schodzi^c "+
		"z jego twarzy. Bia^le niczym mleko, rzadkie w^losy oraz sie^c "+
		"zmarszczek pokrywaj^aca ca^l^a jego twarz. Ostatni^a cech^a jest "+
		"ogromny garb t^lumacz^acy niezwykle niski wzrost starca. Widoczny "+
		"jest on dopiero wtedy, gdy ten odwr^oci si^e bokiem. Ma na sobie "+
		"przyciasn^a ciemn^a, sk^orzan^a kamizelk^e - a^z dziw bierze, ^ze "+
		"uda^lo mu si^e j^a jako^s za^lo^zy^c, oraz par^e przybrudzonych "+
		"br^azowych spodni.\n");

    dodaj_przym("garbaty", "garbaci");
    dodaj_przym("wy^lupiastooki", "wy^lupiastoocy");

    set_act_time(70);
    add_act("emote rozgl^ada si^e w poszukiwaniu potencjalnego klienta.");
    add_act("usmiech pod nosem");
    add_act("emote dostaje nag^lego ataku kaszlu kt^ory po chwili mu "+
		"przechodzi.");
    add_act("emote przek^lada w^edki z miejsca na miejsce przygl^adaj^ac im "+
		"si^e w skupieniu. ");
    add_act("emote bierze szmatk^e po czym zaczyna ni^a polerowa^c "+
		"najbli^zsz^a w^edk^e.");
    add_act("emote mruczy do siebie: Eh, gdybym zn^ow m^og^l by^c m^lody.");
    add_act("popatrz zachecajaco na mezczyzne");
    add_act("popatrz zachecajaco na kobiete");
	add_act("krzyczy: W^edki! Haczyki! Sp^lawiki!");
	add_act("krzyczy: Wszelki osprz^et w^edkarski! Tanio!");

    set_cchat_time(15);
    add_cchat("Hola! To ^ze jestem stary, nie znaczy, ^ze chc^e ju^z "+
		"umiera^c!");
    add_cchat("Stra^z!");
    add_cchat("Stra^z, na pomoc!");

    set_default_answer(VBFC_ME("default_answer"));
    add_ask(({"Bia^ly Most","miasto"}), VBFC_ME("pyt_miasto"));
    add_ask("ryby", VBFC_ME("pyt_ryby"));
    add_ask("haczyki", VBFC_ME("pyt_haczyki"));
	add_ask("w^edki", VBFC_ME("pyt_wedki"));


    set_stats (({30, 60, 28, 55, 35}));

    set_skill(SS_APPR_OBJ, 70 + random(4));
    set_skill(SS_APPR_VAL, 83 + random(10));
    set_skill(SS_AWARENESS, 70 + random(10));
    set_skill(SS_UNARM_COMBAT, 30 + random(5));

    add_armour(BIALY_MOST_UBRANIA + "spodnie/przybrudzone_brazowe_Mc");
    add_armour(BIALY_MOST_UBRANIA + "kamizelki/brazowa_skorzana_Mc");
    add_object(BIALY_MOST_UBRANIA + "buty/stare_skorzane_Mc");

    add_prop(CONT_I_WEIGHT, 80000);
    add_prop(CONT_I_HEIGHT, 175);

    set_alarm_every_hour("co_godzine");
    co_godzine();

    config_default_sklepikarz();
    //set_money_greed_buy(112);
    //set_money_greed_sell(130);

    set_money_greed_buy(212);
    set_money_greed_sell(657);

	set_money_greed_change(100+random(40)); //hihi! :D
    set_co_skupujemy(O_WEDKARSKIE);

    set_store_room("/d/Standard/Redania/Bialy_Most/lokacje/magazyn_haczyka");

    set_alarm(6.0, 0.0, "command", "stan za straganem");
    set_alarm(9.0, 0.0, "command", "emote wyk^lada towar na stragan.");

    //add_prop(NPC_M_NO_ACCEPT_GIVE, 0);

}

void
shop_hook_tego_nie_skupujemy(object ob)
{
    command("powiedz Skupuj^e tylko w^edkarski osprz^et!");
}

void
powiedz_gdy_jest(object player, string tekst)
{
    if ((environment(this_object()) == environment(player)) && CAN_SEE(this_object(), player))
        this_object()->command(tekst);
    else
        this_object()->command("wzrusz ramionami");
}

string
default_answer()
{
    string odp = " ";

    TO->command("pokrec powoli");

    switch(random(3))
    {
        case 0:
            odp += "Nie mam poj^ecia o co ci chodzi.";
            break;
        case 1:
            odp += "Nie wiem, co masz na my^sli.";
            break;
    }

    set_alarm(2.0, 0.0, "powiedz_gdy_jest", this_player(), "powiedz do " +
        OB_NAME(this_player()) + odp);
    return "";
}

string
pyt_pomoc()
{
    if (this_player()->query_prop("pytal_kupcowazrynku_praca") == 0)
    {
        set_alarm(1.0, 0.0, "powiedz_gdy_jest", TP, "powiedz do " + OB_NAME(TP) +
        " Nie trzeba, poradz^e sobie. Ale dzi^ekuj^e za trosk^e.");
        set_alarm(2.5, 0.0, "powiedz_gdy_jest", TP, "usmiech milo do " + OB_NAME(TP));
        TP->add_prop("pytal_haczykowego_praca", 1);
    }
    else
        set_alarm(1.0, 0.0, "powiedz_gdy_jest",
        "':z pogodnym u^smiechem: Przecie^z ju^z m^owi^lem... ");

    return "";
}

string
pyt_praca()
{
    set_alarm(1.0, 0.0, "powiedz_gdy_jest", TP, "powiedz do " + OB_NAME(TP) +
        " Zarabiam sprzedaj^ac w^edki haczyki i inny ekwipunek zdatny dla "+
        "w^edkarza.");
    set_alarm(2.5, 0.0, "command", "wskaz stragan");

    return "";
}

string
pyt_towar()
{
    set_alarm(1.0, 0.0, "powiedz_gdy_jest", TP, "powiedz do " + OB_NAME(TP) +
        " W^edki, haczyki, wszystko, co potrzebne "+
        "do ^lowienia.");
}

void
return_introduce(object ob)
{
    if (environment() == environment(ob))
    {
        set_alarm(1.0, 0.0, "command", "pokiwaj lekko");
        set_alarm(2.0, 0.0, "command", "powiedz Mi^lo mi pozna^c.");
    }

    return;
}

void
emote_hook(string emote, object wykonujacy, string przyslowek)
{
    switch (emote)
    {
        case "kopnij":
        case "spoliczkuj":
        case "opluj":
        case "nadepnij":
            set_alarm(2.0, 0.0, "zly", wykonujacy);
            break;

        case "szturchnij":
            set_alarm(2.0, 0.0, "taki_sobie", wykonujacy);
            break;

        case "za�miej":
        case "poca^luj":
        case "przytul":
        case "pog^laszcz":
            set_alarm(2.0, 0.0, "dobry", wykonujacy);
            break;
    }
}

void
zly(object kto)
{
    switch(random(3))
    {
        case 0:
            set_alarm(1.0, 0.0, "powiedz_gdy_jest", kto,
                "krzyknij Odejd^x, bo zawo^lam stra^z! ");
            break;
        case 1:
            set_alarm(1.0, 0.0, "powiedz_gdy_jest", kto,
                "krzyknij Zostaw mnie bandyto! ");
            break;
        case 2:
            set_alarm(1.0, 0.0, "powiedz_gdy_jest", kto,
                "popatrz z wyrzutem na "+ OB_NAME(kto));
            break;
   }
}


void
taki_sobie(object kto)
{
    command("zagryz wargi");
}

void
dobry(object kto)
{
    if(kto->query_gender() == 1)
    {
        switch(random(3))
        {
            case 0:
                powiedz_gdy_jest(kto, "powiedz do " + OB_NAME(kto) +
                    " Zwinny masz j^ezyczek, szkoda, ^ze to ju^z nie te lata...");
                break;
            case 1:
                powiedz_gdy_jest(kto, "powiedz do " + OB_NAME(kto) +
                    " Ach, gdybym by^l m^lodszy...");
                break;
        }
    }
    else
    {
        switch(random(3))
        {
            case 0:
                command("skrzyw sie");
                break;
            case 1:
                command_present(kto, "':do " + OB_NAME(kto) + " wykrzywiaj^ac si^e z "+
                    "obrzydzeniem: Ej^ze, panie, co pan wyczynia, h^e? ");
                break;
            case 2:
                command_present(kto, "spojrzyj z obrzydzeniem na " + OB_NAME(kto));
                break;
        }
    }
}
void
attacked_by(object wrog)
{
    if(walka==0)
    {
        command("wyjdz zza straganu");
        command("krzyknij Ratunku!");

        set_alarm(5.0, 0.0, "czy_walka", 1);
        walka = 1;

        return ::attacked_by(wrog);
    }

    else if(walka == 1)
    {
        walka = 1;

        return ::attacked_by(wrog);
    }
}

int
czy_walka()
{
    if(!query_attack())
    {
        set_alarm(1.0, "command", "sapnij szybko");
        go_to("/d/Standard/Redania/Rinde/Centrum/lokacje/placne",
            &command("stan za drugim straganem"));
        walka = 0;

        return 1;
    }
    else
        set_alarm(5.0, 0.0, "czy_walka", 1);
}

void
shop_hook_list_empty_store(string str)
{
    command("powiedz W tej chwili nie mam nic do sprzedania, ale zajrzyj do "+
        "mnie za jaki^s czas.");
    set_alarm(1.0, 0.0, "command","mrugnij");
}

void
hook_zabr_podrz_wlas_drog(object kto, object co)
{
    set_alarm(1.0, 0.0, "command", "poaptrz szybko na"+OB_NAME(kto));
}

void
hook_zabr_podrz_wlas_tan(object kto, object co)
{
    set_alarm(1.0, 0.0, "powiedz_gdy_jest", kto, "powiedz do " + OB_NAME(kto)+
    " Lepiej sam pilnuj swoich rzeczy. ");
}

void
hook_zabr_podrz_niewl_wljest(object kto, object co, string czyje)
{
    set_alarm(1.0, 0.0, "powiedz_gdy_jest", kto, "powiedz do " + OB_NAME(kto)+
        " Hej, zaraz! T" + co->koncowka("en", "a", "o", "e", "e") + " " +
    co->query_nazwa() + " jest te" + find_player(lower_case(co->query_prop("podrzucon" +
        "e_na_stragan")))->koncowka("go pana", "ej pani", "go pana") + "!");
    set_alarm(1.5, 0.0, "command", "wskaz " +
        OB_NAME(find_player(co->query_prop("podrzucone_na_stragan"))));
    co->add_prop("podrzucone_na_stragan", czyje);
}

void
hook_zabr_podrz_niewl_wlniema(object kto, object co)
{
    set_alarm(1.0, 0.0, "powiedz_gdy_jest", kto, "powiedz do " + OB_NAME(kto)+
        " Hej, to moje!");
}

void
hook_zabr_pierwsza_rzecz(object kto, object co)
{
    set_alarm(1.0, 0.0, "powiedz_gdy_jest", kto, "powiedz do " + OB_NAME(kto)+
        " O, wypraszam to sobie! Te przedmioty nie s^a do brania! Niech pan"
        + TP->koncowka("", "i") + " to od^lo^zy!");
}

void
hook_zabr_pierwsza_rzecz_vip(object kto, object co)
{
    set_alarm(1.0, 0.0, "command", "popatrz dlug na " + OB_NAME(kto));
}

void
hook_zabr_druga_rzecz(object kto, object co)
{
    set_alarm(1.0, 0.0, "command", "krzyknij Hej, oddaj to, z^lodzieju! ");
    set_alarm(2.0, 0.0, "command", "wyjdz zza straganu");
    set_alarm(3.0, 0.0, "command", "zabij " + OB_NAME(kto));
}

void
hook_zabr_podrz_wlas_x(object kto, object co)
{
    set_alarm(1.0, 0.0, "powiedz_gdy_jest", kto, "powiedz do " + OB_NAME(kto)+
        " Prosz^e natychmiast odda^c to, co pan" + kto->koncowka(" zabra^l",
        "i zabra^la") + "!");
}

void
hook_pol_swoj_tan(object kto, object co)
{
    set_alarm(1.0, 0.0, "powiedz_gdy_jest", kto, "powiedz do " + OB_NAME(kto)+
        " Lepiej niech to pan" + TP->koncowka("", "i") + " st^ad zabierze, " +
        "jeszcze kto^s ukradnie.");
}

void
hook_pol_swoj_drog_wljest(object kto, object co)
{
    set_alarm(1.0, 0.0, "command", "rozejrzyj sie szybko");
    set_alarm(2.0, 0.0, "powiedz_gdy_jest", kto, "powiedz do " + OB_NAME(kto)+
        " Prosz^e to natychmiast st^ad zabra^c!");
}

void
hook_pol_swoj_drog_wlniema(object kto, object co)
{
    set_alarm(1.0, 0.0, "command", "powiedz :cicho: Gdzie ci ludzie maj^a oczy?");
    set_alarm(2.0, 0.0, "command", "wez " + OB_NAME(co));
    set_alarm(3.0, 0.0, "command", "pokrec ciezko");
}

void
hook_pol_zabrane_nieten(object kto, object co)
{
    set_alarm(1.0, 0.0, "command", "powiedz Oh, dzi^ekuj^e.");
    set_alarm(2.0, 0.0, "command", "podziekuj serdecznie " + OB_NAME(kto));
}

void
hook_pol_zabrane_ten(object kto, object co)
{
    set_alarm(1.0, 0.0, "powiedz_gdy_jest", kto, "powiedz do " + OB_NAME(kto)+
        " Wystarczy^lo poprosi^c o pokazanie!");
    set_alarm(2.0, 0.0, "command", "popatrz lekko na " + OB_NAME(kto));
}

void
hook_pol_swoj_ten(object kto, object co)
{
    set_alarm(1.0, 0.0, "powiedz_gdy_jest", kto, "powiedz do " + OB_NAME(kto)+
        " A co to? Prosz^e mi to natychmiast odda^c! To moje!!");
}

void
hook_pol_zabrane_zlodziej(object kto, object co)
{
    set_alarm(1.0, 0.0, "powiedz_gdy_jest", kto, "powiedz do " + OB_NAME(kto)+
        " A reszta?");
}

void
hook_oddal_zabr_ten(object kto, object co)
{
    hook_pol_zabrane_ten(kto, co);
}

void
hook_oddal_zabr_nieten(object kto, object co)
{
    hook_pol_zabrane_nieten(kto, co);
}

void
hook_dal_ten(object kto, object co)
{
    set_alarm(1.0, 0.0, "powiedz_gdy_jest", kto, "powiedz do " + OB_NAME(kto)+
        " Prosz^e mi to natychmiast odda^c!!");

    if (environment() == environment(kto))
    {
        co->remove_prop("podrzucone_na_stragan");
        set_alarm(2.0, 0.0, "command", "daj " + OB_NAME(co) + " " +
        OB_NAME(kto));
    }
    else
        set_alarm(2.0, 0.0, "command", "poloz " + OB_NAME(co) + " na " +
        this_object()->query_prop("stoi_za_straganem"));
}

void
hook_dal_ktos(object kto, object co)
{
    if(!co->query_coin_type())
    {
        set_alarm(1.0, 0.0, "powiedz_gdy_jest", kto, "powiedz do " + OB_NAME(kto)+
            " Nie, dzi�kuj�. Po co mi to?.");

        if (environment() == environment(kto))
        {
            co->remove_prop("podrzucone_na_stragan");
            set_alarm(2.0, 0.0, "command", "daj " + OB_NAME(co) + " " +
                OB_NAME(kto));
        }
        else
        {
            set_alarm(2.0, 0.0, "command", "poloz " + OB_NAME(co) + " na " +
                this_object()->query_prop("stoi_za_straganem"));
        }
    }

}

void
enter_inv(object ob, object from)
{
    ::enter_inv(ob, from);

    if(from->is_humanoid() == 0)
        return;

    if(ob->query_prop("zabrane_ze_straganu"))
    {
        if(from->query_prop("zabral_ze_straganu"))
        {
            hook_oddal_zabr_ten(from, ob);
            ob->remove_prop("zabrane_ze_straganu");
            from->remove_prop("zabral_ze_straganu");
            return;
        }
        else
        {
            hook_oddal_zabr_nieten(from, ob);
            ob->remove_prop("zabrane_ze_straganu");
            return;
        }
    }
    else
    {
        if (from->query_prop("zabral_ze_straganu"))
        {
            ob->add_prop("podrzucone_na_stragan", from->query_name());
            hook_dal_ten(from, ob);
            return;
        }
        else
        {
            ob->add_prop("podrzucone_na_stragan", from->query_name());
            hook_dal_ktos(from, ob);
            return;
        }
    }
}

void
leave_inv(object ob, object to)
{
    ::leave_inv(ob, to);
}

void
znik()
{
    set_alarm(5.0, 0.0, "command", "emote znika w drzwiach jednej z kamieniczek.");
    remove_object();
}

void
do_domu()
{
    if(query_attack())
    {
        set_alarm(10.0,0.0,"do_domu");
        return;
    }
    else
    {
        int k = 0;
        mixed droga2=znajdz_sciezke(environment(this_object()),
            BIALY_MOST_LOKACJE + "ulice/u03");
        command(droga2[k]);
        if(k==sizeof(droga2)-1)
        {
            znik();
            return;
        }
        else
        {
            k++;
            set_alarm(15.0,0.0,"do_domu");
            return;
        }
    }
}

int
co_godzine()
{
    if (environment(TO)->pora_dnia() >= MT_WIECZOR)
    {
        set_alarm(50.0, 0.0, "command", "emote zdejmuje towar ze straganu.");
        set_alarm(53.0, 0.0, "command", "wyjdz zza straganu");
        set_alarm(55.0, 0.0, "do_domu");
    }
}

void
init()
{
    ::init();
    init_sklepikarz();
}
