/*
 * opis by Ohm
 * kodowa� Ohm, poprawki faeve
 * 16.06.07
 *
 */

inherit "/std/act/action";
inherit "/std/act/chat";
inherit "/std/act/asking";

#include <object_types.h>
#include <cmdparse.h>
#include <macros.h>
#include <money.h>
#include <pl.h>
#include <stdproperties.h>
#include <ss_types.h>
#include <exp.h>

#include "dir.h"
inherit BIALY_MOST_STD_NPC;

#define BUTY (BIALY_MOST_UBRANIA +"buty/eleganckie_zamszowe_Mc")
#define SUKNIA (BIALY_MOST_UBRANIA +"suknie/zielonkawa_elegancka_Mc")

void
create_bm_humanoid()
{

    add_prop(CONT_I_WEIGHT, 51000);
    add_prop(CONT_I_HEIGHT, 173);
    add_prop(NPC_I_NO_RUN_AWAY, 0);
    set_stats (({ 20, 20, 20, 46, 20}));

    set_cchat_time(5);
    add_cchat("krzyknij Pomocy!");
    add_cchat("krzyknij Prosz^e! Przesta^n!");

    set_act_time(10);
    add_act("emote rozgl^ada si^e powoli po okolicy.");
    add_act("emote przeczesuje w^losy r^ek^a.");
    add_act("zanuc");
    add_act("emote u^smiecha si^e do jednego z przechodz^acych m^e^zczyzn.");
    add_act("westchnij cicho");

	add_armour(BUTY);
	add_armour(SUKNIA);

	ustaw_odmiane_rasy(PL_KOBIETA);
	dodaj_przym("^ladny", "^ladni");
	dodaj_przym("wysoki", "wysocy");
	dodaj_nazwy("mieszczanka");

    set_gender(1);

	set_long("Ta do^s^c wysoka kobieta, o d^lugich si^egaj^acych po^lowy "+
	"plec^ow br^azowych w^losach i mi^lym u^smiechu, wygl^ada na ^srednio "+
	"zamo^zn^a mieszczank^e. Zapewne jest lubiana przez innych mieszka^nc^ow "+
	"miasta, gdy^z du^za ilo^s^c przechodni^ow k^lania si^e jej z usmiechem na "+
	"twarzy. Co prawda interesuj^acym faktem jest, i^z g^l^ownie s^a to "+
	"me^zczy^xni. Ubrana jest w eleganckie zamszowe buty i w dobrze skrojon^a "+
	"zielonkaw^a sukni^e, kt^ora idealnie podkre^sla jej doskona^l^a figur^e.\n");


	set_default_answer(VBFC_ME("default_answer"));

	set_random_move(100);
	set_restrain_path(BIALY_MOST_LOKACJE_ULICE);
}

void
powiedz_gdy_jest(object player, string tekst)
{
    if (environment(this_object()) == environment(player))
	this_object()->command(tekst);
}


void
return_introduce(string imie)
{
    object osoba;

    osoba = present(imie, environment());

    if (osoba)
        command("powiedz Ach witaj, " + osoba->query_wolacz() + ".");
}


void
emote_hook(string emote, object wykonujacy, string przyslowek)
{
    switch (emote)
    {
        case "uk^lo^n":
            command("poklon sie lekko");
            break;
		case "pog^laszcz":
			set_alarm(1.0, 0.0, "powiedz_gdy_jest", wykonujacy,
            "powiedz trzymaj r^ece przy sobie. ");
            break;
        case "poca^luj":
			set_alarm(1.0, 0.0, "powiedz_gdy_jest",wykonujacy,
            "powiedz pozwalasz sobie na zbyt wiele. ");
            break;
        case "przytul":
			set_alarm(1.0, 0.0, "powiedz_gdy_jest", wykonujacy,
            "powiedz trzymaj r^ece przy sobie. ");
            break;
        case "kopnij":
			set_alarm(1.0, 0.0, "powiedz_gdy_jest", wykonujacy,
            "krzyknij Odejd^x, bo zawo^lam stra^z! ");
            break;
		case "opluj":
			set_alarm(1.0, 0.0, "powiedz_gdy_jest", wykonujacy,
            "krzyknij Odejd^x, bo zawo^lam stra^z! ");
            break;

    }
}

string
default_answer()
{
    if(!query_attack())
    {
    switch(random(4))
    {
    case 0:set_alarm(1.0, 0.0, "powiedz_gdy_jest", this_player(),
        "powiedz Nie mam poj^ecia o co ci chodzi. "); break;
    case 1:set_alarm(1.0, 0.0, "powiedz_gdy_jest", this_player(),
        "powiedz Zostaw mnie w spokoju. "); break;
	case 2:set_alarm(1.0, 0.0, "command_present", this_player(),
		"pokrec lekko"); break;
	case 3:set_alarm(1.0, 0.0, "command_present", this_player(),
		"popatrz powoli na "+TP->query_real_name()); break;

    }
    }
    else
    {
        set_alarm(1.0,0.0,"powiedz_gdy_jest", this_player(),
           "':ze wzburzeniem: Mam teraz pilniejsz^a spraw^e jak "+
			"wida^c!");
    }
    return "";
}

int
co_godzine()
{
    if (MT_GODZINA == 23)
    {
        set_alarm(0.1,0.0, "do_domu");
    }
}

void
do_domu()
{
    if(query_attack())
    {
        set_alarm(10.0,0.0,"do_domu");
    }
    else
    {
        tell_roombb(environment(), QCIMIE(TO,PL_MIA)+" wchodzi "+
            "do jednego z budynk^ow.\n");
        remove_object();
    }
}
