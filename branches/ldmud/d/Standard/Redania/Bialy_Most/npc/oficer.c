/*
 * Oficer z Bia�ego Mostu.
 * Long: Niejaka Heryn alias Zwierzaczek, przeredagowany i nieco zmodyfikowany
 * przez ni�ej podpisanego
 * Eventy, acty i reszt� pope�ni�: Vera
 */

inherit "/std/humanoid.c";
inherit "/lib/straz/straznik.c";

#include "dir.h"

#include <ss_types.h>
#include <macros.h>
#include <stdproperties.h>
#include <pl.h>

#define MIASTO "z Bia�ego Mostu"


#define BRON BIALY_MOST_BRONIE + "miecze/obosieczny_krotki.c"
#define ZBRO BIALY_MOST_ZBROJE + "przeszywanice/pikowana_brazowa_Mc.c"
#define BUTY BIALY_MOST_UBRANIA + "buty/czarne_zolnierskie_Mc.c"
#define SPOD BIALY_MOST_UBRANIA + "spodnie/grube_lniane_Mc.c"

void
create_humanoid()
{
	ustaw_odmiane_rasy("oficer");
	dodaj_nazwy("cz�owiek");
	set_gender(G_MALE);

	random_przym("bezwzgl�dny:bezwzgl�dni||dojrza�y:dojrzali||zielonooki:zielonoocy||starszy:starsi||"
			+"gro�ny:gro�ni||ruchliwy:ruchliwi", 2);

    set_long("Oficer, bo bez w�tpienia jest to wy�szej rangi �o�nierz, "+
	"wygl�da na bezwgl�dnego s�u�bist�. Mocno �ci�gni�te w�skie wargi "+
	"oraz wkl�s�e policzki w po��czeniu z nieco pomarszczon� twarz� ��cz� "+
	"si� w obraz cz�owieka, kt�ry wiele ju� widzia� i kt�ry stawia "+
	"dyscyplin� ponad wszystko. Prezentuje si� i�cie po �o�niersku. Dla "+
	"podw�adnych pewnie jest bezlitosny, a w stosunku do prze�o�onych "+
	"solidny. Mimo dojrza�ego wieku zdaje si� by� sprawny i gro�ny w "+
	"walce.\n");

	set_spolecznosc("Bia�y Most");
	add_prop(LIVE_I_NEVERKNOWN, 1);
	add_prop(CONT_I_WEIGHT, 75000);
	add_prop(CONT_I_HEIGHT, 175);
	add_prop(NPC_I_NO_FEAR,1);

	set_stats(({ 115+random(5), 120+random(5), 170-random(10), 100, 110}));

	add_weapon(BRON);
	add_armour(ZBRO);
	add_armour(BUTY);
	add_armour(SPOD);

	set_attack_chance(100);
	set_aggressive(&check_living());

	set_act_time(60+random(6));
	add_act("emote wodzi czujnym wzrokiem po okolicy.");
	add_act("emote poprawia sw�j or�.");
	add_act("westchnij ciezko");
	add_act("powiedz Hej! Co tu taki rozgardiasz?");
	add_act("powiedz Oj, oj, elfy si� krz�taj� ");
	add_act("prychnij");
	add_act("czas");

	set_cact_time(9+random(4));
	add_cact("krzyknij Na pohybel!");
	add_cact("krzyknij Bij!");
	add_cact("powiedz Zabij na �mier�!");
	add_cact("zmruz oczy");
	add_cact("krzyknij Do ataku!");

	add_ask(({"garnizon"}), VBFC_ME("pyt_o_garnizon"));
    add_ask(({"burmistrza"}),VBFC_ME("pyt_o_burmistrza"));
    set_default_answer(VBFC_ME("default_answer"));

    set_skill(SS_WEP_SWORD,78);
    set_skill(SS_UNARM_COMBAT, 50);
    set_skill(SS_PARRY, 66);
    set_skill(SS_DEFENCE,44);
    set_skill(SS_AWARENESS,64);
    set_skill(SS_BLIND_COMBAT,30);

	create_straznik();
}

void
hook_bija_sie_gracze()
{
	switch(random(2))
    {
		case 0: command("powiedz Przesta�cie!"); break;
		case 1: command("powiedz Natychmiast przesta�cie!"); break;
    }
}
void
hook_bija_sie()
{
    switch(random(3))
    {
        case 0: command("krzyknij Sta�! Ale to ju�!"); break;
        case 1: command("krzyknij Sta�!"); break;
		case 2: command("powiedz Natychmiast przesta�cie!"); break;
    }
}

void
hook_uciekl_poza_brame()
{
	switch(random(4))
    {
          case 0:
           command("krzyknij Wi�cej mi si� tu nie pokazuj!");
          break;
          case 1:
                command("krzyknij Jeszcze ci� dorwiemy!");
          break;
          case 2:
            command("krzyknij I obym ci� tu wi�cej nie widzia�!");
          break;
          case 3:
             command("splun");
             command("powiedz Tch�rz!");
          break;
     }
}

void
hook_kontynuujemy_akcje()
{
	switch(random(2))
    {
        case 0: command("krzyknij Bra� ich!"); break;
        case 1: command("krzyknij Oddzia�! Bra� ich!"); break;
		default: break;
    }
}

void
hook_bron_do_odebrania()
{
	if(TP->query_race() ~= "cz�owiek")
	set_alarm(0.5, 0.0, "command_present", TP,
   	       	command("powiedz do "+TP->short(TO, PL_DOP) +
	       " Bro^n b^edzie do odebrania w sklepie."));
	else
	set_alarm(0.5, 0.0, "command_present", TP,
   	       	command("powiedz do "+TP->short(TO, PL_DOP) +
	       " Bro^n b^edzie w sklepie. Heh, ale ty raczej jej nie "+
			"odzyskasz, nieludziu."));
}

string
hook_okrzyk_do_walki(object wrog)
{
	string str;
	switch(random(6))
    {
        case 0: str="powiedz La�!"; break;
        case 1: str="krzyknij Do boju!"; break;
        case 2: str="krzyknij Do ataku!"; break;
        case 3: str="krzyknij Zaraz wyprujemy ci trzewia "+
					wrog->koncowka("chamie","cholero","psie")+"!"; break;
		case 4: str="krzyknij"; break;
		case 5: str="powiedz Po�egnaj si� z �yciem!"; break;
    }
	return str;
}

void
hook_uciekl_podczas_walki(object kto)
{
	switch(random(6))
    {
        case 0: command("krzyknij W po�cig!"); break;
        case 1: command("krzyknij Za ni"+kto->koncowka("m","�","m")+
						"!"); break;
        case 2: command("krzyknij Uciek�"+kto->koncowka("","a","o")+"! Dalej!");
				 break;
		case 3: command("krzyknij Goni� "+kto->koncowka("go","j�","to")+"!"); break;
		case 4: command("krzyknij Kompania, goni� "+
					kto->koncowka("go","j�","to")+"!"); break;
		case 5: break;
        }
}

void
hook_ostrzegalem()
{
	command("':zm�czonym g�osem: Eech, �eby nie by�o, �e nie ostrzega�em!");
}

void
hook_posluszny_gracz(object kto)
{
	switch(random(4))
    {
        case 0..1:
            command("powiedz Dzi�kuj�.");
            break;
        case 2:
            command("powiedz Doskonale... i prosz� wi�cej tego robi�!");
            break;
        case 3:
            command("pokiwaj powoli");
            break;
    }
}

string
hook_opusc_bron(object kto)
{
	string str;
	switch(random(5))
    {
        case 0: str="Hej! Tak ty! Opu�� t� bro�!"; break;
        case 1..2: str="Opu^s^c bro^n!"; break;
        case 3: str="Prosz� opu�ci� bro�! Inaczej mo�e si� to �le sko�czy�."; break;
        case 4: str="Prosz� natychmiast opu�ci� bro�!"; break;
    }
	return str;
}

string
hook_zlapalismy_zlodzieja(object player)
{
	string str;
	switch(random(3))
	{
		case 0: str="Mamy ci�!";break;
		case 1: str="To "+player->koncowka("ten","ta","to")+
				"! Kompaniaaaa!"; break;
		case 2: str="Nie my�l, �e ujdzie ci to p�azem z�odziej"+
				player->koncowka("u","ko","u")+"!"; break;
	}
	return str;
}

int
query_oficer()
{
    return 1;
}

string
query_magazyn()
{
	return BIALY_MOST_LOKACJE+"magazyn_sklepikarza";
}

init_living()
{
  ::init_living();
  ::init_straznik();
}

void powiedz_gdy_jest(object player, string tekst)
{
	if (environment(this_object()) == environment(player))
        this_object()->command(tekst);
}

string pyt_o_garnizon()
{
        set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
            "powiedz do "+ this_player()->query_name(PL_DOP) +
            "Garnizon znajdziesz na wsch�d od zajazdu, zaraz przed "+
			"p�nocnym wej�ciem do miasta.");
        return "";
}

string pyt_o_burmistrza()
{
        set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
            "powiedz do "+ this_player()->query_name(PL_DOP) +" A no, "+
		"Mamy jakiego�... Nie radz� z nim zadziera�.");
        return "";
}

string default_answer()
{
		set_alarm(1.0, 0.0, "powiedz_gdy_jest", this_player(),
        "'Nie mam czasu.");
        return "";
}

void emote_hook(string emote, object wykonujacy, string przyslowek)
{
	switch (emote)
	{
		case "klepnij":
		case "poca�uj":
		case "pog�aszcz":
		case "po�askocz":
		case "przytul":
		{
			if(wykonujacy->query_gender())
				set_alarm(1.0, 0.0, "kobieta_dobre", wykonujacy);
			else
				set_alarm(1.0, 0.0, "facet_zle", wykonujacy);
			break;
		}
		case "kopnij":
		case "opluj":
		case "nadepnij":
		{
			if(wykonujacy->query_gender())
				set_alarm(1.0, 0.0, "kobieta_zle", wykonujacy);
			else
				set_alarm(1.0, 0.0, "facet_zle", wykonujacy);
			break;
		}
		case "szturchnij":
		{
			set_alarm(1.0, 0.0, "powiedz_gdy_jest", TP,
                        "powiedz do "+wykonujacy->query_name(PL_DOP)
				     +" Hmm?");
			break;
		}
	}
}

void kobieta_dobre(object wykonujacy)
{
	set_alarm(1.0, 0.0, "powiedz_gdy_jest", TP,
                        "powiedz do "+wykonujacy->query_name(PL_DOP)
			+" Hehe, tak, to mi�e...Ale jak widzisz jestem zaj�ty.");
	command("rozloz rece");
}

void kobieta_zle(object wykonujacy)
{
	command("kopnij "+wykonujacy->query_real_name(3));
	set_alarm(1.0, 0.0, "powiedz_gdy_jest", TP,
                        "powiedz do "+wykonujacy->query_name(PL_DOP)+
				" Odczep si�!");
}

void facet_zle(object wykonujacy)
{
	int  i = random(2);

	switch (i)
	{
		case 0:
		{
			command("przetrzyj oczy");
			break;
		}

		case 1:
		{
			command("kopnij "+OB_NAME(wykonujacy));
			break;
		}
	}
}

public string
query_auto_load()
{
    return 0;
}
