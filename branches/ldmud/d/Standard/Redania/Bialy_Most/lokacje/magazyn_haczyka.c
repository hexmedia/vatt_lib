#include <stdproperties.h>
#include </d/Standard/Redania/Bialy_Most/lokacje/dir.h>

inherit BIALY_MOST_STD;
inherit "/lib/store_support";

void create_bm_room()
{
    set_short("Kiesze� haczyka");
    set_long("Jeste� w kieszeni bez dna.\n");

    add_prop(ROOM_I_INSIDE,1);

    add_object(BIALY_MOST_PRZEDMIOTY+"rybackie/duzy_zelazny_haczyk.c",30);
    add_object(BIALY_MOST_PRZEDMIOTY+"rybackie/niewielki_srebrzysty_haczyk.c",30);
    add_object(BIALY_MOST_PRZEDMIOTY+"rybackie/przynetachleb.c", 30);
    add_object(BIALY_MOST_PRZEDMIOTY+"rybackie/prosta_uzywana_wedka.c", 28);
    add_object(BIALY_MOST_PRZEDMIOTY+"rybackie/dluga_jasna_wedka.c", 14);
    add_object(BIALY_MOST_PRZEDMIOTY+"rybackie/szarawa_siec_rybacka.c", 19);
    add_object(BIALY_MOST_PRZEDMIOTY+"rybackie/mala_lekka_lopatka.c",17);
    //dodajemy jako nieko�cz�ce si� te obiekty, kt�re s� niezb�dne do zarobku jako rybak
    add_neverending_object(BIALY_MOST_PRZEDMIOTY+"rybackie/przynetachleb.c");
    add_neverending_object(BIALY_MOST_PRZEDMIOTY+"rybackie/duzy_zelazny_haczyk.c");
    add_neverending_object(BIALY_MOST_PRZEDMIOTY+"rybackie/prosta_uzywana_wedka.c");
}
void
enter_inv(object ob, object skad)
{
    ::enter_inv(ob, skad);
    store_update(ob);
}
 void
 leave_inv(object ob, object to)
 {
     //::leave_env(ob,to);
     store_add_item(ob);
 }
