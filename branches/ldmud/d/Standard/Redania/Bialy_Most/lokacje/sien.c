/*
 * sie^n zajazdu w BM
 * opis by Tinardan
 * popsu^la faeve
 * noc 23/24 maja 2007 i p^o^xniej
 */

#include <stdproperties.h>
#include <mudtime.h>
#include <macros.h>
#include "dir.h"
#include <object_types.h>

inherit BIALY_MOST_STD;
inherit "/lib/peek";

string dlugi_opis();

void
create_bm_room()
{
    set_short("W sieni");
    set_long("@@dlugi_opis@@");

    add_prop(ROOM_I_INSIDE, 1);
    add_exit(BIALY_MOST_LOKACJE + "sala.c","w",0,1,0);
//   add_object("drzwi do sali");
    add_object(BIALY_MOST_LOKACJE_DRZWI + "z_zajazdu.c");
//   add_object("drzwi do stajni");

    add_object(BIALY_MOST_PRZEDMIOTY + "kominek.c");
    dodaj_rzecz_niewyswietlana("elegancki kamienny kominek");


set_event_time(250.0);
	add_event("Pod^loga nieco skrzypi pod twoim ci^e^zarem.\n");
	add_event("Z g^l^ownej sali dochodz^a ci^e ^smiechy i weso^le okrzyki. \n");
	add_event("W stajni parska jaki^s ko^n. \n");
	add_event("Z podw^orza s^lycha^c jakie^s okrzyki. \n");
	add_event("Zza uchylonych drzwi stajni dolatuje charakterystyczny zapach "+
		"ko^nskiego nawozu. \n");

}

string
dlugi_opis()

{
	string str;
	object kominek = present("elegancki kamienny kominek", TO);

	if(kominek->pali_sie() == 1)
	{
	str = "Nie jest to zbyt wielkie pomieszczenie, ale ogrom kominka mo^ze "+
		"zaskoczy^c. Nawet przy drzwiach czu^c na policzkach ciep^lo "+
		"trzaskaj^acego w nim ognia. Pr^ocz niego nie ma w tym pomieszczeniu "+
		"niemal nic - jedynie stary, ^lami^acy si^e wieszak na p^laszcze, "+
		"kt^orego i tak nikt nie u^zywa. Wida^c st^ad stoliki i "+
		"krz^ataj^acego si^e pilnie karczmarza. \n";
	}
	else
	{
	str = "Nie jest to zbyt wielkie pomieszczenie, ale ogrom kominka mo^ze "+
		"zaskoczy^c. Pr^ocz niego nie ma w tym pomieszczeniu niemal nic � "+
		"jedynie stary, ^lami^acy si^e wieszak na p^laszcze, kt^orego i tak "+
		"nikt nie u^zywa. Z prawej strony wida^c niewielkie drzwiczki "+
		"prowadz^ace do stajni, natomiast z lewej do g^l^ownej sali wiedzie "+
		"do^s^c wysoki jak na mo^zliwo^sci skromnej gospody ^luk pozbawiony "+
		"drzwi. Wida^c st^ad stoliki i krz^ataj^acego si^e pilnie karczmarza. \n";
	}
	return str;
}

public string
exits_description()
{
    return "Jedno z wyj^s^c prowadzi na zewn^atrz, drugie za^s, na zachodzie, wprost do sali "+
	"g^l^ownej zajazdu. \n";
}