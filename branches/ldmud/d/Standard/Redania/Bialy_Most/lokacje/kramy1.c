/* Autor: Avard
   Data : 6.04.07
   Opis : Tinardan */

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <pogoda.h>
#include "dir.h"

inherit ULICA_BIALY_MOST_STD;

void create_ulica() 
{
    set_short("Uliczka z kramami rybnymi");
    add_exit(BIALY_MOST_LOKACJE + "kramy2.c","w",0,1,1);
    add_exit(BIALY_MOST_LOKACJE_ULICE + "u05.c","e",0,1,1);
    add_npc(BIALY_MOST_NPC + "lotta");

    set_event_time(500.0);
    add_event("@@eventy_kramu_madafak:"+file_name(TO)+"@@");


    add_prop(ROOM_I_INSIDE,0);
}

string
eventy_kramu_madafak()
{
	if(jest_dzien())
	{
		switch(random(3))
		{
			case 0: return "Kramarze pokrzykuj� do siebie niezrozumiale.\n";
			case 1: return "Jakie� dziecko, przebiegaj�c, ochlapuje ci� b�otem.\n";
			case 2: return "Dochodzi ci^e nowa fala smrodu.\n";
		}
	}
	else
	{
		switch(random(3))
		{
			case 0..1: return "";
			case 2: return "Dochodzi ci^e nowa fala smrodu.\n";
		}
	}
}

string
dlugi_opis()
{
    string str;
    str = "Zaiste, miejsce to ponure i nieciekawe. B�oto, rozchlapywane "+
    "tysi�cem st�p ma nieprzyjemny, zgni�ozielony kolor i zalega tu, "+
    "jak mo�e si� wydawa�, niezale�nie od pory roku";
    
    if(pora_roku() == MT_ZIMA)
    str+=" - nawet �nieg nie da� rady pobieli� tego skrawka uliczki";
    
    str+=". �ciany dom�w po obu stronach s� wysokie i szare, pozbawione okien "+
    "i drzwi, jakby ca�y �wiat chcia� zapomnie� o tym miejscu. I najgorsze "+
    "- wsz�dobylski, lepki smr�d. Przemieszany od�r gnij�cych i �wie�ych "+
    "ryb, ludzkiego potu, b�ota i rynsztok�w tworzy w powietrzu niemal "+
    "namacalne opary. Powietrze stoi, nieruchome, dusz�ce. Smr�d lepi "+
    "si� do wszystkiego - pokrywa w�osy i ubrania, osiada na towarach, na "+
    "twarzach, d�oniach i stopach. Wsz�dzie pe�no ludzi, ale wszystko "+
    "wydaje si� by� szare i pokryte kleistym osadem. Nawet niebo, odleg�e "+
    "i ledwie widoczne, przes�oni�te jest cuchn�cymi oparami niczym mg��. ";
    
    
    if(jest_rzecz_w_sublokacji(0, "Lotta") ||
        jest_rzecz_w_sublokacji(0, "ospa�a brudna kobieta"))
    {
        str += "Handlarze ryb pokrzykuj^a do siebie, dzieci biegaj^a, a "+
            "wok^o^l czaj^a si^e ca^le chmary bezdomnych kot^ow, chudych, "+
            "jednookich obdartus^ow o z^lowrogim wyrazie ponurych "+
            "pyszczk^ow. ";
    }
    str += "Nawet niebo wydaje si^e inne - ";
    if(jest_dzien() == 1)
    {
        str += "szarawe, ";
    }
    else
    {
        str += "szarogranatowe, ";
    }
    str += "przes^loni^ete wszechobecnym smrodem, przydymione i odleg^le.";
    
    str+="\n";
    return str;
}

string
opis_nocy()
{
    string str;
    str = "Wydawa^c si^e mo^ze, ze to miejsce, ciemne i nieprzyjemne, "+
        "wygl^ada podobnie o ka^zdej porze roku. Lepkie, cuchn^ace "+
        "b^loto pokrywaj^ace nier^owne kamienie bruku, szare ^sciany "+
        "dom^ow, kt^ore do tej uliczki zdaj^a si^e odwraca^c plecami � "+
        "^zadnych okien, drzwi czy furtek. I najgorsze � wszechobecny "+
        "smr^od. Przemieszany od^or ^swie^zych i gnij^acych ryb, ludzkiego "+
        "potu, brudu i szlag wie czego jeszcze, zdaje si^e tworzy^c lepk^a, "+
        "niemal namacaln^a zawiesin^e unosz^ac^a si^e nad ulic^a, lepi^ac^a "+
        "si^e do ubra^n, okrywaj^ac^a ka^zd^a woln^a powierzchni^e. ";
    if(jest_rzecz_w_sublokacji(0, "Lotta") ||
        jest_rzecz_w_sublokacji(0, "ospa�a brudna kobieta"))
    {
        str += "Handlarze ryb pokrzykuj^a do siebie, dzieci biegaj^a, a "+
            "wok^o^l czaj^a si^e ca^le chmary bezdomnych kot^ow, chudych, "+
            "jednookich obdartus^ow o z^lowrogim wyrazie ponurych "+
            "pyszczk^ow. ";
    }
    str += "Nawet niebo wydaje si^e inne � ";
    if(jest_dzien() == 1)
    {
        str += "szarawe, ";
    }
    else
    {
        str += "szarogranatowe, ";
    }
    str += "przes^loni^ete wszechobecnym smrodem, przydymione i odleg^le.";
    str+="\n";
    return str;
}

/*

Kram z rybami
Long: Na d�ugim stole ryby walaj� si� w nie�adzie. Mieszaj� si� klenie z marnymi szczupakami, �wie�e z gnij�cymi. Ponad nimi unosz� si� roje opas�ych much i leki, dusz�cy smr�d. Niekt�re ryby jeszcze oddychaj�, ich skrzela pracuj� nerwowym ruchem, drgaj� p�etwy. W osobnym kuble le�� ryby ju� patroszone i przebrane, oczekuj�ce na tych ?lepszych? klient�w, kt�rzy nie posk�pi� grosza. Przy kramie opiera si� o �cian� m�oda, szarooka dziewczyna o bezmy�lnym wyrazie twarzy, ospa�a i znudzona, o�ywiaj�ca si� tylko w momencie, gdy kto� zbli�y si� do jej kramu.

*/










