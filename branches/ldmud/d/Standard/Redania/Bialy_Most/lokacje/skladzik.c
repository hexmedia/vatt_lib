/* Autor: Duana
  Opis : sniegulak
  Data : 23.05.2007
*/

#include "dir.h"

inherit BIALY_MOST_STD;

#include <macros.h>
#include <stdproperties.h>
#include <object_types.h>
#include <filter_funs.h>
#include <ss_types.h>
#include <wa_types.h>
#include <cmdparse.h>
#include <pl.h>
#include <options.h>
#include <formulas.h>

#define SUBLOC_SCIANA ({"^sciana", "na"})

void
create_bm_room()
{
    set_short("Sk^ladzik");
    set_long("Bardzo ma^le pomieszczenie prawie w ca^lo^sci przykrywa kurz. "+
        "Brudne zas^loni^ete ciemnymi kotarami okno, nie wpuszcza tu zbyt "+
        "wiele ^swiat^la, tak wi^ec wieczorami i noc^a trzeba sobie "+
        "przy^swieca^c samemu.@@narzedzia@@"+
        "@@opis_sublokacji|Na p^o^lnocnej ^scianie wisi |^sciana|.@@ "+
        "Po przeciwleg^lej stronie znajduj� si� drzwi prowadz�ce do innego "+
        "pomieszczenia. Najprawdopodobniej w^la^sciciel niezbyt cz^esto "+
        "korzysta z tego miejsca gdy^z inaczej, by^loby tu bardziej "+
        "wysprz^atane.\n");

    add_prop(ROOM_I_INSIDE,1);
    add_prop(ROOM_I_LIGHT,2);

    add_subloc(SUBLOC_SCIANA);
    add_subloc_prop("^sciana", CONT_I_CANT_ODLOZ, 1);
    add_subloc_prop("^sciana", CONT_I_CANT_POLOZ, 1);
    add_subloc_prop("^sciana", SUBLOC_I_MOZNA_POWIES, 1);
    add_subloc_prop("^sciana", SUBLOC_I_DLA_O, O_SCIENNE);
    add_subloc_prop("^sciana", CONT_I_MAX_RZECZY, 1);

    add_object(BIALY_MOST_PRZEDMIOTY + "polka.c", 1, 0, "^sciana");
    dodaj_rzecz_niewyswietlana("solidna drewniana p^o^lka", 1);

    add_object(BIALY_MOST_PRZEDMIOTY + "kopaczka_prosta_mala.c");
    dodaj_rzecz_niewyswietlana("prosta ma^la kopaczka", 1);

    add_object(BIALY_MOST_PRZEDMIOTY + "lopata_prosta_dluga.c");
    dodaj_rzecz_niewyswietlana("prosta d^luga ^lopata", 1);

    add_object(BIALY_MOST_PRZEDMIOTY + "wiadro.c");
    dodaj_rzecz_niewyswietlana("drewniane solidne wiadro", 1);

    add_object(BIALY_MOST_LOKACJE_DRZWI +"ze_skladziku.c");

    add_item("okno","Niewielkich rozmiar^ow okno jest szczelnie "+
        "zas^loni^ete brudnymi kotarami, tak ^ze nic przez nie nie "+
        "wida^c.\n");

}

string
narzedzia()
{
  string str="";
  if(jest_rzecz_w_sublokacji(0,"prosta ma^la kopaczka")&&
jest_rzecz_w_sublokacji(0,"prosta d^luga ^lopata") &&
jest_rzecz_w_sublokacji(0,"drewniane solidne wiadro"))
 str=" W rogu stoj^a oparte o ^scian^e nadz^edzia. Wida^c prost^a ma^l^a kopaczk^e, "+
  "prost^a d^lug^a ^lopat^e oraz drewniane solidne wiadro. ";
else if(jest_rzecz_w_sublokacji(0,"prosta ma^la kopaczka") &&
jest_rzecz_w_sublokacji(0,"prosta d^luga ^lopata"))
	str=" W rogu stoj^a oparte o ^scian^e nadz^edzia. Wida^c prost^a ma^l^a kopaczk^e "+
  "i prost^a d^lug^a ^lopat^e. ";
else if(jest_rzecz_w_sublokacji(0,"prosta ma^la kopaczka") &&
jest_rzecz_w_sublokacji(0,"drewniane solidne wiadro"))
	str=" W rogu stoj^a oparte o ^scian^e nadz^edzia. Wida^c prost^a ma^l^a kopaczk^e "+
  "i drewniane solidne wiadro. ";
else if(jest_rzecz_w_sublokacji(0,"prosta d^luga ^lopata") &&
jest_rzecz_w_sublokacji(0,"drewniane solidne wiadro"))
	str=" W rogu stoj^a oparte o ^scian^e nadz^edzia. Wida^c prost^a d^lug^a ^lopat^e "+
  "i drewniane solidne wiadro. ";
else if(jest_rzecz_w_sublokacji(0,"prosta ma^la kopaczka"))
	str=" W rogu wida^c opart^a o ^scian^e prost^a ma^l^a kopaczk^e. ";
else if(jest_rzecz_w_sublokacji(0,"prosta d^luga ^lopata"))
	str=" W rogu wida^c opart^a o ^scian^e prost^a d^lug^a ^lopat^e";
else if(jest_rzecz_w_sublokacji(0,"drewniane solidne wiadro"))
	str=" W rogu stoi drewniane solidne wiadro. ";

    return str;
}

public string
exits_description()
{
    return "Jedyne wyj^scie st^ad prowadzi przez drzwi do sali g^l^ownej. \n";
}