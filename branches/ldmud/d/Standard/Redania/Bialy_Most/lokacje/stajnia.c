/*
 * Made by Eleb.
 * doko^nczy^la faeve
 * dn. 5.06.07
 */
#include "dir.h"

inherit BIALY_MOST_STD;
inherit "/lib/drink_water";
#include <object_types.h>
#include <pl.h>
#include <macros.h>
#include <stdproperties.h>
string dlugi_opis();

void create_bm_room()
{
    set_short("Stajnia");
    set_long("@@dlugi_opis@@\n");

    //add_npc("stajenny.c");

	/*drzwi*/
	add_object(BIALY_MOST_LOKACJE_DRZWI + "ze_stajni.c");
    /*obiekty*/
    add_object(BIALY_MOST_PRZEDMIOTY + "siano.c");
    add_object(BIALY_MOST_PRZEDMIOTY + "stolek.c");
    dodaj_rzecz_niewyswietlana("siano");
	dodaj_rzecz_niewyswietlana("masywny sto^lek");

    /* drzwi(Jedyne wyjscie)*/
    //itemy
    //boksy, siodla,strzemiona itp. Konie?

    /*propy*/
    add_prop(ROOM_I_INSIDE, 1);

    /*Eventy*/
//	set_event_time
    //szczur, kotek i cos tam jeszcze

}
string dlugi_opis(){
	// rozwinac o jest_ stolek i ten stajenny! i konie!
	string str = "W pomieszczeniu panuje mrok ^swiat^lo wpada tu jedynie przez niewielkie" +
        	     " okienka tu^z przy stropie i uchylone wrota, jednak nawet w tych nik^lych";

        if(jest_dzien()==1){
		str = str + " s^lonecznych ";
	}else{
		str = str + " ksi^e^zycowych ";
	}
	str = str + " promieniach mo^zna dojrze^c unosz^ace si^e wsz^edzie "+
		"drobinki kurzu i py^lu. Na mocno ubitym ziemnym klepisku wsz^edzie "+
		"porozrzucana jest s^loma - wr^ecz wysypuje si^e ona z boks^ow, "+
		"niewielkich, ale czystych, zamykanych na skobel. S^lom^e zebrano "+
		"pod przeciwleg^la ^scian^a i usypano z niej spor^a g^ork^e. ";

	if(jest_rzecz_w_sublokacji(0, "stajenny") ||
            jest_rzecz_w_sublokacji(0, "barszysty ponury m^e^zczyzna"))
	{
		str += "Tu^z pod ni^a, na niskim sto^lku, siedzi stajenny, pochylony "+
		"nad robot^a. Wok^o^l niego roz^lo^zone s^a siod^la, strzemiona i "+
		"pu^sliska wymagaj^ace naprawy. ";
	}
	else
	{
		str += "Na ziemi roz^lo^zone s^a wymagaj^ace naprawy siod^la, strzemiona "+
		"i pu^sliska. ";
	}

	if(jest_rzecz_w_sublokacji(0, "ko^n") ||
            jest_rzecz_w_sublokacji(0, "konie"))
	{
		str += "W boksach stoj^a konie - niekt^ore prawdziwie pi^ekne i "+
		"zadbane, nale^z^ace zapewne do przyjezdnych, inne po^sledniejszych "+
		"ras, niekt^ore nawet zas^luguj^a na miano chabet. Zwierz^eta "+
		"parskaj^a cicho, ich ^z^loby s^a pe^lne";
	}
	else
	{
		str += "^Z^loby s^a pe^lne";
	}

	str += " - wi^ekszo^s^c koni zadowala si^e spor^a ilo^sci^a siana i "+
		"odrobin^a owsa, na tyle, by nie opa^s^c z si^l, ale cz^e^s^c "+
		"zosta^la obdarowana posi^lkiem i^scie kr^olewskim. Wzd^lu^z "+
		"boks^ow, od strony ^sciany przymocowane jest tak^ze koryto pe^lne "+
		"wody.";
	return str;

}
exits_description()
{
    return  "Jedyne wyj^scie st^ad prowadzi przez wrota na podw^orze. \n";
}


