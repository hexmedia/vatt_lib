/*
 * drzwi do garnizonu BM
 * opis by faeve
 * popsu^la ta^z sama
 * dn. 25 maja 2005
 */

inherit "/std/door";

#include <pl.h>
#include "dir.h"

void
create_door()
{
    ustaw_nazwe("drzwi");
    dodaj_przym("prosty","pro^sci");
    dodaj_przym("okuty", "okuci");

    set_other_room(BIALY_MOST_LOKACJE + "gabinet_w_garnizonie.c");
    set_door_id("DRZWI_GARNIZONU_BM");
    set_door_desc("Proste drzwi wykonane ze ^sredniej jako^sci drewna, "+
        "dobrze oheblowane, ale pozbawione zdobie^n. Jedynie w rogach "+
        "wzmocnione mosi^e^znym okuciem, r^ownie^z mosi^e^zna klamka "+
        "utrzymana zosta^la w podonym, prostym stylu. Ca^lo^s^c mo^ze nie "+
        "jest specjalnie elegancka, ale wygl^ada na porz^adne i trwa^le "+
        "zabezpieczenie przed ewentualnym w^lamaniem - jak to przysta^lo "+
        "na stra^z miejsk^a. \n");

    set_open_desc("");
    set_closed_desc("");

    set_pass_command(({"garnizon","drzwi"}),"przez proste okute drzwi",
        "z zewn^atrz");

    set_open(0);
    set_locked(0);

    set_key("KLUCZ_DRZWI_GARNIZONU_BM");
    set_lock_name(({"zamek", "zamku", "zamkowi", "zamek", "zamkiem", "zamku"}), 0, PL_MESKI_NOS_NZYW);
}