/*
 * drzwi do lazni w BM
 * opis by Sniegulak
 * popsu^la faeve
 * dn. 21 maja 2005
 */

inherit "/std/door";

#include <pl.h>
#include "dir.h"

void
create_door()
{
    ustaw_nazwe("drzwi");
    dodaj_przym("zwyk^ly","zwykli");
    dodaj_przym("drewniany", "drewniani");

    set_other_room(BIALY_MOST_LOKACJE + "podworze");

    set_door_id("DRZWI_DO_ZAJAZDU_BM");

    set_door_desc("S^a to do^s^c pot^e^zne jednoskrzyd^lowe drzwi wykonane z "+
        "jakiego^s do^s^c dobrego gatunku drewna, wcale porz^adnie "+
        "wyheblowane, poci^agni^ete jedynie jakim^s ^srodkiem, maj^acym "+
        "zakonserwowa^c drewno i zapobiec jego niszczeniu. Drzwi zaopatrzone "+
        "s^a w solidn^a mosi^e^zn^a klamk^e. \n");

    set_open_desc("");
    set_closed_desc("");

    set_pass_command(({"drzwi","wyj^scie"}),"przez zwyk^le drewniane drzwi",
        "pod^a^za na zewn^atrz.");

    set_open(0);
    set_locked(0);

    set_key("KLUCZ_DRZWI_WEJSCIOWE_DO_KARCZMY_BIALY_MOST");
    set_lock_name(({"zamek", "zamku", "zamkowi", "zamek", "zamkiem", "zamku"}), 0, PL_MESKI_NOS_NZYW);
}