/**
 *Made by Eleb.
 */

inherit "/std/door";
#include <pl.h>
#include "dir.h"

void create_door()
{
    ustaw_nazwe("drzwi");
    dodaj_przym("drewniany","drewniani");

    set_door_desc("Masz przed sob� zwyk�e drewniane drzwi. Zamykane s� na "+
        "zamek. Nie posiadaj� �adnych ozd�b.\n");
    set_door_id("DRZWI_DO_SYPIALNI_BURMISTRZA_BIALEG_MOSTU");

    set_key("KLUCZ_DRZWI_DO_SYPIALNI_BURMISTRZA_BIALEG_MOSTU");

    set_lock_mess("przekr�ca klucz w zamku.\n", "Przekr�casz klucz w zamku.\n", "Slyszysz jaki� szcz�k zamka, "+
        "jak by kto� przekr�ca� w nim klucz.\n");
    set_unlock_mess("przekr�ca klucz w zamku.\n", "Przekr�casz klucz w zamku.\n",
        "Slyszysz jaki� szcz�k zamka, jak by kto� przekr�ca� w nim klucz.\n");

    set_open_desc("");
    set_closed_desc("");
    set_open(0);
    set_locked(1);

    set_other_room(BIALY_MOST_LOKACJE + "sypialnia_burmistrza.c");
    set_pass_command(({"drzwi","sypialnia"}),"przez drewniane drzwi",
        "z sypialni");

    set_lock_name(({"zamek od drzwi", "zamku od drzwi", "zamkowi od drzwi", "zamek od drzwi",
        "zamkiem od drzwi", "zamku od drzwi"}), 0, PL_MESKI_NOS_NZYW);
    set_lock_desc("Sporej wielko�ci zamek. Ani ozdobny ani sk�plikowany.\n");
}
