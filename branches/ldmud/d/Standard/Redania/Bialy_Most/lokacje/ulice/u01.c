/*
 *Ulica Bia�ego Mostu
 *Vera
 */

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"
#include <pogoda.h>

inherit ULICA_BIALY_MOST_STD;


string
evencik()
{
	switch(random(5))
	{
		case 0:
			return "Kto� wyla� wod� w okna kamienicy.\n"; break;
		case 1:
			if(jest_dzien())
			return "Dwa jad�ce z naprzeciwka wozy zaczepi�y o siebie co "+
					"zako�czy�o si� k��tni� wo�nic�w.\n";
			else
			return "Z bocznego zau�ka dochodzi bojowe zawodzenie "+
					"walcz�cych kot�w.\n";
			break;
		case 2:
			if(jest_dzien())
			return "Banda umorusanych dzieciak�w zwinnie przemyka "+
					"pomi�dzy wozami.\n";
			else
			return "Kto� przemkn�� na drug� stron� uliczki.\n";
			break;
		case 3:
			if(jest_dzien())
			return "Pomi�dzy wozami dostrzegasz prowadzone gdzie� stadko k�z.\n";
			else
			return "Z dachu zsun�a si� dach�wka i z �oskotem rozbi�a na ulicy.\n";
			break;
		case 4:
			if(jest_dzien())
			return "Za�adowany po brzegi w�z wjecha� do miasteczka.\n";
			else
			return "Ze wschodu s�ycha� jakie� krzyki.\n";
			break;
	}

}


void create_ulica() 
{
    set_short("Przy wschodniej granicy miasteczka");
    set_long("@@dlugasny@@");
	add_exit(BIALY_MOST_LOKACJE_ULICE + "u09.c","nw",0,1,0);
	add_exit(BIALY_MOST_LOKACJE_ULICE + "u02.c","sw",0,1,0);
	add_exit(TRAKT_BIALY_MOST_OKOLICE_LOKACJE + "t01.c","e",0,8,0);

	add_item(({"sklep","sklepy","kram","kramy"}),
			"Wszelkiej ma�ci budki, kramy, sklepiki i stragany "+
			"poutykane s� wsz�dzie gdzie znalaz� si� cho� s��e� "+
			"miejsca. Zbiegaj�ce si� w tym mie�cie szlaki kupieckie "+
			"zasilaj� je towarami z takich miast jak Wyzima czy Novigrad. "+
			"Jak zwykle przy nat�oku d�br, trzeba sporo si� naszuka� "+
			"by znale�� co� konkretnego.\n");

	set_event_time(340.0);
	add_event("@@evencik:"+file_name(TO)+"@@");
    add_prop(ROOM_I_INSIDE,0);
	add_object(BIALY_MOST_PRZEDMIOTY+"lampa_uliczna");
	add_object(BIALY_MOST_PRZEDMIOTY+"stragan_hacz");

	add_npc(BIALY_MOST_NPC + "haczykowy.c");


}
public string
exits_description() 
{
    return "Ulice prowadz� na p�nocny-zach�d oraz na po�udniowy-zachod, za� "+
			"trakt wiedzie na wsch�d.\n";
}


string
dlugasny()
{
	string str="";


	if(jest_dzien())
	str+="W tym miejscu zbiegaj� si� dwie g��wne ulice miasta oraz "+
			"trakt wiod�cy na wsch�d. Wozy zapakowane przer�nymi "+
		"towarami regularnie przebywaj� j� w obie strony. Schludne "+
		"kamieniczki otaczaj�ce placyk dowodz� maj�tno�ci mieszka�c�w, "+
		"za� w�a�ciciele sklep�w i kram�w uwijaj� si� w�r�d nat�oku. "+
		"Nawet blaszane lampy umieszczone regularnie na �cianach s� "+
		"nowe i nie�le wykonane.";
	else
	str+="W tym miejscu zbiegaj� si� dwie g��wne ulice miasta oraz "+
		"trakt wiod�cy na wsch�d. Schludne kamieniczki otaczaj�ce "+
		"placyk dowodz� maj�tno�ci mieszka�c�w. Liczne sklepy i kramy "+
		"s� o tej porze puste i ciche. Blaszane lampy umieszczone "+
		"regularnie na �cianach roz�wietlaj� mroki nocnych godzin.";



	str+="\n";
	return str;
}

public void
enter_inv(object ob, object from)
{
    ::enter_inv(ob, from);

    if (interactive(ob) && !ob->query_wiz_level()) {
        ob->set_default_start_location(BIALY_MOST_LOKACJE+"strych.c");
    }
}
