/*
 *Ulica Bia�ego Mostu
 *Vera
 */

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"
#include <pogoda.h>

inherit ULICA_BIALY_MOST_STD;

string
evencik()
{
	switch(random(5))
	{
		case 0:
			return "Kto� wyla� wod� w okna kamienicy.\n";
		case 1:
			if(jest_dzien())
			return "Dwa jad�ce z naprzeciwka wozy zaczepi�y o siebie co "+
					"zako�czy�o si� k��tni� wo�nic�w.\n";
			else
			return "Z bocznego zau�ka dochodzi bojowe zawodzenie "+
					"walcz�cych kot�w.\n";
		case 2:
			if(jest_dzien())
			return "Banda umorusanych dzieciak�w zwinnie przemyka "+
					"pomi�dzy wozami.\n";
			else
			return "Kto� przemkn�� na drug� stron� uliczki.\n";
		case 3:
			if(jest_dzien())
			return "Pomi�dzy wozami dostrzegasz prowadzone gdzie� stadko k�z.\n";
			else
			return "Z dachu zsun�a si� dach�wka i z �oskotem rozbi�a na ulicy.\n";
		case 4:
			return "Szyld skrzypi na wietrze.\n";
	}

}

void create_ulica() 
{
    set_short("W�ska uliczka przy zak�adzie snycerskim");
    set_long("@@dlugasny@@");
	add_exit(BIALY_MOST_LOKACJE_ULICE + "u08.c","w",0,1,0);
	add_exit(BIALY_MOST_LOKACJE_ULICE + "u01.c","se",0,1,0);
	add_object(BIALY_MOST_LOKACJE_DRZWI +"do_zakladu.c");

	add_item(({"zak�ad","szyld","budynek","zak�ad snycerski"}),
		"Niewielki, parterowy budynek z szarego kamienia wci�ni�ty "+
		"mi�dzy dwie kamienice ozdobiony jest szyldem "+
		"przedstawiaj�cym ko�lawy kawa�ek sk�ry oraz napisz "+
		"g�osz�cy: Zak�ad snycerski.\n");


    add_prop(ROOM_I_INSIDE,0);
	add_object(BIALY_MOST_PRZEDMIOTY+"lampa_uliczna.c");
}
public string
exits_description() 
{
    return "Ulica ci�gnie si� z zachodu na po�udniowy-wsch�d. S� tu tak�e "+
	"drzwi prowadz�ce do zak�adu.\n";
}


string
dlugasny()
{
	string str="";

	if(jest_dzien())
	str+="W�ska uliczka daje nieco wytchnienia przed t�okiem "+
		"g��wnych trakt�w, czy plac�w. Schludne kamieniczki ci�gn�ce "+
	"si� z obu stron dowodz� maj�tno�ci mieszka�c�w, a pomi�dzy "+
	"nimi wci�ni�ty jest ma�y murowany budynek z szyldem. Blaszane "+
	"lampy umieszczone gdzieniegdzie na �cianach s� nowe i nie�le "+
	"wykonane.";
	else
	str+="W�ska uliczka wydaje si� niemal wymar�a. Schludne "+
	"kamieniczki ci�gn�ce si� z obu stron dowodz� maj�tno�ci mieszka�c�w, "+
	"a pomi�dzy nimi wci�ni�ty jest ma�y murowany budynek z szyldem. "+
	"Blaszane lampy umieszczone gdzieniegdzie na �cianach roz�wietlaj� "+
	"mroki nocnych godzin.";

	str+="\n";
	return str;
}
