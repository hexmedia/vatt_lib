/*
 *Ulica Bia�ego Mostu
 *Vera
 */

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"
#include <pogoda.h>

inherit ULICA_BIALY_MOST_STD;

void create_ulica() 
{
    set_short("Przy p�nocnej rubie�y miasteczka");
    set_long("@@dlugasny@@");
	add_exit(BIALY_MOST_LOKACJE_ULICE + "u07.c","sw",0,1,0);
	add_exit(BIALY_MOST_LOKACJE_ULICE + "u08.c","se",0,1,0);
    add_exit(TRAKT_BIALY_MOST_OKOLICE_LOKACJE + "t25.c","n",0,8,0);
    add_object(BIALY_MOST_LOKACJE_DRZWI +"do_garnizonu.c");
//garnizon

	add_item(({"budynek","garnizon"}),
		"Szerokie wrota z niewielk� furtk� oraz umieszczone "+
		"wysoko, ma�e okienka bardziej przypominaj�ce strzelnice s� "+
		"niemal wszystkim co wyr�nia si� na prostej, wymurowanej z "+
		"czerwonej ceg�y �cianie. Ostatnim godnym uwagi detalem jest "+
		"za�niedzia�a, mosi�na tabliczka.\n");
	add_item("tabliczk�",
 	"  ______________________________\n"+
	" /                              \\ \n"+
	" | BUDYNEK STRA�Y MIEJSKIEJ     | \n"+
	//"|                                                      |"+
	//"| nieupowa�nionym wst�p zabroniony |"+
	" \\______________________________/ \n"+
	"\n");
    add_prop(ROOM_I_INSIDE,0);
	add_object(BIALY_MOST_PRZEDMIOTY+"lampa_uliczna.c");
}
public string
exits_description() 
{
    return "Ulica wiedzie z po�udniowego-zachodu na po�udniowy-wsch�d. "+
	"Jest tu tak�e trakt na p�noc, oraz drzwi do budynku garnizonu.\n";
}


string
dlugasny()
{
	string str;
	if(jest_dzien())
    {
        str="To miejscu gdzie zbiegaj� si� dwie spore ulice oraz trakt "+
	    "wiod�cy na p�noc. Wozy zapakowane przer�nymi towarami regularnie "+
	    "przebywaj� j� w obie strony. Schludne kamieniczki otaczaj�ce "+
    	"placyk dowodz� maj�tno�ci mieszka�c�w. Pomi�dzy nimi wyr�nia si� "+
    	"niema�ymi rozmiarami ceglany budynek. Nawet blaszane lampy "+
    	"umieszczone regularnie na �cianach s� nowe i nie�le wykonane.";
    }
	else
    {
	    str="W tym miejscu zbiegaj� si� dwie spore ulice oraz trakt wiod�cy "+
	    "na p�noc. Schludne kamieniczki otaczaj�ce placyk dowodz� maj�tno�ci "+
	    "mieszka�c�w. Pomi�dzy nimi wyr�nia si� niema�ymi rozmiarami "+
	    "mroczny, ceglany budynek. Blaszane lampy umieszczone regularnie na "+
	    "�cianach roz�wietlaj� mroki nocnych godzin.";
    }
	str+="\n";
	return str;
}

public void
enter_inv(object ob, object from)
{
    ::enter_inv(ob, from);

    if (interactive(ob) && !ob->query_wiz_level()) {
        ob->set_default_start_location(BIALY_MOST_LOKACJE+"strych.c");
    }
}
