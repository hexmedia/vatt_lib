/*
 *Ulica Bia�ego Mostu
 *Vera
 */

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"
#include <pogoda.h>

inherit ULICA_BIALY_MOST_STD;


string
evencik()
{
	switch(random(5))
	{
		case 0:
			return "Kto� wyla� wod� w okna kamienicy.\n"; break;
		case 1:
			if(jest_dzien())
			return "Dwa jad�ce z naprzeciwka wozy zaczepi�y o siebie co "+
					"zako�czy�o si� k��tni� wo�nic�w.\n";
			else
			return "Z bocznego zau�ka dochodzi bojowe zawodzenie "+
					"walcz�cych kot�w.\n";
			break;
		case 2:
			if(jest_dzien())
			return "Banda umorusanych dzieciak�w zwinnie przemyka "+
					"pomi�dzy wozami.\n";
			else
			return "Kto� przemkn�� na drug� stron� uliczki.\n";
			break;
		case 3:
			if(jest_dzien())
			return "Pomi�dzy wozami dostrzegasz prowadzone gdzie� stadko k�z.\n";
			else
			return "Z dachu zsun�a si� dach�wka i z �oskotem rozbi�a na ulicy.\n";
			break;
		case 4:
			if(jest_dzien())
			return "Do najwi�kszej kamienicy wesz�a grupka kupc�w.\n";
			break;
	}

}

void create_ulica() 
{
    set_short("Przed domem burmistrza");
    set_long("@@dlugasny@@");
	add_exit(BIALY_MOST_LOKACJE_ULICE + "u07.c","nw",0,1,0);
	add_exit(BIALY_MOST_LOKACJE_ULICE + "u08.c","ne",0,1,0);
	add_exit(BIALY_MOST_LOKACJE_ULICE + "u05.c","sw",0,1,0);
	add_exit(BIALY_MOST_LOKACJE_ULICE + "u03.c","se",0,1,0);
    add_object(BIALY_MOST_LOKACJE_DRZWI + "do_burmistrza");
//burmistrz
    add_object(BIALY_MOST_PRZEDMIOTY + "drogowskaz-piana.c");

	add_item(({"sklep","sklepy","kram","kramy"}),
			"Wszelkiej ma�ci budki, kramy, sklepiki i stragany "+
			"poutykane s� wsz�dzie gdzie znalaz� si� cho� s��e� "+
			"miejsca. Zbiegaj�ce si� w tym mie�cie szlaki kupieckie "+
			"zasilaj� je towarami z takich miast jak Wyzima czy Novigrad. "+
			"Jak zwykle przy nat�oku d�br, trzeba sporo si� naszuka� "+
			"by znale�� co� konkretnego.\n");
	add_item("kamienic�",
		"Pyszny budynek z daleka przykuwa wzrok. Kunsztownie zdobiony "+
		"front ozdobiony jest p�askorze�bami wyobra�aj�cymi procesje "+
		"ludzi i nieludzi, za� w centralnym miejscu wywieszono du�� tarcz� z herbem "+
		"Redanii, srebrnym or�em w czerwonym polu. Marmurowe stopnie "+
		"wiod� do drzwi, podobnie jak okiennice wykonanych z cedrowego "+
		"drewna i ciesz�cych oko misternymi, mosi�nymi okuciami.\n");

	set_event_time(340.0);
	add_event("@@evencik:"+file_name(TO)+"@@");

    add_prop(ROOM_I_INSIDE,0);
	add_object(BIALY_MOST_PRZEDMIOTY+"lampa_uliczna.c");
        add_object(BIALY_MOST_PRZEDMIOTY+"przystanek.c");
}
public string
exits_description() 
{
    return "Ulice wiod�ce z p�nocnego-zachodu na po�udniowy-wsch�d oraz z "+
	"p�nocnego-wschodu na po�udniowy-zach�d zbiegaj� si� tu, tworz�c to skrzy�owanie."+
	" S� tu tak�e drzwi do kamienicy.\n";
}


string
dlugasny()
{
	string str="";

	if(jest_dzien())
	str+="W tym miejscu szeroka ulica krzy�uje si� z "+
		"drug� mniejsz�. Wozy zapakowane przer�nymi towarami "+
	"regularnie mijaj� ci� poruszaj�c si� w obie strony. Schludne "+
	"kamieniczki otaczaj�ce placyk dowodz� maj�tno�ci mieszka�c�w. "+
	"Spo�r�d budynk�w wyr�nia si� jedna szczeg�lnie bogata, a liczne "+
	"dooko�a kramy i sklepy nie zas�aniaj� jej frontu. "+
        "Obok niej z udeptanej ziemi wyrasta ma�a tabliczka. Nawet blaszane "+
	"lampy umieszczone regularnie na �cianach s� nowe i nie�le "+
	"wykonane.";
	else
	str+="W tym miejscu szeroka ulica krzy�uje si� z drug� mniejsz�. "+
	"Obie s� ciche i spokojne o tej porze. Bogate kamieniczki otaczaj�ce "+
	"placyk dowodz� maj�tno�ci mieszka�c�w. Spo�r�d budynk�w wyr�nia "+
	"si� jedna szczeg�lnie wspania�a, a liczne dooko�a kramy i sklepy nie "+
	"zas�aniaj� jej frontu. Obok niej z udeptanej ziemi wyrasta ma�a "+
        "tabliczka. Blaszane lampy umieszczone regularnie na "+
	"�cianach roz�wietlaj� mroki nocnych godzin.";


	str+="\n";
	return str;
}
