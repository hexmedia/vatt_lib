/*

DRZWI

 * areszt
 * Bia^ly Most
 * opis by Khaes
 * zepsu^la faeve
 * 22 maja 2007
 *
 */

#include <stdproperties.h>
#include <mudtime.h>
#include <macros.h>
#include "dir.h"

inherit BIALY_MOST_STD;
inherit "/lib/peek";

#define JEST(x) jest_rzecz_w_sublokacji(0, (x)->short())

string dlugi_opis();

void
create_bm_room()
{
    set_short("W areszcie");
    set_long("@@dlugi_opis@@");

    add_prop(ROOM_I_INSIDE, 1);

    add_object(BIALY_MOST_LOKACJE_DRZWI +"z_aresztu.c");

    add_object(BIALY_MOST_PRZEDMIOTY + "waski_drewniany_stolik.c");
    dodaj_rzecz_niewyswietlana("w^aski drewniany stolik");

	add_object(BIALY_MOST_PRZEDMIOTY + "lawa_areszt.c");
    dodaj_rzecz_niewyswietlana("^lawa");


 //   add_peek("przez okienko", "/d/Standard/Redania/Rinde/Centrum/lokacje/placs.c");
 //   add_peek("przez okno", "/d/Standard/Redania/Rinde/Centrum/lokacje/placs.c");

 //eventy?
 add_item(({"rysunki","rysy"}),"Wyryte niestarannie w kamieniu przer^o^zne obrazki czy "+
	 "te^z symbole, b^ed^ace jednak najcz^e^sciej nic nieznacz^acymi "+
	 "gryzmo^lami. \n");
 add_item("mis^e","Odrapana, metalowa misa o okr^ag^lym kszta^lcie "+
	 "wype^lniona ekskrementami. \n");
}

string
dlugi_opis()
{
    string str;

    str = "W powietrzu unosi si^e zapach ekskrement^ow, kt^ory b^ed^ac "+
		"wymieszany z naturaln^a duchot^a tego budynku jest nie do "+
		"zniesienia. Na surowych ^scianach gdzieniegdzie dostrzegasz rysy "+
		"albo rysunki wyryte w kamieniu. Samo pomieszczenie jest niemal "+
		"puste - nie licz^ac ";

	if(jest_rzecz_w_sublokacji(0,"^lawa"))
		{
		str += "w^askiej, drewnianej ^lawy pod jedn^a ze ^scian oraz ";
		}

		str += "wielkiej misy, do kt^orej osadzeni za^latwiaj^a swoje "+
			"bardziej przyziemne potrzeby. W p^o^lnocnej ^scianie znajduj^a "+
			"si^e zakratowane drzwi. \n";

		return str;
}

public string
exits_description()
{
    return "W p^o^lnocnej ^scianie znajduj^a si^e zakratowane drzwi. \n";
}
