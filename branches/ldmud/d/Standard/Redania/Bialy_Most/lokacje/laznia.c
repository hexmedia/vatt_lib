#include <stdproperties.h>
#include <mudtime.h>
#include <macros.h>
#include <object_types.h>
#include "dir.h"

inherit BIALY_MOST_STD;
inherit "/lib/peek";

#define JEST(x) jest_rzecz_w_sublokacji(0, (x)->short())
#define SUBLOC_SCIANA ({"^sciana","^sciany","^scianie","^scian^e","^scian^a","^scianie","na"})

string dlugi_opis();

void
create_bm_room()
{
    set_short("^La^xnia");
    set_long("@@dlugi_opis@@");

    add_prop(ROOM_I_INSIDE, 1);

   add_object(BIALY_MOST_LOKACJE_DRZWI +"z_lazni.c");

    add_object(BIALY_MOST_PRZEDMIOTY + "balia.c");
	add_object(BIALY_MOST_PRZEDMIOTY + "wieszak.c",1,0,"na ^scianie");
    add_object(BIALY_MOST_PRZEDMIOTY + "lawka.c");
    dodaj_rzecz_niewyswietlana("ma^l^a drewniana ^lawka");

	add_subloc(SUBLOC_SCIANA);

    dodaj_rzecz_niewyswietlana("wieszak");

 	add_object(BIALY_MOST_LOKACJE_DRZWI +"laznia_okno.c");
 	dodaj_rzecz_niewyswietlana("okienko");


 //   add_peek("przez okienko",
 //"/d/Standard/Redania/Rinde/Centrum/lokacje/placs.c"); FIXME - gdzie skacze?

 //   add_peek("przez okno",
 //"/d/Standard/Redania/Rinde/Centrum/lokacje/placs.c"); FIXME - jw.


 //eventy?

    add_subloc_prop("^sciana", SUBLOC_I_MOZNA_POWIES, 8);
    add_subloc_prop("^sciana", SUBLOC_I_DLA_O, O_UBRANIA);
    add_subloc_prop("^sciana", CONT_I_MAX_RZECZY, 8);
}

string
dlugi_opis()

{
	string str;

	str = "^Srednich rozmiar^ow pomieszczenie w ca^lo^sci obite jest "+
		"sosnowymi deskami, kt^ore dla ochrony przed wilgoci^a pary "+
		"unosz^acej si^e cz^esto znad balii, zaimpregnowano pszczelim "+
		"woskiem. ";

	if(jest_rzecz_w_sublokacji(0,"ma^la drewniana ^lawka"))
		{
    	str += "Pod po^ludniow^a ^scian^a postawiono drewnian^a ^lawk^e a "+
			"tu^z obok, nieco nad ni^a wbito w ^scian^e ko^lki maj^ace "+
			"s^lu^zy^c za wieszak, na kt^orym podr^o^zni chc^acy wzi^a^c "+
			"k^apiel mog^a zawiesi^c swe ubrania. Prawie dok^ladnie "+
			"naprzeciw drzwi znajduje si^e niedu^ze okienko, dostarczaj^ace "+
			"tyle akurat ^swiat^la, by m^oc dostrzec co wa^zniejsze "+
			"szczeg^o^ly otoczenia. ";
		}
		else
			{
			str += "Przy po^ludniowej ^scianie stoi drewniana ^lawka, za^s "+
				"niemal naprzeciw drzwi znajduje si^e niedu^ze okienko, "+
				"dostarczaj^ace tyle akurat ^swiat^la, by m^oc dostrzec co "+
				"wa^zniejsze szczeg^o^ly otoczenia. ";
			}

	str += " \n";
	return str;
}

public string
exits_description()
{
    return "Jedyne wyj^scie st^ad prowadzi przez drzwi do sali g^l^ownej. \n";
}

void
enter_inv(object ob, object from)
{
	//a dla czlonkow lidera wchodzacego do lazni, wykonujemy:
	if(sizeof(ob->query_team()) && ob->query_prop(ZAPLACIL_ZA_LAZNIE))
	{
		foreach(object x : ob->query_team())
			if(ENV(x) == find_object(BIALY_MOST_LOKACJE+"sala"))
				x->command("laznia");
	}
}

void
leave_inv(object ob, object to)
{
	ob->remove_prop(ZAPLACIL_ZA_LAZNIE);
}