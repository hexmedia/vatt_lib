/** @package

        prosta_sypialnia.c

        Copyright(c) ppp 2000

        Author: Eleb
        Created: E   2007-04-18 01:56:49
        Last Change: E   2007-04-18 01:56:49
*/

#include "dir.h"
inherit BIALY_MOST_STD;

#include <macros.h>
#include <stdproperties.h>

string dlugi_opis();

void create_bm_room()
{
    set_short("Prosta sypialnia");
    set_long("@@dlugi_opis@@\n");

    add_item("papiery","Pouk�adane w rowniutkie i wysokie stosiki"+
            " papiery zapisane s^a starannym"+
            " urz�dowym pismem. Papiery zosta�y spi^ete rzemieniem, "+
            " widocznie razem stanowi� jak�� ca�o��.\n");
add_item(({"mysi� dziur�","dziur�"}),"Jest to malutka mysia dziurka. Jej w�a�cicieli nigdzie nie wida�.\n");

   add_prop(ROOM_I_INSIDE, 1);

   add_object(BIALY_MOST_PRZEDMIOTY + "spora_debowa_szafa.c");
   add_object(BIALY_MOST_PRZEDMIOTY + "proste_lozko.c");
   add_object(BIALY_MOST_LOKACJE_DRZWI + "z_sypialni_burmistrza.c");
   dodaj_rzecz_niewyswietlana("spora d�bowa szafa");
   dodaj_rzecz_niewyswietlana("proste jednoosobowe ��ko");

}

string
exits_description()
{
    return "Jedyne wyj^scie prowadzi przez drzwi.\n";
}

string dlugi_opis(){
   string str;
   if(jest_rzecz_w_sublokacji(0,"spora d�bowa szafa") && jest_rzecz_w_sublokacji(0,"proste jednoosobowe ��ko")){
     str = "Podobnie jak w ca�ym budynku i tu panuje zaduch. Wprost"+
           " naprzeciwko"+
           " wej�cia pod �ciana stoi poka�nych rozmiar�w rze�biona szafa."+
           " Rozgl�daj�c"+
           " si� dalej dostrzegasz tak�e proste, jednoosobowe ��ko umiejscowione"+
           " pod jednym"+
           " z okien oraz pi�trz�ce si� obok pos�ania stosy jakich� papier�w i ksi�g."+
           " Poza"+
           " tymi rzeczami pomieszczenie jest puste i sprawia wra�enie ma�o"+
           " u�ywanego. Obok"+
           " szafy wida^c mysi^a dziur� wy��obion^a w �cianie.";
  }
  if(jest_rzecz_w_sublokacji(0,"spora d�bowa szafa") && !jest_rzecz_w_sublokacji(0,"proste jednoosobowe ��ko")){
    str = "Podobnie jak w ca�ym budynku i tu panuje zaduch."+
          " Wprost naprzeciwko wej�cia pod �cian^a stoi poka�nych rozmiar�w"+
          " rze�biona szafa."+
          " Rozgl�daj�c si� dalej dostrzegasz tak�e pi�trz�ce si� obok"+
          " pos�ania stosy jakich� papier�w i ksi�g."+
          " Poza tymi rzeczami pomieszczenie jest puste i sprawia wra�enie ma�o"+
          " u�ywanego. Obok szafy wida^c mysi^a dziur� wy��obion^a w �cianie.";
  }
  if(!jest_rzecz_w_sublokacji(0,"spora debowa szafa") && jest_rzecz_w_sublokacji(0,"proste jednoosobowe ��ko")){
    str = "Podobnie jak w ca�ym budynku i tu panuje zaduch."+
      " Rozgl�daj�c si� dostrzegasz proste, jednoosobowe ��ko umiejscowione"+
      " pod jednym z okien oraz pi�trz�ce si� obok pos�ania stosy jakich� papier�w i ksi�g."+
      " Poza tymi rzeczami pomieszczenie jest puste i sprawia wrazenie malo" +
      " u�ywanego. W �cianie naprzeciwko wej�cia wida^c mysi^a dziur�.";
  }
  if(!jest_rzecz_w_sublokacji(0,"spora debowa szafa") && !jest_rzecz_w_sublokacji(0,"proste jednoosobowe ��ko")){
    str = "Podobnie jak w ca�ym budynku i tu panuje zaduch."+
      " Dooko^la dostrzegasz pi�trz�ce si� obok pos�ania stosy jakich� papier�w ksi�g."+
      " Poza tymi rzeczami pomieszczenie jest puste i sprawia wra�enie ma�o"+
      " u�ywanego. W �cianie naprzeciwko wej�cia wida^c mysi^a dziur�." ;
  }
   return str;
}
