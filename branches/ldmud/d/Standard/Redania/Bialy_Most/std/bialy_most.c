/**
 * Standard Bia�ego Mostu.
 *
 * @author Krun
 * @date Grudzie� 2007
 */

#include "dir.h"

inherit REDANIA_STD;

#include <stdproperties.h>

public void
create_bm_room()
{
}

public nomask void
create_redania()
{
    set_start_place(BM_SLEEP_PLACE);
    add_prop(ROOM_I_TYPE, ROOM_IN_CITY);
    create_bm_room();
}