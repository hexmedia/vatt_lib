//pierdolnik, tabliczka o przystanku. vera.

inherit "/std/object.c";

#include "dir.h"
#include <pl.h>
#include <stdproperties.h>
#include <macros.h>
#include <filter_funs.h>
int przeczytaj_tab();

create_object()
{
    ustaw_nazwe("tabliczka");
    dodaj_przym("ma�y","mali");
set_long(
    "   ____________________________________________\n"+
    "  |      o                             o       |\n"+
    "  | Tu zatrzymuje si� w�z przemierzaj�cy tras� |\n"+
    "  | z Piany, przez Bia�y Most a� pod Rinde.    |\n"+
    "  |    Koszt podr�y to jedyne 9 groszy.       |\n"+
    "  |      o                             o       |\n"+
    "  |____________________________________________|\n"+
    "        | |                           | |\n"+
    "        | |                           | |\n"+
    "        | |                           | |\n"+
    "        | |                           | |\n"+
    "        | |                           | |\n"+
    "        | |                           | |\n"+
    "        | |                           | |\n"+
    "        | |                           | |\n"+
    "       .| |                           | |\n");


    add_prop(OBJ_I_VOLUME, 3485);
    add_prop(OBJ_I_WEIGHT, 1583);
    add_prop(OBJ_I_NO_GET, "Nie da rady jej zabra^c, jest zbyt mocno "+
        "wbita do ziemi.\n");
    add_prop(OBJ_I_DONT_GLANCE,1);

    add_cmd_item(({"ma�� tabliczk�","tabliczk^e"}), "przeczytaj", przeczytaj_tab,
                   "Przeczytaj co?\n"); 
}

int
przeczytaj_tab()
{
   return long();
}