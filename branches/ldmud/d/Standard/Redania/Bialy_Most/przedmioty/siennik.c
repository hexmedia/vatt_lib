/* Autor: Avard
   Opis : Ohm
   Data : 20.06.07 */

inherit "/std/object.c";

#include <pl.h>
#include <stdproperties.h>
#include <macros.h>
#include <filter_funs.h>
#include <materialy.h>
#include <object_types.h>
#include "dir.h"

create_object()
{
    ustaw_nazwe("siennik");
                 
    dodaj_przym("stary","starzy");
    dodaj_przym("wytarty","wytarci");
   

    make_me_sitable("na","na sienniku","z siennika",2);

    set_long("Do^s^c gruby gdzieniedzie powycierany worek, wype^lniony "+
        "zosta^l s^lom^a by zapewni^c izloacje mi^edzy cia^lem ^spi^acego, "+
        "a ziemi^a na kt^orej siennik jest rozk^ladany. Z dziur wystaj^a "+
        "pojedy^ncze ^xd^xb^la ro^slin, kt^ore przebywaj^ace w ciemym "+
        "worku zd^a^zy^ly si^e zasmierdn^a^c. Zapewne niewietrzone s^a "+
        "siedliskiem pche^l i wszelakiego rodzaju insekt^ow.\n");
    add_prop(OBJ_I_WEIGHT, 2000);
    add_prop(OBJ_I_VOLUME, 2000);
    add_prop(OBJ_I_VALUE, 10);
    ustaw_material(MATERIALY_SLOMA, 50);
    ustaw_material(MATERIALY_LEN, 50);
    set_type(O_MEBLE);
    set_owners(({BIALY_MOST_NPC + "karczmarz_bialy_most"}));
}
public int
jestem_siennikiem_karczmy_bm()
{
        return 1;
}