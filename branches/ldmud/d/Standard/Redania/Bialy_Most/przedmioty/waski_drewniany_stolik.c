/*
 * stolik do gabinetu garnizonowego
 * opis by Khaes
 * zepsu^la faeve
 * dn. 12 maja 2007
 *
 */

#pragma strict_types

inherit "/std/container";

#include <pl.h>
#include <macros.h>
#include <filter_funs.h>
#include <stdproperties.h>
#include <composite.h>                                                         

#include <cmdparse.h>
#include "dir.h"

string czy_cos_jest_na_stoliku();


static int newLine;

nomask void
create_container()

{
   ustaw_nazwe("stolik");
	           			       
   set_long("Prosty, zbity z paru topornych kawa^lk^ow drewna stolik. Jego "+
	   "niestarannie oheblowana powierzchnia pokryta jest licznymi "+
	   "niezidentyfikowanymi, ciemnymi plamami oraz woskowymi zaciekami po "+
	   "^swiecach. \n" +
            "@@czy_cos_jest_na_stoliku@@");
   dodaj_przym("w^aski","w^ascy");
   dodaj_przym("drewniany","drewniani");

    set_owners(({BIALY_MOST_NPC + "boldrik"}));
   setuid();
   seteuid(getuid());

   add_prop(CONT_I_MAX_VOLUME, 24000);
   add_prop(CONT_I_MAX_WEIGHT, 24000);
   add_prop(CONT_I_VOLUME, 9891);
   add_prop(CONT_I_WEIGHT, 10019);
//   add_prop(OBJ_I_NO_GET, "Jest zbyt ci^e^zki.\n");
   add_prop(OBJ_I_DONT_GLANCE,1);
   add_prop(CONT_I_DONT_SHOW_CONTENTS, 1);
   add_prop(CONT_I_CANT_WLOZ_DO, 1);
   add_subloc("na");
   add_subloc_prop("na", CONT_I_MAX_WEIGHT, 10500);
   add_subloc("pod");
   add_subloc_prop("pod", CONT_I_MAX_WEIGHT, 10500);
   
}

string
czy_cos_jest_na_stoliku()
{
	newLine = 0;
   if (sizeof(subinventory("na")) > 0) {
       newLine = 1;
       return "Na jego blacie zauwa^zasz "
              + this_object()->subloc_items("na") + ".\n";
   }
      if (newLine)
       {
	 return "\n";
       }
   return "";
}