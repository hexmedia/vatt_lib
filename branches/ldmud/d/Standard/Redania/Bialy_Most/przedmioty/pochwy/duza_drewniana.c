/* Autor: Valor, (poprawki: Sed)
   Opis : �niegulak
   Data : 10.04.07 */
inherit "/std/pochwa";
#include "/sys/formulas.h"
#include <stdproperties.h>
#include <object_types.h>
#include <materialy.h>
#include <macros.h>
#include <cmdparse.h>
#include <pl.h>
#define POCHWA_PD_PLECY         1
#define POCHWA_PD_UDA           2
#define POCHWA_PD_GOLENIE       4
#define POCHWA_PD_PAS           8
#define POCHWA_NA_PLECACH       "_pochwa_na_plecach"
#define POCHWA_PRZY_UDZIE_P     "_pochwa_przy_udzie_p"
#define POCHWA_PRZY_UDZIE_L     "_pochwa_przy_udzie_l"
#define POCHWA_PRZY_GOLENI_P    "_pochwa_przy_goleni_p"
#define POCHWA_PRZY_GOLENI_L    "_pochwa_przy_goleni_l"
#define POCHWA_W_PASIE_P        "_pochwa_w_pasie_p"
#define POCHWA_W_PASIE_L        "_pochwa_w_pasie_l"
#define POCHWA_SUBLOC           "_pochwa_subloc"
void
create_pochwa()
{
    ustaw_nazwe("pochwa");
    dodaj_przym("du�y","duzi");
    dodaj_przym("drewniany","drewniani");
    set_long("Ogl^adasz du^z^a drewnian^a pochw^e w kt^orej mo^zesz "+
        "schowa^c dwur^eczny miecz. Zrobiona jest z dw^och kawa^lk^ow "+
        "drewna po^l^aczonych ze sob^a miedzianymi okuciami, kt^ore "+
        "trzymaj^a drewno ze sob^a. Wydr^a^zony otw^or wy^lo^zony jest "+
        "mi^ekk^a tkanin^a co zapewnia ciche i proste wyci^aganie broni. "+
        "Sk^orzane paski umo^zliwiaj^a przypi^ecie jej do pasa lub "+
        "przewieszenie przez plecy. Brak jakichkolwiek zdobie^n w drewnie "+
        "^swiadczy o tym i^z jest to jedna z najta^nszych pochew. \n");
    set_przypinane_do(POCHWA_PD_PLECY | POCHWA_PD_PAS );
    add_subloc_prop(0, SUBLOC_I_DLA_O, O_BRON_MIECZE_2H);
    set_type(O_POCHWY);
    add_prop(OBJ_I_WEIGHT, 2000);
    add_prop(OBJ_I_VOLUME, 1500);

    add_prop(CONT_I_REDUCE_VOLUME, 125);
    add_prop(CONT_I_MAX_WEIGHT, 10000);
    add_prop(CONT_I_MAX_VOLUME, 10000);
    add_prop(CONT_I_MAX_RZECZY, 1);

    ustaw_material(MATERIALY_DR_DAB, 75);
	ustaw_material(MATERIALY_RZ_MIEDZ, 5);
	ustaw_material(MATERIALY_SK_SWINIA, 15);
	ustaw_material(MATERIALY_TK_LEN , 5);

	add_prop(OBJ_I_VALUE, 620);
}

