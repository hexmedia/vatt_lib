/* Autor: Faeve
   Opis : Tinardan
   Data : 2.06.07 
   Info : Na podstawie kominka Avardowego */
inherit "/std/object";

#include <pl.h>
#include <macros.h>
#include <filter_funs.h>
#include <stdproperties.h>
#include <composite.h>
#include <cmdparse.h>
#include <object_types.h>
#include "dir.h"

int ile_pali=0,pali_sie=0;

nomask void
create_object()
{
    ustaw_nazwe("kominek");
	dodaj_przym("elegancki","eleganccy");
    dodaj_przym("kamienny","kamienni");
    set_long("Elegancki, kamienny, godny miejsca du^zo wi^ekszego i "+
		"okazalszego ni^z to, w kt^orym go wybudowano. Takiego kominka nie "+
		"powstydzi^lby si^e nawet pan na zamku. Jego ogrom wskazuje na to, "+
		"^ze pewnie s^lu^zy tak^ze za g^l^owne ^xr^od^lo ogrzewania gospody "+
		"� szeroki komin prowadz^acy na g^or^e i spore palenisko s^a w "+
		"stanie ogrza^c znaczn^a cz^e^s^c tego budynku. Szary kamie^n, z "+
		"kt^orego kominek zosta^l zbudowany nie wygl^ada na zbyt wyszukany, "+
		"ale jest mocny i zapewne znakomicie spe^lnia swoj^a funkcj^e � "+
		"niezbyt szybko si^e rozgrzewa, ale za to trzyma ciep^lo d^lugo. "+
		"Niekt^ore kamienie s^a ju^z wy^slizgane, jakby dotyka^ly je setki "+
		"r^ak. @@czy_plone_w_longu@@\n");
	    
    setuid();
    seteuid(getuid());
//    add_prop(CONT_I_MAX_VOLUME, 20000);
    add_prop(OBJ_I_VOLUME, 16800);
    add_prop(OBJ_I_NO_GET, "A niby w jaki spos^ob chcesz go podnie^s^c?\n");
    add_prop(OBJ_I_WEIGHT, 10000);
//    add_prop(CONT_I_MAX_WEIGHT, 20000);
//    add_subloc("do");
//    add_subloc_prop("do", CONT_I_MAX_VOLUME, 50000);
    //add_subloc_prop("na", CONT_I_MAX_WEIGHT, 500);
//    add_prop(CONT_I_DONT_SHOW_CONTENTS, 1);
    set_type(O_MEBLE);
}

string
czy_plone_w_longu()
{
    string str;
    if(ile_pali>15)
        str="W palenisku weso^lo trzaska do^s^c du^zy ogie^n.";
    else if(ile_pali>10)
        str="W palenisku weso^lo trzaska ogie^n.";
    else if(ile_pali>5)
        str="W palenisku weso^lo ma^ly ogie^n.";
    else if(ile_pali>0)
        str="W palenisku tli si^e ledwie kilka iskierek.";
    else
        str="Ogie^n w palenisku jest ca^lkowicie wygaszony.";
        
    return str;
}

void
init()
{
    ::init();
    add_action("dokladamy","do^l^o^z");
    add_action("dokladamy","pod^l^o^z");
    add_action("palimy","rozpal");
    add_action("palimy","zapal");
}

int
dokladamy(string str)
{
    mixed kominek_ob;
    object *co;
    //co=filter(co,all_inventory(TP));
    
    notify_fail(capitalize(query_verb())+" co do czego?\n");
    
    if(!stringp(query_verb()))
        return 0;
    if(!strlen(str))
        return 0;
    co=INV_ACCESS(co);
    if(TO->query_ile_pali() < 20)
    {
        if(!parse_command(str, environment(TP),
            "%i:"+PL_BIE+" 'do' %i:"+PL_DOP, co, kominek_ob))
        {
            saybb(QCIMIE(TP,PL_MIA)+ "dok^lada troch^e drewna do kominka.\n");
            write("Dok^ladasz troch^e drewna do kominka.\n");
            ile_pali = ile_pali + 5;
        }
        return 1;
    }
    else
    {
        write("Nic wi^ecej si^e ju^z nie zmie^sci.\n");
        return 1;
    }
}

int
palimy(string str)
{
    mixed kominek_ob;
    object *co;
    
    notify_fail(capitalize(query_verb())+" co?\n");

    if(!stringp(query_verb()))
        return 0;
    if(!strlen(str))
        return 0;
    co=INV_ACCESS(co);
    if(TO->query_pali_sie() == 0)
    {
        if(!parse_command(str, environment(TP),
            "%i:"+PL_BIE+"%i:"+PL_BIE, co, kominek_ob) ||
           !parse_command(str, environment(TP),
            "%i:"+PL_BIE+" [w] %i:"+PL_NAR, co, kominek_ob))
        {
            saybb(QCIMIE(TP,PL_MIA)+ "rozpala kominek.\n");
            write("Rozpalasz kominek.\n");
            ile_pali = 1;
            pali_sie = 1;
            set_alarm(2.0,0.0,"eventy");
        }
        return 1;
    }
    else
    {
        write("Ale kominek ju^z si^e pali.\n");
        return 1;
    }
}

void eventy()
{
    string str;
    switch(random(6))
    {
        case 0: str="P^lomie^n w kominku grzeje przyjemnie."; break;
        case 1: str="P^lomienne j^ezyki li^z^a delikatnie polana."; break;
        case 2: str="Z kominka dobiega do^s^c g^lo^sny trzask drewna."; break;
        case 3: str="Ogie^n weso^lo trzaska w kominku."; break;
        case 4: str="Od kominka bije weso^le ciep^lo."; break;
        case 5: str="Ciep^lo bij^ace od kominka grzeje ci^e przyjemnie."; break;
    }
    str+="\n";
    
    if(pali_sie)
        tell_room(environment(TO),str);
    
    float f;
    switch(random(10))
    {
        case 0:f=80.0;break;
        case 1:f=120.0;break;
        case 2:f=150.0;break;
        case 3:f=200.0;break;
        case 4:f=240.0;break;
        case 5:f=300.0;break;
        case 6:f=360.0;break;
        case 7:f=400.0;break;
        case 8:f=430.0;break;
        case 9:f=660.0;break;
    }    
    ile_pali++;
    
    if(ile_pali>20)
    {
        tell_room(environment(TO),"Ogie^n w kominku powoli przygasa.\n");
        pali_sie=0;
        ile_pali=0;
    }    
    else
        set_alarm(f,0.0,"eventy");
}
    


int
query_ile_pali()
{
    return ile_pali;
}
int
query_pali_sie()
{
    return pali_sie;
}
