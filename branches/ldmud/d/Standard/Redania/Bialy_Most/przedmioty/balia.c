/* 
 * balia w lazni Bialego Mostu
 * opis by Sniegulak
 * zepsula faeve
 * dn. 21 maja 2007
 * naprawil, dodal emoty i kilka innych pierdol - Vera :P
 * 8 czerwca 2007
 */

#pragma strict_types

inherit "/std/object";

#include "dir.h"
#include <pl.h>
#include <macros.h>
#include <filter_funs.h>
#include <stdproperties.h>
#include <composite.h>
#include <cmdparse.h>
#include <sit.h>
#include <object_types.h>
#include <materialy.h>

#define LIVE_O_ON_HORSE "_live_o_on_horse"
object *w_balii;

nomask void
create_object()
{
    ustaw_nazwe("balia");

    set_long("Zbudowana z drewnianych desek zaci^sni^etych stalowymi "+
                "obr^eczami balia, jest w stanie pomie^sci^c dwie osoby.  Drewno "+
                "zosta^lo zaimpregnowane i dok^ladnie oheblowane, by zwi^ekszy^c "+
                "komfort os^ob korzystaj^acych z k^apieli w niej. Wysoka na dwa "+
                "^lokcie pozwala na spokojne zanurzenie ca^lego cia^la, i oddanie "+
                "si^e mi^lemu odpoczynkowi po ci^e^zkim dniu. \n");

        dodaj_przym("du^zy","duzi");
        dodaj_przym("drewniany","drewniani");

    ustaw_material(MATERIALY_DR_BUK);
    setuid();
    seteuid(getuid());
    add_prop(OBJ_I_VOLUME, 566800);
    add_prop(OBJ_I_DONT_GLANCE, 1);
    add_prop(OBJ_I_WEIGHT, 55000);
    add_prop(OBJ_I_NO_GET, "Nie, nie, nie. Za ci^e^zkie i zbyt niewygodne "+
                "do niesienia. \n"); //bylo: wazy z tone
    w_balii = ({ });

//    set_owners(({karczmarz BM?})); FIXME!!!!!!
}

public int
query_type()
{
        return O_MEBLE;
}

public int
jestem_sobie_balia()
{
        return 1;
}

int
wejdz(string str)
{
    object *podest;
    int ret;

    notify_fail("Wejd^x gdzie?\n");

    if (!strlen(str))
        return 0;

    if (TP->query_prop(LIVE_O_ON_HORSE))
    {
        notify_fail("Dosiadzasz przecie^z wierzchowca!\n");
        return 0;
    }

    if(TP->query_prop(SIT_SIEDZACY))
    {
        notify_fail("Przecie^z ju^z gdzie^s siedzisz.\n");
        return 0;
    }
	if(TP->query_prop(SIT_LEZACY))
    {
        notify_fail("A mo^ze wpierw wstaniesz?\n");
        return 0;
    }


    if (!parse_command(str, all_inventory(TP) +
        all_inventory(environment(TP)),
        "'do' %i:"+ PL_DOP, podest))
        return 0;

    podest = NORMAL_ACCESS(podest, 0, 0);

    if (!sizeof(podest))
        return 0;

    if (member_array(TP, w_balii) != -1)
    {
        notify_fail("Przecie^z jeste^s w balii!\n");
        return 0;
    }

  /*  if (w_balii)
    {
        notify_fail("Na pode^scie ju^z kto^s jest.\n");
     return 0;
    }*/

    if (environment(TP) != environment(TO))
    {
        notify_fail("Masz bali^e w ekwipunku? Hmm... Ciekawe... \n");
        return 0;
    }

	object *inv = TP->subinventory();
	foreach(object x : inv)
	{
		if(x->query_prop(OBJ_M_NO_DROP))
			inv-=({x});

		if(x->query_name() == "przemoczenie")
			x->wysusz();
	}

	if(sizeof(inv) || member_array("plecak",all_inventory(TP)->query_name()) != -1 ||
	member_array("pas",all_inventory(TP)->query_name()) != -1 ||
	member_array("pochwa",all_inventory(TP)->query_name()) != -1)
	{
        notify_fail("Musisz wpierw od�o�y� wszystko, co masz przy sobie.\n");
        return 0;
    }

    w_balii += ({ TP });
//  TP->set_no_show_composite(1);
//    TP->add_prop(OBJ_I_DONT_GLANCE,1);
    TP->add_prop(LIVE_S_EXTRA_SHORT, " siedz^ac" +
       TP->koncowka("y", "a", "e") + " w balii");

    TP->add_prop(LIVE_M_NO_MOVE, "Mo^ze lepiej b^edzie gdy "+
                "najpierw wyjdziesz z balii, hm? \n");

    TP->add_prop(SIT_SIEDZACY, TO);

    TO->add_prop(OBJ_M_NO_GET,
        "Nie mo^zesz wzi^a^c balii, gdy kto^s w niej siedzi.\n");

	//Wejscie do balii regeneruje zmeczenie
	//relaksuje...
	TP->set_old_fatigue(TP->query_old_max_fatigue());

	TP->add_prop(PLAYER_I_NO_ACCEPT_GIVE,1);

    write("Powoli wchodzisz do "+TO->query_short(PL_DOP)+", a ciep^lo "+
		"wype^lniaj^acej j^a wody zaczyna ogrzewa^c ci^e przyjemnie.\n");
    saybb(QCIMIE(TP,PL_MIA)+" powoli wchodzi do "+
          TO->query_short(PL_DOP)+" wype^lnionej ciep^l^a i paruj^ac^a wod^a.\n");

    return 1;
}

int
wyjdz(string str)
{
    object *podest;
    int ret;

    notify_fail("Wyjd^x sk^ad?\n");

    if (!strlen(str))
        return 0;

    if (!parse_command(str, all_inventory(environment(TP)),
        "'z' %i:"+ PL_DOP, podest))
	{
		if(member_array(TP,w_balii) != -1)//bo przez okienko tez mozna wyjsc
			notify_fail("Wyjd^x sk^ad?");

        return 0;
	}

    podest = NORMAL_ACCESS(podest, 0, 0);

    if (!sizeof(podest))
        return 0;

    if (member_array(TP, w_balii) == -1)
    {
        notify_fail("Przecie^z nie siedzisz w balii.\n");
        return 0;
    }

    w_balii -= ({ TP });

    TP->remove_prop(LIVE_S_EXTRA_SHORT);
    TP->remove_prop(LIVE_M_NO_MOVE);
    TP->remove_prop(SIT_SIEDZACY);
    TO->remove_prop(OBJ_M_NO_GET);
	TP->remove_prop(PLAYER_I_NO_ACCEPT_GIVE);

	//dodajemy tylko wtedy, jesli nie mamy juz przemoczenia.
	if(member_array("przemoczenie",all_inventory(TP)->query_name()) == -1)
	{
		object przemoczenie = clone_object("/std/efekty/przemoczenie.c");
		przemoczenie->przemocz(TP);
	}
    write("Wychodzisz z "+TO->query_short(PL_DOP)+".\n");
    saybb(QCIMIE(TP,PL_MIA)+" wychodzi z "+
   TO->query_short(PL_DOP)+".\n");

    return 1;
}

int wstan()
{
    if(TP->query_prop(SIT_SIEDZACY) != TO)
        return 0;

    TP->catch_msg("Je�li chcesz sko�czy� k�piel poprostu wyjd� z balii.\n");

    return 1;
}



void
wyplywaja_babelki()
{
	tell_room(ENV(TO),"Kilka b^abelk^ow wyp^lywa na powierzchni^e wody "+
			"w balii...\n");
}


//emoty blokujace niektore czynnosci
//dla siedzacych w balii
int
all_cmds()
{
	if(member_array(TP,w_balii) == -1)
		return 0;

	switch(query_verb())
	{
		case "kopnij":
		case "zata^ncz":
		case "podskocz":
		case "uk�o�":
		case "pok�o�":
		case "przebieraj":
		case "we�":
			write("Wyjd^x z balii, je^sli chcesz to uczyni^c.\n");
			return 1;
			break;
		case "pierdnij":
			write("Koncentrujesz si^e przez chwil^e... Achh ! Co za ulga...\n");
			saybb(QCIMIE(TP,PL_MIA)+" zamyka oczy i koncentruje si^e.\n");
			set_alarm(itof(random(4)),0.0,"wyplywaja_babelki");
			return 1;
			break;
		default:
			return 0;
	}

		return 0;
}

int
ochlap(string str)
{
	object *cele;

    notify_fail(capitalize(query_verb())+" kogo?\n");

    if (!str) return 0;

	if (!parse_command(str, all_inventory(ENV(TP)) +
		all_inventory(TP), "%l:"+PL_BIE, cele))
        return 0;

    cele = NORMAL_ACCESS(cele, 0, 0);

    if (sizeof(cele) >= 2)
    {
        notify_fail("Nie mo�esz chlapa� wszystkich naraz!\n");
        return 0;
    }

    if(sizeof(cele)==0)
    {
        notify_fail("Kogo chcesz ochlapa�?\n");
        return 0;
    }


	if(!sizeof(cele))
		return 0;

	TP->catch_msg("Z psotliwym u^smieszkiem na twarzy chlapiesz wod^a na "+
		QIMIE(cele[0],PL_DOP)+".\n");
	saybb(QCIMIE(TP,PL_MIA)+" z psotliwym u^smieszkiem na twarzy chlapie wod^a "+
		"na "+	QIMIE(cele[0],PL_DOP)+".\n", ({ TP, cele[0] }));
	cele[0]->catch_msg(QCIMIE(TP,PL_DOP)+" z psotliwym u^smieszkiem na twarzy "+
		"chlapie ci^e wod^a.\n");
	return 1;
}

int
zanurkuj(string str)
{
	notify_fail("Zanurkuj?\n");
	if(strlen(str))
		return 0;

	if(member_array(TP,w_balii) == -1)
	{
		write("Musisz wpierw wej^s^c do balii, by m^oc to uczyni^c.\n");
		return 1;
	}
	
	write("Dajesz nura pod powierzchni^e wody pe^lnej mydlin. Ju^z po chwili "+
		"oceniasz, ^ze nie by^l to najlepszy pomys^l - niczego tu nie zobaczysz, "+
		"a szczypi^ace oczy upominaj^a si^e o jak najszybsze wydostanie si^e "+
		"na powierzchni^e. Po chwili podnosisz g^low^e i prychasz resztkami wody "+
		"osadzonej na ustach.\n");
	saybb(QCIMIE(TP,PL_MIA)+" daje nura pod powierzchni^e wody, by po chwili "+
		"wydosta^c si^e na powierzchni^e prychaj^ac przy tym nieznacznie "+
		"wod^a, kt^ora dosta^la "+TP->koncowka("mu","jej","temu")+" si^e "+
		"mi^edzy usta.\n");
	return 1;
}

int
babelkuj()
{
	if(member_array(TP,w_balii) != -1)
		TP->command("pierdnij");
	else
		write("To^c nie jeste^s w balii...Jak^ze to tak?\n");
	return 1;
}

int
babelkowanie()
{
	write("B^abelki, jak b^abelki... Ka^zdy je lubi.\n");
	return 1;
}

public int
pomoc(string str)
{
    object ob;
    notify_fail("Nie ma pomocy na ten temat.\n");
    if (!str) return 0;
    if (!parse_command(str, ENV(TP), "%o:" + PL_MIA, ob))
        return 0;
    if (!ob) return 0;
    if (ob != TO)
        return 0;
   
    write("Oto w istocie balia zwana tak^ze balej^a. Akurat ta balia zdaje si^e"+
		" nie s^lu^zy babom jako naczynie do prania bielizny, lecz ma znacznie "+
		"przyjemniejsze zastosowanie. Spr^obuj do^n wej^s^c, je^sli pragniesz "+
		"zazna^c przyjemno^sci i zrelaksowa^c si^e siedz^ac w ciep^lej wodzie. "+
		"No bo skoro ci^e ona interesuje, za^k^ladam, ^ze lubisz si^e k^apa^c...\n"+
		"Gdy ju^z otoczy ci^e ciep^lo wody, mo^zesz da^c popis swych "+
		"umiej^etno^sci p^lywackich i spr^obowa^c zanurkowa^c. Mo^zesz tak^ze "+
		"uraczy^c kogo^s psotliwymi uciechami, jak ochlapanie.\n");
    return 1;
}


void
init()
{
    add_action("wejdz", "wejd^x");
    add_action("wyjdz", "wyjd^x");
    add_action("wstan", "wstan");
	add_action("all_cmds","",1);
	add_action("zanurkuj","zanurkuj");
	add_action("ochlap","ochlap");
	add_action("ochlap","pochlap");
	add_action("pomoc", "?", 2);
	add_action("babelkowanie", "?b^abelkowanie");
	add_action("babelkuj", "b^abelkuj");
}

