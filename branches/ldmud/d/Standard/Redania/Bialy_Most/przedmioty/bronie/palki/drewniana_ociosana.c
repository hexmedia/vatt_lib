/* Autor: Valor
   Opis:  Faeve
   Dnia:  12.06.07
*/

inherit "/std/weapon";

#include <macros.h>
#include <stdproperties.h>
#include <wa_types.h>
#include <pl.h>
#include <materialy.h>
#include <object_types.h>

void create_weapon() 
{
        ustaw_nazwe("pa^lka");
        dodaj_przym("drewniany", "drewniani");
        dodaj_przym("ociosany", "ociosani");
                    
        set_long("Ten kawa^lek drewna o d^lugo^sci nieco kr^otszej ni^z "+
            "przedrami^e doros^lego cz^lowieka zosta^l dok^ladnie ociosany "+
            "- z jednej strony jest do^s^c szeroki, drugi za^s koniec - "+
            "jest sporo w^e^zszy - na tyle, by mo^zna by^lo zmie^sci^c go "+
            "w d^loni. Ta w^la^snie cz^e^s^c owini^eta jest sk^ork^a - "+
            "do^s^c szorstk^a - by uchwyt by^l pewniejszy i mocniejszy. \n");
            set_hit(25);
            set_pen(19);

        set_wt(W_CLUB);
        set_dt(W_BLUDGEON);
     
        add_prop(OBJ_I_VALUE, 7);
        add_prop(OBJ_I_WEIGHT, 1000);
        add_prop(OBJ_I_VOLUME, 1000);   
        set_type(O_BRON_MACZUGI);
        ustaw_material(MATERIALY_DR_DAB, 90);
        ustaw_material(MATERIALY_SK_BYDLE, 10);
 }