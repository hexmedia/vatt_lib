/* Autor: Valor (poprawki: Sed)
   Opis : �niegulak
   Data : 10.04.07 */
inherit "/std/pochwa";
#include "/sys/formulas.h"
#include <stdproperties.h>
#include <object_types.h>
#include <materialy.h>
#include <macros.h>
#include <cmdparse.h>
#include <pl.h>
#define POCHWA_PD_PLECY         1
#define POCHWA_PD_UDA           2
#define POCHWA_PD_GOLENIE       4
#define POCHWA_PD_PAS           8
#define POCHWA_NA_PLECACH       "_pochwa_na_plecach"
#define POCHWA_PRZY_UDZIE_P     "_pochwa_przy_udzie_p"
#define POCHWA_PRZY_UDZIE_L     "_pochwa_przy_udzie_l"
#define POCHWA_PRZY_GOLENI_P    "_pochwa_przy_goleni_p"
#define POCHWA_PRZY_GOLENI_L    "_pochwa_przy_goleni_l"
#define POCHWA_W_PASIE_P        "_pochwa_w_pasie_p"
#define POCHWA_W_PASIE_L        "_pochwa_w_pasie_l"
#define POCHWA_SUBLOC           "_pochwa_subloc"
void
create_pochwa()
{
    ustaw_nazwe("temblak");
    dodaj_przym("prosty","pro�ci");
    dodaj_przym("miedziany","miedziani");
    set_long("Ogl^adasz prosty, miedziany temblak w kt^orym mo^zesz "+
        "umie^sci^c top^or. Miedziany korpus bez jakichkolwiek ornament^ow "+
        "stanowi g^l^own^a cz^e^s^c tej uprz^e^zy. Przynitowane do niego "+
        "^cwiekowane sk^orzane pasy pozwalaj^a na bezpieczny bieg z bronia "+
        "u pasa. Temblak ten mo^zna przypi^a^c do pasa lub przewiesi^c "+
        "przezplecy. \n");
    set_przypinane_do(POCHWA_PD_PLECY | POCHWA_PD_PAS);
    add_subloc_prop(0, SUBLOC_I_DLA_O, O_BRON_TOPORY);
    set_type(O_POCHWY);
    add_prop(OBJ_I_WEIGHT, 2000);
    add_prop(OBJ_I_VOLUME, 1500);
    add_prop(CONT_I_REDUCE_VOLUME, 125);
    add_prop(CONT_I_MAX_WEIGHT, 10000);
    add_prop(CONT_I_MAX_VOLUME, 10000);
    add_prop(CONT_I_MAX_RZECZY, 1);

		ustaw_material(MATERIALY_RZ_MIEDZ, 65);
	ustaw_material(MATERIALY_SK_SWINIA, 35);

	add_prop(OBJ_I_VALUE, 646);
}
