 /*
 Made by Eleb.
 */


inherit "/std/receptacle";

#pragma strict_types
#include <pl.h>
#include <macros.h>
#include <filter_funs.h>
#include <stdproperties.h>
#include <composite.h>
#include <cmdparse.h>
#include <object_types.h>
#include "/sys/materialy.h"
#include "dir.h"

void
create_container(){
    
    ustaw_nazwe("biurko");
    dodaj_przym("masywny","masywni");
    dodaj_przym("d�bowy","d�bowi");
    ustaw_material(MATERIALY_DR_DAB);
    set_type(O_MEBLE);
    set_long("Masywne, d�bowe biurko sprawiaj�ce wra�enie nie tyle zbitego,"+
             " co wyrze�bionego w drewnie. Motywy w jakie"+
             " rze�biarz przyozdobi� �w mebel to g��wnie rozmyte ludzkie sylwetki,"+ 
             " kt�re zaj�te s� pracami, typowymi dla"+
             " rybak�w. Biurko posiada trzy szuflady, kt�rych ciemne ga�ki maj� kszta�t rybiego �ba.\n"+
             "@@opis_sublokacji|Dostrzegasz na nim |na|.\n||" +
              PL_BIE+ "@@"+
             "@@opis_sublokacji|Dostrzegasz pod nim |pod|.\n||" +
              PL_BIE+ "@@");
             
    set_owners(({BIALY_MOST_NPC + "burmistrz"}));
    add_prop(CONT_I_MAX_VOLUME, 200000);
    add_prop(CONT_I_VOLUME, 80000);
    add_prop(CONT_I_WEIGHT, 90000);
    add_prop(CONT_I_MAX_WEIGHT, 200000);
    add_prop(CONT_I_DONT_SHOW_CONTENTS, 1);
    add_prop(CONT_I_RIGID, 1);
    add_prop(CONT_I_CANT_WLOZ_DO, 1);
    add_prop(CONT_I_NO_OPEN_DESC, 1);
    add_prop(CONT_I_CANT_OPENCLOSE, 1);
    
    
    
    add_subloc("na");
    add_subloc_prop("na", CONT_I_MAX_WEIGHT, 1000);
    add_subloc_prop("na", CONT_I_MAX_VOLUME, 1000);

    add_subloc("pod");
    add_subloc_prop("pod", CONT_I_MAX_WEIGHT, 1000);
    add_subloc_prop("pod", CONT_I_MAX_VOLUME, 1000);
    
  add_item(({"szuflad�", "szuflady"}), "Biurko posiada trzy szerokie szuflady.\n"); 
  /* Pierwsza szuflada */ 
  add_subloc(({"pierwsza szuflada", "pierwszej szuflady", "pierwszej szufladzie",
               "pierwsz� szuflad�", "pierwsz� szuflad�", "pierwszej szufladzie"}));
  add_subloc_prop("pierwsza szuflada", CONT_I_MAX_VOLUME, 10000);
  add_subloc_prop("pierwsza szuflada", CONT_I_MAX_WEIGHT, 20000);
  add_subloc_prop("pierwsza szuflada", SUBLOC_S_OB_GDZIE, "wewn�trz pierwszej szuflady");
  add_subloc_prop("pierwsza szuflada", SUBLOC_I_OB_PRZYP, PL_DOP);
  add_subloc_prop("pierwsza szuflada", CONT_M_OPENCLOSE, "pierwsz� szuflad�");
  add_subloc_prop("pierwsza szuflada", SUBLOC_I_RODZAJ, PL_ZENSKI);
  add_subloc_prop("pierwsza szuflada", CONT_I_CLOSED, 1);
  
  
  add_item("pierwsz� szuflad�", "Jest to szeroka szuflada z ga�ka w kszta�cie rybiego �ba" +
      "@@opis_sublokacji| zawieraj�ca |pierwsza szuflada|.|.@@\n" +
      "@@if_subloc_closed|pierwsza szuflada|Jest zamkni�ta.\n@@");
   /*next one. Jezu! napisze funkcje!! co by bylo gdyby bylo 100 szuflad?! :]*/
  add_subloc(({"druga szuflada", "drugiej szuflady", "drugiej szufladzie",
               "drug� szuflad�", "drug� szuflad�", "drugiej szufladzie"}));
  add_subloc_prop("druga szuflada", CONT_I_MAX_VOLUME, 10000);
  add_subloc_prop("druga szuflada", CONT_I_MAX_WEIGHT, 20000);
  add_subloc_prop("druga szuflada", SUBLOC_S_OB_GDZIE, "wewn�trz drugiej szuflady");
  add_subloc_prop("druga szuflada", SUBLOC_I_OB_PRZYP, PL_DOP);
  add_subloc_prop("druga szuflada", CONT_M_OPENCLOSE, "drug� szuflad�");
  add_subloc_prop("druga szuflada", SUBLOC_I_RODZAJ, PL_ZENSKI);
  add_subloc_prop("pierwsza szuflada", CONT_I_CLOSED, 1);
  add_item("drug� szuflad�", "Jest to szeroka szuflada z ga�ka w kszta�cie rybiego �ba" +
      "@@opis_sublokacji| zawieraj�ca |druga szuflada|.|.@@\n" +
      "@@if_subloc_closed|druga szuflada|Jest zamkni�ta.\n@@");  
      
  add_subloc(({"trzecia szuflada", "trzeciej szuflady", "trzeciej szufladzie",
               "trzeci� szuflad�", "trzeci� szuflad�", "trzeciej szufladzie"}));
  add_subloc_prop("trzecia szuflada", CONT_I_MAX_VOLUME, 10000);
  add_subloc_prop("trzecia szuflada", CONT_I_MAX_WEIGHT, 20000);
  add_subloc_prop("trzecia szuflada", SUBLOC_S_OB_GDZIE, "wewn�trz trzeciej szuflady");
  add_subloc_prop("trzecia szuflada", SUBLOC_I_OB_PRZYP, PL_DOP);
  add_subloc_prop("trzecia szuflada", CONT_M_OPENCLOSE, "trzeci� szuflad�");
  add_subloc_prop("trzecia szuflada", SUBLOC_I_RODZAJ, PL_ZENSKI);
  add_subloc_prop("pierwsza szuflada", CONT_I_CLOSED, 1);
  add_item("trzeci� szuflad�", "Jest to szeroka szuflada z ga�ka w kszta�cie rybiego �ba" +
      "@@opis_sublokacji| zawieraj�ca |trzecia szuflada|.|.@@\n" +
      "@@if_subloc_closed|trzecia szuflada|Jest zamkni�ta.\n@@");
    
    add_prop(CONT_I_DONT_SHOW_CONTENTS,1);
}


