
inherit "/std/object.c";

#include <pl.h>
#include <stdproperties.h>
#include <macros.h>
#include <filter_funs.h>

create_object()
{

    ustaw_nazwe("drogowskaz");
                 
    dodaj_przym("zabrudzony","zabrudzeni");
    dodaj_przym("szeroki","szerocy");
      
    set_long("    **************************\n"+
             "    *                        *\n"+
             "    *  PRZYSTA^N - OXENFURT   *\n"+
             "    *           |            *\n"+
             "    *           |            *\n"+
             "    *           |            *\n"+
             "    *           O---TRETOGOR *\n"+
             "    *           |            *\n"+
             "    *           |            *\n"+
             "    *           |            *\n"+
             "    *       BIALY MOST       *\n"+
             "    *                        *\n"+
             "    **************************\n"+
             "              |    |          \n"+
             "              |    |          \n"+
             "              |    |          \n");
    add_prop(OBJ_I_NO_GET, "Ten drogowskaz jest mocno wbity "+
                           "w ziemi^e, nie masz do^s^c si^ly by go "+
                           "wyrwa^c.\n");

}
