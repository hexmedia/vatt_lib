//Oto lampa uliczna do Bia�ego Mostu. Vera

inherit "/std/lampa_uliczna";
#include "dir.h"
#include <stdproperties.h>


void
create_street_lamp()
{
    set_long("@@dluuuuuuuugi@@");
    set_light_message("Str� zdejmuje ze s�upa lamp�, "+
                      "nape�nia j� olejem, po czym "+
                      "odwiesza z powrotem. Lampa swym "+
		      "delikatnym �wiat�em rozja�nia "+
                      "nieco okolic� i rozwiewa cz�ciowo "+
		      "mroki ulicy.\n");
    set_extinguish_message("Str� zdejmuje ze s�upa lamp�, "+
                           "gasi j�, po czym odwiesza z "+
                           "powrotem.\n");
    set_owners(({BIALY_MOST_NPC + "burmistrz"}));//FIXME dodac burmistrza
}

string dluuuuuuuugi()
{
    string str;
	str="Blaszana lampa zawieszona na solidnym haku ";
	if(this_object()->query_prop(OBJ_I_LIGHT))
		str+="roz�wietla okolic�";
	else
		str+="hu�ta si� nad g�owami przechodni�w";
	
	str+=". Nie�le wykonana i w miar� nowa nie szpeci zgrabnej "+
			"kamieniczki na kt�rej j� umieszczono.\n";
    return str;
}
