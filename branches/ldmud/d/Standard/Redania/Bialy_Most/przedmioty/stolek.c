/*
Made by Eleb
*/

inherit "/std/object.c";

#include "/sys/stdproperties.h"
#include "/sys/macros.h"
#include "/sys/pl.h"
#include "/sys/materialy.h"
#include "/sys/object_types.h"
#include "dir.h"

void
create_object(){
	ustaw_nazwe("sto�ek",PL_MESKI_NOS_NZYW);
	set_long("Wykonany z drewna, masywny sto�ek o krzywych nogach.\n");
	dodaj_przym("masywny","masywni");
	set_type(O_MEBLE);
	ustaw_material(MATERIALY_DR_BUK, 100);
	add_prop(CONT_I_WEIGHT, 1000);
	make_me_sitable("na","na sto�ku","ze sto�ka",1);
	
}
