inherit "/std/object";

#include "/sys/macros.h"
#include "/sys/pl.h"
#include "/sys/materialy.h"
#include "/sys/object_types.h"
#include "/sys/formulas.h"
#include "dir.h"

void
create_object()
{
    ustaw_nazwe("worek");
    dodaj_przym("lniany", "lniani");
    dodaj_przym("gruby", "grubi");

    set_long("Lniany gruby worek wype^lniony po brzegi zosta^l bardzo mocno "+
        "zawi^azany konopnym sznurem. Zapewne s^lu^zy do przechowywania "+
        "pszenicy b^ad^x to jakiego^s innego zbo^za.\n");
         
    add_prop(OBJ_I_WEIGHT, 7000);
    add_prop(OBJ_I_VOLUME, 7000);
    add_prop(OBJ_I_VALUE, 220);
    set_type(O_INNE);

    ustaw_material(MATERIALY_LEN);
    set_owners(({BIALY_MOST_NPC + "karczmarz_bialy_most"}));

}