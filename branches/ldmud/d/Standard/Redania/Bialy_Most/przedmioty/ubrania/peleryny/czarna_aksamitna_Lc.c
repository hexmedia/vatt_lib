
/* Autor: Avard
   Opis : Faeve
   Data : 16.06.07 */

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <pl.h>
#include <macros.h>
#include <materialy.h>
#include <object_types.h>

void
create_armour()
{ 

    ustaw_nazwe("peleryna");
    dodaj_przym("czarny","czarni");
    dodaj_przym("aksamitny", "aksamitni");
    set_long("Delikatny i b^lyszcz^acy meszek aksamitu kontrastuje z "+
        "niema^lym ci^e^zarem ca^lej peleryny. Ko^lnierz-st^ojka zapinany "+
        "na srebrny guziczek tylko dodaje ubiorowi uroku i elegancji. "+
        "Po^ly p^laszcza zachodz^a na klatk^e piersiow^a, gdzie wi^azane "+
        "s^a srebrn^a ta^sm^a. Brzegi materia^lu wyko^nczone s^a srebrn^a "+
        "nici^a, kt^ora b^lyska lekko przy ka^zdym ruchu, podkre^slaj^ac "+
        "misterno^s^c wykonania peleryny.\n"); 

    ustaw_material(MATERIALY_AKSAMIT, 99);
    ustaw_material(MATERIALY_SREBRO, 1);
    set_type(O_UBRANIA);
    set_slots(A_CLOAK);
    add_prop(OBJ_I_WEIGHT, 700);
    add_prop(OBJ_I_VOLUME, 7000);
    add_prop(OBJ_I_VALUE, 330);
    add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    add_prop(ARMOUR_I_D_ROZCIAGLIWOSC, 50);
    add_prop(ARMOUR_I_U_ROZCIAGLIWOSC, 50);
    set_size("L");
    set_likely_cond(17);

}
