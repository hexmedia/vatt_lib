/* 
 * faeve
 * dn. 16.06.07
 *
 */

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <macros.h>
#include <materialy.h>

void
create_armour() 
{
    ustaw_nazwe("suknia");
     dodaj_przym("zielonkawy","zielonkawi");
	 dodaj_przym("d^lugi","d^ludzy");

      
     set_long("Suknia ta prawie nie ma ozd^ob, nie licz^ac ciemnozielonej "+
		 "wst^egi wi^azanej z ty^lu w talii na du^z^a kokard^e oraz "+
		 "przedziwnego dekoltu w miejscu, kt^orego przyszyto kolisty "+
		 "kawa^lek marszczonego kremowego materia^lu. Si^egaj^acy do piersi, "+
		 "na kra^ncach wyko^nczony jest delikatn^a koronk^a, na szyi za^s "+
		 "uformowana jest niedu^za st^ojka, zako^nczona tym^ze samym "+
		 "motywem. Wdzi^ecznej elegancji dodaj^a lekko bufiaste, do^s^c "+
		 "szerokie r^ekawy si^egaj^ace nieco poni^zej ^lokcia, mankieciki "+
		 "za^s wi^azane s^a na cieniutkie wst^a^zeczki w tym samym kolorze, "+
		 "co wst^ega u talii. Suknia, do^s^c skromna, acz elegancka, sprawia "+
		 "bardzo przyjemne i pozytywne wra^zenie, a dama, kt^ora j^a nosi na "+
		 "pewno nie pozostanie niezauwa^zona.\n");

     set_ac(A_THIGHS | A_HIPS | A_STOMACH, 2, 2, 2);
     add_prop(OBJ_I_WEIGHT, 100);
     add_prop(OBJ_I_VOLUME, 1200);
     add_prop(OBJ_I_VALUE, 100);

     add_prop(ARMOUR_I_DLA_PLCI, 1);

	 ustaw_material(MATERIALY_BAWELNA, 80);
	 ustaw_material(MATERIALY_KREPA, 20);


	 set_size("M");
     set_likely_cond(17);
}
