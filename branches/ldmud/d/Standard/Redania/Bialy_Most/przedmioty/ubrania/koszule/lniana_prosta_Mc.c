/* Opis & wykonanie: Valor
   Data: 17.06.07
*/

inherit "/std/armour";
#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <object_types.h>
#include <macros.h>
#include <materialy.h>

void
create_armour()
{  
        ustaw_nazwe("koszula");
    dodaj_przym("lniany","lniani");
    dodaj_przym("prosty", "pro^sci");
    set_long("W ca^lo^sci wykonana z lnu koszula nie cechuje si^e niczym "+
        "szczeg^olnym. Ot, kawa^lek materia^lu, kt^orego jedyn^a ozdob^a "+
        "mo^ze by^c rzemyk wisz^acy tu^z pod brod^a. Kra^nce ubrania "+
        "zd^a^zy^ly si^e ju^z poprzeciera^c, a z r^ekaw^ow wisz^a "+
        "pojedy^ncze nitki tkaniny. \n");

        set_slots(A_TORSO, A_ARMS);

    add_prop(OBJ_I_WEIGHT, 400); 
    add_prop(OBJ_I_VOLUME, 400);
    add_prop(OBJ_I_VALUE, 10);
    add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    set_size("M");
    ustaw_material(MATERIALY_LEN);
    set_type(O_UBRANIA);
    set_likely_cond(22);
}