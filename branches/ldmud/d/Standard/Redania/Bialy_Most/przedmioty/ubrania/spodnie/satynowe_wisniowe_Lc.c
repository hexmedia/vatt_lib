/* Autor: Avard
   Opis:  Faeve
   Data:  16.06.07 */ 

inherit "/std/armour";

#include <wa_types.h>
#include <formulas.h>
#include <stdproperties.h>
#include <pl.h>
#include <macros.h>
#include <materialy.h>
#include <object_types.h>

void
create_armour()
{
    ustaw_nazwe("spodnie");
    ustaw_nazwe_glowna("para");
        
    dodaj_przym("satynowy","satynowi");
    dodaj_przym("wi^sniowy","wi^sniowi");
        
    set_long("Niby taki zwyk^ly kr^oj, a jednak jest w nich co^s dziwnego, "+
        "przyci^agaj^acego spojrzenie. Pomijaj^ac oczywi^scie soczysty, "+
        "wi^sniowy kolor. Spodnie uszyte tak, by zgrabnie przylega^ly do "+
        "bioder, ni^zej za^s nieco si^e rozszerzaj^a tworz^ac swego rozaju "+
        "bufy. W^e^zsze s^a dopiero w okolicy ^lydek - tu, zgrabnie "+
        "pomarszczone, schodz^a si^e w do^s^c d^lugim, przylegaj^acym do "+
        "sk^ory, ale nie obcis^lym mankiecie, kt^ory z ^latwo^sci^a mo^zna "+
        "wsun^a^c w d^lugie buty. \n");
        
    set_slots(A_LEGS, A_HIPS);
    add_prop(OBJ_I_VOLUME, 1000);
    add_prop(OBJ_I_WEIGHT, 1000);
    add_prop(OBJ_I_VALUE, 200);      
    ustaw_material(MATERIALY_SATYNA, 100);
    set_type(O_UBRANIA);
    add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
    set_size("L");
}