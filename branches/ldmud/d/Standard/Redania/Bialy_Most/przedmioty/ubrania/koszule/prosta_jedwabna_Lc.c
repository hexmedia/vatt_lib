/* Autor: Avard
   Opis : Faeve
   Data : 16.06.07 */

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <macros.h>
#include <materialy.h>

void
create_armour()
{
    ustaw_nazwe("koszula");
    dodaj_przym("prosty", "pro^sci");
    dodaj_przym("jedwabny","jedwabni");


    set_long("Kr^oj tej koszuli jest nadzwyczaj prosty, lecz jednocze^snie "+
        "elegancki - niedu^zy, ^snie^znobia^ly ko^lnierzyk podkre^sla "+
        "kszta^lt szyi, a d^lugie r^ekawy, lekko bufiaste i zapinane na "+
        "srebrzyste guziczki nadaj^a garderobie wytworno^sci. Koszula nie "+
        "posiada ^zadnych zb^ednych ozd^ob - wystarczy sam fakt, i^z uszyto "+
        "j^a z wysokiej jako^sci jedwabiu.\n");

    set_slots(A_TORSO | A_SHOULDERS | A_FOREARMS | A_ARMS);
    add_prop(OBJ_I_WEIGHT, 560);
    add_prop(OBJ_I_VOLUME, 2400);
    add_prop(OBJ_I_VALUE, 150);

	set_size("L");
    add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    ustaw_material(MATERIALY_JEDWAB, 95);
	ustaw_material(MATERIALY_SREBRO, 5);
    set_likely_cond(15);
}


