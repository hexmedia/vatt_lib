/* Autor: Duana
  Opis : Anthilie
  Data : 02.06.2007
*/

inherit "/std/object.c";

#include <pl.h>
#include <stdproperties.h>
#include <macros.h>
#include <filter_funs.h>
#include <materialy.h>
#include <object_types.h>
#include "dir.h"

create_object()
{
    ustaw_nazwe("^l^o^zko");
                 
    dodaj_przym("stary","starzy");
    dodaj_przym("drewniany","drewniani");
   

    make_me_sitable("na","na ^l^o^zku","z ^l^o^zka",2);

    set_long("Jest to ci^e^zkie, masywne ^l^o^zko wykonane z drewna, kt^ore "+
        "zd^a^zy^lo ju^z sczernie^c z up^lywem lat. Cho^c deski porozsycha^ly "+
        "si^e, robi^ac znaczne szpary, to wida^c, i^z na pewno b^edzie s^lu^zy^lo "+
        "jeszcze d^lugo. Pokryte jest ciemnobr^azow^a kap^a, kt^ora w niekt^orych "+
        "miejscach ma nieznaczne dziury wygryzione przez mole.\n");
    add_prop(OBJ_I_WEIGHT, 10000);
    add_prop(OBJ_I_VOLUME, 10000);
    add_prop(OBJ_I_VALUE, 340);
    ustaw_material(MATERIALY_DR_JESION, 85);
    ustaw_material(MATERIALY_TK_BAWELNA, 15);
    set_type(O_MEBLE);

    set_owners(({BIALY_MOST_NPC + "karczmarz_bialy_most"}));
}
