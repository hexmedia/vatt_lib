/* Autor: Duana
  Opis : Anthilie
  Data : 02.06.2007
*/

inherit "/std/object.c";

#include <pl.h>
#include <stdproperties.h>
#include <macros.h>
#include <filter_funs.h>
#include <materialy.h>
#include <object_types.h>
#include "dir.h"

create_object()
{
    ustaw_nazwe("szafka");
                 
    dodaj_przym("ma^ly","mali");
    dodaj_przym("drewniany","drewniani");
   
    set_long("Jest to ma^la drewniana szafka, kt^orej drewno kto^s "+
        "porysowa^l jakim^s ostrym narz^edziem. Utrzymuj^a j^a fiku^sne cztery "+
        "n^o^zki, rozchylaj^ace si^e nieco na boki. Za^s na blacie spoczywa "+
        "gruba warstwa kurzu, poprzez, kt^or^a nie wida^c zupe^lnie koloru "+
        "drewna.\n");
    add_prop(OBJ_I_WEIGHT, 5000);
    add_prop(OBJ_I_VOLUME, 5000);
    add_prop(OBJ_I_VALUE, 170);
    ustaw_material(MATERIALY_DR_DAB);
    set_type(O_MEBLE);

    set_owners(({BIALY_MOST_NPC + "karczmarz_bialy_most"}));
}
