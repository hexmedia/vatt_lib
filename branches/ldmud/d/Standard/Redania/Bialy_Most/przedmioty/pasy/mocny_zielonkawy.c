/* 
 * faeve, 13.06.07
 * opis znowu Anonima
 *
 */

inherit "/std/belt";
#include <macros.h>
#include <stdproperties.h>
#include <materialy.h>

create_belt()
{
    ustaw_nazwe("pas");
    dodaj_nazwy("pasek");
    dodaj_przym("mocny", "mocni");
    dodaj_przym("zielonkawy", "zielonkawi");
    
    set_long("Ten zrobiony z twardego materia^lu pas, nie powsta^l po to, by "+
		"ozdabia^c co ^ladniejsze kszta^lty, lecz po to, by solidnie m^og^l "+
		"odgrywac swoj^a rol^e. Posiada du^z^a, miedzian^a klamr^e, na "+
		"kt^orej po d^lu^zszym przypatrzeniu si^e, mo^zna dostrzec wij^ace "+
		"si^e linie, tak male^nskie, ^ze nie wiadomo po co kto^s je tam "+
		"umieszcza^l. Mocna sk^ora pofarbowana zosta^la na odcie^n, nieco "+
		"przypominaj^acy kolor zgni^lej zieleni, lecz mimo to, prezentuje "+
		"si^e przy tym ca^lkiem nie^xle. Pas jest na tyle d^lugi, ^ze "+
		"obejmie nie jedno cia^lo. \n");
    
    set_max_slots_zat(2);
    set_max_slots_prz(4);

    set_max_weight_zat(3400);
    set_max_weight_prz(2800);

    set_max_volume_zat(2400);
    set_max_volume_prz(2000);
    
    add_prop(OBJ_I_WEIGHT, 2800);
    add_prop(OBJ_I_VOLUME, 1550);

	add_prop(OBJ_I_VALUE, 1266);
    
    ustaw_material(MATERIALY_SK_BYDLE, 90);
    ustaw_material(MATERIALY_MIEDZ, 10);
}
