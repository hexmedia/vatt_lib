/* 
 * faeve, 13.06.07
 * opis: Oldzio
 *
 */

inherit "/std/belt";
#include <macros.h>
#include <stdproperties.h>
#include <materialy.h>

create_belt()
{
    ustaw_nazwe("pas");
    dodaj_nazwy("pasek");
    dodaj_przym("matowy", "matowi");
    dodaj_przym("czarny", "czarni");
    
    set_long("Zwyk^ly, skromny pasek, mieszcz^acy si^e zapewne w ka^zdej "+
		"szlufce, nadaje si^e do ka^zdego rodzaju spodni, sp^odnicy czy te^z "+
		"nawet sukienki. Jego matowa powierzchnia nie przyci^aga wzroku, "+
		"przez co staje si^e zupe^lnie neutralny, zupe^lnie jak nie "+
		"wyr^o^zniaj^aca si^e niczym szczeg^olnym - miedziana klamra. \n");
    
    set_max_slots_zat(1);
    set_max_slots_prz(2);
    set_max_weight_zat(2200);
    set_max_weight_prz(3000);
    set_max_volume_zat(2200);
    set_max_volume_prz(2000);
    
    add_prop(OBJ_I_WEIGHT, 842);
    add_prop(OBJ_I_VOLUME, 447);
	add_prop(OBJ_I_VALUE, 467);
    
    ustaw_material(MATERIALY_SK_CIELE, 90);
    ustaw_material(MATERIALY_MIEDZ, 10);
}
