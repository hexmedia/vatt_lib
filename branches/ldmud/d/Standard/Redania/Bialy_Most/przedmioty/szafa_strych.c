/* Autor: Avard
   Opis : Sniegulak
   Data : 20.06.07 */
inherit "/std/receptacle";

#include "/sys/stdproperties.h"
#include "/sys/macros.h"
#include "/sys/pl.h"
#include "/sys/materialy.h"
#include "/sys/object_types.h"
#include "dir.h"

void
create_receptacle()
{
    ustaw_nazwe("szafa");
    dodaj_przym("stary", "starzy");
    dodaj_przym("spr^ochnia^ly", "spr^ochniali");
    ustaw_material(MATERIALY_DR_DAB);
    set_type(O_MEBLE);
    set_long("Stara spr^ochnia^la szafa, nadjedzona z^ebem czasu jak i "+
        "setkami kornik^ow lata ^swietno^sci ma ju^z z pewno^sci^a za "+
        "sob^a. Drewno jest pociemnia^le i zakurzone, drzwiczki kiedy^s "+
        "zamykane na kluczyk, teraz ledwo co wisz^a na pojedy^nczych "+
        "zawiasach skorodowanych i prze^zartych rdz^a. P^o^lki w ^srodku "+
        "cz^e^sciowo po^lamane, na pewno nie s^a zdatne do u^zytku.\n");
	     
    add_prop(OBJ_I_VALUE, 10);
    add_prop(CONT_I_RIGID, 1);
    add_prop(CONT_I_MAX_VOLUME, 250000);
    add_prop(CONT_I_MAX_WEIGHT, 500000);
    add_prop(CONT_I_VOLUME, 10000);
    add_prop(CONT_I_WEIGHT, 20000);
    add_prop(CONT_I_NO_OPEN_DESC, 1);
    set_owners(({BIALY_MOST_NPC + "karczmarz_bialy_most"}));
}

