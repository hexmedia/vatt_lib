/*
 * Jest to skrzyneczka pochwiarza z Bia�ego Mostu!
 	Vera.
 */

inherit "/std/receptacle";
#include <materialy.h>
#include <object_types.h>
#include <pl.h>
#include <macros.h>
#include <stdproperties.h>
#include "dir.h"

string
czy_w_cos_jest()
{
	string str;
	
	if(!TO->query_prop(CONT_I_CLOSED))
	{
	
		if(strlen(TO->subloc_items()))
			str="Zawiera w sobie "+TO->subloc_items()+"\n";
		else
			str="Jest pusta.\n";
	}
	
	return str;
}

create_receptacle()
{
	ustaw_nazwe("skrzyneczka");
	dodaj_przym("kwadratowy","kwadratowi");
	ustaw_material(MATERIALY_DR_DAB);
    set_type(O_MEBLE);

	set_long("Kwadratowa, niska skrzynka zbita niedbale z paru "+
			"drewnianych deseczek.\n"+
			"@@opis_sublokacji|Dostrzegasz na niej |na|.\n||" + PL_BIE+ "@@"+
			"@@czy_w_cos_jest@@");
			
	add_prop(CONT_I_WEIGHT, 1543);
    add_prop(CONT_I_VOLUME, 631);
    add_prop(OBJ_I_VALUE, 27);
	add_prop(CONT_I_RIGID, 1);
    set_owners(({BIALY_MOST_NPC + "snycerz"}));

    add_subloc("na");
    add_subloc_prop("na", CONT_I_MAX_WEIGHT, 4000);
    add_subloc_prop("na", CONT_I_MAX_VOLUME, 1600);
    add_prop(CONT_I_DONT_SHOW_CONTENTS, 1);

}