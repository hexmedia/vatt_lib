inherit "/std/ryby/wedka";

#include <stdproperties.h>
#include <object_types.h>

void
create_wedka()
{
    ustaw_nazwe("wedka");

    dodaj_przym("d�ugi", "d�udzy");
    dodaj_przym("jasny", "ja�ni");

    set_long("Wykonana z jasnego drewna w�dka wygl�da na"
            +" porz�dnie wykonane narz�dzie. Drewno jest dok�adnie"
            +" wyg�adzone i pomalowane jak�� prze�roczyst�"
            +" substancj�, kt�ra najprawdopodobniej chroni je przed"
            +" dzia�aniem wody. R�czka w�dki owini�ta jest kawa�kiem"
            +" ciemnej sk�ry.\n");
            
    set_gietkosc(0.98);

    add_prop(OBJ_I_WEIGHT, 840);
    add_prop(OBJ_I_VOLUME, 632);
    add_prop(OBJ_I_VALUE, 405);
    set_type(O_WEDKARSKIE);
}
