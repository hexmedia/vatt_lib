inherit "/std/ryby/siec";

#include <pl.h>
#include <stdproperties.h>
#include <object_types.h>
void
create_siec()
{
    ustaw_nazwe(({"sie� rybacka", "sieci rybackiej", "sieci rybackiej",
		"sie� ryback�", "sieci� ryback�", "sieci rybackiej"}),
		({"sieci rybackie", "sieci rybackich", "sieciom rybackim",
		 "sieci rybackie", "sieciami rybackimi", "sieciach rybackich"}),
		PL_ZENSKI);

	dodaj_nazwy("sie�");

    dodaj_przym("szarawy", "szarawi");
    
    set_long("Spleciona z setek szarawych nici sie� wygl�da na"
	    +" ca�kiem wytrzyma��. Dodatkowo do jej ko�c�w"
	    +" przywi�zano grube liny, kt�re mog� by� przydatne"
	    +" przy wyci�ganiu sieci.\n");

    set_pojemnosc(10);
    set_min_time(16);
    set_max_time(28);

    add_prop(OBJ_I_WEIGHT, 1504);
    add_prop(OBJ_I_VOLUME, 4000);
    add_prop(OBJ_I_VALUE, 473);
    set_type(O_WEDKARSKIE);

}
