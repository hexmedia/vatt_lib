/* by Ohm*/
inherit "/std/ryby/wedka.c";
#include <wa_types.h>
#include <formulas.h>
#include <stdproperties.h>
#include <macros.h>
#include <materialy.h>
#include <object_types.h>
void
create_wedka()
{
    ustaw_nazwe("wedka");
    dodaj_przym("prosty", "pro^sci");
    dodaj_przym("u^zywany", "u^zywani");

    set_long("Pozbawiony kory, p^o^ltora^lokciowy patyk oraz przyczepiony do "+
		"niego stary sznurek do niego przeczepiony ^swiadcz^a o tym, i^z "+
		"w^edka ta zosta^la wykonana przez jakiego^s ma^lo zr^ecznego, "+
		"domoros^lego rzemie^slnika. Na lince mo^zna dostrzec niewielki "+
		"sp^lawik i kilka sup^l^ow, najprawdopodobniej zawi^azanych z powodu "+
		"jej przerwania. Patyk wygl^ada na do^s^c gi^etki by utrzyma^c "+
		"czterofuntow^a ryb^e, lecz przy po^lowie wi^ekszych okaz^ow - mo^ze "+
		"najzwyczajniej w ^swiecie p^ekn^a^c. \n");

     set_gietkosc(0.96);

     /* Wedka to kontener! (Rantaur) */
     add_prop(CONT_I_WEIGHT, 900);
     add_prop(CONT_I_VOLUME, 1200);
     add_prop(OBJ_I_VALUE, 253);
     set_type(O_WEDKARSKIE);

}
