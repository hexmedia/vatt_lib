inherit "/std/object";

#include <stdproperties.h>
#include <object_types.h>
void
create_object()
{
    ustaw_nazwe("haczyk");
    dodaj_przym("du�y", "duzi");
    dodaj_przym("�elazny", "�ela�ni");

    set_long("Haczyk ten ma kszta�t niewielkiej kotwiczki,"
	    +" cztery, zakrzywione ramiona wyrastaj�"
	    +" z jednego, do�� grubego, pr�cika. Na tego"
	    +" rodzaju haczyki zak�ada si� zazwyczaj"
	    +" wi�ksze przyn�ty, jak na przyk�ad m�ode"
	    +" rybki.\n");

    add_prop(OBJ_I_WEIGHT, 5);
    add_prop(OBJ_I_VOLUME, 2);
    add_prop(OBJ_I_VALUE, 6);
    set_type(O_WEDKARSKIE);
}

int query_haczyk()
{
    return 1;
}

/* Jakie przynety jestesmy w stanie zalozyc
 * na haczyk 
 */
string *query_przynety()
{
    return ({"rybka"});
}

