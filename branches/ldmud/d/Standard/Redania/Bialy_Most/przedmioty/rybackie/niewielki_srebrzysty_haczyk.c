inherit "/std/object";

#include <stdproperties.h>
#include <object_types.h>
void
create_object()
{
    ustaw_nazwe("haczyk");
    dodaj_przym("niewielki", "niewielcy");
    dodaj_przym("srebrzysty", "srebrzy�ci");

    set_long("Haczyk ma bardzo niewielkie rozmiary. Jeden"
	    +" koniec jest nieco sp�aszczony, tak aby"
	    +" mo�na by�o go �atwiej przywi�za� do sznurka"
	    +" w�dki, za� druga, zakrzywiona strona haczyka"
	    +" uwie�czona jest miniaturowym grotem, kt�ry"
	    +" zapobiega zsuwaniu sie przyn�ty. Na tego"
	    +" typu haczyki zazwyczaj zak�ada si� robaki"
	    +" lub te� kulki ciasta.\n");

    add_prop(OBJ_I_WEIGHT, 3);
    add_prop(OBJ_I_VOLUME, 1);
    add_prop(OBJ_I_VALUE, 3);
    set_type(O_WEDKARSKIE);

}

int query_haczyk()
{
    return 1;
}

/* Jakie przynety jestesmy w stanie zalozyc
 * na haczyk 
 */
string *query_przynety()
{
    return ({"chleb", "robak"});
}

