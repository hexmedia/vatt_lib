/*
 * �awka do �a�ni w zaje�dzie BM
 * opis by Sniegulak
 * zepsula faeve
 * dn. 21 maja 2007
 */

inherit "/std/object.c";

#include "dir.h"
#include <pl.h>
#include <stdproperties.h>
#include <macros.h>
#include <filter_funs.h>
#include <object_types.h>
#include <materialy.h>


create_object()
{
    ustaw_nazwe("^lawka");

    dodaj_przym("ma^ly","mali");
    dodaj_przym("drewniany","drewniani");

    make_me_sitable("na","na ^lawce","z ^lawki",2, PL_MIE,
"na");

    ustaw_material(MATERIALY_DR_SOSNA);

    set_long("^Lawka ta zrobiona jest z dw^och pie^nk^ow zbitych ze sob^a "+
		"szerok^a desk^a. Pomimo ma^lych rozmiar^ow jest wystarczaj^aca by "+
		"na niej do^s^c wygodnie przysi^a^s^c.\n");
    
    add_prop(OBJ_I_WEIGHT, 10000);
    add_prop(OBJ_I_VOLUME, 52513);
    add_prop(OBJ_I_VALUE, 269);
 // add_prop(OBJ_I_NO_GET, 1);
    set_owners(({BIALY_MOST_NPC + "karczmarz_bialy_most"}));
}

public int
jestem_lawka_lazni_BM()
{
        return 1;
}

public int
query_type()
{
    return O_MEBLE;
}
