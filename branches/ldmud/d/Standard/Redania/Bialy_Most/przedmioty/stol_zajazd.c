
#pragma strict_types

inherit "/std/container";

#include "dir.h"
#include <pl.h>
#include <macros.h>
#include <filter_funs.h>
#include <stdproperties.h>
#include <composite.h>
#include <cmdparse.h>
#include <object_types.h>
#include <materialy.h>

nomask void
create_container()
{
    ustaw_nazwe("st^o^l");
    dodaj_przym("mocny","mocni");
	    dodaj_przym("drewniany","drewniani");

    set_long("D^lugi na wysoko^s^c cz^lowieka i szeroki na dwa ^lokcie mocny, "+
		"d^ebowy st^o^l zosta^l starannie oszlifowany, tak, by nie "+
		"wystawa^ly ze^n ^zadne, nawet najmniejsze drzazgi. Na powierzchni "+
		"dostrzec mo^zna cieniutk^a warstewk^e smo^ly, kt^ora zapobiega "+
		"wypaczeniu drewna, przez nieostro^zne wylanie zawarto^sci kufli "+
		"czy buk^lak^ow. Blat sto^lu podtrzymuj^a dwie, mocne belki, "+
		"przymocowane, po przeciwleg^lych kra^ncach. Aby podnie^s^c, czy "+
		"nawet rzuci^c tym meblem potrzebna by by^la niebywa^la si^la.\n");


    ustaw_material(MATERIALY_DR_DAB);
    setuid();
    seteuid(getuid());
    add_prop(CONT_I_MAX_VOLUME, 20000);
    add_prop(CONT_I_VOLUME, 17800);

    add_prop(CONT_I_WEIGHT, 50000);
    add_prop(CONT_I_MAX_WEIGHT, 80000);

    add_prop(CONT_I_DONT_SHOW_CONTENTS, 1);

    add_subloc("na");
    set_owners(({BIALY_MOST_NPC + "karczmarz_bialy_most"}));

}

public int
query_type()
{
	return O_MEBLE;
}

