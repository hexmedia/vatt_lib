/**
 * Standard dla lokacji reda�skich
 *
 * @author Krun
 * @date Grudzie� 2007
 *
 * Ten plik nale�y BEZWZGL�DNIE inheritowa� w ka�dej z reda�skich lokacji.
 */

inherit "/std/room.c";

#include "dir.h"

string start_place;

public void
create_redania()
{
}

public void
reset_redania()
{
}

public nomask void
create_room()
{
    create_redania();
}

public nomask void
reset_room()
{
    reset_redania();
}

string
query_panstwo()
{
    return "Redania";
}

string
query_default_money()
{
    return "reda�skie";
}