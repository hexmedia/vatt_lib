/* Autor: Vera
   Opis : Praca zbiorowa ludzi z forum Zakonu, g��wnie Sniegulak
   Data : Tuesday 01 May 2007 */ 
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"
#include <pogoda.h>

inherit TRAKT_ZAKON_OKOLICE_STD;
#include "../std/eventy_sciezki.c"
void create_trakt() 
{
    set_short("w�ska, le�na �cie�ka");
    add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "sj08.c","e",0,SCIEZKA_ZO_FATIG,0);
	add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "sj10.c","nw",0,SCIEZKA_ZO_FATIG,0);
    add_prop(ROOM_I_INSIDE,0);
}
public string
exits_description() 
{
    return "�cie�yna ci�gnie si� z p�nocnego zachodu na wsch�d.\n";
}

string
dlugi_opis()
{
    string str = "";

    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="Bia�y puch przykrywaj�cy ro�liny jest do�� gruby i skrz"+
             "ypi pod krokami, a gdzieniegdzie mo�na zauwa�y� na nim dr"+
             "obne �lady ma�ych zwierz�t. Natura u�piona w czasie zimy"+
             ", jest cicha i spokojna, nie s�ycha� tutaj prawie �adnych"+
             " d�wi�k�w, a prawie wszystko przykryte jest grub� warstw"+
             "a czystego �niegu. Na p�nocy mo�na zobaczy� lekko prze"+
             "rzedzony lasek li�ciasty,a mi�dzy pniami drzew zauwa�asz "+
             "zarysy jeziora. ";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str+="Soczysta ziele�, i wszechobecne odg�osy zamieszkuj�cych t"+
             "en las zwierz�t, powoduj� i� w�dr�wka t� �cie�yn� j"+
             "est naprawd� przyjemna. W powietrzu unosz� sie stada ma�y"+
             "ch muszek, ptaki �piewaj� swoje piosenki, a flora rozkwita"+
             " na ka�dym kroku. Na p�nocy mo�na zobaczy� lekko przer"+
             "zedzony lasek li�ciasty,a mi�dzy pniami drzew zauwa�asz z"+
             "arysy jeziora. ";
    }
    else if(pora_roku() == MT_LATO)
    {
        str+="Soczysta ziele�, i wszechobecne odg�osy zamieszkuj�cych t"+
             "en las zwierz�t, powoduj� i� w�dr�wka t� �cie�yn� j"+
             "est naprawd� przyjemna. W powietrzu unosz� sie stada ma�y"+
             "ch muszek, ptaki �piewaj� swoje piosenki, a flora rozkwita"+
             " na ka�dym kroku. Na p�nocy mo�na zobaczy� lekko przer"+
             "zedzony lasek li�ciasty,a mi�dzy pniami drzew zauwa�asz z"+
             "arysy jeziora. ";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str+="Opad�e z drzew li�cie powoduj� i� na pod�o�u utworzy�"+
             " sie dywan szumi�cy i szeleszcz�cy pod ka�dym krokiem pod"+
             "r�nego. Pod stopami s�yszysz chrz�st patyk�w i szum su"+
             "chych opadni�tych li�ci. Na p�nocy mo�na zobaczy� lek"+
             "ko przerzedzony lasek li�ciasty,a mi�dzy pniami drzew zauw"+
             "a�asz zarysy jeziora. ";
    }

    str+="\n";
    return str;
}
string
opis_polmroku()
{
    string str;
	//Noo, po traktach si� chodzi z lampami!
	//I to o ka�dej porze roku! Nie wiedzieli�cie? :)

    if(CZY_JEST_SNIEG(this_object()))
        str="W mroku jeste� w stanie dostrzec jedynie biel �niegu "+
            "le��cego na �cie�ce.\n";
    else
        str="Nad �cie�k� zapad� mrok, dlatego nie jeste� w stanie dostrzec "+
            "zbyt wiele.\n";

    return str;
}
string
opis_nocy()
{
    string str="";

     str +="\n";
     return str;
}