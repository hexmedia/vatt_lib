/* Autor: Vera
   Opis : Praca zbiorowa ludzi z forum Zakonu, g��wnie Sniegulak
   Data : Tuesday 01 May 2007 */ 
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"
#include <pogoda.h>

inherit TRAKT_ZAKON_OKOLICE_STD;

void create_trakt() 
{
    set_short("Le�ny trakt");
    add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "t14.c","n",0,TRAKT_ZO_FATIG,0);
    add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "t12.c","se",0,TRAKT_ZO_FATIG,0);
    add_prop(ROOM_I_INSIDE,0);
}

public string
exits_description() 
{
    return "Trakt prowadzi z po�udniowego wschodu na p�noc.\n";
}

string
dlugi_opis()
{
    string str="";
	str+="Liczne suche ga��zie, kt�re pospada�y "+
	"z g�ry cz�sto tarasuj� drog� i utrudniaj� podr�. "+
	"Bujna ro�linno�� rozpo�ciera si� po obu stronach traktu, kt�ry "+
			"zdaje si� by� wprost obl�ony zieleni�, a "+
	"powietrze przesycone jest zapachem �ywicy i wilgotnego "+
	"igliwia. Wiatr przynosi do twych uszu ciche odg�osy "+
	"zamieszkuj�cych t� pot�n� puszcz� zwierz�t. Trakt jest do�� "+
	"szeroki, a gdzieniegdzie utworzy�y si� g��bokie koleiny "+
	"wy��obione przez przeje�d�aj�ce t�dy wozy.";

    str += "\n";
    return str;
}
string
opis_nocy()
{
    string str="";

     str +="\n";
     return str;
}