/* Autor: Vera
   Opis : Praca zbiorowa ludzi z forum Zakonu, g��wnie Sniegulak
   Data : Tuesday 01 May 2007 */ 
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"
#include <pogoda.h>

inherit TRAKT_ZAKON_OKOLICE_STD;

void create_trakt() 
{
    set_short("Trakt przy lesie");
	//HA! To jest moja ma�a anty autopogo�! ;) Minie troch� czasu, zanim
	//sobie to zatrygeruj� ;)
    add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "t09.c",
			({"n","traktem w g��b lasu","z po�udnia"}),0,TRAKT_ZO_FATIG,0);
    add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "t07.c",
			({"se","traktem na po�udniowy wsch�d",
			"z p�nocno-zachodniej cz�ci traktu"}),0,TRAKT_ZO_FATIG,0);
	//�cie�ka do jeziorka
	add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "sj01.c",
		({"sw","schodzi z traktu na �cie�k�","z traktu"}),0,TRAKT_ZO_FATIG,0);
    add_prop(ROOM_I_INSIDE,0);
}
public string
exits_description() 
{
    return "Trakt wiedzie z po�udniowego-wschodu na p�noc. Mo�na tu tak�e "+
		"zej�� na le�n� �cie�yn� biegn�c� na po�udniowy zach�d.\n";
}

//For dummies
zejdz(string str)
{
	notify_fail("Zejd� gdzie?\n");
	if(str ~= "na le�n� �cie�k�" || str~="na �cie�k�")
	{
		TP->command("sw");
		return 1;
	}
	return 0;
}
init()
{
	::init();
	add_action(zejdz,"zejd�");
}

string
dlugi_opis()
{
    string str="";
	if(CZY_JEST_SNIEG(TO))
		str+="Szlak ci�gnie si� tutaj dalej w kierunku p�nocnym. "+
		"Na zachodzie i p�nocy wida� g�sty las z wysokimi drzewami, kt�ry "+
		"przys�ania ca�y horyzont. Krzaki i zaro�la przedzielone s� "+
		"w�sk� �cie�k� biegn�c� w kierunku zachodnim. Udeptana stopami "+
		"wygl�da na do�� ruchliw� lecz nie a� tak by trzeba by�o j� "+
		"poszerza� dla woz�w. Trakt i okoliczne drzewa pokryte s� "+
		"�niegiem, kt�rego ilo�� mo�e sprawia� problemy podr�nym. "+
		"Wy��obiony ko�ami pojazd�w, i pe�en b�ota, na kt�rym wida� "+
		"tak�e �lady kopyt, podk�w i st�p wygl�da na do�� u�ywany. "+
		"W niekt�rych miejscach koleiny s� tak g��bokie, i� w�z jad�cy "+
		"t�dy musi bardzo zwolni� by nie z�ama� zawieszenia.";
	else
		str+="Szlak ci�gnie si� tutaj dalej w kierunku p�nocnym. "+
		"Na zachodzie i p�nocy wida� g�sty las z wysokimi drzewami, kt�ry "+
		"przys�ania ca�y horyzont. Krzaki i zaro�la przedzielone s� "+
		"w�sk� �cie�k� biegn�c� w kierunku zachodnim. Udeptana stopami "+
		"wygl�da na do�� ruchliw� lecz nie a� tak by trzeba by�o j� "+
		"poszerza� dla woz�w. Trakt jest wy��obiony ko�ami pojazd�w, "+
		"wida� na nim tak�e �lady kopyt i podk�w. W niekt�rych miejscach "+
		"koleiny s� tak g��bokie, i� w�z jad�cy t�dy musi bardzo zwolni� "+
		"by nie z�ama� zawieszenia.";


    str += "\n";
    return str;
}
string
opis_nocy()
{
    string str="";

     str +="\n";
     return str;
}