/* Autor: Vera
   Opis : Praca zbiorowa ludzi z forum Zakonu, g^l^ownie Sniegulak
   Data : Tuesday 01 May 2007 */

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"
#include <pogoda.h>
#include <mudtime.h>

inherit TRAKT_ZAKON_OKOLICE_STD;
#include "../std/eventy_sciezki.c"
void create_trakt()
{
    set_short("przy pomo�cie");
    add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "sj16.c","se",0,SCIEZKA_ZO_FATIG,0);
    add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "sj18.c","w",0,SCIEZKA_ZO_FATIG,0);
    add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "pomost.c","pomost", 0,SCIEZKA_ZO_FATIG,0);
    add_prop(ROOM_I_INSIDE,0);

    add_item(({"z^lamane drzewo","konar","konary"}),"@@konar@@");
    add_item(({"otw^or","wyrw^e","dziur^e"}),"@@otwor@@");
    add_item("jezioro","@@jezioro@@");
}
public string
exits_description()
{
    return "�cie^zyna ci�gnie si^e z zachodu na po^ludniowy wsch^od. Widnieje "+
        "tu tak^ze wej�cie na pomost.\n";
}

string
dlugi_opis()
{
    string str = "";
    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="W�ska wydeptana ludzkimi stopami �cie^zka prowadz�ca prze"+
            "z las wije si^e mi^edzy drzewami. G^esto rosn�ce praktycznie"+
            " ze wszystkich stron wysokie drzewa przejmuj� groz�, a pow"+
            "ykr^ecane krzewy i pnie dodaj� na niesamowito�ci otoczenia"+
            ". "+
            "Na p^o^lnocy, gdy podniesiemy wzrok ponad martwe i zmarz^le "+
            "konary i krzewy dostrze^zemy oblodzon� tafl^e rozleg^lego "+
            "jeziora, za� na po^ludniu swe miejsce znalaz^ly g^esto rosn�ce "+
            "drzewa, teraz przykryte �niegiem. "+
            "Bia^ly puch przykrywaj�cy ro�liny jest do�^c gruby i sk"+
            "rzypi pod krokami, a gdzieniegdzie mo^zna zauwa^zy^c na nim "+
            "drobne �lady ma^lych zwierz�t. Drzewa i okolica spowite s�"+
            " bia^lym puchem, a mocny mr^oz powoduje, ze �nieg skrzypi "+
            "pod butami. ";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str+="W�ska wydeptana ludzkimi stopami �cie^zka prowadz�ca prze"+
            "z las wije si^e mi^edzy drzewami. G^esto rosn�ce praktycznie"+
            " ze wszystkich stron wysokie drzewa przejmuj� groz�, a pow"+
            "ykr^ecane krzewy i pnie dodaj� na niesamowito�ci otoczenia"+
            ". "+
            "Na p^o^lnocy, za rzadk� �cian� ro�lin i krzew^ow rozpo�ciera "+
            "si^e widok na b^l^ekitn� to^n jeziora i ogromny, zwalony konar,"+
            " za� na po^ludniu okazale "+
            "prezentuje si^e zielona �ciana mieszanych drzew. "+
            "Soczysta ziele^n, i wszechobecne odg^losy zamieszkuj�cych"+
            " ten las zwierz�t, powoduj� i^z w^edr^owka t� �cie^zyn�"+
            " jest naprawd^e przyjemna. Wszystko dooko^la jest przyjemnie"+
            " zazielenione, a cisz^e panuj�c� w lesie od czasu do czasu"+
            " przerywa jaki� �wiergot ptaka. ";
    }
    else if(pora_roku() == MT_LATO)
    {
        str+="W�ska wydeptana ludzkimi stopami �cie^zka prowadz�ca prze"+
            "z las wije si^e mi^edzy drzewami. G^esto rosn�ce praktycznie"+
            " ze wszystkich stron wysokie drzewa przejmuj� groz�, a pow"+
            "ykr^ecane krzewy i pnie dodaj� na niesamowito�ci otoczenia"+
            ". "+
            "Na p^o^lnocy, za rzadk� �cian� ro�lin i krzew^ow rozpo�ciera "+
            "si^e widok na b^l^ekitn� to^n jeziora i ogromny, zwalony konar,"+
            " za� na po^ludniu okazale "+
            "prezentuje si^e zielona �ciana mieszanych drzew. "+
            "Soczysta ziele^n, i wszechobecne odg^losy zamieszkuj�cych"+
            " ten las zwierz�t, powoduj� i^z w^edr^owka t� �cie^zyn�"+
            " jest naprawd^e przyjemna. Wszystko dooko^la jest przyjemnie"+
            " zazielenione, a cisz^e panuj�c� w lesie od czasu do czasu"+
            " przerywa jaki� �wiergot ptaka. ";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str+="W�ska wydeptana ludzkimi stopami �cie^zka prowadz�ca prze"+
            "z las wije si^e mi^edzy drzewami. G^esto rosn�ce praktycznie"+
            " ze wszystkich stron wysokie drzewa przejmuj� groz�, a pow"+
            "ykr^ecane krzewy i pnie dodaj� na niesamowito�ci otoczenia"+
            ". "+
            "Na p^o^lnocy, za rzadk� �cian� ro�lin i krzew^ow rozpo�ciera "+
            "si^e widok na b^l^ekitn� to^n jeziora i ogromny, zwalony konar,"+
            " za� na po^ludniu okazale "+
            "prezentuje si^e zielona �ciana mieszanych drzew. "+
            "Opad^le z drzew li�cie powoduj� i^z na pod^lo^zu utworzy"+
            "^l sie dywan szumi�cy i szeleszcz�cy pod ka^zdym krokiem p"+
            "odr^o^znego. Suche opadni^ete na ziemi^e li�cie szeleszcz�"+
            " pod krokami, zwierz^eta szykuj� si^e do zimowego snu. ";
    }

    str+="\n";
    return str;
}
string
opis_polmroku()
{
    string str;
        //Noo, po traktach si^e chodzi z lampami!
        //I to o ka^zdej porze roku! Nie wiedzieli�cie? :)

    if(CZY_JEST_SNIEG(this_object()))
        str="W mroku jeste� w stanie dostrzec jedynie biel �niegu "+
            "le^z�cego na �cie^zce.\n";
    else
        str="Nad �cie^zk� zapad^l mrok, dlatego nie jeste� w stanie dostrzec "+
            "zbyt wiele.\n";

    return str;
}
string
opis_nocy()
{
    string str="";

    str +="\n";
    return str;
}

string
konar()
{
    string str;
    if(pora_roku() == MT_LATO)
    {
        str = "To z^lamane wp^o^l drzewo, jak^ze martwe i pos^epne, zdaje "+
            "si^e by^c tak naprawd^e istn^a ostoj^a okolicznej przyrody. "+
            "Konar zawis^l w ukosie nad ziemi^a, opieraj^ac si^e jednym "+
            "ko^ncem o pozosta^lo^s^c spruchnia^lego ju^z pnia, drugim za^s "+
            "g^l^eboko zakopuj^ac w ziemi. Liczne krzewy i ro^sliny "+
            "zaros^ly go wok^o^l, skrywaj^ac w^sr^od spolot^ow ga^l^ezi "+
            "niewielkie, ptasie gniazdo. ^Ow konar zdaje si^e te^z ukrywa^c "+
            "spor^a wyrw^e w gruncie, zaro^sni^etym paproci^a i "+
            "soczystozielonym mchem. \n";
    }
    if(pora_roku() == MT_WIOSNA)
    {
        str = "To z^lamane wp^o^l drzewo, jak^ze martwe i pos^epne, zdaje "+
            "si^e by^c tak naprawd^e istn^a ostoj^a okolicznej przyrody. "+
            "Konar zawis^l w ukosie nad ziemi^a, opieraj^ac si^e jednym "+
            "ko^ncem o pozosta^lo^s^c spruchnia^lego ju^z pnia, drugim "+
            "za^s g^l^eboko zakopuj^ac w ziemi. M^lode, wiosenne p^edy "+
            "korzystaj^a z jego podpory, wij^ac si^e wok^o^l, a w zamian "+
            "przyozdabiaj^ac ^swie^zym listowiem i kwieciem. ^Ow zdaje "+
            "si^e te^z ukrywa^c spor^a wyrw^e w gruncie, poro^sni^etym "+
            "niezlicznonymi k^epami ^snie^znobia^lych zawilc^ow.\n";
    }
    if(pora_roku() == MT_JESIEN)
    {
        str = "To z^lamane wp^o^l drzewo, jak^ze martwe i pos^epne, zdaje "+
            "si^e by^c tak naprawd^e istn^a ostoj^a okolicznej przyrody. "+
            "Konar zawis^l w ukosie nad ziemi^a, opieraj^ac si^e jednym "+
            "ko^ncem o pozosta^lo^s^c spruchnia^lego ju^z pnia, drugim za^s "+
            "g^l^eboko zakopuj^ac w ziemi. Tu i ^owdzie, z mi^ekkiej "+
            "^sci^o^lki wybijaj^a si^e ma^le, l^sni^ace od rosy grzyby. "+
            "Konar zdaje si^e te^z ukrywa^c spor^a wyrw^e w gruncie, "+
            "przykrytym dywanem r^o^znokolorwych li^sci. \n";
    }
    if(pora_roku() == MT_ZIMA)
    {
        str = "To z^lamane wp^o^l drzewo, jak^ze martwe i pos^epne, zdaje "+
            "si^e by^c tak naprawd^e istn^a ostoj^a okolicznej przyrody. "+
            "Konar zawis^l w ukosie nad ziemi^a, opieraj^ac si^e jednym "+
            "ko^ncem o pozosta^lo^s^c spruchnia^lego ju^z pnia, drugim za^s "+
            "g^l^eboko zakopuj^ac w ziemi. Konar zdaje si^e te^z ukrywa^c "+
            "spor^a wyrw^e w gruncie, pokrytym bia^l^a, snie^zn^a pierzyn^a."+
            "Liczne ^slady w puchu wskazuj^a na to, ^ze mimo zimowej pory "+
            "roku, wci^a^z t^etni tu ^zycie. \n";
    }
    return str;
}

string
otwor()
{
    string str;
    str = "Spora wyrwa w gruncie okazuje si^e by^c wielk^a, g^l^ebok^a "+
        "dziur^a, prowadz^ac^a w g^l^ab ziemi. Cz^esciowo, z g^ory, "+
        "przys^loni^eta konarem drzewa, zewsz^ad za^s ";
    if(pora_roku() == MT_ZIMA)
    {
        str += "zaspami ^sniegu ";
    }
    else
    {
        str += "g^estymi zaro^slami ";
    }
    str += "wcale nie jest tak widoczna na pierwszy rzut oka. Z pewno^sci^a "+
        "te^z nie zosta^la ukryta przypadkowo. \n";
    return str;
}

string
jezioro()
{
    string str;
    str = "Rozci^agaj^aca si^e na p^o^lnoc stalowoszara to^n jeziora "+
        "zdaje si^e by^c idealnie g^ladka i niezm^acona, jednak co^s raz "+
        "po raz narusza idealn^a harmoni^e, burz^ac g^ladk^a powierzchni^e "+
        "wody. Mimo to otaczaj^ace je drzewa przegl^adaj^a si^e we^n niczym "+
        "w czystym zwierciadle, zanurzaj^ac nieliczne ga^lezie, czy "+
        "korzenie w jej topieli. ";
    if(pora_roku() == MT_ZIMA)
    {
        str += "Ca^ly brzeg znieruchomia^l ca^lkowicie, pokrywaj^ac si^e "+
            "cienk^a wartw^a jasnob^l^ekitnego lodu, za^s po powierzchni "+
            "wody p^lywaj^a drobne od^lamki kry. ";
    }
    else
    {
        str += "Trzciny porastaj^ace brzeg poruszaj^a si^e lekko, "+
            "potrz^asaj^ac delikatnie wiechami, a tatarak kiwa monotonnie "+
            "w rytm ^swiszcz^acego wiatru. ";
    }
    str += "\n";
    return str;
}

void
wyjdz(object player)
{
    player->move(JASKINIA_LOKACJE +"jaskinia_01");
    player->do_glance(2);
    saybb(QCIMIE(TP,PL_MIA)+" przybywa z g^ory z g^lo^snym pluskiem "+
        "l^aduj^ac w m^etnej sadzawce. \n");
    return;
}

int
przejdz(string str)
{
    if(TP->query_prop(PLAYER_M_SIT_SIEDZACY) || TP->query_prop(PLAYER_M_SIT_LEZACY))
        return notify_fail("Musisz najpierw wsta�!\n");

    if(query_verb() ~= "przeci^snij")
    {
        notify_fail("Przeci^snij sie przez co?\n");

        if(!(str ~= "si^e przez otw^or" || str~="si^e przez dziur^e" ||
            str ~="si^e przez otw^or na d^o^l" ||
            str ~="si^e przez dziur^e na d^o^l" ||
            str ~= "sie na d^o^l"))
        {
            return 0;
        }
    }
    else if(query_verb() ~= "przejd^x")
    {
        notify_fail("Przejd^x przez co?\n");

        if(!(str ~= "przez otw^or" || str ~= "przez dziur^e"))
            return 0;
    }
    else if(query_verb() ~= "wejd�")
    {
        notify_fail("Wejd� gdzie?\n");

        if(!(str ~= "w otw�r" || str ~= "w dziur�"))
            return 0;
    }
    else
        return 0;

    object paraliz=clone_object("/std/paralyze");
    paraliz->set_fail_message("");
    paraliz->set_finish_object(this_object());
    paraliz->set_finish_fun("wyjdz");
    paraliz->set_remove_time(4);
    paraliz->setuid();
    paraliz->seteuid(getuid());
    paraliz->move(TP);
    write("Zaczynasz przeciska^c si^e przez otw^or.\n");
    saybb(QCIMIE(TP,PL_MIA)+" zaczyna przeciska^c si^e przez otw^or.\n");
    return 1;
}

void
init()
{
    ::init();
    add_action(przejdz, "przeci^snij");
    add_action(przejdz, "przejd^x");
    add_action(przejdz, "wejd�");
}
