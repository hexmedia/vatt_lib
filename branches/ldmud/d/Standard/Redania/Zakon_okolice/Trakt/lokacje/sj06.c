/* Autor: Vera
   Opis : Praca zbiorowa ludzi z forum Zakonu, g��wnie Sniegulak
   Data : Tuesday 01 May 2007 */ 
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"
#include <pogoda.h>

inherit TRAKT_ZAKON_OKOLICE_STD;
#include "../std/eventy_sciezki.c"
void create_trakt() 
{
    set_short("kr�ta �cie�ka");
    add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "sj05.c","ne",0,SCIEZKA_ZO_FATIG,0);
	add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "sj07.c","sw",0,SCIEZKA_ZO_FATIG,0);
    add_prop(ROOM_I_INSIDE,0);
}
public string
exits_description() 
{
    return "�cie�yna ci�gnie si� z po�udniowego zachodu na p�nocny wsch�d.\n";
}

string
dlugi_opis()
{
    string str = "";

    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="W�ska wydeptana ludzkimi stopami �cie�ka prowadz�ca prze"+
             "z las wije si� mi�dzy drzewami. Drzewa i okolica spowite s�"+
             " bia�ym puchem, a mocny mr�z powoduje, ze �nieg skrzypi "+
             "pod butami. Bia�y puch przykrywaj�cy ro�liny jest do�� "+
             "gruby i skrzypi pod krokami, a gdzieniegdzie mo�na zauwa�y"+
             "� na nim drobne �lady ma�ych zwierz�t. ";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str+="W�ska wydeptana ludzkimi stopami �cie�ka prowadz�ca prze"+
             "z las wije si� mi�dzy drzewami. Wszystko dooko�a jest przy"+
             "jemnie zazielenione, a cisz� panuj�c� w lesie od czasu do"+
             " czasu przerywa jaki� �wiergot ptaka. Soczysta ziele�, i "+
             "wszechobecne odg�osy zamieszkuj�cych ten las zwierz�t, po"+
             "woduj� i� w�dr�wka t� �cie�yn� jest naprawd� przyje"+
             "mna. ";
    }
    else if(pora_roku() == MT_LATO)
    {
        str+="W�ska wydeptana ludzkimi stopami �cie�ka prowadz�ca prze"+
             "z las wije si� mi�dzy drzewami. Wszystko dooko�a jest przy"+
             "jemnie zazielenione, a cisz� panuj�c� w lesie od czasu do"+
             " czasu przerywa jaki� �wiergot ptaka. Soczysta ziele�, i "+
             "wszechobecne odg�osy zamieszkuj�cych ten las zwierz�t, po"+
             "woduj� i� w�dr�wka t� �cie�yn� jest naprawd� przyje"+
             "mna. ";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str+="W�ska wydeptana ludzkimi stopami �cie�ka prowadz�ca prze"+
             "z las wije si� mi�dzy drzewami. Suche opadni�te na ziemi�"+
             " li�cie szeleszcz� pod krokami, zwierz�ta szykuj� si� d"+
             "o zimowego snu. Opad�e z drzew li�cie powoduj� i� na pod"+
             "�o�u utworzy� sie dywan szumi�cy i szeleszcz�cy pod ka�"+
             "dym krokiem podr�nego. ";
    }

    str+="\n";
    return str;
}
string
opis_polmroku()
{
    string str;
	//Noo, po traktach si� chodzi z lampami!
	//I to o ka�dej porze roku! Nie wiedzieli�cie? :)

    if(CZY_JEST_SNIEG(this_object()))
        str="W mroku jeste� w stanie dostrzec jedynie biel �niegu "+
            "le��cego na �cie�ce.\n";
    else
        str="Nad �cie�k� zapad� mrok, dlatego nie jeste� w stanie dostrzec "+
            "zbyt wiele.\n";

    return str;
}
string
opis_nocy()
{
    string str="";

     str +="\n";
     return str;
}