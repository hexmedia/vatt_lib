/* Autor: Vera
   Opis : Praca zbiorowa ludzi z forum Zakonu, g��wnie Sniegulak
   Data : Tuesday 01 May 2007 */ 
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"
#include <pogoda.h>

inherit TRAKT_ZAKON_OKOLICE_STD;

void create_trakt() 
{
    set_short("Trakt po�r�d ��k");
    add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "t03.c","n",0,TRAKT_ZO_FATIG,0);
    add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "t01.c","se",0,TRAKT_ZO_FATIG,0);
    add_prop(ROOM_I_INSIDE,0);
	add_event("Na pobliskim polu wyl�dowa� czarny bocian.\n");
}
public string
exits_description() 
{
    return "Trakt prowadzi z po�udniowego wschodu na p�noc.\n";
}

string
dlugi_opis()
{
    string str="";
	switch(pora_roku())
	{
		case MT_JESIEN:
		case MT_WIOSNA:
			str="Szeroki, zas�any z rzadka drobnymi kamykami trakt "+
			"ci�gnie si� �agodnymi �ukami z p�nocy na po�udnie. W "+
			"miernie utwardzonej, grz�skiej powierzchni drogi swoje "+
			"miejsce znalaz�y liczne koleiny - pozosta�o�ci po "+
			"przeje�d�aj�cych t�dy wozach, gdzieniegdzie da si� tak�e "+
			"zauwa�y� odciski podk�w, kt�re wraz z koleinami wype�ni�y "+
			"si� wod� tworz�c razem rozleg�e ka�u�e. Niska, polna "+
			"ro�linno��, przerzedza si� w miar� przybli�ania do "+
			"go�ci�ca, za� ziele� rosn�ca w najbli�szym s�siedztwie "+
			"drogi wydaje si� marna i niedojrza�a, a ju� na pewno zadeptana.";
			break;
		case MT_ZIMA:
			if(CZY_JEST_SNIEG(TO))
			str="Szeroki, ci�gn�cy si� z p�nocy na po�udnie trakt "+
			"zas�any przez �niegowy puch oraz brudne b�ocko. Na "+
			"oblodzonej, a gdzieniegdzie grz�skiej powierzchni drogi "+
			"trudno jest stawia� nogi, aby si� nie zapa�� w b�ocie, "+
			"czy nie po�lizgn��, a zapewne niejeden wo�nica, czy "+
			"je�dziec przekona� si� tak�e, �e r�wnie trudno jest "+
			"sterowa� w takich warunkach wozem lub wierzchowcem. "+
			"Po obu stronach go�ci�ca rozpo�cieraj� si� rozleg�e, "+
			"otwarte powierzchnie okryte w tej chwili grub�, �nie�n� pokryw�.";
			else
			str="Szeroki, zas�any z rzadka drobnymi kamykami trakt "+
			"ci�gnie si� �agodnymi �ukami z p�nocy na po�udnie. W "+
			"miernie utwardzonej, grz�skiej powierzchni drogi swoje "+
			"miejsce znalaz�y liczne koleiny - pozosta�o�ci po "+
			"przeje�d�aj�cych t�dy wozach, gdzieniegdzie da si� tak�e "+
			"zauwa�y� odciski podk�w, kt�re wraz z koleinami wype�ni�y "+
			"si� wod� tworz�c razem rozleg�e ka�u�e. Niska, polna "+
			"ro�linno��, przerzedza si� w miar� przybli�ania do "+
			"go�ci�ca, za� ziele� rosn�ca w najbli�szym s�siedztwie "+
			"drogi wydaje si� marna i niedojrza�a, a ju� na pewno zadeptana.";
			break;
		case MT_LATO:
			str="Szeroki, zas�any z rzadka drobnymi kamykami trakt "+
			"ci�gnie si� �agodnymi �ukami z p�nocy na po�udnie. Na "+
			"powierzchni drogi swoje miejsce znalaz�y liczne koleiny - "+
			"pozosta�o�ci po przeje�d�aj�cych t�dy wozach. Bujna, polna "+
			"ro�linno�� rozpo�ciera si� po obu stronach traktu, kt�ry "+
			"zdaje si� by� wprost obl�ony zieleni�. Tylko w najbli�szym "+
			"s�siedztwie utwardzonej drogi ro�liny nie potrafi� przetrwa� "+
			"i wybi� si� nad ziemie, a wyj�tki kt�rym to si� uda�o "+
			"zosta�y zadeptane przez licznych w�drowc�w korzystaj�cych z "+
			"tego traktu.";
			break;
	}

    str += "\n";
    return str;
}
string
opis_nocy()
{
    string str="";

     str +="\n";
     return str;
}