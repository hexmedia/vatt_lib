/* Autor: Vera
   Opis : Praca zbiorowa ludzi z forum Zakonu, g��wnie Sniegulak
   Data : Tuesday 01 May 2007 */ 
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"
#include <pogoda.h>

inherit TRAKT_ZAKON_OKOLICE_STD;
#include "../std/eventy_sciezki.c"
void create_trakt() 
{
    set_short("w�ska, le�na �cie�ka");
    add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "sj11.c","ne",0,SCIEZKA_ZO_FATIG,0);
	add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "sj13.c","nw",0,SCIEZKA_ZO_FATIG,0);
    add_prop(ROOM_I_INSIDE,0);
}
public string
exits_description() 
{
    return "�cie�yna ci�gnie si� z p�nocnego zachodu na p�nocny wsch�d.\n";
}

string
dlugi_opis()
{
    string str = "";

    if(CZY_JEST_SNIEG(this_object()))
    {
        str+="�cie�ka wydeptana przez stopy ludzkie jest nie szersza ni�"+
             " na naprawd� malutki w�z. Oblodzona �cie�ka wije si� f"+
             "alist� wst�g� ze wschodu na zach�d, a gdzie nie spojrze�"+
             " dooko�a zalega bia�y puch. G�sty las przez kt�ry prowad"+
             "zi w�ska �cie�ka, po kt�rej z du�ymi problemami porusza"+
             "�by si� nawet ma�ych rozmiar�w w�z, wygl�da na bardzo "+
             "stary. ";
    }
    else if(pora_roku() == MT_WIOSNA)
    {
        str+="�cie�ka wydeptana przez stopy ludzkie jest nie szersza ni�"+
             " na naprawd� malutki w�z. Le�na �cie�ka wije si� fali"+
             "st� wst�g� ze wschodu na zach�d, otoczona zewsz�d natur"+
             "�. G�sty las przez kt�ry prowadzi w�ska �cie�ka, po kt�"+
             "rej z du�ymi problemami porusza�by si� nawet ma�ych roz"+
             "miar�w w�z, wygl�da na bardzo stary. ";
    }
    else if(pora_roku() == MT_LATO)
    {
        str+="�cie�ka wydeptana przez stopy ludzkie jest nie szersza ni�"+
             " na naprawd� malutki w�z. Le�na �cie�ka wije si� fali"+
             "st� wst�g� ze wschodu na zach�d, otoczona zewsz�d natur"+
             "�. G�sty las przez kt�ry prowadzi w�ska �cie�ka, po kt�"+
             "rej z du�ymi problemami porusza�by si� nawet ma�ych roz"+
             "miar�w w�z, wygl�da na bardzo stary. ";
    }
    else //jesien i zima gdy nie ma sniegu
    {
        str+="�cie�ka wydeptana przez stopy ludzkie jest nie szersza ni�"+
             " na naprawd� malutki w�z. Le�na �cie�ka wije si� fali"+
             "st� wst�g� ze wschodu na zach�d, otoczona zewsz�d natur"+
             "�. G�sty las przez kt�ry prowadzi w�ska �cie�ka, po kt�"+
             "rej z du�ymi problemami porusza�by si� nawet ma�ych roz"+
             "miar�w w�z, wygl�da na bardzo stary. ";
    }

    str+="\n";
    return str;
}
string
opis_polmroku()
{
    string str;
	//Noo, po traktach si� chodzi z lampami!
	//I to o ka�dej porze roku! Nie wiedzieli�cie? :)

    if(CZY_JEST_SNIEG(this_object()))
        str="W mroku jeste� w stanie dostrzec jedynie biel �niegu "+
            "le��cego na �cie�ce.\n";
    else
        str="Nad �cie�k� zapad� mrok, dlatego nie jeste� w stanie dostrzec "+
            "zbyt wiele.\n";

    return str;
}
string
opis_nocy()
{
    string str="";

     str +="\n";
     return str;
}