/* Autor: Vera
   Opis : Praca zbiorowa ludzi z forum Zakonu, g��wnie Sniegulak
   Data : Tuesday 01 May 2007 */ 
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"
#include <pogoda.h>

inherit TRAKT_ZAKON_OKOLICE_STD;

void create_trakt() 
{
    set_short("Trakt przy lesie");
    add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "t08.c","nw",0,TRAKT_ZO_FATIG,0);
    add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "t06.c","se",0,TRAKT_ZO_FATIG,0);
    add_prop(ROOM_I_INSIDE,0);
	add_event("Na pobliskim polu wyl�dowa� czarny bocian.\n");
}
public string
exits_description() 
{
    return "Trakt ci�gnie si� z po�udniowego wschodu na p�nocny zach�d.\n";
}

string
dlugi_opis()
{
    string str="";

	str+="Trakt biegn�cy z miasta widocznego na po�udniu wij�c si� "+
	"mi�dzy niewielkimi polami i ��kami, usianymi na p�askim tutaj "+
	"terenie, przybli�a si� do skraju rozci�gaj�cego si� na p�nocy "+
	"wiekowego i ciemnego lasu. G��wna droga znika gdzie� na p�nocy "+
	"zag��biaj�c si� mi�dzy drzewami, lecz w oddali mo�na zauwa�y� "+
	"ma�� przesiek�, odbijaj�c� w kierunku zachodnim.";

    str += "\n";
    return str;
}
string
opis_nocy()
{
    string str="";

     str +="\n";
     return str;
}