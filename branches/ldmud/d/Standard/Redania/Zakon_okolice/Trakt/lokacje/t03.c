/* Autor: Vera
   Opis : Praca zbiorowa ludzi z forum Zakonu, g��wnie Sniegulak
   Data : Tuesday 01 May 2007 */ 
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"
#include <pogoda.h>

inherit TRAKT_ZAKON_OKOLICE_STD;

void create_trakt() 
{
    set_short("Szeroki trakt");
    add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "t04.c","nw",0,TRAKT_ZO_FATIG,0);
    add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "t02.c","s",0,TRAKT_ZO_FATIG,0);
    add_prop(ROOM_I_INSIDE,0);
	add_event("Na pobliskim polu wyl�dowa� czarny bocian.\n");
}
public string
exits_description() 
{
    return "Trakt wiedzie z po�udnia na p�nocny zach�d.\n";
}

string
dlugi_opis()
{
    string str="";

	switch(pora_roku())
	{
		case MT_JESIEN:
		case MT_WIOSNA:
			str="Szeroki, zas�any z rzadka drobnymi kamykami trakt "+
			"ci�gnie si� �agodnymi �ukami z p�nocy na po�udnie. Niska, polna "+
			"ro�linno��, przerzedza si� w miar� przybli�ania do "+
			"go�ci�ca, za� ziele� rosn�ca w najbli�szym s�siedztwie "+
			"drogi wydaje si� marna i niedojrza�a, a ju� na pewno zadeptana.";
			break;
		case MT_ZIMA:
			if(CZY_JEST_SNIEG(TO))
			str="Szeroki, ci�gn�cy si� z p�nocy na po�udnie trakt "+
			"zas�any przez �niegowy puch oraz brudne b�ocko. "+
			"Po obu stronach go�ci�ca rozpo�cieraj� si� rozleg�e, "+
			"otwarte powierzchnie okryte w tej chwili grub�, �nie�n� pokryw�.";
			else
			str="Szeroki, zas�any z rzadka drobnymi kamykami trakt "+
			"ci�gnie si� �agodnymi �ukami z p�nocy na po�udnie. Niska, polna "+
			"ro�linno��, przerzedza si� w miar� przybli�ania do "+
			"go�ci�ca, za� ziele� rosn�ca w najbli�szym s�siedztwie "+
			"drogi wydaje si� marna i niedojrza�a, a ju� na pewno zadeptana.";
			break;
		case MT_LATO:
			str="Szeroki, zas�any z rzadka drobnymi kamykami trakt "+
			"ci�gnie si� �agodnymi �ukami z p�nocy na po�udnie. "+
			"Tylko w najbli�szym "+
			"s�siedztwie utwardzonej drogi ro�liny nie potrafi� przetrwa� "+
			"i wybi� si� nad ziemie, a wyj�tki kt�rym to si� uda�o "+
			"zosta�y zadeptane przez licznych w�drowc�w korzystaj�cych z "+
			"tego traktu.";
			break;
	}

    str += "\n";
    return str;
}
string
opis_nocy()
{
    string str="";

     str +="\n";
     return str;
}