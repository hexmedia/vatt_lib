/* Autor: Vera
   Opis : Praca zbiorowa ludzi z forum Zakonu, g��wnie Sniegulak
   Data : Tuesday 01 May 2007 */ 
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"
#include <pogoda.h>

inherit TRAKT_ZAKON_OKOLICE_STD;

void create_trakt() 
{
    set_short("Na polnym trakcie");
    add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "t24.c","n",0,TRAKT_ZO_FATIG,0);
    add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "t22.c","se",0,TRAKT_ZO_FATIG,0);
    add_prop(ROOM_I_INSIDE,0);
}

public string
exits_description() 
{
    return "Trakt prowadzi z po�udniowego wschodu na p�noc.\n";
}

string
dlugi_opis()
{
    string str="";

	str+="W�ski, zryty koleinami trakt wije sie po�r�d �agodnych, "+
	"wzg�rz. Ca�a okolica usiana jest du�ymi kwadratami p�l uprawnych "+
	"oraz ��k, wykorzystywanych przez okolicznych mieszka�c�w.";

    str += "\n";
    return str;
}
string
opis_nocy()
{
    string str="";

     str +="\n";
     return str;
}