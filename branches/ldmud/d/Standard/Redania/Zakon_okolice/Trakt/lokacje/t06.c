/* Autor: Vera
   Opis : Praca zbiorowa ludzi z forum Zakonu, g��wnie Sniegulak
   Data : Tuesday 01 May 2007 */ 
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"
#include <pogoda.h>

inherit TRAKT_ZAKON_OKOLICE_STD;

void create_trakt() 
{
    set_short("Szeroki trakt");
    add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "t07.c","nw",0,TRAKT_ZO_FATIG,0);
    add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "t05.c","s",0,TRAKT_ZO_FATIG,0);
    add_prop(ROOM_I_INSIDE,0);
	add_event("Na pobliskim polu wyl�dowa� czarny bocian.\n");
}
public string
exits_description() 
{
    return "Trakt ci�gnie si� z po�udnia na p�nocny zach�d.\n";
}

string
dlugi_opis()
{
    string str="";

	str+="Droga �agodnie zakr�ca z po�udnia na p�nocny-zach�d, "+
	"biegn�c w kierunku ciemnego, starego lasu wyra�nie rysuj�cego "+
	"si� na p�nocy. Ca�a okolica jest pokryta ��kami i nigdzie nie "+
	"wida� �ladu po ludzkich osadach a jedynymi �ywymi osobami jakie "+
	"napotykasz s� podr�uj�cy ze swym towarem kupcy. ";

	if(CZY_JEST_SNIEG(TO))
		str+="Prawie wszystko przykryte jest spor� warstw� bielutkiego "+
		"�niegu, kt�ry przysypa� okolice. ";
	else if(pora_roku() == MT_WIOSNA)
		str+="Otaczaj�ca ci� przyroda budzi sie do �ycia po zimowym "+
		"�nie, a lekki wietrzyk przynosi do ciebie �wie�y zapach ro�lin. ";
	else if(pora_roku() == MT_LATO)
		str+="Ca�a przyroda w pe�nym rozkwicie, zieleni otaczaj�cy "+
		"ci� krajobraz, a w powietrzu mo�esz wyczu� intensywny zapach ro�lin. ";
	else if(pora_roku() == MT_JESIEN)
		str+="Kolory powoli zaczynaj� blakn��, przyroda zaczyna "+
		"przygotowywa� si� do zimowego snu. ";

    str += "\n";
    return str;
}
string
opis_nocy()
{
    string str="";

     str +="\n";
     return str;
}