/* Autor: Vera.
   Taki sobie pomost z ��dkami. :)
   Opis : Praca zbiorowa ludzi z forum Zakonu, g��wnie Sniegulak
   Data : Tuesday 13 May 2007 */ 
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <sit.h>
#include "dir.h"
#include <pogoda.h>

inherit "/std/room.c";
inherit "/std/ryby/lowisko";

void create_room() 
{
    set_short("na pomo�cie");
    add_exit(TRAKT_ZAKON_OKOLICE_LOKACJE + "sj17.c","sw",0,10,0);
    add_prop(ROOM_I_INSIDE,0);
	add_prop(ROOM_I_TYPE,ROOM_BEACH);
	add_event("Jaka� ryba rzuci�a si� w jeziorze.\n");
	add_event("Dochodzi ci^e nagle cichy plusk wody.\n");
	add_sit("na pomo�cie","na pomo�cie","z pomostu",0);

	 dodaj_lowisko("w rzece",
                        (["jazgarz":1.0, "ploc":1.5, "leszcz":1.0,
                         "ukleja":1.5, "okon":1.0, "szczupak":0.7]));
}
public string
exits_description() 
{
    return "�cie�yna ci�gnie si� gdzie� na po�udniowym-zachodzie.\n";
}

string
dlugi_opis()
{
    string str="";

	if(TP->query_prop(SIT_SIEDZACY))
		str+="Siedzisz ";
	else
		str+="Stoisz ";
	
	str+="na drewnianym pomo�cie kt�ry lekko wcina si� w tafl� jeziora. ";

	if(CZY_JEST_SNIEG(TO))
		str+="Powierzchnia jeziora, pokryta grub� warstw� lodu "+
		"i �wie�ego �niegu, odcina si� kolorystycznie od "+
		"szarozielonej lini drzew. Okoliczne zwierz�ta zapad�y ju�"+
		" najprawdopodobniej w zimowy sen, dlatego te� okolica jest "+
		"bardzo cicha i spokojna. ";
	else
		str+="Po powierzchni p�ywaj� kaczki i �ab�dzie, a ma�e "+
		"muszki lataj�ce w powietrzu stanowi� �er dla okolicznego "+
		"ptactwa. Od czasu do czasu g�adka powierzchnia jeziora "+
		"marszczy si� lekko pod podmuchami wiatru. ";

	str+="Drewniana konstrukcja pomostu s�u�y te� okolicznym rybakom "+
	"do przywi�zywania tu swoich ��dek, na kt�rych wyruszaj� w g��b "+
	"jeziora w poszukiwaniu ryb. ";

	

    str += "\n";
    return str;
}
string
opis_nocy()
{
    string str="";

     str +="\n";
     return str;
}








/*

ob pomost
Zbudowany z du�ych, nieociosanych bali pomost, wcina si� lekko w jezioro. Grube, wbite w dno podpory s� solidnie po��czone z pomostem zapewniaj�c tym samym stabilno�� konstrukcji tak, �e platforma ani drgnie pod wp�ywem twych krok�w. W kilku miejscach zauwa�asz do�� szerokie szpary, w kt�rych mo�e utkn�� stopa nieuwa�nej osoby. 1 albo 2

1.Do pomostu za pomoc� sznur�w, przywi�zanych jest kilka ma�ych ��deczek nale��cych do miejscowych rybak�w.

2.Do pomostu za pomoc� sznur�w zwykle przywi�zane s� ��dki nale��ce do miejscowych rybak�w, lecz w obecnej chwili wszystkie najprawdopodobniej wyp�yn�y na jezioro.

ob ��dki
Male wios�owe ��dki.

Zbudowane najprawdopodobniej przez jakiego� domoros�ego cie�le ��deczki,zbite s� z nieheblowanych desek, kt�re zosta�y z zewn�trz poci�gni�te smo��. By ��dki nie przecieka�y szpary miedzy deskami, wype�niono namoczonymi w smole szmatami. W poprzek przybito szeroka desk�, na kt�rej mo�na usi��� i zacz�� wios�owa�. Na burtach �odzi nie ma nic o co m�g�by� oprze� wystrugane z sosnowego drewna wios�a tak wiec poruszanie ich b�dzie do�� mozolne i pracoch�onne. Pokrycie �opat smo�� ma za zadanie nie pozwoli� wodzie na wnikni�cie w drewno. Konstrukcja s�abo wywa�ona chybocze sie znacznie na boki, co czasami powoduje wlewanie sie wody do �rodka. 


*/
