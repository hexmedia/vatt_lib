#include "dir.h"

inherit REDANIA_STD;

#include <stdproperties.h>
#include <macros.h>

string *zakazane = ({JASKINIA_LOKACJE+"jaskinia_02"});

#ifdef ZAWALY_WLACZONE
//losujemy kt^ore wyj�cia b^ed� zagruzowane:
string *zagruzowane1 = random(2) ? ({"05","02"}) : ({"05","07"});
string *zagruzowane2 = random(2) ? ({"02","03"}) : ({"04","07"});
string *zagruzowane3 = random(2) ? ({"06","10"}) : ({"07","11"});

int l = 0;

#endif ZAWALY_WLACZONE

int
query_jaskinia_zakon_okolice()
{
    return 2;
}

int
check(string dokad, string skad, int odwroc = 0)
{
    object last;
    if(TP->query_leader()) //w teamie robimy inaczej
    {
        //jesli leader prowadzi:
        if(file_name(ENV(TP->query_leader())) == JASKINIA_LOKACJE+"jaskinia_"+dokad)
            last = (TP->query_leader())->query_prop(LIVE_O_LAST_LAST_ROOM);
        else
            last = TP->query_prop(LIVE_O_LAST_ROOM);
    }
    else
    {
        last = TP->query_prop(LIVE_O_LAST_ROOM);
    }
    //Je�li nie ma wyj�cia to, hmm, losujemy domy�lny prz^od (Krun)
    if(!objectp(last))
    {
        string *wyjscia = TO->query_exit_rooms();

        last = find_object(wyjscia[random(sizeof(wyjscia))]);
    }
#ifdef 0
    write("sk�d "+ skad+" dok�d: "+dokad);
    string gdzie = explode(file_name(ENV(TP)),"_")[sizeof(explode(file_name(ENV(TP)),"_"))-1];
    write(" gdzie: "+gdzie+"\n");*/
#endif

#ifdef ZAWALY_WLACZONE
    //zawa^ly gruzu
    string sskad = explode(file_name(last),"_")[2];
    if((member_array(sskad,zagruzowane1) !=-1 && member_array(dokad,zagruzowane1) !=-1)||
        (member_array(sskad,zagruzowane2) !=-1 && member_array(dokad,zagruzowane2) !=-1)||
        (member_array(sskad,zagruzowane3) !=-1 && member_array(dokad,zagruzowane3) !=-1))
        {
        if(l)
        {
            write("Pot^e^zny zawa^l gruzu i innych odpadk^ow toruj�cych drog^e "+
                "uniemo^zliwia ci pod�^zanie w tym kierunku.\n");
            l = 0;
        }

        l = 1;
        return 2;
    }
#endif ZAWALY_WLACZONE

    if(odwroc)
    {
        if(member_array(file_name(last),zakazane) == -1)
            return 0;
        else
            return 2;
    }
    else
    {
        if(file_name(last) == JASKINIA_LOKACJE+"jaskinia_"+skad)
            return 0;
        else
            return 2;
    }
}

public void
create_jaskinia()
{
}

public nomask void
create_redania()
{
    set_start_place(0);
    create_jaskinia();
    add_sit(({"na ziemi","w wodzie","na kamieniach"}),
        "siadasz w brudnej, mulistej wodzie","z wody",0);
}