//inherit "/std/room";
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <pogoda.h>
#include "dir.h"
inherit JASKINIA_U_STD;


void
create_jaskinia()
{
    set_short("Du^za grota");
    add_prop(ROOM_I_INSIDE,1);
    //add_exit(JASKINIA_LOKACJE + "jaskinia_02.c","n",0,1,0);
	add_exit(JASKINIA_LOKACJE+"jaskinia_06.c",({"ty^l","do ty^lu",
		   "z jednego z korytarzy" 	}), &check("06","06"),J_FA,&check("06","06"));
	add_exit(JASKINIA_LOKACJE+"jaskinia_09.c",({"ty^l","do ty^lu",
		   "z jednego z korytarzy" 	}), &check("09","09"),J_FA,&check("09","09"));

	add_exit(JASKINIA_LOKACJE+"jaskinia_06.c",({"lewo","w lewo",
		   "z jednego z korytarzy" 	}), &check("06","09"),J_FA,&check("06","09"));


	add_exit(JASKINIA_LOKACJE+"jaskinia_09.c",({"prawo","w prawo",
		   "z jednego z korytarzy" 	}), &check("09","06"),J_FA,&check("09","06"));


    add_prop(ROOM_I_LIGHT, 0);
    add_item("^sciany",
        "^Sciany, wy^z^lobione z biegiem lat, przez zalegaj^ac^a tutaj "+
        "prze^smierd^l^a wod^e, pokryte s^a dziwn^a ro^slinno^sci^a "+
        "przypominaj^ac^a glony. Nasi^akni^ete wod^a i zwieszaj^ace si^e "+
        "ze stropu, wygladaj^a na wyj^atkowo o^slizg^le i niemi^le w "+
        "dotyku. \n");

    add_item("szcz^atki",
        "Te szcz^atki, unosz^ace si^e na powierzchni wody, przypominaj^a "+
        "kszta^ltem, barw^a, faktur^a, czy te^z po prostu smrodem, resztki "+
        "ubra^n, buty, deski, a nawet co^s, na wzr^or cia^la jakiego^s "+
        "nieszcz^e^sliwego badacza owej jaskini. Zalegaj^ace gdzieniegdzie "+
        "truch^la drobnych zwierz^at pot^eguj^a wo^n padliny. \n");


        set_event_time(250.0+itof(random(100)));
    add_event("Jaki^s obrzydliwy zapach zdaje si^e dobiega^c z g^l^ebi "+
			"jaskini.\n");
    add_event("Kilka kropel wody spad^lo z sufitu. \n");
    add_event("Powierzchnia wody zafalowa^la lekko. \n");

}

string
dlugi_opis()
{
    string str;
    str = "W^aski, niski korytarz w tym miejscu skr^eca ^lagodnie, po czym "+
        "niknie powoli w mroku. Jego sciany s^a go^le od glon^ow i "+
        "ro^slinno^sci, za to przyzdobione niezwyk^lymi rze^xbieniami, "+
        "wydr^a^zonymi przez krople wody, ^sciekaj^ace ze stropu i "+
        "zwieszaj^acych si^e ze^n stalaktyt^ow. Pod^lo^ze ginie pod "+
        "warstw^a mu^lu i b^lota, w kt^orym utkwi^ly wszelkie szcz^atki, "+
        "^smieci i zniszczone przedmioty. \n";

    return str;
}
