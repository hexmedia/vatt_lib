//inherit "/std/room";
#include "dir.h"
inherit JASKINIA_U_STD;
#include <stdproperties.h>
#include <macros.h>
#include <ss_types.h>
#include <pl.h>
#include <object_types.h>
string dlugi_opis();

#define CIUCHY "/d/Standard/Redania/Piana/quest/ciuchy"

void
create_jaskinia()
{
    set_short("Zalane wej^scie");
    set_long("@@dlugi_opis@@");

    add_exit("jaskinia_07.c",({"ty�","do ty�u","z jednego z korytarzy"}),&check("07","07"),
		    			J_FA,&check("07","07"));


    add_item("otw^or",
        "Ta szczelina w stropie jaskini jest dosy^c kr^otka i w^aska, "+
        "raczej nie wygl^ada na tak^a, przez kt^or^a mo^zna by si^e by^lo "+
        "przecisn^a^c. Najwyra^xniej prowadzi na zewn^atrz, na co wskazuj^a "+
        "lekkie powiewy ^swie^zego powietrza.\n");

    add_item("^sciany",
        "^Sciany, wy^z^lobione z biegiem lat przez zalegaj^ac^a tutaj "+
        "prze^smierd^l^a wod^e, pokryte s^a dziwn^a ro^slinno^sci^a "+
        "przypominaj^ac^a glony. Nasi^akni^ete wod^a i zwieszaj^ace si^e "+
        "ze stropu, wygladaj^a na wyj^atkowo o^slizg^le i niemi^le w "+
        "dotyku. \n");

    add_item("szcz^atki",
        "Te szcz^atki, unosz^ace si^e na powierzchni wody, przypominaj^a "+
        "kszta^ltem, barw^a, faktur^a, czy te^z po prostu smrodem, resztki "+
        "ubra^n, buty, deski, a nawet co^s na wz^or cia^la jakiego^s "+
        "nieszcz^e^sliwego badacza owej jaskini, w tej chwili b^ed^acego raczej " +
        "kup^a starych, pognitych ubra^n. Zalegaj^ace gdzieniegdzie "+
        "truch^la drobnych zwierz^at pot^eguj^a wo^n padliny. \n");


        set_event_time(250.0+itof(random(100)));
    add_event("Powiew ^swie^zego powietrza dochodzi ci^e od "+
			"strony stropu.\n");
    add_event("Jaki^s obrzydliwy zapach zdaje si^e dobiega^c z g^l^ebi "+
			"jaskini.\n");
    add_event("Kilka kropel wody spad^lo z sufitu. \n");
    add_event("Powierzchnia wody zafalowa^la lekko. \n");

    add_prop(ROOM_I_INSIDE,1);
    remove_prop(ROOM_I_LIGHT);
    add_prop(ROOM_S_DARK_LONG,"W ciemnym korytarzu. \n");

    add_object(CIUCHY);
    dodaj_rzecz_niewyswietlana("kupa starych pognitych ubra^n", 0);
}

string
dlugi_opis()
{
    string str;

    str="Naturalna jaskinia wydr^a^zona w wapiennej skale okazuje si^e ci^agn^a^c "+
        "dalej w jednym tylko kierunku. Zalana m^etn^a wod^a na mniej wi^ecej "+
        "trzy stopy zdaje si^e by^c w tym miejscu jedynie korytarzem. Do^s^c "+
        "szeroka by min^e^ly si^e w niej trzy osoby, oraz wysoka na sze^s^c "+
        "^lokci pozwala na do^s^c du^z^a swobod^e ruch^ow. Woda zalewaj^aca "+
        "korytarz jest brudna i mulista, a po powierzchni p^lywaj^a jakie^s "+
        "szcz^atki bli^zej nieokre^slonego pochodzenia, ale niepokoj^aco podobne " +
        "kszta^ltem do cia^la. ^Sciany pokryte "+
        "^sluzowatymi ro^slinami s^a niemi^le w dotyku, za^s przez strop biegnie "+
        "w^aski otw^or.\n";
    return str;
}

