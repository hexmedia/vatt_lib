//inherit "/std/room";
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <pogoda.h>
#include "dir.h"
inherit JASKINIA_U_STD;

void
dodaj_wyjscia_na_07()
{
//	remove_exit("ty^l");
//	remove_exit("lewo");
//	remove_exit("prawo");
	add_exit(JASKINIA_LOKACJE+"jaskinia_07.c",({"ty^l","do ty^lu",
		   "z jednego z korytarzy" 	}), &check("07","07"),J_FA,&check("07","07"));
	add_exit(JASKINIA_LOKACJE+"jaskinia_07.c",({"prawo","w prawo",
		   "z jednego z korytarzy" 	}), &check("07","09"),J_FA,&check("07","09"));
/*
	ugly_update_action("ty^l",1);
	ugly_update_action("prawo",1);*/
}




void
create_jaskinia()
{
    set_short("Du^za grota");
    add_prop(ROOM_I_INSIDE,1);
	add_exit(JASKINIA_LOKACJE+"jaskinia_09.c",({"ty^l","do ty^lu",
		   "z jednego z korytarzy" 	}), &check("09","09"),J_FA,&check("09","09"));
	add_exit(JASKINIA_LOKACJE+"jaskinia_09.c",({"lewo","w lewo",
		   "z jednego z korytarzy" 	}), &check("09","07"),J_FA,&check("09","07"));

    add_prop(ROOM_I_LIGHT, 0);
    add_item("^sciany",
        "^Sciany, wy^z^lobione z biegiem lat, przez zalegaj^ac^a tutaj "+
        "prze^smierd^l^a wod^e, pokryte s^a dziwn^a ro^slinno^sci^a "+
        "przypominaj^ac^a glony. Nasi^akni^ete wod^a i zwieszaj^ace si^e "+
        "ze stropu, wygladaj^a na wyj^atkowo o^slizg^le i niemi^le w "+
        "dotyku. \n");

    add_item("szcz^atki",
        "Te szcz^atki, unosz^ace si^e na powierzchni wody, przypominaj^a "+
        "kszta^ltem, barw^a, faktur^a, czy te^z po prostu smrodem, resztki "+
        "ubra^n, buty, deski, a nawet co^s, na wzr^or cia^la jakiego^s "+
        "nieszcz^e^sliwego badacza owej jaskini. Zalegaj^ace gdzieniegdzie "+
        "truch^la drobnych zwierz^at pot^eguj^a wo^n padliny. \n");


        set_event_time(250.0+itof(random(100)));
    add_event("Jaki^s obrzydliwy zapach zdaje si^e dobiega^c z g^l^ebi "+
			"jaskini.\n");
    add_event("Kilka kropel wody spad^lo z sufitu. \n");
    add_event("Powierzchnia wody zafalowa^la lekko. \n");

}

string
dlugi_opis()
{
    string str;
    str = "Korytarz ko^nczy si^e w tym miejscu pust^a ^scian^a, po kt^orej "+
        "raz po raz ^sciekaj^a krople wody, rze^xbi^ace we^n z biegiem "+
        "czasu, dziwne wzory. Zwieszaj^ace si^e ze sklepienia stalaktyty "+
        "niemal si^egaj^a ziemi, tworz^ac osobliwe, wapienne kolumny. "+
        "Jasknia tonie na dwie stopy w mulistej, cuchn^acej padlin^a "+
        "wodzie, pe^ln^a szcz^atek, desek i ^smieci, a nawet zniszczonej "+
        "broni.  \n";

    return str;
}
