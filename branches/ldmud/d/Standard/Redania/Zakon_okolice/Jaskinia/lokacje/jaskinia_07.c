//inherit "/std/room";
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <pogoda.h>
#include "dir.h"
inherit JASKINIA_U_STD;
void
dodaj_wyjscia_na_05()
{
//	remove_exit("ty�");
//	remove_exit("prz�d");
//	remove_exit("lewo");
//	remove_exit("prawo");
	add_exit(JASKINIA_LOKACJE+"jaskinia_05.c",({"ty�","do ty�u",
		   "z jednego z korytarzy" 	}), &check("05","05"),J_FA,&check("05","05"));
	add_exit(JASKINIA_LOKACJE+"jaskinia_05.c",({"lewo","w lewo",
		   "z jednego z korytarzy" 	}), &check("05","04"),J_FA,&check("05","04"));
	add_exit(JASKINIA_LOKACJE+"jaskinia_05.c",({"prz�d","do przodu",
		   "z jednego z korytarzy" 	}), &check("05","08"),J_FA,&check("05","08"));
	add_exit(JASKINIA_LOKACJE+"jaskinia_05.c",({"prawo","w prawo",
		   "z jednego z korytarzy" 	}), &check("05","11"),J_FA,&check("05","11"));
/*
	ugly_update_action("ty�",1);
	ugly_update_action("lewo",1);
	ugly_update_action("prawo",1);
	ugly_update_action("prz�d",1);*/
}

void
dodaj_wyjscia_na_04()
{
//	remove_exit("ty�");
//	remove_exit("prz�d");
//	remove_exit("lewo");
//	remove_exit("prawo");

	add_exit(JASKINIA_LOKACJE+"jaskinia_04.c",({"prz�d","do przodu",
		   "z jednego z korytarzy" 	}), &check("04","11"),J_FA,&check("04","11"));
	add_exit(JASKINIA_LOKACJE+"jaskinia_04.c",({"ty�","do ty�u",
		   "z jednego z korytarzy" 	}), &check("04","04"),J_FA,&check("04","04"));
	add_exit(JASKINIA_LOKACJE+"jaskinia_04.c",({"lewo","w lewo",
		   "z jednego z korytarzy" 	}), &check("04","08"),J_FA,&check("04","08"));
	add_exit(JASKINIA_LOKACJE+"jaskinia_04.c",({"prawo","w prawo",
		   "z jednego z korytarzy" 	}), &check("04","05"),J_FA,&check("04","05"));

// 	ugly_update_action("ty�",1);
// 	ugly_update_action("lewo",1);
// 	ugly_update_action("prawo",1);
// 	ugly_update_action("prz�d",1);
}



void
dodaj_wyjscia_na_11()
{
//	remove_exit("ty�");
//	remove_exit("prz�d");
//	remove_exit("lewo");
//	remove_exit("prawo");

	add_exit(JASKINIA_LOKACJE+"jaskinia_11.c",({"prz�d","do przodu",
		   "z jednego z korytarzy" 	}), &check("11","04"),J_FA,&check("11","04"));
	add_exit(JASKINIA_LOKACJE+"jaskinia_11.c",({"ty�","do ty�u",
		   "z jednego z korytarzy" 	}), &check("11","11"),J_FA,&check("11","11"));
	add_exit(JASKINIA_LOKACJE+"jaskinia_11.c",({"lewo","w lewo",
		   "z jednego z korytarzy" 	}), &check("11","05"),J_FA,&check("11","05"));
	add_exit(JASKINIA_LOKACJE+"jaskinia_11.c",({"prawo","w prawo",
		   "z jednego z korytarzy" 	}), &check("11","08"),J_FA,&check("11","08"));
/*
	ugly_update_action("ty�",1);
	ugly_update_action("lewo",1);
	ugly_update_action("prawo",1);
	ugly_update_action("prz�d",1);*/
}



void
create_jaskinia()
{
    set_short("Du^za grota");
    add_prop(ROOM_I_INSIDE,1);

	add_exit(JASKINIA_LOKACJE+"jaskinia_08.c",({"prz�d","do przodu",
		   "z jednego z korytarzy" 	}), &check("08","05"),J_FA,&check("08","05"));
	add_exit(JASKINIA_LOKACJE+"jaskinia_08.c",({"ty�","do ty�u",
		   "z jednego z korytarzy" 	}), &check("08","08"),J_FA,&check("08","08"));
	add_exit(JASKINIA_LOKACJE+"jaskinia_08.c",({"lewo","w lewo",
		   "z jednego z korytarzy" 	}), &check("08","11"),J_FA,&check("08","11"));
	add_exit(JASKINIA_LOKACJE+"jaskinia_08.c",({"prawo","w prawo",
		   "z jednego z korytarzy" 	}), &check("08","04"),J_FA,&check("08","04"));

    add_prop(ROOM_I_LIGHT, 0);
    add_item("^sciany",
        "^Sciany, wy^z^lobione z biegiem lat, przez zalegaj^ac^a tutaj "+
        "prze^smierd^l^a wod^e, pokryte s^a dziwn^a ro^slinno^sci^a "+
        "przypominaj^ac^a glony. Nasi^akni^ete wod^a i zwieszaj^ace si^e "+
        "ze stropu, wygladaj^a na wyj^atkowo o^slizg^le i niemi^le w "+
        "dotyku. \n");

    add_item("szcz^atki",
        "Te szcz^atki, unosz^ace si^e na powierzchni wody, przypominaj^a "+
        "kszta^ltem, barw^a, faktur^a, czy te^z po prostu smrodem, resztki "+
        "ubra^n, buty, deski, a nawet co^s, na wzr^or cia^la jakiego^s "+
        "nieszcz^e^sliwego badacza owej jaskini. Zalegaj^ace gdzieniegdzie "+
        "truch^la drobnych zwierz^at pot^eguj^a wo^n padliny. \n");


        set_event_time(250.0+itof(random(100)));
    add_event("Jaki^s obrzydliwy zapach zdaje si^e dobiega^c z g^l^ebi "+
			"jaskini.\n");
    add_event("Kilka kropel wody spad^lo z sufitu. \n");
    add_event("Powierzchnia wody zafalowa^la lekko. \n");

}

string
dlugi_opis()
{
    string str;
    str = "Korytarz ko^nczy si^e w tym miejscu ^scian^a "+
        "pokryt^a jakimi^s glonami. Woda zalewaj^aca przej^scie "+
        "jest brudna i mulista, a po powierzchni p�ywaj^a jakie^s "+
        "szcz^atki, kt^orych nie mo^zesz rozpozna^c. ^Sciany pokryte "+
        "^sluzowatymi ro^slinami s^a niemi^le w dotyku i dodatkowo "+
        "ociekaj^a wod^a, ^z^lobi^ac^a w nich przedziwne formacje skalne. \n";

    return str;
}
