#define REDANIA "/d/Standard/Redania/"
#define ZIOLA "/d/Standard/ziola/"
#define UBRANIA_GLOBAL "/d/Standard/items/ubrania/"

#define DYLEK_PIANA_RINDE (REDANIA+"dylek_piana_rinde/")

#define RINDE                           (REDANIA + "Rinde/")
#define RINDE_NPC                       (RINDE + "npc/")

#define POLUDNIE                        (RINDE + "Poludnie/")
#define POLUDNIE_LOKACJE                (POLUDNIE + "lokacje/")

#define TRAKT_RINDE                     (REDANIA + "Rinde_okolice/Trakt/")
#define TRAKT_RINDE_LOKACJE             (TRAKT_RINDE + "lokacje/")
#define TRAKT_RINDE_NPC                 (TRAKT_RINDE + "npc/")

#define BIALY_MOST                      (REDANIA + "Bialy_Most/")

#define BIALY_MOST_LOKACJE              (BIALY_MOST + "lokacje/")
#define BIALY_MOST_LOKACJE_ULICE        (BIALY_MOST_LOKACJE+"ulice/")

#define BIALY_MOST_PRZEDMIOTY           (BIALY_MOST+"przedmioty/")
#define BIALY_MOST_BRONIE               (BIALY_MOST_PRZEDMIOTY + "bronie/")
#define BIALY_MOST_ZBROJE               (BIALY_MOST_PRZEDMIOTY + "zbroje/")
#define BIALY_MOST_UBRANIA              (BIALY_MOST + "przedmioty/ubrania/")
#define RINDE_UBRANIA                   (RINDE + "przedmioty/ubrania/")

#define BIALY_MOST_OKOLICE              (REDANIA + "Bialy_Most_okolice/")
#define TRAKT_BIALY_MOST_OKOLICE        (BIALY_MOST_OKOLICE+"Trakt/")
#define TRAKT_BIALY_MOST_OKOLICE_LOKACJE    (TRAKT_BIALY_MOST_OKOLICE + "lokacje/")
#define BRZEG_BIALY_MOST_OKOLICE        (BIALY_MOST_OKOLICE+"Brzeg/")
#define BRZEG_BIALY_MOST_LOKACJE        (BRZEG_BIALY_MOST_OKOLICE+"lokacje/")

#define WIOSKA_BIALY_MOST_OKOLICE       (REDANIA+"Wioska_BM/")
#define WIOSKA_BIALY_MOST_OKOLICE_LOKACJE (WIOSKA_BIALY_MOST_OKOLICE+"lokacje/")

#define KANALY                          (REDANIA + "Rinde_kanaly/")
#define KANALY_LOKACJE                  (KANALY + "lokacje/")
#define KANALY_DRZWI                    (KANALY + "drzwi/")
#define KANALY_OBIEKTY                  (KANALY + "obiekty/")

#define ZAKON                           (REDANIA + "Zakon/")
#define ZAKON_LOKACJE                   (ZAKON + "lokacje/")
#define ZAKON_DRZWI                     (ZAKON + "drzwi/")

#define ZAKON_OKOLICE                   (REDANIA+"Zakon_okolice/")
#define TRAKT_ZAKON_OKOLICE             (ZAKON_OKOLICE+"Trakt/")
#define TRAKT_ZAKON_OKOLICE_LOKACJE     (TRAKT_ZAKON_OKOLICE+"lokacje/")

#define TRETOGOR                        (REDANIA + "Tretogor/")
#define TRETOGOR_LOKACJE                (TRETOGOR + "lokacje/")
#define TRETOGOR_DRZWI                  (TRETOGOR + "drzwi/")

#define PIANA                           (REDANIA + "Piana/")
#define PIANA_LOKACJE                   (PIANA + "lokacje/")
#define PIANA_NPC                       (PIANA + "npc/")
#define PIANA_LOKACJE_ULICE             (PIANA_LOKACJE + "ulice/")
#define PIANA_OKOLICE                   (REDANIA + "Piana_okolice/")
#define PIANA_OKOLICE_TRAKT             (PIANA_OKOLICE + "Trakt/")
#define PIANA_OKOLICE_TRAKT_LOKACJE     (PIANA_OKOLICE_TRAKT + "lokacje/")

#define PIANA_BAGNO                     (REDANIA + "Piana_bagno/")
#define PIANA_BAGNO_LOKACJE             (PIANA_BAGNO + "lokacje/")

#define MURIVEL                         (REDANIA + "Murivel/")
#define MURIVEL_ZACHOD                  (MURIVEL + "Zachod/")
#define MURIVEL_ZACHOD_ULICE            (MURIVEL_ZACHOD + "ulice/")

#define REDANIA_STD                     (REDANIA + "std/redania.c")

//Ustawiamy og�lny reda�ski SLEEP_PLACE
#ifdef SLEEP_PLACE
#undef SLEEP_PLACE
#endif SLEEP_PLACE

#define SLEEP_PLACE                     (RINDE + "Polnoc/Karczma/lokacje/start.c")


//quest zielarza z bagien piany
#define QUESTY                 "/d/Standard/questy/"
#define ZIELARZ_ZROBILI                 QUESTY + "zielarz/zrobili"
#define ZIELARZ_ROBIACY                 QUESTY + "zielarz/robiacy"
