/* Autor: Avard
   Opis : Sniegulak
   Data : 9.04.07 */

inherit "/std/holdable_object.c";
#include <materialy.h>
#include <object_types.h>
#include <stdproperties.h>
#include <macros.h>

void
create_holdable_object()
{
    ustaw_nazwe("cegla");
    dodaj_przym("stary","stara");
    dodaj_przym("czerwony","czerwoni");

    set_long("Stara zniszczona ceg^la, koloru ciemnoczerwonego wypalona na "+
        "s^lo^ncu, a nast^epnie w piecu, cal^a pokryta jest ple^sni^a i "+
        "mchem.\n");
    ustaw_material(MATERIALY_RZ_GLINA);
    set_type(O_INNE);
    add_prop(OBJ_I_VALUE, 0);
    add_prop(OBJ_I_VOLUME, 1000);
    add_prop(OBJ_I_WEIGHT, 1000);
    add_prop(OBJ_I_NO_SELL,1);
}