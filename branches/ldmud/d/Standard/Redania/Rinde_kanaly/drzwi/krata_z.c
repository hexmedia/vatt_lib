
/* Autor: Avard
   Opis : Duana
   Data : 10.04.07*/

inherit "/std/door";

#include <pl.h>
#include <stdproperties.h>
#include <macros.h>
#include <ss_types.h>
#include <cmdparse.h>
#include <language.h>

#include "dir.h"
void
create_door()
{
    ustaw_nazwe("krata");
    dodaj_przym("stalowy","stalowi");

    set_other_room(POLUDNIE_LOKACJE + "ulica7.c");
    set_door_id("KRATA_DO_KANALOW_RINDE");
    set_door_desc("Grube czworok^atne pr^ety po^l^aczono tworz^ac krat^e "+
        "zagradzaj^ac^a wyj^scie z miejskich kana^l^ow. Metal pokrywa "+
        "cienka warstwa rdzy.\n");

    set_open_desc("");
    set_closed_desc("");
    set_open(0);

    set_pass_command(({"g^ora","u","drabina","krata"}),"przez otw^or w "+
        "suficie","wychodz^ac z kana^l^ow");

    set_key("KLUCZ_KRATA_DO_KANALOW_RINDE");
    set_lock_name(({"zamek", "zamka", "zamkowi", "zamek", "zamkiem", "zamku"}), 0, PL_MESKI_NOS_NZYW);
}

int
pass_door(string arg)
{
    if (!other_door)
        load_other_door();

    string curCmd = query_verb() + (stringp(arg) ? " " + arg : "");

    //Sprawdzamy czy komenda pasuje;
    if(sizeof(pass_command))
    {
        if(member_array(curCmd, pass_command) < 0)
            return 0;
    }
    else if (!(curCmd ~= pass_command))
        return 0;

    if (this_player()->query_rasa(0) ~= "szczur")
        return 1;

    ::pass_door(arg);
}