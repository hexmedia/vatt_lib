
/* Autor: Avard
   Data : 08.04.07
   Opis : Valor i Faeve */

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <pogoda.h>
#include "dir.h"

inherit RINDE + "std/kanaly.c";

void 
create_kanaly() 
{
    set_short("Okr^ag^la komnata");
    add_prop(ROOM_I_INSIDE,1);
    add_exit(KANALY_LOKACJE + "kanaly11.c","w",0,1,0);
    add_exit(KANALY_LOKACJE + "kanaly13.c",
        ({({"schody","schodki","d"}),"na d^o^l po schodach"}),0,1,0);
    add_npc(RINDE_NPC + "szczur.c");
    add_prop(ROOM_I_LIGHT, 0);

    add_item(({"kolumny","kolumn^e"}),"Wysokie i smuk^le kolumny, wykute w "+
        "granicie pokryte s^a dziwnymi ornamentami i grawerowaniami. "+
        "Szerokie na mniej wi^ecej ^lokiec u nasady zw^e^zaj^a si^e, by "+
        "ponowie na ko^ncu przy stropie, rozszerzy^c si^e do poprzedniej "+
        "szeroko^sci. Ca^la powierzchnia kolum jest pokryta grub^a warstw^a "+
        "kurzu i brudu.\n");

    add_item(({"schodki","schody"}),"Schodki wykute w kamieniu wygl^adaj^a "+
        "na solidne, pokryte s^a kurzem i brudem. Prowadz^a w g^l^ab "+
        "otworu, lecz spowite ciemno^sci^a ziej^ac^a z dziury, ods^laniaj^a "+
        "tylko trzy pierwsze stopnie.\n");

    add_item(({"otw^or","dziur^e","ciemny otw^or"}),"Dziura znajduj^aca "+
        "si^e po ^srodku komnaty jest szeroka tak ^ze mo^ze si^e w niej "+
        "zmie^sci^c du^zych rozmiarow beczka. Jej boki s^a r^owne i "+
        "wyko^nczone innym rodzajem kamienia. Ciemno^s^c ziej^aca z "+
        "otworu nie jest mo^zliwa do przenikni^ecia, a w g^l^ab dziury "+
        "prowadz^a ma^l^ee schodki.\n");
    add_item(({"malunki","rysunki","obrazy","freski"}),"Wykute w kamieniu, "+
        "kiedy^s zapewne pi^ekne, obecnie zniszczone, poobijane i pokryte "+
        "kurzem. Przedstawiaj^a sylwetki elf^ow w r^o^znych sytuacjach. "+
        "Najmiej zniszczona, a zarazem najwi^eksza pokazuje rytua^l "+
        "poch^owku. Stoj^ace w okr^egu elfy pochylaj^a si^e nad cia^lem "+
        "zmar^lego, kt^ory le^zy na kamiennej p^lycie trzymaj^ac na "+
        "piersiach d^lugi ^luk.\n");
}
public string
exits_description() 
{
    return "Kana^ly ci^agn^a si^e na zachodzie, znajduj^a si^e tu tak^ze "+
        "schody prowadz^ace na do^l.\n";
}
string
dlugi_opis()
{
    string str;
    str = "^Sciany tego okr^ag^lego pomieszczenia zdobione s^a wyblak^lymi "+
        "rysunkami i p^laskorze^xbami przedstawiaj^acymi sylwetki elf^ow. "+
        "Do^s^c wysoki sufit podtrzymywany jest przez cztery, ^z^lobione "+
        "kolumny, kt^orych czasy ^swietno^sci dawno min^e^ly, "+
        "pozostawiaj^ac po sobie sm^etne resztki pokryte grub^a warstw^a "+
        "kurzu. Na samym ^srodku pomieszczenia znajduje si^e spora dziura, "+
        "a niewielkie schodki przy jednym z brzeg^ow prowadza w g^l^ab "+
        "cuchn^acej ciemno^sci. \n";    
    return str;
}
