/* Autor: Avard
   Data : 08.04.07
   Opis : Valor i Faeve */

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <pogoda.h>
#include "dir.h"

inherit RINDE + "std/kanaly.c";

void 
create_kanaly() 
{
    set_short("Kana^ly.");
    add_prop(ROOM_I_INSIDE,1);
    
    add_exit(KANALY_LOKACJE + "kanaly2.c","w",0,1,0);
    add_exit(KANALY_LOKACJE + "kanaly8.c","e",0,1,0);

    add_item(({"^la^ncuchy","^lancuch","rzygacze"}),"Stare przerdzewia^le "+
        "^la^ncuchy zwisaj^a z sufitu, nie poruszaj^ac si^e nawet na "+
        "odrobin^e. Metal pokryty jest dziwn^a zielonkaw^a substancj^a "+
        "kt^ora s^aczy si^e z sufitu sp^lywaj^ac mi^edzy ogniwami. "+
        "Najprawdopodobniej ciecz cieknie z komnaty kt^ora znajduje si^e "+
        "powy^zej.\n");
    add_item(({"paj^eczyny","paj^eczyn^e"}),"G^esto utkane paj^eczyny "+
        "zdaj^a si^e szczelnie pokrywa^c ca^l^a powierzchni^e sufitu, "+
        "jak gdyby wisia^ly tam od zawsze. Cz^e^s^c z nich, niczym "+
        "z^luszczony nask^orek, prawie odpad^la ju^z od kamienia i "+
        "dynda swodobnie gotowa zawadzic o g^lowy przechodz^acych "+
        "t^edy os^ob.\n");
    add_prop(ROOM_I_LIGHT, 0);
}
public string
exits_description() 
{
    return "Kana^ly ci^agn^a si^e na wschodzie i zachodzie.\n";
}
string
dlugi_opis()
{
    string str;
    str = "Ziemia niczym g^abka ugina si^e pod twym ci^e^zarem, a "+
        "^smierdz^aca woda i mu^l s^a niemal wsz^edzie. Z sufitu "+
        "zwisaj^a stare, ob^slizg^le ^la^ncuchy, z kt^orych kapie szlam, "+
        "sp^lywaj^acy zapewne z g^ornego pomieszczenia. G^este paj^eczyny, "+
        "pokrywaj^ace niemal wszystko wok^o^l, utrudniaj^a poruszanie si^e. "+
        "Wyj^scie prowadz^ace na wsch^od to dziura wykuta w "+
        "kamiennej ^scianie prowadz^aca do malutkiej bia^lej kropki "+
        "^swiec^acej w ciemno^sciach.\n";    
    return str;
}
