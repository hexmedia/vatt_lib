/* Autor: Avard
   Data : 08.04.07
   Opis : Valor i Faeve */

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <pogoda.h>
#include "dir.h"

inherit RINDE + "std/kanaly.c";

void 
create_kanaly()
{
    set_short("Przy drabinie");
    add_prop(ROOM_I_INSIDE,1);
    add_exit(KANALY_LOKACJE + "kanaly2.c","n",0,1,0);
    add_object(KANALY_DRZWI + "krata_z");

    add_item("drabin^e","Zardzewia^la drabina, pokryta rdz^a wysok^a na "+
        "10 ^lokci prowadzi w g^or^e w stron^e stalowej klapy. Szerokie "+
        "szczeble, lekko sp^laszczone, zapewniaj^a pewne ustawienie "+
        "stopy.\n");
    add_npc(RINDE_NPC + "szczur.c");

    add_item(({"paj^eczyny","paj^eczyn^e"}),"G^esto utkane paj^eczyny "+
        "zdaj^a si^e szczelnie pokrywa^c ca^l^a powierzchni^e sufitu, "+
        "jak gdyby wisia^ly tam od zawsze. Cz^e^s^c z nich, niczym "+
        "z^luszczony nask^orek, prawie odpad^la ju^z od kamienia i "+
        "dynda swodobnie gotowa zawadzic o g^lowy przechodz^acych "+
        "t^edy os^ob.\n");
    add_item(({"^sciany","mur","^scian^e","mury"}),"Stare ^sciany poryte "+
        "s^a g^est^a sieci^a p^ekni^e^c w kt^orych przez lata zgromadzi^la "+
        "si^e niewyobra^zalna ilo^s^c kurzu i przer^o^znych paproch^ow. "+
        "Ceg^ly ju^z dawno utraci^ly swoj^a spoisto^s^c, teraz sta^ly "+
        "kruche i ^lupliwe. Z pewno^sci^a kilka mocnych uderze^n "+
        "wystarczy^loby, aby rozbi^c je na dziesi^atki drobnych "+
        "kawa^lk^ow.\n");
}
public string
exits_description() 
{
    return "Kana^ly ci^agn^a si^e na p^o^lnoc, jest tutaj tak^ze drabina "+
        "prowadz^aca na g^or^e.\n";
}
string
dlugi_opis()
{
    string str;
    str = "Stara, metalowa drabina przymocowana do ^sciany bole^snie "+
        "skrzypi, podczas gdy rdza oboj^etnie prze^zera ka^zdy jej "+
        "szczebelek. Pop^ekane mury prezentuj^a swoje wn^etrze - stare "+
        "ceg^ly, ob^lupane, pokryte sin^a warstw^a kurzu, gdzieniegdzie "+
        "pokazuj^ace wy^z^lobienia po wodzie. Pod sufitem wisz^a ca^le "+
        "girlandy paj^eczyn, a miedzy nimi mo^zna dostrzec pojedyncze "+
        "czarne punkciki nerwowo poruszaj^ace si^e w p^o^lmroku. D^lugi "+
        "ciemny korytarz, ziej^ac ch^lodem i okropnym fetorem miejskich "+
        "^sciek^ow ci^agnie si^e na p^o^lnoc. \n";    
    return str;
}
