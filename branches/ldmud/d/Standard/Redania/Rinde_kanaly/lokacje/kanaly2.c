/* Autor: Avard
   Data : 08.04.07
   Opis : Valor i Faeve */

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <pogoda.h>
#include "dir.h"

inherit RINDE + "std/kanaly.c";

void 
create_kanaly() 
{
    set_short("Kana^ly");
    add_prop(ROOM_I_INSIDE,1);
    add_object(KANALY_OBIEKTY + "cegla",10);
    dodaj_rzecz_niewyswietlana("stara czerwona ceg^la",10);
    add_item(({"rysunki","^sciany","^scian^e","stare rysunki","malowid^la"}),
        "Kamienne ^sciany pokryte obrazami z ^zycia elf^ow, s^a okryte "+
        "grub^a warstw^a kurzu i brudu. Gdzieniegdzie poubijane i "+
        "odpadaj^ace kawa^lki ods^laniaj^a ziemi^e znajduj^ac^a si^e "+
        "pod ^scian^a.\n");
    add_exit(KANALY_LOKACJE + "kanaly1.c","s",0,1,0);
    add_exit(KANALY_LOKACJE + "kanaly3.c","n",0,1,0);
    add_exit(KANALY_LOKACJE + "kanaly7.c","e",0,1,0);
    add_npc(RINDE_NPC + "szczur.c");
    add_npc(RINDE_NPC + "szczur.c");
    add_prop(ROOM_I_LIGHT, 0);
}
public string
exits_description() 
{
    return "Kana^ly ci^agn^a si^e na p^o^lnocy, po^ludniu i wschodzie.\n";
}
string
dlugi_opis()
{
    string str;
    str = "Korytarz zw^e^za si^e w kierunku p^o^lnocnym, a sufit w "+
        "tym miejscu niebezpiecznie obni^za, przez co poruszanie si^e jest "+
        "znacznie utrudnione. Mimo ^ze ziemia jest w miar^e sucha - bije od "+
        "niej okropny, dusz^acy smr^od zgnilizny i odchod^ow. ";
    if(jest_rzecz_w_sublokacji(0,"stara czerwona ceg^la"))
    {
        str += "Niemal wsz^edzie walaj^a si^e gruzy i stare zmursza^le "+
            "ceg^ly. ";
    }
    str += "Na jednej ze ^scian, pomi^edzy p^ekni^eciami i dziwnymi, "+
        "s^acz^acymi si^e strumykami zielonej mazi dostrzec mo^zna "+
        "stare rysunki przedstawiaj^ace elfy. \n";    
    return str;
}
