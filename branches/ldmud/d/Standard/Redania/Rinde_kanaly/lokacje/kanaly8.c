/* Autor: Avard
   Data : 08.04.07
   Opis : Valor i Faeve */

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <pogoda.h>
#include "dir.h"

inherit RINDE + "std/kanaly.c";

void 
create_kanaly() 
{
    set_short("Kana^ly");
    add_prop(ROOM_I_INSIDE,1);
    add_exit(KANALY_LOKACJE + "kanaly7.c","w",0,1,0);
    add_exit(KANALY_LOKACJE + "kanaly9.c","e",0,1,0);
    add_exit(KANALY_LOKACJE + "kanaly10.c","s",0,1,0);
    add_exit(KANALY_LOKACJE + "kanaly4.c","n",0,1,0);
    add_prop(ROOM_I_LIGHT, 0);
    add_item(({"paj^eczyny","paj^eczyn^e"}),"G^esto utkane paj^eczyny "+
        "zdaj^a si^e szczelnie pokrywa^c ca^l^a powierzchni^e sufitu, "+
        "jak gdyby wisia^ly tam od zawsze. Cz^e^s^c z nich, niczym "+
        "z^luszczony nask^orek, prawie odpad^la ju^z od kamienia i "+
        "dynda swodobnie gotowa zawadzic o g^lowy przechodz^acych "+
        "t^edy os^ob.\n");
    add_item(({"^sciany","mur","^scian^e","mury"}),"Stare ^sciany poryte "+
        "s^a g^est^a sieci^a p^ekni^e^c w kt^orych przez lata zgromadzi^la "+
        "si^e niewyobra^zalna ilo^s^c kurzu i przer^o^znych paproch^ow. "+
        "Ceg^ly ju^z dawno utraci^ly swoj^a spoisto^s^c, teraz sta^ly "+
        "kruche i ^lupliwe. Z pewno^sci^a kilka mocnych uderze^n "+
        "wystarczy^loby, aby rozbi^c je na dziesi^atki drobnych "+
        "kawa^lk^ow.\n");
}
public string
exits_description() 
{
    return "Kana^ly ci^agn^a si^e na wschodzie, zachodzie, p^o^lnocy "+
        "i po^ludniu.\n";
}
string
dlugi_opis()
{
    string str;
    str = "Mokra i brudna ziemia cuchnie odchodami, a z sufitu korytarza "+
        "kapie woda. G^este paj^eczyny, pokrywaj^ace niemal wszystko "+
        "wok^o^l, utrudniaj^a poruszanie si^e, a korzenie drzew, wystaj^ace "+
        "gdzieniegdzie ze szczerb w murze, sprawiaj^a wra^zenie ^zywych, "+
        "pragn^acych ople^s^c ci^e swymi ramionami. Na wschodnim "+
        "kra^ncu korytarza wida^c poka^xnych rozmiar^ow bia^l^a plam^e "+
        "o^slepiaj^ac^a swym blaskiem.\n";    
    return str;
}
