/* Autor: Avard
   Opis : Sniegulak
   Data : 18.05.07 */

inherit "/std/gate.c";
#include <stdproperties.h>
#include <macros.h>
#include "dir.h"

void
create_gate()
{
    set_other_room(TRETOGOR_LOKACJE + "pusta.c");
    set_long("Jest to pot^e^zna wykonana z mocnego drewna brama, kt^ora "+
        "sk^lada si^e z dw^och skrzyde^l otwieranych osobno. Stalowe "+
        "okucia maj^a za zadanie zwi^ekszy^c wytrzyma^lo^s^c wr^ot, a "+
        "ca^la konstrukcja zamykana najprawdopodobniej od ^srodka na "+
        "pot^e^zn^a zasuw^e b^edzie w stanie wytrzyma^c cios niejednego "+
        "tarana. \n");
    set_open(1);
    set_locked(0);
    add_prop(GATE_IS_INSIDE,0);
    set_gate_id("TRETOGOR_POLUDNIE_BRAMA");
    set_pass_command(({"p^o^lnoc","n"}));
    set_security_lvl(3);
    godzina_zamkniecia(4);
    godzina_otwarcia(4);
    set_skad("z Tretogoru");
    dodaj_przym("pot^e^zny","pot^e^zni");
}

