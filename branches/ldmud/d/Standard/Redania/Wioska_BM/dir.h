#include "/d/Standard/Redania/dir.h"

#define WIOSKA              ("/d/Standard/Redania/Wioska_BM/")

#define WIOSKA_LOKACJE      (WIOSKA + "lokacje/")
#define WIOSKA_NPC          (WIOSKA + "npc/")
#define WIOSKA_PRZEDMIOTY   (WIOSKA + "przedmioty/")
#define WIOSKA_DRZWI        (WIOSKA + "lokacje/drzwi/")
#define WIOSKA_UBRANIA      (WIOSKA + "przedmioty/ubrania/")

#define WIOSKA_BM_STD       (WIOSKA + "std/wioska_bm.c")
#define WIOSKA_STD          (WIOSKA + "std/wioska.c")
#define WIOSKA_NPC_STD      (WIOSKA + "std/npc.c")
