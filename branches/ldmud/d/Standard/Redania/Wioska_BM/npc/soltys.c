#pragma unique
#include <macros.h>
#include <stdproperties.h>
#include <money.h>
#include <pl.h>
#include <formulas.h>
#include <std.h>
#include <ss_types.h>
#include <wa_types.h>
#include <composite.h>
#include <filter_funs.h>
#include "dir.h"

inherit WIOSKA_NPC_STD;

int i = 0;

void
create_wioska_humanoid()
{
    set_living_name("karel");

    ustaw_odmiane_rasy("mezczyzna");
    set_gender(G_MALE);

    dodaj_nazwy("so^ltys");

    set_title(", syn Dobrogosta, so^ltys wsi");

    ustaw_imie(({"karel", "karela", "karelowi", "karela", "karelem", "karelu"}), PL_MESKI_OS);

    set_long("Wysoki i niezwykle chudy cz�owiek ubrany jest w lnian^a, " +
    "szar^a koszul^e i wykonane z podobnego materia^lu spodnie. Ubi^or, " +
    "mimo ^ze ubogi, jest jednak zadbany i schludny. Malutkie niebieskie " +
    "oczka lustruj� dok^ladnie okolic^e - nic nie umknie uwadze tego " +
    "energicznego, lekko siwiej^acego m^e^zczyzny. Krzaczasty w^as i " +
    "brwi nadaj^a mu i^scie demonicznego wyrazu, jednak w^atpliwe, ^zeby " +
    "mia^l cokolwiek wsp^olnego z si^lami nieczystymi.\n");

    dodaj_przym("wysoki", "wysocy");
    dodaj_przym("^zylasty", "^zyla^sci");

    set_act_time(57);
    add_act("emote podskubuje nerwowo w^asy.");
    add_act("popatrz obojetnie na mezczyzne");
    add_act("popatrz obojetnie na kobiete");
    add_act("emote cicho mruczy co^s pod nosem, jednocze^snie licz^ac na " +
    "palcach.");
    add_act("emote z cichym westchnieniem pociera czo^lo.");

    set_chat_time(131);
    add_chat("Te podatki nas przecie wyko^ncz^a.");
    add_chat("Jeno patrze^c, jak poborca znowu przyjedzie...");

    set_cact_time(14);
    add_cact("krzyknij Ludzie! Ludzie! Morduj^a!");

    set_default_answer(VBFC_ME("default_answer"));
    add_ask(({"so^ltysa", "so^ltysa wsi", "so^ltysa wioski"}), VBFC_ME("pyt_soltys"));
    add_ask(({"wie^s", "wiosk^e"}), VBFC_ME("pyt_wies"));
    add_ask("karczm^e", VBFC_ME("pyt_karczma"));
    add_ask(({"johanna", "brata"}), VBFC_ME("pyt_johann"));

    set_stats (({40, 61, 39, 65, 48}));

    set_skill(SS_APPR_OBJ, 48);
    set_skill(SS_LANGUAGE, 31);
    set_skill(SS_AWARENESS, 46);

    add_armour(WIOSKA_UBRANIA + "szara_lniana_koszula.c");
    add_armour(WIOSKA_UBRANIA + "szare_lniane_spodnie.c");

    add_object(WIOSKA_DRZWI + "k_soltys");

    add_prop(CONT_I_WEIGHT, 87000);
    add_prop(CONT_I_HEIGHT, 182);

    set_alarm_every_hour("co_godzine");
    set_alarm(1.0, 0.0, "co_godzine");
}

void
powiedz_gdy_jest(mixed player, string tekst)
{
    if (pointerp(player))
    {
        if (sizeof(FILTER_PRESENT_LIVE(player)) > 0)
            this_object()->command(tekst);
        else
        switch (random(3))
        {
            case 0:
                this_object()->command("rozejrzyj sie szybko");
                break;

            case 1:
                this_object()->command("powiedz :cicho: Kto to by^l?");
                break;

            case 2:
                this_object()->command("ob sie");
                break;

            case 3:
                this_object()->command("powiedz :cicho: Chyba ju^z wariuj^e...");
                break;
        }
    }


    if (environment(this_object()) == environment(player))
        this_object()->command(tekst);
    else
        switch (random(3))
        {
            case 0:
                this_object()->command("rozejrzyj sie szybko");
                break;

            case 1:
                this_object()->command("powiedz :cicho: Kto to by^l?");
                break;

            case 2:
                this_object()->command("ob sie");
                break;

            case 3:
                this_object()->command("powiedz :cicho: Chyba ju^z wariuj^e...");
                break;
        }
}

string
default_answer()
{
     set_alarm(2.0, 0.0, "powiedz_gdy_jest", this_player(),
             "powiedz do " + OB_NAME(this_player()) +
                  " No i czego mi w robocie przeszkadza? Nie widzi, ^ze " +
                  "inne sprawy mam teraz na g^lowie?");
     return "";
}

string
pyt_soltys()
{
    set_alarm(1.0, 0.0, "powiedz_gdy_jest", TP, "powiedz do " + OB_NAME(TP) +
    " To^c ja tu so^ltys jestem... tak jak i tatko so^ltysem by^l.");

    return "";
}

string
pyt_wies()
{
    command("wzrusz ramionami");

    set_alarm(1.5, 0.0, "powiedz_gdy_jest", TP, "powiedz do " + OB_NAME(TP) +
    " Wie^s jak wie^s, co tu gada^c wiele...");

    return "";
}

string
pyt_karczma()
{
    set_alarm(1.0, 0.0, "powiedz_gdy_jest", TP, "powiedz do " + OB_NAME(TP) +
    " A wyjd^x^ze przed cha^lup^e i zaroz obaczysz. A jak nie obaczysz, " +
    "to us^lyszysz... Ci^e^zko nie us^lysze^c przecie.");

    return "";
}

string
pyt_johann()
{
    command("skrzyw sie .");

    set_alarm(1.5, 0.0, "powiedz_gdy_jest", TP, "powiedz do " + OB_NAME(TP) +
    " A^z ci^e^zko uwierzy^c, ^ze to m^oj brat... Rodzony do tego, psia ko^s^c...");

    return "";
}

void
return_introduce(string imie)
{
    object osoba = present(imie, environment());

    if (osoba)
    {
        command("przedstaw sie " + OB_NAME(osoba));
    }
}

void
emote_hook(string emote, object wykonujacy, string przyslowek)
{
    switch (emote)
    {
        case "kopnij":
        case "spoliczkuj":
        case "opluj":
	case "nadepnij":
	    set_alarm(2.0, 0.0, "zly", wykonujacy);
            break;

        case "szturchnij":
	    set_alarm(2.0, 0.0, "taki_sobie", wykonujacy);
            break;

        case "za�miej":
        case "pog�aszcz":
	    set_alarm(2.0, 0.0, "dobry", wykonujacy);
            break;

        case "poca^luj":
            set_alarm(1.0, 0.0, "pocaluj", wykonujacy);
            break;

        case "przytul":
            if(wykonujacy->query_gender() == G_FEMALE)
            set_alarm(1.0, 0.0, "przytul", wykonujacy);
            else
            set_alarm(1.0, 0.0, "pocaluj", wykonujacy);
            break;
    }
}

void
oddajemy(object ob, object old)
{
    if (interactive(old) && (ENV(old) == ENV(TO))) {
        command_present(old, "powiedz do " + OB_NAME(old) + " Niczego nie przyjm^e!");

        command_present(old, "daj " + OB_NAME(ob) + " " + OB_NAME(old));
        command("odloz " + OB_NAME(ob));
        return;
    }

}

void
zly(object kto)
{
    switch (random(4))
    {
        case 0:
            powiedz_gdy_jest(kto, "powiedz do " + OB_NAME(kto) + " Ja ci dam, gagatku!");
            break;
        case 1:
            powiedz_gdy_jest(kto, "powiedz do " + OB_NAME(kto) + " Jeszcze raz i inaczej pogadamy!");
            break;
        case 2:
            powiedz_gdy_jest(kto, "powiedz do " + OB_NAME(kto) + " Wynocha mi st^ad!");
            break;
        case 3:
            command("popatrz z pogarda na " + OB_NAME(kto));
            break;
    }
    return;
}

void
taki_sobie(object kto)
{
    switch(random(3))
    {
      case 0:
           command("skrzyw sie .");
           break;
      case 1:
           command("wzrusz ramionami");
           break;
      case 2:
           command("popatrz ciezko na " + OB_NAME(kto));
           break;
           }
}

void
dobry(object kto)
{
                 switch(random(3))
    {
      case 0:
           command("skrzyw sie .");
           break;
      case 1:
           command("wzrusz ramionami");
           break;
      case 2:
           command("popatrz ciezko na " + OB_NAME(kto));
           break;
           }
}

void
pocaluj(object ob)
{
    if (ob->query_gender() == G_FEMALE)
    {
         switch(random(4))
         {
             case 0:
                 set_alarm(1.0, "command", "skrzyw sie");
                 powiedz_gdy_jest(ob, "powiedz do " + OB_NAME(ob) +
                 " Wynocha mi st^ad, ladacznico! To^c ja ^zon^e mam!");
                 break;
             case 1:
                 powiedz_gdy_jest(ob, "powiedz do " + OB_NAME(ob) +
                 " Czego mi tu? Jeszcze raz i na zbity pysk wyrzuc^e!");
                 break;
             case 2:
                  powiedz_gdy_jest(ob, "powiedz do " + OB_NAME(ob) +
                 " To^z to nie przystoi takie zachowanie! Upadek obyczaj^ow!");
                 break;
             case 3:
                 command("wskaz drzwi");
                 powiedz_gdy_jest(ob, "powiedz do " + OB_NAME(ob) +
                 " Won, wyw^loko!");
                 break;
         }
    }
    else
    {
        switch(random(3))
        {
            case 0:
            set_alarm(1.5, 0.0, "command", "skrzyw sie z obrzydzeniem");
            powiedz_gdy_jest(ob, "powiedz do " + OB_NAME(ob) +
            " Id^x mi st^ad, zbocze^ncu! Bo ^zelazem pocz^estuj^e!");
            break;
            case 1:
            powiedz_gdy_jest(ob, "powiedz do " + OB_NAME(ob) +
            " Do kro^cset! Sk^ad si^e takie popapra^nce bior^a?");
            break;
            case 2:
            powiedz_gdy_jest(ob, "powiedz do " + OB_NAME(ob) +
            " Jeszcze raz i inaczej porozmawiamy, odmie^ncu!");
            break;
        }
    }
    return;
}

void
przytul(object ob)
{
    switch(random(3))
    {
        case 0:
            powiedz_gdy_jest(ob, "powiedz do " + OB_NAME(ob) +
            " No i jeszcze czego? Pozwoli^l jej kto?");
            break;
        case 1:
            command("rozejrzyj sie nerwowo");
            set_alarm(1.5, 0.0, "powiedz_gdy_jest", ob, "powiedz do " +
            OB_NAME(ob) + " Do^s^c tego, jeszcze moja stara obaczy...");
            break;
        case 2:
            powiedz_gdy_jest(ob, "powiedz do " + OB_NAME(ob) +
            " Starczy, starczy... Jeszcze sobie kto co pomy^sli...");
            break;
    }
    return;
}

void
attacked_by(object wrog)
{
    ::attacked_by(wrog);
}

void
enter_inv(object ob, object from)
{
    if (from == TP)
        set_alarm(1.0, 0.0, "oddajemy", ob, from);

    ::enter_inv(ob, from);
}
void
wypadaja(object *kto, object lok, object drzwi)
{
    switch(sizeof(kto))
    {
        case 0:
            return;
            break;
        case 1:
            find_player("gjoef")->catch_msg(set_color(5) + set_color(22) + "\n***WYLECIELI OD SO^LTYSA: " + COMPOSITE_LIVE(kto, 0) +".\n\n");
            FILTER_LIVE(deep_inventory(lok))->catch_msg(capitalize(
                COMPOSITE_LIVE(filter(kto, &->check_seen(TO)), 0)) +
                " wypada z hukiem przez " + drzwi->short(3) + ".\n");
            return;
            break;
        default:
            find_player("gjoef")->catch_msg(set_color(5) + set_color(22) + "\n***WYLECIELI OD SO^LTYSA: " + COMPOSITE_LIVE(kto, 0) +".\n\n");
            FILTER_LIVE(deep_inventory(lok))->catch_msg(capitalize(
                COMPOSITE_LIVE(filter(kto, &->check_seen(TO)), 0)) +
                " wypadaj^a z hukiem przez " + drzwi->short(3) + ".\n");
            return;
            break;
    }
}

int
czy_ktos_zostal()
{
    object *oni = ({});
    object dokad, drzwi;

    if (FILTER_LIVE(deep_inventory(ENV(TO)) - ({TO})))
        oni = FILTER_LIVE(deep_inventory(ENV(TO)) - ({TO}));
    else
        return 1;

    if (member_array(1, FILTER_LIVE(deep_inventory(ENV(TO))-({TO}))->check_seen(TO)) != -1)
    {
        switch(i)
        {
            case 0:
                 i = 1;
                oni = FILTER_LIVE(deep_inventory(ENV(TO))) - ({TO});
                set_alarm(2.0, 0.0, "powiedz_gdy_jest", filter(oni,
                    &->check_seen(TO)), "powiedz do " + FO_COMPOSITE_LIVE(filter(
                    oni, &->check_seen(TO)), TO, 1) + " Ja ju^z ko^ncz^e prac^e, by^lbym wi^ec wdzi^eczny, gdybym zosta^l sam.");
                set_alarm(12.0, 0.0, "czy_ktos_zostal");
                return 1;
                 break;

            case 1:
                i = 2;
                TO->command("tupnij");
                set_alarm(2.0, 0.0, "powiedz_gdy_jest", filter(oni,
                    &->check_seen(TO)), "powiedz do " + FO_COMPOSITE_LIVE(filter(
                    oni, &->check_seen(TO)), TO, 1) + " Jest ^srodek nocy! Kto pracuje w nocy? Na pewno nie ja, ^zegnam.");
                set_alarm(9.0, 0.0, "czy_ktos_zostal");
                return 1;
                break;

            case 2:
                i = 3;
                oni = FILTER_LIVE(deep_inventory(ENV(TO))) - ({TO});
                TO->command("skrzyw sie .");
                set_alarm(2.0, 0.0, "powiedz_gdy_jest", filter(oni,
                    &->check_seen(TO)), "powiedz do " + FO_COMPOSITE_LIVE(filter(
                    oni, &->check_seen(TO)), TO, 1) + " No ju^z, chc^e i^s^c spa^c!");
                set_alarm(7.0, 0.0, "czy_ktos_zostal");
                set_alarm(5.5, 0.0, "powiedz_gdy_jest", filter(oni,
                    &->check_seen(TO)), "powiedz do " + FO_COMPOSITE_LIVE(filter(
                    oni, &->check_seen(TO)), TO, 1) + " No psia ma^c!");
                return 1;
                break;

            case 3:
                i = 0;
                oni = FILTER_CAN_SEE(FILTER_LIVE(deep_inventory(ENV(TO))) - ({TO}), TO);
                if (wildmatch("*soltys", file_name(ENV(TO))))
                {
                    dokad = find_object(WIOSKA_LOKACJE + "ogrod");
                    drzwi = find_object(WIOSKA_DRZWI + "do_soltysa");
                }
                else
                {
                    dokad = find_object(TP->query_default_start_location());
                    TP->catch_msg("Nast^api^l nieprzewidziany b^l^ad. Niez" +
                    "w^locznie zg^lo^s go czarodziejom!\n");
                    find_player("gjoef")->catch_msg(set_color(1) +
                        set_color(5) + set_color(31) + "\n***B^L^AD!***\n\n");
                    return 1;
                }

                oni->catch_msg(QCIMIE(TO, 0) + " szybkim ruchem wyrzuca " +
                "ci^e za drzwi.\n");
                saybb(QCIMIE(TO, 0) + " szybkim ruchem wyrzuca " + FO_COMPOSITE_LIVE(filter(
                    oni, &->check_seen(TO)), TO, 3) + " za drzwi.\n", oni);

                seteuid(getuid());

                wypadaja(filter(oni, &->check_seen(TO)), dokad, drzwi);

                TO->command("zamknij drzwi");
                oni->move(dokad);
                TO->command("zamknij drzwi kluczem");

                set_alarm(1.5, 0.0, "command", "krzyknij Zero, kurwa, kultury!");
                set_alarm(4.0, 0.0, "command", "pokrec ciezko");

                return 1;
                break;
        }
    }
        else
        {
            if (wildmatch("*soltys", file_name(ENV(TO))))
            {
                set_alarm(2.0, 0.0, "command", "zamknij drzwi");
                set_alarm(4.0, 0.0, "command", "zamknij drzwi kluczem");

                return 1;
            }
        }
    i = 0;
    return 1;
}

int
co_godzine()
{
    if (MT_GODZINA < 5)
    {
        set_alarm(1.0, 0.0, "command", "pociagnij nosem cicho");
        set_alarm(3.0, 0.0, "command", "powiedz Koniec pracy! Dobranoc wszystkim.");
        set_alarm(8.0, 0.0, "command", "rozejrzyj sie uwaznie");
        set_alarm(8.0, 0.0, "czy_ktos_zostal");
    }
    else
    {
        if (present("drzwi", ENV(TO))->query_locked())
            set_alarm(1.0, 0.0, "otworz drzwi kluczem");
    }
}
