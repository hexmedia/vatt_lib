/* Autor: Duana
  Opis : Vortak
  Data : 29.05.2007
*/

#pragma unique

#include <macros.h>
#include <stdproperties.h>
#include <money.h>
#include <pl.h>
#include <formulas.h>
#include <std.h>
#include <ss_types.h>
#include <wa_types.h>
#include <composite.h>
#include <filter_funs.h>
#include "dir.h"

inherit WIOSKA_NPC_STD;

string *imie;
int czy_walka();
int walka = 0;
int i = 0;

void
create_wioska_humanoid()
{
    set_living_name("johann");

    ustaw_odmiane_rasy("m^e^zczyzna");
    set_gender(G_MALE);
    imie=({"johann","johanna","johannowi","johanna",
        "johannem","johannie"});
    ustaw_imie(imie,PL_MESKI_OS);
    set_title(", syn Dobrogosta, w^la^sciciel karczmy");
    dodaj_nazwy("karczmarz");

    set_long("Twarz tego korpulentnego m^e^zczyzny co chwila rozja^snia "+
        "szeroki u^smiech. Zdawa^c by si^e mog^lo, ^ze nic nie mo^ze "+
        "popsu^c mu humoru. Obrazu dope^lniaj^a weso^le, rzucaj^ace "+
        "przyjazne spojrzenia oczka i ogromny brzuch, na kt^orym zwyk^l "+
        "opiera^c pulchne, nieprzywyk^le do pracy w polu d^lonie. Jedynie "+
        "du^zy, czerwony nos pozwala sie domy^sla^c, ^ze w dobry humor "+
        "wprawia si^e poka^xnymi ilo^sciami alkoholu.\n");

        dodaj_przym("t^egi","t^edzy");
        dodaj_przym("rubaszny","rubaszni");

    set_stats(({ 65, 45, 42, 45, 50}));
    set_skill(SS_PARRY, 55);
        set_skill(SS_DEFENCE, 45);
        set_skill(42, 50);

    add_prop(CONT_I_WEIGHT, 100000);
    add_prop(CONT_I_HEIGHT, 160);
    add_prop(LIVE_I_NEVERKNOWN, 0);

    set_act_time(30);
    add_act("emote rozgl^ada si^e przyja^xnie.");
    add_act("emote drapie si^e po poka^xnym brzuszysku.");
    add_act("emote ukradkiem wypija kieliszek prze^xroczystego p^lynu.");
    add_act("':dono^snym g^losem: Komu kieliszeczek przepysznej "+
        "gorza^lczyny?");

    set_cchat_time(10);
    add_cchat("We^xta ode mnie tego psubrat^e, bo mu krzywd^e jeszcze "+
        "zrobi^e!");
    add_cchat("No teraz to zdecydowanie jest przesada!");


    add_ask(({"karczm^e"}), VBFC_ME("pyt_o_karczme"));
    add_ask(({"so^ltysa"}), VBFC_ME("pyt_o_soltysa"));
    add_ask(({"karela","brata"}), VBFC_ME("pyt_o_karela"));

    set_default_answer(VBFC_ME("default_answer"));

    set_alarm_every_hour("co_godzine");
    set_alarm(1.0, 0.0, "co_godzine");
    set_wzywanie_strazy(1);

    add_armour("/d/Standard/items/ubrania/spodnie/wytarte_czarne_Mc.c");
    add_armour("/d/Standard/Redania/Rinde/przedmioty/ubrania/koszule/biala_lniana_Mc.c");
    add_armour("/d/Standard/items/ubrania/fartuchy/losowy_rzemieslniczy.c");

    add_object(WIOSKA_DRZWI + "k_karczma");

        seteuid(getuid());
}


void
powiedz_gdy_jest(mixed player, string tekst)
{
    if (pointerp(player))
    {
        if (sizeof(FILTER_PRESENT_LIVE(player)) > 0)
            this_object()->command(tekst);
        else
        switch (random(3))
        {
            case 0:
                this_object()->command("rozejrzyj sie szybko");
                break;

            case 1:
                this_object()->command("powiedz :cicho: Kto to by^l?");
                break;

            case 2:
                this_object()->command("ob sie");
                break;

            case 3:
                this_object()->command("powiedz :cicho: Chyba ju^z wariuj^e...");
                break;
        }
    }


    if (environment(this_object()) == environment(player))
        this_object()->command(tekst);
    else
        switch (random(3))
        {
            case 0:
                this_object()->command("rozejrzyj sie szybko");
                break;

            case 1:
                this_object()->command("powiedz :cicho: Kto to by^l?");
                break;

            case 2:
                this_object()->command("ob sie");
                break;

            case 3:
                this_object()->command("powiedz :cicho: Chyba ju^z wariuj^e...");
                break;
        }
}

string
pyt_o_karczme()
{
    set_alarm(0.5, 0.0, "command_present", this_player(),
        "powiedz :do " + OB_NAME(TP) + " rozgl^adaj^ac si^e dooko^la i kiwaj^ac z zadowoleniem g^low^a: "+
        "Pi^ekna, nieprawda^z? I do tego moja! Moja w^lasna! ");
    return "";

}
string
pyt_o_soltysa()
{
    set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
        "powiedz do " + OB_NAME(TP) + " Jeno za drzwi wyjdziesz i zara tam po lewej taki domek "+
        "b^edzie... Tam w^la^snie so^ltys urz^eduje. ");
    return "";
}

string
pyt_o_karela()
{
    set_alarm(1.0, 0.0, "powiedz_gdy_jest", this_player(),
        "powiedz do " + OB_NAME(TP) + " Znasz mojego braciszka? So^ltysem jest tu u nas... "+
        "Podobny do mnie jak dwie krople wody! ");
    return "";

}

string
default_answer()
{
        set_alarm(1.0, 0.0, "powiedz_gdy_jest", this_player(),
            "powiedz do " + OB_NAME(TP) + " Wybacz, ale nie mam teraz czasu na pogaw^edki. ");
    return "";
}


void
oddajemy(object ob, object old)
{
    if (interactive(old) && (ENV(old) == ENV(TO))) {
        command_present(old, "powiedz do " + OB_NAME(old) + " Nie interesuj^a mnie takie rzeczy.");

        command_present(old, "daj " + OB_NAME(ob) + " " + OB_NAME(old));
        command("odloz " + OB_NAME(ob));
        return;
    }

}


void
add_introduced(string imie, string biernik)
{
    set_alarm(1.0, 0.0, "return_introduce", imie);
}

void
emote_hook(string emote, object wykonujacy)
{
    switch (emote)
    {
        case "kopnij": set_alarm(1.5, 0.0, "nienajlepszy", wykonujacy);
                    break;
        case "spoliczkuj": set_alarm(1.5, 0.0, "nienajlepszy", wykonujacy);
                    break;
        case "opluj": set_alarm(2.5, 0.0, "nienajlepszy", wykonujacy);
                    break;
        case "poca^luj":
        case "przytul":
        case "pog^laszcz":

              {

              if(wykonujacy->query_gender() == 1)
                switch(random(3))
                 {
                 case 0:command("powiedz :u^smiechaj^ac si^e szeroko: "+
                     "Poczekaj kochaniutka a^z prac^e sko^ncz^e... Wtedy poka^z^e "+
                     "ci to, z czego Johann w okolicy s^lynie! ");
                        break;
                 case 1:command("powiedz ^Smia^lo, ^smia^lo... Nie przerywaj, "+
                     "male^nka. ");
                        break;
                 case 2:command("powiedz :rechocz^ac rubasznie: No, to to "+
                     "ja lubi^e... Oj tak... ");
                        break;

                }
              else
                {
                   switch(random(3))
                   {
                   case 0:command("powiedz :krzywi^ac si^e: Ot co to, to "+
                       "nie.. We^x sobie ch^lystku na wstrzymanie...");
                            break;
                   case 1:command("powiedz Zara nie zdzier^z^e i jak "+
                       "przez ^leb strzel^e...");
                            break;
                   case 2:command("powiedz A ty co? Znowu ^ze^s za du^zo "+
                       "wychla^l?");
                            break;
                   }
                }
              }
                break;
    }
}

void
nienajlepszy(object kto)
{
   switch (random(3))
   {
      case 0: command("powiedz :krzywi^ac si^e: O^z ty w mord^e kopany! "+
          "Ja ci poka^z^e.");
              break;
      case 1: command("powiedz :sapi^ac w^sciekle: Jeszcze chwila i nie "+
          "strzymam...");
              break;
      case 2: command("powiedz Trzymajta mnie, ludziska, bo nie r^ecze za "+
          "siebie!");
              break;
   }
}

void attacked_by(object wrog)
{
    if(!walka)
    {
        walka = 1;
        set_alarm(0.5,0.0,"command","krzyknij Ludziska! Ratujta! Bij^a! "+
            "Morduj^a!");
        set_alarm(15.0, 0.0, "czy_walka", 1);
        return ::attacked_by(wrog);
    }
}
int
czy_walka()
{
    if(!query_attack() && walka)
    {
        set_alarm(0.5,0.0,"command","powiedz ^Zeby to by^lo ostatni raz!");
        walka = 0;
        return 1;
    }
    else
    {
        set_alarm(15.0, 0.0, "czy_walka", 1);
    }
}

void
enter_inv(object ob, object from)
{
    if (from == TP)
        set_alarm(1.0, 0.0, "oddajemy", ob, from);

    ::enter_inv(ob, from);
}

void
wypadaja(object *kto, object lok, object drzwi)
{
    switch(sizeof(kto))
    {
        case 0:
            return;
            break;
        case 1:
            find_player("gjoef")->catch_msg(set_color(5) + set_color(22) + "\n***WYLECIELI Z KARCZMY: " + COMPOSITE_LIVE(kto, 0) +".\n\n");
            FILTER_LIVE(deep_inventory(lok))->catch_msg(capitalize(
                COMPOSITE_LIVE(filter(kto, &->check_seen(TO)), 0)) +
                " wypada z hukiem przez " + drzwi->short(3) + ".\n");
            return;
            break;
        default:
            find_player("gjoef")->catch_msg(set_color(5) + set_color(22) + "\n***WYLECIELI Z KARCZMY: " + COMPOSITE_LIVE(kto, 0) +".\n\n");
            FILTER_LIVE(deep_inventory(lok))->catch_msg(capitalize(
                COMPOSITE_LIVE(filter(kto, &->check_seen(TO)), 0)) +
                " wypadaj^a z hukiem przez " + drzwi->short(3) + ".\n");
            return;
            break;
    }
}

int
czy_ktos_zostal()
{
    object *oni = ({});
    object dokad, drzwi;

    if (FILTER_LIVE(deep_inventory(ENV(TO)) - ({TO})))
        oni = FILTER_LIVE(deep_inventory(ENV(TO)) - ({TO}));
    else
        return 1;

    if (member_array(1, FILTER_LIVE(deep_inventory(ENV(TO))-({TO}))->check_seen(TO)) != -1)
    {
        switch(i)
        {
            case 0:
                 i = 1;
                oni = FILTER_LIVE(deep_inventory(ENV(TO))) - ({TO});
                set_alarm(2.0, 0.0, "powiedz_gdy_jest", filter(oni,
                    &->check_seen(TO)), "powiedz do " + FO_COMPOSITE_LIVE(filter(
                    oni, &->check_seen(TO)), TO, 1) + " Ja ju^z zamykam, dobranoc.");
                set_alarm(12.0, 0.0, "czy_ktos_zostal");
                return 1;
                 break;

            case 1:
                i = 2;
                TO->command("tupnij");
                set_alarm(2.0, 0.0, "powiedz_gdy_jest", filter(oni,
                    &->check_seen(TO)), "powiedz do " + FO_COMPOSITE_LIVE(filter(
                    oni, &->check_seen(TO)), TO, 1) + " Wszyscy maj^a st^ad wyj^s^c, nie b^ed^e " +
                    "tu sta^l do rana.");
                set_alarm(9.0, 0.0, "czy_ktos_zostal");
                return 1;
                break;

            case 2:
                i = 3;
                oni = FILTER_LIVE(deep_inventory(ENV(TO))) - ({TO});
                TO->command("skrzyw sie .");
                set_alarm(2.0, 0.0, "powiedz_gdy_jest", filter(oni,
                    &->check_seen(TO)), "powiedz do " + FO_COMPOSITE_LIVE(filter(
                    oni, &->check_seen(TO)), TO, 1) + " Ostatni raz m^owi^e, " +
                    "prosz^e wyj^s^c!");
                set_alarm(7.0, 0.0, "czy_ktos_zostal");
                set_alarm(5.5, 0.0, "powiedz_gdy_jest", filter(oni,
                    &->check_seen(TO)), "powiedz do " + FO_COMPOSITE_LIVE(filter(
                    oni, &->check_seen(TO)), TO, 1) + " No do jasnej cholery!");
                return 1;
                break;

            case 3:
                i = 0;
                oni = FILTER_CAN_SEE(FILTER_LIVE(deep_inventory(ENV(TO))) - ({TO}), TO);
                if (wildmatch("*karczma", file_name(ENV(TO))))
                {
                    dokad = find_object(WIOSKA_LOKACJE + "wioska3");
                    drzwi = find_object(WIOSKA_DRZWI + "do_karczmy");
                }
                else
                {
                    dokad = find_object(TP->query_default_start_location());
                    TP->catch_msg("Nast^api^l nieprzewidziany b^l^ad. Niez" +
                    "w^locznie zg^lo^s go czarodziejom!\n");
                    find_player("gjoef")->catch_msg(set_color(1) +
                        set_color(5) + set_color(31) + "\n***B^L^AD!***\n\n");
                    return 1;
                }

                oni->catch_msg(QCIMIE(TO, 0) + " szybkim ruchem wyrzuca " +
                "ci^e za drzwi.\n");
                saybb(QCIMIE(TO, 0) + " szybkim ruchem wyrzuca " + FO_COMPOSITE_LIVE(filter(
                    oni, &->check_seen(TO)), TO, 3) + " za drzwi.\n", oni);

                seteuid(getuid());

                wypadaja(filter(oni, &->check_seen(TO)), dokad, drzwi);

                oni->move(dokad);

                TO->command("zamknij drzwi");
                TO->command("zamknij drzwi kluczem");
                set_alarm(2.0, 0.0, "command", "pokrec ciezko");

                return 1;
                break;
        }
    }
        else
        {
            if(ENV(TO) && wildmatch("*karczma", file_name(ENV(TO))))
            {
                set_alarm(2.0, 0.0, "command", "zamknij drzwi");
                set_alarm(4.0, 0.0, "command", "zamknij drzwi kluczem");

                return 1;
            }
        }
    i = 0;
    return 1;
}

int
co_godzine()
{
    if (MT_GODZINA >= 23 || MT_GODZINA < 5)
 // if (666)
    {
        set_alarm(1.0, 0.0, "command", "zatrzyj rece");
        set_alarm(3.0, 0.0, "command", "powiedz No to zamykamy! Prosz^e wszystkich o udanie si^e do dom^ow.");
        set_alarm(8.0, 0.0, "command", "rozejrzyj sie uwaznie");
        set_alarm(8.0, 0.0, "czy_ktos_zostal");
    }
    else
    {
        if (present("drzwi", ENV(TO))->query_locked())
            set_alarm(1.0, 0.0, "otworz drzwi kluczem");
    }
}

int
query_karczmarz()
{
	return 1;
}
