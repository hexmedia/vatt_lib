/**
 * \file /d/Standard/Redania/Wioska_BM/npc/krowa.c
 *
 * Krowa dojna, wersja z mlekiem
 *
 * @author Enkin
 * @date do 3 listopada 2007
 */

inherit "/std/zwierze";
inherit "/std/act/action";
inherit "/std/act/trigaction.c";

#pragma unique

#include <macros.h>
#include <stdproperties.h>
#include <money.h>
#include <pl.h>
#include <formulas.h>
#include <std.h>
#include <ss_types.h>
#include <wa_types.h>
#include <object_types.h>
#include <composite.h>
#include <filter_funs.h>
#include <cmdparse.h>
#include "dir.h"

object  dojona = 0;
object  pojemnik, dojenie, pastuch = 0;
int     alarm_id;
int     zdenerwowanie = 0;

int     Dojenia = 0;

void
red_Dojenia()
{
    Dojenia -= 1;
}

void dojenie_alarm();

void create_zwierze()
{
    add_leftover("/std/leftover", "sk^ora", 1, 0, 1, 1, 5 + random(5), O_SKORY);

    ustaw_odmiane_rasy("krowa");
    set_gender(G_FEMALE);

    set_long("Czarna krowa w niewielkie bia�e �aty ot�pia�ym wzrokiem "+
        "wpatruje si� w otoczenie. Wok� du�ego czarnego �ba zwierz�cia "+
        "lataj^a niesforne muchy, kt�re uciekaj^a za ka�dym razem, gdy "+
        "krowa poruszy uszami. Kr�tka �aciata sier^s� pokrywa jej ko^scisty "+
        "tu��w, pod kt�rym dyndaj^a r�owe wymiona.\n");

    dodaj_przym("stary", "starzy");
    dodaj_przym("�aciaty", "�aciaci");

    set_act_time(30);
    add_act("emote muczy.");
    add_act("emote rozgl^ada si^e powoli.");
    add_act("emote �uje co^s w pysku.");
    add_act("emote muczy g�o^sno.");
    add_act("emote odgania muchy ogonem.");
    add_act("emote potrz�sa �bem.");

    set_stats ( ({ 30, 7, 30, 11, 8 }) );

    set_skill(SS_DEFENCE, 10 + random(4));
    set_skill(SS_UNARM_COMBAT, 15 + random(5));

    set_attack_unarmed(0, 5, 5, W_IMPALE,   50, "rogi");
    set_attack_unarmed(1, 5, 5, W_BLUDGEON, 20, "kopyto", ({"lewy",  "przedni"}));
    set_attack_unarmed(2, 5, 5, W_BLUDGEON, 20, "kopyto", ({"prawy", "przedni"}));
    set_attack_unarmed(3, 5, 5, W_BLUDGEON,  5, "kopyto", ({"lewy",  "tylny"}));
    set_attack_unarmed(4, 5, 5, W_BLUDGEON,  5, "kopyto", ({"prawy", "tylny"}));

    set_hitloc_unarmed(0, ({ 1, 1, 1 }), 10, "�eb");
    set_hitloc_unarmed(1, ({ 1, 1, 1 }), 30, "tu��w");
    set_hitloc_unarmed(2, ({ 1, 1, 1 }), 20, "noga", ({"przedni", "lewy"}));
    set_hitloc_unarmed(3, ({ 1, 1, 1 }), 20, "noga", ({"przedni", "prawy"}));
    set_hitloc_unarmed(4, ({ 1, 1, 1 }), 10, "noga", ({"lewy", "tylny"}));
    set_hitloc_unarmed(5, ({ 1, 1, 1 }), 10, "noga", ({"prawy", "tylny"}));

    add_prop(LIVE_I_NEVERKNOWN, 1);
    add_prop(CONT_I_WEIGHT, 500 + random(100));
    add_prop(CONT_I_HEIGHT, 135 + random(10));

    ustaw_stan_oswojenia(11); //Krowa domowa wi�c oswojona.
}

int query_krowa()
{
    return 1;
}

void emote_hook(string emote, object wykonujacy, string przyslowek)
{
    switch (emote)
    {
        case "pog�aszcz":
            zdenerwowanie = ftoi(sqrt(zdenerwowanie));
            if (wykonujacy->query_skill(SS_ANI_HANDL) > 40)
            pastuch = wykonujacy;
            break;

        case "kopnij":
            set_alarm(1.0, 0.0, "zly", wykonujacy);
            break;

        case "szturchnij":
            set_alarm(1.0, 0.0, "taki_sobie", wykonujacy);
            break;
    }
}

void zly(object kto)
{
    switch (random(5))
    {
        case 0:
            command("emote muczy gro^xnie."); break;
        case 1:
            command("emote muczy g�o^sno."); break;
        case 2:
            command("emote odwraca swoj^a du�^a g�ow� w twoj^a stron�."); break;
        case 3:
            command("emote ryczy g�o�no."); break;
        case 4:
            if (random(20) < zdenerwowanie)
                command("zaatakuj " + OB_NAME(kto));
    }

    zdenerwowanie += 3;
}

void taki_sobie(object kto)
{
    switch (random(3))
    {
        case 0:
            command("emote obraca swoj^a du�^a g�ow� w twoj^a stron�."); break;
        case 1:
            command("emote �uje siano, nie zwracaj^ac na nikogo najmniejszej uwagi."); break;
        case 2:
            command("emote muczy.");
    }

    zdenerwowanie++;
}

void zakoncz_dojenie()
{
    dojenie->stop_paralyze();
    dojona = 0;
}

void attacked_by(object wrog)
{
    ::attacked_by(wrog);
    zdenerwowanie += 10;
    dojona->write("Przestajesz doi� krow�.\n");
    zakoncz_dojenie();
}

int wydoj(string cmd)
{
    mixed *obj;

    if (!cmd || !parse_command(lower_case(cmd), environment(this_player()), "%l:" + PL_BIE + " 'do' %o:" + PL_DOP, obj, pojemnik))
    {
        notify_fail("Wyd�j krow� do czego?\n");
        return 0;
    }

    obj = NORMAL_ACCESS(obj, 0, 0);

    if (sizeof(obj) > 1)
    {
        notify_fail("Chyba �atwiej doi� krowy pojedynczo" + sizeof(obj) + ".\n");
        return 0;
    }

    if (obj[0] != TO)
        return 0;

    if (environment(pojemnik) != TP)
    {
        notify_fail("Naj�atwiej wydoi� krow� do pojemnika, kt�ry masz przy sobie.\n");
        return 0;
    }

    if (pojemnik->query_beczulka() == 0)
    {
        write(capitalize(pojemnik->short(TP, PL_DOP)) + " nie da si� nape�ni� mlekiem.\n");
        return 1;
    }

    if (pojemnik->query_ilosc_plynu() != 0)
    {
        write("W " + pojemnik->short(TP, PL_MIE) + " znajduje si� " +
            (pojemnik->query_ilosc_plynu() > 1000 ? "pe�no " : "nieco ") + pojemnik->query_opis_plynu() + ".\n");
        return 1;
    }

    if (dojona != 0)
    {
        write(capitalize(short(TP, PL_MIA)) + " jest w tej chwili dojona przez " + QIMIE(dojona, PL_BIE) + ".\n");
        return 1;
    }

    if (query_attack() != 0)
    {
        write("Krowa jest w tej chwili atakowana.\n");
        return 1;
    }

    if(!HAS_FREE_HANDS(TP) && !pojemnik->query_wielded())
    {
        write("Musisz mie� wolne r�ce, by m�c to zrobi�.\n");
        return 1;
    }

    if ((random(20) + TP->query_skill(SS_ANI_HANDL) < zdenerwowanie && random(4) != 0) || Dojenia > 2)
    {
        switch (random(3))
        {
        case 0:
            write("Pr�bujesz chwyci� wymiona " + short(TP, PL_DOP) +
                " jednak ta w ostatniej chwili rusza par� krok�w do przodu, niwecz^ac twoje plany.\n");
            saybb(QIMIE(TP, PL_MIA) + " pr�buje chwyci� wymiona " + short(TP, PL_DOP) +
                " jednak ta w ostatniej chwili rusza par^e krok�w do przodu, niwecz^ac " +
                TP->koncowka("jego", "jej") + " plany.\n");
            zdenerwowanie++;
            break;
        case 1:
            write("Pr�bujesz zbli�y� si� do " + short(TP, PL_DOP) + ", jednak ta muczy g�o�no " +
                "i ucieka.\n");
            saybb(QIMIE(TP, PL_MIA) + " pr�buje zbli�y� si� do " + short(TP, PL_DOP) + ", jednak " +
                "ta muczy g�o�no i ucieka.\n");
            zdenerwowanie++;
            break;
        case 2:
            write("Gdy pr�bujesz wydoi� " + short(TP, PL_BIE) + ", rozjuszone zwierz� wydaje z siebie " +
                "g�o�ny ryk i ucieka.\n");
            saybb("Gdy " + QIMIE(TP, PL_MIA) + " pr�buje wydoi� " + short(TP, PL_BIE) + ", rozjuszone " +
                "zwierz� wydaje z siebie g�o�ny ryk i ucieka.\n");
            zdenerwowanie++;
        }
    }
    else if (random(100) < TP->query_skill(SS_ANI_HANDL) + 30)
    {
        write("Umiej�tnie zbli�asz si� do " + short(TP, PL_DOP) + ", chwytasz j� za wymiona i zaczynasz doi�.\n");
        saybb(QIMIE(TP, PL_MIA) + " umiej�tnie zbli�a si� do " + short(TP, PL_DOP) + ", chwyta j� za " +
            "wymiona i zaczyna doi�.\n");

        Dojenia += 1;
        set_alarm(12800.0, 0.0, red_Dojenia);
        dojona = TP;

        pojemnik->set_vol(0);
        pojemnik->set_opis_plynu("�wie�ego mleka prosto od krowy");
        pojemnik->set_nazwa_plynu_dop("mleka");
        pojemnik->set_ilosc_plynu(1);

        dojenie = clone_object("/std/paralyze");
        dojenie->set_fail_message(QIMIE(TP, PL_MIA) + " przestaje doi� " + short(TP, PL_BIE) + ".\n");
        dojenie->set_standard_paralyze("doi�");
        dojenie->set_stop_fun("koniec_dojenia");
        dojenie->set_stop_object(TO);
        dojenie->move(TP);

        alarm_id = set_alarm(4.0, 0.0, dojenie_alarm);
    }
    else
    {
        switch (random(3))
        {
        case 0:
            write("Pr�bujesz chwyci� wymiona " + short(TP, PL_DOP) +
                " jednak ta w ostatniej chwili rusza par� krok�w do przodu, niwecz^ac twoje plany.\n");
            saybb(QIMIE(TP, PL_MIA) + " pr�buje chwyci� wymiona " + short(TP, PL_DOP) +
                " jednak ta w ostatniej chwili rusza par^e krok�w do przodu, niwecz^ac " +
                TP->koncowka("jego", "jej") + " plany.\n");
            zdenerwowanie++;
            break;
        case 1:
            write("Nieumiej�tnie chwytasz wymiona " + short(TP, PL_DOP) +
                " i pr�bujesz je ^sciska� i ci^agn^a� na wszystkie strony, jednak" +
                " niewzruszona krowa nie daje ani kropli mleka.\n");
            saybb(QIMIE(TP, PL_MIA) + " nieumiej�tnie chwyta wymiona " + short(TP, PL_DOP) +
                " i pr�buje je ^sciska� i ci^agn^a� na wszystkie strony, jednak" +
                " niewzruszona krowa nie daje ani kropli mleka.\n");
            zdenerwowanie++;
            break;
        case 2:
            write("Zbli�asz si� z " + pojemnik->short(TP, PL_NAR) + " w r�ku do " + short(TP, PL_DOP) +
                " chc^ac j^a wydoi�, jednak zirytowana twoj^a opiesza�o^sci^a i nieporadno^sci^a krowa" +
                " zaczyna g�o^sno mucze� i mocno kopie kopytem " + pojemnik->short(TP, PL_BIE) + ", kt�r" +
                pojemnik->koncowka("y", "a", "e") + " l^aduje par� s^a�ni dalej.\n");
            saybb(QIMIE(TP, PL_MIA) + " zbli�a si� z " + pojemnik->short(TP, PL_NAR) + " w r�ku do" +
                short(TP, PL_DOP) + " chc^ac j^a wydoi�, jednak zirytowana " + TP->koncowka("jego", "jej") +
                " opiesza�o^sci^a i nieporadno^sci^a krowa zaczyna g�o^sno mucze� i mocno kopie kopytem " +
                pojemnik->short(TP, PL_BIE) + ", kt�r" + pojemnik->koncowka("y", "a", "e") + " l^aduje par�" +
                " s^a�ni dalej.\n");
            pojemnik->move(environment(TP));
            zdenerwowanie += 2;
        }
    }

    return 1;
}

void init()
{
    ::init();

    add_action(&wydoj(), "wyd�j");
}

void koniec_dojenia()
{
    //write("Przestajesz doi� " + short(TP, PL_BIE) + ".\n");
    //saybb(QIMIE(TP, PL_MIA) + " przestaje doi� " + short(TP, PL_BIE) + ".\n");
    remove_alarm(alarm_id);
    dojona = 0;
}

void wylanie_mleka()
{
    string zachowanie = (random(2) ? "g�o�no mucze�" : "nerwowo si� porusza�");
    string wywraca = (random(2) ? "przewraca " : "wywraca ");
    write(capitalize(short(TP, PL_MIA)) + " nagle zaczyna " + zachowanie + " i " +
        wywraca + pojemnik->short(TP, PL_BIE) + ", wylewaj�c ca�e " +
        "mleko na ziemi�.\n");
    saybb("Dojona przez " + QIMIE(dojona, PL_BIE) + " krowa nagle zaczyna " +
        zachowanie + " i " + wywraca + pojemnik->short(TP, PL_BIE) +
        ", wylewaj�c ca�e mleko na ziemi�.\n");
    pojemnik->move(environment(TP));
    pojemnik->set_ilosc_plynu(0);
    zdenerwowanie += 2;
    zakoncz_dojenie();
}

void dojenie_alarm()
{
	if (random(100) < zdenerwowanie)
	{
		wylanie_mleka();
		return;
	}
	else if (random(60) > dojona->query_skill(SS_ANI_HANDL) + 30)
	{
		switch (random(3))
		{
		case 0:
			write("Zdenerwowana twoimi nieporadnymi ruchami krowa wierzga nerwowo kopytami.\n");
			saybb(capitalize(short(TP, PL_MIA)) + " zdenerwowana nieporadnymi ruchami " +
				QIMIE(dojona, PL_MIA) + " wierzga nerwowo kopytami.\n");
			pojemnik->set_ilosc_plynu(pojemnik->query_ilosc_plynu() + 200);
			zdenerwowanie++;
			break;
		case 1:
			write("Z ca�ych si� uciskasz wymiona " + short(TP, PL_DOP) + ", jednak sp�ywa z nich " +
				"ledwo par^e kropel mleka.\n");
			saybb(QIMIE(dojona, PL_MIA) + " z ca�ych si� uciska wymiona " + short(TP, PL_DOP) +
				", jednak sp�ywa z nich ledwo par^e kropel mleka.\n");
			pojemnik->set_ilosc_plynu(pojemnik->query_ilosc_plynu() + 10);
			zdenerwowanie += 2;
			break;
		case 2:
			wylanie_mleka();
			return;
		}
	}
	else
	{
		switch (random(4))
		{
		case 0:
			write("Ci�gniesz mocno " + short(TP, PL_BIE) + " za wymiona, z kt�rych " +
				"leje si� �wie�e mleko prosto do " + pojemnik->short(TP, PL_DOP) + ".\n");
			saybb(QIMIE(dojona, PL_MIA) + " ci�gnie mocno " + short(TP, PL_BIE) +
				" za wymiona, z kt�rych leje si� �wie�e mleko prosto do " +
				pojemnik->short(TP, PL_DOP) + ".\n");
			break;
		case 1:
			write("Sprawnie uciskasz wymiona " + short(TP, PL_DOP) +
				", z kt�rych leje si� ciep�e mleko.\n");
			saybb(QIMIE(dojona, PL_MIA) + " sprawnie uciska wymiona " +
				short(TP, PL_DOP) + ", z kt�rych leje si� mleko.\n");
			break;
		case 2:
			write("Z wpraw� doisz " + short(TP, PL_BIE) + ", kt�rej mleko �cieka do " +
				pojemnik->short(TP, PL_DOP) + ".\n");
			saybb(QIMIE(dojona, PL_MIA) + " z wpraw� doi " + short(TP, PL_BIE) +
				", kt�rej mleko �cieka do " + pojemnik->short(TP, PL_DOP) + ".\n");
			break;
		case 3:
			write("Z zapa�em doisz " + short(TP, PL_BIE) + ".\n");
			saybb(QIMIE(dojona, PL_MIA) + " z zapa�em doi " + short(TP, PL_BIE) + ".\n");
			break;
		}

		pojemnik->set_ilosc_plynu(pojemnik->query_ilosc_plynu() + 400);
	}

	if (pojemnik->query_ilosc_plynu() >= pojemnik->query_pojemnosc())
	{
		write("Nape�niwszy " + pojemnik->short(TP, PL_BIE) + " mlekiem" +
			" przestajesz doi� " + short(TP, PL_BIE) + ".\n");
		saybb(QIMIE(TP, PL_MIA) + " nape�niwszy " + pojemnik->short(TP, PL_BIE) +
			" mlekiem przestaje doi� " + short(TP, PL_BIE) + ".\n");
		pojemnik->set_ilosc_plynu(pojemnik->query_pojemnosc());
		zakoncz_dojenie();
	}
	else
		alarm_id = set_alarm(4.0, 0.0, dojenie_alarm);
}

