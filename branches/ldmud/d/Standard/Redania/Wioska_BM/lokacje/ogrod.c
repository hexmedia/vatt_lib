/*
 * Opis: Vortak
 * Kod: Gjoef
 *     Maj 2007
 */

#include <macros.h>
#include <stdproperties.h>
#include <mudtime.h>
#include <pogoda.h>
#include "dir.h"

inherit WIOSKA_STD;

string
trawa()
{
     string str = "";
     
     if (CZY_JEST_SNIEG(TO))
         str = "Porastaj^aca podw^orko trawa jest teraz schowana pod warstw^a ^sniegu.\n";
     else
     {
         if (pora_roku() == MT_WIOSNA || pora_roku() == MT_LATO)
             str = "R^owno przyci^eta trawa o kolorze soczystej zieleni porasta to podw^orko.\n";
         else
             str = "Lekko po^z^o^lk^la trawa porasta to podw^orko.\n";
     }
     
     return str;
}

void
create_wioska()
{
    set_short("Niewielkie podw^orko");

    add_object(WIOSKA_DRZWI + "do_soltysa");
    add_object(WIOSKA_DRZWI + "z_ogrodu");
    
    add_item(({"p^lot", "ogrodzenie"}), "Wysoki na jakie^s sze^s^c st^op " +
    "p^lot otacza zadban^a posiad^lo^s^c.\n");
    
    add_item(({"psa", "psa za zagrod^a"}), "Wielki kud^laty pies zamkni^ety " +
    "jest za zagrod^a.\n");
    
    add_item(({"dom", "domek", "drewniany domek"}), "Ma^ly, ale ^ladny i za" +
    "dbany domek z drewna stoi na wprost wej^sciowej furtki na podw^orko.\n");
    
    add_item("traw^e", "@@trawa@@");

    add_item(({"grz^adki", "warzywne grz^adki", "warzywa"}), "@@grzadki@@");
}

string
exits_description()
{
    return "Furtka prowadzi na drog^e, s^a tu tak^ze drzwi do domu so^ltysa.\n";
}

string
dlugi_opis()
{
    string str = "Niewielkie, otoczone bia^lym p^lotem, zadbane podw^orze " +
    "przed drewnianym domkiem. Mi^edzy drzwiami wej^sciowymi do budynku a " +
    "furtk^a ci^agnie si^e chodnik u^lo^zony ze ^sci^sle do siebie " +
    "przylegaj^acych kamieni. ";
    
    if (!CZY_JEST_SNIEG(TO))
    {
        str += "Kr^otko przystrzy^zona";

        if (pora_roku() == MT_WIOSNA || pora_roku() == MT_LATO)
            str += ", soczy^scie zielona trawa i zadbane, r^owno wytyczone " +
            "warzywne grz^adki sugeruj^a";
        else
            str += " lekko po^z^o^lk^la trawa sugeruje";
    }
    else
        str += "Dok^ladnie od^snie^zona ^scie^zka sugeruje";

        str += ", ^ze znalaz^le^s si^e u solidnego gospodarza. Do domku " +
        "przylega wysoka, zamkni^eta na skobel zagroda, za kt^or^a miota " +
        "si^e pot^e^zny, pilnuj^acy nocami porz^adku kud^laty pies. Za " +
        "niewielk^a furtk^a wida^c dr^o^zk^e, ci^agn^ac^a si^e a^z do " +
"widocznego na po^ludniu traktu i ";
        
        if (CZY_JEST_SNIEG(TO))
            "przysypan^a warstw^a bia^lego puchu ";
            
        str += "tutejsz^a karczm^e.\n";
    
    return str;
}
