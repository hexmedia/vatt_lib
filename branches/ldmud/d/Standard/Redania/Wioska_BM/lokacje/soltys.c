#include <macros.h>
#include <stdproperties.h>
#include "dir.h"

inherit REDANIA_STD;
inherit "/lib/peek";

string
dlugi_opis()
{
    string str = "Na wyposa^zenie ";

    if (TO->jest_dzien())
        str += "jasno o^swietlonej wpadaj^acymi przez okno promieniami ";

    str += "tej niezbyt przestronnej izby sk^lada si^e raptem kilka " +
    "niezbyt wyszukanych, aczkolwiek sprawiaj^acych wra^zenie solidnych " +
    "i u^zytecznych mebli. Pod jedn^a ze ^scian stoi wysoka a^z po bielony " +
    "sufit dwudrzwiowa szafa, a tu^z obok niej szerokie ^l^o^zko, pod " +
    "kt^orym widzisz skrzyni^e s^lu^z^ac^a do przechowywania po^scieli. " +
    "Schludnie zamieciona d^ebowa klepka le^z^aca na pod^lodze sprawia " +
    "wra^zenie nie tak dawno odnawianej. Pod oknem stoi masywne, " +
    "wyr^o^zniaj^ace si^e eleganckimi zdobieniami biurko.\n";

    return str;
}

public void
create_redania()
{
    set_short("W zadbanej izbie");

    set_long("@@dlugi_opis@@");

    add_prop(ROOM_I_INSIDE, 1);
    add_prop(ROOM_I_LIGHT, 1);

    add_object(WIOSKA_DRZWI + "z_soltysa");
    add_object(WIOSKA_PRZEDMIOTY + "szafa");
    add_object(WIOSKA_PRZEDMIOTY + "skrzynia");
    add_object(WIOSKA_PRZEDMIOTY + "lozko");
    add_object(WIOSKA_PRZEDMIOTY + "biurko");

    add_npc(WIOSKA_NPC + "soltys", 1, "czy");

    add_item(({"skrzyni^e", "skrzyni^e na po^sciel"}), "Niedu^za skrzynia " +
    "na po^sciel umieszczona pod ^l^o^zkiem.\n");

    add_item("okno", "Proste, ale niema^le okno wychodzi na podw^orko przed domem.\n");

    dodaj_rzecz_niewyswietlana_plik(WIOSKA_PRZEDMIOTY + "szafa");
    dodaj_rzecz_niewyswietlana_plik(WIOSKA_PRZEDMIOTY + "biurko");
    dodaj_rzecz_niewyswietlana_plik(WIOSKA_PRZEDMIOTY + "lozko");

    add_peek("przez okno", WIOSKA_LOKACJE + "ogrod");

    add_sit("na pod^lodze", "na pod^lodze", "z pod^logi", 0);
}

int czy()
{
    if (find_living("karel") == 0)
    return 1;
    return 0;
}

string
exits_description()
{
    return "Wyj^scie prowadzi na podw^orko.\n";
}

void
init()
{
    ::init();
    init_peek();
}
