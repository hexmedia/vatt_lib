#include <stdproperties.h>
#include <pl.h>
#include <materialy.h>
#include "dir.h"

inherit "/std/key";

void
create_key()
{
    ustaw_nazwe("klucz");

    set_long("Du^zy, pozbawiony wszelkich zdobie^n klucz.\n");

    dodaj_przym("du^zy", "duzi");
    dodaj_przym("prosty", "pro^sci");

    set_key("WIOSKA_KARCZMA_KLUCZ");
}
