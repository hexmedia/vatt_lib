#include <stdproperties.h>
#include <materialy.h>
#include "dir.h"

inherit "/std/door";

void
create_door()
{
    ustaw_nazwe("drzwi");
    dodaj_przym("krzywy", "krzywi");
    dodaj_przym("nadpr^ochnia^ly", "nadpr^ochniali");

    set_other_room(WIOSKA_LOKACJE + "wioska10.c");
    set_door_id("WIOSKA_CHATA1");
    set_door_desc("Funkcj^e drzwi pe^lni tu nic innego jak kilka starych, " +
    "nadpr^ochnia^lych desek zbitych w kup^e zardzewia^lymi gwo^xdziami.\n");

    set_pass_command("wyj^scie", "na zewn^atrz", "z chaty");
    set_open_desc("");
    set_closed_desc("");

    ustaw_material(MATERIALY_DR_SOSNA);
}
