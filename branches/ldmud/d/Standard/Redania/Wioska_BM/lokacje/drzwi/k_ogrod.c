#include <stdproperties.h>
#include <pl.h>
#include <materialy.h>
#include "dir.h"

inherit "/std/key";

void
create_key()
{
    ustaw_nazwe("klucz");
    dodaj_nazwy("kluczyk");

    set_long("Ma^ly, pozbawiony wszelkich zdobie^n kluczyk.\n");

    dodaj_przym("ma^ly", "mali");
    dodaj_przym("stalowy", "stalowi");

    set_key("WIOSKA_OGROD_KLUCZ");
}
