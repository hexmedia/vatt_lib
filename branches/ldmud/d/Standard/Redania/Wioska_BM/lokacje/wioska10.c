/*
 * Opis: Vortak
 * Kod: Gjoef
 *     Maj 2007
 */

#include <macros.h>
#include <stdproperties.h>
#include <mudtime.h>
#include <pogoda.h>
#include "dir.h"

inherit WIOSKA_STD;

void
create_wioska()
{
    set_short("Na skraju wioski");

    add_exit("wioska9.c", "e");
    
    add_object(WIOSKA_DRZWI + "do_kurnika");
    add_object(WIOSKA_DRZWI + "do_chaty1");
}

string
exits_description()
{
    return "^Scie^zka biegnie na wsch^od. Tu za^s znajduj^a si^e drzwi " +
    "jednej z wiejskich chat i wej^scie do kurnika.\n";
}

string
dlugi_opis()
{
    string str = "^Scie^zka, ledwo widoczna ";

    if (CZY_JEST_SNIEG(TO))
        str += "w^sr^od pokrywaj^acego okolic^e bia^lego puchu ";
    else
        str += "mi^edzy wysokimi chwastami tworz^acymi na poboczu prawdziw " +
        "^a g^estwin^e ";

    str += "^l^aczy widoczny kawa^lek na wschodzie placyk z brudnym " +
    "podw^orkiem, na kt^orym w^la^snie si^e znajdujesz. Dooko^la wida^c " +
    "kilka ubogich, chaotycznie rozrzuconych cha^lupek i zabudowa^n " +
    "gospodarczych. Skar^lowacia^le, powykr^ecane i schorowane drzewka " +
    "rosn^ace tu i ^owdzie sprawiaj^a wra^zenie, jakby od lat nie dawa^ly " +
    "owoc^ow. ";

    if (TEMPERATURA(TO) >= 1)
        str += "Wok^o^l pe^lno jest ka^lu^z wype^lnionych brudn^a, m^etn^a " +
        "wod^a.";
    else
        str += "Ka^lu^ze wype^lniaj^ace bruzdy w ziemi zamarz^ly, tworz^ac " +
        "brudny, m^etny l^od."; //??
    
    str += "\n";
    
    return str;
}
