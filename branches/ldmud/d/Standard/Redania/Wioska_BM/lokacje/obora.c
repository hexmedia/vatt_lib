/*
 * To Enkin, to Enkin!
 */

#include <stdproperties.h>
#include <macros.h>
#include "dir.h"

inherit REDANIA_STD;

void
create_redania()
{
    set_short("Du^za obora");

    set_long("Stare, nadpr�chnia�e deski stanowi�ce kiedy� z pewno�ci� cz�� " +
		"stropu obory le�� teraz na go�ej, gdzieniegdzie pokrytej nadgni�� s�om� " +
		"ziemi. Brak jakichkolwiek drzwi i rozleg�e szpary mi�dzy balami w " +
		"�cianach �wiadcz�, i� gospodarz tej obory zupe�nie nie przyk�ada wagi " +
		"do nale�ytego traktowania zwierz�t gospodarskich nara�onych na ch��d " +
		"zimowego wiatru i jesienny deszcz. W ziemi� pod jedn� ze �cian wbity " +
		"zosta� masywny drewniany ko�ek, do kt�rego zapewne przywi�zuje si� " +
		"zwierz�ta na noc.\n");

    add_prop(ROOM_I_INSIDE, 1);

	add_exit("wioska9.c", ({"wyj�cie","w kierunku wyj�cia","z obory"}));

	clone_object(WIOSKA_NPC + "krowa.c")->move(TO);

	add_item(({"ko^lek", "drewniany ko^lek"}), "Drewniany ko^lek stoi tu, " +
	"by m^oc przywi^aza^c do^n zwierz^e na noc.\n");
	add_item("deski", "B^ed^ace niegdy^s elementem stropu deski spr^ochnia^ly " +
	"i pop^ekane, cz^e^sciowo run^e^ly na ziemi^e.\n");
}

string exits_description()
{
    return "Wyj^scie z obory prowadzi na zewn^atrz.\n";
}
