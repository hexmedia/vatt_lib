#include <stdproperties.h>
#include <macros.h>
#include <materialy.h>
#include "dir.h"

inherit "/std/object";

create_object()
{
   ustaw_nazwe("szyld");
   set_long("Kilka nieoheblowanych desek zbitych ze sob^a zardzewia^lymi " +
   "gwo^xdziami. Wiele lat temu miejscowy artysta namalowa^l na niej " +
   "krzykliwymi kolorami jak^a^s straszliw^a besti^e... jednak napis " +
   "widoczny powy^zej utwierdza ci^e w przekonaniu, ^ze jest to tylko " +
   "poczciwy ba^zant.\n");
   
   ustaw_material(MATERIALY_DR_SOSNA);
   
   add_prop(OBJ_I_WEIGHT, 5000);
   add_prop(OBJ_I_VOLUME, 6000);
   add_prop(OBJ_I_NO_GET, "Szyld wisi zbyt wysoko.\n");
   add_prop(OBJ_I_DONT_GLANCE, 1);
   add_cmd_item(({"szyld", "szyld karczmy", "szyld nad karczm^a", "napis " +
   "na szyldzie", "napis na szyldzie karczmy"}),
                "przeczytaj", "Ko^slawe litery uk^ladaj^a si^e w s^lowa, " +
                "b^ed^ace jednocze^snie nazw^a karczmy: \"Z^LOTY BA^ZANT\".\n",
                "Przeczytaj co?\n");
}
