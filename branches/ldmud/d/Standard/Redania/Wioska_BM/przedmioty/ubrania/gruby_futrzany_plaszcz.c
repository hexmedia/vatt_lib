/*
 * do wioski ko^lo BM
 * opis by Vortak lub  kogo^s ze ^swity jego
 * zepsu^la faeve
 * dn. 18.05.2007
 *
 */

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <pl.h>
#include <object_types.h>
#include <macros.h>
#include <materialy.h>

void
create_armour()
{ 

    ustaw_nazwe("plaszcz");
    dodaj_przym("gruby","grubi");
    dodaj_przym("futrzany", "futrzani");
    set_long("Gruby, d^lugi p^laszcz wykonany zosta^l z idealnie skrojonych "+
		"i wygarbowanych kawa^lk^ow sk^ory. Pokryty d^lugim, g^estym "+
		"w^losiem sprawia wra^zenie nad podziw ciep^lego - i faktycznie, "+
		"otulaj^ac si^e nim mo^zna przetrwa^c nawet najwi^eksze mrozy. "+
		"Materia^l, z kt^orego powsta^lo to okrycie sprawia wra^zenie "+
		"sztywnego i nieprzyjemnego jednak przy dotyku okazuje si^e by^c "+
		"mi^ekki i ca^lkiem przyjemny. Dzi^eki posrebrzanym zapinkom "+
		"p^laszcz mo^zna wygodnie zapi^a^c. Dodatkow^a os^lon^e przed zimnem "+
		"stanowi obszerny kaptur, r^ownie^z obszyty popielatym futrem "+
		"kontrastuj^acym z ciemnym w^losiem pokrywaj^acym reszt^e ubioru. \n");


    ustaw_material(MATERIALY_RZ_STAL , 5);
	ustaw_material(MATERIALY_SZ_SREBRO, 2);
    ustaw_material(MATERIALY_SK_BYDLE, 93);
    set_type(O_UBRANIA);
    set_slots(A_CLOAK);
    add_prop(OBJ_I_WEIGHT, 2502);
    add_prop(OBJ_I_VOLUME, 1190);
    add_prop(OBJ_I_VALUE, 130);
    set_size("M");
}