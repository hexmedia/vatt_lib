#include <macros.h>
#include <object_types.h>
#include <stdproperties.h>
#include "dir.h"

inherit "/std/container";

create_container()
{
    ustaw_nazwe("lada");
    
    set_long("Pod jedn^a ze ^scian stoi d^luga, wykonana z dok^ladnie " +
    "oheblowanego jasnego drewna lada. @@opis_sublokacji|Na jej blacie |na|" +
    ".||0|le^zy |le^z^a |le^zy @@\n");
    
    add_subloc("na");
    add_subloc_prop("na", CONT_I_MAX_WEIGHT, 500000);
    add_subloc_prop("na", CONT_I_MAX_VOLUME, 500000);
    add_subloc_prop("na", SUBLOC_I_MOZNA_POLOZ, 0);
    add_subloc("w");
    add_prop(CONT_I_CANT_WLOZ_DO, 1);
    
    add_prop(OBJ_M_NO_GET, "Lada jest zbyt ci^e^zka.\n");
    add_prop(CONT_I_DONT_SHOW_CONTENTS, 1);
    add_prop(OBJ_I_DONT_GLANCE, 1);
    
    set_owners(({WIOSKA_NPC + "karczmarz"}));
    
    set_type(O_MEBLE);
}
