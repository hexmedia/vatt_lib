#include <stdproperties.h>
#include <object_types.h>
#include "dir.h"

inherit "/std/object";

string
dlugij()
{
    string str = "Niewysoka studnia z pociemnia^lego przez lata drewna. " +
    "Jej cembrowina jest czworoboczna, za^s daszek wspiera si^e na czterech " +
    "prostych s^lupach. Nabieranie wody umo^zliwia masywny drewniany " +
    "ko^lowr^ot, na kt^ory nawini^eto kawa^l konopnego sznura";

    if (2 + 2 == 4)
        str += ", niestety nigdzie w pobli^zu nie wida^c ^zadnego wiadra. ";
    else
        str += " z przywi^azanym do^n wiadrem. ";
        
    str += "Ca^l^a konstrukcj^e wprawia si^e w ruch imponuj^acych rozmiar^o" +
    "w korb^a. Gdzie^s g^l^eboko, na samym dnie studni dostrzegasz tafl^e " +
    "wody.\n";
    
    return str;
}

create_object()
{
    ustaw_nazwe("studnia");
    
    set_long("@@dlugij@@"); 
    
    add_prop(OBJ_I_WEIGHT, 666*666);
    add_prop(OBJ_I_VOLUME, 660181);
    add_prop(OBJ_M_NO_GET, "Nawet je^sli uda^loby ci si^e ud^xw" +
    "ign^a^c konstrukcj^e, jest ona wkopana g^l^eboko w ziemi^e.\n");
}
