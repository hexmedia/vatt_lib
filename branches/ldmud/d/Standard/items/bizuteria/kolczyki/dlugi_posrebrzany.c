
/* Autor: Avard
   Opis : Myria
   Data : 28.07.05 */

inherit "/std/kolczyk.c";
#include <pl.h>
#include "/sys/stdproperties.h"
#include <materialy.h>
#include <object_types.h>

void
create_kolczyk()
{
        dodaj_przym("d^lugi","d^ludzy");
        dodaj_przym("posrebrzany","posrebrzani");

        set_long("Jest to nader elegancki kolczyk, delikatnie kryty czystym "+
        "srebrem. Sk^lada si^e on z dw^och drobnych, przeplataj^acych si^e "+
        "ze sob^a k^o^lek i dziesi^atek odchodz^acych od nich cienkich, "+
        "b^lyszcz^acych la^ncuszk^ow srebra. Zako^nczone s^a one drobnymi "+
        "^lzami tego^z metalu, a w ka^zd^a z ^lez wtopiono niewielk^a "+
        "drobinke bursztynu. Ca^lo^s^c ^swiadczy o dobrym gu^scie "+
        "w^la^sciciela i ^swietnie prezentuje si^e na wszelakich balach i "+
        "uroczysto^sciach.\n");

    add_prop(OBJ_I_VOLUME, 90);
    add_prop(OBJ_I_WEIGHT, 15);
    add_prop(OBJ_I_VALUE, 120);
    ustaw_material(MATERIALY_SREBRO, 100);
    set_type(O_BIZUTERIA);
}