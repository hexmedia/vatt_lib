
/* Zwykle plocienne spodnie
Wykonano dnia 11.08.2005 przez Avarda. Opis napisany przez Tinardan 
Poprawki by Avard 20.05.06 */


inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <macros.h>
#include <materialy.h>
#include <object_types.h>
#include <pl.h>

void
create_armour()
{
	ustaw_nazwe("spodnie");
	dodaj_przym("zwyk^ly","zwykli");
	dodaj_przym("p^l^ocienny","p^l^ocienni");

	set_long("Ka^zdy miejski krawiec ma na swoim koncie setki takich spodni."+
		"Pl^otno wygl^ada na mocne i trudne to przetarcia, a kr^oj dopasowany"+
		"do ka^zdej sylwetki - szerokie nogawki i wi^azanie w pasie przypasuje"+
		"niemal wszystkim, zar^owno podr^o^znikom jak i mieszczanom czy "+
		"ch^lopom. \n");

	set_slots(A_LEGS, A_HIPS);
    add_prop(OBJ_I_WEIGHT, 500);
    add_prop(OBJ_I_VOLUME, 1000);
    add_prop(OBJ_I_VALUE, 40);
    set_type(O_UBRANIA);
    ustaw_material(MATERIALY_LEN, 100);
    add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    set_size("S");
}

string
query_auto_load()
{
   return ::query_auto_load();
}