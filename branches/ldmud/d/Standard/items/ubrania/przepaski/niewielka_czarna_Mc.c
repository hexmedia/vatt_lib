
/* Niewielka czarna przepaska
   Wykonana przez Avarda, dnia 19.05.06 
   Opis by Tinardan */

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <pl.h>
#include <macros.h>
#include <materialy.h>
#include <object_types.h>

void
create_armour()
{
    ustaw_nazwe("przepaska");
    dodaj_przym("niewielki","niewielcy");
    dodaj_przym("czarny","czarni");
    set_long("Na pierwszy rzut oka zdaje si^e to by^c jedynie pasek "+
        "l^sni^acego materia^lu, ale ju^z po uwa^zniejszym przyjrzeniu "+
        "si^e mo^zna spostrzec, ^ze tkanina to nic innego jak jedwab, a "+
        "sam pasek jest zgrabnie uszyt^a przepask^a na g^low^e, "+
        "przytrzymuj^ac^a w^losy. Jest pokryta czarnym haftem, r^ownie "+
        "czarnym jak ona sama, co czyni go ledwo widocznym z daleka. "+
        "Haft przedstawia lec^acego or^la z otwartym dziobem i z^lot^a "+
        "nitk^a wplecion^a w miejscu ^xrenicy.\n");

    set_slots(A_HEAD);
    add_prop(OBJ_I_VOLUME, 200);
    add_prop(OBJ_I_WEIGHT, 100);
    add_prop(OBJ_I_VALUE, 40);
    add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    add_prop(ARMOUR_I_D_ROZCIAGLIWOSC, 30);
    add_prop(ARMOUR_I_U_ROZCIAGLIWOSC, 70);
    set_size("M");
    ustaw_material(MATERIALY_JEDWAB, 100);
    set_type(O_UBRANIA);

}
string
query_auto_load()
{
   return ::query_auto_load();
}