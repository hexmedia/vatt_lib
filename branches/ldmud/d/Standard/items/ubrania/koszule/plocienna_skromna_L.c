/* plocienna skromna koszula
 * Wykonanie: Lil, Opis: Dinrahia(tudziez:Aneta)
 */

inherit "/std/armour";
#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include "/sys/macros.h"
#include "materialy.h"
#include "object_types.h"

void
create_armour()
{
    ustaw_nazwe("koszula");
    dodaj_przym("p��cienny","p��cienni");
    dodaj_przym("skromny","skromni");
    set_long("Ta wykonana z p��tna koszula wyposa�ona zosta�a w szereg "+
             "drewnianych guzik�w. Koszula nie posiada ozd�b, chyba �e za "+
             "tak� uchodzi szew, kt�rego kolor jest o ton ciemniejszy ni� "+
             "reszta materia�u. Na pewno jest jednak solidna, a szyj� chroni "+
             "przed wiatrem niezbyt sztywny ko�nierz.\n"); 
    set_slots(A_TORSO, A_FOREARMS, A_ARMS);
    set_size("L");
    add_prop(OBJ_I_WEIGHT, 612);
    add_prop(OBJ_I_VOLUME, 950);
    add_prop(OBJ_I_VALUE, 9);
    ustaw_material(MATERIALY_LEN);
    set_type(O_UBRANIA);
    add_prop(ARMOUR_I_D_ROZCIAGLIWOSC, 30);
    add_prop(ARMOUR_I_U_ROZCIAGLIWOSC, 30);
    add_prop(ARMOUR_F_PRZELICZNIK, 60.0); //koszula jest bardzo rozciagliwa.

}

string
query_auto_load()
{
   return ::query_auto_load();
}