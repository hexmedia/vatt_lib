
/* Stara plocienna koszula
Wykonano dnia 23.07.2005 przez Avarda. Opis zaczerpniety z banku opisow,
napisany przez Artonula w dziale "ubrania i inne przedmioty" 
dnia 20.05.2005*/

inherit "/std/armour";
#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include "/sys/macros.h"
#include "materialy.h"
#include "object_types.h"



void
create_armour()
{
    ustaw_nazwe("koszula");
    dodaj_przym("stary","starzy");
    dodaj_przym("p��cienny","p��cienni");

    set_long("Ta wykonana przed laty z niskiej jako�ci lnu "+
             "koszula czas swojej '�wietno�ci' ma ju� zdecydowanie "+
             "dawno za sob�. W wielu miejscach lata intensywnego "+
             "u�ytkowania pozostawi�y po sobie "+
	     "wyra�ne �lady w postaci dziur i przetar�. Koszula ta "+
             "na pewno nie jest szczytem mody ani wygody i mo�e s�u�y� "+
             "jako ubranie tylko w ostateczno�ci.\n"); 
    set_slots(A_TORSO, A_FOREARMS, A_ARMS);
    //set_size("S");
    add_prop(ARMOUR_S_DLA_RASY, "gnom");
    add_prop(OBJ_I_WEIGHT, 912);
    add_prop(OBJ_I_VOLUME, 500);
    add_prop(OBJ_I_VALUE, 7);
    ustaw_material(MATERIALY_LEN);
    set_type(O_UBRANIA);
    add_prop(ARMOUR_I_D_ROZCIAGLIWOSC, 30);
    add_prop(ARMOUR_I_U_ROZCIAGLIWOSC, 30);
    add_prop(ARMOUR_F_PRZELICZNIK, 60.0); //koszula jest bardzo rozciagliwa.

}

string
query_auto_load()
{
   return ::query_auto_load();
}