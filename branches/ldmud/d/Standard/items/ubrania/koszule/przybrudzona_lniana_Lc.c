
/*
 * Przybrudzona lniana koszula.
 *
 * Autor: Caandil
 */

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <macros.h>
#include <materialy.h>
#include <object_types.h>
#include <pl.h>

create_armour()
{
    ustaw_nazwe("koszula");
    dodaj_przym("przybrudzony","przybrudzeni");
    dodaj_przym("lniany","lniani");
    set_long("Plamy i ^laty wyra^xnie odznaczaj^a si^e na jasnym materiale koszuli. "+
		     "Mankiety d^lugich r^ekaw^ow i ko^lnierz strz^epi^a si^e na ko^ncach i gubi^a "+
		     "d^lugie szare nitki. Koszula prawdopodobnie by^la kiedy^s bia^la, ale teraz "+
	         "kolorem przypomina raczej py^l unosz^acy si^e nad brukowanymi ulicami miast. "+
		     "Niemniej jednak sprawia wra^zenie ciep^lej i do^s^c wygodnej, a materia^l "+
		     "jest mi^ekki i przyjemny w dotyku.\n");

    ustaw_material(MATERIALY_TK_LEN);
    set_type(O_UBRANIA);
    set_size("L");
    set_slots(A_TORSO, A_FOREARMS, A_ARMS);
    
    add_prop(OBJ_I_WEIGHT, 300);
    add_prop(OBJ_I_VOLUME, 200);
    add_prop(OBJ_I_VALUE, 1500);
    add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    
}
