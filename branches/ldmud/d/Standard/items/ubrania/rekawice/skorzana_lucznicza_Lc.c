/* Skorzana lucznicza rekawiczka
   Wykonany przez Eria 18.06.06
   Opis by Vortak z BO */

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <pl.h>
#include <macros.h>
#include <materialy.h>
#include <object_types.h>

void
create_armour()
{ 
    ustaw_nazwe("r^ekawiczka");
    dodaj_przym("sk^orzany","sk^orzani");
    dodaj_przym("^luczniczy", "^lucznicy");
    set_long("Ciemnobr�zowa sk�rzana r�kawiczka, chroni palce prawej d�oni "+
         "podczas napinania �uku. Uszyta z wytrzyma�ego materia�u, "+
         "dodatkowo posiada wzmocnienie na palcach co znacznie zwi�ksza jej "+
         "trwa^lo^s^c.\n"); 

    ustaw_material(MATERIALY_SK_SWINIA, 100);
    set_type(O_UBRANIA);
    set_slots(A_R_HAND);
    add_prop(OBJ_I_WEIGHT, 10);
    add_prop(OBJ_I_VOLUME, 20);
    add_prop(OBJ_I_VALUE, 50);
    add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    set_size("L");

}