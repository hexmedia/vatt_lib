/*
 * Gruba we�niana czapka
 *
 * Autor: Lil, opis: Dinrahia(tudzie�: Aneta)
 */

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <macros.h>
#include <materialy.h>
#include <object_types.h>

void
create_armour()
{  
  
    ustaw_nazwe("czapka");
    dodaj_przym("gruby","grubi");
    dodaj_przym("we�niany","we�niani");
    set_long("Ta ciemna, we�niana i pozbawiona ozd�b czapka sprawia "+
             "wra�enie ciep�ej oraz wytrzyma�ej. Mimo, �e nie jest "+
             "szczytem elegancji, to ka�dy, kto doswiadczy� przedzierania "+
             "si� przez zamiecie �nie�ne spojrzy na ni� przychylnym okiem. "+
             "Poszukiwacz przyg�d m�g�by wygl�da� zabawnie, ale kto by si� "+
             "przejmowa�, kiedy w uszy ciep�o.\n");
    set_slots(A_HEAD);
    add_prop(OBJ_I_WEIGHT, 220);
    add_prop(OBJ_I_VOLUME, 1200);
    add_prop(OBJ_I_VALUE, 9);
    add_prop(ARMOUR_F_PRZELICZNIK, 40.0);
    /* ustawiamy materia� */
    ustaw_material(MATERIALY_WELNA);
    set_type(O_UBRANIA);
    set_likely_cond(30);
}
string
query_auto_load()
{
   return ::query_auto_load();
} 
