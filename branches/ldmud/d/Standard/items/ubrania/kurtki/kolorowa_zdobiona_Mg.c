
/* Kolorowa zdobiona kurtka
Wykonano dnia 21.06.2005 przez Avarda. Opis zaczerpniety z banku opisow,
napisany przez Ghalleriana w dziale "ubrania i inne przedmioty" 
Poprawki by Avard dnia 20.05.06 */

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <macros.h>
#include <materialy.h>
#include <object_types.h>
#include <pl.h>


void
create_armour()
{
    ustaw_nazwe("kurtka");
    dodaj_przym("kolorowy","kolorowi");
    dodaj_przym("zdobiony","zdobieni");

    set_long("Krawiec mo^ze by^c dumny ze swojego dzie^la. Ubranie, "+
        "kt^ore wykona^l jest wspania^le. Od razu wida^c, ^ze kurta "+
        "zosta^la uszyta dla kogo� wa^znego. Wykonana zosta^la z "+
        "najlepszych materia^l^ow, kt^ore zszyte w ca^lo�^c cudownie "+
        "komponuj^a si^e ze z^lotymi zdobieniami na r^ekawach. Klamry "+
        "do zapinania wykonane s^a ze srebra za� wszystkie sznureczki "+
        "zawieraj^a z^lote nitki. Na plecach mo^zna zauwa^zy^c przepi^ekny "+
        "haft przedstawiaj^acy lec^acego ptaka. Kurta posiada materia^ly "+
        "chyba w ka^zdym kolorze jaki istnieje.\n");

    set_slots(A_TORSO, A_FOREARMS, A_ARMS);
    add_prop(OBJ_I_WEIGHT, 2000);
    add_prop(OBJ_I_VOLUME, 1000);
    add_prop(OBJ_I_VALUE, 280);
    set_type(O_UBRANIA);
    ustaw_material(MATERIALY_AKSAMIT, 49);
    ustaw_material(MATERIALY_JEDWAB, 20 );
    ustaw_material(MATERIALY_ATLAS, 20);
    ustaw_material(MATERIALY_SREBRO, 10);
    ustaw_material(MATERIALY_ZLOTO, 1);
    add_prop(ARMOUR_S_DLA_RASY, "gnom");
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    set_size("M");
}
string
query_auto_load()
{
   return ::query_auto_load();
}