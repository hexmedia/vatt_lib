
/* kolorowy ekstrawagancki kubrak
   Wykonana przez Avarda, dnia 07.06.06 
   Opis by Tinardan */
/* Uzyte w:
Trubadur uliczny z Rinde (/d/Standard/Redania/Rinde/npc/trubadur_uliczny.c);
*/

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <pl.h>
#include <macros.h>
#include <materialy.h>
#include <object_types.h>
void
create_armour()
{
    ustaw_nazwe("kubrak");
    dodaj_przym("kolorowy","kolorowi");
    dodaj_przym("ekstrawagancki","ekstrawaganccy");

    set_long("Ju^z z daleka mo^zna si^e domy^sli^c, do kogo taki kubrak "+
        "nale^zy, albo przynajmniej powinien nale^ze^c. Pozszywany jest z "+
        "d^lugich pas^ow materia^lu w najr^o^zniejszych kolorach, od "+
        "najbardziej krzykliwych, do tych nieco stonowanych, je^sli "+
        "stonowanym mo^zna nazwa^c jasny odcie^n r^o^zu czy fioletu. "+
        "Paski zachodz^a na siebie i przeplataj^a si^e wzajemnie, "+
        "przytwierdzone do podszewki jedynie ko^nc^owkami. R^ekawy u "+
        "g^ory wybitnie bufiaste zw^e^zaj^a si^e ku do^lowi i ko^ncz^a "+
        "eleganckimi mankietami. Kubrak wi^azany jest na niezliczon^a "+
        "ilo^s^c drobnych rzemyczk^ow i za^lo^zenie jego jest zapewne "+
        "do^s^c czasoch^lonne, ale trzeba przyzna^c, ^ze efekt wart "+
        "jest zachodu.\n");


    set_slots(A_TORSO, A_FOREARMS, A_ARMS);
    add_prop(OBJ_I_VOLUME, 1550);
    add_prop(OBJ_I_WEIGHT, 400);
    add_prop(OBJ_I_VALUE, 140);
    add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    add_prop(ARMOUR_I_U_ROZCIAGLIWOSC, 40);
    set_size("M");
    ustaw_material(MATERIALY_SK_KROLIK, 100);
    set_type(O_UBRANIA);

}

string
query_auto_load()
{
   return ::query_auto_load();
}