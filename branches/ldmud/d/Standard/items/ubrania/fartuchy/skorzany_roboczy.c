/* Rantaur
 */

inherit "/std/armour";

#include "/sys/formulas.h"
#include <macros.h>
#include <materialy.h>
#include <object_types.h>
#include <stdproperties.h>

void create_armour()
{
     ustaw_nazwe("fartuch");
    dodaj_przym("sk�rzany", "sk�rzani");
    dodaj_przym("roboczy", "robocze");
    set_long("Fatruch w kilku miejscach jest ju� mocno przetarty, z pewno�ci�"
            +" sporo przeszed� odk�d opu�ci� warsztat ku�nierza. Na �rodku"
	    +" umieszczono g��bok�, pod�u�n� kiesze�, w kt�rej w�a�ciciel mo�e"
	    +" przechowywa� najcz�ciej u�ywane narz�dzia. Wytrzyma�a sk�ra"
	    +" zapewnia skuteczn� ochron� zar�wno przed wszelkimi zadrapaniami,"
	    +" jak i poparzeniami, o poplamieniu ju� nie wspominaj�c - s�owem"
	    +" fartuch nadaje si� do wi�kszosci pospolitych prac.\n");
    set_slots(A_TORSO | A_STOMACH | A_THIGHS);
    add_prop(ARMOUR_F_PRZELICZNIK, 40.0);
    add_prop(OBJ_I_VALUE, 71);
    add_prop(OBJ_I_WEIGHT, 3440);
    add_prop(OBJ_I_VOLUME, 2000);
    set_type(O_UBRANIA);
    ustaw_material(MATERIALY_SK_SWINIA);
   add_prop(ARMOUR_I_D_ROZCIAGLIWOSC, 100); //przeciez nie da sie go 'niezalozyc'!
   add_prop(ARMOUR_I_U_ROZCIAGLIWOSC, 100); //w koncu to fartuch!

}

string query_auto_load()
{
    return ::query_auto_load();
}