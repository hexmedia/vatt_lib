
/* Stare szarawe onuce
Wykonano dnia 11.07.2006 przez Avarda. 
Opis napisany przez Tinardan */
/* Wykorzystane w:
Las Rinde (LAS_REDANIA + las_rinde/las34.c)
          (LAS_REDANIA + las_rinde/las48.c)
          (LAS_REDANIA + las_rinde/las56.c)
          (LAS_REDANIA + las_rinde/las57.c)
*/

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <macros.h>
#include <materialy.h>
#include <pl.h>
#include <object_types.h>

void
create_armour()
{
    ustaw_nazwe("onuce");
    ustaw_nazwe_glowna("para");
    dodaj_przym("stary","starzy");
    dodaj_przym("szarawy","szarawi");

    set_long("Niegdy^s by^l to d^lugi pas bia^lego materia^lu s^lu^z^acy do "+
		"ochrony st^op w bucie i zapewnienia im ciep^la. Natomiast teraz "+
		"raczej jest to jedna wielka dziura, poprzetykana gdzieniegdzie "+
		"szcz^atkami szarawego (cho^c z tym stwierdzeniem mo^znaby si^e "+
		"k^l^oci^c) materia^lu, tak cienkiego, ^ze doskonale wida^c co "+
		"znajduje si^e za nim. \n");
		
    set_slots(A_FEET);
    ustaw_material(MATERIALY_LEN, 100);
    add_prop(OBJ_I_VALUE, 1);
    add_prop(OBJ_I_WEIGHT, 40);
    add_prop(OBJ_I_VOLUME, 40);
    add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    set_size("L");
}
string
query_auto_load()
{
   return ::query_auto_load();
}