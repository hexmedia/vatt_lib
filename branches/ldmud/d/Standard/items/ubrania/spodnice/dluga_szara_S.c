
/* D^luga szara sp^odnica
Wykonano dnia 19.05.06 przez Avarda. 
Opis by Tinardan */


inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <pl.h>
#include <macros.h>
#include <materialy.h>
#include <object_types.h>
void
create_armour()
{
    ustaw_nazwe("spodnica");
    dodaj_przym("d^lugi","d^ludzy");
    dodaj_przym("szary","szarzy");

    set_long("Uszyta w ten spos^ob, ^ze nie kr^epuje ruch^ow, a "+
        "jednocze^snie wygl^ada do^s^c skromnie. W talii wzmocniona "+
        "dodatkowym pasmem materia^lu. Wykonana zosta^la z cienkiego, "+
        "do^s^c szorstkiego p^l^otna i nie posiada ^zadnych ozd^ob. U "+
        "do^lu nieco przetarta i ub^locona, z boku mo^zna dojrze^c "+
        "niewielk^a ^lat^e. Jest d^luga i szeroka, tak by w zupe^lno^sci "+
        "zakrywa^la ludzkiej kobiecie nogi a^z po kostki. Z prawej strony "+
        "przyszyta jest kiesze^n, do kt^orej mo^zna w^lo^zy^c niewielkie "+
        "rzeczy.\n");

/* Trzeba dodac mozliwosc wkladania przedmiotow do kieszeni ;) */

    set_slots(A_LEGS, A_HIPS);
    add_prop(OBJ_I_WEIGHT, 600);
    add_prop(OBJ_I_VOLUME, 1500);
    add_prop(OBJ_I_VALUE, 55);
    add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
    add_prop(ARMOUR_I_DLA_PLCI, 1);
    add_prop(ARMOUR_I_D_ROZCIAGLIWOSC, 30);
    add_prop(ARMOUR_I_U_ROZCIAGLIWOSC, 40);
    set_size("S");
    ustaw_material(MATERIALY_LEN, 100);
    set_type(O_UBRANIA);
}
string
query_auto_load()
{
   return ::query_auto_load();
}
