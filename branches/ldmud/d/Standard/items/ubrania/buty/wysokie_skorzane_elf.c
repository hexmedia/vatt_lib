// g.

inherit "/std/armour.c";
#include <stdproperties.h>
#include <macros.h>
#include <materialy.h>
#include <object_types.h>

void
create_armour()
{
    ustaw_nazwe("buty");
    ustaw_nazwe_glowna("para");

    dodaj_przym("wysoki", "wysocy");
    dodaj_przym("sk^orzany", "sk^orzani");

    set_long("Wysokie, bo niemal si^egaj^ace kolan buty z mi^ekkiej "+
             "ciel^ecej sk^ory. Posiadaj^a one charakterystyczn^a dla "+
             "elfiego obuwia cech^e - klamerkowe zapi^ecia na ca^lej "+
             "d^lugo^sci cholewy. Mimo niewielkiej wagi buty te s^a "+
             "naprawd^e wytrzyma^le i do tego bardzo wygodne. Z nimi ^zadna "+
             "piesza w^edr^owka nie wydaje si^e by^c trudna.\n");

    ustaw_material(MATERIALY_SK_CIELE);
    set_type(O_UBRANIA);
    add_prop(ARMOUR_I_D_ROZCIAGLIWOSC, 40);
    add_prop(ARMOUR_I_U_ROZCIAGLIWOSC, 40);
    add_prop(ARMOUR_S_DLA_RASY, "elf");
    set_size("M");
    set_ac(A_FEET | A_SHINS, 1,1,1);
    //set_slots(A_FEET | A_SHINS);
    add_prop(ARMOUR_F_POW_DL_STOPA, 2.5);
    add_prop(ARMOUR_F_POW_SZ_STOPA, 2.5);
    add_prop(OBJ_I_VALUE, 154);
    add_prop(OBJ_I_VOLUME, 900);
    add_prop(OBJ_I_WEIGHT, 990);
    set_likely_cond(9);
}
string
query_auto_load()
{
   return ::query_auto_load();
}

