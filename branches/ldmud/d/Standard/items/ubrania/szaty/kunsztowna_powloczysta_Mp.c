
/* made by Faeve, 28 maja '06 */

inherit "/std/armour";
#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <pl.h>
#include <macros.h>
#include <materialy.h>
#include <object_types.h>

void
create_armour()
{               
    ustaw_nazwe("szata");
    dodaj_przym("kunsztowny","kunsztowni"); 

    dodaj_przym("pow^l^oczysty","pow^l^oczy^sci");

    set_long("Szata zwraca na siebie uwag^e nie tylko przedziwnym doborem "+
        "kolor^ow, jest bowiem czerwono-granatowa, ale tak^ze niespotykanym "+
        "krojem - lu^xno zwisaj^aca i tak d^luga, ^ze opada na ziemi^e "+
        "i ci^agnie si^e po niej za w^la^scicielem. Za zapi^ecie s^lu^zy "+
        "fantazyjna brosza o trudnym do okre^slenia kszta^lcie, "+
        "przyozdobiona rubinami i ciemnobrunatnymi granatami. Ponadto "+
        "szata zaopatrzona jest w kaptur obszyty skrawkami "+
        "nied^xwiedziego futra.\n");
             
    set_slots(A_ROBE);
    add_prop(OBJ_I_WEIGHT, 4500);
    add_prop(OBJ_I_VOLUME, 3000);
    add_prop(OBJ_I_VALUE, 360);
    set_type(O_UBRANIA);
    ustaw_material(MATERIALY_ATLAS, 95);
    ustaw_material(MATERIALY_SK_NIEDZWIEDZ, 5);
    add_prop(ARMOUR_S_DLA_RASY, "polelf");
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    add_prop(ARMOUR_I_U_ROZCIAGLIWOSC, 40);

    set_size("M");
}
string
query_auto_load()
{
   return ::query_auto_load();
}