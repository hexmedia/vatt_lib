
/* Miekkie skorzane cizemki
Wykonano dnia 25.07.2005 przez Avarda. Opis napisany przez Tinardan 
Poprawki by Avard dnia 20.05.06 */

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <macros.h>
#include <materialy.h>
#include <object_types.h>
#include <pl.h>

void
create_armour()
{
    ustaw_nazwe("cizemki");
    ustaw_nazwe_glowna("para");
	  
    dodaj_przym("mi^ekki","mi^ekcy");
    dodaj_przym("sk^orzany","sk^orzani");

    set_long("Wykonane z mi^eciutkiej sk^ory, cz^lowiekowi si^egaj^a do "+
        "kostek. Specjalnie przycinane cholewki s^a wywini^ete z przodu "+
        "i z ty^lu. Noski s^a modnie zadarte do g^ory, a podeszwa "+
        "usztywniana dodatkow^a warstw^a sk^ory. Ich neutralny, br^azowy "+
        "kolor pasuje niemal do ka^zdego ubrania i miejscy eleganci cz^esto "+
        "je nosz^a. Jako jedyn^a ozdob^e maj^a rzemyczki obwi^azywane "+
        "wok^o^l stopy i kostek. \n");

    set_slots(A_FEET);
    set_type(O_UBRANIA);
    add_prop(OBJ_I_WEIGHT, 500);
    add_prop(OBJ_I_VOLUME, 80);
    add_prop(OBJ_I_VALUE, 60);
    add_prop(ARMOUR_I_U_ROZCIAGLIWOSC, 40);
    add_prop(ARMOUR_I_D_ROZCIAGLIWOSC, 40);
    ustaw_material(MATERIALY_SK_CIELE, 100);
    add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    set_size("L");

	  }
string
query_auto_load()
{
   return ::query_auto_load();
}