
/* Wielki bordowy kapelusz
Wykonano dnia 20.06.2005 przez Avarda. Opis zaczerpniety z banku opisow,
napisany przez Ghalleriana w dziale "ubrania i inne przedmioty" 
dnia 27.04.2005*/

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <macros.h>
#include <materialy.h>
#include <object_types.h>
#include <pl.h>

void
create_armour()
{
    ustaw_nazwe("kapelusz");

    dodaj_przym("wielki","wielcy");
    dodaj_przym("bordowy","bordowi");

    set_long("^Swietnie wykonany, mimo wieku, trzyma si^e jeszcze dobrze. "+
        "Posiada szerokie rondo, kt^ore skutecznie b^edzie chroni^lo przed "+
        "promieniami s^lonecznymi jak i przed deszczem. Bardzo u^zyteczny "+
        "w dalekich podr^o^zach jak i podczas przechadzek. ^Ladnie wyszyte "+
        "z^lote hafty ^swietnie komponuj^a si^e z kolorem kapelusza. Dzi^eki "+
        "swojej elegancji mo^ze s^lu^zy^c jako ozdobna cz^e^s^c garderoby.\n");

    set_slots(A_HEAD);
    add_prop(OBJ_I_WEIGHT, 500);
    add_prop(OBJ_I_VOLUME, 1000);
    add_prop(OBJ_I_VALUE, 45);
    set_type(O_UBRANIA);
    ustaw_material(MATERIALY_BAWELNA, 100); // Do zmiany
    add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    set_size("L");
}

string
query_auto_load()
{
   return ::query_auto_load();
}