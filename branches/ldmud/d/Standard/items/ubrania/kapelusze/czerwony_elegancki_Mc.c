
/* Czerwony elegancki kapelusz
Wykonany przez Avarda dnia 07.06.06 
Opis by Tinardan */
/* Uzyte w:
Trubadur uliczny z Rinde (/d/Standard/Redania/Rinde/npc/trubadur_uliczny.c);
*/

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <macros.h>
#include <materialy.h>
#include <object_types.h>
#include <pl.h>

void
create_armour()
{
    ustaw_nazwe("kapelusz");

    dodaj_przym("czerwony","czerwoni");
    dodaj_przym("elegancki","eleganccy");

    set_long("Kapelusik uszyto wedle najnowszej mody. Jest do^s^c "+
        "niewygodny w noszeniu, ale ^swiadczy o smaku jego w^la^sciciela "+
        "i jego znajomo^sci najnowszych dworskich zwyczaj^ow. Nasuwany "+
        "nieco uko^snie na czo^lo, w kolorze krwistej czerwieni, w "+
        "wywini^ete, male^nkie rondo wpi^ete ma trzy d^lugie, ba^zancie "+
        "pi^ora barwione na krzykliwy kolor pomara^nczu, ^z^o^lci i "+
        "zieleni.\n");

    set_slots(A_HEAD);
    add_prop(OBJ_I_WEIGHT, 800);
    add_prop(OBJ_I_VOLUME, 1000);
    add_prop(OBJ_I_VALUE, 120);
    set_type(O_UBRANIA);
    ustaw_material(MATERIALY_BAWELNA, 100); // Do zmiany
    add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    set_size("M");
}

string
query_auto_load()
{
   return ::query_auto_load();
}