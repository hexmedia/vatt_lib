/* Szare lniane giez�o
Wykonano dnia Thursday 11 of May 2006 przez Lil. Opis zaczerpniety z BO,
napisany przez Lina Utenime w dziale "ubrania i inne przedmioty"
dnia Pon Wrz 05, 2005 8:32 pm*/

inherit "/std/armour";
#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include "/sys/macros.h"
#include "materialy.h"
#include "object_types.h"



void
create_armour()
{
    ustaw_nazwe("giez�o");
    dodaj_przym("szary","szarzy");
    dodaj_przym("lniany","lniani");

    set_long("Mimo, �e tego giez�a nie mo�na uzna� za szczyt "+
             "sztuki krawieckiej, jest bardzo wygodne i nie kr�puje "+
             "ruch�w. Cz�ste pranie spowodowa�o, �e p��tno ca�kowicie "+
             "zszarza�o. Przy dekolcie widniej� hafty wykonane niewprawna "+
             "r�k�, wygl�daj�ce na efekt pracy jakiego� dziecka.\n");
    set_slots(A_TORSO, A_FOREARMS, A_ARMS);
    //set_size("S");
    add_prop(ARMOUR_S_DLA_RASY, "cz�owiek");
    add_prop(OBJ_I_WEIGHT, 1212);
    add_prop(OBJ_I_VOLUME, 1100);
    add_prop(OBJ_I_VALUE, 7);
    ustaw_material(MATERIALY_LEN);
    set_type(O_UBRANIA);
    add_prop(ARMOUR_I_D_ROZCIAGLIWOSC, 30);
    add_prop(ARMOUR_I_U_ROZCIAGLIWOSC, 30);
    add_prop(ARMOUR_F_PRZELICZNIK, 60.0); //koszula jest bardzo rozciagliwa.

}

string
query_auto_load()
{
   return ::query_auto_load();
}