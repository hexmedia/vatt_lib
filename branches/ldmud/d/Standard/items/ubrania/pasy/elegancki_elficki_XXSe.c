
/* Elegancki elficki pasek, wykonany przez Avarda dnia 21.05.06
   Opis by Tinardan */

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <pl.h>
#include <macros.h>
#include <materialy.h>
#include <object_types.h>

void
create_armour()
{  
    ustaw_nazwe("pas");
    dodaj_przym("elegancki","eleganccy");
    dodaj_przym("elficki","elficcy");

	set_long("Zielone rzemyki z delikatnej sk^ory splataj^a si^e w misterny "+
        "wz^or i razem tworz^a ca^lkiem mocny i dobrej jako^sci pasek. Jego "+
        "eleganckie zdobienia, srebrna klamra o kszta^lcie li^scia klonu i "+
        "wyko^nczenia z tego samego materia^lu ^swiadcz^a, ^ze wykona^l go "+
        "nie lada mistrz. Wzd^lu^z ka^zdego rzemyka biegnie cieniutka "+
        "^zy^lka wykonana ze z^lota pomiesznego z jakim^s innym, nadaj^acym "+
        "mu soczyst^a, ciemnozielon^a barw^e. Po jego wewn^etrznej stronie "+
        "widnieje ma^la, miedziana tabliczka z wygrawerowanymi na niej "+
        "dziwnymi znakami. \n");

    set_slots(A_HIPS);
    set_type(O_UBRANIA);
    add_prop(OBJ_I_VOLUME, 300);
    add_prop(OBJ_I_WEIGHT, 150);
    add_prop(OBJ_I_VALUE, 220);
    add_prop(ARMOUR_S_DLA_RASY, "elf");
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    add_prop(ARMOUR_I_U_ROZCIAGLIWOSC, 60);
    add_prop(ARMOUR_I_D_ROZCIAGLIWOSC, 60);
    set_size("XXS");	
    ustaw_material(MATERIALY_SK_SWINIA, 90);
    ustaw_material(MATERIALY_ZLOTO, 10);
}
string
query_auto_load()
{
   return ::query_auto_load();
}