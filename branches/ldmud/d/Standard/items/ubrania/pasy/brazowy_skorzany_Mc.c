
/* Brazowy skorzany pas wykonany przez Avarda dnia 03.08.2005
   Poprawki by Avard dnia 20.05.06 */

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <pl.h>
#include <macros.h>
#include <materialy.h>
#include <object_types.h>

void
create_armour()
{  
    ustaw_nazwe("pas");
    dodaj_przym("br^azowy","br^azowi");
    dodaj_przym("elegancki", "eleganccy");

	set_long("Pas, na kt^ory spogl^adasz, jest wykonany z niezwykle "+
        "twardej, zabarwionej na br^azowo sk^ory jakiego^s zwierz^ecia. "+
        "Wielokolorowe kamienie szlachetne, sprawiaj^ace wra^zenie, jakby "+
        "wyrasta^ly prosto ze^n b^lyszcza niesamowitym blaskiem, nadaj^ac "+
        "patosu i powagi osobie nosz^acej ^ow pas. Twoj^a uwag^e "+
        "najbardziej zwraca masywna, kwadratowa sprz^aczka, wykonana z "+
        "czystego z^lota, na kt^orej widnieje feniks odradzaj^acy si^e z "+
        "popiol^ow. Tylko nielicznych sta� na takowy wyr^ob.\n");

    set_slots(A_HIPS);
    set_type(O_UBRANIA);
    add_prop(OBJ_I_VOLUME, 300);
    add_prop(OBJ_I_WEIGHT, 150);
    add_prop(OBJ_I_VALUE, 260);
    add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    add_prop(ARMOUR_I_U_ROZCIAGLIWOSC, 60);
    add_prop(ARMOUR_I_D_ROZCIAGLIWOSC, 60);
    set_size("M");	
    ustaw_material(MATERIALY_SK_SWINIA, 80);
    ustaw_material(MATERIALY_ZLOTO, 10);
    ustaw_material(MATERIALY_RUBIN, 10);
}
string
query_auto_load()
{
   return ::query_auto_load();
}