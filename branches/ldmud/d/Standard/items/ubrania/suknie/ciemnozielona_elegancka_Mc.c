
/* Autor: Avard
   Opis : Na podstawie sukni Nifredila, Avard
   Data : 19.08.06 */

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <pl.h>
#include <macros.h>
#include <materialy.h>
#include <object_types.h>

void
create_armour()
{
    ustaw_nazwe("suknia");
    dodaj_przym("ciemnozielony","ciemnozieleni");
    dodaj_przym("elegancki","eleganccy");

    set_long("Ta ciemnozielona suknia z czarnym, wyj^atkowo sztywnym "+
        "gorsetem ko^ncz^acym si^e tu^z pod biustem, jest przeznaczona "+
        "dla naprawd^e szczup^lej kobiety. Odchodz^ace od niego dwa "+
        "zw^e^zaj^ace si^e ku g^orze, pasy zielonego materia^lu s^lu^z^a "+
        "do zawi^azania na karku i bardzo skromnie zas^laniaj^a wdzi^eki "+
        "w^la^scicielki sukni. Na wysoko^sci bioder gorset przechodzi w "+
        "si^egaj^ac^a ziemi sp^odnic^e, po kt^orej bokach biegn^a dwa "+
        "rozci^ecia. Z ty^lu suknia zapinana jest na misterne czarne "+
        "guziczki wykonane z odbijaj^acego ^swiat^lo kamienia, za^s "+
        "ca^lo^s^c uszyto z wyj^atkowo delikatnego i mi^ekkiego "+
        "materia^lu.\n");

    set_slots(A_TORSO, A_FOREARMS, A_ARMS, A_LEGS, A_HIPS, A_STOMACH);
    add_prop(OBJ_I_WEIGHT, 4000);
    add_prop(OBJ_I_VOLUME, 4000);
    add_prop(OBJ_I_VALUE, 1440);
    add_prop(ARMOUR_I_U_ROZCIAGLIWOSC, 40);
    add_prop(ARMOUR_I_U_ROZCIAGLIWOSC, 40);
    ustaw_material(MATERIALY_AKSAMIT, 95);
    ustaw_material(MATERIALY_BAWELNA, 5);
    set_type(O_UBRANIA);
    add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
    add_prop(ARMOUR_I_DLA_PLCI, 1);
    set_size("M");
}
string
query_auto_load()
{
   return ::query_auto_load();
}
