/*
 * ciasna po�atana bluzeczka
 * Wykonanie: Lil, Opis: Dinrahia(tudzie� Aneta)
 */


inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <macros.h>
#include <materialy.h>
#include <object_types.h>

void
create_armour()
{
    ustaw_nazwe("bluzeczka");
    dodaj_przym("ciasny","ciasni");
    dodaj_przym("po�atany", "po�atani");
    set_long("Typowo damska bluzka wykonana zosta�a z delikatnego, "+
             "barwionego na czarno materia�u. Jest starannie za�atana i "+
             "dzi�ki prostemu, funkcjonalnemu krojowi nie kr�puje ruch�w "+
             "i doskonale nadaje si� do d�ugich podr�y.\n");
    set_slots(A_TORSO, A_FOREARMS, A_ARMS);
    add_prop(OBJ_I_WEIGHT, 440);
    add_prop(OBJ_I_VOLUME, 800);
    add_prop(OBJ_I_VALUE, 24);
    add_prop(ARMOUR_F_PRZELICZNIK, 50.0);
    add_prop(ARMOUR_I_DLA_PLCI, 1);
    ustaw_material(MATERIALY_LEN);
    add_prop(ARMOUR_I_D_ROZCIAGLIWOSC, 40);
    //add_prop(ARMOUR_S_DLA_RASY, "cz�owiek");
    set_size("M");
    set_type(O_UBRANIE);
    set_likely_cond(25);
}
string
query_auto_load()
{
   return ::query_auto_load();
}
