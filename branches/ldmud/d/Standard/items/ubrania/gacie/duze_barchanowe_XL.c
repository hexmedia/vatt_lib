/*
 * Du�e barchanowe majtki.
 *
 * Autor: Lil, opis: Brz�zek.
 *             Tuesday 09 of May 2006
 */

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <macros.h>
#include <materialy.h>

void
create_armour()
{  
    ustaw_nazwe("majtki");
    ustaw_nazwe_glowna("para");
    dodaj_przym("du�y","duzi");
    dodaj_przym("barchanowy", "barchanowi");
    set_long("Worek lnianego, grubego p��tna �ci�ga si� i wzdyma "+
             "wzd�u� nier�wnych, grubych szw�w ��cz�cych jego boki i "+
             "uwypukla w przedniej cz�ci tworz�c stercz�cy balon. "+
             "Wi�zane wysoko ponad pasem konopne sznurki s�u��ce do "+
             "wi�zania majtek przeci�gni�te s� przez liczne, nier�wno "+
             "wyci�te otworki o postrz�pionych brzegach. Si�gaj�ce do "+
             "ud nogawki ozdobione s� na przemian pokracznymi, wielkimi "+
             "�ciegami i fr�dzlami siepi�cego si� materia�u. \n");

    set_slots(A_HIPS);
    add_prop(OBJ_I_WEIGHT, 180);
    add_prop(OBJ_I_VOLUME, 190);
    set_size("XL");
    add_prop(OBJ_I_VALUE, 9);

    add_prop(ARMOUR_F_PRZELICZNIK, 10.0);
    ustaw_material(MATERIALY_LEN, 100);
    add_prop(ARMOUR_I_D_ROZCIAGLIWOSC, 10);
    add_prop(ARMOUR_I_U_ROZCIAGLIWOSC, 40);
}
string
query_auto_load()
{
   return ::query_auto_load();
} 
