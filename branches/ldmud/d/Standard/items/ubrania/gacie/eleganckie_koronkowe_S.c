/*
 * Eleganckie koronkowe majtki.
 *
 * Autor: Lil, opis: Brz�zek.
 *             Tuesday 09 of May 2006
 */

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <macros.h>
#include <materialy.h>

void
create_armour()
{  
    ustaw_nazwe("majtki");
    ustaw_nazwe_glowna("para");
    dodaj_przym("elegancki","eleganccy");
    dodaj_przym("koronkowy", "koronkowi");
    set_long("Centraln� cz�� niewielkiego tr�jk�ta sprowadzonej z "+
             "dalekiego kraju kosztownej koronki zdobi misternie utkany "+
             "amorek z �ukiem, otoczony przeplataj�cym si� wzorem serduszek "+
             "i kwiatk�w. Do wi�zania tej ods�aniaj�cej wi�cej ni� "+
             "zas�aniaj�cej tkaniny s�u�� podw�jne, jedwabi�cie l�ni�ce "+
             "z�ociste wst�gi przyszyte do naro�nik�w tr�jk�ta.\n");

    set_slots(A_HIPS);
    add_prop(OBJ_I_WEIGHT, 16);
    add_prop(OBJ_I_VOLUME, 80);
    set_size("S");
    add_prop(OBJ_I_VALUE, 99);

    /* majteczki s� bardzo rozci�gliwe */
    add_prop(ARMOUR_F_PRZELICZNIK, 50.0);
    /* ustalamy, �e majteczki s� dla kobiet */
    add_prop(ARMOUR_I_DLA_PLCI, 1);
    /* ustawiamy materia� */
    ustaw_material(MATERIALY_KORDONEK, 90);
    ustaw_material(MATERIALY_JEDWAB, 10);
    /* ustawiamy rozci�gliwo�� w d� na 30% */
    add_prop(ARMOUR_I_D_ROZCIAGLIWOSC, 30);
}
string
query_auto_load()
{
   return ::query_auto_load();
} 
