inherit "/std/object";
inherit "/lib/material";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <macros.h>
#include <materialy.h>

void
create_object()
{
    ustaw_nazwe("sk^ora");
    dodaj_przym("kr^oliczy","kr^oliczy");

    set_long("Ta jedna z najpospolitszych sk^or pochodzi z kr^olika i "+
        "niejednemu mo^ze przyda^c si^e do wykonania jakich^s drobnych "+
        "sakiewek, ma^lych torebek czy innych przydatnych rzeczy. Nie "+
        "wygl^ada na drog^a, a szarawy kolor i mieki w dotyku materia^l, "+
        "z pewno^sci^a mo^zna dobrze wykorzysta^c.\n");

    add_prop(OBJ_I_WEIGHT, 10000);
    add_prop(OBJ_I_VOLUME, 5000);
    add_prop(OBJ_I_VALUE, 100);

    ustaw_material(MATERIALY_SK_KROLIK);
    set_material_capacity(MATERIALY_SK_KROLIK, 10000);
}

public void
notify_change_material_amount(string material, int oldValue, int newValue)
{
    float factor = itof(newValue)/itof(query_material_capacity(material));
    if (newValue == 0) {
        TO->remove_object();
        return;
    }
    add_prop(OBJ_I_WEIGHT, ftoi(factor * 10000.0));
    add_prop(OBJ_I_VOLUME, ftoi(factor * 5000.0));
    add_prop(OBJ_I_VALUE, ftoi(factor * 1000.0));
}

public string
query_auto_load()
{
    return ::query_auto_load() + query_material_auto_load();
}

public string
init_arg(string arg)
{
    return init_material_arg(::init_arg(arg));
}
