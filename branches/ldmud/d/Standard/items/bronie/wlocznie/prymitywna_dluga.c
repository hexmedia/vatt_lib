
/*opis by Tu^lacz
zakodowane by Sedzik 5.04.2007
*/

inherit "/std/weapon";

#include <stdproperties.h>
#include <object_types.h>
#include <materialy.h>
#include <wa_types.h>

void create_weapon()
{
        ustaw_nazwe("w^l^ocznia");
        dodaj_przym("prymitywny", "prymitywni");
        dodaj_przym("d^lugi", "d^ludzy");
        set_long("Jest to w zasadzie r^ownej grubo^sci, d^luga tyczka "
	+"z do^s^c spr^e^zystego drewna, na kt^orej osadzono w^askie, stalowe ostrze. "
	+"Miejsce po^l^aczenia grotu i drzewca zosta^lo ^sci^sle oplecione rzemieniem, "
	+"spod kt^orego wystaj^a dwa d^lugie, czarne pi^ora. Taka bro^n nie jest wprawdzie "
	+"du^zo skuteczniejsza od zwyk^lego no^za, ale pozwoli utrzymac przeciwnika "
	+"w bezpiecznej odleg^lo^sci.\n");
        
        set_hit(15);
        set_pen(13);

        add_prop(OBJ_I_WEIGHT, 5200);
        add_prop(OBJ_I_VOLUME, 3130);
        add_prop(OBJ_I_VALUE, 380);

        set_wt(W_POLEARM);
        set_dt(W_IMPALE);
        
        set_hands(W_BOTH);
        ustaw_material(MATERIALY_STAL, 15);
        ustaw_material(MATERIALY_DR_JESION, 85);
        set_type(O_BRON_DRZEWCOWE_D);
}

