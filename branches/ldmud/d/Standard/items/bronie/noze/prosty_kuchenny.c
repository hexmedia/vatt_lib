
/* Prosty kuchenny noz.
Wykonanio dnia 22.07.2005 przez Avarda. Opis zaczerpniety z banku opisow,
napisany przez Tinardan w dziale "bronie i zbroje" dnia 17.07.2005 */


inherit "/std/weapon";

#include "/sys/formulas.h"
#include <stdproperties.h>
#include <wa_types.h>
#include <pl.h>
#include <materialy.h>
#include <object_types.h>


void
create_weapon()
{
   ustaw_nazwe("n�");
                    
     dodaj_przym("prosty","pro^sci");
	 dodaj_przym("kuchenny","kuchenni");

	 set_long("Jest to zwyk^ly n^o^z, jakich pe^lno wsz^edzie wok^o^l i kt^ory mo^zna "+
		 "znale^x^c niemal^ze w ka^zdej kuchni. Zrobiony ze zwyk^lej stali, o "+
		 "mocnej drewnianej r^ekoje^sci. Na ostrzu widniej� dwa lub trzy "+
	     "wyszczerbienia.\n");

	 set_hit(20);
	 set_pen(12);
        add_prop(OBJ_I_WEIGHT, 400);
        add_prop(OBJ_I_VOLUME, 400);
        add_prop(OBJ_I_VALUE, 6);

	 set_wt(W_KNIFE);
        set_dt(W_IMPALE | W_SLASH);
	 ustaw_material(MATERIALY_STAL, 90);
	 ustaw_material(MATERIALY_DR_SOSNA, 10);
	 set_type(O_BRON_SZTYLETY);

	 set_hands(A_ANY_HAND);


}
