
/* Autor: Avard
   Data : 08.03.06 */

inherit "/std/weapon";

#include "/sys/formulas.h"
#include <stdproperties.h>
#include <wa_types.h>
#include <pl.h>
#include <materialy.h>
#include <object_types.h>

void
create_weapon()
{
     ustaw_nazwe("espadon");
     dodaj_nazwy("miecz");

     dodaj_przym("d^lugi", "d^ludzy");
     dodaj_przym("temerski", "temerscy");

     set_long("Pierwsze co rzuca si^e w oczy to wielko^s^c tej broni. D^lugi na "+
		 "prawie s^a^ze^n, obur^eczny i ci^e^zki miecz nadaje si^e tylko do walki "+
		 "pieszej. R^ekoje^s^c owini^eta ciemn^a sk^or^a, d^lugo�ci^a si^ega prawie na "+
		 "^lokie^c. Od g^lowni oddziela j^a d^lugi, delikatnie wygi^ety w strone "+
		 "klingi jelec pod kt^orym znajduje si^e ricasso, os^laniane przez "+
		 "mocne w^asy. Obusieczny brzeszczot wykonany jest z niez^lej jako^sci "+
		 "stali, a osoba, kt^ora go wyku^la zna si^e na rzeczy. \n");
	 
     set_hit(20); 
     set_pen(36); 
     set_hands(A_HANDS);

     add_prop(OBJ_I_WEIGHT, 4000);
     add_prop(OBJ_I_VOLUME, 4000);
     add_prop(OBJ_I_VALUE, 5400);

     ustaw_material(MATERIALY_STAL, 90); 
     ustaw_material(MATERIALY_SK_SWINIA, 10);
     set_type(O_BRON_MIECZE);
     set_wt(W_SWORD);
     set_dt(W_SLASH);
}