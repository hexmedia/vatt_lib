
/* Maly stalowy sztylet.
Wykonanio dnia 22.07.2005 przez Avarda. Opis zaczerpniety z banku opisow,
napisany przez Ghalleriana w dziale "bron i zbroje" dnia 13.04.2005 
Uzyty w:
Sprzedawany tak�e w sklepie (RINDE+"centrum/magazyn_sklepowy")
*/


inherit "/std/weapon";


#include "/sys/formulas.h"
#include <stdproperties.h>
#include <wa_types.h>
#include <pl.h>
#include <materialy.h>
#include <object_types.h>


void
create_weapon()
{
   ustaw_nazwe("sztylet");
                    
     dodaj_przym("ma^ly","mali");
	 dodaj_przym("stalowy","stalowi");

	 set_long("Bro^n ta wykonana zosta^la z niez^lej stali. Jest elastyczna "+
		 "ale i zarazem twarda. Dlatego sztylet ^swietnie sprawdza si^e "+
		 "w ka^zdej sytuacji. Do tego r^ekoje^s^c oprawiona zosta^la sk^or^a "+
		 "jakiego^s egzotycznego zwierz^ecia. Jest chropowata, dzi^eki czemu "+
		 "d^lo^n nie ^slizga si^e nawet gdy jest spocona.\n");

     add_item("r�koje^s^c", "Ogl^adasz dok^ladnie r^ekoje^s^c i po chwili "+
         "zauwa^zasz niewielkie literki wypalone na sk^orze. Napis brzmi "+
         "\"Zabijaj czekaj^ac na ^smier^c\". \n");

    add_prop(OBJ_I_WEIGHT, 900);
    add_prop(OBJ_I_VOLUME, 900);
    add_prop(OBJ_I_VALUE, 900);
   
	 set_hit(28);
	 set_pen(15);

	 set_wt(W_KNIFE);
        set_dt(W_IMPALE | W_SLASH);
	 ustaw_material(MATERIALY_STAL, 100);
	 set_type(O_BRON_SZTYLETY);

	 set_hands(A_ANY_HAND);


}
