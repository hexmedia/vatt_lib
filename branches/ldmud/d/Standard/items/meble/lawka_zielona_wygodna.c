inherit "/std/object";

#include "/sys/stdproperties.h"
#include "/sys/macros.h"
#include "/sys/pl.h"
#include "/sys/materialy.h"
#include "/sys/object_types.h"

void
create_object()
{
    ustaw_nazwe("�awka");
    dodaj_przym("zielony", "zieloni");
    dodaj_przym("wygodny", "wygodni");
    ustaw_material(MATERIALY_DR_DAB);
    set_type(O_MEBLE);
    set_long("Jest to starannie wykonana �awka pomalowana na jasnozielony " +
             "kolor. Wszystkie deski, z kt�rych jest wykonana s� " +
             "dok�adnie oheblowane i u�o�one w taki spos�b, by stanowi� " +
             "wygodne oparcie.\n");

    add_prop(OBJ_I_WEIGHT, 23111);
    add_prop(OBJ_I_VOLUME, 47211);
    add_prop(OBJ_I_VALUE, 53);

    make_me_sitable("na","na zielonej �awce","z zielonej �awki",3);
}
