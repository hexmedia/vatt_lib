
/* Krzeslo, made by Avard 07.06.06 
   Opis by Tinardan */

inherit "/std/object.c";

#include <pl.h>
#include <stdproperties.h>
#include <macros.h>
#include <filter_funs.h>
#include <materialy.h>
#include <object_types.h>

create_object()
{
    ustaw_nazwe("krzeslo");
    dodaj_przym("elegancki","eleganccy");
    dodaj_przym("wygodny","wygodni");


    set_long("D^ebowe krzes^lo, solidne i dobrze wykonane wygl^ada na "+
        "bardzo wygodne. Wy^scie^lane materia^lem siedzisko b^lyszczy "+
        "si^e elegancko, rze^xbione oparcia l^sni^a wypolerowanym drewnem. "+
        "Lekko podwy^zszony zag^l^owek przystosowany jest dla wygody "+
        "tak^ze i tych wy^zszych przedstawicieli rasy ludzkiej i nieludzi. "+
        "Rze^xbione ga^leczki na oparciu dodaj^a krzes^lu elegancji i "+
        "wykwintno^sci.\n");
    make_me_sitable("na","na krze^sle","z krzesla",1);
    add_prop(OBJ_I_WEIGHT, 4000);
    add_prop(OBJ_I_VOLUME, 4000);
    add_prop(OBJ_I_VALUE, 260);
    add_prop(OBJ_M_NO_SELL, 1);
    ustaw_material(MATERIALY_DR_DAB);
    set_type(O_MEBLE);
}

