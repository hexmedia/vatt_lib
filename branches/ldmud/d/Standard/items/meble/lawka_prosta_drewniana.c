inherit "/std/object";

#include "/sys/stdproperties.h"
#include "/sys/macros.h"
#include "/sys/pl.h"
#include "/sys/materialy.h"
#include "/sys/object_types.h"

void
create_object()
{
    ustaw_nazwe("�awka");
    dodaj_przym("prosty", "pro�ci");
    dodaj_przym("drewniany", "drewniani");
    ustaw_material(MATERIALY_DR_DAB);
    set_type(O_MEBLE);
    set_long("Solidna �awka, wykonana z dw�ch klock�w d�bowego drewna " +
             "stanowi�cych nogi i d�ugiej deski - siedziska.\n");
	     
    add_prop(OBJ_I_WEIGHT, 29431);
    add_prop(OBJ_I_VOLUME, 4212);
    add_prop(OBJ_I_VALUE, 5);

    make_me_sitable("na","na prostej drewnianej �awce","z �awki",4);
}
