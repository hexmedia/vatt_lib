inherit "/std/container";

#include "/sys/stdproperties.h"
#include "/sys/macros.h"
#include "/sys/pl.h"
#include "/sys/materialy.h"
#include "/sys/object_types.h"

void
create_container()
{
    ustaw_nazwe("st�");
    dodaj_przym("pod�u�ny", "pod�u�ni");
    dodaj_przym("solidny", "solidni");
    ustaw_material(MATERIALY_DR_DAB);
    set_type(O_MEBLE);
    set_long("D�ugi na dobre trzy s��nie i szeroki na jeden st� " +
             "pomie�ci swobodnie oko�o tuzinu os�b si�dz�cych przy nim, " +
             "a tak�e nie za�ama�by si� pod ci�arem jad�a dla nich - " +
             "sprawia wra�enie bardzo solidnego.\n"+
             "@@opis_sublokacji|Dostrzegasz na nim |na|.\n||" + PL_BIE+ "@@");
	     
    add_prop(CONT_I_WEIGHT, 133122);
    add_prop(CONT_I_VOLUME, 231209);
    add_prop(OBJ_I_VALUE, 56);
    
    add_subloc("na");
    add_subloc_prop("na", CONT_I_MAX_WEIGHT, 115000);
    add_subloc_prop("na", CONT_I_MAX_VOLUME, 215000);
    add_prop(CONT_I_DONT_SHOW_CONTENTS, 1);
    add_prop(CONT_I_CANT_WLOZ_DO, 1);

}
