inherit "/std/container";

#include "/sys/stdproperties.h"
#include "/sys/macros.h"
#include "/sys/pl.h"
#include "/sys/materialy.h"
#include "/sys/object_types.h"

void
create_container()
{
    ustaw_nazwe("stolik");
    dodaj_przym("okr�g�y", "okr�gli");
    dodaj_przym("niewielki", "niewielcy");
    ustaw_material(MATERIALY_DR_DAB);
    set_type(O_MEBLE);
    set_long("Niewielki, kawiarniany stolik, wystarczaj�cy dla dw�ch czy " +
             "trzech os�b.\n"+
             "@@opis_sublokacji|Dostrzegasz na nim |na|.\n||" + PL_BIE+ "@@");
	     // w towarzystwie kilku fili�anek.\n");
	     
    add_prop(CONT_I_WEIGHT, 8321);
    add_prop(CONT_I_VOLUME, 9123);
    add_prop(OBJ_I_VALUE, 29);
    add_subloc("na");
    add_subloc_prop("na", CONT_I_MAX_WEIGHT, 5000);
    add_subloc_prop("na", CONT_I_MAX_VOLUME, 8000);
    add_prop(CONT_I_DONT_SHOW_CONTENTS, 1);
    add_prop(CONT_I_CANT_WLOZ_DO, 1);
}
