/*
 * Autor: Rantaur
 * Data: 23.04.2006
 *  Serwetka do sklepu gnoma w wiosce tworzenia postaci
 *  */
inherit "/std/object.c";

#include <pl.h>
#include <stdproperties.h>
#include <macros.h>
#include <object_types.h>
#include <materialy.h>

void create_object()
{
    ustaw_nazwe("serwetka");
    dodaj_przym("bia�y", "biali");
    dodaj_przym("lniany", "lniani");
    set_long("Boki serwetki zako�czone s� kr�tkimi, g�sto u�o�onymi"
            +" fr�dzelkami, kt�re w rogach zwi�kszaj� nieco d�ugo��,"
	    +" tworz�c co� w rodzaju warkoczyk�w. G�adki, niczym"
	    +" tafla lodu materia� wygl�da na pozbawiony zdobie�, jednak"
	    +" przypatruj�c mu si� dok�adniej zauwa�asz delikatne, spiralne"
	    +" wzory. Ogl�daj�c je pod r�nymi k�tami masz wra�enie, �e"
	    +" nieustannie zmieniaj� swe u�o�enie, chwiej� si� jak drzewa"
	    +" ko�ysane przez wiatr. Serwetka wygl�da nadzwyczaj elegancko"
	    +" i z pewno�ci� doda uroku ka�demu przedmiotowi.\n");

    add_prop(OBJ_I_WEIGHT, 4);
    add_prop(OBJ_I_VOLUME, 3);
    add_prop(OBJ_I_VALUE, 14);
    set_type(O_INNE);
    ustaw_material(MATERIALY_LEN);   
}
