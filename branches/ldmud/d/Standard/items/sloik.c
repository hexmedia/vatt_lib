/* Sloj na ziola
 * Pshoddy
 */

inherit "/std/receptacle.c";

#include <stdproperties.h>
#include <pl.h>
#include <materialy.h>

create_container()
{
    set_long("Jest to zwyk造 szklany s這ik s逝膨cy do przechowywania "+
		"w nim kompot闚, kiszonej kapusty, zi馧 lub czegokolwiek innego.\n");

    ustaw_nazwe( ({ "s這ik", "s這ika", "s這ikowi", "s這ik",
        "s這ikiem", "s這iku" }), ({ "s這iki", "s這ik闚",
        "s這ikom", "s這iki", "s這ikami", "s這ikach" }) ,
        PL_MESKI_NOS_NZYW);


    dodaj_przym( "szklany", "szklani" );

    add_prop(CONT_I_MAX_WEIGHT, 1000);
    add_prop(CONT_I_MAX_VOLUME, 1000);
    add_prop(CONT_I_RIGID,1);
    add_prop(CONT_I_TRANSP,1);
	add_prop(OBJ_I_VALUE,183);
	//s這ik sam z siebie wa篡:
	add_prop(OBJ_I_WEIGHT,333);

	ustaw_material(MATERIALY_RZ_SZKLO,100);
}


/* Tylko ziola mozna wkladac do naszego sloika.*/

//a to niby dlaczego? - Vera.
/*
int
prevent_enter(object ob)
{
  if (function_exists("create_object",ob) == "/std/herb")
     {
         return 0;
     }
  return 1;
}

/* Ziola w sloiku nie wiedna... */

void
enter_inv(object ob, object from)
{
  ::enter_inv(ob, from);
  ob->stop_decay();
}

/* ...ale poza nim tak. */

void
leave_inv(object ob, object to)
{
  ::leave_inv(ob, to);
  ob->start_decay();
}
