/*
 * Jest to kie� wilko�aka, kt�ry Silvester wr�czy� Nirithen
 * gdy� ona jest z nim w ci��y, a on by� kiedy� wilko�akiem, jest wyleczony
 * tera, ale jest jaki� przes�d i pierdo�y. Kie� nale�y do Nirithen.
 * Vera. Thursday, January 03 2008
 */

#pragma unique
inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <macros.h>
#include <materialy.h>
#include <object_types.h>
#include <pl.h>

void
create_armour()
{

	set_long("Bia�y kie� pochodzi niew�tpliwie z uz�bienia "+
        "jakiego� gro�nego zwierz�cia, jednak�e nie jeste� w "+
        "stanie stwierdzi� z jakiego. Ostro zako�czony i "+
        "zakrzywiony pod do�� dziwnym k�tem w por�wnaniu z innymi "+
        "istotami fauny wyklucza jakie� zwyk�e cz�sto spotykane "+
        "zwierz�. Ca�o�� oprawiona jest w metal szlachetny, "+
        "mieni�cy si� bladym �wiat�em i zawieszona na takim� samym "+
        "�a�cuszku. Dooko�a k�a wij� si� srebrne linie, przeplataj�ce "+
        "si� w r�nych konfiguracjach. Asymetryczno�� ozdoby pobudza "+
        "wyobra�ni�. Wygl�da niczym alabastrowy "+
        "fragment ksi�yca oprawiony w jego srebrzyst� mg��.\n");
	ustaw_nazwe("wisiorek z bia�ym k�em");
        dodaj_nazwy("wisiorek");
	dodaj_przym("srebrny","srebrni");

// 	jak_pachnie="@@zapach@@";
    //jak_wacha_jak_sie_zachowuje="@@reakcja@@";
    set_slots(A_NECK);
    set_type(O_BIZUTERIA);
    add_prop(OBJ_I_WEIGHT, 13);
    add_prop(OBJ_I_VOLUME, 8);
    add_prop(OBJ_I_VALUE, 5);
    add_prop(ARMOUR_I_U_ROZCIAGLIWOSC, 40);
    add_prop(ARMOUR_I_D_ROZCIAGLIWOSC, 40);
    ustaw_material(MATERIALY_SZ_SREBRO, 40);
    ustaw_material(MATERIALY_STAL, 60);
    add_prop(ARMOUR_S_DLA_RASY, "elf");
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    add_prop(ARMOUR_I_NECKLACE, 1);
    set_size("M");
}
/*
string
zapach(string str)
{
  str="Ucho �mierdzi st�chlizn�, lecz znacznie mocniej odczuwalny "+
		"jest zapach jakiego� mocno chemicznego eliksiru.";
  return str;
}*/
