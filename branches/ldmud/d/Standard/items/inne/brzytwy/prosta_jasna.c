/* prosta jasna brzytwa
Made by Avard, 05.08.06
Opis by Valor */

inherit "/std/brzytwa.c";
#include <materialy.h>
#include <object_types.h>
#include <stdproperties.h>
#include <macros.h>

create_holdable_object()
{
    ustaw_nazwe("brzytwa");
    dodaj_przym("prosty","pro^sci");
    dodaj_przym("jasny","ja^sni");
    set_long("Uchwyt brzytwy wykonany zosta^l z jasnego drewna idealnie "+
        "le^z^acego w d^loni. Natomiast kilkucentymetrowe ostrze zrobione "+
        "z b^lyszcz^acej stali, zetnie ka^zdy, nawet najtwardszy zarost. "+
        "Brak jakichkolwiek ozd^ob i prostota wykonania wskazuj^a na to, "+
        "^ze brzytwa przeznaczona jest dla ubo^zszej warstwy spo^lecznej.\n");
     set_hit(2);
     set_pen(1);
     set_wt(W_KNIFE);
     set_dt(W_SLASH);
     set_hands(W_ANYH);
     set_likely_dull(MAXINT);
     set_likely_break(MAXINT);
     set_weapon_hits(9991999);
     ustaw_material(MATERIALY_DR_BUK, 66);
     ustaw_material(MATERIALY_STAL, 34);
     add_prop(OBJ_I_WEIGHT, 299);
     add_prop(OBJ_I_VOLUME, 340);
     add_prop(OBJ_I_VALUE, 90);
     set_type(O_INNE);

     setuid();
     seteuid(getuid());
}

