/*by ohm*/

inherit "/std/object";

#include <stdproperties.h>

void
create_object()
{
    ustaw_nazwe("haczyk");
    dodaj_przym("^prosty", "^prosty");
    dodaj_przym("z^abkowy", "z^abkowi");
     set_long("Pierwsz^a rzecz^a, kt^ora rzuca si^e w oczy jest zadzior znajduj^acy si^e "+
	"na ko^ncu cie^nkiego stalowego drutu, z jakiego zosta^l wykonany ten egzemplarz."+
	"Dzi^eki takiej budowie ryba, kt^ora nieopatrznie skusi^la si^e na przek^aske,"+
	"ma ma^le szanse na wyrwanie si^e z pu^lapki, w ktora wpad^la.\n");

    add_prop(OBJ_I_VALUE, 10);
    add_prop(OBJ_I_WEIGHT, 2);
}

int query_haczyk()
{
    return 1;
}

mixed
	query_przynety()
	{
	    return ({"kulka", "robak", "rybka", "chleb"});
}