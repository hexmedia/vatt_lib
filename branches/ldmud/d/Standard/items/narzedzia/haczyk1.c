/*by ohm*/

inherit "/std/object";

#include <stdproperties.h>

void
create_object()
{
    ustaw_nazwe("haczyk");
    dodaj_przym("^zelazny", "^zela^xni");
    dodaj_przym("prosty", "pro^sci");
     set_long("Haczyk ten, s^lu^z^acy do mocowania przyn^ety, zosta^l wykonany z "+
	"cienkiego, lecz zarazem sztywnego drutu. Nadzwyczaj prosta budowa i brak "+
	"zadziora, kt^ory pomaga nieco w utrzymaniu holowanej ryby na haczyku, powoduj^a, "+
	"i^z egzemplarz ten, jest bardziej odpowiedni dla do^swiadczonych w^edkarzy.\n");
   
    add_prop(OBJ_I_VALUE, 10);
    add_prop(OBJ_I_WEIGHT, 2);

}

int query_haczyk()
{
    return 1;
}

mixed
	query_przynety()
	{
	    return ({"kulka", "robak", "rybka", "chleb"});
}
