/*
 * Jest to zwyk�a lampa. Niemal�e "standardowa"
 *            Vera.
 */
 
/*
 * Sprzedawana jest w sklepie w Rinde
 */
 
inherit "/std/lampa";
#include <macros.h>
#include <object_types.h>
#include <materialy.h>
#include <stdproperties.h>


void
create_lamp()
{
    dodaj_przym("niewielki","niewielcy");
    dodaj_przym("por�czny","por�czni");
    set_long("Niewielka i wygodna lampa zaopatrzona zosta�a w "+
             "sk�rzany pasek, umo�liwiaj�cy przewieszanie jej przez "+
             "rami�. Aczkolwiek jest to model lampy nie nadaj�cy "+
             "si� na w�dr�wki po jaskiniach, a raczej na kr�tkie "+
             "przechadzki przez nieo�wietlone ulice i zau�ki miasta.\n"+
             "@@opis_oleju@@");
    add_prop(OBJ_I_VOLUME, 1400);
    add_prop(OBJ_I_WEIGHT, 800);
    add_prop(OBJ_I_VALUE, 160);
    
    set_time(900+random(100));
/*
//    set_left_time(0); // Na poczatku lampa jest pusta
    set_time_left(0);
    set_strength(4);
    configure_light();

    //test
    set_value(13);
*/
}
