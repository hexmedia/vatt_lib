inherit "/std/humanoid.c

#include <pl.h>
#include <stdproperties.h>

void
create_humanoid()
{
    ustaw_odmiane_rasy( ({"dzin","dzina","dzinowi","dzina","dzinem","dzinie"}),
        ({"dziny","dzinow","dzinom","dziny","dzinami","dzinach"}), PL_MESKI_NOS_ZYW);
    dodaj_przym("ogromny", "ogromne");
    set_gender(0);

    add_prop(LIVE_I_NEVERKNOWN, 1);

    default_config_mobile(300);
    set_aggressive(1);

}
