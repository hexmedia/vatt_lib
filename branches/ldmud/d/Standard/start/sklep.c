inherit "/std/room";
inherit "/lib/shop";

void
create_room()
{
    set_short("Sklep");
    set_long("Jestes w niewielkim sklepie."+
	    ""+
	    ""+
	    ""+
	    ""+
	    ""+
	    "\n");

    add_exit("/d/Standard/start/plac", "zachod");
    add_exit("/d/Standard/start/magazyn", "wschod", wiz_check);
    
    set_store_room("/d/Standard/start/magazyn");
    default_config_trade();
}

void
init()
{
    ::init();
    init_shop();
}
