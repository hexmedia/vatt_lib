inherit "/std/room";
inherit "/lib/pub";

void
create_room()
{
    set_short("Karczma");
    set_long("Mala, brudna knajpka."+
	    ""+
	    ""+
	    ""+
	    ""+
	    ""+
	    "\n");

    add_exit("/d/Standard/start/plac", "polnocny-wschod");
}

void
init()
{
    ::init();
    init_pub();
}
