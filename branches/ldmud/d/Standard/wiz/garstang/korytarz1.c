//Lil, operacja wstepne Thanedd, dnia 8.11.2005
inherit "/std/room";

#include <stdproperties.h>
#define GAR "/d/Standard/wiz/garstang/"

create_room()
{
    set_short("Kamienny Korytarz w Garstangu");
    set_long("Nagi, ciemny i ch�odny kamie� otacza ci� zewsz�d. Korytarz ci�gnie "+
    		"si� jeszcze dalej na p�noc.\n");
  
    add_exit(GAR+"schody_out",({"zach�d","na zach�d, poza mury Garstangu"}));
    add_exit(GAR+"sala_obrad.c",({"wsch�d","do sali obrad"}));
    add_exit(GAR+"korytarz2.c",({"n","wg��b korytarza"}));
    add_exit(GAR+"wiesci","s");
    add_exit(GAR+"bledy",({"sw","na forum Vox populi"}));
    add_exit(GAR+"projekty",({"se","do sali projekt�w"}));
}
