//Lil, operacja wstepne Thanedd, dnia 8.11.2005
inherit "/std/room";

#include <std.h>
#include <macros.h>
#include <stdproperties.h>
#define GAR "/d/Standard/wiz/garstang/"

int moze_przejsc();

void
create_room()
{
    set_short("Korytarz w zakazanej cz�ci Garstangu");
    set_long("OPIS DO NAPISANIA:P\n");

    add_exit("/d/Standard/obj/statue.c",({"s","przez metalowe drzwi"}), &moze_przejsc());
    add_exit(GAR+"korytarz2.c", ({"e", "przez drzwi na wsch�d", "zza zachodnich drzwi"}));
}

nomask int
moze_przejsc()
{
    if(SECURITY->query_wiz_rank(TP->query_real_name()) >= WIZ_MAGE)
        return 0;

    write("Jaka� magiczna si�a nie pozawala Ci tam wej��.\n");
    return 1;
}

nomask int
prevent_enter(object ob)
{
    if(SECURITY->query_wiz_rank(ob->query_real_name()) >= WIZ_MAGE)
        return 0;

    write("Jaka� magiczna si�a dosy�a Ci� w miejsce z kt�rego przyby�e�.\n");
    return 1;
}
