//Lil, operacja wstepne Thanedd, dnia 8.11.2005
inherit "/std/room";

#include <stdproperties.h>
#define GAR "/d/Standard/wiz/garstang/"

create_room()
{
    set_short("Sala obrad");
    set_long("Na samym �rodku tej ogromnej sali "+
    "stoi Wielki Okr�g�y St�, a dooko�a niego wygodne fotele.\n");

    add_prop(ROOM_I_INSIDE,1);
//    add_prop(CONT_I_MAX_RZECZY, 3);
    add_item("fotele",
        "Wygodne fotele otaczaj� Wielki Okr�g�y St�. Zauwa�asz, �e na ka�dym "+
        "wygrawerowane jest jakie� imi�, lecz najwi�kszy i najczytelniejszy "+
        "spo�r�d nich to \"Molder\".\n");
    add_item(({"st�","wielki st�","wielki okr�g�y st�","okr�g�y st�"}),
        "Jest ogromny! To przy nim zostaj� om�wione wszystkie najwa�niejsze sprawy "+
        "tego �wiata.\n");
    add_sit("przy stole","przy stole na fotelu z wygrawerowanym swoim imieniem",
                    "od sto�u",0);
    add_exit(GAR+"korytarz1","w");
    add_object(GAR+"urna");
}


public void
init()
{
   ::init();
   add_action("komendy","",1);
}

public int
komendy(string str)
{
  //  if (SECURITY->query_wiz_rank(this_player()->query_real_name()) >= WIZ_ARCH)
    //    return 0;
    switch(query_verb())
    {
	case "wiz":
        case "goto":
        case "Goto":
        case "tell":
        case "exec":
        case "Call":
        case "gwiz":
        case "echo":
        case "echoto":         
        case "destruct":
        case "Destruct":
	case "clone":
	case "load":
	case "odnow":
	case "More":
	case "control":
	case "possess":
	case "money":
	case "invis":
	case "snoop":
	case "tellall":
	case "trans":
	case "update":
	case "peace":
	case "retire":
	case "modify":
	case "Stat":
	case "stat":
	case "aft":
	case "audience":
	case "more":
	case "tail":
	case "cat":
	case "setmmin":
	case "setmmout":
	case "gtell":
        case "cp": 
	case "mv":
	case "ls":
	case "Ls":
	case "cd":
	case "move":
	case "Move":
	    write("Garstang ob�o�ony jest siln� aur� i blokad� antymagiczn�."+
	           " Nie podzia�a tu �adne zakl�cie.\n"); return 1;
    }
}
