inherit "/std/board";
#include <stdproperties.h>

void create_board()
{
    set_board_name("/d/Standard/wiz/notki");
    set_silent(0);
    add_prop(WIZARD_ONLY, 1);

    set_num_notes(100);
}

int wiesci(string str)
{
    int numer;

    // Jesli nie podano argumentu, wyswietl spis wiesci
    if (!str)
    {
        if (!msg_num)
            write("Nie ma w tej chwili �adnych wie�ci.\n");
        else
        {
            write("\nOto najnowsze dost�pne wie�ci:\n\n" +
            list_headers(1, msg_num) + "\n");
        }
        return 1;
    }

    // Jezeli jako argument podano liczbe, wyswietl wiesc o tym numerze
    if (sscanf(str, "%d", numer))
    {
        if ((numer < 1) || (numer > msg_num))
        {
            write("Nie ma wie�ci o takim numerze!\n");
            return 1;
        }
        write("\n");
        read_msg(str, 1);
        write("\n");
        return 1;
    }

    // W przeciwnym wypadku zwroc standardowy komunikat o niepowodzeniu
    return 0;
}