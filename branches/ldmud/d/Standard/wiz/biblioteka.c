//Lil, operacja wstepne Thanedd, dnia 8.11.2005
inherit "/std/room";
inherit "/lib/guild_support";
#include <stdproperties.h>
#include <macros.h>
#include <pl.h>

#define DOLNA_POLKA ({"dolna p�ka", "dolnej p�ki",\
    "dolnej p�ce", "doln� p�k�",\
    "doln� p�k�", "dolnej p�ce", "na"})
#define SRODKOWA_POLKA ({"�rodkowa p�ka", "�rodkowej p�ki",\
    "�rodkowej p�ce", "�rodkow� p�k�",\
    "�rodkow� p�k�", "�rodkowej p�ce", "na"})
#define GORNA_POLKA ({"g�rna p�ka", "g�rnej p�ki",\
    "g�rnej p�ce", "g�rn� p�k�",\
    "g�rn� p�k�", "g�rnej p�ce", "na"})


create_room()
{
    set_short("W bibliotece Akademii");
    set_long("Mi�dzy rz�dami ksi�g, przy g��wnym regale dostrzegasz "+
             "skromn� tabliczk�. Jest tu tak�e niewielki st�, przy kt�rym "+
             "zwykli zasiada� uczniowie studiuj�cy ksi�gi biblioteki.\n");
    add_item(({"tabliczk�","skromn� tabliczk�"}),
             "Na tabliczce napisano:\n"+
            "Skrypty i pergaminy dla adept�w nale�y zdj�� z dolnej p�ki."+
            "\n"+
             "Na �rodkowej p�ce le�� prace na temat ras.\n"+
             "Na g�rnej p�ce natomiast znajdziecie zw�j o hierarchii.\n"+
             "Poradniki i reszta jest w /doc.\n");
   add_item(({"rega�","g��wny rega�"}),
             "Jest to g��wny rega� biblioteczny"+
            "@@opis_sublokacji|. Na dolnej p�ce |dolna p�ka||||le�y |le�� |le�y @@"+
            "@@opis_sublokacji|. Na �rodkowej p�ce |�rodkowa p�ka||||le�y |le�� |le�y @@"+
            "@@opis_sublokacji|. Na g�rnej p�ce |g�rna p�ka||||le�y |le�� |le�y @@"+".\n");

    add_prop(ROOM_I_INSIDE,1);

    add_sit("przy stole","przy stole","od sto�u",5);
//     add_subloc("na dolnej p�ce rega�u",0,"z dolnej p�ki rega�u");
    add_subloc(DOLNA_POLKA);
    add_subloc(SRODKOWA_POLKA);
    add_subloc(GORNA_POLKA);
    add_subloc_prop("dolna p�ka", SUBLOC_I_MOZNA_ODLOZ, 1);
    add_subloc_prop("�rodkowa p�ka", SUBLOC_I_MOZNA_ODLOZ, 1);
    add_subloc_prop("g�rna p�ka", SUBLOC_I_MOZNA_ODLOZ, 1);
//     add_subloc("na �rodkowej p�ce rega�u",0,"z �rodkowej p�ki rega�u");
//     add_subloc("na g�rnej p�ce rega�u",0,"z g�rnej p�ki rega�u");
//     add_subloc_prop("dolna p�ka rega�u", CONT_I_CANT_ODLOZ, 1);
//     add_subloc_prop("dolna p�ka rega�u", CONT_I_CANT_POLOZ, 1);


    add_object("/d/Standard/wiz/prace/wiz_newbie", 8, 0, DOLNA_POLKA);
    add_object("/d/Standard/wiz/prace/elfy.c", 1, 0, SRODKOWA_POLKA);
    add_object("/d/Standard/wiz/prace/zwoj_niziolki.c", 1, 0, SRODKOWA_POLKA);
    add_object("/d/Standard/wiz/prace/zwoj_hierarchia.c", 1, 0, GORNA_POLKA);

    add_exit("wizroom","w");
    add_exit("slownik","e");

    create_guild_support();
}

void init()
{
    ::init();
    init_guild_support();
}

void
leave_inv(object ob, object dest)
{
    gs_leave_inv(ob, dest);
    ::leave_inv(ob, dest);
}
