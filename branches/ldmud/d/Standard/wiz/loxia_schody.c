//Lil, operacja wstepne Thanedd, dnia 8.11.2005
inherit "/std/room";
create_room()
{
   set_short("Na schodach w Loxii");
   set_long("Pokr�cone schody prowadz� do Aretuzy. Jest tutaj teleport "+
            "do ratusza w Rinde.\n");
   add_exit("/d/Standard/wiz/schody_do_loxii","n");
   add_exit("/d/Standard/wiz/dywanik",({"w","do Vox Mortalis na zachodzie",
				"z trumny"}));
   add_exit("/d/Standard/Redania/Rinde/Centrum/Ratusz/lokacje/korytarz",({"teleport","przez teleport do Rinde"}));
   add_object("/d/Standard/wiz/tablica_rozwoj_muda");
}
