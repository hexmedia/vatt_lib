
/* 
 * Praca na temat nizio�k�w
 * Wykonana przez Avarda, dnia 25.04.06
 */
#pragma save_binary

inherit "/std/scroll";

#include <stdproperties.h>
#include <pl.h>
#include <macros.h>

void
create_scroll()
{
    seteuid(getuid(this_object()));
    ustaw_nazwe("pergamin");
    dodaj_przym("bia�y", "biali");
    set_long("\"Nizio�k�w Rasy Opisanie\". Pi�ra Avarda Zaganzara\n");
    set_autoload();
    set_file("/d/Standard/wiz/prace/niziolki.txt");
    add_prop(WIZARD_ONLY, 1);
}
