
/* 
 * Praca na temat hierarchii
 * Wykonana przez <wielka plama po kawie!>
 */
#pragma save_binary

inherit "/std/scroll";

#include <stdproperties.h>
#include <pl.h>
#include <macros.h>

void
create_scroll()
{
    seteuid(getuid(this_object()));
    ustaw_nazwe("zw�j");
    dodaj_przym("stary", "starzy");
    set_long("Wyra�nie i starannie wykaligrafowany tytu� "+
             "tego zwoju brzmi: \"O zasadach, jak i hierarchii w�r�d "+
             "czarodziej�w s��w kilka\".\n");
    set_autoload();
    set_file("/d/Standard/wiz/prace/hierarchia.txt");
    add_prop(WIZARD_ONLY, 1);
}
