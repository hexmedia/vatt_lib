// na podstawie katalogu fryzjerskiego. potem moze cos zmienie - gjoef.

inherit "/std/book";

#include <object_types.h>
#include <stdproperties.h>
#include <macros.h>

create_book()
{
    ustaw_nazwe("ksi^ega");
    dodaj_nazwy("ksi^a^zka");
    
    dodaj_przym("stary", "starzy");
    
    set_long("Grube, ci^e^zkie, oprawione w sk^or^e tomiszcze. Na grzbiecie " +
             "z^lotymi literami wygrawerowany jest napis \"Aen Seidhe\".\n");

    add_prop(OBJ_I_WEIGHT, 1500);
    add_prop(OBJ_I_VOLUME, 900);
    add_prop(WIZARD_ONLY, 1);

    set_max_pages(7);
    set_type(O_KSIAZKI);
}

void
read_book_at_page(int strona)
{
    switch (strona)
    {
	case 1: write("   SPIS TRE^SCI\n\n" +
                  "2  Przedmowa.\n" +
                  "3  Historia.\n" +
                  "4  Kultura.\n" +
                  "5  Cechy.\n" +
                  "6  Relacje.\n" +
                  "7  Suplement.\n");
	        break;
		
	case 2: write("Przedmowa.\n" +
                  "    Pisz^ac ten dokument, mia^lem na celu ukazanie sylwet" +
                  "ek poszczeg^olnych ras w roku akcji gry, czyli - jak obli" +
                  "czy^lem - ok. 1245. Nale^zy zauwa^zy^c, ^ze ^owczesne rel" +
                  "acje mi^edzy lud^xmi a nielud^xmi nie by^ly jeszcze tak k" +
                  "iepskie jak na pocz^atku Sagi.\n" +
                  "    Przepraszam za ewentualne powt^orzenia, ale czasem na" +
                  "prawd^e trudno o synonimy do wyrazu \"elf\".\n\n" +
                  "Mi^lej i owocnej lektury,\n" +
                  "Gjoef.\n");
            break;
            
    case 3: write("Historia.\n" +
                  "    Ich cywilizacja liczy sobie kilkadziesi^at tysi^ecy l" +
                  "at, ale pierwsze elfie okr^ety przyp^lyn^e^ly na Kontynen" +
                  "t dopiero oko^lo -250 r. I to bynajmniej nie z pokojowymi" +
                  " zamiarami - elfy zacz^e^ly przyw^laszcza^c sobie nowe zi" +
                  "emie. Nie spodoba^lo si^e to tubylcom - krasnoludom, nizi" +
                  "o^lkom i gnomom - skutkiem czego by^ly liczne potyczki zb" +
                  "rojne miedzy nimi a nieproszonymi go^s^cmi. Po d^lu^zszym" +
                  " czasie strony zaniecha^ly walki, lecz nawet do tej pory " +
                  "nie pogodzi^ly si^e do ko^nca.\n");
            break;
            
    case 4: write("Kultura.\n" +
                  "    Elfy mog^a poszczyci^c si^e du^zym dorobkiem kulturow" +
                  "ym. Niestety, same musia^ly zniszczy^c niez^l^a ilo^s^c w" +
                  "ielkich dzie^l artystycznych i naukowych, gdy wycofywa^ly" +
                  " si^e przed lud^xmi wg^l^ab Kontynentu. Cho^c do dnia dzi" +
                  "siejszego przetrwa^lo ca^lkiem sporo manuskrypt^ow, to i " +
                  "tak s^a one nafaszerowane symbolami, akrostychami, szyfra" +
                  "mi i chorobliwie trudno je zrozumie^c ludziom.\n" +
                  "    Elfy s^a te^z ^swietnymi budowniczymi. Dzi^s na miejs" +
                  "cu ich miast zalegaj^a gruzy, ale jeszcze jakie^s dwie^sc" +
                  "ie lat temu pierwsi ludzie mogli podziwia^c pi^ekno osied" +
                  "l Starszego Ludu. Hen Gedymdeith w swoim dziele \"Elfy i " +
                  "ludzie\" okre^sla je jako \"tak delikatne, jakby utkane z" +
                  " porannej mg^ly\" i opisuje \"wie^zyczki zdaj^ace si^e by" +
                  "^c uplecione z bluszczu\" i \"mosty zwiewne jak p^lacz^ac" +
                  "e wierzby\". Pozosta^lo^sci ich architektury mo^zna znale" +
                  "^xc na terenie Akademii Oxenfurckiej, kt^ora wszak zbudow" +
                  "ana jest w^la^snie na elfich fundamentach.\n" +
                  "    Opr^ocz du^zej wiedzy ze wszystkich dziedzin nauki, e" +
                  "lfy przywioz^ly ze sob^a w^lasny j^ezyk, zwany Starsz^a M" +
                  "ow^a. Szybko zyska^la ona popularno^s^c i u^zywana jest d" +
                  "o dzi^s, i to nie tylko przez elfy, ale te^z driady, hama" +
                  "driady, rusa^lki i inne nimfy, a nawet przez ludzi z wysp" +
                  " Skellige i Nilfgaardu. Hen llinge charakteryzuje si^e ty" +
                  "m, ^ze ka^zde s^lowo ma kilka znacze^n, w zwi^azku z czym" +
                  " czasem trudno jest znale^x^c prawdziwy sens wyrwanego z " +
                  "kontekstu zdania.\n" +
                  "    Warto te^z zauwa^zy^c, ^ze elfy nie maj^a ^zadnych b^" +
                  "ostw ani obiekt^ow kultu. Swoj^a postaw^a reprezentuj^a i" +
                  "dee przypominaj^ace te o^swieceniowe - twierdz^a, ^ze wsz" +
                  "ystko mo^zna wyt^lumaczy^c naukowo, uwa^zaj^a umys^l za j" +
                  "edyne ^xr^od^lo poznania etc. ^Slepe wierzenia innych ras" +
                  " maj^a po prostu za ciemnot^e.\n");
            break;

	case 5: write("Cechy rasy.\n" +
                  "    \"Jak zwykle u przedstawicieli Starszego Ludu, nie sp" +
                  "os^ob by^lo oceni^c jego wieku, m^og^l r^ownie dobrze mie" +
                  "^c dwadzie^scia jak i sto dwadzie^scia lat\". Istotnie. E" +
                  "lf - czy mlody, czy stary (a elfy potrafi^a by^c naprawd^" +
                  "e wiekowe) - wygl^ada tak samo. Ka^zdy jest wysoki, szczu" +
                  "p^ly i og^olnie pi^ekny; ma wielkie oczy, delikatne, kszt" +
                  "a^ltne d^lonie, drobne bia^le z^abki i - co najwa^zniejsz" +
                  "e - spiczaste uszy. Elfy od innych ras r^o^zni te^z fakt," +
                  " ^ze nie maj^a zarostu i og^olne ich ow^losienie jest rac" +
                  "zej znikome.\n" +
                  "    Nosz^a te^z charakterystyczn^a odzie^z. W sk^lad typo" +
                  "wego elfiego ubrania wchodz^a wysokie, zapinane na klamer" +
                  "ki buty, koszula (zwykle jedwabna, nie lniana) i kubracze" +
                  "k. Elfki zwykle chodz^a w eleganckich, kolorowych sukniac" +
                  "h i obwieszaj^a si^e ca^le bi^zuteri^a.\n" +
                  "    Aen Seidhe s^a inteligetne i zr^eczne, tote^z opanowa" +
                  "^ly wiele wi^ekszych i mniejszych umiej^etno^sci, takich " +
                  "jak chocia^zby czytanie, pisanie, gra na instrumentach, t" +
                  "a^nczenie, jazda konna, strzelanie z ^luku, wierszowanie " +
                  "i tak dalej, i tak dalej. Posiadaj^a wiedz^e z wielu dzie" +
                  "dzin nauki, na przyk^lad atronomii, matematyki, medyki cz" +
                  "y magii.\n" +
                  "    Wi^ekszosc elf^ow przestrzega zasad kultury osobistej" +
                  ". Nie przeklinaj^a (przynajmniej nie tyle, co np. ludzie " +
                  "lub krasnoludy), nie upijaj^a si^e ani nie konsumuj^a ^za" +
                  "dnych u^zywek. M^owi^a czyst^a mow^a, bez ^zadnych b^l^ed" +
                  "^ow i r^o^znych takich.\n");
	        break;

    case 6: write("Relacje.\n" +
                  "    Elfy raczej nie przepadaj^a za osobnikami innych ras." +
                  " G^l^ownie przez zarozumia^lo^s^c i wysokie mniemanie o s" +
                  "obie, ale dok^ladaj^a si^e te^z do tego stare wa^snie mi^" +
                  "edzy nimi a innymi rasami - jak ju^z wcze^sniej wspomina^" +
                  "lem, najpierw elfy zabra^ly krasnoludom i gnomom spory ka" +
                  "wa^lek ziemi, a potem im samym wydarli tereny ludzie. Cho" +
                  "^c te wydarzenia mia^ly miejsce kilkaset lat temu, to wza" +
                  "jemna niech^e^c ras do siebie pozosta^la pod dzie^n dzisi" +
                  "ejszy.\n" +
                  "    Same te^z nie s^a lubiane, zar^owno przez ludzi, jak " +
                  "i nieludzi. Z tych drugich szczeg^olnie krasnoludy nie kr" +
                  "yj^a si^e ze swoimi pogl^adami w tej sprawie, cho^c objaw" +
                  "ia si^e to co najwy^zej w g^lupich ^zarcikach na ich tema" +
                  "t. Elfy i tak nie zwracaj^a na nie uwagi, uwa^zaj^ac je z" +
                  "a grubia^nskie i po prostu g^lupie.\n");
	        break;
	        
    case 7: write("Suplement - kr^otki przegl^ad elfich imion.\n" +
                  "    K. Shiadhal, Aelirenn, Ettariel, Ithlinne, Toruviel, " +
                  "Francesca.\n" +
                  "    M. Galarr, Crevan, Avallac'h, Yaevinn, Filavandrel, I" +
                  "sengrim.\n");
            break;
    }
}
