/*
 * bielu� - {Bielu� dzi�dzierzawa, Datura stramonium, 
 * roczna roslina zielna (wys. do 1 m) z rodziny psiankowatych; 
 * zawiera silnie truj�ce alkaloidy: skopolamin�, atropin� i hioscyjamin�; 
 * kwiaty du�e, lejkowate; owocem jest kolczasta torebka, przypominajaca 
 * owoc kasztanowca; Eurazja, Afryka, Ameryka Pn.; w Polsce pospolity chwast; 
 * li�cie i nasiona leczn.;} sk�adnik eliksir�w, OZ, W26
 * * 
 * bielun =  datura {= bielu� dzi�dzierzawa, Datura stramonium}
 *     - eliksir daturowy - silnie dzia�aj�ce lekarstwo, WJ 15 
 *     - mieszanka datury i akonitum, WJ 16
 *
 */

inherit "/std/herb";

#include <pl.h>
#include <herb.h>
#include <mudtime.h>
#include <materialy.h>
#include <stdproperties.h>

void
create_herb()
{
    ustaw_nazwe("owoc");
    dodaj_przym("okr^ag^ly", "okr^agli");
    dodaj_przym("kolczasty", "kolcza^sci");

    ustaw_nazwe_glowna_ziola("owoc");
    ustaw_nazwe_ziola("bielu�");

    ustaw_id_nazwe_calosci("bielu�");

    ustaw_unid_nazwe_calosci("owoc");
    dodaj_unid_przym_calosci("wysoki", "wysocy");
    dodaj_unid_przym_calosci("pachn^acy", "pachn^acy");

    set_id_long("Cienki, lecz d^lugi korze^n arcydzi^egla, pokryty jest pod^luznymi "+
        "bruzdami. Wydziela przyjemny, korzenny zapach. Arcydzi^egiel zawiera "+
        "spore ilo�ci olejk^ow eterycznych, dzi^eki czemu dzia^la przeciwskurczowo, "+
        "wzmacniaj�co i uspokajaj�co. \n");

    set_unid_long("Cienki, lecz d^lugi korze^n, pokryty pod^luznymi bruzdami. "+
        "Wydziela przyjemny, korzenny zapach.\n");

    dodaj_komende_zbierania("zerwij", "zrywasz", "zrywa", "zerwa�");

    ustaw_czas_psucia(350, 400); // schniecie niszczenie
    ustaw_trudnosc_identyfikacji(27); 
    ustaw_czestosc_wystepowania(80);

    ustaw_trudnosc_znalezienia(20); 

    ustaw_przym_zepsutego("ususzony", "ususzeni");

    set_amount(15); //wartosc od^zywcza

    add_prop(OBJ_I_VOLUME, 45); 
    add_prop(OBJ_I_WEIGHT, 50); 
    add_prop(OBJ_I_VALUE, 34); 

    ustaw_material(MATERIALY_IN_ROSLINA);

    ustaw_pore_roku(MT_LATO | MT_JESIEN);
}
