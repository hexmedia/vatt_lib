/*
 * Autor: Feave, dn. 20 maja 2007
 * Opis: Brz^ozek
 * 
 */


inherit "/std/herb";

#include <pl.h>
#include <herb.h>
#include <materialy.h>
#include <stdproperties.h>
#include <mudtime.h>


void
create_herb()
{
    ustaw_nazwe("kwiatki");


    ustaw_nazwe_glowna_ziola("ki^s^c");

    ustaw_nazwe_ziola("lilak");

	dodaj_przym("drobny","drobni");
    dodaj_przym("bia^ly","biali");

    
    ustaw_id_nazwe_calosci("lilak");
    dodaj_id_przym_calosci("");

    ustaw_unid_nazwe_calosci(({"szeroko rozga^l^eziony krzew","szeroko rozgałęzionego krzewu);

    set_id_long("Niedu^ze, lejkowate kwiatki o koronie z^lo^zonej ze "+
		"zro^sni^etych czterech b^ad^x pi^eciu p^latk^ow koloru bia^lego, "+
		"tworz^a ci^e^zk^a ki^s^c kwiatostanu bzu lilaka. Kwiatki "+
		"wydzielaj^a mocny lecz przyjemny zapach przynosz^acy ulg^e "+
		"sko^latanym nerwom. Napar z tego aromatycznego kwiecia delikatnie "+
		"wstrzymuje biegunk^e i pobudza apetyt a wymieszany z wi^eksz^a "+
		"ilo^sci^a miodu daje doskona^ly syrop stosowany w leczeniu tak "+
		"gro^xnego dla dzieci kokluszu. \n");
 
    set_unid_long("Niedu^ze, lejkowate kwiatki o koronie z^lo^zonej ze "+
		"zro^sni^etych czterech b^ad^x pi^eciu p^latk^ow koloru bia^lego, "+
		"tworz^a ci^e^zk^a ki^s^c kwiatostanu. Kwiatki wydzielaj^a mocny lecz "+
		"przyjemny zapach przynosz^acy ulg^e sko^latanym nerwom. \n"); 


    ustaw_komendy_zbierania((["zerwij" : ({"zrywasz", "zrywa",
"zerwa^c"})]));

    ustaw_czas_psucia(500, 600); // schniecie niszczenie
    ustaw_trudnosc_identyfikacji(10);
    ustaw_czestosc_wystepowania(30);
   
    ustaw_przym_zepsutego("ususzony", "ususzeni");

    set_amount(15); //wartosc od^zywcza

    add_prop(OBJ_I_VOLUME, 90); 
    add_prop(OBJ_I_WEIGHT, 120); 
    add_prop(OBJ_I_VALUE, 9); 

    ustaw_material(MATERIALY_IN_ROSLINA);
    ustaw_pore_roku( MT_WIOSNA );
}