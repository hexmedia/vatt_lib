/*
 * Krun i Faeve 
 * opis pewnie by Tin
 *
 */

inherit "/std/herb";

#include <pl.h>
#include <herb.h>
#include <mudtime.h>
#include <materialy.h>
#include <stdproperties.h>

void
create_herb()
{
    ustaw_nazwe("li^s^c");
    dodaj_przym("pod^lu^zny", "pod^lu^znini");
    dodaj_przym("lancetowaty", "lancetowaci");      

    ustaw_nazwe_glowna_ziola("li^s^c");
    ustaw_nazwe_ziola("babka lancetowata");
    dodaj_nazwy("babka");

    dodaj_id_nazwy_calosci("babka lancetowata");
    ustaw_id_short_calosci(({"li^s^c babki lancetowatej",
        "li^scia babki lancetowatej", "li^sciowi babki lancetowatej",
        "li^s^c babki lancetowatej", "li^sciem babki lancetowatej",
        "li^sciu babki lancetowatej"}),
       ({"li^scie babki lancetowatej", "li^sci babki lancetowatej",
        "li^sciom babki lancetowatej", "li^scie babki lancetowatej",
        "li^s^cmi babki lancetowatej", "li^sciach babki lancetowatej"}),
        PL_ZENSKI); 

    dodaj_id_nazwy_calosci("li^s^c");
    dodaj_id_przym_calosci("pod^lu^zny", "pod^lu^znini");
    dodaj_id_przym_calosci("lancetowaty", "lancetowaci"); 

    ustaw_unid_nazwe_calosci("ro^slina");
    dodaj_unid_przym_calosci("pod^lu^zny", "pod^lu^zni");
    dodaj_unid_przym_calosci("lancetowaty", "lancetowaci"); 

    set_id_long("Do^s^c pot^e^zny i masywny, jak na tak niedu^z^a "+
        "ro^slin^e, nieregularnie poskr^ecany korze^n o niebieskawej "+
        "barwie. Z boku gdzieniegdzie wyrastaj^a male^nkie odn^o^zki, a z "+
        "lekko odartych fragment^ow tr^aci ostrym, do^s^c nieprzyjemnym "+
        "zapachem. Korze^n biedrze^nca jest doskona^lym ^srodkiem "+
        "wykrztu^snym oraz rozkurczaj^acym - ususzony nieco pokruszy^c, "+
        "niewielk^a ilo^s^c zala^c wrz^atkiem i pi^c w niewielkich dawkach. "+
        "Nale^zy uwa^za^c, bo wi^eksza ilo^s^c mo^ze niekorzystnie "+
        "wp^lywa^c na nerki. \n");

    set_unid_long("Do^s^c pot^e^zny i masywny, jak na tak niedu^z^a "+
        "ro^slin^e, nieregularnie poskr^ecany korze^n o niebieskawej "+
        "barwie. Z boku gdzieniegdzie wyrastaj^a male^nkie odn^o^zki, a z "+
        "lekko odartych fragment^ow tr^aci ostrym, do^s^c nieprzyjemnym "+
        "zapachem. \n");

    ustaw_czas_psucia(350, 400); // schniecie niszczenie
    ustaw_trudnosc_identyfikacji(17);
    ustaw_czestosc_wystepowania(80);
    ustaw_sposob_wystepowania(HERBS_WYST_MALE_GRUPY);

    ustaw_ilosc_w_calosci(3);

    ustaw_trudnosc_znalezienia(25); 

    ustaw_przym_zepsutego("ususzony", "ususzeni");

    set_amount(5); //wartosc od^zywcza

    add_prop(OBJ_I_VOLUME, 15); //FIXME
    add_prop(OBJ_I_WEIGHT, 7); //FIXME
    add_prop(OBJ_I_VALUE, 12); //FIXME

    ustaw_material(MATERIALY_IN_ROSLINA); 

    ustaw_pore_roku(MT_WIOSNA | MT_LATO);
}
