/*
 * Autor: Feave
 * Opis: Tinardan i faeve
 * 23.06.2007
 */

inherit "/std/herb";
inherit "/lib/kwiat";

#include <pl.h>
#include <herb.h>
#include <mudtime.h>
#include <materialy.h>
#include <stdproperties.h>

void
create_herb()
{
    ustaw_nazwe("kwiat");
    dodaj_przym("niewielki", "niewielcy");
    dodaj_przym("b^l^ekitny", "b^l^ekitni");

    ustaw_nazwe_glowna_ziola("kwiat");
    ustaw_nazwe_ziola("chaber");

    ustaw_id_nazwe_calosci("chaber");

    set_id_long("^Smieszne, postrz^epione p^latki tul^a si^e do ciemnego "+
		"^srodka otaczaj^ac go zwartym k^l^ebem. Kolor kwiatka jest "+
		"ciemnob^l^ekitny, niczym g^l^eboka woda albo niebo przed zmierzchem "+
		"- intensywny, czasem nieco zabarwiony fioletem, swoj^a barw^a niemal "+
		"k^luj^acy w oczy. Kiwa si^e na d^lugiej, cienkiej ^lody^zce, niczym "+
		"zako^nczona ^sliczn^a koron^a zwyk^la trawka. P^latki chabra to "+
		"^srodek przeciwzapalny tak delikatny, ^ze napar z nich mo^zna "+
		"stosowa^c jako ok^lad na oczy, natomiast pity dwa razy dziennie "+
		"pomaga w chorobach serca i nerek. \n");

    set_unid_long("^Smieszne, postrz^epione p^latki tul^a si^e do ciemnego "+
		"^srodka i otaczaj^a go zwartym k^l^ebem. Kolor kwiatka jest "+
		"ciemnob^l^ekitny, niczym g^l^eboka woda albo niebo przed zmierzchem "+
		"- intensywny, czasem nieco zabarwiony fioletem, swoj^a barw^a "+
		"niemal k^luj^acy w oczy. Kiwa si^e na d^lugiej, cienkiej ^lody^zce, "+
		"niczym zako^nczona ^sliczn^a koron^a zwyk^la trawka.\n");

    dodaj_komende_zbierania("zerwij", "zrywasz", "zrywa", "zerwa^c");

    ustaw_czas_psucia(320, 400); // schniecie niszczenie
    ustaw_trudnosc_identyfikacji(5);
    ustaw_czestosc_wystepowania(70);

    ustaw_trudnosc_znalezienia(7);

    ustaw_przym_zepsutego("ususzony", "ususzeni");

    set_amount(2); //wartosc od^zywcza

    add_prop(OBJ_I_VOLUME, 7);
    add_prop(OBJ_I_WEIGHT, 15);
    add_prop(OBJ_I_VALUE, 7);

    ustaw_material(MATERIALY_IN_ROSLINA);

    ustaw_pore_roku(MT_WIOSNA | MT_LATO);

    set_wianek("/d/Standard/items/inne/wianek_chaber.c");
}

void
init()
{
    ::init();
    init_kwiat();
}