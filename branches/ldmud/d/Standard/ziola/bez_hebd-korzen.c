/*
 * wyst^epowanie: lasy bukowe
 * Cieniste lasy, zaro�la. zr�by, przydro�a.
 *
 *
 * opis by Brz^ozek
 */

inherit "/std/herb";

#include <pl.h>
#include <herb.h>
#include <mudtime.h>
#include <materialy.h>
#include <stdproperties.h>

void
create_herb()
{
    ustaw_nazwe("korze^n");
    dodaj_przym("du^zy", "duzi");
    dodaj_przym("br�zowy", "br�zowi");      

    ustaw_nazwe_glowna_ziola("korze^n");
    ustaw_nazwe_ziola("bez hebd");
    dodaj_nazwy("bez");

    dodaj_id_nazwy_calosci("bez hebd");
    ustaw_id_short_calosci(({"korze^n bzu hebd",
        "korzenia bzu hebd", "korzeniowi bzu hebd",
        "korze^n bzu hebd", "korzeniem bzu hebd",
        "korzeniu bzu hebd"}),
       ({"korzenie bzu hebd", "korzeni bzu hebd",
        "korzeniom bzu hebd", "korzenie bzu hebd",
        "korzeniami bzu hebd", "korzeniach bzu hebd"}),
        PL_ZENSKI); 

    dodaj_id_nazwy_calosci("krzew");
    dodaj_id_przym_calosci("du^zy", "duzi");
    dodaj_id_przym_calosci("br�zowy", "br�zowi"); 

    ustaw_unid_nazwe_calosci("krzew");
    dodaj_unid_przym_calosci("du^zy", "duzi");
    dodaj_unid_przym_calosci("br�zowy", "br�zowi"); 

    set_id_long("Do�^c gruby, p^lo^z�cy si^e korze^n hebdu zabarwiony na "+
        "ciemnobr�zowo ^latwo rozpozna^c po silnym, niemi^lym zapachu. Po "+
        "wysuszeniu sporz�dza si^e z niego napar o silnym dzia^laniu "+
        "moczop^ednym. \n");

    set_unid_long("Do�^c gruby, p^lo^z�cy si^e korze^n zabarwiony na "+
        "ciemnobr�zowo wydziela siln� nieprzyjemn� wo^n. \n");

    ustaw_czas_psucia(350, 400); // schniecie niszczenie
    ustaw_trudnosc_identyfikacji(25);
    ustaw_czestosc_wystepowania(45);
    ustaw_sposob_wystepowania(HERBS_WYST_POJEDYNCZO);

    ustaw_ilosc_w_calosci(1);

    ustaw_trudnosc_znalezienia(13); 

    ustaw_przym_zepsutego("ususzony", "ususzeni");

    dodaj_komende_zbierania("wyrwij", "wyrywasz", "wyrywa", "wyrwa�");

    set_amount(5); //wartosc od^zywcza

    add_prop(OBJ_I_VOLUME, 13); //FIXME
    add_prop(OBJ_I_WEIGHT, 6); //FIXME
    add_prop(OBJ_I_VALUE, 10); //FIXME

    ustaw_material(MATERIALY_IN_ROSLINA); 

    ustaw_pore_roku(MT_WIOSNA | MT_JESIEN);
}
