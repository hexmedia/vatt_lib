inherit "/std/room";

#pragma strict_types
#pragma no_clone
#pragma no_inherit
#pragma binary_save

#include <pl.h>
#include <std.h>
#include <filepath.h>
#include <stdproperties.h>
#include <macros.h>
#include <log.h>
#include <files.h>

public string   dlugi();
private int     qadmin(object ob);

public void
create_room()
{
    set_short("W ogromnej maszynie - Pokoju zarz�dzania ludem Vatt'gherna");
    set_long(&dlugi());

    add_prop(ROOM_I_LIGHT, 1);
    add_prop(ROOM_I_NO_CLEANUP, 1);
    add_prop(ROOM_I_INSIDE, 1);
    add_exit("/d/Standard/wiz/wizroom", ({"wyj�cie", "z powrotem do Wielkiego Kompleksu pomieszcze� Czarodziei", "z pokoju zarz�dzania ludem"}) );
    setuid();
    seteuid(getuid());
}

public string
dlugi()
{
  string ret;

  ret = "Znajdujesz si� wewn�trz maszyny Pokoju " +
    "zarz�dzania ludem Vatt'gherna.\n";

  ret += "Dost�pne komendy:\n" +
    "npce\t\t- Wypisanie podstawowych informacji o za�adowanych npcach\n" +
    "info <#num>\t- Wypisanie dok�adniejszych informacji o podanym npcu\n";

  if (qadmin(this_player()))
  {
    ret += "TODO\n";
  }

  return ret + "\n";
}

public void
init()
{
    ::init();
    add_action("short_npce", "npce");
    add_action("info_npc", "info");
}

private int
qadmin(object ob)
{
	if (SECURITY->query_wiz_rank(ob->query_real_name()) >= WIZ_MAGE)
		return 1;
	return 0;
}

int
short_npce(string str)
{
	if (strlen(str))
	{
		notify_fail("Ta komenda nie przyjmuje �adnych argument�w.\n");
		return 0;
	}
	NPCE_OBJECT->short_npce();
	return 1;
}

int
info_npc(string str)
{
    int num;
    string foobar;

	if (!stringp(str) || sscanf(str, "%d %s", num, foobar) != 1)
	{
		notify_fail("Spos�b u�ycia: info <#num>\n");
		return 0;
	}
    NPCE_OBJECT->info_npc(num);
    return 1;
}
