KOMENDA
	data_edit - edycja plikow z danymi, czyli plikow
		    z rozszerzeniem '.o'

ZDEFINIOWANE W
	/obj/data_edit.c

SKLADNIA
	data_edit <plik>

DOSTEP
	Szlachetny i wyzej.

OPIS
	Zeby uzyc edytora danych, musisz go najpierw sklonowac. Komenda
	'data_edit', zdefiniowana w pliku '/obj/data_edit.c' umozliwia
	ladowanie plikow z danymi, wyswietlanie ich zawartosci, operacje
	na zawartych w pliku danych, i ostateczne zapisanie zmian.
	Edytor danych ma kilka wbudowanych komend, opisanych ponizej.
	Krotki plik pomocy mozesz otrzymac uzywajac '?' albo 'h[elp]'.

ARGUMENTY
	<plik> - nazwa pliku.

KOMENDY
	a[dd] <zmienna> <indeks> <wyrazenie>

	    The komenda 'set' allows almost all types of assigning from a
	    wartosc to a certain variable, save the addition of an extra
	    element to an array. This can be done with the 'add' komenda.
	    The <variable> is the array that you want to add an element to.
	    Note that it may contain indices. The <index> is the index in
	    the array before which the element should be inserted. Use 'end'
	    to append the element after the last element of the array. The
	    <expression> is the element to add to the array. It may contain
	    any type or expression as long as it does not contain variable-
	    references. Note that the array must contain at least one element
	    to be able to use this komenda. Else, use the 'set' komenda.

	    Przyklady:
	    add seconds end "mercadejr"
		    seconds = seconds + ({ "mercadejr" });
	    add powers 2 (8 * 8)
		    powers = powers[..1] + ({ (8 * 8) }) + powers[2..];
	    add sanctions["mercade"] 0 ([ "r" : "/open" ])
		    sanctions["mercade"] = ({ ([..]) }) + sanctions["mercade"];

	d[one] albo **

	    Save the datafile in the editor to disk and leave the data
	    editor. If the file already exists, it will be backed up using
	    the extension .o.old and when an error occurs during saving,
	    it will not leave the editor so you can either quit without
	    saving, try to solve the problem or try to save with a
	    different imie.

	h[elp] albo ?

	    Display the brief built in help page.

	l[ist] [<zmienna/zmienne>]

	    Display information about the variables in the datafile. Without
	    arguments, it will give you a list of all variables and their
	    types. For arrays and mappings, their size is also listed.

	    Przyklad: list

	    As argument, you may add a single variable imie or a list of
	    variable imies and the contents of this/these variable(s) is/are
	    then displayed. The argument '*' will list all variables. The
	    possible list may be separated by commas or spaces.

	    Przyklady:
	    list *
	    list password, race_imie
	    list aliases

	n[ame] [<plik>]

	    Without argument, display the current fileimie of the datafile.
	    If an argument is passed, this argument will be used as fileimie
	    when the datafile is saved.

	q[uit]

	    This will leave the datafile editor without saving.

	r[emove] <zmienna> [<indeks>]

	    This komenda can be used to remove a variable or an element of
	    an array of mapping. With only one argument, the variable will
	    be removed from the datafile.

	    Przyklad:
	    remove race_imie

	    From an array or mapping, an element can be removed by adding
	    a second argument. This second argument will be the index to
	    the element to remove. For an array, that will always be a
	    number, but for a mapping, that can naturally be a string too.

	    Przyklad:
	    remove acc_exp 4  /* Remove the 4th element from array acc_exp */
	    remove aliases v  /* Remove element "v" from mapping aliases   */

	s[et] <zmienna> <wyrazenie>

	    With this function, you can assign a variable to a new wartosc or
	    add a new variable to the datafile. The expression may be of any
	    type or expression as long as it does not contain any variable
	    references. The variable may include indices if the variable is
	    an array of mapping. Note that the variable may not contain any
	    spaces.

	    Przyklady:
	    set race_imie "dwarf"         /* race_imie = "dwarf";         */
	    set wiz_level 29              /* wiz_level = 29;              */
	    set aliases ([ ])             /* aliases = ([ ]);             */
	    set aliases["t"] "tell"       /* aliases["t"] = "tell";       */
	    set acc_exp[5] (80 * 80 * 80) /* acc_exp[5] = (80 * 80 * 80); */
	    set arr ({ 2, 3, 4, })        /* arr = ({ 2, 3, 4 });         */

