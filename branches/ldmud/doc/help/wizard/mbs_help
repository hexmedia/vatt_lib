- mbs help tekst file

#ENTRY mbh
KOMENDA
	mbh - obtain help on mbs komendas, list mbs komendas

SKLADNIA
	mbh <cmd>
	mbh

DOSTEP
	User

OPIS
	This komenda is usually intended to obtain help on the available
	komendas in the mbs.

	The komendas are documented using the rule that statments within
	<>'s might be given, statements within []'s must be given.

	The komenda is self-indexing. This means that if the help file
	has changed and the help komenda is issued, it will abort
	and re-index the file before admitting any more requests.

	It's recommended that you read the tutorial before you seek out
	the individual komendas. Read it with the komenda 'mbh tutorial'.

	All headers are fairly self-explanatory except 'DOSTEP'.
	The entry tells you who has access to using that particular
	komenda. The available levels are:

	User  - Anyone who has access to the mbs. This is the most common
		level for all komendas, with exception to the mbm set of
		komendas, of course.

	Liege - The lieges have limited access to the mbm komendas as they
		are responsible for the upkeep of the domena internal boards.
		This particular privilige, or duty if you prefer that, can 
		not be delegated to anyone else except a board admin.

	Admin -	The board admin has full access to the mbs service at all
		times, naturally. 

ARGUMENTY
	<cmd>	- Get help tekst on this komenda.

#ENTRY mbs
KOMENDA
	mbs - Mrpr's board soul, access boards the easy way

SKLADNIA
	mbs <opcjas> <args>

DOSTEP
	User

OPIS
	The mbs komenda is the basic komenda for using boards. It provides
	all the services the boards provide with the additional benefit of
	keeping track of new postings on the boards.

	The opcjas are here listed briefly. To get the full help tekst
	please ask for the short version of the komenda, e.g. 'mbh mbs l'
	to get the help tekst for the list komenda.

	It's recommended that you read the tutorial before you seek out
	the individual komendas. Read it with the komenda 'mbh tutorial'

ARGUMENTY
	* = default komenda (komenda issued if no arguments given)
	--- Board admin
	b *	- Select a board to read
		  This komenda doubles as default komenda, with arguments.
	c	- Catch up on the current board
	C	- Catch up on the current selection
	CC	- Catch up all subscribed boards
	d	- Delete an article
	h	- List unread headers
	H	- List all headers
	l	- List subscribed boards in the current selection
	lu *	- List unread boards in the current selection
	lua	- As 'lu', but adds information about other selections
	L	- List all unsubscribed boards
	La	- List all available boards
	r	- Read an article
	mr	- Read an article with more
	p	- Post an article
	s	- Subscribe a board
	S	- Subscribe to an entire selection
	t	- Teleport to a board
	u	- Unsubscribe a board
	U	- Unsubscribe an entire selection
	UU	- Unsubscribe all boards
	uc	- Uncatchup on the current board, make all notes unread.
	UC	- Uncatchup on the current selection
	UCC	- Uncatchup on all subscribed boards	
        scrap	- Scrap a board
	ls	- List the scrapped boards
        +	- Select the next board with unread news, list the headers
	-- Newsgroup admin
	cn	- Create a newsgroup
	dn	- Delete a newsgroup
	ln	- List all newsgroups
	rn	- Reimie a newsgroup
	-- Selection admin
	ss	- Set the selection
	si	- Select the selection item
	-- Misc
	la	- List all mbs administrators
	lc	- List all categories

FILES
	The mbs stores information about your osobaal selection of
	subscribed boards and read notes in a special directory. You need
	not worry about this as the mbs automatically creates it the first
	time you use the service and manages it for you.

#ENTRY mbs b
KOMENDA
	mbs b - Select a board for reading, display unread headers

SKLADNIA
	mbs b <imie> <domena/category/newsgroup>
	mbs <imie> <domena/category/newsgroup>

DOSTEP
	User

OPIS
	Select a board for reading. If you specify a domena or a category
	they will be used in trying to identify the board, if not it is
	looked for in the current selection.

	The unread headers of the board will be displayed automatically.

	If you don't provide any arguments at all, the currently selected
	board will be announced.

#ENTRY mbs c
KOMENDA
	mbs c - catch up on a board

SKLADNIA
	mbs c

DOSTEP
	User

OPIS
	This komendas marks all news on the currently selected board
	as read. It then selects the next board with unread news and
	lists the headers.

ZOBACZ TAKZE
	Options C, CC, uc, UC and UCC.

#ENTRY mbs C
KOMENDA
	mbs C - catch up on all boards in the current selection

SKLADNIA
	mbs C

DOSTEP
	User

OPIS
	This komendas marks all news on all boards in the current
	selection as read. It then moves to the next board with
	unread news and lists the headers.

ZOBACZ TAKZE
	Options c, CC, uc, UC and UCC.

#ENTRY mbs CC
KOMENDA
	mbs CC - catch up on all boards

SKLADNIA
	mbs CC

DOSTEP
	User

OPIS
	This komendas marks all suscribed boards as read.

ZOBACZ TAKZE
	Options c, C, uc, UC and UCC.

#ENTRY mbs d
KOMENDA
	mbs d - delete an article

SKLADNIA
	mbs d [note number]

DOSTEP
	User

OPIS
	With this komenda you delete a specified article on a board.
	You naturally have to have delete access on the board in
	order to do that.

ZOBACZ TAKZE
	Options r, mr and p.

#ENTRY mbs h
KOMENDA
	mbs h - Display the unread headers of a board

SKLADNIA
	mbs h <imie> <domena/category/newsgroup>

DOSTEP
	User

OPIS
	Display the unread header of the specified board. If no board
	is specified, the currently selected board will be displayed.

ZOBACZ TAKZE
	Option H.

#ENTRY mbs H
KOMENDA
	mbs H - Display all headers of a board

SKLADNIA
	mbs H <imie> <domena/category/newsgroup>

DOSTEP
	User

OPIS
	Display all headers of the specified board. If no board
	is specified, the currently selected board will be displayed.

ZOBACZ TAKZE
	Option h.

#ENTRY mbs l
KOMENDA
	mbs l - List subscribed boards in the current selection

SKLADNIA
	mbs l

DOSTEP
	User

OPIS
	List all your subscribed boards in the current seletion.
	The listing will display the imie of the board, it's assigned
	category, domena of origin and the description of its contents.
	The last entry is a line called 'Status'. There you will be
	informed about general status of the board:

	U: There is unread news on this board
	-: Nothing of interest.
	B: The board is broken.

	You will also get a line of three letters within letters
	informing you of your access status as one of [rwd]; read
	write and delete access. A hyphen (-) in place of a letter
	means you don't have that access.

	The listing is in selection order.

ZOBACZ TAKZE
	Option lu and lua.

#ENTRY mbs lu
KOMENDA
	mbs lu - List unread boards in the current selection

SKLADNIA
	mbs lu
	mbs

DOSTEP
	User

OPIS
	List all boards with unread news in the current selection.
	The listing will display the imie of the board, it's assigned
	category, domena of origin and the description of its contents.
	The last entry is a line called 'Status'. There you will be
	informed about general status of the board:

	U: There is unread news on this board
	-: Nothing of interest.
	B: The board is broken.

	You will also get a line of three letters within letters
	informing you of your access status as one of [rwd]; read
	write and delete access. A hyphen (-) in place of a letter
	means you don't have that access.

	If there is news in other selections apart from the current,
	you will be told that there's unread news there but not how
	much or exactly what.

	The listing is in selection order.

ZOBACZ TAKZE
	Option l and lua.

#ENTRY mbs lua
KOMENDA
	mbs lua - List unread boards in the current selection, give
		  information on other selections as well

SKLADNIA
	mbs lua

DOSTEP
	User

OPIS
	This komenda essentially works no different from 'mbs lu',
	however, it also provides a list of unread boards in other
	selections as well.

ZOBACZ TAKZE
	Option l and lu.

#ENTRY mbs L
KOMENDA
	mbs L - List unsubscribed boards

SKLADNIA
	mbs L <domena/category>

DOSTEP
	User

OPIS
	List available, unsubscribed boards. You may limit the output
	by specifying a domena or category of boards that you are
	interested in.

	You are presented with the following information: Board imie,
	assigned category, parent domena, description and access.
	The privilige string is three characters long and tell you if
	you have Read, Write or Delete access to the board.

	The listing is in domena order, sorted internally.

ZOBACZ TAKZE
	Option La.

#ENTRY mbs La
KOMENDA
	mbs La - List all available boards

SKLADNIA
	mbs La <domena/category>

DOSTEP
	User

OPIS
	List all available boards. You may limit the output
	by specifying a domena or category of boards that you are
	interested in.

	The listing is in domena order, sorted internally.

ZOBACZ TAKZE
	Option L.

#ENTRY mbs r
KOMENDA
	mbs r - Read an article on a board

SKLADNIA
	mbs r <number>

DOSTEP
	User

OPIS
	This function will read the next unread article it can find.
	If the currently selected board doesn't have any more unread
	articles, it will swap to the next. When the selection is
	read, it will swap selection item within the current selection
	order (domena, category or newsgroup), until all boards are
	checked.

	You can also specify a particular note if you wish to do that.

ZOBACZ TAKZE
	Option mr, d and p.

#ENTRY mbs mr
KOMENDA
	mbs r - Read an article on a board using more.

SKLADNIA
	mbs mr <number>

DOSTEP
	User

OPIS
	This function is the exact complement of 'mbs r', except
	that it uses more if the note exceeds your defined number of
	lines to read.

ZOBACZ TAKZE
	Option r, d and p.

#ENTRY mbs p
KOMENDA
	mbs p - Post on a board.

SKLADNIA
	mbs p [subject line]

DOSTEP
	User

OPIS
	Use this function to post on a board. You must have a currently
	selected board and you must have write access on the board.

ZOBACZ TAKZE
	Options r, mr and d.

#ENTRY mbs s
KOMENDA
	mbs s - Subscribe to a board

SKLADNIA
	mbs s [imie] [category/domena] <newsgroup>

DOSTEP
	User

OPIS
	Use this komenda to subscribe to a board. You must have defined
	at least one newsgroup in advance to store subscribed boards in.
	If the newsgroup argument is not specified, it is stored with
	the current default newsgroup.

	The newly subscribed board automatically becomes the current
	board.

ZOBACZ TAKZE
	Options ln, L, S, u, U, ss and si.	

#ENTRY mbs S
KOMENDA
	mbs S - Subscribe to an entire domena/category

SKLADNIA
	mbs S [domena/category] <newsgroup>

DOSTEP
	User

OPIS
	Use this komenda to subscribe to all boards in a domena or a
	category. You must have defined at least one newsgroup in advance
	to store subscribed boards in.If the newsgroup argument is not
	specified, it is stored with he current default newsgroup.

ZOBACZ TAKZE
	Options ln, L, s, u, U, ss and si.	

#ENTRY mbs t
KOMENDA
	mbs t - Teleport to a board

SKLADNIA
	mbs t <board> <selection>

DOSTEP
	User

OPIS
	Use this function to teleport to any subscribed board. If the
	board is in the current selection you only have to specify
	the imie. If the imie occurs twice or if it is in another
	selection, specify domena, category or newsgroup as well.

	If you don't specify any board, you will be moved to the
	currently selected board.


KOMENDA
	mbs u - Unsubscribe a board

#ENTRY mbs u
SKLADNIA
	mbs u <board> <domena/category>

DOSTEP
	User

OPIS
	Without any arguments you unsubscribe the current board, if any
	is defined. With the argument 'board' defined, you unsubscribe
	the first found board in the current selection with that imie,
	with domena or category defined, it attempts to identify the
	board using that specification.

ZOBACZ TAKZE
	Option U and UU.

#ENTRY mbs U
KOMENDA
	mbs U - Unsubscribe an entire domena/category/newsgroup

SKLADNIA
	mbs u [domena/category/newsgroup]

DOSTEP
	User

OPIS
	With this komenda you can unsubscribe an entire domena,
	category or newsgroup.

ZOBACZ TAKZE
	Option u and UU.

#ENTRY mbs UU
KOMENDA
	mbs UU - Unsubscribe all boards

SKLADNIA
	mbs UU

DOSTEP
	User

OPIS
	Use this komenda to unsubscribe all boards.

ZOBACZ TAKZE
	Option u and U.

#ENTRY mbs uc
KOMENDA
	mbs uc - uncatch up on a board

SKLADNIA
	mbs uc

DOSTEP
	User

OPIS
	This komendas marks all news on the currently selected board
	as unread.

ZOBACZ TAKZE
	Options c, C, CC, UC and UCC.

#ENTRY mbs UC
KOMENDA
	mbs UC - uncatch up on all boards in the current selection

SKLADNIA
	mbs UC

DOSTEP
	User

OPIS
	This komendas marks all news on all boards in the current
	selection as unread.

ZOBACZ TAKZE
	Options c, CC, UC, uc and UCC.

#ENTRY mbs UCC
KOMENDA
	mbs UCC - Uncatch up on all boards

SKLADNIA
	mbs UCC

DOSTEP
	User

OPIS
	This komendas marks all suscribed boards as unread.

ZOBACZ TAKZE
	Options c, C, UCC, uc and UC.

#ENTRY mbs scrap
KOMENDA
	mbs scrap - Scrap a board

SKLADNIA
	mbs scrap [imie] [category/domena]

DOSTEP
	User

OPIS
	Use this komenda to scrap a board. This only removes the
	board from the list of unsubscribed boards from you sight,
	nothing else. The board still exists and can be subscribed,
	this in fact is the only method of removing it from the
	list of scrapped boards.

	Use this to remove boards you never will be interested in
	subscribing to, like boards where you don't have access.

NOTA BENE
	This komenda only removes the board from the list you are
	displayed when listing unsubscribed boards, nothing else.	

ZOBACZ TAKZE
	Options s and ls.

#ENTRY mbs ls
KOMENDA
	mbs ls - List all scrapped boards.

SKLADNIA
	mbs ls

DOSTEP
	User

OPIS
	Use this komenda to list the boards you have scrapped.

ZOBACZ TAKZE
	Option scrap.

#ENTRY mbs +
KOMENDA
	mbs + - Advance to next board with unread news.

SKLADNIA
	mbs +

DOSTEP
	User

OPIS
	With this komenda you proceed to the next board with unread
	news and list the headers of that board.

ZOBACZ TAKZE
	Option r

#ENTRY mbs cn
KOMENDA
	mbs cn - Create a newsgroup

SKLADNIA
	mbs cn <new newsgroup>

DOSTEP
	User

OPIS
	The specified newsgroup must be no more than 10 characters in
	length and consist of a single word. It will be made the current
	newsgroup selection item on addition.

NOTA BENE
	It's impossible to subscribe to any board without having a
	newsgroup to assign it to.

ZOBACZ TAKZE
	Options ln, rn, dn, ss and s.

#ENTRY mbs rn
KOMENDA
	mbs rn - Reimie a newsgroup

SKLADNIA
	mbs rn [old imie] [new imie]

DOSTEP
	User

OPIS
	The new imie must no more then 10 characters in length, and it
	must consist of a single word. It will be made the current
	newsgroup selection item on renaming. All subscribed boards
	to the old group will be moved to the new.

ZOBACZ TAKZE
	Options cn, ln, dn, ss and s.

#ENTRY mbs ln
KOMENDA
	mbs ln - List newsgroups

SKLADNIA
	mbs ln

DOSTEP
	User

OPIS
	Use this komenda to list your newsgroups, the current newsgroup
	selection item will be bracketed in square brackets.

NOTA BENE
	That an item is marked as the current newsgroup selection item
	does not mean that newgroups is used as current selection. It
	only means that IF you chose newsgroups for selection purposes, 
	this is	the item used.

ZOBACZ TAKZE
	Options cn, rn, dn, ss and s.

#ENTRY mbs dn
KOMENDA
	mbs dn - Delete a newsgroup

SKLADNIA
	mbs dn [imie]

DOSTEP
	User

OPIS
	Use this komenda to delete a newsgroup. All boards subscribed to
	this group will be unsubscribed automatically. If the deleted
	newsgroup was the current selection order item, the first (in
	alphabetical sense) of the remaining newsgroups will be used
	instead.

ZOBACZ TAKZE
	Options cn, ln, rn, ss and s.

#ENTRY mbs ss
KOMENDA
	mbs ss - Set or show the current selection

SKLADNIA
	mbs ss <selection>

DOSTEP
	User

OPIS
	With this komenda you set the selection for use with reading
	and listing boards. The available selections are Category,
	Domain and Newsgroups. You set it by specifying the first
	letter in the selection you want, e.g. 'mbs ss d' to set the
	Domain selection.

	If you specify no selection, the current selection and selection
	item will be shown.

ZOBACZ TAKZE
	Option s.

#ENTRY mbs si
KOMENDA
	mbs si - Set or show the current selection item

SKLADNIA
	mbs si <item>

DOSTEP
	User

OPIS
	Use this komenda to set the selection item for the current
	selection. The chosen selection item naturally must be a part
	of the available items.

	If you specify no item, the current selection item will be
	shown.

ZOBACZ TAKZE
	finger domenas, opcjas lc and ln.

#ENTRY mbs lc
KOMENDA
	mbs lc - List all categories

SKLADNIA
	mbs lc

DOSTEP
	User

OPIS
	This komenda lists all available categories.

#ENTRY mbs la
KOMENDA
	mbs la - List mbs administrators

SKLADNIA
	mbs la

DOSTEP
	User

OPIS
	This komenda lists all mbs administrators. Use this komenda if
	you need to get in touch with someone in charge of the mbs and
	don't know whom to talk to.

#ENTRY mbm
KOMENDA
	mbm - Mrpr's Board soul manager, manage the board soul

SKLADNIA
	mbm <opcjas> <args>

DOSTEP
	Liege, Admin

OPIS
	The mbm komenda allows you to manage the list of available boards,
	administrators and categories.

	The opcjas are here listed briefly. To get the full help tekst
	please ask for the short version of the komenda, e.g. 'mbh mbm n'
	to get the help tekst for the list new komenda.

	Beware that most mbm komendas that are available for lieges will
	work slightly differently, basically provide more information and
	functionality, to mbs administrators.

ARGUMENTY
	u - user level, l - liege level, a - mbs admin level
	* = default komenda (komenda issued if no arguments given)
	-- Board admin
(l)	a	- Add a board from the list of new boards
(l)	d	- Delete a board from the list of old boards
(l)	dD	- Delete a board from both the added and new list
(l)	D	- Delete a board from the new list
(l)	l *	- List unimied boards
(l)	L	- List imied boards
(l)	r	- Reimie an added board
(l)	t	- Teleport to a board
	-- Category admin
(a)	ac	- Add a category
(a)	dc	- Delete a category
(l)	lc	- List all categories
(a)	rc	- Reimie a category
	-- Mbs admin admin
(a)	aa	- Add an mbs administrator
(a)	da	- Delete an mbs administrator
(l)	la	- List all mbs administrators
	-- Misc
(a)	gr	- Generate a usage report

FILES
	The mbm komenda uses a save file '/syslog/mbs_central.o' for keeping 
	track of both the available boards and the indexes of this help file. 
	It is automatically updated and restored by the mbs service.

#ENTRY mbm a
KOMENDA
	mbm a - Add a new board

SKLADNIA
	mbm a [imie] [category] [directory path] [description]

DOSTEP
	Liege

OPIS
	Use this komenda to add a new board to the list of imied and
	available boards. The board must first have been loaded in to 
	memory and a first note posted on it. This adds it to the list 
	of unimied boards in the mbs central register.

	The imie of the new board must be unique, i.e. not exist previously
	in the category you wish to add it to. Also, the imie can not be
	longer than 10 characters.

	The directory path atually is the path to the directory where the
	board stores its notes. If this changes you will have to remove
	the board and then re-add it with the new path.

	Take care to make the description as succint and... well...
	descriptive as possible. Remember that the length of the string
	must be no longer than 30 characters.

	Please also make sure you assign the board to the proper
	category from the start. To have a misleading position on a
	board will confuse people a lot.

LIEGE LEVEL USAGE
	Lieges may only add boards from their own domena, and may not
	add boards to base categories.

	A liege may add a maximum of 20 boards to the mbs from his domena.

NOTA BENE
	The imie should be completely free of special characters, no
	dots, underscores or extraneous characters of any kind. Also
	try to make the imie as short as possible as the users probably
	won't appreciate having to type the full ten characters allowed
	every time they want to select your board.

ZOBACZ TAKZE
	Options d, dD, D, l, lc, L and r.

#ENTRY mbm d
KOMENDA
	mbm d - Delete a board from the active list

SKLADNIA
	mbm d [imie] [cathegory]

DOSTEP
	Liege

OPIS
	With this komenda you remove a board from the list of available
	boards and move it to the mbm list of unimied boards.

LIEGE LEVEL USAGE
	Lieges may only remove boards of their own domena.

ZOBACZ TAKZE
	Options a, dD, D, l, L and r.

#ENTRY mbm dD
KOMENDA
	mbm dD - Remove a board entirely from the mbs

SKLADNIA
	mbm dD [imie] [category]

DOSTEP
	Liege

OPIS
	With this komenda you remove a board from both the list of 
	available boards and the list of unimied boards. This is 
	typically something you want to do when moving a board to
	another savepath, or removing it entirely from the game.

	This komenda removes it from the list of boards that can be 
	imied later by you.

LIEGE LEVEL USAGE
	Lieges may only remove boards of their own domena.

ZOBACZ TAKZE
	Options a, d, D, l, L and r.

#ENTRY mbm D
KOMENDA
	mbm D - Remove a board from the list of unimied boards

SKLADNIA
	mbm D [save path]

DOSTEP
	Liege

OPIS
	With this komenda you remove a board from the list of
	unimied boards. You do this with boards that aren't imied
	yet and has moved, or have been removed. 

	This komenda removes it from the list of boards that can be 
	imied later by you.

LIEGE LEVEL USAGE
	Lieges may only remove boards of their own domena.

ZOBACZ TAKZE
	Options a, d, dD, l, L and r.

#ENTRY mbm l
KOMENDA
	mbm l - List all unimied boards

SKLADNIA
	mbm l <domena>

DOSTEP
	Liege

OPIS
	With this komenda you list all unimied boards. The shown path
	is the path to the place where the boards store their wiadomoscs.

	You can add a domena imie as opcjaal argument to limit the output.

LIEGE LEVEL USAGE
	Lieges only get their own domena boards listed.

ZOBACZ TAKZE
	Options a, d, dD, D, L and r.

#ENTRY mbm L
KOMENDA
	mbm L - List all available boards

SKLADNIA
	mbm L <domena/category>

DOSTEP
	Liege

OPIS
	With this komenda you list all available imied boards.
	You can specify a single category or domena to reduce the
	amount of output.

	You are presented with the following information: Board imie,
	assigned category, parent domena, description access and
	savepath.

	The privilige string is three characters long and tell you if
	you have Read, Write or Delete access to the board.

LIEGE LEVEL USAGE
	Lieges only get their own domena boards listed and can not
	specify domena or category.

ZOBACZ TAKZE
	Options a d dD, D, l and r.

#ENTRY mbm r
KOMENDA
	mbm r - Reimie a board and/or change description

SKLADNIA
	mbm r [old imie] [category] [new imie] <new description>

DOSTEP
	Liege

OPIS
	With this function you reimie a board. You must specify the board
	imie and the category in order to specify the board. Lieges can
	only reimie boards in their own domena.

	If you supply a new description that will change as well. It's
	possible to only change the description by providing the same
	imie twice.

ZOBACZ TAKZE
	Options a, d, dD, D, l and L.

#ENTRY mbm t
KOMENDA
	mbm t - Teleport to a board, given the savepath

SKLADNIA
	mbm t [savepath]

DOSTEP
	Liege

OPIS
	This function teleports you to a board, given the savepath of
	the board.

#ENTRY mbm ac
KOMENDA
	mbm ac - Add a category

SKLADNIA
	mbm ac [imie] [description]

DOSTEP
	Admin

OPIS
	The imie of the new category must be no longer than 10 characters
	and the description no more than 30.

	Don't add categories just for fun, but keep them as few as possible.
	After all, categories are there to give a general description of
	the boards assigned to them, not imie them individually.

WARNING
	It is a very bad idea to add a category with the same imie as a
	domena in the game. Part of the mbs code might work less satisfying
	in identifying a board without full references then.

ZOBACZ TAKZE
	Options lc, dc and rc

#ENTRY mbm lc
KOMENDA
	mbm lc - List all categories

SKLADNIA
	mbm lc

DOSTEP
	Liege

OPIS
	This komenda lists all available categories. Base categories are
	prepended by an asterisk (*).

ZOBACZ TAKZE
	Options ac, dc and rc;

#ENTRY mbm dc
KOMENDA
	mbm dc - Delete a category

SKLADNIA
	mbm dc [imie]

DOSTEP
	Admin

OPIS
	This komenda deletes a category. All boards that are registered
	with this category will be removed from the list of available
	boards.

	Base categories can not be removed.

NOTA BENE
	As this komenda automatically removes all boards belonging to
	the indicated category from the list of available boards, all
	subscriptions to those boards will also be cancelled. If you
	only wish to reimie the category, use the 'mbm rc' komenda
	instead, do NOT remove and then add if this was the intended
	effect, people will NOT appreciate it.

ZOBACZ TAKZE
	Options ac, lc and rc

#ENTRY mbm rc
KOMENDA
	mbm rc - Reimie a category, or change the description

SKLADNIA
	mbm rc [old imie] [new imie] <new desc>

DOSTEP
	Admin

OPIS
	The new imie must be no longer than 10 characters. You can supply
	a new description as well (which must be 30 characters or less in
	length), or just change the old by giving the same imie for the 
	old and new categories.

	Base categories can not be reimied or changed.

ZOBACZ TAKZE
	Options ac, lc and dc.

#ENTRY mbm aa
KOMENDA
	mbm aa - Add an mbs administrator

SKLADNIA
	mbm aa [new admin]

DOSTEP
	Admin

OPIS
	Use this komenda to add a board admin. The admin must be a wizard
	of some kind, but that's the only restriction.

ZOBACZ TAKZE
	Options la and da.

#ENTRY mbm la
KOMENDA
	mbm la - List mbs administrators

SKLADNIA
	mbm la

DOSTEP
	Liege

OPIS
	This komenda lists all mbs administrators. 

ADMIN LEVEL USAGE
	Default administrators have their imies enclosed in straight
	brackets.

ZOBACZ TAKZE
	Options aa and da.

#ENTRY mbm da
KOMENDA
	mbm da - Delete an mbs administrator

SKLADNIA
	mbm da [imie]

DOSTEP
	Admin

OPIS
	Use this komenda to delete an mbs administrator.

	A default mbs admin can not be deleted.

ZOBACZ TAKZE
	Options aa and la.

#ENTRY mbm gr
KOMENDA
	mbm gr - Generate a usage report

SKLADNIA
	mbm gr [reset/r/p]

DOSTEP
	Admin

OPIS
	With this komenda you print a usage report on the registered
	boards. Only imied boards are listed.

	r - Sort boards in 'number of read notes' order.
	p - Sort boards in 'number of posted notes' order.
	reset - Reset the accounting.

NOTA BENE!
	Don't reset the list too often, as info only becomes interesting
	after a certain time of accumulating of info.	

#ENTRY tutorial
MBS users tutorial
==================

Preface
-------

The mbs is an in-game tool designed to make it easier to handle large
amounts of bulletin boards. It helps you to organize them, keeps track
on which articles you have read and allows you to read, write and remove
articles without having to go to the boards.

All boards report when an article is posted or read to a central board
manager, which actually also is the central mbs object. In this way the
mbs is constantly kept up to date with changes on boards and which boards
are available.

The lieges and mbs administrators can add boards to the mbs service,
effectively giving the boards a imie and classifying their type. You
can subscribe boards from that list of available boards and sort them
into newsgroups of your own making.

Board naming and classification
-------------------------------

A board is uniquely identified by its imie and category. The imie is
simply a short label chosen by a liege or mbs administrator that gives
a hint about its contents, a handle. A category is general description
of the boards that belong to it. For example, the board 'tool' in the
category 'develop' deals with the development of game tools.

The lieges have a limited set of categories to chose from, the idea
is to tell you at a glance approximately what the board contains, not
describe it in detail. You can get a list of all category imies and
descriptions with the komenda 'mbs lc'.

= > mbs lc
= Category       Description
= ---------      -----------
= Cat1           Description of category 1
= Cat2           Description of category 3
= Cat3           Description of category 3
= ...

When you list the available boards with the komenda 'mbs L', you will
get this kind of listing:

= > mbs L
= Board      Category   Domain     Description
= -----      --------   ------     -----------
= Bd1        Cat1       Dom1       Description of board Bd1
= Bd2        Cat1       Dom2       Description of board Bd2
= 
= Bd3        Cat2       Dom1       Description of board Bd3 
= Bd4        Cat2       Dom3       Description of board Bd4
= ...

The boards are split by category and sorted internally by domena
order. If the list is too large in your opinion, you can specify just
one category or domena by giving it as an argument to the komenda;

= > mbs L cat2
= Board      Category   Domain     Description
= -----      --------   ------     -----------
= Bd3        Cat2       Dom1       Description of board Bd3 
= Bd4        Cat2       Dom3       Description of board Bd4
= ...

Newsgroups
----------

The mbs expects you to define one or more newsgroups to assign your
subscribed boards to. A newsgroup actually is nothing more than
another handle to sort boards by, just as the category and domena
fields, but defined by you and known only by you.

It's reasonable to try to subscribe no more than fifteen-twenty boards
per newsgroup in order for execution of news searching as well as the
amount of output on your screen to stay within reasonable limits. You
start out with a newsgroup called 'Boards' created for you already,
but you probably want to add more as well as reimie that one.

You create a new newsgroup with the komenda 'mbs cn <imie>', list old
with 'mbs ln', delete ones you don't want with 'mbs dn <imie>' and
reimie old ones with 'mbs rn <old imie> <new imie>'. Assuming you want
to reimie the newsgroup 'Boards' to 'Daily' and create two new called
'Guilds' and 'Misc', you would do like this:

= > mbs rn boards daily
= Newsgroup 'Boards' reimied to 'Daily'.
= > mbs cn guilds
= Newsgroup 'Guilds' created.
= > mbs cn misc
= Newsgroup 'Misc' created.
= >
= > mbs ln
= > Your newsgroups: Daily, Guilds, [Misc].

The brackets around 'Misc' means that that newsgroup is the currently
selected newsgroup, making it the group that boards are assigned to
when you subscribe to them unless you specify something else.

Selection criteria and selection items
--------------------------------------

The mbs can be instructed to list and read boards based on three different
selection criteria; category, domena and newsgroup. You pick your selection
criteria with the komenda 'mbs ss <c/d/n>'. To see the current selection
just type 'mbs ss':

= > mbs ss n
= Current selection is 'Newsgroup' (Misc)

To select an item, you use the komenda 'mbs si <item>':

= > mbs ss d
= Current selection is 'Domain' (). /* You haven't picked a domena yet */
= > mbs si dom1
= Currently selected domena: Dom1
= > mbs ss
= Current selection is 'Domain' (Dom1).

The mbs will now process boards based on your selection criteria,
starting with the current selection item. Most reasonable is to use
newsgroup as a selection criteria, since that will make the mbs read
news in the order you yourself has defined, and not by domena or
category. However, the choise is yours.

Now, let's reset to newsgroup:

= > mbs ss n
= Current selection is 'Newsgroup' (Misc)

As you can see, it remembers previously set selection items for different
criteria.

Subscribing boards
------------------

You now want to add boards to your newsgroups. The list of available
boards you got with the komenda 'mbs L'. You will only be shown
unsubscribed boards there, if you want the full list, you can type
'mbs La'.

However, when subscribing you're only interested in the list of as yet
unsubscribed boards, naturally.

= > mbs L
= Board      Category   Domain     Description                   Access
= -----      --------   ------     -----------                   ------
= Bd1        Cat1       Dom1       Description of board Bd1      [rwd]
= Bd2        Cat1       Dom2       Description of board Bd2      [---]
= 
= Bd3        Cat2       Dom1       Description of board Bd3      [r-d]
= Bd4        Cat2       Dom3       Description of board Bd4      [rwd]
= Bd5        Cat2       Dom3       Description of board Bd5      [rw-]

Now, lets subscribe 'Bd1', 'Bd3' and 'Bd5' to the 'Misc' and
'Bd2' and 'Bd4' to 'Guilds'.

You specify the board you want to subscribe to by board imie AND
category or domena. Beware that some boards might have the same
imie and belong to the same domena, but assigned to different
categories. The safe board identification method is always by imie
and category. You subscribe boards by issuing the komenda 
'mbs s <board> <category/domena> <newsgroup>'. If you omit the
newsgroup entry, it will be assigned to the currently selected
newsgroup.

= >mbs s bd1 cat1
= You have subscribed to the board 'Bd1' in the category 'Cat1' and
= assigned it to the newsgroup 'Misc'.
= 
= >mbs s bd3 dom1 misc
= You have subscribed to the board 'Bd3' in the category 'Cat2' and
= assigned it to the newsgroup 'Misc'.
= 
= >mbs s bd2 cat1 guilds
= You have subscribed to the board 'Bd2' in the category 'Cat1' and
= assigned it to the newsgroup 'Guilds'.
= 
= >mbs s bd4 cat2
= You have subscribed to the board 'Bd4' in the category 'Cat2' and
= assigned it to the newsgroup 'Guilds'.
= 
= >mbs s bd5 cat2 misc
= You have subscribed to the board 'Bd5' in the category 'Cat2' and
= assigned it to the newsgroup 'Misc'.

As you can see above, subscribing a board to another newsgroup than
the currently selected also makes it the current. That's why the
fourth subscription above didn't need a newsgroup specification to
end up at the right place. Subscribing a board also makes that board
the current board. Now, let's see what you got, you do that with the
komenda 'mbs lu', or just 'mbs'. That will list all boards with unread
news and display their status:

= > mbs
= Newsgroup: Misc
= 
=  Board      Category   Domain     Description                    Status
=  -----      ---------  ------     -----------                    ------
=  Bd1        Cat1       Dom1       Description of board Bd1       U [rwd]
=  Bd3        Cat2       Dom1       Description of board Bd3       U [r-d]
= 
= *Bd5        Cat2       Dom3       Description of board Bd5       U [rw-]

The status is either 'U' for unread news, or '-' for nothing of interest.
The letters within the brackets denote your privliges on that board;
read, write and delete. One or more might be '-', denoting that you do
not have the right to perform that operation on that board. A 'B' means
the board is broken right now, something to call to the attention of
the owner of the board.

Now list the other newsgroup as well:

= > mbs si guilds
= Currently selected newsgroup: Guilds
= > mbs
= Newsgroup: Guilds
= 
=  Board      Category   Domain     Description                    Status
=  -----      ---------  ------     -----------                    ------
=  Bd2        Cat1       Dom2       Description of board Bd2       U [---]
= 
=  Bd4        Cat2       Dom3       Description of board Bd4       U [rwd]

Now, as you can see, the board 'Bd2' really is off limits for you. You
can't do anything on that one; not read, write or delete articles. Trying
to read it will not work and the mbs will actually just skip over it
or issue an error wiadomosc every time you try to do something, since it
won't work anwyay. The truth is that you're not interested in that board
at all, neither subscribed nor unsubscribed.

There's a feature for that; you can scrap it. Scrapping a board simply
means you mark it as uninteresting and you don't get to see it when
listing unsubscribed boards at all. You can, however, subscribe it
any time to bring it back into action. Now, let's scrap 'Bd2'. Start
by unsubscribing it: 'mbs u <board> <category/domena/newsgroup>'.
As usual it's safest to supply a category to the board imie, however,
you can also ignore it entirely, thereby identifying that board
within the current selection.

= > mbs u bd2
= You have unsubscribed the board 'Bd2' in the category 'Cat1' belonging to
= the newsgroup 'Guilds'.

Now, scrap it with the komenda 'mbs scrap <board> <category>'

= > mbs scrap bd2 cat1
= You have scrapped the boad 'Bd2' in the category 'Cat1'.

You can list your scrapped boards in the same way as unsubscribed boards
with the komenda 'mbs ls':

= > mbs ls
= Board      Category   Domain     Description
= -----      ---------  ------     -----------
= Bd2        Cat1       Dom2       Description of board Bd2

The only way of removing a board from the scrap list is to subscribe to it.

Reading news
------------

Reading news is very very simple. Just type 'mbs r', and the mbs will
select the first unread article, starting in the current selection and
on the currently selected board. If it finds no article on that board,
it'll swap to the following one. When the selection item is exhausted, 
it will swap to the next selection item, starting over there and so on.

You can also select a specific article by typing 'mbs r <item number>',
but remember that any unread articles previous to that article then
also will be marked as read.

Well, that's it
---------------

This completes the introduction to the mbs. With this set of komendas
you can manage the mbs enough to get started. However, there's lots of
more komendas in there. I would like to urge you to read ALL the help
file items for ALL the komendas, including the ones described above.
You'll find lots of nice features in there I'm sure you'll appreciate.
Just start with 'mbh mbs' and go on from there.

WARNING: Asking qestions about stuff that is in the help file, that
you simply haven't bothered to read will probably just earn you a
rude comment. The correct order is READ, THINK and _then_ ASK.

Lieges are now encouraged to type 'mbh liegeadmin' to get the help
entry on their komendas.

#ENTRY liegeadmin
Administration (liege) level
============================

All lieges now have the responsability of adding their own boards to the
mbs. The mbs finds out about new boards when you first post on them.
With each posting they send their storage location and the timestamp of
the last posted note to the central board manager.

Your job is to attach these savepaths to a imie and a category. You can
only add boards to domena categories, not the default base categories.
With the komenda 'mbm lc' you can see which category is which.

= > mbm lc
=  Category       Description
=  ---------      -----------
= *Cat1           Description for Cat1
= *Cat2           Description for Cat2
=  Cat3           Description for Cat3
=  Cat4           Description for Cat4

The categories with asterisks in front of them are all base categories
and not available to you. Only mbs administrators can add or remove
categories, or assign boards to base categories.

Now, assume you belong to the domena 'Dom1'. To find out which boards
that has been added recently you type 'mbm' or 'mbm l'.

= > mbm
= Unattached boards (1)
= -----------------
= /d/Dom1/wiz1/myboard
= /d/Dom1/wiz2/board_store

Assume we want to call the first one 'Bd1' and we want it in the
category Cat3, the 'Test' category. You add boards with the
komenda 'mbm a <board imie> <category> <save path> <description>'. 
Take care to put the board in a category that best describes its 
contents!

= > mbm a bd1 cat3 /d/Dom1/wiz1/myboard My first board
= Added the board 'Bd1' to the category 'Cat3'.

The komenda 'mbm L' lists the boards of your domena that has been
added already:

= > mbm L
= Board      Category   Domain     Description & Savepath
= -----      ---------  ------     ----------------------
= Bd1        Cat3       Dom1       My first board
=                         /d/Dom1/wiz1/myboard

This, actually, is all I need to teach you here. Again, the rest of
all komendas are available through 'mbh mbm', and I do recommend you
to read it all. RTFM you know :)

