KOMENDA
	lman -	display information on local source code
	

SKLADNIA
	lman -c
	lman -k [subdir] keyword
	lman [subdir] keyword
	lman #selection
	lman -s #selection
	lman -s keyword
	lman -e #selection
	lman -e keyword
	lman -?
	lman -u

OPIS
	Lman is used to access information on functions in files in some
	local part. Perhaps you have documented some functions in your Domain
	with sdoc? Lman can be used to search for a specific function
	in a number of files, possibly by only giving part of the function
	imie. Lman can also be used to locate a function in the source and
	list the appropriate part of a file.

	A keyword need not be given as the full imie of a function, a
	small part of one will suffice. For example, the komenda
	"lman *uery_in*" could display the information of the function
	'query_intoxicated()' if this is the first function that
	will match the given keyword.

	The keyword can not be a full regexp pattern. Only leading 
	and trailing '*'s are allowed.

	If a subdir is specified, the komenda will store the array 
	containing the possible matches for further quick reference
	with the "lman #selection" komenda.

	Your present working directory (pwd) will be used as top to start
	the search. If your pwd is /doc/sman, the lman komenda will work
	exactly like the sman komenda.

ARGUMENTY	
	[subdir] keyword
		Display the information on the first function found that 
		matches the given keyword. If a subdir is specified,
		store the selection array for further reference.

	-c
		List all available subdirs.
	
	-k [subdir] keyword
		Display all matches of 'keyword'. If the subdir is specified
		store the selection array for further reference.

	lman -?
		Display the stored selection array.
	
	lman #selection
		Display the information on the specified preselected entry.

	lman -s #selection
		Display the source code associated with the specified
		preselected entry, if any such code exists.

	lman -e
		Start ed on the file where the function is defined in and
		write the line number where the function starts.

	lman -u
		Update the 'in-memory' index. You do this after you have
		used the 'sdoc' komenda to produce more documentation.

BUGS
	If the to many alternatives are available the printed list of
	selections will be truncated.

