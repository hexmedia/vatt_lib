  __________________________________________________________________________
 /                            CENY                                          \
/----------------------------------------------------------------------------\
|                |               JAKOSC              |                       |
|    Przedmiot   |-----------------------------------|      KOMENTARZ        |
|                | NISKA  | PRZECIETNA | EKSKLUZYWNA |                       |
|----------------------------------------------------------------------------|
|                    JEDZENIE (cena za 1 kg/1 litr)                          |
|----------------------------------------------------------------------------|
| Szklanka wody  |        | 2 grosze   |             |	                     |
| Sok            |        | 10 groszy  |             |                       |
| Piwo           |        | 15 groszy  |             |                       |
| Wino           |        | 2 denary   |             |                       |
| W�dka          |        | 3 denary   |             |                       |
| Chleb          |        | 6 groszy   |             |                       |
| Barszcz        |        | 10 groszy  |             |                       |
| Jajecznica     |        | 8 groszy   |             |                       |
| Kurczak        |        | 70 groszy  |             |                       |
| Baranina       |        | 90 groszy  |             |                       |
|----------------------------------------------------------------------------|
|                    MATERIALY (cena za 1kg)                                 |
|----------------------------------------------------------------------------|
| Srebro         |        | 65 denarow |             |                       |
| Zloto          |        | 130 koron  |             |                       |
| Mithryl        |        | 23500 k.   |             |                       |
| Rubin          |        |            |             |                       |
| Diament        |        |            |             |                       |
| Nefryt         |        |            |             |                       |
| Agat           |        |            |             |                       |
| Turkus         |        |            |             |                       |
|                |        |            |             |                       |
| Cis            |        |            |             |                       |
| Buk            |        |            |             |                       |
| Brzoza         |        |            |             |                       |
| Dab            |        |            |             |                       |
| Swierk         |        |            |             |                       |
| Sosna          |        |            |             |                       |
| Topola         |        |            |             |                       |
| Wierzba        |        |            |             |                       |
| Wiaz           |        |            |             |                       |
| Berberys       |        |            |             |                       |
| Lipa           |        |            |             |                       |
| Klon           |        |            |             |                       |
| Jesion         |        |            |             |                       |
| Jarzebina      |        |            |             |                       |
| Mahon          |        |            |             |                       |
| Heban          |        |            |             |                       |
|                |        |            |             |                       |
| Marmur         |        |            |             |                       |
| Granit         |        |            |             |                       |
| Zelazo         |        | 45 denarow |             |                       |
| Stal           |        | 60 koron   |             |                       |
| Miedz          |        | 180 groszy |             |                       |
| Mosiadz        |        | 35 denarow |             |                       |
| Szklo          |        |            |             |                       |
| Kwarc          |        |            |             |                       |
| Wiklina        |        | 120 groszy |             |                       |
| Glina          |        | 90 groszy  |             |                       |
| Sloma          |        | 2 grosze   |             |                       |
|                |        |            |             |                       |
| Jedwab         |        |            |             |                       |
| Welna          |        |            |             |                       |
| Bawelna        |        |            |             |                       |
| Atlas          |        |            |             |                       |
| Aksamit        |        |            |             |                       |
| Welur          |        |            |             |                       |
| Tiul           |        |            |             |                       |
| Szyfon         |        |            |             |                       |
| Tk. pikowa     |        |            |             |                       |
| Juta           |        |            |             |                       |
| Len            |        |            |             |                       |
| Satyna         |        |            |             |                       |
| Krepa          |        |            |             |                       |
| Mulina         |        |            |             |                       |
| Kordonek       |        |            |             |                       |
|                |        |            |             |                       |
| Sk. bydle      |        | 95 groszy  |             |                       |
| Sk. ciele      |        | 80 groszy  |             |                       |
| Sk. krolik     |        | 10 groszy  |             |                       |
| Sk. wilk       |        | 140 groszy |             |                       |
| Sk. niedzwiedz |        | 200 groszy |             |                       |
| Sk. swinia     |        | 55 groszy  |             |                       |
|                |        |            |             |                       |
|----------------------------------------------------------------------------|
 \--------------------------------------------------------------------------/


				CENNIK

Jest to cennik, kt�ry ma za zadanie przybli�y� nam, czarodziejom, ceny
obowi�zuj�ce w �wiecie Vatt'gherna. Skonstruowa�am go sugeruj�c si�
przede wszystkim informacjami zawartymi w Sadze, sugestiami Moldera
oraz swoimi w�asnymi. Wszelkie uwagi prosz� mi zg�asza� w trybie
natychmiastowym.

Dla ma�ego przypomnienia: 1 korona = 12 denar�w = 240 groszy.
							Lil.

_________________________________________________________
przedmiot:                     |grosze | denary | korony|
-------------------------------|-------|--------|-------|
Piwo			       |  5    |        |       |
Chleb			       |  4    |        |	|
Ko�ciany grzebie�              |  13   |        |       |
P��cienna koszula 	       |  15   |	|	|
Para but�w 		       |       |   3    |	|
Para p��ciennych spodni        |       |   2    |	|
Kr�tka sukienka		       |       |   3    |       |
Kurtka 			       |       |   5    |	|
Srebrny zwyk�y naszyjnik       |       |   8    |       |
�wiekowana kurtka 	       |       |   9    |	|
P�aszcz,gorset,plecak          |       |        | 1     |

Przeszywanica                  |       |        | 5     |
Lekka kolczuga 		       |       |        | 10    |
Przyzwoita kolczuga            |       |        | 30    |
Napier�nik                     |       |        | 25    |
S�aby he�m                     |       |        | 6     |
Przyzwoity he�m                |       |        | 10    |
Tarcza drewniana z okuciami    |       |        | 5     |
                            
S�aby sztylet 		       |       |	| 3     |
Przyzwoity sztylet	       |       |	| 6     |
S�aby miecz 		       |       |	| 9     |
Przyzwoity miecz 	       |       |	| 13    |
Dobry miecz 		       |       |	| 22    |
                               |       |        |	|
Zwyk�y konik 		       |       |	| 40    |
Pi�kna klacz 		       |       |	| 80    |
                               |       |        |	|
Super unikatowy �uk Milvy      |       |	| 400   |
Zwyk�a chatka dla gracza       |       |        |ok.500 |
Du�a wie�, Ma�y zamek 	       |       |	| 21000 |
---------------------------------------------------------
