-----------------------------------------------------------------------------

        Przewodnik po broniach sredniowiecznej Europy - miecze.

Lista stanowi oficjalny wykaz wzorcowych charakterystyk mieczy dla Arkadii. 
Wartosci hit i pen zostaly zatwierdzone przez Kapitule Vatt'gherna. 
Nalezy je stosowac do czasu ukonczenia prac nad nowym systemem walki.

Uwaga:  Podane wartosci hit/pen sa charakterystyczne dla broni o w miare
        dobrym wykonaniu. Za doskonale wykonanie wspolczynniki pen/hit
        moga zostac podniesione o 5%-15%, jednak takie podniesienie jest
        _wyjatkiem_ i kazdorazowo wymaga specjalnej zgody rektoratu. Staty 
        nie moga nigdy przekraczac 40 (wyjatkiem sa magiczne bronie).

KOD reprezentuja maksymalnie 4 litery, z ktorych pierwsza oznacza sposob
trzymania broni, pozostale zas - rodzaj obrazen:

     B -- bron dwureczna                I -- bron kluta
     R -- bron tylko praworeczna        S -- bron tnaca
     L -- bron tylko leworeczna         B -- bron obuchowa
     A -- obojetnie

 TYP BRONI      KOD    hit/pen    OPIS
-----------------------------------------------------------------------------

Tasak           A-S     22/25   Bron sieczna o jednosiecznej, prostej
                                glowni, krotszej niz miecz, rozszerzajacej
                                sie przy sztychu, lub glownie krzywa z
                                wydatnym piorem i scietym sztychem oraz
                                mieczowa rekojescia.

Kord            A-IS    23/21   Krotka glownia jednosieczna, oslona rekojesci,
                                nieco szersze ostrze na koncu o wcietym,
                                ostrym sztychu (cos jak noz finski czy
                                bowie-traperski). Czesto uzywany jako bron
                                mysliwska. Czesto noszony przez rycerstwo, 
                                zamiast miecza (byl bowiem stosunkowo lekki).

Dlugi miecz     A-S     22/24   Wczesna wersja - tylko tnacy.

Dlugi miecz     A-IS    26/25   Standardowa bron rycerstwa - zwykly miecz.

Szeroki miecz   A-S     24/26   Miecz o szerokim ostrzu, nieco krotszy od
                                dlugiego. Bron tnaca.

Miecz mysliwski A-SI    21/24   Miecz uzywany do polowania na dziki z konia
                                w XV w. o dlugiej, bardzo waskiej i
                                obosiecznej glowni i sztychu rozszerzonym
                                w ksztalt liscia oraz typowa mieczowa oprawa
                                rekojesci.

Szamszir,       A-S     30/21   Szabla perska, tzw. lwi ogon z uwagi na
simiterra,                      ksztalt glowni najpierw prostej w linii
scimitar                        rekojesci, potem odgietej do tylu tworzac
                                ksztalt dobry do ciecia przeciaglego, 
                                dl. 75-80 cm. i czesto zdobione.

Bulat           A-S     29/21   Szabla turecka z krotka, krzywa glownia
                                o szerokim piorze.

Szabla          A-IS    32/22   Sieczna bron dluga o zakrzywionej i
                                jednosiecznej glowni (czasami obosiecznym
                                sztychu), o rekojesci otwartej (najstarsza),
                                polotwartej lub zamknietej, znana od IV w,
                                poczatkowo bron plebejskiej piechoty.

Katzbalger      A-IS    25/25   Miecz o dosc szerokiej, niezbyt dlugiej,
                                dwusiecznej glowni i rekojesci z jelcem mocno
                                esowatym, tworzacym niemal pozioma tarczke.
                                Bron z konca XV w dla Lancknechtow, nazwa
                                pochodzi od kociej skorki pokrywajacej pochwe.

Palasz          A-IS    25/25   Bron o prostej, jedno- lub obosiecznej glowni
                                i rekojesci jak u szabli.

Koncerz,        A-I     19/23   Miecz przeznaczony tylko do klucia. Uzywany
granat                          glownie przez jazde; dluga (120-150 cm), 
                                sztywna glownia, o przekroju czworo- lub
                                trojkatnym, rzadziej soczewkowym przy
                                przekroju graniastym -> granat; rekojesc
                                prosta mieczowa.

Mieczorapier    A-IS    24/24   Forma przejsciowa miedzy mieczem, a rapierem
(nazwa wsp.)                    na pocz. XVI w, ciezka mieczowa glowica i
                                zamknieta oprawa rekojesci typowa dla
                                rapierow.

Rapier          A-I     23/25   Miecz o lekkim, waskim ostrzu.

-----------------------------------------------------------------------------
MIECZE DWURECZNE:

Miecz bastard   B-IS    22/27   Miecz poltorareczny o szerokim ostrzu,
                                uzywany w zasadzie tylko jako bron tnaca.
                                Tym mianem okresla sie wszystkie przerobki
                                broni (w odroznieniu od broni firmowych).

Poltorak        B-IS    23/31   Poltorak to lzejsza wersja miecza dwurecznego,
                                nieco krotszy, a przez to szybszy w walce.

Miecz claymore  B-IS    23/31   Bron szkockich gorali z pocz. XV, XVI i XVII
(od galijskiego                 wieku, o dlugim dwusiecznym ostrzu, prostych
slowa claidheamohmor)           ramionach jelca nachylonych ku glowni.

Miecz dwureczny B-SI    20/36   Ciezki, dwureczny miecz o dwu powierzchniach
                                tnacych - waga ok. 8kg, dl. 1,6-2m, dluga
                                rekojesc i mala glowica.

Miecz flamberg  B-SI    21/36   Miecz dwureczny o glowni plomienistej z
                                falistym szlifem ostrza, o 2 haczykowatych
                                wystepach u nasady ulatwiajacych parowanie.

Espadon         B-SI    20/36   Miecz dwureczny o glowni prostej i wystepach
                                ulatwiajacych parowanie.

Miecz katowski  B-S     16/37   Ciezki miecz (ciezszy niz inne miecze
                                dwureczne), zwykle dosc duzy, z szeroka
                                glownia, zwykle ze sztychem scietym poziomo,
                                niekiedy opatrzony napisami, uzywany do
                                scinania glow.

Multon          B-SI    17/34   Wielki miecz o charakterze ceremonialnym,
                                np. miecz poswiecany.

-----------------------------------------------------------------------------
-----------------------------------------------------------------------------


      Przewodnik po broniach sredniowiecznej Europy - bronie drzewcowe.


 TYP BRONI      KOD    hit/pen    OPIS
-----------------------------------------------------------------------------

Dlugi kij       B-B     28/13   Dlugi, prosty kawal kija, czasem okuty na
                                koncu.

Halabarda,      B-IS    23/37   Mordercza bron. Ostrze topora, glowka wloczni
alabarda                        i hak z tylu. Miekki rdzen zelezca okuty
(staropol.)                     twarda stala, w celu zwiekszenia obrazen.
                                Pozniej jako bron ozdobna dla strazy.

Gizarma         B-IS    23/36   Poczatkowo pochodzila od narzedzia to
                                trzebienia krzakow i miala dlugie,
                                zakrzywione na szczycie zelezce z dlugim
                                grotem po zewnetrznej stronie lub 2-ma kolcami
                                sterczacymi na boki; potem z szerokiego pnia
                                ku gorze ostry grot, ponizej sierpowaty hak i
                                po drugiej stronie kolec skierowany w bok,
                                a przy nasadzie jeszcze 2 kolce.

Partyzana       B-I     24/36   Szerokie ostrze jak we wloczni z dwoma
                                dodatkowymi ostrzami wystajacymi z bazy
                                glownego ostrza na boki. Poczatkowo ostrze w
                                ksztalcie trojkata, noszona jako oznaka rangi
                                oficerskiej, czesto zdobiona.

Szponton        B-IS    21/30   Bron podobna do partyzany; szerokie, zwykle
                                trojlistne zelezce, czasem z poprzeczka pod
                                grotem. Takze jako oznaka godnosci
                                oficerskiej w XVIII i XIX wieku, wiec czesto
                                zdobiony herbami, nr. regimentu etc.

Glewia          B-IS    24/35   Zelezce przypominalo w poczatkowych
(w Japoni naginata)             okresach noz, a pozniej wyewoluowalo na
                                bardziej oryginalny ksztalt. Przy sztychu
                                czasem odgiety do tylu. Dlugie drzewce.

Kosa bojowa     B-S     23/36   Ostrze o jednej powierzchni tnacej na dlugim
                                kiju, zelezce z zwyklej kosy przekute na
                                sztorc.

Sierp bojowy    B-IS    20/30   Bron o zelezcu z sierpa rolniczego lub w tym
                                samym ksztalcie wykonanym specjalnie dla
                                niej, przytwierdzone do dlugiego drzewca.
                                Bron chlopska (np. wojny husyckie).

Corseques       B-I     21/29   Podobny do partyzany, z tym, ze boczne 
korseka                         ostrza zostaly zakrzywione do przodu. Zelezce
                                trojdzielne, dlugi iglasty grot posrodku i dwa
                                boczne odgalezienia odgiete na boki

Runka           B-IS    21/31   Bron spokrewniona ze spisa i korseka, zelezce
                                trojdzielne zblizone ksztaltem do trojzeba, z
                                dlugim grotem posrodku i dwoma
zaokraglonymi
                                kolcami bocznymi skierowanymi ku gorze.

Widly bojowe    B-I     20/33   Widly bojowe zazwyczaj maja dodatkowe
                                haki ulatwiajace parowanie i pomagajace
                                wytracac bron. Czasami takze byly elementem
                                broni kombinowanej w polaczeniu z halabarda.

Cep bojowy      B-B     18/28   Dwureczny bojowy cep, bijak cepa czesto
                                nabijany ostrymi kolcami, takze drzewce ma
                                uchwyt do mocowania bijaka na czas drogi,
                                zeby nie kaleczyl.

Szydlo          B-I     17/25   Bron z grotem iglastym, czworograniastym i
                                bardzo dlugim (okolo 1 m), zaopatrzonym u
                                dolu w niewielka tarczke, do walki pieszo
                                (XV w).

Wlocznia,       B-I     18/25   Np. saska wlocznia o dlugim zelezcu byla
sulica                          stosowana przez jazde lub przeciw
niej.

Pika            B-I     17/26   Niewielki, stalowy grot umocowany stalowymi
                                wasami osadzony na dlugim drzewcu, nieraz
                                ponad 5 m. Grot 10 cm, czesto lisciasty lub
                                wieloboczny, srodek ciezkosci okolo 130 cm
                                od tylca i tam owijana rzemieniami dla 
                                lepszego chwytu.

Spisa           B-I     17/27   Bron o zelezcu dlugim, waskim, dluzszym od
                                piki, w zasadzie bedacym grotem. Drzewce
                                okolo 2-3,5 m, uzywane przez pieszych i
                                jazde.

Spisa           B-I     21/28   Bron o zelezcu trojdzielnym; dlugi iglasty
friulska,                       grot i 2 wygiete kolce wachlarzowato
spetum                          rozlozone na boki.
(nazwa od czesci
Wloch - 
Friuli)
-----------------------------------------------------------------------------
-----------------------------------------------------------------------------


        Przewodnik po broniach sredniowiecznej Europy - topory.


 TYP BRONI      KOD    hit/pen    OPIS
-------------------------------------------------------------------------

Reczny toporek  A-S     23/17   Zwykle narzedzie.

Ciupaga         A-SIB   24/20   Czekan goralski sluzacy jako bron lub
jako
                                laska.

Czekan,         A-SBI   20/21   Zelezce o wygladzie s

iekiery -
zaokraglone 
czokan,                         ostrze, mniejsze od tego z siekiery,
czakan                          dekorowane. Po drugiej stronie mlotek.
                                Drzewce 80-100 cm o koncu czesto okutym i
                                zakonczonym kolcem.

Siekiera        A-S     20/22   Wczesny topor, pierwotnie uzywany jako
                                narzedzie.

Toporek         A-IS    24/21   Niewielkie, regularne ostrze, opatrzone
rycerski                        obuchem z drugiej strony.

Topor bojowy    A-S     25/26   Topor bardzo dobrze zbalansowany, zazwyczaj
                                poprzez ciezarek przy stylisku.

Dwureczny       B-S     20/35   Ciezki topor na dlugim stylisku.
berdysz

Szeroki topor   B-S     21/35   Ciezki, dwureczny topor uzywany przez
                                Normanow.

Obosieczny      B-S     20/36   Ciezki dwureczny topor, o polksiezycowych
dwureczny topor                 ostrzach, uwielbiany przez krasnoludy.

-----------------------------------------------------------------------------
-----------------------------------------------------------------------------


      Przewodnik po broniach sredniowiecznej Europy - maczugi.


 TYP BRONI      KOD    hit/pen    OPIS
-----------------------------------------------------------------------------

Nadziak         A-IB    22/25   Na krotkim trzonie osadzone zelezce z ostrym
                B-IB    20/31   graniastym dziobem, lekko zagietym ku
dolowi,
                                po drugiej stronie mlotek, czesto w formie
                                glowicy buzdygana; czasem jako oznaka rangi
                                oficerskiej.

Mlot kowalski   A-B     19/24   Mlot wygladajacy jak te dzisiejsze, czesto
                B-B     17/30   jednak zakonczony z obu stron tak samo.
                                Wystepowaly w bardzo roznych wielkosciach.

Obuszek         A-IB    25/19   Bron przypomina laske lub czekanik, zelezce ma
                                silnie zagiety dziob, z drugiej strony obuch;
                                tylec trzonka okuty.

Mlot rycerski   A-IB    24/25   Mlot z jednej strony zakonczony duuzym
                                kolcem do przebijania zbroi, z drugiej
                                normalny, plaski mlotek; czasem zamiast 
                                dzioba ostra siekierka.

Mlot            B-IB    23/35   Mlot z XIV i XVI wieku; na drzewcu ok. 1m
lucernenski                     osadzone zelezce podobne do mlota, majace
/od miasta Lucerna              dziob i obuch z kilkoma kolcami,
zwienczone
nazwa/                          dlugim, spiczastym grotem.
-----------------------------------------------------------------------------
-----------------------------------------------------------------------------


        Przewodnik po broniach sredniowiecznej Europy - noze i sztylety.


 TYP BRONI      KOD    hit/pen    OPIS
-----------------------------------------------------------------------------

Noz             A-S     21/14   Bron o krotkim ostrzu i pojedynczej
                                powierzchni tnacej.

Puginal         A-IS    24/15   Noz o trojkatnym, obosiecznym ostrzu
(pugio w staroz.                rozszerzajacym sie ku rekojesci, znany
Rzymie),                        juz w epoce brazu, stosowany czesto
tulich (staropol.)              w Europie Srodkowej.

Sztylet         A-IS    28/15   Bron o krotkim ostrzu, z dwoma powierzchniami
                                tnacymi i zaostrzonym koncu.

Lewak           L-IS    25/16   Sztylet z bardzo duza oslona, uzywany
                                w lewej rece w polaczeniu z mieczem.

Igielka         A-I     15/22   Cieniutki i delikatny sztylet o waskim
                                ostrzu.

Mizerykordia    A-I     23/14   Sztylet uzywany do zadania coup-de-grace
                                unieruchomionemu przeciwnikowi.

Baselard,       A-I     27/15   Odmiana puginalu z dosc krotka, szeroka  
basilard                        i obosieczna glownia oraz rekojescia w
                                ksztalcie litery I.

Sztylet szkocki,A-IS    26/16   Sztylet, puginal, renesansowy sztylet
dirk                            szkocki, o charakterystycznej bezjelcowej
                                rekojesci, z dlugim i grubym trzonem, o
                                szerokiej, obosiecznej glowni.

Kama,           A-IS    27/16   Puginal ze Wschodu, szeroka
obosieczna,                     i prosta glownia dl. okolo 20-50 cm o
kindzal                         ostrzach przechodzacych w ostry sztych,
                                rekojesc prosta. Bron czesto zdobiona.

Daga, deka      A-I     24/19   Sztylet o dlugiej glowni 40-60 cm, szerokiej
                                u nasady i zwezajacej sie ku gorze, bron do
                                rozrywania kolczug lub pchniec w miejsca nie
                                osloniete zbroja, uznana za nierycerska.

Sztylet         A-IS    27/16   Sztylet przypominajacy nieco krotki miecz,
rondlowy                        z rekojescia chroniana okraglym
dyskiem.

Cinquenda       A-I     28/18   Dlugi sztylet do klucia, pochodzacy z
(z wloskiego)                   ok. XV wieku popularny wsrod zamoznych
                                Wlochow; nazwa od glowni szerokiej na piec
                                palcow, wyglad podobny do puginalu.

Kordelas        A-IS    29/19   Dlugi noz lub tasak, ok. 50-60 cm. Bron
                                mysliwska o glowni prostej lub lekko
                                zakrzywionej. Rekojesc czasem wykladana
                                rogiem.

-----------------------------------------------------------------------------