Kodowanie przedmiot�w to bardzo kszta�c�ce zaj�cie. :P
Z tych wszystkich trzech mo�e si� wydawa� zaj�ciem naj�atwiejszym, jednak mnie si� wydaje, �e nie do ko�ca. Fakt, kodowo (szczeg�lnie te popularne przedmioty) to zwykle jest nietrudne, jednak zazwyczaj wymaga to wi�kszego zbadania tematu (gooooogle), �eby nie by�o sytuacji, �e np. p�torak jest na naszym mudzie jednosieczny, kiedy dajmy na to w historii w og�le nigdy nie powsta�y jednosieczne p�toraki. Dlatego trzeba uwa�a� na to, co opisujemy i jak kodujemy.
Spr�buj� teraz wst�pnie przybli�y� najwazniejsze zagadnienia i, jak zwykle, r�nice zwyk�ego obiektu arkowego od naszego.


1. Nie robimy niczego w po�piechu i nie ignorujemy atrybut�w, kt�rych "i tak nie wida� na pierwszy rzut oka". Og�lna zasada jest taka, �e im wi�cej dodamy atrybut�w (prop�w) przedmiotu, czyli: im dok�adniej go sprecyzujemy, tym wi�ksza szansa, �e w przysz�o�ci nie b�dzie trzeba ka�dego obiektu kilkakrotnie przegl�da� oraz pewno��, �e ka�dy z przedmiot�w b�dzie odr�nia� si� od innych nie tylko opisem. Dlatego nie ograniczamy si� do prop�w wagi, obj�to�ci i ceny. ABSOLUTNYM minimum jest dodatkowo zdefiniowanie typu przedmiotu oraz materia��w z jakich zosta� stworzony. Czyli, pr�cz add_prop(OBJ_I_WEIGHT,VALUE i VOLUME) piszemy:
ustaw_material(MATERIAL);
opcjonalnie mozemy ustawic udzial danego materialu w procentach. Np.
ustaw_material(MATERIALY_SK_SWINIA, 90); ustaw_material(MATERIALY_SREBRO, 10);
Czyli: dany obiekt to w 90% �wi�ska sk�ra i w 10% srebro (ciekawe po��czenie;)).
Drug� wa�n� kwesti� jest ustawianie jakiego typu jest dany obiekt. W przeciwie�stwie do ustawiania materia�u - ustawienie typu obiektu daje nam ju� na chwil� obecn� znacznie wi�cej mo�liwo�ci. Ustawiamy typ poprzez u�ycie funkcji
set_type(O_TYP);

W obu przypadkach musimy zaincludowa� pewien pliczek (mo�emy go tak�e przejrze�, by pozna� dost�pne typy i materia�y) z definicjami. S� to:
#include <materialy.h>
#include <object_types.h>
Jak na zupe�nie standardowy i zwyczajny obiekt to chyba wystarczy z atrybut�w.

2. Akcje dost�pne w obiekcie. Zazwyczaj (a wi�c: nie zawsze:P), gdy mamy do czynienia z obiektami innymi od zbroi, ubra� czy broni - chcemy, by dany obiekt pos�u�y� nam do czego� specjalnego. Pos�ugujemy si� wtedy funkcj� add_action(), kt�r� piszemy w init(). Przy dodawaniu akcji musimy zwr�ci� szczeg�ln� uwag� na konstruowanie komend oraz tzw. komunikat�w b��du (notify_fail()).
Nale�y zbudowa� komend� tak, by brzmia�a ona logicznie i sensownie. To samo dotyczy notify_fail, jednak tutaj drobna uwaga: Np. przy robieniu akcji "przyczep broszk� do p�aszcza" notify_fail powinien brzmie� jak najbardziej uniwersalnie, np. jako� tak: "Przyczep co gdzie?". Nie powinien natomiast niczego sugerowa� np. "Przyczep co gdzie? Mo�e broszk� do p�aszcza?", a to dlatego, �e komenda "przyczep" mo�e by� u�yta jeszcze w niejednym innym przedmiocie.
Dobra, teraz sama funkcja jakiej� tam akcji: Mo�e najpierw napisz�, czego NIE powinno si� robi�. Nie mo�emy i�� na �atwizn� i np. w akcji "pow�chaj" zbudowa� po prostu warunek "if(str=="kwiatek") wtedy co� tam robisz". Z tego prostego powodu, �e i tym razem musimy spojrze� nieco w przysz�o��. A jak ju� spojrzymy w t� otch�a�;) i si� zastanowimy, to na pewno dojdziemy do wniosku, �e np. nie zadzia�a to na chocia�by "pow�chaj ma�y kwiatek" (przy za�o�eniu, �e przymiotnik tego obiektu to "ma�y"). W og�le musimy odrzuci� tak� budow� warunku, ze wzgl�du na zbyt du�e ograniczenia (jakkolwiek by�my nie rozbudowywali tego if'a dodaj�c, �e str mo�e si� r�wna� "ma�y kwiatek", "pierwszy kwiatek" itp, to i tak b�dzie to b��dne). Zamiast tego wszystkiego musimy wykorzysta� funkcj� parse_command (polecam man parse_command).
Wszystko to wyja�ni� na przyk�adzie ni�ej.
A'propos komend, jeszcze jedna bardzo wa�na kwestia: Komendy musz� brzmie� naturalnie i by� w miar� mo�liwo�ci intuicyjne. �adnych "poprzypnij" czy "frpopraw" - po prostu: przypnij i popraw.

3. Sublokacje. To w�a�nie w obiektach mamy znacznie wi�cej sublokacji, ni� w pomieszczeniach. Jest to do�� obszerny temat, jednak, jako, �e wszystko opiera si� na ustawianiu prop�w sublok�w + ustawianie wy�wietlania zawarto�ci sublok�w w obiekcie - Najlepiej nauczy� si� tego korzystaj�c z przyk�ad�w. Ze swojej strony mog� doda� jedynie, �e nale�y pami�ta� o ograniczaniu obj�to��i kontener�w lub ka�dego subloka osobno, je�li obiekt zawiera wi�cej ni� jedn� sublokacj� - Ale to tak�e jest w przyk�adach!
Oto pliki z przyk�adami: /doc/examples/pojemniki/mebel.c  i  /doc/examples/pojemniki/kredens.c  dodatkowo w katalogu /doc/examples/lokacje/pokoiczek_rzeczy/ znajdziemy ich jeszcze kilka.

4. Ostatnim zagadnieniem, jakie postaram si� przybli�y� to wszelkiego rodzaju pomoce dla obiekt�w. I tym razem �adnych frpomocy, popomocy, mcpomocy! Jest taka zasada, �e je�li dajemy ju� pomoc, to tylko i wy��cznie dost�pn� dla graczy pod "?nazwa obiektu". Pomoc jest mile widziana przy obiektach, kt�re posiadaj� kilka akcji, lub do takich, kt�rych akcje nie wydaj� si� by� "oczywiste" tak, jak to, �e z buk�aka mo�emy si� napi�.
Ponadto powinni�my podawa� pomoc do ka�dego obiektu, kt�ry jest OG�LNODOST�PNY.
Nie jest konieczne pisanie pomocy dla unikatowego przedmiotu, kt�rego odnalezienie b�dzie gratk� dla graczy; nie trzeba podawa� wtedy na tacy wszystkich komend z nim zwi�zanych.
Wspomn� jeszcze o tym, �e pomoc nie musi, ba! nawet nie powinna! by� pisana jak dla debili (jak to ma miejsce niestety na innych mudach). Wystarcz� w niej wskaz�wki nakierowuj�ce graczy na odpowiednie komendy. Przyk�ad: "Broszk� mo�esz przypi�� do ubrania." zamiast: "Oto lista dostepnych komend! Przypnij broszke do plaszcza - przypiecie broszki do plaszcza...".
Aha, i �adnych ASCII tabelek, rubryczek, gwiazdeczek i rameczek dooko�a help�w!
I bro� Bo�e, �adnych sugestii w d�ugim opisie, jakie znamy, chyba szczeg�lnie z Barsawii: "W rogu tej kolorowej poduszeczki dostrzegasz malusie�k�, tak� tyci tyci, �e w og�le dziwne, �e j� widzisz meteczk�, na kt�rej miniaturowymi litereczkami napisano napisik '?poduszeczka'". >_<

No, a teraz obiecany przyk�ad. Pos�u�� si� akcj� pomocy, poniewa� mo�na j� od razu skopiowa� do swojego przedmiotu zapewniaj�c tym samym przynajmniej poprawne funkcjonowanie helpa. :P

/*Wpierw dodajemy: */
void
init()
{
    ::init();
    add_action("pomoc", "?", 2); /*Normalnie nie dajemy tego trzeciego 
                                   argumentu "2". Ale akcja "?" jest
				   wyj�tkowa, ze wzgl�du na to, �e 
				   argument podawany do tej akcji
				   nie zawiera spacji po "?".        */
   /*A normalnie to dodajemy tak: add_action("funkcja","komenda");   */
}
/*A teraz juz funkcja pomocy: */
public int
pomoc(string str)
{
    object broszka;
    notify_fail("Nie ma pomocy na ten temat.\n"); /*Akurat w pomocy
                              taki notify_fail jest konieczny. Nie 
			      zmieniamy go.                        */
    if (!str) return 0;     /* Tutaj warunek: je�li po "?" nie wpiszemy
                               nazwy obiektu, to zwraca 0, czyli notify
			       fail                                    */
    if (!parse_command(str, this_player(), "%o:" + PL_MIA, broszka))
        return 0; /* Warunek: Jesli to wszystko co po wykrzykniku jest
	            nieprawd� (czyli je�li po "?" nie dajemy nazwy obiektu
		    zdefiniowanego jako object broszka), to: zwraca 0,
		    czyli notify_fail                                    */
    if (broszka != this_object())
        return 0;  /* Czyli: jesli obiekt broszka to nie jest ten obiekt,
	              na ktorym wydajemy polecenie: wtedy rowniez zwraca 0 */
	
   /*A w reszcie przypadkow, zwraca w koncu nam ta upragnion� pomoc: */
    write("T� "+this_object()->short()+" mo�esz spr�bowa� "+
           "przypia� do ubrania.\n");
    return 1;
}



To by by�o chyba na tyle. Mam nadziej�, �e uda�o mi si� przedstawi� podstawowe kwestie, kt�re nale�y uwzgl�dni� przy kodowaniu obiekt�w.
                                                               
							       Lil. 1.10.2005
