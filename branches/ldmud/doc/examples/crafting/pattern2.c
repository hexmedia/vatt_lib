/**
 * \file /doc/examples/crafting/pattern1.c
 *
 * Plik z przyk�adowym wzorcem koronkowych majteczek.
 */

inherit "/std/pattern";

#include <pattern.h>
#include <materialy.h>

public void
create()
{
  set_pattern_domain(PATTERN_DOMAIN_TAILOR);
  set_item_filename("/doc/examples/ubrania/spodnie/czarne_skorzane_Mc.c");
  set_estimated_price(800);
  set_time_to_complete(30);
  enable_want_sizes();
  set_possible_materials(({MATERIALY_SK_SWINIA, MATERIALY_SK_CIELE}));
  add_attribute("rodzaj sk�ry", ({"�wi�ska", "ciel�ca"}));
}

public void
configure_item(object item, mapping attrs)
{
    ::configure_item(item, attrs);
    switch (attrs["rodzaj sk�ry"]) {
        case "�wi�ska":
            item->ustaw_material(MATERIALY_SK_SWINIA, 100);
            break;
        case "ciel�ca":
            item->ustaw_material(MATERIALY_SK_SWINIA, 0);
            item->ustaw_material(MATERIALY_SK_CIELE, 100);
            break;
    }
}
