#pragma strict_types

inherit "/std/object.c";

#include <stdproperties.h>
#include "./definicje.h"
#include <macros.h>

void
create_object()
{

    ustaw_nazwe(({"scena", "sceny", "scenie", "scene", "scena", "scenie"}), ({"sceny", "scen", "scenom", "sceny", "scenami", "scenach"}), PL_ZENSKI);
	set_long("Jest to wykonana z rownych desek scena, przed ktora ustawiono wiele stolikow. To na niej "
	  + "znajduje sie wynajeta orkiestra zabawiajaca gosci skocznymi kawalkami. Co jakis czas na "
	  + "tej tez scenie odbywaja sie mrozace krew w zylach przedstawienia badz popisowe sztuczki magiczne.\n");
	
	dodaj_przym("drewniany","drewniani");
	add_prop(OBJ_I_NO_GET,1);

}

