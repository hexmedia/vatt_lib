inherit "/std/receptacle";

#include <pl.h>
#include <stdproperties.h>

create_receptacle()
{
    ustaw_nazwe( ({"kufer","kufra","kufrowi","kufer","kufrem","kufrze"}), 
                 ({"kufry","kufrow","kufrom","kufry","kuframi","kufrach"}), PL_MESKI_NOS_NZYW);
		 
    dodaj_przym("obszerny","obszerni");

    set_long("Jest to zwykly wykonany z drewna kuferek, w ktorym mozesz schowac "
      + "dowolny przedmiot. Wyglada na bardzo obszerna skrzynie, a jej wieko "
      + "jest mocne i solidne.\n");
    
     add_prop(CONT_I_RIGID, 1);
     add_prop(CONT_I_MAX_VOLUME, 240000);
     add_prop(CONT_I_MAX_WEIGHT, 100000);
     add_prop(CONT_I_VOLUME, 168000);
     add_prop(CONT_I_WEIGHT, 50000);
     add_prop(OBJ_I_NO_GET, 1);
}
    

