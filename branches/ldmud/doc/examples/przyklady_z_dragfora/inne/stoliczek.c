inherit "/std/object";

#include <pl.h>
#include <stdproperties.h>

create_object()
{
    ustaw_nazwe( ({"stoliczek","stoliczka","stoliczkowi","stoliczek","stoliczkiem","stoliczku"}), 
                 ({"stoliczki","stoliczkow","stoliczkom","stoliczki","stoliczkami","stoliczkach"}), PL_MESKI_NOS_NZYW);
		 
    dodaj_przym("drewniany","drewniani");

    set_long("Ten niezbyt duzy stoliczek zastepuje w tym ubogim "
      + "sklepiku lade. Zwykle wlasnie za nim siedzi sprzedawca. Na jego blacie ustawiono "
      + "dokladnie: ksiazke z wszelkimi rachunkami, pioro i kalamarz z inkaustem, a takze "
      + "oliwna lampe i pudeleczko czekoladek, ktory sprzedawca pogryza w czasie pracy.\n");
    
     add_prop(OBJ_I_NO_GET, 1);
}
    

