inherit "/d/std/zbroja";          //Copyright Lord Falandir 2003
#include <stdproperties.h>        //Chyba widac co to jest
#include <wa_types.h>
#include <formulas.h>
#include <macros.h>

void
create_armour()
{
    set_autoload();
    ustaw_nazwe( ({ "pierscien", "pierscieni", "pierscieniowi", "pierscien",
    	"pierscieniem", "pierscieniu" }), ({ "pierscienie", "pierscieniow", "pierscieniom",
    	"pierscienie", "pierscieniami", "pierscieniach" }), PL_MESKI_NOS_NZYW);

    dodaj_przym("lekki", "leccy");
    dodaj_przym("zloty","zlote");

    set_long("Jest to kunsztownie zrobiony pierscien z najdrozszego zlota "+
             "a u jego wienczenia znajduje sie kilkukaratowy diament, "+
             "na oko nie jestes w stanie powiedziec jak wiele jest wart. "+
             "Od wewnetrznej strony widnieja insygnia zakladu jubilerskiego "+
             "Kazartona znanego na calym swiecie.\n");
             
    set_slots(A_ANY_FINGER);    


    add_prop(OBJ_I_WEIGHT,30);
    add_prop(OBJ_I_VALUE,5000);


}
