inherit "/std/object.c";
#include <stdproperties.h>
#include <ss_types.h>
#include <wa_types.h>
#include <macros.h>
#include "/sys/tartak.h"
#include <pl.h>
inherit "/cmd/std/command_driver";

#define QP(x)	query_prop((x))
#define TP	this_player()

#define DL 	({"calosc jeszcze nie pocietego drzewa",\
		"polowka jakiegos wiekszego drzewa",\
		"jakas czesc wiekszgo drzewa",\
		"jakas czastka wiekszego drzewa"})
#define GR	({"jeszcze calkowicie nieociosana",\
		"ociosana juz tu i owdzie",\
		"pozadnie ociosana",\
		"juz ladnie ociosana",\
		"calkowicie ociosana"})

#define OBJECT_DESKA	("/d/Wiz/cerdin/tartak/deska.c")

float dl = 10.0,
      gr = 1.1,
      ro = 750.0,
      przelicznik = 0.5;
int   *pot = ({0,0});
 

void
create_drzewo()
{
}

void
update_opis()
{
string *przym = query_przym(1,0,2);
if (sizeof(przym))
{
    remove_adj(przym[0]);
    remove_adj(przym[1]);
}

    switch (ftoi(dl))
	{
 	case 0..6: przym = ({"krociutki","krociutcy"}); break;
	case 7..10: przym = ({"krotki","krotcy"}); break;
	case 11..15: przym = ({"dlugi","dludzy"}); break;
	default: przym = ({"ogromny","ogromni"});
	}
dodaj_przym(przym[0],przym[1]);

    switch(ftoi(gr*10.0))
	{
	case 0..5: przym = ({"cienki","ciency"}); break;
	case 6..10: przym = ({"smukly","smukli"}); break;
	case 11..16: przym = ({"standardowy","standardowi"}); break;
	case 17..23: przym = ({"gruby","grubi"}); break;
	case 24..27: przym = ({"barylkowaty","barylkowaci"}); break;
	default: przym = ({"potezny","potezni"});
	}
dodaj_przym(przym[0],przym[1]);
}

void
update_props()
{
add_prop(OBJ_I_VOLUME, ftoi(3.14*gr*gr*dl*1000.0));
add_prop(OBJ_I_WEIGHT, ftoi(3.14*gr*gr*dl*ro));
add_prop(OBJ_I_VALUE,  ftoi(itof(QP(OBJ_I_WEIGHT))*przelicznik));
}

string
long()
{
string str;

str = "Jest to "+query_przym(1,0,4)[0]+" i "+query_przym(1,0,4)[1]+" kloda. "+
      "Widac ze jest to "+DL[pot[0]]+", "+GR[pot[1]]+". W kilku miejscach "+
      "zauwazasz liczne slady po siekierze lub toporze, a zywica pojawia "+
      "sie juz tu i owdzie, jakby chciala zakryc najbardziej bolesne rany "+
      "obalonego tworu natury. ";
if (this_player()->query_skill(SS_HERBALISM) > 20)
	{
	str += "Ze swej wiedzy o roslinach wnioskujesz ze to chyba "+
	QP(TR_M_TYP)[0]+".";
	}
return str+"\n";
}

void
create_object()
{
ustaw_nazwe(({"drzewo","drzewa","drzewu","drzewo","drzewem","drzewie"}),
    ({"drzewa","drzew","drzewom","drzewa","drzewami","drzewach"}),
    PL_NIJAKI_NOS);
add_prop(TR_M_TYP,TR_SOSNA);
add_prop(OBJ_M_NO_SELL,1);
set_long(long);

create_drzewo();

update_props();
update_opis();
}

int
query_drzewo()
{
return 1;
}

void
ustaw_dlugosc(float dlugosc)
{
dl = dlugosc;
}

float
query_dlugosc()
{
return dl;
}

void
ustaw_grubosc(float grubosc)
{
gr = grubosc;
}

float
query_grubosc()
{
return gr;
}

void
ustaw_gestosc(float gestosc)
{
ro = gestosc;
}

float
query_gestosc()
{
return ro;
}

void
ustaw_przelicznik(float p)
{
przelicznik = p;
}

float
query_przelicznik()
{
return przelicznik;
}


int
rab(string str)
{
object *oblist;
object *dr;

oblist = parse_this(str, "%i:"+ PL_BIE);


if (!sizeof(oblist))
    {
    notify_fail(capitalize(query_verb())+" co?\n");
    return 0;
    }

dr = filter(oblist, &->query_drzewo());

/*
if (oblist != dr);
    {
    notify_fail("Nie mozesz tego uczynic.\n");
    return 0;
    }
*/
if (sizeof(oblist) > 1)
    {
    notify_fail("Po chwili zastanowienia dochodzisz do wniosku ze "+
    "latwiej jednak bedzie "+ (query_verb() == "obciosaj" ? "obciosywac":
    "przecinac") + " pojedynczo.\n");
    return 0;
    }
dr[0] = oblist[0];

oblist = TP->query_weapon(-1);

if (!sizeof(oblist) || oblist[0]->query_wt() != W_AXE)
    {
    notify_fail("Musisz trzymac w reku jakis topor.\n");
    return 0;
    }
if (query_verb() == "obciosaj")
    return dr[0]->ciosaj();
else
    return dr[0]->tnij();
}

void
init()
{
    ::init();
    add_action(rab,"przetnij");
    add_action(rab,"obciosaj");
}

int
ciosaj()
{
if ((pot[1] > (sizeof(GR)-2)) || (gr-0.4 < 0.0))
    {
    notify_fail("Nie potrafisz bardziej ociosac "+short(TP,PL_DOP)+".\n");
    return 0;
    }
write("Obciosujesz "+short(TP,PL_BIE)+".\n");
say(QCIMIE(TP,PL_MIA)+ " obciosuje "+short(TP,PL_BIE)+".\n");
gr -= 0.4;

update_props();
update_opis();
pot[1] += 1;
return 1;
}

int
tnij()
{
object ob;
string *p;

if (pot[1] == 0)
    {
    notify_fail("Pierw by wypadaloby to choc troche ociosac.\n");
    return 0;
    }

if (pot[0] > (sizeof(DL)-2))
    {
    write("Rozcinasz "+short(TP,PL_BIE)+" na pol i ze zdumieniem "+
          "stwierdzasz ze drewno rozpadlo sie na do niczego nie "+
          "przydatne kawalki. Chyba tym razem przesadzil"+
	  TP->koncowka("es","as")+".\n");
    say(QCIMIE(TP,PL_MIA)+" proboje rozciac "+short(TP,PL_BIE)+", lecz "+
        "otrzymuje tylko stos nikomu nie przydatnych kawalkow drewna.\n");
    destruct();
    return 1;
    }

write("Rozcinasz "+short(TP,PL_BIE)+" na pol.\n");
say(QCIMIE(TP,PL_MIA)+" rozcina "+short(TP,PL_BIE)+" na dwie czesci.\n");

pot[0] += 1;

    setuid();
    seteuid(getuid());

ob = clone_object(MASTER);
ob->ustaw_dlugosc(dl/2.2);
ob->ustaw_grubosc(gr);
ob->ustaw_gestosc(ro);
ob->ustaw_przelicznik(przelicznik);
ob->add_prop(TR_M_TYP,QP(TR_M_TYP));
ob->ustaw_pot(pot);
ob->update_props();
ob->update_opis();
ob->move(environment(TP));

ob = clone_object(MASTER);
ob->ustaw_dlugosc(dl/2.2);
ob->ustaw_grubosc(gr);
ob->ustaw_gestosc(ro);
ob->ustaw_przelicznik(przelicznik);
ob->add_prop(TR_M_TYP,QP(TR_M_TYP));
ob->ustaw_pot(pot);
ob->update_props();
ob->update_opis();
ob->move(environment(TP));
destruct();
return 1;
}

ustaw_pot(string *p)
{
pot = p;
}

object *
query_deski()
{
object *ob=({}), obb;
int p,g;

dl = dl*0.6;
g = random(ftoi(gr*6.0))+1;

while (g)
{
p = random(3)+1;
    while (p)
    {
    obb = clone_object(OBJECT_DESKA);
    obb->add_prop(TR_M_TYP,QP(TR_M_TYP));
    obb->update_deska();
    ob += ({ obb });
    p--;
    }
g--;
}

return ob;
}