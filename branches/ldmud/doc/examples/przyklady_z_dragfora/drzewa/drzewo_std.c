#pragma strict_types
inherit "/std/object";
#include <stdproperties.h>
#include <wa_types.h>
#include <ss_types.h>
#include <pl.h>
#include <macros.h>
#include <composite.h>
#include <std.h>
#include <files.h>
#include <cmdparse.h>
#include <language.h>
#define TP this_player()
#define TO this_object()
#define PLAYER_I_SCINAM "_player_i_scinam"
#define PLAYER_I_OCIOSUJE "_player_i_ociosuje"
int ile_desek;
int jak_trudno_sciac;
int ociosane; // 1- ociosane 0-nieociosane
int ktos_ociosuje;
string dlugi_opis;
string nazwa_dop;
void
create_drzewo()
{
    jak_trudno_sciac=1;
}
nomask void
create_object()
{
    create_drzewo();
    ustaw_nazwe(({"drzewo "+nazwa_dop,"drzewa "+nazwa_dop,"drzewu "+nazwa_dop,
    "drzewo "+nazwa_dop,"drzewem "+nazwa_dop,"drzewie "+nazwa_dop}),
     ({"drzewa "+nazwa_dop,"drzew "+nazwa_dop,"drzewom "+nazwa_dop,
     "drzewa "+nazwa_dop,"drzewami "+nazwa_dop,"drzewach "+nazwa_dop}),PL_NIJAKI_NOS);
    dodaj_nazwy(({"drzewo","drzewa","drzewu","drzewo","drzewem","drzewie"}),
     ({"drzewa","drzew","drzewom","drzewa","drzewami","drzewach"}),PL_NIJAKI_NOS);          set_long("@@dlugi_opis");
    ile_desek=random(4)+1;
    switch(ile_desek)
    {
case 1:
    jak_trudno_sciac=1;
    dodaj_przym("maly","mali");
        add_prop(OBJ_I_VOLUME,5000);
        add_prop(OBJ_I_WEIGHT,7000);
break;
case 2:
    jak_trudno_sciac=2;
    dodaj_przym("dlugi","dludzy");
        add_prop(OBJ_I_VOLUME,9000);
        add_prop(OBJ_I_WEIGHT,11000);    
break;
case 3:
    jak_trudno_sciac=3;
    dodaj_przym("duzy","duzi");
        add_prop(OBJ_I_VOLUME,13000);
        add_prop(OBJ_I_WEIGHT,15000);         
break;
case 4:
    jak_trudno_sciac=4;
    dodaj_przym("wielki","wielcy");
        add_prop(OBJ_I_VOLUME,17000);
        add_prop(OBJ_I_WEIGHT,19000);    
break;
case 5:
    jak_trudno_sciac=5;
    dodaj_przym("ogromny","ogromni");
        add_prop(OBJ_I_VOLUME,20000);
        add_prop(OBJ_I_WEIGHT,23000);    
break;
    }    
setuid();
seteuid(getuid());
//    odmien_short();
//    odmien_plural_short();*/
}
string
dlugi_opis()
{
return dlugi_opis;
}
int
query_trudnosc_sciecia()
{
return jak_trudno_sciac;
}
int
query_ilosc_desek()
{
return ile_desek;
}
void
set_dlugi_opis(string opis)
{
dlugi_opis=opis;
}
void
set_nazwa_dop(string nazwa)
{
nazwa_dop=nazwa;
}
string
query_nazwa_dop()
{
return nazwa_dop;
}
int
query_drzewo()
{
return 1;
}
void
set_forma()
{
    ociosane=1;
}
int
query_forma()
{
   return ociosane;
}
void
set_zajete(int x)
{
    ktos_ociosuje=x;
}
int
query_zajete()
{
    return ktos_ociosuje;
}
void
init()
{
    add_action("ociosaj","ociosaj");
}
int
ociosaj(string str)
{
mixed *drzewa;
object *bronie;
object paraliz;
int id;
int wt;
int i;
int z;

    notify_fail("Co chcesz ociosac ?\n");
    if(!str) return 0;
    
    parse_command(str,environment(TO),"%i:"+PL_BIE,drzewa);
    drzewa=NORMAL_ACCESS(drzewa,0,0);
    
    if (!sizeof(drzewa)) return 0;
    
    if (sizeof(drzewa)>1) return 0;
    if (drzewa[0]->query_drzewo()!=1) return 0;
    if (environment(TO)->query_living()==1)
    {
        TP->catch_msg("Moze wpierw odlozysz to co chcesz ociosywac ?\n");
        return 1;
    }
    bronie=TP->query_weapon(-1);
    if(!sizeof(bronie))
    {
        write("Chyba nie chcesz ociosywac drzewa golymi rekoma ?\n");
        return 1;
    }        
    for(i=0;i<sizeof(bronie);i++)
    {
        wt=bronie[i]->query_wt();
        if(wt!=W_AXE)
        {
            write("Nie masz odpowiedniego narzedzia.\n");
            return 1;
        }
    }
    if(TP->query_stat(0)<36)
    {
        write("Chyba jestes za slab"+TP->koncowka("y","a","e")+".\n");
        return 1;
    }
    if(TP->query_old_fatigue()<30)
    {
        write("Jestes zbyt zmeczon"+TP->koncowka("y","a","e")+".\n");
return 1;
    }
     if(drzewa[0]->query_zajete()==1)
    {
        write("Nie mozesz tego ociosywac gdyz juz ktos inny to robi.\n");
        return 1;
    }
    paraliz=clone_object("/d/Faerun/drzewa/ociosajparaliz");
    TP->add_prop("_object_drzewa_",drzewa[0]);
    TP->add_prop(PLAYER_I_OCIOSUJE,1);
     switch(drzewa[0]->query_forma())
    {
    case 0:   //drzewo nie ociosane
          paraliz->move(TP);
          TP->catch_msg("Zaczynasz ociosywac "+drzewa[0]->query_short(PL_BIE)+".\n");
          saybb(QCIMIE(TP,PL_MIA)+" zaczyna ociosywac "+drzewa[0]->query_short(PL_BIE)+".\n");
          drzewa[0]->set_zajete(1);
          id=set_alarm(30.0,0.0,"ociosaj_faza_1",TP,paraliz,drzewa[0]);
          TP->add_prop("_ociosywanie_drzew_alarm_id_",id);
      break;
      case 1:  //drzewo ociosane
          paraliz->move(TP);
          TP->catch_msg("Zaczynasz ociosywac "+drzewa[0]->query_short(PL_BIE)+".\n");
          saybb(QCIMIE(TP,PL_MIA)+" zaczyna ociosywac "+drzewa[0]->query_short(PL_BIE)+".\n");
          drzewa[0]->set_zajete(1);
          id=set_alarm(35.0,0.0,"ociosaj_faza_2",TP,paraliz,drzewa[0]);
          TP->add_prop("_ociosywanie_drzew_alarm_id_",id);
      break;
      case 2: //belka
          paraliz->move(TP);
          TP->catch_msg("Zaczynasz ociosywac "+drzewa[0]->query_short(PL_BIE)+".\n");
          saybb(QCIMIE(TP,PL_MIA)+" zaczyna ociosywac "+drzewa[0]->query_short(PL_BIE)+".\n");
          drzewa[0]->set_zajete(1);
          id=set_alarm(35.0,0.0,"ociosaj_faza_3",TP,paraliz,drzewa[0]);
          TP->add_prop("_ociosywanie_drzew_alarm_id_",id);
    break;
    }
return 1;
}


void
ociosaj_faza_1(object pl,object par,object drzewko)
{
 int waga,objetosc;
    if(!present("ociosajparaliz",pl))
    {
        pl->remove_prop(PLAYER_I_OCIOSUJE);
        drzewko->set_zajete(0);
        return;
    }
 if(!pl) return;
if(!present(pl,environment(TO))) return;
 par->remove_object();
 pl->catch_msg("Konczysz ociosywac "+drzewko->query_short(PL_BIE)+".\n");
 saybb(QCIMIE(pl,PL_MIA)+" konczy ociosywac "+drzewko->query_short(PL_BIE)+".\n");
 drzewko->dodaj_przym("ociosany","ociosani");
    drzewko->odmien_short();
    drzewko->odmien_plural_short();
 drzewko->set_forma();
 waga=drzewko->query_prop(OBJ_I_WEIGHT);
 objetosc=drzewko->query_prop(OBJ_I_VOLUME);
 drzewko->change_prop(OBJ_I_WEIGHT,waga/3);
 drzewko->change_prop(OBJ_I_VOLUME,objetosc/2);
 drzewko->set_zajete(0);
 pl->remove_prop(PLAYER_I_OCIOSUJE);
 pl->remove_prop("_ociosywanie_drzew_alarm_id_");
pl->remove_prop("_object_drzewa_");
}
void
ociosaj_faza_2(object pl, object par, object drzewko)
{
    object belka;
if(!present("ociosajparaliz",pl))
    {
        pl->remove_prop(PLAYER_I_OCIOSUJE);
    drzewko->set_zajete(0);
        return;
    }
    if(!pl->query_prop(PLAYER_I_OCIOSUJE)) return;
     if(!present(pl,environment(TO))) return;
    if(!pl) return;
    par->remove_object();
    pl->catch_msg("Konczysz ociosywac "+drzewko->query_short(PL_BIE)+".\n");
    saybb(QCIMIE(pl,PL_MIA)+" konczy ociosywac "+drzewko->query_short(PL_BIE)+".\n");
    pl->remove_prop(PLAYER_I_OCIOSUJE);
    pl->remove_prop("_ociosywanie_drzew_alarm_id_");
    pl->remove_prop("_object_drzewa_");
    belka=clone_object("/d/Faerun/drzewa/belka");
    belka->ustaw_ile_desek(drzewko->query_ilosc_desek());
    belka->move(environment(drzewko));
    drzewko->remove_object();
}
void
ociosaj_faza_3(object pl, object par, object drzewko)
{
    object deska;
    int zr;
if(!present("ociosajparaliz",pl))
    {
        pl->remove_prop(PLAYER_I_OCIOSUJE);
    drzewko->set_zajete(0);
        return;
    }
    if(!pl->query_prop(PLAYER_I_OCIOSUJE)) return;
    if(!present(pl,environment(TO))) return;
    if(!pl) return;
    par->remove_object();
    pl->catch_msg("Konczysz ociosywac "+drzewko->query_short(PL_BIE)+".\n");
    saybb(QCIMIE(pl,PL_MIA)+" konczy ociosywac "+drzewko->query_short(PL_BIE)+".\n");
    pl->remove_prop(PLAYER_I_OCIOSUJE);
    pl->remove_prop("_ociosywanie_drzew_alarm_id_");
    pl->remove_prop("_object_drzewa_");
    drzewko->zabral_deske();
    deska=clone_object("/d/Faerun/drzewa/deska");
    zr=pl->query_stat(SS_DEX);
    switch(zr)
    {
    case 0..30:
    deska->ustaw_jakosc(1);
    break;
    case 31..50:
    deska->ustaw_jakosc(2);
    break;
    case 51..300:
    deska->ustaw_jakosc(3);
    break;
    }
    deska->move(environment(TO));
    if(drzewko->query_ile_desek()==0) drzewko->remove_object();
    drzewko->set_zajete(0);
}
void
usun_alarma(int numer)
{
remove_alarm(numer);
}
