#pragma strict_types
#pragma no_clone
#include <stdproperties.h>
#include <wa_types.h>
#include <ss_types.h>
#include <pl.h>
#include <macros.h>
#include <composite.h>
#include <std.h>
#include <files.h>
#include <cmdparse.h>
#include <language.h>
#define TP this_player()
#define TO this_object()
#define PLAYER_I_SCINAM "_player_i_scinam"
#define PLAYER_I_OCIOSUJE "_player_i_ociosuje"
string *drzewa=({});
int max_drzew;
void
dodaj_drzewa(string *lista)
{
    drzewa+=lista;
}
string *
query_drzewa()
{
    return drzewa;
}
void
ustaw_max_drzew(int ile)
{
   max_drzew=ile;
}
int
query_max_drzew()
{
    return max_drzew;
}


void
inicjuj_scinanie()
{
    add_action("scinaj","scinaj");
}
int
scinaj(string str)
{
object *bronie;
object paraliz;
object drzewo;
int id;
int wt;
int i;
int z;
    if((!str)||(str!="drzewo"))
    {
        notify_fail("Co chcesz scinac ?\n");
        return 0;
    }
    bronie=TP->query_weapon(-1);
    if(!sizeof(bronie))
    {
        write("Chyba nie chcesz scinac drzewa golymi rekoma ?\n");
        return 1;
    }        
    for(i=0;i<sizeof(bronie);i++)
    {
        wt=bronie[i]->query_wt();
        if(wt!=W_AXE)
        {
            write("Nie masz odpowiedniego narzedzia.\n");
            return 1;
        }
    }
    if(TP->query_stat(0)<36)
    {
        write("Chyba jestes za slab"+TP->koncowka("y","a","e")+".\n");
        return 1;
    }
    if(TP->query_old_fatigue()<30)
    {
        write("Jestes zbyt zmeczon"+TP->koncowka("y","a","e")+".\n");
return 1;
    }
    if(max_drzew==0)
    {
write("Nie zauwazasz tutaj odpowiednich drzew.\n");
return 1;
    }
    //klonujemy paraliz
    paraliz=clone_object("/d/Faerun/drzewa/scinajparaliz.c");
    paraliz->move(TP);
    z=random(sizeof(drzewa));
    drzewo=clone_object(drzewa[z]);
    TP->catch_msg("Zaczynasz scinac drzewo.\n");
    saybb(QCIMIE(TP,PL_MIA)+" zaczyna scinac drzewo.\n");
    TP->add_prop(PLAYER_I_SCINAM,1);
    id=set_alarm(itof(10*drzewo->query_trudnosc_sciecia()),0.0,"scinanie",TP,paraliz,drzewo);
    TP->add_prop("_scinanie_drzew_alarm_id_",id);
    TP->add_prop("_player_o_drzewa_",drzewo);
    return 1;
}

void
scinanie(object pl,object par,object drzewko)
{
    if(!present("scinajparaliz",pl))
    {
        pl->remove_prop(PLAYER_I_SCINAM);
        return;
    }
    if(!pl->query_prop(PLAYER_I_SCINAM)) return;
    if(!present(pl)) return;
    if(!pl) return;
    pl->catch_msg("Ostatnimi ruchami swego topora konczysz scinac drzewo, a ono "+
    "uderza z duzym impetem o ziemie.\n");
   saybb(QCIMIE(pl,PL_MIA)+" ostatnimi uderzaniemi topora konczy scinac drzewo, ktore "+
   "uderza z duzym impetem o ziemie.\n");
    par->remove_object();
    drzewko->move(TO);
    pl->remove_prop(PLAYER_I_SCINAM);
    pl->remove_prop("_scinanie_drzew_alarm_id_");
    pl->remove_prop("_player_o_drzewa_");
    if(random(50))
    {
    }
}

