#pragma strict_types
inherit "/std/object";
#include <macros.h>
#include <pl.h>
#include <stdproperties.h>
int jakosc;
void
create_object()
{
ustaw_nazwe(({"deska","deski","desce","deske","deska","desce"}),
        ({"deski","desek","deska","deski","deskami","deskach"}),
            PL_ZENSKI);
set_long("Jest to najzwyklejsza na swiecie deska.\n");
}
void
ustaw_jakosc(int i)
{
jakosc=i;
    switch(i)
    {
case 1:
        dodaj_przym("krzywy","krzywy");
        add_prop(OBJ_I_VOLUME,500);
        add_prop(OBJ_I_WEIGHT,20);
break;
case 2:
        dodaj_przym("dlugi","dludzy");
        add_prop(OBJ_I_VOLUME,700);
        add_prop(OBJ_I_WEIGHT,300);    
break;
case 3:
        dodaj_przym("dorodny","dorodni");
        add_prop(OBJ_I_VOLUME,700);
        add_prop(OBJ_I_WEIGHT,400);         
break;
}
}
int
query_deska()
{
return 1;
}
int
query_jakosc()
{
return jakosc;
}
