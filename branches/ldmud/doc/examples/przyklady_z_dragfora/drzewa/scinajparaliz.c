#pragma strict_types
inherit "/std/paralyze";
#define TP this_player()
#include <macros.h>
void
create_paralyze()
{
    set_name("scinajparaliz");
    set_stop_verb("przestan");
    set_stop_message("Przestajesz scinac.\n");
    set_fail_message("Jestes teraz zajety scinaniem drzewa. Musisz wpisac " +
                "'przestan' by moc zrobic, to co chcesz.\n");
    set_stop_fun("zatrzymanie");
    set_stop_object(this_object());
}
int
zatrzymanie(string str)
{
int alarm_id;
object drzewo;
alarm_id=TP->query_prop("_scinanie_drzew_alarm_id_");
drzewo->usun_alarma(alarm_id);
drzewo=TP->query_prop("_player_o_drzewa_");
TP->remove_prop("_player_o_drzewa_");
TP->remove_prop("_scinanie_drzew_alarm_id_");
TP->remove_prop("_player_i_scinam");
TP->catch_msg(query_stop_message());
saybb(QCIMIE(TP,PL_MIA)+" przestaje scinac.\n");
drzewo->set_zajete(0);
remove_object();
return 1;
}
