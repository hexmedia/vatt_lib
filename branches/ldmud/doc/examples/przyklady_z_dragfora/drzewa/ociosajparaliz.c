#pragma strict_types
#include <macros.h>
#define TP this_player()
inherit "/std/paralyze";
static int alarm_id;
void
create_paralyze()
{
    set_name("ociosajparaliz");
    set_stop_verb("przestan");
    set_stop_message("Przestajesz ociosywac.\n");
    set_fail_message("Jestes teraz zajety ociosywaniem. Musisz wpisac " +
                "'przestan' by moc zrobic, to co chcesz.\n");
    set_stop_object(this_object());
    set_stop_fun("zatrzymanie");
setuid();
 seteuid(getuid());
}
int
zatrzymanie(string str)
{
object drzewo;
alarm_id=TP->query_prop("_ociosywanie_drzew_alarm_id_");
drzewo=TP->query_prop("_object_drzewa_");
TP->remove_prop("_ociosywanie_drzew_alarm_id_");
drzewo->usun_alarma(alarm_id);
TP->remove_prop("object_drzewa_");
TP->catch_msg(query_stop_message());
saybb(QCIMIE(TP,PL_MIA)+" przestaje ociosywac "+drzewo->query_short(PL_BIE)+".\n");
drzewo->set_zajete(0);
TP->remove_prop("_player_i_ociosuje");
remove_object();
return 1;
}
