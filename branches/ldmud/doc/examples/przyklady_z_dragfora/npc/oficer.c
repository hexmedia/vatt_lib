#include "../pal.h"
#include <ss_types.h>

inherit "/std/monster";

#define PRZYMIOTNIKI ({ ({"mlody","mlodzi"}), ({"sedziwy","sedziwi"}), \
                        ({"umiesniony","umiesnieni"}), ({"muskularny","muskularni"}), \
                        ({"dlugoreki","dlugorecy"}), ({"wysoki","wysocy"}), \
                        ({"masywny","masywni"}), ({"opalony","opaleni"}), ({"opalony","opaleni"}), \
                        ({"jasnowlosy","jasnowlosi"}), ({"ciemnowlosy","ciemnowlosi"}), \
                        ({"ponury","ponurzy" }) })

#define LICZBA_PRZYMIOTNIKOW 12
#define PEN_SPECJALA 50 /* Specjal niedlugo bedzie */

void create_monster()
{
    int i,j;

    ustaw_odmiane_rasy(({"gwardzista", "gwardzisty", "gwardziscie", "gwardziste", "gwardzista", "gwardziscie"}),
                ({"gwardzisci", "gwardzistow", "gwardzistom", "gwardzistow", "gwardzistami", "gwardzistach"}), 
                PL_MESKI_OS);

    set_gender(G_MALE);
    
    set_long("Pelnozbrojny rycerz stoi na strazy miasta. Widzac jego "
    +"muskulature, opanowanie i to zimne spojrzenie, ktore uderza w ciebie "
    +" spod zamknietej przylbicy, wolisz nie wchodzic mu w droge. "
    +" Zauwazasz takze, ze jest on powazniejszy i bardziej dumny od reszty gwardii. "
    +" To zapewne sierzant.\n");

    add_prop(LIVE_I_NEVERKNOWN,1);

    i = random(LICZBA_PRZYMIOTNIKOW);
    j = random(LICZBA_PRZYMIOTNIKOW);
    
    while(!(j>(i+2) || j<(i-2)) ) j = random(LICZBA_PRZYMIOTNIKOW);
       	dodaj_przym(PRZYMIOTNIKI[i][0], PRZYMIOTNIKI[i][1]);
    	dodaj_przym(PRZYMIOTNIKI[j][0], PRZYMIOTNIKI[j][1]);

        set_stats(({130,125,150,90,70,135}));

        set_cact_time(4);
        add_cact("emote mowi: Splynie na ciebie gniew Paladina!");
        add_cact("emote sapie: Jestes waleczny, ale Paladine prowadzi moja dlon");
        add_cact("krzyknij Za miasto Palanthas!");
        add_cact("krzyknij Ku chwale Paladina!");

        set_act_time(10 + random(20));
        add_act("emote rozglada sie groznie.");
        add_act("emote przyglada sie wchodzacym do miasta.");        
        add_act("emote przechadza sie, chrzeszczac zbroja.");
        add_act("emote zamysla sie na moment.");
        add_act("emote sprawdza jakiegos jegomoscia.");

	add_armour(ZBR+"kirys_paladine.c");
	add_armour(ZBR+"helm_paladine.c");
	add_weapon(BRN+"miecz_paladine.c");
	add_weapon(BRN+"miecz_paladine.c");
	
	set_skill(SS_WEP_SWORD,60);
	set_skill(SS_PARRY,60);
	set_skill(SS_SHIELD_PARRY,60);
	set_skill(SS_DEFENCE,50);

    	set_alignment(750);  
}

public void wesprzyj(object ob)
{ 
    command("wesprzyj " + OB_NAME(ob));
    
    if (query_attack())
    {
        switch(random(3))
        {
            case 0: command("krzyknij Za miasto Palanthas!"); break;
            case 1: command("krzyknij W imie Paladina!"); break;
            case 2: command("emote dobywa broni i z fanatycznym blyskiem w oku rzuca sie na ciebie."); break;
        }
        command("wesprzyj " + OB_NAME(ob));
        return ;
    }
}

public void pomocy()
{
    (all_inventory(environment(this_object())) - ({ this_object() }))->wesprzyj(this_object());
}

void attacked_by(object wrog)
{
    command("krzyknij Do boju!");
    set_alarm(0.1, 0.0, "pomocy");
    return ::attacked_by(wrog);
}
