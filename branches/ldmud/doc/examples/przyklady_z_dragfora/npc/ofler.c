#pragma strict types

inherit "/std/monster";
inherit "/std/combat/unarmed";

#include <macros.h>
#include <stdproperties.h>
#include <language.h>
#include <composite.h>
#include <money.h>
#include <pl.h>
#include <time.h>
#include <formulas.h>
#include <std.h>
//#include "../definicje.h"
#include <filter_funs.h>
#include "/sys/money.h"

#define A_PAZURY 0
#define A_KLY 1
#define A_OGON 2

int *umy = ({40, 40, 40});

int przekluwanie (string str);

void
create_monster()
{
    ustaw_imie(({ "ofler", "oflera", "oflerowi", "oflera", "oflerem", "oflerze"}), PL_MESKI_OS);
	
	ustaw_odmiane_rasy(            ({"diabelstwo","diabelstwa","diabelstwu","diabelstwo","diabelstwem","diabelstwie"}),
            ({"diabelstwa","diabelstw","diabelstwom","diabelstwa","diabelstwami","diabelstwach"}),
            PL_NIJAKI_OS);
    
	dodaj_przym("dlugowlosy","dlugowlosi");
    dodaj_przym("spiczastouchy","spiczastousi");
    
	set_stats( ({ 40, 75, 25, 35, 20, 40 }) );
    
	set_gender(G_MALE);
    
	add_prop(CONT_I_WEIGHT, 55000); 
    add_prop(CONT_I_HEIGHT, 160);
    
	set_act_time(360);
    add_act("emote grzebie pomiedzy narzedziami.");
    add_act("usmiechnij sie nieznacznie");
    add_act("emote zastanawia sie nad czyms.");
    add_act("powiedz :syczac:Witam w moim zakladzie.");
	add_act("powiedz :wpatrujac sie w ciebie uwaznie:Tak?");
	add_act("emote kreci sie po zakladzie.");
	add_act("emote macha ogonem.");
    
	set_cchat_time(60);
    add_cchat("Juz ja ci zloje ssskore!");
    add_cchat("Lepiej sie ssstrzez smiertelniku!");
    add_cchat("Sssprawdzimy twoja wytrzymalosc na ciosy mieczem.");

	set_cact_time(10);
	add_cact("emote wykonuje potezny zamach ogonem.");

	set_default_answer(VBFC_ME("default_answer"));
  
    set_long("Jest to wysokosci przecietnego Faerunskiego elfa stworzenie o bardzo "
	       + "nietypowej i zaskakujacej budowie fizycznej. Wyglada ono na diabelstwo "
		   + "czyli biesa, ktory sprowadzony zostal do tego miasta z jakiejs innej "
		   + "sfery materialnej gdzie swiat moglby rzadzic sie zupelnie odmiennymi "
		   + "zasadami. Tutaj jednak wiedzie spokojny i cywilizowany zywot, nie prze"
		   + "jmujac sie, iz wielu mieszczan traktuje go jako potwora. Najbardziej rzucaja "
		   + "sie u niego w oczy dlugie, smolistoczarne wlosy, a takze dlugie rece i przenikliwy "
		   + "wzrok, rzucany przez przekrwione slepia. Chociaz wyglada dosyc spokojnie to "
		   + "jednakze rozsadek nakazuje ci byc ostroznym w obecnosci czorta, ktory w "
		   + "kazdej chwili moglby rozszarpac cie swymi pazurami, badz zmiazdzyc "
		   + "masywnym ogonem.\n");

	set_skill(SS_DEFENCE, umy[0] + random(30));
    set_skill(SS_PARRY, umy[1] + random(15));
    set_skill(SS_UNARM_COMBAT, umy[2] + random(20));     
	
	set_attack_unarmed(A_KLY,   2,   5, W_IMPALE, 10, "klami");
    set_attack_unarmed(A_PAZURY, 15, 15, W_SLASH,  10, "pazurami");
	set_attack_unarmed(A_OGON, 15, 15, W_SLASH,  10, "ogonem");
}
void
add_introduced(string imie_mia, string imie_bie)
{
	set_alarm(2.0, 0.0, "return_introduce", find_player(imie_mia));
}
void
return_introduce(object ob)
{
    command("przedstaw sie " + OB_NAME(ob));
    command("powiedz do " + this_player()->short(PL_DOP) + " Witam w moim zakladzie.");
	set_alarm(3.0,0.0, "przedstawienie");
}
string
default_answer()
{
	command("powiedz do " + this_player()->short(PL_DOP) + " Jak zyje nigdy o czyms takim nie ssslyszalem...");
}
void
emote_hook(string emote, object wykonujacy, string przyslowek)
{
    switch (emote)
    {
        case "kopnij":
        case "spoliczkuj" :
        case "szturchnij":
        case "opluj" : set_alarm(2.0, 0.0, "zly", wykonujacy); break;
        	    
        case "zasmiej": 
        case "pocaluj":
        case "przytul": set_alarm(2.0, 0.0, "dobry", wykonujacy);
    }
}



void
zly(object kto)
{
    switch (random(2)) 
    {
		case 0 : command("powiedz Sssmiertelniku. Zloje ci skore jesli jeszcze raz to zrobisz."); break;
		case 1 : command("powiedz Sssmiertelna istoto! Nie wiesz z kim masz do czynienia...");
    default:
    }
}

void
dobry(object kto) 
{
        
    if(kto->koncowka("chlop","baba")=="chlop") zly(kto);
    else
    {   
        switch (random(1)) 
        {
          case 0 : command("emote pozostaje niewzruszony."); break;      
        }
      }
}

void
przedstawienie(object pla)
{
	command("powiedz do " + this_player()->short(PL_DOP) + " Moze chcial" + this_player()->koncowka("bysss","abysss") + " przekluc sobie jakas "
	      + "czesc ciala?");
} 

void
init_living()
{
	::init_living();
	add_action(przekluwanie, "przekluj");
}

int
przekluwanie (string str)
{
	int wynik;
	object dziurka;

	notify_fail("Przekluj co?\n");
	if(!str)
		return 0;
	
	if (!MONEY_ADD(this_player(),-500))
		{
		notify_fail("Nie masz czym zaplacic.\n");
		return 0;
		}


	if(!(dziurka = present("_dziurka_od_kolczykow",this_player())))
		(dziurka = clone_object("/std/dziurka"))->move(this_player(), 1);

	if (!dziurka) 
		{
		notify_fail("Cos sie skopalo, obiekt dziurki nie znaleziony.\n");
		return 0;
		}

	wynik = dziurka->sprawdz_dziurke(str);
	if(wynik == -1)
		return 0;
	notify_fail("Nie ma juz tam dosyc miejsca na kolejna dziurke.\n");
	if(wynik == 0)
		return 0;

	write(this_object()->query_Imie(this_player(), PL_MIA) + " zbliza sie w twa strone po czym "
	     + "szybkim ruchem przekluwa " + str + " pozostawiajac w tym miejscu nieduza dziurke.\n");

    say(this_object()->query_Imie(this_player(), PL_MIA) + " zbliza sie w strone " + this_player()->query_Imie(this_player(), PL_DOP)
		 + " po czym szybkim ruchem przekluwa " + this_player()->koncowka("jego","jej") + " " + str + " pozostawiajac w tym miejscu nieduza dziurke.\n"); 

	dziurka->dodaj_dziurke(str);

	return 1;
}

