#pragma strict_types
inherit "/std/monster.c";
#include "../pal.h"

void create_mieszkanca()
{
    set_gender(G_MALE);

    add_prop(LIVE_I_NEVERKNOWN,1);

    dodaj_przym("wysoki","wysocy");
    dodaj_przym("szczuply","szczupli");
    ustaw_odmiane_rasy(PL_CZLOWIEK);
    
    set_random_move(20,0); 

    set_long("Jest to typowy mieszkaniec Palanthasu. Ciezko odgadnac, czy "
          + "jest on kupcem, turysta, czy zwyklym przechodniem. Najprosciej "
          + "po prostu stwierdzic, ze jest czescia przewalajacej sie przez "
          + "miasto cizby ludzi.\n");

        set_stats(({40,40,40,40,40,40}));

        set_skill(SS_PARRY,15);
        set_skill(SS_WEP_KNIFE, 20 + random(20));
        set_skill(SS_DEFENCE, 20 + random(5));

        add_prop(CONT_I_WEIGHT, 90000);
        add_prop(CONT_I_HEIGHT, 170+random(20));
        
        set_act_time(3+random(10));
        add_act("emote przechadza sie po miescie.");
        add_act("emote probuje ocenic, jaka bedzie pogoda.");
        
        set_cact_time(3+random(5));
        add_cact("krzyknij Ratunkuuu!");
        add_cact("krzyknij Aaah! Morderca! Pomocy!");
              

}