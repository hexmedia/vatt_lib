/*
 * Filename: kaplan.c
 *
 * Zastosowanie: Kaplan.
 * 
 * Copyright Volothamp Geddarm.
 *
 */
inherit "/std/monster.c";

#include <macros.h>
#include <stdproperties.h>
#include <language.h>
#include <composite.h>
#include <money.h>
#include <pl.h>
#include <time.h>
#include <formulas.h>
#include <std.h>
#include <mudtime.h>
#include "../definicje.h"

int *staty = ({30, 30, 30, 30, 30, 30});
int *umy = ({30, 30, 30});

int f_filter(object ktory);

    string *
    losowy_przym()
{
    string *lp;
    string *lm;
	string *lpa;
	string *lma;
    int i, j;
    lp=({ "wysoki", "niski", "barczysty", "szczuply", "umiesniony"});
    lm=({ "wysocy", "niscy", "barczysci", "szczupli", "umiesnieni"});
	lpa=({ "ascetyczny", "sedziwy", "postawny", "dostojny", "wyniosly"});
	lma=({ "ascetyczni", "sedziwi", "postawni", "dostojni", "wyniosli"});
	i = random(5);
	j = random(5);
	return ({ lp[i] , lm[i], lpa[j], lma[j] });
}
    void
    create_monster()
{
    string *przym = losowy_przym();
	ustaw_odmiane_rasy( ({"kaplan", "kaplana", "kaplanowi", "kaplana", "kaplanem", "kaplanie" }),
		                ({"kaplani", "kaplanow", "kaplanom", "kaplanow", "kaplanami", "kaplanach" }), PL_MESKI_OS);
    set_gender(G_MALE);
	dodaj_nazwy(PL_MEZCZYZNA);
    
	dodaj_przym( przym[0], przym[1] );
    dodaj_przym( przym[2], przym[3] );
    
	set_long("Jest to jeden z wielu kszatajacych sie po swiatyni, postawnych akolitow. "
	  + "Dawno temu wraz z powstaniem przybytku 'Piesn Poranka' oni objeli nad Beregostem "
	  + "opatrznosc. Ten ma nieco grozny lecz powazny wyraz twarzy, a co dziwne jego rysy sa "
	  + "lagodne, a skora biala niczym snieg. Jedyne co cie martwi to muskularna postawa "
	  + "owego kaplana, ktora jednoczesnie wskazuje jaka funkcje pelni w  tym domu modlitwy.\n");
    
	add_prop(LIVE_I_NEVERKNOWN, 1);
    add_prop(CONT_I_WEIGHT, 80000);
    add_prop(CONT_I_HEIGHT, 175);
    
	set_stats(({staty[0] + random(20), staty[1] + random(20), staty[2] + random(10), staty[3] + random(5), staty[4] + random(5), staty[5] + random(40)}));
    
	set_skill(SS_DEFENCE, umy[0] + random(10));
    set_skill(SS_PARRY, umy[1] + random(10));
    set_skill(SS_UNARM_COMBAT, umy[2] + random(10));
    
	set_cchat_time(20 + random(30));
    add_cchat("Napadnieto mnie!");
    add_cchat("Przeklety pomiocie. Zgin, przepadnij!");

	set_act_time(40 + random(20));
	add_act("emote spoglada na ciebie uwaznie.");
	add_act("emote przyglada sie malunkom.");
	add_act("emote kreci sie w kolko.");
	add_act("emote spoglada gdzies nieobecnym wzrokiem.");
	add_act("emote wznosi modly do Lathandera.");

	set_chat_time(55 + random(20));
	add_chat("W imie Lathandera zostalem akolita.");
	add_chat("'Piesn Poranka' to glowna swiatynia Lathandera na Wybrzezu Mieczy.");
	add_chat("Nie myslisz chyba, ze moja praca jest latwa?");

    add_armour(ODZIENIE_PATH + "szata1");
    add_armour(ODZIENIE_PATH + "sandaly");
    add_weapon(WEAPONS_PATH + "kij");

	set_default_answer(VBFC_ME("default_answer"));
}
void
attacked_by (object ob)
{
	object *obecni;

	obecni = all_inventory(environment(this_object())); 

	obecni = filter(obecni, f_filter); 

	obecni -= ({this_object()}); 

	obecni->command("krzyknij Napadnieto kaplana!");  

	obecni->attack_object(ob); 
}



int
f_filter(object ktory)
{
	if((ktory->query_rasa() == "gwardzista") || (ktory->query_rasa() == "straznik")) 
		return 1;                         
	return 0;                          
}
    string
	default_answer()
{
	command("emote zastanawia sie nad zagadnieniem.");
    set_alarm(3.0, 0.0, "zdziwienie");
}
    void
    emote_hook(string emote, object wykonujacy, string przyslowek)
{
    switch (emote)
    {
        case "kopnij": set_alarm(2.0, 0.0, "zly", wykonujacy);
        	    break;
        case "spoliczkuj" :
        case "szturchnij":
        case "opluj" : set_alarm(2.0, 0.0, "zly", wykonujacy);
        	    break;
        	    
        case "zasmiej": 
        case "pocaluj": set_alarm(2.0, 0.0, "dobry", wykonujacy);
        	      break;
        case "przytul": set_alarm(2.0, 0.0, "dobry", wykonujacy);
    }
}



void
zly(object kto)
{
    switch (random(3)) 
    {
        
    case 0 : command("krzyknij Glupcze! Gniew Bozy spadnie na ciebie!"); break;
    case 1 : command("powiedz do " + kto->short(PL_DOP) + " Nie nauczyli cie dobrych manier?"); break;
    case 2 : command("powiedz do " + kto->short(PL_DOP) + " Jak ty sie odnosisz w stosunku do kaplana?!"); break;
    default:
    }
}

void
dobry(object kto) 
{
        
    if(kto->koncowka("chlop","baba")=="chlop") zly(kto);
    else
      {
        
    
        switch (random(3)) 
        {
          case 0 : command("powiedz Przykro mi lecz nie moge kontynuowac tej przyjemnej chwili"); break;
          
        }
      }
} 
void
zdziwienie(object pla)
{
    command("powiedz do " + this_interactive()->short(PL_DOP) + " Niestety moja wiedza nie siega, az tak daleko.");
}

/*----Podpisano Volothamp Geddarm----*/



