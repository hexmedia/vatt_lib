

inherit "/std/monster";
#include <stdproperties.h>
#include <macros.h>
#include <ss_types.h>
#include <pl.h>
#define TP this_player()
#define TO this_object()
string *imie;

void
create_monster()
{

    imie=({"jane","jene","jane","jane","jane","jane"});
    ustaw_odmiane_rasy(PL_CZLOWIEK);
    set_gender(G_FEMALE);
    dodaj_nazwy(PL_KOBIETA);
    ustaw_imie(imie,PL_ZENSKI);
        dodaj_przym("piekna","piekni");
        dodaj_przym("cycata","cycaci");
    set_title("Kobieta Mistrza Simona");

	set_long("Masz przed soba piekna, przedstawicielke rasy elfiej. "+
	"Posiada ona dlugie kruczoczarne wlosy, ktore opadaja jej "+
	"swobodnie na ramiona. Jej twarz posiada lagodne, mlode rysy, "+
	"a jej ksztalty sprawiaja, ze nie mozesz od niej oderwac, "+
	"wzroku, na prawym ramieniu, zauwazasz siecacy tatuaz "+
	"przedstawiajacy, jakiegos czlowieka, po blizszym przyjrzeniu "+
	"sie poznajesz go jest nim Mistrz Simon.\n");
        


        add_prop(CONT_I_WEIGHT, 55000);
        add_prop(CONT_I_HEIGHT, 165);

        set_stats(({200,200,200 , 40, 40, 30 }));
        
        set_act_time(10);
        set_cchat_time(6);

        add_act("emote szepcze: Kochasiu ... nie chcialbys sie zabawic ?");
        add_act("emote pyta: Czy nie chcialbys aby ktos cie ogrzal ?");
        add_act("emote mowi: Ehh ... parszywa pogoda dzisiaj.");
        add_act("emote mowi: Chyba czas do domu.");
        add_act("emote usmiecha sie do ciebie zalotnie.");
        add_act("emote puszcza do ciebie zalotnie oczko.");
        add_act("emote kuszaco poprawia sukienke.");
	add_act("emote mowi: Wylizesz mi szparke malutki?\n");
        
        add_cchat("ZBOCZENIEC !!! RATUNKU !!!");
        add_cchat("Nie mysl sobie ze tak latwo ci ze mna pojdzie.");
        add_cchat("A moze sie inaczej zabawimy ??");
        
    set_alignment(100);

}
void
add_introduced(string imie_mia, string imie_bie)
{
    set_alarm(2.0, 0.0, "return_introduce", find_player(imie_mia));
}

void
return_introduce(object ob)
{

            command("przedstaw sie " + OB_NAME(ob));
            command("usmiechnij sie szeroko do "+OB_NAME(ob));
     


}


int
zgwalc( string str )
{
    if (str == "kobiete" || str == "jane")
	{
	if (TP->query_real_name()!="simon")
	{
	    TO->command("krzyknij Mistrzu Simonie gwalca mnie, pomoz mi.");
	    return 1;
	}
	if (TP->query_prop("players_sperm") >= 3 )
	    {
	    TO->command("powiedz Oj chlopczyku, twoja faja opadla, wiec jak "+
		"chcesz mnie wydymac?");
	    return 1;
	    }
	if (TP->query_gender() == 1 )
	    {
	    write("Hm, zdaje sie, ze jestes kobieta.\n");
	    return 1;
	    }
	
	TP->catch_msg("Lapiesz "+QIMIE(TO,PL_BIE)+", wywracasz "+
	"na lozko, po czym z rozkosza gwalcisz ja od tylca spogladajac "+
	"sobie na jej sliczny, zgrabny tyleczek. Nagle z twej armaty "+
	"wystrzeliwuje strumien gesty i mleczny jak mleko, "+
	"a ty jeczysz rozkosznie delektujac sie ta cudowna chwila.\n");
		
	tell_room(environment(TP),""+QCIMIE(TP,PL_MIA)+" rzuca sie "+
	"na "+QIMIE(TO,PL_BIE)+", wywraca na lozko, a po chwili "+
	"zaczyna ja intensywnie posuwac... Slyszysz jek rozkoszy, "+
	"a zadowolony wstaje z usmiechem na twarzy.\n",({TP}));
	
	TP->heal_hp(50);
	TP->add_prop("players_sperm",TP->query_prop("players_sperm")+1);
	return 1;	
	}


write("Co?\n");
return 1;
}




void
init_living()
{
::init_living();
    
add_action("zgwalc","zgwalc");    

}