#pragma strict types

inherit "/std/monster";

#include <macros.h>
#include <stdproperties.h>
#include <language.h>
#include <composite.h>
#include <money.h>
#include <pl.h>
#include <time.h>
#include <formulas.h>
#include <std.h>
#include "../definicje.h"
#include <filter_funs.h>

int *umy = ({40, 40, 40});
int f_filter(object ktory);

void
create_monster()
{
    ustaw_imie(({ "oddi", "oddiego", "oddiemu", "oddiego", "oddim", "oddim"}), PL_MESKI_OS);
	
	dodaj_nazwy(PL_MEZCZYZNA);
    
	set_title("Lovenhaupt");
    
	dodaj_przym("maly","mali");
    dodaj_przym("uprzejmy","uprzejmi");
    
	ustaw_odmiane_rasy( ({"niziolek","niziolka","niziolkowi","niziolka","niziolkiem","niziolku"}),
		                ({"niziolki","niziolkow","niziolkom","niziolkow","niziolkami","niziolkach"}), PL_MESKI_OS);
    
	set_stats( ({ 42, 43, 45, 41, 41, 41 }) );
    
	set_gender(G_MALE);
    
	add_prop(CONT_I_WEIGHT, 39000); 
    add_prop(CONT_I_VOLUME, 35000); 
    add_prop(CONT_I_HEIGHT, 135);
    
	set_act_time(360);
    add_act("emote rozglada sie po sklepiku.");
    add_act("usmiechnij sie wesolo");
    add_act("emote smieje z jakiegos starego dowcipu.");
    add_act("emote przeglada swoje towary.");

    set_chat_time(300);
    add_chat("Mo-mo-moze zechcesz przejrzec moje towary?");
	add_chat("Rze-rze-rzeczy jakie tu sprzedaje sprowadzam z Gullykin.");
	add_chat("Chcia-chcialbys cos sprzedac?");
    
	set_cchat_time(60);
    add_cchat("Pomocy!");
    add_cchat("Przestan prosze!");
	add_cchat("Tylko nie po twarzy... Prosze!");

	set_cact_time(10);
	add_cact("emote kuli sie w rogu.");
	add_cact("emote zaslania twarz.");

	set_default_answer(VBFC_ME("default_answer"));
  
    set_long("Jest to maly niziolek o trefnej lecz nieco przestraszonej twarzyczce. "
	  + "Szybkim wzrokiem rozglada sie po nalezacym do niego sklepiku, zas jego "
	  + "spojrzenie z reguly spoczywa na ewentualnych klientach. Wyglada on na "
	  + "osobe nieco nerwowa i strachliwa lecz w przypadku przedstawicieli tej rasy "
	  + "pozory naprawde moga zmylic czlowieka. Mimo to jest on zapewne bardzo mily "
	  + "i uprzejmy w stosunku do przyjezdnych i nowych twarzy w miasteczku. Zapewne "
	  + "za zgoda burmistrza ma on gdzies w okolicy swa norke ale co do tego nikt "
	  + "nie moze byc stuprocentowo pewnym.\n");

	set_skill(SS_DEFENCE, umy[0] + random(40));
    set_skill(SS_PARRY, umy[1] + random(30));
    set_skill(SS_WEP_CLUB, umy[2] + random(20));
    set_liv_name("_sklepikarz_z_beregostu_");
        add_armour(ODZIENIE_PATH + "spodenki");
    add_armour(ODZIENIE_PATH + "berecik");
    add_armour(ODZIENIE_PATH + "buciki");
}
void
attacked_by (object ob)
{
	object *obecni;

	obecni = all_inventory(environment(this_object())); 

	obecni = filter(obecni, f_filter); 

	obecni -= ({this_object()}); 

	obecni->command("powiedz Zaatakowano Oddiego!");  

	obecni->attack_object(ob); 
}
int
f_filter(object ktory)
{
	if((ktory->query_rasa() == "gwardzista") || (ktory->query_rasa() == "straznik")) 
		return 1;                         
	return 0;                          
}
void
add_introduced(string imie_mia, string imie_bie)
{
	set_alarm(2.0, 0.0, "return_introduce", find_player(imie_mia));
}
void
return_introduce(object ob)
{
    command("przedstaw sie " + OB_NAME(ob));
    command("powiedz Wi-wi-witam psze " + this_player()->koncowka("pana", "pani") +".");
	set_alarm(3.0,0.0, "przedstawienie");
}
string
default_answer()
{
	command("powiedz Nic o tym mi nie-nie wiadomo.");
}
void
emote_hook(string emote, object wykonujacy, string przyslowek)
{
    switch (emote)
    {
        case "kopnij":
        case "spoliczkuj" :
        case "szturchnij":
        case "opluj" : set_alarm(2.0, 0.0, "zly", wykonujacy); break;
        	    
        case "zasmiej": 
        case "pocaluj":
        case "przytul": set_alarm(2.0, 0.0, "dobry", wykonujacy);
    }
}



void
zly(object kto)
{
    switch (random(2)) 
    {
		case 0 : command("powiedz Prze-przestan."); break;
		case 1 : command("powiedz Pro-pro-prosze...");
    default:
    }
}

void
dobry(object kto) 
{
        
    if(kto->koncowka("chlop","baba")=="chlop") zly(kto);
    else
    {   
        switch (random(1)) 
        {
          case 0 : command("emote pozostaje niewzruszony."); break;      
        }
      }
}
void
przedstawienie(object pla)
{
	command("powiedz To-to moj sklepik.");
	command("emote usmiecha sie dumnie.");
} 


