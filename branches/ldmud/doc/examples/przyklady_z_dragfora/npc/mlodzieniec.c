/*
 * Filename: mlodzieniec.c
 *
 * Zastosowanie: Npc do gospody w Beregoscie.
 * 
 * Copyright Volothamp Geddarm.
 *
 */
inherit "/std/monster.c";

#include <macros.h>
#include <stdproperties.h>
#include <language.h>
#include <composite.h>
#include <money.h>
#include <pl.h>
#include <time.h>
#include <formulas.h>
#include <std.h>
#include <mudtime.h>
#include "../definicje.h"

int *staty = ({10, 10, 10, 10, 10, 10});
int *umy = ({10, 10, 10});

int f_filter(object ktory);

    string *
    losowy_przym()
{
    string *lp;
    string *lm;
	string *lpa;
	string *lma;
    int i, j;
    lp=({ "wysoki", "blekitnooki", "wesoly", "elegancki"});
    lm=({ "wysocy", "blekitnoocy", "weseli", "eleganccy"});
	lpa=({ "dorodny", "przystojny", "szczuply", "jasnowlosy"});
	lma=({ "dorodni", "przystojni", "szczupli", "jasnowlosi"});
	i = random(4);
	j = random(4);
	return ({ lp[i] , lm[i], lpa[j], lma[j] });
}
    void
    create_monster()
{
    string *przym = losowy_przym();
	ustaw_odmiane_rasy( ({"mlodzieniec", "mlodzienca", "mlodziencowi", "mlodzienca", "mlodziencem", "mlodziencu" }),
		                ({"mlodziency", "mlodziencow", "mlodziencom", "mlodziencow", "mlodziencami", "mlodziencach" }), PL_MESKI_OS);
    set_gender(G_MALE);
	dodaj_nazwy(PL_MEZCZYZNA);
    
	dodaj_przym( przym[0], przym[1] );
    dodaj_przym( przym[2], przym[3] );
    
	set_long("Jest to mlody obywatel Beregostu, ktoremu w glowie sa jedynie zabawy, "
	  + "gospody, uczty i dziewki. Nie przejmuje sie on ni obowiazkami ni trudami "
	  + "zycia i korzysta w pelni nalezycie ze swego wieku, ktory tak szybko przemija. "
	  + "Ubrany jest on w elegancka, biala koszule, nieco ekstrawaganckie spodnie "
	  + "wykonane z czarnego materialu i subtelny beret. Wielu sporo by dalo by byc "
	  + "jeszcze raz tak beztroska osoba jak ten oto mlodzieniec.\n");
    
	add_prop(LIVE_I_NEVERKNOWN, 1);
    add_prop(CONT_I_WEIGHT, 80000);
    add_prop(CONT_I_HEIGHT, 170);
    
	set_stats(({staty[0] + random(20), staty[1] + random(20), staty[2] + random(10), staty[3] + random(5), staty[4] + random(5), staty[5] + random(40)}));
    
	set_skill(SS_DEFENCE, umy[0] + random(10));
    set_skill(SS_PARRY, umy[1] + random(10));
    set_skill(SS_UNARM_COMBAT, umy[2] + random(10));
    
	set_cchat_time(20 + random(30));
    add_cchat("Zaczynasz ze mna zaczynasz z innymi.");
    add_cchat("Szykuj sie glupcze.");

	set_cact_time(25 + random(30));
	add_cact("emote zaciska piesci gotujac je do dalszej walki.");
	add_cact("emote wzywa straz glosnym krzykiem.");

	set_act_time(40 + random(20));
	add_act("emote nuci pod nosem jakas marzycielska melodie.");
	add_act("emote klaszcze rytmicznie w takt granej muzyki.");
	add_act("emote puszcza subtelne oczko karczemnej dziewce.");

	set_chat_time(55 + random(20));
	add_chat("A slyszeliscie o starym Ernescie?! Ten to dopiero mial wstyd.");
	add_chat("Szynkarz! Kolejka wina dla mnie i dla moich chlopakow.");
	add_chat("Jutro znow ojciec zagoni mnie do roboty.");

    add_armour(ODZIENIE_PATH + "beret");
    add_armour(ODZIENIE_PATH + "spodnie");
    add_armour(ODZIENIE_PATH + "koszula");

	set_default_answer(VBFC_ME("default_answer"));
}
void
attacked_by (object ob)
{
	object *obecni;

	obecni = all_inventory(environment(this_object())); 

	obecni = filter(obecni, f_filter); 

	obecni -= ({this_object()}); 

	obecni->command("powiedz Pokazmy "+ this_player()->koncowka("mu", "jej") + ".");  

	obecni->attack_object(ob); 
}



int
f_filter(object ktory)
{
	if((ktory->query_rasa() == "mlodzieniec") || (ktory->query_rasa() == "straznik")) 
		return 1;                         
	return 0;                          
}
    string
	default_answer()
{
	command("emote masuje pobrodek w zamysleniu.");
    set_alarm(3.0, 0.0, "zdziwienie");
}
    void
    emote_hook(string emote, object wykonujacy, string przyslowek)
{
    switch (emote)
    {
        case "kopnij": set_alarm(2.0, 0.0, "zly", wykonujacy);
        	    break;
        case "spoliczkuj" :
        case "szturchnij":
        case "opluj" : set_alarm(2.0, 0.0, "zly", wykonujacy);
        	    break;
        	    
        case "zasmiej": 
        case "pocaluj": set_alarm(2.0, 0.0, "dobry", wykonujacy);
        	      break;
        case "przytul": set_alarm(2.0, 0.0, "dobry", wykonujacy);
    }
}



void
zly(object kto)
{
    switch (random(2)) 
    {
        
    case 0 : command("chrzaknij"); break;
    case 1 : command("powiedz do " + kto->short(PL_DOP) + " Glupcze."); break;
    default:
    }
}

void
dobry(object kto) 
{
        
    if(kto->koncowka("chlop","baba")=="chlop") zly(kto);
    else
      {
        
    
        switch (random(3)) 
        {
          case 0 : command("mrugnij"); break;
          
        }
      }
} 
void
zdziwienie(object pla)
{
    command("powiedz Nic mi o tym nie wiadomo.");
}

/*----Podpisano Volothamp Geddarm----*/



