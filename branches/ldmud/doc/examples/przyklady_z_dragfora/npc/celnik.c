/*
 * Filename: celnik.c
 *
 * Zastosowanie: Npc do skladu celnego w Beregoscie.
 * 
 * Copyright Volothamp Geddarm.
 *
 */
inherit "/std/monster.c";

#include <macros.h>
#include <stdproperties.h>
#include <language.h>
#include <composite.h>
#include <money.h>
#include <pl.h>
#include <time.h>
#include <formulas.h>
#include <std.h>
#include <mudtime.h>
#include "../definicje.h"

int *staty = ({10, 10, 10, 10, 10, 10});
int *umy = ({10, 10, 10});

int f_filter(object ktory);

    string *
    losowy_przym()
{
    string *lp;
    string *lm;
	string *lpa;
	string *lma;
    int i, j;
    lp=({ "wysoki", "niski", "barczysty", "szczuply", "umiesniony"});
    lm=({ "wysocy", "niscy", "barczysci", "szczupli", "umiesnieni"});
	lpa=({ "czarnobrody", "gestobrewy", "rudowlosy", "czarnooki", "ogorzaly"});
	lma=({ "czarnobrodzi", "gestobrewi", "rudowlosi", "czarnoocy", "ogorzali"});
	i = random(5);
	j = random(5);
	return ({ lp[i] , lm[i], lpa[j], lma[j] });
}
    void
    create_monster()
{
    string *przym = losowy_przym();
	ustaw_odmiane_rasy( ({"celnik", "celnika", "celnikowi", "celnika", "celnikiem", "celniku" }),
		                ({"celnicy", "celnikow", "celnikom", "celnikow", "celnikami", "celnikach" }), PL_MESKI_OS);
    set_gender(G_MALE);
	dodaj_nazwy(PL_MEZCZYZNA);
    
	dodaj_przym( przym[0], przym[1] );
    dodaj_przym( przym[2], przym[3] );
    
	set_long("Oto jeden z urzednikow, ktorzy sprawuja w miescie wladze celna. "
	  + "Ich glowne zajecie to kontrolowanie zawartosci kupieckich wozow, ktore "
	  + "przyjezdzaja do miasta, a nastepnie okladanie towaru odpowiednimi podatkami. "
	  + "Z reguly takie osoby przebywaja w zbudowanych przy bramach miast, skladach "
	  + "celnych. Ten wyglada na takiego, ktory ma jeszcze przed soba mase pracy "
	  + "zanim powroci do swojego domostwa. \n");
    
	add_prop(LIVE_I_NEVERKNOWN, 1);
    add_prop(CONT_I_WEIGHT, 80000);
    add_prop(CONT_I_HEIGHT, 175);
    
	set_stats(({staty[0] + random(20), staty[1] + random(20), staty[2] + random(10), staty[3] + random(5), staty[4] + random(5), staty[5] + random(40)}));
    
	set_skill(SS_DEFENCE, umy[0] + random(10));
    set_skill(SS_PARRY, umy[1] + random(10));
    set_skill(SS_UNARM_COMBAT, umy[2] + random(10));
    
	set_cchat_time(20 + random(30));
    add_cchat("Napasc na urzednika!");
    add_cchat("Przeklety pomiocie. Zgin, przepadnij!");

	set_act_time(40 + random(20));
	add_act("emote siada przy stole.");
	add_act("emote wstaje od stolu.");
	add_act("emote przeglada jakies papiery.");
	add_act("ziewnij.");

	set_chat_time(55 + random(20));
	add_chat("Ciezka praca celnika.");
	add_chat("Pobieranie cla to twardy kawalek chleba.");
	add_chat("Nie myslisz chyba, ze moja praca jest latwa?");

	add_armour("/d/Faerun/Miasta/beregost/odzienie/tunika1");
	add_armour("/d/Faerun/Miasta/beregost/odzienie/sandaly");

	set_default_answer(VBFC_ME("default_answer"));
}
void
attacked_by (object ob)
{
	object *obecni;

	obecni = all_inventory(environment(this_object())); 

	obecni = filter(obecni, f_filter); 

	obecni -= ({this_object()}); 

	obecni->command("krzyknij Napasc na urzednika!");  

	obecni->attack_object(ob); 
}



int
f_filter(object ktory)
{
	if((ktory->query_rasa() == "gwardzista") || (ktory->query_rasa() == "straznik")) 
		return 1;                         
	return 0;                          
}
    string
	default_answer()
{
	command("emote przyglada ci sie ze zdziwieniem.");
    set_alarm(3.0, 0.0, "zdziwienie");
}
    void
    emote_hook(string emote, object wykonujacy, string przyslowek)
{
    switch (emote)
    {
        case "kopnij": set_alarm(2.0, 0.0, "zly", wykonujacy);
        	    break;
        case "spoliczkuj" :
        case "szturchnij":
        case "opluj" : set_alarm(2.0, 0.0, "zly", wykonujacy);
        	    break;
        	    
        case "zasmiej": 
        case "pocaluj": set_alarm(2.0, 0.0, "dobry", wykonujacy);
        	      break;
        case "przytul": set_alarm(2.0, 0.0, "dobry", wykonujacy);
    }
}



void
zly(object kto)
{
    switch (random(5)) 
    {
        
    case 0 : command("krzyknij Co to za zachowanie?!"); break;
    case 1 : command("powiedz do " + kto->short(PL_DOP) + " Nie nauczyli cie dobrych manier?"); break;
    case 2 : command("powiedz do " + kto->short(PL_DOP) + " Uspokoj sie lepiej."); break;
    default:
    }
}

void
dobry(object kto) 
{
        
    if(kto->koncowka("chlop","baba")=="chlop") zly(kto);
    else
      {
        
    
        switch (random(3)) 
        {
          case 0 : command("powiedz Moglabys to powtorzyc?"); break;
          
        }
      }
} 
void
zdziwienie(object pla)
{
    command("powiedz Nie mam pojecia o co ci chodzi.");
}

/*----Podpisano Volothamp Geddarm----*/



