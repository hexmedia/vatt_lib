/*----Ribald Kramarz----*/
/*----Copyright Volo----*/
/*----Sklepikarz na TP--*/
/* Simon Quest */
#pragma strict types

inherit "/std/monster";
inherit "/d/Faerun/std/questy";
#include <macros.h>
#include <stdproperties.h>
#include <language.h>
#include <composite.h>
#include <money.h>
#include <pl.h>
#include <time.h>
#include <formulas.h>
#include <std.h>
#include "../definicje.h"
#include <ss_types.h>
#define PLAY(a) lower_case(a->query_name(PL_DOP))
int f_filter(object ktory);
int zajety;
void
create_monster()
{
    ustaw_imie(({ "marius", "mariusa", "mariusowi", "mariusa", "mariusem", "mariusie"}), PL_MESKI_OS);
	dodaj_nazwy(PL_MEZCZYZNA);
    set_title("Wilfen, emerytowany wilk morski");
    dodaj_przym("siwowlosy","siwowlosi");
    dodaj_przym("barczysty","barczysci");
    ustaw_odmiane_rasy(PL_MEZCZYZNA);
    set_stats( ({ 41, 42, 51, 31, 32, 51 }) );
    set_gender(G_MALE);
    add_prop(CONT_I_WEIGHT, 78000);
    add_prop(CONT_I_VOLUME, 35000);
    add_prop(CONT_I_HEIGHT, 188);
    set_act_time(120);
    add_act ("rozejrzyj sie czujnie");
    add_act ("emote popija rumu z malej szklaneczki.");
    add_act ("emote patrzy na ciebie uwaznie.");
    add_act ("rozejrzyj sie dookola uwaznie");
    add_act ("emote przypomina sobie jakas niebezpieczna przygode.");
    add_act ("emote wspomina stare lata spedzone na pokladach okretow.");
    add_act ("emote podspiewuje jakas stara szante.");
    add_act ("ziewnij");
    add_act ("namysl sie nad podrozami");
	add_act ("krzyknij Hej spokoj tam chlopcy!");
	add_act ("krzyknij Hej spokoj tam chlopcy!");
	add_act ("krzyknij Hej spokoj tam chlopcy!");
    set_chat_time(180);
    add_chat("Tak... Pamietam jeszcze te stare przygody.");
    add_chat("Jesli nudzisz sie na ladzie to wyruszaj w morze.");
    add_chat("Napij sie rumu czy piwska dobrego, a potem zaciagnij sie na jakis rejs.");
    add_chat("Polecam nasz okretowy gin. Dobrze wplywa na trawienie.");
    set_cchat_time(10);
    add_cchat("Juz ja cie naucze moresu szczurze ladowy!");
    add_cchat("Miernota gorsza od niczego!");
	set_default_answer(VBFC_ME("default_answer"));


    set_long("Przygladasz sie wlasnie miejscowemu szynkarzowi. Jest to mezczyzna o ogorzalej skorze "
	  + "oraz szerokich barach i duzym wzroscie. Ubrany jest w czarna kamizelke, brazowe spodnie, pare "
	  + "okutych butow, a takze szeroki pas marynarski. Jest to prawdopodobnie jeden z tych slynnych "
	  + "emerytowanych wilkow morskich, ktorzy zamiast do konca swych dni plywac po morzach postanawiaja "
	  + "zalozyc wlasna tawerne w jednym z kupieckich miast i tam dozywac szczesliwej starosci. Zapewne "
	  + "zna on wiele niezwyklych opowiesci o piratach, morskich potworach i porywistych sztormach, a w jego "
	  + "blekitnych oczach dostrzegasz bystrosc orla wypatrujacego z daleka swej ofiary, ktora juz zaraz "
	  + "wpadnie w jego szpony. \n");
	set_skill(SS_DEFENCE, 15 + random(10));
    set_skill(SS_UNARM_COMBAT, 40 + random(10));
    set_skill(SS_PARRY, 10 + random(10));
    remove_prop(NPC_M_NO_ACCEPT_GIVE);
    add_ask(({"wymiar"}),VBFC_ME("historia_wymiar"));
    add_ask(({"rodzine", "historie", "skrzynke"}),VBFC_ME("his"));
}
    void
    add_introduced(string imie_mia, string imie_bie)
{
	set_alarm(2.0, 0.0, "return_introduce", find_player(imie_mia));
}
    void
    return_introduce(object ob)
{
    command("przedstaw sie " + OB_NAME(ob));
    command("uklon sie " + OB_NAME(ob));
}
    string
	default_answer()
{
	command("powiedz Czego sie mnie czepiasz?! Nie zawracaj mi teraz glowy!");
}
    void
    emote_hook(string emote, object wykonujacy, string przyslowek)
{
    switch (emote)
    {
        case "kopnij": set_alarm(2.0, 0.0, "zly", wykonujacy);
        	    break;
        case "spoliczkuj" :
        case "szturchnij":
        case "opluj" : set_alarm(2.0, 0.0, "zly", wykonujacy);
        	    break;

        case "zasmiej":
        case "pocaluj": set_alarm(2.0, 0.0, "dobry", wykonujacy);
        	      break;
        case "przytul": set_alarm(2.0, 0.0, "dobry", wykonujacy);
    }
}



void
zly(object kto)
{
    switch (random(5))
    {

    case 0 : command("powiedz Jeszcze raz a przetrace ci kark szczurze ladowy."); break;
    case 1 : command("kopnij " + OB_NAME(kto)); break;
    case 2 : command("spoliczkuj " + OB_NAME(kto)); break;
    default:
    }
}

void
dobry(object kto)
{

    if(kto->koncowka("chlop","baba")=="chlop") zly(kto);
    else
      {


        switch (random(3))
        {
          case 0 : command("powiedz Zobaczymy sie jescze pozniej."); break;
          case 1 : command("powiedz No, no malenka. Nie przesadzaja z tymi czuloscami."); break;
          case 2 : command("powiedz Gdybys kiedys wlazla mi do lozka byloby mi duzo cieplej."); break;

        }
      }
}
/*Funkcja wywolywana gdy ktos zaatakuje berserkera*/

void
attacked_by (object ob)
{
	object *obecni;

	obecni = all_inventory(environment(this_object()));  //Tu dostajemy cala zawartosc pomieszczenia;

	obecni = filter(obecni, f_filter); //Tu filtrujemy tylko mariusow

	obecni -= ({this_object()}); //Ze wszystkich mariusow usuwamy tego ktory zostal zaatakowany (juz walczy)

	obecni->command("powiedz Kto bije naszego gospodarza ma do czynienia z nami!");   //Tekst jaki krzycza zeglarze gdy pomagaja koledze

	obecni->attack_object(ob); //Reszta atakuje napastnika
}

/*Funkcja filtrujaca karczmarzy*/

int
f_filter(object ktory)
{
	if((ktory->query_rasa() == "zeglarz") || (ktory->query_rasa() == "marynarz")) //Jesli obiekt ktory sprawdzamy to berserker
		return 1;                          //<------------ To zwacamy 1
	return 0;                              //<------------ W przeciwnym wypadku zwaracamy 0
}
void
enter_inv(object ob,object from)
{

   ::enter_inv(ob,from);
   if(ob->skrzynka_do_questa()==1)
   {
   command("powiedz A coz to mi dajesz?");

   if(query_quest_skonczony(from,"Skrzynka_Czarna_Crimmor"))
   {
      command("powiedz No nie wiem. Dziekuje ci bardzo.");
      command("usmiechnij sie krzywo");
   }
   else
   {
      zajety=1;
      command("usmiechnij sie szeroko.");
      command("powiedz Wiedzialem, ze ten dzien nadejdzie! Oto jest skrzynka mojego pradziada.");
      set_alarm(5.0,0.0,"catch_quest",-1,from);
   }
   }
   else
   {
   if(living(from))
   {
   command("powiedz To na nic mi sie nie przyda.");
   command("daj "+lower_case(ob->query_name(PL_BIE))+" "+lower_case(from->query_name(PL_CEL)));
   }
   }
}


/*----Podpisano: 'Volothamp Geddarm'----*/

void
catch_quest(int faza,object pla)
{

    if (!present(pla, environment(this_object())))
    {
	command("powiedz No i poszedl sobie, wielka szkoda.");
	command("usmiechnij sie smutno");
	zajety=0;
	return;
    }
    switch(++faza)
    {
    case 0:
        command("emote wspomina swojego pradziada");
        set_alarm(5.0, 0.0, "catch_quest", faza,pla);
    break;
    case 1:
        command("powiedz To jest to. Stokrotne dzieki poslancu bogow!");
        set_alarm(4.0, 0.0, "catch_quest", faza,pla);
    break;
    case 2:
        command("powiedz No ale nie bede ci zanudzal opowiesciami o mojej rodzinie...");
        command("powiedz Kiedys jak bedziesz chcial"+pla->kocowka("","a","o")+" przyjdz i zapytaj sie mnie o ta historie...");
        set_alarm(5.0, 0.0, "catch_quest", faza,pla);
    break;
    case 3:
           command("powiedz Nie mam wiele. Ta tawerna nie przynosi wielkich zyskow. Lecz moge ci ofiarowac...");
           command("powiedz To.");
           MONEY_MAKE_GC(5)->move(this_object());
           command("daj monety "+lower_case(pla->query_name(PL_CEL)));
           nagrodz_gracza("Skrzynka_Czarna_Crimmor",pla);
           pla->catch_msg("Czujesz sie ze przybywa ci nieco doswiadczenia.\n");
           zajety=0;
    break;
    }

}

int
his()
{
    object for_who;
    for_who=this_player();

    if(zajety==1)
    {
	        command("powiedz Poczekaj chwilke daj mi skonczyc.");
	        return 1;
    }
    if(for_who->query_prop("_player_i_spytal_o_historie_mariusa")==1)
    {
        command("powiedz do " + PLAY(for_who) + " O tym juz ci opowiadalem ale jak chcesz moge ci conieco o tym wymiarze opowiedziec.");
        return 1;
    }
    if((query_quest_skonczony(for_who,"Skrzynka_Czarna_Crimmor")==0)||(query_quest_skonczony(for_who,"Skrzynka_Czarna_Crimmor_Cz2")==1))
    {
    	    command("powiedz do " + PLAY(for_who) + " Nie mam powodu aby ci o tym opowiadac.");
            return 1;
    }
    else
    {
        zajety=1;
        command("powiedz do " + PLAY(for_who) + " A wiec jednak zdecydowales sie abym ci opowiedzial...");
        set_alarm(5.0,0.0,"catch_quest2",-1,for_who);
    }
}

void
catch_quest2(int faza,object pl)
{
   if (!present(pl, environment(this_object())))
    {
	command("powiedz No i poszedl sobie, wielka szkoda.");
	command("usmiechnij sie smutno");
	zajety=0;
	return;
    }
    switch(++faza)
    {
		case 0:
		    command("powiedz do " + PLAY(pl) + " Zatem spocznij spokojnie, bo nie jest to krotka opowiesc.");
		    set_alarm(7.0,0.0,"catch_quest2",faza,pl);
		break;
        case 1:
            command("emote namysla sie");
            command("powiedz do " + PLAY(pl) + " Moj pradziad Alex Wilfen byl podroznikiem i poszukiwaczem skrabow....");
            set_alarm(7.0,0.0,"catch_quest2",faza,pl);
        break;
        case 2:
            command("powiedz do " + PLAY(pl) + " Podczas jednej z wypraw w krainach wiecznego mrozu, natknal sie na stare ruiny...");
            set_alarm(7.0,0.0,"catch_quest2",faza,pl);
        break;
        case 3:
            command("powiedz do " + PLAY(pl) + " To wlasnie tam znalazl owa przekleta czarna skrzyneczke.");
            command("emote spoglada gdzies ze strachem.");
            set_alarm(7.0,0.0,"catch_quest2",faza,pl);
        break;
        case 4:
            command("powiedz do " + PLAY(pl) + " Niczego nieswiadomy zabral ja ze soba i wrocil do najblizszego miasta.");
            command("emote namysla sie nad glupota swego pradziada.");
            set_alarm(7.0,0.0,"catch_quest2",faza,pl);
        break;
        case 5:
            command("powiedz do " + PLAY(pl) + " W nocy w jego pokoiku w jednej z karczm pojawil sie mag!");
            command("zblednij");
            set_alarm(7.0,0.0,"catch_quest2",faza,pl);
        break;
        case 6:
            command("powiedz do " + PLAY(pl) + " Owy mag jak sie okazalo byl wlascicielem tej skrzyneczki.");
            set_alarm(7.0,0.0,"catch_quest2",faza,pl);
        break;
        case 7:
            command("powiedz do " + PLAY(pl) + " Nie chcial on zupelnie sluchac tlumaczen mego pradziada...");
            set_alarm(7.0,0.0,"catch_quest2",faza,pl);
        break;
        case 8:
            command("powiedz do " + PLAY(pl) + " Powiedzial tak: 'Wilfenie ty ktory osmieliles sie przywlaszczyc moja wlasnosc poniesiesz kare...'");
            set_alarm(7.0,0.0,"catch_quest2",faza,pl);
        break;
        case 9:
            command("powiedz do " + PLAY(pl) + " 'Rzucam na ciebie i na twoj rod klatwe. Jesli ty lub ktorys z czlonkow twej rodziny umrzecie wasze "+
                    " dzusze nie pojda w zaswiaty lecz do wymiaru stworzonego przeze mnie...'");
		    set_alarm(7.0,0.0,"catch_quest2",faza,pl);
        break;
        case 10:
            command("powiedz do " + PLAY(pl) + " 'Beda one skazane na wiecznie cierpienie, dopoki ktos nie odwazy sie ich uwolnic.'");
            set_alarm(7.0,0.0,"catch_quest2",faza,pl);
        break;
        case 11:
            command("powiedz do " + PLAY(pl) + " 'Wiedz jednak Wilfenie ze nie bedzie to latwe, gdyz aby je uwolnic trzeba pokonac straznika.'");
            command("emote wzdycha ze smutkiem.");
            set_alarm(7.0,0.0,"catch_quest2",faza,pl);
        break;
        case 12:
            command("powiedz do " + PLAY(pl) + " Powiedzial mu takze w jaki sposob moze sie dostac do owego wymiaru. Jednak nie przekazal mu "+
            "niczego wiecej o strazniku oraz o tym co moze go spotkac tam. Potem znikl i juz nigdy sie nie pokazal.");
            set_alarm(7.0,0.0,"catch_quest2",faza,pl);
        break;
        case 13:
           command("powiedz do " + PLAY(pl) + " Skrzyneczka oraz informacje o klatwie byly przekazywane z pokolenia na "+
           " pokolenie, moj dziadek i moj ojciec oraz pradziad zgineli probojac uwolnic nas z tej klatwy...");
           set_alarm(7.0,0.0,"catch_quest2",faza,pl);
        break;
        case 14:
            command("powiedz do " + PLAY(pl) + " Teraz pewnie ich dusze sa tam uwiezione.");
            command("usmiech smutno");
            set_alarm(7.0,0.0,"catch_quest2",faza,pl);
        break;
        case 15:
           command("powiedz do " + PLAY(pl) + " Niestety ja nigdy nie mialem odwagi na taki czyn, a teraz jestem za stary na to.");
           set_alarm(7.0,0.0,"catch_quest2",faza,pl);
        break;
        case 16:
            command("powiedz do " + PLAY(pl) + " W dodatku zgubilem ta skrzyneczke pare lat temu podczas jednej z moich wypraw, dopuki ty mi jej"+
           " nie zwrocil"+pl->koncowka("es","as","es")+".");
            set_alarm(7.0,0.0,"catch_quest2",faza,pl);
        break;
        case 17:
           command("emote namysla sie.");
           command("powiedz do " + PLAY(pl) + " Moze mogl"+pl->koncowka("bys","abys","obys")+" mi pomoc ?");
           pl->add_prop("_player_i_spytal_o_historie_mariusa",1);
           zajety=0;
        break;
    }
}
int
historia_wymiar()
{
    object for_who;
    for_who=this_player();

    if(zajety==1)
    {
	        command("powiedz Poczekaj chwilke daj mi skonczyc.");
	        return 1;
    }
    if(for_who->query_prop("_player_i_spytal_o_historie_mariusa")==0)
    {
        command("powiedz do " + PLAY(for_who) + " Nie mam powodu aby ci odpowiadac na to pytanie.");
        return 1;
    }
    if((query_quest_skonczony(for_who,"Skrzynka_Czarna_Crimmor")==0)||(query_quest_skonczony(for_who,"Skrzynka_Czarna_Crimmor_Cz2")==1))
    {
    	    command("powiedz do " + PLAY(for_who) + " Nie mam powodu aby ci o tym opowiadac.");
            return 1;
    }
    else
    {
        zajety=1;
        command("powiedz do " + PLAY(for_who) + " Tak wymiar, wiem jedynie jak mozna sie do niego dostac.");
        command("powiedz do " + PLAY(for_who) + " Zatem spocznij spokojnie, bo nie jest to krotka opowiesc.");
        //set_alarm(5.0,0.0,"catch_quest2",-1,from);
    }
}
