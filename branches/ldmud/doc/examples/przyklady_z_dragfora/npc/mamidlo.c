#pragma strict types

inherit "/std/monster";

#include <macros.h>
#include <stdproperties.h>
#include <language.h>
#include <composite.h>
#include <money.h>
#include <pl.h>
#include <time.h>
#include <formulas.h>
#include <std.h>
#include "../definicje.h"
#include <filter_funs.h>

int *umy = ({10, 10, 10});
object *przyzwani = ({});

void
create_monster()
{
    ustaw_imie(({ "orlen", "orlena", "orlenowi", "orlena", "orlenem", "orlenie"}), PL_MESKI_OS);
	
	dodaj_nazwy(PL_MEZCZYZNA);
    
	set_title("Der, Czarnoksieznik z Waterdeep");
    
	dodaj_przym("bardzo stary","bardzo starzy");
    dodaj_przym("siwobrody","siwobrodzi");
    
	ustaw_odmiane_rasy(PL_MEZCZYZNA);
    
	set_stats( ({ 42, 43, 41, 100, 100, 99 }) );
    
	set_gender(G_MALE);
    
	add_prop(CONT_I_WEIGHT, 59000); 
    add_prop(CONT_I_VOLUME, 35000); 
    add_prop(CONT_I_HEIGHT, 175);
	add_prop(LIVE_I_NO_CORPSE, 1);

	set_act_time(180);

	set_default_answer(VBFC_ME("default_answer"));
  
    set_long("Jest to bardzo stary juz mezczyzna, ktory prawdopoobnie wiele "
	  + "przezyl podczas swego zycia. Na pierwszy rzut oka przypomina on wiekowego "
	  + "maga o dlugiej siwej brodzie i przyodzianego w prawdziwie czarodziejskie "
	  + "odzienie jakim jest spiczasty kapelusz i blekitna szata. Laska, ktora trzyma "
	  + "oburacz posiada plonaca koncowka i z cala pewnoscia nie jest ona zwyklym kijem, "
	  + "podobnie zreszta jak osobnik, ktorego obserwujesz nie jest zwyklym starcem acz "
	  + "zapewne bardzo poteznym i uzdolnionym magicznie bytem o wielkim doswiadczeniu "
	  + "zyciowym i jeszcze wiekszej, lecz moze slusznie, pewnosci siebie.\n");

	set_skill(SS_DEFENCE, umy[0] + random(40));
    set_skill(SS_PARRY, umy[1] + random(30));
    set_skill(SS_WEP_POLEARM, umy[2] + random(20));

    add_armour(ODZIENIE_PATH + "szata");
    add_armour(ODZIENIE_PATH + "sandaly");
    add_armour(ODZIENIE_PATH + "kapelusz");
    add_weapon(WEAPONS_PATH + "laska");
}

void
add_introduced(string imie_mia, string imie_bie)
{
	set_alarm(2.0, 0.0, "return_introduce", find_player(imie_mia));
}
void
return_introduce(object ob)
{
    command("przedstaw sie " + OB_NAME(ob));
    command("uklon sie " + OB_NAME(ob) + " z godnoscia");
}

string
default_answer()
{
	set_alarm(2.0,0.0, "pytanie", this_interactive());
}
void
pytanie(object pla)
{
	command("powiedz do " + pla->short(PL_DOP) + " Nie zawarcaj mi glowy glupia, smiertelna istoto!");
}

void
emote_hook(string emote, object wykonujacy, string przyslowek)
{
    switch (emote)
    {
        case "kopnij":
        case "spoliczkuj" :
        case "szturchnij":
        case "opluj" : set_alarm(2.0, 0.0, "zly", wykonujacy); break;
        	    
        case "zasmiej": 
        case "pocaluj":
        case "przytul": set_alarm(2.0, 0.0, "dobry", wykonujacy);
    }
}


void
zly(object kto)
{
    switch (random(2)) 
    {
		case 0 : command("krzyknij Precz z moich oczu!"); 
					break;
		case 1 : command("krzyknij Odejdz stad glupcze pokim jeszcze dobry!");
					break;
    default:
    }
}

void
dobry(object kto) 
{
        
    if(kto->koncowka("chlop","baba")=="chlop") zly(kto);
    else
    {   
        switch (random(2)) 
        {
          case 0 : command("powiedz Odczep sie ode mnie!"); break;
          case 1 : command("powiedz Precz z moich oczu!"); break;          
        }
      }
} 

public int
special_attack(object victim)
{
int szansa = random(100);
object nowy;

		if(szansa <= 22)
		{	
			write("Czarnoksieznik kieruje w twoja strone swa reke, a z jego dlonie wylatuje strzala raniac cie bolesnie.\n");
			say("Czarnoksieznik kieruje swa reke w strone " + victim->query_name(PL_BIE) + ", a z jego dlonie wylatuje strzala raniac "+ victim->koncowka("go", "ja") +" bolesnie.\n");
		}
		if((szansa >= 23) && (szansa <= 46))
		{
			write("Czarnoksieznik sklada rece wykonujac tajemniczy gest, a spomiedzy jego dloni wyplywaja kule czystej energii uderzajac w twe cialo.\n");
			say("Nagly blysk i jeki " + victim->query_name(PL_BIE) + " swiadcza o bolu doznanym przy zderzeniu z kulami magicznej mocy.\n");
		}
		if((szansa >= 47) && (szansa <= 71))
		{
			write("Spomiedzy rak czarownika wydobywaja sie z naglym sykiem plomienie parzac cie dotkliwie.\n");
			say("Zapach palonego ciala dochodzacy od strony " + victim->query_name(PL_BIE) + " jest najwyrazniej efektem poparzenia przez czar " + this_object()->query_name(PL_BIE) + ".\n");
		}
		else
		{
			write("Mag kieruje w twa strone swe rece, wysylajac forumjac dlonmi stozek wyjatkowego zimna.\n");
			say("Lodowaty powiew skupiajcay sie na " + victim->query_name(PL_NAR) + " jest oznaka rzuconego " + this_player()->koncowka("nan", "na nia") + " zaklecia.\n");
		}
	return 1;
}

void
remove_object()
{
	write(this_object()->query_name() + " rozplywa sie z sykiem.\n");
	say(this_object()->query_name() + " rozplywa sie z sykiem.\n");
	all_inventory(this_object())->remove_object();
	::remove_object();
}

