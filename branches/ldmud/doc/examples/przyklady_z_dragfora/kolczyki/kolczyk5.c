#include <pl.h>
#include "/sys/stdproperties.h"
inherit "/d/Faerun/std/kolczyk_std.c";

void
create_kolczyk()
{
set_long("Kolczyk ten to pozlacana obraczka, spieta misternym "+
	"zapieciem, do ktorej przymocowano polokragla, szklana "+
	"kopulke. Jest ona przezroczysta, choc lekko zamglona, lecz "+
	"pod wplywem padajacego swiatla rozjarza sie wszystkimi "+
	"kolorami teczy.\n");
dodaj_przym("szklany","szkalni");
dodaj_przym("okragly","okragli");

    add_prop(OBJ_I_VALUE, 200);
    add_prop(OBJ_I_VOLUME, 17);
    add_prop(OBJ_I_WEIGHT, 13);

}
