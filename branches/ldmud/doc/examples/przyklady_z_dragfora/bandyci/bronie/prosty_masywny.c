inherit "/d/std/bron";

#include <stdproperties.h>
#include <wa_types.h>
#include <pl.h>
#include <macros.h>
#include <formulas.h>
void
create_weapon() 
{
     set_autoload();
     ustaw_nazwe(({ "miecz", "miecza", "mieczowi", "miecz", "mieczem", "mieczu" }),
                 ({ "miecze", "mieczy", "mieczom", "miecze", "mieczami", 
                    "mieczach" }), PL_MESKI_NOS_NZYW);
                    
     dodaj_przym( "prosty", "prosci" );
     dodaj_przym( "masywny","masywni");
                         
     set_long("To zwykly prosty dlugi miecz. Jest dosc masywny, wiec potrzeba "+
"sily do wladania nim, ale dzieki temu moze bardziej zranic wroga. "+
"Zostal wykonany przez kowala od niedawna zajmujacego sie robieniem mieczy. "+
"Widziales juz duzo gorsze miecze, ale ten tez pozostawia wiele do zyczenia. "+
"Jest przystosowany do walki jedna reka.\n");


     set_hit(20);
     set_pen(25);

     set_wt(W_SWORD);
     set_dt(W_SLASH);

     set_hands(W_ANYH);
     
     add_prop(OBJ_I_VALUE, F_VALUE_WEAPON(query_hit(), query_pen()));    
     add_prop(OBJ_I_WEIGHT, 5500);
     add_prop(OBJ_I_VOLUME, query_prop(OBJ_I_WEIGHT)/5);
 }