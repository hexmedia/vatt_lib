#include <pl.h>

inherit "/d/Faerun/std/dylizanse/dylizans.c";

create_dylizans()
{

    ustaw_nazwe(        ({"dylizans","dylizansu","dylizansowi","dylizans","dylizansem","dylizansie"}),
        ({"dylizansy","dylizansow","dylizansom","dylizansy","dylizansami","dylizansach"}),
        PL_MESKI_NOS_NZYW); // Nazwa "dylizans".

	dodaj_przym("czarny","czarni");
    
    ustaw_trase(({"--", "s", "brama|open_door:3", "s", "**", "se", "**", "s", "**", "se", "**", "e", "**", 
                  "s", "**", "brama|open_door:3", "e", "e", "e", "e", "e", "e","--"})); // Trasa Ber - Crimm.
                  
    ustaw_inversje(1); // Funkcja umozliwiajaca droge powrotna po trasie symetrycznej do danej...
    
    ustaw_cene(300); // Cena przejazdu to trzy zlocisze.
    
     ustaw_wnetrze_obj("/d/Faerun/Miasta/beregost/dylizans/wnetrze.c"); 
    
    ustaw_czas_jazdy(3); // Czas pobytu dylizansu na jednej lokacji w czasie jazdy.
    
    ustaw_czas_postoju(30); // Czas postoju.
    
    set_long("Jest to dosyc maly dylizans, zaprzegniety w dwa kare, dostojne ogiery.\n"); // Long...

}
