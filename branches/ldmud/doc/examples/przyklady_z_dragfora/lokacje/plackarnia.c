#pragma strict_types
inherit "/d/std/room";
inherit "lib/pub";
#include <stdproperties.h>
#include <macros.h>
public int no_command();
public int no_command2();
void
create_room()
{	
    set_short("Plackarnia");
    set_long("Pierwsze co od razu  po przyjsciu tutaj zwraca twoja "+
     "uwage to piekny intensywny zapach wypiekow. Twe nozdrza wyczowaja "+
     "swieze drozdze i owoce.. W kolo ciebie kreci sie kilku pacholkow "+
     "calych pokrytych maka, ktorzy bez przerwy chodza i dogladaja "+
     "swych wyrobow. Widzac co poniektore placki naprawde cieknie "+
     "ci slinka. Na scianie wisi jakas tabliczka.\n");
    add_prop(ROOM_I_INSIDE,1);
    add_cmd_item(({"tabliczke","menu"}),({"czytaj","przeczytaj"}),
		   "@@menu");
    add_item("stol","Jest to ogragly stolik wykonany z drewna, stoi przy nim kilka krzesel "+
    "na ktorych mozna usiasc.\n");
       
    add_drink("mleka","mleko","szklanke bialego mleka","",0,0,250,25,25,
    PL_NIJAKI_NOS);
    
    add_food("placek z jagodami",0,700,60,PL_MESKI_NOS_ZYW,
	     "wysmienitego, okraglego, nadziewanego jagodami placka. ");
    
    add_food("placek z jablkami",0,700,55,PL_MESKI_NOS_ZYW,
	     "duzego okraglego placka z jablkowym nadzieniem. ");
    
    add_food("placek z truskawkami",0,700,60,PL_MESKI_NOS_ZYW,
	     "przepysznego placka z czerwonym, truskawkowym nadzieniem. ");
    
	add_food("placek z kruszonka",0,600,30,PL_MESKI_NOS_ZYW,
	     "smacznego, okraglego placka posypanego slodka kruszonka. ");
      
             set_event_time(65,this_object());
    add_event("Wyczuwasz unoszacy sie w powietrzu zapach jeszcze goracych plackow.\n");
    add_event("Jakies dziecko spoglada na ciebie przez okno.\n");
    add_event("Tuz obok ciebie przeszedl plackarz trzymajacy swiezego placka\n");
    add_event("Na widok jak plackarz ugniata ciasto cieknie ci slinka.\n");                               
    		         	      	          	     
    add_exit("uplack","polnocny-zachod");

}
void
init()
{
    ::init();
    init_pub();
    add_action("sit_down","usiadz");
    add_action("stand_up","wstan");  
    add_action("do_all_commands", "", 1);    
}
string
menu()
{
    say(QCIMIE(this_player(),PL_MIA)+ " czyta tabliczke.\n");
    return
    "\nTabliczka wyglada nastepujaco:\n\n"+
    "\t\tMENU:\n"+
    "Placki:\n"+
    " Placek z jagodami:			60 miedziakow\n"+
    " Placek z truskawkami:   		60 miedziakow\n"+
    " Placek z kruszonka:           30 miedziakow\n"+
    " Placek z jablkami:            55 miedziakow\n\n"+
    "Napoje:\n"+
    " Mleko:                		6 miedziakow\n";
    
}
public int
do_all_commands(string str)
{
    string verb;
    string *nie_dozwolone;
    string *nie_dozwolone2;
    nie_dozwolone=({"n","polnoc","s","poludnie","w","zachod","e","wschod","nw",
        "polnocny-zachod","sw","poludniowy-zachod","se","poludniowy-wschod",
        "polnocny-wschod","ne","teleport",
        "u","gora","d","dol","goto","home"});
    nie_dozwolone2=({"zamow"});   
    verb = query_verb();
    if (this_player()->query_prop(SIEDZACY)==1)
    {
    if (member_array(verb,nie_dozwolone)==-1)
        return 0;
    else 
        return no_command();
    }
    if(this_player()->query_prop(SIEDZACY)==0)
    {
    if(member_array(verb,nie_dozwolone2)==-1)
	return 0;
	else
	return no_command2();
    }
    return 0; 
}
public int
no_command()
{
    write("Moze bys tak najpierw wstal.\n");
    return 1;
}
public int
no_command2()
{
    write("Placki ktore mozesz tu zamowic, podajemy tylko na miejscu wiec usiadz "+
    "przy stole.\n");
    return 1;
}
int
sit_down(string str)
{
    object who,*list;
    string check;
  
    if (!str)
        return 0;

    if (sscanf(str,"przy %s",check)==1)
        str = check;
    
    
    notify_fail("Gdzie chcesz usiasc, moze przy stole?\n");
    if(str!="stole")return 0;
    if(this_player()->query_prop(SIEDZACY)==1)
    {
    write("Ale ty juz siedzisz.\n");
    return 1;
    }
    this_player()->add_prop(SIEDZACY,1);
    this_player()->add_prop(LIVE_S_EXTRA_SHORT," siedzac"+
    this_player()->koncowka("y","a")+" przy stole");
    write("Rozsiadasz sie wygodnie na krzesle sojacym przy stole.\n");
    say(QCTNAME(this_player())+" siada przy stole.\n");
    return 1;
}
int
stand_up(string str)
{
    object who,*list;
    string check;

    notify_fail("Nie siedzisz nigdzie.\n");
    if (this_player()->query_prop(SIEDZACY)==0)
        return 0;

    this_player()->change_prop(SIEDZACY,0);
    this_player()->change_prop(LIVE_S_EXTRA_SHORT,"");
    write("Wstajesz z krzesla.\n");
    say(QCTNAME(this_player())+" wstaje od stolu.\n");
    return 1;
}

