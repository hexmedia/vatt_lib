/*
 * Przykladowy bank
 */
 
inherit "/d/std/room";
inherit "/lib/bank";

#include <stdproperties.h>

/*
 * Function name: create_room
 * Description:   Set up default trade and cofigure it if wanted.
 */
void
create_room()
{
    set_short("Skarbiec miasta Athkatla");
    set_long("W tym budynku znajduje sie tylko jedno,  "+
        "zakratowane okno. Sciany wykonane sa z cegly. "+
        "Za stolem, obok kilku grubych ksiag siedzi urzednik "+
        "przeliczajacy monety. W tym skarbcu mozesz zlozyc "+
        "swoje oszczednosci aby pomnazaly swa objetosc. "+
        "Na scianie wisi jakas tabliczka.\n");

    add_item( "tabliczke",
        "W sumie to nic ciekawego, choc mozesz ja pewnie przeczytac.\n");

    add_cmd_item( ({ "tabliczke" }), "przeczytaj", "@@standard_bank_sign");

    add_prop(ROOM_I_INSIDE, 1);

    config_default_trade(); /* ustawia standardowe parametry banku -
    			     * niezbedny, jesli nie ustawiamy wszystkiego
    			     * samemu */
    set_bank_fee(5); /* % pobierany przy wszystkich tranzakcjach */
    config_trade_data();
    add_exit("uskarbiec","poludnie");
}

init()
{
    ::init();

    bank_init(); /* Dodaje komendy banku */
}

