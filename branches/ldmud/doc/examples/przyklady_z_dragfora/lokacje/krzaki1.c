#pragma strict_types


inherit "/d/Faerun/std/room";

#include <stdproperties.h>
#include <macros.h>

void
create_room()
{
    set_short("Geste krzaki");
    set_long("Stoisz po srodku gestej kepy krzakow, pelnej roznego rodzaju"
    +" roslin. Spogladajac na polnoc dostrzegasz droge, zas w oddali na"
    +" zachodzie mury twierdzy Candlekeep. Ten fragment kepy zostal"
    +" juz troche wydeptany, widac nie tylko ty znasz ta ukryta w lesie"
    +" sciezke.\n");

    add_item( ({"droge","trakt"}), "Rowny, piaszczysty trakt jest jedynie odnoga znanej "
      + "pod nazwa 'Lwi trakt' drogi. Pono ciagnie sie ona daleko, az do Pomocnej Dloni "
      + "i miasteczka zwanego Beregostem. Mozna by sie nan dostac wychodzac z krzakow.\n");
    add_item( ({"drzewa","krzewy","las"}), "Geste krzewy i sosnowe drzewa skladaja sie na "
          + "istnienie otaczajacego ciebie, gestego lasu.\n");
    add_item(({"mur","mury"}), "Nawet z tej odleglosci z latwoscia dostrzegasz"
    + " olbrzymie mury twierdzy Candlekeep. Ciekawe czemu sciezka prowadzi w"
    +" w tamtym kierunku?\n");
    add_exit("krzaki2",({"mury","w kierunku murow twierdzy."}),0,2,0);
    set_event_time(500.0,500.0);
    add_event("Na niebie dostrzegasz klucz ptakow.\n",1);
    add_event("W mroku nocy slyszysz huczenie sowy.\n",2);
    add_event("Slyszysz nieopodal trzask galazki.\n",0);
    add_event("Gwiazdy pieknie blyszcza sie na ciemnym sklepieniu.\n",2);
    add_event("Z glebi lasu dochodza twych uszu niepokojace dzwieki przyrody.\n",0);
}


int bush_exit(string str)
{
        notify_fail("Wyjdz skad?\n");
        if(str != "z krzakow") return 0;
        saybb(QCIMIE(this_player(),0) + " wychodzi z krzakow i podaza w kierunku drogi.\n");
        write("Wychodzi z krzakow prosto na trakt.\n");
        this_player()->move("/d/Faerun/Trakty/cand-ber/lokacje/trakt5");
        saybb(QCIMIE(this_player(),0) + " wchodzi na trakt wprost z kepy krzakow.\n");
        return 1;

}

void init()
{
        ::init();
        add_action("bush_exit","wyjdz");
}
