/*---Karczma 'Po zapitym zeglarzem'---*/
/*-----------Copyright Volo-----------*/
/*---------Gospoda w Crimmor----------*/
#pragma strict_types

inherit "/d/Faerun/std/karczma.c";

#include <stdproperties.h>
#include "../definicje.h"
#include <macros.h>
#include <mudtime.h>
#include <pub.h>

void
create_room()
{
    set_short("Tawerna 'Pod zapitym zeglarzem'");
    set_long("Stoisz w tej chwili na parkiecie w pewnej przytulnej i dobrze ogrzanej izbie. "
	  + "Jest to oczywiscie glowna sala gospody 'Pod zapitym zeglarzem', do ktorej zwyczaj "
	  + "maja sciagac stare wilki morskie zaraz po przycumowania swego okretu do drewnianego "
	  + "pomostu. Ustawiono tutaj szereg debowych law po obu stronach izby, zas w rogu znajduje "
	  + "sie drewniany stolik, przy ktorym kapitan ma zwyczaj zasiadac kiedy brak mu marynarzy "
	  + "do nastepnej podrozy. Wtedy tez kazdy wyczytywany zeglarz wstaje od swego posilku i podchodzi "
	  + "don zaciagajac sie na nastepny rejs. Cala izbe oswietla skrupulatnie wykonana z mosiadzu olejna lampa zawieszona "
	  + "pod sufitem. Co jakis czas mija cie jeden z wielu strudzonych marynarzy, "
	  + "ktorzy od kilku dni, a moze i miesiecy nie mieli okazji przemyslec swojego zywota nad "
	  + "drewnianym kufelkiem pelnym piwa. Na scianie zawieszono drewniana tablice, ktora jak sadzisz "
	  + "moze sluzyc jako menu tego nietypowego przybytku. \n");
    add_item( "lawy", "Zwykle, debowe lawy. Z reguly to przy nich wlasnie zasiadaja strudzeni "
	  + "rejsem zeglarze i w spokoju spozywaja zamowiony posilek, badz wypijaja ulubiony trunek "
	  + "oczekujac, az kolejny dobrotliwy kapitan zaciagnie ich na swa lajbe. \n");
    add_item( "lampe", "Przymocowana do scian za pomoca zelaznych uchwytow, olejna lampa w mrozne noce "
	  + "znakomicie oswietla izbe dodajac otuchy zmeczonym rejsem marynarzom \n");
    add_item( "stolik", "Normalny drewniany stolik. Stoi on w rogu izby i to przy nim zasiada kapitan "
	  + "kiedy brak mu marynarzy do kolejnego rejsu. \n");
    add_item( "tablice", "Ta zawieszona na scianiei, drewniana tablica jest miejscowym "
      + "jadlospisem. Moze wartalo by ja wpierw przeczytac? \n");
    add_cmd_item(({"tablice","jadlospis"}),({"czytaj","przeczytaj"}),
        "@@menu");
    
	set_event_time(240.0,240.0);
	set_events_on(1);
    add_event("Minal cie jeden z wielu uczeszczajacych tu wilkow morskich. \n");
    add_event("Pewien spity marynarz wyskakuje na stol spiewajac nowo powstala serenade. \n");
    add_event("Po calej izbie rozchodzi sie muzyka spiewanych przez zeglarzy szant marynarskich. \n");
    add_event("Ktos wylal na jednego z gosci caly kufelek piwa! \n");
    add_event("Pewien kapitan wchodzi do gospody i zasiada przy jednej z law. \n");
    add_event("W gospodzie zapada cisza, gdyz jeden z wilkow morskich zaczyna snuc swa opowiesc. \n");
    
	add_exit("ulicam17","zachod");
    
	set_sit("przy lawie","przy debowej lawie","od lawy");
	ustaw_karczmarza(NPC_PATH + "marius.c");

    add_npcs(NPC_PATH + "zeglarz.c", 1);

	add_npcs(NPC_PATH + "zeglarz.c", 1);

    add_prop(ROOM_I_INSIDE,1);

	add_drink("piwo",({"piwa", "kufla piwa"}), ({"piwo", "kufel piwa", "ciemne piwo", "marynarskie piwo"}), "ciemnego, marynarskiego piwska",
    500,7,40,PL_NIJAKI_NOS, KUFEL, "piwa", "Jest to drewniany kufel z porecznym uchwytem. Na jego powierzchni "
	+ "wystrugano niewielki znaczek tawerny 'Pod zapitym zeglarzem'.\n");

	add_drink("rum",({"rumu", "kieliszka rumu"}), ({"rum", "kieliszek rumu", "mocny rum", "zeglarski rum"}), "mocnego rumu",
    50,35,400,PL_MESKI_NOS_NZYW, KIELISZEK, "rumu", "Jest to maly, szklany kieliszek.\n");

	add_drink("gin",({"ginu", "kieliszka ginu"}), ({"gin", "kieliszek ginu", "aromatyczny gin"}), "aromatycznego ginu",
    50,28,400,PL_MESKI_NOS_NZYW, KIELISZEK, "ginu", "Jest to maly, szklany kieliszek.\n");

	add_food("Smaczne, swietnie wysmazone skwarki", ({"skwarki","wysmazone skwarki"}), 
                "smaczne, swietnie wysmazone skwarki", 320, 40, "talerz smacznych, swietnie wysmazonych skwarkow");

	add_food("Wysmienita golonka", ({"golonke","smaczna golonke"}), 
                "wysmienita golonke", 380, 30, "wysmienita golonke");
}
string
menu()
{
    say(QCIMIE(this_player(),PL_MIA)+ " studiuje uwaznie tresc drewnianej tablicy.\n");
    return
    "\no------------------------------------------------------o\n\n"+
    "\t\t    Jadlospis:\n"+
    "   Jadlo:\n"+
    "   Wysmazone skwarki                40 miedziakow\n"+
    "   Smaczna golonka                  30 miedziakow\n"+
    "   Trunki:\n"+
    "   Ciemne, marynarskie piwo         20 miedziakow\n"+
    "   Mocny, zeglarski rum             20 miedziakow\n"+
    "   Aromatyczny gin                  20 miedziakow\n"+
    "                                                          \n"+  
	"\no------------------------------------------------------o\n"+
    "\n";
}

/*----Podpisano Volothamp Geddarm----*/


