#pragma strict_types

#include "definicje.h"
inherit "/d/Faerun/std/karczma";
#include <stdproperties.h>
#include <macros.h>
#include <pub.h>

void
create_karczma()
{	
    set_short("Gospoda Candlekeep");
    set_long("Bar w tej gospodzie jest obszerny i bardzo funkcjonalny. "+
    "W pomieszczeniu panuje zaduch, zas w powietrzu unosi sie zapach alkoholu, "+
    "tytoniu i ludzkiego potu. Pomieszczenie jest zadbane i czyste. Nad barem "+
    "wisi tabliczka pelniaca role jadlospisu, a stoliki przy ktorych mozna usiasc sa "+
    "okragle i na kazdym stoi krysztalowa popielnica, sluzaca dla ludzi palacych.\n");
    add_prop(ROOM_I_INSIDE,1);

    add_item(({"stol","stoly"}),"Jest to kilka okraglchy stolikow "+
    "wykonanych z drewna. Stoi przy nich kilka krzesel "+
    "na ktorych mozna w spokoju usiasc.\n");
    
    add_drink("Miejscowe, ciemne piwo",({"ciemnego schlodzonego piwa",
    "piwa"}),({"ciemne piwo","piwo"}),"ciemno-bursztynowego, "+
    "wspaniale pieniacego sie piwa",500,17,100,PL_NIJAKI_NOS,KUFEL,
    "piwa",
    "Jest to kufel wykonany z krystalicznego szkla, ktorego uchwyt "+
    "wykonano z dobrego debowego drewna.");
    
    add_drink("Czerwone, wytrawne wino",({"czerwonego wytrawnego wina",
    "wina"}),({"czerwone wytrawne wino","wino","czerwone wino"}),
    "czerwonego wytrawnego wina, ktore zapewne przez kilka lat dojrzewalo "+
    "w piwnicach Candlekeep. Jego slawa jest znana w calym Faerunie",
    200,12,200,PL_NIJAKI_NOS,KIELISZEK,"wina","Jest to kieliszek wykonany ze "+
    "szkla. Doskonale przezroczysty, widac ze w karczmie w ktorej w "+
    "takich kieliszkach podaja trunki, bardzo dba sie o czystosc.",2000);
    
    add_drink("Mocny, krasnoludzki spirytus",({"mocnego spirytusu",
    "spirytusu"}),({"mocny krasnoludzki spirytus","spirytus",
    "mocny spirytus"}),"bardzo mocnego krasnoludzkiego spirytusu. Malo kto "+
    "ma odwage wypic chocby troche tak mocnego spirytusu, tylko zaprawieni "+
    "w piciu wojownicy zdolaja wypic ta piekielna wode",200,45,250,
    PL_MESKI_NOS_NZYW,SZKLANKA,"spirytusu","Jest to zwykla, wykonana ze "+
    "szkla szklanka na ktorej namalowano mocno podpitego krasnoluda, "+
    "oraz otwarta ksiege "+
    "ktora oznacza ze szklanka pochodzi z gospody w Candlekeep.",2000);
    
    add_food("Soczysty smaczny stek z ryzem",({"stek z ryzem","stek",
    "soczysty stek"}),"miske z ryzem i soczystym stekiem",300,30,
    "ryz z soczystym stekiem");
    
    add_food("Baranina",({"baranine"}),"talerz z obficie przyprawiona "+
    "rumiana baranina",500,50,"obficie przyprawiona i rumina baranine");
    

    add_exit("droga11","poludniowy-wschod");    
    add_exit("gospoda2","zachod");
    set_event_time(30.0,60.0);
    add_event("Ktos tlucze szklanke.\n");
    add_event("Paru podpitych gosci, bije sie ze soba piesciami.\n");
    add_event("Dym unoszacy sie z fajki drazni twe gardlo.\n");
    add_event("Slyszysz chrapanie dochodzace od jednego ze stolikow.\n");
add_event("Ktos spada z krzesla i laduje na ziemi.\n");
    ustaw_karczmarza(NPC_PATH+"karczmarz");
    set_sit("za stolem","przy drewnianym stole","od stolu",7);
    add_npcs(NPC_PATH+"kaplan",1);
add_cmd_item(({"tabliczke","jadlospis"}),({"czytaj","przeczytaj"}),
		   "@@jadlospis");
}

int
jadlospis()
{
    say(QCIMIE(this_player(),PL_MIA)+ " czyta jadlospis.\n");
    sporzadz_liste();
    return 1;
}

void
init()
{
    ::init();
}

    
