#include "../../pal.h"
inherit STD+"pal_std.c"; /* std'ka Palanthasu */
inherit "/lib/shop"; /* ten obiekt jest sklepem */
object sprzedawca;
void create_pal_room()
{
	set_short("Sklep z bronia");
	set_long("Znajdujesz sie w przestronnym sklepie z bronia. Na scianach "
	+ "rozwieszone sa rozne egzemplarze sprzedawane zapewne w sklepie. "
	+ "Wsrod nich jestes w stanie wyroznic przede wszystkim ogromne miecze, "
	+ "z cala pewnoscia uzywane przez tutejsze rycerstwo, ale wisza tu "
	+ "takze topory, mloty, dlugie halabardy i inne bronie drzewcowe oraz "
	+ "cale mnostwo broni przeroznych masci.\nW glebi sklepu widzisz drewniane "
	+ "drzwi prowadzace zapewne do magazynu, natomiast nad lada zauwazasz "
	+ "mosiezna tabliczke, z wygrawerowanymi jakimis napisami.\n");

    	add_item( ({ "instrukcje", "tabliczke" }), "Tabliczka ze wskazowkami" 
        +"dla klientow. Z cala pewnoscia warto byloby ja przeczytac.\n");
        
        add_item( ({ "drzwi", "magazyn" }), "Probujesz zajrzec do magazynu przez "
        + "uchylone drzwi, jednak nie udaje ci sie dostrzec niczego ciekawego. Chyba "
        + "prosciej bedzie poprosic sprzedawce o pokazanie interesujacej cie broni.\n");
        
    	add_cmd_item( ({ "instrukcje", "tabliczke" }), "przeczytaj",
        "@@standard_shop_sign");
        
        add_exit(LOC+"sklepy/bron_mag.c",({ "magazyn", "do magazynu" }),"@@wiz_check",1);
        add_exit(LOC+"swiatynna5.c","poludnie");
        
        add_prop(ROOM_I_INSIDE,1);
        
        config_default_trade();
        set_store_room(LOC+"sklepy/bron_mag.c");
	sprzedawca=clone_object(NPC+"bron_sprzedawca.c");
	sprzedawca->move(this_object());
}

void init()
{
	::init();
	init_shop();	
}
/*
int shop_hook_allow_sell(object ob)
{
    if (function_exists("create_weapon",ob))
      return ::shop_hook_allow_sell(ob);
    if (objectp(sprzedawca))
    {
      	sprzedawca->command("emote spoglada na ciebie "
      	+"z dziwnym usmiechem.");
      	sprzedawca->command("powiedz do " + 
         (this_player()->query_nazwa(PL_DOP)) + 
         " Przykro mi, Pani"+this_player()->koncowka("e","")+
         ", ale skupuje i sprzedaje jedynie orez.");
      notify_fail("");
    }
    else
      notify_fail("Sprzedawca pelnym smutku glosem mowi: " 
      +"Przykro mi, ale skupuje i sprzedaje jedynie orez.\n");
    return 0;
}*/