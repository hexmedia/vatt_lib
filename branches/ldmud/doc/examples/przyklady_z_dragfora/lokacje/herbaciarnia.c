#pragma strict_types
inherit "/d/std/room";
inherit "lib/pub";
#include <stdproperties.h>
#include <macros.h>
public int no_command();
public int no_command2();
void
create_room()
{	
    set_short("Herbaciarnia");
    set_long("Herbaciarnia to sredniej wielkosci kamienny budynek. Pomieszczenie "+
        "wypelnia unoszacy sie w powietrzu zapach herbaty. Przy scianach stoja "+
        "drewniane polki wypelnione slojami i paczuszkami o rozmaitej wielkosci "+
        "i zawartosci. Z boku znajduje sie niewielki piecyk z imbrykiem do "+
        "parzenia herbaty. W rogu, obok okna ustawiony jest niewielki stol wraz z dwoma "+
        "krzeslami. Zza lady spoglada na Ciebie siwy dlugobrody staruszek.\n"); 
    add_prop(ROOM_I_INSIDE,1);
    add_cmd_item(({"tabliczke","menu"}),({"czytaj","przeczytaj"}),
		   "@@menu");
    add_item("stol","Jest to ogragly, niewielki stolik wykonany z drewna, stoja przy nim dwa krzesla "+
    "na ktorych mozna usiasc.\n");
    add_item("piecyk","Jest to niewielki piecyk na wegiel. Stoi na nim jakis imbryk.\n");
    add_item("polki","Sa to drewniane polki. Zauwazasz na nich przerozne sloje.\n");
    add_item("sloje","Ogladasz przeroznej wielkosci sloje rowno ustawione na polkach. Sa one wypelnione "+
            "przeroznego rodzaju herbatami.\n"); 
    
    
    add_drink("herbaty mnichow","herbate mnichow","filizanke przepysznej herbaty mnichow","",0,0,250,25,25,
    PL_NIJAKI_NOS); 
    add_drink("czarnej herbaty","czarna herbate","filizanke mocnej czarnej herbaty","",0,0,250,25,25,
    PL_NIJAKI_NOS);
    
add_exit("uherb","polnocny-zachod");

}
void
init()
{
    ::init();
    init_pub();
    add_action("sit_down","usiadz");
    add_action("stand_up","wstan");  
    add_action("do_all_commands", "", 1);    
}
string
menu()
{
    say(QCIMIE(this_player(),PL_MIA)+ " czyta tabliczke.\n");
    return
    "\nTabliczka wyglada nastepujaco:\n\n"+
    "\t\tMENU:\n"+
    "Trunki:\n"+
    " Wspaniale, elfie wino:		100 miedziakow\n"+
    " Dobre, wytrawne wino:		70 miedziakow\n"+
    " Szlachetne, pyszne wino:      110 miedziakow\n"+
    " Czerwone, slodkie wino:      60 miedziakow\n"+
    " Biale, wytrawne wino:     80 miedziakow\n"; 
    
}
public int
do_all_commands(string str)
{
    string verb;
    string *nie_dozwolone;
    string *nie_dozwolone2;
    nie_dozwolone=({"n","polnoc","s","poludnie","w","zachod","e","wschod","nw",
        "polnocny-zachod","sw","poludniowy-zachod","se","poludniowy-wschod",
        "polnocny-wschod","ne","teleport",
        "u","gora","d","dol","goto","home"});
    nie_dozwolone2=({"zamow"});   
    verb = query_verb();
    if (this_player()->query_prop(SIEDZACY)==1)
    {
    if (member_array(verb,nie_dozwolone)==-1)
        return 0;
    else 
        return no_command();
    }
    if(this_player()->query_prop(SIEDZACY)==0)
    {
    if(member_array(verb,nie_dozwolone2)==-1)
	return 0;
	else
	return no_command2();
    }
    return 0; 
}
public int
no_command()
{
    write("Moze bys tak najpierw wstal.\n");
    return 1;
}
public int
no_command2()
{
    write("Wina ktore mozesz tu zamowic, podajemy tylko na miejscu wiec usiadz "+
    "przy stole.\n");
    return 1;
}
int
sit_down(string str)
{
    object who,*list;
    string check;
  
    if (!str)
        return 0;

    if (sscanf(str,"przy %s",check)==1)
        str = check;
    
    
    notify_fail("Gdzie chcesz usiasc, moze przy stole?\n");
    if(str!="stole")return 0;
    if(this_player()->query_prop(SIEDZACY)==1)
    {
    write("Ale ty juz siedzisz.\n");
    return 1;
    }
    this_player()->add_prop(SIEDZACY,1);
    this_player()->add_prop(LIVE_S_EXTRA_SHORT," siedzac"+
    this_player()->koncowka("y","a")+" przy stole");
    write("Rozsiadasz sie wygodnie na krzesle sojacym przy stole.\n");
    say(QCTNAME(this_player())+" siada przy stole.\n");
    return 1;
}
int
stand_up(string str)
{
    object who,*list;
    string check;

    notify_fail("Nie siedzisz nigdzie.\n");
    if (this_player()->query_prop(SIEDZACY)==0)
        return 0;

    this_player()->change_prop(SIEDZACY,0);
    this_player()->change_prop(LIVE_S_EXTRA_SHORT,"");
    write("Wstajesz z krzesla.\n");
    say(QCTNAME(this_player())+" wstaje od stolu.\n");
    return 1;
}

