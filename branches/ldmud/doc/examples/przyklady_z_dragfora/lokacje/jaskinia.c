inherit "/std/room";
#include <macros.h>
#include <stdproperties.h>
#define KAPIACY "_kapiacy"
int wejdz_do_jeziorka(string wdj);
int nurkowanie();
int chlapanie();
int plywanie();
int wychodzenie_z_jeziorka(string wzj);
int siadanie_na_tronie(string snt);
int wstawanie(string wzt);
int nie_wyjdziesz();


void
create_room()
{
    set_short("Pieczara Shili");
    set_long("Znajdujesz sie w sporej jaskini o nieregularnym ksztalcie, w ktorej panuje "
            + "nieznosnie wysoka temeratura. Powietrze, drzace w niesamowitym zarze, "
            + "przesycone jest drazniacym zapachem siarki. Sciany, sklepienie, a nawet "
            + "podloge pieczary przecinaja szczeliny emanujace pulsujacym pomaranczowym "
            + "swiatlem, zas sam jej srodek stanowi okragle jeziorko po brzegi wypelnione "
            + "bulgoczaca lawa. Mniej wiecej w centrum jeziora dostrzegasz niewielka "
            + "wysepke, na ktorej wznosi sie tron z czarnego kamienia. Jedna ze scian "
            + "zajmuje otoczony czerwonawa aura owal; jego wnetrze zdaje sie wirowac, co "
            + "jakis czas ukazujac zdeformowane, wykrzywione w bolesnym grymasie twarze. "
            + "Z ciemnosci zalegajacych katy pomieszczenia lypia na ciebie zlowrozbne "
            + "zielone slepia, a do twych uszu dolatuja stlumione piski i warkniecia.\n");

   add_exit("/d/Faerun/wiz/wizroom", ({"portal", "przez portal i znika posrod wirujacej czerwieni"}), 0, 1);
   add_exit("/d/Faerun/wiz/podmrok", ({"podmrok", "do Podmroku"}), 0, 1);
   add_exit("/d/Podmrok/swiat/Menzoberranzan/lokacje/narbondel", 
           ({"menzo", "do Menzoberranzan"}), 0, 1);
   add_exit("/d/Krainy/tenser/zamek/dziedziniec2.c", "poludnie");
   
   add_item(({"sciany", "sufit", "sklepienie", "sciana", "podloga"}), "Sciany, podloge oraz "
          + "sufit stanowi nie tkniety zadna obrobka smolistoczarny "
          + "granit, gdzieniegdzie poprzecinany czerwonawymi zylkami. Jego powierzchnia jest "
          + "tak porysowana i chropowata, ze gdybys przejechal po niej dlonia, z pewnoscia "
          + "bys sie pokaleczyl. Co pare metrow dostrzegasz szczeliny, z ktorych wydobywa sie "
          + "nieprzerwanie pulsujace swiatlo o pomaranczowym zabarwieniu, przywodzace na mysl "
          + "lawe.\n");
   add_item(({"jezioro", "jeziorko"}), "Stanawiace centrum jaskini jeziorko "
          + "wypelnia nieustannie bulgoczaca, czerwono-pomaranczowo-zolta lawa, "
          + "wydzielajaca wrecz niesamowite ilosci ciepla. Co chwile z powierzchni jeziorka "
          + "wznosi sie w gore slup ognia, leca na okolo rozzarzone krople magmy. "
          + "Do twych nozdrzy dolatuje rowniez ostra nieprzyjemna won siarki.\n");
   add_item(({"wyspe", "wysepke"}), "Srodek magmowego jeziorka zajmuje niewielka "
          + "skalna wysepka, ktorej istnienie w tym miejscu usprawiedliwia jedynie " 
          + "znajdujacy sie na niej kamienny tron.\n");
   add_item("tron", "Zupelnie prosty w konstrukcji, ogromny tron wyrzezbiony zostal w "
          + "czarnym marmurze. "
          + "Nie dostrzegasz na nim zadnych zdobien procz dwoch niezywkle zlosliwie "
          + "wygladajacych ni to impow ni to diablikow o rubinowych oczach przycupnietych "
          + "na kamiennym oparciu. Te niewielkie rzezby zdaja sie co jakis czas minimalnie " 
          + "poruszac... A moze to tylko zludzenie?\n");
   add_item(({"portal", "owal"}), "Owalny swietlisty ksztalt widnieje na jednej ze scian "
          + "groty. Emituje on niezbyt silna czerwonawa poswiate, a jego wnetrze zdaje sie "
          + "mienic tysiacem odcieni purpury. Co jakis czas gdzies posrod tej rozedrganej "
          + "wirujacej czerwonosci pojawiaja sie zarysy twarzy wykrzywionych jakims "
          + "potwornym cierpieniem, po to tylko by rozmyc sie w mgnieniu oka.\n");
   
   add_prop(ROOM_I_INSIDE,1);   
 
   /* set_event_time(180.0);
   add_event("Lawa w jeziorku skwierczy cicho.\n");
   add_event("Od strony owalnego portalu do twych uszu dobiega cos jakby stlumiony jek.\n");
   add_event("Drazniaca won siarki stala sie jeszcze intensywniejsza pod wplywem "
           + "naglego powiewu.\n");
   add_event("Jedna z rzezb impow przycupnietych na oparciu kamiennego tronu lypnela na "
           + "ciebie zlowrogo.\n");
   add_event("Znad jeziorka lawy unosza sie siarczane opary.\n");
   add_event("W jednym z katow pieczary cos paskudnie zaskrzeczalo.\n");
   add_event("Podloga jaskini jakby lekko zadrzala...\n");    */



   
}


 int
 wejdz_do_jeziorka(string wdj)
 {
  notify_fail("Gdzie chcesz wejsc? Moze do jeziorka?\n");
   if((wdj=="do jeziorka")||(wdj=="do jeziora"))
    {
    switch(this_player()->query_prop(KAPIACY))
    {
   case 1:
    { 
    write("Przeciez juz siedzisz po szyje w lawie!\n"); 
    break;
    }
   case 2:
    {
    write("Moze najpierw wstan z tronu?\n");
    break;
    }    
   default:
    {    
    write("Podchodzisz do brzegu jeziorka, przygladajac sie ciekawie wypelniajacej je " 
           + "lawie. Bierzesz kilka krokow rozbiegu i z rozmachem wskakujesz "
           + "do bulgoczacej magmy, wzniecajac deszcz drobniutkich ognistych kropelek.\n");
    saybb(QCIMIE(this_player(), PL_MIA) + " podchodzi wolno do brzegu jeziorka, " 
           + "wlepiajac wzrok w wypelniajaca je lawe. Robi kilka ostroznych krokow w tyl, " 
           + "poczym z rozmachem wskakuje do bulgoczacej magmy, "
           + "wzniecajac deszcz drobniutkich ognistych kropelek.\n");
    this_player()->add_prop(LIVE_S_EXTRA_SHORT," kapiac" 
           + this_player()->koncowka("y","a") + " sie w jeziorku");
    this_player()->add_prop(LIVE_S_SOULEXTRA,"pluska sie w jeziorku lawy");    
    this_player()->add_prop(KAPIACY,1);
    }
    }
   return 1;
  }
}
 int
 nurkowanie()
 {
  if(this_player()->query_prop(KAPIACY)==1)
  {
   write("Lapiesz spory chaust zalatujacego siarka powietrza i dajesz nura pod "
           + "powierzchnie rozzarzonej lawy. Widzisz jedynie pulsujaca pomaranczowa "
           + "matnie. Wynurzasz sie posrod mnostwa plonacych iskierek.\n");
   saybb(QCIMIE(this_player(), PL_MIA) + " lapie chaust powietrza i znika "    
           + "pod powierzchnia lawy . Po krotkiej chwili wynurza sie "
           + this_player()->koncowka("caly", "cala") + " w kropelkach plonacej magmy.\n");
   return 1;
  }
 else
  {
   write("Przeciez nie jestes w jeziorku!\n");
   return 1;
  }
}
 

 int
 chlapanie()
 {
  if(this_player()->query_prop(KAPIACY)==1)
  {
    write("Zaczynasz gwaltownie uderzac rekoma w powierzchnie lawy, "
          + "calkowicie ochlapujac ognistym deszczem siebie i wszystko wokol. "
          + "Dobrze, ze nie widzi tego zaden smiertelnik, bo pewnie umarlby "
          + "juz na atak serca.\n"); 
    saybb(QCIMIE(this_player(), PL_MIA) + " zaczyna mlocic rekoma lawe, "
          + "rozbryzgujac ja na wszystkie strony. Zanim zdarzasz zareagowac w "
          + "jakikolwiek sposob, w okolo Ciebie fruwa juz mnostwo ognistych kropel.\n");
    return 1;
  }
 else
  {
   write("Przeciez nie jestes w jeziorku!\n");
   return 1;
  }
 }

 int
 plywanie()
 {
  if(this_player()->query_prop(KAPIACY)==1)
  {
    write("Wyciagasz sie na powierzchni lawy i po kilku ruchach rak i "
         + "nog udaje Ci sie doplynac do najblizszego brzegu. " 
         + "Odbijasz sie od niego sprawnie i po kilkunastu kolejnych ruchach " 
         + "docierasz do przeciwleglej krawedzi jeziorka.\n");
    saybb(QCIMIE(this_player(), PL_MIA) + " wykonujac kilkanascie ruchow "
         + "plywackich przeplywa z jedenego konca jeziorka na drugi i " 
     
    + "spowrotem.\n");
    return 1;
  }
 else
  {
   write("Przeciez nie jestes w jeziorku!\n");
   return 1;
}
 }
 int
 wychodzenie_z_jeziorka(string wzj)
 {
  notify_fail("Skad chcesz wyjsc? Moze z jeziorka?\n");
  
  if((wzj=="z jeziorka")||(!wzj))
  {
  if(this_player()->query_prop(KAPIACY)==1)
  {
    write("Z pewnym ociaganiem decydujesz sie opuscic przyjemny zar " 
         + "plonacej magmy. Wychodzisz na brzeg, pozostawiajac za soba strumyczek roztopionej " 
         + "skaly.\n");
    saybb(QCIMIE(this_player(), PL_MIA) + " z wyrazna niechecia zbliza "
         + "sie do brzegu jeziorka i ociagajac sie wychodzi z niego, " 
         + "zostawiajac na posadzce strumyczek plonacej skaly.\n");
    this_player()->add_prop(KAPIACY,0);
    this_player()->remove_prop(LIVE_S_EXTRA_SHORT);
    this_player()->remove_prop(LIVE_S_SOULEXTRA);       
    return 1;

  }
 else
  {
   write("Przeciez nie jestes w jeziorku!\n");
   return 1;
  }
  }
 }

 int
 siadanie_na_tronie(string snt)
 {
 notify_fail("Na czym chcesz usiasc? Moze na tronie?\n");
   
 if(snt=="na tronie")
 {
 if(this_player()->query_real_name()!="tenser") {write("Ten tron jest przeznaczony " 
                         + "wylacznie dla wlascicielki tej jaskini.\n");
                         return 1;}
   switch(this_player()->query_prop(KAPIACY))
  {
   case 1:
   {
   write("Moze najpierw wyjdziesz z jeziorka?\n");
   break;
   }
   case 2:
   {
   write("Przeciez juz siedzisz na tronie.\n");
   break;
   }
   default:
   {
    saybb(QCIMIE(this_player(), PL_MIA) + " pewnie zasiada na czarnym marmurowym "
         + "tronie. Przycupniete na oparciu rzezby impow zdaja sie spogladac swymi " 
         + "rubinowymi oczami prosto na siedzac"+ this_player()->koncowka("ego.","a."));
    write("Bez wahania siadasz na siedzisku czarnego marmurowego tronu. " 
         + "Spostrzegasz, ze przycupniete na oparciu rzezby impow zdaja sie spogladac "
         + "swymi rubinowymi oczami wprost na Ciebie.\n");
    this_player()->add_prop(LIVE_S_EXTRA_SHORT," siedzac" + this_player()->koncowka("y","a")
         + " na tronie");
    this_player()->add_prop(KAPIACY,2);
   }
  }
  return 1;
  }
 }
 int
 wstawanie(string wzt)
 {
  notify_fail("Z czego chcesz wstac? Moze z tronu?\n");
  
  if((wzt=="z tronu")||(wzt==""))
  {
  if(this_player()->query_prop(KAPIACY)==2)
   {
    saybb(QCIMIE(this_player(), PL_MIA) + " wstaje z tronu.\n");
    write("Podnosisz sie z twardego marmurowego siedziska.\n");
    this_player()->remove_prop(LIVE_S_EXTRA_SHORT);
    this_player()->add_prop(KAPIACY,0);
    return 1;
   }
  else
   {
    write("Przeciez juz siedzisz na tronie!\n");
    return 1; 
   }
   }
 }
int
nie_wyjdziesz()
{
 if(this_player()->query_prop(KAPIACY)==1)
  {
   write("Moze najpierw wyjdz z jeziorka?\n");
   return 1;  
  }
 if(this_player()->query_prop(KAPIACY)==2)
  {
   write("Moze najpierw wstan z tronu?\n");
   return 1;
  }
 if(this_player()->query_prop(KAPIACY)==0)
 {

 this_player()->remove_prop(KAPIACY);  
 return 0;
 }  
}     



init()
{
  ::init();
  add_action("nurkowanie", "nurkuj");
  add_action("chlapanie", "chlap");
  add_action("plywanie", "plyn");
  add_action("plywanie", "plywaj");
  add_action("wstawanie", "wstan");
  add_action("wejdz_do_jeziorka", "wejdz");
  add_action("wychodzenie_z_jeziorka", "wyjdz");
  add_action("siadanie_na_tronie", "usiadz");
}
