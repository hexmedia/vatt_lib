/* Administracja patrolem, wiec tutaj znajduja sie wszystkie funkcje
 * dotyczace obslugi patrolu Candlkeep, skladajacego sie z 5 zolnierzy i
 * oficera ktory nimi dowodzi, oficer dzierzy wyjebisty bron z klasy mieczy
 * nieco lepsza kolczuge itp. i jest silniejszy i teraz taki bajer ze
 * jesli bedzie atak kogos na straznika ale tylko i wylacznie straznika
 * to patrol po 20 sekundach przybywa na miejsce akcji i tlucze delikwenta
 * malo tego blokuje droge ucieczki, zreszta straznicy takze blokuja.
 * Delikwent atakujacy to miasto musi miec swiadomosc ze zanim z tad wyjdzie
 * musi przedrzec sie przez kupe straznikow co nie bedzie latwe
 * ale jesli pokona patrol przyjdzie mu to latwiej, najgorzej bedzie mu
 * pokonac straznika bramy ale jesli to zrobi bedzie dzierzyc calkiem niezly
 * mlot i posiadac niezla kolczuge takze warto :D.
 * Mistrz Froan(simon) Pazdziernik 2003
 *
 *
 */
#pragma strict_types
#pragma no_clone

inherit "/std/room";
 
#include <stdproperties.h>
#include <macros.h>
#include <pl.h>
#include "../definicje.h"
object *lista_npcs=allocate(5);
void klonuj_i_tworz_druzyne();
int czy_walka();
void odchodza();
void patrol(int faza);

 void
 create_room()
 {
    set_short("Administracja patrolu candlekeep");
    set_long("Jesli jestes smiertelnikiem nie powinienes tego nigdy "+
	     "ogladac.\n");
   klonuj_i_tworz_druzyne();    
 }
void 
klonuj_i_tworz_druzyne()
{
    object ob;
    int x;
    ob=clone_object("/d/Faerun/candlekeep/npc/sierzant.c");
    ob->move(this_object());
    ob->set_random_move(0);
    lista_npcs+=({ob});
    for(x=0;x<5;x++)
    {
        ob=clone_object("/d/Faerun/candlekeep/npc/straznik.c");
        ob->move(this_object());
        ob->set_random_move(0);
        lista_npcs += ({ob});
    }
    patrol(0);
}
void
patrol(int faza)
{
int z;
    switch(faza)
    {
    case 0:
    for(z=0;z<sizeof(lista_npcs);z++) 
    lista_npcs[z]->move("/d/Faerun/candlekeep/drogak2");    
    lista_npcs[0]->command("krzyknij Sluchajcie mnie uwaznie pokraki !");
    set_alarm(1.0,0.0,"patrol",1);
    break;
    case 1:
    if(czy_walka()){  set_alarm(60.0,0.0,"sprawdz_patrol",1);break;}
    lista_npcs[0]->command("powiedz Jestescie straznikami tego miasta, i waszym "+
    "zadaniem jest chronienie go, wiec dzis was wybralem na patrol.");
    set_alarm(2.0,0.0,"patrol",2);
    break;
    case 2:
    if(czy_walka()){  set_alarm(60.0,0.0,"sprawdz_patrol",2);break;}
    lista_npcs[0]->command("powiedz Macie sluchac moich rozkazow. Zrozumiano ?");
    lista_npcs[1]->command("powiedz Tak jest panie sierzancie.");
    lista_npcs[2]->command("krzyknij Ku chwale Candlekeep panie sierzancie.");
    lista_npcs[3]->command("powiedz Tak, jest.");
    lista_npcs[4]->command("powiedz Tak, jest.");
    set_alarm(2.0,0.0,"patrol",3);
    break;
    case 3:
    if(czy_walka()){  set_alarm(60.0,0.0,"sprawdz_patrol",3);break;}
    lista_npcs[0]->command("krzyknij Za mna straznicy, ku chwale Candlkeep.");
    lista_npcs->command("w");
    set_alarm(2.0,0.0,"patrol",4);
    break;
    case 4:
    if(czy_walka()){  set_alarm(60.0,0.0,"sprawdz_patrol",4);break;}
    lista_npcs[0]->command("powiedz Ruszac sie ofermy, nie ma czasu na obijanie.");
    lista_npcs->command("w");
    set_alarm(30.0,0.0,"patrol",5);
    break;
    case 5:
    if(czy_walka()){  set_alarm(60.0,0.0,"sprawdz_patrol",5);break;}
    lista_npcs[0]->command("powiedz Za mna, mile kotki, za mna na patrol.");
    lista_npcs[3]->command("krzyknij Taa, jest panie sierzancie.");
    lista_npcs->command("w");
    set_alarm(30.0,0.0,"patrol",6);
    break;
    case 6:
    if(czy_walka()){  set_alarm(60.0,0.0,"sprawdz_patrol",6);break;}
    lista_npcs[0]->command("powiedz Za mna, mile kotki, za mna na patrol.");
    lista_npcs[3]->command("krzyknij Taa, jest panie sierzancie.");
    lista_npcs->command("w");
    set_alarm(15.0,0.0,"patrol",7);
    break;
    case 7:
    if(czy_walka()){  set_alarm(60.0,0.0,"sprawdz_patrol",7);break;}
    lista_npcs->command("w");
    set_alarm(15.0,0.0,"patrol",8);
    break;
    case 8:
    if(czy_walka()){  set_alarm(60.0,0.0,"sprawdz_patrol",8);break;}
    lista_npcs->command("s");
    set_alarm(15.0,0.0,"patrol",9);
    break;
    case 9:
    if(czy_walka()){  set_alarm(60.0,0.0,"sprawdz_patrol",9);break;}
    lista_npcs->command("sw");
    set_alarm(15.0,0.0,"patrol",10);
    break;
    case 10:
    if(czy_walka()){  set_alarm(60.0,0.0,"sprawdz_patrol",10);break;}
    lista_npcs->command("w");
    set_alarm(20.0,0.0,"patrol",11);
    break;
    case 11:
    if(czy_walka()){  set_alarm(60.0,0.0,"sprawdz_patrol",11);break;}
    lista_npcs->command("w");
    set_alarm(20.0,0.0,"patrol",12);
    break;
    case 12:
    if(czy_walka()){  set_alarm(60.0,0.0,"sprawdz_patrol",12);break;}
    lista_npcs->command("w");
    set_alarm(20.0,0.0,"patrol",13);
    break;
    case 13:
    if(czy_walka()){  set_alarm(60.0,0.0,"sprawdz_patrol",13);break;}
    lista_npcs->command("nw");
    set_alarm(20.0,0.0,"patrol",14);
    break;
    case 14:
    if(czy_walka()){  set_alarm(60.0,0.0,"sprawdz_patrol",14);break;}
    lista_npcs->command("n");
    set_alarm(15.0,0.0,"patrol",15);
    break;
    case 15:
    if(czy_walka()){  set_alarm(60.0,0.0,"sprawdz_patrol",15);break;}
    lista_npcs->command("n");
    set_alarm(15.0,0.0,"patrol",16);
    break;
    case 16:
    if(czy_walka()){  set_alarm(60.0,0.0,"sprawdz_patrol",16);break;}
    lista_npcs->command("w");
    set_alarm(15.0,0.0,"patrol",17);
    break;
    }
}
 
int
czy_walka()
{
int x;
int size;
size=sizeof(lista_npcs);
for(x=0;x<size;x++)
{
    if(lista_npcs[x]->query_attack()) return 1;
}
    return 0;
}    
void
sprawdz_patrol(int faza)
{
int i = 0;
int z;
if(czy_walka()){  set_alarm(60.0,0.0,"sprawdz_patrol",faza);return;}
if(!objectp(lista_npcs[0]))
{
  while(i < 5)
  {
    if(!lista_npcs[i]) 
    i++;
    else
    z=i;
  }
  
  if(i==5)
  {
 set_alarm(3600.0,0.0,"klonuj_i_tworz_druzyne");
 return;
  }
  else
  {
  if(i>1)
    lista_npcs[z]->command("powiedz Chlopaki idziemy z tad, sierzanta nie ma "+
    "trzeba zglosic jego smierc.");
    else
    lista_npcs[z]->command("powiedz Za Candlekeep ich smierc bedzie pomszczona,"+
    "nie mam juz czego tutaj stac, trzeba zglosic ich smierc.");
     odchodza();
     set_alarm(3600.0,0.0,"klonuj_i_tworz_druzyne");
     return;
  }
}
patrol(faza);
}
void 
odchodza()
{
int i;
for(i=0;i<sizeof(lista_npcs);i++)
{
    lista_npcs[i]->move(this_object());
}
}
void
czekamy1(int od, int dod, int faza_nastepna, string gdzie)
{
int ktory;

    if(czy_walka())
    {
      set_alarm(20.0,0.0,&czekamy1(od,dod,faza_nastepna,gdzie)); 
      return;
    } 
    ktory = od;
    while(ktory < dod+1)
    {
      lista_npcs[ktory]->command(gdzie);
      ktory++;
    }  
    set_alarm(20.0,0.0,"patrol",faza_nastepna);
    return;
}        
