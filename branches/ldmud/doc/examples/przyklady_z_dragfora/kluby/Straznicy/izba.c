/* autor : dhor */
/* Poprawki i dodawanie: Tigr */

inherit "/d/Imperium/std/room";

#include "mith.h"

void
create_imperium_room()
{
/* rodzaj, i czy jasno .... */

        add_prop ( ROOM_I_INSIDE, 0); // czy jest to lokacja na polu, czy pokoj 
        add_prop ( ROOM_I_LIGHT, 1); // jasno czy ciemno


/* co widac */

        set_short( "Zwykla izba");

        set_long( "Znajdujesz sie w zwyklej izbie, ktora zapelnia kilka "+
        "stolow i law do siedzenia, oraz wstawione pod zachodnia sciane "+
        "dwie proste prycze. "+
        "Strop podtrzymuje wstawiony na srodku "+
        "sali mocny, debowy slup, do ktorego przybita jest jakas zdobiona "+
        "tablica, z wizerunkiem jezdzca na koniu i podpisem ponizej. "+
        "Na jednej ze scian powieszono wiekszy kawal deski, na "+
        "ktorej jest cos wypisane. \n");
        

/* pobawimy sie ? */

        add_item ( ({"lawy","stoly"}),
        "Znajduja sie tu trzy stoly, jeden ustawiony w rogu sali, "+
        "drugi tuz obok slupa na srodku izby, trzeci blisko wejscia. \n");
         	
        add_item ( "deske",
       "Widzisz na niej wypisane imiona i nazwiska oraz rysopisy "+
       "mordercow sciganych po calym Imperium. \n");

	add_item ( "prycze",
	"Zwykle zbite z debowych desek prycze z slomianym siennkiem. "+
	"Choc wygladaja prosta, sa czyste i schludne, tak wiec wypoczynek "+
	"w nich dla wielu Straznikow Krain jest zbawieniem i przyjemnoscia "+
	"po kilku dniach patrolowania okolic. \n");
	    
       
        
    add_exit(NPATH + "hall", "wschod", 0, 1);
    
    
    set_event_time(200.0, 200.0);
    add_event("Ktos glosno wstaje z lawy i podaza szybkim krokiem do wyjscia. \n");
    
    set_events_on(1);
      
}

start_here(string str)
{
   notify_fail("Spij tutaj?\n");
   if (str!="tutaj") return 0;
   
   if (!ADMIN->query_czlonek(TP->query_real_name()))
      return notify_fail("Nie jestes Straznikiem Krain!\n");
   
   if (!TP->set_default_start_location(MASTER))
      return notify_fail("Nie ma tu dla ciebie miejsca. Skontaktuj sie z Komendantem.\n");
   
   write("Od tej chwili swe wedrowki po swiecie Arkadii bedziesz rozpoczynal tu.\n");
   
   return 1;
   
}

init()
{
  ::init();
  add_action(start_here,"spij");
}
