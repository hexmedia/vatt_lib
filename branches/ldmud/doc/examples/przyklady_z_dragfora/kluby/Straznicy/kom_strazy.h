#define NAME TP->query_real_name()

init_komendy()
{
   if ((ADMIN->query_komendant())==NAME)
      {
         add_action("awansuj","skawansuj");
//         add_action("skwyrzuc","skwyrzuc");
//	   add_action("zrezygnuj","skzrezygnuj");
//	   add_action("zdegraduj","skzdegraduj");
      }
   if (ADMIN->query_sierzant(NAME))
      {
//         add_action("zrezygnuj","skzrezygnuj");
//         add_action("skwyrzuc","skwyrzuc");
      }
    add_action("lista","sklista");
}

int
awansuj(string str)
{
   mixed kogo;
   notify_fail("Awansuj <kogo> na <jakie stanowisko>?\n");
   if (!strlen(str)) return 0;
   
   parse_command(str,environment(TP),"%l:" + PL_BIE + " 'na' %s",kogo,str);
   
   kogo=NORMAL_ACCESS(kogo,0,0);
   
   kogo=filter(kogo,interactive);
   
   if (!sizeof(kogo)) return 0;
   
   if ((ADMIN->query_komendant())!=NAME)
      return notify_fail("Nie jestes Komendantem Strazy Krain!\n");
   
   if (str!="sierzanta"&&str!="komendanta") 
      return notify_fail("Mozesz awansowac na sierzanta lub komendanta.\n");
   
   if (sizeof(kogo)>1) 
      return notify_fail("Mozesz awansowac co najwyzej jedna osobe naraz.\n");
   
   if (!ADMIN->query_czlonek(kogo[0]->query_real_name()))
      return notify_fail(C(kogo[0]->short(TP,PL_MIA)) + " nie jest Straznikiem.\n");
   
   if (str=="sierzanta")
      {
         if (!ADMIN->set_sierzant(kogo[0]->query_real_name()))
            return notify_fail(C(kogo[0]->short(TP,PL_MIA)) + " juz jest Sierzantem Strazy Krain.\n");
            
         write("Awansowal" +TP->koncowka("es ","as ") + 
            kogo[0]->short(TP,PL_BIE) + " na " +
            "Sierzanta Strazy Krain.\n");
         kogo[0]->catch_msg(C(TP->short(kogo[0],PL_MIA)) + " awansowal" +
            TP->koncowka("","a") + " cie na Sierzanta Strazy Krain!\n");
         return 1;
      }
   if (!ADMIN->set_komendant(kogo[0]->query_real_name()))
      return notify_fail(C(kogo[0]->short(TP,PL_MIA)) + " nie jest Sierzantem Strazy Krain, " +
         "nie mozesz " + kogo[0]->query_zaimek(PL_BIE) + " awansowac na Komendanta.\n");
         
   write(C(kogo[0]->short(TP,PL_MIA)) + "jest nowym Komendantem Strazy Krain!\n");
   kogo[0]->catch_msg("Jestes nowym Komendantem Strazy Krain!\n");
   
   return 1;
}

int
wyrzucamy(mixed kogo)
{
   object okogo=(objectp(kogo)?kogo:find_player(kogo));
   string skogo=(stringp(kogo)?kogo:kogo->query_real_name());
   
   if (ADMIN->query_komendant()!=NAME && ADMIN->query_sierzant(skogo))
      return notify_fail("Nie mozesz wyrzucic innego sierzanta.\n");
   
   if (ADMIN->remove_member(skogo)) 
      {
         write(capitalize(skogo) + " zostal wyrucony ze Strazy.\n");
      }
   else
      {
         write(capitalize(skogo) + " nie jest czlonkiem Strazy Krain.\n");
      }
   
/*   if (objectp(kogo)) 
      {
         
      } */
}

podaj_imie(string str)
{

}

int
skwyrzuc(string str)
{
   mixed oblist;
   
   if (!ADMIN->query_sierzant(NAME)&&ADMIN->query_komendant()!=NAME)
      return notify_fail("Nie masz prawa wyrzucac kogokolwiek ze Strazy Krain.\n");
   
   if (!TP->query_wiz_level()) return 0;
   
   if (strlen(str))
      {
         parse_command(str,environment(TP),"%l:"+PL_BIE,oblist);
         oblist=NORMAL_ACCESS(oblist,0,0);
         if (sizeof(oblist)>1) return notify_fail("Wyrzucac mozesz tylko pojedynczo.\n");
         if (!sizeof(oblist)) return notify_fail("Skwyrzuc albo skwyrzuc <kogo>.\n");
         
         dump_array(oblist[0]);
         return 0;
         return wyrzucamy(oblist[0]);
      }
   else
      {
         input_to(podaj_imie);
      }
   return 1;
}


string * cap(string *arr)
{
   string *tmp=allocate(sizeof(arr));
   int i;
   
   for (i=0;i<sizeof(arr);i++)
       tmp[i]=C(arr[i]);
   
   return tmp;
}

int
lista()
{
   string *memb=ADMIN->query_czlonkowie(),
   	  *sierz=ADMIN->query_sierzanci(),
   	  kom=ADMIN->query_komendant(),str="";
   
   memb-=sierz;
   memb-=({kom});
   
   str+="Komendant:\t" + C(kom) + ".\n";
   
   str+="Sierzanci:\t" + COMPOSITE_WORDS(cap(sierz)) + ".\n";
   
   str+="Czlonkowie:\n" + COMPOSITE_WORDS(cap(memb)) + ".\n";
   
   write(str);
   
   return 1;
   
}