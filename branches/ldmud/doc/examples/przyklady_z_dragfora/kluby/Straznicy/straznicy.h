#include <pl.h>
#include <stdproperties.h>
#include <macros.h>
#include <ss_types.h>
#include <wa_types.h>
#include <formulas.h>

#define NPATH "/d/Utopia/Palanthas/kluby/Straznicy/"
#define ADMIN (NPATH +"admin")

#define TP this_player()

#define C(str) capitalize(str)