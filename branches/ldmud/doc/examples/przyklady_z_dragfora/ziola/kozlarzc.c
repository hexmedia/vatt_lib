inherit "/std/herb";

#include "ziola.h"

create_herb()
{
    ustaw_nazwe(GRZYB, PL_MESKI_OS);

    dodaj_przym("czerwony", "czerwoni");
    dodaj_przym("dorodny", "dorodni");

    ustaw_nazwe_ziola(({"czerwony kozlarz", "czerwonego kozlarza", "czerwonemu kozlarzowi", 
                        "czerwonego kozlarza", "czerwonym kozlarzem", "czerwonym kozlarzu"}), PL_MESKI_NOS_NZYW); 
    
    set_id_long("Masz przed soba typowego czerwonego kozlarza. Trzon pokryty jest czarnym osadem, "+
	"posiada takze czerwony kapelusz.\n");

    set_unid_long("Dziwny, twardy grzyb o trzonie pokrytym czarnym osadem i czerwonym kapeluszu.\n"); 

    set_decay_time(1150);
    set_id_diff(25);
    set_find_diff(4);
    set_amount(26);
    set_herb_value(51);
}