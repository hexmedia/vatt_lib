inherit "/std/herb";

#include "ziola.h"

create_herb()
{
    ustaw_nazwe(LISC, PL_MESKI_NOS_NZYW);
  
    dodaj_przym("kolczasty", "kolczasci");
    dodaj_przym("gruby", "grubi");

    ustaw_nazwe_ziola(({"aloes", "aloesu", "aloesowi", "aloes", "aloesem", "aloesie"}), PL_MESKI_NOS_NZYW); 
    
    set_id_long("Masz przed soba lisc aloesu, kaktusa uzywanego przez znachorow calego "+
        "Faerunu. Przylozony do rany potrafi uleczyc.\n");

    set_unid_long("Jest to gruby, bardzo mocno pachnacy czystym zapachem jakby miety "+
	"lisc. Na lisciu zauwazasz rownie grube kolce, jest on rowniez matowy.\n"); 
  
    set_decay_time(150);
    set_id_diff(45);
    set_find_diff(5);
    set_amount(9);
    set_herb_value(73);

    add_prop(OBJ_I_TYTON, 1);
}
