inherit "/std/herb";

#include "ziola.h"

create_herb()
{
    ustaw_nazwe(GRZYB, PL_MESKI_OS);
  
    dodaj_przym("oslizgly", "oslizgli");
    dodaj_przym("bulwiasty", "bulwiasci");

    ustaw_nazwe_ziola(({"casur", "casura", "casurowi", "casur", "casurem", "casurze"}), PL_MESKI_NOS_NZYW); 
    
    set_id_long("Na pierwszy rzut oka rozpozanjesz grzyb o nazwie Casur. Zjedzony, choc "+
	"nie smakuje najlepiej potrafi zwiekszyc twoja sile...\n");

    set_unid_long("Dziwny oslizgly, mazisty i bulwiasty grzyb od ktorego bije bardzo "+
	"nieprzyjemny zapach...\n");

    set_ingest_verb( ({ "zjedz", "zjadasz", "zjada", "zjesc" }) );      
    set_effect(HERB_SPECIAL, special_effect);       
    set_decay_time(207);
    set_id_diff(43);
    set_find_diff(7);   
    set_amount(18);
    set_herb_value(102);
}
void
special_effect()
{
    object casur;
    setuid();
    seteuid(getuid());
    casur=clone_object(EFEKT+"casur_effect.c");
    casur->move(TP);
    casur->rozjeb_czache();
}
