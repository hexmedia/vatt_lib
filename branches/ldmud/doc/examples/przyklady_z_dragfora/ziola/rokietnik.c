inherit "/std/herb";

#include "ziola.h"

create_herb()
{
    ustaw_nazwe(MECH, PL_MESKI_NOS_NZYW);
  
    dodaj_przym("zielony", "zieloni");
    dodaj_przym("miekki", "miekkcy");

    ustaw_nazwe_ziola(({"rokietnik", "rokietniku", "rokietnikowi", "rokietnika", "rokietnikiem", "rokietniku"}), PL_MESKI_NOS_NZYW); 
    
    set_id_long("Masz przed soba zielony, dosc wilgotny mech zwany rokietnikiem. Czesto "+
	"wystepuje w sciolce lesnej.\n");

    set_unid_long("Jest to zielony, stosunkowo wilgotny mech.\n"); 

    set_decay_time(235);
    set_id_diff(35);
    set_find_diff(4);   
    set_amount(11);
    set_herb_value(58);
}
