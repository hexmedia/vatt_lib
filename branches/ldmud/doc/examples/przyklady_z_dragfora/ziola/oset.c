inherit "/std/herb";

#include "ziola.h"

create_herb()
{
    ustaw_nazwe(ROSLINA, PL_ZENSKI);

    dodaj_przym("klujacy", "klujacy");
    dodaj_przym("suchy", "susi");

    ustaw_nazwe_ziola(({"oset", "ostu", "ostowi", "oset", "ostem", "oscie"}), PL_MESKI_NOS_NZYW); 
    
    set_id_long("Masz przed soba oset, czesto spotykana, bardzo klujaca rosline. Jej zjedzenie "+
	"moze sie skonczyc podraznieniem gardla.\n");

    set_unid_long("Jest to kolczasta roslina, klujaca ci rece. Jest calkowicie ususzona.\n"); 

    set_ingest_verb(({"zjedz", "zjadasz", "zjada", "zjesc"}));
    set_effect(HERB_SPECIAL, special_effect);

    set_decay_time(45000);
    set_id_diff(2);
    set_find_diff(1);
    set_amount(2);
    set_herb_value(3);
}

int
special_effect()
{
    write("Tfu! Ziolo poklulo ci cale gardlo, a w dodatku jego smak jest nie do zniesienia!\n");
    TP->set_hp(TP->query_hp()-5-random(5));
    TP->set_mana(TP->query_mana()-2-random(2));
    TP->set_old_fatigue(TP->query_old_fatigue()-5-random(10));
    return 1;
}
