inherit "/std/herb";

#include "ziola.h"

create_herb()
{
    ustaw_nazwe(OWOC, PL_MESKI_NOS_ZYW);
  
    dodaj_przym("niewielki", "niewielcy");
    dodaj_przym("czerwony", "czerwoni");

    ustaw_nazwe_ziola(({"jarzebina", "jarzebiny", "jarzebinie", "jarzebine", "jarzebina", "jarzebinie"}), PL_MESKI_NOS_NZYW); 
    
    set_id_long("Masz przed soba zwykla jarzebina, przysmak wszelakich ptakow. Zjedzenie jej "+
	"na pewno nie pomoglo by ci w zaspokojeniu twojego glodu. Mozna sie nia powaznie zatruc.\n");

    set_unid_long("Ladnie pachnace, male twarde owoce.\n");

    set_decay_time(200);
    set_id_diff(25);
    set_find_diff(2);   
    set_amount(3);
    set_herb_value(51);

    set_ingest_verb(({"zjedz", "zjadasz", "zjada", "zjesc"}));
    set_effect(HERB_SPECIAL, special_effect);
}

void
special_effect()
{
    object kwas;
    setuid();
    seteuid(getuid());
    kwas=clone_object(EFEKT+"jarzebina_effect.c");
    kwas->move(TP);
    kwas->rozjeb_czache();
}
