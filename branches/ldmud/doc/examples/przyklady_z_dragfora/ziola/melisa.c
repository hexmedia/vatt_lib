inherit "/std/herb";

#include "ziola.h"

create_herb()
{
    ustaw_nazwe(LISC, PL_MESKI_NOS_NZYW);

    dodaj_przym("gladki", "gladcy");
    dodaj_przym("dlugi", "dludzy");

    ustaw_nazwe_ziola(({"melisa", "melisy", "melisie", "melise", "melisa", "melisie"}), PL_ZENSKI); 
    
    set_id_long("Masz przed soba melise, ziolo, ktore w odpowiedni sposob uzyte "+
	"moze zregenerowac zmeczenie.\n");

    set_unid_long("Jest to gladki lisc, ktorego zapach wydaje sie byc bardzo "+
	"przyjemny. Niestety, nie masz pojecia do czego moze sluzyc.\n"); 
    
    set_ingest_verb(({"przezuj", "przezuwasz", "przezuwa", "przezuwac"}));
    set_effect(HERB_SPECIAL, special_effect);     

    set_id_diff(8);
    set_find_diff(3);
    set_amount(11);
    set_herb_value(19);
}

int
special_effect()
{
    write("Czujesz jak wracaja ci sily.\n");
    TP->set_old_fatigue(TP->query_old_fatigue()+18+random(8));
    return 1;
}
