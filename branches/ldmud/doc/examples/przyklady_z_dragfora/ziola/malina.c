inherit "/std/herb";

#include "ziola.h"

create_herb()
{
    ustaw_nazwe(OWOC, PL_MESKI_NOS_NZYW);
  
    dodaj_przym("czerwony", "czerwoni");
    dodaj_przym("maly", "mali");

    ustaw_nazwe_ziola(({"malina", "maliny", "malinie", "maline", "malina", "malinie"}), PL_ZENSKI); 
    
    set_id_long("Masz przed soba lesny owoc - maline. Choc jest mala i malo pozywna to spora "+
	"garscia tych przepysznych owocow moze sie najesc nawet najwiekszy glodomor.\n");

    set_unid_long("Jest to zbity z wielu czerwonych kuleczek owoc. Jest delikatny i przy "+
	"nawet najdelikatniejszym dotknieciu moze ulec rozmasleniu.\n"); 
        
    set_decay_time(85);
    set_id_diff(2);
    set_find_diff(1);   
    set_amount(3);
    set_herb_value(6);
}