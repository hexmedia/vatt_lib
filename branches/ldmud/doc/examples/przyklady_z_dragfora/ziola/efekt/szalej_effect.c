inherit "/std/poison_effect.c";
#include <macros.h>
#include <pl.h>
#include <poison_types.h>

int dzialanie = 0;

create_poison_effect()
{
    set_damage( ({ POISON_HP, 60, POISON_USER_DEF, 30 }) );
    set_strength(50);
    set_poison_type("standard");
}

int
special_damage(int *damage, int i)
{
    poisonee->add_panic(random(25) + 15);
    poisonee->set_hp(poisonee->query_hp()-5-random(10));
    if(!dzialanie)
    {
        tell_object(poisonee, "Zaczynasz sie nerwowo rozgladac. Juz wiesz "
	+"ze do twojego organizmu dostala sie trucizna... Dostajesz drgawek "
	+"i goraczki. Swiat rozmazuje ci sie przed oczyma...\n");
            
        dzialanie = 1;
    }

    // Zwierze nie moze tak reagowac, jak slusznie zauwazyl Samobor, ktory 
    // walczyl tym sztyletem z wezem.
    
    if ((!function_exists("create_zwierze",poisonee))&&(!function_exists("create_potwor",poisonee)))
       {
          switch(random(4))
              {
                 case 0:
                    poisonee->catch_msg("Spogladajac pod nogi spostrzegasz plomienie... "
	+"Zaczynasz podskakiwac aby sie nie poparzyc...\n");
                    tell_room(environment(poisonee), QCIMIE(poisonee, PL_MIA) +
                       " zaczyna podskakiwac z przerazeniem wpatrujac sie "
	+"w podloze.\n",
                         poisonee);
                    break;
                 case 1:
                    {
                       poisonee->catch_msg("Robi sie coraz zimniej...\n");
                       tell_room(environment(poisonee), QCIMIE(poisonee, PL_MIA) +
                         " zaczyna sie trzasc i chuchac na rece.\n",
                         poisonee);
                       poisonee->set_hp(poisonee->query_max_hp()-15-random(15));
                    }
                    break;
                 case 2:
                    {
                        poisonee->catch_msg("Zaczynasz sie dusic!\n\n\A nie... "
	+"Zdawalo ci sie...\n");
                        tell_room(environment(poisonee), QCIMIE(poisonee, PL_MIA) +
                           " wytrzeszcza oczy i lapie sie za gardlo...\n", poisonee);


                     }
                     break;
                  case 3:
                     {
                         poisonee->catch_msg("Nic nie widzisz... tracisz wzrok!! A nie "
	+"to tylko zludzenie...\n");
                        tell_room(environment(poisonee), QCIMIE(poisonee, PL_MIA) +
                           " zaczyna pocierac oczy...\n", poisonee);
                     }
                     break;
              }
       }
     return i+2;
}