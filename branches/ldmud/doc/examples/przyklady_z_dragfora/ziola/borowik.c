inherit "/std/herb";

#include "ziola.h"

create_herb()
{
    ustaw_nazwe(GRZYB, PL_MESKI_OS);
  
    dodaj_przym("brazowy", "brazowi");
    dodaj_przym("okazaly", "okazali");

    ustaw_nazwe_ziola(({"borowik", "borowika", "borowikowi", "borowika", "borowikiem", "borowiku"}), PL_MESKI_NOS_NZYW); 
    
    set_id_long("Masz przed soba borowika szlachetnego, jednego z najsmaczniejszych i najbardziej "+
	"wartosciowych grzybow. Bije od niego przyjemny zapach.\n");

    set_unid_long("Ogladajac grzyba, dochodzisz do wniosku, ze nie masz pojecia jak sie nazywa. "+
	"Jego kapelusz jest brazowy, natomiast noga gruba.\n"); 

    set_decay_time(450);
    set_id_diff(30);
    set_find_diff(5);
    set_amount(30);
    set_herb_value(65);
}

