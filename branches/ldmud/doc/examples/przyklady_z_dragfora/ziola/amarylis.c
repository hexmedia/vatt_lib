inherit "/std/herb";

#include "ziola.h"

create_herb()
{
    ustaw_nazwe(KWIAT, PL_MESKI_NOS_NZYW);

    dodaj_przym("rozowoczerwony", "rozowoczerwoni");
    dodaj_przym("grzbiecisty", "grzbiecisci");
   
    ustaw_nazwe_ziola(({"amarylis", "amarylisu", "amarylisowi", "amarylis", "amarylisem", "amarylisie"}), PL_MESKI_NOS_NZYW); 
    
    set_id_long("Masz przed soba kwiat o wysokosci okolo jednego saznia. Ziolo to jet nazywane "+
	"amarylisem (Amaryllis belladonna). Jego korona jest grzbiecista, koloru rozowo-czerwonego. "+
	"Posiada rowniez krotka rurke. Przewaznie hoduje sie ja, jednak gdzieniegdzie rosnie "+
	"rowniez dziko.\n");

    set_unid_long("Masz przed soba rozowo-czerwony kwiat, o grzbiecistej koronie posiadajacej "+
	"rowniez krotka rurke.\n"); 

    set_id_diff(85); 
    set_find_diff(9);

    set_amount(5+random(2));   
    set_decay_time(323);
    set_herb_value(172);
}

