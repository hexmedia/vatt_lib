inherit "/std/herb";

#include "ziola.h"

create_herb()
{
    ustaw_nazwe(KLOS, PL_MESKI_NOS_NZYW);

    dodaj_przym("dlugi", "dludzy");

    ustaw_nazwe_ziola(({"zyto", "zyta", "zytu", "zyto", "zytem", "zytu"}), PL_NIJAKI_NOS); 

    set_id_long("Masz przed soba najzwyklejszy klos zyta uprawianego przez wszystkich rolnikow.\n");

    set_unid_long("Jest to klos, ktorego wasy kluja ci lekko rece. Bije "+
	"od niego niezbyt przyjemny zapach, lecz da sie go tolerowac.\n");

    set_decay_time(10000);
    set_id_diff(30);
    set_find_diff(4);   
    set_amount(15);
    set_herb_value(30);
}
