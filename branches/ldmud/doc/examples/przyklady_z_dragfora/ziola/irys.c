inherit "/std/herb";

#include "ziola.h"

create_herb()
{
    ustaw_nazwe(KWIAT, PL_MESKI_NOS_NZYW);
  
    dodaj_przym("kremowy", "kremowi");
    dodaj_przym("wysoki", "wysocy");

    ustaw_nazwe_ziola(({"irys", "irysa", "irysowi", "irysa", "irysem", "irysie"}), PL_MESKI_NOS_NZYW); 
    
    set_id_long("Na pierwszy rzut oka rozpoznajesz kwiat, ktory bardzo czesto jest hodowany "+
	"w ogrodach - irys. Pachnie on lekkim, subtelnym zapachem natomiast jego platki sa "+
	"kremowozolte.\n");

    set_unid_long("Masz przed soba subtelnie pachnacy kwiat o kremowozoltych "+
	"platkach. Kwiat jest dosc wysoki, a jego lodyga jak i liscie sa sztywne.\n"); 

    set_decay_time(264);
    set_id_diff(50);
    set_find_diff(5);
    set_amount(2);
    set_herb_value(64);
}
