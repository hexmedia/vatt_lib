inherit "/std/herb";

#include "ziola.h"

create_herb()
{
    ustaw_nazwe(OWOC, PL_MESKI_NOS_NZYW);

    dodaj_przym("duzy", "duzi");
    dodaj_przym("ciemnogranatowy", "ciemnogranatowi");
   
    ustaw_nazwe_ziola(({"siezygron", "siezygronu", "siezygronowi", "siezygron", "siezygronem", "siezygronie"}), PL_MESKI_NOS_NZYW); 
    
    set_id_long("Niezwykle rzadka jagoda siezygronu, coraz rzadziej spotykanej rosliny. Roslina siezygronu "+
	"owocuje jedynie raz w roku, tylko jedna jagoda. Wobec jego skutecznosci, i ogromnego popytu, "+
	"tempo rozmnazania zdecydowanie nie dorownuje zbieraczom. Jak dotychczas nie znany jest przypadek "+
	"trucizny, ktorej nie uleczylby siezygron po rozgryzieniu.\n");

    set_unid_long("Dziwny ciemnogranatowy owoc. Wydaje ci sie znajomy.\n"); 

    set_ingest_verb(({"rozgryz", "rozgryzasz", "rozgryza", "rozgryzc"}));
    set_effect(HERB_SPECIAL, special_effect);
    
    set_id_diff(80);
    set_find_diff(10);
    set_decay_time(250);
    set_herb_value(2500);
}

void
special_effect()
{
    write("Czujesz sie znacznie lepiej pod kazdym wzgledem.\n");
    TP->set_old_fatigue(this_player()->query_old_fatigue()+15+random(20));
    TP->heal_hp(30+random(10));
    TP->remove_stun();
}
