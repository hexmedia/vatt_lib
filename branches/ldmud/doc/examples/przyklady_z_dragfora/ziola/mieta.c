inherit "/std/herb";

#include "ziola.h"

create_herb()
{
    ustaw_nazwe(LISC, PL_MESKI_NOS_NZYW);

    dodaj_przym("wlosiasty", "wlosiasci");
   
    ustaw_nazwe_ziola(({"mieta", "miety", "miecie", "miete", "mieta", "miecie"}), PL_ZENSKI); 
    
    set_id_long("Jest to mieta, ktora powachana moze zregenerowac twoje zmeczenie.\n");

    set_unid_long("Masz przed soba wlosiasty lisc. Wydziela przyjemny zapach.\n"); 

    set_ingest_verb(({"powachaj", "wachasz", "wacha", "powachac"}));
    set_effect(HERB_SPECIAL, special_effect);       

    set_id_diff(9);
    set_find_diff(2); 
    set_amount(15);
    set_herb_value(15);
}

int
special_effect()
{
    write("Czujesz jak wracaja ci sily.\n");
    TP->set_old_fatigue(TP->query_old_fatigue()+15+random(10));
    return 1;
}
