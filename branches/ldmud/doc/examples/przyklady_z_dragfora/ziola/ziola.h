#include <herb.h>
#include <macros.h>
#include <pl.h>
#include <stdproperties.h>

#define TO      this_object()
#define TP      this_player()
#define EFEKT "/d/Faerun/ziola/efekt/"

#define KWIATOSTAN ({ "kwiatostan", "kwiatostanu", "kwiatostanwi","kwiatostan", "kwiatostanem", "kwiatostanie" }),\
        ({ "kwiatostany", "kwiatostanow", "kwiatostanom", "kwiatostany","kwiatostanami", "kwiatostanach" })

#define LISC ({ "lisc", "liscia", "lisciowi", "lisc", "lisciem", \
        "lisciu" }), ({ "liscie", "lisci", "lisciom", "liscie", "lisciami", \
        "lisciach" })

#define KWIAT ({ "kwiat", "kwiatu", "kwiatowi", "kwiat", "kwiatem", "kwiecie"}), \
        ({ "kwiaty", "kwiatow", "kwiatom", "kwiaty", "kwiatami", "kwiatach" })

#define KORZEN ({ "korzen", "korzenia", "korzeniowi", "korzen", "korzeniem","korzeniu" }),\
        ({ "korzenie", "korzeni", "korzeniom", "korzenie", "korzeniami","korzeniach" })

#define LISTEK ({ "listek", "listka", "listkowi", "listek", "listkiem","listku" }),\
        ({ "listki", "listkow", "listkom", "listki", "listkami", "listkach" })

#define KLOS ({ "klos", "klosa", "klosowi", "klos", "klosem", "klosie" }),\
        ({ "klosy", "klosow", "klosom", "klosy", "klosami", "klosach" })

#define GRZYB ({ "grzyb", "grzyba", "grzybowi", "grzyba", "grzybem","grzybie"}),\
        ({ "grzyby", "grzybow", "grzybom", "grzyby", "grzybami", "grzybach" })

#define LODYGA ({ "lodyga", "lodygi", "lodydze", "lodyge", "lodyga","lodydze"}),\
        ({ "lodygi", "lodyg", "lodygom", "lodygi", "lodygami", "lodygach" })

#define OWOC ({ "owoc", "owocu", "owocowi", "owoc", "owocem", "owocu"}),\
        ({ "owoce", "owocow", "owocom", "owoce", "owocami", "owocach" })

#define PLATEK ({ "platek", "platka", "platkowi", "platek", "platkiem","platku"}),\
        ({ "platki", "platkow", "platkom", "platki", "platkami", "platkach" })

#define ROSLINA ({ "roslina", "rosliny", "roslinie", "rosline", "roslina","roslinie"}),\
        ({ "rosliny", "roslin", "roslinom", "rosliny", "roslinami", "roslinach"})

#define MECH ({ "mech", "mchu", "mchowi", "mech", "mchem", "mchu"}),\
        ({ "mchy", "mchow", "mchom", "mchy", "mchami", "mchach" })

#define PNACZE ({ "pnacze", "pnacza", "pnaczu", "pnacze", "pnaczem","pnaczu"}),\
        ({ "pnacza", "pnaczow", "pnaczom", "pnacza", "pnaczami", "pnaczach" })

