inherit "/std/herb";

#include "ziola.h"

create_herb()
{
    ustaw_nazwe(GRZYB, PL_MESKI_OS);

    dodaj_przym("nakrapiany", "nakrapiani");
    dodaj_przym("czerwony", "czerwoni");

    ustaw_nazwe_ziola(({"muchomor czerwony", "muchomora czerwonego", "muchomorowi czerwonemu", 
                        "muchomor czerwony", "muchomorem czerwonym", "muchomorze czerwonym"}), PL_MESKI_NOS_NZYW); 
    
    set_id_long("Masz przed soba muchomor czerwony. Jest to ladny grzyb, "+
	"jednak pozory czesto myla. Posiada bardzo silna trucizne, uaktywniajaca "+
	"sie po zjedzeniu.\n");

    set_unid_long("Masz przed soba grzyba, o czerwonym kapeluszu w biale "+
	"kropki. Na trzonie posiada pierscien.\n");

    set_ingest_verb(({"zjedz", "zjadasz", "zjada", "zjesc"}));      
    set_effect(HERB_SPECIAL, special_effect);
    
    set_decay_time(187);
    set_id_diff(40);
    set_find_diff(2);   
    set_amount(15);
    set_herb_value(76);
}

void
special_effect()
{
    object czer;
    setuid();
    seteuid(getuid());
    czer=clone_object(EFEKT+"muchomorc_effect.c");
    czer->move(TP);
    czer->rozjeb_czache();
}