inherit "/std/herb";

#include "ziola.h"

create_herb()
{
    ustaw_nazwe(GRZYB, PL_MESKI_OS);

    dodaj_przym("pofaldowany", "pofaldowani");
    dodaj_przym("wielki", "wielcy");

    ustaw_nazwe_ziola(({"szmaciak", "szmaciaka", "szmaciakowi", "szmaciaka", "szmaciakiem", "szmaciaku"}), PL_MESKI_NOS_NZYW); 

    set_id_long("Masz przed soba szmaciaka galezistego, jest grzybem z klasy "+
	"podstawczakow. Jego owocniki sa ciemno, lub bladawozolte bardzo pofaldowane. Grzyb "+
	"ten nie zawsze posiada trzon.\n");

    set_unid_long("Masz przed soba silnie pofaldowanego, bladozoltego grzyba. O dziwo "+
	"nie posiada wcale trzonu, bije od niego przyjemny orzechowy zapach.\n"); 

    set_decay_time(550);
    set_id_diff(75);
    set_find_diff(8);
    set_amount(60);
    set_herb_value(853);
}