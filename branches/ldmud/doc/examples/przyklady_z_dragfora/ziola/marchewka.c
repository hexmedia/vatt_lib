inherit "/std/herb";

#include "ziola.h"

create_herb()
{
    ustaw_nazwe(KORZEN, PL_MESKI_NOS_NZYW);

    dodaj_przym("rudopomaranczowy", "rudopomaranczowi");

    ustaw_nazwe_ziola(({"marchewka", "marchewki", "marchewce", "marchewke", "marchewka", "marchewce"}), PL_ZENSKI); 
    
    set_id_long("Patrzac na ziolo do razu poznajesz tak pospolita i smaczna marchewke. Wiesz "+
	"napewno, ze ci nie zaszkodzi, ale takze w niczym nie pomoze - najwyzej moze zaspokoic glod.\n");

    set_unid_long("Jest to rudopomaranczowy korzen, dosc brudny od ziemi i innych zwiazkow "+
	"znajdujacych sie w glebie.\n"); 

    set_decay_time(500);
    set_id_diff(15);
    set_find_diff(1);
    set_amount(35);
    set_herb_value(18);
}
