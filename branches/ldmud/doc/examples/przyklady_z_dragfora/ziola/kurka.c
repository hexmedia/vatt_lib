inherit "/std/herb";

#include "ziola.h"

create_herb()
{
    ustaw_nazwe(GRZYB, PL_MESKI_OS);

    dodaj_przym("zolty", "zolci");
    dodaj_przym("nieduzy", "nieduzi");

    ustaw_nazwe_ziola(({"kurka", "kurki", "kurce", "kurke", "kurka", "kurce"}), PL_ZENSKI); 
    
    set_id_long("Na pierwszy rzut oka rozpoznajesz kurke.\n");

    set_unid_long("Jest to dziwny, zolty grzyb. Jego kapelusz jest blaszkowaty, caly grzyb "+
	"jest bardzo twardy i zbity.\n"); 

    set_decay_time(1500);
    set_id_diff(10);
    set_find_diff(1);
    set_amount(14);
    set_herb_value(35);
}

