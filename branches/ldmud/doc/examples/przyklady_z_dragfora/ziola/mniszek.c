inherit "/std/herb";

#include "ziola.h"

create_herb()
{
    ustaw_nazwe(KWIAT, PL_MESKI_NOS_NZYW);
  
    dodaj_przym("zolty", "zolci");
    dodaj_przym("duzy", "duzi");

    ustaw_nazwe_ziola(({"mniszek", "mniszka", "mniszkowi", "mniszek", "mniszkiem", "mniszku"}), PL_MESKI_NOS_NZYW); 
    
    set_id_long("Masz przed soba mniszek lekarski, znany rowniez pod innymi nazwami - "+
	"mlecz czy dmuchawiec. Mleczko znajdujace sie w koncu powoduje zasklepienie "+
	"sie ran.\n");

    set_unid_long("Dziwny kwiat, o grubej lodydze oraz zoltym kwiecie.\n");

    set_ingest_verb(({"potrzyj", "pocierasz", "pociera", "pocierac"}));
    set_effect(HERB_SPECIAL, special_effect);

    set_decay_time(99);
    set_id_diff(10);
    set_find_diff(1);   
    set_amount(9);
    set_herb_value(19);
}

void
special_effect()
{
    write("Czujesz sie lepiej.\n");
    TP->heal_hp(7+random(8));
}