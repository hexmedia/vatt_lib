inherit "/d/std/zbroja";
#include <stdproperties.h>
#include <macros.h>
#include <pl.h>
#include <wa_types.h>
#include <ss_types.h>

#define A "helm_rycerski"
#define TP this_player()
#define TO this_object()


void
create_armour()
{
    set_autoload();
    ustaw_nazwe(({"przylbica","przylbicy","przylbicy","przylbice","przylbica",
               "przylbicy"}), ({"przylbice","przylbic","przylbicom",
                    "przylbice","przylbicami","przylbicach"}), PL_ZENSKI);

    ustaw_nazwe(({"helm", "helmu", "helmowi", "helm", "helmem",
                  "helmie" }), ({ "helmy", "helmow", "helmom",
                  "helmy", "helmami", "helmach" }), PL_MESKI_NOS_NZYW );


    dodaj_przym("lsniacy","lsniacy");
    dodaj_przym("ciezki","ciezcy");

    set_long("Helm z przylbica wykonano z jakiegos lsniacego metalu. Z cala "
    +"pewnoscia zapewnia on bardzo dobra ochrone przed ciosami, gdyz oprocz "
    +"glowy chroni takze twarz wojownika.\n");

    set_ac(A_HEAD, 30, 30, 30);
    set_af(this_object());

}

int
wear( object ob )
{
    write("Zakladasz helm przyslaniajac twarz przylbica.\n");
    say(QCIMIE(TP,PL_MIA)+" zaklada helm przyslaniajac twarz przylbica.\n");
    TP->remove_subloc(A);
    TP->add_subloc(A,TO);
    TO->move(TP,A);
    return 1;
}

int
remove( object ob )
{
    write("Zdejmujesz helm.\n");
    say(QCIMIE(TP,PL_MIA)+" zdejmuje helm z przylbica.\n");
    TP->remove_subloc(A);
    return 1;
}

string
show_subloc( string subloc, object ob, object obf )
{

    if ( subloc != A ) return "";
    if ( obf == ob ) return "";
    return "Spod przylbicy spoglada na ciebie zimne spojrzenie "+
            "nieustraszonego rycerza.\n";
}

