
inherit "/d/std/zbroja";
#include "../pal.h"

void create_armour()
{
    set_autoload();
    ustaw_nazwe(
    ({ "kaftan", "kaftana", "kaftanowi", "kaftan", "kaftanem", "kaftanie" }),
    ({ "kaftany", "kaftanow", "kaftanom", "kaftany", "kaftanami", "kaftanach" }),
    PL_MESKI_NOS_NZYW);

    dodaj_przym("czarny", "czarni");
    dodaj_przym("cwiekowany","cwiekowani");

    set_long("Zwykly, skorzany kaftan nabijany srebrnymi cwiekami. Takie kaftany nosi "
    +"wielu mieszczan, gdyz sa bardzo modne, lekkie i wygodne.\n");
             
    set_ac(A_BODY, 7, 8, 7);
    add_prop(OBJ_I_VOLUME, query_default_weight() / 6);
}
