/*
 * Filename: zbroja_plytowa.c
 *
 * Zastosowanie: Zbroja plytowa.
 * 
 * Copyright Volothamp Geddarm.
 *
 */
inherit "/d/Faerun/std/zbroja.c";

#include <stdproperties.h>
#include <wa_types.h>
#include <formulas.h>
#include <macros.h>

void
create_armour()
{
	set_autoload();

    ustaw_nazwe( ({"zbroja plytowa", "zbroi plytowej", "zbroi plytowej", "zbroje plytowa", "zbroja plytowa", "zbroi plytowej"}),
		         ({"zbroje plytowe", "zbroi plytowych", "zbrojom plytowym", "zbroje plytowe", "zbrojami plytowymi", "zbrojach plytowych"}), PL_ZENSKI);
    
	dodaj_przym("pelny", "pelni");

    set_long("Ta oto pieknie wypolerowana i niemalze blyszczaca zbroja plytowa wykonana jest z doskonale "
	  + "dopasowanych elementow stworzonych z litego metalu. Sa one dodatkowo wymodelowane tak aby odbijaly "
	  + "uderzenia broni roznego typu, a w tym i strzal. Zwykle tego typu uzbrojenie jest zdobione, upiekszane "
	  + "badz grawerowane lecz w tym momencie jedyne co nan dostrzegasz to osobisty znak slynnej kuzni polozonej "
	  + "w Beregoscie. Mimo to wojownik, ktory zechce zalozyc ow plytowy pancerz bedzie wygladal bardzo "
	  + "elegancko, a zarazem bardzo niebezpiecznie.\n");

    set_ac(A_BODY, 44, 44, 44,
		   A_ARMS, 42, 42, 42,
		   A_LEGS, 40, 40, 40);

    add_prop(OBJ_I_VOLUME, query_default_weight() / 6);
	add_prop(OBJ_I_WEIGHT, 17000);
	add_prop(OBJ_I_VALUE, 21100);
}


