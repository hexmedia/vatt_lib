/*
 * Filename: buty.c
 *
 * Zastosowanie: Buty.
 * 
 * Copyright Volothamp Geddarm.
 *
 */
inherit "/d/Faerun/std/zbroja.c";

#include <stdproperties.h>
#include <wa_types.h>
#include <formulas.h>
#include <macros.h>

void
create_armour()
{
	set_autoload();

    dodaj_nazwy(({ "buty", "butow", "butom", "buty", "butami", "butach" }),
                PL_MESKI_NOS_NZYW);

	ustaw_nazwe(({ "para butow", "pary butow", "parze butow",
        "pare butow", "para butow", "parze butow" }), ({ "pary butow", 
        "par butow", "parom butow", "pary butow", "parami butow",
        "parach butow" }), PL_ZENSKI);
	
	ustaw_shorty(({ "para wysokich okutych butow", "pary wysokich okutych butow", 
        "parze wysokich okutych butow", "pare wysokich okutych butow", 
        "para wysokich okutych butow", "parze wysokich okutych butow" }), 
        ({ "pary wysokich okutych butow", "par wysokich okutych butow", 
        "parom wysokich okutych butow", "pary wysokich okutych butow", 
        "parami wysokich okutych butow", "parach wysokich okutych butow" }), PL_ZENSKI);
    
	dodaj_przym("wysoki", "wysocy");
	dodaj_przym("okuty", "okuci");

    set_long("Sa to wysokie buty uszyte z twardej skory, ktora na dodatek postanowiono "
	  + "wzmocnic zelaznymi okuciami. Posiadaja one rowniez podwojna podeszwe i "
	  + "miekka wkladke, ktora zapewnia komfort i wygode podczas dlugich, zbrojnych "
      + "marszow. Po ich wewnetrznej stronie dostrzegasz osobisty znak slynnej kuzni "
	  + "polozonej w Beregoscie.\n");

    set_ac(A_FEET, 6, 6, 6);

    add_prop(OBJ_I_VOLUME, query_default_weight() / 6);
	add_prop(OBJ_I_WEIGHT, 4500);
	add_prop(OBJ_I_VALUE, 450);
}


