inherit "/d/std/zbroja";
#include <ss_types.h>
#include <wa_types.h>
#include <macros.h>
#include <stdproperties.h>

void
create_armour()
{
   set_autoload();
   ustaw_nazwe(({"kirys","kirysa","kirysowi","kirys","kirysem","kirysie"}),
     ({"kirysy","kirysow","kirysom","kirysy","kirysami","kirysach"}),
     PL_MESKI_NZYW);

   dodaj_przym("kryty","kryci");
   dodaj_przym("blyszczacy","blyszczacy");

   set_long("Blyszczacy napiersnik z jakiegos drogiego metalu polaczono tu"
   +" z takim samym naplecznikiem przy pomocy skorzanych zaczepow. Kirys "
   +" ten wyglada na bardzo wytrzymaly, gdyz pokryty jest dodatkowo jakims "
   +" utwardzeniem. Na piersi dostrzegasz znak strazy swiatynnej Paladina.\n");
           
            set_ac(A_BODY, 25, 26, 25);
                      
            add_prop(OBJ_I_VOLUME, 5000);
}

