inherit "/d/std/zbroja";
#include <ss_types.h>
#include <wa_types.h>
#include <macros.h>
#include <stdproperties.h>

void
create_armour()
{
   set_autoload();
   ustaw_nazwe(({"kirys","kirysa","kirysowi","kirys","kirysem","kirysie"}),
     ({"kirysy","kirysow","kirysom","kirysy","kirysami","kirysach"}),
     PL_MESKI_NZYW);

   dodaj_przym("lekki","leccy");
   dodaj_przym("solamnijski","solamnijscy");

   set_long("Ta dziwna zbroja sklada sie na lekki stalowy napiersnik, ktory polaczono tu"
   +" z takim samym naplecznikiem przy pomocy skorzanych zaczepow. Kirys "
   +" ten nie wyglada na bardzo wytrzymaly, jednak jest bardzo lekki i nie obciaza "
   +"zbytnio noszacej go osoby.\n");
           
            set_ac(A_BODY, 14, 15, 14);
                      
            add_prop(OBJ_I_VOLUME, 5000);
}

