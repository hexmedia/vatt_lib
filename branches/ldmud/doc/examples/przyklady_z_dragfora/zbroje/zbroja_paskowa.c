/*
 * Filename: zbroja_paskowa.c
 *
 * Zastosowanie: Zbroja paskowa.
 * 
 * Copyright Volothamp Geddarm.
 *
 */
inherit "/d/Faerun/std/zbroja.c";

#include <stdproperties.h>
#include <wa_types.h>
#include <formulas.h>
#include <macros.h>

void
create_armour()
{
	set_autoload();

    ustaw_nazwe( ({"zbroja paskowa", "zbroi paskowej", "zbroi paskowej", "zbroje paskowa", "zbroja paskowa", "zbroi paskowej"}),
		         ({"zbroje paskowe", "zbroi paskowych", "zbrojom paskowym", "zbroje paskowe", "zbrojami paskowymi", "zbrojach paskowych"}), PL_ZENSKI);
    
	dodaj_przym("mocny", "mocni");
    set_long("Jest to mocna zbroja wykonana z pojedynczych rzemieni zapewniajacych ochrone "
	  + "jeszcze lepsza anizeli twarde kolczugi. Mozesz zalozyc ja tak, aby pokrywala i chronila tulow "
	  + "oraz ramiona. Przetrwa starcia zarowno z ogrzymi maczugami, gnollimi buzdyganami jak i "
	  + "waskimi koboldzkimi mieczami. Jedyna jej wada jest ciezar i to, ze dostatecznie ogranicza "
	  + "szerokie ruchy. Dostrzegasz na jej powierzchni osobisty znak slynnej kuzni polozonej w "
	  + "Beregoscie.\n");

    set_ac(A_BODY, 23, 24, 24,
		   A_ARMS, 22, 21, 20);
    add_prop(OBJ_I_VOLUME, query_default_weight() / 6);
	add_prop(OBJ_I_WEIGHT, 8500);
	add_prop(OBJ_I_VALUE, 1350);
}


