/*
 * Filename: sandaly.c
 *
 * Zastosowanie: Sandaly dla mieszkancow Beregostu.
 * 
 * Copyright Volothamp Geddarm.
 *
 */
inherit "/d/Faerun/std/stdubrania.c";

#include <wa_types.h>
#include <stdproperties.h>
#include <pl.h>

void
create_ubranie() 
{
    dodaj_nazwy(({ "sandaly", "sandalow", "sandalom", "sandaly", "sandalami", "sandalach" }),
                PL_ZENSKI);
	ustaw_nazwe(({ "para sandalow", "pary sandalow", "parze sandalow",
        "pare sandalow", "para sandalow", "parze sandalow" }), ({ "pary sandalow", 
        "par sandalow", "parom sandalow", "pary sandalow", "parami sandalow",
        "parach sandalow" }), PL_ZENSKI);
    ustaw_shorty(({ "para lekkich sandalow", "pary lekkich sandalow", 
        "parze lekkich sandalow", "pare lekkich sandalow", 
        "para lekkich sandalow", "parze lekkich sandalow" }), 
        ({ "pary lekkich sandalow", "par lekkich sandalow", 
        "parom lekkich sandalow", "pary lekkich sandalow", 
        "parami lekkich sandalow", "parach lekkich sandalow" }), PL_ZENSKI);
    dodaj_przym("lekki", "leccy");
    set_long("Sa to zwykle sandaly wykonane z cienkich rzemykow, ktore mocuja "
	  + "je do stopy. W takim obuwiu poruszaja sie zwykli mieszczanie, urzednicy, "
	  + "niewolnicy podczas trwania pory letniej. Sadzisz, ze nie nadaja sie one "
	  + "do dreptania po snieznych zaspach.\n");
set_slots(A_FEET);
	add_prop(OBJ_I_VALUE, 10);
}


