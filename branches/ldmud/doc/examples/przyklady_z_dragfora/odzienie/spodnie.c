/*
 * Filename: spodnie.c
 *
 * Zastosowanie: Spodnie dla mlodzienca.
 * 
 * Copyright Volothamp Geddarm.
 *
 */
inherit "/d/Faerun/std/stdubrania.c";

#include <stdproperties.h>
#include <wa_types.h>
#include <formulas.h>
#include <macros.h>

void
create_ubranie()
{
    ustaw_nazwe( ({"spodnie", "spodni", "spodniom", "spodnie", "spodniami", "spodniach"}),
		         ({"spodnie", "spodni", "spodniom", "spodnie", "spodniami", "spodniach"}), PL_NIJAKI_NOS);

	ustaw_shorty( ({"para ciemnych gustownych spodni", "pary ciemnych gustownych spodni", "parze ciemnych gustownych spodni", "pare ciemnych gustownych spodni", "para ciemnych gustownych spodni", "parze ciemnych gustownych spodni"}),
		          ({"pary ciemnych gustownych spodni", "par ciemnych gustownych spodni", "parom ciemnych gustownych spodni", "pary ciemnych gustownych spodni", "parami ciemnych gustownych spodni", "parach ciemnych gustownych spodni"}), PL_ZENSKI);

    dodaj_przym("ciemny", "ciemni");
    dodaj_przym("gustowny", "gustowni");
    set_long("Sa to dlugie, siegajace kostek spodnie wykonane z ciemnego materialu. "
	  + "Wygladaja na odzienie w miare eleganckie lecz i gustowne oraz nonszalanckie. "
+ "Tak ubieraja sie zazwyczaj atrakcyjni mlodziency, ktorzy cale dnie spedzaja "
	  + "w karczmach wznoszac toasty za zdrowie karczemnych dziewek i rozbijajac o "
	  + "kant stolu kolejne lampki czerwonego wina.\n");
    set_slots(A_LEGS);
    add_prop(OBJ_I_VOLUME, query_default_weight() / 6);
	add_prop(OBJ_I_VALUE, 10);
}


