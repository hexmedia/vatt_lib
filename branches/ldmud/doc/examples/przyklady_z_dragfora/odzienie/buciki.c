/*
 * Filename: buciki.c
 *
 * Zastosowanie: Buciki.
 * 
 * Copyright Volothamp Geddarm.
 *
 */
inherit "/d/Faerun/std/stdubrania.c";

#include <stdproperties.h>
#include <wa_types.h>
#include <formulas.h>
#include <macros.h>

void
create_ubranie() 
{
    dodaj_nazwy(({ "buty", "butow", "butom", "buty", "butami", "butach" }), PL_ZENSKI);
	dodaj_nazwy(({ "buciki", "bucikow", "bucikom", "buciki", "bucikami", "bucikach" }), PL_ZENSKI);
	ustaw_nazwe(({ "para bucikow", "pary bucikow", "parze bucikow",
        "pare bucikow", "para bucikow", "parze bucikow" }), ({ "pary bucikow", 
        "par bucikow", "parom bucikow", "pary bucikow", "parami bucikow",
        "parach bucikow" }), PL_ZENSKI);
    ustaw_shorty(({ "para nieduzych bucikow", "pary nieduzych bucikow", 
        "parze nieduzych bucikow", "pare nieduzych bucikow", 
        "para nieduzych bucikow", "parze nieduzych bucikow" }), 
        ({ "pary nieduzych bucikow", "par nieduzych bucikow", 
        "parom nieduzych bucikow", "pary nieduzych bucikow", 
        "parami nieduzych bucikow", "parach nieduzych bucikow" }), PL_ZENSKI);
    dodaj_przym("nieduzy", "nieduzi");
    set_long("Sa to nieduze rozmiarem buciki - prawdopodobnie trzewiki. Wykonano "
	  + "je prawdopodobnie albo na stope dziecka albo na stope gnoma czy niziolka. "
	  + "Sadzisz jednak, ze w tym przypadku moga to byc trzewiki dla niziolka, gdyz "
	  + "sa gustowne lecz zarazem wygodne.\n");
    set_slots(A_FEET);
	add_prop(OBJ_I_VALUE, 10);
}


