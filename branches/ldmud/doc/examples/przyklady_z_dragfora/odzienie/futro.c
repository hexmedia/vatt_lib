/*
 * Filename: futro.c
 *
 * Zastosowanie: Futro dla mieszczanki.
 * 
 * Copyright Volothamp Geddarm.
 *
 */
inherit "/d/Faerun/std/stdubrania.c";

#include <stdproperties.h>
#include <wa_types.h>
#include <formulas.h>
#include <macros.h>

void
create_ubranie()
{
    ustaw_nazwe( ({"futro", "futra", "futrze", "futro", "futrem", "futrze"}),
		         ({"futra", "futr", "futrom", "futra", "futrami", "futrach"}), PL_NIJAKI_NOS);
    
	dodaj_przym("gruby", "grubi");
    dodaj_przym("zimowy", "zimowi");
    set_long("Jest to ocieplane futro zwierzece bedace jednoczesnie oznaka "
	  + "pewnej przecietnej elegancji jak i dobrym sposobem na uchronienie "
	  + "sie przed zimowym mrozem. Ma ono dwie kieszenie, zas poszewka jest "
	  + "delikatna i miekka pod dotykiem. Uwazasz jednak, ze "
	  + "zapocil" + this_player()->koncowka("bys", "abys") + " sie w tym podczas "
	  + "slonecznego lata.\n");
    set_slots(A_BODY, A_ARMS);
    add_prop(OBJ_I_VOLUME, query_default_weight() / 6);
	add_prop(OBJ_I_VALUE, 10);
}


