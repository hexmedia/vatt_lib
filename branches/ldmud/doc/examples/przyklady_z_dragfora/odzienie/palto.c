/*
 * Filename: palto.c
 *
 * Zastosowanie: Palta nie widziales?
 * 
 * Copyright Volothamp Geddarm.
 *
 */
inherit "/d/Faerun/std/stdubrania.c";

#include <stdproperties.h>
#include <wa_types.h>
#include <formulas.h>
#include <macros.h>

void
create_ubranie()
{

    ustaw_nazwe( ({"palto", "palta", "paltu", "palto", "paltem", "palcie"}),
		         ({"palta", "palt", "paltom", "palta", "paltami", "paltach"}), PL_NIJAKI_NOS);
    
	dodaj_przym("krotki","krotcy");
	dodaj_przym("wygodny", "wygodni");
    
	set_long("Takie palto, czyli krotki plaszczyk stanowi nienajgorsza ochrone podczas podrozy, "
      + "totez wielu doswiadczonych wedrowcow, stanowczo je poleca. Grube lecz wygodne, "
      + "miekkie a eleganckie. To sa dobre strony posiadania takiego ubioru podczas " 
      + "licznych wedrowek czy dlugiej podrozy. Po jego wewnetrznej stronie dostrzegasz "
      + "niewielki znaczek pracowni krawieckiej 'Krolewski Gryf'.\n");
    
	set_slots(A_ROBE);

    add_prop(OBJ_I_WEIGHT, 50);
	add_prop(OBJ_I_VALUE, 355);
}


