/*
 * Filename: kapelusz1.c
 *
 * Zastosowanie: Kapelusz dlakupca w gospodzie.
 * 
 * Copyright Volothamp Geddarm.
 *
 */
inherit "/d/Faerun/std/stdubrania.c";

#include <stdproperties.h>
#include <wa_types.h>
#include <formulas.h>
#include <macros.h>

void
create_ubranie()
{
    ustaw_nazwe( ({"kapelusz", "kapelusza", "kapeluszowi", "kapelusz", "kapeluszem", "kapeluszu"}),
		         ({"kapelusze", "kapeluszy", "kapeluszom", "kapelusze", "kapeluszami", "kapeluszach"}), PL_MESKI_NOS_NZYW);
    dodaj_przym("elegancki", "eleganccy");
	dodaj_przym("szeroki", "szerocy");
    set_long("Jest to gustowny i elegancki kapelusz o szerokim rondzie, ktory "
	  + "przewaznie noszony jest przez postawnych kupcow obeznanych w trudnej "
	  + "sztuce handlu i znajacych jej tajniki. Ma on troche miejsca, aby przyczepic "
	  + "don cos malego na przyklad piorko badz nawet mala, poreczna kusze 'protektor', "
	  + "o ktrych wiele sie mowi ostatnimi czasy.\n");
    
	set_slots(A_HEAD);

    add_prop(OBJ_I_WEIGHT, 500);
	add_prop(OBJ_I_VALUE, 10);
}


