/*
 * Filename: suknia1.c
 *
 * Zastosowanie: Suknia dla Jasmine.
 * 
 * Copyright Volothamp Geddarm.
 *
 */
inherit "/d/Faerun/std/stdubrania.c";

#include <stdproperties.h>
#include <wa_types.h>
#include <formulas.h>
#include <macros.h>

void
create_ubranie()
{
    ustaw_nazwe( ({"suknia", "sukni", "sukni", "suknie", "suknia", "sukni"}),
		         ({"suknie", "sukni", "sukniom", "suknie", "sukniami", "sukniach"}), PL_ZENSKI);
    
	dodaj_przym("wymiety", "wymieci");
    dodaj_przym("szary", "szarzy");
    set_long("Jest to dluga, nieco wymieta juz suknia, w ktora bywaja ubrane "
	  + "z reguly starsze kobiety o czym swiadczy ponury, szary kolor, ktory "
	  + "zastepuje noszone przez mlodych ludzi krzykliwe kolory. Wyglada ona "
	  + "na bardzo ladna i nieco kosztowna choc i tak ma juz swoje lata i "
	  + "nadgryziona jest przez potezny zab przemijajacego czasu.\n");
    set_slots(A_BODY, A_ARMS);
    add_prop(OBJ_I_VOLUME, query_default_weight() / 6);
	add_prop(OBJ_I_VALUE, 10);
}


