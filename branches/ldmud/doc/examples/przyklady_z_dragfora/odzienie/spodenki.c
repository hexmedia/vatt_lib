/*
 * Filename: spodenki.c
 *
 * Zastosowanie: Spodenki.
 * 
 * Copyright Volothamp Geddarm.
 *
 */
inherit "/d/Faerun/std/stdubrania.c";

#include <wa_types.h>
#include <stdproperties.h>
#include <pl.h>

void
create_ubranie() 
{
    dodaj_nazwy(({ "spodenki", "spodenek", "spodenkom", "spodenki", "spodenkami", "spodenkach" }), PL_ZENSKI);
	dodaj_nazwy(({ "spodnie", "spodni", "spodniom", "spodnie", "spodniami", "spodniach" }), PL_ZENSKI);
	ustaw_nazwe(({ "para spodenek", "pary spodenek", "parze spodenek",
        "pare spodenek", "para spodenek", "parze spodenek" }), ({ "pary spodenek", 
        "par spodenek", "parom spodenek", "pary spodenek", "parami spodenek",
        "parach spodenek" }), PL_ZENSKI);
    ustaw_shorty(({ "para spodenek na szelkach", "pary spodenek na szelkach", 
        "parze spodenek na szelkach", "pare spodenek na szelkach", 
        "para spodenek na szelkach", "parze spodenek na szelkach" }), 
        ({ "pary spodenek na szelkach", "par spodenek na szelkach", 
        "parom spodenek na szelkach", "pary spodenek na szelkach", 
        "parami spodenek na szelkach", "parach spodenek na szelkach" }), PL_ZENSKI);
    set_long("Sa to bardzo ladne, male spodenki, do ktorych przeczepiono "
	  + "podtrzymujace je w pasie szeleczki. Nie wiesz czy nosza cos takiego dzieci "
	  + "czy tez niziolki lecz wyglada to w gruncie rzeczy bardzo elegancko i gustownie.\n");
    set_slots(A_LEGS);
	add_prop(OBJ_I_VALUE, 10);
}


