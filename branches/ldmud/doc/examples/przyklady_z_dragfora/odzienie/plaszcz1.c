/*
 * Filename: plaszcz1.c
 *
 * Zastosowanie: Plaszcza nie widziales?
 * 
 * Copyright Volothamp Geddarm.
 *
 */
inherit "/d/Faerun/std/stdubrania.c";

#include <stdproperties.h>
#include <wa_types.h>
#include <formulas.h>
#include <macros.h>

void
create_ubranie()
{

    ustaw_nazwe( ({"plaszcz", "plaszcza", "plaszczowi", "plaszcz", "plaszczem", "plaszczu"}),
		         ({"plaszcze", "plaszczy", "plaszczom", "plaszcze", "plaszczami", "plaszczach"}), PL_MESKI_NOS_NZYW);
    
	dodaj_przym("dlugi","dludzy");
	dodaj_przym("ciemny","ciemni");
    
	set_long("Jest to wykonany z grubego, skorzanego i ciemnego materialu plaszcz, ktory "
	  + "dodatko zaopatrzono w kaptur, ktory ma mozliwosc przesloniecia twarzy noszacej go "
	  + "osoby. Nie ma na sobie zadnych wzorow za to po wewnetrznej i zewnetrznej stronie "
	  + "posiada liczne kieszenie sluzace czesto jako skrytki. Z tego wlasnie wzgledu "
	  + "osoby poruszajace sie w podobnym odzieniu budza strach i groze w zwyklych ludziach, "
	  + "gdyz takie plaszcze zainteresowaniem darza glownie zlodzieje i skrytobojcy.\n");
    
	set_slots(A_ROBE);

    add_prop(OBJ_I_WEIGHT, 50);
	add_prop(OBJ_I_VALUE, 500);
}


