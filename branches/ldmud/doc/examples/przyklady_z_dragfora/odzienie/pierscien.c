#include "giogi.h"
#pragma strict_types
inherit "/std/armour";
inherit "/cmd/std/command_driver";
#include <stdproperties.h>
#include <wa_types.h>
#include <formulas.h>
#include <cmdparse.h>
#include <composite.h>
#include <macros.h>
#include <pl.h>
#include <ss_types.h>
#include <filter_funs.h>
#include <options.h>
#define TO this_object()
#define TP this_player()
#define DPIERSCIEN_SUBLOC "dpierscien_subloc"
#define DEMONPATH TPATH+"sluga.c"

object demon;
int ptak=0;
int mana = 200;

void
create_armour()
{
  ustaw_nazwe(({"pierscien","pierscienia","pierscieniowi","pierscien","pierscieniem","pierscieniu"}),
              ({"pierscienie","pierscieni","pierscieniom","pierscienie","pierscieniami","pierscieniach"}),
                 PL_MESKI_NOS_NZYW);
ustaw_shorty(({ "srebrny pierscien z rubinem", "srebrnego pierscienia z rubinem", 
      		"srebrnemu pierscieniowi z rubinem", "srebrny pierscien z rubinem", 
      		"srebrnym pierscieniem z rubinem", "srebrnym pierscieniu z rubinem" }),
      	     ({ "srebrne pierscienie z rubinem", "srebrnych pierscieni z rubinem", 
                "srebrnym pierscieniom z rubinem", "srebrne pierscienie z rubinem", 
      		"srebrnymi pierscieniami z rubinem", "srebrnych pierscieniach z rubinem" }), 
                 PL_MESKI_NOS_NZYW);

    add_name("dpierscien");  

    dodaj_przym("srebrny","srebrni");
    
    add_prop(OBJ_M_NO_SELL, "Tylko glupiec by sprzedal ten pierscien... " +
    "a ty nie jestes glupcem, czyz nie?\n");
    add_prop(OBJ_M_NO_DROP, "Tylko glupiec pozbylby sie tego pierscienia... " +
    "a ty nie jestes glupcem, czyz nie?\n");
    add_prop(OBJ_M_NO_GIVE, "Tylko glupiec podarowalby komus ten pierscien... " +
    "a ty nie jestes glupcem, czyz nie?\n");
    
    set_long("Przygladasz sie niewiarygodnie misternie wykonanemu, "+
    "srebrnemu pierscieniowi ozdobionemu olbrzymim rubinem. Magiczna "+
    "aura tego przedmiotu jest tak wielka, ze wrecz zdaje sie ze mozna "+
    "dotknac jej reka. To, ze cala obraczka pokryta jest runami, a "+
    "rubin swieci wlasnym swiatlem, tylko wyolbrzymiaja to mistyczne "+
    "wrazenie.\n" +
    "Jedyny zrozumialy dla ciebie napis zostal umieszczony na "+
    "wewnetrznej stronie pierscienia, brzmi on <dpomoc>.\n");

    set_ac(A_ANY_FINGER,10,10,10);
    add_prop(MAGIC_AM_MAGIC, ({ 100, "enchantment"})); 
	add_prop(MAGIC_AM_ID_INFO, ({30,
	"Pierscien ten tylko, w rekach zlego moc swa objawi.",
	40,"Wrogow kula ognista porazi, tym silniejsza, im "+
	"wladca pierscienia silnij z mrokiem zwiazan bedzie"}) );
	add_prop(OBJ_I_IS_MAGIC_ARMOUR,1);
    add_prop(OBJ_I_VALUE, 4000);
    add_prop(OBJ_I_WEIGHT, 2);
    add_prop(OBJ_I_VOLUME, 2);
    add_prop(OBJ_I_LIGHT, 1);
    set_alarm(1.0,0.0,"laduj");
    TP->add_prop("pierscien_ran", 1);
    this_object()->set_af(this_object());	

    add_item(({"runy", "napis", "napisy", "znaczki"}), 
    "Misternie wykonane runy pokrywaja cala powierzchnie pierscienia. "+
    "Nie wiesz czemu, ale masz dziwne przeczucie, ze nie wykonal ich zaden "+
    "mieszkaniec I sfery materialnej...\n");

    add_item(({"rubin", "klejnot"}), 
    "Ten olbrzymi kamien szlachetny zostal wprawiony w pierscien. "+
    "Mimo, ze zdaje sie to nieprawdopodobne, to klejnot wytwarza "+
    "wlasne, zlowieszcze swiatlo!\n");
}

/*
 * -------------------------------------
 * Zakladanie itemu                    |
 * -------------------------------------
 */

mixed
wear(object armour)
{
    wearer=TP;
    write("Powolnym ruchem zakladasz na swoj palec " +short(PL_BIE)+ ". Nagle "+
    "klejnot rozblyskuje piekielnym, rubinowym swiatlem. W swej glowie slyszysz "+
    "demoniczny glos: ''Witaj " + this_player()->koncowka("moj Mistrzu", "moja Mistrzyni") +"''.\n");
    say(QCIMIE(this_player(),PL_MIA)+ " zaklada " +short(PL_BIE)+ ". Nagle " +
    "klejnot rozblyskuje piekielnym, rubinowym swiatlem. Gdy wraca Ci zdolnosc " +
    "widzenia, " + TP->query_rasa(PL_MIA) + " stoi z zalozona ozdoba jakby nic " +
    "sie nie stalo.\n");

    this_player()->set_combat_speed(this_player()->query_combat_speed()+45);
    this_player()->add_subloc(DPIERSCIEN_SUBLOC, this_object());    

    return 1;
}

/*
 * -------------------------------------
 * Zdejmowanie itemu                   |
 * -------------------------------------
 */
mixed
remove(object armour)
{

   object *kogo;
   
      kogo = ({TO->query_worn()});
  
      kogo[0]->catch_msg("Zdejmujesz z siebie pierscien. Slyszysz " +
      "mentalny, demonicny glos: ''Zegnaj "
      + this_player()->koncowka("moj Mistrzu", "moja Mistrzyni") +"''.\n");                       
    say(QCIMIE(kogo[0],PL_MIA)+ " zdejmuje z siebie " +short(PL_BIE)+ ".\n");

    this_player()->set_combat_speed(this_player()->query_combat_speed()-45);
    this_player()->remove_subloc(DPIERSCIEN_SUBLOC);
        
    return 1;
}


string 
query_recover()
{
    return 0;
}

string
query_auto_load()
{
    return MASTER;
}

/*
 * -------------------------------------
 * Czesc przywolujaca demona           |
 * -------------------------------------
 */

void
bac()
{
write("Wracaja ci sily.\n");
    ptak=0;
}


int
wezwij( string str )
{
object play = environment(this_object());

if (!str)
{
    write("Dwezwij demona?\n");
    return 1;
}


if ( str != "demona" )
{
    write("Co pragniesz wezwac?\n");
    return 1;
}


if ( demon != 0 )
{
    demon->command("wesprzyj "+TP->query_real_name(PL_BIE)+"");
    return 1;
}

if ( ptak == 1 )
{
write("Ekhem!\n");
return 1;
}

setuid();
seteuid(getuid());

    tell_room(environment(TP),"Niebo wokol Ciebie nagle, z niezwykla " +
    "dokladnoscia przeslaniaja mroczne chmury. Gdzies tam, ponad toba " +
    "ku twemu wielkiemu zdziwieniu otwiera sie dziura miedzy wymiarami, " +
    "portal zbudowany z kosci i krwii istot zyjacych. Po chwili, gdy " +
    "przejscie jest juz gotowe, wylania sie zen niezwykle groteskowa " + 
    "istota, posiadajaca trzy pary rak, skrzydla i rogi... upewnia " +
    "Cie to w przekonaniu, ze portal sprowadzil z ktoregos z poziomow " +
    "Otchlani jedna z demonicznych istot, ktorej przybycie nie " +
    "zwiastuje nic dobrego, dla tych ktorzy naraza sie bestii lub " +
    "osobie ktora ja przyzwala.\n");

    demon = clone_object(DEMONPATH);
    demon->move(environment(TP));
    demon->command("wesprzyj "+play->query_real_name(PL_BIE)+"");
    demon->gon(play);
    demon->set_follow(play,0.1,1);
    
    ptak=1;
    set_alarm(600.0,0.0,"bac");
    
return 1;
}


int
zniszcz(string str)
{
if (!str)
{
demon->remove_object();
return 1;
}

}


/*
 * -------------------------------------
 * Czesc mowy demonicznej              |
 * -------------------------------------
 */
// To sa wyrazy z przedzialu 1 - 3 znaki.
string *kat01 = ({"zr","se'th","sihr","e'nh","anh","zez","sel","er","se","ata","u",
		"sha","ksh","shsy","bas","ksset","seb","zeu","saf","si",
		"urg","ar","nek","tsi","als","zar","alm","du","wir",
		"dzu","ihz'","aer","vaes","ard","zzko","dez","al'","mo","ab","om"});
// 4 - 5
string *kat02 = ({"hsalk","zzadla","osors","zok'am","resze","alam","eben",
		"es'hal","azma","kasser","lar'am","dzare","anfel",
		"basal","batti","bane","krema","ful","le'a",
		"also","mun","ebrik","fen'th","szaj","kakal",
		"zahra","gerso","al'sza","kub","loz","halib",
		"sekk","za'ht","melesh","bedz","ajbad",
		"ah'masr","asfar","etho","rive","tslan","veires",
		"nestoz","arbeh","asth","mocha","gfarh'","hakibs",
		"lime","bot'","tur'h","lite","ghaz","shar'",
		"bikah","aherr","tone","rehlat","telr","ochty"});
		
// > 6
string *kat03 = ({"sochuna","dar'bat","battich","mal'ful","szamandar","busala",
	    "siera'h","fendzan","dzozet","kronte","bazela",
	    "dokkan szaj","kam alsheab","alghada","fel'ahmar",
	    "alfotur","olbat meh'l","sh'chodar","tosbeh al'cha",
	    "mas'haj","alhamodo lah","szokran","tafadal","sallem",
	    "motaz","sa'asheb le'anam","jadzeb an","soma",
	    "ju'safe","afham","arras'","a'ref","chamar","tenthe",
	    "mileroe","talil","lasi'","chafif","monharef","tatual",
	    "telorn'","dimme","dumme","bimme","errlesh","fatesh",
	    "bine'th ar eine","toshe reighe","disse","fure'th",
	    "a'alea","hatete"});



void
tell_all( string words )
{
    tell_room(environment(TP),""+words+"",({TP}));
}

int
query_gracz( object gracz )
{
int r;
int proc;
int srednia;
int z_jezykow=gracz->query_skill(SS_LANGUAGE);
int in=gracz->query_stat(3); // int
int md=gracz->query_stat(4); // madrosc
int runy=gracz->query_skill(40); // to runy smierci

    if ( gracz->query_prop(LIVE_I_UNDEAD) == 100 )
	return 1;

    if ( z_jezykow > 75 )
	z_jezykow=90;
    if ( in > 80 )
	in = 100;
    if ( md > 80 )
	md = 100;
    if ( runy > 90 )
	runy = 90;

    srednia = (( z_jezykow + in + md + runy ) / 4) - 60;
	if ( z_jezykow < 35 ) // tutaj troche wzmocnilem skille, czeba miec
	    srednia -= 10;
	if ( runy < 40 )
	    srednia -= 10;

	if ( srednia < 0 )
	    srednia = 0;
	if ( srednia > 100 )
	    srednia = 100;

    r = random(100);
	if ( r < srednia )
	    return 1;

    
return 0;
}

string
return_convword( string word, object ob )
{
    switch(strlen(word))
	{
	case 1..3:
	    if (query_gracz(ob))
	    return word;
	    return kat01[random(sizeof(kat01))];
	    break;
	case 4..5:
	    if (query_gracz(ob))
	    return word;
	    return kat02[random(sizeof(kat02))];
	    break;
	default:
	    if (query_gracz(ob))
	    return word;
	    return kat03[random(sizeof(kat03))];
	    break;
	
	}
}


mixed
return_players()
{
int i;
object *players=({});
object *ob = all_inventory(environment(TP));
    ob = FILTER_LIVE(ob);
    

    for ( i = 0; i < sizeof(ob); i ++ )
	{
	if ( interactive(ob[i]) )
	    players += ({ob[i]});
	}

    if (sizeof(players))
	return players;
	return 0;

}

int
unpowiedz( string words )
{
int i,j;

string slowo="";
string znak="";	
string text="";
string last_char="";

object *gracze = return_players();

    if (!words)
	{
	write("W ostatniej chwili decydujesz sie nic nie mowic.\n");
	say(QCIMIE(TP,PL_MIA)+" w ostatniej chwili decyduje sie nic nie mowic.\n");
	return 1;
	}

gracze = return_players();
for ( j = 0; j < sizeof(gracze); j++ )
{

    for ( i = 0; i < strlen(words); i++ )
	{
	znak = sprintf("%c",words[i]);
	
	if ( znak != " " )
	slowo+=znak;
			
	if ( znak == " " || i == strlen(words)-1 || znak == "," )
	    {
	    text+=return_convword(slowo,gracze[j]);
		if ( znak == " " )
		    text+=" ";
		if ( znak == "," )
		    text+=", ";
		    slowo="";	    
	    }
	    
	}
last_char = sprintf("%c",words[strlen(words)-1]);
    if ( last_char == "?" ||
	 last_char == "." ||
	 last_char == "!" )
	 {
         }else{
	    last_char="";
	 }

if (gracze[j]==TP)
gracze[j]->catch_msg("Przemawiasz w mowie demonow: "+words+"\n");
else
gracze[j]->catch_msg(""+QCIMIE(TP,PL_MIA)+" przemawia w mowie demonow: "+
	""+capitalize(text)+""+last_char+"\n");
text="";
slowo="";
znak="";
}

return 1;
}


/*
 * -------------------------------------
 * Czesc demonicznej telepati          |
 * -------------------------------------
 */

int
mtell( string str )
{

  string who, what;
  object ob;
   if (!stringp(str))
   {
   notify_fail("Dprzekaz komu co?\n");
   return 0;
   }
   if (sscanf(str, "%s to %s", who, what) != 2 &&
   sscanf(str, "%s %s", who, what) != 2)
   {
   notify_fail("Dprzekaz kto co?\n");
  return 0;
								   
 }
  ob = find_living(who);
     if (!objectp(ob))
     {
         notify_fail("Nie ma takiej osoby jak:" + who + ".\n");
             return 0;
    }

    if (ob->query_wiz_level())
	{
	write("Ooj nie, to jest glupi, bardzo glupi pomysl.\n");
	return 1;
	}

	TP->catch_msg("Przekazujesz mentalnie "+QIMIE(ob,PL_CEL)+": "+what+"\n");	
	ob->catch_msg("Nagle czujesz niewiarygodny gorac, zas przed twymi "+
        "oczyma pojawiaja sie niezliczone obrazy bolu, strachu i cierpienia, "+
        "a takze niezwyklych, karykatularnych demonow. Gdy twe zmysly opanowuja "+
        "sie w pewnym stopniu, w twym umysle slyszysz powolny glos mowiacy: "+what+"\n");

		return 1;
				
}


/*
 * -------------------------------------
 * Czesc dostawania many               |
 * -------------------------------------
 */

void
laduj()
{

	mana+=10;
if ( mana > 200 )
    mana=200;
    
set_alarm(7.5,0.0,"laduj");
}
int
dotknij( string str )
{
int i;
int m=0;
    if ( str != "rubin" )
	{
	write("Dotknij czego?\n");
	return 1;
	}
	
    if ( mana == 0 )
	{
	write("Dotykasz rubin, lecz nic sie nie dzieje.\n");
	return 1;
	}
    if ( TP->query_mana() == TP->query_max_mana() )
	{
	write("Nie zdolasz przyjac tak duzej ilosci many.\n");
	return 1;
	}
	
    for ( i = 0; i < (TP->query_max_mana()-TP->query_mana()); i++ )
	{
	if ( mana > 0 )
	{
	m++;
	mana--;
	}
	}
	
	write("Dotykasz rubin, czujac jak przeplywa do ciebie "+
		"energia.\n");	

	TP->set_mana(TP->query_mana()+m);
	return 1;


}

int
pomoc()
{
write(" +=========================================================================+ " +
      " |                         Pomoc demonicznego pierscienia                  | " +
      " |_________________________________________________________________________| " +
      " |                                                                         | " +
      " | Pewnie nie wiesz nawet smiertelniku jak  wielki skarb zdobyles. Otoz    | " +
      " | ten potezny pierscien zawiera w sobie dusze jednego z demonicznych      | " +
      " | ksiazat, co w oczywisty sposob daje wlascicielowi artefaktu pewne       | " +
      " | nadzwyczajne zdolnosci...                                               | " +
      " |_________________________________________________________________________| " +
      " |                                                                         | " +
      " | + dwezwij <demona>                - wezwanie wsparcia                   | " +
      " | + dodeslij                        - odeslanie wsparcia                  | " +
      " | + dskieruj [co] [w kogo]          - odeslanie wsparcia                  | " +
      " | + dpowiedz [co]                   _ powiedzenie czegos w                | " +
      " | + d'       [co]                     demonicznej mowie                   | " +
      " | + dprzekaz [co]                   - przekazanie mentalnej wiadomosci    | " +
      " | + ddotknij [rubin]                - drobny zastrzyk energii magicznej   | " +
      " | + dpomoc                          - powyzszy spis                       | " +
      " | + dzniszcz                        - zniszczenie pierscienia             | " +
      " |_________________________________________________________________________| " +
      " |                        Dodatkowe emocje:                                | " +
      " |                                                                         | " +
      " | + dgrymas                          - zlowrogi wyraz twarzy              | " +
      " | + dzagroz [komu]                   - grozenie komus                     | " +
      " | + dspoliczkuj [kogo]               - pogardliwe spoliczkowanie          | " +
      " | + dsmiech                          - demoniczny smiech                  | " +
      " | + dprzypatrz [sie] [komu]          - przyjrzenie sie komus              | " +
      " | + ddobro                           - wyrazenie opini na temat dobra     | " +
      " | + dkrzyk                           - przerazajacy krzyk                 | " +
      " +=========================================================================+ \n");
        return 1;
}

int
destruct(string co)
{
saybb(QCIMIE(this_player(), PL_MIA) + " przy pomocy sily woli niszczy " + 
"swoj srebrny pierscien. Uwolniona w ten sposob energia wybucha, powalajac " +
TP->koncowka("go", "ja")+" na ziemie.\n");
write("Przy pomocy swej sily woli, niszczysz srebrny pierscien. W chwili " +
"gdy uwolniona energia wybucha, powalajac twe martwe juz cialo na ziemie, " +
"zdajesz sobie sprawe, ze uwolnienie duszy ksiecia demonow nie bylo takie " +
"madre...\n");
this_player()->heal_hp(-(this_player()->query_max_hp()));
this_player()->do_die(this_object());
TP->remove_subloc("pierscien_ran", TO);
TO->remove_object();
return 1;
}

string
show_subloc(string subloc, object on_obj, object for_obj)
{
    if (subloc == DPIERSCIEN_SUBLOC)
    {
        if (for_obj == on_obj)
        return "Na serdecznym palcu nosisz niezwykle piekny " +
        "srebrny pierscien z rubinem, swiadczacy o twym dobrym " +
        "guscie i smaku.\n";
        return "Na serdecznym palcu nosi niezwykle piekny, " +
        "srebrny pierscien z rubinem ktory... swieci?! " +
        this_player()->koncowka("Jego","Jej")+ "oczy maja jakis " +
        "dziwny, mroczny wyraz.\n";
    }
}


void
init()
{

    ::init();
        add_action("wezwij","dwezwij");
        add_action("zniszcz","dodeslij");
        add_action("unpowiedz","dpowiedz");
        add_action("unpowiedz","d'");
        add_action("mtell","dprzekaz"); 
        add_action("dotknij","ddotknij");
        add_action("pomoc","dpomoc");
        add_action("destruct","dzniszcz");
        add_action("przypatrz","dprzypatrz");
        add_action("grymas","dgrymas");
        add_action("grozba","dzagroz");
        add_action("spoliczkuj","dspoliczkuj");
        add_action("smiech","dsmiech");
        add_action("dobro","ddobro");
        add_action("krzyk","dkrzyj");
	add_action("do_skieruj","dskieruj");
}

/* 
 * EMOTY
 */

int 
krzyk()
{
    write("Wydajesz z siebie budzacy groze, straszliwy ryk bojowy, nie majacy nic "+
		  "wspolnego z zadnym z ziemskich odglosow, a przyprawiajacy twoich wrogow o "+
		  "dreszcz strachu.");
    allbb("wydaje z siebie budzacy groze, straszliwy ryk bojowy, nie majacy nic "+
		  "wspolnego z zadnym z ziemskich glosow, a wzbudzajacy przerazenie.");
	
	return 1;
}

int 
dobro()
{
    write("Ze wsciekloscia i grymasem na twarzy przeklinasz wszystko co dobre.\n");
    allbb("z odrazajacym wyrazem twarzy przeklina dobro.");
    return 1;
}

int przypatrz(string str)
{
          object *oblist;
          if (!str) return notify_fail("Dprzypatrz sie [komu] ?\n");
          oblist = parse_this(str,"'sie' %l:"+PL_CEL);
          if (!sizeof(oblist)) return notify_fail("Komu sie chcesz przypatrzec? \n");
        actor("Spogladasz na",oblist,PL_DOP," tajemniczym wzrokiem.");
        all2act("spoglada na",oblist,PL_DOP," swoim mrocznym, nieludzkim wzrokiem, "+
        "Widzisz jak ten zaczyna szukac miejsca w ktorym, moglby sie schowac...");
       target("wpatruje sie w ciebie swoim przepelnionym wyzszoscia, nieludzkim wzrokiem, "+
        "w spojrzeniu tym wyczuwasz mrok i zadze krwi. Masz wielkie pragnie " 
       + "ukryc sie gdzies przed tym spojrzeniem.",oblist);

                return 1;
}

int grymas(string str)

{
      if (str) return notify_fail("Ta komenda nie przyjmuje zadnych argumentow.\n");
        write("Wykrzywiasz twarz w nadnaturalnym grymasie.\n");
        all("wykrzywia twarz w straszliwym, nieludzkim grymasie. Przez mysl "
          + "przemknal ci obraz wilka ktory upatrzyl sobie zwierzyne.");           
        return 1;
}


int
grozba(string str)
{
    object *cele;
    cele = parse_this (str, "%l:"+PL_DOP);

        if (sizeof(cele=parse_this(str, "%l:" + PL_BIE)))
        {
          actor("Wbijasz swoj przerazajacy, wzrok w",cele,PL_BIE, " dajac mu "+
        "tym wyraznie do zrozumienia, iz jeden z was musi odejsc, niestety "+
        "z pewnoscia nie bedziesz to ty.");
         target("wbija w ciebie swe chlodne spojrzenie, a ty w jego oczach "+
        "zauwazasz rzadze krwi. Zdajesz sobie sprawe, iz jeden z was musi odejsc, "+
        "i chyba chodzi o ciebie.",cele);
       all2act("wbija swe chlodne spojrzenie w",cele,PL_BIE," ,a w jego "+
        "oczach zauwazsz zadze krwi, ktora niewrozy niczego dobrego.");
            return 1;
        }
    notify_fail("Komu chcesz dzagrozic?\n");
    return 0;
}
int smiech(string str)
{
	if (str) return notify_fail("Ta komenda nie przyjmuje zadnych argumentow.\n");
		write("Mruzysz lekko oczy i wybuchasz chrypiacym smiechem.\n");
		all("mruzac lekko oczy, wybucha przerazliwym, chrypiacym smiechem "+
		"ktory drazni twoje uszy.");                                 
		return 1;
}

int
spoliczkuj(string str)
{
    object *oblist;
    
    oblist = parse_this (str, "%l:"+PL_BIE);
    if(sizeof(oblist) !=0)
        {
            target("z pogarda wymierza ci siarczysty policzek.",oblist);
            actor("Z pogarda wymierzasz",
                  oblist,PL_CEL," siarczysty policzek.");
            all2actbb("z pogarda wymierza",oblist,PL_CEL," siarczysty policzek.");
            return 1;
        }
    notify_fail("Dspoliczkuj kogo?\n");
    return 0;
}

int
do_skieruj(string s)
{
	object *ob = all_inventory(environment(this_player())), *wr, *tmp;
	int align, r_hp, dam;
	object tp = this_player();
	
	notify_fail("Dskieruj pierscien w czyja strone ?\n");
	if (!ob || !s || s==""  || !query_worn())
		return 0;
	ob = FILTER_LIVE(ob);
	ob -= ({tp});
	tmp = ob + ({});
	if (!ob)
		return 0;
	ob = FILTER_IS_SEEN(tp, ob);
	if (!ob)
		return 0;
	if (!parse_command(s, ob, " 'pierscien' 'w' 'strone' %l:"+PL_DOP, wr))
	{
		if (!parse_command(s, ob, " 'pierscien' "+
		"'w' %o:"+PL_DOP+" 'strone' ", wr))
		return 0;
	}
	wr = NORMAL_ACCESS(wr,0,0);
	if (sizeof(wr) && sizeof(wr)>1)
		return 0;
	tp->set_obiekty_zaimkow(wr);
	say(QCIMIE(tp, PL_MIA) + " skierwal"+tp->koncowka("","a")+
			" "+short(PL_BIE)+ " w strone "+
			QIMIE(wr[0], PL_DOP)+".\n", ({tp, wr[0]}));
	wr[0]->catch_msg(QCIMIE(tp, PL_MIA) + " skierwal"+tp->koncowka("","a")+
			" "+short(PL_BIE)+ " w Twoja strone.\n");
	align = tp->query_alignment();
	dam = 2;
	if (align < 0)
		dam = -align;
	align += 300;
	if (align<0)
		align = -align;
	if ((tp->query_mana()-align)<0)
	{
		align /= 2;
		dam /= 2;
		if ((tp->query_mana()-align)<0)
		{
		write("Nie jestes w stanie zmusic "+
			short(PL_DOP)+" do posluszenstwa.\n");
		return 1;
		}
	}
	set_alarm(10.0, 0.0, "uderzenie",tp, wr[0], dam);
	
	write("Czujesz jak tajemnicza moc wysysa z Ciebie "+
		"czesc sil umyslowych.\n");
	tell_room(environment(tp), capitalize(short(PL_MIA))+
			" rozjarzyl sie "+
			"ognistoczerwona poswiata.\n");
	tp->add_mana(-align);
	return 1;
}
void
uderzenie(object tp, object wr, int dam)
{
	mixed *tmp = ({}), *kogo=({}), *ob = all_inventory(environment(tp));
	int i, j, k;
	string jak="",co="";
	mixed *p1, *p2;
	
#if 0
	if (!wr->query_attack())
		tp->attack_object(wr);
	else
#endif
		if (member_array(wr, (tmp = tp->query_enemy(-1)))!=-1)
			kogo += tmp- ({wr});
	kogo += ({wr});
	ob -= kogo;
	switch(dam)
	{
		case 0..20:
			co="Blyskawica";
			jak = "wyladowaniami";
			break;
		case  21..100:
			co="Ognista strzala";
			jak="plomieniami";
			break;
		case 101..200:
			co="Ognista wlocznia";
			jak = "plomieniami";
			break;
		case 201..400:
			co="Kula ognia";
			jak="plomieniami";
			break;
		default:
			co="Potezna kula ognia";
			jak="palacymi plomieniami";
	}
	ob = FILTER_PLAYERS(ob);
	i = sizeof(ob);
	while (--i>=0)
	{
	ob[i]->catch_msg(co+" uformowala "+
			"sie tuz przed skrzacym sie na pierscieniu rubinem,"+
			" po czym"+(sizeof(kogo)>1?", rozdzielajac sie,":"")+
			" blyskawicznie poszybowala w kierunku ");
	ob[i]->catch_msg(FO_COMPOSITE_LIVE(kogo, ob[i], PL_DOP)+".\n");
	}
#if 0
	write(", rozbijajac sie na "+(sizeof(kogo)>1?
			"cialach":"ciele") + " i spowijajac je "+jak+".\n");
#endif
	
	
	ob = kogo+({});
	tmp = ({({})});
	tmp[0] = ({kogo[0]});
	ob -= ({kogo[0]});
	i = sizeof(ob);
	j = 0;
	while (sizeof(ob))
	{
		while (--i>=0)
			if (environment(ob[i])==environment(tmp[j][0]))
			{
				tmp[j] += ({ob[i]});
			}
		ob -= tmp[j];
		j++;
		if (sizeof(ob))
			tmp += ({ob[0]});
		ob -= ({ob[0]});
	}
	i=sizeof(tmp);
	while (--i>=0)
	{
		j = sizeof(tmp[i]);
		if (j>1)
		{
			if (environment(tmp[i][0])==environment(tp))
			while (--j>=0)
			{
				tmp[i][j]->catch_msg(co+" uformowala "+
			"sie tuz przed skrzacym sie na pierscieniu rubinem,"+
			" po czym"+(sizeof(tmp[i])>1?", rozdzielajac sie,":"")+
			" blyskawicznie poszybowala w kierunku ");
				tmp[i][j]->catch_msg(FO_COMPOSITE_LIVE(
					tmp[i]-tmp[i][j], tmp[i][j], PL_DOP)+
					" oraz twoim, spowijajac was "+
					""+jak+".\n");
			}
			else
			while(--j>=0)
			{
				tmp[i][j]->catch_msg(co+" nadleciala "+
			" po czym"+(sizeof(tmp[i])>1?", rozdzielajac sie,":"")+
			" blyskawicznie poszybowala w kierunku ");
				tmp[i][j]->catch_msg(FO_COMPOSITE_LIVE(
					tmp[i]-tmp[i][j], tmp[i][j], PL_DOP)+
					" oraz twoim, spowijajac was "+
					jak+".\n");
			}
		}
		else
			if (environment(tmp[i][0])==environment(tp))
				tmp[i][0]->catch_msg(co+" uformowala "+
			"sie tuz przed skrzacym sie na pierscieniu rubinem,"+
				", po czym uderzyla w Twoje cialo"+
					", spowijajac "+
					"je "+jak+".\n");
			else
				tmp[i][0]->catch_msg(co+" nadleciala "+
			"z ogromna predkoscia"+
				", po czym uderzyla w Twoje cialo"+
					", spowijajac "+
					"je "+jak+".\n");
		p1 = FILTER_PLAYERS(all_inventory(environment(tmp[i][0])));
		p1 -= tmp[i];
		k = sizeof(p1);
		while (--k>=0)
		p1[k]->catch_msg(co + 
			" uderzyla w "+FO_COMPOSITE_LIVE(tmp[i],p1[k],PL_BIE)+
			", rozbijajac sie na "+(sizeof(tmp[i])>1?
			"ich cialach":
			(tmp[i][0]->koncowka("jego","jej")+" ciele")) + 			
	" i spowijajac je "+jak+".\n");
	}
	i=sizeof(kogo);
	while(--i>=0)
	{
		kogo[i]->hit_me(dam, MAGIC_DT, random(80)+20, tp, -1);
		dam /= 2;
	}
}
