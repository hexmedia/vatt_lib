/*
 * Filename: kpeleryna.c
 *
 * Zastosowanie: Peleryna??
 * 
 * Copyright Volothamp Geddarm.
 *
 */
inherit "/d/Faerun/std/stdubrania.c";

#include <stdproperties.h>
#include <wa_types.h>
#include <formulas.h>
#include <macros.h>

void
create_ubranie()
{

    ustaw_nazwe( ({"peleryna", "peleryny", "pelerynie", "peleryne", "peleryna", "pelerynie"}),
		         ({"peleryny", "peleryn", "pelerynom", "peleryny", "pelerynami", "pelerynach"}), PL_ZENSKI);
    
	dodaj_przym("fioletowy","fioletowi");
    
	set_long("Gdy przychodzac na przyjecie u hrabiny chcesz dobrze wypasc, pewnikiem jest, ze "
      + "zakladajac te fioletowa peleryne to uczynisz. Poza dodawaniem elegancji owa "
      + "peleryna dobrze nadaje sie jako okrycie podczas wedrowki,  jednakze nic nie "
      + "zdziala podczas zamieci snieznej w gorach. Czesto widuje sie ja, na bardach czy "
      + "minstrelach, ktorzy wedruja od dworu do dworu, prezentujac swoj talent. Po jej "
      + "wewnetrznej stronie dostrzegasz niewielki znaczek pracowni krawieckiej 'Krolewski "
      + "Gryf'.\n");
    
	set_slots(A_ROBE);

    add_prop(OBJ_I_WEIGHT, 50);
	add_prop(OBJ_I_VALUE, 600);
}


