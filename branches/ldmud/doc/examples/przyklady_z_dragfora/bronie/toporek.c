/*
 * Filename: toporek.c
 *
 * Zastosowanie: Toporek.
 * 
 * Copyright Volothamp Geddarm.
 *
 */
inherit "/d/std/bron";  

#include <filter_funs.h>
#include <ss_types.h>
#include <wa_types.h>
#include <stdproperties.h>
#include <wa_types.h>
#include <cmdparse.h>
#include <macros.h>
#include <pl.h>
#include <options.h>
#include <macros.h>
#include <formulas.h>
#include <const.h>

#define TP this_player()
object wielder;

    void
    create_weapon()
{
    set_autoload();  
    
	ustaw_nazwe(({ "toporek","toporka","toporkowi","toporek","toporkiem","toporku"}),
		        ({ "toporki","toporkow","toporkom","toporki","toporkami","toporkach"}),PL_MESKI_NOS_NZYW);

	dodaj_przym("maly", "mali");
	dodaj_przym("niepozorny", "niepozorni");

    set_long("Spogladasz na niepozorny toporek bedacy krotka bronia jaka posluguja sie "
+ "zazwyczaj humanoidalne stwory zamieszkujace dzicz, takie jak na przyklad gobliny "
	  + "czy orki. Twe fachowe oko wyraznie dostrzega amatorska robote, ktora sprawia, ze "
	  + "owa bron jest nie tyle dziwna i rozklekotana co wrecz smieszna.\n");
    
	set_hit(5);
    set_pen(12);
    
	set_wt(W_SLASH);
	set_wt(W_IMPALE);
    
	set_hands(W_ANYH);
    
	add_prop(OBJ_I_WEIGHT, 500);                
	add_prop(OBJ_I_VALUE, 150);
}

/*----Podpisano: 'Volothamp Geddarm'----*/


  

