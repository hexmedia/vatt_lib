/*
 * Filename: bekart.c
 *
 * Zastosowanie: Bekart.
 * 
 * Copyright Volothamp Geddarm.
 *
 */
inherit "/d/std/bron";  

#include <filter_funs.h>
#include <ss_types.h>
#include <wa_types.h>
#include <stdproperties.h>
#include <wa_types.h>
#include <cmdparse.h>
#include <macros.h>
#include <pl.h>
#include <options.h>
#include <macros.h>
#include <formulas.h>
#include <const.h>

#define TP this_player()
object wielder;

    void
    create_weapon()
{
    set_autoload();  
    ustaw_nazwe(({ "bekart", "bekarta", "bekartowi", "bekart", "bekartem", "bekarcie" }),
                ({ "bekarty", "bekartow", "bekartom", "bekarty", "bekartami", "bekartach" }), PL_MESKI_NOS_NZYW); 
    
	dodaj_przym("lekki", "leccy");
	dodaj_przym("poreczny", "poreczni");

    set_long("Jest to srednio dlugi i ciezki miecz, ktory powszechnie wojownicy ozwali "
	  + "bekartem czyli mieczem poltorarecznym. Mozna go chwytac w dowolna reke przez "
	  + "co jest mozliwe uzywanie tarczy podczas boju, a jednoczesnie zadaje prawie takie "
	  + "obrazenia w walce jakie zadaja miecze obureczne. Wydaje sie to byc powaznym "
	  + "atutem owej broni, totez przez osoby parajace sie wojaczka obdarzony bywa "
      + "niemalym szacunkiem. Na rekojesci pokrytej cienka warstwa zwierzecej skory "
	  + "dostrzegasz ponadto osobisty znak slynnej kuzni z Beregostu.\n");
    
	set_hit(33);
    set_pen(33);
    
	set_wt(W_SWORD);
	
	set_dt(W_SLASH);
	set_dt(W_IMPALE);
    
	set_hands(W_ANYH);
    
	add_prop(OBJ_I_WEIGHT, 3500);                
	add_prop(OBJ_I_VALUE, 900);
}

/*----Podpisano: 'Volothamp Geddarm'----*/


  

