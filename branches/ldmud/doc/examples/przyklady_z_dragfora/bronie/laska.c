/*
 * Filename: laska.c
 *
 * Zastosowanie: Laska dla maga Orlena.
 * 
 * Copyright Volothamp Geddarm.
 *
 */
inherit "/d/std/bron";  

#include <filter_funs.h>
#include <ss_types.h>
#include <wa_types.h>
#include <stdproperties.h>
#include <wa_types.h>
#include <cmdparse.h>
#include <macros.h>
#include <pl.h>
#include <options.h>
#include <macros.h>
#include <formulas.h>
#include <const.h>

#define TP this_player()
object wielder;

    void
    create_weapon()
{
    set_autoload();  
    ustaw_nazwe(({ "laska", "laski", "lasce", "laske", "laska", "lasce" }),
                ({ "laski", "lasek", "laskom", "laski", "laskami", "laskach" }), PL_ZENSKI); 
    dodaj_przym("dlugi", "dludzy");
    dodaj_przym("plomienisty", "plomienisci");
    set_long("Z cala pewnoscia laska, ktorej sie przygladasz nie jest w gruncie "
	  + "rzeczy zwykla bronia czy kijem podroznym. Jej koniec zarzy sie jasnym "
	  + "plomieniem zas trzon jest cieply, niemalze parzacy. Domyslasz sie "
	  + "ze ta laska nie ma za zadanie zwiekszac obrazenia, ktora zadaje osoba "
	  + "nia walczaca lecz raczej wspierac jej moc i magiczny potencjal, a takze "
	  + "wzmagac zdolnosc koncentracji. Byla to zapewne smiertelna bron w rekach "
	  + "jakiegos czarnoksieznika.\n");
    
	set_hit(10);
    set_pen(10);
    
	set_wt(W_POLEARM);
	
	set_dt(W_SLASH);
	set_dt(W_IMPALE);
    
	set_hands(W_BOTH);
    
	add_prop(OBJ_I_WEIGHT, 1000);                
	add_prop(OBJ_I_VALUE, 10);
}

/*----Podpisano: 'Volothamp Geddarm'----*/


  

