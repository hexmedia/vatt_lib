/*
	Brama Palanthasu
		Aetherian@Sarcan
		Data: 30.12.2002
*/

#include <money.h>
#include "../pal.h"

inherit "/d/std/room";

int otwarta(object br);

void otworz_brame(object br);
void zamknij_brame(object br);

//object* query_guard(){return query_npce()[0];}

void set_guard(){
  dodaj_npca(NPC+"gwardzista");
  dodaj_npca(NPC+"gwardzista");
  dodaj_npca(NPC+"oficer");
} 


void create_room() {

  add_prop(ROOM_I_INSIDE, 1);
  add_prop(ROOM_I_LIGHT, 1);

}






void otwarta(object br){
  string s;
  int otw;

  otw=0;
  s = TO->dzien_noc();
  if (s=="dzien")
    otw=1;


  if ((br->query_open())&&(!otw))
    set_alarm(5.0,0.0,&zamknij_brame(br));
    
  if ((!(br->query_open()))&&otw)
    otworz_brame(br);

}


void otworz_brame(object br) {
    object s;
    string naz;
    
    s = br->query_other_door();
    if (!s)
      br->load_other_door();
    
      
    br->set_open(1);
    br->do_open_door("W akompaniamencie zgrzytow i trzaskow mechanizmu "
    +"brama zostaje otwarta.\n");
    s->do_open_door("W akompaniamencie zgrzytow i trzaskow mechanizmu "
    +"brama zostaje otwarta.\n");
}



void zamknij_brame(object br) {
    object s;
    
    s = br->query_other_door();
    if (!s)
      br->load_other_door();
    
    
    br->set_open(0);
    br->do_close_door("Z glosnym zgrzytem brama zostaje zamknieta.\n");
    s->do_close_door("Z glosnym zgrzytem brama zostaje zamknieta.\n");
}


