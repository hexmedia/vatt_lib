/* Urna do swiatyni
	-- Aetherian 27/12/2002
*/

#include "../pal.h"
inherit "/std/receptacle";

#define PALADINE_URNA OBJ+"urna.c"

string *olist = ({});

void load();

void save_me()
{
    save_object(PALADINE_URNA);
}

void create_container()
{
    ustaw_nazwe(({"skrzynia","skrzyni","skrzyni","skrzynie","skrzynia",
                  "skrzyni"}),
                ({"skrzynie","skrzyn","skrzyniom","skrzynie","skrzyniami",
                  "skrzyniach"}), PL_ZENSKI);
    dodaj_przym("duzy","duzi");
    dodaj_przym("drewniany","drewniani");
    set_long("Jest to spora, drewniana skrzynia, do ktorej mozesz wrzucac przedmioty, ktore "
    +"nie sa ci juz potrzebne i mozesz je oddac mlodszym podroznikom, ktorym zdobycie "
    +"tychze przychodzi z wieksza trudnoscia niz tobie. W chwili slabosci mozesz tez "
    +"wziac jakies przedmioty z urny aby dozbroic sie odpowiednio.\n");
    add_prop(CONT_I_WEIGHT,50000);
    add_prop(CONT_I_MAX_WEIGHT,900000);
    add_prop(CONT_I_MAX_VOLUME,900000);
    add_prop(CONT_I_RIGID,1);
    add_prop(OBJ_M_NO_GET,"Probujesz wziac skrzynie, jednak okazuje sie ona przytwierdzona "
    +"do podloza jakas magiczna sila.\n");
    setuid();
    seteuid(getuid());
    restore_object(PALADINE_URNA);
    set_alarm(2.0, 0.0,load);	
}

void enter_inv( object co, object skad )
{
	::enter_inv(co, skad);
    olist += ({MASTER_OB(co)});
    save_me();
}
void leave_inv( object co, object dokad )
{
    ::leave_inv(co, dokad);
    olist -= ({MASTER_OB(co)});
    save_me();
}

void load()
{
    int i;
    object *items=({});
    write("Wokol urny rozblyska nagle dziwna, niebieskawa poswiata, jednak po chwili wszystko "
    +"wraca do normy.\n");
    for ( i = 0; i < sizeof(olist); i++ )
        items += ({clone_object(olist[i])});
        olist->remove_object();
        olist=({});     
        save_me();
        for ( i = 0; i < sizeof(items); i++ )
            items[i]->move(TO); 
}

void init()
{
   ::init();
}
