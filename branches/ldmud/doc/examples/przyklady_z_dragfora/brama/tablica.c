inherit "/std/board.c";

#include <std.h>
#include "../pal.h"
#include <stdproperties.h>
#include <pl.h>


void create_board()
 {
  add_prop(OBJ_M_NO_GET,"Raczej nie jestes w stanie wyrwac z ziemi tablicy "
  + "ogloszeniowej.\n");
  
  set_long("Tablica ogloszeniowa jest dosyc duza, wykonana ze zbitych ze "
  + "soba desek i solidnie umocowana w ziemi za pomoca grubego i ciezkiego "
  + "palika.\n");

    dodaj_nazwy(({"tablica","tablicy","tablicy","tablice","tablica",
        "tablicy"}), allocate(6), PL_ZENSKI);

    dodaj_przym("ogloszeniowy", "ogloszeniowi");

    set_board_name(OBJ + "tablica");
    set_num_notes(30);
    set_silent(1);
 }

