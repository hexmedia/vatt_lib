// Brama Palanthasu
// Aetherian@Sarcan 30.12.2002

inherit "/std/door";
#include "../pal.h"


void create_door()
{   
    ustaw_nazwe(({"brama","bramy","bramie","brame","brama","bramie"}),
    		({"bramy","bram","bramom","bramy","bramami","bramach"}),
               PL_ZENSKI);

    dodaj_nazwy(({"wrota","wrot","wrotom","wrota","wrotami","wrotach"}),
      allocate(6), PL_MESKI_NOS_NZYW);
        
    dodaj_przym("masywny", "masywni");
    dodaj_przym("stalowy", "stalowi");
    
    set_other_room(LOC+"przed_brama_w.c");
    
    set_door_id("_brama_w_Palanthas_");

    set_door_desc("Stoisz przed ogromna, masywna stalowa brama, "
    +"prowadzaca na zewnatrz miasta. Wrota umieszczone sa na "
    +"gigantycznych zawiasach, skrzypiacych potwornie przy otwieraniu "
    +"i zamykaniu.\n");
    
    set_open_desc("Brama jest otwarta.\n");
    set_closed_desc("Brama jest zamknieta.\n");
        
    set_pass_command(({"zachod","w"}));
    set_pass_mess("przez wielka brame na zachod");
    set_fail_pass("Przechodzenie przez zamkniete wrota"+
    	" nie nalezy chyba do najlepszych pomyslow.\n");
    	
    set_open_command("zapukaj");
    set_fail_open("");
    set_close_command("");
    set_locked(0);
    set_lock_name("");
    set_lock_command("");
    set_unlock_command("");
}


int open_door(string str)
{
  int ok;
  ok=1;
  if (open_status)
  {
    notify_fail("A po co? Brama jest juz otwarta.\n");
    return 0;
  }
      write("Uderzasz dosc mocno w stalowa brame.\n");
      write("Twoje uderzenie wywoluje gluchy odglos.\n");
      set_alarm(4.0, 0.0, "otworz_drzwi");

  if (!other_door)
    load_other_door();
  return ok;
}


void otworz_drzwi()
{
  do_open_door("W akompaniamencie zgrzytow i trzaskow mechanizmu "
    +"brama zostaje otwarta.\n");
  other_door->do_open_door("W akompaniamencie zgrzytow i trzaskow mechanizmu "
    +"brama zostaje otwarta.\n");
  set_open(1);
}
