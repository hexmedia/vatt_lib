/*
 * Posrebrzany lekki pas
 *
 * Autor: Alcyone
 */

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <macros.h>
#include <clothes.h>
#include "/sys/materialy.h"

	void
create_armour()
{  

	ustaw_nazwe(PAS,  PL_MESKI_NOS_NZYW);
	dodaj_przym("posrebrzany","posrebrzani");
	dodaj_przym("lekki", "lekcy");
	set_long("Cie�ki posrebrzany pas zrobiono z do�� twardego i mocnego materia�u. " +
		 "Przy ka�dym padaj�cym na niego promieniu �wiat�a b�yszczy malowniczo, " +
		 "jakby nieustannie co� si� na nim porusza�o. Jego bogato zdobiona klamra " +
		 "w kszta�cie r�y jest niezwykle dok�adnie wykonana. Wtopione w ni� male�kie " +
		 "rubiny daj� niecodzienn� gr� kolor�w. Pas nadaje si� do wytwornych " +
		 "i wyszukanych stroj�w, by ol�ni� nim zazdrosne spojrzenia.\n");

	set_slots(A_HIPS);
	add_prop(OBJ_I_WEIGHT, 200);
	add_prop(OBJ_I_VOLUME, 200);
	add_prop(OBJ_I_VALUE, 300);

	add_item(({"klamr�", "rubiny", "r��"}), "Bogato zdobiona klamra w kszta�cie r�y jest doskonale wykonana. Nawet przy dok�adnym przyjrzeniu si� nie mo�na wskaza� jakiejkolwiek skazy w tym miniaturowym dziele sztuki. Niecodziennego widoku dope�niaj� wtopnione w srebrn� powierzchni� male�kie rubiny, sprawiaj�c wra�enie, jakby p�atki r�y by�y �ywe.\n");
	
	/* ustawiamy rozci�gliwo�� w d� na 80% */
	add_prop(ARMOUR_I_D_ROZCIAGLIWOSC, 80);

	/* Ustawiamy materia�y, z kt�rych jest wykonany pas */
	ustaw_material(MATERIALY_RUBIN, 1);
	ustaw_material(MATERIALY_SREBRO, 10);
	ustaw_material(MATERIALY_SK_SWINIA, 89); /* �winie s� wsz�dzie !!! */
}

string
query_auto_load()
{
   return ::query_auto_load();
} 
