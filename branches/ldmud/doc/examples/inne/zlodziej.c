inherit "/std/humanoid.c";

#include <wa_types.h>
#include <formulas.h>
#include <pl.h>
#include <money.h>
#include <const.h>
#include <ss_types.h>
#include <macros.h>
#include <stdproperties.h>

#define TO    this_object()
#define MIECZ "/doc/examples/bronie/miecz.c"
#define BUTY  "/doc/examples/zbroje/buty.c"
#define ZBRO "/doc/examples/zbroje/kolczuga.c"
#define SPOD "/doc/examples/zbroje/spodnie.c"
#define ZLODZIEJ_AKTYWNY "_jest_zlodziej_aktywny1"

void
create_humanoid()
{
    ustaw_shorty(({ "z這dziej", "z這dzieja", "z這dziejowi", "z這dzieja", "z這dziejem", "z這dzieju"}),
                 ({ "z這dzieje", "z這dziei", "z這dziejom", "z這dziei", "z這dziejami", "z這dziejach" }),
                    PL_MESKI_NOS_ZYW);
    //Poprawn� form� jest te�
    //ustaw_short("z這dziej");
    //je郵i nazwa z這dziej jest w s這wniku.

    dodaj_nazwy(({ "z這dziej", "z這dzieja", "z這dziejowi", "z這dzieja", "z這dziejem", "z這dzieju"}),
                 ({ "z這dzieje", "z這dziei", "z這dziejom", "z這dziei", "z這dziejami", "z這dziejach" }),
                    PL_MESKI_NOS_ZYW);
    //Popdobnie jak wy瞠j mo積e te�:
    //dodaj_nazwy("z這dziej");

    set_long("Zlodziej prezentuje soba raczej dosc mizerny widok - poobrywane ubranie " +
        "i wystrzepiony noz swiadcza o tym, ze jego bieda zmusila go do takiego, a nie " +
        "innego sposobu zarabiania na zycie.\n");

    ustaw_odmiane_rasy(PL_CZLOWIEK);
    set_gender(G_MALE);
    add_prop(LIVE_I_NEVERKNOWN, 1);
    add_prop(CONT_I_WEIGHT, 65000); // ... 65 kilo...
    add_prop(CONT_I_HEIGHT, 175);   // ... i 175 cm wzrostu..
    add_prop(NPC_I_NO_FEAR, 1);
    add_prop(NPC_I_NO_RUN_AWAY, 1);
    set_skill(SS_DEFENCE, 25 + random(5));
    set_skill(SS_WEP_SWORD, 25 + random(10));
    set_skill(SS_UNARM_COMBAT, 15 + random(5));
    set_skill(SS_PARRY, 15 + random(10));

    set_stats(({ 8, 14, 9, 7, 9})); // Si豉, Zr璚zno嗆, Wytrzyma這嗆, Intelekt, Odwaga
    add_weapon(MIECZ);
    add_armour(ZBRO);
    add_armour(BUTY);
    add_armour(SPOD);
    set_aggressive(1);
}
