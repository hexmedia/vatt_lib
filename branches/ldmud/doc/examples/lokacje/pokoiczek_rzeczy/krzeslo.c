inherit "/std/object.c";

#include <pl.h>
#include <stdproperties.h>
#include <macros.h>
#include <filter_funs.h>
#include "object_types.h"

create_object()
{
    ustaw_nazwe("krzes�o");

    make_me_sitable("na","na krze�le","z krzes�a",1);

    set_long("Wykonane z drewna krzes�o o prostej budowie.\n");
    add_prop(CONT_I_WEIGHT, 500);
}

public int
query_type()
{
    return O_MEBLE;
}

