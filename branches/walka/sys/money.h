    /**
 * \file /sys/money.h
 *
 * Ten plik zawiera makra u�yteczne przy operowaniu pieni�dzmi. 
 * Je�li szukasz definicji cechy r�nych rodzaj�w monet zajrzyj do pliku 
 * /config/sys/money2.h , kt�ry jest tutaj include'owany.
 */

/* Komentarze przet�umaczone/dopisane przez Moldera. */

#ifndef MONEY_DEFINITIONS
#define MONEY_DEFINITIONS

/**
 * Tre�ci funkcji z kt�rych korzystaj� makra: 
 */
#define MONEY_FN "/sys/global/money.c"

/**
 * MONEY_COINS zwraca tablic� zawieraj�c� ilo�� monet ka�dego typu
 * posiadanych przez obiekt ob.
 * @return tablic� postaci ({ grosze, dukaty, korony })
 */
#define MONEY_COINS(ob)        ((int *)MONEY_FN->what_coins(ob))

/**
 * MONEY_SPLIT zwraca tablice zawieraj�c� tablic� monet o najwy�szych
 * mo�liwych nomina�ach, kt�re po wysumowaniu daj� cp groszy. 
 * @return tablic� postaci ({ grosze, dukaty, korony })
 */
#define MONEY_SPLIT(cp)        ((int *)MONEY_FN->split_values(cp))

/**
 * MONEY_MERGE zwraca warto�� (w groszach) tablicy typ�w monet
 * podanej jako argument. 
 * @param carr ma posta�: ({ grosze, denary, korony }).
 */
#define MONEY_MERGE(carr)      ((int)MONEY_FN->merge_values(carr))

/**
 * TOTAL_MONEY zwraca ilo�� groszy posiadan� przez obiekt 'who'.
 */
#define TOTAL_MONEY(who)       ((int)MONEY_FN->total_money(who))

/**
 * MONEY_MAKE robi to o czym zawsze marzyli�my -  tworzy pieni�dze 
 * (i zwraca wska�nik do nich).  
 * @param num - ilo�� monet
 * @param ctype - typ monet (zgodny z MONEY_TYPES)
 *
 * U�ywaj raczej makr: MONEY_MAKE_G, MONEY_MAKE_D lub MONEY_MAKE_K 
 * kt�re tworz� monety konkretnego typu (odpowiednio: grosze, denary, korony).
 */
#define MONEY_MAKE(num, ctype) ((object)MONEY_FN->make_coins(ctype, num))

/**
 * MONEY_MOVE przenosi odpowiedni� ilo�� monet z obiektu 'from' do 'to'. 
 * @param num - ilo�� monet
 * @param ctype - typ monet (zgodny z MONEY_TYPES) 
 * @param from - obiekt, <b>z</b> kt�rego przenosimy monety
 * @param to - obiekt, <b>do</b> kt�rego przenosimy monety
 * Podaj argument from == 0 aby utworzy� nowe monety.
 * 
 * @return        -1      gdy obiekt 'from' nie ma (wystarczaj�co) monet danego typu
 * @return         0      gdy monety zosta�y przemieszczone
 * @return        >1    gdy wyst�pi� b��d przy przenoszeniu (move) monet (wygenerowany przez /std/object)
 *
 * U�ywaj raczej makr: MONEY_MOVE_G, MONEY_MOVE_D, MONEY_MOVE_K
 * (przenosz� odpowiednio: grosze, denary, korony).
 */
#define MONEY_MOVE(num, ctype, from, to) \
    ((int)MONEY_FN->move_coins(ctype, num, from, to))

/**
 * MONEY_MOVE_COIN_TYPES przesuwa odpowiedzni� ilo�� monet r�nych typ�w
 * z obiektu 'from' do 'to'. Argument 'carr' to tablica ilo�ci przesuwanych monet
 * zgodna z MONEY_TYPES (czyli np. ({ 10, 20, 1}) - 10 groszy, 20 denar�w, 1 korona).
 * Podaj argument from == 0 aby utworzy� nowe monety. 
 * Rezultat jest taki sam jak przy korzystaniu z MONEY_MOVE.
 *
 * @return         -1       gdy obiekt 'from' nie ma (wystarczaj�co) monet danego typu
 * @return         0        gdy monety zosta�y przemieszczone
 * @return         >1     gdy wyst�pi� b��d przy przenoszeniu (move) monet (wygenerowany przez /std/object)
 */
#define MONEY_MOVE_COIN_TYPES(carr, from, to) \
    ((int)MONEY_FN->move_cointypes(carr, from, to))

/**
 * MONEY_ADD dodaje obiektowi 'who' monety o ��cznej warto�ci 'amount' 
 * o najwy�szych mo�liwych nomina�ach. kwota 'amount' mo�e by� ujemna 
 * wtedy obiektowi 'who' b�d� zabrane monety o najni�szych mo�liwych 
 * nomina�ach. 
 *
 * @return dla amount >= 0     zawsze zwraca 1 
 * @return dla amount <  0       je�li 1 - uda�o si� zabra� monety
 * @return                          je�li 0 - obiekt 'who' ma zbyt ma�o pieni�dzy (nic nie zosta�o zabrane)
 */
#define MONEY_ADD(who, amount) ((int)MONEY_FN->add_money(who, amount))

/**
 * MONEY_TEXT zwraca string opisujacy monety podane w tablicy, w podanym
 * przypadku. 
 * @param arr - tablica postaci ({ grosze, denary, korony })
 * @param przyp - przypadek, w kt�rym chcemy uzyska� string
 * @return string typu: "2 grosze, 3 dukaty i korona".
*/
#define MONEY_TEXT(arr, przyp) ((string)MONEY_FN->money_text(arr, przyp))

/**
 * MONEY_TEXT_SPLIT zwraca string opisuj�cy warto�� monet 'cc' 
 * w monetach o najwy�szych mo�liwych nomina�ach, w przypadku 'przyp'.
 * @param cc - warto�� w groszach
 * @param przyp - przypadek, w kt�rym chcemy uzyska� string
 * @return string typu: "2 grosze, 3 dukaty i korona".
*/
#define MONEY_TEXT_SPLIT(cc, przyp) (MONEY_TEXT(MONEY_SPLIT(cc), przyp))

/**
 * MONEY_UNIQUE_NAME zwraca warto�� do u�ycia z HEAP_S_UNIQUE_ID 
 * dla monet typu 'ct'.
 */
#define MONEY_UNIQUE_NAME(ct)  ("_coin_" + (ct))

/**
 * MONEY_EXPAND i MONEY_CONDENSE zamieniaj� pieni�dze obiektu 
 * 'obj' odpowiednio z i na prop OBJ_M_HAS_MONEY tego obiektu.
 * Czyli dzi�ki temu obiekt mo�e mie� pieni�dze w propie a nie w inwentarzu.
 * 
 * Uwaga: motywacja tych makr jest troch� podejrzana. 
 * P�ki co nie u�ywa� bez pozwolenia Moldera.
 */
#define MONEY_CONDENSE(obj) MONEY_FN->money_condense(obj)
#define MONEY_EXPAND(obj)   MONEY_FN->money_expand(obj)

/**
 * Uwaga: makro nie dzia�a i to chyba od zawsze! 
 * W razie czego pyta� Moldera.
 *
 * MONEY_PARSE parsuje string 'str' i zwraca tablic� int�w w formie
 * ({ grosze, denary, korony }),  gdzie ka�dy element tablicy wskazuje ile
 * monet danego typu by�o wyszczeg�lnionych w parsowanym napisie.
 * Warto�� -1 kt�rego� elementu tablicy oznacza, �e wszystkie monety 
 * tego typu maj� zosta� u�yte. 
 *
 * Przyk�adowo, MONEY_PARSE("wszystkie grosze i 3 korony") zwr�ci
 * tablic� ({ -1, 0, 3}) (wszystkie grosze, �adnego denara, 3 korony).
 *  
 * @return Je�li makro zwr�ci 0 napis nie m�g� zosta� sparsowany.
 */
#define MONEY_PARSE(str) MONEY_FN->parse_coins(str);

/**
 * Uwaga: makro nie dzia�a i to chyba od zawsze! 
 * W razie czego pyta� Moldera.
 *
 * MONEY_PARSE_OB parsuje string 'str' i zwraca tablic� int�w w formie
 * ({ grosze, denary, korony }),  gdzie ka�dy element tablicy wskazuje ile
 * monet danego typu by�o wyszczeg�lnionych w parsowanym napisie.
 * W przeciwie�stwie do MONEY_PARSE, do tablicy nie jest nigdy wstawiane -1,
 * a zamiast tego obiekt 'ob' jest przeszukiwany, i wstawiona jest posiadana 
 * przez niego ilo�� monet danego typu.  
 *
 * Przyk�adowo, MONEY_PARSE_OB("wszystkie grosze i 3 korony", this_player())
 * zwr�ci tablic� ({ 20, 0, 3}) (wszystkie grosze, �adnego denara, 3 korony)
 * - zak�adaj�c, �e gracz posiada 20 groszy. 
 *
 * @return Je�li makro zwr�ci 0 napis nie m�g� zosta� sparsowany.
 */
#define MONEY_PARSE_OB(str, ob) MONEY_FN->parse_coins(str, ob)			  

/** Do��czamy plik zawieraj�cy szczeg�owe informacje o typach monet 
 * na naszym mudzie */
#include "/config/sys/money2.h"

/* Nie pisa� nic poni�ej tej linii. */
#endif MONEY_DEFINITIONS
