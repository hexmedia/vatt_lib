/*
 * Kalendarz Wiedzminlandu
 * (/sys/global/mudtime2.c)
 *
 * stworzony przez Moldera,
 * kwiecien 2004   
 */

#include <files.h>
#include <language.h>
#include <mudtime.h>
#include <sun_times.h>

/* Co jaki czas (w sekundach) czas mudowy uaktualnia sie z systemowym */
#define SYNC_TIME 43200.0

string Miesiac;
int Godzina, Minuta, Sekunda, Dzien, Rok;

public int pora_dnia_second();

/*
 * Pobiera czas z zegara systemowego
 */
void
get_zmienne(void)
{
    string dzien_tyg, miesiac, godzina, minuta, sekunda;    

    ({find_player("root"),find_player("molder")})->catch_msg("Update czasu: " + Godzina + ":" + Minuta + ":" + Sekunda + ".\n");
    
    /* sztuczka rodem z ctime() */
    sscanf(efun::ctime(time()), "%s %s %d %s:%s:%s %d", 
	dzien_tyg, Miesiac, Dzien, godzina, minuta, sekunda, Rok);

    Godzina = atoi(godzina);
    if (Godzina == 0)
    	Godzina = 24;
    Minuta = atoi(minuta);
    Sekunda = atoi(sekunda);
       
    ({find_player("molder"),find_player("molder")})->catch_msg("Po uaktualnieniu: " + Godzina + ":" + Minuta + ":" + Sekunda + ".\n");
}

/*
 * Alarm wywolywany co sekunde uaktualnia czas 
 */
private void
update_zmienne(void)
{
    if((Sekunda = (Sekunda + 1) % 60) == 0)
    {
	if((Minuta = (Minuta + 1) % 60) == 0)
	{
	    if((Godzina = (Godzina + 1) % 24) == 0)
    	    {
		Godzina = 24;
                /* �eby uaktualni� si� kalendarz */
                get_zmienne();
		ZEGAR_OBJECT->dzien(Dzien);
            }
	    ZEGAR_OBJECT->godzina(Godzina - 1);
	}
	ZEGAR_OBJECT->minuta(Minuta);
    }
}
	
/*
 * uruchomienie synchronizacji z zegarem systemowym 
 */
public void 
create()
{
    get_zmienne();
    set_alarm(SYNC_TIME, SYNC_TIME, "get_zmienne");
    set_alarm(1.0, 1.0, "update_zmienne");
}

/* 
 * opis godziny (pora, przed-po godzina)
 */

private string
godzina(void)
{
    string uzup, godzina, uzup2;
    int godzinapom = Godzina;
    
    switch(pora_dnia_second())
    {
      case MT_SWIT:
        uzup2 = "�wit";
        break;
      case MT_WCZESNY_RANEK:
        uzup2 = "wczesny ranek";
        break;
      case MT_RANEK:
        uzup2 = "ranek";
        break;
      case MT_POLUDNIE:
        uzup2 = "�rodek dnia";
        break;
      case MT_POPOLUDNIE:
        uzup2 = "popo�udnie";
        break;
      case MT_WIECZOR:
        uzup2 = "wiecz�r";
        break;
      case MT_POZNY_WIECZOR:
        uzup2 = "p�ny wiecz�r";
        break;
      case MT_NOC:
        uzup2 = "noc";
        break;
    }
 
    /* jesli trzeba doda godzine */
    switch (Minuta)
    {
	case 0..4:
	    uzup = " godzina ";
	    break;
	case 5..30:
	    uzup = " po ";
	    break;
	case 31..55:
	    godzinapom += 1;
	    uzup = " przed ";	
	    break;
	case 56..60:
	    godzinapom += 1;
	    uzup = " godzina ";
	    break;
    }	

    switch(uzup)
    {
	case " przed ": 
		godzina = LANG_SORD(((godzinapom - 1) % 12) + 1, PL_BIE, PL_ZENSKI);
		break;
	case " godzina ":
		godzina = LANG_SORD(((godzinapom - 1) % 12) + 1, PL_MIA, PL_ZENSKI);
		break;
	case " po ":
		godzina = LANG_SORD(((godzinapom - 1) % 12) + 1, PL_DOP, PL_ZENSKI);
		break;
	default : return "b^l^ad w przed_po";
    }

return uzup2 + "," + uzup + godzina ;

}		 


/*
 * sprawdza, czy rok jest przestepny,
 * zwraca 1 jesli jest, 0 jesli nie jest.
 */

private int
przestepny(int rok)
{
    if((rok % 4) == 0)
	return 1;
    else
        return 0;
}

/* 
 * ustala Dzien jako liczbe dni ktore minely od poczatku roku,
 * po czym zwraca ja
 */

private int
dzien_roku(void)
{
    int dzien = Dzien;
    	
    switch(Miesiac)
	{
	case "Jan":
	    break;
	case "Dec":
	    dzien += 30;
	case "Nov":
	    dzien += 31;
	case "Oct":
	    dzien += 30;
	case "Sep":
	    dzien += 31;
	case "Aug":
	    dzien += 30;
	case "Jul":
	    dzien += 31;
	case "Jun":
	    dzien += 31;
	case "May":
	    dzien += 30;
	case "Apr":
	    dzien += 31;
	case "Mar":
	    dzien += 28 + przestepny(Rok);
	case "Feb":
 	    dzien += 31;
	    break;
	default:
	    dzien = -1;
	}
return dzien;
}
    
/*
 * zamienia liczbe dni od poczatku roku na date z kalendarza elfow.
 * (jezeli chcesz te funkcje zrozumiec, to na poczatku  
 * uswiadom sobie skutki roku przestepnego) 
 *
 * argument: dzien roku
 */

public string
data_elfow(int dzien)
{
    int prz;
    string miesiac;
    
    prz = przestepny(Rok);
  
    if (dzien > 0 && dzien < 36 )
    {
	miesiac = "Yule";	
	dzien = dzien + 10; 
    }

    else if (dzien > 35 && dzien < (80 + prz))
    {
	miesiac = "Imbaelk";
	dzien = dzien - 35;
    }

    else if (dzien > (79 + prz) && dzien < (127 + prz))
    {
	miesiac = "Birke";
	dzien = dzien - 79 - prz;
    }
    
    else if (dzien > (126 + prz) && dzien < (175 + prz))
    {
	miesiac = "Blathe";
	dzien = dzien - 126 - prz;
    }

    else if (dzien > (174 + prz) && dzien < (220 + prz))
    {
	miesiac = "Feainn";
	dzien = dzien - 174 - prz;
    }

    else if (dzien > (219 + prz) && dzien < (266 + prz))
    {
	miesiac = "Lammas";
	dzien = dzien - 219 - prz;
    }

    else if (dzien > (265 + prz) && dzien < (311 + prz))
    {
	miesiac = "Velen";
	dzien = dzien - 265 - prz;
    }

    else if (dzien > (310 + prz) && dzien < (356 + prz))
    {
	miesiac = "Saovine";
	dzien = dzien - 310 - prz;
    }

    else if (dzien > (355 + prz) && dzien < (366 + prz))
    {
	miesiac = "Yule";
	dzien = dzien - 355 - prz;
    }

    else return "b��d w data_elfow";
 
    return LANG_SORD(dzien, PL_MIA, PL_MESKI_OS) + " " + miesiac;
}


/*
 * odliczanie do wazniejszych swiat w wiedzminlandzie.
 * argument: dzien roku.
 */


public string
swieto(void)
{

    int dzien, prz, d;    
    string *za, *co;
 
    dzien = dzien_roku();
    prz = przestepny(Rok);

    d = dzien - prz;    // patrz switch. latwiej jednak olac :)


    za = ({"Za tydzie�", "Za sze�� dni", "Za pi�� dni", "Za cztery dni",
	   "Za trzy dni", "Za dwa dni", "Jutro", "Dzi� jest"});
    
    co = ({" �wi�to Imbaelk!\n", " �wi�to Birke!\n", " �wi�to Belleteyn!\n", 
	   " �wi�to Midaete!\n", " �wi�to Lammas!\n", " �wi�to Velen!\n", 
	   " �wi�to Saovine!\n", " �wi�to Midinvaerne!\n"});
	
    
if ((dzien > 28) && (dzien < 177))
{
    if (dzien < 82)
    {
	switch(dzien)
    {
	case 29: return za[0]+co[0];
	case 30: return za[1]+co[0];
	case 31: return za[2]+co[0];
	case 32: return za[3]+co[0];
	case 33: return za[4]+co[0];
	case 34: return za[5]+co[0];
	case 35: return za[6]+co[0];
	case 36: return za[7]+co[0];
    }
	switch(d)
    {
    	case 73: return za[0]+co[1];
	case 74: return za[1]+co[1];
	case 75: return za[2]+co[1];
	case 76: return za[3]+co[1];
	case 77: return za[4]+co[1];
	case 78: return za[5]+co[1];
	case 79: return za[6]+co[1];
	case 80: return za[7]+co[1];
    }
    }

    if (d > 119)
	switch(d)
    {
	case 120: return za[0]+co[2];
	case 121: return za[1]+co[2];
	case 122: return za[2]+co[2];
	case 123: return za[3]+co[2];
	case 124: return za[4]+co[2];
	case 125: return za[5]+co[2];
	case 126: return za[6]+co[2];
	case 127: return za[7]+co[2];
   	case 168: return za[0]+co[3];
	case 169: return za[1]+co[3];
	case 170: return za[2]+co[3];
	case 171: return za[3]+co[3];
	case 172: return za[4]+co[3];
	case 173: return za[5]+co[3];
	case 174: return za[6]+co[3];
	case 175: return za[7]+co[3];
    }
}

if ((d > 212) && (d < 357))
{
    if (d < 266)
        switch(d)
    {
	case 213: return za[0]+co[4];
	case 214: return za[1]+co[4];
	case 215: return za[2]+co[4];
	case 216: return za[3]+co[4];
	case 217: return za[4]+co[4];
	case 218: return za[5]+co[4];
	case 219: return za[6]+co[4];
	case 220: return za[7]+co[4];
    	case 259: return za[0]+co[5];
	case 260: return za[1]+co[5];
	case 261: return za[2]+co[5];
	case 262: return za[3]+co[5];
       	case 263: return za[4]+co[5];
	case 264: return za[5]+co[5];
	case 265: return za[6]+co[5];
	case 266: return za[7]+co[5];
    }
    
    
    if (d > 303)
       	switch(d)
    {
	case 304: return za[0]+co[6];
	case 305: return za[1]+co[6];
	case 306: return za[2]+co[6];
	case 307: return za[3]+co[6];
	case 308: return za[4]+co[6];
	case 309: return za[5]+co[6];
	case 310: return za[6]+co[6];
	case 311: return za[7]+co[6];
  	case 349: return za[0]+co[7];
	case 350: return za[1]+co[7];
	case 351: return za[2]+co[7];
	case 352: return za[3]+co[7];
	case 353: return za[4]+co[7];
	case 354: return za[5]+co[7];
	case 355: return za[6]+co[7];
	case 356: return za[7]+co[7];
    }
}

    return "";

}


/*
 * Zwraca opis dla komendy 'czas'
 */
 
public string
czas_na_mudzie(void)
{
    return "Jest " + godzina() + ".\nWedle rachuby elf�w mamy dzi� "
    	+ data_elfow(dzien_roku()) + ".\n" + swieto();
}


/*
 * do wykorzystania przy opisie lokacji.
 */

public int
pora_dnia_second()
{
  int cur_time = Godzina * 60 + Minuta;
  int cur_day = dzien_roku();
  int sunrise = SUN_TIMES[cur_day%365][0];
  int noon = 720; // 12 * 60
  int sunset = SUN_TIMES[cur_day%365][1];

  if (cur_time < sunrise - 45) {
    return MT_NOC;
  }
  if (cur_time < sunrise + ftoi(itof(noon - sunrise) * 0.05)) {
    return MT_SWIT;
  }
  if (cur_time < sunrise + ftoi(itof(noon - sunrise) * 0.5)) {
    return MT_WCZESNY_RANEK;
  }
  if (cur_time < noon) {
    return MT_RANEK;
  }
  if (cur_time < noon + ftoi(itof(sunset - noon) * 0.17)) {
    return MT_POLUDNIE;
  }
  if (cur_time < noon + ftoi(itof(sunset - noon) * 0.79)) {
    return MT_POPOLUDNIE;
  }
  if (cur_time < sunset + 10) {
    return MT_WIECZOR;
  }
  if (cur_time < sunset + 45) {
    return MT_POZNY_WIECZOR;
  }
  return MT_NOC;
}

public int
query_day()
{
    return Dzien;
}

public int
query_hour()
{
    return Godzina;
}

public int
query_nhour()
{
    int toReturn = Godzina;
    if (Minuta >= 30)
        toReturn++;
    if (toReturn == 25)
        toReturn = 1;
    return toReturn;
}

public int
query_minute()
{
    return Minuta;
}

public int
query_second()
{
     return Sekunda;
}

public string
query_time()
{
    string time;

    time = sprintf("%d:%d:%d",Godzina, Minuta, Sekunda);
   
    return time;
}
