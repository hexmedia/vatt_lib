/**
 * System obs�ugi tag�w.
 * Napisany przez Delviego, poprawki Krun.
 *
 * TODO:
 *  - trzeba poprawi� kolejno�� wychwytywania tag�w zako�czenia
 *    bo je�li teraz zrobimy w tagu taki sam tag, to zostanie on 
 *    wcze�niej zako�czony.
 *  - zapami�tywanie kolejno�ci ustawianych kolor�w.
 */

#pragma strict_types
#pragma no_clone
#pragma no_inherit
#pragma resident

#include <macros.h>
#include <mudtime.h>
#include <colors.h>

#define TAG_LT      ""
#define TAG_GT      ""

string xml_to_ansi(string xml, mixed vp=0);

public object
find_previous_living()
{
    int i;
    for(i=0;;i++)
    {
        if(living(previous_object(-i)))
           return previous_object(-i);
        if(!previous_object(-i))
            return previous_object(-1);
    }
}

public int
always()
{
    return 1;
}

public int
never()
{
    return 0;
}

public int
test(string str)
{
    write(str);
    return atoi(str);
}

public int
dzien()
{
    object room = ENV(find_previous_living());
    return !room->dzien_noc();
}

public int
noc()
{
    object room = ENV(find_previous_living());
    return room->dzien_noc();
}

public int
wiosna()
{
    object room = ENV(find_previous_living());
    return (room->pora_roku() == MT_WIOSNA);
}

public int
lato()
{
    object room = ENV(find_previous_living());
    return (room->pora_roku() == MT_LATO);
}

public int
jesien()
{
    object room = ENV(find_previous_living());
    return (room->pora_roku() == MT_JESIEN);
}

public int
zima()
{
    object room = ENV(find_previous_living());
    return (room->pora_roku() == MT_ZIMA);
}

public string
process_tags(string str, mixed vp=0)
{
    int gt;
    string prefix, postfix, tag, body, *args;
    string str1, str2, *tmp;

    if(!str)
        return "";

    //Doda�em w pierwszej kolejno�ci sprawdzanie tag�w kolork�w. Krun
    str = xml_to_ansi(str, vp);
   // write("Otrzymany od xml_to_ansi: " + str + "\n");
    while (wildmatch("*<*>*", str))
    {
//         write("str1 = " + str + "\n");

        sscanf(str, "%s<%s>%s", prefix, str1, str2);

        if(!str1)
        {
            str = prefix + TAG_LT + str1 + TAG_GT + str2;
            continue;
        }

        if(str1[0] == '/')
        {
            str = prefix + TAG_LT + str1 + TAG_GT + str2;
            continue;
        }

        tmp = explode(str1, ":");
        if(sizeof(tmp) > 0)
            tag = tmp[0];
        else
            tag = str1;

        //Je�li funkcja nie istnieje lub nie ma taga to nie zmianiamy str'a
        if(!tag || !function_exists(tag, TO))
        {
            str = prefix + TAG_LT + str1 + TAG_GT + str2;
            continue;
        }

        if(sizeof(tmp) > 1)
            args = tmp[1..];
        else
            args = ({ });

        if(!wildmatch("*</"+tag+">*", str2))
            str2 += "</"+tag+">";

        sscanf(str2, "%s</" + tag + ">%s", body, postfix);

        if(call_selfv(tag, args))
            str = prefix + body + postfix;
        else
            str = prefix + postfix;
//          write("str2 = " + str + "\n");
    }
    string *exp;
    exp = explode(str, TAG_LT);
    if(sizeof(exp) > 1)
        str = implode(exp, "<");
    else if(sizeof(exp) == 1)
        str = exp[0];

    if(str[-1..-1] == TAG_GT)
        gt = 1;

    exp = explode(str, TAG_GT);
    if(sizeof(exp) > 1)
        str = implode(exp, ">");
    else if(sizeof(exp) == 1)
        str = exp[0];

    return str + (gt ? ">" : "");
}

string
xml_to_ansi(string str, mixed vp=0)
{
    int color, colorc, gt;
    string prefix, postfix, tag, body, arg;
    string str1, str2="", *tmp;

    if(!str)
        return "";

    while (wildmatch("*<*>*", str))
    {
//         write("str1 = " + str + "\n");
        sscanf(str, "%s<%s>%s", prefix, str1, str2);

        if(!str1)
        {
            str = prefix + TAG_LT + str1 + TAG_GT + str2;
            continue;
        }

        if(str1[0] == '/')
        {
            str = prefix + TAG_LT + str1 + TAG_GT + str2;
            continue;
        }

        if(sizeof(tmp = explode(str1, "=")) >= 2)
        {
            tag = tmp[0];
            arg = implode(tmp[1..], "=");
        }
        else
            tag = str1;

        switch(tag)
        {
            case "b":
            case "big":
            case "bold":
                color   = COLOR_BOLD_ON;
                colorc  = COLOR_BOLD_OFF;
                break;
            case "strikethrough":
            case "strike":
            case "s":
                color   = COLOR_STRIKETHROUGH_ON;
                colorc  = COLOR_STRIKETHROUGH_OFF;
                break;
            case "inverse":
                color   = COLOR_INVERSE_ON;
                colorc  = COLOR_INVERSE_OFF;
                break;
            case "i":
            case "italics":
                color   = COLOR_ITALICS_ON;
                colorc  = COLOR_ITALICS_OFF;
                break;
            case "u":
            case "underline":
                color   = COLOR_UNDERLINE_ON;
                colorc  = COLOR_UNDERLINE_OFF;
                break;
            case "dark":
                color   = COLOR_DARK;
                colorc  = COLOR_FG_DEFAULT;//Nie ma do tego odpowiedniej funkcji.
                break;
            case "color":
                switch(arg)
                {
                    case "black":
                        color = COLOR_FG_BLACK;
                        break;
                    case "red":
                        color = COLOR_FG_RED;
                        break;
                    case "green":
                        color = COLOR_FG_GREEN;
                        break;
                    case "yellow":
                        color = COLOR_FG_YELLOW;
                        break;
                    case "blue":
                        color = COLOR_FG_BLUE;
                        break;
                    case "magenta":
                        color = COLOR_FG_MAGENTA;
                        break;
                    case "cyan":
                        color = COLOR_FG_CYAN;
                        break;
                    case "white":
                        color = COLOR_FG_WHITE;
                        break;
                    case "default":
                        color = COLOR_FG_DEFAULT;
                        break;
                    default:
                        str = prefix + TAG_LT + str1 + TAG_GT + str2;
                        continue;
                }
                colorc  = COLOR_FG_DEFAULT;
                break;
            case "background":
            case "back":
            case "bg":
                switch(arg)
                {
                    case "black":
                        color = COLOR_BG_BLACK;
                        break;
                    case "red":
                        color = COLOR_BG_RED;
                        break;
                    case "green":
                        color = COLOR_BG_GREEN;
                        break;
                    case "yellow":
                        color = COLOR_BG_YELLOW;
                        break;
                    case "blue":
                        color = COLOR_BG_BLUE;
                        break;
                    case "magenta":
                        color = COLOR_BG_MAGENTA;
                        break;
                    case "cyan":
                        color = COLOR_BG_CYAN;
                        break;
                    case "white":
                        color = COLOR_BG_WHITE;
                        break;
                    case "default":
                        color = COLOR_BG_DEFAULT;
                        break;
                    default:
                        str = prefix + TAG_LT + str1 + TAG_GT + str2;
                    continue;
                }
                colorc  = COLOR_BG_DEFAULT;
                break;
            default:
                str = prefix + TAG_LT + str1 + TAG_GT + str2;
                continue;
        }

        if(!wildmatch("*</"+tag+">*", str2))
            str2 += "</"+tag+">";

        sscanf(str2, "%s</" + tag + ">%s", body, postfix);

        if(vp == 1)
            str = prefix + SET_COLOR(color) + body + SET_COLOR(colorc) + postfix;
        else if(objectp(vp) && interactive(vp))
            str = prefix + set_color(vp, color) + body + set_color(vp, colorc) + postfix;
        else if(!objectp(vp))
            str = prefix + set_color(color) + body + set_color(colorc) + postfix;

    }

    string *exp;
    exp = explode(str, TAG_LT);
    if(sizeof(exp) > 1)
        str = implode(exp, "<");
    else if(sizeof(exp) == 1)
        str = exp[0];

    if(str[-1..-1] == TAG_GT)
        gt = 1;

    exp = 0;
    exp = explode(str, TAG_GT);
    if(sizeof(exp) > 1)
        str = implode(exp, ">");
    else if(sizeof(exp) == 1)
        str = exp[0];

    return str + (gt ? ">" : "");
}
