#pragma no_clone
//#pragma strict_types

mixed Zarost_dlugosci = ([ ]), Broda_dlugosci = ([ ]), Wasy_dlugosci = ([ ]);
mixed Broda_typy = ([ ]), Wasy_typy = ([ ]);

void
create()
{
    int i;
    int min, max;
    string str;
    mixed tmp;

    setuid();
    seteuid(getuid());

    set_auth(this_object(), "root:root");

    Zarost_dlugosci = explode(read_file("/d/Standard/obj/zarost/zarost_dlugosci.txt"), "\n");
    Broda_dlugosci = explode(read_file("/d/Standard/obj/zarost/broda_dlugosci.txt"), "\n");
    Wasy_dlugosci = explode(read_file("/d/Standard/obj/zarost/wasy_dlugosci.txt"), "\n");

    tmp = explode(read_file("/d/Standard/obj/zarost/broda_typy.txt"), "\n");
    for (i = 0; i < sizeof(tmp); i++)
    {
	if (sscanf(tmp[i], "%s:%d-%d", str, min, max) == 3)
	    Broda_typy[str] = ({ min, max });
	else
	    Broda_typy[tmp[i]] = ({ 1, sizeof(Broda_dlugosci) });
    }

    tmp = explode(read_file("/d/Standard/obj/zarost/wasy_typy.txt"), "\n");
    for (i = 0; i < sizeof(tmp); i++)
    {
	if (sscanf(tmp[i], "%s:%d-%d", str, min, max) == 3)
	    Wasy_typy[str] = ({ min, max });
	else
	    Wasy_typy[tmp[i]] = ({ 1, sizeof(Wasy_dlugosci) });
    }
}

