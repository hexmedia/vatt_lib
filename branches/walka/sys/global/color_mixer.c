#include <colors.h>
#include <macros.h>

// colors - ({ ({ ({255, 255, 255}), 50}), ({ ({0, 0, 0}), 50}) })
// - mieszanie bialego z czarnym w rownych proporcjach

int *
hex_to_rgb(string hex)
{
    mixed r, g, b;
    
    dump_array(hex);
    if(!hex)
        return 0;
    
    if(strlen(hex) > 6)
        hex = hex[0..5];
    if(strlen(hex) < 6)
        hex = sprintf("%-06s", hex);
    
    r = hex[0..1];
    b = hex[2..3];
    g = hex[4..5];
    
    sscanf(r, "%x", r);
    sscanf(g, "%x", g);
    sscanf(b, "%x", b);
    
    return ({r, b, g});
}

string
rgb_to_hex(int *rgb)
{
    return sprintf("%02s", sprintf("%x", rgb[0])) + 
        sprintf("%02s", sprintf("%x", rgb[1])) + sprintf("%02s", sprintf("%x", rgb[2]));
}

int
map_filter1(mixed i)
{
    return sizeof(i);
}

int *
generate_new_color(mixed colors)
{
    int b=0, g=0, r=0, mix = 0, i;
    
    if(!sizeof(colors))
        return 0;
    if(fold(map(colors, map_filter1), &operator(+)(), 0) % 4 != 0)
        return 0;
    
    for(i=0;i<sizeof(colors);i++)
    {
        r += colors[i][0][0] * colors[i][1];
        g += colors[i][0][1] * colors[i][1];
        b += colors[i][0][2] * colors[i][1];
        mix += colors[i][1];   
    }
    
    r /= mix;
    g /= mix;
    b /= mix;
    
    return ({ r, g, b });
}

int *
map_filter2(mixed color)
{
    return ({hex_to_rgb(color[0]), color[1]});
}

mixed
generate_new_color_hex(mixed colors)
{
    return generate_new_color(map(colors, map_filter2));
}

int
map_filter3(int *rgb)
{
    return rgb[0] + rgb[1] + rgb[2];
}

int
sort_func(int i, int j)
{
    return i >= j;
}

mixed
find_nearest_color(int *rgb)
{
    int i, r, t, t0, t1, t2, r0, r1, r2, nc, rg, rd;
    mixed color, *colors;
    
    color = rgb_to_hex(rgb);
    colors = map(CL_RGB_KOLOROW, rgb_to_hex);
    
    if(member_array(color, colors) != -1)
        return rgb + ({0});
    
    color = rgb[0] + rgb[1] + rgb[2];
    colors = map(CL_RGB_KOLOROW, map_filter3);
    
    // Ehh cos mi sie zdaje ze ten kod pod spodem to trza przetlumaczyc:P Bo
    // ja sam nie bede za tydzien wiedzial o co biega...
    // Zacznijmy od wyjsnienia zmiennych. Otoz:
    // t  - to roznica miedzy suma rgb koloru ktory podalismy
    //      i koloru ktory aktualnie sprawdzamy,
    // t0 - tez roznica.. ale tylko skladnika r(ed),
    // t1 - jw.. ale skladnika g(reen)
    // t2 - jw.. skladnika b(lue)
    // r  - jak t z tym ze nie jest to aktualna roznica tylko
    //      ta ustawiona juz wczesniej.. jak o tym nizej.
    // r0..2 - podobnie do r.
    // nc - rgb mowego koloru
    
    // Teraz co to robi.. a robi... Chyba nawet dobrze:)
    // Otoz... pierwsze co robi petla: 
    // 1) blicza zmienne t,
    // 2) pozniej sprawdza zmiennym r nadane sa
    //    juz jakies wartosc jesli nie to krok 4)
    // 3) porownuje sume wartosci t do sumy wartosci r
    // 4) jesli suma t jest mniejsza od sumy r to
    //    za r wstawia t, i ustawia nc - nowy kolor.
    // Chyba tyle.. jak cos nizrozumiale to pytac szybko..
    // zanim zapomne co mialem na mysli..
    for(i=0;i<sizeof(colors);i++)
    {
        t  = ABS(colors[i] - color);
        t0 = ABS(CL_RGB_KOLOROW[i][0] - rgb[0]);  
        t1 = ABS(CL_RGB_KOLOROW[i][1] - rgb[1]);
        t2 = ABS(CL_RGB_KOLOROW[i][2] - rgb[2]);
        //write("T=" + t + ";R=" + r + ";T0=" + t0 + ";R0=" + r0 + ";T1=" + t1 + ";R1=" + r1 + ";T2=" + t2 + ";R2=" + r2 +"\n");
        if((!r && !r0 && !r1 && !r2) || (r + r0 + r1 + r2 > t + t0 + t1 + t2))
        {
            //write(rgb_to_hex(CL_RGB_KOLOROW[i]));
            r  = t;
            r0 = t0;
            r1 = t1;
            r2 = t2;
            nc = CL_RGB_KOLOROW[i];
        }
    }
    return nc;
}   

string
query_color_name(int *rgb, int przyp, int rodzaj, int liczba)
{
    int i;
    string *colors, color;
    
    rgb = find_nearest_color(rgb);
    
    colors = map(CL_RGB_KOLOROW, rgb_to_hex);
    color  = rgb_to_hex(rgb);
    
    i = member_array(color, colors);
    
    if(i==-1)
        find_player("krun")->catch_msg("kolorki sie sypia:P\n");
    
    return oblicz_przym(CL_NAZWY_KOLOROW[i][0], CL_NAZWY_KOLOROW[i][1],
        przyp, rodzaj, liczba);
}
