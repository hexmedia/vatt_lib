#pragma no_clone
#pragma strict_types

#include <materialy.h>


mixed Nazwy = ([
	MATERIALY_SREBRO : ({ "srebro", "srebra", "srebru", "srebro",
		"srebrem", "srebrze" }),
	MATERIALY_ZLOTO : ({ "z這to", "z這ta", "z這tu", "z這to", "z這tem",
		"z這cie" }),
	MATERIALY_MITHRIL : ({ "mithril", "mithrilu", "mithrilowi", "mithril",
		"mithrilem", "mithrilu" }),
	MATERIALY_RUBIN : ({ "rubin", "rubinu", "rubinowi", "rubin", "rubinem", "rubinie" }),
        MATERIALY_DIAMENT : ({ "diament", "diamentu", "diamentowi", "diament", "diamentem", "diamencie" }),

	MATERIALY_DR_CIS : ({ "cisowe drewno", "cisowego drewna",
		"cisowemu drewnu", "cisowe drewno", "cisowym drewnem", 
		"cisowym drewnie" }),
	MATERIALY_DR_BUK : ({ "bukowe drewno", "bukowego drewna",
		"bukowemu drewnu", "bukowe drewno", "bukowym drewnem",
		"bukowym drewnie" }),
	MATERIALY_DR_BRZOZA : ({ "brzozowe drewno", "brzozowego drewna",
		"brzozowemu drewnu", "brzozowe drewno", "brzozowym drewnem",
		"brzozowym drewnie" }),
	MATERIALY_DR_DAB : ({ "d瑿owe drewno", "d瑿owego drewna",
		"d瑿owemu drewnu", "d瑿owe drewno", "d瑿owym drewnem",
		"d瑿owym drewnie" }),
	MATERIALY_DR_SWIERK : ({ "鈍ierkowe drewno", "鈍ierkowego drewna",
		"鈍ierkowemu drewnu", "鈍ierkowe drewno", "鈍ierkowym drewnem",
		"鈍ierkowym drewnie" }),
	MATERIALY_DR_SOSNA : ({ "sosnowe drewno", "sosnowego drewna",
		"sosnowemu drewnu", "sosnowe drewno", "sosnowym drewnem",
		"sosnowym drewnie" }),
	MATERIALY_DR_TOPOLA : ({ "topolowe drewno", "topolowego drewna",
		"topolowemu drewnu", "topolowe drewno", "topolowym drewnem",
		"topolowym drewnie" }),
	MATERIALY_DR_WIERZBA : ({ "wierzbowe drewno", "wierzbowego drewna",
		"wierzbowemu drewnu", "wierzbowe drewno", "wierzbowym drewnem",
		"wierzbowym drewnie" }),
	MATERIALY_DR_WIAZ : ({ "wi您owe drewno", "wi您owego drewna",
		"wi您owemu drewnu", "wi您owe drewno", "wi您owym drewnem",
		"wi您owym drewnie" }),
	MATERIALY_DR_BERBERYS : ({ "berberysowe drewno",
		"berberysowego drewna", "berberysowemu drewnu",
		"berberysowe drewno", "berberysowym drewnem",
		"berberysowym drewnie" }),
	MATERIALY_DR_LIPA : ({ "lipowe drewno", "lipowego drewna",
		"lipowemu drewnu", "lipowe drewno", "lipowym drewnem",
		"lipowym drewnie" }),
	MATERIALY_DR_KLON : ({ "klonowe drewno", "klonowego drewna",
		"klonowemu drewnu", "klonowe drewno", "klonowym drewnem",
		"klonowym drewnie" }),
	MATERIALY_DR_JESION : ({ "jesionowe drewno", "jesionowego drewna",
		"jesionowemu drewnu", "jesionowe drewno", "jesionowym drewnem",
		"jesionowym drewnie" }),
	MATERIALY_DR_JODLA : ({ "jod這we drewno", "jod這wego drewna",
		"jod這wemu drewnu", "jod這we drewno", "jod這wym drewnem",
		"jod這wym drewnie" }),
	MATERIALY_DR_JARZEBINA : ({ "jarz瑿inowe drewno",
		"jarz瑿inowego drewna", "jarz瑿inowemu drewnu",
		"jarz瑿inowe drewno", "jarz瑿inowym drewnem",
		"jarz瑿inowym drewnie" }),

	MATERIALY_MARMUR : ({ "marmur", "marmuru", "marmurowi", "marmur",
		"marmurem", "marmurze" }),
	MATERIALY_GRANIT : ({ "granit", "granitu", "granitowi", "granit",
		"granitem", "granicie" }),
	MATERIALY_ZELAZO : ({ "瞠lazo", "瞠laza", "瞠lazu", "瞠lazo",
		"瞠lazem", "瞠lazie" }),
	MATERIALY_STAL : ({ "stal", "stali", "stali", "stal", "stal�",
		"stali" }),
	MATERIALY_MIEDZ : ({ "mied�", "miedzi", "miedzi", "mied�", "miedzi�",
		"miedzi" }),
	MATERIALY_MOSIADZ : ({ "mosi康z", "mosi康zu", "mosi康zowi",
		"mosi康z", "mosi康zem", "mosi康zu" }),
	MATERIALY_SZKLO : ({ "szk這", "szk豉", "szk逝", "szk這", "szk貫m",
		"szkle" }),
	MATERIALY_KWARC : ({ "kwarc", "kwarcu", "kwarcowi", "kwarc",
		"kwarcem", "kwarcu" }),
	MATERIALY_WIKLINA : ({ "wiklina", "wikliny", "wiklinie", "wiklin�",
		"wiklin�", "wiklinie" }),

	MATERIALY_JEDWAB : ({ "jedwab", "jedwabiu", "jedwabiowi", "jedwab",
		"jedwabiem", "jedwabiu" }),
	MATERIALY_WELNA : ({ "we軟a", "we軟y", "we軟ie", "we軟�", "we軟�",
		"we軟ie" }),
        MATERIALY_BAWELNA : ({ "bawe軟a", "bawe軟y", "bawe軟ie", "bawe軟�",
		"bawe軟�", "bawe軟ie" }),
	MATERIALY_ATLAS : ({ "at豉s", "at豉su", "at豉sowi", "at豉s",
		"at豉sem", "at豉sie" }),
	MATERIALY_AKSAMIT : ({ "aksamit", "aksamitu", "aksamitowi",
		"aksamit", "aksamitem", "aksamicie" }),
	MATERIALY_WELUR : ({ "welur", "weluru", "welurowi", "welur",
		"welurem", "welurze" }),
	MATERIALY_TIUL : ({ "tiul", "tiulu", "tiulowi", "tiul", "tiulem",
		"tiulu" }),
	MATERIALY_SZYFON : ({ "szyfon", "szyfonu", "szyfonowi", "szyfon",
		"szyfonem", "szyfonie" }),
	MATERIALY_TK_PIKOWA : ({ "tkanina pikowa", "tkaniny pikowej",
		"tkaninie pikowej", "tkanin� pikow�", "tkanin� pikow�",
		"tkaninie pikowej" }),
	MATERIALY_JUTA : ({ "juta", "juty", "jucie", "jut�", "jut�",
		"jucie" }),
	MATERIALY_LEN : ({ "len", "lnu", "lnu", "len", "lnem", "lnie" }),
	MATERIALY_SATYNA : ({ "satyna", "satyny", "satynie", "satyn�",
		"satyn�", "satynie" }),
	MATERIALY_KREPA : ({ "krepa", "krepy", "krepie", "krep�", "krep�",
		"krepie" }),
	MATERIALY_MULINA : ({ "mulina", "muliny", "mulinie", "mulin�",
		"mulin�", "mulinie" }),
	MATERIALY_KORDONEK : ({ "kordonek", "kordonka", "kordonkowi",
		"kordonek", "kordoniem", "kordonku" }),

        MATERIALY_SK_BYDLE : ({ "bydl璚a sk鏎a", "bydl璚ej sk鏎y",
		"bydl璚ej sk鏎ze", "bydl璚� sk鏎�", "bydl璚� sk鏎�",
		"bydl璚ej sk鏎ze" }),
        MATERIALY_SK_CIELE : ({ "ciel璚a sk鏎a", "ciel璚ej sk鏎y",
		"ciel璚ej sk鏎ze", "ciel璚� sk鏎�", "ciel璚� sk鏎�",
		"ciel璚ej sk鏎ze" }),
        MATERIALY_SK_KROLIK : ({ "kr鏊icza sk鏎a", "kr鏊iczej sk鏎y",
		"kr鏊iczej sk鏎ze", "kr鏊icz� sk鏎�", "kr鏊icz� sk鏎�",
		"kr鏊iczej sk鏎ze" }),
        MATERIALY_SK_WILK : ({ "wilcza sk鏎a", "wilczej sk鏎y",
		"wilczej sk鏎ze", "wilcz� sk鏎�", "wilcz� sk鏎�",
		"wilczej sk鏎ze" }),
        MATERIALY_SK_NIEDZWIEDZ : ({ "nied德iedzia sk鏎a",
		"nied德iedziej sk鏎y", "nied德iedziej sk鏎ze",
		"nied德iedzi� sk鏎�", "nied德iedzi� sk鏎�",
		"nied德iedziej sk鏎ze" }),
	MATERIALY_SK_SWINIA : ({ "鈍i雟ka sk鏎a", "鈍i雟kiej sk鏎y",
		"鈍i雟kiej sk鏎ze", "鈍i雟k� sk鏎�", "鈍i雟k� sk鏎�",
		"鈍i雟kiej sk鏎ze" }),
        MATERIALY_IN_ROSLINA : ({ "ro郵ina", "ro郵iny", "ro郵inie",
                "ro郵in�", "ro郵in�", "ro郵inie"}),
    ]);


mixed Gestosc = ([
	MATERIALY_SREBRO : 10.5,
	MATERIALY_ZLOTO : 19.3,
	MATERIALY_MITHRIL : 1.0,/*FIXME*/
	MATERIALY_RUBIN : 1.0,/*FIXME*/
        MATERIALY_DIAMENT : 1.0, /*FIXME*/

	MATERIALY_DR_CIS : 1.0,/*FIXME*/
	MATERIALY_DR_BUK : 0.86,
	MATERIALY_DR_BRZOZA : 0.65,
	MATERIALY_DR_DAB : 0.8,
	MATERIALY_DR_SWIERK : 0.585,
	MATERIALY_DR_SOSNA : 0.55,
	MATERIALY_DR_TOPOLA : 0.4,
	MATERIALY_DR_WIERZBA : 1.0,/*FIXME*/
	MATERIALY_DR_WIAZ : 0.815,
	MATERIALY_DR_BERBERYS : 1.0,/*FIXME*/
	MATERIALY_DR_LIPA : 0.45,
	MATERIALY_DR_KLON : 0.765,
	MATERIALY_DR_JESION : 0.75,
	MATERIALY_DR_JODLA : 0.725,
	MATERIALY_DR_JARZEBINA : 1.0,/*FIXME*/

	MATERIALY_MARMUR : 2.7,
	MATERIALY_GRANIT : 2.7,
	MATERIALY_ZELAZO : 7.8,
	MATERIALY_STAL : 7.8,
	MATERIALY_MIEDZ : 8.9,
	MATERIALY_MOSIADZ : 8.6,
	MATERIALY_SZKLO : 2.5,
	MATERIALY_KWARC : 1.0,/*FIXME*/
	MATERIALY_WIKLINA : 1.0,/*FIXME*/

	MATERIALY_JEDWAB : 1.0,/*FIXME*/
	MATERIALY_WELNA : 1.0,/*FIXME*/
	MATERIALY_BAWELNA : 1.0,/*FIXME*/
	MATERIALY_ATLAS : 1.0,/*FIXME*/
	MATERIALY_AKSAMIT : 1.0,/*FIXME*/
	MATERIALY_WELUR : 1.0,/*FIXME*/
	MATERIALY_TIUL : 1.0,/*FIXME*/
	MATERIALY_SZYFON : 1.0,/*FIXME*/
	MATERIALY_TK_PIKOWA : 1.0,/*FIXME*/
	MATERIALY_JUTA : 1.0,/*FIXME*/
	MATERIALY_LEN : 1.0,/*FIXME*/
	MATERIALY_SATYNA : 1.0,/*FIXME*/
	MATERIALY_KREPA : 1.0,/*FIXME*/
	MATERIALY_MULINA : 1.0,/*FIXME*/
	MATERIALY_KORDONEK : 1.0,/*FIXME*/

	MATERIALY_SK_BYDLE : 1.0,/*FIXME*/
	MATERIALY_SK_CIELE : 1.0,/*FIXME*/
	MATERIALY_SK_KROLIK : 1.0,/*FIXME*/
	MATERIALY_SK_WILK : 1.0,/*FIXME*/
	MATERIALY_SK_NIEDZWIEDZ : 1.0,/*FIXME*/
	MATERIALY_SK_SWINIA : 1.0,/*FIXME*/
	MATERIALY_IN_ROSLINA : 1.0,/*FIXME*/
    ]);

mixed Wytrzymalosc = ([
	MATERIALY_SREBRO : 1.0,/*FIXME*/
	MATERIALY_ZLOTO : 1.0,/*FIXME*/
	MATERIALY_MITHRIL : 1.0,/*FIXME*/
	MATERIALY_RUBIN : 1.0,/*FIXME*/
	MATERIALY_DIAMENT:  1.0, /*FIXME*/
	
	MATERIALY_DR_CIS : 1.0,/*FIXME*/
	MATERIALY_DR_BUK : 1.0,/*FIXME*/
	MATERIALY_DR_BRZOZA : 1.0,/*FIXME*/
	MATERIALY_DR_DAB : 1.0,/*FIXME*/
	MATERIALY_DR_SWIERK : 1.0,/*FIXME*/
	MATERIALY_DR_SOSNA : 1.0,/*FIXME*/
	MATERIALY_DR_TOPOLA : 1.0,/*FIXME*/
	MATERIALY_DR_WIERZBA : 1.0,/*FIXME*/
	MATERIALY_DR_WIAZ : 1.0,/*FIXME*/
	MATERIALY_DR_BERBERYS : 1.0,/*FIXME*/
	MATERIALY_DR_LIPA : 1.0,/*FIXME*/
	MATERIALY_DR_KLON : 1.0,/*FIXME*/
	MATERIALY_DR_JESION : 1.0,/*FIXME*/
	MATERIALY_DR_JODLA : 1.0,/*FIXME*/
	MATERIALY_DR_JARZEBINA : 1.0,/*FIXME*/

	MATERIALY_MARMUR : 1.0,/*FIXME*/
	MATERIALY_GRANIT : 1.0,/*FIXME*/
	MATERIALY_ZELAZO : 1.0,/*FIXME*/
	MATERIALY_STAL : 1.0,/*FIXME*/
	MATERIALY_MIEDZ : 1.0,/*FIXME*/
	MATERIALY_MOSIADZ : 1.0,/*FIXME*/
	MATERIALY_SZKLO : 1.0,/*FIXME*/
	MATERIALY_KWARC : 1.0,/*FIXME*/
	MATERIALY_WIKLINA : 1.0,/*FIXME*/

	MATERIALY_JEDWAB : 1.0,/*FIXME*/
	MATERIALY_WELNA : 1.0,/*FIXME*/
	MATERIALY_BAWELNA : 1.0,/*FIXME*/
	MATERIALY_ATLAS : 1.0,/*FIXME*/
	MATERIALY_AKSAMIT : 1.0,/*FIXME*/
	MATERIALY_WELUR : 1.0,/*FIXME*/
	MATERIALY_TIUL : 1.0,/*FIXME*/
	MATERIALY_SZYFON : 1.0,/*FIXME*/
	MATERIALY_TK_PIKOWA : 1.0,/*FIXME*/
	MATERIALY_JUTA : 1.0,/*FIXME*/
	MATERIALY_LEN : 1.0,/*FIXME*/
	MATERIALY_SATYNA : 1.0,/*FIXME*/
	MATERIALY_KREPA : 1.0,/*FIXME*/
	MATERIALY_MULINA : 1.0,/*FIXME*/
	MATERIALY_KORDONEK : 1.0,/*FIXME*/

	MATERIALY_SK_BYDLE : 1.0,/*FIXME*/
	MATERIALY_SK_CIELE : 1.0,/*FIXME*/
	MATERIALY_SK_KROLIK : 1.0,/*FIXME*/
	MATERIALY_SK_WILK : 1.0,/*FIXME*/
	MATERIALY_SK_NIEDZWIEDZ : 1.0,/*FIXME*/
	MATERIALY_SK_SWINIA : 1.0,/*FIXME*/
	MATERIALY_IN_ROSLINA : 1.0,/*FIXME*/
    ]);

mixed Kruchosc = ([
	MATERIALY_SREBRO : 1.0,/*FIXME*/
	MATERIALY_ZLOTO : 1.0,/*FIXME*/
	MATERIALY_MITHRIL : 1.0,/*FIXME*/
	MATERIALY_RUBIN : 1.0,/*FIXME*/
        MATERIALY_DIAMENT : 1.0, /*FIXME*/

	MATERIALY_DR_CIS : 1.0,/*FIXME*/
	MATERIALY_DR_BUK : 1.0,/*FIXME*/
	MATERIALY_DR_BRZOZA : 1.0,/*FIXME*/
	MATERIALY_DR_DAB : 1.0,/*FIXME*/
	MATERIALY_DR_SWIERK : 1.0,/*FIXME*/
	MATERIALY_DR_SOSNA : 1.0,/*FIXME*/
	MATERIALY_DR_TOPOLA : 1.0,/*FIXME*/
	MATERIALY_DR_WIERZBA : 1.0,/*FIXME*/
	MATERIALY_DR_WIAZ : 1.0,/*FIXME*/
	MATERIALY_DR_BERBERYS : 1.0,/*FIXME*/
	MATERIALY_DR_LIPA : 1.0,/*FIXME*/
	MATERIALY_DR_KLON : 1.0,/*FIXME*/
	MATERIALY_DR_JESION : 1.0,/*FIXME*/
	MATERIALY_DR_JODLA : 1.0,/*FIXME*/
	MATERIALY_DR_JARZEBINA : 1.0,/*FIXME*/

	MATERIALY_MARMUR : 1.0,/*FIXME*/
	MATERIALY_GRANIT : 1.0,/*FIXME*/
	MATERIALY_ZELAZO : 1.0,/*FIXME*/
	MATERIALY_STAL : 1.0,/*FIXME*/
	MATERIALY_MIEDZ : 1.0,/*FIXME*/
	MATERIALY_MOSIADZ : 1.0,/*FIXME*/
	MATERIALY_SZKLO : 1.0,/*FIXME*/
	MATERIALY_KWARC : 1.0,/*FIXME*/
	MATERIALY_WIKLINA : 1.0,/*FIXME*/

	MATERIALY_JEDWAB : 1.0,/*FIXME*/
	MATERIALY_WELNA : 1.0,/*FIXME*/
	MATERIALY_BAWELNA : 1.0,/*FIXME*/
	MATERIALY_ATLAS : 1.0,/*FIXME*/
	MATERIALY_AKSAMIT : 1.0,/*FIXME*/
	MATERIALY_WELUR : 1.0,/*FIXME*/
	MATERIALY_TIUL : 1.0,/*FIXME*/
	MATERIALY_SZYFON : 1.0,/*FIXME*/
	MATERIALY_TK_PIKOWA : 1.0,/*FIXME*/
	MATERIALY_JUTA : 1.0,/*FIXME*/
	MATERIALY_LEN : 1.0,/*FIXME*/
	MATERIALY_SATYNA : 1.0,/*FIXME*/
	MATERIALY_KREPA : 1.0,/*FIXME*/
	MATERIALY_MULINA : 1.0,/*FIXME*/
	MATERIALY_KORDONEK : 1.0,/*FIXME*/

	MATERIALY_SK_BYDLE : 1.0,/*FIXME*/
	MATERIALY_SK_CIELE : 1.0,/*FIXME*/
	MATERIALY_SK_KROLIK : 1.0,/*FIXME*/
	MATERIALY_SK_WILK : 1.0,/*FIXME*/
	MATERIALY_SK_NIEDZWIEDZ : 1.0,/*FIXME*/
	MATERIALY_SK_SWINIA : 1.0,/*FIXME*/
	MATERIALY_IN_ROSLINA : 1.0,/*FIXME*/
    ]);

// int: 0 lub 1
mixed Latwopalnosc = ([
	MATERIALY_SREBRO : 0,
	MATERIALY_ZLOTO : 0,
	MATERIALY_MITHRIL : 0,
	MATERIALY_RUBIN : 0,
	MATERIALY_DIAMENT : 1,
	
	MATERIALY_DR_CIS : 1,
	MATERIALY_DR_BUK : 1,
	MATERIALY_DR_BRZOZA : 1,
	MATERIALY_DR_DAB : 1,
	MATERIALY_DR_SWIERK : 1,
	MATERIALY_DR_SOSNA : 1,
	MATERIALY_DR_TOPOLA : 1,
	MATERIALY_DR_WIERZBA : 1,
	MATERIALY_DR_WIAZ : 1,
	MATERIALY_DR_BERBERYS : 1,
	MATERIALY_DR_LIPA : 1,
	MATERIALY_DR_KLON : 1,
	MATERIALY_DR_JESION : 1,
	MATERIALY_DR_JODLA : 1,
	MATERIALY_DR_JARZEBINA : 1,

	MATERIALY_MARMUR : 0,
	MATERIALY_GRANIT : 0,
	MATERIALY_ZELAZO : 0,
	MATERIALY_STAL : 0,
	MATERIALY_MIEDZ : 0,
	MATERIALY_MOSIADZ : 0,
	MATERIALY_SZKLO : 0,
	MATERIALY_KWARC : 0,
	MATERIALY_WIKLINA : 1,

	MATERIALY_JEDWAB : 1,
	MATERIALY_WELNA : 1,
	MATERIALY_BAWELNA : 1,
	MATERIALY_ATLAS : 1,
	MATERIALY_AKSAMIT : 1,
	MATERIALY_WELUR : 1,
	MATERIALY_TIUL : 1,
	MATERIALY_SZYFON : 1,
	MATERIALY_TK_PIKOWA : 1,
	MATERIALY_JUTA : 1,
	MATERIALY_LEN : 1,
	MATERIALY_SATYNA : 1,
	MATERIALY_KREPA : 1,
	MATERIALY_MULINA : 1,
	MATERIALY_KORDONEK : 1,

	MATERIALY_SK_BYDLE : 1,
	MATERIALY_SK_CIELE : 1,
	MATERIALY_SK_KROLIK : 1,
	MATERIALY_SK_WILK : 1,
	MATERIALY_SK_NIEDZWIEDZ : 1,
	MATERIALY_SK_SWINIA : 1,
	MATERIALY_IN_ROSLINA : 1,
    ]);

public string
nazwa_materialu(string m, int przyp = 0)
{
    return Nazwy[m][przyp];
}

public float
gestosc_materialu(string m)
{
    return Gestosc[m];
}

public float
wytrzymalosc_materialu(string m)
{
    return Wytrzymalosc[m];
}

public float
kruchosc_materialu(string m)
{
    return Kruchosc[m];
}

public int
latwopalnosc_materialu(string m)
{
    return Latwopalnosc[m];
}

