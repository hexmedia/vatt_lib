#ifndef OBJECT_TYPES_H
#define OBJECT_TYPES_H

#define O_MEBLE                 (1 << 0)  /* np. szafka, stol */
#define O_NARZEDZIA             (1 << 1)  /* np. mlotek, wiadro, motyka */
#define O_UBRANIA               (1 << 2)  /* np. kurtka, plaszcz, stanik */
#define O_BUTY                  (1 << 3)
#define O_KAMIENIE_SZLACHETNE   (1 << 4)
#define O_BIZUTERIA             (1 << 5)  /* np. kolczyki, pierscionki */
#define O_SMIECI                (1 << 6)  /* np. rozbite butelki */
#define O_SCIENNE               (1 << 7)  /* np. obrazy, zaslony, poroza */
#define O_KUCHENNE              (1 << 8)  /* np. patelnie, garnki */
#define O_JEDZENIE              (1 << 9)  /* np. chleb */

#define O_INNE                  (1 << 10)  /* wszystkie inne! */
#define O_ZWOJE                 (1 << 11)  /* np. mapy, manuskrypty, kartki */
#define O_KSIAZKI               (1 << 12)
#define O_KOSCI                 (1 << 13)  /* np. zeby */
#define O_ELIKSIRY              (1 << 14)
#define O_ZIOLA                 (1 << 15)
#define O_LINY                  (1 << 16)
#define O_LAMPY                 (1 << 17)
#define O_POCHODNIE             (1 << 18)
#define O_MONETY                (1 << 19)

#define O_ZBROJE                (1 << 20)
#define O_BRON_MIECZE           (1 << 21)
#define O_BRON_MIECZE_2H        (1 << 22)
#define O_BRON_SZABLE           (1 << 23)
#define O_BRON_SZTYLETY         (1 << 24)
#define O_BRON_TOPORY           (1 << 25)
#define O_BRON_TOPORY_2H        (1 << 26)
#define O_BRON_DRZEWCOWE_D      (1 << 27)
#define O_BRON_DRZEWCOWE_K      (1 << 28)
#define O_BRON_MACZUGI          (1 << 29)
#define O_BRON_MLOTY            (1 << 30)
#define O_BRON_MIOTANE          (1 << 31)
#define O_BRON_STRZELECKIE      (1 << 32)
#define O_BRON_STRZALY          (1 << 33)

#define O_POCHWY                (1 << 34)  /* na bronie */
#define O_DREWNO                (1 << 35)
#define O_KWIATY                (1 << 36)
#define O_NA_WLOSY              (1 << 37)
#define O_SKORY                 (1 << 38)

#define O_RYBY                  (1 << 39)

#define O_WEDKARSKIE            (1 << 39)


#endif OBJECT_TYPES_H
