/*
 * /sys/stdproperties.h
 *
 * This file hold definition of all the standard properties that an object
 * can have. The documentation for the properties can all be found in the
 * directory /doc/man/properties.
 *
 * The following prefixes are used:
 *
 * ARMOUR_ - armour property
 * CONT_   - container property
 * SUBLOC_ - sublocation property
 * CORPSE_ - corpse property
 * HEAP_   - heap property
 * HERB_   - herb property
 * LIVE_   - living property
 * MAGIC_  - magic property
 * OBJ_    - object property
 * PLAYER_ - player property
 * SELLER_ - seller property
 * NPC_    - npc property
 * ROOM_   - room property
 * TEMP_   - temporary property
 * WIZARD_ - wizard property
 *
 * The first infix (i.e. the part after the prefix) identifies the type
 * of the property. The following values are possible:
 *
 * AX - array of type X, where X is either of I F O S FUN M
 * I  - integer
 * F  - float
 * O  - object
 * S  - string
 * FUN - function
 * M  - mixed
 */

#ifndef PROP_DEF
#define PROP_DEF

/** *********************************************************
 * Armour properties
 */

#define ARMOUR_I_MAGIC_PARRY            "_armour_i_magic_parry"

/* troszk�stuffu na potrzeby rozmiar� */
#define ARMOUR_S_DLA_RASY               "_armour_s_dla_rasy"
#define ARMOUR_I_DLA_PLCI               "_armour_i_dla_plci"
#define ARMOUR_F_PRZELICZNIK            "_armour_f_przelicznik"
#define ARMOUR_I_D_ROZCIAGLIWOSC        "_armour_i_d_rozciagliwosc"
#define ARMOUR_I_U_ROZCIAGLIWOSC        "_armour_i_u_rozciagliwosc"
#define ARMOUR_F_POWIEKSZENIE           "_armour_f_powiekszenie"
#define ARMOUR_F_POW_GLOWA              "_armour_f_pow_glowa"
#define ARMOUR_F_POW_SZYJA              "_armour_f_pow_szyja"
#define ARMOUR_F_POW_BARKI              "_armour_f_pow_barki"
#define ARMOUR_F_POW_DL_RAMIE           "_armour_f_pow_dl_ramie"
#define ARMOUR_F_POW_OB_RAMIE           "_armour_f_pow_ob_ramie"
#define ARMOUR_F_POW_DL_PRAMIE          "_armour_f_pow_dl_pramie"
#define ARMOUR_F_POW_OB_PRAMIE          "_armour_f_pow_ob_pramie"
#define ARMOUR_F_POW_NADGARSTEK         "_armour_f_pow_nadgarstek"
#define ARMOUR_F_POW_DL_DLON            "_armour_f_pow_dl_dlon"
#define ARMOUR_F_POW_SZ_DLON            "_armour_f_pow_sz_dlon"
#define ARMOUR_F_POW_TULOW              "_armour_f_pow_tulow"
#define ARMOUR_F_POW_PIERSI             "_armour_f_pow_piersi"
#define ARMOUR_F_POW_PAS                "_armour_f_pow_pas"
#define ARMOUR_F_POW_BIODRA             "_armour_f_pow_biodra"
#define ARMOUR_F_POW_DL_UDO             "_armour_f_pow_dl_udo"
#define ARMOUR_F_POW_OB_UDO             "_armour_f_pow_ob_udo"
#define ARMOUR_F_POW_DL_GOLEN           "_armour_f_pow_dl_golen"
#define ARMOUR_F_POW_OB_GOLEN           "_armour_f_pow_ob_golen"
#define ARMOUR_F_POW_KOSTKA             "_armour_f_pow_kostka"
#define ARMOUR_F_POW_DL_STOPA           "_armour_f_pow_dl_stopa"
#define ARMOUR_F_POW_SZ_STOPA           "_armour_f_pow_sz_stopa"

#define ARMOUR_I_NECKLACE_FULL          "_armour_i_necklace_full"
#define ARMOUR_I_NECKLACE               "_armour_i_necklace"


/** *********************************************************
 * Container properties
 */

#define CONT_I_IN                       "_cont_i_in"
#define CONT_M_NO_INS                   "_cont_m_no_ins"
#define CONT_I_NO_REM                   "_cont_i_no_rem"
#define CONT_M_NO_REM                   "_cont_m_no_rem"

#define CONT_I_ATTACH                   "_cont_i_attach"
#define CONT_I_CLOSED                   "_cont_i_closed"
#define CONT_I_DONT_SHOW_CONTENTS       "_cont_i_dont_show_contents"
#define CONT_I_HEIGHT                   "_cont_i_height"
#define CONT_I_HIDDEN                   "_cont_i_hidden"
#define CONT_I_LIGHT                    "_cont_i_light"
#define CONT_I_LOCK                     "_cont_i_lock"
#define CONT_I_RIGID                    "_cont_i_rigid"
#define CONT_I_TRANSP                   "_cont_i_transparent"
#define CONT_I_VOLUME                   "_cont_i_volume"
#define CONT_I_WEIGHT                   "_cont_i_weight"
#define CONT_I_MAX_VOLUME               "_cont_i_max_volume"
#define CONT_I_MAX_WEIGHT               "_cont_i_max_weight"
#define CONT_I_MAX_RZECZY               "_cont_i_max_rzeczy"
#define CONT_I_IL_RZECZY                "_cont_i_il_rzeczy"
#define CONT_I_REDUCE_VOLUME            "_cont_i_reduce_volume"
#define CONT_I_REDUCE_WEIGHT            "_cont_i_reduce_weight"
#define CONT_I_HOLDS_COMPONENTS         "_cont_i_holds_components"
#define CONT_I_IS_QUIVER                "_cont_i_is_quiver"

/* jeremian's changes */

#define CONT_I_NO_OPEN_DESC             "_cont_i_no_open_desc"
#define CONT_I_CANT_OPENCLOSE           "_cont_i_cant_openclose"
#define CONT_M_OPENCLOSE                "_cont_m_openclose"

#define CONT_I_CANT_WLOZ_POD            "_cont_i_cant_wloz_pod"
#define CONT_I_CANT_WLOZ_DO             "_cont_i_cant_wloz_do"
#define CONT_I_CANT_ODLOZ               "_cont_i_cant_odloz"
#define CONT_I_CANT_POLOZ               "_cont_i_cant_poloz"
// defaultowo nie mo�na wiesza� wi�c prop niepotrzebny
//#define CONT_I_CANT_POWIES          "_cont_i_cant_powies"

/* jednak pierwsze intuicje okazay si�prawdziwe... */

#define SUBLOC_I_MOZNA_WLOZ             "_subloc_i_mozna_wloz"
#define SUBLOC_I_MOZNA_ODLOZ            "_subloc_i_mozna_odloz"
#define SUBLOC_I_MOZNA_POLOZ            "_subloc_i_mozna_poloz"
#define SUBLOC_I_MOZNA_POWIES           "_subloc_i_mozna_powies"

#define SUBLOC_I_RODZAJ                 "_subloc_i_rodzaj"
#define SUBLOC_I_TYLKO_MN               "_subloc_i_tylko_mn"
#define SUBLOC_I_OB_PRZYP               "_subloc_i_ob_przyp"
#define SUBLOC_S_OB_GDZIE               "_subloc_s_ob_gdzie"
#define SUBLOC_I_POD_NIE_ZOSTANA        "_subloc_i_pod_nie_zostana"
#define SUBLOC_I_NA_NIE_SPADNA          "_subloc_i_na_nie_spadna"
#define SUBLOC_I_TYP_POD                "_subloc_i_typ_pod"
#define SUBLOC_I_TYP_NA                 "_subloc_i_typ_na"
#define SUBLOC_I_TYP_W                  "_subloc_i_typ_w"
#define SUBLOC_I_DLA_O                  "_subloc_i_dla_o"

/* Tego ten...do zamykania oczu & blokujace np. popatrz, mrugnij, oczko. Lil */
#define EYES_CLOSED                     "_eyes_closed"

/* A ten prop dodalam dla zwyklych (nic nie dajacych) emotek w ubrankach. Lil */
#define OBJ_I_ZAPIETY                   "_obj_i_zapiety"

/* A ten ustawia zapach przedmiotu. Zagrywka czysto rpgowa:P Lil.  */
//#define OBJ_I_SMELL                        "_obj_i_smell"
//Jednak zrezygnowalam. Jest lepsze wyjscie.
/* --- */

/** *********************************************************
 * Corpse properties
 */
#define CORPSE_M_RACE                   "_corpse_m_race"
#define CORPSE_M_PRACE			"_corpse_m_prace"
#define CORPSE_I_RRACE                  "_corpse_i_rrace"
#define CORPSE_AS_KILLER                "_corpse_as_killer"

/** *********************************************************
 * Door properties
 */
#define DOOR_I_HEIGHT                   "_door_i_height"
#define DOOR_I_KEY                      "_door_i_key"

/** *********************************************************
 * Gate properties
 */
#define ROOM_AS_GATEID                  "_room_as_gateid"
#define ROOM_AO_GATEOB                  "_room_ao_gateob"

/** *********************************************************
 * Heap properties
 */
#define HEAP_I_IS                       "_heap_i_is"
#define HEAP_S_UNIQUE_ID                "_heap_s_unique_id"
#define HEAP_I_UNIT_LIGHT               "_heap_i_unit_light"
#define HEAP_I_UNIT_VALUE               "_heap_i_unit_value"
#define HEAP_I_UNIT_VOLUME              "_heap_i_unit_volume"
#define HEAP_I_UNIT_WEIGHT              "_heap_i_unit_weight"

/** *********************************************************
 * Herb properties
 */

#define HERB_I_USED                     "_herb_i_used"

/** *********************************************************
 * Living properties
 */

#define LIVE_I_IS                       "_live_i_is"
#define LIVE_I_NON_FORGET               "_live_i_non_forget"
#define LIVE_I_NON_REMEMBER             "_live_i_non_remember"
#define LIVE_I_ALWAYSKNOWN              "_live_i_alwaysknown"
#define LIVE_I_NEVERKNOWN               "_live_i_neverknown"
#define LIVE_I_MAX_INTOX                "_live_i_max_intox"
#define LIVE_I_MIN_INTOX                "_live_i_min_intox"
#define LIVE_I_MAX_DRINK                "_live_i_max_drink"
#define LIVE_I_MAX_EAT                  "_live_i_max_eat"
#define LIVE_I_SEE_INVIS                "_live_i_see_invis"
#define LIVE_I_SEE_DARK                 "_live_i_see_dark"
#define LIVE_S_SOULEXTRA                "_live_s_soulextra"
#define LIVE_AS_ATTACK_FUMBLE           "_live_as_attack_fumble"
#define LIVE_I_ATTACK_DELAY             "_live_i_attack_delay"
#define LIVE_I_STUNNED                  "_live_i_stunned"
#define LIVE_I_QUICKNESS                "_live_i_quickness"
#define LIVE_O_LAST_KILL                "_live_o_last_kill"
#define LIVE_O_SPELL_ATTACK             "_live_o_spell_attack"
#define LIVE_O_ENEMY_CLING              "_live_o_enemy_cling"
#define LIVE_S_EXTRA_SHORT              "_live_s_extra_short"
#define LIVE_O_LAST_ROOM                "_live_o_last_room"
#define LIVE_O_LAST_LAST_ROOM           "_live_o_last_last_room"
#define LIVE_S_LAST_MOVE                "_live_s_last_move"
#define LIVE_I_UNDEAD                   "_live_i_undead"
#define LIVE_I_NO_CORPSE                "_live_i_no_corpse"
#define LIVE_I_NO_BODY                  "_live_i_no_body"
#define LIVE_I_MEDITATES                "_live_i_meditates"
#define LIVE_I_SNEAK                    "_live_i_sneak"
#define LIVE_I_LANGUAGE                 "_live_i_language"
#define LIVE_I_NO_FOOTPRINTS            "_live_i_no_footprints"
#define LIVE_I_NO_GENDER_DESC           "_live_i_no_gender_desc"
#define LIVE_I_CONCENTRATE              "_live_i_concentrate"
#define LIVE_O_CONCENTRATE              "_live_i_concentrate"
#define LIVE_S_BREAK_CONCENTRATE        "_live_s_break_concentrate"
#define LIVE_I_MOUTH_BLOCKED            "_live_m_mouth_blocked"
#define LIVE_M_MOUTH_BLOCKED            "_live_m_mouth_blocked"
#define LIVE_O_STEED                    "_live_o_steed"
#define LIVE_I_NO_ACCEPT_GIVE           "_live_m_no_accept_give"
#define LIVE_M_NO_ACCEPT_GIVE           "_live_m_no_accept_give"
#define LIVE_M_STOP_FIGHTING            "_live_m_stop_fighting"
#define LIVE_AO_THIEF                   "_live_ao_thief"
#define LIVE_I_VICTIM_ADDED_AWARENESS   "_live_i_victim_added_awareness"
#define LIVE_I_LAST_STEAL               "_live_i_last_steal"
#define LIVE_I_BACKSTABBING             "_live_i_backstabbing"
#define LIVE_I_GOT_BACKSTABBED          "_live_i_got_backstabbed"
#define LIVE_I_ATTACK_THIEF             "_live_i_attack_thief"
#define LIVE_M_ATTACK_THIEF             "_live_m_attack_thief"
#define LIVE_AO_SPARRING                "_live_ao_sparring"
#define LIVE_I_INTRODUCING              "_live_i_introducing"
#define LIVE_I_SILENT_MOVE              "_live_i_silent_move"
#define LIVE_M_NO_MOVE                  "_live_m_no_move"
#define LIVE_I_FOUND_HERBS              "_live_i_found_herbs"
#define LIVE_I_UKRYWAL_SIE_W_KTO        "_live_i_ukrywal_sie_w_kto"

/* Definicje efekt�w zi� */
#define LIVE_I_HERB_EFF_STR             "_live_i_herb_eff_str"
#define LIVE_I_HERB_EFF_DEX             "_live_i_herb_eff_dex"
#define LIVE_I_HERB_EFF_CON             "_live_i_herb_eff_con"
#define LIVE_I_HERB_EFF_INT             "_live_i_herb_eff_int"
#define LIVE_I_HERB_EFF_WIS             "_live_i_herb_eff_wis"
#define LIVE_I_HERB_EFF_DIS             "_live_i_herb_eff_dis"
#define LIVE_I_HERB_EFF_HAL             "_live_i_herb_eff_hal"
#define LIVE_I_HERB_EFF_HP              "_live_i_herb_eff_hp"
#define LIVE_I_HERB_EFF_FAT             "_live_i_herb_eff_fat"
#define LIVE_I_HERB_EFF_ODT             "_live_i_herb_eff_odt"
#define LIVE_I_HERB_EFF_HAL             "_live_i_herb_eff_hal"

/** *********************************************************
 * Sellers properties
 */
#define SELLER_I_NO_BARGAIN             "_seller_i_no_bargain"


/** *********************************************************
 * Magic properties
 */

#define MAGIC_AM_ID_INFO                "_magic_am_id_info"
#define MAGIC_I_ILLUSION                "_magic_i_illusion"
#define MAGIC_AM_MAGIC                  "_magic_am_magic"

#define MAGIC_I_RES_ACID                "_magic_i_res_acid"
#define MAGIC_I_RES_COLD                "_magic_i_res_cold"
#define MAGIC_I_RES_ELECTRICITY         "_magic_i_res_electricity"
#define MAGIC_I_RES_LIGHT               "_magic_i_res_electricity"
#define MAGIC_I_RES_IDENTIFY            "_magic_i_res_identify"
#define MAGIC_I_RES_MAGIC               "_magic_i_res_magic"
#define MAGIC_I_RES_POISON              "_magic_i_res_poison"
#define MAGIC_I_RES_ILLUSION            "_magic_i_res_illusion"

#define MAGIC_I_RES_AIR                 "_magic_i_res_air"
#define MAGIC_I_RES_WATER               "_magic_i_res_water"
#define MAGIC_I_RES_EARTH               "_magic_i_res_earth"
#define MAGIC_I_RES_FIRE                "_magic_i_res_fire"
#define MAGIC_I_RES_LIFE                "_magic_i_res_life"
#define MAGIC_I_RES_DEATH               "_magic_i_res_death"

#define MAGIC_I_BREATHE_WATER           "_magic_i_breath_water"
#define MAGIC_I_BREATH_WATER            MAGIC_I_BREATHE_WATER

/** *********************************************************
 * Object properties
 */
#define OBJ_I_LIGHT                     "_obj_i_light"
#define OBJ_I_VALUE                     "_obj_i_value"
#define OBJ_I_VOLUME                    "_obj_i_volume"
#define OBJ_I_WEIGHT                    "_obj_i_weight"

#define OBJ_I_NO_GET                    "_obj_m_no_get"
#define OBJ_M_NO_GET                    "_obj_m_no_get"
#define OBJ_I_NO_DROP                   "_obj_m_no_drop"
#define OBJ_M_NO_DROP                   "_obj_m_no_drop"
#define OBJ_I_NO_GIVE                   "_obj_m_no_give"
#define OBJ_M_NO_GIVE                   "_obj_m_no_give"
#define OBJ_I_NO_INS                    "_obj_m_no_ins"
#define OBJ_M_NO_INS                    "_obj_m_no_ins"

#define OBJ_I_HIDE                      "_obj_i_hide"
#define OBJ_I_INVIS                     "_obj_i_invis"
#define OBJ_I_WHO_INVIS                 "_obj_i_who_invis"
#define OBJ_S_WIZINFO                   "_obj_s_wizinfo"

#define OBJ_I_BROKEN                    "_obj_i_broken"
#define OBJ_I_IS_MAGIC_ARMOUR           "_obj_i_is_magic_armour"
#define OBJ_I_IS_MAGIC_WEAPON           "_obj_i_is_magic_weapon"
#define OBJ_O_LOOTED_IN_ROOM            "_obj_o_looted_in_room"
#define OBJ_I_AERODYNAMICZNOSC          "_obj_i_aerodynamicznosc"

#define OBJ_M_NO_MAGIC                  "_obj_m_no_magic"
#define OBJ_I_NO_MAGIC                  "_obj_m_no_magic"
#define OBJ_M_NO_TELEPORT               "_obj_m_no_teleport"
#define OBJ_I_NO_TELEPORT               "_obj_m_no_teleport"
#define OBJ_M_NO_MAGIC_ATTACK           "_obj_m_no_magic_attack"
#define OBJ_I_NO_MAGIC_ATTACK           "_obj_m_no_magic_attack"
#define OBJ_M_NO_STEAL                  "_obj_m_no_steal"
#define OBJ_I_NO_STEAL                  "_obj_m_no_steal"
#define OBJ_M_NO_ATTACK                 "_obj_m_no_attack"
#define OBJ_I_NO_ATTACK                 "_obj_m_no_attack"

#define OBJ_I_SEARCH_TIME               "_obj_i_search_time"
#define OBJ_S_SEARCH_FUN                "_obj_s_search_fun"

#define OBJ_I_CONTAIN_WATER             "_obj_i_contain_water"
#define OBJ_I_HAS_FIRE                  "_obj_i_has_fire"
#define OBJ_M_HAS_MONEY                 "_obj_m_has_money"

#define OBJ_I_ATTACHABLE                "_obj_i_attachable"

#define OBJ_I_NO_BUY                    "_obj_m_no_buy"
#define OBJ_M_NO_BUY                    "_obj_m_no_buy"
#define OBJ_I_NO_SELL                   "_obj_m_no_sell"
#define OBJ_M_NO_SELL                   "_obj_m_no_sell"
#define OBJ_I_IN_REPAIR_BY_CRAFTSMAN    "_obj_i_in_repair_by_craftsman"

#define PRE_OBJ_MAGIC_RES               "_obj"

#define OBJ_I_RES_MAGIC                 "_obj_magic_i_res_magic"
#define OBJ_I_RES_ACID                  "_obj_magic_i_res_acid"
#define OBJ_I_RES_COLD                  "_obj_magic_i_res_cold"
#define OBJ_I_RES_ELECTRICITY           "_obj_magic_i_res_electricity"
#define OBJ_I_RES_LIGHT                 "_obj_magic_i_res_electricity"
#define OBJ_I_RES_POISON                "_obj_magic_i_res_poison"
#define OBJ_I_RES_IDENTIFY              "_obj_magic_i_res_identify"

#define OBJ_I_RES_AIR                   "_obj_magic_i_res_air"
#define OBJ_I_RES_WATER                 "_obj_magic_i_res_water"
#define OBJ_I_RES_EARTH                 "_obj_magic_i_res_earth"
#define OBJ_I_RES_FIRE                  "_obj_magic_i_res_fire"
#define OBJ_I_RES_LIFE                  "_obj_magic_i_res_life"
#define OBJ_I_RES_DEATH                 "_obj_magic_i_res_death"
#define OBJ_I_ALIGN                     "_obj_i_align"
#define OBJ_AM_POISONOUS                "_obj_am_poisonous"
#define OBJ_AM_POISONOUS_INFO           "_obj_am_poisonous_info"

#define OBJ_AI_RES_MAGIC                "_obj_ai_res_magic"

/* do full-wypasionych compositow ze wspolnym desc'em ;) /d */
#define OBJ_S_EXTRA_DESC                "_obj_s_extra_desc"

/* z draga */
#define OBJ_I_DONT_GLANCE               "_obj_i_dont_glance"
#define OBJ_I_DONT_SHOW_IN_LONG         "_obj_i_dont_show_in_long"
//to na dole: po prostu nie pokazuje przedmiotu w inventory. Lil.
#define OBJ_I_DONT_INV                  "_obj_i_dont_show_in_inv"

//#define OBJ_I_KRADZIONE               "_obj_i_kradzione"

#define OBJ_I_STAY_IN_WORLD             "_obj_i_stay_in_world"

/** *********************************************************
 * Player properties
 */
#define PLAYER_I_AUTOSAVE               "_player_i_autosave"
#define PLAYER_I_AUTOLOAD_TIME          "_player_i_autoload_time"
#define PLAYER_I_IN_TEAM_GLANCE_LATER   "_player_i_in_team_glance_later"
#define PLAYER_I_SEE_ERRORS             "_player_i_see_errors"
#define PLAYER_I_LASTXP                 "_player_i_lastxp"
#define PLAYER_I_LAST_FIGHT             "_player_i_last_fight"
#define PLAYER_I_LAST_NOTE              "_player_i_last_note"
#define PLAYER_I_NO_ACCEPT_GIVE         "_player_i_no_accept_give"
#define PLAYER_AS_REPLY_WIZARD          "_player_as_reply_wizard"
#define PLAYER_S_TRANSED_FROM           "_player_s_transed_from"
#define PLAYER_AO_HAS_EFFECTS           "_player_ao_has_effects"

#define PLAYER_FINAL_SEQUENCE           "_player_i_final_sequence"



/*
 * nazwy prop� do rozmiar� ubra�niech b� w playerze, bo
 * tam te s uywane.
 * 
 * --- jeremian */

#define PLAYER_F_ROZ_GLOWA              "_player_f_roz_glowa"
#define PLAYER_F_ROZ_SZYJA              "_player_f_roz_szyja"
#define PLAYER_F_ROZ_BARKI              "_player_f_roz_barki"
#define PLAYER_F_ROZ_DL_RAMIE           "_player_f_roz_dl_ramie"
#define PLAYER_F_ROZ_OB_RAMIE           "_player_f_roz_ob_ramie"
#define PLAYER_F_ROZ_DL_PRAMIE          "_player_f_roz_dl_pramie"
#define PLAYER_F_ROZ_OB_PRAMIE          "_player_f_roz_ob_pramie"
#define PLAYER_F_ROZ_NADGARSTEK         "_player_f_roz_nadgarstek"
#define PLAYER_F_ROZ_DL_DLON            "_player_f_roz_dl_dlon"
#define PLAYER_F_ROZ_SZ_DLON            "_player_f_roz_sz_dlon"
#define PLAYER_F_ROZ_TULOW              "_player_f_roz_tulow"
#define PLAYER_F_ROZ_PIERSI             "_player_f_roz_piersi"
#define PLAYER_F_ROZ_PAS                "_player_f_roz_pas"
#define PLAYER_F_ROZ_BIODRA             "_player_f_roz_biodra"
#define PLAYER_F_ROZ_DL_UDO             "_player_f_roz_dl_udo"
#define PLAYER_F_ROZ_OB_UDO             "_player_f_roz_ob_udo"
#define PLAYER_F_ROZ_DL_GOLEN           "_player_f_roz_dl_golen"
#define PLAYER_F_ROZ_OB_GOLEN           "_player_f_roz_ob_golen"
#define PLAYER_F_ROZ_KOSTKA             "_player_f_roz_kostka"
#define PLAYER_F_ROZ_DL_STOPA           "_player_f_roz_dl_stopa"
#define PLAYER_F_ROZ_SZ_STOPA           "_player_f_roz_sz_stopa"

/*
 * Te propy nie sa juz uzywane. Ich role przejal system options.
 * Po usunieciu starego kodu do usuniecia, jak mniemam. (Slvthrc)
 */
#define PLAYER_I_MORE_LEN               "_player_i_more_len"
#define PLAYER_I_SCREEN_WIDTH           "_player_i_screen_width"

/** *********************************************************
 * NPC properties
 */
#define NPC_M_NO_ACCEPT_GIVE            "_npc_m_no_accept_give"
#define NPC_I_NO_FEAR                   "_npc_i_no_fear"
#define NPC_I_NO_LOOKS                  "_npc_i_no_looks"
#define NPC_I_NO_RUN_AWAY               "_npc_i_no_run_away"

/** *********************************************************
 * Room properties
 */
#define ROOM_I_IS                       "_room_i_is"
#define ROOM_I_LIGHT                    "_room_i_light"
#define ROOM_I_INSIDE                   "_room_i_inside"
#define ROOM_I_TYPE                     "_room_i_type"
#define ROOM_NORMAL                     0
#define ROOM_IN_WATER                   1
#define ROOM_UNDER_WATER                2
#define ROOM_IN_AIR                     3
#define ROOM_BEACH                      4
#define ROOM_DESERT                     5
#define ROOM_FOREST                     6
#define ROOM_FIELD                      7
#define ROOM_CAVE                       8
#define ROOM_SWAMP                      9
#define ROOM_MOUNTAIN                   10
#define ROOM_TRACT                      11
#define ROOM_IN_CITY                    12
#define ROOM_TREE                       13
#define ROOM_I_NO_EXTRA_EXIT            "_room_i_no_extra_exit"
#define ROOM_I_NO_EXTRA_ITEM            "_room_i_no_extra_item"
#define ROOM_I_NO_EXTRA_DESC            "_room_i_no_extra_desc"
#define ROOM_AS_DOORID                  "_room_as_doorid"
#define ROOM_AO_DOOROB                  "_room_as_doorob"
#define ROOM_I_NO_CLEANUP               "_room_i_no_cleanup"
#define ROOM_AS_DIR                     "_room_as_dir"
#define ROOM_S_DARK_MSG                 "_room_s_dark_msg"
#define ROOM_S_DARK_LONG                "_room_s_dark_long"
#define ROOM_I_NO_SKY                   "_room_m_no_sky"
#define ROOM_M_NO_SKY                   "_room_m_no_sky"
#define ROOM_S_EXIT_FROM_DSC            "_room_s_exit_from_desc"

#define ROOM_I_NO_MAGIC                 "_room_m_no_magic"
#define ROOM_M_NO_MAGIC                 "_room_m_no_magic"
#define ROOM_I_NO_TELEPORT              "_room_m_no_teleport"
#define ROOM_M_NO_TELEPORT              "_room_m_no_teleport"
#define ROOM_M_NO_TELEPORT_TO           "_room_m_no_teleport_to"
#define ROOM_M_NO_TELEPORT_FROM         "_room_m_no_teleport_from"
#define ROOM_I_NO_MAGIC_ATTACK          "_room_m_no_magic_attack"
#define ROOM_M_NO_MAGIC_ATTACK          "_room_m_no_magic_attack"
#define ROOM_I_NO_STEAL                 "_room_m_no_steal"
#define ROOM_M_NO_STEAL                 "_room_m_no_steal"
#define ROOM_I_NO_ATTACK                "_room_m_no_attack"
#define ROOM_M_NO_ATTACK                "_room_m_no_attack"
#define ROOM_I_NO_ALLOW_STEED           "_room_i_no_allow_steed"
#define ROOM_I_ALLOW_STEED              "_room_i_allow_steed"
#define ROOM_I_NO_HORSE_LEAD            "_room_i_no_horse_lead"
#define ROOM_I_ALWAYS_BRIGHT            "_room_i_always_bright"

#define ROOM_I_HIDE                     "_room_i_hide"

#define ROOM_I_NIE_ZAPISUJ              "_room_i_nie_zapisuj"
#define ROOM_I_ALWAYS_LOAD              "_room_i_always_load"

/*prop definiujacy czy bylo tu ognisko. Tropienie moze to wykryc */
#define ROOM_WAS_FIRE                   "_room_was_fire"

/* wspolrzedne geograficzne */
#define ROOM_I_WSP_X                    "_room_i_wsp_x"
#define ROOM_I_WSP_Y                    "_room_i_wsp_y"

// Ilo�� chrustu na lokacji
#define ROOM_I_ILOSC_CHRUSTU            "_room_i_ilosc_chrustu"

#define CORPSE_S_OBJPATH                "_corpse_s_objpath"

/* Prop do zamknietej bramy miejskiej */
#define GATE_IS_CLOSED                  "_gate_is_closed"
/* Prop tez do bram. Dodawany chwilowo do gracza przedstawionego */
#define INTRODUCED_AT_GATE              "_introduced_at_gate"
/* Dwa propy definiujace, czy brama jest od strony miasta, czy z zewnatrz */
#define GATE_IS_INSIDE                  "_gate_is_inside"
#define GATE_IS_OUTSIDE                 "_gate_is_outside"
/* I jeszcze jeden prop, dla graczy, ktorych trzeba przyblokowac */
#define NO_PASS_THRU_GATE               "_no_pass_thru_gate"

/** *********************************************************
 * Temporary properties
 */
#define TEMP_OBJ_ABOUT_TO_DESTRUCT      "_temp_obj_about_to_destruct"
#define TEMP_SUBLOC_SHOW_ONLY_THINGS    "_temp_subloc_show_only_things"
#define TEMP_STDFOOD_CHECKED            "_temp_stdfood_checked"
#define TEMP_STDDRINK_CHECKED           "_temp_stddrink_checked"
#define TEMP_STDREAD_CHECKED            "_temp_stdread_checked"
#define TEMP_STDTORCH_CHECKED           "_temp_stdtorch_checked"
#define TEMP_STDCORPSE_CHECKED          "_temp_stdcorpse_checked"
#define TEMP_DRAGGED_ENEMIES            "_temp_dragged_enemies"
#define TEMP_LEFTOVER_ORGAN             "_temp_leftover_organ"
#define TEMP_STDROPE_CHECKED            "_temp_stdrope_checked"
#define TEMP_STDHERB_CHECKED            "_temp_stdherb_checked"
#define TEMP_STDPOTION_CHECKED          "_temp_stdpotion_checked"
#define TEMP_LIBCONTAINER_CHECKED       "_temp_libcontainer_checked"
#define TEMP_BACKUP_BRIEF_OPTION        "_temp_backup_brief_option"
#define TEMP_SHOW_ALL_THINGS            "_temp_show_all_things"

/** *********************************************************
 * Wizard properties
 */
#define WIZARD_AM_MAN_SEL_ARR           "_wizard_am_man_sel_arr"
#define WIZARD_S_MAN_SEL_CHAPT          "_wizard_s_man_sel_chapt"
#define WIZARD_AM_SMAN_SEL_ARR          "_wizard_am_sman_sel_arr"
#define WIZARD_S_SMAN_SEL_DIR           "_wizard_s_sman_sel_dir"
#define WIZARD_I_BUSY_LEVEL             "_wizard_i_busy_level"
//przeniesione z /cmd/wiz/apprentice/files.c dla potrzeb tracera (krun)
#define WIZARD_S_LAST_DIR               "_wizard_s_last_dir"
//dodane przez lil. Potrzebne do: ksiazek, scrolli,tablic itp.
#define WIZARD_ONLY                     "_wizard_only"

/* No definitions beyond this line. */
#endif PROP_DEF
