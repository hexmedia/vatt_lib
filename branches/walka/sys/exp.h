/*
 * ======================================================
 * NIE MODYFIKOWA� BEZ KONSULTACJI Z AoB!!!!!!!!!!!!!!!!
 * ======================================================
 *
 *
 *
 *
 *
 * /sys/exp.c
 *
 * plik przechowuje warto�ci expa przyznawane
 * za wszelkie czynno�ci i przy innych okazjach.
 *
 * ostatnie s�owo w nazwie define'a m�wi o tym
 * kt�remu skillowi/statowi przyznajemy expa.
*/

#ifndef EXP_VALUES
#define EXP_VALUES

/*  ============================
 *                do�wiadczenie og�lne
 *   ============================ */ 

/* za ka�dy odebrany punkt zm�czenia dostaje si� tyle.
    UWAGA: koniecznie floaty!!! */
#define EXP_ZA_PUNK_ZMECZENIA_STR       0.3
#define EXP_ZA_PUNK_ZMECZENIA_CON       0.3


/*  ============================
 *                   przy rzucaniu
 *   ============================ */ 

/* exp dla rzucajacacego */
#define EXP_RZUCANIE_PRZY_SPUDLOWANIU_DEX       2
#define EXP_RZUCANIE_PRZY_SPUDLOWANIU_THROWING      2

#define EXP_RZUCANIE_PRZY_UNIKU_PRZECIWNIKA_DEX     2
#define EXP_RZUCANIE_PRZY_UNIKU_PRZECIWNIKA_THROWING        2

#define EXP_RZUCANIE_TRAFILISMY_DEX        7
#define EXP_RZUCANIE_TRAFILISMY_THROWING        8

#define EXP_RZUCANIE_NIEBRONIA_TRAFILISMY_DEX        1
#define EXP_RZUCANIE_NIEBRONIA_TRAFILISMY_THROWING        2

/* exp dla tego w kogo rzucamy*/
#define EXP_RZUCANIE_UNIK_PRZED_POCISKIEM_DEX       9
#define EXP_RZUCANIE_UNIK_PRZED_POCISKIEM_DEFENSE       8
#define EXP_RZUCANIE_UNIK_PRZED_POCISKIEM_AWARENESS     7

#define EXP_RZUCANIE_UNIK_PRZED_NIEBRONIA_DEX       1
#define EXP_RZUCANIE_UNIK_PRZED_NIEBRONIA_DEFENSE       2
#define EXP_RZUCANIE_UNIK_PRZED_NIEBRONIA_AWARENESS     1

#define EXP_RZUCANIE_DOSTALEM_AWARENESS         1


/*  ============================
 *                  przy strzelaniu
 *   ============================ */

/* exp dla strzelaj�cego */
#define EXP_STRZELANIE_KRYT_PUDLO_MISSILE        1
#define EXP_STRZELANIE_PUDLO_MISSILE         2
#define EXP_STRZELANIE_TRAFILEM_MISSILE        10
#define EXP_STRZELANIE_TRAFILEM_KRYT_MISSILE       16


/*  ============================
 *                   w czasie walki
 *   ============================ */ 

/* przy trafieniu, zale�nie od tego czym walczymy */
#define EXP_WALKA_TRAFILISMY_SKILLBRONI        7
#define EXP_WALKA_TRAFILISMY_2H_SKILLBRONI     7
#define EXP_WALKA_TRAFILISMY_WALKAWRECZ        7
#define EXP_WALKA_TRAFILISMY_DIS        9
#define EXP_WALKA_ZABILISMY_DIS         150

/* gdy przeciwnik sparuje nasz cios dostaje du�o r�nych */
#define EXP_WALKA_SPAROWALEM_STR        7
#define EXP_WALKA_SPAROWALEM_DEX        5
#define EXP_WALKA_SPAROWALEM_CON        2       
#define EXP_WALKA_SPAROWALEM_PARRY      10
#define EXP_WALKA_SPAROWALEM_AWARENESS      2

/* gdy przeciwnik uniknie naszego ciosu dostaje du�o r�nych */
#define EXP_WALKA_UNIKNALEM_STR        4
#define EXP_WALKA_UNIKNALEM_DEX        8
#define EXP_WALKA_UNIKNALEM_CON        1       
#define EXP_WALKA_UNIKNALEM_DEFENSE      10
#define EXP_WALKA_UNIKNALEM_AWARENESS      5

/* przy ka�dym ciosie wyprowadzanym przez nas czy przez wroga w ciemnosci */
#define EXP_WALKA_W_CIEMNOSCI_ZA_CIOS_BLINDCMBT         7


/*  ============================
 *            przy u�ywaniu r�nych skilli 
 *   ============================ */ 

/* handel - tyle expa dostajemy do skilla za kazde 10 groszy obrotu */
#define EXP_HANDEL_SPRZEDANIE_ZA_10_GROSZY         1
#define EXP_HANDEL_KUPIENIE_ZA_10_GROSZY         1

/* wspinaczka - za wspinanie si� ;) */
#define EXP_WSPINAM_NIEUDANY_CLIMB				3
#define EXP_WSPINAM_NIEUDANY_CLIMB_STR			3
#define EXP_WSPINAM_NIEUDANY_CLIMB_DEX      	2
#define EXP_WSPINAM_NIEUDANY_CLIMB_CON			1

#define EXP_WSPINAM_PRAWIE_UDANY_CLIMB			7
#define EXP_WSPINAM_PRAWIE_UDANY_CLIMB_STR		7
#define EXP_WSPINAM_PRAWIE_UDANY_CLIMB_DEX      6
#define EXP_WSPINAM_PRAWIE_UDANY_CLIMB_CON		8

#define EXP_WSPINAM_UDANY_CLIMB					4
#define EXP_WSPINAM_UDANY_CLIMB_STR				5
#define EXP_WSPINAM_UDANY_CLIMB_DEX             4
#define EXP_WSPINAM_UDANY_CLIMB_CON				5

/* szukanie */
#define EXP_SZUKANIE_NIEUDANE_AWARENESS                         2
#define EXP_SZUKANIE_UDANE_AWARENESS                            9

/* szukanie zi� */
#define EXP_SZUKANIE_ZIOLA_UDANE_HERBALISM                       4
#define EXP_SZUKANIE_ZIOLA_NIEUDANE_HERBALISM                    1
#define EXP_SZUKANIE_ZIOLA_UDANE_WIS                             2
#define EXP_SZUKANIE_ZIOLA_NIEUDANE_WIS                          1
#define EXP_SZUKANIE_ZIOLA_UDANE_INT                             2
#define EXP_SZUKANIE_ZIOLA_NIEUDANE_INT                          1
#define EXP_SZUKANIE_ZIOL_UDANE_HERBALISM                        3
#define EXP_SZUKANIE_ZIOL_NIEUDANE_HERBALISM                     1 
#define EXP_SZUKANIE_ZIOL_UDANE_WIS                              3
#define EXP_SZUKANIE_ZIOL_NIEUDANE_WIS                           1
#define EXP_SZUKANIE_ZIOL_UDANE_INT                              1
#define EXP_SZUKANIE_ZIOL_NIEUDANE_INT                           1

/* tropienie */
#define EXP_TROPIENIE_NIEUDANY_TRACKING			2
#define EXP_TROPIENIE_NIEUDANY_TRACKING_WIS		2
#define EXP_TROPIENIE_PRAWIE_UDANY_TRACKING		6
#define EXP_TROPIENIE_PRAWIE_UDANY_TRACKING_WIS		5
#define EXP_TROPIENIE_UDANY_TRACKING			12
#define EXP_TROPIENIE_UDANY_TRACKING_WIS		10

/* scinanie drzew - odpowiednio za najmniejsze
 * i najwieksze drzewo 
 */
#define EXP_SCINANIE_DRZEW_STR_MIN			4
#define EXP_SCINANIE_DRZEW_WOODCUTTING_MIN		4
#define EXP_SCINANIE_DRZEW_STR_MAX			18
#define EXP_SCINANIE_DRZEW_WOODCUTTING_MAX		21
#define EXP_SCINANIE_DRZEW_PRZECINANIE_STR		4
#define EXP_SCINANIE_DRZEW_PRZECINANIE_WOODCUTTING  	4

/* w�dkarstwo */
#define EXP_WEDKARSTWO_PRZYWIAZYWANIE_DEX		    1
#define EXP_WEDKARSTWO_PRZYWIAZYWANIE_FISHING		1
#define EXP_WEDKARSTWO_ZLOWIONA_DEX			        5		
#define EXP_WEDKARSTWO_ZLOWIONA_FISHING			    10
#define EXP_WEDKARSTWO_ZERWANA_DEX			        3
#define EXP_WEDKARSTWO_ZERWANA_FISHING	        	5



/* ================
 *     QUESTY
 * ================ */

/* sygnet dla raghola */
#define EXP_QUEST_SYGNET_DLA_RAGHOLA_WOODCUTING    3000

#endif EXP_VALUES
