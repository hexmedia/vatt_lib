/*
 * options.h
 *
 * This file contains all wizard option defines.
 */

#ifndef _options_h
#define _options_h


// z gena
//#define OPT_DEFAULT_STRING " 20 8036\"  !"
#define OPT_DEFAULT_STRING " 20 80       !"
// The lowest bit number for toggles, first three bytes reserved for
// more length.
#define OPT_BASE             48

// Begin with general/mortal options

// More length
#define OPT_MORE_LEN          0
// Screen width
#define OPT_SCREEN_WIDTH      1
// Brief display mode
#define OPT_BRIEF             2
// Echo display mode
#define OPT_ECHO              3
// Whimpy level
#define OPT_WHIMPY            4
// See fights of others
#define OPT_NO_FIGHTS         5
// See fights
#define OPT_BLOOD             5
// Disable unarmed combat
#define OPT_UNARMED_OFF       7
// Gag all messages of misses // z gena
#define OPT_GAG_MISSES        8
// Be merciful in combat      // z gena
#define OPT_MERCIFUL_COMBAT   9
// Auto-wrap long notes       // z gena
#define OPT_AUTOWRAP         10
// MCCP2
#define OPT_MCCP2            11
// MSP
#define OPT_MSP              12
//KODOWANIE, czyli polskie czcionki
//#define OPT_KODOWANIE 13 //NIEUZYWANE, BO JEST FUNKCJA 
                           //czcionka() i ustaw_czcionke()

//Opcja czy w og�le kolorujemy...
#define OPT_COLORS           14
//Je�li tak, to co i jak :)
#define OPT_COLORS_SPEECH    15
#define OPT_COLORS_EXITS     16

#define OPT_RECEIVING        17

#define OPT_HIDE_IN_WHO      18


// Wizard options

// Automatic display of current directory on cwd changes
#define OPT_AUTO_PWD          6

#endif /* _options_h */
