/*
 * wa_types.h
 *
 * This file defines the different types of weapons and armour
 * and thier damage modifers. Any weapon or armour in the game
 * must be of one these types. No other are accepted.
 */

#ifndef WA_TYPES_DEF
#define WA_TYPES_DEF

#include <composite.h>

#define SLOTS_FILE		"/sys/global/slots"

#define SLOT_NAME(slot, przyp)		\
		SLOTS_FILE->slot_name(slot, przyp)

// Zwraca slowny opis slotow, danych w postaci inta
#define COMPOSITE_SLOTS(slots, przyp)	\
		SLOTS_FILE->composite_slots(slots, przyp)

/*
 * GUIDANCE FOR WEAPONS and GUIDANCE FOR ARMOURS
 *
 * Can be found under /doc/man/general/weapon and /doc/man/general/armour
 *
 */

/*
 * Sublocations for combat tools
 */
#define SUBLOC_WIELD  "wielded"
#define SUBLOC_WORNA  "worn_a"

/* Damage types */

#define W_IMPALE    1
#define W_SLASH     2
#define W_BLUDGEON  4

//Ilosc DT
#define W_NO_DT     3

#define DT_PHYSICAL ({W_IMPALE, W_SLASH, W_BLUDGEON})
/*
 * The magic damage type is used to indicate that no ac will prevent
 * this type of attack.
 */
#define AIR_DT      8
#define EARTH_DT    16
#define FIRE_DT     32
#define WATER_DT    64
#define LIFE_DT     128
#define DEATH_DT    256

#define MAGIC_DT    512
#define W_NO_M_DT     6
#define DT_MAGICAL ({AIR_DT, EARTH_DT, FIRE_DT, WATER_DT, LIFE_DT, DEATH_DT, MAGIC_DT})

#define MAX_DAM     256


#define W_HAND_HIT  10
#define W_HAND_PEN  5
#define A_NAKED_MOD ({ 0, 0, 0 })

/*
 * Tool slots for humanoids.
 *
 * The armourtype is a bitwise combination of these slots. Some of the
 * slots correspond to hitlocations.
 *
 * There is also a magic flag that can be set in the armourtype, indicating
 * that the armour is magically enhanced.
 */
#define WA_BIT(x)		(1 << x)
				
#define TS_HEAD		WA_BIT(0)
#define TS_NECK		WA_BIT(1)
#define TS_R_SHOULDER	WA_BIT(2)
#define TS_L_SHOULDER	WA_BIT(3)
#define TS_CHEST		WA_BIT(4)
#define TS_STOMACH		WA_BIT(5)
#define TS_CLOAK		WA_BIT(6)
#define TS_R_ARM		WA_BIT(7)
#define TS_L_ARM		WA_BIT(8)
#define TS_R_FOREARM		WA_BIT(9)
#define TS_L_FOREARM		WA_BIT(10)
#define TS_R_HAND		WA_BIT(11)
#define TS_L_HAND		WA_BIT(12)
#define TS_HIPS		WA_BIT(13)
#define TS_R_THIGH		WA_BIT(14)
#define TS_L_THIGH		WA_BIT(15)
#define TS_R_SHIN		WA_BIT(16)
#define TS_L_SHIN		WA_BIT(17)
#define TS_R_FOOT		WA_BIT(18)
#define TS_L_FOOT		WA_BIT(19)

#define TS_R_FINGER		WA_BIT(20)
#define TS_L_FINGER		WA_BIT(21)

#define TS_WAIST		WA_BIT(22)

#define MAX_TS		WA_BIT(22)

#define TS_R_WEAPON		TS_R_HAND
#define TS_L_WEAPON		TS_L_HAND

#define TS_FINGERS 		(TS_L_FINGER | TS_R_FINGER)
#define TS_SHOULDERS	     	(TS_L_SHOULDER | TS_R_SHOULDER)
#define TS_FOREARMS		(TS_L_FOREARM | TS_R_FOREARM)
#define TS_ARMS		(TS_L_ARM | TS_R_ARM)
#define TS_THIGHS		(TS_L_THIGH | TS_R_THIGH)
#define TS_SHINS		(TS_L_SHIN | TS_R_SHIN)
#define TS_FEET		(TS_L_FOOT | TS_R_FOOT)
#define TS_HANDS		(TS_L_HAND | TS_R_HAND)
#define TS_L_LEG		(TS_L_SHIN | TS_L_THIGH)
#define TS_R_LEG		(TS_R_SHIN | TS_R_THIGH)
#define TS_LEGS		(TS_L_LEG | TS_R_LEG)
#define TS_TORSO		(TS_CHEST | TS_STOMACH | TS_WAIST)
#define TS_ROBE 		(TS_CHEST | TS_STOMACH | \
                     	TS_SHOULDERS | TS_FOREARMS | \
       			TS_ARMS | TS_THIGHS | \
                     	TS_SHINS)

#define TS_HBODY    		(TS_CHEST | TS_STOMACH | TS_HEAD | \
                     	TS_SHOULDERS | TS_FOREARMS | \
		     		TS_ARMS | TS_THIGHS | \
                     	TS_SHINS | TS_FEET | TS_HANDS)

/* Weapon hand, only applicable to humanoids
 *
 * Some of these are used as attack id's
 */
#define W_RIGHT     TS_R_WEAPON
#define W_LEFT      TS_L_WEAPON
#define W_BOTH      (W_RIGHT | W_LEFT)
#define W_FOOTR     TS_R_FOOT
#define W_FOOTL     TS_L_FOOT

#define W_ANYH      0               /* These mark that any hand is possible */
#define W_NONE      0

#define W_NO_WH     6

/*
 * Hitlocations for humanoids
 *
 */
#define A_NECK		TS_NECK
#define A_CHEST		TS_CHEST
#define A_STOMACH		(TS_STOMACH | TS_WAIST)
#define A_BODY		TS_TORSO
#define A_TORSO		TS_TORSO
#define A_HEAD		TS_HEAD
#define A_HIPS		TS_HIPS

#define A_R_THIGH		TS_R_THIGH
#define A_L_THIGH		TS_L_THIGH
#define A_THIGHS		TS_THIGHS
#define A_ANY_THIGH		(-TS_R_THIGH) // przejrzysciej to ma byc !!!

#define A_R_SHIN		TS_R_SHIN
#define A_L_SHIN		TS_L_SHIN
#define A_SHINS		TS_SHINS
#define A_ANY_SHIN		(-TS_R_SHIN) // !!!

#define A_R_LEG		TS_R_LEG
#define A_L_LEG		TS_L_LEG
#define A_LEGS		TS_LEGS
#define A_ANY_LEG		(-A_R_LEG) // !!!

#define A_R_SHOULDER		TS_R_SHOULDER
#define A_L_SHOULDER		TS_L_SHOULDER
#define A_SHOULDERS		TS_SHOULDERS
#define A_ANY_SHOULDER	(-TS_R_SHOULDER) // !!!

#define A_R_FOREARM		TS_R_FOREARM
#define A_L_FOREARM		TS_L_FOREARM
#define A_FOREARMS		TS_FOREARMS
#define A_ANY_FOREARM	(-TS_R_FOREARM)

#define A_R_ARM		TS_R_ARM
#define A_L_ARM		TS_L_ARM
#define A_ARMS		TS_ARMS
#define A_ANY_ARM		(-TS_R_ARM)

#define A_R_HAND		TS_R_HAND
#define A_L_HAND		TS_L_HAND
#define A_HANDS		TS_HANDS
#define A_ANY_HAND		(-TS_R_HAND)

/*nowe, test. lil*/
#define A_R_FINGER		TS_R_FINGER
#define A_L_FINGER		TS_L_FINGER
#define A_FINGERS		TS_FINGERS
#define A_ANY_FINGER		(-TS_R_FINGER)

/* molder  - w ubraniach sa bledy z niezdefiniowanego A_WAIST,
 * dodalem TS_WAIST do A_STOMACH */
#define A_WAIST		TS_WAIST

/* by� mo�e trzeba rozdzieli� tu��w na prz�d i ty� */
#define A_BACK      TS_TORSO


#define A_R_FOOT		TS_R_FOOT
#define A_L_FOOT		TS_L_FOOT
#define A_FEET		TS_FEET
#define A_ANY_FOOT		(-TS_R_FOOT)

#define A_ROBE		TS_ROBE
#define A_CLOAK		TS_CLOAK

#define A_SHIELD		-(TS_R_WEAPON | TS_R_FOREARM) // !!!
#define A_BUCKLER		-(TS_R_FOREARM) // !!!



#define A_SIDE_SLOTS_MAP    ([  A_R_SHOULDER : A_L_SHOULDER,\
                            A_L_SHOULDER : A_R_SHOULDER,\
			    A_R_HAND : A_L_HAND,\
                            A_L_HAND : A_R_HAND,\
                            A_R_FOOT : A_L_FOOT,\
                            A_L_FOOT : A_R_FOOT,\
                            A_R_FOREARM : A_L_FOREARM,\
                            A_L_FOREARM : A_R_FOREARM,\
                            A_R_THIGH : A_L_THIGH,\
                            A_L_THIGH : A_R_THIGH,\
                            A_R_SHIN : A_L_SHIN,\
                            A_L_SHIN : A_R_SHIN,\
                            A_R_LEG : A_L_LEG,\
                            A_L_LEG : A_R_LEG,\
                            A_R_ARM : A_L_ARM,\
                            A_L_ARM : A_R_ARM ])
#define A_SIDE_SLOTS_ARR    ({ A_R_SHOULDER, A_L_SHOULDER, A_R_HAND, A_L_HAND, A_R_FOOT, A_L_FOOT, A_R_FOREARM,\
                            A_L_FOREARM, A_R_THIGH, A_L_THIGH, A_R_SHIN, A_L_SHIN, A_R_LEG, A_L_LEG,\
                            A_R_ARM, A_L_ARM })

#define A_ALL_SLOTS ({ A_HEAD, A_NECK, A_ROBE, A_TORSO, A_ARMS, A_L_ARM, A_R_ARM, A_SHOULDERS,\
        A_L_SHOULDER, A_R_SHOULDER, A_FOREARMS, A_L_FOREARM, A_R_FOREARM, A_HANDS, A_L_HAND,\
	A_R_HAND, A_HIPS, A_LEGS, A_R_LEG, A_L_LEG, A_THIGHS, A_L_THIGH, A_R_THIGH, A_SHINS,\
        A_L_SHIN, A_R_SHIN, A_FEET, A_L_FOOT, A_R_FOOT })

#define ALL_SLOTS ({ A_HEAD, A_NECK, A_ROBE, A_TORSO, A_L_SHOULDER, A_R_SHOULDER, A_L_FOREARM,\
        A_R_FOREARM, A_L_HAND, A_R_HAND, A_HIPS, A_L_THIGH, A_R_THIGH,\
        A_L_SHIN, A_R_SHIN, A_L_FOOT, A_R_FOOT })

#define TUNIKA_SLOTS ({ A_TORSO, A_L_SHOULDER, A_R_SHOULDER, A_L_FOREARM,\
        A_R_FOREARM, A_HIPS, A_L_THIGH, A_R_THIGH })

#define SHIELD_SLOTS ({ A_R_FOREARM, A_L_FOREARM })

#define ROBE_SLOTS ({ A_ROBE, A_NECK, A_TORSO, A_L_SHOULDER, A_R_SHOULDER, A_L_FOREARM, A_R_FOREARM,\
        A_HIPS, A_R_THIGH, A_L_THIGH })

#define STR_ALSO ({ "Opr�cz tego", "Dodatkowo", "Ponadto" })


/*
 * Magic flag, this is used in armourtypes in combination with tool slots.
 * Note that this is not a slot, any number of magic armours can be used.
 *
 * Magical combat tools can be used without allocating a tool slot.
 * see (/std/combat/ctool.c)
 */
#define A_MAGIC     -2

#define A_NO_T      16

/* Normal AC (3) + Magic AC (6) */
#define A_UARM_AC   ({1, 1, 1, 0, 0, 0, 0, 0, 0})

#define A_MAX_AC    ({ 100, 100, 100, 100, 100, 100, 100, 100, \
                       100, 100, 100, 100, 100, 100, 100, 100 })

#define SH_PUKLERZ		0
#define SH_LEKKA		1
#define SH_TARCZA		2
#define SH_RYCERSKA		3
#define SH_DLUGA_RYCERSKA 	4
#define SH_PELNA_PIECHOTY 	5

#include "/config/sys/wa_types2.h"

#define POC_KUSZA               1
#define POC_LUK                 2
#define POC_PROCA               3

#endif
