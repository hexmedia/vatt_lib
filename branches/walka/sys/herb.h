// Definicje do ziolek:)
// (c) Krun 2007

// Definicje sposobow przygotowania ziolka..
#define HERBS_PRZYG_GOTOWANIE                     1
#define HERBS_PRZYG_SUSZENIE                      2
#define HERBS_PRZYG_ZGNIATANIE                    4
#define HERBS_PRZYG_MIELENIE                      8
    
// Definicje sposobow uzywania przygotowanych mikstur
#define HERBS_PRZYG_CMD_JEDZENIE                      1
#define HERBS_PRZYG_CMD_PICIE                         2
#define HERBS_PRZYG_CMD_PRZYKLADANIE                  4
#define HERBS_PRZYG_CMD_WACHANIE                      8
#define HERBS_PRZYG_CMD_SMAROWANIE                    16

// Definicje reakcji ziola na czynnosc gracza(i wszyscy wszystko wiedza:P)
#define HERBS_EFF_ODTRUWANIE                1
#define HERBS_EFF_LECZENIE                  2
#define HERBS_EFF_REGENERACJA               4
#define HERBS_EFF_HALUCYNACJE               8
#define HERBS_EFF_ZWIEKSZENIE_SILY          16
#define HERBS_EFF_ZWIEKSZENIE_ZRECZNOSCI    32
#define HERBS_EFF_ZWIEKSZENIE_WYTRZYMALOSCI 64
#define HERBS_EFF_ZWIEKSZENIE_MADROSCI      126
#define HERBS_EFF_ZWIEKSZENIE_INTELIGENCJI  256
#define HERBS_EFF_ZWIEKSZENIE_ODWAGI        512
#define HERBS_EFF_ZWIEKSZENIE_MENTALI       HERBS_EFF_ZWIEKSZENIE_INTELIGENCJI|HERBS_EFF_ZWIEKSZENIE_MADROSCI|HERBS_EFF_ZWIEKSZENIE_ODWAGI
#define HERBS_EFF_ZWIEKSZENIE_FIZYCZNYCH    HERBS_EFF_ZWIEKSZENIE_SILY|HERBS_EFF_ZWIEKSZENIE_WYTRZYMALOSCI|HERBS_EFF_ZWIEKSZENIE_ZRECZNOSCI

// Komendy do uzycia na ziolach przerobionych
#define HERBS_CMD_JEDZENIE          (["zjedz" : ({ "zjadasz", "zjada", "zje^s^c"})])
#define HERBS_CMD_PICIE             (["wypij" : ({ "wypijasz", "wypija", "wypi^c"}), ({"pij", "wypijasz", "wypija", "wypi^c"})])
#define HERBS_CMD_PRZYKLADANIE      (["przy^lo^z" : ({"przyk^ladasz", "przyk^lada", "przy^lo^zy^c"})])
#define HERBS_CMD_WACHACNIE         (["powa^chaj" : ({"w^achasz", "w^acha", "w^acha^c"})])
#define HERBS_CMD_SMAROWANIE        (["posmaruj" : ({"smarujesz", "smaruje", "smarowa^c"})])
#define HERBS_CMD_NACIERANIE        (["natrzyj" : ({"natcierasz", "natrze^c", "naciera^c"})])

// Sposoby wyst�powania zio�a(czyli po ile zi� b�dzie na lokacji)
#define HERBS_WYST_POJEDYNCZO   1
#define HERBS_WYST_BMALE_GRUPY  3
#define HERBS_WYST_MALE_GRUPY   6
#define HERBS_WYST_GRUPY        9
#define HERBS_WYST_DUZE_GRUPY   13
#define HERBS_WYST_BDUZE_GRUPY  15
