inherit "/std/workroom";

create_workroom()
{
    set_long("Znajdujesz sie w pomieszczeniu o, lekko mowiac, spartanskim "+
        "wystroju. Tak naprawde nie znajduje sie tutaj nic, poza "+
        "wejsciem do kompleksu pomieszczen czarodziei Vatt'gherna.\n");
        
    set_short("Pracownia Czarodzieja");

    add_exit("/d/Standard/wiz/wizroom", ({ "wejscie", "poprzez wejscie" }), 
        0, 0);
}
