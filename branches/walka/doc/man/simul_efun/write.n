NAZWA
	write - write data (normally text) to the current player

SKLADNIA
	void write(mixed data)

OPIS
	Writes data to the current 'command giver', ie the object returned by
	this_player. Data is normally text.	

NOTA BENE
	write as well as tell_object sends unprocessed messages to the
	recieving object. If the mudlib uses processing on messages
	then calling this function will send a raw message.

ZOBACZ TEZ
	tell_object, this_player, set_this_player, this_interactive
