Value By Function Call (VBFC)
=============================

Wprowadzenie
============

Warto�� zmiennej jest z natury statyczna, najpierw ustawiasz zmienn�
a potem u�ywasz tej zmiennej np. w funkcji, itp. Cz�sto zdarza si�,
�e musisz nada� zmiennej warto��, mimo, �e nie wiesz, jaka konkretnie
ta warto�� musi by�, by� mo�e warto�� powinna zale�e� od pewnych
innych rzeczy. G��wna r�nica pomi�dzy bezpo�rednim nadaniem
zmiennej warto�ci, a ustawieniem warto�ci zawieraj�cej VBFC
(np. za pomoc� wywo�ania jakiej� funkcji set_xxx) jest taka, �e
normalnie warto�� zostaje okre�lona w momencie ustawienia zmiennej,
natomiast przy VBFC warto�� jest okre�lana dynamicznie, przy sprawdzaniu
warto�ci zmiennej (np. przy u�yciu jakiej� funkcji query_xxx).

Skupmy si� na dw�ch przyk�adach. Oba b�da dokl�adniej opisane poni�ej.
Oczekuj�, �e przeczytasz oba, bo zawieraj� informacje, kt�rych nie
znajdziesz w innych cz�ciach tego opracowania.

Pierwszy przyk�ad to wypisywanie jakiego� tekstu. Kiedy gracz co� m�wi,
to mimo �e wiadomo�� jest dla ka�dego taka sama, nie ka�dy otrzymuje
j� w takiej samej postaci (cho�by dlatego, �e kto� mo�e zna� imi�
mowi�cego, a kto inny - nie). Kiedy co� m�wi�, jeden gracz mo�e dosta�
komunikat "Mercade m�wi: Cze��!", a inny, kt�ry mnie nie zna, ujrzy
"Przyjacielski opanowany krasnolud m�wi: Cze��!".

Drugi przyk�ad to lustro. Musisz wywo�a� set_long w funkcji create_object,
aby ustawi� d�ugi opis jakiego� przedmiotu. Jednak w wypadku lustra -
kiedy kto� je ogl�da - powinien ujrze� opis samego lustra, oraz swoje
w�asne odbicie. To drugi przyk�ad u�ycia VBFC - ustawiamy opis czego�,
ale konkretny opis jest ustalany dopiero, kiedy zajdzie taka potrzeba
(nie wczesniej).

/Mercade


Sk�adnia
========

VBFC jest zawsze �a�cuchem znak�w i ma posta�:

"@@funkcja[:nazwa_pliku][|argument1[|argument2[|argument...]]]@@"

Jak widzisz, nazwa pliku i argumenty s� opcjonalne. Je�li nie podasz
nazwy pliku, domy�lnie zostanie u�yty this_object(). Zwr�� uwag�, �e
argumenty do funkcji wywo�ywanej za pomoc� VBFC mog� mie� tylko posta�
�a�cucha znak�w (string), wszystkie warto�ci typu int czy inne s�
automatycznie zamieniane na warto�� typu string.

Koncepcja VBFC zak�ada, �e mo�e by� wywo�ana praktycznie ka�da funkcja
w dowolnym obiekcie. Teraz ju� wiesz, czemu nie mo�esz np. powiedzie� @@.
Gdyby� spr�bowa�{/a/o} to zrobi�, zobaczysz, �e zostanie to zast�pione
przez pojedynczy znak @. Jest to zrobione celowo, aby uniemo�liwi�
graczowi u�ywania tego, jakby nie by�o, pot�nego narz�dzia.

Warto�� �a�cucha zawieraj�cego VBFC b�dzie wyliczona nast�puj�co:

call_other(nazwa_pliku, funkcja, argument1, argument2, ...)

Oznacza to, �e VBFC nie mo�e by� u�yte aby wywo�a� funkcj�
zadeklarowan� jako 'private' lub 'static' (zobacz "man access_classes").


Wyliczanie VBFC
===============

VBFC nie jest automatycznie u�ywane wsz�dzie. Istniej� specjalne funkcje,
kt�re musisz wywo�a�, �eby wyliczy� warto�� wyra�enia VBFC.
Takie wyliczenie jest ju� dokonywane np. w nast�puj�cych funkcjach:

- query_prop(), mo�esz zrobi� add_prop(prop, "@@funkcja@@") - i gdy
  sprawdzana jest warto�� propa, wyliczane jest VBFC
- long() i short(), mo�esz ustawi� set_long("@@funkcja@@"). Zobacz
  przyk�ad poni�ej

// Poni�sze 2 linie by�y w wersji angielskiej,
// ale to si� raczej mija z prawd�

//- write()
//- catch_msg(), tylko, je�li argument jest typu 'string' lub 'string *'

- say() i tell_room()
- add_exit(), w pierwszym, trzecim i czwartym argumencie. Dla przyk�adu
  portal mo�e wysy�a� gracza w jedno z kilku losowych miejsc w
  nast�puj�cy spos�b:
    add_exit("@@dokad@@", "portal", 0, 5);

Maj�c jaki� obiekt i chc�c samodzielnie obs�ugiwa� VBCF, powin{iene�/na�/no�}
u�y� funkcji

mixed check_call(string argument_do_wyliczenia, object wywolujacy)

aby wyliczy� t� warto��. Pierwszy argument to string, kt�ry zawiera
potencjaln� VBFC. Je�li nie zawiera VBFC, zostanie zwr�cony w niezmienionej
postaci. Drugi parametr jest opcjonalny i zawiera obiekt, kt�ry chce pozna�
warto�� wyliczonego VBFC (bo w wielu wypadkach VBFC jest u�ywane, aby
uzyska� r�ne warto�ci dla r�nych graczy przy u�yciu jednej komendy).
Patrz przyk�ad poni�ej.

Uwagi/Sugestie zwi�zane z VBFC
==============================

Por�wnuj�c VBFC ze zwyk�ym (statycznym) kodem, mo�na doj�� do wniosku,
�e VBFC jest wolniejsze ni� statyczny kod lub zwyczajne przypisanie
warto�ci do zmiennej. Tak wi�c nie powin{iene�/na�/no�} u�ywa� VBFC,
je�li nie jest to konieczne. Zawsze powin{iene�/na�/no�} rozwa�y� najpierw
u�ycie zwyk�ego kodu, a dopiero je�li za jego pomoc� nie da si� uzyska�
po�adanych wynik�w, mo�esz u�y� VBFC.

Pierwszy przyk�ad poni�ej pokazuje, �e makra typu QCIMIE() i podobne
s� zrealizowane przy u�yciu VBFC.

cel->catch_msg(QCIMIE(kto, PL_MIA) + " trafia ci�.");

w tym konkretnym wypadku mo�e by� zast�pione przez szybsze wywo�anie

cel->catch_msg(kto->query_Imie(cel) + " trafia ci�.");

co daje identyczny wynik, ale jest szybsze, bo nie zawiera VBFC.
Pomimo, �e QCIMIE jest by� mo�e �atwiejsze i wygodniejsze w u�yciu,
nie powin{iene�/na�/no�} u�ywa� go w tym wypadku, bo szybko�� wykonania
nie jest czym� co mo�na zaniedba�.



Przyk�ad 1: mowa
================

Do mowy, i og�lniej: do wszystkich dzia�a� wykonywanych przez gracza
stosuje si� system znanych/nieznanych (met/nonmet). We�my przyk�adowe
zdanie z wprowadzenia powy�ej. Ka�dy gracz na lokacji dostaje wiadomo��
"<kto> m�wi: Cze��!", ale <kto> mo�e si� r�ni� w zale�no�ci od
odbiorcy.

Oczywi�cie mo�emy wzi�� wszystkich graczy na lokacji i sprawdza�
ka�dego z nich z osobna, jednak oznacza to du�o wolnego i nieefektywnego
kodu. Szybsz� i bardziej efektywn� rzecz� jest wys�anie tego samego
komunikatu do ka�dego z graczy, �eby obiekt gracza sam ustali� czy
np. nas zna czy te� nie.

I znowu, nie chcemy wyznacza� warto�ci zmiennej od razu, lecz uzyska�
konkretn� warto�� p�niej. I dlatego u�ywamy VBFC. W tym przyk�adzie,
gdy gracz wpisuje komend� "powiedz Cze��!", przy wydaniu komendy zostaje
wywo�ana funkcja communicate(), a zmienna text przyjmuje warto�� "Cze��!".

int
communicate(string text)
{
    say(QCIMIE(this_player(), PL_MIA) + " m�wi: " + text);
    write("Ok.");
    return 1;
}

Gwoli �cis�o�ci cz�� "m�wi" te� jest wyliczana przy u�yciu VBFC,
przecie� ka�da rasa inaczej s�yszy d�wi�ki wydawane przez inn� ras�.

Makro QCIMIE(this_player(), PL_MIA) zostaje zast�pione przez nast�puj�cy
�a�cuch znak�w przed kompilacj�, i mo�emy w nim �atwo wyr�nic VBFC:

"@@query_Imie:" + file_name(this_player()) + "|" + PL_MIA + "@@"

W konsekwencji say() wysy�a do graczy komunikat postaci:

"@@query_Imie:/std/dwarf#1234|0@@ m�wi: Cze��!"

kt�ry jest przez system rozpoznaj�cy VBFC interpretowany nast�puj�co:

call_other("/std/dwarf#1234", "query_Imie", 0) + " m�wi: Cze��!"

Poniewa� query_Imie() dla r�nych os�b przy wywo�aniu zwraca r�ne
wyniki, wi�c ka�dy z graczy dostanie indywidualny opis m�wi�cego,
w zale�no�ci od tego, czy go zna, czy nie zna.


Przyk�ad 2: lustro
==================

Je�li robisz lustro, to zapewne chcia�{/a/o}by�, �eby ka�dy gracz m�g�
w nim ujrze� swoje w�asne odbicie. Poni�ej umieszczony jest kr�tki
przyk�ad takiego lustra. Bardziej rozbudowana wersja (angielska) znajduje
si� w /doc/examples/obj/mirror.c i warto si� z ni� r�wnie� zapozna� ;-)

W poni�szym przyk�adzie VBFC u�yte jest w set_long, wi�c za ka�dym
razem, gdy jest potrzebny d�ugi opis lustra (tj. kto� je ogl�da),
wywo�ywana jest funkcja mirror_description() i zwraca opis tego, co
w danej chwili wida� w lustrze.

inherit "/std/object";

#include <language.h>
#include <pl.h>

/*
 * Ta funkcja tworzy nasze lustro.
 */
void
create_object()
{
    ustaw_nazwe("lustro");
    dodaj_przym("ma�y", "mali");
/*
 * W tym miejscu u�ywamy VBFC aby ustawi� d�ugi opis. Za ka�dym razem,
 * gdy kto� ogl�da lustro, wywo�uje si� funkcja mirror_description()
 * i zwraca opis adekwatny do sytuacji (czyli zawieraj�cy opis gracza,
 * kt�ry, przegl�da si� w lustrze).
 */
    set_long("@@mirror_description@@");
}

/*
 * Ta funkcja zwr�ci opis gracza (niepoznanego), kt�ry przegl�da
 * si� w lustrze.
 */
string
mirror_description()
{
    return break_string("Spogl�dasz na lustro i dostrzegasz w nim siebie, " +
	this_player()->query_nonmet_name(PL_BIE) + ".", 75) + "\n";
}

/*
 * Koniec przyk�adu 2 ;)
 */
