
                       *----------------------*
                        PORADNIK PISANIA UBRA�
                       *----------------------*

W zwi�zku z pojawiaj�cymi si� w�tpliwo�ciami dotycz�cymi systemu ubra�
na Vatt'ghernie, nadesz�a pora na przedstawienie kompleksowego tutoriala
dotycz�cego tej kwestii. Poniewa� u nas idea chowaj�ca si� za
implementacj� jest nieco bardziej skomplikowana ni� na innych mudach,
dlatego tak trudno zacz�� si� w tym wszystkim orientowa�. Mam nadziej�,
�e po przeczytaniu tego oto poradnika b�dziesz w stanie samodzielnie
zakodowa� ubranie dok�adnie takie, jakie ma by�. Je�eli jednak
jakiekolwiek w�tpliwo�ci pozostan� - nie wahaj si� s�a� list�w na mudzie
do Jeremiana.

=======
I. Idea
=======

Dzisiaj standardem na mudach ju� jest, �e ubrania mo�na zak�ada� na
siebie w wielu warstwach. U nas jednak poszli�my o krok dalej i
odeszli�my od sztywnego podzia�u na odg�rnie ustalone warstwy. Je�eli
kto� lubi zak�ada� na kolczug� lu�ne majtasy w celu zniszczenia
psychicznego wroga - jego sprawa. Zdecydowanie ogranicze� dla graczy
powinno by� jak najmniej.

Tak wi�c idea kryj�ca si� za systemem ubra� jest nast�puj�ca:
 * Zdefiniowanych jest na tyle du�o rozmiar�w opisuj�cych posta�, by w
   miar� dok�adnie m�c zdecydowa�, czy dan� rzecz da si� za�o�y�, czy
   nie.
 * Ka�da rasa/p�e� ma ustalone swoje unikalne kroje ubra�. Poszczeg�lne
   cz�ci garderoby uszyte s� dla konkretnej rasy i p�ci.
 * Na rozmiary postaci wp�yw ma opr�cz rasy i p�ci tak�e wzrost i waga,
   oraz pewien (niewielki) czynnik losowy.
 * Ubrania maj� mo�liwo�� ustalenia dok�adnych wymiar�w, na jakie
   pasuj�.
 * Mo�na za�o�y� ubranie tylko pod warunkiem, �e wszystkie rozmiary
   postaci mieszcz� si� w przedzia�ach przez nie akceptowanych.
 * Dla os�b trzecich widoczne s� tylko ubrania niezas�oni�te w pe�ni
   przez inne noszone rzeczy.

====================
II. Rozmiary postaci
====================

Jednak na jakiej� podstawie trzeba umie� zdecydowa�, czy pi�ta kolczuga
jeszcze wejdzie na gracza, czy ju� przypadkiem si� nie zmie�ci. Dlatego
zosta�y wprowadzone wymiary postaci. Oto tabelka przedstawiaj�ca
wszystkie zdefiniowane wymiary opisuj�ce livinga:

 ----------------------------------------------------------------------
   nr  |  sta�a                        |  opis
 ----------------------------------------------------------------------
   0   |  ROZ_OB_GLOWY                 |  obw�d g�owy
   1   |  ROZ_OB_SZYI                  |  obw�d szyi
   2   |  ROZ_OB_OKG                   |  obw�d obr�czy ko�czyny g�rnej
       |                               |
   3   |  ROZ_DL_RAMIENIA              |  d�ugo�� ramienia
   4   |  ROZ_OB_RAMIENIA              |  obw�d ramienia
   5   |  ROZ_DL_PRZEDRAMIENIA         |  d�ugo�� przedramienia
   6   |  ROZ_OB_PRZEDRAMIENIA         |  obw�d przedramienia
   7   |  ROZ_OB_NADGARSTKA            |  obw�d nadgarstka
   8   |  ROZ_DL_DLONI                 |  d�ugo�� d�oni
   9   |  ROZ_SZ_DLONI                 |  szeroko�� d�oni
       |                               |
  10   |  ROZ_DL_TULOWIA               |  d�ugo�� tu�owia
  11   |  ROZ_OB_W_KLATCE              |  obw�d w klatce piersiowej
  12   |  ROZ_OB_W_PASIE               |  obw�d w pasie
  13   |  ROZ_OB_W_BIODRACH            |  obw�d w biodrach
       |                               |
  14   |  ROZ_DL_UDA                   |  d�ugo�� uda
  15   |  ROZ_OB_UDA                   |  obw�d uda
  16   |  ROZ_DL_GOLENI                |  d�ugo�� goleni
  17   |  ROZ_OB_GOLENI                |  obw�d goleni
  18   |  ROZ_OB_KOSTKI                |  obw�d kostki
  19   |  ROZ_DL_STOPY                 |  d�ugo�� stopy
  20   |  ROZ_SZ_STOPY                 |  szeroko�� stopy
 ----------------------------------------------------------------------

Do pobierania rozmiar�w postaci s�u�� nast�puj�ce funkcje:

 * float* pobierz_rozmiary() - funkcja zwraca tablic� (z polami w takiej
   kolejno�ci jak w powy�szej tabeli) rozmiar�w danego livinga. Rozmiary
   s� wyliczane tylko przy pierwszym u�yciu tej funkcji. Nast�pne jej
   wywo�ania zwracaj� ju� zapami�tane wcze�niej wyniki.
   
 * void wylicz_rozmiary() - funkcja wylicza rozmiary livinga na
   podstawie jego rasy, p�ci, wagi i wzrostu. Mo�na wywo�ywa� t� funkcj�
   wielokrotnie, co spowoduje nadpisanie wcze�niej wyliczonych rozmiar�w.

 * float* oblicz_rozmiary_osoby() - funkcja zwraca tablic� (z polami
   w takiej kolejno�ci jak w powy�szej tabeli) aktualnych rozmiar�w danego
   livinga (czyli uwzgl�dniaj�c rzeczy, jakie ma na siebie za�o�one). Gdy
   kt�ry� z rozmiar�w wynosi -1.0, oznacza to, �e dane miejsce jest
   ca�kowicie zaj�te przez jaki� przedmiot i ju� nic nie mo�na tam zmie�ci�.

======================
III. W�a�ciwo�ci ubra�
======================

Je�eli nie zadamy sobie trudu ustalenia odpowiednich w�a�ciwo�ci
ubrania, to jego rozmiary zostan� wyliczone w spos�b automatyczny. W
takim przypadku mo�emy otrzyma� akurat to, co chcieli�my, jednak cz�sto
istnieje potrzeba r�cznego 'dopieszczenia' obiektu, by w pe�ni spe�nia�
nasze oczekiwania.

Na szcz�cie nasze mo�liwo�ci w tej materii s� ca�kiem spore. Oto lista
w�a�ciwo�ci ubra�, kt�re maj� wp�yw na p�niejszy komfort (lub w og�le
mo�liwo��) ich noszenia:

 ----------------------------------------------------------------------
  sta�a                    | opis
 ----------------------------------------------------------------------
  ARMOUR_S_DLA_RASY        | dla jakiej rasy dana rzecz jest zrobiona
  ARMOUR_I_DLA_PLCI        | dla jakiej p�ci dana rzecz jest zrobiona
                           |
  ARMOUR_F_PRZELICZNIK     | ile razy obj�to�� obiektu zak�adaj�cego
                           | ubranie jest wi�ksza od obj�to�ci nieza�o�onego
                           | ubrania
                           |
  ARMOUR_I_D_ROZCIAGLIWOSC | o ile procent mo�e by� zmniejszona dolna
                           | granica pasowania do rozmiaru (opr�cz
                           | standardowych 20%)
  ARMOUR_I_U_ROZCIAGLIWOSC | o ile procent mo�e by� zwi�kszona g�rna
                           | granica pasowania do rozmiaru (opr�cz
                           | standardowych 30%)
                           |
  ARMOUR_F_POWIEKSZENIE    | NIEU�YWANE
                           |
  ARMOUR_F_POW_GLOWA       | jak bardzo ubranie powi�ksza obw�d g�owy
  ARMOUR_F_POW_SZYJA       | jak bardzo ubranie powi�ksza obw�d szyi
  ARMOUR_F_POW_BARKI       | jak bardzo ubranie powi�ksza obw�d obr�czy
                           | ko�czyny g�rnej
  ARMOUR_F_POW_DL_RAMIE    | NIEU�YWANE
  ARMOUR_F_POW_OB_RAMIE    | jak bardzo ubranie powi�ksza obw�d ramienia
  ARMOUR_F_POW_DL_PRAMIE   | NIEU�YWANE
  ARMOUR_F_POW_OB_PRAMIE   | jak bardzo ubranie powi�ksza obw�d
                           | przedramienia
  ARMOUR_F_POW_NADGARSTEK  | NIEU�YWANE
  ARMOUR_F_POW_DL_DLON     | jak bardzo ubranie powi�ksza d�ugo�� d�oni
  ARMOUR_F_POW_SZ_DLON     | jak bardzo ubranie powi�ksza szeroko��
                           | d�oni
  ARMOUR_F_POW_TULOW       | NIEU�YWANE
  ARMOUR_F_POW_PIERSI      | jak bardzo ubranie powi�ksza obw�d w klatce
                           | piersiowej
  ARMOUR_F_POW_PAS         | jak bardzo ubranie powi�ksza obw�d w pasie
  ARMOUR_F_POW_BIODRA      | jak bardzo ubranie powi�ksza obw�d w
                           | biodrach
  ARMOUR_F_POW_DL_UDO      | NIEU�YWANE
  ARMOUR_F_POW_OB_UDO      | jak bardzo ubranie powi�ksza obw�d uda
  ARMOUR_F_POW_DL_GOLEN    | NIEU�YWANE
  ARMOUR_F_POW_OB_GOLEN    | jak bardzo ubranie powi�ksza obw�d goleni
  ARMOUR_F_POW_KOSTKA      | NIEU�YWANE
  ARMOUR_F_POW_DL_STOPA    | jak bardzo ubranie powi�ksza d�ugo�� stopy
  ARMOUR_F_POW_SZ_STOPA    | jak bardzo ubranie powi�ksza szeroko��
                           | stopy
                           |
  PLAYER_F_ROZ_GLOWA       | rozmiar ubrania: obw�d g�owy
  PLAYER_F_ROZ_SZYJA       | rozmiar ubrania: obw�d szyi
  PLAYER_F_ROZ_BARKI       | rozmiar ubrania: obw�d obr�czy ko�czyny g�rnej
  PLAYER_F_ROZ_DL_RAMIE    | rozmiar ubrania: d�ugo�� ramienia
  PLAYER_F_ROZ_OB_RAMIE    | rozmiar ubrania: obw�d ramienia
  PLAYER_F_ROZ_DL_PRAMIE   | rozmiar ubrania: d�ugo�� przedramienia
  PLAYER_F_ROZ_OB_PRAMIE   | rozmiar ubrania: obw�d przedramienia
  PLAYER_F_ROZ_NADGARSTEK  | rozmiar ubrania: obw�d nadgarstka
  PLAYER_F_ROZ_DL_DLON     | rozmiar ubrania: d�ugo�� d�oni
  PLAYER_F_ROZ_SZ_DLON     | rozmiar ubrania: szeroko�� d�oni
  PLAYER_F_ROZ_TULOW       | rozmiar ubrania: d�ugo�� tu�owia
  PLAYER_F_ROZ_PIERSI      | rozmiar ubrania: obw�d w klatce piersiowej
  PLAYER_F_ROZ_PAS         | rozmiar ubrania: obw�d w pasie
  PLAYER_F_ROZ_BIODRA      | rozmiar ubrania: obw�d w biodrach
  PLAYER_F_ROZ_DL_UDO      | rozmiar ubrania: d�ugo�� uda
  PLAYER_F_ROZ_OB_UDO      | rozmiar ubrania: obw�d uda
  PLAYER_F_ROZ_DL_GOLEN    | rozmiar ubrania: d�ugo�� goleni
  PLAYER_F_ROZ_OB_GOLEN    | rozmiar ubrania: obw�d goleni
  PLAYER_F_ROZ_KOSTKA      | rozmiar ubrania: obw�d kostki
  PLAYER_F_ROZ_DL_STOPA    | rozmiar ubrania: d�ugo�� stopy
  PLAYER_F_ROZ_SZ_STOPA    | rozmiar ubrania: szeroko�� stopy
 ----------------------------------------------------------------------

Jak wida� cz�� w�a�ciwo�ci nie jest jeszcze u�ywana, co by� mo�e
zostanie w przysz�o�ci zmienione. Je�eli chodzi o powi�kszanie rozmiar�w
przez ubrania, to jest to powi�kszenie dodatkowe. Opr�cz niego ka�de
ubranie zwi�ksza rozmiary o domy�ln� warto��:

* 1 cm - w przypadku szeroko�ci i d�ugo�ci d�oni
* 2 cm - w przypadku szeroko�ci i d�ugo�ci stopy
* 4 cm - w przypadku pozosta�ych rozmiar�w

========================
IV. Domy�lne kroje ubra�
========================

Domy�lne kroje ubra� dla poszczeg�lnych ras i p�ci zdefiniowane s� w
/d/Standard/login/login.h w mappingu RACEROZMOD. Mapping ten przypisuje
nazwom ras tablic� z nast�puj�cymi trzema polami:

*[0] = tablica z rozmiarami dla p�ci m�skiej
*[1] = tablica z rozmiarami dla p�ci �e�skiej
*[2] = tablica ze wsp�czynnikami odniesienia

Pierwsze dwie tablice to po prostu rozmiary w takiej kolejno�ci jak
przedstawione w rozdziale II. Wsp�czynniki odniesienia to tablica z
dwoma elementami:

*[0] = wzrost (w centymetrach)
*[1] = waga (w gramach)

Je�eli gracz ma inny wzrost lub/i wag� ni� parametry ze wsp�czynnik�w
odniesienia, to odpowiadaj�ce mu rozmiary (dla jego rasy i p�ci) zostan�
odpowiednio przeskalowane.

============
V. Przyk�ady
============

Nic tak nie uczy, jak przyk�ady rzeczy rzeczywi�cie u�ywanych. W zwi�zku
z tym wklej� tutaj kod czerwonych koronkowych majteczek napisany kiedy�
przez Alcyone. Opr�cz tego przyk�adu mo�na oczywi�cie znale��
mas� przydatnych rzeczy w /doc oraz po prostu przyjrze� si�
obiektom u�ywanym na mudzie.

>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

/*
 * Czerwone koronkowe majteczki.
 *
 * Autor: Alcyone
 */

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <macros.h>
#include <materialy.h>
void
create_armour()
{  
   ustaw_nazwe(({"majteczki","majteczek","majteczkom","majteczki","majteczkami","majteczkach"}),
                PL_MESKI_NOS_NZYW);

        
    ustaw_shorty(({"para czerwonych koronkowych majteczek", "pary czerwonych koronkowych majteczek", 
        "parze czerwonych koronkowych majteczek", "par� czerwonych koronkowych majteczek", 
        "par� czerwonych koronkowych majteczek", "parze czerwonych koronkowych majteczek"}), 
        ({"pary czerwonych koronkowych majteczek", "par czerwonych koronkowych majteczek", 
        "parom czerwonych koronkowych majteczek", "pary czerwonych koronkowych majteczek", 
        "parami czerwonych koronkowych majteczek", "parach czerwonych koronkowych majteczek"}), PL_ZENSKI);
    
    dodaj_przym("czerwony","czerwoni");
    dodaj_przym("koronkowy", "koronkowi");
    set_long("Do�� mocno wyci�te czerwone koronkowe majteczki. Kszta�ty znajduj�ce "+ 
             "si� na nich przedstawiaj� male�kie rozwini�te czarne r�yczki, kt�re przeplataj� "+ 
             "si� ze sob� tworz�c stylowy wz�r. Nie posiadaj� �adnych zb�dnych ozd�b.\n");

    add_item(({"kszta�ty", "wz�r", "r�yczki", "czarne r�yczki"}), "Male�kie rozwini�te czarne r�yczki " +
        "odwzorowane zosta�y z niesamowit� precyzj�.\n");
    
    set_slots(A_HIPS);
    add_prop(OBJ_I_WEIGHT, 20);
    add_prop(OBJ_I_VOLUME, 100);
    add_prop(OBJ_I_VALUE, 50);
    
    /* majteczki s� bardzo rozci�gliwe */
    add_prop(ARMOUR_F_PRZELICZNIK, 50.0);
    /* ustalamy, �e majteczki s� dla kobiet */
    add_prop(ARMOUR_I_DLA_PLCI, 1);
    /* ustawiamy materia� */
    ustaw_material(MATERIALY_KORDONEK);
    /* ustawiamy rozci�gliwo�� w d� na 30% */
    add_prop(ARMOUR_I_D_ROZCIAGLIWOSC, 30);
}
string
query_auto_load()
{
   return MASTER;
}

>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

My�l�, �e kod jest w miar� jasny. Je�eli nie, to prosz� si� kontaktowa�
z Jeremianem na mudzie.

*******************
01.03.2006 Jeremian
