                  *--------------------------------*
                  DODATEK DO PORADNIKA PISANIA UBRA�
                  *--------------------------------*


1. Je�li kto� jest ciekaw zawarto�ci tej tablicy RACEROZMOD z pliku login.h oto ona:

#define RACEROZMOD ([ \
	"cz^lowiek"	: ({({59.0,37.0,104.0,30.0,28.0,28.0,25.0,17.0,20.0,11.0,60.0,\
			      99.0,80.0,92.0,50.0,51.0,40.0,36.0,24.0,28.0,11.0}),\
		            ({57.0,35.0,95.0,30.0,25.0,26.0,23.0,16.0,17.0,10.0,62.0,\
			      95.0,66.0,95.0,45.0,48.0,39.0,34.0,24.0,25.0,10.0}),\
		            ({179.0,65000.0})}),\
	"elf"		: ({({59.0,35.0,97.0,35.0,24.0,27.0,22.0,16.0,21.0,10.0,73.0,\
			      87.0,71.0,88.0,49.0,45.0,44.0,34.0,24.0,26.0,10.0}),\
		            ({56.0,31.0,90.0,34.0,22.0,28.0,20.0,14.0,19.0,9.0,64.0,\
			      89.0,62.0,90.0,50.0,44.0,42.0,32.0,22.0,24.0,9.0}),\
		            ({188.0,60000.0})}),\
	"p^o^lelf"	: ({({59.0,37.0,104.0,30.0,28.0,28.0,25.0,17.0,20.0,11.0,60.0,\
			      99.0,80.0,92.0,50.0,51.0,40.0,36.0,24.0,28.0,11.0}),\
		            ({57.0,35.0,95.0,30.0,25.0,26.0,23.0,16.0,17.0,10.0,62.0,\
			      95.0,66.0,95.0,45.0,48.0,39.0,34.0,24.0,25.0,10.0}),\
		            ({180.0,64000.0})}),\
	"krasnolud"	: ({({60.0,42.0,120.0,27.0,33.0,25.0,30.0,20.0,17.0,14.0,40.0,\
			      109.0,100.0,107.0,40.0,59.0,31.0,45.0,31.0,23.0,11.0}),\
		            ({59.0,39.0,105.0,26.0,28.0,24.0,27.0,17.0,17.0,12.0,39.0,\
			      111.0,90.0,101.0,39.0,61.0,30.0,39.0,27.0,20.0,10.0}),\
		            ({140.0,80000.0})}),\
	"nizio^lek"	: ({({57.0,35.0,86.0,25.0,25.0,24.0,23.0,15.0,15.0,9.0,34.0,\
			      89.0,87.0,87.0,37.0,48.0,27.0,31.0,20.0,21.0,9.0}),\
		            ({56.0,34.0,82.0,24.0,24.0,23.0,22.0,13.0,14.0,8.0,33.0,\
			      89.0,85.0,89.0,36.0,50.0,26.0,30.0,20.0,20.0,8.0}),\
		            ({120.0,55000.0})}),\
	"gnom"		: ({({57.0,35.0,86.0,27.0,25.0,26.0,23.0,15.0,17.0,9.0,34.0,\
			      87.0,84.0,86.0,37.0,47.0,27.0,30.0,20.0,22.0,10.0}),\
		            ({56.0,34.0,82.0,27.0,25.0,26.0,23.0,14.0,16.0,9.0,33.0,\
			      87.0,82.0,88.0,36.0,50.0,26.0,29.0,20.0,21.0,9.0}),\
		            ({120.0,50000.0})}),\
                    ])



2. Lista hitlokacji dla humanoid�w to:


#define A_NECK			TS_NECK
#define A_CHEST			TS_CHEST
#define A_STOMACH		(TS_STOMACH | TS_WAIST)
#define A_BODY			TS_TORSO
#define A_TORSO			TS_TORSO
#define A_HEAD			TS_HEAD
#define A_HIPS			TS_HIPS

#define A_R_THIGH		TS_R_THIGH
#define A_L_THIGH		TS_L_THIGH
#define A_THIGHS		TS_THIGHS
#define A_ANY_THIGH		(-TS_R_THIGH) // przejrzysciej to ma byc !!!

#define A_R_SHIN		TS_R_SHIN
#define A_L_SHIN		TS_L_SHIN
#define A_SHINS			TS_SHINS
#define A_ANY_SHIN		(-TS_R_SHIN) // !!!

#define A_R_LEG			TS_R_LEG
#define A_L_LEG			TS_L_LEG
#define A_LEGS			TS_LEGS
#define A_ANY_LEG		(-A_R_LEG) // !!!

#define A_R_SHOULDER		TS_R_SHOULDER
#define A_L_SHOULDER		TS_L_SHOULDER
#define A_SHOULDERS		TS_SHOULDERS
#define A_ANY_SHOULDER		(-TS_R_SHOULDER) // !!!

#define A_R_FOREARM		TS_R_FOREARM
#define A_L_FOREARM		TS_L_FOREARM
#define A_FOREARMS		TS_FOREARMS
#define A_ANY_FOREARM		(-TS_R_FOREARM)

#define A_R_ARM			TS_R_ARM
#define A_L_ARM			TS_L_ARM
#define A_ARMS			TS_ARMS
#define A_ANY_ARM		(-TS_R_ARM)

#define A_R_HAND		TS_R_HAND
#define A_L_HAND		TS_L_HAND
#define A_HANDS			TS_HANDS
#define	A_ANY_HAND		(-TS_R_HAND)

/*nowe, test. lil*/
#define A_R_FINGER		TS_R_FINGER
#define A_L_FINGER		TS_L_FINGER
#define A_FINGERS		TS_FINGERS
#define A_ANY_FINGER		(-TS_R_FINGER)

/* molder  - w ubraniach sa bledy z niezdefiniowanego A_WAIST,
 * dodalem TS_WAIST do A_STOMACH */
#define	A_WAIST			TS_WAIST


#define A_R_FOOT		TS_R_FOOT
#define A_L_FOOT		TS_L_FOOT
#define A_FEET			TS_FEET
#define A_ANY_FOOT		(-TS_R_FOOT)

#define A_ROBE			TS_ROBE

#define A_SHIELD		-(TS_R_WEAPON | TS_R_FOREARM) // !!!
#define A_BUCKLER		-(TS_R_FOREARM) // !!!










Pozdrawiam,
Lil