
                        *----------------------*
                          PORADNIK PISANIA NPC
                        *----------------------*

Ten oto niewielki poradnik ma pomoc Wam postawic pierwsze kroki
w nielatwej sztuce tworzenia bohaterow niezaleznych ;)
W zadnym wypadku nie nalezy tego poradnika traktowac jako wyczerpujacego
podrecznika w tym temacie, jedynie jako zbior wskazowek, jak w miare
prosty sposob i szybko osiagnac cel, jakim jest napisanie przecietnego
NPC'a. Oczywiscie wszelkie uwagi beda zawzse mile widziane.. i byc moze
dzieki Wam poradnik ow zamieni sie kiedys w porzadny i rzetelny podrecznik ;)

Przykladowy kod umieszczony tutaj pochodzi z karczmarza w Rinde (by Lil).



I. Najprostszy NPC.

Wystarczy zaledwie pare linijek, by stworzyc prostego NPC'a:

#include <macros.h>
inherit "/std/monster";

void
create_monster()
{
    // Istotne jest jakiej rasy jest nasz NPC.
    // W tym wypadku jest to czlowiek plci meskiej ;)
    ustaw_odmiane_rasy(PL_MEZCZYZNA);

    // Najczesciej trzeba tez bezposrednio ustawic mu plec.
    set_gender(G_MALE);

    // Tu nastepuje opis NPC'a widziany przy jego ogladaniu.
    set_long("Lekko zaczerwieniony na licach, cho� pr�dzej od zm�czenia, "+
                "ni�li jakich trunk�w spo�ywania. Odziany jest w szar� "+
                "koszul�, lu�ne spodnie oraz d�ugi poplamiony fartuch, "+
                "kt�ry niegdy� m�g� by� bia�y, lecz teraz przybiera "+
                "jedynie odcienie ��ci z ciemniejszymi plamami w "+
                "kilku miejscach. Jego oblicze mieni si� od potu, dlatego "+
                "te� nader cz�sto przeciera je noszona za pasem szmat�, "+
                "czy ramieniem, gdy d�onie jego zaj�te. A zaj�� ma "+
                "sporo - biega za kontuarem niczym oszala�y, czy�ci "+
                "z czci� ka�de naczynie, polewa do kufli i kieliszk�w, "+
                "zdarzy si�, �e rzuci jakie zdawkowe has�o, li to rozkaz "+
                "w stron� kuchni, a w wolnej chwili, je�li si� takowa "+
                "nadarzy - wys�uchuje cierpliwie zwierze� tych co bardziej "+
                "upojonych trunkami klient�w.\n");

    // .. i przymiotniki.. wedle uznania.
    dodaj_przym("pulchny","pulchni");
    dodaj_przym("spocony","spoceni");

    // Musimy koniecznie okreslic cechy NPC'a..
    set_stats ( ({ 65, 31, 70, 21, 30, 58 }) );

    // .. i jego podstawowe rozmiary, czyli waga (w gramach) i wysokosc (w cm) ;)
    add_prop(CONT_I_WEIGHT, 91000);
    add_prop(CONT_I_HEIGHT, 172);
}
EOF


Tak wyglada najprostszy przyklad enpeca. Oczywiscie kolejnosc ustawiania
roznych rzeczy w create_monster nie musi byc taka jak podana tutaj, moze
byc zupelnie dowolna, wazne jest tylko, zeby wystapily wszystkie rzeczy
wymienione powyzej.


II. Emoty i akcje.

Najczesciej bedziemy chcieli, zeby nasz enpec nie stal sobie bezczynnie na lokacji,
ale 'zyl', robil cos i mowil, albo odpowiadal na pytania graczy. W create_monster
musimy ustawic jakie akcje ma nasz enpec wykonywac, co mowic, i jakich odpowiedzi
na pytania udzielac:

set_act_time(30); // Robi cos co 30 sekund.
add_act("emote wyciera szklanki.");
add_act("emote ociera pot z czo�a.");

// Tu sa teksty mowione gdy enpeca nikt nie atakuje..
set_chat_time(45);
add_chat("Ehhh, wszedzie te przeklete myszy!");

// .. a tu teksty w trakcie walki
set_cchat_time(15);
add_cchat("Zaraza!");
add_cchat("Nie licz, �e ujdzie ci to p�azem!");

Zeby enpec odpowiadal na pytania musimy mu dodac nastepujace rzeczy. W create_monster:

add_ask(({"pomoc", "prac�", "zadanie"}), VBFC_ME("pyt_o_pomoc"));
set_default_answer(VBFC_ME("default_answer"));

No i skoro juz uzywamy w tym miejscu funkcji pyt_o_pomoc i default_answer,
to musimy je zadeklarowac (za funkcja create_monster):

// Funkcja pomocnicza: enpec mowi jakis tekst, pod warunkiem ze obiekt 'player'
// znajduje sie na lokacji.
void
powiedz_gdy_jest(object player, string tekst)
{
    if (environment(this_object()) == environment(player))
        this_object()->command(tekst);
}

string
pyt_o_pomoc()
{
    set_alarm(2.0, 0.0, "powiedz_gdy_jest", this_player(),
        "powiedz do " + this_player()->query_name(PL_DOP) + " Bol� mnie plecy.");
    return "";
}

string
default_answer()
{
    set_alarm(2.0, 0.0, "powiedz_gdy_jest", this_player(),
        "powiedz do "+ this_player()->query_name(PL_DOP) +
        " Przykro mi. Pom�g� bym ci, ale teraz nie mam czasu.");
    return "";
}


III. Umiejetnosci i ekwipunek.

Czesto chcemy zeby tworzony przez nas enpec posiadal jakies umiejetnosci.
Wystarczy dla kazdej umiejetnosci dodac w create_monster linijke postaci:

// W tym wypadku ustawi to losowo umiejetnosc unikow w przedziale 10-13
set_skill(SS_DEFENCE, 10 + random(4));

Ekwipunek dodaje sie w nastepujacy sposob:

add_weapon("/lib/obiekty/bronie/krotki_miecz");
add_armour(RINDE_UBRANIA + "fartuch_los_los_rzemieslniczy.c");

Powoduje to ze tworzony enpec od razu ma na sobie okreslone ubrania i zbroje
oraz dobywa broni.


