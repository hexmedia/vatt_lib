Data ostatniej modyfikacji: 4.10.2005

Poniewa� manuale oraz wszelkie pomoce na ten temat na innych forach (np. Mudy w sosie w�asnym) staj� si� coraz mniej podobne do stylu naszych lokacji postaram si� w miar� tre�ciwie i kr�tko przedstawi� w punktach spos�b, w jaki lokacje powinny by� kodowane na Vatt'ghernie. Postaram si� skoncentrowa� g��wnie na tych najwi�kszych r�nicach mi�dzy standardowymi prostymi lokacjami, a tymi na naszym mudzie, dlatego nie jest to �adne kompendium ani pe�na pomoc na temat kodowania lokacji.

1. Po pierwsze i zdecydowanie najwa�niejsze: OBIEKTOWO��.
W tym punkcie postaram si� wyja�ni� na czym to ma polega� i jak to ugry��. Ot�, przede wszystkim musimy kierowa� si� zasad� dobrego smaku i zdrowym rozs�dkiem. Staramy si�, by na lokacji by�o JAK NAJMNIEJ pseudo obiekt�w tworzonych funkcj� add_item(). Lecz nie popadajmy w skrajno��.
Zwyk�e przedmioty, kt�re mo�emy tylko obejrze� (tj. stworzy� za pomoc� funkcji add_item()) s� jak najbardziej na miejscu przy: pod�ogach, �cianach, suficie i tego typu sta�ych elementach. Ca�� reszt� musimy zakodowa� w osobnych plikach, jako niezale�ne obiekty. Nawet, je�li chcemy postawi� na �rodku lokacji super ci�ki st�, lub przymocowan� na sta�e do �ciany tabliczk� z menu w karczmie - musimy uczyni� z nich osobne obiekty. Do siadania tak�e starajmy si� u�ywa� cz�ciej funkcji make_me_sitable() w obiektach, ni� add_sit() w lokacjach. Funkcja add_sit() zarezerwowana jest do miejsc niezale�nych od obiekt�w - np. na ziemi, na bruku, pod �cian�.
Jest jeszcze sprawdz drzwi (/std/door) i okien (/std/window) - MUSZ� by� one zawsze tworzone tam, gdzie lokacja tego wymaga.
Wi�cej szczeg��w o kodowaniu obiekt�w napisa�am w: /doc/jak_kodowac/obiekty.

2. Akcje, kt�re s� powi�zane bezpo�rednio z obiektami tworzymy w tych oto obiektach, a nie w kodzie lokacji.

3. Opisy lokacji. Opisy musz� by� jak najbardziej dynamiczne ze wzgl�du na punkt 1. Opis musi by� prawdziwy i ukazywa� lokacj� tak�, jak� jest w istocie (ale odkrycie!), z uwzgl�dnieniem przedmiot�w aktualnie znajduj�cych si� na niej.
Dlatego te� zalecane jest robienie osobnej funkcji jako d�ugi opis. W takim wypadku wpisujemy miedzy ma�pki funkcj� np.dlugi_opis(), czyli powinno wygl�da� to tak: set_long(@@dlugi_opis@@);.
 3a. Teraz mo�emy stworzy� poza funkcj� create_room() funkcj� string dlugi_opis(), a w niej w�a�nie ju� ca�� mas� zale�no�ci, by na ko�cu zwr�ci� (return) tego stringa. Stop. Zale�no�ci - to w�asnie one s� najistotniejsze w opisywaniu lokacji. Na szcz��ie s� tak�e (IMHO) najprostsze, a to dzi�ki specjalnym funkcjom temu s�u��cym. Oto kr�tka lista kilku przyk�adowych zale�no�ci (s� to fragmenty kodu ju� gotowe do wrzucenia do lokacji):

a) if(jest_rzecz_w_sublokacji(0,"ma�a umorusana dziewczynka"))
  Jak sama nazwa wskazuje, sprawdza, czy taka dziewczynka znajduje si� na lokacji.
b) if(!dzien_noc())
   Czyli: Jesli jest dzien, to....
c) if(sizeof(FILTER_LIVE(all_inventory()))>=5)
   Czyli: Jesli na lokacji jest 5 lub wiecej istot, to...
   UWAGA: Tutaj dodatkowo musimy na gorze pliku dopisac: #include <filter_funs.h>

A oto nieco trudniejsza, lecz chyba najcz�ciej u�ywana zale�no��:
Jest to warunek sprawdzaj�cy ilo�� niewy�wietlanych obiekt�w (o niewy�wietlanych obiektach i sublokacjach napisz� w nast�pnym podpunkcie).
Wpierw musimy zdefiniowa� rzeczy niewy�wietlane w funkcji lokacji (czyli w create_room()), a robimy to w spos�b nast�puj�cy:
  dodaj_rzecz_niewyswietlana("d�bowa �awa", 2);
  lub, zamiast kr�tkiego opisu przedmiotu, mo�emy poda� adres pliku (daje to pewno��, �e tylko TEN przedmiot wychwyci ta funkcja), w spos�b taki:
  dodaj_rzecz_niewyswietlana("/lib/krzemien",2)
  Drugi argument (w tym przypadku cyfra 2) okre�la ilo�� tych przedmiot�w, kt�re nie b�d� wy�wietlane po d�ugim opisie (czyli jako swobodnie od�o�one). Tzn: tylko dwie d�bowe �awy nie b�d� wy�wietlane jako przedmioty l꿱ce swobodnie, a je�li na lokacji znajd� si� 3 takie same �awy, to jako przedmioty swobodnie le��ce b�dzie wida� tylko jedn� �aw�.
  Okej, niewy�wietlanie przedmiot�w ju� za nami, teraz musimy te dwie �awy pokaza� w d�ugim opisie. Zak�adam, �e robimy osobn� funkcj� dlugi_opis(), a w niej musimy wpisa� tak:
string dlugi_opis(string str)
{
  int i, il, il_stolow;
  object *inwentarz;
  inwentarz=all_inventory();
  il_stolow=0;
  il=sizeof(inwentarz);
  for (i = 0; i < il; ++i)
  {
     if (inwentarz[i]->jestem_super_stolem())
     {
     ++il_stolow;
     }
  }
  
/* I teraz mo�emy ju� dalej uk�ada� warunki, a musimy uwzgl�dni� wszystkie mo�liwo�ci ilo�ci sto��w w d�ugim opisie. W tym wypadku mamy trzy mo�liwo��i: Brak sto��w(kto� je zabra�), jeden st� i dwa sto�y. Dlatego uk�adamy 3 warunki: */

if(il_stolow == 0)
{
 str = "Nie ma stolow."; 
}
if(il_stolow == 1)
{
 str = "Jest tylko jeden stol.";
}
if(il_stolow == 2)
{
 str = "Sa dwa stoly.";
}

/* I to wszystko! A teraz, skoro idzie nam ju� tak dobrze - nauczymy sie przy okazji dodawac cos jeszcze do 'str' i zakonczymy ladnie funkcje dlugi_opis() */

str += " Jest calkiem fajnie.";

/* ----------tutaj jest miejsce na reszte funkcji ----------- */

str += "\n";
return str;
}

Koniec tego dobrego. :) Aha, pamietajmy o tym, by na gorze pliku zadeklarowac funkcje. W tym przypadku powinno to wygladac w ten sposob: "string dlugi_opis(string str);"

3b. Sublokacje. Jak wiadomo (albo i nie), nasze lokacje r�ni� si� tak�e sublokacjami. Tutaj tak�e kierujemy si� rozs�dkiem. Wpierw musimy wymy�le� jak najg�upzse warianty ustawiania rzeczy w sublokacjach, jakie przychodz� nam do g�owy. Mo�emy by� pewni, �e gracze wymy�l� jeszcze g�upsze, ca�kiem nie�wiadomie.:P
Atrybut�w sublokacji mamy tak�e na tyle du�o, �e z pewno�ci� wystarcz�, by spe�ni� nasze oczekiwania.
I tutaj, podobnie, jak w punkcie pierwszym, musimy uwa�a�, by nie tworzy� sublokacji zwi�zanych bezpo�rednio z przedmiotami. Mo�emy stworzy� sublokacj� "w rogu" lub "na �cianie", lecz sublokacj� "na wieszaku" robimy ju� w samym obiekcie wieszaka.
Przedstawi� tu sublokacj� "na �cianie" i wyja�nie wszystko na tym przyk�adzie:
A zatem, w create_room() dodajemy wpierw sublokacj�:

add_subloc("na �cianie",0,"ze �ciany");
  /* Teraz ustawiamy jej atrybuty: */
add_subloc_prop("na �cianie", SUBLOC_I_MOZNA_POWIES, 1);
                     /* Czyli mo�na wiesza� na tym subloku */
add_subloc_prop("na �cianie", CONT_I_CANT_ODLOZ, 1);
                     /* Nie mo�na odk�ada� na tego subloka */
add_subloc_prop("na �cianie", SUBLOC_I_DLA_O, O_SCIENNE);
                 /* Przyjmuje tylko obiekty typu Sciennego (obraz,poroza..) */
add_subloc_prop("na �cianie", CONT_I_MAX_RZECZY, 1);
               /* Jako,�e na �cianie jest tylko jedno miejsce - mo�emy 
	          powiesi� tam tylko jeden przedmiot   */
/* No i ladujemy jakis obrazek, by domyslnie na scianie cos wisialo.. */
clone_object("/Sciezka/do/obrazu/obraz.c")->move(this_object(), "na �cianie");


Wi�cej o sublokacjach jest wyja�nione na przyk�adzie pokoiczku w /doc/examples/lokacje/pokoiczek.c. W�a�ciwie to tylko powieli�am fragment z tamtego przyk�adu. 
Co prawda, tam warunki w d�ugim opisie lokacji s� robione od razu w funkcji set_long() - c�, kwestia gustu. :)


4. Eventy. 90% z nich wymaga pewnych specjalnych warunk�w, by zaistnie�. Dlatego te�, event "Z sali obok dochodzi ci� gromki �miech" b�dzie m�g� zaistnie� tylko wtedy, gdy faktycznie kto� obok (np. gracz) si� za�mieje gromko. Dalej: do eventu "Kot przebieg� ci drog�" potrzebny jest...kot! Musimy zakodowa� wpierw takie zwierz�tko, a w funkcji add_event() da� odwo�anie do funkcji klonuj�cej kota. Wygl�da� to powinno w ten spos�b:

add_event("Kot przebieg� ci drog�", "klonuj_kota");

i poza funkcj� create_room() piszemy:

string klonuj_kota()
{
   clone_object("/Sciezka/do/kota/kot.c")->move(environment(this_player()));
   return 1;
}

I to wszystko ze strony lokacji. Bedziemy jeszcze musieli teraz dopie�ci� naszego kotka. Powinni�my w nim umie�ci� alarm na czas np. 4sekund na usuwanie kotka. Mo�emy to zrobi� mniej wi�cej tak:

int idzie(); /* deklarujemy to na g�rze pliku kotka */
create_monster()
{
 /* miejsce na dlugi opis, odmiane rasy, plec, statsy itd. */

 set_alarm(4.0, 0.0, idzie);
}
/* i teraz opisujemy funkcje idzie() */
int idzie()
{
remove_object();
return 1;
}

Oto najrpostszy event na �wiecie. :) Oczywi�cie, powinni�my tak�e wzi�� pod uwag� to, �e kto� m�g�by sobie np. wzi�� tego kotka (wtedy alarm nie powinien by� aktywowany), spr�bowa� go zabi� itd. itd. Ale to ju� zale�y od naszych inwencji i to chyba troszk� wy�sza szko�a jazdy....



To by chyba by�o na tyle z tego, na co k�adziemy g��wny nacisk przy tworzeniu lokacji. Og�lnie (pr�cz event�w i d�ugiego opisu) chodzi o to, by lokacje by�y jak najprostsze, odwrotnie proporcjonalnie do obiekt�w wyst�puj�cych na danej lokacji.

Ze swojej strony polecam gor�co manual do VBFC (to s� w�a�nie te funkcje mi�dzy ma�pkami w np. d�ugim opisie lokacji), dost�pny pod: man VBFC_MANUAL. Jest to pot�na dawka informacji, kt�r� powinien posi��� ka�dy. Niestety, p�ki co jest w j�zyku angielskim (mo�e kto� ch�tny do przet�umaczenia?).
Polecam tak�e zapoznanie si� z plikami w katalogu: /doc/examples/lokacje/ <- Zwr��cie uwag� na to, �e tylko lokacja "pokoiczek" jest napisana dla potrzeb Vatt'gherna.

Wszelkie uwagi i sugestie a'propos tego kr�tkiego poradnika b�d� mile widziane.

                                               Lil.
