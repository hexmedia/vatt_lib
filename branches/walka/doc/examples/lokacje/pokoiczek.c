#include <stdproperties.h>
#include <object_types.h>
#include <pl.h>
#include <macros.h>

inherit "/std/room";

#define sciezka "/doc/examples/lokacje/pokoiczek_rzeczy/"

/* Trzeba si� troszk� nagimnastykowa� z opisami tych mebli. Szczeg�lnie z niejakim
 * kryszta�owym sto�em. */

static int ile_krstol = 0;
string opis_mebelkow();

create_room()
{
set_short("Testowy pokoiczek");

set_long("Czerwone ^swiat^lo zalewa@@opis_sublokacji_od_rzeczy|kryszta^lowy st^o^l||" +
         " per^low^a komnat^e, odbija si� od kryszta^lowego sto^lu i rozprasza" +
	 " na bielonych �cianach.| t^e per^low^a komnat^e.@@" +
	 "@@opis_sublokacji| W rogu |r�g|.|||stoi |stoj� |stoi @@" +
	 "@@opis_sublokacji| Na �cianie |�ciana|.|||wisi |wisz� |wisi @@" +
	 "@@opis_mebelkow@@\n");

/* dodajemy sublokacj� nie podpadaj�c� pod �aden z g�ownych typ�w */
add_subloc(({"r�g", "rogu", "rogowi", "r�g", "rogiem", "rogu"}));
/* w zwi�zku z tym musimy ustawi� odpowiedniego propa, by mo�na by�o do sublokacji odk�ada� rzeczy */
add_subloc_prop("r�g", SUBLOC_I_MOZNA_ODLOZ, 1);
/* a tak�e ustali�, �e je�eli chodzi o branie, to sublokacja zachowuje si� jak defaultowa */
add_subloc_prop("r�g", SUBLOC_I_TYP_W, 1);
/* w rogu mo�na tylko meble k�a�� */
add_subloc_prop("r�g", SUBLOC_I_DLA_O, O_MEBLE);

/* dodajemy sublokacj� typu 'na', jednak dosy� specyficzn� */
add_subloc(({"�ciana", "�ciany", "�cianie", "�cian�", "�cian�", "�cianie", "na"}));
/* nie chcemy, by mo�na by�o na �cian� odk�ada� ani k�a�� rzeczy */
add_subloc_prop("�ciana", CONT_I_CANT_ODLOZ, 1);
add_subloc_prop("�ciana", CONT_I_CANT_POLOZ, 1);
/* oczywi�cie mo�na na �cianie wiesza� przedmioty */
add_subloc_prop("�ciana", SUBLOC_I_MOZNA_POWIES, 1);
/* ale tylko typu �ciennego */
add_subloc_prop("�ciana", SUBLOC_I_DLA_O, O_SCIENNE);
/* ograniczamy ilo�� rzeczy na �cianie do dw�ch */
add_subloc_prop("�ciana", CONT_I_MAX_RZECZY, 2);

/* nie chcemy, by kryszta�owy st� by� wy�wietlany w defaltowej sublokacji */
dodaj_rzecz_niewyswietlana("kryszta^lowy st^o^l");

/* Testowy pokoiczek jest pomieszczeniem zamkni�tym, tak wi�c trzeba odpowiedni prop ustali� */
add_prop(ROOM_I_INSIDE, 1);

/* Wykorzystujemy informacj�, czy lokacja zosta�a odczytana czy nie */
if (!czy_odczytana_lokacja())
{
	write("Lokacja jest tworzona pierwszy raz!\n");
	/* Teraz wrzucamy tutaj tyle rzeczy, by long by� taki jak w po�cie Moldera na forum ;) */
	clone_object(sciezka + "krysztalowy_stol.c")->move(this_object());
	clone_object(sciezka + "obraz.c")->move(this_object(), "na �cianie");
	clone_object(sciezka + "obraz.c")->move(this_object(), "na �cianie");
	clone_object(sciezka + "krzeslo.c")->move(this_object());
	clone_object(sciezka + "krzeslo.c")->move(this_object());
	clone_object(sciezka + "krzeslo.c")->move(this_object());
	clone_object(sciezka + "krzeslo.c")->move(this_object());
	clone_object(sciezka + "stol.c")->move(this_object());
	clone_object("/doc/examples/bronie/mizerykordia")->move(this_object());
	clone_object("/doc/examples/pojemniki/skrzynia")->move(this_object());
	clone_object(sciezka + "szafa.c")->move(this_object(), "w rogu");
}
else
{
	write("Lokacja zosta�a odczytana z pliku!\n");
}
}

/* Chcemy, by meble nie by�y wy�wietlane w defaultowej sublokacji, */
/* w zwi�zku z czym dodajemy ka�dy nowy mebel do rzeczy niewy�wietlanych */

public void 
enter_inv(object ob, object from)
{
	::enter_inv(ob, from);
	if (!objectp(ob)) {
		return;
	}
	if (ob->query_type() == O_MEBLE) {
		if ((ob->short() ~= "kryszta�owy st�")) {
			++ile_krstol;
			if (ile_krstol == 1) {
				return;
			}
		}
		dodaj_rzecz_niewyswietlana(ob->short());
	}
}

public void
leave_inv(object ob, object to)
{
	::leave_inv(ob, to);
	if (!objectp(ob)) {
		return;
	}
	if (ob->query_type() == O_MEBLE) {
		if ((ob->short() ~= "kryszta�owy st�")) {
			--ile_krstol;
			if (ile_krstol <= 0) {
				ile_krstol = 0;
				return;
			}
		}
		usun_rzecz_niewyswietlana(ob->short());
	}
}

/* a tak�e swoj� funkcj� opisujemy meble */

string
opis_mebelkow()
{
	int i, il, ile;
	object *obarr;

	obarr = subinventory(0);
	obarr = filter(obarr, &operator(==)(, O_MEBLE) @ &->query_type());


	if ((il = sizeof(obarr))) {
		for (i = 0; i < il; ++i) {
			if (obarr[i]->short() ~= "kryszta�owy st�") {
				obarr -= ({ obarr[i] });
				break;
			}
		}
	}
	if ((il = sizeof(obarr))) {
		ile = 0;
		for (i = 0; i < il; ++i) {
			if (obarr[i]->short() ~= obarr[0]->short()) {
				++ile;
			}
		}
		return " Po�rodku pomieszczenia " + ilosc(ile, "stoi", "stoj�", "stoi") + " " + COMPOSITE_DEAD(obarr, PL_MIA) + ".";
	}
	return "";
}
