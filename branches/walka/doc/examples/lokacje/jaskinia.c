/* lokacja by jeremian. ja (molder) pozwolilem sobie umiescic ja jako
    przyklad, posiada ona bowiem magiczne drzwi otwierane starozytnym haslem .. */




inherit "/std/room";
void otw_wrota();
#include <ss_types.h>
#include <wa_types.h>
#include <formulas.h>
#include <stdproperties.h>
#include <macros.h>
#include <money.h>
#include <pl.h>
void ot_n_wrota();
#define DIR "/doc/examples/lokacje/"
void zam_wrota();
int ob_wrota();
int ot_wrota();
string haslo = "dupa";
string d_haslo = "'dupa";
object room1 = DIR + "jaskinia.c";
object room2 = DIR + "lab11.c";
int alarm_d;
int alarm_o;
void
create_room()
{
    set_short("Tajemny pokoik");
    set_long("Male pomieszczenie o idealnie gladkich scianach jest oswietlone przez " +
    "niebieskawy kamien, z ktorego bije cieple swiatlo. Sciany sa gladkie, wrecz" +
    " blyszczace, nie pokryte w ogole kurzem. Na jednej ze scian widzisz tabliczke" +
    " niewielkich rozmiarow. We wschodniej scianie widac kamienne wrota.\n");
    add_item("sciany","Sciany tego pomieszczenia naprawde sa bardzo gladkie. Ktos tu " +
    "musi regularnie sprzatac i polerowac ich powierzchnie.\n");
    add_item("kamien","Niewielki kamien umocowany mocno w scianie, swiecacy niebiesk" +
    "awa aura.\n");
    add_item(({"tabliczke","tablice"}),"Niewielkich rozmiarow tablica zawiera jakis " +
    "napis, ktory mozna zapewne przeczytac.\n");
    add_cmd_item(({"tablice","tabliczke","napis"}),"przeczytaj","\nTajemne miejsce, " +
    "gdzie moze kiedys bedzie cos specjalnego.\n");
    add_cmd_item("kamien",({"wydlub","wez"}),"Po zastanowieniu stwierdzasz, ze lepiej nie budzic " +
    "drzemiacych tutaj mocy.\n");
    add_cmd_item("wrota",({"obejrzyj","ob"}),ob_wrota);
    add_cmd_item("wrota","otworz",ot_wrota);
    add_item("runo","Skomplikowanie wyryte wzory ukladaja sie w jeden, trudnorozpoznawalny" +
    " wyraz " + haslo + ". Jest to pewnie jakies starozytne slowo.\n");

   
   add_prop(ROOM_I_INSIDE, 1);
}

int
ot_wrota()
{
int i,test;
mixed exits;
exits = room1->query_exit();
test = 0;
    for (i = 0 ; i < sizeof(exits) ; i += 3)
    {
	if (exits[i + 1] == "zachod")
	    test = 1;
    }
if (test == 0)
write("Kamienne wrota nie poddaja sie zadnej probie otwarcia. Na ich " +
"powierzchni dostrzegasz jakies runo.\n");
else
write("Kamienne wrota sa juz otwarte.\n");
return 1;
}

int
ob_wrota()
{
int i,test;
mixed exits;
exits = room1->query_exit();
test = 0;
    for (i = 0 ; i < sizeof(exits) ; i += 3)
    {
	if (exits[i + 1] == "zachod")
	    test = 1;
    }
if (test == 0)
write("Kamienne wrota sa dokladnie zamkniete. Olbrzymie plyty, z ktorych sa " +
"zrobione, przylegaja mocno do siebie nie dajac zadnej szansy na ich silowe " +
"otwarcie. Na kamieniu wrot jest wyryte jakies runo.\n");
else
write("Kamienne wrota sa otwarte na osciez. Olbrzymie plyty, z ktorych sa " +
"zrobione, schowane sa aktualnie prawie calkowicie w scianach. Na kamieniu " +
"wrot dostrzegasz jakies runo.\n");
return 1;
}

init()
{
    ::init();
    add_action("pow_haslo","powiedz");
    add_action("pow_d_haslo",d_haslo);
    add_action("pow_haslo","'");
    add_action("now_haslo","ustaw");
}

now_haslo(string str)
{
if (!str)
  return 0;
if (this_player()->query_name() == "Jeremian")
{
haslo = str;
d_haslo="'"+haslo;
add_action("pow_d_haslo",d_haslo);
write("Zmieniles haslo na " + haslo + " !\n");
room2->remove_item("runo");
room2->add_item("runo","Skomplikowanie wyryte wzory ukladaja sie w jeden, trudnorozpoznawalny" +
" wyraz " + haslo + ". Jest to pewnie jakies starozytne slowo.\n");
return 1;
}
else
return 0;
}

pow_d_haslo(string text)
{
string verb;
verb = query_verb();

if ((verb == d_haslo) && (text == 0))
{
otw_wrota();
return 1;
}
else
return 0;
}

pow_haslo(string text)
{
string test;
if (!text)
  return 0;
if (sscanf(text,"%s",test)==1)
        text = test;
if (text == haslo)
{
otw_wrota();
return 1;
}
else
return 0;
}

void
otw_wrota()
{
int i,test;
mixed exits;
exits = room1->query_exit();
test = 0;
    for (i = 0 ; i < sizeof(exits) ; i += 3)
    {
	if (exits[i + 1] == "zachod")
	    test = 1;
    }
write("Wypowiadasz tajemnicze slowo, kladac nacisk na poszczegolne" +
" akcenty.\n");
   saybb(QCIMIE(this_player(), PL_MIA) + " wypowiada tajemnicze " +
       "slowo, kladac nacisk na poszczegolne akcenty.\n");
if (test == 0)
{
alarm_o = set_alarm(2.0,0.0,ot_n_wrota);
alarm_d = set_alarm(12.0,0.0,zam_wrota);
}
}

void
zam_wrota()
{
int i,test;
mixed exits;
exits = room1->query_exit();
test = 0;
    for (i = 0 ; i < sizeof(exits) ; i += 3)
    {
	if (exits[i + 1] == "zachod")
	    test = 1;
    }
if (test == 1)
{
room1->remove_exit("zachod");
room2->remove_exit("wschod");
tell_room(room1,"Kamienne wrota zamykaja sie z wielkim hukiem.\n");
tell_room(room2,"Kamienne wrota zamykaja sie z wielkim hukiem.\n");
}
}

void
ot_n_wrota()
{
int i,test;
mixed exits;
exits = room1->query_exit();
test = 0;
    for (i = 0 ; i < sizeof(exits) ; i += 3)
    {
	if (exits[i + 1] == "zachod")
	    test = 1;
    }
if (test == 0)
{
room1->add_exit(DIR + "lab11.c","zachod",0,1);
room2->add_exit(DIR + "lab6.c","wschod",0,1);
tell_room(room1,"Kamienne wrota otwieraja sie wprawiajac okolice w drzenie.\n");
tell_room(room2,"Kamienne wrota otwieraja sie wprawiajac okolice w drzenie.\n");
}
}
