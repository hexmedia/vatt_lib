inherit "/std/object";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include "/sys/object_types.h"
#include <macros.h>

void
create_object() 
{
      ustaw_nazwe("obraz");
      
      set_long("Po obu stronach p��tna ci�gn� si� pasma drzew. Wierzcho�ki s� wyra�nie zarysowane. " +
		      "Porusza je �agodny i o�ywczy podmuch. Pomi�dzy drzewami wiedzie wiejska droga, " +
		      "na kt�r� padaj� ich ogromne cienie. Id� ni� r�wnie� barany pilnowane przez psa. " +
		      "Po �rodku obrazu wida� otwart� furtk� prowadz�c� na pola pszenicy, kt�rych ��� " +
		      "przy dominuj�cych kolorach br�zu i zieleni sprawia wra�enie jeszcze bardziej " +
		      "intensywnej.\n");
}

public int
query_type()
{
	return O_SCIENNE;
}

