/*
 * szafa
 * (C) 2005 by jeremian
 */

#pragma strict_types

inherit "/std/container";

#include <pl.h>
#include <macros.h>
#include <filter_funs.h>
#include <stdproperties.h>
#include <composite.h>
#include <cmdparse.h>
#include <object_types.h>

nomask void
create_container()
{
    ustaw_nazwe("szafa");

    set_long("Prosta, drewniana szafa." +
	    "@@opis_sublokacji| Wewn�trz ||.|||znajduje si� |znajduj� si� | znajduje si�@@" +
	    "@@opis_sublokacji| Na szafie widzisz |na|.||" + PL_BIE+ "@@\n");
    setuid();
    seteuid(getuid());
    add_prop(CONT_I_MAX_VOLUME, 20000);
    add_prop(CONT_I_VOLUME, 16800);

    add_prop(CONT_I_WEIGHT, 10000);
    add_prop(CONT_I_MAX_WEIGHT, 20000);

    add_subloc("na");
    
    /* sami opisujemy kontener */
    add_prop(CONT_I_DONT_SHOW_CONTENTS, 1);
}

public int
query_type()
{   
    return O_MEBLE;
}

