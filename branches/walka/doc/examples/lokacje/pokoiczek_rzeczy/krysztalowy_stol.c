/* Ostra przerobka ze stolika na napoje z Dragfor,
     Dziaa ju prawie jak container :)
        Praca zbiorowa :P
        Delvert, Gregh, Lil
	A na koniec Molder wywali�� po�ow� i wklei�� co innego :P */

/* krysztalowy stol jako przyklad dla tworzenia longow */

#pragma strict_types

inherit "/std/container";

#include <pl.h>
#include <macros.h>
#include <filter_funs.h>
#include <stdproperties.h>
#include <composite.h>
#include <cmdparse.h>
#include <object_types.h>

nomask void
create_container()
{
    ustaw_nazwe("st�");

    dodaj_przym("kryszta^lowy","kryszta^lowi");

    set_long("Kryszta�owa, grubociosana p�yta opiera si� na solidnych, kamiennych nogach. " +
	     "Lekko prze�wituj�cy blat rozprasza padaj�ce na� �wiat�o. Jego g�adka powierzchnia " +
	     "zdaje si� nie mie� nawet najmniejszych zarysowa�. Na wykonanych z czarnego marmuru " +
	     "nogach wida� jednak delikatne p�kni�cia." +
	     "@@opis_sublokacji| Na blacie |na|.|||le�y |le�� |le�y @@\n");

    add_item(({ "p�yt�", "blat", "powierzchnia" }), "Kryszta�owa powierzchnia jest doskonale g�adka. " +
		    "Nie wida� na niej nawet najmniejszego zadrapania.\n");
    add_item(({ "nog�", "nogi" }), "Marmurowe nogi, szersze ni� bochen chleba, przyczepione s� do grubego, " +
		    "kryszta�owego blatu.\n");
    add_item(({ "p�kni�cia", "p�kni�cia n�g", "p�kni�cia nogi" }), "Delikatne p�kni�cia na grubych czarnych " +
		    "nogach s� tylko powierzchowne.\n");
    setuid();
    seteuid(getuid());
    add_prop(CONT_I_MAX_VOLUME, 20000);
    add_prop(CONT_I_VOLUME, 16800);

    add_prop(CONT_I_CANT_WLOZ_DO, 1); /* By nie mo�na by�o nic do niego w�o�y� komend� 'wloz do' */

    add_prop(CONT_I_WEIGHT, 10000);
    add_prop(CONT_I_MAX_WEIGHT, 20000);

    add_prop(CONT_I_DONT_SHOW_CONTENTS, 1);

    /* dodajemy sublokacj� 'na', by mo�na by�o co� na st� k�a�� */
    add_subloc("na");
}

public int
query_type()
{
	return O_MEBLE;
}

