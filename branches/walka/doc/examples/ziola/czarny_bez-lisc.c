/**
 * by Brz�zek
 * 
 * Poprawki - Krun 2007
 */

inherit "/std/herb";

#include <pl.h>
#include <herb.h>
#include <materialy.h>
#include <stdproperties.h>

void
create_herb()
{
    ustaw_nazwe_glowna_ziola("li��");
    dodaj_przym_ziola("czarny", "czarni");
    ustaw_nazwe_ziola("bez");	
	
    ustaw_nazwe("li��");

    dodaj_przym("du�y", "duzi");
    dodaj_przym("z�o�ony", "z�o�eni");

    dodaj_id_nazwy_calosci("bez");
    ustaw_id_short_calosci(({"wysoki drzewiasty krzew czarnego bzu",
        "wysokiego drzewiastego krzewu czarnego bzu", "wysokiemu drzewiastemu krzewowi czarnego bzu",
        "wysoki drzewiasty krzew czarnego bzu", "wysokim drzewiastym krzewem czarnego bzu",
        "wysokim drzewiastym krzewie czarnego bzu"}),
       ({"wysokie drzewiaste krzewy czarnego bzu", "wysokich drzewiastych krzew�w czarnego bzu",
        "wysokim drzewiastym krzewom czarnego bzu", "wysokie drzewiaste krzewy czarnego bzu",
        "wysokimi drzewiastymi krzewami czarnego bzu", "wysokich drzewiastych krzewach czarnego bzu"}),
        PL_MESKI_NOS_NZYW);
    dodaj_nazwy("krzew");
    dodaj_id_przym_calosci("wysoki", "wysocy");
    dodaj_id_przym_calosci("czarny", "czarni");
    dodaj_id_przym_calosci("drzewiasty", "drzewiasci");

    ustaw_unid_nazwe_calosci("krzew");
    dodaj_unid_przym_calosci("wysoki", "wysocy");
    dodaj_unid_przym_calosci("drzewiasty", "drzewiasci");

    set_id_long("Zielone, eliptyczne listki o nier�wnopi�kowanych brzegach, "+
        "zako�czone dosy� ostro i osadzone nieparzystopierzastosiecznie na kruchej "+
        "�ody�ce, tworz�ce z�o�ony li�� czarnego bzu pachn� niezbyt zach�caj�co. "+
        "A jednak ok�ad ze �wie�ych, roztartych li�ci zastosowany na obrz�ki, oparzenia "+
        "i obola�e stawy przynosi ulg�. Niekt�rzy medycy podaj� te� napar z kwiat�w, "+
        "li�ci i owoc�w czarnego bzu cukrzykom. \n");

    set_unid_long("Zielone, eliptyczne listki o nier�wnopi�kowanych brzegach, "+
        "zako�czone dosy� ostro i osadzone naprzeciwlegle pod pojedyncztm "+
        "li�ciem szczytowym tworz� du�y, z�o�ony li�� o niezbyt mi�ej woni \n");

    ustaw_czas_psucia(350, 400);
    ustaw_trudnosc_identyfikacji(17);
    ustaw_czestosc_wystepowania(4); 
    ustaw_sposob_wystepowania(HERBS_WYST_POJEDYNCZO);
    ustaw_ilosc_w_calosci(5);
    set_amount(1); //wartosc odzycza

    ustaw_przym_zepsutego("suchy", "suche");

    ustaw_ilosc_w_calosci(1); //FIXME

    add_prop(OBJ_I_VALUE, 10); //warto�� zio�a
    add_prop(OBJ_I_VOLUME, 10); //FIXME
    add_prop(OBJ_I_WEIGHT, 10); //FIXME    

//    ustaw_material(MATERIALY_ROSLINA_ZIELONA);
}
