inherit "/std/monster.c";

#include <wa_types.h>
#include <formulas.h>
#include <pl.h>
#include <money.h>
#include <const.h>
#include <ss_types.h>
#include <macros.h>
#include <stdproperties.h>

#define TO    this_object()
#define MIECZ "/doc/examples/bronie/miecz.c"
#define BUTY  "/doc/examples/zbroje/buty.c"
#define ZBRO "/doc/examples/zbroje/kolczuga.c"
#define SPOD "/doc/examples/zbroje/spodnie.c"
#define ZLODZIEJ_AKTYWNY "_jest_zlodziej_aktywny1"

void
create_monster()
{
    ustaw_shorty(({ "zlodziej", "zlodzieja",
       		    "zlodziejowi", "zlodzieja",
       		    "zlodziejem", "zlodzieju"}),
                 ({ "zlodzieje", "zlodziei",
                    "zlodziejom", "zlodziei", 
                    "zlodziejami", "zlodziejach" }),
                    PL_MESKI_NOS_ZYW);

    dodaj_nazwy(({ "zlodziej", "zlodzieja",
       		    "zlodziejowi", "zlodzieja",
       		    "zlodziejem", "zlodzieju"}),
                 ({ "zlodzieje", "zlodziei",
                    "zlodziejom", "zlodziei", 
                    "zlodziejami", "zlodziejach" }),
                    PL_MESKI_NOS_ZYW);


    set_long("Zlodziej prezentuje soba raczej dosc mizerny widok - poobrywane ubranie " +
	"i wystrzepiony noz swiadcza o tym, ze jego bieda zmusila go do takiego, a nie " +
	"innego sposobu zarabiania na zycie.\n");

    ustaw_odmiane_rasy(PL_CZLOWIEK);
    set_gender(G_MALE);
    add_prop(LIVE_I_NEVERKNOWN, 1);
    add_prop(CONT_I_WEIGHT, 65000); // ... 65 kilo...
    add_prop(CONT_I_HEIGHT, 175);   // ... i 175 cm wzrostu..
    add_prop(NPC_I_NO_FEAR, 1);
    add_prop(NPC_I_NO_RUN_AWAY, 1);
    set_skill(SS_DEFENCE, 25 + random(5));
    set_skill(SS_WEP_SWORD, 25 + random(10));
    set_skill(SS_UNARM_COMBAT, 15 + random(5));
    set_skill(SS_PARRY, 15 + random(10));

    set_stats(({ 8, 14, 9, 7, 8, 9}));
    add_weapon(MIECZ);
    add_armour(ZBRO);
    add_armour(BUTY);
    add_armour(SPOD);
    set_aggressive(1);
}
