inherit "/std/object.c";

#include <pl.h>
#include <stdproperties.h>
#include <macros.h>
#include <filter_funs.h>

create_object()
{
    ustaw_nazwe(({"wiadro","wiadra","wiadru","wiadro","wiadrem","wiadrze"}),
               ({"wiadra","wiader","wiadrom","wiadra","wiadrami","wiadrach"}),
                  PL_NIJAKI_NOS);
                 
    dodaj_przym("zardzewialy","zardzewiali");
    dodaj_przym("dziurawy","dziurawi");
   
    /*pierwszy argument z make_me_sitable to to, jakiego zaimka b�dziemy u�ywa� do siadania
       drugi argument to komunikat jaki pokazuje sie nam oraz obecnym na lokacji,
       trzeci to wstawanie, a ostatni to ilosc miejsc.*/
   make_me_sitable("na","na wiadrze odwracajac je denkiem do gory","z wiadra",1);
   
    set_long("Male wiadro, ktore kiedy� zdaje sie bylo srebrne, teraz "+
             "pokryte jest rdza do granic mozliwosci. "+
	     "Duza dziura z boku nie pozwala juz na dalsza jego "+
	     "eksploatacje.\n");
    add_prop(CONT_I_WEIGHT, 100);
}
