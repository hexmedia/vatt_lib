/*dziedziczymy standard object. Jesli zas chcielibysmy, przy przedmiot
  byl 'chwytywalny' to zrobilibysmy: inherit "/std/holdable_object";   */
inherit "/std/object";

#include <stdproperties.h>
#include <macros.h>
#include <pl.h>
#include <cmdparse.h>
#include <ss_types.h>
/*ustawiamy jakby 'aliasy', co by ciagle
  nie pisac "this_player()" tylko "TP" */
#define TP this_player()
#define TO this_object()

/*niektore funkcje musza byc wstepnie zadeklarowane na gorze... */
int chpomoc();
int wytrzyj(string str);
int wyczysc(string str);
int smarknij(string str);
int podaj(string str);

int czy_brudna;

/*Tworzymy obiekt. Jesli mialby to byc obiekt 'chwytywalny' to
  dziedziczylibysmy co innego (na samej gorze) a zamiast "create_object()
  wpisalibysmy: create_holdable_object()  */
void
create_object()
{
   /*To cale 'czy_brudna" mogloby z powodzeniem zostac wyrzucone...
     Tylko ogranicza niepotrzebnie, ale dla przykladu zostawiam... */
   czy_brudna = 0;
   ustaw_nazwe(({"chusteczka", "chusteczki", "chusteczce",
                 "chusteczke", "chusteczka", "chusteczce"}),
               ({"chusteczki", "chusteczek", "chusteczkom",
                 "chusteczki", "chusteczkami", "chusteczkach"}), PL_ZENSKI);
   dodaj_przym("koronkowy", "koronkowi");
   dodaj_przym("bialy", "biali");
   
   add_prop(OBJ_I_WEIGHT, 100);
   add_prop(OBJ_I_VALUE, 150);
   
   /*Tylko nie piszcie w longu niczego o pomocy!
     Ma niebyc zadnych "napisikow rogu: ?chusteczka". Co nie oznacza,
     ze pomocy do przedmiotow nie bedzie...Ale o tym troszke nizej...
     Aha, i nie biezcie przykladu z dlugosci tego longu ^_^ */
   set_long("Snieznobiala, ozdobiona koronka chusteczka.\n");
}

/* A tutaj w init ustawiamy akcje jakie ma posiadac dany przedmiot */
init()
{
   ::init();
   /*W kazdym przedmiocie, ktory ma akcje, musi byc komenda pomocy "?" !!! */
   add_action(chpomoc, "?chusteczka");
   add_action(wytrzyj, "wytrzyj");
   add_action(wyczysc, "wyczysc");
   add_action(smarknij, "smarknij");
   add_action(podaj, "podaj");
}

/*To jest wlasnie pomoc. Nie musi tak wygladac jak w tym przykladzie,
  wystarczy sama lista komend wymienionych po przecinkach.. */
int
chpomoc()
{
   write("* wytrzyj - wytarcie noska, oczu, lub raczek\n");
   write("* wyczysc - wyczyszczenie chusteczki\n");
   write("* smarknij - jesli masz za duzo nieprzyjemnosci w nosie\n");
   write("* podaj - podajesz komus chusteczke, widac, ze jej bardzo "+
         "\n          potrzebuje, skladnia: podaj chusteczke [komu]\n");
   return 1;
}

/* No, a tutaj dalej to juz same funkcje, ktore sie wywoluje add_action */

int
wytrzyj(string str)
{
   
   if(czy_brudna==1) /*jesli jest brudna to: */
   {
       write("Chyba nie bedziesz sie wycierac brudna chusteczka? Najpierw "+
             "lepiej ja 'wyczysc'.\n");
       return 1;
   }
   
   if(str != "nos" && str != "oczy" && str != "rece") /*jesli argument do
                         'wytrzyj' nie rowna sie 'nos' albo 'oczy' albo
			 'rece' to: */    
   {
       write("Co chcesz wytrzec? Nos? Oczy? A moze rece?\n");
       return 1;
   }
   
   if (str == "nos") /*jesli argument do wytrzyj rowna sie 'nos' to: */
   {
       write("Czujesz jak powoli wydobywa sie z twojego nosa nieprzyjemna "+
             "ciecz, wiec szybko przykladasz chusteczke do swojego nosa "+
             "i wycierasz to, co powoli wylatuje.\n");
       saybb(QCIMIE(TP, PL_MIA)+" wyciera nos chusteczka.\n");
       czy_brudna = 1; /* ustawia, ze chustka sie zabrudzila :) */
       return 1;
   }
   if (str == "oczy") /* a jesli "oczy" to: */
   {
       write("Do twych oczu docieraja lzy... energicznie przykladasz "+
             "do nich chusteczke i delikatnie je wycierasz.\n");
       saybb(QCIMIE(TP, PL_CEL)+" zalsnily lzy w oczach, wiec szybkim "+
             "ruchem przyklada chusteczke do oczu i delikatnie je "+
             "wyciera.\n");
       czy_brudna = 1; /* ustawia, ze chustka sie zabrudzila :) */
       return 1;
   }
   if (str == "rece") /* j.w. */
   {
       write("Spogladasz na swoje rece, i stwierdziwszy, ze sa brudne, "+
             "uzywasz chusteczki do wytarcia ich. Powoli, dokladnie "+
             "czyscisz swoje rece, kazdy paluch osobno, lewa i prawa "+
             "reke. Na chusteczce dostrzegasz male plamy brudu. "+
             "Czerwienisz sie na mysl, ze mial"+TP->koncowka("es","as")+
             " takie brudne rece.\n");
       saybb(QCIMIE(TP,PL_MIA)+" spoglada na swoje rece, po czym powoli "+
             "wyciera je w chusteczke. Po kilku chwilach na twarzy "+
             QCIMIE(TP, PL_DOP)+" pojawia sie cien rumienca.\n");
       czy_brudna = 1; /* ustawia, ze chustka sie zabrudzila :) */
       return 1;
   }
}

int
wyczysc(string str)
{
   if(czy_brudna == 0) /* jesli jest 0 to znaczy, ze jest czysta... */
   {
       write("Przeciez chusteczka jest czysta.\n");
       return 1;
   }
   if(str != "chusteczke") /* jesli przy 'wyczysc' nie dodajemy 'chusteczke' to: */
   {
       write("Co takiego chcesz wyczyscic? Chyba chusteczke, co?\n");
       return 1;
   }
   /* a jesli funkcja przejdzie przez dwa warunku (if'y) wyzej, to robi: */
   {
       write("Zauwazasz, ze chusteczka jest brudna, po czym kilkoma "+
             "energicznymi strzepnieciami pozbywasz sie pozostawionej "+
             "na chusteczce brudu. O dziwo kazdy kawalek brudu odrywa "+
             "sie od powierzchni chusteczki i odlatuje w nieznane.\n");
       saybb(QCIMIE(TP,PL_MIA)+" chwyta chusteczke za jeden z rogow "+
             "i strzepuje ja kilka razy. Chusteczka w dloni "+
             QCIMIE(TP,PL_DOP)+" wyglada teraz jak nowa.\n");
       czy_brudna = 0;
       return 1;
   }
}

             
int
smarknij(string str)
{
   if(czy_brudna == 1) /* jesli  jest brudna to: */
   {
       write("Chusteczka juz jest dosyc brudna.\n");
       return 1;
   }
   /* a jesli nie jest brudna, to znaczy, ze jest czysta:) i robi: */
   {
       write("Czujesz jak cos gestego wyplywa ci z nosa. Szybkim ruchem "+
             "przykladasz chusteczke do nosa i smarczesz w nia cicho. "+
             "Od razu lepiej.\n");
       saybb(QCIMIE(TP, PL_MIA)+" chowa nagle nos w chusteczce, po czym "+
             "slyszysz miarowy odglos smarkania. Po chwili "+
             QCIMIE(TP, PL_MIA)+" z zadowolona mina odciaga chusteczke "+
             "od nosa.\n");
       czy_brudna = 1; /* ...i ustawia, ze just jest brudna */
       return 1;
   }
}

int
podaj(string str)
{
   object *cele; /*deklarujemy 'cele' czyli osobe, ktorej chcemy podac chustke*/
   cele = PARSE_THIS (str, "'chusteczke' %l:"+PL_CEL); /*czyli musimy dac
                                            w "podaj" dwa argumenty: 'chusteczke'
					     oraz '%l" w przypadku:celownik.
					     %l czyli osoba, ktora jest celem */

   if(czy_brudna == 1)
   {
       /*to tez troszke glupie jest...Bo moge chciec dac brudna ;) */
       write("Chyba nie dasz nikomu brudnej chusteczki?\n");
       return 1;
   }
   if(sizeof(cele)==0) /* jesli 'cele' rowna sie 0, czyli jesli wpiszemy zle imie,
                          zly przypadek, lub imie osoby, ktorej nie ma na lokacji itd.*/
   {
       notify_fail("Podaj chusteczke [komu]?\n");
       return 0;
   }
   else /*czyli: jesli zadne z powyzszych if'ow to w takim razie: */
   {
       /*podajemy chusteczke 'celowi', a jego imie ma byc pokazane w komunikacie
                                         w celowniku */
       write("Podajesz chusteczke "+cele[0]->query_Imie(TP,PL_CEL)+
             ", poniewaz masz wrazenie, ze jej potrzebuje.\n");
      /* A saybb (lub say) pokazuje komunikat osobom trzecim...*/
       saybb(QCIMIE(TP, PL_MIA)+" zwinnym ruchem podaje "+
             cele[0]->query_Imie(TP,PL_CEL)+" chusteczke, poniewaz "+
             cele[0]->query_Imie(TP,PL_MIA)+" najwidoczniej potrzebowal"+
             cele[0]->koncowka("","a")+
             " jakiejs chusteczki.\n", ({ TP, cele[0] }));
       /*A celowi musimy dac jeszcze inny komunikat....*/
       cele[0]->catch_msg("Poczul"+cele[0]->koncowka("es","as")+" nagla "+
             "potrzebe chusteczki, a "+TP->query_Imie(cele[0], PL_MIA)+
             " jakby wyczul"+
             TP->koncowka("","a")+" twoja potrzebe, poniewaz podal"+
             TP->koncowka("","a")+" tobie chusteczke.\n");
       move(cele[0]); /* a na koniec 'przesuwamy' obiekt chusteczka z poprzedniego
                         wlasciciela to osoby 'cele' */
       return 1;
   }
}
