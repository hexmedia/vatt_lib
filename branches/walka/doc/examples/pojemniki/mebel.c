/*
 * du�y mebel
 * (C) 2005 by jeremian
 * 
 * przyk�adowy kod maj�cy co� wsp�lnego z sublokacjami
 * jest du�o komentarzy, wi�c powinno by� wszystko jasne
 * a jak nie - to prosz� skontaktowa� si� ze mn� na mudzie
 */

#pragma strict_types

inherit "/std/container";

#include <pl.h>
#include <macros.h>
#include <filter_funs.h>
#include <stdproperties.h>
#include <composite.h>
#include <cmdparse.h>
#include <object_types.h>

nomask void
create_container()
{
    ustaw_nazwe(({"mebel","mebla","meblowi","mebel","meblem",
    "meblu"}),({"meble","mebli","meblom","meble","meblami",
    "meblach"}),PL_MESKI_NOS_NZYW);

    dodaj_przym("du^zy","du^ze");

    /* 
     * Funkcja subloc_items zwraca opis rzeczy w danej sublokacji. Rzeczy porozdzielane
     * s� przecinkami. Je�eli chce si� samemu jaki� opis zrobi�, to mo�na na przyk�ad
     * skorzysta� z funkcji subinventory, kt�ra zwraca tablic� rzeczy w danej
     * sublokacji
     */
    set_long("Oto " + implode(query_przym(1), " ") + " " + query_nazwa() +
	    ". Mo�na wk�ada� rzeczy pod i do niego, a tak�e na nim. Pod nim jest: " +
	    "@@subloc_items|pod@@. Na nim: @@subloc_items|na@@. No a w �rodku ma: " +
	    "@@subloc_items@@.\n");
    setuid();
    seteuid(getuid());
    add_prop(CONT_I_MAX_VOLUME, 20000);
    add_prop(CONT_I_VOLUME, 16800);

    /* ten mebel mo�na dowolnie przenosi� - czemu nie? */
/*    add_prop(OBJ_I_NO_GET, "Nie da sie go podniesc.\n");*/

    add_prop(CONT_I_WEIGHT, 10000);
    add_prop(CONT_I_MAX_WEIGHT, 20000);

    /* dodajemy sublokacje */
    add_subloc("pod");
    add_subloc("na");

    /* okre�lamy limit obj�to�ci w sublokacji 'pod' */
    add_subloc_prop("pod", CONT_I_MAX_VOLUME, 500);
    
    /* okre�lamy limit wagi w sublokacji 'na' */
    add_subloc_prop("na", CONT_I_MAX_WEIGHT, 500);
    /* na meblu tylko sztylety i topory mo�na k�a�� */
    add_subloc_prop("na", SUBLOC_I_DLA_O, O_BRON_SZTYLETY | O_BRON_TOPORY);
    
    /* sami opisujemy kontener */
    add_prop(CONT_I_DONT_SHOW_CONTENTS, 1);
}

public int
query_type()
{
	    return O_MEBLE;
}

