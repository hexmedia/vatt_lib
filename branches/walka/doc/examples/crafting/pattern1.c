/**
 * \file /doc/examples/crafting/pattern1.c
 *
 * Plik z przykładowym wzorcem koronkowych majteczek.
 */

inherit "/std/pattern";

#include <pattern.h>

public void
create()
{
  set_pattern_domain(PATTERN_DOMAIN_TAILOR);
  set_item_filename("/doc/examples/ubrania/gacie/czerwone_koronkowe_M.c");
  set_estimated_price(800);
  set_time_to_complete(30);
  enable_want_sizes();
}
