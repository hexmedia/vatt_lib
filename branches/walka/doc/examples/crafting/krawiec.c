#pragma strict types
inherit "/std/monster";
inherit "/std/act/action";
inherit "/std/act/trigaction.c";
inherit "/lib/craftsman";

#include <macros.h>
#include <stdproperties.h>
#include <money.h>
#include <pl.h>
#include <formulas.h>
#include <std.h>
#include <ss_types.h>
#include <wa_types.h>
#include <composite.h>
#include <filter_funs.h>
#include <pattern.h>

#include "dir.h"

void
create_monster()
{
    ustaw_odmiane_rasy("m�czyzna");
    dodaj_nazwy("krawiec");
    set_gender(G_MALE);
    set_title(", Krawiec");
    set_long("D�ugi opis przyk�adowego krawca.\n");
    dodaj_przym("wysoki","wysocy");
    dodaj_przym("jasnow�osy","jasnow�osi");


    set_stats ( ({ 74, 41, 78, 21, 30, 84 }) );
    set_skill(SS_DEFENCE, 50 + random(4));
    set_skill(SS_WEP_CLUB, 33 + random(10));
    set_skill(SS_PARRY, 10 + random(10));
    set_skill(SS_UNARM_COMBAT, 55 + random(15));
    
    add_prop(NPC_I_NO_RUN_AWAY, 1);
    add_prop(CONT_I_WEIGHT, 91000);
    add_prop(CONT_I_HEIGHT, 172);

    set_skill(SS_CRAFTING_TAILOR, 80);
    config_default_craftsman();
    craftsman_set_domain(PATTERN_DOMAIN_TAILOR);
    craftsman_set_sold_item_patterns( ({
                PATH + "pattern1.c",
                PATH + "pattern2.c"
                }) );
}

void
init()
{                                                                          
    ::init();
    craftsman_init();
}

public string
query_auto_load()
{
    return ::query_auto_load() + query_craftsman_auto_load();
}

public string
init_arg(string arg)
{
    if (arg == 0) { // obiekt jest tworzony od zera - NIE jest odtwarzany
        clone_object(PATH + "nozyczki")->move(this_object());
        clone_object(PATH + "sk_cieleca")->move(this_object());
        force_expand_patterns_items();
    }
    return init_craftsman_arg(::init_arg(arg));
}
