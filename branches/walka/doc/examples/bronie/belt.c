inherit "/std/pocisk";

#include <wa_types.h>
#include <pl.h>

public void
create_pocisk()
{
        ustaw_nazwe( ({"be^lt", "be^lta", "be^ltowi", "be^lt", "be^ltem",
"be^lcie"}),
                ({"be^lty", "be^ltow", "be^ltom", "be^lty", "be^ltami",
"be^ltach"}),
                PL_MESKI_NOS_NZYW);
   dodaj_przym("d^lugi", "d^ludzy");
   set_long("Prosty, drewniany be^lt.\n");
   set_jakosc(5);
   set_typ(W_IMPALE);
}