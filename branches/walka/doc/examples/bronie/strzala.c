inherit "/std/pocisk";

#include <wa_types.h>
#include <pl.h>

public void
create_pocisk()
{
   ustaw_nazwe("strzala");
   dodaj_przym("d^lugi", "d^ludzy");
   set_long("Prosta, drewniana strza^la.\n");
   set_jakosc(5);
   set_typ(W_IMPALE);
}