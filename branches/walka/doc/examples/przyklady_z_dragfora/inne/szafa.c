inherit "/std/receptacle";

#include <pl.h>
#include <stdproperties.h>

create_receptacle()
{
    ustaw_nazwe( ({"szafa","szafy","szafie","szafe","szafa","szafie"}), 
                 ({"szafy","szaf","szafom","szafy","szafami","szafach"}), PL_ZENSKI);
		 
    dodaj_przym("obszerny","obszerni");

    set_long("Jest to zrobiona z debowego drewna, dwudrzwiowa szafa.\n");
    
     add_prop(CONT_I_RIGID, 1);
     add_prop(CONT_I_MAX_VOLUME, 240000);
     add_prop(CONT_I_MAX_WEIGHT, 100000);
     add_prop(CONT_I_VOLUME, 168000);
     add_prop(CONT_I_WEIGHT, 50000);
     add_prop(OBJ_I_NO_GET, 1);
}
    

