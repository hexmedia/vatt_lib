inherit "/std/receptacle.c";

#include <stdproperties.h>
#include <pl.h>

create_container()
{
    set_long("Obserwujesz wlasnie wykonany ze skory woreczek sluzacy do przechowywania jedynie "
      + "ziol jakie zebrac mozna w lasach czy na traktach. Prawdopodobnie uzywaja takowych "
      + "jedynie doswiadczoni zielarze, aby zebrane przez nich korzenie, liscie czy kwiaty nie "
      + "uschly zanim nie powroca do swoich chatek.\n");


    ustaw_nazwe( ({ "woreczek", "woreczka", "woreczkowi", "woreczek", "woreczkiem", "woreczku" }), 
                 ({ "woreczki", "woreczkow", "woreczkom", "woreczki", "woreczkami", "woreczkach" }) , PL_MESKI_NOS_NZYW);
        
    dodaj_przym("nieduzy","nieduzi");
    dodaj_przym("skorzany","skorzani");
    
	add_prop(OBJ_I_VALUE, 70);
    add_prop(CONT_I_MAX_WEIGHT, 1000);
    add_prop(CONT_I_MAX_VOLUME, 1000);
    add_prop(CONT_I_RIGID,1);
    add_prop(CONT_I_TRANSP,1);
    
    
}
int
prevent_enter(object ob)
{
  if (function_exists("create_object",ob) == "/std/herb")
     { 
         return 0; 
     }
  return 1;
}         
void
enter_inv(object ob, object from)
{
  ::enter_inv(ob, from);
  ob->stop_decay();
}
void
leave_inv(object ob, object to)
{
  ::leave_inv(ob, to);
  ob->restart_decay();
}  

