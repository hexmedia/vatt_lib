#pragma strict types

inherit "/std/monster";

#include <macros.h>
#include <stdproperties.h>
#include <language.h>
#include <composite.h>
#include <money.h>
#include <pl.h>
#include <time.h>
#include <formulas.h>
#include <std.h>
#include "../definicje.h"
#include <filter_funs.h>

int *umy = ({40, 40, 40});
int f_filter(object ktory);

void
create_monster()
{
    ustaw_imie(({ "kelddath", "kelddatha", "kelddathowi", "kelddatha", "kelddathem", "kelddathcie"}), PL_MESKI_OS);
	
	dodaj_nazwy(PL_MEZCZYZNA);
    
	set_title("Ormlyr, Najwyzszy kaplan Lathandera");
    
	dodaj_przym("ascetyczny","ascetyczni");
    dodaj_przym("czarnowlosy","czarnowlosi");
    
	ustaw_odmiane_rasy(PL_MEZCZYZNA);
    
	set_stats( ({ 42, 43, 45, 76, 81, 83 }) );
    
	set_gender(G_MALE);
    
	add_prop(CONT_I_WEIGHT, 59000); 
    add_prop(CONT_I_VOLUME, 35000); 
    add_prop(CONT_I_HEIGHT, 178);
    
	set_act_time(180);
    add_act("emote spoglada na ciebie bystro.");
    add_act("zmarszcz czolo");
    add_act("hmm");
    add_act("emote odprawia tajemniczy rytual.");
    add_act("emote wznosi modly do Lathandera.");
    add_act("emote wykonuje uklon w strone posagu.");
	add_act("emote odspiewuje chwalebny hymn ku bostwu poranka.");
    add_act("emote porzadkuje kadzidla w okolicach oltarza.");
	add_act("emote zastanawia sie nad czyms gleboko.");

    set_chat_time(300);
    add_chat("Jedyne o co naprawde dbam to Beregost i jego bezpiecznenstwo.");
	add_chat("Ufam, ze nie bedziesz wszczynal bojek w Beregoscie.");
	add_chat("Wiele podrozowalem i wiele tez sie nauczylem. Wedrowki buduja charakter.");
    
	set_cchat_time(60);
    add_cchat("Zaprzestan ataku bo bede zmuszony cie zabic!");
    add_cchat("Wiesz cozes uczynil glupcze?!");
    add_cchat("Sciagasz na siebie gniew Lathandera.");
	add_cchat("Boski Lathanderze! Wspomoz mnie w walce!");

	set_cact_time(30);
	add_cact("emote uderza z calej sily wekiera o ziemie.");
	add_cact("emote unika kazdego ciosu efektownie po czym wyprowadza wlasny atak.");


	set_default_answer(VBFC_ME("default_answer"));
  
    set_long("Jest to nieco wyniosly mezczyzna o ascetycznej postawie. Jak sadzisz "
	  + "moze byc to nie kto inny jak sam Kelddath Ormlyr, najwyzszy kaplan Lathandera "
	  + "i jedyny burmistrz Beregostu. Mowi sie o tym czlowieku, ze jego upragnionym "
	  + "celem jest zapewnienie w miasteczku calkowitego bezpieczenstwa, a przez to "
	  + "zwiekszenie oplacalnosci handlu oraz transportu. Z checia pozycza on pieniadze "
	  + "kupcom, zaklada coraz to wieksze gospody i karczmy, a takze wysyla patrole, "
	  + "ktore utrudniaja ewentualnym zlodziejom penetrowanie okolicznych ruin. To wszystko "
	  + "musi go kosztowac sporo wysilku i nerwow, gdyz jego fryzura gdzieniegdzie przypruszona "
	  + "jest siwizna poczciwca. Mimo to prawdopodobnie sprawiedliwie i madrze sprawuje on "
	  + "wladze w miescie i za to jest jego najbardziej szanowanym mieszczkancem.\n");
    set_liv_name("_arcykaplan_z_beregostu_");
	set_skill(SS_DEFENCE, umy[0] + random(40));
    set_skill(SS_PARRY, umy[1] + random(30));
    set_skill(SS_WEP_CLUB, umy[2] + random(20));

    add_weapon(WEAPONS_PATH + "wekiera");
    add_armour(ODZIENIE_PATH + "sandaly");
     add_armour(ARMOUR_PATH + "buty");
    add_armour(ARMOUR_PATH + "kolczuga1");
}
void
attacked_by (object ob)
{
	object *obecni;

	obecni = all_inventory(environment(this_object())); 

	obecni = filter(obecni, f_filter); 

	obecni -= ({this_object()}); 

	obecni->command("powiedz Lathanderze! Wspomoz mnie w walce!");  

	obecni->attack_object(ob); 
}
int
f_filter(object ktory)
{
	if((ktory->query_rasa() == "straznik swiatynny") || (ktory->query_rasa() == "straznik")) 
		return 1;                         
	return 0;                          
}
void
add_introduced(string imie_mia, string imie_bie)
{
	set_alarm(2.0, 0.0, "return_introduce", find_player(imie_mia));
}
void
return_introduce(object ob)
{
    command("przedstaw sie " + OB_NAME(ob));
    command("powiedz Witaj w przybytku Lathandera wedrowcze.");
	set_alarm(2.0,0.0, "przedstawienie");
}
string
default_answer()
{
	command("wzrusz ramionami");
}
void
emote_hook(string emote, object wykonujacy, string przyslowek)
{
    switch (emote)
    {
        case "kopnij":
        case "spoliczkuj" :
        case "szturchnij":
        case "opluj" : set_alarm(2.0, 0.0, "zly", wykonujacy); break;
        	    
        case "zasmiej": 
        case "pocaluj":
        case "przytul": set_alarm(2.0, 0.0, "dobry", wykonujacy);
    }
}



void
zly(object kto)
{
    switch (random(2)) 
    {
		case 0 : command("powiedz Sciagasz na siebie gniew bozy."); break;
		case 1 : command("powiedz Samokontrola to podstawa wspolzycia z innymi.");
    default:
    }
}

void
dobry(object kto) 
{
        
    if(kto->koncowka("chlop","baba")=="chlop") zly(kto);
    else
    {   
        switch (random(1)) 
        {
          case 0 : command("emote pozostaje niewzruszony."); break;      
        }
      }
}
void
przedstawienie(object pla)
{
	command("powiedz Zawsze bedziesz tu mile widziany.");
	command("emote blogoslawi cie kilkoma zwinnymi ruchami.");
} 

