#include "../pal.h"

inherit "/std/monster";

void create_monster()
{
	int mlot_on;
	
    ustaw_imie(({"dalean","daleana","daleanowi","daleana","daleanem","daleanie"}),
                PL_MESKI_OS);
    set_title("Thigr z Uthgardu, Kupiec z Palanthasu");

    ustaw_odmiane_rasy(
    ({"polork","polorka","polorkowi","polorka","polorkiem","polorku"}),
    ({"polorki","polorkow","polorkom","polorkow","polorkami","polorkach"}),
    PL_MESKI_NZYW
    );

    set_gender(G_MALE);
    
    set_long("Dosc wysoki i niezle umiesniony osobnik stojacy przed toba jest "
    +"jednym z najdziwniejszych polaczen jakie udalo ci sie zobaczyc. Ma ciemniejszy "
    +"niz czlowiek kolor skory i jest od przecietnego czlowieka duzo wyzszy i "
    +"tezszy. Jego twarz bardzo przypomina orcza, jednak jego gesty i zachowanie "
    +"swiadcza o calkowicie ludzkim wychowaniu. Bez watpienia jest on polorkiem.\n");

    dodaj_przym("wielki","wielcy");
    dodaj_przym("ciezki","ciezcy");

        set_stats(({100,25,90,20,20,95}));

        set_cact_time(4);
        add_cact("krzyknij Aaaaaargh!");
        add_cact("krzyknij Myslisz ze ze mna wygrasz?!");
        add_cact("krzyknij Twa smierc przybrala postac Daleana z Uthgardu!");

        set_act_time(10 + random(20));
        add_act("emote rozglada sie po sklepie.");
        add_act("emote pokazuje klientowi piekny kirys.");
        add_act("emote przeciaga sie poteznie, az chrupia jego grube kosci.");        

    set_skill(SS_DEFENCE, 60);
    set_skill(SS_PARRY, 75);
    set_skill(SS_WEP_WARHAMMER,90);
    
    add_armour(ZBR+"koszula.c");
    add_armour(ZBR+"spodnie.c");

    set_alignment(50); 
    mlot_on=0; 
}

void add_introduced(string kto)
{
     set_alarm (2.0,0.0,"intro",find_player(kto));
}

void intro(object ob)
{
    if (objectp(ob)) 
        command ("przedstaw sie "+OB_NAME(ob));
}

public void bron_sie()
{
    int mlot_on;
    command("emote kuca, jakby chcial wykonac unik, po czym pojawia sie z mlotem w dloni!"); 
    if (mlot_on==0)
    {
    	(clone_object(BRN+"mlot_dalka.c")->move(TO));
    	mlot_on=1;
    }
    command("dobadz mlota");
    command("krzyknij Za Uthgard!");
}


void attacked_by(object wrog)
{
    command("emote usmiecha sie dziwacznie.");
    set_alarm(0.1, 0.0, "bron_sie");
    return ::attacked_by(wrog);
}
