#pragma strict types

inherit "/std/monster";

#include <macros.h>
#include <stdproperties.h>
#include <language.h>
#include <composite.h>
#include <money.h>
#include <pl.h>
#include <time.h>
#include <formulas.h>
#include <std.h>
#include "../definicje.h"
#include <filter_funs.h>

int *umy = ({40, 40, 40, 40, 40, 40});

void
create_monster()
{
    ustaw_imie(({ "scalf", "scalfa", "scalfowi", "scalfa", "scalfem", "scalfie"}), PL_MESKI_OS);
	
	dodaj_nazwy(PL_MEZCZYZNA);
    
	set_title("Syn Righorna");
    
	dodaj_przym("hardy","hardzi");
    dodaj_przym("kruczobrody","kruczobrodzi");
    
	ustaw_odmiane_rasy(PL_KRASNOLUD);
    
	set_stats( ({60,35,75,30,35,80}) );
    
	set_gender(G_MALE);
    
	add_prop(CONT_I_WEIGHT, 10000); 
    add_prop(CONT_I_VOLUME, 35000); 
    add_prop(CONT_I_HEIGHT, 125);
    
	set_act_time(360);
    add_act("emote rozglada sie czujnie.");
    add_act("emote wypatruje czegos uwaznie w oddali.");

    set_chat_time(500);
    add_chat("Do kresu wedrowki tak jeszcze daleko.");

    set_cchat_time(60);
    add_cchat("Nie wiesz z kim masz do czynienia " + this_player()->koncowka("chlopcze","dziewczynko") + ".");
    add_cchat("Sproboj jeszcze raz. Dlugo juz nie przyjdzie ci zyc.");
	add_cchat("Zmow modlitwe do bogow bys " + this_player()->koncowka("zginal","zginela") + " szybko.");

	set_cact_time(50);
	add_cact("emote unosi swoj topor w gore.");

	set_default_answer(VBFC_ME("default_answer"));
  
    set_long("Jest to krasnolud niskiego wzrostu o bardzo hardym wyrazie twarzy. "
	  + "Jego dumnie i majestatycznie wygladajaca, krucza broda zapleciona jest "
	  + "w dlugie pojedyncze warkoczyki. Czujne i badawcze spojrzenie przenika "
	  + "cie zglebiajac wszystkie twe tajemnice i sekrety. Co do budowy tego "
	  + "osobnika to cieszy sie on duza szerokoscia barek i rownie muskularnych "
	  + "konczynach. Sadzisz, ze podobnie jak wielu podroznikow i ten tula sie "
	  + "po swiecie prowadzac niekonczaca wloczege, ktora jednakze zaczal juz "
+ "zapewne bardzo dawno temu i uchodzi juz za doswiadczonego podroznika.\n");

	set_skill(SS_DEFENCE, umy[0] + random(40));
    set_skill(SS_PARRY, umy[1] + random(30));
    set_skill(SS_WEP_AXE, umy[2] + random(20));
	set_skill(SS_CLIMB, umy[3] + random(20));
	set_skill(SS_BLIND_COMBAT, umy[4] + random(20));
	set_skill(SS_AWARENESS, umy[5] + random(20));

    set_liv_name("_krasnoludzki_wedrowiec_");

    add_weapon(WEAPONS_PATH + "topor");
    add_armour(ARMOUR_PATH + "helm");
    add_armour(ARMOUR_PATH + "buty");
    add_armour(ARMOUR_PATH + "zbroja_paskowa");
	add_armour("/d/Faerun/Miasta/crimmor/ubrania/plaszcz");
	add_armour("/d/Faerun/Miasta/crimmor/ubrania/pas");

	set_alarm(3.0,0.0, "idz");
                       
}
void
add_introduced(string imie_mia, string imie_bie)
{
	set_alarm(2.0, 0.0, "return_introduce", find_player(imie_mia));
}
void
return_introduce(object ob)
{
    command("przedstaw sie " + OB_NAME(ob));
    command("powiedz W czyms ci pomoc " + this_player()->koncowka("brachu","panienko") + "?");
}
string
default_answer()
{
	command("zaprzecz");
}
void
emote_hook(string emote, object wykonujacy, string przyslowek)
{
    switch (emote)
    {
        case "kopnij":
        case "spoliczkuj" :
        case "szturchnij":
        case "opluj" : set_alarm(2.0, 0.0, "zly", wykonujacy); break;
        	    
        case "zasmiej": 
        case "pocaluj":
        case "przytul": set_alarm(2.0, 0.0, "dobry", wykonujacy);
    }
}



void
zly(object kto)
{
    switch (random(2)) 
    {
		case 0 : write("Krasnolud uderza cie z calej sily prosto w zoladek.\n");
		         say("Krasnolud uderza " + this_player()->short(PL_BIE) + " z "
				   + "calej sily prosto w zoladek\n"); 
				 clone_object(PATH+"paraliz1.c")->move(this_player()); break;
		case 1 : command("powiedz Zjezdzaj gnoju pokim mily!");
    default:
    }
}

void
dobry(object kto) 
{
        
    if(kto->koncowka("chlop","baba")=="chlop") zly(kto);
    else
    {   
        switch (random(1)) 
        {
          case 0 : command("emote pozostaje niewzruszony."); break;      
        }
      }
}
void
idz()
{
	command("polnoc");
	set_alarm(20.0,0.0, "idz1");
}
void
idz1()
{
	command("polnoc");
	set_alarm(20.0,0.0, "idz2");
}
void
idz2()
{
	command("zachod");
	set_alarm(20.0,0.0, "idz3");
}
void
idz3()
{
	command("zachod");
	set_alarm(20.0,0.0, "idz4");
}
void
idz4()
{
	command("polnoc");
	set_alarm(20.0,0.0, "idz5");
}
void
idz5()
{
	command("polnocny-zachod");
	set_alarm(20.0,0.0, "idz6");
}
void
idz6()
{
	command("zachod");
	set_alarm(20.0,0.0, "idz7");
}
void
idz7()
{
	command("zapukaj we wrota");
	set_alarm(9.0,0.0, "idz8");
}
void
idz8()
{
	command("zachod");
	set_alarm(20.0,0.0, "idz9");
}
void
idz9()
{
	command("polnocny-zachod");
	set_alarm(20.0,0.0, "idz10");
}
void
idz10()
{
	command("polnocny-zachod");
	set_alarm(20.0,0.0, "idz11");
}
void
idz11()
{
	command("zachod");
	set_alarm(20.0,0.0, "idz12");
}
void
idz12()
{
	command("zachod");
	set_alarm(20.0,0.0, "idz13");
}
void
idz13()
{
	command("polnocny-zachod");
	set_alarm(20.0,0.0, "idz14");
}
void
idz14()
{
	command("polnocny-zachod");
	set_alarm(20.0,0.0, "idz15");
}
void
idz15()
{
	command("zachod");
	set_alarm(20.0,0.0, "idz16");
}
void
idz16()
{
	command("popatrz ciekawie na drzewo");
	command("zachod");
	set_alarm(20.0,0.0, "idz17");
}
void
idz17()
{
	command("polnocny-zachod");
	set_alarm(20.0,0.0, "idz18");
}
void
idz18()
{
	command("polnocny-zachod");
	set_alarm(20.0,0.0, "idz19");
}
void
idz19()
{
	command("polnoc");
	set_alarm(20.0,0.0, "idz20");
}
void
idz20()
{
	command("popatrz uwaznie na obelisk");
	set_alarm(20.0,0.0, "idz21");
}
void
idz21()
{
	command("zachod");
	set_alarm(20.0,0.0, "idz22");
}
void
idz22()
{
	command("zachod");
	set_alarm(20.0,0.0, "idz23");
}
void
idz23()
{
	command("zachod");
	set_alarm(20.0,0.0, "idz24");
}
void
idz24()
{
	command("zachod");
	set_alarm(20.0,0.0, "idz25");
}
void
idz25()
{
	command("polnocny-zachod");
	set_alarm(20.0,0.0, "idz26");
}
void
idz26()
{
	command("polnocny-zachod");
	set_alarm(20.0,0.0, "idz27");
}
void
idz27()
{
	command("zachod");
	set_alarm(20.0,0.0, "idz28");
}
void
idz28()
{
	command("powiedz Doprawdy ciekawe sa te glazy.");
	set_alarm(20.0,0.0, "idz29");
}
void
idz29()
{
	command("zachod");
	set_alarm(20.0,0.0, "idz30");
}
void
idz30()
{
	command("zachod");
	set_alarm(20.0,0.0, "idz31");
}
void
idz31()
{
	command("zachod");
	set_alarm(20.0,0.0, "idz32");
}
void
idz32()
{
	command("polnocny-zachod");
	set_alarm(20.0,0.0, "idz33");
}
void
idz33()
{
	command("polnocny-zachod");
	set_alarm(20.0,0.0, "idz34");
}
void
idz34()
{
	command("zachod");
	set_alarm(20.0,0.0, "idz35");
}
void
idz35()
{
	command("zachod");
	set_alarm(20.0,0.0, "idz36");
}
void
idz36()
{
	command("zachod");
	set_alarm(7.0,0.0, "idz37");
}
void
idz37()
{
	command("emote podaza na wschod.");
	move_object("/d/Faerun/Miasta/beregost/lokacje/ulica12");
	set_alarm(3.0,0.0, "idz");
}


