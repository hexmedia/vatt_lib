#include <macros.h>
#include <stdproperties.h>
#include <ss_types.h>
#include <tasks.h>
#include <money.h>


object *przedstawieni = ({});


void
create_monster()
{
    ustaw_imie(({"plackarz", "plackarza", "plackarzowi", "plackarza", 
                 "plackarzem", "plackarzu"}), PL_MESKI_OS);

    set_title("plackarz");

    ustaw_odmiane_rasy(PL_MEZCZYZNA);

    dodaj_przym("garbaty", "garbaci");
    dodaj_przym("brzydki", "brzydki");

    set_long("Ugniatajac ciasto, niziolek ten zapewne pracuje."
           + "Mimo pochloniecia praca, bacznie lustruje przechodzacych. "
           + "Lekka lysina swiadczy o tym, ze jest w podeszlym wieku. "
           + "Popijajac czasami troche soku zaspokaja zmeczenie. "
           + "Znajdujacy sie w pochwie sztylet, sluzy do obrony. "
           + "Niewielka blizna znajdujaca sie na jego czole oznacza "
           + "zapewne, ze stoczyl jakas krwawa bojke.  "
           + "Duzy brzuszek oznacza, ze nie zaznal glodu  "
           + ".\n");

    set_gender(G_MALE);

    add_prop(CONT_I_WEIGHT, 61000);
    add_prop(CONT_I_HEIGHT, 168);

    set_stats(({55, 70, 65, 20, 15, 50}));

    set_skill(SS_WEP_KNIFE, 40);

    set_alignment(50);

    set_act_time(10 + random(30));
    add_act("podrap sie");
    add_act("emote wydaje z siebie dlugie sapniecie.");
    add_act("emote usmiecha sie tepo.");
    add_act("emote lustruje wszystkich wzrokiem.");
    add_act("emote rozglada sie uwaznie.");
    add_act("ziewnij");
    add_act("westchnij");

    set_cact_time(5 + random(15));
    add_cact("emote kosztuje kawalek placka");
    add_cact("emote wsadza ciasto do pieca.");
    add_cact("emote glosno sapie.");
    add_cact("splun");
    add_cact("zazgrztaj");
    add_cact("emote krzywi sie.");

    set_chat_time(10 + random(27));
    add_chat("Echh... Ciezka praca");
    add_chat("Rzucilbym ta robote, ale... "
    add_chat("Ciezkie jest zycie plackarza");
    add_chat("Potrzebuje pomocnikaaa!");

    set_default_answer(VBFC_ME("default_answer"));
        
    MONEY_ADD(this_object(), 100);
}

string
default_answer()
{
    command("powiedz Eee... Nie wiem co masz na mysli");
    command("emote spoglada na boki z zaklopotaniem");

    return "";
}

void
add_introduced(string imie_mia, string imie_bie)
{
    set_alarm(2.0, 0.0, "return_introduce", find_player(imie_mia));
}

void
return_introduce(object ob)
{
    if (ob && environment(ob) == environment())
        if (member_array(ob, przedstawieni -= ({0})) != -1)
        {
            command("powiedz Eee... Czesc, " + ob->query_wolacz() + ".");
            command("usmiechnij sie do " + OB_NAME(ob));
        }
        else
        {
            przedstawieni += ({ob});
            command("przedstaw sie " + OB_NAME(ob));
         
        }
}



