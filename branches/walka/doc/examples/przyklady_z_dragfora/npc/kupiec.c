/*
 * Filename: kupiec.c
 *
 * Zastosowanie: Npc do skladu gospody w Beregoscie.
 * 
 * Copyright Volothamp Geddarm.
 *
 */
inherit "/std/monster.c";

#include <macros.h>
#include <stdproperties.h>
#include <language.h>
#include <composite.h>
#include <money.h>
#include <pl.h>
#include <time.h>
#include <formulas.h>
#include <std.h>
#include <mudtime.h>
#include "../definicje.h"

int *staty = ({10, 10, 10, 10, 10, 10});
int *umy = ({10, 10, 10});

int f_filter(object ktory);

    string *
    losowy_przym()
{
    string *lp;
    string *lm;
	string *lpa;
	string *lma;
    int i, j;
    lp=({ "niski", "krzaczastobrewy", "wesoly", "milczacy"});
    lm=({ "niscy", "krzaczastobrewi", "weseli", "milczacy"});
	lpa=({ "pekaty", "barylkowaty", "otyly", "sennooki"});
	lma=({ "pekaci", "barylkowaci", "otyli", "sennoocy"});
	i = random(4);
	j = random(4);
	return ({ lp[i] , lm[i], lpa[j], lma[j] });
}
    void
    create_monster()
{
    string *przym = losowy_przym();
	ustaw_odmiane_rasy( ({"kupiec", "kupca", "kupcowi", "kupca", "kupcem", "kupcu" }),
		                ({"kupcy", "kupcow", "kupcom", "kupcow", "kupcami", "kupcach" }), PL_MESKI_OS);
    set_gender(G_MALE);
	dodaj_nazwy(PL_MEZCZYZNA);
    
	dodaj_przym( przym[0], przym[1] );
    dodaj_przym( przym[2], przym[3] );
    
	set_long("Oto jeden z czesto odwiedzajacych Beregost kupcow, ktorych cale zycie "
	  + "opiera sie na przemierzaniu traktow wraz ze swymi karawanami i sprzedawaniu "
	  + "przeroznych towarow za uczciwe ceny. Ten osobnik wyglada na nieco podpitego, "
	  + "totez zapewne nie warto z nim rozmawiac o czymkolwiek, a o dziwo nawet o "
	  + "interesach. Mimo jego aktualnego stanu dostrzegasz w nim nadal te mocna "
	  + "kamienna twarz, a takze inne cechy jakie konieczne sa kazdemu kupcowi.\n");
    
	add_prop(LIVE_I_NEVERKNOWN, 1);
    add_prop(CONT_I_WEIGHT, 80000);
    add_prop(CONT_I_HEIGHT, 170);
    
	set_stats(({staty[0] + random(20), staty[1] + random(20), staty[2] + random(10), staty[3] + random(5), staty[4] + random(5), staty[5] + random(40)}));
    
	set_skill(SS_DEFENCE, umy[0] + random(10));
    set_skill(SS_PARRY, umy[1] + random(10));
    set_skill(SS_UNARM_COMBAT, umy[2] + random(10));
    
	set_cchat_time(20 + random(30));
    add_cchat("Napadnieto mnie.");
    add_cchat("Pomocy. Dajcie jeszcze wodki.");

	set_cact_time(25 + random(30));
	add_cact("emote odgania cie od siebie wymachujac rekami.");
	add_cact("emote krzyczy cos niezrozumiale, a alkohol doslownie zionie z jego ust.");

	set_act_time(40 + random(20));
	add_act("ziewnij.");
	add_act("czknij");
	add_act("emote zastanawia sie nad interesami po pijaku.");
	add_act("ziewnij.");
	add_act("emote zamawia jeszcze jeden kieliszek.");
	add_act("emote zastanawia sie nad praca.");

	set_chat_time(55 + random(20));
	add_chat("Co mowiles? Nic nie slysze.");
	add_chat("Nie ma jak dobre winko i cichy rog w gospodzie.");
	add_chat("Po ciezkim dniu nie dziwie sie sobie, ze tu sie udalem.");

    add_armour(ODZIENIE_PATH + "kapelusz1");
    add_armour(ODZIENIE_PATH + "buty");
     add_armour(ODZIENIE_PATH + "kaftan");

	set_default_answer(VBFC_ME("default_answer"));
}
void
attacked_by (object ob)
{
	object *obecni;

	obecni = all_inventory(environment(this_object())); 

	obecni = filter(obecni, f_filter); 

	obecni -= ({this_object()}); 

	obecni->command("krzyknij Brac go!");  

	obecni->attack_object(ob); 
}



int
f_filter(object ktory)
{
	if((ktory->query_rasa() == "gwardzista") || (ktory->query_rasa() == "straznik")) 
		return 1;                         
	return 0;                          
}
    string
	default_answer()
{
	command("emote przyglada ci sie spod zmruzonych powiek.");
    set_alarm(3.0, 0.0, "zdziwienie");
}
    void
    emote_hook(string emote, object wykonujacy, string przyslowek)
{
    switch (emote)
    {
        case "kopnij": set_alarm(2.0, 0.0, "zly", wykonujacy);
        	    break;
        case "spoliczkuj" :
        case "szturchnij":
        case "opluj" : set_alarm(2.0, 0.0, "zly", wykonujacy);
        	    break;
        	    
        case "zasmiej": 
        case "pocaluj": set_alarm(2.0, 0.0, "dobry", wykonujacy);
        	      break;
        case "przytul": set_alarm(2.0, 0.0, "dobry", wykonujacy);
    }
}



void
zly(object kto)
{
    switch (random(5)) 
    {
        
    case 0 : command("krzyknij Co to za zachowanie?!"); break;
    case 1 : command("powiedz do " + kto->short(PL_DOP) + " Ty. Ja zloze na cienie skarge!"); break;
    case 2 : command("powiedz do " + kto->short(PL_DOP) + " To zachowanie jest co najmniej... Bezczelne!"); break;
    default:
    }
}

void
dobry(object kto) 
{
        
    if(kto->koncowka("chlop","baba")=="chlop") zly(kto);
    else
      {
        
    
        switch (random(3)) 
        {
          case 0 : command("powiedz Ja... Nie wiem co powiedziec."); break;
          
        }
      }
} 
void
zdziwienie(object pla)
{
    command("emote mowi, a z jego ust zionie alkohol: Nie wiem o co ci chodzi... Zlotko.");
}

/*----Podpisano Volothamp Geddarm----*/



