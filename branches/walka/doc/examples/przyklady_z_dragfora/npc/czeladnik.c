/*
 * Filename: czeladnik.c
 *
 * Zastosowanie: Czeladnik z kuzni.
 * 
 * Copyright Volothamp Geddarm.
 *
 */
inherit "/std/monster.c";

#include <macros.h>
#include <stdproperties.h>
#include <language.h>
#include <composite.h>
#include <money.h>
#include <pl.h>
#include <time.h>
#include <formulas.h>
#include <std.h>
#include <mudtime.h>
#include "../definicje.h"

int *staty = ({10, 10, 10, 10, 10, 10});
int *umy = ({10, 10, 10});

int f_filter(object ktory);

    string *
    losowy_przym()
{
    string *lp;
    string *lm;
	string *lpa;
	string *lma;
    int i, j;
    lp=({ "mlody", "bardzo mlody", "barczysty", "smukly", "niski"});
    lm=({ "mlodzi", "bardzo mlodzi", "barczysci", "smukli", "niscy"});
	lpa=({ "zdolny", "energiczny", "jasnowlosy", "nerwowy", "wielkonosy"});
	lma=({ "zdolni", "energiczni", "jasnowlosi", "nerwowi", "welkonosi"});
	i = random(5);
	j = random(5);
	return ({ lp[i] , lm[i], lpa[j], lma[j] });
}
    void
    create_monster()
{
    string *przym = losowy_przym();
	ustaw_odmiane_rasy( ({"czeladnik", "czeladnika", "czeladnikowi", "czeladnika", "czeladnikiem", "czeladniku" }),
		                ({"czeladnicy", "czeladnikow", "czeladnikom", "czeladnikow", "czeladnikami", "czeladnikach" }), PL_MESKI_OS);
    set_gender(G_MALE);
	dodaj_nazwy(PL_MEZCZYZNA);
    
	dodaj_przym( przym[0], przym[1] );
    dodaj_przym( przym[2], przym[3] );
    
	set_long("Jest to zapewne jeden ze zdolnych uczniow kowala Taeroma 'Grzmiacego Mlota'. "
+ "Jak sadzisz umie on wykuc juz wiele przydatnych przedmiotow, nie sa to jednak bronie lecz "
	  + "mniejsze zamki, zawiasy czy kasetki. Prawdopodobnie juz niedlugo ow mlodzieniec "
	  + "biegly i obeznany w rzemieslniczym kunszcie przystapi do egzaminu zakanczajacego "
+ "praktyke. Wyglada on nawet hardo, zas w jego oczach dostrzegasz plomien mlodzienczego "
	  + "zapalu i fascynacji. Muskularna i ksztaltna sylwetka kwalifikuje go do roli, dla ktorej "
	  + "jeszcze dzis pobiera nauke, a rowniez do ktorej jest jak sadzisz stworzony.\n");
    
	add_prop(LIVE_I_NEVERKNOWN, 1);
    add_prop(CONT_I_WEIGHT, 59000);
    add_prop(CONT_I_HEIGHT, 168);
    
	set_stats(({staty[0] + random(20), staty[1] + random(20), staty[2] + random(10), staty[3] + random(5), staty[4] + random(5), staty[5] + random(40)}));
    
	set_skill(SS_DEFENCE, umy[0] + random(10));
    set_skill(SS_PARRY, umy[1] + random(10));
    set_skill(SS_WEP_WARHAMMER, umy[2] + random(10));
    
	set_cchat_time(30 + random(30));
    add_cchat("Odczep sie ode mnie.");
    add_cchat("Odejdz!");

	set_cact_time(30 + random(30));
	add_cact("emote wykonuje kilka obronnych uderzen swym mlotem kowalskim.");
	add_cact("emote wykrzykuje na caly glos imie swego mistrza.");

	set_act_time(50 + random(20));
	add_act("emote kreci sie po kuzni.");
	add_act("emote przy pomocy miechu kowalskiego podgrzewa palenisko.");
	add_act("emote kilkoma uderzeniami wykuwa prosta kasetke.");
	add_act("ziewnij");
	add_act("emote wpatruje sie w ciebie badawczo.");

	set_chat_time(55 + random(20));
	add_chat("Jesli masz jakies zamowienie idz do mego Mistrza.");
	add_chat("Widziales juz moje okucia?");
	add_chat("Pobieram tu praktyke od sporego czasu.");

    add_weapon(WEAPONS_PATH + "mlot_kowalski1.c");
    add_armour(ARMOUR_PATH + "buty.c");
    add_armour(ODZIENIE_PATH + "koszula.c");
    add_armour(ODZIENIE_PATH + "beret.c");
    add_armour(ODZIENIE_PATH + "spodnie.c");

	set_default_answer(VBFC_ME("default_answer"));
}
void
attacked_by (object ob)
{
	object *obecni;

	obecni = all_inventory(environment(this_object())); 

	obecni = filter(obecni, f_filter); 

	obecni -= ({this_object()}); 

	obecni->command("krzyknij Pomozmy chlopcu!");  

	obecni->attack_object(ob); 
}



int
f_filter(object ktory)
{
	if((ktory->query_rasa() == "gwardzista") || (ktory->query_rasa() == "straznik")) 
		return 1;                         
	return 0;                          
}
    string
	default_answer()
{
	command("emote obraca kilka razy mlot w rece.");
    set_alarm(3.0, 0.0, "zdziwienie");
}
    void
    emote_hook(string emote, object wykonujacy, string przyslowek)
{
    switch (emote)
    {
        case "kopnij": set_alarm(2.0, 0.0, "zly", wykonujacy);
        	    break;
        case "spoliczkuj" :
        case "szturchnij":
        case "opluj" : set_alarm(2.0, 0.0, "zly", wykonujacy);
        	    break;
        	    
        case "zasmiej": 
        case "pocaluj": set_alarm(2.0, 0.0, "dobry", wykonujacy);
        	      break;
        case "przytul": set_alarm(2.0, 0.0, "dobry", wykonujacy);
    }
}



void
zly(object kto)
{
    switch (random(5)) 
    {
        
    case 0 : command("krzyknij Przestan glupcze bo pozalujesz!!"); break;
    case 1 : command("powiedz do " + kto->short(PL_DOP) + " Nie umiesz sie dobrze zachowywac?"); break;
    case 2 : command("zignoruj " + kto->short(PL_BIE)); break;
    default:
    }
}

void
dobry(object kto) 
{
        
    if(kto->koncowka("chlop","baba")=="chlop") zly(kto);
    else
      {
        
    
        switch (random(3)) 
        {
          case 0 : command("usmiechnij sie promiennie"); break;
          
        }
      }
} 
void
zdziwienie(object pla)
{
    command("powiedz Jesli masz jakies zamowienie idz "
	  + "do mego Mistrza Taeroma.");
	command("wskaz wschod");
}

/*----Podpisano Volothamp Geddarm----*/



