#pragma strict types

inherit "/std/monster";

#include <macros.h>
#include <stdproperties.h>
#include <language.h>
#include <composite.h>
#include <money.h>
#include <pl.h>
#include <time.h>
#include <formulas.h>
#include <std.h>
#include "../definicje.h"
#include <filter_funs.h>

int *umy = ({10, 10, 10});

void
create_monster()
{
    ustaw_imie(({ "dorian", "doriana", "dorianowi", "doriana", "dorianem", "dorianie"}), PL_MESKI_OS);
	
	dodaj_nazwy(PL_MEZCZYZNA);
    
	dodaj_przym("bystry","bystrzy");
    dodaj_przym("muskularny","muskularni");
    
	ustaw_odmiane_rasy(PL_MEZCZYZNA);
    
	set_stats( ({ 23, 23, 25, 20, 20, 45 }) );
    
	set_gender(G_MALE);
    
	add_prop(CONT_I_WEIGHT, 100000); 
    add_prop(CONT_I_VOLUME, 35000); 
    add_prop(CONT_I_HEIGHT, 180);
    
	set_act_time(180);
    add_act("emote piluje kawalek drewna.");
    add_act("zmarszcz brwi badawczo");
    add_act("emote ociera pot z czola.");
    add_act("emote masuje podbrodek w zamysleniu.");
    add_act("podrap sie po glowie");

    set_chat_time(500);
    add_chat("Ciezki jest zywot ciesli.");
	add_chat("Jestem jedynym ciezla w tym miescie.");
    
	set_cchat_time(60);
    add_cchat("Zabierzcie go ode mnie.");
    add_cchat("Wezwac straz!");

	set_default_answer(VBFC_ME("default_answer"));

    add_armour(ODZIENIE_PATH + "beret.c");
    add_armour(ODZIENIE_PATH + "koszula.c");
     add_armour(ODZIENIE_PATH + "spodnie.c");
  
    set_long("Jest to prawdopodobnie jedyny w miescie czlowiek, ktorego fachem "
	  + "i praca jest obrobka wszelkiego rodzaju drewna. Mowa tu oczywiscie "
	  + "o miejscowym ciesli, ktory posrod mieszkancow miasteczka cieszy "
	  + "sie spora popularnoscia i darzony jest odpowiednim do jego funkcji "
	  + "szacunkiem. Ma on spokojne i bystre spojrzenie, ruda lecz nieco "
	  + "poszarpana grzywke, a takze muskularna sylwetke, ktora nabral podczas "
	  + "lat ciezkiej pracy.\n");

	set_skill(SS_DEFENCE, umy[0] + random(20));
    set_skill(SS_PARRY, umy[1] + random(20));
    set_skill(SS_UNARM_COMBAT, umy[2] + random(20));

}
void
add_introduced(string imie_mia, string imie_bie)
{
	set_alarm(2.0, 0.0, "return_introduce", find_player(imie_mia));
}
void
return_introduce(object ob)
{
    command("przedstaw sie " + OB_NAME(ob));
	command("powiedz Witaj w moim warsztacie.");
}
string
default_answer()
{
	command("podrap sie w nos");
	set_alarm(2.0,0.0, "pytanie", this_interactive());
}
void
pytanie(object pla)
{
	command("powiedz Nic mi o tym nie wiadomo prosze " + this_player()->koncowka("pana","pani") + ".");
}
void
emote_hook(string emote, object wykonujacy, string przyslowek)
{
    switch (emote)
    {
        case "kopnij":
        case "spoliczkuj" :
        case "szturchnij":
        case "opluj" : set_alarm(2.0, 0.0, "zly", wykonujacy); break;
        	    
        case "zasmiej": 
        case "pocaluj":
        case "przytul": set_alarm(2.0, 0.0, "dobry", wykonujacy);
    }
}



void
zly(object kto)
{
    switch (random(2)) 
    {
		case 0 : command("powiedz Nikt nie nauczyl cie dobrych manier?"); 
		case 1 : command("powiedz Nie lubie osob zle wychowanych.");
    default:
    }
}

void
dobry(object kto) 
{
        
    if(kto->koncowka("chlop","baba")=="chlop") zly(kto);
    else
    {   
        switch (random(1)) 
        {
          case 0 : command("powiedz Przepraszam lecz mam juz zone, a takze dzieci."); break;    
        }
      }
}

