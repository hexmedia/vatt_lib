/*
 * Filename: urzednik.c
 *
 * Zastosowanie: Urzednik.
 * 
 * Copyright Volothamp Geddarm.
 *
 */
inherit "/std/monster.c";
inherit "/d/Faerun/std/questy";

#include <macros.h>
#include <stdproperties.h>
#include <language.h>
#include <composite.h>
#include <money.h>
#include <pl.h>
#include <time.h>
#include <formulas.h>
#include <std.h>
#include <mudtime.h>
#include "../definicje.h"

int *staty = ({10, 10, 10, 10, 10, 10});
int *umy = ({10, 10, 10});

int f_filter(object ktory);

    string *
    losowy_przym()
{
    string *lp;
    string *lm;
	string *lpa;
	string *lma;
    int i, j;
    lp=({ "wysoki", "niski", "barczysty", "szczuply", "umiesniony"});
    lm=({ "wysocy", "niscy", "barczysci", "szczupli", "umiesnieni"});
	lpa=({ "dlugowlosy", "gestobrewy", "rudowlosy", "czarnooki", "ogorzaly"});
	lma=({ "dlugowlosi", "gestobrewi", "rudowlosi", "czarnoocy", "ogorzali"});
	i = random(5);
	j = random(5);
	return ({ lp[i] , lm[i], lpa[j], lma[j] });
}
    void
    create_monster()
{
    string *przym = losowy_przym();
	ustaw_odmiane_rasy( ({"pracownik poczty", "pracownika poczty", "pracownikowi poczty", "pracownika poczty", "pracownikiem poczty", "pracowniku poczty" }),
		                ({"pracownicy poczty", "pracownikow poczty", "pracownikom poczty", "pracownikow poczty", "pracownikami poczty", "pracownikach poczty" }), PL_MESKI_OS);
    dodaj_nazwy( ({"pracownik", "pracownika", "pracownikowi", "pracownika", "pracownikiem", "pracowniku" }),
		         ({"pracownicy", "pracownikow", "pracownikom", "pracownikow", "pracownikami", "pracownikach" }), PL_MESKI_OS);
	
	set_gender(G_MALE);
	dodaj_nazwy(PL_MEZCZYZNA);
    
	dodaj_przym( przym[0], przym[1] );
    dodaj_przym( przym[2], przym[3] );
    
	set_long("Ow mezczyzna wyglada dosyc przyjaznie i prawdopodobnie to on sprawuje w tym miejscu urzad "
	  + "najwyzszego poczmistrza. Podobnie jak wielu urzednikow jest do swojej funkcji stosownie ubrany "
	  + "i wyglada calkiem powaznie co znaczy jedynie tyle, ze wszystko co czyni, robi sumiennie i skrupulatnie. "
	  + "Prawdopodobnie nie tylko zajmuje sie on w tym miejscu listami ale i wydaje paczki poslancom "
	  + "co sprawia, ze znany jest pomiedzy prostymi wedrowcami i goncami.\n");
    
	add_prop(LIVE_I_NEVERKNOWN, 1);
    add_prop(CONT_I_WEIGHT, 80000);
    add_prop(CONT_I_HEIGHT, 175);
    
	set_stats(({staty[0] + random(20), staty[1] + random(20), staty[2] + random(10), staty[3] + random(5), staty[4] + random(5), staty[5] + random(40)}));
    
	set_skill(SS_DEFENCE, umy[0] + random(10));
    set_skill(SS_PARRY, umy[1] + random(10));
    set_skill(SS_UNARM_COMBAT, umy[2] + random(10));
    
	set_cchat_time(20 + random(30));
    add_cchat("Napasc na urzednika!");
    add_cchat("Przeklety pomiocie. Zgin, przepadnij!");

	set_act_time(40 + random(20));
	add_act("emote podbija cos pieczatka.");
	add_act("emote wydaje poslancowi odpowiedni list.");
	add_act("emote przeglada jakies papiery.");
	add_act("emote ziewa.");
	add_act("emote przeglada wazne dokumenty.");
	add_act("emote daje zlecenie wolnemu goncowi.");

	set_chat_time(55 + random(20));
	add_chat("Ciezka praca poczmistrza.");
	add_chat("Przebieranie takiej ilosci listow to udreka.");
	add_chat("Nie myslisz chyba, ze moja praca jest latwa?");

	add_armour("/d/Faerun/Miasta/beregost/odzienie/tunika1");
	add_armour("/d/Faerun/Miasta/beregost/odzienie/sandaly");
    
	add_ask(({"pomoc", "prace", "zlecenie", "list"}),VBFC_ME("list"));
	set_default_answer(VBFC_ME("default_answer"));
	set_giver(1);
}
void
attacked_by (object ob)
{
	object *obecni;

	obecni = all_inventory(environment(this_object())); 

	obecni = filter(obecni, f_filter); 

	obecni -= ({this_object()}); 

	obecni->command("krzyknij Napasc na urzednika!");  

	obecni->attack_object(ob); 
}



int
f_filter(object ktory)
{
	if((ktory->query_rasa() == "gwardzista") || (ktory->query_rasa() == "straznik")) 
		return 1;                         
	return 0;                          
}
    string
	default_answer()
{
	command("emote przyglada ci sie ze zdziwieniem.");
    set_alarm(3.0, 0.0, "zdziwienie");
}
    void
    emote_hook(string emote, object wykonujacy, string przyslowek)
{
    switch (emote)
    {
        case "kopnij": set_alarm(2.0, 0.0, "zly", wykonujacy);
        	    break;
        case "spoliczkuj" :
        case "szturchnij":
        case "opluj" : set_alarm(2.0, 0.0, "zly", wykonujacy);
        	    break;
        	    
        case "zasmiej": 
        case "pocaluj": set_alarm(2.0, 0.0, "dobry", wykonujacy);
        	      break;
        case "przytul": set_alarm(2.0, 0.0, "dobry", wykonujacy);
    }
}



void
zly(object kto)
{
    switch (random(5)) 
    {
        
    case 0 : command("krzyknij Co to za zachowanie?!"); break;
    case 1 : command("powiedz do " + kto->short(PL_DOP) + " Nie nauczyli cie dobrych manier?"); break;
    case 2 : command("powiedz do " + kto->short(PL_DOP) + " Uspokoj sie lepiej."); break;
    default:
    }
}

void
dobry(object kto) 
{
        
    if(kto->koncowka("chlop","baba")=="chlop") zly(kto);
    else
      {
        
    
        switch (random(3)) 
        {
          case 0 : command("powiedz Moglabys to powtorzyc?"); break;
          
        }
      }
} 
void
zdziwienie(object pla)
{
    command("powiedz Nie mam pojecia o co ci dzisiaj chodzi.");
}


/*----Podpisano Volothamp Geddarm----*/



