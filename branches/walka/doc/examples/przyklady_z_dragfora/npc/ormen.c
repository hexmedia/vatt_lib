#pragma strict types

inherit "/std/monster";

#include <macros.h>
#include <stdproperties.h>
#include <language.h>
#include <composite.h>
#include <money.h>
#include <pl.h>
#include <time.h>
#include <formulas.h>
#include <std.h>
#include "../definicje.h"
#include <filter_funs.h>

int *umy = ({10, 10, 10});

void
create_monster()
{
    ustaw_imie(({ "ormen", "ormena", "ormenowi", "ormena", "ormenem", "ormenie"}), PL_MESKI_OS);
	
	dodaj_nazwy(PL_MEZCZYZNA);
    
	dodaj_przym("czarnowlosy","czarnowlosi");
    
	ustaw_odmiane_rasy(PL_MEZCZYZNA);
    
	set_stats( ({ 23, 23, 25, 20, 20, 45 }) );
    
	set_gender(G_MALE);
    
	add_prop(CONT_I_WEIGHT, 75000); 
    add_prop(CONT_I_VOLUME, 35000); 
    add_prop(CONT_I_HEIGHT, 175);
    
	set_act_time(180);
    add_act("urozejrzyj sie powaznie");
    add_act("emote rozglada sie po wszystkich bywalcach gospody.");
    add_act("emote nakazuje sluzbie wyniesc z piwnicy butelke sherry.");
    add_act("emote polewa wina nowo przybylemu gosciowi.");
    add_act("emote na podstawie nieduzej ksiazki przydziela jednemu z bywalcow apartament.");

    set_chat_time(500);
    add_chat("Nie mozesz przegapic naszych tart z cebula i grzybkami.");
	add_chat("Chcesz moze wynajac apartament?");
    
	set_cchat_time(60);
    add_cchat("Odczep sie ode mnie!");
    add_cchat("Straz! Pomocy!");

	set_default_answer(VBFC_ME("default_answer"));
  
    set_long("Jest to prawdopodobnie tutejszy karczmarz, ktory nie wyglada "
	  + "szczegolnie wesolo lecz powaznie i w pelni wyniosle. Ma na sobie "
	  + "elegancki stroj i jak sadzisz moze byc to jeden z potomkow slynnego "
	  + "Feldeposta - zalozyciela karczmy i jego pierwszego gospodarza. "
	  + "Prawdopodobnie to on dyryguje miejscowa sluzba, przydziela pokoje "
	  + "i przyjmuje wszystkie zamowienia dotyczace miejscowych potraw i trunkow.\n");

    add_armour(ODZIENIE_PATH + "koszula");
    add_armour(ODZIENIE_PATH + "buty");
    add_armour(ODZIENIE_PATH + "kapelusz1");
    add_armour(ODZIENIE_PATH + "spodnie");
    set_liv_name("_karczmarz_z_beregostu_");
	set_skill(SS_DEFENCE, umy[0] + random(20));
    set_skill(SS_PARRY, umy[1] + random(20));
    set_skill(SS_UNARM_COMBAT, umy[2] + random(20));

}
void
add_introduced(string imie_mia, string imie_bie)
{
	set_alarm(2.0, 0.0, "return_introduce", find_player(imie_mia));
}
void
return_introduce(object ob)
{
    command("przedstaw sie " + OB_NAME(ob));
	command("podaj reke " + ob->query_name(PL_CEL));
}
string
default_answer()
{
	command("podrap sie w nos");
	set_alarm(2.0,0.0, "pytanie", this_interactive());
}
void
pytanie(object pla)
{
	command("emote rozklada rece bezradnie.");
}
void
emote_hook(string emote, object wykonujacy, string przyslowek)
{
    switch (emote)
    {
        case "kopnij":
        case "spoliczkuj" :
        case "szturchnij":
        case "opluj" : set_alarm(2.0, 0.0, "zly", wykonujacy); break;
        	    
        case "zasmiej": 
        case "pocaluj":
        case "przytul": set_alarm(2.0, 0.0, "dobry", wykonujacy);
    }
}



void
zly(object kto)
{
    switch (random(2)) 
    {
		case 0 : command("powiedz Nie toleruje zle wychowanych osob!");
		case 1 : command("powiedz Wynos sie stad!");
    default:
    }
}

void
dobry(object kto) 
{
        
    if(kto->koncowka("chlop","baba")=="chlop") zly(kto);
    else
    {   
        switch (random(1)) 
        {
          case 0 : command("powiedz Powtorz to, a moze dostaniesz darmowy kieliszek najlpeszej wisniowki."); break;    
        }
      }

}



