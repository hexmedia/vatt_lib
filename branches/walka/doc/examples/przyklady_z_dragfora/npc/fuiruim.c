#pragma strict types

inherit "/std/kowal.c";
inherit "/lib/trade";

#include "../definicje.h"

void
create_kowal()
{
    ::create_kowal();

    ustaw_imie(({ "taerom", "taeroma", "taeromowi", "taeroma", "taeromem", "taeromie"}), PL_MESKI_OS);
   
    ustaw_odmiane_rasy(PL_MEZCZYZNA);
    
    dodaj_przym("masywny", "masywni");
    dodaj_przym("wasaty", "wasaci");
    
    dodaj_nazwy(({"fuiruim", "fuiruima", "fuiruimowi", "fuiruima", "fuiruimem", "fuiruimie"}), PL_MESKI_OS);

    set_title("'Grzomiacy Mlot' Fuiruim");
    set_stats(({60, 45, 60, 50, 50, 50}));
    set_gender(G_MALE);    
    set_alignment(200);
    set_skill(SS_PARRY, 45);
    set_skill(SS_DEFENCE, 45);
    set_skill(SS_WEP_CLUB, 60);
    set_skill(SS_WEP_WARHAMMER, 75);
    set_default_answer(VBFC_ME("default_answer"));
    set_liv_name("_kowal_z_beregostu_");
    set_money_accept(({1,1,1,0}));
    config_default_trade();

    add_prop(CONT_I_WEIGHT, 94000);
    add_prop(CONT_I_VOLUME, 54000);
    add_prop(CONT_I_HEIGHT, 210);
   
    set_act_time(80 + random(10));
    add_act("rozejrzyj sie uwaznie");
    add_act("emote usmiecha sie szeroko na twoj widok.");
    add_act("emote przyglada sie swemu mlotowi kowalskiemu.");
    add_act("emote wykonuje kilka krokow smiesznie sie kiwajac.");
    add_act("emote instruuje dokladnie jednego z czeladnikow.");
    add_act("zanuc pracowicie");
    add_act("skin glowa");

    set_chat_time(60 + random(10));
    add_chat("Moje wyroby najlepsze na Wybrzezu Mieczy.");
    add_chat("Zapewniam dwutygodniowa gwarancje na przedmioty, ktore beda odpowiednio oznakowane.");
    add_chat("Mam na uslugach wielu czeladnikow, ktorzy bardzo pomagaja mi w pracy.");
    add_chat("Moze zechcesz zamowic jedna z moich wysmienitych zbroi?");
    add_chat("Kunsztu kowalskiego nauczyl mnie poniegdaj pewien krasnolud, ktory dzis mieszka w Athkatli.");
    add_chat("Miecze, ktore tu wykuwamy sa wytrzymalsze nawet od krasnoludzkich toporow.");
    
    set_cchat_time(60);
    add_cchat("Dlaczego mnie atakujesz?!");
    add_cchat("Co to za pomysly?! Zjezdzaj!");

    set_long("Czlowiek na, ktorego spogaladasz to slynny Terom Fuiruim. Ma "+
        "on ponad dwa metry wzrostu i krzepka postawe, mimo iz jest w podeszlym "+
	"wieku o czym swiadcza liczne zmarszczki na jego twarzy. Widzac "+
	"go przypominasz sobie wszystko co slyszales na temat tego czlowieka "+
	"i jego kunsztu, ktorego slawa siega do dalekich krain polnocy, "+
	"podziemi krasnoludzkich twierdz oraz odleglych wysp. Jego potezna "+
	"dlon sciska pewnie mlot kowalski jakby robila to od zawsze, a geste i "+
	"sumiaste wasy pieknie podkreslaja surowosc twarzy. Mowi sie o tym "+
	"olbrzymie, ze za pomoca dwa i pol metrowej palki umie zabic gnolla "+
	"jednym uderzeniem i nie sadzisz by byly to czce plotki mieszkancow Beregostu.\n");

	
    add_armour(ODZIENIE_PATH + "beret");
    add_weapon(WEAPONS_PATH + "mlot_kowalski");
    add_armour(ODZIENIE_PATH + "buty");
    add_armour(ODZIENIE_PATH + "fartuch");

    dodaj_przedmioty(({
    ARMOUR_PATH + "kolczuga.c",
    ARMOUR_PATH + "zbroja_paskowa.c",
    ARMOUR_PATH + "zbroja_skorzana.c",
    ARMOUR_PATH + "zbroja_plytowa.c",
    ARMOUR_PATH + "helm.c",
    ARMOUR_PATH + "buty.c",
    WEAPONS_PATH + "miecz.c",
    WEAPONS_PATH+ "miecz1.c",
    WEAPONS_PATH + "bekart.c",
    WEAPONS_PATH + "mlot.c",
    WEAPONS_PATH + "topor.c",
    WEAPONS_PATH + "wekiera.c"}));
	
    ustaw_emoty_przy_pracy(({
    "emote chwyta pewniej kowalski mlot.",
    "emote poprawia ulozenie swego kowalskiego fartucha.",
    "emote reka sciera z czola pot.",
    "emote uzywajac miechu podgrzewa rozrzazone palenisko.",
    "emote uzywajac mlota wykonuje kilka uderzen.",
    "emote za pomoca szczypiec umieszcza cos w bali wody, a w pomieszczeniu robi sie parno."}));
}

void
add_introduced(string kto)
{
    set_alarm(2.0, 0.0, "return_introduce", find_player(kto));
}

void
return_introduce(object ob)
{
    command("przedstaw sie " + OB_NAME(ob));
    command("powiedz do " + ob->short(PL_DOP) + " Witaj w mojej kuzni.");
    set_alarm(2.0,0.0, "przedstawienie");
}

string
default_answer()
{
    command("podrap sie w glowe");
    set_alarm(2.0,0.0, "pytanie", this_interactive());
}

void
pytanie(object pla)
{
    command("powiedz Czyzbys chcial zamowic jakis przedmiot?");
}

void
emote_hook(string emote, object wykonujacy, string przyslowek)
{
    switch (emote)
    {
        case "kopnij":
        case "spoliczkuj" :
        case "szturchnij":
        case "opluj" : set_alarm(2.0, 0.0, "zly", wykonujacy); break;
        	    
        case "zasmiej": 
        case "pocaluj":
        case "przytul": set_alarm(2.0, 0.0, "dobry", wykonujacy);
    }
}

void
zly(object kto)
{
    switch (random(2)) 
    {
		case 0 : command("powiedz Nie toleruje zle wychowanych osob!");
		case 1 : command("powiedz Wynos sie stad niegodziwcze!");
    default:
    }
}

void
dobry(object kto) 
{
        
    if(kto->koncowka("chlop","baba")=="chlop") zly(kto);
    else
    {   
        switch (random(1)) 
        {
          case 0 : command("zarumien sie"); break;    
        }
    }
}

void
przedstawienie(object pla)
{
	command("powiedz Wlasciwie to milo cie tu widziec.");
	command("usmiechnij sie szeroko");
}
