/*
 * Filename: straznik_swiatynny.c
 *
 * Zastosowanie: Straznik swiatyni! ;)
 * 
 * Copyright Volothamp Geddarm.
 *
 */
inherit "/std/monster.c";

#include <macros.h>
#include <stdproperties.h>
#include <language.h>
#include <composite.h>
#include <money.h>
#include <pl.h>
#include <time.h>
#include <formulas.h>
#include <std.h>
#include <mudtime.h>
#include "../definicje.h"

int *staty = ({35, 40, 45, 10, 10, 35});
int *umy = ({25, 20, 30});

int f_filter(object ktory);

    string *
    losowy_przym()
{
    string *lp;
    string *lm;
    int i;
    lp=({ "smialy", "barczysty", "milczacy", "czujny", "uwazny"});
    lm=({ "smiali", "barczysci", "milczacy", "czujni", "uwazni"});
	i = random(5);
	return ({ lp[i] , lm[i] });
}
    void
    create_monster()
{
    string *przym = losowy_przym();
	ustaw_odmiane_rasy( ({"straznik swiatynny", "straznika swiatynnego", "straznikowi swiatynnemu", "straznika swiatynnego", "straznikiem swiatynnym", "strazniku swiatynnym" }),
		                ({"straznicy swiatynni", "straznikow swiatynnych", "straznikom swiatynnym", "straznikow swiatynnych", "straznikami swiatynnymi", "straznikach swiatynnych" }), PL_MESKI_OS);

	dodaj_nazwy( ({"straznik","straznika","straznikowi","straznika","straznikiem","strazniku"}),
		         ({"straznicy","straznikow","straznikom","straznikow","straznikami","straznikach"}), PL_MESKI_OS);

    set_gender(G_MALE);
	dodaj_nazwy(PL_MEZCZYZNA);
    
	dodaj_przym( przym[0], przym[1] );
    
	set_long("Jest to umiesniony mezczyzna o twardych rysach twarzy, ktory przyglada sie kazdemu z ukosa. "
	  + "Jego harda i smiala twarz obrazuje jedynie najprostsze mysli, a rozni sie on od innych wartownikow "
+ "tym, ze jest bardziej religijny a przez to i madrzejszy. Jego zadanie to pilnowanie tu oltarza, kamiennego "
	  + "posagu, a takze tutejszego kaplana przed osobami postronnymi. Wyglada, ze dobrze pelni swoj obowiazek, "
	  + "totez arcykaplan musi byc z niego dumny.\n");
    
	add_prop(LIVE_I_NEVERKNOWN, 1);
    add_prop(CONT_I_WEIGHT, 80000);
    add_prop(CONT_I_HEIGHT, 175);
    
	set_stats(({staty[0] + random(20), staty[1] + random(20), staty[2] + random(10), staty[3] + random(5), staty[4] + random(5), staty[5] + random(40)}));
    
	set_skill(SS_DEFENCE, umy[0] + random(10));
    set_skill(SS_PARRY, umy[1] + random(10));
    set_skill(SS_WEP_AXE, umy[2] + random(10));
    
	set_cchat_time(20 + random(30));
    add_cchat("To tak sie teraz traktuje oswieconych?");
    add_cchat("Lathanderze badz mi ty sciezka, badz poratunkiem.");

	set_act_time(40 + random(20));
	add_act("emote spoglada na ciebie z ukosa.");
	add_act("emote podejrzliwie patrzy w kazdy zakatek sali.");
	add_act("rozejrzyj sie czujnie");
	add_act("emote spoglada na posadzke.");

	set_chat_time(55 + random(20));
	add_chat("Lepiej na siebie uwazaj.");
	add_chat("Przyszedles sie tutaj modlic?");
	add_chat("Nic nie rozni cie od innych.");

	add_armour("/d/Faerun/Miasta/beregost/zbroje/zbroja_paskowa");
	add_armour("/d/Faerun/Miasta/beregost/zbroje/buty");
	add_weapon("/d/Faerun/Miasta/beregost/zbroje/mlot");
	add_armour("/d/Faerun/Miasta/beregost/zbroje/helm");

	set_default_answer(VBFC_ME("default_answer"));
}
void
add_introduced(string imie_mia, string imie_bie)
{
	set_alarm(2.0, 0.0, "return_introduce", find_player(imie_mia));
}
void
return_introduce(object ob)
{
    command("pokiwaj glowa powitalnie");
}
void
attacked_by (object ob)
{
	object *obecni;

	obecni = all_inventory(environment(this_object())); 

	obecni = filter(obecni, f_filter); 

	obecni -= ({this_object()}); 

	obecni->command("powiedz Lathanderze wspomoz mnie w walce.");  

	obecni->attack_object(ob); 
}
int
f_filter(object ktory)
{
	if((ktory->query_rasa() == "straznik swiatynny") || (ktory->query_imie() == "kelddath")) 
		return 1;                         
	return 0;                          
}
    string
	default_answer()
{
	command("emote spoglada na ciebie uwaznie.");
    set_alarm(3.0, 0.0, "zdziwienie");
}
    void
    emote_hook(string emote, object wykonujacy, string przyslowek)
{
    switch (emote)
    {
        case "kopnij": set_alarm(2.0, 0.0, "zly", wykonujacy);
        	    break;
        case "spoliczkuj" :
        case "szturchnij":
        case "opluj" : set_alarm(2.0, 0.0, "zly", wykonujacy);
        	    break;
        	    
        case "zasmiej": 
        case "pocaluj": set_alarm(2.0, 0.0, "dobry", wykonujacy);
        	      break;
        case "przytul": set_alarm(2.0, 0.0, "dobry", wykonujacy);
    }
}



void
zly(object kto)
{
    switch (random(5)) 
    {
        
    case 0 : command("krzyknij Lathander cie ukaze!"); break;
    case 1 : command("powiedz do " + kto->short(PL_DOP) + " Dobrze, ze sie jeszcze kontroluje."); break;
    case 2 : command("powiedz do " + kto->short(PL_DOP) + " Lathander mowi: Samokontrola to klucz do "
	  + "wspolzycia z innymi."); break;
    default:
    }
}

void
dobry(object kto) 
{
        
    if(kto->koncowka("chlop","baba")=="chlop") zly(kto);
    else
      {
        
    
        switch (random(3)) 
        {
          case 0 : command("emote nie pozostawia na twarzy najmniejszego wyrazu zadowolenia."); break;
          
        }
      }
} 
void
zdziwienie(object pla)
{
    command("powiedz Niestety. Nie wiem o co ci chodzi.");
}

/*----Podpisano Volothamp Geddarm----*/



