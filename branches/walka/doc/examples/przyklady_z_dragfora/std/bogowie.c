#pragma strict_types
#pragma no_clone

#include <pl.h>

#define GARL  ({\
	({"garl","garla","garlowi","garla","garlem","garlu"}),\
	({"Garl, bog ochrony, humoru, oszustwa, obrobki klejnotow i gnomow","Garla, boga ochrony, humoru, oszustwa, obrobki klejnotow i gnomow",\
	  "Garlowi, bogowi ochrony, humoru, oszustwa, obrobki klejnotow i gnomow","Garla, boga ochrony, humoru, oszustwa, obrobki klejnotow i gnomow",\
	  "Garlem, bogiem ochrony, humoru, oszustwa, obrobki klejnotow i gnomow","Garlu, bogu ochrony, humoru, oszustwa, obrobki klejnotow i gnomow"}), \
	  PL_MESKI_OS})


#define LATHANDER 	({\
	({"lathander","lathandera","lathanderowi","lathandera",\
	"lathanderem","lathanderze"}),\
	({"Lathander, Pan Poranka","Lathandera, Pana Poranka","Lathanderowi, Panu Poranka",\
	"Lathandera, Pana Poranka","Lathanderem, Panem Poranka",\
	"Lathanderze, Panu Poranka"}),\
	PL_MESKI_OS})

#define OGHMA ({\
	({"oghma", "oghmy", "oghmie", "oghme", "oghma", "oghmie"}),\
	({"Oghma, bostwo inspiracji, inwencji i wiedzy",\
	"Oghmy, bostwa inspiracji, inwencji i wiedzy",\
	"Oghmie, bostwu inspiracji, inwencji i wiedzy",\
	"Oghme, bostwo inspiracji, inwencji i wiedzy",\
	"Oghma, bostwem inspiracji, inwencji i wiedzy",\
	"Oghmie, bostwie inspiracji, inwencji i wiedzy"}),\
	PL_MESKI_OS})

#define SHAUNDAKUL ({\
	({"shaundakul", "shaundakula", "shaundakulowi", "shaundakula",\
	"shaundakulem", "shaundakulu"}),\
	({"Shaundakul, bostwo podrozy i odkrywania",\
	"Shaundakula, bostwa podrozy i odkrywania",\
	"Shaundakulowi, bostwu podrozy i odkrywania",\
	"Shaundakula, bostwa podrozy i odkrywania",\
	"Shaundakulem, bostwem podrozy i odkrywania",\
	"Shaundakulu, bostwie podrozy i odkrywania"}),\
	PL_MESKI_OS})

#define HELM ({\
	({"helm", "helma", "helmowi", "helma", "helmem", "helmie"}),\
	({"Helm, bog straznikow, obroncow i ochrony",\
	"Helma, boga straznikow, obroncow i ochrony",\
	"Helmowi, bogowi straznikow, obroncow i ochrony",\
	"Helma, boga straznikow, obroncow i ochrony",\
	"Helmem, bogiem straznikow, obroncow i ochrony",\
	"Helmie, bogu straznikow, obroncow i ochrony"}),\
	PL_MESKI_OS})

#define AERDIE ({\
({"aerdie","aerdie","aerdie","aerdie","aerdie","aerdie"}),\
({"Aerdie Faenyi, bogini powietrza, deszczu i plodnosci",\
"Aerdie Faenyi, bogini powietrza, deszczu i plodnosci",\
"Aerdie Faenyi, bogini powietrza, deszczu i plodnosci",\
"Aerdie Faenyi, boginie powietrza, deszczu i plodnosci",\
"Aerdie Faenyi, boginia powietrza, deszczu i plodnosci",\
"Aerdie Faenyi, bogini powietrza, deszczu i plodnosci"}),\
PL_ZENSKI})

#define SELUNE ({\
({"selune","selune","selune","selune","selune","selune"}),\
({"Selune, bogini ksiezyca i gwiazd",\
"Selune, bogini ksiezyca i gwiazd",\
"Selune, bogini ksiezyca i gwiazd",\
"Selune, bogini ksiezyca i gwiazd",\
"Selune, bogini ksiezyca i gwiazd",\
"Selune, bogini ksiezyca i gwiazd"}),\
PL_ZENSKI})

mapping bogowie = ([
	"lathander": LATHANDER,
	"oghma":OGHMA,
	"shaundakul":SHAUNDAKUL,
	"helm":HELM,
	"aerdie":AERDIE,
	"selune":SELUNE,
	"garl":GARL,
	]);


string
query_bog(string bog, int przypadek, int long = 0)
{
if (!pointerp(bogowie[bog])) return "";

return bogowie[bog][long][przypadek];
}

mixed
query_rodzaj(string bog)
{
if (!pointerp(bogowie[bog])) return "";

return bogowie[bog][3];
}

