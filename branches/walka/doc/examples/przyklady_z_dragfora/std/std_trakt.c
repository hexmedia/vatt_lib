#pragma strict_types
inherit "/d/Faerun/std/room";
#include "../definicje.h"

void 
create_trakt()
{
    set_short("Zwykla dorga na trakcie crimmor-athkatla");
    set_long("Standard\n");
    
    
}
public void 
reset_trakt()
{
}
public nomask void
create_room()
{       
    setuid();
    seteuid(getuid());    
    set_event_time(130.0,130.0);
    add_event("Na horyzoncie dostrzegasz chmure pylu.\n"); 
    add_event("Kolo ciebie przejezdza woz kupiecki kierujecy "+
              "sie w strone Athkatli.\n");
    add_event("Minal cie chlop, prowadzacy swa krowe.\n");
    add_event("Kolo ciebie przejezdza woz kupiecki kierujacy "+
              "sie w strone Crimmor.\n");
    add_event("Jakis czlowiek na koniu przejezdza kolo ciebie "+
              "kierujac sie do Athkatli\n");
    add_event("Jakis czlowiek na koniu przejezdza kolo ciebie "+
              "kierujac sie do Crimmor.\n");
    add_event("Na uboczu drogi dostrzegasz rodzine jezy.\n");
    add_item("trakt","Jest to trakt kupiecki prowadzacy prze dwa "+
             "duze miasta Amnu: Athkatle i Crimmor. Droga jest "+
             "dobrze ubita i nie widac na niej zadnych dziur "+
             "ani zniszczen, nie ma na niej takze zadnych "+
             "kepek trawy. Trakt ten jest bardzo czesto uczeszczany "+
             "i dosc szeroki aby wozy kupieckie nie mial zbytnich "+
             "problemow z przejazdem.\n");
    
    set_events_on(1);
    set_ziemia(1);
    create_trakt();
}
public nomask void
reset_room()
{
    setuid();
    seteuid(getuid());
    reset_trakt();
}
string d_opis()
{
    string opis = "";


    if (pora_roku2() == "zima")
    {
        opis +="";

        switch(MT_PORA_DNIA_STR(pora_dnia()))
        {
            case "swit": opis += ""; break;
                    
            case "wczesny ranek": opis += ""; break;

            case "ranek": opis += ""; break;
                    
            case "poludnie": opis += ""; break;
                    
            case "popoludnie": opis += ""; break;
                    
            case "wieczor": opis += ""; break;
                    
            case "pozny wieczor": opis += ""; break;
                    
            case "noc": opis += "";
        }
    }
    else

    if (pora_roku2() == "wiosna")
    {
        if (MT_PORA_DNIA_STR(pora_dnia()) != "noc") 
            opis += "";
        switch (MT_PORA_DNIA_STR(pora_dnia()))
        {
            case "swit": opis += ""; break;
                
            case "wczesny ranek": opis += ""; break;
                
            case "ranek": opis += ""; break;

            case "poludnie": opis += ""; break;
                
            case "popoludnie": opis += ""; break;
                 
            case "wieczor": opis += ""; break;
                
            case "pozny wieczor": opis += ""; break;
                
            case "noc": opis += "";
        }
    }
    else

    if (pora_roku2() == "lato")
    {
        opis+="";
        switch(MT_PORA_DNIA_STR(pora_dnia()))
        {
            case "swit": opis += ""; break;
                
            case "wczesny ranek": opis += ""; break;
                
            case "ranek": opis += ""; break;

            case "poludnie": opis += ""; break;
                
            case "popoludnie": opis += ""; break;
                 
            case "wieczor": opis += ""; break;
                
            case "pozny wieczor": opis += ""; break;
                
            case "noc": opis += "";
        }
    }
    else

    if (pora_roku2() == "jesien")
    {
       opis+="";

        switch(MT_PORA_DNIA_STR(pora_dnia()))
        {
            case "swit": opis += ""; break;
                
            case "wczesny ranek": opis += ""; break;
                
            case "ranek": opis += ""; break;
                
            case "poludnie": opis += "";  break;
                
            case "popoludnie": opis += ""; break;
                
            case "wieczor": opis += ""; break;
                
            case "pozny wieczor": opis += ""; break;
                
            case "noc": opis +=""; break;
        }
    }

return opis;
}
