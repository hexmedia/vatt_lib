#pragma strict_types

#include <pl.h>
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <cmdparse.h>
#include <composite.h>

#include "bandyci.h"

#define PL_BANDYTA ({ "bandyta", "bandyty", "bandycie", "bandyte", "bandyta", "bandycie" }), ({ "bandyci", "bandytow", "bandytom", "bandytow", "bandytami", "bandytach"}), PL_MESKI_OS
#define PL_BANDYTKA ({ "bandytka", "bandytki", "bandytce", "bandytke", "bandytka", "bandytce" }), ({ "bandytki", "bandytek", "bandytkom", "bandytki", "bandytkami", "bandytkach"}), PL_ZENSKI

inherit "/std/monster";

int *staty = ({30, 30, 30, 25, 25, 35});
int *umy = ({30, 30, 30});
string* prasy = ({});
string* pprzym = ({});
string* dprzym = ({});
int mowi = 0;
int proc = 20;
object *bron = ({});
mapping alarmy = ([]);

string* losowe_przym(int ile);
mixed* wylosuj_rase();
int bic();
int wyjscia (string str);
void zbieranie (object *obs, object od);
int odkladanie (string str);
int wartosc (object ob);
void zniszcz();
varargs void spotkanie(object obj);
void countdown(object czyje, int faza);
object* query_bandyci();
mixed oke_to_move(string exit);

void
create_monster()
{
    mixed *rasa;

    set_long("Obserwujesz wlasnie typowego bandyte jaki w grupach grasuje na drogach i okrada bogatych. "+
			 "Ow osobnik dawno temu wyparl sie zwierzchnictwa swego wladcy i odwrocil sie do niego stajac "+
			 "sie po prostu zwyklym banita. Aktualnie czycha on na twe pieniadze oraz reszty przejezdzajacych "+
		     "tedy ludzi. Dobrze uzbrojony zastawia droge kazdej osobie, ktora zechce opuscic to miejsce "+
		     "zanim rzuci swe kosztownosci na ziemie.\n");


    pprzym = losowe_przym(1);
    dodaj_przym(pprzym[0], pprzym[1]);

    dprzym = losowe_przym(2);
    while(pprzym[0] == dprzym[0])
        dprzym = losowe_przym(2);
    dodaj_przym(dprzym[0], dprzym[1]);

    rasa = wylosuj_rase();
    ustaw_odmiane_rasy(rasa[0], rasa[1], rasa[2]);
    prasy = rasa[3];

    add_prop(LIVE_I_NEVERKNOWN,1);
    add_prop(OBJ_I_WEIGHT, 74000 + (random(31)*1000)); 
	add_prop(OBJ_I_VOLUME, 35000 + (random(31)*1000));
	add_prop(NPC_I_NO_RUN_AWAY, 1);
	add_prop(NPC_I_NO_FEAR, 1);
	add_prop(NPC_M_NO_ACCEPT_GIVE,
             " mowi do Ciebie: Zadnych sztuczek! Odkladaj rzeczy na ziemie!\n");
		
    set_stats(({staty[0] + random(30), staty[1] + random(30), 
                staty[2] + random(30), staty[3] + random(15), 
                staty[4] + random(15), staty[5] + random(20)}));
    
	set_skill(SS_PARRY, umy[0] + random(20));
	set_skill(SS_DEFENCE, umy[1] + random(20));
	set_skill(SS_WEP_SWORD, umy[2] + random(20));
    
	bron += ({add_weapon(WEAPONS + "prosty_masywny")});
	bron += ({add_armour(WEAPONS + "kolczuga")});
	bron += ({add_armour(WEAPONS + "buty")});
	bron += ({add_armour(WEAPONS + "spodnie")});

	set_alignment(-100);

	set_default_answer("Nie gadaj tylko dawaj kosztownosci!\n");
	
	set_aggressive(bic);
	
	add_notify_meet_interactive("spotkanie");
}

void
init_living()
{
    ::init_living();
    add_action(wyjscia, "", 1);
    add_action(odkladanie, "odloz");
}

string *
losowe_przym(int ile)
{
	string *lp, *lm;
	int i;

	if(ile == 1)
	{
		lp = ({"grozny", "brzydki", "szpetny", "jednooki", "paskudny", "wielki"});
		lm = ({"grozni", "brzydcy", "szpetni", "jednoocy", "paskudni", "wielcy"});
		i = random(6);
		return ({lp[i], lm[i]});
	}
	else
	{
		lp = ({"potezny", "muskularny", "umiesniony", "szczurowaty", "zwinny", "atletyczny"});
		lm = ({"potezni", "muskularni", "umiesnieni", "szczurowaci", "zwinni", "atletyczni"});
		i = random(6);
		return ({lp[i], lm[i]});
	}
	return ({" ", " "});
}

mixed *
wylosuj_rase()
{
	int rasa = random(19);

	switch(rasa)
	{
		case 0:
			return({PL_MEZCZYZNA, ({"ludzki", "ludzcy"})});
		case 1:
			return({PL_KOBIETA, ({"ludzki", "ludzcy"})});
		case 2:
			return({PL_ELF, ({"elfi", "elfi"})});
		case 3:
			return({PL_ELFKA, ({"elfi", "elfi"})});
		case 4:
			return({PL_KRASNOLUD, ({"krasnoludzki", "krasnoludzcy"})});
		case 5:
			return({PL_KRASNOLUDKA, ({"krasnoludzki", "krasnoludzcy"})});
		case 6:
			return({PL_HOBBIT, ({"hobbicki", "hobbiccy"})});
		case 7:
			return({PL_HOBBITKA, ({"hobbicki", "hobbiccy"})});
		case 8:
			return({PL_GNOM, ({"gnomi", "gnomi"})});
		case 9:
			return({PL_GNOMKA, ({"gnomi", "gnomi"})});
		case 10:
			return({PL_GOBLIN, ({"goblinski", "goblinscy"})});
		case 11:
			return({PL_GOBLINKA, ({"goblinski", "goblinscy"})});
		case 12:
			return({PL_HALFLING, ({"halflinski", "halflinscy"})});
		case 13:
			return({PL_HALFLINKA, ({"halflinski", "halflinscy"})});
		case 14:
			return({PL_OGR, ({"ogrzy", "ogrzy"})});
		case 15:
			return({PL_OGRZYCA, ({"ogrzy", "ogrzy"})});
		case 16:
			return({PL_ORK, ({"orczy", "orczy"})});
		case 17:
			return({PL_ORCZYCA, ({"orczy", "orczy"})});
		case 18:
			return({PL_POLORK, ({"polorczy", "polorczy"})});
		case 19:
			return({PL_POLORCZYCA, ({"polorczy", "polorczy"})});
	}
}

void
dodaj_bandyta_short()
{
    dodaj_nazwy(query_rasy(), query_prasy(), query_rodzaj_rasy());
    if(query_rodzaj_rasy()==3)
        ustaw_odmiane_rasy(PL_BANDYTKA);
    else
        ustaw_odmiane_rasy(PL_BANDYTA);
    
    dodaj_przym(prasy[0],prasy[1]);
    add_prop(LIVE_I_NEVERKNOWN, 1);
}

void
zmien_pprzym(string lp, string lm)
{
    if(!dodaj_przym(lp,lm))
        return 0;
    remove_adj(pprzym[0]);
    pprzym = ({lp, lm});
}

void
zmien_dprzym(string lp, string lm)
{
    if(!dodaj_przym(lp,lm))
        return 0;
    remove_adj(dprzym[0]);
    dprzym = ({lp, lm});
}

void
set_rprzym(string lp, string lm)
{
    prasy = ({lp, lm});
}

void
zmien_umy(int* new)
{
	set_skill(SS_PARRY, new[0] + random(20));
	set_skill(SS_DEFENCE, new[1] + random(20));
	set_skill(SS_WEP_SWORD, new[2] + random(20));
}

void
zmien_staty(int* new)
{
    set_stats(({new[0] + random(30), new[1] + random(30), 
                new[2] + random(30), new[3] + random(15), 
                new[4] + random(15), new[5] + random(20)}));
}

void
set_mowi(int i)
{
mowi = i;
if(i)
    set_alarm(1800.0, 0.0, "zniszcz");
}

void
attacked_by(object attacker)
{
    if(alarmy[BAN_ALARM_ID(attacker)])
    {
        remove_alarm(alarmy[BAN_ALARM_ID(attacker)]);
        alarmy = m_delete(alarmy, BAN_ALARM_ID(attacker));
    }
    query_bandyci()->attack_object(attacker);
    attacker->add_prop(BAN_I_ATAKOWAL, 1);
    ::attacked_by(attacker);
}

int
bic ()
{
    return (this_player()->query_prop(BAN_I_ATAKOWAL));   
}

int
wyjscia (string str)
{
    string cmd;
    string* exits;
    object* band;
    int i,sz, zr;
    
    cmd = query_verb();
    exits = environment(this_object())->query_exit_cmds();
    
    if(!mowi)
        return 0;
    if(member_array("/std/player_sec.c", inherit_list(this_interactive())) == -1)
        return 0;

    if(this_player()->query_prop(BAN_I_ODDAL) || 
      (this_player()->query_prop(BAN_I_DOODDANIA) <= 0))
        return 0;

    if((cmd == "zakoncz") || (cmd == "ukryj"))
    {
        write("To chyba nie najlepszy pomysl w tej chwili.\n");
        return 1;
    }
    
    if(member_array(cmd, exits) == -1)
        return 0;
    if(!CAN_SEE(this_object(), this_player()))
        return 0;
    band = query_bandyci();
    if((this_player()->query_prop(BAN_I_UCIEKAL)) && 
        !(this_player()->query_prop(BAN_I_ATAKOWAL)))
    {
        this_player()->add_prop(BAN_I_ATAKOWAL, 1);
        band->attack_object(this_player());
        remove_alarm(alarmy[BAN_ALARM_ID(this_player())]);
        alarmy = m_delete(alarmy, BAN_ALARM_ID(this_player()));
        if(CAN_SEE(this_object(), this_interactive()))
            command("powiedz :groznie: Nie ujdzie Ci to na sucho!");
    }
    if(!(this_player()->query_prop(BAN_I_UCIEKAL)) && !(this_player()->query_prop(BAN_I_ATAKOWAL)))
    {
        if(CAN_SEE(this_object(), this_player()))
            command("powiedz :groznie: Nie probuj tego drugi raz!");
        this_player()->add_prop(BAN_I_UCIEKAL, 1);
    }

    for(i = 0, sz = sizeof(band);i < sz;i++)
    {
        zr += band[i]->query_stat(1);
    }

    if(this_player()->query_stat(1) > zr)
    {
        write("Udaje Ci sie wyrwac bandytom!\n");
        remove_alarm(alarmy[BAN_ALARM_ID(this_player())]);
        alarmy = m_delete(alarmy, BAN_ALARM_ID(this_player()));
        return 0;
    }
    write(capitalize(short()) + " zastawia Ci droge.\n");
    say(capitalize(this_object()->short()) + " zastawia droge " + this_player()->query_Imie(this_object(), PL_CEL) + ".\n");
    return 1;
}

void
dal(object komu)
{
    if(mowi && CAN_SEE(this_object(), komu))
        command("powiedz do " + komu->query_Imie(this_object(), PL_DOP) + " Zejdz mi z oczu!");
}


void
odliczanie(object czyje, int faza)
{
    int alarmek;
    
    countdown(czyje, faza);
    if(faza < 5)
        alarmy[BAN_ALARM_ID(czyje)] = set_alarm(60.0,0.0,"odliczanie", czyje, ++faza);
}

int
widza(object kogo)
{
    return CAN_SEE(this_object(), kogo);
}

void
countdown(object czyje, int faza)
{
    if(!mowi)
        return ;
    if(CAN_SEE(this_object(), czyje))
    {
        switch(faza)
        {
                case 0: command("powiedz do " + czyje->query_Imie(this_object(), PL_DOP) +
                                " Oddawaj co masz! Bo inaczej poznasz smak miecza! Masz 5 minut..."); break;
                case 1: command("powiedz do " + czyje->query_Imie(this_object(), PL_DOP) + 
                                " Masz jeszcze 4 minuty. Dawaj!..."); break;
                case 2: command("powiedz do " + czyje->query_Imie(this_object(), PL_DOP) + 
                                " Masz jeszcze 3 minuty. Zaczynasz mi dzialac na nerwy!"); break;
                case 3: command("powiedz do " + czyje->query_Imie(this_object(), PL_DOP) + 
                                " Masz jeszcze 2 minuty. Dawaj bo tego nie przezyjesz!"); break;
                case 4: command("powiedz do " + czyje->query_Imie(this_object(), PL_DOP) + 
                                " Masz jeszcze 1 minute. A potem koniec z toba!"); break;
                case 5: command("powiedz do " + czyje->query_Imie(this_object(), PL_DOP) + 
                                " Sam tego chciales."); break;
        
                default: command("powiedz do " + czyje->query_Imie(this_object(), PL_DOP) + 
                                " Dawnom sie nie bil..."); break;
        }
    }
    if(faza == 5)
    {
        alarmy = m_delete(alarmy, BAN_ALARM_ID(czyje));
        query_bandyci()->attack_object(czyje);
    }
}
int
odkladanie (string str)
{
    object *obs;
     
    if(!mowi)
        return 0; 
    if(!str)
        return 0;
    if(!parse_command(str, environment(this_object()), "i:" + PL_BIE, obs))
        return 0;
    obs = NORMAL_ACCESS(obs, 0,0);
    obs = filter(obs, wartosc);
    if(!sizeof(obs))
        return 0;
    set_alarm(2.0, 0.0, "zbieranie", obs, this_player());
    return 0;
}

void
zbieranie (object *obs, object od)
{
    int ile = 0, value = 0;
    int i, sz, wziete;
    
    ile = od->query_prop(BAN_I_DOODDANIA);
    for(i = 0, sz = sizeof(obs);i < sz;i++)
    {
        if(this_object()->command("wez " + 
                (member_array("/std/heap.c", inherit_list(obs[i])) > -1 ? obs[i]->query_pnazwa(PL_BIE) : FO_COMPOSITE_DEAD(obs[i], this_object(), PL_BIE))
                ))
        {
                value += obs[i]->query_prop(OBJ_I_VALUE);
                wziete++;
        }
        else
            if(CAN_SEE(this_object(), od))    
                command("powiedz :przez zeby:do " + od->query_Imie(this_object(), PL_DOP) + " Nie denerwuj mnie!");
                
    }

    if(!wziete)
        return ;

    if(ile <= 0)
    {
        if(CAN_SEE(this_object(), od))        
                command("powiedz do " + od->query_Imie(this_object(), PL_DOP) +
                " Niczym wartosciowym nie pogardzimy!");
    }
    else
    {
        ile -= value;
        od->add_prop(BAN_I_DOODDANIA, ile);
        if(ile <= 0)
        {
                if(CAN_SEE(this_object(), od))
                                command("powiedz do " + od->query_Imie(this_object(), PL_DOP) +
                                " Dobrze! A teraz zejdz mi z oczu!");
                od->add_prop(BAN_I_ODDAL,1);
                remove_alarm(alarmy[BAN_ALARM_ID(od)]);
                alarmy = m_delete(alarmy, BAN_ALARM_ID(od));
        }       
        else
                if(CAN_SEE(this_object(), od))
                                command("powiedz do " + 
                                od->query_Imie(this_object(), PL_DOP) + 
                                " Dalej!");
    }
    return;
}

int
wartosc (object ob)
{
    if(ob->query_value() > 0)
        return 1;
    return 0;
}

void
zniszcz()
{
    object *inv;
    inv = deep_inventory();
    inv -= bron;
    inv->remove_object();
    set_alarm(1800.0, 0.0, "zniszcz");
}

void
catch_tell (string msg)
{
    string dummy, kogo;

    if(sscanf(msg, "Zabil%s %s.", dummy, kogo))
        set_alarm(2.0, 0.0, "zabiles", kogo);
    ::catch_tell(msg);
}

void
zabiles (string kogo)
{
    this_object()->command("wez wszystko z ciala");
}

varargs void
spotkanie (object obj = 0)
{
    int value, i, sz;
    object *invent, *bandyci;
    object ob;
    if(!objectp(obj)) 
        ob = this_interactive();
    bandyci = query_bandyci();
    
    for(i = 0,sz = sizeof(bandyci);i < sz;i++)
    {
        if(bandyci[i] == 0)
                continue;
        if(!bandyci[i]->widza(ob))
                return ;
    }
    
    if(living(ob) && (member_array(ob, bandyci) == -1)
       && (member_array("/std/player_sec.c", inherit_list(this_interactive())) >= 0)
       && !(ob->query_prop(BAN_I_ATAKOWAL)))
    {
        if(ob->query_prop(BAN_I_ODDAL) == 1)
                set_alarm(2.0, 0.0, "dal", ob);
        else
        {
                invent = deep_inventory(ob);
                for(i = 0,sz = sizeof(invent);i < sz;i++)
                    value += (invent[i]->query_prop(OBJ_I_VALUE));
                value *= proc;
                value /= 100;
                ob->add_prop(BAN_I_DOODDANIA, value);
                if(value <= 0)
                      set_alarm(2.0, 0.0, "dal", ob);
                else
                      set_alarm(2.0, 0.0, "odliczanie", ob, 0);
        }
    }
}

object*
query_bandyci()
{
    return (query_team() + ({this_object()}));
}

void
do_die(object killer)
{
    object *nasi;
    int i,j,k;

    nasi = query_bandyci();
    nasi -= ({this_object()});
    if(k = sizeof(nasi))
    {
        for(j = 0;j < k; j++)
                this_object()->team_leave(nasi[j]);
        for(j = 1; j < k;j++)
        {
                nasi[0]->team_join(nasi[j]);
                nasi[0]->add_following(nasi[j]);
                nasi[0]->set_mowi(1);
        }
        nasi[0]->set_random_move(300);
    }
    ::do_die(killer);    
}

int
ustaw_proc(int ile)
{
	if((ile <= 0) || (ile > 100))
		return 0;
	proc = ile;
	return 1;
}

void
enter_env(object dest, object old)
{
    object *obs;
    int i, sz;
    
    ::enter_env(dest, old);
    obs = all_inventory(dest);
    for(i = 0, sz = sizeof(obs);i < sz;i++)
    {
        spotkanie(obs[i]);
    }
}

int
filter_fun(mixed testowa)
{
    if(testowa[1] == "odliczanie")
        return 1;
    return 0;
}

mixed
oke_to_move(string exit)
{
    mixed *alarms;
    alarms = get_all_alarms();
    
    alarms = filter(alarms, filter_fun);
    if (sizeof(alarms) > 0)
    {
        find_player("yevaud")->catch_msg(dump_array(alarms));
        return 0;
    }
    return ::oke_to_move(exit);
}
