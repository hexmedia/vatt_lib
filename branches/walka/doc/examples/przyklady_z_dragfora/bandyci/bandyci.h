#ifndef BANDYCI_DEFS
#define PATH "/d/Faerun/std/bandyci/"
#define WEAPONS PATH + "bronie/"

#define BAN_I_DOODDANIA "_ban_i_dooddania_" + file_name(environment(this_object()))
#define BAN_I_ODDAL "_ban_i_oddal_" + file_name(environment(this_object()))
#define BAN_I_UCIEKAL "_ban_i_uciekal_" + file_name(environment(this_object()))
#define BAN_ALARM_ID(x) x->query_real_name()
#define BAN_I_ATAKOWAL "_ban_i_atakowal_" + file_name(environment(this_object()))

#define BANDYCI_DEFS
#endif

