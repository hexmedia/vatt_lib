#pragma strict_types

#include <pl.h>
#include <stdproperties.h>
#include <ss_types.h>
#include "bandyci.h"

object* bandyci = ({});
string* dane = ({});
int ilu = 0;
int prawd = 50;
int proc = 20;
int* staty =({});
int* umy = ({});
int sprawdz ();

int dodaj_bandyte(string *prasa, string *mrasa, int rodz, string *rprzym, string *pprzym,
                  string *dprzym, string dl, int bandit);
int stworz_bandytow();
int zyja ();
int ustaw_szanse(int ile);
int ustaw_proc(int ile);
void start_bandytow();

varargs int
dodaj_bandyte(string *prasa = 0, string *mrasa = 0, int rodz = 0, string *rprzym = 0,
              string *pprzym = ({}), string *dprzym = ({}), string dl = "", int bandit = 1)
{
	string temp;

	if((!prasa) || (!mrasa) || (!rodz) || (!rprzym))
	     prasa = mrasa = rodz = rprzym = 0;
    if(sizeof(pprzym) != 2)
         pprzym = ({});
    if(sizeof(dprzym) != 2)
         dprzym = ({});

	temp = (dl + "|" + implode(pprzym, ":") + "|" + implode(dprzym, ":") + "|" + implode(rprzym, ":") + "|" + implode(prasa, ":") + "|" + implode(mrasa, ":") + "|" + rodz + "|" + bandit);
	dane += ({temp});
	ilu++;
	return 1;
}

int
stworz_bandytow()
{
	int i, pierwszy = -1;
	string *elem;

	if (!ilu) return 1;

	if(!sizeof(bandyci)) bandyci = allocate(ilu);

	if(!zyja())
	{
		object *tu;
		int a, b;
		tell_room(this_object(), "Nagle bandyci wyskakuja ze swej kryjowki!\n");
		tu = all_inventory(this_object());
		for(a = 0, b = sizeof(tu) ; a < b ; a++)
		{
			if(living(tu[a]))
				set_alarm(1.0, 0.0, "enter_inv", tu[a], this_object());
		}
	}
	else
	{
	    int a,b;
	    for(a = 0, b = sizeof(bandyci);a<b;a++)
	         if(bandyci[a]) {pierwszy = a; break;}
	}
	
	for(i = 0 ; i < ilu ; i++)
	{
		if(bandyci[i] != 0) continue;
		
		elem = explode(dane[i], "|");
		bandyci[i] = clone_object(PATH + "bandyta.c");

		if(sizeof(staty) == 6) bandyci[i]->zmien_staty(staty);
		if(sizeof(umy) == 3) bandyci[i]->zmien_umy(umy);
		if(elem[0] != "") bandyci[i]->set_long(elem[0]);
		if(elem[1] != "") bandyci[i]->zmien_pprzym(explode(elem[1], ":")[0], explode(elem[1], ":")[1]);
		if(elem[2] != "") bandyci[i]->zmien_dprzym(explode(elem[2], ":")[0], explode(elem[2], ":")[1]);
		if(elem[3] != "0")
		{
		    bandyci[i]->set_rprzym(explode(elem[3], ":")[0], explode(elem[3], ":")[1]);
		    bandyci[i]->ustaw_odmiane_rasy(explode(elem[4], ":"), explode(elem[5], ":"), atoi(elem[6]));
		    bandyci[i]->add_prop(LIVE_I_NEVERKNOWN, 1);
		}
		if(atoi(elem[7]) == 1) bandyci[i]->dodaj_bandyta_short();
		if(pierwszy == -1)
        {
                bandyci[i]->set_mowi(1);
                bandyci[i]->set_random_move(300);
                bandyci[i]->move(this_object(), 1);
                pierwszy = i;
        }
		else
        {
                bandyci[i]->set_mowi(0);
                bandyci[pierwszy]->team_join(bandyci[i]);
                bandyci[pierwszy]->add_following(bandyci[i]);
                bandyci[i]->move(environment(bandyci[pierwszy]), 1);
        }
        bandyci[i]->ustaw_proc(proc);
	}
	
	return 1;
}

void
usun_bandytow()
{
    if(!zyja())
        return ;
    bandyci->remove_object();
}


int
ustaw_staty(int *nowe)
{
	if(nowe == 0)
		return 0;
	if(sizeof(nowe) != 6)
		return 0;

	staty = nowe;
	return 1;
}

int
ustaw_szanse(int ile)
{
	if((ile < 0) || (ile > 100))
		return 0;
	prawd = ile;
	return 1;
}

int
ustaw_proc(int ile)
{
	if((ile <= 0) || (ile > 100))
		return 0;
	proc = ile;
	return 1;
}

int
ustaw_umy(int *nowe)
{
	if(nowe == 0)
		return 0;
	if(sizeof(nowe) != 3)
		return 0;

	umy = nowe;
	return 1;
}

int
sprawdz()
{
	if(random(100) < prawd) return 1;
	return 0;
}

void
start_bandytow()
{
	if(sprawdz())
	{
		stworz_bandytow();
	}
}

int
zyja()
{
int i = 0, lv = 0;

	if(!ilu || !prawd || !sizeof(bandyci))
		return 0;

	for(i = 0 ; i < ilu ; i++)
		if(bandyci[i] == 0)
			lv++;
	
	if(lv == ilu)
		return 0;
	return 1;
} 
