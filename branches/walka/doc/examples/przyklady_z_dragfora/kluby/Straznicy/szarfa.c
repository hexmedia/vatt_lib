/*
 *		Bialy plaszcz... na szczescie bez krzyza
 *		Dla Rycerzy Zakonnych
 *
 *		Napisal : Mithandir ... 1998
 *
 */
 
inherit "/std/object.c";

#include <cmdparse.h>
#include <macros.h>


#include <stdproperties.h>
#include <wa_types.h>

#define TP this_player()
#define LOG "/d/Imperium/Straz/log_strazy"

#include "mith.h"
#include "emoty_strazy.h"
#include "kom_strazy.h"

#pragma no_inherit
#define STRAZ_SUBLOC "_szarfa_czlonka_Strazy_Krain_"



int zawiazana;
int wear(string co);
int remove(string co);

create_object()
{
	ustaw_nazwe (({"szarfa","szarfy","szarfie","szarfe","szarfa","szarfie"}),
		({"szarfy","szarf","szarfom","szarfy","szarfami","szarfach"}),
		PL_ZENSKI);

	dodaj_przym ("szkarlatny","szkarlatni");
	
	set_long ("Jest to szeroka, szkarlatna szarfa z wyhaftowanymi literami \"Straz Krain\".\nAby uzyskac informacje o mozliwosciach szarfy uzyj komendy 'skpomoc'.\n");
	
	seteuid("silvy");
		
	add_prop (OBJ_I_WEIGHT, 300);
	add_prop (OBJ_I_VALUE, 0);
	set_alarm(0.3,0.0,"dodaj_propy");
}
void
init()
{
    ::init ();
	add_action("remove","zdejmij");
	add_action("wear","zawiaz");
	add_action("zniszcz","zniszcz");
	init_emoty();
	init_komendy();
}

wyrzuc(object kogo)
{
   kogo->catch_msg("Decyzja Komendanta zostal" + kogo->koncowka("es","as") + 
   	" wyrzucony ze Strazy Krain. Twa szarfa zniknela!\n");
   kogo->clear_bit(1,0);
   remove_object();
}

dodaj_propy()
{
	add_prop(OBJ_M_NO_SELL, "Nie mozesz sprzedac szarfy, symbolu przynaleznosci do Strazy Krain.\n");
    add_prop(OBJ_M_NO_DROP, "Nie mozesz wyrzucic szarfy.\n");
    add_prop(OBJ_M_NO_GIVE, "Nie mozesz nikomu dac szarfy.\n");
    add_prop(OBJ_M_NO_STEAL, 1);
	add_prop(OBJ_M_NO_GET,"Nie jestes w stanie podniesc szarfy... Cos poteznego powstrzymuje twoja wole.\n");
   
   if (!ADMIN->query_czlonek(environment(this_object())->query_real_name())&&
       !environment(this_object())->query_wiz_level())
      wyrzuc(environment(this_object())); 
   else
      environment(this_object())->set_bit(1,0);

}

usun_propy()
{
	remove_prop(OBJ_M_NO_SELL);
    remove_prop(OBJ_M_NO_DROP);
    remove_prop(OBJ_M_NO_GIVE);
    remove_prop(OBJ_M_NO_STEAL);
	remove_prop(OBJ_M_NO_GET);
}


int
wear(string co)
{
	mixed pom;
	object komu;
	
	if (zawiazana) return notify_fail("Ta szarfa jest juz zawiazana.\n");
    if (!co || !wildmatch("szarfe*",co)) return notify_fail("Co chcesz zawiazac.\n");
    if (wildmatch("szarfe ??*",co)) 
    {
    	parse_command(co, environment(TP),"'szarfe' %l:"+PL_CEL,pom);
    	pom=NORMAL_ACCESS(pom,0,0);
    	if (!sizeof(pom)||sizeof(pom)>1) return notify_fail("Komu chcesz zawiazac szarfe?\n");
    	komu=pom[0];
    	if (!ADMIN->query_s_lub_k(TP->query_real_name()))
    	   return notify_fail("Nie jestes upowazniony do przyjmowania " +
    	   	"nowych Straznikow.\n");
    	if (!interactive(komu)) return notify_fail("Nie mozesz zawiazac szarfy " +komu->short(PL_CEL) + "!\n");
    	if (komu==TP) return notify_fail("Aby zawiazac sobie szarfe uzyj komendy 'zawiaz szarfe'.\n");
    	if (komu->query_subloc_obj(STRAZ_SUBLOC)) 
    	{
    		write(capitalize(komu->short())+" juz ma zawiazana szarfe.\n");
    		return 1;
    	}
    	
    	if (ADMIN->query_czlonek(komu->query_real_name()))
    	   return notify_fail(capitalize(komu->short())+" jest juz w Strazy Krain.\n");
    	
   // KLUB  
  if(komu->test_bit("imperium",1,0))
  {
    write(komu->short()+" nie moze przylaczyc sie do tego klubu, jest juz bowiem "+
      "w innym.\n");
    return 1;
  }

		usun_propy();
		setuid();
		seteuid(getuid());
    	komu->add_subloc(STRAZ_SUBLOC,this_object());
        move(komu,STRAZ_SUBLOC);
        clone_object(MASTER_OB(this_object()))->move(TP);
        ADMIN->add_member(komu->query_real_name());
        write_file(LOG,TP->query_name() + " przyj" + TP->koncowka("al","ela") +
        		" do Strazy " + komu->query_name(3) + ".\n");
        dodaj_propy();

    	write("Zawiazujesz "+komu->short(PL_CEL)+" szkarlatna szarfe.\n");
    	komu->catch_msg(QCIMIE(TP,PL_MIA)+" zawiazuje ci szkarlatna szarfe, symbol przynaleznosci do Strazy Krain. Aby uzyskac informacje o mozliwosciach szarfy uzyj komendy 'skpomoc'.\n");
    	tell_room(environment(TP),QCIMIE(TP,PL_MIA)+" zawiazuje "+QCIMIE(komu,PL_CEL)+" szkarlatna szarfe, symbol przynaleznosci do Strazy Krain.\n",({TP,komu}));

  komu->set_bit(1,0);

    	return 1;
    }	
    this_player()->add_subloc(STRAZ_SUBLOC, this_object());
    write("Zawiazujesz sobie na skos na piersiach szkarlatna szarfe, symbol czlonkowstwa w Strazy Krain.\n");
    say(QCIMIE(TP,PL_MIA)+" zawiazuje sobie na skos na piersiach szkarlatna szarfe z napisem \"Straz Krain\". \n");
    move(TP, STRAZ_SUBLOC);
    zawiazana=1;
//	update_actions();
    return 1;
}

int
remove(string co)
{
    notify_fail("Co chcesz zdjac?\n");
    if (!(co=="szarfe")) return 0;
    if (!zawiazana) return notify_fail("Ta szarfa nie jest zawiazana.\n");
    this_player()->remove_subloc(STRAZ_SUBLOC);
    move(TP);
    zawiazana=0;
    write("Zdejmujesz szkarlatna szarfe.\n");
    say(QCIMIE(TP,PL_MIA)+" zdejmuje szkarlatna szarfe.\n") ;
    
//	update_actions();
    return 1;
}

void
leave_env(object from, object to)
{

    this_player()->remove_subloc(STRAZ_SUBLOC);
    ::leave_env(from, to);
}

string
show_subloc(string subloc, object on_obj, object for_obj)
{
    if (on_obj == for_obj)
        return "Szkarlatna szarfa jest symbolem przynaleznosci do Strazy Krain.\n";
    return "Przepasan" + on_obj->koncowka("y","a") +" jest szkarlatna szarfa "
        + "z wyhaftowanym napisem \"Straz Krain\".\n";
}

string 
query_auto_load ()
{
        return MASTER_OB(this_object());
}

int
zniszcz(string str)
{
	if (str!="szarfe") return notify_fail("Co chcesz zniszczyc?\n");
	write("Niszczysz szarfe na znak opuszczenia Strazy Krain.\n");
	say(QCIMIE(TP, PL_MIA)+" niszczy szarfe na znak opuszczenia Strazy Krain.\n");
	(ADMIN->remove_member(TP->query_real_name()));
	write_file(LOG, TP->query_name() + " porzucil" + TP->koncowka("","a") +
		" Straz Krain.\n");
	this_player()->clear_bit(1,0);
	remove_object();
	return 1;
}
