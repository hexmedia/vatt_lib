inherit "/d/std/zbroja";

#include <wa_types.h>
#include <macros.h>
#include <stdproperties.h>

void
create_armour()
{
    set_autoload();
   ustaw_nazwe( ({ "szyszak", "szyszaka", "szyszakow", "szyszak", "szyszakiem",
                   "szyszaku" }), ({ "szyszaki", "szyszakow", "szyszakom",
                   "szyszaki", "szyszakami", "szyszakach" }), PL_MESKI_NOS_NZYW );

   dodaj_nazwy( ({ "helm", "helmu", "helmowi", "helm", "helmem",
                   "helmie" }), ({ "helmy", "helmow", "helmom",
                   "helmy", "helmami", "helmach" }), PL_MESKI_NOS_NZYW );


    dodaj_przym("polyskliwy","polyskliwi");
    dodaj_przym("solamnijski","solamnijscy");

   set_long("Jest to polyskujacy szyszak wykonany ze stali. Stal jest bardzo cienka, "+
   	"wyscielana wewnatrz miekkim materialem. Z cala pewnoscia jest on bardzo "
   	+"wygodny, jednak bardzo watpliwa jest jego wytrzymalosc na ciosy, szczegolnie "
   	+"zadawane przy pomocy broni obuchowych.\n");


   set_ac(A_HEAD, 15, 20, 15);
}
                           