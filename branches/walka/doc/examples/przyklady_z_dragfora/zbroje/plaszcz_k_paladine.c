inherit "/d/std/zbroja";
#include <stdproperties.h>;
#include <wa_types.h>;
#include <formulas.h>;
#include <macros.h>;


void
create_armour()
{
	set_autoload();
	ustaw_nazwe(({"plaszcz", "plaszcza", "plaszczowi", "plaszcz", "plaszczem", 
		      "plaszczu"}), ({"plaszcze", "plaszczy", "plaszczom", "plaszcze",
		      "plaszczmi", "plaszczach"}), PL_MESKI_NZYW);

	dodaj_przym("bialy", "biali");
	dodaj_przym("piekny", "piekni");
		       
	set_long("Bialy plaszcz wykonany z lekkiego materialu. Posiada on "
	+"kaptur, ktorym mozna oslonic glowe podczas jazdy konnej. Na srodku plaszcza "
	+"widzisz wyszytego zlota nicia smoka, znak kaplanow Paladina.\n");
	
	set_ac(A_BODY, 3, 3, 3, A_ARMS, 3, 3, 3, A_LEGS, 3, 3, 3);
	set_slots(A_ROBE);
	add_prop(OBJ_I_VALUE, 150);
}