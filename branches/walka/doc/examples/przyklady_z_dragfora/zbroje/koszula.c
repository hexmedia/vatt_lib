inherit "/d/std/zbroja";
#include "../pal.h"

void create_armour()
{
    set_autoload();
    ustaw_nazwe(
    ({ "koszula", "koszuli", "koszuli", "koszule", "koszula", "koszuli" }),
    ({ "koszule", "koszul", "koszulom", "koszule", "koszulami", "koszulach" }),
    PL_ZENSKI);

    dodaj_przym("bialy", "biali");
    dodaj_przym("lniany","lniani");

    set_long("Biala, lniana koszula, noszona przez wielu mezczyzn ze wzgledu na to, "
    +"ze jest bardzo wygodna, chociaz nie jest specjalnie modna. Ma nakrochmalony, "
    +"twardy, postawiony kolnierz.\n");
             
    set_ac(A_BODY, 2, 3, 2);
}
