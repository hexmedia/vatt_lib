#pragma strict_types
inherit "/std/object";
#include <stdproperties.h>
#include <wa_types.h>
#include <ss_types.h>
#include <pl.h>
#include <macros.h>
#include <composite.h>
#include <std.h>
#include <files.h>
#include <cmdparse.h>
#include <language.h>
#define TP this_player()
#define TO this_object()
#define PLAYER_I_SCINAM "_player_i_scinam"
#define PLAYER_I_OCIOSUJE "_player_i_ociosuje"
int ile_desek;
int jak_trudno_sciac;
int ktos_ociosuje;
void
create_object()
{
ustaw_nazwe(({"belka","belki","belce","belke","belka","belce"}),
        ({"belki","belek","belka","belki","belkami","belkach"}),
            PL_ZENSKI);
set_long("Jest to belka jakiegos drzewa. Moze ci posluzyc do podtrzymania "+
"jakiegos stropu badz do pozyskania desek.\n");
}
void
ustaw_ile_desek(int ile)
{
ile_desek=ile;
    switch(ile)
    {
case 1:
        dodaj_przym("maly","mali");
        add_prop(OBJ_I_VOLUME,5000/3);
        add_prop(OBJ_I_WEIGHT,7000/2);
break;
case 2:
        dodaj_przym("dlugi","dludzy");
        add_prop(OBJ_I_VOLUME,9000/3);
        add_prop(OBJ_I_WEIGHT,11000/2);    
break;
case 3:
        dodaj_przym("duzy","duzi");
        add_prop(OBJ_I_VOLUME,13000/3);
        add_prop(OBJ_I_WEIGHT,15000/2);         
break;
case 4:
        dodaj_przym("wielki","wielcy");
        add_prop(OBJ_I_VOLUME,17000/3);
        add_prop(OBJ_I_WEIGHT,19000/2);    
break;
case 5:
        dodaj_przym("ogromny","ogromni");
        add_prop(OBJ_I_VOLUME,20000/3);
        add_prop(OBJ_I_WEIGHT,23000/2);    
break;
    }    
}
void
set_zajete(int x)
{
    ktos_ociosuje=x;
}
int
query_zajete()
{
    return ktos_ociosuje;
}
int
query_drzewo()
{
    return 1;
}
int
query_forma()
{
    return 2;
}
int
query_ile_desek()
{
    return ile_desek;
}
void
zabral_deske()
{
    ile_desek--;
}

void
init()
{
    ::init();
    add_action("ociosaj","ociosaj");
}
int
ociosaj(string str)
{
mixed *drzewa;
object *bronie;
object paraliz;
int id;
int wt;
int i;
int z;

    notify_fail("Co chcesz ociosac ?\n");
    if(!str) return 0;
    
    parse_command(str,environment(TO),"%i:"+PL_BIE,drzewa);
    drzewa=NORMAL_ACCESS(drzewa,0,0);
    
    if (!sizeof(drzewa)) return 0;
    
    if (sizeof(drzewa)>1) return 0;
    if (drzewa[0]->query_drzewo()!=1) return 0;
    if (environment(TO)->query_living()==1)
    {
        TP->catch_msg("Moze wpierw odlozysz to co chcesz ociosywac ?\n");
        return 1;
    }
    bronie=TP->query_weapon(-1);
    if(!sizeof(bronie))
    {
        write("Chyba nie chcesz ociosywac drzewa golymi rekoma ?\n");
        return 1;
    }        
    for(i=0;i<sizeof(bronie);i++)
    {
        wt=bronie[i]->query_wt();
        if(wt!=W_AXE)
        {
            write("Nie masz odpowiedniego narzedzia.\n");
            return 1;
        }
    }
    if(TP->query_stat(0)<36)
    {
        write("Chyba jestes za slab"+TP->koncowka("y","a","e")+".\n");
        return 1;
    }
    if(TP->query_fatigue()<30)
    {
        write("Jestes zbyt zmeczon"+TP->koncowka("y","a","e")+".\n");
return 1;
    }
     if(drzewa[0]->query_zajete()==1)
    {
        write("Nie mozesz tego ociosywac gdyz juz ktos inny to robi.\n");
        return 1;
    }
    paraliz=clone_object("/d/Faerun/drzewa/ociosajparaliz");
    TP->add_prop("_object_drzewa_",drzewa[0]);
    TP->add_prop(PLAYER_I_OCIOSUJE,1);
     switch(drzewa[0]->query_forma())
    {
    case 0:   //drzewo nie ociosane
          paraliz->move(TP);
          TP->catch_msg("Zaczynasz ociosywac "+drzewa[0]->query_short(PL_BIE)+".\n");
          saybb(QCIMIE(TP,PL_MIA)+" zaczyna ociosywac "+drzewa[0]->query_short(PL_BIE)+".\n");
          drzewa[0]->set_zajete(1);
          id=set_alarm(30.0,0.0,"ociosaj_faza_1",TP,paraliz,drzewa[0]);
          TP->add_prop("_ociosywanie_drzew_alarm_id_",id);
      break;
      case 1:  //drzewo ociosane
          paraliz->move(TP);
          TP->catch_msg("Zaczynasz ociosywac "+drzewa[0]->query_short(PL_BIE)+".\n");
          saybb(QCIMIE(TP,PL_MIA)+" zaczyna ociosywac "+drzewa[0]->query_short(PL_BIE)+".\n");
          drzewa[0]->set_zajete(1);
          id=set_alarm(35.0,0.0,"ociosaj_faza_2",TP,paraliz,drzewa[0]);
          TP->add_prop("_ociosywanie_drzew_alarm_id_",id);
      break;
      case 2: //belka
          paraliz->move(TP);
          TP->catch_msg("Zaczynasz ociosywac "+drzewa[0]->query_short(PL_BIE)+".\n");
          saybb(QCIMIE(TP,PL_MIA)+" zaczyna ociosywac "+drzewa[0]->query_short(PL_BIE)+".\n");
          drzewa[0]->set_zajete(1);
          id=set_alarm(35.0,0.0,"ociosaj_faza_3",TP,paraliz,drzewa[0]);
          TP->add_prop("_ociosywanie_drzew_alarm_id_",id);
    break;
    }
return 1;
}


void
ociosaj_faza_1(object pl,object par,object drzewko)
{
 int waga,objetosc;
    if(!present("ociosajparaliz",pl))
    {
        pl->remove_prop(PLAYER_I_OCIOSUJE);
        drzewko->set_zajete(0);
        return;
    }
 if(!pl->query_prop(PLAYER_I_OCIOSUJE)) return;
if(!present(pl,environment(TO))) return;
 if(!pl) return;
 par->remove_object();
 pl->catch_msg("Konczysz ociosywac "+drzewko->query_short(PL_BIE)+".\n");
 saybb(QCIMIE(pl,PL_MIA)+" konczy ociosywac "+drzewko->query_short(PL_BIE)+".\n");
 drzewko->dodaj_przym("ociosany","ociosani");
    drzewko->odmien_short();
    drzewko->odmien_plural_short();
 drzewko->set_forma();
 waga=drzewko->query_prop(OBJ_I_WEIGHT);
 objetosc=drzewko->query_prop(OBJ_I_VOLUME);
 drzewko->change_prop(OBJ_I_WEIGHT,waga/3);
 drzewko->change_prop(OBJ_I_VOLUME,objetosc/2);
 drzewko->set_zajete(0);
 pl->remove_prop(PLAYER_I_OCIOSUJE);
 pl->remove_prop("_ociosywanie_drzew_alarm_id_");
pl->remove_prop("_object_drzewa_");
}
void
ociosaj_faza_2(object pl, object par, object drzewko)
{
    object belka;
if(!present("ociosajparaliz",pl))
    {
        pl->remove_prop(PLAYER_I_OCIOSUJE);
    drzewko->set_zajete(0);
        return;
    }
    if(!pl->query_prop(PLAYER_I_OCIOSUJE)) return;
    if(!present(pl,environment(TO))) return;
    if(!pl) return;
    par->remove_object();
    pl->catch_msg("Konczysz ociosywac "+drzewko->query_short(PL_BIE)+".\n");
    saybb(QCIMIE(pl,PL_MIA)+" konczy ociosywac "+drzewko->query_short(PL_BIE)+".\n");
    pl->remove_prop(PLAYER_I_OCIOSUJE);
    pl->remove_prop("_ociosywanie_drzew_alarm_id_");
    pl->remove_prop("_object_drzewa_");
    belka=clone_object("/d/Faerun/drzewa/belka");
    belka->ustaw_ile_desek(drzewko->query_ilosc_desek());
    belka->move(environment(drzewko));
drzewko->remove_object();
}
void
ociosaj_faza_3(object pl, object par, object drzewko)
{
    object deska;
    int zr;
if(!present("ociosajparaliz",pl))
    {
        pl->remove_prop(PLAYER_I_OCIOSUJE);
    drzewko->set_zajete(0);
        return;
    }
    if(!pl->query_prop(PLAYER_I_OCIOSUJE)) return;
    if(!present(pl,environment(TO))) return;
    if(!pl) return;
    par->remove_object();
    pl->catch_msg("Konczysz ociosywac "+drzewko->query_short(PL_BIE)+".\n");
    saybb(QCIMIE(pl,PL_MIA)+" konczy ociosywac "+drzewko->query_short(PL_BIE)+".\n");
    pl->remove_prop(PLAYER_I_OCIOSUJE);
    pl->remove_prop("_ociosywanie_drzew_alarm_id_");
    pl->remove_prop("_object_drzewa_");
    drzewko->zabral_deske();
    deska=clone_object("/d/Faerun/drzewa/deska");
    zr=pl->query_stat(SS_DEX);
    switch(zr)
    {
    case 0..30:
    deska->ustaw_jakosc(1);
    break;
    case 31..50:
    deska->ustaw_jakosc(2);
    break;
    case 51..300:
    deska->ustaw_jakosc(3);
    break;
    }
    deska->move(environment(drzewko));
    if(drzewko->query_ile_desek()==0) drzewko->remove_object();
    drzewko->set_zajete(0);
}
void
usun_alarma(int numer)
{
remove_alarm(numer);
}
