#pragma strict_types
#pragma no_clone
inherit "/std/room.c";
#include <stdproperties.h>

string store_room;

void
set_store_room(string str)
{
store_room = str;
}

void
create_room()
{
    set_short("Miejsce gdzie tnie sie drewno");
    set_long("Magazyn w ktorym tnie sie drewno z tartaku.\n");
    
}

void
tnij(object ob)
{
object *des;

if (!(ob->query_drzewo())) return;

des = ob->query_deski();
ob->remove_object();
if (!sizeof(des)) return;

des->move(store_room);

}


void
potnij(object ob)
{
if (!stringp(store_room)) 
store_room = previous_object()->query_store_room();
set_alarm(60.0,0.0,"tnij",ob);
}