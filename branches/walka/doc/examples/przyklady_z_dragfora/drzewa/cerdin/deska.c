#pragma strict_types

inherit "/std/object.c";

#include <stdproperties.h>
#include "/sys/tartak.h"
#include <pl.h>
#define QP(x)	query_prop((x))

int dl = 1, przelicznik = 30+random(20);

string
query_deska()
{
return QP(TR_M_TYP)[0];
}

void
create_object()
{
ustaw_nazwe(({"deska","deski","desce","deske","deska","desce"}),
    ({"deski","desek","deskom","deski","deskami","deskach"}),
    PL_ZENSKI);
add_prop(OBJ_M_NO_SELL,1);
}

string
long()
{
return "Jest to "+query_przym(1,0,4)[0]+", nieoheblowana deska.\n";
}

void
update_deska()
{
dl = random(3)+1;
add_prop(OBJ_I_VOLUME, 200*dl*random(7));
add_prop(OBJ_I_WEIGHT, 150*dl*random(7));
add_prop(OBJ_I_VALUE,  dl*przelicznik);
dodaj_przym(QP(TR_M_TYP)[1],QP(TR_M_TYP)[2]);
switch(dl)
    {
    case 1: dodaj_przym("krotki","krotcy"); break;
    case 2: dodaj_przym("sredni","sredni"); break;
    case 3: dodaj_przym("dlugi","dludzy"); break;
    }
set_long(long);
odmien_short();
odmien_plural_short();
}