#pragma strict_types
inherit "/d/std/bron";
#include <filter_funs.h>
#include <ss_types.h>
#include <wa_types.h>
#include <stdproperties.h>
#include <wa_types.h>
#include <cmdparse.h>
#include <macros.h>
#include <pl.h>
#include <options.h>
#include <macros.h>
#include <formulas.h>
#include <const.h>
#define CO_ILE 10
int ktory_cios;
#define TP this_player()
object wielder;

void
create_weapon() 
{
    set_autoload();
     ustaw_nazwe(({ "miecz", "miecza", "mieczowi", "miecz", "mieczem",
"mieczu"}),
                 ({ "miecze", "mieczow", "mieczom", "miecze", "mieczami", 
                    "mieczach" }), PL_MESKI_NOS_NZYW);
                    
     dodaj_przym( "zakrzywiony", "zakrzywieni" );
     dodaj_przym( "polyskujacy", "polyskujacy" );
                    
     set_long("Jest to wyjatkowo lekki i poreczny miecz jakiego uzywaja "
        +"elfi zolnierze. Wykonany z nienajlepszego metalu stanowi lepsza "
        +"bron od niejednej broni wykonanej z duzo lepszych materialow. "
        +"Rekojesc jest oblozona dziwnym niebieskim materialem, nie masz "
        +"pojecia jakim, natomiast klinga jest przykryta u konca cieniutka "
        +"warstwa admantytu. Bron ta sprawia wrazenie bardzo dobrze "
        +"wywazonej i w odpowiednich rekach zabojczej... Od broni bije "
        +"dziwne i lekko tajemnicze jaskrawo-zielone swiatlo, ktore "
        +"budzi w tobie zachwyt, ale jednoczesnie lek...\n");

     set_hit(20);
     set_pen(15);
     add_prop(OBJ_I_IS_MAGIC_WEAPON, 1);
     // moze nie ma najlepszych statow to napewno zasluguje na miano magika
     set_wt(W_SWORD);
     set_dt(W_SLASH);
     set_hands(W_ANYH);     
     add_prop(OBJ_I_VALUE, 450+random(500));
     add_prop(OBJ_I_WEIGHT, 2000);
     add_prop(OBJ_I_VOLUME, query_prop(OBJ_I_WEIGHT)/5);
 }
int
did_hit(int aid, string hdesc, int phurt, object enemy, int dt, int phit, int
dam)
{
        object *ja = FILTER_LIVE(all_inventory(environment(this_player()))) -
({ this_player() });
        int i;
        mixed* rezultat;
        string jak;
        ::did_hit();
        if (ktory_cios < CO_ILE) ktory_cios++;
        if ((this_player()->query_stat(SS_DEX) > 50) && (ktory_cios == CO_ILE))
        {
         rezultat=enemy->hit_me(2*((this_player()->query_stat(SS_DEX)+
	 this_player()->query_stat(SS_WIS))), 50, W_SLASH, ja, -1);
         ja-= ({ enemy});
         // opiszemy specjal
         jak = "niezauwazalnie";
         if (rezultat[0] > 5) jak = "nieznacznie";
         if (rezultat[0] > 8) jak = "zauwazalnie";
         if (rezultat[0] > 10) jak = "lekko";
         if (rezultat[0] > 15) jak = "znacznie";
         if (rezultat[0] > 20) jak = "niezle";
         if (rezultat[0] > 25) jak = "solidnie";
         if (rezultat[0] > 30) jak = "dotkliwie";
         if (rezultat[0] > 40) jak = "mocno";
         if (rezultat[0] > 50) jak = "ciezko";
         if (rezultat[0] > 55) jak = "bardzo ciezko"; 
         if (rezultat[0] > 60) jak = "okropnie"; // itp
         if (enemy->query_hp() <= 0)
         {
                 this_player()->catch_msg("Miecz w twoich rekach zaczyna
wirowac, zdaje sie, ze przejal "
        +"nad soba kontrole. Nim zdazyl"+ this_player()->koncowka("es", "as")+"
nad nim zapanowac lub "
        +"zmiarkowac co sie dzieje dostrzegasz jak dzieki calkowicie szalonemu
i niemalze nieprzewidywalnemu "
        +"cieciu miecz przecina skronie przeciwnika...\n");
                 enemy->catch_msg("Bron w rekach " + QIMIE(this_player(),
PL_DOP) + " zaczyna wirowac z zawrotna "
        +"predkoscia. Dzieki calkowicie nieprzewidywalnemu i wrecz szalonemu
cieciu " + QIMIE(this_player(), PL_DOP)
        +" udaje sie cie zaskoczyc. Nim zdazysz zmiarkowac co sie dzieje, miecz
kontrolowany juz nie przez "
        +"przeciwnika, ale chyba przez jakies boskie moce przecina twoja skron.
Nie widzisz juz nic. Oprocz "
        +"jednego. Oprocz ciemnosci... \n");
                 for(i=0; i<sizeof(ja); i++)
                  ja[i]->catch_msg("Miecz w rekach " + QIMIE(this_player(),
PL_DOP) +
        " zaczyna wriowac z zawrotna szybkoscia, tak ze przewidzenie nastepnego
jego ruchu czy posuniecia "
        +"graniczy chyba z cudem. Calkowicie szalonym cieciem, ktorego w zaden
sposob nie mozna "
        +"bylo przewidziec " + QIMIE(this_player(), PL_CEL) + " udalo sie
zaskoczyc " + QIMIE(enemy, PL_DOP) 
        +". "+ enemy->koncowka("Ten", "Ta") +" pada z lekkimi jeszcze
konwulsjami na ziemie. Cialo jest "
        +"cale we krwi... \n");
        enemy->do_die(this_player());
                 return 1;
         }
         enemy->catch_msg(QCIMIE(this_player(), PL_MIA) + " ze stanowcza mina
wykonuje serie zwodow, a gdy "
        +"spostrzega, ze jestes calkowicie zdezorientowan" + enemy-
>koncowka("y", "a") +
        " wyprowadza potezny cios raniac cie " + jak + ".\n");
         this_player()->catch_msg("Ze stanowcza mina wykonujesz serie zwodow,
po chwili zauwazajac "
        +"zdezorientowana mine przeciwnika wyprowadzasz potezny cios znad glowy
" + jak + " " + enemy->koncowka("go", "ja") +
                       " raniac.\n");
         for(i=0; i<sizeof(ja); i++)
          ja[i]->catch_msg(QCIMIE(this_player(), PL_MIA) + " ze stanowcza mina
wykonuje serie zwodow, a po chwili "
        +"zauwazajac zdezorientowana mine "+QIMIE(enemy, PL_DOP) + " wyprowadza
potezne ciecie znad glowy raniac "
        +enemy->koncowka("go", "ja")+" " + jak + ".\n");
         return 2;
       }
       return 0;
}
mixed
wield(object what)
{
   set_alarm(0.1,0.0,"add_wielder");
   TP->set_skill_extra(0, 30);
   TP->set_skill_extra(23, 30);
   write("Miecz mieni sie barwami teczy, lekko cie oslepiajac.\n");
   say("Miecz mieni sie wszystkimi barwami teczy, lekko cie oslepiajac, gdy "+
       QCIMIE(this_player(),PL_MIA) + " dobywa go.\n");
   return 1;
}
void
add_wielder()
{
    wielder = environment(this_object());
}
object
query_wielder()
{
    return wielder;
}    
mixed
unwield(object what)
{
   set_this_player(wielder);
   if(environment(this_object())!=wielder) 
       return 1;
   TP->set_skill_extra(0, 0);
   TP->set_skill_extra(23, 0);
   write("Ostrze jakby przygasa gdy opuszczasz swa bron...\n");
   say(QCIMIE(this_player(),PL_MIA)+" opuszcza swoj elfi miecz, a ten "
        +"przygasa nieco.\n");     
   return 1;
}