/*
 * Filename: mlot_kowalski1.c
 *
 * Zastosowanie: Mlot kowalski.
 * 
 * Copyright Volothamp Geddarm.
 *
 */
inherit "/d/std/bron";  

#include <filter_funs.h>
#include <ss_types.h>
#include <wa_types.h>
#include <stdproperties.h>
#include <wa_types.h>
#include <cmdparse.h>
#include <macros.h>
#include <pl.h>
#include <options.h>
#include <macros.h>
#include <formulas.h>
#include <const.h>

#define TP this_player()
object wielder;

    void
    create_weapon()
{
    set_autoload();  
    ustaw_nazwe(({ "mlot kowalski", "mlota kowalskiego", "mlotowi kowalskiemu", "mlot kowalski", "mlotem kowalskim", "mlocie kowalskim" }),
                ({ "mloty kowalskie", "mlotow kowalskich", "mlotom kowalskim", "mloty kowalskie", "mlotami kowalskimi", "mlotach kowalskich" }), PL_MESKI_NOS_NZYW); 
    
	dodaj_nazwy(({ "mlot","mlota","mlotowi","mlot","mlotem","mlocie"}),
		        ({ "mloty","mlotow","mlotom","mloty","mlotami","mlotach"}),PL_MESKI_NOS_NZYW);

	dodaj_przym("lekki", "leccy");

    set_long("Jest to wykonany z zelaza typowy mlot kowalski jaki stosowany jest przez mlodych kowali, "
	  + "ktorzy dopiero praktykuja swoj kunszt jako czeladnicy. Wyglada on na bardzo lekki i poreczny. "
	  + "Nawet, iz nie jest on w pewnym sensie wyrobem kuzni posiada on na trzonie jej osobisty "
	  + "i jak widac nieodzowny znak.\n");
    
	set_hit(10);
    set_pen(10);
    
	set_wt(W_WARHAMMER);

	set_dt(W_BLUDGEON);
    
	set_hands(W_ANYH);
    
	add_prop(OBJ_I_WEIGHT, 1500);                
	add_prop(OBJ_I_VALUE, 10);
}

/*----Podpisano: 'Volothamp Geddarm'----*/


  

