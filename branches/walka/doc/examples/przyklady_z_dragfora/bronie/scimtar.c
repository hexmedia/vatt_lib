#pragma strict_types
inherit "/d/std/bron";

#include <stdproperties.h>
#include <macros.h>
#include <filter_funs.h>
#include <wa_types.h>
#include <ss_types.h>
#include <formulas.h>
#include <options.h>
object wielder;
void
create_weapon() 
{
     set_autoload();
     ustaw_nazwe(({ "scimtar", "scimtara", "scimtarowi", "scimtar", "scimtarem", "scimtarze" }),
                 ({ "scimtary", "scimtarow", "scimtara", "scimtary", "scimtarami", 
                    "scimtarach" }), PL_MESKI_NOS_NZYW);

     dodaj_przym( "smukly","smukli");                    
     dodaj_przym( "lsniacy","lsniace");                    
                    
set_long("Spogladasz na jeden z najpiekniejszych okazow broni jakie widziales. "+
"Dlugie, delikatnie lsniace ostrze o lekko zakrzywionym ksztalcie w calosci "+
"pokryte jest drobnymi niczym pajecza siec ornamentami i zdobieniami. "+
"Wspaniale odbija refleksy swiatla nawet przy drobnym poruszeniu. "+
"Tnaca krawedz wykonano z ciemnego niczym noc metalu po ktorym zdaja sie "+
"pelzac nieznaczne nimby swiatla. Rekojesc zrobiono z pieknie rzezbionego "+
"jeleniego rogu w ktory wprawiony zostal duzy, wielokaratowy rubin. "+
"Jest to misternie wykonane elfie ostrze, ktore procz "+
"swych walorow estetycznych zdaje sie byc takze smiertelnie grozne.\n");

add_item("rubin","Wspaniale oszlifowany krwiscie czerwony rubin. "+
"Po dokladniejszym przygladnieciu sie nie odnajdujesz na nim "+
"najmniejszej skazy. Rozszczepia nawet najdrobniejsze promyki "+
"slonca tworzac godne podziwu kaskady swiatla we wszystkich kolorach teczy.\n");
     set_hit(30); //latwo trafic
     set_pen(50); //duze obrazenia

     set_wt(W_SWORD);
     set_dt(W_SLASH);

     set_hands(W_ANYH); //dwie lapki
     

     add_prop(OBJ_I_VALUE, 25000); //25 zlota
     add_prop(OBJ_I_WEIGHT, 5000); //duzo jak na jedna reke
     add_prop(OBJ_I_VOLUME, query_prop(OBJ_I_WEIGHT)/5);
    set_wf(this_object());
    set_alarm(0.5,0.0,"add_wielder");
 }
mixed
wield(object what)
{
   set_alarm(0.1,0.0,"add_wielder");
   
write("Plynnym ruchem dobywasz smuklego lsniacego scimitara wycinajac "+
"nim przy okazji w powietrzu kilka furkoczacych mlyncow. Rubin w rekojesci "+
"zdaje sie przez chwile sciemniec do koloru purpury by po chwili wrocic do "+
"krwistoczerwonej barwy ... a moze to tylko gra swiatla.\n");

say("Widzisz jak "+QIMIE(this_player(),PL_MIA)+" plynnym ruchem dobywa smuklego "+
"lsniacego scimitara wycinajac nim przy okazji kilka furkoczacych mlyncow. "+
"Rubin w rekojesci zdawal sie przez chwile sciemniec do koloru purpury, "+
"by po chwili wrocic do krwistoczerwonej barwy... a moze to tylko gra swiatla.\n"); 
   return 1;
}

void
add_wielder()
{
    wielder = environment(this_object());
}

object
query_wielder()
{
    return wielder;
}    

mixed
unwield(object what)
{
   set_this_player(wielder);
   if(environment(this_object())!=wielder) 
       return 1;
   
write("Opuszczasz smukly lsniacy scimitar mimowolnie pieszczac dlonia "+
"jego ostrze. Twa dlon zdaje sie juz do niego tesknic.\n");

say("Widzisz jak "+QCIMIE(this_player(),PL_MIA)+" opuszcza smukly "+
"lsniacy scimitar delikatnie go przy tym glaszczac i nieznacznie usmiechajac "+
"sie.\n");       
   return 1;
}

static void
tell_watcher(string str, object enemy)
{
   object me,*ob;
   int i;

   me = wielder;
   ob = FILTER_LIVE(all_inventory(environment(me))) - ({ me });
   ob -= ({ enemy });
   for (i = 0; i < sizeof(ob); i++)
   if (!ob[i]->query_option(OPT_BLOOD) && CAN_SEE_IN_ROOM(ob[i]))
      {
      if (CAN_SEE(ob[i], me))
         ob[i]->catch_msg(str);
      else
         tell_object(ob[i], "Ktos trafil kogos.\n");
   }
}

public mixed
did_hit(int aid,string hdesc,int phurt,object enemy,int dt,int phit,int dam,int tohit,mixed
def_ob,object armour)
{
   wielder = query_wielded();
    switch(phurt)
   {
      case -1:
      case 0:
      wielder->catch_msg("Skrecasz sie w piruecie nadajac ped ostrzu, "+
      "ktore z sykiem rozcina powietrze prawie trafiajac "+
      "w "+QIMIE(enemy,PL_DOP)+".\n");
      enemy->catch_msg(QCIMIE(wielder,PL_MIA)+" skreca sie w piruecie nadajac "+
      "ped ostrzu smuklego lsniacego scimitara, ktorego klinga z sykiem "+
      "rozcina powietrze prawie cie trafiajac.\n");
      tell_watcher(QCIMIE(wielder,PL_MIA)+" skreca sie w piruecie nadajac "+
      "ped ostrzu smuklego lsniacego scimitara ktorego klinga z sykiem "+
      "rozcina powietrze obok "+QIMIE(enemy,PL_DOP)+" prawie "+
      enemy->koncowka("go","ja","je")+" trafiajac.\n",enemy);
      break;
      case 1..16:
      wielder->catch_msg("Markujesz ciecie w glowe "+QIMIE(enemy,PL_DOP)+
      "po czym blyskawicznie zmieniasz kierunek ciosu i omijajac zastawe "+
      "przeciwnika trafiasz go w "+hdesc+" nieznacznie raniac.\n");
      enemy->catch_msg(QCIMIE(wielder,PL_MIA)+" wyprowadza ciecie w twa glowe, "+
      "gdy skladasz zastawe ostrze smuklego lsniacego scimitara blyskawicznie "+
      "zmienia kierunek lotu trafjajac cie w "+hdesc+" na szczescie "+
      "nieznacznie tylko raniac.\n");
      tell_watcher(QCIMIE(wielder,PL_MIA)+" wyprowadza cios w glowe "+
      QIMIE(enemy,PL_DOP)+", nagle blyskawicznie zmienia tor lotu "+
"ostrza smuklego lsniacego scimitara zrecznie omijajac zastawe "+
"i trafia "+enemy->koncowka("go","ja","je")+" w "+hdesc+
" nieznacznie raniac.\n",enemy);
      break;
      case 17..33:
      wielder->catch_msg("Szybkim zwodem zmylasz przeciwnika i blyskawicznie "+
      "tniesz samym koncem ostrza w "+hdesc+" pozostawiajac lekka krwawiaca "+
      "rane.\n");
      enemy->catch_msg(QCIMIE(wielder,PL_MIA)+" szybkim zwodem ciala zmyla "+
      "twa parade i blyskawicznie tnie cie samym koncem ostrza smuklego "+
      "lsniacego scimitara w "+hdesc+" pozostawiajac bolesna, "+
      "lekko krwawiaca rane.\n");
      tell_watcher(QCIMIE(wielder,PL_MIA)+" szybkim zwodem ciala zmyla "+
      "parade "+QIMIE(enemy,PL_DOP)+" i blyskawicznie tnac samym "+
"koncem ostrza smuklego lsniacego scimitara trafia w "+
hdesc+ " pozostawiajac lekko krwawiaca rane.\n",enemy);
      break;
      case 34..50:
      wielder->catch_msg("Zataczasz bronia szybka osemke nadajac glowni "+
      "predkosc, po czym tniesz z polobrotu w "+QIMIE(enemy,PL_DOP)+
      "trafiajac "+enemy->koncowka("go","ja","je")+" prosto w "+
      hdesc+". Slyszysz jak klinga zgrzyta o kosc, a katem oka "+
      "zauwazasz warkocz posoki ciagnacy sie za ostrzem.\n");
      enemy->catch_msg(QCIMIE(wielder,PL_MIA)+" zatacza bronia szybka "+
      "osemke nadajac glowni smuklego lsniacego scimitara "+
"predkosc, po czym tnie cie blyskawicznie z polobrotu trafiajac prosto w "+
hdesc + ". Czujesz jak ostrze przecina miesnie i "+
"zgrzyta o twoje kosci. Rozchodzace sie cieplo swiadczy zas o obficie plynacej "+
"z rany krwii.\n");
tell_watcher(QCIMIE(wielder,PL_MIA)+" zatacza bronia szybka osemke nadajac "+
"glowni smuklego lsniacego scimitara predkosc, po czym tnie z polobrotu "+
"w "+QIMIE(enemy,PL_DOP)+" trafiajac "+enemy->koncowka("go","ja","je")+
" prosto w "+hdesc + ". Slyszysz jak klinga zgrzyta o kosc, "+
"a katem oka zauwazasz warkocz posoki ciagnacy sie za ostrzem.\n",enemy);
      break;
      case 51..66:
wielder->catch_msg("Dopadasz szybkim skokiem do "+QIMIE(enemy,PL_DOP)+
" wykonujac potezne pchniecie w "+hdesc+", przeciwnik stara sie zejsc z "+
"drogi ciosu lecz jest zbyt wolny. Czujesz jak klinga wbija sie gleboko "+
"w cialo. Gdy wyszarpujesz bron uwalniajac ostrze, "+
"z ciezkiej rany ktora wlasnie zadales tryska na ziemie fontanna krwii.\n");     
enemy->catch_msg(QCIMIE(wielder,PL_MIA)+" dopada do ciebie szybkim skokiem i "+
"wykonuje potezne pchniecie celujac w "+hdesc+", starasz sie zejsc z "+
"drogi ciosu lecz jestes zbyt wolny. Czujesz jak glownia smuklego "+
"lsniacego scimitara wbija sie gleboko w twoje cialo. Gdy przeciwnik "+
"wyszarpuje bron uwalniajac ostrze, z ciezkiej rany ktora otrzymales "+
"tryska na ziemie gesta fontanna drogocennej krwii.\n");
tell_watcher(QCIMIE(wielder,PL_MIA)+" dopada szybkim skokiem do "+
QIMIE(enemy,PL_DOP)+" wykonujac potezne pchniecie w "+hdesc+
", ofiara stara sie zejsc z drogi ciosu lecz "+
"jest zbyt wolna. Widzisz jak glownia smuklego lsniacego scimitara wbija "+
"sie gleboko w cialo pozostawiajac duza rane z ktorej na ziemie tryska gesta "+
"fontanna krwii.\n",enemy);

      break;
      case 67..83:
wielder->catch_msg("Bierzesz potezny zamach i z impetem przebijasz zastawe "+
QIMIE(enemy,PL_DOP)+" wdrazajac ostrze gleboko w "+hdesc+" tnac kosci, "+
"sciegna i uwalniajac fontanne tryskajacej na boki krwii.\n");
enemy->catch_msg(QCIMIE(wielder,PL_MIA)+" bierze potezny zamach i z impetem "+
"przebija zastawe ktora zlozyles wdrazajac ostrze smuklego lsniacego "+
"scimitara gleboko w "+hdesc+" tnac kosci, "+
"sciegna i uwalniajac fontanne tryskajacej na boki krwii. Porazajacy bol "+
"niemal calkowicie przycmiewa ci na chwile wzrok. \n");
tell_watcher(QCIMIE(wielder,PL_MIA)+" bierze potezny zamach i z "+
"impetem przebija zastawe "+QIMIE(enemy,PL_DOP)+" wdrazajac ostrze "+
"smuklego lsniacego scimitara gleboko w "+hdesc+" tnac kosci, sciegna "+
"i uwalniajac fontanne tryskajacej na boki krwii.\n",enemy);
      break;
      case 84..99:
wielder->catch_msg("Skrecasz sie w piruecie nadajac ped ostrzu ktore z "+
"sykiem rozcina powietrze trafiajac w "+hdesc+" "+QIMIE(enemy,PL_DOP)+
". Ten potezny cios tnie tkanke i miazdzy kosci pozostawiajac po sobie "+
"ogromna buchajaca posoka, prawie smiertelna rane. "+
"Widzisz jak przeciwnik z trudem utrzymuje rownowage.\n");
enemy->catch_msg(QCIMIE(wielder,PL_MIA)+" skreca sie w piruecie nadajac "+
"ped ostrzu ktore z sykiem rozcina powietrze trafiajac cie w "+
hdesc+". Czujesz jak ten potezny cios tnie tkanke i miazdzy kosci. "+
"Porazajacy bol niemal calkowicie przycmiewa ci na chwile wzrok. "+
"Z trudem utrzymujesz rownowage prawie upadajac.\n");
tell_watcher(QCIMIE(wielder,PL_MIA)+" skreca sie w piruecie nadajac ped "+
"ostrzu ktore z sykiem rozcina powietrze trafiajac w "+
hdesc+" "+QIMIE(enemy,PL_DOP)+". Ten potezny cios "+
"tnie tkanke i miazdzy kosci pozostawiajac po sobie ogromna buchajaca "+
"posoka, prawie smiertelna rane. Widzisz jak ofiara z trudem utrzymuje "+
"rownowage prawie upadajac.\n",enemy);
      break;

      default:
wielder->catch_msg("Podrywasz bron do szalenczego mlynca, "+
"z glosnym sykiem tnac powietrze dekoncentrujac tym uwage "+
QIMIE(enemy,PL_DOP)+". Blyskawicznym wypadem przerywasz ten "+
"zwodniczy pokaz wyprowadzajac fantastyczne ciecie prosto w glowe ktora "+
"przy odglosie glosnego mlasniecia odrywa sie od tulowia. "+
"Przed upadkiem zatacza ona kilka kregow w powietrzu chlapiac we wszystkie "+
"strony posoka. Cialo twego przeciwnika jeszcze przez chwile stoi sztywno "+
"po czym zwala sie "+
"bezwladnie na ziemie.\n");

enemy->catch_msg(QCIMIE(wielder,PL_MIA)+" podrywa bron do szalenczego "+
"mlynca z glosnym sykiem tnac powietrze. "+
"Uwaznie sledzisz ruchy ostrza dopiero w ostatniej chwili poczynasz sobie "+
"zdawac sprawe ze ma to tylko odwrocic twa uwege, lecz jest juz za pozno. "+
"Blyskawiczny wypad nie zostawia ci czasu na zlozenie zastawy. Klinga uderza "+
"dokladnie prosto w "+
"szyje ... slyszysz odglos glosnego mlasniecia po czym swiat zaczyna nagle "+
"wirowac. Przez chwile widzisz bezglowe cialo, ze zdumieniem poznajesz ze "+
"niegdys nalezalo do ciebie ... potem jest juz tylko ciemnosc.\n");
tell_watcher(QCIMIE(wielder,PL_MIA)+" podrywa bron do szalenczego mlynca "+
"i z glosnym sykiem tnac powietrze dekoncentruje tym uwage "+
QIMIE(enemy,PL_DOP)+". Blyskawicznym wypadem przerywa ten zwodniczy "+
"pokaz i wyprowadzaja fantastyczne ciecie prosto w "+
"glowe ktora przy odglosie glosnego mlasniecia odrywa sie od tulowia. "+
"Przed upadkiem zatacza ona kilka kregow w powietrzu chlapiac we wszystkie "+
"strony posoka. Cialo jeszcze przez chwile stoi sztywno po czym zwala sie "+
"bezwladnie na ziemie.\n",enemy);
      break;
   }

   return 1;
}
