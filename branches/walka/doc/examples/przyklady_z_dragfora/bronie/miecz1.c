/*
 * Filename: miecz1.c
 *
 * Zastosowanie: Miecz obureczny.
 * 
 * Copyright Volothamp Geddarm.
 *
 */
inherit "/d/std/bron";  

#include <filter_funs.h>
#include <ss_types.h>
#include <wa_types.h>
#include <stdproperties.h>
#include <wa_types.h>
#include <cmdparse.h>
#include <macros.h>
#include <pl.h>
#include <options.h>
#include <macros.h>
#include <formulas.h>
#include <const.h>

#define TP this_player()
object wielder;

    void
    create_weapon()
{
    set_autoload();  
    ustaw_nazwe(({ "miecz", "miecza", "mieczowi", "miecz", "mieczem", "mieczu" }),
                ({ "miecze", "mieczy", "mieczom", "miecze", "mieczami", "mieczach" }), PL_MESKI_NOS_NZYW); 
    
	dodaj_przym("ciezki", "ciezcy");
	dodaj_przym("obureczny", "obureczni");

    set_long("Jest to sporej dlugosci miecz o wyjatkowym ciezarze sprawiajacym, ze "
	  + "ta bron przystosowana jest do trzymania oburacz. Rekojesc pokryto misternymi "
	  + "zdobieniami i dodatkowo pozlocono a szerokie ostrze wyglada niczym pokryta "
	  + "zlotoglowiem zaslona okienna w bogatej posesji. Zauwazasz rowniez na jego "
	  + "powierzchni znak mowiacy, ze ta bron wyrobem jest slawnego kowala z Beregostu.\n");
    
	set_hit(38);
    set_pen(38);
    
	set_wt(W_SWORD);
	
	set_dt(W_SLASH);
	set_dt(W_IMPALE);
    
	set_hands(W_BOTH);
    
	add_prop(OBJ_I_WEIGHT, 8500);                
	add_prop(OBJ_I_VALUE, 800);
}

/*----Podpisano: 'Volothamp Geddarm'----*/


  

