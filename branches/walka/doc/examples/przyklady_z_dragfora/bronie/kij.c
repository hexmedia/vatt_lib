/*
 * Filename: kij.c
 *
 * Zastosowanie: Kij dla kaplana.
 * 
 * Copyright Volothamp Geddarm.
 *
 */
inherit "/d/std/bron";  

#include <filter_funs.h>
#include <ss_types.h>
#include <wa_types.h>
#include <stdproperties.h>
#include <wa_types.h>
#include <cmdparse.h>
#include <macros.h>
#include <pl.h>
#include <options.h>
#include <macros.h>
#include <formulas.h>
#include <const.h>

#define TP this_player()
object wielder;

    void
    create_weapon()
{
    set_autoload();  
    ustaw_nazwe(({ "kostur", "kostura", "kosturowi", "kostur", "kosturem", "kosturze" }),
                ({ "kostury", "kosturow", "kosturom", "kostury", "kosturami", "kosturach" }), PL_MESKI_NOS_NZYW); 
    dodaj_przym("krotki", "krotcy");
    dodaj_przym("srebrzysty", "srebrzysci");
    set_long("Kij, ktory obserwujesz jest najwyrazniej typowym kaplanskim kosturem. "
	  + "Cala jego powierzchnie pokrywaja dziwne symbole, ktore z jakichs dziwnych "
+ "powodow emanuja swiatlem tylko w rekach pelnoprawnego wyznawcy Pana Poranka. "
	  + "Poza tym jego srebrzysta barwa odbija otaczajace swiatlo i moze nawet oslepiac "
	  + "jesli to samo swiatlo jest mocno jaskrawe. Ponadto ow kawalek drewna nie rozni "
	  + "sie niczym wiecej od zwyklej podrozniczej laski.\n");
    
	set_hit(10);
    set_pen(10);
    
	set_wt(W_POLEARM);
	
	set_dt(W_SLASH);
	set_dt(W_IMPALE);
    
	set_hands(W_ANYH);
    
	add_prop(OBJ_I_WEIGHT, 1000);                
	add_prop(OBJ_I_VALUE, 10);
}

/*----Podpisano: 'Volothamp Geddarm'----*/


  

