#pragma strict_types
inherit "/d/std/bron";

#include "../pal.h"

void
create_weapon()
{
    set_autoload();
   ustaw_nazwe( ({ "claymore", "claymora", "claymorowi", "claymore", "claymorem",
                   "claymorze" }), ({ "claymory", "claymorow", "claymorom",
                   "claymory", "claymorami", "claymorach" }), PL_MESKI_NOS_NZYW);
  
   dodaj_nazwy(({ "miecz", "miecza", "mieczowi", "miecz", "mieczem",
                   "mieczu" }), ({ "miecze", "mieczy", "mieczom",
                   "miecze", "mieczami", "mieczach" }), PL_MESKI_NOS_NZYW);

    dodaj_przym("dwusieczny","dwusieczni");
    dodaj_przym("blyszczacy","blyszczacy");
   
    set_long("Ten dlugi na prawie trzy stopy dwusieczny miecz o prostym i szerokim ostrzu "+
             "posiada plytkie zbrocze. Ramiona jelca zostaly odchylone"+
             " w strone glowni, a rekojesc jest na tyle dluga i wygodna "+
             "by zapewnic pewny chwyt. Polkolista glowica zabezpiecza "+
             "przed wysuwaniem sie miecza z dloni.\n");
   
            set_hit(24);
            set_pen(30);
            
            set_wt(W_SWORD);
            set_hands(W_ANYH);
            set_dt(W_SLASH|W_IMPALE);
                      
            add_prop(OBJ_I_WEIGHT, 8500);
            add_prop(OBJ_I_VOLUME, 6000);
}
                           
