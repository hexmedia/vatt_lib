#pragma strict_types
inherit "/d/std/bron";
#include "../pal.h"

void create_weapon()
{
   set_autoload();
   dodaj_nazwy( ({ "mlot", "mlota", "mlotowi", "mlot", "mlotem",
                   "mlocie" }), ({ "mloty", "mlotow", "mlotom",
                   "mloty", "mlotami", "mlotach" }), PL_MESKI_NOS_NZYW );
    dodaj_przym("masywny","masywni");
    dodaj_przym("orczy","orczy");
   ustaw_nazwe( ({ "mlot bojowy", "mlota bojowego", "mlotowi bojowemu",
                    "mlot bojowy", "mlotem bojowym", "mlocie bojowym"}),
                  ({"mloty bojowe", "mlotow bojowych", "mlotom bojowym",
                    "mloty bojowe", "mlotami bojowymi", "mlotach bojowych"}), PL_MESKI_NOS_NZYW );
                    
   dodaj_nazwy( ({ "bojowy mlot", "bojowego mlota", "bojowemu mlotowi",
                    "bojowy mlot", "bojowym mlotem", "bojowym mlocie"}),
                  ({"bojowe mloty", "bojowych mlotow", "bojowym mlotom",
                    "bojowe mloty", "bojowymi mlotami", "bojowych mlotach"}), PL_MESKI_NOS_NZYW );

   set_long("Jest to stalowy mlot bojowy, uzywwany przez orczych wojownikow. "
   	+" Na dosyc dlugim drzewcu osadzone jest zelezce, "+
       "podobne do zwyklego, kowalskiego mlota, jednak majace dziob i obuch z kilkoma kolcami i "+
       "zwienczone dlugim, spiczastym grotem. Bron ta wyglada na niezwykle "+
       "skuteczne narzedzie w walce.\n");

            set_hit(17);
            set_pen(37);
            
            set_wt(W_WARHAMMER);
            set_dt(W_BLUDGEON|W_IMPALE);
            set_hands(W_BOTH);
             
            add_prop(OBJ_I_WEIGHT, 8500);
            add_prop(OBJ_I_VOLUME, 4444);
                         
}
                           