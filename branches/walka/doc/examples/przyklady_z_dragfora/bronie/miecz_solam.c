#pragma strict_types
inherit "/d/std/bron";
#include "../pal.h"

void create_weapon()
{
   set_autoload();   
   ustaw_nazwe(({ "miecz", "miecza", "mieczowi", "miecz", "mieczem",
                   "mieczu" }), ({ "miecze", "mieczy", "mieczom",
                   "miecze", "mieczami", "mieczach" }), PL_MESKI_NOS_NZYW);

    dodaj_przym("lekki","leccy");
    dodaj_przym("lsniacy","lsniacy");
   
    set_long("Jest to prosty miecz, do wyrobu ostrza ktorego dodano jakiejs "
    +"domieszki powodujacej, ze ostrze lsni i odbija padajace swiatlo. Moze "
    +"to byc bardzo przydatne w walce, gdyz odpowiednie ulozenie klingi moze "
    +"oslepic przeciwnika. Na ostrzu widzisz wypuncowany symbol Solamnii.\n"); 
   
            set_hit(17);
            set_pen(17);
            
            set_wt(W_SWORD);
            set_hands(W_ANYH);
            set_dt(W_SLASH|W_IMPALE);
                      
            add_prop(OBJ_I_WEIGHT, 3000);
            add_prop(OBJ_I_VOLUME, 3000);
}
                       
