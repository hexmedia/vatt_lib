inherit "/d/std/bron"; 

#include <filter_funs.h>
#include <ss_types.h>
#include <wa_types.h>
#include <stdproperties.h>
#include <wa_types.h>
#include <cmdparse.h>
#include <macros.h>
#include <pl.h>
#include <options.h>
#include <macros.h>
#include <formulas.h>
#include <const.h>

object wielder;

void
create_weapon()
{
    
    set_autoload(); 
    
    ustaw_nazwe(        ({"mlot bojowy","mlota bojowego","mlotowi bojowemu","mlot bojowy","mlotem bojowym","mlocie bojowym"}),
        ({"mloty bojowe","mlotow bojowych","mlotom bojowym","mloty bojowe","mlotami bojowymi","mlotach bojowych"}),
        PL_MESKI_NOS_NZYW); 
    
    dodaj_nazwy(({"mlot","mlota","mlotowi","mlot","mlotem","mlocie"}),
        ({"mloty","mlotow","mlotom","mloty","mlotami","mlotach"}),
        PL_MESKI_NOS_NZYW);

    dodaj_przym("obureczny", "obureczni");

    set_long("Jest to masywny, obureczny mlot bojowy jakim posluguja sie zazwyczaj "
           + "wprawni wojownicy. Rowny trzon zostal owiniety "
	       + "cienka warstwa skory zwierzecej, azeby trzymajac ta bron w rekach "
	       + "nie istnialo jakies szczegolne ryzyko wypuszczenia jej podczas starcia.\n");
    
    set_hit(22);
    set_pen(22);
    
    set_wt(W_WARHAMMER);

    set_dt(W_BLUDGEON);
    
    set_hands(W_BOTH);
    
    add_prop(OBJ_I_WEIGHT, 4500);                
    add_prop(OBJ_I_VALUE, 1350);

}

