#pragma strict_types


inherit "/d/Faerun/std/room";

#include <stdproperties.h>
#include <macros.h>

void
create_room()
{
    set_short("Pod murami Candlekeep");
    set_long("Znajdujesz sie dokladnie pod murami twierdzy Candlekeep."
    +" Stoisz na czyms w rodzaju niewielkiej polanki, ktora z jednej strony"
    +" ogranicza las i krzaki, a z drugiej mury. Dziwne jest to ze straznicy"
    +" jeszcze cie nie zauwazyli, wszak jest to ich obowiazek.\n");
    add_item( ({"drzewa","krzewy","las","krzaki"}), "Geste krzewy i sosnowe drzewa skladaja sie na "
          + "istnienie otaczajacego ciebie, gestego lasu.\n");
    add_item(({"mur","mury"}), "Olbrzymie mury twierdzy, ponoc niezdobyte ani razu."
    +" Chronia doskonale cala twierdze i powstrzymaly by cale oblezenie."
    +" Hmm.. A coz to takiego... Wyglada jak duzych rozmiarow szczelina w murze!\n");
    add_item("szczeline","Ciekawe.. dosyc sporych rozmiarow szczelina w murze.."
    + " Ktora na dodatek prowadzi chyba wprost do miasta! W sumie mozna by sprobowac"
    + " sie przezen przecisnac.\n");
    add_exit("krzaki1",({"krzaki","w kierunku gestwiny krzakow."}),0,2,0);
    set_event_time(500.0,500.0);
    add_event("Na niebie dostrzegasz klucz ptakow.\n",1);
    add_event("W mroku nocy slyszysz huczenie sowy.\n",2);
    add_event("Slyszysz nieopodal trzask galazki.\n",0);
    add_event("Gwiazdy pieknie blyszcza sie na ciemnym sklepieniu.\n",2);
    add_event("Z glebi lasu dochodza twych uszu niepokojace dzwieki przyrody.\n",0);
}


int cand_enter(string str)
{
        notify_fail("Przecisnij sie przez co?\n");
        if(str != "sie przez szczeline" && str != "sie przez szczeline w murze") return 0;
        saybb(QCIMIE(this_player(),0) + " przeciska sie przez szczeline w murze.\n");
        write("Przeciskasz sie przez szczeline.\n");
        this_player()->move("/d/Faerun/Miasta/candlekeep/szpital3");
        saybb(QCIMIE(this_player(),0) + " wychodzi ze szczeliny w scianie!\n");
        return 1;

}

void init()
{
        ::init();
        add_action("cand_enter","przecisnij");
}
