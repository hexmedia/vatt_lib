#pragma strict_types
#pragma no_clone
inherit "/std/workroom";
#include "/sys/stdproperties.h"
#include "/sys/macros.h"
int co; //0-krzeslo 1-fotel
int zajete1, zajete2;
#define KRZESLO "sit_on_krzeslo"
#define FOTEL   "sit_on_fotel"
void

void
create_room()
{
set_short("Pracownia Tolbringera");
set_long("Jest to obszerne pomieszczenie. Pod sciana widzisz ustawione wysokie regaly zapelnione "+
         "przeroznymi, opaslymi tomiskami. Na wiekszosci z nich widzisz nieznaczne slady plesni. "+
         "Na srodku sali stoi wielkie, debowe biurko i drewniane krzeslo oba meble sa "+
         "zaslane roznymi notatkami i chemikaliami. Cale pomieszczenie zalane jest "+
         "swiatlem dobywajacym sie z dziwnych kul umieszczonych na podstawkach przy scianach. "+
         "Uswiadamiasz sobie ze jest to pracownia Wielkiego Maga Tolbringera ktora widzialo "+
         "niwielu sposrod smiertelnych.\n");
         
add_prop(ROOM_I_INSIDE,1);

add_exit("/d/Krainy/tenser/workroom", ({"portal", "przez fosforyzujacy okrag i znika"}), 0, 1);

add_exit("schody_u.c" , ({"dol", "powoli schodami w dol"}), 0, 1);

add_item(({"regaly" , "regal" , "polke" , "polki" , "ksiazki" , "ksiegi"}), "Widzisz potezne, siegajace sklepienia polki. Wypelnione sa po brzegi "+
                                                                            "roznymi ksiegami. Zauwazasz tam takie dziwne tytuly jak 'Ars Magica' "+
                                                                            "lub tez dziela medrcow z dalekiego poludnia. Na niektorych tomiskach "+
                                                                            "zauwazasz slady plesni lub zakrzeplej krwi swiadczacej o tym ze nie bylo "+
                                                                            "latwo je zdobyc/n");

add_item(({"biurko"}), "Biurko wykonano z drewana debowego, jednak nie jest to zwykly material "+
                       "dostepny smiertelnikom, lecz umagicznione drewno, ktore wytrzyma najwieksze  "+
                       "ciezary. Ta wlasnosc jest bardzo przydatna, gdyz wszystkie materialy jakie mag "+
                       "kolekcjonuje w swojej pracowni przewaznie leza na biurku.\n");
                       
add_item(({"kule" , "lampy"}), "Sa to niewielkie kule z wnetrza ktorych dobywa sie delikatne, jasne swiatlo. "+
                               "Osadzone sa na czyms na ksztalt swiecznikow, ktore zabezpieczaja je przed upadkiem "+
                               "i niechybnym zbiciem.\n");

add_item(({"krzeslo" , "stolek"}), "Widzisz niewysokie krzeslo zrobione z tego samego materialu co biurko - umagicznionego drewna.\n");

}

int
siadz_przy_biurku(string swf)
{
    notify_fail("Gdzie chcesz siasc? Moze przy biurku?\n");
    if(swf=="przy biurku")
    {
        switch(this_player()->query_prop(SIEDZACY))
        {
           case 1:
            {
                write("Przeciez juz siedzisz!\n");
                break;
            }
            default:
            {
	    if(!this_player()->query_wiz_level())
	    {
	    write(" Probujesz siasc przy biurku, lecz "+
	    "jakas pradawna, straszna sila odpycha cie.\n");
	    }
	    if(zajete1==0)
	    {
                saybb(QCIMIE(this_player(), PL_MIA) + " rozsiada sie przy biurku.\n");
                write("Siadasz wygodnie przy biurku i czujesz napelniajaca cie "+
                      "sile i pelna gotowosc do pracy.\n");
                this_player()->add_prop(LIVE_S_EXTRA_SHORT," siedzac" +
                    this_player()->koncowka("y","a") + " w fotelu");
                this_player()->add_prop(SIEDZACY,1);
		this_player()->add_prop(BIURKO,1);
		zajete1=1;
	    }
	    else write("Przy biurku juz ktos siedzi.\n");
	
	
            }
            return 1;
        }
    }
}
int
wstan()
{

    if(this_player()->query_prop(SIEDZACY)==1)
    {
    if(this_player()->query_prop(BIURKO)==1)
    {
        saybb(QCIMIE(this_player(), PL_MIA) + " podnosi sie z ociaganiem od biurka.\n");
        write("Podnosisz sie niechetnie od biurka.\n");
        this_player()->remove_prop(LIVE_S_EXTRA_SHORT);
        this_player()->remove_prop(SIEDZACY);
	this_player()->remove_prop(BIURKO);
	zajete1=0;
        return 1;
    }
    else if(this_player()->query_prop(KRZESLO)==1)
    {
        saybb(QCIMIE(this_player(), PL_MIA) + " wstaje powoli z krzesla.\n");
        write("Wstajesz powoli z krzesla.\n");
        this_player()->remove_prop(LIVE_S_EXTRA_SHORT);
        this_player()->remove_prop(SIEDZACY);
	zajete2=0;
        return 1;
    }
    }
    else
    {
        write("Przeciez nigdzie nie siedzisz!\n");
        return 1;
    }
}
int
usiadz_na_krzesle(string unk)
{
    notify_fail("Gdzie chcesz usiasc? Moze na krzesle?\n");
    if(unk=="na krzesle")
    {
        switch(this_player()->query_prop(SIEDZACY))
        {
           case 1:
            {
                write("Przeciez juz siedzisz!\n");
                break;
            }
            default:
            {
	    if(zajete2==0)
	    {
                saybb(QCIMIE(this_player(), PL_MIA) + " siada powoli na krzesle.\n");
                write("Siadasz powoli na krzesle z zainteresowaniem"+
		              "ogladajac kazdy kat pomieszczenia.\n");
                this_player()->add_prop(LIVE_S_EXTRA_SHORT," siedzac" +
                    this_player()->koncowka("y","a") + " na krzesle");
                this_player()->add_prop(SIEDZACY,1);
                this_player()->add_prop(KRZESLO,1);
		zajete2=1;
	    }
	    else
	    {
	    write("Krzeslo jest zajete przez kogos.\n");
	    }
            }
            return 1;
        }
    }
}

int
nie_wyjdziesz()
{
    if(this_player()->query_prop(SIEDZACY)==1)
    {
        write("Moze najpierw wstaniesz z krzesla?\n");
        return 1;
    }
    else
    {
        return 0;
    }
}
void
init()
{
    ::init();
    add_action("usiadz_za_biurkiem", "usiadz");
    add_action("usiadz_na_krzesle", "usiadz");
    add_action("wstan", "wstan");
}                      
