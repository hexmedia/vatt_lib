#include "../../pal.h"
inherit STD+"pal_std.c";
inherit "/lib/store_support"; /* obiekt jest magazynem sklepowym */
 
void create_pal_room()
{

	set_short("Magazyn sklepu z bronia.\n");
    	set_long("Magazyn. Do tego pomieszczenia nie maja wstepu gracze, wiec "
    	+"nie potrzeba wymyslnego longa. Powinny tu lezec rzeczy, ktore gracz "
    	+"sprzedal oraz standardowe uzbrojenie dostepne w sklepie zaraz po "
    	+"Apokalipsie.\n");
    
    	add_prop(ROOM_I_INSIDE, 1);
    
    	add_exit(LOC+"sklepy/bron.c", "sklep"); 
    	
    	(clone_object(BRN+"miecz_solam.c"))->move(TO);
    	(clone_object(BRN+"miecz_solam.c"))->move(TO);
    	(clone_object(BRN+"miecz_solam.c"))->move(TO);
    	(clone_object(BRN+"miecz_solam.c"))->move(TO);
    	
    	(clone_object(BRN+"sztylet1.c"))->move(TO);
    	(clone_object(BRN+"sztylet1.c"))->move(TO);
    	(clone_object(BRN+"sztylet1.c"))->move(TO);
    	(clone_object(BRN+"sztylet1.c"))->move(TO);
    	
    	(clone_object(BRN+"topor1.c"))->move(TO);
    	(clone_object(BRN+"topor1.c"))->move(TO);
    	(clone_object(BRN+"topor1.c"))->move(TO);
    
}

void enter_inv(object ob, object skad) 
{				  

    ::enter_inv(ob, skad);
    
    store_update(ob);
}

