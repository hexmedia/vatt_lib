/* Tawerna 'Pod Wybitym Zebem' 
			Aetherian@Sarcan
			Data: 28.12.2002
*/

#include "../../pal.h"
inherit STD+"pal_std.c";
inherit "/lib/pub";

void create_pal_room()
{
	set_short("Tawerna 'Pod Wybitym Zebem'");
	set_long("Ta knajpa z cala pewnoscia zasluguje na swoja nazwe. Nawet wchodzac "
	+"do niej zaczynasz sie juz obawiac o stan swojego uzebienia. Wokol ciebie "
	+"ustawione sa stoliki przy ktorych siedza lub leza bywalcy tej tawerny. "
	+"Nad szynkiem wisi sporych rozmiarow glowa gorskiego trolla z wybitym "
	+"przednim zebem. Wokol stolikow krzataja sie dziewki sluzebne, zbierajace "
	+"zamowienia i roznoszace napitki. Obok lady przywieszona jest drewniana tablica, "
	+"sluzaca zapewne za menu.\n");
	
	add_item( ({"glowe","trolla","glowe trolla"}), "Nad szynkiem wisi glowa wielkiego "
	+"gorskiego trolla, zapewne jakas pamiatka dla karczmarza.\n");
	add_item( ({"lade","szynkwas"}), "Lada zbita jest z debowych desek. Jej powierzchnia "
	+"jest przetarta od czestego uzywania.\n");
	add_item( ({"dziewki","dziewki sluzebne","dziewke"}), "@@opis_dziewek" );
	
	add_cmd_item( ({ "tablice", "tabliczke", "menu" }), ({ "czytaj", "przeczytaj" }),
	"@@menu");
	add_cmd_item("dziewke",({ "szczypnij","uszczypnij" }),"@@uszczypnij_dziewke");
	
	add_prop(ROOM_I_INSIDE, 1);
	add_prop(ROOM_I_LIGHT, 1);
	
	add_exit(LOC+"poludniowa1","wyjscie");
	
	dodaj_npca(NPC+"karczmarz1.c");
	
    	add_food("stek", 0,
            50, 35, PL_MESKI_NZYW, "spory kawal ociekajacego tluszczem miesa z ziemniakami.");

    	add_food("pasztet", 0,
            200, 60, PL_MESKI_NZYW, "pasztet z zajaca");

    	add_food("dzika", 0,
            150, 100, PL_MESKI_NZYW, "potrawke z dzika");

        add_drink("piwa","piwo","kufel rozwodnionego piwa",
            "rozwodnionego piwa",
            ({ ({"cieply"}), ({"ciepli"}) }),
            4, 500, 10, 44, PL_NIJAKI_NOS);
                
        add_drink("wina","wino","szklanke czerwonego wina",
            "brudno-czerwone wino",
            ({ ({"czerwony"}), ({"czerwoni"}) }),
            10, 200, 6, 99, PL_NIJAKI_NOS);
}

void init()
{
    ::init();
    init_pub();
}

string opis_dziewek()
{
	string opis="Dziewki sluzebne krzataja sie wokol stolikow zbierajac zamowienia i "
	+"roznoszac napitki. ";	
	
	if (this_player()->query_gender()==G_MALE)
	{
		return opis+"Wygladaja calkiem ladnie. Ciekawe co by bylo gdyby tak jedna "
		+"uszczypnac?";	
	} else {
		return opis;
	}	
}

int uszczypnij_dziewke()
{
	if (this_player()->query_gender()==G_MALE)
	{
		write("Odczekujesz odpowiedni moment, gdy dziewka sluzebna podejdzie do "
		+"przeciwleglego stolika i nachyli sie do klienta i wtedy szczypiesz ja "
		+"bezlitosnie w posladek.\nZaskoczona ofiara wydaje z siebie donosny pisk, "
		+"upuszcza tace, oblewajac klienta piwem i zaczerwieniona ucieka na "
		+"zaplecze. Rozgladasz sie po karczmie, gdzie cala meska czesc bywalcow "
		+"rechocze w nieboglosy. Czujesz sie szczesliwy.\n");
		saybb(QCIMIE(this_player(),PL_MIA)+" odczekuje odpowiedni moment i gdy "
		+"dziewka sluzebna nachyla sie nad stolikiem naprzeciwko, bezlitosnie "
		+"szczypie ja w posladek.\nZaskoczona ofiara, ku uciesze calej meskiej "
		+"czesci bywalcow, wydaje z siebie donosny pisk, upuszcza tace, oblewajac "
		+"jednego z klientow piwem i zarumieniona ucieka na zaplecze.\nNa twarzy "
		+QIMIE(this_player(),PL_DOP) + " wykwita wyraz zadowolenia.\n");
		return 1;
	} else {
		write("Juz zamierzasz sie do bezlitosnego uszczypniecia, gdy nagle dociera "
		+"do ciebie mysl: 'A co sobie ludzie pomysla?!'. Lekko zarumieniona odsuwasz "
		+"reke od tylniej czesci ciala dziewki sluzebnej i siadasz spokojnie przy "
		+"swoim stoliku.\n");
		saybb(QCIMIE(this_player(),PL_MIA)+" odsuwa sie na chwile od stolika i przysuwa "
		+"dlon do posladka dziewki sluzebnej, jakby chciala ja uszczypnac. Po chwili "
		+"jednak opamietuje sie i lekko zarumieniona siada spokojnie przy swoim stoliku.\n");
		return 1;
	}
	return 0;
}

string menu()
{
        say(QCIMIE(this_player(), PL_MIA) + " z trudem usiluje przeczytac menu.\n");
        return
        "\nNa tabliczce krzywymi literami namalowano:\n\n"
        +"\t\t M E N U \n"
        +"Ciemne piwo:       22 miedziaki\n"
        +" (w beczce 10l)   680 miedziakow\n" 
        +"Wino:              19 miedziakow\n"+
         " (w beczce 6l)    798 miedziakow\n\n"
        +"Tlusty stek:       35 miedziakow\n"
        +"Pasztet:           50 miedziakow\n\n"
        +"Specjalnosc dnia:\n"
        +"Potrawka z dzika: 100 miedziakow\n\n";
}       

