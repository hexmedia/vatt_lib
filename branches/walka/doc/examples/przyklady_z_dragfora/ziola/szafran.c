inherit "/std/herb";

#include "ziola.h"

create_herb()
{
    ustaw_nazwe(KWIAT, PL_MESKI_NOS_NZYW);
  
    dodaj_przym("drobny", "drobni");
    dodaj_przym("fioletowy", "fioletowi");

    ustaw_nazwe_ziola(({"szafran", "szafranu", "szafranowi", "szafran", "szafranem", "szafranie"}), PL_MESKI_NOS_NZYW); 
    
    set_id_long("Masz przed soba szafran, zwany tez krokusem. Mozna go znalezc i to bez trudu "+
	"na gorskich zboczach, albowiem jego fioletowe kwiaty rzucaja sie w oczy. Powachany "+
	"potrafi swietnie leczyc i regenerowac sily.\n");

    set_unid_long("Jest to fioletowy, bardzo droby i delikatny kwiat. Bije od niego "+
	"przyjemny i uspokajajacy zapach.\n"); 
    
    set_ingest_verb(({"powachaj", "wachasz", "wacha", "powachac"}));
    set_effect(HERB_SPECIAL, special_effect);

    set_decay_time(330);
    set_id_diff(40);
    set_find_diff(2);
    set_amount(5);
    set_herb_value(34);
}

int
special_effect()
{
    write("Czujesz sie lepiej.\n");
    TP->heal_hp(10+random(5));
    TP->set_fatigue(TP->query_fatigue()+5+random(3));
    return 1;
}
