inherit "/std/herb";

#include "ziola.h"

create_herb()
{
    ustaw_nazwe(PNACZE, PL_NIJAKI_NOS); 

    dodaj_przym("aromatyczny", "aromatyczni");

    ustaw_nazwe_ziola(({"wanilia plaskolistna", "wanilii plaskolistnej", "wanilii plaskolistnej", 
                        "wanilie plaskolistna", "wanilia plaskolistna", "wanilii plaskolistnej"}), PL_ZENSKI); 

    set_id_long("Jest to wanilia plaskolistna, pnacze z rodziny storczykowatych. Nie "+
	"jest wlasciwie przydatna, lecz posiada piekny, slodki zapach.\n");

    set_unid_long("Masz przed soba bardzo slodkie, w dodatku mocno pachnace pnacze.\n"); 

    set_decay_time(340);
    set_id_diff(63);
    set_find_diff(9);
    set_amount(18);
    set_herb_value(301);
}
