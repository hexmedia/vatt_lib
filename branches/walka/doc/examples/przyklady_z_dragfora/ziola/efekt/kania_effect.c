inherit "/std/object";

#include <macros.h>
#include <pl.h>
#include <stdproperties.h>

#define TO this_object()
#define TP this_player()


void
start(int f)
{
    switch(f)
    {
        case 0:
        write("Zaczyna cie bolec zoladek...\n");
        set_alarm(20.0,0.0,"start",(f+1)); // to jest za 20 sekund kolejna faza
        break;
        case 1:
        write("Teraz juz wiesz, ze pomysl ze zjedzeniem surowego grzyba nie byl dobry...\n");
        set_alarm(10.0,0.0,"start",(f+1)); // 10 sekund do nastepnej fazy
        break;
        case 2:
        write("Brzuch boli cie coraz bardziej\n");
        TP->command("krzyknij Hurhh..harr..h.hf.");
        TP->command("zwymiotuj");
        set_alarm(30.0,0.0,"start",(f+1)); // 30 sekund do nastepnej fazy     
        break;
        default:
        write("Bole przechodza...\n");
        TO->remove_object();     
    }
}

void
create_object()
{
    add_prop(OBJ_M_NO_DROP,"");
    add_prop(OBJ_M_NO_GIVE,"");
    add_prop(OBJ_M_NO_GET,"");
    set_no_show();
}

void
rozjeb_czache()
{
    set_alarm(0.5,0.0,"start",0);
}
