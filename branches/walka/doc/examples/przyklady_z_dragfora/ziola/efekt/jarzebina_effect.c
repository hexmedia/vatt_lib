inherit "/std/object";

#include <macros.h>
#include <pl.h>
#include <stdproperties.h>

#define TO this_object()
#define TP this_player()
int jak_widzial = 0;
void
koniec( object kogo )
{

TP->set_hp(TP->query_hp()+2);
TP->catch_msg("Czujesz sie lepiej.\n");
TO->remove_object();  
}
void
rmhp3( object kogo )
{

TP->set_hp(TP->query_hp()-5-random(5));
TP->catch_msg("Czujesz sie gorzej.\n");
}
void
rmhp2( object kogo )
{
TP->set_hp(TP->query_hp()-15-random(30));
TP->catch_msg("Czujesz sie coraz gorzej..\n");
}
void
rmhp( object kogo )
{
TP->set_hp(TP->query_hp()-15-random(10));
TP->catch_msg("Czujesz sie gorzej.\n");
}


void
start(int f)
{
        TP->set_hp(TP->query_hp()-3-random(2));
        TP->catch_msg("Czujesz sie gorzej.\n");
        set_alarm(20.0,0.0,"rmhp",TP);
        set_alarm(60.0,0.0,"rmhp2",TP);
        set_alarm(100.0,0.0,"rmhp3",TP);
        set_alarm(120.0,0.0,"koniec",TP);

}

void
create_object()
{
    add_prop(OBJ_M_NO_DROP,"");
    add_prop(OBJ_M_NO_GIVE,"");
    add_prop(OBJ_M_NO_GET,"");
    set_no_show();
}

void
rozjeb_czache()
{
    set_alarm(10.5,0.0,"start",0);
}
