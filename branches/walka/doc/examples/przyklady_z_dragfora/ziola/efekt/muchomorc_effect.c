inherit "/std/object";

#include <macros.h>
#include <pl.h>
#include <stdproperties.h>

#define TO this_object()
#define TP this_player()

int no_way(string what)
{

 write("Niestety, w twojej krwi znajduje sie trucizna nie pozwala ci tego zrobic.\n");
 return 1;
}
void init()
{
   ::init();
   add_action(no_way, "zakoncz");
   add_action(no_way, "nagraj");
   add_action(no_way, "usmiech");

}
void
po( object kogo )
{
TP->catch_msg("Ten bol jest nie do zniesienia!\n");
TP->set_hp(TP->query_hp()-13);
}
void
za( object kogo )
{

TP->catch_msg("Zaczynasz dostawac drgawek.\n");
TP->command("krzyknij BoooLLIiiII!");
TP->set_hp(TP->query_hp()-19);
}
void
zza( object kogo )
{

TP->catch_msg("Dostajesz wymiotow.\n");
TP->command("zwymiotuj");
TP->set_hp(TP->query_hp()-35);
}
void
koniec( object kogo )
{
if(TP->query_stat(3) > 49)
{
TP->catch_msg("Czujesz sie znacznie lepiej.\n");
TP->set_hp(TP->query_hp()+25);
        TO->remove_object();  
}
else
{
TP->catch_msg("Zaczynasz sobie zdawac sprawe, ze za chwile nadejdzie smierc...\n\n\nCzujesz jak "
	+"twoje cialo przestaje pracowac. Krazenie oraz bicie serca ustaje. Umierasz..\n");
        this_player()->heal_hp(-(this_player()->query_max_hp()));
        this_player()->do_die();
        TO->remove_object();    
}
}


void
start(int f)
{
        TP->catch_msg("Dostajesz zawrotow glowy.\n");
        set_alarm(40.0,0.0,"po",TP);
        set_alarm(300.0,0.0,"za",TP);
        set_alarm(800.0,0.0,"zza",TP);
        set_alarm(1000.0,0.0,"koniec",TP);
}

void
create_object()
{
    add_prop(OBJ_M_NO_DROP,"");
    add_prop(OBJ_M_NO_GIVE,"");
    add_prop(OBJ_M_NO_GET,"");
    set_no_show();
}
void
rozjeb_czache()
{
    set_alarm(10.5,0.0,"start",0);
}

