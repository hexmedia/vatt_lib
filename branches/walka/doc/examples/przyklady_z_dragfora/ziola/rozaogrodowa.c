inherit "/std/herb";

#include "ziola.h"

create_herb()
{
    ustaw_nazwe(KWIAT, PL_MESKI_NOS_NZYW);
  
    dodaj_przym("czerwony", "czerwoni");
    dodaj_przym("kolczasty", "kolczasci");

    ustaw_nazwe_ziola(({"roza ogrodowa", "rozy ogrodowej", "rozy ogrodowej",
                        "roze ogrodowa", "roza ogrodowa", "rozy ogrodowej"}), PL_ZENSKI); 
    
    set_id_long("Masz przed soba roze ogrodowa, kwiat tak ulubiony i ukochany przez zakochanych. "+
        "Na lodygach rozy zauwazasz stosunkowo grube oraz ostre kolce mogace niezle "+
        "zadrapac. Roza nie ma chyba innych specjalnych zastosowan.\n");

    set_unid_long("Jest to pieknie pachnacy, czerwony kwiat, ktorego lodygi sa pokryte dosc "+
        "grubymi i ostrymi kolcami. Sam kwiat jest zlozony z wielu czerwonych platkow.\n"); 
    
    
    set_decay_time(453);
    set_id_diff(63);
    set_find_diff(9);
    set_amount(2);
    set_herb_value(118);
}
