inherit "/std/herb";

#include "ziola.h"

create_herb()
{
    ustaw_nazwe(KWIAT, PL_MESKI_NOS_NZYW);

    dodaj_przym("brudnozolty", "brudnozolci");
    dodaj_przym("malutki", "malutcy");
   
    ustaw_nazwe_ziola(({"rumianek", "rumianku", "rumiankowi", "rumianek", "rumiankiem", "rumianku"}), PL_MESKI_NOS_NZYW); 
    
    set_id_long("Masz przed soba rumianek, ktory przezuty pozwala lekko zregenerowac "+
	"zmeczenie i uleczyc.\n");

    set_unid_long("Masz przed soba kwiat o brudnozoltym srodeczku i bialych platkach. Pachnie "+
	"charakterystyczna wonia, ktorej zapach napewno zapadnie ci w pamiec...\n");

    set_ingest_verb(({"przezuj", "przezuwasz", "przezuwa", "przezuc"}));
    set_effect(HERB_SPECIAL, special_effect);
    
    set_id_diff(19);
    set_find_diff(2);
    set_decay_time(250);
    set_herb_value(45);
}

void
special_effect()
{
    write("Czujesz jak wracaja ci sily zarowno witalne jak i fizyczne.\n");
    TP->set_fatigue(this_player()->query_fatigue()+7+random(3));
    TP->heal_hp(8+random(2));
}
