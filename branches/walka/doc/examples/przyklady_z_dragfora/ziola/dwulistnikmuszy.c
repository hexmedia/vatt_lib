inherit "/std/herb";

#include "ziola.h"

create_herb()
{
    ustaw_nazwe(ROSLINA, PL_ZENSKI);

    dodaj_przym("wysoki", "wysocy");
    dodaj_przym("zoltawy", "zoltawi");

    ustaw_nazwe_ziola(({"dwulistnik muszy", "dwulistnika muszego", "dwulistnikowi muszemu", "dwulistnik muszy",                         "dwulistnikiem muszym", "dwulistniku muszym"}), PL_MESKI_NOS_NZYW); 
    
    set_id_long("Na pierwszy rzut oka rozpoznajesz Dwulistnik Muszy (Ophrys insectifera), rosline nalezaca "+
	"do rodziny storczykowatych. Posiada ona zolte, piekne kwiaty podobne do much - stad pochodzi nazwa. "+
	"Storczyk ma rowniez bulwe.");

    set_unid_long("Jest to dziwna roslina, z zoltymi kwiatami zblizonymi ksztaltem do much. Zauwazasz "+
	"tez bulwe.\n"); 

    set_decay_time(780);
    set_id_diff(90);
    set_find_diff(8);
    set_amount(9);
    set_herb_value(285);
}
