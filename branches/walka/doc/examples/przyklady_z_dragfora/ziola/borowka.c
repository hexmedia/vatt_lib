inherit "/std/herb";

#include "ziola.h"

create_herb()
{
    ustaw_nazwe(OWOC, PL_MESKI_NOS_NZYW);
  
    dodaj_przym("czerwony", "czerwoni");
    dodaj_przym("twardy", "twardzi");

    ustaw_nazwe_ziola(({"borowka", "borowki", "borowce", "borowke", "borowka", "borowce"}), PL_ZENSKI); 
    
    set_id_long("Widzisz borowke, owoc bardzo czesto spotykany w lesie.\n");

    set_unid_long("Masz przed soba bardzo maly, czerwony owoc. Jest on stosunkowo twardy "+
	"i nie tak latwo go rozmaslic...\n"); 
        
    set_decay_time(45);
    set_id_diff(9);
    set_find_diff(2);   
    set_amount(1);
    set_herb_value(12);
}