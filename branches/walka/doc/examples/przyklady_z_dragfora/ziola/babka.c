inherit "/std/herb";

#include "ziola.h"

create_herb()
{
    ustaw_nazwe(LISC, PL_MESKI_NOS_NZYW);

    dodaj_przym("nerwiasty", "nerwiasci");
    dodaj_przym("lancetowaty", "lancetowaci");

    ustaw_nazwe_ziola(({"babka", "babki", "babce", "babke", "babka", "babce"}), PL_ZENSKI); 
    
    set_id_long("Ciemnozielony lancetowaty lisc, z jasniejszymi zylkami, czesto wystepuje "+
	"w poblizu pastwisk, lak i wszelkiego rodzaju przydrozy. Roslina ta znana jest wsrod "+
	"ludnosci Faerunu jako Babka koniczynowa, Babka waskolistna lub Jezyczki polne, podczas "+
	"gdy prawidlowa nazwa brzmi Babka lancetowata (Plantago lancolata). Przylozona do "+
	"rany powoduje ustanie krwawienia.\n");

    set_unid_long("Jest to dziwny, mocno unerwiony lisc, znany w calym Ishar. Wiesz, ze "+
	"juz nie raz go widziales, jednak nie umiesz sobie przypomniec co to jest...\n"); 

    set_ingest_verb(({"przyloz", "przykladasz", "przyklada", "przylozyc"}));
    
    set_decay_time(300);
    set_id_diff(5);
    set_find_diff(1);
    set_effect(HERB_SPECIAL, special_effect);    
    set_amount(2);
    set_herb_value(10);
}

int
special_effect()
{

   write("Czujesz sie lepiej.\n");
   TP->heal_hp(5+random(3));
   return 1;
}
