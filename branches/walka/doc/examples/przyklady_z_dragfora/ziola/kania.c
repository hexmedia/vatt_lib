inherit "/std/herb";

#include "ziola.h"

create_herb()
{
    ustaw_nazwe(GRZYB, PL_MESKI_OS);

    dodaj_przym("bialy", "biali");
    dodaj_przym("blaszkowaty", "blaszkowaci");

    ustaw_nazwe_ziola(({"kania", "kani", "kani", "kanie", "kania", "kani"}), PL_ZENSKI); 
    
    set_id_long("Masz przed soba, bardzo smaczny grzyb pod kazda postacia - kanie. Ma "+
	"on wspanialy zapach. Niektorzy miela ususzone kanie, a zmielone resztki "+
	"dorzucaja do zup w celu uzyskania lepszego aromatu.\n");

    set_unid_long("Masz przed soba duzy, blaszkowaty grzyb ze wspanialym aromatem. "+
	"Na nodze zauwazasz pierscien.\n"); 

    set_ingest_verb(({"zjedz", "zjadasz", "zjada", "zjesc"}));    
    set_effect(HERB_SPECIAL, special_effect);

    set_decay_time(900);
    set_id_diff(45);
    set_find_diff(4);
    set_amount(22);
    set_herb_value(100);
}

void
special_effect()
{
    object arg;
    setuid();
    seteuid(getuid());
    arg=clone_object(EFEKT+"kania_effect.c");
    arg->move(TP);
    arg->rozjeb_czache();
}