inherit "/std/herb";

#include "ziola.h"

create_herb()
{
    ustaw_nazwe( KWIAT, PL_MESKI_NOS_NZYW);
  
    dodaj_przym("zolty", "zolci");
    dodaj_przym("bardzo maly", "bardzo mali");

    ustaw_nazwe_ziola(({"stulicha", "stulichy", "stulisze", "stuliche", "stulicha", "stulisze"}), PL_ZENSKI); 
    
    set_id_long("Ziolo, ktore masz przed soba to stulicha, bardzo rozpowszechniona trucizna. "+
	"Skutki jej zazycia nie sa moze bardzo zgubne, lecz napewno niewarto sie narazac. Liscie "+
	"Descuarinia sophia so pokryte wloskami.\n");

    set_unid_long("Jest to jasnozoltykwiat o platkach zblizonych ksztaltem do trojkata mieniacych "+
	"sie roznymi kolorami.\n"); 

    set_ingest_verb( ({ "zjedz", "zjadasz", "zjada", "zjesc" }) );
    set_effect(HERB_SPECIAL, special_effect);

    set_id_diff(21);    
    set_find_diff(2);
    set_amount(1);
    set_decay_time(350);
    set_herb_value(25);
}

void
special_effect()
{
    object trucizna;
    setuid();
    seteuid(getuid());
    trucizna = clone_object(EFEKT+"stulicha_effect.c");
    trucizna->move(TP);
    trucizna->set_time(500);
    trucizna->set_interval(20);
    trucizna->start_poison();
}
