inherit "/std/herb";

#include "ziola.h"

create_herb()
{
    ustaw_nazwe(LISC, PL_MESKI_NOS_NZYW);

    dodaj_przym("szeroki", "szerocy");
    dodaj_przym("aromatyczny", "aromatyczni");

    ustaw_nazwe_ziola(({"szalwia", "szalwii", "szalwii", "szalwie", "szalwia", "szalwii"}), PL_ZENSKI);

    set_id_long("Jest to aromatyczny lisc nazywany szalwia. Posluguja sie nimi "+
	"wszelkiego rodzaju magowie i wiedzmy. Ludzie powiadaja, iz powachanie go "+
        "pomaga w zasklepianiu sie ran.\n");

    set_unid_long("Masz przed soba szeroki lisc, od ktorego bije mocny, specyficzny zapach.\n");

    set_ingest_verb(({"powachaj", "wachasz", "wacha", "powachac"}));
    set_effect(HERB_SPECIAL, special_effect);

    set_decay_time(150);
    set_id_diff(25);
    set_find_diff(4);
    set_amount(6);
    set_herb_value(40);
}

int
special_effect()
{
    write("Fizycznie czujesz sie lepiej, natomiast zauwazasz, ze opuszczaja cie sily mentalne...\n");
    TP->heal_hp(5+random(10));
    TP->set_mana(TP->query_mana()-15-random(8));
    return 1;
}
