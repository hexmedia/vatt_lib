inherit "/std/herb";

#include "ziola.h"

create_herb()
{
    ustaw_nazwe(PLATEK, PL_MESKI_NOS_NZYW);

    dodaj_przym("okazaly", "okazali");

    ustaw_nazwe_ziola(({"paproc", "paproci", "paproci", "paproc", "paprocia", "paproci"}), PL_ZENSKI); 
    
    set_id_long("Masz przed soba platek legendarnego kwiatu paproci. Powiadaja, iz posiada on "+
	"zdolnosci magiczne, lecz nikt nie ma calkowitej pewnosci.\n");

    set_unid_long("Jest to okazaly, duzy platek.\n"); 

    set_decay_time(400);
    set_id_diff(100);
    set_find_diff(10);   
    set_amount(9);
    set_herb_value(10000);

}

int
powachaj(string what)
{
    if (what=="platek" || what=="kwiat paproci" || what=="paproc")
    {
        write("Wachasz platek... Czujesz sie lepiej.\n");
        say(QCIMIE(this_player(),PL_MIA)+" wacha okazaly platek.\n");
        TP->set_hp(TP->query_max_hp());
        TO->remove_object();
        return 1;
    }

    return 0;
}

int
zjedz(string what)
{
    if (what=="platek" || what=="kwiat paproci" || what=="paproc")
    {
        write("Zjadasz platek... czujesz sie zupelnie wypoczety.\n");
        say(QCIMIE(this_player(),PL_MIA)+" zjada okazaly platek.\n");
        TP->set_fatigue(TP->query_max_fatigue());
        TO->remove_object();
        return 1;
 
    }
  
    return 0;
}

int
potrzyj(string what)
{
    if (what=="platek" || what=="kwiat paproci" || what=="paproc")
    {
        write("Pocierasz platek... Czujesz sie mniej zmeczony mentalnie.\n");
        say(QCIMIE(this_player(),PL_MIA)+" pociera platek.\n");
        TP->set_mana(TP->query_max_mana());
        TO->remove_object();
        return 1;

    }

    return 0;
}

void
init()
{
   ::init();
   add_action("zjedz", "zjedz");
   add_action("potrzyj", "potrzyj");
   add_action("powachaj", "powachaj");
}