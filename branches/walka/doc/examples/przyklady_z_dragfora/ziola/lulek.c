inherit "/std/herb";

#include "ziola.h"

create_herb()
{

    ustaw_nazwe(KWIAT, PL_MESKI_NOS_NZYW);

    dodaj_przym("brudnozolty", "brudnozolci");

    ustaw_nazwe_ziola(({"lulek czarny", "lulka czarnego", "lulkowi czarnemu", 
                        "lulka czarnego", "lukiem czarnym", "lulku czarnym"}), PL_MESKI_NOS_NZYW); 
    
    set_id_long("Jest to lulek czarny, kwiat, ktory uzyty w nieodpowiedni sposob potrafi "+
	"nawet zabic. Powachany nie powinien wyrzadzac szkod, wrecz przeciwnie - "+
	"podobno ma wlasciwosci lecznicze.\n");

    set_unid_long("Masz przed soba niemilo pachnacy kwiat, posiadajacy fioletowe zylki. Jest "+
	"koloru brudnozoltego.\n");

    set_decay_time(312);
    set_id_diff(26);
    set_find_diff(3);   
    set_amount(11);
    set_herb_value(31);

}

int
powachaj(string what)
{
    if (what=="lulka" || what=="kwiat")
    {
        write("Wachasz brudnozolty kwiat.\n");
        write("Czujesz sie lepiej.\n");
        say(QCIMIE(this_player(),PL_MIA)+" wacha brudnozolty kwiat.\n");
        TP->heal_hp(15+random(4));
        TO->remove_object();
        return 1;
    }

    return 0;
}

int
zjedz(string what)
{
    if (what=="lulka" || what=="kwiat")
    {
        object trutka;
        write("Zjadasz brudnozolty kwiat.\n");
        write("Czujesz sie gorzej.\n");
        say(QCIMIE(this_player(),PL_MIA)+" zjada brudnozolty kwiat.\n");
        TP->set_hp(TP->query_hp()-15+random(2));
        setuid();
        seteuid(getuid());
        trutka=clone_object(EFEKT+"lulek_effect.c");
        trutka->move(TP);
        trutka->rozjeb_czache();
        TO->remove_object();
        return 1;
    }

    return 0;
}


void init()
{
    ::init();
    add_action("zjedz", "zjedz");
    add_action("powachaj", "powachaj");
}