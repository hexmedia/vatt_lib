inherit "/std/herb";

#include "ziola.h"

create_herb()
{
    ustaw_nazwe(GRZYB, PL_MESKI_OS);
  
    dodaj_przym("zielonawy", "zielonawi");
    dodaj_przym("delikatny", "delikatni");

    ustaw_nazwe_ziola(({"gaska", "gaski", "gasce", "gaske","gaska", "gasce"}), PL_ZENSKI); 
    
    set_id_long("Jest to gaska, grzyb, ktory czesto myli sie z muchomorem sromotnikowym. Jest delikatny "+
        "i ma zielonkawy kolor. Z gaska trzeba obdchodzic sie nalezycie, gdyz jedno nieumiejetne dotkniecie "+
        "moze spowodowac zepsucie sie grzyba.\n");

    set_unid_long("Masz przed soba delikatnego, zielonawego grzyba. Grzyb naprawde "+
	"nalezy do tych najdelikatniejszych, albowiem kazde mocniejsze dotkniecie "+
	"powoduje jego obkruszenie.\n"); 

    set_decay_time(800);
    set_id_diff(14);
    set_find_diff(3);
    set_amount(9);
    set_herb_value(39);
}

