inherit "/std/herb";

#include "ziola.h"

create_herb()
{
    ustaw_nazwe(KWIAT, PL_MESKI_NOS_NZYW);
  
    dodaj_przym("okazaly","okazali");
    dodaj_przym("jasnopomaranczowy", "jasnopomaranczowi");

    ustaw_nazwe_ziola(({"arnika", "arniki", "arnice", "arnike", "arnika", "arnice"}), PL_ZENSKI); 
    
    set_id_long("Masz przed soba Arnike Gorska, ktora odpowiednio uzyta moze cie pobudzic, jednak "+
	"w wiekszych ilosciach moze byc niebezpieczna... Jest koloru jasno-pomaranczowego.\n");

    set_unid_long("Jest to bardzo duzy kwiat koloru jasnopomaranczowego. Unosi sie nad nim "+
	"pobudzajacy zapach...\n"); 

    set_effect(HERB_SPECIAL, special_effect);       
    set_decay_time(250);
    set_id_diff(63);
    set_find_diff(7);
    set_amount(8);
    set_herb_value(69);
}

void
special_effect()
{
   write("Czujesz jak napelniaja cie sily.\n");
   TP->set_fatigue(this_player()->query_fatigue()+5+random(20));
}
