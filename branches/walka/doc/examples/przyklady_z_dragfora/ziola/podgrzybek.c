inherit "/std/herb";

#include "ziola.h"

create_herb()
{
    ustaw_nazwe(GRZYB, PL_MESKI_OS);

    dodaj_przym("brunatny", "brunatni");
    dodaj_przym("nieduzy", "nieduzi");

    ustaw_nazwe_ziola(({"podgrzybek", "podgrzybka", "podgrzybkowi","podgrzybka", "podgrzybkiem", "podgrzybku"}), PL_MESKI_NOS_NZYW); 
    
    set_id_long("Poznajesz podgrzybka, grzyba z rodziny borowikowatych. Ten nalezy do gatunku "+
        "podgrzybka brunatnego o kapeluszu ciemnobrazowym. Rosna one najczesciej w lasach szpilkowatych.\n");

    set_unid_long("Masz przed soba grzyba o ciemnobrazowym kapeluszu, do ktorego przylepilo "+
        "sie kilka igiel i sciolki lesnej.\n"); 

    set_decay_time(750);
    set_id_diff(29);
    set_find_diff(2);
    set_amount(25);
    set_herb_value(50);
}