inherit "/std/herb";

#include "ziola.h"

create_herb()
{
    ustaw_nazwe(LISC, PL_MESKI_NOS_NZYW);

    dodaj_przym("zielony", "zieloni");
    dodaj_przym("podluzny", "podluzni");

    ustaw_nazwe_ziola(({"szczaw", "szczawiu", "szczawiowi", "szczaw", "szczawiem", "szczawiu"}), PL_MESKI_NOS_NZYW); 
    
    set_id_long("Masz przed soba typowy szczaw, przysmak wszelkiego rodzaju gryzoni. Nie "+
	"jest on w stanie cie uleczyc, czy usunac zmeczenia, ale napewno jest bardzo "+
	"smaczny.\n");

    set_unid_long("Masz przed soba podluzny, stosunkowo gruby lisc koloru zielonego. Bije "+
	"od niego dziwnie znajomy aromat...\n"); 

    set_decay_time(240);
    set_id_diff(5);
    set_find_diff(1);
    set_amount(4);
    set_herb_value(13);
}
