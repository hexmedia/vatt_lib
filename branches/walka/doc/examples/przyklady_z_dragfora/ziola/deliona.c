inherit "/std/herb";

#include "ziola.h"

create_herb()
{
    ustaw_nazwe( KWIAT, PL_MESKI_NOS_NZYW);
  
    dodaj_przym("zoltawy", "zoltawi");
   
    ustaw_nazwe_ziola(({"deliona", "deliony", "delionie", "delione", "deliona", "delionie"}), PL_ZENSKI); 
    
    set_id_long("Jest to deliona, kwiat o zoltawych, trojkatnych platkach mieniacych sie kolorami "+
	"teczy. Przezute potrafi uspokajac, jednak ta odmiana moze tylko zaszkodzic...\n");

    set_unid_long("Jest to jasnozoltykwiat o platkach zblizonych ksztaltem do trojkata mieniacych "+
	"sie roznymi kolorami.\n"); 
    
    set_ingest_verb(({"powachaj", "wachasz", "wacha", "powachac"}));

    set_id_diff(55);
    set_find_diff(4);
    set_effect(HERB_SPECIAL, special_effect);
    set_decay_time(650);
    set_herb_value(50);
}

void
special_effect()
{
   write("Zaczynasz sie bac...\n");
   TP->add_panic(10+random(10));
}
