inherit "/std/herb";

#include "ziola.h"

create_herb()
{
    ustaw_nazwe(KWIAT, PL_MESKI_NOS_NZYW);
  
    dodaj_przym("fioletowy", "fioletowi");
    dodaj_przym("maly", "mali");

    ustaw_nazwe_ziola(({"fiolek", "fiolka", "fiolkowi", "fiolka", "fiolkiem", "fiolku"}), PL_MESKI_NOS_NZYW); 
    
    set_id_long("Masz przed soba najzwyklejszego w swiecie fiolka, ktorego wlasciwosci pozwalaja "+
        "po wtarciu poprawe wzroku... Pamietac nalezy takze o mozliwych skutkach obocznych.\n");

    set_unid_long("Jest to dziwny fioletowy kwiat, bije od niego subtelny aromat - prawie "+
	"niewyczuwalny...\n"); 

    set_decay_time(10000);
    set_id_diff(20);
    set_find_diff(3);   
    set_amount(2);
    set_herb_value(25);

    set_ingest_verb(({"wetrzyj", "wcierasz", "wciera", "wcierac"}));
    set_effect(HERB_SPECIAL, special_effect);
}

void
special_effect()
{
    object kwas;
    setuid();
    seteuid(getuid());
    kwas=clone_object(EFEKT+"fiolek_effect.c");
    kwas->move(TP);
    kwas->rozjeb_czache();
}