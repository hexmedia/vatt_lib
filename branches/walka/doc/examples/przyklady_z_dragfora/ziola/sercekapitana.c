inherit "/std/herb";

#include "ziola.h"

create_herb()
{
    ustaw_nazwe(LISC, PL_MESKI_NOS_NZYW);

    dodaj_przym("sercowaty", "sercowaci");
    dodaj_przym("maly", "mali");

    ustaw_nazwe_ziola(({"serce kapitana", "serca kapitana", "sercu kapitana", 
                        "serce kapitana", "sercem kapitana", "sercu kapitana"}), PL_NIJAKI_OS); 
    
    set_id_long("Masz przed soba serce kapitana. Ziolo to jest uzywane przez mnichow "+
	"ze wzgledu na jego wlasciwosci lecznice, ale niestety posiada takze skutki uboczne.\n");

    set_unid_long("Dziwny, czerwony lisc w ksztalcie zblizonym do serca. Bije od niego "+
	"dziwna won.\n"); 

    set_ingest_verb(({"przezuj", "przezuwasz", "przezuwa", "przezuc"}));
    set_effect(HERB_SPECIAL, special_effect);        

    set_id_diff(55);
    set_find_diff(9); 
    set_amount(15);
    set_herb_value(350);
    set_decay_time(150);
}

int
special_effect()
{
    write("Czujesz sie lepiej.\n");
    TP->heal_hp(35+random(10));
    TP->set_fatigue(TP->query_fatigue()-4);
    TP->set_mana(TP->query_mana()-12);
    return 1;
}
