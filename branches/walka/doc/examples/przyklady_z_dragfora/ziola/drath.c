inherit "/std/herb";

#include "ziola.h"

create_herb()
{
    ustaw_nazwe(MECH, PL_MESKI_NOS_NZYW);

    dodaj_przym("wilgotny", "wilgotni");
    dodaj_przym("karminowy", "karminowi");

    ustaw_nazwe_ziola(({"drath", "drathu", "drathowi", "drath", "drathem", "drathu"}), PL_MESKI_NOS_NZYW); 
    
    set_id_long("Masz przed soba mech, ktory porasta jedynie pnie dlugowiecznych drzew, zwany jest "+
	"drathem. Gdzieniegdzie jest czczony, tak jak drzewa, ktore porasta. Przezuty potrafi "+
	"zwiekszyc wytrzymalosc.\n");

    set_unid_long("Dziwny, jakby lekko polsujacy i zywy mech...\n"); 

    set_ingest_verb(({"przezuj", "przezuwasz", "przezuwa", "przezuc"}));      
    set_effect(HERB_SPECIAL, special_effect);            
    set_decay_time(90);
    set_id_diff(50);
    set_find_diff(7);   
    set_amount(15);
    set_herb_value(78);
}

void
special_effect()
{
    object drath;
    setuid();
    seteuid(getuid());
    drath=clone_object(EFEKT+"drath_effect.c");
    drath->move(TP);
    drath->rozjeb_czache();
    TP->heal_hp(10+random(5));
    TP->catch_msg("Czujesz sie lepiej.\n");
}
