/* Brama light */
#pragma strict_types
#pragma no_clone

inherit "/lib/brama";
inherit "/d/Faerun/std/room.c";
#include "/sys/stdproperties.h"
#include "/sys/macros.h"


void
create_room()
{
    
    set_short("Brama2");

    set_long("Miejsce w ktorym stoi BRAMA. Nie jest moze zbyt ozdobna, lecz wyglada "+
             "na to ze sam fakt iz STOI napawa ja duma.\n");
    add_brama_item("brame",({"Brama jest otwarta\n","Brama jest zamknieta\n"}));
    add_brama_exit("/d/Wiz/cerdin/pp/br1","polnoc","Brama jest zamknieta\n");
add_brama_desc(({"Otwarta brama prowadzi na polnoc.\n","Zamknieta brama prowadzaca "+
	"na polnoc.\n"}));
    set_czasy_bramy(15); // Mozna od biedy pominac...
}

void
init()
{
::init();
brama_init();
}