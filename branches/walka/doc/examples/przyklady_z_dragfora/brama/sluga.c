inherit "/std/monster";
#include "giogi.h"
#define A_BITE  0
#define A_LCLAW 1
#define A_RCLAW 2
#define H_HEAD 0
#define H_BODY 1
#define H_SKRZY 2
#define H_NOGA 3
void
create_monster()
{

    set_long("Ta nieziemska istota pochodzi z planu istnienia, "+
    "znanego jako Otchlan. Skad mozesz mniec taka pewnosc? Otoz "+
    "tylko tam mieszkaja trzy i pol metrowe istoty z trzema parami "+
    "rak, skrzydlami, oczami plonacymi zywym ogniem, a takze wieloma "+
    "innymi, mniej znaczacymi atrybutami. Ten konkretny demon "+
    "jest otoczony jakas specyficzna aura mrocznej potegi, wskazujaca "+
    "jego wysoka pozycje wsrod swych piekielnych pobratymcow.\n");

    ustaw_odmiane_rasy(({"demon","demona","demonowi","demona","demonem",
      "demonie"}),({"demony","demonow","demonom","demona", 
      "demonami","demonach"}),PL_MESKI_OS);

    
    dodaj_przym("skrzydlaty","skrzydlaci");
    dodaj_przym("mroczny","mroczni");

    set_gender(G_MALE);
    set_stats(({ 350, 250, 350, 100, 100, 250}));

    set_alignment(-1000);
    add_prop(LIVE_I_NEVERKNOWN, 1);
    add_prop(OBJ_I_WEIGHT, 500000);
    add_prop(OBJ_I_VOLUME, 350);

    set_skill(SS_DEFENCE, 90);
    set_skill(SS_UNARM_COMBAT, 100);

    set_attack_unarmed(A_BITE,  10, 13, W_IMPALE, 40, "olbrzymimi szponami");
    set_attack_unarmed(A_LCLAW,  10, 13, W_IMPALE, 40, "jednym ze skrzydel");
    set_attack_unarmed(A_RCLAW,  10, 13, W_IMPALE, 40, "potezna noga");

    set_hitloc_unarmed(H_HEAD, ({ 5,4,5 }), 20, "glowe");
    set_hitloc_unarmed(H_BODY, ({ 5,6,3 }), 80, "korpus");    
    set_hitloc_unarmed(H_SKRZY, ({ 5,6,3 }), 80, "skrzydlo"); 
    set_hitloc_unarmed(H_NOGA, ({ 5,6,3 }), 80, "noge");     
}

void
ublok( object ofiara )
{
ofiara->remove_stun();
ofiara->catch_msg("Odzyskujesz swiadomosc umyslu.\n");
}

void
special_attack( object ob )
{
int stun;
stun = random(100);


    if ( stun < 45 )
	{
	ob->catch_msg(""+QCIMIE(TO,PL_MIA)+" z wielkim impetem uderza cie "+
	    "w glowe swoim skrzydlem! Tracisz orietacje!\n");
	tell_room(environment(TO),""+QCIMIE(TO,PL_MIA)+" "+
	"wykonuje potezny zamach skrzydlem "+
	"ogluszajac "+QIMIE(ob,PL_BIE)+".\n",({TP,ob}));
		ob->add_stun();	
		ob->set_fatigue(ob->query_fatigue()-200);
	set_alarm(8.0,0.0,"ublok",ob);
	
	}	
	

}