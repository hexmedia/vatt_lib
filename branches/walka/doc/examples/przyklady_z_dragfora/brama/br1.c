/* Brama FULL :) */

#pragma strict_types
#pragma no_clone

inherit "/lib/brama";
inherit "/d/Faerun/std/room.c";
#include "/sys/stdproperties.h"
#include "/sys/macros.h"

int przejscie();
int widoczna();

void
create_room()
{
    
    set_short("Ciemna brama");

    set_long("Stoisz w ciemnej bramie, ktora jest jedynym wyjsciem z tego zaulka. "+
             "Niebardzo wiesz skad sie tu wzioles ani po co... \n");
    /* Pierwszy argument to verb do bramy, pierwszy wyraz ktory gracz musi 
       wpisac zeby sie brama otworzyla. Drugi argument to tablica kolejnych 
       wyrazow komendy */
    set_otwieranie_bramy("walnij",({"w cwieki","po cwiekach"}));

    /* Sciezka do lokacji z druga brama, komenda na przejscie, co sie wyswietli
       wrazie proby przejscia przez zamknieta brame.*/
    add_brama_exit("/d/Wiz/cerdin/pp/br2","poludnie","Niebardzo wiesz jak sie "+
	"zabrac do przechodzenia przez zamknieta brame.\n");

    add_brama_item("brame",({"Brama jest otwarta","Brama jest zamknieta"}));

    /* Pierwszy argument to opoznienie zanim sie brama zamknie po jej otwaciu.
       Drugi to opoznienie miedzy wpisaniem przez gracza komendy a otwarciem bramy.
       Trzeci to opoznienie miedzy momentem w ktorym brama sie powinna zamknac a 
       faktycznie sie zamknela. Kazdy argument mozna pominac, i zalecane jest 
       pominiecie trzeciego jesli nie ma potrzeby go dawac. */
    set_czasy_bramy(15,3,4);
add_brama_desc(({"Otwarta brama prowadzi na poludnie.\n","Zamknieta brama prowadzaca "+
	"na poludnie.\n"}));
}


int
przejscie()
{
if (::query_status_bramy())
return 0;
write("Przeciesz brama jest zamknieta!\n");
return 1;
}

int
widoczna()
{
return (::query_status_bramy());
}

/* Kazda z tych funkcji mozna pominac, beda wtedy urzywane standardy */

/*
Co sie dzieje zaraz po wpisaniu przez gracza poprawnej komendy.
Jesli nie urzywacie opoznienia otwierania najlepiej pominac
*/
void
pre_otworz()
{
tell_room(this_object(),"Skad dochodzi glos: Czego znowu?\n");
}

/*
Co sie dzieje gdy brama sie ma juz zamykac. Jesli nie urzywacie 
opoznienia bramy najlepiej pominac.... wogole najlepiej pominac
*/
void
pre_zamknij()
{
tell_room(this_object(),"Brama drgnela, jakby miala sie zamykac.\n");
}

/*
Co sie dzieje gdy brama sie otwiera
*/
void
otwieranie()
{
tell_room(this_object(),"Brama otwiera sie nagle! Wprost niesamowite!\n");
}

/*
Co sie dzieje gdy brama sie zamyka
*/
void
zamykanie()
{
tell_room(this_object(),"Brama zamyka sie nagle! Zdumiewajace!\n");
}

/*
Zwraca napis ktory pojawi sie graczowi jesli sproboje otwierac otwarta
brame.
*/
string
brama_juz_otwarta()
{
return "Walisz i walisz w otwarta brame... \n";
}

/*
Ten napis bedzie wyswietlany graczowi jesli wpisze nieprawidlowa komende
ale pierwszy wyraz sie bedzie zgadzal
*/
string
brama_nf()
{
return "A w coz takiego chcesz walnac?\n";
}

/*
Funkcja wywolywana gdy gracz puka. Sugerowane podmaskowanie jej.
*/
void
brama_pukanie()
{
write("Walisz mocno w brame.\n");
saybb(QCIMIE(this_player(),PL_MIA) + " wali z serdecznym uczuciem w brame.\n");
}

void
init()
{
::init();
brama_init();
}