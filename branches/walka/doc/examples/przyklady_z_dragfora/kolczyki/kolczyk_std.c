/*
 * Plik:	kolczyk_std.c
 * Autor:	Cerdin
 * Data:	27.12.03 (bardzo raaaano!)
 * Opis:	Standard kolczyka.
 */


#include <pl.h>

inherit "/std/object";

#include <macros.h>

void
create_kolczyk()
{
set_long("Jest to pozlacany, niewielki kolczyk.\n");
}

nomask void
create_object()
{
    ustaw_nazwe(        ({"kolczyk","kolczyka","kolczykowi","kolczyk","kolczykiem","kolczyku"}),
        ({"kolczyki","kolczykow","kolczykom","kolczyki","kolczykami","kolczykach"}),
        PL_MESKI_NOS_NZYW);

create_kolczyk();
}

int
query_kolczyk()
{
    return 1;
}

public string
query_auto_load()
{
return MASTER+":";
}

string
query_recover()
{
    return 0;
}

