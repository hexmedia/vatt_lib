/*
 * Plik:	dziurka.c
 * Autor:	Cerdin
 * Data:	27.12.03 (srodek nocy... cos kolo 12 w poludnie... ale mnie zdarlo)
 * Opis:	Obiekt obslugujacy kolczyki, dodajacy sloty na nie,
 *		i obsluge emocji do kolczykow.
 */

#pragma strict_types

inherit "/std/object";
inherit "/cmd/std/command_driver";

#include <cmdparse.h>
#include "/sys/stdproperties.h"
#include "/sys/macros.h"
#include <pl.h>
#include <tasks.h>
#include <ss_types.h>
#include <wa_types.h>
#include <composite.h>
#include <filter_funs.h>
#include "/sys/language.h"

#define SUB	"_subloc_kolczyka"
#define KOLCZYK	"_kolczyk_i_umieszczony_gdzie"
#define SPRAWDZ_KOLCZYK	if (!cele[0]->query_kolczyk())\
			{\
			notify_fail("Przeciez "+cele[0]->short(this_player(),PL_MIA)+\
			" nie jest kolczykiem!\n");\
			}\
			if (!(nr = cele[0]->query_prop(KOLCZYK)))\
			{\
			notify_fail("Ale przeciez "+cele[0]->short(this_player(),PL_MIA)+\
			" nie jest nigdzie umieszczony.\n");\
			return 0;\
			}\

static private mixed *kolczyki = ({0,0,0,0,0,0,0});

static private int *dziurka_max = ({15, // Lewe ucho
				    15, // Prawe ucho
        			    3, // Nos
				    2, // Lewa brew
				    2, // Prawa brew
				    1, // Podbrodek
				    1, // Jezyk
				  });
static private int *dziurka = ({0,0,0,0,0,0,0});

static private string *opis_mie = ({"lewym uchu",
				"prawym uchu",
				"nosie",
				"lewej brwi",
				"prawej brwi",
				"podbrodku",
				"jezyku",
				});
static private string *opis_bie = ({"lewe ucho",
				"prawe ucho",
				"nos",
				"lewa brew",
				"prawa brew",
				"podbrodek",
				"jezyk",
				});

static private int Size = sizeof(dziurka);
static private int kolczyk = 0;

void
create_object()
{
    ustaw_nazwe(        ({"dziurka","dziurki","dziurce","dziurke","dziurka","dziurce"}),
        ({"dziurki","dziurek","dziurkom","dziurki","dziurkami","dziurkach"}),
        PL_ZENSKI);
    remove_prop(OBJ_I_VALUE);
    remove_prop(OBJ_I_VOLUME);
    remove_prop(OBJ_I_WEIGHT);

    add_name("_dziurka_od_kolczykow");
    set_no_show();
}

int
query_dziurka()
{
return 1;
}

/* Funkcja:	Sprawdz dziurke
 * Opis:	Podaje czy mozliwe jest stworzenie kolejnej 
 *		dziurki na podanej lokacji
 * Argumenty:   string lokacja - nazwa w bierniku na ktorej chcemy 
 *		stworzyc dziurke.
 * Zwraca:	int, 1 jesli mozna stworzyc tam kolejna dziurke
 *		0 - jesli nie.
 *		-1 - Jesli nie ma takiej lokacji do przebicia
 */
int
sprawdz_dziurke(string lokacja)
{
int nr;

if ((nr = member_array(lokacja, opis_bie)) == -1) return -1;

if (dziurka_max[nr] == dziurka[nr]) return 0;

return 1;

}

/* Funkcja:	dodaj_dziurke
 * Opis:	Tworzy dziurke kolejna na podanej lokacji
 * Argumenty:   string lokacja - nazwa w bierniku na ktorej chcemy 
 *		stworzyc dziurke.
 * Zwraca:	int, 1 jesli prawidlowo stworzona
 *		0 - jesli nie.
 *		-1 - Jesli nie ma takiej lokacji do przebicia
 */

int
dodaj_dziurke(string lokacja)
{
int nr;

if ((nr = member_array(lokacja, opis_bie)) == -1) return -1;

if (dziurka_max[nr] == dziurka[nr]) return 0;

dziurka[nr]++;
return 1;
}

int *
query_dziurki()
{
return dziurka + ({});
}

int
koumiesc(string str)
{
string *pomoc;
object *cele;
int nr;
int nr2;

notify_fail("Koumiesc co w czym?\n");

if (!str || sizeof(pomoc = explode(str," w ")) != 2) return 0;

cele = parse_this(pomoc[0], "%i:" + PL_MIA);
if (!sizeof(cele)) return 0;
if (sizeof(cele) != 1)
    {
    notify_fail("Naprawde latwiej jest umieszczac pojedynczo.\n");
    return 0;
    }
if ((nr = member_array(pomoc[1], opis_mie)) == -1) return 0;
if (dziurka[nr] == 0) return 0;
if (!cele[0]->query_kolczyk()) return 0;
if (nr2 = cele[0]->query_prop(KOLCZYK))
    {
    notify_fail("Alez "+cele[0]->short(this_player(),PL_MIA)+" jest juz "+
	"umieszczony w "+opis_mie[nr2-1]+"!\n");
    return 0;
    }

if (dziurka[nr] == sizeof(kolczyki[nr]))
    {
    notify_fail("Nie znajdujesz zadnego wolnego miejsca w "+pomoc[1]+" by "+
	"umiescic "+cele[0]->short(this_player(),PL_BIE)+".\n");
    return 0;
    }

write("Umieszczasz "+cele[0]->short(this_player(),PL_BIE)+" w "+pomoc[1]+".\n");
saybb(QCIMIE(this_player(),PL_MIA)+" umieszcza "+cele[0]->short(this_player(),PL_BIE)
	+" w "+pomoc[1]+".\n");
cele[0]->add_prop(OBJ_I_DONT_INV,1);
cele[0]->add_prop(KOLCZYK,nr+1);
cele[0]->add_prop(OBJ_I_NO_DROP,"Najpierw musisz kozdjac "+
			cele[0]->short(this_player(),PL_BIE)+"!\n");
cele[0]->add_prop(OBJ_I_NO_GIVE,"Najpierw musisz kozdjac "+
			cele[0]->short(this_player(),PL_BIE)+"!\n");
cele[0]->add_prop(OBJ_M_NO_SELL,"Najpierw musisz kozdjac "+
			cele[0]->short(this_player(),PL_BIE)+"!\n");

kolczyk++;

if (!sizeof(kolczyki[nr]))
   kolczyki[nr] = ({ cele[0] });
else
    kolczyki[nr] += ({ cele[0] });

return 1;

}

int
zdejmij_wszystkie()
{
object *kol=({});
int i = -1;

if (!kolczyk)
    {
    notify_fail("Przeciez nie masz na sobie zadnego kolczyka.\n");
    return 0;
    }

    while (++i < Size)
	{
	if (!sizeof(kolczyki[i]))
	    continue;
	kol += kolczyki[i];
	kolczyki[i] = 0;
	}

kolczyk = 0;

write("Zdejmujesz z siebie "+COMPOSITE_DEAD(kol,PL_BIE)+".\n");
saybb(QCIMIE(this_player(),PL_MIA)+" zdejmuje "+COMPOSITE_DEAD(kol,PL_BIE)+".\n");

kol->remove_prop(OBJ_I_DONT_INV);
kol->remove_prop(KOLCZYK);
kol->remove_prop(OBJ_I_NO_DROP);
kol->remove_prop(OBJ_I_NO_GIVE);
kol->remove_prop(OBJ_M_NO_SELL);
return 1;
}


int
kozdejmij(string str)
{
object *cele;
int nr;

notify_fail("Kozdejmij co?\n");
if (!str) return 0;

if (str == "wszystkie kolczyki") return zdejmij_wszystkie();

if (!kolczyk) 
    {
    notify_fail("Przeciez nie masz na sobie zadnego kolczyka.\n");
    return 0;
    }

cele = parse_this(str, "%i:" + PL_MIA);

if (!sizeof(cele))
    return 0;

if (sizeof(cele) != 1)
    {
    notify_fail("Prosciej bedzie kozdejmowac pojedyczo.\n");
    return 0;
    }

SPRAWDZ_KOLCZYK

write("Zdejmujesz "+cele[0]->short(this_player(),PL_BIE)+".\n");
saybb(QCIMIE(this_player(),PL_MIA)+" zdejmuje "+cele[0]->short(this_player(),PL_BIE)+".\n");
cele[0]->remove_prop(OBJ_I_DONT_INV);
cele[0]->remove_prop(KOLCZYK);
cele[0]->remove_prop(OBJ_I_NO_DROP);
cele[0]->remove_prop(OBJ_I_NO_GIVE);
cele[0]->remove_prop(OBJ_M_NO_SELL);

kolczyk--;

kolczyki[nr-1] -= ({cele[0]});

return 1;

}

int
kopomoc(string str)
{
if (str)
    {
    notify_fail("Kopomoc?\n");
    return 0;
    }

write("    *** Pomoc do kolczykow wszelakich. ***\n"+
      " * koumiesc       - Umieszczenie kolczyka.\n"+
      " * kozdejmij      - Zdjecie kolczyka. Mozna takze \n"+
      "                    kozdjac wszystkie kolczyki.\n\n"+
      " * kobaw sie      - Zabawa kolczykiem.\n"+
      " * kopociagnij    - Sprawdzenie umocowania kolczyka.\n"+
      " * kopoleruj      - Polerowanie kolczyka.\n"+
      " * kowyczysc      - Oczyszczenie kolczyka.\n"+
      " * kopodrap sie   - Drapanie sie w jakas czesc twarzy.\n"+
      " * kozaslon       - Proba ukrycia kolczyka przed wzrokiem innych\n"+
      " * kopopraw       - Poprawienie kolczyka.\n"+
      " * kopochwal sie  - Pochwalenie sie kolczykiem.\n\n"+
      " * kosprawdz      - Podaje gdzie i ile masz dziurek na kolczyki.\n"+
      " * kopomoc        - Powyzszy spis.\n");
return 1;
}

int
kobaw(string str)
{
object *cele;
int nr;

notify_fail("Kobaw sie czym?\n");
if (!str) return 0;

if (!kolczyk) 
    {
    notify_fail("Przeciez nie masz na sobie zadnego kolczyka.\n");
    return 0;
    }

cele = parse_this(str, "[sie] %i:" + PL_NAR);

if (!sizeof(cele))
    return 0;

if (sizeof(cele) != 1) 
    {
    notify_fail("Prosciej bawic sie kolczykami pojedynczo.\n");
    return 0;
    }

SPRAWDZ_KOLCZYK

write("Bawisz sie swoim "+cele[0]->short(this_player(),PL_NAR)+" umieszczonym"+
	" w "+opis_mie[nr-1]+".\n");
saybb(QCIMIE(this_player(),PL_MIA)+" bawi sie swoim "
+cele[0]->short(this_player(),PL_NAR)+" umieszczonym"+
" w "+opis_mie[nr-1]+".\n");
return 1;
}

int
kopociagnij(string str)
{
object *cele;
int nr;

notify_fail("Kopociagnij za co?\n");
if (!str) return 0;

if (!kolczyk) 
    {
    notify_fail("Przeciez nie masz na sobie zadnego kolczyka.\n");
    return 0;
    }

cele = parse_this(str, "[za] %i:" + PL_BIE);

if (!sizeof(cele))
    return 0;

if (sizeof(cele) != 1) 
    {
    notify_fail("Niebardzo wiesz jak sie do tego zabrac.\n");
    return 0;
    }

SPRAWDZ_KOLCZYK

write("Ciagniesz delikatnie za umieszczony w "+opis_mie[nr-1]+
" "+cele[0]->short(this_player(),PL_MIA)+", sprawdzajac czy napewno "+
"nie wypadnie.\n");
saybb(QCIMIE(this_player(),PL_MIA)+" ciagnie delikatnie za umieszczony w "
+opis_mie[nr-1]+" "+cele[0]->short(this_player(),PL_MIA)+", jakby chcial"+
this_player()->koncowka("","a")+" sprawdzic "+cele[0]->koncowka("jego","jej","jego")+
" umocowanie i zyskac pewnosc ze nie wypadnie przy jakims gwaltownym ruchu.\n");
return 1;

}

int
kopoleruj(string str)
{
object *cele;
int nr;

notify_fail("Kopoleruj co?\n");
if (!str) return 0;

if (!kolczyk) 
    {
    notify_fail("Przeciez nie masz na sobie zadnego kolczyka.\n");
    return 0;
    }

cele = parse_this(str, "%i:" + PL_BIE);

if (!sizeof(cele))
    return 0;

if (sizeof(cele) != 1) 
    {
    notify_fail("Niebardzo wiesz jak sie do tego zabrac.\n");
    return 0;
    }

SPRAWDZ_KOLCZYK

write("Kilkoma zdecydowanymi ruchami polerujesz "+
cele[0]->short(this_player(),PL_BIE)+".\n");
saybb(QCIMIE(this_player(),PL_MIA)+" kilkoma pewnymi ruchami poleruje "
+cele[0]->short(this_player(),PL_MIA)+", sprawiajac ze bizuteria "+
"nabiera nowego polysku.\n");
return 1;

}

int
kowyczysc(string str)
{
object *cele;
int nr;

notify_fail("Kowyczysc co?\n");
if (!str) return 0;

if (!kolczyk) 
    {
    notify_fail("Przeciez nie masz na sobie zadnego kolczyka.\n");
    return 0;
    }

cele = parse_this(str, "%i:" + PL_BIE);

if (!sizeof(cele))
    return 0;

if (sizeof(cele) != 1) 
    {
    notify_fail("Niebardzo wiesz jak sie do tego zabrac.\n");
    return 0;
    }

SPRAWDZ_KOLCZYK

write("Zdejmujesz na chwile umieszczony w "+opis_mie[nr-1]+" "+
cele[0]->short(this_player(),PL_BIE)+", czyszczac dokladne "+
"kazdy element blyskotki.\n");
saybb(QCIMIE(this_player(),PL_MIA)+" zdejmuje na chwile umieszczony w "+opis_mie[nr-1]+" "
+cele[0]->short(this_player(),PL_MIA)+", oczyszczajac dokladnie kazdy element"+
" blyskotki.\n");
return 1;

}



int
kozaslon(string str)
{
object *cele;
int nr;

notify_fail("Kozaslon co?\n");
if (!str) return 0;

if (!kolczyk) 
    {
    notify_fail("Przeciez nie masz na sobie zadnego kolczyka.\n");
    return 0;
    }

cele = parse_this(str, "%i:" + PL_BIE);

if (!sizeof(cele))
    return 0;

if (sizeof(cele) != 1) 
    {
    notify_fail("Niebardzo wiesz jak sie do tego zabrac.\n");
    return 0;
    }

SPRAWDZ_KOLCZYK

write("Naglym gestem przykladasz dlon do umieszczonego w "+opis_mie[nr-1]+" "+
cele[0]->short(this_player(),PL_BIE)+", probojac go zaslonic przed czyimkolwiek "+
"spojrzeniem.\n");
saybb(QCIMIE(this_player(),PL_DOP)+" naglym gestem przyklada dlon do "+
"umieszczonego w "+opis_mie[nr-1]+" "
+cele[0]->short(this_player(),PL_DOP)+", bezkutecznie probojac go ukryc przed "+
"twym wzrokiem.\n");
return 1;

}

int
kopodrap(string str)
{
int nr;

if (!str || sscanf(str,"sie w %s",str) != 1 || (nr = member_array(str,opis_bie)) == -1 )
    {
    notify_fail("Kopodrap sie gdzie?\n");
    return 0;
    }

if (sizeof(kolczyki[nr]))
    str += ", mimowolnie poprawiajac umieszczony tam "+
    kolczyki[nr][0]->short(this_player(),PL_BIE);

write("Drapiesz sie w "+str+".\n");
saybb(QCIMIE(this_player(),PL_MIA)+" drapie sie w "+str+".\n");
return 1;
}

int
kopopraw(string str)
{
object *cele;
int nr;

notify_fail("Kopopraw co?\n");
if (!str) return 0;

if (!kolczyk) 
    {
    notify_fail("Przeciez nie masz na sobie zadnego kolczyka.\n");
    return 0;
    }

cele = parse_this(str, "%i:" + PL_BIE);

if (!sizeof(cele))
    return 0;

if (sizeof(cele) != 1) 
    {
    notify_fail("Niebardzo wiesz jak sie do tego zabrac.\n");
    return 0;
    }

SPRAWDZ_KOLCZYK

write("Poprawiasz "+
cele[0]->short(this_player(),PL_BIE)+", delikatnie zmieniajac jego polozenie.\n");
saybb(QCIMIE(this_player(),PL_DOP)+" poprawia "
+cele[0]->short(this_player(),PL_BIE)+", delikatnie zmieniajac jego polozenie.\n");
return 1;

}


int
kopochwal(string str)
{
object *cele;
int nr;

notify_fail("Kopochwal sie czym?\n");
if (!str) return 0;

if (!kolczyk) 
    {
    notify_fail("Przeciez nie masz na sobie zadnego kolczyka.\n");
    return 0;
    }

cele = parse_this(str, "[sie] %i:" + PL_NAR);

if (!sizeof(cele))
    return 0;

if (sizeof(cele) != 1) 
    {
    notify_fail("Niebardzo wiesz jak sie do tego zabrac.\n");
    return 0;
    }

SPRAWDZ_KOLCZYK

write("Z niklym usmiechem na twarzy pocierasz umieszczony w "+opis_mie[nr-1]+" "+
cele[0]->short(this_player(),PL_MIA)+", probojac zwrocic na niego uwage otoczenia.\n");
saybb(QCIMIE(this_player(),PL_DOP)+" pociera "+
"umieszczony w "+opis_mie[nr-1]+" "
+cele[0]->short(this_player(),PL_MIA)+", starajac sie zwrocic na niego twa uwage.\n");
return 1;

}

string *
query_opis_dziur(int i)
{
string str;
if (!dziurka[i]) return ({});
str = LANG_SNUM(dziurka[i], PL_MIA, PL_ZENSKI)+" ";
switch(dziurka[i])
    {
    case 1:     str += "dziurke"; break;
    case 2..4:  str += "dziurki"; break;
    case 5..20: str += "dziurek"; break;
    }
str += " w "+opis_mie[i];

return ({ str });
}

int
kosprawdz(string str)
{
int i = -1;
string *tab = ({});

if (str)
    {
    notify_fail("Kosprawdz?\n");
    return 0;
    }

while (++i < Size)
    tab += query_opis_dziur(i);

write("Masz "+COMPOSITE_WORDS(tab)+".\n");
return 1;
}

void
init()
{
    ::init();
    add_action("koumiesc","koumiesc");
    add_action("kozdejmij","kozdejmij");
    add_action("kobaw","kobaw");
    add_action("kopociagnij","kopociagnij");
    add_action("kopoleruj","kopoleruj");
    add_action("kowyczysc","kowyczysc");
    add_action("kopodrap","kopodrap");
    add_action("kozaslon","kozaslon");
    add_action("kopopraw","kopopraw");
    add_action("kopochwal","kopochwal");
    add_action("kosprawdz","kosprawdz");
    add_action("kopomoc","kopomoc");

}

void
enter_env(object dest, object old)
{
    ::enter_env(dest,old);
    if (!living(dest)) return;
    dest->add_subloc(SUB,this_object());
}

void
leave_env(object from, object to)
{
    ::leave_env(from, to);
    if (!living(from)) return;
    from->remove_subloc(SUB);
}

string
show_subloc(string subloc, object on_obj, object for_obj)
{
string str;
string allsub="";
int i = -1;

   if ((subloc != SUB) || on_obj->query_prop(TEMP_SUBLOC_SHOW_ONLY_THINGS)
	|| !kolczyk)
      return "";

if (on_obj == for_obj) 
    str = " masz ";
else
    str = " ma ";

set_this_player(for_obj);

while (++i < Size)
    {
    if (!sizeof(kolczyki[i]))
	continue;
    allsub += "W "+opis_mie[i]+str+COMPOSITE_DEAD(kolczyki[i],PL_BIE)+".\n";
    }
return allsub;

}

public void
init_arg(string arg)
{
string *pomoc = explode(arg,"##");
int i = -1;

if (sizeof(pomoc) != Size) return;
    while (++i < Size)
	dziurka[i] = atoi(pomoc[i]);
}

public string
query_auto_load()
{
string str = "";
int i = -1;

    while (++i < Size)
	str += dziurka[i]+"##";
    
    return MASTER + ":" + str;
}