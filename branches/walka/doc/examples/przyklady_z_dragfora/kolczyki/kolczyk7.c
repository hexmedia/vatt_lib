#include <pl.h>
#include "/sys/stdproperties.h"
inherit "/d/Faerun/std/kolczyk_std.c";

void
create_kolczyk()
{
set_long("Do wykonanej ze zlota zapinki przymocowano niewielka "+
	"perle, do polowy zawinieta w zloty lisc, nabijany cyrkoniami. "+
	"Calosc jest leciutko zmatowiala, co jednak nie odbiera jej "+
	"swoistego uroku.\n");
dodaj_przym("perlowy","perlowi");
dodaj_przym("zloty","zloci");

    add_prop(OBJ_I_VALUE, 500);
    add_prop(OBJ_I_VOLUME, 27);
    add_prop(OBJ_I_WEIGHT, 25);

}
