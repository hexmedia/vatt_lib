#include <pl.h>
#include "/sys/stdproperties.h"
inherit "/d/Faerun/std/kolczyk_std.c";

void
create_kolczyk()
{
set_long("Widzisz przed soba prawdziwe dzielo sztuki. "+
	"Cienkie niczym wlos, srebrne nici spleciono w misterne "+
	"druty, z tych ostatnich formujac pentagram. Na srodku kszaltu "+
	"widzisz niewielki okruch jakiegos ciemnoniebieskiego kamienia, moze "+
	"szafiru? Do calej tej konstrukcji przymocowano gustowna zapinke "+
	"takze wykonana z czystego, jasniejacego srebra. Nie masz pojecia "+
	"ktoz moglby wykonac tak misterna ozdobe.\n" );
dodaj_przym("srebrny","srebrni");
dodaj_przym("pentagramiczny","pentagramiczni");

    add_prop(OBJ_I_VALUE, 200);
    add_prop(OBJ_I_VOLUME, 17);
    add_prop(OBJ_I_WEIGHT, 13);

}
