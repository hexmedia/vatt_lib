#pragma strict_types
inherit "/d/std/zbroja";
#include <stdproperties.h>
#include <pl.h>
#include <macros.h>
#include <wa_types.h>
object wielder;
int kutas();
void
create_armour()
{
     set_autoload();
      ustaw_nazwe( ({ "dresy", "dresow", "dresom", "dresy",
      		      "dresami", "dresach" }), PL_ZENSKI);


      ustaw_shorty(({ "dlugie lsnice dresy", "dlugich lsniacych dresow", 
      		      "dlugim lsniacym dresom", "dlugie lsniace dresy", 
      		      "dlugimi lsniacymi dresami", "dlugich lsniacych dresach" }),
      		      PL_ZENSKI);

     dodaj_przym("dlugi", "dludzy" );
     set_long("Jest to para dlugich wspaniale lsniacych dresow. Dresy maja "+
     "po obu bokach po cztery paski, oraz z przodu napis Adonis. Dresy jak to "+
     "dresy maja duzo kieszeni aby mozna bylo w nich przechowywac duzo komorek. "+
     "Zostaly one stworzone specjalnie dla Durgona najwiekszego doktora DRESIARZA :P."+
     "Serdeczne pozdrowienia od mistrzunia Simona:))). Przyjemnego uzywania.\n");     
     
    set_af(this_object());
    set_alarm(0.5,0.0,"add_wielder");
     set_ac(A_LEGS, 2, 2, 2);
     add_prop(OBJ_I_WEIGHT,200);
}
mixed
wear(object what)
{
   set_alarm(0.1,0.0,"add_wielder");
   write("Zakladasz ostroznie spodnie dresow na nogi, wygladzajac paski po bokach. "+
   "A nastepnie zakladasz bluze i z zadowolona mina zapinasz jej zamek. Czujesz jak "+
   "powoli podnosi ci sie namiot z podniecenia.\n");
   say("Gdy "+
       QIMIE(this_player(),PL_MIA) + " zaklada ostroznie spodnie dresow na nogi, "+
       "wygladzajac paski po bokach. A nastepnie zaklada bluze i zadowolona mina "+
       "zapina jej zamek, widzisz jak powoli podnosi sie mu namiot z podniecenia.\n");
    add_action(kutas,"kutas");
   return 1;
}
void
init()
{
    ::init();

}
void
add_wielder()
{
    wielder = environment(this_object());
}

object
query_worn()
{
    return wielder;
}    

mixed
remove(object what)
{
   set_this_player(wielder);
   if(environment(this_object())!=wielder) 
       return 1;
   write("Zdejmujac dresy czujesz sie podle muszac pozbywac sie takiego "+
   "pieknego ubrania, ztwierdzasz z niezadowolona mina ze namiot sie kladzie.\n");
   say(QCIMIE(this_player(),PL_MIA)+" zdejmuje dresy, "+
       "widzisz jaka ma niezadowolona mine poniewaz namiot sie kladzie.\n");     
   return 1;
}
int 
kutas()
{
   if(environment(this_object())!=wielder) 
       return 1;
    write("Rozpinasz rozporek spodni dresow, wyciagasz malego i wszem i obec "+
    "wymachujesz nim na wszystkie strony opryskujac wszystkich moczem.\n");
   say(QCIMIE(this_player(),PL_MIA)+" rozpina rozporek spodni dresow, "+
   "wyciaga swojego malego i wszem i obec wymachuje nim opryskujac ciebie "+
   "moczem.\n");
   return 1;
}