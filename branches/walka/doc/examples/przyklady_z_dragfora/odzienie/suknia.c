/*
 * Filename: suknia.c
 *
 * Zastosowanie: Suknia dla maga Christy.
 * 
 * Copyright Volothamp Geddarm.
 *
 */
inherit "/d/Faerun/std/stdubrania.c";

#include <stdproperties.h>
#include <wa_types.h>
#include <formulas.h>
#include <macros.h>

void
create_ubranie()
{
    ustaw_nazwe( ({"suknia", "sukni", "sukni", "suknie", "suknia", "sukni"}),
		         ({"suknie", "sukni", "sukniom", "suknie", "sukniami", "sukniach"}), PL_ZENSKI);
    
	dodaj_przym("dlugi", "dludzy");
    dodaj_przym("ametystowy", "ametystowi");
    set_long("Jest to dluga, siegajaca stop suknia, ktora wykonano z jakiegos "
	  + "bardzo drogiego, a zarazem lekkiego materialu. Ow material musial byc "
	  + "kosztownym suknem jakie jedynie sprzedaja w portowych, kupieckich miastach. "
	  + "Sama w sobie ozdobna szata ma odcien ciemnego ametystu, ktory kusi oczy "
	  + "kazdej osoby, ktora nan spoglada. Uwazasz rowniez, ze takie odzienie maja "
	  + "zwyczaj nosic jedynie bogate i piekne damy mlodego wieku, ktore czesto "
	  + "uczeszczaja na huczne biediady oraz inne uroczystosci nie przejmujac sie "
	  + "ludzmi nizszego pochodzenia.\n");
    set_slots(A_BODY, A_ARMS);
    add_prop(OBJ_I_VOLUME, query_default_weight() / 6);
	add_prop(OBJ_I_VALUE, 10);
}


