/*
 * Filename: pas.c
 *
 * Zastosowanie: Pas.
 * 
 * Copyright Volothamp Geddarm.
 *
 */
inherit "/d/Faerun/std/stdubrania.c";

#include <stdproperties.h>
#include <wa_types.h>
#include <formulas.h>
#include <macros.h>

void
create_ubranie()
{

    ustaw_nazwe( ({"pas", "pasa", "pasowi", "pas", "pasem", "pasie"}),
		         ({"pasy", "pasow", "pasom", "pasy", "pasami", "pasach"}), PL_MESKI_NOS_NZYW);

    dodaj_przym("zdobiony", "zdobieni");

    set_long("Jest to znakomitego wykonania, pas ze zwierzecej skory. Posiada on pozlacana "
      + "klamre oraz kilka sztucznych diamentow, wen wprawionych. Dobrze sie on " 
	  + "prezentuje i na wybor kupna tegoz pasa, polakomilo by sie wielu zamoznych "
      + "mieszkancow niejednego kupieckiego miasta. Po jego wewnetrznej stronie "
      + "dostrzegasz niewielki znaczek pracowni krawieckiej 'Krolewski Gryf'.\n");
	
	set_slots(A_WAIST);

    add_prop(OBJ_I_WEIGHT, 100);
	add_prop(OBJ_I_VALUE, 450);
}

