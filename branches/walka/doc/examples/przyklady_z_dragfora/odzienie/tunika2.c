/*
 * Filename: tunika2.c
 *
 * Zastosowanie: Tunika dla mieszczanina.
 * 
 * Copyright Volothamp Geddarm.
 *
 */
inherit "/d/Faerun/std/stdubrania.c";

#include <stdproperties.h>
#include <wa_types.h>
#include <formulas.h>
#include <macros.h>

void
create_ubranie()
{
    ustaw_nazwe( ({"tunika", "tuniki", "tunice", "tunike", "tunika", "tunice"}),
		         ({"tuniki", "tunik", "tunikom", "tuniki", "tunikami", "tunikach"}), PL_ZENSKI);
    dodaj_przym("czerwony", "czerwoni");
    dodaj_przym("lekki", "leccy");
    set_long("Jest to lekka tunika, ktora wykonano z materialow nieco przypominajacych "
	  + "malarskie plotno zabarwione w ciemnym odcieniu czerwieni z domieszka barw oliwnych. "
	  + "Mniej wiecej posrodku tego odzienia znajduja sie specjalne miejsca, przez ktore przetoczyc "
	  + "mozna skorzany pasek, zas znajdujacy sie u gory kolnierz jest rozpiety dajac wlascicielowi "
	  + "tuniki pelna swobode.\n");
    set_slots(A_BODY);
    add_prop(OBJ_I_VOLUME, query_default_weight() / 6);
	add_prop(OBJ_I_VALUE, 10);
}


