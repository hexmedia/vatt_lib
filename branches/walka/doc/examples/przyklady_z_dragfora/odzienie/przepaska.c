/*
 * Filename: przepaska.c
 *
 * Zastosowanie: Przepaski nie widziales?
 * 
 * Copyright Volothamp Geddarm.
 *
 */
inherit "/d/Faerun/std/stdubrania.c";

#include <stdproperties.h>
#include <wa_types.h>
#include <formulas.h>
#include <macros.h>

void
create_ubranie()
{
    ustaw_nazwe( ({"przepaska", "przepaski", "przepasce", "przepaske", "przepaska", "przepasce"}),
		         ({"przepaski", "przepasek", "przepaskom", "przepaski", "przepaskami", "przepaskach"}), PL_ZENSKI);
    
	dodaj_przym("czarny", "czarni");
    
	set_long("Oto dosyc sporych rozmiarow przepaska, jaka przeslaniaja sobie oko osoby, ktore "
	  + "uszkodzily je sobie na przyklad podczas licznych, krwawych bitew. Ten egzemplarz "
	  + "byl prawdopodobnie dosyc czesto uzywany, gdyz po jego wewnetrznej stronie dostrzegasz "
	  + "liczne slady po zakrzeplej krwi.\n");

    add_prop(OBJ_I_WEIGHT, 50);
	add_prop(OBJ_I_VALUE, 0);
}


