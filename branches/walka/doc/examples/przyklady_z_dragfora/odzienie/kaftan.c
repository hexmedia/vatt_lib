/*
 * Filename: kaftan.c
 *
 * Zastosowanie: Kaftan dla kupca w gospodzie.
 * 
 * Copyright Volothamp Geddarm.
 *
 */
inherit "/d/Faerun/std/stdubrania.c";

#include <stdproperties.h>
#include <wa_types.h>
#include <formulas.h>
#include <macros.h>

void
create_ubranie()
{
    ustaw_nazwe( ({"kaftan", "kaftana", "kaftanowi", "kaftan", "kaftanem", "kaftanie"}),
		         ({"kaftany", "kaftanow", "kaftanom", "kaftany", "kaftanami", "kaftanach"}), PL_MESKI_NOS_NZYW);
    dodaj_przym("gruby", "grubi");
    dodaj_przym("kupiecki", "kupieccy");
    set_long("Jest to szeroki, wykonany z grubego materialu kaftan jaki czesto "
	  + "ujrzec mozna na grubym ciele jakiegos dostatnio noszacego sie kupca. "
	  + "Na tym zas egzemplarzu mozna zobaczyc nie tylko slady ptasich odchodow "
	  + "lecz i nie do konca sprane slady plam po winie badz innych mocnych trunkach.\n");
    set_slots(A_BODY, A_ARMS);
    add_prop(OBJ_I_VOLUME, query_default_weight() / 6);
	add_prop(OBJ_I_VALUE, 10);
}


