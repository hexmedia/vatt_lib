/*
 * Filename: fartuszek.c
 *
 * Zastosowanie: Fartuszek dla Jasmine.
 * 
 * Copyright Volothamp Geddarm.
 *
 */
inherit "/d/Faerun/std/stdubrania.c";

#include <stdproperties.h>
#include <wa_types.h>
#include <formulas.h>
#include <macros.h>

void
create_ubranie()
{
    ustaw_nazwe( ({"fartuszek", "fartuszka", "fartuszkowi", "fartuszek", "fartuszkiem", "fartuszku"}),
		         ({"fartuszki", "fartuszkow", "fartuszkom", "fartuszki", "fartuszkami", "fartuszkach"}), PL_MESKI_NOS_NZYW);
    dodaj_przym("bialy", "biali");
    dodaj_przym("kucharski", "kucharscy");
    set_long("Jest to bialutki niczym snieg kaftanik, ktory nosza zazwyczaj kucharze "
	  + "badz inne osoby parajace sie z gotowaniem. O dziwo jest on dosyc czysty "
	  + "nieliczac kilku plam, ktore rozpoznajesz jako slady domowych konfitur. "
	  + "Wygklada na to, ze przyda sie on duzo bardziej w kuchni przy kafelkowym piecu, "
	  + "anizeli na polu bitwy, w ktrorym poplamic go moze nie tylko dzem lecz i "
	  + "krew wrogow.\n");
set_slots(A_TORSO);
    add_prop(OBJ_I_VOLUME, query_default_weight() / 6);
	add_prop(OBJ_I_VALUE, 10);
}


