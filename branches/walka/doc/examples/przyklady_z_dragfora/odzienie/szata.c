/*
 * Filename: szata.c
 *
 * Zastosowanie: Szata dla maga Orlena.
 * 
 * Copyright Volothamp Geddarm.
 *
 */
inherit "/d/Faerun/std/stdubrania.c";

#include <stdproperties.h>
#include <wa_types.h>
#include <formulas.h>
#include <macros.h>

void
create_ubranie()
{
    ustaw_nazwe( ({"szata", "szaty", "szacie", "szate", "szata", "szacie"}),
		         ({"szaty", "szat", "szatom", "szaty", "szatami", "szatach"}), PL_ZENSKI);
    
	dodaj_przym("niebieski", "niebiescy");
    dodaj_przym("czarodziejski", "czarodziejscy");
    set_long("Jest to dluga, siegajaca stop szata jaka nosza szanujacy sie "
	  + "magowie na calym Wybrzezu Mieczy. Ozdobiona jest w liczne tajemnicze "
	  + "wzory, ktore wyszyto na grubym, welnianym materiale. Sa wsrod nich "
	  + "nie tylko demonologiczne pentagramy lecz rozne uklady pojedynczych "
	  + "konstelacji. Jest to naprawde czarodziejska szata.\n");
    set_slots(A_BODY, A_ARMS);
	add_prop(OBJ_I_VALUE, 10);
}


