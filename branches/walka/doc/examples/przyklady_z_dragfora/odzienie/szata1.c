/*
 * Filename: szata1.c
 *
 * Zastosowanie: Szaty dla kaplanow Lathandera.
 * 
 * Copyright Volothamp Geddarm.
 *
 */
inherit "/d/Faerun/std/stdubrania.c";

#include <stdproperties.h>
#include <wa_types.h>
#include <formulas.h>
#include <macros.h>

void
create_ubranie()
{
    ustaw_nazwe( ({"szaty", "szat", "szayom", "szaty", "szatami", "szatach"}),
({"szaty", "szat", "szatom", "szaty", "szatami","szatach"}),PL_ZENSKI);
    
	dodaj_przym("kaplanski", "kaplanscy");
    dodaj_przym("snieznobialy", "snieznobiali");
    set_long("Sa to dlugie szaty o kolorze swiezo spadnietego sniegu. Wykonano "
	  + "je z dosyc cienkiego materialu o niklych wlasciwosciach zatrzymywania ciepla. "
	  + "Mimo to ozdobiono je licznymi ornamentami, znakami i symbolami, ktore chociaz "
	  + "w najmniejszym stopniu wiaza sie z Panem Poranka badz jego kultem. Osoba, ktora "
 + "nosi to odzienie od razu wydaje sie byc bardziej postawna, charyzmatyczna, wyniosla "
 + "i bardziej przekonywujaca anizeli byla przed zalozeniem szat.\n");
set_slots(A_BODY, A_ARMS, A_LEGS);
    add_prop(OBJ_I_VOLUME, query_default_weight() / 6);
	add_prop(OBJ_I_VALUE, 10);
}


