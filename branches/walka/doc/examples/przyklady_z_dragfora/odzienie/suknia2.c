/*
 * Filename: suknia2.c
 *
 * Zastosowanie: Suknia dla mieszczanki.
 * 
 * Copyright Volothamp Geddarm.
 *
 */
inherit "/d/Faerun/std/stdubrania.c";

#include <stdproperties.h>
#include <wa_types.h>
#include <formulas.h>
#include <macros.h>

void
create_ubranie()
{
    ustaw_nazwe( ({"suknia", "sukni", "sukni", "suknie", "suknia", "sukni"}),
		         ({"suknie", "sukni", "sukniom", "suknie", "sukniami", "sukniach"}), PL_ZENSKI);
    
	dodaj_przym("dlugi", "dludzy");
    dodaj_przym("kolorowy", "kolorowi");
    set_long("Jest to dluga, siegajaca stop suknia, ktora wykonano z "
	  + "wyjatkowo trwalego lecz i kolorowego i barwnego materialu. Z reguly "
	  + "takie dodatki stosuje sie, aby ktokolwiek zwrocil na osobe uwage lecz "
	  + "mozesz z cala pewnoscia przyznac, ze nie jest to bynajmniej jakas "
	  + "kosztowna i droga suknia lecz dostepny u kazdego krawca ciuch.\n");
    set_slots(A_BODY, A_ARMS);
    add_prop(OBJ_I_VOLUME, query_default_weight() / 6);
	add_prop(OBJ_I_VALUE, 10);
}


