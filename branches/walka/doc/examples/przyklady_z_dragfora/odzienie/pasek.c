/*
 * Filename: pasek.c
 *
 * Zastosowanie: Pasek dla mieszczanina.
 * 
 * Copyright Volothamp Geddarm.
 *
 */
inherit "/d/Faerun/std/stdubrania.c";

#include <stdproperties.h>
#include <wa_types.h>
#include <formulas.h>
#include <macros.h>

void
create_ubranie()
{
    ustaw_nazwe( ({"pasek", "paska", "paskowi", "pasek", "paskiem", "pasku"}),
		         ({"paski", "paskow", "paskom", "paski", "paskami", "paskach"}), PL_MESKI_NOS_NZYW);
    dodaj_przym("waski", "wascy");
	dodaj_przym("skorzany", "skorzani");
    set_long("Jest to gustowny pasek zawierajacy wykonana z taniej miedzi klamre. "
	  + "Podobne pasy maja zwyczaj nosic rolnicy, ktorzy w ciezki dzien zas "
	  + "czesto zmierzaja z ranka na role, a wracaja zen dopiero pozno w nocy. "
	  + "Ma on wiele dziurek i z pewnoscia spelni wymagania muskularnego drwala "
	  + "jak i watlego celnika.\n");
	
	set_slots(A_WAIST);

    add_prop(OBJ_I_WEIGHT, 100);
	add_prop(OBJ_I_VALUE, 10);
}


