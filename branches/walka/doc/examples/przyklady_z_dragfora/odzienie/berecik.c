/*
 * Filename: berecik.c
 *
 * Zastosowanie: Berecik.
 * 
 * Copyright Volothamp Geddarm.
 *
 */
inherit "/d/Faerun/std/stdubrania.c";

#include <stdproperties.h>
#include <wa_types.h>
#include <formulas.h>
#include <macros.h>

void
create_ubranie()
{
    ustaw_nazwe( ({"berecik z antenka", "berecika z antenka", "berecikowi z antenka", "berecik z antenka", "berecikiem z antenka", "bereciku z antenka"}),
		         ({"bereciki z antenkami", "berecikow z antenkami", "berecikom z antenkami", "bereciki z antenkami", "berecikami z antenkami", "berecikach z antenkami"}), PL_MESKI_NOS_NZYW);
	
	dodaj_nazwy( ({"berecik", "berecika", "berecikowi", "berecik", "berecikiem", "bereciku"}),
		         ({"bereciki", "berecikow", "berecikom", "bereciki", "berecikami", "berecikach"}), PL_MESKI_NOS_NZYW);

    dodaj_przym("maly", "mali");

    set_long("Jest to nieduze nakrycie glowy w sam raz dla szanujacego sie niziolka. "
	  + "Ow filcowy berecik wyposazony jest w gustowna antenke, ktora zwraca uwage "
	  + "kazdego przechodznia na osobe, ktora ja nosi. Zatem trzeba przyznac, ze "
	  + "chociaz ten kapelusik nie okaze sie pomocny podczas ulewy to jednak posiada "
	  + "wszechstronne i uniwersalne zastosowanie.\n");
    
	set_slots(A_HEAD);

    add_prop(OBJ_I_WEIGHT, 500);
	add_prop(OBJ_I_VALUE, 10);
}


