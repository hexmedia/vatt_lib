/*
 * Filename: kozuch.c
 *
 * Zastosowanie: Kozucha nie widziales?
 * 
 * Copyright Volothamp Geddarm.
 *
 */
inherit "/d/Faerun/std/stdubrania.c";

#include <stdproperties.h>
#include <wa_types.h>
#include <formulas.h>
#include <macros.h>

void
create_ubranie()
{

    ustaw_nazwe( ({"kozuch", "kozucha", "kozuchowi", "kozuch", "kozuchem", "kozuchu"}),
		         ({"kozuchy", "kozuchow", "kozuchom", "kozuchy", "kozuchami", "kozuchach"}), PL_MESKI_NOS_NZYW);
    
	dodaj_przym("gruby","grubi");
	dodaj_przym("welniany", "welniani");
    
	set_long("Kiedy w gorach panuje sniezna zamiec, widocznosc staje sie coraz bardziej "
       + "ograniczona, a grad ciska w ciebie pelna moca... Wtedy doswiadczony podroznik "
       + "ma zawsze pod reka taki kozuszek. Zakladajac go zapewnisz sobie nie tylko "
       + "cieplo ale i znakomita ochrone przed deszczem, czy ostatecznie gradem. " 
       + "Na jego podszewce dostrzegasz niewielki znaczek pracowni krawieckiej "
       + "'Krolewski Gryf'.\n");
    
	set_slots(A_BODY,A_ARMS);

    add_prop(OBJ_I_WEIGHT, 50);
	add_prop(OBJ_I_VALUE, 400);
}


