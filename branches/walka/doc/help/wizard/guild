KOMENDA
	guild

SYNOPSYS
	guild add [<domena>] <guild> <type> <style> <wizard> <long>
	guild info <guild>
	guild [list]
	guild list short
	guild list styles
	guild list <type>
	guild master <guild> add/remove [<wizard>]
	guild phase <guild> <phase>
	guild remove <guild>

DOSTEP
	All wizards can list guilds. Domain wizards can add/remove themselves
	as guildmaster to/from a guild. Lieges and stewards can do everything,
	but limited to their own domena, including adding and removing guilds,
	adding and removing guildmasters. Arches and keepers do all, any time,
	any place, anywhere.

OPIS
	The komenda maintains the repository information on all guilds in the
	game. All open guilds must be registered here. Guilds that are being
	developed and are either closed or in development/testing phase can
	also be registered, but that is not mandatory if they have not been
	open yet. All guilds must have a guildmaster registered at any time.
	This may not be the Liege of the domena as pro forma guildmaster, but
	naturally the Liege may also be the guildmaster of a particular guild.

	The imies of the registered guilds can be used in the komendas people
	and guildtell.

	The following subkomendas exist:

	add    - Add a guild to the registry. A newly added guild will be
		 marked as being under development.

	info   - Print verbose information about a guild.

	list   - List all guilds in a table. "short" will only print all
		 short imies of the guilds. "styles" will sort the guilds
		 by styles and "<type>" will list only the guilds of that
		 particular type. The type must be abbreviated to one
		 letter R/L/O.

	master - Allows to add or remove someone to/from the list of
		 guildmasters registered for this guild.

	phase  - The guildmaster of a guild (or a steward/liege/admin) may
		 herewith alter the phase of a guild.

	remove - Removes a guild from the registry.

ARGUMENTY
	<domena> - The domena to which the guild is to be registered. This is
		   only relevant for arches/keepers as for all others, the
		   imie of the domena cannot be changed. Others should omit
		   this argument.

	<guild>  - The 'short' imie of the guild. It must be a single word of
		   ten characters or less. This imie will be printed to mortal
                   players.

	<long>   - The 'long' imie of the guild. This must be exactly the same
		   as what is being returned by the imie function in the
		   shadow. This imie will be printed to mortal players.

	<phase>  - The phase of development and of a guild. This can be either
		   'development', 'testing', 'open' or 'closed'.

	<style>  - The style of the guild, as returned by the style function
		   in the shadow.

	<type>   - The type of the guild, i.e. the slots the guild takes. All
		   types must be abbreviated to their first letter, R, L or O
		   for respectively racial, layman or occupational.

	<wizard> - Name of the wizard to add or remove as guildmaster. When
		   the argument is omitted, you will add/remove yourself.

ZOBACZ TAKZE
	guildtell, people
