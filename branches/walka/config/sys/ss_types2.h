/*
 * ss_types2.h
 *
 * This file defines the available stats and skills that are locally
 * configured. The only use of this file is that it should be included
 * by /sys/ss_types.h.
 */

#include <tasks.h>

/*Define this if you want skills to be limited by stats*/
#define STAT_LIMITED_SKILLS

/*
 * Limity dla poszczegolnych umiejetnosci sa zdefiniowane w mappingu
 * SS_SKILL_DESC.
 */

/* List of skills as is going to be used */
#define SS_WEP_FIRST               10   /* The first weapon index */

#define SS_WEP_SWORD            SS_WEP_FIRST + 0  /* W_SWORD */
#define SS_WEP_POLEARM          SS_WEP_FIRST + 1  /* W_POLEARM */
#define SS_WEP_AXE              SS_WEP_FIRST + 2  /* W_AXE */
#define SS_WEP_KNIFE            SS_WEP_FIRST + 3  /* W_KNIFE */
#define SS_WEP_CLUB             SS_WEP_FIRST + 4  /* W_CLUB */
#define SS_WEP_WARHAMMER        SS_WEP_FIRST + 5  /* W_WARHAMMER */
#define SS_WEP_MISSILE          SS_WEP_FIRST + 6  /* W_MISSILE */
#define SS_WEP_BOW              SS_WEP_FIRST + 7  /* W_L_BOW */
#define SS_WEP_CROSSBOW         SS_WEP_FIRST + 8  /* W_L_CROSSBOW and W_L_H_CROSSBOW */
#define SS_WEP_SLING            SS_WEP_FIRST + 9  /* W_L_SLING */


/*
 * Mapping jest indeksowany numerami poszczegolnych umiejetnosci. Jego pola
 * to odpowiednio: nazwa umiejetnosci, nazwa w bierniku (do komendy 'trenuj'),
 * dlugi opis w miejscowniku, wspolczynnik kosztu (0-100), limity z cech i
 * umiejetnosci, maksymalny poziom dostepny poza gildiami. Mapping limitow
 * jest indeksowany numerami cech i umiejetnosci, zgodnie z definicjami z
 * <tasks.h> i <ss_types.h>. Jego pola to odpowiednio: poziom limitujacej
 * cechy/umiejetnosci koniecznej do rozpoczecia szkolenia limitowanej
 * umiejetnosci (0) i poziom konieczny do wyszkolenia sie do maksymalnego
 * dostepnego poziomu (czyli 100). Jesli zamiast tablicy poda sie tylko ta
 * druga liczbe, przyjmuje sie, ze pierwsza wynosi 0.
 */

#define SS_SKILL_DESC ([ \
/* Bieglosci w poszczegolnych rodzajach broni */                                    \
    SS_WEP_SWORD:                                                                   \
        ({"miecze", "miecze", "walce mieczem",                                      \
          100, ([TS_DEX:60, TS_INT:30]), 30}),                                      \
    SS_WEP_POLEARM:                                                                 \
        ({"bronie drzewcowe", "bronie drzewcowe", "walce broni� drzewcowa",         \
          80, ([TS_STR:45]), 30}),                                                  \
    SS_WEP_AXE:                                                                     \
        ({"topory", "topory", "walce toporem", 70, ([TS_STR:45]), 30}),             \
    SS_WEP_KNIFE:                                                                   \
        ({"sztylety", "sztylety", "walce sztyletem", 46, ([TS_DEX:45, TS_INT:30]),  \
            30}),                                                                   \
    SS_WEP_CLUB:                                                                    \
        ({"maczugi", "maczugi", "walce maczug�", 50, ([TS_STR:60]), 30}),           \
    SS_WEP_WARHAMMER:                                                               \
        ({"m�oty", "m�oty", "walce m�otem", 80, ([TS_STR:60]), 30}),                \
    SS_WEP_MISSILE:                                                                 \
        ({"bronie strzeleckie", "bronie strzeleckie",                               \
            "strzelaniu wszelkiego rodzaju bro�mi strzeleckimi",                     \
            70, ([TS_DEX:45, TS_INT:30]), 30}),                                     \
    SS_WEP_BOW:                                                                     \
        ({"�ucznictwo", "�ucznictwo", "strzelaniu z �uku", 70,                      \
            ([TS_DEX:30, TS_STR:30, TS_INT:30, SS_WEP_MISSILE:50]), 30}),           \
    SS_WEP_CROSSBOW:                                                                \
        ({"kusznictwo", "kusznictwo", "strzelaniu kusz�", 70,                       \
            ([TS_DEX:40, TS_STR:20, TS_INT:30, SS_WEP_MISSILE:50]), 30}),           \
    SS_WEP_SLING:                                                                   \
        ({"bronie miotane", "bronie miotane", "strzelaniu bro�mi miotanymi", 70,    \
            ([TS_DEX:45, TS_STR:10, TS_INT:30, SS_WEP_MISSILE:10]), 30}),           \
/* Ogolne umiejetnosci bojowe */					\
    SS_2H_COMBAT:							\
        ({"walka dwiema bro�mi", "walk� dwiema bro�mi", "walce dwiema bro�mi jednocze�nie",\
          100, ([TS_DEX:60, TS_INT:30]), 20}),				\
    SS_UNARM_COMBAT:							\
        ({"walka bez broni", "walk� bez broni", "walce bez broni",	\
          90, ([TS_STR:45, TS_DEX:45, TS_WIS:30, TS_DIS:30]), 20}),	\
    SS_BLIND_COMBAT:							\
        ({"walka w ciemno�ci", "walk� w ciemno�ci", "walce w ciemno�ciach",\
          95, ([TS_INT:30, TS_WIS:30, TS_DIS:30, SS_AWARENESS:50]), 20}),\
    SS_PARRY:								\
        ({"parowanie", "parowanie", "parowaniu cios�w przeciwnika",	\
          80, ([TS_STR:50, TS_DEX:35, TS_INT:30]), 20}),		\
    SS_SHIELD_PARRY:							\
        ({"tarczownictwo", "tarczownictwo", "skutecznym u�ywaniu tarczy",\
          80, ([TS_STR:45, TS_DEX:30, TS_INT:30]), 20}),		\
    SS_DEFENCE:								\
        ({"uniki", "uniki", "unikaniu cios�w przeciwnika",		\
          80, ([TS_DEX:60, TS_INT:30]), 20}),				\
    SS_THROWING:								\
        ({"rzucanie", "rzucanie", "rzucaniu wszelkiego typu przedmiotami",\
          80, ([TS_DEX:70, TS_INT:30, SS_AWARENESS:90]), 100}),				\
    SS_MOUNTED_COMBAT:							\
        ({"walka konna", "walk� konn�", "walce konnej",	\
          100, ([TS_STR:45, TS_DEX:60, TS_WIS:30, SS_ANI_HANDL:50, SS_RIDING:50]), 0}),\
/* Umiejetnosci magiczne */						\
    SS_SPELLCRAFT:							\
        ({"rzucanie czar�w", "rzucanie czar�w", "u�ywaniu magii",	\
          70, ([TS_INT:60, TS_WIS:45, SS_HERBALISM:20]), 20}),		\
    SS_HERBALISM:							\
        ({"zielarstwo", "zielarstwo", "znajdowaniu i rozpoznawaniu zi�",\
          70, ([TS_INT:45, TS_WIS:45]), 20}),				\
    SS_ALCHEMY:								\
        ({"alchemia", "alchemi�", "warzeniu i rozpoznawaniu mikstur",	\
          70, ([TS_INT:45, TS_WIS:60, SS_HERBALISM:40]), 20}),		\
/* Do zmiany, badz usuniecia...						\
    SS_FORM_TRANSMUTATION:({ "transmutation spells", 90, SS_INT, 110, 0 }),\
    SS_FORM_ILLUSION:    ({ "illusion spells",    70, SS_INT, 110, 0 }), \
    SS_FORM_DIVINATION:  ({ "divination spells",  70, SS_INT, 110, 0 }), \
    SS_FORM_ENCHANTMENT: ({ "enchantment spells", 80, SS_INT, 110, 0 }), \
    SS_FORM_CONJURATION: ({ "conjuration spells", 80, SS_INT, 110, 0 }), \
    SS_FORM_ABJURATION:  ({ "abjuration spells",  70, SS_INT, 110, 0 }), \
    SS_ELEMENT_FIRE:     ({ "fire spells",        70, SS_WIS, 110, 0 }), \
    SS_ELEMENT_AIR:      ({ "air spells",         70, SS_WIS, 110, 0 }), \
    SS_ELEMENT_EARTH:    ({ "earth spells",       70, SS_WIS, 110, 0 }), \
    SS_ELEMENT_WATER:    ({ "water spells",       70, SS_WIS, 110, 0 }), \
    SS_ELEMENT_LIFE:     ({ "life spells",        80, SS_WIS, 110, 0 }), \
    SS_ELEMENT_DEATH:    ({ "death spells",       90, SS_WIS, 110, 0 }), \
 */									\
/* Umiejetnosci zlodziejskie */						\
    SS_OPEN_LOCK:							\
        ({"otwieranie zamk�w", "otwieranie zamk�w", "otwieraniu zamk�w bez w�a�ciwego klucza",\
          70, ([TS_DEX:45, TS_INT:30]), 20}),				\
    SS_PICK_POCKET:							\
        ({"kieszonkostwo", "kieszonkostwo", "opiekowaniu si� rzeczami nale��cymi do kogo� innego",\
          70, ([TS_DEX:60, TS_DIS:30]), 20}),				\
    SS_ACROBAT:								\
        ({"akrobatyka", "akrobatyk�", "akrobatyce",			\
          70, ([TS_DEX:60]), 20}),					\
    SS_FR_TRAP:								\
        ({"wykrywanie pu�apek", "wykrywanie pu�apek", "wykrywaniu pu�apek",\
          70, ([TS_DEX:45, TS_INT:30, TS_WIS:45]), 30}),		\
    SS_SNEAK:								\
        ({"skradanie si�", "skradanie si�", "przemykaniu si� ukradkiem",\
          70, ([TS_DEX:60]), 30}),					\
    SS_HIDE:								\
        ({"ukrywanie si�", "ukrywanie si�", "ukrywaniu siebie i przedmiot�w",\
          70, ([TS_DEX:45]), 30}),					\
/* Umiejetnosci ogolnego przeznaczenia */				\
    SS_APPR_MON:							\
        ({"ocena przeciwnika", "ocen� przeciwnika", "ocenianiu cech i stan�w os�b",\
          50, ([TS_WIS:45]), 30}),					\
    SS_APPR_OBJ:							\
        ({"ocena obiektu", "ocen� obiektu", "ocenianiu w�asno�ci przedmiot�w",\
          50, ([TS_WIS:45]), 30}),					\
    SS_APPR_VAL:							\
        ({"szacowanie", "szacowanie", "szacowaniu warto�ci przedmiot�w",\
          50, ([TS_WIS:45]), 30}),					\
    SS_SWIM:								\
        ({"p�ywanie", "p�ywanie", "p�ywaniu i nurkowaniu",		\
          50, ([TS_STR:30, TS_DEX:30, TS_CON:45]), 50}),		\
    SS_CLIMB:								\
        ({"wspinaczka", "wspinaczk�", "wspinaniu si�",			\
          50, ([TS_STR:45, TS_DEX:45, TS_CON:30, TS_DIS:30]), 50}),	\
    SS_ANI_HANDL:							\
        ({"opieka nad zwierz�tami", "opiek� nad zwierz�tami", "opiekowaniu si� zwierz�tami",\
          50, ([TS_WIS:45]), 30}),					\
    SS_LOC_SENSE:							\
        ({"wyczucie kierunku", "wyczucie kierunku", "rozpoznawaniu kierunk�w i znajdowaniu �cie�ek",\
          50, ([TS_INT:30, TS_WIS:60]), 30}),				\
    SS_TRACKING:							\
        ({"tropienie", "tropienie", "znajdowaniu i rozpoznawaniu �lad�w",\
          50, ([TS_INT:30, TS_WIS:60]), 30}),				\
    SS_HUNTING:								\
        ({"�owiectwo", "�owiectwo", "�owieniu dzikich zwierz�t",	\
          50, ([TS_INT:30, TS_WIS:30, SS_TRACKING:50, SS_AWARENESS:50]), 30}),\
    SS_LANGUAGE:							\
        ({"znajomo�� j�zyk�w", "znajomo�� j�zyk�w", "identyfikacji staro�ytnych oraz wsp�czesnych j�zyk�w i pism",\
          50, ([TS_INT:60, TS_WIS:60]), 40}),				\
    SS_AWARENESS:							\
        ({"spostrzegawczo��", "spostrzegawczo��", "zauwa�aniu tego, co ukryte",\
          50, ([TS_INT:30, TS_WIS:45]), 50}),				\
    SS_TRADING:								\
        ({"targowanie si�", "targowanie si�", "zawieraniu korzystnych transakcji handlowych",\
          50, ([TS_INT:45]), 30}),					\
    SS_RIDING:								\
        ({"je�dziectwo", "je�dziectwo", "je�dzie konnej",		\
          75, ([TS_DEX:60, TS_WIS:45]), 0}),				\
    SS_MUSIC:								\
        ({"muzykalno��", "muzykalno��", "�piewie i pos�ugiwaniu si� instrumentami",			\
          75, ([TS_DEX:60, TS_WIS:45]), 0}),				\
    SS_WOODCUTTING:							\
	  ({"drwalstwo", "drwalstwo", "sztuce �cinania i obr�bki drzewa", \
	  50, ([TS_STR:40, TS_CON:30]), 0}), \
    SS_FISHING:								\
	({"w�dkarstwo", "w�dkarstwo", "po�awianiu ryb",		\
	50, ([TS_DEX:40]), 0}),						\
])
