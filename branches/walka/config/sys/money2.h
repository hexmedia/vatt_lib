/**
 * \file /config/sys/money2.h
 *
 * Ten plik zawiera definicje og�lnomudowej waluty i jest on
 * include'owany przez /sys/money.h.
 */
/* Komentarze przet�umaczone/dopisane przez Moldera. */

/**  tablica nazw typ�w monet. */
#define MONEY_TYPES  ({ "grosze", "denary", "korony" })
/** tablica warto�ci monet w stosunku do najmniejszej jednostki. */
#define MONEY_VALUES ({ 1      ,  20    , 240 })
/** tablica wag pojedynczych monet danego typu. */
#define MONEY_WEIGHT ({ 5      ,  14    , 7 })
/** tablica obj�to�� pojedynczych monet danego typu.  */
#define MONEY_VOLUME ({ 1      ,  2     , 1 })

#define MONEY_NAMES ({ \
	({ ({"grosz","grosza","groszowi","grosz","groszem","groszu"}), ({"grosze","groszy","groszom","grosze","groszami","groszach"}), PL_MESKI_NOS_NZYW }), \
	({ ({"denar","denara","denarowi","denara","denarem","denarze"}), ({"denary","denar�w","denarom","denary","denarami","denarach"}), PL_MESKI_NOS_NZYW }), \
	({ ({"korona","korony","koronie","koron�","koron�","koronie"}), ({"korony","koron","koronom","korony","koronami","koronach"}), PL_ZENSKI }) \
    })

//#define MONEY_ODM ({	({ "", "" }),		\
//			({ "", "" }),		\
//		  })
		  
#define MONEY_TEMATY ({	"grosz",	\
			"denar",	\
			"koron",	\
		     })

/**
 * Makra MONEY_MAKE_G, MONEY_MAKE_D, MONEY_MAKE_K zwracaj� wska�nik 
 * do nowoutworzonego stosu monet (odpowiednio: groszy, dukat�w, koron).
 * @param num - ilo�� monet w stosie
 */
#define MONEY_MAKE_D(num)    MONEY_MAKE(num, "denary")
/** opis przy MONEY_MAKE_D(num) */
#define MONEY_MAKE_G(num)    MONEY_MAKE(num, "grosze")
/** opis przy MONEY_MAKE_D(num) */
#define MONEY_MAKE_K(num)    MONEY_MAKE(num, "korony")

/**
 * Makra: MONEY_MOVE_G, MONEY_MOVE_D, MONEY_MOVE_K
 * przenosz� odpowiednio: grosze, denary, korony z obiektu 'from' do obiektu 'to'.
 * @param num - ilo�� monet
 * @param ctype - typ monet  
 * @arg Podaj argument from == 0 aby utworzy� nowe monety.
 * 
 * @return          -1      gdy obiekt 'from' nie ma (wystarczaj�co) monet danego typu
 * @return          0       gdy monety pomy�lnie przemieszczono
 * @return         >1     gdy wyst�pi� b��d przy przenoszeniu (move) monet (wygenerowany przez /std/object)
 */
#define MONEY_MOVE_D(num, from, to) MONEY_MOVE(num, "denary", from, to)
/** opis przy MONEY_MOVE_G(num) */
#define MONEY_MOVE_G(num, from, to) MONEY_MOVE(num, "grosze", from, to)
/** opis przy MONEY_MOVE_G(num) */
#define MONEY_MOVE_K(num, from, to) MONEY_MOVE(num, "korony", from, to)

#define MONEY_LOG_LIMIT ([ "grosze" : 900, "denary" : 50, "korony" : 2 ])
#define MONEY_LOG_SIZE 400000
