/*
 * /cmd/wiz/junior_shadow.c
 *
 * This shadow redefines the function do_die() in a junior player to a
 * wizard. It prevents them from death if they so choose.
 *
 * There is some protection in the shadow for we do not want ordinary
 * mortal players to get this protection. I bet they would want to have it!
 *
 * /Mercade, 6 February 1994
 */

#pragma no_inherit
#pragma strict_types
#pragma save_binary

inherit "/std/shadow";

#include <macros.h>
#include <std.h>

/*
 * Nazwa funkcji: remove_death_protection
 * Opis         : Remove this shadow from a player.
 * Zwraca       : 1 - always
 */
nomask public int
remove_death_protection()
{
    tell_object(query_shadow_who(), "Ju^z nie jeste^s chronion" +
        query_shadow_who()->koncowka("y", "a") + " przed ^smierci^a. " +
        "Powodzenia! ^Zycie to ci^e^zki kawa^lek chleba..\n");

    destruct();
    return 1;
}

/*
 * Nazwa funkcji: illegal_shadow_use
 * Opis         : If you are not allowed to use this shadow, this function
 *                will take care of it.
 */
nomask private void
illegal_shadow_use(object player)
{
    SECURITY->log_syslog("ILLEGAL_JUNIOR_SHADOW",
        ctime(time()) + ", " + player->query_real_name() + 
        "<-\tshadow: uid[" + getuid() + "], euid[" + geteuid() + "].\n");
}

/*
 * Nazwa funkcji: do_die
 * Opis         : This function is called whenever someone suspects that
 *                we have died.
 * Argumenty    : killer - the object that killed us.
 */
public void
do_die(object killer)
{
    string  name;
    object *enemies;

    if ((query_shadow_who()->query_hp() > 0) ||
	(query_shadow_who()->query_ghost()))
    {
	return;
    }

    if (!objectp(killer))
    {
	killer = previous_object();
    }

    if (sscanf((string)query_shadow_who()->query_real_name(),
	"%sjr", name) != 1)
    {
	illegal_shadow_use(query_shadow_who());
	set_alarm(0.1, 0.0, "remove_death_protection");
	query_shadow_who()->do_die(killer);
	return;
    }

    if (SECURITY->query_wiz_rank(name) <= WIZ_RETIRED)
    {
	illegal_shadow_use(query_shadow_who());
	set_alarm(0.1, 0.0, "remove_death_protection");
	query_shadow_who()->do_die(killer);
	return;
    }

    enemies = (object *)query_shadow_who()->query_enemy(-1) - ({ 0 });

    if (sizeof(enemies))
    {
	query_shadow_who()->stop_fight(enemies);
	enemies->stop_fight(query_shadow_who());
    }

    /* Give him/her at least one hitpoint so this function won't be called
     * over and over again.
     */
    query_shadow_who()->set_hp(1);

    tell_roombb(environment(query_shadow_who()),
	"\n" + QCIMIE(query_shadow_who(), PL_MIA) + " umiera ....\n" +
	" .... zaraz ...  " + query_shadow_who()->query_zaimek(PL_MIA) +
	" ca^ly czas ^zyje!\n" +
	"Pomimo, i^z nie ma ju^z w " + query_shadow_who()->query_zaimek(PL_MIE) +
	" si^l ^zyciowych, Bogowie nie chc^a przyj^a^c " +
	query_shadow_who()->query_zaimek(PL_DOP, 1) + " duszy do siebie.\n",
	({query_shadow_who()}), query_shadow_who());
    tell_object(killer, "Zabi^l" + killer->koncowka("e^s", "a^s") + " " +
	query_shadow_who()->query_imie(killer, PL_BIE) + ".\n");
    tell_object(query_shadow_who(), "\nUMAR^L" + 
        query_shadow_who()->koncowka("E^S", "A^S") + " !!!\n\n" +
	"Junior tool jednak^ze ochroni^l ci^e przed ^smierci^a.\n" +
	"Przestajesz walczy^c ze wszystkimi dotychczasowymi " + 
	"przeciwnikami.\n\n");
}

/*
 * Nazwa funkcji: query_death_protection
 * Opis         : This function will return whether the player is protected
 *                from Death.
 * Zwraca       : 1 (always)
 */
nomask public int
query_death_protection()
{
    return 1;
}

/*
 * Nazwa funkcji: shadow_me
 * Opis         : This function is called to make this shadow shadow the
 *                player. It add the autoloading feature to the player.
 * Argumenty    : player - the player
 * Zwraca       : 1 - everything went right
 *                0 - no player or player already shadowed
 */
nomask public int
shadow_me(object player)
{
    string name;

    if (sscanf((string)player->query_real_name(), "%sjr", name) != 1)
    {
	tell_object(player,
	    "ACK! Nigdy nie powin" + player->koncowka("iene^s by^l", "na^s by^la") +
	        " dosta^c tego shadowa!\n");
	
	illegal_shadow_use(player);
	return 0;
    }

    if (SECURITY->query_wiz_rank(name) <= WIZ_RETIRED)
    {
	tell_object(player,
	    "ACK! Nigdy nie powin" + player->koncowka("iene^s by^l", "na^s by^la") +
	        " dosta^c tego shadowa!\n");
	illegal_shadow_use(player);
	return 0;
    }

    if (!::shadow_me(player))
    {
	tell_object(player,
	    "Co^s jest bardzo ^xle z junior shadowem. Nie chce " + 
	        "zashadowowa^c...\n");
	return 0;
    }

    tell_object(player, "Od teraz jeste^s chronion" + 
        player->koncowka("y", "a") + " przed ^Smierci^a!\n");
    return 1;
}
