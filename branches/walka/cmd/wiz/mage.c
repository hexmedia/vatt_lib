/*
 * /cmd/wiz/mage.c
 *
 * This object holds the mage wizards commands.
 * The following commands are supported:
 *
 * - echo
 * - echoto
 * - force
 * - money
 * - snort
 */

#pragma no_clone
#pragma no_inherit
#pragma save_binary
#pragma strict_types

inherit "/cmd/std/command_driver";

#include <macros.h>
#include <money.h>
#include <std.h>
#include <options.h>

#define CHECK_SO_MAGE 	if (WIZ_CHECK < WIZ_MAGE) return 0; \
			if (this_interactive() != this_player()) return 0

/* **************************************************************************
 * Return a list of which souls are to be loaded.
 * The souls are listed in order of command search.
 */
nomask string *
get_soul_list()
{
    return ({ WIZ_CMD_MAGE,
              WIZ_CMD_HELPER,
              WIZ_CMD_NORMAL,
              WIZ_CMD_APPRENTICE,
              MBS_SOUL });
}

/* **************************************************************************
 * Return a proper name of the soul in order to get a nice printout.
 */
nomask string
get_soul_id()
{
    return WIZNAME_MAGE;
}

/* **************************************************************************
 * The list of verbs and functions. Please add new in alfabetical order.
 */
nomask mapping
query_cmdlist()
{
    return ([
             "echo"		: "echo",
             "echoto"		: "echo_to",
             "force"		: "force",
             "money"		: "money",
	     "snort"		: "snort"
	     ]);
}

/* **************************************************************************
 * Here follows the actual functions. Please add new functions in the 
 * same order as in the function name list.
 * **************************************************************************/


/* **************************************************************************
 * echo - echo something in a room
 */
nomask int
echo(string str)
{
    CHECK_SO_MAGE;

    if (!stringp(str))
    {
        notify_fail("Echo co?\n");
        return 0;
    }

    say(str + "\n");

#ifdef LOG_ECHO
    if (SECURITY->query_wiz_rank(this_player()->query_real_name()) < WIZ_ARCH)
    {
        SECURITY->log_syslog(LOG_ECHO, sprintf("%s %-11s: %s -> (%s)\n",
            ctime(time()), capitalize(this_interactive()->query_real_name()),
            str,
            implode((FILTER_LIVE(all_inventory(environment(this_player())) -
            ({ this_player() })))->query_real_name(), ", ")));
    }
#endif LOG_ECHO

    if (this_player()->query_option(OPT_ECHO))
    {
        write("Tworzysz echo: " + str + "\n");
    }
    else
    {
        write("Ju�.\n");
    }
    return 1;
}

/* **************************************************************************
 * echoto - echo something to someone
 */
nomask int
echo_to(string str)
{
    object ob;
    string who, msg;

    CHECK_SO_MAGE;

    if (!stringp(str) ||
        (sscanf(str, "%s %s", who, msg) != 2))
    {
        notify_fail("Sk^ladnia: echoto <kto> <co>\n");
        return 0;
    }

    ob = find_player(lower_case(who));
    if (!objectp(ob))
    {
        notify_fail("Nie ma w grze takiego gracza.\n");
        return 0;
    }

#ifdef LOG_ECHO
    if (SECURITY->query_wiz_rank(this_player()->query_real_name()) < WIZ_ARCH)
    {
        SECURITY->log_syslog(LOG_ECHO, sprintf("%s %-11s to %-11s: %s\n",
            ctime(time()), capitalize(this_interactive()->query_real_name()),
            capitalize(who), msg));
    }
#endif LOG_ECHO

    tell_object(ob, msg + "\n");
    if (this_player()->query_option(OPT_ECHO))
    {
        write("Tworzysz echo do " + capitalize(who) + ": " + msg + "\n");
    }
    else
    {
        write("Ju�.\n");
    }

    return 1;
}


/* **************************************************************************
 * force - force a player to do something
 */
nomask int
force(string str)
{
    string who, what;
    object ob;

    CHECK_SO_MAGE;

    if (!stringp(str))
    {
        notify_fail("Zmu^s kogo do czego?\n");
        return 0;
    }

    if (sscanf(str, "%s to %s", who, what) != 2 &&
        sscanf(str, "%s %s", who, what) != 2)
    {
        notify_fail("Zmu^s kogo do zrobienia czego?\n");
        return 0;
    }

    ob = find_living(who);
    if (!objectp(ob))
    {
        notify_fail("Nie ma w grze gracza o tym imieniu.\n");
        return 0;
    }

#ifdef LOG_FORCE
    if (SECURITY->query_wiz_rank(this_player()->query_real_name()) < WIZ_ARCH)
    {
        SECURITY->log_syslog(LOG_FORCE, sprintf("%s %-11s to %-11s: %s\n",
            ctime(time), capitalize(this_interactive()->query_real_name()),
            capitalize(who), what));
    }
#endif LOG_FORCE

    if (ob->command(what))
    {
        tell_object(ob, this_interactive()->query_The_name(ob) +
                    " zmusza ci^e do wykonania: " + what + "\n");
        write("Ju�.\n");
    }
    else
    {
        tell_object(ob, this_interactive()->query_The_name(ob) +
                    " pr^obowa^l" + this_interactive()->koncowka("", "a") +
                    " ci^e zmusi^c do wykonania: " + what + "\n");
        write("Nie uda^lo ci si^e.\n");
    }
    return 1;
}

/* **************************************************************************
 * money - display, clone or destruct money
 */
nomask int
money(string str)
{
    object coin;
    int *coins;
    int *new_coins;
    int index;
    int size;

    CHECK_SO_MAGE;

    if (stringp(str))
    {
        new_coins = allocate(4);
        if ((size = sscanf(str, "%d %d %d %d", new_coins[0], new_coins[1],
            new_coins[2], new_coins[3])) == 0)
        {
            notify_fail("Sk^ladnia: money <grosze> <denary> <korony>\n");
            return 0;
        }

        if (size > sizeof(MONEY_TYPES))
        {
            notify_fail("Jest tylko " + sizeof(MONEY_TYPES) +
                " typy monet.\n");
            return 0;
        }

        if (SECURITY->query_wiz_rank(geteuid(this_interactive())) < WIZ_MAGE)
        {
            notify_fail("Nie jeste� do tego upowa�nion"+TP->koncowka("y","a")+
	                ".\n");
            return 0;
        }


        index = -1;
        while(++index < size)
        {
            /* If the wizard already has such coins, change the coins, else
             * we only clone new if that is necessary.
             */
            if (objectp(coin = present(("_" + MONEY_TYPES[index] +
                " moneta_"), this_player())))
            {
                /* If we indeed need to keep these coins, change the number
                 * or else remove them.
                 */
                if (new_coins[index])
                {
                    coin->set_heap_size(new_coins[index]);
                }
                else
                {
                    coin->remove_object();
                }
            }
            else
            {
                if (new_coins[index])
                {
                    MONEY_MAKE(new_coins[index],
                        MONEY_TYPES[index])->move(this_player());
                }
            }
        }
    }

    coins = MONEY_COINS(this_player());
    write("Masz przy sobie:\n");
    index = -1;
    size = sizeof(MONEY_TYPES);
    while(++index < size)
    {
        write(sprintf("%-11s: %6d\n", capitalize(MONEY_TYPES[index]),
            coins[index]));
    }

    write(sprintf("%11s: %6d\n", "^L^aczna warto^s^c", MONEY_MERGE(coins)));
    return 1;
}


/* **************************************************************************
 * snort - snort at peasants
 */
nomask int
snort(string name)
{
    object *snortees;

    CHECK_SO_MAGE;

    if (WIZ_CHECK <= WIZ_MAGE)
    {
	notify_fail("No one can snort like a Mage. " +
		    "You would only look ridiculous if you tried.\n");
	return 0;
    }

    if (!stringp(name))
    {
	notify_fail("Snort at who?\n");
	return 0;
    }

    snortees = parse_this(name, "[the] %l");
    if (!sizeof(snortees))
    {
	notify_fail("Snort at who?\n");
	return 0;
    }

    if (sizeof(snortees) > 1)
    {
	notify_fail("Snorting takes a lot of concentration. You can only " +
	    "snort at one person at a time.\n");
	return 0;
    }

    if (SECURITY->query_wiz_rank(snortees[0]->query_real_name()) < WIZ_MAGE)
    {
	say(QCTNAME(this_player()) + " snorts disdainfully at " +
	    QTNAME(snortees[0]) + ".\n", ({ this_player(), snortees[0] }) );
	tell_object(snortees[0], this_player()->query_The_name(snortees[0]) +
	    " snorts disdainfully at you.\n");
	write("You snort disdainfully at " +
	    snortees[0]->query_the_name(this_player()) + ".\n");
    }
    else
    {
	write(snortees[0]->query_The_name(this_player()) +
	    " looks quite respectable and does not deserve that kind of " +
	    "treatment.\n");
    }

    return 1;
}
