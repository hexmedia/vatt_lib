/*
 * /cmd/wiz/apprentice.c
 *
 * This object holds the apprentice wizards commands. The soul has been split
 * over a few modules. Hence, the list of commands below only reflects the
 * commands actually coded in this module:
 * 
 * - allcmd
 * - altitle
 * - applications
 * - apply
 * - bit
 * - domainsanction (short: dsanction)
 * - finger
 * - gd
 * - gfinger
 * - goto
 * - graph
 * - gtell
 * - guild
 * - gwiz
 * - gwize
 * - home
 * - last
 * - localcmd
 * - metflag
 * - mudlist
 * - newhooks
 * - notify
 * - odmimie
 * - ranking
 * - regret
 * - review
 * - rsupport
 * - rwho
 * - rwiz
 * - rwize
 * - sanction
 * - second
 * - setmin
 * - setmmin
 * - setmmout
 * - setmout
 * - start
 * - title
 * - unmetflag
 * - xmltags
 * - wizopt
 * - whereis
 * - whichsoul
 */

#pragma no_clone
#pragma no_inherit
#pragma strict_types
#pragma save_binary

inherit "/cmd/std/tracer_tool_base.c";

#include <cmdparse.h>
#include <composite.h>
#include <filter_funs.h>
#include <formulas.h>
#include <language.h>
#include <mail.h>
#include <macros.h>
#include <ss_types.h>
#include <std.h>
#include <stdproperties.h>
#include <time.h>
#include <options.h>
#include <pl.h>
#include <living_desc.h>
#include <colors.h>
#include <files.h>

#define CHECK_SO_WIZ 	if (WIZ_CHECK < WIZ_APPRENTICE) return 0; \
			if (this_interactive() != this_player()) return 0
#define ALLCMD_LIST_SOULS(name, souls, screen) \
            (name + break_string(implode((souls)->get_soul_id(), ", "), \
                                 screen, 10)[10..] + "\n")
#define WIZARD_S_GOTO_SET "_wizard_s_goto_set"

#include "/cmd/wiz/apprentice/communication.c"
#include "/cmd/wiz/apprentice/files.c"
#include "/cmd/wiz/apprentice/manual.c"
#include "/cmd/wiz/apprentice/people.c"
#include "/cmd/wiz/apprentice/slownik.c"

/* **************************************************************************
 * At creation, the save-file of the soul is restored.
 */
nomask void
create()
{
    setuid();
    seteuid(getuid());

    // nie wiadomo po co to -- jeremian
//    restore_object(WIZ_CMD_APPRENTICE);

    init_line();
}

/* **************************************************************************
 * Return a list of which souls are to be loaded.
 * The souls are listed in order of command search.
 */
nomask string *
get_soul_list()
{
    return ({ WIZ_CMD_APPRENTICE,
	      MBS_SOUL });
}

/* **************************************************************************
 * Return a proper name of the soul in order to get a nice printout.
 */
nomask string
get_soul_id()
{
    return WIZNAME_APPRENTICE;
}

/* **************************************************************************
 * The list of verbs and functions. Please add new in alfabetical order.
 */
nomask mapping
query_cmdlist()
{
    return ([
                "allcmd"        : "allcmd",
#ifndef NO_ALIGN_TITLE
                "altitle"       : "altitle",
#endif NO_ALIGN_TITLE
                "applications"  : "applications",
                "apply"         : "apply",
                "audience"      : "audience",

                "bit"           : "bit",
                "busy"          : "busy",

                "cat"           : "cat_file",
                "cd"            : "cd",

                "dirs"          : "dirs",
                "domainsanction": "domainsanction",
                "dsanction"     : "domainsanction",

                "emote"         : "emote",
                ":"             : "emote",

                "finger"        : "finger",

                "gd"            : "gd_info",
                "gfinger"       : "gfinger",
                "graph"         : "graph",
                "goto"          : "goto",
                "gtell"         : "gtell",
                "guild"         : "guild",
                "guildtell"     : "guildtell",
                "gwiz"          : "gwiz",
                "gwize"         : "gwiz",

                "home"          : "home",

                "last"          : "last",
                "line"          : "line",
                "linee"         : "line",
                "lineconfig"    : "lineconfig",
                "lman"          : "lman",
                "localcmd"      : "localcmd",
                "ls"            : "list_files",

                "man"           : "man",
                "metflag"       : "metflag",
                "more"          : "more_file",
                "mpeople"       : "people",
                "mudlist"       : "mudlist",

                "newhooks"      : "newhooks",
                "next"          : "next",
                "notify"        : "notify",

                "odmimie"       : "odm_imie",

                "people"        : "people",
                "people_guild"  : "people_guild",
                "popd"          : "popd",
                "pushd"         : "pushd",
                "pwd"           : "pwd",

                "ranking"       : "ranking",
                "regret"        : "regret",
                "review"        : "review",
                "rsupport"      : "rsupport",
                "rwho"          : "rwho",
                "rwiz"          : "rwiz",
                "rwize"         : "rwiz",

                "sanction"      : "sanction",
                "second"        : "second",
                "send"          : "send",
                "setmin"        : "set_m_in",
                "setmmin"       : "set_mm_in",
                "setmmout"      : "set_mm_out",
                "setmout"       : "set_m_out",
                "s�ownik"       : "slownik",
                "sman"          : "sman",
                "start"         : "start",

                "tail"          : "tail_file",
                "tell"          : "tell",
                "title"         : "title",
                "tree"          : "tree",

                "unmetflag"     : "unmetflag",

                "xmltags"       : "xmltags",

                "wizopt"        : "wizopt",
                "whereis"       : "whereis",
                "whichsoul"     : "whichsoul",
                "wiz"           : "wiz",
                "wize"          : "wiz",
                "wsay"          : "wsay",
                "wsend"         : "wsend",
                "resethp"       : "resethp",
        ]);
}

/* **************************************************************************
 * Here follows the actual functions. Please add new functions in the
 * same order as in the function name list.
 * **************************************************************************/

/* **************************************************************************
 * allcmd - List all commands available to a player
 */

/*
 * Nazwa funkcji: print_soul_list
 * Opis         : This function actually prints the souls in wizard and
 *                commands linked to them.
 * Argumenty    : string *soul_list - the souls in this section.
 *                string  soul      - then soul the wizard wants to see,
 *                                    0 if (s)he wants to see all.
 * Zwraca       : Number of matching souls found.
 */
static int
print_soul_list(string *soul_list, string soul)
{
    int index = -1;
    int size = sizeof(soul_list);
    int counter = 0;
    string soul_id;

    while(++index < size)
    {
        soul_id = soul_list[index]->get_soul_id();

        if (!soul || soul == soul_id)
        {
            write("----- " + capitalize(soul_id) + ":\n");
            write(implode(sort_array(m_indices(
                soul_list[index]->query_cmdlist())), ", ") + "\n");

            counter++;
        }
    }

    return counter;
}

public int
allcmd(string soul)
{
    object ob;
    int counter;

    CHECK_SO_WIZ;

    if (soul)
    {
        string *list = explode(soul, " ");

        if (list[0] == "me")
        {
            ob = this_player();
            soul = sizeof(list) == 1 ? 0 : implode(list[1..], " ");
        }
        else if (ob = find_living(list[0]))
            soul = sizeof(list) == 1 ? 0 : implode(list[1..], " ");
        else
            ob = this_player();
    }
    else
        ob = this_player();

    if (soul == "list")
    {
        int screen_width = this_player()->query_option(OPT_SCREEN_WIDTH);
        screen_width = (screen_width ? screen_width : 80);
        //Je�li wiz ma ustawiona szeroko�� na 0 to allcmd jest pokazywane
        //w standardowej szerokosci.

        write("Localcmd: base\n");
        if (sizeof(ob->query_wizsoul_list()))
            write(ALLCMD_LIST_SOULS("Wizsouls: ", ob->query_wizsoul_list(),
                                    screen_width));
        if (sizeof(ob->query_tool_list()))
            write(ALLCMD_LIST_SOULS("Tools:    ", ob->query_tool_list(),
                                    screen_width));
        if (sizeof(ob->query_cmdsoul_list()))
            write(ALLCMD_LIST_SOULS("Cmdsouls: ", ob->query_cmdsoul_list(),
                                    screen_width));

        return 1;
    }

    if (!soul || soul == "base")
    {
        /* We do not sort the local commands because their order is linked
         * to the order of their execution. The first command listed is
         * tested. This order also reflects the inventory of the player.
         * Objects first in the inventory are evaluated before objects later
         * in the inventory. */

        write("----- Base:\n");
        write(implode(ob->local_cmd(), ", ") + "\n");

        counter = 1;
    }
    else
    {
        counter = 0;
        notify_fail("Nie istnieje soul \"" + soul + "\".\n");
    }

    return counter + print_soul_list(ob->query_wizsoul_list()
                                   + ob->query_tool_list()
                                   + ob->query_cmdsoul_list(), soul);
}

/* **************************************************************************
 * altitle - display/change the alignment title
 */
#ifndef NO_ALIGN_TITLE
nomask int
altitle(string t)
{
    CHECK_SO_WIZ;

    if (!stringp(t))
    {
	write("Your alignment title is '" +
	      this_interactive()->query_al_title() + "'.\n");
	return 1;
    }
    this_interactive()->set_al_title(t);
    write("Ju�.\n");
    return 1;
}
#endif

/* **************************************************************************
 * applications - display applications to a domain
 */
nomask int
applications(string domain)
{
    CHECK_SO_WIZ;

    return SECURITY->list_applications(domain);
}

/* **************************************************************************
 * apply - apply to a domain
 */
nomask int
apply(string domain)
{
    CHECK_SO_WIZ;

    return SECURITY->apply_to_domain(domain);
}

/* **************************************************************************
 * bit - affect bits
 */
nomask int
bit(string args)
{
    int    argc;
    int    group;
    int    bit;
    int    index;
    int    index2;
    string *argv;
    string result;
    object player;

    CHECK_SO_WIZ;

    if (!stringp(args))
    {
	notify_fail("Komenda 'bit' wymaga podania argument^ow.\n");
	return 0;
    }
    argv = explode(args, " ");
    argc = sizeof(argv);

    if (argc == 1)
    {
	notify_fail("Zbyt ma^lo argument^ow do komendy 'bit'.\n");
	return 0;
    }

    if (!objectp(player = find_player(argv[1])))
    {
    	notify_fail("Gracz " + capitalize(argv[1]) + " nie przebywa obecnie w " +
	    "grze.\n");
    	return 0;
    }

    if (argc == 5)
    {
    	if (SECURITY->query_wiz_rank(this_player()->query_real_name()) <
    	    WIZ_ARCH)
    	{
    	    notify_fail("Nie masz odpowiednich uprawnie^n, ^zeby jako " +
    	    	"argument podawa^c nazw^e domeny. ^Zeby zmieni^c bit dotycz^acy " +
    	    	"twojej domeny, zwyczajnie pomi^n jej nazw^e.\n");
    	    return 0;
    	}

	argv[2] = capitalize(argv[2]);
    	if (SECURITY->query_domain_number(argv[2]) == -1)
    	{
    	    notify_fail("Nie istnieje domena o nazwie '" + argv[2] + "'.\n");
    	    return 0;
    	}

	if (!seteuid(argv[2]))
	{
	    write("Nie uda^lo si^e zmieni^c euid na " + argv[2] + ".\n");
	    return 1;
	}

	write("Euid zmienione na " + argv[2] + " do wykonania operacji!\n");
    	argv = exclude_array(argv, 2, 2);
    	argc--;
    }

    switch(argv[0])
    {
    case "set":
	if (argc != 4)
	{
	    write("Niepoprawna sk^ladnia. Zobacz '?bit'.\n");
	    return 1;
	}
	if ((sscanf(argv[2], "%d", group) != 1) ||
	    (sscanf(argv[3], "%d", bit) != 1))
	{
	    write("Niepoprawna sk^ladnia. Zobacz '?bit'.\n");
	    return 1;
	}
	if (!(player->set_bit(group, bit)))
	{
	    write("Nie masz uprawnie^n, ^zeby zmieni^c bit " + group + ":" + bit +
		" w " + player->query_name(PL_NAR) + ".\n");
	    return 1;
	}
	write("Ustawiony bit " + group + ":" + bit + " w " +
	    player->query_name(PL_NAR) + ".\n");
	return 1;

    case "clear":
	if (argc != 4)
	{
	    write("Niepoprawna sk^ladnia. Zobacz '?bit'.\n");
	    return 1;
	}
	if ((sscanf(argv[2], "%d", group) != 1) ||
	    (sscanf(argv[3], "%d", bit) != 1))
	{
	    write("Niepoprawna sk^ladnia. Zobacz '?bit'.\n");
	    return 1;
	}
	if (!(player->clear_bit(group, bit)))
	{
	    write("Nie masz uprawnie^n, ^zeby usun^a^c bit " + group + ":" + bit +
		" w " + player->query_name(PL_NAR) + ".\n");
	    return 1;
	}
	write("Usuni^eto bit " + group + ":" + bit + " w " +
	    player->query_name(PL_NAR) + ".\n");
	return 1;

    case "list":
	if (argc != 3)
	{
	    write("Sk^ladnia: bit list <gracz> <domena>\n");
	    return 1;
	}

	argv[2] = capitalize(argv[2]);
	result = "";
	index = -1;
	while(++index < 5)
	{
	    index2 = -1;
	    while(++index2 < 20)
	    {
		if (player->test_bit(argv[2], index, index2))
		{
		    result += " " + index + ":" + index2;
		}
	    }
	}
	if (!strlen(result))
	{
	    write("Domena " + argv[2] + " nie ma ustawionych bit^ow w " +
		player->query_name(PL_NAR) + ".\n");
	    return 1;
	}

	write("Domena " + argv[2] + " ma ustawione bity:" + result + "\n");
	return 1;

    default:
    	notify_fail("Komenda 'bit' nie przyjmuje argumentu '" + argv[0] + "'.\n");
	return 0;
    }

    notify_fail("Should never happen: switch() error in then end of bit()\n");
    return 0;
}

/* **************************************************************************
 * domainsanction - manage the domain sanction list
 */
nomask int
domainsanction(string str)
{
    CHECK_SO_WIZ;

    return SECURITY->domainsanction(str);
}

/* **************************************************************************
 * finger - display information about player/domain/wiztype
 */

/*
 * Nazwa funkcji: domainfinger
 * Opis         : This function is called to display finger information about
 *                a domain.
 * Argumenty    : string domain - the domain name.
 */
nomask static void
domainfinger(string domain)
{
    string *names;
    string name;
    string path;
    int    size;

    names = (string *)SECURITY->query_domain_members(domain);
    name = SECURITY->query_domain_lord(domain);
    if (strlen(name))
    {
	write("Lordem domeny " + domain + " jest " + capitalize(name));
	names = names - ({ name });
    }
    else
    {
	write("Domena " + domain + " nie ma Lorda");
    }

    name = SECURITY->query_domain_steward(domain);
    if (strlen(name))
    {
	write(", " + capitalize(name) + " jest Seniorem.\n");
	names -= ({ name });
    }
    else
    {
	write(".\n");
    }

    if (!sizeof(names))
    {
	write("Domena nie ma ^zadnych cz^lonk^ow.\n");
    }
    else
    {
	names = sort_array(map(names, capitalize));
	write("Domena ma " + LANG_SNUM(sizeof(names), PL_DOP, PL_MESKI_OS) +
	    " cz^lonk" + ilosc(sizeof(names), "a", "^ow", "^ow") + ": " +
	      COMPOSITE_WORDS(names) + ".\n");
    }

    name = SECURITY->query_domain_madwand(domain);
    if (strlen(name))
    {
	write("Madwandem domeny jest " + capitalize(name) + ".\n");
    }

    size = SECURITY->query_domain_max(domain) -
	sizeof(SECURITY->query_domain_members(domain));
    if (!size)
    {
	write("Nie ma wolnych miejsc w domenie.\n");
    }
    else
    {
	write("W domenie " + ilosc(size, "jest ", "s^a ", "jest ") +
	    LANG_SNUM(size, PL_MIA, PL_MESKI_NZYW) + " woln" +
	    ilosc(size, "e miejsce", "e miejsca", "ych miejsc") + ".\n");
    }

    path = SECURITY->query_wiz_path(domain) + "/open/finger_info";
    if (file_size(path) > 0)
    {
	write("--------- Dodatkowa informacja o domenie:\n" +
	      read_file(path, 1, 10) + "\n");
    }
}

/*
 * Nazwa funkcji: playerfinger
 * Opis         : This function is called to display finger information about
 *                a player.
 * Argumenty    : string name - the player name.
 *                int l - true if the long version is desired
 */
nomask static void
playerfinger(string name, int l)
{
    int    real;
    object player;
    object env;
    string pronoun;
    string domain;
    string str;
    string *names;
    int    chtime;

    /* Display the location of the player in the game, get a 'finger_player'
     * if the player is not in the game.
     */
    if (objectp(player = find_player(name)))
    {
        write(capitalize(name) + " jest obecnie w grze. Po^lo^zenie: "
            + ((env = environment(player)) ? RPATH(file_name(env)) : "VOID")
            + "\n");
        real = 1;
    }
    else
    {
        player = SECURITY->finger_player(name);
        if(player->query_final_death())
            write(set_color(COLOR_FG_RED) +
                player->query_name() + " jest ju� na tamtym �wiecie.\n" +
                clear_color());
        else
            write(player->query_name(PL_DOP) + " nie ma obecnie w grze.\n");
        real = 0;
    }

    /* Display the long description of the player. */
    if (l || !real)
        write(player->long());
    else
        write("Jest " + player->query_nonmet_name(PL_NAR) + ", znan"
            + player->koncowka("ym", "^a") + " jako:\n"
            + player->query_presentation() + ".\n");

    pronoun = ((this_player()->query_real_name() == name) ? "Jeste^s " :
	    	"Jest ");

    /* Display the rank/level of the player. */
    if (SECURITY->query_wiz_rank(name) >= WIZ_APPRENTICE)
	write(pronoun +
	      WIZ_RANK_NAME(SECURITY->query_wiz_rank(name)) +
	      player->koncowka("em", "k^a") +
	      " (poziom " + SECURITY->query_wiz_level(name) +
	      "), ustanowion" + player->koncowka("ym", "^a") + " przez " +
	      (strlen(str = SECURITY->query_wiz_chl(name, 1)) ?
	       capitalize(str) : "roota") +
#ifdef FOB_KEEP_CHANGE_TIME
	      ((chtime = SECURITY->query_wiz_chl_time(name)) ?
	       (" (" + ctime(chtime)[..15]) + ")" : "") +
#endif FOB_KEEP_CHANGE_TIME
	      ".\n");
    else
	write(pronoun + "^smiertelni" + player->koncowka("kiem", "czk^a") + ".\n");

    /* Display the domain the player is in. */
    if (strlen(domain = SECURITY->query_wiz_dom(name)))
	write(pronoun +
	      ((SECURITY->query_wiz_rank(name) == WIZ_LORD) ? "Lordem" :
	       "cz^lonkiem") + " domeny " + domain + ", dodanym przez " +
	      (strlen(str = SECURITY->query_wiz_chd(name, 1)) ?
	       capitalize(str) : "root") +
#ifdef FOB_KEEP_CHANGE_TIME
	      ((chtime = SECURITY->query_wiz_chd_time(name)) ?
	       (" on " + ctime(chtime)) : "") +
#endif FOB_KEEP_CHANGE_TIME
	      ".\n");

    /* Display login information. */
    if (real)
    {
        write(break_string(pronoun + "zalogowan" + player->koncowka("y", "a") +
	    " od " + CONVTIME_PL(time() - player->query_login_time(), PL_DOP) +
	    " z " + player->query_login_from() + ".\n", 70));
	if (query_ip_number(player))
	{
	    if (query_idle(player) > 0)
		write("Nieaktywn" + player->koncowka("y", "a") +
		    " przez: " + CONVTIME(query_idle(player)) + ".\n");
	}
	else
	{
        write("Zerwa� " + player->koncowka("", "a", "o") + " po��czenie " + 
            " " + CONVTIME(time() - player->query_linkdead()) + " temu.\n");
        }
    }
    else
    {
	write(break_string("Ostatnie logowanie: " +
	    CONVTIME(time() - player->query_login_time()) +
	    " temu z " + player->query_login_from() + ".\n", 70));
        chtime = file_time(PLAYER_FILE(name) + ".o") -
            player->query_login_time();
        if (chtime < 86400) /* 24 hours, guard against patched files */
        {
            write("Czas sp^edzony w grze: " + CONVTIME(chtime) + ".\n");
        }
    }

    if(SECURITY->query_wiz_rank(TP->query_real_name()) >= WIZ_MAGE && player->query_wiz_level() &&
        SECURITY->query_wiz_rank(name) < SECURITY->query_wiz_rank(TP->query_real_name()) || 
        name == TP->query_real_name())
    {
        string *tmp;
        tmp = player->query_seconds();
        if(sizeof(tmp) > 0)
            write("Posiada nast�puj�ce secondy: " + COMPOSITE_WORDS2(tmp, " oraz ") + ".\n");
    }

    /* Display the age and email address of the player. */
    write("Wiek: " + CONVTIME(player->query_age() * 2) + ".\n");
    write("Email: " + player->query_mailaddr() + "\n");

    // info o ostatnich logowaniach
    if(WIZ_CMD_HELPER->may_use_command("lastlogin"))
    {
        string ent = read_bytes("/d/Standard/log/ENTER",
            file_size("/d/Standard/log/ENTER") - 30000, 30000);
        if(strlen(ent))
            string *enters = explode(ent, "\n");

        enters = filter(enters, &wildmatch("* " + name + " *",));

        if (sizeof(enters) > 3)
            enters = enters[-3..];

        write("Ostatnie logowania:\n");
        foreach (string x : enters)
            write(x + "\n");
    }

    /* Wizowe finger_info. */
    player->finger_info();

    /* Ewentualne pinfo o graczu. */
    if (WIZ_CMD_HELPER->pinfo_info(name)) {
        write("Jest o " + player->query_objective() + " pinfo.\n");
        write("------------------\n");
//        write(read_file(PINFO_FILE(name)));
        TP->command("pinfo t " + name);
        write("------------------\n");
    }

    /* Clean up after ourselves if the player is not logged in. */
    if (!real)
	player->remove_object();
}

nomask static void
kontofinger(string name)
{
    int    real;
    object player;
    object env;
    string pronoun;
    string domain;
    string str;
    string *names;
    int    chtime;

    int i;
    mixed postacie, postacie_krzyzyk;
    string output = "";

    player = SECURITY->finger_konto(name);

    if (!player->query_name())
    {
	write("Nie ma takiego konta.\n");
	return;
    }

    output = "\nKonto: " + player->query_name() + "\n";
    
    postacie_krzyzyk = player->query_postacie_krzyzyk();
    postacie = player->query_postacie();
    if (sizeof(postacie))
    {
	output += "Posiada nastepuj^ace postacie: " +
	    COMPOSITE_WORDS(map(postacie, capitalize)) + ".\n";
    }
    else
	output += "Nie posiada ^zadnych postaci.\n";

    if (sizeof(postacie_krzyzyk))
    {
	for (i = 0; i < sizeof(postacie_krzyzyk); ++i)
	    postacie_krzyzyk[i] = set_color(COLOR_FG_RED) +
		capitalize(postacie_krzyzyk[i]) + clear_color();
	output += "Nastepuj^ace postacie zgin^e^ly: " +
	    COMPOSITE_WORDS(postacie_krzyzyk) + ".\n";
    }
    
    write(output);
    
    /* Ewentualne pinfo o koncie. */
    if (WIZ_CMD_HELPER->pinfo_info(name))
    {
        write("Jest o nim pinfo:\n" + 
            "---------------------\n");
        TP->command("pinfo t " + name);
        write("--------------------\n");
    }

    /* Wizowe finger_info. */
    player->finger_info();

    /* Clean up after ourselves if the player is not logged in. */
    if (!real)
        player->remove_object();
}

nomask int
finger(string str)
{
    int    index;
    int    size;
    string *names, str2;
    mapping gread;
    object *logins;
    mixed *banished;

    CHECK_SO_WIZ;

    /* This function never uses notify_fail() and return 0 since I do not
     * want it to fall back to the 'mortal' finger command. This is not
     * because of the emote as such, but merely to not mess up the fail
     * message which might contain useful information.
     */
    if (!stringp(str))
    {
        write("Sk^ladnia: finger <co^s>\n");
        return 1;
    }

#if 0
    /* Wizard wants to use the 'mortal' finger command. */
    if (wildmatch("-m *", str))
    {
	return SOUL_CMD->finger(extract(str, 3));
    }
#endif

    if (wildmatch("*@*", str))
    {
        kontofinger(str);
        return 1;
    }

    /* Wizard wants to finger a player. */
    if (wildmatch("-l *", str) &&
        SECURITY->exist_player(str2 = lower_case(extract(str, 3))))
    {
        playerfinger(str2, 1);
        return 1;
    }

    if (SECURITY->exist_player(str2 = lower_case(str)))
    {
        playerfinger(str2, 0);
        return 1;
    }

    /* Wizard wants to list all domains. */
    if ((str == "domains") || (str == "domeny"))
    {
        write(sprintf("Na mudzie s^a nastepuj^ace domeny:\n%-60#s\n",
		      implode(sort_array(SECURITY->query_domain_list()),
			      "\n")));
	return 1;
    }

    /* Wizard wants to list a particular domain. */
    if (SECURITY->query_domain_number(str) > -1)
    {
	domainfinger(capitalize(str));
	return 1;
    }

    /* Wizard wants to list the people in the queue. */
    if ((str == "queue") ||
	(str == "q") ||
	(str == "kolejka"))
    {
	names = QUEUE->queue_list(1);
	if (!(size = sizeof(names)))
	{
	    write("Aktualnie nie ma ^zadnych graczy w kolejce.\n");
	    return 1;
	}

	index = -1;
	while(++index < size)
	{
	    names[index] = sprintf("%2d: %s", (index + 1), names[index]);
	}
	write("Gracze stoj^acy w kolejce:\n" +
	      sprintf("%-70#s\n", implode(names, "\n")));
	return 1;
    }

    /* Wizard wants to list all trainees. */
    if ((str == "trainee") ||
	(str == "trainees"))
    {
	names = SECURITY->query_trainees();
	
	if (!(size = sizeof(names)))
	{
	    write("Nie ma ^zadnych praktykant^ow.\n");
	    return 1;
	}

	names = sort_array(names);
	write("Praktykant     Domena\n----------- ------\n");
	index = -1;
	while(++index < size)
	{
	    write(sprintf("%-11s", capitalize(names[index])) + " " +
		  SECURITY->query_wiz_dom(names[index]) + "\n");
	}
	return 1;
    }

    /* Wizard wants to list those with global read. */
    if ((str == "global read") ||
	(str == "global") ||
	(str == "globals"))
    {
	gread = SECURITY->query_global_read();
	if (!m_sizeof(gread))
	{
	    write("Nie ma czarodziei z globalnym prawem odczytu.\n");
	    return 1;
	}
	
	write("Czarodziej  Dodany przez Pow^od\n");
	write("----------- ------------ ------\n");
	names = sort_array(m_indices(gread));
	index = -1;
	size = sizeof(names);
	while(++index < size)
	{
	    write(sprintf("%-11s %-12s %s\n",
			  capitalize(names[index]),
			  capitalize(gread[names[index]][0]),
			  gread[names[index]][1]));
	}
	return 1;
    }
    
    if (str == "login")
    {
        logins = object_clones(find_object("/secure/login"));
        if (!(size = sizeof(logins)))
            write("W chwili obecnej nikt si^e nie loguje.\n");

        index = -1;
        while(++index < size)
            write(logins[index]->query_name() + " - " + 
                query_ip_number(logins[index]) + ", " + 
                query_ip_name(logins[index]) + ".\n");
        return 1;
    }

    /* Wizard wants to see a particular class of wizards. */
    if ((index = member_array(LANG_SWORD(str), WIZ_N)) > -1)
    {
	names = SECURITY->query_wiz_list(WIZ_R[index]);
	if (!sizeof(names))
	{
	    write("Nie ma czarodziei o takiej randze.\n");
	    return 1;
	}

	write(sprintf("Czarodzieje o randze " + WIZ_N[index] +
		      ":\n%-60#s\n",
		      implode(sort_array(map(names, capitalize)), "\n")));
	return 1;
    }

    banished = SECURITY->banish(str, 0);
    if (sizeof(banished) == 2)
    {
        write("Imi^e " + capitalize(str) + " zosta^lo zbanowane przez " +
            capitalize(banished[0]) + " (" + ctime(banished[1], 1) + ").\n");
        return 1;
    }

    /* Ewentualne pinfo o graczu. */
    if (WIZ_CMD_HELPER->pinfo_info(str))
    {
        TP->command("pinfo t " + str);
        return 1;
    }

    write("Nie ma takiego gracza, domeny ani kategorii.\n");
    return 1;
}

/* **************************************************************************
 * gd_info - Get some relevant Game driver information
 */
nomask int
gd_info(string icmd)
{
    string inf, *p;
    object ob;
    int f;

    if (!stringp(icmd))
    {
	notify_fail("Komenda 'gd' wymaga podania argument^ow.\n");
	return 0;
    }

    inf = SECURITY->do_debug(icmd);
    if (!inf)
    {
	if (sizeof(p = explode(icmd, " ")) > 1)
	{
	    if (sizeof(p) > 2)
	    {
		f = 0;
		if (sscanf(p[1],"%d", f) != 1)
		    f = 0;
		ob = parse_list(p[2]);
		inf = SECURITY->do_debug(p[0], f, ob);
	    }
	    else
	    {
		ob = parse_list(p[1]);
		inf = SECURITY->do_debug(p[0], ob);
	    }
	}
    }

    if (stringp(inf))
	write(icmd + ":\n" + inf + "\n");
    else
	dump_array(inf);
    write("\n");
    return 1;
}

/* **************************************************************************
 * gfinger - finger showone at another mud
 */
nomask int
gfinger(string str)
{
    CHECK_SO_WIZ;
#ifdef UDP_MANAGER
    return UDP_MANAGER->cmd_gfinger(str);
#else
    notify_fail("No udp manager active.\n");
    return 0;
#endif
}


/* **************************************************************************
 * graph - display user graphs
 */
nomask int
graph(string str)
{
    return SECURITY->graph(str);
}

/* **************************************************************************
 * goto - teleport somewhere
 */
nomask int
goto(string dest)
{
    object loc;

    CHECK_SO_WIZ;

    if (!stringp(dest))
    {
	if (objectp(loc = this_interactive()->query_prop(LIVE_O_LAST_ROOM)))
	{
	    this_interactive()->move_living("X", loc);
	    return 1;
	}

	notify_fail("Gdzie chcesz si^e uda^c? Nie masz poprzedniej lokacji.\n");
	return 0;
    }

    if (!objectp(loc = find_player(dest)))
    {
	loc = find_living(dest);
    }

    if (objectp(loc) &&
	objectp(environment(loc)))
    {
	this_interactive()->move_living("X", environment(loc));
	return 1;
    }

    if (sscanf(dest, "set %s", dest) == 1)
    {
	if (!objectp(loc = parse_list(dest)))
	{
	    notify_fail("Nie istnieje lokacja '" + dest + "'.\n");
	    return 0;
	}

	dest = file_name(loc);
	this_interactive()->add_prop(WIZARD_S_GOTO_SET, dest);
	write("Goto ustawione na '" + dest + "'.\n");

	return 1;
    }

    if (dest == "set")
    {
	if (!stringp(dest = this_interactive()->query_prop(WIZARD_S_GOTO_SET)))
	{
	    notify_fail("Nie masz ustawionej poprzedniej lokacji.\n");
	    return 0;
	}
    }
    else
    {
	dest = FTPATH(this_interactive()->query_path(), dest);
    }

    if (!objectp(loc = find_object(dest)))
    {
	if (LOAD_ERR(dest))
	{
	    notify_fail("Nie mo^zna zaladowa^c '" + dest + "'.\n");
	    return 0;
	}

	if (!objectp(loc = find_object(dest)))
	{
	    notify_fail("Nie znaleziono '" + dest + "'.\n");
	    return 0;
	}
    }

    this_interactive()->move_living("X", loc);
    return 1;
}

/* **************************************************************************
 * gtell - tell showone at another mud
 */
nomask int
gtell(string str)
{
    CHECK_SO_WIZ;
#ifdef UDP_MANAGER
    return UDP_MANAGER->cmd_gtell(str);
#else
    notify_fail("No udp manager active.\n");
    return 0;
#endif
}

/* **************************************************************************
 * guild - manage information about guilds
 */
nomask int
guild(string str)
{
    CHECK_SO_WIZ;

    return SECURITY->guild_command(str);
}

/* **************************************************************************
 * gwiz - intermud wizline
 */
nomask int
gwiz(string str)
{
    CHECK_SO_WIZ;
#ifdef UDP_MANAGER
    return UDP_MANAGER->cmd_gwiz(str, query_verb() == "gwize");
#else
    notify_fail("No udp manager active.\n");
    return 0;
#endif
}

/* **************************************************************************
 * home - go home, to a domain workroom, or to an other wizards home
 */
nomask int
home(string str)
{
    CHECK_SO_WIZ;

    if (str == "admin")
    {
	str = ADMIN_HOME;
    }
    else
    {
	if (!stringp(str))
	{
	    str = this_interactive()->query_real_name();
	}

	str = SECURITY->wiz_home(str);
    }

    if (this_interactive()->move_living("X", str))
    {
	write("Taka pracownia nie istnieje.\n");
    }
    return 1;
}

/* **************************************************************************
 * last - give information on the players login-time
 */

/*
 * Nazwa funkcji: last_check
 * Opis         : This function is used by the last-command. It prints the
 *                individual lines about all players the wizard selected
 *                to gather information about.
 * Argumenty    : string who   - the name of the wizard to check.
 *                int    login - true for login-info rather than logout-info.
 * Zwraca       : string - the line to print.
 */
static nomask string
last_check(string who, int login)
{
    string result;
    object pl;
    int    tmp;
    int    t_in;
    int    t_out;

    if (objectp(pl = find_player(who)))
    {
	if (interactive(pl))
	{
	    tmp = time() - pl->query_login_time();
	    result = "Logged on    " + TIME2STR(tmp, 2);

	    /* Only list idle time if more than one minute. */
	    if ((tmp = query_idle(pl)) >= 60)
	    {
		result += "   " + TIME2STR(tmp, 2);
	    }
	}
	else
	{
	    tmp = time() - pl->query_login_time();
	    result = "Linkdead     " + TIME2STR(tmp, 2);
            tmp = time() - pl->query_linkdead();
            result += "   " + TIME2STR(tmp, 2);
	}
    }
    else
    {
	/* Get the time the file was last modified. If the file_time is
	 * false, it means that the name is not valid for a player.
	 */
	t_out = SECURITY->query_player_file_time(who);
	if (!t_out)
	{
	    return sprintf("%-12s Nie ma takiego gracza", capitalize(who));
	}

	/* Get a finger-player to get the login time, then clean out the
	 * finger-player again. We do not want to waste the memory.
	 */
	pl = SECURITY->finger_player(who);
	t_in = pl->query_login_time();
	pl->remove_object();

	/* This test checks whether the alleged duration of the last
	 * visit of the wizard does not exceed two days. If the wizard
	 * was reported to have been logged in for more than two days,
	 * this probably means that the playerfile was adjusted after
	 * the player last logged out. Ergo, the duration time would
	 * range from the moment the wizard logged in until the moment
	 * his/her playerfile was changed and that is naturally not
	 * something we want.
	 */
	if ((t_out - t_in) < 172800)
	{
	    tmp = time() - (login ? t_in : t_out);
	    result = " " + TIME2STR(tmp, 2);

	    tmp = t_out - t_in;
	    result += "  " + TIME2STR(tmp, 2);
	}
	else
	{
	    /* If the player's file has been changed, we use the login
	     * time since the logout time (ie the file time) is not
	     * correct.
	     */
	    tmp = time() - t_in;
	    result = " " + TIME2STR(tmp , 2) + "         -";
	}
    }

    return sprintf("%-12s ", capitalize(who)) + result;
}

nomask int
last(string str)
{
    int     login = 0;
    int     index;
    string *plist = ({ });

    CHECK_SO_WIZ;

    if (stringp(str))
    {
        str = lower_case(str);
	if (login = wildmatch("-i*", str))
	{
	    str = extract(str, 3);
	}
    }

    /* Get the wizards currently logged in. We have to test for strlen
     * again because the wizard may have given only '-i' as argument.
     */
    if (!strlen(str))
    {
    	plist = filter(users(), &->query_wiz_level())->query_real_name();
    }
    /* The name may be a class of wizards. */
    else if ((index = member_array(str, WIZ_N)) >= 0)
    {
    	plist = SECURITY->query_wiz_list(WIZ_R[index]);

	/* Ask for arches and get the keepers too. */
    	if (WIZ_R[index] == WIZ_ARCH)
    	{
    	    plist += SECURITY->query_wiz_list(WIZ_KEEPER);
    	}
    }
    /* The name may be the name of a domain. */
    else if (SECURITY->query_domain_number(str) > -1)
    {
    	plist = SECURITY->query_domain_members(str);
    }
    /* The name may be a mail alias. */
    else if (IS_MAIL_ALIAS(str))
    {
    	plist = EXPAND_MAIL_ALIAS(str);
    }
    /* The list of one or more players. */
    else
    {
    	plist = (explode(str, " ") - ({ "" }) );
    }

    if (login)
    {
	write("Kto          Login          Czas        Idle\n");
	write("---          -----------    --------    ---------\n");
    }
    else
    {
	write("Kto          Logout         Czas        Idle\n");
	write("---          -----------    --------    ---------\n");
    }

    write(implode(map(sort_array(plist), &last_check(, login)), "\n") + "\n");
    return 1;
}

/* **************************************************************************
 * localcmd - list my available commands
 */
nomask int
localcmd(string arg)
{
    return allcmd((stringp(arg) ? arg : "me") + " base");
}

/* **************************************************************************
 * metflag - Set the metflag, a wizard can decide if met or unmet with everyone
 */
nomask int
metflag(string str)
{
    CHECK_SO_WIZ;

    if (stringp(str))
    {
        notify_fail("Metflag co?\n");
        return 0;
    }

    this_interactive()->set_wiz_unmet(0);
    write("Ju�.\n");
    return 1;
}

/* **************************************************************************
 * mudlist - List the muds known to us
 */
nomask int
mudlist(string arg)
{
    CHECK_SO_WIZ;
#ifdef UDP_MANAGER
    return UDP_MANAGER->cmd_mudlist(arg);
#else
    notify_fail("No udp manager active.\n");
    return 0;
#endif
}

/* **************************************************************************
 * newhooks - get new command hooks
 */
nomask int
newhooks(string str)
{
    object pl;

    CHECK_SO_WIZ;

    if ((WIZ_CHECK < WIZ_ARCH) ||
    	(!stringp(str)) ||
	(find_player(str) == this_interactive()))
    {
	write("Uaktualniam twoje command hooks.\n");
	this_interactive()->update_hooks();
	return 1;
    }

    pl = find_player(str);
    if (!objectp(pl))
    {
	pl = present(str, environment(this_interactive()));
    }
    if (!objectp(pl) ||
    	!living(pl))
    {
	pl = find_living(str);
    }
    if (!objectp(pl))
    {
	notify_fail("Nie ma takiego livinga.\n");
	return 0;
    }
    pl->update_hooks();
    write("Uaktualniono command hooks: " + str + "(" + file_name(pl) + ").\n");
    return 1;
}

/* **************************************************************************
 * notify - Set whether wizard is to be notified of logins or not
 */

/*
 * Nazwa funkcji: notify_string
 * Opis         : This function will return the notify flag in string form.
 * Argumenty    : int nf - the notify level in int form.
 * Zwraca       : string - the notify level in string form.
 */
nomask string
notify_string(int nf)
{
    string nstring;

    nstring = "";

    if (nf & 1)
	nstring += "A";
    if (nf & 2)
	nstring += "W";
    if (nf & 4)
	nstring += "L";
    if (nf & 8)
	nstring += "D";
    if (nf & 16)
	nstring += "I";
    if (nf & 32)
	nstring += "X";
    if (nf & 64)
	nstring += "B";
    if (nf & 128)
	nstring += "S";

    return nstring;
}

nomask int
notify(string what)
{
    int i, nf;

    CHECK_SO_WIZ;

    nf = this_player()->query_notify();

    if (!strlen(what))
    {
        if (!nf)
        {
            write("You have the notifier turned off.\n");
	    return 1;
	}
	write("Your notification level is: " + notify_string(nf) + ".\n");
	return 1;
    }

    if ((what == "clear") ||
	(what == "O") ||
	(what == "off"))
    {
	if (!nf)
	{
	    write("The notifier was already shut off.\n");
	    return 1;
	}
	nf = 0;
    }
    else
    {
	if (what[0] == '+')
	{
	    what = what[1..];
	    if (strlen(what) < 3 || !SECURITY->exist_player(what))
	    {
		write("Nie ma postaci o tym imieniu.\n");
		return 1;
	    }
	    if (this_player()->query_notified(what))
	    {
		write("Osoba o tym imieniu ju^z jest wpisana na list^e.\n");
		return 1;
	    }
	    if (!this_player()->add_notified(what))
	    {
		write("Zbyt du^zo imion wpisanych do notify. By doda^c " +
		    "nowe, musisz usun^a^c jakie^s stare.\n");
		return 1;
	    }

	    write("Ju�.\n");
	    return 1;
	}
	if (what[0] == '-')
	{
	    what = what[1..];
	    if (!this_player()->query_notified(what))
	    {
		/* Na wszelki wypadek, gdyby wartosc w mappingu == 0 */
		this_player()->remove_notified(what);
		write("Osoba o tym imieniu nie jest wpisana na list^e.\n");
		return 1;
	    }
	    this_player()->remove_notified(what);
	    write("Ju�.\n");
	    return 1;
	}
	if (what[0] == '?')
	{
	    write("Osoby wpisane na list^e 'notify': " +
		COMPOSITE_WORDS(this_player()->query_notified()) + ".\n");
	    return 1;
	}

	for (i = 0; i < strlen(what); i++)
	{
	    switch (what[i])
	    {
	    case 'A':
		nf ^= 1;
		break;
	    case 'W':
		nf ^= 2;
		break;
	    case 'L':
		nf ^= 4;
		break;
	    case 'D':
		nf ^= 8;
		break;
	    case 'I':
		nf ^= 16;
		break;
	    case 'X':
		nf ^= 32;
		break;
	    case 'B':
		nf ^= 64;
		break;
	    case 'S':
		nf ^= 128;
		break;
	    default:
		write("Strange notify state: " + extract(what, i, i) + ".\n");
		break;
	    }
	}
    }

    this_interactive()->set_notify(nf);
    write("Ju�.\n");
    return 1;
}

/* **************************************************************************
 * odmimie - Odmien swoje lub czyjes imie
 */
 
nomask int
odm_imie(string str)
{
    string *odmiana, *stare, imie;
    object ob;
    int x, y, len;
    
    if (str)
    {
        str = lower_case(str);
        
        if (wildmatch("*,*", str))
            ob = find_player(imie = (odmiana = explode(str, ","))[0]);
        else
            ob = find_player(imie = str);
        if (!ob)
        {
            write("Nie mog^e znale^x^c gracza o imieniu " + 
                capitalize(imie) + ".\n");
            return 1;
        }
        
        if (!sizeof(odmiana))
        {
            write("Oto odmiana imienia " + ob->query_name(1) + ":\n" +
                implode(ob->query_imiona(), ", ") + ".\n");
        }
        else
        {
            stare = ob->query_imiona();
            ob->remove_names(stare);
            
            if (sizeof(odmiana) != 6)
            {
                write("Odmiana musi zawiera^c 6 przypadk^ow oddzielonych " +
                    "przecinkiem.\n");
                return 1;
            }
            
            x = -1;
            while (++x < 6)
            {
                len = strlen(odmiana[x]); y = -1;
                while (++y < len && odmiana[x][y] == ' ')
                    ;
                
                if (y <= len)
                    odmiana[x] = odmiana[x][y..];
                    
                odmiana[x] = lower_case(odmiana[x]);
            }
            
            if (!ob->ustaw_imie(odmiana))
            {
                write("Nie udalo mi si^e ustawi^c odmiany.\n");
//                ob->ustaw_imie(stare);
                return 1;
            }
            if (this_interactive() != ob && WIZ_CHECK < WIZ_ARCH)
                SECURITY->log_syslog("ODM_IMIE",
                sprintf("%s %10s -> %-10s \n Nowa odmiana: (%s)\n\n", 
                ctime(time()), this_interactive()->query_real_name(), 
                imie, str), -1);
                
            write("Ju�, odmiana ustawiona.\n");
        }
    }
    else
    {
        write("Odmiana twojego imienia wygl^ada nastepuj^aco:\n" + 
            implode(this_interactive()->query_imiona(), ", ") + ".\n");
    }
    
    return 1;
}

/* **************************************************************************
 * odmprzym - Odmien swoje przymiotniki
 */

nomask int
odm_przym(string str)
{
    string *przym, *przym2, *przym3, przym4;
    int nr, x;

    CHECK_SO_WIZ;
    

    
    if (!str)
    {
        przym = (string *)this_interactive()->query_adjs();
        if (!(nr = sizeof(przym)))
        {
            write("Nie masz zdefiniowanych ^zadnych przymiotnik^ow.\n");
            return 1;
        }
        else write("Oto odmiana twoich przymiotnik^ow:\n");
        {
            nr /= 6;
            x = 1;
            while (x <= nr)
            {
                write(x + ": " + implode(przym[(x-1)*6..x*6], ", ") + ".\n");
                x++;
            }
            if (sizeof(przym)%6 != 0)
                write(x + ": " + implode(przym[(x-1)*6..], ", ") + ".\n");
        }
    }
    else
    {
        przym = explode(str, "|");
        przym3 = ({});
        for (x = 0; x < sizeof(przym); x++)
        {
            przym2 = explode(przym[x], ",");
            if(sizeof(przym2) != 6)
            {
                write("Z^la liczba przypadk^ow w " +
                    LANG_SORD((x+1), PL_NAR, PL_MESKI_NOS_NZYW) +
                    " przymiotniku.\n");
                return 1;
            }
            przym3 += przym2;
        }
        przym = (string *)this_interactive()->query_adjs();
        this_interactive()->remove_adj(przym);
        if (!(this_interactive()->set_adj(przym3)))
        {
            write("Zbyt wiele przymiotnik^ow (maks. 2), lub s^a one zbyt " +
                "d^lugie.\n");
            this_interactive()->set_adj(przym);
            return 1;
        }
        write("Ju�.\n");
    }
    return 1;
}

static mixed *old_rank = 0;
static int old_time = -1;
#define MIN_DIFF_TIME 300

/* **************************************************************************
 * ranking - Print a ranking list of the domains
 */

nomask int
rank_sort(mixed *elem1, mixed *elem2)
{
    if (elem1[1] < elem2[1]) return -1;
    if (elem1[1] == elem2[1]) return 0;
    return 1;
}

nomask int
ranking(string dom)
{
    string *doms, sg;
    mixed *mems; /* Array of array of members */
    int il, q, c, s, cmd, aft;

    CHECK_SO_WIZ;

    if (!sizeof(old_rank) || (time() - old_time) > MIN_DIFF_TIME)
    {
	doms = SECURITY->query_domain_list();
	for (mems = ({}), il = 0; il < sizeof(doms); il++)
	{
	    mems = mems + ({ ({ doms[il] }) +
			   SECURITY->query_domain_members(doms[il]) });
	}

	for (mems = ({}), il = 0; il < sizeof(doms); il++)
	{
	    q = SECURITY->query_domain_qexp(doms[il]);
	    c = SECURITY->query_domain_cexp(doms[il]);
	    cmd = SECURITY->query_domain_commands(doms[il]);
#ifdef DOMAIN_RANKWEIGHT_FORMULA
	    s = DOMAIN_RANKWEIGHT_FORMULA(q, c);
#else
	    s = 100 + (q / 25) + ((-c) / 10000);
#endif
	    mems += ({ ({ doms[il], (cmd * s) / 100,
			  s, cmd, q, c }) });
	}

	mems = sort_array(mems, rank_sort);

	old_rank = mems;
	old_time = time();
    }
    else
	mems = old_rank;

    if (stringp(dom))
    {
	if (sscanf(dom, "%d", aft) != 1)
	{
	    dom = capitalize(dom);
	    for (aft = (sizeof(mems) - 5), il = 0; il < sizeof(mems); il++)
		if (dom == mems[il][0])
		    aft = il;
	}
    }

    write("Ranking of domains:     Rank#     Weight   Base Rank    Quest   Combat\n-------------------\n");

    for (il = sizeof(mems) -1; il >= 0; il--)
    {
	q = sizeof(mems) - il;
	if ((!stringp(dom) && (q < 11)) ||
	    (stringp(dom) && (ABS(il - aft) < 3)) ||
	    dom == "All")
	{
	    sg = (mems[il][2] < 0 ? "-" + ABS(mems[il][2] / 100) : " " +
		ABS(mems[il][2] / 100));
	    write(sprintf("%2d: %-12s     %8d     %4s.%02d   %8d %8d %8d\n",
			  q,
			  mems[il][0], mems[il][1],
			  sg, ABS(mems[il][2] % 100),
			  mems[il][3], mems[il][4], mems[il][5]));
	}
    }

    return 1;
}

/* **************************************************************************
 * regret - regret an application to a domain
 */
nomask int
regret(string dom)
{
    CHECK_SO_WIZ;

    return SECURITY->regret_application(dom);
}

/* **************************************************************************
 * review - review move messages
 */
nomask int
review(string str)
{
    object tp;

    CHECK_SO_WIZ;

    if (!stringp(str))
	tp = this_interactive();
    else
	tp = find_player(str);

    if (!objectp(tp))
    {
	notify_fail("Nie ma takiego gracza: " + str +".\n");
	return 0;
    }

    write("M-out:\t" + tp->query_m_out() +
	  "\nM-in:\t" + tp->query_m_in() +
	  "\nMM-out:\t" + tp->query_mm_out() +
	  "\nMM-in:\t" + tp->query_mm_in() + "\n");
    return 1;
}

/* **************************************************************************
 * rsupport - show what another mud support
 */
nomask int
rsupport(string str)
{
    CHECK_SO_WIZ;
#ifdef UDP_MANAGER
    return UDP_MANAGER->cmd_support(str);
#else
    notify_fail("No udp manager active.\n");
    return 0;
#endif
}

/* **************************************************************************
 * rwho - show people on other muds
 */
nomask int
rwho(string str)
{
    CHECK_SO_WIZ;
#ifdef UDP_MANAGER
    return UDP_MANAGER->cmd_rwho(str);
#else
    notify_fail("No udp manager active.\n");
    return 0;
#endif
}

/* **************************************************************************
 * sanction - sanction actions
 */
nomask int
sanction(string str)
{
    CHECK_SO_WIZ;

    return SECURITY->sanction(str);
}

/* **************************************************************************
 * second - note someone as second.
 */
nomask int
second(string what)
{
    string *args, *seconds;
    int rval = 1;
    int size;
    object player;

    if (!stringp(what))
	what = "l l";

    args = explode(what, " ");
    size = sizeof(args);

    notify_fail("Z^la sk^ladnia. Obejrzyj '?second'.\n");

    switch (args[0])
    {
    case "a":
    case "add":
        if (size != 2)
            return 0;
            
	rval = this_player()->add_second(args[1]);
	if (!rval)
	{
	    write("Nie ma takiej postaci, albo nie jest ona graczem.\n");
	    return 1;
	}
	break;

    case "r":
    case "remove":
        if (size != 2)
            return 0;

	rval = this_player()->remove_second(args[1]);
	if (!rval)
	{
	    write("Nie masz takiego seconda.\n");
	    return 1;
	}
	
	break;

    case "l":
    case "list":
	if (size == 1)
	{
	    if (!sizeof(this_player()->query_seconds()))
		write("Nie masz zarejestrowanych ^zadnych second^ow.\n");
	    else write("Zarejestrowa^l" + this_player()->koncowka("e^s", "a^s") +
		"es nastepuj^acych second^ow: " +
		implode(this_player()->query_seconds(), " ") + "\n");
	    return 1;
	}
	else if (size == 2)
	{
	    args[1] = lower_case(args[1]);
	    player = find_object(args[1]);
	    if (!player)
		player = SECURITY->finger_player(args[1]);
	    if (!player)
	    {
		write("Nie ma postaci o imieniu " +
		    capitalize(args[1]) + ".\n");
		return 1;
	    }
	    
	    if (!SECURITY->query_wiz_rank(args[1]))
	    {
		write(capitalize(args[1]) + " nie jest czarodziejem.\n");
		return 1;
	    }
	    
	    seconds = player->query_seconds();
	    if (!seconds)
	    {
		write("Nie masz poj^ecia, jakich " + capitalize(args[1]) + 
		    " mo^ze mie^c second^ow.\n");
		return 1;
	    }
	    
	    if (!sizeof(seconds))
	    {
		write(capitalize(args[1]) + " nie ma ^zadnych second^ow.\n");
		return 1;
	    }
	    
	    write(capitalize(args[1]) + " ma nastepuj^acych second^ow: " +
		COMPOSITE_WORDS(seconds) + ".\n");
	    return 1;
	}

	break;
    default:
        return 0;	
    }

    write("Ju�.\n");

    return 1;
}

/* **************************************************************************
 * setmout - set move out message.
 */
nomask int
set_m_out(string m)
{
    CHECK_SO_WIZ;

    if (!strlen(m))
    {
        write("Twoj m-out: " + this_interactive()->query_m_out() + "\n");
        return 1;
    }

    this_interactive()->set_m_out(m);
    write("M-out zmieniony na: " + m + "\n");
    return 1;
}

/* **************************************************************************
 * setmin - set move in message.
 */
nomask int
set_m_in(string m)
{
    CHECK_SO_WIZ;

    if (!strlen(m))
    {
        write("Twoj m-in: " + this_interactive()->query_m_in() + "\n");
        return 1;
    }

/*    if (wildmatch("*[.!]", m))
    {
        notify_fail("Please observe that there should not be a period to " +
            "this text. Consult the help page if you are doubtful.\n");
        return 0;
    }*/

    this_interactive()->set_m_in(m);
    write("M-in zmieniony na: " + m + "\n");
    return 1;
}

/* **************************************************************************
 * setmmout - set teleport out message.
 */
nomask int
set_mm_out(string m)
{
    CHECK_SO_WIZ;

    if (!strlen(m))
    {
        write("Twoj mm-out: " + this_interactive()->query_mm_out() + "\n");
        return 1;
    }

    this_interactive()->set_mm_out(m);
    write("MM-out zmieniony na: " + m + "\n");
    return 1;
}

/* **************************************************************************
 * setmmin - set teleport in message.
 */
nomask int
set_mm_in(string m)
{
    CHECK_SO_WIZ;

    if (!strlen(m))
    {
        write("Twoj mm-in: " + this_interactive()->query_mm_in() + "\n");
        return 1;
    }

    this_interactive()->set_mm_in(m);
    write("MM-in zmieniony na: " + m + "\n");
    return 1;
}

/* **************************************************************************
 * start - set start point.
 */
nomask int
start(string str)
{
    CHECK_SO_WIZ;

    if (!stringp(str))
    {
	write("Twoja domy^slna lokacja startowa: " +
	    this_interactive()->query_default_start_location() + "\n");
	return 1;
    }

    if (str == "valid")
    {
	str = implode(SECURITY->query_list_def_start(), ", ");
	write("Dost^epne lokacje startowe:\n" + break_string(str, 76, 3)
	      + "\n");
	return 1;
    }

    if (str == "here")
    {
	str = file_name(environment(this_interactive()));
    }

    if (!this_interactive()->set_default_start_location(str))
    {
	write("<" + str + "> nie jest w^la^sciw^a lokacj^a startow^a.\n");
	return 1;
    }
    
    write("Ju�, od teraz startujesz w '" + str + "'.\n");
    return 1;
}

/* **************************************************************************
 * title - change the title
 */
nomask int
title(string t)
{
    CHECK_SO_WIZ;

    if (!stringp(t))
    {
	write("Tw^oj aktualny tytu^l: '" + this_interactive()->query_title() + "'.\n");
	return 1;
    }

    this_interactive()->set_title(t);
    write("Ju�.\n");
    return 1;
}

/* **************************************************************************
 * unmetflag - Set the unmetflag, a wizard can decide: met/unmet with everyone
 */
nomask int
unmetflag(string str)
{
    CHECK_SO_WIZ;

    if (stringp(str) &&
	(str != "npc"))
    {
        notify_fail("Sk^ladnia: unmetflag [\"npc\"].\n");
        return 0;
    }

    this_interactive()->set_wiz_unmet(((str == "npc") ? 2 : 1));
    write("Ju�.\n");
    return 1;
}

/**
 * Komenda wy�wietla dost�pne tagi xml.
 */
nomask int
xmltags(string arg)
{
    string *ansi_tags = ({"color", "background", "b", "i", "u", "inverse", "strikethrought"});
    string *mud_tags = SECURITY->do_debug("functionlist", find_object(TAGI));
    //Usuwamy funkcje kt�re nie s� tagami:)
    mud_tags -= ({"find_previous_living"});
    mud_tags -= ({"xml_to_ansi"});
    mud_tags -= ({"process_tags"});

    ansi_tags = KOLORUJ_TABLICE(ansi_tags, COLOR_FG_CYAN);
    mud_tags  = KOLORUJ_TABLICE(mud_tags, COLOR_FG_CYAN);

    write("Oto dost�pne tagi xml:\n" + 
        "* tagi ansi(kolorki): " + COMPOSITE_WORDS(ansi_tags) + "\n" + 
        "* tagi wewn�trzmudowe: " + COMPOSITE_WORDS(mud_tags) + "\n");
    return 1;
}

/* **************************************************************************
 * wizopt - Change/view the wizard options
 */
nomask int
wizopt(string arg)
{
    string *args;

    if (!stringp(arg))
    {
	wizopt("autopwd");
	return 1;
    }
    
    args = explode(arg, " ");
    if (sizeof(args) == 1)
    {
	switch(arg)
	{
	case "autopwd":
	case "pwd":
	    write("Auto pwd:     " + (this_player()->query_option(OPT_AUTO_PWD) ? "On" : "Off") + "\n");
	    break;

	default:
	    return notify_fail("Nie ma takiej opcji.\n");
	    break;
	}
	return 1;
    }

    switch(args[0])
    {
    case "autopwd":
    case "pwd":
	if (args[1] == "on")
	    this_player()->set_option(OPT_AUTO_PWD, 1);
	else
	    this_player()->set_option(OPT_AUTO_PWD, 0);
	wizopt("autopwd");
	break;

    default:
	return notify_fail("Nie ma takiej opcji.\n");
	break;
    }
    return 1;
}

/* **************************************************************************
 * whereis - Display the location of a player or living.
 */
nomask int
whereis(string str)
{
    object obj;
    object *live;
    object *dead;
    int    verbose;

    if (!stringp(str))
    {
	notify_fail("Kogo chcesz zlokalizowa^c?\n");
	return 0;
    }

    verbose = sscanf(lower_case(str), "-v %s", str);

    if (!objectp(obj = find_player(str)))
    {
	if (!objectp(obj = find_living(str)))
	{
	    notify_fail("Nie znaleziono takiego gracza ani npc'a.\n");
	    return 0;
	}

	write("Nie ma w grze gracza '" + capitalize(str) +
	    "'. Jest jednak npc o tym imieniu.\n");
    }

    if (!objectp(obj = environment(obj)))
    {
	write(capitalize(str) + " tkwi w pustce.\n");
	return 1;
    }

    write("Plik: " + file_name(obj) + "\n");
    if (!verbose)
    {
	write("Lokacja: " + obj->short(this_player()) + "\n");
	return 1;
    }

    write(obj->long(0));

    live = FILTER_LIVE(dead = all_inventory(obj));
    dead = FILTER_SHOWN(dead - live);
    if (sizeof(dead) &&
	strlen(str = COMPOSITE_DEAD(dead, 0)))
    {
	write(break_string((capitalize(str) + "."), 76) + "\n");
    }
    if (sizeof(live) &&
	strlen(str = COMPOSITE_LIVE(live, 0)))
    {
	write(break_string((capitalize(str) + "."), 76) + "\n");
    }
    return 1;
}

/**
 * Komenda resetuje hp i wszystkie hitloci.
 */
nomask int
resethp(string str)
{
    this_interactive()->set_hp(this_interactive()->query_max_hp());
    this_interactive()->reset_hitloc_hps_info();
    return 1;
}

 /* **************************************************************************
 * whichsoul - Find out which soul defines a particular command.
 */

/*
 * Nazwa funkcji: print_whichsoul
 * Opis         : Searches a type of souls for the command desired.
 * Argumenty    : string *soul_list - the souls to search through.
 *                string cmd - the command to look for.
 *                string type - the type of soul.
 * Zwraca       : int - number of times the command was found.
 */
static nomask int
print_whichsoul(string *soul_list, string cmd, string type)
{
    int    index = -1;
    int    size = sizeof(soul_list);
    string soul_id;
    string *cmds;
    int    found = 0;

    while(++index < size)
    {
	cmds = m_indices(soul_list[index]->query_cmdlist());
	if (member_array(cmd, cmds) > -1)
	{
	    soul_id = soul_list[index]->get_soul_id();
	    soul_id = (strlen(soul_id) ? soul_id : "noname");
	    write(sprintf("%-7s  %-15s  %-s\n", type, soul_id,
		 soul_list[index]));
	    found++;
	}
    }

    return found;
}

int
whichsoul(string str)
{
    object target;
    string *cmds;
    int    found = 0;

    if (!stringp(str))
    {
	notify_fail("Sk^ladnia: whichsoul [<osoba>] <komenda>\n");
	return 0;
    }

    cmds = explode(str, " ");
    switch(sizeof(cmds))
    {
	case 1:
	    target = this_player();
	    break;

	case 2:
	    target = find_player(lower_case(cmds[0]));
	    if (!objectp(target))
	    {
		notify_fail("Nie ma w grze gracza o tym imieniu.\n");
		return 0;
	    }
	    str = cmds[1];
	    break;

	default:
	    notify_fail("Zbyt wiele argument^ow. Prawid^lowa sk^ladnia: " +
		"whichsoul [<osoba>] <komenda>\n");
	    return 0;
    }

    write("Szukam komendy '" + str + "' w " + target->query_name(PL_MIE) + ".\n");

    cmds = target->local_cmd();
    if (member_array(str, cmds) > -1)
    {
	write("Istnieje co najmniej jedna komenda lokalna. (add_action)\n");
    }

    write("Type     Soulname         Filename\n");

    found += print_whichsoul(target->query_wizsoul_list(), str, "wizard");
    found += print_whichsoul(target->query_tool_list(),    str, "tool");
    found += print_whichsoul(target->query_cmdsoul_list(), str, "command");

    if (!found)
    {
	write("Nie znaleziono komendy '" + str + "' w ^zadnym soulu.\n");
    }

    return 1;
}
