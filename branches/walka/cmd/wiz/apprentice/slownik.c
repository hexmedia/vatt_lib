#include <pl.h>

int
slownik(string str)
{
    string *args;

    CHECK_SO_WIZ;

    if (!str)
    {
	notify_fail("Niew^la^sciwa ilo^s^c argument^ow.\n");
	return 0;
    }
    args = explode(str, " ");

    switch(args[0])
    {
	case "d":
	case "dodaj":
	    write("Liczba pojedyncza\n\nMianownik\t[kto? co?]:");
	    input_to("q_lp_mia");
//	    write(SLOWNIK->dodaj(implode(args[1..], " ")));
	    return 1;
	case "u":
	case "usu^n":
	    write(SLOWNIK->usun(implode(args[1..], " ")));
	    return 1;
	case "p":
	case "poka^z":
	    write(SLOWNIK->pokaz(implode(args[1..], " ")));
	    return 1;
	case "n":
	case "nagraj": 
	    SLOWNIK->zapisz_slownik();
	    write("Ju�.\n");
	    return 1;
	case "s":
	case "sprawd^x":
	    {
		mixed m = SLOWNIK->query_odmiana(implode(args[1..], " "));
		if (!m)
		{
		    write("Nie ma tego slowa w slowniku.\n");
		    return 1;
		}
		SLOWNIK->przejrzyj(implode(args[1..], " "));
		return 1;
	    }
    }
    notify_fail("Nieprawid^lowy argument.\n");
    return 0;
}

void q_lp_mia(string str) { if (!strlen(str)) { TP->add_prop("_odm", ({ })); write("Brak liczby pojedynczej.\n\nLiczba mnoga\n\nMianownik      [kto? co?]:"); input_to("q_lm_mia"); return; } if (SLOWNIK->query_odmiana(str)) { write("To slowo jest juz w slowniku!\n"); return; } TP->add_prop("_odm", ({ str })); write("Dope^lniacz     [kogo? czego?]:"); input_to("q_lp_dop"); }
void q_lp_dop(string str) { TP->add_prop("_odm", TP->query_prop("_odm") + ({ str })); write("Celownik       [komu? czemu?]:"); input_to("q_lp_cel"); }
void q_lp_cel(string str) { TP->add_prop("_odm", TP->query_prop("_odm") + ({ str })); write("Biernik        [kogo? co?]:"); input_to("q_lp_bie"); }
void q_lp_bie(string str) { TP->add_prop("_odm", TP->query_prop("_odm") + ({ str })); write("Narz^ednik      [z kim? z czym?]:"); input_to("q_lp_nar"); }
void q_lp_nar(string str) { TP->add_prop("_odm", TP->query_prop("_odm") + ({ str })); write("Miejscownik    [o kim? o czym?]:"); input_to("q_lp_mie"); }
void q_lp_mie(string str) { TP->add_prop("_odm", TP->query_prop("_odm") + ({ str })); write("\nLiczba mnoga\n\nMianownik      [kto? co?]:"); input_to("q_lm_mia"); }

void q_lm_mia(string str) { TP->add_prop("_odm", TP->query_prop("_odm") + ({ str })); write("Dope^lniacz     [kogo? czego?]:"); input_to("q_lm_dop"); }
void q_lm_dop(string str) { TP->add_prop("_odm", TP->query_prop("_odm") + ({ str })); write("Celownik       [komu? czemu?]:"); input_to("q_lm_cel"); }
void q_lm_cel(string str) { TP->add_prop("_odm", TP->query_prop("_odm") + ({ str })); write("Biernik        [kogo? co?]:"); input_to("q_lm_bie"); }
void q_lm_bie(string str) { TP->add_prop("_odm", TP->query_prop("_odm") + ({ str })); write("Narz^ednik      [z kim? z czym?]:"); input_to("q_lm_nar"); }
void q_lm_nar(string str) { TP->add_prop("_odm", TP->query_prop("_odm") + ({ str })); write("Miejscownik    [o kim? o czym?]:"); input_to("q_lm_mie"); }
void q_lm_mie(string str) { TP->add_prop("_odm", TP->query_prop("_odm") + ({ str })); write("\nRodzaj\n1. Meski os (np. dwaj m^e^zczy^xni)\n2. M^eski nos ^zyw (np. dwa elfy)\n3. M^eski nos n^zyw (np. dwa kamienie)\n4. ^Ze^nski\n5. Nijaki os (np. dwoje dzieci)\n6. Nijaki nos (np. dwa ^xd^xb^la)\n>>> "); input_to("q_rodzaj"); }

void q_rodzaj(string str)
{
    int rodz;
    string *odm;

    switch (str) {
	case "1": rodz = PL_MESKI_OS; break;
	case "2": rodz = PL_MESKI_NOS_ZYW; break;
	case "3": rodz = PL_MESKI_NOS_NZYW; break;
	case "4": rodz = PL_ZENSKI; break;
	case "5": rodz = PL_NIJAKI_OS; break;
	case "6": rodz = PL_NIJAKI_NOS; break;
	default:
	    write("\nRodzaj\n1. M^eski os (np. dwaj m^e^zczy^xni)\n2. M^eski nos ^zyw (np. dwa elfy)\n3. M^eski nos n^zyw (np. dwa kamienie)\n4. ^Ze^nski\n5. Nijaki os (np. dwoje dzieci)\n6. Nijaki nos (np. dwa ^xd^xb^la)\n>>> ");
	    input_to("q_rodzaj");
	    return;
    }
    
    odm = TP->query_prop("_odm");
    if (sizeof(odm) == 12)
	SLOWNIK->wpisz_nazwe(odm[0..5], odm[6..11], rodz);
    else
	SLOWNIK->wpisz_nazwe(odm, rodz);
}
