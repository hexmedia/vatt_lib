/*
 * /cmd/wiz/helper.c
 *
 * This special soul contains some commands useful for wizards with a
 * helper function. The following commands are supported:
 *
 * - pinfo
 */

#pragma no_clone
#pragma no_inherit
#pragma strict_types
#pragma save_binary

inherit "/cmd/std/command_driver";

#include <std.h>
#include <mail.h>
#include <colors.h>
#include <macros.h>

#define ALLOWED_WIZARD_FILE "/cmd/wiz/helpers"
#define ALLOWED_LIEGE_COMMANDS ({ "pinfo", "lastlogin" })
#define PINFO_EDIT_DONE     "pinfo_edit_done"
#define PINFO_WRITE_DONE    "pinfo_write_done"

#define CHECK_ALLOWED       \
    if (!may_use_command()) \
    {                       \
        return 0;           \
    }

/*
 * Global variables.
 */
private static mapping allowed_wizards = ([ ]);
private static mapping pinfo_edit = ([ ]);

/*
 * Prototype.
 */
string strip_comments(string text);

/*
 * Nazwa funkcji: create
 * Opis         : Constructor. Called at creation.
 */
nomask void
create()
{
    string *lines;
    int    index;
    string *args;

    SECURITY->set_helper_soul_euid();

    if (file_size(ALLOWED_WIZARD_FILE) != -1)
    {
        lines = explode(strip_comments(read_file(ALLOWED_WIZARD_FILE)), "\n") -
            ({ "" });
        index = sizeof(lines);

        while(--index >= 0)
        {
            args = explode(lower_case(lines[index]), " ") - ({ "" });
            allowed_wizards[args[0]] = args[1..];
        }
    }
}

/*
 * Nazwa funkcji: get_soul_id
 * Opis         : Return a proper name of the soul in order to get a nice
 *                printout. People can also use this name with the "allcmd"
 *                command.
 * Zwraca       : string - the name.
 */
nomask string
get_soul_id()
{
    return "helper";
}

/*
 * Nazwa funkcji: query_cmd_soul
 * Opis         : Identify this as a command soul.
 * Zwraca       : int 1 - always.
 */
nomask int
query_cmd_soul()
{
    return 1;
}

/*
 * Nazwa funkcji: using_soul
 * Opis         : Called when a wizard hooks onto the soul.
 * Argumenty    : object wizard - the wizard hooking onto the soul.
 */
public void
using_soul(object wizard)
{
    SECURITY->set_helper_soul_euid();
}

/*
 * Nazwa funkcji: query_cmdlist
 * Opis         : The list of verbs and functions. Please add new in
 *                alphabetical order.
 * Zwraca       : mapping - ([ "verb" : "function name" ])
 */
nomask mapping
query_cmdlist()
{
    return ([
        "lastlogin"     : "lastlogin",
        "pinfo"         : "pinfo", 
        ]);
}

/*
 * Nazwa funkcji: may_use_command
 * Opis         : Tests whether a particular wizard may use a the command.
 *                Arches++ can use all. The function operates on
 *                this_interactive() and query_verb().
 * Zwraca       : int 1/0 - allowed/disallowed. 
 */
nomask int
may_use_command(string verb = query_verb())
{
    string name = this_interactive()->query_real_name();

    /* No name, or no verb, no show. */
    if (!strlen(verb) ||
	!strlen(name))
    {
	return 0;
    }

    switch(SECURITY->query_wiz_rank(name))
    {
	case WIZ_ARCH:
	case WIZ_KEEPER:
	case WIZ_MAGE: // tymczasowo dodalem. d
	    /* Arches and keepers do all. */
	    return 1;
        
        case WIZ_STEWARD:
	case WIZ_LORD:
	    /* Lieges have some special commands. */
            if (member_array(verb, ALLOWED_LIEGE_COMMANDS) >= 0)
	    {
		/* Set the euid of the object right. */
		return 1;
	    }

	    /* Intentionally no break; */

	default:
	    /* Wizard may have been allowed on the commands. */
            if (member_array(verb, allowed_wizards[name]) >= 0)
	    {
		/* Set the euid of the object right. */
		return 1;
	    }
    }

    /* All others, no show. */
    return 0;
}

/*
 * Nazwa funkcji: strip_comments
 * Opis         : Take a text and strip all comments from that file in the
 *                form "slash asterisk ... asterisk slash". It will assume
 *                that the comment-markers are always paired.
 * Argumenty    : string text - the text to strip comments from.
 * Zwraca       : string - the text without comments.
 */
public string
strip_comments(string text)
{
    string left;
    string middle;
    string right;

    /*
     * See whether there is a begin marker /*. If so, then we parse out the
     * start-end combination for comments, and everything in between.
     */
    while(wildmatch("*/\\**", text))
    {
        sscanf(("$" + text + "$"), "%s/*%s*/%s", left, middle, right);
        text = extract((left + right), 1, -2);
    }

    return text;
}

/* ***************************************************************************
 * PINFO: Edit/view the information file on a player.
 */

/*
 * Nazwa funkcji: pinfo_write_done
 * Opis         : Called from the editor when the wizard is done writing
 *                the text for the file on the player.
 * Argumenty    : string text - the text to add to the file.
 */
public void
pinfo_write_done(string text)
{
    string wname = this_player()->query_real_name();

    if (MASTER_OB(previous_object()) != EDITOR_OBJECT)
    {
	write("Nielegalne wywo^lanie pinfo_write_done().\n");
	return;
    }

    if (!stringp(pinfo_edit[wname]))
    {
	write("No pinfo_edit information. Impossible! Please report!\n");
	return;
    }

    if (!strlen(text))
    {
        write("Pinfo zaniechane.\n");
        return;
    }

    /* Make sure we have the proper euid. */
    SECURITY->set_helper_soul_euid();

    write_file(pinfo_edit[wname], ctime(time()) + " " + capitalize(wname) +
        " (" + capitalize(WIZ_RANK_NAME(SECURITY->query_wiz_rank(wname))) +
	"):\n" + text + "\n");
    pinfo_edit = m_delete(pinfo_edit, wname);
    write("Zachowano informacj�.\n");
}

/*
 * Nazwa funkcji: pinfo_edit_done
 * Opis         : Called from the editor when the wizard is done editing
 *                the text for the file on the player.
 * Argumenty    : string text - the text to add to the file.
 */
public void
pinfo_edit_done(string text)
{
    string wname = this_player()->query_real_name();

    if (MASTER_OB(previous_object()) != EDITOR_OBJECT)
    {
	write("Nielegalne wywo^lanie pinfo_edit_done().\n");
	return;
    }

    if (!stringp(pinfo_edit[wname]))
    {
	write("No pinfo_edit information. Impossible! Please report!\n");
	return;
    }

    if (!strlen(text))
    {
        write("Pinfo zaniechane.\n");
        return;
    }

    /* Make sure we have the proper euid. */
    SECURITY->set_helper_soul_euid();

    rm(pinfo_edit[wname]);
    write_file(pinfo_edit[wname], text + "\n" + ctime(time()) + " " +
	capitalize(wname) + " (" +
	capitalize(WIZ_RANK_NAME(SECURITY->query_wiz_rank(wname))) +
	"):\nEdycja poprzedniego wpisu.\n\n");
    pinfo_edit = m_delete(pinfo_edit, wname);
    write("Zachowano informacj^e.\n");
}

/*
 * Nazwa funkcji: pinfo_info
 * Opis         : Na potrzeby fingera, sprawdza, czy istnieje pinfo o
 *                fingerowanym graczu, ktore this_player() moze przeczytac.
 * Argumenty:   : name - imie fingerowanego
 */
public nomask int
pinfo_info(string name)
{
    string wname = this_player()->query_real_name();

    SECURITY->set_helper_soul_euid();

    return file_name(previous_object()) == WIZ_CMD_APPRENTICE &&
           may_use_command("pinfo") &&
           (!(SECURITY->query_wiz_rank(name)) ||
            (SECURITY->query_wiz_rank(wname) == WIZ_LORD &&
             SECURITY->query_wiz_dom(wname) ==
                 SECURITY->query_wiz_dom(name)) ||
            SECURITY->query_wiz_rank(wname) >= WIZ_MAGE) && // bylo ARCH. d
           file_size(PINFO_FILE(name)) > 0;
}

nomask int
lastlogin(string str)
{
    CHECK_ALLOWED;

    NF("Sk�adnia: lastlogin [-<ilo��>] <imi�>\n");

    if(!str)
        return 0;

    int ilosc;
    string imie;
    if(str[0] == '-')
    {
        string *tmp = explode(str, " ");
        ilosc = atoi(tmp[0][1..-1]);
        imie = tmp[1];
    }
    else
    {
        ilosc = 5;
        imie = str;
    }

    string ent = read_bytes("/d/Standard/log/ENTER",
        file_size("/d/Standard/log/ENTER") - 30000, 30000);
    if(strlen(ent))
        string *enters = filter(explode(ent, "\n"), &wildmatch("* " + imie + " *",));

    if(!sizeof(enters))
    {
        write("Nie zosta�y zapisane �adne ostatnie logowania tego gracza.\n");
        return 1;
    }

    if(sizeof(enters) > ilosc)
        enters = enters[-ilosc..];

    write("Ostatnie logowania gracza o imieniu: " + UC(imie) + ":\n");
    foreach(string x : enters)
        write(x + "\n");

    return 1;
}

nomask int
pinfo(string str)
{
    string *args;
    string name;
    string wname = this_player()->query_real_name();
    int    rank = SECURITY->query_wiz_rank(wname);
    int    do_konta = 0;
    string cmd;
    string text;
    string file;
    object editor;
    object player;

    CHECK_ALLOWED;

    if (!strlen(str))
    {
        notify_fail("Sk�adnia: pinfo [k] [r / t / w / d / e] <imi�> [<tekst>]\n");
        return 0;
    }

    args = explode(str, " ");

    if(sizeof(args) == 1)
        name = lower_case(args[0]);
    else if(args[0] == "k" && sizeof(args) == 3)
        name = lower_case(args[2]);
    else
        name = lower_case(args[1]);

    player = SECURITY->finger_player(name);

    if(sizeof(args) && args[0] == "k")
    {
        args = args[1..-1];
        if(sizeof(explode(str, "@")) != 2)
            name = player->query_mailaddr();
        player = SECURITY->finger_konto(name);
        do_konta = 2;
    }

    if(sizeof(explode(str, "@")) == 2)
    {
        if(!do_konta)
            player = SECURITY->finger_konto(name);
        do_konta += 1;
    }

    args = ((sizeof(args) == 1) ? ( ({ "r" }) + args) : args);

    cmd = args[0];
    if (sizeof(args) > 2)
    {
        text = implode(args[2..], " ");
    }

    if(!player)
    {
        write(set_color(COLOR_FG_GREEN) + (do_konta ? "Konto" : "Posta�") + 
            " o tej nazwie nie istnieje.\n" + clear_color());
    }
    /*
     * Access check. The following applies:
     *
     * - arches/keepers can do all.
     * - lieges can only access information on their subject wizards.
     * - people allowed for the command can handle mortal players.
     */
    switch(rank)
    {
        case WIZ_ARCH:
        case WIZ_KEEPER:
        case WIZ_MAGE:
            /* Te poziomy mog� zrobi� wsio.  */
            break;
        case WIZ_LORD:
        case WIZ_STEWARD:
            /* Can only handle their subject wizards. */
            if ((SECURITY->query_wiz_dom(wname) ==
                SECURITY->query_wiz_dom(name)) &&
                (SECURITY->query_wiz_rank(name) <= rank))
            {
                break;
            }
            /* Intentionally no break. Could be an allowed user. */

        default:
            /* May not handle wizards here. */
            if (SECURITY->query_wiz_rank(name))
            {
                write("Nie mo�esz ogl�da� pinfa o innych czarodziejach.\n");
                player->remove_object();
                return 1;
            }
    }

    /* Make sure we have the proper euid. */
    SECURITY->set_helper_soul_euid();
    file = PINFO_FILE(name);

    if(!do_konta)
        name = UC(name);

    switch(cmd)
    {
        case "d":
            if (rank < WIZ_ARCH && member_array(wname, EXPAND_MAIL_ALIAS("mg")) == -1)
            {
                notify_fail("Tylko arch, keeper b�d� MG mo�e usuwa� pinfo.\n");
                player->remove_object();
                return 0;
            }

            if (file_size(file) == -1)
            {
                write("Nie ma �adnego pinfo o " + (do_konta ? "koncie:" : "graczu o imieniu:") + " " + name + ".\n");
                player->remove_object();
                return 1;
            }

            rm(file);
            write("Usuni�to pinfo o " + (do_konta ? "koncie:" : "graczu o imieniu:") + " " + name + ".\n");
            player->remove_object();
            return 1;

        case "e":
            if (rank < WIZ_ARCH && member_array(wname, EXPAND_MAIL_ALIAS("mg")) == -1)
            {
                notify_fail("Tylko arch, keeper b�d� MG mo�e edytowa� pinfo.\n");
                player->remove_object();
                return 0;
            }

            if (file_size(file) == -1)
            {
                write("Nie ma �adnego pinfo o " + (do_konta ? "koncie:" : "graczu o imieniu:") + " " + name + ".\n");
                player->remove_object();
                return 1;
            }
            pinfo_edit[wname] = file;
            text = read_file(file);
            clone_object(EDITOR_OBJECT)->edit(PINFO_EDIT_DONE, text,
                sizeof(explode(text, "\n")));
            player->remove_object();
            return 1;

        case "m":
        case "r":
            if (file_size(file) == -1)
            {
                write("Nie ma �adnego pinfo o " + (do_konta ? "koncie:" : "graczu o imieniu:") + " " + name + ".\n");
                player->remove_object();
                return 1;
            }

            text = read_file(file);

            /* Automatically invoke more, or wizards request. */
            if ((cmd == "m") ||
                (sizeof(explode(text, "\n")) > 100))
            {
                TP->more(text);
            }
            else
            {
                write(text);
            }

            setuid();seteuid(getuid());
            if(do_konta == 3)
            {
                object *postacie, *postacie_krzyzyk;

                postacie = player->query_postacie();
                postacie_krzyzyk = player->query_postacie_krzyzyk();

                string *ch_names, *ch_pinfo;
                ch_names = (sizeof(postacie) ? postacie : ({})) + (sizeof(postacie_krzyzyk) ? postacie_krzyzyk : ({}));

                if(sizeof(ch_names))
                {
                    write("O postaciach znajduj�cych si� na tym koncie s� nast�puj�ce pinfa:\n");
                    write("=======================================\n");

                    foreach(string sname : ch_names)
                    {
                        if(WIZ_CMD_HELPER->pinfo_info(sname))
                        {
                            write("----------------\n" +
                                "O postaci o imieniu " + UC(sname) + ":\n");
                            TP->command("pinfo t " + sname);
                            write("----------------\n");
                        }
                    }
                    write("=======================================\n");
                }
            }
            player->remove_object();
            return 1;

        case "t":
            if(file_size(file) == -1)
            {
                write("Nie ma �adnego pinfo o " + (do_konta ? "koncie:" : "graczu o imieniu:") + " " + name + ".\n");
                player->remove_object();
                return 1;
            }
            player->remove_object();
            tail(file);

            setuid();seteuid(getuid());
            if(do_konta == 3)
            {
                object *postacie, *postacie_krzyzyk;

                postacie = player->query_postacie();
                postacie_krzyzyk = player->query_postacie_krzyzyk();

                string *ch_names, *ch_pinfo;
                ch_names = (sizeof(postacie) ? postacie : ({})) + (sizeof(postacie_krzyzyk) ? postacie_krzyzyk : ({}));

                if(sizeof(ch_names))
                {
                    write("O postaciach znajduj�cych si� na tym koncie s� nast�puj�ce pinfa:\n");
                    write("=======================================\n");

                    foreach(string sname : ch_names)
                    {
                        if(WIZ_CMD_HELPER->pinfo_info(sname))
                        {
                            write("----------------\n" + 
                                "O postaci o imieniu " + UC(sname) + ":\n");
                            TP->command("pinfo t " + sname);
                            write("----------------\n");
                        }
                    }
                    write("=======================================\n");
                }
            }
            return 1;

        case "w":
            if (file_size(file) == -1)
            {
                write("Tworzenie pliku pinfa dla " + (do_konta ? "konta:" :  "gracza o imieniu:") + " " + name + ".\n");
            }
            else
            {
                write("Plik pinfo " + (do_konta ? "konta:" : "gracza o imieniu:") + " " + name + " zosta� " + 
                    "uzupe�niony o podane informacje.\n");
            }

            if(strlen(text))
            {
                if (strlen(text) > 75)
                {
                    text = break_string(text, 75);
                }

                write_file(file, ctime(time()) + " " + capitalize(wname) + " (" +
                capitalize(WIZ_RANK_NAME(SECURITY->query_wiz_rank(wname))) +
                    "):\n" + text + "\n\n");
            }
            else
            {
                pinfo_edit[wname] = file;
                clone_object(EDITOR_OBJECT)->edit(PINFO_WRITE_DONE);
            }

            player->remove_object();
            return 1;

        default:
            notify_fail("Sk�adnia: pinfo [k] [r / t / w / d / e] <imi�> [<tekst>]\n");
            player->remove_object();
            return 0;
    }

    player->remove_object();
    write("Nieoczekiwane zako�czenie funkcji 'pinfo()'. Prosz� zg�o� o tym.\n");
    return 1;
}
