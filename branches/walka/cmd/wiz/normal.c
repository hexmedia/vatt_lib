/*
 * /cmd/wiz/normal.c
 *
 * This object holds the normal wizards commands. Since the soul is split
 * over a few modules, the list below only reflects the commands actually
 * coded in this module:
 *
 * - banish
 * - buglog
 * - cbd
 * - cbs
 * - cmdsoul
 * - combatdata
 * - combatstat
 * - control
 * - donelog
 * - dtell
 * - dtelle
 * - du
 * - errlog
 * - idealog
 * - invis
 * - ip
 * - leave
 * - lplog
 * - multi
 * - peace
 * - possess
 * - praiselog
 * - retire
 * - runlog
 * - sdoc
 * - shutdown
 * - skillstat
 * - snoop
 * - stat
 * - tellall
 * - toolsoul
 * - tradestat
 * - trans
 * - typolog
 * - vis
 */

#pragma no_clone
#pragma no_inherit
#pragma strict_types
#pragma save_binary

inherit "/cmd/std/tracer_tool_base.c";

/*
 * This is necessary for AFT.
 */
inherit "/lib/cache";

#include <composite.h>
#include <filepath.h>
#include <files.h>
#include <filter_funs.h>
#include <flags.h>
#include <language.h>
#include <log.h>
#include <macros.h>
#include <money.h>
#include <sit.h>
#include <ss_types.h>
#include <std.h>
#include <stdproperties.h>
#include <options.h>
#include <colors.h>
#include <ip_admin.h>

#define CHECK_SO_WIZ         if (WIZ_CHECK < WIZ_NORMAL) return 0; \
                        if (this_interactive() != this_player()) return 0

#include "/cmd/wiz/normal/edit.c"
#include "/cmd/wiz/normal/files.c"

static nomask int somelog(string str, string logname, string log);
static nomask int valid_possess(object demon, object possessed);

/*
 * Nazwa funkcji: create
 * Descritpion  : Constructor. Called when the soul is loaded. We use it
 *                to set the cache size larger than the default. Because the
 *                cache is used for two commands, we doubled it.
 */
nomask void
create()
{
    set_cache_size(20);
}

/* **************************************************************************
 * Return a list of which souls are to be loaded.
 * The souls are listed in order of command search.
 */
nomask string *
get_soul_list()
{
    return ({ WIZ_CMD_NORMAL,
              WIZ_CMD_APPRENTICE,
              MBS_SOUL,
              WIZ_CMD_HELPER});
}

/* **************************************************************************
 * Return a proper name of the soul in order to get a nice printout.
 */
nomask string
get_soul_id()
{
    return WIZNAME_NORMAL;
}

/* **************************************************************************
 * The list of verbs and functions. Please add new in alfabetical order.
 */
nomask mapping
query_cmdlist()
{
    return ([
             "aft"          : "aft",

             "banish"       : "banish",
             "buglog"       : "buglog",

             "cbd"          : "combatdata",
             "cbs"          : "combatstat",
             "clone"        : "clone",
             "cmdsoul"      : "cmdsoul",
             "combatdata"   : "combatdata",
             "combatstat"   : "combatstat",
             "control"      : "control",
             "cp"           : "cp_cmd",

             "destruct"     : "destruct_ob",
             "distrust"     : "distrust",
             "donelog"      : "donelog",
             "dtell"        : "dtell",
             "dtelle"       : "dtell",
             "du"           : "du",

             "ed"           : "ed_file",
             "errlog"       : "errlog",
             "exec"         : "exec_code",


             "idealog"      : "idealog",
             "invis"        : "invis",
             "ip"           : "ip",

             "leave"        : "leave",
             "lplog"        : "lplog",
             "load"         : "load",

             "mkdir"        : "makedir",
             "multi"        : "multi",
             "mv"           : "mv_cmd",


             "pad"          : "pad",
             "peace"        : "peace",
             "possess"      : "possess",
             "praiselog"    : "praiselog",

//             "odmie�"     : "odmien",
             "odn�w"        : "odnow",

             "remake"       : "remake_object",
             "retire"       : "retire",
             "rm"           : "rm_cmd",
             "rmdir"        : "removedir",
             "runlog"       : "runlog",

             "sdoc"         : "sdoc",
             "shutdown"     : "shutdown_game",
             "skillstat"    : "skillstat",
             "snoop"        : "snoop_on",
             "stat"         : "stat",

             "tellall"      : "tellall",
             "toolsoul"     : "toolsoul",
             "tradestat"    : "tradestat",
             "trans"        : "trans",
             "trust"        : "trust_ob",
             "typolog"      : "typolog",

             "update"       : "dupd",

             "vis"          : "vis",
             ]);
}

/* **************************************************************************
 * Here follows the actual functions. Please add new functions in the
 * same order as in the function name list.
 * **************************************************************************/

/* **************************************************************************
 * banish - banish a name
 */
nomask int
banish(string arg)
{
    int wtype;
    string *argv, name, what;
    mixed *rval;

    if (stringp(arg) == 0)
    {
        notify_fail("Zbanowa^c kogo?\n");
        return 0;
    }

    argv = explode(arg, " ");

    if (sizeof(argv) == 2)
    {
        what = argv[0];
        name = argv[1];
    }
    else
    {
        what = "-i";
        name = argv[0];
    }

    if (SECURITY->exist_player(name))
    {
        notify_fail("Gracz " + capitalize(name) + " ju^z istnieje.\n");
        return 0;
    }

    wtype = SECURITY->query_wiz_rank(this_interactive()->query_real_name());
    switch (what)
    {
    case "-a":
    case "-add":
        /*
         * Arch and keeper can banish anywhere.
         */
        if (wtype < WIZ_ARCH)
        {
            if (function_exists("create_room",
                environment(this_interactive())) != ADMIN_HOME)
            {
                notify_fail("Mo^zesz zbanowa^c imi^e tylko w pracowni " +
                    "administrator^ow.\n");
                return 0;
            }
        }

        /*
         * Try to banish.
         */
        rval = SECURITY->banish(name, 2);

        if (sizeof(rval) != 0)
            write(capitalize(name) + " ma bana dzi�ki " +
                  capitalize(rval[0]) + " w " + ctime(rval[1], 1) + ".\n");
        else
            write("Zbanowa^l" + this_player()->koncowka("e^s", "a^s") +
                ": " + capitalize(name) + ".\n");

        break;

    case "-i":
    case "-info":
        /*
         * Look for information.
         */
        rval = SECURITY->banish(name, 0);
        if (sizeof(rval) != 0)
            write(capitalize(name) + " ma bana dzi�ki " +
                  capitalize(rval[0]) + " w " + ctime(rval[1], 1) + ".\n");
        else
            write(capitalize(name) + " nie ma bana.\n");
        break;

    case "-r":
    case "-remove":
        /*
         * Try to remove.
         */
        rval = SECURITY->banish(name, 0);
        if (sizeof(rval) != 0)
        {
            if (wtype != WIZ_KEEPER && wtype != WIZ_ARCH)
            {
                if (rval[0] != this_interactive()->query_real_name())
                {
                    notify_fail("Nie zbanowa�"+this_player()->koncowka("e� ","a� ")+
                                capitalize(name) + ".\n");
                    return 0;
                }
            }
        }
        else
        {
            notify_fail("Imi^e '" + capitalize(name) +
                "' nie zosta^lo zbanowane.\n");
            return 0;
        }

        rval = SECURITY->banish(name, 1);
        if (sizeof(rval) != 0)
        {
            write(capitalize(name) + " ma bana dzi�ki " +
                  capitalize(rval[0]) + " w " + ctime(rval[1], 1) + ".\n");
            write("Usun"+this_player()->koncowka("��e�","�a�")+" bana dla "+
                  capitalize(name) + ".\n");
        }

        break;

    default:
        break;
    }
    return 1;
}

/* **************************************************************************
 * buglog - list a buglog
 */
nomask int
buglog(string str)
{
    return somelog(str, "bug log", "/bugs");
}

/* **************************************************************************
 * cmdsoul - affect your command souls
 */
nomask int
cmdsoul(string str)
{
    string *cmdsoul_list;
    int index, size;

    CHECK_SO_WIZ;

    cmdsoul_list = (string *)this_interactive()->query_cmdsoul_list();
    if (!stringp(str))
    {
        index = -1;
        size = sizeof(cmdsoul_list);
        write("Aktualne cmdsoul'e:\n");
        while(++index < size)
        {
            write(sprintf("%2d: %s\n", (index + 1), cmdsoul_list[index]));
        }
        return 1;
    }

    str = FTPATH((string)this_interactive()->query_path(), str);

    if (member_array(str, cmdsoul_list) >= 0)
    {
        if (this_interactive()->remove_cmdsoul(str))
            write("Usuni^eto soula '" + str + "'.\n");
        else
            write("Nie uda^lo si^e usun^a^c soula '" + str + "'.\n");
        return 1;
    }

    if (this_interactive()->add_cmdsoul(str))
        write("Dodano soula '" + str + "'.\n");
    else
        write("Nie uda^lo si^e doda^c soula '" + str + "'.\n");
    return 1;
}

/* **************************************************************************
 * combatdata - Show combat statistics for a living object
 */
nomask int
combatdata(string str)
{
    object live;

    CHECK_SO_WIZ;

    if (!str)
        live = this_player();
    else if (live = present(str, environment(this_player())));
    else if (live = find_player(str));
    else if (live = find_living(str));

    if (live)
        write(live->combat_data());
    else
    {
        notify_fail("Nie znaleziono livinga: " + str + "\n");
        return 0;
    }
    return 1;
}

/* *************************************************************************
 * combatstat - Show some combat statistics for a living object
 */
nomask int
combatstat(string str)
{
    object live;

    CHECK_SO_WIZ;

    if (!str)
        live = this_player();
    else if (live = present(str, environment(this_player())));
    else if (live = find_player(str));
    else if (live = find_living(str));

    if (live)
        write(live->combat_status());
    else
    {
        notify_fail("Nie znaleziono livinga: " + str + "\n");
        return 0;
    }
    return 1;
}

/* **************************************************************************
 * control - Get a 'control wand' for a specific monster
 */
nomask int
control(string npc)
{
    object mon, owner;
    string str;

    CHECK_SO_WIZ;

    notify_fail("Nad kim chcesz obj^a^c kontrol^e?\n");

    if (!npc)
        return 0;

    mon = present(npc, environment(this_player()));

    if (!mon)
        mon = find_living(npc);

    if (!mon)
    {
        notify_fail("Nie ma tu takiego npc'a.\n");
        return 0;
    }

    if (!mon->query_npc())
    {
        notify_fail("Czujesz jak^a^s si^l^e, powstrzymuj^ac^a ci^e przed tym.\n");
        return 0;
    }

    if (owner = (object)mon->query_link_remote())
    {
        owner = environment(owner);
        if (living(owner))
            str = "jest w posiadaniu " + owner->short(this_player(), PL_DOP);
        else
            str = "zdaje si^e by^c w: " + owner->short(this_player(), PL_NAR);
        write(capitalize(npc) + " jest ju^z pod czyj^a^s kontrol^a. R^o^zd^zka " +
              str + "\n");
        return 1;
    }
    mon->set_link_remote();
    write("Ju�.\n");
    return 1;
}

/* **************************************************************************
 * donelog - list donelog
 */
nomask int
donelog(string str)
{
    return somelog(str, "done log", "/done");
}

/* **************************************************************************
 * dtell  - actually an alias for 'line <my domain>'
 * dtelle - actually an alias for 'linee <my domain>'
 */
nomask int
dtell(string str)
{
    string domain;

    if (!stringp(str))
    {
            notify_fail(capitalize(query_verb()) + " co?\n");
            return 0;
    }

    if (!strlen(domain =
        SECURITY->query_wiz_dom(this_interactive()->query_real_name())))
    {
            notify_fail("Nie jeste^s cz^lonkiem ^zadnej domeny.\n");
            return 0;
    }

    return WIZ_CMD_APPRENTICE->line((domain + " " + str),
        (query_verb() == "dtelle"));
}



/* **************************************************************************
 * errlog - list an error log
 */
nomask int
errlog(string str)
{
    return somelog(str, "error log", "/errors");
}


/* **************************************************************************
 * idealog - read the idea log
 */
nomask int
idealog(string str)
{
    return somelog(str, "idea log", "/ideas");
}

/* **************************************************************************
 * invis - become invisible
 */
nomask int
invis(string arg)
{
    CHECK_SO_WIZ;

    if(arg == "k" || arg == "kto")
    {
	if(TP->query_invis(1))
        {
	    notify_fail("Jeste� ju� niewidoczny w 'kto'.\n");
	    return 0;
	}
	TP->set_invis(1, 1);
	write("Od teraz jeste� niewidoczny w kto.\n");
	return 1;
    }

    if (this_player()->query_invis())
    {
        notify_fail("Ju^z jeste^s niewidzialn" +
            this_player()->koncowka("y", "a") + ".\n");
        return 0;
    }
    
    if(sizeof(this_player()->query_prop(SIT_SIEDZACY)))
    {
        notify_fail("Musisz wpierw wsta� by m�c to uczyni�!\n");
        return 0;
    }

    write("Stajesz si^e niewidzialn" + this_player()->koncowka("y", "a") +
        ".\n");
    saybb(QCIMIE(this_player(), PL_MIA) + " " + this_player()->query_mm_out()
        + "\n");
    TP->set_invis(1);
    TP->set_invis(1, 1);
    saybb(QCIMIE(this_player(), PL_MIA)
        + " staje si� niewidzialn"+this_player()->koncowka("y","a")+".\n");
    return 1;
}

nomask int
ip(string str)
{
    CHECK_SO_WIZ;

    if(SECURITY->query_wiz_rank(TP->query_real_name()) < IP_RANK_ZOBACZENIE)
        return 0;

    notify_fail("Sk�adnia: ip <subkomenda> <adres>\n");
    
    if(!str)
        return 0;
    
    string arg;
    if(sscanf(str, "%s %s", str, arg) != 2)
    {
        mapping ips = SECURITY->query_ip_list();
        
	if(!m_sizeof(ips))
	{
            write("W chwili obecnej nie ma �adnych adres�w ip na li�cie.\n");
	    return 1;
        }
        
        write(sprintf("%15|s %20|s %44|s\n", "Adres Ip", "Dodajacy", "Data dodania")); 

	foreach(string name : m_indexes(ips))
            write(sprintf("%15|s %20|s %44|s\n", name, ips[name][1], ctime(ips[name][2]))); 
		
        return 1;
    }

    switch(str)
    {
        case "add":
        case "a":
        case "dodaj":
        case "d":
            mixed tmp;

            if(SECURITY->query_wiz_rank(TP->query_real_name()) < IP_RANK_DODAWANIE)
                return 0;
            
            notify_fail("Sk�adnia: ip <add/dodaj/a/d/> <adres> <pow�d>.\n");
	    if(sscanf(arg, "%s %s", arg, str) != 2)
            {
                str = "";                
                return 0;
	    }
            if(tmp = find_player(arg))
                arg = query_ip_number(tmp);
            
	    notify_fail("Podaj poprawny adres ip lub imi� gracza obecnego w grze.\n");	    
            if(sizeof(tmp=explode(arg, ".")) != 4)
                return 0;
	    if(!atoi(tmp[0]) || !atoi(tmp[1]) || !atoi(tmp[2]) || !atoi(tmp[3]))
                return 0;
	    if(!str) 
	    {
	        notify_fail("Musisz poda� pow�d.\n");
                return 0;
	    }
            if(strlen(str) < IP_MIN_DLUGOSC_POWODU)
	    {
                notify_fail("Opis powodu za kr�tki. Minimalna dlugo�� wynosi " + 
                    IP_MIN_DLUGOSC_POWODU + " znak�w.\n");
                return 0;
	    }
	    tmp = SECURITY->add_ip_to_list(arg, str);
	    if(!tmp)
            {
                notify_fail("Nie uda�o si� doda� adresu do listy.\n");
	        return 0;	
            }
            if(tmp == -1)
            {
                notify_fail("Taki adres zosta� ju� dodany.\n");
		return 0;
	    }
	    write("Ju�.\n");
	    return 1;
        case "remove":
        case "delete":
        case "r":
        case "usu�":
        case "u":
            //mixed tmp;
            notify_fail("Podaj poprawny adres ip.\n");	    
            if(sizeof(tmp=explode(arg, ".")) != 4)
                return 0;
            if(!atoi(tmp[0]) || !atoi(tmp[1]) || !atoi(tmp[2]) || !atoi(tmp[3]))
                return 0;
            if(SECURITY->query_wiz_rank(TP->query_real_name()) < IP_RANK_USUWANIE)
            {
                notify_fail("Nie mo�esz usuwa� adres�w ip z listy. " +
                    "Jest to dozwolone dla " + WIZ_S[IP_RANK_USUWANIE] + "'a i wy�ej.\n");
                return 0;
            } 
            tmp = SECURITY->remove_ip_from_list(arg);
            if(!tmp)
            {
                notify_fail("Nie uda�o si� usun�� adresu z listy.\n");
		return 0;
	    }
            if(tmp == -1)
            {
                notify_fail("Takiego adresu nie ma w li�cie dozwolonych multi ip�w.\n");
		return 0;
            }
            write("Ju�.\n");
	    return 1;
        case "show":
        case "s":
        case "poka�":
        case "p":
	    //mixed tmp;
            notify_fail("Podaj poprawny adres ip.\n");	    
            if(sizeof(tmp=explode(arg, ".")) != 4)
                return 0;
	    if(!atoi(tmp[0]) || !atoi(tmp[1]) || !atoi(tmp[2]) || !atoi(tmp[3]))
                return 0; 
	    tmp = SECURITY->query_ip_from_list(arg);
            if(!tmp)
            {
                notify_fail("Nie uda�o si� odczyta� adresu.\n");
		return 0;
	    }
	    if(tmp == -1)
            {
                notify_fail("Adres '" + arg + "' nie istnieje.\n");
		return 0;
	    }
            write("Adres: " + arg + "\nPow�d: " + tmp[0] + "\nOdblokowuj�cy: " +
                tmp[1] + "\nData: " + ctime(time()) + "\n");        
	    return 1;
        default:
            notify_fail("Nieznana subkomenda.\n");
    }
    
    return 0;
}
/* **************************************************************************
 * leave - leave a domain
 */
nomask int
leave(string str)
{
    CHECK_SO_WIZ;

    if (str != "domain")
    {
        notify_fail("Sk^ladnia: leave domain\n");
        return 0;
    }

    return SECURITY->leave_domain();
}

nomask int
lplog(string str)
{
    TP->command("tail /lplog");
    return 1;
}

nomask int
multi(string str)
{
    CHECK_SO_WIZ;

    mixed mlogs = SECURITY->query_multi_log();

    if(str)
    {
        if(strlen(str) >= 2)
        {
            if(str[0..1] == "-l")
            {
                if(str[2] == 't')
                    TP->command("tail /d/Standard/log/MULTILOGI");
                else if(str[2] == 'm')
                    TP->command("more /d/Standard/log/MULTILOGI");
                else if(str[2] == 'r')
                    TP->command("rm /d/Standard/log/MULTILOGI");
                else if(str[2] == 'e')
                    TP->command("ed /d/Standard/log/MULTILOGI");
                else
                    TP->command("cat /d/Standard/log/MULTILOGI");
                return 1;
            }
        }
    }

    if(!mlogs)
    {
        write("W chwili obecnej nie ma �adnych zarejestrowanych multilog�w.\n");
        return 1;
    }

    if(!sizeof(mlogs[0]) && !sizeof(mlogs[1]))
    {
        write("W chwili obecnej nie ma �adnych zarejestrowanych multilog�w.\n");
        return 1;
    }
    if(!sizeof(mlogs[0]))
        write("Nie zarejestrowano �adnych multilog�w z adres�w ip, natomiast uda�o si� znale�� "+
           "multilog " + (sizeof(mlogs[1]) > 1 ? "i" : "") + " z kont postaci. Oto one:\n");
    else
        write("Oto aktuanle mutlilogi z adres�w ip:\n");

    write(sprintf("%7|s %15|s %57|s\n", "Ilo��", "Adres", "Imiona postaci"));

    foreach(mixed *log : mlogs[0])
    {
        int i = sizeof(log[1]);

        mixed tmp = sprintf("%7|s %15|s %57|s\n", i+"", log[0]+"", implode(log[1], ", "));

        write((i>2 ? set_color(COLOR_FG_RED) + tmp + clear_color() : tmp ));
    }
    if(sizeof(mlogs[0]) && sizeof(mlogs[1]))
        write("A oto multilogi z kont postaci:\n");
    else
        write("Z kont postaci nie zarejestrowano �adnych multilog�w.\n");

    foreach(mixed *log : mlogs[1])
    {

        int i = sizeof(log[1]);

        mixed tmp = sprintf("%7|s %15|s %57|s\n", i+"", log[0]+"", implode(log[1], ", "));

        write((i>2 ? set_color(COLOR_FG_RED) + tmp + clear_color() : tmp ));
    }

    return 1;
}


/* **************************************************************************
 * peace - stop fighting (in the room)
 */
nomask int
peace(string str)
{
    object *oblist;
    object *targets;
    int     index = -1;
    int size;

    CHECK_SO_WIZ;

    if (!stringp(str))
    {
        oblist = ({ this_player() });

        say(QCIMIE(this_player(), PL_MIA) + " przekazuje sobie i swoim "
          + "przeciwnikom znak pokoju.\n");
	write("Przekazujesz same"+this_player()->koncowka("mu","j")+
	      " sobie znak pokoju.\n");
    }
    else
    {
        oblist = parse_this(str, "[the] %l");

        if (!sizeof(oblist))
        {
            notify_fail("Peace dla kogo?\n");
            return 0;
        }

        actor("Przekazujesz", oblist, PL_CEL," znak pokoju.");
        target("przekazuje sobie oraz swoim przeciwnikom" +
               " znak pokoju.", oblist);
        all2act("przekazuje", oblist, PL_CEL," znak pokoju.");
    }

    size = sizeof(oblist);
    while(++index < size)
    {
        targets = oblist[index]->query_enemy(-1);

        oblist[index]->stop_fight(targets);
        targets->stop_fight(oblist[index]);
    }

    return 1;
}

/* **************************************************************************
 * possess - possess a living object
 */
nomask int
possess(string arg)
{
    object victim, possob;
    string *argv, v_name, p_name;
    int argc;

    CHECK_SO_WIZ;

    if (!strlen(arg))
    {
        notify_fail("Possess kogo?\n");
        return 0;
    }

    argv = explode(arg, " ");
    argc = sizeof(argv);

    if (argc == 0)
    {
        notify_fail("Possess kogo?\n");
        return 0;
    }

    if(SECURITY->query_wiz_level(this_player()->query_real_name()) <= 15)
    {
        notify_fail("Ta komenda dost�pna jest tylko dla bardziej "+
                    "zas�u�onych uczni�w i wy�ej.\n");
        return 0;
    }

    /*
     * Find the victim of this evil spell.
     */
    if (argc == 1)
        v_name = lower_case(argv[0]);
    else
        v_name = lower_case(argv[1]);

    victim = present(v_name, environment(this_interactive()));

    if (!victim)
        victim = find_living(v_name);

    if (!victim)
    {
        notify_fail("Nie ma takiego livinga.\n");
        return 0;
    }

    p_name = (string)victim->query_possessed();

    if (!living(victim))
    {
        notify_fail("Mo�esz tylko zaw�adn�� nad �ywymi.\n");
        return 0;
    }

    if (victim == this_interactive())
    {
        notify_fail("Hej! Ju� siedzisz w swojej w�asnej g�owie!\n");
        return 0;
    }

    if (!valid_possess(this_interactive(), victim))// &&
    //     SECURITY->query_wiz_rank(geteuid(this_interactive())) < WIZ_MAGE) 
    // NIe wiem po co ten kawa�ek kodu, ale pozwala� wszystkim >= WIZ_MAGE possessowa� wszystko, Krun.
    {
        notify_fail("Nie jeste� do tego upowa�nion"+this_player()->koncowka("y","a") + ".\n");
        return 0;
    }
    /*
     * Only an Arch or Keeper can break or force a possession.
     */
    if ((argc == 2) &&
        (SECURITY->query_wiz_rank(geteuid(this_interactive())) >= WIZ_ARCH))
    {
        switch (argv[0])
        {
        case "force":
            if (strlen(p_name))
            {
                victim->command("quit");
                write("You exorcised " + capitalize(p_name) + ".\n");
                tell_object(find_player(p_name), "You were exorcised by " + capitalize((string)this_interactive()->query_real_name()) + ".\n");
            }
            else
                write(capitalize(v_name) + " is unpossessed.\n");
            break;

        case "break":
            if (strlen(p_name))
            {
                victim->command("quit");
                write("You exorcised " + capitalize(p_name) + ".\n");
                tell_object(find_player(p_name), "You were exorcised by " + capitalize((string)this_interactive()->query_real_name()) + ".\n");
            }
            else
                write(capitalize(v_name) + " is unpossessed.\n");
            return 1;
            break;

        default:
            notify_fail("Dziwna komenda: " + argv[0] + ".\n");
            return 0;
            break;
        }
    }
    else
    {
        if (strlen((string)victim->query_possessed()))
        {
            notify_fail(capitalize(v_name) + " w tej g�owie siedzi w�a�nie " + capitalize(p_name) + "!\n");
            return 0;
        }
    }

    if (!query_ip_name(victim))
    {
        if (SECURITY->exist_player(v_name))
        {
	    notify_fail("Nie mo�esz zaw�adn�� nad obiektem z t� sam� nazw� co gracz.\n");
            return 0;
        }
    }

    /*
     * Create the control object and do the possession.
     */
    possob = clone_object(POSSESSION_OBJECT);
    possob->move(victim, 1);
    possob->set_name(v_name);

    if (possob->possess(this_interactive(), victim))
    {
        snoop(possob, victim);
        possob->set_lock();
    }
    else
    {
        possob->remove_object();
    }

    return 1;
}

static nomask int
valid_possess(object demon, object possessed)
{
    int rank;
    string dom, by, on;

    rank = SECURITY->query_wiz_rank(geteuid(demon));

    /* Arch & keeper possesses all everywhere */
    if (rank >= WIZ_ARCH)
        return 1;

    by = geteuid(demon);
    dom = SECURITY->query_wiz_dom(by);
    on = geteuid(possessed);

    /* never possess an arch or keeper. */
    if ((rank < WIZ_ARCH) &&
        (SECURITY->query_wiz_rank(on) >= WIZ_ARCH))
    {
        return 0;
    }

    /* Lords and stewards can possess members & member's objects everywhere */
    if (rank == WIZ_LORD || rank == WIZ_STEWARD)
    {
        if (SECURITY->query_domain_lord(dom) == by)
        {
            if (dom == SECURITY->query_wiz_dom(on))
            {
                return 1;
            }
        }
    }

    /* You can possess objects in your domain */
    if (!query_ip_number(possessed) &&
        ((dom == on) || (dom == SECURITY->query_wiz_dom(on))))
    {
        return 1;
    }

    /* Ka�dy od 15 poziomu mo�e possessowa� obiekty z domeny standard */
    if(SECURITY->domain_object(possessed) == "Standard" && SECURITY->query_wiz_level(by) > 15)
        return 1;

    return 0;
}

/* **************************************************************************
 * praiselog - list your praise log
 */
nomask int
praiselog(string str)
{
    return somelog(str, "praise log", "/praise");
}

/* **************************************************************************
 * retire - Retire from active wizhood.
 */
nomask int
retire(string str)
{
    CHECK_SO_WIZ;

    if (str != "from czarymaryhokuspokus")
    {
        notify_fail("Je^sli chcesz zrezygnowa^c z czarowania, wpisz " +
            "'retire from czarymaryhokuspokus'.\n");
        return 0;
    }

    return SECURITY->retire_wizard();
}

/* **************************************************************************
 * runlog - list a runtime log
 */
nomask int
runlog(string str)
{
    return somelog(str, "runtime log", "/runtime");
}

/* **************************************************************************
 * sdoc - extract documentation from file(s)
 */
nomask int
sdoc(string str)
{
    string *argv, *files, *parts, path;
    int argc, i;
    mixed *ord;

    if (!str)
    {
        notify_fail("Co chcesz udokumentowa�? Zobacz '?sdoc'\n");
        return 0;
    }

    argv = explode(str, " ");
    argc = sizeof(argv);

    switch (argv[0])
    {
    case "-r":
        DOCMAKER->doc_reset();
        write("Ju�.\n");
        return 1;
        break;
    case "-?":
        write(DOCMAKER->doc_query_status());
        ord = DOCMAKER->doc_query_orders();
        for (i = 0; i < sizeof(ord); i++)
        {
            write(ord[i][1] + " in " + ord[i][0] + " by: " +
                  implode(ord[i][2], ", ") + "\n");
        }
        return 1;
        break;
    default:
        if (argc < 2)
        {
            notify_fail("Niepoprawna sk^ladnia, zobacz '?sdoc'.\n");
            return 0;
        }

        argv[1] = FTPATH(this_player()->query_path(), argv[1]);
        if (sizeof(explode(" " + argv[1] + " ", "*")) == 1)
        {
            files = ({ argv[1] });
            path = "";
        }
        else
        {
            files = get_dir(argv[1]);

            if (sizeof(files))
                files -= ({ ".", ".." });

            if (sizeof(parts = explode(argv[1], "/")) > 1)
            {
                path = "/" + implode(parts[0..sizeof(parts) - 2], "/") + "/";
            }
            else
                path = "/";
        }

        if (!sizeof(files))
        {
            notify_fail("No such file(s): " + argv[1] + "\n");
            return 0;
        }

        write("The Docscribe will be told of your request.\n");

        for (i = 0; i < sizeof(files); i++)
        {
            if (file_size(path + files[i]) != -2)
                DOCMAKER->doc_file(argv[0], path + files[i]);
            else
                write(path + files[i] + " jest katalogiem.\n");
        }
        return 1;

        break;
    }
}

/* **************************************************************************
 * shutdown - shut the game down
 */
/* **************************************************************************
 * shutdown - shut the game down
 */
nomask int
shutdown_game(string str)
{
    string *argv;
    int     grace;

    CHECK_SO_WIZ;

    if (!stringp(str))
    {
        write("Nie podano argument^ow. Zobacz '?shutdown'.\n");
        return 1;
    }
    // Przeniosl by to ktos do soula mage'a...
    if (SECURITY->query_wiz_rank(geteuid(this_interactive())) < WIZ_MAGE)
    {
        notify_fail("Nie jeste� do tego upowa�nion"+this_player()->koncowka("y","a")+
	            ".\n");
        return 1;
    }

    argv = explode(str, " ");
    if (sizeof(argv) == 1)
    {
        switch(argv[0])
        {
        case "abort":
            SECURITY->cancel_shutdown();
            return 1;

        case "runlevel":
            if (grace = SECURITY->query_runlevel())
            {
                write("Runlevel: " + WIZ_RANK_NAME(grace) + " (i wy�ej).\n");
            }
            else
            {
                write("Gra jest aktualnie otwarta dla wszystkich graczy.\n");
            }
            return 1;
        case "autoload":
            if (SECURITY->query_auto_load_rooms()) {
                write("Auto-�adowanie pomieszcze� jest w��czone.\n");
            }
            else {
                write("Auto-�adowanie pomieszcze� jest wy��czone.\n");
            }
            return 1;
        }
        grace = atoi(argv[0]);
        if ((grace >= 1) ||
                (argv[0] == "now"))
        {
            SECURITY->request_shutdown("", grace);
            return 1;
        }

        notify_fail("B��dny argument dla shutdown: " + str +
            "\nZobacz '?shutdown' dla uzyskania pomocy.\n");
        return 0;
    }

    grace = atoi(argv[0]);
    if ((grace >= 1) ||
        (argv[0] == "now"))
    {
        SECURITY->request_shutdown(implode(argv[1..], " "), grace);
        return 1;
    }

    if (argv[0] == "runlevel")
    {
        if (argv[1] == "all")
        {
            argv[1] = WIZNAME_MORTAL;
        }

        if ((grace = member_array(argv[1], WIZ_N)) == -1)
        {
            if ((grace = member_array(argv[1], WIZ_S)) == -1)
            {
                notify_fail("Nie ma rangi nazwanej '" + argv[1] + "'.\n");
                return 0;
            }
        }

        /* Runlevel keeper -> runlevel arch. */
        SECURITY->set_runlevel((grace == WIZ_KEEPER) ? WIZ_ARCH : grace);
        write("Runlevel zmieniony na " + argv[1] + " (i wy�ej).\n");
        return 1;
    }
    if (argv[0] == "autoload") {
        if (argv[1] == "on") {
            SECURITY->set_auto_load_rooms(1);
            write("Auto-�adowanie pomieszcze� w��czone.\n");
            return 1;
        }
        else if (argv[1] == "off") {
            SECURITY->set_auto_load_rooms(0);
            write("Auto-�adowanie pomieszcze� wy��czone.\n");
            return 1;
        }
        else {
            notify_fail("U�ycie: shutdown autoload [on|off]\n");
            return 0;
        }
    }

    notify_fail("B��dny argument dla shutdown: " + str +
        "\nZobacz '?shutdown' dla uzyskania pomocy.\n");
    return 0;
}

/* **************************************************************************
 * snoop - snoop someone
 */
nomask int
snoop_on(string str)
{
    object on, by;
    string *argv;
    int    argc;

    CHECK_SO_WIZ;

    /* No argument, try to terminate the current snoop. */
    if (!stringp(str))
    {
        if (snoop(this_interactive()))
        {
            write("Snoop wstrzymany.\n");
        }
        else
        {
            write("Nie uda^lo si^e wstrzyma^c snoopa.\n");
        }
        return 1;
    }

    argv = explode(str, " ");

    if (sizeof(argv) == 1)
    {
        on = find_player(argv[0]);
        if (!on)
        {
            write("Nie ma takiej istoty.\n");
            return 1;
        }

        if ((object)SECURITY->query_snoop(on))
        {
            write(capitalize(argv[0]) + " jest ju� snoopowany.\n");
            return 1;
        }

        if (snoop(this_interactive(), on))
        {
            write("Snoopujemy " + capitalize(argv[0]) + ".\n");
            return 1;
        }
        else
        {
            write("Nie uda�o si� snoopowa� " + capitalize(argv[0]) + ".\n");
        }

        return 1;
    }

    if (argv[0] == "break")
    {
        on = find_player(argv[1]);
        if (!on)
        {
            write("Nie ma takiej istoty.\n");
            return 1;
        }
        if (on->query_wiz_level() && snoop(on))
        {
            tell_object(on, capitalize((string)this_interactive()->
                query_real_name()) + " z�ama�/a twojego snoopa.\n");
            write("Z�amany snoop przez " + capitalize(argv[1]) + ".\n");
        }
        else
        {
            write("Nie uda�o si� z�ama� snoopa na " + capitalize(argv[1]) + ".\n");
        }
        return 1;
    }

    by = find_player(argv[0]);
    on = find_player(argv[1]);
    if (!on)
    {
        write("Nie ma takiej istoty: " + capitalize(argv[0]) + ".\n");
        return 1;
    }
    if (!by)
    {
        write("Nie ma takiej istoty: " + capitalize(argv[1]) + ".\n");
        return 1;
    }
    if (snoop(by, on))
    {
        tell_object(by, "Snoopujesz " + capitalize(argv[1]) + ".\n");
        write(capitalize(argv[0]) + " w�a�nie snoopuje " +
              capitalize(argv[1]) + ".\n");
    }
    else
    {
        write("Nie uda�o si� ustawi� snoopa na " + capitalize(argv[1]) + " przez " +
              capitalize(argv[0]) + ".\n");
    }

    return 1;
}

#define STAT_PLAYER      0x1
#define STAT_LIVING      0x2
#define STAT_OBJECT      0x4

/*
 * Nazwa funkcji: show_stat
 * Opis         : This function actually prints the stat-information about
 *                the object to the wizard.
 * Argumenty    : object ob   - the object to stat.
 *                int    what - flags to see what information to display.
 * Zwraca       : int 1 - always.
 */
nomask int
show_stat(object ob, int what)
{
    string tmp, str = "";
    /*
    if (what & STAT_PLAYER)
        str += ob->stat_playervars();
     */
    if (what & STAT_LIVING)
        str += ob->stat_living();
    if (what & STAT_OBJECT)
        str += ob->stat_object();
    write(str);

    if (what & STAT_PLAYER)
    {
        if (ob != this_player() &&
            !ob->query_wiz_level() &&
            SECURITY->query_wiz_rank(this_player()->query_real_name()) < WIZ_LORD)

            SECURITY->log_syslog("STAT", sprintf("%s: %10s -> %-10s\n",
                ctime(time()), this_player()->query_real_name(),
                ob->query_real_name()));
    }

    return 1;
}

/* **************************************************************************
 * stat - displays stats of something
 */
nomask int
stat(string str)
{
    object ob;
    int    result;

    CHECK_SO_WIZ;

    if (!stringp(str))
        return show_stat(this_interactive(), STAT_PLAYER | STAT_LIVING);

    if (ob = find_player(str))
        return show_stat(ob, STAT_PLAYER | STAT_LIVING);

    if ((ob = parse_list(str)) ||
        (ob = present(str, environment(this_interactive()))))
    {
        if (living(ob))
            return show_stat(ob, STAT_LIVING);
        return show_stat(ob, STAT_OBJECT);
    }

    if (ob = find_living(str))
    {
        return show_stat(ob, STAT_LIVING);
    }

    if (ob = present(str, this_interactive()))
        return show_stat(ob, STAT_OBJECT);

    if (ob = (object)SECURITY->finger_player(str))
    {
        result = show_stat(ob, STAT_PLAYER | STAT_LIVING);
        ob->remove_object(); /* destruct phony players!!! */
        return result;
    }

    notify_fail("Nie ma takiej rzeczy.\n");
    return 0;
}

nomask int
skillstat(string str)
{
    object ob;

    CHECK_SO_WIZ;

    if (!stringp(str))
    {
        ob = this_interactive();
    }
    else if ((!objectp(ob = find_player(str))) &&
        (!(objectp(ob = present(str, environment(this_interactive())))) ||
            (!living(ob))) &&
        (!objectp(ob = find_living(str))))
    {
        notify_fail("Nie ma takiej istoty.\n");
        return 0;
    }

    write(ob->skill_living());
    return 1;
}

/* **************************************************************************
 * tellall - tell something to everyone
 */
nomask int
tellall(string str)
{
    string name_mia = this_player()->query_real_name(PL_MIA);
    string name_cel = this_player()->query_real_name(PL_CEL);
    int index = -1;
    int size;
    object *list;
    string *names;

    /* We do NOT add the wizard-check here since Armageddon should be
     * able to use the function.
     */

    if (!stringp(str))
    {
        notify_fail("Tellall co?\n");
        return 0;
    }

    list = users() - ({ this_player(), 0 });
    list -= (object *)QUEUE->queue_list(0);
    size = sizeof(list);
    while(++index < size)
    {
        if (!(list[index]->query_prop(WIZARD_I_BUSY_LEVEL) & BUSY_T) ||
            (name_mia == "armageddon"))
        {
            if (list[index]->query_wiz_level())
            {
                tell_object(list[index], capitalize(name_mia) +
                    " przekazuje wszystkim: " + str + "\n");
            }
            else
            {
                tell_object(list[index], "Nagle, tu^z przed tob^a, z chmury "+
                    "dymu wy^lania si^e wizerunek " +
                    this_player()->query_imie(list[index], PL_DOP) + ".\n" +
                    "W twym umy^sle d^xwi^eczy " +
                    this_player()->koncowka("jego", "jej") + " g^los: " +
                    str + "\n" + "Dym ponownie osnuwa posta^c i po chwili " +
                    "rozwiewa si^e, pozostawiaj^ac tylko wspomnienie po tej " +
                    "dziwnej wizycie.\n\n");
            }

            names = list[index]->query_prop(PLAYER_AS_REPLY_WIZARD);
            if (pointerp(names))
            {
                names = ({ name_cel }) + (names - ({ name_cel }) );
            }
            else
            {
                names = ({ name_cel });
            }
            list[index]->add_prop(PLAYER_AS_REPLY_WIZARD, names);
        }
    }

    if (this_player()->query_option(OPT_ECHO))
        write("Przekazujesz wszystkim: " + str + "\n");
    else
        write("Ju�.\n");
    return 1;
}

/* **************************************************************************
 * toolsoul - affect your tool souls
 */
nomask int
toolsoul(string str)
{
    string *tool_list;
    int index, size;

    CHECK_SO_WIZ;

    tool_list = (string *)this_interactive()->query_tool_list();
    if (!stringp(str))
    {
        index = -1;
        size = sizeof(tool_list);
        write("Aktualne tool souls:\n");
        while(++index < size)
        {
            write(sprintf("%2d: %s\n", (index + 1), tool_list[index]));
        }
        return 1;
    }

    str = FTPATH((string)this_interactive()->query_path(), str);

    if (member_array(str, tool_list) >= 0)
    {
        if (this_interactive()->remove_toolsoul(str))
            write("Removed " + str + ".\n");
        else
            write("Failed on " + str + ".\n");
        return 1;
    }

    if (this_interactive()->add_toolsoul(str))
        write("Added " + str + ".\n");
    else
        write("Failed on " + str + ".\n");
    return 1;
}

/* **************************************************************************
 * tradestat - Stat the trade in a room
 */
nomask int
tradestat()
{
    if (!function_exists("stat_trade", environment(this_player())))
    {
        notify_fail("No obvious trade in this room.\n");
        return 0;
    }

    write(environment(this_player())->stat_trade());
    return 1;
}

/* **************************************************************************
 * trans - teleport someone somewhere
 */
nomask int
trans(string str)
{
    object          ob;
    string *args;
    int reverse;
    mixed lokacja;

    CHECK_SO_WIZ;

    if (!stringp(str))
    {
        notify_fail("Trans kogo?\n");
        return 0;
    }

    if (SECURITY->query_wiz_rank(geteuid(this_interactive())) < WIZ_MAGE)
    {
        notify_fail("Nie jeste� do tego upowa�nion"+this_player()->koncowka("y","a")+
	            ".\n");
        return 0;
    }

    args = explode(str, " ");

    if (args[0] == "-r")
    {
        if (sizeof(args) == 1)
        {
            notify_fail("Trans kogo z powrotem?\n");
            return 0;
        }

        args = args[1..sizeof(args)];
        reverse = 1;
    }

    ob = find_living(args[0]);
    if (!ob)
    {
        notify_fail("Nie ma takiej istoty.\n");
        return 0;
    }

    if (!reverse)
    {
        ob->add_prop("_player_transed_from", MASTER_OB(environment(ob)));
        if (sizeof(args) > 1)
            lokacja = FTPATH(this_interactive()->query_path(), args[1]);
        else
            lokacja = environment(this_interactive());
    }
    else
    {
        if (ob->query_prop("_player_transed_from"))
        {

            lokacja = ob->query_prop("_player_transed_from");
            ob->remove_prop("_player_transed_from");
        }
        else
        {
            notify_fail(capitalize(args[0]) + " nie mo�e by� transed z powrotem.\n");
            return 0;
        }
    }

    if (SECURITY->query_wiz_rank(ob->query_real_name()))
        tell_object(ob, "Czujesz, jak " +
            capitalize(this_player()->query_real_name()) + " ci^e gdzie^s " +
            "przenosi.\n");
    else
        tell_object(ob, "Nagle czujesz, jak jaka^s magiczna si^la ci^e gdzie^s " +
            "przenosi.\n");

    ob->move_living("X", lokacja);


    return 1;
}


/* **************************************************************************
 * typolog - read the typolog
 */
nomask int
typolog(string str)
{
    return somelog(str, "typo log", "/typos");
}

/* **************************************************************************
 * vis - become visible
 */
nomask int
vis(string str)
{
    CHECK_SO_WIZ;
    
    if((TP->query_invis(1) && !TP->query_invis()))
    {
        if(!TP->query_invis(1))
        {
            notify_fail("Nie jeste� niewidoczny w 'kto'.\n");
	    return 0;
	}
        TP->set_invis(0, 1);
	write("Od teraz jeste� widoczny w 'kto'.\n");
	return 1;
    }

    if(str == "k")
    {
        if(TP->query_inivs())
        {
            notify_fail("Nie jeste� niewidzialn" + TP->koncowka("y", "a", "e") + ".\n");
	    return 0;
	}
	TP->set_invis(0);
	write("Ujawniasz si�. Juz nie jeste� niewidoczn" + TP->koncowka("y", "a", "e") + ". "+ 
            "Pozostajesz jednak niewidoczny w 'kto'.\n");
	saybb(QCIMIE(TP, PL_MIA) + " " + TP->query_mm_in() + "\n");
	return 1;
    }

    if (!(TP->query_invis()) && !(TP->query_invis(1)))
    {
	notify_fail("Nie jeste� niewidzialn" + TP->koncowka("y", "a", "e") + ".\n");
        return 0;
    }
    if(TP->query_invis())
        TP->set_invis(0);
    if(TP->query_invis(1))
	TP->set_invis(0, 1);
    
    write("Ujawniasz si�. Ju� nie jeste� niewidoczn" + TP->koncowka("y", "a", "e")+
             ".\n");
    saybb(QCIMIE(TP, PL_MIA) + " " + TP->query_mm_in() + "\n");
    return 1;
}

/*
 * Nazwa funkcji: somelog
 * Opis         : Look at error logs
 */
static int
somelog(string str, string logname, string log)
{
    string file, rem;

    if (!stringp(str))
        str = (string)this_interactive()->query_real_name();
    else if (sscanf(str, "-r %s", rem) == 1)
    {
        str = rem;
    }
    else if (str == "-r")
    {
        str = (string)this_interactive()->query_real_name();
        rem = str;
    }
    else
        rem = 0;

    file = (string)SECURITY->query_wiz_path(str) + "/log";

    if (!strlen(file))
        return (notify_fail("No " + logname + " for: " + str + ".\n"),0);

    file += log;

    if (file_size(file) < 0)
        return (notify_fail("Can't find: " + file + ".\n"),0);

    if (!rem)
    {
        write(capitalize(logname) + ": " + file + "\n");
        this_interactive()->command("tail " + file);
    }
    else
    {
        write("Removing " + capitalize(logname) + ": " + file + "\n");
        this_interactive()->command("rm " + file);
    }
    return 1;
}
