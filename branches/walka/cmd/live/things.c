/*
 * /cmd/live/things.c
 *
 * General commands for manipulating things.
 * X oznacza komend� wycofan�
 *
 * - appraise	X
 * - count	X
 * - drop	X
 * - examine	X
 * - exa	X
 * - get	X
 * - give	X
 * - hide	X
 * - i		- zamienione na inwentarz
 * - inventory	X
 * - keep	X
 * - l		X
 * - look	X
 * - peek	X
 * - pick	X
 * - put	X
 * - reveal	X
 * - search	X
 * - sneak	X
 * - take	X
 * - track	X
 * - unkeep	X
 */

#pragma no_clone
#pragma no_inherit
#pragma save_binary
#pragma strict_types

inherit "/cmd/std/command_driver";

#include <adverbs.h>
#include <composite.h>
#include <cmdparse.h>
#include <filter_funs.h>
#include <formulas.h>
#include <language.h>
#include <macros.h>
#include <ss_types.h>
#include <std.h>
#include <stdproperties.h>
#include <subloc.h>
#include <options.h>

#include "/cmd/live/things/rzuc.c"

#define PREV_LIGHT CAN_SEE_IN_ROOM(this_player()) 

/*
 * Prototypes
 */
int visibly_hold(object ob);
int manip_set_dest(string prep, object *carr);
int manip_set_odloz_dest(string prep, object *carr);
varargs int visible(object ob, object cobj);
int sublokacja_pasuje(object ob);

/*
 * Global variables
 */
static int silent;                 /* silent flag if person did 'get/drop all' */
static object gDest;               /* destination use for put and give */
static object *gContainers;        /* array of containers to try */
static object *gFrom;              /* array of objects where player did get things */
static object gHolder;
static string gItem;               /* string to hold pseudoitem from look command */
static string gBezokol = "";       /* bezokolicznik */
static string gMiejsce = "";       /* sublokacja, gdzie przemieszczamy rzeczy */
static string gFromMiejsce = "";   /* sublokacja, sk�d przemieszczamy rzeczy */
static string gPrep = "";          /* przyimek u�ywany w komendzie */
static mixed gPreviousSubloc = ""; /* sublokacja, z kt�rej bierzemy przedmiot */
static int gTypSublokacji = 0;     /* typ sublokacji, jakiej poszukujemy */
static int gPisacODest = 0;        /* czy funkcje maj� wypisywa� informacje o docelowym obiekcie */

static mapping sneak_dirs = ([
				"n"  : "p�noc",
				"ne" : "p�nocny-wsch�d",
				"e"  : "wsch�d",
				"se" : "po�udniowy-wsch�d",
				"s"  : "po�udnie",
				"sw" : "po�udniowy-zach�d",
				"w"  : "zach�d",
				"nw" : "p�nocny-zach�d",
				"u"  : "g�r�",
				"d"  : "d�",
			     ]);



void
create()
{
    seteuid(getuid(this_object())); 
}

/* **************************************************************************
 * Return a proper name of the soul in order to get a nice printout.
 */
string
get_soul_id()
{
    return "things";
}

/* **************************************************************************
 * This is a command soul.
 */
int
query_cmd_soul()
{
    return 1;
}

/* **************************************************************************
 * The list of verbs and functions. Please add new in alfabetical order.
 */
mapping
query_cmdlist()
{
    return ([
        "czas"          : "czas",

        "daj"           : "daj",

        "i"             : "inwentarz",
        "inwentarz"     : "inwentarz",

        "obejrzyj"      : "obejrzyj",
        "ob"            : "obejrzyj",
        "oce�"          : "ocen",
        "odbezpiecz"    : "zabezpiecz",
        "od��"         : "odloz",
        "powie�"        : "odloz",

        "podejrzyj"     : "podejrzyj",
        "poka�"         : "pokaz",
        "policz"        : "policz",
        "po��"         : "odloz",
        "przemknij"     : "przemknij",
        "przeszukaj"    : "przeszukaj",

        "rzu^c"         : "rzuc",

        "schowaj"       : "ukryj",
        "sp"            : "spojrz",
        "sp�jrz"        : "spojrz",
        "szukaj"        : "szukaj",

        "trop"          : "trop",

        "ujawnij"       : "ujawnij",
        "ukryj"         : "ukryj",

        "we�"           : "wez",
        "zdejmij"       : "wez",
        "wyjmij"        : "wez",
        "w��"          : "wloz",

        "zabezpiecz"    : "zabezpiecz",
        "zerknij"       : "zerknij"
        ]);
}

/*
 * Function name: using_soul
 * Description:   Called once by the living object using this soul. Adds
 *		  sublocations responsible for extra descriptions of the
 *		  living object.
 */
public void 
using_soul(object live)
{
/*
    live->add_subloc(SUBLOC_MISCEXTRADESC, file_name(this_object()));
    live->add_textgiver(file_name(this_object()));
*/
}

/* **************************************************************************
 * Funkcje pomocnicze.
 * **************************************************************************/

/*
 * Sortuje obiekty pod wzgl�dem przydatno�ci do relokacji.
 * W pierwszej kolejno�ci id� przedmioty b�d�ce bezpo�rednio
 * w inwentarzu gracza, na ko�cu przedmioty dobyte, za�o�one i inne.
 */
object *
sort_items(object *x)
{
    object *y = filter(x, operator(!) @ &->query_subloc()); // obiekty nie w sublokach

    return y + (x - y);
}

int
kobieta()
{
    return (this_player()->query_rodzaj() == PL_ZENSKI);
}

/*
 * We fail to do something because it is dark
 */
int
light_fail(string str = "dostrzec cokolwiek")
{
    string prop = environment(this_player())->query_prop(ROOM_S_DARK_MSG);

    notify_fail((prop ? prop : "Jest zbyt ciemno") + ", by m�c " + str
	      + ".\n");
    return 0;
}

/*
 * Function name: move_err_short
 * Description:   Translate move error code to message and prints it.
 * Arguments:	  ierr: move error code
 *		  ob:   object
 * Outputs:	  Message string.
 *
 * Error message examples:
	"The bag is too heavy."
	"The bird can not be dropped."
	"The pearl can not be removed."
	"The ankheg refuses."
	"The chest is full."
	"The river can not be taken."
 *
 * Ref: 	  see /std/object.c for the move function 
 */
varargs void 
move_err_short(int ierr, object ob, object dest)
{
    mixed str, str2;
    string shortdesc;
    int rodzaj;

    if (silent || ierr <= 0 || ierr >= 12 || (ierr == 7) || !objectp(ob) ||
	ob->query_no_show())
	return;

    str = "   ";
    rodzaj = ob->query_rodzaj();

    switch (ierr)
    {
    case 1:
	if (ob->query_prop(HEAP_I_IS) && ob->num_heap() > 1)
	    str += capitalize(ob->query_pname(0, PL_MIA)) + " s� zbyt ci�" + 
	    ob->koncowka(0, 0, 0, "cy", "kie") + ".\n";
	else
	    str += capitalize(ob->short(PL_MIA)) + 
		(ob->query_tylko_mn() ? " s�" : " jest") + " zbyt ci�" + 
		ob->koncowka("ki", "ka", "kie", "cy", "kie") + ".\n";
	break;
    case 2:
	str2 = ob->query_prop(OBJ_M_NO_DROP);
	if (!stringp(str2))
	    str += "Nie mo�esz porzuci� " + ob->short(PL_DOP) + ".\n";
	else
	    str = str2;
	break;
    case 3:
	if (query_verb() == "daj")
	{
	    str2 = ob->query_prop(OBJ_M_NO_GIVE);
	    if (!stringp(str2))
		str += "Nie mo�esz odda� " + ob->short(PL_DOP) + ".\n";
	    else
		str = str2;
	    break;
	}
	if (dest)
	    str2 = environment(ob)->query_prop(CONT_M_NO_REM);
	if (!stringp(str2))
	    str += "Nie mo�esz wyj�� " + ob->short(PL_DOP) + ".\n";
	else
	    str = str2;
	break;
    case 4:
	str2 = ob->query_prop(OBJ_M_NO_INS);
	if (!stringp(str2))
	    str += capitalize(ob->short(PL_MIA)) + " nie chce wej��.\n";
	else
	    str = str2;
	break;
    case 5:
	if (dest)
	    str2 = dest->query_prop(CONT_M_NO_INS);
	if (!dest->query_prop(CONT_I_IN))
	    str += "Nie mo�esz tam w�o�y� " + ob->short(PL_DOP) + ".\n";
	else
	    str = str2;
	break;
    case 6:
	str2 = ob->query_prop(OBJ_M_NO_GET);
	if (!stringp(str2))
	    str += "Nie mo�esz wzi�� " + ob->short(PL_DOP) + ".\n";
	else
	    str = str2;
	break;
    case 8:
	str += capitalize(ob->short(PL_MIA)) + " " + 
	    (ob->query_tylko_mn() ? "s�" : "jest") + " zbyt " +
	    ob->koncowka("du�y", "du�a", "du�e", "duzii", "du�e");
	    
	str += ".\n";
	break;
    case 9:
	str += "Nie mo�esz wyj�� " + ob->short(PL_DOP) + " z zamkni�tego " +
	    "pojemnika.\n";
	break;
    case 10:
	str += "Nie jeste� w stanie w�o�y� " + ob->short(PL_DOP) + 
	    " do zamkni�tego pojemnika.\n";
    	break;
    case 11:
	str += capitalize(ob->short(PL_MIA)) + " ju� si� nie " +
	       (ob->query_tylko_mn() ? "zmieszcz�" : "zmie�ci") + ".\n";
    }

    write(str);
}
   
/*
 * Function name: manip_drop_access
 * Description:   Test if player carries an object
 * Arguments:	  ob: object
 * Returns:       1: caried object
 *		  0: otherwise
 *
 */
int
manip_drop_access(object ob)
{
    if (!objectp(ob))
	return 0;
    if (environment(ob) == this_player())
	return 1;
    return 0;
}

/*
 * Function name: manip_give_access
 * Description  : This function is called to see whether the player can
 *		actually give this object. To be able to give an object
 *		to another player, you must first have it yourself.
 * Arguments    : object ob - the object to give.
 * Returns      : int 1/0 - true if the player has the object in his
 *			  inventory.
 */
int
manip_give_access(object ob)
{
    if (!objectp(ob))
    {
	return 0;
    }

    return (environment(ob) == this_player());
}

/**
 * Funkcja filtruj�ca sublokacje pasuj�ce do podanej nazwy.
 *
 * @param mixes sublok - rozpatrywana sublokacja
 * @param sloc - nazwa sublokacji
 * @param przyp - przypadek, w kt�rym testujemy sublokacje
 *
 * @return <ul>
 *           <li> <b>1</b> sublokacja pasuje do podanej nazwy
 *           <li> <b>0</b> sublokacja <b>nie</b> pasuje do podanej nazwy
 *         </ul>
 */

nomask varargs int 
subloc_filter(mixed sublok, mixed sloc, int przyp = 0)
{
  /* sublokacja jest nowego typu */
  if (pointerp(sublok)) {
//    write("subloc_filter: pointerp: " + sloc + " ~= " + sublok[przyp] + "\n");
    return (sloc ~= sublok[przyp]);
  }
  
  /* sublokacja jest tradycyjnego typu */
//  write("subloc_filter: normal: " + sloc + " ~= " + sublok + "\n");
  return (sloc ~= sublok);
}

/*
 * Function name: manip_relocate_to
 * Description:   Test if an object can be moved to another one and do it.
 * Arguments:	  item_o: object to be moved
 *		  to: object to move to
 * Returns:       1: object moved
 *		  0: otherwise
 * Outputs:	  move error messages
 * Notify_fail:   ""  (no notify fail because of output of error messages)
 *
 */
varargs int
manip_relocate_to(object item_o, object to)
{
  object dest;	/* receiver */
  int ierr;
  string destmsg;
  string errormsg;
  string errormsgsay;
  string miejsce = "";
  mixed destSublok;
  int przyp = 0;
  int directMove = 0;

  //    write("manip_relocate_to\n");

  if (!objectp(item_o))
    return 0;
  if (!to)
  {
    dest = environment(this_player());
    destmsg = "";
  }
  else
  {
    dest = to;
    /* jeremian's changes */
    destmsg = (gBezokol ~= "da�" ? " " + QSHORT(dest, PL_CEL)
        : ( gPrep == "do" ? " do " + QSHORT(dest, PL_DOP)
          : (gBezokol ~= "od�o�y�" ? " " + gPrep + " " + QSHORT(dest, PL_BIE)
            : " " + gPrep + " " + QSHORT(dest, PL_MIE) )));
    /* --- */
  }

  if (item_o == dest)
    return 0;		/* not into itself */
  if (item_o == this_player())
    return 0;		/* not him self */

  if (item_o->query_prop(HEAP_I_IS) && query_verb() == "ukryj")
    item_o->set_no_merge(1);

  /* jeremian's changes */

  //    write("Cel=[" + gMiejsce + "]\n");
  //    write("Przyimek=[" + gPrep + "]\n");

  if (gMiejsce == "") {
      if (!(ierr = item_o->move(dest))) {
          if (item_o->query_prop(HEAP_I_IS) && query_verb() == "ukryj")
              item_o->restore_no_merge();
          return 1;
      }
  }
  else {
//    write("else\n");
    if (gMiejsce == "do" || gMiejsce == "w" || gMiejsce == "we") {
      directMove = 1;
    }
    //    write("miejsce=[" + miejsce + "]\n");
    if ((gMiejsce == "pod") || (gMiejsce == "na")) {
      miejsce = gMiejsce;
    }
    else {
      miejsce = gMiejsce[(strlen(gPrep)+1)..];
    }
    //    write("miejsce=[" + miejsce + "]\n");
    if (gBezokol ~= "od�o�y�") {
      if (gPrep == "w") {
        przyp = PL_DOP;
      }
      else {
        przyp = PL_BIE;
      }
    }
    else if (gBezokol ~= "po�o�y�") {
      if (gPrep == "pod") {
        przyp = PL_NAR;
      }
      else {
        przyp = PL_MIE;
      }
    }
    else if (gBezokol ~= "powiesi�") {
      przyp = PL_MIE;
    }
    else if (gBezokol ~= "w�o�y�") {
      przyp = PL_DOP;
    }
    if (!directMove) {
      if (sizeof(destSublok = filter(dest->query_sublocs(), &subloc_filter(, miejsce, przyp))) < 1) {
        //        write(">>> 1\n");
        write("Gdzie chcesz " + gBezokol + " " + COMPOSITE_DEAD(item_o, PL_BIE) + "?\n");
        silent = 1;
        if (item_o->query_prop(HEAP_I_IS) && query_verb() == "ukryj")
            item_o->restore_no_merge();
        return 0;
      }
      /*
       * Dopuszczamy tylko sublokacje w nowym formacie, to znaczy tablice.
       * Jedynym wyj�tkiem s� sublokacje o nazwach "na" lub "pod".
       */
      if ((!stringp(destSublok[0]) || ((destSublok[0] != "na") && (destSublok[0] != "pod"))) &&
          (!pointerp(destSublok[0]))){
        //        write(">>> 2\n");
        write("Gdzie chcesz " + gBezokol + " " + COMPOSITE_DEAD(item_o, PL_BIE) + "?\n");
        silent = 1;
        if (item_o->query_prop(HEAP_I_IS) && query_verb() == "ukryj")
            item_o->restore_no_merge();
        return 0;
      }
      /* Teraz w sublokacjach w nowym formacie mo�na definiowa� przyimek. */
      if (pointerp(destSublok[0]) && (sizeof(destSublok[0]) == 7)) {
        if (!(gPrep ~= destSublok[0][6])) {
          write("Gdzie chcesz " + gBezokol + " " + COMPOSITE_DEAD(item_o, PL_BIE) + "?\n");
          silent = 1;
        if (item_o->query_prop(HEAP_I_IS) && query_verb() == "ukryj")
            item_o->restore_no_merge();
          return 0;
        }
      }
    }
    else {
      destSublok = ({ 0 });
    }
    if ( /* Trzeba tutaj troszk� przypadk�w pouwzgl�dnia�: */
        /* 1. Na sublokacj� jest na�o�one ograniczenie dopuszczalnych przedmiot�w */
        (dest->is_subloc_prop_set(destSublok[0], SUBLOC_I_DLA_O) &&
         (dest->query_subloc_prop(destSublok[0], SUBLOC_I_DLA_O) != item_o->query_type())) ||
        /* 2. Komenda to 'odloz' a sublokacja nie jest 'na' */
        ((gBezokol ~= "od�o�y�") && (((dest->liberal_parse_subloc_type(destSublok[0])!=2)&&
                                      (dest->query_subloc_prop(destSublok[0], SUBLOC_I_MOZNA_ODLOZ) == 0) &&
                                      (dest->query_subloc_prop(destSublok[0], SUBLOC_I_TYP_NA) == 0)) ||
                                     (dest->query_subloc_prop(destSublok[0], CONT_I_CANT_ODLOZ)))) ||
        /* 3. Komenda to 'poloz' a sublokacja nie jest 'na' */
        ((gBezokol ~= "po�o�y�") && (((dest->liberal_parse_subloc_type(destSublok[0])!=2)&&
                                      (dest->query_subloc_prop(destSublok[0], SUBLOC_I_MOZNA_POLOZ) == 0) &&
                                      (dest->query_subloc_prop(destSublok[0], SUBLOC_I_TYP_NA) == 0)) ||
                                     (dest->query_subloc_prop(destSublok[0], CONT_I_CANT_POLOZ)))) ||
        /* 4. Trzeba explicite okre�li�, �e mo�na wiesza� na lokacji */
        ((gBezokol ~= "powiesi�") &&
         (dest->query_subloc_prop(destSublok[0], SUBLOC_I_MOZNA_POWIES) == 0)) ||
        /* 5. Komenda to 'wloz' a sublokacja nie jest 'pod' lub 'w' */
        ((gBezokol ~= "w�o�y�") && (dest->liberal_parse_subloc_type(destSublok[0]) != 1) &&
         (dest->liberal_parse_subloc_type(destSublok[0]) != 3) &&
         (dest->query_subloc_prop(destSublok[0], SUBLOC_I_MOZNA_WLOZ) == 0) &&
         (dest->query_subloc_prop(destSublok[0], SUBLOC_I_TYP_W) == 0) &&
         (dest->query_subloc_prop(destSublok[0], SUBLOC_I_TYP_POD) == 0))
        ) {
          /* FIXME: jeszcze du�o tutaj trzeba pozmienia�... */
//                    write("E) manip_relocate_to\n");
//                    write("item=[" + dest->query_short() + "]\n");
//                    write("gBezokol=[" + gBezokol + "]\n");
//                    write("miejsce=[" + miejsce + "]\n");
//                    write("typ=[" + dest->liberal_parse_subloc_type(destSublok[0]) + "]\n");
//                    write("---\n");

          errormsg = "Nie mo�na " + gBezokol + " " +
            COMPOSITE_DEAD(item_o, PL_DOP) + " " + gPrep +
            (pointerp(destSublok[0]) ? (" " + destSublok[0][przyp]) : "");
          errormsgsay = QCIMIE(this_player(), PL_MIA) +
            " bezskutecznie pr�buje " + gBezokol + " " +
            COMPOSITE_DEAD(item_o, PL_BIE) + " " + gPrep +
            (pointerp(destSublok[0]) ? (" " + destSublok[0][przyp]) : "");

          if (gPisacODest) {
            if (pointerp(destSublok[0])) {
              errormsg += " " + dest->short(PL_DOP);
              errormsgsay += " " + QSHORT(dest, PL_DOP);
            }
            else {
              switch (gPrep) {
                case "do": {
                  errormsg += " " + dest->short(PL_DOP);
                  errormsgsay += " " + QSHORT(dest, PL_DOP);
                  break;
                }
                case "pod": case "na": {
                  if ((gBezokol ~= "po�o�y�") || (gBezokol ~= "powiesi�")) {
                    errormsg += " " + dest->short(PL_MIE);
                    errormsgsay += " " + QSHORT(dest, PL_MIE);
                  }
                  else {
                    errormsg += " " + dest->short(PL_BIE);
                    errormsgsay += " " + QSHORT(dest, PL_BIE);
                  }
                  break;
                }
              }
            }
          }
          write(errormsg + ".\n");
          say(errormsgsay + ".\n");
          silent = 1;
        if (item_o->query_prop(HEAP_I_IS) && query_verb() == "ukryj")
            item_o->restore_no_merge();
          return 0;
        }
    else {
      //      write("S) manip_relocate_to\n");
      //      write("item=[" + dest->query_short() + "]\n");
      //      write("gBezokol=[" + gBezokol + "]\n");
      //      write("miejsce=[" + miejsce + "]\n");
      //      write("typ=[" + dest->liberal_parse_subloc_type(destSublok[0]) + "]\n");
      //      write("---\n");
      if (!(ierr = item_o->move(dest, destSublok[0]))) {
        if (item_o->query_prop(HEAP_I_IS) && query_verb() == "ukryj")
            item_o->restore_no_merge();
        return 1;
      }
      //      write("mrt: ---\n");
    }
  }
  /* --- */

  if (!silent && !item_o->query_no_show())
  {
    saybb(QCIMIE(this_player(), PL_MIA) + " bezskutecznie pr�buje " +
        gBezokol + " " + (stringp(item_o->short(PL_BIE)) ? 
          QSHORT(item_o, PL_BIE) : 
          item_o->query_name(PL_BIE)) + destmsg + ".\n",
        ({ this_player(), dest }) );

    if (living(dest))
      dest->catch_msg(QCIMIE(this_player(), PL_MIA) + " bezskutecznie " +
          "pr�buje ci " + gBezokol + " " + (stringp(item_o->short(PL_BIE)) ? 
            QSHORT(item_o, PL_BIE) : item_o->query_name(PL_BIE)) + 
          ".\n");
  }


  move_err_short(ierr, item_o, dest);
  notify_fail("");
        if (item_o->query_prop(HEAP_I_IS) && query_verb() == "ukryj")
            item_o->restore_no_merge();
  return 0;
}

int
manip_put_dest(object item) 
{ 
    return manip_relocate_to(item, gDest); 
}

/* jeremian's changes */

int
manip_set_odloz_dest(string bezokol, object *carr)
{ 
    string vb;

    if (!carr || sizeof(carr) == 0)
    {
	notify_fail(capitalize(gBezokol) + " " + gPrep + " " + (bezokol == "odloz" ?
		    "co" : "czym") + "?\n");
	return 0;
    }
    if (sizeof(carr) > 1)
    {
	notify_fail("Zdecyduj si�, gdzie chcesz to " + gBezokol + ".\n");
	return 0;
    }
    gDest = carr[0];

    if (living(gDest))
    {
	notify_fail(gDest->query_Imie(this_player(), PL_MIA) + 
	    " nie zgodzi si� na to.\n");
	return 0;
    }

    if (bezokol == "odloz")
    {
	    if (gMiejsce != "") {
	notify_fail("Co chcesz " +gBezokol+ " " +gPrep+ " " +gMiejsce[(strlen(gPrep)+1)..]+
		(gPisacODest ? " " + gDest->short(PL_DOP) : "") + "?\n");
	    }
	    else {
	notify_fail("Co chcesz " +gBezokol+ " " +gPrep+
		(gPisacODest ? " " + gDest->short(PL_MIA) : "") + "?\n");
	    }
	return 1;
    }
    else
    {
	    if (gMiejsce != "") {
	notify_fail("Co chcesz " +gBezokol+ " " +gPrep+ " " +gMiejsce[(strlen(gPrep)+1)..]+
		(gPisacODest ? " " + gDest->short(PL_DOP) : "") + "?\n");
	    }
	    else {
	notify_fail("Co chcesz " +gBezokol+ " " +gPrep+
		(gPisacODest ? " " + gDest->short(PL_MIE) : "") + "?\n");
	    }
	return 1;
    }
}

/* --- */

int
manip_set_dest(string prep, object *carr)
{ 
    string vb;

    if (!carr || sizeof(carr) == 0)
    {
	    /* jeremian's changes */
	notify_fail(capitalize(gBezokol) + " " + (prep == "w" ?
		    "w czym" : (prep == "pod" ? "pod co" : prep + " czego")) + "?\n");
	/* --- */
	return 0;
    }
    if (sizeof(carr) > 1)
    {
	notify_fail("Zdecyduj si�, gdzie chcesz to " + gBezokol + ".\n");
	return 0;
    }
    gDest = carr[0];

    if (living(gDest))
    {
	notify_fail(gDest->query_Imie(this_player(), PL_MIA) + 
	    " nie zgodzi si� na to.\n");
	return 0;
    }

/*
    if (parse_command(prep, ({0}), "'w' / 'do' / 'wewnatrz'"))
 */

    /* jeremian's changes */
    if ((prep == "do") || (prep == "pod"))
    /* --- */
    {
	notify_fail("Co chcesz " + gBezokol + " " + prep + " " +
/*
		    gDest->short((prep == "w" ? PL_BIE : PL_DOP)) + "?\n");
 */
		    gDest->short(PL_DOP) + "?\n");
	return 1;
    }
    else
    {
	notify_fail("Nie rozumiem, co to znaczy w�o�y� " + prep +".\n");
	return 0;
    }
}

/**
 * Funkcja przenosz�ca obiekt do gracza.
 *
 * Funkcja wypisuje informacje o b��dach. Ponadto ustawia wtedy notify_fail
 * na pusty napis, by ukry� niepowodzenie.
 * 
 * @param item_o przenoszony obiekt
 *
 * @return <ul>
 *           <li> <b>1</b> obiekt zosta� przeniesiony
 *           <li> <b>0</b> nie uda�o si� przenie�� obiektu
 *         </ul>
 */
int
manip_relocate_from(object item_o)
{
  int ierr;
  object env;
  string tmp;
  string errormsg;
  string errormsgsay;

  if (!objectp(item_o))
    return 0;
  if (item_o == this_player())
    return 0;
  env = environment(item_o);
  if (env == this_player())
    return 0;

  if (env->query_prop(CONT_I_HIDDEN) ||
      (env->query_prop(CONT_I_CLOSED) && intp(item_o->query_subloc()) && (item_o->query_subloc() == 0)))
  {
    tmp = " " + gBezokol + " " + QSHORT(item_o, PL_DOP) + " " + gPrep + " ";

    if (living(env))
    {
      tell_object(env, this_player()->query_imie(env, PL_MIA) +
          " bezskutecznie pr�buje" + tmp + " ci�.\n");
    }
    saybb(QCIMIE(this_player(), PL_MIA) + " bezskutecznie pr�buje" + tmp + 
        QSHORT(env, PL_DOP) + ".\n", ({ env, this_player() }) );
    this_player()->catch_msg("Nie udaje ci si�" + tmp + 
        QSHORT(env, PL_DOP) + ".\n");
    if (env->query_prop(CONT_I_CLOSED))
    {
      tmp = capitalize(env->short(PL_MIA)) + " " + (env->query_tylko_mn()
          ? "s�" : "jest") + " zamkni�" + env->koncowka("ty", "ta", "te",
            "ci", "te");

      write(tmp + ".\n");
    }
    notify_fail("");
    return 0;
  }
  
  if (!(intp(item_o->query_subloc()) && (item_o->query_subloc() == 0)) &&
      env->query_subloc_prop(item_o->query_subloc(), CONT_I_CLOSED))
  {
    write(">>> FIXME <<<\n");

    notify_fail("");
    return 0;
  }

  if (item_o->query_prop(HEAP_I_IS))
    item_o->set_no_merge(1);

  /* je�eli gFromMiejsce jest puste, to po prostu przenosimy obiekt do gracza */
  if (gFromMiejsce == "") {
    if ((ierr = item_o->move(this_player())) == 0)
    {
      gPreviousSubloc = "";
      gFrom = gFrom + ({env});
      if (item_o->query_prop(HEAP_I_IS))
        item_o->restore_no_merge();
      return 1;
    }
  }
  else {
    /* w innym wypadku zdajemy si� na funkcj� sublokacja_pasuje() */
    if (sublokacja_pasuje(item_o)) {
      mixed tmpPreviousSubloc = item_o->query_subloc();
      if ((ierr = item_o->move(this_player())) == 0)
      {
        gPreviousSubloc = tmpPreviousSubloc;
        gFrom = gFrom + ({env});
        if (item_o->query_prop(HEAP_I_IS))
          item_o->restore_no_merge();
        return 1;
      }
      gFromMiejsce = item_o->query_subloc();
    }
    else {
      if (item_o->query_prop(HEAP_I_IS))
        item_o->restore_no_merge();
      return 0;
    }
  }

  if (!silent && !item_o->query_no_show())
  {
    if (living(item_o))
      item_o->catch_msg(this_player()->query_Imie(item_o, PL_MIA) +
          " bezskutecznie pr�buje " + gBezokol + " ciebie.\n");
    saybb(QCIMIE(this_player(), PL_MIA) + " bezskutecznie pr�buje " +
        gBezokol + " " +
        (stringp(item_o->short(PL_BIE)) ? QSHORT(item_o, PL_BIE) :
         item_o->query_name(PL_BIE)) + ".\n", ({ item_o, this_player() }));
  }
  move_err_short(ierr, item_o, this_player());
  notify_fail("");
  if (item_o->query_prop(HEAP_I_IS))
    item_o->restore_no_merge();
  return 0;
}

/*
 * Function name: manip_put_whom
 * Description  : We only allow the object to be moved if this_player()
 *		  has it in his inventory. For additional information on
 *		  the move process and the return values, see the documents
 *		  on manip_relocate_to(item, gDest).
 * Arguments    : object item - the object to move.
 * Returns      : int 1/0 - it will fail if 'item' is not in this_player(),
 *			    else see manip_relocate_to().
 */
int
manip_put_whom(object item) 
{
    if (environment(item) != this_player())
    {
	return 0;
    }

    return manip_relocate_to(item, gDest);
}

int
manip_set_whom(object *carr) /* Argument prep usuniety */
{ 
    mixed tmp;
    int i;

    if (!carr || sizeof(carr) == 0)
	return 0;

    if (sizeof(carr) > 1)
    {
	notify_fail("Zdecyduj si�, komu chcesz to da�?\n");
	return 0;
    }

    gDest = carr[0];
    if (gDest->query_npc() &&
	(tmp = gDest->query_prop(NPC_M_NO_ACCEPT_GIVE)))
    {
	if (stringp(tmp))
    	notify_fail(gDest->query_Imie(this_player(), PL_MIA) + tmp);
	return 0;
    }

    notify_fail("Co chcesz " + gBezokol + " " +
	gDest->query_imie(this_player(), PL_CEL) + "?\n");
	
    return 1;
}

/**
 * Funkcja sprawdzaj�ca, czy obiekt jest w kt�rym� z kontener�w.
 *
 * Funkcja korzysta z globalnej tablicy <b>gContainers</b>.
 * 
 * @param ob sprawdzany obiekt
 *
 * @return <ul>
 *           <li> <b>1</b> jest w kontenerze
 *           <li> <b>0</b> nie jest w kontenerze
 *         </ul>
 */
int
in_gContainers(object ob) 
{
  mixed res;

  if (!objectp(ob))
    return 0;
  if (environment(ob) != gContainers[0] &&
      environment(ob) != gContainers[0]->query_room())
    return 0;

  if (((gContainers[0]->query_prop(CONT_I_CLOSED) &&
          intp(ob->query_subloc()) && (ob->query_subloc() == 0)) &&
        !gContainers[0]->query_prop(CONT_I_TRANSP)) ||
      gContainers[0]->query_prop(CONT_I_HIDDEN))
    return 0;

  if ((!(intp(ob->query_subloc()) && (ob->query_subloc() == 0)) &&
      gContainers[0]->query_subloc_prop(ob->query_subloc(), CONT_I_CLOSED) &&
      !gContainers[0]->query_subloc_prop(ob->query_subloc(), CONT_I_TRANSP)) ||
      gContainers[0]->query_subloc_prop(ob->query_subloc(), CONT_I_HIDDEN)) {
    return 0;
  }

  return 1;
}

/*
 * Here are some functions with the looks command.
 */
void
show_contents(object cobj)
{
    object *obarr, linked;
    string str;

    if (linked = (object)cobj->query_room())
	obarr = all_inventory(linked);
    else
    	obarr = all_inventory(cobj);
    obarr = filter(obarr, &visible(, cobj));
    if (sizeof(obarr) > 0)
    {
	str = COMPOSITE_DEAD(obarr, PL_BIE);
	write(capitalize(cobj->short(PL_MIA)) + " zawiera" +
	    (cobj->query_tylko_mn() ? "j� " : " ") + 
	    str + ".\n");
    }
    else
    {
	str = "  " + capitalize(cobj->short()) + " " +
	    (cobj->query_tylko_mn() ? "s�" : "jest") + " pus" +
	    cobj->koncowka("ty", "ta", "te", "ci", "te");
	
	write(str + ".\n");
    }
}

void
look_living_exec(mixed plr)
{
    write(plr->long());
}

/*
 * Function name: show_exec
 * Description:   Shows an item depending on its position, normally the long
 *		  description, but short description for items carried or
 *		  inside other items.
 * Arguments:	  object ob
 *
 */
void 
show_exec(object ob)
{
    object env;
    object cur_ob;
    string str;
    
    /* jeremian's changes 01.07.2005 */
    
    int tmpprzyp = PL_DOP;
    string przyimek = "wewn�trz";
    cur_ob = ob;
    
    /* --- */
    
    env = environment(ob); str = 0;

    if (env == this_player() || env == environment(this_player()))
	write(ob->long());
    
    /* objects inside transparent objects */
    while (env != this_player() && env != environment(this_player()))
    {
	    switch (env->parse_subloc_type(cur_ob->query_subloc())) {
		    case 2: /* 'na' */
			    tmpprzyp = PL_MIE;
			    przyimek = "na";
			    break;
		    case 3: /* 'pod' */
			    tmpprzyp = PL_NAR;
			    przyimek = "pod";
			    break;
		    default:
			    tmpprzyp = PL_DOP;
			    przyimek = "wewn�trz";
	    }
	    if (env->is_subloc_prop_set(cur_ob->query_subloc(), SUBLOC_I_OB_PRZYP)) {
		    tmpprzyp = env->query_subloc_prop(cur_ob->query_subloc(), SUBLOC_I_OB_PRZYP);
	    }
	    if (env->is_subloc_prop_set(cur_ob->query_subloc(), SUBLOC_S_OB_GDZIE)) {
		    przyimek = env->query_subloc_prop(cur_ob->query_subloc(), SUBLOC_S_OB_GDZIE);
	    }
	if (!strlen(str))
	    str = "Widzisz " + ob->short(this_player(), PL_BIE);
	if (living(env))
	{
	    str += " niesion";
	    switch(cur_ob->query_rodzaj())
	    {
		case PL_MESKI_OS:
		case PL_MESKI_NOS_ZYW:
		case PL_MESKI_NOS_NZYW: str += "ego"; break;
		case PL_ZENSKI: str += "�"; break;
		default: str += "e"; break;
	    }
	     str += " przez " + env->short(this_player(), PL_BIE);
	}
	
	    /* jeremian's changes 01.07.2005 */
	
	else {
	    str += " " + przyimek + " " + env->short(this_player(), tmpprzyp);
	}
	cur_ob = env;
	
	    /* --- */
	
	env = environment(env);
    }

    if (str) 
    {
	str += ".\n";
	write(str);
    }
}

/**
 * Sprawdza, czy obiekt posiada rzecz gItem.
 *
 * Sprawdzana jest r�wnie� zgodno�� z przypadkiem.
 *
 * U�ywane zmienne globalne:
 * <b>gItem</b>
 *
 * @param ob sprawdzany obiekt
 * @param cur_przyp przypadek, z kt�rym zgodno�� sprawdzamy
 *
 * @return <ul>
 *           <li> <b>1</b> gItem pasuje
 *           <li> <b>0</b> gItem nie pasuje
 *         </ul>
 */
int 
item_access(object ob, int cur_przyp)
{
  if (!objectp(ob))
    return 0;

  return (int) ob->item_id(gItem, cur_przyp);
}

int
inside_visible(object cobj)
{
    object env;

    if (!objectp(cobj) || cobj->query_no_show())
	return 0;

    /* Properties stop us from seing the inside
     */
    if (!cobj->query_prop(CONT_I_IN) || cobj->query_prop(CONT_I_HIDDEN) ||
		(cobj->query_prop(CONT_I_CLOSED) &&
	 	!cobj->query_prop(CONT_I_TRANSP) &&
	 	!cobj->query_prop(CONT_I_ATTACH)))
	return 0;	

    env = environment(cobj);
    if (env == this_player() || env == environment(this_player()) ||
		visibly_hold(cobj))
	return 1;

    while (env && (!env->query_prop(CONT_I_CLOSED) ||
	    env->query_prop(CONT_I_TRANSP)) && !env->query_no_show())
    {
	if (visibly_hold(env))
	    return 1;
	env = environment(env);
	if (env == this_player() || env == environment(this_player()))
	    return 1;
    }
    return 0;
}

varargs int
visible(object ob, object cobj)
{
  object env;

//  write(">>> visible: ob=" + file_name(ob) + ", env(ob)=" + file_name(environment(ob))+ "\n");
  if (!objectp(ob)) {
//    write("!objectp(ob)\n");
    return 0;
  }

  if (cobj && (env = (object)cobj->query_room()) &&
      (cobj->query_prop(CONT_I_TRANSP) ||
       !cobj->query_prop(CONT_I_CLOSED)) &&
      (!cobj->query_subloc_prop(ob->query_subloc(), CONT_M_OPENCLOSE) ||
       cobj->query_subloc_prop(ob->query_subloc(), CONT_I_TRANSP) ||
       !cobj->query_subloc_prop(ob->query_subloc(), CONT_I_CLOSED)))
  {
//    write("cobj && (env = (object)cobj->query_room()) &&\n" +
//          "(cobj->query_prop(CONT_I_TRANSP) ||\n" +
//          "!cobj->query_prop(CONT_I_CLOSED)) &&\n" +
//          "(!cobj->query_subloc_prop(ob->query_subloc(), CONT_M_OPENCLOSE) ||\n" +
//          "cobj->query_subloc_prop(ob->query_subloc(), CONT_I_TRANSP) ||\n" +
//          "!cobj->query_subloc_prop(ob->query_subloc(), CONT_I_CLOSED))\n");
    return ((env->query_prop(OBJ_I_LIGHT) >
          -(this_player()->query_prop(LIVE_I_SEE_DARK))) && 
        CAN_SEE(this_player(), ob));
  }

  env = environment(ob);
//  write("env=" + file_name(env) + "\n");
  if (env == this_player() || env == environment(this_player())) {
//    write("env == this_player() || env == environment(this_player())\n");
    return CAN_SEE(this_player(), ob);
  }

  while (objectp(env) && !living(env) &&
      ((ob->query_subloc() == 0) ?
       (env->query_prop(CONT_I_TRANSP) || !env->query_prop(CONT_I_CLOSED)) :
       (env->query_subloc_prop(ob->query_subloc(), CONT_I_TRANSP) ||
        !env->query_subloc_prop(ob->query_subloc(), CONT_I_CLOSED))))
  {
    env = environment(env);
    if (env == this_player() || env == environment(this_player())) {
//      write("env == this_player() || env == environment(this_player())\n");
      return CAN_SEE(this_player(), ob);
    }
  }
//  write("0\n");
  return 0;
}

/* 
 * Is 
 */
int
visibly_hold(object ob)
{
    object env;
    if (!objectp(ob))
	return 0;
 
    env = environment(ob);
    while (objectp(env))
    {
	if (env == gHolder)
	    return 1;

	if (env->query_prop(CONT_I_HIDDEN) ||
	    (!env->query_prop(CONT_I_TRANSP) &&
	     !env->query_prop(CONT_I_ATTACH) &&
	     env->query_prop(CONT_I_CLOSED)))
	    return 0;
	else
	    env = environment(env);
    }
    return 0;
}

/* 
 * Look ended here.
 */

/* **************************************************************************
 * Here follows the actual functions. Please add new functions in the 
 * same order as in the function name list.
 * **************************************************************************/

/*
 * appraise - Appraise something
 */
int
appraise(string str)
{
    notify_fail("Komenda 'appraise' zosta�a wycofana. Zamiast " +
	"niej mo�esz u�y� 'oce�'.\n");

    return 0;
}

/*
 * Function name: count
 * Description:   Let the players count stuff other than coins (heaps)
 * Arguments:	  str - the string describing what players want to count
 */
int
count(string str)
{
    notify_fail("Komenda 'count' zosta�a wycofana. Zamiast " +
	"niej mo�esz u�y� 'policz'.\n");

    return 0;
}

/*
 * czas - Zwraca aktualny czas w otoczeniu gracza (zaleznie od domeny).
 */
int
czas(string str)
{
    string czas = environment(this_player())->check_time();
//Dodalam tylko tych kilka linijek ponizej. Lil
    if(environment(this_player())->query_prop(ROOM_I_INSIDE))
    {
       if(environment(this_player())->dzien_noc())
       {
          write("Zdaje si�, �e jest noc, jednak musisz wyj�� na "+
	        "zewn�trz, aby sprawdzi� por� dok�adniej.\n");
       }
       else
       {
          write("Zdaje si�, �e jest dzie�, jednak musisz wyj�� na "+
	        "zewn�trz, aby sprawdzi� por� dok�adniej.\n");
       }
       return 1;
       
    }
    write(czas ? czas : "To nie jest rodzaj miejsca, w kt�rym czas by�by "
	+ "czym� istotnym...\n");
    saybb(QCIMIE(this_player(), PL_MIA) + " spogl�da badawczym wzrokiem na niebo.\n");
    return 1;
}

/* 
 * daj - Daj cos komus
 */
int
daj(string str)
{
    object *a;
    object *item1, *item2;
    string str2;

    gMiejsce="";
    gPrep="";

    if (!PREV_LIGHT)
	return light_fail("komu� co� da�");

    notify_fail(capitalize(query_verb()) + " co komu?\n");

    if (!strlen(str))
    {
	return 0;
    }
    
    silent = 0;

    if (!parse_command(str, environment(this_player()), 
	"%i:" + PL_BIE + " %l:" + PL_CEL, item1, item2))
       return 0;
    
    gBezokol = "da�";

    item2 = CMDPARSE_STD->normal_access(item2, 0, this_object());
    if (!item2)
	return 0;

    CMDPARSE_SET_MODIFY_FUN("sort_items");
    item1 = CMDPARSE_STD->normal_access(item1, "manip_give_access",
	this_object());
    if (!item1)
	return 0;

    if (!manip_set_whom(item2))
	return 0;

    if (interactive(gDest) && (!gDest->query_option(OPT_RECEIVING) ||
			gDest->query_prop(PLAYER_I_NO_ACCEPT_GIVE))) {
        write(gDest->query_Imie(this_player(), PL_MIA) + " nie chce przyj�� od ciebie " +
                COMPOSITE_DEAD(item1, PL_DOP) + ".\n");
        gDest->catch_msg(this_player()->query_Imie(gDest, PL_MIA) +
                " pr�buje da� ci " + COMPOSITE_DEAD(item1, PL_BIE) +
                ", ale ty nie chcesz od niego tego przyj��.\n");
        saybb(QCIMIE(this_player(), PL_MIA) + " bezskutecznie pr�buje da� " +
                COMPOSITE_DEAD(item1, PL_BIE) + " " + QCIMIE(gDest, PL_CEL) + ".\n",
                ({ gDest, this_player()}));
        return 1;
    }

	
    item1 = filter(item1, "manip_put_whom", this_object());

    if (sizeof(item1) > 0)
    {
	this_player()->set_obiekty_zaimkow(item1, item2);
    
	write("Dajesz " + COMPOSITE_DEAD(item1, PL_BIE) + " " +
	    gDest->query_imie(this_player(), PL_CEL) + ".\n");
	gDest->catch_msg(this_player()->query_Imie(gDest, PL_MIA) +
	    " daje ci " + QCOMPDEAD(PL_BIE) + ".\n");
    	saybb(QCIMIE(this_player(), PL_MIA) + " daje " + QCOMPDEAD(PL_BIE)
    	   + " " + QIMIE(gDest, PL_CEL) + ".\n", ({ gDest, this_player()}) );
    	return 1;
    }

    return 0;
}

int
drop(string str)
{
    notify_fail("Komenda 'drop' zosta�a wycofana. Zamiast " +
	"niej mo�esz u�y� 'po��'.\n");

    return 0;
}

/*
 * policz - pozwala graczom policzyc cos innego niz monety
 */
int
policz(string str)
{
    object *ob;
    int nr;
    int rodzaj;

    if (!stringp(str))
    {
	notify_fail("Co chcesz policzy�?\n");
	return 0;
    }

    gBezokol = "policzy�";
    
    if (!CAN_SEE_IN_ROOM(this_player()))
	return light_fail("liczy�");

    ob = FIND_STR_IN_OBJECT(str, this_player(), PL_BIE);
    if (!sizeof(ob))
	ob = FIND_STR_IN_OBJECT(str, environment(this_player()), PL_BIE);

    if (sizeof(ob))
    {
 	/* Heaps (coins) have their own routines for countind */
	if (ob[0]->query_prop(HEAP_I_IS))
	    return 0;

	nr = sizeof(ob);

	write ("Doliczy�" + (kobieta() ? "a�" : "e�") + " si� " +
	    LANG_SNUM(nr, PL_DOP, PL_ZENSKI) + " " +
	    (nr > 1 ? "sztuk" : "sztuki") + ".\n");
	
//            LANG_SNUM(nr, PL_DOP, ob[0]->query_rodzaj()) + " " + 
//           (nr == 1 ? ob[0]->short(PL_DOP) : 
//            ob[0]->plural_short(LANG_PRZYP(nr, PL_DOP, 
//            ob[0]->query_rodzaj()))) + ".\n");
	return 1;
    }

    notify_fail("Nie widzisz tu niczego takiego.\n");
    return 0;
}


/*
 * get - get something
 */
/*
 * Function name: get, pick, take
 * Description:   get items from environment or containers
 * Arguments:	  str: tail of command string
 * Returns:       1: did get something
 *		  0: 
 * Globals:	  gFrom: sources of items
 *		  gContainers: containers 
 * Notify_fail:   Get what?
 *		  Get from what?
 *
 * Ex:		  get item(s), get item(s) from container(s)
 *
*/
int
get(string str)
{
    notify_fail("Komenda 'get' zosta�a wycofana. Zamiast " +
	"niej mo�esz u�y� 'we�'.\n");

    return 0;
}

/* 
 * give - Give something to someone
 */
/*
 * Function name: give
 * Description:   tries to move object to another living
 * Arguments:	  str: tail of command string
 * Returns:       1: manage to give something
 *		  0: failed
 * Ex:		  give item(s) to player
 */
int
give(string str)
{
    notify_fail("Komenda 'give' zosta�a wycofana. Zamiast " +
	"niej mo�esz u�y� 'daj'.\n");

    return 0;
}

/*
 * hide - Hide something.
 */
int
hide(string str)
{
    notify_fail("Komenda 'hide' zosta�a wycofana. Zamiast " +
	"niej mo�esz u�y� 'ukryj'.\n");

    return 0;
}
//Dodalam do filter_inv sprawdzanie czy przedmiot nie ma propa... Lil
int
filter_inv(object ob)
{
    return !(ob->query_no_show() || ob->query_no_show_composite() ||
             ob->query_prop(OBJ_I_DONT_INV));
}

/*
 * inventory - List things in my inventory
 */
int
inwentarz(string str)
{
    object tp, *obarr;
    int id, id2;

    if (stringp(str))
    {
//	    write("i: ["+str+"]\n");
	if (!(this_player()->query_wiz_level()))
	{
	    notify_fail("Czyj inwentarz?\n");
	    return 0;
	}

	str = capitalize(lower_case(str));
	
	id = member_array(str, users()->query_met_name(PL_DOP));
	
	if (id != -1)
	{
	    tp = users()[id];
	}	
	else
	{
	   if (!objectp(tp = find_player(str)))
	   {
	       if (!objectp(tp = find_living(str)))
	       {
		   notify_fail("Nie ma nikogo takiego.\n");
		   return 0;
	       }
	   }
	}
    }
    else
	tp = this_player();

    if (PREV_LIGHT <= 0)
    {
	return light_fail("dostrzec cokolwiek");
    }

    tp->add_prop(TEMP_SUBLOC_SHOW_ONLY_THINGS, 1);
    tp->add_prop(TEMP_SHOW_ALL_THINGS, 1);

    write(tp->show_sublocs(this_player()));
    tp->remove_prop(TEMP_SUBLOC_SHOW_ONLY_THINGS);
    tp->remove_prop(TEMP_SHOW_ALL_THINGS);
    remove_alarm(id);
    remove_alarm(id2);

    obarr = (object*)tp->subinventory(0);
    obarr = filter(obarr, &filter_inv());
    if (sizeof(obarr) > 0)
    {
	str = COMPOSITE_DEAD(obarr, PL_BIE);
	write((tp == this_player() ? "Masz" : 
	    tp->query_name(PL_MIA) + " ma") + " przy sobie " + 
	    str + "." + "\n");
    }
    else
    {
	write("  " + (tp == this_player() ? "Nie masz" : 
	   tp->query_name(PL_MIA) + " nie ma") +
	      " nic przy sobie.\n");
    }
    return 1;
}

/*
 * keep - set the OBJ_M_NO_SELL property in an object.
 * unkeep - remove the OBJ_M_NO_SELL property from an object.
 */
int
keep(string str)
{
    notify_fail("Komendy 'keep' i 'unkeep' zosta�y wycofane. Zamiast " +
	"nich mo�esz u�y� 'zabezpiecz' i 'odbezpiecz'.\n");

    return 0;
}

/*
 * look - Look at something
 */
/*
 * Function name: look
 * Description:   glances around, examines objects, or looks at pseudo items
 * Arguments:	  str: tail of look command
 * Returns:       1: found something to look at
 *		  0: failed to find object
 * Notify_fail:   several
 * 
 * Globals:       gItem:  
 *		  gHolder:
 * Ex:	    look, look at orcs, look inside chest
 *
   Documentation of look
   
   The look at (in, inside) command has the following features.

   look	
   	Shows long description of the room and short description of
   	all visible items in the environment (see do_glance() ).

   look at 'multiple objects'
     	Show the long description of several objects, carried or in the
	environment and the short description of object inside transparent
	objects.
	
   look at 'multiple living'
     	Show the long description of several living in the environment.

   look at 'single living'
   	Show the long description of one living in the environment and
	the objects carried by this being.

   look in 'containers'
     	Show the long description of the containers and short descriptions 
	their contents.

   look at 'single object'
     	Shows the long description of the object and if the object is open
	and a container, shows short descriptions of it contents.
   
   look at 'object' carried by 'living'
   	Shows the short description of the object and if the object is
	an open or transparent container shows short descriptions of
	it contents.
   
 */
int 
look(string str)
{
    notify_fail("Komenda 'look' zosta�a wycofana. Zamiast " +
	"niej mo�esz u�y� 'sp�jrz', lub pro�ciej - 'sp'.\n");

    return 0;
}


/*
 * examine - Examine something
 */
/*
 * Function name: examine
 * Description:   pseudonym for  look at, look in, etc
 * Arguments:	  string str: tail of examine command or exa command
 * Returns:       1: found something to look at
 *		  0: failed to found object
 * Ex:	    examine("knife")
 *
*/
int
examine(string str)
{
#if 0
    if (!stringp(str))
    {
	notify_fail("Examine what?\n");
	return 0;
    }

    return look("prp_examine " + str);
#endif

    notify_fail("Komenda 'examine' zosta�a wycofana. Zamiast " +
	"niej mo�esz u�y� 'obejrzyj', lub pro�ciej - 'ob'.\n");

    return 0;

}

/*
 * peek - Peek into someone's inventory, part of someone's inventory.
 */
int
peek(string str)
{
    notify_fail("Komenda 'peek' zosta�a wycofana. Zamiast " +
	"niej mo�esz u�y� 'podejrzyj'.\n");

    return 0;
}

/*
 * przemknij - przemknij gdzies
 */
int
przemknij(string str)
{
    int hiding, *dirs, i, val, bval;
    string str2;

    if (!stringp(str))
    {
	notify_fail("Gdzie chcesz si� przemkn��?\n");
	return 0;
    }

    if (str[0..3] ~= "si� ")
	str = str[4..];
	
    if (str[0..2] == "na ")
    {
	i = 1;
	str = str[3..];
    }
    else i = 0;
    
    gBezokol = "przemkn��";

/*
 * W /secure/master.c pamietane sa kierunki w zlej formie (gora/gore)
 
    str2 = SECURITY->modify_command(str, environment(this_player()));
*/

    str2 = sneak_dirs[str];
    if (strlen(str2))
	str = "na " + str2;
    else if (i)
	str = "na " + str;

    dirs = environment(this_player())->query_exit_full_cmds();
    if ((i = member_array(str, dirs)) < 0)
    {
	notify_fail("Gdzie chcesz si� przemkn��?\n");
	return 0;
    }

    if (this_player()->query_prop(OBJ_I_LIGHT) &&  
	(this_player()->query_prop(OBJ_I_LIGHT) >=
	environment(this_player())->query_prop(OBJ_I_LIGHT)))
    {
	notify_fail("Nie potrafisz przemkn�� si� niezauwa�enie " +
	    "posiadaj�c przy sobie �r�d�o �wiat�a.\n");
	return 0;
    }

    if (objectp(this_player()->query_attack()))
    {
	notify_fail("Nie mo�esz tak po prostu wymkn�� si� z walki.\n");
	return 0;
    }

    hiding = environment(this_player())->query_prop(ROOM_I_HIDE);
    bval = this_player()->query_skill(SS_SNEAK) * 2 + this_player()->query_skill(SS_HIDE) / 3;
    bval = (bval - hiding) / 2;

    if (hiding < 0 || bval <= 0)
    {
	notify_fail("Nie jeste� w stanie wymkn�� si� st�d niezauwa�enie.\n");
	return 0;
    }	

    val = bval + random(bval);
    this_player()->add_prop(OBJ_I_HIDE, val);

    this_player()->add_prop(LIVE_I_SNEAK, 1);
    this_player()->command(environment(this_player())->query_exit_cmds()[i]);

    hiding = environment(this_player())->query_prop(ROOM_I_HIDE);
    bval = this_player()->query_skill(SS_HIDE);
    bval = (bval - hiding) / 2;

    if (hiding < 0 || bval <= 0)
    {
	write("Jest tu zbyt ci�ko si� schowa�, wi�c jeste� widoczn" +
	    (kobieta() ? "a" : "y") + " z powrotem.\n");
	this_player()->reveal_me(0);
	return 1;
    }	

    if (this_player()->query_prop(OBJ_I_LIGHT) &&  
	(this_player()->query_prop(OBJ_I_LIGHT) > 
	environment(this_player())->query_prop(OBJ_I_LIGHT)))
    {
	write("Posiadaj�c �r�d�o �wiata nie jeste� w stanie ukry� si� " +
	    "skutecznie.\n");
	this_player()->reveal_me(1);
	return 1;
    }

    val = bval + random(bval);
    this_player()->add_prop(OBJ_I_HIDE, val);
    return 1;
}

private int
owner_pred(object ob, string* owners)
{
    return (member_array(MASTER_OB(ob), owners) != -1);
}

/*
 * Przeszukaj - Przeszukaj cos
 */
int
przeszukaj(string str)
{
    object *ob, obj;
    int time;
    string item, rest;
    object* owners, env;

    notify_fail("Przeszukaj co?\n");
    
    if (!stringp(str))
	return 0;

    if (this_player()->query_attack())
    {
	write("Ale przecie� jeste� w �rodku walki!\n");
	return 1;
    }

	if(this_player()->query_prop("_sit_siedzacy") ||
		this_player()->query_prop("_sit_lezacy"))
	{
		write("A mo�e wpierw wstaniesz?\n");
		return 1;
	}

    if (!CAN_SEE_IN_ROOM(this_player()))
	return light_fail("dostrzec cokolwiek");
   
    if(TP->query_mana() < F_SEARCH_MANA_COST + 1)
    {
        notify_fail("Jeste� zbyt wyczerpan" + TP->koncowka("y", "a", "e") + " mentalnie.\n");
	return 0;
    }

    gBezokol = "przeszuka�";

    if (!sizeof(ob = FIND_STR_IN_OBJECT(str, this_player(), PL_BIE)) &&
	!sizeof(ob = FIND_STR_IN_OBJECT(str, environment(this_player()),
					PL_BIE)))
    {
	 if (environment(this_player())->item_id(str))
	     ob = ({environment(this_player())});
    }
    else
	ob = FILTER_CAN_SEE(ob, this_player());

    if (!sizeof(ob))
	return 0;

    obj = ob[0];
    /*object *vis_liv=
          FILTER_LIVE(FILTER_SHOWN(all_inventory(environment(this_player()))));*/
    if(obj==this_player())// || sizeof(vis_liv)==1)
    {
        write("Nie mo�esz przeszukiwa� sam"+this_player()->koncowka("","a")+
        " siebie.\n");
        return 1;
    }
    
    if (obj == environment(this_player()))
    {
	write("Zaczynasz przeszukiwa� " + str + ".\n");
	saybb(QCIMIE(this_player(), PL_MIA) + " zaczyna przeszukiwa� " +
	    str + ".\n");
    } 
    else 
    {
	if (living(obj))
	{
	    write("Zaczynasz przeszukiwa� " +
		obj->query_imie(this_player(), PL_BIE) + ".\n");
	    tell_object(obj, this_player()->query_Imie(obj, PL_MIA) +
		" zaczyna przeszukiwa� CIEBIE!\n");
	    saybb(QCIMIE(this_player(), PL_MIA) + " zaczyna przeszukiwa� " + 
		QIMIE(obj, PL_BIE) + 
		".\n", ({ obj, this_player() }));
   	} 
   	else 
   	{
	    write("Zaczynasz przeszukiwa� " + COMPOSITE_DEAD(obj, PL_BIE) + 
		".\n");
	    saybb(QCIMIE(this_player(), PL_MIA) + " zaczyna przeszukiwa� " + 
		QSHORT(obj, PL_BIE) + ".\n");
	}
    }

    obj->search_object(str, 1);
    env = ENV(TP);
    if (objectp(env)) {
        owners = filter(all_inventory(env), &owner_pred(, obj->query_owners()));
        owners->signal_rusza_moje_rzeczy(this_player(), obj);
    }
    
    TP->add_fatigue(-F_SEARCH_MANA_COST);

    return 1;
}


/*
 * put - Put something
 */
/*
 * Function name: put, drop
 * Description:   put items in environment or cointainers
 * Arguments:	  str: tail of command string
 * Returns:       1: did get something
 *		  0: failedd to get anything
 * Notify_fail:   * "Put what?"
 *		  * 
 * Ex:	       drop item(s), put item(s) in container
 *
 */
int
put(string str)
{
    notify_fail("Komenda 'put' zosta�a wycofana. Zamiast " +
	"niej mo�esz u�y� 'w��'.\n");

    return 0;
}

/*
 * ocen - Ocen jakas rzecz
 */
int
ocen(string str)
{
    object *ob;
    int i;

    if (PREV_LIGHT <= 0)
	return light_fail("ocenia�");

    notify_fail("Co chcesz oceni�?\n");
    if (!stringp(str))
	return 0;

    gBezokol = "oceni�";

    if (str == "siebie")
        ob = ({this_player()});
    else if (!sizeof(ob = FIND_STR_IN_OBJECT(str, this_player(), PL_BIE)) &&
             !sizeof(ob = FIND_STR_IN_OBJECT(str, environment(this_player()),
                                             PL_BIE)))
        return 0;

    for (i = 0; i < sizeof(ob); i++)
    {
	if (living(ob[i]))
	    this_player()->catch_msg("Ogl�dasz dok�adnie " +
			ob[i]->query_imie(this_player(), PL_BIE) + 
			".\n");
	else
	    this_player()->catch_msg("Oceniasz starannie " +
			ob[i]->short(this_player(), PL_BIE) + ".\n");
	ob[i]->appraise_object();
    }

    return 1;
}

/*
 * Nazwa funkcji: odloz
 * Opis: Funkcja odpowiedzialna za dzia�anie komendy 'od��'.
 * Argumenty: str - komenda napisana przez gracza
 * Zwraca: 0 - funkcja zako�czy�a si� niepowodzeniem
 *         1 - funkcja zako�czy�a si� powodzeniem
 */
int
odloz(string str)
{
    object *itema, *cont;
    int i;
    
    gMiejsce = ""; /* added by Delvert */
    /* jeremian's changes */
    gPrep = "";
    gDest = 0;
    gItem = 0;
    gPisacODest = 1;
    /* --- */

    if ((query_verb() ~= "po��") && stringp(str) && (str ~= "si�")) {
      return 0;
    }

    gBezokol = (query_verb() ~= "od��" ? "od�o�y�" :
		    (query_verb() ~= "po��" ? "po�o�y�" : "powiesi�"));
    notify_fail(capitalize(query_verb()) + " co?\n");
    
    if (!stringp(str))
	return 0;

    /* This is done to avoid all those stupid messages 
       when you try 'drop all' 
     */
    if (str == "wszystko")
	silent = 1;
    else
	silent = 0;

    /*
     * Pierwszy przypadek: gracz chce co� po prostu 'od�o�y�' lub 'po�o�y�'.
     * Przedmioty pr�buje si� przenie�� do defaultowej sublokacji otoczenia gracza.
     */
    
    if (parse_command(str, environment(this_player()), "%i:" + PL_BIE, itema) &&
		   ((query_verb() ~= "od��") || (query_verb() ~= "po��"))) 
    {
	CMDPARSE_SET_MODIFY_FUN("sort_items");
	itema = CMDPARSE_STD->normal_access(itema, "manip_drop_access",
		this_object(), 1);

	if (sizeof(itema) == 0) 
	{
	    if (silent)
		notify_fail("Nic nie od�o�y�" + 
		    this_player()->koncowka("e�", "a�", "o�") + ".\n");
	    return 0;
	}
	itema = filter(itema, manip_relocate_to);

	if (sizeof(itema) > 0)
	{
	    this_player()->set_obiekty_zaimkow(itema);
	    write(((query_verb() ~= "od��") ? "Odk�adasz " : "K�adziesz ") +
			    COMPOSITE_DEAD(itema, PL_BIE) + ".\n");
	    saybb(QCIMIE(this_player(), PL_MIA) +
			    ((query_verb() ~= "od��") ? " odk�ada " : " k�adzie ") + 
		QCOMPDEAD(PL_BIE) + ".\n");
	    return 1;
	}
    }

/* jeremian's changes */

   /*
    * Drugi przypadek: gracz pr�buje co� 'od�o�y�' lub 'po�o�y�' w jakiej� podstawowej sublokacji
    * kontenera obecnego na lokacji.
    */

    else if (parse_command(str, environment(this_player()), "%i:" + PL_BIE + " %w %i:" + (query_verb() ~= "od��" ? PL_BIE : PL_MIE), itema, gPrep, cont))
    {
	    cont = NORMAL_ACCESS(cont, 0, 0);

	    if (query_verb() ~= "od��") {
		    if (!manip_set_odloz_dest("odloz", cont))
		    {
			    return 0;
		    }
	    }
	    else {
		    if (!manip_set_odloz_dest("poloz", cont))
		    {
			    return 0;
		    }
	    }

	    CMDPARSE_SET_MODIFY_FUN("sort_items");
	    itema = NORMAL_ACCESS(itema, "manip_drop_access", this_object());

	    if (sizeof(itema) == 0)
	    {
		    notify_fail(capitalize(query_verb()) + " co?\n");
		    return 0;
	    }

	    if ((query_verb() ~= "od��") && gDest->query_prop(CONT_I_CANT_ODLOZ)) {
		    notify_fail("Nie mo�na od�o�y� nic " + gPrep + " " + gDest->short(PL_DOP) + ".\n");
		    say(QCIMIE(this_player(), PL_MIA) + " bezskutecznie pr�buje od�o�y� " +
		    COMPOSITE_DEAD(itema, PL_BIE) + " " + gPrep + " " + QSHORT(gDest, PL_DOP) + ".\n");
		    return 0;
	    }

	    if ((query_verb() ~= "po��") && gDest->query_prop(CONT_I_CANT_POLOZ)) {
		    notify_fail("Nie mo�na po�o�y� nic " + gPrep + " " + gDest->short(PL_MIE) + ".\n");
		    say(QCIMIE(this_player(), PL_MIA) + " bezskutecznie pr�buje po�o�y� " +
		    COMPOSITE_DEAD(itema, PL_BIE) + " " + gPrep + " " + QSHORT(gDest, PL_MIE) + ".\n");
		    return 0;
	    }

	    gMiejsce = gPrep;

	    itema = filter(itema, manip_put_dest);
	    
	    if (sizeof(itema) > 0)
	    {
		    this_player()->set_obiekty_zaimkow(itema);
		    if (query_verb() ~= "od��")
		    {
			    write("Odk�adasz " + COMPOSITE_DEAD(itema, PL_BIE) + " " + gPrep + " " +
					    gDest->short(PL_BIE) + ".\n");
			    saybb(QCIMIE(this_player(), PL_MIA) + " odk�ada " + QCOMPDEAD(PL_BIE) +
					    " " + gPrep + " " + (gPrep == "w" ? QSHORT(gDest, PL_DOP) : QSHORT(gDest, PL_BIE)) + ".\n");
		    }
		    else if (query_verb() ~= "po��")
		    {
			    write("K�adziesz " + COMPOSITE_DEAD(itema, PL_BIE) + " " + gPrep + " " +
					    gDest->short(PL_MIE) + ".\n");
			    saybb(QCIMIE(this_player(), PL_MIA) + " k�adzie " + QCOMPDEAD(PL_BIE) +
				    " " + gPrep + " " + (gPrep == "pod" ? QSHORT(gDest, PL_NAR) : QSHORT(gDest, PL_MIE)) + ".\n");
		    }
		    else {
			    write("Wieszasz " + COMPOSITE_DEAD(itema, PL_BIE) + " " + gPrep + " " +
					    gDest->short(PL_MIE) + ".\n");
			    saybb(QCIMIE(this_player(), PL_MIA) + " wiesza " + QCOMPDEAD(PL_BIE) +
				    " " + gPrep + " " + QSHORT(gDest, PL_MIE) + ".\n");
		    }
		    return 1;
	    }

	    /* manip_relocate_to() ju� wypisa�a komunikat o b��dzie */
	    notify_fail("");
    }
   /*
    * trzeci przypadek: gracz pr�buje co� 'od�o�y�' lub 'po�o�y�' w jakiej� bardziej skomplikowanej
    * sublokacji kontenera obecnego na lokacji.
    */
    else if (parse_command(str, environment(this_player()), "%i:" + PL_BIE + " 'na' %s %i:" + PL_DOP, itema, gMiejsce, cont))
    {
	    gPrep = "na";

	    cont = NORMAL_ACCESS(cont, 0, 0);

	    if (query_verb() ~= "od��") {
		    if (!manip_set_odloz_dest("odloz", cont))
		    {
			    return 0;
		    }
	    }
	    else {
		    if (!manip_set_odloz_dest("poloz", cont))
		    {
			    return 0;
		    }
	    }

	    if (gMiejsce == "") {
		    notify_fail("Gdzie chcesz to " + gBezokol + "?\n");
		    return 0;
	    }
	    
	    CMDPARSE_SET_MODIFY_FUN("sort_items");
	    itema = NORMAL_ACCESS(itema, "manip_drop_access", this_object());

	    if (sizeof(itema) == 0)
	    {
		    notify_fail(capitalize(query_verb()) + " co?\n");
		    return 0;
	    }

	    gMiejsce = "na " + gMiejsce;

	    itema = filter(itema, manip_put_dest);
	    
	    if (sizeof(itema) > 0)
	    {
		    this_player()->set_obiekty_zaimkow(itema);
		    if (query_verb() ~= "od��")
		    {
			    write("Odk�adasz " + COMPOSITE_DEAD(itema, PL_BIE) + " na " +
					    itema[0]->query_subloc()[PL_BIE] + " " + gDest->short(PL_DOP) + ".\n");
			    saybb(QCIMIE(this_player(), PL_MIA) + " odk�ada " + QCOMPDEAD(PL_BIE) +
					    " na " + itema[0]->query_subloc()[PL_BIE] +
					    " " + QSHORT(gDest, PL_DOP) + ".\n");
		    }
		    else if (query_verb() ~= "po��")
		    {
			    write("K�adziesz " + COMPOSITE_DEAD(itema, PL_BIE) + " na " +
					    itema[0]->query_subloc()[PL_MIE] + " " + gDest->short(PL_DOP) + ".\n");
		    saybb(QCIMIE(this_player(), PL_MIA) + " k�adzie " + QCOMPDEAD(PL_BIE) +
				    " na " + itema[0]->query_subloc()[PL_MIE] +
				    " " +  QSHORT(gDest, PL_DOP) + ".\n");
		    }
		    else {
			    write("Wieszasz " + COMPOSITE_DEAD(itema, PL_BIE) + " na " +
					    itema[0]->query_subloc()[PL_MIE] + " " + gDest->short(PL_DOP) + ".\n");
		    saybb(QCIMIE(this_player(), PL_MIA) + " wiesza " + QCOMPDEAD(PL_BIE) +
				    " na " + itema[0]->query_subloc()[PL_MIE] +
				    " " +  QSHORT(gDest, PL_DOP) + ".\n");
		    }
		    return 1;
	    }

	    /* manip_relocate_to() ju� wypisa�a komunikat o b��dzie */
	    notify_fail("");
    }
   /*
    * czwarty przypadek: gracz pr�buje co� 'od�o�y�' lub 'po�o�y�' w jakiej� sublokacji otoczenia
    */
    else if (parse_command(str, environment(this_player()), "%i:" + PL_BIE + " %w %s ",itema,gPrep,gMiejsce))
    {
//	   write("4] gPrep=["+gPrep+"], gMiejsce=["+gMiejsce+"]\n");

	    CMDPARSE_SET_MODIFY_FUN("sort_items");
	    itema = NORMAL_ACCESS(itema, "manip_drop_access", this_object());

	   if (sizeof(itema) == 0)
	   {
		   notify_fail(capitalize(query_verb()) + " co?\n");
		   return 0;
	   }
	   
	    gPisacODest = 0;

	    gMiejsce = gPrep + ((gMiejsce == "") ? : (" " + gMiejsce));

	    itema = filter(itema, manip_put_dest);

	   if (sizeof(itema) > 0)
	   {
		   this_player()->set_obiekty_zaimkow(itema);
		   if (query_verb() ~= "od��")
		   {
			   write("Odk�adasz " + COMPOSITE_DEAD(itema, PL_BIE) + " " + gPrep + " " +
				   itema[0]->query_subloc()[((gPrep == "w") ? PL_DOP : PL_BIE)] + ".\n");
			   saybb(QCIMIE(this_player(), PL_MIA) + " odk�ada " + QCOMPDEAD(PL_BIE) +
					   " " + gPrep + " " + itema[0]->query_subloc()[((gPrep == "w") ? PL_DOP : PL_BIE)] + ".\n");
		   }
		   else if (query_verb() ~= "po��")
		   {
			   write("K�adziesz " + COMPOSITE_DEAD(itema, PL_BIE) + " " + gPrep + " " +
             itema[0]->query_subloc()[((gPrep == "pod") ? PL_NAR : PL_MIE)] + ".\n");
			   saybb(QCIMIE(this_player(), PL_MIA) + " k�adzie " + QCOMPDEAD(PL_BIE) +
					   " " + gPrep + " " + itema[0]->query_subloc()[((gPrep == "pod") ? PL_NAR : PL_MIE)] + ".\n");
		   }
		   else {
			   write("Wieszasz " + COMPOSITE_DEAD(itema, PL_BIE) + " " + gPrep + " " +
             itema[0]->query_subloc()[PL_MIE] + ".\n");
			   saybb(QCIMIE(this_player(), PL_MIA) + " wiesza " + QCOMPDEAD(PL_BIE) +
					   " " + gPrep + " " + itema[0]->query_subloc()[PL_MIE] + ".\n");
		   }
		   return 1;
	   }

	   /* manip_relocate_to() ju� wypisa�a komunikat o b��dzie */
	   notify_fail("");

   }
   else
   {
	/* --- */
	if (!PREV_LIGHT)
	    return light_fail("dostrzec cokolwiek");

	return 0;
   }
   return 0;
}

string
substitute_dir_alias(string str)
{
    switch (str) {
	case "n": return "p�noc";
	case "ne": return "p�nocny-wsch�d";
	case "e": return "wsch�d";
	case "se": return "po�udniowy-wsch�d";
	case "s": return "po�udnie";
	case "sw": return "po�udniowy-zach�d";
	case "w": return "zach�d";
	case "nw": return "p�nocny-zach�d";
	case "d": return "d�";
	case "u":
	case "g�r�": return "g�ra";
	default: return str;
    }
}

int
is_standard_direction(string str)
{
    switch (str) {
	case "p�noc":
	case "p�nocny-wsch�d":
	case "wsch�d":
	case "po�udniowy-wsch�d":
	case "po�udnie":
	case "po�udniowy-zach�d":
	case "zach�d":
	case "p�nocny-zach�d":
	case "d�":
	case "g�ra": return 1;
	default: return 0;
    }
}


static void
describe_combat(object *livings)
{
    int     index;
    int     size;
    string  text = "";
    object  victim;
    mapping fights = ([ ]);
    mixed tmp_alv;

    if ((size = sizeof(livings)) < 1)
    {
	return;
    }

    livings += ({ this_object() });
    size++;
    index = -1;
    while(++index < size)
    {
	if (objectp(victim = livings[index]->query_attack()))
	{
	    if (pointerp(fights[victim]))
	    {
		fights[victim] += ({ livings[index] });
	    }
	    else
	    {
		fights[victim] = ({ livings[index] });
	    }
	}
    }

    if (!m_sizeof(fights))
    {
	return;
    }
    
    livings = m_indices(fights);
    size = sizeof(livings);
    index = -1;
    while(++index < size)
    {
	if (!fights[livings[index]])
	    continue;
              
	if (objectp(victim = livings[index]->query_attack()) &&
	    (member_array(victim, fights[livings[index]]) >= 0))
	{
	    fights[livings[index]] -= ({ victim });
	    
	    if (!fights[victim])
	    {
		text += victim->short(this_player(), PL_MIA) +
		    " koncentruje si� na walce " + 
		    z_ze(livings[index]->short(this_player(), PL_NAR)) + 
		    ".\n";
	    }
	    else
	    {
	        fights[victim] -= ({ livings[index] });
	    
		text += livings[index]->short(this_player(), PL_MIA);
		
		if (sizeof(fights[victim]))
		{
		    text += ", wraz " + z_ze(FO_COMPOSITE_LIVE(fights[victim], 
			this_player(), PL_NAR));
		}
		
		text += " walczy " + z_ze(victim->short(this_player(),
		    PL_NAR));
		
		if (sizeof(fights[livings[index]]))
		{
		    text += ", wspart" + victim->koncowka("ym", "�")
			+ " przez " + FO_COMPOSITE_LIVE(fights[livings[index]],
			this_player(), PL_BIE);
		}
		
		text += ".\n";
	    }
	}
	else
	{
	    text += capitalize(FO_COMPOSITE_LIVE(fights[livings[index]],
		this_player(), PL_MIA)) + " koncentruj" + 
		((sizeof(fights[livings[index]]) == 1) ? "e" : "�") +
		" si� na walce " + 
		z_ze(livings[index]->short(this_player(), PL_NAR)) + 
		".\n";
	}

	fights = m_delete(fights, livings[index]);
	fights = m_delete(fights, victim);
    }

    write(text);
}

int objects_filter(object ob)
{
    if (ob->query_no_show() || ob->query_no_show_composite()) return 0;
    if (ob->query_prop(OBJ_I_DONT_GLANCE)) return 0;
    if (!ob->query_prop(OBJ_I_VOLUME)) return 1;
    return ob->query_prop(OBJ_I_VOLUME) > 50000;
}

/* jeremian's changes 13.07.2005 - zmiany w obejrzyj */

/* Nie mo�na ju� ogl�da� bezpo�rednio rzeczy dodanych przez add_item.
 * Trzeba poda� tak�e nazw� rzeczy, w kt�rej umieszczony jest przedmiot.
 * Wyj�tek stanowi� przedmioty dodane do otoczenia gracza
 */

/* --- */






/**
 * Podaje opis jakiego� przedmiotu.
 *
 * @param str tre�� komendy gracza
 * @param adv przys��wek, z jakim wykonano komend�
 *
 * @return <ul>
 *           <li> <b>0</b> nie uda�o si� odnale�� opisu przedmiotu
 *           <li> <b>1</b> opis przedmiotu zosta� wypisany
 *         </ul>
 */
int
obejrzyj(string str, string adv = "")
{
  string	*tmp;
  int		i;
  object	*obarr, *obd, *obl;
  string      tmpText = "";

  if (!stringp(str))
  {
    notify_fail("Co chcesz obejrze�?\n");
    return 0;
  }

  if (PREV_LIGHT <= 0)
    return light_fail("dostrzec cokolwiek");

  gItem = lower_case(str);
  gBezokol = "obejrze�";

  str = lower_case(str);
  tmp = explode(str, " ");
  if (sizeof(tmp) > 1 && tmp[1][0] == '0')
    return 0;

  if (!parse_command(str, ENV(TP), "%i:" + PL_BIE, obarr) ||
      !sizeof(obarr = NORMAL_ACCESS(obarr, "visible", this_object())))
  {
    int found = 0;
    string str2 = str = substitute_dir_alias(str);

    if (is_standard_direction(str)) {
      object *doors = filter(all_inventory(environment(this_player())), &->is_door());

      doors = filter(doors, &->query_open());
      for (i = 0; i < sizeof(doors); ++i) {
        if (member_array(str, doors[i]->query_pass_command()) != -1) {
          found = 1;
          break;
        }
      }
      if (found)
        str2 = doors[i]->query_other_room();
      else {
        if ((i = (member_array(str2, environment(this_player())->query_exit_cmds()))) != -1)
        {
          found = 1;
          str2 = environment(this_player())->query_exit_rooms()[i];
        }
      }
    }

    if (found)
    {
      set_auth(this_object(), "root:root");

      write("Spogl�dasz" + adv + " na " + (str~="g�ra"?"g�r�":str) + ":\n");
      allbb("spogl�da" + adv + " na " + (str~="g�ra"?"g�r�":str) + ".", adv);
      LOAD_ERR(str2);



		TP->daj_paraliz_na_sp(str2);

		return 1;
    }

    /* No objects found */
    /* Test for pseudo item in the environment */
    if (environment(this_player())->item_id(str) &&
        CAN_SEE(this_player(), ENV(TP)))
    {
      write(environment(this_player())->long(str));
      return 1;
    }
    else
    {
      if (str == "siebie" || str == "mnie")
      {
        write(this_player()->long(this_player()));
        return 1;
      }
      if ((str == "przeciwnika" || str == "wroga") && 
          this_player()->query_attack())
      {
        write(this_player()->query_attack()->long(this_player()));
        return 1;
      }

      /*
       * Nie uda�o si� nic znale��, wi�c sprawdzamy, czy gracz nie chce sobie
       * obejrze� jakiego� przedmiotu dodanego komend� add_item do przedmiotu
       * b�d�cego na danej lokacji.
       */
      notify_fail("Nie zauwa�asz niczego takiego.\n", 3);
      for (int cur_przyp = PL_MIA; cur_przyp <= PL_MIE; cur_przyp++) {
        if (parse_command(str, ENV(TP), "%s %i:" + cur_przyp, tmpText, obarr)) {
          gItem = lower_case(tmpText);
          obarr = NORMAL_ACCESS(obarr, "visible", this_object());
          obarr = filter(obarr, &item_access(, cur_przyp));
          if (sizeof(obarr) > 0) 
          {
            write(obarr[0]->long(gItem));
            return 1;
          } 
        }
        else
        {
          obarr = 0;
        }
      }
      //		return 0;
    }
  }

  // to jest bez sensu!
  /*
     if (pointerp(obarr))
     {
     obarr -= this_player()->query_armour(-1) + this_player()->query_weapon(-1);
     }
     */

  if (sizeof(obarr) == 0)
  {
    string* strtab;
    string piewyr;
    notify_fail("Nie zauwa�asz niczego takiego.\n", 3);
    strtab = explode(str, " ");
    if (!pointerp(strtab) || !sizeof(strtab))
    {
      return 0;
    }
    if ((strtab[0] == "") && (sizeof(strtab) > 1))
    {
      piewyr = strtab[1];
      str = implode(strtab[2..], " ");
    }
    else {
      piewyr = strtab[0];
      str = implode(strtab[1..], " ");
    }

    /*
     * Nic nie znale�li�my, wi�c trzeba jeszcze spr�bowa� w rzeczach za�o�onych.
     */
    // .. to tez
    /*
       if (((piewyr ~= "za�o�ony") || (piewyr ~= "za�o�on�") || (piewyr ~= "za�o�one")) &&
       (parse_command(str, this_player()->query_armour(-1), "%i:" + PL_BIE, obarr) && sizeof(obarr = NORMAL_ACCESS(obarr, "visible", this_object()))))
       {
       }
       else if (((piewyr ~= "dobyty") || (piewyr ~= "dobyt�") || (piewyr ~= "dobyte") ||
       (piewyr ~= "chwycony") || (piewyr ~= "chwycon�") || (piewyr ~= "chwycone")) &&
       (parse_command(str, this_player()->query_weapon(-1), "%i:" + PL_BIE, obarr) && sizeof(obarr = NORMAL_ACCESS(obarr, "visible", this_object()))))
       {
       }
       else
       {
       return 0;
       }
       */
  }

  obd = obl = ({ });
  if (sizeof(obarr))
  {
    obd = FILTER_DEAD(obarr);
    obl = FILTER_LIVE(obarr);
  }
  if (sizeof(obd) == 0 && sizeof(obl) == 0)
  {
    notify_fail("Nie zauwa�asz niczego takiego.\n", 3);
    return 0;
  }

//  write(">>> obejrzyj: obiekty na lokacji\n");
  
  /* if single container we show the contents */
  if (sizeof(obd) == 1 && inside_visible(obd[0]))
  {
    show_exec(obd[0]);
    if (!obd[0]->query_prop(CONT_I_DONT_SHOW_CONTENTS))
      show_contents(obd[0]);
  } 
  else
    map(obd, show_exec);

  /* if a single living being we show carried items */
  if (sizeof(obl) == 1)
    look_living_exec(obl[0]);
  else
    map(obl, show_exec);

  this_player()->set_obiekty_zaimkow(obarr);

  return 1;
}

int 
peek_access(object ob)
{
    if (!living(ob) || ob->query_ghost() || ob == this_player())
	return 0;
    else
	return 1;
}

/*
 * podejrzyj - Podejrzyj czyjs ekwipunek, lub czesc ekwipunku.
 */
int
podejrzyj(string str)
{
    string vb;
    object *p, *inv;
    int id, i, pp_skill;

    vb = query_verb(); gBezokol = "podejrze�";

    notify_fail(capitalize(vb) + " czyj ekwipunek?\n");

    if (!stringp(str))
	return 0;

    p = CMDPARSE_ONE_ITEM(str, "peek_access", "peek_access");

    if (!sizeof(p))
    {
	return 0;
    }
    if (sizeof(p) > 1)
    {
	notify_fail("Mo�esz podejrze� ekwipunek tylko jednej osobie naraz.");
	return 0;
    }

    pp_skill = this_player()->query_skill(SS_PICK_POCKET) / 2;
    if ((pp_skill + random(pp_skill) > p[0]->query_skill(SS_AWARENESS)) &&
	(!p[0]->query_wiz_level()))
    {
	inv = all_inventory(p[0]);

	p[0]->add_prop(TEMP_SUBLOC_SHOW_ONLY_THINGS, 1);
	id = set_alarm(0.1, 0.0, &(p[0])->remove_prop(TEMP_SUBLOC_SHOW_ONLY_THINGS));
	write(p[0]->show_sublocs(this_player()));
	p[0]->remove_prop(TEMP_SUBLOC_SHOW_ONLY_THINGS);
	remove_alarm(id);

	inv = (object*)p[0]->subinventory(0);
	inv = FILTER_SHOWN(inv);
	if (sizeof(inv))
	    write(p[0]->query_Imie(this_player(), PL_MIA) +
		" ma przy sobie " +
		COMPOSITE_DEAD(inv, PL_BIE) + ".\n");
	else
	    write(p[0]->query_Imie(this_player(), PL_MIA) +
		" nie ma nic przy sobie.\n");
    }
    else
    {
	tell_object(p[0], "Przy�apujesz " +
	    this_player()->query_Imie(p[0], PL_BIE) +
	    " na penetrowaniu twojego w�asnego ekwipunku! Co za brak "+
	    "kultury...\n");
	write("O kurcz�! Zdaje si�, �e " + 
	    p[0]->query_Imie(this_player(), PL_MIA) + " przy�apa�" +
	    p[0]->koncowka("", "a") + " ci�!!\n");
    }
    return 1;
}

/*
 * pokaz - Pokazywanie przedmiotu innej osobie.
 *                                          
 */

int
pokaz(string str)
{
    object *a;
    object *item1, *item2;
    string str2;

    gMiejsce="";
    gPrep="";

    if (!PREV_LIGHT)
	return light_fail("komu� co� pokaza�");

    notify_fail(capitalize(query_verb()) + " co komu ?\n");

    if (!strlen(str))
    {
	return 0;
    }
    
    if (!parse_command(str, environment(this_player()), 
	"%i:" + PL_BIE + " %l:" + PL_CEL, item1, item2))
       return 0;
    
    gBezokol = "pokaza�";

    item2 = CMDPARSE_STD->normal_access(item2, 0, this_object());
    if (!item2)
	return 0;

    item1 = CMDPARSE_STD->normal_access(item1, 0, this_object());
    item1 -= all_inventory(environment(this_player()));
    if (!item1)
	return 0;

    if (sizeof(item2) > 1) {
	write("Mo�esz pokaza� co� tylko jednej osobie naraz.\n");
	return 1;
    }

    if (item2[0]->query_attack())
    {
      write(item2[0]->query_imie(this_player(), PL_MIA) + " jest zbyt zaj�t" +
            item2[0]->koncowka("y", "a") + ", aby na to spojrze�.\n");
      return 1;
    }

    if (sizeof(item1) > 0)
    {
	this_player()->set_obiekty_zaimkow(item1, item2);
    
	write("Pokazujesz " + COMPOSITE_DEAD(item1, PL_BIE) + " " +
	    item2[0]->query_imie(this_player(), PL_CEL) + ".\n");
	item2[0]->catch_msg(this_player()->query_Imie(item2[0], PL_MIA) +
	    " pokazuje ci " + QCOMPDEAD(PL_BIE) + ":\n");
	map(item1, &(item2[0])->catch_msg() @ &->long());
    	saybb(QCIMIE(this_player(), PL_MIA) + " pokazuje " + QCOMPDEAD(PL_BIE)
    	   + " " + QIMIE(item2[0], PL_CEL) + ".\n", ({ item2[0], this_player()}) );
    	return 1;
    }

    return 0;
}



/*
 * ukryj - Chowanie czegos.
 */
int
ukryj(string str)
{
    object *itema, *cont;
    string vb = query_verb();
    int hiding, i, val, bval, poorly, self;

    notify_fail(capitalize(vb) + " co?\n");

    gBezokol = "ukry�";

    if (!stringp(str))
	return 0;

    if (sscanf(str, "pobie�nie %s", str))
	poorly = 1;
    else
	poorly = 0;
	
    if (str ~= "si�" || str == "siebie")
	self = 1;

    if (!CAN_SEE_IN_ROOM(this_player()))
	return light_fail("m�c cokolwiek ukry�");

    if ((this_player()->query_prop(OBJ_I_LIGHT) > 0) &&  
	(this_player()->query_prop(OBJ_I_LIGHT) > 
	environment(this_player())->query_prop(OBJ_I_LIGHT)))
    {
	notify_fail("�wiec�c tak nie jeste� w stanie " +
	    (self ? "si� dobrze ukry�" : "ukry� czegokolwiek") + "!\n");
	return 0;
    }

    hiding = environment(this_player())->query_prop(ROOM_I_HIDE);
    bval = this_player()->query_skill(SS_HIDE);
    if (hiding < 0 || hiding > bval)
    {
	notify_fail("Nie jeste� w stanie " + (self ? "si� tu dobrze schowa�" :
	    "tu czegokolwiek schowa�") + ".\n");
	return 0;
    }

    bval = (bval - hiding) / 2;
    val = bval + random(bval);

    if ((str ~= "si�" || str == "siebie") && !poorly)
    {
	cont = all_inventory(environment(this_player()));
	itema = FILTER_LIVE(cont);
	itema = FILTER_CAN_SEE(itema, this_player());
	if (sizeof(itema) > 1)
	{
	    notify_fail("Nie mo�esz si� schowa� kiedy inni patrz�!\n");
	    return 0;
	}

	if (this_player()->query_attack())
	{
	    notify_fail("Podczas walki nie mo�esz si� tak po prostu " +
		"schowa�.\n");
	    return 0;
	}

	if (this_player()->query_prop(OBJ_I_HIDE))
	{
	    notify_fail("Nie potrafisz schowa� si� jeszcze lepiej.\n");
	    return 0;
	}
	else
	{
	    saybb(QCIMIE(this_player(), PL_MIA) + " pr�buje ukry� si� " +
		"jak najlepiej.\n");
	    write("Chowasz si� najlepiej jak potrafisz.\n");
	    if (this_player()->query_prop(OBJ_I_INVIS))
		this_player()->add_prop(OBJ_I_HIDE, val / 2);
	    else
		this_player()->add_prop(OBJ_I_HIDE, val);
	}
	return 1;
    }

    silent = 0;
    if (str == "wszystko")
	silent = 1;

    if (parse_command(str, environment(this_player()), "%i:" + PL_BIE, itema)) 
    {
	itema = CMDPARSE_STD->normal_access(itema, "manip_drop_access",
		this_object(), 1);
	if (sizeof(itema) == 0) 
	{
	    if (silent)
		notify_fail("Nic nie schowa�" + (kobieta() ? "a�" : "e�") + 
		    ".\n");
	    return 0;
	}
	itema = filter(itema, manip_relocate_to);
	if (poorly)
	{
	    bval = this_player()->query_skill(SS_AWARENESS) / 2;
	    val = val > bval * 2 ? bval + random(bval) : val;
	}

	if (sizeof(itema) > 0)
	{
	    this_player()->set_obiekty_zaimkow(itema);
	    write("Chowasz " + COMPOSITE_DEAD(itema, PL_BIE) + ".\n");
	    saybb(QCIMIE(this_player(), PL_MIA) + " chowa co�.\n");
	    itema->add_prop(OBJ_I_HIDE, val);
	    return 1;
	}
    }

    if (!PREV_LIGHT)
	return light_fail("dostrzec cokolwiek");

    if (silent)
    {
	notify_fail("Nic nie schowa�" + (kobieta() ? "a�" : "e�") + ".\n");
	return 0;
    }

    if (!parse_command(str, environment(this_player()),
	"%i:" + PL_BIE + " 'w' %i:" + PL_MIE, itema, cont))
	return 0;

    cont = NORMAL_ACCESS(cont, 0, 0);
    if (!manip_set_dest("w", cont))
    {
	notify_fail("Ukryj w czym ?\n");
	return 0;
    }

    itema = NORMAL_ACCESS(itema, "manip_drop_access", this_object());
    if (sizeof(itema) == 0)
    {
	notify_fail(capitalize(vb) + " co?\n");
	return 0;
    }

    itema = filter(itema, manip_put_dest);
    if (sizeof(itema) > 0)
    {
	this_player()->set_obiekty_zaimkow(itema, cont);
	write("Chowasz " + COMPOSITE_DEAD(itema, PL_BIE) + " do " +
	    gDest->short(PL_DOP) + ".\n");
	saybb(QCIMIE(this_player(), PL_MIA) + " chowa co� do " +
	    QSHORT(gDest, PL_DOP) + ".\n");
	itema->add_prop(OBJ_I_HIDE, (this_player()->query_skill(SS_HIDE) / 2) +
	    random(this_player()->query_skill(SS_HIDE)));
	return 1;
    }

    return 0;
}


/*
 * reveal - Reveal something hidden
 */
int
reveal(string str)
{
    notify_fail("Komenda 'reveal' zosta�a wycofana. Zamiast " +
	"niej mo�esz u�y� 'ujawnij'.\n");

    return 0;
}

/*
 * search - Search something
 */
int
search(string str)
{
    notify_fail("Komenda 'search' zosta�a wycofana. Zamiast " +
	"niej mo�esz u�y� 'szukaj' lub 'przeszukaj'.\n");

    return 0;
}

/*
 * sneak - sneak somewhere.
 */
int
sneak(string str)
{
    notify_fail("Komenda 'sneak' zosta�a wycofana. Zamiast " +
	"niej mo�esz u�y� 'przemknij si�'.\n");
	
    return 0;
}

int trop(string str);

/*
 * szukaj - szuka czegos w pokoju
 */
int
szukaj(string str)
{
    string kom1, kom2;

    gBezokol = "szuka�";

    if (this_player()->query_attack())
    {
	write("Ale przecie� jeste� w �rodku walki!\n");
	return 1;
    }
    
    if (!CAN_SEE_IN_ROOM(this_player()))
	return light_fail("m�c szuka�");

    if(TP->query_mana() < F_SEARCH_MANA_COST + 1)
    {
        notify_fail("Jeste� zbyt wyczerpan" + TP->koncowka("y", "a", "e") + 
        	" mentalnie.\n");
	return 0;
    }

	if(this_player()->query_prop("_sit_siedzacy") ||
		this_player()->query_prop("_sit_lezacy"))
	{
		write("A mo�e wpierw wstaniesz?\n");
		return 1;
	}

	//doda�em zm�czenie, vera.
	TP->add_fatigue(-(random(5)));
	
	if(TP->query_fatigue() < 5)
	{
		notify_fail("Jeste� na to zbyt zm�czon"+TP->koncowka("y", "a", "e") + 
					" fizycznie.\n");
		return 0;
	}

    if (str)
    {
//	if (strlen(str) > 30)
//	    str = "sensu �ycia";


        // --- Zmiany Kruna ----
	// Coby nie pisa�o 'ziol'
	
        if(str ~= "zi�")
	{
            switch(ENV(TP)->query_prop(ROOM_I_TYPE))
            {   
                case ROOM_IN_CITY:
                case ROOM_CAVE:
                    kom1 = "Zaczynasz chodzi� po okolicy szukaj�c zi�.\n";
                    kom2 = QCIMIE(TP, PL_MIA) + " zaczyna chodzi� po okolicy wyra�nie czego� wypatruj�c.\n";
                    break;
                case ROOM_IN_WATER:
		    kom1 = "Zaczynasz brodzi� po okolicy w poszukiwaniu zi�.\n";
                    kom2 = QCIMIE(TP, PL_MIA) + "brodzi� po okolicy wyra�nie czego� szukaj�c.\n";
		    break;
                case ROOM_UNDER_WATER:
                    kom1 = "Zaczynasz p�ywa� z miejsca w miejsce w poszukiwaniu ro�lin wodnych.\n";
		    kom2 = QCIMIE(TP, PL_MIA) + " zaczyna p�ywa� z miejsca w miejsce wyra�nie czego� szukaj�c.\n";
                    break;
                case ROOM_TRACT:
		    kom1 = "Schodzisz z traktu, kl�kasz na ziemi i zaczynasz przegl�da� ro�linno�� w poszukiwaniu zi�.\n";
                    kom2 = QCIMIE(TP, PL_MIA) + " schodzi z traktu, kl�ka na ziemi i zaczyna przegl�da� ro�linno��.\n";
		    break;
		case ROOM_SWAMP:
                    kom1 = "Schylasz si� nieco i zaczynasz przegl�da� b�otnist� ma� w poszukiwaniu zi�.\n";
		    kom2 = QCIMIE(TP, PL_MIA) + " schyla si� nieco i zaczyna uwa�nie przegl�da� b�otnist� ma�, wyra�nie czego� wypatruj�c.\n";
		    break;
                case ROOM_DESERT:
                case ROOM_BEACH:
                case ROOM_IN_AIR:
                case ROOM_TREE:
		    write("Ju� na pierwszy rzut oka widzisz, �e nic tu nie ro�nie.\n");
		    return 1;
            }

	    kom1 = kom1 || "Kl�kasz na ziemi i zaczynasz przegl�da� ro�linno�� " + 
                "w poszukiwaniu zi�.\n";
	    kom2 = kom2 || QCIMIE(TP, PL_MIA) + " kl�ka na ziemi i zaczyna przegl�da� ro�linno��.\n";

            write(kom1);
            saybb(kom2);
        }
	else if(str == "�lad�w")
	    return trop("");
	else
	{	
	    write("Zaczynasz chodzi� po okolicy szukaj�c " + str + ".\n");
	    saybb(QCIMIE(this_player(), PL_MIA) + " zaczyna chodzi� po okolicy wyra�nie czego� wypatruj�c.\n");
	}
    }
    else
    {
	write("Zaczynasz chodzi� po okolicy przeszukuj�c j� dok�adnie.\n");
	saybb(QCIMIE(this_player(), PL_MIA) + " zaczyna chodzi� po okolicy wyra�nie czego� wypatruj�c.\n");
    }
 
    TP->add_mana(-F_SEARCH_MANA_COST);

    environment(this_player())->search_object(str, 0);
    
    return 1;
}


int
spojrz(string str)
{
    string *arr;
    gBezokol = "patrze�";

    if (!str)
    {
        this_player()->do_glance();

        return 1;
    }

    arr = parse_adverb_with_space(str, NO_DEFAULT_ADVERB, 0);
    if (stringp(arr[0]) && arr[0][0..2] == "na ")
        return obejrzyj(arr[0][3..], arr[1] == NO_DEFAULT_ADVERB_WITH_SPACE ? "" : arr[1]);

    notify_fail("Gdzie lub na co chcesz spojrze�?\n", 1);

    return 0;
}

/*
 * Function name: track
 * Description:   look for tracks
 * Argument:      str - the string given to the command
 * Returns:       0 - failure
 */
int
track(string str)
{
    notify_fail("Komenda 'track' zosta�a wycofana. Zamiast " +
	"niej mo�esz u�y� 'trop'.\n");
	
    return 0;
}

int
trop(string str)
{
    object  room = ENV(TP);
    int room_type;

    gBezokol = "tropi�";

    if (this_player()->query_attack())
    {
	notify_fail("Ale jeste� w �rodku walki!\n");
	return 0;
    }
    
    if (!room->query_prop(ROOM_I_IS))
    {
	notify_fail("Nie mo�esz szuka� tu �lad�w!\n");
	return 0;
    }

    if (room->query_prop(ROOM_I_INSIDE))
    {
	notify_fail("�lad�w mo�esz szuka� tylko na otwartej przestrzeni!\n");
	return 0;
    }
    
    room_type = room->query_prop(ROOM_I_TYPE);
    if(room_type == ROOM_IN_AIR)
    {
        notify_fail("W powietrzu nie znajdziesz �adnych �lad�w.\n");
        return 0;
    }
    else if(room_type == ROOM_UNDER_WATER)
    {
        notify_fail("Wszelkie �lady jakie tu mog�e� znale�� ju� dawno zosta�y zmyte przez wod�.\n");
        return 0;
    }
    else if(room_type == ROOM_IN_WATER)
    {
	notify_fail("Nie mo�esz szuka� �lad�w w wodzie!\n");
	return 0;
    }
    else if(room_type == ROOM_TREE)
    {
        notify_fail("Nie mo�esz szuka� tutaj �lad�w.\n");
        return 0;
    }
    else if(room_type == ROOM_IN_CITY)
    {
    	notify_fail("W mie�cie nie mo�esz szuka� �lad�w.\n");
	    return 0;
    }


    if(this_player()->query_prop("_sit_siedzacy") || 
	   this_player()->query_prop("_sit_lezacy"))
    {
	    notify_fail("Mo�e najpierw wstaniesz?\n");
	    return 0;
    }

    if (this_player()->query_mana() < 2*F_TRACK_MANA_COST)
    {
	    notify_fail("Jeste� zbyt wyczepran" + (kobieta() ? "a" : "y") + 
	                " mentalnie.\n");
	    return 0;
    }

    write("Kl�kasz aby dok�adnie obejrze� grunt w poszukiwaniu �lad�w.\n");
    saybb(QCIMIE(this_player(), PL_MIA) + " kl�ka, by obejrze� dok�adnie " +
	"grunt.\n");

    this_player()->add_prop(LIVE_S_EXTRA_SHORT, " kl�cz�c" +
	this_player()->koncowka("y", "a") + " na ziemi");
    this_player()->add_mana(-F_TRACK_MANA_COST);

    room->track_room();
    return 1;
}


/*
 * ujawnij - Zdekonspiruj cos ukrytego ;-)
 */
int
ujawnij(string str)
{
    object *itema, *cont, linked, *obarr;
    string vb, prep, items;
    int i, size;
    
    if (!PREV_LIGHT) return light_fail("dostrzec cokolwiek");

    vb = query_verb();
    
    gBezokol = "ujawni�";
    
    notify_fail(capitalize(vb) + " co?\n");  /* access failure */

    if (!stringp(str))
	return 0;

    gFrom = ({});


    if (str == "mnie" || str ~= "si�" || str == "siebie")
    {
	if (this_player()->reveal_me(0))
	{
	    write("Wychodzisz z ukrycia.\n");
	    return 1;
	}
	else
	{
	    notify_fail("Przecie� nie jeste� ukryt" + 
		(kobieta() ? "a" : "y") + ".\n");
	    return 0;
	}
    }

    silent = 0;
    if (str == "wszystko")
	silent = 1;

    if (parse_command(str, environment(this_player()), "%i:" + PL_BIE, itema))
    {
//FIXME!!!!! Ujawnianie obiektu heapowanego nie robi split_heap
//przez to 'ujawnij grosz' ujawnia WSZYSTKIE grosze!!! FIXME!!!!!


//	itema = CMDPARSE_STD->normal_access(itema, 0, 0, 1);
	itema = NORMAL_ACCESS(itema, 0, 0);
	itema = filter(itema, &->query_prop(OBJ_I_HIDE));
	if (sizeof(itema) == 0)
	{
	    if (silent)
		notify_fail("Nic nie ujawni�" + 
		    (kobieta() ? "a�" : "e�") + ".\n");
	    return 0;
	} 
	if (sizeof(itema))
	{
	    itema->remove_prop(OBJ_I_HIDE);
	    this_player()->set_obiekty_zaimkow(itema);

	    write("Ujawniasz " + COMPOSITE_DEAD(itema, PL_BIE) + ".\n");
	    obarr = FILTER_LIVE(itema);
	    say(QCIMIE(this_player(), PL_MIA) + " ujawnia " +
		QCOMPDEAD(PL_BIE) + ".\n", obarr + ({ this_player() }));
	    size = sizeof(obarr);
	    if (size == 1)
	        obarr[0]->catch_msg(this_player()->query_Imie(obarr[i],
		    PL_MIA) + " ujawnia ci�.\n");
	    else if (size > 1)
	    {
		i = -1;
		while (++i < size)
		obarr[i]->catch_msg(this_player()->query_Imie(obarr[i],
		    PL_MIA) + " ujawnia ciebie oraz " +
		    FO_COMPOSITE_LIVE((obarr - ({ obarr[i] })), obarr[i],
		    PL_BIE) + ".\n");
	    }
	    itema->force_heap_merge();
	    
	    return 1;
	}
    }

    if (silent)
    {
	notify_fail("Nic nie ujawni�" + 
	    (kobieta() ? "a�" : "e�") + ".\n");
	return 0;
    }

    gFrom = ({});
    
/*
 * Konstrukcja nie dziala w oryginale... nie chce mi sie na razie u nas
 * poprawiac
 */
/*
    if (parse_command(str, environment(this_player()),
		      "%s 'w' %i:" + PL_MIE, items, cont))
    {
	gContainers = NORMAL_ACCESS(cont, 0, 0);
	gContainers = filter(gContainers, &->query_prop(OBJ_I_HIDE));
	gContainers = FILTER_DEAD(gContainers);
	if (sizeof(gContainers) == 0)
	{
	    notify_fail(capitalize(vb) + " w czym?\n");
	    return 0;
	}

	if (linked = gContainers[0]->query_room())
	    obarr = all_inventory(linked);
	else
	    obarr = deep_inventory(gContainers[0]);

	if (!parse_command(items, obarr, "%i:" + PL_BIE, itema))
	    return 0;

	itema = NORMAL_ACCESS(itema, "in_gContainers", this_object());
	if (sizeof(itema) == 0) 
	    return 0;

	itema->remove_prop(OBJ_I_HIDE);

	write("Ujawniasz " + vb + " " + COMPOSITE_DEAD(itema, PL_BIE) + 
	   " w " + gContainers[0]->short(PL_NAR) + ".\n");
	say(QCIMIE(this_player(), PL_MIA) + " ujawnia " + QCOMPDEAD(PL_BIE) +
	    " w " + QSHORT(gContainers[0], PL_NAR) + ".\n");
	return 1;
    }
*/
    if (environment(this_player())->item_id(str))
    {
	notify_fail("Nie mo�esz tego ujawni�.\n");
	return 0;
    }

    return 0;
}


/* 28.06.2005 Jeremian
 *
 * Du�e zmiany w funkcji wez. Podzia� na wyjmij, wez i zdejmij.
 * Zmian na tyle du�o, �e poszczeg�lne linie nie s� komentowane.
 */

int
sublokacja_pasuje(object ob)
{
    string skad = gFromMiejsce;
    
    // testy wzi�te z in_gContainers
     
    if (!objectp(ob)) {
	return 0;
    }
    if ((environment(ob) != gContainers[0]) && (environment(ob) != gContainers[0]->query_room())) {
	return 0;
    }
    
    // pozosta�e testy
    
//    write("sublokacja_pasuje\n");
//    write("ob: [" + file_name(ob) + "]\n");
//    write("skad: [" + skad + "]\n");
//    if (pointerp(ob->query_subloc())) {
//      write("sublok:\n");
//      dump_array(ob->query_subloc());
//    }
//    else {
//      write("sublok: [" + ob->query_subloc() + "]\n");
//    }
//    write("---\n");
    if (subloc_filter(ob->query_subloc(), skad, PL_DOP) ||
		    (stringp(ob->query_subloc()) && (skad == ""))) {
//      write(">>>\n");
		    if (gContainers[0]->liberal_parse_subloc_type(ob->query_subloc()) == gTypSublokacji) {
			    return 1;
		    }
		    switch (gTypSublokacji) {
			    case 0: case 1:
				    if (gContainers[0]->query_subloc_prop(ob->query_subloc(),
							    SUBLOC_I_TYP_W) == 1) {
					    return 1;
				    }
				    break;
			    case 2:
				    if (gContainers[0]->query_subloc_prop(ob->query_subloc(),
							    SUBLOC_I_TYP_NA) == 1) {
					    return 1;
				    }
				    break;
			    case 3:
				    if (gContainers[0]->query_subloc_prop(ob->query_subloc(),
							    SUBLOC_I_TYP_POD) == 1) {
					    return 1;
				    }
				    break;
		    }
    }
    else if (gContainers[0]->parse_subloc_type(ob->query_subloc()) == gTypSublokacji) {
//      write(">>> else\n");
	    if (gFromMiejsce == "") {
		    return 1;
	    }
    }
    
//    write("nie mozna (" + ob->short()+ "), skad=[" + skad + "], obs=[" + ob->query_subloc() + "]\n");
    // obiektu nie mo�na bra�
    return 0;
}


/**
 * Funkcja obs�uguj�ca pr�b� brania (wyjmowania, zdejmowania) czego�.
 *
 * Mo�liwe s� nast�puj�ce komendy:
 * <ul>
 *   <li> <i>we�</i> co�
 *   <li> <i>we�</i> co� z czego�
 *   <li> <i>we�</i> co� spod czego�
 *   <li> <i>wyjmij</i> co� z czego�
 *   <li> <i>wyjmij</i> co� spod czego�
 *   <li> <i>zdejmij</i> co� z czego�
 * </ul>
 */
int
wez(string str)
{
  object *itema, *cont, linked, *obarr;
  string vb, prep, items;
  int i, il_rzeczy, nfilt;

  gMiejsce = "";
  gPrep = "";
  gFromMiejsce = "";
  gTypSublokacji = 0;
  prep = "";
  nfilt = 0;

  if (!PREV_LIGHT) return light_fail("bra� cokolwiek");

  vb = query_verb();
  if (vb ~= "zdejmij") {
    gBezokol = "zdj��";
  }
  else if (vb ~= "wyjmij") {
    gBezokol = "wyj��";
  }
  else {
    gBezokol = "wzi��";
  }

  notify_fail(capitalize(vb) + " co?\n");  /* access failure */

  if (!stringp(str))
    return 0;

  gFrom = ({});

  /* This is done to avoid all those stupid messages 
     when you try 'get all' 
     */
  silent = 0;
  if (str == "wszystko")
    silent = 1;

  if (parse_command(str, environment(this_player()), "%i:" + PL_BIE, itema) && (vb ~= "we�"))
  {
    itema = NORMAL_ACCESS(itema, 0, 0);
    itema = filter(itema, &->is_in_subloc(0));
    itema = filter(itema, manip_relocate_from);
    if (sizeof(itema) == 0)
    {
      if (silent)
        notify_fail("Nic nie " + gBezokol[0..2] + (kobieta() ? "�a�" : "��e�") +
            ".\n");
      return 0;
    } 
    if (sizeof(itema) > 0)
    {
      this_player()->set_obiekty_zaimkow(itema);
      itema->remove_prop(OBJ_I_HIDE);
      write("Bierzesz " + COMPOSITE_DEAD(itema, PL_BIE) + ".\n");
      say(QCIMIE(this_player(), PL_MIA) + " bierze " + QCOMPDEAD(PL_BIE) + ".\n");
      il_rzeczy = sizeof(itema);
      for (i = 0; i < il_rzeczy; ++i) {
        if (itema[i]->czy_cosbylopod()) {
          write("Pod " + itema[i]->short(PL_NAR) + " co� by�o.\n");
          say("Pod " + itema[i]->short(PL_NAR) + " co� by�o.\n");
        }
        if (itema[i]->czy_cosbylona()) {
          write("Z " + itema[i]->short(PL_DOP) + " co� spad�o.\n");
          say("Z " + itema[i]->short(PL_DOP) + " co� spad�o.\n");
        }
      }

      itema->force_heap_merge();

      return 1;
    }
  }

  if (silent)
  {
    notify_fail("Nic nie " + gBezokol[0..2] + (kobieta() ? "�a�" : "��e�") +
        ".\n");
    return 0;
  }

  gFrom = ({});

  /*
   * Najpierw sprawdzamy, czy bierzemy z jakiegos kontenera na lokacji
   */

  if (parse_command(str, environment(this_player()),
        "%s 'z' %s %i:" + PL_DOP, items, gFromMiejsce, cont))
  {
    prep = "z";
    nfilt = 1;
  }
  else if (parse_command(str, environment(this_player()),
        "%s 'ze' %s %i:" + PL_DOP, items, gFromMiejsce, cont))
  {
    prep = "ze";
    nfilt = 1;
  }
  else if (parse_command(str, environment(this_player()),
        "%s 'spod' %s %i:" + PL_DOP, items, gFromMiejsce, cont) &&
      ((vb ~= "wyjmij") || (vb ~= "we�")))
  {
    prep = "spod";
    nfilt = 1;
  }
  /*
   * Nast�pnie sprawdzamy sublokacje na lokacji
   */
  else if (parse_command(str, environment(this_player()),
        "%s 'z' %s", items, gFromMiejsce))
  {
    prep = "z";
    gContainers = ({ environment(this_player()) });
  }
  else if (parse_command(str, environment(this_player()),
        "%s 'ze' %s", items, gFromMiejsce))
  {
    prep = "ze";
    gContainers = ({ environment(this_player()) });
  }
  else if (parse_command(str, environment(this_player()),
        "%s 'spod' %s", items, gFromMiejsce) &&
      ((vb ~= "wyjmij") || (vb ~= "we�")))
  {
    prep = "spod";
    gContainers = ({ environment(this_player()) });
  }

  silent = 0;
  if (items == "wszystko")
    silent = 1;

  gPrep = prep;

  if (vb ~= "zdejmij") {
    if ((prep ~= "z") || (prep ~= "ze")) {
      gTypSublokacji = 2;
    }
    else {
      gTypSublokacji = 4;
    }
  }
  else { // we� / wyjmij
    if ((prep ~= "z") || (prep ~= "ze")) {
      gTypSublokacji = 1;
    }
    else if (prep ~= "spod") {
      gTypSublokacji = 3;
    }
    else {
      gTypSublokacji = 4;
    }
  }

  if (prep != "") {
    if (nfilt) {
      gContainers = NORMAL_ACCESS(cont, 0, 0);
      gContainers = FILTER_DEAD(gContainers);
    }
    if (sizeof(gContainers) == 0)
    {
      notify_fail(capitalize(vb) + " sk�d?\n");
      return 0;
    }

    if (linked = gContainers[0]->query_room())
      obarr = all_inventory(linked);
    else
      obarr = deep_inventory(gContainers[0]);

//    gFromMiejsce = prep + ((gFromMiejsce == "") ? "" : (" " + gFromMiejsce));

    // ograniczenie obiekt�w tylko do tych z prawid�owych sublokacji
//    write("gFromMiejsce=[" + gFromMiejsce + "], sizeof(obarr)=[" + sizeof(obarr) + "]\n");
//    dump_array(obarr);
    obarr = filter(obarr, sublokacja_pasuje);
//    write("gFromMiejsce=[" + gFromMiejsce + "], sizeof(obarr)=[" + sizeof(obarr) + "]\n");
//    dump_array(obarr);
    if (sizeof(obarr) == 0) 
      return 0;

    if (!parse_command(items, obarr, "%i:" + PL_BIE, itema))
      return 0;

//    write("sizeof(itema)=[" + sizeof(itema) + "]\n");
    itema = NORMAL_ACCESS(itema, "in_gContainers", this_object());
//    write("sizeof(itema)=[" + sizeof(itema) + "]\n");
    if (sizeof(itema) == 0) 
      return 0;

    itema = filter(itema, manip_relocate_from);
    if (sizeof(itema) == 0)
      return 0;

    this_player()->set_obiekty_zaimkow(itema, gContainers);
    itema->remove_prop(OBJ_I_HIDE);
    write(((vb ~= "zdejmij") ? "Zdejmujesz" : ((vb ~= "wyjmij") ? "Wyjmujesz" : "Bierzesz")) + " " + COMPOSITE_DEAD(itema, PL_BIE) + " " + prep +
        (pointerp(gPreviousSubloc) ? " " + gPreviousSubloc[PL_DOP] : "") +
        (nfilt ? " " + gContainers[0]->short(PL_DOP) : "") + ".\n");
    say(QCIMIE(this_player(), PL_MIA) + " " + ((vb ~= "zdejmij") ? "zdejmuje" : ((vb ~= "wyjmij") ? "wyjmuje" : "bierze")) + " " + QCOMPDEAD(PL_BIE) +
        " " + prep +
        (pointerp(gPreviousSubloc) ? " " + gPreviousSubloc[PL_DOP] : "") +
        (nfilt ? " " + QSHORT(gContainers[0], PL_DOP) : "") + ".\n");
    il_rzeczy = sizeof(itema);
    for (i = 0; i < il_rzeczy; ++i) {
      if (itema[i]->czy_cosbylopod()) {
        write("Pod " + itema[i]->short(PL_NAR) + " co� by�o.\n");
        say("Pod " + itema[i]->short(PL_NAR) + " co� by�o.\n");
      }
      if (itema[i]->czy_cosbylona()) {
        write("Z " + itema[i]->short(PL_DOP) + " co� spad�o.\n");
        say("Z " + itema[i]->short(PL_DOP) + " co� spad�o.\n");
      }
    }

    itema->force_heap_merge();

    return 1;
  }

  if (environment(this_player())->item_id(str))
  {
    notify_fail("Nie mo�esz tego " + gBezokol[0..2] + "��.\n");
    return 0;
  }

  if (silent)
  {
    notify_fail("Nic nie " + gBezokol[0..2] + (kobieta() ? "�a�" : "��e�") +
        ".\n");
    return 0;
  }

  return 0;
}

/*
 * wloz - Wloz cos
 */
int
wloz(string str)
{
    object *itema;
    object *cont;
    string prep, vb;
    int nfilt;
    
    gMiejsce = "";
    /* jeremian's changes */
    gPrep = "";
    gDest = 0;
    gItem = 0;
    nfilt = 0;
    gPisacODest = 1;
    /* --- */
    vb = query_verb(); 
    notify_fail(capitalize(vb) + " co do czego?\n");
    
    if (!stringp(str))
	return 0;

    silent = 0;
    
    if (!PREV_LIGHT)
	return light_fail("dostrzec cokolwiek.");
	
    gBezokol = "w�o�y�";

    if (parse_command(str, environment(this_player()), 
	"%i:" + PL_BIE + " %w %s %i:" + PL_DOP, itema, gPrep, gMiejsce, cont)) {
	    nfilt = 1;
    }
    else if (parse_command(str, environment(this_player()), 
	"%i:" + PL_BIE + " %w %s %i:" + PL_BIE, itema, gPrep, gMiejsce, cont)) {
	    nfilt = 1;
    }
    else if (parse_command(str, environment(this_player()), 
	"%i:" + PL_BIE + " %w %s", itema, gPrep, gMiejsce)) {
	    cont = ({ environment(this_player()) });
	    gPisacODest = 0;
        if (gMiejsce == "") {
            return 0;
        }
    }
    else {
	return 0;
    }

//    write("wloz\n");
//    write("gPrep=[" + gPrep + "]\n");
//    write("gMiejsce=[" + gMiejsce + "]\n");
    
    prep = gPrep;

    if (nfilt) {
	    cont = NORMAL_ACCESS(cont, 0, 0);
    }
    if (!manip_set_dest(prep, cont))
    {
	return 0;
    }
   
    itema = NORMAL_ACCESS(itema, "manip_drop_access", this_object());
    if (sizeof(itema) == 0)
    {
	notify_fail(capitalize(vb) + " co?\n");
	return 0;
    }
 
    /* jeremian's changes */
    
    if (gMiejsce == "" && prep == "pod" && gDest->query_prop(CONT_I_CANT_WLOZ_POD)) {
	    notify_fail("Nie mo�na nic w�o�y� pod " +
	    gDest->short(PL_BIE) + ".\n");
	    say(QCIMIE(this_player(), PL_MIA) + " bezskutecznie pr�buje w�o�y� " +
	    COMPOSITE_DEAD(itema, PL_BIE) + 
	    " pod " + QSHORT(gDest, PL_BIE) + ".\n");
	    return 0;
    }

    if (gMiejsce == "" && prep == "do" && gDest->query_prop(CONT_I_CANT_WLOZ_DO)) {
	    notify_fail("Nie mo�na nic w�o�y� do " +
	    gDest->short(PL_DOP) + ".\n");
	    say(QCIMIE(this_player(), PL_MIA) + " bezskutecznie pr�buje w�o�y� " +
	    COMPOSITE_DEAD(itema, PL_BIE) + 
	    " do " + QSHORT(gDest, PL_DOP) + ".\n");
	    return 0;
    }
 
    
    gMiejsce = prep + ((gMiejsce == "") ? "" : (" " + gMiejsce));

    /* --- */
//    write("wloz 2\n");
//    write("---\n");
    
    itema = filter(itema, manip_put_dest);
    
    if (sizeof(itema) > 0)
    {
	this_player()->set_obiekty_zaimkow(itema, cont);
	if (gMiejsce == prep) {
	write("Wk�adasz " + COMPOSITE_DEAD(itema, PL_BIE) + " " + prep + " " +
	    (prep == "pod" ? gDest->short(PL_BIE) : gDest->short(PL_DOP)) + ".\n");
	say(QCIMIE(this_player(), PL_MIA) + " wk�ada " + COMPOSITE_DEAD(itema, PL_BIE) + 
		" " + prep + " " + (prep == "pod" ? QSHORT(gDest, PL_BIE) : QSHORT(gDest, PL_DOP)) + ".\n");
	}
	else {
	write("Wk�adasz " + COMPOSITE_DEAD(itema, PL_BIE) + " " + prep + " " + itema[0]->query_subloc()[PL_DOP] +
	    (gPisacODest ? " " + gDest->short(PL_DOP) : "") + ".\n");
	say(QCIMIE(this_player(), PL_MIA) + " wk�ada " + COMPOSITE_DEAD(itema, PL_BIE) + 
		" " + prep + " " + itema[0]->query_subloc()[PL_DOP] +
		(gPisacODest ? " " + QSHORT(gDest, PL_DOP) : "") + ".\n");
	}
	return 1;
    }

/*
 * Blad powinien byc wypisany przez manip_relocate_to(), ktora wywola
 * move_err_short()
 *
 *    notify_fail(capitalize(vb) + " " + prep + " czego?\n");
 */
    notify_fail("");
    return 0;
}

/*
 * keep - set the OBJ_M_NO_SELL property in an object.
 * unkeep - remove the OBJ_M_NO_SELL property from an object.
 */
int
zabezpiecz(string str)
{
    object *objs;
    object *keep_objs;
    int    keep = (query_verb() == "zabezpiecz");
    int    list;

    gBezokol = (keep ? "zabezpieczy�" : "odbezpieczy�");

    if (!stringp(str))
    {
	notify_fail(capitalize(query_verb()) + " co?\n");
	return 0;
    }

    /* Player wants to list, remove the flag. */
    if (list = wildmatch("poka^z*", str))
    {
	str = extract(str, 6);
    }

    /* Playes wants to list, but didn't give any argument. Get all items
     * in his/her inventory.
     */
    if (list &&
	!strlen(str))
    {
	if (!sizeof(objs = FILTER_CAN_SEE(all_inventory(this_player()),
	    this_player())))
	{
	    notify_fail("Nie masz nic przy sobie.\n");
	    return 0;
	}
    }
    /* Or parse the argument to see which items to process. */
    else if (!parse_command(str, this_player(), "%i:" + PL_BIE, objs) ||
	!sizeof(objs = NORMAL_ACCESS(objs, 0, 0)))
    {
	notify_fail(capitalize(query_verb()) + (list ? " poka^z" : "") +
	    " co?\n");
	return 0;
    }
    
    this_player()->set_obiekty_zaimkow(objs);

    /* Filter all non-keepable objects. */
    keep_objs = filter(objs, &not() @ &->query_keepable());

    /* List the 'keep' status of the selected items. */
    if (list)
    {
	if (sizeof(keep_objs))
	{
	    write("Niezabezpieczalne --------------\n" +
		break_string(COMPOSITE_DEAD(keep_objs, 0), 70, 5) + "\n");
	    objs -= keep_objs;
	}

	/* Filter all kept objects. */
	keep_objs = filter(objs, &->query_keep());
	if (sizeof(keep_objs))
	{
	    write("Zabezpieczone ------------\n" +
	 	break_string(COMPOSITE_DEAD(keep_objs, 0), 70, 5) + "\n");
	    objs -= keep_objs;
	}

	/* The remainder is keepable, but not kept. */
	if (sizeof(objs))
	{
	    write("Niezabezpieczone --------\n" +
		break_string(COMPOSITE_DEAD(objs, 0), 70, 5) + "\n");
	}
	
	return 1;
    }

    /* None of the objects are keepable. */
    if (sizeof(keep_objs) == sizeof(objs))
    {
	notify_fail("Niezabezpieczalne: " +
	    COMPOSITE_DEAD(keep_objs, 0) + ".\n");
	return 0;
    }

    /* Now select the objects to (un)keep. First remove the non-keepable
     * objects.
     */
    objs -= keep_objs;
    if (keep)
    {
	keep_objs = filter(objs, &not() @ &->query_keep());
    }
    else
    {
	keep_objs = filter(objs, &->query_keep());
    }

    /* No objects to process. */
    if (!sizeof(keep_objs))
    {
	notify_fail((keep ? "Ju� zabezpieczone: " : "niezabezpieczone: ") +
	    COMPOSITE_DEAD(objs, 0) + ".\n");
	return 0;
    }

    keep_objs->set_keep(keep);
    write((keep ? "Zabezpieczone: " : "Odbezpieczone: ") + 
	COMPOSITE_DEAD(keep_objs, 0) + ".\n");
    return 1;
}

int /*Wypasiona funkcja :P Zerznieta z arki. :P */
zerknij(string str)
{
    gBezokol = "zerka�";
  
    if (!str)
    {
        this_player()->do_glance(2);
        return 1;
    }
    /* --- Zmiany Kruna ---*/
    else
    {
       notify_fail("Na co chcesz zerkn��?\n", 2);
       if(str[0..2] != "na ")
           return 0;
       return obejrzyj(str[3..-1]);
    }
    /*---------------------*/
    notify_fail("Zerknij?\n");
    return 0;
}
