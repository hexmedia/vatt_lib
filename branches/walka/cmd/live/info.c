/**
 * /cmd/live/info.c
 *
 */

#pragma no_clone
#pragma no_inherit
#pragma save_binary
#pragma strict_types

inherit "/cmd/std/command_driver";

#include <cmdparse.h>
#include <language.h>
#include <log.h>
#include <macros.h>
#include <std.h>
#include <time.h>
#include <stdproperties.h>
#include <flags.h>
#include <filepath.h>
#include <mail.h>
#include <colors.h>

/* These properties are only internal to this module, so we define them
 * here rather than in the properties file stdproperties.h.
 */
#define PLAYER_I_LOG_TYPE   "_player_i_log_type"
#define PLAYER_O_LOG_OBJECT "_player_o_log_object"

#define COMMON_BOARD        "/d/Standard/wiz/garstang/wiesciboard"
#define COMMON_BOARD_ROOM   "/d/Standard/wiz/garstang/wiesci"

object Common_Board = 0;

/* **************************************************************************
 * The constructor.
 */
void 
create()
{
    setuid();
    seteuid(getuid()); 
}

/* **************************************************************************
 * Return a proper name of the soul in order to get a nice printout.
 */
string
get_soul_id()
{
    return "info";
}

/* **************************************************************************
 * This is a command soul.
 */
int
query_cmd_soul()
{
    return 1;
}

/* **************************************************************************
 * The list of verbs and functions. Please add new in alfabetical order.
 */
mapping
query_cmdlist()
{
    return ([
        "?"             : "command_help",

        "pomoc"         : "pomoc",
        "pomocy"        : "pomoc",

        "system"        : "system",

        "wie�ci"        : "wiesci",

        "zg�o�"         : "zglos",

        "test_na_obecnosc_paralizu" : "test_na_obecnosc_paralizu" ]);
        //Co to za funkcja???? Potrzebna jeszcze? Krun
}

/*
 * Function name: using_soul
 * Description:   Called once by the living object using this soul. Adds
 *		  sublocations responsible for extra descriptions of the
 *		  living object.
 */
public void 
using_soul(object live)
{
}

/* **************************************************************************
 * Here follow some support functions. 
 * **************************************************************************/

/*
 * Function name: done_reporting
 * Descripiton  : This function is called from the editor when the player
 *                has finished the report he or she was writing. If any
 *                text was entered it is logged and the player is thanked.
 * Arguments    : string str - the text entered into the editor.
 */
public void
done_reporting(string str, int flag, object target, int tresc, string przyimek, int przyp)
{
    int type = this_player()->query_prop(PLAYER_I_LOG_TYPE);
    object acttrg = this_player()->query_prop(PLAYER_O_LOG_OBJECT);

    if (!strlen(str))
    {
        write(LOG_ABORT_MSG(LOG_MSG(flag)));
        return;
    }

    /* Log the note, thank the player and then clean up after ourselves. */
    SECURITY->note_something(str, flag, acttrg);
    write(LOG_THANK_MSG(LOG_MSG(flag)));

    this_player()->remove_prop(PLAYER_I_LOG_TYPE);
    this_player()->remove_prop(PLAYER_O_LOG_OBJECT);

    int sys;
    string slowo;

    //Pocz�tek kodu info dla wiz�w by Vera, poprawki Krun
    switch(flag)
    {
        case LOG_SYSPRAISE_ID: slowo = "globaln� pochwa��"; sys = 1;    break;
        case LOG_PRAISE_ID:    slowo = "pochwa��";                      break;
        case LOG_SYSIDEA_ID:   slowo = "globalny pomys�";   sys = 1;    break;
        case LOG_IDEA_ID:      slowo = "pomys�";                        break;
        case LOG_SYSBUG_ID:    slowo = "globalny b��d";     sys = 1;    break;
        case LOG_BUG_ID:       slowo = "b��d";                          break;
        case LOG_BREAKDOWN_ID: slowo = "naruszenie zasad";              break;
        case LOG_MISPRINT_ID:  slowo = "liter�wk�";                     break;
        default: slowo = "komentarz";
    }

    object *us = filter(users(), &->query_wiz_level());

    foreach(mixed u : us)
    {
        //Zg�aszanie narusze� zasad.. Krun
        if(flag == LOG_BREAKDOWN_ID &&
            !(u->query_prop(WIZARD_I_BUSY_LEVEL) & BUSY_C)
            && member_array(u->query_real_name(), EXPAND_MAIL_ALIAS("mg")) >= 0)
        {
            u->catch_msg(set_color(u, COLOR_FG_RED) + "ZG�OSZENIE: " + 
                UC(TP->query_name(PL_MIA)) + " zg�asza " + 
                "naruszenie zasad przez " + UC(target->query_name(PL_BIE)) + ".\n" +
                clear_color());
        }
        else if (flag != LOG_BREAKDOWN_ID && interactive(u) &&
            !(u->query_prop(WIZARD_I_BUSY_LEVEL) & BUSY_C))
        {
            if(tresc)
            {
                u->catch_msg(set_color(u, COLOR_FG_RED) + "ZG�OSZENIE: " +
                   this_player()->query_name(PL_MIA)+" zg�asza " +
                   slowo + (!sys ? " w " + (acttrg->is_room() ? "lokacji " : "") +
                   acttrg->short(this_player(), przyp) +
                   " ("+RPATH(file_name(acttrg)) +") " : "") + ".\n" + clear_color());
            }
            else
            {
                //A to to nie wiem po kija tu jest.. Jak nie ma tre�ci to nie ma zg�oszenia
                //chyba proste? Krun.
                object env;
                u->catch_msg(SET_COLOR(COLOR_FG_RED) + "ZG�OSZENIE: " +
                    this_player()->query_name(PL_MIA)+" zg�asza "+
                    slowo + " " + (type >= LOG_SYSBUG_ID ? LOG_GLOBAL_NAME(type) : "w lokacji: "+
                    ((env = environment(this_player())) ? RPATH(file_name(env)) : "VOID"))+
                    ".\n" + clear_color());
            }
        }
    }

    /* koniec kodu info dla wiz�w */
}

/* **************************************************************************
 * Now the actual commands. Please add new in the alphabetical order.
 * **************************************************************************/


/**
 * Wy�wietla pomoc na dany temat.
 */
int
command_help(string str)
{
    string dir = "general/";

    if (!str)
        str = "pomoc";

    if(str == "?")
        str = "pomoc";

    str = plain_string(str);

    /* Wizards get to see the wizard help pages by default. */
    if (SECURITY->query_wiz_level(this_player()->query_real_name()))
    {
	/* ... unless they want to see the general page. */
	if (wildmatch("g *", str))
	{
	    str = extract(str, 2);
	}
	else if (file_size("/doc/help/wizard/" + str) > 0)
	{
	    dir = "wizard/";
	}
    }

    if (file_size("/doc/help/" + dir + str) > 0)
    {
    	setuid();
    	seteuid(getuid());

	this_player()->more(("/doc/help/" + dir + str), 1, 0, 1);
	return 1;
    }

    notify_fail("Nie ma pomocy na ten temat.\n");
    return 0;
}

/**
 * Podaje sk�adnie pomocy.
 */
int
pomocy(string str)
{
    return command_help("?");
}

/**
 * Podaje lokalny czas, dat�, oraz podstawowe informacje o mudzie.
 */
int
system()
{
    int runlevel;

    write("�wiat odrodzi� si�  : " + ctime(SECURITY->query_start_time(), 1) + 
    					"\n");
    write("Lokalny czas        : " + ctime(time(), 1) + "\n");
    write("�wiat istnieje      : " + CONVTIME(time() - 
        SECURITY->query_start_time()) + "\n");
    write(SECURITY->query_memory_percentage() + "% �wiata zosta�o opanowane "+
        "przez Ciemno��.\n");
#ifdef REGULAR_REBOOT
    write("Regularny restart: Codziennie po " + REGULAR_REBOOT + ":00\n");
#endif REGULAR_REBOOT

    /* Tell wizards some system data. */
    if (this_player()->query_wiz_level())
    {
//	string load_average = SECURITY->do_debug("load_average");

#ifdef REGULAR_UPTIME
        write("Regularna Apokalipsa: " +
            CONVTIME(SECURITY->query_irregular_uptime() +
            SECURITY->query_start_time() - time()) + " to go.\n");
#endif REGULAR_UPTIME

	write("Dane systemowe      :\n\t" +
	    implode(explode(SECURITY->do_debug("load_average"), ", "), ",\n\t") +
	    "\n");

        if (runlevel = SECURITY->query_runlevel())
        {
            write("Runlevel      : " + WIZ_RANK_NAME(runlevel) +
                " (i wy�ej).\n");
        }
    }

    if (ARMAGEDDON->shutdown_active())
    {
	write("Do rozpocz�cia Apokalipsy " + 
	    CONVTIME(ARMAGEDDON->shutdown_time()) + ".\n");
        if (!this_player()->query_wiz_level())
        return 1;
	    
	write("Wywo�ana przez      : " + capitalize(ARMAGEDDON->query_shutter()) +
	     ".\n");

    string powod = ARMAGEDDON->query_reason();

    if(powod && powod != "")
        write("Pow�d               : " + powod + "\n");
    }

    return 1;
}

int
test_na_obecnosc_paralizu(string str)
{
    notify_fail("");
    return 0;
}

/**
 * Zg�aszanie pomys��w, liter�wek, naruszenia zasad, itp.
 */
int
zglos(string str)
{
    object *oblist;
    int type = 0;
    string parse_string;

    notify_fail(
        "Prawid�owa sk�adnia: 'zg�o� [globalny] b��d [w <obiekcie>]'\n" +
        "                     'zg�o� [globalny] pomys� [do <obiekt>]'\n" +
        "                     'zg�o� [globaln�] pochwa�� [za <obiekt>]'\n" + 
        "                     'zg�o�            liter�wke [w <obiekcie>]'\n" +
        "                     'zg�o�            naruszenie zasad przez <imi� gracza>'\n");

    if (!strlen(str))
        return 0;

    string *slowa = explode(str, " ");

    if(wildmatch("naruszenie zasad*", str))
    {
        notify_fail("Musisz sprecyzowa� przez kogo zasady zosta�y naruszone.\n");

        if(str[17..21] != "przez" || strlen(str) <= 23)
            return 0;

        string imie;

        if(sscanf(str, "naruszenie zasad przez %s", imie) != 1)
            return 0;

        object lamiacy;
        if(!(lamiacy = SECURITY->finger_player(imie, PL_BIE)))
        {
            notify_fail("Nie ma gracza o takim imieniu!\n"); 
            return 0;
        }

        this_player()->add_prop(PLAYER_I_LOG_TYPE, type);
        this_player()->add_prop(PLAYER_O_LOG_OBJECT, lamiacy);

        setuid();
        seteuid(getuid());

        if(lamiacy->query_real_name() == TP->query_real_name())
            write("Zg�aszasz, �e naruszy�e� zasady. Z pewno�ci� b�dzie to wzi�te "+
                "pod uwag� podczas rozpatrywania twojego przewnienia.\n");
        else
            write("Zg�aszasz naruszenie zasad przez " + lamiacy->query_name(PL_BIE) + ".\n");

        clone_object(EDITOR_OBJECT)->edit(&done_reporting(, LOG_BREAKDOWN_ID, lamiacy)); 
        return 1;	
    }

    string slowo;

    if (slowa[0][0..6] == "globaln" && sizeof(slowa) > 1)
    {
        type = LOG_TYPES[implode(slowa[0..1], " ")];
        slowo = slowa[1];
        str = implode(slowa[2..], " ");
    }
    else
    {
        slowo = slowa[0];
       	str = implode(slowa[1..], " ");
        type = LOG_TYPES[slowo];
    }
    if(!type)
        return 0;
    int tresc = !!str;
    int przyp;
    string przyimek;

    object target;
    /* Player may describe the object to make a report about. */
    if (strlen(str))
    {
        switch(type)
        {
            case LOG_PRAISE_ID:
            case LOG_SYSPRAISE_ID: 
                parse_string = "'za' %i:" + PL_BIE;
                przyimek = "za"; przyp = PL_BIE;
                break;
            case LOG_IDEA_ID:
            case LOG_SYSIDEA_ID:   
                parse_string = "'do' %i:" + PL_DOP;
                przyimek = "do"; przyp = PL_DOP;
                break;
            default:
                parse_string = "'w' / 'we' %i:" + PL_MIE;
                przyimek = "w"; przyp = PL_MIE;
        }
        
		/* Find the target. */
		if (!parse_command(str, environment(this_player()), parse_string, oblist) ||
            (!sizeof(oblist = NORMAL_ACCESS(oblist, 0, 0))))
        {
			if(str!="w sobie")
	            return 0;
			else
			{	
				oblist=allocate(1);
				oblist[0]=this_player();
			}
        }

        /* One target at a time. */
        if (sizeof(oblist) > 1)
        {
            notify_fail("Sprecyzuj, o kt�ry konkretnie obiekt ci chodzi.\n");
            return 0;
        }

        this_player()->set_obiekty_zaimkow(oblist);

		if(str=="w sobie")
			target=this_player();
		else
			target = oblist[0];

        write("Zg�aszasz " + LOG_NICE_NAMES(type-1)
            + " " + przyimek + " " + 
            target->short(this_player(), przyp) + ".\n");
    }
    else
    {
        target = environment(this_player());
        write("Zg�aszasz " + LOG_NICE_NAMES(type-1) + " " + (type >= LOG_SYSBUG_ID && type <= LOG_SYSIDEA_ID ?
            "odnosz�c" + (type == LOG_SYSPRAISE_ID ? "�" : "y") + " si� do ca�ego �wiata" :
            "w pomieszczeniu, w kt�rym stoisz") + ".\n");
    }

    this_player()->add_prop(PLAYER_I_LOG_TYPE, type);
    this_player()->add_prop(PLAYER_O_LOG_OBJECT, target);

    setuid();
    seteuid(getuid());

    clone_object(EDITOR_OBJECT)->edit(&done_reporting(, type, target, tresc, przyimek, przyp));
    return 1;
}

/**
 * System przegl�dania przez graczy tablicy po�wi�conej og�lnym
 * wydarzeniom i zmianom na Vatt'ghernie.
 */
int
wiesci(string str)
{
    if (!Common_Board)
    {
        object *clones = object_clones(find_object(COMMON_BOARD));
        int index = sizeof(clones);

        while (index--)
            if (file_name(environment(clones[index])) == COMMON_BOARD_ROOM)
            {
                Common_Board = clones[index];
                break;
            }

        if (!Common_Board)
        {
            notify_fail("Z jakich� dziwnych powod�w System Wie�ci Vatt'gherna "
                      + "jest chwilowo niedost�pny.\n");
            return 0;
        }
    }

    return Common_Board->wiesci(str);
}
