/*
 * /cmd/live/items.c
 *
 * General commands for manipulating items.
 *
 * - drink
 * - eat
 * - extinguish
 * - hold
 * - light
 * - ociosaj
 * - (m)read
 * - release
 * - remove
 * - ulam
 * - unwield
 * - wear
 * - wield
 * - wyryj
 * - zetnij
 *
 * Except where noted, these commands can be used on any item in the
 * player's possession which correctly defines the "use function"
 * called by a particular command.  For instance, "read" can be used
 * on any item which correctly implements a command_read() function.
 * These "use functions" should return a string value (error message)
 * on failure and 1 on success (see the /std modules for which these
 * commands were implemented for examples.)
 *
 * Commands and "use functions":
 *
 * Command       Function                Remark
 * =========================================================================
 *   drink       command_drink()
 *   eat         command_eat()
 * + extinguish  command_extinguish()
 *   hold        command_hold()
 * + light       command_light()
 * + (m)read     command_read(int more)  If "more" is true, the item should be
 *                                       read using more().
 *   release     command_release()
 *   remove      command_remove()
 *   unwield     command_unwield()
 *   wear        command_wear()
 *   wield       command_wield()
 * =========================================================================
 * + These commands can be used on items in the living's environment as well.
 */

#pragma no_clone
#pragma no_inherit
#pragma save_binary
#pragma strict_types

inherit "/cmd/std/command_driver";

#include <cmdparse.h>
#include <files.h>
#include <composite.h>
#include <language.h>
#include <macros.h>


//Te dwa definy nizej potrzebne byly do chwyc/pusc. Lil.
/* Use an item in the actor's inventory */
#define USE_INV(str, func, silent, one, formy, przyp) \
    use_described_items(str, all_inventory(this_player()), func, silent, one, formy, przyp)

/* Use an item in the actor's inventory or environment */
#define USE_ENV(str, func, silent, one, formy, przyp) \
    use_described_items(str, all_inventory(this_player()) + \
    all_inventory(environment(this_player())), func, silent, one, formy, przyp)


/*
 * Use the #if 0 trick to fool the document maker. This way, we can here define
 * a description of all the functions this soul may call in items.
 */

#if 0
/*
 * Function name: command_drink
 * Description  : This is a pseudo function, used to fool the document maker.
 *                Define this in an object that is drinkable. It is by default
 *                built into /std/drink.c
 * Returns      : string - an error message upon failure.
 *                int 1  - when successfully imbided.
 */
public mixed
command_drink()
{
}

/*
 * Function name: command_eat
 * Description  : This is a pseudo function, used to fool the document maker.
 *                Define this in an object that is edible. It is by default
 *                built into /std/food.c
 * Returns      : string - an error message upon failure.
 *                int 1  - when successfully eaten.
 */
public mixed
command_eat()
{
}

/*
 * Function name: command_extinguish
 * Description  : This is a pseudo function, used to fool the document maker.
 *                Define this in an object that contains light and can be
 *                extinguished. It is by default built into /std/torch.c
 * Returns      : string - an error message upon failure.
 *                int 1  - when successfully extinguished.
 */
public mixed
command_extinguish()
{
}

/*
 * Function name: command_hold
 * Description  : This is a pseudo function, used to fool the document maker.
 *                Define this in an object that can be held. It is by default
 *                built into /lib/holdable_item.c
 * Returns      : string - an error message upon failure.
 *                int 1  - when successfully eaten.
 */
public mixed
command_hold()
{
}

/*
 * Function name: command_light
 * Description  : This is a pseudo function, used to fool the document maker.
 *                Define this in an object that can contain light and can be
 *                lit. It is by default built into /std/torch.c
 * Returns      : string - an error message upon failure.
 *                int 1  - when successfully lit.
 */
public mixed
command_light()
{
}

/*
 * Function name: command_read
 * Description  : This is a pseudo function, used to fool the document maker.
 *                Define this in an object that can be read.
 * Arguments    : int more - if true, the command should read using more.
 * Returns      : string - an error message upon failure.
 *                int 1  - when successfully read.
 */
public mixed
command_read(int more)
{
}

/*
 * Function name: command_release
 * Description  : This is a pseudo function, used to fool the document maker.
 *                Define this in an object that can be held and thus released.
 *                It is by default built into /lib/holdable_item.c
 * Returns      : string - an error message upon failure.
 *                int 1  - when successfully released.
 */
public mixed
command_release()
{
}

/*
 * Function name: command_remove
 * Description  : This is a pseudo function, used to fool the document maker.
 *                Define this in an object that can be worn and thus removed.
 *                It is by default built into /lib/wearble_item.c
 * Returns      : string - an error message upon failure.
 *                int 1  - when successfully removed.
 */
public mixed
command_remove()
{
}

/*
 * Function name: command_unwield
 * Description  : This is a pseudo function, used to fool the document maker.
 *                Define this in an object that can be wielded and thus
 *                unwielded. It is by default built into /std/weapon.c
 * Returns      : string - an error message upon failure.
 *                int 1  - when successfully unwield.
 */
public mixed
command_unwield()
{
}

/*
 * Function name: command_wear
 * Description  : This is a pseudo function, used to fool the document maker.
 *                Define this in an object that can be worn. It is by default
 *                built into /lib/wearble_item.c
 * Returns      : string - an error message upon failure.
 *                int 1  - when successfully worn.
 */
public mixed
command_wear()
{
}

/*
 * Function name: command_wield
 * Description  : This is a pseudo function, used to fool the document maker.
 *                Define this in an object that can be wielded. It is by
 *                default built into /std/weapon.c
 * Returns      : string - an error message upon failure.
 *                int 1  - when successfully wielded.
 */
public mixed
command_wield()
{
}
#endif /* 0 */


/* **************************************************************************
 * Return a proper name of the soul in order to get a nice printout.
 */
string
get_soul_id()
{
    return "items";
}

/* **************************************************************************
 * This is a command soul.
 */
int
query_cmd_soul()
{
    return 1;
}

/*
 * Function name: using_soul
 * Description:   Called once by the living object using this soul. Adds
 *                sublocations responsible for extra descriptions of the
 *                living object.
 */
public void
using_soul(object live)
{
}

/* **************************************************************************
 * The list of verbs and functions. Please add new in alfabetical order.
 */
mapping
query_cmdlist()
{
    return
        ([
         "wypij"       : "drink",
         "zjedz"       : "eat",
         "zga�"        : "extinguish",
         "chwy�"       : "hold",
         "zapal"       : "light",
         "przeczytaj"  : "read",
         "dob�d�"      : "wield",
         "opu��"       : "unwield",
         "za��"       : "wear",
         "zdejmij"     : "remove",
         "zapnij"      : "button",
         "odepnij"     : "unbutton",
	"zetnij"     : "zetnij",
	 "ociosaj"    : "ociosaj",
	 "u�am"       : "ulam",
	 "urwij"      : "ulam",
	 "wyryj"      : "wyryj",
         ]);
}

//A takze ta funkcja: use_items.  Lil.    

/*
 * Function name: use_items
 * Description:   Cause the actor to "use" the given items
 * Arguments:     object *items - the items to use
 *                function f - the function to call in the objects found to
 *                             use them.
 *                int silent - suppress the default message given when items
 *                             are successfully used.
 * Returns:       object * - an array consisting of the objects that were
 *                           successfully used
 */
varargs public object *
use_items(object *items, function f, int silent,
    string *formy, int przyp = PL_BIE)
{
    mixed res;
    object *used = ({ });
    string fail_msg = "";
    int fail, i;

    foreach (object ob : items)
    {
        /* Call the function to "use" the item */
        res = f(ob);

        if (!res)
        {
            /* The item cannot be used in this way */
            continue;
        }

        if (stringp(res))
        {
            /* The attempt to use the item failed */
            fail = 1;
            fail_msg += res;
        }
        else
        {
            /* The item was successfully used */
            used += ({ ob });
        }
    }

    if (!sizeof(used))
    {
        /* Nothing could be used.  Say why. */

        if (!fail)
        {
            notify_fail("Nie mo�esz " +
                    ((sizeof(items) > 1) ? "ich" : "tego") + " " + formy[0] + ".\n");
            return 0;
        }

        write(fail_msg);
        return ({});
    }

    if (!silent)
    {
        write(capitalize(formy[1]) + " " + COMPOSITE_DEAD(used, przyp) +
                ".\n");
        say(QCTNAME(this_player()) + " " + formy[2] + " " +
                COMPOSITE_DEAD(used, przyp) + ".\n");
    }

    this_player()->set_obiekty_zaimkow(used);

    return used;
}

//No i jeszcze to. use_described_items. Takze potrzebne do
// chwyc/pusc.    Lil.

/*
 * Function name: use_described_items
 * Description:   Given a string, cause the actor to "use" the items described
 *                by the string.
 * Arguments:     string str  - the string describing what to use
 *                object *obs - the items to be matched with the string
 *                function f  - the function to call in the objects found to
 *                              use them.
 *                int silent  - suppress the default message given when items are
 *                              successfully used.
 *                int use_one - Allow only one item to be used
 * Returns:       0 - No items found that matched the string describer
 *                object * - an array consisting of the objects that were
 *                           successfully used
 */
varargs public object *
use_described_items(string str, object *obs, function f, int silent,
    int use_one, string *formy, int przyp = PL_BIE, string acsfunc = 0, object acsobj = 0)
{
    mixed items;

    if (!strlen(str) ||
        !parse_command(str, obs, "%i:" + przyp, items) ||
        !sizeof(items = NORMAL_ACCESS(items, acsfunc, acsobj)))
    {
        notify_fail(capitalize(query_verb()) + " " + (przyp == PL_DOP ? "czego" : "co")
        + "?\n", 0);
        return 0;
    }

    if (use_one && (sizeof(items) > 1))
    {
        notify_fail("Mo�esz " + formy[0] + " naraz tylko jedn� rzecz.\n");
        return 0;
    }

    return use_items(items, f, silent, formy, przyp);
}


public int
drink(string str)
{
    object *drinkable;

    if (!(drinkable = USE_INV(str, &->command_drink(), 0, 0, ({"wypi�", "pijesz", "pije"}), PL_BIE)))
    {
        return 0;
    }

    /* Remove the drinks after we are through */
    drinkable->remove_drink();

    return 1;
}

public int
eat(string str)
{
    object *eatable;

    if (!(eatable = USE_INV(str, &->command_eat(), 0, 0, ({"zje��", "zjadasz", "zjada"}), PL_BIE)))
    {
        return 0;
    }

    /* Remove the food after we are through */
    eatable->remove_food();

    return 1;
}

public int
extinguish(string str)
{
    return !!USE_ENV(str, &->extinguish_me(), 1, 0, ({"zgasi�", "gasisz", "gasi"}), PL_BIE);
}

public int
light(string str)
{
    return !!USE_ENV(str, &->light_me(), 1, 0, ({"zapali�", "zapalasz", "zapala"}), PL_BIE);
}

public int
read(string str)
{
    return !!USE_ENV(str, &->command_read(), 1, 1, ({"przeczyta�", "czytasz", "czyta"}), PL_BIE);
}


public int
wear(string str)
{
    return !!USE_INV(str, &->wear_me(), 1, 0, ({"za�o�y�", "zak�adasz", "zak�ada"}), PL_BIE);
}

nomask int
is_pochwa_in_inv(object ob)
{
    if (!objectp(ob))
        return 0;
    if (environment(ob) == this_player())
        return 1;
    if (environment(ob) == environment(this_player()))
        return 1;
    if (environment(ob)->query_is_pochwa() && (environment(environment(ob)) == this_player()))
        return 1;
    return 0;
}

public int
wield(string str)
{
    return !!use_described_items(str, deep_inventory(this_player()), &->wield_me(), 1, 0,
            ({"doby�", "dobywasz", "dobywa"}), PL_DOP, "is_pochwa_in_inv", this_object());
}

public int
remove(string str)
{
    return !!USE_INV(str, &->remove_me(), 1, 0, ({"zdj��", "zdejmujesz", "zdejmuje"}), PL_BIE);
}

public int
unwield(string str)
{
    mixed items;
    object *inv;
    int i, size;

    notify_fail("Opu�� co?\n");

    if (!str)
        return 0;

    items = this_player()->query_weapon(-1);

    if (!pointerp(items))
        inv = ({ items });
    else
        inv = items;

    inv -= ({ 0 });


    if (!sizeof(inv))
    {
        return 0;
    }

    if (!parse_command(str, inv, "%i:"+PL_BIE, items))
        return 0;

    items = NORMAL_ACCESS(items, 0, 0);
    items = filter(items, &->check_weapon());
    size = sizeof(items);

    if (!size)
        return 0;

    return !!use_items(items, &->unwield_me(), 1, ({"opu�ci�", "opuszczasz", "opuszcza"}), PL_BIE);
}

public int
zetnij(string str)
{  
    return !!USE_ENV(str, &->try_zetnij(), 1, 1, ({"�ci��", "�cinasz", "�cina"}), PL_BIE);
}

public int
ociosaj(string str)
{
    return !!USE_ENV(str, &->try_ociosaj(), 1, 1, ({"ociosa�", "ociosujesz", "ociosuje"}), PL_BIE);
}

public int 
ulam(string str)
{
    notify_fail("Co i sk�d chcesz u�ama�?\n");
    if(!str)
	return 0;

    object *objs;
    if(!parse_command(lower_case(str), environment(TP), " 'ga���' 'z'  %i:"+PL_DOP , objs))
	return 0;

    objs = NORMAL_ACCESS(objs, 0, 0);
	
    if(!objs || !sizeof(objs))
	return 0;

    if(sizeof(objs) > 1)
	return 0;
    
    if(function_exists("create_object", objs[0]) != "/std/drzewo")
	return 0;
     
    mixed ret = objs[0]->try_ulam();
    
    if(stringp(ret))
	write(ret);
    
    return 1;
}

public int
wyryj(string str)
{
    notify_fail("Chyba wyryj <na czym> napis <tekst>?\n");
    
    if(!str)
	return 0;

    object *objs;
    string tekst="";
    if(!parse_command(str, environment(TP), " 'na' %i:"+PL_MIE+" 'napis' %s ", objs, tekst))
	return 0;

    objs = NORMAL_ACCESS(objs, 0, 0);
	
    if(!objs)
	return 0;
	
    if(sizeof(objs) > 1)
	return 0;
 
    if(strlen(tekst) < 1)
	return 0;

    mixed ret = objs[0]->try_wyryj(tekst);
    
    if(stringp(ret))
	write(ret);
    
    return 1;
}


//Nowosc :P Nie wiem czy sie sprawdzi, mam nadzieje, ze tak.
//Wiecej znajduje sie w /lib/holdable_item.c
// Lil, psuja ;)


public int
hold(string str)
{
    return !!USE_INV(str, &->hold_me(), 1, 0, ({"chwyci�", "chwytasz", "chwyta"}), PL_BIE);
}

public int
button(string str)
{
    return !!USE_INV(str, &->button_me(), 1, 0, ({"zapi��", "zapinasz", "zapina"}), PL_BIE);
}

public int
unbutton(string str)
{
    return !!USE_INV(str, &->unbutton_me(), 1, 0, ({"odpi��", "odpinasz", "odpina"}), PL_BIE);
}
