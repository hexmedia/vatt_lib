/*
 * /secure/pogoda/ksiezyc.c
 *
 * Plik odpowiedzialny za funkcje zwi�zane z ksi�ycem.
 *
 */

/*
 * Nazwa funkcji: opis_ksiezyca
 * Opis         : Funkcja zwraca opis ksi�yca w zale�no�ci od czasu i czynnik�w pogodowych.
 * Argumenty    : string roomstr - file_name pomieszczenia, nad kt�rym ksi�yc opisujemy
 * Zwraca       : string - opis ksi�yca
 */

string
opis_ksiezyca(string roomstr)
{
	int x, y;
	string toReturn = "";
	object room = find_object(roomstr);
	object plan_pogody;

	if (!objectp(room))
	{
		return "Nie zauwa�asz niczego takiego.";
	}

  if (ustaw_ksiezyc(room))
	{
		return "Nie zauwa�asz niczego takiego.";
	}
	
	x = room->query_prop(ROOM_I_WSP_X);
	y = room->query_prop(ROOM_I_WSP_Y);
	
	check_pogoda(x, y);

	plan_pogody = get_plan(x, y);

	switch (PORA_MUDOWA)
	{
		case MT_WIECZOR:
			toReturn += "Dopiero co wschodz�cy ksi�yc";
			break;
		case MT_POZNY_WIECZOR:
			toReturn += "Wisz�cy nisko nad horyzontem ksi�yc";
			break;
		case MT_NOC:
			toReturn += "Sun�cy wysoko po niebie ksi�yc";
			break;
	}
	
	switch (plan_pogody->get_zachmurzenie())
	{
		case POG_ZACH_ZEROWE:
			toReturn += " jest doskonale widoczny podczas bezchmurnej pogody.";
			break;
		case POG_ZACH_LEKKIE:
			toReturn += " jest doskonale widoczny mimo nielicznych chmurek.";
			break;
		case POG_ZACH_SREDNIE:
			toReturn += " jest cz�ciowo zakryty chmurami.";
			break;
		case POG_ZACH_DUZE:
			toReturn += " jest niemal zupe�nie zakryty grubymi chmurami.";
			break;
	}
	
	if ((plan_pogody->get_zachmurzenie() == POG_ZACH_CALKOWITE) || (PORA_MUDOWA < MT_WIECZOR))
	{
		room->remove_item("ksi�yc");
		return "Nie zauwa�asz niczego takiego.";
	}
	
	return toReturn;
}

/*
 * Nazwa funkcji: ustaw_ksiezyc
 * Opis         : Funkcja sprawdza, czy w danym pomieszczeniu ksi�yc ma by� widoczne
 *                czy nie.
 * Argumenty    : object room - lokacja, w kt�rej obecno�� ksi�yca jest sprawdzana
 */

int
ustaw_ksiezyc(object room)
{
	object plan_pogody;
	if (!objectp(room))
	{
		return 0;
	}
	room->remove_item("ksi�yc");
	plan_pogody = get_plan(room->query_prop(ROOM_I_WSP_X), room->query_prop(ROOM_I_WSP_Y));
	if ((room->query_prop(ROOM_I_INSIDE) && !room->widac_ksiezyc()) ||
			((plan_pogody->get_zachmurzenie() == POG_ZACH_CALKOWITE) || (PORA_MUDOWA < MT_WIECZOR)))
	{
		return 1;
	}
	room->add_item(({"ksi�yc"}), "@@opis_ksiezyca:"+POGODA_OBJECT+"|"+
			file_name(room)+"@@\n");
	return 0;
}
