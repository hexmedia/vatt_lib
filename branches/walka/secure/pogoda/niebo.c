/*
 * /secure/pogoda/niebo.c
 *
 * Plik odpowiedzialny za funkcje zwi�zane z niebem.
 *
 */

/*
 * Nazwa funkcji: opis_nieba
 * Opis         : Funkcja zwraca opis nieba w zale�no�ci od czasu i czynnik�w pogodowych.
 * Argumenty    : string roomstr - file_name pomieszczenia, nad kt�rym niebo opisujemy
 * Zwraca       : string - opis nieba
 */

string
opis_nieba(string roomstr)
{
	int x, y;
	string toReturn = "";
	string op1 = "";
	string op2 = "";
	object room = find_object(roomstr);
	object plan_pogody;

	if (!objectp(room))
	{
		return "Nie zauwa�asz niczego takiego.";
	}

	if (ustaw_niebo(room))
	{
		return "Nie zauwa�asz niczego takiego.";
	}
	
	x = room->query_prop(ROOM_I_WSP_X);
	y = room->query_prop(ROOM_I_WSP_Y);
	
	check_pogoda(x, y);

	plan_pogody = get_plan(x, y);
	
	switch (PORA_MUDOWA)
	{
		case MT_SWIT:
			toReturn += "Ja�niej�ce lekko na wschodzie";
			op1 += " bledn�ce gwiazdy i ksi�yc.";
			break;
		case MT_WCZESNY_RANEK:
			toReturn += "Wczesnoranne";
			op1 += " dopiero co wzesz�e s�o�ce.";
			break;
		case MT_RANEK:
			toReturn += "Ranne";
			op1 += " poranne s�o�ce.";
			break;
		case MT_POLUDNIE:
			toReturn += "Jasne";
			op1 += " s�o�ce w zenicie.";
			break;
		case MT_POPOLUDNIE:
			toReturn += "Popo�udniowe";
			op1 += " popo�udniowe s�o�ce.";
			break;
		case MT_WIECZOR:
			toReturn += "Lekko szarzej�ce";
			op1 += " s�o�ce chyl�ce si� ku zachodowi.";
			break;
		case MT_POZNY_WIECZOR:
			toReturn += "Ciemne";
			op1 += " pierwsze ja�niej�ce gwiazdy.";
			break;
		case MT_NOC:
			toReturn += "Ciemne, nocne";
			op1 += " mrowie �wiec�cych gwiazd i ksi�yc.";
			break;
	}
	
	switch (plan_pogody->get_zachmurzenie())
	{
		case POG_ZACH_ZEROWE:
			toReturn += " niebo nie jest zakryte nawet najmniejsz� chmurk�.";
			op2 = "Dok�adnie wida�";
			break;
		case POG_ZACH_LEKKIE:
			toReturn += " niebo jest gdzieniegdzie pokryte ma�ymi chmurkami.";
			op2 = "Lekko zas�oni�te ma�ymi chmurkami wida�";
			break;
		case POG_ZACH_SREDNIE:
			toReturn += " niebo jest �rednio zachmurzone.";
			op2 = "Pomi�dzy chmurami wida�";
			break;
		case POG_ZACH_DUZE:
			toReturn += " niebo jest pokryte pokryte niemal zupe�nie grubymi chmurami.";
			op2 = "Gdzieniegdzie spod grubych chmur przebija si�";
			break;
		case POG_ZACH_CALKOWITE:
			toReturn += " niebo jest ca�kowicie zachmurzone.";
			break;
	}
	
	if (plan_pogody->get_zachmurzenie() < POG_ZACH_CALKOWITE)
	{
		toReturn += " " + op2 + op1;
	}
	
	return toReturn;
}

/*
 * Nazwa funkcji: ustaw_niebo
 * Opis         : Funkcja sprawdza, czy w danym pomieszczeniu niebo ma by� widoczne
 *                czy nie.
 * Argumenty    : object room - lokacja, w kt�rej obecno�� nieba jest sprawdzana 
 */

int
ustaw_niebo(object room)
{       
    if (!objectp(room))
    {
        return 0;
    }
    room->remove_item("niebo");
    if (room->query_prop(ROOM_I_INSIDE) && !room->widac_niebo())
    {
        return 1;
    }
    room->add_item(({"niebo", "niebosk�on"}), "@@opis_nieba:"+POGODA_OBJECT+"|"+
            file_name(room)+"@@\n");
    return 0;
}
