/*
 * /secure/pogoda/slonce.c
 *
 * Plik odpowiedzialny za funkcje zwi�zane ze s�o�cem.
 *
 */

/*
 * Nazwa funkcji: opis_slonca
 * Opis         : Funkcja zwraca opis slo�ca w zale�no�ci od czasu i czynnik�w pogodowych.
 * Argumenty    : string roomstr - file_name pomieszczenia, nad kt�rym s�o�ce opisujemy
 * Zwraca       : string - opis s�o�ca
 */

string
opis_slonca(string roomstr)
{
	int x, y;
	string toReturn = "";
	object room = find_object(roomstr);
	object plan_pogody;

	if (!objectp(room))
	{
		return "Nie zauwa�asz niczego takiego.";
	}

	if (ustaw_slonce(room))
	{
		return "Nie zauwa�asz niczego takiego.";
	}
	
	x = room->query_prop(ROOM_I_WSP_X);
	y = room->query_prop(ROOM_I_WSP_Y);
	
	check_pogoda(x, y);

	plan_pogody = get_plan(x, y);
	
	switch (PORA_MUDOWA)
	{
		case MT_SWIT:
			toReturn += "Ja�niej�ce lekko niebo na wschodzie";
			break;
		case MT_WCZESNY_RANEK:
			toReturn += "Jasne, wczesnoranne s�o�ce";
			break;
		case MT_RANEK:
			toReturn += "Ranne, mocne s�o�ce";
			break;
		case MT_POLUDNIE:
			toReturn += "Mocno �wiec�ce s�o�ce w zenicie";
			break;
		case MT_POPOLUDNIE:
			toReturn += "Sun�ce powoli na niebosk�onie s�o�ce";
			break;
		case MT_WIECZOR:
			toReturn += "Chowaj�ce si� na zachodzie s�o�ce";
			break;
	}
	
	switch (plan_pogody->get_zachmurzenie())
	{
		case POG_ZACH_ZEROWE:
			toReturn += " jest doskonale widoczne podczas bezchmurnej pogody.";
			break;
		case POG_ZACH_LEKKIE:
			toReturn += " jest doskonale widoczne mimo nielicznych chmurek.";
			break;
		case POG_ZACH_SREDNIE:
			toReturn += " jest cz�ciowo zakryte chmurami.";
			break;
		case POG_ZACH_DUZE:
			toReturn += " jest niemal zupe�nie zakryte grubymi chmurami.";
			break;
	}
	
	if ((plan_pogody->get_zachmurzenie() == POG_ZACH_CALKOWITE) || (PORA_MUDOWA > MT_WIECZOR))
	{
		room->remove_item("s�o�ce");
		return "Nie zauwa�asz niczego takiego.";
	}
	
	return toReturn;
}

/*
 * Nazwa funkcji: ustaw_slonce
 * Opis         : Funkcja sprawdza, czy w danym pomieszczeniu s�o�ce ma by� widoczne
 *                czy nie.
 * Argumenty    : object room - lokacja, w kt�rej obecno�� s�o�ca jest sprawdzana
 */

int
ustaw_slonce(object room)
{
	object plan_pogody;
	if (!objectp(room))
	{
		return 0;
	}
	room->remove_item("s�o�ce");
	plan_pogody = get_plan(room->query_prop(ROOM_I_WSP_X), room->query_prop(ROOM_I_WSP_Y));
	if ((room->query_prop(ROOM_I_INSIDE) && !room->widac_slonce()) ||
			((plan_pogody->get_zachmurzenie() == POG_ZACH_CALKOWITE) || (PORA_MUDOWA > MT_WIECZOR)))
	{
		return 1;
	}
	room->add_item(({"s�o�ce"}), "@@opis_slonca:"+POGODA_OBJECT+"|"+
			file_name(room)+"@@\n");
	return 0;
}
