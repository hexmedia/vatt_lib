/*
 * /secure/pogoda/gwiazdy.c
 *
 * Plik odpowiedzialny za funkcje zwi�zane z gwiazdami.
 *
 */

/*
 * Nazwa funkcji: opis_gwiazd
 * Opis         : Funkcja zwraca opis gwiazd w zale�no�ci od czasu i czynnik�w pogodowych.
 * Argumenty    : string roomstr - file_name pomieszczenia, nad kt�rym gwiazdy opisujemy
 * Zwraca       : string - opis gwiazd
 */

string
opis_gwiazd(string roomstr)
{
	int x, y;
	string toReturn = "";
	object room = find_object(roomstr);
	object plan_pogody;

	if (!objectp(room))
	{
		return "Nie zauwa�asz niczego takiego.";
	}

	if (ustaw_gwiazdy(room))
	{
		return "Nie zauwa�asz niczego takiego.";
	}
	
	x = room->query_prop(ROOM_I_WSP_X);
	y = room->query_prop(ROOM_I_WSP_Y);
	
	check_pogoda(x, y);

	plan_pogody = get_plan(x, y);
	
	switch (PORA_MUDOWA)
	{
		case MT_SWIT:
			toReturn += "Ostatnie bledn�ce gwiazdy";
			break;
		case MT_WIECZOR:
			toReturn += "Pierwsze, pojedy�cze gwiazdy";
			break;
		case MT_POZNY_WIECZOR:
			toReturn += "Liczne skupiska gwiazd";
			break;
		case MT_NOC:
			toReturn += "Liczne, uk�adaj�ce si� w rozbudowane konstelacje, gwiazdy";
			break;
	}
	
	switch (plan_pogody->get_zachmurzenie())
	{
		case POG_ZACH_ZEROWE:
			toReturn += " s� doskonale widoczne podczas bezchmurnej pogody.";
			break;
		case POG_ZACH_LEKKIE:
			toReturn += " s� doskonale widoczne mimo nielicznych chmurek.";
			break;
		case POG_ZACH_SREDNIE:
			toReturn += " s� cz�ciowo zakryte chmurami.";
			break;
		case POG_ZACH_DUZE:
			toReturn += " s� niemal zupe�nie zakryte grubymi chmurami.";
			break;
	}
	
	if ((plan_pogody->get_zachmurzenie() == POG_ZACH_CALKOWITE) ||
			((PORA_MUDOWA < MT_WIECZOR) && (PORA_MUDOWA > MT_SWIT)))
	{
		room->remove_item("gwiazdy");
		return "Nie zauwa�asz niczego takiego.";
	}
	
	return toReturn;
}

/*
 * Nazwa funkcji: ustaw_gwiazdy
 * Opis         : Funkcja sprawdza, czy w danym pomieszczeniu gwiazdy maj� by� widoczne
 *                czy nie.
 * Argumenty    : object room - lokacja, w kt�rej obecno�� gwiazd jest sprawdzana
 */

int
ustaw_gwiazdy(object room)
{
	object plan_pogody;
	if (!objectp(room))
	{
		return 0;
	}
	room->remove_item("gwiazdy");
	plan_pogody = get_plan(room->query_prop(ROOM_I_WSP_X), room->query_prop(ROOM_I_WSP_Y));
	if ((room->query_prop(ROOM_I_INSIDE) && !room->widac_gwiazdy()) ||
			((plan_pogody->get_zachmurzenie() == POG_ZACH_CALKOWITE) ||
			 ((PORA_MUDOWA < MT_WIECZOR) && (PORA_MUDOWA > MT_SWIT))))
	{
		return 1;
	}
	room->add_item(({"gwiazdy"}), "@@opis_gwiazd:"+POGODA_OBJECT+"|"+
			file_name(room)+"@@\n");
	return 0;
}
