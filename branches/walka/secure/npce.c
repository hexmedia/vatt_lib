/*
 * /secure/npce.c
 *
 * Plik odpowiedzialny za sterowanie niebem i zjawiskami atmosferycznymi dla
 * poszczeg�lnych room�w.
 *
 */

inherit "/std/room.c";

#pragma no_clone
#pragma no_inherit
#pragma save_binary
#pragma strict_types

#include <mudtime.h>
#include <files.h>
#include <stdproperties.h>
#include <macros.h>

static mapping npce = ([]);

/*
 * Nazwa funkcji: short
 * Opis         : Funkcja zwraca opis obiektu.
 * Zwraca       : string - opis obiektu
 */

string
short()
{
	return "g��wny zarz�dca npcami";
}

/*
 * Nazwa funkcji: short_npce
 * Opis         : Funkcja wypisuje informacje o zarejestrowanych npcach
 */

void
short_npce()
{
    int i = 0;
	write("*** " + capitalize(short()) + " ***\n");
    foreach (string name : m_indices(npce)) {
        i++;
        write(i + ") " + name + ": " + npce[name][0] + " (" + npce[name][1] + ")\n");
    }
	write("* * *\n");
}

void
info_npc(int num)
{
    object ob;
    object* clones;
    int i = 0;
    int j = 0;
    foreach (string name : m_indices(npce)) {
        i++;
        if (i == num) {
            write("*** " + i + ") " + name + " ***\n");
            ob = find_object(name);
            if (ob) {
                clones = object_clones(ob);
                foreach (object cur : clones) {
                    j++;
                    write("\n" + j + ") " + file_name(cur) + "\n  " + (objectp(ENV(cur)) ? file_name(ENV(cur)) : "0") + "\n");
                }
            }
            return;
        }
    }
    write("Z�y numer npca!\n");
}

public void
register_npc(object ob)
{
    string nazwa = MASTER_OB(ob);
    if (pointerp(npce[nazwa])) {
        npce[nazwa][0] += 1;
    }
    else {
        npce[nazwa] = ({ 1, 0 });
    }
}

public void
remove_npc(object ob)
{
    string nazwa = MASTER_OB(ob);
    if (pointerp(npce[nazwa])) {
        npce[nazwa][0] -= 1;
        if (npce[nazwa][0] < 0) {
            npce[nazwa][0] = 0;
        }
    }
}

public void
new_npc(string nazwa)
{
    if (pointerp(npce[nazwa])) {
        npce[nazwa][1] += 1;
    }
    else {
        npce[nazwa] = ({ 0, 1 });
    }
}

public void
delete_npc(string nazwa)
{
    if (pointerp(npce[nazwa])) {
        npce[nazwa][1] -= 1;
        if (npce[nazwa][1] < 0) {
            npce[nazwa][1] = 0;
        }
    }
}

public int
get_free_slots(string nazwa)
{
    if (pointerp(npce[nazwa])) {
        int res = npce[nazwa][1] - npce[nazwa][0];
        if (res < 0) {
            res = 0;
        }
        return res;
    }
    return 0;
}
