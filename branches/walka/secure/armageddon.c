/* Lekko przerobila Lil, a opisy napisal Rantaur,
 *         Wednesday 10 of May 2006
 *
 *
 * /secure/armageddon.c
 *
 * This object takes the mud down in a gracefull manner.
 *
 * This is supposed to be inherited by the actual Armageddon object
 * for mud-specific actions to be taken when the game closes down.
 * SECURITY is the only object that is allowed to shut down the game.
 * All requests have to go through the master.
 */

#pragma no_clone
#pragma no_shadow
#pragma save_binary
#pragma strict_types

inherit "/std/creature";

#include <macros.h>
#include <std.h>
#include <stdproperties.h>
#include <time.h>

#define SCENE_INTRO    0
#define SCENE_INTRO12  1
#define SCENE_INTRO2   2
#define SCENE_ROOT     3
#define SCENE_ROOT12   4
#define SCENE_DELAY    5
#define SCENE_THEEND   6
#define SCENE_CANCEL  -1
#define SCENE_WEIRD   -2

#define INSIDE_SEQUENCES ({1, 2})
#define OUTSIDE_SEQUENCES ({0, 1, 2})

/*
 * Global variables.
 */
static private string shutdown_shutter = 0;
static private string shutdown_reason = 0;
static private int    shutdown_delay = 0;
static private int    shutdown_alarm = 0;

/*
 * Function name: query_init_master
 * Description:   Makes sure that the master object is initialized properly.
 */
public nomask int
query_init_master()
{
    return 1;
}

/**
 * Ustawia numer sekwencji dla gracza.
 *
 * Sekwencja losowana jest z puli dost�pnych sekwencji (w zale�no�ci od tego,
 * czy gracz jest w �rodku czy na zewn�trz).
 *
 * @param player obiekt gracza
 */

void
set_sequence(object player)
{
  if(environment(player)->query_prop(ROOM_I_INSIDE)) {
    player->add_prop(PLAYER_FINAL_SEQUENCE, INSIDE_SEQUENCES[random(sizeof(INSIDE_SEQUENCES))]);
  }
  else {
    player->add_prop(PLAYER_FINAL_SEQUENCE, OUTSIDE_SEQUENCES[random(sizeof(OUTSIDE_SEQUENCES))]);
  }
}

/**
 * Pobiera tekst okre�lonej sceny dla gracza.
 *
 * Tekst tworzony jest na podstawie ustawionej sekwencji oraz numeru sceny.
 *
 * @param player obiekt gracza
 * @param scene numer sceny
 */

string
get_scene(object player, int scene)
{
  string toReturn;
  if (!player->is_prop_set(PLAYER_FINAL_SEQUENCE)) {
    /* fina�owa sekwencja nie jest ustalona? ustalamy j�!!! */
    set_sequence(player);
  }
  
  if (scene == SCENE_WEIRD) {
    return "Dziwne... M�j Pan nie pozwoli� mi wyzwoli� mej mocy.";
  }

  if (scene == SCENE_ROOT) {
    return "Nadchodzi Czas Apokalipsy: " + shutdown_reason;
  }

  if (scene == SCENE_ROOT12) {
    toReturn = "Nadchodzi Czas Apokalipsy: " + shutdown_reason;
    scene = SCENE_INTRO2;
  }
  
  /* Defaultowa scena powinna by� zawsze mo�liwa, gdy� jest ona fallbackiem */
  switch (player->query_prop(PLAYER_FINAL_SEQUENCE)) {
    case 0:
      switch (scene) {
        case SCENE_INTRO:
        case SCENE_INTRO12:
          toReturn = "Niespodziewanie zrywa si� pot�na �nie�yca! " +
                     "Rozp�dzone p�atki �niegu bezlito^snie smagaj� " +
                     "tw� twarz, zmuszaj�c do zamkni�cia oczu. Z " +
                     "pozornie bez�adnego zawodzenia wiatru z trudem " +
                     "zaczynasz wy�awia� �wiszcz�ce s�owa: " +
                     "Z woli " + capitalize(shutdown_shutter) +
                     " niebawem nadejdzie Czas Ko�ca" + (strlen(shutdown_reason)?
                             ": " + shutdown_reason : "!");
          if (scene == SCENE_INTRO) {
            return toReturn;
          }
        case SCENE_INTRO2:
          toReturn += " Wiatr jednak po chwili uspokaja si� i " +
                      "cichnie, ustepuj�c miejsca ch�odnemu, " +
                      "kobiecemu g�osowi, kt�rego ka�de kolejne s�owo " +
                      "brzmi coraz bardziej wiarygodnie, przez co wzbudza " +
                      "w tobie narastaj�cy, wibruj�cy l�k:\n\n" +
                      "\t\t\"Kto jest daleko, umrze od zarazy\n" +
                      "\t\t kto jest blisko, padnie od miecza\n" +
                      "\t\t Kto uchowa si�, umrze z g�odu\n" +
                      "\t\t Kto przetrwa, tego zgubi mr�z... \n" +
                      "\t\t Albowiem nadejdzie Tedd Deireadh,\n" +
                      "\t\t Czas Ko�ca, Czas Miecza i Topora,\n" +
                      "\t\t Czas Pogardy, Czas Bia�ego Zimna i\n\t\t " +
                      "Wilczej Zamieci...\"\n\n" +
                      "Pami�taj �miertelniku, pozosta�" +
                      ilosc(atoi(CONVTIME(shutdown_delay * 60)), "a", "y", "o") +
                      " ci ju� tylko " + CONVTIME(shutdown_delay * 60) + ".";
          return toReturn;
        case SCENE_DELAY:
          return "Dochodzi twych uszu zimny, kobiecy g�os: " +
                 "Przeznaczenie nieustannie wype�nia si�. " +
                 "Pami�taj o tym, bowiem nie zosta�o ci " +
                 "wi�cej ni�li " + CONVTIME(shutdown_delay * 60) + ".";
        case SCENE_THEEND:
          return "Czas ju� nadszed�, niechaj �wiat pogr��y " +
                 "si� w zapomnieniu.";
        case SCENE_CANCEL:
          return "S�yszysz cichy, tajemniczy g�os: "+
                 capitalize(shutdown_shutter) + " postanowi�" +
                 (find_player(shutdown_shutter)->query_gender() == G_FEMALE ?
                 "a" : "") + " na razie oszcz�dzi� ten �wiat. " +
                 "Pami�taj jednak - koniec jest nieuchronny.";
      }
      break;
    default:
    case 1:
      switch (scene) {
        case SCENE_INTRO:
        case SCENE_INTRO12:
          toReturn = "Niespodziewanie powietrze zaczyna dr�e� " +
                     "przynosz�c ze soba g�uche, rytmiczne dudnienie, " +
                     "kt�remu wt�ruj^a przera�liwe wrzaski, szybkie, " +
                     "niespokojne oddechy, agoniczne westchnienia - " +
                     "straszliwa symfonia echem wije si� po twej g�owie, " +
                     "zasnuwaj�c oczy mglist� wizj�...\n\n" +
                     "Upiorni, odziani w p�owe, wystrzepione szaty " +
                     "je�d�cy dosiadaj�cy ko�skich szkielet�w... Jeden " +
                     "z nich wyci�ga ku tobie skostnia�� d�on, a zza " +
                     "trupiej maski wydobywa si� sykliwy g�os: " +
                     "\""+capitalize(shutdown_shutter)+ " przes�dzi�" +
                     (find_player(shutdown_shutter)->query_gender() == G_FEMALE?"a":"") +
                     " ju� o losach tego �wiata" + (strlen(shutdown_reason)?
                             ": " + shutdown_reason : "!");
          if (scene == SCENE_INTRO) {
            return toReturn + "\"";
          }
        case SCENE_INTRO2:
          toReturn += " Gotuj si�, " +
                      "bowiem koniec jest bliski\". Istoty wydaj� z " +
                      "siebie przeszywaj�cy, " +
                      "dziki krzyk, by za chwil� pogalopowa� ponad " +
                      "niebosk�onem szerz�c " +
                      "Ostatnie S�owa.\n\nPami�taj �miertelniku, " +
                      "pozosta�"+
                      ilosc(atoi(CONVTIME(shutdown_delay * 60)), "a", "y", "o") +
                      " ci ju� tylko " + CONVTIME(shutdown_delay * 60) + ".";
          return toReturn;
        case SCENE_DELAY:
          return "Nagle twym oczom ukazuje si� ogromna " +
                 "klepsydra, lecz miast ziaren piasku " +
                 "przesypuje si� w niej niezliczona ilo�� ludzkich g��w! " +
                 "Ka�da z nich zdaje si� szepta�: "+
                 "Za " + CONVTIME_PL(shutdown_delay * 60, PL_BIE) +
                 " nadejdzie Czas Ko�ca, a ty, �miertelniku, "+
                 "do��czysz do nas.";
        case SCENE_THEEND:
          return "Przez okolic� przetacza si� pot�ny " +
                 "grzmot zmieszany z wrzaskami setek " +
                 "cierpi�cych istot. Chwil� potem �wiat niknie w " +
                 "ciemno�ciach...";
        case SCENE_CANCEL:
          return "Tw�j umys� nawiedza przeszywaj�cy g�os: " +
                 capitalize(shutdown_shutter) + " postanowi�" +
                 (find_player(shutdown_shutter)->query_gender() == G_FEMALE ?
                 "a" : "") + " oddali� zag�ad�. Kiedy� jednak koniec " +
                 "nadejdzie, a �mier� zbierze swe �niwo.";
      }
      break;
    case 2:
      switch (scene) {
        case SCENE_INTRO:
        case SCENE_INTRO12:
          toReturn = "Czujesz jak kto� k�adzie d�o� na twym ramieniu... " +
                     "Odwr�ciwszy si� spostrzegasz smuk�� kobiet� " +
                     "�widruj�c� ci� spojrzeniem swych niezwyk�ych oczu - " +
                     "budz� one w tobie g��boki niepok�j, bezustannie " +
                     "emanuj�c jak�� dzik�, ob��dn� fascynacj�... " +
                     "Wojowniczka chrz�szcz�c zbroj� " +
                     "przybli�a si� do ciebie i cichym g�osem szepcze: " +
                     "\"Powstan� dzieci pode Czarnym S�o�cem pocz�te, " +
                     "by rzeki krwi� przepe�ni�... " +
                     "A " + capitalize(shutdown_shutter) +
                     " ca�� sw� wol� wspiera� je b�dzie" + (strlen(shutdown_reason)?
                             ": " + shutdown_reason : "!") + "\"";
          if (scene == SCENE_INTRO) {
            return toReturn;
          }
        case SCENE_INTRO2:
          toReturn += " Dotyk zimnych warg na twej szyi wywo�uje " +
                      "lekki zawr�t g�owy, gdy wszystko " +
                      "wraca do normy, zauwa�asz, i� posta� znikne�a " +
                      "r�wnie szybko jak si� pojawi�a.\n\n" +
                      "Pami�taj �miertelniku, pozosta�" +
                      ilosc(atoi(CONVTIME(shutdown_delay * 60)), "a", "y", "o") +
                      " ci ju� tylko " + CONVTIME(shutdown_delay * 60) + ".";
          return toReturn;
        case SCENE_DELAY:
          return "Pe�en zimna g�os szepcze ci do ucha: " +
                 "Pozosta�" +
                      ilosc(atoi(CONVTIME(shutdown_delay * 60)), "a", "y", "o") +
                 " ju� tylko " +
                 CONVTIME(shutdown_delay * 60) +
                 ", potem nastan� nowe czasy.\n" +
                 "Przez chwil� masz wra�enie, �e w powietrzu " +
                 "unosi si� md�y zapach krwi.";
          break;
        case SCENE_THEEND:
          return "Nadszed� czas. Rzeki sp�yn� krwi�, "+
               "a �wiat zginie w odm�tach wojen...";
        case SCENE_CANCEL:
          return "Dochodzi twych uszu ch�odny szept: " +
                 capitalize(shutdown_shutter) + " postanowi�" +
                 (find_player(shutdown_shutter)->query_gender() == G_FEMALE ?
                 "a" : "") + " wstrzyma� nadci�gaj�c� zag�ad�. " +
                 "Na razie �wiat zostanie oszcz�dzony.";
      }
      break;
  }
}

/*
 * Function name: create_creature
 * Description  : Called to create the statuette.
 */
public void
create_creature()
{
  ustaw_imie(({"armageddon", "armageddonu", "armageddonowi",
        "armageddon", "armageddonem", "armageddonie"}),
      PL_MESKI_NZYW);

  dodaj_nazwy(({"statua", "statuy", "statui", "statue", "statua",
        "statui"}),
      ({"statuy", "statui", "statuom", "statuy", "statuami",
       "statuach"}), PL_ZENSKI);

  dodaj_przym("ma^ly", "ma^li");

  ustaw_shorty(({"ma^la statua", "ma^lej statuy", "ma^lej statui",
        "ma^la statue", "ma^la statua", "ma^lej statui"}),
      ({"ma^le statuy", "ma^lych statui", "ma^lym statuom",
       "ma^le statuy", "ma^lymi statuami", "ma^lych statuach"}),
      PL_ZENSKI);

  set_long("Ma^la statuetka przedstawiaj^aca Armageddon!\n");

  set_living_name("armageddon");
  set_tell_active(1);

  add_prop(LIVE_I_ALWAYSKNOWN, 1);
}


/*
 * Funkcja   : armageddon_tell
 * Opis      : Wysyla komunikat do jednego z graczy.
 * Argumenty : gracz - wskazany gracz
 *             str - wysylany komunikat
 */
static void
armageddon_tell(object gracz, string str)
{
  gracz->catch_tell(str+"\n");
}

/*
 * Funkcja   : armageddon_tellall
 * Opis      : Wysyla komunikat do wszystkich graczy.
 * Argumenty : scene - numer sceny do wy�wietlenia
 */
static void
armageddon_tellall(int scene)
{

  foreach (object x : users())
  {
    if ((scene == SCENE_INTRO) || (scene == SCENE_INTRO12) || (scene == SCENE_ROOT12)) {
      set_sequence(x);
    }
    armageddon_tell(x, get_scene(x, scene));
  }
}

/*
 * Function name: shutdown_now
 * Description  : When the game finally goes down, this is the function
 *                that tells the master to do so.
 */
private nomask void
shutdown_now()
{
#ifdef SLOWNIK_AKTYWNY
  SLOWNIK->zapisz_slownik();
#endif
  armageddon_tellall(SCENE_THEEND);

  if (!SECURITY->master_shutdown(sprintf("%-11s: %s\n",
          capitalize(shutdown_shutter), shutdown_reason)))
  {
    armageddon_tellall(SCENE_WEIRD);

    shutdown_alarm   = 0;
    shutdown_delay   = 0;
    shutdown_reason  = 0;
    shutdown_shutter = 0;
  }
}

/*
 * Funkcja   : shutdown_dodelay
 * Opis      : Odlicza czas pozostaly do reboota.
 * Argumenty : silent - za pierwszym razem o pozostalym im czasie gracze
 *                      dowiedza sie gdzie indziej
 */
private nomask void
shutdown_dodelay(int silent = 0)
{
  int period;

  /* No more delay, it is closing time. */
  if (!shutdown_delay)
  {
    shutdown_now();
    return;
  }

  if (!silent) {
    armageddon_tellall(SCENE_DELAY);
  }

  /* If the shutdown period is longer, we will not notify the players
   * each minute, but use a larger delay.
   */
  if (shutdown_delay >= 1080)
    period = 720;
  else if (shutdown_delay >= 90)
    period = 60;
  else if (shutdown_delay >= 25)
    period = 15;
  else if (shutdown_delay >= 10)
    period = 5;
  else
    period = 1;

  shutdown_alarm = set_alarm((itof(period * 60)), 0.0, shutdown_dodelay);
  shutdown_delay -= period;
}

/*
 * Function name: shutdown_started
 * Description  : This function is called when the game is shut down. It
 *                can be redefined by the local armageddon object at your
 *                mud.
 */
public void
shutdown_started()
{

}

/*
 * Function name: start_shutdown
 * Description  : When the game has to be shut down in a gentle way,
 *                this is the function you are looking for. You should
 *                not try to call it directly, but use the 'shutdown'
 *                command.
 * Arguments    : string reason  - the reason to close the game.
 *                int    delay   - the delay in minutes.
 *                string shutter - who is shutting down the game.
 */
public nomask void
start_shutdown(string reason, int delay, string shutter)
{
  string komunikat;

  if (previous_object() != find_object(SECURITY))
    return;

  if (strlen(reason)) {
      switch (reason[-1..])
      {
          case ".":
              case "!":
              case "?":
              break;
          default:
          reason += ".";
      }
  }

  shutdown_shutter = shutter;
  shutdown_reason  = reason;
  shutdown_delay   = delay;

  /* When shutdown is started, we destruct the queue and tell the people
   * to get back later.
   */
  QUEUE->tell_queue("Niedlugo (jeszcze " + CONVTIME(shutdown_delay * 60) +
      ") nastapi restart Vatt'gherna. Prosze sprobowac polaczyc " +
      "sie ponownie, kiedy gra znowu wstanie. Sam restart " +
      "nie powinien trwac dluzej, niz kilka minut.\n");
  (QUEUE->queue_list(0))->remove_object();

#ifdef STATUE_WHEN_LINKDEAD
#ifdef OWN_STATUE
  /* Since people are not allowed to re-link when the game is about to
   * be shut down, we inform the statue-object of the fact so they can
   * save the players and log them out.
   */
  OWN_STATUE->shutdown_activated();
#endif OWN_STATUE
#endif STATUE_WHEN_LINKDEAD

  if (!shutdown_delay)
  {
    if (shutter == ROOT_UID) {
      armageddon_tellall(SCENE_ROOT);
    }
    else {
      armageddon_tellall(SCENE_INTRO);
    }
    shutdown_now();
    return;
  }

  shutdown_started();

  if (shutter == ROOT_UID) {
    armageddon_tellall(SCENE_ROOT12);
  }
  else {
    armageddon_tellall(SCENE_INTRO12);
  }


  shutdown_dodelay(1);
}

/*
 * Function name: shutdown_stopped
 * Description  : This function is called when the shutdown process is
 *                stopped. It may be redefined by the local armageddon
 *                object at your mud.
 * Arguments    : string stopper - the one who decided not to stop.
 */
public void
shutdown_stopped(string stopper)
{
}

/*
 * Function name: cancel_shutdown
 * Description  : If the wizard who was shutting the game down changed
 *                his mind, this is the way to stop it. Do not call the
 *                function directly, though use: 'shutdown abort'
 * Arguments    : string shutter - the person canceling the shutdown.
 */
public nomask void
cancel_shutdown(string shutter)
{
  if (previous_object() != find_object(SECURITY))
    return;

  set_this_player(this_object());

  shutdown_shutter = shutter;

  armageddon_tellall(SCENE_CANCEL);

  shutdown_stopped(capitalize(shutter));

  remove_alarm(shutdown_alarm);

  shutdown_shutter = 0;
  shutdown_reason  = 0;
  shutdown_alarm   = 0;
  shutdown_delay   = 0;
}

/*
 * Function name: shutdown_active
 * Description  : Returns true if Armageddon is active.
 * Returns      : int 1/0 - true if Armageddon is active.
 */
public nomask int
shutdown_active()
{
    return sizeof(get_alarm(shutdown_alarm));
}

/*
 * Function name: query_shutter
 * Description  : Return the name of the person shutting us down.
 * Returns      : string - the name.
 */
public nomask string
query_shutter()
{
  return shutdown_active() ? shutdown_shutter : 0;
}

/*
 * Function name: query_reason
 * Description  : Return the reason for the shutdown.
 * Returns      : string - the reason.
 */
public nomask string
query_reason()
{
  return shutdown_active() ? shutdown_reason : 0;
}

/*
 * Function name: shutdown_time
 * Description  : This function returns how long it will take before the
 *                game is shut down.
 * Returns      : int - the remaining time in minutes.
 */
public nomask int
shutdown_time()
{
    if (!shutdown_active())
        return 0;

    /* Get the remaining time until the next alarm and the time needed
     * after the next alarm is called.
     */
    return ftoi(get_alarm(shutdown_alarm)[2]) + (shutdown_delay * 60);
}
