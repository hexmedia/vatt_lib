#pragma save_binary
#pragma strict_types
//#pragma no_clone

inherit "/std/object";

#include <files.h>
#include <macros.h>
#include <std.h>
#include <stdproperties.h>
#include <log.h>
#include <mail.h>

/* It is not allowed to read notes larger than 100 lines without more. This
 * is done to prevent errors when trying to write too much text to the screen
 * of the player at once.
 */
#define MIN_HEADER_LENGTH   3
#define MAX_HEADER_LENGTH  37
#define MIN_RANK_LENGTH     3
#define MAX_RANK_LENGTH    12
#define MIN_NAME_LENGTH     3
#define MAX_NAME_LENGTH    11
#define RANK_BEGIN         38
#define AUTHOR_BEGIN       51
#define AUTHOR_END         61
#define LENGTH_BEGIN       63
#define LENGTH_END         65

#define HEADER_FORMAT      "%-37s %s %-11s %s  %-7s"
#define RENAME_HEADER_FORMAT   "%s%12s %-11s%s"
#define LEGAL_CHARS        "abcdefghijklmnopqrstuvwxyz'-"
#define MIN_WRITE_AGE	   43200

/*
 * Global variables. They are not savable, the first two are private too,
 * which means that people cannot dump them.
 */
static private mixed   headers = ({ });
static private mapping comments = ([ ]);
static private mapping writing = ([ ]);

static string  board_name = "";
static string  comments_path = "";
static string  remove_str = "Na tej tablicy notki kasowa� mog� tylko czarodzieje pe�ni�cy funkcje MG oraz administracja(ARCHY).\n";
static int     keep_discarded = 0;
static int     notes = 2000;
static int     silent = 1;
static int     msg_num;
static int     remove_rank = WIZ_ARCH;
static int     show_lvl = 1;
static int     comment_lvl = 10;
static int     no_report = 0;
static int     fuse = 0;

/*
 * Prototypes.
 */
public nomask  int list_notes(string str);
public nomask  int new_msg(string msg_head);
public varargs int read_msg(string what_msg, int mr);
public nomask  int remove_msg(string what_msg);
public nomask  int rename_msg(string str);
public nomask  int store_msg(string str);
nomask private string *extract_headers(string str);

/*
 * Function name: set_num_notes
 * Description  : Set the maximum number of notes on the board.
 * Arguments    : int n - the number of notes on the board. (default: 30)
 */
public nomask void
set_num_notes(int n)
{
    notes = (fuse ? notes : n);
}

/*
 * Function name: set_silent
 * Description  : Set this to make the board silent. That means that
 *                bystanders are not noticed of people examining the
 *                board and reading notes.
 * Arguments    : int n - true if silent (default: true)
 */
public nomask void
set_silent(int n)
{
    silent = (fuse ? silent : (n ? 1 : 0));
}

/*
 * Function name: set_no_report
 * Description  : If you set this to true, the central board master is
 *                not notified of new notes. The only thing this does is
 *                preventing MBS from showing the board.
 * Arguments    : int n - true if the central board master should not
 *                        be notified. (default: false)
 */
public nomask void
set_no_report(int n)
{
    no_report = (fuse ? no_report : (n ? 1 : 0));
}

/*
 * Function name: set_board_name
 * Description  : This function sets the path in which the notes on the
 *                board are kept. This must _always_ be set!
 * Arguments    : strint str - the pathname of the board. It should not
 *                             end with a /.
 */
public nomask void
set_board_name(string str)
{
    board_name = (fuse ? board_name : str);
}

/*
 * Function name: query_board_name
 * Description  : Return the name of the board. This is the path of the
 *                directory in which the notes are stored.
 * Returns      : string - the name of the board (path-name).
 */
public nomask string
query_board_name()
{
    return board_name;
}

/**
 * Ustawia scie�k� z komentarzami do notek.
 * @param str Scie�ka
 */
public nomask void
set_comments_path(string str)
{
    comments_path = (fuse ? comments_path : str);
}

/**
 * @return Scie�k� z komentarzami do notek.
 */
public nomask string
query_comments_path(string str)
{
    return comments_path;
}

/*
 * Function name: set_remove_rank
 * Description  : With this function you can set the minimum rank people
 *                must have to be able to remove other peoples notes.
 *                NOTE: The argument to this function must be a define
 *                      from std.h, ie WIZ_NORMAL, WIZ_LORD, etc.
 * Arguments    : int n - the minimum level (default WIZ_LORD, ie lords++).
 */
public nomask void
set_remove_rank(int n)
{
    remove_rank = (fuse ? remove_rank : n);
}

/*
 * Function name: set_remove_str
 * Description  : Set the string to be printed when a player tries to
 *                remove a note while he is not allowed to do so.
 * Arguments    : string str - the message (default: Only a Lord is
 *                             allowed to remove other peoples notes.)
 */
public nomask void
set_remove_str(string str)
{
    remove_str = (fuse ? remove_str : str);
}

/*
 * Function name: set_show_lvl
 * Description  : Set this if you want the rank of the authors of the notes
 *                added to the header of the notes.
 * Arguments    : int n - true if the rank should be show (default: true)
 */
public nomask void
set_show_lvl(int n)
{
    show_lvl = (fuse ? show_lvl : (n ? 1 : 0));
}

/*
 * Function name: set_keep_discarded
 * Description  : Set this if you want old notes to be kept. They will be
 *                kept if in a directory with the same name as the
 *                actual notes, though with _old appended to it.
 * Arguments    : int n - true if old notes should be kept (default: false)
 */
public nomask void
set_keep_discarded(int n)
{
    keep_discarded = (fuse ? keep_discarded : (n ? 1 : 0));
}

/*
 * Function name: query_author
 * Description  : Return the name of the author of a certain note. Observe
 *                that the note-numbers start counting at 1, like the way
 *                they appear on the board.
 * Argument     : int note - the number of the note to find the author of.
 * Returns      : string - the name of the author or 0 if the note doesn't
 *                         exist.
 */
public nomask string
query_author(int note)
{
    if (note < 1 || note > msg_num)
	return 0;

    return lower_case(explode(headers[note - 1][0][AUTHOR_BEGIN..AUTHOR_END],
                              " ")[0]);
}

/*
 * Function name: no_special_fellow
 * Description  : Some people can always handle the board. If you own the
 *                board (same euid), if the board is in your domain, or
 *                if you are a member of the administration or the Lord of
 *                the domain the board it in, you can always meddle.
 * Returns      : int - 1/0 - true if (s)he is not a special fellow.
 */
nomask public int
no_special_fellow()
{
    string name = this_interactive()->query_real_name();
    string euid = geteuid();

    /* I'm an arch or keeper, so I can do anything. */
    if (SECURITY->query_wiz_rank(name) >= WIZ_ARCH)
    {
	return 0;
    }

    /* The board is mine */
    if (name == euid)
    {
	return 0;
    }

    /* The board is my domains */
    if (euid == SECURITY->query_wiz_dom(name))
    {
	return 0;
    }

    /* I am Lord and the board is of one of my wizards. */
    if ((SECURITY->query_wiz_rank(name) == WIZ_LORD) &&
	(SECURITY->query_wiz_dom(name) == SECURITY->query_wiz_dom(euid)))
    {
	return 0;
    }

    return 1;
}

/*
 * Function name: block_reader
 * Description  : If this_player() is not allowed to read notes of other
 *                people on this board, this function should be used to
 *                block access. If you print an error message on failure,
 *                please check whether this_player() is in the environment
 *                of the board.
 * Returns      : int - true if the player is NOT allowed to read.
 */
public int
block_reader()
{
    return 0;
}

/*
 * Function name: check_reader
 * Description  : This function will return true if this_player() is allowed
 *                to read on the board. Note that players may always read
 *                their own notes.
 * Arguments    : int note - an optional argument for a particular note to
 *                           be checked. If omitted give general access to
 *                           the board.
 * Returns      : int 1/0 - true if the player is prohibited to read.
 */
public nomask varargs int
check_reader(int note = 0)
{
    if (note &&
	(this_player()->query_real_name() == query_author(note)))
    {
	return 0;
    }

    return (block_reader() ? no_special_fellow() : 0);
}

/*
 * Function name: block_writer
 * Description  : If this_player() is not allowed to write notes on this
 *                board, this function should be used to block access.
 *                If you print an error message on failure, please check
 *                whether this_player() is in the environment of the board.
 * Returns      : int 1/0 - true if the player is NOT allowed to write.
 */
public int
block_writer()
{
    return 0;
}

/*
 * Function name: check_writer
 * Description  : This function will return true if this_player() is allowed
 *                to write on the board.
 * Returns      : int 1/0 - true if the player may not write.
 */
public nomask int
check_writer()
{
    return (block_writer() ? no_special_fellow() : 0);
}

/*
 * Function name: allow_remove
 * Description  : This function checks whether this_player() is allowed
 *                to remove notes from this board. If you print an error
 *                message on failure, please check whether this_player()
 *                is in the environment of the board. This function works
 *                independant of the set_remove_rank function.
 * Returns      : int - true if the player is allowed to remove notes.
 */
public int
allow_remove()
{
    return 0;
}

/*
 * Function name: check_remove
 * Description  : This function will return true if this_player() is allowed
 *                to remove (other peoples) notes from the board.
 * Arguments    : int note - an optional argument for a particular note to
 *                           be checked. If omitted give general access to
 *                           the board.
 * Returns      : int 1/0 - true if the player may not remove the note.
 */
public nomask varargs int
check_remove(int note = 0)
{
    if (note &&
	(this_player()->query_real_name() == query_author(note)))
    {
	return 0;
    }

    return (no_special_fellow() &&
	    (SECURITY->query_wiz_rank(this_player()->query_real_name()) <
	     remove_rank) &&
	    !allow_remove());
}

/*
 * Function name: load_headers
 * Description  : Load the headers when the board is created. This is done
 *                in a little alarm since it is also possible to configure
 *                the board by cloning it and then calling the set-functions
 *                externally. This function also sets the fuse that makes
 *                it impossible to alter the board-specific properties.
 */
private nomask void
load_headers()
{
    string *arr;

    /* Set the fuse to make it impossible to alter any of the board-specific
     * properties.
     */
    fuse = 1;

    seteuid(getuid());

    arr = get_dir(board_name + "/b*");
    msg_num = sizeof(arr);
    headers = (msg_num ? map(arr, extract_headers) : ({ }) );
}

/**
 * �aduje komentarze do danej notki, podczas pierwszego odczytu notki.
 * @param file nazwa pliku notki
 */
private nomask void
load_comments(string file)
{
    string *arr;

    fuse = 1;

    seteuid(getuid());
    
    if(file_size(comments_path) != -2)
        return;
    if(file_size(comments_path + "/" + file) != -2)
        return;

    arr = get_dir(comments_path + "/" + file + "/c*");
    comments[file] = arr;
}

/*
 * Function name: create_board
 * Description  : Since create_object() is nomasked, you must redefine this
 *                function in order to create your own board.
 */
void
create_board()
{
  set_board_name("/d/Standard/wiz/zlamania_zasad");
  set_comments_path("/d/Standard/wiz/zlamania_zasad/comments");
  set_silent(0);
  set_num_notes(2000);
  set_remove_rank(WIZ_ARCH);
  add_prop(WIZARD_ONLY, 1);
}

/*
 * Function name: create_object
 * Description  : Create the object. Use create_board() to set up the
 *                board yourself.
 */
public nomask void
create_object()
{
    ustaw_nazwe("tablica og�oszeniowa");
    dodaj_nazwy("tablica");

    ustaw_shorty("tablica og�oszeniowa");

    dodaj_przym("og�oszeniowy", "og�oszeniowi");

    set_long("Jest to tablica og�oszeniowa, na kt�rej mo�esz umieszcza� " +
	"adresowane do innych notki ku nauce, przestrodze, rozwadze czy " +
        "te� uciesze czytelnik�w.\n");

    add_prop(OBJ_M_NO_GET,
             "Tablica og�oszeniowa jest zbyt mocno przymocowana.\n");

    create_board();

    /* We must do this with an alarm because it is also possible to clone
     * the /std/board.c and confugure it with the set_functions.
     */
    set_alarm(0.5, 0.0, load_headers);

    enable_reset();
}

/*
 * Function name: reset_board
 * Description  : Since reset_object() is nomasked, you must mask this
 *                function at reset time. Enable_reset() is already
 *                called, so you do not have to do that yourself.
 */
void
reset_board()
{
/* 
(!!!) czekam na dobry pomysl na zamiane. Alvin.
    if (!random(5))
    {
	tell_room(environment(),
	    "A small gnome appears and secures some notes on the " +
	    short() + " that were loose.\nThe gnome then leaves again.\n");
    }
*/
}

/*
 * Function name: reset_object
 * Description  : Every half hour or about, the object resets. Use
 *                reset_board() for user reset functionality.
 */
nomask void
reset_object()
{
    reset_board();
}

/*
 * Nazwa funkcji: count_notes
 * Opis         : Funkcja zwraca informacje na temat liczby notek na tablicy.
 */
public nomask string
count_notes()
{
    string str;

    if (!msg_num)
        return capitalize(short(PL_MIA)) + " jest pust"
             + koncowka("y", "a", "e");

    str = capitalize(short(PL_MIA)) + " zawiera "
        + LANG_SNUM(msg_num, PL_BIE, PL_ZENSKI) + " ";

    str += ilosc(msg_num, "notk�", "notki", "notek");

    return str;
}

/**
 * @return Liczbe komentarzy do notki
 */
public nomask int
        count_comments(string note_file)
{
    return sizeof(get_dir(comments_path + "/" + note_file + "/"));
}

/*
 * Nazwa funkcji: list_headers
 * Opis         : Funkcja zwraca naglowki z podanego zakresu.
 */
public nomask string
list_headers(int start, int end, string match = "*")
{
    string str = "";

    /* Check whether the range is valid. */
    if (end < 1 || end > msg_num)
        end = msg_num;

    if (start > end)
        start = end;
    else if (start < 1)
        start = 1;

    start -= 2;

    /* If the player is not allowed to read the board, only display the
     * notes the player wrote him/herself.
     */
    if (check_reader())
    {
        string name = this_player()->query_real_name();

        while (++start < end)
            if (name == query_author(start + 1))
                str += sprintf("%2d(%d): %s\n", start + 1, count_comments(headers[start][1]), headers[start][0]);
    }
    else
        while (++start < end)
            if (wildmatch(match, headers[start][0]))
                str += sprintf("%2d(%d): %s\n", start + 1, count_comments(headers[start][1]), headers[start][0]);

    return str;
}

public nomask string
print_comments(string note_file)
{
    int i=-1;
    string ret = "";
   
    if(!pointerp(comments[note_file]))
        load_comments(note_file);
    if(!pointerp(comments[note_file]))
        return "Brak komentarzy do tej notki.\n";
    
    while(i++ < sizeof(comments[note_file])-1)
         ret += read_file(comments_path + "/" + note_file + "/" + comments[note_file][i], 0, 20);
 
    return ret;
}

/*
 * Function name: long
 * Description  : This function returns the long description on the board.
 * Arguments    : string item    - pseudo-item do opisania.
 *                object for_obj - obiekt dla ktorego przenzczony jest opis.
 * Returns      : string - the long description.
 */
public nomask string
long(string item = 0, object for_obj = this_player())
{
    string str;

    if (item)
        return ::long(item, for_obj);

    str = check_call(query_long(), this_player())
        + "Komendy: napisz notk�, usu� [notk�] <numer>, "
        + "przejrzyj [notki] [<odk�d-dok�d>],\n"
        + "         przeczytaj [notk�] <numer>, "
        + "przeczytaj poprzedni�/nastepn� [notk�]"
        + (this_player()->query_wiz_level() ?
           ",\n         zachowaj [notk�] <numer> <nazwa pliku>" : "") + "\n\n"
        + count_notes();

    if (!msg_num)
        return str + ".\n";

    if (msg_num < 17)
        return str + ":\n" + list_headers(1, msg_num);

    return str + ". Oto najnowsze z nich:\n"
         + list_headers(msg_num - 15, msg_num);
}

public int
comment_writed(object player, string file_name, string note_file, string content)
{
    if(!content)
    {
        write("Brak tre�ci.\n");
	return 0;
    }    

    content = sprintf("%30|s %40|s\n", UC(player->query_real_name()), ctime(time())) +
        sprintf("%79|s\n", "-----------------------------------------------------------------------------") + content +
  	sprintf("%79|s\n", "-----------------------------------------------------------------------------");

    setuid();
    seteuid(getuid());
    if(!write_file(file_name, content))
    {
        write("Wyst�pi� b��d podczas tworzenia pliku komentarza.\n");
	return 0;
    }

    write("Tw�j komentarz zosta� dodany.\n");
    load_comments(note_file);
    
    return 1;
}

public int
comment_msg(string arg)
{
    mixed nr;
    string file_name;

    notify_fail("Co takiego chcesz skomentowa�?\n");

    if(!arg)
        return 0;

    if(sscanf(arg, "notke %s", nr) != 1)
        if(sscanf(arg, "%s", nr) != 1)
            return 0;

    if(!(nr = atoi(nr)))
        nr = LANG_NUMS(nr);

    if(nr > msg_num)
    {
        notify_fail("Nie ma notki o takim numerze.\n");
	return 0;
    }

    if(SECURITY->query_wiz_level(TP->query_real_name()) <= comment_lvl)
    {
        notify_fail("Jaka� magiczna si�a nie pozwala Ci skomentowa� �adnej notki.\n");
        return 0;
    }
    
    if(file_size(headers[--nr][1]) != -2)
        mkdir(comments_path + "/" + headers[nr][1]);

    file_name = comments_path + "/" + headers[nr][1] + "/c" + time();
    int t = time();
    while(file_size(file_name) != -1)
        file_name = comments_path + "/" + headers[nr][1] + "/c" + (++t);
    
    object editor;
    editor = clone_object(EDITOR_OBJECT);
    editor->edit(&comment_writed(TP, file_name, headers[nr][1]));
    return 1;
}
/*
 * Function name: init
 * Description  : Link the commands to the player.
 */
public void
init()
{
    ::init();

    /* Only interactive players can write notes on boards. */
    if (!query_ip_number(this_player()))
    {
	return;
    }

    add_action(list_notes,  "przejrzyj");
    add_action(new_msg,     "napisz");
    add_action(read_msg,    "przeczytaj");
    add_action(remove_msg,  "usu�");
    add_action(comment_msg, "skomentuj");
    add_action(rename_msg,  "rename");
    add_action(store_msg,   "zachowaj");
}

/*
 * Function name: extract_headers
 * Description  : This is a map function that reads the note-file and
 *                extracts the headers of the note.
 * Arguments    : string str - the name of the note.
 * Returns      : string* - ({ header-string, note-name })
 */
private nomask string *
extract_headers(string str)
{
    string title;
    int time;

    seteuid(getuid());

    if (sscanf(str, "b%d", time) != 1)
	return 0;
    str = "b" + time;

    if (!stringp(title = read_file(board_name + "/" + str, 1, 1)))
	return 0;

    return ({ extract(title, 0, strlen(title) - 2), str });
}

/*
 * Function name: query_latest_note
 * Description  : Find and return the name of the last note on the board.
 * Returns      : string - the filename (without path) of the last note
 *                         on the note or 0 if no notes are on the board.
 */
public nomask string
query_latest_note()
{
    return (msg_num ? headers[msg_num - 1][1] : 0);
}

/*
 * Function name: query_num_messages
 * Description  : Find a return the number of messages on this board.
 * Returns      : int - the number of messages on the board.
 */
public nomask int
query_num_messages()
{
    return msg_num;
}

/*
 * Function name: list_notes
 * Description  : With this command a player can list only a portion of
 *                the board.
 * Arguments    : string str - the command line argument.
 * Returns      : int 1/0 - success/failure.
 */
public nomask int
list_notes(string str)
{
    string from, to;
    int start, end;

    if (str ~= "globalne b^l^edy")
    {
	write(list_headers(0, 0, "*GLOBALNY B^L^AD*"));
	return 1;
    }
    if (str ~= "globalne pomys^ly")
    {
	write(list_headers(0, 0, "*GLOBALNY POMYS^L*"));
	return 1;
    }
    if (str ~= "b^l^edy")
    {
	write(list_headers(0, 0, "*b^l^ad*"));
	return 1;
    }
    if (str ~= "pomys^ly")
    {
	write(list_headers(0, 0, "*pomys^l*"));
	return 1;
    }

    mixed note_number;
    if(str)
    {
        if(sscanf(str, "komentarze do notki %s", note_number) || sscanf(str, "komentarze do %s", note_number))
        {
            string note_file;

            if(!(note_number = atoi(note_number)))
                note_number = LANG_NUMS(note_number);
            if(!note_number)
            {
                notify_fail("Podaj poprawny numer.\n");
                return 0;
            }

            if(note_number > msg_num)
            {
                notify_fail("Nie ma notki o tym numerze, jak wi�c chcesz " + 
                    "przejrze� do niej komentarze?\n");
                return 0;
            }

            note_number--;

            write(print_comments(headers[note_number][1]));
            say(QCIMIE(TP, PL_MIA) + " przegl�da komentarze kt�rej� z notek.\n");

            return 1;	
        }
    }

    if (!str || str == "notki")
        str = "-";
    else
        sscanf(str, "notki %s", str);

    if (sscanf(str, "%s-%s", from, to) != 2 ||
        !(start = atoi(from)) && from != "" ||
        !(end = atoi(to)) && to != "")
    {
        notify_fail("Z�y zakres. Prawid�owo powinien mie� form� "
                  + "\"odk�d-dok�d\", \"odk�d-\" lub \"-dok�d\".\n");
        return 0;
    }

    if (!silent && present(this_player(), environment()))
        saybb(QCIMIE(this_player(), PL_MIA) + " przegl�da " + short(PL_BIE)
            + ".\n");

    if (!msg_num)
        write(count_notes() + ".\n");
    else
        this_player()->more(count_notes() + ":\n" + list_headers(start, end));

    return 1;
}

/*
 * Nazwa funkcji: get_subject
 * Opis         : Po pobraniu i sprawdzeniu poprawnosci tytulu notki odpala
 *                edytor liniowy.
 * Argumenty    : msg_head: Tytul notki.
 */
public nomask void
get_subject(string msg_head)
{
    string *times;
    string date;
    string rank;

//    switch (msg_head = implode(explode(msg_head, " ") - ({""}), " "))
    switch (msg_head = implode(explode(implode(explode(msg_head, " ") - ({""}), " "), "@@"), "##"))
    {
        case "":
            write("Brak tytu�u notki.\n");
            return;

        case "~q":
            write("Pisanie notki zaniechane.\n");
            return;
    }

    if (strlen(msg_head) > MAX_HEADER_LENGTH)
    {
        write("Zbyt d�ugi tytu� notki, spr�buj jeszcze raz.\n");
        return;
    }

    if (strlen(msg_head) < MIN_HEADER_LENGTH)
    {
        write("Zbyt kr�tki tytu� notki, spr�buj jeszcze raz.\n");
        return;
    }

    if (present(this_player(), environment()))
        saybb(QCIMIE(this_player(), PL_MIA) + " zaczyna pisa� notk�.\n");

    this_player()->add_prop(LIVE_S_EXTRA_SHORT,
        " pisz�c" + this_player()->koncowka("y", "a", "e") + " notk�");

    /* We use an independant editor and therefore we must save the header
     * the player has typed. When the player is done editing, the header
     * will be used again to save the message. Don't you just love these
     * kinds of statements.
     */
    times = explode(ctime(time(), 1), " ");
    date = sprintf("%2s %-4s", times[1], times[2]);
//    rank = show_lvl ?
//        (SECURITY->query_wiz_pretitle(this_player()))[..MAX_RANK_LENGTH] : "";
    rank = "";

    writing[this_player()] = sprintf(HEADER_FORMAT, msg_head, sprintf("%d", rank),
        capitalize(this_player()->query_real_name()), "???", date);

    seteuid(getuid());

    clone_object(EDITOR_OBJECT)->edit("done_editing", "");
}

/*
 * Nazwa funkcji: new_msg
 * Opis         : Gracz chce napisac notke.
 * Argumenty    : str: Argument komendy.
 */
public nomask int
new_msg(string str)
{
    if (!(str ~= "notk�"))
    {
        notify_fail("Co chcesz napisa�? Mo�e notk�?\n");
        return 0;
    }

   if(!HAS_FREE_HAND(this_player()))
   {
       write("Musisz miec przynajmniej jedna d�o� woln�, aby m�c to zrobi�.\n");
       return 1;
   }
   
    if (this_player()->query_real_name() == GUEST_NAME)
    {
        write("Jeste� go�ciem na Vatt'ghernie i jako taki nie jeste� uprawniony do "
            + "pisania na tablicach og�oszeniowych. Je�li chcesz wzi�� "
            + "udzia� w dyskusji, stw�rz sobie prawdziw� posta�. Nowy gracz "
            + "jest zawsze mile widziany na Vatt'ghernie.\n");
        return 1;
    }
    
    if ((this_player()->query_age() < MIN_WRITE_AGE) &&
        (!this_player()->query_wiz_level()))
    {
	write("Niestety, osoby o wieku mniejszym ni� 1 dzien nie mog� " +
	    "zamieszcza� notek na tablicach og�oszeniowych. Obejrzyj troch� " +
	    "�wiata, zanim zaczniesz si� publicznie wypowiada�.\n");
	return 1;
    }

    /* Player is not allowed to write a note on this board. */
    if (check_writer())
    {
        notify_fail("Nie jeste� uprawnion" + 
            this_player()->koncowka("y", "a", "y") + " by umieszcza� "+
            "nowe notki.\n");
	return 0;
    }

    write("Podaj tytu� notki: ");
    input_to(get_subject);
    return 1;
}

/*
 * Function name: block_discard
 * Description  : If this function returns 1, it will prevent the
 *		  removal of a note.
 * Arguments	: file - the file to remove
 */
public int
block_discard(string file)
{
    return 0;
}

/*
 * Function name: discard_message
 * Description  : When there are too many notes on the board, the oldest
 *                is discarded. It is either moved or deleted.
 * Arguments    : string file - the file (without path) to save.
 */
private nomask void
discard_message(string file)
{
    seteuid(getuid());

    if (block_discard(file))
	return;

    if (keep_discarded)
    {
	if (file_size(board_name + "_old") == -1)
	{
	    mkdir(board_name + "_old");
	}

	//Jak kto� zrobi funkcje copy to mo�na to przenosi�, ale i tak
	//z tego co widze nie ma zapamietywania wi�c chyba nie ma sensu
	//(krun)
    }
    else
    {
	rm(board_name + "/" + file);
    }
    
    string *files;
    files = get_dir(comments_path + "/" + file + "/");
 
    int i = -1;
    
    while(i++ < sizeof(files)-1)
        rm(comments_path + "/" + file + "/" + files[i]);
    rmdir(comments_path + "/" + file);
}

/*
 * Function name: post_note_hook
 * Description  : This function is called when someone posts a note.
 * Arguments    : string head - the header of the note.
 */
public void
post_note_hook(string head)
{
    string dev_null;
    
    head = head[0..36];
    sscanf(head, "%s %s", head, dev_null);
    
    if (present(this_player(), environment()))
        saybb(QCIMIE(this_player(), PL_MIA) + " ko�czy pisanie notki " +
        "zatytu�owanej \"" + head + "\", kt�ra otrzymuje numer " + 
        msg_num + ".\n");
}

/*
 * Function name: post_note
 * Description  : This function actually posts the note on the board,
 *                stores it to disk and notifies the board master.
 * Arguments    : string head    - the header of the note.
 *                string message - the message body.
 */
private nomask void
post_note(string head, string message)
{
    string author, fname;
    int    t;
    
    /* If there are too many notes on the board, remove them. */
    while(msg_num >= notes)
    {
        author = this_player()->query_real_name();

        /* Piata notka pod rzad */
        if (author == query_author(msg_num) &&
            author == query_author(msg_num - 1) &&
            author == query_author(msg_num - 2) &&
            author == query_author(msg_num - 3))
            log_file("board_recycle", ctime(time()) + ": "
                   + headers[msg_num - 1][0] + "\n");

	discard_message(headers[0][1]);
	headers = exclude_array(headers, 0, 0);
	msg_num--;
    }

    seteuid(getuid());

    /* If the directory doesn't exist, create it. */
    if (file_size(board_name) == -1)
    {
	mkdir(board_name);
    }

    /* Check that the message file isn't used yet. */
    fname = "b" + (t = time());
    while(file_size(board_name + "/" + fname) != -1)
    {
	fname = "b" + (++t);
    }

    /* Write the message to disk and update the headers. */
    write_file(board_name + "/" + fname, head + "\n" + message);
    headers += ({ ({ head, fname }) });
    msg_num++;
    
    /* Update the master board central unless that has been prohibited. */
    if (!no_report)
   	BOARD_CENTRAL->new_note(board_name, fname, MASTER_OB(environment()));
   
    post_note_hook(head);
}

/*
 * Function name: done_editing
 * Description  : When the player is done editing the note, this function
 *                will be called with the message as parameter. If all
 *                is well, we already have the header.
 * Arguments    : string message - the note typed by the player.
 * Returns      : int - 1/0 - true if the note was added.
 */
public nomask int
done_editing(string message)
{
    string head;
    int size;

    this_player()->remove_prop(LIVE_S_EXTRA_SHORT);

    if (!strlen(message))
    {
	write("Nie wpisa�" + this_player()->koncowka("e�", "a�", "e�") + 
	    " �adnej informacji.\n");
	if (present(this_player(), environment()))
	{
	    saybb(QCIMIE(this_player(), PL_MIA) + " przerywa "+
	          "wpisywanie notki.\n");
	}

	writing = m_delete(writing, this_player());
	return 0;
    }

    if (!stringp(writing[this_player()]))
    {
    	write("Tw�j nag�owek uleg� zniszczeniu! Nie umie�ci�" +
    	    this_player()->koncowka("e�", "a�", "e�") + " �adnej notatki.\n" +
    	    "Zg�o� b��d!\n");
	return 0;
    }

    head = writing[this_player()];
    size = sizeof(explode(message, "\n"));
    head = head[..(LENGTH_BEGIN - 1)] + (size > 999 ? "***" :
           sprintf("%3d", size)) + head[(LENGTH_END + 1)..];
    writing = m_delete(writing, this_player());

    post_note(head, message);

    write("Ju�.\n");
    return 1;
}

/*
 * Function name: create_note
 * Description  : This function can be called externally in order to write
 *                a note on a board generated by code. The note will then
 *                appear on the board just like any other note. There are a
 *                few restrictions to the note:
 *                - the euid of the object generating the note must be the
 *                  same as the euid of the board;
 *                - the name of the author may _not_ be the name of a player
 *                  in the game, mortal or wizard, nor may it be banished.
 *                In addition to that, you should ensure that the note is
 *                properly formatted and that it has newlines inserted at
 *                regular intervals, preferably before the 80th char/line.
 * Arguments    : string header - the header of the note (min: 3; max: 40)
 *                string author - the alleged author of the note (max: 11)
 *                string body   - the note itself.
 *                string rank   - optionally, the alleged rank (title, etc)
 *                                of author (max: 10)
 * Returns      : int 1/0 - success/failure.
 */
public nomask int
create_note(string header, string author, string body, string rank = "")
{
    string *times;
    string lower = lower_case(author);
    string head;
    int    index = -1;
    int    len;

    if (geteuid() != geteuid(previous_object()))
	return 0;

    /* The author may not be a real player and the length of the name
     * must be in the valid range. */
    len = strlen(lower);
    if (len > MAX_NAME_LENGTH || len < MIN_NAME_LENGTH ||
        SECURITY->exist_player(lower))
    {
	return 0;
    }

    /* Author's name may only be letters, dash (-), or apostrophe ('). */
    while(++index < len)
	if (!wildmatch("*" + lower[index..index] + "*", LEGAL_CHARS))
	{
	    return 0;
	}

    /* Rank has to be either empty or of valid length. */
    len = strlen(rank);
    if (len && (len > MAX_RANK_LENGTH || len < MIN_RANK_LENGTH))
        return 0;

    /* Header size must be correct and there must be a body too. */
    len = strlen(header);
    if (len < MIN_HEADER_LENGTH || len > MAX_HEADER_LENGTH || !strlen(body))
	return 0;

    len = sizeof(explode(body, "\n"));
    times = explode(ctime(time(), 1), " ");
    head = sprintf(HEADER_FORMAT, header, sprintf("%d", rank), author,
                   len > 999 ? "***" : sprintf("%3d", len),
                   sprintf("%2s %-4s", times[1], times[2]));

    post_note(head, body);

    tell_roombb(environment(), "Na " + short(PL_MIE)
              + " spotrzegasz notki kt�rych wcze�niej nie widzia�"
              + koncowka("e�", "a�", "e�") + ".\n", ({}), this_object());
    return 1;
}

/*
 * Function name: read_msg
 * Description  : Read a message on the board
 * Arguments    : string what_msg - the message number.
 *                int    mr       - read with more if true.
 * Returns      : int 1/0 - success/failure.
 */
public nomask varargs int
read_msg(string what_msg, int mr)
{
    int note;
    string in;

    load_headers();

    switch (what_msg)
    {
        case "poprzedni�":
        case "poprzedni� notk�":
            note = this_player()->query_prop(PLAYER_I_LAST_NOTE) - 1;
            break;

        case "bie��c�":
        case "bie��c� notk�":
        case "obecn�":
        case "obecn� notk�":
            note = this_player()->query_prop(PLAYER_I_LAST_NOTE);
            break;

        case "nast�pn�":
        case "nast�pn� notk�":
            note = this_player()->query_prop(PLAYER_I_LAST_NOTE) + 1;
            break;

        default:
            if (!what_msg)
            {
                notify_fail("Przegl�dasz pobie�nie tablic�, ale poniewa� nie "
                          + "mo�esz si� zdecydowa� na nic konkretnego, nie "
                          + "znajdujesz niczego ciekawego.\n");
                return 0;
            }

        /* hihihih. LiL :P */     
        if (this_player()->query_prop(EYES_CLOSED))
        {
            notify_fail("Ciemno��...Ciemno�� widzisz!\n");
            return 0;
        }
    
        mixed note_number;
        if(sscanf(what_msg, "komentarze do notki %s", note_number) || sscanf(what_msg, "komentarze do %s", note_number))
        {
            string note_file;
            if(!(note_number = atoi(note_number)))
                note_number = LANG_NUMS(note_number);
    
            if(!note_number)
            {
                notify_fail("Podaj poprawny numer.\n");
                return 0;
            }
            if(note_number > msg_num)
            {
                notify_fail("Nie ma notki o tym numerze, jak wi�c chcesz " + 
                        "przejrze� do niej komentarze?\n");
                return 0;
            }
    
            note_number--;
    
            write(print_comments(headers[note_number][1]));
            say(QCIMIE(TP, PL_MIA) + " przegl�da komentarze kt�rej� z notek.\n");
    
            return 1;	
        }

	    if (!sscanf(what_msg, "notk� %d", note) &&
                !sscanf(what_msg, "%d", note))
            {
                notify_fail("Kt�r� notk� chcesz przeczyta�?\n");
                return 0;
            }
    }

    if (msg_num < 1)
    {
        notify_fail("Na " + short(PL_MIE) + " nie ma �adnych notek.\n");
        return 0;
    }

    if (note < 1)
    {
        notify_fail("Chyba nie ma notki o takim numerze...\n");
        return 0;
    }

    if (note > msg_num)
    {
/*	if (msg_num == 1)
	    in = "jest";
	else if (msg_num % 10 > 1 && msg_num % 10 < 5 &&
		  (msg_num % 100 < 10 || msg_num % 100 > 20))
	    in = "s�";
	else
	    in = "jest ich"; */
	in = ilosc(msg_num, "jest", "s�", "jest ich");
	    
	notify_fail(sprintf("Nie mo�esz przeczyta� %s notki. " +
	    "Na %s %s tylko %s.\n", LANG_SORD(note, PL_DOP, PL_ZENSKI),
            short(PL_MIE), in, LANG_SNUM(msg_num, PL_MIA, PL_ZENSKI)));
	return 0;
    }

    /* See whether the player can read the note. */
    if (check_reader(note))
    {
    	notify_fail("Nie mo�esz przeczyta� tej notki.\n");
	return 0;
    }

    if(this_object()->query_prop(WIZARD_ONLY) &&
       !(this_player()->query_wiz_level()))
        {
               write(capitalize(short(this_player(), PL_MIA)) + 
                 " jest zapisan" +
                koncowka("y", "a", "e") + " jakimi� tajemniczymi "+
                "znakami, kt�rych nie jeste� w stanie odczyta�. "+
                "Zdaje si�, �e to j�zyk czarodziej�w z wyspy Thanedd.\n");
               return 1;
        }

    this_player()->add_prop(PLAYER_I_LAST_NOTE, note);
    if (present(this_player(), environment()))
    {
        write("Czytasz " + LANG_SORD(note, PL_BIE, PL_ZENSKI) + " notk�.\n");
    }

    note--;
    if (!silent &&
	present(this_player(), environment()))
    {
	saybb(QCIMIE(this_player(), PL_MIA) + " czyta notk� zatytu�owan�:\n" +
	    headers[note][0] + "\n");
    }

    seteuid(getuid());

/*
 * Automatyczny more
    if (!mr)
    {
	mr = (query_verb() == "mread");
    }

    if ((!mr) &&
	(atoi(headers[note][0][42..44]) > MAX_NOM_MREAD))
    {
	write("Too long note. More automatically invoked.\n");
	mr = 1;
    }
*/
   mr = 1;

    if (mr == 1)
    {
	this_player()->more(headers[note][0] + "\n\n" +
	    read_file(board_name + "/" + headers[note][1], 2));
    }
    else
    {
	write(headers[note][0] + "\n\n");
	cat(board_name + "/" + headers[note][1], 2);
    }

    return 1;
}

/*
 * Function name: remove_msg
 * Description  : Remove a note from the board.
 * Arguments    : string what_msg - the message to remove.
 * Returns      : int 1/0 - success/failure.
 */
public nomask int
remove_msg(string what_msg)
{
    int note;

    if(member_array(TP->query_real_name(), EXPAND_MAIL_ALIAS("mg")) == -1 && 
        SECURITY->query_wiz_rank(TP->query_real_name()) < WIZ_ARCH)
    {
        notify_fail("Tylko czarodzieje pe�ni�cy funkcje MG mog� usuwa� notki z tej tablicy.\n");
	return 0;
    }

    if (!stringp(what_msg))
    {
	notify_fail("Ktor� notk� chcesz usuna�?\n");
	return 0;
    }

    if (!sscanf(what_msg, "notk� %d", note) &&
	!sscanf(what_msg, "%d", note))
    {
	notify_fail("Kt�r� notk� chcesz usuna�?\n");
	return 0;
    }

    if ((note < 1) ||
	(note > msg_num))
    {
	write("Zdaje si�, �e notka nr " + note + " nie istnieje.\n");
	return 1;
    }

    if (check_remove(note))
    {
	write(remove_str);
	return 1;
    }

    note--;
    if (present(this_player(), environment()))
    {
	saybb(QCIMIE(this_player(), PL_MIA) + " usuwa notk�:\n" +
	      headers[note][0] + "\n");
    }

    discard_message(headers[note][1]);
    headers = exclude_array(headers, note, note);
    msg_num--;

    if ((note == msg_num) &&
	(!no_report))
    {
	BOARD_CENTRAL->new_note(board_name, note > 0 ? headers[note-1][1] : 0,
	    MASTER_OB(environment()));
    }

    write("Ju�.\n");
    return 1;
}

/*
 * Function name: rename_msg
 * Description  : With this command, the wizard who wrote a note on his
 *                'own' board can rename the note to another players name.
 * Arguments    : string str - the command line argument.
 * Returns      : int 1/0 - success/failure.
 */
public nomask int
rename_msg(string str)
{
    string name;
    string lower;
    string rank;
    string note;
    string old_author;
    string renamer;
    int    num;
    int    index = -1;
    int    len;

    if (no_special_fellow())
    {
        notify_fail("Nie masz uprawnie� do tego rodzaju operacji na tej "
                  + "tablicy.\n");
	return 0;
    }
    
    if (!str || sscanf(str, "note %d %s", num, name) != 2 &&
	        sscanf(str, "%d %s", num, name) != 2)
    {
        notify_fail("Pod kogo chcesz si� podszy� i w kt�rej notce?\n"
                  + "Prawid�owa sk�adnia to: rename <numer notki> "
                  + "<nowy autor>" + (show_lvl ? " [<tytu� autora>]" : "")
                  + "\n");
	return 0;
    }

    if ((num < 1) ||
	(num > msg_num))
    {
	notify_fail("There are only " + msg_num + " notes.\n");
	return 0;
    }

    renamer = this_player()->query_real_name();
    old_author = query_author(num);

    if (renamer != old_author &&
        (SECURITY->query_wiz_rank(renamer) < WIZ_ARCH ||
         SECURITY->exist_player(old_author)))
    {
	write("You are not the author of that note.\n");
	return 1;
    }

    if (show_lvl)
    {
        string *words = explode(name, " ");

        name = capitalize(words[0]);
        rank = capitalize(implode(words[1..], " "));
    }
    else
    {
        name = capitalize(name);
        rank = "";
    }

    len = strlen(lower = lower_case(name));
    if (len < MIN_NAME_LENGTH)
    {
        write("Imi� '" + name + "' jest za kr�tkie.\n");
        return 1;
    }
    if (len > MAX_NAME_LENGTH)
    {
        write("Imi� '" + name + "' jest za d�ugie.\n");
        return 1;
    }
    if (SECURITY->exist_player(lower))
    {
        write("Nie mo�esz podszywa� si� pod istniej�cego gracza.\n");
        return 1;
    }

    while(++index < len)
	if (!wildmatch("*" + lower[index..index] + "*", LEGAL_CHARS))
	{
            write("Imi� '" + name + "' zawiera nielegalne znaki. Dozwolone "
                + "to du�e i ma�e litery, apostrof i ��cznik.\n");
	    return 1;
	}

    if (show_lvl && (len = strlen(rank)))
    {
        if (len < MIN_RANK_LENGTH)
        {
            write("Tytu� '" + rank + "' jest za kr�tki.\n");
            return 1;
        }
        if (len > MAX_RANK_LENGTH)
        {
            write("Tytu� '" + rank + "' jest za d�ugi.\n");
            return 1;
        }
    }

    seteuid(getuid());

    num--;
    old_author = implode(explode(headers[num][0][RANK_BEGIN..AUTHOR_END],
                                 " ") - ({""}), " ");
    headers[num][0] = sprintf(RENAME_HEADER_FORMAT,
        headers[num][0][..(RANK_BEGIN - 1)], rank, name,
        headers[num][0][(AUTHOR_END + 1)..]);
    note = headers[num][0] + "\n" +
	read_file(board_name + "/" + headers[num][1], 2);
    rm(board_name + "/" + headers[num][1]);
    write_file(board_name + "/" + headers[num][1], note);

    write("Autorem " + LANG_SORD(num + 1, PL_DOP, PL_ZENSKI)
        + " notki nie jest juz " + old_author + ", lecz " + (strlen(rank) ?
          rank + " " + name : name) + ".\n");
    return 1;
}

/*
 * Function name: store_msg
 * Description  : Store a message to disk.
 * Arguments    : string str - the message to store and the filename.
 * Returns      : int 1/0 - success/failure.
 */
nomask public int
store_msg(string str)
{
    int    note;
    string file;

    if (SECURITY->query_wiz_rank(this_player()->query_real_name()) <
	WIZ_NORMAL)
    {
	notify_fail("Niestety, nie mo�esz nagrywa� notek.\n");
	return 0;
    }

    if (!stringp(str))
    {
	notify_fail("Kt�r� notke chcesz nagra�?\n");
	return 0;
    }

    if ((sscanf(str, "notke %d %s", note, file) != 2) &&
	(sscanf(str, "%d %s", note, file) != 2))
    {
	notify_fail("Gdzie chcesz nagra� ktor� notk�?\n");
	return 0;
    }

    if ((note < 1) ||
	(note > msg_num))
    {
	write("Ta notka nie istnieje.\n");
	return 1;
    }

    if (check_reader(note))
    {
	write("Nie masz uprawnie� �eby m�c nagra� t� notk�.\n");
	return 1;
    }

    note--;
    str = (headers[note][0] + "\n\n" +
	   read_file((board_name + "/" + headers[note][1]), 2));
    EDITOR_SECURITY->board_write(file, str);

    /* We always return 1 because we don't want other commands to be
     * evaluated.
     */
    return 1;
}

/*
 * Function name: query_headers
 * Description  : Return the headers if this_player() is allowed to see
 *                them.
 * Returns      : mixed - the headers in the following format:
 *                        ({ ({ string header, string filename }) })
 *                        or just ({ }) if no read access.
 */
public nomask mixed
query_headers()
{
    return (check_reader() ? ({ }) : secure_var(headers) );
}

public nomask void
post_msg(string date, int id, string file, string who, string msg)
{
  int size = sizeof(explode(msg, "\n"));
  string head = sprintf(HEADER_FORMAT, file, LOG_NAMES(id), who,
      (size > 999 ? "***" : sprintf("%3d", size)),
      date);
  post_note(head, msg);
}
