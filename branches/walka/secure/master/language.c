/*
 * /secure/master/language.c
 *
 * Module to /secure/master.c
 * Handles all default language stuff.
 *
 * These are used by the efun parse_command().
 */

/*
 * Nazwa funkcji: parse_command_id_list
 * Opis         : This will return the words that can be used to point at
 *                one particular object, i.e. a singular 'id'.
 * Zwraca       : string * - the list.
 */
string *
parse_command_id_list()
{
    return ({ "rzecz" });
}

/*
 * Nazwa funkcji: parse_command_plural_id_list
 * Opis         : This will return the words that can be used to point at
 *                a particular group of objects, i.e. a plural 'id'.
 * Zwraca       : string * - the list.
 */
string *
parse_command_plural_id_list()
{
    return ({ "rzeczy" });
}

int *
parse_command_rodz_list()
{
    return ({ PL_ZENSKI });
}

int *
parse_command_prodz_list()
{
     return ({ PL_ZENSKI });
}
