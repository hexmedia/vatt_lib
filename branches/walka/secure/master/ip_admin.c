/**
 * Administrator adres�w ip.
 * Autor: Krun 
 * Data: 20.04.2007
 *
 * TODO:
 * - dopisa� obs�uge imion, kt�re mog� mie� multiloga ze sob�
 * - dopisa� obs�ug� imion, kt�re mog� mie� multiloga ze wszystkimi... ASTER rzomdzi
 */

#include "/sys/ip_admin.h"
#include "/sys/composite.h"

mapping ip_list = ([]);
    // lista adres�w ip ([ adres : ({dlaczego dozwolone multi, osoba zezwalajaca, czas}) ])
mixed *names_list;
    // lista, list imion kt�re mog� mie� ze sob� multiloga np. ({ ({"krun", "krunjr"}) })

#define CHECK_SO_CAN_ADD      if(SECURITY->query_wiz_rank(TP->query_real_name()) < IP_RANK_DODAWANIE)  {return 0;}
#define CHECK_SO_CAN_REMOVE   if(SECURITY->query_wiz_rank(TP->query_real_name()) < IP_RANK_USUWANIE)   {return 0;}
#define CHECK_SO_CAN_SEE      if(SECURITY->query_wiz_rank(TP->query_real_name()) < IP_RANK_ZOBACZENIE) {return 0;}

#define DLACZEGO 0
#define CZAS     1
#define OSOBA    2

nomask int
add_ip_to_list(string ip, string powod)
{
    CHECK_SO_CAN_ADD;

    mixed tmp;

    if(sizeof(tmp = explode(ip, ".")) != 4)
        return 0;
    if(!atoi(tmp[0]) || !atoi(tmp[1]) || !atoi(tmp[2]) || !atoi(tmp[3]))
        return 0;

    if(!powod)
        return 0;
    if(strlen(powod) < IP_MIN_DLUGOSC_POWODU)
        return 0;

    if(ip_list[ip])
        return -1;

    ip_list[ip] = ({powod, TP->query_real_name(), time()});

    save_master();

    return 1;
}

nomask int
remove_ip_from_list(string ip)
{
    CHECK_SO_CAN_REMOVE;

    mixed tmp;

    if(sizeof(tmp = explode(ip, ".")) != 4)
        return 0;
    if(!atoi(tmp[0]) || !atoi(tmp[1]) || !atoi(tmp[2]) || !atoi(tmp[3]))
        return 0;
    
    if(!ip_list[ip])
        return -1;

    ip_list = m_delete(ip_list, ip);
   
    save_master();

    return 1;
}

nomask mapping
query_ip_list()
{
    CHECK_SO_CAN_SEE;

    return ip_list;
}

nomask public mixed
query_ip_from_list(string ip)
{
    CHECK_SO_CAN_SEE;

    mixed tmp;

    if(sizeof(tmp = explode(ip, ".")) != 4)
        return 0;
    if(!atoi(tmp[0]) || !atoi(tmp[1]) || !atoi(tmp[2]) || !atoi(tmp[3]))
        return 0;
    
    if(!ip_list[ip])
        return -1;

    return ip_list[ip];
}

mixed
query_multi_log()
{
    mapping all_ips = ([]);
    mapping all_mails = ([]);

    foreach(mixed u : users())
    {
        string name = u->query_real_name();
        string ip_number = query_ip_number(u);
        string email = u->query_mailaddr();
        
        if(lower_case(name) == "podanie" || lower_case(name) == "logon")
            continue;
        
        if(lower_case(email) != "brak")
            if(is_mapping_index(email, all_mails))
                all_mails[email] += ({name});
            else
                all_mails[email] = ({name});
	
	if(is_mapping_index(ip_number, ip_list))
            continue;
        
	if(is_mapping_index(ip_number, all_ips))
            all_ips[ip_number] += ({name});
        else
            all_ips[ip_number] = ({name});
    }
     
    mixed *ret_ips = ({});
    mixed *ret_mails = ({});

    foreach(mixed i : m_indexes(all_ips))
    {
        if(sizeof(all_ips[i]) < 2)
            continue;
        ret_ips += ({ ({i, all_ips[i]}) });
    }  
    foreach(string e : m_indexes(all_mails)) 
    {
        if(sizeof(all_mails[e]) < 2)
            continue;
        ret_mails += ({ ({e, all_mails[e]}) });
    }
    
    return ({ret_ips, ret_mails}); 
}

public int
log_multi(string str, string *players)
{
    SECURITY->log_public("MULTILOGI",  ctime(time()) + ": " + str + ": " + COMPOSITE_WORDS(players) + "\n ");
}

