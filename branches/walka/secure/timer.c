#pragma no_clone
#pragma save_binary
#pragma strict_types

inherit "/std/object";

#include <cmdparse.h>
#include <composite.h>
#include <files.h>
#include <language.h>
#include <macros.h>
#include <stdproperties.h>
#include <const.h>

/* Jakby cos sie sypalo */
#define SAVE_TIME 1

/* Obiekt do ktorego zapisywany jest stan zegara */
#define SAVE_CZAS ("/syslog/czas")

                                                                               
/* Co jaki czas (w sekundach) bedzie zapisywany stan zegara */
#define TIME_TO_SAVE    300.0
/* Co ile sekund rzeczywistych mija godzina mudowa */
#define MHOUR_TO_RSEC   300.0
/* Definicje dlugosci poszczegolnych odcinkow czasu */
#define MIESIECY_W_ROKU 10
#define DNI_W_ROKU 365
#define DNI_W_MIESIACU ({0, 40, 40, 40, 40, 40, 40, 40, 40, 40, 5})
#define GODZIN_NA_DZIEN 24

/* Ile obiektow powiadamiamy na raz (zeby nie sfloodowac calego muda) */
#define POWIADOM_ILE 30

private int godzina = 0;
private int dzien = 1;
private int dzien_roku = 1;
private int miesiac = 1;
private int rok = 1;

void start_alarm();                                                            
void time_tick();
void zapisz_zegar();
void powiadom_mnie(string funkcja);
void powiadamiaj(string *obiekty, int godzina);
mapping query_powiadamiani();

int query_godzina();
int query_dzien();
int query_dzien_roku();
int query_miesiac();
int query_rok();
string query_data();

string dlugi_opis();

/*
 * mapping z obiektami, ktore maja byc powiadamiane o nadejsciu kazdej
 * godziny, mapping ma postac:
 * mapping powiadamiani ([
 *                        "string_okreslajacy_obiekt" : "funkcja_jaka_mamy_wykonac"                                                    
 *                      ])
 * gdzie "string_okreslajacy_obiekt" jest stringiem wydobytym z file_path(object ob)
 * a "funkcja_jaka_mamy_wykonac" jest funkcja, jaka zostanie wykonana na danym
 * obiekcie po nadejsciu kazdej godziny, funkcja ta musi byz zadeklarowana tak:
 *
 * void funkcja_jaka_mamy_wykonac(object obiekt_na_ktorym_to_wykonujemy,
 *                                int obecna_nowa_godzina);
 */
static mapping powiadamiani = ([]);

string
dlugi_opis()
{
    return "Spogladasz w otchlan czasu i oto co dostrzegasz:\n" +
        "\tGodzina:\t\t" + godzina + ",\n" +
        "\tDzien Miesiaca:\t\t" + dzien + ",\n" +
        "\tDzien Roku:\t\t" + dzien_roku + ",\n" +
        "\tMiesiac:\t\t" + miesiac + ",\n" +
        "\tRok:\t\t\t" + rok + ".\n";
}                                                                              

int
long_time()
{
    return godzina + (GODZIN_NA_DZIEN * dzien) +
        (GODZIN_NA_DZIEN * DNI_W_MIESIACU[miesiac] * miesiac) +
        (GODZIN_NA_DZIEN * DNI_W_MIESIACU[miesiac] * MIESIECY_W_ROKU * rok);
}

public void
create_object()
{

    ustaw_nazwe( ({ "kalendarz", "kalendarza", "kalendarzowi", "kalendarz", "kalendarzem",
                "kalendarzu" }), ({ "kalendarze", "kalendarzom", "kalendarzami",
                "kalendarze", "kalendarzami", "kalendarzach" }), PL_MESKI_NZYW);

    set_long("@@dlugi_opis");
    setuid();
    seteuid(getuid());
    restore_object(SAVE_CZAS);

    start_alarm();
}

/*
 * Funkcja:     start_alarm
 * Opis:        Funkcja startuje "tick" zegara oraz alarm
 *              automatycznego zapisu stanu zegara
 */
void
start_alarm()
{
    if (SAVE_TIME)
        set_alarm(TIME_TO_SAVE, TIME_TO_SAVE, "zapisz_zegar");
    set_alarm(MHOUR_TO_RSEC, MHOUR_TO_RSEC, "time_tick");
}

/*                                                                             
 * Funkcja:     zapisz_zegar
 * Opis:        Funkcja zapisuje stan zegara
 */
void
zapisz_zegar()
{
    if (SAVE_TIME)
        save_object(SAVE_CZAS);
}

/*
 * Funkcja:     time_tick
 * Opis:        Glowna funkcja zegara, ktora nim wlasciwie jest
 *              funkcja ta wywolywana jest co godzine mudowa
 *              jednoczesnie ja zmieniajac
 *              Ponadto funkcja powiadamia o nastepnej godzinie
 *              wszystkie obiekty, ktore sobie tego zyczyly.
 *              Jednakze obiekty powiadamiane sa grupami po kilka
 *              sztuk, zeby nie sfloodowac calego muda.
 */                                                                            
void
time_tick()
{
    int size, i;
    float czas_powiadamiania = 0.0;
    string *indeksy, *powiadom;
    object powiadamiany;

    godzina++;
    if(godzina >= GODZIN_NA_DZIEN) //Od 0 liczymy... godz ostatnia == pierwsza
    {
        godzina = 0;
        dzien++;
        dzien_roku++;
    }
    if(dzien > DNI_W_MIESIACU[miesiac]) //Od 1 liczymy...
    {
        dzien = 1;
        miesiac++;
    }                                                                          
    if(miesiac > MIESIECY_W_ROKU) //Od 1 liczymy...
    {
        miesiac = 1;
        dzien_roku = 1;
        rok++;
    }

    indeksy = m_indexes(powiadamiani);
    size = sizeof(indeksy);
    while(size > 0)
    {
        set_alarm(czas_powiadamiania, 0.0, &powiadamiaj(
            indeksy[( (size >= POWIADOM_ILE) ? (size - POWIADOM_ILE) : 0)..(size - 1)],
            godzina) );
        size -= POWIADOM_ILE;
        czas_powiadamiania += 1.0;
    }
}

/*
 * Funkcja:     powiadamiaj
 * Opis:        Funkcja powiadamia obiekty z tablicy o mijajacej
 *              godzinie, jesli obiekt juz nie istnieje to zostaje
 *              usuniety z listy obiektow powiadamianych
 * Argumenty:   *obiekty - tablica ze wskaznikami (stringowymi)
 *                      do powiadamianych obiektow
 *              godzina - nowa godzina
 */
void
powiadamiaj(string *obiekty, int godzina)
{
    int i, size;
    object powiadamiany;

    size = sizeof(obiekty);

    for (i = 0; i < size; i++)
    {
        powiadamiany = find_object(obiekty[i]);
        if (objectp(powiadamiany) && stringp(powiadamiani[obiekty[i]]))
            call_otherv(powiadamiany, powiadamiani[obiekty[i]],
                ({ powiadamiany, godzina }));
        else
            powiadamiani = m_delete(powiadamiani, obiekty[i]);
        powiadamiany = 0;
    }
}

/*
 * Funkcja:     query_godzina
 * Opis:        Funkcja zwraca aktualna godzine
 * Zwraca:      int - aktualna godzine
 */
int
query_godzina()
{
    return godzina;
}

/*
 * Funkcja:     query_dzien
 * Opis:        Funkcja zwraca aktualny dzien miesiaca
 * Zwraca:      int - aktualny dzien miesiaca
 */
int
query_dzien()
{
    return dzien;
}

/*
 * Funkcja:     query_dzien_roku
 * Opis:        Funkcja zwraca aktualny dzien roku
 * Zwraca:      int - aktualny dzien roku
 */
int
query_dzien_roku()
{
    return dzien_roku;
}

/*
 * Funkcja:     query_miesiac
 * Opis:        Funkcja zwraca aktualny miesiac
 * Zwraca:      int - aktualny miesiac
 */
int
query_miesiac()
{
    return miesiac;
}

/*
 * Funkcja:     query_dlugosc_miesiaca
 * Opis:        Funkcja zwraca dlugosc w dniach aktualnego miesiaca
 * Argumenty:   int miesiac, 1..MIESIECY_W_ROKU, badz bez, jesli aktualny.
 * Zwraca:      int - dlugosc
 */
int
query_dlugosc_miesiaca(int m = miesiac)
{
    return DNI_W_MIESIACU[m];
}

int
query_dni_w_roku()
{
    return DNI_W_ROKU;
}
/*
 * Funkcja:     query_rok
 * Opis:        Funkcja zwraca aktualny rok
 * Zwraca:      int - aktualny rok
 */
int
query_rok()
{
    return rok;
}

                                                                                           
void
set_rok(int r)
{
    rok = r;
}
void
set_dzien(int d)
{
    dzien = d;
    dzien_roku += d;
}
void
set_dzien_roku(int dr)
{
    dzien_roku = dr;
}
void
set_miesiac(int m)
{
    int a;
    miesiac = m;
    dzien_roku = 0;
    for(a = 0; a < m; a++)
        dzien_roku += DNI_W_MIESIACU[a];
}

float
mhour_to_rsec()
{
    return MHOUR_TO_RSEC;
}
/*
 * Funkcja:     remove_object
 * Opis:        Funkcja wywolywana podczas niszczenia obiektu
 *              bedzie sie starala zapisac wartosc zegara
 */
void
remove_object()
{
    if (SAVE_TIME)
        save_object(SAVE_CZAS);
    ::remove_object();
}

/*
 * Funkcja:     powiadom_mnie
 * Opis:        Funkcja ktora wywolana przez jakikolwiek obiekt
 *              spowoduje dopisanie go do listy obiektow
 *              powiadamianej o kazdej nowej godzinie
 */
void
powiadom_mnie(string funkcja)
{
  if (!mappingp(powiadamiani))
    powiadamiani = ([ ]);

  powiadamiani[file_name(previous_object())] = funkcja;
}

/*
 * Funkcja:     query_powiadamiani
 * Opis:        Funkcja zwraca liste powiadamianych obiektow,
 *              wraz z funkcjami do ktorych sie ma zegar odwolywac
 * Zwraca:      mapping - mapping powiadamianych obiektow
 */
mapping
query_powiadamiani()
{
  return powiadamiani;
}

/*
 * Funkcja:     query_data
 * Opis:        Zwraca date mudowa
 * Zwraca:      string - data w formacie:
 *              Rok Dzien_Roku, Miesiac Dzien, Godzina
 */
string
query_data()
{
    string rtrn;

    rtrn = sprintf("%4d %3d, %2d %2d, %2d", rok, dzien_roku, miesiac, dzien, godzina);
    return rtrn;
}
