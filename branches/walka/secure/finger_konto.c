#pragma no_inherit
#pragma no_shadow
#pragma strict_types

#include <config.h>

#include <formulas.h>
#include <living_desc.h>
#include <login.h>
#include <macros.h>
#include <ss_types.h>
//#include <state_desc.h>
#include <std.h>
#include <debug.h>

private string	*postacie,
		*postacie_krzyzyk,
		password,
		name;

private int	password_time;

public void
create()
{
    seteuid(0);
}

public void
finger_info()
{
}

public int
load_konto(string konto)
{
    int ret;
   
    if (!konto || wildmatch("* *", konto))
        return 0;

    seteuid(getuid());
    ret = restore_object(KONTO_FILE(konto));
    seteuid(0);

    return ret;
}

public void
master_set_name(string n)
{
    if (file_name(previous_object()) == SECURITY)
        name = n;
}

public void
open_konto()
{
    if (file_name(previous_object()) == SECURITY)
        seteuid(0);
}

/*
 * Nazwa funkcji: query_finger_player
 * Opis         : This function identifies this object as a finger-player
 *                object.
 * Zwraca       : int 1 - always.
 */
public int
query_finger_konto()
{
    return 1;
}

public string *
query_postacie()
{
    return postacie ? secure_var(postacie) : ({});
}

public string *
query_postacie_krzyzyk()
{
    return postacie_krzyzyk ? secure_var(postacie_krzyzyk) : ({});
}

public string
query_name()
{
    return name;
}

public int query_password_time() { return password_time; }

/*
 * Nazwa funkcji: query_password
 * Opis         : Return the password of the player. Only SECURITY may do
 *                this.
 * Zwraca       : string - the password, else 0.
 */
public string
query_password()
{
    return file_name(previous_object()) == SECURITY ? password : "[haslo]";
}

/*
 * Nazwa funkcji: match_password
 * Opis         : Match the password of a player with an arbitrary string
 *                that is claimed to be the password of a player. NOTE that
 *                if the player has NO password, everything matches.
 * Argumenty    : string p - the password to match.
 * Zwraca       : int - true/false.
 */
nomask int
match_password(string p)
{
    return !password || password == crypt(p, password);
}

public void 
remove_object()
{
    destruct();
}

