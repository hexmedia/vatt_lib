#pragma strict_types

inherit "/std/object";

#include <config.h>
#include <files.h>

#define TIME_OUT_TIME 300
#define KONTO_INFO "/d/Standard/doc/login/KONTO_INFO"

static mixed    *sect_array;	/* The array holding section information */
static string   *text_array;    /* Array holding all sections text */
static string	cur_sect,       /* Current section to display */
                cur_menu;       /* Current menu */
		

static void write_info();
static void do_menu(string s_num);
string fix_menu(string str);
void enter_game();
void nagraj_konto();
void lista_postaci();
void nowa_postac();
void haslo_konta();
//void haslo_postaci();
void zasady();

private string password;	// Haslo konta
private int password_time;	// Czas ostatniej zmiany hasla
private string name;		// Nazwa konta
private string *postacie;
private string *postacie_krzyzyk;

static int time_out_alarm;
static string old_password;
static int password_set = 0;
static int login_flag;


/*
 * Nazwa funkcji: new_password
 * Opis         : This function is used to let a new character set his
 *                password.
 * Argumenty    : string p - the intended password.
 */
static void
new_password(string p)
{
    write_socket("\n");
//    remove_alarm(time_out_alarm);

    if (p == "zakoncz")
    {
        write_socket("Trudno sie mowi - do nastepnego razu.\n");
	remove_object();
	return;
    }

//    time_out_alarm = set_alarm(TIMEOUT_TIME, 0.0, time_out);

    if (!password)
    {
	if (strlen(p) < 6)
	{
	    write_socket("Haslo musi miec przynajmniej 6 znakow.\n");
	    input_to(new_password, 1);
	    write_socket("Haslo: ");
	    return;
	}
	
	if (!(SECURITY->proper_password(p)))
	{
	    write_socket("Podane haslo nie spelnia ustalonych przez nas " +
	        "podstawowych kryteriow\nbezpieczenstwa.\n");
	    input_to(new_password, 1);
	    write_socket("Haslo: ");
	    return;
	}
	
	if (strlen(old_password) &&
	    (crypt(p, old_password) == old_password))
	{
	    write_socket("Haslo musi sie roznic od poprzedniego.\n");
	    write_socket("Haslo: ");
	    input_to(new_password, 1);
	    return;
	}
	
	password = p;
	input_to(new_password, 1);
	write_socket("Dobrze. Ponownie wpisz to samo haslo, zeby je " +
	    "zweryfikowac.\n");
	write_socket("Haslo (to samo): ");
	return;
    }

    if (password != p)
    {
	password = 0;
	write_socket("Hasla sie nie zgadzaja. Wiecej konsekwencji "+
	    "nastepnym razem!\n");
	input_to(new_password, 1);
	write_socket("Haslo (nowe haslo, pierwsze podejscie): ");
	return;
    }

    /* Crypt the password. Use a new seed. */
    password = crypt(password, "$1$");
    nagraj_konto();

    enter_game();
/*    if (password_set)
    {
	enter_game();
    }
    else
    {
	start_player1();
	enter_game();
    }*/
}

/*
 * Nazwa funkcji: tell_password
 * Opis         : This function tells the player what we expect from his
 *                new password and then prompt him for it.
 */
static void
tell_password()
{
    write_socket(
"Aby ustrzec twe haslo przed zlamaniem, uwazamy ze powinno ono spelniac\n" +
" podstawowe kryteria:\n" +
" - musi miec przynajmniej 6 znakow;\n" +
" - musi miec przynajmniej 1 znak nie bedacy litera;\n" +
" - musi zaczynac sie i konczyc litera.\n\n" +

"Nowe haslo: ");
    
    input_to(new_password, 1);
}

/*
 * Nazwa funkcji: check_password
 * Opis         : If an existing player tries to login, this function checks
 *                for the password. If you fail, you are a granted a second
 *                try.
 * Argumenty    : string p - the intended password.
 */
static void
check_password(string p)
{
    int wiz;
    
    write_socket("\n");

//    remove_alarm(time_out_alarm);
//    time_out_alarm = set_alarm(TIMEOUT_TIME, 0.0, time_out);

    /* Player has no password, force him/her to set a new one. */
    if (password == 0)
    {
        write_socket("Nie masz zadnego hasla!\nMusisz ustawic nowe " +
           "zanim dopuscimy cie dalej.\n\n");
	password_set = 1;
	old_password = password;
	password = 0;
	tell_password();
	return;
    }

    /* Password matches. Go! */
    if (crypt(p, password) == password)
    {
#ifdef FORCE_PASSWORD_CHANGE
/*	wiz = SECURITY->query_wiz_rank(name);
	    
	if ((password_time + (wiz ? FOUR_MONTHS : SIX_MONTHS)) < time())
        {
	    write_socket("Minel" + (wiz ? "y ponad 4 miesiace"
	        : "o ponad 6 miesiecy") + " od momentu ostatniej " +
		"zmiany twego hasla.\nW zwiazku z tym musisz je zmienic, " +
		"zanim dopuscimy cie dalej.\n\n");
	    password_set = 1;
	    old_password = password;
	    password = 0;
	    tell_password();
	    return;
        }*/
#endif FORCE_PASSWORD_CHANGE

	SECURITY->notify(this_object(), 0);
	enter_game();
	return;
    }

    write_socket("Niewlasciwe haslo!\n");

    /* Player already had a second chance. Kick him/her out. */
    if (login_flag)
    {
	remove_object();
	return;
    }

    login_flag = 1;
    write_socket("Haslo (druga i ostatnia proba): ");
    input_to(check_password, 1);
}

void
start_player(string str)
{
    mapping map;

    set_auth(this_object(), "root:root");
    map = restore_map("/players/konta/" + extract(str, 0, 0) + "/" + str);
    
    name = map["name"];
    password = map["password"];
    password_time = map["password_time"];
    postacie = map["postacie"];
    postacie_krzyzyk = map["postacie_krzyzyk"];
    write_socket("Haslo: ");
    input_to(check_password, 1);
}

void
enter_game()
{
    string data, *split, *split2, str, fnam;
    int il, il2;

    data = read_file(KONTO_INFO);
    if (!data)
	remove_object();

    split = explode(data + "##", "##");
/* discard first empty element */
    split = split[1..sizeof(split)];

    sect_array = ({});
    text_array = ({});

    for (il = 0; il < sizeof(split); il += 2)
    {
	sect_array += ({ split[il] });
	split2 = explode(split[il + 1], "#include");
	if (sizeof(split2) < 2)
	    text_array += ({ split[il + 1] });
	else
	{
	   for (il2 = 1; il2 < sizeof(split2); il2++)
	   {
	       if (sscanf(split2[il2],"\"%s\"%s", fnam, str) == 2)
		   split2[il2] = read_file(fnam) + str;
	   }
	   text_array += ({ implode(split2, " ") });
        }
    }

    enable_commands();
//    write_socket("\nWpisz 'zakoncz' aby wyjsc z konta.\n\n");
    cur_sect = sect_array[0];
    cur_menu = fix_menu(cur_sect);
    write_info();
    input_to(do_menu);
}

static void
do_menu(string s_num)
{
    int max, num;

    if ((s_num == "zakoncz") || (s_num == "0"))
    {
	nagraj_konto();
	remove_object();
	return;
    }

    num = member_array(s_num, sect_array);
    if (num >= 0)
	cur_sect = s_num;

    switch(num)
    {
	case 1: lista_postaci(); return;
	case 2: nowa_postac(); return;
	case 3: haslo_konta(); return;
	case 4: zasady(); return;
    }

    write_info();
    input_to(do_menu);
}

void
time_out()
{
    write_socket("Twoj czas uplynal.\n");
    nagraj_konto();
    remove_object();
}

void
write_info()
{
/*    mixed *calls = get_all_alarms();
    int pos;

    for (pos=0 ; pos<sizeof(calls) ; pos++)
	if (calls[pos][1] == "time_out")
	    remove_alarm(calls[pos][0]);
    set_alarm(600.0, 0.0, time_out);*/

    write_socket("\n\n\t   VATT'GHERN - konto " + name + "\n\n" +
	"\t[1] - Lista stworzonych postaci.\n" +
	"\t[2] - Stworzenie nowej postaci.\n" +
	"\t[3] - Zmiana hasla konta.\n" +
	"\t[4] - Zasady.\n\n" +
	"\t[0] - Wyjscie.\n\n" +
	"Wybierz jedna z opcji: ");

}

static string
fix_menu(string str)
{
    int pos, il, clen;

    return "Wybierz opcje: ";
}

string
query_real_name()
{
    return "<" + name + ">";
}

void
nagraj_konto()
{
    mapping map;

    set_auth(this_object(), "root:root");
    map = (["name": name,
	"password": password,
	"password_time": password_time,
	"postacie": postacie,
    "postacie_krzyzyk": postacie_krzyzyk]);
    save_map(map, "/players/konta/" + extract(name, 0, 0) + "/" + name);
}

void
wcisnij_enter(string str)
{
    write_info();
    input_to(do_menu);
}

void
lista_postaci()
{
    int i;
    string str;

    if (i = sizeof(postacie))
    {
	str = "\nPosiadasz nastepujace postacie: ";
	while (--i > -1)
	    str += capitalize(postacie[i]) + (i > 1 ? ", " :
		(i == 1 ? " i " : ".\n\n"));
    }
    else
    str = "\nNie masz na razie zadnych postaci.\n\n";


    if (i = sizeof(postacie_krzyzyk))
    {
    str = "\nNiegdys posrod zywych: ";
    while (--i > -1)
        str += capitalize(postacie_krzyzyk[i]) + (i > 1 ? ", " :
        (i == 1 ? " i " : ".\n\n"));
    }

    write_socket(str);
    write_socket("[Wcisnij ENTER, aby przejsc do glownego menu]");
    input_to(wcisnij_enter);
}

void
nowa_postac()
{
    object nowa;
    write_socket("\nNowa postac...\n\n");
    nowa = clone_object("/secure/login");
    exec(nowa, this_object());
    nowa->stworz_postac(name);
//    postacie += ({ nowa->query_pl_name() });
    nagraj_konto();
    remove_object();
//    write_socket("[Wcisnij ENTER, aby przejsc do glownego menu]");
//    input_to(wcisnij_enter);
}

void
haslo_konta()
{
    write_socket("\nHaslo konta...\n\n");
    old_password = password;
    password = 0;
    tell_password();
//    write_socket("[Wcisnij ENTER, aby przejsc do glownego menu]");
//    input_to(wcisnij_enter);
}

/*void
haslo_postaci()
{
}*/

void
zasady()
{
    
    write_socket(read_file("/doc/help/general/zasady"));
    write_socket("\n[Wcisnij ENTER, aby przejsc do glownego menu]");
    input_to(wcisnij_enter);
}

/*void
dodaj_postac(string str)
{
    postacie += ({ str });
}*/

public int
remove_object()
{
    destruct();
    return 1;
}
