/* Brzytwa do golenia brody i w�s�w.
 *               Lil.
 */
inherit "/std/holdable_object";

#include <object_types.h>
#include <materialy.h>
#include <macros.h>
#include <stdproperties.h>
#include <pl.h>

#define BRZYTWA_ROZLOZONA "_brzytwa_rozlozona"

int skladanie(string str);
int rozkladanie(string str);
int strzyzenie(string str);


create_holdable_object()
{
    ustaw_nazwe("brzytwa");
    dodaj_przym("niewielki","niewielcy");
    dodaj_przym("rozk�adany","rozk�adani");
    set_long("To narz�dzie do strzy�enia sk�ada si� z bardzo ostrego "+
             "ostrza z nierdzewnej stali osadzonego przegubowo "+
	     "w drewnianej prostej r�czce. Brak tu jakichkolwiek "+
	     "zdobie�. Jest to przedmiot typowo praktyczny i "+
	     "zdawa�oby si�, niezb�dnik ka�dego dojrza�ego m�czyzny.\n");
     set_hit(2);
     set_pen(1);
     set_wt(W_KNIFE);
     set_dt(W_SLASH);
     set_hands(W_ANYH);
     set_likely_dull(MAXINT);
     set_likely_break(MAXINT);
     set_weapon_hits(9991999);
     ustaw_material(MATERIALY_DR_BUK, 66);
     ustaw_material(MATERIALY_STAL, 34);
     add_prop(OBJ_I_WEIGHT, 299);
     add_prop(OBJ_I_VOLUME, 340);
     add_prop(OBJ_I_VALUE, 90);
     
    /*Brzytwa - narz�dzie u�ywane kiedy� (obecnie sporadycznie) do golenia, w kszta�cie bardzo ostrego no�a z nierdzewnej stali wysokow�glowej, osadzonego przegubowo w drewnianej lub plastikowej r�czce. Do jej ostrzenia wykorzystywano pasy z twardej sk�ry. Zanim zosta�y wyparte przez �yletki i golarki, brzytwy by�y stosowane przez m�czyzn do usuwania zarostu oraz przez kobiety do usuwania nadmiernego ow�osienia (zwykle z n�g i spod pach). Brzytwy mo�na spotka� jeszcze w niekt�rych zak�adach fryzjerskich.
Staro�ytny "przodek" brzytwy (po lewej u g�ry)
Powi�ksz
Staro�ytny "przodek" brzytwy (po lewej u g�ry)

Po raz pierwszy zaokr�glone narz�dzia przypominaj�ce brzytwy, wykonane z br�zu, by�y u�ywane w epoce br�zu.*/
}

void
init()
{
    ::init();
    add_action(strzyzenie, "ostrzy�");
    add_action(strzyzenie, "zg�l");
    add_action(strzyzenie, "og�l");
    add_action(skladanie, "z��");
    add_action(rozkladanie, "roz��");
    add_action("pomoc", "?", 2);
}

int
strzyzenie(string str)
{
    notify_fail(capitalize(query_verb())+" co ?\n");

   if(!str)
     return 0;

   if(!this_object()->query_wielded())
   {
   		notify_fail("Musisz wpierw chwyci� brzytw�, aby to zrobi�.\n");
   		return 0;
   }
   if(!this_object()->query_prop(BRZYTWA_ROZLOZONA))
     {
      write("Jak chcesz si� ogoli� z�o�on� brzytw�?\n");
      return 1;
     }

   if(TP->query_gender()==1)
     {
       write("Hmm..ciekawe co mog�aby� sobie ostrzyc?\n");
       return 1; 
     }

   sscanf("%s",str);
   
   if(str~="brod�")
   {
      if(TP->query_dlugosc_brody() < 1.0)
      {
          write("Przecie� nie masz brody.\n");
          return 1;
      }

      write("Wprawnymi ruchami rozpoczynasz strzy�enie sobie brody "+
            this_object()->query_short(PL_NAR)+".\n");
      saybb(QCIMIE(this_player(), PL_MIA)+" wprawnymi ruchami rozpoczyna strzyc "+
            "sobie brod� "+this_object()->query_short(PL_NAR)+".\n");
      clone_object("/std/brzytwa_paralize/broda")->move(this_player());
      return 1;
   }   
   
   if(str~="w�sy")
   {
      if(TP->query_dlugosc_wasow()*10.0 < 1.0)
      {
          write("Przecie� nie masz w�s�w.\n");
          return 1;
      }
      write("Wprawnymi ruchami rozpoczynasz strzy�enie sobie w�s�w "+
            this_object()->query_short(PL_NAR)+".\n");
      saybb(QCIMIE(this_player(), PL_MIA)+" wprawnymi ruchami rozpoczyna strzyc "+
            "sobie w�sy "+this_object()->query_short(PL_NAR)+".\n");
      clone_object("/std/brzytwa_paralize/wasy")->move(this_player());
      return 1;
   }
   if(str~="si�" || str~="brod� i w�sy" || str~="w�sy i brod�" || str=="zarost")  
   {
      if(TP->query_dlugosc_wasow()*10.0 < 0.7 && TP->query_dlugosc_brody() < 0.7)
      {
          write("Przecie� nie masz zarostu.\n");
          return 1;
      }

      write("Wprawnymi ruchami rozpoczynasz si� strzyc "+
            this_object()->query_short(PL_NAR)+".\n");
      saybb(QCIMIE(this_player(), PL_MIA)+" wprawnymi ruchami rozpoczyna si� strzyc "+
            this_object()->query_short(PL_NAR)+".\n");
      clone_object("/std/brzytwa_paralize/zarost")->move(this_player());
      return 1;
   }
}

int
skladanie(string str)
{
    object brzytfa;

    notify_fail("Z�� co ?\n");

    if (!str) return 0;

    if (!parse_command(str, this_player(), "%o:" + PL_BIE, brzytfa))
        return 0;

    if (!brzytfa) return 0;

    if (brzytfa != this_object())
        return 0;   

    if(!brzytfa->query_prop(BRZYTWA_ROZLOZONA))
    {
        write(capitalize(brzytfa->short(PL_MIA))+" ju� jest z�o�ona.\n");
	return 1;
    }
	   
    write("Sk�adasz "+brzytfa->short(PL_BIE)+".\n");
    saybb(QCIMIE(this_player(),PL_MIA)+" sk�ada "+brzytfa->short(PL_BIE)+".\n");
    brzytfa->remove_prop(BRZYTWA_ROZLOZONA);
	brzytfa->remove_adj("roz�o�ony");
	brzytfa->dodaj_przym("rozk�adany","rozk�adani");
	brzytfa->odmien_short();
    return 1;
}

int
rozkladanie(string str)
{
    object brzytfa;

    notify_fail("Roz�� co ?\n");

    if (!str) return 0;

    if (!parse_command(str, this_player(), "%o:" + PL_BIE, brzytfa))
        return 0;

    if (!brzytfa) return 0;

    if (brzytfa != this_object())
        return 0;

    if(!brzytfa->query_wielded())
    {
	write("Wpierw musisz j� chwyci�.\n");
	return 1;
    }

    if(brzytfa->query_prop(BRZYTWA_ROZLOZONA))
    {
        write(capitalize(brzytfa->short(PL_MIA))+" ju� jest roz�o�ona.\n");
	return 1;
    }
          
    write("Rozk�adasz "+brzytfa->short(PL_BIE)+".\n");
    saybb(QCIMIE(this_player(),PL_MIA)+" rozk�ada "+brzytfa->short(PL_BIE)+".\n");
    brzytfa->add_prop(BRZYTWA_ROZLOZONA, 1);
	brzytfa->remove_adj("rozk�adany");
	brzytfa->dodaj_przym("roz�o�ony","roz�o�eni");
	brzytfa->odmien_short();
    return 1;
}
       
public int
pomoc(string str)
{
    object brzytfa;

    notify_fail("Nie ma pomocy na ten temat.\n");

    if (!str) return 0;

    if (!parse_command(str, this_player(), "%o:" + PL_MIA, brzytfa))
        return 0;

    if (!brzytfa) return 0;

    if (brzytfa != this_object())
        return 0;
    
    write("Mo�esz ni� ostrzyc w�sy i/lub brod�. Pami�taj, by "+
          "j� wpierw roz�o�y�!\n");
    return 1;
}
    
