#include <math.h>
#include <mudtime.h>

/* Katalog zawierajacy pliki ryb */
#define RYBY "/d/Standard/ryby/"

/* Stala do czasu wyciagania ryby - nia 
 * najwygodniej regulowac ow czas */
#define CZAS_WYCIAGANIA 60.0

#define DBG(x) find_player("rantaur")->catch_msg(x+"\n");

mapping lowiska; // = (["w rzece":(["szczupak":0.6, "ukleja":0.4])]);

/* Zwraca plik danej ryby */
string query_file(string ryba)
{
    ryba = lower_case(ryba);
    return RYBY+ryba+".c";
}

/**
 * Dodaje do lokacji �owisko o podanej nazwie z okre�lon� zawarto�ci� ryb
 * @param gdzie Nazwa �owiska - poprzez nia rozumiemy miejsce w kt�re
 *	  	        gracz ma zarzuci� w�dke (np. nazw� �owiska jest "w rzece")
 * @param ryby  Mapping sk�adaj�cy si� z nazwy ryby i jej procentowej
 *		        zawarto�ci w �owisku. Ryby podajemy nazwami np.
 *		        dodaj_lowisko("do rzeki":(["karp":0.9, "okon":0.1]))
 * @return      1 w przypadku udanego dodania, 0 w przypadku porazki
 */
int dodaj_lowisko(string gdzie, mapping ryby)
{
    if(!gdzie)
	    return 0;

    if(!ryby || !m_sizeof(ryby))
	    return 0;

    if(!lowiska)
	    lowiska = ([ ]);

    if(lowiska[gdzie] != 0)
	    return 0;

    /* Przerabiamy nazwy ryb na sciezki */
    string *indexy = m_indexes(ryby);
    int size = sizeof(indexy);

    for(int i = 0; i < size; i++)
    {
	    if(file_size(indexy[i]) == -1)
	    {
	        ryby[query_file(indexy[i])] = ryby[indexy[i]];
	        ryby = m_delete(ryby, indexy[i]);
	    }
    }

    lowiska[gdzie] = ryby;

    return 1;
}

void wyczysc_lowiska()
{
    lowiska = ([ ]);
}

string *query_lowiska()
{
    return m_indexes(lowiska);
}

/* Sprawdza czy obiekt jest rybka czy nie */
int query_ryba(string ryba)
{
    object tmp = clone_object(ryba);
    if(function_exists("create_object", tmp) == "/std/ryby/ryba")
    {
	    tmp->remove_object();
	    return 1;
    }
    else
    {
	    tmp->remove_object();
	    return 0;
    }
}

/**
 * Zwraca list� gatunk�w ryb wyst�puj�cych na danym �owisku
 * @param lowisko Nazwa �owiska
 */
string *query_ryby(string lowisko)
{
    return m_indexes(lowiska[lowisko]);
}

/* Modyfikuje prawdopodobienstwo wystapienia ryby pod
 * wzgledem pory dnia */
float modify_pora_dnia(string ryba, float prob)
{
    return prob*(ryba->query_mod_pory(PORA_MUDOWA));
}

/* Modyfikuje prawdopodobienstwo wziecia ryby wzgledem
 * uzytej przynety. Czyli zero albo jeden.
 */
float modify_przyneta(string przyneta, string ryba, float prob)
{
    if(member_array(przyneta, ryba->query_przynety()) != -1)
	    return prob;
    else
	    return 0.0;
}

/* Modyfikuje porawdopodobienstwo wystapienia ryby
 * wzgledem skilla gracza
 */
float modify_skill(int skill, string ryba, float prob)
{
    /* Modyfikacja zalezy od maksymalnej ceny ryby -
     * im ta wyzsza i im wyzszy skill tym 
     * wieksze prawdopodob. na wyciagniecie tej
     * ryby
     */

    float cena = itof(ryba->query_max_cena());
    prob *= (1.08 + 0.0015*itof(skill) - 0.1552/cena);

    return prob;
}

/* Zwraca wage wylosowanej ryby */
int get_weight(string ryba, int skill, string przyneta)
{
    /* Okreslamy dolna i gorna granice wagi ryby */
    int min;    
    int max;

    int tmp = 0;
    if(member_array(przyneta, ryba->query_przynety_mniejszej()) != -1)
	    tmp += 1;

    if(member_array(przyneta, ryba->query_przynety_wiekszej()) != -1)
	    tmp += 2;

    switch(tmp)
    {
	/* Mozemy zlapac tylko mala */
	case 1:
	    min = ryba->query_min_weight();
	    max = ryba->query_avg_weight();
	    break;
	case 2:
	/* Mozemy zlapac tylko duza */
	    min = ryba->query_avg_weight();
	    max = ryba->query_max_weight();
	    break;
	case 3:
	/* Mozemy zlapac duza lub mala */
	    min = ryba->query_min_weight();
	    max = ryba->query_max_weight();
	    break;
    }

    int weight = (7*skill*max)/1000;
    weight += random(max-weight-min+1);
    weight += min;

    return weight;
}

/* Zwraca czas przez jaki bedziemy �owi� ryb� */
int get_time(string zlowione, int waga, int skill)
{
    float walecznosc;
    float ret;

    if(waga <= zlowione->query_avg_weight())
	    walecznosc = zlowione->query_walecznosc()[0];
    else
	    walecznosc = zlowione->query_walecznosc()[1];

    ret = CZAS_WYCIAGANIA;
    ret *= walecznosc;
    ret *= LINEAR_FUNC(0.0, 5.75, 100.0, 1.0, itof(skill));
    ret += 10.0;
    ret += itof(random(11));

    return ftoi(ret);
}

/* Okresla czy ryba zerwie sie czy nie
 * @return - 1 - ryba sie zerwie
 *	     0 - ryba sie nie zerwie
 */
int czy_zerwie(string zlowione, int waga, int skill, float gietkosc)
{
    float walecznosc;

    if(waga <= zlowione->query_avg_weight())
	    walecznosc = zlowione->query_walecznosc()[0];
    else
	    walecznosc = zlowione->query_walecznosc()[1];

    /* Okre�lamy czy ryba sie zerwie */
    float prawd_niezerwania = gietkosc;
    prawd_niezerwania *= (1.0 - walecznosc);
    prawd_niezerwania *= LINEAR_FUNC(0.0, 0.7, 100.0, 1.47, itof(skill));

    int czy_zerwie = PROB_RANDOM(([0:prawd_niezerwania,
				1:(1.0-prawd_niezerwania)]));

    /* Czy haczyk sie nie urwie */
    if(czy_zerwie)
    {
        float szansa = LINEAR_FUNC(0.0, 0.4, 100.0, 0.8,itof(skill));
        if(PROB_RANDOM( ([1:szansa, 0:(1.0-szansa)]) ))
            return 2;
    }

    return czy_zerwie;
}

/**
 * Funkcja zwraca rzecz, kt�r� z�owili�my
 * @param lowisko Lowisko w kt�rym �owimy
 * @param skill Skill wedkujacego
 * @param przyneta Przyn�ta u�ywana przez w�dkuj�cego
 * @return Tablic�:
 *	    [0] �cie�ka do pliku obiektu
 *	    [1] waga obiektu
 *	    [2] czasu po�owu
 *	    [3] informacja o tym czy ryba sie zerwie 
 *	        (czyli 1, 2 gdy zerwie sie ale bez haczyka lub 0)
 *	    [4] rodzaj brania
 */
mixed fish(string lowisko, int skill, string przyneta, float gietkosc)
{
    mapping ryby = ([ ])+lowiska[lowisko];

    string *keys = m_indexes(ryby);

    int size = sizeof(keys);

    for(int i = 0; i < size; i++)
    {
	    /* Jesli obiekt nie jest ryba to nic nie ma na niego wplywu */
	    if(!query_ryba(keys[i]))
	        continue;
	
	    /* Modyfikujemy wzgl�dem pory dnia */
	    ryby[keys[i]] = modify_pora_dnia(keys[i], ryby[keys[i]]);

	    /* Modyfikujemy wzgledem przynety */
	    ryby[keys[i]] = modify_przyneta(przyneta, keys[i], ryby[keys[i]]);

	    /* Modyfikujemy wzgledem skilla */
	    ryby[keys[i]] = modify_skill(skill, keys[i], ryby[keys[i]]);

	    /* Je�li prawdopodobie�stwo wynosi 0 usuwamy pozycje */
	    if(ryby[keys[i]] == 0.0)
	        ryby = m_delete(ryby, keys[i]);
    }

    string zlowione = PROB_RANDOM(ryby);

    // Poki co dla nie-ryb mamy wartosci wzglednie stale
    // Druga wartosc tablicy tylko po to zeby sie indexy
    // zgadzaly z indexami tablicy ryby
    if(!query_ryba(zlowione))
	    return ({zlowione, 0, 60+random(11), random(2), 0});

    int waga = get_weight(zlowione, skill, przyneta);
    int czas = get_time(zlowione, waga, skill);
    int zerwie = czy_zerwie(zlowione, waga, skill, gietkosc);
    string rodzaj_brania = zlowione->query_rodzaj_brania(zlowione->duza_mala(waga));

    return ({zlowione, waga, czas, zerwie, rodzaj_brania});	
}

void stat_lowisko(string str)
{
    if(!str)
    {
	    write("Na lokacji s� dost�pne nast�puj�ce �owiska:\n");
	    dump_array(m_indexes(lowiska));
	    return;
    }

    if(!lowiska[str])
    {
	    write("Nie ma takiego �owiska!\n");
	    return;
    }

    mapping mryby = ([ ]) + lowiska[str];
    string *aryby = m_indexes(mryby);
    int size = sizeof(aryby);

    write("W �owisku s� nast�puj�ce ryby:\n");
    for(int i = 0; i < size; i++)
    {
	    if(!query_ryba(aryby[i]))
	    {
	        write(" * "+aryby[i]+"\t"+ftoa(mryby[aryby[i]])+"\n");
	        continue;
	    }
	
	    /* Modyfikujemy wzgl�dem pory dnia */
	    mryby[aryby[i]] = modify_pora_dnia(aryby[i], mryby[aryby[i]]);

	    write(" * "+aryby[i]+"\t"+ftoa(mryby[aryby[i]])+"\n");
    }
}


varargs float simulate_fishing(string lowisko, int skill,
                             string przyneta, float gietkosc, int it=50)
{
    int zerwanych_z_haczykiem;
    int zerwanych_bez_haczyka;
    int czas;
    int kasa;
    mixed *ryba;

    for(int i = 0; i < it; i++)
    {
        ryba = fish(lowisko, skill, przyneta, gietkosc);

        /* Ryby zerwane */
        if(ryba[3] == 1)
        {
            zerwanych_z_haczykiem++;
            czas += ftoi(0.42*itof(ryba[2]));
            continue;
        }

        if(ryba[3] == 2)
        {
            zerwanych_bez_haczyka++;
            czas += ftoi(0.42*itof(ryba[2]));
            continue;
        }

        czas += ryba[2];
        kasa += ftoi(LINEAR_FUNC(itof(ryba[0]->query_min_weight()), 1.0,
                                 itof(ryba[0]->query_max_weight()),
							     itof(ryba[0]->query_max_cena()),
                                 itof(ryba[1])));
    }

    float ile_na_godzine = itof(kasa)/(itof(czas)/3600.0);
    float proc_zerwanych = itof(zerwanych_z_haczykiem+zerwanych_bez_haczyka)/itof(it);

    write("********************************************************\n");
    write("Ilosc bran = "+it+"\n");
    write("Ilosc zerwanych z haczykiem = "+zerwanych_z_haczykiem+"\n");
    write("Ilosc zerwanych bez haczyka = "+zerwanych_bez_haczyka+"\n");
    write("Zerwanych razem = "+(zerwanych_bez_haczyka+zerwanych_z_haczykiem)+"\n");
    write("Procent zerwanych = "+ftoa(proc_zerwanych)+"\n");
    write("Calkowity czas lowienia = "+czas+"\n");
    write("Zarobek = "+kasa+"\n");
    write("Zarobek na godzine = "+ftoa(ile_na_godzine)+"\n");
    write("********************************************************\n");
}




