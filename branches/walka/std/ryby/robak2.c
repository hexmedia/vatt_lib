/*by ohm*/

inherit "/std/object";
#include <stdproperties.h>

void
create_object()
{
    ustaw_nazwe("robak");
    dodaj_przym("ma^ly", "mali");
    dodaj_przym("o^slizg^ly", "o^slizgli");
	dodaj_nazwy("przyn^eta");
     set_long("Bardzo ma^ly, wij^acy si^e do^s^c ^zwawo robaczek, jest "+
		 "pokryty grudkami ziemi, jakby niedawno go stamt^ad wykopano.\n");

    add_prop(OBJ_I_VALUE, 3);
    add_prop(OBJ_I_WEIGHT, 1);
}

/* Zwraca rodzaj przynety. Mozliwe wartosci to:
 * 'chleb', 'robak', 'rybka'
 */
string
query_przyneta_typ()
{
    return "robak";
}