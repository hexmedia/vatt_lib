inherit "/std/shadow";
#include <macros.h>
#include <composite.h>
//#include "local.h"

#define SH              shadow_who


static mapping          sub_map = ([]); /* Subloc : att_ob */


int
query_att_ctrl()
{
    return 1;
}

void
link_ob(object o, string id)
{
    sub_map[id] = o;
}

void
remove_att_shadow()
{    
    remove_shadow();
}

string
show_subloc(string sloc, object on, object fob)
{
    int i;
    object ob, att_ob;
    mixed tmpz, tmpp, ob_mapp, ob_mapz;
    string me_str, all_str, *str_arrp, *str_arrz;

    tmpz = m_indices(sub_map);
    tmpp = m_indices(sub_map);

if(!sizeof(tmpp))
    {   
        remove_att_shadow();
        return ::SH->show_subloc(sloc, on, fob);
    }
    
    if (!(att_ob=sub_map[sloc]))
        return ::SH->show_subloc(sloc, on, fob);
                
    if (environment(att_ob) != SH)
        return "";
            
    ob_mapz = att_ob->att_ob_map(1);
    ob_mapp = att_ob->att_ob_map(2);

    str_arrz = ({});
    str_arrz = ob_mapz->short(on, PL_NAR);
    
    str_arrp = ({});
    str_arrp = ob_mapp->short(on, PL_BIE);
    
    if (sizeof(str_arrz))
        tmpz = COMPOSITE_WORDS(str_arrz);
    else
        tmpz = "";
        
    if (sizeof(str_arrp))
        tmpp = COMPOSITE_WORDS(str_arrp);
    else
        tmpp = "";

    me_str = att_ob->att_on_desc(on, att_ob, tmpz, tmpp);
    all_str = att_ob->att_fob_desc(on, att_ob, tmpz, tmpp);

    if (on == fob)
        return me_str;

    return all_str;
}
/*
string
show_subloc(string sloc, object on, object fob)
{
    int i;
    object ob, att_ob;
    mixed tmp, ob_map;
    string me_str, all_str, *str_arr;

    tmp = m_indices(sub_map);

    if (!sizeof(tmp))
    {
        remove_att_shadow();
        return ::SH->show_subloc(sloc, on, fob);
    }

    if (!(att_ob=sub_map[sloc]))
        return ::SH->show_subloc(sloc, on, fob);

    if (environment(att_ob) != SH)
        return "";

    ob_map = att_ob->att_ob_map();

    str_arr = ({});
    str_arr = ob_map->short(on, PL_BIE);

 /* str_arrp = ({});
    str_arrp = ob_mapp->short(on, PL_BIE); /

    if (sizeof(str_arr))
        tmp = COMPOSITE_WORDS(str_arr);
    else
        tmp = "";

    me_str = att_ob->att_on_desc(on, att_ob, tmp, ob_map);
    all_str = att_ob->att_fob_desc(on, att_ob, tmp, ob_map);

    if (on == fob)
        return me_str;

    return all_str;
}
*/
void
leave_inv(object ob, object to)
{
    string sloc, att_ob;
    
    ::SH->leave_inv(ob, to);

    sloc = ob->query_prop("_att_id");
    att_ob = sub_map[sloc];
        
    if (att_ob->att_query(ob))
    {
        if (wildmatch("*Z", att_ob->query_ob_map()[ob]))
            att_ob->att_hook_wyjmij(TP, ob, att_ob);

        if (wildmatch("*P", att_ob->query_ob_map()[ob]))
            att_ob->att_hook_odczep(TP, ob, att_ob);
            
        att_ob->att_remove(ob);
    }
}


