/*
 * /std/pochwa
 *
 * Autor: Lil
 */

// TODO: auto_�adowanie
// TODO: dobywanie z konkretnej pochwy
// TODO: dwie pochwy na plecach

#pragma strict_types

inherit "/std/container";
inherit "/lib/keep";
#include <object_types.h>

#include <stdproperties.h>
#include <pl.h>
#include <macros.h>
#include <cmdparse.h>
#include "/sys/materialy.h"

#define POCHWA_NA_PLECACH	"_pochwa_na_plecach"
#define POCHWA_PRZY_UDZIE_P	"_pochwa_przy_udzie_p"
#define POCHWA_PRZY_UDZIE_L	"_pochwa_przy_udzie_l"
#define POCHWA_PRZY_GOLENI_P	"_pochwa_przy_goleni_p"
#define POCHWA_PRZY_GOLENI_L	"_pochwa_przy_goleni_l"
#define POCHWA_W_PASIE_P	"_pochwa_w_pasie_p"
#define POCHWA_W_PASIE_L	"_pochwa_w_pasie_l"
#define POCHWA_SUBLOC		"_pochwa_subloc"

#define POCHWA_PD_PLECY         1
#define POCHWA_PD_UDA           2
#define POCHWA_PD_GOLENIE       4
#define POCHWA_PD_PAS           8

int     gPrzypinaneDo           = POCHWA_PD_PAS;
string	gGdziePrzypieta         = 0;
object	gWlasciciel             = 0;

static string should_wear;

mixed gGdzieMozna = ({
		      "na plecach",
                      "przy prawym udzie",
                      "przy lewym udzie",
                      "przy prawej goleni",
                      "przy lewej goleni",
                      "w pasie po prawej stronie",
                      "w pasie po lewej stronie"
                     });

mixed gGdzieMoznaProp = ({
		          POCHWA_NA_PLECACH ,
			  POCHWA_PRZY_UDZIE_P,
			  POCHWA_PRZY_UDZIE_L,
			  POCHWA_PRZY_GOLENI_P,
			  POCHWA_PRZY_GOLENI_L,
			  POCHWA_W_PASIE_P,
			  POCHWA_W_PASIE_L
			 });
mixed gGdzieSloty = ({
                      TS_CHEST,
		      TS_R_THIGH,
		      TS_L_THIGH,
		      TS_R_SHIN,
		      TS_L_SHIN,
		      0,
		      0
		     });


void set_przypinane_do(int i);

public void
create_pochwa()
{
    ustaw_nazwe("pochwa");
    dodaj_przym("�adny","�adni");
    dodaj_przym("poz�acany","poz�acani");
    add_prop(CONT_I_WEIGHT, 2000);
    add_prop(CONT_I_VOLUME, 2500);
    set_przypinane_do(POCHWA_PD_PLECY | POCHWA_PD_UDA | POCHWA_PD_GOLENIE);
    add_subloc_prop(0, SUBLOC_I_DLA_O, O_BRON_MIECZE);
}

nomask void
create_container()
{
    add_prop(CONT_I_RIGID,              1);
    add_prop(CONT_I_REDUCE_VOLUME,    125);
    add_prop(CONT_I_MAX_WEIGHT,      5000);
    add_prop(CONT_I_MAX_VOLUME,      5000);
    add_prop(CONT_I_MAX_RZECZY,         1);
    set_long("Jest to pochwa, kt�r� mo�esz sobie gdzie� przypi��. S�u�y "+
	"do przechowywania broni.\n");
    set_keep(1);
    create_pochwa();
}

void
clean()
{
    if (gGdziePrzypieta)
    {
        if (gWlasciciel)
	{
            gWlasciciel->remove_subloc(POCHWA_SUBLOC+gGdziePrzypieta);
	    gWlasciciel->zwolnij_sloty(TO);
	    gWlasciciel->remove_prop(gGdzieMoznaProp[member_array(gGdziePrzypieta,gGdzieMozna)]);
	    obj_subloc = 0;
	}
        gWlasciciel = 0;
        gGdziePrzypieta = 0;
    }
}

void
init()
{
    add_action("attach", "przypnij");
    add_action("attach", "przyczep");
    add_action("detach", "odepnij");
    add_action("pomoc", "?", 2);
}

public int
pomoc(string str)
{                                                                              
    object pohf;
    string toWrite;
    int liczM = 0;

    notify_fail("Nie ma pomocy na ten temat.\n");

    if (!str) return 0;
    
    if (!parse_command(str, this_player(), "%o:" + PL_MIA, pohf))
        return 0;
    if (!pohf) return 0;
    if (!str) return 0;
    if (pohf != this_object())
        return 0;

    toWrite = TO->koncowka("Ten","T�","To")+" "+
	    TO->short(PL_BIE)+" mo�esz przypi�� w nast�puj�cych miejscach: ";
    if (TO->query_przypinane_do() & POCHWA_PD_PLECY)
    {
	    toWrite += "na plecach";
	    ++liczM;
    }
    if (TO->query_przypinane_do() & POCHWA_PD_UDA)
    {
	    if (liczM)
	    {
		    toWrite += ", ";
	    }
	    toWrite += "przy prawym udzie, przy lewym udzie";
	    ++liczM;
    }
    if (TO->query_przypinane_do() & POCHWA_PD_GOLENIE)
    {
	    if (liczM)
	    {
		    toWrite += ", ";
	    }
	    toWrite += "przy prawej goleni, przy lewej goleni";
	    ++liczM;
    }
    if (TO->query_przypinane_do() & POCHWA_PD_PAS)
    {
	    if (liczM)
	    {
		    toWrite += ", ";
	    }
	    toWrite += "w pasie po prawej stronie, w pasie po lewej stronie";
	    ++liczM;
    }
    if (!liczM)
    {
	    toWrite = TO->koncowka("Tego","Tej","Tego")+" "+
		                TO->short(PL_DOP)+" nigdzie nie mo�esz przypi��";
    }
    toWrite += ".\n";
    write(toWrite);
		 
        return 1;
}

int
attach(string str)
{
    object *obs;
    int ret;
    string gdzie;
    notify_fail("Przypnij co gdzie ?\n");

    if (!strlen(str))
        return 0;

    if (!parse_command(str, TP->subinventory(),
        "%i:"+ PL_BIE + " %s", obs, gdzie))
        return 0;

    obs = NORMAL_ACCESS(obs, 0, 0);
    if (!sizeof(obs)||!strlen(gdzie))
        return 0;

    if (sizeof(obs) > 1)
    {
        notify_fail("Zdecyduj si�, co chesz przypi��.\n");
        return 0;
    }
    ret = obs[0]->attach_now(this_player(),gdzie);

    if (ret)
        this_player()->set_obiekty_zaimkow(obs);

    return ret;
}

int
detach(string str)
{
    object *obs;

    object *tmpobs;
    int ret;
    notify_fail("Odepnij co?\n");

    if (!strlen(str))
        return 0;

    tmpobs = all_inventory(this_player());
    tmpobs -= TP->subinventory();
    
    if (!parse_command(str, tmpobs, "%i:"+ PL_BIE, obs))
        return 0;

    obs = NORMAL_ACCESS(obs, 0, 0);
    if (!sizeof(obs))
        return 0;

    if (sizeof(obs) > 1)
    {
        notify_fail("Zdecyduj si�, co chcesz odpi��.\n");
        return 0;
    }
    ret = obs[0]->detach_now(this_player());

    if (ret)
        this_player()->set_obiekty_zaimkow(obs);

    return ret;
}

int
detach_now(object who)
{
    if(!who)
        return 0;
    set_this_player(who);
    if(!gGdziePrzypieta)
    {
        write("Ale� " + this_object()->query_short(PL_MIA) + " nie jest nigdzie "+
	    "przypi�t" + koncowka("y","a","e") + ".\n");
        return 1;
    }
    clean();
    write("Odpinasz " + this_object()->query_short(PL_BIE) + ".\n");
    saybb(QCIMIE(this_player(),PL_MIA) + " odpina " + QSHORT(this_object(),PL_BIE)
        + ".\n");
    return 1;
}

int
attach_now(object who, string gdzie, int silent = 0)
{
	object *tmptab, *tmptab2;
    string gdzie2 = 0;
    int i = 0;
    if(!gdzie || !who)
        return 0;
    set_this_player(who);
    if(gGdziePrzypieta)
    {
        if (!silent) {
        write("Ale� " + this_object()->query_short(PL_MIA) + " jest ju� gdzie� "+
	    "przypi�t" + koncowka("y","a","e") + ".\n");
        }
        return 1;
    }

    if((gdzie == "w pasie") || (gdzie == "przy pasie"))
    {
        if(TP->query_prop(POCHWA_W_PASIE_P) && TP->query_prop(POCHWA_W_PASIE_L))
        {
            if (!silent) {
            write("Nie masz ju� miejsca, by przypi�� " + this_object()->query_short(PL_BIE)
                  + " " + gdzie + ".\n");
            }
            return 1;
        }
	if(TP->query_prop(POCHWA_W_PASIE_L))
            return attach_now(who,"w pasie po prawej stronie");
        return attach_now(who,"w pasie po lewej stronie");

    }
    if(gdzie == "przy udzie")
    {
        if(TP->query_prop(POCHWA_PRZY_UDZIE_P) && TP->query_prop(POCHWA_PRZY_UDZIE_L))
        {
            if (!silent) {
            write("Nie masz ju� miejsca, by przypi�� " + this_object()->query_short(PL_BIE)
                  + " " + gdzie + ".\n");
            }
            return 1;
        }
	if(TP->query_prop(POCHWA_PRZY_UDZIE_L))
            return attach_now(who,"przy prawym udzie");
        return attach_now(who,"przy lewym udzie");

    }
    if(gdzie == "przy goleni")
    {
        if(TP->query_prop(POCHWA_PRZY_GOLENI_P) && TP->query_prop(POCHWA_PRZY_GOLENI_L))
        {
            if (!silent) {
            write("Nie masz ju� miejsca, by przypi�� " + this_object()->query_short(PL_BIE)
                  + " " + gdzie + ".\n");
            }
            return 1;
        }
	if(TP->query_prop(POCHWA_PRZY_GOLENI_L))
            return attach_now(who,"przy prawej goleni");
        return attach_now(who,"przy lewej goleni");

    }
    i = member_array(gdzie,gGdzieMozna);
    if(i == -1)
    {
        if (!silent) {
        write("Przypnij " + this_object()->query_name(0,PL_BIE) +
    	    " gdzie?\n");
        }
        return 1;
    }
    gdzie2 = gGdzieMoznaProp[i];
    tmptab = TP->query_slot(gGdzieSloty[i]);
    tmptab2 = TP->query_armour(gGdzieSloty[i]);

    if (pointerp(tmptab2))
    {
	    if (pointerp(tmptab))
	    {
		    tmptab -= tmptab2;
	    }
    }
    if (pointerp(tmptab) && sizeof(tmptab))
    {
        if (!silent) {
        write("Przypi�cie "+ short(PL_DOP) +" uniemo�liwia ci "+ tmptab[-1..][0]->short() +"!\n");
        }
        return 1;
    }
    switch(gdzie)
    {
    		case "na plecach":
            		if(!(TO->query_przypinane_do() & POCHWA_PD_PLECY))
                        {
                            if (!silent) {
                                write(koncowka("Tego","Tej","Tego") + " " +
						this_object()->query_name(0,PL_DOP) +
						" nie da si� przypi�� na plecach.\n");
                            }
                                return 1;
                        }
                        break;
                case "przy prawym udzie":
                case "przy lewym udzie":
                        if(!(TO->query_przypinane_do() & POCHWA_PD_UDA))
                        {
                            if (!silent) {
                                write(koncowka("Tego","Tej","Tego") + " " +
						this_object()->query_name(0,PL_DOP) +
						" nie da si� przypi�� przy udzie.\n");
                            }
                                return 1;
                        }
                        break;
                case "przy prawej goleni":
                case "przy lewej goleni":
                        if(!(TO->query_przypinane_do() & POCHWA_PD_GOLENIE))
                        {
                            if (!silent) {
                                write(koncowka("Tego","Tej","Tego") + " " +
						this_object()->query_name(0,PL_DOP) +
						" nie da si� przypi�� przy goleni.\n");
                            }
                                return 1;
                        }
                        break;

                case "w pasie po lewej stronie":
                case "w pasie po prawej stronie":
                        if(!(TO->query_przypinane_do() & POCHWA_PD_PAS))
                        {
                            if (!silent) {
                                write(koncowka("Tego","Tej","Tego") + " " +
						this_object()->query_name(0,PL_DOP) +
						" nie da si� przypi�� w pasie.\n");
                            }
                                return 1;
                        }
                        break;
        }
        gGdziePrzypieta = gdzie;
        TP->add_subloc(POCHWA_SUBLOC+gGdziePrzypieta,TO);
	TP->add_prop(gdzie2, 1);
	obj_subloc = POCHWA_SUBLOC+gGdziePrzypieta;
        gWlasciciel = TP;
	TP->zajmij_sloty(TO);
    if (!silent) {
        write("Przypinasz sobie " + TO->query_short(PL_BIE) + " " + gdzie + ".\n");
        saybb(QCIMIE(TP,PL_MIA) + " przypina sobie " +
			TO->query_short(PL_BIE) + " " + gdzie + ".\n");
    }
        return 1;
}

int
query_is_pochwa()
{
    return 1;
}

void
set_przypinane_do(int i)
{
	gPrzypinaneDo = i;
}

int
query_przypinane_do()
{
	return gPrzypinaneDo;
}

string
show_subloc(string subloc, object on_obj, object for_obj)
{
    object *obs = all_inventory(this_object());
    string zwroc = capitalize(gGdziePrzypieta);
    if (on_obj == for_obj)
        zwroc += " przypi�t"+TO->koncowka("y","�","e")+" masz ";
    else
        zwroc += " przypi�t"+TO->koncowka("y","�","e")+" ma ";
    zwroc += this_object()->query_short(PL_BIE);
    if (sizeof(obs))
        zwroc += " zawieraj�c" + koncowka("y ","� ","e ") +
                 obs[0]->query_nazwa(PL_BIE);
    zwroc += ".\n";
    return zwroc;
}

void
remove_object()
{
    clean();
    ::remove_object();
}

void
leave_env(object from, object to)
{
    clean();
    ::leave_env(from, to);
}

public int *
query_slots()
{
	switch(gGdziePrzypieta)
	{
		case "na plecach":
			return ({TS_CHEST});
		case "przy prawym udzie":
			return ({TS_R_THIGH});
		case "przy lewym udzie":
			return ({TS_L_THIGH});
		case "przy prawej goleni":
			return ({TS_R_SHIN});
		case "przy lewej goleni":
			return ({TS_L_SHIN});
	}
	return ({ });
}

public nomask string
query_pochwa_auto_load()
{
    if(gGdziePrzypieta)
    {
	return " ~PochwaWorn~" + gGdziePrzypieta + "~ ";
    }

    return "";
}

public nomask string
init_pochwa_arg(string arg)
{
    string toReturn, gdzieByla;

    if (arg == 0) {
        return 0;
    }
    if (wildmatch("*~PochwaWorn~*", arg)) {
        string tmp;
        sscanf(arg, "%s~PochwaWorn~%s~ %s", tmp, gdzieByla, toReturn);
        foreach (string curstr : explode(tmp, "")) {
            if (curstr != " ")
                return arg;
        }
        should_wear = gdzieByla;
        return toReturn;
    }
    return arg;

}

public string
query_auto_load()
{
    return ::query_auto_load() + query_pochwa_auto_load();
}

public string
init_arg(string arg)
{
    return init_pochwa_arg(::init_arg(arg));
}

void
enter_env(object dest, object old)
{
    if (should_wear) {
        TO->short(PL_MIA);
        attach_now(dest, should_wear, 1);
        should_wear = 0;
    }
    ::enter_env(dest, old);
}
