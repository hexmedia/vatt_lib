/**
 * \file /std/room.c
 *
 * Oto obiekt lokacji. Powinien by� inheritowany przez wszystkie lokacje
 */

#pragma save_binary
#pragma strict_types

inherit "/std/container";

#include "/lib/sit.c";

#include <files.h>
#include <macros.h>
#include <ss_types.h>
#include <std.h>
#include <stdproperties.h>

#include "/std/room/exits.c"
#include "/std/room/description.c"
#include "/std/room/link.c"
#include "/std/room/move.c"
#include "/std/room/event.c"

/* na potrzeby odczytywania lokacji */
int lokacja_jest_odczytana = 0;
string init_args = 0;

static string	obj_short; /* Przeniesione z /std/object.c */

static object   room_link_cont;	/* Linked container */
static object   *accept_here = ({ }); /* Items created here on roomcreation */

/* jeremian's changes - 28.07.2005 */

static mapping rzeczy_niewyswietlane = ([ ]); /* rzeczy, kt�rych nie wy�wietlamy w defaultowej sublokacji */
static mapping rzeczy_niewyswietlane_plik = ([]); /* rzeczy, kt�rych nie wy�wietlamy w defaultowej sublokacji */
static int niewyswietlane_mask = 0; /* maska typ�w rzeczy niewy�wietlanych */

static mixed room_npcs = ({ }); // tablica sciezek do npcow
static mixed room_npcs_present = ({ }); // tablica npcow
static mixed room_npcs_functions = ({ }); // tablica funkcji

static int enable_clone_npcs;

string polmrok_long; //Dlugi opis polmroku.

void
add_npc(string path, int ile = 1, mixed fun = 0)
{
        if (path[-2..] == ".c")
                path = path[..-3];

    while (ile--)
    {
        NPCE_OBJECT->new_npc(path);
        room_npcs += ({ path });
        room_npcs_functions += ({ fun });
        room_npcs_present += ({ 0 });
    }
}

/*
 * Nazwa funkcji: zapisz_mnie
 * Opis         : Funkcja prosi SECURITY o zapisanie informacji o danym pomieszczeniu w pliku.
 */
public nomask void
zapisz_mnie()
{
	seteuid(0);
	SECURITY->save_room();
	seteuid(getuid(this_object()));
}

/*
 * Nazwa funkcji: zapisz_lokacje
 * Opis         : Funkcja fizycznie zapisuje informacje o danym pomieszczeniu w pliku.
 */

public nomask int
zapisz_lokacje(string room_name)
{
	if (!room_name)
	{
		return 0;
	}
	    
	seteuid(getuid(this_object()));
	save_object(room_name);
	seteuid(getuid(this_object()));

	return 1;
}

/*
 * Nazwa funkcji: odczytaj_lokacje
 * Opis         : Funkcja odczytuje informacje o danym pomieszczeniu w pliku.
 * Zwraca       : int 0/1 - sukces/pora�ka
 */

public int
odczytaj_lokacje()
{
    int ret;

    if (!((MASTER != ROOM_OBJECT) && (sizeof(explode(file_name(this_object()), "#")) == 1))) {
        return 1;
    }

    seteuid(getuid(this_object()));
    if (catch(ret = restore_object(file_name(this_object())))) {
        return 1;
    }

    return ret;
}

/**
 * Funkcja odpowiedzialna za zapis lokacji do pliku.
 */

public void
remove_room()
{
    int i = sizeof(room_npcs);
    if ((MASTER != ROOM_OBJECT) && (sizeof(explode(file_name(this_object()), "#")) == 1))
    {
        if (query_prop(ROOM_I_NIE_ZAPISUJ) == 0) {
            init_args = query_auto_load();
            if (stringp(init_args)) {
                init_args = implode(explode(init_args, ":")[1..], ":");
            }
            zapisz_mnie();
        }
    }

    while (i--) {
        NPCE_OBJECT->delete_npc(room_npcs[i]);
    }
}

/*
 * Nazwa funkcji: remove_object
 * Opis         : Funkcja ta najpierw zapisuje lokacj�, a nast�pnie wywo�uje remove_object()
 *                z nadklasy.
 * Zwraca       : int - wynik remove_object() z nadklasy
 */

public int
remove_object()
{
  remove_room();
	return ::remove_object();
}

/*
 * Nazwa funkcji: czy_odczytana_lokacja
 * Opis         : Funkcja podaje czy lokacja zosta�a odczytana z pliku czy nie.
 *                Wizard nie ma �adnego interesu w nadpisywaniu tej funkcji, tak wi�c
 *                jest ona no mask.
 * Zwraca       : int 1/0 - nie odczytana/odczytana
 */

nomask int
czy_odczytana_lokacja()
{
	return lokacja_jest_odczytana;
}

/**
 * Funkcja ustawiaj�ca mask� rzeczy niewy�wietlanych.
 *
 * @param maska - nowa maska rzeczy niewy�wietlanych.
 *
 * Maska rzeczy niewy�wietlanych odpowiada za wyrzucenie z opis�w lokacji
 * pewnych typ�w przedmiot�w.
 */

public void
set_niewyswietlane_mask(int maska)
{
  niewyswietlane_mask = maska;
}

/**
 * Funkcja zwracaj�ca mask� rzeczy niewy�wietlanych
 *
 * @return maska rzeczy niewy�wietlanych.
 * 
 * Maska rzeczy niewy�wietlanych odpowiada za wyrzucenie z opis�w lokacji
 * pewnych typ�w przedmiot�w.
 */

public int
get_niewyswietlane_mask()
{
  return niewyswietlane_mask;
}

/*
 * Nazwa funkcji: dodaj_rzecz_niewyswietlana
 * Opis: Dodaje rzecz (rzeczy) do rzeczy niewy�wietlanych w defaultowej sublokacji
 * Argumenty: string rzecz - nazwa (short) rzeczy, kt�rej nie chcemy, by by�a wy�wietlana
 *            int ilosc - ile rzeczy ma by� niewy�wietlanych. Defaultowo jedna.
 */

public void
dodaj_rzecz_niewyswietlana(string rzecz, int ilosc = 1)
{
	int tmp, n;
	string* pomtab;
//	write("ilosc: "+ilosc);
	pomtab = m_indices(rzeczy_niewyswietlane);
	if ((n = member_array(rzecz, pomtab)) == -1) {
		rzeczy_niewyswietlane[rzecz] = ({0, ilosc});
	}
	else {
		tmp = rzeczy_niewyswietlane[pomtab[n]][1];
		rzeczy_niewyswietlane[pomtab[n]] = ({0, ilosc + tmp});
	}
}

/*
 * Nazwa funkcji: dodaj_rzecz_niewyswietlana_plik
 * Opis: Dodaje rzecz (rzeczy) do rzeczy niewy�wietlanych w defaultowej sublokacji
 * Argumenty: string rzecz - nazwa pliku rzeczy, kt�rej nie chcemy, by by�a wy�wietlana
 *            int ilosc - ile rzeczy ma by� niewy�wietlanych. Defaultowo jedna.
 */

public void
dodaj_rzecz_niewyswietlana_plik(string rzecz, int ilosc = 1)
{
	int tmp, n;
	string* pomtab;
//	write("ilosc: "+ilosc);
	pomtab = m_indices(rzeczy_niewyswietlane_plik);
	if ((n = member_array(rzecz, pomtab)) == -1) {
		rzeczy_niewyswietlane_plik[rzecz] = ({0, ilosc});
	}
	else {
		tmp = rzeczy_niewyswietlane_plik[pomtab[n]][1];
		rzeczy_niewyswietlane_plik[pomtab[n]] = ({0, ilosc + tmp});
	}
}

/*
 * Nazwa funkcji: usun_rzecz_niewyswietlana
 * Opis: Usuwa rzecz (rzeczy) z rzeczy niewy�wietlanych w defaultowej sublokacji
 * Argumenty: string rzecz - nazwa (short) rzeczy, kt�rej nie chcemy, by by�a wy�wietlana
 *            int ilosc - ile rzeczy ma by� na powr�t wy�wietlana. Defaultowo jedna.
 */

public void
usun_rzecz_niewyswietlana(string rzecz, int ilosc = 1)
{
	int tmp, n;
	string* pomtab;
	pomtab = m_indices(rzeczy_niewyswietlane);
	if ((n = member_array(rzecz, pomtab)) != -1) {
		tmp = rzeczy_niewyswietlane[pomtab[n]][1] - ilosc;
		if (tmp <= 0) {
			tmp = 0;
			m_delete(rzeczy_niewyswietlane, pomtab[n]);
			return;
		}
		rzeczy_niewyswietlane[pomtab[n]] = ({0, tmp});
	}
}

/*
 * Nazwa funkcji: usun_rzecz_niewyswietlana_plik
 * Opis: Usuwa rzecz (rzeczy) z rzeczy niewy�wietlanych w defaultowej sublokacji
 * Argumenty: string rzecz - nazwa pliku rzeczy, kt�rej nie chcemy, by by�a wy�wietlana
 *            int ilosc - ile rzeczy ma by� na powr�t wy�wietlana. Defaultowo jedna.
 */

public void
usun_rzecz_niewyswietlana_plik(string rzecz, int ilosc = 1)
{
	int tmp, n;
	string* pomtab;
	pomtab = m_indices(rzeczy_niewyswietlane_plik);
	if ((n = member_array(rzecz, pomtab)) != -1) {
		tmp = rzeczy_niewyswietlane_plik[pomtab[n]][1] - ilosc;
		if (tmp <= 0) {
			tmp = 0;
			m_delete(rzeczy_niewyswietlane_plik, pomtab[n]);
			return;
		}
		rzeczy_niewyswietlane_plik[pomtab[n]] = ({0, tmp});
	}
}

/*
 * Nazwa funkcji: oczysc_rzeczy_niewyswietlane
 * Opis: Resetuje liczniki wyswietlen w rzeczach niewyswietlanych
 */

public void
oczysc_rzeczy_niewyswietlane()
{
	int i, il, tmp;
	string* pomtab;
	pomtab = m_indices(rzeczy_niewyswietlane);
	il = sizeof(pomtab);
	for (i = 0; i < il; ++i) {
		tmp = rzeczy_niewyswietlane[pomtab[i]][1];
		rzeczy_niewyswietlane[pomtab[i]] = ({ 0, tmp });
	}
}

/*
 * Nazwa funkcji: oczysc_rzeczy_niewyswietlane_plik
 * Opis: Resetuje liczniki wyswietlen w rzeczach niewyswietlanych
 */

public void
oczysc_rzeczy_niewyswietlane_plik()
{
	int i, il, tmp;
	string* pomtab;
	pomtab = m_indices(rzeczy_niewyswietlane_plik);
	il = sizeof(pomtab);
	for (i = 0; i < il; ++i) {
		tmp = rzeczy_niewyswietlane_plik[pomtab[i]][1];
		rzeczy_niewyswietlane_plik[pomtab[i]] = ({ 0, tmp });
	}
}

/*
 * Nazwa funkcji: czy_wyswietlic_rzecz
 * Opis: Sprawdza, czy dana rzecz ma by� wy�wietlana. Aktualizuje liczniki wy�wietle�.
 * Argument: object rzecz - nazwa (short) rzeczy
 * Zwraca: 0 - rzecz nie mo�e by� wy�wietlona
 *         1 - rzecz mo�e by� wy�wietlona
 */

public int
czy_wyswietlic_rzecz(object rzecz)
{
	int n;
	int* tmp;
	string* pomtab;

	if (!objectp(rzecz)) {
		return 0;
	}
	
  // sprawdzamy mask� rzeczy niewy�wietlanych
  if (rzecz->query_type() & niewyswietlane_mask) {
    return 0;
  }
  // sprawdzamy odpowiedniego propa
  if (rzecz->query_prop(OBJ_I_DONT_SHOW_IN_LONG)) {
    return 0;
  }
  
	// sprawdzamy w�r�d rzeczy po pliku;
	pomtab = m_indices(rzeczy_niewyswietlane_plik);
        if ((n=member_array(MASTER_OB(rzecz), m_indices(rzeczy_niewyswietlane_plik))) != -1) {
		tmp = rzeczy_niewyswietlane_plik[pomtab[n]];
		tmp[0] = tmp[0] + 1;
		rzeczy_niewyswietlane_plik[pomtab[n]] = tmp;
		if (rzeczy_niewyswietlane_plik[pomtab[n]][1] == 0) {
			return 0;
		}
		if (rzeczy_niewyswietlane_plik[pomtab[n]][0] <= rzeczy_niewyswietlane_plik[pomtab[n]][1]) {
			return 0;
		}
	}
	
	// sprawdzamy w�r�d rzeczy po nazwie
	pomtab = m_indices(rzeczy_niewyswietlane);
  if ((n = member_array(rzecz->short(), m_indices(rzeczy_niewyswietlane))) != -1) {
    tmp = rzeczy_niewyswietlane[pomtab[n]];
    tmp[0] = tmp[0] + 1;
    rzeczy_niewyswietlane[pomtab[n]] = tmp;
    if (rzeczy_niewyswietlane[pomtab[n]][1] == 0) {
      return 0;
    }
    if (rzeczy_niewyswietlane[pomtab[n]][0] <= rzeczy_niewyswietlane[pomtab[n]][1]) {
      return 0;
    }
  }
	return 1;
}
	
/* --- */




//----------FUNKCJE DO USTAWIANIA WLASNEGO OPISU POLMROKU--------

public string
polmrok_long()
{
    return check_call(polmrok_long);
}

/*
 * Function name: set_polmrok_long
 * Description:   Dziala podobnie jak set_long. Jesli set_polmrok_long bedzie zdefiniowany
 *                to ten opis bedzie sie pokazywal podczas polmroku. Jesli go nie ma, to
 *                w nocy bedziemy widzieli domyslny long polmroku.
 */
public void
set_polmrok_long(string str)
{
    polmrok_long = str;
}
//----------------------------------------------------------------


/*
 * Function name: create_room
 * Description  : Constructor. You should redefine this function to create
 *                your own room.
 */
public void
create_room()
{
    remove_prop(ROOM_I_INSIDE);          /* Default room has heaven above */
    add_prop(ROOM_I_TYPE, ROOM_NORMAL);  /* Default is a room */

    set_polmrok_long("Panuje tu p�mrok, wi�c nie widzisz wszystkiego dok�adnie.\n");
}


/*
 * Function name: create_container
 * Description  : Constructor. Since you may not redefine this function,
 *                you must define the function create_room() to create your
 *                room.
 */
nomask void
create_container()
{
    add_prop(ROOM_I_IS,    1);
    add_prop(ROOM_I_LIGHT, 1);
    add_prop(ROOM_I_HIDE, 25);
    
    room_link_cont = 0;

    seteuid(creator(this_object()));

    /* As service to the folks, we automatically call the function
     * enable_reset() to start resetting if the function reset_room() has
     * been redefined.
     */
    if (function_exists("reset_room", this_object()) != ROOM_OBJECT)
	enable_reset();

    if (odczytaj_lokacje())
    {
	    lokacja_jest_odczytana = 1;
    }

    set_event_time(300.0);
    set_long("@@dlugi_opis@@");

    create_room();

    //Resetujemy zio�a coby sobie ros�y �adnie.
    reset_herbs();

    if(query_prop(ROOM_I_TYPE) == ROOM_FOREST && !query_prop(ROOM_I_ILOSC_CHRUSTU))
        add_prop(ROOM_I_ILOSC_CHRUSTU, random(10));

    POGODA_OBJECT->register_room(this_object());
    
    /* Jesli w lokacji rosna ziola, odpalamy ich reset. */
   // if (sizeof(query_herb_files()))
   //     enable_reset();

    accept_here = all_inventory(this_object());
    if (!sizeof(accept_here))
	accept_here = ({ });

    if (!lokacja_jest_odczytana) {
        init_arg(0);
    }
    else {
        if (SECURITY->query_auto_load_rooms() || query_prop(ROOM_I_ALWAYS_LOAD)) {
            init_arg(init_args);
        }
        else {
            init_arg(0);
        }
    }
}

/*
 * Function name: reset_room
 * Description  : This function should be redefined to make the room reset
 *                every half our or so. If you redefine it, you do not have
 *                to call enable_reset() since we call it as part of our
 *                service ;-) Note that this service is only valid for rooms.
 */
void
reset_room()
{
    if(query_prop(ROOM_I_TYPE) == ROOM_FOREST && !query_prop(ROOM_I_ILOSC_CHRUSTU))
        add_prop(ROOM_I_ILOSC_CHRUSTU, random(5));
    reset_herbs();
}

/*
 * Function name: reset_container
 * Description  : This function will reset the container. Since you may not
 *                redefine it, you must define the function reset_room() to
 *                make the room reset.
 */
nomask void
reset_container()
{
    if (enable_clone_npcs) {
        object ob;
        string res;
        mixed fun;
        int i = sizeof(room_npcs);

        while (i--)
        {
            if (!NPCE_OBJECT->get_free_slots(room_npcs[i])) {
                continue;
            }

            if (room_npcs_present[i])
                continue;

            fun = room_npcs_functions[i];

            if (fun && !((stringp(fun) && call_self(fun)) || (functionp(fun) && fun()))) {
                continue;
            }

            if ((res = catch(room_npcs_present[i] = clone_object(room_npcs[i]))))
            {
                write("\nB��d przy tworzeniu npca\"" + room_npcs[i] + "\".\n"+res+"\n");
                continue;
            }


            room_npcs_present[i]->move(this_object(), 1);
            room_npcs_present[i]->init_arg(0);
            room_npcs_present[i]->start_me();
        }
    }

    reset_room();

    if (!sizeof(accept_here))
        accept_here = ({ });
    else
        accept_here = accept_here - ({ 0 });
}

public string
short()
{
    return obj_short;
}

public void
set_short(string str)
{
    if (strlen(str) && str[-1..-1] == "\n") str = str[..-2];
    if (strlen(str) && str[-1..-1] == ".") str = str[..-2];
    obj_short = str;
}

public string
query_short()
{
    return obj_short;
}

/*
 * Function name: clone_here
 * Description  : The behaviour of this function is exactly the same as the
 *                efun clone_object(). It clones the 'file' and returns the
 *                objectpointer to that object. However, it will also add
 *                the object to a list of items that 'belongs' in this room.
 *                This means that the presence of this object in the room will
 *                not prevent the room from being cleaned with clean_up().
 * Arguments    : string file - the path to the item to clone.
 * Returns      : object - the objectpointer to the clone.
 */
public object
clone_here(string file)
{
    object ob;
    
    ob = clone_object(file);
    accept_here += ({ ob });
    return ob;
}

/*
 * Function name: query_cloned_here
 * Description  : Returns all objects that have been cloned in this room with
 *                clone_here() or registered with add_accepted_here().
 * Returns      : object * - the list of objects.
 */
public object *
query_cloned_here()
{
    return secure_var(accept_here);
}

/*
 * Function name: add_accepted_here
 * Description  : With this function, you can register an object as being
 *                accepted in this room. This means that the object will not
 *                prevent the room from being cleaned up. It will give an
 *                item the same status as when it was cloned with clone_here()
 *                in this room.
 * Arguments    : object ob - the object to register.
 */
void
add_accepted_here(object ob)
{
    accept_here += ({ ob });
}

/*
 * Function name: light
 * Description:   Returns the light status in this room
 *                This function is called from query_prop() only.
 * Returns:	  Light value
 */
nomask int
light()
{
    int li;
    
    li = query_prop(ROOM_I_LIGHT);
    if (objectp(room_link_cont))
    {
	if ((environment(room_link_cont)) &&
	    (room_link_cont->query_prop(CONT_I_TRANSP) ||
	     room_link_cont->query_prop(CONT_I_ATTACH) ||
	    !room_link_cont->query_prop(CONT_I_CLOSED)))
	{
	    li += (environment(room_link_cont))->query_prop(OBJ_I_LIGHT);
	}
    }
    return query_internal_light() + li;
}

/*
 * Function name: set_container
 * Description:   Sets the container for which the room represents the inside
 * Arguments:	  ob: The container object
 */
public void
set_container(object ob)
{
    room_link_cont = ob;
}

/*
 * Function name: set_room
 * Description  : This function is a mask for the function set_room() in
 *                /std/container.c. That function is not valid for rooms,
 *                so we block it here.
 * Arguments    : the arguments described in /std/container.c
 */
public nomask void
set_room(mixed room)
{
}

/* 
 * Function name: update_internal
 * Description:   Updates the light, weight and volume of things inside
 *                also updates a possible connected container.
 * Arguments:     l: Light diff.
 *		  w: Weight diff. (Ignored)
 *		  v: Volume diff. (Ignored)
 */
public void
update_internal(int l, int w, int v, int a)
{
    ::update_internal(l, w, v, a);
    
    if (l)
	all_inventory()->notify_light_change(l);
    
    if (room_link_cont)
	room_link_cont->update_internal(l, w, v, a);
}

/*
 * Function name: clean_up
 * Description  : This function destruct the room if there is nothing in it.
 *                If you have special variables stored in a room you should
 *		  define your own clean_up(). Also if you on startup of the
 *		  room clone some objects and put inside it, please define
 *		  your own clean_up() to destruct the room. This saves a
 *		  lot of memory in the game.
 * Returns      : int 1/0 - call me again/ don't bother me again.
 */
public int
clean_up()
{
	/* Do not destroy the room object. */
	if (MASTER == ROOM_OBJECT)
	{
		return 0;
	}

	if (!query_prop(ROOM_I_NO_CLEANUP) &&
			!sizeof(all_inventory(this_object()) - accept_here))
	{
		remove_object();
	}

	return 1;
}

/*
 * Function name: room_add_object
 * Description:   Clone and move an object into the room
 * Arguments:	  file - What file it is we want to clone
 *		  num  - How many clones we want to have, if not set 1 clone
 *		  mess - Message to be written when cloned
 */
varargs void
room_add_object(string file, int num, string mess)
{
    int i;
    object ob;

    if (num < 1)
	num = 1;

    seteuid(getuid());
    for (i = 0; i < num; i++)
    {
	ob = clone_object(file);
	if (stringp(mess))
	{
	    ob->move(this_object(), 1);
	    tell_roombb(this_object(), mess, ({}), ob);
 	}
	else if (living(ob))
	    ob->move_living("xx", this_object());
	else
	    ob->move(this_object(), 1);
    }
}

/*
 * Function name: stat_object
 * Description:   Called when someone tries to stat the room
 * Returns:	  A string to write
 */
string
stat_object()
{
    string str;
    int type;

    str = ::stat_object();

    if (query_prop(ROOM_I_INSIDE))
	str += "wewn^atrz\t";
    else
	str += "zewn^atrz\t";

    type = query_prop(ROOM_I_TYPE);
    str += " ";
    switch (type)
    {
	case 0: str += "zwyk^ly"; break;
        case 1: str += "w wodzie"; break;
	case 2: str += "pod wod^a"; break;
        case 3: str += "w powietrzu"; break;
	case 4: str += "pla^za"; break;
        default: str += "nieznany typ"; break;
    }
    str += "\t";

    return str + "\n";
}

/*
 * Function name: query_domain
 * Description  : This function will return the name of the domain this
 *                room is in.
 * Returns      : string - the domain name.
 */
nomask string
query_domain()
{
    /* Normal room. */
    if (wildmatch("/d/*", file_name(this_object())))
    {
	return explode(file_name(this_object()), "/")[2];
    }

    /* Link-room. */
    /* Dodalem test query_link_master(), bo jak 0 wchodzilo do wildmatch
     * to byl runtime...
     */
    if (query_link_master() && wildmatch("/d/*", query_link_master()))
    {
	return explode(query_link_master(), "/")[2];
    }

    /* This shouldn't happen. */
    return BACKBONE_UID;
}

public int
is_room()
{
    return 1;    
}

/**
 * 
 */
void
leave_inv(object ob, object dest)
{
    //Zmieni�em kolejno�� a pozatym doda�em funkcje czyszcz�c� found_herbs'
    //z zi� ju� znalezionych.

    if(ob->query_herb())
        clean_found_herbs_from_ob(ob);
    leave_inv_sit(ob);
    ::leave_inv(ob,dest);
}


/*Funkcja do usuwania informacji o ogniskach. Vera */
void
run_fire_alarm()
{
    set_alarm(900.0+itof(random(1000)),0.0,"remove_prop",ROOM_WAS_FIRE);
}

void
enable_cloning_npcs()
{
    enable_clone_npcs = 1;
    reset();
}

public string
init_arg(string arg)
{
    set_alarm(0.1, 0.0, &enable_cloning_npcs());
    return ::init_arg(arg);
}

void
pozbierany_chrust(object player)
{
    int mverr;
    object ob;

    set_this_player(player);
    
    ob = clone_object("/std/chrust.c");

    mverr = ob->move(TP);

    if(mverr)
    {
        if(mverr == 1)
            write(UC(ob->short(PL_MIA)) + " jest za ci�k" + ob->koncowka("i", "a", "ie") + ".\n");
        else if(mverr == 8)
            write(UC(ob->short(PL_MIA)) + " jest za du�" + ob->koncowka("y", "a", "e") + ", "+ 
                "odk�adasz j� na ziemie.\n");
        else
            write("Nie jeste� w stanie podnie�� " + ob->short(PL_MIA) + ".\n");
	
	saybb(QCIMIE(TP, PL_MIA) + " odk�ada " + QSHORT(ob, PL_BIE) + ".\n");

	ob->move(environment(TP));     
    }
    else
    {
        write("Zebra�"+TP->koncowka("e�", "a�")+" troch� chrustu.\n");
        saybb(QCIMIE(TP, PL_MIA) + " uzbiera�"+TP->koncowka("", "a")
	      +" troch� chrustu.\n");
    }

    change_prop(ROOM_I_ILOSC_CHRUSTU, (query_prop(ROOM_I_ILOSC_CHRUSTU)-1));
    TP->remove_prop(LIVE_S_EXTRA_SHORT);
}

varargs int
przestan_zbierac(mixed arg)
{
    if(!objectp(arg))
    {
        mixed *alarmy = get_all_alarms();
        mixed *args;
        int i;
 
 	for(i=0;i<sizeof(alarmy);i++)
        {
	    if(alarmy[i][1] == "pozbierany_chrust")
            {
                args = alarmy[i][4];
                if(args[0] == TP)
		    remove_alarm(alarmy[i][0]);		    
            }
        }
    }
    saybb(QCIMIE(TP, PL_MIA) + " ko�czy zbiera� chrust.\n");
    TP->remove_prop(LIVE_S_EXTRA_SHORT);

}

int
zbieranie_chrustu_KURWA_ZLE()
{
	write("Komendy na Vatt'ghernie wyst�puj� w formie "+
	"dokonanej. Proponuj� u�y� 'zbierz'.\n");
	return 1;
}

int
zbieranie_chrustu(string str)
{
    int time;
    object par;

    notify_fail("Co takiego chcesz zebra�?\n");

    if(str != "chrust")
        return 0;

    if(!query_prop(ROOM_I_ILOSC_CHRUSTU))
    {
        write("Nie widzisz tu nigdzie ani jednej ga��zki, kt�r� "
	       +TP->koncowka("m�g�by�", "mog�aby�")+" zebra�.\n");
        return 1;
    }


    if(TP->query_prop(SIT_SIEDZACY) || TP->query_prop(SIT_LEZACY))
    {
        notify_fail("A mo�e najpierw staniesz na nogi?\n");
        return 0;
    }

    write("Schylasz si� i zaczynasz zbiera� chrust.\n");
    saybb(QCIMIE(TP, PL_MIA) + " schyla si� i zaczyna zbiera� chrust.\n");

    if(time = query_prop(OBJ_I_SEARCH_TIME))
        if(time < 0)
            time = 5;
        else
            time += 5;

    time = max(2, (time - query_prop(ROOM_I_ILOSC_CHRUSTU)));

    par = clone_object("/std/paralyze.c");
    par -> set_stop_verb("przesta�");
    par -> set_standard_paralyze("zbiera� chrust");
    par -> set_stop_fun("przestan_zbierac");
    par -> set_stop_object(TO);
    par -> set_remove_time(time);
    par -> set_fail_message("Teraz zbierasz chrust, nie mo�esz tego zrobi�!\n");
    par -> move(TP, 1);

    TP->add_prop(LIVE_S_EXTRA_SHORT, " zbieraj�c"+TP->koncowka("y", "a")+" chrust");
    set_alarm(itof(time) + 0.01, 0.0, &pozbierany_chrust(TP));

    return 1;
}

void
init_chrust()
{
    //"ZBIERAJ"? Kurwa�, co to za sk�adnia?
    //"u�miechaj si�"? "Bekaj"? "M�w: Kurwa!" ?!?!
    //Vera.
    //Czepiasz si� szczeg��w, chyba Delvi tak chcia�!
    //Krun.
    //add_action(zbieranie_chrustu_KURWA_ZLE, "zbieraj");
    add_action(zbieranie_chrustu, "zbierz");
}

void
init()
{
    init_dirs();
    init_chrust();
    init_sit();
    ::init();
}

/*
 * Nazwa funkcji : prevent_leave
 * Opis          : Sprawdza, czy dany obiekt moze opuscic t� lokacj�.
 * Argumenty     : object - sprawdzany obiekt.
 * Funkcja zwraca: 1 - jesli _NIE_ moze. 0 - jesli moze.
 */
public int
prevent_leave(object ob)
{
    return ob->prevent_leave_env(TO);
}

/*
 * Nazwa funkcji : prevent_enter
 * Opis          : Sprawdza, czy dany obiekt moze wejsc do tej lokacji.
 * Argumenty     : object - sprawdzany obiekt.
 * Funkcja zwraca: 1 - jesli _NIE_ moze wejsc, 0 - jesli moze.
 */
public int
prevent_enter(object ob)
{
    return ob->prevent_enter_env(TO);
}
