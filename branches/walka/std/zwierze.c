/*
  /std/zwierze.c

  Uzyj tego obiektu, aby stworzyc dowolne DZIKIE zwierze.
  np. kot, pies, borsuk, �o�, nied�wied� etc.

  STD nieco przeze mnie przerobiony. By unikn�� bezsensownej walki
  z kotem czy innym zwierzakiem - oraz by umo�liwi� polowania z
  prawdziwego zdarzenia, a nie takie pierdy, �e szczur mnie zabija :P
  I takie tam.... ^_^

                                       Friday 25 of August 2006, Vera

*/
#pragma save_binary
#pragma strict_types

#include <pl.h>
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <object_types.h>
#include <filter_funs.h>

inherit "/std/creature";
inherit "/std/combat/unarmed";

inherit "/std/act/seqaction";
inherit "/std/act/trigaction";
inherit "/std/act/domove";
inherit "/std/act/action";
inherit "/std/act/attack";

int run_away_alarm;
//object ostatnia_lok;

#define QEXC query_combat_object()

void
create_zwierze()
{
}

nomask void
create_creature()
{
    add_leftover("/std/skora", "sk�ra", 1, 0, 1, 1, 0, O_SKORY);

    add_leftover("/std/leftover", "mi�so", 1, 0, 0, 1, 0, O_JEDZENIE);

    add_leftover("/std/leftover", "z�b", random(5) + 2, 0, 1, 0, 0, O_KOSCI);

    add_leftover("/std/leftover", "czaszka", 1, 0, 1, 1, 0, O_KOSCI);

    add_leftover("/std/leftover", "ko��", random(3) + 2, 0, 1, 1, 0, O_KOSCI);

    add_leftover("/std/leftover", "serce", 1, 0, 0, 1, 0, O_JEDZENIE);

    add_leftover("/std/leftover", "oko", 2, 0, 0, 0, 0, O_JEDZENIE);

    create_zwierze();

    dodaj_nazwy("zwierz�");
}

void
reset_zwierze()
{
}

nomask void
reset_creature()
{
    reset_zwierze();
}

/*
 * Function name:  default_config_zwierze
 * Description:    Sets some necessary values for this creature to function
 *                 This is basically stats and skill defaults.
 */
varargs public void
default_config_zwierze(int lvl)
{
    default_config_creature(lvl);
}

/*
 * Function name: add_attack
 * Description:   Add an attack to the attack array.
 * Arguments:	  
 *             wchit: Weapon class to hit
 *             wcpen: Weapon class penetration
 *	       dt:    Damage type
 *             %use:  Chance of use each turn
 *	       id:    Specific id, for humanoids W_NONE, W_RIGHT etc
 *
 * Returns:       True if added.
 */
public int
add_attack(int wchit, int wcpen, int damtype, int prcuse, int id)
{
    if (!QEXC)
	return 0;
    return (int)QEXC->cb_add_attack(wchit, wcpen, damtype, prcuse, id);
}

/*
 * Function name: remove_attack
 * Description:   Removes a specific attack
 * Arguments:     id: The attack id
 * Returns:       True if removed
 */
public int
remove_attack(int id)
{
    if (!QEXC)
	return 0;
    return (int)QEXC->cb_remove_attack(id); 
}

/*
 * Function name: add_hitloc
 * Description:   Add a hitlocation to the hitloc array
 * Arguments:	  
 *	      ac:    The ac's for a given hitlocation
 *	      %hit:  The chance that a hit will hit this location
 *	      desc:  String describing this hitlocation, ie "head", "tail"
 *	      id:    Specific id, for humanoids A_TORSO, A_HEAD etc
 *
 * Returns:       True if added.
 */
public int
add_hitloc(int *ac, int prchit, string desc, int id)
{
    if (!QEXC)
	return 0;
    return (int)QEXC->cb_add_hitloc(ac, prchit, desc, id);
}

/*
 * Function name: remove_hitloc
 * Description:   Removes a specific hit location
 * Arguments:     id: The hitloc id
 * Returns:       True if removed
 */
public int
remove_hitloc(int id)
{
    if (!QEXC)
	return 0;
    return (int)QEXC->cb_remove_hitloc(id); 
}

/*
 * Function name: query_humanoid
 * Description  : Since this is not a humanoid, we mask this function so
 *                that it returns 0.
 * Returns      : int - 0.
 */
public int
query_humanoid()
{
    return 0;
}

public int
query_zwierze()
{
    return 1;
}

//To jest z�e! :P
/*public int
query_npc()
{
    return 0;
}*/


/*
 * Function name: attacked_by
 * Description:   Dzikie zwierz�ta generalnie nie walcz�, z
 *                pewnymi wyj�tkami - dlatego potrzebne s� tu du�e zmiany
 */
void
attacked_by(object wrog)
{
//write("attacked_by\n");
    ::attacked_by(wrog);    

    //Wpierw sprawd�my, czy jaki� wyexpiony zwierz ma jakie� szanse
    // np. nied�wied�, w�� :P
    if(TO->query_average_stat() >= wrog->query_average_stat())
        return; 

	if(get_alarm(run_away_alarm) == 0)
	{
    //Je�li jeste�my zwierzyn� w lesie, lub gdziekolwiek indziej byleby
    //nie w mie�cie to spierdalamy jak najdalej i w zale�no�ci od hp
	//- wiadomo, im bardziej ranne jest zwierz�, tym w wi�kszej panice
	//jest i spierdziela gdzie popadnie :)
    if(ENV(TO)->query_prop(ROOM_I_TYPE) != ROOM_NORMAL)
	{
		//procentowy wykaz ilo�ci hp
		int hp_p = 100 * query_hp() / query_max_hp();

		switch(hp_p)
		{
			case 80..100:
				run_away_alarm = set_alarm(0.3,0.0,"run_away",1);
				break;
			case 60..79:
				run_away_alarm = set_alarm(0.3,0.0,"run_away",2);
				break;
			case 40..59:
				run_away_alarm = set_alarm(0.3,0.0,"run_away",random(3)+1);
				break;
			case 20..39:
				run_away_alarm = set_alarm(0.3,0.0,"run_away",random(3)+2);
				break;
			case 1..19:
				run_away_alarm = set_alarm(0.3,0.0,"run_away",random(4)+3);
				break;
			default:
				run_away_alarm = set_alarm(0.3,0.0,"run_away");
				break;
		}
	}
	else
		run_away_alarm = set_alarm(0.3,0.0,"run_away"); //o jedn� lok.

	}
}

/*
 *Dzi�ki tej funkcji sp�oszony zwierzak nie powinien
 *nas zafludowa� uciekaniem, co si� zdarza�o, je�li
 *wywo�a�y si� np. 2 run_away z rz�du i drugi by� de facto
 *powrotem na lokacje gracza (i tak kilka razy...)
 */
public int
prevent_enter_env(object dest)
{
//find_player("vera")->catch_msg("prefent_enter\n");
//if(dest == TO->query_prop(LIVE_O_LAST_ROOM) &&
	//if(TO->query_prop(LIVE_O_LAST_ROOM))

	object *gracze = FILTER_PLAYERS(all_inventory(dest));

	//if(sizeof(TO->query_enemy(-1)))
	//{)
	
	if(sizeof(gracze))
		return 1;

		//if(ENV(TO->query_enemy()) == dest)
//			return 1;
	//}
	return 0;
}

void
ploszymy_sie(object przez_kogo)
{
	if(TO->query_aggressive() || TO->query_attack())
		return;

	if(get_alarm(run_away_alarm) != 0)
		return;

	if(!objectp(przez_kogo))
		return;

	if(ENV(przez_kogo) != ENV(TO))
		return;

	if(random(2))
		command("emote p�oszy si�!");


	if(ENV(TO)->query_prop(ROOM_I_TYPE) != ROOM_NORMAL)
		run_away(random(3)+1);
	else
		run_away();
}

nomask void
signal_enter(object ob, object skad)
{
	if(!sizeof(FILTER_IS_SEEN(ob,({TO}))) ||
	   !sizeof(FILTER_CAN_SEE_IN_ROOM(({TO}))))
		return;


    //Je�li to zwyk�a lokacja typu: miasto, pomieszczenie, to nie ucieka
    //zwierzak - bo jest przyzwyczajony do ludzi.
    if(environment(TO)->query_prop(ROOM_I_TYPE) == ROOM_NORMAL ||
       !ob->query_humanoid())
        return;

    //sprawd�my, czy jest to agresywne zwierz�. W takim przypadku mo�e
    //nawet zaatakowa�. <lol>
    if(TO->query_aggressive())
        return;

	//podczas walki uciekamy, gdy nas goni�.
	if(TO->query_enemy(0) && TO->query_attack() &&
		ENV(TO->query_enemy(0)) == ENV(TO))
		set_alarm(itof(random(2))+0.5,0.0,"run_away",random(3)+1);

    //W przeciwnym wypadku...napi�tnowany biedny zwierzak umyka ;)
    //Osoba o niskiej charyzmie dla zwierzat - czyli o niskim umie
    //opieki nad zwierzakami - bedzie je ploszyc.
    if(ob->query_prop(SS_ANI_HANDL) < 25+TO->query_prop(SS_AWARENESS)/2+random(15))
    {
        if (get_alarm(run_away_alarm) == 0)
			if(!TO->query_attack())
            	set_alarm(itof(random(2))+0.5,0.0,"ploszymy_sie",ob);
    }

    return;
}

/**
 * Tutaj mo�emy ustawi� jak zwierze reaguje na krzyk.
 */
void
sygnal_krzyku(object ob, string str, object env=0)
{
    //Je�li wiz possessnie zwierze, to ono nie zareaguje nijak
    if(interactive(TO))
        return;

    ploszymy_sie(ob);
}
