/*
   /std/combat/combat.h
*/

#define MAX_ATTACK 10
#define MAX_HITLOC 20
#define MAX_ENEMIES 10
#define MAX_ATTACKING_ENEMIES 4

#define ATT_WCHIT  0
#define ATT_WCPEN  1
#define ATT_DAMT   2
#define ATT_PROCU  3
#define ATT_SKILL  4           /* The skill 0-100 of this attack */
#define ATT_M_HIT  5
#define ATT_M_PEN  6
#define ATT_WEIGHT 7
#define ATT_FCOST  8

#define HIT_AC   0
#define HIT_PHIT 1
#define HIT_DESC 2
#define HIT_M_AC 3

#define NW_OPISY_CIOSOW ({\
		"staraj�c si� przewidzie� kolejny ruch przeciwnika,",\
		"korzystaj�c z nieuwagi przeciwnika,",\
		"wykorzystuj�c dekoncentracj� przeciwnika,",\
		"planuj�c kolejny cios,",\
		"dostrzegaj�c luk� w obronie przeciwnika,",\
		"uprzedzaj�c cios przeciwnika,",\
		"zauwa�aj�c ods�oni�cie przeciwnika,",\
		"wychylaj�c si� nieznacznie do przodu,",\
		"czuj�c przyspieszony oddech przeciwnika,",\
		"widz�c dekoncentracj� przeciwnika,",\
		"pr�buj�c znale�� luk� w obronie przeciwnika,",\
		"staraj�c si� zdekoncentrowa� przeciwnika,"})

#define NW_PRZYMIOTNIK ({"niepewnym",\
		"szcz�liwym",\
		"spokojnym",\
		"nieznacznym",\
		"powolnym",\
		"prostym",\
		"finezyjnym",\
		"nietypowym",\
		"zaskakuj�cym",\
		"przemy�lanym",\
		"pewnym",\
		"szybkim",\
		"celnym",\
		"zdecydowanym"})

#define NW_RODZAJ_CIOSU ([W_IMPALE:({"wypadem", "pchni�ciem", "wbiciem", "uderzeniem", "ciosem", "atakiem"}),\
		W_SLASH:({"zamachem", "wymachem", "ci�ciem", "uderzeniem", "ciosem", "atakiem"}),\
		W_BLUDGEON:({"zamachem", "wymachem", "uderzeniem", "ciosem", "atakiem"})])

#define NW_O_LEDWO ([W_IMPALE:({({"nak�uwa", " nieznacznie"}),\
			({"nak�uwa", " delikatnie"}),\
			({"k�uje", " nieznacznie"}),\
			({"k�uje", " delikatnie"})}),\
		W_SLASH:({({"zadrapuje", " nieznacznie"}),\
			({"zadrapuje", " delikatnie"}),\
			({"nacina", " nieznacznie"}),\
			({"nacina", " delikatnie"})}),\
		W_BLUDGEON:({({"muska", " nieznacznie"}),\
			({"obija", " delikatnie"}),\
			({"ociera", " nieznacznie"}),\
			({"siniaczy", " delikatnie"})})])

#define NW_O_LEKKO ([W_IMPALE:({({"k�uje", " p�ytko"}),\
			({"nak�uwa", " lekko"})}),\
		W_SLASH:({({"tnie", " p�ytko"}),\
			({"tnie", " lekko"})}),\
		W_BLUDGEON:({({"obija", " lekko"}),\
			({"uderza", " lekko"})})])

#define NW_O_RANISZ ([W_IMPALE:({({"rani", " bole�nie"})}),\
		W_SLASH:({({"tnie", " bole�nie"}),\
			({"tnie", " do�� g��boko"})}),\
		W_BLUDGEON:({({"t�ucze", " bole�nie"}),\
			({"t�ucze", " powa�nie"}),\
			({"�omocze", ""})})])

#define NW_O_POWAZNIE ([W_IMPALE:({({"trafia", " zadaj�c powa�ne obra�enia"})}),\
		W_SLASH:({({"siecze", " g��boko"})}),\
		W_BLUDGEON:({({"gruchocze", ""}),\
			({"obija", " porz�dnie"})})])

#define NW_O_BCIEZKO ([W_IMPALE:({({"rani", " bardzo g��boko"}),\
			({"trafia", " rani�c powa�nie"})}),\
		W_SLASH:({({"r�bie", " bardzo ci�ko rani�c"}),\
			({"r�bie", ""})}),\
		W_BLUDGEON:({({"mia�d�y", ""}),\
			({"grzmoci", " z ca�ej si�y"}),\
			({"druzgocze", " pot�nie"})})])

#define NW_O_MASAKRUJESZ ([W_IMPALE:({({"masakruje", ""})}),\
		W_SLASH:({({"masakruje", ""})}),\
		W_BLUDGEON:({({"masakruje", ""})})])

// Sta�e zwi�zane z ranami
#define HITLOC_WOUND_THRESHOLD 0.2
#define AREA_WOUND_THRESHOLD 0.2
