/*
  /std/combat/humunarmed.c

  Humanoid unarmed initialization routines.

  These routines can inherited by all living objects that need
  humanoid unarmed combat values of a standardized type.

  For player objects which normally use a locked combat system, ie where
  the hitlocations and attacks can not be added or removed, this file
  is normally inherited for unarmed initialization purposes.

  Revision history:

 */
#pragma save_binary
#pragma strict_types

inherit "/std/combat/unarmed";

#include "/sys/wa_types.h"
#include "/sys/ss_types.h"
#include "/sys/macros.h"
#include "/sys/formulas.h"

#define QEXC (this_object()->query_combat_object())
#define WW_BONUS 3

/*
 * Function name: cr_configure
 * Description:   Configures basic values for this humanoid.
 */
public nomask void
cr_configure() 
{
    ::cr_configure();

    if (query_attackuse())
	QEXC->cb_set_attackuse(query_attackuse());
    else
	QEXC->cb_set_attackuse(100);
}

/*
 * Function name: cr_reset_attack
 * Description:   Set the values for a specific attack. These are called from
 *		  the external combat object.
 * Arguments:     aid: The attack id
 */
public nomask void
cr_reset_attack(int aid)
{
    int wchit, wcpen, uskill;

    ::cr_reset_attack(aid);

    if (!sizeof(query_ua_attack(aid)))
    {
	wchit = W_HAND_HIT;
	wcpen = W_HAND_PEN;
	if (uskill = this_object()->query_skill(SS_UNARM_COMBAT))
	{
	    wchit += F_UNARMED_HIT(uskill, this_object()->query_stat(SS_DEX));
	    wcpen += F_UNARMED_PEN(uskill, this_object()->query_stat(SS_STR));
	}

	if (uskill < 1)
	   uskill = -1;

	switch(aid)
	{
	case W_RIGHT:
        if ((!QEXC->is_configured("prawa r�ka")) ||
                (QEXC->query_hitloc_area_percentage("prawa r�ka") <= 0.0)) {
	      QEXC->cb_add_attack(0, 0, W_BLUDGEON, 0, aid, uskill + WW_BONUS);
        }
        else {
	      QEXC->cb_add_attack(wchit, wcpen, W_BLUDGEON, 25, aid, uskill + WW_BONUS);
        }
	    break;
	case W_LEFT:
        if ((!QEXC->is_configured("lewa r�ka")) ||
                (QEXC->query_hitloc_area_percentage("lewa r�ka") <= 0.0)) {
	      QEXC->cb_add_attack(0, 0, W_BLUDGEON, 0, aid, uskill + WW_BONUS);
        }
        else {
	      QEXC->cb_add_attack(wchit, wcpen, W_BLUDGEON, 25, aid, uskill + WW_BONUS);
        }
	    break;
	case W_BOTH:
	    /*
             * We use the hands separately in unarmed combat
             */
	    QEXC->cb_add_attack(0, 0, W_BLUDGEON, 0, aid, uskill + WW_BONUS);
	    break;
	case W_FOOTR:
	    QEXC->cb_add_attack(wchit, wcpen, W_BLUDGEON, 25, aid, uskill);
	    break;
	case W_FOOTL:
	    QEXC->cb_add_attack(wchit, wcpen, W_BLUDGEON, 25, aid, uskill);
	    break;
	}
    }
}

/*
 * Function name: cr_reset_hitloc
 * Description:   Set the values for a specific hitlocation
 * Arguments:     hid: The hitlocation (bodypart) id
 */
public nomask void
cr_reset_hitloc(int hid)
{
    ::cr_reset_hitloc(hid);

    if (!sizeof(query_ua_hitloc(hid))) 
    {
	switch(hid)
	{
	case A_HEAD:
	    QEXC->cb_add_hitloc(A_UARM_AC, 4, "g�ow�", hid, 4);
	    TO->add_hitloc_hp_info(hid, 4);
	    break;
	case A_CHEST:
	    QEXC->cb_add_hitloc(A_UARM_AC, 15, "klatk� piersiow�", hid, 15);
	    TO->add_hitloc_hp_info(hid, 15);
	    break;
	case A_STOMACH:
	    QEXC->cb_add_hitloc(A_UARM_AC, 15, "brzuch", hid, 15);
	    TO->add_hitloc_hp_info(hid, 15);
	    break;
	case A_L_SHOULDER:
	    QEXC->cb_add_hitloc(A_UARM_AC, 5, "lewy bark", hid, 5);
	    TO->add_hitloc_hp_info(hid, 5);
	    break;
	case A_R_SHOULDER:
	    QEXC->cb_add_hitloc(A_UARM_AC, 5, "prawy bark", hid, 5);
	    TO->add_hitloc_hp_info(hid, 5);
	    break;
	case A_L_ARM:
	    QEXC->cb_add_hitloc(A_UARM_AC, 6, "lewe rami�", hid, 6);
	    TO->add_hitloc_hp_info(hid, 6);
	    break;
	case A_R_ARM:
	    QEXC->cb_add_hitloc(A_UARM_AC, 6, "prawe rami�", hid, 6);
	    TO->add_hitloc_hp_info(hid, 6);
	    break;
	case A_L_FOREARM:
	    QEXC->cb_add_hitloc(A_UARM_AC, 5, "lewe przedrami�", hid, 5);
	    TO->add_hitloc_hp_info(hid, 5);
	    break;
	case A_R_FOREARM:
	    QEXC->cb_add_hitloc(A_UARM_AC, 5, "prawe przedrami�", hid, 5);
	    TO->add_hitloc_hp_info(hid, 5);
	    break;
	case A_L_HAND:
	    QEXC->cb_add_hitloc(A_UARM_AC, 3, "lew� d�o�", hid, 3);
	    TO->add_hitloc_hp_info(hid, 3);
	    break;
	case A_R_HAND:
	    QEXC->cb_add_hitloc(A_UARM_AC, 3, "praw� d�o�", hid, 3);
	    TO->add_hitloc_hp_info(hid, 3);
	    break;
	case A_L_THIGH:
	    QEXC->cb_add_hitloc(A_UARM_AC, 6, "lewe udo", hid, 6);
	    TO->add_hitloc_hp_info(hid, 6);
	    break;
	case A_R_THIGH:
	    QEXC->cb_add_hitloc(A_UARM_AC, 6, "prawe udo", hid, 6);
	    TO->add_hitloc_hp_info(hid, 6);
	    break;
	case A_L_SHIN:
	    QEXC->cb_add_hitloc(A_UARM_AC, 5, "lew� �ydk�", hid, 5);
	    TO->add_hitloc_hp_info(hid, 5);
	    break;
	case A_R_SHIN:
	    QEXC->cb_add_hitloc(A_UARM_AC, 5, "praw� �ydk�", hid, 5);
	    TO->add_hitloc_hp_info(hid, 5);
	    break;
	case A_L_FOOT:
	    QEXC->cb_add_hitloc(A_UARM_AC, 3, "lew� stop�", hid, 3);
	    TO->add_hitloc_hp_info(hid, 3);
	    break;
	case A_R_FOOT:
	    QEXC->cb_add_hitloc(A_UARM_AC, 3, "praw� stop�", hid, 3);
	    TO->add_hitloc_hp_info(hid, 3);
	    break;
	}
    }
}

/*
 * Function name: cr_try_hit
 * Description:   Decide if a certain attack fails because of something
 *                related to the attack itself.
 * Arguments:     aid:   The attack id
 * Returns:       True if hit, otherwise 0.
 */
public nomask int
cr_try_hit(int aid) { return 1; }

/*
 * Function name: cr_attack_desc
 * Description:   Gives the description of a certain unarmed attack slot.
 * Arguments:     aid:   The attack id
 * Returns:       string holding description
 */
public nomask string
cr_attack_desc(int aid)
{
    switch(aid)
    {
    case W_RIGHT:return (random(100)<80 ? "prawej pi�ci" : "prawego �okcia");
    case W_LEFT:return (random(100)<80 ? "lewej pi�ci" : "lewego �okcia");
    case W_BOTH:return "obu r�k";
    case W_FOOTR:return (random(100)<80 ? "prawej stopy" : "prawego kolana");
    case W_FOOTL:return (random(100)<80 ? "lewej stopy" : "lewego kolana");
    }  
    return "mind"; /* should never occur */
}

/*
 * Function name: cr_got_hit
 * Description:   Tells us that we got hit. It can be used to reduce the ac
 *                for a given hitlocation for each hit.
 * Arguments:     hid:   The hitloc id
 *                ph:    The %hurt
 *                att:   Attacker
 *		  aid:   The attack id
 *                dt:    The damagetype
 *		  dam:   The damage in hit points
 */
public void
cr_got_hit(int hid, int ph, object att, int aid, int dt, int dam)
{
    /* We do not tell if we get hit in any special way
    */
}

/*
 * Function name: set_all_attack_unarmed
 * Description:   Set up all unarmed attack basics
 * Arguments:     hit - the hit value
 * 		  pen - the pen value
 */
public nomask void
set_all_attack_unarmed(int hit, int pen)
{
    set_attack_unarmed(W_RIGHT, hit, pen, W_BLUDGEON, 25, "");
    set_attack_unarmed(W_LEFT, hit, pen, W_BLUDGEON, 25, "");
    set_attack_unarmed(W_FOOTR, hit, pen, W_BLUDGEON, 25, "");
    set_attack_unarmed(W_FOOTL, hit, pen, W_BLUDGEON, 25, "");
}

/*
 * Function name: set_all_hitloc_unarmed
 * Description:   Set up all unarmed hit location basics
 * Arguments:     ac - an int or an array to modify the ac....
 */
public nomask void
set_all_hitloc_unarmed(mixed ac)
{
    set_hitloc_unarmed(A_BODY, ac, 45, "korpus");
    set_hitloc_unarmed(A_HEAD, ac, 15, "g^low^e");
    set_hitloc_unarmed(A_LEGS, ac, 20, "nogi");
    set_hitloc_unarmed(A_R_ARM, ac, 10, "lewe rami^e");
    set_hitloc_unarmed(A_L_ARM, ac, 10, "prawe rami^e");
}

/*
 * Function name: update_procuse()
 * Description:   use this to update the percent usage of attacks
 */
public nomask void
update_procuse()
{
    QEXC->cb_modify_procuse();
}
