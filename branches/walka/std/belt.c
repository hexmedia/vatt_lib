/**
 * Standard pasow.
 * Stworzony kiedys przez Ghasha, wzglednie dokonczony
 * przez Gjoefa pod patronatem Very.
 * Dzis mamy 6 maja 2007.
 *
 * Aha, nie poprawialem komentarzy ;)
 */
 
#include <macros.h>
#include <stdproperties.h>
#include <subloc.h>
#include <cmdparse.h>
#include <wa_types.h>

inherit "/std/object";
inherit "/lib/keep";

#define CAP(x)          capitalize(x)

#define ATT_SH          "/std/belt_sh"


static int              max_slots_prz;      /* Ile maksymalnie rzeczy mozna przyczepic */
static int              max_slots_zat;
static int              max_weight_zat;
static int              max_weight_prz;     /* Ile moze wazyc najciezszy przedmiot za pasem */
static int              max_volume_zat;     /* Ile wynosi maksymalna objetosc dla pojed. itemu */
static int              max_volume_prz;

static string           subloc_id;      /* Unikalny sublok */

static object           wearer;         /* Osoba, noszaca przedmiot */

static mapping          ob_map = ([]);  /* Lista obiektow podlaczonych */
//static mapping          ob_map_prz = ([]);

int
query_attachable()
{
    return 1;
}

int
query_attached()
{
    if (!wearer)
        return 0;

    return 1;
}

/**
 *  Ustawia unikalne id dla danego przedmiotu. Kazdy standard,
 *  bazujacy na tym libie musi miec unikalny, wlasne id.
 *  @param id Id subloca 
 */
void
set_subloc_id(string id)
{
    subloc_id = id;
}

string
query_subloc_id()
{
    return subloc_id;
}

/**
 * Ustawia maksymaln� wag�, kt�ra decyduje o tym, jak
 * cie�kie przedmioty mo�emy zatkn�� za pasem.
 * @param w Maksymalna waga
 */
void
set_max_weight_zat(int w)
{
    max_weight_zat = w;
}

/**
 * @return Maksymaln� wag� przedmiotu jaki mo�emy zatkn�� za pasem.
 */
int
query_max_weight_zat()
{
    return max_weight_zat;
}

/**
 * Ustawia maksymaln� wag� przedmiotu jaki mo�emy do pasa przyczepi�.
 * @param w Maksymalna waga
 */
void
set_max_weight_prz(int w)
{
    max_weight_prz = w;
}
/**
 * @return Maksymaln� wag� przedmiotu jaki mo�emy do pasa przyczepi�.
 */
int
query_max_weight_prz()
{
    return max_weight_prz;
}

/**
 * Ustawia maksymaln� obj�to�� przedmiotu, kt�ry mo�emy zatkn�� za pasem.
 * @param v Maksymalna obj�to��
 */
void
set_max_volume_zat(int v)
{
    max_volume_zat = v;
}

/**
 * @return Zwraca maksymaln� obj�to�� przedmiotu, kt�ry mo�emy zatkn�� za pasem
 */
int
query_max_volume_zat()
{
    return max_volume_zat;
}

/**
 * Ustawia maksymaln� obj�to�� przedmiotu, kt�ry mo�e by� do pasa przyczepiony.
 * @param v Maksymalna obj�to��.
 */
void
set_max_volume_prz(int v)
{
    max_volume_prz = v;
}

/**
 * @return Maksymaln� obj�to�� przedmiotu, kt�ry mo�e by� do pasa przyczepiony.
 */
int
query_max_volume_prz()
{
    return max_volume_prz;
}

/**
 * Ustawia maksymaln� ilo�� slot�w na przedmioty przyczepiane do pasa.
 * @param max Maksymalna ilo�� slot�w.
 */
void
set_max_slots_prz(int max)
{
    max_slots_prz = max;
}

/**
 * @return Maksymaln� ilo�� slot�w na przedmioty przyczepiane do pasa..
 */
int
query_max_slots_prz()
{
    return max_slots_prz;
}

/**
 * Ustawia maksymaln� ilo�� slot�w na przedmioty zatykane za pasem.
 * @param max Maksymalna ilo�� slot�w.
 */
void
set_max_slots_zat(int max)
{
    max_slots_zat = max;
}

/**
 * @return Maksymaln� ilo�� slot�w na przedmioty zatykane za pasem.
 */
int
query_max_slots_zat()
{
    return max_slots_zat;
}

void
set_wearer(object ob)
{
    wearer = ob;
}


object
query_wearer()
{
    return wearer;
}


void                                                                        //
att_add_prz(string sloc, object ob)
{
    ob_map[ob] = sloc;
    ob->add_prop("_att_id", subloc_id);
    ob->add_prop(OBJ_I_DONT_INV, 1);
}

void                                                                        //
att_add_zat(string sloc, object ob)
{
    ob_map[ob] = sloc;
    ob->add_prop("_att_id", subloc_id);
    ob->add_prop(OBJ_I_DONT_INV, 1);
}
    

void
att_remove(object ob)
{
    mixed tmp;
        
    tmp = ob_map[ob];
        
    if (objectp(wearer))    
        wearer->remove_subloc(tmp);

    ob_map = m_delete(ob_map, ob);
    
    ob->remove_prop("_att_id");
    ob->remove_prop(OBJ_I_DONT_INV);
}

object
att_query(object ob)
{
    return ob_map[ob];
}

mixed
query_ob_map()
{
    return ob_map;
}

mixed
att_ob_map(int i = 0)
{
    int a;
    object *ret = ({});
    mixed dupa;

    switch (i)
    {
        case 1:
            for (a = 0; a < m_sizeof(ob_map); a++)
            {
                if (wildmatch("*Z", m_values(ob_map)[a]))
                {
                    dupa = m_indices(ob_map)[a];
                    if (objectp(dupa))
                        ret += ({dupa});
                }
            }

            return ret;
            break;

        case 2:
            for (a = 0; a < m_sizeof(ob_map); a++)
            {
                if (wildmatch("*P", m_values(ob_map)[a]))
                    ret += ({m_indices(ob_map)[a]});
            }

            return ret;
            break;

        default:
            return m_indices(ob_map);
            break;
    }
}

void
att_hook_wear(object liv, object ob)
{
    liv->catch_msg("Zak^ladasz "+ob->query_short(PL_BIE)+".\n");
    tell_room(environment(liv), QCIMIE(liv, PL_MIA)+" zak^lada "+
        QSHORT(ob, PL_BIE)+".\n", ({liv}));
}

void
att_hook_remove(object liv, object ob)
{
    liv->catch_msg("Zdejmujesz "+QSHORT(ob, PL_BIE)+".\n");
    tell_room(environment(liv), QCIMIE(liv, PL_MIA)+" zdejmuje "+
        QSHORT(ob, PL_BIE)+".\n", ({liv}));
}

void
att_hook_zatknij(object liv, object ob, object belt)
{
    liv->catch_msg("Zatykasz "+QSHORT(ob, PL_BIE)+" za "+
        QSHORT(belt, PL_BIE)+".\n");
    tell_room(environment(liv), QCIMIE(liv, PL_MIA)+" zatyka "+
        QSHORT(ob, PL_BIE)+" za "+QSHORT(belt, PL_BIE)+".\n",
        ({liv}));
}

void
att_hook_wyjmij(object liv, object ob, object belt)
{
    liv->catch_msg("Wyjmujesz "+QSHORT(ob, PL_BIE)+" zza "+
        QSHORT(belt, PL_DOP)+".\n");
    tell_room(environment(liv), QCIMIE(liv, PL_MIA)+" wyjmuje "+
        QSHORT(ob, PL_BIE)+" zza "+QSHORT(belt, PL_DOP)+".\n",
        ({liv}));
}

void
att_hook_przyczep(object liv, object ob, object belt)
{
    liv->catch_msg("Przyczepiasz "+QSHORT(ob, PL_BIE)+" do "+
        QSHORT(belt, PL_DOP)+".\n");
    tell_room(environment(liv), QCIMIE(liv, PL_MIA)+" przyczepia "+
        QSHORT(ob, PL_BIE)+" do "+QSHORT(belt, PL_DOP)+".\n",
        ({liv}));
}


void
att_hook_odczep(object liv, object ob, object belt)
{
    liv->catch_msg("Odczepiasz "+QSHORT(ob, PL_BIE)+" od "+
        QSHORT(belt, PL_DOP)+".\n");
    tell_room(environment(liv), QCIMIE(liv, PL_MIA)+" odczepia "+
        QSHORT(ob, PL_BIE)+" od "+QSHORT(belt, PL_DOP)+".\n",
        ({liv}));
}

nomask mixed
wear_me(int silent=0)
{
    object sh;

    set_this_player(environment(TO));

    setuid();seteuid(getuid());

    if (TP->query_subloc_obj(subloc_id))
        return "Ju^z masz na sobie pas.\n";

    if (!silent)
        att_hook_wear(TP, TO);

    if (!TP->query_att_ctrl())
    {
        sh = clone_object(ATT_SH);
        if (!sh->shadow_me(TP))
        {
            TP->catch_msg("Uj, nie moge zalozyc shadowa! Zglos to!\n");
            sh->remove_object();
            return 0;
        }
    }

    TP->link_ob(TO, subloc_id);   
    TP->add_subloc(subloc_id, TP);
    TO->move(TP, subloc_id); 

    wearer = TP;
    add_prop(OBJ_I_DONT_INV, 1);
    return 1;
}

mixed
remove_me(int silent=0)
{
    int i;

    if (!wearer)
        return 0;

    if (!silent)
        att_hook_remove(wearer, TO);

    for (i = 0; i < max_slots_zat; i++)
        wearer->remove_subloc(subloc_id+i+"Z");

    for (i = 0; i < max_slots_prz; i++)
        wearer->remove_subloc(subloc_id+i+"P");

    /* Usuwamy shadowa, jezeli nie mamy zadnych
        innych, przyczepionych przedmiotow. */
    if (!sizeof(filter(all_inventory(wearer)-({TO}), &->query_attached())))
        wearer->remove_att_shadow();    

    wearer->remove_subloc(subloc_id);
    wearer = 0;

    m_indexes(ob_map)->remove_prop(OBJ_I_DONT_INV);

    ob_map = ([]);
    remove_prop(OBJ_I_DONT_INV);
    return 1;
}


int
wear_cmd(string str)
{
    mixed val;
    object *arr, ob, sh;

    notify_fail("Za^l^o^z co?\n");

    if(!str)
        return 0;

    arr = PARSE_THIS(str, "%i:"+PL_BIE);

    if (!sizeof(arr))
        return 0;

    ob = arr[0];

    if(!ob->query_attachable())
        return 0;

    val = ob->wear_me();
    return stringp(val) ? notify_fail(val) : 1;
}

int
remove_cmd(string str)
{
    object *arr, ob, sh;

    notify_fail("Zdejmij co?\n");

    if(!str)
        return 0;

    arr = PARSE_THIS(str, "%i:"+PL_BIE);

    if (!sizeof(arr))
        return 0;

    ob = arr[0];

    if (!ob->query_attachable())
        return 0;

    notify_fail("Nie masz " + ob->query_short(1) + " na sobie.\n");
    if (ob->query_subloc() != subloc_id)
        return 0;

    ob->remove_me();

    return 1;
}

int
filter_obs(object ob)
{
    if (!ob->query_subloc())
        return 1;
}


int
filter_attached(object ob, mixed m = calling_function(-3))
{
    string sloc;

    sloc = ob_map[ob];
    if (!sloc)
        return 0;

    switch(m)
    {
        case "wyjmij":
        if (wildmatch(subloc_id+"*Z", sloc))
            return 1;

        case "odczep":
        if (wildmatch(subloc_id+"*P", sloc))
            return 1;

        default:
        if (wildmatch(subloc_id+"*", sloc))
            return 1;
    }
}


int
is_attachable(object ob, int i)
{
    switch(i)
    {
        case 1:
            if(function_exists("create_weapon", ob))
            {
                if(ob->query_wt() == W_KNIFE ||
                    ob->query_wt() == W_SWORD ||
                    ob->query_wt() == W_AXE ||
                    ob->query_wt() == W_CLUB ||
                    ob->query_wt() == W_WARHAMMER ||
                    ob->query_nazwa(0) == "siekiera" ||
                    ob->query_nazwa(0) == "siekierka" ||
                    ob->query_nazwa(0) == "toporek" ||
                    ob->query_nazwa(0) == "mieczyk" ||
                    ob->query_nazwa(0) == "miecz")
                {
                    return 1;
                }
            return 0;
            }
        case 2:
            if(ob->query_prop(OBJ_I_ATTACHABLE))
                return 1;

            return 0;
        default:
            return 1;
    }
}

int
zatknij(string str)
{
    int i, slot;
    mixed tmp;
    object *arr1, *arr2, ob, att;

    notify_fail("Zatknij co gdzie?\n");
    if (!str)
        return 0;

    if (!parse_command(str, all_inventory(TP),
        "%i:"+PL_BIE+" 'za' %i:"+PL_BIE, arr1, arr2))
        return 0;

    arr1 = NORMAL_ACCESS(arr1, "filter_obs", TO);
    arr2 = NORMAL_ACCESS(arr2, 0, 0);

    if (!sizeof(arr1) || !sizeof(arr2))
        return 0;

    notify_fail("Nie mo^zesz zatkn^a^c naraz wi^ecej ni^z jednego przedmiotu.\n");
    if (sizeof(arr1) > 1)
        return 0;

    ob = arr1[0];
    att = arr2[0];

    slot = -1;

    notify_fail("Nie masz na sobie niczego, za co m" + TP->koncowka("^og^l",
    "og^la") + "by^s to zatkn^a^c.\n");
    if (!ob)
        return 0;

    notify_fail("Ale^z masz ju^z " + ob->query_short(3) + " za " + TO->query_short(4) + ".\n");
    if (att_query(ob))
        return 0;

    notify_fail(CAP(ob->short(TP, PL_MIA))+" jest na to zbyt ci^e^zk"+
        ob->koncowka("i","a","ie")+".\n");
    if (ob->query_prop(OBJ_I_WEIGHT) > max_weight_zat ||
        ob->query_prop(CONT_I_WEIGHT) > max_weight_zat)
        return 0;

    notify_fail(CAP(ob->short(TP, PL_MIA))+" jest na to zbyt du^z"+
        ob->koncowka("y","a","e")+".\n");
    if (ob->query_prop(OBJ_I_VOLUME) > max_volume_zat ||
        ob->query_prop(CONT_I_VOLUME) > max_volume_zat)
        return 0;

    notify_fail("Musisz najpierw za^lo^zy^c " + att->query_short(3) + ".\n");
    if (!TP->query_subloc_obj(att->query_subloc_id()))
        return 0;

    notify_fail("Nie mo^zesz zatkn^a^c " + ob->query_short(1) + " za " + att->query_short(3) + ".\n");
    if (!is_attachable(ob, 1))
        return 0;

    tmp = att->query_subloc_id();

    /* Znajduje pierwszy wolny slot */
    for (i = 0; i < att->query_max_slots_zat(); i++)
        if (!TP->query_subloc_obj(tmp+i+"Z"))
        {
            slot = i;
            break;
        }

    notify_fail("Nie uda ci si^e zatkn^a^c ju^z niczego wi^ecej.\n");
    if (slot == -1)
        return 0;

    /* Jezeli ktos chce zabronic przyczepiania czegos */
    if (att->att_prevent_attach(TP, ob))
        return 1;

    /* Jezeli ktos chce zmienic opis przyczepiania */
    if (!ob->att_hook_zatknij(TP, att))
        att_hook_zatknij(TP, ob, att);

    tmp = tmp+slot+"Z";

    att->att_add_zat(tmp, ob);
    TP->add_subloc(tmp, ob);
    ob->move(TP, tmp);
    ob->add_prop("_att_id", att->query_subloc_id());

    return 1;
}


int
wyjmij(string str)
{
    int i;
    mixed tmp;
    object *arr1, *arr2, ob, att;

    notify_fail("Wyjmij co?\n");
    if (!str)
        return 0;

    if (!parse_command(str, all_inventory(TP),
        "%i:"+PL_BIE+" 'zza' %i:"+PL_DOP, arr1, arr2))
        return 0;

    arr1 = NORMAL_ACCESS(arr1, "filter_attached", TO);
    arr2 = NORMAL_ACCESS(arr2, 0, 0);

    if (!sizeof(arr1) || !sizeof(arr2))
        return 0;

    ob = arr1[0];
    att = arr2[0];

    notify_fail("Nie mo^zesz niczego wyj^a^c zza "+
        att->short(TP, PL_DOP)+".\n");
    if (!att->query_attachable())
        return 0;

    notify_fail(CAP(ob->short(TP, PL_MIA))+" nie jest zatkni^et"+ob->koncowka("y","a","e")+" za " + att->query_short(3) + ".\n");
    if (!att_query(ob))
        return 0;

    /* Jezeli ktos chce zmienic opis odczepiania */
    if (!ob->att_hook_wyjmij(TP, att))
        att_hook_wyjmij(TP, ob, att);

    att->att_remove(ob);

    return 1;
}

int
przyczep(string str)
{
    int i, slot;
    mixed tmp;
    object *arr1, *arr2, ob, att;

    notify_fail("Przyczep co gdzie?\n");
    if (!str)
        return 0;

    if (!parse_command(str, all_inventory(TP),
        "%i:"+PL_BIE+" 'do' %i:"+PL_DOP, arr1, arr2))
        return 0;

    arr1 = NORMAL_ACCESS(arr1, "filter_obs", TO);
    arr2 = NORMAL_ACCESS(arr2, 0, 0);

    if (!sizeof(arr1) || !sizeof(arr2))
        return 0;

    notify_fail("Nie mo^zesz przyczepi^c naraz wi^ecej ni^z jednego przedmiotu.\n");
    if (sizeof(arr1) > 1)
        return 0;

    ob = arr1[0];
    att = arr2[0];

    slot = -1;

    notify_fail("Nie masz na sobie niczego, do czego m" + TP->koncowka("^og^lby^s",
    "og^laby^s") + " przyczepi^c " + ob->query_short(3) + ".\n");
    if (!ob)
        return 0;

    notify_fail("Ale^z masz ju^z " + ob->query_short(3) + " przy " + TO->query_short(5) + ".\n");
    if (att_query(ob))
        return 0;

    notify_fail(CAP(ob->short(TP, PL_MIA))+" jest na to zbyt ci^e^zk"+
        ob->koncowka("i","a","ie")+".\n");
    if (ob->query_prop(OBJ_I_WEIGHT) > max_weight_prz ||
        ob->query_prop(CONT_I_WEIGHT) > max_weight_prz)
        return 0;

    notify_fail(CAP(ob->short(TP, PL_MIA))+" jest na to zbyt du^z"+
        ob->koncowka("y","a","e")+".\n");
    if (ob->query_prop(OBJ_I_VOLUME) > max_volume_prz ||
        ob->query_prop(CONT_I_VOLUME) > max_volume_prz)
        return 0;

    notify_fail("Musisz najpierw za^lo^zy^c " + att->query_short(3) + ".\n");
    if (!TP->query_subloc_obj(att->query_subloc_id()))
        return 0;

    notify_fail("Nie mo^zesz przyczepi^c " + ob->query_short(1) + " do " + att->query_short(1) + ".\n");
    if (!is_attachable(ob, 2))
        return 0;


    tmp = att->query_subloc_id();

    /* Znajduje pierwszy wolny slot */
    for (i = 0; i < att->query_max_slots_prz(); i++)
        if (!TP->query_subloc_obj(tmp+i+"P"))
        {
            slot = i;
            break;
        }

    notify_fail("Nie uda ci si^e przyczepi^c ju^z niczego wi^ecej.\n");
    if (slot == -1)
        return 0;

    /* Jezeli ktos chce zabronic przyczepiania czegos */
    if (att->att_prevent_attach(TP, ob))
        return 1;

    /* Jezeli ktos chce zmienic opis przyczepiania */
    if (!ob->att_hook_przyczep(TP, att))
        att_hook_przyczep(TP, ob, att);

    tmp = tmp+slot+"P";

    att->att_add_prz(tmp, ob);
    TP->add_subloc(tmp, ob);
    ob->move(TP, tmp);
    ob->add_prop("_att_id", att->query_subloc_id());

    return 1;
}

int
odczep(string str)
{
    int i;
    mixed tmp;
    object *arr1, *arr2, ob, att;

    notify_fail("Odczep co?\n");
    if (!str)
        return 0;

    if (!parse_command(str, all_inventory(TP),
        "%i:"+PL_BIE+" 'od' %i:"+PL_DOP, arr1, arr2))
        return 0;

    arr1 = NORMAL_ACCESS(arr1, "filter_attached", TO);
    arr2 = NORMAL_ACCESS(arr2, 0, 0);

    if (!sizeof(arr1) || !sizeof(arr2))
        return 0;

    ob = arr1[0];
    att = arr2[0];

    notify_fail("Nie mo^zesz niczego odczepi^c od "+
        att->short(TP, PL_DOP)+".\n");
    if (!att->query_attachable())
        return 0;

    notify_fail(CAP(ob->short(TP, PL_MIA))+" nie jest przyczepion"+ob->koncowka("y","a","e")+" do " + att->query_short(1) + ".\n");
    if (!att_query(ob))
        return 0;

    /* Jezeli ktos chce zmienic opis odczepiania */
    if (!ob->att_hook_odczep(TP, att))
        att_hook_odczep(TP, ob, att);

    att->att_remove(ob);

    return 1;
}

void
dobadz(string str)
{
    object *was, das;

    if (!str || str=="")
    {
        notify_fail("Dob^ad^x czego?\n");
        return;
    }

    if (parse_command(str, all_inventory(TP),
        "%i:"+PL_DOP, was))
    {
        was = NORMAL_ACCESS(was, 0, 0);
        das = was[0];

        if (member_array(das, m_indices(ob_map)) != -1)
            att_hook_wyjmij(TP, das, TO);

        TO->att_remove(das);
    }
}

void
chwyc(string str)
{
    object *was, das;

    if (!str || str=="")
    {
        notify_fail("Chwy^c co?\n");
        return;
    }

    if (parse_command(str, all_inventory(TP),
        "%i:"+PL_BIE, was))
    {
        was = NORMAL_ACCESS(was, 0, 0);
        das = was[0];

        if (member_array(das, m_indices(ob_map)) != -1)
            att_hook_wyjmij(TP, das, TO);
        TO->att_remove(das);
    }
}

void
init_attach_lib()
{
    add_action(&zatknij(), "zatknij");
    add_action(&wyjmij(), "wyjmij");

    add_action(&przyczep(), "przyczep");
    add_action(&odczep(), "odczep");

    add_action(&dobadz(), "dob^a^d^x");
    add_action(&chwyc(), "chwy^c");

	if(ENV(TO) != wearer)
    remove_me();
}

/*
*  Function name: init_wear_cmds
*  Description  : Ustawia dodakowe komendy "zaloz" i "zdejmij", jezeli
*                 nie chcemy korzystac z soulowych odpowiednikow.
*/
void
init_wear_cmds()
{
    add_action(&wear_cmd(), "za^l^o^z");
    add_action(&remove_cmd(), "zdejmij");
}


/*
*  Function name: att_remove_object
*  Description  : Funkcja ta musi byc w kazdym remove_object, dziedziczacym ten
*                 lib. 
*/
void
att_remove_object()
{
    remove_me();
}


string
att_on_desc(object on, object att, string zat, string prz, object *ob_list=({}))
{
    string str;

    str = "W biodrach spina ci^e "+att->short(att, PL_MIA);

    if (strlen(zat))
    {
        str += " z zatkni^etym za^n "+zat;

        if (strlen(prz))
            str += ", przy nim za^s nosisz " + prz;
    }
    else if (strlen(prz))
        str += ", przy kt^orym nosisz " + prz;

    return str + ".\n";
}


string
att_fob_desc(object on, object att, string zat, string prz, object *ob_list=({}))
{
    string str;

    str = "W biodrach spina "+ wearer->koncowka("go", "j^a", "je") + " " +
        att->short(att, PL_MIA);

    if (strlen(zat))
    {
        str += " z zatkni^etym za^n "+zat;

        if (strlen(prz))
            str += ", przy nim za^s nosi " + prz;
    }
    else if (strlen(prz))
        str += ", przy kt^orym nosi " + prz;

    return str + ".\n";
}

///////////////////////////////////////////////////////////////////////////////
int
query_belt()
{
    return 1;
}

void
create_belt()
{
    ustaw_nazwe("pas");
    dodaj_przym("gotowy", "gotowi");
}

nomask void
create_object()
{
    set_long("Pass!\n");

    add_name("belt_id");

    add_prop(OBJ_I_WEIGHT, 1000);
    add_prop(OBJ_I_VOLUME, 500);

    set_max_slots_zat(3);
    set_max_slots_prz(3);
    set_max_weight_zat(2500);
    set_max_weight_prz(800);
    set_max_volume_zat(2300);
    set_max_volume_prz(1200);
    set_subloc_id("_pas_subloc");

    create_belt();
}

int
help(string str)
{
    object ob;

    notify_fail("Nie ma pomocy na ten temat.\n");

    if (!str) return 0;

    if (!parse_command(str, this_player(), "%o:" + PL_MIA, ob))
        return 0;

    if (!ob) return 0;

    if (ob != this_object())
        return 0;

        write("Maj^ac pas, mo^zesz przyczepi^c do niego sakiewk^e lub zatkn^a^c " +
        "za^n bro^n.\n");
    return 1;
}

void
init()
{
    ::init();
    init_attach_lib();

    add_action("help", "?", 2);
}

void
remove_object()
{
    att_remove_object();
    ::remove_object();
}

void
init_arg(string arg)
{
    init_keep_arg(arg);
}

string
query_auto_load()
{
    return MASTER+":"+query_keep_auto_load();
}