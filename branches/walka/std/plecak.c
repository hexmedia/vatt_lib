/*
 * /doc/examples/pojemniki/plecak.c
 *
 * Plecak z zabezpieczeniem przed przypadkowym sprzedaniem.
 *
 * /Mercade@Genesis, 24 January 1993
 * Mozliwosc dziedziczenia - Olorin@Genesis
 *
 * Globalne zmiany i przerobienie (zaimki itp.) by Tigr@Arkadia (Sep 1998)
 * Pare poprawek... Garagoth (nie wiem kiedy)
 *
 *           Par� poprawek i przystosowanie go do Vatt'gherna - Lil. 19.09.2005
 */

#pragma save_binary
#pragma strict_types

inherit "/std/receptacle";
inherit "/lib/keep";

#include <cmdparse.h>
#include <composite.h>
#include <language.h>
#include <macros.h>
#include <ss_types.h>
#include <stdproperties.h>

#define NO_SELL_VBFC "@@no_sell_vbfc@@"
#define PLECAK_SUBLOC  "_plecak"
#define ENTO environment(this_object())

int fillpack(string str);
int emptypack(string str);

int worn;
string  fill_verb = "nape�nij",
        empty_verb = "opr�nij",
        backpack_long;

void
set_fill_verb(string s)
{
    fill_verb = s;
}
void
set_empty_verb(string s)
{
    empty_verb = s;
}
string
query_backpack_long()
{
    return backpack_long;
}
void
set_backpack_long(string s)
{
    backpack_long = s;
}

void
create_backpack()
{
    dodaj_przym("stary", "starzy");
}

nomask void
create_container()
{
    set_long("Du�y plecak wykonany ze �wi�skiej sk�ry, kt�ra " +
        "jest bardzo wytrzyma�ym materia�em. ");
    
    ustaw_nazwe("plecak");
    set_keep(1);

    add_prop(OBJ_I_VALUE, 700);
    add_prop(CONT_I_CLOSED, 1);
    add_prop(CONT_I_WEIGHT, 1000);
    add_prop(CONT_I_VOLUME, 2000);
    add_prop(CONT_I_MAX_WEIGHT, 50000);
    add_prop(CONT_I_MAX_VOLUME, 50000);
    add_prop(CONT_I_REDUCE_VOLUME, 125);

    add_prop(OBJ_M_NO_SELL, NO_SELL_VBFC);
    remove_prop(CONT_I_RIGID);

    create_backpack();
}


int
wear_me(int cicho)
{
  object kto = this_player();
  if (!present(this_object(), kto))
  {
    notify_fail("Przecie� nie masz " + query_nazwa(PL_DOP) + " !\n");
    return 0;
  }

  if (worn)
  {
    write("Ju� masz " + koncowka("go", "j�", "go", "ich", "je")
        + " na plecach.\n");
    return 1;
  }

  if (member_array(PLECAK_SUBLOC, kto->query_sublocs()) > -1)
  {
    write("Niestety, masz ju� na plecach co� innego.\n");
    return 1;
  }

  kto->add_subloc(PLECAK_SUBLOC, this_object());
  obj_subloc = PLECAK_SUBLOC;
  add_prop(OBJ_I_DONT_INV,1);
  if (!cicho) {
    write("Zak�adasz na plecy " + short(kto, PL_BIE) + ".\n");
    saybb(QCIMIE(kto, PL_MIA) + " zak�ada na plecy " +
        QSHORT(this_object(), PL_BIE) + ".\n");
  }

  TP->set_obiekty_zaimkow(({ TO }));
  worn = 1;
  return 1;
}


int
czy_to_plecak(object ob)
{
    return ob->query_plecak();
}

int
remove_me(int cicho = 0)
{
  object kto = this_player();
  if (!worn || !objectp(kto))
    return 0;

  kto->remove_subloc(PLECAK_SUBLOC);
  obj_subloc = 0;
  remove_prop(OBJ_I_DONT_INV);
  if (!cicho) {
    kto->catch_msg("Zdejmujesz " + short(kto, PL_BIE) + ".\n");
    saybb(QCIMIE(kto, PL_MIA) + " zdejmuje " + QSHORT(this_object(), PL_BIE) +
        ".\n", kto);
  }

  TP->set_obiekty_zaimkow(({ TO }));
  worn = 0;
  move(kto);
  return 1;
}

void
leave_env(object old, object dest)
{
    if (worn)
    {
        old->remove_subloc(PLECAK_SUBLOC);
        worn = 0;
        old->catch_msg("Zdejmujesz " + short(old, PL_BIE) + ".\n");

        saybb(QCIMIE(old, PL_MIA) + " zdejmuje " +
            QSHORT(this_object(), PL_BIE) + ".\n", old);
    }
    ::leave_env(old, dest);
}

public string
show_subloc(string subloc, object on, object for_obj)
{
    string data;

    if (subloc == PLECAK_SUBLOC)
    {
        if (for_obj != on)
        data = "Nosi na plecach " + this_object()->short(for_obj, PL_BIE) +
            ".\n";
        else
            data = "Nosisz na plecach " + this_object()->short(on, PL_BIE) +
                ".\n";

        return data;
    }
}

mixed
no_sell_vbfc()
{
    if (worn)
    {
            return capitalize(short(this_player(), PL_MIA)) + " jest za�o�on" +
            koncowka("y", "a", "e", "i", "e") + "!\n";
    }
    if (sizeof(all_inventory(this_object())))
    {
            return capitalize(short(this_player(), PL_MIA)) + " nie jest pust"+
            koncowka("y", "a", "e", "i", "e") + ".\n";
    }
    return 0;
}

public int
pomoc(string str)
{
    object plecaczek;
    string long_desc = query_backpack_long();
    string add_desc;
   // string add_desc;

    notify_fail("Nie ma pomocy na ten temat.\n");
    if (!stringp(str))
        return 0;

    if (!parse_command(str, this_player(), "%o:" + PL_MIA, plecaczek))
        return 0;

    if (plecaczek != this_object())
        return 0;


    

    if (ENTO != this_player())
        return long_desc + "\n";

    add_desc = "Je�li pomy�lisz '" + fill_verb
        + ((worn) ? "" : " <co>") + "', w�o�ysz wszystkie "
        + "rzeczy kt�rych nie masz na sobie, z wyj�tkiem pieni�dzy "
        + "i pojemnik�w, do " + query_nazwa(PL_DOP) + ". Je�li "
        + "natomiast pomy�lisz '" + empty_verb
        + ((worn) ? "" : " <co>")
        + "', wyci�gniesz je z powrotem.";
    if (query_keep())
    {
        add_desc += " W tej chwili, " + query_nazwa() + " jest pod ochrona, "
            + " dzi�ki czemu nie sprzedasz "
            + koncowka("go", "jej", "go", "ich", "ich") + ", "
            + "spieni�aj�c swoje �upy. Pomy�l <odbezpiecz "
            + query_nazwa(PL_BIE) + "> aby zdj�� to zabezpieczenie.";
    }
    else
    {
        add_desc += " Sprzedaj�c ekwipunek, mo�esz tak�e sprzeda� "
             + query_nazwa() + ". Aby si� przed tym uchroni�, pomy�l "
             + "<zabezpiecz " + query_nazwa(PL_BIE) + ">.";
    }
    
    write(add_desc+"\n");	
	
	return 1;
}

void
init()
{
    ::init();
    add_action(fillpack, fill_verb);
    add_action(emptypack, empty_verb);
    add_action(pomoc, "?",2);
}

int
prevent_enter(object ob)
{
    if (living(ob))
    {
            write(capitalize(ob->short(this_player(), PL_DOP)) +
            " nie da si� w�o�y� do " + query_nazwa(PL_DOP) + "!\n");
            return 1;
    }
    return ::prevent_enter(ob);
}

int
a_filter(object ob)
{
    if (living(ob))
        return 0;
    if (ob->query_prop(OBJ_I_INVIS))
        return 0;
    if (function_exists("create_skora", ob) != 0)
        return 1;
    /* skory mozna wlozyc */
    if (!(ob->query_prop(OBJ_I_VALUE)))
        return 0;
    if (ob->query_prop(OBJ_M_NO_SELL))
        return 0;
    if (function_exists("create_torch", ob) != 0 && ob->query_lit(-1))
        return 0;
    if (function_exists("create_lamp", ob) != 0)
        return 0;
    if (ob->query_prop(OBJ_M_NO_GIVE))
        return 0;
    if (ob->query_prop(OBJ_M_NO_DROP))
        return 0;
    if (ob->query_prop(CONT_I_IN))
        return 0;
    if (ob->query_worn())
        return 0;
    if (ob->query_wielded())
        return 0;
    if (function_exists("create_beczulka", ob))
        return 0;
    if (function_exists("create_rock", ob))
         return 0;
    if (ob->query_prop(LIVE_I_IS))
        return 0;
    if (ob->query_prop(OBJ_I_HAS_FIRE))
        return 0;

    return 1;
}

int
fill_me(object kto)
{
    object      *conts = ({}),
            *moved = ({});
    int x;

    if (this_object()->query_prop(CONT_I_CLOSED))
    {
        kto->catch_msg("Mo�e p�jdzie ci �atwiej, je�li najpierw otworzysz " +
            query_nazwa(PL_BIE) + "?\n");
         return 1;
    }

    conts = filter(all_inventory(kto), "a_filter", this_object());
    for (x = 0; x < sizeof(conts); x++)
    {
        if (!conts[x]->move(this_object()))
        {
            moved += ({ conts[x] });            /*successful move */
        }
    }

    if (!sizeof(moved))
    {
        kto->catch_msg("Nie chowasz nic do " + query_nazwa(PL_DOP) + ".\n");
        return 1;
    }
    else
    {
        kto->catch_msg("Chowasz " + COMPOSITE_DEAD(moved, PL_BIE) +
            " do " + short(kto, PL_DOP) + ".\n");

        saybb(QCIMIE(kto, PL_MIA) + " chowa " +
            QCOMPDEAD(PL_BIE) + " do " + QSHORT(this_object(), PL_DOP) +
            ".\n", kto);
    }
    return 1;
}

int
fillpack(string str)
{
    mixed plecaki;
    int ret_val;

    notify_fail(capitalize(fill_verb) + " co <czym>?\n"); /*to (czym) jest dla napeln. lamp olejem*/

    if (!strlen(str))
        return
    (this_player()->query_subloc_obj(PLECAK_SUBLOC))->fill_me(this_player());
    parse_command(str, all_inventory(this_player()), "%i:" + PL_BIE, plecaki);
    plecaki = NORMAL_ACCESS(plecaki, "czy_to_plecak", this_object());

    if (!sizeof(plecaki))
        return 0;

    if (sizeof(plecaki) > 1)
        return 0;

    if (this_object() != plecaki[0])
        return 0;

    if (ret_val = (plecaki[0]->fill_me(this_player())))
        this_player()->set_obiekty_zaimkow( ({ plecaki[0] }) );
    return ret_val;
}

int
empty_me(object kto)
{
    object      *conts = ({}),
                *moved = ({});
    int x;

    if (this_object()->query_prop(CONT_I_CLOSED))
    {
        kto->catch_msg("Mo�e p�jdzie ci �atwiej, je�li najpierw otworzysz " +
            query_nazwa(PL_BIE) + "?\n");
        return 1;
    }

    if (!sizeof(all_inventory(this_object())))
    {
        kto->catch_msg("Przecie� " + query_nazwa(PL_MIA) + " jest pust" +
            koncowka("y", "a", "e", "i", "e") + "!\n");
        return 1;
    }

    conts = filter(all_inventory(this_object()), "a_filter", this_object());
    for (x = 0; x < sizeof(conts); x++)
    {
        conts[x]->move(kto);
        if (present(conts[x], kto))
        {
            moved += ({ conts[x] });
        }
    }
    if (sizeof(moved) == 0)
        kto->catch_msg("Nie wyci�gasz nic z " + query_nazwa(PL_DOP) + ".\n");
    else
    {
        kto->catch_msg("Wyci�gasz " + COMPOSITE_DEAD(moved, PL_BIE) +
            " z " + short(kto, PL_DOP) + ".\n");

        saybb(QCIMIE(kto, PL_MIA) + " wyci�ga " +
            QCOMPDEAD(PL_BIE) + " z " + QSHORT(this_object(), PL_DOP) +
            ".\n", kto);
    }
    return 1;
}

int
emptypack(string str)
{
    mixed plecaki;
    int ret_val;

    notify_fail(capitalize(empty_verb) + " co?\n");

    if (!strlen(str))
        return
    (this_player()->query_subloc_obj(PLECAK_SUBLOC))->empty_me(this_player());

    parse_command(str, all_inventory(this_player()), "%i:" + PL_BIE, plecaki);
    plecaki = NORMAL_ACCESS(plecaki, "czy_to_plecak", this_object());

    if (!sizeof(plecaki))
        return 0;
    if (sizeof(plecaki) > 1)
        return 0;

    if (this_object() != plecaki[0])
        return 0;

    if (ret_val = (plecaki[0]->empty_me(this_player())))
        this_player()->set_obiekty_zaimkow( ({ plecaki[0] }) );
    return ret_val;
}

int
czy_to_forsa(object ob)
{
    if (!function_exists("create_coins", ob))
        return 0;
    return 1;
}

int
jaki_worn()
{
    return worn;
}

int
query_plecak()
{
    return 1;
}
