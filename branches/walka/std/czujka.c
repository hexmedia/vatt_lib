/* Jest to czujka rzucana pod nogi wrogom, informuje o jego przemieszczaniu sie.
 * Uzywana np. przez lucznikow celujacych do kogos na inna lokacje. :P
 * Vera
 */
inherit "/std/object";
#include <macros.h>
#include <pl.h>
#include <stdproperties.h>
object cel, ob;

create_object()
{
    ustaw_nazwe( ({ "czujka", "czujki", "czujce", "czujk^e",
        "czujk^a", "czujce" }), ({ "czujki", "czujek",
        "czujkom", "czujki", "czujkami", "czujkach" }) ,
        PL_ZENSKI);
    dodaj_przym("^luczniczy","^luczniczy");
    add_prop(OBJ_M_NO_GET, "We^x co?\n");
    add_prop(OBJ_M_NO_GIVE, "Daj co komu?\n");
    add_prop(OBJ_M_NO_STEAL, 1);
    add_prop(OBJ_M_NO_BUY, 1);
    set_no_show(1);
}
int
ustaw_obiekty_na(object ob1, object cel1)
{
    ob1=ob;
    cel1=cel;
    return 1;
}

int
signal_leave(object kto, object dokad)
{
    string gdzie;
    int i;
    for(i=0;i<sizeof(environment(TO)->query_exit_rooms());i++)
    {
        if(environment(TO)->query_exit_rooms()[i] == dokad)
            gdzie=environment(TO)->query_exit_cmds()[i];
    }
    if(kto==cel)
    {
        find_player(ob->query_name())->write("CEL SPIERDALA NA "+gdzie+"!!\n");
    }
    return 1;
}
