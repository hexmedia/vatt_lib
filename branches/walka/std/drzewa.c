/* Pliczek odpowiedzialny za dodawanie i odnawianie
   drzew na lokacji - by Rantaur
   
   Achutng! Achtung!
   Zeby drzewka sie w ogole klonowaly i odnawialy
   w create_room'ie trzeba wywolac funkcje init_drzewa()
*/

#include <macros.h>
#include <mudtime.h>

#define TREES_DIR "/d/Standard/drzewa/"

string *lista_drzew;
int liczba_drzew=random(5)+3;

void odnow_drzewa();
void ustaw_liste_drzew(string *s);

// Ustawia liczbe drzew na lokacji
void ustaw_liczbe_drzew(int i)
{
	liczba_drzew = i;
}

int query_liczba_drzew()
{
	return liczba_drzew;
}

/* Ustawia liste drzew jakie moga rosnac na lokacji;
   podajemy nazwy drzew - "buk", "brzoza" itp. */
void ustaw_liste_drzew(string *s)
{
	lista_drzew = s;
}

string *query_lista_drzew()
{
	return lista_drzew;
}

int
filter_uncuted_trees(object ob)
{
    if(function_exists("create_object", ob) == "/std/drzewo")
	{
		if(ob->query_sciete())
			return 0;
		
		return 1;
	}

    return 0;
}

int
filter_cuted_trees(object ob)
{
    if(function_exists("create_object", ob) == "/std/drzewo")
	{
		if(ob->query_sciete())
			return 1;
		
		return 0;
	}

    return 0;
}

// Zwraca sciezke do drzewa o nazwie podanej w argumencie
string query_tree_path(string str)
{
	string ret = TREES_DIR;
	ret += lower_case(str);
	ret += "/"+lower_case(str)+".c";
	
	return ret;
}

// Odnawia drzewa na lokacji
void odnow_drzewa()
{  
	if(!sizeof(lista_drzew))
		return;
	
	object *inv = all_inventory(this_object());
	object *drzewa = filter(inv, &filter_uncuted_trees());
	object *drzewa_sciete = filter(inv, &filter_cuted_trees());
	
	int do_dodania=liczba_drzew-sizeof(drzewa);
	int lista_size = sizeof(lista_drzew); 
	
	string sciezka;
	object drzewo;
	for(int i=0;i < do_dodania;i++)
	{
		sciezka = query_tree_path(lista_drzew[random(lista_size)]);
		drzewo = clone_object(sciezka);
		drzewo->move(this_object());
	}
	
	if(sizeof(drzewa_sciete) > 0)
	{
	    drzewa_sciete->move("/d/Standard/Redania/Rinde_okolice/Las/lokacje/tartak_magazyn");
	    drzewa_sciete->move(TO);
	}
}

int
set_alarm_every_hour(string fun)
{
    set_alarm((59.0 - itof(MT_MINUTA)) * 60.0 + (65.0 - itof(MT_SEKUNDA)),
	3600.0, fun);
}

void 
init_drzewa()
{
    set_alarm(2.0, 0.0, &odnow_drzewa());
    set_alarm_every_hour("odnow_drzewa");
}
