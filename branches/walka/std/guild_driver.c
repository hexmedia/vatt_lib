/**
 * Silnik gildii
 *
 * Autor: Krun
 * 2007-05-06
 */

inherit "/std/room.c";

#include <pl.h>
#include <files.h>
#include <macros.h>
#include <composite.h>


mapping czlonkowie = ([]);
    // ([czas wstapienia, ranga, dod_title, specjalne])
mapping odeszli = ([]);
    // ([czas wstapienia, czas odejscia])
mapping wyrzuceni = ([]);
    // ([czas wstapienia, czas wyrzucenia])

string *tytuly;
string *tytuly_specjalne;
string *krotkie_nazwy;
string nazwa_gildii;
string opiekun;
string soul_file;

#define CZAS_WSTAPIENIA       0
#define RANGA                 1
#define DOD_TYTUL             2
#define SPECJALNE             3

#define CZAS_WYRZUCENIA       1
#define CZAS_ODEJSCIA         1

#define MIN_RANK_OPIEKUNA     4

#define GUILD_LOG_FILE       "/d/Standard/log/GILDIE/" + implode(explode(nazwa_gildii, " "), "_")


void
create_guild_driver()
{
}

void sprawdz_opiekuna();
void notify_all_wizards(object ob, string str);
void write_log_file(string str);

void
create_room()
{
    create_guild_driver();

    if(!nazwa_gildii)
        notify_all_wizards(TO, "Gildia nie ma ustawionej nazwy!!!!");

    setuid();
    seteuid(getuid());

    if(opiekun)
        sprawdz_opiekuna();
    else
        notify_all_wizards(TO, "Gildia nie ma ustawionego opiekuna!!!!");
    set_short("Silnik gildii: " + nazwa_gildii);
    set_long("Narazie bez longa, to zedytuje p�niej.\n");
}

void
reset_guild_driver()
{
}

void
reset_room()
{
    reset_guild_driver();
}

void
zapisz(int krok=0)
{
    if(!krok)
    {
        set_alarm(1.0, 0.0, &zapisz(1));
        return;
    }

    setuid();
    seteuid(getuid());

    string plik_zapisu = MASTER;

    if(file_size(plik_zapisu +".o") == -1)
        write_log_file("Tworze plik zapisu: " + plik_zapisu + ".o");

    save_object(plik_zapisu);
}

void
ustaw_plik_soula(string path)
{
    if(path[-2..-1] != ".c")
        path += ".c";

    if(file_size(path) == -1)
        return;

    if(soul_file != path)
    {
        write_log_file("Zmieniono plik soula z '" + soul_file + "' na '" + path + "'.\n");
        soul_file = path;
        zapisz();
    }
}

string
query_plik_soula()
{
    return soul_file;
}

void
ustaw_nazwe_gildii(string n)
{
    nazwa_gildii = n;
}

string
query_nazwa_gildii()
{
    return nazwa_gildii;
}

void
ustaw_opiekuna(string o)
{
    if(SECURITY->query_wiz_rank(o) >= MIN_RANK_OPIEKUNA)
    {
        if(opiekun != o)
        {
            write_log_file("Zmieniono opiekuna gildii z " + UC(opiekun) + " na " + UC(o));
            opiekun = o;
        }
        zapisz();
        return ;
    }
    notify_all_wizards(TO, "Opiekunem gildii nie mo�e by� gracz o randze < " + MIN_RANK_OPIEKUNA + ".\n");
}

string
query_opiekun()
{
    return opiekun;
}

void
ustaw_tytuly(string *t)
{
    if(!compare_arr(tytuly, t))
    {
        write_log_file("Zmieniono tytu�y w gildii.\n Stare to:\n" + 
            "\t " + COMPOSITE_WORDS(tytuly) + "\n Nowe:\n" + 
            "\t " + COMPOSITE_WORDS(t) + "\n");
         tytuly = t;
        zapisz();
    }
}

string *
query_tytuly()
{
    return tytuly;
}

int
query_tytul(string t)
{
    if(member_array(t, tytuly) >= 0)
        return 1;
    else if(member_array(lower_case(t), map(tytuly, lower_case)) >= 0)
        return 2;
}


void
ustaw_tytuly_specjalne(string *t)
{
    if(!compare_arr(tytuly_specjalne, t))
    {
        write_log_file("Zmieniono tytu�y specjalne gildii.\n Stare to:\n" +
            "\t " + COMPOSITE_WORDS(tytuly_specjalne) + "\n Nowe:\n" + 
            "\t " + COMPOSITE_WORDS(t) + "\n");
        tytuly_specjalne = t;
        zapisz();
    }
}

string *
query_tytuly_specjalne()
{
    return tytuly_specjalne;
}

int
query_tytul_specjalny(string t)
{
    if(member_array(t, tytuly_specjalne) >= 0)
        return 1;
    else if(member_array(lower_case(t), map(tytuly_specjalne, lower_case)) >= 0)
        return 2;
}

static private void
daj_soula_czlonkowi(object player)
{
    if(!interactive(player))
        return;
    player->add_cmdsoul(soul_file);
    player->update_hooks();
}

void
ustaw_krotkie_nazwy(string *nazwy)
{
    if(!compare_arr(nazwy, krotkie_nazwy))
    {
        write_log_file("Zmieniono kr�tkie nazwy gildii.\n Stare to:\n" + 
            "\t " + COMPOSITE_WORDS(krotkie_nazwy) + "\n Nowe:\n" + 
            "\t " + COMPOSITE_WORDS(nazwy) + "\n");
        krotkie_nazwy = nazwy;
        zapisz();
    }
}

string *
query_krotkie_nazwy()
{
    return krotkie_nazwy;
}

int
dodaj_czlonka(mixed nc)
{
    if(!stringp(nc))
    {
        if(objectp(nc))
            nc = nc->query_real_name();
        else
            return 0;	
    }

    if(is_mapping_index(nc, czlonkowie))
        return -1;

    if(is_mapping_index(nc, wyrzuceni))
        wyrzuceni = m_delete(wyrzuceni, nc);
    if(is_mapping_index(nc, odeszli))
        odeszli = m_delete(odeszli, nc);

    czlonkowie[nc] = allocate(4);

    czlonkowie[nc][CZAS_WSTAPIENIA] = time();
    czlonkowie[nc][RANGA] = 1;
    czlonkowie[nc][DOD_TYTUL] = 0;
    czlonkowie[nc][SPECJALNE] = ({});

    if(is_mapping_index(nc, czlonkowie))
    {
        write_log_file(UC(nc) + " do��czy� do gildii.\n");
        find_player(nc)->dodaj_gildie(MASTER_OB(TO));
        daj_soula_czlonkowi(find_player(nc));
        zapisz();
        return 1;
    }
}

int
usun_czlonka(mixed nc, int flag)
{
    if(!stringp(nc))
    {
        if(objectp(nc))
            nc = nc->query_real_name();
        else
            return 0;
    }

    if(!is_mapping_index(nc, czlonkowie))
        return -1;

    switch (flag)
    {
        case 1: 
            wyrzuceni[nc] = allocate(2);
            wyrzuceni[nc][CZAS_WSTAPIENIA] = czlonkowie[nc][CZAS_WSTAPIENIA];
            wyrzuceni[nc][CZAS_WYRZUCENIA] = time();
            break;
        case 2:
            odeszli[nc] = allocate(2);
            odeszli[nc][CZAS_WSTAPIENIA] = czlonkowie[nc][CZAS_WSTAPIENIA];
            odeszli[nc][CZAS_ODEJSCIA]   = time();
            break;
    }

    if(czlonkowie = m_delete(czlonkowie, nc))
    {
        write_log_file(UC(nc) + " " + (flag == 1 ? "zosta� wyrzucony" : (flag == 2 ? "odszed�" :
            "zosta� cicho wyrzucony(systemowo!)")) + " z gildii.\n");
        find_player(nc)->usun_gildie(MASTER_OB(TO));
        zapisz();
        return 1;
    }
}

int
query_czlonek(mixed nc)
{
    if(!stringp(nc))
    {
        if(objectp(nc))
            nc = nc->query_real_name();
        else
            return 0;
    }

    return is_mapping_index(nc, czlonkowie);
}

int
query_wyrzucony(mixed nc)
{
    if(!stringp(nc))
    {
        if(objectp(nc))
            nc = nc->query_real_name();
        else
            return 0;
    }

    return is_mapping_index(nc, wyrzuceni);
}

int
query_odeszly(mixed nc)
{
    if(!stringp(nc))
    {
        if(objectp(nc))
            nc = nc->query_real_name();
        else
            return 0;
    }

    return is_mapping_index(nc, odeszli);
}

string
query_tytul_czlonka(mixed nc)
{
    if(!stringp(nc))
    {
        if(objectp(nc))
            nc = nc->query_real_name();
        else
            return 0;
    }

    return tytuly[czlonkowie[nc][RANGA]-1] + 
        (czlonkowie[nc][DOD_TYTUL] ? ", " + tytuly_specjalne[czlonkowie[nc][DOD_TYTUL]-1] : "");
}

mapping
query_czlonkowie()
{
    return czlonkowie;
}

mapping
query_wyrzuceni()
{
    return wyrzuceni;
}

mapping
query_odeszli()
{
    return odeszli;
}

int
ustaw_range_czlonka(mixed nc, int r)
{
    if(!stringp(nc))
    {
        if(objectp(nc))
            nc = nc->query_real_name();
        else
            return 0;
    }

    if(!is_mapping_index(nc, czlonkowie))
        return -1;

    if(czlonkowie[nc][RANGA] != r)
    {
        write_log_file(UC(nc) + " zosta� " + (czlonkowie[nc][RANGA] > r ? "zdegradowany do " +
            r + " poziomu" : "awansowany na poziom " + r) + " z " + czlonkowie[nc][RANGA] +
            "poziomu.");
        czlonkowie[nc][RANGA] = r;
    }
    return 1;
}

int
ustaw_specjalne_czlonka(mixed nc, mixed spec)
{
    if(!stringp(nc))
    {
        if(objectp(nc))
            nc = nc->query_real_name();
        else
            return 0;
    }

    if(!is_mapping_index(nc, czlonkowie))
        return -1;

    czlonkowie[nc][SPECJALNE] = spec;
    zapisz();
    return 1;
}

mixed 
query_ar_czlonka(mixed nc)
{
    if(!stringp(nc))
    {
        if(objectp(nc))
            nc = nc->query_real_name();
        else
            return 0;
    }

    if(!is_mapping_index(nc, czlonkowie))
        return -1;

    return czlonkowie[nc];
}

int
query_ranga_czlonka(mixed nc)
{
    mixed arr = query_ar_czlonka(nc);

    if(sizeof(arr))
        return arr[RANGA];
}

int
query_dod_tytul_czlonka(mixed nc)
{    
    mixed arr = query_ar_czlonka(nc);

    if(sizeof(arr))
        return arr[DOD_TYTUL];
}
   
int
query_czas_wstapienia_czlonka(mixed nc)
{    
    mixed arr = query_ar_czlonka(nc);

    if(sizeof(arr))
        return arr[CZAS_WSTAPIENIA];
}

mixed
query_specjalne_czlonka(mixed nc)
{   
    mixed arr = query_ar_czlonka(nc);

    if(sizeof(arr))
        return arr[SPECJALNE];
}

mixed 
query_ar_wyrzuconego(mixed nc)
{
    if(!stringp(nc))
    {
        if(objectp(nc))
            nc = nc->query_real_name();
        else
            return 0;
    }

    if(!is_mapping_index(nc, wyrzuceni))
        return -1;

    return wyrzuceni[nc];
}

int
query_czas_wstapienia_wyrzuconego(mixed nc)
{
    mixed arr = query_ar_wyrzuconego(nc);

    if(sizeof(arr))
        return arr[CZAS_WSTAPIENIA];
}

int
query_czas_wyrzucenia_wyrzuconego(mixed nc)
{
    mixed arr = query_ar_wyrzuconego(nc);

    if(sizeof(arr))
        return arr[CZAS_WYRZUCENIA];
}

mixed 
query_ar_odeszlego(mixed nc)
{
    if(!stringp(nc))
    {
        if(objectp(nc))
            nc = nc->query_real_name();
        else
            return 0;
    }

    if(!is_mapping_index(nc, odeszli))
        return -1;

    return odeszli[nc];
}

int
query_czas_wstapienia_odeszlego(mixed nc)
{    
    mixed arr = query_ar_odeszlego(nc);

    if(sizeof(arr))
        return arr[CZAS_WSTAPIENIA];
}

int
query_czas_odejscia_odeszlego(mixed nc)
{

    mixed arr = query_ar_odeszlego(nc);

    if(sizeof(arr))
        return arr[CZAS_ODEJSCIA];
}

private void
sprawdz_opiekuna()
{
    if(SECURITY->query_wiz_rank(opiekun) < MIN_RANK_OPIEKUNA)
        notify_all_wizards(TO, UC(opiekun) + " nie mo�e by� ju� opiekunem tej gildii. "+
            "prawdopodobnie zosta� zdemotowany, lub zmieni�y si� wymagania co do "+
	        "opiekun�w gildii.\n");
}

private static void
notify_all_wizards(object ob, string str)
{
    if(MASTER != "/std/guild_driver")
        notify_wizards(ob, str);
}

void
write_log_file(string str)
{
    setuid(); 
    seteuid(getuid()); 
    write_file(GUILD_LOG_FILE, ctime(time()) + ":" + str + "\n");
}
