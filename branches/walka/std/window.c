/* 
 * window.c
 *
 * Standardowe okno nie ma zamkow, wazy 60 kilo, i ma objetosc
 * 80 litrow. Standardowa wysokosc okna wynosi 1 metr.
 *
 *
 *  Trzeba by�o troch� przerobi�...Teraz mo�na mie� okna obustronne
 *  jak i jednostronne.
 *  Obustronne: Wymagane s� 2 obiekty okien - ka�dy na ka�d� stron�
 *               (czyli identycznie jak z drzwiami). Mo�na wyj��
 *               przez nie, jak i wej�� z powrotem.
 *  Jednostronne: Jest tylko jeden obiekt okna. I tylko na jednej
 *                lokacji. Po wyskoczeniu nie mo�na ju� wr�ci�
 *                przez okno, bo po drugiej stronie nie ma takiego
 *                obiektu. Jest to przydatne do pi�ter i innych
 *                wysoko�ci.
 *
 * Nale�y te� pami�ta� o tym, by, gdy okno jest "zewn�trz" deklarowa�
 * w nim: set_outer(1);
 *
 * Vera.
 */
#pragma save_binary
#pragma strict_types

inherit "/std/object";
#include <stdproperties.h>
#include <macros.h>
#include <ss_types.h>
#include <cmdparse.h>
#include <language.h>

string	other_room,	/* The name of the other side of the window */
	window_id,	/* A unique (?) id of the window */
    	window_desc,	/* A description of the window */
	lock_desc,	/* A description of the lock */
    	open_desc,	/* The description of the open window */
	closed_desc,	/* The description of the closed window */
	pass_mess,	/* tekst przy wychodzeniu */
	fail_pass,	/* The fail pass message */
	fail_close,	/* The fail close message */
	fail_unlock;	/* The fail unlock message */

string	*open_mess,	/* The open messages */
	*fail_open,	/* The fail open messages */
	*close_mess,	/* The close messages */
	*lock_mess,	/* The lock messages */
	*fail_lock,	/* The fail lock messages */
	*unlock_mess;	/* The unlock messages */

mixed	
//	window_name,	/* The name(s) of the window */
	lock_name,	/* The name(s) of the lock */
    	key;		/* The key for opening locks */

object	klucz,		/* Obiekt klucza, do komunikatow */
        other_window; /*czasem jest(parter), a czasem go nie ma(z wysokosci) */

string	*pass_commands,		/* The commands used to enter the window */
	*open_commands,		/* The commands used to open the window */
	*close_commands,	/* The commands used to close the window */
	*lock_commands,		/* The commands used to lock the window */
	*unlock_commands;	/* The commands used to unlock the window */

string	pass_function;

int	is_outer;

int	open_status,		/* If the window is open or not */
	lock_status,		/* If the window is locked or not */
	no_pick,		/* If the window is possible to pick */
	pick,			/* How hard is the lock to pick? */
	open_str;		/* Strength needed to open window */


/* 
 * Some prototypes 
 */
void create_window();
void set_lock_desc(string desc);
void set_lock_mess(string *mess);
void set_fail_lock(mixed mess);
void set_unlock_mess(string *mess);
void set_fail_unlock(string mess);
void set_other_room(string name);
void set_window_id(string id);
// void set_window_name(mixed name);
void set_window_desc(string desc);
void set_open_desc(string desc);
void set_closed_desc(string desc);
void set_pass_command(mixed command);
void set_pass_mess(string mess);
void set_pass_function(string fun);
void set_fail_pass(string mess);
void set_open_command(mixed command);
void set_open_mess(string *mess);
void set_fail_open(mixed mess);
void set_close_command(mixed command);
void set_close_mess(string *mess);
void set_fail_close(string mess);
static void remove_window_info(object dest);
void do_open_window(string mess);
void do_close_window(string mess);
void do_lock_window(string mess);
void do_unlock_window(string mess);
int  lock_procedure(string arg);
void set_open(int i);
void set_locked(int i);
void do_set_key(mixed keyval);

void set_other_window();

/*
 * Function name: create_object
 * Description:   Initialize object.
 */
void
create_object()
{
//    pass_commands = ({ "wyjd^x", "wejd^x", "wyskocz" });
    open_commands = ({});
    close_commands = ({});
    pick = 100;
    add_prop(OBJ_I_WEIGHT, 60000);
    add_prop(OBJ_I_VOLUME, 80000);
    add_prop(DOOR_I_HEIGHT, 100); /* Standardowa wysokosc 1 metr. */
    add_prop(OBJ_I_NO_GET, 1);
    set_no_show();

    /*
     * Default messages.
     */
    set_open_desc("@@standard_open_desc");
    set_closed_desc("@@standard_closed_desc");

    set_fail_pass("@@standard_fail_pass");
    
    set_open_command("otw^orz");
    set_open_mess(({"@@standard_open_mess1", "@@standard_open_mess2",
        "@@standard_open_mess3" }));
    set_fail_open(({"@@standard_fail_open1", "@@standard_fail_open2" }));
    
    set_close_command("zamknij");
    set_close_mess(({"@@standard_close_mess1", "@@standard_close_mess2",
        "@@standard_close_mess3" }));
    set_fail_close("@@standard_fail_close");
    
    set_window_desc("Wygl^ada mocno.\n");
    set_lock_desc("Zwyk^ly zamek.\n");
    
    set_lock_mess(({"@@standard_lock_mess1", "@@standard_lock_mess2", 
        "@@standard_lock_mess3" }));
    set_fail_lock(({"@@standard_fail_lock1", "@@standard_fail_lock2"}));
    
    set_unlock_mess(({"@@standard_unlock_mess1", "@@standard_unlock_mess2",
        "@@standard_unlock_mess3" }));
    set_fail_unlock("@@standard_fail_unlock");
    
    set_pass_mess("@@standard_pass_mess");
    set_open(1);
    set_locked(0);

    /* If you want to have a lock on the window, and be able to both lock it
       and unlock it you have to add these functions yourself. Don't do it
       here since all windows would have locks then... */

    /* You have to define the following yourself:
       (see docs in the beginning of this file)

             set_other_room()
             set_window_id()
             set_pass_command()
    */

    create_window();

    set_alarm(1.0,0.0,"set_other_window");
}

/*
 * Function name: create_window
 * Description:   Sets default names and id
 */
void
create_window()
{
}

/*
 * Function name: reset_window
 * Description:   Reset the window
 */
void
reset_window() 
{
}

/*
 * Function name: reset_object
 * Description:   Reset the object
 */
nomask void
reset_object()
{
    reset_window();
}

int pass_window(string str);
int open_window(string str);
int close_window(string str);
int lock_window(string str);
int unlock_window(string str);
int komenda_okno(string str);
// int pick_lock(string str);

/*
 * Function name: init
 * Description:   Initalize the window actions
 */
void
init()
{
    int i;

    ::init();
//    for (i = 0 ; i < sizeof(pass_commands) ; i++)
//	add_action(pass_window, check_call(pass_commands[i]));
    //-----komendy----------
    if (is_outer)
	add_action(pass_window, "wejd^x");
    else
    {
	add_action(pass_window, "wyjd^x");
	add_action(pass_window, "wyskocz");
    }
    add_action(komenda_okno,"okno");
    /*if(sizeof(pass_commands))
    {
        int i;
        for(i=0;i<sizeof(pass_commands);i++)
           add_action(pass_window, pass_commands[i]);
    }
    else
       add_action(pass_window, pass_commands);*/
    //----------------------

    for (i = 0 ; i < sizeof(open_commands) ; i++)
	add_action(open_window, check_call(open_commands[i]));

    for (i = 0 ; i < sizeof(close_commands) ; i++)
	add_action(close_window, check_call(close_commands[i]));
	
    for (i = 0 ; i < sizeof(lock_commands) ; i++)
	add_action(lock_window, check_call(lock_commands[i]));

    for (i = 0 ; i < sizeof(unlock_commands) ; i++)
	add_action(unlock_window, check_call(unlock_commands[i]));

/*
    if (!no_pick && sizeof(unlock_commands))
	add_action(pick_lock, "pick");
*/
}

/*
 * Function name: komenda_okno
 * Description:   Taka malutka funkcja, dzieki ktorej komenda "okno"
 *                dziala tak samo jak np. komenda "drzwi" lub "wyjscie"
 */
int
komenda_okno(string str)
{
    if(str)
        return 0;

    pass_window("przez okno");
    return 1;
}


/*
 * Function name: pass_window
 * Description:   Pass the window.
 * Arguments:	  arg - arguments given
 */
int
pass_window(string arg)
{
    object window;
    int dexh;


    if (!arg ||
	!parse_command(arg, environment(this_player()), " 'przez' %o:" + PL_BIE, window) ||
	window != this_object())
    {
	notify_fail(capitalize(query_verb()) + " przez co?\n");
	return 0;
    }

    /*
	The times higher a player can be and still get through
    */
    dexh = 2 + (this_player()->query_stat(SS_DEX) / 25);

    if (open_status)
    {
	/* Lets say we arbitrarily can bend as dexh indicates.
	   For something else, inherit and redefine.
	 */
	if ((int)this_player()->query_prop(CONT_I_HEIGHT) > 
			query_prop(DOOR_I_HEIGHT) * dexh) 
	{
	    write("Jeste^s zbyt du^z" + 
	        this_player()->koncowka("y", "a", "e") + 
	        " i za ma^lo zr^eczn" + 
	        this_player()->koncowka("y", "a", "e") +
	        ", ^zeby si^e przecisn^a^c przez " + short(PL_BIE));
	    return 1;
	}
	else if ((int)this_player()->query_prop(CONT_I_HEIGHT) > 
			query_prop(DOOR_I_HEIGHT))
	{
	    write("Z wielkim trudem przeciskasz si^e przez " + short(PL_BIE) + 
	        ".\n");
	    saybb(QCIMIE(this_player(), PL_MIA) + " z wielkim trudem "
	        + "przeciska si^e przez " + this_object()->short(PL_BIE)
	        + ".\n");
	}
	else
	{
	    write("Wyskakujesz przez " + short(PL_BIE) + ".\n");
	    saybb(QCIMIE(this_player(), PL_MIA) + " wyskakuje przez " +
		this_object()->short(PL_BIE) + ".\n");
	}

	this_player()->add_prop(LIVE_I_SILENT_MOVE, 1);
	this_player()->move_living(check_call(pass_mess), other_room);
	this_player()->remove_prop(LIVE_I_SILENT_MOVE);
	saybb(QCIMIE(this_player(), PL_MIA) + " wyskakuje z otwartego okna " +
	    "i l^aduje nieopodal.\n");
	if (pass_function) call_self(pass_function);
    }
    else
	write(check_call(fail_pass));

    return 1;
}

/*
 * Function name: open_window
 * Description:   Open the window.
 * Arguments:	  arg - arguments given
 */
int
open_window(string arg)
{
    mixed okno_ob;
    
    notify_fail("Co chcesz otworzy^c?\n");
    
    if (!stringp(arg))
        return 0;
            
    if (!parse_command(arg, environment(this_object()), "%i:3", okno_ob))
        return 0;

    okno_ob = CMDPARSE_STD->normal_access(okno_ob, 0, this_object(), 1);
    
    if (!okno_ob || !sizeof(okno_ob) || okno_ob[0] != this_object())
        return 0;

    if (!open_status)
    {
        if (lock_status)
	    write(check_call(fail_open[1]));
	else if (this_player()->query_stat(SS_STR) < open_str)
//	    write("You lack the strength needed.\n");
            write(check_call(fail_open[1]));
	else
	{
	    write(check_call(open_mess[2]));
	    saybb(QCIMIE(this_player(), PL_MIA) + " "
	        + check_call(open_mess[0]));
	    do_open_window("");

            if(other_window)
                other_window->do_open_window(check_call(open_mess[1]));
	}
    }
    else
	write(check_call(fail_open[0]));

    return 1;
}

void
do_open_window(string mess)
{
    object env;

    env = environment(this_object());
    env->change_my_desc(check_call(open_desc));
    if (strlen(mess))
	tell_roombb(env, mess);
    open_status = 1;
}

/*
 * Function name: close_window
 * Description:   Close the window.
 * Arguments:	  arg - arguments given
 */
int
close_window(string arg)
{
    mixed okno_ob;
    
    notify_fail("Co chcesz zamkn^a^c?\n");
    
    if (!stringp(arg))
        return 0;
            
    if (!parse_command(arg, environment(this_object()), "%i:3", okno_ob))
        return 0;

    okno_ob = CMDPARSE_STD->normal_access(okno_ob, 0, this_object(), 1);
    
    if (!okno_ob || !sizeof(okno_ob) || okno_ob[0] != this_object())
        return 0;

    if (open_status)
    {
	if (this_player()->query_stat(SS_STR) < open_str)
	{
            write(capitalize(short(PL_MIA)) + " nie chc" +
                (query_tylko_mn() ? "^a" : "e") + " si^e ruszy^c.\n");
	}
	else
	{
	    write(check_call(close_mess[2]));
	    saybb(QCIMIE(this_player(), PL_MIA) + " " +
		check_call(close_mess[0]));
	    do_close_window("");
            if(other_window)
                other_window->do_close_window(check_call(close_mess[1]));
	}
    }
    else
    {
        notify_fail(check_call(fail_close));
        return 0;
    }

    return 1;
}

void
do_close_window(string mess)
{
    object env;

    env = environment(this_object());
    env->change_my_desc(check_call(closed_desc));
    if (strlen(mess))
	tell_roombb(env, mess);
    open_status = 0;
}

/*
 * Function name: lock_window
 * Description:   Lock the window.
 * Arguments:	  arg - arguments given
 */
int
lock_window(string arg)
{
    int ret;
    
    if (!lock_status)
    {
	if (open_status)
	{
	    notify_fail(check_call(fail_lock[1]));
	    return 0;
	}
	else
	{
            if (!(ret = lock_procedure(arg)))
                return 0;
        	
            if (ret == 2)
                return 1; /* lock_procedure wyswietla wlasny blad */
                
	    write(check_call(lock_mess[2]));
	    saybb(QCIMIE(this_player(), PL_MIA) + " " +
		check_call(lock_mess[0]));
	    do_lock_window("");

            if(other_window)
                other_window->do_lock_window(check_call(lock_mess[1]));
	}
    }
    else
    {
	notify_fail(check_call(fail_lock[0]));
	return 0;
    }

    return 1;
}

void
do_lock_window(string mess)
{
    if (strlen(mess))
	tell_roombb(environment(this_object()), mess);
    lock_status = 1;
}

/*
 * Function name: unlock_window
 * Description:   Unlock the window.
 * Arguments:	  arg - arguments given
 */
int
unlock_window(string arg)
{
    int ret;
    
    if (lock_status)
    {
        if (!(ret = lock_procedure(arg)))
    	return 0;
    	
        if (ret == 2)
            return 1; /* lock_procedure wyswietla wlasny blad */
	    
	write(check_call(unlock_mess[2]));
	saybb(QCIMIE(this_player(), PL_MIA) + " "
	    + check_call(unlock_mess[0]));
	do_unlock_window("");
        if(other_window)
            other_window->do_unlock_window(check_call(unlock_mess[1]));
    }
    else
    {
	notify_fail(check_call(fail_unlock));
	return 0;
    }

    return 1;
}

void
do_unlock_window(string mess)
{
    if (strlen(mess))
	tell_roombb(environment(this_object()), mess);
    lock_status = 0;
}

int
sprawdz_klucz(object ob)
{
    return (environment(ob) == this_player());
}

/*
 * Nazwa funkcji: lock_procedure
 * Opis:          Funkcja jest wywolywana w celu sprawdzenia, czy
 *		  zamek moze byc otwarty/zamkniety.
 *
 *  Istnieja dwa warianty:
 *
 *  1: Nie ma klucza. Drzwi odblokowuje sie "<komenda> <okno>".
 *  2: Jest klucz. Otwiera sie przez "<komenda> <okno> <kluczem>".
 *
 * Argumenty:  arg - wszystko to, co zostalo napisane przez gracza po komendzie
 * Zwraca:       1 - Ju�, 0 - Nie mozna, 2 - Nie mozna, ale lock_procedure
 *		   wyswietla wlasny komunikat bledu
 */
int
lock_procedure(string arg)
{
    int use_key;
    mixed okno_ob, klucz_ob;
    
    notify_fail(capitalize(query_verb()) + " co?\n");

    if (!strlen(arg))
        return 0;

    use_key = query_prop(DOOR_I_KEY);
    
    if (parse_command(arg, environment(this_object()), 
        "%i:" + PL_BIE + " %i:" + PL_NAR, okno_ob, klucz_ob))
    {
        notify_fail(capitalize(query_verb()) + " co? Albo mo^ze czym^s?\n");
        
        okno_ob = CMDPARSE_STD->normal_access(okno_ob, 0, this_object(), 1);
        if (!okno_ob || !sizeof(okno_ob) || okno_ob[0] != this_object())
            return 0;
            
        klucz_ob = CMDPARSE_STD->normal_access(klucz_ob, "sprawdz_klucz", 
            this_object());
            
        if (!klucz_ob || !sizeof(klucz_ob))
        {
            write(capitalize(query_verb()) + " " + 
                okno_ob[0]->short(PL_BIE) + " czym?\n");
            return 2;
        }

        if (!use_key)
        {
            write("W " + short(PL_MIE) + " nie widzisz dziurki od klucza.\n");
            return 2;
        }
        
        if ((mixed)klucz_ob[0]->query_key() == key)
        {
            klucz = klucz_ob[0];
            return 1;
        }
               
        write(capitalize(klucz_ob[0]->short(PL_MIA)) + " nie pasuje.\n");
        return 2;
    }
    
    if (use_key)
        return 0;

    if (!parse_command(arg, environment(this_object()), "%i:3", okno_ob))
        return 0;

    okno_ob = CMDPARSE_STD->normal_access(okno_ob, 0, this_object(), 1);

    if (!okno_ob || !sizeof(okno_ob) || okno_ob[0] != this_object())
    {
        notify_fail(capitalize(query_verb()) + " co?\n");
        return 0;
    }
    
    return 1;
}

#if 0
/*
 * Function name: do_pick_lock
 * Description:   Here we pick the lock, redefine this function if you want
 *		  something to happen, like a trap or something.
 * Arguments:	  skill - randomized picking skill of player
 *		  pick  - how difficult to pick the lock
 */
void
do_pick_lock(int skill, int pick)
{
    if (skill > pick)
    {
	write("Uda^lo si^e! Od zamka dochodzi ci^e satysfakcjonuj^ace " + 
	   "klikni^ecie.\n");
	say("S^lyszysz ciche klikni^ecie dochodz^ace od zamka.\n");
	do_unlock_window("");
        if(other_window)
            other_window->do_unlock_window(check_call(unlock_mess[1]));
    } 
    else 
    if (skill < (pick - 50))
	write("You failed to pick the lock. It seems unpickable to you.\n");
    else
	write("You failed to pick the lock.\n");
}

/*
 * Function name: pick_lock
 * Description:   Pick the lock of the window.
 * Arguments:	  str - arguments given
 */
int
pick_lock(string str)
{
    int skill;
    string arg;

    notify_fail("Pick lock on what?\n");
    if (!str)
	return 0;
    if (sscanf(str, "lock on %s", arg) != 1)
	arg = str;
    notify_fail("No " + arg + " here.\n");
    if (member_array(arg, window_name) < 0)
	return 0;

    if (!lock_status)
    {
	write("Much to your surprise, you find it unlocked already.\n");
	return 1;
    }

    if (this_player()->query_mana() < 10)
    {
        write("You can't concetrate enough to pick the lock.\n");
	return 1;
    }

    this_player()->add_mana(-10); /* Cost 10 mana to try to pick a lock.*/
    write("You try to pick the lock.\n");
    say(QCTNAME(this_player()) + " tries to pick the lock.\n");
    skill = random(this_player()->query_skill(SS_OPEN_LOCK));

    do_pick_lock(skill, pick);

    return 1;      
}
#endif 0

/*
 * Function name: set_str
 * Description:   Set the strength needed to open/close the window
 * Arguments:     str - strength needed
 */
void
set_str(int str) { open_str = str; }

/*
 * Function name: set_no_pick
 * Description:   Make sure the lock of the window is not pickable
 */
void
set_no_pick() { no_pick = 1; }

/*
 * Function name: query_no_pick
 * Description:	  Return 1 if window not pickable
 */
int
query_no_pick()	{ return no_pick; }

/*
 * Function name: set_pick
 * Description:   Set the difficulty to pick the lock, 100 impossible, 10 easy
 */
void
set_pick(int i)	{ pick = i; }

/*
 * Function name: query_pick
 * Description:	  Returns how easy it is to pick the lock on a window
 */
int
query_pick() { return pick; }

/*
 * Function name: set_open
 * Description:   Set the open staus of the window
 */
void
set_open(int i)	{ open_status = i; }

/*
 * Function name: query_open
 * Description:   Query the open status of the window.
 */
int
query_open() { return open_status; }

#if 0
/*
 * Function name: set_window_name
 * Description:	  Set the name of the window
 */
void
set_window_name(mixed name) 
{ 
  if (pointerp(name))
      window_name = name;
  else 
      window_name = ({ name });
}
#endif

/*
 * Function name: set_window_desc
 * Description:   Set the long description of the window
 */
void
set_window_desc(string desc) { window_desc = desc; }

/*
 * Function name: query_window_desc
 * Description:   Query the long description of the window
 */
string
query_window_desc() { return window_desc; }

/*
 * Function name: set_open_desc
 * Description:   Set the description of the window when open
 */
void
set_open_desc(string desc) { open_desc = desc; }

/*
 * Function name: query_open_desc
 * Description:   Query the open description of the window
 */
string
query_open_desc() { return open_desc; }

/*
 * Function name: set_closed_desc
 * Description:   Set the description of the window when closed
 */
void
set_closed_desc(string desc) { closed_desc = desc; }

/*
 * Function name: query_closed_desc
 * Description:   Query the description of the window when closed
 */
string
query_closed_desc() { return closed_desc; }


public string
convert_exit_desc_to_polish(string x)
{
    switch (plain_string(x)) {
	case "n":
	case "polnoc": return "p^o^lnoc";
	case "ne":
	case "polnocny-wschod": return "p^o^lnocny-wsch^od";
	case "e":
	case "wschod": return "wsch^od";
	case "se":
	case "poludniowy-wschod": return "po^ludniowy-wsch^od";
	case "s":
	case "poludnie": return "po^ludnie";
	case "sw":
	case "poludniowy-zachod": return "po^ludniowy-zach^od";
	case "w":
	case "zachod": return "zach^od";
	case "nw":
	case "polnocny-zachod": return "p^o^lnocny-zach^od";
	case "d":
	case "dol": return "d^o^l";
	case "u":
	case "gora": return "g^ora";
	default: return x;
    }
}

/*
 * Function name: set_pass_command
 * Description:   Set which command is needed to pass the window
 */
void
set_pass_command(mixed command)
{
    if(pointerp(command))
    {
         //pass_commands = command;
         if(pointerp(command[0]))
        {
            pass_commands = command[0];
            int i;
            for(i=0;i<sizeof(command[0]);i++)
                pass_commands[i] = convert_exit_desc_to_polish(pass_commands[i]);
        }
        else
        {
            pass_commands = command;
	    pass_commands[0] = convert_exit_desc_to_polish(pass_commands[0]);
        }
    }
    else // przeksztalcamy standardowe nazwy wyjsc na POLSKIE /d
        pass_commands = map(pass_commands, convert_exit_desc_to_polish);
        //pass_commands = convert_exit_desc_to_polish(command);
        //pass_commands = ({ command });
}

void
set_pass_mess(string mess)
{
    pass_mess = mess;
}

void
set_pass_function(string fun)
{
    pass_function = fun;
}

/*
 * Function name: query_pass_command
 * Description:   Query what command lets you pass the window
 */
string *
query_pass_command() { return pass_commands; }

/*
 * Function name: set_fail_pass
 * Description:   Set messaged when failing to pass the window.
 */
void
set_fail_pass(string mess) { fail_pass = mess; }

/*
 * Function name: query_fail_pass
 * Description:   Query message when failing to pass the window
 */
string
query_fail_pass()		{ return fail_pass; }

/*
 * Function name: set_open_command
 * Description:   Set command to open the window
 */
void
set_open_command(mixed command)
{
    if (pointerp(command))
	open_commands = command;
    else
	open_commands = ({ command });
}

/*
 * Function name: query_open_command
 * Description:   Query what command opens the window
 */
string *
query_open_command() { return open_commands; }

/*
 * Function name: set_open_mess
 * Description:   Set the message to appear when window opens
 */
void
set_open_mess(string *mess) { open_mess = mess; }

/*
 * Function name: query_open_mess
 * Description:   Query what messages we get when dor is opened
 */
string *query_open_mess() { return open_mess; }

/*
 * Function name: set_fail_open
 * Description:   Set the message when we fail to open window
 */
void	
set_fail_open(mixed mess)	
{ 
    if (pointerp(mess))
	fail_open = mess;
    else
	fail_open = ({ mess });
}

/*
 * Function name: query_fail_open
 * Description:   Query message when open fails
 */
string	*
query_fail_open() { return fail_open; }

/*
 * Function name: set_close_command
 * Description:   Set what command closes the window
 */
void
set_close_command(mixed command)
{
    if (pointerp(command))
	close_commands = command;
    else
	close_commands = ({ command });
}

/*
 * Function name: query_close_command
 * Description:   Query what command closes the window
 */
string *
query_close_command() { return close_commands; }

/*
 * Function name: set_close_mess
 * Description:   Set the message to appear when we close the window
 */
void
set_close_mess(string *mess) { close_mess = mess; }

/*
 * Function name: query_close_mess
 * Description:   Query message when we close the window
 */
string *query_close_mess()		{ return close_mess; }

/*
 * Function name: set_fail_close
 * Description:   Set message when we fail to close the window
 */
void
set_fail_close(string mess) { fail_close = mess; }

/*
 * Function name: query_fail_close
 * Description:   Query message when we fail to close the window
 */
string
query_fail_close() { return fail_close; }

/*
 * Function name: set_locked
 * Description:   Set lock status
 */
void
set_locked(int i) { lock_status = i; }

/*
 * Function name: query_locked
 * Description:   Query lock status
 */
int
query_locked() { return lock_status; }

/*
 * Function name: set_other_room
 * Description:   Set which rooms is on the other side
 */
void
set_other_room(string name) { other_room = name; }

/*
 * Function name: query_other_room
 * Description:   Query what room is on the other side
 */
string
query_other_room() { return other_room; }

/*
 * Function name: set_lock_name 
 * Description:   Set name of the lock
 */
void
set_lock_name(mixed name) { lock_name = name; }

/*
 * Function name: query_lock_name
 * Description:   Query the name of the lock
 */
mixed
query_lock_name() { return lock_name; }

/*
 * Function name: set_lock_desc
 * Description:   Set the description of the lock
 */
void
set_lock_desc(string desc) { lock_desc = desc; }

/*
 * Function name: query_lockdesc
 * Description:   Query the description of the lock
 */
string
query_lock_desc() { return lock_desc; }

/*
 * Function name: set_locl_command
 * Description:   Set which command locks the window
 */
void
set_lock_command(mixed command)
{
    if (pointerp(command))
	lock_commands = command;
    else
	lock_commands = ({ command });
}

/*
 * Function name: query_lock_command
 * Description:   Query what command locks the window
 */
string *
query_lock_command() { return lock_commands; }

/*
 * Function name: set_lock_mess
 * Description:   Set message when locking window
 */
void
set_lock_mess(string *mess) { lock_mess = mess; }

/*
 * Function name: query_lock_mess
 * Description:   Query the message when locking window
 */
string *
query_lock_mess() { return lock_mess; }

/*
 * Function name: set_fail_lock
 * Description:   Set message when fail to lock the window
 */
void	
set_fail_lock(mixed mess)	
{ 
    if (pointerp(mess))
	fail_lock = mess; 
    else
    	fail_lock = ({ mess });
}

/*
 * Function name: query_fail_lock
 * Description:   Query message when lock fails
 */
string	*
query_fail_lock() { return fail_lock; }

/*
 * Function name: set_unlock_command
 * Description:   Set what command unlocks the window
 */
void
set_unlock_command(mixed command)
{
    if (pointerp(command))
	unlock_commands = command;
    else
	unlock_commands = ({ command });
}

/*
 * Function name: query_unlock_command
 * Description:   Query what command unlocks the window
 */
string *
query_unlock_command() { return unlock_commands; }

/*
 * Function name: set_unlock_mess
 * Description:   Set message when unlocking window
 */
void
set_unlock_mess(string *mess) { unlock_mess = mess; }

/*
 * Function name: query_unlock_mess
 * Description:   Query message when unlocking window
 */
string *
query_unlock_mess() { return unlock_mess; }

/*
 * Function name: set_fail_unlock
 * Description:   Set fail message when unlocking
 */
void
set_fail_unlock(string mess) { fail_unlock = mess; }

/*
 * Function name: query_fail_unlock
 * Description:   Query message when failing to unlock window
 */
string
query_fail_unlock() { return fail_unlock; }

/*
 * Function name: set_key
 * Description:   Set the number of the key that fits
 */
void	
set_key(mixed keyval)		
{ 
    do_set_key(keyval);
}

/*
 * Function name: do_set_key
 * Description:   Called from the other side
 */
void
do_set_key(mixed keyval)
{
    key = keyval;
    add_prop(DOOR_I_KEY, 1);
}

/*
 * Function name: query_key
 * Description:   Query what key that fits.
 */
mixed
query_key() { return key; }

/*
 * Nazwa funkcji : query_used_key
 * Opis          : Zwraca obiekt klucza, ktorym gracz sie wlasnie posluzyl
 *		   do otwarcia/zamkniecia okna na klucz. Stworzone
 *		   z mysla o komunikatach otwierania/zamykania.
 * Funkcja zwraca: Wskaznik obiektu klucza.
 */
object
query_used_key()
{
    return klucz;
}

/*
 * Function name: set_window_id
 * Description:   Set the id of the window
 */
void	
set_window_id(string id) 		
{ 
    id = "--" + id + "--";
    window_id = id; 
//    ustaw_shorty(({ id, id, id, id, id, id }) , PL_ZENSKI);
}

void
set_outer(int i = 1)
{
    is_outer = i;
}

/*
 * Function name: query_window_id
 * Description:   Query the id of the window
 */
string
query_window_id()	{ return window_id; }

/*
 * Function name: query_other_window
 * Description:   Blablabla
 */
object
query_other_window()
{
    if (other_window)
	return other_window;
    else
        return 0;
}

/*
 * Function name: set_other_window
 * Description:   SET THE OTHER WINDOW YOU FUK. :P Stupid function, ut
 *                NECCESSARY BITCH!
 * Arguments:     No arguments! :D
 */
void
set_other_window()
{
    string *window_ids;
    object *windows;
    int pos;
    //object window;

    seteuid(getuid());

    /*
     * No other side or already loaded.
     */
    if (!strlen(other_room))// || other_door)
	return;

    /*
     * Try to load the other side.
     */
    if (!find_object(other_room))
    {
	other_room->teleledningsanka();
	if (!find_object(other_room))
	{
	    write("B^l^ad w ^ladowaniu drugiej strony okna: " + other_room + ".\n");
	    remove_window_info(environment(this_object()));
	    remove_object();
	    return;
	}
    }

    window_ids = (string *)other_room->query_prop(ROOM_AS_DOORID);
    windows = (object *)other_room->query_prop(ROOM_AO_DOOROB);
    pos = member_array(window_id, window_ids);
    if (pos < 0)
    {   //MA NIE BY�! :P Tzn. mo�e nie by� :P
	/*write("Po drugiej stronie, po za^ladowaniu pokoju nie ma okna: " + 
		other_room + ".\n");
	remove_window_info(environment(this_object()));
	remove_object();*/
	return;
    }

    other_window = windows[pos];
    //other_window = find_object(windows[pos]);
}

/*
 * Function name: add_window_info
 * Description:   Add information about this window to the room it
 *		  stands in. If this window already exists, autodestruct.
 * Arguments:	  dest - The room that contains the window.
 */
static void
add_window_info(object dest)
{
    string *window_ids;
    object *windows;

    window_ids = (string *)dest->query_prop(ROOM_AS_DOORID);
    windows = (object *)dest->query_prop(ROOM_AO_DOOROB);
    if (!pointerp(window_ids))
    {
	window_ids = ({});
	windows = ({});
    }
    
    /* 
     * One window of the same type is enough.
     */
    if (member_array(window_id, window_ids) >= 0)
    {
	write("Jedno okno wystarczy.\n");
	remove_object();
	return;
    }

    window_ids += ({ window_id });
    windows += ({ this_object() });

    dest->add_prop(ROOM_AS_DOORID, window_ids);
    dest->add_prop(ROOM_AO_DOOROB, windows);
}

/*
 * Function name: remove_window_info
 * Description:   Remove information about this window from the room it
 *		  stands in.
 * Arguments:	  dest - The room that contains the window.
 */
static void
remove_window_info(object dest)
{
    string *window_ids;
    object *windows;
    int pos;

    window_ids = (string *)dest->query_prop(ROOM_AS_DOORID);
    windows = (object *)dest->query_prop(ROOM_AO_DOOROB);
    if (!sizeof(window_ids))
	return;

    pos = member_array(window_id, window_ids);
    if (pos < 0)
	return;

    window_ids = exclude_array(window_ids, pos, pos);
    windows = exclude_array(windows, pos, pos);

    dest->add_prop(ROOM_AS_DOORID, window_ids);
    dest->add_prop(ROOM_AO_DOOROB, windows);
}

/*
 * Function name: enter_env
 * Description:   The window enters a room, activate it.
 * Arguments:	  dest - The destination room,
 * 		  old - Where it came from
 */
void
enter_env(object dest, object old)
{
    string *itemy;
    int x;
    
    add_window_info(dest); 
    if (open_status)
        dest->change_my_desc(check_call(open_desc), this_object());
    else
        dest->change_my_desc(check_call(closed_desc), this_object());
    if (strlen(window_desc))
    {
        itemy = ({ query_nazwa(PL_BIE) });
        
        
        for (x = 0; x < sizeof(query_przym(1, PL_BIE)); x++)
        {
            itemy = itemy + ({ query_przym(1, PL_BIE)[x] + " " +
                query_nazwa(PL_BIE) });
        }
	dest->add_item(itemy, window_desc);
    }
    if (strlen(lock_desc) && lock_name) 
	dest->add_item(lock_name, lock_desc);
}

/*
 * Function name: leave_env
 * Description:   The window leaves a room, remove it.
 * Arguments:     old - Where it came from,
 * 		  dest - The destination room
 */
void
leave_env(object old, object dest)
{
    if (!old)
	return;

    old->remove_my_desc(this_object());
    if (strlen(window_desc))
	old->remove_item(query_nazwa(PL_BIE), window_desc);
    if (strlen(lock_desc) && lock_name) 
    {
	if (pointerp(lock_name))
	{
	    if (sizeof(lock_name))
		old->remove_item(lock_name[0], lock_desc);
	}
	else
	    old->remove_item(lock_name, lock_desc);
    }	
    remove_window_info(old);
}

/*
 * Function name: standard_open_desc
 */
string
standard_open_desc() 
{
  string temp_desc;
  
  temp_desc = "Widzisz otwart" + koncowka("ego", "^a", "e", "ych", "e") + 
      " " + short(PL_BIE);
//  if (strlen(pass_commands[0]) <= 2)
//    temp_desc = temp_desc + " prowadz^ace na " + pass_commands[1] + ".\n";
//  else
    temp_desc = temp_desc + ".\n";
  return temp_desc;
}


/*
 * Function name: standard_closed_desc
 */
string
standard_closed_desc()
{
  string temp_desc;
  temp_desc = "Widzisz zamkni^et" + koncowka("ego", "^a", "e", "ych", "e") + " "
            + short(PL_BIE);
//  if (strlen(pass_commands[0]) <= 2)
//   temp_desc = temp_desc + " prowadz^ace na " + pass_commands[1] + ".\n";
//  else
    temp_desc = temp_desc + ".\n";
  return temp_desc;
}

/*
 * Function name: standard_open_mess1
 */
string
standard_open_mess1()
{
    return "otwiera " + short(PL_BIE) + ".\n";
}

/*
 * Function name: standard_open_mess2
 */
string
standard_open_mess2()
{
    return capitalize(short(PL_MIA)) + " otwiera" +
        (query_tylko_mn() ? "j^a" : "") + " si^e.\n";
}

/*
 * Function name: standard_open_mess3
 */
string
standard_open_mess3()
{
    return "Otwierasz " + short(this_player(), PL_BIE) + ".\n";
}

/*
 * Function name: standard_fail_open1
 */
string
standard_fail_open1()
{
    return capitalize(short(PL_MIA)) + 
        (query_tylko_mn() ? " s^a" : " jest") + " ju^z otwar" +
        koncowka("ty", "ta", "te", "ci", "te") + ".\n";
}

/*
 * Function name: standard_fail_open2
 */
string
standard_fail_open2()
{
    return capitalize(short(PL_MIA)) + " nie chc" +
        (query_tylko_mn() ? "^a" : "e") + " si^e ruszy^c.\n";
}

/*
 * Function name: standard_close_mess1
 */
string
standard_close_mess1()
{
    return "zamyka " + short(PL_BIE) + ".\n";
}

/*
 * Function name: standard_close_mess2
 */
string
standard_close_mess2()
{ 
    return capitalize(short(PL_MIA)) + " zamyka" +
        (query_tylko_mn() ? "j^a" : "") + " si^e.\n";
}

/*
 * Function name: standard_close_mess3
 */
string
standard_close_mess3()
{
    return "Zamykasz " + short(this_player(), PL_BIE) + ".\n";
}

/*
 * Function name: standard_lock_mess1
 */
string
standard_lock_mess1()
{
    return "zamyka " + short(PL_BIE) + " " + klucz->short(PL_NAR) + ".\n";
}

/*
 * Function name: standard_lock_mess2
 */
string
standard_lock_mess2()
{
    return "S^lyszysz ciche klikni^ecie, dochodz^ace od " + short(PL_DOP) + ".\n";
}

/*
 * Function name: standard_lock_mess3
 */
string
standard_lock_mess3()
{
    return "Zamykasz " + short(this_player(), PL_BIE) + " " +
        klucz->short(PL_NAR) + ".\n";
}


/*
 * Function name: standard_fail_lock1
 */
string
standard_fail_lock1()
{
    return capitalize(short(PL_MIA)) + " ju^z " + 
        (query_tylko_mn ? "s^a" : "jest") + " zamkni^e" +
        koncowka("ty", "ta", "te", "ci", "te") + ".\n";
}

/*
 * Function name: standard_fail_lock2
 */
string
standard_fail_lock2()
{
     return "Wpierw musisz zwyczajnie zamkn^a^c " + short(PL_BIE) + ".\n";
}

/*
 * Function name: standard_unlock_mess1
 */
string
standard_unlock_mess1()
{
     return "otwiera " + short(PL_BIE) + " " + klucz->short(PL_NAR) + ".\n";
}

/*
 * Function name: standard_unlock_mess2
 */
string
standard_unlock_mess2()
{
    return "S^lyszysz ciche klikni^ecie, dochodz^ace od " + short(PL_DOP) + ".\n";
}

/*
 * Function name: standard_unlock_mess3
 */
string
standard_unlock_mess3()
{
    return "Otwierasz " + short(this_player(), PL_BIE) + " " + 
        klucz->short(PL_NAR) + ".\n";
}


/*
 * Function name: standard_window_desc
 */
string
standard_window_desc()
{
    return "Mocno wygladaj^ac" + koncowka("y", "a", "e", "y", "e") + 
        short(PL_MIA) + ".\n";
}

/*
 * Function name: standard_fail_pass
 */
string
standard_fail_pass()
{
    return capitalize(short(PL_MIA)) + " " + 
        (query_tylko_mn() ? "s^a" : "jest") + " zamkni^e" + 
        koncowka("ty", "ta", "te", "ci", "te") + ".\n";
}

/*
 * Function name: standard_fail_close
 */
string
standard_fail_close()
{
     return capitalize(short(PL_MIA)) + " ju^z " +
         (query_tylko_mn() ? "s^a" : "jest") + " zamkni^e" +
         koncowka("ty", "ta", "te", "ci", "te") + ".\n";
}

/*
 * Function name: standard_fail_unlock
 */
string
standard_fail_unlock()
{
    return capitalize(short(PL_MIA)) + " ju^z " + 
        (query_tylko_mn() ? "s^a" : "jest") + " otwar" +
        koncowka("ty", "ta", "te", "ci", "te") + ".\n";
}

string
standard_pass_mess()
{
    return query_verb();
}

public int
is_window()
{
    return 1;
}

/*
 * Function name: 
 * Description:   
 * Arguments:	  
 * Returns:       
 */
