/* Std luku poczynione przez Aldeima */

inherit "/std/strzelecka";
#include <stdproperties.h>
#include <macros.h>

int jakosc;

public void
create_luk()
{
}

int
query_jakosc()
{
  return jakosc;
}

void
ustaw_jakosc(int x)
{
  /* Zakres cech luku:
     celnosc: 18 - 90
     szybkosc: 2 - 12
     sila:    16 - 70
   */

  jakosc = x;
  set_celnosc(10 + (jakosc*8));
  set_szybkosc(2 + jakosc);
  set_sila(10 + (jakosc*6));
}

nomask void
create_strzelecka()
{
  ustaw_nazwe(({ "^luk", "^luku", "^lukowi", "^luk", "^lukiem", "^luku" }),
                 ({ "^luki", "^luk^ow", "^lukom", "^luki", "^lukami",
                    "^lukach" }), PL_MESKI_NOS_NZYW);
  set_long("Najzwyczajniejszy w ^swiecie ^luk.\n");
  set_min_sila(40);
  set_zasieg(1);
  set_pocisk("strza^la");
  create_luk();
}

public void
przygotowanie(object kto, object bron, object strzala)
{
  set_this_player(kto);
  write("K^ladziesz " + strzala->query_short(PL_BIE) + " na "+
    "ci^eciw^e " + bron->query_short(PL_DOP) + " i napinasz j^a.\n");
  saybb(QCIMIE(this_player(), PL_MIA) + " k^ladzie " +
    strzala->query_short(PL_BIE) + " na ci^eciw^e " +
    bron->query_short(PL_DOP) + " i napina j^a.\n");
}
