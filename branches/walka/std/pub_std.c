/*
 * Garagoth
 * Popelnione gdzies kolo 30.05.2002 (ostatnio, ale jeszcze nie do konca)
 * Ostatnia zmiana: 07.10.2002
 */

#pragma strict_types
#pragma save_binary

#include <stdproperties.h>
#include <macros.h>
#include <money.h>

inherit "/lib/pub";
inherit "/std/room";
//inherit "/lib/siadanie";
inherit "/lib/sit";

varargs object set_karczmarz(string str);
void set_nieobecnytekst(string str);
string query_nieobecnytekst();
object query_karczmarz();

private static object sprzed;
private static string str_sprzed;
private static int z_karczmarzem = 0;
private static string nieobecnytekst = "";

private nomask int
is_room(object ob)
{
    return function_exists("create_object", ob) == "/std/room";
}

void
create_pub()
{
    set_short("Mala karczma");
    set_long("Przykladowa karczma.\n");
    set_karczmarz(0);
}

string
wizinfo()
{
   return "'Command reference':\n" +
        "** Funkcje zwykle:\n" +
        "\tvoid create_pub(); - do zamaskowania,\n" +
        "\tvoid set_karczmarz(object|string); - ustawia karczmarza -\n" +
        "\t\tistniejacego|nowego,\n" +
        "\tobject query_karczmarz() - zwraca karczmarza,\n" +
        "\tvoid set_nieobecnytekst() - ustawia tekst, ktory sie pokaze, jak nie\n" +
        "\t\tbedzie karczmarza a gracz cos zamowi,\n" +
        "\tstring query_nieobecnytekst() - zwroci powyzsze,\n" +
        "** Funkcje dotyczace siadania w karczmach:\n" +
        "\tvoid add_sitting_place(string, string, string, string); - np.\n" +
        "\t\tadd_sitting_place('przy', 'stole', [ile_miejsc, 'stol' "+
        "Opis stolu.\\n\"]),\n" +
    "\t\twywolac w create_pub(), mozna kilka razy,\n" +
    "\tremove_sitting_place(\"przy\", \"stole\", [\"stol\"]) - usuwa" +
     "siedzisko,\n\n" +
    "  Argumenty w [] sa opcjonalne.\n" +
        "  Pamietaj, aby zamaskowac funkcje create_pub().\n" +
        "\t\t\t\tPytania? - Call garagoth help%%pub.\n";
}


nomask void
create_room()
{
    sprzed = 0;
    str_sprzed = "";

    add_prop(ROOM_I_INSIDE, 1);
    add_prop(OBJ_S_WIZINFO, &wizinfo());
    create_pub();
}

void
reset_pub()
{
}

void
reset_room()
{
    if (!sprzed && z_karczmarzem == 1)
    {
        sprzed = clone_object(str_sprzed);
        sprzed->szykuj();
        sprzed->move(this_object());
    }
    reset_pub();
}

void
init()
{
    ::init(); /* MUSI byc wywolane w kazdym inicie */
    init_pub(); /* dodanie komend takich jak 'kup', 'sprzedaj' itp */
    //init_siadanie(); // Do siadanie przy stolach.
}


varargs void
set_karczmarz(mixed str)
{
    if (!str)
    {
        z_karczmarzem = 0;
        return;
    }
    if(objectp(str))
    {
        str_sprzed = file_name(str);
        sprzed = str;
        z_karczmarzem = 1;
    }
    else
    if(stringp(str))
    {
        str_sprzed = str;
        sprzed = clone_object(str);
        sprzed->szykuj();
        sprzed->move(this_object());
        z_karczmarzem = 1;
    }
    else
    {
        z_karczmarzem = 0;
        return;
    }
}

object
query_karczmarz()
{
    if(z_karczmarzem == 1)
        return sprzed;
    else
        return 0;
}

void
set_nieobecnytekst(string str)
{
    nieobecnytekst = str;
}
string
query_nieobecnytekst()
{
    return nieobecnytekst;
}

int
order(string co)
{
    int wynik;

    if( (!objectp(sprzed) || (is_room(this_object()) && !present(sprzed,
this_object())) ) && z_karczmarzem == 1)
    {
            if(nieobecnytekst == "")
            notify_fail("Karczmarz chyba sobie gdzies poszedl.\n");
            else
            notify_fail(nieobecnytekst + "\n");
            return 0;
    }

    if(z_karczmarzem == 1)
    {
            if(!CAN_SEE(sprzed, this_player()))
            {
                sprzed->command("emote rozglada sie z wyrazem zagubienia "+
		               "na twarzy.");
                return notify_fail("");
            }
    }

    if(query_trzeba_siadac() && query_verb() == "zamow")
            if(!czy_siedzi(this_player()))
            {
                notify_fail("Usiadz, zanim cos zamowisz.\n");
                return 0;
            }

    wynik=::order(co);
    return wynik;
}

public void
pub_hook_buys_food_na_miejscu(string short, string long, int cena, int
nieskonczyl = 0)
{
    write("Zamawiasz " + long +
        (z_karczmarzem == 1 ? " u " +
query_karczmarz()->query_imie(this_player(), PL_DOP) : "") +
        ", placac " + MONEY_TEXT_SPLIT(cena, PL_BIE) + ".\n" +
        "Po chwili otrzymujesz " +
        (nieskonczyl ? "zamowiona potrawe lecz jestes tak najedzon" +
            this_player()->koncowka("y", "a", "e") + " ze " +
            "nie jestes w stanie jej dojesc" : "i zjadasz zamowiona potrawe") +
        ".\n");
    saybb(QCIMIE(this_player(), PL_MIA) + " zamawia" +
        (z_karczmarzem == 1 ? " u " + QIMIE(query_karczmarz(), PL_DOP) : "") +
        " " + short +
            (nieskonczyl ? ", lecz zjada tylko czesc" : " i zjada calosc "+
	                 "na miejscu") +
            ".\n");
}

public void
pub_hook_buys_food_na_wynos(object ob, int cena)
{
    write("Kupujesz " + ob->short(this_player(), PL_BIE) +
        (z_karczmarzem == 1 ? " u " +
query_karczmarz()->query_imie(this_player(), PL_DOP) : "") +
        ", placac " + MONEY_TEXT_SPLIT(cena, PL_BIE) + ".\n");
    saybb(QCIMIE(this_player(), PL_MIA) + " kupuje" +
        (z_karczmarzem == 1 ? " u " + QIMIE(query_karczmarz(), PL_DOP) : "") +
        " "+QSHORT(ob, PL_BIE) + ".\n");
}

public void
pub_hook_buys_drink_na_miejscu(string short, string long, int cena, int
nieskonczyl = 0)
{
    write("Zamawiasz " + long +
        (z_karczmarzem == 1 ? " u " +
query_karczmarz()->query_imie(this_player(), PL_DOP) : "") +
        ", placac " + MONEY_TEXT_SPLIT(cena, PL_BIE) + ".\n" +
        "Po chwili otrzymujesz " +
        (nieskonczyl ? "zamowiony trunek lecz jestes tak peln" +
            this_player()->koncowka("y", "a", "e") + " ze " +
            "nie jestes w stanie go dopic" : "i wypijasz zamowiony trunek") +
        ".\n");
    saybb(QCIMIE(this_player(), PL_MIA) + " zamawia " +
            (nieskonczyl ? short + ", lecz wypija tylko troche" : "i wypija "
+ short) + ".\n");
}

public void
pub_hook_buys_drink_na_wynos(object ob, int cena)
{
    string str;

    str = ob->query_short(PL_BIE) + " pel" + ob->koncowka("en", "na", "ne") +
        " " + ob->query_opis_plynu();

    write("Nabywasz " + str +
        (z_karczmarzem == 1 ? " u " +
query_karczmarz()->query_imie(this_player(), PL_DOP) : "") +
        ", placac " + MONEY_TEXT_SPLIT(cena, PL_BIE) + ".\n");
    saybb(QCIMIE(this_player(), PL_MIA) + " kupuje " + str +
        (z_karczmarzem == 1 ? " u " + QIMIE(query_karczmarz(), PL_DOP) : "") +
        ".\n");
}


public varargs int
visible(object ob, object forobj)
{
    if(!siadanie_visible(ob))
            return 0;
    else
            return ::visible(ob, forobj);
}

public int
prevent_leave(object ob, object to)
{
    int b;
    b = siadanie_prevent_leave(ob, to);
    if(b)
    {
        ob->catch_msg("Hm, przeciez siedzisz. Jak wiec chcesz gdzies isc?\n");
    }
    return b;
}

void
leave_inv(object ob, object to)
{
    siadanie_leave_inv(ob, to);
    ::leave_inv(ob, to);
}

