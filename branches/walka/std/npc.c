/*
  /std/npc.c

  This is the base for all NPC creatures that need to use combat tools.

*/
#pragma save_binary
#pragma strict_types

inherit "/std/creature";

#include <pl.h>
#include <stdproperties.h>
#include <filter_funs.h>
#include <macros.h>
#include <object_types.h>

#define DBG(x) find_player("vera")->catch_msg(x);

object Admin;
int wzywanie_strazy = 0;
int reakcja_na_walke = 0;
string spolecznosc = "";
int byl_donos, ile=0;
int jest_signal_steal=0;
public int spierniczylismy_signal_fight(object atakujacy, object atakowany, object gdzie);
public int alarm_na_signal_fight(object dokogo,object ob, object lok);
public int signal_fight(object atakowany, object gdzie, object skad);
public void set_admin(string str);
public object query_admin();
public string set_spolecznosc(string s);
public string query_spolecznosc();

void
create_npc()
{

    ::create_creature();
}

nomask void
create_creature()
{
    if (!random(4))
	add_leftover("/std/leftover", "z�b", random(5) + 1, 0, 1, 0, 0, O_KOSCI);
	  
    if (!random(3))
	add_leftover("/std/leftover", "czaszka", 1, 0, 1, 1, 0, O_KOSCI);

    if (!random(4))
	add_leftover("/std/leftover", "ko��", 2, 0, 1, 1, 0, O_KOSCI);
	    
    if (!random(4))
        add_leftover("/std/leftover", "rzepka", 2, 0, 1, 1, 0, O_KOSCI);
            
    if (!random(4))
	add_leftover("/std/leftover", "�opatka", 2, 0, 1, 1, 0, O_KOSCI);
	    
    if (query_prop(LIVE_I_UNDEAD))
    {
	create_npc();
	return;
    }

    if (!random(4))
        add_leftover("/std/leftover", "ucho", 2, 0, 0, 0);

    if (!random(5))
        add_leftover("/std/leftover", "skalp", 1, 0, 0, 1);

    if (!random(3))
        add_leftover("/std/leftover", "paznokie�", random(5) + 1, 0, 0, 0);

    if (!random(2))
	add_leftover("/std/leftover", "serce", 1, 0, 0, 1, 0, O_JEDZENIE);

    if (!random(3))
	add_leftover("/std/leftover", "nos", 1, 0, 0, 0);

    if (!random(2))
	add_leftover("/std/leftover", "nerka", 2, 0, 0, 1, 0, O_JEDZENIE);

    add_leftover("/std/leftover", "oko", 2, 0, 0, 0, 0, O_JEDZENIE);

    create_npc(); 
}

void
reset_npc() 
{  
    ::reset_creature(); 
}

nomask void
reset_creature() 
{ 
    reset_npc(); 
}

/*
 * Description:  Use the combat file for generic tools
 */
public string 
query_combat_file() 
{
    return "/std/combat/ctool"; 
}

/*
 * Function name:  default_config_npc
 * Description:    Sets all neccessary values for this npc to function
 */
varargs public void
default_config_npc(int lvl)
{
    default_config_creature(lvl);
}






//Tutaj zaczyna sie kod zwiazany z wzywaniem strazy, wysylaniem sygnalow
// o walce i kradziezy oraz reakcji na nie.       Vera.




public void
set_wzywanie_strazy(int i)
{
    wzywanie_strazy = i;
}

public int
query_wzywanie_strazy()
{
    return wzywanie_strazy;
}

/*
 * To jest funkcja ustawiaj�ca reakcj� NIE w samym bitym npcu, ale
 * w npcach, kt�re to us�ysz�, �e kto� si� bije.
 * Ustawi� mo�na:
 * 0 - tylko krzyczy, �e si� bij� i przesy�a sygna� dalej, je�li 
 * ma query_wzywanie_strazy
 * 1 - przychodzi i pomaga bitemu npcowi
 */
public int
set_reakcja_na_walke(int i)
{
    reakcja_na_walke = i;
}
public int
query_reakcja_na_walke()
{
    return reakcja_na_walke;
}

/*
 * Function name: set_spolecznosc
 * Description:   Funkcja ta ustawia spolecznosc do jakiej nalezy dany
 *                npc. Ustawienie tego jest wymagane jedynie w wypadku
 *                ustawienia reakcji na walke na pomaganie sobie wzajemnie
 *                Dzieki temu npce wiedza komu pomagac, a komu nie.
 * Arguments:     String - nazwa spolecznosci.
 */
public string
set_spolecznosc(string s)
{
	spolecznosc = s;
}

public string
query_spolecznosc()
{
	return spolecznosc;
}

/*
 *Funkcja s�u��ca do udania si� do ENV danego npca. Przydatna np. podczas wspierania
 *innych oraz wracania si� p�niej na swoj� lokacj�.
 * Zwraca 0 je�li si� nie uda�o i�� (npce nie potrafi� wywa�a� drzwi)
 */
int
idz_na_lok(object dokogo)
{
	mixed droga=znajdz_sciezke(ENV(TO),dokogo);
    int i;
    for(i=0;i<sizeof(droga);i++)
    {
		object env = ENV(TO);
	    TO->command(droga[i]);
		if (ENV(TO) == env) //nie uda�o si� przej��
		{
			//no to otwieramy
	    	object door = filter(filter(all_inventory(ENV(TO)), 
					&->is_door()), &operator(==)(droga[i]) @
					&operator([])(,0) @ &->query_pass_command())[0];
			//door2 to drzwi po drugiej strony
			object door2 = door->query_other_door();

			//door->do_unlock_door("");
			//door2->do_unlock_door("");
			door->open_door(door->short(0));
			door2->open_door(door2->short(0));
			//tell_room(ENV(door2),"Z wielkim hukiem "+door2->short(PL_BIE)+" otwieraj� si� "+
			//"wywa�one przez "+QIMIE(straznik,PL_DOP)+".\n");

	    	TO->command(droga[i]);
		}
    }
	if(ENV(TO) == dokogo)
		return 1;
	else
		return 0;
}

/*
 * Funkcja pomocnicza, wywo�uje si� w npcu, kt�ry wspiera drugiego npca
 * Ma na celu sprawdzanie, czy jeszcze toczy si� walka, je�li nie to 
 * powr�t na poprzedni� lokacj� na kt�rej dany npc by� 
 */
void
zacznij_sprawdzac_czy_jeszcze_walka(object komu)
{
	if(komu->query_attack())
	{
		/*ale to jeszcze nie wszystko, �ycie jest brutalne, ma�o kto
		  po�wi�ci�by �ycie za drugiego bli�niego :> */
		if(komu->query_hp() < 20)
		{
			komu->command("przestan atakowac");
			komu->command("spanikuj");
			idz_na_lok(komu->query_prop("ostatnio_bylem_na_lok"));
			komu->remove_prop("ostatnio_bylem_na_lok");
		}
		else
			set_alarm(4.0,0.0,"zacznij_sprawdzac_czy_jeszcze_walka",komu);
	}
	else
	{
		idz_na_lok(komu->query_prop("ostatnio_bylem_na_lok"));
		komu->remove_prop("ostatnio_bylem_na_lok");
	}
}

/*
 * Ucieklismy z pola bitwy. Ustawiamy wiec alarm co chwila sprawdzajac, 
 * czy nie wrocilismy do walki
 */
public int
spierniczylismy_signal_fight(object atakujacy, object atakowany,object gdzie)
{
    if (!atakowany || !atakujacy) {
        return 1;
    }
    if(environment(atakowany) == environment(atakujacy))
        alarm_na_signal_fight(atakujacy,atakowany,gdzie); //??
    else
    {
        set_alarm(5.0,0.0,"spierniczylismy_signal_fight",atakujacy,atakowany,gdzie);
//        ile++;
    }
    //Nie bedziemy przeciez przestawiali alarmu w nieskonczonosc. Po 20 razach dajemy
    //sobie siana, niech sie dzieje wola boza... Ktos ma lepszy pomysl? :)
//TODO
/*    if(ile>20)
    {
        
    }*/
}

public int
alarm_na_signal_fight(object dokogo,object ob, object lok)
{
    dokogo->signal_fight(ob, lok, ENV(dokogo));
    return 1;
}

public int
zeruj_donos()
{
    byl_donos=0;
    return 1;
}

public int
signal_fight(object atakowany, object gdzie, object skad)
{
    object atakujacy=atakowany->query_enemy();
    if (!atakowany || !atakujacy) {
        return 1;
    }
    if(atakowany->query_enemy())
    {
       //Przesylamy sygnal dalej:
       int i,y;
       object *liv;
       string *sasiednie_lok=ENV(TO)->query_exit_rooms();
       for(i=0;i<sizeof(sasiednie_lok);i++)
       {
//find_player("vera")->catch_msg("robimy dla s�siedniej lok: "+sasiednie_lok[i]+"\n");
           if(environment(atakowany) == environment(atakujacy)) //po primo: sprawdzmy czy
           {                                           //przypadkiem nie spierdolilismy z lokacji :P
           //Nie przesylamy sygnalu na lokacje, skad on pochodzi...
           if(sasiednie_lok[i] != file_name(gdzie) && find_object(sasiednie_lok[i]) &&
              sasiednie_lok[i] != file_name(skad))
           {
//find_player("vera")->catch_msg("signal_fight! Atakowany: "+file_name(atakowany)+", gdzie: "+file_name(gdzie)+"\n");
               liv=FILTER_LIVE(all_inventory(find_object(sasiednie_lok[i])));

			//wykonywane dla ka�dego livinga ka�dej s�siedniej lokacji
            for(y=0;y<sizeof(liv);y++)
            {
//find_player("vera")->catch_msg("robimy dla npca: "+file_name(liv[y])+"\n");
               switch(liv[y]->query_reakcja_na_walke())
               {
                   case 0: //donos, krzyk, ze bija, przekazywanie sygnalu dalej. Default.
                       if(liv[y]->query_straznik() || liv[y]->query_oficer())
                           return 1;

                       if(byl_donos==0 && !this_object()->query_enemy())
                       {
                           switch(random(6))
                           {
                               case 0: 
								if(liv[y]->query_wzywanie_strazy())
									command("emote m�wi: Bij� si�! Stra�e!");
								else
									command("krzyknij Na pomoc!");
								break;
                               case 1: 
								if(liv[y]->query_wzywanie_strazy())
									command("krzyknij Stra�e! Bij� si�!");
								else
									command("powiedz Ech, znowu kogo� t�uk�.");
								break;
                               case 2: 
									if(liv[y]->query_wzywanie_strazy())
										command("krzyknij Stra�!");
									break;
                               case 3: command("powiedz Och! S�yszycie to?! Bij� si�!"); break;
                               case 4: 
									if(liv[y]->query_wzywanie_strazy())
										command("krzyknij Stra�e! Stra�e! Bandyci!");
									break;
								case 5: break;
                           }
                           byl_donos=1;
                           set_alarm(7.0,0.0,"zeruj_donos");
                       }
						if(liv[y]->query_wzywanie_strazy())
						{
                           //liv[y]->signal_fight(atakowany, gdzie, environment(this_object()));
                           set_alarm(0.5,0.0,"alarm_na_signal_fight",liv[y],atakowany,gdzie);
						}
                       break;
				case 1:
//find_player("vera")->catch_msg("case 1\n");
						//wspieramy tylko npca tej samej spolecznosci co my.
					if(atakowany->query_npc() && !atakowany->query_zwierze() &&
						atakowany->query_spolecznosc() ~= liv[y]->query_spolecznosc())
					{
//find_player("vera")->catch_msg("nom\n");
						liv[y]->add_prop("ostatnio_bylem_na_lok",ENV(liv[y]));
						liv[y]->idz_na_lok(ENV(atakowany));
						liv[y]->command("wesprzyj "+TO->short(3));
						liv[y]->zacznij_sprawdzac_czy_jeszcze_walka(liv[y]);
						
					}
                   case 2: //RESZTY NIE MA...Tj. uciekanie, jak si� us�yszy walk�
							//dla wyj�tkowo p�ochliwych... TODO
                       break;

               }
           }
			}

           }
           else //Ojej, ucieklismy z pola bitwy? :)
           {
               spierniczylismy_signal_fight(atakujacy,atakowany,gdzie);
           }
       }
    }

    return 1;
}

public void
attacked_by(object ob)
{
    if (TO->query_wzywanie_strazy() && !query_prop(LIVE_M_MOUTH_BLOCKED))
    {
		switch(random(9))
        {
            case 0: command("krzyknij Stra�! Na pomoc!"); break;
            case 1: command("krzyknij Stra�! Pomocy!"); break;
            case 2: command("krzyknij Pomocy, atakuj^a!"); break;
            case 3: command("krzyknij Na pomoc! Bij^a!"); break;
            case 4: command("krzyknij Stra�! Stra^z! Stra^z!"); break;
            default: break;
        }

		if(objectp(Admin))
			Admin->atak_na_npca(this_object(),ob);
		else
		{//No jak to? Ma wzywanie stra�y ustawione, a Admina nie?! Niemo�eby�!
			write("Wyst�pi� b��d! Zg�o� to natychmiast w postaci, kt�r� w�a�nie "+
				"atakujesz!\n");
		}
		
        //Wysylanie sygnalu o atakowaniu na sasiednie lokacje
        int i=0,y=0;
        object lok=environment(this_object()), ob=this_object();
        string *sasiednie_lok=lok->query_exit_rooms();
        //object *liv=FILTER_LIVE(all_inventory(find_object(sasiednie_lok[i])));
        //object *liv;
        //Wysylamy do livingow na sasiednich lokacjach:
        for(i=0;i<sizeof(sasiednie_lok);i++)
        {
             //Sprawdzamy wpierw, czy sasiednia lokacja jest w ogole zaladowana do pamieci
             if(find_object(sasiednie_lok[i]))
             {
                 /*liv=FILTER_LIVE(all_inventory(find_object(sasiednie_lok[i])));
                 for(y=0;y<sizeof(liv);y++)
                 {
                     set_alarm(0.5,0.0,"alarm_na_signal_fight",liv[y],ob, lok);
                 }*/

                 //Dojebie jeszcze taki maly event. ;)
                 string exit_cmd=environment(this_object())->query_exit_cmds()[i], str;
                 switch(exit_cmd)
                 {
                     case "wsch�d": str="z zachodu"; break;
                     case "zach�d": str="ze wschodu"; break;
                     case "p�noc": str="z po�udnia"; break;
                     case "po�udnie": str="z p�nocy"; break;
                     case "po�udniowy-wsch�d": str="z p�nocnego-zachodu"; break;
                     case "po�udniowy-zach�d": str="z p�nocnego-wschodu"; break;
                     case "p�nocny-wsch�d": str="z po�udniowego-zachodu"; break;
                     case "p�nocny-zach�d": str="z po�udniowego-wschodu"; break;
                     case "g�ra": str="z do�u"; break;
                     case "d�": str="z g�ry"; break;
                     default: str="z oddali"; break;
                 }
                 tell_room(sasiednie_lok[i] ,capitalize(str)+
                           " dochodz^a twych uszu odg^losy walki!\n");
             }
        }
        //Wysylamy do livingow na tej samej lokacji:
        object *liv_here=FILTER_OTHER_LIVE(all_inventory(lok));
        for(i=0;i<sizeof(liv_here);i++)
        {
            //liv_here[i]->signal_fight(ob, lok);
            set_alarm(1.0,0.0,"alarm_na_signal_fight",liv_here[i],ob,lok);
        }

    }
    else //Dla tych, ktorzy nie sa chronieni przez straz... TODO
    {
    }

    ::attacked_by(ob);
}



int
sprawdz_miejsce_obiektu(object co)
{

    //p�ki co sublok nie jest sprawdzany... bo jak?
    if(file_name(ENV(co)) == co->query_orig_place()[0])
        return 1;
    else
        return 0;
}

void
zglos_kradziez(object kto, object co)
{
		string *tab=({ }), dodajemy;
		int prior,prz;
		for(prior=0;prior<=10;prior++)
		{
			int ile_przym=sizeof(kto->query_lista_przymiotnikow2(prior));
			if(ile_przym)
			{
				foreach(string *przyms:kto->query_lista_przymiotnikow2(prior))
					tab+=({przyms[0]});
			}
		}

//DBG("przesy�am argumenty: "+file_name(co)+", "+file_name(kto)+" przymiotniki:\n");
//foreach(string x:tab)
//DBG(x+"\n");

		co->set_who_stole(({kto->query_real_name()}) + tab);
		//co->add_prop(OBJ_I_KRADZIONE,1);
}

void
test_co_zrobil_gracz(object kto, object co)
{
    //czy 'co' wyl�dowa�o tam, gdzie przynale�y...(orig_place)
    if(sprawdz_miejsce_obiektu(co))
    {
//DBG("od�o�one!\n");
        //je�li zd��y� wr�ci� na lokacj� z npcem, to jaki� komunikacik:>
		switch(random(9))
		{
			case 0:
        	set_alarm(1.0, 0.0, "command_present", kto, "powiedz do "+
                           OB_NAME(kto)+" Dzi�kuj�. Prosz� tego wi�cej "+
                            "nie robi�.");
			break;
			case 1:
        	set_alarm(1.0, 0.0, "command_present", kto, "powiedz do "+
                           OB_NAME(kto)+" Dzi�kuj�.");
			break;
			case 2: command("skin"); break;
			case 3: command("m�wi do siebie pod nosem: Ech, z�odziejaszki, "+
					"wszystko by wynie�li.");
					break;
			case 4: command("powiedz No, tak lepiej.");
					break;
			case 5..8: break;
		}

    }
    else if (ENV(kto) != ENV(TO)) //uciek� ?
    {
//DBG("nieod�o�one! wzywam stra�!\n");
		if(query_wzywanie_strazy())
			command("krzyknij Stra�! Z�odziej!");
		else
			command("krzyknij Z�odziej!");
		zglos_kradziez(kto,co);

    }
    else if(ENV(kto) == ENV(TO)) //nie uciek�, stoi jak ko�ek nadal.
    {
//DBG("stoi jak ko�ek nadal\n");
		if(query_wzywanie_strazy())
	        set_alarm(1.0, 0.0, "command_present", kto, "powiedz do "+
                            OB_NAME(kto)+" Do�� tego! Wzywam stra�!");//?
		else
	        set_alarm(1.0, 0.0, "command_present", kto, "powiedz do "+
                            OB_NAME(kto)+" Do�� tego z�odzieju!");//?
        set_alarm(2.0,0.0,"zglos_kradziez",kto,co);
    }
    else {
//DBG("dupa???\n");
    }
}

void
set_admin(string str)
{
	Admin = find_object(str);
}

object
query_admin()
{
    return Admin;
}


void
zeruj_signal_steal()
{
	jest_signal_steal=0;
}

/*
 * Standardowa reakcja na kradzie�.
 */
void
signal_steal(object kto, object co)
{
	if(jest_signal_steal)
		return;

	if(member_array(kto,FILTER_CAN_SEE(FILTER_LIVE(all_inventory(ENV(TO))),
		TO)) == -1)
		return;

	if(query_prop(LIVE_M_MOUTH_BLOCKED))
		return;

	jest_signal_steal=1;
	string co_mowie="";
//DBG("SIGNAL_STEAL, kto: "+kto->query_real_name()+", co: "+co->short());
    if(TO->query_wzywanie_strazy())
    {
        switch(random(6))
        {
            case 0: co_mowie="Od�� "+co->koncowka("m�j","moj�","moje")+
                    " "+co->query_name(PL_MIA)+" na swoje miejsce, bo wezw� "+
                    "stra�!"; break;
            case 1: co_mowie="Hej! Od�� "+co->koncowka("m�j","moj�","moje")+
                    " "+co->query_name(PL_MIA)+" na miejsce, bo wezw� "+
                    "stra�e!"; break;
            case 2: co_mowie="Ej�e! "+co->koncowka("Ten","Ta","To")+
                    " "+co->query_name(PL_MIA)+" jest "+
                    co->koncowka("m�j","moja","moje")+", od�� to na "+
                    "miejsce, w przeciwnym razie wezw� stra�e!"; break;
            case 3: co_mowie="Ej�e! "+co->koncowka("Ten","Ta","To")+
                    " "+co->query_name(PL_MIA)+" jest "+
                    co->koncowka("m�j","moja","moje")+", od�� to na "+
                    "miejsce, w przeciwnym razie wezw� stra�e i si� "+
                    "z tob� porachuj�!"; break;
            case 4: co_mowie="Zostaw "+co->koncowka("ten","ta","to")+
                    " "+co->query_name(PL_MIA)+", bo wezw� stra�e!"; break;
            case 5: co_mowie="Od�� to na swoje miejsce!"; break;
            default: co_mowie="Od�� "+co->koncowka("m�j","moj�","moje")+
                    " "+co->query_name(PL_MIA)+" na swoje miejsce, bo wezw� "+
                    "stra�!"; break;
        }
    }
    else
    {
        switch(random(6))
        {
            case 0: co_mowie="Od�� "+co->koncowka("m�j","moj�","moje")+
                    " "+co->query_name(PL_MIA)+" na swoje miejsce!"; break;
            case 1: co_mowie="Hej! Od�� "+co->koncowka("m�j","moj�","moje")+
                    " "+co->query_name(PL_MIA)+" na miejsce, bo porozmawiamy "+
                    "inaczej!"; break;
            case 2: co_mowie="Ej�e! "+co->koncowka("Ten","Ta","To")+
                    " "+co->query_name(PL_MIA)+" jest "+
                    co->koncowka("m�j","moja","moje")+", od�� to na "+
                    "miejsce, w przeciwnym razie wezw� ziomk�w i si� z "+
					"tob� porachuj�!"; break;
            case 3: co_mowie="Ej�e! "+co->koncowka("Ten","Ta","To")+
                    " "+co->query_name(PL_MIA)+" jest "+
                    co->koncowka("m�j","moja","moje")+", od�� to na "+
                    "miejsce, bo...."; break;
            case 4: co_mowie="Zostaw "+co->koncowka("ten","ta","to")+
                    " "+co->query_name(PL_MIA)+"!"; break;
            case 5: co_mowie="Od�� to na swoje miejsce!"; break;
            default: co_mowie="Od�� "+co->koncowka("m�j","moj�","moje")+
                    " "+co->query_name(PL_MIA)+" na swoje miejsce!"; break;
        }
    }


        //Dajemy graczowi ostrze�enie.
        set_alarm(1.0, 0.0, "command_present", kto, "powiedz do "+
                            OB_NAME(kto)+" "+co_mowie);

        //Sprawd�my, czy poskutkowa�o...
        set_alarm(10.0+itof(random(10)),0.0,"test_co_zrobil_gracz",kto,co);

	set_alarm(2.0,0.0,"zeruj_signal_steal");
}
