inherit "/std/creature";
inherit "/std/combat/unarmed";

inherit "/std/act/action";

#include <wa_types.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <stdproperties.h>
#include <files.h>
#include <cmdparse.h>

/*
 * Definiujemy to dla wlasnej wygody
 */
#define A_DZIOB		0
#define A_LLAPA		1
#define A_PLAPA		2

#define H_GLOWA		0
#define H_KORPUS	1

object list;
string adresat;

string zaatakowany();

void
create_golab()
{
    ustaw_odmiane_rasy( ({"go^l^ab","go^l^ebia","go^l^ebiowi","go^l^ebia",
        "go^l^ebiem","go^l^ebiu"}),({"go^l^ebie","go^l^ebi","go^l^ebiom",
        "go^l^ebie","go^l^ebiami","go^l^ebiach"}), PL_MESKI_NZYW);

    ustaw_shorty( ({"go^l^ab pocztowy","go^l^ebia pocztowego",
        "go^l^ebiowi pocztowemu","go^l^ebia pocztowego",
        "go^l^ebiem pocztowym","go^l^ebiu pocztowym"}),
        ({"go^l^ebie pocztowe","go^l^ebi pocztowych",
        "go^l^ebiom pocztowym","go^l^ebie pocztowe",
        "go^l^ebiami pocztowymi","go^l^ebiach pocztowych"}), PL_MESKI_NZYW);

    set_gender(G_MALE);

    set_long("Zwyk^ly go��b pocztowy.\n");
}

void
create_creature()
{
    set_stats( ({ 5, 40, 5, 5, 15, 5}));

    set_skill(SS_DEFENCE, 35);
    set_skill(SS_UNARM_COMBAT, 30);

    set_attack_unarmed(A_DZIOB, 10, 10, W_IMPALE, 60, "dziobem");
    set_attack_unarmed(A_LLAPA, 5, 5, W_SLASH,  20, "pazurami lewej ^lapki");
    set_attack_unarmed(A_PLAPA, 5, 5, W_SLASH,  20, "pazurami prawej ^lapki");

    set_hitloc_unarmed(H_GLOWA, ({ 10, 20, 15 }), 15, "g^low^e");
    set_hitloc_unarmed(H_KORPUS, ({ 10, 10, 20 }), 85, "korpus");

    remove_prop(OBJ_M_NO_GET);
    add_prop(LIVE_I_NEVERKNOWN, 1);

    add_prop(CONT_I_WEIGHT, 2000);
    add_prop(CONT_I_VOLUME, 3000);
    add_prop(OBJ_I_VALUE,196);

    add_prop(OBJ_M_NO_ATTACK, &zaatakowany());

    create_golab();

    list = 0;
}

void
destructnij()
{
    destruct();
}

string 
zaatakowany()
{
    set_alarm(0.1, 0.0, &write("Pr�bujesz zaatakowa� "+
        this_object()->short(PL_BIE)+", lecz ten"
        +" w ostatniej chwili wzbija si� w powietrze"
        +" i odlatuje.\n"));
    saybb(capitalize(this_object()->short(PL_MIA))+" w ostatniej chwili wzbija si� w powietrze i"
        +" odlatuje.\n");
        set_alarm(0.5,0.0,&destructnij());
  return "";
}


string
query_long()
{
    return ::query_long() + (list ? "Zauwa^zasz ma^l^a karteczk^e przy jego n^o^zce.\n" : "");
}

void
init()
{
    ::init();
    add_action("napisz", "napisz");
    add_action("wyslij", "wy^slij");
    add_action("odczep", "odczep");
    add_action("pomoc", "?", 2);
}

public int
pomoc(string str)
{
    object pohf;

    notify_fail("Nie ma pomocy na ten temat.\n");

    if (!str) return 0;

    if (!parse_command(str, this_player(), "%o:" + PL_MIA, pohf))
        return 0;

    if (pohf != this_object())
        return 0;

    write("Napisz wiadomo�� i wy�lij, za� je�li si� pomylisz po prostu j� odczep.\n");
    return 1;
}

public void
done_editing(string text, object tp=0)
{
    list = clone_object("/lib/wiadomosc");
    list->move(this_object());
    list->set_message(text);
    tp->catch_msg("Ko�czysz pisanie wiadomo�ci.\n");
}

int
napisz(string str)
{
    if (!(str ~= "wiadomo^s^c"))
    {
        notify_fail("Napisz co? Mo^ze wiadomo^s^c?\n");
        return 0;
    }

    if (list)
    {
        notify_fail("Ten go^l^ab ju^z ma jedn^a wiadomo^s^c.\n");
        return 0;
    }

    clone_object(EDITOR_OBJECT)->edit(&done_editing(,TP));

    return 1;
}

int
wyslij(string str)
{
    object golab;

    if (!str || !parse_command(str, this_player(), "%o:" + PL_BIE + " 'do' %s", golab, adresat) || golab != this_object())
    {
        notify_fail("Wy^slij " + short(PL_BIE) + " do kogo?\n");
        return 0;
    }

    if(ENV(TP)->query_prop(ROOM_I_INSIDE))
    {
        notify_fail("Aby to uczyni� musisz wyj�� na zewn�trz.\n");
        return 0;
    }

    write("Wysy^lasz go^l^ebia pocztowego w sin^a dal.\n");
    saybb(QCIMIE(this_player(), PL_MIA) + " wysy^la go^l^ebia pocztowego w sin^a dal.\n");

    set_no_show();
    set_no_show_composite(1);
    set_alarm(7.0+frandom(10, 2), 0.0, "przylot", this_player());

    return 1;
}

void
przylot(object player)
{
    int i;
    string kto;
    object prev_player;

    i = member_array(adresat, users()->query_real_name(PL_DOP));
    // golab wraca, jesli adresata nie ma w grze, a jesli jest w grze,
    // to random(2), czy wroci (50%) / na razie bez randoma
    if (i == -1 || ENV(users()[i])->query_prop(ROOM_I_INSIDE))
    {
        prev_player = TP;
        set_this_player(player);
        unset_no_show();
        set_no_show_composite(0);
        write("Go^l^ab powr^oci^l... widocznie nie znalaz^l adresata.\n");
        saybb("Go^l^ab pocztowy siada na ramieniu "+
                QIMIE(this_player(), PL_DOP) + ".\n");
        set_this_player(prev_player);
    }
    else
    {
        prev_player = TP;
        set_this_player(users()[i]);
        this_object()->move(this_player(), 1);
        unset_no_show();
        set_no_show_composite(0);
        write("\nJaki^s go^l^ab pocztowy l^aduje ci na ramieniu.\n\n");
        saybb("Go^l^ab pocztowy siada na ramieniu "+
                QIMIE(this_player(), PL_DOP) + ".\n");
        set_this_player(prev_player);
    }
}

int
odczep(string str)
{
    NF("Co takiego chcesz odczepi�?\n");

    if(!str)
        return 0;

    if(!wildmatch("wiadomo�� *", str) && !wildmatch("karteczk� *", str))
        return 0;

    NF("Od czego chcesz odczepi� wiadomo��?\n");

    object *golab;
    if(!parse_command(str, all_inventory(TP), "'wiadomo��' / 'karteczk�' 'od' [nogi] / [n�ki] %i:" + PL_DOP, golab))
        return 0;

    if(!golab)
        return 0;

    golab = NORMAL_ACCESS(golab, 0, 0);

    if(!sizeof(golab))
        return 0;

    if(sizeof(golab) > 1)
    {
        NF("Nie mo�esz odczepi� jednej wiadomo�ci od n�g kilku go��bi.\n");
        return 0;
    }

    if(golab[0] != TO)
        return 0;

    if(!list)
    {
        NF("Do n�ki " + short(TP, PL_DOP) + " nie jest przyczepiona �adna "+
            "wiadomo��.\n");
        return 0;
    }

    list->move(TP, 1);
    list = 0;

    write("Odczepiasz wiadomo�� od n�ki " + short(TP, PL_DOP) + ".\n");
    saybb(QCIMIE(TP, PL_MIA) + " odczepia wiadomo�� od n�ki " + short(TP, PL_DOP) + ".\n");
    return 1;
}

//To jest z�e! :P
/*
public int
query_npc()
{
	return 0;
}*/
