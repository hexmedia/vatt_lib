 /* Std broni strzeleckiej. Popelnione przez  Aldeima
    na podstawie prac Felana, listopad 2006.
     
     
     
     
     i tak wi�kszo�� przerobi�em, 
     g��wnie z powod�w ogromnej ilo�ci b��d�w
     vera
     
     
     */

inherit "/std/weapon";
#include <stdproperties.h>
#include <wa_types.h>
#include <macros.h>
#include <cmdparse.h>
#include <filter_funs.h>
#include <ss_types.h>
#include <exp.h>

#define PROP  "_postrzelony_z_daleka"

/*#define DEBUG(x) (filter(users(), &->query_wiz_level())->catch_msg("Debug z broni strz.:\n" + x + "\n"))*/
#define DEBUG(x)  find_player("vera")->catch_msg("DBG: "+x+"\n");


object lokacja_celu, lokacja_gracza, cel, pocisk, paralyze;
int min_sila, celnosc, szybkosc, sila, zasieg;
int alarm;
string kierunek, pocisk_opis;
int celowanie_alarm;

int przestan(string str, int silent=0);
public void	przygotowanie(object kto, object bron, object strzala);
int lec();

public void
create_strzelecka()
{
}

public void
strzelecka_init()
{
}

nomask void
create_weapon()
{
  setuid();
  seteuid(getuid());

  set_hit(1);
  set_pen(1);
  set_wt(W_MISSILE);
  set_dt(W_BLUDGEON);
  set_hands(W_BOTH);
  min_sila = 0;
  zasieg = 1;
  celnosc = 10;
  szybkosc = 1;
  sila = 10;

  add_prop(OBJ_I_VALUE, 984);
  add_prop(OBJ_I_WEIGHT, 5524);
  add_prop(OBJ_I_VOLUME, query_prop(OBJ_I_WEIGHT)/5);

  create_strzelecka();
}

/* Minimalna sila potrzebna do uzycia broni broni
   w zakresie od jednego do stu dwudziestu pieciu */
void
set_min_sila(int ile)
{
  min_sila = ile;
}

int
query_min_sila()
{
  return min_sila;
}

/* Celnosc broni w zakresie od jednego do stu */
void
set_celnosc(int ile)
{
  if (ile < 1 || ile > 100) return;
  celnosc = ile;
}

int
query_celnosc()
{
  return celnosc;
}

/* Szybkosc broni w zakresie od jednego do pietnastu */
void
set_szybkosc(int ile)
{
  if (ile < 1 || ile > 15) return;
  szybkosc = ile;
}

int
query_szybkosc()
{
  return szybkosc;
}

/* Sila broni w zakresie od jednego do stu */
void
set_sila(int ile)
{
  if (ile < 1 || ile > 100) return;
  sila = ile;
}

int
query_sila()
{
  return sila;
}

/* Zasieg broni, jesli zero - bronia mozna strzelac tylko do
   celow znajdujacych sie na tej samej lokacji, co strzelec,
   jesli jeden - rowniez do celi o lokacje dalej */
void
set_zasieg(int ile)
{
  if (!ile) zasieg = 0;
  else zasieg = 1;
}

int
query_zasieg()
{
  return zasieg;
}

/* Funkcja ustawia nazwe pocisku obslugiwanego przez ta bron. */
void
set_pocisk(string str)
{
  pocisk_opis = str;
}

string
query_pocisk()
{
  return pocisk_opis;
}

int
sprawdz_czy_mamy_pociski(string pocisk_opis, object *ekwipunek)
{
//DEBUG("SPRAWDZ_CZY_MAMY_POCISKI");
	int i;
//	object *ekwipunek = all_inventory(TP)->query_nazwa();
	
	
  if ((i = member_array(pocisk_opis, ekwipunek)) != -1)
  {
		if(all_inventory(TP)[i]->query_jakosc() != 0)
			return i;
		else
			return -1;
  }
  else
  {
    write("Nie masz odpowiedniego pocisku!\n");
    return -1;
  }
}


void
celowanie_meczy(object kogo)
{
	set_this_player(kogo);
	kogo->set_fatigue(kogo->query_fatigue() - random(9));
	
	if(kogo->query_fatigue() == 0)
	{
		write("Zm�czenie uniemo�liwia ci dalsze celowanie.\n");
		przestan("strzelac");
		przestan("celowac",1);
    		
		return;
	}
	else
		set_alarm(1.0,0.0,"celowanie_meczy",kogo);
}

void
hook_celuj(object kto, object cel)
{
	przygotowanie(TP, TO, pocisk);

	object bron = this_object();
	kto->catch_msg("Unosisz sw"+bron->koncowka("�j","oj�","oje")+
		" "+bron->query_short(PL_BIE)+" i zaczynasz mierzy� do "
	+QIMIE(cel,PL_DOP) + ".\n");
	
	object *inni=FILTER_OTHER_LIVE(all_inventory(ENV(kto)));
	inni = FILTER_IS_SEEN(kto, inni);
	inni -= ({ cel });
	
	if(ENV(kto) == ENV(cel)) //prosta sprawa ;)
	{
		foreach(object x: inni)
			x->catch_msg(QCIMIE(kto,PL_MIA)+ " unosi sw"+bron->koncowka("�j","oj�","oje")+
		" "+bron->query_short(PL_BIE)+" i zaczyna mierzy� do "+
			QIMIE(cel,PL_DOP) + ".\n");

		if(sizeof(FILTER_IS_SEEN(kto,({cel}))))
		cel->catch_msg(QCIMIE(kto,PL_MIA)+ " unosi sw"+bron->koncowka("�j","oj�","oje")+
		" "+bron->query_short(PL_BIE)+" i zaczyna mierzy� do "+
		"CIEBIE!\n");
	
	}
	else
	{
		int gdzie = member_array(file_name(ENV(cel)),
						ENV(kto)->query_exit_rooms());
		string dokad="";
		if(gdzie != -1)
			dokad=ENV(kto)->query_exit_cmds()[gdzie];
		if(dokad != "")
		{
			foreach(object x: inni)
			x->catch_msg(QCIMIE(kto,PL_MIA)+ " unosi sw"+bron->koncowka("�j","oj�","oje")+
		" "+bron->query_short(PL_BIE)+" i zaczyna mierzy� na "+
			dokad+".\n");
		}
		else //tj. spieprzy� cel z pola widzenia ?!
		{
			write("Wyst�pi� krytyczny b��d w mierzeniu do ofiary! Zg�o� "+
					"to natychmiast!\n");
		}
	
	}
	
	if(get_alarm(celowanie_alarm) ==0)
		 celowanie_alarm = set_alarm(1.0,0.0,"celowanie_meczy",kto);
	
	return;
}

/* Funkcja odpowiada za pierwszy komunikat wyswietlany
   na poczatku strzalu. Do przemaskowania w zaleznosci od
   rodzaju broni. */
public void
przygotowanie(object kto, object bron, object strzala)
{
  set_this_player(kto);
  this_player()->catch_msg("Przygotowujesz si^e do strza^lu.\n");
  saybb(QCIMIE(this_player(), PL_MIA) + " przygotowuje si^e do strza^lu.\n");
}

int
widzi(object kto, object kogo, int zwroci = 0)
{
  int wzrost, trudnosc, i, poczatkowa, ciemno, widzenie;
  object lok_kto, lok_kogo;
  mixed wyjscia;

  lok_kto = environment(kto);
  lok_kogo = environment(kogo);

  if (lok_kto == lok_kogo && zwroci == 0) return 1;

  wyjscia = lok_kto->query_exit();

  if ((i = member_array(file_name(lok_kogo), wyjscia)) == -1 &&
      zwroci == 0) return 0;

  poczatkowa = 100;
  if ((widzenie = kto->query_prop(LIVE_I_SEE_DARK)) == 0)
     {
      if ((ciemno = lok_kogo->query_prop(OBJ_I_LIGHT)) == 0) {
      return 0; }
     }

  trudnosc = 10 * lok_kto->query_tired_exit(i + 2);
  wzrost = kogo->query_prop(CONT_I_HEIGHT);
  i = poczatkowa + kto->query_skill(SS_AWARENESS) + wzrost / 10;
  i -= 10 * trudnosc;

  if (zwroci) return i;
  if (i > 0) return 1;
   return 0;
}

void
zeruj(int zeruj_kierunek = 1)
{
  cel = 0;
  lokacja_celu = 0;
  lokacja_gracza = 0;
  if(zeruj_kierunek)
  	kierunek = 0;
}

/* Funkcja dodaje doswiadczenie w poslugiwaniu sie bronmi
   strzeleckimi.

   w przypadku krytycznego pudla (argument -1):     0
   w przypadku pudla (argument 0):                  1
   w przypadku trafienia (argument 1):              2
   w przypadku krytycznego trafienia (argument 2)   5
 */
void
dodaj_ss_expa(object kto, int ile)
{
  int x;
  switch(ile)
  { 
    case -1: x = EXP_STRZELANIE_KRYT_PUDLO_MISSILE; break;
    case 0: x =    EXP_STRZELANIE_PUDLO_MISSILE;      break;
    case 1: x =    EXP_STRZELANIE_TRAFILEM_MISSILE;     break;
    case 2: x =    EXP_STRZELANIE_TRAFILEM_KRYT_MISSILE;break;
  }

  kto->increase_ss(SS_WEP_MISSILE, x);
}

/* Funkcja oblicza, czy strzelec trafil do celu. Pod uwage brane
   sa statystyki strzelca, celu oraz luku.

   Funkcja zwraca:  -1  jesli krytyczne chybienie
                     0  jesli chybienie
                     1  jesli trafienie w cialo
                     2  jesli trafienie w glowe */
int
oblicz_czy_trafil(object kto, object kogo)
{
  int szansa = (kto->query_skill(SS_WEP_MISSILE) + kto->query_stat(SS_DEX) +
    this_object()->query_celnosc()) / 2;
  szansa += random(51) - 25;

  if ((kogo->query_stat(SS_DEX) + ((kogo->query_skill(SS_ACROBAT)) +
    kogo->query_skill(SS_DEFENCE) / 2)) >
    (kto->query_stat(SS_DEX) + kto->query_skill(SS_WEP_MISSILE)))
    szansa -= random(kogo->query_skill(SS_ACROBAT) +
    kogo->query_skill(SS_DEFENCE) + kogo->query_stat(SS_DEX));

  else
    szansa -= random(kogo->query_skill(SS_DEFENCE));

  int los = random(101);
  if (los == 0) return -1; // magiczne wlasciwosci liczby 0
  if (los == 100) return 2; // magiczne wlasciwosci liczby 100

  /* Jesli strzelec trafil sprawdzamy, czy trafi w cialo
     (rozumiane jako wszystko poza glowa) czy w glowe, co jest
     znacznie trudniejsze i niebezpieczniejsze dla celu. */

  if (los <= szansa)
  {
    szansa = kto->query_skill(SS_WEP_MISSILE) / 2;
    if (kogo->query_zwierze())
        szansa += (kto->query_skill(SS_HUNTING) / 2);

    szansa += random(61) - (40 + random(11));

    if (random(101) < szansa) return 2;
    else return 1;
  }

  return 0;
}

/* Funkcja oblicza, ile obrazen otrzyma cel po trafieniu. Argument
   glowa ustawiamy na jeden, jesli strzelec trafil w glowe.

   Funkcja zwraca zadane obrazenia. */
int
oblicz_obrazenia(object kto, object kogo, object strzala, int glowa = 0)
{
  mixed zbroja;
  int min;
  int um = kto->query_skill(SS_WEP_MISSILE) * 2;
  um += (kto->query_stat(SS_STR) + kto->query_stat(SS_DEX)) / 2;
  um += query_sila() + (pocisk->query_jakosc() * 5);
  um *= 2;
  um += random(10);

  int obr = kto->query_skill(SS_WEP_MISSILE) + random(um) +
    random(5);
  int typ = strzala->query_typ();

  if (glowa)
  {
    obr += random(60);
    zbroja = kogo->query_armour(TS_HEAD);
    if (!zbroja) return obr;
    min = zbroja->query_ac(typ) * 2;
  }

  else
  {
    zbroja = kogo->query_armour(-1);
    if (kogo->query_armour(TS_HEAD) != 0)
    { zbroja -= ({ kogo->query_armour(TS_HEAD) }); }

    if (!zbroja || !sizeof(zbroja)) return obr;
    for(int i = 0; i < sizeof(zbroja); i++)
    {
      min += zbroja[i]->query_ac(typ);
    }
  }

  min += (kogo->query_stat(SS_CON) / 5);
  obr -= min;
  return obr;
}

/* Nazwa funkcji: cel_w_zasiegu
 * Opis: Sprawdza, czy aktualnie ustawiony cel znajduje sie w zasiegu
 *     luku.
 * Zwraca: 0 - brak celu
 *     1 - cel i gracz nie poruszyli sie, cel w zasiegu
 *     2 - cel i/lub gracz poruszyli sie, cel w zasiegu
 *     3 - cel i/lub gracz poruszyli sie, cel poza zasiegiem
 *     4 - cel i/lub gracz poruszyli sie, znajduja sie na tej samej lokacji
 *     5 - brak celu, gracz celuje w jakims kierunku (na razie wylaczone)
 */
int
cel_w_zasiegu()
{
  int i;
  mixed *wyjscia = environment(this_player())->query_exit();

  if (cel == 0) return 0;

/*
  if (cel == 0) {
    if (kierunek == 0) return 0;
    else {
      if (lokacja_gracza == environment(this_player())) {
        return 5;
      } else {
        return 0;
      }
    }
  }
*/
    else {
    if (lokacja_celu == environment(cel) && lokacja_gracza == environment(this_player()))
{
      return 1;
    } else {
      if (lokacja_celu != environment(cel) || lokacja_gracza != environment(this_player()))
{
        if (!widzi(this_player(), cel)) {
          zeruj();
          return 3;
        }
        lokacja_celu = environment(cel);
        lokacja_gracza = environment(this_player());
        wyjscia = lokacja_gracza->query_exit();
        if ((i = member_array(file_name(lokacja_celu), wyjscia)) != -1) {
          if (!pointerp(wyjscia[i + 1])) kierunek = wyjscia[i + 1];
          else kierunek = wyjscia[i + 1][0];
          return 2;
        } else if (lokacja_gracza == lokacja_celu) {
          kierunek = 0;
          return 4;
        } else {
          zeruj();
          return 3;
        }
      }
    }
  }
}

string
ustaw_kierunek(string kierun, int przeciw = 0)
{
  if (przeciw == 0) {
    if (kierun ~= "n" || kierun ~= "p^o^lnocy")
      kierunek = "p^o^lnoc";
    else if (kierun ~= "ne" || kierun ~= "p^o^lnocnym-wschodzie")
      kierunek = "p^o^lnocny-wsch^od";
    else if (kierun ~= "e" || kierun ~= "wschodzie")
      kierunek = "wsch^od";
    else if (kierun ~= "se" || kierun ~= "po^ludniowym-wschodzie")
      kierunek = "po^ludniowy-wsch^od";
    else if (kierun ~= "s" || kierun ~= "po^ludniu")
      kierunek = "po^ludnie";
    else if (kierun ~= "sw" || kierun ~= "po^ludniowym-zachodzie")
      kierunek = "po^ludniowy-zach^od";
    else if (kierun ~= "w" || kierun ~= "zachodzie")
      kierunek = "zach^od";
    else if (kierun ~= "nw" || kierun ~= "p^o^lnocnym-zachodzie")
      kierunek = "p^o^lnocny-zachod";
    else if (kierun ~= "d" || kierun ~= "dole")
      kierunek = "d^o^l";
    else if (kierun ~= "g" || kierun ~= "g^orze")
      kierunek = "g^ora";
    return 0;
  }
    if (przeciw == 1) {
    if (kierun ~= "p^o^lnoc") return "p^o^lnocy";
    else if (kierun ~= "p^o^lnocny-wsch^od") return "p^o^lnocnym-wschodzie";
    else if (kierun ~= "wsch^od") return "wschodzie";
    else if (kierun ~= "po^ludniowy-wsch^od") return "po^ludniowym-wschodzie";
    else if (kierun ~= "po^ludnie") return "po^ludniu";
    else if (kierun ~= "po^ludniowy-zach^od") return "po^ludniowym-zachodzie";
    else if (kierun ~= "zach^od") return "zachodzie";
    else if (kierun ~= "p^o^lnocny-zach^od") return "p^o^lnocnym-zachodzie";
    else if (kierun ~= "d^o^l") return "dole";
    else if (kierun ~= "g^ora") return "g^orze";
  }

    if (przeciw == 2) {
    if (kierun ~= "po^ludnie") return "p^o^lnocy";
    else if (kierun ~= "po^ludniowy-zach^od") return "p^o^lnocnym-wschodzie";
    else if (kierun ~= "zach^od") return "wschodzie";
    else if (kierun ~= "p^o^lnocny-zach^od") return "po^ludniowym-wschodzie";
    else if (kierun ~= "p^o^lnoc") return "po^ludniu";
    else if (kierun ~= "p^o^lnocny-wsch^od") return "po^ludniowym-zachodzie";
    else if (kierun ~= "wsch^od") return "zachodzie";
    else if (kierun ~= "po^ludniowy-wsch^od") return "p^o^lnocnym-zachodzie";
    else if (kierun ~= "g^ora") return "dole";
    else if (kierun ~= "d^o^l") return "g^orze";
  }
}


void
przenies_pocisk(object ofiara, int porazka)
{
	int val = random(100);

	pocisk->niszczymy();

//DEBUG("PRZENIES_POCISK");
  if(porazka)
  {
//DEBUG("PRZENIES_POCISK na lok");
  	switch(random(10))
  	{
  		case 0..4: //50% szans
  			if(random(2))
  				pocisk->move(ENV(ofiara), 1);
  			else
  			{
  				pocisk->move(ENV(ofiara), 1);
  				pocisk->add_prop(OBJ_I_HIDE, val);
  			}
  			break;
  		case 5..7: //30% szans
  			int rand_size = random(sizeof(ENV(ofiara)->query_exit_rooms()));
  			string rand_lok = ENV(ofiara)->query_exit_rooms()[rand_size];
  			pocisk->move(rand_lok,1);
  			break;
  		default: //20% szans
  		pocisk->destruct();
  		break;
  	}
    return;
  }
  //FIXME!!! ma si� wbi�, czy co?!
  if(interactive(ofiara)) //czy jest �YWYM playerem
  {

  	if (!random(10))
{//DEBUG("PRZENIES_POCISK do gracza - na lok!");
  		pocisk->move(environment(ofiara), 1);
		pocisk->add_prop(OBJ_I_HIDE, val);
  	}
  	else
{//DEBUG("PRZENIES_POCISK do gracza");
  		pocisk->move(ofiara, 1);
}
  }
  else
  {
//DEBUG("PRZENIES_POCISK DESTRUCT!");
	pocisk->move(ENV(ofiara),1);
	pocisk->add_prop(OBJ_I_HIDE, val);
  	//pocisk->destruct(); //FIXME!!
  }
  	return;
}

void
reakcja_npca(object ofiara, object agresor)
{
  if (environment(ofiara) == environment(agresor))
  {
    ofiara->add_prop(PROP, 1);
    ofiara->attacked_by(agresor);
    return;
  }

  if (!ofiara->query_npc()) return;
  if (!ofiara->query_prop(PROP))
  {
    ofiara->add_prop(PROP, 1);
    return;
  }

  string sciezka = file_name(environment(agresor));
  int i = member_array(sciezka, environment(ofiara)->query_exit_rooms());
  if (i == -1)
  {
  	agresor->catch_msg("Wyst^apil b^l^ad w ocenie odleg^lo^sci. "+
      "Zg^lo^s go czym pr^edzej w broni strzeleckiej.\n");
    return;
  }

  ofiara->command(environment(ofiara)->query_exit_cmds()[i]);
  ofiara->attacked_by(agresor);
}

int
przestan_paraliz()
{
	przestan("strzela�");
	return 1;
}

int
przestan(string str,int silent = 0)
{
  if (str ~= "celowa^c")
  {
    if (cel == 0 && kierunek == 0)
    {
    	if(!silent)
      		this_player()->catch_msg("Nie celujesz ju^z do nikogo.\n");
      return 1;
    }
    		
    if(get_alarm(celowanie_alarm))
    		celowanie_alarm = 0;
    		
    		
    if(!silent)
    {
    	this_player()->catch_msg("Zwalniasz ci�ciw� i przestajesz celowa^c.\n");
    	saybb(QCIMIE(TP,PL_MIA)+" zwalnia ci�ciw� i przestaje celowa�.\n");
    }
    zeruj();
    return 1;
  }
  else if (str ~= "strzela^c")
  {
  	if(get_alarm(celowanie_alarm))
    		celowanie_alarm = 0;
  
  
    paralyze->set_finish_fun("");
    paralyze->stop_paralyze();
    remove_alarm(alarm);
    
    if(!silent)
    {
    	this_player()->catch_msg("Zwalniasz ci�ciw� i przestajesz strzela�.\n");
    	saybb(QCIMIE(TP,PL_MIA)+" zawalnia ci�ciw� i przestaje strzela�.\n");
    }
    return 1;
  }
}

int
celuj(string gdzie, int cicho = 0)
{
  int i, c, do_w;
  string nazwa_celu;
  mixed *wyjscia, *poprawne, cele_access, *poprawne_access, cele;
  object lokacja;

  notify_fail("Aby celowa^c, musisz najpierw doby^c " +
    query_short(PL_DOP) + ".\n");
  if (!query_wielded()) return 0;

	if(TP->query_fatigue() < 10)
	{
		write("Nie masz ju� na to si�y.\n");
		return 1;
	}

	/*if(!sprawdz_czy_mamy_pociski())
		return 0;*/
	if(TP->query_stat(SS_STR) < min_sila)
  	{
	    notify_fail("Masz zbyt ma^lo krzepy, by u^zywa^c "+
      	query_short(PL_DOP) + ".\n");
    	return 0;
  	}

  wyjscia = environment(this_player())->query_exit();

  if (!gdzie)
  {

    switch(cel_w_zasiegu())
    {
      case 0:
        this_player()->catch_msg("Celuj do kogo [gdzie]?\n");
        break;

      case 1:
        this_player()->catch_msg("Aktualnie celujesz do " +
          QIMIE(cel, PL_DOP) +
          ", kt^or" + cel->koncowka("y","a","e") + " znajduje si^e " +
          (kierunek == 0 ? "obok ciebie.\n" :
          "na " + ustaw_kierunek(kierunek, 1) + ".\n"));
        break;

      case 2:
        this_player()->catch_msg("Tw^oj cel poruszy^l si^e, ale nadal "+
                              "znajduje si^e " +
          "w zasi^egu wzroku - na " + ustaw_kierunek(kierunek, 1) +
          ".\n");
        break;

      case 3:
        this_player()->catch_msg("Tw^oj cel znikn^a^l ci z pola widzenia.\n");
        break;

      case 4:
        this_player()->catch_msg("Tw^oj cel poruszy^l si^e, teraz znajduje"+
                                  " sie tu^z " +
          "obok ciebie!\n");
        break;

      case 5:
        this_player()->catch_msg("Aktualnie celujesz na " + kierunek + ".\n");
    }
    return 1;
  }

  if (((sscanf(gdzie, "do %s na %s", nazwa_celu, kierunek) == 2) && (do_w = 1)) ||
          ((sscanf(gdzie, "w %s na %s", nazwa_celu, kierunek) == 2) && (do_w = 2))) {

    if (!zasieg)
    {
      notify_fail(capitalize(query_short(PL_MIA))+
      " nie jest broni^a, z kt^orej mo^zna strzela^c na tak du^ze "+
      "odleg^lo^sci. Spr^obuj obra^c cel, kt^ory jest nieopodal "+
      "ciebie.\n");
      return 0;
    }

    ustaw_kierunek(kierunek);

    if ((i = member_array(kierunek, wyjscia)) != -1)
    {
      call_other(wyjscia[i - 1], "load_me");
      lokacja = find_object(wyjscia[i - 1]);
      if (!parse_command(nazwa_celu, all_inventory(lokacja), "%l:" + 
      		((do_w == 1) ? PL_DOP : PL_BIE),	cele))
      {
        notify_fail("Celuj do kogo [gdzie]?\n");
        return 0;
      }
      cele_access = cele[0];
      cele = DIRECT_ACCESS(cele_access);
      if (!sizeof(cele_access)) {
        notify_fail("Celuj do kogo [gdzie]?\n");
        return 0;
      }
      if (sizeof(cele_access) >= 2)
      {
        parse_command(nazwa_celu, cele_access, "%l:" + ((do_w == 1) ? PL_DOP : PL_BIE), poprawne);
        poprawne_access = poprawne[0];
        DIRECT_ACCESS(poprawne_access);
        if (poprawne_access[0] < 0) c = -poprawne_access[0];
        else c = poprawne_access[0];
        if (sizeof(cele_access) <= c)
        {
          notify_fail("Celuj do kogo [gdzie]?\n");
          return 0;
        }
      }
      else c = 1;
      if (c) {
        zeruj(0);
        cel = poprawne_access[c];
        lokacja_celu = environment(cel);
        lokacja_gracza = environment(this_player());
        if (!cicho)
        	hook_celuj(this_player(),cel);
        return 1;
      }
    } else {
      for (i = 1; i < sizeof(wyjscia); i += 3) {
        if (pointerp(wyjscia[i])) {
          if (kierunek == wyjscia[i][0]) {
            call_other(wyjscia[i - 1], "load_me");
            lokacja = find_object(wyjscia[i - 1]);
            if (!parse_command(nazwa_celu, all_inventory(lokacja), "%l:" +
                        ((do_w == 1) ? PL_DOP : PL_BIE), cele)) {
              notify_fail("Celuj do kogo [gdzie]?\n");
              return 0;
            }
            cele_access = cele[0];
            cele = DIRECT_ACCESS(cele_access);
            if (!sizeof(cele_access)) {
              notify_fail("Celuj do kogo [gdzie]?\n");
              return 0;
            }
            if (sizeof(cele_access) >= 2) {
              parse_command(nazwa_celu, cele_access, "%l:" + ((do_w == 1) ? PL_DOP : PL_BIE), poprawne);

              poprawne_access = poprawne[0];
              DIRECT_ACCESS(poprawne_access);
              if (poprawne_access[0] < 0) c = -poprawne_access[0];
              else c = poprawne_access[0];
              if (sizeof(cele_access) <= c) {
                notify_fail("Celuj do kogo [gdzie]?\n");
                return 0;
              }
            }
            else c = 1;
            if (c) {
              zeruj(0);
              cel = poprawne_access[c];
              lokacja_celu = environment(cel);
              lokacja_gracza = environment(this_player());
              if (!cicho)
        		hook_celuj(this_player(),cel);
              return 1;
            }
          }
        }
      }
    }
  }

  else if (((sscanf(gdzie, "do %s", nazwa_celu) == 1) && (do_w = 1)) ||
          ((sscanf(gdzie, "w %s", nazwa_celu) == 1) && (do_w = 2))) {
    lokacja = environment(this_player());
    if (!parse_command(nazwa_celu, all_inventory(lokacja) - ({ this_player()
}), "%l:" + ((do_w == 1) ? PL_DOP : PL_BIE), cele)) {
        notify_fail("Celuj do kogo [gdzie]?\n");
        return 0;
    }
    cele_access = cele[0];
    cele = NORMAL_ACCESS(cele_access, 0, 0);
    if (sizeof(cele_access) >= 2) {
      parse_command(nazwa_celu, cele_access, "%l:" + ((do_w == 1) ? PL_DOP : PL_BIE), poprawne);
      poprawne_access = poprawne[0];
      DIRECT_ACCESS(poprawne_access);
      if (poprawne_access[0] < 0) c = -poprawne_access[0];
      else c = poprawne_access[0];
      if (sizeof(cele_access) <= c) {
        notify_fail("Celuj do kogo [gdzie]?\n");
        return 0;
      }
    }
    else c = 1;
    if (c) {
      zeruj();
      cel = poprawne_access[c];
      lokacja_celu = environment(cel);
      lokacja_gracza = environment(this_player());
      if (!cicho)
        	hook_celuj(this_player(),cel);
      return 1;
    }
  }

/*
  else if (sscanf(gdzie, "na %s", kierunek) == 1) {

    if (!zasieg)
    {
      notify_fail(capitalize(query_short(PL_MIA))+
      " nie jest broni^a, z kt^orej mo^zna strzela^c na tak du^ze "+
      "odleg^lo^sci. Spr^obuj obra^c cel, kt^ory jest nieopodal "+
      "ciebie.\n");
      return 0;
    }

    ustaw_kierunek(kierunek);
    zeruj(0);
    if (!cicho) this_player()->catch_msg("Kierujesz sw" + this_object()-
>koncowka("^oj","oj^a","oje")+
      " " + short(this_object(), PL_BIE) + " na " + kierunek + ".\n");
    return 1;
  }
*/

  notify_fail("Celuj do kogo [gdzie]?\n");
  return 0;
}

int
lec()
{
  int x = oblicz_czy_trafil(this_player(), cel);
  dodaj_ss_expa(this_player(), x);


	if(ENV(TP) == ENV(cel))
	{
		TP->attack_object(cel);
    	TP->add_prop(LIVE_O_LAST_KILL, cel);
    	
    	//i ju� lecimy z koksem dalej!
    	alarm = set_alarm(1.0,0.0,"strzel");
    }

  switch(x)
  {

    /* Chybienie i krytyczne chybienie. 5% szans na trafienie osoby postronnej. */
    case -1:
    case 0:

    if (lokacja_celu == lokacja_gracza)
    {
      object *lista = all_inventory(environment(this_player()));
      lista = filter(lista, living);
      lista -= ({this_player(), cel});

      if (!random(20) && sizeof(lista) != 0)
      {
        object ob = lista[random(sizeof(lista))];
        this_player()->catch_msg("Wypuszczon" + pocisk->koncowka("y","a","e") +

          " przez ciebie " + pocisk->query_short(PL_MIA)+
          " chybia celu, przelatuj^ac obok " +
          QIMIE(cel, PL_DOP) +
          ". Pocisk leci jednak tak niefortunnie, ^ze godzi "+
          "w znajduj^a" + ob->koncowka("cego", "c^a", "ce") +
          " si^e obok " + QIMIE(ob, PL_BIE)+".\n");
        cel->catch_msg("Wypuszczon" + pocisk->koncowka("y","a","e") +
          " przez " + QIMIE(this_player(), PL_BIE) + " " +
          pocisk->query_short(PL_MIA)+
          " chybia, przelatuj^ac obok ciebie. "+
          "Pocisk leci jednak tak niefortunnie, ^ze godzi "+
          "w znajduj^a" + ob->koncowka("cego", "c^a", "ce") +
          " si^e obok " + QIMIE(ob, PL_BIE)+".\n");
        ob->catch_msg("Wypuszczon" + pocisk->koncowka("y","a","e") +
          " przez " + QIMIE(this_player(), PL_BIE) + " " +
          pocisk->query_short(PL_MIA)+
          " chybia celu, przelatuj^ac obok " +
          QIMIE(cel, PL_DOP) +
          ". Pocisk leci jednak tak niefortunnie, ^ze godzi "+
          "prosto w ciebie!\n");
        tell_room(environment(this_player()),
          "Wypuszczon" + pocisk->koncowka("y","a","e") +
          " przez " + QIMIE(this_player(), PL_BIE) + " " +
          pocisk->query_short( PL_MIA)+
          " chybia celu, przelatuj^ac obok " +
          QIMIE(cel, PL_DOP) +
          ". Pocisk leci jednak tak niefortunnie, ^ze godzi "+
          "w znajduj^a" + ob->koncowka("cego", "c^a", "ce") +
          " si^e obok " + QIMIE(ob, PL_BIE)+".\n",
          ({this_player(), cel, ob}));

        ob->set_hp(ob->query_hp() -
	    oblicz_obrazenia(this_player(), ob, pocisk, 0));

	//zgon jesli trzeba
	if (ob->query_hp() <= 0)
	    ob->do_die();
	
        ob->attacked_by(this_player());
        pocisk->efekt_specjalny(this_player(), ob, this_object());
        przenies_pocisk(ob, 0);
        return 1;
      }

      this_player()->catch_msg("Wypuszczon" + pocisk->koncowka("y","a","e") +
          " przez ciebie " + pocisk->query_short( PL_MIA)+
          " chybia celu, przelatuj^ac obok " +
          QIMIE(cel, PL_DOP) + ".\n");
      cel->catch_msg("Wypuszczon" + pocisk->koncowka("y","a","e") +
          " przez " + QIMIE(this_player(), PL_BIE) + " " +
          pocisk->query_short( PL_MIA)+
          " chybia, przelatuj^ac obok ciebie.\n");
      tell_room(environment(this_player()),
          "Wypuszczon" + pocisk->koncowka("y","a","e") +
          " przez " + QIMIE(this_player(), PL_BIE) + " " +
          pocisk->query_short(PL_MIA)+
          " chybia celu, przelatuj^ac obok " +
          QIMIE(cel, PL_DOP) + ".\n",
          ({this_player(), cel}));
      przenies_pocisk(cel, 1);
      return 1;
    }

    this_player()->catch_msg("Tw" + pocisk->koncowka("^oj","oja","oje") + " " +

      pocisk->query_short(PL_MIA) + " polecia^l"+
      pocisk->koncowka("","a","o") + " daleko na " +
      kierunek + ".\n");
    tell_room(lokacja_gracza, capitalize(pocisk->short(this_player(),PL_MIA))+

      " wystrzelon" + pocisk->koncowka("y","a","e") + " przez " +
      QIMIE(this_player(), PL_BIE) + " polecia^l" +
      pocisk->koncowka("","a","o") + " daleko na " +
      kierunek + ".\n", ({this_player()}));
    cel->catch_msg("Nagle na " + ustaw_kierunek(kierunek,2) +
      " dostrzegasz lec^ac" + pocisk->koncowka("y","^a","e")+
      " prosto w ciebie " + pocisk->query_short(PL_BIE)+
      "! Na szcz^escie pocisk nie trafia ci^e, mijaj^ac w "+
      "bezpiecznej odleg^lo^sci.\n");
    tell_room(lokacja_celu, "Nagle na " + ustaw_kierunek(kierunek,2)+
      " dostrzegasz lec^ac" + pocisk->koncowka("y","^a","e")+
      " prosto w " + QIMIE(cel, PL_BIE) + " " +
      pocisk->query_short(PL_BIE)+
      "! Pocisk nie trafia jednak " + cel->query_rasa(PL_DOP) +
      ", mijaj^ac " + cel->query_zaimek(PL_BIE, 0) +
      " w bezpiecznej odleg^lo^sci.\n", ({cel}));
    przenies_pocisk(cel, 1);
    return 1;
    break;

    // Trafienie
    case 1:
    if (lokacja_celu == lokacja_gracza)
    {
      this_player()->catch_msg("Tw" + pocisk->koncowka("^oj","oja","oje") + " "
+
        pocisk->query_short(PL_MIA) + " trafi^l"+
        pocisk->koncowka("","a","o") + " w " +
        QIMIE(cel, PL_BIE) + ".\n");
      tell_room(lokacja_gracza, capitalize(pocisk->short(this_player(),PL_MIA))+

      " wystrzelon" + pocisk->koncowka("y","a","e") + " przez " +
        QIMIE(this_player(), PL_BIE) + " trafi^l" +
        pocisk->koncowka("","a","o") + " w " +
        QIMIE(cel, PL_BIE) + ".\n",
        ({this_player(), cel}));
      cel->catch_msg(capitalize(pocisk->short(this_player(),PL_MIA))+
      " wystrzelon" + pocisk->koncowka("y","a","e") + " przez " +
        QIMIE(this_player(), PL_BIE) + " trafi^l" +
        pocisk->koncowka("","a","o") + " cie!\n");
      cel->set_hp(cel->query_hp() -
    	oblicz_obrazenia(this_player(), cel, pocisk, 0));
    	
    if (cel->query_hp() <= 0)
		cel->do_die();
	
      cel->attacked_by(this_player());
      pocisk->efekt_specjalny(this_player(), cel, this_object());
      przenies_pocisk(cel, 0);
      return 1;
    }

    else
    {
    	if(cel_w_zasiegu() ==3)
    	{
    		TP->catch_msg("Cel umkn�� z twojego pola widzenia.\n");
    		return 1;
    	}
    
    this_player()->catch_msg("Tw" + pocisk->koncowka("^oj","oja","oje") + " " +

      pocisk->query_short(PL_MIA) + " polecia^l"+
      pocisk->koncowka("","a","o") + " daleko na " +
      kierunek + ".\n");
    tell_room(lokacja_gracza, capitalize(pocisk->short(this_player(),PL_MIA))+

      " wystrzelon" + pocisk->koncowka("y","a","e") + " przez " +
      QIMIE(this_player(), PL_BIE) + " polecia^l" +
      pocisk->koncowka("","a","o") + " daleko na " +
      kierunek + ".\n", ({this_player()}));
    cel->catch_msg("Nagle na " + ustaw_kierunek(kierunek,2) +
      " dostrzegasz lec^ac" + pocisk->koncowka("y","^a","e")+
      " prosto w ciebie " + pocisk->query_short(PL_BIE)+
      "! Pocisk trafia ci^e!\n");
    tell_room(lokacja_celu, "Nagle na " + ustaw_kierunek(kierunek,2)+
      " dostrzegasz lec^ac" + pocisk->koncowka("y","^a","e")+
      " prosto w " + QIMIE(cel, PL_BIE) + " " +
      pocisk->query_short(PL_BIE) +
      "! Pocisk trafia " + cel->query_rasa(PL_BIE) + ".\n", ({cel}));

    cel->set_hp(cel->query_hp() -
	oblicz_obrazenia(this_player(), cel, pocisk, 0));
    if (cel->query_hp() <= 0)
	cel->do_die();
    pocisk->efekt_specjalny(this_player(), cel, this_object());
    if (cel->query_npc()) reakcja_npca(cel, this_player());
    przenies_pocisk(cel, 0);
    return 1;
    }

    break;

    // Trafienie krytyczne
    case 2:
    if (lokacja_celu == lokacja_gracza)
    {
      this_player()->catch_msg("Tw" + pocisk->koncowka("^oj","oja","oje") + " "
+
        pocisk->query_short(PL_MIA) + " wyj^atkowo mocno i "+
        "celnie trafi^l"+
        pocisk->koncowka("","a","o") + " w " +
        QIMIE(cel, PL_BIE) + ".\n");
      tell_room(lokacja_gracza, capitalize(pocisk->short(this_player(),PL_MIA))+

      " wystrzelon" + pocisk->koncowka("y","a","e") + " przez " +
        QIMIE(this_player(), PL_BIE) + " wyj^atkowo mocno i celnie trafi^l" +
        pocisk->koncowka("","a","o") + " w " +
        QIMIE(cel, PL_BIE) + ".\n",
        ({this_player(), cel}));
      cel->catch_msg(capitalize(pocisk->short(this_player(),PL_MIA))+
      " wystrzelon" + pocisk->koncowka("y","a","e") + " przez " +
        QIMIE(this_player(), PL_BIE) + " trafi^l" +
        pocisk->koncowka("","a","o") + " cie wyj^atkowo mocno!\n");
      cel->set_hp(cel->query_hp() - oblicz_obrazenia(this_player(), cel, pocisk, 1));
      if (cel->query_hp())
        cel->do_die();
	
      cel->attacked_by(this_player());
      przenies_pocisk(cel, 0);
      return 1;
    }

    else
    {
    	if(cel_w_zasiegu() ==3)
    	{
    		TP->catch_msg("Cel umkn�� z twojego pola widzenia.\n");
    		return 1;
    	}
    
    this_player()->catch_msg("Tw" + pocisk->koncowka("^oj","oja","oje") + " " +

      pocisk->query_short(PL_MIA) + " polecia^l"+
      pocisk->koncowka("","a","o") + " wyj^atkowo celnie i pr^edko na " +
      kierunek + ".\n");
    tell_room(lokacja_gracza, capitalize(pocisk->short(this_player(),PL_MIA))+

      " wystrzelon" + pocisk->koncowka("y","a","e") + " przez " +
      QIMIE(this_player(), PL_BIE) + " polecia^l" +
      pocisk->koncowka("","a","o") + " wyj^atkowo celnie i pr^edko na " +
      kierunek + ".\n", ({this_player()}));
    cel->catch_msg("Nagle na " + ustaw_kierunek(kierunek,2) +
      " dostrzegasz lec^ac" + pocisk->koncowka("y","^a","e")+
      " prosto w ciebie " + pocisk->query_short(PL_BIE)+
      "! Pocisk trafia ci^e wyj^atkowo mocno!\n");
    tell_room(lokacja_celu, "Nagle na " + ustaw_kierunek(kierunek,2)+
      " dostrzegasz lec^ac" + pocisk->koncowka("y","^a","e")+
      " prosto w " + QIMIE(cel, PL_BIE) + " " +
      pocisk->query_short(PL_BIE) +
      "! Pocisk trafia " + cel->query_rasa(PL_BIE) + " "+
      "wyj^atkowo mocno.\n", ({cel}));

    cel->set_hp(cel->query_hp() - oblicz_obrazenia(this_player(), cel, pocisk,1));
    
    if (cel->query_hp() <= 0)
		cel->do_die();
		
    if (cel->query_npc())
    	reakcja_npca(cel, this_player());
    	
    przenies_pocisk(cel, 0);
    
    return 1;
    }
    break;

  this_player()->catch_msg("Wystapi^l b^lad broni strzeleckiej. Nie "+
                          "zaszed^l ^zaden "+
    "z przewidzianych przypadk^ow.\n");
  return 1;
  }
}

void
opisz_strzal()
{
  this_player()->remove_stun();

	TP->set_fatigue(TP->query_fatigue() - random(7));

//    DEBUG("1");

  int opis = cel_w_zasiegu();
  if (opis == 2)
   this_player()->catch_msg("Zauwa^zasz, ^ze tw^oj cel poruszy^l si^e,"+
                           " ale nadal " +
   "znajduje si^e w zasi^egu wzroku.\n");

  if (opis == 4)
   this_player()->catch_msg("Zauwa^zasz, ^ze teraz tw^oj cel znajduje si^e "+
                              "tu^z obok " +
        "ciebie!\n");


	if(!TP->query_fatigue())
	{
		write("Zm�czenie nie pozwala ci na kontynuowanie strza�u.\n");
		if(get_alarm(alarm))
			alarm = 0;
		if(get_alarm(celowanie_alarm))
			celowanie_alarm = 0;
			
		return;
	}

//    DEBUG("2");
  if (lokacja_gracza == lokacja_celu)
  {
    //DEBUG("3 " + file_name(lokacja_gracza) + " " + file_name(lokacja_celu));
    this_player()->catch_msg("Strzelasz z " + this_object()->query_short(PL_DOP)+

      ", usi^luj^ac pos^la^c wypuszczon" + pocisk->koncowka("y","^a","e") + " "
+
      pocisk->query_short(PL_BIE) +
      " w kierunku " + QIMIE(cel, PL_DOP) + ".\n");
    cel->catch_msg(QCIMIE(this_player(), PL_MIA) + " strzela z "+
      this_object()->query_short(PL_DOP)+
      ", usi^luj^ac pos^la^c wypuszczon" + pocisk->koncowka("y","^a","e") + " "
+
            pocisk->query_short(PL_BIE) +
     " w twym kierunku!\n");
    tell_room(lokacja_gracza, QCIMIE(this_player(), PL_MIA) + " strzela z "+
      this_object()->query_short(PL_DOP)+
      ", usi^luj^ac pos^la^c wypuszczon" + pocisk->koncowka("y","^a","e") + " "
+
            pocisk->query_short(PL_BIE) +
      " leci w kierunku " + QIMIE(cel, PL_DOP) + ".\n",
      ({this_player(), cel}));
    //DEBUG("3a");
  }

  else
  {
//    DEBUG("4");
    this_player()->catch_msg("Strzelasz z " + this_object()->query_short(PL_DOP)+

      ", usi^luj^ac pos^la^c wypuszczon" + pocisk->koncowka("y","^a","e") + " "
+
      pocisk->query_short(PL_BIE) +
      " na " + kierunek + ".\n");
    tell_room(lokacja_gracza, QCIMIE(this_player(), PL_MIA) + " strzela z "+
      this_object()->query_short(PL_DOP)+
      ", usi^luj^ac pos^la^c wypuszczon" + pocisk->koncowka("y","^a","e") + " "
+
      pocisk->query_short(PL_BIE) +
      " na " + kierunek + ".\n", ({this_player()}));
  }

}

int
strzel(string gdzie)
{
  int odl;
  string *wyjscia;
  mixed *cele;


  paralyze = clone_object("/std/paralyze");
  paralyze->set_name("strzela");
  paralyze->set_standard_paralyze("strzela^c");
  paralyze->set_stop_verb("przesta�");
  //paralyze->set_stop_message("Przestajesz strzela�.\n");
  paralyze->set_finish_fun("opisz_strzal");
  paralyze->set_finish_object(this_object());
  paralyze->set_stop_object(TO);
  paralyze->set_stop_fun("przestan_paraliz");
  paralyze->set_remove_time(19 - query_szybkosc());
  paralyze->set_fail_message("Koncentrujesz si^e na strzale, nie mo^zesz " +
    "teraz zrobi^c niczego innego!\n");

  notify_fail("Aby celowa^c, musisz najpierw doby^c " +
    query_short(PL_DOP) + ".\n");
  if (!query_wielded()) return 0;

  if (this_player()->query_stat(SS_STR) < min_sila)
  {
    notify_fail("Masz zbyt ma^lo krzepy, by u^zywa^c "+
      query_short(PL_DOP) + ".\n");
    return 0;
  }

  if (gdzie)
  {
    if (!celuj(gdzie, 1))
    {
      notify_fail("Strzel do kogo [gdzie]?\n");
      return 0;
    }
  }

  if (cel == 0)
  {
    notify_fail("Musisz wybra^c jaki^s cel.\n");
    return 0;
  }

  odl = cel_w_zasiegu();

  /*
  if (odl == 5) {
    if ((i = member_array(kierunek, (wyjscia = environment(this_player())-
>query_exit()))) != -1) {
      call_other(wyjscia[i - 1], "load_me");
      cele = FILTER_LIVE(all_inventory(find_object(wyjscia[i - 1])));
      i = random(sizeof(cele));
      cel = cele[i];
    } else {
      zeruj();
      notify_fail("Strzel do kogo [gdzie]?\n");
    }

  }
  */

  if (odl == 3)
  {
    notify_fail("Tw^oj cel znikn^a^l ci z pola widzenia.\n");
    return 0;
  }

	int i;
	object *ekwipunek = all_inventory(TP)->query_nazwa();

	if((i = sprawdz_czy_mamy_pociski(pocisk_opis, ekwipunek)) == -1)
		return 0;
	else
	{
		ekwipunek = all_inventory(TP);
    	pocisk = ekwipunek[i];
    	
    	//je�li nie wymierzymy wcze�niej..
    	if(get_alarm(celowanie_alarm) == 0)
    	{	
    		//przygotowanie(TP, TO, pocisk);
    		hook_celuj(TP, cel);
    	}
    	
    		
    	paralyze->move(TP, 1);
    	TP->add_stun();
    	
    		//TODO: po uprzednim wycelowaniu pierwszy strza� ma leciec od razu
    	//if(TP->query_attack() != cel && get_alarm(celowanie_alarm))
   		alarm = set_alarm(itof(19 - query_szybkosc()) + 0.7, 0.0, lec);
   		
   		return 1;
	}

  notify_fail("Strzel do kogo [gdzie]?\n");
  return 0;
}

public int
pomoc(string str)
{
  object strzel;
  notify_fail("Nie ma pomocy na ten temat.\n");
  if (!str) return 0;
  if (!parse_command(str, this_player(), "%o" + PL_MIA, strzel)) return 0;
  if (strzel != this_object()) return 0;
  this_player()->catch_msg(capitalize(this_object()->short()) + 
      " jest bez w^atpienia broni^a "+
    "strzeleck^a i jako taka mo^ze z pewno^scia zosta^c dobyta. Za jej "+
    "pomoc^a mo^zna przy odrobinie dobrych ch^eci - po uprzednim wycelowaniu "+
              "-"+
    "strzeli^c do kogo^s, kogo si^e upatrzy^lo. Oczywi^scie w ka^zdej chwili "+

    "mo^zna przesta^c celowa^c.\n");
  return 1;
}

nomask void
init()
{
  ::init();
  add_action(celuj, "celuj");
  add_action(strzel, "strzel");
  // add_action(strzelaj, "strzelaj");
  add_action(przestan, "przestan");
  add_action(pomoc, "?", 2);
  zeruj();
  strzelecka_init();
}
