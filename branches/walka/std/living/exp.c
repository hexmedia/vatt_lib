/**
 * \file /std/living/exp.c
 *
 * Ten plik zawiera funkcje do obsługi nowego systemu expa.
 */

/* co ile czasu spada ss */
#define EXP_INTERVAL 1800
/* defaultowa wielkość, o jaką spada ss */
#define EXP_DEFAULT_STEP 4
/* przez ile czasu od ostatniego użycia spada ss */
#define EXP_USAGE_INTERVAL 86400

/* mapping ze szczegółowymi wielkościami, o ile spadają ss */
#define EXP_STEPS_MAP ([\
    SS_STR:10,\
    SS_DEX:8,\
    SS_CON:7,\
    SS_INT:3,\
    SS_WIS:2,\
    SS_DIS:1\
    ])

/* --- */

#include <macros.h>

mapping exp_ss_map;
int exp_last_decay;

static int exp_alrm_id = 0;
static mapping exp_delta = ([]);    /* Zmiany w doświadczeniu poszczególnych ss */
static int exp_stat_delta = 0;
static int exp_skill_delta = 0;

/**
 * Funkcja zmieniająca wartość doświadczenia związanego z ss.
 *
 * @param ss numer cechy/umiejętności
 * @param amount wielkość doświadczenia
 */
public void
increase_ss(int ss, int amount)
{
//  write("DEBUG: increase_ss(" + ss + ", " + amount + ")\n");
  if (ss < SS_NO_STATS) {
    set_acc_exp(ss, query_acc_exp(ss) + amount);
    exp_delta[ss] += amount;
    exp_stat_delta += amount;
    update_stat(ss);
  }
  else {
    set_acc_exp(ss, query_acc_exp(ss) + amount);
    exp_delta[ss] += amount;
    exp_skill_delta += amount;
    update_skill(ss);
  }

  if (!mappingp(exp_ss_map)) {
    exp_ss_map = ([]);
  }

  if (!pointerp(exp_ss_map[ss])) {
    int step = EXP_DEFAULT_STEP;
    if (EXP_STEPS_MAP[ss] > 0) {
      step = EXP_STEPS_MAP[ss];
    }
    exp_ss_map[ss] = ({time(), step});
  }
}

/**
 * Funkcja odpowiedzialna za zmniejszanie cech/umiejętności.
 */
public void
decrease_ss()
{
  int i, size, cur_time;
  int* itab, *curtab;

  if (!mappingp(exp_ss_map)) {
    exp_ss_map = ([]);
  }
  
  itab = m_indices(exp_ss_map);
  size = sizeof(itab);
  cur_time = time();

  for (i = 0; i < size; ++i) {
//    write("DEBUG: decrease_ss(): decreasing=" + itab[i] + "\n");
    curtab = exp_ss_map[itab[i]];
//    write("DEBUG: decrease_ss(): last usage=" + (cur_time - curtab[0]) + "\n");
    if ((cur_time - curtab[0]) < EXP_USAGE_INTERVAL) {
      increase_ss(itab[i], -curtab[1]);
    }
  }
  if (!exp_alrm_id) {
    exp_alrm_id = set_alarm(itof(EXP_INTERVAL), itof(EXP_INTERVAL), decrease_ss);
  }

  exp_last_decay = cur_time;
}

/**
 * Uruchamia proces zmniejszania się cech/umiejętności.
 */
public void
start_ss_decay()
{
  int t_in, t_out, i, size, t_delta, to_decr;
  int* itab, *curtab;
  t_in = time();
  t_out = SECURITY->query_player_file_time(query_real_name());
  t_delta = t_in - t_out;
//  write("DEBUG: start_ss_decay(): logout time=" + t_delta + ", last decay=" + exp_last_decay + "\n");

  if (!mappingp(exp_ss_map)) {
    exp_ss_map = ([]);
  }
  
  itab = m_indices(exp_ss_map);
  size = sizeof(itab);

  for (i = 0; i < size; ++i) {
//    write("DEBUG: start_ss_decay(): updating=" + itab[i] + "\n");
    curtab = exp_ss_map[itab[i]];
    curtab[0] = curtab[0] + t_delta;
  }

  exp_last_decay += t_delta;

  if (!exp_alrm_id) {
    to_decr = 0;
    if ((t_in - exp_last_decay) < EXP_INTERVAL) {
      to_decr = EXP_INTERVAL - t_in + exp_last_decay;
    }
    exp_alrm_id = set_alarm(itof(to_decr), itof(EXP_INTERVAL), decrease_ss);
  }
}

/**
 * Funkcja zwracająca wielkość zmian doświadczenia związanego z cechami.
 *
 * @return Wielkość zmian doświadczenia związanego z cechami.
 */
public int
query_exp_stat_delta()
{
    return exp_stat_delta;
}

/**
 * Funkcja zwracająca wielkość zmian doświadczenia związanego z umiejętnościami.
 *
 * @return Wielkość zmian doświadczenia związanego z umiejętnościami.
 */
public int
query_exp_skill_delta()
{
    return exp_skill_delta;
}
