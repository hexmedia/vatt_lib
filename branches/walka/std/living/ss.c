/**
 * \file /std/living/ss.c
 *
 * Plik ten jest cz�ci� living.c
 * Znajduj� si� tutaj funkcje do obs�ugi cech i umiej�tno�ci.
 *
 * Plik ten includowany jest do living.c
 */

#include <macros.h>
#include <ss_types.h>

static int *delta_stat;  /* Tymczasowe dodatkowe cechy */
static int *stats;       /* Cechy wyliczone z acc_exp */
static int *stat_extra;  /* Dodatkowe warto�ci do cech */
static private mapping skillmap; /* Warto�ci cech */
static private mapping skill_extra_map; /* Warto�ci bonus�w do cech */

public int *query_all_skill_types();
nomask int query_base_skill(int skill);

/**
 * Resetuje cechy i umiej�tno�ci przy starcie postaci.
 */
static void
ss_reset()
{
    stats = allocate(SS_NO_STATS); 
    delta_stat = allocate(SS_NO_STATS);

    acc_exp = allocate(SS_NO_STATS); 
    stat_extra = allocate(SS_NO_STATS);
}

/* --< cechy >-- */

/**
 * Ustawia dodatkow� warto�� do dodania do normalnych cech.
 *
 * Mo�e to by� u�ywane przez jakie� przedmioty, kt�re przy ich
 * noszeniu zwi�kszaj� cechy.
 *
 * @param stat numer cechy
 * @param val warto��, o jak� cecha ma by� podniesiona
 *
 * @return Warto��, o jak� cecha ma by� podniesiona.
 */
public int
set_stat_extra(int stat, int val)
{
    if ((stat < 0) || (stat >= SS_NO_STATS))
        return 0;

    stat_extra[stat] = val;
    return val;
}

/**
 * Zwraca warto�� dodawan� do normalnych cech.
 *
 * @param stat numer cechy
 *
 * @return Warto��, o jak� dana cecha jest podnoszona.
 */
public int
query_stat_extra(int stat)
{
    if ((stat < 0) || (stat >= SS_NO_STATS))
        return 0;

    return stat_extra[stat];
}

/**
 * Ustawia warto�� bazow� cechy.
 *
 * @param stat numer cechy
 * @param val nowa warto�� cechy
 *
 * @return Warto�� cechy albo 0, je�eli ustawienie zako�czy�o si�
 * niepowodzeniem.
 */
int
set_base_stat(int stat, int val)
{
    if ((stat < 0) || (stat >= SS_NO_STATS) || (val < 1))
        return 0;

//    write("LIVING: set_base_stat("+stat+","+val+")\n");
    stats[stat] = val;
    return val;
}  

/**
 * Zwraca warto�� bazow� cechy.
 *
 * @param stat numer cechy
 *
 * @return Warto�� cechy albo -1, je�eli pobranie zako�czy�o si�
 * niepowodzeniem.
 */
public int
query_base_stat(int stat)
{
    if (stat < 0 || stat >= SS_NO_STATS)
        return -1;
    return stats[stat];
}

/**
 * Zwraca warto�� �redni� trzech cech bojowych, tzn.:
 * <ul>
 *   <li> si�y
 *   <li> zr�czno�ci
 *   <li> kondycji
 * </ul>
 *
 * @return Wyliczona �rednia cech bojowych.
 */
public int
query_cmb_average_stat()
{
    return (query_base_stat(SS_STR) + query_base_stat(SS_DEX) +
            query_base_stat(SS_CON)) / 3;
}   

/**
 * Zwraca warto�� �redni� wszystkich cech.
 *
 * @return Wyliczona �rednia wszystkich cech.
 */
public int
query_average_stat()
{
    return (query_base_stat(SS_STR) + query_base_stat(SS_DEX) +
            query_base_stat(SS_CON) + query_base_stat(SS_INT) +
            query_base_stat(SS_WIS) + query_base_stat(SS_DIS)) / 6;
}   

/**
 * Zwraca sum� wszystkich skilli.
 *
 * @return Suma wszystkich skilli.
 */
public int
query_sum_skill()
{
    int sum = 0;
    foreach (int skill : query_all_skill_types())
        sum += query_base_skill(skill);
    if (!sum)
        return 1;
    return sum;
}

/**
 * Usuwa informacje o tymczasowych dodatkach do cech.
 *
 * @param stat numer cechy
 * @param value warto�� do odj�cia
 */
void
expire_tmp_stat(int stat, int value)
{
    delta_stat[stat] -= value;
}

/**
 * Dodaje tymczasowy bonus do cechy.
 *
 * @param stat numer cechy
 * @param ds o ile zmieni� ma si� cecha
 * @param dt ile okres�w F_INTERVAL_BETWEEN_HP_HEALING ma by� wa�na
 * zmiana
 *
 * @return <ul>
 *           <li> <b>1</b> - uda�o si� doda� zmian�
 *           <li> <b>0</b> - nie uda�o si� doda� zmiany
 *         </ul>
 */
public int
add_tmp_stat(int stat, int ds, int dt)
{
    int tmp, i, start;
    int *end;

    tmp = query_stat(stat) - query_base_stat(stat);

    if (ds + tmp > 10 + query_base_stat(stat) / 10 || dt <= 0)
        return 0;

    dt = MIN(dt, F_TMP_STAT_MAX_TIME);

    delta_stat[stat] += ds;
    set_alarm(itof(dt * F_INTERVAL_BETWEEN_HP_HEALING), 0.0, &expire_tmp_stat(stat, ds));

    return 1;
}

/**
 * Zwraca wynikow� warto�� cechy. Nigdy nie jest ona mniejsza ni� 1
 *
 * @param stat numer cechy
 *
 * @return Wynikowa warto�� cechy albo -1, je�eli nie uda�o si� obliczy�.
 */
public int
query_stat(int stat)
{
    int i, tmp;

    if (stat < 0 || stat >= SS_NO_STATS)
        return -1;

    tmp = query_base_stat(stat);
    tmp += delta_stat[stat];
    tmp += stat_extra[stat];

    return (tmp > 0 ? tmp : 1);
}

/**
 * T�umaczy podan� warto�� do�wiadczenia na wielko�� cechy.
 *
 * @param exp warto�� do�wiadczenia
 *
 * @return Warto�� cechy wyliczona na podstawie podanego do�wiadczenia.
 */
nomask int
exp_to_stat(int exp)
{
    return F_EXP_TO_STAT(exp);
}

/**
 * T�umaczy podan� wielko�� cechy na warto�� do�wiadczenia.
 *
 * @param stat wielko�� cechy
 *
 * @return Warto�� do�wiadczenia wyliczona na podstawie podanej
 * wielko�ci cechy.
 */
nomask int
stat_to_exp(int stat)
{
    return F_STAT_TO_EXP(stat);
}

/**
 * Uaktualnia wielko�� cechy do poziomu wyznaczanego przez do�wiadczenie.
 *
 * @param stat numer cechy
 */
public void
update_stat(int stat)
{
//	write("DEBUG: update_stat\n");
    set_base_stat(stat, exp_to_stat(query_acc_exp(stat)));
}

/**
 * T�umaczy aktualne bazowe warto�ci cech na acc_exp.
 *
 * Funkcja ta jest u�ywana tylko przy domy�lnym konfigurowaniu gracza
 * w player_sec::new_init()
 */
static void
stats_to_acc_exp()
{
    int il, tmp;

//    write("DEBUG: stats_to_acc_exp()\n");
    for (il = SS_STR; il < SS_NO_STATS; il++) {
        tmp = stat_to_exp(query_base_stat(il));
        if (tmp > 0) {
            set_acc_exp(il, tmp);
        }
        else {
            set_acc_exp(il, 0);
        }
    }
}

/**
 * T�umaczy do�wiadczenie cech na ich warto�ci.
 */
static void
acc_exp_to_stats()
{
    int il;

//    write("DEBUG: acc_exp_to_stats()\n");
    for (il = SS_STR; il < SS_NO_STATS; il++) {
        if (query_base_stat(il) >= 0) {
            update_stat(il);
        }
    }
}

/**
 * Zwraca losow� liczb� zale�n� od numeru obiektu gracza i numeru
 * podanego obiektu.
 *
 * Liczba ta zawsze b�dzie taka sama dla tego samego obiektu.
 *
 * @param ival g�rna granica przedzia�u, z kt�rego ma pochodzi� liczba
 * @param obj obiekt
 *
 * @return Losowa liczba z przedzia�u [0, ival] lub -1, je�eli podany
 * obiekt nie istnieje.
 */
public nomask int
object_random(int ival, object obj)
{
    string *list, s_num;
    int num;

    if (!objectp(obj))
        return -1;

    list = explode(file_name(this_object()), "#");
    if (sizeof(list) > 1)
        s_num = list[1];
    else
        s_num = "0";
    list = explode(file_name(obj), "#");
    s_num += list[1];
    sscanf(s_num, "%d", num);

    return random(ival, num);
}

/**
 * Zwraca obiekt odpowiedzialny za opis danej cechy.
 *
 * @param stat numer cechy
 *
 * @return Obiekt, kt�ry jest odpowiedzialny za opis danej cechy.
 */
public object
find_stat_describer(int stat)
{
    string *obf;
    int il;
    object ob;

    obf = query_textgivers();

    for (il = 0; il < sizeof(obf); il++) {
        ob = find_object(obf[il]);
        if (!ob) {
            catch(obf[il]->teleledningsanka());
            ob = find_object(obf[il]);
        }
        if (ob && ob->desc_stat(stat))
            return ob;
    }
    return 0;
}

/**
 * Funkcja pomocnicza, do u�ytku tylko przez aministrator�w muda.
 *
 * <b>Nigdy</b> nie powinno si� wywo�ywa� tej funkcji, z �adnego
 * powodu.
 *
 * @param s numer cechy
 * @param to nowa warto�� cechy
 *
 * @return <ul>
 *           <li><b>1</b> je�eli uda�o si� zmieni� warto�� cechy
 *           <li><b>0</b> je�eli nie uda�o si� zmieni� warto�� cechy
 *         </ul>
 */
public nomask int
modify_stat(int s, int to)
{
    int from;
    string log;

    if (s < SS_STR || s >= SS_NO_STATS)
        return 0;

    from = query_base_stat(s);
    set_base_stat(s,to);

    SECURITY->log_syslog("CHANGE_STAT", ctime(time()) + ": "
            + (interactive() ? query_real_name() : file_name()) + " "
            + SS_STAT_DESC[s] + " " + from + "->" + to + " by "
            + this_player()->query_real_name() + "\n");

    set_base_stat(s,to);
    stats_to_acc_exp();
    return 1;
}

/* --< umiej�tno�ci >-- */

/**
 * Resetuje mapping z bonusami do umiej�tno�ci przy inicjalizacji.
 */
private void
skill_extra_map_reset()
{
    skill_extra_map = ([ ]);
}

/**
 * Resetuje mapping z wielko�ciami umiej�tno�ci przy inicjalizacji.
 */
private void
skillmap_reset()
{
    skillmap = ([ ]);
}

/**
 * Zwraca obiekt odpowiedzialny za opis danej umiej�tno�ci.
 *
 * @param skill numer cechy
 *
 * @return Obiekt, kt�ry jest odpowiedzialny za opis danej
 * umiej�tno�ci.
 */
public object
find_skill_describer(int stat)
{
    string *obf;
    int il;
    object ob;

    obf = query_textgivers();

    for (il = 0; il < sizeof(obf); il++) {
        ob = find_object(obf[il]);
        if (!ob) {
            catch(obf[il]->teleledningsanka());
            ob = find_object(obf[il]);
        }
        if (ob && ob->desc_skill(stat))
            return ob;
    }
    return 0;
}

/**
 * Ustawia warto�� umiej�tno�ci na danym poziomie.
 *
 * @param skill numer umiej�tno�ci
 * @param val nowa warto�� umiej�tno�ci
 *
 * @return <ul>
 *           <li><b>1</b> je�eli operacja zako�czy�a si� sukcesem
 *           <li><b>0</b> w przeciwnym przypadku
 *         </ul>
 */
public int
set_skill(int skill, int val)
{
    if (!mappingp(skillmap))
        skillmap = ([]);

    if (!intp(skill))
        return 0;

    if (val < 0)
        val = 0;

    skillmap[skill] = val;
    return 1;
}

/**
 * Ustawia tymczasowy modyfikator do umiej�tno�ci.
 *
 * @param skill numer umiej�tno�ci
 *
 * @return Nowa warto�� modyfikatora do umiej�tno�ci.
 */
public void
set_skill_extra(int skill, int val)
{
    if (val == 0)
    {
        skill_extra_map = m_delete(skill_extra_map, skill);
        return;
    }

    skill_extra_map[skill] = val;
}

/**
 * Zwraca tymczasowy modyfikator do umiej�tno�ci.
 *
 * @param skill numer umiej�tno�ci
 *
 * @return Warto�� tymczasowego modyfikatora do umiej�tno�ci.
 */
public int
query_skill_extra(int skill)
{
    return skill_extra_map[skill];
}

/**
 * Usuwa podan� umiej�tno��.
 *
 * @param skill numer umiej�tno�ci
 */
void
remove_skill(int skill)
{
    if (mappingp(skillmap))
    {
#ifdef LOG_SET_SKILL
        if (interactive(this_object()) &&
                (this_interactive() != this_object()))
        {
            SECURITY->log_syslog(LOG_SET_SKILL,
                    sprintf("%s %-11s: %6d %3d ->    0 by %s\n", ctime(time()),
                        capitalize(query_real_name()), skill, skillmap[skill],
                        (objectp(this_interactive()) ?
                         capitalize(this_interactive()->query_real_name()) :
                         MASTER_OB(previous_object()))));
        }
#endif LOG_SET_SKILL

        skillmap = m_delete(skillmap, skill);
    }
}

/**
 * Zwraca podstawow� wielko�� umiej�tno�ci (bez modyfikator�w).
 *
 * @param skill numer umiej�tno�ci
 *
 * @return Prawdziwa wielko�� umiej�tno�ci.
 */
nomask int
query_base_skill(int skill)
{
    if (!mappingp(skillmap))
        return 0;

    return skillmap[skill];
}

/**
 * Zwraca wielko�� podanej umiej�tno�ci.
 *
 * @param skill numer umiej�tno�ci
 *
 * @return Wielko�� umiej�tno�ci.
 */
public int
query_skill(int skill)
{
    int toReturn;
    if (!mappingp(skillmap))
        return 0;

    toReturn = skillmap[skill] + skill_extra_map[skill];
    return (toReturn > 100) ? 100 : toReturn;
}

/**
 * Zwraca numery wszystkich posiadanych umiej�tno�ci.
 *
 * return Tablica z numerami wszystkich posiadanych umiej�tno�ci.
 */
public int *
query_all_skill_types()
{
    if (!mappingp(skillmap))
        return 0;
    return m_indexes(skillmap);
}

/**
 * Zwraca �cie�k� do obiektu, kt�ry definiuje funkcj� sk_rank, kt�ra
 * przypisuje opisy do poziom�w umiej�tno�ci.
 *
 * @return �cie�ka do obiektu opisuj�cego umiej�tno�ci.
 */
public string
query_skill_descriptor()
{
    return "/lib/skill_raise";
}

/**
 * T�umaczy podan� warto�� do�wiadczenia na wielko�� umiej�tno�ci.
 *
 * @param exp warto�� do�wiadczenia
 *
 * @return Warto�� umiej�tno�ci wyliczona na podstawie podanego do�wiadczenia.
 */
nomask int
exp_to_skill(int exp)
{
    int toReturn;
//    write("DEBUG: exp_to_skill(" + exp + ")\n");
    toReturn = F_EXP_TO_SKILL(exp);
    return (toReturn > 100) ? 100 : toReturn;
}

/**
 * T�umaczy podan� wielko�� umiej�tno�ci na warto�� do�wiadczenia.
 *
 * @param skill wielko�� umiej�tno�ci
 *
 * @return Warto�� do�wiadczenia wyliczona na podstawie podanej
 * wielko�ci umiej�tno�ci.
 */
nomask int
skill_to_exp(int stat)
{
    return F_SKILL_TO_EXP(stat);
}

/**
 * Uaktualnia wielko�� umiej�tno�ci do poziomu wyznaczanego przez
 * do�wiadczenie.
 *
 * @param skill numer umiej�tno�ci
 */
public void
update_skill(int skill)
{
    set_skill(skill, exp_to_skill(query_acc_exp(skill)));
}

/**
 * T�umaczy do�wiadczenie umiej�tno�ci na ich warto�ci.
 */
static void
acc_exp_to_skills()
{
//    write("DEBUG: acc_exp_to_skills()\n");
    if (!mappingp(skillmap))
        skillmap = ([]);

    if (!mappingp(skill_exp))
        return;

    foreach (int sk : m_indices(skill_exp)) {
        update_skill(sk);
    }
}
