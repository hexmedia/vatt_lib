/*
 * /std/living/move.c
 *
 * This is a subpart of living.c
 *
 * All movement related routines are coded here.
 */
 
#include <stdproperties.h>
#include <macros.h>
#include <filter_funs.h>
#include <std.h>
#include <options.h>
#include <sit.h>
#include <colors.h>

static object	*following_me;		/* List of objects following me */

static string
zmien_kierunek(string str)
{
    switch (str)
    {
	case "p�noc": return " na p�noc";
	case "po�udnie": return " na po�udnie";
	case "wsch�d": return " na wsch�d";
	case "zach�d": return " na zach�d";
	case "p�nocny-wsch�d": return " na p�nocny-wsch�d";
	case "p�nocny-zach�d": return " na p�nocny-zach�d";
	case "po�udniowy-wsch�d": return " na po�udniowy-wsch�d";
	case "po�udniowy-zach�d": return " na po�udniowy-zach�d";
	case "d�": return " na d�";
	case "g�ra": return " na g�r�";
	default: return " " + str;
    }
}

static string
zmien_kierunek_na_przeciwny(string str)
{
    switch (str)
    {
	case "p�noc": return " z po�udnia";
	case "po�udnie": return " z p�nocy";
	case "wsch�d": return " z zachodu";
	case "zach�d": return " ze wschodu";
	case "p�nocny-wsch�d": return " z po�udniowego-zachodu";
	case "p�nocny-zach�d": return " z po�udniowego-wschodu";
	case "po�udniowy-wsch�d": return " z p�nocnego-zachodu";
	case "po�udniowy-zach�d": return " z po�udniowego-wschodu";
	case "d�": return " z g�ry";
	case "g�ra": return " z do�u";
	default: return "";
    }
}

/*
 * Function name: move_reset
 * Description  : Reset the move module of the living object.
 */
static nomask void
move_reset()
{
    set_m_in(LD_ALIVE_MSGIN);
    set_m_out(LD_ALIVE_MSGOUT);
 
    set_mm_in(LD_ALIVE_TELEIN);
    set_mm_out(LD_ALIVE_TELEOUT);
    
    following_me = ({});
}

int
filter_team_present(object ob)
{
    if (environment(this_object()) != environment(ob)) return 0;
    if (this_object()->check_seen(ob)) {
        if (ob->query_prop(SIT_LEZACY) || ob->query_prop(SIT_SIEDZACY)) {
            object env = ENV(ob);
            if (env) {
		    object old_tp = TP;
                set_this_player(ob);
                env->wstan();
                set_this_player(old_tp);
            }
        }
        if (ob->command("test_na_obecnosc_paralizu")) {
            return 0;
        }
        return 1;
    }
    return 0;
}

static private void
do_glance_for_teams(object player)
{
    player->do_glance(player->query_option(OPT_BRIEF));
}

/*
 * Function name: move_living
 * Description:   Posts a move command for a living object somewhere. If you
 *                have a special exit like 'climb tree' you might want to
 *                use set_dircmd() and set it to 'tree' in the room to allow
 *                teammembers to follow their leader.
 * Arguments:     how:      The direction of travel, like "north".
 *                          "X" for teleportation
 *                          "M" if you write leave and arrive messages yourself.
 *                to_dest:  Destination
 *                dont_follow: A flag to indicate group shall not follow this
 *                          move if this_object() is leader
 *                no_glance: Don't look after move.
 *
 * Returns:       Result code of move:
 *                      0: Success.
 *
 *                      3: Can't take it out of it's container.
 *                      4: The object can't be inserted into bags etc.
 *                      5: The destination doesn't allow insertions of objects.
 *                      7: Other (Error message printed inside move() func)
 */
public varargs int
move_living(mixed how, mixed to_dest, int dont_follow, int no_glance)
{
  int    index, size, size_present;
  object *team, *team_present, *other_present, *drag, env, oldtp;
  string vb, com, msgout, msgin;
  mixed msg;

  oldtp = this_player();

  if (!objectp(to_dest)) {
      msg = LOAD_ERR(to_dest);

      if (stringp(msg))
      {
          if (!environment(this_object()))
          {
              tell_object(this_object(), "PANIC Move error: " + msg);
              to_dest = this_object()->query_default_start_location();
              msg = LOAD_ERR(to_dest);
              to_dest = find_object(to_dest);
          }
          else
          {
              tell_object(this_object(), msg);
              SECURITY->log_loaderr(to_dest, environment(this_object()), how,
                      previous_object(), msg);
              return 7;
          }
      }
      else {
          to_dest = find_object(to_dest);
      }
  }

  if (!to_dest->query_prop(ROOM_I_IS))
  {
    return 7;
  }

  if (!how)
  {
    return move(to_dest, 1);
  } 

  if (this_object()->query_prop(LIVE_M_NO_MOVE))
  {
    write(this_object()->query_prop(LIVE_M_NO_MOVE));
    return 7;
  }

  team = query_team();
  size = sizeof(team);
  team_present = filter(team, filter_team_present);
  size_present = sizeof(team_present);

  if (pointerp(how))
  {
    how = secure_var(how);
    if (pointerp(how[0]))
        how[0] = how[0][0];
    if (stringp(how[1]))
      how[1] = " " + how[1];
    if (sizeof(how) > 2) {
      if (stringp(how[2]))
        how[2] = " " + how[2];
    }
    else
      how += ({ "" });
  }
  if (stringp(how))
  {
    how = ({ how, zmien_kierunek(how), zmien_kierunek_na_przeciwny(how) });
  }

  if (how[0] == "M") 
  {
    msgin = 0;
    msgout = 0;
  } 
  else if (how[0] == "X") 
  {
    msgin = this_object()->query_mm_in() + "\n";
    msgout = this_object()->query_mm_out() + "\n";
  }
  else if (query_prop(LIVE_I_SNEAK)) 
  {
    if (pointerp(how[2]) && (sizeof(how[2]) == 3)) {
      msgin = how[2][2] + ".\n";
    }
    else {
      msgin = this_object()->query_m_in() +
        " skradaj^ac si�" + how[2] + ".\n";
    }
    if (pointerp(how[1]) && (sizeof(how[1]) == 4)) {
      msgout = how[1][2] + ".\n";
    }
    else {
      msgout = "przemyka si�" + how[1] + ".\n";
    }
  }
  else 
  {
    if (pointerp(how[2]) && (sizeof(how[2]) == 3)) {
      msgin = how[2][0] + ".\n";
    }
    else {
      msgin = this_object()->query_m_in() + how[2] + ".\n";
    }
    if (pointerp(how[1]) && (sizeof(how[1]) == 4)) {
      msgout = how[1][0] + ".\n";
    }
    else {
      msgout = this_object()->query_m_out() + how[1] + ".\n";
    }
  }

  if (size_present && how[0] != "X" && how[0] != "M")
  {
    if (pointerp(how[2]) && (sizeof(how[2]) == 3)) {
      msgin = how[2][1] + ".\n";
    }
    else {
      msgin = " przybywaj�" + how[2] + ".\n";
    }
    if (pointerp(how[1]) && (sizeof(how[1]) == 4)) {
      msgout = how[1][1] + ".\n";
    }
    else {
      msgout = " pod��aj�" + how[1] + ".\n";
    }
  }

  /* Make us this_player() if we aren't already. */
  if (this_object() != this_player())
  {
    set_this_player(this_object());
  }

  if (index = move(to_dest, 0, 0, 1)) 
  {
    return index;
  }

  if (env = environment(this_object()))
  {
    /* Update the last room settings. */
    add_prop(LIVE_O_LAST_LAST_ROOM, query_prop(LIVE_O_LAST_ROOM));
    add_prop(LIVE_O_LAST_ROOM, env);
    add_prop(LIVE_S_LAST_MOVE, (vb = query_verb()));

    /* Update the hunting status */
    this_object()->adjust_combat_on_move(1);

    /* Leave footprints. */
    if (!env->query_prop(ROOM_I_INSIDE) && !query_prop(LIVE_I_NO_FOOTPRINTS))
    {
	/* Sprawdzamy czy sposob wyjscia jest godny zapisania */
	if(how[1] ~= " M" || how[1] ~= " X")
		env->add_prop(ROOM_AS_DIR, ({ "...do nik�d - urywaj� si� nagle w"
					+" pewnym miejscu", query_rasa(PL_BIE) }));
	else
		env->add_prop(ROOM_AS_DIR, ({ how[1], query_rasa(PL_BIE) }));
	
    }

    /* Report the departure. */                     
    if (msgout && !query_prop(LIVE_I_SILENT_MOVE))
    {
      if (size_present && how[0] != "X" && how[0] != "M")
      {
        write(set_color(COLOR_FG_DEFAULT, COLOR_BOLD_ON) + "Wraz z " + COMPOSITE_LIVE(team_present, PL_NAR) +
            ((pointerp(how[1]) && (sizeof(how[1]) == 4)) ?
             how[1][3] : " pod��asz" + how[1]) + ".\n" + clear_color());
        other_present = FILTER_LIVE(all_inventory(environment())) - team_present - ({ this_object() });
        foreach (object x : other_present)
        {
            set_this_player(x);

            if(!CAN_SEE_IN_ROOM(x))
                continue;

            int *chs, sch;
            object *showed = team_present + ({TO});
            if((sch=fold((chs=showed->check_seen(x)), &operator(+)(,), 0)) == sizeof(showed))
                x->catch_msg(capitalize(COMPOSITE_LIVE(showed, PL_MIA)) + msgout);
            else if(sch+1 > 0)
            {
                int i;
                for(i=0;i<sizeof(showed);i++)
                    if(chs[i] == 0)
                        showed = exclude_array(showed, i, i);

                if(sizeof(showed) == 1)
                    msgout = " " + this_object()->query_m_out() + how[1] + ".\n";

                x->catch_msg(capitalize(COMPOSITE_LIVE(showed, PL_MIA)) + msgout);
            }
        }
        set_this_player(this_object());
      }
      else
        saybb(QCIMIE(this_player(), PL_MIA) + " " + msgout);
    }
  }

  if (!query_prop(LIVE_I_SNEAK))
  {
    remove_prop(OBJ_I_HIDE); 
  }
  else
  {
    remove_prop(LIVE_I_SNEAK);
  }

  if (index = move(to_dest)) 
  {
    return index;
  }

  if (msgin && !query_prop(LIVE_I_SILENT_MOVE))
  {
    if (size_present && how[0] != "X" && how[0] != "M")
    {
      other_present = FILTER_LIVE(all_inventory(environment())) - team_present - ({ this_object() });
      foreach (object x : other_present)
      {
        set_this_player(x);

        if(!CAN_SEE_IN_ROOM(x))
            continue;

        int *chs, sch;
        object *showed = team_present + ({TO});
        //Sprawdzamy czy wsio widzimy ca�� dru�yn�.
        if((sch=fold((chs=showed->check_seen(x)), &operator(+)(,), 0)) == sizeof(showed))
            x->catch_msg(set_color(x, COLOR_FG_GREEN) +
                capitalize(COMPOSITE_LIVE(showed, PL_MIA)) + msgin + clear_color());
        else if(sch > 0)
        {
            int i;
            for(i=0;i<size_present;i++)
                if(chs[i] == 0)
                    showed = exclude_array(showed, i, i);

            if(sizeof(showed) == 1)
                msgin = " " + this_object()->query_m_in() + how[2] + ".\n";

            x->catch_msg(set_color(x, COLOR_FG_GREEN) + 
                    capitalize(COMPOSITE_LIVE(showed, PL_MIA)) + msgin + clear_color());
        }
      }
      set_this_player(this_object());
      //	    saybb(capitalize(msgin));
    }
    else
      saybb(SET_COLOR(COLOR_FG_GREEN) + QCIMIE(this_player(), PL_MIA) + " " + msgin + clear_color());
  }

  /* See is people were hunting us or if we were hunting people. */
  this_object()->adjust_combat_on_move(0);

  if (!dont_follow)
  {
    /* Command for the followers or team-mates. */
    if (!strlen(vb))
    {
      if (sizeof(explode(how[0], " ")) == 1)
      {
        com = how[0];
      }
      else
      {
        com = "";
      }
    }
    else if (com = env->query_dircmd())
    {
      com = vb + " " + com;
    }
    else
    {
      com = vb;
    }

    if (size_present && how[0] != "X" && how[0] != "M")
    {
      team_present->add_prop(LIVE_I_SILENT_MOVE, 1);
      /* Move the present team_present members. */
      index = -1;
      while(++index < size_present)
      {
          set_this_player(team_present[index]);
          team_present[index]->catch_msg(set_color(COLOR_FG_DEFAULT, COLOR_BOLD_ON) + "Wraz z " +
                  COMPOSITE_LIVE(team_present - ({team_present[index]}) + ({this_object()}), PL_NAR) +
                  ((pointerp(how[1]) && (sizeof(how[1]) == 4)) ?
                   how[1][3] : " pod��asz" + how[1]) + ".\n" + clear_color());
          team_present[index]->follow_leader(com);
          set_this_player(oldtp);
      }
      foreach(object ob : team_present)
      {
          set_this_player(ob);
          ob->do_glance(ob->query_option(OPT_BRIEF));
          set_this_player(oldtp);
      }
      team_present->remove_prop(LIVE_I_SILENT_MOVE);
    }

    if (size = sizeof(following_me))
    {
      /* Move the present people following me. */
      while(--size >= 0)
      {
        if (!following_me[size])
        {
          following_me = exclude_array(following_me, size, size);
          continue;
        }
        if ((environment(following_me[size]) == env) &&
            this_object()->check_seen(following_me[size]))
        {
          following_me[size]->hook_follow_me(com);
        }
        else
        {
          following_me[size]->hook_follow_you_lost_me();
        }
      }
    }
  }

  /* Take a look at the room you've entered, before the combat adjust.
  * Only interactive players bother to look. Don't waste our precious
  * CPU-time on NPC's. Przenios�em tu, �eby by�o wida� ca�� przyby��
  * dru�yn� (krun).
  */
  if (interactive(this_object()) && !no_glance)
  {
      this_object()->do_glance(this_object()->query_option(OPT_BRIEF));
  }

  /* Only reset this_player() if we weren't this_player already. */
  if (oldtp != this_object())
  {
    set_this_player(oldtp);
  }

  return 0;
}

/*
 * Function name: follow_leader
 * Description  : If the leader of the team moved, follow him/her.
 * Arguments    : string com - the command to use to follow the leader.
 *
 * WARNING      : This function makes the person command him/herself. This
 *                means that when a wizard is in a team, the team leader can
 *                force the wizard to perform non-protected commands. Wizard
 *                commands cannot be forced as they are protected.
 */
public void
follow_leader(string com) 
{
    /* Only accept this call if we are called from our team-leader. */
    if (previous_object() != query_leader())
    {
	return;
    }

    set_this_player(this_object());

    /* We use a call_other since you are always allowed to force yourself.
     * That way, we will always be able to follow our leader.
     */
    this_object()->add_prop(PLAYER_I_IN_TEAM_GLANCE_LATER, 1);
    this_object()->command("$" + com);
}

/*
 * Function name: reveal_me
 * Description  : Reveal me unintentionally.
 * Arguments    : int tellme - true if we should tell the player.
 * Returns      : int - 1 : He was hidden, 0: He was already visible.
 */
public nomask int
reveal_me(int tellme)
{
    object *list, *list2;
    int index, size;

    if (!query_prop(OBJ_I_HIDE))
    {
	return 0;
    }

    if (tellme)
    {
	this_object()->catch_msg("Nie jeste� ju� schowan" +
	    koncowka("y", "a", "e") + ".\n");
    }

    list = FILTER_LIVE(all_inventory(environment()) - ({ this_object() }) );
    list2 = FILTER_IS_SEEN(this_object(), list);
    list -= list2;

    remove_prop(OBJ_I_HIDE);

    list = FILTER_IS_SEEN(this_object(), list);

    index = -1;
    size = sizeof(list);
    while(++index < size)
	tell_object(list[index], "Ku twojemu zdumieniu, " +
	    this_object()->query_imie(list[index], PL_MIA) + " pojawi�" +
	    koncowka("", "a", "o") + " si� nagle tu� obok ciebie!\n");

    index = -1;
    size = sizeof(list2);
    while(++index < size)
	tell_object(list2[index], this_object()->query_Imie(list2[index],
	    PL_MIA) + " wychodzi z ukrycia.\n");

    return 1;
}

public void
add_following(object ob)
{
    if (ob)
        following_me = (following_me - ({ ob })) + ({ ob });
}

public void
remove_following(object ob)
{
    following_me -= ({ ob });
}

public object *
query_following()
{
    return following_me + ({});
}

// -------------- bardziej zaawansowane funkcje do poruszania sie -----

private mixed go_to_where;
private mixed go_to_finally;
private mixed go_to_path;
private int go_to_path_index;
private int go_to_alarm;

/*
 * go_to - rusza livinga do okreslonego miejsca
 * where - object/string - miejsce docelowe
 * finally - string/function - funkcja wywolana po dotarciu do celu 
 */
varargs public void
go_to(mixed where, mixed finally, float go_to_time=10.0)
{
    mixed path;
    
    if (!environment(this_object()))
	return;
	
    path = znajdz_sciezke(environment(this_object()), where);
    
    if (!pointerp(path))
        return;

    go_to_where = where;
    go_to_finally = finally;

    if (sizeof(path) == 0)
    {
	this_object()->go_to_finally();
	return;
    }   
    
    go_to_path = path;
    go_to_path_index = 0;
    go_to_alarm = set_alarm(go_to_time, 0.0, "go_to_one_step(go_to_time)");
}

private void
go_to_one_step(float go_to_time)
{
    this_object()->command(go_to_path[go_to_path_index++]);
    
    if (go_to_path_index >= sizeof(go_to_path))
	this_object()->go_to_finally();
    else
        go_to_alarm = set_alarm(go_to_time, 0.0, "go_to_one_step(go_to_time)");
}

private void
go_to_finally()
{
    if (stringp(go_to_finally))
	call_self(go_to_finally);
    else if (functionp(go_to_finally))
	applyv(go_to_finally, 0);
}
