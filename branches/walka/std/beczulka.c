#pragma strict_types
#pragma save_binary

//inherit "/std/object";
//Malutkie przygotowania do zmian... ;) lil
inherit "/std/holdable_object";

#include <object_types.h>
#include <cmdparse.h>
#include <pl.h>
#include <macros.h>
#include <stdproperties.h>
#include <wa_types.h>

private	int	pojemnosc_naczynia,
		ilosc_plynu,
		procent_alco;
private	string	opis_plynu,
		nazwa_dop,
		dlugi_opis;

string jak_smakuje = 0, jak_pije_jak_sie_zachowuje = 0;

private	int	napij(string str);
private	int	pij(string str);
private	int	wylej(string str);
private int	przelej(string str);
private int	do_destruct(string str);


public string *
parse_command_adjectiv1_id_list()
{
    if (ilosc_plynu == 0)
	return ::parse_command_adjectiv1_id_list() + ({ "pusty" });
    if (ilosc_plynu == pojemnosc_naczynia)
	return ::parse_command_adjectiv1_id_list() + ({ "pe�ny" });
    return ::parse_command_adjectiv1_id_list();
}

public string *
parse_command_adjectiv2_id_list()
{
    if (ilosc_plynu == 0)
	return ::parse_command_adjectiv2_id_list() + ({ "pu�ci" });
    if (ilosc_plynu == pojemnosc_naczynia)
	return ::parse_command_adjectiv2_id_list() + ({ "pe�ni" });
    return ::parse_command_adjectiv2_id_list();
}

/*
 * Nazwa funkcji : create_beczulka
 * Opis          : Wywolywana przez create_object(), tworzy beczulke. Mozna
 *		   ja zamaskowac, konfigurujac beczulke przy tworzeniu.
 */
public void
create_beczulka()
{
}

/*
 * Nazwa funkcji : create_holdable_object
 * Opis          : Konstruktor obiektu. Ustawiona jako nomask, wywoluje
 *		   create_beczulka().
 */
public nomask void
create_holdable_object()
{
    pojemnosc_naczynia = 5000;
    ilosc_plynu = 0;
    procent_alco = 0;
    opis_plynu = "";
    nazwa_dop = "";
//    jak_smakuje = "Smakuje tak sobie.";
//   jak_pije_jak_sie_zachowuje = " test.";

    add_prop(OBJ_I_WEIGHT, "@@query_waga");
    add_prop(OBJ_I_VOLUME, "@@query_objetosc");
    add_prop(OBJ_I_VALUE, 100);
    add_prop(OBJ_M_NO_SELL, "@@query_mozna_sprzedac");
    
    set_long("@@long");

    set_hands(W_ANYH);

    create_beczulka();
}

/* No, pokazuje efekt, jaki uzyskujemy np. po napiciu sie oleju do lamp.
 *
 */
int
drink_effect()
{
    if (jak_smakuje) write(check_call(jak_smakuje)+"\n");
    if (jak_pije_jak_sie_zachowuje) saybb(QCIMIE(this_player(), PL_MIA)+" "+check_call(jak_pije_jak_sie_zachowuje)+"\n");
    return 1;
}



/*
 * Wywoluje sie wtedy gdy ilosc plynu w naczyniu ulega zmianie.
 */
void
zmienia_sie_ilosc_plynu()
{
//    if (ilosc_plynu == 0)
//	dodaj_przym("pusty", "pu^sci");
//    else
//	remove_adj("pusty");
//    odmien_short();
//    odmien_plural_short();
}

/*
 * Nazwa funkcji : query_waga
 * Opis          : Zwraca wage beczulki, w zaleznosci od jej pojemnosci i
 *		   ilosci plynu w srodku.
 * Funkcja zwraca: int - patrz wyzej.
 */
public int
query_waga()
{
    return ilosc_plynu + (pojemnosc_naczynia / 4);
}

/*
 * Nazwa funkcji : query_objetosc
 * Opis          : Zwraca objetosc beczulki, w zaleznosci od jej pojemnosci.
 * Funkcja zwraca: int - patrz wyzej.
 */
public int
query_objetosc()
{
    return 6 * pojemnosc_naczynia / 5;
}

/*
 * Nazwa funkcji : query_mozna_sprzedac
 * Opis          : Zwraca czy mozna sprzedac - w zaleznosci od tego, czy jest
 *		   pusta czy nie.
 * Funkcja zwraca: 	0 - mozna sprzedac,
 *		   string - nie mozna sprzedac, wyjasnienie dlaczego.
 */
public mixed
query_mozna_sprzedac()
{
    return (ilosc_plynu ? "Nie kupujemy pe^lnych " + plural_short(PL_DOP) + ".\n" 
			: 0);
}




public int
pomoc(string str)
{
    object beczuleczka;

    notify_fail("Nie ma pomocy na ten temat.\n");

    if (!str) return 0;

    if (!parse_command(str, this_player(), "%o:" + PL_MIA, beczuleczka))
        return 0;

    if (beczuleczka != this_object())
        return 0;

    write("Z "+this_object()->short(PL_DOP)+" mo�esz spr�bowa� si� napi�"+
          " lub pi�. Mo�esz tak�e "+this_object()->koncowka("go","j�","to")+
	  " rozbi�, wyla� "+this_object()->koncowka("jego","jej","tego")+
	  " zawarto��, b�d� przela� do innego pojemnika.\n");

        return 1;
}




public void
init()
{
     ::init();
     add_action(do_destruct, "rozbij");
     add_action(napij, "napij");
     add_action(pij, "pij");
     add_action(wylej, "wylej");
     add_action(przelej, "przelej");
     add_action("pomoc", "?", 2);
}

/*
 * Function name: do_destruct
 * Description  : Powoduje rozbicie butelki
 * Arguments    : string - no to co gracz wpisal :P
 */
int
do_destruct(string str)
{
    object *beczulka;
    string vb = capitalize(query_verb());
    
    if (!str)
    {
        notify_fail(vb + " co?\n");
        return 0;
    }

    parse_command(str, all_inventory(this_player()), "%i:"+PL_BIE + " %s", beczulka, str);
    beczulka = NORMAL_ACCESS(beczulka, 0, 0);
//    beczulka = filter(beczulka, &filter_olej());
    
    if (!sizeof(beczulka))
    {
        notify_fail(vb + " co?\n");
        return 0;
    }
    
    if(beczulka[0]->query_name()~="buk�ak")
    {
        write("Chyba nie rozbijesz buk�aka.\n");
        return 1;
    }
    
//    write("Rozbijasz " + FO_COMPOSITE_DEAD(butelki, this_player(), PL_BIE, 1) + ".\n");
    write("Rozbijasz " + FO_COMPOSITE_DEAD(beczulka, this_player(), PL_BIE) + " na kawa�ki.\n");
//    say(QCIMIE(this_player(), PL_MIA) + " rozbija " + QCOMPDEAD(PL_BIE, 1) + ".\n");
    say(QCIMIE(this_player(), PL_MIA) + " rozbija " + QCOMPDEAD(PL_BIE) + " na kawa�ki.\n");

    beczulka->remove_object();
    return 1;
}

/*
 * Nazwa funkcji : napij
 * Opis          : Wywolywana, gdy gracz wpisze komende 'napij'. Sprawdza
 *		   z czego gracz chce sie napic i wywoluje drink_from_it()
 *		   w odpowiedniej beczulce. Zwraca to co zwroci 
 *		   drink_from_it().
 * Argumenty     : string - argument do komendy.
 * Funkcja zwraca: W zaleznosci od rezultatu funkcji ob->drink_from_it(),
 *			1 - komenda przyjeta, lub
 *			0 - nie przyjeta, szukaj w innym obiekcie.
 */
private int
napij(string str)
{
    object *obs;
    int ret;
    
    notify_fail("Napij si^e z czego?\n");
    
    if (!strlen(str))
        return 0;
    
    if (!parse_command(str, all_inventory(this_player()), 
        "'si^e' 'z' %i:" + PL_DOP, obs))
        return 0;
    
    obs = NORMAL_ACCESS(obs, 0, 0);
    
    if (!sizeof(obs))
        return 0;
        
    if (sizeof(obs) > 1)
    {
        notify_fail("Mo^zesz si^e napi^c tylko z jednej rzeczy naraz.\n");
        return 0;
    }
    
    ret = obs[0]->drink_from_it(this_player());
    if (ret)
	this_player()->set_obiekty_zaimkow(obs);
    return ret;
}

/*
 * Nazwa funkcji : pij
 * Opis          : Wywolywana, gdy gracz wpisze komende 'pij'.
 * Argumenty     : string - argument do komendy.
 * Funkcja zwraca: 1 - komenda przyjeta, lub
 *		   0 - nie przyjeta, szukaj w innym obiekcie.
 */
private int
pij(string str)
{
    object *obs;
    int ret;
    
    notify_fail("Pij z czego?\n");
    
    if (!strlen(str))
        return 0;
    
    if (!parse_command(str, all_inventory(this_player()), 
        " 'z' %i:" + PL_DOP, obs))
        return 0;
    
    obs = NORMAL_ACCESS(obs, 0, 0);
    
    if (!sizeof(obs))
        return 0;
        
    if (sizeof(obs) > 1)
    {
        notify_fail("Mo^zesz pi^c tylko z jednej rzeczy naraz.\n");
        return 0;
    }

    ret = obs[0]->do_pij();

    if (ret)
	this_player()->set_obiekty_zaimkow(obs);

    return ret;
}

/*
 * Nazwa funkcji : wylej
 * Opis          : Wywolywana, gdy gracz wpisze komende 'wylej'. Sprawdza
 *		   z czego gracz chce wylac zawartosc i wywoluje
 *		   wylej_from_it() w odpowiedniej beczulce. Zwraca to co
 *		   zwroci wylej_from_it().
 * Argumenty     : string - argument do komendy.
 * Funkcja zwraca: W zaleznosci od rezultatu funkcji ob->wylej_from_it(),
 *			1 - komenda przyjeta, lub
 *			0 - nie przyjeta, szukaj w innym obiekcie.
 */
public int
wylej(string str)
{
    object *obs;
    int ret;
    
    //notify_fail("Wylej co? Mo^ze zawarto^s^c " + short(PL_DOP) + ".\n");
    notify_fail("Wylej co? Mo�e zawarto�� jakiego� naczynia?\n");
    
    if (!strlen(str))
        return 0;
        
    if (!parse_command(str, all_inventory(this_player()), 
        "'zawarto^s^c' %i:" + PL_DOP, obs))
        return 0;
    
    obs = NORMAL_ACCESS(obs, 0, 0);
    
    if (!sizeof(obs))
        return 0;
        
    if (sizeof(obs) > 1)
    {
        notify_fail("Mo^zesz wyla^c zawarto^s^c tylko jednej rzeczy naraz.\n");
        return 0;
    }

    ret = obs[0]->wylej_from_it();

    if (ret)
	this_player()->set_obiekty_zaimkow(obs);

    return ret;
}

/*
 * Nazwa funkcji : przelej
 * Opis          : Wywolywana, gdy gracz wpisze komende 'przelej'.
 * Argumenty     : string - argument do komendy.
 * Funkcja zwraca: 1 - komenda przyjeta, lub
 *		   0 - nie przyjeta, szukaj w innym obiekcie.
 */
private int
przelej(string str)
{
    object *obs1, *obs2;
    string plyn;
    int tmp;
    
    notify_fail("Przelej z czego do czego?\n");
    
    if (!strlen(str))
        return 0;
    
    if (!parse_command(str, all_inventory(this_player()), 
        " 'z' / 'ze' %i:" + PL_DOP + " 'do' %i:" + PL_DOP, obs1, obs2))
        return 0;    
    obs1 = NORMAL_ACCESS(obs1, 0, 0);
    obs2 = NORMAL_ACCESS(obs2, 0, 0);
    
    if (!sizeof(obs1) || !sizeof(obs2))
        return 0;
        
    if (sizeof(obs1) > 1)
    {
        notify_fail("Mo^zesz nape^lniai^c tylko z jednej rzecz naraz.\n");
        return 0;
    }

    if (sizeof(obs2) > 1)
    {
        notify_fail("Mo^zesz nape^lniai^c tylko jedn^a rzecz naraz.\n");
        return 0;
    }
    
    if (obs1[0]->query_ilosc_plynu() <= 0)
    {
	notify_fail(capitalize(obs1[0]->short(PL_MIA)) + " jest pust" +
	    obs1[0]->koncowka("y", "a", "e") + ".\n");
	return 0;
    }

    if (obs2[0]->query_ilosc_plynu() > 0)
    {
	notify_fail(capitalize(obs2[0]->short(PL_MIA)) + " nie jest pust" +
	    obs2[0]->koncowka("y", "a", "e") + ".\n");
	return 0;
    }

    tmp = MIN(obs1[0]->query_ilosc_plynu(),
	    obs2[0]->query_pojemnosc() - obs2[0]->query_ilosc_plynu());
    obs1[0]->set_ilosc_plynu(obs1[0]->query_ilosc_plynu() - tmp);
    obs2[0]->set_ilosc_plynu(obs2[0]->query_ilosc_plynu() + tmp);
    obs2[0]->set_nazwa_plynu_dop(obs1[0]->query_nazwa_plynu_dop());
    obs2[0]->set_opis_plynu(obs1[0]->query_opis_plynu());

    write("Przelewasz troch^e " + obs1[0]->query_nazwa_plynu_dop() +
	" z " + obs1[0]->short(PL_DOP) + " do " +
	obs2[0]->short(PL_DOP) + ".\n");
    say(QCIMIE(this_player(), PL_MIA) + " przelewa troch^e " +
	obs1[0]->query_nazwa_plynu_dop() + " z " + obs1[0]->short(PL_DOP) +
        " do " + obs2[0]->short(PL_DOP) + ".\n");

    this_player()->set_obiekty_zaimkow(obs1);
    this_player()->set_obiekty_zaimkow(obs2);
    return 1;
}


/*
 * Nazwa funkcji : drink_from_it
 * Opis          : Gracz pije konkretnie z tej beczulki. Pije albo nie,
 *		   wypisuje odpowiednie teksty.
 * Funkcja zwraca: 1 - komenda przyjeta.
 */
public int
drink_from_it()
{
    int lyk;
    string str;
    
    if (!ilosc_plynu)
    {
        write(capitalize(short()) + " jest zupe^lnie pust" + 
            koncowka("y", "a", "e") + ".\n");
        return 1;
    }
    
    lyk = MIN(this_player()->drink_max() / 20, this_player()->drink_max() - 
    		this_player()->query_soaked());
    if (!lyk)
    {
        write("Nie wmusisz w siebie ani kropelki wi^ecej.\n");
        return 1;
    }
    
    lyk = MIN(ilosc_plynu, lyk);
    
    if (!this_player()->drink_alco((lyk * procent_alco) / 100, 0))
    {
        write("Trunek w " + short(PL_MIE) + " jest taki mocny, ^ze chyba " +
            "nie dasz rady go wypi^c...\n");
        return 1;
    }
    
    if (!this_player()->drink_soft(lyk, 0))
    {
         write("Nie wmusisz w siebie ani kropelki wi^ecej.\n");
         return 1;
    }
    
    ilosc_plynu -= lyk;
    zmienia_sie_ilosc_plynu();

    if (!ilosc_plynu)
        str = ", opr^o^zniaj^ac " + koncowka("go", "j^a", "je") + " zupe^lnie.\n";
    else
        str = ".\n";
        
    write("Pijesz troch^e " + nazwa_dop + " z " + short(PL_DOP) + str);
    say(QCIMIE(this_player(), PL_MIA) + " poci^aga troch^e " + nazwa_dop + 
        " z " + QSHORT(this_object(), PL_DOP) + str);
    drink_effect();
        
    if (!ilosc_plynu)
    {
        procent_alco = 0;
        opis_plynu = "";
        nazwa_dop = "";

//	dodaj_przym("pusty","pu�ci");
//        odmien_short();
//        odmien_plural_short();
    }

    return 1;
}

/*
 * Nazwa funkcji : do_pij
 * Opis          : Gracz pije konkretnie z tej beczulki. Pije albo nie,
 *		   wypisuje odpowiednie teksty.
 * Funkcja zwraca: 1 - komenda przyjeta.
 */
public int
do_pij()
{
    int lyk;
    string str;
    
    if (!ilosc_plynu)
    {
        write(capitalize(short()) + " jest zupe^lnie pust" + 
            koncowka("y", "a", "e") + ".\n");
        return 1;
    }
    
    lyk = MIN(this_player()->drink_max() / 20, this_player()->drink_max() - 
    		this_player()->query_soaked());
    if (!lyk)
    {
        write("Nie wmusisz w siebie ani kropelki wi^ecej.\n");
        return 1;
    }
    
    lyk = pojemnosc_naczynia;
    
    if (!this_player()->drink_alco((lyk * procent_alco) / 100, 0))
    {
        write("Trunek w " + short(PL_MIE) + " jest taki mocny, ^ze chyba " +
            "nie dasz rady go wypi^c...\n");
        return 1;
    }
    
    if (!this_player()->drink_soft(lyk, 0))
    {
         write("Nie wmusisz w siebie ani kropelki wi^ecej.\n");
         return 1;
    }
    
    ilosc_plynu -= lyk;
    zmienia_sie_ilosc_plynu();

    if (!ilosc_plynu)
        str = ", opr^o^zniaj^ac " + koncowka("go", "j^a", "je") + " zupe^lnie.\n";
    else
        str = ".\n";
        
    write("Pijesz troch^e " + nazwa_dop + " z " + short(PL_DOP) + str);
    say(QCIMIE(this_player(), PL_MIA) + " poci^aga troch^e " + nazwa_dop + 
        " z " + QSHORT(this_object(), PL_DOP) + str);
	drink_effect();
        
    if (!ilosc_plynu)
    {
        procent_alco = 0;
        opis_plynu = "";
        nazwa_dop = "";

//	dodaj_przym("pusty","pu�ci");
//        odmien_short();
//        odmien_plural_short();
    }

    return 1;
}

/*
 * Nazwa funkcji : wylej_from_it
 * Opis          : Gracz chce wylac konkretnie z tej beczulki. Wylewa albo
 *		   nie, wypisuje odpowiednie teksty.
 * Funkcja zwraca: 1 - komenda przyjeta.
 */
public int
wylej_from_it()
{
    if (!ilosc_plynu)
    {
        write(capitalize(short()) + " jest zupe^lnie pust" + 
            koncowka("y", "a", "e") + ".\n");
        return 1;
    }
    
    write("Wylewasz ca^l^a zawarto^s^c " + short(PL_DOP) + ", w postaci " + 
        opis_plynu + " na ziemi^e.\n");
    say(QCIMIE(this_player(), PL_MIA) + " wylewa ca^l^a zawarto^s^c " +
        QSHORT(this_object(), PL_DOP) + ", w postaci " + opis_plynu + 
        " na ziemi^e.\n");
        
    ilosc_plynu = 0;
    zmienia_sie_ilosc_plynu();
    procent_alco = 0;
    opis_plynu = "";
    nazwa_dop = "";
    
//    dodaj_przym("pusty","pu�ci");
//    odmien_short();
//    odmien_plural_short();
    
    return 1;
}

public string
long()
{
    string str;
    
    str = (dlugi_opis ? dlugi_opis : "Jest to " + short() + " s^lu^z^ac" + 
	koncowka("y", "a", "e") + " do przechowywania w " + 
	koncowka("nim", "niej") + " r^o^znych trunk^ow.") + 
	" W tej chwili ";
        
    switch(100 * ilosc_plynu / pojemnosc_naczynia)
    {
        case 0: str += "jest zupe^lnie pust" + koncowka("y", "a", "e"); break;
        case 1..20: str += "zawiera ma^l^a ilo^s^c " + opis_plynu; break;
        case 21..50: str += "zawiera troch^e " + opis_plynu; break;
        case 51..70: str += "zawiera sporo " + opis_plynu; break;
        case 71..90: str += "jest prawie pe^ln" + koncowka("y", "a", "e") +
            " " + opis_plynu; break;
        default: str += "jest pe^ln" + koncowka("y", "a", "e") + " " +
            opis_plynu; break;
    }
    
    str += ".\n";
    
    return str;
}

/*
 * Nazwa funkcji : set_dlugi_opis
 * Opis          : Ustawia dlugi opis beczulki. Do opisu ustawionego
 *		   przez te funkcje dodawany jest opisik o zawartosci
 *		   pojemnika.
 * Argumenty     : string opis - opis.
 */
public void
set_dlugi_opis(string opis)
{
    dlugi_opis = opis;
}

/*
 * Nazwa funkcji : query_dlugi_opis
 * Opis          : Zwraca dlugi opis beczulki ustawiony przez wiza.
 * Funkcja zwraca: string - dlugi opis.
 */
public string
query_dlugi_opis()
{
    return dlugi_opis;
}

/*
 * Nazwa funkcji : query_beczulka
 * Opis          : Zwraca 1 jesli obiekt jest naczyniem.
 * Funkcja zwraca: int 1
 */
 public int
 query_beczulka()
 {
    return 1;
 }

/*
 * Nazwa funkcji : set_pojemnosc
 * Opis          : Sluzy do ustawiania pojemnosci beczulki w mililitrach.
 * Argumenty     : int - pojemnosc w mililitrach.
 */
public void
set_pojemnosc(int pojemnosc)
{
    pojemnosc_naczynia = pojemnosc;

    if (pojemnosc > 9900)
	set_hands(W_BOTH);
    else
	set_hands(W_ANYH);
}

/*
 * Nazwa funkcji : query_pojemnosc
 * Opis          : Zwraca pojemnosc beczulki w mililitrach.
 * Funkcja zwraca: int - pojemnosc w mililitrach.
 */
public int
query_pojemnosc()
{
    return pojemnosc_naczynia;
}

/*
 * Nazwa funkcji : set_ilosc_plynu
 * Opis          : Sluzy do ustawienia ile ml plynu znajduje sie akt w
 *		   pojemniku.
 * Argumenty     : int - liczba mililitrow plynu.
 */
public void
set_ilosc_plynu(int ilosc)
{
    if (ilosc > pojemnosc_naczynia)
        ilosc = pojemnosc_naczynia;
        
    ilosc_plynu = ilosc;
    zmienia_sie_ilosc_plynu();
}

/*
 * Nazwa funkcji : query_ilosc_plynu
 * Opis          : Zwraca ile aktualnie ml. plynu znajduje sie w pojemniku.
 * Funkcja zwraca: int - patrz wyzej.
 */
public int
query_ilosc_plynu()
{
    return ilosc_plynu;
}

/*
 * Nazwa funkcji : set_nazwa_plynu_dop
 * Opis          : Ustawia nazwe plynu w dopelniaczu. Nazwa jest
 *		   wykorzystywana w komunikatach wysylanych graczowi,
 *		   np. przy piciu.
 * Argumenty     : string - nazwa plynu w dopelniaczu.
 */
public void
set_nazwa_plynu_dop(string str)
{
    nazwa_dop = str;
}

/*
 * Nazwa funkcji : query_nazwa_plynu_dop
 * Opis          : Zwraca nazwe plynu, ktory znajduje sie akt. w beczulce
 *		   w dopelniaczu.
 * Funkcja zwraca: string - patrz wyzej.
 */
public string
query_nazwa_plynu_dop()
{
    return nazwa_dop;
}

/*
 * Nazwa funkcji : set_opis_plynu
 * Opis          : Sluzy do ustawienia dlugiego opisu plynu znajdujacego
 *		   sie akt. w beczulce. Powinien on byc w dopelniaczu.
 *		   Wykorzystywany jest np. w dlugim opisie beczulki.
 * Argumenty     : string - dlugi opis w dopelaniczu.
 */
public void
set_opis_plynu(string opis)
{
    opis_plynu = opis;
}

/*
 * Nazwa funkcji : query_opis_plynu
 * Opis          : Zwraca dlugi opis plynu znajdujacego sie akt. w beczulce,
 *		   w dopelniaczu.
 * Funkcja zwraca: string - patrz wyzej.
 */
public string
query_opis_plynu()
{
    return opis_plynu;
}

/*
 * Nazwa funkcji : set_vol
 * Opis          : Ustawia ile procent alkoholu jest w cieczy, ktora
 *		   jest w beczulce.
 * Argumenty     : int - procent alkoholu
 */
public void
set_vol(int vol)
{
    procent_alco = vol;
}

/*
 * Nazwa funkcji : query_vol
 * Opis          : Zwraca jak bardzo procentowy jak napoj w beczulce.
 * Funkcja zwraca: int - ile procent alkoholu jest w plynie w beczulce.
 */
public int
query_vol()
{
    return procent_alco;
}

public string
query_auto_load()
{
    if (MASTER == BECZULKA_OBJECT)
    {
        return 0;
    }

    return ::query_auto_load();
}
