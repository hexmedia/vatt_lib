/* Standard woreczkow, sakiewek i tym podobnych -
 * wykonany "na kolanie" przez Gjoefa w niedziele, maj 2007.
 */

#include <macros.h>
#include <stdproperties.h>
#include <subloc.h>
#include <cmdparse.h>
#include <wa_types.h>
#include <materialy.h>

inherit "/std/receptacle";

void
create_sakiewka()
{
    ustaw_nazwe("sakiewka");
}

create_receptacle()
{
    set_long("Vera mi mierzy czas!\n");
    
    add_prop(OBJ_I_WEIGHT, 200);
    add_prop(OBJ_I_VOLUME, 100);
    
    add_prop("_obj_i_attachable", 1);
    add_prop(CONT_I_MAX_VOLUME, 500);
    add_prop(CONT_I_MAX_WEIGHT, 400);
    
    ustaw_material(MATERIALY_SK_CIELE, 100);
    
    create_sakiewka();
}

public int
pomoc(string str)
{
    object ten;

    notify_fail("Nie ma pomocy na ten temat.\n");

    if (!str) return 0;

    if (!parse_command(str, this_player(), "%o:" + PL_MIA, ten))
        return 0;

    if (!ten) return 0;

    if (ten != this_object())
        return 0;

    write(capitalize(TO->query_name()) + " pozwala przechowywa^c monety " +
    "i r^o^zne drobne przedmioty. Mo^zesz przyczepi^c " + TO->koncowka("go",
    "j^a") + " do pasa, je^sli takowy posiadasz.\n");
    return 1;
}

void
init()
{
    ::init();

    add_action("pomoc", "?", 2);
}
