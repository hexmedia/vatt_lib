inherit "/std/room";

#include <cmdparse.h>
#include <filter_funs.h>
#include <macros.h>
#include <options.h>
#include <pl.h>
#include <stdproperties.h>
#include <speech.h>
#define DEBUG(x) find_player("rantaur")->catch_msg(x+"\n");


int wyjdz(string str);
void krzyknij(string str);
void dodaj_event_jazdy(string str);
void dodaj_komende(string cmd, string os3, string inf);
void set_dylizans(object obj);
string query_exitcmd();

int event_alarmid;
int event_time = 10;

object dylizans;
object *do_wyskoczenia = ({ });
string *eventy = ({ });
mixed *komendy = ({ });

void create_wnetrze()
{
}

void dodaj_komende(string cmd, string os3, string inf)
{
	komendy += ({ ({cmd})+({os3})+({inf}) });
}
nomask void create_room()
{
	set_short("Standardowe wnetrze dylizans");
	set_long("Standardowe wnetrze dylizansu.\n");
	eventy = ({ });
	komendy = ({ });
	dodaj_komende("wyskocz", "wyskakuje", "wyskoczy�");
	create_wnetrze();
}

init()
{
	::init();
	init_cmds();
	add_action(&krzyknij(), "krzyknij");
}

nomask void init_cmds()
{
	int cmdarr_size = sizeof(komendy);
	
	for(int i =0;i < cmdarr_size; i++)
	{
		mixed cmd = komendy[i][0];
		add_action("wyjdz", cmd);
	}
}

int wyjdz(string str)
{
	int cmdarr_size = sizeof(komendy);
	int cmdindex;
	
	for(int i = 0; i < cmdarr_size; i++)
	{
		mixed tmp = komendy[i][0];
		if(query_verb() ~= tmp)
		{
			cmdindex = i;
			break;
		}
	}

	if(!str)
	{
		notify_fail("Sk�d chcesz "+komendy[cmdindex][2]+"?\n");
		return 0;
	}
    
	if(!parse_command(lower_case(str), this_object(), " 'z' "+"'"+dylizans->query_nazwa(PL_DOP)+"' ")) 
	{
		notify_fail("Sk�d chcesz "+komendy[cmdindex][2]+"?\n");
		return 0;
	}

	if(this_player()->query_prop("_sit_siedzacy") != 0)
	{
		notify_fail("Mo�e najpierw wstaniesz?\n");
		return 0;
	}

	if(dylizans->query_ruszam_sie())
	{
		if(query_verb() != "wyskocz")
		{
			notify_fail("Nie mo�esz w tej chwili "+komendy[cmdindex][2]+" z "
					+dylizans->query_nazwa(PL_DOP)+".\n");
			return 0;
		}
		
		if(member_array(this_player(), do_wyskoczenia) != -1)
		{
			//TODO: wyskakiwanie z �odzi
			//TODO: Opisy w zaleznosci od obrazen
			do_wyskoczenia -= ({this_player()});
			tell_roombb(environment(dylizans), QCIMIE(this_player(), PL_MIA)+" wyskakuje z "
						+dylizans->query_nazwa(PL_DOP)+" obijaj�c si� przy tym co nieco.\n");
			this_player()->move_living("M", environment(dylizans), 1);
			tell_roombb(this_object(), QCIMIE(this_player(), PL_MIA)+" wyskakuje z "
					   +dylizans->query_nazwa(PL_DOP)+"!\n");
			this_player()->catch_msg("Z niema�� si�� uderzasz o ziemi�. Au�...\n");
			int maxhp = this_player()->query_max_hp();
			int dmg = random((3*maxhp)/20)+(maxhp/20);
			int newhp = this_player()->query_hp() - dmg;
			this_player()->set_hp(newhp);
			return 1;
		}
		else
		{
			do_wyskoczenia += ({this_player()});
			notify_fail("Napewno chcesz wyskoczy�"
			+" z p�dz�c"+koncowka("ego", "ej", "ego", "ych", "ych")
			+" "+dylizans->query_nazwa(1)+"?\n");
			return 0;
		}
	}

	tell_roombb(environment(dylizans), QCIMIE(this_player(), PL_MIA)+" "+komendy[cmdindex][1]
			+" z "+dylizans->query_nazwa(PL_DOP)+".\n");
	this_player()->move_living("M", environment(dylizans), 1);
	tell_roombb(this_object(), QCIMIE(this_player(), PL_MIA)+" "+komendy[cmdindex][1]
			+" z "+dylizans->query_nazwa(PL_DOP)+".\n");
	return 1;
}

void set_dylizans(object obj)
{
  dylizans = obj;
}

string
shout_name()
{
    object pobj = previous_object(-1);

    if (environment(pobj) == environment(this_player()) &&
        CAN_SEE_IN_ROOM(pobj) && CAN_SEE(pobj, this_player()))
        return this_player()->query_Imie(pobj, PL_MIA);
    if (pobj->query_met(this_player()))
        return this_player()->query_name(PL_MIA);
    return this_player()->query_osobno() ?
           this_player()->koncowka("Jakis mezczyzna", "Jakas kobieta") :
           this_player()->koncowka("Jakis ", "Jakas ", "Jakies ")
         + this_player()->query_rasa(PL_MIA);
}

string
shout_string(string shout_what)
{
    
    object pobj = previous_object(-1);
    int empty_string = shout_what == "0";

    if (environment(pobj) == environment(this_player()))
        return empty_string ? "krzyczy glosno." : "krzyczy: " + shout_what;

    return empty_string ? "krzyczy glosno z "+dylizans->query_nazwa(PL_DOP)+"." :
    "krzyczy z "+dylizans->query_nazwa(PL_DOP)+": " + shout_what;
}

int
krzyknij(string str)
{
    object env;
    object *lokacje;
    mixed Prop;
    if (Prop = this_player()->query_prop(LIVE_M_MOUTH_BLOCKED) != 0)
      {									
	notify_fail(stringp(Prop) ? Prop :				
		    "Nie jeste� w stanie wydoby� z siebie �adnego d�wi�ku.\n");
	return 0;
      }

    if (env = environment(this_player()))
      lokacje = ({environment(dylizans)}) + ({env}); 

    if (this_player()->query_option(OPT_ECHO))
        write("Krzyczysz" + (str ? ": " + str : " glosno.") + "\n");
    else
        write("Juz.\n");
    int ile = sizeof(lokacje);
    while(ile--)
    tell_room(lokacje[ile], "@@shout_name:" + file_name(this_object())
                + "@@ @@shout_string:" + file_name(this_object()) + "|"
		+ SPEECH_DRUNK(str, this_player()) + "@@\n", ({this_player()}));
    return 1;
}

void dodaj_event_jazdy(string str)
{
	eventy += ({str});
}

void set_event_time(int i)
{
	event_time = i;
}

void show_events()
{
	int index = random(sizeof(eventy));
	tell_roombb(this_object(), eventy[index]+"\n");
	event_alarmid = set_alarm(itof(event_time+random(5)), 0.0, &show_events());
	
}

void stop_events()
{
	remove_alarm(event_alarmid);
}

string query_exitcmd()
{
	string str = komendy[1][0]+" z "+dylizans->query_nazwa(PL_DOP);
	return str;
}
	
