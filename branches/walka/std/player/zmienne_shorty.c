/*
 * To jest kod, w ktorym sa zdefiniowane funkcje obslugujace zmienne przymiotniki
 * graczy.
 *                                                  Gdzies w lato, 2006
 *                                                                      Vera.
 *
 * Dopisano: 2006-12-07 21:40
 *  Tak patrz� i troszk� poprawiam com sknoci� i si� nadziwi� nie mog�, jak
 *  ja si� w tym wszystkim po�apa�em wtedy? Przecie� to jest najgorzej napisany
 *  kod, jaki w �yciu sp�odzi�em.
 *  Co bardziej wra�liwych prosz� o nie ogl�danie tego...
 *                                                              Vera
 * TODO : jest z�a kolejno�� dodawania przymiotnik�w.
 *        np. mamy nagi zielonooki elf. Ubieramy si� - nagi si� usuwa
 *        i wskakuje inny przym, i jest: "zielonooki w�t�y elf", a powinno
 *        by�: "w�t�y zielonooki elf" - kolejno��! Je�li zielonooki by� drugi
 *        to niech tak zostanie!
 */
#include <macros.h>
#include <flags.h>
#include <filepath.h>
#include <stdproperties.h>
#define DBG(x) find_player("vera")->catch_msg(x)

mapping lista_przymiotnikow = ([]);
void dodaj_przym_do_listy(int priorytecik,string przym_lpoj,string przym_lmn);
void usun_przym_z_listy(int priorytecik,string przym_lpoj,string przym_lmn);
int wyszukaj_przym_z_listy(string pp, string pp2);
void usuwamy_powtorki_z_listy(string pp, string pp2);
void zjebalo_sie_cos();
void force_oblicz_przymiotniki();
int is_still_drunk();
//void dodajemy_przymiotniczki();
int i_count=0; //Znacznik, czy sa powtorki w liscie.
int i_count_where=-1; //Jesli sa powtorki to jaki maja priorytet.
string *aktualne_lpoj;
string *aktualne_lmn;


/*Jest to funkcja pomocnicza, wysy�aj�ca info wizardom przebywaj�cym
 *aktualnie na mudzie - dzi�ki temu w razie awarii - czarodzieje s�
 *szybko poinformowani.
 *Uwaga: U�ywa� TYLKO w przypadkach KRYTYCZNYCH!!!
 *Argumenty: object kto - w kim, lub czym wyst�pi� b��d
 *           string co - co to jest za b��d
 *Vera
 * PS. Duplikat tej funkcji jest w /std/object, bo nie wiedzia�em gdzie
 * to wpieprzyc, �eby dzia�a�o na obiektach i na playerze... :/
 */
int notify_wizards(object kto, string co)
{
    object *us = filter(users(), &->query_wiz_level());
    int il = -1;
    int size = sizeof(us);
    object env;
    object old_player = TP;
    while(++il < size)
    {
        if (query_ip_number(us[il]) &&
            !(us[il]->query_prop(WIZARD_I_BUSY_LEVEL) & BUSY_C))
        {
            if(strlen(co))
            {
			set_this_player(us[il]);
	       tell_object(us[il],set_color(31)+"\n\nUWAGA!\nObiekt: "+
                kto->query_name(PL_MIA)+" ("+RPATH(file_name(kto))+").\nTre��: "+
                co+clear_color()+"\n");
            }
            else
            {
	        /*tell_object(us[il],"\nB��D KRYTYCZNY!\nObiekt: "+
                this_player()->query_name(PL_MIA)+" zg�asza "+
                slowo+" "+(type >= LOG_SYSBUG_ID ?
	        slowa[0][0..7] : "w lokacji: "+
	     ((env = environment(this_player())) ? RPATH(file_name(env)) : "VOID"))+
	     ".\n");*/
            }
        }
    }
	set_this_player(old_player);
    return 1;
}


/*
 * Function name:	dodaj_przym_do_listy
 * Arguments:		int priorytet, string przymiotnik w liczbie pojedynczej,
 *                      string przymiotnik w liczbie mnogiej.
 * Description:		Dodaje dany przymiotnik o danym priorytecie do listy
 *                      przymiotnikow. O ile dodawany przymiotnik jest wyzszy
 *                      od aktualnych przymiotnikow postaci, to zostanie od razu
 *                      dodany do shorta postaci. Ta funkcja takze sprawdza
 *                      czy postac ma na pewno 2 przymiotniki, oraz zmienia
 *                      je, jesli to koniecznie. Ustawiany jest tu takze alarm
 *                      na funkcje force_oblicz_przymiotniki()
 */
void
dodaj_przym_do_listy(int priorytecik,string przym_lpoj,string przym_lmn)
{
    if(!przym_lpoj || !przym_lmn || przym_lpoj=="0")
        return;
//TP->drink_alco();
    //set_alarm(0.5,0.0,&force_oblicz_przymiotniki()); //Upewniamy sie ;)


    //Na poczatku sprawdzamy, czy taki przymiotnik juz jest.
    int blabla;
    for(blabla=0;blabla<sizeof(lista_przymiotnikow[priorytecik]);blabla++)
    {
        if((lista_przymiotnikow[priorytecik][blabla][0] ~= przym_lpoj ||
           !przym_lpoj) ||
			(lista_przymiotnikow[priorytecik][blabla][0] ~= przym_lmn ||
           !przym_lmn) || przym_lpoj ~= przym_lmn)
        {
// DBG("PRZYM_DBG: ZONK! Proba dodania przymiotnika ("+przym_lpoj+
//  "), ktory juz jest na liscie! "+
//            "Nie ma chuja we wsi! Return 0.\n");
            return;
        }
    }

/* Sprawd�my ile mamy przymiotnik�w */
    int ile_ich=sizeof(TP->query_przymiotniki()[0]);
    if(ile_ich != 2)
    {
// DBG("PRZYM_DBG: ZONK!!!! przymiotniki != 2!!!\n");
        if(ile_ich > 2)
        {
// DBG("PRZYM_DBG: ZONK!!!! przymiotniki > 2. Usuwam.\n");
            int i;
            int *priorytety_akt=({});
            for(i=0;i<ile_ich;i++)
            {
                //wpierw usuwamy powtorki, chyba o to mi chodzilo? ;)
                //i pobieramy ich priorytet!
                wyszukaj_przym_z_listy(TP->query_przymiotniki()[0][i],
                                       TP->query_przymiotniki()[1][i]);
                priorytety_akt+=({i_count_where});
            }
            //dump_array(priorytety_akt);
            //To terazy sprawdzamy ktora liczba jest najmniejsza
            int x, najnizszy=11;
            for(x=0;x<sizeof(priorytety_akt);x++)
            {
                if(priorytety_akt[x]<najnizszy)
                    najnizszy=priorytety_akt[x];
            }
            //int rand=random(sizeof(lista_przymiotnikow[najnizszy]));
            //string jaki_gdzie=lista_przymiotnikow[najnizszy];
            //if(lista_przymiotnikow[najnizszy][rand][0])
            if(lista_przymiotnikow[najnizszy][0][0])
            {
//                DBG("PRZYM_DBG: Usuwam jakis przymiotnik z shorta.\n");
                TP->remove_adj(lista_przymiotnikow[najnizszy][0][0]);
            }
            
        }
        else if(ile_ich < 2)
        {
// DBG("PRZYM_DBG: przymiotniki < 2. Dodaje "+
//           przym_lpoj+", "+przym_lmn+". "+
//            "O ile nie ma na liscie innych o wyszym priorytecie.\n");
            //string *aktualne=TP->query_przymiotniki()[0];
            string *aktualne_lpoj=TP->query_przymiotniki()[0];
            string *aktualne_lmn=TP->query_przymiotniki()[1];
            string dodawany_lpoj,dodawany_lmn;
            
            if(!sizeof(aktualne_lpoj))
            {
// DBG("PRZYM_DBG: DZIWNE! ZERO PRZYMIOTNIKOW, "+
//          "Wiec dodaje z listy, jesli sa jakies. ;)\n");
                //Dodajemy zatem z listy
                int i;
                //int rrand;
                for(i=10;priorytecik<=i;i--)
                {
                    if(sizeof(lista_przymiotnikow[i])>0)
                    {
                        //rrand=random(sizeof(lista_przymiotnikow[i]));
                        dodawany_lpoj=lista_przymiotnikow[i][0][0];
                        dodawany_lmn=lista_przymiotnikow[i][0][1];
                        //if(ile_ich<2)
// DBG("PRZYM_DBG: DODAJE "+dodawany_lpoj+"\n");
                         //TP->dodaj_przym(dodawany_lpoj,dodawany_lmn);    
                    }
                    
                }
            }
            else
            {
                //Ale nie dodajemy takiego, jaki juz jest.
                if(aktualne_lpoj[0]!=przym_lpoj || aktualne_lpoj[1]!=przym_lpoj)
                {
// DBG("PRZYM_DBG: (else:NIE MA PRZYMIOTNIKOW W SHORCIE!) "+
//      "DODAJE "+przym_lpoj+
//          ". Czyzbys recznie usunal sobie przymiotnik? :P\n");
                     TP->dodaj_przym(przym_lpoj,przym_lmn,1);
                }
           // else
//                DBG("PRZYM_DBG: Kurew, nie dodam, bo taki "+
//                               "juz jest. :/\n");
            }
        }
    }
    else //Jeli jest tak, jak byc powinno, czyli mamy 2 przymiotniki...
    {
// DBG("PRZYM_DBG: Dobra ziom. Mamy 2 przymiotniki...\n");
        
        //Sprawdzmy, czy nasze aktualne przymiotniki maja wyzsze priorytety
        string *aktualne_lpoj=TP->query_przymiotniki()[0];
        string *aktualne_lmn=TP->query_przymiotniki()[1];
        int pierwszy=-2,drugi=-2;
        wyszukaj_przym_z_listy(aktualne_lpoj[0],aktualne_lmn[0]);
        string str;
        switch(i_count_where)
        {
          case 0: str="0";pierwszy=0;break;
          case 1: str="1";pierwszy=1;break;
          case 2: str="2";pierwszy=2;break;
          case 3: str="3";pierwszy=3;break;
          case 4: str="4";pierwszy=4;break;
          case 5: str="5";pierwszy=5;break;
          case 6: str="6";pierwszy=6;break;
          case 7: str="7";pierwszy=7;break;
          case 8: str="8";pierwszy=8;break;
          case 9: str="9";pierwszy=9;break;
          case 10: str="10";pierwszy=10;break;
          default: str="NIE mamy pierwszego przymiotnika!!";pierwszy=-1;break;
        }
// DBG("PRZYM_DBG: Mamy pierwszy przymiotnik o priorytecie: "+str+"\n");

        wyszukaj_przym_z_listy(aktualne_lpoj[1],aktualne_lmn[1]);
        switch(i_count_where)
        {
           case 0: str="0";drugi=0;break;
           case 1: str="1";drugi=1;break;
           case 2: str="2";drugi=2;break;
           case 3: str="3";drugi=3;break;
           case 4: str="4";drugi=4;break;
           case 5: str="5";drugi=5;break;
           case 6: str="6";drugi=6;break;
           case 7: str="7";drugi=7;break;
           case 8: str="8";drugi=8;break;
           case 9: str="9";drugi=9;break;
           case 10: str="10";drugi=10;break;
           default: str="NIE mamy drugiego chama!!";drugi=-1;break;
        }
// DBG("PRZYM_DBG: Mamy drugi przymiotnik o priorytecie: "+str+"\n");
// DBG("PRZYM_DBG: POROWNUJEMY\n");
        if(pierwszy<=drugi)
        {
// DBG("PRZYM_DBG: Drugi ma wiekszy priorytet (lub jest rowny), "+
//       "wiec zmieniamy pierwszy.\n");
            TP->remove_adj(aktualne_lpoj[0]);
            TP->dodaj_przym(przym_lpoj,przym_lmn,1);
        }
        else if(pierwszy>drugi)
        {
// DBG("PRZYM_DBG: Pierwszy ma wiekszy priorytet, wiec zmieniamy drugi.\n");
            TP->remove_adj(aktualne_lpoj[1]);
            TP->dodaj_przym(przym_lpoj,przym_lmn);
        }
        
    }

    if(lista_przymiotnikow)
    {
        if(lista_przymiotnikow[priorytecik])//Dodajemy do juz istniejacego pola
        {
            lista_przymiotnikow[priorytecik]+=({({przym_lpoj,przym_lmn})});
            wyszukaj_przym_z_listy(przym_lpoj,przym_lmn); //Usuwamy powtorki;)
        }
        else
            lista_przymiotnikow+=([priorytecik:({({przym_lpoj,przym_lmn})})]);
    }
    else
        lista_przymiotnikow =([priorytecik:({({przym_lpoj,przym_lmn})})]);


    force_oblicz_przymiotniki();
}


/*
 * Function name:	usun_przym_z_listy
 * Arguments:		int priorytet, string przymiotnik w liczbie pojedynczej,
 *                      string przymiotnik w liczbie mnogiej.
 * Description:		Usuwa dany przymiotnik z listy, a takze sprawdza, czy
 *                      usuwany przymiotnik jest w shorcie postaci. Jesli tak,
 *                      to automatycznie wyszukuje jakis inny przymiotnik o
 *                      najwyzszym priorytecie z listy i go dodaje do shorta.
 */
void
usun_przym_z_listy(int priorytecik,string przym_lpoj,string przym_lmn)
{
    //Wpierw sprawdzamy, czy usuwany przymiotnik nie jest przypadkiem w shorcie
    int i;
    for(i=0;i<sizeof(TP->query_przymiotniki()[0]);i++)
    {
        if(TP->query_przymiotniki()[0][i] ~= przym_lpoj)
        {
            //DBG("PRZYM_DBG: USUWANY PRZYM "+przym_lpoj+" JEST W SHORCIE! USUWAM "+
                  //"I SZUKAM ZAMIENNIKA.\n");

            TP->remove_adj(przym_lpoj);
            //Dodajemy zatem z listy (skopiowany kod z dodaj_przym_do_listy)
                int i;
                //int rrand;
                string dodawany_lpoj,dodawany_lmn;
                for(i=10;-1<i;i--)
                {
                    if(sizeof(lista_przymiotnikow[i])>0)
                    {
                        //rrand=random(sizeof(lista_przymiotnikow[i]));
                        dodawany_lpoj=lista_przymiotnikow[i][0][0];
                        dodawany_lmn=lista_przymiotnikow[i][0][1];
                        if(sizeof(TP->query_przymiotniki()[0])<2 && dodawany_lpoj!=przym_lpoj)
                        {
  //                        DBG("PRZYM_DBG: JEST ZAMIENNIK. DODAJE "+
    //                             dodawany_lpoj+"\n");
                          TP->dodaj_przym(dodawany_lpoj,dodawany_lmn,1);
                          
                        }
                    }
                }
        }
    }
    
    if (lista_przymiotnikow)
        lista_przymiotnikow[priorytecik] -= filter(lista_przymiotnikow[priorytecik], 
            &equal_array(, ({przym_lpoj,przym_lmn}) ));
        /*lista_przymiotnikow[priorytecik] = filter(lista_przymiotnikow[priorytecik], 
            &not() @ &->equal_array(,({ ({przym_lpoj,przym_lmn}) })));*/

    force_oblicz_przymiotniki();
}


/*
 * Function name:	query_lista_przymiotnikow
 * Description:		Pokazuje list� przymiotnik�w w bardziej czytelny
 *                      spos�b ni� Dump lista_przymiotnikow. :)
 * Returns:             Zawsze 1.
 */
int
query_lista_przymiotnikow()
{
     /*write("*---------------DUMP-----------------*\n");
     dump_array(lista_przymiotnikow);
     write("*------------------------------------*\n");*/

    int licznik=0;

    for(int i=10;i>=0;i--)
    {
        if(sizeof(lista_przymiotnikow[i]))
        {
            for(int l=0;l<sizeof(lista_przymiotnikow[i]);l++)
            {
                licznik++;
                write(licznik+". Priorytet: "+i+"   "+
                      "Przym.: \""+
                      lista_przymiotnikow[i][l][0]+"\", \""+
                      lista_przymiotnikow[i][l][1]+"\"\n");
                      //opcjonalnie 'l+1' wskazuje na liczb� przymiotnik�w,
                      //kt�re maj� ten sam priorytet.

            }
        }
    }

    return 1;
}

/*
 *Pokazuje w formacie potrzebnym, to systemu kradzie�y i innej
 *identyfikacji
 */
mixed
query_lista_przymiotnikow2(int priorytet= -1)
{
	if(priorytet == -1)
		return lista_przymiotnikow;
	else
		return lista_przymiotnikow[priorytet];
}



/*Zwraca priorytet wyszukiwanego przymiotnika
 *je�li przymiotnika nie ma na li�cie, zwraca -1
 */
int
wyszukaj_przym_z_listy(string pp, string pp2)
{
    int i,i2;
    string pprzym;
    for(i=0;i<11;i++)
    {
        for(i2=0;i2<sizeof(lista_przymiotnikow[i]);i2++)
        {
            pprzym=lista_przymiotnikow[i][i2][0];
            if(pp~=pprzym)
            {
                //DBG("PRZYM_DBG: Wyszukuje ("+pprzym+") w liscie. JEST!\n");
                i_count++;
                i_count_where=i;//od teraz i_count_where wskazuje prior. gdzie
            }                   //jest wyszukiwany przymiotnik.
            else
                i_count_where=-1;
        }
    }
    usuwamy_powtorki_z_listy(pp,pp2); //Just in case ;)

    return i_count_where;
}

void
usuwamy_powtorki_z_listy(string pp, string pp2)
{
    int pr=i_count_where;
    if(i_count>1)
    {
        /*DBG("PRZYM_DBG: I to w dodatku "+
                               "kilkakrotnie! Usuwam powtorki.\n");*/
        lista_przymiotnikow[pr] -= filter(lista_przymiotnikow[pr], 
            &equal_array(, ({pp,pp2}) ));
        //dodaj_przym_do_listy(pr,pp,pp2); TO BY ZLYYY POMYSL ;)
        lista_przymiotnikow+=([pr:({({pp,pp2})})]);
        
    }
    /*else
        DBG("PRZYM_DBG: Nie zanotowano powtorek.\n");*/

    i_count=0;
}

/*
 * Jest to pomocnicza funkcja wywo�ywana wtedy,
 * jak co� si� popsuje i gracz nie ma aktualnie pe�nego
 * zestawu przymiotnik�w
 */
void
zjebalo_sie_cos()
{
    if(sizeof(TP->query_przymiotniki()[0]) == 2)
        TP->remove_adj(TP->query_przymiotniki()[0][1]);

    if(sizeof(TP->query_przymiotniki()[0]) == 1)
        TP->remove_adj(TP->query_przymiotniki()[0][0]);

    force_oblicz_przymiotniki();
    return;
}


void
force_oblicz_przymiotniki()
{
// DBG("force_oblicz_przym\n");
    if(TP->query_race() == "byt astralny" || TP->query_race()=="0")
        return;


    //Zacznijmy od sytuacji krytycznych...Kto� ma wi�cej ni� 3przym
    if(sizeof(TP->query_przymiotniki()[0])>3)
    {   /*Najprostszy spos�b XD */
        TP->remove_adj(TP->query_przymiotniki()[0]);
        force_oblicz_przymiotniki();
        return;
    }

    else if(sizeof(TP->query_przymiotniki()[0])==3)
    {
//         DBG("PRZYM_DBG: Odpalam FORCE. Bo za duzo przymiotnikow masz! (3)\n");
        
        int prior1,prior2,prior3;
        string przym1=TP->query_przymiotniki()[0][0];
        string przym2=TP->query_przymiotniki()[0][1];
        string przym3=TP->query_przymiotniki()[0][2];
        //szukam gdzie jest pierwszy przymiotnik
        int i,x;
        for(i=0;i<=10;i++)
        {
            for(x=0;x<sizeof(lista_przymiotnikow[i]);x++)
            {
                if(lista_przymiotnikow[i][x] ~= przym1)
                {    prior1=i;
//                      DBG("PRZYM_DBG: Mam priorytet pierwszego.\n");
                }
            }
        }
        //szukam gdzie jest drugi przymiotnik
        for(i=0;i<=10;i++)
        {
            for(x=0;x<sizeof(lista_przymiotnikow[i]);x++)
            {
                if(lista_przymiotnikow[i][x] ~= przym2)
                {    prior2=i;
//                      DBG("PRZYM_DBG: Mam priorytet drugiego.\n");
                }
            }
        }
        //szukam gdzie jest trzeci przymiotnik
        for(i=0;i<=10;i++)
        {
            for(x=0;x<sizeof(lista_przymiotnikow[i]);x++)
            {
                if(lista_przymiotnikow[i][x] ~= przym3)
                {    prior3=i;
//                      DBG("PRZYM_DBG: Mam priorytet trzeciego.\n");
                }
            }
        }
        //Sprawdzam ktory ma najmniejszy priorytet
        if(przym3>=przym2 && przym3>=przym1)
        {
//             DBG("PRZYM_DBG: OK! Przymiotnik "+przym3+
//                     " (nr.3) ma najmniejszy priorytet. Usuwam go!\n");
            TP->remove_adj(przym3);
        }
        else if(przym2>=przym3 && przym2>=przym1)
        {
//             DBG("PRZYM_DBG: OK! Przymiotnik "+
//                   przym2+" (nr.2) ma najmniejszy priorytet. Usuwam go!\n");
            TP->remove_adj(przym2);
        }
        else if(przym1>=przym2 && przym1>=przym3)
        {
//             DBG("PRZYM_DBG: OK! Przymiotnik "+przym1+
//                     " (nr.1) ma najmniejszy priorytet. Usuwam go!\n");
            TP->remove_adj(przym1);
        }
        
        if(sizeof(TP->query_przymiotniki()[0])>2)
        {
//             DBG("PRZYM_DBG: NOSZ KURWA JEGO MAC. Przymiotniki "+
//                     "nadal > 2. :/ Odpalam FORCE jesio raz.\n");
            force_oblicz_przymiotniki();
        }
    }
    else if(sizeof(TP->query_przymiotniki()[0])<2)
    {
//         DBG("PRZYM_DBG: Odpalam FORCE. Bo za malo przymiotnikow masz!\n");
        
        if(sizeof(TP->query_przymiotniki()[0])==1)
        {
//             DBG("PRZYM_DBG: FORCE: Musze dodac tylko jeden przymiotnik.\n");
            string aktualny_lpoj=TP->query_przymiotniki()[0][0];
            string aktualny_lmn=TP->query_przymiotniki()[1][0];
            string dodawany_lpoj, dodawany_lmn;
            int i,x;
            for(i=0;i<=10;i++)
            {
                if(sizeof(lista_przymiotnikow[i]))
                {
                    for(x=0;x<sizeof(lista_przymiotnikow[i]);x++)
                    {
                        if(lista_przymiotnikow[i][x][0] != aktualny_lpoj)
                        {
                            dodawany_lpoj=lista_przymiotnikow[i][x][0];
                            dodawany_lmn =lista_przymiotnikow[i][x][1];
                        }
                    }
                }
            }
            if(dodawany_lpoj)
            {
//                 DBG("PRZYM_DBG: FORCE: OK! MAM: "+dodawany_lpoj+"! Dodaje go!\n");
                TP->dodaj_przym(dodawany_lpoj,dodawany_lmn);
            }
            else
            {
//                 DBG("PRZYM_DBG: FORCE: Kurwa! Nie moge znalezc "+
//                         "drugiego przymiotnika w liscie. :/ \n");
                  write("\nWyst�pi� krytyczny b��d o numerze: 82xf63. "+
                        "Zg�o� to natychmiast, podaj�c ten numer oraz "+
                        "okoliczno�ci "+
                        "w jakich do tego dosz�o. Opisuj�c to zdarzenie, "+
                        "zwr�� szczeg�ln� "+
                        "uwag� na przymiotniki twojej postaci oraz ich "+
                        "ewentualne zmiany.\n\n");
                  notify_wizards(TP,"Za ma�o ma przymiotnik�w "+
                                 "na li�cie_przymiotnik�w, b��d nr. 82xf63");
            }
            
        }
        else
        {
//             DBG("PRZYM_DBG: FORCE: Lipa. Nie masz zadnych "+
//                     "przymiotnikow! Musze dodac dwa.\n");



        /*Pomoc, je�li co� si� spierdoli i gracz nie ma przymiotnik�w
        //w l.mn. czy co�...
        if(!TP->query_przymiotniki()[0][0] || TP->query_przymiotniki()[0][1]
           || TP->query_przymiotniki()[1][0] || TP->query_przymiotniki()[1][1])
        {
            //w takim razie odpalamy pomocnicz� funkcj�...
            zjebalo_sie_cos();
            return;
        }*/


            string dodawany1_lpoj,dodawany2_lpoj,dodawany1_lmn,dodawany2_lmn;
            int i/*,rrand*/;
            for(i=0;10>=i;i++)
            {
                if(sizeof(lista_przymiotnikow[i]))
                {
                    //rrand=random(sizeof(lista_przymiotnikow[i]));
                    dodawany1_lpoj=lista_przymiotnikow[i][0][0];
                    dodawany1_lmn=lista_przymiotnikow[i][0][1];
                }
            }
            //Szukamy drugiego przymiotnika...
            for(i=0;10>=i;i++)
            {
                if(sizeof(lista_przymiotnikow[i]))
                {
                    //rrand=random(sizeof(lista_przymiotnikow[i]));
                    if(lista_przymiotnikow[i][0][0] != dodawany1_lpoj)
                    {
                        dodawany2_lpoj=lista_przymiotnikow[i][0][0];
                        dodawany2_lmn=lista_przymiotnikow[i][0][1];
                    }
                }
            }

            if(dodawany1_lpoj && dodawany2_lpoj &&
            	dodawany1_lpoj != dodawany1_lmn &&
				dodawany2_lpoj != dodawany2_lmn)
            {
// DBG("OK< ZNALEZIONO: "+dodawany1_lpoj+"(lmn: "+dodawany1_lmn+") i "+
// 	dodawany2_lpoj+"(lmn: "+dodawany2_lmn+")\n");
// DBG("PRZYM_DBG: FORCE: OK! Znalazlem przymiotniki. "+
//     "Pierwszy to: "+dodawany1_lpoj+"."+
//     " A drugi to: "+dodawany2_lpoj+". Dodaje.\n");
                TP->dodaj_przym(dodawany2_lpoj,dodawany2_lmn);
                TP->dodaj_przym(dodawany1_lpoj,dodawany1_lmn);
            }
            else if(TP->query_race_name() != "byt astralny" ||
					!intp(TP->query_race_name()))
            {
// DBG("PRZYM_DBG: FORCE: Nie znalazlem przymiotnikow w liscie! "+
//     "Dupa blada! Kurew! Upewnij sie, ze masz w liscie conajmniej dwa!\n");
                  write("\nWyst�pi� krytyczny b��d o numerze: 41zx62. "+
                        "Zg�o� to natychmiast, podaj�c ten numer oraz okoliczno�ci "+
                        "w jakich do tego dosz�o. Opisuj�c to zdarzenie, zwr�� szczeg�ln� "+
                        "uwag� na przymiotniki twojej postaci oraz ich ewentualne zmiany.\n\n");
                  notify_wizards(TP,"Za ma�o ma przymiotnik�w "+
                                 "na li�cie_przymiotnik�w, b��d nr. 41zx62");
            }
        }
    }
    else if(sizeof(TP->query_przymiotniki()[0])==2)
    {
// DBG("PRZYM_DBG: FORCE: Ok. Przymiotniki=2, ale sprawdze "+
//     "czy sa to wlasciwe przymiotniki...\n");


        string aktualny1_lpoj=TP->query_przymiotniki()[0][0];
        string aktualny1_lmn=TP->query_przymiotniki()[1][0];
        string aktualny2_lpoj=TP->query_przymiotniki()[0][1];
        string aktualny2_lmn=TP->query_przymiotniki()[1][1];
        int i,x,prior1,prior2;
        int p1,p2; // Priorytety dodawanych(o ile takie sa)
        string sprawdzany1_lpoj, sprawdzany1_lmn, sprawdzany2_lpoj,
               sprawdzany2_lmn;       
        prior1=wyszukaj_przym_z_listy(aktualny1_lpoj,aktualny1_lmn);
        //prior1=i_count_where;
        prior2=wyszukaj_przym_z_listy(aktualny2_lpoj,aktualny2_lmn);
        //prior2=i_count_where;

        for(i=0;i<=10;i++)//Teraz sprawdzmy jakie sa na liscie o najwyzszych p.
        {
            //Tu juz nie ma randoma (rrand). Pierwszy lepszy...
            if(sizeof(lista_przymiotnikow[i]))
            {
                sprawdzany1_lpoj=lista_przymiotnikow[i][0][0];
                p1=i;
            }
            if(sizeof(lista_przymiotnikow[i]))
            {
                sprawdzany1_lmn=lista_przymiotnikow[i][0][1];
            }
        }
        for(i=0;i<=10;i++)//Szukamy drugiego przym.
        {
            //Tu juz nie ma randoma (rrand). Pierwszy lepszy...
            if(sizeof(lista_przymiotnikow[i]))
            {
               if(lista_przymiotnikow[i][0][0] != sprawdzany1_lpoj)
               {
                   sprawdzany2_lpoj=lista_przymiotnikow[i][0][0];
                   sprawdzany2_lmn=lista_przymiotnikow[i][0][1];
                   p2=i;
               }
               else if(sizeof(lista_przymiotnikow[i])>1)
               {
                   if(lista_przymiotnikow[i][1][0] != sprawdzany1_lpoj)
                   {
                       sprawdzany2_lpoj=lista_przymiotnikow[i][1][0];
                       sprawdzany2_lmn=lista_przymiotnikow[i][1][1];
                       p2=i;
                   }
               }
            }
        }
        
// DBG("PRZYM_DBG: FORCE: Znalazlem przymiotniki o najwyzszych "+
//               "priorytetach. Sa to: "+sprawdzany1_lpoj+" i "+sprawdzany2_lpoj+
//              ". Sprawdzam czy mamy takie w shorcie...\n");
        if(aktualny1_lpoj~=sprawdzany1_lpoj && aktualny2_lpoj~=sprawdzany2_lpoj ||
           aktualny2_lpoj~=sprawdzany1_lpoj && aktualny1_lpoj~=sprawdzany2_lpoj)
        {
// DBG("PRZYM_DBG: FORCE: Tak, mamy je w shorcie. Sprawdze jeszcze "+
//           "czy pierwszy przymiotnik ma prawo byc pierwszym. (czy ma wiekszy prior"+
//           " od drugiego)\n");
            if(p1>=p2 && aktualny1_lpoj ~= sprawdzany1_lpoj)
            {
// DBG("PRZYM_DBG: FORCE: Spoko, pierwszy przym ma wiekszy "+
//                     "(lub rowny) priorytet.\n");
            }
            else
            {
// DBG("PRZYM_DBG: FORCE: Nie! Drugi przym ma wiekszy priorytet! "+
//            "Zatem zmieniam ich kolejnosc.\n");
                //TP->remove_adj(aktualny1_lpoj);
                TP->remove_adj(aktualny2_lpoj);
                //TP->dodaj_przym(aktualny1_lpoj,aktualny1_lmn);    
                TP->dodaj_przym(aktualny2_lpoj,aktualny2_lmn,1);
            }
        }
        else
        {
// DBG("PRZYM_DBG: FORCE: Nie! W shorcie mamy inne! Ustawiam takie.\n");
            TP->remove_adj(aktualny1_lpoj);
            TP->remove_adj(aktualny2_lpoj);
            
            if(p1>=p2)
            {
// DBG("PRZYM_DBG: FORCE: Priorytet od "+sprawdzany1_lpoj+" jest "+
       //"wiekszy niz priorytet od "+sprawdzany2_lpoj+", wiec on ma bedzie jako "+
//         "pierwszy.\n");
                TP->dodaj_przym(sprawdzany2_lpoj,sprawdzany2_lmn);    
                TP->dodaj_przym(sprawdzany1_lpoj,sprawdzany1_lmn,1);
            }
            else if(p1<p2)
            {
 /*DBG("PRZYM_DBG: FORCE: Priorytet od "+sprawdzany2_lpoj+" jest "+
      "wiekszy niz priorytet od "+sprawdzany1_lpoj+", wiec on ma bedzie jako "+
        "pierwszy.\n");
*/
                TP->dodaj_przym(sprawdzany2_lpoj,sprawdzany2_lmn,1);
                TP->dodaj_przym(sprawdzany1_lpoj,sprawdzany1_lmn);
            }
            else
            {
                TP->dodaj_przym(sprawdzany2_lpoj,sprawdzany2_lmn);
                TP->dodaj_przym(sprawdzany1_lpoj,sprawdzany1_lmn);
            }
        }

    }

	//TP->odmien_short();

//DBG("PRZYM_DBG: FORCE: KONIEC.\n");
}




/*
 * Function name: check_if_idle
 * Description:   Ta funkcja po prostu sprawdza, czy idlujemy czy nie. :)
 *                W kazdym graczu jest ustawiony alarm by co jakis czas
 *                idle byl sprawdzany (wiec jesli ktos zadecyduje, ze
 *                alarmow jest za duzo, to mozna usunac ta funkcje i tym samym
 *                zrezygnowac z dodawania przymiotnika "zamyslony" dla idlerow.
 * Argument:          object gracz - na kim wykonujemy te operacje ;)  
 * 		int arg - je�li arg==1 to usuwamy zamy�lonego i tak.
 *                (potrzebne przy zaka�czaniu)

 */
int
check_if_idle(object gracz, int arg=0)
{
    if(!interactive(gracz))
	    	return 0;

    if(arg)
    {
        gracz->usun_przym_z_listy(10,"zamy�lony","zamy�leni");
        return 1;
    }

    if(query_idle(gracz)>600)
    {
		if(gracz->wyszukaj_przym_z_listy("zamy�lony") != 10)
        	gracz->dodaj_przym_do_listy(10,"zamy�lony","zamy�leni");
        set_alarm(5.0,0.0,"check_if_idle",gracz);
    }
    else
    {
       gracz->usun_przym_z_listy(10,"zamy�lony","zamy�leni");
        //set_alarm(20.0,0.0,&check_if_idle());
    }

    return 1;
}

/*
 * Function name:       is_still_drunk
 * Description:         Sprawdza, czy gracz nadal jest wciety. Funkcja usuwa
 *                      graczowi przymiotnik "pijany" z listy, jesli wytrzezwial,
 *                      lub ustawia alarm na sama siebie, jesli nadal jest pijany.
 */
int
is_still_drunk()
{
//DBG("ist_still_drunk\n");

   if(TP->intoxicated_max()-(TP->intoxicated_max()/4)<TP->query_intoxicated()+10)
{
		if(TP->wyszukaj_przym_z_listy("pijany") == -1)
        	TP->dodaj_przym_do_listy(8,"pijany","pijani");

       set_alarm(100.0,0.0,"is_still_drunk");
//DBG("ist_still_drunk  TAK\n");
}
   else
   {
       TP->usun_przym_z_listy(8,"pijany","pijani");
//DBG("ist_still_drunk  NIE. USUWAMY!\n");
	}
   return 1;
}

/*
 * Function name:	przym_alco()
 * Description:		Dodaje przymiotnik "pijany", je�li trzeba, i ustawia
 *                      alarm na sprawdzanie czy nadal jest pijany -
 *                      is_still_Drunk()
 */
void
przym_alco()
{
    if(TP->intoxicated_max()-(TP->intoxicated_max()/4)< TP->query_intoxicated())
    {
		if(TP->wyszukaj_przym_z_listy("pijany") == -1)
        	TP->dodaj_przym_do_listy(8,"pijany","pijani");
        set_alarm(100.0,0.0,&is_still_drunk());
    }
}


