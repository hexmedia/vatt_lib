/*
 * player/death_sec.c
 *
 * This is a subpart of player_sec.c
 *
 * Handling of what happens when the player dies and resurrects.
 * Scars all also handled here.
 *
 * This file is included into player_sec.c
 */

#include <formulas.h>
#include <log.h>
#include <ss_types.h>
#include <std.h>

#define WSTEP_SMIERCI "/d/Standard/smierc/wstep_smierci.txt"

/*
 * Prototypes
 */
void modify_on_death();
int query_average();


/*
 *Trzeba usun�� wpis o graczu, je�li zezgoni� z r�ki
 *stra�nik�w.
 */
void
usun_wroga_straz(object komu,object killer)
{
	object admin = killer->query_admin();
	admin->usun_wroga_i_kontynuuj_patrol(killer->query_id_patrolu(), komu);

	//jeszcze dajmy info do logow STRAZy, zeby bylo przejrzysciej je przegladac:
	log_file(LOG_STRAZ,"ZGON: "+komu->query_real_name()+
			", killer: "+file_name(killer)+
			" w: "+file_name(environment(killer)) + "\n");
}



/*
 * Function name:   second_life
 * Description:     Handles all that should happen to a dying player.
 * Argument:        Object that caused the kill.
 * Returns:         True if the living object should get a second life
 */
public int
second_life(object killer)
{
    string log_msg, wstep;

    if (query_wiz_level())
    	return 0;

    log_msg = ctime(time()) + " " +
        capitalize(this_object()->query_real_name()) +
        " (" + this_object()->query_average_stat() + ") by ";

    if (objectp(killer))
    {
	    if (interactive(killer))
        {
	       log_msg += capitalize(killer->query_real_name()) +
		      " (" + killer->query_average_stat() + ")";
            if (environment() && file_name(environment()) ==
                    query_default_start_location())
                log_msg += " [startloc]";
        }
	    else
	    {
	       log_msg += MASTER_OB(killer);
	       
	       //je�li zabil go stra�nik...
	    	if(killer->query_straznik() || killer->query_oficer())
				usun_wroga_straz(this_object(),killer);
	    }
    }
    else
	   log_msg += "poison";

    log_msg += "\n";

#ifdef LOG_PLAYERKILLS
    if (objectp(killer) && interactive(killer))
    {
        log_file(LOG_PLAYERKILLS, log_msg, -1);
    }
    else
#endif LOG_PLAYERKILLS
    {
#ifdef LOG_KILLS
        log_file(LOG_KILLS, log_msg, -1);
#endif LOG_KILLS
    }

    if(!this_object()->query_prop("_sit_lezacy"))
        this_object()->catch_msg("Osuwasz si� na ziemi�.\n");
    this_object()->catch_msg("Zamykasz oczy.\n");


    modify_on_death();
    inc_death_count();

    set_m_in(F_GHOST_MSGIN);
    set_m_out(F_GHOST_MSGOUT);

    set_headache(0);
    set_intoxicated(0);
    set_stuffed(0);
    set_soaked(0);

    stop_fight(query_enemy(-1)); /* Forget all enemies */

    this_object()->death_sequence();
    save_me(); /* Save the death badge if player goes linkdead. */

    return 1;
}

/*
 * Function name:   modify_on_death
 * Description:     Modifies some values (e.g. exp, stats and hp) when a
 *                  player has died.
 */
static void 
modify_on_death()
{
    set_hp(F_DIE_START_HP(query_max_hp()));
    increase_ss(SS_STR, ftoi((-0.2) * itof(query_acc_exp(SS_STR))));
    increase_ss(SS_DEX, ftoi((-0.2) * itof(query_acc_exp(SS_DEX))));
    increase_ss(SS_CON, ftoi((-0.2) * itof(query_acc_exp(SS_CON))));
    foreach (int skill : query_all_skill_types()) {
        increase_ss(skill, ftoi((-0.05) * itof(query_acc_exp(skill))));
    }
}

/*
 * Function name:   reincarnate
 * Description:     Manages the reincarnation of a player
 */
public void
reincarnate()
{
    LOGIN_NEW_PLAYER->reincarnate_me();
}

/*
 * Function name:   remove_ghost
 * Description:     This is normally not called except when a poor soul has
 *		    not been fetched by Death and reincarnated.
 * Arguments:       quiet : true if no messages should be printed
 * Returns:         0 if the player is not a ghost
 *                  1 otherwise.
 */
public int
remove_ghost(int quiet)
{
    if (!quiet && query_ghost())
    {
	   tell_object(this_object(), "Zostajesz nagle wskrzeszon" + 
	       koncowka("y", "a", "e") + "!\n");
	   say(QCIMIE(this_object(), PL_MIA) + " zostaje nagle wszkrzeszon" +
	       koncowka("y", "a", "e") + "!\n");
    }

    set_ghost(0);

    save_me();
    return 1;
}
