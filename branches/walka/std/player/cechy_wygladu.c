/*
 * Sa tutaj zdefiniowane cechy takie jak: kolor_oczu, 
 * wiek oraz budowa_ciala i cecha_szczegolna(opcjonalnie)
 * +czy ktos jest lysy na stale.
 *
 *          Lil.
 */

string *kolor_oczu = ({});
int wiek;
string *budowa_ciala = ({});
string *cecha_szczegolna=({});
int lysy_na_stale;


public string
*query_kolor_oczu()
{
    return kolor_oczu;
}

public void
set_kolor_oczu(string *str)
{
    kolor_oczu = str;
}

public int
query_wiek()
{
    return wiek;
}

public int
set_wiek(int liczba)
{
    wiek = liczba;
}

public int
query_lysy_na_stale()
{
    return lysy_na_stale;
}
public int //jesli chcemy kogos lysego na stale, to dajemy mu 1.
set_lysy_na_stale(int liczba)
{
    if(liczba==1)
       this_object()->set_dlugosc_wlosow(0.0);

    lysy_na_stale = liczba;
}

public string
*query_budowa_ciala()
{
   return budowa_ciala;
}

public string
set_budowa_ciala(string *str)
{
   budowa_ciala = str;
}

public string
*query_cecha_szczegolna()
{
   return cecha_szczegolna;
}

public string
set_cecha_szczegolna(string *str)
{
   cecha_szczegolna = str;
}
