/*
 * Oto plik, w kt�rym mie�ci si� ca�y kod zarz�dzaj�cy sygna�ami nawi�zywania walki mi�dzy dwoma graczami.
 * signal_fight miedzy graczem a npcem jest w /std/npc.c
 * Vera
 */

#include <macros.h>
#include <filter_funs.h>
object atakujacy;

public int
prepare_signal_fight_pvp()
{
//find_player("vera")->catch_msg("signal PVP!\n");

     //Wysylanie sygnalu o atakowaniu na sasiednie lokacje.
    int i=0,y=0;
    object lok=environment(TP);
    string *sasiednie_lok=lok->query_exit_rooms();
    //object *liv=FILTER_LIVE(all_inventory(find_object(sasiednie_lok[i])));
    object *liv;
    //Wysylamy do livingow na sasiednich lokacjach:
    for(i=0;i<sizeof(sasiednie_lok);i++)
    {
         //Sprawdzamy wpierw, czy sasiednia lokacja jest w ogole zaladowana do pamieci
         if(find_object(sasiednie_lok[i]))
         {
             liv=FILTER_LIVE(all_inventory(find_object(sasiednie_lok[i])));
             for(y=0;y<sizeof(liv);y++)
             {
                 set_alarm(2.0,0.0,"alarm_na_signal_fight",liv[y],TP, lok);
             }
              //Dojebie jeszcze taki maly event. ;)
             string exit_cmd=environment(this_object())->query_exit_cmds()[i], str;
             switch(exit_cmd)
             {
                 case "wsch�d": str="z zachodu"; break;
                 case "zach�d": str="ze wschodu"; break;
                 case "p�noc": str="z po�udnia"; break;
                 case "po�udnie": str="z p�nocy"; break;
                 case "po�udniowy-wsch�d": str="z p�nocnego-zachodu"; break;
                 case "po�udniowy-zach�d": str="z p�nocnego-wschodu"; break;
                 case "p�nocny-wsch�d": str="z po�udniowego-zachodu"; break;
                 case "p�nocny-zach�d": str="z po�udniowego-wschodu"; break;
                 case "g�ra": str="z do�u"; break;
                 case "d�": str="z g�ry"; break;
                 default: str="z oddali"; break;
             }
             tell_room(sasiednie_lok[i] ,capitalize(str)+
                       " dochodz^a twych uszu odg^losy walki!\n");
         }
    }
    //Wysylamy do livingow na tej samej lokacji:
    object *liv_here=FILTER_OTHER_LIVE(all_inventory(lok));
    for(i=0;i<sizeof(liv_here);i++)
    {
        //liv_here[i]->signal_fight(ob, lok);
        set_alarm(2.0,0.0,"alarm_na_signal_fight",liv_here[i],TP,lok);
    }

}

public int
alarm_na_signal_fight(object dokogo,object ob, object lok)
{
    object atakowany=ob->query_enemy(); //Tu jest na odwrot niz w /std/npc.
    atakujacy=ob;
    dokogo->signal_fight(atakowany, lok, environment(dokogo));
    return 1;
}

public int
signal_fight(object atakowany, object gdzie, object skad)
{

    if (!atakowany || !atakujacy) {
        return 1;
    }
    if(atakowany->query_enemy())
    {
       //Przesylamy sygnal dalej:
       int i,y,byl_donos=0;
       object *liv;
       string *sasiednie_lok=environment(this_object())->query_exit_rooms();
       for(i=0;i<sizeof(sasiednie_lok);i++)
       {
           //Nie przesylamy sygnalu na lokacje, skad on pochodzi...i jesli ucieklismy
           if(sasiednie_lok[i] != file_name(gdzie) && find_object(sasiednie_lok[i]) &&
              sasiednie_lok[i] != file_name(skad) && environment(atakowany) == environment(atakujacy))
           {
//find_player("vera")->catch_msg("signal_fight! Atakowany: "+file_name(atakowany)+", gdzie: "+file_name(gdzie)+"\n");
               liv=FILTER_LIVE(all_inventory(find_object(sasiednie_lok[i])));
               for(y=0;y<sizeof(liv);y++)
               {
                  //liv[y]->signal_fight(atakowany, gdzie, environment(this_object()));
                  set_alarm(1.5,0.0,"alarm_na_signal_fight",liv[y],atakowany,gdzie);
               }
           }
       }
    }

    return 1;
}
