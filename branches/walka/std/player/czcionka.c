/*
 * Taki ma�y pliczek pomocniczy, do obs�ugi r�nych standard�w
 * kodowania polskich znak�w.
 */
#define FONT_STANDARD   0
#define FONT_LATIN2     1
#define FONT_WIN1250    2
#define FONT_UTF8       4

int czcionka;

int
czcionka()
{
    return czcionka;
}

void
ustaw_czcionke(int i)
{
    czcionka = i;
}

int
query_czcionka()
{
    return czcionka;
}
