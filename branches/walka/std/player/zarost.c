//Poprawki 10.07.2006, Vera.

/*
 *   uwaga: podczas recznych prob modyfikacji zarostu - 
 *             przymiotniki si� nie zmieniaj�
 *
 *
 * ostatnie poprawki: Grudzie�'06, Vera
*/
#include <macros.h>
// w sekundach (float)
#define CO_ILE_ROSNIE		1200.0
// ile sekund ro�nie jeden centymetr
#define SZYBKOSC_ROSNIECIA	156920.0

// wszystko w centymetrach
float dlugosc_wlosow;
float dlugosc_brody;
float dlugosc_wasow;
float czas_do_zejscia_farby;

string *naturalny_kolor_wlosow, *kolor_wlosow, *kolor_zarostu;
string opis_fryzury;
string typ_brody;
string typ_wasow;

int priorytet_naturalnego; //..koloru wlosow.
int priorytet_farby; //analogicznie...
int priorytet_zarostu;

void rosniecie();
void przym_od_wlosow(); //sprawdzamy przymiotniki zwiazane z wlosami
void przym_od_zarostu(); //sprawdzamy przymiotniki zwiazane z zarostem


// ustawia wszystkie alarmy itede
void
init_zarost()
{
     if(TP->query_race() == "byt astralny")
        return;


    if (intp(dlugosc_wlosow)) dlugosc_wlosow = 0.0;
    if (intp(dlugosc_brody)) dlugosc_brody = 0.0;
    if (intp(dlugosc_wasow)) dlugosc_wasow = 0.0;
    if (intp(czas_do_zejscia_farby)) czas_do_zejscia_farby = 0.0;
    if (!typ_brody) typ_brody = "zaniedban�";
    if (!typ_wasow) typ_wasow = "zaniedbane";
    if (!opis_fryzury) opis_fryzury = "w totalnym nie�adzie";
    if (!sizeof(naturalny_kolor_wlosow)) naturalny_kolor_wlosow += ({"matowe"});
    if (!sizeof(kolor_wlosow)) kolor_wlosow += ({"matowe"});

    add_subloc("fryzura_na_glowie", TO);
    add_subloc("subloc_zarost", TO);
    add_subloc("kwiaty_we_wlosach", TO);

    set_alarm(CO_ILE_ROSNIE, CO_ILE_ROSNIE, rosniecie);
}

void
rosniecie()
{
    if (czas_do_zejscia_farby > 0.0)
    {
	czas_do_zejscia_farby -= CO_ILE_ROSNIE;
	if (czas_do_zejscia_farby <= 0.0)
	{
	    czas_do_zejscia_farby = 0.0;
	    TO->usun_przym_z_listy(TO->query_priorytet_farby(),
	                     TO->query_kolor_wlosow()[1],
	                     TO->query_kolor_wlosow()[2]);
	    kolor_wlosow = naturalny_kolor_wlosow;
	    TO->dodaj_przym_do_listy(TO->query_priorytet_naturalnego(),
	                     TO->query_naturalny_kolor_wlosow()[1],
	                     TO->query_naturalny_kolor_wlosow()[2]);
	    
	}
    }
    if(TO->query_lysy_na_stale()==1)//to jest zdefiniowane w cechy_wygladu.c
    {
       dlugosc_wlosow=0.0;
    }
    else
    {
       dlugosc_wlosow += CO_ILE_ROSNIE / SZYBKOSC_ROSNIECIA;
    }

    //jesli jest dzieciakiem, to tez nie rosnie zarost. Lil.
    if (TO->query_gender() != 1
	&& TO->query_race_name() != "elf"
        && TO->query_wiek()>19)
    {
	dlugosc_brody += CO_ILE_ROSNIE / SZYBKOSC_ROSNIECIA;

	dlugosc_wasow += CO_ILE_ROSNIE / SZYBKOSC_ROSNIECIA;

        przym_od_zarostu();
    }
    else
        kolor_zarostu=({});

    przym_od_wlosow(); //nawet je�li jest �ysy ;)

    TP->check_if_idle(TP); /*A dowale tutaj, bo czemu by nie?
                          skoro jest ju� jeden alarm "rosniecie",
                          to nie bede tworzy� specjalnie nowego, dla
                          dodawania przymiotnika "zamy�lony" ;) */
}

public string
opis_dlugosci_wlosow()
{
    switch (ftoi(dlugosc_wlosow) + 1)
    {
	case 0..3: return "";
	case 4..6: return "bardzo kr�tkie";
	case 7..10: return "kr�tkie";
	case 11..30: return "�redniej d�ugo�ci";
	case 31..40: return "si�gaj�ce ramion";
	case 41..50: return "d�ugie";
	case 51..65: return "bardzo d�ugie";
	case 66..85: return "si�gaj�ce pasa";
	case 86..110: return "si�gaj�ce po�owy ud";
	case 111..160: return "si�gaj�ce kolan";
	default: return "si�gaj�ce ziemi";
    }
}

public string
opis_wlosow(object for_obj = 0)
{
    string str = TO->koncowka("Jego", "Jej", "Jego");

    if (for_obj == TO) switch (ftoi(dlugosc_wlosow) + 1)
    {
	case 0..3: return "Jeste� �ys" + TO->koncowka("y", "a", "e") + ".\n";
	case 4..6: return "Twoje bardzo kr�tkie, " + kolor_wlosow[0] + " w�osy s� " + opis_fryzury + ".\n";
	case 7..10: return "Twoje kr�tkie, " + kolor_wlosow[0] + " w�osy s� " + opis_fryzury + ".\n";
	case 11..30: return "Twoje �redniej d�ugo�ci, " + kolor_wlosow[0] + " w�osy s� " + opis_fryzury + ".\n";
	case 31..40: return "Twoje si�gaj�ce ramion, " + kolor_wlosow[0] + " w�osy s� " + opis_fryzury + ".\n";
	case 41..50: return "Twoje d�ugie, " + kolor_wlosow[0] + " w�osy s� " + opis_fryzury + ".\n";
	case 51..65: return "Twoje bardzo d�ugie, " + kolor_wlosow[0] + " w�osy s� " + opis_fryzury + ".\n";
	case 66..85: return "Twoje si�gaj�ce pasa, " + kolor_wlosow[0] + " w�osy s� " + opis_fryzury + ".\n";
	case 86..110: return "Twoje si�gaj�ce po�owy ud, " + kolor_wlosow[0] + " w�osy s� " + opis_fryzury + ".\n";
	case 111..160: return "Twoje si�gaj�ce kolan, " + kolor_wlosow[0] + " w�osy s� " + opis_fryzury + ".\n";
	default: return "Twoje si�gaj�ce ziemi, " + kolor_wlosow[0] + " w�osy s� " + opis_fryzury + ".\n";
    }
    else switch (ftoi(dlugosc_wlosow) + 1)
    {
	case 0..3: return "Jest �ys" + TO->koncowka("y", "a", "e") + ".\n";
	case 4..6: return str + " bardzo kr�tkie, " + kolor_wlosow[0] + " w�osy s� " + opis_fryzury + ".\n";
	case 7..10: return str + " kr�tkie, " + kolor_wlosow[0] + " w�osy s� " + opis_fryzury + ".\n";
	case 11..30: return str + " �redniej d�ugo�ci, " + kolor_wlosow[0] + " w�osy s� " + opis_fryzury + ".\n";
	case 31..40: return str + " si�gaj�ce ramion, " + kolor_wlosow[0] + " w�osy s� " + opis_fryzury + ".\n";
	case 41..50: return str + " d�ugie, " + kolor_wlosow[0] + " w�osy s� " + opis_fryzury + ".\n";
	case 51..65: return str + " bardzo d�ugie, " + kolor_wlosow[0] + " w�osy s� " + opis_fryzury + ".\n";
	case 66..85: return str + " si�gaj�ce pasa, " + kolor_wlosow[0] + " w�osy s� " + opis_fryzury + ".\n";
	case 86..110: return str + " si�gaj�ce po�owy ud, " + kolor_wlosow[0] + " w�osy s� " + opis_fryzury + ".\n";
	case 111..160: return str + " si�gaj�ce kolan, " + kolor_wlosow[0] + " w�osy s� " + opis_fryzury + ".\n";
	default: return str + " si�gaj�ce ziemi, " + kolor_wlosow[0] + " w�osy s� " + opis_fryzury + ".\n";
    }
}

/*  Jest to funkcja pomocnicza - m.in. dla tych postaci, kt�re zosta�y
 *  za�o�one przed reform� zarostu, a tak�e...mo�e w przysz�o�ci: dla tych,
 *  kt�rych na �yczenie postarzejemy, czyni�c ich na tyle starych (>19), �e
 *  dostan� zarost. Vera
 *  PS. Ta funkcja jest inicjowana automatycznie jakby co :)
 */
void
set_kolor_zarostuX()
{
    if(!sizeof(naturalny_kolor_wlosow))
    {
        write("Wyst�pi� paskudny b�ad zwi�zany z zarostem. "+
              "Zg�o� to natychmiast!\n");
        notify_wizards(TP,"Ten gracz nie ma naturalnego_koloru_w�os�w!");
        return;
    }
    int p_zarost;
    string kolor;
    //robimy tak, bo nie chcemy, zeby ktos mial np. z�ot� lub kasztanow� brod� :P
    switch(naturalny_kolor_wlosow[0])
    {
       case "bia�e": kolor="bia��"; p_zarost=4;
                    kolor_zarostu=({kolor,"bia�obrody","bia�obrodzi"}); break;
       case "z�ote": 
       case "jasne": kolor="jasn�"; p_zarost=3;
                   kolor_zarostu=({kolor,"jasnobrody","jasnobrodzi"}); break;
       case "siwe": 
       case "srebrne": kolor="siw�"; p_zarost=3;
                   kolor_zarostu=({kolor,"siwobrody","siwobrodzi"}); break;
       case "popielate": kolor="popielat�"; p_zarost=3;
             kolor_zarostu=({kolor,"popielatobrody","popielatobrodzi"}); break;
       case "kasztanowe":
       case "zielone":
       case "ciemne": kolor="ciemn�"; p_zarost=5;
               kolor_zarostu=({kolor,"ciemnobrody","ciemnobrodzi"}); break;
       case "rude": kolor="rud�"; p_zarost=5;
               kolor_zarostu=({kolor,"rudobrody","rudobrodzi"}); break;
       case "pomara�czowe": kolor="pomara�czow�"; p_zarost=7;
               kolor_zarostu=({kolor,"pomara�czowobrody",
                               "pomara�czowoobrodzi"}); break;
       case "ogniste":
       case "czerwone": kolor="czerwon�"; p_zarost=7;
               kolor_zarostu=({kolor,"czerwonobrody","czerwonobrodzi"}); break;
       case "kruczoczarne":
       case "czarne": kolor="czarn�"; p_zarost=5;
             kolor_zarostu=({kolor,"czarnobrody","czarnobrodzi"}); break;
       default: kolor=naturalny_kolor_wlosow[0]; p_zarost=4;
                kolor=kolor[..-2]+"�";
                string bla1=explode(TP->query_naturalny_kolor_wlosow()[1],
                         "w^losy")[0]+"brody";
                string bla2=explode(TP->query_naturalny_kolor_wlosow()[2],
                         "w^losi")[0]+"brodzi";
                string *blabla_arr= ({bla1,bla2});
                kolor_zarostu=({kolor,blabla_arr[0],
                              blabla_arr[1]}); break;
    }

    if(!priorytet_zarostu)
        priorytet_zarostu=p_zarost;

}

public string
opis_zarostu(object for_obj = 0)
{
    int jeszcze_zarost=0;
    string str = TO->koncowka("Jego", "Jej", "Jego");
    string pre, opis1, opis2;
    int zarost = (dlugosc_brody < 1.0); // 0 lub 1

    if (TO->query_gender() == 1
	|| TO->query_race_name() == "elf"
        || TO->query_wiek()<19)
	return "";

    if(!sizeof(kolor_zarostu))
        set_kolor_zarostuX();

    string kolor=kolor_zarostu[0];

    if (for_obj == TO)
	pre = "Posiadasz ";
    else
	pre = "Posiada ";

    // w milimetrach
    switch (ftoi(dlugosc_brody * 10.0))
    {
	case 0: opis1 = ""; break;
	case 1..3: opis1 = "ledwie widoczny zarost"; jeszcze_zarost=1; break;
	case 4..6: opis1 = "lekki zarost"; jeszcze_zarost=2; break;
	case 7..9: opis1 = "kilkudniowy zarost"; jeszcze_zarost=3; break;
	case 10..20: opis1 = "kr�ciutk�"; break;
	case 21..40: opis1 = "kr�tk�"; break;
	case 41..70: opis1 = "�redniej d�ugo�ci"; break;
	case 71..180: opis1 = "si�gaj�c� szyi"; break;
	case 181..390: opis1 = "si�gaj�c� piersi"; break;
	case 391..1000: opis1 = "si�gaj�c� pasa"; break;
	case 1001..1590: opis1 = "si�gaj�c� kolan"; break;
	default: opis1 = "si�gaj�c� ziemi"; break;
    }

    if (opis1 != "")
    {
	if (zarost)
	{
	    // np. czarne -> czarny
	    if (kolor[-1..-1] == "e")
	       kolor = kolor[..-2] + "y";
	       
	    if(!jeszcze_zarost)
	       opis1 = (for_obj == TO ? "Twoj�" : "Jego") + 
	               " twarz pokrywa " + kolor + ", " + opis1;
	    else
	       opis1 = (for_obj == TO ? "Twoj�" : "Jego") + 
	               " twarz pokrywa " + opis1;
	}
	else
	{
	    // np. czarne -> czarn�
	    if (kolor[-1..-1] == "e")
	        kolor = kolor[..-2] + "�";
	        
	    //if(!jeszcze_zarost)
	        opis1 = (for_obj == TO ? "Posiadasz " : "Posiada ")+
	        opis1 + ", " + kolor + ", " + typ_brody + " brod�";
	    /*else
	        opis1 = (for_obj == TO ? "Posiadasz " : "Posiada ")+
	        opis1 + ", " + kolor + ", " + typ_brody + " brod�";*/
	}
    }

    /*Je�li mamy jeszcze zarost(<=2) to nie pokazujemy w�s�w*/
    if((jeszcze_zarost==1 || jeszcze_zarost==2) && dlugosc_wasow<=0.5)
        return opis1 + ".\n";

    // w centymetrach
    switch (ftoi(dlugosc_wasow * 10.0))
    {
	case 0: opis2 = ""; break;
	case 1..3: opis2 = "kr�ciutkie"; break;
	case 4..6: opis2 = "kr�tkie"; break;
	case 7..13: opis2 = "�redniej d�ugo�ci"; break;
	case 14..19: opis2 = "d�ugie"; break;
	default: opis2 = "bardzo d�ugie"; break;
    }

    if (opis1 == "" && opis2 == "")
	return "";

    if (opis1 == "")
	return (for_obj == TO ? "Posiadasz " : "Posiada ") + opis2 + ", " +
	    typ_wasow + " " + naturalny_kolor_wlosow[0] + " w�sy.\n";

    if (opis2 == "")
	return opis1 + ".\n";

    if (zarost)
	return opis1 + ", " + (for_obj == TO ? "posiadasz" : "posiada") +
	    " te� " + opis2 + ", " + typ_wasow + " w�sy.\n";

    return opis1 + " i " + opis2 + ", " + typ_wasow + " w�sy.\n";

// wasy
// 1-2 kr�ciutkie
// 3-4 kr�tkie
// 5-9 �r.d�.
// 10-15 d�ugie
// 16 b.d�.
// broda
// 1-2 kr�ciutka
// 3-4 kr�tka
// 5-7 �rednia
// 8-18 si�gaj�ca szyi
// 19-39 si�gaj�ca piersi
// 40-100 s. pasa
// 101-159 s. kolan
// 160+ s. ziemi
// zarost
// 1-3, 4-7, 8-10
}

/* Ponizsza funkcje popelnil Rantaur */
string opis_kwiatow(object for_obj = 0)
{
    object *kwiaty = TO->subinventory("kwiaty_we_wlosach");
    int arrsize = sizeof(kwiaty); 

    if(!arrsize || !kwiaty)
        return "";

    if(for_obj == TO)
        return "We w�osach masz "+COMPOSITE_DEAD(kwiaty, PL_BIE)+".\n";

    string verb;

    if(arrsize == 1)
        verb = "wplecion"+kwiaty[0]->koncowka("y", "�", "e");
    else
        verb = "wplecione";


    return "We w�osy ma "+verb+" "+COMPOSITE_DEAD(kwiaty, PL_BIE)+".\n";

}

void
przym_od_wlosow()
{
    if(TO->query_dlugosc_wlosow()>=3.0) //To na wszelki wypadek,
    {                                       //jakby ktos chcial sie ogolic..
        TP->usun_przym_z_listy(8,"^lysy","^lysi");

        if(naturalny_kolor_wlosow[0] != kolor_wlosow[0]) //tzn, ze ma farbe
            TP->dodaj_przym_do_listy(TP->query_priorytet_farby(),
                           kolor_wlosow[1],kolor_wlosow[2]);
        else //tzn. ze ma naturalny
        {
            TP->usun_przym_z_listy(TP->query_priorytet_farby(),
                           kolor_wlosow[1],kolor_wlosow[2]);

            TP->dodaj_przym_do_listy(TP->query_priorytet_naturalnego(),
                           naturalny_kolor_wlosow[1],naturalny_kolor_wlosow[2]);
        }
    }
    else //Jest lysy...
    {
        TP->usun_przym_z_listy(TP->query_priorytet_farby(),
                           kolor_wlosow[1],kolor_wlosow[2]);
        TP->usun_przym_z_listy(TP->query_priorytet_naturalnego(),
                           naturalny_kolor_wlosow[1],naturalny_kolor_wlosow[2]);
        TP->dodaj_przym_do_listy(8,"^lysy","^lysi");
    }
}

void
przym_od_zarostu()
{
    //Tutaj sa dodawane przymiotniki wynikajace z koloru i dlugosci posiadanej brody
    int x1=TP->query_priorytet_zarostu()+1;
    int x2=x1+1;

    TP->usun_przym_z_listy(TP->query_priorytet_zarostu(),
                      kolor_zarostu[1],kolor_zarostu[2]);
    TP->usun_przym_z_listy(x1,
                      kolor_zarostu[1],kolor_zarostu[2]);
    TP->usun_przym_z_listy(x2,
                      kolor_zarostu[1],kolor_zarostu[2]);

    TP->usun_przym_z_listy(6,"w�saty","w�saci");

    if(TP->query_dlugosc_brody()<6.0 && TP->query_dlugosc_wasow()<1.0)
        return;


    switch(ftoi(TP->query_dlugosc_brody()))
    {
        case 0..10: 
            if((dlugosc_wasow*10.0)>=10.0) //w centymetrach
                TP->dodaj_przym_do_listy(6,
                      "w�saty","w�saci");
            break;
        case 11..15:
            TP->dodaj_przym_do_listy(TP->query_priorytet_zarostu(),
                      kolor_zarostu[1],kolor_zarostu[2]);
            break;
        case 16..45:
            TP->dodaj_przym_do_listy(x1,
                      kolor_zarostu[1],kolor_zarostu[2]);
            break;
       case 46..999:
            TP->dodaj_przym_do_listy(x2,
                      kolor_zarostu[1],kolor_zarostu[2]);
            break;
       default:
            TP->dodaj_przym_do_listy(TP->query_priorytet_zarostu(),
                      kolor_zarostu[1],kolor_zarostu[2]);
            break;
    }


}


// funkcje do sprawdzania i ustawiania d�ugo�ci, i paru innych rzeczy

public void
set_dlugosc_wlosow(mixed x)
{
    if (floatp(x))
	dlugosc_wlosow = x;
    else
	dlugosc_wlosow = itof(x);
}

public float
query_dlugosc_wlosow()
{
    return dlugosc_wlosow;
}

public void
set_dlugosc_brody(mixed x)
{
    if (floatp(x))
	dlugosc_brody = x;
    else
	dlugosc_brody = itof(x);
}

public float
query_dlugosc_brody()
{
    return dlugosc_brody;
}

public void
set_dlugosc_wasow(mixed x)
{
    if (floatp(x))
	dlugosc_wasow = x;
    else
	dlugosc_wasow = itof(x);
}

public float
query_dlugosc_wasow()
{
    return dlugosc_wasow;
}

public void
set_typ_brody(string typ)
{
    typ_brody = typ;
}

public string
query_typ_brody()
{
    return typ_brody;
}

public void
set_typ_wasow(string typ)
{
    typ_wasow = typ;
}

public string
query_typ_wasow()
{
    return typ_wasow;
}

public void
set_opis_fryzury(string opis)
{
    opis_fryzury = opis;
}

public string
query_opis_fryzury()
{
    return opis_fryzury;
}

public void
set_naturalny_kolor_wlosow(string k,string a, string b)
{
    naturalny_kolor_wlosow = ({k,a,b});
    kolor_wlosow = ({k,a,b});
}

public mixed
query_naturalny_kolor_wlosow()
{
    return naturalny_kolor_wlosow;
}

public void
set_kolor_wlosow(string k,string a, string b)
{
    kolor_wlosow = ({k,a,b});
    czas_do_zejscia_farby = 2592000.0; // 1 miesi�c
}

public mixed
query_kolor_wlosow()
{
    return kolor_wlosow;
}

public void
//set_kolor_zarostu(string k,string a, string b)
set_kolor_zarostu(string *k)
{
    kolor_zarostu = ({});
    kolor_zarostu = k;
}

public mixed
query_kolor_zarostu()
{
    return kolor_zarostu;
}


public int
set_priorytet_naturalnego(int x)
{
    if(x>10) x=10;
    else if(x<0) x=0;

    priorytet_naturalnego = x;
}

public int
query_priorytet_naturalnego()
{
    return priorytet_naturalnego;
}

public int
set_priorytet_farby(int x)
{
    if(x>10) x=10;
    else if(x<0) x=0;
        
    priorytet_farby = x;
}

public int
query_priorytet_farby()
{
    return priorytet_farby;
}

public int
set_priorytet_zarostu(int x)
{
    if(x>10) x=10;
    else if(x<0) x=0;

    priorytet_zarostu = x;
}

public int
query_priorytet_zarostu()
{
    return priorytet_zarostu;
}
