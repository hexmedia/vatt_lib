#include <pl.h>
#include <stdproperties.h>
#include <object_types.h>

inherit "/std/object";

string* _rasa;
int _rodzaj_rasy;

void
create_object()
{
    ustaw_nazwe("sk�ra");
    set_long("Krwawa sk�ra, zbyt zniszczona, by m�c ustali� jej" +
            " poprzedniego w�a�ciciela.\n");
    add_prop(OBJ_I_WEIGHT, 2000);
    add_prop(OBJ_I_VOLUME, 1500);
    add_prop(OBJ_I_VALUE, 5);
    set_type(O_SKORY);
}

public varargs void
leftover_init(string organ, string *rasa, int rodzaj_rasy = PL_MESKI_NOS_ZYW, int add_value = 1)
{
    _rasa = rasa;
    _rodzaj_rasy = rodzaj_rasy;

    switch (rasa[0]) {
        case "zaj^ac":
            dodaj_przym("zaj�czy","zaj�czy");
            if (add_value)
            add_prop(OBJ_I_VALUE, 8 + random(5));
            break;
        case "wiewi^orka":
            dodaj_przym("wiewi�rczy", "wiewi�rczy");
            if (add_value)
            add_prop(OBJ_I_VALUE, 10 + random(5));
            break;
        case "kr^olik":
            dodaj_przym("kr�liczy", "kr�liczy");
            if (add_value)
            add_prop(OBJ_I_VALUE, 18 + random(5));
            break;
        case "borsuk":
            dodaj_przym("borsuczy", "borsuczy");
            if (add_value)
            add_prop(OBJ_I_VALUE, 20 + random(5));
            break;
        case "lis":
            dodaj_przym("lisi", "lisi");
            if (add_value)
            add_prop(OBJ_I_VALUE, 28 + random(10));
            break;
        case "ry^s":
            dodaj_przym("rysi", "rysi");
            if (add_value)
            add_prop(OBJ_I_VALUE, 33 + random(10));
            break;
        case "jele^n":
            dodaj_przym("jeleni", "jeleni");
            if (add_value)
            add_prop(OBJ_I_VALUE, 150 + random(15));
            break;
        case "sarna":
            dodaj_przym("sarni", "sarni");
            if (add_value)
            add_prop(OBJ_I_VALUE, 90 + random(15));
            break;
        case "nied^xwied^x":
            dodaj_przym("nied�wiedzi", "nied�wiedzi");
            if (add_value)
            add_prop(OBJ_I_VALUE, 350 + random(30));
            break;
        case "^zubr":
            dodaj_przym("�ubrzy", "�ubrzi");
            if (add_value)
            add_prop(OBJ_I_VALUE, 230 + random(30));
            break;
        case "tur":
            dodaj_przym("turzy", "turzi");
            if (add_value)
            add_prop(OBJ_I_VALUE, 220 + random(30));
            break;
        case "^lo^s":
            dodaj_przym("�osiowy", "�osowi");
            if (add_value)
            add_prop(OBJ_I_VALUE, 210 + random(30));
            break;
        default:
        return;
    }

    set_long("Krwawa sk�ra, najprawdopodobniej jakie" + (rodzaj_rasy == PL_ZENSKI ? "j^s"
                : "go^s") + " " + rasa[PL_DOP] + ".\n");
}

public string
query_skora_auto_load()
{
    if (pointerp(_rasa))
        return " #skora#" + _rodzaj_rasy + "," + _rasa[0] + "," + _rasa[1] + "," + _rasa[2] +
            "," + _rasa[3] + "," + _rasa[4] + "," + _rasa[5] + "#skora# ";
    return "";
}

public string
init_skora_arg(string arg)
{
    string foobar, toReturn;
    int tmp;
    string a1, a2, a3, a4, a5, a6;

    if (arg == 0) {
        return 0;
    }

    if (sscanf(arg, "%s#skora#%d,%s,%s,%s,%s,%s,%s#skora#%s",
                foobar, tmp, a1, a2, a3, a4, a5, a6, toReturn) == 9) {
        leftover_init(0, ({ a1, a2, a3, a4, a5, a6 }), tmp, 0);
        return toReturn;
    }
    return arg;
}

public string
query_auto_load()
{
    return ::query_auto_load() + query_skora_auto_load();
}

public string
init_arg(string arg)
{
    return init_skora_arg(::init_arg(arg));
}
