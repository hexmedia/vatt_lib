/** 
 * /std/room/description.c
 *
 * This is a sub-part of /std/room.c
 *
 * In this module you will find the things relevant to the description of the
 * room.
 *
 * TODO(zielarstwo):
 * - pokazywanie si� znalezionych zi� w d�ugim opisie lokacji(robi� co� takiego)
 * - lekkie przerobienie reset_herbs, �eby wszystko co w zio�ach ustawiane bra�o
 *   pod uwag�.
 * - powolne zapominanie znalezionych zi�(automatyczne)
 * - (p) reset_herbs musi sprawdza� ilo�� w ziele, i losowa� poszczeg�lne zio�a
 */

#include <composite.h>
#include <language.h>
#include <login.h>
#include <mudtime.h>
#include <ss_types.h>
#include <stdproperties.h>
#include <colors.h>
#include <math.h>
#include <exp.h>

#define EXITS_COLOR COLOR_FG_YELLOW
#define HERBS_DIR     "/d/Standard/ziola/"

static  mixed   room_descs;         /* Extra longs added to the rooms own */
static  mixed   herbs;              /* Pliki wszystkich zi�, kt�re mog� by� w roomie. */
static  mixed   rosnace;            /* Jakie zio�a w chwili obecnej rosn� na lokacji */
static  mapping found_herbs = ([]); /* Jakie zio�a przez jakiego gracza zosta�y znalezione */
static  int gDefaultExitDesc = 0;

#define DBG(x) find_player("krun")->catch_msg("B��D W ROOMIE(descr)" + x + "\n")


/*
 * Function name: 	add_my_desc
 * Description:   	Add a description printed after the normal
 *                      longdescription.
 * Arguments:	  	str: Description as a string
 *			cobj: Object responsible for the description
 *			      Default: previous_object()
 */
public void
add_my_desc(string str, object cobj = previous_object())
{
    if (query_prop(ROOM_I_NO_EXTRA_DESC))
	return;

    if (!room_descs)
        room_descs = ({ cobj, str });
    else
        room_descs = room_descs + ({ cobj, str });
}

/*
 * Function name:       change_my_desc
 * Description:         Change a description printed after the normal
 *                      longdescription. NOTE: if an object has more than
 * 			one extra description only one will change.
 * Arguments:           str: New description as a string
 *                      cobj: Object responsible for the description
 *                            Default: previous_object()
 */
public void
change_my_desc(string str, object cobj = previous_object())
{
    int i;
    mixed tmp_descs;

    if (query_prop(ROOM_I_NO_EXTRA_DESC))
	return;

    if (!cobj)
        return;

    i = member_array(cobj, room_descs);

    if (i < 0)
	add_my_desc(str, cobj);
    else
	room_descs[i + 1] = str;
}

/*
 * Function name: 	remove_my_desc
 * Description:   	Removes an earlier added  description printed after
 *                      the normal longdescription.
 * Arguments:	  	cobj: Object responsible for the description
 *			      Default: previous_object()
 */
public void
remove_my_desc(object cobj = previous_object())
{
    int i, sf;

    if (query_prop(ROOM_I_NO_EXTRA_DESC))
	return;

    sf = objectp(cobj);

    i = member_array(cobj, room_descs);
    while (i >= 0)
    {
	if (sf)
	    room_descs = exclude_array(room_descs, i, i + 1);
	else
	    room_descs = exclude_array(room_descs, i - 1, i);
	i = member_array(cobj, room_descs);
    }
}

private mixed
only_first_arg(mixed arg)
{
    if(pointerp(arg))
        return arg[0];
    else 
        return arg;
}

static private string
opisz_ziola(mixed herbs_obs)
{
    mixed tmp;
    string ret;

    herbs_obs = map(herbs_obs, only_first_arg);

    tmp = unique_array(herbs_obs, &->short(TP, PL_MIA));

    if(!pointerp(tmp) || !sizeof(tmp))
        return 0;

    if(pointerp(tmp[0]))
        ret = LANG_CZAS(sizeof(tmp[0]), "Ro�nie", "Rosn�");
    else
        ret = "Ro�nie";

    ret += " tu " + COMPOSITE_DEAD(herbs_obs, PL_MIA) + ".\n"; 
    return ret;
}

/*
 * Function name: 	query_desc
 * Description:   	Gives a list of all added descriptions to this room
 * Returns:       	Array on the form:
 *			    desc1, obj1, desc2, obj2, ..... descN, objN
 */
public mixed
query_desc()
{
    return slice_array(room_descs, 0, sizeof(room_descs));
}

private string
add_colors(string exit)
{
    return set_color(EXITS_COLOR) + exit + clear_color();
}

/*
 * Function name: exits_description
 * Description:   This function will return the obvious exits described
 *		  in a njice way.
 * Returns:	  The obvious exits
 */
public string
exits_description()
{
    string *exits;
    int size;

    exits = query_obvious_exits();
    exits += map((mixed *)(filter(filter(all_inventory(this_object()), &->is_door()), &->query_open())->query_pass_command()) - ({ 0 }), &operator([])(,0));

    exits = map(exits, &add_colors());

    size = sizeof(exits);
    if (size == 0)
	return "";
   
    gDefaultExitDesc = 1;

    switch(ilosc(size, 1, 2, 3))
    {
        case 1:
            return "Jest tutaj jedno widoczne wyj^scie: " + exits[0] + ".\n";
        case 2:
	    return "S^a tutaj " + LANG_SNUM(size, PL_MIA, PL_NIJAKI_NOS)
	         + " widoczne wyj^scia: " + COMPOSITE_WORDS(exits) + ".\n";
        case 3:
            return "Jest tutaj " + LANG_SNUM(size, PL_MIA, PL_NIJAKI_NOS)
                 + " widocznych wyj^s^c: " + COMPOSITE_WORDS(exits) + ".\n";
    }

}

/*
 * Function name: long
 * Description:   Describe the room and possibly the exits
 * Arguments:	  str: name of item or 0
 * Returns:       A string holding the long description of the room.
 */
varargs public mixed
long(string str)
{
    int index;
    int size;
    mixed lg;
    mixed herbs_obs;
    string ex, ziola;

    lg = ::long(str);
    if (stringp(str))
	return lg;
    if (!stringp(lg))
	lg = "";

    /* This check is to remove extra descriptions that have been added by
     * an object that is now destructed.
     */
    while ((index = member_array(0, room_descs)) >= 0)
    {
	room_descs = exclude_array(room_descs, index, index + 1);
    }

    if (pointerp(room_descs))
    {
	index = -1;
	size = sizeof(room_descs);
	while((index += 2) < size)
	{
	    lg = lg + process_tags(check_call(room_descs[index]));
	}
    }

    if (room_no_obvious)
    {
	return lg;
    }

    herbs_obs = TP->query_prop(LIVE_I_FOUND_HERBS);
    if(mappingp(herbs_obs))
        if(member_array(MASTER, m_indexes(herbs_obs)))
            herbs_obs = herbs_obs[MASTER];
        else
            herbs_obs = 0;
    else
        herbs_obs = 0;

    //FIXME: Dopisa� zapominanie znalezionych zi�.

    ex = exits_description();
 //   ziola = opisz_ziola(herbs_obs);

    return lg + (ziola ? ziola : "") + (gDefaultExitDesc ? ex :
        set_color(EXITS_COLOR) + ex + clear_color());
}

/*
 * @param str Je�li zmienna ta jest podana komunikat bedzie j� zawiera�.
 * @return Komunikat gdy gracz nie znajduje zi�.
 */
static string
no_find(string str = 0)
{
    return str ?
        "Nie znajdujesz ^zadnych zi^o^l. " + str + "\n" :
        "Szukasz wsz^edzie, ale nie znajdujesz ^zadnych zi^o^l.\n";
}

static private string
dodaj_sciezke(string plik)
{
    if(plik[0..0] != "/")
        plik = HERBS_DIR + plik;
    if(plik[-2..-1] != ".c")
        plik += ".c";
    return plik;
}

varargs private mixed
clone_herbs(mixed f, int r, int w)
{
    mixed herbs_obs;

    if(pointerp(f))
    {
        if(!w)
            f = map(f, dodaj_sciezke);
        herbs_obs = map(f, clone_object);

        if(r)
            map(herbs_obs, &->ustaw_ze_rosnie(1));
        return herbs_obs;
    }

    if(!w)
        f = dodaj_sciezke(f);

    herbs_obs = clone_object(f); 

    if(r)
        herbs_obs -> ustaw_ze_rosnie(1);

    return herbs_obs;
}

int
mz_filter(mixed ob)
{
    string imie = TP->query_real_name();

    if(!is_mapping_index(imie, found_herbs))
        return (ob[0]->moze_znalezc(TP));
    return (ob[0]->moze_znalezc(TP) && (member_array(ob[0], found_herbs[imie]) == -1));
}

private static
master_ob_sp(mixed ob)
{
    if(pointerp(ob))
        return map(ob, master_ob_sp);
    if(!objectp(ob))
        return;
    return MASTER_OB(ob);	
}

//Pozwala znale�� pierwsze zio�o w grupie.
object *
znajdz_pierwsze_z_grupy(object *ziola)
{
    object *pierwsze = ({});

    foreach(object x : ziola)
    {
        foreach(object *y : rosnace)
        {
            if(x == y[0])
            {
                pierwsze += ({x});
                break;
            }
        }
    }
    return pierwsze;
}

void
clean_found_herbs()
{
    if(m_sizeof(found_herbs))
    {
        foreach(string imie : m_indexes(found_herbs))
        {
            int i;
            if(!found_herbs[imie])
            {
                found_herbs = m_delete(found_herbs, imie);
                continue;
            }
            if((i = member_array(found_herbs[imie], all_inventory(TO))) >= 0)
                found_herbs[imie] = exclude_array(found_herbs[imie], i, i);

            if(!found_herbs[imie])
                found_herbs = m_delete(found_herbs, imie);
        }
    }
}

void
clean_found_herbs_from_ob(object ob)
{
    if(m_sizeof(found_herbs))
    {
        foreach(string imie : m_indexes(found_herbs))
        {

            int i;
            if((i = member_array(ob, found_herbs[imie])) >= 0)
                found_herbs[imie] = exclude_array(found_herbs[imie], i, i);
            else
            {
                for(i=0;i<sizeof(found_herbs[imie]);i++)
                {
                    if(pointerp(found_herbs[imie][i]))
                        if(member_array(ob, found_herbs[imie][i]) >= 0)
                            found_herbs[imie] = exclude_array(found_herbs[imie], i, i);
                }
            }
            if(!found_herbs[imie])
                found_herbs = m_delete(found_herbs, imie);
        }
    }
}

static private nomask void
dodaj_exp_za_nieudane()
{
    TP->increase_ss(SS_HERBALISM, EXP_SZUKANIE_ZIOL_NIEUDANE_HERBALISM);
    TP->increase_ss(SS_AWARENESS, EXP_SZUKANIE_NIEUDANE_AWARENESS);
    TP->increase_ss(SS_WIS, EXP_SZUKANIE_ZIOL_NIEUDANE_WIS);
    TP->increase_ss(SS_INT, EXP_SZUKANIE_ZIOL_NIEUDANE_INT);
}

static private nomask void
dodaj_exp_za_znalezione(int size, int flaga=0)
{
    int i;
    if(!flaga)
    {
        for(i=0;i<size;i++)
        {
            TP->increase_ss(SS_HERBALISM, EXP_SZUKANIE_ZIOL_UDANE_HERBALISM);
            TP->increase_ss(SS_WIS, EXP_SZUKANIE_ZIOL_UDANE_WIS);
            TP->increase_ss(SS_INT, EXP_SZUKANIE_ZIOL_UDANE_INT);
            TP->increase_ss(SS_AWARENESS, EXP_SZUKANIE_UDANE_AWARENESS);
        }
    }
    else
    {
        for(i=0;i<size;i++)
        {
            TP->increase_ss(SS_HERBALISM, EXP_SZUKANIE_ZIOLA_UDANE_HERBALISM);
            TP->increase_ss(SS_WIS, EXP_SZUKANIE_ZIOLA_UDANE_WIS);
            TP->increase_ss(SS_INT, EXP_SZUKANIE_ZIOLA_UDANE_INT);
            TP->increase_ss(SS_AWARENESS, EXP_SZUKANIE_UDANE_AWARENESS);
        }
    }
}

/**
 * Funkcja wywo�ywana gdy gracz szuka na lokacji zi�.
 * @param herb_name Nazwa zio�a, kt�rego gracz szuka. (opcjonalny)
 * @return Wiadomo�� do wypisania na ekranie graczowi.
 */
static varargs string
search_for_herbs(object searched_herb, string searched_herb_name)
{
    /* --- Zmiany Kruna ---*/
    string ret;
    object *znalezione;
    /*---------------------*/

    if (!sizeof(herbs))
    {
        switch (query_prop(ROOM_I_TYPE))
        {
            case ROOM_IN_WATER:
            case ROOM_UNDER_WATER:
                return no_find("Mo^ze posz^loby ci lepiej, gdyby^s szuka^l" +
                       this_player()->koncowka("", "a") + " ich na suchym "
                     + "l^adzie.");
            case ROOM_IN_AIR:
                return no_find("Mo^ze posz^loby ci lepiej, gdyby^s szuka^l" +
                       this_player()->koncowka("", "a") + " ich na bardziej "
                     + "twardym gruncie.");
        }
        if (query_prop(ROOM_I_INSIDE))
            return no_find("Mo^ze posz^loby ci lepiej, gdyby^s szuka^l" +
                   this_player()->koncowka("", "a") + " ich na otwartym "
                 + "terenie.");
        dodaj_exp_za_nieudane();
        return no_find();
    }

    string imie = TP->query_real_name();

    int i;
    object *znalezione_wczesniej;

    if(is_mapping_index(imie, found_herbs))
    {
        if(pointerp(found_herbs[imie]))
            znalezione_wczesniej = found_herbs[imie];
    }

    int searched_found, from_found;
    /* --- Zmiany Kruna --- */
    if(objectp(searched_herb))
    {
        if(member_array(MASTER_OB(searched_herb), map(flatten(rosnace), master_ob_sp)) != -1)
        {
            if(member_array(searched_herb, znalezione_wczesniej) != -1)
            {
                from_found = 1;
                searched_found = 1;
            }
            else if(searched_herb->moze_znalezc(TP) && (searched_herb->moze_zidentyfikowac(TP) || random(5) > 3))
                searched_found = 1;
        }
    }
    else
    {
        if(pointerp(rosnace))
        {
            mixed tmp = rosnace;

            if(pointerp(znalezione_wczesniej))
                if(sizeof(znalezione_wczesniej))
                    tmp -= znalezione_wczesniej;
            znalezione = filter(tmp, mz_filter);
        }
    }

    if(!sizeof(znalezione) && !sizeof(znalezione_wczesniej) && !objectp(searched_herb))
    {
        dodaj_exp_za_nieudane();
        return no_find();
    }
    else if(!sizeof(znalezione) && !sizeof(znalezione_wczesniej) && objectp(searched_herb))
    {
        dodaj_exp_za_nieudane();
    }

    mixed wszystkie_znalezione = (pointerp(znalezione) ? znalezione : ({})) +
            (pointerp(znalezione_wczesniej) ? znalezione_wczesniej : ({}));

    mixed herbs_obs = flatten(wszystkie_znalezione);
    herbs_obs->unset_no_show();
    herbs_obs->set_no_show_composite(0);

    if(objectp(searched_herb) && searched_found)
    {
        wszystkie_znalezione = ({searched_herb});
    }

    if(objectp(searched_herb))
    {
        if(searched_found && !from_found)
            dodaj_exp_za_znalezione(1, 1);

        if(searched_found)
        {
            ret = "Znajdujesz " + searched_herb->short(TP, PL_BIE) + ".\n";
            if(!from_found)
            {
                if(is_mapping_index(imie, found_herbs) && pointerp(found_herbs[imie]))
                    found_herbs[imie] += ({searched_herb});
                else
                    found_herbs[imie] = ({searched_herb});
            }
        }
    }
    else
    {
        dodaj_exp_za_znalezione(sizeof(znalezione), 0);
        ret = opisz_ziola(wszystkie_znalezione);
        found_herbs[imie] = wszystkie_znalezione;
    }
    herbs_obs->set_no_show();

    return ret;
    /*----------------------*/
}

/**
 * Dodaje zio�o do lokacji.
 * @param files Plik lub pliki zio�a. Kilka plik�w mo�emu ustawi� zio�u, kt�re ma kilka cz�ci, np. pietruszka(na� i korze�).
 * @return 1 je�li oddane
 */
int
dodaj_ziolo(mixed files)
{
    if (!files)
        return 0;
    if(stringp(files))
        files = ({files});

    if(!pointerp(files))
        return 0;

    // --- Zmiany Kruna ---
    if (!herbs)
        herbs = ({ files });
    else
        herbs += ({ files });

    return 1;
    //---------------------------
}

// --- Zmiany Kruna ---

/**
 * Funkcja odpowida za maksymaln� ilo�� zi� dost�pn� na lokacji.
 */
public int
ilosc_ziol()
{
    switch(query_prop(ROOM_I_TYPE))
    {
        case ROOM_UNDER_WATER:
        case ROOM_BEACH:
        case ROOM_DESERT:
        case ROOM_IN_CITY:
        case ROOM_IN_AIR:
        case ROOM_CAVE:
        case ROOM_TREE: //Narazie na drzewach nic nie ro�nie.
            return 0;
        case ROOM_NORMAL:
            return 3;
        case ROOM_FOREST:
        case ROOM_SWAMP:
        case ROOM_FIELD:
            return 5;
        case ROOM_MOUNTAIN:
        case ROOM_TRACT:
            return 2;
        case ROOM_IN_WATER:
            return 1;
    }
}

//Zwraca czy zio�o wyst�pi czy te� nie
private int
filter_wystepowania(mixed x)
{
    if(pointerp(x))
    {
        if(x[0]->query_pora_roku() && x[0]->query_pora_roku() != TO->pora_roku())
            return 0;

        return (x[0]->query_czestosc_wystepowania() > random(100));
    }
    else
    {
        if(x->query_pora_roku() && x->query_pora_roku() != TO->pora_roku())
            return 0;

        return (x->query_czestosc_wystepowania() > random(100));
    }
}

public void
reset_herbs()
{
    int ilosc;
    mixed pliki_rosnacych = ({});    

    if(!herbs)
        return;

    ilosc = random(ilosc_ziol());

    if(!ilosc)
        return;

    //Losujemy jakie zi�ka b�d� prawdopodobnie ros�y, potem sprawdzimy jaka
    //jest cz�sto�� wyst�powania.
    //FIXME: Trzeba to uzale�ni� od cz�sto�ci wyst�powania zi�.
    while(ilosc-- > 0)
        pliki_rosnacych += ({herbs[random(sizeof(herbs))]}); 

    flatten(rosnace)->remove_object();

    rosnace = map(pliki_rosnacych, &clone_herbs(, 1));

    int i, size;
    size = sizeof(rosnace);
    for(i=0;i<sizeof(rosnace);i++)
    {
        if(pointerp(rosnace[i]))
        {
            if(i <  size)
            {
                ilosc = random(rosnace[i][0]->query_sposob_wystepowania());
                while(ilosc-- > 0)
                    rosnace += ({rosnace[i]});
            }
            int j, sizew;
            for(j=0;j<sizew;j++)
            {
                if(j < sizew)
                {
                    ilosc = random(rosnace[i]->query_ilosc_w_calosci());
                    while(ilosc-- > 0)
                        rosnace[i] += ({rosnace[i][j]});
                }
            }
        }
        else if(objectp(rosnace[i]) && i < size)
        {
            ilosc = random(rosnace[i]->query_sposob_wystepowania());
            rosnace += ({ rosnace[i] });	    
        }
    }

    rosnace = filter(rosnace, filter_wystepowania);
    flatten(rosnace)->move(TO);
    found_herbs = ([]); //Resetujemy znalezione, bo skoro nowe zio�a s�, to i nikt ich
        //jeszcze nie znalaz�.
}

/**
 * @return Pliki zi�ek jakie mog� znajdowa� si� na lokacji.
 */
string *
query_ziola()
{
    return herbs;
}

/**
 * Ustawia jakie zio�a rosn� na lokacji
 * @param ziola Zio�a rosn�ce na lokacji
 */
void
ustaw_rosnace(object *ziola)
{
    rosnace = ziola;
}

void
usun_rosnace(object *ob)
{
    int i;

    for(i=0;i < sizeof(rosnace);i++)
    {
       if(rosnace[i][0] == ob[0])
       {
	   rosnace = exclude_array(rosnace, i, i);
       }
    }
    if(!is_mapping_index(TP->query_real_name(), found_herbs))
       DBG(UC(TP->query_real_name()) + " prawdopodbnie zebra� zio�o nie znajduj�c go wcze�niej.\n");

    found_herbs[TP->query_real_name()] -= ob;
}

/**
 * @return Mapping indeksowany po plikach(tablicach plik�w) zi�ek, o warto�ciach ilo�ci zi�ek.
 */
mapping
query_rosnace()
{
    return rosnace;
}

/**
 * @param imie Imi� gracza
 * @return Zio�a znalezione przez gracza.
 */
public mixed
query_znalezione(string imie)
{
    if(!is_mapping_index(imie, found_herbs))
        return ;
    return found_herbs[imie];
}

/**
 * Sprawdza czy gracz szuka zi�, nie koniecznie rosn�cych na 
 * lokacji tak jak by�o to w poprzedniej wersji.
 */
/*static*/ string
herb_search(string str)
{
    int i;
    mixed herbs_obs, tmp;

    if(!str)
        return 0;

    if(str ~= "zi�")
        return search_for_herbs();

    herbs_obs = map(rosnace, only_first_arg);

    if(!sizeof(herbs_obs))
        return 0;

    object fherbs_obs = flatten(herbs_obs);
    fherbs_obs->unset_no_show();
    fherbs_obs->set_no_show_composite(0);
    mixed found;
    if(!parse_command(str, herbs_obs, "%i:" + PL_DOP, found))
        return 0;
    found = DIRECT_ACCESS(found);

    if(sizeof(found) > 1)
    {
        NF("Nie mo�esz szuka� wi�cej ni� jednego okre�lonego gatunku zio�a jednocze�nie.\n");
        return 0;
    }
    if(sizeof(!found))
        return 0;

    fherbs_obs->set_no_show();

    if(sizeof(herbs_obs))
        return search_for_herbs(found[0], str);

    return 0;
}

static string 
chrust_search(string str)
{
    if(str ~= "chrustu" || str ~= "chrust")
        if(query_prop(ROOM_I_ILOSC_CHRUSTU) > 0)
            return "Znajdujesz tu troch� chrustu kt�ry "
		    +TP->koncowka("m�g�by�", "mog�aby�")+" zebra�.\n";
        else
            return "Nie widzisz tu ani ga��zki, kt�r� "
		+TP->koncowka("m�g�by�", "mog�aby�")+" zebra�.\n";

    return 0;
}


//--------------------------------------------

/**
 * Sprawdza czy gracz szuka na lokacji jakiego� standardowego obiektu,
 * takiego jak zio�a czy chrust.
 */ 
static string
search_fun(string str, int trail)
{
    string search_result;

    if (!trail && (search_result = chrust_search(str)))
        return search_result;       
    
    if(!trail && (search_result = herb_search(str)))
        return search_result;        
        
    return ::search_fun(str, trail);
}

// dopisuje z przodu "na" jesli kierunek jest standardowy, typu "polnoc"
string
modify_dir(string dir)
{
    switch(dir)
    {
	case "p^o^lnoc":
	case "p^o^lnocny-wsch^od":
	case "wsch^od":
	case "po^ludniowy-wsch^od":
	case "po^ludnie":
	case "po^ludniowy-zach^od":
	case "zach^od":
	case "p^o^lnocny-zach^od":
	case "g^ore":
	case "d^o^l":
	    return "na " + dir;
	case "g^ora":
	    return "na g^or^e";
	default:
	    return dir;
    }
}

/**
 * Funkcja wywo�ywana kiedy gracz tropi.
 * @param player tropi�cy
 * @param track_skill u�yta umiej�tno�� tropienia.
 */
void
track_now(object player, int track_skill)
{
    string *track_arr,
            result = "Nie znajdujesz ^zadnych ^slad^ow.\n",
            dir,
           *dir_arr,
            race,
           *races = RACES + ({ "jakie^s zwierz^e" });
    int     i;
    mixed  *exits;
    
    if (!player)
        return ;

    track_arr = query_prop(ROOM_AS_DIR);

    // just in case, but presently, ROOM_I_INSIDE prevents setting of ROOM_AS_DIR
    if (query_prop(ROOM_I_INSIDE)) 
       result += "Wyst�pi� b��d RII w tropieniu, koniecznie zg�o� go czarodziejom!\n";

    track_skill /= 2;
    track_skill += random(track_skill);

    if (CAN_SEE_IN_ROOM(player) && pointerp(track_arr) && track_skill > 0)
    {
        dir = track_arr[0];

        race = track_arr[1];
        switch (race)
        {
            case "elfk^e": race = "elfa"; break;
            case "p^olelfk^e": race = "elfa"; break;
            case "p^olelfa": race = "elfa"; break;
            case "krasnoludk^e": race = "krasnoluda"; break;
            case "nizio^lk^e": race = "nizio^lka"; break;
            case "m^e^zczyzn^e": race = "cz^lowieka"; break;
            case "kobiet^e": race = "cz^lowieka"; break;
            case "gnomk^e": race = "gnoma"; break;
        }

        result = "Jeste^s w stanie wyr^o^zni^c kilka ^slad^ow na ziemi.\n";

        TP->add_mana(-4);
        TP->add_fatigue(-1);

        switch(track_skill)
        {
            case  1..10:
                player->increase_ss(SS_TRACKING, EXP_TROPIENIE_NIEUDANY_TRACKING);
		        player->increase_ss(SS_WIS, EXP_TROPIENIE_NIEUDANY_TRACKING_WIS);
		        break;

            case 11..20:
                /* Mamy szanse na pomylenie kierunku */
                if(random(3) > 1)
                {
                    exits = query_exit();

                    if((sizeof(exits) > 0) && (!wildmatch("w stron�*", dir)))
                    {
                        /* Oczyszczamy tablice z fake-wyjsc (Rantaur) */
                        int exits_size = sizeof(exits);
                        mixed *to_del = ({ });

                        for(int j = 0; j < exits_size; j++)
                        {
                            if(!stringp(exits[j]))
                                continue;

                            if(explode(exits[j], "#")[0] ~= "/std/drzewo/nadrzewie")
                            {
                                to_del += ({ exits[j] });
                                to_del += ({ exits[j+1] });
                                to_del += ({ exits[j+2] });

                                j += 2;
                            }
                        }
                            
                        exits -= to_del;

                        if(i = sizeof(exits) - 1)
                        {
                			i = random(i / 3) * 3 + 1;

                			if (pointerp(exits[i]))
            	   	   	        dir = exits[i][1];
            			    else
            			        dir = exits[i];

                            dir = " "+dir;
                        }
                    }
                }
                              
                /* Z niskim umem nie zobaczymy prowadzacego na drzewo - Rantaur */
		        if(!wildmatch("w stron�*", dir))
		            result += "Naj^swie^zsze prowadz^a prawdopodobnie"
                        + modify_dir(dir) + ".\n";

                player->increase_ss(SS_TRACKING, EXP_TROPIENIE_PRAWIE_UDANY_TRACKING);
                player->increase_ss(SS_WIS, EXP_TROPIENIE_PRAWIE_UDANY_TRACKING_WIS);
                break;
            

            case 21..50:
		        result += "Naj^swie^zsze prowadz^a" + modify_dir(dir) + ".\n";
                player->increase_ss(SS_TRACKING, EXP_TROPIENIE_UDANY_TRACKING);
                player->increase_ss(SS_WIS, EXP_TROPIENIE_UDANY_TRACKING_WIS);
                break;

            case 51..75:
                if(random(2))
                    race = ODMIANA_RASY[races[random(sizeof(races))]][0][PL_BIE];

                result += "Naj^swie^zsze zosta^ly pozostawione " +
                    "prawdopodobnie przez " + race + " i prowadz^a" + modify_dir(dir) + 
                    ".\n";
                player->increase_ss(SS_TRACKING, EXP_TROPIENIE_UDANY_TRACKING);
                player->increase_ss(SS_WIS, EXP_TROPIENIE_UDANY_TRACKING_WIS);
                break;

            case 76..150:
                result += "Najswie^zsze zosta^ly pozostawione przez " + race +
                    " i prowadz^a" + modify_dir(dir) + ".\n";
                player->increase_ss(SS_TRACKING, EXP_TROPIENIE_UDANY_TRACKING);
                player->increase_ss(SS_WIS, EXP_TROPIENIE_UDANY_TRACKING_WIS);
                break;
        }
    }
    else
    {
        player->increase_ss(SS_TRACKING, EXP_TROPIENIE_NIEUDANY_TRACKING);
        player->increase_ss(SS_WIS, EXP_TROPIENIE_NIEUDANY_TRACKING_WIS);
    }

    if (CAN_SEE_IN_ROOM(player) && track_skill > 0)
    {
        /*Info o ognisku. Vera */
        switch(query_prop(ROOM_WAS_FIRE))
        {
            case 0: break;
            case 1:
                   if(track_skill > 90)
                       result+="Zauwa�asz, �e jeszcze niedawno by�o tu "+
                               "urz�dzone malutkie ognisko.\n";
                   break;
            case 2:
                   if(track_skill > 80)
                       result+="Zauwa�asz, �e jeszcze niedawno by�o tu "+
                               "urz�dzone ma�e ognisko.\n";
                   break;
            case 3..4:
                   if(track_skill > 62)
                       result+="Zauwa�asz, �e jeszcze niedawno by�o tu "+
                               "urz�dzone niewielkie ognisko.\n";
                   break;
            case 5:
                   if(track_skill > 42)
                       result+="Zauwa�asz, �e jeszcze niedawno by�o tu "+
                               "urz�dzone do�� spore ognisko.\n";
                   break;
            case 6:
                   if(track_skill > 22)
                       result+="Zauwa�asz, �e jeszcze niedawno by�o tu "+
                               "urz�dzone spore ognisko.\n";
                   break;
            case 7..10:
                   if(track_skill > 0)
                       result+="Zauwa�asz, �e jeszcze niedawno by�o tu "+
                               "urz�dzone ogromne ognisko.\n";
                   break;
            default:
                   if(query_prop(ROOM_WAS_FIRE)>10)
                       result+="Zauwa�asz, �e jeszcze niedawno by�o tu "+
                               "urz�dzone ogromne ognisko.\n";
                   break;
        }
        /*-------------------- */
    }

    player->catch_msg(result);
    player->remove_prop(LIVE_S_EXTRA_SHORT);
    tell_roombb(environment(player), QCIMIE(player, PL_MIA) + " wstaje.\n",
                ({player}), player);
    return;
}

/**
 * Jaki� gracz rozpoczyna tropienie w tym pomieszczeniu.
 */
void
track_room()
{
    int     time,
            track_skill;
    object  paralyze;

    time = query_prop(OBJ_I_SEARCH_TIME);
    if (time < 1)
        time = 10;
    else
        time += 5;

    track_skill = this_player()->query_skill(SS_TRACKING);
    time -= track_skill/10;

    if (time < 1)
        track_now(this_player(), track_skill);
    else
    {
        set_alarm(itof(time), 0.0, &track_now(this_player(), track_skill));

        seteuid(getuid());
        paralyze = clone_object("/std/paralyze");
        paralyze->set_standard_paralyze("tropi^c");
        paralyze->set_stop_fun("stop_track");
		paralyze->set_stop_object(this_object());
        paralyze->set_stop_verb("przesta^n");
        paralyze->set_stop_message("Przestajesz szuka^c ^slad^ow.\n");
        paralyze->set_remove_time(time);
        paralyze->set_fail_message("Jeste^s zaj^et" + 
            (this_player()->query_gender() == G_FEMALE ? "a" : "y") +
            " szukaniem ^slad^ow. Wpisz 'przesta^n', je^sli chcesz zrobi^c co^s " +
            "innego.\n");
        paralyze->move(this_player(),1);
    }
 }

/**
 * Zako�czenie, lub przerwanie poszukiwania �lad�w.
 * @return Zawsze 0.
 */
varargs int
stop_track(mixed arg)
{
    if (!objectp(arg))
    {
	mixed *calls = get_all_alarms();
	mixed *args;
	int i;

	for (i = 0; i < sizeof(calls); i++)
	    if (calls[i][1] == "track_now")
	    {
		args = calls[i][4];
		if (args[0] == this_player())
		{
			remove_alarm(calls[i][0]);
		}
	    }
    }
    saybb(QCIMIE(this_player(), PL_MIA) + " przestaje szuka^c slad^ow.\n");
    this_player()->remove_prop(LIVE_S_EXTRA_SHORT);

    return 0;
}

/**
 * Definiuje komunikat dla komendy czas.
 * Funkcja ta mo�e, a nawet powinna by� maskowana stosownie
 * do kalendarza lokalnego.(Mo�na by co� pomy�le� nad opcj�
 * wyboru kalendarza kt�rym chcemy si� pos�ugiwa�, jako cz�owiek
 * ma�o prawdopodobne abym wog�le zna� elfi kalendarz..).
 * @return Pe�en komunikat b�d� 0 - je�li 0 gracz otrzyma komunikat standardowy
 */
public string
check_time()
{
    return CZAS_MUDOWY;
}

/**
 * @return Aktualna pora dnia zgodna z definicjami z <mudtime.h>
 */
public int
pora_dnia()
{
    return PORA_MUDOWA;
}

/**
 * @return Aktualna pora roku z <mudtime.h>.
 */
public int
pora_roku()
{
    int dzien, miesiac;
    string *ct;
    
    ct = explode(ctime(time(), 1), " ");
    
    dzien = atoi(ct[1]);
    miesiac = RZYM2INT(ct[2]);

    // Ju� nie trzeba zmienia� r�cznie.

    if((dzien >= 21 && miesiac == 3) || miesiac == 4 || miesiac == 5 || (dzien <= 21 && miesiac == 6))
        return MT_WIOSNA;
    else if((dzien >= 22 && miesiac == 6) || miesiac == 7 || miesiac == 8 || (dzien <= 22 && miesiac == 9))
        return MT_LATO;
    else if((dzien >= 23 && miesiac == 9) || miesiac == 10 || miesiac == 11 || (dzien <= 22 && miesiac == 12))
        return MT_JESIEN;
    else 
        return MT_ZIMA;
}

/**
 * Sprawdza czy na lokacji jest dzie� czy noc.
 * UWAGA: Lepiej u�ywa� funkcji o bardziej intuicyjnej nazwie 'jest_dzien'.
 * @return 1 je�li jest obecnie noc, 0 je�li dzie�.
 */
public int
dzien_noc()
{
    switch (PORA_MUDOWA)
    {
        case MT_WCZESNY_RANEK:
        case MT_RANEK:
        case MT_POLUDNIE:
        case MT_POPOLUDNIE:
        case MT_WIECZOR:
            return 0;
        case MT_POZNY_WIECZOR:
        case MT_NOC:
        case MT_SWIT:
            return 1;
        default:
            throw("Illegal value returned: pora_dnia() = " + pora_dnia()
                + ".\n");  
    }
}

/**
 * Sprawdza czy na lokacji jest dzie� czy noc.
 * @return 1 je�li dzie�, 0 je�li noc.
 */
public int
jest_dzien()
{
    switch (PORA_MUDOWA)
    {
        case MT_WCZESNY_RANEK:
        case MT_RANEK:
        case MT_POLUDNIE:
        case MT_POPOLUDNIE:
        case MT_WIECZOR:
            return 1;
        case MT_POZNY_WIECZOR:
        case MT_NOC:
        case MT_SWIT:
            return 0;
        default:
            throw("Illegal value returned: pora_dnia() = " + pora_dnia()
                + ".\n");    case MT_WCZESNY_RANEK:
    }
}

// --- Zmiany Kruna ---
//Funkcje do obs�ugiwania kolorowania wyj��.

/**
 * @return Zwraca kolor jaki ustawione maj� wyj�cia.
 */
int
query_exits_color()
{
    return EXITS_COLOR;
}

/**
 * @return Zwraca czy opis wyj�� pokazywany graczowi na lokacji jest standardowy czy te� nie.
 */
int
query_default_exits_description()
{
    return gDefaultExitDesc;
}
//----------------------
