/*
 * /std/room/move.c
 *x
 * This is a sub-part of /std/room.c
 *
 * It handles loading rooms and moving between rooms.
 */

static string room_dircmd;   /* Command given after the triggering verb */

/*
 * Function name: load_room
 * Description  : Finds a room object for a given array index.
 * Arguments    : int index - of the file name entry in the room_exits array.
 * Returns      : object - pointer to the room corresponding to the argument
 *                         or 0 if not found.
 */
object
load_room(int index)
{
    mixed droom;
    string err;
    object ob;

    droom = check_call(room_exits[index]);
    if (objectp(droom))
    {
        return droom;
    }

    /* Handle linkrooms that get destructed, bad wizard... baa-aad wizard. */
    if (!stringp(droom))
    {
        remove_exit(room_exits[index + 1]);
        this_player()->move_living("X", query_link_master());
        return 0;
    }

    ob = find_object(droom);
    if (objectp(ob))
    {
        return ob;
    }

    if (err = LOAD_ERR(droom))
    {
        SECURITY->log_loaderr(droom, environment(this_object()),
                              room_exits[index + 1], this_object(), err);
        write("B^l^ad we wczytywaniu:\n" + err + " <" + droom +
            ">\nKoniecznie 'zg^lo^s b^l^ad' o tym.\n");
        return 0;
    }
    return find_object(droom);
}

/*
 * Function name: query_dircmd
 * Description:   Gives the rest of the command given after move verb.
 *                This can be used in blocking functions (third arg add_exit)
 * Returns:       The movecommand as given.
 */
public string
query_dircmd()
{
    return room_dircmd;
}

/*
 * Function name: set_dircmd
 * Description:   Set the rest of the command given after move verb
 *                  This is mainly to be used from own move_living() commands
 *                  where you want a team to be able to follow their leader.
 * Arguments:     rest - The rest of the move command
 */
public void
set_dircmd(string rest)
{
    room_dircmd = rest;
}

/*
 * Function name: unq_move
 * Description  : This function is called when a player wants to move through
 *                a certain exit. VBFC is applied to the first and third
 *                argument to add_exit(). Observe that if you have a direction
 *                command that uses its trailing test as in 'enter portal'
 *                and fails if the second word is not 'portal', your block
 *                function should return 2 as you want the rest of the dir
 *                commands to be tested.
 * Arguments    : string str - the command line argument.
 * Returns      : int 1/0.
 */
public int
unq_move(string str)
{
  int index;
  int size;
  int wd;
  int tired;
  int tmp;
  int flag = TP->query_prop(PLAYER_I_IN_TEAM_GLANCE_LATER);
  object room;
  string wyjscie;
  string move_cmd = query_verb();
  if (stringp(str)) {
    move_cmd += " " + str;
  }

  TP->remove_prop(PLAYER_I_IN_TEAM_GLANCE_LATER);

  room_dircmd = str;
  index = -3;
  size = sizeof(room_exits);
  while((index += 3) < size)
  {
    wyjscie = (pointerp(room_exits[index + 1]) ? room_exits[index + 1][0]
        : room_exits[index + 1]);
    /* This is not the verb we were looking for. */
    if (!(move_cmd ~= wyjscie))
    {
      continue;
    }

    /* Check whether the player is allowed to use the exit. */
    if ((wd = check_call(room_exits[index + 2])) > 0)
    {
      if (wd > 1)
        continue;
      else
        return 1;
    }

    /* Room could not be loaded, error message is printed. */
    if (!objectp(room = load_room(index)))
    {
      return 1;
    }

    if ((this_player()->query_age() > 7200))
    {
      /* Compute the fatigue bonus. Sneaking gives double fatigue and
       * so does talking with 80% encumberance.
       */
      tired = query_tired_exit(index / 3);
      tired = (this_player()->query_prop(LIVE_I_SNEAK) ?
          (tired * 2) : tired);

		//to jest stare od przeciazenia:
      //tired = ((tmp > 80) ? (tired * 2) : tired);
		//========a to jest moje nowe, zbalansowane (vera) :============
	    //jest stopniowane!
	    switch(this_player()->query_encumberance_weight())
	    {
		case 20..39:
			tired += tired / 4 > 0 ? tired / 4 : 1; break;
		case 40..59:
			tired += tired / 3 > 3 ? tired / 3 : 3; break;
		case 60..79:
			tired += tired / 2 > 5 ? tired / 2 : 5; break;
		case 80..100:
			tired += tired     > 8 ? tired     : 8; break;
	    }
	    //==============================================================
	    //punkcik zmeczenia od nienoszenia butow
	    if(!sizeof(TP->query_slot(TS_R_FOOT)))
			tired += 1;

		//i punkciki za glod, spragnienie:
		//w procentach:
		int glod = TP->query_stuffed() * 100 / TP->query_prop(LIVE_I_MAX_EAT);
		int pragnienie = TP->query_soaked() * 100 / TP->query_prop(LIVE_I_MAX_DRINK);

		switch(pragnienie)
		{
			case 0..30:
				tired += 3; break;
			case 31..50:
				tired += 2; break;
			case 51..60:
				tired += 1; break;
			case 61..85: break;
			default:
				if(glod> 90)
					tired -= 1;
				break;
		}
		switch(glod)
		{
			case 0..30:
				tired += 2; break;
			case 31..50:
				tired += 1; break;
		}

		//i za kaca:
		int kac = TP->query_headache() * 100 / TP->query_prop(LIVE_I_MAX_INTOX);
		switch(kac)
		{
			case 30..40:
				tired += 1; break;
			case 41..60:
				tired += 2; break;
			case 61..80:
				tired += 3; break;
			case 81..100:
				tired += 4; break;
		}
		if(!tired)
			tired = 1;

      /* Player is too tired to move. */
      if (this_player()->query_fatigue() < tired)
      {
        notify_fail("Jeste^s tak zm^eczon" +
            this_player()->koncowka("y", "a") + ", ^ze nie mo^zesz " +
            "i^s^c w tym kierunku.\n");
        return 0;
      }

      this_player()->add_fatigue(-tired);
    }

    if ((wd == 0) ||
        (!transport_to(room_exits[index + 1], room, -wd)))
    {
      this_player()->im_moving(wyjscie);
      switch (this_player()->move_living(room_exits[index + 1], room, 0, flag))
      {
        case 11:
          notify_fail("Ju� si� tam nie zmie�cisz.\n");
          return 0;
      }
    }
    return 1;
  }

  /* We get here if a 'block' function stopped a move. The block function
   * should have printed a fail message.
   */
  return 0;
}

/*
 * Function name: unq_no_move
 * Description  : This function here so that people who try to walk into a
 *                'normal', but nonexistant direction get a proper fail
 *                message rather than the obnoxious "What?". Here, 'normal'
 *                exits are north, southeast, down, excetera.
 * Arguments    : string str - the command line argument.
 * Returns      : int 0 - always.
 */
public int
unq_no_move(string str)
{
    if (member_array(query_verb(), DEF_DIRS) != -1)
        notify_fail("Nie widzisz ^zadnego wyj^scia prowadz^acego na " +
            (query_verb() ~= "g^ora" ? "g^or^e" : query_verb()) + ".\n");
    return 0;
}
