/*x Zmienne */
static int event_al = 0; // alarm event'a
static mixed eventy = ({ });

/* Prototypy */
void show_event();

void
set_event_time(float czas)
{
    if (event_al)
        remove_alarm(event_al);

    event_al = set_alarm(czas, czas, show_event);
}

void add_event(string jaki, string func = "")
{
    if(jaki)
        eventy += ({ ({ jaki, func }) });
}

void show_event()
{
    int i = sizeof(eventy);

    if(i > 0)
    {
        i = random(i);
        tell_roombb(this_object(), eventy[i][0], 0, 0, 1);
        if (eventy[i][1] != "" && stringp(eventy[i][1])) 
            call_other(this_object(), eventy[i][1]);
    }
}

