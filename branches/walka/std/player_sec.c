/*
 * /std/player_sec.c
 *
 * This file is statically inherited by player_pub to ensure the
 * protection of all lower level routines.
 */

#pragma save_binary
#pragma strict_types

inherit "/std/living";

/* This order is on purpose to limit the number of prototypes necessary. */
#include "/std/player/savevars_sec.c"
#include "/std/player/quicktyper.c"
#include "/std/player/cmd_sec.c"
#include "/std/player/getmsg_sec.c"
#include "/std/player/death_sec.c"
#include "/std/player/querys_sec.c"
#include "/std/player/pcombat.c"
#include "/std/player/more.c"
#include "/std/player/czcionka.c"
#include "/std/player/zarost.c"
#include "/std/player/tatuaze.c"
//cechy postaci takie jak: kolor oczu, wiek oraz cechy budowy ciala(opcjonalnie)
#include "/std/player/cechy_wygladu.c"
//zmienne shorty graczy
#include "/std/player/zmienne_shorty.c"
//sygnaly o atakowaniu innego gracza
#include "/std/player/signal_fight.c"
//system obs�ugi gildii
#include "/std/player/gildie.c"

#include "/lib/cloth_wearer.c"

#include <files.h>
#include <macros.h>
#include <money.h>
#include <language.h>
#ifndef OWN_STATUE
#include <living_desc.h>
#endif
#include <ss_types.h>
#include <std.h>
#include <stdproperties.h>
#include <options.h>
#include <composite.h>
#include <debug.h>
#include <const.h>

#define LINKDEATH_TIME 180.0 /* three minutes */

#define DEBUG_EVAL_LIMIT	10000

/* 
 * List of properties that are to be saved in the player object. This list
 * is used at both restore and save since the name of the property itself
 * isn't stored, just the value.
 * 
 * WARNING: If you add something, add it at the end of the array. Do
 *          NOT insert anything, or all previously stored arrays will
 *          be restored in an incorrect sequence.
 */
#define SAVE_PROPS ({ CONT_I_WEIGHT, CONT_I_HEIGHT, WIZARD_I_BUSY_LEVEL, \
		      PLAYER_I_MORE_LEN, CONT_I_VOLUME, LIVE_I_LANGUAGE, \
		      PLAYER_I_SCREEN_WIDTH })
        //Niech kto� na to looknie bo wydaje mi si�, �e zapisywanie
        //player_i_more_len i player_i_screen_width jest nie bardzo konieczne
        //skoro obs�uguj� to opcje. Tylko trzeba uwa�a� przy usuni�ciu,
        //bo si� postacie gracz� zjebi�(WARNING powy�ej). (Krun)

/*
 * Global variables. They are not saved.
 */
private static int    ld_alarm;  /* The alarm used when a player linkdies. */

/*
 * Prototypes
 */
void new_init();
void load_auto_obj(string *load_arr);
void load_auto_shadow(string *load_arr);
static void init_saved_props();

/*
 * Przed zniszczeniem obiektu, sprawdza czy wywolane przez mudlib,
 * a jesli nie, to loguje w stosownym pliku.
 */
public int
remove_object()
{
    object ti;

    ti = this_interactive();

    if (ti && (file_name(ti)[0..6] != "/secure")
           && (getuid(ti) != getuid(this_object()))
           && (IS_CLONE))
        SECURITY->log_syslog("REM_PLAYER", ctime(time()) + ": UID[" +
        getuid(this_interactive()) + "], p_obj[" + 
        file_name(previous_object()) + "] -> " + 
        this_object()->query_name() + (query_ip_number(this_object()) ? "" :
            "[IDLE]") + "\n", 10000);
        
    return ::remove_object();
}


/*
 * Function name: query_def_start
 * Description  : Return the default starting location of the player type.
 *                This function is supposed to be replaced in inheriting
 *                player objects.
 */
public string
query_def_start()
{
    return DEFAULT_START;
}

/*
 * Function name: query_orig_stat
 * Description:   Return the default starting stats of a player
 *                This function is supposed to be replaced in inheriting
 *                player objects.
 */
public int *
query_orig_stat() 
{
    int i, *list;

    list = ({});
    i = -1;
    while(++i < SS_NO_STATS)
    {
	list += ({ 1 });
    }
    return list;
}

/*
 * Function name: query_orig_learn
 * Description:   Return the default starting stats of a player
 *                This function is supposed to be replaced in inheriting
 *                player objects.
 */
public int *
query_orig_learn() 
{
    int i, *list;

    list = ({});
    i = -1;
    while(++i < SS_NO_STATS) {
        list += ({ 100 / SS_NO_STATS });
    }
    return list;
}

#ifndef NO_ALIGN_TITLE
/*
 * Function name: query_new_al_title
 * Description:   Return the default starting title of a player
 *                This function is supposed to be replaced in inheriting
 *                player objects.
 */
public string
query_new_al_title()
{
    return "neutral";
}
#endif NO_ALIGN_TITLE

/*
 * Function name: fixup_screen
 * Description:   Restore the players screen width. Normally called
 *                during login.
 */
public nomask void
fixup_screen()
{
    int width = query_option(OPT_SCREEN_WIDTH);

    set_screen_width(width);
}

/*
 * Function name: add_prop_player_i_screen_width
 * Description:   Autodetect when a player's screen width is modified
 *                and notify the game driver. Do not allow VBFC values.
 * Arguments:     val: The new property value
 */
public nomask int
add_prop_player_i_screen_width(mixed val)
{
    if (!intp(val))
	return 1;
    if (val)
	set_screen_width(val);
    else
	set_screen_width(80);
    return 0;
}

/*
 * Function name: enter_game
 * Description  : Enter the player into the game.
 * Arguments    : string pl_name - the name of the player.
 *                string pwd     - the password if it was changed.
 * Returns      : int 1/0 - login succeeded/failed.
 */
public nomask int
enter_game(string pl_name, string pwd)
{
    string      fname;
    string	*souls;
    int 	il, size;
    int		lost_money;
    object 	ob;

    if ((MASTER_OB(previous_object()) != LOGIN_OBJECT) &&
	(MASTER_OB(previous_object()) != LOGIN_NEW_PLAYER) &&
	(MASTER_OB(previous_object()) != WIZ_CMD_ARCH))
    {
	write("Bad login object: " + file_name(previous_object()) + "\n");
	return 0;
    }

    set_name(pl_name);
    set_wiz_level();

    new_init();             	/* All variables to default condition */

    seteuid(0);

    if (!SECURITY->load_player())
    {
        return 0;
    }

    ustaw_imie(query_imiona());
    seteuid(query_wiz_level() ? pl_name : getuid(this_object()));
    dodaj_przym(0);			/* Set the adjectives as loaded */
    fixup_screen();

//  add_name(query_race_name());

    ustaw_odmiane_rasy(0);	/* Laduje odmiane rasy */
//    set_gender(query_gender());
    add_gender_names();
//     if (query_wiz_level())
//          dodaj_nazwy( ({ "czarodziej", "czarodzieja", "czarodziejowi",
//            "czarodzieja", "czarodziejem", "czarodzieju" }),
//           ({ "czarodzieje", "czarodzieji", "czarodziejom", 
//           "czarodzieji", "czarodziejami", "czarodziejach" }), PL_MESKI_OS);
//         Usuno�em bo po co kto� ma odrazu wiedzie�, �e�my s� czarowniki?:P (krun)

    /* Make some sanity things to guard against old and patched .o files */
    set_m_out(query_m_out());
    set_m_in(query_m_in());
    set_mm_out(query_mm_out());
    set_mm_in(query_mm_in());

    set_living_name(pl_name);
    cmd_sec_reset();
    player_save_vars_reset();

    if(query_final_death())
        return 0;

    //Po apoce zawsze w lokacji startowej
    if(query_last_logout_location() && ((time() - query_last_logout_time() < 
        LOGOUT_TIME) && SECURITY->query_start_time() < query_last_logout_time()))
    {
        object loc = find_object(query_last_logout_location());
        if(loc)
            if(!catch(move_living(0, loc)));
                write("Mi�dzy wylogowaniem a powt�rnym zalogowaniem up�yn�� zbyt kr�tki okres czasu. " +
                    "Zostajesz przeniesiony do miejsca w kt�rym si� wylogowa�e�.\n"); 
    }

    if(!environment())
    {
        if (query_temp_start_location() &&
            SECURITY->check_temp_start_loc(query_temp_start_location()) >= 0)
        {
            catch(move_living(0, query_temp_start_location()));
            set_temp_start_location(0);
        }
    }

    if (!environment())
    {
      if (!query_default_start_location() ||
          ((!query_wiz_level() && !wildmatch("*jr", pl_name)) &&
           SECURITY->check_def_start_loc(query_default_start_location()) < 0))
        set_default_start_location(query_def_start());
      if (query_ghost() && (query_default_start_location() == "/d/Standard/login/email")) {
        catch(move_living("X", clone_object("/d/Standard/login/email"), 1, 1));
      }
      else {
        catch(move_living(0, query_default_start_location()));
      }
    }

    /* Let players start even if their start location is bad */
    if (!environment())
    {
	if (catch(move_living(0, query_def_start())))
	{
        /* pr�bujemy jeszcze przenie�� do og�lnomudowej lokacji startowej */
        if (catch(move_living(0, "/d/Standard/Redania/Rinde/Polnoc/Karczma/lokacje/start")))
        {
	    /* If this start location is corrupt too, destruct the player */
            write("Twoja lokacja startowa jest zepsuta, nie mozesz teraz polaczyc sie z mudem. " + 
                "skontaktuj sie z czarodziejami Vatt'gherna za posrednictwem forum lub tez "+
                "piszac wiadomosc na adres: administracja@vattghern.pl.\n");
            destruct();
    	}
    }}

    /* Restore the bits */
    unpack_bits();

    /* Restore the saved properties and add a default one. */
    add_prop(PLAYER_I_MORE_LEN, 20);
    init_saved_props();

    init_zarost();
    init_tatuaze();
    //init_shorty();
    is_still_drunk();

    /* Non wizards should not have a lot of command souls */
    if (!query_wiz_level())
    {
        souls = query_cmdsoul_list();
        foreach (string soul : souls)
        remove_cmdsoul(soul);
    }

    if (!m_alias_list)
    {
        m_alias_list = ([]);
    }

    if (query_autoshadow_list())
        load_auto_shadow(query_autoshadow_list());

    acc_exp_to_stats();   /* Setup our current stats */
    acc_exp_to_skills();  /* Ustawia bie��ce warto�ci umiej�tno�ci */

    query_combat_object()->cb_configure();

    if (query_auto_load())
        load_auto_obj(query_auto_load());

    /* Tell the player when he was last logged in and from which site. */
    write("Ostatnie logowanie: " + ctime(query_login_time(), 1) +
        "\n           z hosta: " + query_login_from() + "\n");

    /* Start him up */
    this_object()->start_player();

    /* start zmniejszania skilli */
    start_ss_decay();

    /* Do this after startup, so we can use the address and time at startup. */
    set_login_time();
    set_login_from();

    /* Non wizards should not have a lot of tool souls */
    if (!query_wiz_level())
    {
        souls = query_tool_list();
        foreach (string soul : souls)
        this_object()->remove_toolsoul(soul);
    }

    /* Sprawdzenie czy gracz jest jeszcze we wszystkich gildiach,
       kt�re ma zapisane. (krun)*/
    sprawdz_gildie();

    /* Dodanie soula gildiowego(musi by� bo nie wiem jakim cudem soul
       sobie znika...) */
    foreach(string name : m_indexes(silniki_gildii))
        TO->add_cmdsoul((silniki_gildii[name])->query_plik_soula()); 

    /* If a password was changed, set it. */
    if (strlen(pwd))
    {
        set_password(pwd);
        save_me();
    }

    //----info dla wiz�w o multilogu + wpis do log�w!------------

    string login_name = TO->query_real_name();
    string login_ip = query_ip_number(TO);
    string login_email = TO->query_mailaddr();
    foreach(mixed u : users())
    {
        if(!interactive(u))
            continue;

        string name = u->query_real_name();

        //Mo�na si� r�wnocze�nie logowa� i by� zalogowanym.. To nie przest�pstwo:P
        if(name == "logon")
            continue;
        //Pozatym(poprawcie mnie jesli �le m�wie(usuncie poni�sze 2 ify)) posta� i konto
        //mog� by� r�wnocze�nie zalogowane.(krun)
        if(sizeof(explode(name, "@")) > 1 && sizeof(explode(login_name, "@")) < 2)
            continue;
        if(sizeof(explode(name, "@")) < 2 && sizeof(explode(login_name, "@")) > 1)
            continue;
        if(name ~= login_name)
            continue;
        string ip_number = query_ip_number(u);
        string email = u->query_mailaddr();
        if(login_ip == ip_number && !u->query_wiz_level() && !TO->query_wiz_level())
        {
            if(login_email == email)
            {
                SECURITY->log_multi("Multilog z tego samego konta postaci", ({name, login_name}));
                notify_wizards(TO, "MULTILOG! Z TEGO SAMEGO KONTA!\n");
            }
            else
            {
                SECURITY->log_multi("Multilog z tego samego adresu ip postaci", ({name, login_name}));
                notify_wizards(TO, "MULTILOG!\n");
            }
        }
    }

    //------koniec info dla wizow o multi--------

    //Usuwamy soula �piewania, kt�ry zosta� przeniesiony do communication.
    TP->remove_cmdsoul("/cmd/live/singing");
    TP->remove_cmdsoul("/cmd/live/singing.c");

    return 1;
}

/*
 * Function name: init_saved_props
 * Description  : Add the saved properties to the player.
 */
static void
init_saved_props()
{
    int index = -1;
    int size = ((sizeof(SAVE_PROPS) < sizeof(saved_props)) ?
	sizeof(SAVE_PROPS) : sizeof(saved_props));

    while(++index < size)
    {
	if (saved_props[index])
	{
	    add_prop(SAVE_PROPS[index], saved_props[index]);
	}
	else
	{
	    remove_prop(SAVE_PROPS[index]);
	}
    }

    saved_props = 0;
}

/*
 * Function name: open_player
 * Description  : This function may only be called by SECURITY or by the
 *                login object to reset the euid of this object.
 */
public nomask void
open_player()
{
    if ((previous_object() == find_object(SECURITY)) ||
	(MASTER_OB(previous_object()) == LOGIN_OBJECT))
    {
	seteuid(0);
    }
}

/*
 * Function name: fix_saveprops_list
 * Description  : Before the player is saved, this function is called to
 *                store several properties into an array that will be
 *                saved in the player file.
 */
nomask public int
fix_saveprop_list()
{
    /* Fix the saved_props list before save */
    saved_props = ({ });
    foreach (string prop : SAVE_PROPS)
	saved_props += ({ query_prop(prop) });
}

/*
 * Function name: save_player
 * Description  : This function actually saves the player object.
 * Arguments    : string pl_name - the name of the player
 * Returns      : int 1/0 - success/failure.
 */
nomask public int
save_player(string pl_name)
{
    if (!pl_name)
	return 0;

    pack_bits();
    seteuid(getuid(this_object()));
    save_object(PLAYER_FILE(pl_name));
    seteuid(getuid(this_object()));

    /* Discard the props again */
    saved_props = 0;
    return 1;
}

/*
 * Function name: load_player
 * Description  : This function actually loads the player file into the
 *                player object.
 * Arguments    : string pl_name - the name of the player.
 * Returns      : int 1/0 - success/failure.
 */
nomask public int
load_player(string pl_name)
{
    int ret;
   
    if (!pl_name)
	return 0;

    seteuid(getuid(this_object()));
    ret = restore_object(PLAYER_FILE(pl_name));
    seteuid(0);
    this_object()->set_option(OPT_MCCP2, this_object()->query_option(OPT_MCCP2));
    this_object()->set_option(OPT_MSP, this_object()->query_option(OPT_MSP));
    return ret;
}

/*
 * Function name: actual_linkdeath
 * Description  : This function is called when the player actually linkdies.
 *                If the player is in combat, this will be delayed, or else
 *                it is called directly.
 */
static nomask void
actual_linkdeath()
{
#ifdef STATUE_WHEN_LINKDEAD
#ifdef OWN_STATUE
    OWN_STATUE->die(this_object());
#else   
    tell_roombb(environment(), LD_STATUE_TURN(this_object()), ({}),
                this_object());
#endif OWN_STATUE
#endif STATUE_WHEN_LINKDEAD

    /* People should not autosave while they are linkdead. */
    stop_autosave();


    remove_alarm(ld_alarm);
    ld_alarm = 0;
    set_linkdead(1);
}

/*
 * Function name: linkdie
 * Description  : When a player linkdies, this function is called.
 */
nomask public void
linkdie()
{
    if (previous_object() != find_object(SECURITY))
    {
	return;
    }

    if (objectp(query_attack()))
    {
	tell_room(environment(), QCIMIE(this_object(), PL_MIA) +
	    " traci kontakt z rzeczywisto^sci^a.\n", ({ this_object() }) );

	ld_alarm = set_alarm(LINKDEATH_TIME, 0.0, actual_linkdeath);
    }
    else
    {
	actual_linkdeath();
    }
}

/*
 * Function name: query_linkdead_in_combat
 * Description  : This function returns true if the player is linkdead,
 *                but still in combat.
 * Returns      : int 1/0 - in combat while linkdead or not.
 */
nomask public int
query_linkdead_in_combat()
{
    return (ld_alarm != 0);
}

/*
 * Function name: revive
 * Description  : When a player revives from linkdeath, this function is
 *                called.
 */
nomask public void
revive()
{
    object default_death;

    if (MASTER_OB(previous_object()) != LOGIN_OBJECT)
    {
	return;
    }

    /* If the player is not in combat, revive him. Else, just give a
     * a message about the fact that the player reconnected.
     */
    if (!ld_alarm)
    {
	set_linkdead(0);

#ifdef OWN_STATUE
	OWN_STATUE->revive(this_object());
#else	
	tell_roombb(environment(), QCIMIE(this_object(), PL_MIA) + " " +
	    STATUE_TURNS_ALIVE + ".\n", ({this_object()}), this_object());
#endif OWN_STATUE

	/* We reset these variables so the player does not gain mana or
	 * hitpoints while in LD.
	 */
	player_save_vars_reset();
	save_vars_reset();

	/* Start autosaving again. */
	start_autosave();
	
	/* Jesli gracz zginal w momencie straty polaczenia,
	 * przepuszczamy go przez sekwencje smierci.
	 */
	if (query_ghost())
	{
	    default_death = present("_default_death_", this_object());
	    if (!default_death)
		this_object()->death_sequence();
	    else
		default_death->init();
	}
    }
    else
    {
	tell_roombb(environment(), QCIMIE(this_object(), PL_MIA) +
	    " odzyskuje kontakt z rzeczywistosci^a.\n", ({this_object()}),
	    this_object());

	remove_alarm(ld_alarm);
	ld_alarm = 0;
    }

    this_object()->set_option(OPT_MCCP2, this_object()->query_option(OPT_MCCP2));
    this_object()->set_option(OPT_MSP, this_object()->query_option(OPT_MSP));
}

/*
 * Function name: load_auto_obj
 * Description  : Loads all autoloaded objects
 * Arguments    : string *load_arr - the array of objects to load.
 */
nomask static void
load_auto_obj(string *load_arr)
{
    string file, argument, *parts;
    object ob;
    int	   il, size;

    if (!pointerp(load_arr) ||
	!sizeof(load_arr))
    {
	return;
    }

    il = -1;
    size = sizeof(load_arr);
    while (++il < size)
    {
	if (!stringp(load_arr[il]))
	{
	    write("Autoload array element " + il + " corrupt.\n");
	    continue;
	}

	if (sscanf(load_arr[il], "%s:%s", file, argument) != 2)
	{
	    file = load_arr[il];
	    argument = 0;
	}
	if (!stringp(file))
	{
	    write("Auto load string corrupt: " + load_arr[il] + "\n");
	    continue;
	}
	if (LOAD_ERR(file))
	    continue;
	if (!objectp(find_object(file)))
	    continue;
	catch(ob = clone_object(file));

	/* Note that we don't check for strlen() since we also want to call
	 * init_arg() if the format is 'filename:'.
	 */
	if (stringp(argument))
	{
	    if (catch(ob->init_arg(argument)))
	    {
		catch(ob->remove_object());
		continue;
	    }
	}
	catch(ob->move(this_object(), 1));
    }
}

/*
 * Function name: load_auto_shadow
 * Description  : Startup all the shadows that should shadow this player.
 * Arguments    : string *load_arr - the array of shadows to add.
 */
nomask static void
load_auto_shadow(string *load_arr)
{
    string file, argument;
    object ob;
    int	   il, size;

    if (!load_arr || !sizeof(load_arr))
	return;

    il = -1;
    size = sizeof(load_arr);
    while(++il < size)
    {
	if (sscanf(load_arr[il], "%s:%s", file, argument) != 2)
	{
	    write("Shadow load string corrupt: " + load_arr[il] + "\n");
	    continue;
	}
	if (LOAD_ERR(file))
	    continue;
	ob = find_object(file);
	if (!ob)
	    continue;
	ob = clone_object(file);
	if (argument)
	    ob->autoload_shadow(argument);
	else
	    ob->autoload_shadow(0);
    }
}

/*
 * Function name: new_save
 * Description  : This function is called to save the player initially.
 *                It is only called when new a player enters the game. It
 *                makes it possible to initialize variables using the
 *                standard set_ calls.
 * Arguments    : string pl_name - the name of the player.
 *                string pwd     - the (encrypted) password of the player.
 *                string pfile   - the player save file.
 * Returns      : int 1/0 - success/failure.
 */
public nomask int
new_save(string pl_name, string pwd, string pfile)
{
    if (!CALL_BY(LOGIN_NEW_PLAYER))
    {
	return 0;
    }

    write("Tworzymy now^a posta^c: " + pl_name + "\n");
    seteuid(getuid(this_object()));
    set_name(pl_name);
    set_password(pwd);
    set_player_file(pfile);
    new_init();			/* Initialize all variables on startup */

    save_object(PLAYER_FILE(pl_name));
    return 1;
}

/*
 * Function:     new_init
 * Description:  Initialises all variables to default conditions.
 */
static nomask void
new_init()
{
    int i;
    int *ostat;

    ostat = query_orig_stat();

    i = -1;
    while(++i < SS_NO_STATS)
    {
        set_base_stat(i, ostat[i]);
    }

    stats_to_acc_exp();

#ifndef NO_ALIGN_TITLE
    set_al_title(query_new_al_title());
#endif NO_ALIGN_TITLE
}

/*
 * Function name: create_living
 * Description  : Called to create the player. It initializes some variables.
 */
public nomask void
create_living()
{
    player_save_vars_reset();
}

/*
 * Function name: reset_living
 * Description  : We don't want people to mask this function.
 */
public nomask void
reset_living()
{
    return;
}

/*
 * Function name:	command
 * Description:		Makes the player execute a command, If the player is a
 *			wizard then the object must have the same effective
 *			userid as the wizard being forced.
 * Arguments:		cmd: String containing the command
 * Returns:		eval_cost or '0' if unsuccessful
 */
public nomask int
command(string cmd)
{
/*    if (query_wiz_level() &&
	objectp(previous_object()))
    {
        if (!SECURITY->wiz_force_check(geteuid(previous_object()),
	    geteuid()))
	{
	    return 0;
	}
    }*/

    return ::command(cmd);
}

/*
 * Function name: id
 * Description  : Returns whether this object can be identified by a certain
 *                name. That isn't the case if the player hasn't met you
 *                while the real name is used.
 * Arguments    : string str - the name to test
 * Returns      : int 1/0 - true if the name is valid.
 */
public int
id(string str)
{
    if ((str == query_real_name()) &&
	notmet_me(previous_object()))
    {
	return 0;
    }

    return ::id(str);
}

string
show_subloc(string subloc, object on_obj, object for_obj)
{
    switch (subloc)
    {
	case "fryzura_na_glowie":
		if (query_prop(TEMP_SUBLOC_SHOW_ONLY_THINGS))
			return "";
		return this_object()->opis_wlosow(for_obj);
	case "subloc_zarost":
		if (query_prop(TEMP_SUBLOC_SHOW_ONLY_THINGS))
			return "";
		return this_object()->opis_zarostu(for_obj);
	case "subloc_tatuaze":
		if (query_prop(TEMP_SUBLOC_SHOW_ONLY_THINGS))
			return "";
		return this_object()->opis_tatuazy(for_obj);
    case "kwiaty_we_wlosach":
        return this_object()->opis_kwiatow(for_obj);
	default:
		return ::show_subloc(subloc, on_obj, for_obj);
    }
}
