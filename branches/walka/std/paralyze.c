/**
 *
 * \file /std/paralyze.c
 *
 * Clone and move this object to a player if you want to paralyze him.
 *
 * Gdy minie czas parali�u wywo�ywana jest w obiekcie finish_object 
 * funkcja finish_fun. Gdy parali� zostanie przerwany komend� 
 * wywo�ana jest stop_fun w stop_object.  - Molder.
 *
 * Eventy - dodajemy je w funkcji init_events() za pomoca funkcji
 *          add_event (bo wtedy mozna uzywac QCIMIE(VBFC) i innych
 *          fajnych rzeczy(tagi, etc) ;)
 */
#pragma strict_types
#include <stdproperties.h>

inherit "/std/object";

/*
 * Variables
 */
static string	stop_verb,	    /* What verb to stop this paralyze ? */
		        stop_fun,	    /* What function to call when stopped */
		        finish_fun,
                event_fun,
		        fail_message,   /* Message to write when command failed */
		        stop_message,	/* Message to write when paralyze stopped */
		        finish_message;

static int	    remove_time,	/* Shall it go away automatically? */
			    event_time,
                stopping;

static object	stop_object,	/* Object to call stop_fun in when stopped */
		        finish_object,
                event_object;

static mixed    stop_fun_args,     /* Argumenty do wywo�ania przez stop_funa */
                finish_fun_args;   /* Argumenty do wywo�ania przez funish_funa */           

static string*  events = ({ });

/* Niekt�re komendy s� zawsze dozwolone i nie ma sposobu na ich usuni�cie. */
#define ALLOWED ({"zg�o�", "przyzwij", "odpowiedz", "wie�ci"})
#define SPECIAL_ALLOWED ({"?"})

static string *allowed = ALLOWED + ({ "zako�cz", "kto", "stan", "cechy",
            "um", "u�miech", "umiej�tno�ci", "k", "kondycja", "sp",
            "sp�jrz", "ob", "objerzyj", "zerknij", "u�miechnij", "za�piewaj", 
            "pomoc", "pomocy","powiedz","krzyknij", "exec", "ski�", "pokiwaj", "pokr��",
            "skrzyw", "Dump", "Call", "alias", "i", "inwentarz"}); 
            /* Doda�em exec na wypadek gdyby jakiemu� wizowi co� nie wysz�o..
             * Zawsze mo�na si� t� komend� uratowa�. */

static string *special_allowed = SPECIAL_ALLOWED + ({"'"});

/*
 * Prototypes
 */
varargs void add_event(string str1, string str2=0);
void show_event();
void set_standard_paralyze(string str);
int stop(string str);
varargs void stop_paralyze();


/**
 * Ustawia jako dost�pne tylko standardowe komendy, kt�rych nie mo�na usun��
 * z allowed.
 */
void
standard_allowed()
{
    allowed = ALLOWED;
    special_allowed = SPECIAL_ALLOWED;
}

/**
 * Dodaje komend� do komend mo�liwych do wykonania dla gracza maj�cego
 * na sobie parali�.
 *
 * @param str nazwa komendy
 * @param flag czy jest to komenda, po kt�rej nie nast�puje znak spacji
 */
void
add_allowed(string str, int flag=0)
{
    if(flag)
    {
        if(member_array(str, special_allowed) == -1)
            special_allowed += ({str});
    }
    else
        if(member_array(str, allowed) == -1)
            allowed += ({str});
}

/**
 * Usuwa komend�, kt�ra gracz posiadaj�cy parali� moze wykona�
 *
 * @param str nazwa komendy
 * @param flag czy jest to komenda, po kt�rej nie nast�puje znak spacji
 */
void
remove_allowed(string str, int flag=0)
{
    if(flag)
    {
        if(member_array(str, SPECIAL_ALLOWED) == -1)
            special_allowed -= ({str});
    }
    else
        if(member_array(str, ALLOWED) == -1)
            allowed -= ({str});
}

/**
 * Tworzy i konfiguruje paraliz.
 */
void
create_paralyze()
{
    set_standard_paralyze("paralyze");
}

void init_events()
{
}

/*
 * Function name: create_object
 * Description:   The standard create routine.
 */
nomask void
create_object()
{
    add_prop(OBJ_M_NO_DROP, 1);
    add_prop(OBJ_M_NO_STEAL,1);
    add_prop(OBJ_M_NO_SELL, 1);
    add_prop(OBJ_M_NO_GIVE, 1);

    create_paralyze();

    set_no_show();
}

/*
 * Function name: init
 * Description:   Called when meeting an object
 */
void
init()
{
    if (remove_time)
	   set_alarm(itof(remove_time), 0.0, stop_paralyze);
  
    if(sizeof(events) > 0 || strlen(event_fun))
	   set_alarm(itof(event_time), 0.0, show_event);
    
    add_action(stop, "", 1);
}

/*
 * Nazwa        : stop
 * Description : Here all commands the player gives comes.
 * Argument   : string str - The command line argument.
 * Returns      : int 1/0    - komenda gracza zostanie/nie zostanie 
 *                                      sparali�owana.
 */
int
stop(string str)
{
    if ((!str) && (query_verb() == "test_na_obecnosc_paralizu"))
        return 1;

    /* parali�ujemy tylko tych kt�rzy maj� parali� w inventory */
    if (environment() != this_player())
        return 0;

    /* je�li komenda jest w allowed to nie parali�ujemy jej */
    if(member_array(query_verb(), allowed) >= 0)
        return 0;

    /* je�li komenda jest w special_allowed to nie parali�ujemy �adnej komendy
       zaczynaj�cej si� od stringa identycznego jak string w special_allowed
       (specjalna tablica do fukcji takich jak: "'", "?"*/
    foreach(string x : special_allowed)
    {
        //Musimy wykluczy� znaki, kt�re wildmatch traktuje jako dowolne
        if(x == "?" && query_verb()[0] != '?')
            continue; 
        if(x == "*" && query_verb()[0] != '*')
            continue;
        if(wildmatch(x + "*", query_verb()))
            return 0;
    }

    /* je�li jest komenda przerywaj�ca parali�, sprawdzamy j� */
    if (stringp(stop_verb) && (query_verb() ~= stop_verb))
    {
        /* Je�li stop_fun jest zdefiniowana MUSI zwr�ci� 1 aby
            parali� nie zadzia�a� */
        if (objectp(stop_object) && call_other(stop_object, stop_fun, ({str}) + stop_fun_args))
            return 1;

        stopping = 1;
        set_alarm(0.1, 0.0, remove_object);

        if (stringp(stop_message))
            this_player()->catch_msg(stop_message);

        return 1;
    }

    /* We allow VBFC, so here we may use catch_msg(). */
    if (stringp(fail_message))
        this_player()->catch_msg(fail_message);

    return 1;
}

/*
 * Function name: dispel_magic
 * Description  : If this paralyze should be able to lift magical, define this
 *		  function.
 * Arguments    : int i   - a number indicating how strong the dispel spell is.
 * Returns      : int 1/0 - 1 if dispelled; otherwise 0.
 */
int
dispel_magic(int i)
{
    return 0;
}

/*
 * Function name: set_stop_verb
 * Description  : Set the verb to stop paralyze, if possible.
 * Arguments    : string verb - the verb to use.
 */
void
set_stop_verb(string verb)
{
    stop_verb = verb;
}

/*
 * Function name: query_stop_verb
 * Description  : Return the stopping verb.
 * Returns      : string - the verb.
 */
string
query_stop_verb()
{
    return stop_verb;
}

/**
 * Dzi�ki tej funkcji mo�emy ustawi� funkcje, kt�ra b�dzie wywo�ywana kiedy parali� zostanie
 * przerwany/zako�czony.
 * @param fun nazwa funkcji.
 * @param ... argumenty do funkcji
 */
varargs void
set_stop_fun(string fun, ...)
{
    stop_fun = fun;
    stop_fun_args = argv;
}

/*
 * Function name: query_stop_fun
 * Description  : Returns the function to call when paralyze stops.
 * Returns      : string - the function name.
 */
string
query_stop_fun()
{
    return stop_fun;
}

varargs void
set_finish_fun(string fun, ...)
{
    finish_fun = fun;
    finish_fun_args = argv;
}

string
query_finish_fun()
{
    return finish_fun;
}

mixed
query_finish_fun_args()
{
    return finish_fun_args;
}

/*
 * Function name: set_stop_object.
 * Description  : Set which object to call the stop function in.
 * Arguments    : object ob - the object.
 */
void
set_stop_object(object ob)
{
    stop_object = ob;
}

/*
 * Function name: query_stop_object
 * Description  : Returns which object to call the stop function in.
 * Returns      : object - the object.
 */
object
query_stop_object()
{
    return stop_object;
}


void
set_finish_object(object ob)
{
    finish_object = ob;
}

object
query_finish_object()
{
    return finish_object;
}

/*
 * Function name: set_fail_message
 * Description  : Set the fail message when player tries to do something.
 *                This supports VBFC and uses this_player().
 * Arguments    : string - the fail message.
 */
void
set_fail_message(string message)
{
    fail_message = message;
}

/*
 * Function name: query_fail_message
 * Description  : Returns the fail message when player tries to do something.
 *                This returns the real value, not resolved for VBFC.
 * Returns      : string - the message.
 */
string
query_fail_message()
{
    return fail_message;
}

/*
 * Function name: set_remove_time
 * Description  : Set how long time player should be paralyzed (in seconds).
 * Arguments    : int time - the time to set.
 */
void
set_remove_time(int time)
{
    remove_time = time;
}

/*
 * Function name: query_remove_time
 * Description  : Returns the paralyze time (in seconds).
 * Returns      : int - the time.
 */
int
query_remove_time()
{
    return remove_time;
}

/*
 * Function name: set_stop_message
 * Description  : Set the message written when paralyze stops. This may
 *                support VBFC and uses this_player().
 * Arguments    : string - the message.
 */
void
set_stop_message(string message)
{
    stop_message = message;
}

/*
 * Function name: query_stop_message
 * Description  : Returns the message written when paralyze stops. It returns
 *                the real value, not solved for VBFC.
 * Returns      : string - the message.
 */
string
query_stop_message()
{
    return stop_message;
}

void
set_finish_message(string message)
{
    finish_message = message;
}

string
query_finish_message()
{
    return finish_message;
}

void
set_event_fun(string str)
{
    event_fun = str;
}

string
query_event_fun()
{
    return event_fun;
}

void
set_event_object(object ob)
{
    event_object = ob;
}

object
query_event_object()
{
    return event_object;
}

/*
 * Function name: set_standard_paralyze
 * Description  : Set up standard settings for a paralyze.
 * Arguments    : string str - When the player uses the stop-verb, 'stop',
 *                             the message 'You stop <str>.\n' is printed
 *                             to the player.
 */
void
set_standard_paralyze(string str)
{
    set_stop_verb("przesta�");
/*
    set_stop_fun("stop_paralyze");
    set_stop_object(previous_object());
    
    Wywoluje runtima too deep recursion, bowiem stop_fun jest wywolywane
    ze stop_paralyze(), a tamtem wywoluje samego siebie - o ile
    set_stop_paralyze() jest wywolywane z samego siebie, z funkcji
    create_paralyze(). 
 */
    set_stop_message("Przestajesz " + str + ".\n");
    set_fail_message("Jeste� teraz zaj�t"+this_player()->koncowka("y","a")+
                    " czym innym. Musisz wpierw " +
		"przesta� by m�c zrobi�, to co chcesz.\n");
}

/**
 * Funkcja wywo�ana gdy minie czas trwania parali�u.
 */
void
stop_paralyze()
{
    if (!objectp(environment()))
    {
    	remove_object();
    	return;
    }

    if (stopping)
        return;

    set_this_player(environment());

    if (objectp(finish_object) && stringp(finish_fun))
    {
	try {
    	    call_otherv(finish_object, finish_fun, ({environment()}) + finish_fun_args);
	} catch (mixed x) {
	    filter(users(), &->query_wiz_level())->catch_msg(x);
	}
    }
    else if (strlen(finish_message))
        environment()->catch_msg(finish_message);
    
    remove_object();
}

/*
 * Function name: stat_object
 * Description  : Function called when wiz tries to stat this object.
 * Returns      : string - the extra information to print.
 */
string
stat_object()
{
    string str = ::stat_object();

    if (strlen(stop_verb))
    {
	str += "Stop verb: " + stop_verb + "\n";
    }
    if (strlen(stop_fun))
    {
	str += "Stop fun:  " + stop_fun + "\n";
    }
    if (strlen(stop_message))
    {
	str += "Stop mess: " + stop_message + "\n";
    }
    if (strlen(finish_fun))
    {
	str += "Finish fun:  " + finish_fun + "\n";
    }
    if (strlen(finish_message))
    {
	str += "Finish mess: " + finish_message + "\n";
    }
    if (strlen(fail_message))
    {
  	str += "Fail mess: " + fail_message + "\n";
    }
    if (remove_time)
    {
	str += "Duration:  " + remove_time + "\n";
    }
    if (objectp(stop_object))
    {
	str += "Stop obj:  " + file_name(stop_object) + "\n";
    }
    if (objectp(finish_object))
    {
	str += "Stop obj:  " + file_name(finish_object) + "\n";
    }

    return str;
}

/*
 * Function name: add_event
 * Description  : Dodaje event do paralizu.
 * Argumnets  : str1 - event dla sparalizowanego gracza
 *              str2 - nieobowiazkowy event dla reszty graczy 
 *                     na lokacji, domyslnie niewyswietlany
 */

varargs void add_event(string str1, string str2=0)
{
	events += ({ ({str1, str2}) });
}

void set_event_time(int i)
{
	event_time = i;
}

int query_event_time()
{
	return event_time;
}

void show_event()
{	 
	 if(!interactive(environment()))
		return;
	
    /* Jesli zdefiniowalismy funkcje eventowa,
     * to eventy z tablicy zostaja olane */
    if(strlen(event_fun) && objectp(event_object))
    {
        call_other(event_object, event_fun);
    }
    else
    {
	    int index = random(sizeof(events));
	
	    environment()->catch_msg(events[index][0]+"\n");
	
	    if(events[index][1] != 0)
        {
            object prev_player = this_player();
            set_this_player(environment());
		    saybb(process_tags(events[index][1])+"\n", environment());
            set_this_player(prev_player);
        }
    }
	
	set_alarm(itof(event_time+random(4)), 0.0, show_event);
}

/**
 * Wy��czamy auto_�adowanie parali�u.
 */
string
query_auto_load()
{
       return 0;
}
