/**
 * Standard zio�a
 * (c) Krun 2007
 * 
 * TODO:
 * - Trzeba przepisa� od nowa rodzaje, w chwili obecnej nie dzia�a to zbyt dobrze (prawdopodobnie poprawione)
 * - Trzeba dopisa� w zbieraniu uwzgl�dnianie poszczeg�lnych cz�ci zio�a
 *   i komend do nich, ale r�wnocze�nie nie rozbija� tego w opisie.
 * - Doda� shorty i usun�� nazwy g��wne z rosn�cych
 * - Trzeba wszystkie opisy sprawdzi� bo nie mam zielonego poj�cia dlaczego,
 *   ale opisy si� dziwnie zachowuj�(zdarza si�, �e s� bez przymiotnik�w.. itp) (prawdopodobnie done)
 */

inherit "/std/object.c";


#include <herb.h>
#include <files.h>
#include <formulas.h>
#include <ss_types.h>
#include <materialy.h>
#include <cmdparse.h>
#include <object_types.h>

int      decay_alarm,                 //Id alarmu schni�cia
         decay_stopped;               //Zapami�tuje czy schni�cie jest zatrzymane

#define DEBUGGER   "krun" /* Osoba kt�ra b�dzie informowana o b��dach */
#define DBG(x)     find_player(DEBUGGER)->catch_msg("HERB DEBUUGER:" + x + "\n")
#define DBG_EFF(x) DBG(TP->query_real_name() + " " + x)

//Predefinicje funkcji

public int query_rosnie();
public int moze_znalezc(object pl);
public int moze_zidentyfikowac(object pl);
private static object find_previous_living();
int start_decay();
int stop_decay();

#include "herb/properties.c"
#include "herb/description.c"

//Redeklaracje parse_command*

/**
 * Funkcja pomocnicza, okre�laj�ca najbli�szego livinga, dla kt�rego prawdopodobnie
 * ma by� opis, nie dam g�owy, �e zawsze to ten living, ale nie mam zielonego
 * poj�cia jak inaczej co� takiego sprawdzi�.
 * @return pierwszego livinga z previous_object
 */
static private object
find_previous_living()
{
    if(TP)
        return TP;

    int i;
    for(i=0;;i++)
    {
        if(living(previous_object(-i)))
            return previous_object(-i);
        if(!previous_object(-i))
            return PO;
    }
}

//FIXME: Doda� shorty i ng.
public string *
parse_command_id_list(int przyp)
{
    int i=0, mz;

    mz = moze_zidentyfikowac(find_previous_living());
    //FIXME: DODA� SHORTY + USUN�� NAZWY Z ROSNACYCH.
    if(rosnie && mz && sizeof(id_pnazwa_cal))
        return map(id_pnazwa_cal, &operator([])(,przyp)) + ::parse_command_id_list(przyp);
    else if(rosnie && sizeof(unid_pnazwa_cal))
        return map(unid_pnazwa_cal, &operator([])(,przyp)) + ::parse_command_id_list(przyp);
    else if(mz && sizeof(pnazwa_zident))
        return map(pnazwa_zident, &operator([])(,przyp)) + ::parse_command_id_list(przyp);
    else
        return ::parse_command_id_list(przyp);
}

public string *
parse_command_plural_id_list(int przyp)
{
    int i=0, mz;
    object fo = find_previous_living();

    mz = moze_zidentyfikowac(find_previous_living());

    if(query_tylko_mn(fo))
        return parse_command_id_list(przyp);

    if(mz && rosnie && sizeof(id_pnazwa_cal))
        return map(id_mnazwa_cal, &operator([])(,przyp)) + (sizeof(id_mshort_cal) ? ({id_mshort_cal[przyp]}) : ({})) + ::parse_command_plural_id_list(przyp);
    else if(rosnie && sizeof(unid_pnazwa_cal))
        return map(unid_mnazwa_cal, &operator([])(, przyp)) + (sizeof(unid_mshort_cal) ? ({unid_mshort_cal[przyp]}) : ({})) + ::parse_command_plural_id_list(przyp);
    else if(mz && sizeof(pnazwa_zident))
        return map(mnazwa_zident, &operator([])(, przyp)) + (sizeof(mshort_zident) ? ({mshort_zident[przyp]}) : ({})) + ::parse_command_plural_id_list(przyp);
    else
        return ::parse_command_plural_id_list(przyp);
}

public int *
parse_command_rodzaj_id_list(int przyp)
{
    int i=0, mz;

    mz = moze_zidentyfikowac(find_previous_living());

    if(mz && rosnie && sizeof(id_pnazwa_cal))
        return id_rodz_cal + ::parse_command_rodzaj_id_list(przyp);
    else if(rosnie && sizeof(unid_pnazwa_cal))
        return unid_rodz_cal + ::parse_command_rodzaj_id_list(przyp);
    else if(mz && sizeof(pnazwa_zident))
        return rodz_zident + ::parse_command_rodzaj_id_list(przyp);
    else
        return ::parse_command_rodzaj_id_list(przyp);
}

public int *
parse_command_plural_rodzaj_id_list(int przyp)
{
    int mz;

    mz = moze_zidentyfikowac(find_previous_living());

    if(mz && rosnie && sizeof(id_pnazwa_cal))
        return id_rodz_cal + ::parse_command_plural_rodzaj_id_list(przyp);
    else if(rosnie && sizeof(unid_pnazwa_cal))
        return unid_rodz_cal + ::parse_command_plural_rodzaj_id_list(przyp);
    else if(mz && sizeof(pnazwa_zident))
        return rodz_zident + ::parse_command_plural_rodzaj_id_list(przyp);
    else
        return ::parse_command_plural_rodzaj_id_list(przyp);
}

public string *
parse_command_adjectiv1_id_list()
{
    int mz;

    mz = moze_zidentyfikowac(find_previous_living());

    if(rosnie && mz && sizeof(id_pnazwa_cal))
    {
        return ((decayed && sizeof(decay_adj) == 2 && decay_adj[0] && decay_adj[1]) ? ({decay_adj[0]}) : ({})) + 
            ((sizeof(id_przym_cal) == 2 && id_przym_cal[0] && id_przym_cal[1]) ? id_przym_cal[0] : ({}));
    }
    else if(sizeof(unid_pnazwa_cal) && rosnie)
    {
        return ((decayed && sizeof(decay_adj) == 2 && decay_adj[0] && decay_adj[1]) ? ({decay_adj[0]}) : ({})) + 
            ((sizeof(unid_przym_cal) == 2 && unid_przym_cal[0] && unid_przym_cal[1]) ? unid_przym_cal[0] : ({}));
    }
    else if(mz && sizeof(pnazwa_zident))
    {
        return ((decayed && sizeof(decay_adj) == 2 && decay_adj[0] && decay_adj[1]) ? ({decay_adj[0]}) : ({})) + 
            ((sizeof(przym_zident) == 2 && przym_zident[0] && przym_zident[1]) ? przym_zident[0] : ({})) + ::parse_command_adjectiv1_id_list();
    }
    else
        return ((decayed && sizeof(decay_adj) == 2  && decay_adj[0] && decay_adj[1]) ? ({decay_adj[0]}) : ({})) + ::parse_command_adjectiv1_id_list();
}

public string *
parse_command_adjectiv2_id_list()
{
    int mz;

    mz = moze_zidentyfikowac(find_previous_living());

    if(rosnie && mz && sizeof(id_pnazwa_cal))
    {
        return ((decayed && sizeof(decay_adj) == 2 && decay_adj[0] && decay_adj[1]) ? ({decay_adj[1]}) : ({})) +
            ((sizeof(id_przym_cal) == 2 && id_przym_cal[0] && id_przym_cal[1]) ? id_przym_cal[1] : ({}));
    }
    else if(sizeof(unid_pnazwa_cal) && rosnie)
    {
        return ((decayed && sizeof(decay_adj) == 2 && decay_adj[0] && decay_adj[1]) ? ({decay_adj[1]}) : ({})) +
            ((sizeof(unid_przym_cal) == 2 && unid_przym_cal[0] && unid_przym_cal[1]) ? unid_przym_cal[1] : ({}));
    }
    if(mz && sizeof(pnazwa_zident))
    {
        return ((decayed && sizeof(decay_adj) == 2 && decay_adj[0] && decay_adj[1]) ? ({decay_adj[1]}) : ({})) +
            ((sizeof(przym_zident) == 2 && przym_zident[0] && przym_zident[1]) ? przym_zident[1] : ({})) + ::parse_command_adjectiv2_id_list();
    }
    else
        return ((decayed && sizeof(decay_adj) == 2 && decay_adj[0] && decay_adj[1]) ? ({decay_adj[1]}) : ({})) + ::parse_command_adjectiv2_id_list();
}

public string *
parse_command_ng_id_list(int przyp)
{
    int mz;

    mz = moze_zidentyfikowac(find_previous_living());

    if(mz && rosnie && id_png_cal)
        return ({id_png_cal[przyp]});
    else if(rosnie && unid_png_cal)
        return ({unid_png_cal[przyp]});
    else if(mz && png_zident)
        return ({png_zident[przyp]});
    else
        return ::parse_command_ng_id_list(przyp);
}

public string *
parse_command_ng_plural_id_list(int przyp)
{
    int mz;

    mz = moze_zidentyfikowac(find_previous_living());

    if(mz && rosnie && id_png_cal)
        return ({id_mng_cal[przyp]});
    else if(rosnie && unid_png_cal)
        return ({unid_mng_cal[przyp]});
    else if(mz && png_zident)
        return ({mng_zident[przyp]});
    else
        return ::parse_command_ng_plural_id_list(przyp);
}

public int *
parse_command_ng_rodzaj_id_list(int przyp)
{
    int mz;

    mz = moze_zidentyfikowac(find_previous_living());

    if(mz && rosnie && id_png_cal)
        return ({id_rodz_ng_cal});
    else if(rosnie && unid_png_cal)
        return ({unid_rodz_ng_cal});
    else if(mz && png_zident)
        return ({rodz_ng_zident});
    else
        return ::parse_command_ng_rodzaj_id_list(przyp);

}

public int *
parse_command_ng_plural_rodzaj_id_list(int przyp)
{
    int mz;

    mz = moze_zidentyfikowac(find_previous_living());

    if(mz && rosnie && id_png_cal)
        return ({id_rodz_ng_cal});
    else if(rosnie && unid_png_cal)
        return ({unid_rodz_ng_cal});
    else if(mz && png_zident)
        return ({rodz_ng_zident});
    else
        return ::parse_command_ng_plural_rodzaj_id_list(przyp);
}
//Koniec fixme.

/**
 * Kreator ziola, funkcja wywolywana w create_object
 */
void
create_herb()
{
    ustaw_nazwe("kwiatek");
    start_decay();
    ustaw_nazwe_ziola("mak");
}

/**
 * Reseter ziola, wywolywany kiedy ziolo sie resetuje, funkcja
 * wywolywana jest w reset_object
 */
void
reset_herb()
{
}

void
create_object()
{
    decay_adj = ({"ususzony", "ususzeni"});
    dodaj_nazwy("zio�o");
    ustaw_czas_psucia(130, 150);

    set_type(O_ZIOLA);
    create_herb();

    if(!m_sizeof(komendy_zbierania))
        komendy_zbierania = (["zerwij":({"zrywasz", "zrywa", "zerwa�", 100})]);

    decay_mess = QCSHORT(TO, PL_MIA) + " usycha.\n";
    total_decay_mess =  QCSHORT(TO, PL_MIA) + " jest tak such" + koncowka("y", "a", "e") + ", " +
        "�e rozsypuje si� na proch.\n";

    set_long(&herb_long_desc());
}

void
reset_object()
{
    reset_herb();
}

public int
moze_zidentyfikowac(object pl)
{
    if(!objectp(pl))
        return 0;
    if(!living(pl))
        return 0;

    return F_HERB_MOZE_ZIDENTYFIKOWAC(pl->query_skill(SS_HERBALISM),trudnosc);
}

public int
moze_znalezc(object pl)
{
    //Dodatkowy filtr, gdyby przypadkiem zio�u uda�o si� jako� pojawi� na lokacji
    if(pora_roku && pora_roku != ENV(TO)->pora_roku())
        return 0;

    if(!objectp(pl))
        return 0;
    if(!living(pl))
        return 0;

    float zielarstwo = itof(pl->query_skill(SS_HERBALISM));
    float spostrzegawczosc = F_STAT_TO_100(itof(pl->query_skill(SS_AWARENESS)));

    if(ENV(TO)->dzien_noc())
    {
        zielarstwo /= 2.0;
        spostrzegawczosc /= 2.0;
    }

    return F_HERB_MOZE_ZNALEZC(zielarstwo, spostrzegawczosc, itof(trudnosc_znalezienia));
}

void
total_decay()
{
    object env;

    if(decayed != 1)
        return 0;

    decay_alarm = 0;

    if(total_decay_mess)
    {
        if(env = environment())
        {
            if(living(env))
                env->catch_msg(total_decay_mess);
            else
                all_inventory(env)->catch_msg(total_decay_mess);
        }
    }
    remove_object();
}

void
decay()
{
    int tmp;
    object env;

    if(decayed)
        return 0;

    decay_alarm = 0;

    if(decay_mess)
    {
        if(env = environment())
        {
            if(living(env))
                env->cach_msg(decay_mess);
            else
                all_inventory(env)->catch_msg(decay_mess);
        }
    }

    decayed = 1;
    start_decay();
}

int
start_decay()
{
    //zio�o si� ju� ca�kiem zniszczy�o, ale z niewiadomych powod�w jeszcze istnieje.
    if(decayed > 1)
        return 1;

    if(decay_alarm)
        return 2;

    decay_stopped = 0;

    //Je�li zio�o jest zio�em od razu si� psuj�cym, lub ju� si� ususzy�o
    //to startujemy z alarmem, na kt�rego ko�cu zio�o po prostu przestanie
    //istnie�
    if(!decay_time_sch || decayed == 1)
        decay_alarm = set_alarm(decay_time_nis_z, 0.0, &total_decay());
    else
        decay_alarm = set_alarm(decay_time_sch_z, 0.0, &decay());
}

int
restart_decay()
{
    if(!decay_alarm)
        return 0;

    remove_alarm(decay_alarm);
    decay_alarm = 0;
    if(!decay_time_sch || decayed == 1)
        decay_alarm = set_alarm(decay_time_nis, 0.0, &total_decay());
    else
        decay_alarm = set_alarm(decay_time_sch, 0.0, &decay());

    decay_stopped = 0;

    return 1;
}

int
stop_decay()
{
    mixed al;

    if(!decay_alarm)
        return 0;

    al = get_alarm(decay_alarm);
    if(!decay_time_sch || decayed == 1)
        decay_time_nis_z = decay_time_nis - al[2];
    else
        decay_time_sch_z = decay_time_sch - al[2];

    remove_alarm(decay_alarm);

    decay_alarm = 0;
    decay_stopped = 1;

    return 1;
}

/* Hooki */
string
herb_hook_komunikat_meczenia_fop(int intens)
{
    if(intens > 0)
         return "Czujesz si� mniej zm�czony.\n";
    else 
         return "Czujesz si� bardziej zm�czony.\n";
}

string
herb_hook_komunikat_meczenia_foo(int intens)
{
    return "";
}

string
herb_hook_komunikat_zdrowienia_fop(int intens)
{
    if(intens > 0)
        return "Czujesz si� lepiej.\n";
    else
        return "Czujesz si� gorzej.\n";
}

string
herb_hook_komunikat_zdrowienia_foo(int intens)
{
    return "";
}

string
herb_hook_komunikat_sily_fop(int intens)
{
    if(intens > 0)
        return "Czujesz si� silniejszy.\n";
    else
        return "Czujesz si� jakby troche s�abszy.\n";
}

string
herb_hook_komunikat_sily_foo(int intens)
{
    return "";
}

string
herb_hook_komunikat_wytrzymalosci_fop(int intens)
{
    if(intens > 0)
        return "Czujesz si� troche bardziej wytrzyma�y.\n";
    else
        return "Czujesz si� jakby troch� mniej wytrzyma�y.\n";
}

string
herb_hook_komunikat_wytrzymalosci_foo(int intens)
{
    return "";
}

string
herb_hook_komunikat_zrecznosci_fop(int intens)
{
    if(intens > 0)
        return "Czujesz si� troche bardziej zr�czny.\n";
    else
        return "Czujesz si� jakby troch� mniej zr�czny.\n";
}

string
herb_hook_komunikat_zrecznosci_foo(int intens)
{
    return "";
}

string
herb_hook_komunikat_inteligencji_fop(int intens)
{
    return "";
}

string
herb_hook_komunikat_inteligencji_foo(int intens)
{
    return "";
}

string
herb_hook_komunikat_madrosci_fop(int intens)
{
    if(intens > 0)
        return "Wydaje Ci si�, �e wiesz teraz troche wi�cej ni� jeszcze przed chwil�.\n";
    else
        return "";
}

string
herb_hook_komunikat_madrosci_foo(int intens)
{
    return "";
}

string
herb_hook_komunikat_odwagi_fop(int intens)
{
    if(intens > 0)
        return "Czujesz si� troche bardziej odwa�ny i pewny siebie.\n";
    else
        return "Nagle �wiat zaczyna wydawa� Ci si� troche bardziej straszny, " + 
            "wszystko troche bardziej Ci� przyt�acza, rzeczy kt�re jeszcze niedawno "+
            "wydawa�y si� niegro�ne, wywo�uj� w tobie strach.\n";
}

string
herb_hook_komunikat_odwagi_foo(int intens)
{
    return "";
}

/* Koniec Hookow */

/**
 * Funkcja dzi�ki kt�rej mo�esz doda� swoje zawi�o��i do dowolnej
 * komendy wywo�ywanej na ziole.
 */
void
special_effect(string verb) {;}

/**
 * Funkcja wywo�ywana za ka�dym razem kiedy z jakim� zio�em co� jest robione
 * za pomoc� komendy ustawionej przez ustaw_komendy.
 */
int
herb_effect(string verb)
{
    int i, st, *eff, *in, intensywnosc=0, tmp, str, dex, con, dis, wis, intel, odt, hep, fat, hal;
    string kom;
    object efekt_ob, prop_remover;
    if(m_sizeof(efekty)) 
    {
        if(is_mapping_index(verb, efekty))
        {	
            if(pointerp(efekty[verb][0]))
                eff = efekty[verb][0];
            else
                eff = ({efekty[verb][0]});

            if(pointerp(efekty[verb][1]))
                in = efekty[verb][1];
            else
                in = ({efekty[verb][1]});
        }
    }

    if(sizeof(eff))
    {
        efekt_ob = clone_object(EFFECT_OBJECT);
        efekt_ob -> move(TP, 1);
        prop_remover = clone_object(TIME_FUNCTION_CHANGER);
        prop_remover -> move(TP, 1);
    }
    for(i=0;i<sizeof(eff);i++)
    {
        int *prop;
        switch(eff[i])
        {
            case HERBS_EFF_ODTRUWANIE:
                return 0;
                odt = 1;
            case HERBS_EFF_LECZENIE:
                st = (((in[i] * TP->query_max_fatigue()) / 100) - (in[i]/10 * query_prop(HERB_I_USED))) / 4;      
                st = (in[i] > 0 ? max(0, st) : min(0, st));

                TP->add_prop(LIVE_I_HERB_EFF_HP, 1);

                if(st != 0)
                {
                    int i = 10;
                    while(i-- > 0)
                    {
                        tmp = random(st);
                        if(tmp)
                            efekt_ob -> add_event(herb_hook_komunikat_zdrowienia_fop(tmp),
                                herb_hook_komunikat_zdrowienia_foo(tmp), st, "heal_hp", tmp);
                    }
                    intensywnosc += (st > 0 ? st : -st);
                    hep = 1;
                }
                break;
            case HERBS_EFF_REGENERACJA:
                st = (((in[i] * TP->query_max_hp()) / 1000) - (in[i]/10 * query_prop(HERB_I_USED))) / 4;
                st = (in[i] > 0 ? max(0, st) : min(0, st));

                TP->add_prop(LIVE_I_HERB_EFF_FAT, 1);

                if(st != 0)
                {
                    int i = 10;
                    while(i-- > 0)
                    {
                        tmp = random(st);
                        if(tmp)
                            efekt_ob -> add_event(herb_hook_komunikat_meczenia_fop(tmp),
                                herb_hook_komunikat_meczenia_foo(tmp), st, "add_fatigue", tmp);	
                    }
                    intensywnosc += (st > 0 ? st : -st);
                    fat = 1;
                }
                break;
            case HERBS_EFF_HALUCYNACJE:
                return 0;
                hal = 1;
            case HERBS_EFF_ZWIEKSZENIE_SILY:
                st = (in[i] * TP->query_stat(SS_STR)) / 200;
                st = (in[i] > 0 ? max(0, st) : min(0, st));

                if(st && TP->query_prop(LIVE_I_HERB_EFF_STR))
                {
                    TP->add_prop(LIVE_I_HERB_EFF_STR, 1);

                    int tmp = random(st);

                    if(tmp)
                        efekt_ob -> add_event(herb_hook_komunikat_sily_fop(tmp),
                            herb_hook_komunikat_sily_foo(tmp), tmp, "add_tmp_stat", SS_STR, tmp);
                    intensywnosc += (st > 0 ? st : -st);
                    str = 1;
                }
            case HERBS_EFF_ZWIEKSZENIE_ZRECZNOSCI:
                st = (in[i] * TP->query_stat(SS_DEX)) / 200;
                st = (in[i] > 0 ? max(0, st) : min(0, st));

                if(st && TP->query_prop(LIVE_I_HERB_EFF_DEX))
                {
                    TP->add_prop(LIVE_I_HERB_EFF_DEX, 1);

                    int tmp = random(st);

                    if(tmp)
                        efekt_ob -> add_event(herb_hook_komunikat_zrecznosci_fop(tmp),
                            herb_hook_komunikat_zrecznosci_foo(tmp), tmp, "add_tmp_stat", SS_DEX, tmp);
                    intensywnosc += (st > 0 ? st : -st);
                    dex = 1;
                }
            case HERBS_EFF_ZWIEKSZENIE_WYTRZYMALOSCI:
                st = (in[i] * TP->query_stat(SS_CON)) / 200;
                st = (in[i] > 0 ? max(0, st) : min(0, st));

                if(st && TP->query_prop(LIVE_I_HERB_EFF_CON))
                {
                    TP->add_prop(LIVE_I_HERB_EFF_CON, 1);

                    int tmp = random(st);

                    if(tmp)
                        efekt_ob -> add_event(herb_hook_komunikat_wytrzymalosci_fop(tmp),
                            herb_hook_komunikat_wytrzymalosci_foo(tmp), tmp, "add_tmp_stat", SS_CON, tmp);
                    intensywnosc += (st > 0 ? st : -st);
                    con = 1;
                }
            case HERBS_EFF_ZWIEKSZENIE_ODWAGI:
                st = (in[i] * TP->query_stat(SS_DIS)) / 200;
                st = (in[i] > 0 ? max(0, st) : min(0, st));

                if(st && TP->query_prop(LIVE_I_HERB_EFF_DIS))
                {
                    TP->add_prop(LIVE_I_HERB_EFF_DIS, 1);

                    int tmp = random(st);

                    if(tmp)
                        efekt_ob -> add_event(herb_hook_komunikat_odwagi_fop(tmp),
                            herb_hook_komunikat_odwagi_foo(tmp), tmp, "add_tmp_stat", SS_DIS, tmp);
                    intensywnosc += (st > 0 ? st : -st);
                    dis = 1;
                }
            case HERBS_EFF_ZWIEKSZENIE_MADROSCI:
                st = (in[i] * TP->query_stat(SS_WIS)) / 200;
                st = (in[i] > 0 ? max(0, st) : min(0, st));

                if(st && TP->query_prop(LIVE_I_HERB_EFF_WIS))
                {
                    TP->add_prop(LIVE_I_HERB_EFF_WIS, 1);

                    int tmp = random(st);

                    if(tmp)
                        efekt_ob -> add_event(herb_hook_komunikat_inteligencji_fop(tmp),
                            herb_hook_komunikat_inteligencji_foo(tmp), tmp, "add_tmp_stat", SS_WIS, tmp);
                    intensywnosc += (st > 0 ? st : -st);
                    wis = 1;
                }
            case HERBS_EFF_ZWIEKSZENIE_INTELIGENCJI:
                st = (in[i] * TP->query_stat(SS_INT)) / 200;
                st = (in[i] > 0 ? max(0, st) : min(0, st));

                if(st && TP->query_prop(LIVE_I_HERB_EFF_INT))
                {
                    TP->add_prop(LIVE_I_HERB_EFF_INT, 1);

                    int tmp = random(st);

                    if(tmp)
                        efekt_ob -> add_event(herb_hook_komunikat_inteligencji_fop(tmp),
                            herb_hook_komunikat_inteligencji_foo(tmp), tmp, "add_tmp_stat", SS_INT, tmp);
                    intensywnosc += (st > 0 ? st : -st);
                    intel = 1;
                }
            }
    }

    if(efekt_ob && intensywnosc)
    {
        efekt_ob -> set_event_time(max(1,intensywnosc/8), max(1,intensywnosc/8));
        efekt_ob -> set_effect_time(max(1, intensywnosc * 4));
        efekt_ob -> start_effect(TP);
        if(str)
            prop_remover -> wykonaj_za(max(1, intensywnosc * 5), "remove_prop", LIVE_I_HERB_EFF_STR);
        if(dex)
            prop_remover -> wykonaj_za(max(1, intensywnosc * 5), "remove_prop", LIVE_I_HERB_EFF_DEX);
        if(con)
            prop_remover -> wykonaj_za(max(1, intensywnosc * 5), "remove_prop", LIVE_I_HERB_EFF_CON);
        if(wis)
            prop_remover -> wykonaj_za(max(1, intensywnosc * 5), "remove_prop", LIVE_I_HERB_EFF_WIS);
        if(intel)
            prop_remover -> wykonaj_za(max(1, intensywnosc * 5), "remove_prop", LIVE_I_HERB_EFF_INT);
        if(dis)
            prop_remover -> wykonaj_za(max(1, intensywnosc * 5), "remove_prop", LIVE_I_HERB_EFF_DIS);
        if(hep)
            prop_remover -> wykonaj_za(max(1, intensywnosc * 5), "remove_prop", LIVE_I_HERB_EFF_HP);
        if(fat)
            prop_remover -> wykonaj_za(max(1, intensywnosc * 5), "remove_prop", LIVE_I_HERB_EFF_FAT);
        if(hal)
            prop_remover -> wykonaj_za(max(1, intensywnosc * 5), "remove_prop", LIVE_I_HERB_EFF_HAL);
        if(odt)
            prop_remover -> wykonaj_za(max(1, intensywnosc * 5), "remove_prop", LIVE_I_HERB_EFF_ODT);
    }
    else
    {
        efekt_ob -> remove_object();
        prop_remover -> remove_object();
    }
    return intensywnosc;
}

int
efekt_dzialania_ziola(string str)
{
    int tmp;
    object *obs;
    string verb = query_verb(), kom;

    // TODO: Mo�na jeszcze na hooki przerobi�
    notify_fail("Co takiego chcesz " + komendy[verb][2] + "?\n");

    if(!str)
        return 0;
    if(!parse_command(str, all_inventory(TP), "%i:" + PL_BIE, obs))
        return 0;

    obs = VISIBLE_ACCESS(obs, 0, 0);

    if(!sizeof(obs))
    {
        notify_fail("Nie ma tu niczego takiego.\n");
        return 0;
    }
    else if(sizeof(obs) > 1)
    {
        notify_fail("Nie mo�esz " + komendy[verb][2] + " tylu rzeczy na raz.\n");
        return 0;
    }
    if(!obs[0]->query_herb())
    {
        notify_fail("Nie mo�esz " + komendy[verb][2] + " " + COMPOSITE_DEAD(obs, PL_DOP) + ".\n");
        return 0;
    }

    if((tmp = obs[0]->special_effect(verb)))
    {
        if(tmp)
        {
            if(is_mapping_index(verb, efekty))
                if(pointerp(efekty[verb]))
                    if(efekty[verb][2] == 1)		
                        obs[0]->add_prop(HERB_I_USED, query_prop(HERB_I_USED) + 1);
            if(!obs->query_prop(HERB_I_USED))
                obs[0]->remove_object();
        }
        return (tmp == 1 ? 0 : 1);
    }

    obs[0]->herb_effect(verb);

    write(UC(komendy[verb][0]) + " " + COMPOSITE_DEAD(obs, PL_BIE) + ".\n");
    say(QCIMIE(TP, PL_MIA) + " " + komendy[verb][1] + " " + QCOMPDEAD(PL_BIE) + ".\n");

    if(is_mapping_index(verb, komendy))
       if(pointerp(komendy[verb]))
            if(sizeof(komendy[verb]) > 3)
                if(komendy[verb][3])
                    obs[0]->add_prop(HERB_I_USED, query_prop(HERB_I_USED) + 1);
    if(!obs[0]->query_prop(HERB_I_USED))
        obs[0]->remove_object();

    return 1;
}

public string
zebranie_ziola(string verb)
{;}

private static mixed
only_first_arg(mixed ob)
{
    if(pointerp(ob))
        return ob[0];
    return ob;
}

object *
znajdz_calosc_ziola(object ob, object *ziola, string verb)
{
    foreach(mixed x : ziola)
    {
        if(member_array(ob, x) >= 0)
        {
            foreach(mixed y : x)
            {
                if(!is_mapping_index(verb, y->query_komendy_zbierania()))
                {
                    x -= ({y});
                    //y -> remove_object();
                }
            }
            return x;
        }
    }
}

static nomask private int
herb_access(object ob)
{
    return (ob->query_herb() && (member_array(ob, flatten(ENV(TP)->query_znalezione(TP->query_real_name()))) >= 0));
}

int
gracz_zbiera_ziolo(string str)
{
    mixed r;
    object *obs;
    mixed herbs_obs;
    string verb = query_verb();

    if(!str)
    {
        notify_fail("Co takiego chcesz " + komendy_zbierania[verb][2] + "?\n");
        return 0;
    }

    herbs_obs = ENV(TP)->query_rosnace();
    // FIXME: Trzeba przepisa� na flattena i potem wyszukiwa� o kt�re zio�o chodzi, coby 
    //        dzia�a�y r�ne komendy dla r�nych kawa�k�w zio�a.
    herbs_obs = map(herbs_obs, only_first_arg);
    herbs_obs = flatten(herbs_obs);

    herbs_obs->unset_no_show();
    herbs_obs->set_no_show_composite(0);
    herbs_obs->remove_prop(OBJ_M_NO_GET);

    if(!parse_command(str, herbs_obs, "%i:" + PL_BIE, obs))
    {
        NF("Co chcesz " + komendy_zbierania[verb][2] + "?\n");
        return 0;
    }

    herbs_obs = ENV(TP)->query_rosnace();
    obs = DIRECT_ACCESS(obs);
    obs = filter(obs, herb_access);

    if(!sizeof(obs))
    {
        notify_fail("Nie ma tu niczego takiego.\n");
        return 0;
    }
    else if(sizeof(obs)>1)
    {
        notify_fail("Mo�esz " + komendy_zbierania[verb][2] + " tylko jedno zio�o.\n");
        return 0;
    }

    if(obs[0] != TO)
    {
        NF("Co chcesz " + komendy_zbierania[verb][2] + "?\n");
        return 0;
    }

    if(!query_rosnie())
        return 0; //Reaguje tak jakby tej komendy nie by�o.


    obs = znajdz_calosc_ziola(obs[0], herbs_obs, verb);
    obs->remove_prop(OBJ_M_NO_GET);

    if(random(100) >= komendy_zbierania[verb][3])
    {
        write(UC(komendy_zbierania[verb][0]) + " " + COMPOSITE_DEAD(({obs[0]}), PL_BIE) + ", "+
            "jednak nie udaje Ci si� zdoby� �adnej warto�ciowej cz�ci zio�a.\n");
        saybb(QCIMIE(TP, PL_MIA) + " " + komendy_zbierania[verb][1] + " " + QCOMPDEAD(PL_BIE) + ".\n");
        ENV(TP)->usun_rosnace(obs);
        obs->remove_object();
        return 1;
    }

    r = zebranie_ziola(verb);

    if(r)
    {
        notify_fail(r);
        return 0;
    }

    r = obs->move(TP);
    //DBG("Move");dump_array(r);
    r -= ({1});

    if(r == ({}))
    {
        notify_fail("Nie mo�esz tego " + komendy_zbierania[verb][2] + ".\n");
        return 0;
    }

    write(UC(komendy_zbierania[verb][0]) + " " + COMPOSITE_DEAD(({obs[0]}), PL_BIE) + ".\n");
    saybb(QCIMIE(TP, PL_MIA) + " " + komendy_zbierania[verb][1] + " " + QCOMPDEAD(PL_BIE) + ".\n");
    mixed tmp = flatten(herbs_obs);
    tmp->set_no_show();
    tmp->add_prop(OBJ_M_NO_GET, "We� co?\n");
    //DBG("TMP = ");dump_array(tmp);
    obs->ustaw_ze_rosnie(0);
    ENV(TP)->usun_rosnace(obs);

    return 1;
}

void
init_zbieranie()
{
    map(m_indexes(komendy_zbierania), &add_action(gracz_zbiera_ziolo,));
}

void
init_uzywanie()
{
    int i;
    string *cmds;

    cmds = m_indexes(komendy);

    for(i=0;i<sizeof(cmds);i++)
    {
        if(cmds[i] == "zjedz")
            continue;
        add_action(efekt_dzialania_ziola, cmds[i]);
    }
}

void
init()
{
    init_zbieranie();
    init_uzywanie();
    ::init();
}

public nomask string
query_auto_load()
{
    if(query_rosnie())
        return 0;
    else
        return ::query_auto_load();
}
