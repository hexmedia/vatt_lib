/* 
 *  /std/kamien.c
 *
 *  Uzyj tego obiektu, aby stworzyc kamien szlachetny.
 */

#pragma save_binary
#pragma strict_types

inherit "/std/object";
inherit "/lib/keep";

#include <object_types.h>
#include <stdproperties.h>
#include <macros.h>
#include <pl.h>

/*
 * Function name: create_kamien
 * Description  : Uzyj tej funkcji, aby stworzyc kamien.
 */
public void
create_kamien()
{
    ustaw_nazwe("kwarc");
    dodaj_przym("bia^ly", "biali");
    set_long("Masz przed sob^a kwarc, niezwykle pospolity i nie posiadaj^acy " +
	"wi^ekszej warto^sci minera^l. Jest p^o^lprzezroczysty o bia^lawym " +
	"zabarwieniu.\n");
}

/*
 * Function name: create_object
 * Description  : Initialize this object. It will set a few default
 *                variables. You may not mask this function. Define the
 *                function create_kamien() instead.
 */
nomask void
create_object()
{
    add_prop(OBJ_I_VALUE, 100);
    add_prop(OBJ_I_VOLUME,  5);
    add_prop(OBJ_I_WEIGHT, 30);

    ustaw_nazwe( ({ "kamien", "kamienia", "kamieniowi", "kamien", "kamieniem",
        "kamieniu" }), ({ "kamienie", "kamieni", "kamieniom", "kamienie",
        "kamieniami", "kamieniach" }), PL_MESKI_NOS_NZYW);

    set_keep(1);

    set_type(O_KAMIENIE_SZLACHETNE);

    create_kamien();
}

/*
 * Function name: reset_kamien
 * Description  : This function should hold the code that is to be executed
 *                when this object resets. In order for reset to work, you
 *                must call enable_reset() from your create_kamien.
 */
public void
reset_kamien()
{
}

/*
 * Function name: reset_object
 * Description  : You may not mask this function to make the key reset.
 *                Rather define the function reset_key.
 */
nomask void
reset_object()
{
    reset_kamien();
}
