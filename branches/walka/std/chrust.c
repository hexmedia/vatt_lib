/*
 * Jest to obiekt chrustu z kt�rego mo�na u�o�y� ognisko! :P
 *
 * Dzielenie wi�zki chrustu zer�ni�te z /std/food, z poprawkami
 * u�atwiaj�cymi implementacj� ;)
 *
 * Vera
 */ 

//#pragma strict_types
inherit "/std/object";
//#include <object_types.h>

#include <macros.h>
//#include <files.h>
#include <stdproperties.h>
#include <cmdparse.h>
#define DBG(x) find_player("vera")->catch_msg(x+"\n");

#define CHRUST_OBJ "/std/chrust"

static	object	*gFail;
object paraliz;

void
create_object()
{
    ustaw_nazwe_glowna("wi�zka");
    ustaw_nazwe("chrust");

    set_long("Suche patyki, drobne ga��zki, kora i igliwie tworzy "+
             "t� wi�zk� chrustu.\n");
    add_prop(OBJ_I_WEIGHT, 800 + random(1200));
    add_prop(OBJ_I_VOLUME, query_prop(OBJ_I_WEIGHT) * 7);
}



void
complete()
{

    object ogn = clone_object("/std/ognisko.c");

    switch(TP->query_acc_exp(4))
    {
        case 0..1000: write("Nie, przecie� ty kompletnie nie wiesz jak to zrobi�! "+
                      "Twoja misterna konstrukcja rozpada si� natychmiast po "+
                      "tym, jak odsuwasz od niej swe d�onie.\n");
                      saybb(QIMIE(this_player(), PL_CEL)+" uda�o si� u�o�y� jak�� "+
                      "konstrukcj� przypominaj�c� ognisko, lecz ta prawie natychmiast "+
                      "po tym rozpada si�.\n");
                      break;
        case 1001..2000: write("Uda�o ci si� u�o�y� co�, co swoim wygl�dem przypomina "+
                         "troch� ognisko.\n");
                      saybb(QIMIE(this_player(), PL_CEL)+" uda�o si� u�o�y� co�, "+
                         "co swoim wygl�dem przypomina troch� ognisko.\n");
                      //ogn->add_fuel( ({ this_object() }) );
                      ogn->move(environment(this_player()), 1);
                      break;
        default:      write("Uda�o ci si� u�o�y� niewielkie ognisko.\n");
                      saybb(QIMIE(this_player(), PL_CEL)+" uda�o si� u�o�y� "+
                      "niewielkie ognisko.\n");
                      //ogn->add_fuel( ({ this_object() }) );
                      ogn->move(environment(this_player()), 1);
                      break;
    }

    paraliz->remove_object();
    remove_object();
}


int
sprawdz_typ_lokacji()
{
	if(ENV(TP)->query_prop(ROOM_I_TYPE) == ROOM_SWAMP ||
		ENV(TP)->query_prop(ROOM_I_TYPE) == ROOM_IN_CITY ||
		ENV(TP)->query_prop(ROOM_I_TYPE) == ROOM_IN_AIR ||
		ENV(TP)->query_prop(ROOM_I_TYPE) == ROOM_UNDER_WATER ||
		ENV(TP)->query_prop(ROOM_I_TYPE) == ROOM_IN_WATER ||
		ENV(TP)->query_prop(ROOM_I_TYPE) == ROOM_TREE ||
		ENV(TP)->query_prop(ROOM_I_TYPE) == ROOM_NORMAL)
		return 1;
	else
		return 0;
}

int
uloz(string str)
{
    if(str != "ognisko")
    {
        notify_fail(capitalize(query_verb())+" co ?\n");
        return 0;
    }

    if(!HAS_FREE_HANDS(TP))
    {
        notify_fail("Musisz mie� obie d�onie wolne, aby m�c to zrobi�.\n"); 
        return 0;
    }
    
    if(sprawdz_typ_lokacji())
    {
        notify_fail("To chyba nie jest odpowiednie miejsce do "+
        	"uk�adania ogniska.\n"); 
        return 0;
    }

    setuid();
    seteuid(getuid());
    paraliz = clone_object("/std/paralyze.c");
    paraliz->set_fail_go_message("Jeste� zaj�t"+TP->koncowka("y","a","e")+
             " uk�adaniem ogniska, musisz wpierw przesta� to robi�, by m�c "+
              "si� tam uda�.\n");
    paraliz->set_fail_message("Jeste� zaj�t"+TP->koncowka("y","a","e")+
             " uk�adaniem ogniska, musisz wpierw przesta� to robi�, by m�c "+
              "to uczyni�.\n");
    paraliz->set_stop_message("");
    paraliz->set_finish_object(this_object());
    paraliz->set_stop_verb("przesta�");
    paraliz->set_stop_object(this_object());
    paraliz->set_finish_fun("complete");
    paraliz->set_standard_paralyze("uk�ada� ugnisko");
    paraliz->set_remove_time(10 + random(15));
    paraliz->move(this_player());
    write("Kl�kasz na ziemi i zaczynasz uk�ada� ognisko.\n");
    saybb(QCIMIE(this_player(), PL_MIA) + " kl�ka na ziemi i zaczyna " +
        "uk�ada� ognisko.\n");

    return 1;
}

int
divide_one_thing(object ob)
{
    /*int am, num, i;

    am = (int) ob->query_prop(OBJ_I_WEIGHT);
    num = (int) ob->num_heap();

    for (i = 1; i <= num; i++)
    {*/
        if (ob->query_prop(OBJ_I_WEIGHT) < 1000)
        {
	 /*   if (i == 1)
	    {
		ob->split_heap(1);*/
		
		//gFail += ({ ob });
	    	return 0;
	    }
	    /*ob->split_heap(i - 1);
	    return 1;
        }
    }*/

    return 1;
}

int
divide_access(object ob)
{ 
    if ((environment(ob) == TP) &&
	(function_exists("uloz", ob)) &&
	(ob->query_short()))
	return 1;
    else
	return 0;
}

/*
 * Function name: divide_text
 * Description:   Here the divide message is written. You may redefine it if
 *		  you wish.
 * Arguments:     arr - Objects being divided
 *		  vb  - The verb player used to divide them
 */
void
divide_text(object *arr, string vb)
{
    string str;

    write("Dzielisz " + (str = COMPOSITE_DEAD(arr, PL_BIE)) + " na dwie cz^e^sci.\n");
    saybb(QCIMIE(TP, PL_MIA) + " dzieli " + str + " na dwie cz^e^sci.\n");
}

void
destruct_object()
{
    /*if (leave_behind > 0)
    {
	set_heap_size(leave_behind);
    }
    else
    {*/
	add_prop(TEMP_OBJ_ABOUT_TO_DESTRUCT, 1);
	set_alarm(0.5, 0.0, remove_object);
    //}
}

public int
podziel(string str)
{
    object 	*a, *foods;
    int		il;
    string str2, vb;

    notify_fail("Co chcesz podzieli^c?\n");

    /* This food has already been divided or already been tried, so we won't
     * have to test again.
     */
    if (query_prop(TEMP_OBJ_ABOUT_TO_DESTRUCT) ||
	TP->query_prop(TEMP_STDFOOD_CHECKED))
    {
	return 0;
    }

    //gFail = ({ });
    vb = query_verb(); 
    
    a = CMDPARSE_ONE_ITEM(str, "divide_one_thing", "divide_access");
//    a = CMDPARSE_ONE_ITEM(str, "", "divide_access");
//    if (sizeof(a) > 1)
//    {
//	write("Nie mo^esz dzieli^c wi^ecej ni^z jednej rzeczy naraz.\n");
//	return 1;
//    }
    if (sizeof(a) > 0)
    {
	divide_text(a, vb);
	for (il = 0; il < sizeof(a); il++)
	{
	    object ob1 = clone_object(CHRUST_OBJ), ob2 = clone_object(CHRUST_OBJ);
	    string dop = a[il]->query_nazwa(PL_DOP), sdop = a[il]->singular_short(PL_DOP);
	    //mixed przym = a[il]->query_przymiotniki();
	    int j;

	   /* if (dop[0..6] == "kawa^lka")
	    {
		ob1->ustaw_nazwe(({a[il]->query_nazwa(PL_MIA),
		    a[il]->query_nazwa(PL_DOP), a[il]->query_nazwa(PL_CEL),
		    a[il]->query_nazwa(PL_BIE), a[il]->query_nazwa(PL_NAR),
		    a[il]->query_nazwa(PL_NAR)}), ({a[il]->query_pnazwa(PL_MIA),
		    a[il]->query_pnazwa(PL_DOP), a[il]->query_pnazwa(PL_CEL),
		    a[il]->query_pnazwa(PL_BIE), a[il]->query_pnazwa(PL_NAR),
		    a[il]->query_pnazwa(PL_MIE)}), a[il]->query_rodzaj());
		ob1->ustaw_shorty(({a[il]->singular_short(PL_MIA),
		    a[il]->singular_short(PL_DOP), a[il]->singular_short(PL_CEL),
		    a[il]->singular_short(PL_BIE), a[il]->singular_short(PL_NAR),
		    a[il]->singular_short(PL_NAR)}), ({a[il]->plural_short(PL_MIA),
		    a[il]->plural_short(PL_DOP), a[il]->plural_short(PL_CEL),
		    a[il]->plural_short(PL_BIE), a[il]->plural_short(PL_NAR),
		    a[il]->plural_short(PL_MIE)}), PL_MESKI_NOS_NZYW);
	    }
	    else
	    {
		ob1->ustaw_nazwe(({"kawa^lek " + dop, "kawa^lka " + dop,
		    "kawa^lkowi " + dop, "kawa^lek " + dop, "kawa^lkiem " + dop,
		    "kawa^lku " + dop}), ({"kawa^lki " + dop, "kawa^lk^ow " + dop,
		    "kawa^lkom " + dop, "kawa^lki " + dop, "kawa^lkami " + dop,
		    "kawa^lkach " + dop}), PL_MESKI_NOS_NZYW);
//		ob1->dodaj_nazwy(({a[il]->query_nazwa(PL_MIA),
//		    a[il]->query_nazwa(PL_DOP), a[il]->query_nazwa(PL_CEL),
//		    a[il]->query_nazwa(PL_BIE), a[il]->query_nazwa(PL_NAR),
//		    a[il]->query_nazwa(PL_NAR)}), ({a[il]->query_pnazwa(PL_MIA),
//		    a[il]->query_pnazwa(PL_DOP), a[il]->query_pnazwa(PL_CEL),
//		    a[il]->query_pnazwa(PL_BIE), a[il]->query_pnazwa(PL_NAR),
//		    a[il]->query_pnazwa(PL_MIE)}), a[il]->query_rodzaj());
		ob1->ustaw_shorty(({"kawa^lek " + sdop, "kawa^lka " + sdop,
		    "kawa^lkowi " + sdop, "kawa^lek " + sdop, "kawa^lkiem " + sdop,
		    "kawa^lku " + sdop}), ({"kawa^lki " + sdop, "kawa^lk^ow " + sdop,
		    "kawa^lkom " + sdop, "kawa^lki " + sdop, "kawa^lkami " + sdop,
		    "kawa^lkach " + sdop}), PL_MESKI_NOS_NZYW);
	    }
	    for (j = 0; j < sizeof(przym[0]); ++j)
		ob1->dodaj_przym(przym[0][j], przym[1][j]);
	    ob1->set_amount(a[il]->query_amount()/2);*/

            ob1->add_prop(OBJ_I_WEIGHT,a[il]->query_prop(OBJ_I_WEIGHT)/2);
            ob1->add_prop(OBJ_I_VOLUME,a[il]->query_prop(OBJ_I_VOLUME)/2);

	    ob1->set_long(a[il]->long());
	    /*ob1->add_prop(HEAP_I_UNIT_VOLUME, a[il]->query_prop(HEAP_I_UNIT_VOLUME)/2);
	    ob1->add_prop(HEAP_I_UNIT_WEIGHT, a[il]->query_prop(HEAP_I_UNIT_WEIGHT)/2);
	    ob1->add_prop(HEAP_I_UNIT_VALUE, a[il]->query_prop(HEAP_I_UNIT_VALUE)/2);
	    ob1->set_decay_time(a[il]->query_decay_time());
	    ob1->set_decay_adj(a[il]->query_decay_adj());
	    if (dop[0..6] == "kawa^lka")
	    {
		ob2->ustaw_nazwe(({a[il]->query_nazwa(PL_MIA),
		    a[il]->query_nazwa(PL_DOP), a[il]->query_nazwa(PL_CEL),
		    a[il]->query_nazwa(PL_BIE), a[il]->query_nazwa(PL_NAR),
		    a[il]->query_nazwa(PL_NAR)}), ({a[il]->query_pnazwa(PL_MIA),
		    a[il]->query_pnazwa(PL_DOP), a[il]->query_pnazwa(PL_CEL),
		    a[il]->query_pnazwa(PL_BIE), a[il]->query_pnazwa(PL_NAR),
		    a[il]->query_pnazwa(PL_MIE)}), a[il]->query_rodzaj());
		ob2->ustaw_shorty(({a[il]->singular_short(PL_MIA),
		    a[il]->singular_short(PL_DOP), a[il]->singular_short(PL_CEL),
		    a[il]->singular_short(PL_BIE), a[il]->singular_short(PL_NAR),
		    a[il]->singular_short(PL_NAR)}), ({a[il]->plural_short(PL_MIA),
		    a[il]->plural_short(PL_DOP), a[il]->plural_short(PL_CEL),
		    a[il]->plural_short(PL_BIE), a[il]->plural_short(PL_NAR),
		    a[il]->plural_short(PL_MIE)}), PL_MESKI_NOS_NZYW);
	    }
	    else
	    {
		ob2->ustaw_nazwe(({"kawa^lek " + dop, "kawa^lka " + dop,
		    "kawa^lkowi " + dop, "kawa^lek " + dop, "kawa^lkiem " + dop,
		    "kawa^lku " + dop}), ({"kawa^lki " + dop, "kawa^lk^ow " + dop,
		    "kawa^lkom " + dop, "kawa^lki " + dop, "kawa^lkami " + dop,
		    "kawa^lkach " + dop}), PL_MESKI_NOS_NZYW);
//		ob2->dodaj_nazwy(({a[il]->query_nazwa(PL_MIA),
//		    a[il]->query_nazwa(PL_DOP), a[il]->query_nazwa(PL_CEL),
//		    a[il]->query_nazwa(PL_BIE), a[il]->query_nazwa(PL_NAR),
//		    a[il]->query_nazwa(PL_NAR)}), ({a[il]->query_pnazwa(PL_MIA),
//		    a[il]->query_pnazwa(PL_DOP), a[il]->query_pnazwa(PL_CEL),
//		    a[il]->query_pnazwa(PL_BIE), a[il]->query_pnazwa(PL_NAR),
//		    a[il]->query_pnazwa(PL_MIE)}), a[il]->query_rodzaj());
		ob2->ustaw_shorty(({"kawa^lek " + sdop, "kawa^lka " + sdop,
		    "kawa^lkowi " + sdop, "kawa^lek " + sdop, "kawa^lkiem " + sdop,
		    "kawa^lku " + sdop}), ({"kawa^lki " + sdop, "kawa^lk^ow " + sdop,
		    "kawa^lkom " + sdop, "kawa^lki " + sdop, "kawa^lkami " + sdop,
		    "kawa^lkach " + sdop}), PL_MESKI_NOS_NZYW);
	    }
	    for (j = 0; j < sizeof(przym[0]); ++j)
		ob2->dodaj_przym(przym[0][j], przym[1][j]);
	    ob2->set_amount(a[il]->query_amount()/2);*/

            ob2->add_prop(OBJ_I_WEIGHT,a[il]->query_prop(OBJ_I_WEIGHT)/2);
            ob2->add_prop(OBJ_I_VOLUME,a[il]->query_prop(OBJ_I_VOLUME)/2);

	    ob2->set_long(a[il]->long());
	    /*ob2->add_prop(HEAP_I_UNIT_VOLUME, a[il]->query_prop(HEAP_I_UNIT_VOLUME)/2);
	    ob2->add_prop(HEAP_I_UNIT_WEIGHT, a[il]->query_prop(HEAP_I_UNIT_WEIGHT)/2);
	    ob2->add_prop(HEAP_I_UNIT_VALUE, a[il]->query_prop(HEAP_I_UNIT_VALUE)/2);
	    ob2->set_decay_time(a[il]->query_decay_time());
	    ob2->set_decay_adj(a[il]->query_decay_adj());
*/
	    a[il]->destruct_object();
//	    ob1->set_heap_num(1);
	    ob1->move(TP, 1);
//	    ob1->start_decay();
//	    ob2->set_heap_num(1);
	    ob2->move(TP, 1);
//	    ob2->start_decay();
	}
	return 1;
    }
    else
    {
	/*TP->add_prop(TEMP_STDFOOD_CHECKED, 1);
	set_alarm(1.0, 0.0,
	    &(TP)->remove_prop(TEMP_STDFOOD_CHECKED));*/
	//if (sizeof(gFail))
	  //  notify_fail("@@divide_fail:" + file_name(this_object()));
        notify_fail(capitalize(TO->short(PL_MIA)) + " " +
		    koncowka("jest", "jest", "jest", "s^a", "s^a") + " zbyt " +
		    koncowka("ma^ly", "ma^la", "ma^le", "mali", "ma^le") + ", ^zeby " +
		    koncowka("go", "j^a", "je", "ich", "je") + " podzieli^c.\n");

	return 0;
    }
}




void
init()
{
    ::init();
    add_action(uloz, "u��");
    add_action(uloz, "zbuduj");
    add_action(podziel, "podziel");
}

int
query_chrust()
{
    return 1;
}
