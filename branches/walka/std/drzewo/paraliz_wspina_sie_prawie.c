#pragma strict_types
#include <exp.h>
#include <ss_types.h>
#include <macros.h>
#include <stdproperties.h>

inherit "/std/paralyze";

void
create_paralyze()
{
    set_name("wspinasie");
    set_stop_verb("przesta�");
    set_stop_message("Przestajesz si� wspina� na drzewo.\n");
    set_fail_message("Jeste� teraz zaj�t"+TP->koncowka("y","a")+
                     " wspinaniem si� na drzewo. Musisz wpisa� "+
                "'przesta�' by m�c zrobi�, to co chcesz.\n");
    set_finish_object(TO);
    set_finish_fun("koniec_wspinaczki");
    set_stop_object(TO);
    set_stop_fun("stop_wspinaczka");
    set_remove_time(6);
    setuid();
    seteuid(getuid());
}

int
stop_wspinaczka(string str)
{
    if (str) {
        return 1;
    }
    TP->add_prop(LIVE_S_EXTRA_SHORT, TP->query_prop("_live_s_extra_short_old"));
    tell_room(ENV(TP), QCIMIE(TP, PL_MIA) + " przestaje wspina� si� na drzewo.\n", TP);
    return 0;
}

void
koniec_wspinaczki(object player)
{
    TP->add_prop(LIVE_S_EXTRA_SHORT, TP->query_prop("_live_s_extra_short_old"));
    player->catch_msg("Puszczasz si� pnia i spadasz bole�nie na ziemi�.\n"+
           "Czujesz, �e gdyby� potrafi�"+player->koncowka("","a")+
	   " jeszcze troch� bardziej si� "+
	   "wspina� podo�a�"+player->koncowka("by�","aby�")+" temu zadaniu.\n");
    tell_room(ENV(player), QCIMIE(player, PL_MIA) + " puszcza si� pnia i "+
           "spada bole�nie na ziemi�.\n", player);
    player->set_hp(player->query_hp()-9);
    player->add_fatigue(-40);
    player->increase_ss(SS_CLIMB,EXP_WSPINAM_PRAWIE_UDANY_CLIMB);
	player->increase_ss(SS_STR,EXP_WSPINAM_PRAWIE_UDANY_CLIMB_STR);
    player->increase_ss(SS_DEX,EXP_WSPINAM_PRAWIE_UDANY_CLIMB_DEX);
	player->increase_ss(SS_CON,EXP_WSPINAM_PRAWIE_UDANY_CLIMB_CON);
    
    remove_object();
}
