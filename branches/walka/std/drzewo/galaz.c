inherit "/std/object.c";

#include <macros.h>
#include <stdproperties.h>

float gestosc=0.6;

//int zbuduj(string str);

void create_galaz()
{
	ustaw_nazwe("galaz");
	set_long("Jest to zwyk�a, niczym nie wyr�niaj�ca sie ga���.\n");
	add_prop(OBJ_I_WEIGHT, 2300);
}

void create_object()
{
	create_galaz();
}

init()
{
    ::init();
}

/* Przy ustawianiu masy automatycznie generujemy objetosc (na podst. gestosci i masy) */
void add_prop(string prop, mixed val)
{
	if(prop == OBJ_I_WEIGHT)
	{
		::add_prop(prop, val);
		::add_prop(OBJ_I_VOLUME, ftoi(itof(val)/gestosc));
	}
	else
		::add_prop(prop, val);
}

void ustaw_gestosc(float f)
{
	gestosc = f;
}

float query_gestosc()
{
	return gestosc;
}

/* Odszukuje lokacje w ktorej sie znajdujemy (przydatne do opisow) */
object find_room()
{
	object tmp = this_object();
	
	while(function_exists("create_container", tmp) != "/std/room")
		tmp = environment(tmp);
	
	return tmp;
}

int czy_galaz()
{
	return 1;
}