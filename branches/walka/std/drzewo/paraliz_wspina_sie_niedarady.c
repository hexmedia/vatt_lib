#pragma strict_types
#include <exp.h>
#include <ss_types.h>
#include <macros.h>
#include <stdproperties.h>

inherit "/std/paralyze";

void
create_paralyze()
{
    set_name("wspinasie");
    set_stop_verb("przesta�");
    set_stop_message("Przestajesz si� wspina� na drzewo.\n");
    set_fail_message("Jeste� teraz zaj�t"+TP->koncowka("y","a")+
                     " wspinaniem si� na drzewo. Musisz wpisa� "+
                "'przesta�' by m�c zrobi�, to co chcesz.\n");
    set_finish_object(TO);
    set_finish_fun("koniec_wspinaczki");
    set_stop_object(TO);
    set_stop_fun("stop_wspinaczka");
    set_remove_time(4);
    setuid();
    seteuid(getuid());
}

int
stop_wspinaczka(string str)
{
    if (str) {
        return 1;
    }
    TP->add_prop(LIVE_S_EXTRA_SHORT, TP->query_prop("_live_s_extra_short_old"));
    tell_room(ENV(TP), QCIMIE(TP, PL_MIA) + " przestaje wspina� si� na drzewo.\n", TP);
    return 0;
}

void
koniec_wspinaczki(object player)
{
    player->add_prop(LIVE_S_EXTRA_SHORT, player->query_prop("_live_s_extra_short_old"));
    player->catch_msg("Ju� po chwili oceniasz, �e nie dasz rady si� tam wspi��.\n");
    tell_room(ENV(player), QCIMIE(player, PL_MIA) + " spostrzeg�szy, �e drzewo jest "+
         "jak dla nie"+player->koncowka("go","j")+" zbyt du�ym "+
	 "wyzwaniem zmienia swoj� decyzj� i przestaje si� wspina�.\n", player);
    player->add_fatigue(-30);
    player->increase_ss(SS_CLIMB,EXP_WSPINAM_NIEUDANY_CLIMB);
	player->increase_ss(SS_STR,EXP_WSPINAM_NIEUDANY_CLIMB_STR);
    player->increase_ss(SS_DEX,EXP_WSPINAM_NIEUDANY_CLIMB_DEX);
	player->increase_ss(SS_CON,EXP_WSPINAM_NIEUDANY_CLIMB_CON);
    
    remove_object();
}
