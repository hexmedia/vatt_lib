#pragma strict_types
#include <exp.h>
#include <ss_types.h>
#include <macros.h>
#include <pl.h>
#include <stdproperties.h>

inherit "/std/paralyze";

object drzewo;

void
create_paralyze()
{
    set_name("wspinasie");
    set_stop_verb("przesta�");
    set_stop_message("Przestajesz wspina^c si^e na drzewo.\n");
    set_fail_message("Jeste� teraz zaj�t"+TP->koncowka("y","a")+
                     " wspinaniem si� na drzewo. Musisz wpisa� "+
                "'przesta�' by m�c zrobi�, to co chcesz.\n");
    set_finish_object(TO);
    set_finish_fun("koniec_wspinaczki");
    set_stop_object(TO);
    set_stop_fun("stop_wspinaczka");
    set_remove_time(8);
    setuid();
    seteuid(getuid());

	add_allowed("'");
	add_allowed("powiedz");
}

int
stop_wspinaczka(string str)
{
    if (str) {
        return 1;
    }
    TP->add_prop(LIVE_S_EXTRA_SHORT, TP->query_prop("_live_s_extra_short_old"));
    tell_room(ENV(TP), QCIMIE(TP, PL_MIA) + " przestaje wspina� si� na drzewo.\n", TP);
    return 0;
}

void
koniec_wspinaczki(object player)
{
    object nadrzewie;
    player->add_prop(LIVE_S_EXTRA_SHORT, player->query_prop("_live_s_extra_short_old"));
    if (!objectp(drzewo)) {
        player->catch_msg("Ju� po chwili oceniasz, �e nie dasz rady si� tam wspi��.\n");
        tell_room(ENV(player), QCIMIE(player, PL_MIA) + " spostrzeg�szy, �e drzewo jest "+
                "jak dla nie"+player->koncowka("go","j")+" zbyt du�ym "+
                "wyzwaniem zmienia swoj� decyzj� i przestaje si� wspina�.\n", player);
        player->add_fatigue(-30);
        remove_object();
        return;
    }
    if (drzewo->query_sciete()) {
        player->catch_msg("Rezygnujesz ze wspinania si� na �ci�te drzewo.\n");
        tell_room(ENV(player), QCIMIE(player, PL_MIA) + " rezygnuje ze wspinania si� na " +
                "�ci�te drzewo.\n", player);
        player->add_fatigue(-15);
        remove_object();
        return;
    }
    player->catch_msg("Ko^nczysz wspinaczk^e.\n");
    
    saybb(QCIMIE(player,PL_MIA)+" ko^nczy wspinaczk^e.\n");
    //tell_room(ENV(player), QCIMIE(player,PL_MIA)+" ko^nczy wspinaczk^e.\n", player);
    player->add_fatigue(-25);
    
    player->increase_ss(SS_CLIMB,EXP_WSPINAM_UDANY_CLIMB);
	player->increase_ss(SS_STR,EXP_WSPINAM_UDANY_CLIMB_STR);
    player->increase_ss(SS_DEX,EXP_WSPINAM_UDANY_CLIMB_DEX);
	player->increase_ss(SS_CON,EXP_WSPINAM_UDANY_CLIMB_CON);
    
    nadrzewie = drzewo->query_na_drzewie();
    if (!objectp(nadrzewie)) {
        nadrzewie = clone_object("/std/drzewo/nadrzewie");
        nadrzewie->set_drzewo(drzewo);
        drzewo->set_na_drzewie(nadrzewie);
    }
	
	object room = ENV(player);
    player->move_living("M", nadrzewie);
	room->add_prop(ROOM_AS_DIR, ({" w stron� "+drzewo->short(PL_DOP), player->query_rasa(PL_DOP)}));
    
    saybb(QCIMIE(player,PL_MIA)+" przybywa z do�u.\n");
    //tell_room(ENV(player), QCIMIE(player,PL_MIA)+" przybywa z do�u.\n", player);
    
    remove_object();
}

void
set_drzewo(object ob)
{
    drzewo = ob;
}

object
query_drzewo()
{
    return drzewo;
}
