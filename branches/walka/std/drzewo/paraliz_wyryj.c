inherit "/std/paralyze";

#include <macros.h>

object drzewo;
string napis, autor;

void wyryty();

void create_paralyze()
{
	set_name("scinka_paraliz_wyryj");
	set_stop_verb("przesta�");
	set_stop_message("Przestajesz ry� napis.\n");
	set_fail_message("Teraz ryjesz napis, aby zrobi� co� innego wystarczy przesta�.\n");
	set_finish_object(this_object());
	set_finish_fun("wyryty");
	set_remove_time(random(12)+9);
	

	add_allowed("'");
	add_allowed("powiedz");

	setuid();
        seteuid(getuid());
}

void wyryty()
{
	ENV(TO)->catch_msg("Na drzewie uda�o ci si� wyry� napis: "+napis+".\n");
	drzewo->ustaw_napis(napis);
	drzewo->ustaw_autor_napisu(autor);
}

void przechowaj(string str1, string str2, object obj)
{
	napis = str1;
	autor = str2;
	drzewo = obj;
}