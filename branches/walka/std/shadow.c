/*
 * /std/shadow.c
 *
 * This is a generic shadow object.
 */

#pragma save_binary
#pragma strict_types

inherit "/std/callout";

#include "/sys/macros.h"

static object	shadow_who;	/* Which object are we shadowing */

/**
 * Wywo�ywane przez /std/player_sec do za�adowania auto shado��w.
 * @param arg Nie wiem po co.. jakie� mo�liwe argumenty, nigdzie nie u�ywane.
 */
public void
autoload_shadow(mixed arg)
{
    if (!objectp(shadow_who))
    {
	shadow_who = previous_object();
	shadow(shadow_who, 1);
    }
}

/**
 * Sprawdza czy na obiekt jest ju� na�o�ony shadow.
 * @param fname Nazwa pliku shadowa
 * @param ob Sprawdzany obiekt.
 * @return 0/1 nie jest na�o�ony/jest na�o�ony
 */
static int
shadow_double(string fname, object ob)
{
    while (objectp(ob = shadow(ob, 0)))
    {
        if (fname == MASTER_OB(ob))
        {
            return 1;
        }
    }

    return 0;
}

/**
 * Usuwa zar�wno obiekt jak i shadow na niego na�o�ony.
 */
public void 
remove_object() 
{ 
    if (objectp(shadow_who))
    {
        shadow_who->remove_object();
    }

    destruct();
}

/**
 * Usuwa shadow nie usuwaj�c przy tym obiektu.
 */
public void 
remove_shadow()
{
    destruct();
}

/**
 * Funkcja wywo�ywana w celu na�o�enia na obiekt shadowa.
 * Je�li jako argument podamy imi� gracza, na gracza na�o�ony
 * zostanie shadow. Je�eli podamy obiekt to shadow zostanie
 * na�o�ony na ten obiekt. W przypadku gdy nie podamy nic
 * shadow zostanie na�o�ony na previous_object().
 * @param to_shadow imi� gracza, obiekt do zashado�owania lub 0.
 * @return 0/1 nie na�o�ony/na�o�ony
 */
public varargs int
shadow_me(mixed to_shadow)
{
    if (stringp(to_shadow))
    {
	to_shadow = find_player(to_shadow);
    }
    
    if (!objectp(to_shadow))
    {
	to_shadow = previous_object();
    }

    if ((!objectp(shadow_who)) &&
	(!shadow_double(MASTER, to_shadow)))
    {
	if (shadow(to_shadow, 1))
	{
	    shadow_who = to_shadow;
	    return 1;
	}
    }
    return 0;
}

/**
 * @return Obiekt, na kt�ry na�o�ony jest shadow.
 */
public object
query_shadow_who()
{
    return shadow_who;
}

/**
 * Sprawdza czy funkcja istnieje w shadole.
 * @param func_name Nazwa funkcji
 * @return 0/1 nie istnieje/istnieje
 */
int
function_exists_in_shadow(string func_name)
{
    if(function_exists(func_name, TO))
        return 1;
}

/**
 * @return Zawsze 1, w ka�dym obiekcie z shadowem.
 */
nomask int
is_shadowed()
{
    return 1;
}