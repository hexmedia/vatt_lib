/*
 * /std/plan_pogodowy.c
 *
 * Plik zawieraj�cy prototyp planu pogodowego
 */

#pragma save_binary
#pragma strict_types

#include <pogoda.h>;

private static int zachmurzenie; /* stopie� zachmurzenia */
private static int opady; /* stopie� i rodzaj opad�w */
private static int wiatry; /* stopie� wietrzno�ci */
private static int temperatura; /* temperatura */
private static int ilosc_sniegu; /* ile na lokacji jest �niegu */
private static int ilosc_wody; /* ile na lokacji jest wody (tej napadanej) */

int co_pada();

/*
 * Nazwa funkcji: init_pogoda
 * Opis         : Funkcja initializuje plan pogodowy
 */

void
init_pogoda()
{
    object warunki = POGODA_OBJECT->get_warunki();
	zachmurzenie = warunki->get_zachmurzenie();
	opady = warunki->get_opady();
	wiatry = warunki->get_wiatr();
	temperatura = warunki->get_temperatura();
}

/*
 * Nazwa funkcji: short
 * Opis         : Funkcja zwraca opis obiektu.
 * Zwraca       : string - opis obiektu
 */

string
short()
{
	return "standardowy plan pogodowy";
}

void
update_pogoda()
{
    init_pogoda(); // pobieramy warunki pogodowe z rl
    ilosc_wody = ilosc_sniegu = 0;
    switch (co_pada()) {
        case PADA_DESZCZ: ilosc_wody = 1;
                          break;
        case PADA_SNIEG:
        case PADA_GRAD: ilosc_sniegu = 1;
                        break;
    }
}

/*
 * Nazwa funkcji: next_step
 * Opis         : Funkcja zmienia stan planu pogodowego oraz zwraca ilo�� sekund, kiedy
 *                ma nast�pi� kolejna zmiana.
 * Zwraca       : int - ilo�� sekund do kolejnej zmiany
 */

int
next_step()
{
    update_pogoda();
	return 120 + random(180);
}

/*
 * Nazwa funkcji: set_zachmurzenie
 * Opis         : Funkcja ustawia zachmurzenie w danym obiekcie.
 * Argumenty    : int zach - nowe zachmurzenie
 */

void
set_zachmurzenie(int zach)
{
	zachmurzenie = zach;
}

/*
 * Nazwa funkcji: set_opady
 * Opis         : Funkcja ustawia opady w danym obiekcie.
 * Argumenty    : int opad - nowe opady
 */

void
set_opady(int opad)
{
	opady = opad;
}

/*
 * Nazwa funkcji: set_wiatry
 * Opis         : Funkcja ustawia wiatry w danym obiekcie.
 * Argumenty    : int wiatr - nowe wiatry
 */

void
set_wiatry(int wiatr)
{
	wiatry = wiatr;
}

/*
 * Nazwa funkcji: set_temperatura
 * Opis         : Funkcja ustawia temperatur� w danym obiekcie.
 * Argumenty    : int temp - nowa temperatura
 */

void
set_temperatura(int temp)
{
	temperatura = temp;
}

/*
 * Nazwa funkcji: get_zachmurzenie
 * Opis         : Funkcja zwraca zachmurzenie w danym obiekcie.
 * Zwraca       : int - zachmurzenie
 */

int
get_zachmurzenie()
{
	return zachmurzenie;
}

/*
 * Nazwa funkcji: get_opady
 * Opis         : Funkcja zwraca opady w danym obiekcie.
 * Zwraca       : int - opady
 */

int
get_opady()
{
	return opady;
}

/*
 * Nazwa funkcji: get_wiatry
 * Opis         : Funkcja zwraca wiatry w danym obiekcie.
 * Zwraca       : int - wiatry
 */

int
get_wiatry()
{
	return wiatry;
}

/*
 * Nazwa funkcji: get_temperatura
 * Opis         : Funkcja zwraca temperatur� w danym obiekcie.
 * Zwraca       : int - temperatura
 */

int
get_temperatura()
{
	return temperatura;
}

/**
 * Zwraca opis eventa zwi�zanego z zachmurzeniem.
 *
 * @param zachmurzenie wielko�� zachmurzenia
 */

string
event_zachmurzenie(int zachmurzenie)
{
    return 0;
}

/**
 * Zwraca opis eventa zwi�zanego z opadami.
 *
 * @param opady rodzaj i wielko�� opad�w
 */

string
event_opady(int opady)
{
    return 0;
}

/**
 * Zwraca opis eventa zwi�zanego z wiatrem.
 *
 * @param wiatr si�a wiatru
 */

string
event_wiatr(int wiatr)
{
    return 0;
}

/**
 * Zwraca opis eventa zwi�zanego z temperatur�.
 *
 * @param temperatura wielko�� temperatury
 */

string
event_temperatura(int temperatura)
{
    return 0;
}

/**
 * Zwraca informacj� o tym, jaki jest typ opad�w.
 *
 * @return typ opad�w
 */

int
co_pada()
{
  switch(get_opady()) {
    case POG_OP_D_L:
    case POG_OP_D_S:
    case POG_OP_D_C:
    case POG_OP_D_O:
      return PADA_DESZCZ;
    case POG_OP_S_L:
    case POG_OP_S_S:
    case POG_OP_S_C:
      return PADA_SNIEG;
    case POG_OP_G_L:
    case POG_OP_G_S:
      return PADA_GRAD;
    default: return NIC_NIE_PADA;
  }
}

/**
 * Zwraca informacj� o tym, jak mocno pada.
 *
 * @return moc opad�w
 */

int
moc_opadow()
{
  switch (get_opady()) {
    case POG_OP_D_L:
    case POG_OP_S_L:
    case POG_OP_G_L:
      return PADA_LEKKO;
    case POG_OP_D_S:
    case POG_OP_S_S:
    case POG_OP_G_S:
      return PADA_SREDNIO;
    case POG_OP_D_C:
    case POG_OP_S_C:
      return PADA_MOCNO;
    case POG_OP_D_O:
      return PADA_BMOCNO;
    default: return NIC_NIE_PADA;
  }
}

int
get_ilosc_wody()
{
    return ilosc_wody;
}

int
get_ilosc_sniegu()
{
    return ilosc_sniegu;
}
