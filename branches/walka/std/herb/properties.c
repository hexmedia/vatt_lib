#include <files.h>
#include <stdproperties.h>

int      amount,                      //Warto�� od�ywcza zio�a
         rosnie,                      //Prawda je�li zio�o jeszcze ro�nie.
         decayed,                     //Zapamietuje czy zio�o jest uschni�te
         trudnosc,                    //Trudno�� zidentyfikowania zio�a
         trudnosc_znalezienia,        //Trudno�� znalezienia zio�a
         czestosc,                    //Cz�sto�� wyst�powania zio�a
        *sposoby,                     //Sposoby przygotowania zio�a TODO:check
         efekty_palenia,              //Efekt palenia zio�a w fajce(pod palenie)
         sposob_wystepowania,         //Spos�b wyst�powania zio�a(w grupach, samotnie(itp.))
         ilosc_w_calosci,             //Ilosc zi� w ca�ym ziele.
         pora_roku;                   //Pora roku w kt�rej zio�o wyst�puje
float    decay_time_sch,              //Czas potrzebny aby zio�o usch�o
         decay_time_sch_z,            //Czas, kt�ry pozosta�, do uschni�cia zio�a
         decay_time_nis,              //Czas potrzebny aby uschni�te zio�o si� rozsypa�o
         decay_time_nis_z;            //Czas, kt�ry pozosta�, do rozsypania si� zio�a
mixed    slowo_grupy;                 //S�owo u�ywane zamiast wiele w rosn�cych zio�ach.
mapping  komendy = ([]),              //Komendy oddzia�ywuj�ce na zio�o nieprzetworzone ?? TODO
         komendy_zbierania = ([]),    //Komendy u�ywane do zebrania zio�a.
         trudnosc_przyg = ([]),       //Trudno�� przygotowania mikstury z zio�a
         sposoby_uzycia = ([]),       //Sposoby u�ycia zio�a
         kolory = ([]),               //Kolory mikstury powsta�ej z zio�a
         efekty = ([]),               //Efekty dzia�ania nieprzygotowanego zio�a
         efekty_przyg = ([]);         //Efekt dzia�ania mikstury powsta�ej z zio�a.


/**
 * Ustawia trudno�� znalezienia zio�a, im zio�o wieksze tym �atwiej powinno by�
 * je znale��, dodatkowym sprawdzeniem umiej�tno�ci gracza b�dzie komenda
 * pozwalaj�ca zio�o zdoby�.
 * @param t trudno�� znalezienia(1-100) 
 */
void
ustaw_trudnosc_znalezienia(int t)
{
    trudnosc_znalezienia = max(1, min(100, t));
}

/**
 * @return Trudno�� znalezienia zio�a
 */
int
query_trudnosc_znalezenia()
{
    return trudnosc_znalezienia;
}

/**
 * Funkcja pozwala ustawi� pore roku w jakiej zio�o b�dzie mo�na znale��.
 * @param p pora roku z mudtime.h
 */
void
ustaw_pore_roku(int p)
{
    pora_roku = p;
}

/**
 * Zwraca por� roku w jakiej zio�o b�dzie ros�o.
 */
int
query_pora_roku()
{
    return pora_roku;
}

/**
 * Pozwala ustawi� ilo�� poszczeg�lnych zi�lek w ca�ym ziele
 * @param i ilo�� zi�
 */
void
ustaw_ilosc_w_calosci(int i)
{
    ilosc_w_calosci = i;
}

/**
 * @return Zwraca ilo�� poszczeg�lnych pojedynczych zi�ek w 
 * ca�ym ziele.
 */
int
query_ilosc_w_calosci()
{
    return ilosc_w_calosci;
}

/**
 * Ustawia komendy kt�rymi zio�o b�dzie zbierane.
 * @param cmds mapping z komendami w formie ([komenda : ({1osoba, 2osoba, bezokolicznik, szansa(opt)})])
 */
void
ustaw_komendy_zbierania(mapping cmds)
{
    foreach(string cmd : m_indexes(cmds))
    {
        if(sizeof(cmds < 3))
            throw("Poda�e� za ma�o argument�w do komendy zbierania.\n");
        if(!pointerp(cmds[cmd]))
            continue;

        if(sizeof(cmds[cmd]) == 3)
            cmds += ({100});
        if(!intp(cmds[cmd][3]))
            cmds[3] = 100;
    }

    komendy_zbierania = cmds;
}

/**
 * Dodaje komende zbierania do tych ju� istniej�cych
 * @param komenda komenda,
 * @param pos     pierwsza osoba,
 * @param dos     druga osoba,
 * @param bezoko  bezokolicznik,
 * @param sznasa  szansa na udane zdobycie zio�a.
 */
void
dodaj_komende_zbierania(string komenda, string pos, string dos, string bezokol, int szansa = 100)
{
    komendy_zbierania[komenda] = ({pos, dos, bezokol, szansa});
}

/**
 * @return Zwraca mapping z komendami zbierania zi�.
 */
mapping
query_komendy_zbierania()
{
    return komendy_zbierania;
}

/**
 * Ustawia �e zio�o ro�nie.
 */
void
ustaw_ze_rosnie(int i)
{
    rosnie = i;
    if(i)
    {
        add_prop(OBJ_M_NO_GET, "We� co?");
        set_no_show();
        stop_decay();
    }
    if(!i)
    {
        unset_no_show();
        set_no_show_composite(0);
        remove_prop(OBJ_M_NO_GET);
        start_decay();
    }
}

/**
 * @return Sprawdza czy zio�o ro�nie.
 */
public int
query_rosnie()
{
    return rosnie;
}

/**
 * Ustawia w jaki spos�b zio�o b�dzie wyst�powa�o.
 */
void
ustaw_sposob_wystepowania(int s)
{
    sposob_wystepowania = s;
}

/**
 * @return Spos�b w jaki zio�o b�dzie wyst�powa�o na lokacjach.
 */
int
query_sposob_wystepowania()
{
    return sposob_wystepowania;
}

/**
 * Efekty wywolywane na graczu kiedy ten pali ziolo.
 * Cpuny wszystkich krajow laczcie sie.
 * @param e Efekt lub tablica efekt�w
 * @param i Intensywnosc b�d� tablica z intensywno�ci�.
 * FIXME: Trzeba to zrobi� ale narazie palenia nie ma wie nie ruszam
 *        Funkcji prosze nie u�ywa� to tylko przygot�wka pod fajki.
 */
void
ustaw_efekt_palenia(int e)
{
    efekty_palenia = e;
}

/**
 * Zwraca efekty wywolywane na graczu kiedy
 * ten pali zio�o.
 */
int
query_efekt_palenia()
{
   return efekty_palenia;
}

public mixed
query_prop(string prop)
{
    if(prop == OBJ_I_DONT_GLANCE && rosnie)
        return 1;
    else 
        return ::query_prop(prop);
}

/**
 * Ustawia czas potrzebny na zepsucie si� zio�a. Czas podajemy w minutach.
 * @param sch Czas do ususzenia si� zio�a(ustawiena przymiotnikow_zepsutego)
 * @param nis Czas potrzebny do calkowitego rozpadni�cia si� zio�a, liczony od momentu kiedy zio�o si� ususzy.
 */
public varargs void
ustaw_czas_psucia(int sch, int nis)
{
    decay_time_sch = itof(sch) * 60.0;
    decay_time_nis = itof(nis) * 60.0;
    decay_time_nis_z = decay_time_nis;
    decay_time_sch_z = decay_time_sch;
}

/**
 * czas psucia sie ziola
 */
public mixed
query_czas_psucia(int arg)
{
    if(arg)
        return ({decay_time_sch, decay_time_nis});
    else if(decayed)
        return decay_time_nis;
    else 
        return  decay_time_sch;
}

/**
 * Zwraca pozostaly czas psucia sie zio�a
 */
public mixed
query_pozostaly_czas_psucia(int arg)
{
    if(arg)
        return ({decay_time_sch_z, decay_time_nis_z});
    else if(decayed)
        return decay_time_nis_z;
    else
        return decay_time_sch_z;
}

/**
 * Sprawdza czy zio�o jest zepsute
 */
int
query_zepsute()
{
    return decayed;
}

int
query_decayed()
{
    return decayed;
}

/**
 * Ustawia poziom trudnosci identyfikacji ziola.
 * min - 0, max - 100.
 */
void
ustaw_trudnosc_identyfikacji(int t)
{
    trudnosc = max(0, min(100, t));
}

/**
 * Zwraca trudnosc identyfikacji ziola
 */
int
query_trudnosc_identyfikacji()
{
    return trudnosc;
}

/**
 * Ustawia czestosc wystepowania ziola.
 * min - 1 , max - 100
 */
void
ustaw_czestosc_wystepowania(int c)
{
    czestosc = max(1, min(100, c));
}

/**
 * Zwraca czestosc wystepowania  ziola.
 */
int
query_czestosc_wystepowania()
{
    return czestosc;
}

/**
 * Ustawia komendy, ktore gracz moze wykonac na ziole, zanim zostalo ono przerobione na mixturke,
 * komendy dotycza takze ziol ususzonych(query_decayed()==1)
 * @param cmds Mapping z komendami w formie:
 *     <i>(["pow�chaj" : ({"w�chasz", "w�cha", "w�cha�", 1/0})])</i>
 *     ostatni arg okre�la czy po wykonaniu komendy zio�o ma zosta� w i gracza(1) czy te� ma zosta� zniszczone(0).
 */
public void
ustaw_komendy(mapping cmds)
{
    komendy = cmds;
}

/**
 * Ustawia po�ywno�� zio�a
 * @param a po�ywno��
 */
void
set_amount(int a)
{
    amount = a;
}

/**
 * @param k Komenda ktora chcemy podejrzec.
 * @return Zwraca tablice z:
 * [0] czasownikiem w 2os.
 * [1] czasownikiem w 3os.
 * [2] bezokolicznikiem
 */
string *
query_komneda(string k)
{
    return komendy[k];
}

/**
 * Ustawiamy sposoby w jaki ziolo mozna przygotowac...
 * @param sp tablica ze sposobami przygotowania
 * @param t  Trusnosc przygotowania.
 */
public void
ustaw_sposoby_przygotowania(int *sp)
{
    sposoby = sp;
}

/**
 * Zwraca sposoby jakimi ziolo moze zostac przygotowane(przerobione na mixture)
 */
int *
query_sposoby_przygotowania()
{
    return sposoby;
}

/**
 * Ustawia trudno�� przygotowania zio�a w dany spos�b
 * @param sp spos�b przygotowania
 * @param t trudno�� przygotowania
 */
public void
ustaw_trudnosc_przygotowania(int sp, int t)
{
    trudnosc_przyg[sp] = max(1, min(100, t));
}

/**
 * Zwraca trudnosc przygotowania ziola.
 * @param sp - sposob przygotowania
 */
int
query_trudnosc_przygotowania(int sp)
{
    return trudnosc_przyg[sp];
}

/**
 * Ustawia mozliwe sposoby uzycia ziola, po danym przygotowaniu
 * @param sp - Sposob przygotowania
 * @param spu Sposoby uzycia.
 */
void
ustaw_sposob_uzycia(int sp, int spu)
{
    if(mappingp(sposoby_uzycia))
        sposoby_uzycia += ([sp:spu]);
    else
        sposoby_uzycia = ([sp:spu]);
}

/**
 * Zwraca mozliwe sposoby uzycia ziola po danym przygotowaniu
 * @param sp Sposob przygotowania
 */
int
query_sposob_uzycia(int sp)
{
    return sposoby_uzycia[sp];
}

/**
 * Ustawia kolor, jaki bedzie nadawany miksturze przez to ziolo, po danym sposobie przygotowania.
 * @param sp Sposob przygotowania
 * @param kol Kolor w rgb
 * @param in Intensywnosc koloru
 */
void
ustaw_kolor(int sp, int *kol, int in)
{
    if(sizeof(kol)!=3)
    {
        throw("Do ziola podano kolor w formacie nie RGB.");
        return;
    }
    if(mappingp(kolory))
        kolory += ([sp:({kol, in})]);
    else
        kolory = ([sp:({kol, in})]);
}

/**
 * Zwraca kolor jaki bedzie przekazywany miksturze po danym sposobie przygotowania
 * @param sp Sposob przygotowania
 */
int *
query_kolor(int sp)
{
    return kolory[sp];
}

/**
 * Ustawia efekt wywolywany na graczu kiedy ten zrobi cos z nieprzetworzonym ziolem.
 * @param cmd Komend ktora wywoluje sie efekt
 * @param eff Effekt
 * @param in  Intensywnosc efektu(0-100)
 */
void
ustaw_efekt(string cmd, int eff, int in, int zn = 0)
{
    in = max(-100, min(100, in));
    if(mappingp(efekty))
        efekty += ([cmd:({eff, in, zn})]);
    else
        efekty = ([cmd:({eff, in, zn})]);
}

/**
 * Zwraca efekt wywolywany na graczu kiedy ten zrobi coz z ziolem za pomoca podanej komendy
 * @param cmd Komenda
 */
int *
query_efekty(string cmd)
{
    return map(efekty[cmd], &operator([])(0));
}
/**
 * Zwraca intensywnosc efektu.
 * @param cmd komenda ktora wuwoluje sie efekt
 */
int
query_intensywnosc_efektu(string cmd, int eff)
{
    return efekty[cmd];
}

/**
 * Ustawia efekty wywolywane na graczu po zjedzeniu ziola krore zostalo juz przetworzone
 * @param sp Sposob przygotowania
 * @param eff Efekt jaki wywoluje na graczu ziolo przygotowane w dany sposob lub tablica z efektami
 * @param in Tablica z intensywnoscia poszczegolnych efektow. Podawane w tablicy w kolejnosci w jakiej podalismy efekty. Jesli nie ustawimy wartosci efektu to zostanie on ustawiony na wartosc domyslna = 20.
 */
void
ustaw_efekt_przygotowanego(int sp, mixed eff, mixed in)
{
    int i, s;

    // Jesli eff jest int'em to przerabiamy go na tablice
    if(intp(eff))
    {
        eff = ({eff});
    }
    else if(!pointerp(eff))
        eff = ({});

    // Jesli in jest int'em to przerabiamy go na tablice
    if(intp(in))
    {
        in = ({in});
    }
    else if(!pointerp(eff))
        in = ({});

    // Jesli tablica in jest mniejsza od tablicy eff to tablice in najpierw wyrownujemy do tablicy
    // eff, a pozniej w nowopowstalych elementach ustawiamy wartosc domyslna(20)
    if(sizeof(in) < sizeof(eff))
    {
        s = sizeof(in);
        for(i=s-1;i<sizeof(eff);i++)
            in += ({20});
        for(i=0;i<s;i++)
            in = max(-100, min(100, in[i]));
    }
    efekty_przyg += ([sp : ({eff, in})]);
}
/**
 * Zwraca intensywnosc efektu.
 * @param sp Sposob przygotowania ziola
 * @param eff Efekt, ktorego intensywnosc ustawiamy
 */
int
query_int_efektu_przygotowanego(int sp, int eff)
{
    int m = member_array(eff, efekty_przyg[sp][0]);

    if(!m)
        return 0;

    return efekty_przyg[sp][1][m];
}

/**
 * Ustawia s�owo kt�re b�dzie pokazywane w rosn�cych zio�ach
 * zamiast 'wiele'.
 * @param sl S�owo wy�wietlane zamiast wiele, w pe�nej odmnianie.
 * @param il Ilo�� zi� po jakich wy�wietlane b�dzie to s�owo.
 * @return 1 je�li sukces
 */
public int
ustaw_slowo_grupy(mixed sl, int il)
{
    mixed odmiana;

    if(!pointerp(sl))
    {
        if(stringp(sl))
        {
            odmiana = SLOWNIK->query_odmiana(sl);
            sl = odmiana[0];
        }
        else
            return 0;
    }
    slowo_grupy = ({sl, il});
    return 1;
}

/**
 * Zwraca s�owo wy�wietlane w rosn�cych zio�ach zamiast wiele.
 * @param arg Czy wy�wietli� s�owo(0), czy ilo�� zi� aby wy�wietli� to s�owo(2), czy ca�� tabli�(!=2&&!=0)
 */
mixed
query_slowo_grupy(int arg)
{
   if(!slowo_grupy)
       return 0;
   if(arg == 2)
       return slowo_grupy[1];
   else if(arg)
       return slowo_grupy;
   else
       return slowo_grupy[0];
}

/**
 * Zio�o jest jedzeniem wi�c query_food musi zwr�ci� 1.
 * @return 1-zawsze
 */
int
query_food()
{
    return 1;
}

/**
 * @return Zawsze 1 bo w ko�cu na std zio�a nikt nie buduje dom�w.
 */
int
query_herb()
{
    return 1;
}

