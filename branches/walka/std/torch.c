/*
 * /std/torch.c
 *
 * The standard torch code.
 */

#pragma save_binary
#pragma strict_types

inherit "/std/holdable_object";
 
#include <object_types.h>
#include <cmdparse.h>
#include <composite.h>
#include <files.h>
#include <macros.h>
#include <stdproperties.h>
 
/*
 * Prototypes:
 */
public void set_strength(int strength);
public void set_value(int value);
public void set_time(int time);
public int torch_value();
public void burned_out();

/*
 * Global variables:
 */
private int Torch_Value,	/* The max value of the torch. */
	    Light_Strength,	/* How strongly the 'torch' will shine */
	    Time_Left;		/* How much time is left? */
static  int Burn_Alarm,		/* Alarm used when torch is lit */
	    Max_Time,		/* How much is the max time (start time) */
	    jakis_alarm;	// alarm na gasniecie po opuszczeniu
private object *Fail;





//Lil.
nomask void
configure_light()
{
 //  this_object()->add_prop(OBJ_I_VALUE,  light_value);
    this_object()->add_prop(OBJ_I_VALUE,  torch_value);
}





/*
 * Function name: create_torch
 * Description:   The standard create. This has some standard settings on
 *		  long and short descriptions, but don't be afraid to change
 *		  them.
 *                The pshort has to be set to some value, otherwise the short
 *                description of several objects will look ugly.
 */
void
create_torch()
{
    ustaw_nazwe("pochodnia");
}

/*
 * Function name: create_object
 * Description:   The standard create routine.
 */
nomask void
create_holdable_object()
{
    set_long("Kr�tki palik i zlepek nasi�kni�tej na czubku tkaniny "+
             "tworzy t� zwyk�a, prost� pochodni�.\n");

    add_prop(OBJ_I_LIGHT,     0);
    add_prop(OBJ_I_WEIGHT,  700);
    add_prop(OBJ_I_VOLUME, 1000);
    add_prop(OBJ_I_VALUE,  torch_value);

    set_time(1700 +random(100));
    set_strength(1);
    set_value(13);

    set_type(O_POCHODNIE);

    create_torch();
}

/*
 * Function name: reset_torch
 * Description  : Since you may not mask reset_object() in torches, you have
 *                to redefine this function.
 */
public void
reset_torch()
{
}

/*
 * Function name: reset_object
 * Description:   Reset the object. Since this function is nomasked, you
 *                must redefine reset_torch() to make the torch reset.
 */
public nomask void
reset_holdable_object()
{
    reset_torch();
}

 
/*
 * Function name: torch_value
 * Description:   A VBFC gets here when someone wants to know the value of
 *		  the value of this object, default setting
 * Returns:	  The value
 */
public int
torch_value()
{
    int v;

    if (!Max_Time)
    	return 0;

    if (Burn_Alarm && sizeof(get_alarm(Burn_Alarm)))
	v = ftoi(get_alarm(Burn_Alarm)[2]);
    else
	v = Time_Left;

    return (v * (Torch_Value - 5)) / Max_Time;
}

/*
 * Function name: init
 * Description:   Here we add some commands to the player.
 */
public void
init()
{
    ::init();

    add_action("pomoc", "?", 2);
}


public int
pomoc(string str)
{                                                                              
    object pohf;

    notify_fail("Nie ma pomocy na ten temat.\n");

    if (!str) return 0;

    if (!parse_command(str, this_player(), "%o:" + PL_MIA, pohf))
        return 0;

    if (pohf != this_object())
        return 0;

	write("Nie baw si� ogniem!\n");
        return 1;
}

/*
 * Function name: set_time
 * Description:   Set how long time the torch can burn.
 */
public void
set_time(int time)
{
    Max_Time = time;
    Time_Left = time;
}

/*
 * Function name: query_max_time
 * Description:	  Query the original burn time of the torch
 * Returns:       the time
 */
public int
query_max_time()
{
    return Max_Time;
}

/*
 * Function name: query_time
 * Description:	  Query how long time the torch can burn
 * Argument:      flag: if true, then return the time until the
 *                torch burns out if the torch is lit
 * Returns:       the time left
 */
public int
query_time(int flag = 0)
{
    mixed   alarm;

    if (flag && Burn_Alarm && sizeof(alarm = get_alarm(Burn_Alarm)))
	return ftoi(alarm[2]);
    return Time_Left;
}

/*
 * Function name: query_lit
 * Description:   Query of the torch is lit.
 * Argument:      flag - if set, return id of the alarm to the function
 *                that is called when the torch burns out.
 * Returns:       0        - if torch is not lit,
 *                -1       - if torch is lit,
 *                alarm id - if torch is lit and flag was set.
 */
public int
query_lit(int flag)
{
    if (flag)
	return Burn_Alarm;
    else
	return (!Burn_Alarm ? 0 : -1);
}

/*
 * Function name: set_time_left
 * Description:   Set how long time the torch can burn.
 *                Use this for 'torches' that can be refilled, like oil lamps.
 * Arguments    : int left - the time left.
 */
public void
set_time_left(int left)
{
    Time_Left = ((left < Max_Time) ? left : Max_Time);

    /* If lit, then also update the alarm. */
    if (Burn_Alarm)
    {
	remove_alarm(Burn_Alarm);
	Burn_Alarm = set_alarm(itof(Time_Left), 0.0, burned_out);
    }
}

/*
 * Function name: set_strength
 * Description:   Set the light strength of this 'torch'
 */
public void
set_strength(int strength)
{
    Light_Strength = strength;
}

/*
 * Function name: query_strength
 * Description:   Query how strongly the torch will shine
 */
public int
query_strength()
{
    return Light_Strength;
}

/*
 * Function name: set_value
 * Description:   Set the max value of the torch
 */
public void
set_value(int value)
{
    Torch_Value = value;
}

public mixed
light_me()
{
    if (environment(this_player())->query_prop(ROOM_I_TYPE) ==
            ROOM_UNDER_WATER)
    {
        return "Jeste� w tej chwili pod wod�.\n";
    }

    if (!Time_Left)
        return "Pr^obujesz zapali^c " + short(PL_BIE) + ", ale ci si^e nie "+
            "udaje... " + (query_tylko_mn() ? "s^a" : "jest") +
            " bezu^zyteczn" + koncowka("y", "a", "e", "i", "e") + ".\n";

    if (Burn_Alarm)
        return capitalize(short(PL_MIA)) + " ju^z " + 
            (query_tylko_mn() ? "s^a" : "jest") + " zapal" + 
            koncowka("ony", "ona", "one", "eni", "one") + ".\n";

    //Tak trzeba, bo lampy mozna zapalic majac je np. przypiete gdzies
    if (!this_object()->query_wielded() && this_object()->query_name() != "lampa")
    {
        return "Musisz wpierw chwyci^c " + koncowka("ten", "t^a", "to") +
                " " + query_nazwa(PL_BIE) + ".\n";
    }

    if (!sizeof(filter(all_inventory(this_player()), &->query_krzemien())))
    {
        return "Nie masz czym wykrzesa^c ognia.\n";
    }

	saybb(QCIMIE(this_player(), PL_MIA) + " zapala " +
            QSHORT(this_object(), PL_BIE) + ".\n");
	write("Zapalasz " + short(PL_BIE) + ".\n");
	dodaj_przym("zapalony", "zapaleni");
	odmien_short();
	odmien_plural_short();

    add_prop(OBJ_I_HAS_FIRE, 1);
    add_prop(OBJ_I_LIGHT, Light_Strength);

    Burn_Alarm = set_alarm(itof(Time_Left), 0.0, burned_out);
    return 1;
}

/*
 * Function name: extinguish_me
 * Description:   Extinguish this torch.
 * Returns:       1/0 - success/failure
 */
public int
extinguish_me()
{
    mixed   alarm;

    if (!Burn_Alarm || !sizeof(get_alarm(Burn_Alarm)))
    {
	Burn_Alarm = 0;
	return capitalize(short()) + " nie " + (query_tylko_mn() ? "s^a" : 
	    "jest") + " zapal" + koncowka("ony", "ona", "one", "eni", "one") +
	    ".\n";
    }

    if (sizeof(alarm = get_alarm(Burn_Alarm)))
    {
	Time_Left = ftoi(get_alarm(Burn_Alarm)[2]);
	remove_alarm(Burn_Alarm);
    }
    Burn_Alarm = 0;

	saybb(QCIMIE(this_player(), PL_MIA) + " gasi " +
            QSHORT(this_object(), PL_BIE) + ".\n");
	write("Gasisz " + short(PL_BIE) + ".\n");
    remove_adj("zapalony");
    odmien_short();
    odmien_plural_short();
    
    remove_prop(OBJ_I_LIGHT);
    remove_prop(OBJ_I_HAS_FIRE);
    return 1;
}

/*
 * Function name: burned_out
 * Description:	  If this function is called when the torch has burned out.
 */
public void
burned_out()
{
    object ob = environment(),
	   env;

    Time_Left = 0;
    Burn_Alarm = 0;

    remove_adj("zapalony");

    if (ob)
    {
	odmien_short();
	odmien_plural_short();

	if (living(ob))
	{
	    ob->catch_msg(capitalize(short(ob, PL_MIA))
	                + " wypala si^e i ga^snie.\n");

	    if (env = environment(ob))
	        tell_roombb(env, QCSHORT(this_object(), PL_MIA) + " trzyman"
	                  + koncowka("y", "a", "e", "i", "e") + " przez "
	                  + QIMIE(ob, PL_BIE) + " wypala si^e i ga^snie.\n",
	                    ({ob}), ob);
	}
	else
	    tell_roombb(ob, QCSHORT(this_object(), PL_MIA)
	              + " wypala si^e i ga^snie.\n", ({}), this_object());
    }

    dodaj_przym("wypalony", "wypaleni");
    odmien_short();
    odmien_plural_short();

	remove_prop(OBJ_I_VALUE);
    remove_prop(OBJ_I_LIGHT);
    remove_prop(OBJ_I_HAS_FIRE);
}

/*
 * Function name: query_torch_auto_load
 * Description:   Return the auto_load string for changing tourch values
 * Returns:	  part of auto_load string
 */
public string
query_torch_auto_load()
{
    int tmp;

    if (Burn_Alarm && sizeof(get_alarm(Burn_Alarm)))
	tmp = ftoi(get_alarm(Burn_Alarm)[2]);
    else
	tmp = Time_Left;

    return "#t_t#" + tmp + "#t_l#" + query_prop(OBJ_I_LIGHT) + "#";
}

/*
 * Function name: init_torch_arg
 * Description:   Initialize the torch variables at auto_load.
 * Arguments:     arg - The auto_load string as recieved from
 *			query_torch_auto_load()
 */
public string
init_torch_arg(string arg)
{
    string foobar, toReturn;
    int tmp;

    if (arg == 0) {
        return 0;
    }

    sscanf(arg, "%s#t_t#%d#%s", foobar, Time_Left, foobar);
    sscanf(arg, "%s#t_l#%d#%s", foobar, tmp, toReturn);
    if (Time_Left == 0) {
      dodaj_przym("wypalony", "wypaleni");
      odmien_short();
      odmien_plural_short();
		remove_prop(OBJ_I_VALUE);
      return toReturn;
    }
    if (tmp > 0)
    {
        add_prop(OBJ_I_LIGHT, tmp);
        add_prop(OBJ_I_HAS_FIRE, 1);
        dodaj_przym("zapalony", "zapaleni");
        odmien_short();
        odmien_plural_short();
        Burn_Alarm = set_alarm(itof(Time_Left), 0.0, burned_out);
    }
    return toReturn;
}

/*
 * Function name: query_auto_load
 * Description:   A default query_auto_load() for torches.
 * Returns:	  A default auto_load string.
 */
public string
query_auto_load()
{
    return ::query_auto_load() + query_torch_auto_load();
}

/*
 * Function name: init_arg
 * Description:   A default init_arg() for torches.
 * Arguments:	  arg - String with variables to auto_load.
 */
public string
init_arg(string arg)
{
    return init_torch_arg(::init_arg(arg));
}

int
unwield_me()
{
    if (jakis_alarm)
	remove_alarm(jakis_alarm);

    jakis_alarm = set_alarm(5.0, 0.0, "zgasnij_no");

    return ::unwield_me();
}

varargs public int
move(mixed dest, mixed subloc, int force = 0, int test_only = 0)
{
    if (jakis_alarm)
	remove_alarm(jakis_alarm);

    jakis_alarm = set_alarm(5.0, 0.0, "zgasnij_no");

    return ::move(dest, subloc, force, test_only);
}

int
zgasnij_no()
{
    if (!query_wielded() && query_lit(1))
    {
	extinguish_me();
	tell_room(environment(this_object()), capitalize(short(PL_MIA)) + " ga^snie.\n");
    }
}

