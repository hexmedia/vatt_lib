/*
 *   Jest to standard do lamp. Napisany przez Jeremiana, Delverta i Lil.
 *
 *     Lampe mozemy przewiesic przez lewe lub prawe ramie, lub ja chwycic.
 *       I masa innych pierdol...Co tu duzo gadac, kiedy lampy to raczej 
 *        zadna rewelacja, ale nie powiem, ze bylo latwo... ;)
 *                                             Chyba skonczylismy ok. 20.09.2005
 */

#pragma strict_types

//inherit "/std/object";
//inherit "/lib/light";
inherit "/std/torch";
inherit "/lib/keep";
 
#include <cmdparse.h>
#include <composite.h>
#include <files.h>
#include <macros.h>
#include <stdproperties.h>
#include <object_types.h>

//#define LAMPA_SUBLOC    "_lampa_subloc"
#define PRAWE_RAMIE_SUBLOC	"_prawe_ramie_subloc"
#define LEWE_RAMIE_SUBLOC	"_lewe_ramie_subloc"

//mixed Gdzie_Mozna = ({ "na prawym ramieniu", "na lewym ramieniu" });
//mixed Gdzie_Mozna_Prop = ({ PRAWE_RAMIE_SUBLOC, LEWE_RAMIE_SUBLOC });

int gNieBrac = 0;
int gWhereIs = 0;

void    set_lamp_value(int ile);
string  lamp_cond_desc();

/* Ta funkcje mozna redefiniowac w klonach
 */
void
create_lamp()
{
} 

nomask void
create_torch()
{
    ustaw_nazwe("lampa");

    set_long("Lampa, kt�ra po nape�nieniu paliwem mo�e da� ci nieco �wiat�a.\n" +
	"@@opis_oleju@@");
    
    set_keep(1);

    add_prop(OBJ_I_VOLUME, 1000);
    add_prop(OBJ_I_WEIGHT, 700);
    add_prop(OBJ_I_VALUE, 60);   // wartosc samej lampy

    set_type(O_LAMPY);

//    add_my_desc(&lamp_cond_desc(), this_object());
    
    set_time(1300+random(100));    

//    set_left_time(0); // Na poczatku lampa jest pusta
    set_time_left(0);
    set_strength(4);
    configure_light();
    
    //test
    set_value(13);
    
    create_lamp();
}

string
opis_oleju()
{
    string str = capitalize(short(PL_MIA)) + " ";

    switch(100 * this_object()->query_time(1) / this_object()->query_max_time())
    {
        case 0: str += "jest zupe^lnie pust" + koncowka("y", "a", "e"); break;
        case 1..20: str += "zawiera ma^l^a ilo^s^c oleju"; break;
        case 21..50: str += "zawiera troch^e oleju"; break;
        case 51..70: str += "zawiera sporo oleju"; break;
        case 71..90: str += "jest prawie pe^ln" + koncowka("y", "a", "e") +
            " oleju"; break;
        default: str += "jest pe^ln" + koncowka("y", "a", "e") + " oleju"; break;
    }
    return str + ".\n";
}

/* Cos takiego jest w /std/torch i wtedy pochodnia odlozona
 * lub dana gasnie. Lampy nie powinny.
 */
int zgasnij_no()
{
}

void
reset_lamp()
{
}

nomask public void
reset_torch()
{
    reset_lamp();
}

void 
set_lamp_value(int value)
{
   add_prop(OBJ_I_VALUE, value);
}
 
string
show_subloc(string subloc, object on_obj, object for_obj)
{
    //if (subloc != LAMPA_SUBLOC)
    if ((subloc != LEWE_RAMIE_SUBLOC) && (subloc != PRAWE_RAMIE_SUBLOC))
        return "";
    
    if (on_obj == for_obj)
        return "Przez " + ((subloc == LEWE_RAMIE_SUBLOC) ? "lewe" : "prawe") + " rami� masz przewieszon� " + short(for_obj, PL_BIE) + ".\n";

    return "Przez " + ((subloc == LEWE_RAMIE_SUBLOC) ? "lewe" : "prawe") + " rami� ma przewieszon� " + short(for_obj, PL_BIE) + ".\n";
}

// Kontrola czy sprawdzany obiekt jest olejem, wymagane od niego posiadanie
// funkcji fill_other()
int
check_oil(object ob)
{
    if(ob->jestem_butelka_oleju() && ob->query_nazwa_plynu_dop()=="oleju")    
    return (function_exists("fill_other",ob) != 0);
      
}

// Napelnianie lampy
int
do_fill(string str)
{
    string vb = capitalize(query_verb());

    object *lampy, *lista;
    int i;
    
    if (!str)
    {
        notify_fail(vb + " co <czym>?\n");
        return 0;
    }
    
    parse_command(str, all_inventory(this_player()), "%i:"+PL_BIE + " %i:"+PL_NAR,
         lampy, lista);
    
    lampy = CMDPARSE_STD->normal_access(lampy, 0, 0, 1);
    lista = CMDPARSE_STD->normal_access(lista, 0, 0, 1);
    lista = filter(lista, check_oil);
    
    if (!sizeof(lampy))
    {
        notify_fail(vb + " co <czym>?\n");
        return 0;
    }   

    if (!sizeof(lista))
    {
        //notify_fail(vb + " " + lampy[0]->short(this_player(), PL_BIE) + " czym?\n");
	notify_fail(vb + " " + lampy[0]->short(this_player(), PL_BIE) + " olejem?\n");
        return 0;
    }

    if (lampy[0]->query_max_time() && 
        lampy[0]->query_time(1) == lampy[0]->query_max_time())
    {
        notify_fail(capitalize(lampy[0]->short(this_player(), PL_MIA)) +
            " jest ju� pe�na.\n");
        return 0;
    }
    
    lista[0]->fill_other(lampy[0]);
    lampy[0]->remove_adj("wypalony");
    lampy[0]->odmien_short();

    return 1;
}

int
filter_lamp(object ob)
{
    if (function_exists("filter_lamp",ob) != 0 &&
        CAN_SEE(this_player(), ob))
        return 1;
    return 0;
}

void
przed_chwyceniem(int cicho)
{
//	write("lampa: zdejmij_mnie() -> query_subloc()=["+query_subloc()+"]\n");
    if (query_subloc() == LEWE_RAMIE_SUBLOC)
    {
//        this_player()->command("zdejmij lampe z lewego ramienia");
        if (!cicho)
	{
        write("Zdejmujesz " + short(this_player(), PL_BIE) + " z lewego ramienia.\n");
        say(QCIMIE(this_player(), PL_MIA) + 
            " zdejmuje " + QSHORT(this_object(), PL_BIE) + " z lewego ramienia.\n");
	}
        obj_subloc = 0;
        this_player()->remove_subloc(LEWE_RAMIE_SUBLOC);
        remove_prop(OBJ_I_DONT_INV);
	this_player()->zwolnij_sloty(this_object());
	gWhereIs = 0;
	return;
    }
    if (query_subloc() == PRAWE_RAMIE_SUBLOC)
    {
//        this_player()->command("zdejmij lampe z prawego ramienia");
        if (!cicho)
	{
        write("Zdejmujesz " + short(this_player(), PL_BIE) + " z prawego ramienia.\n");
        say(QCIMIE(this_player(), PL_MIA) + 
            " zdejmuje " + QSHORT(this_object(), PL_BIE) + " z prawego ramienia.\n");
	}
        obj_subloc = 0;
        this_player()->remove_subloc(PRAWE_RAMIE_SUBLOC);
        remove_prop(OBJ_I_DONT_INV);
	this_player()->zwolnij_sloty(this_object());
	gWhereIs = 0;
	return;
    }
}

void
przed_opuszczeniem()
{
    if (query_lit(0))
    {
        write("Gasisz " + this_object()->short(this_player(), PL_BIE) + ".\n");
	say(QCIMIE(this_player(), PL_MIA) + " gasi " + QSHORT(this_object(), PL_BIE) + ".\n");
        this_object()->extinguish_me();
    }
}

int
przewies_lampe(string str)
{
    string vb = capitalize(query_verb());
    object *lampy;
    int i;
    string where, op_where;
    
    if (environment(this_object()) != this_player())
        return 0;
    
    if (!str)
    {
        notify_fail(vb + " co przez co?\n");
        return 0;
    }
    
    parse_command(str, all_inventory(this_player()), "%i:"+PL_BIE + " %s", lampy, str);
    lampy = CMDPARSE_STD->normal_access(lampy, "filter_lamp", this_object(), 1);
    
    if (!sizeof(lampy))
    {
        notify_fail(vb + " co przez co?\n");
        return 0;
    }   

    if (str ~= "przez prawe rami�")
    {
	    where = PRAWE_RAMIE_SUBLOC;
	    op_where = "przez prawe rami�";
    }
    else if (str ~= "przez lewe rami�")
    {
	    where = LEWE_RAMIE_SUBLOC;
	    op_where = "przez lewe rami�";
    }
    else if (str ~= "przez rami�")
    {
	    where = 0;
    }
    else
    {
        notify_fail(vb + " " + short(this_player(), PL_BIE) + " przez co?\n");
        return 0;
    }
    
    
    if (lampy[0]->query_subloc() == LEWE_RAMIE_SUBLOC)
    {
        notify_fail("Ale� " + lampy[0]->short(this_player(), PL_MIA) +
            " ju� jest przewieszona przez lewe rami�!\n");
        return 0;
    }
    
    if (lampy[0]->query_subloc() == PRAWE_RAMIE_SUBLOC)
    {
        notify_fail("Ale� " + lampy[0]->short(this_player(), PL_MIA) +
            " ju� jest przewieszona przez prawe rami�!\n");
        return 0;
    }

    if (!where)
    {
	    where = LEWE_RAMIE_SUBLOC;
	    op_where = "przez lewe rami�";
	    if (this_player()->query_subloc_obj(LEWE_RAMIE_SUBLOC))
	    {
		    where = PRAWE_RAMIE_SUBLOC;
	            op_where = "przez prawe rami�";
	    }
    }
    if (this_player()->query_subloc_obj(where))
    {
        notify_fail("Masz ju� przewieszon� " +
            this_player()->query_subloc_obj(where)->short(this_player(), PL_BIE) + " " + op_where + ".\n");
        return 0;
    }
    

    if(lampy[0]->query_wielded()) {
//        this_player()->command("opusc lampe");
        if (check_seen(this_player()))
	            write("Opuszczasz " + short(this_player(), PL_BIE) + ".\n");
        else
	            write("Opuszczasz co^s.\n");
        saybb(QCIMIE(this_player(), PL_MIA) + " opuszcza " +
		              QSHORT(this_object(), PL_BIE) + ".\n");
	wielder->unwield(this_object());
	wielded = 0;
	wielded_in_hand = wep_hands;
    }
	
    this_player()->add_subloc(where, lampy[0]);
//    lampy[0]->move(this_player(), LAMPA_SUBLOC);    
    obj_subloc = where;

    if (where ~= PRAWE_RAMIE_SUBLOC)
    {
	    gWhereIs = 1;
    }
    else
    {
	    gWhereIs = 2;
    }

    this_player()->zajmij_sloty(this_object());
    
    write("Przewieszasz " + lampy[0]->short(this_player(), PL_BIE) + " " + op_where + ".\n");
    say(QCIMIE(this_player(), PL_MIA) + 
        " przewiesza " + QSHORT(lampy[0], PL_BIE) + " " + op_where + ".\n");
	add_prop(OBJ_I_DONT_INV, 1);
    
    return 1;
}

int 
odepnij_lampe(string str)
{
    string vb = capitalize(query_verb());
    object *lampy;
    int i;
    string where, op_where;
   
//    write("odepnij_lampe("+str+")\n");
    
    if (environment(this_object()) != this_player())
        return 0;
    
    if (!str)
    {
        notify_fail(vb + " co sk�d?\n");
        return 0;
    }
    
    parse_command(str, all_inventory(this_player()), "%i:"+PL_BIE + " %s", lampy, str);
    lampy = CMDPARSE_STD->normal_access(lampy, "filter_lamp", this_object(), 1);

//    write("odepnij_lampe: str=["+str+"]\n");
    
    if (!sizeof(lampy))
    {
        notify_fail(vb + " co sk�d?\n");
        return 0;
    }

    if (str ~= "z prawego ramienia")
    {
	    where = PRAWE_RAMIE_SUBLOC;
	    op_where = "z prawego ramienia";
    }
    else if (str ~= "z lewego ramienia")
    {
	    where = LEWE_RAMIE_SUBLOC;
	    op_where = "z lewego ramienia";
    }
    else if (str ~= "z ramienia")
    {
	    where = 0;
    }
    else
    {
        notify_fail(vb + " " + short(this_player(), PL_BIE) + " sk�d?\n");
        return 0;
    }
   
    if (!where)
    {
	    where = LEWE_RAMIE_SUBLOC;
	    op_where = "z lewego ramienia";
	    if (!this_player()->query_subloc_obj(LEWE_RAMIE_SUBLOC))
	    {
		    where = PRAWE_RAMIE_SUBLOC;
	            op_where = "z prawego ramienia";
	    }
    }
    
    if (lampy[0]->query_subloc() != where)
    {
        notify_fail("Ale� " + lampy[0]->short(this_player(), PL_MIA) + 
            " nie jest przewieszon" + lampy[0]->koncowka("y","a","e") + " przez "+
	    ((where == PRAWE_RAMIE_SUBLOC) ? "prawe" : "lewe")+" rami�!\n");
        return 0;
    }
    
    obj_subloc = 0;
    this_player()->remove_subloc(where);
    
    this_player()->zwolnij_sloty(this_object());
    gWhereIs = 0;
    
    if (query_lit(0))
    {
        write("Gasisz " + lampy[0]->short(this_player(), PL_BIE) + " po czym zdejmujesz " +
            this_object()->koncowka("go", "j�", "je") + " " + op_where + ".\n");
        say(QCIMIE(this_player(), PL_MIA) + 
            " gasi " + QSHORT(lampy[0], PL_BIE) + " po czym zdejmuje " +
            this_object()->koncowka("go", "j�", "je") + " " + op_where + ".\n");
        this_object()->extinguish_me();
    }
    else
    {
        write("Zdejmujesz " + lampy[0]->short(this_player(), PL_BIE) + " " + op_where + ".\n");
        say(QCIMIE(this_player(), PL_MIA) + 
            " zdejmuje " + QSHORT(lampy[0], PL_BIE) + " " + op_where + ".\n");
	   
    }
    
    
    remove_prop(OBJ_I_DONT_INV);
    return 1;
}



void
init()
{
    ::init();

//    init_light();
    add_action(do_fill, "nape�nij");  
    add_action(do_fill, "wype�nij");
    add_action(przewies_lampe, "przewie�");    
    add_action(odepnij_lampe, "zdejmij"); 
    add_action("pomoc", "?", 2);
}

public int
pomoc(string str)
{
    object lampeczka;

    notify_fail("Nie ma pomocy na ten temat.\n");

    if (!str) return 0;

    if (!parse_command(str, this_player(), "%o:" + PL_MIA, lampeczka))
        return 0;

    if (!lampeczka) return 0;

    if (lampeczka != this_object())
        return 0;
    
    write("Mo�esz j� nape�ni�, przewiesi� sobie gdzie�, lub nast�pnie "+
          "zdj��. Naturalnie, przedmiot jak ten mo�esz tak�e zapali� "+
          "lub zgasi�.\n");
    return 1;
}

public int
light_me()
{
	if ((query_subloc() != PRAWE_RAMIE_SUBLOC) &&
		(query_subloc() != LEWE_RAMIE_SUBLOC)  &&
	    !(objectp(query_wielded())))
	{
        write("Chwy� najpierw " + short(this_player(), PL_BIE) + 
                    " lub przewie� przez rami�, bo jeszcze si� poparzysz.\n");
        return 1;
    }

    if (!query_time(1))
    {
        write("Pr�bujesz zapali� " + short(this_player(), PL_BIE)
            + ", ale ci si� nie udaje... " + (query_tylko_mn() ? "s�" : "jest")
            +" wyczerpan" + koncowka("y", "a", "e", "i", "e") + ".\n");
        return 1;
    }

    return ::light_me();
}


// Konieczne bylo pokrycie ze wzgledu na uwage o bezuzytecznosci w pochodni
// To na dole chyba nie dziala (Rantaur)
public string
query_light_fail()
{
    if ((TO->query_subloc() != PRAWE_RAMIE_SUBLOC) &&
		(TO->query_subloc() != LEWE_RAMIE_SUBLOC) &&
        !(objectp(TO->query_wielded())))
        {
	        return "Chwy� najpierw " + short(this_player(), PL_BIE) + 
                   " lub przewie� przez rami�, bo jeszcze si� poparzysz.\n";
        }

    if (!query_time(1))
        return "Pr�bujesz zapali� " + short(this_player(), PL_BIE) + ", ale ci si� nie "+
            "udaje... " + (query_tylko_mn() ? "s�" : "jest") +
            " wyczerpan" + koncowka("y", "a", "e", "i", "e") + ".\n";

    if (query_lit(0))
        return capitalize(short(this_player(), PL_MIA)) + " ju� " + 
            (query_tylko_mn() ? "s�" : "jest") + " zapal" + 
            koncowka("ony", "ona", "one", "eni", "one") + ".\n";

    return "";
}


// Odzyskiwanie lampy
public string
query_auto_load()
{
    return ::query_auto_load() + query_keep_auto_load();
}

public string
init_arg(string arg)
{
    return init_keep_arg(::init_arg(arg));
}

// Dodatkowa linijka w opisie lampy, o stanie jej napelnienia
public string
lamp_cond_desc()
{
    string tmp;
    int proc;
    
    tmp = capitalize(short(this_player(), PL_MIA)) + " jest ";

//    if (!query_max_time())
        proc = 100;     
//    else        
//        proc = (query_max_time() - query_time()) * 100 / query_max_time();
    
    switch (proc)
    {
        case 0:
            tmp += "pe�n";
            break;
        case 1..30:
            tmp += "prawie pe�n";
            break;
        case 31..70:
            tmp += "cz�ciowo wype�nion";
            break;
        case 71..99:
            tmp += "prawie pust";
            break;
        case 100:
            tmp += "pust";
            break;
    }

    return (tmp + koncowka("y","a","e") + ".\n");
}

void
leave_env(object old, object desc)
{
    if (old->query_subloc_obj(LEWE_RAMIE_SUBLOC) == this_object())
    {
        set_this_player(old);
        this_player()->remove_subloc(LEWE_RAMIE_SUBLOC);
        write("Zdejmujesz " + short(this_player(), PL_BIE) + " z lewego ramienia.\n");
        say(QCIMIE(this_player(), PL_MIA) + 
            " zdejmuje " + QSHORT(this_object(), PL_BIE) + " z lewego ramienia.\n");
	    remove_prop(OBJ_I_DONT_INV);
	    this_player()->zwolnij_sloty(this_object());
	    gWhereIs = 0;
    }
    if (old->query_subloc_obj(PRAWE_RAMIE_SUBLOC) == this_object())
    {
        set_this_player(old);
        this_player()->remove_subloc(PRAWE_RAMIE_SUBLOC);
        write("Zdejmujesz " + short(this_player(), PL_BIE) + " z prawego ramienia.\n");
        say(QCIMIE(this_player(), PL_MIA) + 
            " zdejmuje " + QSHORT(this_object(), PL_BIE) + " z prawego ramienia.\n");
	    remove_prop(OBJ_I_DONT_INV);
	    this_player()->zwolnij_sloty(this_object());
	    gWhereIs = 0;
    }
    
    ::leave_env(old, desc);
}

void
enter_env(object dest, object old)
{
	if ((function_exists("create_container", dest) != "/std/living") && query_lit(0)) {
		add_prop(OBJ_I_NO_GET, capitalize(short(this_player(), PL_MIA)) + " jest zbyt gor�c" +
				koncowka("y", "a", "e") + "!\n");
		gNieBrac = 1;
	}
}

public int
extinguish_me()
{
	int res;
	if (intp(res = ::extinguish_me()) && (res > 0)) {
		if (gNieBrac) {
			remove_prop(OBJ_I_NO_GET);
		}
	}
	return res;
}

public int *
query_slots()
{
	if (gWhereIs == 1)
	{
//		write("1) gWhereIs == " + gWhereIs);
		return ({ TS_R_ARM }) + ::query_slots();
	}
	if (gWhereIs == 2)
	{
//		write("2) gWhereIs == " + gWhereIs);
		return ({ TS_L_ARM }) + ::query_slots();
	}
//		write("3) gWhereIs == " + gWhereIs);
	return ::query_slots();
}
