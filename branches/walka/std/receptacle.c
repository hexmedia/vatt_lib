/**
 * \file /std/receptacle.c
 *
 * Plik ten zawiera kod obs�uguj�cy otwieranie/zamykanie kontener�w. Ponadto
 * znajduje si� w nim wsparcie dla zamykania/otwierania za pomoc� klucza.
 * Dodatkowo mo�na spr�bowa� wy�ama� zamek, je�eli istnieje.
 *
 * Je�eli chcesz doda� jakie� dodatkowe sprawdzenia przed pr�b� manipulowania
 * kontenerem, lub co� innego, mo�esz dopisa� w�asny kod. Nale�y go umie�ci�
 * w nast�puj�cych funkcjach:
 * 
 *   varargs int open(object ob)
 *   varargs int close(object ob)
 *   varargs int lock(object ob)   Tylko wtedy gdy kontener ma zamek
 *   varargs int unlock(object ob) Tylko wtedy gdy kontener ma zamek
 *   varargs int pick(object ob)   Tylko je�eli zamek mo�na wy�ama�
 *
 * Funkcje te s� wywo�ywane poprzez wska�nik do aktualnie rozwa�anego kontenera
 * i s� opcjonalne. �eby okre�li�, w kt�rych plikach funkcje te mog� by�
 * znalezione, musisz u�y�:
 *
 *   void set_cf(object o)   Ustawia obiekt z definicj� open(), close() etc.
 *
 * Funkcje te mog� zwraca� nast�puj�ce warto�ci:
 * <ul>
 *   <li> <b>0</b> je�eli nie ma zmiany w manipulacji kontenerami
 *   <li> <b>1</b> je�eli mo�na manipulowa�, lecz tekst wypisywany jest w funkcji
 *   <li> <b>2</b> je�eli <b>nie</b> mo�na manipulowa�
 *   <li> <b>3</b> je�eli <b>nie</b> mo�na manipulowa� i tekst wypisywany jest
 *                 w funkcji.
 * </ul>
 *
 * Do obs�ugi zamka w kontenerze s� nast�puj�ce funkcje:
 *
 *   void set_key(mixed keyvalue)  Ustawia warto�� klucza
 *   void set_pick(int pick_level) Ustawia stopie� trudno�ci wy�amania zamka
 *   void set_no_pick(1/0)         Ustawia niemo�no�� wy�amania zamka
 *
 * Standardowy poziom trudno�ci wy�amania zamka ustawiony jest na 40. Mo�esz
 * ustawi� poziom trudno�ci wy�amania zamka w zakresie [1..99]. Poziom ten
 * sprawdzany jest z liczb� wyliczon� na podstawie umiej�tno�ci gracza. Warto��
 * spoza przedzia�u spowoduje wy�amanie zamka niemo�liwym do wykonania. Dodatkowo
 * mo�esz ustawi� <i>set_no_pick</i> by zamka na pewno nie mo�na by�o wy�ama�.
 * 
 * Dla ka�dej funkcji <i>set_</i> jest odpowiadaj�ca funkcja <i>query_</i>
 *
 * <b>UWAGA</b>
 *
 * Je�eli nie zdefiniujesz w�asnych kr�tkich opis�w dla liczby mnogiej,
 * mog� zosta� wygenerowane bardzo brzydkie napisy.
 *
 * <b>PORADA</b>
 *
 * Nie przedefiniowuj funkcji <i>short</i>, <i>pshort</i> i <i>long</i>
 * w swoim w�asnym kodzie. Lepiej u�ywaj <i>set_short</i>, <i>set_pshort</i>
 * i <i>set_long</i> z VBFC by uzyska� lepsze wyniki.
 */
#pragma save_binary
#pragma strict_types

inherit "/std/container";

#include <cmdparse.h>
#include <files.h>
#include <macros.h>
#include <ss_types.h>
#include <stdproperties.h>

#define MANA_TO_PICK_LOCK      10
#define PICK_SKILL_DIFFERENCE  30
#define HANDLE_KEY_FAIL_STRING " co czym?"

public nomask varargs object*
normal_access(string str, string pattern, string fail_str);
public nomask int    filter_open_close_containers(object ob);
public nomask int    filter_subloc_keys(mixed sublok);
public nomask mixed  normal_access_key(string str);
public int           do_default_open(string str);
public int           do_default_close(string str);
public int           do_default_lock(string str);
public int           do_default_unlock(string str);
public int           do_default_pick(string str);

/*
 * Zmienne globalne
 */
static object cont_func;      /* obiekt definiuj�cy open(), close(), etc.        */
static mixed  cont_key;       /* Identyfikator klucza otwieraj�cego ten kontener */
static mapping keys = ([]);   /* mapping kluczy otwieraj�cych sublokacje tego kontenera */
static mapping picks = ([]);  /* mapping trudno�ci wy�amania zamk�w sublokacji */
static int    cont_pick = 40; /* Poziom trudno�ci wy�amania zamka             */

/**
 * Konstruktor obiektu.
 *
 * Funkcj� t� nale�y przedefiniowa�, by m�c ustawi� odpowiednie rzeczy w nowo
 * konstruowanym obiekcie. Jednak�e skoro jest to tylko kontener z kilkoma
 * dodatkami, mo�na w tym celu u�y� tak�e funkcji <i>create_container()</i>
 */
void
create_receptacle()
{
}

/**
 * Konstruktor obiektu.
 *
 * Skoro plik ten nazywa si� <b>/std/receptacle</b>, mo�esz u�y� funkcji
 * <i>create_receptacle</i> dla stworzenia obiektu. Dodatkowo, jak zwykle,
 * mo�esz u�y� <i>create_container</i>, gdy� jest to tylko kontener z kilkoma
 * dodatkami.
 */
void
create_container()
{
  create_receptacle();
}

/**
 * Funkcja wywo�ywana przy resecie obiektu.
 * 
 * Je�eli chcesz, by ten obiekt resetowa� si� w regularnych odst�pach czasu,
 * musisz przedefiniowa� t� w�a�nie funkcj�. Poniewa� obiekt ten jest tylko
 * kontenerem z kilkoma dodatkami, mo�esz tak�e u�y� funkcji
 * <i>reset_container()</i>. Nie zapomnij wywo�a� <i>enable_reset()</i>
 * w konstruktorze, by obiekt rzeczywi�cie zacz�� si� resetowa�.
 */
void
reset_receptacle()
{
}

/**
 * Funkcja wywo�ywana przy resecie obiektu.
 * 
 * Je�eli stworzy�e� kontener za pomoc� <i>create_receptacle</i> i chcesz,
 * by si� resetowa� za pomoc� <i>reset_receptacle</i>, to ta funkcja w�a�nie
 * to umo�liwia. Nie zapomnij wywo�a� <i>enable_reset()</i> w konstruktorze,
 * by obiekt rzeczywi�cie zacz�� si� resetowa�.
 */
void
reset_container()
{
  reset_receptacle();
}

/**
 * Funkcja wywo�ywana przy inicjalizacji.
 *
 * Dowi�zuje ona komendy do manipulowania kontenerem do odpowiednich funkcji.
 */
public void
init()
{
  ::init();
  int picks_enabled = 0;

  add_action(do_default_open,  "otw�rz");
  add_action(do_default_close, "zamknij");

  if (cont_key || m_sizeof(keys))
  {
    add_action(do_default_lock,   "zamknij");
    add_action(do_default_unlock, "otw�rz");
    foreach (int x : m_values(picks)) {
      if (x != 0) {
        picks_enabled = 1;
        break;
      }
    }
    if ((cont_pick != 0) || picks_enabled)
    {
      add_action(do_default_pick, "wy�am");
    }
  }
}

/**
 * Zwraca prawdziwy kr�tki opis.
 *
 * Prawdziwy kr�tki opis jest bez dodanych przymiotnik�w 'otwarty'/'zamkni�ty'.
 * 
 * @param for_obj dla kogo jest generowany opis.
 * @param przyp przypadek, w kt�rym ma by� opis.
 *
 * @return Kr�tki opis.
 */
public varargs string
real_short(mixed for_obj, int przyp)
{
  return ::short(for_obj, przyp);
}

/**
 * Zwraca kr�tki opis.
 *
 * Zwracany opis jest z dodanymi przymiotnikami 'otwarty'/'zamkni�ty'.
 *
 * @param for_obj dla kogo jest generowany opis.
 * @param przyp przypadek, w kt�rym ma by� opis.
 *
 * @return Kr�tki opis.
 */
public varargs string
short(mixed for_obj, mixed przyp)
{
  int closed;

  if (!objectp(for_obj))
  {
    if (intp(for_obj))
      przyp = for_obj;
    else if (stringp(for_obj))
      przyp = atoi(for_obj);

    for_obj = previous_object();
  }
  else
    if (stringp(przyp))
      przyp = atoi(przyp);

  closed = query_prop(CONT_I_CLOSED);
  
  if (query_prop(CONT_I_NO_OPEN_DESC)) {
    return ::short(for_obj, przyp);
  }

  return oblicz_przym(closed ? "zamkni�ty" : "otwarty", closed ? 
      "zamkni�ci" : "otwarci", przyp, query_rodzaj(), query_tylko_mn()) + 
    " " + ::short(for_obj, przyp);
}

/**
 * Zwraca kr�tki opis dla wielu przedmiot�w.
 *
 * Zwracany opis jest z dodanymi przymiotnikami 'otwarty'/'zamkni�ty'
 * 
 * @param for_obj dla kogo jest generowany opis.
 * @param przyp przypadek, w kt�rym ma by� opis.
 *
 * @return Kr�tki opis dla wielu przedmiot�w.
 */
public varargs string
plural_short(mixed for_obj, int przyp)
{
  string str;
  int closed;

  if (intp(for_obj)) 
  {
    przyp = for_obj;
    for_obj = this_player();
  }
  /* nie sprawdzamy czy przyp to int, bo nie ma makr do tej funkcji */

  str = ::plural_short(for_obj, przyp);

  if (query_prop(CONT_I_NO_OPEN_DESC)) {
    return str;
  }

  if (stringp(str))
  {
    closed = query_prop(CONT_I_CLOSED);

    return oblicz_przym(closed ? "zamkni�ty" : "otwarty", closed ? 
        "zamkni�ci" : "otwarci", przyp, query_rodzaj(), 1) + " " + str;
  }
  else
    return str;
}

/**
 * Zwraca d�ugi opis.
 *
 * Do d�ugiego opisu jest doklejana informacja, czy kontener jest 'otwarty',
 * czy 'zamkni�ty'.
 * 
 * @param for_obj dla kogo jest generowany opis.
 * 
 * @return D�ugi opis.
 */
public varargs string
long(object for_obj)
{
  if (query_prop(CONT_I_NO_OPEN_DESC)) {
    return ::long(for_obj);
  }
  return ::long(for_obj) + (query_tylko_mn() ? "S� " : "Jest ") + 
    ((query_prop(CONT_I_CLOSED)) ? "zamkni�" : "otwar") + 
    koncowka("ty", "ta", "te", "ci", "te") + ".\n";
}

private int
owner_pred(object ob, string* owners)
{
    return (member_array(MASTER_OB(ob), owners) != -1);
}

/**
 * Funkcja obs�uguj�ca otwieranie.
 * 
 * Pr�buje otworzy� jedn� lub wi�cej kontener�w.
 *
 * @param str tre�� komendy gracza.
 *
 * @return <ul>
 *            <li> <b>1</b> sukces,
 *            <li> <b>0</b> pora�ka.
 *         </ul>
 */
public int
do_default_open(string str)
{
  object tp = this_player();
  object *items;
  int    i;
  int    cres;
  int    succes = 0;
  string what;
  string gFail = "";
  string gSucces = "";
  string sublok = "";
  string *sublocs;
  object* owners, env;

  if (!stringp(str)) {
      notify_fail("Otw�rz co?");
      return 0;
  }

  if (sizeof(items = normal_access(str, "%i:3")))
  {
    for (i = 0; i < sizeof(items); i++)
    {
      what = (string)items[i]->real_short(tp, PL_MIA);

      if (!(items[i]->query_prop(CONT_I_CLOSED)))
      {
        gFail += capitalize(what) + " ju� " + 
          (items[i]->query_tylko_mn() ? "s�" : "jest") + " otwar" + 
          items[i]->koncowka("ty", "ta", "te", "ci", "te") + ".\n";
        write(gFail);
        return 1;
      }
      else if (items[i]->query_prop(CONT_I_LOCK))
      {
        gFail += capitalize(what) + " " + (items[i]->query_tylko_mn() ? 
            "s�" : "jest") + " zamkni�" + 
          items[i]->koncowka("ty", "ta", "te", "ci", "te") + 
          " na klucz.\n";
        write(gFail);
        return 1;
      }
      else
      {
        if (!objectp(items[i]->query_cf()))
        {
          cres = 0;
        }
        else
        {
          cres = (int)((items[i]->query_cf())->open(items[i]));

          if (cres)
          {
            gFail += capitalize(what) + " nie chc" +
              (items[i]->query_tylko_mn() ? "�" : "e") + 
              " si� otworzy�.\n";
          }
        }

        if (!cres)
        {
          what = items[i]->real_short(tp, PL_BIE);

          this_player()->set_obiekty_zaimkow(({ items[i] }));
          gSucces += "Otwierasz " + what + ".\n";
          say(QCIMIE(tp, PL_MIA) + " otwiera " + what + ".\n");
          succes = 1;
          items[i]->remove_prop(CONT_I_CLOSED);
          env = ENV(TP);
          if (objectp(env)) {
              owners = filter(all_inventory(env), &owner_pred(, items[i]->query_owners()));
              owners->signal_rusza_moje_rzeczy(this_player(), items[i]);
          }
        }
      }
    }
  }
  else {
//    write(">>> jaki� sublok\n");
    if (!CAN_SEE_IN_ROOM(tp))
    {
      notify_fail("Jest zbyt ciemno, �eby to zrobi�.\n");
      return 0;
    }
    parse_command(str, (all_inventory(environment(tp)) + all_inventory(tp)),
        "%s %i:" + PL_DOP, sublok, items);
    if (sublok == "") {
      return 0;
    }
//    write("sublok = " + sublok + "\n");

    if (!sizeof(items = NORMAL_ACCESS(items, 0, 0)))
    {
      return 0; 
    }
    
    if (!sizeof(items = filter(items, filter_open_close_containers))) {
      return 0;
    }

    for (i = 0; i < sizeof(items); i++)
    {
      what = (string)items[i]->real_short(tp, PL_DOP);

//      write("query_sublocs:\n");
//      dump_array(items[i]->query_sublocs());
//      write("---\n");
      sublocs = filter(items[i]->query_sublocs(), &operator(~=)(sublok, ) @ &(items[i])->query_subloc_prop(, CONT_M_OPENCLOSE));
//      write("po na�o�eniu filtra:\n");
//      dump_array(sublocs);
//      write("---\n");

      if (sizeof(sublocs) > 1) {
        notify_fail("Zdecyduj si�, co chcesz otworzy�.\n");
        return 0;
      }
      
      if ((!sizeof(sublocs)) || (!pointerp(sublocs[0]))) {
        return 0;
      }

      if (!(items[i]->query_subloc_prop(sublocs[0], CONT_I_CLOSED))) {
        gFail += capitalize(sublocs[0][PL_MIA]) + " " + what + " ju� " + 
          (items[i]->query_subloc_prop(sublocs[0], SUBLOC_I_TYLKO_MN) ? "s�" : "jest") + " otwar" + 
          items[i]->subloc_koncowka(sublocs[0], "ty", "ta", "te", "ci", "te") + ".\n";
        write(gFail);
        return 1;
      }
      else if (items[i]->query_subloc_prop(sublocs[0], CONT_I_LOCK))
      {
        gFail += capitalize(sublocs[0][PL_MIA]) + " " + what + " " +
          (items[i]->query_subloc_prop(sublocs[0], SUBLOC_I_TYLKO_MN) ? "s�" : "jest") +
          " zamkni�" + items[i]->subloc_koncowka(sublocs[0], "ty", "ta", "te", "ci", "te") + 
          " na klucz.\n";
        write(gFail);
        return 1;
      }
      else
      {
        if (!objectp(items[i]->query_cf()))
        {
          cres = 0;
        }
        else
        {
          cres = (int)((items[i]->query_cf())->open(items[i], sublocs[0]));

          if (cres)
          {
            gFail += capitalize(sublocs[0][PL_MIA]) + " " + what + " nie chc" +
              (items[i]->query_subloc_prop(sublocs[0], SUBLOC_I_TYLKO_MN) ? "�" : "e") + 
              " si� otworzy�.\n";
          }
        }

        if (!cres)
        {
          this_player()->set_obiekty_zaimkow(({ items[i] }));
          gSucces += "Otwierasz " + sublocs[0][PL_BIE] + " " + what + ".\n";
          say(QCIMIE(tp, PL_MIA) + " otwiera " + sublocs[0][PL_BIE] + " " + what + ".\n");
          succes = 1;
          items[i]->remove_subloc_prop(sublocs[0], CONT_I_CLOSED);
          env = ENV(TP);
          if (objectp(env)) {
              owners = filter(all_inventory(env), &owner_pred(, items[i]->query_owners()));
              owners->signal_rusza_moje_rzeczy(this_player(), items[i]);
          }
        }
      }       
    }
  }

  if (succes)
  {
    if (strlen(gFail))
    {
      write(gFail);
    }
    if (strlen(gSucces))
    {
      write(gSucces);
    }
    return 1;
  }

  notify_fail(gFail);
  return 0;
}

/**
 * Funkcja obs�uguj�ca zamykanie.
 * 
 * Pr�buje zamkn�� jeden lub wi�cej kontener�w.
 *
 * @param str tre�� komendy gracza.
 *
 * @return <ul>
 *            <li> <b>1</b> sukces,
 *            <li> <b>0</b> pora�ka.
 *         </ul>
 */
public int
do_default_close(string str)
{
  object tp = this_player();
  object *items;
  int    i;
  int    cres;
  int    succes = 0;
  string what;
  string gFail = "";
  string gSucces = "";
  string sublok = "";
  string *sublocs;
  object* owners, env;

  if (!stringp(str)) {
      notify_fail("Zamknij co?");
      return 0;
  }
//  write(">>> do_default_close\n");
  if (sizeof(items = normal_access(str, "%i:3")))
  {
    for (i = 0; i < sizeof(items); i++)
    {
      what = (string)items[i]->real_short(tp);

      if (items[i]->query_prop(CONT_I_CLOSED))
      {
        gFail += capitalize(what) + " ju� " + 
          (items[i]->query_tylko_mn() ? "s�" : "jest") + " zamkni�" + 
          items[i]->koncowka("ty", "ta", "te", "ci", "te") + ".\n";
        write(gFail);
        return 1;
      }
      else if (items[i]->query_prop(CONT_I_LOCK))
      {
        gFail += capitalize(what) + " nie chc" +
          (items[i]->query_tylko_mn() ? "�" : "e") + 
          " si� zamkn��.\n";
        write(gFail);
        return 1;
      }
      else
      {
        if (!objectp(items[i]->query_cf()))
        {
          cres = 0;
        }
        else
        {
          cres = (int)((items[i]->query_cf())->close(items[i]));

          if (cres)
          {
            gFail += capitalize(what) + " nie chc" +
              (items[i]->query_tylko_mn() ? "�" : "e") + 
              " si� zamkn��.\n";
          }
        }

        if (!cres)
        {
          what = items[i]->real_short(tp, PL_BIE);

          this_player()->set_obiekty_zaimkow(({ items[i] }));                
          gSucces += "Zamykasz " + what + ".\n";
          say(QCIMIE(tp, PL_MIA) + " zamyka " + what + ".\n");
          succes = 1;
          items[i]->add_prop(CONT_I_CLOSED, 1);
          env = ENV(TP);
          if (objectp(env)) {
              owners = filter(all_inventory(env), &owner_pred(, items[i]->query_owners()));
              owners->signal_rusza_moje_rzeczy(this_player(), items[i]);
          }
        }
      }
    }
  }
  else {
//    write(">>> jaki� sublok\n");
    if (!CAN_SEE_IN_ROOM(tp))
    {
      notify_fail("Jest zbyt ciemno, �eby to zrobi�.\n");
      return 0;
    }
    parse_command(str, (all_inventory(environment(tp)) + all_inventory(tp)),
        "%s %i:" + PL_DOP, sublok, items);
    if (sublok == "") {
      return 0;
    }
//    write("sublok = " + sublok + "\n");

    if (!sizeof(items = NORMAL_ACCESS(items, 0, 0)))
    {
      return 0; 
    }
    
    if (!sizeof(items = filter(items, filter_open_close_containers))) {
      return 0;
    }

    for (i = 0; i < sizeof(items); i++)
    {
      what = (string)items[i]->real_short(tp, PL_DOP);

//      write("query_sublocs:\n");
//      dump_array(items[i]->query_sublocs());
//      write("---\n");
      sublocs = filter(items[i]->query_sublocs(), &operator(~=)(sublok, ) @ &(items[i])->query_subloc_prop(, CONT_M_OPENCLOSE));
//      write("po na�o�eniu filtra:\n");
//      dump_array(sublocs);
//      write("---\n");

      if (sizeof(sublocs) > 1) {
        notify_fail("Zdecyduj si�, co chcesz zamkn��.\n");
        return 0;
      }
      
      if ((!sizeof(sublocs)) || (!pointerp(sublocs[0]))) {
        return 0;
      }

      if (items[i]->query_subloc_prop(sublocs[0], CONT_I_CLOSED)) {
        gFail += capitalize(sublocs[0][PL_MIA]) + " " + what + " ju� " + 
          (items[i]->query_subloc_prop(sublocs[0], SUBLOC_I_TYLKO_MN) ? "s�" : "jest") + " zamkni�" + 
          items[i]->subloc_koncowka(sublocs[0], "ty", "ta", "te", "ci", "te") + ".\n";
        write(gFail);
        return 1;
      }
      else if (items[i]->query_subloc_prop(sublocs[0], CONT_I_LOCK))
      {
        gFail += capitalize(sublocs[0][PL_MIA]) + " " + what + " nie chc" +
          (items[i]->query_subloc_prop(sublocs[0], SUBLOC_I_TYLKO_MN) ? "�" : "e") + 
          " si� zamkn��.\n";
        write(gFail);
        return 1;
      }
      else
      {
        if (!objectp(items[i]->query_cf()))
        {
          cres = 0;
        }
        else
        {
          cres = (int)((items[i]->query_cf())->close(items[i], sublocs[0]));

          if (cres)
          {
            gFail += capitalize(sublocs[0][PL_MIA]) + " " + what + " nie chc" +
              (items[i]->query_subloc_prop(sublocs[0], SUBLOC_I_TYLKO_MN) ? "�" : "e") + 
              " si� zamkn��.\n";
          }
        }

        if (!cres)
        {
          this_player()->set_obiekty_zaimkow(({ items[i] }));
          gSucces += "Zamykasz " + sublocs[0][PL_BIE] + " " + what + ".\n";
          say(QCIMIE(tp, PL_MIA) + " zamyka " + sublocs[0][PL_BIE] + " " + what + ".\n");
          succes = 1;
          items[i]->add_subloc_prop(sublocs[0], CONT_I_CLOSED, 1);
          env = ENV(TP);
          if (objectp(env)) {
              owners = filter(all_inventory(env), &owner_pred(, items[i]->query_owners()));
              owners->signal_rusza_moje_rzeczy(this_player(), items[i]);
          }
        }
      }
    }
  }

  if (succes)
  {
    if (strlen(gFail))
    {
      write(gFail);
    }
    if (strlen(gSucces))
    {
      write(gSucces);
    }
    return 1;
  }

  notify_fail(gFail);
  return 0;
}

/**
 * Funkcja obs�uguj�ca zamykanie zamka.
 *
 * Pr�buje zamkn�� zamek u jednego lub wi�kszej ilo�ci kontener�w.
 * 
 * @param str tre�� komendy gracza.
 *
 * @return <ul>
 *            <li> <b>1</b> sukces,
 *            <li> <b>0</b> pora�ka.
 *         </ul>
 */
public int
do_default_lock(string str)
{
  object tp = this_player();
  object container_item;
  object key_item;
  mixed  subloc;
  mixed  items;
  mixed  keyval;
  string qvb = query_verb();

  if (!pointerp(items = normal_access_key(str)))
    return items;

  container_item = items[0];
  key_item = items[1];
  subloc = items[2];

  if (subloc == 0) {
    if (container_item->query_prop(CONT_I_LOCK))
    {
      write(capitalize(container_item->real_short(tp)) + " ju� " +
          (container_item->query_tylko_mn() ? "s�" : "jest") + 
          " zamkni�" + container_item->koncowka("ty", "ta", "te", "ci", 
            "te") + ".\n");
      return 1;
    }

    if ((objectp(container_item->query_cf())) &&
        ((container_item->query_cf())->lock(container_item)))
    {
      write("Nie mo�esz zamkn�� " + container_item->real_short(tp, PL_DOP)
          + ".\n");
      return 1;
    }

    this_player()->set_obiekty_zaimkow(({ container_item }));
    container_item->add_prop(CONT_I_LOCK, 1);
    write("Zamykasz " + container_item->real_short(tp, PL_BIE) + " " +
        key_item->short(tp, PL_NAR) + ".\n");
    say(QCIMIE(tp, PL_MIA) + " zamyka " + container_item->real_short(PL_BIE) + " " +
        QSHORT(key_item, PL_NAR) + ".\n");
  }
  else {
    if (container_item->query_subloc_item(subloc, CONT_I_LOCK)) {
      write(capitalize(subloc[0]) + " " + container_item->real_short(tp, PL_DOP) +
          " ju� " + (container_item->query_subloc_prop(subloc, SUBLOC_I_TYLKO_MN) ? "s�" : "jest") +
          " zamkni�" + container_item->subloc_koncowka(subloc, "ty", "ta", "te", "ci", "te") + ".\n");
      return 1;
    }

    // TODO: !!!
  }

  return 1;
}

/**
 * Funkcja obs�uguj�ca otwieranie zamka.
 *
 * Pr�buje otworzy� zamek u jednego lub wi�kszej ilo�ci kontener�w.
 * 
 * @param str tre�� komendy gracza.
 *
 * @return <ul>
 *            <li> <b>1</b> sukces,
 *            <li> <b>0</b> pora�ka.
 *         </ul>
 */
public int
do_default_unlock(string str)
{
  object tp = this_player();
  object container_item;
  object key_item;
  mixed  items;
  mixed  keyval;
  string qvb = query_verb();

  if (!pointerp(items = normal_access_key(str)))
    return items;

  container_item = items[0]; key_item = items[1];

  if (!(container_item->query_prop(CONT_I_LOCK)))
  {
    write(capitalize(container_item->real_short(tp)) + " nie " +
        (container_item->query_tylko_mn() ? "s�" : "jest") + 
        " zamkni�" + container_item->koncowka("ty", "ta", "te", "ci", 
          "te") + " na klucz.\n");
    return 1;
  }

  if ((objectp(container_item->query_cf())) &&
      ((container_item->query_cf())->lock(container_item)))
  {
    write("Nie mo�esz otworzy� " + container_item->real_short(tp, PL_DOP)
        + ".\n");
    return 1;
  }

  this_player()->set_obiekty_zaimkow(({ container_item }));
  container_item->remove_prop(CONT_I_LOCK);
  write("Otwierasz " + container_item->real_short(tp, PL_BIE) + " " +
      key_item->short(tp, PL_NAR) + ".\n");
  say(QCIMIE(tp, PL_MIA) + " otwiera " + container_item->real_short(PL_BIE) +
      " " + QSHORT(key_item, PL_NAR) + ".\n");

  return 1;
}

/**
 * Funkcja obs�uguj�ca wy�amywanie zamka.
 *
 * Pr�buje wy�ama� zamek kontenera. Umiej�tno�� gracza wy�amywania zamk�w
 * b�dzie losowo zmieniona i nast�pnie por�wnana z poziomem trudno�ci
 * wy�amania zamka.
 * 
 * @param str tre�� komendy gracza.
 *
 * @return <ul>
 *            <li> <b>1</b> sukces,
 *            <li> <b>0</b> pora�ka.
 *         </ul>
 */
public int
do_default_pick(string str)
{
  object tp = this_player();
  object *items;
  int skill;

  if (!sizeof(items = normal_access(str, " 'zamek' %i:1 ",
          " zamek czego?")))
  {
    return 0;
  }

  skill = (int)tp->query_skill(SS_OPEN_LOCK);
  if (skill == 0)
  {
    write("Nie masz poj�cia jak si� wy�amuje zamki.\n");
    return 1;
  }

  if (tp->query_mana() <= MANA_TO_PICK_LOCK)
  {
    write("Nie potrafisz si� odpowiednio skoncentrowa�, by wy�ama� zamek.\n");
    return 1;
  }
  tp->add_mana(-(MANA_TO_PICK_LOCK));

  if (!(items[0]->query_prop(CONT_I_LOCK)))
  {
    write("Okaza�o si�, �e " +
        items[0]->real_short(tp, PL_MIA) + " nie " +
        items[0]->koncowka("by� zamkni�ty", "by�a zamkni�ta", "by�o zamkni�te") +
        ".\n");
    return 1;
  }

  if (items[0]->query_no_pick())
  {
    write("Po bli�szym przyjrzeniu si� " +
        items[0]->real_short(tp, PL_CEL) +
        " okaza�o si�, �e nie masz �adnej mo�liwo�ci wy�amania zamka.\n");
    return 1;
  }

  skill = ((skill * 3 / 4) + random(skill / 2));

  if ((skill >= items[0]->query_pick()) &&
      (objectp(items[0]->query_cf()) ?
       (!((items[0]->query_cf())->pick(items[0]))) : 1))
  {
    items[0]->remove_prop(CONT_I_LOCK);

    write("Czujesz du�� satysfakcj�, kiedy z zamka " +
        items[0]->real_short(tp, PL_DOP) +
        " wydoby�o si� ciche klikni�cie przeskakuj�cego mechanizmu.\n");
    say(QCIMIE(tp, PL_MIA) + " d�ubie przez chwil� przy zamku " +
        items[0]->real_short(tp, PL_DOP) +
        ((environment(this_object()) == this_player()) ? ", kt�r" +
         items[0]->koncowka("y", "�", "e") + " ma przy sobie, " : " ") +
        "po czym s�yszysz ciche klikni�cie przeskakuj�cego mechanizmu.\n");
    return 1;
  }

  write("Nie uda�o si� wy�ama� zamka " + items[0]->real_short(tp, PL_DOP) +
      "." + ((skill + PICK_SKILL_DIFFERENCE < items[0]->query_pick()) ?
        " Wydaje si�, �e masz za s�abe umiej�tno�ci.\n" : "\n"));
  say(QCIMIE(tp, PL_MIA) + " d�ubie przez chwil� przy zamku " +
      items[0]->real_short(tp, PL_DOP) +
      ((environment(this_object()) == this_player()) ? ", kt�r" +
       items[0]->koncowka("y", "�", "e") + " ma przy sobie, " : " ") +
      "po czym nic si� nie dzieje.\n");
  return 1;
}

/**
 * Ustawia obiekt odpowiedzialny za dodatkowy kod przy otwieraniu/zamykaniu.
 *
 * Pozwala ustawi� obiekt, kt�ry przechowuje kod wywo�ywany przed pr�b�
 * otwarcia konkretnego obiektu. Kod ten b�dzie wywo�any w funkcjach
 * <i>open()</i> i <i>close()</i>. Funkcje te powinny zwraca� <b>1</b>, je�eli
 * nie jest mo�liwe otwarcie/zamkni�cie kontenera lub <b>0</b>, gdy nie ma
 * �adnych przeciwwskaza� ku temu.
 * 
 * @param obj obiekt przechowuj�cy dodatkowy kod.
 */
public void
set_cf(object obj)
{
  if (query_lock())
  {
    return;
  }

  cont_func = obj;
}

/**
 * Zwraca obiekt przechowuj�cy dodatkowy kod.
 *
 * @return obiekt przechowuj�cy dodatkowy kod.
 */
public object
query_cf()
{
  return cont_func;
}

/**
 * Powoduje, �e zamku w kontenerze nie da si� wy�ama�.
 */
public void
set_no_pick()
{
  if (query_lock())
  {
    return;
  }

  cont_pick = 0;
}

/**
 * Sprawdza, czy zamek w kontenerze mo�e by� wy�amany.
 *
 * @return <ul>
 *           <li> <b>1</b> zamek <b>nie</b> mo�e by� wy�amany
 *           <li> <b>0</b> zamek mo�e by� wy�amany
 *         </ul>
 */
public int
query_no_pick()
{
  return (cont_pick == 0);
}

/**
 * Ustawia trudno�� wy�amania zamka.
 *
 * @param i trudno�� wy�amania zamka.
 */
public void
set_pick(int i)
{
  if (query_lock())
  {
    return;
  }

  if ((i > 0) && (i < 100)) /* warto�� z odpowiedniego zakresu */
  {
    cont_pick = i;
    return;
  }

  cont_pick = 0; /* nie da si� wy�ama� zamka */
}

/**
 * Zwraca poziom trudno�ci wy�amania zamka.
 *
 * @return poziom trudno�ci wy�amania zamka.
 */
public int
query_pick()
{
  return cont_pick;
}

/**
 * Ustawia klucz pasuj�cy do zamka.
 *
 * @param keyval klucz pasuj�cy do zamka.
 */
public void
set_key(mixed keyval)
{
  if (query_lock())
  {
    return;
  }

  cont_key = keyval;
}

/**
 * Zwraca klucz pasuj�cy do zamka.
 *
 * @return klucz pasuj�cy do zamka.
 */
public mixed
query_key()
{
  return cont_key;
}

/**
 * Funkcja ta filtruje przedmioty.
 *
 * U�ywana jest przez wszystkie funkcje manipuluj�ce na zamykanych
 * kontenerach, by uzyska� tablic� przedmiot�w, z kt�rymi gracz mo�e
 * co� zrobi�.
 *
 * @param str tre�� komendy gracza
 * @param pattern wz�r, wed�ug kt�rego dopasowywane s� przedmioty
 * @param fail_str tekst, kt�ry ma by� doklejony do <i>notify_fail</i>
 *        w razie b��du
 *
 * @return <ul>
 *           <li> <b>object*</b> tablica obiekt�w, z kt�rymi gracz mo�e
 *                co� zrobi�
 *           <li> <b>0</b> gdy operacja si� nie powiod�a
 *         </ul>
 */
public varargs nomask object *
normal_access(string str, string pattern, string fail_str)
{
  object tp = this_player();
  object *items;

  if (!CAN_SEE_IN_ROOM(tp))
  {
    notify_fail("Jest zbyt ciemno, �eby to zrobi�.\n");
    return 0;
  }

  notify_fail(capitalize(query_verb()) +
      (strlen(fail_str) ? fail_str : " co?") + "\n");
  if (!strlen(str))
  {
    return 0;
  }

  if (!sizeof(items =
        filter((all_inventory(environment(tp)) + all_inventory(tp)),
          filter_open_close_containers)))
  {
    return 0;
  }

  if (!sizeof(items = filter(items, &not() @ &->query_prop(CONT_I_CANT_OPENCLOSE)))) {
    return 0;
  }

  if (fail_str == HANDLE_KEY_FAIL_STRING)
  {
    /* oznacza to, �e funkcja ta jest u�yta w celu uzyskania podstawowego dost�pu */
    return ({ tp });
  }

  if (!(parse_command(str, items, pattern, items)))
  {
    return 0;
  }
  if (!sizeof(items = NORMAL_ACCESS(items, 0, 0)))
  {
    return 0;
  }

  return items;
}

/**
 * Funkcja ta filtruje przedmioty.
 *
 * U�ywana jest przez wszystkie funkcje manipuluj�ce na zamykanych
 * kontenerach, by uzyska� tablic� przedmiot�w, z kt�rymi gracz mo�e
 * co� zrobi�, kluczy, kt�rymi chce manipulowa� przy zamkach, oraz
 * sublokacji, gdzie s� zamontowane zamki.
 *
 * @param str tre�� komendy gracza
 *
 * @return <ul>
 *           <li> tablica, kt�rej pierwszym elementem jest kontener,
 *                drugim klucz, a trzecim sublokacja.
 *           <li> <b>0</b> gdy operacja si� nie powiod�a
 *         </ul>
 */
public nomask mixed
normal_access_key(string str)
{
  object *container_items;
  object *key_items;
  string sublok = "";
  string *sublocs;
  int keyval;

  notify_fail(capitalize(query_verb()) + HANDLE_KEY_FAIL_STRING + "\n");

  if (!stringp(str)) {
      return 0;
  }

  if (!CAN_SEE_IN_ROOM(this_player()))
  {
    notify_fail("Jest zbyt ciemno, �eby to zrobi�.\n");
    return 0;
  }

  if (parse_command(str, (all_inventory(environment(this_player())) +
      all_inventory(this_player())), "%i:3 %i:4",
      container_items, key_items))
  {
    sublok = 0;

    key_items = NORMAL_ACCESS(key_items, 0, this_object());

    if (!key_items || !sizeof(key_items))
      return 0;

    container_items = NORMAL_ACCESS(container_items, 
        "filter_open_close_containers", this_object());

    container_items = filter(container_items, &not() @ &->query_prop(CONT_I_CANT_OPENCLOSE));

    if (!container_items || !sizeof(container_items))
      return 0;

    if ((sizeof(container_items) > 1) || (sizeof(key_items) > 1))
    {
      write("Musisz konkretniej sprecyzowa�, o co ci chodzi.\n");
      return 1;
    }

    if (!(keyval = container_items[0]->query_key()))
    {
      write(capitalize(container_items[0]->real_short(this_player())) +
          " nie ma" + (container_items[0]->query_tylko_mn() ? "j�" : "") +
          " zamka.\n");
      return 1;
    }

    if (keyval != key_items[0]->query_key())
    {
      write(capitalize(key_items[0]->short(PL_MIA)) + " nie pasuje.\n");
      return 1;
    }

    return ({ container_items[0], key_items[0], 0 });

  }
  else {
    parse_command(str, (all_inventory(environment(this_player())) +
          all_inventory(this_player())), "%s %i:1 %i:4", sublok,
        container_items, key_items);
    if (sublok == "") {
      return 0;
    }
    write("sublok = " + sublok + "\n");
    
    key_items = NORMAL_ACCESS(key_items, 0, 0);

    if (!key_items || !sizeof(key_items))
      return 0;

    container_items = NORMAL_ACCESS(container_items, 
        "filter_open_close_containers", this_object());
    
    foreach(object item : container_items) {
      sublocs = filter(item->query_sublocs(), &operator(~=)(sublok, ) @ &item->query_subloc_prop(, CONT_M_OPENCLOSE));
      sublocs = filter(sublocs, &filter_subloc_keys());
      if (sizeof(sublocs)) {
        container_items = ({ item });
        break;
      }
    }
    
    if (!container_items || !sizeof(container_items))
      return 0;

    if ((sizeof(container_items) > 1) || (sizeof(key_items) > 1))
    {
      write("Musisz konkretniej sprecyzowa�, o co ci chodzi.\n");
      return 1;
    }
    
    if (sizeof(sublocs) > 1) {
      notify_fail("Zdecyduj si�, co chcesz zamkn��.\n");
      return 0;
    }
      
    if ((!sizeof(sublocs)) || (!pointerp(sublocs[0]))) {
      return 0;
    }

    if (!(keyval = container_items[0]->query_key(sublocs[0])))
    {
      write(capitalize(sublocs[0][PL_MIA]) + " " +
          container_items[0]->real_short(this_player(), PL_DOP) +
          " nie ma" + (container_items[0]->query_subloc_prop(sublocs[0], SUBLOC_I_TYLKO_MN) ? "j�" : "") +
          " zamka.\n");
      return 1;
    }

    if (keyval != key_items[0]->query_key())
    {
      write(capitalize(key_items[0]->short(PL_MIA)) + " nie pasuje.\n");
      return 1;
    }

    return ({ container_items[0], key_items[0] , sublocs[0] });
  }
}

/**
 * Funkcja sprawdza, czy do danej sublokacji przypisany jest zamek.
 *
 * @param sublok sprawdzany sublok
 *
 * @return <ul>
 *           <li> <b>1</b> je�eli do subloka przypisany jest zamek,
 *           <li> <b>0</b> w przeciwnym przypadku.
 *         </ul>
 */
public nomask int
filter_subloc_keys(mixed sublok)
{
  if (!pointerp(sublok)) {
    return 0;
  }
  if (keys[sublok[0]] == 0) {
    return 0;
  }
  return 1;
}

/**
 * Funkcja ta sprawdza, czy kontener jest zamykany.
 *
 * @return <ul>
 *           <li> <b>1</b> kontener jest zamykany
 *           <li> <b>0</b> kontener nie jest zamykany
 *         </ul>
 */
public nomask int
filter_open_close_containers(object ob)
{
  return (function_exists("filter_open_close_containers", ob) ==
      RECEPTACLE_OBJECT);
}

/**
 * Funkcja ta zwraca opis przedmiotu dla czarodzieja.
 *
 * @return opis przedmiotu dla czarodzieja.
 */
public string
stat_object()
{
  string str = ::stat_object();

  if (cont_key)
  {
    str += "Kod klucza: \"" + cont_key + "\"\n";

    if (cont_pick == 0)
    {
      str += "Zamek nie do wy�amania.\n";
    }
    else
    {
      str += "Poziom trudno�ci wy�amania: " + cont_pick + "\n";
    }
  }
  else
  {
    str += "Nie ma zamka.\n";
  }

  return str;
}

/**
 * Zwraca opis trudno�ci otworzenia zamka.
 *
 * Opis ten jest budowany na podstawie r�nicy mi�dzy poziomem umiej�tno�ci
 * gracza a poziomem trudno�ci wy�amania zamka.
 *
 * @param pick_val wspomniana r�nica
 *
 * @return opis trudno�ci otworzenia zamka.
 */
public nomask string
get_pick_chance(int pick_val)
{
  if (pick_val >= 40)
    return "zamek da si� otworzy� przez samo spojrzenie na niego";
  if (pick_val >= 30)
    return "zamek jest bardzo �atwo otworzy�";
  if (pick_val >= 20)
    return "zamek jest ca�kiem �atwo otworzy�";
  if (pick_val >= 10)
    return "zamek jest �atwo otworzy�";
  if (pick_val >= 0)
    return "zamek da si� otworzy�";
  if (pick_val >= -10)
    return "zamek da si� otworzy� z pewnymi trudno�ciami";
  if (pick_val >= -20)
    return "otwarcie zamka mo�e nastr�czy� troch� trudno�ci";
  if (pick_val >= -30)
    return "zamek jest bardzo ci�ko otworzy�";
  if (pick_val >= -40)
    return "zamka prawie na pewno nie da si� otworzy�";

  /* pick_val < -40 */
  return "zamka nie da si� w �aden spos�b otworzy�";
}

/**
 * Funkcja oceniaj�ca dany obiekt.
 *
 * @param num liczba u�ywana zamiast poziomu umiej�tno�ci oceniania gracza
 */
public varargs void
appraise_object(int num)
{
  int pick_level = cont_pick;
  int seed;
  int skill;

  ::appraise_object(num);

  if (!cont_key)
  {
    return;
  }

  if (!num)
  {
    skill = (int)this_player()->query_skill(SS_APPR_OBJ);
  }
  else
  {
    skill = num;
  }

  sscanf(OB_NUM(this_object()), "%d", seed);
  skill = random((1000 / (skill + 1)), seed);
  pick_level = (int)this_player()->query_skill(SS_OPEN_LOCK) -
    cut_sig_fig(pick_level + (skill % 2 ? -skill % 70 : skill) *
        pick_level / 100, 2);

  write ("Masz wra�enie, �e " + get_pick_chance(pick_level) + ".\n");
}

/**
 * Funkcja zwracaj�ca napis dla odtworzenia.
 *
 * @return napis dla odtworzenia.
 */
public nomask string
query_receptacle_auto_load()
{
  return ("#c_c#" + query_prop(CONT_I_CLOSED) + "#" +
      "#c_l#" + query_prop(CONT_I_LOCK)   + "#");
}

/**
 * Funkcja ustawiaj�ca parametry obiektu na podstawie napisu.
 *
 * @param arg napis odtwarzania.
 */
public nomask string
init_receptacle_arg(string arg)
{
  string foobar, rest;
  int    tmp;

  if (arg == 0) {
      return 0;
  }

  sscanf(arg, "%s#c_c#%d#%s", foobar, tmp, foobar);
  add_prop(CONT_I_CLOSED, tmp);
  sscanf(arg, "%s#c_l#%d#%s", foobar, tmp, rest);
  add_prop(CONT_I_LOCK, tmp);
  return rest;
}

/**
 * Funkcja zwracaj�ca napis dla auto_�adowania.
 *
 * @return Napis dla auto_�adowania
 */
public string
query_auto_load()
{
    return ::query_auto_load() + query_receptacle_auto_load();
}

/**
 * Funkcja inicjalizuj�ca obiekt przy auto_�adowaniu.
 *
 * @param arg napis odtwarzania.
 */
public string
init_arg(string arg)
{
    return init_receptacle_arg(::init_arg(arg));
}
