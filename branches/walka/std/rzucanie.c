/**
    \file /std/rzucanie.c

    Plik obs�uguj�cy rzucanie przedmiotami.
    Obiekt nale�y sklonowa� do inwentarza gracza.

    Tak jak W:GW przykaza� testujemy zawsze jak�� sum� 
    cechy i umiej�tno�ci (jednej b�d� kilku). 

    TODO: parowanie nadlatuj�cych przedmiot�w broni� (tarcz�)
    TODO: obra�enia od broni broni
    TODO: przygniatanie przez ci�kie rzeczy
**/

#pragma strict_types

inherit "/std/object";

#include <composite.h>
#include <cmdparse.h>
#include <pl.h>
#include <stdproperties.h>
#include <macros.h>
#include <filter_funs.h>
#include <ss_types.h>
#include <wa_types.h>

int manip_drop_access(object ob); /* podebrana z /cmd/live/things.c */
public int rzuc(string str);
void real_throw(object co, object cel);
int czy_wolne_rece(object co);
int czy_da_rade(object co);
int obj_speed(object co);
int losowa_hitlokacja(object cel);
int throw_try_hit(object co, int speed, object cel);
void throw_do_dam(object co, int v, object cel, int hitloc_no);
void przewroc(object cel);

/** 
 * Dop�ki Delvert nie poprawi wygl�da to tak, �e graczowi klonujemy obiekt rzutnika :P
**/
public void
create_object()
{
    ustaw_nazwe( ({"rzutnik","rzutnika","rzutnikowi","rzutnik","rzutnikiem","rzutniku"}),
                 ({"rzutniki","rzutnikow","rzutnikom","rzutniki","rzutnikami","rzutnikach"}), PL_MESKI_NOS_NZYW);

    dodaj_przym("kolorowy","kolorowi");

    set_long("No w�asnie, do czego mo�e s�u�y� rzutnik?\n");

    add_prop(OBJ_I_WEIGHT, 0);
    add_prop(OBJ_I_VOLUME, 0);

    set_no_show();
    
    add_prop(OBJ_M_NO_DROP, 1);
    add_prop(OBJ_M_NO_STEAL,1);
    add_prop(OBJ_M_NO_SELL, 1);
    add_prop(OBJ_M_NO_GIVE, 1);
}

public void
init()
{
    ::init();
    add_action(rzuc,"rzu�");
}

/** 
 * Funkcja parsuj�ca komend� 'rzu�'.
**/
public int
rzuc(string str)
{
    object *itemy;
    object item1, *item2;

    notify_fail("Rzu� czym w kogo?\n");

    if (!strlen(str))
        return 0;
    
    if (parse_command(str, environment(TP), "%i:" + PL_NAR + " 'w' %l:" + PL_BIE, itemy, item2))
    {
        itemy = CMDPARSE_STD->normal_access(itemy, "manip_drop_access", this_object());

	    if(!sizeof(itemy))
	    {
  	        notify_fail("Nie masz niczego takiego.\n");
	        return 0;
	    }
	    if(sizeof(itemy)>1 || (itemy[0]->query_prop(HEAP_I_IS) && itemy[0]->num_heap()!=1 ))
	    {
    	    notify_fail("Mo�esz rzuci� tylko jednym przedmiotem na raz.\n");
	        return 0;
        }
        if(itemy[0]->query_prop(OBJ_M_NO_DROP))
        {
            notify_fail(itemy[0]->query_prop(OBJ_M_NO_DROP));
            return 0;
        }
	    
        item2 = CMDPARSE_STD->normal_access(item2, 0, this_object());

        if (sizeof(item2)>1)
        {
            notify_fail("Mo�esz rzuci� tylko w jedn� osob� na raz.\n");
	        return 0;
        }
	    if (sizeof(item2)==0)
	    {
	        notify_fail("Nie ma tu nikogo takiego.\n");
	        return 0;
	    }
	    if(!czy_wolne_rece(itemy[0]) || !czy_da_rade(itemy[0]))
    	   	return 0;

        real_throw(itemy[0],item2[0]);
	    return 1;
    }
	else if (parse_command(str, environment(TP), "%i:" + PL_BIE, itemy))
    {
    	itemy = CMDPARSE_STD->normal_access(itemy, "manip_drop_access", this_object());

	    if(sizeof(itemy)>1)
	    {
	        notify_fail("Mo�esz rzuci� tylko jednen przedmiot na raz.\n");
	        return 0;
	    }
        if(itemy[0]->query_prop(OBJ_M_NO_DROP))
        {
            notify_fail(itemy[0]->query_prop(OBJ_M_NO_DROP));
            return 0;
        }

    	if(sizeof(itemy))
	    {
	        TP->set_obiekty_zaimkow(itemy);
		    itemy[0]->move(environment(TP),1);
	        write("Rzucasz " + COMPOSITE_DEAD(itemy, PL_BIE) + " na ziemi�.\n");
	        say(QCIMIE(TP, PL_MIA) + " rzuca " +
		    QCOMPDEAD(PL_NAR) + " na ziemi�.\n");
	        return 1;
	    }
	    else
	    {
       	    notify_fail("Nie masz niczego takiego.\n");
	        return 0;
	    }
    }

    else if(parse_command(str, environment(TP), "%i:" + PL_NAR + " 'w' %w"))
    {
	    notify_fail("W kogo chcesz rzuci�?\n");
		return 0;
    }

    return 0;
}

/** 
 * Funkcja obs�uguj�ca od pocz�tku do ko�ca rzut czym� w co�.
**/
void 
real_throw(object co, object cel)
{
	int hitloc_no;
	mixed *hitloc;
	int v; /* predkosc */
    int rzut; /* czy rzucili�my i trafili�my */

	object co_cel = cel->query_combat_object();

	/* tablica potrzebna do makr opisow */
	object *tab = allocate(1);
	tab[0] = co;

        if(!co->query_prop(OBJ_I_WEIGHT) || 
           !co->query_prop(OBJ_I_VOLUME)) //przed wywo�aniem obj_speed(co)
        {
            write("Wyst�pi� krytyczny b��d w rzucaniu tym obiektem. Zg�o� to "+
                  "natychmiast, podaj�c okoliczno�ci, "+
                 "w jakich do tego dosz�o.\n");
            notify_wizards(co,"Ten obiekt nie ma podstawowych prop�w!!!\n");
            return;
        }

	v = obj_speed(co);

    if(v > 100) 
    {
        TP->catch_msg("Z ogromn� si�� rzucasz " + COMPOSITE_DEAD(tab, PL_NAR)
            + " w " + QIMIE(cel, PL_BIE));
        cel->catch_msg(QCIMIE(TP, PL_MIA) + " rzuca w ciebie " 
            + COMPOSITE_DEAD(tab, PL_NAR) + ", kt�r" + co->koncowka("y","a") + 
            " z ogromn� pr�dko�ci� mknie w twoj� stron�");
        saybb(QCIMIE(TP, PL_MIA) + " z ogromn� si�� rzuca " 
            + COMPOSITE_DEAD(tab, PL_NAR) + " w " + QIMIE(cel, PL_BIE),({cel,TP}));
    }
   else if(v > 20) 
    {
       TP->catch_msg("Rzucasz " + COMPOSITE_DEAD(tab, PL_NAR) + " w " 
            + QIMIE(cel, PL_BIE));
       cel->catch_msg(QCIMIE(TP, PL_MIA) +
            " rzuca w ciebie " + COMPOSITE_DEAD(tab, PL_NAR));
       saybb(QCIMIE(TP, PL_MIA) + " rzuca " + COMPOSITE_DEAD(tab, PL_NAR)
            + " w " + QIMIE(cel, PL_BIE),({cel,TP}));
    }
    else
    {
       TP->catch_msg("Z du�ym wysi�kiem rzucasz " 
            + COMPOSITE_DEAD(tab, PL_NAR) + " w " + QIMIE(cel, PL_BIE));
        cel->catch_msg(QCIMIE(TP, PL_MIA) + " z du�ym wysi�kiem rzuca w ciebie " 
            + COMPOSITE_DEAD(tab, PL_NAR) + ", kt�r" + co->koncowka("y","a") + 
            " wolno szybuje w twoj� stron�");
       saybb(QCIMIE(TP, PL_MIA) + " z du�ym wysi�kiem rzuca " 
            + COMPOSITE_DEAD(tab, PL_NAR) + " w " + QIMIE(cel, PL_BIE),({cel,TP}));
    }

	rzut = throw_try_hit(co,v,cel);
    hitloc_no = losowa_hitlokacja(cel);

    if(hitloc_no == 666) 
        return;   

	if(rzut == 0)
	{
	    write(". Spud�owa�" + TP->koncowka("e�","a�")  + ".\n");
	    cel->catch_msg(". Na szcz�cie niezbyt celnie.\n");
	    saybb(". Niecelnie.\n",({cel,TP}));

        co->move(environment(cel));
	}
	else if(rzut == -1)
	{
        write(", on" + cel->koncowka("","a","o") + " jednak zd��y�" 
            + cel->koncowka("","a","o") + " si� uchyli�.\n");
        cel->catch_msg(", ale zdo�a�" + cel->koncowka("e�","a�") 
            + " si� uchyli�.\n");
        saybb(", on" + cel->koncowka("","a","o") + " jednak zd��y�" 
            + cel->koncowka("","a","o") + " si� uchyli�.\n",({cel,TP}));

        co->move(environment(cel));
    }
	else
	{
        hitloc = co_cel->query_hitloc(hitloc_no);

        write(". Trafi�" + TP->koncowka("e�","a�") 
            + " w " + hitloc[2] + ". ");
        cel->catch_msg(". Dosta�" + cel->koncowka("e�","a�") 
            + " w " + hitloc[2] + ". ");
        saybb(". Trafi�" + TP->koncowka("","a") 
            + " w " + hitloc[2] + ". ",({cel,TP}));

        throw_do_dam(co,v,cel,hitloc_no);

        //troch� konsekwentno�ci. :P Vera
        if(cel->query_prop(NPC_M_NO_ACCEPT_GIVE))
            co->move(environment(cel));
        else
            co->move(cel,1);
	}

    /* jeden punkt zm�czenia za ka�de 10 kg, minimum 1 */
    TP->add_fatigue(- max(1, (co->query_prop(OBJ_I_WEIGHT)/10000)));

}

/** 
 * Okre�la czy trafili�my nasz cel, czy te� mo�e spud�owali�my
 * albo zrobi� on unik. Trafienie zale�y od naszej zr�czno�ci i rzucania,
 * a unik od pr�dko�ci pocisku oraz zr�czno�ci, unik�w i spostrzegawczo�ci
 * celu. Jest te� modyfikator od wielko�ci celu.
 *
 * @param co  - obiekt kt�rym rzucamy
 * @param v - pr�dko�� obiektu kt�rym rzucamy
 * @param cel - cel w kt�ry rzucamy
 * @return -1 gdy cel wykona� unik
 * @return 0 gdy spud�owali�my
 * @return 1 gdy trafili�my
 *
**/
int
throw_try_hit(object co, int v, object cel)
{

	int zr, rzut, spc, objc, unikc, zrc; /* staty i skile rzucajacego i celu*/
	int hit;

    /* cechy rzucaj�cego maj�ce wp�yw na trafienie */
	zr = TP->query_stat(SS_DEX);
	rzut = TP->query_skill(SS_THROWING);

    /* cechy tego w kogo rzucamy */
    zrc = cel->query_stat(SS_DEX);
    spc = TP->query_skill(SS_AWARENESS);
    unikc = cel->query_skill(SS_DEFENSE);
	objc = cel->query_prop(OBJ_I_VOLUME);

    /* jak celnie rzucamy, jak hit >= 100 to zawsze trafimy */    
	hit = zr + rzut + random(10);

	/* za��my, �e standardowo obj�to�� gracza to 50 litr�w 
        je�li jest wi�ksza b�d� mniejsza dodajemy modyfikator do trafienia,
        1 punkt za ka�de 10 litr�w r�nicy  */
    hit += (objc/1000 - 50)/10;

	/* to teraz losujemy czy trafili�my */
	hit  -= random(100);

	/* jesli trafiamy to przeciwnik probuje uniku */
 	if(hit > 0)
	{
		/* czy uda� si� unik (zr�czno��, uniki, spostrzegawczo��) */
		if(v > (zrc + unikc + spc)/2 + random(50))
			return 1;
		else
			return -1;
	}
	else 
        return 0;
}

/** 
 * Funkcja zwraca wylosowany numer hitlokacji (zale�ny od szans trafienia).
 * @param cel - obiekt kt�remu losujemy hitlokacj�
 * @return wylosowany numer hitlokacji 
 * @return 666 (b��d) gdy obiekt nie ma zdefiniowanych hitlokacji
**/
int
losowa_hitlokacja(object cel)
{
    object co_cel = cel->query_combat_object();
    int *hitlocs, sum, i, rand;
    mixed *hitloc;

    rand = random(100);
    hitlocs = co_cel->query_hitloc_id();

    if(sizeof(hitlocs) == 0)
    {
        write(".\nZg�o� b��d w: " + cel->query_name + ". Nie ma zdefiniowanych hitlokacji!\n");
        saybb("\n",({TP}));
        return 666;
    }

    for(i=0,sum=0; sum<=rand; i++)
    {
        hitloc = co_cel->query_hitloc(hitlocs[i]);
        sum += hitloc[1];
    }

    return hitlocs[i-1];
}
 
/** 
 * Przyznaje obra�enia trafionemu celowi, ewentualnie go przewraca.
 * Obra�enia zale�� liniowo od masy pocisku i kwadratowo od pr�dko�ci. 
 * Przed przewr�ceniem broni test zr�czno�ci i akrobatyki.
 *
 * @param co  - obiekt kt�rym rzucamy
 * @param v - pr�dko�� obiektu kt�rym rzucamy
 * @param cel - cel w kt�ry rzucamy
 * @param hitloc_no - w kt�r� hitlokacj� trafili�my
**/
void
throw_do_dam(object co, int v, object cel, int hitloc_no)
{
	object co_cel = cel->query_combat_object();
	object armor, p;
	int dam,ac;
    int zrc, akrobc;

    int waga = co->query_prop(OBJ_I_WEIGHT);

    /* kilogramowy odwa�nik rzucony z pr�dko�ci� 10 zadaje 1 punkt obra�e�,
        z pr�dko�ci� 100 zadaje 100 punkt�w obra�e�*/
	dam = (waga * (v/10) * (v/10))/1000 ;

    //write("\nDEBUG INFO: pr�dko�� - " + v + ", obra�enia (bez zbroi) - " + max(0,dam) + ".");

    if (function_exists("create_object", co) == "/std/weapon")
    {
        if(co->query_dt() == W_IMPALE)
        {
                ;
        }
        else if(co->query_dt() == W_SLASH)
        {
                ;
        }
        dam += co->query_pen();
    }

	if(objectp(co_cel->cb_query_armour(hitloc_no)))
	{
		armor = co_cel->cb_query_armour(hitloc_no);
		ac = armor->query_ac(hitloc_no,W_IMPALE);

        cel->catch_msg(armor->query_nazwa(PL_MIA) + " zatrzyma�" + armor->koncowka("","a","o") + " pocisk. ");
        dam -= ac;
	}

    write("\n");
    cel->catch_msg("\n");
    saybb("\n",({cel, TP}));

    cel->heal_hp(-max(0, dam));

    /* teraz sprawdzamy przewr�cenie */    
    /* cechy celu, mo�e pozwol� mu utrzyma� si� na nogach */
    zrc = cel->query_stat(SS_DEX);
    akrobc = cel->query_skill(SS_ACROBAT);

    if((waga/1000) * (v/10) > zrc + akrobc)
            przewroc(cel);
}

/** 
 * Wywo�ywana aby przewr�ci� obiekt (kt�ry nie le�y ani nie siedzi).
 * @param cel - living kt�rego przewracamy 
**/
void przewroc(object cel)
{
    object p;    

    if(cel->query_prop("_przewrocony") != 0)
       return;
    
    if(cel->query_prop("_sit_siedzacy") != 0)
       return;

    cel->add_prop("_przewrocony", 1);
    write(cel->query_Imie(cel, PL_MIA) + " przewraca si�.\n");
    cel->catch_msg("Tracisz r�wnowag� i przewracasz si�.\n");
    saybb(cel->query_Imie(cel, PL_MIA) + " rozpaczliwie zamacha�" + cel->koncowka("","a","o") 
        + " r�kami i przewr�ci�" + cel->koncowka("","a","o") 
        + " si� jak sta�" + cel->koncowka("","a","o") + ".\n", ({cel, TP}));

    seteuid(getuid(this_object()));
    p = clone_object("/std/paralize/lezenie.c");
    p->move(cel, 1);
    cel->command("przestan atakowac wszystkich"); //ze wzgledu na upadek :P Vera
}

/**
 * Oblicza pr�dko�� z jak� poszybuje rzucony przez nas obiekt.
 * @param co - obiekt kt�rym rzucamy
 * @return predko�� (co najmniej 10, ale raczej mniej ni� 200)
**/
int
obj_speed(object co)
{

	int sila, rzut, moc; /* skile rzucajacego */
    float zmeczenie, kondycja; /* stopien zmeczenia i poranienia rzucajacego */
	int waga, obj; /* cechy rzucanego obiektu */
    float  gestosc; 
	
    int v; /* predkosc kt�r� w�a�nie wyliczamy*/

	sila = TP->query_stat(SS_STR);
	rzut = TP->query_skill(SS_THROWING);

	kondycja = itof(TP->query_hp())/itof(TP->query_max_hp());
	zmeczenie = itof(TP->query_fatigue())/itof(TP->query_max_fatigue());

    /* zale�nie od zm�czenia i poranienia */
	if((kondycja += 0.5) > 1.0)
		kondycja = 1.0;
	if((zmeczenie += 0.5) > 1.0)
		zmeczenie = 1.0;

    /* takiego powera ma nasz gracza */
	moc = ftoi(itof((sila + rzut)/2) * kondycja * zmeczenie);
        

    /* teraz przyjrzyjmy si� obiektowi kt�rym rzucamy */
    waga = co->query_prop(OBJ_I_WEIGHT);
    obj = co->query_prop(OBJ_I_VOLUME);

    gestosc = itof(waga)/itof(obj); 

    /* dla zbalansowania */
    if(gestosc > 4.0)
        gestosc = 4.0;        

    /* i rzucamy - por�wnujemy moc gracza z mas� obiektu,
        i modyfikujemy w zale�no�ci od g�sto�ci obiektu */     
    v = moc - (waga/1000);
    v = ftoi(itof(v) * sqrt(gestosc));     

    if(v < 10)
        v = 10;

    v += random(10);
    
	return v;
}

/** 
 * Sprawdza czy gracz ma wolne r�ce.
 * Je�li przedmiot jest niewielki mo�na rzuci� jedn� r�k�.
**/
int
czy_wolne_rece(object co)
{

	if((co->query_wielded()) == TP)
	{
        if(co->unwield_me())
            return 1;
        else
        {
            notify_fail("Nie idzie tym rzuci�.\n");
            return 0;
        }
	}
    else if((co->query_worn()) == TP)
    {
        if(co->remove_me())
            return 1;
        else
        {
            notify_fail("Nie idzie tego zdj��.\n");
            return 0;
        }
    }
    else if(HAS_FREE_HANDS(TP))
        return 1;
	else if(((co->query_prop(OBJ_I_VOLUME))>1000 +
		50*(TP->query_stat(SS_DEX))) && !HAS_FREE_HANDS(TP) )
	{
		notify_fail("�eby tym rzuci� musisz mie� wolne obie r�ce.\n");
		return 0;
	}
	else if(!HAS_FREE_HAND(TP))
	{
		notify_fail("Nie masz wolnych r�k.\n");
		return 0;
	}
	else
		return 1;

}

/**
 * Sprawdza czy gracz ma do�� si�y i nie jest do�� zm�czony by rzuca�.
 */
int
czy_da_rade(object co)
{

	if((TP->query_stat(SS_STR)) * 1000 < co->query_prop(OBJ_I_WEIGHT))
    {
	   notify_fail("Nie jeste� wystarczaj�co siln" + TP->koncowka("y","a","e") + ".\n");
        return 0;
    }

    if(TP->query_fatigue() < 5)
    {
       notify_fail("Jeste� zbyt zm�czon" + TP->koncowka("y","a","e") + ".\n");
        return 0;
    }

    return 1;
}


/*
 * Function name: manip_drop_access
 * Description:   Test if player carries an object
 * Arguments:	  ob: object
 * Returns:       1: caried object
 *		  0: otherwise
 */
int
manip_drop_access(object ob)
{
    if (!objectp(ob))
		return 0;
    if (environment(ob) == TP)
		return 1;
    return 0;
}
