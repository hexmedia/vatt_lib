/*
 * Drobne zmiany: skoro czas rzeczywisty - czemu rozk�adamy si� tak szybko? ^_^
 *                      + mozliwosc kopania cial
 *
 *			Niestety, mozna kopac tylko 'cialo' lub 'cialo <pelny short>',
 *				nie mozna zas np. "cialo dziewczynki"
 *                   Lil. 23.08.2005
 * 
 * /std/corpse.c
 *
 * This is a decaying corpse. It is created automatically
 * when a player or monster die.
 *
 * Troch� zmian tu i tam (cia�a ju� short 'cia�o wiewi�rki')
 * no i eventy, opisy longi, shorty, wilki i ghoule by Vera.
 *
 */

#pragma save_binary
#pragma strict_types

inherit "std/container";

#include <cmdparse.h>
#include <composite.h>
#include <files.h>
#include <filter_funs.h>
#include <language.h>
#include <macros.h>
#include <ss_types.h>
#include <stdproperties.h>
#include <wa_types.h>
#include <object_types.h>


/* rozk�ad trwa 2 doby */
#define DECAY_TIME	2880 

#define DBG(x)		find_player("vera")->catch_msg(x);
#define BYL_EVENT_JEDZONKO	"_byl_event_jedzonko"
#define WILK_PATH	"/d/Standard/obj/wilk_event_trupy.c"
#define GHOUL_PATH	"/d/Standard/obj/ghoul_event_trupy.c"

int             decay;
int             decay_id;
int 		known_flag;	/* 1 - neverknown, 2 - alwaysknown */
string          *met_name, *nonmet_name, *nonmet_pname, *state_desc, 
		*pstate_desc;
mixed		leftover_list;
string		file;
string		*tmp=({}), *tmp2=({});

int		kiedy_event = 41+random(41); //w jakim procentowym stopniu
				//rozkladu maja sie pojawic ghoule lub wilki
int		ktory_event = kiedy_event > 64 ? 1 : 0; //1 to wilki, 0 to ghoul
				//ghoul lubi bardziej rozlozone trupki ;)
/*
 * Prototypy
 */
public int	get_leftover(string arg);
int		kick_corpse(string arg);
static int	move_out(object ob);
void		decay_fun();
public int	get_leftover(string arg);
void		decay_fun();

/*
 * Pomocnicza funkcyjka, pierdolka. Procentowo podaje decay
 */
int
get_proc_decay()
{
    return (decay * 100) / DECAY_TIME;
}

void
init()
{
    ::init();

    add_action(get_leftover,	"wyrwij");
    add_action(get_leftover,	"wytnij");
    add_action(kick_corpse,	"kopnij");
}

nomask void
create_container()
{
    add_prop(OBJ_I_SEARCH_TIME, 2);
    add_prop(CONT_I_DONT_SHOW_CONTENTS, 1); /* Sami wypiszemy zawartosc */

    this_object()->create_corpse();

    decay = DECAY_TIME;
}

nomask void
reset_container()
{
    this_object()->reset_corpse();
}

void set_file(string str)
{
    file = str;
}

void
ustaw_imie_denata(string *n)
{
    int i;

	string *temp, *temp2;

    object pob;
    
    pob = previous_object();
    
    if (pob->query_prop(LIVE_I_NEVERKNOWN))
        known_flag = 1;
    else if (pob->query_prop(LIVE_I_ALWAYSKNOWN))
        known_flag = 2;
    else known_flag = 0;
    
    temp = ({ "cia�o", "cia�a", "cia�u", "cia�o", "cia�em", "ciele", 
        "cia�a", "cia�", "cia�om", "cia�a", "cia�ami", "cia�ach" });
        
    temp2 = temp + ({ });

    state_desc = temp[0..5];
    pstate_desc = temp[6..11];

    ustaw_nazwe(temp[0..5], temp[6..11], PL_NIJAKI_NOS);
    
    nonmet_name = allocate(6);
    nonmet_pname = nonmet_name + ({ });
    met_name = nonmet_name + ({ });
    
    if (known_flag != 2)
    {
        for (i = 0; i < 6; i++)
        {
            nonmet_name[i] = pob->query_nonmet_name(i);
            nonmet_pname[i] = pob->query_nonmet_pname(i);
        }

        for (i = 0; i < 6; i++)
        {
            temp2[i] = temp2[i] + " " + nonmet_name[PL_DOP];
            temp2[i + 6] = temp2[i + 6] + " " + nonmet_pname[PL_DOP];
        }

        dodaj_nazwy(temp2[0..5], temp2[6..11], PL_NIJAKI_NOS);
    }

    if (known_flag != 1)
    {
        for (i = 0; i < 6; i++)
            met_name[i] = lower_case(n[i]);

        for (i = 0; i < 6; i++)
        {
            temp[i] = temp[i] + " " + met_name[PL_DOP];
            temp[i+6] = temp[i + 6] + " " + met_name[PL_DOP];
        }

        dodaj_nazwy( temp[0..5], temp[6..11], PL_NIJAKI_NOS);
    }

    temp = ({ "szcz�tki", "szcz�tk�w", "szcz�tkom", "szcz�tki",
        "szcz�tkami", "szcz�tkach" });
    temp2 = temp + ({ });

    dodaj_nazwy(temp, PL_MESKI_NOS_NZYW);
    
    if (known_flag != 2)
    {
        for (i = 0; i < 6; i ++)
            temp2[i] = temp2[i] + " " + nonmet_name[PL_DOP];

        dodaj_nazwy(temp2, PL_MESKI_NOS_NZYW);
    }

    if (known_flag != 1)
    {
        for (i = 0; i < 6; i ++)
        {
            temp[i] = temp[i] + " " + met_name[PL_DOP];
        }

        dodaj_nazwy(temp, PL_MESKI_NOS_NZYW);
    }

    ustaw_shorty( ({ "@@short_func|0", "@@short_func|1", "@@short_func|2",
        "@@short_func|3", "@@short_func|4", "@@short_func|5" }), 
        ({"@@pshort_func|0", "@@pshort_func|1", "@@pshort_func|2",
        "@@pshort_func|3", "@@pshort_func|4", "@@pshort_func|5" }), 
        PL_NIJAKI_NOS);
        
    set_long("@@long_func");
    
    change_prop(CONT_I_WEIGHT, 50000);
    change_prop(CONT_I_VOLUME, 50000);
    decay_id = set_alarm(60.0, 60.0, decay_fun);
}


//dodajemy 'cialo krowy', a nie tylko 'cialo xxx yyy krowy'. V.
int
dodaj_jeszcze_jedna_nazwe()
{
	tmp = ({"cia�o "+query_prop(CORPSE_M_RACE)[1],
		"cia�a "+query_prop(CORPSE_M_RACE)[1],
		"cia�u "+query_prop(CORPSE_M_RACE)[1],
		"cia�o "+query_prop(CORPSE_M_RACE)[1],
		"cia�em "+query_prop(CORPSE_M_RACE)[1],
		"ciele "+query_prop(CORPSE_M_RACE)[1] });
	tmp2 = ({"cia�a "+query_prop(CORPSE_M_PRACE)[1],
		"cia� "+query_prop(CORPSE_M_PRACE)[1],
		"cia�om "+query_prop(CORPSE_M_PRACE)[1],
		"cia�a "+query_prop(CORPSE_M_PRACE)[1],
		"cia�ami "+query_prop(CORPSE_M_PRACE)[1],
		"cia�ach "+query_prop(CORPSE_M_PRACE)[1] });

    return dodaj_nazwy(tmp,tmp2,PL_NIJAKI_NOS);
}



/*
 * Nazwa funkcji : set_decay
 * Opis          : Ustawia poziom rozk�adu cia�a. Cia�o zaczyna rozk�ada�
 *		   si� na poziomie 10 (nieprawda, bo 2880.Lil), zupe�nie rozk�ada si� przy zej�ciu do 0.
 * Argumenty     : int - stopie� rozk�adu.
 */
void
set_decay(int d)
{
    decay = d;
}

/*
 * Nazwa funkcji : query_decay
 * Opis          : Zwraca stopie� rozk�adu cia�a. Cia�o zaczyna rozk�ada� si�
 *		   na poziomie 10(nieprawda, bo 2880.Lil), za� zupe�nie rozk�ada si� przy zej�ciu
 *		   do 0.
 * Funkcja zwraca: int - stopie� rozk�adu cia�a.
 */
int
query_decay()
{
    return decay;
}

varargs string
short_func(string prz)
{
    object pob;
    int przyp = atoi(prz);
    string str;
    
    pob = vbfc_caller();

    if (!pob || !query_ip_number(pob) || pob == this_object())
	pob = this_player();

    if (pob && pob->query_real_name() == lower_case(met_name[0]))
    {
        switch (query_rodzaj())
        {
             case PL_NIJAKI_NOS:
                switch (przyp)
		{
		    case PL_MIA:
		    case PL_BIE: str = "twoje"; break;
		    case PL_DOP: str = "twojego"; break;
		    case PL_CEL: str = "twojemu"; break;
		    case PL_NAR:
		    case PL_MIE: str = "twoim"; break;
		}
		break;
	    case PL_ZENSKI:
                switch (przyp)
		{
		    case PL_MIA:
		    case PL_BIE:
		    case PL_NAR: str = "twoja"; break;
		    case PL_DOP:
		    case PL_CEL:
		    case PL_MIE: str = "twojej"; break;
		}
        }
        
        return str + " " + state_desc[przyp];
    }
    else
    	if ( (known_flag != 1) && 
        ((known_flag == 2) || 
         (pob && pob->query_met(met_name[0]))) ) 
	    return state_desc[przyp] + " " + capitalize(met_name[PL_DOP]);
    else
	return state_desc[przyp] + " " + nonmet_name[PL_DOP];
}

string
pshort_func(string prz)
{
    object pob;
    int przyp = atoi(prz);

    pob = vbfc_caller();
    if (!pob || !query_ip_number(pob) || pob == this_object())
	pob = this_player();
    if (pob && pob->query_real_name() == lower_case(met_name[0]))
	return ({ "twoje", "twoich", "twoim", "twoje", "twoimi", 
            "twoich" })[przyp] + " " + pstate_desc[przyp];
    else
        if ( (known_flag != 1) && 
             ((known_flag == 2) || 
              (pob && pob->query_met(met_name[0]))) ) 
	return pstate_desc[przyp] + " " + capitalize(met_name[PL_DOP]);
    else
	return pstate_desc[przyp] + " " + nonmet_pname[PL_DOP];
}

string
long_func()
{
    object pob, *inv;
    string ret;

    pob = vbfc_caller();
    if (!pob || !query_ip_number(pob) || pob == this_object())
	pob = this_player();
    if (pob->query_real_name() == lower_case(met_name[0]))
	ret = "Jest to twoje w�asne cia�o.\n";
    else if (pob->query_met(met_name[0]))
	ret = "Jest to martwe cia�o " + capitalize(met_name[PL_DOP]) + ".\n";
    else
	ret = "Jest to martwe cia�o " + nonmet_name[PL_DOP] + ".\n";

    inv = all_inventory(this_object());
    if (!sizeof(inv))
        return ret;
    return ret + "Zauwa�asz przy " + query_zaimek(PL_MIE) + " " +
	FO_COMPOSITE_DEAD(inv, pob, PL_BIE) + ".\n";
}

int
query_corpse()
{
    return 1;
}

void
zarzuc_temat(int nr_eventu)
{
//DBG("zarzuc_temat\n");
    //nawet je�li ghoul, to wymu� wilki je�li dzie�
    if(ENV(TO)->jest_dzien())
        nr_eventu = 1;

    object *trupki = filter(all_inventory(ENV(TO)),&->query_corpse());

    //losujemy
    int ile_wilkow = random(5) + 2;
    int ile_ghouli = random(2) + 1;

    //w og�le to musz� by� przynajmniej 2 �eby cokolwiek si� dzia�o
    if(sizeof(trupki) < 2)
	    return;

    if(query_prop(BYL_EVENT_JEDZONKO))
	    return;

    //tylko wi�ksze cia�ka
    if(query_prop(CONT_I_VOLUME) < 900)
		return;

    //tylko na specjalnego typu lokacjach event ten sie pojawi
    if(member_array(ENV(TO)->query_prop(ROOM_I_TYPE),
	({ROOM_BEACH,ROOM_FOREST,ROOM_FIELD,ROOM_MOUNTAIN,
	 ROOM_TRACT})) == -1)
	    return;

    object *npce = ({ });
    switch(nr_eventu)
    {
        case 1: //wilki
			for(int z = 0 ; z < ile_wilkow ; z++)
		    	    npce += ({clone_object(WILK_PATH)});
			//map(npce[1..],&(npce[0])->team_join());
			break;
		case 0: //ghoule	
			for(int z = 0 ; z < ile_ghouli ; z++)
		    	    npce += ({clone_object(GHOUL_PATH)});
			//map(npce[1..],&(npce[0])->team_join());
			break;

		default: break;
    }

	foreach(object x : npce[1..])
		npce[0]->team_join(x);

    //sprytny myk: npce przybywaja, jesli sie uda;)
    string *lokacje = ENV(TO)->query_exit_rooms();
    object skad;
    //no chyba, �e na tej lokacji nikogo ni ma, to po co? ;)
    if(!sizeof(FILTER_PLAYERS(all_inventory(ENV(TO)))))
        skad = ENV(TO);
    int tt = 0;
    while(sizeof(lokacje) && !objectp(skad))
    {
        if(!sizeof(FILTER_PLAYERS(all_inventory(find_object(lokacje[tt])))))
    	    skad = find_object(lokacje[tt]);
	else
	    lokacje -= ({ lokacje[tt] });

        tt++;
    }
//DBG("lok: "+file_name(skad)+"\n");
    //je�li si� nie uda�o (tj. na wszystkich s�siednich lokacjach s� gracze)
    if(!objectp(skad))
	    skad=ENV(TO);

	string kierunek = znajdz_sciezke(skad,ENV(TO))[0];
	//int kierunek = 	member_array("/"+file_name(ENV(TO))+".c",
					//ENV(npce[0])->query_exit_rooms());

    foreach(object x : npce)
		x->move(skad);

    if(skad == ENV(TO))
	    tell_room(ENV(TO),UC(COMPOSITE_LIVE(npce,PL_MIA)+" przybywa"+
		(sizeof(npce) > 1 ? "j�" : "")+
		"wiedzion"+(sizeof(npce) > 1 ? "e" : "y") +" zapachem padliny.\n"));
	else
		npce[0]->command(kierunek);
		//npce[0]->command(ENV(npce[0])->query_exit_cmds()[kierunek]);
//DBG(kierunek+"\n"+ile_wilkow+ile_ghouli+"\n");
    foreach(object x : trupki)
	    x->add_prop(BYL_EVENT_JEDZONKO,1);
}	

/*
 * Nazwa funkcji : decay_fun
 * Opis          : Funkcja rozkladajaca cialo o jeden stopien. Przy zejsciu
 *		   na poziom 3(nieprawda, bo 60.Lil) cialo rozklada sie na 'resztki'. Przy zejsciu
 *		   na 0 i te sie rozkladaja.
 */
void
decay_fun()
{
    object *ob, obj;
    string desc;
    int i, j, flag;

    decay--;

    //jaki� tam evencik, zwyk�y napisik..
    if(get_proc_decay() < 95)
    {
     switch(random(20))
     {
	case 0:
	    tell_room(ENV(TO),
		"Czujesz smr�d padliny.\n"); break;
	case 1:
	    tell_room(ENV(TO),
		"Powietrze wype�nia fetor zgni�ego mi�sa.\n"); break;
	case 2:
		if(!sizeof(FILTER_IS_SEEN(TO,({TP}))))
	    tell_room(ENV(TO),
		"Uderza w ciebie smr�d zgnilizny pochodz�cy z "+
		QSHORT(TO,PL_DOP)+".\n");
		else
		tell_room(ENV(TO),
		"Uderza w ciebie smr�d zgnilizny.\n");
		 break;
	case 3:
		if(!sizeof(FILTER_IS_SEEN(TO,({TP}))))
		{
	    if(get_proc_decay() > 20)
		    tell_room(ENV(TO),
			"�cierwo "+query_prop(CORPSE_M_RACE)[1]+
			" cuchnie okropnie.\n");
		}
		else
		tell_room(ENV(TO),"Co� tu okropnie cuchnie.\n");
 break;
	//case 4:
	default: break;
     }
    }

    //fiufiu, no to jedziemy z koksem
    if(get_proc_decay() == kiedy_event)
        zarzuc_temat(ktory_event);

    switch(get_proc_decay())
    {
	case 98:
	ustaw_shorty( tmp, tmp2, PL_NIJAKI_NOS);
//DBG("1\n");
	break;

	case 80:
	if(!random(5)) //evencik
		if(!sizeof(FILTER_IS_SEEN(TO,({TP}))))
		tell_room(ENV(TO),"Cia�o onegdaj �ywe z ka�d� chwil� "+	
			"coraz bardziej przypomina zwyk�� kup� lekko nadgni�e"+
			"go ju� mi�sa.\n");
	break;

	case 61:
	if(!random(5)) //evencik
		tell_room(ENV(TO),"");
	break;

	case 20:
	if(!random(5)) //evencik
		if(!sizeof(FILTER_IS_SEEN(TO,({TP}))))
		tell_room(ENV(TO),"Zw�oki "+query_prop(CORPSE_M_RACE)[1]+
			" z biegiem czasu zamieniaj� si� w trudne "+
			"do zidentyfikowania, cuchn�ce �cierwo.\n");
	ustaw_shorty("�cierwo");
	dodaj_nazwy("�cierwo");
        break;	

	case 6:
	ob = filter(all_inventory(this_object()), move_out);
        /* fix this to get singular/plural of 'appear' */
	i = ((sizeof(ob) != 1) ? sizeof(ob) :
             ((ob[0]->query_prop(HEAP_I_IS)) ? (int)ob[0]->num_heap() : 1));

	if(!sizeof(FILTER_IS_SEEN(TO,({TP}))))
	tell_roombb(environment(this_object()), QCSHORT(this_object(), PL_MIA) + 
            " rozk�ada si�, pozostawiaj�c po sobie " +
            (i ? "sm�tne resztki, w postaci " + COMPOSITE_DEAD(ob, PL_DOP)
               : "tylko sm�tne resztki") + ".\n", ({}), this_object());
               
	state_desc = ({ "sterta", "sterty", "stercie", "sterte", "stert�",
            "stercie" });

	pstate_desc = ({ "sterty", "stert", "stertom", "sterty",
            "stertami", "stertach" });

        ustaw_nazwe(state_desc, pstate_desc, PL_ZENSKI);
        
	ustaw_shorty( ({ "@@short_func|0", "@@short_func|1", "@@short_func|2",
            "@@short_func|3", "@@short_func|4", "@@short_func|5" }), 
            ({ "@@pshort_func|0", "@@pshort_func|1", "@@pshort_func|2",
            "@@pshort_func|3", "@@pshort_func|4", "@@pshort_func|5" }), 
            PL_ZENSKI);

                for (i = 0; i < 6; i++)
        {
            state_desc[i] = state_desc[i] + " rozk�adaj�cych si� szcz�tk�w";
            pstate_desc[i] = pstate_desc[i] + " rozk�adaj�cych si� szcz�tk�w";
        }
	break;


    }

	

    
    if (decay > 0)
	return;

    for (i = 0; i < sizeof(leftover_list); i++)
    {
	if (leftover_list[i][4])
	{
	    for (j = 0; j < leftover_list[i][2]; j++)
	    {
		obj = clone_object(leftover_list[i][0]);
		obj->leftover_init(leftover_list[i][1], 
		  query_prop(CORPSE_M_RACE), query_prop(CORPSE_I_RRACE) - 1,
		  leftover_list[i][4],query_prop(CONT_I_WEIGHT));
		obj->move(environment(this_object()), 0);
		flag = 1;
	    }
	}
    }

    if (flag)
		if(!sizeof(FILTER_IS_SEEN(TO,({TP}))))
        tell_roombb(environment(this_object()),
              QCSHORT(this_object(), PL_MIA) +
                 " rozpada si�.\n", ({}), this_object());

    map(all_inventory(this_object()), move_out);
    remove_object();
}

static int
move_out(object ob)
{
    if (ob->move(environment(this_object())))
	return 0;
    else
	return 1;
}

void
remove_object()
{
    map(all_inventory(this_object()), move_out);

    ::remove_object();
}

varargs mixed
query_race(int przyp)
{
    return query_prop(CORPSE_M_RACE)[przyp];
}

varargs mixed
query_prace(int przyp)
{
    return query_prop(CORPSE_M_PRACE)[przyp];
}

/*
 * Function name: add_leftover
 * Description:   Add leftovers (at death) to the body.
 * Arguments:	  leftover - The leftover list to use.
 */
varargs public void
add_leftover(mixed list)
{
    leftover_list = list;
}

/*
 * Function name: query_leftover
 * Description:   Return the leftover list. If an organ is specified, that
 *		  actual entry is looked for, otherwise, return the entire
 *		  list.
 *		  The returned list contains the following entries:
 *		  ({ objpath, organ, nitems, vbfc, hard, cut})
 * Arguments:	  organ - The organ to search for W MIANOWNIKU.
 */
varargs public mixed
query_leftover(string organ, int przyp = PL_MIA)
{
    int i, j;
    mixed ret = ({ });

  if (!sizeof(leftover_list))
    return ({ });

  if (!strlen(organ))
    return leftover_list;

  for (i = 0; i < sizeof(leftover_list); i++) {
    if (przyp == PL_MIA) {
      if (organ ~= leftover_list[i][1])
        return ({ leftover_list[i] });
    }
    else {
      mixed odm = SLOWNIK->query_odmiana(leftover_list[i][1]);
      if (!odm) {
        throw("Nie ma nazwy '" + leftover_list[i][1] + "' w s�owniku.\n");
        return 0;
      }
      if (organ ~= odm[0][przyp])
        return ({ leftover_list[i] });
      if (organ ~= odm[1][przyp]) {
          for (j = 0; j < leftover_list[i][2]; ++j)
          ret += ({ leftover_list[i] });
      }
    }
  }
    return ret;
}

/*
 * Function name: remove_leftover
 * Description:   Remove a leftover entry from a body.
 * Arguments:	  organ - Which entry to remove W MIANOWNIKU.
 * Returns:       1 - Ju�, removed, 0 - Not found.
 */
public int
remove_leftover(string organ)
{
    int i;

    if (!sizeof(leftover_list))
	return 0;

    leftover_list = filter(leftover_list, operator(!) @ &operator(~=)(organ,) @ &operator([])(,1));
//    for (i = 0; i < sizeof(leftover_list); i++)
//	if (leftover_list[i][1][PL_BIE] == organ)
//	    leftover_list = leftover_list[0..(i - 1)] +
//		leftover_list[(i + 1)..(sizeof(leftover_list))];
}

/*
 * Function name: get_leftover
 * Description:   Get leftovers from the body.
 */
public int
get_leftover(string arg)
{
    mixed	corpses, leftovers;
    object 	*found, *weapons, theorgan;
    int		i, slash;
    string	organ, vb, fail;
    
    if (this_player()->query_prop(TEMP_STDCORPSE_CHECKED))
	return 0;

    vb = query_verb();
    
    notify_fail(capitalize(vb) + " co z czego?\n");  /* access failure */
    if (!arg)
	return 0;

    if (!parse_command(arg, environment(this_player()), "%s 'z' %i:" + PL_DOP,
		organ, corpses))
	return 0;

    if (!strlen(organ))
        return 0;

    if (this_player()->query_attack())
    {
        write("Jeste� w trakcie walki - lepiej skoncentruj si� na niej, " +
            "bo inaczej bedziesz wygl�da� jak " + short(PL_MIA) + "!\n");
        return 1;
    }

    found = VISIBLE_ACCESS(corpses, "find_corpse", this_object());
    
    if (sizeof(found) == 1)
    {
	switch(vb)
	{
	case "wytnij":
	    slash = 0;
	    if (!sizeof(weapons = this_player()->query_weapon(-1)))
	    {
        	notify_fail("@@get_fail:" + file_name(this_object()) +
        	    "|Warto by trzyma� w r�ce co�, czym mo�na by wyci��, " +
        	    "nie uwa�asz?\n@@");
        	return 0;
	        
	    }
	    
	    for (i = 0 ; i < sizeof(weapons) ; i++)
		if (weapons[i]->query_dt() & W_SLASH)
		    slash = 1;
		    
	    if (slash == 0)
            {
        	notify_fail("@@get_fail:" + file_name(this_object()) +
        	    "|Je�li chcesz co� wyci��, poszukaj lepiej czego� " +
        	    "ostrzejszego.\n@@");
        	return 0;
            }

	    break;

	case "wyrwij":
	default:
	    slash = 0;
	    break;
	}

	if (vb == "wytnij" && slash == 0)
	{
	    notify_fail("@@get_fail:" + file_name(this_object()) +
		"|Je�li chcesz co� wyci��, poszukaj lepiej czego� " +
		"ostrzejszego.\n@@");
	    return 0;
	}
	
	leftovers = found[0]->query_leftover(organ, PL_BIE);
	if (!sizeof(leftovers))
	{
	    notify_fail("@@get_fail:" + file_name(this_object()) +
		"|W " + found[0]->short(PL_MIE) + 
		" nie ma niczego takiego.\n@@");
	    return 0;
	}

	foreach (mixed leftover : leftovers)
	{
	
	    if (!sizeof(leftover) || leftover[2] == 0)
	    {
		notify_fail("@@get_fail:" + file_name(this_object()) +
		    "|W " + found[0]->short(PL_MIE) + 
		    " nie ma niczego takiego.\n@@");
		return 0;
	    }

	    if (leftover[5] && slash == 0)
	    {
		notify_fail("Nie mo�esz tego tak po prostu wyrwa�. U�yj no�a, " +
	    	    "albo czego� w tym stylu...\n");
		return 0;
	    }

	    if (strlen(leftover[3]))
		fail = check_call(leftover[3]);

	    if (strlen(fail))
	    {
		notify_fail("@@get_fail:" + file_name(this_object()) + "|" +
		    fail + ".\n@@");
		return 0;
	    }

	    if(leftover[2]-- == 0)
		remove_leftover(leftover[1]);

	    theorgan = clone_object(leftover[0]);
	    theorgan->set_type(leftover[7]);
	    if (leftover[6]) {
    		theorgan->remove_prop(OBJ_M_NO_SELL);
    		theorgan->add_prop(OBJ_I_VALUE, leftover[6]);
	    }
	    theorgan->leftover_init(leftover[1], 
		found[0]->query_prop(CORPSE_M_RACE),
	        found[0]->query_prop(CORPSE_I_RRACE) - 1,
		leftover[4],query_prop(CONT_I_WEIGHT));
	    if (theorgan->move(this_player(), 0))
		theorgan->move(environment(this_player()));
	
	    this_player()->set_obiekty_zaimkow(({ theorgan }), found);
	
	    vb = (vb == "wytnij" ? "wycina" : "wyrywa");
	
	    organ = SLOWNIK->query_odmiana(leftover[1])[0][PL_BIE];
	    saybb(QCIMIE(this_player(), PL_MIA) + " " + vb + " " + organ +
		" z " + QSHORT(found[0], PL_DOP) + ".\n");
	    write(capitalize(vb) + "sz " + organ + " z " + found[0]->short(PL_DOP) 
		+ ".\n");
	    
	}
	
	return 1;
    }

    set_alarm(0.5, 0.0, &(this_player())->remove_prop(TEMP_STDCORPSE_CHECKED));
    this_player()->add_prop(TEMP_STDCORPSE_CHECKED, 1);
    if (sizeof(found))
	notify_fail("@@get_fail:" + file_name(this_object()) +
	    "|Spr�buj bardziej sprecyzowa�, o kt�re cia�o ci chodzi.\n@@");
    else
	notify_fail("@@get_fail:" + file_name(this_object()) +
	    "|Nie widzisz nigdzie w okolicy takiego cia�a.\n@@");
    return 0;
}

string
get_fail(string fault)
{
    return fault;
}

public int
find_corpse(object ob)
{
    if ((function_exists("create_container") == CORPSE_OBJECT) &&
	((environment(ob) == this_player()) ||
	 (environment(ob) == environment(this_player()))))
    {
	return 1;
    }

    return 0;
}


/*
 * Function name: search_fun
 * Description:   This function is called when someone searches the corpse
 *		  as set up in create_container()
 * Arguments:	  player - The player searching
 *		  str    - If the player specifically said what to search for
 * Returns:       A string describing what, if anything the player found.
 */
string
search_fun(string str, int trail)
{
  mixed left;
  string *found;
  int i;

  left = query_leftover();
  found = ({});

  for (i = 0; i < sizeof(left); i++)
  {
    mixed odm = SLOWNIK->query_odmiana(left[i][1]);
    if (!odm) {
      throw("Nie ma nazwy '" + left[i][1] + "' w s�owniku.\n");
      return 0;
    }
    if (left[i][2] == 1) {
      found += ({ odm[0][PL_BIE] });
    }
    else if (left[i][2] > 1) {
      found += ({ LANG_SNUM(left[i][2], PL_BIE, odm[2]-1) + " " + 
          odm[1][LANG_PRZYP(left[i][2], PL_BIE, odm[2]-1)] });
    }
  }

  if (sizeof(found) > 0)
    return "Po dok�adnym przeszukaniu cia�a stwierdzasz, �e " +
      this_player()->koncowka("m�g�by�", "mog�aby�") + " wyrwa� lub wyci�� " +
      COMPOSITE_WORDS(found) + ".\n";

  return ::search_fun(str, trail);
}

/*Lil*/
int
kick_corpse(string arg)
{
	string vb;
	mixed corpses;
	object *found;

  vb = query_verb();
	
 notify_fail(capitalize(vb) + " kogo [gdzie] ?\n");
    if (!arg)
	return 0;

  if (!parse_command(arg, environment(this_player()), "%i:" + PL_BIE,
		corpses))
	return 0;

  found = VISIBLE_ACCESS(corpses, "find_corpse", this_object());

  if (sizeof(found) == 1)
  {
    saybb(QCIMIE(this_player(), PL_MIA) + " kopie brutalnie " + 
		  QSHORT(found[0], PL_BIE) + ".\n");
    write("Kopiesz brutalnie " + found[0]->short(PL_BIE) 
	    + ".\n");
	return 1;
  }
}

public string
query_corpse_auto_load()
{
    string toReturn = " @C@ ";
    return toReturn;
}

public string
init_corpse_arg(string arg)
{
    if (arg == 0) {
        return 0;
    }
}

public string
query_auto_load()
{
    return 0;
//    return ::query_auto_load() + query_corpse_auto_load();
}

public string
init_arg(string arg)
{
    return ::init_arg(arg);
//    return init_corpse_arg(::init_arg(arg));
}
