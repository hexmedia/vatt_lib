/*
 * Drobne zmiany: skoro czas rzeczywisty - czemu rozk�adamy si� tak szybko? ^_^
 *                      + mozliwosc kopania cial
 *
 *			Niestety, mozna kopac tylko 'cialo' lub 'cialo <pelny short>',
 *				nie mozna zas np. "cialo dziewczynki"
 *                   Lil. 23.08.2005
 * 
 * /std/corpse.c
 *
 * This is a decaying corpse. It is created automatically
 * when a player or monster die.
 */

#pragma save_binary
#pragma strict_types

inherit "std/container";

#include <cmdparse.h>
#include <composite.h>
#include <files.h>
#include <filter_funs.h>
#include <language.h>
#include <macros.h>
#include <ss_types.h>
#include <stdproperties.h>
#include <wa_types.h>
#include <object_types.h>


/* Czyli tydzie�. Pierwotnie by�o 10. Lil. */
#define DECAY_TIME	10080 

int             decay;
int             decay_id;
int 		known_flag;	/* 1 - neverknown, 2 - alwaysknown */
string          *met_name, *nonmet_name, *nonmet_pname, *state_desc, 
		*pstate_desc;
mixed		leftover_list;
string		file;

/*
 * Prototypy
 */
int		kick_corpse(string arg);
static int	move_out(object ob);

nomask void
create_container()
{
    add_prop(CONT_I_DONT_SHOW_CONTENTS, 1); /* Sami wypiszemy zawartosc */

    this_object()->create_corpse();
}

nomask void
reset_container()
{
    this_object()->reset_corpse();
}

void set_file(string str)
{
    file = str;
}

void
ustaw_imie_denata(string *n)
{
    int i;
    string *temp, *temp2;
    object pob;
    
    pob = previous_object();
    
    if (pob->query_prop(LIVE_I_NEVERKNOWN))
        known_flag = 1;
    else if (pob->query_prop(LIVE_I_ALWAYSKNOWN))
        known_flag = 2;
    else known_flag = 0;
    
    temp = ({ "cia�o", "cia�a", "cia�u", "cia�o", "cia�em", "ciele", 
        "cia�a", "cia�", "cia�om", "cia�a", "cia�ami", "cia�ach" });
        
    temp2 = temp + ({ });

    state_desc = temp[0..5];
    pstate_desc = temp[6..11];

    ustaw_nazwe(temp[0..5], temp[6..11], PL_NIJAKI_NOS);
    
    nonmet_name = allocate(6);
    nonmet_pname = nonmet_name + ({ });
    met_name = nonmet_name + ({ });
    
    if (known_flag != 2)
    {
        for (i = 0; i < 6; i++)
        {
            nonmet_name[i] = pob->query_nonmet_name(i);
            nonmet_pname[i] = pob->query_nonmet_pname(i);
        }

        for (i = 0; i < 6; i++)
        {
            temp2[i] = temp2[i] + " " + nonmet_name[PL_DOP];
            temp2[i + 6] = temp2[i + 6] + " " + nonmet_pname[PL_DOP];
        }

        dodaj_nazwy(temp2[0..5], temp2[6..11], PL_NIJAKI_NOS);
    }

    if (known_flag != 1)
    {
        for (i = 0; i < 6; i++)
            met_name[i] = lower_case(n[i]);

        for (i = 0; i < 6; i++)
        {
            temp[i] = temp[i] + " " + met_name[PL_DOP];
            temp[i+6] = temp[i + 6] + " " + met_name[PL_DOP];
        }

        dodaj_nazwy( temp[0..5], temp[6..11], PL_NIJAKI_NOS);
    }

    temp = ({ "szcz�tki", "szcz�tk�w", "szcz�tkom", "szcz�tki",
        "szcz�tkami", "szcz�tkach" });
    temp2 = temp + ({ });

    dodaj_nazwy(temp, PL_MESKI_NOS_NZYW);
    
    if (known_flag != 2)
    {
        for (i = 0; i < 6; i ++)
            temp2[i] = temp2[i] + " " + nonmet_name[PL_DOP];

        dodaj_nazwy(temp2, PL_MESKI_NOS_NZYW);
    }

    if (known_flag != 1)
    {
        for (i = 0; i < 6; i ++)
        {
            temp[i] = temp[i] + " " + met_name[PL_DOP];
        }

        dodaj_nazwy(temp, PL_MESKI_NOS_NZYW);
    }

    ustaw_shorty( ({ "@@short_func|0", "@@short_func|1", "@@short_func|2",
        "@@short_func|3", "@@short_func|4", "@@short_func|5" }), 
        ({"@@pshort_func|0", "@@pshort_func|1", "@@pshort_func|2",
        "@@pshort_func|3", "@@pshort_func|4", "@@pshort_func|5" }), 
        PL_NIJAKI_NOS);
        
    set_long("@@long_func");
    
    change_prop(CONT_I_WEIGHT, 50000);
    change_prop(CONT_I_VOLUME, 50000);
}

varargs string
short_func(string prz)
{
    object pob;
    int przyp = atoi(prz);
    string str;
    
    pob = vbfc_caller();
    
    if (!pob || !query_ip_number(pob) || pob == this_object())
	pob = this_player();

    if (pob && pob->query_real_name() == lower_case(met_name[0]))
    {
        switch (query_rodzaj())
        {
             case PL_NIJAKI_NOS:
                switch (przyp)
		{
		    case PL_MIA:
		    case PL_BIE: str = "twoje"; break;
		    case PL_DOP: str = "twojego"; break;
		    case PL_CEL: str = "twojemu"; break;
		    case PL_NAR:
		    case PL_MIE: str = "twoim"; break;
		}
		break;
	    case PL_ZENSKI:
                switch (przyp)
		{
		    case PL_MIA:
		    case PL_BIE:
		    case PL_NAR: str = "twoja"; break;
		    case PL_DOP:
		    case PL_CEL:
		    case PL_MIE: str = "twojej"; break;
		}
        }
        
        return str + " " + state_desc[przyp];
    }
    else 
        if ( (known_flag != 1) && 
             ((known_flag == 2) || 
              (pob && pob->query_met(met_name[0]))) ) 
	    return state_desc[przyp] + " " + capitalize(met_name[PL_DOP]);
    else
	return state_desc[przyp] + " " + nonmet_name[PL_DOP];
}

string
pshort_func(string prz)
{
    object pob;
    int przyp = atoi(prz);

    pob = vbfc_caller();
    if (!pob || !query_ip_number(pob) || pob == this_object())
	pob = this_player();
    if (pob && pob->query_real_name() == lower_case(met_name[0]))
	return ({ "twoje", "twoich", "twoim", "twoje", "twoimi", 
            "twoich" })[przyp] + " " + pstate_desc[przyp];
    else
        if ( (known_flag != 1) && 
             ((known_flag == 2) || 
              (pob && pob->query_met(met_name[0]))) ) 
	return pstate_desc[przyp] + " " + capitalize(met_name[PL_DOP]);
    else
	return pstate_desc[przyp] + " " + nonmet_pname[PL_DOP];
}

string
long_func()
{
    object pob, *inv;
    string ret;

    pob = vbfc_caller();
    if (!pob || !query_ip_number(pob) || pob == this_object())
	pob = this_player();
    if (pob->query_real_name() == lower_case(met_name[0]))
	ret = "Jest to twoje w�asne cia�o.\n";
    else if (pob->query_met(met_name[0]))
	ret = "Jest to nieprzytomne cia�o " + capitalize(met_name[PL_DOP]) + ".\n";
    else
	ret = "Jest to nieprzytomne cia�o " + nonmet_name[PL_DOP] + ".\n";

    inv = all_inventory(this_object());
    if (!sizeof(inv))
        return ret;
    return ret + "Zauwa�asz przy " + query_zaimek(PL_MIE) + " " +
	FO_COMPOSITE_DEAD(inv, pob, PL_BIE) + ".\n";
}

//dodajemy 'cialo krowy', a nie tylko 'cialo xxx yyy krowy'. V.
int
dodaj_jeszcze_jedna_nazwe()
{
	string *tmp = ({"cia�o "+query_prop(CORPSE_M_RACE)[1],
		"cia�a "+query_prop(CORPSE_M_RACE)[1],
		"cia�u "+query_prop(CORPSE_M_RACE)[1],
		"cia�o "+query_prop(CORPSE_M_RACE)[1],
		"cia�em "+query_prop(CORPSE_M_RACE)[1],
		"ciele "+query_prop(CORPSE_M_RACE)[1] });
	string *tmp2 = ({"cia�a "+query_prop(CORPSE_M_PRACE)[1],
		"cia� "+query_prop(CORPSE_M_PRACE)[1],
		"cia�om "+query_prop(CORPSE_M_PRACE)[1],
		"cia�a "+query_prop(CORPSE_M_PRACE)[1],
		"cia�ami "+query_prop(CORPSE_M_PRACE)[1],
		"cia�ach "+query_prop(CORPSE_M_PRACE)[1] });

    return dodaj_nazwy(tmp,tmp2,
		/*({temp[6]+"@@query_prace|0",temp[7]+"@@query_prace|1",
		temp[8]+"@@query_prace|2",temp[9]+"@@query_prace|3",
		temp[10]+"@@query_prace|4",temp[11]+"@@query_prace|5"}),*/
		PL_NIJAKI_NOS);

}


static int
move_out(object ob)
{
    if (ob->move(environment(this_object())))
	return 0;
    else
	return 1;
}

void
remove_object()
{
    map(all_inventory(this_object()), move_out);

    ::remove_object();
}

varargs mixed
query_race(int przyp)
{
    return query_prop(CORPSE_M_RACE)[przyp];
}

string
get_fail(string fault)
{
    return fault;
}

public int
find_corpse(object ob)
{
    if ((function_exists("create_container") == CORPSE_OBJECT) &&
	((environment(ob) == this_player()) ||
	 (environment(ob) == environment(this_player()))))
    {
	return 1;
    }

    return 0;
}

/*Lil*/
int
kick_corpse(string arg)
{
	string vb;
	mixed corpses;
	object *found;

  vb = query_verb();
	
 notify_fail(capitalize(vb) + " kogo [gdzie] ?\n");
    if (!arg)
	return 0;

  if (!parse_command(arg, environment(this_player()), "%i:" + PL_BIE,
		corpses))
	return 0;

  found = VISIBLE_ACCESS(corpses, "find_corpse", this_object());

  if (sizeof(found) == 1)
  {
    saybb(QCIMIE(this_player(), PL_MIA) + " kopie brutalnie " + 
		  QSHORT(found[0], PL_BIE) + ".\n");
    write("Kopiesz brutalnie " + found[0]->short(PL_BIE) 
	    + ".\n");
	return 1;
  }
}

public string
query_corpse_auto_load()
{
    string toReturn = " @C@ ";
    return toReturn;
}

public string
init_corpse_arg(string arg)
{
    if (arg == 0) {
        return 0;
    }
}

public string
query_auto_load()
{
    return 0;
//    return ::query_auto_load() + query_corpse_auto_load();
}

public string
init_arg(string arg)
{
    return ::init_arg(arg);
//    return init_corpse_arg(::init_arg(arg));
}
