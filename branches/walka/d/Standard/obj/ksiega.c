/* Ksiega z informacjami dla poczatkujacych graczy
 * Lewy 31.12.1997
 */

inherit "/std/scroll";

#include <stdproperties.h>
#include <macros.h>


void
create_scroll()
{

    ustaw_nazwe("ksi�ga");

    set_long("\nJest to gruba ksi�ga oprawiona w sk�r�. Na jej ok�adce z�otymi "+
	"literami wyt�oczono napis PORADNIK POCZ�TKUJ�CEGO ODKRYWCY. "+
	"Ksi�ga jest tak popularna, �e przymocowano j� �a�cuchem, aby nikt "+
	"jej nie ukrad�.\n");

    add_prop(OBJ_I_VALUE, 0);
    add_prop(OBJ_I_WEIGHT, 0);
    add_prop(OBJ_I_VOLUME, 0);
    add_prop(OBJ_M_NO_GET, "Ksi�ga jest przymocowana �a�cuchem do sto�u.\n");
    
    set_file("/d/Standard/obj/gracz_FAQ");
}

