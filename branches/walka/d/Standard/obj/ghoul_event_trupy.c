
/* Autor: Vera. Z�ynka z ghoula z kana��w Rinde
   Opis : Faeve
   Data : podkoniecczerwca07 */

inherit "/std/creature";
inherit "/std/combat/unarmed";
inherit "/std/act/attack";
inherit "/std/act/action";
inherit "/std/act/trigaction.c";
inherit "/std/act/domove";
#include <std.h>
#include <ss_types.h>
#include <macros.h>
#include <stdproperties.h>
#include <pl.h>
#include <composite.h>
//#include "dir.h"

object macierzysta_lok;
void spadamy();

int
czy_atakowac()
{
    if(TP->query_humanoid() && TP->query_rasa() != "ghoul")
         return 1;
    else
        return 0;
}


string
zjedz()
{
	if(TO->query_attack())
		return "";
    object *trupki = filter(all_inventory(ENV(TO)),&->query_corpse());
    string msg = "";
    if(!sizeof(trupki))
       return "";
    else
    {
        object ktory = trupki[random(sizeof(trupki))];
        switch(random(8))
		{
			case 0:
           msg = QCSHORT(TO,PL_MIA)+" odrywa spory kawa� mi�sa z "+
                QSHORT(ktory,PL_DOP)+".\n";
			break;
        	case 1:
           msg = QCSHORT(TO,PL_MIA)+" gryzie �apczywie "+QSHORT(ktory,PL_BIE)+".\n";
			break;
        	case 2:
           msg = QCSHORT(TO,PL_MIA)+" rzuca si� na "+QSHORT(ktory,PL_BIE)+
                " i z szale�czym zapa�em zaczyna je po�era�.\n";
			break;
			case 3:
			msg=QCSHORT(TO,PL_MIA)+" �lini�c si�, bierze obgryzion� ko��, "+
			"po czym zaczyna wysysa� z niej szpik.\n";
			break;
			case 4:
			msg=QCSHORT(TO,PL_MIA)+" agresywnymi ruchami szcz�k odrywa "+
			"kawa�ek cia�a, kt�ry po chwili znika w jego paszczy.\n";
			break;
			case 5:
			msg=QCSHORT(TO,PL_MIA)+" pochylaj�c si� nad ofiar�, "+
			"zaczyna j� powoli obw�chiwa�.\n";
			break;
			case 6:
			msg=QCSHORT(TO,PL_MIA)+" z oczami pe�nymi nigdy nienasyconego "+
			"g�odu po�yka k�s za k�sem.\n";
			break;
			case 7:
			msg="Rozchlapuj�c dooko�a lepk� ma�, kt�ra "+
			"zapewne jest �lin�, "+QSHORT(TO,PL_MIA)+" gwa�townymi ruchami"+
			" szarpie resztki cia�a.\n";
			break;
		}
    }
//find_player("vera")->catch_msg(QSHORT(TO,PL_MIA)+"\n");
    //filter(all_inventory(ENV(TO)), interactive)->catch_msg(msg);
    tell_roombb(ENV(TO), msg);
    set_alarm(itof(random(11)+5),0.0,"zjedz");

    return "";
}




void random_przym(string str, int n);

void
create_creature()
{
    ustaw_odmiane_rasy("ghoul");
    set_gender(G_MALE);
    random_przym("wyg�odnia�y:wyg�odniali oszpecony:oszpeceni brudny:brudni "+
        "^smierdz^acy:^smierdz^acy zaniedbany:zaniedbani||"+
        "agresywny:agresywni nerwowy:nerwowi wysoki:wysocy "+
        "chudy:chudzi wychudzony:wychudzeni",2);

    set_long("Kreatura kt^orej si^e przygl^adasz wygl^ada na humanoida, "+
        "gdy^z budowa cia^la przypomina starca z powykrecanymi ko^nczynami. "+
        "Jego d^lugie r^ece zaopatrzone s^a w szerokie pazury s^lu^z^ace do "+
        "rozgrzebywania mogi^l i ^swie^zej ziemi. Dziwna substancja kt^or^a "+
        "ociekaja pazury potwora budzi w tobie obawy. Ostre jak brzytwa "+
        "z^eby i d^lugi j^ezyk pozwalaj^a mia^zd^zy^c ko^sci, a nast^epnie "+
        "wysysa^c z nich szpik kostny. Stw^or ten pojawia si^e na "+
        "powierzchni tylko noc^a, w dzie^n za^s przesiaduje w mrocznych "+
        "miejscach by ukry^c si^e przed ra^z^acym go s^lo^ncem. Zwykle nie "+
        "poluje, zadowalajac si^e tylko padlin^a b^ad^x cia^lami "+
        "pozostawionymi po bitwie. Lecz gdy zobaczy s^labego przeciwnika "+
        "lub gdy zostanie zaskoczony b^edzie walczy^l z pe^ln^a "+
        "zaci^eto^sci^a staj^ac si^e naprawde gro^xnym przeciwnikiem.\n");
    set_stats (({50+random(30),50+random(20),110+random(40),40,40,100}));
    add_prop(CONT_I_WEIGHT, 60000);
    add_prop(CONT_I_HEIGHT, 185);
    add_prop(LIVE_I_NEVERKNOWN, 1);
    set_aggressive(&czy_atakowac());
    set_attack_chance(100);
    set_npc_react(1); 
    
    set_cact_time(10);
    add_cact("jeknij");
    add_act("emote spogl^ada na ciebie wrogo.");
    set_act_time(1);
    add_act(&zjedz());
    add_act(&zjedz());
    add_act("emote charczy przeci�gle.");
    add_act("mlasnij");
    add_act("zajecz przerazliwie");


    set_attack_unarmed(0, 25, 30, W_SLASH,  50, "pazurami prawej d�oni");
    set_attack_unarmed(1, 25, 30, W_SLASH,  50, "pazurami lewej d�oni");
    set_hitloc_unarmed(0, ({ 1, 1, 1 }), 10, "g^low^e");
    set_hitloc_unarmed(1, ({ 1, 1, 1 }), 30, "tu^l^ow");
    set_hitloc_unarmed(2, ({ 1, 1, 1 }), 20, "lew^a r�ke");
    set_hitloc_unarmed(3, ({ 1, 1, 1 }), 20, "praw^a r�ke");
    set_hitloc_unarmed(4, ({ 1, 1, 1 }), 20, "nogi");

    add_prop(LIVE_I_SEE_DARK, 1);





    set_alarm(itof(random(900)+650),0.0,"spadamy");
}

/*moze kiedys bedziemy chcieli, zeby te eventowe stworki lazily i 
atakowaly lesnych expiarzy...?
void
polazimy()
{

	set_random_move(90);
    set_monster_home( );
    set_restrain_path( ({  }) );
}*/

void
spadamy()
{
    if(query_attack())
    {
	set_alarm(140.0,0.0,"spadamy");
	return;
    }

    if(!query_leader())
    {
        //usuwamy cia�a
        object *trupki = filter(all_inventory(ENV(TO)),&->query_corpse());
                if(sizeof(trupki))
                      foreach(object x : trupki)
                          x->remove_object();
 
	if(sizeof(query_team_others()))
	{
            tell_room("Nasycone "+COMPOSITE_LIVE(query_team(),PL_BIE)+
                    " odchodz� w nieznane.\n");
           foreach(object x : query_team())
                x->remove_object();
	   TO->remove_object();
        }
        else
        {
            tell_room("Nasycony "+QSHORT(TO,PL_BIE)+" odchodzi "+
                           "w nieznane.\n");
            remove_object();
        }
    }
}

void
przestan_sledzic()
{
    unset_follow();
    string *drozka = znajdz_sciezke(ENV(TO),macierzysta_lok);
    foreach(string x : drozka)
            command(x);
}

void
attacked_by(object wrog)
{
    ::attacked_by(wrog);

    if(!query_leader()) //jeste�my liderem, lub samotnikiem
    {
        macierzysta_lok = ENV(TO);
        set_follow(wrog,itof(random(3)+1) / 10.0);
        set_alarm(itof(random(5)+2),0.0,"przestan_sledzic");
    }
}

		
void
random_przym(string str, int n)
{
        mixed* groups = ({ });
        groups = explode(str, "||");
        int groups_size = sizeof(groups);
        if(n > groups_size)
                return;
        for(int i =0; i < groups_size; i++)
                groups[i] = explode(groups[i], " ");
        
        for(int a = 0; a < n; a++)
        {
                int group_index = random(sizeof(groups));
                int adj_index = random(sizeof(groups[group_index]));
                string* tmp = explode(groups[group_index][adj_index], ":");
                dodaj_przym(tmp[0], tmp[1]);
                groups = groups-({groups[group_index]});
        }
}
