
//to taki oficer, kt�ry nie ma akcji z liba admina, mo�na go possessowa� 
//z jego �wit� i co� z nimi rpgowa� ;)
inherit "/std/monster";
//inherit "/lib/straz/straznik";

#include "/d/Standard/Redania/Rinde/straz/dir.h"

#include <ss_types.h>
#include <macros.h>
#include <stdproperties.h>
#include <pl.h>

#define MIASTO "z Rinde"

							//CHWILOWO TUTAJ!! TODO!
//#define BIURO_ZNALEZIONYCH "/d/Standard/wiz/biuro_rzeczy.c"

#define HELM RINDE+"przedmioty/zbroje/helmy/lekki_oksydowany_M.c"
#define MIECZ RINDE+"przedmioty/bronie/miecze/dlugi_stalowy.c"
#define ZBRO RINDE+"przedmioty/zbroje/kolczugi/szara_matowa_M.c"
#define BUTY RINDE+"przedmioty/ubrania/buty/wysokie_skorzane_M.c"
#define SPOD RINDE+"przedmioty/zbroje/spodnie/wzmacniane_skorzane_M.c"

void random_przym(string str, int n);

void
create_monster()
{
	ustaw_odmiane_rasy("oficer");
	dodaj_nazwy("cz�owiek");
	set_gender(G_MALE);

	random_przym("ros�y:ro�li przygarbiony:przygarbieni||m�tnooki:m�tnoocy||�ysawy:�ysawi||"
			+"starszy:starszi||energiczny:energiczni", 2);

	set_long("Cz�owiek o poci�g�ej, nosz�cej ju� �lady staro�ci twarzy rozgl�da"
		+" si� dooko�a bystrym wzrokiem. Mimo, iz najlepsze lata ma ju� za sob�,"
		+" to wci�� jest pe�en krzepy o czym �wiadcz� jego gromkie okrzyki"
		+" strofuj�ce m�odych stra�nik�w. W przeciwie�stwie do reszty gwardzist�w,"
		+" ten wydaje si� mie� nieco wi�cej og�ady i kultury, cho� mo�e to tylko z�udne"
		+" wra�enie spowodowane jego leciwym wiekiem...\n");
    
	add_prop(LIVE_I_NEVERKNOWN, 1);
	add_prop(CONT_I_WEIGHT, 75000);
	add_prop(CONT_I_HEIGHT, 175);
	add_prop(NPC_I_NO_FEAR,1);

	set_stats(({ 145, 120, 170, 150, 150, 150}));

	add_weapon(MIECZ);
	add_armour(HELM);
	add_armour(ZBRO);
	add_armour(BUTY);
	add_armour(SPOD);

	//set_attack_chance(100);
	//set_aggressive(&check_living());

	set_act_time(20+random(4));
	add_act("emote rozgl�da sie po okolicy.");
	add_act("emote poprawia zbroj�.");
	add_act("emote odprowadza wzrokiem przechodz�c� nieopodal dziewk�.");
	add_act("mlasnij");
	add_act("oczko do drugiej kobiety");
	add_act("oczko do kobiety");
	add_act("obliz sie oblesnie");
	add_act("westchnij ciezko");
	add_act("emote m�wi do siebie: Pogrucha�bym z tamt� cizi�, hehe.");
	add_act("beknij");
	add_act("emote m�wi cicho: Ysz kurwa. Co za los.");
	add_act("popatrz spode lba na elfa");
	add_act("powiedz Rusza� si� to wcze�niej sko�czym.");
	add_act("emote m�wi pod nosem: Ej�e, mo�e kogo� ubijem w ciemnym zau�ku? Hehe, "+
		"zawsze to jaka� rozrywka, no nie?");

	set_cact_time(9+random(3));
	add_cact("powiedz Osz kurwa!");
	add_cact("krzyknij Bi�, nie �a�owa�, kurwa wasza ma�!");
	add_cact("powiedz Zabij na �mier�!");
	add_cact("splun");
	add_cact("krzyknij Bijem siem kurwa!");
	
	add_ask(({"ratusz"}), VBFC_ME("pyt_o_ratusz"));
	add_ask(({"�wi�tynie"}), VBFC_ME("pyt_o_swiatynie"));
	add_ask(({"stra�nik�w","stra�"}), VBFC_ME("pyt_o_straz"));
	add_ask(({"garnizon"}), VBFC_ME("pyt_o_garnizon"));
	add_ask(({"burmistrza","Nevilla","nevilla"}),VBFC_ME("pyt_o_burmistrza"));
	set_default_answer(VBFC_ME("default_answer"));

    set_skill(SS_WEP_SWORD,78);
    set_skill(SS_UNARM_COMBAT, 50);
    set_skill(SS_PARRY, 66);
    set_skill(SS_DEFENCE,44);
    set_skill(SS_AWARENESS,64);
    set_skill(SS_BLIND_COMBAT,30);

	//create_straznik();
}
/*
int
query_oficer()
{
    return 1;
}

init_living()
{
  ::init_living();
  ::init_straznik();
}

void powiedz_gdy_jest(object player, string tekst)
{
	if (environment(this_object()) == environment(player))
        this_object()->command(tekst);
}

string pyt_o_ratusz()
{
        set_alarm(0.5, 0.0, "powiedz_gdy_jest", TP,
            "powiedz do "+ TP->query_name(PL_DOP) +" Jak mo^zna "+
            "nie wiedzie^c "+
            "pan"+TP->koncowka("ie","i")+" gdzie ratusz stoi! "+
            "Przecie to samo centrum miasta z t^a ceglan^a wie^z^a ino.");
        return "";
}

string pyt_o_swiatynie()
{
        set_alarm(0.5, 0.0, "powiedz_gdy_jest", TP,
            "powiedz do "+ OB_NAME(TP) +
            " Pan"+TP->koncowka("ie","i")+" widzi "+
            "pan"+TP->koncowka("","i")+" ino tamt^a bia^l^a "+
            "wie^ze. To ino tam.");
        return "";
}

string pyt_o_straz()
{
        set_alarm(0.5, 0.0, "powiedz_gdy_jest", TP,
            "powiedz do "+ TP->query_name(PL_DOP) +" Tak, to ja!");
        return "";
}

string pyt_o_garnizon()
{
        set_alarm(0.5, 0.0, "powiedz_gdy_jest", TP,
            "powiedz do "+ TP->query_name(PL_DOP) +
            "Garnizon pan"+TP->koncowka("ie","i")+" to "+
            "ten du^zy kamienny budynek na ws... wsch... Tam gdzie s^lo^nce "+
            "si^e pojawia!");
        return "";
}

string pyt_o_burmistrza()
{
        set_alarm(0.5, 0.0, "powiedz_gdy_jest", TP,
            "powiedz do "+ TP->query_name(PL_DOP) +" Neville "+
            "jest naszym burmistrzem!");
        return "";
}

string default_answer()
{
        set_alarm(2.0, 0.0, "powiedz_gdy_jest", TP,
        "powiedz Nie zawracaj pan"+TP->koncowka("ie","i")+" dupy.");
        return "";
}
*/
void random_przym(string str, int n)
{
	mixed* groups = ({ });
        groups = explode(str, "||");
        int groups_size = sizeof(groups);
        if(n > groups_size)
                return;
        for(int i =0; i < groups_size; i++)
                groups[i] = explode(groups[i], " ");
        
        for(int a = 0; a < n; a++)
	{
                int group_index = random(sizeof(groups));
                int adj_index = random(sizeof(groups[group_index]));
                string* tmp = explode(groups[group_index][adj_index], ":");
                dodaj_przym(tmp[0], tmp[1]);
                groups = groups-({groups[group_index]});
        }
}

void emote_hook(string emote, object wykonujacy, string przyslowek)
{
	switch (emote)
	{
		case "klepnij":
		case "poca�uj":
		case "pog�aszcz":
		case "po�askocz":
		case "przytul":
		{
			if(wykonujacy->query_gender())
				set_alarm(1.0, 0.0, "kobieta_dobre", wykonujacy);
			else
				set_alarm(1.0, 0.0, "facet_zle", wykonujacy);
			break;
		}
		case "kopnij":
		case "opluj":
		case "nadepnij":
		{
			if(wykonujacy->query_gender())
				set_alarm(1.0, 0.0, "kobieta_zle", wykonujacy);
			else
				set_alarm(1.0, 0.0, "facet_zle", wykonujacy);
			break;
		}
		case "szturchnij":
		{
			set_alarm(1.0, 0.0, "powiedz_gdy_jest", TP,
                        "powiedz do "+wykonujacy->query_name(PL_DOP)
				     +" Taaa?");
			break;
		}
	}
}

void kobieta_dobre(object wykonujacy)
{
	string rasa = wykonujacy->query_race();
	if(rasa ~= "krasnolud" || rasa ~= "gnom" || rasa ~= "nizio�ek")
	{
		set_alarm(1.0, 0.0, "powiedz_gdy_jest", TP,
                        "powiedz do "+wykonujacy->query_name(PL_DOP)+
			" Won pokurczu, albo ci �eb ukr�ce!");
		command("kopnij "+wykonujacy->query_real_name(3));
	}
	else
	{
		command("klepnij "+wykonujacy->query_real_name(3)+" bezczelnie w po�ladek");
		set_alarm(1.0, 0.0, "powiedz_gdy_jest", TP,
                        "powiedz do "+wykonujacy->query_name(PL_DOP)
			+" Mo�e zajrzysz do mnie po robocie?");
		command("usmiechnij sie oblesnie");
	}
}

void kobieta_zle(object wykonujacy)
{
	string rasa = wykonujacy->query_race();
	if(rasa ~= "krasnolud" || rasa ~= "gnom" || rasa ~= "nizio�ek")
	{
		set_alarm(1.0, 0.0, "powiedz_gdy_jest", TP,
                        "powiedz do "+wykonujacy->query_name(PL_DOP)+
			" Won pokurczu, albo ci �eb ukr�ce!");
		command("kopnij "+wykonujacy->query_real_name(3));
	}
	else
	{
		set_alarm(1.0, 0.0, "powiedz_gdy_jest", TP,
                        "powiedz do "+wykonujacy->query_name(PL_DOP)+
				" Te, male�ka! Nie podskakuj!");
		command("spojrzyj z usmieszkiem na "+wykonujacy->query_real_name(3));
	}
}

void facet_zle(object wykonujacy)
{
	int  i = random(2);
	
	switch (i)
	{
		case 0:
		{
			//command("powiedz do "+OB_NAME(wykonujacy)+
                        set_alarm(1.0, 0.0, "powiedz_gdy_jest", TP,
                        "powiedz do "+wykonujacy->query_name(PL_DOP)+
                        " Spieprzaj, psie nasienie, albo ci� w�asna matka "+
                        "nie pozna!");
			command("spojrzyj beznamietnie na "+OB_NAME(wykonujacy));
			break;
		}
		
		case 1:
		{
			set_alarm(1.0, 0.0, "powiedz_gdy_jest", TP,
                        "powiedz do "+wykonujacy->query_name(PL_DOP)+
				" Spr�buj tego jeszcze raz, to ci jaja rzyci� wyjd�.");
			command("kopnij "+OB_NAME(wykonujacy));
			break;
		}
	}
}

public string
query_auto_load()
{
    return 0;
}
