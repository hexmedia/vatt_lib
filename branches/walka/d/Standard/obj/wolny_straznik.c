
//to taki stra�nik, kt�ry nie ma akcji z liba admina, mo�na go possessowa� 
//z jego �wit� i co� z nimi rpgowa� ;)
inherit "/std/monster";
inherit "/std/act/action";
inherit "/std/act/chat";
inherit "/std/act/asking";
//inherit "/lib/straz/straznik";

#include "/d/Standard/Redania/Rinde/straz/dir.h"

#include <macros.h>
#include <pl.h>
#include <ss_types.h>
#include <stdproperties.h>

#define MIASTO "z Rinde"

//CHWILOWO TUTAJ!! TODO!
//#define BIURO_ZNALEZIONYCH "/d/Standard/wiz/biuro_rzeczy.c"

#define HELM RINDE+"przedmioty/zbroje/helmy/lekki_oksydowany_M.c"
#define MIECZ RINDE+"przedmioty/bronie/miecze/masywny_stalowy.c"
#define ZBRO RINDE+"przedmioty/zbroje/kolczugi/szara_matowa_M.c"
#define BUTY RINDE+"przedmioty/ubrania/buty/wysokie_skorzane_M.c"
#define SPOD RINDE+"przedmioty/zbroje/spodnie/wzmacniane_skorzane_M.c"

object admin=TO->query_admin();

void random_przym(string str, int n);
void random_long();

void
create_monster()
{
    ustaw_odmiane_rasy("straznik");
    dodaj_nazwy("cz�owiek");
    set_gender(G_MALE);

    random_przym("znudzony:znudzeni energiczny:energiczni zblazowany:zblazowani"+
	" krzepki:krzepcy ospa�y:ospali||muskularny:muskularni ros�y:ro�li"+
	" ciemnow�osy:ciemnow�osi pryszczaty:pryszczaci kr�tkow�osy:kr�tkow�osi", 2);

    random_long();
	
    add_prop(LIVE_I_NEVERKNOWN, 1);
    add_prop(CONT_I_WEIGHT, 75000);
    add_prop(CONT_I_HEIGHT, 178);
    add_prop(NPC_I_NO_FEAR, 1);

    add_weapon(MIECZ);
    add_armour(HELM);
    add_armour(ZBRO);
    add_armour(BUTY);
    add_armour(SPOD);

    set_stats(({100, 150,150, 150, 150, 150}));

    set_attack_chance(100);
    //set_aggressive(&check_living());

    set_act_time(20+random(9));
    add_act("emote rozgl�da si� szukaj�c zaczepki.");
    add_act("oczko do kobiety");
    add_act("ziewnij");
    add_act("beknij");
	add_act("podrap sie po jajach");
    add_act("popatrz na elfke");
    add_act("popatrz spode lba na elfa");
    add_act("powiedz A mog�em pos�ucha� matki, a nie ojca...");
    add_act("popatrz spode lba na krasnoluda");
    add_act("mlasnij");
    add_act("powiedz Eh, kiedy to ja ostatnio ch�do�y�em...?");
    add_act("emote m�wi pod nosem: Pieprzony oficer, zachcia�o mu si� �azi� po mie�cie.");
    add_act("skrzyw sie do oficera");
    add_act("pokiwaj niemrawo");
    add_act("emote m�wi pod nosem: Zaraza. Mam chu� na jak�� p�elfk�.");
    add_act("powiedz W rzy� by wsadzi� taki patrol. Nic si� nie dzieje.");
    add_act("popatrz na kobiete");
    add_act("powiedz Ale nudy.");
    add_act("emote poprawia przypi�ty u boku miecz.");
    add_act("emote g�o�no zbiera flegm� i pluje na ulic�.");

    set_cact_time(11+random(9));
    add_cact("krzyknij Wszystkie gnaty ci �mieciu po�amiem!");
    add_cact("powiedz O tak! To lubi�!");
    add_cact("podskocz gwa�townie");
    add_cact("powiedz No kompania! To si� kurna zabawimy!");
    add_cact("krzyknij Bij, zabij!");
    
    add_ask(({"ratusz"}), VBFC_ME("pyt_o_ratusz"));
    add_ask(({"�wi�tynie"}), VBFC_ME("pyt_o_swiatynie"));
    add_ask(({"stra�nik�w","stra�"}), VBFC_ME("pyt_o_straz"));
    add_ask(({"garnizon"}), VBFC_ME("pyt_o_garnizon"));
    add_ask(({"burmistrza","Nevilla","nevilla"}),VBFC_ME("pyt_o_burmistrza"));
    set_default_answer(VBFC_ME("default_answer"));

    set_skill(SS_WEP_SWORD,68);
    set_skill(SS_UNARM_COMBAT, 50);
    set_skill(SS_PARRY, 46);
    set_skill(SS_DEFENCE,24);
    set_skill(SS_AWARENESS,64);
    set_skill(SS_BLIND_COMBAT,20);

    //create_straznik();
}
/*
void powiedz_gdy_jest(object player, string tekst)
{
	if (environment(this_object()) == environment(player))
        this_object()->command(tekst);
}

string pyt_o_ratusz()
{
        set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
            "powiedz do "+ this_player()->query_name(PL_DOP) +" Jak mo^zna "+
            "nie wiedzie^c "+
            "pan"+this_player()->koncowka("ie","i")+" gdzie ratusz stoi! "+
            "Przecie to samo centrum miasta z t^a ceglan^a wie^z^a ino.");
        return "";
}

string pyt_o_swiatynie()
{
        set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
            "powiedz do "+ OB_NAME(this_player()) +
            " Pan"+this_player()->koncowka("ie","i")+" widzi "+
            "pan"+this_player()->koncowka("","i")+" ino tamt^a bia^l^a "+
            "wie^ze. To ino tam.");
        return "";
}

string pyt_o_straz()
{
        set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
            "powiedz do "+ this_player()->query_name(PL_DOP) +" Tak, to ja!");
        return "";
}

string pyt_o_garnizon()
{
        set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
            "powiedz do "+ this_player()->query_name(PL_DOP) +
            "Garnizon pan"+this_player()->koncowka("ie","i")+" to "+
            "ten du^zy kamienny budynek na ws... wsch... Tam gdzie s^lo^nce "+
            "si^e pojawia!");
        return "";
}

string pyt_o_burmistrza()
{
        set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
            "powiedz do "+ this_player()->query_name(PL_DOP) +" Neville "+
            "jest naszym burmistrzem!");
        return "";
}

string default_answer()
{
        set_alarm(2.0, 0.0, "powiedz_gdy_jest", this_player(),
        "rozloz rece");
        return "";
}
    
int
query_straznik()
{
    return 1;
}

init_living()
{
  ::init_straznik();
}
*/
void random_przym(string str, int n)
{
	mixed* groups = ({ });
        groups = explode(str, "||");
        int groups_size = sizeof(groups);
        if(n > groups_size)
                return;
        for(int i =0; i < groups_size; i++)
                groups[i] = explode(groups[i], " ");
        
        for(int a = 0; a < n; a++)
	{
                int group_index = random(sizeof(groups));
                int adj_index = random(sizeof(groups[group_index]));
                string* tmp = explode(groups[group_index][adj_index], ":");
                dodaj_przym(tmp[0], tmp[1]);
                groups = groups-({groups[group_index]});
        }
}

void random_long()
{
	int i = random(3);
	switch (i)
	{
		case 0: 
		{
			set_long("Stra�nik powoli wodzi wzrokiem po okolicy najwyra�niej"
		+" szukaj�c jakiej� atrakcji. Jego sylwetka �wiadczy o tym,"
		+" i� musi by� on naprawd� zahartowany i zapewne w�a�nie"
		+" dlatego zosta� naj�ty jako gwardzista miasta Rinde."
		+" Wydaje ci si�, �e walka z nim mog�aby przyprawi� sporo"
		+" k�opot�w.\n");
		break;
		}
		
		case 1:
		{
		set_long("M�czyzna co chwil� mruczy pod nosem jakie� przekle�stwa,"
		+" najwyra�niej narzekaj�c na prze�o�onego, tudzie� na ca��"
		+" swoj� robot�. Leniwie, oci�a�e ruchy i niezbyt przyjemne spojrzenie"
		+" wskazuj� na to, �e stra�nik jest ju� dostatecznie znu�ony i"
		+" zdenerwowany, wi�c lepiej by�oby nie wchodzi� mu w drog�.\n");
		break;
		}
		
		case 2:
		{
		set_long("Biegn�ca po prawym policzku szrama, ma�e,"
		+" rozbiegane oczka i dope�niaj�cy ca�o�� oble�ny u�miech sprawiaj�, �e"
		+" na pierwszy rzut oka nietrudno pomyli� tego cz�owieka z pospolitym"
		+" bandyt�. Najwyra�niej burmistrz miasta uzna�, i� dobra stra� to tania stra�,"
		+" a ka�dy charakter mo�na z�agodzi� odpowiednim wynagrodzeniem.\n");
		break;
		}
	}
}

void emote_hook(string emote, object wykonujacy, string przyslowek)
{
	switch (emote)
	{
		case "klepnij":
		case "poca�uj":
		case "pog�aszcz":
		case "po�askocz":
		case "przytul":
		{
			if(wykonujacy->query_gender())
				set_alarm(1.0, 0.0, "kobieta_dobre", wykonujacy);
			else
				set_alarm(1.0, 0.0, "facet_zle", wykonujacy);
			break;
		}
		case "kopnij":
		case "opluj":
		case "nadepnij":
		{
			if(wykonujacy->query_gender())
				set_alarm(1.0, 0.0, "kobieta_zle", wykonujacy);
			else
				set_alarm(1.0, 0.0, "facet_zle", wykonujacy);
			break;
		}
		case "szturchnij":
		{
			set_alarm(1.0, 0.0, "powiedz_gdy_jest", TP,
                        "powiedz do "+wykonujacy->query_name(PL_DOP)
				     +" Taaa?");
			break;
		}
	}
}

void kobieta_dobre(object wykonujacy)
{
	string rasa = wykonujacy->query_race();
	if(rasa ~= "krasnolud" || rasa ~= "gnom" || rasa ~= "nizio�ek")
	{
		set_alarm(1.0, 0.0, "powiedz_gdy_jest", TP,
                        "powiedz do "+wykonujacy->query_name(PL_DOP)+
			" Won pokurczu, albo ci �eb ukr�ce!");
		command("kopnij "+wykonujacy->query_real_name(3));
	}
	else
	{
		command("klepnij "+wykonujacy->query_real_name(3)+" bezczelnie w po�ladek");
		set_alarm(1.0, 0.0, "powiedz_gdy_jest", TP,
                        "powiedz do "+wykonujacy->query_name(PL_DOP)
			+" Mo�e zajrzysz do mnie po robocie?");
		command("usmiechnij sie oblesnie");
	}
}

void kobieta_zle(object wykonujacy)
{
	string rasa = wykonujacy->query_race();
	if(rasa ~= "krasnolud" || rasa ~= "gnom" || rasa ~= "nizio�ek")
	{
		set_alarm(1.0, 0.0, "powiedz_gdy_jest", TP,
                        "powiedz do "+wykonujacy->query_name(PL_DOP)+
			" Won pokurczu, albo ci �eb ukr�ce!");
		command("kopnij "+wykonujacy->query_real_name(3));
	}
	else
	{
		set_alarm(1.0, 0.0, "powiedz_gdy_jest", TP,
                        "powiedz do "+wykonujacy->query_name(PL_DOP)+
				" Te, male�ka! Nie podskakuj!");
		command("spojrzyj z usmieszkiem na "+wykonujacy->query_real_name(3));
	}
}

void facet_zle(object wykonujacy)
{
	int  i = random(5);
	
	switch (i)
	{
		case 0..1:
		{
			set_alarm(1.0, 0.0, "powiedz_gdy_jest", TP,
                        "powiedz do "+wykonujacy->query_name(PL_DOP)+
                        " Spieprzaj, psie nasienie, albo ci� w�asna matka "+
                        "nie pozna!");
			command("spojrzyj beznamietnie na "+OB_NAME(wykonujacy));
			break;
		}
		
		case 2..3:
		{
			set_alarm(1.0, 0.0, "powiedz_gdy_jest", TP,
                        "powiedz do "+wykonujacy->query_name(PL_DOP)+
				" Spr�buj tego jeszcze raz, to ci jaja rzyci� wyjd�.");
			command("kopnij "+OB_NAME(wykonujacy));
			break;
		}
        case 4:
        {
        	admin->add_enemy(wykonujacy->query_real_name(),1);
			command("zabij "+OB_NAME(wykonujacy));
        	break;
        }
	}
}

public string
query_auto_load()
{
    return 0;
}
