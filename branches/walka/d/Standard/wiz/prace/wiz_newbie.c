/*Krotko mowiac: ile mozna kompletnym zoltkom tlumaczyc ciagle to samo?
 *               Temu zrobilam cos takiego...
 *               Niechaj kazdy nowy wiz to otrzymuje.
 *                                                         Lil. 10.10.2005
 */

#pragma save_binary

inherit "/std/scroll";

#include <stdproperties.h>
#include <pl.h>
#include <macros.h>

void
create_scroll()
{
    seteuid(getuid(this_object()));
    add_prop(OBJ_I_NO_DROP, "Pot�na magia zabrania ci tego.\n");
    ustaw_nazwe("pergamin");
    dodaj_przym("po��k�y", "po��kli");
    set_long("Czujesz pot�n� magi� bij�c� z tego starego, po��k�ego pergaminu. "+
             "Magia wdziera si� do twojej g�owy, przez co ogarnia ci� ogromna "+
	     "wr�cz ochota by go przeczyta�.\n");
    set_autoload();
    set_file("/d/Standard/wiz/prace/wiz_newbie");
    add_prop(WIZARD_ONLY, 1);
}

void
init()
{
    ::init();
    add_action("czytaj", "czytaj");
}

int
czytaj(string str)
{
    write("Komendy na Vatt'ghernie wyst�puja w formie dokonanej. Proponuj� u�y� "
        + "'przeczytaj'.\n");
    return 1;
}
