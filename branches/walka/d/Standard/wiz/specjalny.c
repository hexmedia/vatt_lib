//Lil, operacja wstepne Thanedd, dnia 8.11.2005
inherit "/std/room";
#include <stdproperties.h>

create_room()
{
    set_short("Galeria Chwa�y");
    set_long("W�r�d licznych obraz�w powieszonych w kolejno�ci, a "+
             "przedstawiaj�cych histori� magii od zarania dziej�w "+
	     "twoj� uwag� przyk�uwa tablica z przyczepionymi do niej "+
	     "zapisanymi pergaminami. Zupe�nie nie pasuje do bogatego "+
	     "wystroju tego wn�trza.\n");
    add_item("obrazy", "Powieszone jeden za drugim obrazy przedstawiaj� "+
              "mi�dzy innymi staro�ytny �aglowiec i wiele scen zbiorowych.\n");
    add_object("/d/Standard/wiz/tablica2");
    add_sit("na ziemi","na kamiennej posadzce","z ziemi",0);
    add_sit("pod tablic^a","pod tablic^a og^loszeniow^a","spod tablicy",4);
    add_exit("/d/Standard/wiz/wizroom", "se");
    add_exit("/d/Standard/wiz/placyk","sw");
    add_exit("/d/Standard/wiz/taras","taras");
    add_prop(ROOM_I_INSIDE, 1);
}

