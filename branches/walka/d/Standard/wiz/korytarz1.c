//Lil, operacja wstepne Thanedd, dnia 8.11.2005
inherit "/std/room";
#include <stdproperties.h>
create_room()
{
   set_short("W�ski korytarz w Akademii");
   set_long("D�ugi korytarzyk ci�gnie si� a� w kierunku wyj�cia z Pa�acu Aretuzy.\n");
   add_prop(ROOM_I_INSIDE, 1);
   add_sit("pod �cian�","pod �cian�","spod �ciany",0);
   add_exit("/d/Standard/wiz/wizroom",({"n","w kierunku g��wnego hallu"}));
   add_exit("/d/Standard/wiz/poczta",({"ne","na p�nocny wsch�d, do Akademii"}));
   add_exit("/d/Standard/wiz/schody_do_loxii",({"e","na zewn�trz"}));
   add_exit("/d/Standard/wiz/biuro_rzeczy",({"s","do biura rzeczy znalezionych"}));
}