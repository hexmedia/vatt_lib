inherit "/std/board";
#include <stdproperties.h>

create_board()
{
    set_board_name("/d/Standard/wiz/notki_nt_rozwoju");
    set_silent(0);
    set_long("Jest to tablica tymczasowa. Na niej zostaj� "+
             "uzgadniane kwestie na temat rozwoju (terytorialnego) �wiata.\n");
    add_prop(WIZARD_ONLY, 1);
}
