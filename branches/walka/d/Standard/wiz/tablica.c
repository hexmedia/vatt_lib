inherit "/std/board";
#include <stdproperties.h>

create_board()
{
    set_board_name("/d/Standard/wiz/notki");
    set_silent(0);
    add_prop(WIZARD_ONLY, 1);

    set_num_notes(100);
}
