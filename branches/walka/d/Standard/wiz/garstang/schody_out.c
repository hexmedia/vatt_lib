//Lil, operacja wstepne Thanedd, dnia 8.11.2005
inherit "/std/room";

#include <stdproperties.h>
#define GAR "/d/Standard/wiz/garstang/"
#define ARE "/d/Standard/wiz/"

create_room()
{
    set_short("Kamienne schody");
    set_long("D�ugie, kamienne schody prowadz� u zbocza g�adkiego kamienia "+
             "Garstangu w d�, ku Aretuzie.\n");
  
    add_exit(GAR+"korytarz1",({"wsch�d","na wsch�d, do Garstangu"}));
    add_exit(ARE+"placyk",({"po�udnie","d�ugimi schodami na po�udnie"}));
    
 
}
