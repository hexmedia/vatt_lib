inherit "/std/room";

#include <stdproperties.h>
#include <files.h>
#define GAR "/d/Standard/wiz/garstang/"

object tab;
int pomoc();

create_room()
{
    set_short("Sala projekt�w");
    set_long("Na �rodku pomieszczenia stoi du�a tablica z wieloma zapiskami. " +
             "Co chwila wypisuj� si� na niej nowe projekty od rzemie�lnik�w.\n"+
             "Wpisz 'pomoc', by dowiedzie� si� czego� wi�cej.\n");
    PROJLOG_OBJECT->move(this_object());
    add_exit(GAR+"korytarz1", ({"nw","na korytarz"}));
    add_prop(ROOM_I_INSIDE, 1);
    setuid();
    seteuid(getuid());
}

void
init()
{
    ::init();
    add_action(pomoc,"pomoc");
    add_action(pomoc,"?tablica");
}
    
int
pomoc()
{
    string str;
       
    str="\n---------------------POMOC-----------------------------------\n"+
        "Standardowy wpis na tablicy wygl�da tak:\n"+
        "1: /doc/examples/crafting/krawiec#419     Jeremian      2  28 XI 2006"+
        "\n\nPierwsza liczba to numer notki, nast�pnie jako tytu� jest podawany "+
        "obiekt, do kt�rego zosta�o z�o�one podanie. "+
        "Dalej mamy imi� zg�aszaj�cego, kolejny numerek to liczba "+
        "wierszy wyst�puj�cych w zg�oszeniu, a na samym ko�cu widnieje data "+
        "zg�oszenia.\n"+
        "-------------------------------------------------------------\n";
    write(str);
    return 1;
}
