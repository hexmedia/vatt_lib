
inherit "/std/room";
inherit "/lib/straz/admin";

#include "/d/Standard/Redania/dir.h"
#include "/d/Standard/Redania/Rinde/dir.h"
#include <ss_types.h>
#include <composite.h>
#include <macros.h>
#include <stdproperties.h>
#include <filter_funs.h>
#include <pl.h>

#define RINDE_STRAZ  "/d/Standard/Redania/Rinde/straz/"
#define DBG(x)   (find_player("vera")->catch_msg(x + "\n"))

string
dlugi_opis()
{
    string opis;

    opis = "Znajdujesz si^e obecnie w adminie stra^zy w Rinde. Magiczna komenda brzmi: "+
           "info <numer patrolu> oraz 'lista wrog�w'.\n"+
			"Patrole od 1-9 s� z Rinde,\n"+
			"patrole od 10 do 15 s� z Bia�ego Mostu.\n";

    return opis;
}

void
create_room()
{
    set_short("Admin gwardii w og�le");
    set_long(&dlugi_opis());

    add_prop(ROOM_I_INSIDE, 1);

    add_exit("korytarz2",
				({"po�udnie","do pomieszczenia administracyjnego stra^z^a w Rinde",
                                         "z pomieszczenia administracyjnego stra^z^a w Rinde"}));

    // Tu ustawiamy patrole
    ustaw_predkosc_chodzenia(15,20);

    dodaj_patrol(1, ({RINDE_STRAZ+"oficer", 1, RINDE_STRAZ+"straznik", 3 }));
    ustaw_miejsce_startu(1,POLNOC_LOKACJE + "braman");
    ustaw_trase(1, ({"s","s","s","e","e","n","n","nw","nw","s","s","s","e","e","e","nw","s","n","nw","nw"}));
	ustaw_teren_dzialania(1,"Redania/Rinde");

    dodaj_patrol(2, ({RINDE_STRAZ+"oficer", 1, RINDE_STRAZ+"straznik", 3 }));
    ustaw_miejsce_startu(2,WSCHOD_LOKACJE + "bramae");
    ustaw_trase(2, ({"w","w","s","s","s","w","n","n","n","n","n","e","ne","sw","s","s","e","e"}));
	ustaw_teren_dzialania(2,"Redania/Rinde");

    dodaj_patrol(3, ({RINDE_STRAZ+"oficer", 1, RINDE_STRAZ+"straznik", 3 }));
    ustaw_miejsce_startu(3,POLUDNIE_LOKACJE + "bramas");
    ustaw_trase(3, ({"e","e","n","w","n","e","w","w","s","s","nw","nw","n","n","e","e","s","e","e","s","s","nw","s","w"}));
	ustaw_teren_dzialania(3,"Redania/Rinde");

    dodaj_patrol(4, ({RINDE_STRAZ+"oficer", 1, RINDE_STRAZ+"straznik", 3 }));
    ustaw_miejsce_startu(4,ZACHOD_LOKACJE + "ulica8");
    ustaw_trase(4, ({"s","e","n","n","e","e","s","s","e","e","n","n","w","w","s","s","w","w","s","s","n","n","w","n"}));
	ustaw_teren_dzialania(4,"Redania/Rinde");
	
	//Patrole stoj�ce ca�y czas przy bramach
	dodaj_patrol(5, ({RINDE_STRAZ+"oficer", 1, RINDE_STRAZ+"straznik", 3 }));
    ustaw_miejsce_startu(5,POLNOC_LOKACJE + "braman");
    ustaw_trase(5, ({}));
	ustaw_teren_dzialania(5,"Redania/Rinde");

    dodaj_patrol(6, ({RINDE_STRAZ+"oficer", 1, RINDE_STRAZ+"straznik", 3 }));
    ustaw_miejsce_startu(6,WSCHOD_LOKACJE + "bramae");
    ustaw_trase(6, ({}));
	ustaw_teren_dzialania(6,"Redania/Rinde");
	
	dodaj_patrol(7, ({RINDE_STRAZ+"oficer", 1, RINDE_STRAZ+"straznik", 3 }));
    ustaw_miejsce_startu(7,POLUDNIE_LOKACJE + "bramas");
    ustaw_trase(7, ({}));
	ustaw_teren_dzialania(7,"Redania/Rinde");

        //Jeden do ratusza - Gjoef.
    dodaj_patrol(8, ({RINDE_STRAZ+"straznik", 1, RINDE_STRAZ+"straznik", 1 }));
    ustaw_miejsce_startu(8,CENTRUM + "Ratusz/lokacje/korytarz");
    ustaw_trase(8, ({}));
    ustaw_teren_dzialania(8,"Redania/Rinde");
	
	




	//do bia�ego mostu

	//ten chodzi
	dodaj_patrol(10, ({BIALY_MOST+"npc/oficer", 1, BIALY_MOST+"npc/straznik", 3 }));
    ustaw_miejsce_startu(10,BIALY_MOST_LOKACJE + "ulice/u10");
    ustaw_trase(10, ({"sw","se","sw","se","ne","e","ne","nw","w","nw"}));
	ustaw_teren_dzialania(10,"Redania/Bialy_Most");
	//a te stoja
	dodaj_patrol(11, ({BIALY_MOST+"npc/oficer", 1, BIALY_MOST+"npc/straznik", 3 }));
    ustaw_miejsce_startu(11,BIALY_MOST_LOKACJE + "ulice/u10");
    ustaw_trase(11, ({}));
	ustaw_teren_dzialania(11,"Redania/Bialy_Most");

	dodaj_patrol(12, ({BIALY_MOST+"npc/oficer", 1, BIALY_MOST+"npc/straznik", 3 }));
    ustaw_miejsce_startu(12,BIALY_MOST_LOKACJE + "ulice/u01");
    ustaw_trase(12, ({}));
	ustaw_teren_dzialania(12,"Redania/Bialy_Most");

	dodaj_patrol(13, ({BIALY_MOST+"npc/oficer", 1, BIALY_MOST+"npc/straznik", 3 }));
    ustaw_miejsce_startu(13,BIALY_MOST_LOKACJE + "ulice/u11");
    ustaw_trase(13, ({}));
	ustaw_teren_dzialania(13,"Redania/Bialy_Most");





    create_admin();
}

void
init()
{
    ::init();
    init_admin();
}
