/* 
 *   Dlugi sekaty kij na ktorym mozna sie oprzec
 *   Napisal Gregh
 */


inherit "/std/object";


#include <formulas.h>
#include <macros.h>
#include <money.h>
#include <ss_types.h>
#include <stdproperties.h>
#include <wa_types.h>
#include <cmdparse.h>
#include <files.h>
#include <language.h>


int zajete;
string *glosowali = ({ });
mixed *opcje = ({ });

void
create_object()
{
    ustaw_nazwe(({"urna wyborcza", "urny wyborczej", "urnie wyborczej",
                  "urn^e wyborcz^a", "urn^a wyborcz^a", "urnie wyborczej"}),
                ({"urny wyborcze", "urn wyborczych", "urnom wyborczym",
                  "urn wyborczych", "urnami wyborczymi",
                  "urnach wyborczych"}), PL_ZENSKI);
    dodaj_nazwy(({"urna", "urny", "urnie",
                  "urn^e", "urn^a", "urnie"}),
                ({"urny", "urn", "urnom",
                  "urn", "urnami", "urnach"}), PL_ZENSKI);
     
    dodaj_przym( "automatyczny", "automatyczni" );

    set_long("Jest to zwyczajna automatyczna urna wyborcza stoj^aca zwykle " +
             "we wszystkich lokalach wyborczych na terenie wszystkich " +
             "wyborczych okr^eg^ow.\n\n" +
             "INSTRUKCJA\n" +
             "^Zeby zresetowa^c urn^e 'zresetuj urn^e'.\n" +
             "^Zeby zako^nczy^c obecne g^losowanie 'podlicz g^losy^.\n" +
             "^Zeby zag^losowa^c 'zag^losuj na <nr opcji>'.\n\n" +
             "!!TERAZ TRUDNE UWAGA!!\n" +
             "^Zeby rozpocz^a^c nowe g^losowanie 'zresetuj urn^e' a potem " +
             "'odpal wybory <i tu ewentualnie opcje>' opcje maj^a by^c " +
             "po przecinku w taki spos^ob:\n" +
             "odpal wybory Adam, Ewa, tablica ogloszeniowa\n" +
             "Je^zeli nie ma podanych ^zadnych opcji to automatyczna " +
             "urna podstawia automatycznie: za, przeciw i wstrzyma^l.\n\n" +
             "@@obecne@@");

    add_prop(OBJ_I_WEIGHT, 60000);
    add_prop(OBJ_I_VOLUME, 100000);
    add_prop(OBJ_I_VALUE, 1000);



}

public int
query_glosowal(string kto)
{
    if (member_array(kto, glosowali) > -1)
        return 1;

    return 0;
}


int
podlicz(string str)
{
    int i;
    for (i = 0; i < sizeof(opcje); i += 1)
    {
        tell_room(environment(this_player()), "Urna m^owi: " + (i + 1) + ". " +
                  opcje[i][0] + " : " + opcje[i][1] + ".\n");
    }
    glosowali = ({ });
    opcje = ({ });
    zajete = 0;
    return 1;    
    return 1;
}

int
glos(string str)
{
    int num;
    notify_fail("Zag^losuj na co?\n");

    if (query_glosowal( this_player()->query_real_name() ) )
    {
        notify_fail("G^losuje si^e tylko raz na kolejk^e.\n");        
        return 0;
    }
    if (!str || !sscanf(str, "na %s", str))
        return 0;

    num = LANG_NUMS(str);

    if (num < 1 || num > sizeof(opcje) + 1 || !num)
        return 0;

    opcje[num - 1][1] += 1;

    glosowali += ({ this_player()->query_real_name() });

    saybb(QCIMIE(this_player(), PL_MIA) + " wciska guzik do g^losowania.\n");
    write("Oddajesz sw^oj glos na opcje nr " + num  + ". " +
          opcje[num - 1][0] + ".\n"); 

    return 1;
}

int
odpal(string str)
{
    string str1;
    int i;

    notify_fail("Odpal co?\n");

    if (!str)
        return 0;

    if (zajete)
    {
        notify_fail("W tej chwili trwa ju^z jakie^s g^losowanie.\n");
        return 0;
    }


    if (str == "wybory")
    {
        opcje += ({({"Za", 0}),({"Przeciw", 0}), ({"Wstrzyma^l si^e", 0})});
        zajete = 1;
        saybb(QCIMIE(this_player(), PL_MIA) + " przekr^eca ga^lke i odpala g^losowanie.\n");
        write("Nastawiasz wszystkie ga^lki i odpalasz g^losowanie.\n"); 
        return 1;
    }

    if (!sscanf(str, "wybory %s", str))
        return 0;


    i = 2;
    while (i > 0)
    {
        if (str1)
            opcje += ({({str1, 0})});            
        i = sscanf(str, "%s, %s", str1, str);
    }

    opcje += ({({str, 0})});
    zajete = 1;
    saybb(QCIMIE(this_player(), PL_MIA) + " przekr^eca ga^lke i odpala g^losowanie.\n");
    write("Nastawiasz wszystkie ga^lki i odpalasz g^losowanie.\n"); 
    return 1;
}

string
obecne()
{
    string str;
    int i;

    if (!sizeof(opcje))
        return "Nie ma teraz ^zadnego g^losowania.\n";

    for (i = 0; i < sizeof(opcje); i += 1)
    {
        str = str + (i + 1) + ". " + opcje[i][0] + ".\n";
    }
    return str;
}

int
zresetuj(string str)
{
    if (!(str ~= "urn^e"))
    {
        notify_fail("Zresetuj co?\n");
        return 0;
    }

    saybb(QCIMIE(this_player(), PL_MIA) + " potrz^asa energicznie urn^a wyborcz^a.\n");
    write("Zbierasz wszystkie si^ly po czym rzucasz si^e na urn^e wyborcza " +
          "resetuj^ac j^a dokumentnie.\n");

    glosowali = ({ });
    opcje = ({ });
    zajete = 0;
    return 1;
}



void
init()
{
    ::init();
    add_action(zresetuj, "zresetuj");
    add_action(odpal, "odpal");
    add_action(glos, "zag^losuj");
    add_action(podlicz, "podlicz");
}
