//Lil, operacja wstepne Thanedd, dnia 8.11.2005
inherit "/std/board";

#include <pl.h>
#define GAR "/d/Standard/wiz/garstang/"

create_board()
{
    ustaw_shorty( ({"wielka tablica wie�ciowa", "wielkiej tablicy wie�ciowej",
	"wielkiej tablicy wie�ciowej", "wielk� tablic� wie�ciow�",
	"wielk� tablic� wie�ciow�", "wielkiej tablicy wie�ciowej"}),
	({"wielkie tablice wie�ciowe", "wielkich tablic wiesciowych",
	"wielkim tablicom wiesciowym", "wielkie tablice wiesciowe",
	"wielkimi tablicami wiesciowymi", "wielkich tablicach wiesciowych"}),
	PL_ZENSKI);
    dodaj_przym("wielki", "wielcy");
    dodaj_przym("wie�ciowy", "wie�ciowi");
    
    set_board_name(GAR+"wiesci");
    set_num_notes(1000);
    set_silent(1);
    set_keep_discarded(1);
}

int wiesci(string str)
{
    int numer;

// Jesli nie podano argumentu, wyswietl spis wiesci
    if (!str)
    {
	if (!msg_num)
	{
	    write("Nie ma w tej chwili �adnych wie�ci.\n");
	}
	else
	{
	    write("\nOto najnowsze dost�pne wie�ci:\n\n" +
		list_headers(1, msg_num) + "\n");
	}
	return 1;
    }

// Jezeli jako argument podano liczbe, wyswietl wiesc o tym numerze
    if (sscanf(str, "%d", numer))
    {
	if ((numer < 1) || (numer > msg_num))
	{
	    write("Nie ma wie�ci o takim numerze!\n");
	    return 1;
	}
	write("\n");
	read_msg(str, 1);
	write("\n");
	return 1;
    }

// W przeciwnym wypadku zwroc standardowy komunikat o niepowodzeniu
    return 0;
}
