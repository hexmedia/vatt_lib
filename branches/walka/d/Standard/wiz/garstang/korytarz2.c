//Lil, operacja wstepne Thanedd, dnia 8.11.2005
inherit "/std/room";

#include <stdproperties.h>
#define GAR "/d/Standard/wiz/garstang/"

create_room()
{
    set_short("Dalsza cz�� kamiennego Korytarza w Garstangu");
    set_long("Nagi, ciemny i ch�odny kamie� otacza ci� zewsz�d. Korytarz ci�gnie "+
    		"si� jeszcze dalej na po�udnie. Wej�cia do komnat zarz�dzania "+
			"patrolami gwardii ca�ego �wiata znajduj� si� prawie z ka�dej strony.\n");
  
    add_exit(GAR+"korytarz1.c",({"s","wg��b korytarza"}));
	add_exit(GAR+"podania.c",({"e","do szafy na zachodzie","z korytarza"}));
    add_exit("admin_straza",
    			({"n","na p�noc do pomieszczenia zarz�dzania patrolami"}));
    add_exit("/d/Standard/wiz/players", "ne");
}
