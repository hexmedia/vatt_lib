//Lil, operacja wstepne Thanedd, dnia 8.11.2005
inherit "/std/room";
#include <stdproperties.h>
#include <pogoda.h>
#define GAR "/d/Standard/wiz/garstang/"
#define ARE "/d/Standard/wiz/"
string blabla();
string pogodynka();
object plan_pogody;

create_room()
{
    set_short("Niewielki placyk w�r�d ogrodu piwonii");
    set_long("Starannie wybrukowany placyk, gdzieniegdzie z "+
             "pozostawionymi miejscami na rosn�ce tu piwonie otaczaj� "+
	     "zewsz�d mury pa�acu Aretuzy. @@blabla@@\n");
    add_sit("na bruku","na bruku","z bruku",0);
    add_exit(GAR+"schody_out",({"p�noc","na schody, w kierunku Garstangu"}));
    add_exit(ARE+"specjalny",({"ne","na p�nocny wsch�d, do Galerii Chwa�y"}));
    add_exit(ARE+"wizroom",({"e","na wsch�d do g��wnego hallu pa�acu Aretuzy"}));
    add_object("/d/Standard/items/meble/lawka_zielona_wygodna");
    dodaj_rzecz_niewyswietlana("zielona wygodna �awka", 1);


    add_item(({"po�wiat�","niebieskaw� po�wiat�"}),
             "B��kitne promienie otaczaj� skacz�ce i rozmazuj�ce si� napisy, "+
             "z kt�rych mo�esz wyczyta�:\n\n"+
             //POGODA :P
             "@@pogodynka@@");
}


string
sprawdz_pogode_dla(int x, int y)
{
    string str="";

    if(!x && !y)
      str+=set_color(32)+"Pogoda dla Rinde:"+clear_color()+"\n";
    else if(x==-1 && y==-1)
      str+=set_color(32)+
           "\nPogoda dla po�udniowo zachodnich okolic Rinde:"+
           clear_color()+"\n";
	else if(x==-4 && y==-2)
      str+=set_color(32)+
           "\nPogoda dla Bia�ego Mostu i jego okolic:"+
           clear_color()+"\n";
	else if(x==0 && y==2)
      str+=set_color(32)+
           "\nPogoda dla Zakonu, jeziorka i okolic (trakt Tretogor-Rinde):"+
           clear_color()+"\n";

  str+="  Zachmurzenie: ";
  switch(plan_pogody->get_zachmurzenie()) {
    case POG_ZACH_ZEROWE: str+="zerowe\n"; break;
    case POG_ZACH_LEKKIE: str+="lekkie\n"; break;
    case POG_ZACH_SREDNIE: str+="�rednie\n"; break;
    case POG_ZACH_DUZE: str+="du�e\n"; break;
    case POG_ZACH_CALKOWITE: str+="ca�kowite\n"; break;
  }
  str+="  Temperatura:  " + plan_pogody->get_temperatura() + "\n";
  str+="  Opady: \t";
  switch(plan_pogody->get_opady()) {
    case POG_OP_ZEROWE: str+="brak\n"; break;
    case POG_OP_D_L: str+="lekki deszcz\n"; break;
    case POG_OP_D_S: str+="�redni deszcz\n"; break;
    case POG_OP_D_C: str+="ci�ki deszcz\n"; break;
    case POG_OP_D_O: str+="oberwanie chmury\n"; break;
    case POG_OP_S_L: str+="lekki �nieg\n"; break;
    case POG_OP_S_S: str+="�redni �nieg\n"; break;
    case POG_OP_S_C: str+="ci�ki �nieg\n"; break;
    case POG_OP_G_L: str+="lekki grad\n"; break;
    case POG_OP_G_S: str+="�redni grad\n"; break;
  }
  str+="  Wiatry: \t";
  switch(plan_pogody->get_wiatry()) {
    case POG_WI_ZEROWE: str+="brak\n"; break;
    case POG_WI_LEKKIE: str+="lekkie\n"; break;
    case POG_WI_SREDNIE: str+="�rednie\n"; break;
    case POG_WI_DUZE: str+="du�e\n"; break;
    case POG_WI_BDUZE: str+="bardzo du�e\n"; break;
  }

    return str;
}

string
pogodynka()
{   //Zer�ni�te z pomieszczenia pogodowego ;p
    int x=0, y=0; //dla Rinde
    string toret="";
    plan_pogody = POGODA_OBJECT->get_plan(x, y);
    if (!objectp(plan_pogody)) {
        return "Kszzzzz Kszzzz, ni ma!";
    }

    toret+=sprawdz_pogode_dla(x, y);
    toret+=sprawdz_pogode_dla(-1,-1); //okolice Rinde (na SW od Rinde)
	toret+=sprawdz_pogode_dla(-4,-2); //Bia�y Most i jego okolice
	toret+=sprawdz_pogode_dla(0,2);   //zakon i jego okolice
    return toret;

}

string blabla()
{
    string str;
    if(jest_rzecz_w_sublokacji(0,"zielona wygodna �awka"))
    str="W �rodkowej cz�ci na bruku dostrzegasz niebieskaw� po�wiat� "+
        "emanuj�c� migotliwymi promieniami uk�adaj�cymi si� w kr�g. "+
        "Przed ni� stoi sobie zielona wygodna �awka.";
    else
    str="W �rodkowej cz�ci na bruku dostrzegasz niebieskaw� po�wiat� "+
        "emanuj�c� migotliwymi promieniami uk�adaj�cymi si� w kr�g.";
    return str;
}
