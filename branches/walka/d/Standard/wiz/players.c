inherit "/std/room";

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <files.h>

mixed players = ([ ]);

#define P_EXP		0
#define P_STATS		1
#define P_AGE		2

#define P_MAX		3

void
create_room()
{
    set_short("Pomieszczenie zarz^adzania graczami");
    set_long("Znajdujesz si^e w niewielkim pomieszczeniu, z kt^orego " +
	"czarodzieje ^sledz^a losy ^smiertelnik^ow.\n");

    BREAKDOWN_OBJECT->move(TO, 1);

    add_exit("/d/Standard/wiz/garstang/korytarz2", "sw");

    add_prop(ROOM_I_INSIDE, 1);
    
    seteuid(getuid());
}

void
init()
{
    add_action("aktualizuj", "aktualizuj");
    add_action("wyswietl", "wy^swietl");
    ::init();
}

public int
aktualizuj(string str)
{
    string *dir;
    object finger;
    string name;
    int i;

    players = ([ ]);
    foreach (string l : explode(ALPHABET, ""))
    {
	dir = get_dir("/players/" + l + "/");
	foreach (string pl : dir)
	{
	    if (pl[-2..-1] == ".o")
	    {
		name = pl[..-3];
		finger = SECURITY->finger_player(name);
		players[name] = allocate(P_MAX);
		players[name][P_EXP] = allocate(6);
		players[name][P_STATS] = allocate(6);
		
		try {
		    for (i = 0; i < 6; ++i)
			players[name][P_EXP][i] = finger->query_acc_exp(i);
		    for (i = 0; i < 6; ++i)
			players[name][P_STATS][i] = finger->query_stat(i);
		} catch (mixed e) {
		    write("Nie mog^e odczyta^c gracza '" + name + "'.\n");
		}
	    }
	}
    }

    return 1;
}

public int
wyswietl(string str)
{
    string ret = "";

    notify_fail("Wy^wietl exp / stat / wiek.\n");
    if (!str)
	return 0;

    if (str == "exp")
    {
	foreach (string name : sort_array(m_indexes(players)))
	{
	    ret += sprintf("%-10s%10@d\n", capitalize(name), players[name][P_EXP]);
	}
	this_player()->more(ret);
	return 1;
    }

    return 1;
}
