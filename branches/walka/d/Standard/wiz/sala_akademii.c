//Lil, operacja wstepne Thanedd, dnia 8.11.2005
inherit "/std/room";
#include <stdproperties.h>

void
create_room()
{
  set_short("G��wna sala Akademii Aretuzy");
  set_long("Tutaj cz�onkowie Akademii maj� sw� tablic�. "+
           "Tak�e na niej s� pisane wszelkie zlecenia dla uczni�w etc. "+
	   "Na po�udnie st�d mie�ci si� niewielki urz�d pocztowy.\n");
  add_object("/d/Standard/wiz/tablica_akademii");
  add_exit("/d/Standard/wiz/wizroom", ({"nw",
         "do g��wnego hallu pa�acu Aretuzy, opuszczaj�c tym samym Akademi�"}));
  add_exit("/d/Standard/wiz/poczta",({"s","na po�udnie, do poczty"}));
  add_exit("/d/Standard/slownik/slownik",
               ({"s�ownik","do Wielkiej Machiny S�ownika Vatt'gherna"}));
    add_prop(ROOM_I_INSIDE, 1);
}
