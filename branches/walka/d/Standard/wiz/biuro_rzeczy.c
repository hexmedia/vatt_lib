/*    Magazyn Rzeczy Znalezionych.
      P�ki co jest tylko dla wizard�w dost�pny.
      Vera */

#include <macros.h>
inherit "/std/room.c";
#include <stdproperties.h>
#include <mudtime.h>
//#include "dir.h"


string
dlugi_opis()
{
   string str = "To du�e pomieszczenie zastawione jest szafami, "+
   "na kt�rych a� roi si� od jakich� grat�w. Jako �e na p�kach "+
   "zabrak�o ju� miejsca, ogromna ilo�� rupieci wala si� po "+
   "pod�odze, nie daj�c mo�liwo�ci przedostania si� na drugi koniec "+
   "izby. Je�li kto� tu sprz�ta, to robi to chyba wyj�tkowo rzadko - "+
   "�wiat�o wpadaj�ce przez niedu�e okienko za�amuje si� na drobinkach "+
   "kurzu unosz�cych si� w powietrzu.\n";
		 
	return str;
}


void
create_room()
{
 set_short("Magazyn biura rzeczy znalezionych");
 set_long("@@dlugi_opis@@");
 	   

   add_sit("na pod�odze", "po pod�odze", "z pod�ogi", 0);
   add_exit("/d/Standard/wiz/korytarz1","n", 0, 0);
   add_prop(ROOM_I_INSIDE,1);
   
}
public string
exits_description() 
{
    return "Wyj^scie z magazynu prowadzi na p�nocy.\n";
}

