//Vera, opisiki skonfiskowane z workrooma Duanki
#include <stdproperties.h>
inherit "/std/room";

//prevent_enter_env(object dest)
public int
check_mortal()
{
	if(objectp(this_player())  && !this_player()->query_wiz_level())
	{
		write("Jaka� pot�na si�a zabrania ci tego!\n");
		return 1;
	}	
	return 0;
}

create_room()
{
   add_exit("/d/Standard/wiz/loxia_schody",({"trumna","do trumny",
		"z Vox Mortalis"}),  "@@check_mortal@@");

    set_long("Sporych rozmiar^ow pomieszczenie o podstawie kwadratu "+
        "trudno nazwa^c pokojem. Na kamiennych ^scianach brakuje "+
        "jakiegokolwiek ^sladu po oknach czy drzwiach i oczywistym jest, "+
        "^ze nie spos^ob wydosta^c si^e st^ad w ^zaden konwencjonalny "+
        "spos^ob. Najdziwniejsza jest jednak wysoko^s^c pomieszczenia. "+
        "Przypomina ono bowiem czworok^atn^a studni^e czy te^z mo^ze "+
        "pionowy tunel biegn^acy gdzie^s wysoko. Przy ^swietle ^swiec nie "+
        "spos^ob dociec na jakiej wysoko^sci znajduje si^e strop. By^c "+
        "mo^ze w^la^snie dlatego budzi on uczucie niepokoju. Centraln^a "+
        "cz^e^s^c pokoju zajmuje masywny st^o^l z p^lon^acymi na nim czarnymi "+
        "^swiecami oraz stoj^ace z dw^och jego stron krzes^la. Pod jedn^a "+
        "ze ^scian, na katafalku, najzwyczajniej w ^swiecie le^zy drewniana "+
        "trumna, a nad ni^a na ^scianie wisi wielki wypchany nietoperz. Po "+
        "przeciwnej za^s stronie postawiono pod drug^a ^scian^a dziwnie "+
        "wygl^adaj^ac^a sof^e, a po jej prawej stronie, w niedu^zej "+
        "odleg^lo^sci, gilotyn^e. W rogach pomieszczenia jarz^a si^e trupim "+
        "blaskiem wysokie ^swieczniki.\n");



        add_prop(ROOM_I_LIGHT,2);
        add_prop(ROOM_I_INSIDE, 1);
        //set_short("mroczny ciemny pok^oj");
		set_short("Vox Mortalis. Mroczny ciemny pok^oj");
        add_sit("na sofie","na sofie","z sofy",3);
        add_sit("na krze^sle","na krze^sle","z krzes^la",1);
        add_sit("na drugim krze^sle","na drugim krze^sle","z drugiego krzes^la",1);


        //add_exit("/d/Aretuza/valor/workroom", ({"jama", "przez jam^e"}), 0, 0);
        //add_exit("/d/Aretuza/duana/workroom/trumna.c", ({"trumna", "do trumny"}), 0, 0);


        add_item("krzes^lo","Bli^xniaczo wygl^adaj^ace do stoj^acego po "+
            "przeciwnej stronie drugiego krzes^la, to r^ownie^z postawiono "+
            "przy wielkim stole. U^zycie identycznego drewna i podobna "+
            "budowa oraz masywny kszta^lt zdradzaj^a, ^ze te trzy meble "+
            "wykona^l ten sam rzemie^slnik, prawdopodobnie na specjalne "+
            "zam^owienie. Krzes^lo nie posiada ^zadnych ozd^ob, obi^c, ani "+
            "por^eczy i z powodu swej twardo^sci mo^ze znacz^aco doskwiera^c "+
            "przy d^lu^zszym na nich siedzeniu.\n");

        add_item("drugie krzes^lo","Bli^xniaczo wygl^adaj^ace do stoj^acego "+
            " po przeciwnej stronie drugiego krzes^la, to r^ownie^z "+
            "postawiono przy wielkim stole. U^zycie identycznego drewna i "+
            "podobna budowa oraz masywny kszta^lt zdradzaj^a, ^ze te trzy "+
            "meble wykona^l ten sam rzemie^slnik, prawdopodobnie na "+
            "specjalne zam^owienie. Krzes^lo nie posiada ^zadnych ozd^ob, "+
            "obi^c, ani por^eczy i z powodu swej twardo^sci mo^ze znacz^aco "+
            "doskwiera^c przy d^lu^zszym na nich siedzeniu.\n");

        add_item("pod^log^e","Utworzona z jednej wielkiej bazaltowej p^lyty. "+
            "Nie wypolerowano jej ca^lkowicie, przez co wyczuwalne s^a pod "+
            "stopami lekkie wypuk^lo^sci i nier^owno^sci.\n");

       add_item("krzes^la","Bli^xniaczo wygl^adaj^ace ci^e^zkie krzes^la "+
            "postawiono po obu stronach wielkiego sto^lu. U^zycie "+
            "identycznego drewna i podobna budowa oraz masywny kszta^lt "+
            "zdradzaj^a, ^ze te trzy meble wykona^l ten sam rzemie^slnik, "+
            "prawdopodobnie na specjalne zam^owienie. Krzes^la nie posiadaj^a "+
            "^zadnych ozd^ob, obi^c, ani por^eczy i z powodu swej twardo^sci "+
            "mog^a znacz^aco doskwiera^c przy d^lu^zszym na nich siedzeniu.\n");

        add_item("^sciany","Na pierwszy rzut oka bardzo grube i solidne "+
            "^sciany wymurowano z du^zych bry^l ciemnego granitu, spajaj^ac je "+
            "ze sob^a bordowego koloru zapraw^a. Kamienie bardzo dok^ladnie "+
            "oszlifowano i wyr^ownano tak, ^ze wspi^a^c si^e po nich m^og^lby "+
            "chyba tylko paj^ak.\n");

        add_item(({"strop","sufit","g^or^e"}),"Gdy oczy twe kieruj^a si^e w "+
            "g^or^e by spojrze^c na sufit, okazuje si^e, ^ze go nie wida^c. "+
            "^Swiat^la ^swiec za s^labo rozja^sniaj^a sal^e by m^oc go dojrze^c i z "+
            "pewno^sci^a musi by^c wysoko, poniewa^z wyczulone w p^o^lmroku "+
            "zmys^ly wyczuwaj^a u g^ory pustk^e. Wyt^e^zaj^ac jednak oczy, mo^zna "+
            "dostrzec co^s co ledwo majaczy na granicy ciemno^sci. Kilka "+
            "niewyra^xnych kszta^lt^ow zdaje si^e zwisa^c nieruchomo w "+
            "powietrzu. Skupiaj^ac si^e jeszcze bardziej, wida^c, ^ze s^a to "+
            "powieszone ludzkie cia^la.\n");

        add_item("st^o^l","Masywny st^o^l stoi majestatycznie po^srodku "+
            "pomieszczenia niczym mityczny wielki lewiatan p^lawi^acy si^e na "+
            "powierzchni morza. Grube deski wyci^eto z bardzo ciemnego, "+
            "niemal czarnego, drewna bez s^ek^ow. Kr^otkie i bardzo grube nogi "+
            "sto^lu zmy^slny rze^xbiarz wyciosa^l na kszta^lt zwierz^ecych ^lap "+
            "zako^nczonych pot^e^znymi gryfimi pazurami, kt^ore zdaj^a si^e ora^c "+
            "i wrzyna^c w kamienn^a pod^log^e. Drewno pokrywaj^a brunatne ^slady "+
            "zakrzepni^etej krwi, a w blacie widnieje g^l^eboko wbity n^o^z, "+
            "obok kt^orego stoj^a p^lon^ace czarne ^swiece.\n");

        add_item("^swiece","Cztery ^swiece grubo^sci m^eskiego przedramienia "+
            "stoj^a nieco z boku blatu masywnego ciemnego sto^lu. Dodany do "+
            "nich sk^ladnik lub sk^ladniki spowodowa^ly, ze nabra^ly one "+
            "ca^lkowicie czarnego koloru. Pal^a si^e ^z^o^ltym ogniem.\n");

        add_item("^swiec^e","Jest to ^swieca grubo^sci m^eskiego przedramienia. "+
            "Dodany do niej sk^ladnik lub sk^ladniki spowodowa^ly, ze nabra^la "+
            "ona ca^lkowicie czarnego koloru. Pali si^e ^z^o^ltym ogniem.\n");

        add_item("n^o^z","Wbity a^z do po^lowy ostrza n^o^z sterczy ze ^srodka "+
            "masywnego ciemnego sto^lu niczym ko^s^c jakiego^s stwora. R^ekoje^s^c "+
            "wykonano ze smoczego k^la, kt^ory misternie wyrze^xbiono "+
            "delikatnymi rytami. Ostrze z^lowrogo b^lyszczy odbijaj^ac ^swiat^lo "+
            "^swiecznik^ow i ^swiec.\n");

        add_item("trumn^e","Solidnie wykonana z wi^sniowego drewna trumna "+
            "spoczywa spokojnie na kamiennym katafalku. Cicho zaprasza do "+
            "swego wn^etrza kusz^ac obietnic^a s^lodkiego snu ostatecznego i "+
            "ca^lkowitego zapomnienia, kt^ore ukoj^a ka^zdy b^ol i smutek. "+
            "Wy^scielono j^a szkar^latnym aksamitem i w^lo^zono do^n bukiet "+
            "niespotykanie bia^lych ja^smin^ow o intensywnym zapachu. "+
            "Jest otwarta.\n");

        add_item("katafalk","Wykuty z pojedynczego bloku marmuru katafalk "+
            "ci^e^zko stoi pod jedn^a ze ^scian. Pokryto go w wielu miejscach "+
            "tajemnymi runami o niewiadomej tre^sci. Niezwyk^ly, wr^ecz "+
            "nienaturalny ch^l^od, kt^ory z niego emanuje, budzi l^ek i mrozi "+
            "krew w ^zy^lach, ale r^ownocze^snie w nieodparty spos^ob przyci^aga. "+
            "Spoczywa na nim trumna.\n");

        add_item("nietoperza","Na ^scianie przy trumnie zawieszono olbrzymich "+
            "wr^ecz rozmiar^ow nietoperza pokrytego g^estym szczeciniastym "+
            "w^losiem. Wielkie b^loniaste skrzyd^la o rozpi^eto^sci niemal "+
            "siedmiu st^op z^lowrogo rozchylaj^a si^e nad trumn^a. Paskudny "+
            "rozwarty pysk stwora ukazuje mas^e d^lugich i ostrych z^eb^ow oraz "+
            "czterech, jeszcze wi^ekszych, k^l^ow. M^etne oczy maszkary "+
            "spogl^adaj^a na ciebie lodowato. \n");

        add_item("sof^e","^Srednich rozmiar^ow sofa zach^eca by na niej usi^a^s^c. "+
            "Obszyto j^a delikatnym i mi^eciutkim futerkiem, kt^ore jest "+
            "bardzo mi^le w dotyku. Po przyjrzeniu okazuje si^e, ^ze jest to "+
            "kocie futro, a pozna^c mo^zna to po wystaj^acych tu i ^owdzie "+
            "^lapkach, uszkach i ogonkach s^lodkich futrzak^ow. Za zwie^nczenie "+
            "opar^c sofy s^lu^z^a wystaj^ace dwa du^ze wypchane ^lby kocur^ow.\n");

        add_item("gilotyn^e","Standardowe wyposa^zenie, ka^zdego grodu, "+
            "kasztelu, czy te^z wi^ekszego miasta. Solidna drewniana "+
            "konstrukcja zmniejsza ryzyko awarii b^ad^x nieprzewidzianego "+
            "wypadku, a wielkie ostrze, spuszczane z wysoko^sci sze^sciu st^op "+
            "na kark ofiary, zapewnia bardzo szybk^a i bezbolesn^a ^smier^c.\n");

        add_item("^swieczniki","Stoj^ace w ka^zdym z rog^ow pokoju ^swieczniki "+
            "rzucaj^a dooko^la m^etne, jakby ska^zone, sino^z^o^lte ^swiat^lo. "+
            "Stojaki wykonane ze srebra dor^ownuj^a wzrostem doros^lemu "+
            "m^e^zczy^xnie. Nie wiesz dlaczego, ale zbyt bliskie przebywanie "+
            "przy nich wywo^luje u ciebie lekkie md^lo^sci i bli^zej "+
            "nieokre^slony wstr^et. Nienaturalne ^swiat^lo zdaje si^e by^c "+
            "efektem magii lub innej wy^zszej si^ly.\n");

        add_item("^swiecznik","Stoj^acy w ka^zdym z rog^ow pokoju ^swiecznik "+
            "rzuca dooko^la m^etne, jakby ska^zone, sino^z^o^lte ^swiat^lo. Stojak "+
            "wykonany ze srebra dor^ownuje wzrostem doros^lemu m^e^zczy^xnie. "+
            "Nie wiesz dlaczego, ale zbyt bliskie przebywanie przy nim "+
            "wywo^luje u ciebie lekkie md^lo^sci i bli^zej nieokre^slony wstr^et.\n");
    //add_object("/d/Aretuza/duana/sypialnia_bm/drzwi2.c");

add_event("Zza ^sciany dobiega ci^e tajemnicze drapanie.\n", 30);
add_event("Masz wra^zenie, ^ze przez chwil^e kto^s wygl^ada^l z trumny.\n", 10);
add_event("P^lomienie ^swiec drgn^e^ly poruszone tajemniczym podmuchem.\n", 40);

}