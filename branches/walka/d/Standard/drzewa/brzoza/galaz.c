inherit "/std/drzewo/galaz";

#include <mudtime.h>
#include <stdproperties.h>
#include <object_types.h>

create_galaz()
{
	ustaw_nazwe("galaz");
	random_przym("bia�awy:bia�awi||prosty:pro�ci||smuk�y:smukli||g�adki:g�adcy", 2);
	set_long("@@dlugi@@");
	ustaw_gestosc(0.65);
	set_type(O_DREWNO);
	add_prop(OBJ_I_WEIGHT, 2300);
}

string dlugi()
{
	string opis = "";
	
	if(find_room()->pora_roku() == MT_LATO)
	{
		opis += "Na ga��zi dostrzegasz sporo drobnych listk�w o z�bkowatych"
				+" kraw�dziach, kt�re kontrastuj� z jej bia�� kor�. Ma ona raczej"
				+" prosty i �agodny kszta�t, cho� odrasta od niej wiele, innych, cieniutkich"
				+" ga��zek.";
	}
	
	if(find_room()->pora_roku() == MT_JESIEN)
	{
		opis += "Na ga��zi dostrzegasz wiele drobnych, zbr�zowia�ych listk�w o"
				+" z�bkowanych kraw�dziach. Ma ona raczej prosty i �agodny kszta�t,"
				+" cho� odrasta od niej wiele, innych, cieniutkich ga��zek.";
	}
	
	if(find_room()->pora_roku() == MT_WIOSNA)
	{
		opis += "Na ga��zi dostrzegasz drobne, jasnozielone p�czki oraz kilka m�odziutkich,"
				+" ledwo rozwini�tych listk�w. Ma ona raczej prosty i �agodny kszta�t,"
				+" cho� odrasta od niej wiele, innych, cieniutkich ga��zek.";
	}
	
		
	if(find_room()->pora_roku() == MT_ZIMA)
	{
		opis += "Na ga��zi nie dostrzegasz ani jednego listka, za� jej naturalnie bia�a kora wydaje si�"
				+" by� nieco poczernia�a. Ga��� ma raczej prosty i �agodny kszta�t,"
				+" cho� odrasta od niej wiele, innych, cieniutkich ga��zek.";
	}
	
	opis += "\n";
	
	return opis;
}

