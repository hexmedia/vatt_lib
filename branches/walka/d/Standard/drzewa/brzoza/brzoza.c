inherit "/std/drzewo";

#include <mudtime.h>
#include <pl.h>
#include <stdproperties.h>
#include <object_types.h>

void create_tree()
{
	ustaw_nazwe( ({"brzoza", "brzozy", "brzozie", "brzoz�", "brzoz�", "brzozie"}) ,
				({"brzozy", "brz�z", "brzozom", "brzozy", "brzozami", "brzozach"}),
				PL_ZENSKI);
				
	random_przym("smuk�y:smukli||strzelisty:strzeli�ci||bia�awy:bia�awi||"
				+ "omsza�y:omszali", 1);
	set_long("@@dlugi@@");
	set_sciezka_galezi("/d/Standard/drzewa/brzoza/galaz.c");
	set_sciezka_klody("/d/Standard/drzewa/brzoza/kloda.c");
	set_cena(2.75);
	set_gestosc(0.65);
	set_type(O_DREWNO);
	add_prop(OBJ_I_WEIGHT, random(1450000)+50000);
				
	setuid();
	seteuid(getuid());
}

string dlugi()
{
	string opis = "";
	
	if(find_room()->pora_roku() == MT_WIOSNA)
	{
		opis += "Na poruszanych delikatnym wiatrem ga��ziach zauwa�asz"
				+" jasnozielone p�czki oznajmiaj�ce nadej�cie wiosny.";
		
		if(query_sciete())
			opis += " Pokryty bia�aw� kor�, niezbyt gruby pie� niegdy� d�wiga� na sobie"
					+" ci�ar rozbujanych ga��zi, teraz za� le�y martwo, zwalony na ziemi�.";
		else
			opis += " Pokryty bia�aw� kor�, niezbyt gruby, pie� d�wiga na sobie"
					+" ci�ar rozbujanych ga��zi z kt�rych co jaki� czas spada przyschni�ta ga��zka.";
	}
	
	if(find_room()->pora_roku() == MT_ZIMA)
	{
		opis += "�yse ga��zie drzewa leniwie ko�ysz� si� poruszane mro�nym wiatrem.";
		
		if(query_sciete())
			opis += " Pokryty bia�aw� kor�, niezbyt gruby, pie� niegdy� d�wiga� na sobie"
					+" ci�ar rozbujanych ga��zi, teraz za� le�y martwo, zwalony na ziemi�.";
		else
			opis += " Pokryty bia�aw� kor�, niezbyt gruby, pie� d�wiga na sobie"
					+" ci�ar rozbujanych ga��zi z kt�rych co jaki� czas spada przyschni�ta ga��zka.";
	}
	
	if(find_room()->pora_roku() == MT_JESIEN || find_room()->pora_roku() == MT_LATO)
	{
		opis += "Drobne li�cie drzewa szumi� poruszane od czasu do czasu podmuchami wiatru.";
		
		if(query_sciete())
			opis += " Pokryty bia�aw� kor�, niezbyt gruby, pie� niegdy� d�wiga� na sobie"
					+" ci�ar rozbujanych ga��zi, teraz za� le�y martwo, zwalony na ziemi�.";
		else
			opis += " Pokryty bia�aw� kor�, niezbyt gruby, pie� d�wiga na sobie ci�ar rozbujanych" 
					+" ga��zi z kt�rych co jaki� czas spada przy��k�y li��.";
	}
	
	if(query_mlode())
		opis += " Ta brzoza jest jeszcze do�� m�oda, jej kora jest niezwykle g�adka zdaje si�, i� niemo�liwym jest oderwanie jej od pnia.";
	else
		opis +=  " Ta brzoza z pewno�ci� liczy sobie wiele lat, jej pomarszczona kora w niekt�rych miejscach pokryta jest zielonkawym nalotem.";
	
	opis += "\n";
	return opis;
}
