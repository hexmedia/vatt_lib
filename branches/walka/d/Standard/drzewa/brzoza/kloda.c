inherit "/std/drzewo/kloda";

#include <stdproperties.h>
#include <object_types.h>

void create_kloda()
{
	ustaw_nazwe("kloda");

	random_przym("smuk�y:smukli||bia�awy:bia�awi||omsza�y:omszali", 1);

	set_long("K�oda okryta jest bia��, g�adk� kor�, cho� dawna podstawa"
			+" pnia posiada brudn�, czarnozielonkaw� barw�. Nie jest ona"
			+" zbyt gruba, jej obw�d szybko maleje wraz z d�ugo�ci�.\n");

	set_type(O_DREWNO);
	
	setuid();
	seteuid(getuid());
}
	
