inherit "/std/drzewo";

#include <materialy.h>
#include <mudtime.h>
#include <object_types.h>
#include <pl.h>
#include <stdproperties.h>

void create_tree()
{
	ustaw_nazwe(({"buk", "buku", "bukowi", "buk", "bukiem", "buku"}),
		    ({"buki", "buk�w", "bukom", "buki", "bukami", "bukach"}),
		    PL_MESKI_NZYW);

	random_przym("strzelisty:strzeli�ci||g�adki:g�adcy||popielatoszary:popielatoszarzy", 1);

	set_long("@@dlugi@@");

	set_type(O_DREWNO);
	ustaw_material(MATERIALY_DR_BUK, 100);

	ustaw_gestosc(0.73);
	add_prop(OBJ_I_WEIGHT, random(1450000)+50000);

	ustaw_sciezke_galezi("/d/Standard/drzewa/buk/galaz.c");
	ustaw_sciezke_klody("/d/Standard/drzewa/buk/kloda.c");
	ustaw_cene(2.75);
		    
	setuid();
	seteuid(getuid());
}

string dlugi()
{
	string opis = "Niezwykle g�adka kora drzewa przywodzi na my�l"
				+" delikatno�� aksamitu.";
	
	if(query_sciete())
	{
		opis += " Niemal po�owa pnia pozbawiona jest ga��zi,"
				+" jednak na jego ko�cu rozrastaj� si� one"
				+" swodobnie na wszystkie strony.";
	}
	else
	{
		opis += " Kilka pojedy�czych ga��zi wyrasta niewysoko ponad"
				+" ziemi�, za� w koronie rozpo�cieraj� si� one na"
				+" wszystkie strony, tworz�c g�ste sklepienie."
				+" Drzewo co jaki� czas skrzypi przeci�gle poruszane"
				+" silniejszymi podmuchami wiatru.";
	}

	opis += "\n";

	return opis;

}
