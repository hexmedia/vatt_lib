inherit "/std/drzewo/galaz";

#include <materialy.h>
#include <mudtime.h>
#include <object_types.h>

void create_galaz()
{
	ustaw_nazwe("galaz");
	random_przym("wykr�cony:wykr�ceni �ukowaty:�ukowaci"
		+" szponiasty:szponia�ci||omsza�y:omszali g�adki:g�adcy"
		+" pomarszczony:pomarszczeni||srebrzystoszary:srebrzystoszarzy", 2);

	set_long("@@dlugi@@");

	ustaw_material(MATERIALY_DR_BUK, 100);
	set_type(O_DREWNO);
}

string dlugi()
{
	string opis = "Rozro�ni�ta ga���";
	
	if(find_room()->pora_roku() == MT_WIOSNA)
	{
		opis += " ozdobiona jest zielonymi p�czkami, kt�re"
				+" niebawem przekszta�c� si� w drobne listki.";
	}

	if(find_room()->pora_roku() == MT_LATO)
	{
		opis += " pokryta jest g�sto drobnymi, owalnymi li��mi.";
	}
	
	if(find_room()->pora_roku() == MT_JESIEN)
	{
		opis += " pokryta jest ��to-br�zowymi li��mi, kt�re ju�"
				+" niebawem opadn� pozostawiaj�c ga��� zupe�nie nag�.";
	}
	
	if(find_room()->pora_roku() == MT_ZIMA)
	{
		opis += " jest pozbawiona jakichkolwiek li�ci, jedyn� jej"
				+" ozdob� s� grudki �niegu, kt�re przymarz�y do"
				+" g�adkiej kory drzewa.";
	}
	
	opis += "\n";
	
	return opis;
}
