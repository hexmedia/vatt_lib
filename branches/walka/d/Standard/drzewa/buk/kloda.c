inherit "/std/drzewo/kloda";

#include <materialy.h>
#include <object_types.h>

void create_kloda()
{
	ustaw_nazwe("kloda");
	dodaj_przym("kszta�tny", "kszta�tni");
	set_long("G�adka kora k�ody wygl�da jak aksamitna sk�ra,"
			+" kt�ra chroni delikatne wn�trze drzewa. Grubo��"
			+" pnia jest w ka�dym miejscu niemal taka sama,"
			+" dzi�ki czemu k�oda wygl�da jakby kto� w pocie"
			+" czo�a wypracowa� jej kszta�t.\n");
	
	ustaw_material(MATERIALY_DR_BUK, 100);
	set_type(O_DREWNO);
	
	setuid();
	seteuid(getuid());
}