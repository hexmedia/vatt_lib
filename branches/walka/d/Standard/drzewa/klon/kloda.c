inherit "/std/drzewo/kloda";

#include <stdproperties.h>
#include <object_types.h>

void create_kloda()
{
	ustaw_nazwe("kloda");
	dodaj_przym("ciemny", "ciemni");
	set_long("K�oda posiada do�� grub� kor� o nier�wnej powierzchni,"
			+" jakby przez lata rz�siste krople deszczu wyrze�bi�y na jej"
			+" powierzchni setki rys. Na obu jej ko�cach, w miejscach w" 
			+" kt�rych zosta�a ogo�ocona z kory, pojawi�y si� drobne kropelki"
			+" drzewnego soku.\n");

	set_type(O_DREWNO);
	
	setuid();
	seteuid(getuid());
}
