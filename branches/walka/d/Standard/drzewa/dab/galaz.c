inherit "/std/drzewo/galaz";

#include <materialy.h>
#include <mudtime.h>
#include <object_types.h>

void create_galaz()
{
	ustaw_nazwe("galaz");
	random_przym("prosty:pro�ci wykr�cony:wykr�ceni �ukowaty:�ukowaci"
		+" szponiasty:szponia�ci||omsza�y:omszali chropowaty:chropowaci"
		+" pomarszczony:pomarszczeni", 2);

	set_long("@@dlugi@@");

	ustaw_material(MATERIALY_DR_DAB, 100);
	set_type(O_DREWNO);
}

string dlugi()
{
	string opis = "";

	opis += "Ga��� pokryta jest pofa�dowan� kor� na kt�rej"
		+" mo�esz zauwa�y� liczne wg��bienia.";

	if(find_room()->pora_roku() == MT_WIOSNA)
	{
		opis += " Dostrzegasz na niej r�wnie� m�ode p�czki"
			+" z kt�rych zapewne za jaki� czas rozwin�"
			+" si� li�cie.";
	}

	if(find_room()->pora_roku() == MT_LATO)
        {
		opis += " Okryta jest spor� ilo�cia ciemnozielonych li�ci"
				+" o falistych brzegach.";
	}
	
	if(find_room()->pora_roku() == MT_JESIEN)
	{
		opis += " Poro�ni�ta jest li��mi o z�ocistobr�zowych barwach.";
	}
	
	if(find_room()->pora_roku() == MT_ZIMA)
	{
		opis += " Jest zupe�nie ogo�ocona z li�ci.";
	}
	
	opis += "\n";
	
	return opis;
}
