inherit "/std/ryby/ryba";

void
create_ryba()
{
    ustaw_nazwe("ryba");

    dodaj_przym("pomara�czowooki", "pomara�czowoocy");
	dodaj_przym("drobny", "drobni");

    set_unid_long("Jasnosrebrzysta ryba z szaro-czarnym grzbietem"
	   +" ma oczy niemal tak czerwone jak p�etwy na brzuchu."
	   +" Jej cia�o jest nieco wyd�u�one, w ca�o�ci okaz ten"
	   +" wygl�da raczej pospolicie.\n");

    set_id_long("Jasnosrebrzysta ryba z szaro-czarnym grzbietem"
	   +" ma oczy niemal tak czerwone jak p�etwy na brzuchu."
	   +" Jej cia�o jest nieco wyd�u�one, z �atwo�ci� rozpoznajesz,"
	   +" i� jest to pospolita p�otka. Fakt, �e jej mi�so jest bardzo"
	   +" smaczne oraz niema�a ilo�� w rzekach czyni j� cz�st�"
	   +" zdobycz� w�dkarzy. Niestety ma�o kiedy osi�ga ona wi�ksze"
	   +" rozmiary.\n");

    set_min_weight(50);
    set_avg_weight(600);
    set_max_weight(1500);

    set_mod_pory(({1.0, 0.5, 1.0, 1.0, 0.1}));
    set_przynety(({"chleb"}), ({"chleb", "robak"}));
    set_walecznosc(({0.05, 0.2}));
    set_rodzaj_brania(({"trykanie", "uderzenie"}));
    set_max_cena(31);
    set_rozpoznawalnosc(7);
}
