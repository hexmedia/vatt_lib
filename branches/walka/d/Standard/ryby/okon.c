inherit "/std/ryby/ryba";

void
create_ryba()
{
    ustaw_nazwe("ryba");

    dodaj_przym("pr�gowany", "pr�gowani");
    dodaj_przym("pomara�czowop�etwy", "pomara�czowop�etwi");

    set_unid_long("Pasiaste boki tej ryby w po��czeniu z pot�n�,"
		+" g�rn� p�etw� oraz masywnym pyskiem nadaj� jej"
	        +" drapie�ny wygl�d. Brzuch ryby zdobi� dwie "
		+" niewielkie, pomara�czowe p�etwy.\n");

    set_id_long("Pasiaste boki okonia w po��czeniu z pot�n�, g�rn�"
	   +" p�etw� oraz masywnym pyskiem nadaj� jej drapie�ny wygl�d"
	   +" i taka jest w istocie jego natura. Na brzuchu ryby zauwa�asz"
	   +" dwie niewielkie, pomara�czowe p�etwy. Mi�so okonia nale�y do"
	   +" najsmaczniejszych spo�r�d ryb popularnych w rzekach. Na"
	   +" odpowiedni� przyn�t� mo�na z�apa� nawet do�� spory okaz,"
	   +" lecz najcz�ciej gatunek ten nie osi�ga du�ych rozmiar�w.\n");

    set_min_weight(100);
    set_avg_weight(700);
    set_max_weight(2500);

    set_mod_pory(({1.0, 0.5, 0.5, 1.0, 0.5}));
    set_przynety(({"robak"}), ({"robak", "rybka"}));
    set_walecznosc(({0.18, 0.35}));
    set_rodzaj_brania(({"trykanie", "zgiecie"}));
    set_max_cena(40);
    set_rozpoznawalnosc(11);
}
