inherit "/std/ryby/ryba";

void
create_ryba()
{
    ustaw_nazwe("ryba");

    dodaj_przym("srebrzysty", "srebrzy�ci");
    dodaj_przym("l�ni�cy", "l�niacy");

    set_unid_long("Cia�o tej ryby jest wyd�u�one, sp�aszczone"
	   +" bocznie, pokryte delikatnymi �atwo odpadaj�cymi"
	   +" �uskami o intensywnym, srebrzystym ubarwieniu.\n");

    set_id_long("Cia�o tej ryby jest wyd�u�one, sp�aszczone"
	   +" bocznie, pokryte delikatnymi �atwo odpadaj�cymi"
	   +" �uskami o intensywnym, srebrzystym ubarwieniu."
	   +" Ukleja �yje zazwyczaj gromadnie w strefie przybrze�nej"
	   +" jezior lub w rzekach, �ywi si� planktonem oraz owadami"
	   +" i ich larwami. Jest niewielkich rozmiar�w i nie"
	   +" przekracza p� �okcia.\n");

    set_min_weight(10);
    set_avg_weight(200);
    set_max_weight(500);

    set_mod_pory(({0.5, 1.0, 1.0, 1.0, 0.1}));
    set_przynety(({"chleb"}), ({"chleb"}));
    set_walecznosc(({0.01, 0.05}));
    set_rodzaj_brania(({"trykanie", "trykanie"}));
    set_max_cena(7);
    set_rozpoznawalnosc(5);
}
