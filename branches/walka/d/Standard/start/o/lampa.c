inherit "/std/object";

#include <pl.h>

int dzin;
int czekaj;
object ob;

void
create_object()
{
    ustaw_nazwe( ({"lampa", "lampy", "lampie", "lampe", "lampa", "lampie"}),
	({"lampy", "lamp", "lampom", "lampy", "lampami", "lampach"}), PL_ZENSKI);
    dodaj_przym("stary", "starzy");
    set_long("Jest stara i zardzewiala, ale gdybys ja dobrze potarl, " +
	    "na pewno odzyskalaby dawny polysk.\n");
    dzin = 1;
    czekaj = 0;
}


void
init()
{
    ::init();
    add_action("potrzyj","potrzyj");
}

void
wyskocz()
{
    ob = clone_object("/d/Standard/start/n/dzin");
    ob->move(environment(this_player()));
    czekaj = 0;
    write("Nagle, ze starej lampy wyskakuje ogromny dzin!\n");
}

int
potrzyj(string str)
{
    if (str!="lampe")
    {
	notify_fail("Potrzyj co?\n");
	return 0;
    }
    if (dzin)
    {
        write("Ostroznie pocierasz lampe.\n");
	set_alarm(6.0, 0.0, &wyskocz());
	dzin = 0; // W lampie jest tylko jeden dzin :)
	czekaj = 1;
    }
    else
    {
	if (czekaj) {
	    write("Ostroznie pocierasz lampe. Czujesz, ze w srodku cos sie poruszylo.\n");
	} else {
	    write("Ostroznie pocierasz lampe, ale nic sie nie dzieje.\n");    
	}
    }    
    return 1;
}
