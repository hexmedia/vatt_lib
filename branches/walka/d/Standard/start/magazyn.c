inherit "/std/room";
inherit "/lib/store_support";

object ob;

void
create_room()
{
    set_short("Magazyn");
    set_long("Jestes w magazynie na tylach sklepu. Pomieszczenie jest "+
	    "wypelnione skrzyniami i workami."+
	    ""+
	    ""+
	    ""+
	    ""+
	    "\n");

    add_exit("/d/Standard/start/sklep", "zachod");
    
    ob = clone_object("/std/armour");
    ob->set_value(1000);
    ob->set_ac(100);
    ob->move(this_object());
    ob = clone_object("/std/torch");
    ob->move(this_object());
    ob = clone_object("/d/Standard/start/o/lampa");
    ob->move(this_object());
}

public void
enter_inv(object ob, object from)
{
    ::enter_inv(ob, from);
    store_update(ob);
}
