inherit "/std/room";
inherit "/lib/guild_support";
inherit "/lib/skill_raise";

void
create_room()
{
    set_short("Gildia podroznikow");
    set_long("Jestes w gildii podroznikow. Mozesz tu sprawdzic swoje cechy, "+
	    "a takze wytrenowac nowe umiejetnosci."+
	    ""+
	    ""+
	    ""+
	    ""+
	    "\n");

    add_exit("/d/Standard/start/plac", "polnocny-zachod");
}

void
init()
{
    ::init();
    init_guild_support();
    init_skill_raise();
}
