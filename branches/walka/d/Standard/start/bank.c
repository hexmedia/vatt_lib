inherit "/std/room";
inherit "/lib/bank";

void
create_room()
{
    set_short("Bank");
    set_long("Znajdujesz sie w banku. Mozesz tu wymienic albo zdenominowac "+
	    "swoje pieniadze."+
	    ""+
	    ""+
	    ""+
	    ""+
	    "\n");

    config_default_trade();
    config_trade_data();
    add_exit("/d/Standard/start/plac", "poludniowy-wschod");
}

void
init()
{
    ::init();
    bank_init();
}
