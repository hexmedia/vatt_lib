#include <macros.h>

inherit "/std/room";

object ob;

void
create_room()
{
    set_short("Duzy plac");
    set_long("Jestes w centrum duzego placu, ktory jak na razie jest centrum "+
	    "tego niewielkiego swiata. Na poludniu dostrzegasz budynek swiatyni."+
	    ""+
	    ""+
	    ""+
	    ""+
	    "\n");

    ob = clone_object("/std/board");
    ob->move(this_object());
    ob->set_board_name("/d/Standard/start/notki");

    add_exit("/d/Standard/start/sklep", "wschod");
    add_exit("/d/Standard/start/church", "poludnie");
    add_exit("/d/Standard/start/gildia", "poludniowy-wschod");
    add_exit("/d/Standard/start/karczma", "poludniowy-zachod");
    add_exit("/d/Standard/start/bank", "polnocny-zachod");
    add_exit("/d/Standard/start/zajazd/wej1", "polnocny-wschod");
    if (LOAD_ERR("/d/Standard/start/pustynia/pustynia"))
    {
	write("Blad przy ladowaniu mapy.\n");
	return;
    }
    add_exit("","polnoc","@@enter_map:/d/Standard/start/pustynia/pustynia|X@@");
}

