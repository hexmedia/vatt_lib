/* Autor: Avard
   Data : 24.04.07
   Opis : Sniegulak */

inherit "/std/room";

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <pogoda.h>
#include "dir.h"


void create_room() 
{
    set_short("Pustka");
    add_object(TRETOGOR_DRZWI + "brama_poludniowa_z");
}

public string
exits_description() 
{
    return "Tu nie ma wyj��!\n";
}

string
dlugi_opis()
{
    string str;
    str = "Tu nic nie ma, nie powinno ci� tu by�.\n";
    return str;
}