/*
 * do wioski ko^lo BM
 * opis by Vortak lub  kogo^s ze ^swity jego
 * zepsu^la faeve
 * dn. 18.05.2007
 *
 */

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <pl.h>
#include <object_types.h>
#include <macros.h>
#include <materialy.h>

void
create_armour()
{ 

    ustaw_nazwe("plaszcz");
    dodaj_przym("czerwony","czerwoni");
    dodaj_przym("sk^orzany", "sk^orzani");
    set_long("P^laszcz zosta^l wykrojony z kilku kawa^lk^ow niewprawnie "+
		"garbowanej sk^ory. Nier^owne szwy wykonane z grubych, sztywnych "+
		"nici nadaj^a temu elementowi garderoby niezbyt wyszukany wygl^ad, a "+
		"i korzystanie z niego czyni^a niezbyt przyjemnym. Powierzchnia "+
		"sk^ory zosta^la pokryta czerwonym barwnikiem. W kilku miejscach "+
		"wszyto kr^otkie kawa^lki sznurka umo^zliwiaj^ace zapi^ecie "+
		"p^laszcza. \n");



    ustaw_material(MATERIALY_TK_MULINA, 5);
    ustaw_material(MATERIALY_SK_BYDLE, 95);
    set_type(O_UBRANIA);
    set_slots(A_CLOAK);
    add_prop(OBJ_I_WEIGHT, 2000);
    add_prop(OBJ_I_VOLUME, 1200);
    add_prop(OBJ_I_VALUE, 46);
    set_size("M");
}