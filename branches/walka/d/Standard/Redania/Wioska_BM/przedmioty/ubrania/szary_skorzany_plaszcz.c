/*
 * do wioski ko^lo BM
 * opis by Vortak lub  kogo^s ze ^swity jego
 * zepsu^la faeve
 * dn. 18.05.2007
 *
 */

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <pl.h>
#include <object_types.h>
#include <macros.h>
#include <materialy.h>

void
create_armour()
{ 

    ustaw_nazwe("plaszcz");
    dodaj_przym("szary","szarzy");
    dodaj_przym("sk^orzany", "sk^orzani");
    set_long("Wykonany z grubej, dok^ladnie wygarbowanej sk^ory p^laszcz "+
		"stanowi doskona^le okrycie w ch^lodne, jesienne dni, a i w zimowe "+ 
		"mrozy mo^ze okaza^c si^e nieoceniony. Wysoki ko^lnierz obszyty jest "+
		"bia^lym, jak^ze mi^lym w dotyku borsuczym futrem. Odzienie mo^zna "+
		"zapi^a^c dzi^eki umocowanym wzd^lu^z ca^lej jego d^lugo^sci spinkom. "+
		"Mimo braku jakichkolwiek ozd^ob czy haft^ow ubranie sprawia wra^zenie "+
		"eleganckiego i do^s^c wytrzyma^lego, a co najwa^zniejsze "+
		"funkcjonalnego. \n");


    ustaw_material(MATERIALY_RZ_STAL , 5);
    ustaw_material(MATERIALY_SK_BYDLE, 95);
    set_type(O_UBRANIA);
    set_slots(A_CLOAK);
    add_prop(OBJ_I_WEIGHT, 2102);
    add_prop(OBJ_I_VOLUME, 1190);
    add_prop(OBJ_I_VALUE, 66);
    set_size("M");
}