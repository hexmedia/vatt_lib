/*
 * do wioski ko^lo BM
 * opis by Vortak lub  kogo^s ze ^swity jego
 * zepsu^la faeve
 * dn. 18.05.2007
 *
 */

#include <stdproperties.h>
#include <macros.h>
#include <materialy.h>
#include <object_types.h>

inherit "/std/armour.c";

void
create_armour()
{
    ustaw_nazwe("koszula");
    
    dodaj_przym("po^latany", "po^latani");
    dodaj_przym("bury", "burzy");
    
    set_long("Niegdy^s zapewne porz^adna koszula lata ^swietno^sci ma ju^z "+
		"za sob^a. Bardziej w^la^sciwym by^lo by przerobienie jej na szmat^e "+
		"kuchenn^a, jednak w desperacji zawsze mo^zna za^lo^zy^c j^a na "+
		"kark. Sztywny len, z kt^orego j^a wykonano powinien da^c jeszcze "+
		"nieco ciep^la, zw^laszcza ^ze liczne ^laty pokrywaj^a wi^ekszo^s^c "+
		"dziur, jakie z biegiem lat powsta^ly w odzieniu. Niegdy^s na piersi "+
		"naszyta by^la kiesze^n, teraz przypomina o niej jedynie sm^etnie "+
		"zwisaj^acy kawa^lek materia^lu.\n");

    ustaw_material(MATERIALY_TK_LEN);
        
    add_prop(OBJ_I_VOLUME, 500);
    add_prop(OBJ_I_WEIGHT, 150);
    add_prop(OBJ_I_VALUE, 7);
    
    set_slots(A_TORSO, A_FOREARMS, A_ARMS); 
    set_type(O_UBRANIA);
    add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    set_size("M");
}