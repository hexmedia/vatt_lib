/*
 * do wioski ko^lo BM
 * opis by Vortak lub  kogo^s ze ^swity jego
 * zepsu^la faeve
 * dn. 18.05.2007
 *
 */

inherit "/std/armour";

#include <wa_types.h>
#include <formulas.h>
#include <stdproperties.h>
#include <macros.h>
#include <object_types.h>
#include <materialy.h>

void
create_armour()
{
    ustaw_nazwe("spodnie");
    ustaw_nazwe_glowna("para");

    dodaj_przym("szary", "szarzy");
    dodaj_przym("lniany", "lniani");

    set_long("Ju^z na pierwszy rzut oka mo^zna oceni^c, ^ze uszyte z "+
		"grubego, szorstkiego lnu spodnie nie s^a dzie^lem mistrza "+
		"krawieckiego. Jednak nie mo^zna tak^ze powiedzie^c, by ich tw^orca "+
		"kompletnie spartaczy^l robot^e. Napewno doskonale spe^lniaj^a sw^a "+
		"rol^e, dodatkowo s^a wygodne i wytrzyma^le a pojemne kieszenie "+
		"umo^zliwiaj^a ukrycie w nich niezb^ednych przedmiot^ow.\n");
    
	set_ac(A_THIGHS | A_HIPS | A_STOMACH, 2, 2, 2);
    ustaw_material(MATERIALY_LEN);
    set_type(O_UBRANIA);
    add_prop(OBJ_I_VOLUME, 1000);
    add_prop(OBJ_I_WEIGHT, 509);
    add_prop(OBJ_I_VALUE, 13);
    add_prop(ARMOUR_I_D_ROZCIAGLIWOSC, 30);
    add_prop(ARMOUR_I_U_ROZCIAGLIWOSC, 30);
    add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    set_size("M");

	add_item("kieszenie","S^a to dwie ca^lkiem pojemne kieszenie, "+
		"znajduj^ace si^e na wysoko^sci bioder, mog^ace pomie^sci^c kilka "+
		"przydatnych drobiazg^ow.\n");
	add_subloc(({"pierwsza kiesze^n", "pierwszej kieszeni", 
		"pierwszej kieszeni", "pierwsz^a kiesze^n", "pierwsz^a kieszeni^a",
"pierwszej kieszeni"}));
    add_subloc_prop("pierwsza kiesze^n", SUBLOC_S_OB_GDZIE, "wewn^atrz pierwszej kieszeni");
    add_subloc_prop("pierwsza kiesze^n", SUBLOC_I_OB_PRZYP, PL_DOP);
    add_item("kiesze^n w", "Jest to pojemna kiesze^n" +
        "@@opis_sublokacji| zawieraj^aca |kiesze^n|.|.|" + PL_BIE + "@@\n",
PL_MIE);

	add_subloc(({"druga kiesze^n", "drugiej kieszeni", 
		"drugiej kieszeni", "drug^a kiesze^n", "druga^a kieszeni^a",
"drugiej kieszeni"}));
    add_subloc_prop("druga kiesze^n", SUBLOC_S_OB_GDZIE, "wewn^atrz pierwszej kieszeni");
    add_subloc_prop("druga kiesze^n", SUBLOC_I_OB_PRZYP, PL_DOP);
    add_item("kiesze^n w", "Jest to pojemna kiesze^n" +
        "@@opis_sublokacji| zawieraj^aca |kiesze^n|.|.|" + PL_BIE + "@@\n",
PL_MIE);
}