inherit "/std/container.c";

#include <pl.h>
#include <stdproperties.h>
#include <macros.h>
#include <filter_funs.h>
#include <object_types.h>
#include <materialy.h>
#include <mudtime.h>
#include <composite.h>
#include <sit.h>
#include "dir.h"

void
create_container()
{
    ustaw_nazwe("biurko");
    dodaj_przym("masywny", "masywni");
    dodaj_przym("elegancki", "eleganccy");
    
    set_long("@@dlugi_opis@@");

    make_me_sitable("na", "na masywnym eleganckim biurku",
        "z masywnego eleganckiego biurka", 1, PL_MIE, "na");

    ustaw_material(MATERIALY_DR_MAHON);
    
    add_prop(OBJ_I_WEIGHT, 80000);
    add_prop(OBJ_I_VOLUME, 130000);
    add_prop(CONT_I_MAX_VOLUME, 300000);
    add_prop(CONT_I_CANT_WLOZ_DO, 1);
    add_prop(CONT_I_MAX_WEIGHT, 100000);
    add_prop(CONT_I_DONT_SHOW_CONTENTS, 1);
    add_prop(OBJ_M_NO_GET, "Biurko jest zbyt du^ze.\n");

    add_subloc("na");
    
    set_owners(({WIOSKA_NPC + "soltys"}));
}

public int
query_type()
{
    return O_MEBLE;
}

string
dlugi_opis()
{
    string str = "Wykonany z ciemnego drewna mebel sprawia wra^zenie " +
    "solidnego i wytrzyma^lego. Rogi d^lugiego blisko na trzy ^lokcie " +
    "blatu wzmocnione s^a ^zelaznymi, rze^xbionymi w motywy ro^slinne " +
    "okuciami. ";//Pod nim za^s umieszczone s^a dwie szuflady, kt�re mo�na wysun�c b�d� wsun�� dzi�ki masywnym o�owianym uchwytom. ";

    switch (sizeof(all_inventory(this_object())))
    {
        case 0:
            str += "\n";
            break;
            
        case 1:
            str += "Le^zy na nim " +
            this_object()->subloc_items("na")[..-1] + ".\n";
            break;
            
        default:
            str += "Le^z^a na nim " +
            this_object()->subloc_items("na")[..-1] + ".\n";
            break;
    }
    
    return str;
}
