/* Jajo do kurnika wioski przy BM.
   autorzy: gjoef i vera ;)
 */

#include <macros.h>
#include <stdproperties.h>
#include "dir.h"

inherit "/lib/keep";
inherit "/std/object";

int Zbuk = 86400;

void
create_object()
{
    ustaw_nazwe("jajko");
    
    dodaj_przym("kurzy", "kurzy");
    
    set_long("Prawdziwe kurze jajko prosto z wiejskiej hodowli.\n");
    
    add_prop(OBJ_I_WEIGHT, 54 + random(12));
    add_prop(OBJ_I_VOLUME, query_prop(OBJ_I_WEIGHT) + 10);
    
    set_keep(1);
}

void
zbuk()
{
    Zbuk -= 10;
}

void
set_zbuk(int i)
{
    Zbuk = i;
}

int
query_zbuk()
{
    return Zbuk;
}

void
enter_env(object dest, object old)
{
	if (Zbuk < 86400 || (interactive(dest) && random(2)))
        set_alarm(10.0, 10.0, "zbuk");
}

void
grrr(object dest,object player)
{
        player->catch_msg("Starasz si^e wyj^a^c kurze jajko " +
        "z " + QSHORT(dest, 1) + ", jednak nieszcz^e^sliwie p^ek^a ci " +
        "w d^loni, zalepiaj^ac wszystko g^est^a mazi^a.\n");
        saybb(QCIMIE(player, 0) + " stara si^e wyj^a^c kurze jajko " +
        "z " + QSHORT(dest, 1) + ", jednak nieszcz^e^sliwie p^ek^a " +
        player->koncowka("mu", "jej") + " w d^loni, zalepiaj^ac wszystko g^est^a mazi^a.\n");
        
        //remove_object();
        destruct();
        return;

}

void
leave_env(object dest, object old)
{
       
    if (dest->query_name() ~= "plecak" ||
        dest->query_name() ~= "worek" ||
        dest->query_name() ~= "woreczek" ||
        dest->query_name() ~= "sakwa" ||
        dest->query_name() ~= "sakiewka" ||
        dest->query_name() ~= "torebka" ||
        dest->query_name() ~= "torba")
    {
		set_alarm(0.1,0.0,"grrr",dest,TP);
    }
        

}


void
throw_effect(object cel)
{
//write("\n234\n");
	if(cel->query_prop(ROOM_I_IS))
	{
		if(ENV(ENV(TO))->query_prop(ROOM_I_TYPE) != ROOM_IN_WATER ||
			ENV(ENV(TO))->query_prop(ROOM_I_TYPE) != ROOM_UNDER_WATER ||
			ENV(ENV(TO))->query_prop(ROOM_I_TYPE) != ROOM_IN_AIR ||
			ENV(ENV(TO))->query_prop(ROOM_I_TYPE) != ROOM_SWAMP)
		{
			if(Zbuk)
				tell_room(cel,"Kurze jajko rozbija si� z plaskiem.\n");
			else
				tell_room(cel,"Kurze jajko rozbija si� z plaskiem, "+
				"roztaczaj�c dooko�a straszny od�r.\n");

    		destruct();
		}
	}
	else
	{
	    if (Zbuk)
	    {
        saybb("Kurze jajko trafia w " + QIMIE(cel, 3) +
		 ", a lepka ma^x znajduj" +
        "^aca si^e w ^srodku powoli si^e po ni" + cel->koncowka("m", "ej","im") +
        " rozp^lywa.\n", ({cel,TP}));
		TP->catch_msg("Kurze jajko trafia w " + QIMIE(cel, 3) +
		 ", a lepka ma^x znajduj" +
        "^aca si^e w ^srodku powoli si^e po ni" + cel->koncowka("m", "ej","im") +
        " rozp^lywa.\n");
        cel->catch_msg("Kurze jajko trafia ci^e, a lepka ma^x znajduj" +
        "^aca si^e w ^srodku powoli si^e rozp^lywa si^e po twoim ubraniu.\n");
    	}
    	else
    	{
        saybb("Kurze jajko trafia w " + QIMIE(cel, 3) + ", a mocno ju^z zepsuta " +
        "lepka ma^x znajduj^aca si^e w ^srodku powoli si^e po ni" +
		 cel->koncowka("m", "ej","im") +
        " rozp^lywa, roztaczaj^ac dooko^la straszny od^or.\n", ({cel,TP}));
		TP->catch_msg("Kurze jajko trafia w " + QIMIE(cel, 3) + ", a mocno ju^z "+
		"zepsuta " +
        "lepka ma^x znajduj^aca si^e w ^srodku powoli si^e po ni" + 
		cel->koncowka("m", "ej","im") +
        " rozp^lywa, roztaczaj^ac dooko^la straszny od^or.\n");
        cel->catch_msg("Kurze jajko trafia ci^e, a mocno ju^z zepsuta lepka "+
		"ma^x znajduj" +
        "^aca si^e w ^srodku powoli si^e rozp^lywa si^e po twoim ubraniu, " +
        "roztaczaj^ac dooko^la straszny od^or.\n");
    	}

    	destruct();
	}
}
/*
string
query_jajko_auto_load()
{
    return "#JAJKO#" + Zbuk + "#JAJKO#";
}


public void
init_jajko_arg(string arg)
{
    string reszt;
    string zbuk;

    sscanf(arg, "%s#JAJKO#%s##%s#JAJKO#%s", reszt, zbuk, reszt);

    set_zbuk(str2val(zbuk));
}

public string
query_auto_load()
{
    return ::query_auto_load() + query_jajko_auto_load();
}

public void
init_arg(string arg)
{
    ::init_arg(arg);
    init_jajko_arg(arg);
} */
