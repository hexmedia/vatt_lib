//opis zucha vortaka

#include <stdproperties.h>
#include <materialy.h>
#include <object_types.h>
#include "dir.h"

inherit "/std/object";

void
create_object()
{
    ustaw_nazwe("^laweczka");
    dodaj_przym("ma^ly", "mali");
    dodaj_przym("ko^slawy", "ko^slawi");
    ustaw_material(MATERIALY_DR_SOSNA);
    set_type(O_MEBLE);
    set_long("Kilka drewnianych bali po^l^aczonych ze sob^a d^lugimi, " +
    "zardzewia^lymi gwo^xdziami. Dok^ladnie oheblowane, mo^zna na nich " +
    "usi^a^s^c bez ryzyka, ^ze jaka^s drzazga bole^snie ci^e uk^luje.\n");

    add_prop(OBJ_I_WEIGHT, 96300);
    add_prop(OBJ_I_VOLUME, 106110);
    add_prop(OBJ_I_VALUE, 70);
    
    add_prop(OBJ_M_NO_GET, "Przez chwil^e mocujesz si^e z ma^l^a ko^slaw^a " +
    "^laweczk^a, jednak jest ona solidnie wkopana w ziemi^e i nie chce si^e " +
    "ruszy^c nawet na cal.\n");

    make_me_sitable("na","na ma^lej ko^slawej ^laweczce","z ma^lej ko^slawej ^laweczki",3);
}
