#include <macros.h>
#include <stdproperties.h>
#include <materialy.h>
#include <object_types.h>
#include "dir.h"

inherit "/std/container";

nomask void
create_container()
{
    ustaw_nazwe("kociolek");
    dodaj_przym("ma^ly", "mali");
    dodaj_przym("blaszany", "blaszani");
    
    set_long("Kawa^lek do^s^c grubej, niechlujnie wyprofilowanej blachy, " +
    "rozszerzaj^acy si^e przy dnie zaopatrzony zosta^l po bokach w dwoje " +
    "uszu, do kt^orych przymocowano mocny, gruby rzemie^n stanowi^acy w " +
    "miar^e solidny uchwyt. Kocio^lek wype^lniony jest resztkami starej, " +
    "gnij^acej strawy wydzielaj^acej ohydny od^or. @@opis_sublokacji|W ^srodku ||" +
    ".||3|wida^c |wida^c |wida^c @@\n");
    
    add_prop(OBJ_I_WEIGHT, 1500);
    add_prop(CONT_I_MAX_WEIGHT, 12000);
    add_prop(OBJ_I_VOLUME, 4000);
    add_prop(CONT_I_MAX_VOLUME, 8000);
    add_prop(OBJ_I_VALUE, 200);
    
 /* add_subloc("w");
    add_subloc_prop("w", CONT_I_MAX_WEIGHT, 500);
    add_subloc_prop("w", CONT_I_MAX_VOLUME, 500);
    add_subloc_prop("w", SUBLOC_I_MOZNA_POLOZ, 0);
    add_subloc("w");
    add_prop(CONT_I_CANT_WLOZ_DO, 0); */
    add_prop(CONT_I_DONT_SHOW_CONTENTS, 1);
    ustaw_material(MATERIALY_MOSIADZ);
    
    set_type(O_KUCHENNE);
    
    setuid();
    seteuid(getuid());
}
