#include <macros.h>
#include <stdproperties.h>
#include <object_types.h>
#include <materialy.h>
#include "dir.h"

inherit "/std/container";

create_container()
{
    ustaw_nazwe("stol");
    dodaj_przym("d^lugi", "d^ludzy");
    dodaj_przym("drewniany", "drewniani");
    
    set_long("D^lugi, mog^acy pomie^sci^c przy sobie niejednego " +
    "strudzonego w^edrowca st^o^l wykonany jest ze zbitych ze sob^a " +
    "drewnianych bali ustawionych na masywnych nogach. @@opis_sublokacji|Na jego blacie |na|" +
    ".||0|le^zy |le^z^a |le^zy @@\n");
    
    add_subloc("na");
    add_subloc_prop("na", CONT_I_MAX_WEIGHT, 500000);
    add_subloc_prop("na", CONT_I_MAX_VOLUME, 500000);
    add_subloc_prop("na", SUBLOC_I_MOZNA_POLOZ, 0);

    add_prop(CONT_I_CANT_WLOZ_DO, 1);
    add_prop(OBJ_I_WEIGHT, 250000);
    add_prop(OBJ_I_VOLUME, 267000);
    add_prop(OBJ_M_NO_GET, "St^o^l jest zbyt ci^e^zki.\n");
    add_prop(OBJ_I_VALUE, 1000);
    ustaw_material(MATERIALY_DR_SWIERK);
    
    set_owners(({WIOSKA_NPC + "karczmarz"}));
    
    set_type(O_MEBLE);
    
    add_prop(OBJ_I_DONT_INV, 1);
    add_prop(OBJ_I_DONT_GLANCE, 1);
}
