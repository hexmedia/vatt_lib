#include <macros.h>
#include <stdproperties.h>
#include <materialy.h>
#include <object_types.h>
#include "dir.h"

inherit "/std/object";

void
create_object()
{
     ustaw_nazwe("szafa");
     
     dodaj_przym("wysoki", "wysocy");
     dodaj_przym("dwudrzwiowy", "dwudrzwiowi");
     
     set_long("Wysoka i solidna dwudrzwiowa szafa. W drzwiach wida^c " +
     "niewielki otw^or, zapewne jaki^s sprytny mechanizm umo^zliwiaj^acy " +
     "uchronienie zawarto^sci przed niepo^z^adanymi osobami.\n");

     add_prop(OBJ_I_WEIGHT, 400000);
     add_prop(OBJ_I_VOLUME, 700000);
     add_prop(OBJ_I_VALUE, 2400);
     add_prop(OBJ_M_NO_GET, "Szafa jest zbyt ci^e^zka.\n");
     
     ustaw_material(MATERIALY_DR_SWIERK);
     
     set_owners(({WIOSKA_NPC + "soltys"}));
}
