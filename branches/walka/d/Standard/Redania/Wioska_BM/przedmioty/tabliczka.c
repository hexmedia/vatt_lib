#include <macros.h>
#include <stdproperties.h>
#include "dir.h"

inherit "/std/object";

string
dlugi()
{
    string str = "+-------------------------------------+\n";            //?
          str += "|           -~%JAD^LOSPIS%~-           |\n";
          str += "|                                     |\n";
          str += "|  Groch^owka                4 grosze  |\n";
          str += "|  ^Zur                      6 groszy  |\n";
          str += "|  Bigos                    6 groszy  |\n";
          str += "|                                     |\n";
          str += "|  Mleko                    2 grosze  |\n";
          str += "|  Kompot                   2 grosze  |\n";
          str += "|  Piwo                     8 groszy  |\n";
          str += "|  Piwo z wk^ladk^a          10 groszy  |\n";
          str += "|  W^odka                   10 groszy  |\n";
          str += "|  ^Sliwowica               12 groszy  |\n";
          str += "|  Specjalno^s^c zak^ladu     15 groszy  |\n";
          str += "|                                     |\n";
          str += "|             -~%o~0-o%~-        -~%^O |\n";
          str += "+-------------------------------------+\n";
    
    return str;
}

create_object()
{
    ustaw_nazwe("tabliczka");
    dodaj_nazwy("menu");
    
    set_long("@@dlugi@@");
    
    add_prop(OBJ_I_DONT_GLANCE, 1);
    add_prop(OBJ_M_NO_GET, "Tabliczka wisi zbyt daleko od ciebie.\n");
    
    add_prop(OBJ_I_WEIGHT, 800);
    add_prop(OBJ_I_VOLUME, 760);
    
    set_owners(({WIOSKA_NPC + "karczmarz"}));
}
