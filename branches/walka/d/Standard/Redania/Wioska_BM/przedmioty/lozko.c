#include <macros.h>
#include <stdproperties.h>
#include <materialy.h>
#include <object_types.h>
#include "dir.h"

inherit "/std/object";

string
dlugi()
{
    string str = "Solidny, drewniany mebel. Zbity z d^lugich, prostych desek " +
    "stela^z utrzymuje przykryty pledem siennik. Masywne nogi, na kt^orych " +
    "stoi ca^la konstrukcja gwarantuj^a, ^ze nie tylko chudzielec mo^ze " +
    "spokojnie po^lo^zy^c si^e na tym ^l^o^zku. Pod stela^zem umieszczono " +
    "skrzyni^e na po^sciel, kt^or^a dzi^eki specjalnym prowadnicom mo^zna " +
    "wysuwa^c i wsuwa^c pod ^l^o^zko. W tej chwili jest w";
    
    if (present("skrzynia", environment(TO))->query_prop("wysunieta"))
        str += "y";
        
    str += "suni^eta.\n";
    
    return str;
}

create_object()
{
    ustaw_nazwe("^l^o^zko");
    dodaj_przym("prosty", "pro^sci");
    dodaj_przym("masywny", "masywni");
    
    set_long("@@dlugi@@");

    add_prop(OBJ_M_NO_GET, "^L^o^zko jest zbyt ci^e^zkie.\n");
    add_prop(OBJ_I_WEIGHT, 200000);
    add_prop(OBJ_I_VOLUME, 250000);
    add_prop(OBJ_I_VALUE, 1300);
    
    set_type(O_MEBLE);
    
    set_owners(({WIOSKA_NPC + "soltys"}));
    
    ustaw_material(MATERIALY_DR_JESION, 40);
    
    make_me_sitable("na","na prostym masywnym ^l^o^zku","z prostego masywnego ^l^o^zka",6);
}
