/* Autor: Duana
  Opis : Vortak
  Data : 30.05.2007
*/

#include <macros.h>
#include <stdproperties.h>
#include <money.h>
#include <pl.h>
#include <formulas.h>
#include <std.h>
#include <ss_types.h>
#include <wa_types.h>
#include <composite.h>
#include <filter_funs.h>
#include <pogoda.h>
#include "dir.h"

inherit WIOSKA_NPC_STD;

int czy_walka();
int walka = 0;

void
create_wioska_monster()
{
    set_living_name("mlodzieniec_wioska");

    dodaj_nazwy("m^lodzieniec");

    ustaw_odmiane_rasy(PL_MEZCZYZNA);
    set_gender(G_MALE);

    set_long("Poci^ag^la, blada twarz, podkr^a^zone oczy i czerwony, "+
        "opuchni^ety nos dobitnie ^swiadcz^a, ^ze ten cz^lowiek wiecznie "+
        "cierpi na uporczywe przezi^ebienia. Obwis^le ramiona i zapad^la "+
        "klatka piersiowa zakryte s^a po^latan^a koszulin^a. D^lonie, widocznie "+
        "nieprzywyk^le s^a do ci^e^zkiej pracy w polu, nogi za^s chudziutkie i "+
        "jakby powykr^ecane jak^a^s chorob^a.\n");

        dodaj_przym("m^lody","m^lodzi");
        dodaj_przym("szczup^ly","szczupli");

    set_stats(({ 40, 45, 32, 40, 40, 35}));
    set_skill(SS_ANI_HANDL, 45);
    set_skill(SS_TRACKING, 35);
    set_skill(SS_AWARENESS, 35);

    add_prop(CONT_I_WEIGHT, 78000);
    add_prop(CONT_I_HEIGHT, 178);
    add_prop(LIVE_I_NEVERKNOWN, 1);

    set_act_time(30);
    add_act("pociagnij nosem smutno");
    add_act("emote zatyka nos palcami, a nast^epnie stara si^e wysmarka^c na "+
        "ziemi^e. Jako ^ze niezbyt mu si^e to uda^lo, wyciera nos w r^ekaw "+
        "koszuli.");
    add_act("powiedz :sapi^ac ci^e^zko: A ostrzegali tatko, coby si^e nie "+
        "przem^ecza^c...");
    add_act("mrugnij tepo");
    add_act("kaszlnij");
    set_random_move(20);
    
    add_armour(WIOSKA_UBRANIA + "szara_lniana_koszula.c");
    add_armour(WIOSKA_UBRANIA + "stare_powycieranie_spodnie.c");
    if (TEMPERATURA(ENV(TO)) < 10) add_armour(WIOSKA_UBRANIA + "szary_skorzany_plaszcz");

    set_default_answer(VBFC_ME("default_answer"));

    set_alarm_every_hour("co_godzine");
    set_alarm(1.0, 0.0, "co_godzine");
    set_reakcja_na_walke(0);
}


void
powiedz_gdy_jest(object player, string tekst)
{
    if (environment(this_object()) == environment(player))
        this_object()->command(tekst);
}
string
default_answer()
{
        set_alarm(1.0, 0.0, "powiedz_gdy_jest", this_player(),
            "powiedz do " + OB_NAME(TP) + " A czego mi g^low^e zawraca... Nie widzi, ^ze chory jestem?");
    return "";
}


void
oddajemy(object ob, object old)
{
    if (interactive(old) && (ENV(old) == ENV(TO))) {
        command_present(old, "powiedz do " + OB_NAME(old) + " Nie interesuj^a mnie takie rzeczy.");

        command_present(old, "daj " + OB_NAME(ob) + " " + OB_NAME(old));
        command("odloz " + OB_NAME(ob));
        return;
    }

}

void
enter_inv(object ob, object from)
{
    if (from == TP)
        set_alarm(1.0, 0.0, "oddajemy", ob, from);

    ::enter_inv(ob, from);
}

void
add_introduced(string imie, string biernik)
{
    set_alarm(1.0, 0.0, "command", "pokiwaj lekko");
    set_alarm(2.5, 0.0, "powiedz_gdy_jest", "powiedz do " + OB_NAME(TP) + " No... mi^lo mi.");
}

void
emote_hook(string emote, object wykonujacy)
{
    switch (emote)
    {
        case "kopnij": set_alarm(1.5, 0.0, "nienajlepszy", wykonujacy);
                    break;
        case "spoliczkuj": set_alarm(1.5, 0.0, "nienajlepszy", wykonujacy);
                    break;
        case "opluj" : set_alarm(2.5, 0.0, "nienajlepszy", wykonujacy);
                    break;
        case "poca^luj":
        case "przytul":
        case "pog^laszcz":

              {
                switch(random(2))
                 {
                 case 0:command("powiedz Id^x^ze mi st^ad! Jeszcze mnie "+
                     "czym zarazi! ");
                        break;
                 case 1:command("powiedz To^z to nie przystoi!");
                        break;
                 }
              }
    }
}
void
nienajlepszy(object kto)
{
   switch(random(2))
   {
      case 0: command("powiedz :rogl^adaj^ac si^e nerwowo: Czego chceta od "+
          "chorego cz^lowieka...");
              break;
      case 1: command("powiedz :podskakuj^ac tch^orzliwie: Ostawcie mnie...");
              break;

   }
} 

void attacked_by(object wrog)
{
    if(!walka)
    {
        walka = 1;
        set_alarm(0.5,0.0,"command","krzyknij Ratujta, ludzie, ratujta!");
        set_alarm(15.0, 0.0, "czy_walka", 1);
        return ::attacked_by(wrog);
    }
}
int
czy_walka()
{
    if(!query_attack() && walka)
    {
        set_alarm(0.5,0.0,"command","powiedz ^Zeby to by^lo ostatni raz!");
        walka = 0;
        return 1;
    }
    else
    {
        set_alarm(15.0, 0.0, "czy_walka", 1);
    }
}

void
koniec()
{
    if(query_attack())
    {
        set_alarm(10.0,0.0,"koniec");
        return;
    }
    else
    {
        int k = 0;
        mixed droga2=znajdz_sciezke(environment(this_object()),
            WIOSKA_LOKACJE + "wioska9");
		if(file_name(ENV(TO)) != WIOSKA_LOKACJE+"wioska9")
        	command(droga2[k]);
        if(k==sizeof(droga2)-1)
        {
            TO->command("emote znika w drzwiach domu.");
            remove_object();
            return;
        }
        else
        {
            k++;
            set_alarm(15.0,0.0,"koniec");
            return;
        }
    }
}

int
co_godzine()
{
    if (MT_GODZINA >= 20)
        koniec();
        
    return 1;
}