/* Autor: Duana
  Opis : Vortak
  Data : 31.05.2007
*/

#include <macros.h>
#include <stdproperties.h>
#include <money.h>
#include <pl.h>
#include <formulas.h>
#include <std.h>
#include <ss_types.h>
#include <wa_types.h>
#include <composite.h>
#include <filter_funs.h>
#include <pogoda.h>
#include "dir.h"

inherit WIOSKA_NPC_STD;

int czy_walka();
int walka = 0;

void
create_wioska_monster()
{
    set_living_name("staruszek_wioska");

    dodaj_nazwy("staruszek");

    ustaw_odmiane_rasy(PL_MEZCZYZNA);
    set_gender(G_MALE);

    set_long("Przygarbiony m^e^zczyzna ubrany w po^latan^a koszulin^e i "+
        "r^ownie^z nie pierwszej m^lodo^sci spodnie spogl^ada dooko^la "+
        "niebieskimi, podkr^a^zonymi oczami. Na czo^lo opadaj^a mu wci^a^z "+
        "niesforne siwe kosmyki - mimo ^ze nie zosta^lo ju^z ich zbyt "+
        "wiele, dalej nie pozwalaj^a si^e nijak u^lo^zy^c. D^lonie, "+
        "zniszczone wieloletni^a prac^a w polu, ju^z nie s^a tak silne jak "+
        "kiedy^s, tote^z staruszkowi nie pozostaje do roboty nic innego " +
        "jak kr^eci^c si^e po okolicy, zabijaj^ac wolny czas.\n");

        dodaj_przym("siwy","siwi");
        dodaj_przym("zgarbiony","zgarbieni");

    set_stats(({ 45, 35, 32, 35, 35, 40}));
    set_skill(SS_HERBALISM, 45);
    set_skill(SS_APPR_OBJ, 40);

    add_prop(CONT_I_WEIGHT, 80000);
    add_prop(CONT_I_HEIGHT, 160);
    add_prop(LIVE_I_NEVERKNOWN, 1);

    set_act_time(30);
    add_act("emote drepcze przed siebie.");
    add_act("emote prze^zuwa co^s w bezz^ebnych ustach.");
    add_act("usmiechnij sie promiennie");
    add_act("emote stara si^e odgarn^a^c w^losy z czo^la.");
    
    set_cchat_time(10);
    add_cchat("Za co? To^c ja nic z^lego nie zrobi^l...");
    add_cchat("Ja ci dam, ^lobuzie! Szacunku do starych ko^sci nie ma.");


    add_ask(({"karczm^e"}), VBFC_ME("pyt_o_karczme"));
    add_ask(({"so^ltysa"}), VBFC_ME("pyt_o_soltysa"));
    add_ask(({"syna"}), VBFC_ME("pyt_o_syna"));

    set_default_answer(VBFC_ME("default_answer"));

    set_reakcja_na_walke(0);
    set_random_move(30);
    
    add_armour(WIOSKA_UBRANIA + "polatana_bura_koszula.c");
    add_armour(WIOSKA_UBRANIA + "stare_powycieranie_spodnie.c");
    if (TEMPERATURA(ENV(TO)) < 10) add_armour(WIOSKA_UBRANIA + "czerwony_skorzany_plaszcz");

    set_alarm_every_hour("co_godzine");
    set_alarm(1.0, 0.0, "co_godzine");
}


void
powiedz_gdy_jest(object player, string tekst)
{
    if (environment(this_object()) == environment(player))
        this_object()->command(tekst);
}
string
pyt_o_karczme()
{
    command("usmiechnij sie mimowolnie");
    set_alarm(1.0, 0.0, "command_present", this_player(),
        "powiedz do " + OB_NAME(TP) + " Jak m^lodszy by^lem to lubi^lem "+
        "tam zachodzi^c, a teraz? Dudk^ow ni ma, a ta cholera, "+
        "m^oj syn ni grosza nie da. ");
    return "";
    
}
string
pyt_o_soltysa()
{
    set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
        "powiedz do " + OB_NAME(TP) + " Dy^c jak si^e bez most przejdzie, to zaraz so^ltysa "+
        "cha^lupa stoi. ");
    set_alarm(2.0, 0.0, "command", "wskaz");
    return "";
}

string
pyt_o_syna()
{
    command("skrzyw sie .");
    set_alarm(1.0, 0.0, "powiedz_gdy_jest", this_player(),
        "powiedz do " + OB_NAME(TP) + " Na w^lasnej piersi odchowany... A, szkoda gada^c nawet.");
    return "";
    
}

string
default_answer()
{
    command("wzrusz ramionami");
        set_alarm(1.0, 0.0, "powiedz_gdy_jest", this_player(),
            "powiedz do " + OB_NAME(TP) + " A sk^ad mi wiedzie^c takie pierdo^ly?");
    return "";
}


void
oddajemy(object ob, object old)
{
    if (interactive(old) && (ENV(old) == ENV(TO))) {
        command_present(old, "powiedz do " + OB_NAME(old) + " Nie interesuj^a mnie takie rzeczy.");

        command_present(old, "daj " + OB_NAME(ob) + " " + OB_NAME(old));
        command("odloz " + OB_NAME(ob));
        return;
    }

}

void
enter_inv(object ob, object from)
{
    if (from == TP)
        set_alarm(1.0, 0.0, "oddajemy", ob, from);

    ::enter_inv(ob, from);
}

void
add_introduced(string imie, string biernik)
{
    set_alarm(1.0, 0.0, "command", "usmiech promiennie");
}

void
emote_hook(string emote, object wykonujacy)
{
    switch (emote)
    {
        case "kopnij": set_alarm(1.5, 0.0, "nienajlepszy", wykonujacy);
                    break;
        case "spoliczkuj": set_alarm(1.5, 0.0, "nienajlepszy", wykonujacy);
                    break;
        case "opluj": set_alarm(2.5, 0.0, "nienajlepszy", wykonujacy);
                    break;
        case "poca^luj":
        case "przytul":
        case "pog^laszcz":

              {

              if(wykonujacy->query_gender() == 1)
                switch(random(2))
                 {
                 command("usmiechnij sie promiennie");
                 case 0: set_alarm(1.0, 0.0, "powiedz_gdy_jest", TP, "powiedz do "+ OB_NAME(TP) +
                     " Kochaniutka, to^c ja twoim dziadkiem m^og^lbym "+
                     "by^c...");
                        break;
                 case 1:
                      set_alarm(1.5, 0.0, "command", "westchnij tesknie");
                      powiedz_gdy_jest(TP, "powiedz do " + OB_NAME(TP) + " A^z mi "+
                     "si^e moja stara wspomnia^la... To^c to b^edzie "+
                     "dziesi^e^c wiosen, jak jej si^e na zaraz^e zmar^lo.");
                        break;

                }
              else
                {
                   switch(random(3))
                   {
                   case 0:set_alarm(1.0, 0.0, "powiedz_gdy_jest", TP,
                   "powiedz do " + OB_NAME(TP) + " A p^ojdzie mi st^ad! "+
                       "B^edzie starego zaczepia^l!");
                            break;
                   case 1:set_alarm(1.0, 0.0, "powiedz_gdy_jest", TP,
                   "powiedz do " + OB_NAME(TP) + " Wstrzymaj si^e, bo przez "+
                       "^leb zdziel^e!");
                            break;
                   case 2:set_alarm(1.0, 0.0, "powiedz_gdy_jest", TP, "powiedz do " +
                   OB_NAME(TP) + " A czego ty mi! ^Ze si^e "+
                       "rodz^a takie zbocze^nce...");
                            break;
                   }
                }
              }
                break;
    }
}

void
nienajlepszy(object kto)
{
   switch (random(3))
   {
      case 0: set_alarm(1.0, 0.0, "powiedz_gdy_jest", TP, "powiedz do " + OB_NAME(TP) +
          " Ost... ostawcie starego w spokoju...");
              break;
      case 1: set_alarm(1.0, 0.0, "command", "westchnij ciezko");
             set_alarm(2.5, 0.0, "powiedz_gdy_jest", TP, "powiedz do " +
             OB_NAME(TP) + " No i co ja wam "+
          "z^lego zrobi^lem?");
              break;
      case 2: set_alarm(1.0, 0.0, "powiedz_gdy_jest", TP, "powiedz do " +
      OB_NAME(TP) + " To^c ja "+
          "niewinny!");
          set_alarm(3.0, 0.0, "command", "rozejrzyj sie smutno");
              break;
   }
} 

void attacked_by(object wrog)
{
    if(!walka)
    {
        walka = 1;
        set_alarm(0.5,0.0,"command","krzyknij Ludzie! Ratujta! Zb^oje we "+
            "wsi!");
        set_alarm(15.0, 0.0, "czy_walka", 1);
        return ::attacked_by(wrog);
    }
}
int
czy_walka()
{
    if(!query_attack() && walka)
    {
        set_alarm(0.5,0.0,"command","powiedz I po co to by^lo?");
        walka = 0;
        return 1;
    }
    else
    {
        set_alarm(15.0, 0.0, "czy_walka", 1);
    }
}

void
koniec()
{
    if(query_attack())
    {
        set_alarm(10.0,0.0,"koniec");
        return;
    }
    else
    {
        int k = 0;
        mixed droga2=znajdz_sciezke(environment(this_object()),
            WIOSKA_LOKACJE + "wioska7");
        command(droga2[k]);
        if(k==sizeof(droga2)-1)
        {
            TO->command("emote znika w drzwiach jednej z chat.");
            remove_object();
            return;
        }
        else
        {
            k++;
            set_alarm(15.0,0.0,"koniec");
            return;
        }
    }
}

int
co_godzine()
{
    if (MT_GODZINA >= 20)
        koniec();

    return 1;
}