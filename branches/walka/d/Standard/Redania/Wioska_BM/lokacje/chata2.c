#include <macros.h>
#include <stdproperties.h>
#include "dir.h"

inherit "/std/room";

create_room()
{
    set_short("Pustka");
    
    set_long("Oto czego potrzebuj^a bogaci, a ^zebracy to maj^a: nic.\n");
    
    add_object(WIOSKA_DRZWI + "z_chaty2");
}

string
exits_description()
{
    return "Drzwi prowadz^a z powrotem do ^swiata ^zywych.\n";
}
