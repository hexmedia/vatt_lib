#include <stdproperties.h>
#include <macros.h>
#include "dir.h"

inherit "/std/room";

string
dlugij()
{
    string str = "Panuj^acy tu zaduch i potworny wr^ecz smr^od ptasiego " +
    "^lajna zdaje si^e zupe^lnie nie przeszkadza^c kurom, troskliwie " +
    "wysiaduj^acym jajka na szerokich, dwupoziomowych grz^edach zajmuj^ac" +
    "ych wi^eksz^a cz^e^s^c tego pomieszczenia. ";

    if (jest_dzien())
        str += "W w^askich smu^zkach ^swiat^la, przedostaj^acych si^e " +
        "przez otwory z niedok^ladnie zbitych desek w ^scianach ";
    else
        str += "W powietrzu ";

    str += "unosz^a si^e ogromne ilo^sci kurzu i pierza, " +
    "pokrywaj^acego tak^ze cienk^a warstw^e wilgotnego siana niechlujnie " +
    "rozrzuconego po pod^lodze. Sufit, zas^lany g^est^a sieci^a zwisaj^acy" +
    "ch niczym baldachimy paj^eczyn oraz porozrzucane wsz^edzie skorupki " +
    "rozbitych jaj ^swiadcz^a, i^z gospodarz tego kurnika nie zagl^ada tu " +
    "zbyt cz^esto.\n";
    
    return str;
}

void
kury()
{
    if (TO->jest_dzien() && !present("kura", this_object()))
    {
      int n = random(4)+2;
      for (int i = 0; i < n; i++)
          clone_object(WIOSKA_NPC + "kura.c")->move(TO);
    }
}

void
jaja()
{
    clone_object(WIOSKA_PRZEDMIOTY + "jajko.c")->move(TO);
}

create_room()
{
    set_short("Niewielki kurnik");
    
    set_long("@@dlugij@@");
    
    add_prop(ROOM_I_INSIDE, 1);
    add_prop(ROOM_I_LIGHT, 1);
    
    add_object(WIOSKA_DRZWI + "z_kurnika");
    clone_object(WIOSKA_PRZEDMIOTY + "jajko.c")->move(TO);
    clone_object(WIOSKA_PRZEDMIOTY + "jajko.c")->move(TO);
    clone_object(WIOSKA_PRZEDMIOTY + "jajko.c")->move(TO);
    clone_object(WIOSKA_PRZEDMIOTY + "jajko.c")->move(TO);
    clone_object(WIOSKA_PRZEDMIOTY + "jajko.c")->move(TO);
    
    kury();
    set_alarm_every_hour("kury");
    
    set_alarm(1.0, 14400.0, "jaja");
    
    dodaj_rzecz_niewyswietlana("kurze jajko", 100000);
}

string
exits_description()
{
    return "Wyj^scie z kurnika prowadzi na zewn^atrz.\n";
}
