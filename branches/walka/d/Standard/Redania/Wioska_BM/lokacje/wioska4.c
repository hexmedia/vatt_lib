/*
 * Opis: Vortak
 * Kod: Gjoef
 *     Maj 2007
 */

#include <macros.h>
#include <stdproperties.h>
#include <mudtime.h>
#include <pogoda.h>
#include "dir.h"

inherit WIOSKA_STD;

string
domek()
{
    string str = "";

    if (CZY_JEST_SNIEG(TO))
        str = "Schowany w bia^lym puchu";
    else
        str = "Ukryty w^sr^od zieleni";

    str += " domek sprawia wra^zenie schludnego i zadbanego. Prowadzi do^n " +
    "od po^ludnia " +
    "w^aska, dok^ladnie zamieciona ^scie^zka. Posiad^lo^s^c otacza solidny " +
    "p^lot.\n";

    return str;
}

void
create_wioska()
{
    set_short("W pobli^zu placu");

    add_exit("wioska5.c", "n");
    add_exit("wioska3.c", "sw");
    
    add_item("^zywop^lot", "G^esty ^zywop^lot otacza z obu stron ^scie^zk^e.\n");
}

string
exits_description()
{
    return "^Scie^zka rozci^aga si^e na po^ludniowy-zach^od i p^o^lnoc.\n";
}

string
dlugi_opis()
{
    string str = "Otoczona z obu stron ";
    
    if (CZY_JEST_SNIEG(TO))
        str += "schowanym pod warstw^a ^sniegu";
    else
        str += "g^estym, zielonym";
        
    str += " ^zywop^lotem ^scie^zka omija ^lagodnym ^lukiem widoczny na " +
    "zachodzie schludny, bielony domek. Jest na tyle szeroka, ^ze bez " +
    "trudu minie si^e na niej dw^och konnych, a zapewne i niedu^zy pow^oz " +
    "da^lby rad^e przejecha^c. Niedaleko st^ad, na po^ludnie od dr^o^zki " +
    "wy^lania si^e zza drzew kawa^lek traktu. ";
    
    if (TEMPERATURA(TO) <= 0)
        str += "Siarczysty mr^oz sku^l go grub^a warstw^a lodu. ";
    else
        if (jest_dzien())
            str += "Jego cichy szmer zag^luszany jest ujadaniem ps^ow " +
            "pilnuj^acych gospodarstw. ";
    
    str += "Raptem kilka krok^ow na p^o^lnoc od miejsca, w kt^orym si^e " +
    "znalaz^le^s rozci^aga si^e placyk stanowi^acy centrum wioski. W " +
    "oddali wida^c bez^ladnie rozrzucone ";

    if (CZY_JEST_SNIEG(TO))
        str += "po ^sniegu ";
    else
        str += "w^sr^od zieleni ";

    str += "drewniane cha^lupy i zabudowania gospodarcze. ";

    str += "\n";
    
    return str;
}
