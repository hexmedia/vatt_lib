/*
 * Opis: Vortak
 * Kod: Gjoef
 *     Maj 2007
 */

#include <macros.h>
#include <stdproperties.h>
#include <mudtime.h>
#include <pogoda.h>
#include "dir.h"

inherit WIOSKA_STD;

void
create_wioska()
{
    set_short("Przy studni");

    add_exit("wioska7.c", "w");
    add_exit("wioska5.c", "sw");
    add_exit("wioska6.c", "s");
    
    add_object(WIOSKA_PRZEDMIOTY + "studnia");
    
    dodaj_rzecz_niewyswietlana("studnia");
}

string
exits_description()
{
    return "Pozosta^la cz^e^s^c placu widoczna jest na zachodzie, po^ludnio" +
    "wym-zachodzie i po^ludniu.\n";
}

string
dlugi_opis()
{
    string str = "Znajdujesz si^e na p^o^lnocno-wschodnim kra^ncu niewielki" +
    "ego, stanowi^acego centrum wioski placu. Ziemia, w innych jego cz^e^sc" +
    "iach porz^adnie ubita, tutaj zamieni^la si^e ";

    if (TEMPERATURA(TO) >= 1)
        str += "w rozmi^ek^l^a, b^lotnist^a brej^e. ";
    else
        str += "niemal w lodowisko. ";

    str += "W^la^snie w tym miejscu stoi niedu^za, zadaszona studnia, z " +
    "kt^orej krystalicznie czyst^a wod^e czerpi^a mieszka^ncy miejscowo^sc" +
    "i. Od p^o^lnocy na zach^od rozci^aga si^e widok wiejskich chat i zab" +
    "udowa^n gospodarczych, za^s niedaleko na po^ludniu przebiega dukt kupiecki. ";
    
    str += "\n";
    
    return str;
}

int
picku(string str)
{
    switch (str)
    {
        case "si^e wody ze studni":
        case "si^e ze studni":
            notify_fail("Nie jeste^s w stanie napi^c si^e wody z samej studni.\n");
            return 0;
            break;
        default:
            notify_fail("Napij si^e czego sk^ad?\n");
            return 0;
            break;
    }
}

//na kiedys... ;)
int
wskocz(string str)
{
    notify_fail("S^lucham?\n");
    return 0;
}

void
init()
{
    ::init();

    add_action("picku", "napij");
    add_action("wskocz", "wskocz");
}
