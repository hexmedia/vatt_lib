/*
 * Opis: Vortak
 * Kod: Gjoef
 *     Maj 2007
 */

#include <macros.h>
#include <stdproperties.h>
#include <mudtime.h>
#include <pogoda.h>
#include "dir.h"

inherit WIOSKA_STD;

void
create_wioska()
{
    set_short("Na zach^od od placu");

    add_exit("wioska7.c", "e");
    add_exit("wioska10.c", "w");
    
    add_object(WIOSKA_DRZWI + "do_chaty2");
    
    add_npc(WIOSKA_NPC + "mlody_szczuply_mezczyzna", 1, "czy");
}

int czy()
{
    if (find_living("mlodzieniec_wioska") == 0)
    return 1;
    return 0;
}

string
exits_description()
{
    return "^Scie^zka rozci^aga si^e na zach^od i wsch^od, w stron^e placu." +
    " Tu za^s znajduj^a si^e drzwi jednej z wiejskich chat.\n";
}

string
dlugi_opis()
{
    string str = "W^aska ^scie^zka, wydeptana przez przechodz^acych t^edy " +
    "ka^zdego dnia tubylc^ow ^l^aczy widoczny na wschodzie niewielki, " +
    "b^ed^acy centrum wioski placyk z niedu^zymi, ubogo wygl^adaj^acymi " +
    "cha^lupkami, porozrzucanymi bez^ladnie po ";

    if (CZY_JEST_SNIEG(TO))
        str += "schowanej w ^sniegu ";

    str += "okolicy. Pomi^edzy nimi dostrzec mo^zna ma^le, powykr^ecane " +
    "drzewka";
    
    if (!CZY_JEST_SNIEG(TO))
        str += ", kt^ore od dawna nie wyda^ly ^zadnego owocu";
        
    str += ". Niewygodny, pe^len wyboi i nier^owno^sci dukt wydeptany jest ";
    
    if (CZY_JEST_SNIEG(TO))
        str += "w g^l^ebokiej warstwie ^snie^znobia^lego puchu";
    else
        str += "mi^edzy wysokimi, g^estymi chwastami, tworz^acymi na " +
        "poboczu trudny do przebycia g^aszcz";
    
    str += ".\n";
    
    return str;
}
