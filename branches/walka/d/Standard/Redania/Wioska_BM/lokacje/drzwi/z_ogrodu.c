#include <stdproperties.h>
#include <materialy.h>
#include "dir.h"

inherit "/std/door";

void
create_door()
{
    ustaw_nazwe("furtka");
    dodaj_przym("niewielki", "niewielcy");

    set_other_room(WIOSKA_LOKACJE + "wioska3.c");
    set_door_id("WIOSKA_OGROD");
    set_door_desc("Bia^la furtka w wysokim na sze^s^c st^op p^locie.\n");
        
    set_pass_command(({({"wyj^scie","furtka"}), "na zewn^atrz", "zza furtki"}));
    set_open_desc("");
    set_closed_desc("");
    
    ustaw_material(MATERIALY_DR_JESION); //? ja o drewnie wiem tyle, ze mokre sie nie pali
    
    add_prop(DOOR_I_HEIGHT, 666);

    set_locked(0);

    set_key("WIOSKA_OGROD_KLUCZ");
}
