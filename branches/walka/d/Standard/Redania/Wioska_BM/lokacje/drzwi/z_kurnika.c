#include <stdproperties.h>
#include <materialy.h>
#include "dir.h"

inherit "/std/door";

void
create_door()
{
    ustaw_nazwe("drzwiczki");
    dodaj_przym("ma^ly", "mali");
    dodaj_przym("drewniany", "drewniani");
    
    set_other_room(WIOSKA_LOKACJE + "wioska10.c");
    set_door_id("WIOSKA_KURNIK");
    set_door_desc("Kilka krzywych sosnowych desek zbito razem, tworz^ac " +
    "prowizoryczne, wysokie na ledwie pi^e^c st^op drzwiczki.\n");
        
    set_pass_command(({"wyj^scie", "na zewn^atrz", "z kurnika"}));
    set_open_desc("");
    set_closed_desc("");
    
    ustaw_material(MATERIALY_DR_SOSNA);
    add_prop(DOOR_I_HEIGHT, 150);
}
