#include <stdproperties.h>
#include <materialy.h>
#include "dir.h"

inherit "/std/door";

void
create_door()
{
    ustaw_nazwe("drzwi");
    dodaj_przym("stary", "starzy");
    dodaj_przym("sosnowy", "sosnowi");

    set_other_room(WIOSKA_LOKACJE + "wioska9.c");
    set_door_id("WIOSKA_CHATA2");
    set_door_desc("Funkcj^e drzwi pe^lni tu nic innego jak kilka starych, " +
    "sosnowych desek zbitych w kup^e. Tu i tam korniki zacz^e^ly prze^zera" +
    "^c drewno, a mimo to konstrukcja wygl^ada w miar^e solidnie.\n");
        
    set_pass_command(({"wyj^scie", "na zewn^atrz", "z chaty"}));
    set_open_desc("");
    set_closed_desc("");
    
    ustaw_material(MATERIALY_DR_SOSNA);
    
    set_open(0);
    set_locked(1);
}
