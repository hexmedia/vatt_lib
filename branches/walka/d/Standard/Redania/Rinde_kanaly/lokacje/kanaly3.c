/* Autor: Avard
   Data : 08.04.07
   Opis : Valor i Faeve */

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <pogoda.h>
#include "dir.h"
#define DEF_DIRS ({"p^o^lnoc"})
inherit RINDE + "std/kanaly.c";

void 
create_kanaly() 
{
    set_short("Kana^ly");
    add_prop(ROOM_I_INSIDE,1);
    add_exit(KANALY_LOKACJE + "kanaly2.c","s",0,1,0);
    add_prop(ROOM_I_LIGHT, 0);
}
int unq_no_move(string str)
{
    ::unq_no_move(str);

    if (member_array(query_verb(), DEF_DIRS) != -1)
        notify_fail("Niestety, droga na p^o^lnocy jest zasypana.\n");
    return 0;
}
public string
exits_description() 
{
    return "Kana^ly ci^agn^a si^e na po^ludniu.\n";
}
string
dlugi_opis()
{
    string str;
    str = "Otaczaj^a ci^e wilgotne ^sciany pokryte zielonkawym "+
        "szlamem. S^a one pozbawione cegie^l i ziej^a wyja^lowion^a "+
        "ziemi^a, z kt^orej do^s^c g^esto wystaj^a, stare korzenie. Z "+
        "sufitu raz po raz spada malutka kropelka wody, kt^ora wpadaj^ac "+
        "do ka^lu^zy wydaje cichy d^xwi^ek. Twe uszy jednocze^snie "+
        "wy^lapuj^a ciche skrobanie dochodz^ace z dalszej cz^e^sci "+
        "korytarza. W powietrzu czuje si^e lekko s^lodkawy, obrzydliwy "+
        "zapach rozk^ladaj^acego si^e mi^esa. \n"; 
    return str;
}
