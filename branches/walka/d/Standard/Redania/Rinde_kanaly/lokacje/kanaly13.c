/* Autor: Avard
   Data : 08.04.07
   Opis : Valor i Faeve */

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include <pogoda.h>
#include "dir.h"

inherit RINDE + "std/kanaly.c";

void 
create_kanaly() 
{
    set_short("Przy kracie");
    add_prop(ROOM_I_INSIDE,1);
    add_object(KANALY_OBIEKTY + "kupa");
    add_exit(KANALY_LOKACJE + "kanaly6.c",
        ({"u","na g^or^e po schodach"}),0,1,0);
    add_exit(KANALY_LOKACJE + "kanaly6.c",
        ({"schody","na g^or^e po schodach"}),0,1,0);
    add_exit(KANALY_LOKACJE + "kanaly6.c",
        ({"schodki","na g^or^e po schodach"}),0,1,0);

    add_item(({"schodki","schody"}),"Kamienne schodki prowadz^a w g^or^e. "+
        "Wykute w kamieniu s^a ma^le lecz mo^zna po nich pewnie st^apa^c.\n");

    add_npc(RINDE_NPC + "ghoul");
    add_npc(RINDE_NPC + "szczur.c");
    add_npc(RINDE_NPC + "szczur.c");
    add_npc(RINDE_NPC + "szczur.c");
    add_prop(ROOM_I_LIGHT, 0);
}
public string
exits_description() 
{
    return "Znajduj^a si^e tutaj schody po kt^orych mo^zesz wej^s^c "+
        "na g^or^e.\n";
}
string
dlugi_opis()
{
    string str;
    str = "Znajdujesz si^e w ma^lej komacie o kwadratowej powierzchni, "+
        "szerokiej i d^lugiej na sze^s^c ^lokci. ^Sciany pokryte b^lotem "+
        "i krwi^a, nadaj^a temu miejscu przera^zaj^acy widok. Pod^loga "+
        "usiana ko^s^cmi i szmatami wskazuje, i^z zy^c tu musi jaki^s "+
        "drapie^znik. W jednym rogu widzisz wieksz^a kup^e podgni^lych "+
        "ubra^n a w przeciwleg^lym - ma^le kamienne schodki prowadz^ace "+
        "w g^or^e. Cuchn^acy zapach i st^ech^le powietrze ledwo "+
        "pozwalaj^a oddycha^c.\n";    
    return str;
}
