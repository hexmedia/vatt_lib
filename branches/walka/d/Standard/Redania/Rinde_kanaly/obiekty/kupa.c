/* Autor: Avard
   Opis : Sniegulak
   Data : 9.04.07 */

inherit "/std/object";

#include "/sys/stdproperties.h"
#include "/sys/macros.h"
#include "/sys/pl.h"
#include "/sys/materialy.h"
#include "/sys/object_types.h"

void
create_object()
{
    ustaw_nazwe(({"kupa ubra^n","kupy ubra^n","kupie ubra^n","kup^e ubra^n",
        "kup^a ubra^n","kupie ubra^n"}),({"kupy ubra^n","kup ubra^n",
        "kupom ubra^n","kupy ubra^n","kupami ubra^n","kupach ubra^n"}), 
        PL_ZENSKI);
    ustaw_material(MATERIALY_LEN);
    set_type(O_INNE);
    set_long("Jest to kupa przegni^lych ubra^n rzuconych w jedno miejsce. "+
        "Niekt^ore zakrwawione i poszrapane, s^a niezdatne do u^zytku. "+
        "Zapach od nich bij^acy przyprawia ci^e o md^lo^sci.\n");
             
    add_prop(OBJ_I_WEIGHT, 7000); 
    add_prop(OBJ_I_VOLUME, 7000); 
    add_prop(OBJ_I_VALUE, 0);    
    add_prop(OBJ_I_DONT_SHOW_IN_LONG, 1);
    add_prop(OBJ_I_NO_GET,"Tych ubra^n jest ca^la masa, w dodatku nie "+
        "nadaj^a si^e do niczego, wi^ec po co je w og^ole rusza^c?\n");

    jak_pachnie="@@zapach@@";
    jak_wacha_jak_sie_zachowuje="@@reakcja@@";
}

string
zapach(string str)
{
  str="Chyba lepiej by^lo tego nie robi^c. Nieziemski smr^od "+
      "rozkladaj^acych si^e resztek, krwi i brudnych ubra^n "+
      "wywo^luje skr^et kiszek i zawroty g^lowy.";
  return str;
}

string
reakcja(string str)
{
  str=" w^acha kup^e starych, ^smierdz^acych ubra^n";
  return str;
}