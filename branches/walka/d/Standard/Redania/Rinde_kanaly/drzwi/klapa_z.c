
/* Autor: Avard
   Opis : Faeve
   Data : 10.04.07*/

inherit "/std/door";

#include <pl.h>
#include "dir.h"
void
create_door()
{
    ustaw_nazwe("klapa"); 
    dodaj_przym("drewniany","drewniany");
    
    set_other_room(POLNOC + "Karczma/lokacje/spizarnia.c");
    set_door_id("KLAPA_DO_KANALOW_RINDE");
    set_door_desc("Drewniana klapa wzmocniona stalowymi pr^etami.\n");

    set_open_desc("");
    set_closed_desc("");
    set_open(0);
    set_locked(1);
        
    set_pass_command(({({"g^ora","u","drabina","klapa"}),"przez klap^e",
        "wychodz^ac z kana^l^ow"}));
    
    set_lock_command("zamknij");
    set_unlock_command("otworz");

    set_key("KLUCZ_KLAPA_DO_KANALOW_RINDE");
    set_lock_name("zamek");
}