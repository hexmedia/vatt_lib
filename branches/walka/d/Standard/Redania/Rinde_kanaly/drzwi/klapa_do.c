
/* Autor: Avard
   Opis : Faeve
   Data : 10.04.07*/

inherit "/std/door";

#include <pl.h>
#include "dir.h"
void
create_door()
{
    ustaw_nazwe("klapa"); 
    dodaj_przym("drewniany","drewniany");
    
    set_other_room(KANALY_LOKACJE + "kanaly12.c");
    set_door_id("KLAPA_DO_KANALOW_RINDE");
    set_door_desc("Drewniana klapa wzmocniona stalowymi pr^etami.\n");

    set_open_desc("");
    set_closed_desc("");
    set_open(0);
    set_locked(1);
        
    set_pass_command(({({"d^o^l","d","klapa"}),"przez klap^e",
        "schodz�c po drabinie"}));
    
    set_lock_command("zamknij");
    set_unlock_command("otworz");

    set_key("KLUCZ_KLAPA_DO_KANALOW_RINDE");
    set_lock_name("zamek");
}