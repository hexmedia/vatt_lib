
/* Autor: Avard
   Opis : Duana
   Data : 10.04.07*/

inherit "/std/door";

#include <pl.h>
#include <stdproperties.h>
#include <macros.h>
#include <ss_types.h>
#include <cmdparse.h>
#include <language.h>

#include "dir.h"
void
create_door()
{
    ustaw_nazwe("krata"); 
    dodaj_przym("stalowy","stalowi");
    
    set_other_room(POLUDNIE_LOKACJE + "ulica7.c");
    set_door_id("KRATA_DO_KANALOW_RINDE");
    set_door_desc("Grube czworok^atne pr^ety po^l^aczono tworz^ac krat^e "+
        "zagradzaj^ac^a wyj^scie z miejskich kana^l^ow. Metal pokrywa "+
        "cienka warstwa rdzy.\n");

    set_open_desc("");
    set_closed_desc("");
    set_open(0);
        
    set_pass_command(({({"g^ora","u","drabina","krata"}),"przez otw^or w "+
        "suficie","wychodz^ac z kana^l^ow"}));
    
    set_lock_command("zamknij");
    set_unlock_command("otworz");

    set_key("KLUCZ_KRATA_DO_KANALOW_RINDE");
    set_lock_name("zamek");
}

int
pass_door(string arg)
{
    int dexh;

    if (!other_door)
        load_other_door();

    int okej = 0;
    string curCmd = query_verb() + (stringp(arg) ? " " + arg : "");
    if(sizeof(pass_commands))
    {
      int i;
      for(i=0;i<sizeof(pass_commands);i++)
      {
        if (curCmd ~= pass_commands[i]) {
          okej = 1;
          break;
        }
      }
    }
    else if (curCmd ~= pass_commands)
      okej = 1;
    if (!okej) {
      return 0;
    }

    /*
        The times higher a player can be and still get through
    */
    dexh = 2 + (this_player()->query_stat(SS_DEX) / 25);

    if (this_player()->query_rasa(0) ~= "szczur")
        return 1;

    if (open_status)
    {
        /* Lets say we arbitrarily can bend as dexh indicates.
           For something else, inherit and redefine.
         */
        if ((int)this_player()->query_prop(CONT_I_HEIGHT) > 
                        query_prop(DOOR_I_HEIGHT) * dexh) 
        {
            write("Jeste^s zbyt du^z" + 
                this_player()->koncowka("y", "a", "e") + 
                " i za ma^lo zr^eczn" + 
                this_player()->koncowka("y", "a", "e") +
                ", ^zeby si^e przecisn^a^c przez " + short(PL_BIE));
            return 1;
        }
        else if ((int)this_player()->query_prop(CONT_I_HEIGHT) > 
                        query_prop(DOOR_I_HEIGHT))
        {
            write("Z wielkim trudem przeciskasz si^e przez " + short(PL_BIE) + 
                ".\n");
            saybb(QCIMIE(this_player(), PL_MIA) + " z wielkim trudem "
                + "przeciska si^e przez " + this_object()->short(PL_BIE)
                + ".\n");
        }
            
        if (my_pass_mess) tell_object(this_player(), capitalize(check_call(my_pass_mess)));
//      this_player()->move_living(check_call(pass_mess), other_room);
        this_player()->move_living(sposob, other_room);
//      tell_roombb(environment(this_player()), QCIMIE(this_player(), PL_MIA) +
//          " pod^a^za " + check_call(pass_mess) + ".\n", this_player());
//      if (my_pass_mess) tell_object(this_player(), capitalize(check_call(my_pass_mess)));
//      this_player()->move_living("M", other_room);
    }
    else
        write(check_call(fail_pass));

    return 1;
}