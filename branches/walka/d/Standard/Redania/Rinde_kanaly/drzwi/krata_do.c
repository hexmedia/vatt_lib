
/* Autor: Avard
   Opis : Duana
   Data : 10.04.07*/

inherit "/std/door";

#include <pl.h>
#include "dir.h"
void
create_door()
{
    ustaw_nazwe("krata"); 
    dodaj_przym("stalowy","stalowi");
    
    set_other_room(KANALY_LOKACJE + "kanaly1.c");
    set_door_id("KRATA_DO_KANALOW_RINDE");
    set_door_desc("Grube czworok^atne pr^ety po^l^aczono tworz^ac krat^e "+
        "zagradzaj^ac^a wej^scie do miejskich kana^l^ow. Metal pokrywa "+
        "cienka warstwa rdzy.\n");

    set_open_desc("");
    set_closed_desc("");
    set_open(0);
        
    set_pass_command(({({"d^o^l","d","krata"}),"do kana��w",
        "schodz�c po drabinie"}));
    
    set_lock_command("zamknij");
    set_unlock_command("otworz");

    set_key("KLUCZ_KRATA_DO_KANALOW_RINDE");
    set_lock_name("zamek");
}