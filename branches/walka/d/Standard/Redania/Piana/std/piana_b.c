inherit "/std/room";

#include <mudtime.h>
#include <language.h>
#include <stdproperties.h>
#include <macros.h>

void
create_brzeg()
{
}

/*
string
evencik()
{
	switch(random(4))
	{
		case 0:
			return "Kto� wyla� wod� w okna kamienicy.\n"; break;
		case 1:
			if(jest_dzien())
			return "Dwa jad�ce z naprzeciwka wozy zaczepi�y o siebie co "+
					"zako�czy�o si� k��tni� wo�nic�w.\n";
			else
			return "Z bocznego zau�ka dochodzi bojowe zawodzenie "+
					"walcz�cych kot�w.\n";
			break;
		case 2:
			if(jest_dzien())
			return "Banda umorusanych dzieciak�w zwinnie przemyka "+
					"pomi�dzy wozami.\n";
			else
			return "Kto� przemkn�� na drug� stron� uliczki.\n";
			break;
		case 3:
			if(jest_dzien())
			return "Pomi�dzy wozami dostrzegasz prowadzone gdzie� stadko k�z.\n";
			else
			return "Z dachu zsun�a si� dach�wka i z �oskotem rozbi�a na ulicy.\n";
			break;
	}

}*/

nomask void
create_room()
{
    set_long("@@dlugi_opis@@"); 
    set_polmrok_long("@@opis_nocy@@");
    add_sit("na brzegu", "na brzegu", "z brzegu", 0);
	
	add_prop(ROOM_I_TYPE,ROOM_IN_CITY);
	
    create_brzeg();

	set_event_time(300.0);
	add_event("@@evencik:"+file_name(TO)+"@@");

    /* prop�w o warto�ci 0 si� nie dodaje, ale jakby kto� mia� skorzysta�
     * z tego std do innych miast, musi te propy uwzgl�dni�...*/
    add_prop(ROOM_I_WSP_Y, -2); //Wsp�rz�dne, do systemu pogodowego.
    add_prop(ROOM_I_WSP_X, -4); //Rinde jest p�pkiem wszech�wiata ;)
    					//A Bia�y Most na zach�d od niego...
}
/*
public void
enter_inv(object ob, object from)
{
    ::enter_inv(ob, from);

    if (interactive(ob) && !ob->query_wiz_level()) {
        ob->set_default_start_location(PIANA_LOKACJE + "");
    }
}*/