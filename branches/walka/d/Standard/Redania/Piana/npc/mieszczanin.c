
/* Autor: Avard
   Opis : Samaia
   Data : 23.07.07 */

#include <std.h>
#include <ss_types.h>
#include <macros.h>
#include <stdproperties.h>
#include <pl.h>
#include <money.h>
#include <object_types.h>
#include "dir.h"

inherit PIANA_STD_NPC;

int czy_walka();
int walka = 0;

void
create_piana_npc()
{
    ustaw_odmiane_rasy("mezczyzna");
    dodaj_nazwy("mieszczanin");
    dodaj_nazwy("mlodzieniec");
    set_gender(G_MALE);
    dodaj_przym("m^lody","m^lodzi");
    dodaj_przym("ospa^ly","ospali");

    set_long("W^at^ly, bladosk^ory m^lodzieniec, wpatruj^acy si^e jasnymi "+
        "oczyma w dal, wygl^ada raczej na znu^zonego, ni^z zm^eczonego, "+
        "cho^c zniszczone d^lonie wskazuj^a na to, i^z nie zwyk^l "+
        "pr^o^znowa^c. Jego nieproporcjonalna sylwetka przejawia si^e "+
        "g^l^ownie w zbyt d^lugich r^ekach, zwieszonych bezwiednie wzd^lu^z "+
        "chudego cia^la, oraz ko^slawych nogach. M^e^zczyzna raz po raz "+
        "bezwiednie poprawia na sobie i tak brudne oraz zniszczone "+
        "odzienie.");

    set_stats (({ 70, 90, 70, 50, 55, 70 }));
    add_prop(CONT_I_WEIGHT, 60000);
    add_prop(CONT_I_HEIGHT, 180);

    set_act_time(60);
    add_act("rozejrzyj sie ze znudzeniem");
    add_act("przestap");
    add_act("emote przygl^ada si^e swoim d^loniom, po czym zaczyna obgryza^c paznokcie.");
    add_act("emote wzdycha, spogl^adaj^ac gdzie^s mglistym wzrokiem.");
    
    set_cact_time(10);
    add_cact("'Przesta^n, natychmiast!");
    add_cact("krzyknij Stra^z! Stra^z, pomocy!");

    add_ask(({"pian^e","Pian^e","miasto"}), VBFC_ME("pyt_o_piane"));
    add_ask(({"karczm^e","garnizon","sklep","zak^lad snycerski","snycerza",
        "zak^lad","burmistrza","dom burmistrza","ratusz","brzeg","rynek"}), 
        VBFC_ME("pyt_o_miejsce"));
    set_default_answer(VBFC_ME("default_answer"));

    set_random_move(100);
    set_restrain_path(PIANA_LOKACJE_ULICE);
}

void
powiedz_gdy_jest(object player, string tekst)
{
    if (environment(this_object()) == environment(player))
        this_object()->command(tekst);
}

string
pyt_o_piane()
{
    if(!query_attack())
    {
        set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
        "':kiwaj^ac g^low^a powoli: Tu si^e urodzi^lem.");
    }
    else
    {
        set_alarm(0.5,0.0,"powiedz_gdy_jest", TP, "'Zostaw mnie!");
    }    
    return "";
}
string
pyt_o_miejsce()
{
    if(!query_attack())
    {
        set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
        "':wskazuj^ac nieokre^slony kierunek: To gdzie^s tam.");
    }
    else
    {
        set_alarm(0.5,0.0,"powiedz_gdy_jest", TP, "'Zostaw mnie!");
    }    
    return "";
}
string
default_answer()
{
    if(!query_attack())
    {
        set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
            "':wzruszaj^ac ramionami: Daj mi spok^oj.");
    }
    else
    {
        set_alarm(0.5,0.0,"powiedz_gdy_jest", TP, "'Zostaw mnie!");
    }
    return "";
}

void
add_introduced(string imie_mia, string imie_bie)
{
    set_alarm(2.0, 0.0, "return_introduce", find_player(imie_mia));
}
void
return_introduce(object ob)
{
    command("emote kiwa g^low^a i mamrocze co^s cicho do siebie.");
}

void
emote_hook(string emote, object wykonujacy)
{
    switch (emote)
    {
        case "kopnij": set_alarm(1.5, 0.0, "nienajlepszy", wykonujacy);
                    break;
        case "spoliczkuj": set_alarm(1.5, 0.0, "nienajlepszy", wykonujacy);
                    break;
        case "opluj" : set_alarm(2.5, 0.0, "nienajlepszy", wykonujacy);
                    break;
        case "szturchnij" : set_alarm(2.5, 0.0, "nienajlepszy", wykonujacy);
                    break;
        case "poca^luj": set_alarm(1.5,0.0, "malolepszy",wykonujacy);
                    break;
        case "prychnij": set_alarm(1.0,0.0, "command", "spojrzyj "+
            "lekcewa^z^aco na "+OB_NAME(wykonujacy)); 
                    break;
        case "przytul": set_alarm(1.5,0.0, "malolepszy",wykonujacy);
                    break;
        case "poglaszcz": set_alarm(0.5, 0.0, "malolepszy", wykonujacy);
                    break;
        case "poklep": set_alarm(0.5, 0.0, "malolepszy", wykonujacy);
                    break;
        case "usmiech": set_alarm(0.5, 0.0, "malolepszy", wykonujacy);
                    break;
        case "mrugnij": set_alarm(0.5, 0.0, "malolepszy", wykonujacy);
                    break;
    }
}

void
malolepszy(object wykonujacy)
{
    switch(random(1))
    {
        case 0: command("emote rumieni si^e i spuszcza nie^smia^lo wzrok.");break;
    }
}
void
nienajlepszy(object wykonujacy)
{
    switch(random(1))
    {
        case 0: command("':j^akaj^ac si^e nerwowo: B-bo z-zawo^lam stra^z!");break;
    }
} 

void
attacked_by(object wrog)
{
    if(walka==0) 
    {
        set_alarm(0.1,0.0,"command","krzyknij Pomocy!");
        set_alarm(10.0, 0.0, "czy_walka", 1);
        walka = 1;
        return ::attacked_by(wrog); 
    }
    else
    {
        walka = 1;
        return ::attacked_by(wrog);
    }
}

int
czy_walka()
{
    if(!query_attack())
    {
        command("powiedz Po walce.");
        walka = 0;
        return 1;
    }
    else
    {
       set_alarm(5.0, 0.0, "czy_walka", 1);
    }

}