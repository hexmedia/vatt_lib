/* Autor: 
   Opis : 
   Data : */

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"
#include <pogoda.h>

inherit PIANA_STD;

void create_ulica() 
{
    set_short("Rynek");
    add_exit(PIANA_LOKACJE_ULICE + "u02","w",0,1,0);
    add_exit(PIANA_LOKACJE_ULICE + "u04","n",0,1,0);
    add_exit(PIANA_LOKACJE_ULICE + "u05","s",0,1,0);
    add_exit(PIANA_LOKACJE_ULICE + "u07","e",0,1,0);

    add_prop(ROOM_I_INSIDE,0);
    add_object(PIANA_PRZEDMIOTY + "lampa_uliczna");
}

public string
exits_description() 
{
    return "Ulica biegnie st^ad na zach^od, p^o^lnoc, wsch^od i po^ludnie.\n";
}

string
dlugi_opis()
{
    string str;
    if(jest_dzien())
    {
        str = "Rynek za dnia.";
    }
    else
    {
        str = "Rynek noc^a, centrum z^la..";
    }
    str += "\n";
    return str;
}
