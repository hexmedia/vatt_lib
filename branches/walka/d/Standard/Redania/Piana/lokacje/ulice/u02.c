/* Autor: 
   Opis : 
   Data : */

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"
#include <pogoda.h>

inherit PIANA_STD;

void create_ulica() 
{
    set_short("Ulica");
    add_exit(PIANA_LOKACJE_ULICE + "u01","w",0,1,0);
    add_exit(PIANA_LOKACJE + "rynek","e",0,1,0);

    add_prop(ROOM_I_INSIDE,0);
    add_object(PIANA_PRZEDMIOTY + "lampa_uliczna");
}

public string
exits_description() 
{
    return "Ulica ci^agnie si^e z zachodu na wsch^od, w stron^e rynku, "+
        "za^s na p^o^lnocy dostrzec mo^zna <duzy dom - chodzi o "+
        "burmistrza, jak bedzie wyglad domu to sie dokonczy. ;)>.\n";
}

string
dlugi_opis()
{
    string str;
    if(jest_dzien())
    {
        str = "Ulica za dnia.";
    }
    else
    {
        str = "Ulica noc^a, z^lo jest wsz^edzie.";
    }
    str += "\n";
    return str;
}
