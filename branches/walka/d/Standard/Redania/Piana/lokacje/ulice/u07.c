/* Autor: 
   Opis : 
   Data : */

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"
#include <pogoda.h>

inherit PIANA_STD;

void create_ulica() 
{
    set_short("Ulica");
    add_exit(PIANA_LOKACJE_ULICE + "u08","e",0,1,0);
    add_exit(PIANA_LOKACJE + "rynek","w",0,1,0);
    //karczma

    add_prop(ROOM_I_INSIDE,0);
    add_object(PIANA_PRZEDMIOTY + "lampa_uliczna");
}

public string
exits_description() 
{
    return "Ulica ci^agnie si^e ze wschodu na zach^od, w stron^e rynku, "+
        "za^s na p^o^lnocy znajduje si^e wej^scie do karczmy.\n";
}

string
dlugi_opis()
{
    string str;
    if(jest_dzien())
    {
        str = "Ulica za dnia.";
    }
    else
    {
        str = "Ulica noc^a, z^lo jest wsz^edzie.";
    }
    str += "\n";
    return str;
}
