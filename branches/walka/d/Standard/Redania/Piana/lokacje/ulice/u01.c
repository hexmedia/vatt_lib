/* Autor: Sed
   Opis : vortak z poprawkami Faeve
   Data : 09.08.2007 */

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"
#include <pogoda.h>

inherit PIANA_STD;
#define DEF_DIRS ({"zach^od"})

void create_ulica() 
{
    set_short("Ulica");

    add_exit(PIANA_LOKACJE_ULICE + "u02","e",0,1,0);
    add_exit(PIANA_LOKACJE_ULICE + "u03","se",0,1,0);
    add_exit(PIANA_LOKACJE_BRZEG + "b01","s",0,1,0);

    add_prop(ROOM_I_INSIDE,0);
    add_object(PIANA_PRZEDMIOTY + "lampa_uliczna");
}

public string
exits_description() 
{
    return "Uliczka ciagnie si^e na wsch^od i po^ludniowy wsch^od, na "+
        "zachodzie wida^c most, za^s na po^ludniu widoczne jest zej^scie "+
        "na brzeg Pontaru.\n";
}

int unq_no_move(string str)
{
    ::unq_no_move(str);

    if (member_array(query_verb(), DEF_DIRS) != -1)
        notify_fail("Most jest w remoncie, nie mo^zesz po nim przej^s^c.\n");
    return 0;
}

string
dlugi_opis()
{
    string str;
    if(jest_dzien())
    {
        str = "Niezbyt szeroka, wybrukowana przed laty ulica wiedzie od "+
            "spinaj^acego brzegi Pontaru drewnianego mostu do "+
            "rozci^agaj^acego si^e na wschodzie niewielkiego miasteczka. "+
            "Szeroka w tym miejscu rzeka z wyra^xnie odcinaj^acym si^e od "+
            "miejskich odg^los^ow szumem toczy swe wody na p^o^lnoc, ku "+
            "niezbyt ju^z odleg^lej delcie gdzie^s w okolicach Novigradu. "+
            "Kilka krok^ow na po^ludnie brzeg rzeki zosta^l wzmocniony "+
            "d^lugimi, pociemnia^lymi ju^z od wody deskami. Id^ac za^s na "+
            "wsch^od zatopisz si^e w t^lum mieszka^nc^ow, pokrzykuj^acych "+
            "przekupek i zachwalaj^acych sw^oj towar handlarzy. Nad gwarnym "+
            "miasteczkiem wyra^xnie g^oruje widoczny na wschodzie, "+
            "zdecydowanie wi^ekszy od pozosta^lych budynek - zapewne "+
            "w^la^snie tam urz^eduje burmistrz mie^sciny.";
    }
    else
    {
        str = "Niezbyt szeroka, wybrukowana przed laty ulica wiedzie od "+
            "spinaj^acego brzegi Pontaru drewnianego mostu do "+
            "rozci^agaj^acego si^e na wschodzie niewielkiego miasteczka. "+
            "Szeroka w tym miejscu rzeka z wyra^xnie odcinaj^acym si^e od "+
            "miejskich odg^los^ow szumem toczy swe wody na p^o^lnoc, ku "+
            "niezbyt ju^z odleg^lej delcie gdzie^s w okolicach Novigradu. "+
            "Kilka krok^ow na po^ludnie brzeg rzeki zosta^l wzmocniony "+
            "d^lugimi, pociemnia^lymi ju^z od wody deskami. Nad "+
            "miasteczkiem wyra^xnie g^oruje widoczny na wschodzie, "+
            "zdecydowanie wi^ekszy od pozosta^lych budynek - zapewne "+
            "w^la^snie tam urz^eduje burmistrz mie^sciny.";
    }
    str += "\n";
    return str;
}