/* Autor: Sed
   Opis : Bosch z du^zymi poprawkami Faeve
   Data : 09.08.2007 */

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"
#include <pogoda.h>

inherit PIANA_STD;

void create_ulica() 
{
    set_short("Ulica");
    add_exit(PIANA_LOKACJE_ULICE + "u01","nw",0,1,0);
    add_exit(PIANA_LOKACJE_ULICE + "u06","se",0,1,0);
    add_exit(PIANA_LOKACJE_BRZEG + "b02","sw",0,1,0);

    add_prop(ROOM_I_INSIDE,0);
    add_object(PIANA_PRZEDMIOTY + "lampa_uliczna");
}

public string
exits_description() 
{
    return "Ulica ci^agnie si^e z p^o^lnocnego zachodu na po^ludniowy "+
        "wsch^od, za^s na po^ludniowym zachodzie dostrzec mo^zna "+
        "zej^scie na brzeg Pontaru.\n";
}

string
dlugi_opis()
{
    string str;
    if(jest_dzien())
    {
        str = "Uliczka ta, do^s^c szeroka, wy^lo^zona jest starym, w wielu "+
            "miejscach pop^ekanym, a jednocze^snie wy^slizganym tysi^acami "+
            "kopyt i n^og prawie do b^lysku brukiem. Gdzieniegdzie ulica na "+
            "brzegach za^lama^la si^e lekko, a w powsta^lych "+
            "zag^l^ebieniach powsta^ly ca^lkiem g^l^ebokie ka^lu^ze "+
            "wype^lnione mieszanin^a wody i odchod^ow. Wzd^lu^z ci^agn^a "+
            "si^e barwne stragany, od kt^orych bije nieprzyjemny, rybi "+
            "sw^ad. Tu^z za nimi stoj^a niedu^ze domki, ze szczelnie "+
            "pozamykanymi oknami - tak, jakby mieszka^ncy pr^obowali "+
            "chroni^c si^e przed wszechobecnym smrodem. Jedynie od czasu "+
            "do czasu od po^ludniowego zachodu poczu^c mo^zna lekki powiew "+
            "^swie^zego powietrza znad Pontaru. ";
    }
    else
    {
        str = "Uliczka ta, do^s^c szeroka, wy^lo^zona jest starym, w wielu "+
            "miejscach pop^ekanym, a jednocze^snie wy^slizganym tysi^acami "+
            "kopyt i n^og prawie do b^lysku brukiem. Gdzieniegdzie ulica na "+
            "brzegach za^lama^la si^e lekko, a w powsta^lych "+
            "zag^l^ebieniach powsta^ly ca^lkiem g^l^ebokie ka^lu^ze "+
            "wype^lnione mieszanin^a wody i odchod^ow. Wzd^lu^z ci^agn^a "+
            "si^e ciemne szkielety stragan^ow, od kt^orych bije "+
            "nieprzyjemny, rybi sw^ad. Tu^z za nimi stoj^a niedu^ze domki, "+
            "ze szczelnie pozamykanymi oknami - tak, jakby mieszka^ncy "+
            "pr^obowali chroni^c si^e przed wszechobecnym smrodem. Jedynie "+
            "od czasu do czasu od po^ludniowego zachodu poczu^c mo^zna "+
            "lekki powiew ^swie^zego powietrza znad Pontaru. ";
    }
    str += "\n";
    return str;
}