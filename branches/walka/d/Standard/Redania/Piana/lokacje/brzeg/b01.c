/* Autor: Avard
   Opis : vortak
   Data : 5.07.07 */

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"
#include <pogoda.h>

inherit PIANA_STD_B;

void create_brzeg() 
{
    set_short("Brzeg Pontaru");
    add_exit(PIANA_LOKACJE_BRZEG + "b02.c","s",0,1,0);
    add_exit(PIANA_LOKACJE_ULICE + "u01.c","n",0,1,0);

    add_prop(ROOM_I_INSIDE,0);
}

public string
exits_description() 
{
    return "Dalsza cz^e^s^c nabrze^za znajduje si^e na po^ludniu "+
        ", za^s ulic^e mo^zna dostrzec na "+
        "p^o^lnocy.\n";
}


string
dlugi_opis()
{
	string str;
	str ="Brzeg szerokiego w tym miejscu Pontaru, tocz^acego swe wody na "+
        "p^o^lnoc, w kierunku swego uj^scia gdzie^s w okolicach Novigradu "+
        "zosta^l tu wzmocniony d^lugimi, d^ebowymi deskami. Mimo, ^ze "+
        "pociemnia^le ju^z od wody i nadszarpni^ete nieco z^ebem czasu "+
        "tworz^a ca^lkiem solidny pomost. Kawa^lek dalej, na po^ludniu "+
        "wcina si^e on dobre kilka metr^ow w g^l^ab rzeki. Jednak zdaje "+
        "si^e, ^ze i tutaj jest doskona^le miejsce do ^lowienia ryb, o "+
        "czym ^swiadczy^c mog^a widoczne w szparach mi^edzy deskami ^luski "+
        "oraz bez^ladnie porozrzucane rybie wn^etrzno^sci. Na p^o^lnocy "+
        "widzisz spinaj^acy obydwa brzegi rzeki solidny, drewniany most "+
        "przez kt^ory mo^zna dosta^c si^e z zachodniej strony do "+
        "rozci^agaj^acego si^e na wschodzie niewielkiego miasteczka. ";
	str+="\n";
	return str;
}
string
opis_nocy()
{
    string str;
	str ="Brzeg szerokiego w tym miejscu Pontaru, tocz^acego swe wody na "+
        "p^o^lnoc, w kierunku swego uj^scia gdzie^s w okolicach Novigradu "+
        "zosta^l tu wzmocniony d^lugimi, d^ebowymi deskami. Mimo, ^ze "+
        "pociemnia^le ju^z od wody i nadszarpni^ete nieco z^ebem czasu "+
        "tworz^a ca^lkiem solidny pomost. Kawa^lek dalej, na po^ludniu "+
        "wcina si^e on dobre kilka metr^ow w g^l^ab rzeki. Jednak zdaje "+
        "si^e, ^ze i tutaj jest doskona^le miejsce do ^lowienia ryb, o "+
        "czym ^swiadczy^c mog^a widoczne w szparach mi^edzy deskami ^luski "+
        "oraz bez^ladnie porozrzucane rybie wn^etrzno^sci. Na p^o^lnocy "+
        "widzisz spinaj^acy obydwa brzegi rzeki solidny, drewniany most "+
        "przez kt^ory mo^zna dosta^c si^e z zachodniej strony do "+
        "rozci^agaj^acego si^e na wschodzie niewielkiego miasteczka. ";
    str += "\n";
    return str;
}