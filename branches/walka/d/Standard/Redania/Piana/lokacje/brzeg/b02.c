/* Autor: Avard
   Opis : vortak
   Data : 5.07.07 */

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"
#include <pogoda.h>

inherit PIANA_STD_B;

void create_brzeg() 
{
    set_short("Brzeg Pontaru");
	add_exit(PIANA_LOKACJE_BRZEG + "b01.c","n",0,1,0);
    add_exit(PIANA_LOKACJE_BRZEG + "b03.c","e",0,1,0);
    add_exit(PIANA_LOKACJE_ULICE + "u03.c","ne",0,1,0);

    add_prop(ROOM_I_INSIDE,0);
}

public string
exits_description() 
{
    return "Dalsza cz^e^s^c nabrze^za znajduje si^e na p^o^lnocy "+
        "i wschodzie, za^s ulic^e mo^zna dostrzec na "+
        "p^o^lnocnym wschodzie.\n";
}


string
dlugi_opis()
{
	string str;
	if(jest_dzien())
    {
        str ="Brzeg Pontaru, p^lyn^acego z dalekiego po^ludnia na "+
            "p^o^lnoc, w kierunku swego uj^scia gdzie^s niedaleko "+
            "Novigradu, jest w miejscu w kt^orym si^e znalaz^le^s "+
            "wzmocniony ciemnymi ju^z od staro^sci deskami. Dzi^eki "+
            "pot^e^znym, d^ebowym palom wbitym w dno rzeki nabrze^ze "+
            "wcina si^e dobre kilka metr^ow w kierunku g^l^ownego nurtu. ";
        if(ZACHMURZENIE(TO) == POG_ZACH_ZEROWE || 
            ZACHMURZENIE(TO) == POG_ZACH_LEKKIE)
        {
            str += "B^lyszcz^ace w ^swietle s^lo^nca rybie ";
        }
        else
        {
            str += "Rybie ";
        }
        str += "^luski oraz porozrzucane tu i ^owdzie rybie wn^etrzno^sci "+
            "niezbicie ^swiadcz^a o tym, ^ze miejsce to cz^esto jest "+
            "odwiedzane przez mieszkaj^acych w widocznym na p^o^lnocnym "+
            "wschodzie niewielkim miasteczku. Do widocznych tam niewysokich "+
            "budynk^ow wiedzie niezbyt szeroka uliczka. Na wschodzie i "+
            "p^o^lnocy widzisz drewniane nabrze^ze, podobne do tego na "+
            "kt^orym w^lasnie si^e znajdujesz.";
    }
	else
	{
        str ="Brzeg Pontaru, p^lyn^acego z dalekiego po^ludnia na "+
            "p^o^lnoc, w kierunku swego uj^scia gdzie^s niedaleko "+
            "Novigradu, jest w miejscu w kt^orym si^e znalaz^le^s "+
            "wzmocniony ciemnymi ju^z od staro^sci deskami. Dzi^eki "+
            "pot^e^znym, d^ebowym palom wbitym w dno rzeki nabrze^ze "+
            "wcina si^e dobre kilka metr^ow w kierunku g^l^ownego nurtu. ";
        if(ZACHMURZENIE(TO) == POG_ZACH_ZEROWE || 
            ZACHMURZENIE(TO) == POG_ZACH_LEKKIE)
        {
            str += "B^lyszcz^ace w ^swietle ksi^e^zyca rybie ";
        }
        else
        {
            str += "Rybie ";
        }
        str += "^luski oraz porozrzucane tu i ^owdzie rybie wn^etrzno^sci "+
            "niezbicie ^swiadcz^a o tym, ^ze miejsce to cz^esto jest "+
            "odwiedzane przez mieszkaj^acych w widocznym na p^o^lnocnym "+
            "wschodzie niewielkim miasteczku. Do widocznych tam niewysokich "+
            "budynk^ow wiedzie niezbyt szeroka uliczka. Na wschodzie i "+
            "p^o^lnocy widzisz drewniane nabrze^ze, podobne do tego na "+
            "kt^orym w^lasnie si^e znajdujesz.";
    }
	str+="\n";
	return str;
}
string
opis_nocy()
{
    string str;
    str ="Brzeg Pontaru, p^lyn^acego z dalekiego po^ludnia na "+
        "p^o^lnoc, w kierunku swego uj^scia gdzie^s niedaleko "+
        "Novigradu, jest w miejscu w kt^orym si^e znalaz^le^s "+
        "wzmocniony ciemnymi ju^z od staro^sci deskami. Dzi^eki "+
        "pot^e^znym, d^ebowym palom wbitym w dno rzeki nabrze^ze "+
        "wcina si^e dobre kilka metr^ow w kierunku g^l^ownego nurtu. ";
    if(ZACHMURZENIE(TO) == POG_ZACH_ZEROWE || 
        ZACHMURZENIE(TO) == POG_ZACH_LEKKIE)
    {
        str += "B^lyszcz^ace w ^swietle s^lo^nca rybie ";
    }
    else
    {
        str += "Rybie ";
    }
    str += "^luski oraz porozrzucane tu i ^owdzie rybie wn^etrzno^sci "+
        "niezbicie ^swiadcz^a o tym, ^ze miejsce to cz^esto jest "+
        "odwiedzane przez mieszkaj^acych w widocznym na p^o^lnocnym "+
        "wschodzie niewielkim miasteczku. Do widocznych tam niewysokich "+
        "budynk^ow wiedzie niezbyt szeroka uliczka. Na wschodzie i "+
        "p^o^lnocy widzisz drewniane nabrze^ze, podobne do tego na "+
        "kt^orym w^lasnie si^e znajdujesz.";
    str += "\n";
    return str;
}