/* Autor: Avard
   Opis : vortak
   Data : 5.07.07 */

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"
#include <pogoda.h>

inherit PIANA_STD_B;

void create_brzeg() 
{
    set_short("Brzeg Pontaru");
    add_exit(PIANA_LOKACJE_BRZEG + "b02.c","w",0,1,0);
    add_exit(PIANA_LOKACJE_ULICE + "u06.c","e",0,1,0);

    add_prop(ROOM_I_INSIDE,0);
}

public string
exits_description() 
{
    return "Dalsza cz^e^s^c nabrze^za znajduje si^e na zachodzie "+
        ", za^s ulic^e mo^zna dostrzec na "+
        "wschodzie.\n";
}

string
dlugi_opis()
{
	string str;
	if(jest_dzien())
    {
        str ="Na po^ludniowym zachodzie hucz^a wody do^s^c szerokiego w "+
            "tych okolicach Pontaru. Pot^e^zna rzeka ^lagodnym ^lukiem "+
            "omija widoczne na p^o^lnocym wschodzie miasteczko i zmierza na "+
            "p^o^lnoc, w kierunku niedalekiego ju^z uj^scia gdzie^s w "+
            "okolicach Novigradu. Stoisz na wzmacniaj^acych piaszczyste "+
            "nabrze^ze ciemnych, d^ebowych deskach. Raptem kilka krok^ow "+
            "dalej na zachodzie, dzi^eki pot^e^znym palom pomost wcina si^e "+
            "kilka metr^ow wg^l^ab rzeki, jednak i tu zdaje si^e nierzadko "+
            "zagl^adaj^a w poszukiwaniu dobrych ^lowisk okoliczni "+
            "mieszka^ncy. Porozrzucane gdzieniegdzie rybie wn^etrzno^sci i ";
        
        if(ZACHMURZENIE(TO) == POG_ZACH_ZEROWE || 
            ZACHMURZENIE(TO) == POG_ZACH_LEKKIE)
        {
            str += "odbijaj^ace promienie s^lo^nca drobne ";
        }
        else
        {
            str += "Drobne ";
        }
        str += "^luski wymownie ^swiadcz^a o bogactwie Pontaru. Id^ac na "+
            "wsch^od dotrze^c mo^zna do wiod^acego na po^ludnie szerokiego, "+
            "dobrze utrzymanego traktu.";
    }
	else
	{
        str ="Na po^ludniowym zachodzie hucz^a wody do^s^c szerokiego w "+
            "tych okolicach Pontaru. Pot^e^zna rzeka ^lagodnym ^lukiem "+
            "omija widoczne na p^o^lnocym wschodzie miasteczko i zmierza na "+
            "p^o^lnoc, w kierunku niedalekiego ju^z uj^scia gdzie^s w "+
            "okolicach Novigradu. Stoisz na wzmacniaj^acych piaszczyste "+
            "nabrze^ze ciemnych, d^ebowych deskach. Raptem kilka krok^ow "+
            "dalej na zachodzie, dzi^eki pot^e^znym palom pomost wcina si^e "+
            "kilka metr^ow wg^l^ab rzeki, jednak i tu zdaje si^e nierzadko "+
            "zagl^adaj^a w poszukiwaniu dobrych ^lowisk okoliczni "+
            "mieszka^ncy. Porozrzucane gdzieniegdzie rybie wn^etrzno^sci i ";
        
        if(ZACHMURZENIE(TO) == POG_ZACH_ZEROWE || 
            ZACHMURZENIE(TO) == POG_ZACH_LEKKIE)
        {
            str += "odbijaj^ace ^swiat^lo ksi^e^zyca drobne ";
        }
        else
        {
            str += "Drobne ";
        }
        str += "^luski wymownie ^swiadcz^a o bogactwie Pontaru. Id^ac na "+
            "wsch^od dotrze^c mo^zna do wiod^acego na po^ludnie szerokiego, "+
            "dobrze utrzymanego traktu.";
    }
	str+="\n";
	return str;
}
string
opis_nocy()
{
    string str;
    str ="Na po^ludniowym zachodzie hucz^a wody do^s^c szerokiego w "+
        "tych okolicach Pontaru. Pot^e^zna rzeka ^lagodnym ^lukiem "+
        "omija widoczne na p^o^lnocym wschodzie miasteczko i zmierza na "+
        "p^o^lnoc, w kierunku niedalekiego ju^z uj^scia gdzie^s w "+
        "okolicach Novigradu. Stoisz na wzmacniaj^acych piaszczyste "+
        "nabrze^ze ciemnych, d^ebowych deskach. Raptem kilka krok^ow "+
        "dalej na zachodzie, dzi^eki pot^e^znym palom pomost wcina si^e "+
        "kilka metr^ow wg^l^ab rzeki, jednak i tu zdaje si^e nierzadko "+
        "zagl^adaj^a w poszukiwaniu dobrych ^lowisk okoliczni "+
        "mieszka^ncy. Porozrzucane gdzieniegdzie rybie wn^etrzno^sci i ";
        
    if(ZACHMURZENIE(TO) == POG_ZACH_ZEROWE || 
        ZACHMURZENIE(TO) == POG_ZACH_LEKKIE)
    {
        str += "odbijaj^ace ^swiat^lo ksi^e^zyca drobne ";
    }
    else
    {
        str += "Drobne ";
    }
    str += "^luski wymownie ^swiadcz^a o bogactwie Pontaru. Id^ac na "+
        "wsch^od dotrze^c mo^zna do wiod^acego na po^ludnie szerokiego, "+
        "dobrze utrzymanego traktu.";
    return str;
}