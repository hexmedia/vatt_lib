/* Autor: Avard
   Opis : vortak
   Data : 4.08.08 */

inherit "/std/room.c";

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"
#include <pogoda.h>

void 
create_room() 
{
    set_short("Niewielki miejski garnizon");
    add_prop(ROOM_I_INSIDE,1);
    add_object(PIANA_PRZEDMIOTY + "biurko_garnizon");
    add_object(PIANA_PRZEDMIOTY + "fotel_garnizon");
    add_object(PIANA_PRZEDMIOTY + "krzeslo_garnizon",2);
    add_object(PIANA_PRZEDMIOTY + "regal_garnizon");
    dodaj_rzecz_niewyswietlana("du^ze ciemnobr^azowe biurko");
    dodaj_rzecz_niewyswietlana("du^zy czerwony fotel");
    dodaj_rzecz_niewyswietlana("stabilne drewniane krzes^lo",2);
    dodaj_rzecz_niewyswietlana("prosty solidny rega^l");
    //drzwi na zewnatrz
    //do opisania jeszcze lampy i okno
}

public string
exits_description() 
{
    return "Drzwi prowadz^a na zewn^atrz.\n";
}

string
dlugi_opis()
{
    string str;
    int i, il, il_krzesel;
    object *inwentarz;
   
    inwentarz = all_inventory();
    il_krzesel = 0;
    il = sizeof(inwentarz);
   
    for (i = 0; i < il; ++i) {
         if (inwentarz[i]->jestem_krzeslem_garnizonu_w_pianie()) {
                        ++il_krzesel; }}
    str = "Jeste^s w do^s^c obszernym pomieszczeniu garnizonowym. Grube, "+
        "pot^e^zne bale z kt^orych zbudowano budynek s^a uszczelnione "+
        "g^esto poutykan^a s^lom^a, za^s niezbyt czyste, niewielkie okna "+
        "dok^ladnie zamkni^ete. Zreszt^a otworzenie ich uniemo^zliwiaj^a "+
        "umieszczone w nich masywne kraty. Wszystko to powoduje, ^ze w "+
        "stoj^acym bez ruchu powietrzu unosi si^e ostra wo^n potu zmieszana "+
        "z zapachem pal^acych si^e lamp. W^la^snie lampy stanowi^a tu "+
        "g^l^owne ^xr^od^lo ^swiat^la - przez niewielkie okienka ";
    if(jest_dzien() == 1)
    {
        str += "z trudem przedzieraj^a si^e promienie s^lo^nca. ";
    }
    else
    {
        str += "z trudem przedziera si^e ^swiat^lo ksi^e^zyca. ";
    }
    if(jest_rzecz_w_sublokacji(0,"du^ze ciemnobr^azowe biurko") || 
        jest_rzecz_w_sublokacji(0,"prosty solidny rega^l") || 
        jest_rzecz_w_sublokacji(0,"du^zy czerwony fotel") || 
        jest_rzecz_w_sublokacji(0,"stabilne drewniane krzes^lo"))
    {
        str += "Na skromne, ascetyczne wr^ecz umeblowanie tego pokoiku "+
            "sk^lada si^e ";
        if(jest_rzecz_w_sublokacji(0,"du^ze ciemnobr^azowe biurko"))
        {
            str += "stoj^ace dok^ladnie na ^srodku drewniane biurko ";
            if(jest_rzecz_w_sublokacji(0,"prosty solidny rega^l"))
            {
                str += "i z podobnego drewna zbudowany rega^l z p^o^lkami "+
                    "uginaj^acymi si^e pod ci^e^zarem le^z^acych na nich "+
                    "papierzysk. ";
                if(jest_rzecz_w_sublokacji(0,"du^zy czerwony fotel"))
                {
                    str += "Za biurkiem stoi prosty fotel";
                    if(il_krzesel == 2)
                    {
                        str += ", za^s dla interesant^ow przewidziano dwa "+
                            "zwyk^le, drewniane krzes^la";
                    }
                    if(il_krzesel == 1)
                    {
                        str += ", za^s dla interesant^ow przewidziano "+
                            "zwyk^le, drewniane krzes^lo";
                    }
                    str += ".";
                }
                else
                {
                    if(il_krzesel == 2)
                    {
                        str += "Dla interesant^ow przewidziano dwa "+
                            "zwyk^le, drewniane krzes^la.";
                    }
                    if(il_krzesel == 1)
                    {
                        str += "Dla interesant^ow przewidziano "+
                            "zwyk^le, drewniane krzes^lo.";
                    }
                }
            }
            else
            {
                if(jest_rzecz_w_sublokacji(0,"du^zy czerwony fotel") || 
                    jest_rzecz_w_sublokacji(0,"stabilne drewniane krzes^lo"))
                {
                    str += ", oraz ";
                }
                if(jest_rzecz_w_sublokacji(0,"du^zy czerwony fotel"))
                {
                    str += "prosty fotel";
                    if(il_krzesel == 2)
                    {
                        str += " i dwa zwyk^le, drewniane krzes^la "+
                            "przewidziane dla interesant^ow";
                    }
                    if(il_krzesel == 1)
                    {
                        str += " i zwyk^le, drewniane krzes^lo przewidziane "+
                            "dla interesant^ow";
                    }
                    str += ".";
                }
                else
                {
                    if(il_krzesel == 2)
                    {
                        str += "dwa zwyk^le, drewniane krzes^la "+
                            "przewidziane dla interesant^ow.";
                    }
                    if(il_krzesel == 1)
                    {
                        str += "zwyk^le, drewniane krzes^lo przewidziane "+
                            "dla interesant^ow.";
                    }
                }
            }
            
        }
        else
        {
            if(jest_rzecz_w_sublokacji(0,"prosty solidny rega^l"))
            {
                str += "drewniany rega^l z p^o^lkami uginaj^acymi si^e pod "+
                    "ci^e^zarem le^z^acych na nich papierzysk";
                if(jest_rzecz_w_sublokacji(0,"du^zy czerwony fotel") || 
                    jest_rzecz_w_sublokacji(0,"stabilne drewniane krzes^lo"))
                {
                    str += ", oraz ";
                }
                if(jest_rzecz_w_sublokacji(0,"du^zy czerwony fotel"))
                {
                    str += "prosty fotel";
                    if(il_krzesel == 2)
                    {
                        str += " i dwa zwyk^le, drewniane krzes^la "+
                            "przewidziane dla interesant^ow";
                    }
                    if(il_krzesel == 1)
                    {
                        str += " i zwyk^le, drewniane krzes^lo "+
                            "przewidziane dla interesant^ow";
                    } 
                }
                else
                {
                   if(il_krzesel == 2)
                    {
                        str += "dwa zwyk^le, drewniane krzes^la "+
                            "przewidziane dla interesant^ow";
                    }
                    if(il_krzesel == 1)
                    {
                        str += "zwyk^le, drewniane krzes^lo "+
                            "przewidziane dla interesant^ow";
                    }
                }
                str += ".";
            }
            else
            {
                if(jest_rzecz_w_sublokacji(0,"du^zy czerwony fotel") || 
                    jest_rzecz_w_sublokacji(0,"stabilne drewniane krzes^lo"))
                {
                    str += ", oraz ";
                }
                if(jest_rzecz_w_sublokacji(0,"du^zy czerwony fotel"))
                {
                    str += "prosty fotel";
                    if(il_krzesel == 2)
                    {
                        str += " i dwa zwyk^le, drewniane krzes^la "+
                            "przewidziane dla interesant^ow";
                    }
                    if(il_krzesel == 1)
                    {
                        str += " i zwyk^le, drewniane krzes^lo "+
                            "przewidziane dla interesant^ow";
                    }
                }
                else
                {
                   if(il_krzesel == 2)
                    {
                        str += "dwa zwyk^le, drewniane krzes^la "+
                            "przewidziane dla interesant^ow";
                    }
                    if(il_krzesel == 1)
                    {
                        str += "zwyk^le, drewniane krzes^lo "+
                            "przewidziane dla interesant^ow";
                    }
                }
                str += ".";
            }
        }
    }
    str += "\n";
    return str;
}
