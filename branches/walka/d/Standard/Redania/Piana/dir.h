#include "../dir.h"

#define PIANA_LOKACJE_ULICE     (PIANA_LOKACJE + "ulice/")
#define PIANA_LOKACJE_BRZEG     (PIANA_LOKACJE + "brzeg/")
#define PIANA_LOKACJE_DRZWI     (PIANA_LOKACJE + "drzwi/")
#define PIANA_NPC               (PIANA + "npc/")
#define PIANA_PRZEDMIOTY        (PIANA + "przedmioty/")
#define PIANA_UBRANIA           (PIANA_PRZEDMIOTY + "ubrania/")
#define PIANA_BRONIE            (PIANA_PRZEDMIOTY + "bronie/")
#define PIANA_ZBROJE            (PIANA_PRZEDMIOTY + "zbroje/")
#define PIANA_STD               (PIANA + "std/piana")
#define PIANA_STD_B             (PIANA + "std/piana_b")
#define PIANA_STD_NPC           (PIANA + "std/npc")