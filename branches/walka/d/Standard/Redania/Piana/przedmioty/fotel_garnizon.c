/* Autor: Avard
   Opis : vortak
   Data : 06.08.2007 */

inherit "/std/object.c";

#include <pl.h>
#include <stdproperties.h>
#include <macros.h>
#include <filter_funs.h>
#include <materialy.h>
#include <object_types.h>
#include "dir.h"

create_object()
{
    ustaw_nazwe("fotel");
                 
    dodaj_przym("du^zy","duzi");
    dodaj_przym("czerwony","czerwoni");
   

    make_me_sitable(({"w","na"}), "w fotelu", "z fotela", 1);

    set_long("Na pierwszy rzut oka sprawia wra^zenie do^s^c solidnego, tak "+
        "te^z chyba jest w rzeczywisto^sci. Siedzisko pokryte jest "+
        "mi^ekkim, niegdy^s zabarwionym krwisto czerwonym barwnikiem "+
        "materia^lem. Z biegiem czasu kolor nieco wyblak^l i przybra^l "+
        "nieco bardziej stonowany odcie^n. Oparcie jest na tyle wysokie, "+
        "^ze nawet najwi^ekszy dryblas mo^ze wygodnie si^e rozsi^a^s^c. "+
        "Ca^lo^sci dope^lniaj^a oparcia wykonane z d^ebowego drewna, "+
        "ozdobione misternymi wzorami.\n");
    add_prop(OBJ_I_WEIGHT, 5000);
    add_prop(OBJ_I_VOLUME, 5000);
    add_prop(OBJ_I_VALUE, 540);
    ustaw_material(MATERIALY_DR_DAB, 85);
    ustaw_material(MATERIALY_TK_BAWELNA, 15);
    set_type(O_MEBLE);

   // set_owners(({BIALY_MOST_NPC + "karczmarz_bialy_most"}));
}
