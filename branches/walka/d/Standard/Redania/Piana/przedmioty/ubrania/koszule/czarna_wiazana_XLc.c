/**
 * 06.08.2007
 *  
 * Autor: Daarth
 * Opis : Samaia
 */

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <pl.h>
#include <macros.h>
#include <materialy.h>
#include <object_types.h>
#include <wa_types.h>
void
create_armour()
{
	ustaw_nazwe("koszula");
	
	set_long(
    "Zwyk�a koszula z bawe�ny, zabarwiona na czarny kolor i pozbawiona "+
    "jakichkolwiek ozd�b jest idealna zar�wno dla zacnego rycerza, "+
    "mieszczanina, kupca, b�d� kogokolwiek innego stawiaj�cego raczej na "+
    "jako�� wyrobu, ni� jego zdobnienia. Jest wi�zana tu� pod szyj�, a tak�e "+
    "przy r�kawach cienkim, acz mocnym sznurkiem, z daleka zupelnie nikn�cym "+ 
    "na czerni ubrania.\n"
	);
	
	dodaj_przym("czarny","czarni");
	dodaj_przym("wi^azany","wi^azani");
	
    ustaw_material(MATERIALY_BAWELNA, 100);
    set_slots(A_TORSO, A_R_FOREARM, A_L_FOREARM, A_ARMS);
  
    add_prop(OBJ_I_WEIGHT, 300);
    add_prop(OBJ_I_VOLUME, 300);
    add_prop(OBJ_I_VALUE, 24);
    add_prop(ARMOUR_I_D_ROZCIAGLIWOSC, 40);
    add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
    add_prop(ARMOUR_I_DLA_PLCI, 0);

    set_size("L");
    set_type(O_UBRANIA);
}

