/**
 * 06.08.2007
 *  
 * Autor: Daarth
 * Opis : Samaia
 */
inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <pl.h>
#include <macros.h>
#include <materialy.h>
#include <object_types.h>

void
create_armour()
{  
	dodaj_nazwy("sukienka");

	dodaj_przym("czerwony","czerwoni");
    dodaj_przym("falbaniasty", "falbania^sci");
    
	set_long(
			"�liczna, czerwona sukieneczka pe�na kokardek i koronek zosta�a rownie� "+
			"przyozdobiona poka�nych rozmiarow plamami, a tka�e licznymi "+
			"przetarciami. D�ugie r�kawy zako�czone zosta�y wstawkami ze z�otego, "+
			"b�yszcz�cego materia�u, �ywo odcinaj�cego si� kolorystycznie od reszty."+
			" W pasie zosta�a przewi�zana szerok� ta�m�, kt�ra ginie jednak pod "+
			"stert� falbanek.\n"
	);

    ustaw_material(MATERIALY_JEDWAB, 85);
    ustaw_material(MATERIALY_SATYNA , 5);
	ustaw_material(MATERIALY_BAWELNA, 10);
 
   set_slots(A_TORSO, A_ARMS, A_LEGS, A_BODY);
   add_prop(OBJ_I_WEIGHT, 600);
   add_prop(OBJ_I_VOLUME, 600);
   add_prop(OBJ_I_VALUE, 64);
   add_prop(ARMOUR_I_D_ROZCIAGLIWOSC, 40);
   add_prop(ARMOUR_S_DLA_RASY, "czlowiek");
   add_prop(ARMOUR_I_DLA_PLCI, 1);

   set_size("S");
   set_type(O_UBRANIA);
}
  