/* Autor: Avard
   Opis : Yran
   Data : 03.12.06 */
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"

inherit TRAKT_RINDE_STD;

void create_trakt() 
{
    set_short("Trakt w lesie");
    add_exit(TRAKT_RINDE_LOKACJE + "trakt30.c","se",0,4,0);
    add_exit(TRAKT_RINDE_LOKACJE + "trakt32.c","sw",0,4,0);
    //add_exit(LAS_RINDE_LOKACJE + "las68.c","s",0,4,0);
    add_exit(LAS_RINDE_LOKACJE + "las61.c","w",0,4,0);
    //add_exit(LAS_RINDE_LOKACJE + "las60.c","e",0,4,0);

    add_prop(ROOM_I_INSIDE,0);
}
public string
exits_description() 
{
    return "Trakt prowadzi na po^ludniowy-zach^od i po^ludniowy-wsch^od.\n";
}

string
dlugi_opis()
{
    string str;
    if(jest_dzien() == 1)
    {
        if(pora_roku() != MT_ZIMA)
        {
            str = "Trakt przebiega w tym miejscu przez ciemny las. "+
                "Spl^atane ze sob^a ga^l^ezie pote^znych drzew niemal "+
                "ca^lkowicie przys^laniaj^a  niebo, lecz na szcz^e^scie "+
                "rosn^a na tyle wysoko, ^ze nie utrudniaj^a w^edr^owki. "+
                "Okolica zdaj^e si^e by^c wymarzona dla zb^oj^ow, po bokach "+
                "rosn^a liczne zaro^sle b^ed^ace znakomit^a kryj^owka dla "+
                "czekaj^acych na okazje do napadu z^loczy^nc^ow. Na "+
                "jednym z drzew przysiad^la grupa wron, kt^ora "+
                "swoim krakaniem obudzi^la do ^zycie innych mieszka^nc^ow "+
                "lasu, kt^orz^a id^ac za ich przyk^ladem zacze^ly wydawa^c "+
                "r^o^zne odg^losy. Ubite pod^lo^ze jest suche i z ka^zdym "+
                "twoim st^apnieciem po nim unosi si^e warstwa py^lu "+
                "dra^zni^aca nozdrza. ";
        }
        else
        {
            str = "Trakt przebiega w tym miejscu przez ciemny las. "+
                "Spl^atane ze sob^a bezlistne ga^l^ezie pote^znych "+
                "drzew niemal ca^lkowicie przys^laniaj^a  niebo, lecz na "+
                "szcz^e^scie rosn^a na tyle wysoko, ^ze nie utrudniaj^a "+
                "w^edr^owki. Okolica zdaj^e si^e by^c wymarzona dla "+
                "zb^oj^ow, po bokach rosn^a liczne kolczaste krzaki "+
                "b^ed^ace znakomit^a kryj^owka dla czekaj^acych na okazje "+
                "do napadu z^loczy^nc^ow. Na "+
                "jednym z drzew przysiad^la grupa wron, kt^ora swoim "+
                "krakaniem obudzi^la do ^zycie innych mieszka^nc^ow lasu, "+
                "kt^orz^a id^ac za ich przyk^ladem zacze^ly wydawa^c "+
                "r^o^zne odg^losy. Ubite pod^lo^ze pokryte jest bia^lym "+
                "puchem, na kt^orym zauwa^zasz wiele ^slad^ow zwierz^at. ";
        }
    }
    if(jest_dzien() == 0)
    {
        if(pora_roku() != MT_ZIMA)
        {
            str = "Trakt przebiega w tym miejscu przez ciemny las. "+
                "Spl^atane ze sob^a ga^l^ezie pote^znych drzew niemal "+
                "ca^lkowicie przys^laniaj^a  niebo, lecz na szcz^e^scie "+
                "rosn^a na tyle wysoko, ^ze nie utrudniaj^a w^edr^owki. "+
                "Okolica zdaj^e si^e by^c wymarzona dla zb^oj^ow, po bokach "+
                "rosn^a liczne zaro^sle b^ed^ace znakomit^a kryj^owka dla "+
                "czekaj^acych na okazje do napadu z^loczy^nc^ow. Ubite "+
                "pod^lo^ze jest suche i z ka^zdym "+
                "twoim st^apnieciem po nim unosi si^e warstwa py^lu "+
                "dra^zni^aca nozdrza. ";
        }
        else
        {
            str = "Trakt przebiega w tym miejscu przez ciemny las. "+
                "Spl^atane ze sob^a bezlistne ga^l^ezie pote^znych "+
                "drzew niemal ca^lkowicie przys^laniaj^a  niebo, lecz na "+
                "szcz^e^scie rosn^a na tyle wysoko, ^ze nie utrudniaj^a "+
                "w^edr^owki. Okolica zdaj^e si^e by^c wymarzona dla "+
                "zb^oj^ow, po bokach rosn^a liczne kolczaste krzaki "+
                "b^ed^ace znakomit^a kryj^owka dla czekaj^acych na okazje "+
                "do napadu z^loczy^nc^ow. Ubite "+
                "pod^lo^ze pokryte jest bia^lym puchem, na kt^orym "+
                "zauwa^zasz wiele ^slad^ow zwierz^at. ";
        }
    }
    str += "\n";
    return str;
}
string
opis_nocy()
{
    string str;
    if(pora_roku() != MT_ZIMA)
        {
            str = "Trakt przebiega w tym miejscu przez ciemny las. "+
                "Spl^atane ze sob^a ga^l^ezie pote^znych drzew niemal "+
                "ca^lkowicie przys^laniaj^a  niebo, lecz na szcz^e^scie "+
                "rosn^a na tyle wysoko, ^ze nie utrudniaj^a w^edr^owki. "+
                "Okolica zdaj^e si^e by^c wymarzona dla zb^oj^ow, po bokach "+
                "rosn^a liczne zaro^sle b^ed^ace znakomit^a kryj^owka dla "+
                "czekaj^acych na okazje do napadu z^loczy^nc^ow. Ubite "+
                "pod^lo^ze jest suche i z ka^zdym "+
                "twoim st^apnieciem po nim unosi si^e warstwa py^lu "+
                "dra^zni^aca nozdrza. ";
        }
        else
        {
            str = "Trakt przebiega w tym miejscu przez ciemny las. "+
                "Spl^atane ze sob^a bezlistne ga^l^ezie pote^znych "+
                "drzew niemal ca^lkowicie przys^laniaj^a  niebo, lecz na "+
                "szcz^e^scie rosn^a na tyle wysoko, ^ze nie utrudniaj^a "+
                "w^edr^owki. Okolica zdaj^e si^e by^c wymarzona dla "+
                "zb^oj^ow, po bokach rosn^a liczne kolczaste krzaki "+
                "b^ed^ace znakomit^a kryj^owka dla czekaj^acych na okazje "+
                "do napadu z^loczy^nc^ow. Ubite "+
                "pod^lo^ze pokryte jest bia^lym puchem, na kt^orym "+
                "zauwa^zasz wiele ^slad^ow zwierz^at. ";
        }
    str += "\n";
    return str;
}