/* Autor: Avard
   Opis : Tinardan
   Data : 06.10.06 */
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"

inherit TRAKT_RINDE_STD;

void create_trakt() 
{
    set_short("Trakt");
    add_exit(TRAKT_RINDE_LOKACJE + "trakt25.c","e",0,4,0);
    add_exit(TRAKT_RINDE_LOKACJE + "trakt27.c","sw",0,4,0);
    //add_exit(LAS_RINDE_LOKACJE + "las50.c","w",0,4,0);
    add_exit(LAS_RINDE_LOKACJE + "las38.c","n",0,4,0);
    add_exit(LAS_RINDE_LOKACJE + "las59.c","s",0,4,0);
    add_exit(LAS_RINDE_LOKACJE + "las58.c","se",0,4,0);

    add_prop(ROOM_I_INSIDE,0);
}
public string
exits_description() 
{
    return "Trakt prowadzi na po^ludniowy-zach^od i wsch^od.\n";
}

string
dlugi_opis()
{
    string str;
    if(jest_dzien() == 1)
    {
    if(pora_roku() != MT_ZIMA)
    {
        str = "Na p^o^lnocy wyrasta ciemna ^sciana lasu. G^esty i ciemny "+
            "zupe^lnie nie zach^eca podr^o^znych, by si^e w niego "+
            "zag^l^ebi^c. Runo le^sne jest poro^sni^ete krzewami, natomiast "+
            "g^l^ebi nie mo^zna dostrzec i nie wiadomo co si^e w g^estwinie "+
            "ukrywa. Na tle ciemnej zieleni trakt odcina si^e wyra^xnie, "+
            "ciemny, ubity i twardy, pokryty koleinami woz^ow, tysi^acem "+
            "^slad^ow ko^nskich kopyt i ludzkich st^op. Pomimo i^z droga "+
            "by^la niegdy^s specjalnie ubita i dostosowana do wygodnych "+
            "podr^o^zy, lata wykorzystywania jej spowodowa^ly, i^z pe^lno "+
            "na niej r^o^znorakich nier^owno^sci, dziur i kamieni. Na "+
            "po^ludniu wida^c niewielki pag^orek";
    }
    else
    {
        str = "Na p^o^lnocy wyrasta ciemna ^sciana lasu. G^esty i ciemny "+
            "zupe^lnie nie zach^eca podr^o^znych, by si^e w niego "+
            "zag^l^ebi^c. Runo le^sne jest poro^sni^ete krzewami, natomiast "+
            "g^l^ebi nie mo^zna dostrzec i nie wiadomo co si^e w g^estwinie "+
            "ukrywa. Na tle ciemnej zieleni trakt odcina si^e wyra^xnie, "+
            "ciemny, ubity i twardy, pokryty koleinami woz^ow, tysi^acem "+
            "^slad^ow ko^nskich kopyt i ludzkich st^op. Pomimo i^z droga "+
            "by^la niegdy^s specjalnie ubita i dostosowana do wygodnych "+
            "podr^o^zy, lata wykorzystywania jej spowodowa^ly, i^z pe^lno "+
            "na niej r^o^znorakich nier^owno^sci, dziur i kamieni. Na "+
            "po^ludniu wida^c niewielki pag^orek";
    }
    }
    if(jest_dzien() == 0)
    {
    if(pora_roku() != MT_ZIMA)
    {
        str = "Na p^o^lnocy wyrasta ciemna ^sciana lasu. G^esty i ciemny "+
            "zupe^lnie nie zach^eca podr^o^znych, by si^e w niego "+
            "zag^l^ebi^c. Runo le^sne jest poro^sni^ete krzewami, natomiast "+
            "g^l^ebi nie mo^zna dostrzec i nie wiadomo co si^e w g^estwinie "+
            "ukrywa. Na tle ciemnej zieleni trakt odcina si^e wyra^xnie, "+
            "ciemny, ubity i twardy, pokryty koleinami woz^ow, tysi^acem "+
            "^slad^ow ko^nskich kopyt i ludzkich st^op. Pomimo i^z droga "+
            "by^la niegdy^s specjalnie ubita i dostosowana do wygodnych "+
            "podr^o^zy, lata wykorzystywania jej spowodowa^ly, i^z pe^lno "+
            "na niej r^o^znorakich nier^owno^sci, dziur i kamieni. Na "+
            "po^ludniu wida^c niewielki pag^orek";
    }
    else
    {
        str = "Na p^o^lnocy wyrasta ciemna ^sciana lasu. G^esty i ciemny "+
            "zupe^lnie nie zach^eca podr^o^znych, by si^e w niego "+
            "zag^l^ebi^c. Runo le^sne jest poro^sni^ete krzewami, natomiast "+
            "g^l^ebi nie mo^zna dostrzec i nie wiadomo co si^e w g^estwinie "+
            "ukrywa. Na tle ciemnej zieleni trakt odcina si^e wyra^xnie, "+
            "ciemny, ubity i twardy, pokryty koleinami woz^ow, tysi^acem "+
            "^slad^ow ko^nskich kopyt i ludzkich st^op. Pomimo i^z droga "+
            "by^la niegdy^s specjalnie ubita i dostosowana do wygodnych "+
            "podr^o^zy, lata wykorzystywania jej spowodowa^ly, i^z pe^lno "+
            "na niej r^o^znorakich nier^owno^sci, dziur i kamieni. Na "+
            "po^ludniu wida^c niewielki pag^orek";
    }
    }
    str += "\n";
    return str;
}
opis_nocy()
{
    string str;
    if(pora_roku() != MT_ZIMA)
    {
        str = "Na p^o^lnocy wyrasta ciemna ^sciana lasu. G^esty i ciemny "+
            "zupe^lnie nie zach^eca podr^o^znych, by si^e w niego "+
            "zag^l^ebi^c. Runo le^sne jest poro^sni^ete krzewami, natomiast "+
            "g^l^ebi nie mo^zna dostrzec i nie wiadomo co si^e w g^estwinie "+
            "ukrywa. Na tle ciemnej zieleni trakt odcina si^e wyra^xnie, "+
            "ciemny, ubity i twardy, pokryty koleinami woz^ow, tysi^acem "+
            "^slad^ow ko^nskich kopyt i ludzkich st^op. Pomimo i^z droga "+
            "by^la niegdy^s specjalnie ubita i dostosowana do wygodnych "+
            "podr^o^zy, lata wykorzystywania jej spowodowa^ly, i^z pe^lno "+
            "na niej r^o^znorakich nier^owno^sci, dziur i kamieni. Na "+
            "po^ludniu wida^c niewielki pag^orek";
    }
    else
    {
        str = "Na p^o^lnocy wyrasta ciemna ^sciana lasu. G^esty i ciemny "+
            "zupe^lnie nie zach^eca podr^o^znych, by si^e w niego "+
            "zag^l^ebi^c. Runo le^sne jest poro^sni^ete krzewami, natomiast "+
            "g^l^ebi nie mo^zna dostrzec i nie wiadomo co si^e w g^estwinie "+
            "ukrywa. Na tle ciemnej zieleni trakt odcina si^e wyra^xnie, "+
            "ciemny, ubity i twardy, pokryty koleinami woz^ow, tysi^acem "+
            "^slad^ow ko^nskich kopyt i ludzkich st^op. Pomimo i^z droga "+
            "by^la niegdy^s specjalnie ubita i dostosowana do wygodnych "+
            "podr^o^zy, lata wykorzystywania jej spowodowa^ly, i^z pe^lno "+
            "na niej r^o^znorakich nier^owno^sci, dziur i kamieni. Na "+
            "po^ludniu wida^c niewielki pag^orek";
    }
    str += "\n";
    return str;
}