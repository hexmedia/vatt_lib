/* Autor: 
   Opis : 
   Data : */
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"

inherit TRAKT_RINDE_STD;
inherit "/lib/drink_water";
void create_trakt() 
{
    set_short("Trakt");
    add_exit(TRAKT_RINDE_LOKACJE + "trakt29.c","e",0,4,0);
    add_exit(TRAKT_RINDE_LOKACJE + "trakt31.c","nw",0,4,0);
   // add_exit(LAS_RINDE_LOKACJE + "las68.c","w",0,4,0);
   // add_exit(LAS_RINDE_LOKACJE + "las60.c","n",0,4,0);
    set_drink_places(({"z rzeki","z pontaru","z Pontaru"}));

    add_prop(ROOM_I_INSIDE,0);

    add_item(({"Pontar","pontar","rzek^e"}),"P^lyn^acy tu od tysi^ecy lat "+
    "szeroki Pontar zdaje si^e by^c wci^a^z nieujarzmionym i niezdobytym, "+
    "wyznacza nie tylko granice mi^edzy kr^olestwami Redanii i Temeri, "+
    "ale i skutecznie ogranicza ludzkie zap^edy w wyrywaniu z r^ak "+
    "matki Natury wszystkiego, co sie jej nale^zy. Gdzieniegdzie "+
    "pojawiaj^ace si^e z nienacka niewielkie zawirowania wody na "+
    "spokojnie, jakby leniwie poruszaj^acej si^e m^etnej tafli swiadcz^a "+
    "o nieprzywidywalno^sci i zdradzieckiej naturze nurtu, kt^ory zapewne "+
    "pochlon^a^l niejedno istnienie bezmy^slnie probuj^ace zmierzy^c si^e z "+
    "pot^eg^a rzeki.\n");
}
public string
exits_description() 
{
    return "Trakt prowadzi na p^o^lnocny-zach^od i wsch^od.\n";
}
void
init()
{
    ::init();
    init_drink_water(); 
} 

string
dlugi_opis()
{
    string str;

    if(jest_dzien())
    {
        if(pora_roku() == MT_ZIMA)
        {
    str = "Ziemia na trakcie uk�ada si� w ca�kiem spore g�rki i do�ki, "+
"niczym kocie �by na miejskiej ulicy. Z tego te� powodu droga, mimo �e "+
"szeroka - wcale nie jest wygodna. Trakt pokryty jest �niegiem, co wcale "+
"nie u�atwia w�dr�wki czy jazdy - wr�cz przeciwnie - czyni j� jeszcze mniej "+
"komfortow� i bezpieczna. O�nie�one drzewa, rosn�ce wzd�u� trasy, "+
"przys�aniaj� niemal ca�y widnokr�g. Mimo �e trakt jest do�� mocno "+
"ucz�szczany, to na jego poboczach nie brak bujnej ro�linno�ci, teraz, co "+
"prawda, pozbawionej li�ci,ale pokrytej ca�kiem poka�n� pierzynk� �niegu. "+
"Gdzie� w niedalekiej odleg�o�ci znajduje si� jaka� pot�na rzeka, gdy� "+
"pomimo du�ej ilo�ci drzew i krzew�w - s�ycha� szum przelewaj�cych si� "+
"ogromnych mas wody.";
        }
        if(pora_roku() == MT_JESIEN)
	{
str = "Ziemia na trakcie uklada si� w ca�kiem spore g�rki i do�ki, niczym "+
"kocie �by na miejskiej ulicy. Z tego te� powodu droga, mimo �e szeroka - "+
"wcale nie jest wygodna. Wok� rosn� wysokie drzewa, doszcz�tnie "+
"przys�aniaj�ce prawie ca�y widnokr�g. Mimo �e trakt jest do�� mocno "+
"ucz�szczany, to na jego poboczach nie brak bujnej ro�linno�ci, kt�ra o tej "+
"porze roku wygl�da wyj�tkowo okazale - li�cie mieni� si� r�nymi odcieniami "+
"��ci, pomara�czu i czerwieni - ca�o�� przypomina wielk�, misternie "+
"wykonan� mozaik�. Gdzie� w niedalekiej odleg�o�ci znajduje si� jaka� pot�na "+
"rzeka - z po�udnia, gdzie� zza drzew dochodzi szum przelewaj�cych si� "+
"ogromnych mas wody.";
	}
	if(pora_roku() == MT_WIOSNA)
	{
str = "Ziemia na trakcie uklada si� w ca�kiem spore g�rki i do�ki, niczym "+
"kocie �by na miejskiej ulicy. Z tego te� powodu droga, mimo �e szeroka - "+
"wcale nie jest wygodna. Wok� rosn� wysokie drzewa, doszcz�tnie "+
"przys�aniaj�ce prawie ca�y widnokr�g. Mimo �e trakt jest do�� mocno "+
"ucz�szczany, to na jego poboczach nie brak bujnej ro�linno�ci - od "+
"niewielkich krzewinek bor�wek, po krzewy dzikiej r�y czy jakie� inne, "+
"mniej znane, o ciemnych i sk�rzastych listkach, b�d� ja�niutkich, pokrytych "+
"lekkim meszkiem. Ponadto przydro�ne, �wie�o i wiosennie zielone trawy "+
"poprzetykane s� bielutkimi kwiatami zawilc�w oraz jaskrawo��tymi, male�kimi "+
"g��wkami kacze�c�w. Gdzie� w niedalekiej odleg�o�ci znajduje si� jaka� "+
"pot�na rzeka, gdy� do�� wyra�nie s�ycha� szum przelewaj�cych si� ogromnych "+
"mas wody.";
	}
	if(pora_roku() == MT_LATO)
	{
str = "Ziemia na trakcie uklada si� w ca�kiem spore g�rki i do�ki, niczym "+
"kocie �by na miejskiej ulicy. Z tego te� powodu droga, mimo �e szeroka - "+
"wcale nie jest wygodna. Wok� rosn� wysokie drzewa, doszcz�tnie "+
"przys�aniaj�ce prawie ca�y widnokr�g. Mimo �e trakt jest do�� mocno "+
"ucz�szczany, to na jego poboczach nie brak bujnej ro�linno�ci - od "+
"niewielkich krzewinek bor�wek, po krzewy dzikiej r�y czy jakie� inne, "+
"mniej znane, o ciemnych i sk�rzastych listkach, b�d� ja�niutkich, pokrytych "+
"lekkim meszkiem. Gdzie� w niedalekiej odleg�o�ci znajduje si� jaka� pot�na "+
"rzeka, gdy� pomimo du�ej ilo�ci drzew i krzew�w - s�ycha� szum "+
"przelewaj�cych si� ogromnych mas wody.";
	}
}
    else
    {
        if(pora_roku() != MT_ZIMA)
        {
    str = "Ziemia na trakcie uk�ada si� w ca�kiem spore g�rki i do�ki, "+
"niczym kocie �by na miejsciej ulicy. Z tego te� powodu droga, mimo �e "+
"szeroka - wcale nie jest wygodna, a podr�uj�c ni� o tej porze bardzo "+
"�atwo potkna� si� o jak�� nier�wno��. Wok� rosn� wysokie, ziej�ce "+
"ciemn� pustk� drzewa, otaczaj�ce trakt ze wszystkich stron. Granic� traktu "+
"wyznaczaj� rosn�ce wzd�u� niego krzewy i krzewinki, teraz ledwie widoczne "+
"na tle lasu. Gdzie� w niedalekiej odleg�o�ci znajduje si� jaka� pot�na "+
"rzeka, gdy� pomimo du�ej ilo�ci drzew i krzew�w, kt�re powinny nieco t�umi� "+
"d�wi�ki - s�ycha� szum przelewaj�cych si� ogromnych mas wody.";
        }
        else
        {
    str = "Ziemia na trakcie uk�ada si� w ca�kiem spore g�rki i do�ki, niczym"+
" kocie �by na miejsckiej ulicy. Z tego te� powodu droga, mimo �e szeroka - "+
"wcale nie jest wygodna, a podr�uj�c ni� o tej porze - bardzo �atwo potkn�� "+
"si� o jak�� nier�wno��. Ciemno�� rozja�nia nieco �nieg pokrywaj�cy wszystko "+
"doko�a. Wok� rosn� wysokie, ziej�ce ciemn� pustk� drzewa, otaczaj�ce trakt "+
"ze wszystkich stron. Dodatkow� granic� wyznaczaj� krzewy i krzewinki "+
"ci�gn�ce si� wzd�u� niego, odznaczaj�ce si� na tle lasu jasnymi czapami "+
"�niegu, pokrywaj�cymi ich ga��zki. Gdzie� w niedalekiej odleg�o�ci znajduje "+
"si� jaka� pot�na rzeka, gdy� zza drzew na po�udniu dochodzi szum "+
"przelewaj�cych si� ogromnych mas wody.";
        }
    }
    str += "\n";

    return str;
}
string
opis_nocy()
{
   string str;
   if(pora_roku() != MT_ZIMA)
   {
       str = "Ziemia na trakcie uk�ada si� w ca�kiem spore g�rki i do�ki, "+
"niczym kocie �by na miejsciej ulicy. Z tego te� powodu droga, mimo �e "+
"szeroka - wcale nie jest wygodna, a podr�uj�c ni� o tej porze bardzo "+
"�atwo potkna� si� o jak�� nier�wno��. Wok� rosn� wysokie, ziej�ce "+
"ciemn� pustk� drzewa, otaczaj�ce trakt ze wszystkich stron. Granic� traktu "+
"wyznaczaj� rosn�ce wzd�u� niego krzewy i krzewinki, teraz ledwie widoczne "+
"na tle lasu. Gdzie� w niedalekiej odleg�o�ci znajduje si� jaka� pot�na "+
"rzeka, gdy� pomimo du�ej ilo�ci drzew i krzew�w, kt�re powinny nieco t�umi� "+
"d�wi�ki - s�ycha� szum przelewaj�cych si� ogromnych mas wody.";
   }
   else
   {
       str = "Ziemia na trakcie uk�ada si� w ca�kiem spore g�rki i do�ki, niczym"+
" kocie �by na miejsckiej ulicy. Z tego te� powodu droga, mimo �e szeroka - "+
"wcale nie jest wygodna, a podr�uj�c ni� o tej porze - bardzo �atwo potkn�� "+
"si� o jak�� nier�wno��. Ciemno�� rozja�nia nieco �nieg pokrywaj�cy wszystko "+
"doko�a. Wok� rosn� wysokie, ziej�ce ciemn� pustk� drzewa, otaczaj�ce trakt "+
"ze wszystkich stron. Dodatkow� granic� wyznaczaj� krzewy i krzewinki "+
"ci�gn�ce si� wzd�u� niego, odznaczaj�ce si� na tle lasu jasnymi czapami "+
"�niegu, pokrywaj�cymi ich ga��zki. Gdzie� w niedalekiej odleg�o�ci znajduje "+
"si� jaka� pot�na rzeka, gdy� zza drzew na po�udniu dochodzi szum "+
"przelewaj�cych si� ogromnych mas wody.";
   }
   str += "\n";
   return str;
}