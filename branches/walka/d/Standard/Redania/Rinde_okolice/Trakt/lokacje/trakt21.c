/* Autor: Avard
   Opis : Tinardan
   Data : 08.11.06 */
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"
#include <pogoda.h>

inherit TRAKT_RINDE_STD;

void create_trakt() 
{
    set_short("Trakt w lesie");
    add_exit(TRAKT_RINDE_LOKACJE + "trakt20.c","e",0,4,0);
    add_exit(TRAKT_RINDE_LOKACJE + "trakt22.c","w",0,4,0);
    add_exit(LAS_RINDE_LOKACJE + "las57.c","n",0,4,0);
    add_exit(LAS_RINDE_LOKACJE + "las72.c","sw",0,4,0);
    add_exit(LAS_RINDE_LOKACJE + "las71.c","s",0,4,0);

    add_prop(ROOM_I_INSIDE,0);
}
public string
exits_description() 
{
    return "Trakt prowadzi na zach^od i wsch^od.\n";
}

string
dlugi_opis()
{
    string str;
    if(jest_dzien())
    {
        if(pora_roku() != MT_ZIMA)
        {
            str = "W tym miejscu najprawdopodobniej, jaki� zniech�cony "+
            "jednostajn� robot� budowniczy porzuci� swoj� prac� i odszed� "+
            "w nieznane, gdzie indziej szuka� szcz�cia. Na wschodzie trakt "+
            "wy�o�ony jest, zapewne niegdy� eleganckimi, dzi� ju� "+
            "zniszczonymi, kamieniami, natomiast na zachodzie ci�gnie si� "+
            "zwyczajna, ubita, szeroka droga. Niestety, trudno powiedzie� "+
            "kt�ra z decyzji budowniczego by�a lepsza - ta o porzuceniu czy "+
            "kontynuowaniu pracy. Kamienie po wielu latach u�ytkowania "+
            "wy�lizga�y si�, gdzieniegdzie ziej� pot�ne dziury, ci�gn� si� "+
            "koleiny. Na zachodzie za� ziemia jest co prawda r�wna, ale "+
            "podczas deszczu";
            if(CO_PADA(this_object()) == PADA_DESZCZ)
            {
                str += ", jak wida�,";
            }
            str += " zmienia si� po prostu w b�otnist� rzeczk�, p�yn�c� "+
            "r�wnolegle z niedalekim Pontarem. Jednak podr�nym nie wydaje "+
            "si� to zbytnio przeszkadza�, zapewne wielu z nich widzia�o ju� "+
            "gorzej zachowane drogi i �cie�ki. W ko�cu trakt jest szeroki, "+
            "podr� w miar� bezpieczna i o wiele milsze jest to, ni� "+
            "b��dzenie �r�d nieprzebytych bor�w i grz�zawisk.";
        }
        else
        {
            str = "W tym miejscu najprawdopodobniej, jaki� zniech�cony "+
            "jednostajn� robot� budowniczy porzuci� swoj� prac� i odszed� "+
            "w nieznane, gdzie indziej szuka� szcz�cia. Na wschodzie trakt "+
            "wy�o�ony jest, zapewne niegdy� eleganckimi, dzi� ju� "+
            "zniszczonymi, kamieniami, natomiast na zachodzie ci�gnie si� "+
            "zwyczajna, ubita, szeroka droga. Niestety, trudno powiedzie� "+
            "kt�ra z decyzji budowniczego by�a lepsza - ta o porzuceniu czy "+
            "kontynuowaniu pracy. Kamienie po wielu latach u�ytkowania "+
            "wy�lizga�y si�, gdzieniegdzie ziej� pot�ne dziury, ci�gn� si� "+
            "koleiny. Na zachodzie za� ziemia jest co prawda r�wna, ale "+
            "podczas deszczu";
            if(CO_PADA(this_object()) == PADA_DESZCZ)
            {
                str += ", jak wida�,";
            }
            str += " zmienia si� po prostu w b�otnist� rzeczk�, p�yn�c� "+
            "r�wnolegle z niedalekim Pontarem. Jednak podr�nym nie wydaje "+
            "si� to zbytnio przeszkadza�, zapewne wielu z nich widzia�o ju� "+
            "gorzej zachowane drogi i �cie�ki. W ko�cu trakt jest szeroki, "+
            "podr� w miar� bezpieczna i o wiele milsze jest to, ni� "+
            "b��dzenie �r�d nieprzebytych bor�w i grz�zawisk.";
        }
    }
    else
    {
        if(pora_roku() != MT_ZIMA)
        {
            str = "W tym miejscu najprawdopodobniej, jaki� zniech�cony "+
            "jednostajn� robot� budowniczy porzuci� swoj� prac� i odszed� "+
            "w nieznane, gdzie indziej szuka� szcz�cia. Na wschodzie trakt "+
            "wy�o�ony jest, zapewne niegdy� eleganckimi, dzi� ju� "+
            "zniszczonymi, kamieniami, natomiast na zachodzie ci�gnie si� "+
            "zwyczajna, ubita, szeroka droga. Niestety, trudno powiedzie� "+
            "kt�ra z decyzji budowniczego by�a lepsza - ta o porzuceniu czy "+
            "kontynuowaniu pracy. Kamienie po wielu latach u�ytkowania "+
            "wy�lizga�y si�, gdzieniegdzie ziej� pot�ne dziury, ci�gn� si� "+
            "koleiny. Na zachodzie za� ziemia jest co prawda r�wna, ale "+
            "podczas deszczu";
            if(CO_PADA(this_object()) == PADA_DESZCZ)
            {
                str += ", jak wida�,";
            }
            str += " zmienia si� po prostu w b�otnist� rzeczk�, p�yn�c� "+
            "r�wnolegle z niedalekim Pontarem. Jednak podr�nym nie wydaje "+
            "si� to zbytnio przeszkadza�, zapewne wielu z nich widzia�o ju� "+
            "gorzej zachowane drogi i �cie�ki. W ko�cu trakt jest szeroki, "+
            "podr� w miar� bezpieczna i o wiele milsze jest to, ni� "+
            "b��dzenie �r�d nieprzebytych bor�w i grz�zawisk.";
        }
        else
        {
            str = "W tym miejscu najprawdopodobniej, jaki� zniech�cony "+
            "jednostajn� robot� budowniczy porzuci� swoj� prac� i odszed� "+
            "w nieznane, gdzie indziej szuka� szcz�cia. Na wschodzie trakt "+
            "wy�o�ony jest, zapewne niegdy� eleganckimi, dzi� ju� "+
            "zniszczonymi, kamieniami, natomiast na zachodzie ci�gnie si� "+
            "zwyczajna, ubita, szeroka droga. Niestety, trudno powiedzie� "+
            "kt�ra z decyzji budowniczego by�a lepsza - ta o porzuceniu czy "+
            "kontynuowaniu pracy. Kamienie po wielu latach u�ytkowania "+
            "wy�lizga�y si�, gdzieniegdzie ziej� pot�ne dziury, ci�gn� si� "+
            "koleiny. Na zachodzie za� ziemia jest co prawda r�wna, ale "+
            "podczas deszczu";
            if(CO_PADA(this_object()) == PADA_DESZCZ)
            {
                str += ", jak wida�,";
            }
            str += " zmienia si� po prostu w b�otnist� rzeczk�, p�yn�c� "+
            "r�wnolegle z niedalekim Pontarem. Jednak podr�nym nie wydaje "+
            "si� to zbytnio przeszkadza�, zapewne wielu z nich widzia�o ju� "+
            "gorzej zachowane drogi i �cie�ki. W ko�cu trakt jest szeroki, "+
            "podr� w miar� bezpieczna i o wiele milsze jest to, ni� "+
            "b��dzenie �r�d nieprzebytych bor�w i grz�zawisk.";
        }
    }
    str += "\n";
    return str;
}
string
opis_nocy()
{
    string str;
    if(pora_roku() != MT_ZIMA)
    {
        str = "W tym miejscu najprawdopodobniej, jaki� zniech�cony "+
        "jednostajn� robot� budowniczy porzuci� swoj� prac� i odszed� "+
        "w nieznane, gdzie indziej szuka� szcz�cia. Na wschodzie trakt "+
        "wy�o�ony jest, zapewne niegdy� eleganckimi, dzi� ju� "+
        "zniszczonymi, kamieniami, natomiast na zachodzie ci�gnie si� "+
        "zwyczajna, ubita, szeroka droga. Niestety, trudno powiedzie� "+
        "kt�ra z decyzji budowniczego by�a lepsza - ta o porzuceniu czy "+
        "kontynuowaniu pracy. Kamienie po wielu latach u�ytkowania "+
        "wy�lizga�y si�, gdzieniegdzie ziej� pot�ne dziury, ci�gn� si� "+
        "koleiny. Na zachodzie za� ziemia jest co prawda r�wna, ale "+
        "podczas deszczu";
        if(CO_PADA(this_object()) == PADA_DESZCZ)
        {
            str += ", jak wida�,";
        }
        str += " zmienia si� po prostu w b�otnist� rzeczk�, p�yn�c� "+
        "r�wnolegle z niedalekim Pontarem. Jednak podr�nym nie wydaje "+
        "si� to zbytnio przeszkadza�, zapewne wielu z nich widzia�o ju� "+
        "gorzej zachowane drogi i �cie�ki. W ko�cu trakt jest szeroki, "+
        "podr� w miar� bezpieczna i o wiele milsze jest to, ni� "+
        "b��dzenie �r�d nieprzebytych bor�w i grz�zawisk.";
    }
    else
    {
        str = "W tym miejscu najprawdopodobniej, jaki� zniech�cony "+
        "jednostajn� robot� budowniczy porzuci� swoj� prac� i odszed� "+
        "w nieznane, gdzie indziej szuka� szcz�cia. Na wschodzie trakt "+
        "wy�o�ony jest, zapewne niegdy� eleganckimi, dzi� ju� "+
        "zniszczonymi, kamieniami, natomiast na zachodzie ci�gnie si� "+
        "zwyczajna, ubita, szeroka droga. Niestety, trudno powiedzie� "+
        "kt�ra z decyzji budowniczego by�a lepsza - ta o porzuceniu czy "+
        "kontynuowaniu pracy. Kamienie po wielu latach u�ytkowania "+
        "wy�lizga�y si�, gdzieniegdzie ziej� pot�ne dziury, ci�gn� si� "+
        "koleiny. Na zachodzie za� ziemia jest co prawda r�wna, ale "+
        "podczas deszczu";
        if(CO_PADA(this_object()) == PADA_DESZCZ)
        {
            str += ", jak wida�,";
        }
        str += " zmienia si� po prostu w b�otnist� rzeczk�, p�yn�c� "+
        "r�wnolegle z niedalekim Pontarem. Jednak podr�nym nie wydaje "+
        "si� to zbytnio przeszkadza�, zapewne wielu z nich widzia�o ju� "+
        "gorzej zachowane drogi i �cie�ki. W ko�cu trakt jest szeroki, "+
        "podr� w miar� bezpieczna i o wiele milsze jest to, ni� "+
        "b��dzenie �r�d nieprzebytych bor�w i grz�zawisk.";
    }
    str +="\n";
    return str;
}