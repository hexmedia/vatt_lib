/* Autor: Avard
   Opis : vortak
   Data : 08.10.06 */
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"

inherit TRAKT_RINDE_STD;
inherit "/lib/drink_water";

void create_trakt() 
{
    set_short("Trakt wzd^lu^z rzeki.");
    add_exit(TRAKT_RINDE_LOKACJE + "trakt16.c","n",0,4,0);
    add_exit(TRAKT_RINDE_LOKACJE + "trakt18.c","sw",0,4,0);
    add_exit(LAS_RINDE_LOKACJE + "wlas17.c","nw",0,4,0);
    add_exit(LAS_RINDE_LOKACJE + "las45.c","w",0,4,0);
    set_drink_places(({"z rzeki","z pontaru","z Pontaru"}));
    add_sit(({"nad rzek^a","nad pontarem","nad Pontarem"}),
        "nad rzek^a","z nad rzeki",0);
    add_prop(ROOM_I_INSIDE,0);
    add_item(({"Pontar","pontar","rzek^e"}),"P^lyn^acy tu od tysi^ecy lat "+
    "szeroki Pontar zdaje si^e by^c wci^a^z nieujarzmionym i niezdobytym, "+
    "wyznacza nie tylko granice mi^edzy kr^olestwami Redanii i Temeri, "+
    "ale i skutecznie ogranicza ludzkie zap^edy w wyrywaniu z r^ak "+
    "matki Natury wszystkiego, co sie jej nale^zy. Gdzieniegdzie "+
    "pojawiaj^ace si^e z nienacka niewielkie zawirowania wody na "+
    "spokojnie, jakby leniwie poruszaj^acej si^e m^etnej tafli swiadcz^a "+
    "o nieprzywidywalno^sci i zdradzieckiej naturze nurtu, kt^ory zapewne "+
    "pochlon^a^l niejedno istnienie bezmy^slnie probuj^ace zmierzy^c si^e z "+
    "pot^eg^a rzeki.\n");
}
public string
exits_description() 
{
    return "Trakt prowadzi na p^o^lnoc i po^ludniowy-zach^od.\n";
}
void
init()
{
    ::init();
    init_drink_water(); 
} 

string
dlugi_opis()
{
    string str;
    if(jest_dzien() == 1)
    {
    if(pora_roku() != MT_ZIMA)
    {
        str = "Trakt ci^agn^acy si^e wzd^lu^z p^o^lnocnego brzegu Pontaru "+
            "nieznacznie zmienia sw^oj kierunek. Mo^zesz si^e uda^c na "+
            "po^ludniowy zach^od b^ad^x na p^o^lnoc. Nigdy nie remontowana "+
            "nawierzchnia jest w tym miejscu wyj^atkowo mocno zniszczona. "+
            "Zdaje si^e, ^ze trasa zosta^la wytyczona zbyt blisko rzeki i "+
            "podczas wiosennych roztop^ow jest zalewana przez wody Pontaru. "+
            "Z kamieni, kt^ore niegdy^s u^lo^zono na ubitej ziemi prawie "+
            "nic nie pozosta^lo. G^l^ebokie b^loto rozdeptane przez "+
            "licznych podr^o^znych rozci^aga si^e na ca^lej szeroko^sci "+
            "traktu. Przebrni^ecie przez nie stanowi^c musi nie lada "+
            "wyzwanie dla ci^e^zkich, za^ladowanych po brzegi woz^ow "+
            "kupieckich a i piechur musi uwa^za^c, ^zeby nie wpa^s^c w "+
            "g^lebokie ka^lu^ze. Na zachodzie rozci^aga si^e niezbyt "+
            "g^esty w tym miejscu bukowy lasek.";
    }
    else
    {
        str = "Trakt ci^agn^acy si^e wzd^lu^z p^o^lnocnego brzegu Pontaru "+
            "nieznacznie zmienia sw^oj kierunek. Mo^zesz si^e uda^c na "+
            "po^ludniowy zach^od b^ad^x na p^o^lnoc. Nigdy nie remontowana "+
            "nawierzchnia jest w tym miejscu wyj^atkowo mocno zniszczona. "+
            "Zdaje si^e, ^ze trasa zosta^la wytyczona zbyt blisko rzeki i "+
            "podczas wiosennych roztop^ow jest zalewana przez wody Pontaru. "+
            "Z kamieni, kt^ore niegdy^s u^lo^zono na ubitej ziemi prawie "+
            "nic nie pozosta^lo. Na szcz^e^scie teraz b^loto rozci^agaj^ace "+
            "si^e na ca^lej szeroko^sci traktu jest zamar^xni^ete. Jednak "+
            "latem przebrni^ecie przez nie stanowi^c musi nie lada wyzwanie "+
            "dla ci^e^zkich, za^ladowanych po brzegi woz^ow kupieckich. Na "+
            "zachodzie rozci^aga si^e niezbyt g^esty w tym miejscu bukowy "+
            "lasek. Korony drzew przykryte s^a grub^a warstw^a bia^lego "+
            "puchu.";
    }
    }
    if(jest_dzien() == 0)
    {
    if(pora_roku() != MT_ZIMA)
    {
        str = "Trakt ci^agn^acy si^e wzd^lu^z p^o^lnocnego brzegu Pontaru "+
            "nieznacznie zmienia sw^oj kierunek. Mo^zesz si^e uda^c na "+
            "po^ludniowy zach^od b^ad^x na p^o^lnoc. Nigdy nie remontowana "+
            "nawierzchnia jest w tym miejscu wyj^atkowo mocno zniszczona. "+
            "Zdaje si^e, ^ze trasa zosta^la wytyczona zbyt blisko rzeki i "+
            "podczas wiosennych roztop^ow jest zalewana przez wody Pontaru. "+
            "Z kamieni, kt^ore niegdy^s u^lo^zono na ubitej ziemi prawie "+
            "nic nie pozosta^lo. G^l^ebokie b^loto rozdeptane przez "+
            "licznych podr^o^znych rozci^aga si^e na ca^lej szeroko^sci "+
            "traktu. Przebrni^ecie przez nie stanowi^c musi nie lada "+
            "wyzwanie dla ci^e^zkich, za^ladowanych po brzegi woz^ow "+
            "kupieckich a i piechur musi uwa^za^c, ^zeby nie wpa^s^c w "+
            "g^lebokie ka^lu^ze. Na zachodzie rozci^aga si^e niezbyt "+
            "g^esty w tym miejscu bukowy lasek.";
    }
    else
    {
        str = "Trakt ci^agn^acy si^e wzd^lu^z p^o^lnocnego brzegu Pontaru "+
            "nieznacznie zmienia sw^oj kierunek. Mo^zesz si^e uda^c na "+
            "po^ludniowy zach^od b^ad^x na p^o^lnoc. Nigdy nie remontowana "+
            "nawierzchnia jest w tym miejscu wyj^atkowo mocno zniszczona. "+
            "Zdaje si^e, ^ze trasa zosta^la wytyczona zbyt blisko rzeki i "+
            "podczas wiosennych roztop^ow jest zalewana przez wody Pontaru. "+
            "Z kamieni, kt^ore niegdy^s u^lo^zono na ubitej ziemi prawie "+
            "nic nie pozosta^lo. Na szcz^e^scie teraz b^loto rozci^agaj^ace "+
            "si^e na ca^lej szeroko^sci traktu jest zamar^xni^ete. Jednak "+
            "latem przebrni^ecie przez nie stanowi^c musi nie lada wyzwanie "+
            "dla ci^e^zkich, za^ladowanych po brzegi woz^ow kupieckich. Na "+
            "zachodzie rozci^aga si^e niezbyt g^esty w tym miejscu bukowy "+
            "lasek. Korony drzew przykryte s^a grub^a warstw^a bia^lego "+
            "puchu.";
    }
    }
    str += "\n";
    return str;
}
string
opis_nocy()
{
    string str;
    if(pora_roku() != MT_ZIMA)
    {
        str = "Trakt ci^agn^acy si^e wzd^lu^z p^o^lnocnego brzegu Pontaru "+
            "nieznacznie zmienia sw^oj kierunek. Mo^zesz si^e uda^c na "+
            "po^ludniowy zach^od b^ad^x na p^o^lnoc. Nigdy nie remontowana "+
            "nawierzchnia jest w tym miejscu wyj^atkowo mocno zniszczona. "+
            "Zdaje si^e, ^ze trasa zosta^la wytyczona zbyt blisko rzeki i "+
            "podczas wiosennych roztop^ow jest zalewana przez wody Pontaru. "+
            "Z kamieni, kt^ore niegdy^s u^lo^zono na ubitej ziemi prawie "+
            "nic nie pozosta^lo. G^l^ebokie b^loto rozdeptane przez "+
            "licznych podr^o^znych rozci^aga si^e na ca^lej szeroko^sci "+
            "traktu. Przebrni^ecie przez nie stanowi^c musi nie lada "+
            "wyzwanie dla ci^e^zkich, za^ladowanych po brzegi woz^ow "+
            "kupieckich a i piechur musi uwa^za^c, ^zeby nie wpa^s^c w "+
            "g^lebokie ka^lu^ze. Na zachodzie rozci^aga si^e niezbyt "+
            "g^esty w tym miejscu bukowy lasek.";
    }
    else
    {
        str = "Trakt ci^agn^acy si^e wzd^lu^z p^o^lnocnego brzegu Pontaru "+
            "nieznacznie zmienia sw^oj kierunek. Mo^zesz si^e uda^c na "+
            "po^ludniowy zach^od b^ad^x na p^o^lnoc. Nigdy nie remontowana "+
            "nawierzchnia jest w tym miejscu wyj^atkowo mocno zniszczona. "+
            "Zdaje si^e, ^ze trasa zosta^la wytyczona zbyt blisko rzeki i "+
            "podczas wiosennych roztop^ow jest zalewana przez wody Pontaru. "+
            "Z kamieni, kt^ore niegdy^s u^lo^zono na ubitej ziemi prawie "+
            "nic nie pozosta^lo. Na szcz^e^scie teraz b^loto rozci^agaj^ace "+
            "si^e na ca^lej szeroko^sci traktu jest zamar^xni^ete. Jednak "+
            "latem przebrni^ecie przez nie stanowi^c musi nie lada wyzwanie "+
            "dla ci^e^zkich, za^ladowanych po brzegi woz^ow kupieckich. Na "+
            "zachodzie rozci^aga si^e niezbyt g^esty w tym miejscu bukowy "+
            "lasek. Korony drzew przykryte s^a grub^a warstw^a bia^lego "+
            "puchu.";
    }
    str += "\n";
    return str;
}