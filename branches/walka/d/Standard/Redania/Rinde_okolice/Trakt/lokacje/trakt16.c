/* Autor: Avard
   Opis : vortak
   Data : 07.10.06 */
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"

#define DEF_DIRS ({"po^ludnie"})

inherit TRAKT_RINDE_STD;
inherit "/lib/drink_water";

void create_trakt() 
{
    set_short("Szeroki trakt");
    add_exit(TRAKT_RINDE_LOKACJE + "trakt15.c","ne",0,4,0);
    add_exit(LAS_RINDE_LOKACJE + "wlas16.c","n",0,4,0);
    add_exit(LAS_RINDE_LOKACJE + "tartak.c","nw",0,4,0);
    add_exit(LAS_RINDE_LOKACJE + "wlas17.c","w",0,4,0);
    add_exit(LAS_RINDE_LOKACJE + "las45.c","sw",0,4,0);
    if(pora_roku() != MT_WIOSNA)
    {
        add_exit(TRAKT_RINDE_LOKACJE + "trakt17.c","s",0,4,0);
    }

    set_drink_places(({"z rzeki","z pontaru","z Pontaru"}));
    add_sit(({"nad rzek^a","nad pontarem","nad Pontarem"}),
        "nad rzek^a","z nad rzeki",0);

    add_prop(ROOM_I_INSIDE,0);
    add_event("@@pora_rok:"+file_name(TO)+"@@");
    add_item(({"^scie^zk^e","le^sn^a ^scie^zk^e",
        "wydeptan^a ^scie^zk^e"}),"@@sciezka@@");
    add_item(({"rozlewisko","rozlewisko Pontaru",
        "rozlewisko pontaru","rozlewisko na po^ludniu"}),"@@rozlewisko@@");
    add_item(({"Pontar","pontar","rzek^e"}),"P^lyn^acy tu od tysi^ecy lat "+
    "szeroki Pontar zdaje si^e by^c wci^a^z nieujarzmionym i niezdobytym, "+
    "wyznacza nie tylko granice mi^edzy kr^olestwami Redanii i Temeri, "+
    "ale i skutecznie ogranicza ludzkie zap^edy w wyrywaniu z r^ak "+
    "matki Natury wszystkiego, co sie jej nale^zy. Gdzieniegdzie "+
    "pojawiaj^ace si^e z nienacka niewielkie zawirowania wody na "+
    "spokojnie, jakby leniwie poruszaj^acej si^e m^etnej tafli swiadcz^a "+
    "o nieprzywidywalno^sci i zdradzieckiej naturze nurtu, kt^ory zapewne "+
    "pochlon^a^l niejedno istnienie bezmy^slnie probuj^ace zmierzy^c si^e z "+
    "pot^eg^a rzeki.\n");

}
void
init()
{
    ::init();
    init_drink_water(); 
} 
int unq_no_move(string str)
{
    ::unq_no_move(str);

    if (member_array(query_verb(), DEF_DIRS) != -1 && 
        pora_roku() == MT_WIOSNA)
        notify_fail("Idziesz na "+ (query_verb() ~=
        "g^ora" ? "g^or^e" : query_verb()) +", jednak tam rozci^aga si^e "+
        "szerokie rozlewisko Pontaru. Po chwili zrezygnowan"
	+TP->koncowka("y", "a")+" zawracasz.\n");
    return 0;
}
public string
exits_description() 
{
    if(jest_dzien() == 1)
    {
        return "Trakt prowadzi na po^ludnie i p^o^lnocny-wsch^od, za^s na "+
        "po^ludniowym-zachodzie wida^c le^sn^a ^scie^zk^e.\n";
    }
    if(jest_dzien() == 0)
    {
        return "Trakt prowadzi na po^ludnie i p^o^lnocny-wsch^od.\n";
    }
}

string
dlugi_opis()
{
    string str;
    if(jest_dzien() == 1)
    {
    if(pora_roku() != MT_ZIMA)
    {
        str = "Trakt wij^acy si^e nad brzegiem Pontaru jest na tyle "+
            "szeroki, i^z bez najmniejszego problemu wymin^a^c si^e na "+
            "nim mog^a nie tylko piechurzy i konni ale nawet wi^eksze "+
            "wozy. Drog^e wy^lo^zono g^ladkimi kamieniami, niestety "+
            "nawierzchnia od dawna ju^z nie by^la remontowana, tak^ze "+
            "podr^oz do najprzyjemniejszych nie nale^zy. Ko^la zaprz^eg^ow, "+
            "kt^ore od lat t^edy przeje^zd^za^ly wy^z^lobi^ly w pod^lo^zu "+
            "g^l^ebokie koleiny wype^lnione mieszanin^a ^smieci, wody, "+
            "ko^nskich odchod^ow i nie wiadomo czego jeszcze. Niekt^ore "+
            "kamienie z biegiem czasu pop^eka^ly, inne zosta^ly wyrwane ze "+
            "swojego pierwotnego miejsca. P^o^lnocny i zachodni skraj duktu "+
            "poro^sni^ety jest rachitycznymi krzakami zza kt^orych wida^c "+
            "zabudowania tartaku. Na po^ludniowym zachodzie widzisz niezbyt "+
            "g^esty bukowy las do kt^orego prowadzi s^labo widoczna "+
            "^scie^zka. Z po^ludnia dociera do ciebie uspokajaj^acy szum "+
            "ukrytego za krzakami Pontaru.";
    }
    else
    {
        str = "Trakt wij^acy si^e nad brzegiem Pontaru jest na tyle "+
            "szeroki, i^z bez najmniejszego problemu wymin^a^c si^e na nim "+
            "mog^a nie tylko piechurzy i konni ale nawet wi^eksze wozy. "+
            "Drog^e wy^lo^zono g^ladkimi kamieniami, niestety nawierzchnia "+
            "od dawna ju^z nie by^la remontowana, tak^ze podr^oz do "+
            "najprzyjemniejszych nie nale^zy. Ko^la zaprz^eg^ow, kt^ore od "+
            "lat t^edy przeje^zd^za^ly wy^z^lobi^ly w pod^lo^zu g^l^ebokie "+
            "koleiny wype^lnione zmar^xni^et^a brej^a. Niekt^ore kamienie z "+
            "biegiem czasu pop^eka^ly, inne zosta^l wyrwane ze swojego "+
            "pierwotnego miejsca. P^o^lnocny i zachodni skraj duktu "+
            "poro^sni^ety jest zasypanymi bia^lym puchem rachitycznymi "+
            "krzakami, zza kt^orych wida^c zabudowania tartaku. Na "+
            "po^ludniowym zachodzie widzisz niezbyt g^esty bukowy las, do "+
            "kt^orego prowadzi ledwie widoczna ^scie^zka. Z po^ludnia "+
            "dociera do ciebie potrzasiwnie lodu, kt^ory unieruchomi^l "+
            "wody Pontaru.";
    }
    }
    if(jest_dzien() == 0)
    {
    if(pora_roku() != MT_ZIMA)
    {
        str = "Trakt wij^acy si^e nad brzegiem Pontaru jest na tyle "+
            "szeroki, i^z bez najmniejszego problemu wymin^a^c si^e na "+
            "nim mog^a nie tylko piechurzy i konni ale nawet wi^eksze "+
            "wozy. Drog^e wy^lo^zono g^ladkimi kamieniami, niestety "+
            "nawierzchnia od dawna ju^z nie by^la remontowana, tak^ze "+
            "podr^oz do najprzyjemniejszych nie nale^zy. Ko^la zaprz^eg^ow, "+
            "kt^ore od lat t^edy przeje^zd^za^ly wy^z^lobi^ly w pod^lo^zu "+
            "g^l^ebokie koleiny wype^lnione mieszanin^a ^smieci, wody, "+
            "ko^nskich odchod^ow i nie wiadomo czego jeszcze. Niekt^ore "+
            "kamienie z biegiem czasu pop^eka^ly, inne zosta^ly wyrwane ze "+
            "swojego pierwotnego miejsca. P^o^lnocny i zachodni skraj duktu "+
            "poro^sni^ety jest rachitycznymi krzakami zza kt^orych wida^c "+
            "zabudowania tartaku. Na po^ludniowym zachodzie widzisz niezbyt "+
            "g^esty bukowy las. Z po^ludnia dociera do ciebie uspokajaj^acy "+
            "szum ukrytego za krzakami Pontaru.";
    }
    else
    {
        str = "Trakt wij^acy si^e nad brzegiem Pontaru jest na tyle "+
            "szeroki, i^z bez najmniejszego problemu wymin^a^c si^e na nim "+
            "mog^a nie tylko piechurzy i konni ale nawet wi^eksze wozy. "+
            "Drog^e wy^lo^zono g^ladkimi kamieniami, niestety nawierzchnia "+
            "od dawna ju^z nie by^la remontowana, tak^ze podr^oz do "+
            "najprzyjemniejszych nie nale^zy. Ko^la zaprz^eg^ow, kt^ore od "+
            "lat t^edy przeje^zd^za^ly wy^z^lobi^ly w pod^lo^zu g^l^ebokie "+
            "koleiny wype^lnione zmar^xni^et^a brej^a. Niekt^ore kamienie z "+
            "biegiem czasu pop^eka^ly, inne zosta^l wyrwane ze swojego "+
            "pierwotnego miejsca. P^o^lnocny i zachodni skraj duktu "+
            "poro^sni^ety jest zasypanymi bia^lym puchem rachitycznymi "+
            "krzakami, zza kt^orych wida^c zabudowania tartaku. Na "+
            "po^ludniowym zachodzie widzisz niezbyt g^esty bukowy las. Z "+
            "po^ludnia dociera do ciebie potrzasiwnie lodu, kt^ory "+
            "unieruchomi^l wody Pontaru.";
    }
    }
    str += "\n";
    return str;
}
string
opis_nocy()
{
    string str;
    if(pora_roku() != MT_ZIMA)
    {
        str = "Trakt wij^acy si^e nad brzegiem Pontaru jest na tyle "+
            "szeroki, i^z bez najmniejszego problemu wymin^a^c si^e na "+
            "nim mog^a nie tylko piechurzy i konni ale nawet wi^eksze "+
            "wozy. Drog^e wy^lo^zono g^ladkimi kamieniami, niestety "+
            "nawierzchnia od dawna ju^z nie by^la remontowana, tak^ze "+
            "podr^oz do najprzyjemniejszych nie nale^zy. Ko^la zaprz^eg^ow, "+
            "kt^ore od lat t^edy przeje^zd^za^ly wy^z^lobi^ly w pod^lo^zu "+
            "g^l^ebokie koleiny wype^lnione mieszanin^a ^smieci, wody, "+
            "ko^nskich odchod^ow i nie wiadomo czego jeszcze. Niekt^ore "+
            "kamienie z biegiem czasu pop^eka^ly, inne zosta^ly wyrwane ze "+
            "swojego pierwotnego miejsca. P^o^lnocny i zachodni skraj duktu "+
            "poro^sni^ety jest rachitycznymi krzakami zza kt^orych wida^c "+
            "zabudowania tartaku. Na po^ludniowym zachodzie widzisz niezbyt "+
            "g^esty bukowy las. Z po^ludnia dociera do ciebie uspokajaj^acy "+
            "szum ukrytego za krzakami Pontaru.";
    }
    else
    {
        str = "Trakt wij^acy si^e nad brzegiem Pontaru jest na tyle "+
            "szeroki, i^z bez najmniejszego problemu wymin^a^c si^e na nim "+
            "mog^a nie tylko piechurzy i konni ale nawet wi^eksze wozy. "+
            "Drog^e wy^lo^zono g^ladkimi kamieniami, niestety nawierzchnia "+
            "od dawna ju^z nie by^la remontowana, tak^ze podr^oz do "+
            "najprzyjemniejszych nie nale^zy. Ko^la zaprz^eg^ow, kt^ore od "+
            "lat t^edy przeje^zd^za^ly wy^z^lobi^ly w pod^lo^zu g^l^ebokie "+
            "koleiny wype^lnione zmar^xni^et^a brej^a. Niekt^ore kamienie z "+
            "biegiem czasu pop^eka^ly, inne zosta^l wyrwane ze swojego "+
            "pierwotnego miejsca. P^o^lnocny i zachodni skraj duktu "+
            "poro^sni^ety jest zasypanymi bia^lym puchem rachitycznymi "+
            "krzakami, zza kt^orych wida^c zabudowania tartaku. Na "+
            "po^ludniowym zachodzie widzisz niezbyt g^esty bukowy las. Z "+
            "po^ludnia dociera do ciebie potrzasiwnie lodu, kt^ory "+
            "unieruchomi^l wody Pontaru.";
    }
    str += "\n";
    return str;
}
string
sciezka()
{
    if(jest_dzien() == 1)
    {
        if(pora_roku() != MT_ZIMA)
        {
            return ("Wydeptana w trawie, ledwo teraz widoczna ^scie^zka. Na "+
                "tyle szeroka, ^ze mo^zna by si^e pokusi^c, by przejecha^c "+
                "t^edy wozem.\n");
        }
        else
        {
            return ("Pod warstw^a ^sniegu znajdujesz ^scie^zk^e. Na tyle "+
                "szerok^a, ^ze mo^zna by si^e pokusi^c o przejechanie t^edy "+
                "wozem.\n");
        }
    }
    if(jest_dzien() == 0)
    {
        return ("Nie zauwa^zasz niczego takiego.\n");
    }
}
string
rozlewisko()
{
    if(pora_roku() == MT_WIOSNA)
        return ("Wydaje si^e, ^ze trakt zosta^l wytyczony zbyt blisko "+
        "rzeki. Wraz z wiosennymi roztopami, gdy poziom wody w rzece "+
        "wzr^os^l trakt zosta^l zalany. Widzisz jedynie g^ladk^a "+
        "powierzchni^e wody i wystaj^ace ponad ni^a k^epy krzak^ow.\n");
    if(pora_roku() != MT_WIOSNA)
        return ("Nie zauwa^zasz niczego takiego.\n");
}
string
pora_rok()
{
    switch (pora_roku())
    {
         case MT_LATO:
         case MT_WIOSNA:
             return ({"Pontar szumi koj^aco.\n","Pontar szumi cichutko.\n",
             "^Spiew ptak^ow ucich^l na chwil^e.\n"})
             [random(2)];
         case MT_JESIEN:
             return ({"Gdzie^s niedaleko s^lycha^c smutne krakanie wron.\n"})
             [random(1)];
    }
}
