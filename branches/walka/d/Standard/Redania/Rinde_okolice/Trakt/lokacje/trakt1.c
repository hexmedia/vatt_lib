/* Autor: 
   Opis : 
   Data : */
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"

inherit TRAKT_RINDE_STD;
#define DEF_DIRS ({"wsch^od"})
void create_trakt() 
{
    set_short("Trakt");
    add_exit(TRAKT_RINDE_LOKACJE + "trakt2.c","nw",0,4,0);
//add_exit(TRAKT_RINDE_LOKACJE + "trakt21.c","e",0,4,0); do Murivel TODO

    add_prop(ROOM_I_INSIDE,0);
}
public string
exits_description() 
{
    return "Trakt prowadzi na p^o^lnocny-zach^od i wsch^od.\n";
}
int unq_no_move(string str)
{
    ::unq_no_move(str);

    if (member_array(query_verb(), DEF_DIRS) != -1)
        notify_fail("Niestety, trakt jest dopiero w trakcie odbudowy, nie "+
            "mo^zesz tam p^oj^s^c w tym momencie.\n");
    return 0;
}

string
dlugi_opis()
{
    string str;

    if(jest_dzien())
    {
        if(pora_roku() != MT_ZIMA)
        {
    str = "Opis traktu w dzien, !zima";
        }
        else
        {
    str = "Opis traktu w dzien, zima";
        }
    }
    else
    {
        if(pora_roku() != MT_ZIMA)
        {
    str = "Opis traktu noc, !zima.";
        }
        else
        {
    str = "Opis traktu noc, zima.";
        }
    }
    str += "\n";

    return str;
}