/* Autor: Avard
   Opis : Brz^ozek
   Data : 11.08.06 */
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"
#include <pogoda.h>

inherit TRAKT_RINDE_STD;
inherit "/lib/drink_water";

void create_trakt() 
{
    set_short("Trakt");
    add_exit(TRAKT_RINDE_LOKACJE + "trakt14.c","se",0,4,0);
    add_exit(TRAKT_RINDE_LOKACJE + "trakt16.c","sw",0,4,0);
    add_exit(LAKA_RINDE_LOKACJE + "laka54.c","e",0,4,0);
    add_exit(LAKA_RINDE_LOKACJE + "laka49.c","ne",0,4,0);
    add_exit(LAS_RINDE_LOKACJE + "wlas12.c","n",0,4,0);
    add_exit(LAS_RINDE_LOKACJE + "wlas13.c","nw",0,4,0);
    add_exit(LAS_RINDE_LOKACJE + "wlas16.c","w",0,4,0);
    set_drink_places(({"z rzeki","z pontaru","z Pontaru"}));
    add_sit(({"nad rzek^a","nad pontarem","nad Pontarem"}),
        "nad rzek^a","z nad rzeki",0);
    add_prop(ROOM_I_INSIDE,0);
    add_item(({"Pontar","pontar","rzek^e"}),"P^lyn^acy tu od tysi^ecy lat "+
    "szeroki Pontar zdaje si^e by^c wci^a^z nieujarzmionym i niezdobytym, "+
    "wyznacza nie tylko granice mi^edzy kr^olestwami Redanii i Temeri, "+
    "ale i skutecznie ogranicza ludzkie zap^edy w wyrywaniu z r^ak "+
    "matki Natury wszystkiego, co sie jej nale^zy. Gdzieniegdzie "+
    "pojawiaj^ace si^e z nienacka niewielkie zawirowania wody na "+
    "spokojnie, jakby leniwie poruszaj^acej si^e m^etnej tafli swiadcz^a "+
    "o nieprzywidywalno^sci i zdradzieckiej naturze nurtu, kt^ory zapewne "+
    "pochlon^a^l niejedno istnienie bezmy^slnie probuj^ace zmierzy^c si^e z "+
    "pot^eg^a rzeki.\n");
}
public string
exits_description() 
{
    return "Trakt prowadzi na po^ludniowy-zach^od i po^ludniowy-wsch^od.\n";
}
void
init()
{
    ::init();
    init_drink_water(); 
} 
public int
unq_no_move(string str)
{
    notify_fail("Niestety, ale krzaki nie pozwalaj^a ci p^oj^s^c "+
        "w tym kierunku.\n");
    return 0;
}
string
dlugi_opis()
{
    string str;
    if(jest_dzien() == 1)
    {
    if(pora_roku() != MT_ZIMA)
    {
        str = "Biegn^acy z po^ludniowego-wschodu, od bram Rinde trakt, "+
            "wystarczaj^aco szeroki, aby bez trudu min^e^ly si^e na nim "+
            "dwa wy^ladowane, kupieckie wozy skr^eca tutaj na po^ludniowy-"+
            "zach^od w kierunku por^eby i niknie dalej w do^s^c g^estym "+
            "lesie porastaj^acym brzegi Pontaru. Grube drzewa rosn^ace "+
            "po obu jego stronach splataj^a swe ga^l^ezie tworz^ac g^esty "+
            "dach nad wy^lo^zon^a dopasowanymi kamieniami niezbyt r^own^a "+
            "drog^a. ";
        if(sizeof(FILTER_LIVE(all_inventory()))>2)
        {
            str += "Harmider spowodowany przez przemieszczaj^acych si^e "+
               "w^edrowc^ow nape^lnia przestrze^n cienistego tunelu "+
                "irytuj^ac^a kakofoni^a d^xwi^ek^ow. ";
        }
        else
        {
            str += "Brz^eczenie owad^ow i ^swiergot uganiaj^acych "+
                "si^e za nimi ptak^ow nape^lniaj^a przestrze^n "+
                "cienistego tunelu mi^lymi dla ucha d^xwi^ekami. ";
        }
        str += "Brunatne b^locko pokrywa prawie wszystko dooko^la � "+
            "kamienie, kt^orymi wy^lo^zona jest droga, mech rosn^acy w "+
            "przerwach pomi^edzy nimi, pobocze, a nawet cz^e^sciowo pnie "+
            "drzew rosn^acych wzd^lu^z traktu. Zielone, kolczaste krzewy "+
            "wype^lniaj^ace prawie ca^lkowicie przestrze^n miedzy drzewami "+
            "po obu stronach drogi i nieliczne, rachityczne zielska "+
            "rosn^ace na poboczach to jedyna ro^slinno^s^c, kt^ora nie "+
            "uleg^la zadeptaniu przez watahy w^edrowc^ow. Za wyr^abanym "+
            "jakim^s ostrym narz^edziem i systematycznie poszerzanym "+
            "przej^sciem w zaro^slach po wschodniej stronie traktu "+
            "ci^agn^aca si^e a^z po horyzont r^ownina ";

        if(pora_roku() == MT_LATO || pora_roku() == MT_WIOSNA)
        {
            str +="po^lyskuje szmaragdow^a zieleni^a murawy";
        }
        if(pora_roku() == MT_JESIEN)
        {
            str +="faluje ^lanem z^locistych traw";
        }

        str +=", a zza znacznie ni^zszych krzak^ow po po^ludniowej "+
        "jego stronie dobiega ";
        if(CO_PADA(this_object()) == PADA_DESZCZ)
        {
            str += "gro^xne huczenie wzburzonych w^od Pontaru.";
        }
        if(MOC_OPADOW(this_object()) == NIC_NIE_PADA)
        {
            str += "jednostajny, melodyjny szum p^lyn^acych w^od Pontaru.";
        } 
    }
    else
    {
        str = "Biegn^acy od bram Rinde na po^ludniowym-wschodzie trakt, "+
            "wystarczaj^aco szeroki, aby bez trudu min^e^ly si^e na nim "+
            "dwa wy^ladowane, kupieckie wozy skr^eca tutaj na po^ludniowy-"+
            "zach^od w kierunku por^eby i niknie dalej w do^s^c g^estym "+
            "lesie porastaj^acym brzegi Pontaru. Obficie pokryte "+
            "^sniegiem i lodem grube drzewa rosn^ace po obu jego "+
            "stronach splataj^a swe ga^l^ezie tworz^ac g^esty dach "+
            "nad wy^lo^zon^a dopasowanymi kamieniami niezbyt r^own^a "+
            "drog^a. ";
        if(jest_dzien() == 1)
        {
            str +="Ochryp^le krakanie wron ";
        }
        else
        {
            str +="Trzeszczenie marzn^acych drzew ";
        }
        str += "m^aci panuj^ac^a w tym ";
        if(jest_dzien() == 1)
        {
            str +="ponurym ";
        }
        else
        {
            str +="ciemnym ";
        }
        str +="tunelu cisz^e. Oba pobocza pokryte zamarzni^et^a, brunatn^a "+
            "bryj^a w niczym ju^z nie przypominaj^ac^a ^sniegu nie "+
            "zach^ecaj^a do odpoczynku. Za przerw^a w okrytych grub^a "+
            "warstw^a ^sniegu zaro^slach po p^o^lnocnej stronie traktu ";
        if(jest_dzien() == 1)
        {
            str +="l^sni o^slepiaj^ac^a biel^a ";
        }
        else
        {
            str +="bieli si^e w ciemno^sciach ";
        }
        str +="pokryta ^sniegiem, ci^agn^aca si^e a^z po horyzont r^ownina, "+
            "a zza znacznie ni^zszych krzak^ow po po^ludniowej stronie "+
            "traktu dobiega ciche potrzaskiwanie p^ekaj^acego lodu "+
            "pokrywaj^acego Pontar.";
    }
    }
    if(jest_dzien() == 0)
    {
    if(pora_roku() != MT_ZIMA)
    {
        str = "Biegn^acy z po^ludniowego-wschodu, od bram Rinde trakt, "+
            "wystarczaj^aco szeroki, aby bez trudu min^e^ly si^e na nim "+
            "dwa wy^ladowane, kupieckie wozy skr^eca tutaj na po^ludniowy-"+
            "zach^od w kierunku por^eby i niknie dalej w do^s^c g^estym "+
            "lesie porastaj^acym brzegi Pontaru. Grube drzewa rosn^ace "+
            "po obu jego stronach splataj^a swe ga^l^ezie tworz^ac g^esty "+
            "dach nad wy^lo^zon^a dopasowanymi kamieniami niezbyt r^own^a "+
            "drog^a. ";
        if(sizeof(FILTER_LIVE(all_inventory()))>=2)
        {
            str += "Harmider spowodowany przez przemieszczaj^acych si^e "+
                "w^edrowc^ow nape^lnia przestrze^n mrocznego tunelu "+
                "irytuj^ac^a kakofoni^a d^xwi^ek^ow. ";
        }
        else
        {
            str += "Brz^eczenie owad^ow i pohukiwanie male^nkich "+
                "s^owek nape^lnia przestrze^n mrocznego tunelu "+
                "nieprzyjemnymi d^xwi^ekami mog^acymi napawa^c "+
                "strachem podczas d^lugich w^edr^owek traktem. "; 
        }
        str += "Brunatne b^locko pokrywa prawie wszystko dooko^la � "+
            "kamienie, kt^orymi wy^lo^zona jest droga, mech rosn^acy w "+
            "przerwach pomi^edzy nimi, pobocze, a nawet cz^e^sciowo pnie "+
            "drzew rosn^acych wzd^lu^z traktu. Zielone, kolczaste krzewy "+
            "wype^lniaj^ace prawie ca^lkowicie przestrze^n miedzy drzewami "+
            "po obu stronach drogi i nieliczne, rachityczne zielska "+
            "rosn^ace na poboczach to jedyna ro^slinno^s^c, kt^ora nie "+
            "uleg^la zadeptaniu przez watahy w^edrowc^ow. Za wyr^abanym "+
            "jakim^s ostrym narz^edziem i systematycznie poszerzanym "+
            "przej^sciem w zaro^slach po wschodniej stronie traktu "+
            "ci^agn^aca si^e a^z po horyzont r^ownina faluje ^lanem "+
            "poszarza^lych traw, a zza znacznie ni^zszych krzak^ow "+
            "po po^ludniowej jego stronie dobiega ";
        if(CO_PADA(this_object()) == PADA_DESZCZ)
        {
            str += "gro^xne huczenie wzburzonych w^od Pontaru.";
        }
        if(MOC_OPADOW(this_object()) == NIC_NIE_PADA)
        {
            str += "jednostajny, melodyjny szum p^lyn^acych w^od Pontaru.";
        }

    }
    else
    {
        str = "Biegn^acy od bram Rinde na po^ludniowym-wschodzie trakt, "+
            "wystarczaj^aco szeroki, aby bez trudu min^e^ly si^e na nim "+
            "dwa wy^ladowane, kupieckie wozy skr^eca tutaj na po^ludniowy-"+
            "zach^od w kierunku por^eby i niknie dalej w do^s^c g^estym "+
            "lesie porastaj^acym brzegi Pontaru. Obficie pokryte "+
            "^sniegiem i lodem grube drzewa rosn^ace po obu jego "+
            "stronach splataj^a swe ga^l^ezie tworz^ac g^esty dach "+
            "nad wy^lo^zon^a dopasowanymi kamieniami niezbyt r^own^a "+
            "drog^a. Trzeszczenie marzn^acych drzew m^aci panuj^ac^a w tym "+
            "ciemnym tunelu cisz^e. Oba pobocza pokryte zamarzni^et^a, "+
            "brunatn^a bryj^a w niczym ju^z nie przypominaj^ac^a ^sniegu "+
            "nie zach^ecaj^a do odpoczynku. Za przerw^a w okrytych grub^a "+
            "warstw^a ^sniegu zaro^slach po wschodniej stronie traktu "+
            "bieli si^e w ciemno^sciach pokryta ^sniegiem, ci^agn^aca si^e "+
            "a^z po horyzont r^ownina, a zza znacznie ni^zszych krzak^ow "+
            "po po^ludniowej stronie traktu dobiega ciche potrzaskiwanie "+
            "p^ekaj^acego lodu pokrywaj^acego Pontar.";
    }
    }
    str += "\n";
    return str;
}

string
opis_nocy()
{
    string str;
    if(pora_roku() != MT_ZIMA)
    {
        str = "Biegn^acy z po^ludniowego-wschodu, od bram Rinde trakt, "+
            "wystarczaj^aco szeroki, aby bez trudu min^e^ly si^e na nim "+
            "dwa wy^ladowane, kupieckie wozy skr^eca tutaj na po^ludniowy-"+
            "zach^od w kierunku por^eby i niknie dalej w do^s^c g^estym "+
            "lesie porastaj^acym brzegi Pontaru. Grube drzewa rosn^ace "+
            "po obu jego stronach splataj^a swe ga^l^ezie tworz^ac g^esty "+
            "dach nad wy^lo^zon^a dopasowanymi kamieniami niezbyt r^own^a "+
            "drog^a. ";
        if(sizeof(FILTER_LIVE(all_inventory()))>=2)
        {
            str += "Harmider spowodowany przez przemieszczaj^acych si^e "+
                "w^edrowc^ow nape^lnia przestrze^n mrocznego tunelu "+
                "irytuj^ac^a kakofoni^a d^xwi^ek^ow. ";
        }
        else
        {
            str += "Brz^eczenie owad^ow i pohukiwanie male^nkich "+
                "s^owek nape^lnia przestrze^n mrocznego tunelu "+
                "nieprzyjemnymi d^xwi^ekami mog^acymi napawa^c "+
                "strachem podczas d^lugich w^edr^owek traktem. "; 
        }
        str += "Brunatne b^locko pokrywa prawie wszystko dooko^la � "+
            "kamienie, kt^orymi wy^lo^zona jest droga, mech rosn^acy w "+
            "przerwach pomi^edzy nimi, pobocze, a nawet cz^e^sciowo pnie "+
            "drzew rosn^acych wzd^lu^z traktu. Zielone, kolczaste krzewy "+
            "wype^lniaj^ace prawie ca^lkowicie przestrze^n miedzy drzewami "+
            "po obu stronach drogi i nieliczne, rachityczne zielska "+
            "rosn^ace na poboczach to jedyna ro^slinno^s^c, kt^ora nie "+
            "uleg^la zadeptaniu przez watahy w^edrowc^ow. Za wyr^abanym "+
            "jakim^s ostrym narz^edziem i systematycznie poszerzanym "+
            "przej^sciem w zaro^slach po wschodniej stronie traktu "+
            "ci^agn^aca si^e a^z po horyzont r^ownina faluje ^lanem "+
            "poszarza^lych traw, a zza znacznie ni^zszych krzak^ow "+
            "po po^ludniowej jego stronie dobiega ";
        if(CO_PADA(this_object()) == PADA_DESZCZ)
        {
            str += "gro^xne huczenie wzburzonych w^od Pontaru.";
        }
        if(MOC_OPADOW(this_object()) == NIC_NIE_PADA)
        {
            str += "jednostajny, melodyjny szum p^lyn^acych w^od Pontaru.";
        }
    }
    else
    {
        str = "Biegn^acy od bram Rinde na po^ludniowym-wschodzie trakt, "+
            "wystarczaj^aco szeroki, aby bez trudu min^e^ly si^e na nim "+
            "dwa wy^ladowane, kupieckie wozy skr^eca tutaj na po^ludniowy-"+
            "zach^od w kierunku por^eby i niknie dalej w do^s^c g^estym "+
            "lesie porastaj^acym brzegi Pontaru. Obficie pokryte "+
            "^sniegiem i lodem grube drzewa rosn^ace po obu jego "+
            "stronach splataj^a swe ga^l^ezie tworz^ac g^esty dach "+
            "nad wy^lo^zon^a dopasowanymi kamieniami niezbyt r^own^a "+
            "drog^a. Trzeszczenie marzn^acych drzew m^aci panuj^ac^a w tym "+
            "ciemnym tunelu cisz^e. Oba pobocza pokryte zamarzni^et^a, "+
            "brunatn^a bryj^a w niczym ju^z nie przypominaj^ac^a ^sniegu "+
            "nie zach^ecaj^a do odpoczynku. Za przerw^a w okrytych grub^a "+
            "warstw^a ^sniegu zaro^slach po wschodniej stronie traktu "+
            "bieli si^e w ciemno^sciach pokryta ^sniegiem, ci^agn^aca si^e "+
            "a^z po horyzont r^ownina, a zza znacznie ni^zszych krzak^ow "+
            "po po^ludniowej stronie traktu dobiega ciche potrzaskiwanie "+
            "p^ekaj^acego lodu pokrywaj^acego Pontar.";
    }
    str += "\n";
    return str;
}

string
event_pora_roku()
{
    switch (pora_roku())
    {
        case MT_JESIEN:
        case MT_LATO:
        case MT_WIOSNA:
            return ({"Promienie s^loneczne przedzieraj^a si^e pomi^edzy "+
            "ga^l^eziami i maluj^a z^lociste plamy na brunatnych "+
            "kamieniach traktu.\n"})[random(1)];
        case MT_ZIMA:
            return ({"Wielka pecyna mokrego ^sniegu spada ci wprost za "+
            "ko^lnierz.\n","D^lugie, ^za^losne wycie wyg^lodnia^lego "+
            "wilczyska dudni w^sr^od rosn^acych wzd^lu^z traktu drzew.\n",
            "P^latki ^sniegu opadaj^a powoli zas^laniaj^ac bia^l^a "+
            "warstewk^a brunatn^a bryj^e pokrywaj^ac^a drog^e.\n",
            "Promienie s^loneczne przedzieraj^a si^e pomi^edzy ga^l^eziami "+
            "i maluj^a z^lociste plamy na brunatnych kamieniach traktu.\n"})
            [random(4)];
    }
}