/* Autor: Avard
   Opis : vortak
   Data : 02.12.06 */
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"

inherit TRAKT_RINDE_STD;
inherit "/lib/drink_water";
void create_trakt() 
{
    set_short("Trakt nad Pontarem.");
    add_exit(TRAKT_RINDE_LOKACJE + "trakt18.c","ne",0,4,0);
    add_exit(TRAKT_RINDE_LOKACJE + "trakt20.c","w",0,4,0);
    add_exit(LAS_RINDE_LOKACJE + "las56.c","n",0,4,0);
    set_drink_places(({"z rzeki","z pontaru","z Pontaru"}));
    add_sit(({"nad rzek^a","nad pontarem","nad Pontarem"}),
        "nad rzek^a","z nad rzeki",0);
    add_item(({"Pontar","pontar","rzek^e"}),"P^lyn^acy tu od tysi^ecy lat "+
    "szeroki Pontar zdaje si^e by^c wci^a^z nieujarzmionym i niezdobytym, "+
    "wyznacza nie tylko granice mi^edzy kr^olestwami Redanii i Temeri, "+
    "ale i skutecznie ogranicza ludzkie zap^edy w wyrywaniu z r^ak "+
    "matki Natury wszystkiego, co sie jej nale^zy. Gdzieniegdzie "+
    "pojawiaj^ace si^e z nienacka niewielkie zawirowania wody na "+
    "spokojnie, jakby leniwie poruszaj^acej si^e m^etnej tafli swiadcz^a "+
    "o nieprzywidywalno^sci i zdradzieckiej naturze nurtu, kt^ory zapewne "+
    "pochlon^a^l niejedno istnienie bezmy^slnie probuj^ace zmierzy^c si^e z "+
    "pot^eg^a rzeki.\n");

    add_prop(ROOM_I_INSIDE,0);
}
public string
exits_description() 
{
    return "Trakt prowadzi na zach^od i p^o^lnocny-wsch^od.\n";
}
void
init()
{
    ::init();
    init_drink_water(); 
} 


string
dlugi_opis()
{
    string str;
    if(jest_dzien() == 1)
    {
    if(pora_roku() != MT_ZIMA)
    {
        str = "Ubity, wy^lo^zony kamieniami trakt ci^agnie si^e z "+
            "po^ludniowego zachodu na p^o^lnoc. Jest na tyle szeroki, "+
            "^ze nawet za^ladowane po brzegi kupieckie wozy mog^a si^e na "+
            "nim spokojnie wymin^a^c. Niegdy^s musia^l by^c niezwykle "+
            "wygodny, jednak z biegiem lat nigdy nie remontowana "+
            "nawierzchnia uleg^la zniszczeniu. Niekt^ore kamienie "+
            "pokruszy^ly si^e, inne zosta^ly wyrwane za^s w tych, kt^ore "+
            "pozosta^ly wy^z^lobione s^a g^l^ebokie koleiny. Wszystkie "+
            "wyrwy wype^lnione s^a g^estym b^lotem, kt^orego sk^ladu wolisz "+
            "si^e nawet nie domy^sla^c. Na po^ludniu i wschodzie, ukryty "+
            "jedynie za w^askim pasmem krzak^ow leniwie toczy swe wody "+
            "Pontar. Z zachodu dobiega do ciebie szum bukowego lasu i "+
            "^spiew ukrytych w nim ptak^ow.";
    }
    else
    {
        str = "Ubity, wy^lo^zony kamieniami trakt ci^agnie si^e z "+
            "po^ludniowego zachodu na p^o^lnoc. Jest na tyle szeroki, "+
            "^ze nawet za^ladowane po brzegi kupieckie wozy mog^a si^e na "+
            "nim spokojnie wymin^a^c. Niegdy^s musia^l by^c niezwykle "+
            "wygodny, jednak z biegiem lat nigdy nie remontowana "+
            "nawierzchnia uleg^la zniszczeniu. Niekt^ore kamienie "+
            "pokruszy^ly si^e, inne zosta^ly wyrwane za^s w tych, kt^ore "+
            "pozosta^ly wy^z^lobione s^a g^l^ebokie koleiny. Wszystkie "+
            "wyrwy wype^lnione s^a zmar^xni^et^a brej^a, kt^orej sk^ladu "+
            "wolisz si^e nawet nie domy^sla^c. Na po^ludniu i wschodzie, za "+
            "w^askim pasmem krzak^ow ukry^l si^e skuty grub^a warstw^a lodu "+
            "Pontar. Na zachodzie widzisz przykryty grub^a ^snie^zn^a "+
            "pokryw^a bukowy las.";
    }
    }
    if(jest_dzien() == 0)
    {
    if(pora_roku() != MT_ZIMA)
    {
        str = "Ubity, wy^lo^zony kamieniami trakt ci^agnie si^e z "+
            "po^ludniowego zachodu na p^o^lnoc. Jest na tyle szeroki, "+
            "^ze nawet za^ladowane po brzegi kupieckie wozy mog^a si^e na "+
            "nim spokojnie wymin^a^c. Niegdy^s musia^l by^c niezwykle "+
            "wygodny, jednak z biegiem lat nigdy nie remontowana "+
            "nawierzchnia uleg^la zniszczeniu. Niekt^ore kamienie "+
            "pokruszy^ly si^e, inne zosta^ly wyrwane za^s w tych, kt^ore "+
            "pozosta^ly wy^z^lobione s^a g^l^ebokie koleiny. Wszystkie "+
            "wyrwy wype^lnione s^a g^estym b^lotem, kt^orego sk^ladu wolisz "+
            "si^e nawet nie domy^sla^c. Na po^ludniu i wschodzie, ukryty "+
            "jedynie za w^askim pasmem krzak^ow leniwie toczy swe wody "+
            "Pontar. Z zachodu dobiega do ciebie szum bukowego lasu i "+
            "^spiew ukrytych w nim ptak^ow.";
    }
    else
    {
        str = "Ubity, wy^lo^zony kamieniami trakt ci^agnie si^e z "+
            "po^ludniowego zachodu na p^o^lnoc. Jest na tyle szeroki, "+
            "^ze nawet za^ladowane po brzegi kupieckie wozy mog^a si^e na "+
            "nim spokojnie wymin^a^c. Niegdy^s musia^l by^c niezwykle "+
            "wygodny, jednak z biegiem lat nigdy nie remontowana "+
            "nawierzchnia uleg^la zniszczeniu. Niekt^ore kamienie "+
            "pokruszy^ly si^e, inne zosta^ly wyrwane za^s w tych, kt^ore "+
            "pozosta^ly wy^z^lobione s^a g^l^ebokie koleiny. Wszystkie "+
            "wyrwy wype^lnione s^a zmar^xni^et^a brej^a, kt^orej sk^ladu "+
            "wolisz si^e nawet nie domy^sla^c. Na po^ludniu i wschodzie, za "+
            "w^askim pasmem krzak^ow ukry^l si^e skuty grub^a warstw^a lodu "+
            "Pontar. Na zachodzie widzisz przykryty grub^a ^snie^zn^a "+
            "pokryw^a bukowy las.";
    }
    }
    str += "\n";
    return str;
}
string
opis_nocy()
{
    string str;
    if(pora_roku() != MT_ZIMA)
    {
        str = "Ubity, wy^lo^zony kamieniami trakt ci^agnie si^e z "+
            "po^ludniowego zachodu na p^o^lnoc. Jest na tyle szeroki, "+
            "^ze nawet za^ladowane po brzegi kupieckie wozy mog^a si^e na "+
            "nim spokojnie wymin^a^c. Niegdy^s musia^l by^c niezwykle "+
            "wygodny, jednak z biegiem lat nigdy nie remontowana "+
            "nawierzchnia uleg^la zniszczeniu. Niekt^ore kamienie "+
            "pokruszy^ly si^e, inne zosta^ly wyrwane za^s w tych, kt^ore "+
            "pozosta^ly wy^z^lobione s^a g^l^ebokie koleiny. Wszystkie "+
            "wyrwy wype^lnione s^a g^estym b^lotem, kt^orego sk^ladu wolisz "+
            "si^e nawet nie domy^sla^c. Na po^ludniu i wschodzie, ukryty "+
            "jedynie za w^askim pasmem krzak^ow leniwie toczy swe wody "+
            "Pontar. Z zachodu dobiega do ciebie szum bukowego lasu i "+
            "^spiew ukrytych w nim ptak^ow.";
    }
    else
    {
        str = "Ubity, wy^lo^zony kamieniami trakt ci^agnie si^e z "+
            "po^ludniowego zachodu na p^o^lnoc. Jest na tyle szeroki, "+
            "^ze nawet za^ladowane po brzegi kupieckie wozy mog^a si^e na "+
            "nim spokojnie wymin^a^c. Niegdy^s musia^l by^c niezwykle "+
            "wygodny, jednak z biegiem lat nigdy nie remontowana "+
            "nawierzchnia uleg^la zniszczeniu. Niekt^ore kamienie "+
            "pokruszy^ly si^e, inne zosta^ly wyrwane za^s w tych, kt^ore "+
            "pozosta^ly wy^z^lobione s^a g^l^ebokie koleiny. Wszystkie "+
            "wyrwy wype^lnione s^a zmar^xni^et^a brej^a, kt^orej sk^ladu "+
            "wolisz si^e nawet nie domy^sla^c. Na po^ludniu i wschodzie, za "+
            "w^askim pasmem krzak^ow ukry^l si^e skuty grub^a warstw^a lodu "+
            "Pontar. Na zachodzie widzisz przykryty grub^a ^snie^zn^a "+
            "pokryw^a bukowy las.";
    }
    str += "\n";
    return str;
}