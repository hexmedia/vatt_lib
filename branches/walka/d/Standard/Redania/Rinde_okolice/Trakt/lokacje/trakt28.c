/* Autor: Faeve 
   Opis : Faeve
   Data : 12.1.2007*/
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"

inherit TRAKT_RINDE_STD;

string sciezka_dzien_niezima();
string sciezka_dzien_zima();

void create_trakt() 
{
    set_short("Trakt");
    add_exit(TRAKT_RINDE_LOKACJE + "trakt27.c","e",0,4,0);
    add_exit(TRAKT_RINDE_LOKACJE + "trakt29.c","s",0,4,0);
    add_exit(LAS_RINDE_LOKACJE + "las51.c","nw",0,4,0);
   // add_exit(LAS_RINDE_LOKACJE + "las60.c","w",0,4,0);
    //add_exit(LAS_RINDE_LOKACJE + "las50.c","ne",0,4,0);
    //add_exit(LAS_RINDE_LOKACJE + "las67.c","se",0,4,0);
    add_exit(TRAKT_RINDE_LOKACJE + "trakt28b.c","n",0,8,0);

    add_object(TRAKT_RINDE_OBIEKTY+"glaz");
    dodaj_rzecz_niewyswietlana("g�az");

    add_prop(ROOM_I_INSIDE,0);
}
public string
exits_description() 
{
    return "Trakt prowadzi na po^ludnie i wsch^od.\n";
}

string
dlugi_opis()
{
    string str;

    if(jest_dzien())
    {
        if(pora_roku() != MT_ZIMA)
        {
    str = "W tym miejscu trakt ostro skr^eca, przez co wydaje si^e, ^ze las "+
"jest jeszcze ciemniejszy i bardziej ponury. Wysokie drzewa zdaj^a si^e "+
"sk^lania^c ku tobie swe pot^e^zne konary, niczym jakie^s pod^le stwory "+
"chc^ace zagar^a^c ci^e w swoje szpony. " + sciezka_dzien_niezima() +
"Wzd�u� drogi ro�nie kilka krzew�w berberysu i ja�owca, z ga��zi kt�rych "+
"dochodzi ptasi �wiergot, cichn�cy, gdy tylko zbli�a si� jaka� karawana. "+
"Natomiast na skraju traktu, tu� pod roz�o�ystym d�bem le�y ogromny g�az. "+
"Wygl�da tak, jakby boska r�ka go tam zrzuci�a po to, by zdro�eni w�drowcy "+
"mogli usi��� i odpocz�� po m�cz�cej podr�y.";
        }
        else
        {
    str = "W tym miejscu trakt ostro skr�ca, przez co wydaje si�, �e las "+
"jest jeszcze ciemniejszy i bardziej ponury. Wra�enia tego dodaje tak�e "+
"zalegaj�cy na drzewach, krzewach i �ci�ce �nieg. Wysokie drzewa zdaj� si� "+
"sk�ania� ku tobie swe pot�ne, o�nie�one konary, niczym jakie� pod�e stwory, "+
"chc�ce zagarn�� ci� w swoje szpony. " + sciezka_dzien_zima() +
"Roz�o�yste krzewy berberysu rosn�ce tu� przy trakcie prawie ca�kowicie "+
"przykry� �nie�nobia�y puch. Gdzieniegdzie wygl�daj� spod niego pi�knie "+
"kontrastuj�ce z nieskaziteln� biel�, podlu�ne, koralowoczerwone owoce. "+
"Natomiast na skraju traktu, tu� pod roz�o�ystym d�bem le�y ogromny g�az. "+
"Wygl�da tak, jakby boska r�ka go tam zrzuci�a po to, by zdro�eni w�drowcy "+
"mogli usi��� i odpocz�� po m�cz�cej podr�y. Teraz co prawda jest "+
"za�nie�ony, ale c� to za problem ten �nieg odgarn��?";
        }
    }
    else
    {
        if(pora_roku() != MT_ZIMA)
        {
    str = "W tym miejscu trakt ostro skr�ca, przez co wydaje si�, �e las "+
"jest jeszcze ciemniejszy i bardziej ponury, wra�enie to pot�gowane jest "+
"jeszcze bardziej przez panuj�cy wok� mrok. Wysokie drzewa zdaj� si� "+
"sk�ania� ku tobie swe pot�ne konary, niczym jakie� pod�e stwory chc�ce "+
"zagarn�� ci� w swoje szpony. Od ciemnej �ciany lasu odgradza ci� tylko kilka "+
"nieco ja�niejszych plam - s� to jakie� przydro�ne krzewy, bynajmniej nie "+
"dodaj�ce miejscu uroku. A lekko prze�wiecaj�cy mi�dzy drzewami ksi�yc, "+
"rzuca delikatne, srebrzyste �wiat�o na ga��zie d�bu, kt�re upiornie k�ad� "+
"cie� na le��cy pod nimi olbrzymi g�az.";
        }
        else
        {
    str = "W tym miejscu trakt ostro skr�ca, przez co wydaje si�, �e las "+
"jest jeszcze ciemniejszy i bardziej ponury, wra�enie to pot�gowane jest "+
"jeszcze bardziej przez panuj�cy wok� mrok. Wysokie drzewa, teraz o�nie�one, "+
"sk�pane w srebrnym �wietle ksi�yca, zdaj� si� sk�ania� ku tobie swe pot�ne "+
"konary, niczym jakie� pod�e stwory chc�ce zagarn�� ci� w swoje szpony. Od "+
"ciemnej �ciany lasu odgradza ci� tylko kilka nieco ja�niejszych, poktytych "+
"czapami �niegu, plam - s� to jakie� przydro�ne krzewy, bynajmniej nie "+
"dodaj�ce miejscu uroku. A lekko prze�wiecaj�cy mi�dzy drzewami ksi�yc, "+
"rzuca delikatne, srebrzyste �wiat�o na ga��zie d�bu, kt�re upiornie k�ad� "+
"cie� na le��cy pod nimi olbrzymi g�az. Tajemnoczo�� tego miejsca pot�guj� "+
"skrz�ce si�, jak tysi�ce mrocznych iskierek, p�atki �niegu, o�wietlane przez "+
"delikatne promienie ksi�yca, przebijaj�ce si� mi�dzy ga��ziami drzew. ";
        }
    }
    str += "\n";

    return str;
}

string
opis_nocy()
{
    string str;
    if(pora_roku() != MT_ZIMA)
    {
str = "W tym miejscu trakt ostro skr�ca, przez co wydaje si�, �e las jest "+
"jeszcze ciemniejszy i bardziej ponury, wra�enie to pot�gowane jest przez "+
"panuj�cy wok� mrok. Wysokie drzewa zdaj� si� sk�ania� ku tobie swe pot�ne "+
"konary, niczym jakie� pod�e stwory chc�ce zagarn�� ci� w swoje szpony. Od "+
"ciemnej �ciany lasu odgradza ci� tylko kilka nieco ja�niejszych plam - s� "+
"to jakie� przydro�ne krzewy, bynajmniej nie dodaj�ce miejscu uroku. A lekko "+
"prze�wiecaj�cy mi�dzy drzewami ksi�yc rzuca delikatne, srebrzyste �wiat�o "+
"na ga��zie d�bu, kt�re upiornie k�ad� cie� na le��cy pod nimi g�az.";
	}
else
{
str = "W tym miejscu trakt ostro skr�ca, przez co wydaje si�, �e las jest "+
"jeszcze ciemniejszy i bardziej ponury, wra�enie to pot�gowane jest jeszcze "+
"bardziej przez panuj�cy wok� mrok. Wysokie drzewa, teraz o�nie�one, "+
"sk�pane w srenrmym �wietle ksi�yca, zdaj� si� sk�ania� ku tobie swe "+
"pot�ne konary, niczym jakie� pod�e stwory chc�ce zagarn�� ci� w swoje "+
"szpony. Od ciemnej �ciany lasu odgradza ci� tylko kilka nieco ja�niejszych, "+
"poktytych czapami �niegu, plam - s� to jakie� przydro�ne krzewy, bynajmniej "+
"nie dodaj�ce miejscu uroku. A lekko prze�wiecaj�cy mi�dzy drzewami ksi�yc, "+
"rzuca delikatne, srebrzyste �wiat�o na ga��zie d�bu, kt�re upiornie k�ad� "+
"cie� na le��cy pod nimi g�az. Tajemnoczo�� tego miejsca pot�guj� skrz�ce "+
"si�, jak tysi�ce mrocznych iskierek, p�atki �niegu o�wietlane przez "+
"ksi�yc, przebijaj�cy si� mi�dzy ga��ziami drzew.";
}
str += "\n";
return str;
}


string
sciezka_dzien_niezima()
{
if(TP->query_skill(SS_AWARENESS) >= 30)
{
   if(TP->query_skill(SS_AWARENESS) <= 49)
   {
       return "Mimo to na p�nocy, w�r�d g�stych ga��zi dostrzegasz jaki� "+
"niewielki przesmyk.";
   }
   if(TP->query_skill(SS_AWARENESS) == 69)
   {
       return "Mimo to na p�nocy, mi�dzy g�stymi ga��ziami, zdaje si� "+
"majaczy� jaka� ma�o ucz�szczana lesna �cie�yna.";
   }
return "Mimo to a p�nocy dostrzegasz jak�� le�n� �cie�k�, ma�o "+
"ucz�szczan�, prowadz�c� mi�dzy g�stymi, drapi�cymi jak ciernie ga��ziami "+
"ja�owc�w.";

}
return "";
}

string
sciezka_dzien_zima()
{
if(TP->query_skill(SS_AWARENESS) >= 30)
{
  if(TP->query_skill(SS_AWARENESS) <= 49)
   {
       return "Mimo to na p�nocy, w�r�d g�stych ga��zi dostrzegasz jaki� "+
"niewielki przesmyk, jakby lekkie zag��bienie.";
   }
  if(TP->query_skill(SS_AWARENESS) <= 69)
   {
       return "Mimo to na p�nocy, mi�dzy g�stymi ga��ziami, zdaje si� "+
"majaczy� jaka� ma�o ucz�szczana lesna �cie�yna.";
   }
   return "Mimo to a p�nocy dostrzegasz jak�� le�n� �cie�k�, ma�o "+
"ucz�szczan�, prowadz�c� mi�dzy g�stymi, drapi�cymi jak ciernie ga��ziami "+
"ja�owc�w.";

}
return "";
}