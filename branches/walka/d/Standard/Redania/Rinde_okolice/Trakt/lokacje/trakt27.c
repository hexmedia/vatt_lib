/* Autor: 
   Opis : 
   Data : */
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"

inherit TRAKT_RINDE_STD;

void create_trakt() 
{
    set_short("Trakt");
    add_exit(TRAKT_RINDE_LOKACJE + "trakt26.c","ne",0,4,0);
    add_exit(TRAKT_RINDE_LOKACJE + "trakt28.c","w",0,4,0);
   // add_exit(LAS_RINDE_LOKACJE + "las50.c","n",0,4,0);
    add_exit(LAS_RINDE_LOKACJE + "las59.c","e",0,4,0);
    //add_exit(LAS_RINDE_LOKACJE + "las67.c","s",0,4,0);

    add_prop(ROOM_I_INSIDE,0);
}
public string
exits_description() 
{
    return "Trakt prowadzi na zach^od i p^o^lnocny-wsch^od.\n";
}

string
dlugi_opis()
{
    string str;

    if(jest_dzien())
    {
        if(pora_roku() != MT_ZIMA)
        {
    str = "Trakt w tym miejscu przypomina nieco slalom albo poskr�can� "+
"wst��eczk� - dopiero co sko�czy� si� jeden zakr�t - a ju� zaczyna si� "+
"kolejny. Droga, mimo �e szeroka - wcale nie jest wygodna. Pe�no w niej "+
"dziur i kolein. Dok�adnie po �rodku traktu znajduje si� eliptyczna wysepka "+
"zi� i traw, tworz�cych swoisty kolorowy dywan wok� wysokiej i smuk�ej "+
"brzozy. Praktycznie ze wszystkich stron, trakt obro�ni�ty jest drzewami; "+
"wzd�u� niego rosn� te� najr�niejsze krzewy i krzewinki - ja�owce, dzika "+
"r�a, bor�wki. Gdzie� na po�udniu s�ycha� szum przelewaj�cych si� mas wody "+
"- to Pontar, przeb�yskuj�cy gdzieniegdzie mi�dzy ga��ziami drzew.";
        }
        else
        {
    str = "Trakt w tym miejscu przypomina nieco slalom albo poskr�can� "+
"wst��eczk� - dopiero co sko�czy� sie jeden zakr�t - a ju� zaczyna si� "+
"kolejny. Droga, mimo �e szeroka - wcale nie jest wygodna. Pe�no w niej "+
"dziur i kolein. Teraz pokryta jest �niegiem, co wcale nie u�atwia podr�y. "+
"Dok�adnie po �rodku traktu znajduje si� eliptyczna wysepka nieco g�adszego "+
"�niegu, okalaj�cego niczym puszysty, bia�y dywan wysok� i smuk�� brzoz�. "+
"Praktycznie ze wszystkich stron trakt obro�ni�ty jest drzewami; wzd�u� "+
"niego rosn� te� najr�niejsze krzewy i krzewinki - ja�owce oraz jakie� "+
"inne, gubi�ce na zim� li�cie - te o ciernistych ga��ziach to prawdopodobnie "+
"berberysy i dzikie r�e. Gdzie� na po�udniu s�ycha� szum przelewaj�cych si� "+
"mas wody - to Pontar, przeb�yskuj�cy gdzieniegdzie mi�dzy konarami drzew.";
        }
    }
    else
    {
        if(pora_roku() != MT_ZIMA)
        {
    str = "Trakt w tym miejscu przypomina nieco slalom albo poskr�can� "+
"wst��eczk� - dopiero co sko�czy� si� jeden zakr�t - a ju� zaczyna si� "+
"kolejny. Droga, mimo �e szeroka - wcale nie jest wygodna. Pe�no w niej "+
"dziur i kolein, ledwo widocznych ze wzgl�du na do�� s�abe o�wietlenie. "+
"Mimo iluminacji - wok� jest tak ciemno, ze trudno dostrzec jakie� "+
"szczeg�y - granic� traktu wyznaczaj� cienie krzew�w, natomiast od po�udnia "+
"s�ycha� szum rzeki.";
        }
        else
        {
    str = "Trakt w tym miejscu przypomina nieco slalom albo poskr�can� "+
"wst��eczk� - dopiero co sko�czy� si� jeden zakr�t - a ju� zaczyna si� "+
"kolejny. Droga, mimo �e do�� szeroka - wcale nie jest wygodna. Pe�no w niej "+
"dziur i kolein, ledwo widocznych ze wzgl�du na do�� s�abe o�wietlenie, ale "+
"za to �wietnie wyczuwalnych. Teraz pokrytych dodatkowo �niegiem, co wcale "+
"nie u�atwia podr�y. Mimo �r�d�a �wiat�a wok� trudno dostrzec jakie� "+
"szczeg�y. Granic� traktu wyznaczaj� krzewy, kontrastowo odznaczaj�ce si� "+
"�nie�nymi czapami na tle lasu. Gdzie� na po�udniu s�ycha� szum rzeki.";
        }
    }
    str += "\n";

    return str;
}

string
opis_nocy()
{
    string str;
    if(pora_roku() != MT_ZIMA)
    {
        str = "Trakt w tym miejscu przypomina nieco slalom albo poskr�can� "+
"wst��eczk� - dopiero co sko�czy� si� jeden zakr�t - a ju� zaczyna si� "+
"kolejny. Droga, mimo �e do�� szeroka - wcale nie jest wygodna. Pe�no w niej "+
"dziur i kolein, nie widocznych co prawda w nocy, ale za to doskonale "+
"wyczuwalnych. W ciemno�ci nie wida� dok�adnie kontur�w otoczenia - wszystko "+
"sk�ada si� z plam - od bardzo ciemnych szaro�ci do g��bokiej czerni. Jedn� "+
"z ciemniejszych jest ta w kszta�cie wysokiego i smuk�ego drzewa, rosn�cego "+
"na samym �rodku szerokiego traktu, kt�rego granice wyznaczaj� cienie "+
"krzew�w rosn�cych wzd�u� niego. Gdzie� na po�udniu s�ycha� szum "+
"przelewaj�cych si� mas wody.";
    }
    else
    {
        str = "Trakt w tym miejscu przypomina nieco slalom albo poskr�can� "+
"wst��eczk� - dopiero co sko�czy� si� jeden zakr�t - a ju� zaczyna si� "+
"kolejny. Droga, mimo �e do�� szeroka - wcale nie jest wygodna. Pe�no w niej "+
"dziur i kolein, nie widocznych co prawda w nocy, ale za to �wietnie "+
"wyczuwalnych. Teraz pokryta jest �niegiem, co wcale nie u�atwia podr�y. "+
"Rozlewaj�ce si� wok� ciemne plamy drzew rozja�nia bielutki �nieg. Dzi�ki "+
"niemu mo�na dostrzec przydro�ne krzewy b�d�ce granic� traktu. Dok�adnie po "+
"�rodku dojrze� mo�na wysok�, smuk�� plam� drzewa o bia�awym pniu. Gdzie� na "+
"po�udniu s�ycha� szum Pontaru.";
    }
    str += "\n";
    return str;
}