
/* Autor: Sedzik
   Opis : Zwierzaczek
   Data : 18.03.07 */

inherit "/std/monster";
inherit "/std/act/action";
inherit "/std/act/chat";
inherit "/std/act/asking";

#include <std.h>
#include <ss_types.h>
#include <macros.h>
#include <stdproperties.h>
#include <pl.h>
#include <money.h>
#include "dir.h"

int czy_walka();
int walka = 0;
void random_przym(string str, int n);

void
create_monster()
{
    set_living_name("zb^oj");
    ustaw_odmiane_rasy("m^e^zczyzna");
    set_gender(G_MALE);
    dodaj_nazwy("bandyta");
    dodaj_nazwy("zb^oj");
   random_przym("oszpecony:oszpeceni brudny:brudni "+
        "^smierdz^acy:^smierdz^acy zaniedbany:zaniedbani "+
        "zakrwawiony:zakrwawieni ogolony:ogoleni||"+
        "agresywny:agresywni umi^e^sniony:umi^e^snieni wysoki:wysocy "+
        "niski:niscy kr^epy:kr^epi ogorza^ly:ogorzali",2);

    set_long("Ogolony cz^lowiek, kt^oremu si^e przygl^adasz, nie wygl^ada na "
	+"wykszta^lconego w jakimkolwiek tego s^lowa znaczeniu. Na nieco "
	+"puco^lowatej g^ebie widnieje niczym wielki kartofel nos, prawdopodobnie "
	+"niejednokrotnie z^lamany. Czerwone policzki osobnika ^swiadcz^a tak^ze "
	+"o jego zainteresowaniu testowaniem wszelakiej ma^sci trunk^ow. Pomimo "
	+"tego cz^lowiek zdaje si^e by^c sprawny w walce - a przynajmniej silny, "
	+"co sugeruje jego umi^e^sniona, kr^epa budowa cia^la.\n");

    set_stats (({ 90, 30, 100, 10, 10, 100 }));
    add_prop(CONT_I_WEIGHT, 70000);
    add_prop(CONT_I_HEIGHT, 185);
    
    set_cact_time(10);
    add_cact("beknij przeci^agle");
    add_cact("krzyknij Psia ma^c! Ch^edo^zony ty...");

    set_default_answer(VBFC_ME("default_answer"));
	
    add_armour(TRAKT_RINDE_OBIEKTY +"poszarpany_dlugi_Lc.c");
    add_weapon(TRAKT_RINDE_OBIEKTY +"prosta_ciemna.c");
    add_armour(TRAKT_RINDE_OBIEKTY +"czarne_luzne_Lc.c");
    add_armour(TRAKT_RINDE_OBIEKTY +"stary_cwiekowany_Lc.c");
    add_armour(TRAKT_RINDE_OBIEKTY +"znoszone_skorzane_Lc.c");
    add_armour(TRAKT_RINDE_OBIEKTY +"lekkie_skorzane_Lc.c");
    set_skill(SS_DEFENCE, 20 + random(6));
    set_skill(SS_PARRY, 40 + random(8));
    set_skill(SS_WEP_CLUB, 70 + random(11));

    add_prop(LIVE_I_NEVERKNOWN, 1);

	set_aggressive(1);
    set_attack_chance(100);
    set_npc_react(0);
}
 
void
powiedz_gdy_jest(object player, string tekst)
{
    if (environment(this_object()) == environment(player))
        this_object()->command(tekst);
}
string
default_answer()
{
    set_alarm(2.0, 0.0, "powiedz_gdy_jest", this_player(),
        "powiedz A co ja? Jaki czarownik jest, ze wszystko wiem? Patrz tylko, "
	+"jak ci ryja obije...");
    return "";
}



void
add_introduced(string imie_mia, string imie_bie)
{
    set_alarm(2.0, 0.0, "return_introduce", find_player(imie_mia));
}

void
return_introduce(object ob)
{
    command("powiedz Nie dla psa kie^lbasa.");
	command("splun z niesmakiem na ziemie");
}

void
emote_hook(string emote, object wykonujacy)
{
    switch (emote)
    {
        case "kopnij": set_alarm(1.5, 0.0, "nienajlepszy", wykonujacy);
                    break;
        case "spoliczkuj": set_alarm(1.5, 0.0, "nienajlepszy", wykonujacy);
                    break;
        case "opluj" : set_alarm(2.5, 0.0, "nienajlepszy", wykonujacy);
                    break;
        case "poca^luj":
              {
              if(wykonujacy->query_gender() == 1)
                switch(random(1))
                 {
                 case 0: command("emote m^owi: Te... nie pozwalaj se...."); break;        
                }
              else
                {
                   switch(random(1))
                   {
                   case 0:command("emote m^owi: Ty psie ch^edo^zony..."); break;
                   }
                }
              
                break;
                }
        case "prychnij":
              {
                switch(random(1))
                 {
                 case 0:command("spojrzyj lekcewazaco "+
                     "na "+ OB_NAME(wykonujacy)); break;        
                 }
                break;
               }
        case "przytul": set_alarm(0.5, 0.0, "malolepszy", wykonujacy);
                   break;
    }
}

void
malolepszy(object wykonujacy)
{
    switch(random(1))
    {
        case 0: set_alarm(0.5, 0.0, "powiedz_gdy_jest", this_player(),
			"emote m^owi: Ty psie ch^edo^zony...");
    }
}

void
nienajlepszy(object wykonujacy)
{
  switch(random(2))
    {
    case 0: 
      command("emote m^owi: Te... nie pozwalaj se...");
      break;
    case 1: 
      command("emote spluwa z niesmakiem na ziemi^e.");
      break;
    }
} 

void random_przym(string str, int n)
{
        mixed* groups = ({ });
        groups = explode(str, "||");
        int groups_size = sizeof(groups);
        if(n > groups_size)
                return;
        for(int i =0; i < groups_size; i++)
                groups[i] = explode(groups[i], " ");
        
        for(int a = 0; a < n; a++)
        {
                int group_index = random(sizeof(groups));
                int adj_index = random(sizeof(groups[group_index]));
                string* tmp = explode(groups[group_index][adj_index], ":");
                dodaj_przym(tmp[0], tmp[1]);
                groups = groups-({groups[group_index]});
        }
}
void
attacked_by(object wrog)
{
    if(walka==0) 
    {
        TO->command("krzyknij");
        set_alarm(1.0, 0.0, "czy_walka", 1);
        walka = 1;
        return ::attacked_by(wrog); 
    }
    else if(walka == 1)
    {
      set_alarm(1.0, 0.0, "czy_walka", 1);
      walka = 1;
      return ::attacked_by(wrog);
    }
}

int
czy_walka()
{
    if(!query_attack())
    {
       walka = 0;
       return 1;
    }
       else
       {
       set_alarm(1.0, 0.0, "czy_walka", 1);
       }
}

