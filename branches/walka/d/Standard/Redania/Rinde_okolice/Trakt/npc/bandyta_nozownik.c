
/* Autor: Avard
   Opis : Valor
   Data : 15.03.07 */

inherit "/std/monster";
inherit "/std/act/action";
inherit "/std/act/chat";
inherit "/std/act/asking";
#include <std.h>
#include <ss_types.h>
#include <macros.h>
#include <stdproperties.h>
#include <pl.h>
#include "dir.h"

void random_przym(string str, int n);

void
create_monster()
{
    ustaw_odmiane_rasy("m^e^zczyzna");
    set_gender(G_MALE);
    dodaj_nazwy("bandyta");
    dodaj_nazwy("zb^oj");
    random_przym("nerwowy:nerwowi porywczy:porwyczy zwinny:zwinni "+
        "zakrwawiony:zakrwawieni agresywny:agresywni||"+
        "szczup^ly:szczupli oszpecony:oszpeceni brudny:brudni",2);
     
    set_long("Oczy tego cz^lowieka sprawiaj^a wra^zenie jakby nigdy nie "+
        "zatrzyma^ly si^e na jednym przedmiocie d^lu^zej ni^z kilka sekund. "+
        "Liczne blizny pozwalaj^a s^adzi^c, ^ze na wiele go sta^c i nie boi "+
        "si^e wyzwa^n, a si^nce pod oczami i usta wykrzywione w wiecznie "+
        "drwi^acym u^smieszku dope^lniaj^a obrazu rasowego bandyty "+
        "przemierzaj^acego trakty. Raczej ^sredni wzrost i szczup^la "+
        "sylwetka, a do tego szybkie, nerwowe ruchy wskazuj^a na to, "+
        "^ze mo^ze m^e^zczyzna nie jest pot^e^znym wojownikiem, ale "+
        "bez problemu mo^ze wbi^c nieostro^znemu w^edrowcowi n^o^z w "+
        "plecy.\n");
    set_stats (({50,100,60,40,40,90}));//FIXME balans przed otwarciem
    add_prop(CONT_I_WEIGHT, 60000);
    add_prop(CONT_I_HEIGHT, 172);
    add_prop(LIVE_I_NEVERKNOWN, 1);
    set_aggressive(1);
    set_attack_chance(100);
    set_npc_react(0);
    
    set_cact_time(10);
    add_cact("emote m^owi: Ju^z jeste^s martw"+this_player()->koncowka("y","a")+"!");
    add_cact("emote m^owi: Po^zegnaj si^e z ^zyciem.");
    add_cact("usmiechnij sie drapieznie");

    add_armour(TRAKT_RINDE_OBIEKTY +"czarne_luzne_Lc.c");
    add_armour(TRAKT_RINDE_OBIEKTY +"stary_cwiekowany_Lc.c");
    add_armour(TRAKT_RINDE_OBIEKTY +"znoszone_skorzane_Lc.c");
    add_weapon(TRAKT_RINDE_OBIEKTY +"czarny_zakrzywiony.c");
    add_armour(TRAKT_RINDE_OBIEKTY +"lekkie_skorzane_Lc.c");
    set_default_answer(VBFC_ME("default_answer"));

    set_skill(SS_DEFENCE, 20 + random(6));
    set_skill(SS_PARRY, 40 + random(8));
    set_skill(SS_WEP_KNIFE, 70 + random(11));
}
void
powiedz_gdy_jest(object player, string tekst)
{
    if (environment(this_object()) == environment(player))
        this_object()->command(tekst);
}

string
default_answer()
{
    set_alarm(2.0, 0.0, "powiedz_gdy_jest", this_player(),
        "powiedz do "+ this_player()->query_name(PL_BIE) +
        "Chyba nie my^slisz, ^ze b^ede odpowiada^l na twoje pytania?");
    return "";
}

void
add_introduced(string imie_mia, string imie_bie)
{
    set_alarm(2.0, 0.0, "return_introduce", find_player(imie_mia));
}
void
return_introduce(object ob)
{
    command("'A co mnie to obchodzi?");
}
void random_przym(string str, int n)
{
        mixed* groups = ({ });
        groups = explode(str, "||");
        int groups_size = sizeof(groups);
        if(n > groups_size)
                return;
        for(int i =0; i < groups_size; i++)
                groups[i] = explode(groups[i], " ");
        
        for(int a = 0; a < n; a++)
        {
                int group_index = random(sizeof(groups));
                int adj_index = random(sizeof(groups[group_index]));
                string* tmp = explode(groups[group_index][adj_index], ":");
                dodaj_przym(tmp[0], tmp[1]);
                groups = groups-({groups[group_index]});
        }
}
