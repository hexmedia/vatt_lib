/* Autor: Avard
   Opis : Eria
   Data : 18.03.07 */
   
inherit "/std/plecak.c";

#include "/sys/formulas.h"
#include <stdproperties.h>
#include <wa_types.h>
#include <pl.h>
#include <materialy.h>
#include <object_types.h>

void
create_backpack()
{
    ustaw_nazwe("torba");
    dodaj_przym("po^latany","po^latani");
    dodaj_przym("brudny","brudni");
    set_long("Torba, kt^or^a kto^s uszy^l w wyra^xnym po^spiechu, nie "+
        "wygl^ada ani na now^a ani tym bardziej na czyst^a i u^zyteczn^a. "+
        "Zrobiona zosta^la z kilku kawa^lk^ow jakiej^s grubszej tkaniny "+
        "farbowanej na szaro, zszytych niedbale ze sob^a, co daje niezbyt "+
        "zachwycaj^ac^a ca^lo^s^c. Mimo to, jakie^s drobne przedmioty, bez "+
        "trudu powinny si^e do niej zmie^sci^c. Jej uchwyt sprawia "+
        "wra^zenie, jakby mia^l si^e zaraz przerwa^c.\n");

    add_prop(OBJ_I_VALUE, 20);
    add_prop(CONT_I_CLOSED, 1);
    add_prop(CONT_I_WEIGHT, 1000);
    add_prop(CONT_I_VOLUME, 1000);
    add_prop(CONT_I_MAX_WEIGHT, 10000);
    add_prop(CONT_I_MAX_VOLUME, 10000);
    add_prop(CONT_I_REDUCE_VOLUME, 125);
    set_type(O_INNE);
    ustaw_material(MATERIALY_TK_BAWELNA);
}
