/* Autor: Avard
   Opis : Khaes
   Data : 19.03.07 */

inherit "/std/weapon";
#include <stdproperties.h>
#include <macros.h>
#include <wa_types.h>
#include <ss_types.h>
#include <formulas.h>
#include <object_types.h>
#include <materialy.h>

void
create_weapon()
{
    ustaw_nazwe("toporek");
    dodaj_nazwy("topor");

    ustaw_material(MATERIALY_DR_DAB, 60);
    ustaw_material(MATERIALY_ZELAZO, 40);

    set_type(O_BRON_TOPORY);

    dodaj_przym("d^lugi","d^ludzy");
    dodaj_przym("lekki","lekcy");
    
    set_long("Toporek jest na tyle d^lugi i solidny, ^ze gdy oprzesz "+
        "si^e na jego ^zele^xcu z powodzeniem mo^ze ci s^lu^zy^c za lask^e. "+
        "W d^lugim na prawie metr stylisku kto^s wy^z^lobi^l lekkimi "+
        "naci^eciami li^scie bluszczu oplataj^ace bro^n na ca^lej jej "+
        "d^lugo^sci. Ma^le ostrze toporka znacz^a liczne rysy, a tu i "+
        "^owdzie znajdzie si^e te^z szczerba.\n");
           
    set_hit(24);  
    set_pen(20);   

    add_prop(OBJ_I_VALUE, 800);
    add_prop(OBJ_I_VOLUME, 2000);
    add_prop(OBJ_I_WEIGHT, 2000);

    set_wt(W_AXE);   
    set_dt(W_BLUDGEON | W_IMPALE | W_SLASH);  
    set_hands(W_ANYH);  
}
