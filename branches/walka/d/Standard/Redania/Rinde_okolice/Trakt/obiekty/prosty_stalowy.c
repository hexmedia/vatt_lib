/*
* Hadart, 17.12.2006
* Opis Tinardan
*/

inherit "/std/weapon";
#include <stdproperties.h>
#include <macros.h>
#include <wa_types.h>
#include <ss_types.h>
#include <formulas.h>
#include <object_types.h>
#include <materialy.h>

void
create_weapon()
{
    ustaw_nazwe("morgenstern");
    dodaj_nazwy("maczuga");

    ustaw_material(MATERIALY_DR_DAB, 70);
    ustaw_material(MATERIALY_ZELAZO, 30);

    set_type(O_BRON_MACZUGI);

    dodaj_przym("prosty","pro^sci");
    dodaj_przym("stalowy","stalowi");
    
    set_long("Nie przypadkowo ch^lopi nazywaj^a te bro^n \"gwiazd^a "
        + "zarann^a\". G^lowica, osadzona na grubym drzewcu b^lyska "
	+ "z^lowrogo ciemnym metalem. Od ci^e^zkiej kuli, niczym promienie, "
	+ "odstaj^a mocne, d^lugie na dobre kilka cali, budz^ace groz^e "
	+ "kolce. Drzewce jest kr^otkie, wykonane najprawdopodobniej "
	+ "z ca^lkiem niez^lej jako^sci drewna, najpewniej d^ebowego, "
	+ "dodatkowo hartowanego i nabitego ^cwiekami. Cho^c bro^n "
	+ "wygl^ada na prymitywn^a, \"gwiazda zaranna\" we wprawnych "
        + "r^ekach z pewno^sci^a mo^ze by^c ^smiertelnie gro^xna. \n");
           
    set_hit(28);  
    set_pen(13);   

    add_prop(OBJ_I_VALUE, 2400);
    add_prop(OBJ_I_VOLUME, 1300);
    add_prop(OBJ_I_WEIGHT, 1300);

    set_wt(W_CLUB);   
    set_dt(W_BLUDGEON);  
    set_hands(W_ANYH);  
}
