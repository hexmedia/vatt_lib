
/* Autor: Avard
   Opis : Faeve
   Data : 16.03.07 */

inherit "/std/armour";

#include "/sys/wa_types.h"
#include "/sys/formulas.h"
#include "/sys/stdproperties.h"
#include <pl.h>
#include <macros.h>
#include <materialy.h>
#include <object_types.h>
void
create_armour()
{  
    ustaw_nazwe_glowna("para");
    ustaw_nazwe("buty");

    dodaj_przym("znoszony","znoszeni");
    dodaj_przym("sk^orzany","sk^orzani");

    set_long("S^a ju^z bardzo znoszone. Sk^ora na noskach jest po^scierana "+
        "i odchodzi niedu^zymi p^latkami. Podeszwa jest gruba i mocna, "+
        "dzi^eki czemu w^la^sciciel obuwia nie musi obawia^c si^e dalekich "+
        "w^edr^owek. Wygod^e zwi^eksza dodatkowo cholewka, si^egaj^aca "+
        "grubo ponad kostk^e - od wewn^atrz podszyta filcem tak, by nie "+
        "ociera^la ^lydek.\n");
		
	set_slots(A_FEET);
	add_prop(OBJ_I_VOLUME, 1000);
	add_prop(OBJ_I_WEIGHT, 500);
    add_prop(OBJ_I_VALUE, 56);
    add_prop(ARMOUR_S_DLA_RASY, "krasnolud");
    add_prop(ARMOUR_I_DLA_PLCI, 0);
    set_size("L");
	ustaw_material(MATERIALY_SK_SWINIA);
	set_type(O_UBRANIA);
}