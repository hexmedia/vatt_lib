inherit "/std/gate.c";
#include <stdproperties.h>
#include <macros.h>
#include "dir.h"


#define POLNOC            (RINDE + "Polnoc/")
#define POLNOC_LOKACJE    (POLNOC + "lokacje/")

void
create_gate()
{
    set_other_room(POLNOC_LOKACJE+"braman");
    set_open(1);
    set_locked(0);
    add_prop(GATE_IS_OUTSIDE,1);
    set_gate_id("polnoc_rinde");
    set_pass_command(({"po�udnie","s"}));
    set_pass_mess("przez bram� do miasta");
    godzina_zamkniecia(21);
    godzina_otwarcia(5);
    set_skad("z Rinde");
    dodaj_przym("pot�ny","pot�ni");
    set_long("@@dlugasny@@");
}

string dlugasny()
{
    string str;

    str="Jest to du�a dwuskrzyd�owa brama miejska. ";

    str+= opened_or_closed_desc();
    return str+"\n";

}
