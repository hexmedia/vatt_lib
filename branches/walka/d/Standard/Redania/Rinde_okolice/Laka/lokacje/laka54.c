/* Opis : Leci z systemu opis� z std. */
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"

inherit LAKA_RINDE_STD;
static string opis=losuj_kurwa_long();

void create_laka() 
{
    set_short("Gdzie^s w^sr^od ^l^ak.");
    add_exit(TRAKT_RINDE_LOKACJE + "trakt15.c","w",0,8,1);
    add_exit(TRAKT_RINDE_LOKACJE + "trakt14.c","s",0,8,1);
    add_exit(TRAKT_RINDE_LOKACJE + "trakt13.c","se",0,8,1);
    add_exit(TRAKT_RINDE_LOKACJE + "trakt12.c","e",0,8,1);
    add_exit(LAKA_RINDE_LOKACJE + "laka48.c","ne",0,8,1);
    add_exit(LAKA_RINDE_LOKACJE + "laka49.c","n",0,8,1);
    add_exit(LAS_RINDE_LOKACJE + "wlas12.c","nw",0,8,1);
    add_prop(ROOM_I_INSIDE,0);
}

string dlugi_opis()
{
    return opis;
}