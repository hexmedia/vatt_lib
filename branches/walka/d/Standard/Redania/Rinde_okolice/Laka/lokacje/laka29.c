/* Opis : Leci z systemu opis�w z std. */
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"

inherit LAKA_RINDE_STD;
static string opis=losuj_kurwa_long();

void create_laka() 
{
    set_short("Gdzie^s w^sr^od ^l^ak.");
    add_exit(LAKA_RINDE_LOKACJE + "laka30.c","w",0,8,1);
    add_exit(LAKA_RINDE_LOKACJE + "laka35.c","sw",0,8,1);
    add_exit(LAKA_RINDE_LOKACJE + "laka34.c","s",0,8,1);
    add_exit(TRAKT_RINDE_LOKACJE + "trakt8.c","se",0,8,1);
    add_exit(LAKA_RINDE_LOKACJE + "laka28.c","e",0,8,1);
    add_exit(LAKA_RINDE_LOKACJE + "laka23.c","nw",0,8,1);
    add_prop(ROOM_I_INSIDE,0);
    add_npc(LAKA_RINDE_LIVINGI + "zajac");
}

string dlugi_opis()
{
    return opis;
}