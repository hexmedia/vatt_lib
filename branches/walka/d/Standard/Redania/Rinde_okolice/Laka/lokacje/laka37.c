
/* Autor: Avard
   Opis : Brz^ozek
   Data : 28.07.06 */

#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"

inherit LAKA_RINDE_STD;
static string opis=losuj_kurwa_long();

void create_laka() 
{
    set_short("^L^aka na skraju por^eby");
    add_exit(LAKA_RINDE_LOKACJE +"laka33.c","nw",0,8,1);
    add_exit(LAKA_RINDE_LOKACJE +"laka38.c","w",0,8,1);
    add_exit(LAS_RINDE_LOKACJE +"wlas9.c","sw",0,8,1);
    add_exit(LAKA_RINDE_LOKACJE +"laka43.c","s",0,8,1);
    add_exit(LAKA_RINDE_LOKACJE + "laka32.c","n",0,8,1);
    add_exit(LAKA_RINDE_LOKACJE + "laka31.c","ne",0,8,1);
    add_exit(LAKA_RINDE_LOKACJE + "laka36.c","e",0,8,1);
    add_exit(LAKA_RINDE_LOKACJE + "laka42.c","se",0,8,1);

    add_prop(ROOM_I_INSIDE,0);

}

string dlugi_opis()
{
    return opis;
}