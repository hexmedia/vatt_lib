/* Opis : Leci z systemu opis�w z std. */
#include <stdproperties.h>
#include <ss_types.h>
#include <macros.h>
#include <filter_funs.h>
#include "dir.h"

inherit LAKA_RINDE_STD;

static string opis=losuj_kurwa_long();

void create_laka() 
{
    set_short("Gdzie^s w^sr^od ^l^ak.");
    add_exit(LAKA_RINDE_LOKACJE + "laka10.c","sw",0,8,1);
    add_exit(LAKA_RINDE_LOKACJE + "laka9.c","s",0,8,1);
    add_exit(LAKA_RINDE_LOKACJE + "laka8.c","se",0,8,1);
    add_exit(LAKA_RINDE_LOKACJE + "laka6.c","e",0,8,1);
    add_exit(LAKA_RINDE_LOKACJE + "laka5.c","ne",0,8,1);
    add_prop(ROOM_I_INSIDE,0);
}

string dlugi_opis()
{
    return opis;
}